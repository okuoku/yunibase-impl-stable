/*===========================================================================*/
/*   (Unsafe/bignumber.scm)                                                  */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Unsafe/bignumber.scm -indent -o objs/obj_u/Unsafe/bignumber.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

/* User header */
#if( BGL_HAVE_GMP == 0 )
#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___BIGNUM_TYPE_DEFINITIONS
#define BGL___BIGNUM_TYPE_DEFINITIONS
#endif													// BGL___BIGNUM_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t bgl_seed_rand(long);
	BGL_EXPORTED_DECL bool_t BXZERO(obj_t);
	extern obj_t BGl_u8vectorzd2ze3listz31zz__srfi4z00(obj_t);
	extern obj_t BGl_makezd2u8vectorzd2zz__srfi4z00(long, uint8_t);
	BGL_EXPORTED_DECL obj_t bgl_long_to_bignum(long);
	static obj_t BGl_fixnumzd2listzd2ze3bignumze3zz__bignumz00(obj_t, long);
	static obj_t BGl_z62z42z42bitlshbxz62zz__bignumz00(obj_t, obj_t, obj_t);
	extern long BGl_modulofxz00zz__r4_numbers_6_5_fixnumz00(long, long);
	static obj_t BGl_z62z42z42divrembxz62zz__bignumz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62z42z42stringzd2ze3bignumz53zz__bignumz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62z42z42bitmaskbxz62zz__bignumz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t bgl_bignum_neg(obj_t);
	static obj_t BGl_z62z42z42remainderbxz62zz__bignumz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62z42z42negbxz62zz__bignumz00(obj_t, obj_t);
	static obj_t BGl_z62z42z42stringzd2ze3integerzd2objz81zz__bignumz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t bgl_flonum_to_bignum(double);
	static obj_t BGl_bignumzd2ze3fixnumzd2listze3zz__bignumz00(obj_t, long);
	static obj_t BGl_requirezd2initializa7ationz75zz__bignumz00 = BUNSPEC;
	static obj_t BGl_z62z42z42za2bxzc0zz__bignumz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BXPOSITIVE(obj_t);
	extern obj_t BGl_makezd2u16vectorzd2zz__srfi4z00(long, uint16_t);
	static obj_t BGl_z62z42z42oddbxzf3z91zz__bignumz00(obj_t, obj_t);
	static obj_t BGl_z62z42z42quotientbxz62zz__bignumz00(obj_t, obj_t, obj_t);
	static int64_t BGl_bignumzd2minzd2int64zd2divzd2radixz00zz__bignumz00(void);
	BGL_EXPORTED_DECL obj_t bgl_bignum_to_string(obj_t, long);
	static obj_t BGl_z62z42z42randbxz62zz__bignumz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t bgl_bignum_not(obj_t);
	static obj_t BGl_bignumzd2sum2zd2zz__bignumz00(obj_t, obj_t, long, long);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__bignumz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_z62z42z42bitorbxz62zz__bignumz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL long bgl_bignum_to_elong(obj_t);
	BGL_EXPORTED_DECL obj_t bgl_bignum_sub(obj_t, obj_t);
	static obj_t BGl_z62z42z42bignumzd2ze3elongz53zz__bignumz00(obj_t, obj_t);
	static obj_t BGl_bignumzd2exptzd2zz__bignumz00(obj_t, obj_t);
	static obj_t BGl_z62z42z42zb2bxzd0zz__bignumz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t bgl_bignum_quotient(obj_t, obj_t);
	static obj_t BGl_z62z42z42exptbxz62zz__bignumz00(obj_t, obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zz__bignumz00(void);
	static obj_t BGl_z42z42fixnumzd2ze3bignumzd2freshze3zz__bignumz00(long);
	static obj_t BGl_z62z42z42bitnotbxz62zz__bignumz00(obj_t, obj_t);
	static obj_t BGl_z62z42z42bignumzd2ze3uint64z53zz__bignumz00(obj_t, obj_t);
	static obj_t BGl_z62z42z42uint64zd2ze3bignumz53zz__bignumz00(obj_t, obj_t);
	static long BGl_bignumzd2integerzd2lengthz00zz__bignumz00(obj_t);
	static obj_t BGl_convertze70ze7zz__bignumz00(obj_t, obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__bignumz00(void);
	static obj_t BGl_makezd2randomzd2u8vectorz00zz__bignumz00(obj_t);
	static obj_t BGl_z62z42z42positivebxzf3z91zz__bignumz00(obj_t, obj_t);
	extern obj_t bgl_reverse(obj_t);
	extern obj_t BGl_assvz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zz__bignumz00(void);
	static long BGl_bignumzd2signzd2zz__bignumz00(obj_t);
	extern long bgl_bignum_to_long(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__bignumz00(void);
	BGL_EXPORTED_DECL obj_t bgl_bignum_rsh(obj_t, long);
	static obj_t BGl_gczd2rootszd2initz00zz__bignumz00(void);
	static obj_t BGl_z62z42z42fixnumzd2ze3bignumz53zz__bignumz00(obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zz__bignumz00(void);
	static obj_t BGl_bignumzd2signzd2setz12z12zz__bignumz00(obj_t, int);
	BGL_EXPORTED_DECL obj_t bgl_bignum_xor(obj_t, obj_t);
	extern obj_t BGl_listzd2ze3stringz31zz__r4_strings_6_7z00(obj_t);
	BGL_EXPORTED_DECL obj_t bgl_bignum_or(obj_t, obj_t);
	extern obj_t bgl_bignum_mul(obj_t, obj_t);
	static obj_t BGl_z62z42z42flonumzd2ze3bignumz53zz__bignumz00(obj_t, obj_t);
	static obj_t BGl_z62z42z42zd2bxzb0zz__bignumz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_memvz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_bignumzd2shrinkzd2zz__bignumz00(obj_t, long);
	static obj_t BGl_z62z42z42seedzd2randzb0zz__bignumz00(obj_t, obj_t);
	static obj_t BGl_bignumzd2removezd2leadingzd2za7eroesz75zz__bignumz00(obj_t);
	BGL_EXPORTED_DECL int64_t bgl_bignum_to_int64(obj_t);
	BGL_EXPORTED_DECL obj_t bgl_elong_to_bignum(long);
	BGL_EXPORTED_DECL long bgl_bignum_to_long(obj_t);
	BGL_EXPORTED_DECL obj_t bgl_bignum_mask(obj_t, long);
	static obj_t BGl_z62z42z42bignumzd2ze3int64z53zz__bignumz00(obj_t, obj_t);
	static obj_t BGl_z62z42z42elongzd2ze3bignumz53zz__bignumz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t bgl_bignum_gcd(obj_t, obj_t);
	static obj_t BGl_z62z42z42bignumzd2ze3stringz53zz__bignumz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62z42z42bitrshbxz62zz__bignumz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62z42z42gcdbxz62zz__bignumz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62z42z42bitxorbxz62zz__bignumz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL double bgl_bignum_to_flonum(obj_t);
	extern obj_t BGl_moduloz00zz__r4_numbers_6_5_fixnumz00(obj_t, obj_t);
	static obj_t BGl_preallocatedzd2bignumszd2zz__bignumz00 = BUNSPEC;
	extern obj_t make_vector(long, obj_t);
	static obj_t BGl_bignumzd2subzd2nonnegz00zz__bignumz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t bgl_string_to_bignum(char *, int);
	BGL_EXPORTED_DECL obj_t bgl_bignum_and(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BXNEGATIVE(obj_t);
	static obj_t BGl_methodzd2initzd2zz__bignumz00(void);
	extern obj_t bgl_make_bignum(obj_t);
	static obj_t BGl_z62z42z42evenbxzf3z91zz__bignumz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t bgl_bignum_odd(obj_t);
	BGL_EXPORTED_DECL obj_t bgl_bignum_abs(obj_t);
	static bool_t BGl_z42zc3bxz81zz__bignumz00(obj_t, obj_t);
	static obj_t BGl_z62z42z42absbxz62zz__bignumz00(obj_t, obj_t);
	static obj_t BGl_z62z42z42za7erobxzf3z36zz__bignumz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t bgl_rand_bignum(obj_t);
	BGL_EXPORTED_DECL obj_t bgl_llong_to_bignum(BGL_LONGLONG_T);
	static obj_t BGl_z62z42z42llongzd2ze3bignumz53zz__bignumz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t bgl_bignum_remainder(obj_t, obj_t);
	static obj_t BGl_z62z42z42bitandbxz62zz__bignumz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t bgl_string_to_integer_obj(char *, long);
	static obj_t BGl_bignumzd2addzd2nonnegz00zz__bignumz00(obj_t, obj_t);
	static long BGl_bignumzd2minzd2elongzd2divzd2radixz00zz__bignumz00(void);
	BGL_EXPORTED_DECL obj_t bgl_int64_to_bignum(int64_t);
	BGL_EXPORTED_DECL obj_t bgl_bignum_mul(obj_t, obj_t);
	static obj_t BGl_z62z42z42int64zd2ze3bignumz53zz__bignumz00(obj_t, obj_t);
	static obj_t BGl_bignumzd2divzd2zz__bignumz00(obj_t, obj_t);
	static obj_t BGl_z62z42z42bignumzd2cmpzb0zz__bignumz00(obj_t, obj_t, obj_t);
	extern int64_t BGl_expts64z00zz__r4_numbers_6_5_fixnumz00(int64_t, int64_t);
	BGL_EXPORTED_DECL obj_t bgl_bignum_expt(obj_t, obj_t);
	static obj_t BGl_list2510z00zz__bignumz00 = BUNSPEC;
	static obj_t BGl_list2513z00zz__bignumz00 = BUNSPEC;
	static obj_t BGl_list2514z00zz__bignumz00 = BUNSPEC;
	extern obj_t bgl_long_to_bignum(long);
	BGL_EXPORTED_DECL obj_t bgl_bignum_lsh(obj_t, long);
	static obj_t BGl_list2515z00zz__bignumz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t bgl_bignum_div(obj_t, obj_t);
	static obj_t BGl_list2516z00zz__bignumz00 = BUNSPEC;
	static obj_t BGl_list2517z00zz__bignumz00 = BUNSPEC;
	static obj_t BGl_list2518z00zz__bignumz00 = BUNSPEC;
	extern bool_t BGl_evenzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	static obj_t BGl_bignumzd2za7eroz75zz__bignumz00 = BUNSPEC;
	extern long BGl_stringzd2ze3integerz31zz__r4_numbers_6_5_fixnumz00(obj_t,
		long, long);
	static long BGl_bignumzd2lengthzd2zz__bignumz00(obj_t);
	static obj_t BGl_z62z42z42bignumzd2ze3fixnumz53zz__bignumz00(obj_t, obj_t);
	extern obj_t BGl_quotientz00zz__r4_numbers_6_5_fixnumz00(obj_t, obj_t);
	static obj_t BGl_z62z42z42negativebxzf3z91zz__bignumz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t bgl_bignum_lcm(obj_t, obj_t);
	BGL_EXPORTED_DECL uint64_t bgl_bignum_to_uint64(obj_t);
	BGL_EXPORTED_DECL obj_t bgl_uint64_to_bignum(uint64_t);
	static obj_t BGl_z62z42z42lcmbxz62zz__bignumz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62z42z42bignumzd2ze3flonumz53zz__bignumz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t bgl_bignum_add(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t bgl_bignum_even(obj_t);
	static BGL_LONGLONG_T
		BGl_bignumzd2minzd2llongzd2divzd2radixz00zz__bignumz00(void);
	BGL_EXPORTED_DECL BGL_LONGLONG_T bgl_bignum_to_llong(obj_t);
	static obj_t BGl_z62z42z42bignumzd2ze3llongz53zz__bignumz00(obj_t, obj_t);
	BGL_EXPORTED_DECL int bgl_bignum_cmp(obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_z42z42bignumzd2ze3flonumzd2envze3zz__bignumz00,
		BgL_bgl_za762za742za742bignumza72544z00,
		BGl_z62z42z42bignumzd2ze3flonumz53zz__bignumz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_z42z42positivebxzf3zd2envz21zz__bignumz00,
		BgL_bgl_za762za742za742positiv2545za7,
		BGl_z62z42z42positivebxzf3z91zz__bignumz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_z42z42divrembxzd2envzd2zz__bignumz00,
		BgL_bgl_za762za742za742divremb2546za7,
		BGl_z62z42z42divrembxz62zz__bignumz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_z42z42bignumzd2ze3stringzd2envze3zz__bignumz00,
		BgL_bgl_za762za742za742bignumza72547z00,
		BGl_z62z42z42bignumzd2ze3stringz53zz__bignumz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_z42z42zb2bxzd2envz60zz__bignumz00,
		BgL_bgl_za762za742za742za7b2bxza7d2548za7,
		BGl_z62z42z42zb2bxzd0zz__bignumz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_z42z42bignumzd2ze3llongzd2envze3zz__bignumz00,
		BgL_bgl_za762za742za742bignumza72549z00,
		BGl_z62z42z42bignumzd2ze3llongz53zz__bignumz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_z42z42za7erobxzf3zd2envz86zz__bignumz00,
		BgL_bgl_za762za742za742za7a7erob2550z00,
		BGl_z62z42z42za7erobxzf3z36zz__bignumz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_z42z42int64zd2ze3bignumzd2envze3zz__bignumz00,
		BgL_bgl_za762za742za742int64za7d2551z00,
		BGl_z62z42z42int64zd2ze3bignumz53zz__bignumz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_z42z42lcmbxzd2envzd2zz__bignumz00,
		BgL_bgl_za762za742za742lcmbxza762552z00, BGl_z62z42z42lcmbxz62zz__bignumz00,
		0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2500z00zz__bignumz00,
		BgL_bgl_string2500za700za7za7_2553za7, "&$$*bx", 6);
	      DEFINE_STRING(BGl_string2501z00zz__bignumz00,
		BgL_bgl_string2501za700za7za7_2554za7, "&$$exptbx", 9);
	      DEFINE_STRING(BGl_string2502z00zz__bignumz00,
		BgL_bgl_string2502za700za7za7_2555za7, "/bx", 3);
	      DEFINE_STRING(BGl_string2503z00zz__bignumz00,
		BgL_bgl_string2503za700za7za7_2556za7, "divide by zero", 14);
	      DEFINE_STRING(BGl_string2504z00zz__bignumz00,
		BgL_bgl_string2504za700za7za7_2557za7, "&$$divrembx", 11);
	      DEFINE_STRING(BGl_string2505z00zz__bignumz00,
		BgL_bgl_string2505za700za7za7_2558za7, "&$$quotientbx", 13);
	      DEFINE_STRING(BGl_string2506z00zz__bignumz00,
		BgL_bgl_string2506za700za7za7_2559za7, "&$$remainderbx", 14);
	      DEFINE_STRING(BGl_string2507z00zz__bignumz00,
		BgL_bgl_string2507za700za7za7_2560za7, "&$$absbx", 8);
	      DEFINE_STRING(BGl_string2508z00zz__bignumz00,
		BgL_bgl_string2508za700za7za7_2561za7, "&$$gcdbx", 8);
	      DEFINE_STRING(BGl_string2509z00zz__bignumz00,
		BgL_bgl_string2509za700za7za7_2562za7, "&$$lcmbx", 8);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_z42z42randbxzd2envzd2zz__bignumz00,
		BgL_bgl_za762za742za742randbxza72563z00,
		BGl_z62z42z42randbxz62zz__bignumz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2511z00zz__bignumz00,
		BgL_bgl_string2511za700za7za7_2564za7,
		"0123456789abcdefghijklmnopqrstuvwxyz", 36);
	      DEFINE_STRING(BGl_string2512z00zz__bignumz00,
		BgL_bgl_string2512za700za7za7_2565za7, "&$$bignum->string", 17);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_z42z42bignumzd2ze3fixnumzd2envze3zz__bignumz00,
		BgL_bgl_za762za742za742bignumza72566z00,
		BGl_z62z42z42bignumzd2ze3fixnumz53zz__bignumz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_z42z42gcdbxzd2envzd2zz__bignumz00,
		BgL_bgl_za762za742za742gcdbxza762567z00, BGl_z62z42z42gcdbxz62zz__bignumz00,
		0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2519z00zz__bignumz00,
		BgL_bgl_string2519za700za7za7_2568za7, "&$$string->bignum", 17);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_z42z42flonumzd2ze3bignumzd2envze3zz__bignumz00,
		BgL_bgl_za762za742za742flonumza72569z00,
		BGl_z62z42z42flonumzd2ze3bignumz53zz__bignumz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2520z00zz__bignumz00,
		BgL_bgl_string2520za700za7za7_2570za7, "&$$bignum->fixnum", 17);
	      DEFINE_STRING(BGl_string2521z00zz__bignumz00,
		BgL_bgl_string2521za700za7za7_2571za7, "&$$bignum->elong", 16);
	      DEFINE_STRING(BGl_string2522z00zz__bignumz00,
		BgL_bgl_string2522za700za7za7_2572za7, "&$$bignum->llong", 16);
	      DEFINE_STRING(BGl_string2523z00zz__bignumz00,
		BgL_bgl_string2523za700za7za7_2573za7, "&$$bignum->int64", 16);
	      DEFINE_STRING(BGl_string2524z00zz__bignumz00,
		BgL_bgl_string2524za700za7za7_2574za7, "&$$bignum->uint64", 17);
	      DEFINE_STRING(BGl_string2525z00zz__bignumz00,
		BgL_bgl_string2525za700za7za7_2575za7, "&$$seed-rand", 12);
	      DEFINE_STRING(BGl_string2526z00zz__bignumz00,
		BgL_bgl_string2526za700za7za7_2576za7, "&$$randbx", 9);
	      DEFINE_STRING(BGl_string2527z00zz__bignumz00,
		BgL_bgl_string2527za700za7za7_2577za7, "&$$flonum->bignum", 17);
	      DEFINE_STRING(BGl_string2528z00zz__bignumz00,
		BgL_bgl_string2528za700za7za7_2578za7, "real", 4);
	      DEFINE_STRING(BGl_string2529z00zz__bignumz00,
		BgL_bgl_string2529za700za7za7_2579za7, "&$$bignum->flonum", 17);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_z42z42exptbxzd2envzd2zz__bignumz00,
		BgL_bgl_za762za742za742exptbxza72580z00,
		BGl_z62z42z42exptbxz62zz__bignumz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2530z00zz__bignumz00,
		BgL_bgl_string2530za700za7za7_2581za7, "&$$bitlshbx", 11);
	      DEFINE_STRING(BGl_string2531z00zz__bignumz00,
		BgL_bgl_string2531za700za7za7_2582za7, "&$$bitrshbx", 11);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_z42z42stringzd2ze3bignumzd2envze3zz__bignumz00,
		BgL_bgl_za762za742za742stringza72583z00,
		BGl_z62z42z42stringzd2ze3bignumz53zz__bignumz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2532z00zz__bignumz00,
		BgL_bgl_string2532za700za7za7_2584za7, "bitmaskbx", 9);
	      DEFINE_STRING(BGl_string2533z00zz__bignumz00,
		BgL_bgl_string2533za700za7za7_2585za7, "not implemented", 15);
	      DEFINE_STRING(BGl_string2534z00zz__bignumz00,
		BgL_bgl_string2534za700za7za7_2586za7, "&$$bitmaskbx", 12);
	      DEFINE_STRING(BGl_string2535z00zz__bignumz00,
		BgL_bgl_string2535za700za7za7_2587za7, "bitorbx", 7);
	      DEFINE_STRING(BGl_string2536z00zz__bignumz00,
		BgL_bgl_string2536za700za7za7_2588za7, "&$$bitorbx", 10);
	      DEFINE_STRING(BGl_string2537z00zz__bignumz00,
		BgL_bgl_string2537za700za7za7_2589za7, "bitxorbx", 8);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_z42z42bitnotbxzd2envzd2zz__bignumz00,
		BgL_bgl_za762za742za742bitnotb2590za7,
		BGl_z62z42z42bitnotbxz62zz__bignumz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2538z00zz__bignumz00,
		BgL_bgl_string2538za700za7za7_2591za7, "&$$bitxorbx", 11);
	      DEFINE_STRING(BGl_string2539z00zz__bignumz00,
		BgL_bgl_string2539za700za7za7_2592za7, "bitandbx", 8);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_z42z42bitandbxzd2envzd2zz__bignumz00,
		BgL_bgl_za762za742za742bitandb2593za7,
		BGl_z62z42z42bitandbxz62zz__bignumz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_z42z42negativebxzf3zd2envz21zz__bignumz00,
		BgL_bgl_za762za742za742negativ2594za7,
		BGl_z62z42z42negativebxzf3z91zz__bignumz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_z42z42bignumzd2ze3uint64zd2envze3zz__bignumz00,
		BgL_bgl_za762za742za742bignumza72595z00,
		BGl_z62z42z42bignumzd2ze3uint64z53zz__bignumz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_z42z42uint64zd2ze3bignumzd2envze3zz__bignumz00,
		BgL_bgl_za762za742za742uint64za72596z00,
		BGl_z62z42z42uint64zd2ze3bignumz53zz__bignumz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_z42z42bignumzd2ze3elongzd2envze3zz__bignumz00,
		BgL_bgl_za762za742za742bignumza72597z00,
		BGl_z62z42z42bignumzd2ze3elongz53zz__bignumz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2540z00zz__bignumz00,
		BgL_bgl_string2540za700za7za7_2598za7, "&$$bitandbx", 11);
	      DEFINE_STRING(BGl_string2541z00zz__bignumz00,
		BgL_bgl_string2541za700za7za7_2599za7, "bitnotbx", 8);
	      DEFINE_STRING(BGl_string2542z00zz__bignumz00,
		BgL_bgl_string2542za700za7za7_2600za7, "&$$bitnotbx", 11);
	      DEFINE_STRING(BGl_string2543z00zz__bignumz00,
		BgL_bgl_string2543za700za7za7_2601za7, "__bignum", 8);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_z42z42evenbxzf3zd2envz21zz__bignumz00,
		BgL_bgl_za762za742za742evenbxza72602z00,
		BGl_z62z42z42evenbxzf3z91zz__bignumz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_z42z42bignumzd2cmpzd2envz00zz__bignumz00,
		BgL_bgl_za762za742za742bignumza72603z00,
		BGl_z62z42z42bignumzd2cmpzb0zz__bignumz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_z42z42absbxzd2envzd2zz__bignumz00,
		BgL_bgl_za762za742za742absbxza762604z00, BGl_z62z42z42absbxz62zz__bignumz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_z42z42oddbxzf3zd2envz21zz__bignumz00,
		BgL_bgl_za762za742za742oddbxza7f2605z00,
		BGl_z62z42z42oddbxzf3z91zz__bignumz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_z42z42seedzd2randzd2envz00zz__bignumz00,
		BgL_bgl_za762za742za742seedza7d22606z00,
		BGl_z62z42z42seedzd2randzb0zz__bignumz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_z42z42bitmaskbxzd2envzd2zz__bignumz00,
		BgL_bgl_za762za742za742bitmask2607za7,
		BGl_z62z42z42bitmaskbxz62zz__bignumz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2477z00zz__bignumz00,
		BgL_bgl_string2477za700za7za7_2608za7,
		"/tmp/bigloo/runtime/Unsafe/bignumber.scm", 40);
	      DEFINE_STRING(BGl_string2478z00zz__bignumz00,
		BgL_bgl_string2478za700za7za7_2609za7, "&$$string->integer-obj", 22);
	      DEFINE_STRING(BGl_string2479z00zz__bignumz00,
		BgL_bgl_string2479za700za7za7_2610za7, "bstring", 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_z42z42stringzd2ze3integerzd2objzd2envz31zz__bignumz00,
		BgL_bgl_za762za742za742stringza72611z00,
		BGl_z62z42z42stringzd2ze3integerzd2objz81zz__bignumz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_z42z42za2bxzd2envz70zz__bignumz00,
		BgL_bgl_za762za742za742za7a2bxza7c2612za7,
		BGl_z62z42z42za2bxzc0zz__bignumz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2480z00zz__bignumz00,
		BgL_bgl_string2480za700za7za7_2613za7, "bint", 4);
	      DEFINE_STRING(BGl_string2481z00zz__bignumz00,
		BgL_bgl_string2481za700za7za7_2614za7, "&$$elong->bignum", 16);
	      DEFINE_STRING(BGl_string2482z00zz__bignumz00,
		BgL_bgl_string2482za700za7za7_2615za7, "belong", 6);
	      DEFINE_STRING(BGl_string2483z00zz__bignumz00,
		BgL_bgl_string2483za700za7za7_2616za7, "&$$llong->bignum", 16);
	      DEFINE_STRING(BGl_string2484z00zz__bignumz00,
		BgL_bgl_string2484za700za7za7_2617za7, "bllong", 6);
	      DEFINE_STRING(BGl_string2485z00zz__bignumz00,
		BgL_bgl_string2485za700za7za7_2618za7, "&$$int64->bignum", 16);
	      DEFINE_STRING(BGl_string2486z00zz__bignumz00,
		BgL_bgl_string2486za700za7za7_2619za7, "bint64", 6);
	      DEFINE_STRING(BGl_string2487z00zz__bignumz00,
		BgL_bgl_string2487za700za7za7_2620za7, "&$$uint64->bignum", 17);
	      DEFINE_STRING(BGl_string2488z00zz__bignumz00,
		BgL_bgl_string2488za700za7za7_2621za7, "buint64", 7);
	      DEFINE_STRING(BGl_string2489z00zz__bignumz00,
		BgL_bgl_string2489za700za7za7_2622za7, "&$$fixnum->bignum", 17);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_z42z42bitlshbxzd2envzd2zz__bignumz00,
		BgL_bgl_za762za742za742bitlshb2623za7,
		BGl_z62z42z42bitlshbxz62zz__bignumz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_z42z42bitrshbxzd2envzd2zz__bignumz00,
		BgL_bgl_za762za742za742bitrshb2624za7,
		BGl_z62z42z42bitrshbxz62zz__bignumz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2490z00zz__bignumz00,
		BgL_bgl_string2490za700za7za7_2625za7, "&$$zerobx?", 10);
	      DEFINE_STRING(BGl_string2491z00zz__bignumz00,
		BgL_bgl_string2491za700za7za7_2626za7, "bignum", 6);
	      DEFINE_STRING(BGl_string2492z00zz__bignumz00,
		BgL_bgl_string2492za700za7za7_2627za7, "&$$negativebx?", 14);
	      DEFINE_STRING(BGl_string2493z00zz__bignumz00,
		BgL_bgl_string2493za700za7za7_2628za7, "&$$positivebx?", 14);
	      DEFINE_STRING(BGl_string2494z00zz__bignumz00,
		BgL_bgl_string2494za700za7za7_2629za7, "&$$evenbx?", 10);
	      DEFINE_STRING(BGl_string2495z00zz__bignumz00,
		BgL_bgl_string2495za700za7za7_2630za7, "&$$oddbx?", 9);
	      DEFINE_STRING(BGl_string2496z00zz__bignumz00,
		BgL_bgl_string2496za700za7za7_2631za7, "&$$bignum-cmp", 13);
	      DEFINE_STRING(BGl_string2497z00zz__bignumz00,
		BgL_bgl_string2497za700za7za7_2632za7, "&$$+bx", 6);
	      DEFINE_STRING(BGl_string2498z00zz__bignumz00,
		BgL_bgl_string2498za700za7za7_2633za7, "&$$-bx", 6);
	      DEFINE_STRING(BGl_string2499z00zz__bignumz00,
		BgL_bgl_string2499za700za7za7_2634za7, "&$$negbx", 8);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_z42z42bitxorbxzd2envzd2zz__bignumz00,
		BgL_bgl_za762za742za742bitxorb2635za7,
		BGl_z62z42z42bitxorbxz62zz__bignumz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_z42z42bitorbxzd2envzd2zz__bignumz00,
		BgL_bgl_za762za742za742bitorbx2636za7, BGl_z62z42z42bitorbxz62zz__bignumz00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_z42z42zd2bxzd2envz00zz__bignumz00,
		BgL_bgl_za762za742za742za7d2bxza7b2637za7,
		BGl_z62z42z42zd2bxzb0zz__bignumz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_z42z42quotientbxzd2envzd2zz__bignumz00,
		BgL_bgl_za762za742za742quotien2638za7,
		BGl_z62z42z42quotientbxz62zz__bignumz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_z42z42bignumzd2ze3int64zd2envze3zz__bignumz00,
		BgL_bgl_za762za742za742bignumza72639z00,
		BGl_z62z42z42bignumzd2ze3int64z53zz__bignumz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_z42z42elongzd2ze3bignumzd2envze3zz__bignumz00,
		BgL_bgl_za762za742za742elongza7d2640z00,
		BGl_z62z42z42elongzd2ze3bignumz53zz__bignumz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_z42z42fixnumzd2ze3bignumzd2envze3zz__bignumz00,
		BgL_bgl_za762za742za742fixnumza72641z00,
		BGl_z62z42z42fixnumzd2ze3bignumz53zz__bignumz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_z42z42llongzd2ze3bignumzd2envze3zz__bignumz00,
		BgL_bgl_za762za742za742llongza7d2642z00,
		BGl_z62z42z42llongzd2ze3bignumz53zz__bignumz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_z42z42remainderbxzd2envzd2zz__bignumz00,
		BgL_bgl_za762za742za742remaind2643za7,
		BGl_z62z42z42remainderbxz62zz__bignumz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_z42z42negbxzd2envzd2zz__bignumz00,
		BgL_bgl_za762za742za742negbxza762644z00, BGl_z62z42z42negbxz62zz__bignumz00,
		0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__bignumz00));
		     ADD_ROOT((void *) (&BGl_preallocatedzd2bignumszd2zz__bignumz00));
		     ADD_ROOT((void *) (&BGl_list2510z00zz__bignumz00));
		     ADD_ROOT((void *) (&BGl_list2513z00zz__bignumz00));
		     ADD_ROOT((void *) (&BGl_list2514z00zz__bignumz00));
		     ADD_ROOT((void *) (&BGl_list2515z00zz__bignumz00));
		     ADD_ROOT((void *) (&BGl_list2516z00zz__bignumz00));
		     ADD_ROOT((void *) (&BGl_list2517z00zz__bignumz00));
		     ADD_ROOT((void *) (&BGl_list2518z00zz__bignumz00));
		     ADD_ROOT((void *) (&BGl_bignumzd2za7eroz75zz__bignumz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__bignumz00(long
		BgL_checksumz00_5716, char *BgL_fromz00_5717)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__bignumz00))
				{
					BGl_requirezd2initializa7ationz75zz__bignumz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__bignumz00();
					BGl_cnstzd2initzd2zz__bignumz00();
					BGl_importedzd2moduleszd2initz00zz__bignumz00();
					return BGl_toplevelzd2initzd2zz__bignumz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__bignumz00(void)
	{
		{	/* Unsafe/bignumber.scm 45 */
			BGl_list2510z00zz__bignumz00 = MAKE_YOUNG_PAIR(BINT(0L), BNIL);
			BGl_list2513z00zz__bignumz00 =
				MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '+')),
				MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '-')), BNIL));
			BGl_list2515z00zz__bignumz00 =
				MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 'x')), BINT(16L));
			BGl_list2516z00zz__bignumz00 =
				MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 'd')), BINT(10L));
			BGl_list2517z00zz__bignumz00 =
				MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 'o')), BINT(8L));
			BGl_list2518z00zz__bignumz00 =
				MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 'b')), BINT(2L));
			return (BGl_list2514z00zz__bignumz00 =
				MAKE_YOUNG_PAIR(BGl_list2515z00zz__bignumz00,
					MAKE_YOUNG_PAIR(BGl_list2516z00zz__bignumz00,
						MAKE_YOUNG_PAIR(BGl_list2517z00zz__bignumz00,
							MAKE_YOUNG_PAIR(BGl_list2518z00zz__bignumz00, BNIL)))), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__bignumz00(void)
	{
		{	/* Unsafe/bignumber.scm 45 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__bignumz00(void)
	{
		{	/* Unsafe/bignumber.scm 45 */
			{	/* Unsafe/bignumber.scm 406 */
				obj_t BgL_vz00_1283;

				BgL_vz00_1283 = make_vector(33L, BFALSE);
				{
					long BgL_iz00_2789;
					long BgL_nz00_2790;

					BgL_iz00_2789 = 0L;
					BgL_nz00_2790 = -16L;
				BgL_loopz00_2788:
					if ((16L < BgL_nz00_2790))
						{	/* Unsafe/bignumber.scm 408 */
							BGl_preallocatedzd2bignumszd2zz__bignumz00 = BgL_vz00_1283;
						}
					else
						{	/* Unsafe/bignumber.scm 408 */
							VECTOR_SET(BgL_vz00_1283, BgL_iz00_2789,
								BGl_z42z42fixnumzd2ze3bignumzd2freshze3zz__bignumz00
								(BgL_nz00_2790));
							{
								long BgL_nz00_5755;
								long BgL_iz00_5753;

								BgL_iz00_5753 = (BgL_iz00_2789 + 1L);
								BgL_nz00_5755 = (BgL_nz00_2790 + 1L);
								BgL_nz00_2790 = BgL_nz00_5755;
								BgL_iz00_2789 = BgL_iz00_5753;
								goto BgL_loopz00_2788;
							}
						}
				}
			}
			{	/* Unsafe/bignumber.scm 420 */
				obj_t BgL_res2327z00_2809;

				{	/* Unsafe/bignumber.scm 415 */
					bool_t BgL_test2647z00_5757;

					if ((0L < -16L))
						{	/* Unsafe/bignumber.scm 415 */
							BgL_test2647z00_5757 = ((bool_t) 1);
						}
					else
						{	/* Unsafe/bignumber.scm 415 */
							BgL_test2647z00_5757 = (16L < 0L);
						}
					if (BgL_test2647z00_5757)
						{	/* Unsafe/bignumber.scm 415 */
							BgL_res2327z00_2809 =
								BGl_z42z42fixnumzd2ze3bignumzd2freshze3zz__bignumz00(0L);
						}
					else
						{	/* Unsafe/bignumber.scm 415 */
							BgL_res2327z00_2809 =
								VECTOR_REF(BGl_preallocatedzd2bignumszd2zz__bignumz00,
								(0L + 16L));
						}
				}
				return (BGl_bignumzd2za7eroz75zz__bignumz00 =
					BgL_res2327z00_2809, BUNSPEC);
			}
		}

	}



/* $$string->integer-obj */
	BGL_EXPORTED_DEF obj_t bgl_string_to_integer_obj(char *BgL_strz00_3,
		long BgL_radixz00_4)
	{
		{	/* Unsafe/bignumber.scm 194 */
			{	/* Unsafe/bignumber.scm 195 */

				return
					BINT(BGl_stringzd2ze3integerz31zz__r4_numbers_6_5_fixnumz00
					(string_to_bstring(BgL_strz00_3), BgL_radixz00_4, 0L));
			}
		}

	}



/* &$$string->integer-obj */
	obj_t BGl_z62z42z42stringzd2ze3integerzd2objz81zz__bignumz00(obj_t
		BgL_envz00_5503, obj_t BgL_strz00_5504, obj_t BgL_radixz00_5505)
	{
		{	/* Unsafe/bignumber.scm 194 */
			{	/* Unsafe/bignumber.scm 195 */
				long BgL_auxz00_5776;
				char *BgL_auxz00_5767;

				{	/* Unsafe/bignumber.scm 195 */
					obj_t BgL_tmpz00_5777;

					if (INTEGERP(BgL_radixz00_5505))
						{	/* Unsafe/bignumber.scm 195 */
							BgL_tmpz00_5777 = BgL_radixz00_5505;
						}
					else
						{
							obj_t BgL_auxz00_5780;

							BgL_auxz00_5780 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
								BINT(8257L), BGl_string2478z00zz__bignumz00,
								BGl_string2480z00zz__bignumz00, BgL_radixz00_5505);
							FAILURE(BgL_auxz00_5780, BFALSE, BFALSE);
						}
					BgL_auxz00_5776 = (long) CINT(BgL_tmpz00_5777);
				}
				{	/* Unsafe/bignumber.scm 195 */
					obj_t BgL_tmpz00_5768;

					if (STRINGP(BgL_strz00_5504))
						{	/* Unsafe/bignumber.scm 195 */
							BgL_tmpz00_5768 = BgL_strz00_5504;
						}
					else
						{
							obj_t BgL_auxz00_5771;

							BgL_auxz00_5771 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
								BINT(8257L), BGl_string2478z00zz__bignumz00,
								BGl_string2479z00zz__bignumz00, BgL_strz00_5504);
							FAILURE(BgL_auxz00_5771, BFALSE, BFALSE);
						}
					BgL_auxz00_5767 = BSTRING_TO_STRING(BgL_tmpz00_5768);
				}
				return bgl_string_to_integer_obj(BgL_auxz00_5767, BgL_auxz00_5776);
			}
		}

	}



/* bignum-min-elong-div-radix */
	long BGl_bignumzd2minzd2elongzd2divzd2radixz00zz__bignumz00(void)
	{
		{	/* Unsafe/bignumber.scm 236 */
			{	/* Unsafe/bignumber.scm 237 */
				long BgL_arg1197z00_1305;

				BgL_arg1197z00_1305 = (((long) -2) * ((long) (1L) << (int) (30L)));
				return (BgL_arg1197z00_1305 / ((long) 16384));
		}}

	}



/* bignum-min-llong-div-radix */
	BGL_LONGLONG_T BGl_bignumzd2minzd2llongzd2divzd2radixz00zz__bignumz00(void)
	{
		{	/* Unsafe/bignumber.scm 243 */
			{	/* Unsafe/bignumber.scm 244 */
				BGL_LONGLONG_T BgL_arg1200z00_1308;

				BgL_arg1200z00_1308 =
					((-((BGL_LONGLONG_T) 2)) * (LONG_TO_LLONG(1L) << (int) (60L)));
				return (BgL_arg1200z00_1308 / ((BGL_LONGLONG_T) 32768));
			}
		}

	}



/* bignum-min-int64-div-radix */
	int64_t BGl_bignumzd2minzd2int64zd2divzd2radixz00zz__bignumz00(void)
	{
		{	/* Unsafe/bignumber.scm 252 */
			{	/* Unsafe/bignumber.scm 253 */
				int64_t BgL_arg1203z00_1311;

				BgL_arg1203z00_1311 =
					((int64_t) (-2) *
					BGl_expts64z00zz__r4_numbers_6_5_fixnumz00((int64_t) (2),
						(int64_t) (60L)));
				return (BgL_arg1203z00_1311 / (int64_t) (32768));
			}
		}

	}



/* bignum-length */
	long BGl_bignumzd2lengthzd2zz__bignumz00(obj_t BgL_bnz00_6)
	{
		{	/* Unsafe/bignumber.scm 260 */
			{	/* Unsafe/bignumber.scm 261 */
				long BgL_arg1210z00_2843;

				{	/* Unsafe/bignumber.scm 261 */
					obj_t BgL_arg1212z00_2844;

					BgL_arg1212z00_2844 = BGL_BIGNUM_U16VECT(BgL_bnz00_6);
					BgL_arg1210z00_2843 = BGL_HVECTOR_LENGTH(BgL_arg1212z00_2844);
				}
				{	/* Unsafe/bignumber.scm 261 */
					uint16_t BgL_xz00_2846;

					BgL_xz00_2846 = (uint16_t) (BgL_arg1210z00_2843);
					{	/* Unsafe/bignumber.scm 261 */
						long BgL_arg2271z00_2847;

						BgL_arg2271z00_2847 = (long) (BgL_xz00_2846);
						return (long) (BgL_arg2271z00_2847);
		}}}}

	}



/* bignum-sign */
	long BGl_bignumzd2signzd2zz__bignumz00(obj_t BgL_bnz00_7)
	{
		{	/* Unsafe/bignumber.scm 262 */
			{	/* Unsafe/bignumber.scm 263 */
				uint16_t BgL_arg1215z00_2849;

				{	/* Unsafe/bignumber.scm 263 */
					obj_t BgL_arg1216z00_2850;

					BgL_arg1216z00_2850 = BGL_BIGNUM_U16VECT(BgL_bnz00_7);
					BgL_arg1215z00_2849 = BGL_U16VREF(BgL_arg1216z00_2850, 0L);
				}
				{	/* Unsafe/bignumber.scm 263 */
					long BgL_arg2271z00_2852;

					BgL_arg2271z00_2852 = (long) (BgL_arg1215z00_2849);
					return (long) (BgL_arg2271z00_2852);
		}}}

	}



/* bignum-sign-set! */
	obj_t BGl_bignumzd2signzd2setz12z12zz__bignumz00(obj_t BgL_bnz00_8,
		int BgL_signz00_9)
	{
		{	/* Unsafe/bignumber.scm 264 */
			{	/* Unsafe/bignumber.scm 265 */
				obj_t BgL_arg1218z00_2854;
				uint16_t BgL_arg1219z00_2855;

				BgL_arg1218z00_2854 = BGL_BIGNUM_U16VECT(BgL_bnz00_8);
				{	/* Unsafe/bignumber.scm 265 */
					long BgL_tmpz00_5810;

					BgL_tmpz00_5810 = (long) (BgL_signz00_9);
					BgL_arg1219z00_2855 = (uint16_t) (BgL_tmpz00_5810);
				}
				BGL_U16VSET(BgL_arg1218z00_2854, 0L, BgL_arg1219z00_2855);
				return BUNSPEC;
			}
		}

	}



/* bignum-shrink */
	obj_t BGl_bignumzd2shrinkzd2zz__bignumz00(obj_t BgL_xz00_17,
		long BgL_lenz00_18)
	{
		{	/* Unsafe/bignumber.scm 278 */
			{	/* Unsafe/bignumber.scm 279 */
				obj_t BgL_yz00_1326;

				{	/* Unsafe/bignumber.scm 279 */
					int BgL_lenz00_2871;

					BgL_lenz00_2871 = (int) (BgL_lenz00_18);
					{	/* Unsafe/bignumber.scm 259 */
						obj_t BgL_arg1208z00_2872;

						{	/* Unsafe/bignumber.scm 259 */

							BgL_arg1208z00_2872 =
								BGl_makezd2u16vectorzd2zz__srfi4z00(
								(long) (BgL_lenz00_2871), (uint16_t) (0));
						}
						BgL_yz00_1326 = bgl_make_bignum(BgL_arg1208z00_2872);
				}}
				{	/* Unsafe/bignumber.scm 280 */
					long BgL_g1042z00_1327;

					BgL_g1042z00_1327 = (BgL_lenz00_18 - 1L);
					{
						long BgL_iz00_1329;

						BgL_iz00_1329 = BgL_g1042z00_1327;
					BgL_zc3z04anonymousza31226ze3z87_1330:
						if ((BgL_iz00_1329 < 0L))
							{	/* Unsafe/bignumber.scm 281 */
								return BgL_yz00_1326;
							}
						else
							{	/* Unsafe/bignumber.scm 281 */
								{	/* Unsafe/bignumber.scm 284 */
									long BgL_arg1228z00_1332;

									{	/* Unsafe/bignumber.scm 284 */
										int BgL_iz00_2878;

										BgL_iz00_2878 = (int) (BgL_iz00_1329);
										{	/* Unsafe/bignumber.scm 267 */
											uint16_t BgL_arg1220z00_2879;

											{	/* Unsafe/bignumber.scm 267 */
												obj_t BgL_arg1221z00_2880;

												BgL_arg1221z00_2880 = BGL_BIGNUM_U16VECT(BgL_xz00_17);
												{	/* Unsafe/bignumber.scm 267 */
													long BgL_tmpz00_5823;

													BgL_tmpz00_5823 = (long) (BgL_iz00_2878);
													BgL_arg1220z00_2879 =
														BGL_U16VREF(BgL_arg1221z00_2880, BgL_tmpz00_5823);
											}}
											{	/* Unsafe/bignumber.scm 267 */
												long BgL_arg2271z00_2882;

												BgL_arg2271z00_2882 = (long) (BgL_arg1220z00_2879);
												BgL_arg1228z00_1332 = (long) (BgL_arg2271z00_2882);
									}}}
									{	/* Unsafe/bignumber.scm 284 */
										int BgL_iz00_2885;
										int BgL_digitz00_2886;

										BgL_iz00_2885 = (int) (BgL_iz00_1329);
										BgL_digitz00_2886 = (int) (BgL_arg1228z00_1332);
										{	/* Unsafe/bignumber.scm 269 */
											obj_t BgL_arg1223z00_2887;
											uint16_t BgL_arg1225z00_2888;

											BgL_arg1223z00_2887 = BGL_BIGNUM_U16VECT(BgL_yz00_1326);
											{	/* Unsafe/bignumber.scm 269 */
												long BgL_tmpz00_5831;

												BgL_tmpz00_5831 = (long) (BgL_digitz00_2886);
												BgL_arg1225z00_2888 = (uint16_t) (BgL_tmpz00_5831);
											}
											{	/* Unsafe/bignumber.scm 269 */
												long BgL_tmpz00_5834;

												BgL_tmpz00_5834 = (long) (BgL_iz00_2885);
												BGL_U16VSET(BgL_arg1223z00_2887, BgL_tmpz00_5834,
													BgL_arg1225z00_2888);
											} BUNSPEC;
								}}}
								{
									long BgL_iz00_5837;

									BgL_iz00_5837 = (BgL_iz00_1329 - 1L);
									BgL_iz00_1329 = BgL_iz00_5837;
									goto BgL_zc3z04anonymousza31226ze3z87_1330;
								}
							}
					}
				}
			}
		}

	}



/* bignum-remove-leading-zeroes */
	obj_t BGl_bignumzd2removezd2leadingzd2za7eroesz75zz__bignumz00(obj_t
		BgL_xz00_19)
	{
		{	/* Unsafe/bignumber.scm 287 */
			{	/* Unsafe/bignumber.scm 288 */
				long BgL_signz00_1335;

				{	/* Unsafe/bignumber.scm 263 */
					uint16_t BgL_arg1215z00_2892;

					{	/* Unsafe/bignumber.scm 263 */
						obj_t BgL_arg1216z00_2893;

						BgL_arg1216z00_2893 = BGL_BIGNUM_U16VECT(BgL_xz00_19);
						BgL_arg1215z00_2892 = BGL_U16VREF(BgL_arg1216z00_2893, 0L);
					}
					{	/* Unsafe/bignumber.scm 263 */
						long BgL_arg2271z00_2895;

						BgL_arg2271z00_2895 = (long) (BgL_arg1215z00_2892);
						BgL_signz00_1335 = (long) (BgL_arg2271z00_2895);
				}}
				{	/* Unsafe/bignumber.scm 265 */
					obj_t BgL_arg1218z00_2899;
					uint16_t BgL_arg1219z00_2900;

					BgL_arg1218z00_2899 = BGL_BIGNUM_U16VECT(BgL_xz00_19);
					BgL_arg1219z00_2900 = (uint16_t) (1L);
					BGL_U16VSET(BgL_arg1218z00_2899, 0L, BgL_arg1219z00_2900);
					BUNSPEC;
				}
				{	/* Unsafe/bignumber.scm 290 */
					long BgL_g1043z00_1336;

					{	/* Unsafe/bignumber.scm 290 */
						long BgL_arg1252z00_1352;

						{	/* Unsafe/bignumber.scm 261 */
							long BgL_arg1210z00_2902;

							{	/* Unsafe/bignumber.scm 261 */
								obj_t BgL_arg1212z00_2903;

								BgL_arg1212z00_2903 = BGL_BIGNUM_U16VECT(BgL_xz00_19);
								BgL_arg1210z00_2902 = BGL_HVECTOR_LENGTH(BgL_arg1212z00_2903);
							}
							{	/* Unsafe/bignumber.scm 261 */
								uint16_t BgL_xz00_2905;

								BgL_xz00_2905 = (uint16_t) (BgL_arg1210z00_2902);
								{	/* Unsafe/bignumber.scm 261 */
									long BgL_arg2271z00_2906;

									BgL_arg2271z00_2906 = (long) (BgL_xz00_2905);
									BgL_arg1252z00_1352 = (long) (BgL_arg2271z00_2906);
						}}}
						BgL_g1043z00_1336 = (BgL_arg1252z00_1352 - 1L);
					}
					{
						long BgL_iz00_1338;

						BgL_iz00_1338 = BgL_g1043z00_1336;
					BgL_zc3z04anonymousza31230ze3z87_1339:
						{	/* Unsafe/bignumber.scm 291 */
							bool_t BgL_test2652z00_5852;

							{	/* Unsafe/bignumber.scm 291 */
								long BgL_arg1249z00_1350;

								{	/* Unsafe/bignumber.scm 291 */
									int BgL_iz00_2910;

									BgL_iz00_2910 = (int) (BgL_iz00_1338);
									{	/* Unsafe/bignumber.scm 267 */
										uint16_t BgL_arg1220z00_2911;

										{	/* Unsafe/bignumber.scm 267 */
											obj_t BgL_arg1221z00_2912;

											BgL_arg1221z00_2912 = BGL_BIGNUM_U16VECT(BgL_xz00_19);
											{	/* Unsafe/bignumber.scm 267 */
												long BgL_tmpz00_5855;

												BgL_tmpz00_5855 = (long) (BgL_iz00_2910);
												BgL_arg1220z00_2911 =
													BGL_U16VREF(BgL_arg1221z00_2912, BgL_tmpz00_5855);
										}}
										{	/* Unsafe/bignumber.scm 267 */
											long BgL_arg2271z00_2914;

											BgL_arg2271z00_2914 = (long) (BgL_arg1220z00_2911);
											BgL_arg1249z00_1350 = (long) (BgL_arg2271z00_2914);
								}}}
								BgL_test2652z00_5852 = (BgL_arg1249z00_1350 == 0L);
							}
							if (BgL_test2652z00_5852)
								{
									long BgL_iz00_5861;

									BgL_iz00_5861 = (BgL_iz00_1338 - 1L);
									BgL_iz00_1338 = BgL_iz00_5861;
									goto BgL_zc3z04anonymousza31230ze3z87_1339;
								}
							else
								{	/* Unsafe/bignumber.scm 291 */
									if ((BgL_iz00_1338 == 0L))
										{	/* Unsafe/bignumber.scm 293 */
											return BGl_bignumzd2za7eroz75zz__bignumz00;
										}
									else
										{	/* Unsafe/bignumber.scm 293 */
											{	/* Unsafe/bignumber.scm 296 */
												int BgL_signz00_2920;

												BgL_signz00_2920 = (int) (BgL_signz00_1335);
												{	/* Unsafe/bignumber.scm 265 */
													obj_t BgL_arg1218z00_2921;
													uint16_t BgL_arg1219z00_2922;

													BgL_arg1218z00_2921 = BGL_BIGNUM_U16VECT(BgL_xz00_19);
													{	/* Unsafe/bignumber.scm 265 */
														long BgL_tmpz00_5867;

														BgL_tmpz00_5867 = (long) (BgL_signz00_2920);
														BgL_arg1219z00_2922 = (uint16_t) (BgL_tmpz00_5867);
													}
													BGL_U16VSET(BgL_arg1218z00_2921, 0L,
														BgL_arg1219z00_2922);
													BUNSPEC;
											}}
											{	/* Unsafe/bignumber.scm 297 */
												bool_t BgL_test2654z00_5871;

												{	/* Unsafe/bignumber.scm 297 */
													long BgL_arg1244z00_1348;

													{	/* Unsafe/bignumber.scm 297 */
														long BgL_arg1248z00_1349;

														{	/* Unsafe/bignumber.scm 261 */
															long BgL_arg1210z00_2925;

															{	/* Unsafe/bignumber.scm 261 */
																obj_t BgL_arg1212z00_2926;

																BgL_arg1212z00_2926 =
																	BGL_BIGNUM_U16VECT(BgL_xz00_19);
																BgL_arg1210z00_2925 =
																	BGL_HVECTOR_LENGTH(BgL_arg1212z00_2926);
															}
															{	/* Unsafe/bignumber.scm 261 */
																uint16_t BgL_xz00_2928;

																BgL_xz00_2928 =
																	(uint16_t) (BgL_arg1210z00_2925);
																{	/* Unsafe/bignumber.scm 261 */
																	long BgL_arg2271z00_2929;

																	BgL_arg2271z00_2929 = (long) (BgL_xz00_2928);
																	BgL_arg1248z00_1349 =
																		(long) (BgL_arg2271z00_2929);
														}}}
														BgL_arg1244z00_1348 = (BgL_arg1248z00_1349 - 1L);
													}
													BgL_test2654z00_5871 =
														(BgL_iz00_1338 == BgL_arg1244z00_1348);
												}
												if (BgL_test2654z00_5871)
													{	/* Unsafe/bignumber.scm 297 */
														return BgL_xz00_19;
													}
												else
													{	/* Unsafe/bignumber.scm 297 */
														return
															BGl_bignumzd2shrinkzd2zz__bignumz00(BgL_xz00_19,
															(BgL_iz00_1338 + 1L));
													}
											}
										}
								}
						}
					}
				}
			}
		}

	}



/* $$fixnum->bignum-fresh */
	obj_t BGl_z42z42fixnumzd2ze3bignumzd2freshze3zz__bignumz00(long BgL_nz00_21)
	{
		{	/* Unsafe/bignumber.scm 305 */
			{	/* Unsafe/bignumber.scm 306 */
				long BgL_negzd2nzd2_1353;

				if ((BgL_nz00_21 < 0L))
					{	/* Unsafe/bignumber.scm 306 */
						BgL_negzd2nzd2_1353 = BgL_nz00_21;
					}
				else
					{	/* Unsafe/bignumber.scm 306 */
						BgL_negzd2nzd2_1353 = (0L - BgL_nz00_21);
					}
				{
					long BgL_nbzd2digitszd2_1355;
					long BgL_xz00_1356;

					BgL_nbzd2digitszd2_1355 = 0L;
					BgL_xz00_1356 = BgL_negzd2nzd2_1353;
				BgL_zc3z04anonymousza31253ze3z87_1357:
					if ((BgL_xz00_1356 == 0L))
						{	/* Unsafe/bignumber.scm 311 */
							obj_t BgL_rz00_1359;

							{	/* Unsafe/bignumber.scm 311 */
								long BgL_arg1307z00_1373;

								BgL_arg1307z00_1373 = (BgL_nbzd2digitszd2_1355 + 1L);
								{	/* Unsafe/bignumber.scm 311 */
									int BgL_lenz00_2939;

									BgL_lenz00_2939 = (int) (BgL_arg1307z00_1373);
									{	/* Unsafe/bignumber.scm 259 */
										obj_t BgL_arg1208z00_2940;

										{	/* Unsafe/bignumber.scm 259 */

											BgL_arg1208z00_2940 =
												BGl_makezd2u16vectorzd2zz__srfi4z00(
												(long) (BgL_lenz00_2939), (uint16_t) (0));
										}
										BgL_rz00_1359 = bgl_make_bignum(BgL_arg1208z00_2940);
							}}}
							if ((BgL_nz00_21 < 0L))
								{	/* Unsafe/bignumber.scm 265 */
									obj_t BgL_arg1218z00_2946;
									uint16_t BgL_arg1219z00_2947;

									BgL_arg1218z00_2946 = BGL_BIGNUM_U16VECT(BgL_rz00_1359);
									BgL_arg1219z00_2947 = (uint16_t) (0L);
									BGL_U16VSET(BgL_arg1218z00_2946, 0L, BgL_arg1219z00_2947);
									BUNSPEC;
								}
							else
								{	/* Unsafe/bignumber.scm 265 */
									obj_t BgL_arg1218z00_2950;
									uint16_t BgL_arg1219z00_2951;

									BgL_arg1218z00_2950 = BGL_BIGNUM_U16VECT(BgL_rz00_1359);
									BgL_arg1219z00_2951 = (uint16_t) (1L);
									BGL_U16VSET(BgL_arg1218z00_2950, 0L, BgL_arg1219z00_2951);
									BUNSPEC;
								}
							{
								long BgL_iz00_2977;
								long BgL_xz00_2978;

								BgL_iz00_2977 = 1L;
								BgL_xz00_2978 = BgL_negzd2nzd2_1353;
							BgL_loop2z00_2976:
								if ((BgL_xz00_2978 == 0L))
									{	/* Unsafe/bignumber.scm 316 */
										return BgL_rz00_1359;
									}
								else
									{	/* Unsafe/bignumber.scm 316 */
										{	/* Unsafe/bignumber.scm 321 */
											long BgL_arg1268z00_2981;

											{	/* Unsafe/bignumber.scm 321 */
												long BgL_arg1272z00_2982;

												{	/* Unsafe/bignumber.scm 321 */
													long BgL_n1z00_2984;
													long BgL_n2z00_2985;

													BgL_n1z00_2984 = BgL_xz00_2978;
													BgL_n2z00_2985 = 16384L;
													{	/* Unsafe/bignumber.scm 321 */
														bool_t BgL_test2659z00_5901;

														{	/* Unsafe/bignumber.scm 321 */
															long BgL_arg2169z00_2987;

															BgL_arg2169z00_2987 =
																(((BgL_n1z00_2984) | (BgL_n2z00_2985)) &
																-2147483648);
															BgL_test2659z00_5901 =
																(BgL_arg2169z00_2987 == 0L);
														}
														if (BgL_test2659z00_5901)
															{	/* Unsafe/bignumber.scm 321 */
																int32_t BgL_arg2166z00_2989;

																{	/* Unsafe/bignumber.scm 321 */
																	int32_t BgL_arg2167z00_2990;
																	int32_t BgL_arg2168z00_2991;

																	BgL_arg2167z00_2990 =
																		(int32_t) (BgL_n1z00_2984);
																	BgL_arg2168z00_2991 =
																		(int32_t) (BgL_n2z00_2985);
																	BgL_arg2166z00_2989 =
																		(BgL_arg2167z00_2990 % BgL_arg2168z00_2991);
																}
																{	/* Unsafe/bignumber.scm 321 */
																	long BgL_arg2270z00_2995;

																	BgL_arg2270z00_2995 =
																		(long) (BgL_arg2166z00_2989);
																	BgL_arg1272z00_2982 =
																		(long) (BgL_arg2270z00_2995);
															}}
														else
															{	/* Unsafe/bignumber.scm 321 */
																BgL_arg1272z00_2982 =
																	(BgL_n1z00_2984 % BgL_n2z00_2985);
															}
													}
												}
												BgL_arg1268z00_2981 = (0L - BgL_arg1272z00_2982);
											}
											{	/* Unsafe/bignumber.scm 318 */
												int BgL_iz00_2999;
												int BgL_digitz00_3000;

												BgL_iz00_2999 = (int) (BgL_iz00_2977);
												BgL_digitz00_3000 = (int) (BgL_arg1268z00_2981);
												{	/* Unsafe/bignumber.scm 269 */
													obj_t BgL_arg1223z00_3001;
													uint16_t BgL_arg1225z00_3002;

													BgL_arg1223z00_3001 =
														BGL_BIGNUM_U16VECT(BgL_rz00_1359);
													{	/* Unsafe/bignumber.scm 269 */
														long BgL_tmpz00_5914;

														BgL_tmpz00_5914 = (long) (BgL_digitz00_3000);
														BgL_arg1225z00_3002 = (uint16_t) (BgL_tmpz00_5914);
													}
													{	/* Unsafe/bignumber.scm 269 */
														long BgL_tmpz00_5917;

														BgL_tmpz00_5917 = (long) (BgL_iz00_2999);
														BGL_U16VSET(BgL_arg1223z00_3001, BgL_tmpz00_5917,
															BgL_arg1225z00_3002);
													} BUNSPEC;
										}}}
										{	/* Unsafe/bignumber.scm 322 */
											long BgL_arg1304z00_3004;
											long BgL_arg1305z00_3005;

											BgL_arg1304z00_3004 = (BgL_iz00_2977 + 1L);
											BgL_arg1305z00_3005 = (BgL_xz00_2978 / 16384L);
											{
												long BgL_xz00_5923;
												long BgL_iz00_5922;

												BgL_iz00_5922 = BgL_arg1304z00_3004;
												BgL_xz00_5923 = BgL_arg1305z00_3005;
												BgL_xz00_2978 = BgL_xz00_5923;
												BgL_iz00_2977 = BgL_iz00_5922;
												goto BgL_loop2z00_2976;
											}
										}
									}
							}
						}
					else
						{	/* Unsafe/bignumber.scm 310 */
							long BgL_arg1308z00_1374;
							long BgL_arg1309z00_1375;

							BgL_arg1308z00_1374 = (BgL_nbzd2digitszd2_1355 + 1L);
							BgL_arg1309z00_1375 = (BgL_xz00_1356 / 16384L);
							{
								long BgL_xz00_5927;
								long BgL_nbzd2digitszd2_5926;

								BgL_nbzd2digitszd2_5926 = BgL_arg1308z00_1374;
								BgL_xz00_5927 = BgL_arg1309z00_1375;
								BgL_xz00_1356 = BgL_xz00_5927;
								BgL_nbzd2digitszd2_1355 = BgL_nbzd2digitszd2_5926;
								goto BgL_zc3z04anonymousza31253ze3z87_1357;
							}
						}
				}
			}
		}

	}



/* $$elong->bignum */
	BGL_EXPORTED_DEF obj_t bgl_elong_to_bignum(long BgL_nz00_22)
	{
		{	/* Unsafe/bignumber.scm 325 */
			{	/* Unsafe/bignumber.scm 326 */
				long BgL_negzd2nzd2_1379;

				if ((BgL_nz00_22 < ((long) 0)))
					{	/* Unsafe/bignumber.scm 326 */
						BgL_negzd2nzd2_1379 = BgL_nz00_22;
					}
				else
					{	/* Unsafe/bignumber.scm 326 */
						long BgL_res2328z00_3017;

						{	/* Unsafe/bignumber.scm 326 */
							long BgL_tmpz00_5930;

							BgL_tmpz00_5930 = (((long) 0) - BgL_nz00_22);
							BgL_res2328z00_3017 = (long) (BgL_tmpz00_5930);
						}
						BgL_negzd2nzd2_1379 = BgL_res2328z00_3017;
					}
				{
					long BgL_nbzd2digitszd2_1381;
					long BgL_xz00_1382;

					BgL_nbzd2digitszd2_1381 = 0L;
					BgL_xz00_1382 = BgL_negzd2nzd2_1379;
				BgL_zc3z04anonymousza31312ze3z87_1383:
					if ((BgL_xz00_1382 == (long) (0L)))
						{	/* Unsafe/bignumber.scm 331 */
							obj_t BgL_rz00_1385;

							{	/* Unsafe/bignumber.scm 331 */
								long BgL_arg1326z00_1401;

								BgL_arg1326z00_1401 = (BgL_nbzd2digitszd2_1381 + 1L);
								{	/* Unsafe/bignumber.scm 331 */
									int BgL_lenz00_3021;

									BgL_lenz00_3021 = (int) (BgL_arg1326z00_1401);
									{	/* Unsafe/bignumber.scm 259 */
										obj_t BgL_arg1208z00_3022;

										{	/* Unsafe/bignumber.scm 259 */

											BgL_arg1208z00_3022 =
												BGl_makezd2u16vectorzd2zz__srfi4z00(
												(long) (BgL_lenz00_3021), (uint16_t) (0));
										}
										BgL_rz00_1385 = bgl_make_bignum(BgL_arg1208z00_3022);
							}}}
							if ((BgL_nz00_22 < ((long) 0)))
								{	/* Unsafe/bignumber.scm 265 */
									obj_t BgL_arg1218z00_3029;
									uint16_t BgL_arg1219z00_3030;

									BgL_arg1218z00_3029 = BGL_BIGNUM_U16VECT(BgL_rz00_1385);
									BgL_arg1219z00_3030 = (uint16_t) (0L);
									BGL_U16VSET(BgL_arg1218z00_3029, 0L, BgL_arg1219z00_3030);
									BUNSPEC;
								}
							else
								{	/* Unsafe/bignumber.scm 265 */
									obj_t BgL_arg1218z00_3033;
									uint16_t BgL_arg1219z00_3034;

									BgL_arg1218z00_3033 = BGL_BIGNUM_U16VECT(BgL_rz00_1385);
									BgL_arg1219z00_3034 = (uint16_t) (1L);
									BGL_U16VSET(BgL_arg1218z00_3033, 0L, BgL_arg1219z00_3034);
									BUNSPEC;
								}
							{
								int BgL_iz00_3050;
								long BgL_xz00_3051;

								BgL_iz00_3050 = (int) (1L);
								BgL_xz00_3051 = BgL_negzd2nzd2_1379;
							BgL_loop2z00_3049:
								if ((BgL_xz00_3051 == ((long) 0)))
									{	/* Unsafe/bignumber.scm 336 */
										return BgL_rz00_1385;
									}
								else
									{	/* Unsafe/bignumber.scm 336 */
										{	/* Unsafe/bignumber.scm 341 */
											long BgL_arg1317z00_3055;

											{	/* Unsafe/bignumber.scm 341 */
												long BgL_arg1318z00_3056;

												{	/* Unsafe/bignumber.scm 341 */
													long BgL_arg1319z00_3057;

													{	/* Unsafe/bignumber.scm 341 */
														long BgL_arg1320z00_3058;

														BgL_arg1320z00_3058 = (long) (16384L);
														BgL_arg1319z00_3057 =
															(BgL_xz00_3051 % BgL_arg1320z00_3058);
													}
													BgL_arg1318z00_3056 = (long) (BgL_arg1319z00_3057);
												}
												BgL_arg1317z00_3055 = (0L - BgL_arg1318z00_3056);
											}
											{	/* Unsafe/bignumber.scm 338 */
												int BgL_digitz00_3065;

												BgL_digitz00_3065 = (int) (BgL_arg1317z00_3055);
												{	/* Unsafe/bignumber.scm 269 */
													obj_t BgL_arg1223z00_3066;
													uint16_t BgL_arg1225z00_3067;

													BgL_arg1223z00_3066 =
														BGL_BIGNUM_U16VECT(BgL_rz00_1385);
													{	/* Unsafe/bignumber.scm 269 */
														long BgL_tmpz00_5957;

														BgL_tmpz00_5957 = (long) (BgL_digitz00_3065);
														BgL_arg1225z00_3067 = (uint16_t) (BgL_tmpz00_5957);
													}
													{	/* Unsafe/bignumber.scm 269 */
														long BgL_tmpz00_5960;

														BgL_tmpz00_5960 = (long) (BgL_iz00_3050);
														BGL_U16VSET(BgL_arg1223z00_3066, BgL_tmpz00_5960,
															BgL_arg1225z00_3067);
													} BUNSPEC;
										}}}
										{	/* Unsafe/bignumber.scm 342 */
											long BgL_arg1322z00_3069;
											long BgL_arg1323z00_3070;

											BgL_arg1322z00_3069 = ((long) (BgL_iz00_3050) + 1L);
											{	/* Unsafe/bignumber.scm 342 */
												long BgL_n2z00_3074;

												BgL_n2z00_3074 = (long) (16384L);
												BgL_arg1323z00_3070 = (BgL_xz00_3051 / BgL_n2z00_3074);
											}
											{
												long BgL_xz00_5969;
												int BgL_iz00_5967;

												BgL_iz00_5967 = (int) (BgL_arg1322z00_3069);
												BgL_xz00_5969 = BgL_arg1323z00_3070;
												BgL_xz00_3051 = BgL_xz00_5969;
												BgL_iz00_3050 = BgL_iz00_5967;
												goto BgL_loop2z00_3049;
											}
										}
									}
							}
						}
					else
						{	/* Unsafe/bignumber.scm 330 */
							long BgL_arg1327z00_1402;
							long BgL_arg1328z00_1403;

							BgL_arg1327z00_1402 = (BgL_nbzd2digitszd2_1381 + 1L);
							{	/* Unsafe/bignumber.scm 330 */
								long BgL_arg1329z00_1404;

								BgL_arg1329z00_1404 = ((long) (1L) << (int) (15L));
								BgL_arg1328z00_1403 = (BgL_xz00_1382 / BgL_arg1329z00_1404);
							}
							{
								long BgL_xz00_5977;
								long BgL_nbzd2digitszd2_5976;

								BgL_nbzd2digitszd2_5976 = BgL_arg1327z00_1402;
								BgL_xz00_5977 = BgL_arg1328z00_1403;
								BgL_xz00_1382 = BgL_xz00_5977;
								BgL_nbzd2digitszd2_1381 = BgL_nbzd2digitszd2_5976;
								goto BgL_zc3z04anonymousza31312ze3z87_1383;
							}
						}
				}
			}
		}

	}



/* &$$elong->bignum */
	obj_t BGl_z62z42z42elongzd2ze3bignumz53zz__bignumz00(obj_t BgL_envz00_5506,
		obj_t BgL_nz00_5507)
	{
		{	/* Unsafe/bignumber.scm 325 */
			{	/* Unsafe/bignumber.scm 326 */
				long BgL_auxz00_5978;

				{	/* Unsafe/bignumber.scm 326 */
					obj_t BgL_tmpz00_5979;

					if (ELONGP(BgL_nz00_5507))
						{	/* Unsafe/bignumber.scm 326 */
							BgL_tmpz00_5979 = BgL_nz00_5507;
						}
					else
						{
							obj_t BgL_auxz00_5982;

							BgL_auxz00_5982 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
								BINT(12664L), BGl_string2481z00zz__bignumz00,
								BGl_string2482z00zz__bignumz00, BgL_nz00_5507);
							FAILURE(BgL_auxz00_5982, BFALSE, BFALSE);
						}
					BgL_auxz00_5978 = BELONG_TO_LONG(BgL_tmpz00_5979);
				}
				return bgl_elong_to_bignum(BgL_auxz00_5978);
			}
		}

	}



/* $$llong->bignum */
	BGL_EXPORTED_DEF obj_t bgl_llong_to_bignum(BGL_LONGLONG_T BgL_nz00_23)
	{
		{	/* Unsafe/bignumber.scm 345 */
			{	/* Unsafe/bignumber.scm 346 */
				BGL_LONGLONG_T BgL_negzd2nzd2_1407;

				if ((BgL_nz00_23 < ((BGL_LONGLONG_T) 0)))
					{	/* Unsafe/bignumber.scm 346 */
						BgL_negzd2nzd2_1407 = BgL_nz00_23;
					}
				else
					{	/* Unsafe/bignumber.scm 346 */
						BgL_negzd2nzd2_1407 = (((BGL_LONGLONG_T) 0) - BgL_nz00_23);
					}
				{
					long BgL_nbzd2digitszd2_1409;
					BGL_LONGLONG_T BgL_xz00_1410;

					BgL_nbzd2digitszd2_1409 = 0L;
					BgL_xz00_1410 = BgL_negzd2nzd2_1407;
				BgL_zc3z04anonymousza31331ze3z87_1411:
					if ((BgL_xz00_1410 == LONG_TO_LLONG(0L)))
						{	/* Unsafe/bignumber.scm 351 */
							obj_t BgL_rz00_1413;

							{	/* Unsafe/bignumber.scm 351 */
								long BgL_arg1344z00_1429;

								BgL_arg1344z00_1429 = (BgL_nbzd2digitszd2_1409 + 1L);
								{	/* Unsafe/bignumber.scm 351 */
									int BgL_lenz00_3086;

									BgL_lenz00_3086 = (int) (BgL_arg1344z00_1429);
									{	/* Unsafe/bignumber.scm 259 */
										obj_t BgL_arg1208z00_3087;

										{	/* Unsafe/bignumber.scm 259 */

											BgL_arg1208z00_3087 =
												BGl_makezd2u16vectorzd2zz__srfi4z00(
												(long) (BgL_lenz00_3086), (uint16_t) (0));
										}
										BgL_rz00_1413 = bgl_make_bignum(BgL_arg1208z00_3087);
							}}}
							if ((BgL_nz00_23 < ((BGL_LONGLONG_T) 0)))
								{	/* Unsafe/bignumber.scm 265 */
									obj_t BgL_arg1218z00_3094;
									uint16_t BgL_arg1219z00_3095;

									BgL_arg1218z00_3094 = BGL_BIGNUM_U16VECT(BgL_rz00_1413);
									BgL_arg1219z00_3095 = (uint16_t) (0L);
									BGL_U16VSET(BgL_arg1218z00_3094, 0L, BgL_arg1219z00_3095);
									BUNSPEC;
								}
							else
								{	/* Unsafe/bignumber.scm 265 */
									obj_t BgL_arg1218z00_3098;
									uint16_t BgL_arg1219z00_3099;

									BgL_arg1218z00_3098 = BGL_BIGNUM_U16VECT(BgL_rz00_1413);
									BgL_arg1219z00_3099 = (uint16_t) (1L);
									BGL_U16VSET(BgL_arg1218z00_3098, 0L, BgL_arg1219z00_3099);
									BUNSPEC;
								}
							{
								long BgL_iz00_3115;
								BGL_LONGLONG_T BgL_xz00_3116;

								BgL_iz00_3115 = 1L;
								BgL_xz00_3116 = BgL_negzd2nzd2_1407;
							BgL_loop2z00_3114:
								if ((BgL_xz00_3116 == ((BGL_LONGLONG_T) 0)))
									{	/* Unsafe/bignumber.scm 356 */
										return BgL_rz00_1413;
									}
								else
									{	/* Unsafe/bignumber.scm 356 */
										{	/* Unsafe/bignumber.scm 361 */
											long BgL_arg1336z00_3120;

											{	/* Unsafe/bignumber.scm 361 */
												long BgL_arg1337z00_3121;

												{	/* Unsafe/bignumber.scm 361 */
													BGL_LONGLONG_T BgL_arg1338z00_3122;

													{	/* Unsafe/bignumber.scm 361 */
														BGL_LONGLONG_T BgL_arg1339z00_3123;

														BgL_arg1339z00_3123 = LONG_TO_LLONG(16384L);
														BgL_arg1338z00_3122 =
															(BgL_xz00_3116 % BgL_arg1339z00_3123);
													}
													BgL_arg1337z00_3121 =
														LLONG_TO_LONG(BgL_arg1338z00_3122);
												}
												BgL_arg1336z00_3120 = (0L - BgL_arg1337z00_3121);
											}
											{	/* Unsafe/bignumber.scm 358 */
												int BgL_iz00_3129;
												int BgL_digitz00_3130;

												BgL_iz00_3129 = (int) (BgL_iz00_3115);
												BgL_digitz00_3130 = (int) (BgL_arg1336z00_3120);
												{	/* Unsafe/bignumber.scm 269 */
													obj_t BgL_arg1223z00_3131;
													uint16_t BgL_arg1225z00_3132;

													BgL_arg1223z00_3131 =
														BGL_BIGNUM_U16VECT(BgL_rz00_1413);
													{	/* Unsafe/bignumber.scm 269 */
														long BgL_tmpz00_6016;

														BgL_tmpz00_6016 = (long) (BgL_digitz00_3130);
														BgL_arg1225z00_3132 = (uint16_t) (BgL_tmpz00_6016);
													}
													{	/* Unsafe/bignumber.scm 269 */
														long BgL_tmpz00_6019;

														BgL_tmpz00_6019 = (long) (BgL_iz00_3129);
														BGL_U16VSET(BgL_arg1223z00_3131, BgL_tmpz00_6019,
															BgL_arg1225z00_3132);
													} BUNSPEC;
										}}}
										{	/* Unsafe/bignumber.scm 362 */
											long BgL_arg1341z00_3134;
											BGL_LONGLONG_T BgL_arg1342z00_3135;

											BgL_arg1341z00_3134 = (BgL_iz00_3115 + 1L);
											{	/* Unsafe/bignumber.scm 362 */
												BGL_LONGLONG_T BgL_n2z00_3139;

												BgL_n2z00_3139 = LONG_TO_LLONG(16384L);
												BgL_arg1342z00_3135 = (BgL_xz00_3116 / BgL_n2z00_3139);
											}
											{
												BGL_LONGLONG_T BgL_xz00_6026;
												long BgL_iz00_6025;

												BgL_iz00_6025 = BgL_arg1341z00_3134;
												BgL_xz00_6026 = BgL_arg1342z00_3135;
												BgL_xz00_3116 = BgL_xz00_6026;
												BgL_iz00_3115 = BgL_iz00_6025;
												goto BgL_loop2z00_3114;
											}
										}
									}
							}
						}
					else
						{	/* Unsafe/bignumber.scm 350 */
							long BgL_arg1346z00_1430;
							BGL_LONGLONG_T BgL_arg1347z00_1431;

							BgL_arg1346z00_1430 = (BgL_nbzd2digitszd2_1409 + 1L);
							{	/* Unsafe/bignumber.scm 350 */
								BGL_LONGLONG_T BgL_arg1348z00_1432;

								BgL_arg1348z00_1432 = (LONG_TO_LLONG(1L) << (int) (30L));
								BgL_arg1347z00_1431 = (BgL_xz00_1410 / BgL_arg1348z00_1432);
							}
							{
								BGL_LONGLONG_T BgL_xz00_6033;
								long BgL_nbzd2digitszd2_6032;

								BgL_nbzd2digitszd2_6032 = BgL_arg1346z00_1430;
								BgL_xz00_6033 = BgL_arg1347z00_1431;
								BgL_xz00_1410 = BgL_xz00_6033;
								BgL_nbzd2digitszd2_1409 = BgL_nbzd2digitszd2_6032;
								goto BgL_zc3z04anonymousza31331ze3z87_1411;
							}
						}
				}
			}
		}

	}



/* &$$llong->bignum */
	obj_t BGl_z62z42z42llongzd2ze3bignumz53zz__bignumz00(obj_t BgL_envz00_5508,
		obj_t BgL_nz00_5509)
	{
		{	/* Unsafe/bignumber.scm 345 */
			{	/* Unsafe/bignumber.scm 346 */
				BGL_LONGLONG_T BgL_auxz00_6034;

				{	/* Unsafe/bignumber.scm 346 */
					obj_t BgL_tmpz00_6035;

					if (LLONGP(BgL_nz00_5509))
						{	/* Unsafe/bignumber.scm 346 */
							BgL_tmpz00_6035 = BgL_nz00_5509;
						}
					else
						{
							obj_t BgL_auxz00_6038;

							BgL_auxz00_6038 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
								BINT(13354L), BGl_string2483z00zz__bignumz00,
								BGl_string2484z00zz__bignumz00, BgL_nz00_5509);
							FAILURE(BgL_auxz00_6038, BFALSE, BFALSE);
						}
					BgL_auxz00_6034 = BLLONG_TO_LLONG(BgL_tmpz00_6035);
				}
				return bgl_llong_to_bignum(BgL_auxz00_6034);
			}
		}

	}



/* $$int64->bignum */
	BGL_EXPORTED_DEF obj_t bgl_int64_to_bignum(int64_t BgL_nz00_24)
	{
		{	/* Unsafe/bignumber.scm 365 */
			{	/* Unsafe/bignumber.scm 366 */
				int64_t BgL_negzd2nzd2_1435;

				if ((BgL_nz00_24 < (int64_t) (0)))
					{	/* Unsafe/bignumber.scm 366 */
						BgL_negzd2nzd2_1435 = BgL_nz00_24;
					}
				else
					{	/* Unsafe/bignumber.scm 366 */
						BgL_negzd2nzd2_1435 = ((int64_t) (0) - BgL_nz00_24);
					}
				{
					long BgL_nbzd2digitszd2_1437;
					int64_t BgL_xz00_1438;

					BgL_nbzd2digitszd2_1437 = 0L;
					BgL_xz00_1438 = BgL_negzd2nzd2_1435;
				BgL_zc3z04anonymousza31350ze3z87_1439:
					if ((BgL_xz00_1438 == (int64_t) (0)))
						{	/* Unsafe/bignumber.scm 371 */
							obj_t BgL_rz00_1441;

							{	/* Unsafe/bignumber.scm 371 */
								long BgL_arg1364z00_1457;

								BgL_arg1364z00_1457 = (BgL_nbzd2digitszd2_1437 + 1L);
								{	/* Unsafe/bignumber.scm 371 */
									int BgL_lenz00_3151;

									BgL_lenz00_3151 = (int) (BgL_arg1364z00_1457);
									{	/* Unsafe/bignumber.scm 259 */
										obj_t BgL_arg1208z00_3152;

										{	/* Unsafe/bignumber.scm 259 */

											BgL_arg1208z00_3152 =
												BGl_makezd2u16vectorzd2zz__srfi4z00(
												(long) (BgL_lenz00_3151), (uint16_t) (0));
										}
										BgL_rz00_1441 = bgl_make_bignum(BgL_arg1208z00_3152);
							}}}
							if ((BgL_nz00_24 < (int64_t) (0)))
								{	/* Unsafe/bignumber.scm 265 */
									obj_t BgL_arg1218z00_3159;
									uint16_t BgL_arg1219z00_3160;

									BgL_arg1218z00_3159 = BGL_BIGNUM_U16VECT(BgL_rz00_1441);
									BgL_arg1219z00_3160 = (uint16_t) (0L);
									BGL_U16VSET(BgL_arg1218z00_3159, 0L, BgL_arg1219z00_3160);
									BUNSPEC;
								}
							else
								{	/* Unsafe/bignumber.scm 265 */
									obj_t BgL_arg1218z00_3163;
									uint16_t BgL_arg1219z00_3164;

									BgL_arg1218z00_3163 = BGL_BIGNUM_U16VECT(BgL_rz00_1441);
									BgL_arg1219z00_3164 = (uint16_t) (1L);
									BGL_U16VSET(BgL_arg1218z00_3163, 0L, BgL_arg1219z00_3164);
									BUNSPEC;
								}
							{
								long BgL_iz00_3184;
								int64_t BgL_xz00_3185;

								BgL_iz00_3184 = 1L;
								BgL_xz00_3185 = BgL_negzd2nzd2_1435;
							BgL_loop2z00_3183:
								if ((BgL_xz00_3185 == (int64_t) (0)))
									{	/* Unsafe/bignumber.scm 376 */
										return BgL_rz00_1441;
									}
								else
									{	/* Unsafe/bignumber.scm 376 */
										{	/* Unsafe/bignumber.scm 381 */
											long BgL_arg1356z00_3189;

											{	/* Unsafe/bignumber.scm 381 */
												long BgL_arg1357z00_3190;

												{	/* Unsafe/bignumber.scm 381 */
													int64_t BgL_arg1358z00_3191;

													{	/* Unsafe/bignumber.scm 381 */
														int64_t BgL_arg1359z00_3192;

														BgL_arg1359z00_3192 = (int64_t) (16384L);
														BgL_arg1358z00_3191 =
															(BgL_xz00_3185 % BgL_arg1359z00_3192);
													}
													{	/* Unsafe/bignumber.scm 381 */
														long BgL_arg2269z00_3198;

														BgL_arg2269z00_3198 = (long) (BgL_arg1358z00_3191);
														BgL_arg1357z00_3190 = (long) (BgL_arg2269z00_3198);
												}}
												BgL_arg1356z00_3189 = (0L - BgL_arg1357z00_3190);
											}
											{	/* Unsafe/bignumber.scm 378 */
												int BgL_iz00_3202;
												int BgL_digitz00_3203;

												BgL_iz00_3202 = (int) (BgL_iz00_3184);
												BgL_digitz00_3203 = (int) (BgL_arg1356z00_3189);
												{	/* Unsafe/bignumber.scm 269 */
													obj_t BgL_arg1223z00_3204;
													uint16_t BgL_arg1225z00_3205;

													BgL_arg1223z00_3204 =
														BGL_BIGNUM_U16VECT(BgL_rz00_1441);
													{	/* Unsafe/bignumber.scm 269 */
														long BgL_tmpz00_6072;

														BgL_tmpz00_6072 = (long) (BgL_digitz00_3203);
														BgL_arg1225z00_3205 = (uint16_t) (BgL_tmpz00_6072);
													}
													{	/* Unsafe/bignumber.scm 269 */
														long BgL_tmpz00_6075;

														BgL_tmpz00_6075 = (long) (BgL_iz00_3202);
														BGL_U16VSET(BgL_arg1223z00_3204, BgL_tmpz00_6075,
															BgL_arg1225z00_3205);
													} BUNSPEC;
										}}}
										{	/* Unsafe/bignumber.scm 382 */
											long BgL_arg1361z00_3207;
											int64_t BgL_arg1362z00_3208;

											BgL_arg1361z00_3207 = (BgL_iz00_3184 + 1L);
											{	/* Unsafe/bignumber.scm 382 */
												int64_t BgL_n2z00_3212;

												BgL_n2z00_3212 = (int64_t) (16384L);
												BgL_arg1362z00_3208 = (BgL_xz00_3185 / BgL_n2z00_3212);
											}
											{
												int64_t BgL_xz00_6082;
												long BgL_iz00_6081;

												BgL_iz00_6081 = BgL_arg1361z00_3207;
												BgL_xz00_6082 = BgL_arg1362z00_3208;
												BgL_xz00_3185 = BgL_xz00_6082;
												BgL_iz00_3184 = BgL_iz00_6081;
												goto BgL_loop2z00_3183;
											}
										}
									}
							}
						}
					else
						{	/* Unsafe/bignumber.scm 370 */
							long BgL_arg1365z00_1458;
							int64_t BgL_arg1366z00_1459;

							BgL_arg1365z00_1458 = (BgL_nbzd2digitszd2_1437 + 1L);
							{	/* Unsafe/bignumber.scm 370 */
								int64_t BgL_arg1367z00_1460;

								BgL_arg1367z00_1460 =
									BGl_expts64z00zz__r4_numbers_6_5_fixnumz00((int64_t) (2),
									(int64_t) (30L));
								BgL_arg1366z00_1459 = (BgL_xz00_1438 / BgL_arg1367z00_1460);
							}
							{
								int64_t BgL_xz00_6088;
								long BgL_nbzd2digitszd2_6087;

								BgL_nbzd2digitszd2_6087 = BgL_arg1365z00_1458;
								BgL_xz00_6088 = BgL_arg1366z00_1459;
								BgL_xz00_1438 = BgL_xz00_6088;
								BgL_nbzd2digitszd2_1437 = BgL_nbzd2digitszd2_6087;
								goto BgL_zc3z04anonymousza31350ze3z87_1439;
							}
						}
				}
			}
		}

	}



/* &$$int64->bignum */
	obj_t BGl_z62z42z42int64zd2ze3bignumz53zz__bignumz00(obj_t BgL_envz00_5510,
		obj_t BgL_nz00_5511)
	{
		{	/* Unsafe/bignumber.scm 365 */
			{	/* Unsafe/bignumber.scm 366 */
				int64_t BgL_auxz00_6089;

				{	/* Unsafe/bignumber.scm 366 */
					obj_t BgL_tmpz00_6090;

					if (BGL_INT64P(BgL_nz00_5511))
						{	/* Unsafe/bignumber.scm 366 */
							BgL_tmpz00_6090 = BgL_nz00_5511;
						}
					else
						{
							obj_t BgL_auxz00_6093;

							BgL_auxz00_6093 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
								BINT(14032L), BGl_string2485z00zz__bignumz00,
								BGl_string2486z00zz__bignumz00, BgL_nz00_5511);
							FAILURE(BgL_auxz00_6093, BFALSE, BFALSE);
						}
					BgL_auxz00_6089 = BGL_BINT64_TO_INT64(BgL_tmpz00_6090);
				}
				return bgl_int64_to_bignum(BgL_auxz00_6089);
			}
		}

	}



/* $$uint64->bignum */
	BGL_EXPORTED_DEF obj_t bgl_uint64_to_bignum(uint64_t BgL_nz00_25)
	{
		{	/* Unsafe/bignumber.scm 385 */
			{	/* Unsafe/bignumber.scm 386 */
				uint64_t BgL_negzd2nzd2_1463;

				if ((BgL_nz00_25 < (uint64_t) (0)))
					{	/* Unsafe/bignumber.scm 386 */
						BgL_negzd2nzd2_1463 = BgL_nz00_25;
					}
				else
					{	/* Unsafe/bignumber.scm 386 */
						BgL_negzd2nzd2_1463 = ((uint64_t) (0) - BgL_nz00_25);
					}
				{
					long BgL_nbzd2digitszd2_1465;
					uint64_t BgL_xz00_1466;

					BgL_nbzd2digitszd2_1465 = 0L;
					BgL_xz00_1466 = BgL_negzd2nzd2_1463;
				BgL_zc3z04anonymousza31369ze3z87_1467:
					if ((BgL_xz00_1466 == (uint64_t) (0)))
						{	/* Unsafe/bignumber.scm 391 */
							obj_t BgL_rz00_1469;

							{	/* Unsafe/bignumber.scm 391 */
								long BgL_arg1384z00_1485;

								BgL_arg1384z00_1485 = (BgL_nbzd2digitszd2_1465 + 1L);
								{	/* Unsafe/bignumber.scm 391 */
									int BgL_lenz00_3223;

									BgL_lenz00_3223 = (int) (BgL_arg1384z00_1485);
									{	/* Unsafe/bignumber.scm 259 */
										obj_t BgL_arg1208z00_3224;

										{	/* Unsafe/bignumber.scm 259 */

											BgL_arg1208z00_3224 =
												BGl_makezd2u16vectorzd2zz__srfi4z00(
												(long) (BgL_lenz00_3223), (uint16_t) (0));
										}
										BgL_rz00_1469 = bgl_make_bignum(BgL_arg1208z00_3224);
							}}}
							if ((BgL_nz00_25 < (uint64_t) (0)))
								{	/* Unsafe/bignumber.scm 265 */
									obj_t BgL_arg1218z00_3231;
									uint16_t BgL_arg1219z00_3232;

									BgL_arg1218z00_3231 = BGL_BIGNUM_U16VECT(BgL_rz00_1469);
									BgL_arg1219z00_3232 = (uint16_t) (0L);
									BGL_U16VSET(BgL_arg1218z00_3231, 0L, BgL_arg1219z00_3232);
									BUNSPEC;
								}
							else
								{	/* Unsafe/bignumber.scm 265 */
									obj_t BgL_arg1218z00_3235;
									uint16_t BgL_arg1219z00_3236;

									BgL_arg1218z00_3235 = BGL_BIGNUM_U16VECT(BgL_rz00_1469);
									BgL_arg1219z00_3236 = (uint16_t) (1L);
									BGL_U16VSET(BgL_arg1218z00_3235, 0L, BgL_arg1219z00_3236);
									BUNSPEC;
								}
							{
								long BgL_iz00_3255;
								uint64_t BgL_xz00_3256;

								BgL_iz00_3255 = 1L;
								BgL_xz00_3256 = BgL_negzd2nzd2_1463;
							BgL_loop2z00_3254:
								if ((BgL_xz00_3256 == (uint64_t) (0)))
									{	/* Unsafe/bignumber.scm 396 */
										return BgL_rz00_1469;
									}
								else
									{	/* Unsafe/bignumber.scm 396 */
										{	/* Unsafe/bignumber.scm 401 */
											long BgL_arg1375z00_3260;

											{	/* Unsafe/bignumber.scm 401 */
												long BgL_arg1376z00_3261;

												{	/* Unsafe/bignumber.scm 401 */
													uint64_t BgL_arg1377z00_3262;

													{	/* Unsafe/bignumber.scm 401 */
														uint64_t BgL_arg1378z00_3263;

														BgL_arg1378z00_3263 = (uint64_t) (16384L);
														{	/* Unsafe/bignumber.scm 401 */
															int64_t BgL_tmpz00_6120;

															BgL_tmpz00_6120 = (int64_t) (BgL_arg1378z00_3263);
															BgL_arg1377z00_3262 =
																(BgL_xz00_3256 % BgL_tmpz00_6120);
													}}
													{	/* Unsafe/bignumber.scm 401 */
														long BgL_arg2268z00_3268;

														BgL_arg2268z00_3268 = (long) (BgL_arg1377z00_3262);
														BgL_arg1376z00_3261 = (long) (BgL_arg2268z00_3268);
												}}
												BgL_arg1375z00_3260 = (0L - BgL_arg1376z00_3261);
											}
											{	/* Unsafe/bignumber.scm 398 */
												int BgL_iz00_3272;
												int BgL_digitz00_3273;

												BgL_iz00_3272 = (int) (BgL_iz00_3255);
												BgL_digitz00_3273 = (int) (BgL_arg1375z00_3260);
												{	/* Unsafe/bignumber.scm 269 */
													obj_t BgL_arg1223z00_3274;
													uint16_t BgL_arg1225z00_3275;

													BgL_arg1223z00_3274 =
														BGL_BIGNUM_U16VECT(BgL_rz00_1469);
													{	/* Unsafe/bignumber.scm 269 */
														long BgL_tmpz00_6129;

														BgL_tmpz00_6129 = (long) (BgL_digitz00_3273);
														BgL_arg1225z00_3275 = (uint16_t) (BgL_tmpz00_6129);
													}
													{	/* Unsafe/bignumber.scm 269 */
														long BgL_tmpz00_6132;

														BgL_tmpz00_6132 = (long) (BgL_iz00_3272);
														BGL_U16VSET(BgL_arg1223z00_3274, BgL_tmpz00_6132,
															BgL_arg1225z00_3275);
													} BUNSPEC;
										}}}
										{	/* Unsafe/bignumber.scm 402 */
											long BgL_arg1380z00_3277;
											uint64_t BgL_arg1382z00_3278;

											BgL_arg1380z00_3277 = (BgL_iz00_3255 + 1L);
											{	/* Unsafe/bignumber.scm 402 */
												uint64_t BgL_n2z00_3282;

												BgL_n2z00_3282 = (uint64_t) (16384L);
												{	/* Unsafe/bignumber.scm 402 */
													int64_t BgL_tmpz00_6137;

													BgL_tmpz00_6137 = (int64_t) (BgL_n2z00_3282);
													BgL_arg1382z00_3278 =
														(BgL_xz00_3256 / BgL_tmpz00_6137);
											}}
											{
												uint64_t BgL_xz00_6141;
												long BgL_iz00_6140;

												BgL_iz00_6140 = BgL_arg1380z00_3277;
												BgL_xz00_6141 = BgL_arg1382z00_3278;
												BgL_xz00_3256 = BgL_xz00_6141;
												BgL_iz00_3255 = BgL_iz00_6140;
												goto BgL_loop2z00_3254;
											}
										}
									}
							}
						}
					else
						{	/* Unsafe/bignumber.scm 390 */
							long BgL_arg1387z00_1486;
							uint64_t BgL_arg1388z00_1487;

							BgL_arg1387z00_1486 = (BgL_nbzd2digitszd2_1465 + 1L);
							{	/* Unsafe/bignumber.scm 390 */
								int64_t BgL_arg1389z00_1488;

								BgL_arg1389z00_1488 =
									BGl_expts64z00zz__r4_numbers_6_5_fixnumz00((int64_t) (2),
									(int64_t) (30L));
								{	/* Unsafe/bignumber.scm 390 */
									uint64_t BgL_n2z00_3285;

									BgL_n2z00_3285 = (uint64_t) (BgL_arg1389z00_1488);
									{	/* Unsafe/bignumber.scm 390 */
										int64_t BgL_tmpz00_6146;

										BgL_tmpz00_6146 = (int64_t) (BgL_n2z00_3285);
										BgL_arg1388z00_1487 = (BgL_xz00_1466 / BgL_tmpz00_6146);
							}}}
							{
								uint64_t BgL_xz00_6150;
								long BgL_nbzd2digitszd2_6149;

								BgL_nbzd2digitszd2_6149 = BgL_arg1387z00_1486;
								BgL_xz00_6150 = BgL_arg1388z00_1487;
								BgL_xz00_1466 = BgL_xz00_6150;
								BgL_nbzd2digitszd2_1465 = BgL_nbzd2digitszd2_6149;
								goto BgL_zc3z04anonymousza31369ze3z87_1467;
							}
						}
				}
			}
		}

	}



/* &$$uint64->bignum */
	obj_t BGl_z62z42z42uint64zd2ze3bignumz53zz__bignumz00(obj_t BgL_envz00_5512,
		obj_t BgL_nz00_5513)
	{
		{	/* Unsafe/bignumber.scm 385 */
			{	/* Unsafe/bignumber.scm 386 */
				uint64_t BgL_auxz00_6151;

				{	/* Unsafe/bignumber.scm 386 */
					obj_t BgL_tmpz00_6152;

					if (BGL_UINT64P(BgL_nz00_5513))
						{	/* Unsafe/bignumber.scm 386 */
							BgL_tmpz00_6152 = BgL_nz00_5513;
						}
					else
						{
							obj_t BgL_auxz00_6155;

							BgL_auxz00_6155 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
								BINT(14714L), BGl_string2487z00zz__bignumz00,
								BGl_string2488z00zz__bignumz00, BgL_nz00_5513);
							FAILURE(BgL_auxz00_6155, BFALSE, BFALSE);
						}
					BgL_auxz00_6151 = BGL_BINT64_TO_INT64(BgL_tmpz00_6152);
				}
				return bgl_uint64_to_bignum(BgL_auxz00_6151);
			}
		}

	}



/* $$fixnum->bignum */
	BGL_EXPORTED_DEF obj_t bgl_long_to_bignum(long BgL_nz00_26)
	{
		{	/* Unsafe/bignumber.scm 414 */
			{	/* Unsafe/bignumber.scm 415 */
				bool_t BgL_test2680z00_6161;

				if ((BgL_nz00_26 < -16L))
					{	/* Unsafe/bignumber.scm 415 */
						BgL_test2680z00_6161 = ((bool_t) 1);
					}
				else
					{	/* Unsafe/bignumber.scm 415 */
						BgL_test2680z00_6161 = (16L < BgL_nz00_26);
					}
				if (BgL_test2680z00_6161)
					{	/* Unsafe/bignumber.scm 415 */
						BGL_TAIL return
							BGl_z42z42fixnumzd2ze3bignumzd2freshze3zz__bignumz00(BgL_nz00_26);
					}
				else
					{	/* Unsafe/bignumber.scm 415 */
						return
							VECTOR_REF(BGl_preallocatedzd2bignumszd2zz__bignumz00,
							(BgL_nz00_26 + 16L));
					}
			}
		}

	}



/* &$$fixnum->bignum */
	obj_t BGl_z62z42z42fixnumzd2ze3bignumz53zz__bignumz00(obj_t BgL_envz00_5514,
		obj_t BgL_nz00_5515)
	{
		{	/* Unsafe/bignumber.scm 414 */
			{	/* Unsafe/bignumber.scm 415 */
				long BgL_auxz00_6168;

				{	/* Unsafe/bignumber.scm 415 */
					obj_t BgL_tmpz00_6169;

					if (INTEGERP(BgL_nz00_5515))
						{	/* Unsafe/bignumber.scm 415 */
							BgL_tmpz00_6169 = BgL_nz00_5515;
						}
					else
						{
							obj_t BgL_auxz00_6172;

							BgL_auxz00_6172 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
								BINT(15614L), BGl_string2489z00zz__bignumz00,
								BGl_string2480z00zz__bignumz00, BgL_nz00_5515);
							FAILURE(BgL_auxz00_6172, BFALSE, BFALSE);
						}
					BgL_auxz00_6168 = (long) CINT(BgL_tmpz00_6169);
				}
				return bgl_long_to_bignum(BgL_auxz00_6168);
			}
		}

	}



/* $<bx */
	bool_t BGl_z42zc3bxz81zz__bignumz00(obj_t BgL_xz00_29, obj_t BgL_yz00_30)
	{
		{	/* Unsafe/bignumber.scm 439 */
			{	/* Unsafe/bignumber.scm 440 */
				bool_t BgL_test2683z00_6178;

				{	/* Unsafe/bignumber.scm 440 */
					long BgL_arg1423z00_1536;
					long BgL_arg1424z00_1537;

					{	/* Unsafe/bignumber.scm 263 */
						uint16_t BgL_arg1215z00_3292;

						{	/* Unsafe/bignumber.scm 263 */
							obj_t BgL_arg1216z00_3293;

							BgL_arg1216z00_3293 = BGL_BIGNUM_U16VECT(BgL_xz00_29);
							BgL_arg1215z00_3292 = BGL_U16VREF(BgL_arg1216z00_3293, 0L);
						}
						{	/* Unsafe/bignumber.scm 263 */
							long BgL_arg2271z00_3295;

							BgL_arg2271z00_3295 = (long) (BgL_arg1215z00_3292);
							BgL_arg1423z00_1536 = (long) (BgL_arg2271z00_3295);
					}}
					{	/* Unsafe/bignumber.scm 263 */
						uint16_t BgL_arg1215z00_3298;

						{	/* Unsafe/bignumber.scm 263 */
							obj_t BgL_arg1216z00_3299;

							BgL_arg1216z00_3299 = BGL_BIGNUM_U16VECT(BgL_yz00_30);
							BgL_arg1215z00_3298 = BGL_U16VREF(BgL_arg1216z00_3299, 0L);
						}
						{	/* Unsafe/bignumber.scm 263 */
							long BgL_arg2271z00_3301;

							BgL_arg2271z00_3301 = (long) (BgL_arg1215z00_3298);
							BgL_arg1424z00_1537 = (long) (BgL_arg2271z00_3301);
					}}
					BgL_test2683z00_6178 = (BgL_arg1423z00_1536 == BgL_arg1424z00_1537);
				}
				if (BgL_test2683z00_6178)
					{	/* Unsafe/bignumber.scm 442 */
						long BgL_lenxz00_1519;
						long BgL_lenyz00_1520;

						{	/* Unsafe/bignumber.scm 261 */
							long BgL_arg1210z00_3306;

							{	/* Unsafe/bignumber.scm 261 */
								obj_t BgL_arg1212z00_3307;

								BgL_arg1212z00_3307 = BGL_BIGNUM_U16VECT(BgL_xz00_29);
								BgL_arg1210z00_3306 = BGL_HVECTOR_LENGTH(BgL_arg1212z00_3307);
							}
							{	/* Unsafe/bignumber.scm 261 */
								uint16_t BgL_xz00_3309;

								BgL_xz00_3309 = (uint16_t) (BgL_arg1210z00_3306);
								{	/* Unsafe/bignumber.scm 261 */
									long BgL_arg2271z00_3310;

									BgL_arg2271z00_3310 = (long) (BgL_xz00_3309);
									BgL_lenxz00_1519 = (long) (BgL_arg2271z00_3310);
						}}}
						{	/* Unsafe/bignumber.scm 261 */
							long BgL_arg1210z00_3313;

							{	/* Unsafe/bignumber.scm 261 */
								obj_t BgL_arg1212z00_3314;

								BgL_arg1212z00_3314 = BGL_BIGNUM_U16VECT(BgL_yz00_30);
								BgL_arg1210z00_3313 = BGL_HVECTOR_LENGTH(BgL_arg1212z00_3314);
							}
							{	/* Unsafe/bignumber.scm 261 */
								uint16_t BgL_xz00_3316;

								BgL_xz00_3316 = (uint16_t) (BgL_arg1210z00_3313);
								{	/* Unsafe/bignumber.scm 261 */
									long BgL_arg2271z00_3317;

									BgL_arg2271z00_3317 = (long) (BgL_xz00_3316);
									BgL_lenyz00_1520 = (long) (BgL_arg2271z00_3317);
						}}}
						if ((BgL_lenxz00_1519 < BgL_lenyz00_1520))
							{	/* Unsafe/bignumber.scm 445 */
								bool_t BgL_test2685z00_6200;

								{	/* Unsafe/bignumber.scm 471 */
									long BgL_arg1428z00_3322;

									{	/* Unsafe/bignumber.scm 263 */
										uint16_t BgL_arg1215z00_3324;

										{	/* Unsafe/bignumber.scm 263 */
											obj_t BgL_arg1216z00_3325;

											BgL_arg1216z00_3325 = BGL_BIGNUM_U16VECT(BgL_xz00_29);
											BgL_arg1215z00_3324 =
												BGL_U16VREF(BgL_arg1216z00_3325, 0L);
										}
										{	/* Unsafe/bignumber.scm 263 */
											long BgL_arg2271z00_3327;

											BgL_arg2271z00_3327 = (long) (BgL_arg1215z00_3324);
											BgL_arg1428z00_3322 = (long) (BgL_arg2271z00_3327);
									}}
									BgL_test2685z00_6200 = (BgL_arg1428z00_3322 == 0L);
								}
								if (BgL_test2685z00_6200)
									{	/* Unsafe/bignumber.scm 445 */
										return ((bool_t) 0);
									}
								else
									{	/* Unsafe/bignumber.scm 445 */
										return ((bool_t) 1);
									}
							}
						else
							{	/* Unsafe/bignumber.scm 444 */
								if ((BgL_lenyz00_1520 < BgL_lenxz00_1519))
									{	/* Unsafe/bignumber.scm 471 */
										long BgL_arg1428z00_3333;

										{	/* Unsafe/bignumber.scm 263 */
											uint16_t BgL_arg1215z00_3335;

											{	/* Unsafe/bignumber.scm 263 */
												obj_t BgL_arg1216z00_3336;

												BgL_arg1216z00_3336 = BGL_BIGNUM_U16VECT(BgL_xz00_29);
												BgL_arg1215z00_3335 =
													BGL_U16VREF(BgL_arg1216z00_3336, 0L);
											}
											{	/* Unsafe/bignumber.scm 263 */
												long BgL_arg2271z00_3338;

												BgL_arg2271z00_3338 = (long) (BgL_arg1215z00_3335);
												BgL_arg1428z00_3333 = (long) (BgL_arg2271z00_3338);
										}}
										return (BgL_arg1428z00_3333 == 0L);
									}
								else
									{	/* Unsafe/bignumber.scm 449 */
										long BgL_g1045z00_1524;

										BgL_g1045z00_1524 = (BgL_lenxz00_1519 - 1L);
										{
											long BgL_iz00_1526;

											BgL_iz00_1526 = BgL_g1045z00_1524;
										BgL_zc3z04anonymousza31417ze3z87_1527:
											if ((0L < BgL_iz00_1526))
												{	/* Unsafe/bignumber.scm 451 */
													long BgL_dxz00_1529;
													long BgL_dyz00_1530;

													{	/* Unsafe/bignumber.scm 451 */
														int BgL_iz00_3344;

														BgL_iz00_3344 = (int) (BgL_iz00_1526);
														{	/* Unsafe/bignumber.scm 267 */
															uint16_t BgL_arg1220z00_3345;

															{	/* Unsafe/bignumber.scm 267 */
																obj_t BgL_arg1221z00_3346;

																BgL_arg1221z00_3346 =
																	BGL_BIGNUM_U16VECT(BgL_xz00_29);
																{	/* Unsafe/bignumber.scm 267 */
																	long BgL_tmpz00_6218;

																	BgL_tmpz00_6218 = (long) (BgL_iz00_3344);
																	BgL_arg1220z00_3345 =
																		BGL_U16VREF(BgL_arg1221z00_3346,
																		BgL_tmpz00_6218);
															}}
															{	/* Unsafe/bignumber.scm 267 */
																long BgL_arg2271z00_3348;

																BgL_arg2271z00_3348 =
																	(long) (BgL_arg1220z00_3345);
																BgL_dxz00_1529 = (long) (BgL_arg2271z00_3348);
													}}}
													{	/* Unsafe/bignumber.scm 452 */
														int BgL_iz00_3351;

														BgL_iz00_3351 = (int) (BgL_iz00_1526);
														{	/* Unsafe/bignumber.scm 267 */
															uint16_t BgL_arg1220z00_3352;

															{	/* Unsafe/bignumber.scm 267 */
																obj_t BgL_arg1221z00_3353;

																BgL_arg1221z00_3353 =
																	BGL_BIGNUM_U16VECT(BgL_yz00_30);
																{	/* Unsafe/bignumber.scm 267 */
																	long BgL_tmpz00_6225;

																	BgL_tmpz00_6225 = (long) (BgL_iz00_3351);
																	BgL_arg1220z00_3352 =
																		BGL_U16VREF(BgL_arg1221z00_3353,
																		BgL_tmpz00_6225);
															}}
															{	/* Unsafe/bignumber.scm 267 */
																long BgL_arg2271z00_3355;

																BgL_arg2271z00_3355 =
																	(long) (BgL_arg1220z00_3352);
																BgL_dyz00_1530 = (long) (BgL_arg2271z00_3355);
													}}}
													if ((BgL_dxz00_1529 < BgL_dyz00_1530))
														{	/* Unsafe/bignumber.scm 453 */
															bool_t BgL_test2689z00_6232;

															{	/* Unsafe/bignumber.scm 471 */
																long BgL_arg1428z00_3360;

																{	/* Unsafe/bignumber.scm 263 */
																	uint16_t BgL_arg1215z00_3362;

																	{	/* Unsafe/bignumber.scm 263 */
																		obj_t BgL_arg1216z00_3363;

																		BgL_arg1216z00_3363 =
																			BGL_BIGNUM_U16VECT(BgL_xz00_29);
																		BgL_arg1215z00_3362 =
																			BGL_U16VREF(BgL_arg1216z00_3363, 0L);
																	}
																	{	/* Unsafe/bignumber.scm 263 */
																		long BgL_arg2271z00_3365;

																		BgL_arg2271z00_3365 =
																			(long) (BgL_arg1215z00_3362);
																		BgL_arg1428z00_3360 =
																			(long) (BgL_arg2271z00_3365);
																}}
																BgL_test2689z00_6232 =
																	(BgL_arg1428z00_3360 == 0L);
															}
															if (BgL_test2689z00_6232)
																{	/* Unsafe/bignumber.scm 453 */
																	return ((bool_t) 0);
																}
															else
																{	/* Unsafe/bignumber.scm 453 */
																	return ((bool_t) 1);
																}
														}
													else
														{	/* Unsafe/bignumber.scm 453 */
															if ((BgL_dyz00_1530 < BgL_dxz00_1529))
																{	/* Unsafe/bignumber.scm 471 */
																	long BgL_arg1428z00_3371;

																	{	/* Unsafe/bignumber.scm 263 */
																		uint16_t BgL_arg1215z00_3373;

																		{	/* Unsafe/bignumber.scm 263 */
																			obj_t BgL_arg1216z00_3374;

																			BgL_arg1216z00_3374 =
																				BGL_BIGNUM_U16VECT(BgL_xz00_29);
																			BgL_arg1215z00_3373 =
																				BGL_U16VREF(BgL_arg1216z00_3374, 0L);
																		}
																		{	/* Unsafe/bignumber.scm 263 */
																			long BgL_arg2271z00_3376;

																			BgL_arg2271z00_3376 =
																				(long) (BgL_arg1215z00_3373);
																			BgL_arg1428z00_3371 =
																				(long) (BgL_arg2271z00_3376);
																	}}
																	return (BgL_arg1428z00_3371 == 0L);
																}
															else
																{
																	long BgL_iz00_6245;

																	BgL_iz00_6245 = (BgL_iz00_1526 - 1L);
																	BgL_iz00_1526 = BgL_iz00_6245;
																	goto BgL_zc3z04anonymousza31417ze3z87_1527;
																}
														}
												}
											else
												{	/* Unsafe/bignumber.scm 450 */
													return ((bool_t) 0);
												}
										}
									}
							}
					}
				else
					{	/* Unsafe/bignumber.scm 471 */
						long BgL_arg1428z00_3381;

						{	/* Unsafe/bignumber.scm 263 */
							uint16_t BgL_arg1215z00_3383;

							{	/* Unsafe/bignumber.scm 263 */
								obj_t BgL_arg1216z00_3384;

								BgL_arg1216z00_3384 = BGL_BIGNUM_U16VECT(BgL_xz00_29);
								BgL_arg1215z00_3383 = BGL_U16VREF(BgL_arg1216z00_3384, 0L);
							}
							{	/* Unsafe/bignumber.scm 263 */
								long BgL_arg2271z00_3386;

								BgL_arg2271z00_3386 = (long) (BgL_arg1215z00_3383);
								BgL_arg1428z00_3381 = (long) (BgL_arg2271z00_3386);
						}}
						return (BgL_arg1428z00_3381 == 0L);
					}
			}
		}

	}



/* $$zerobx? */
	BGL_EXPORTED_DEF bool_t BXZERO(obj_t BgL_xz00_37)
	{
		{	/* Unsafe/bignumber.scm 467 */
			{	/* Unsafe/bignumber.scm 468 */
				long BgL_arg1427z00_3390;

				{	/* Unsafe/bignumber.scm 261 */
					long BgL_arg1210z00_3392;

					{	/* Unsafe/bignumber.scm 261 */
						obj_t BgL_arg1212z00_3393;

						BgL_arg1212z00_3393 = BGL_BIGNUM_U16VECT(BgL_xz00_37);
						BgL_arg1210z00_3392 = BGL_HVECTOR_LENGTH(BgL_arg1212z00_3393);
					}
					{	/* Unsafe/bignumber.scm 261 */
						uint16_t BgL_xz00_3395;

						BgL_xz00_3395 = (uint16_t) (BgL_arg1210z00_3392);
						{	/* Unsafe/bignumber.scm 261 */
							long BgL_arg2271z00_3396;

							BgL_arg2271z00_3396 = (long) (BgL_xz00_3395);
							BgL_arg1427z00_3390 = (long) (BgL_arg2271z00_3396);
				}}}
				return (BgL_arg1427z00_3390 == 1L);
			}
		}

	}



/* &$$zerobx? */
	obj_t BGl_z62z42z42za7erobxzf3z36zz__bignumz00(obj_t BgL_envz00_5516,
		obj_t BgL_xz00_5517)
	{
		{	/* Unsafe/bignumber.scm 467 */
			{	/* Unsafe/bignumber.scm 468 */
				bool_t BgL_tmpz00_6258;

				{	/* Unsafe/bignumber.scm 468 */
					obj_t BgL_auxz00_6259;

					if (BIGNUMP(BgL_xz00_5517))
						{	/* Unsafe/bignumber.scm 468 */
							BgL_auxz00_6259 = BgL_xz00_5517;
						}
					else
						{
							obj_t BgL_auxz00_6262;

							BgL_auxz00_6262 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
								BINT(17040L), BGl_string2490z00zz__bignumz00,
								BGl_string2491z00zz__bignumz00, BgL_xz00_5517);
							FAILURE(BgL_auxz00_6262, BFALSE, BFALSE);
						}
					BgL_tmpz00_6258 = BXZERO(BgL_auxz00_6259);
				}
				return BBOOL(BgL_tmpz00_6258);
			}
		}

	}



/* $$negativebx? */
	BGL_EXPORTED_DEF bool_t BXNEGATIVE(obj_t BgL_xz00_38)
	{
		{	/* Unsafe/bignumber.scm 470 */
			{	/* Unsafe/bignumber.scm 471 */
				long BgL_arg1428z00_3399;

				{	/* Unsafe/bignumber.scm 263 */
					uint16_t BgL_arg1215z00_3401;

					{	/* Unsafe/bignumber.scm 263 */
						obj_t BgL_arg1216z00_3402;

						BgL_arg1216z00_3402 = BGL_BIGNUM_U16VECT(BgL_xz00_38);
						BgL_arg1215z00_3401 = BGL_U16VREF(BgL_arg1216z00_3402, 0L);
					}
					{	/* Unsafe/bignumber.scm 263 */
						long BgL_arg2271z00_3404;

						BgL_arg2271z00_3404 = (long) (BgL_arg1215z00_3401);
						BgL_arg1428z00_3399 = (long) (BgL_arg2271z00_3404);
				}}
				return (BgL_arg1428z00_3399 == 0L);
			}
		}

	}



/* &$$negativebx? */
	obj_t BGl_z62z42z42negativebxzf3z91zz__bignumz00(obj_t BgL_envz00_5518,
		obj_t BgL_xz00_5519)
	{
		{	/* Unsafe/bignumber.scm 470 */
			{	/* Unsafe/bignumber.scm 471 */
				bool_t BgL_tmpz00_6273;

				{	/* Unsafe/bignumber.scm 471 */
					obj_t BgL_auxz00_6274;

					if (BIGNUMP(BgL_xz00_5519))
						{	/* Unsafe/bignumber.scm 471 */
							BgL_auxz00_6274 = BgL_xz00_5519;
						}
					else
						{
							obj_t BgL_auxz00_6277;

							BgL_auxz00_6277 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
								BINT(17097L), BGl_string2492z00zz__bignumz00,
								BGl_string2491z00zz__bignumz00, BgL_xz00_5519);
							FAILURE(BgL_auxz00_6277, BFALSE, BFALSE);
						}
					BgL_tmpz00_6273 = BXNEGATIVE(BgL_auxz00_6274);
				}
				return BBOOL(BgL_tmpz00_6273);
			}
		}

	}



/* $$positivebx? */
	BGL_EXPORTED_DEF bool_t BXPOSITIVE(obj_t BgL_xz00_39)
	{
		{	/* Unsafe/bignumber.scm 473 */
			{	/* Unsafe/bignumber.scm 474 */
				bool_t BgL_test2693z00_6283;

				{	/* Unsafe/bignumber.scm 474 */
					bool_t BgL_test2694z00_6284;

					{	/* Unsafe/bignumber.scm 468 */
						long BgL_arg1427z00_3410;

						{	/* Unsafe/bignumber.scm 261 */
							long BgL_arg1210z00_3412;

							{	/* Unsafe/bignumber.scm 261 */
								obj_t BgL_arg1212z00_3413;

								BgL_arg1212z00_3413 = BGL_BIGNUM_U16VECT(BgL_xz00_39);
								BgL_arg1210z00_3412 = BGL_HVECTOR_LENGTH(BgL_arg1212z00_3413);
							}
							{	/* Unsafe/bignumber.scm 261 */
								uint16_t BgL_xz00_3415;

								BgL_xz00_3415 = (uint16_t) (BgL_arg1210z00_3412);
								{	/* Unsafe/bignumber.scm 261 */
									long BgL_arg2271z00_3416;

									BgL_arg2271z00_3416 = (long) (BgL_xz00_3415);
									BgL_arg1427z00_3410 = (long) (BgL_arg2271z00_3416);
						}}}
						BgL_test2694z00_6284 = (BgL_arg1427z00_3410 == 1L);
					}
					if (BgL_test2694z00_6284)
						{	/* Unsafe/bignumber.scm 474 */
							BgL_test2693z00_6283 = ((bool_t) 1);
						}
					else
						{	/* Unsafe/bignumber.scm 471 */
							long BgL_arg1428z00_3420;

							{	/* Unsafe/bignumber.scm 263 */
								uint16_t BgL_arg1215z00_3422;

								{	/* Unsafe/bignumber.scm 263 */
									obj_t BgL_arg1216z00_3423;

									BgL_arg1216z00_3423 = BGL_BIGNUM_U16VECT(BgL_xz00_39);
									BgL_arg1215z00_3422 = BGL_U16VREF(BgL_arg1216z00_3423, 0L);
								}
								{	/* Unsafe/bignumber.scm 263 */
									long BgL_arg2271z00_3425;

									BgL_arg2271z00_3425 = (long) (BgL_arg1215z00_3422);
									BgL_arg1428z00_3420 = (long) (BgL_arg2271z00_3425);
							}}
							BgL_test2693z00_6283 = (BgL_arg1428z00_3420 == 0L);
				}}
				if (BgL_test2693z00_6283)
					{	/* Unsafe/bignumber.scm 474 */
						return ((bool_t) 0);
					}
				else
					{	/* Unsafe/bignumber.scm 474 */
						return ((bool_t) 1);
					}
			}
		}

	}



/* &$$positivebx? */
	obj_t BGl_z62z42z42positivebxzf3z91zz__bignumz00(obj_t BgL_envz00_5520,
		obj_t BgL_xz00_5521)
	{
		{	/* Unsafe/bignumber.scm 473 */
			{	/* Unsafe/bignumber.scm 474 */
				bool_t BgL_tmpz00_6296;

				{	/* Unsafe/bignumber.scm 474 */
					obj_t BgL_auxz00_6297;

					if (BIGNUMP(BgL_xz00_5521))
						{	/* Unsafe/bignumber.scm 474 */
							BgL_auxz00_6297 = BgL_xz00_5521;
						}
					else
						{
							obj_t BgL_auxz00_6300;

							BgL_auxz00_6300 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
								BINT(17147L), BGl_string2493z00zz__bignumz00,
								BGl_string2491z00zz__bignumz00, BgL_xz00_5521);
							FAILURE(BgL_auxz00_6300, BFALSE, BFALSE);
						}
					BgL_tmpz00_6296 = BXPOSITIVE(BgL_auxz00_6297);
				}
				return BBOOL(BgL_tmpz00_6296);
			}
		}

	}



/* $$evenbx? */
	BGL_EXPORTED_DEF bool_t bgl_bignum_even(obj_t BgL_xz00_40)
	{
		{	/* Unsafe/bignumber.scm 476 */
			{	/* Unsafe/bignumber.scm 477 */
				bool_t BgL__ortest_1046z00_3428;

				{	/* Unsafe/bignumber.scm 468 */
					long BgL_arg1427z00_3431;

					{	/* Unsafe/bignumber.scm 261 */
						long BgL_arg1210z00_3433;

						{	/* Unsafe/bignumber.scm 261 */
							obj_t BgL_arg1212z00_3434;

							BgL_arg1212z00_3434 = BGL_BIGNUM_U16VECT(BgL_xz00_40);
							BgL_arg1210z00_3433 = BGL_HVECTOR_LENGTH(BgL_arg1212z00_3434);
						}
						{	/* Unsafe/bignumber.scm 261 */
							uint16_t BgL_xz00_3436;

							BgL_xz00_3436 = (uint16_t) (BgL_arg1210z00_3433);
							{	/* Unsafe/bignumber.scm 261 */
								long BgL_arg2271z00_3437;

								BgL_arg2271z00_3437 = (long) (BgL_xz00_3436);
								BgL_arg1427z00_3431 = (long) (BgL_arg2271z00_3437);
					}}}
					BgL__ortest_1046z00_3428 = (BgL_arg1427z00_3431 == 1L);
				}
				if (BgL__ortest_1046z00_3428)
					{	/* Unsafe/bignumber.scm 477 */
						return BgL__ortest_1046z00_3428;
					}
				else
					{	/* Unsafe/bignumber.scm 477 */
						long BgL_arg1431z00_3429;

						{	/* Unsafe/bignumber.scm 267 */
							uint16_t BgL_arg1220z00_3441;

							{	/* Unsafe/bignumber.scm 267 */
								obj_t BgL_arg1221z00_3442;

								BgL_arg1221z00_3442 = BGL_BIGNUM_U16VECT(BgL_xz00_40);
								BgL_arg1220z00_3441 = BGL_U16VREF(BgL_arg1221z00_3442, 1L);
							}
							{	/* Unsafe/bignumber.scm 267 */
								long BgL_arg2271z00_3444;

								BgL_arg2271z00_3444 = (long) (BgL_arg1220z00_3441);
								BgL_arg1431z00_3429 = (long) (BgL_arg2271z00_3444);
						}}
						return
							BGl_evenzf3zf3zz__r4_numbers_6_5_fixnumz00(BINT
							(BgL_arg1431z00_3429));
					}
			}
		}

	}



/* &$$evenbx? */
	obj_t BGl_z62z42z42evenbxzf3z91zz__bignumz00(obj_t BgL_envz00_5522,
		obj_t BgL_xz00_5523)
	{
		{	/* Unsafe/bignumber.scm 476 */
			{	/* Unsafe/bignumber.scm 477 */
				bool_t BgL_tmpz00_6319;

				{	/* Unsafe/bignumber.scm 477 */
					obj_t BgL_auxz00_6320;

					if (BIGNUMP(BgL_xz00_5523))
						{	/* Unsafe/bignumber.scm 477 */
							BgL_auxz00_6320 = BgL_xz00_5523;
						}
					else
						{
							obj_t BgL_auxz00_6323;

							BgL_auxz00_6323 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
								BINT(17221L), BGl_string2494z00zz__bignumz00,
								BGl_string2491z00zz__bignumz00, BgL_xz00_5523);
							FAILURE(BgL_auxz00_6323, BFALSE, BFALSE);
						}
					BgL_tmpz00_6319 = bgl_bignum_even(BgL_auxz00_6320);
				}
				return BBOOL(BgL_tmpz00_6319);
			}
		}

	}



/* $$oddbx? */
	BGL_EXPORTED_DEF bool_t bgl_bignum_odd(obj_t BgL_xz00_41)
	{
		{	/* Unsafe/bignumber.scm 479 */
			{	/* Unsafe/bignumber.scm 480 */
				bool_t BgL_test2698z00_6329;

				{	/* Unsafe/bignumber.scm 480 */
					bool_t BgL_res2329z00_3459;

					{	/* Unsafe/bignumber.scm 477 */
						bool_t BgL__ortest_1046z00_3448;

						BgL__ortest_1046z00_3448 =
							(BGl_bignumzd2lengthzd2zz__bignumz00(BgL_xz00_41) == 1L);
						if (BgL__ortest_1046z00_3448)
							{	/* Unsafe/bignumber.scm 477 */
								BgL_res2329z00_3459 = BgL__ortest_1046z00_3448;
							}
						else
							{	/* Unsafe/bignumber.scm 477 */
								long BgL_arg1431z00_3449;

								{	/* Unsafe/bignumber.scm 267 */
									uint16_t BgL_arg1220z00_3454;

									{	/* Unsafe/bignumber.scm 267 */
										obj_t BgL_arg1221z00_3455;

										BgL_arg1221z00_3455 = BGL_BIGNUM_U16VECT(BgL_xz00_41);
										BgL_arg1220z00_3454 = BGL_U16VREF(BgL_arg1221z00_3455, 1L);
									}
									{	/* Unsafe/bignumber.scm 267 */
										long BgL_arg2271z00_3457;

										BgL_arg2271z00_3457 = (long) (BgL_arg1220z00_3454);
										BgL_arg1431z00_3449 = (long) (BgL_arg2271z00_3457);
								}}
								BgL_res2329z00_3459 =
									BGl_evenzf3zf3zz__r4_numbers_6_5_fixnumz00(BINT
									(BgL_arg1431z00_3449));
					}}
					BgL_test2698z00_6329 = BgL_res2329z00_3459;
				}
				if (BgL_test2698z00_6329)
					{	/* Unsafe/bignumber.scm 480 */
						return ((bool_t) 0);
					}
				else
					{	/* Unsafe/bignumber.scm 480 */
						return ((bool_t) 1);
					}
			}
		}

	}



/* &$$oddbx? */
	obj_t BGl_z62z42z42oddbxzf3z91zz__bignumz00(obj_t BgL_envz00_5524,
		obj_t BgL_xz00_5525)
	{
		{	/* Unsafe/bignumber.scm 479 */
			{	/* Unsafe/bignumber.scm 480 */
				bool_t BgL_tmpz00_6339;

				{	/* Unsafe/bignumber.scm 480 */
					obj_t BgL_auxz00_6340;

					if (BIGNUMP(BgL_xz00_5525))
						{	/* Unsafe/bignumber.scm 480 */
							BgL_auxz00_6340 = BgL_xz00_5525;
						}
					else
						{
							obj_t BgL_auxz00_6343;

							BgL_auxz00_6343 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
								BINT(17293L), BGl_string2495z00zz__bignumz00,
								BGl_string2491z00zz__bignumz00, BgL_xz00_5525);
							FAILURE(BgL_auxz00_6343, BFALSE, BFALSE);
						}
					BgL_tmpz00_6339 = bgl_bignum_odd(BgL_auxz00_6340);
				}
				return BBOOL(BgL_tmpz00_6339);
			}
		}

	}



/* $$bignum-cmp */
	BGL_EXPORTED_DEF int bgl_bignum_cmp(obj_t BgL_n1z00_42, obj_t BgL_n2z00_43)
	{
		{	/* Unsafe/bignumber.scm 482 */
			if (BGl_z42zc3bxz81zz__bignumz00(BgL_n1z00_42, BgL_n2z00_43))
				{	/* Unsafe/bignumber.scm 484 */
					return (int) (-1L);
				}
			else
				{	/* Unsafe/bignumber.scm 484 */
					if (BGl_z42zc3bxz81zz__bignumz00(BgL_n2z00_43, BgL_n1z00_42))
						{	/* Unsafe/bignumber.scm 485 */
							return (int) (1L);
						}
					else
						{	/* Unsafe/bignumber.scm 485 */
							return (int) (0L);
		}}}

	}



/* &$$bignum-cmp */
	obj_t BGl_z62z42z42bignumzd2cmpzb0zz__bignumz00(obj_t BgL_envz00_5526,
		obj_t BgL_n1z00_5527, obj_t BgL_n2z00_5528)
	{
		{	/* Unsafe/bignumber.scm 482 */
			{	/* Unsafe/bignumber.scm 484 */
				int BgL_tmpz00_6356;

				{	/* Unsafe/bignumber.scm 484 */
					obj_t BgL_auxz00_6364;
					obj_t BgL_auxz00_6357;

					if (BIGNUMP(BgL_n2z00_5528))
						{	/* Unsafe/bignumber.scm 484 */
							BgL_auxz00_6364 = BgL_n2z00_5528;
						}
					else
						{
							obj_t BgL_auxz00_6367;

							BgL_auxz00_6367 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
								BINT(17359L), BGl_string2496z00zz__bignumz00,
								BGl_string2491z00zz__bignumz00, BgL_n2z00_5528);
							FAILURE(BgL_auxz00_6367, BFALSE, BFALSE);
						}
					if (BIGNUMP(BgL_n1z00_5527))
						{	/* Unsafe/bignumber.scm 484 */
							BgL_auxz00_6357 = BgL_n1z00_5527;
						}
					else
						{
							obj_t BgL_auxz00_6360;

							BgL_auxz00_6360 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
								BINT(17359L), BGl_string2496z00zz__bignumz00,
								BGl_string2491z00zz__bignumz00, BgL_n1z00_5527);
							FAILURE(BgL_auxz00_6360, BFALSE, BFALSE);
						}
					BgL_tmpz00_6356 = bgl_bignum_cmp(BgL_auxz00_6357, BgL_auxz00_6364);
				}
				return BINT(BgL_tmpz00_6356);
			}
		}

	}



/* bignum-add-nonneg */
	obj_t BGl_bignumzd2addzd2nonnegz00zz__bignumz00(obj_t BgL_xz00_44,
		obj_t BgL_yz00_45)
	{
		{	/* Unsafe/bignumber.scm 491 */
			{
				obj_t BgL_xz00_1554;
				obj_t BgL_yz00_1555;
				long BgL_lenxz00_1556;
				long BgL_lenyz00_1557;

				{	/* Unsafe/bignumber.scm 530 */
					long BgL_lenxz00_1551;
					long BgL_lenyz00_1552;

					{	/* Unsafe/bignumber.scm 261 */
						long BgL_arg1210z00_3546;

						{	/* Unsafe/bignumber.scm 261 */
							obj_t BgL_arg1212z00_3547;

							BgL_arg1212z00_3547 = BGL_BIGNUM_U16VECT(BgL_xz00_44);
							BgL_arg1210z00_3546 = BGL_HVECTOR_LENGTH(BgL_arg1212z00_3547);
						}
						{	/* Unsafe/bignumber.scm 261 */
							uint16_t BgL_xz00_3549;

							BgL_xz00_3549 = (uint16_t) (BgL_arg1210z00_3546);
							{	/* Unsafe/bignumber.scm 261 */
								long BgL_arg2271z00_3550;

								BgL_arg2271z00_3550 = (long) (BgL_xz00_3549);
								BgL_lenxz00_1551 = (long) (BgL_arg2271z00_3550);
					}}}
					{	/* Unsafe/bignumber.scm 261 */
						long BgL_arg1210z00_3553;

						{	/* Unsafe/bignumber.scm 261 */
							obj_t BgL_arg1212z00_3554;

							BgL_arg1212z00_3554 = BGL_BIGNUM_U16VECT(BgL_yz00_45);
							BgL_arg1210z00_3553 = BGL_HVECTOR_LENGTH(BgL_arg1212z00_3554);
						}
						{	/* Unsafe/bignumber.scm 261 */
							uint16_t BgL_xz00_3556;

							BgL_xz00_3556 = (uint16_t) (BgL_arg1210z00_3553);
							{	/* Unsafe/bignumber.scm 261 */
								long BgL_arg2271z00_3557;

								BgL_arg2271z00_3557 = (long) (BgL_xz00_3556);
								BgL_lenyz00_1552 = (long) (BgL_arg2271z00_3557);
					}}}
					if ((BgL_lenxz00_1551 < BgL_lenyz00_1552))
						{	/* Unsafe/bignumber.scm 532 */
							BgL_xz00_1554 = BgL_yz00_45;
							BgL_yz00_1555 = BgL_xz00_44;
							BgL_lenxz00_1556 = BgL_lenyz00_1552;
							BgL_lenyz00_1557 = BgL_lenxz00_1551;
						BgL_zc3z04anonymousza31436ze3z87_1558:
							{	/* Unsafe/bignumber.scm 494 */
								obj_t BgL_rz00_1559;

								{	/* Unsafe/bignumber.scm 494 */
									long BgL_arg1460z00_1593;

									BgL_arg1460z00_1593 = (BgL_lenxz00_1556 + 1L);
									{	/* Unsafe/bignumber.scm 494 */
										int BgL_lenz00_3463;

										BgL_lenz00_3463 = (int) (BgL_arg1460z00_1593);
										{	/* Unsafe/bignumber.scm 259 */
											obj_t BgL_arg1208z00_3464;

											{	/* Unsafe/bignumber.scm 259 */

												BgL_arg1208z00_3464 =
													BGl_makezd2u16vectorzd2zz__srfi4z00(
													(long) (BgL_lenz00_3463), (uint16_t) (0));
											}
											BgL_rz00_1559 = bgl_make_bignum(BgL_arg1208z00_3464);
								}}}
								{	/* Unsafe/bignumber.scm 265 */
									obj_t BgL_arg1218z00_3469;
									uint16_t BgL_arg1219z00_3470;

									BgL_arg1218z00_3469 = BGL_BIGNUM_U16VECT(BgL_rz00_1559);
									BgL_arg1219z00_3470 = (uint16_t) (1L);
									BGL_U16VSET(BgL_arg1218z00_3469, 0L, BgL_arg1219z00_3470);
									BUNSPEC;
								}
								{
									long BgL_iz00_1561;
									long BgL_cz00_1562;

									BgL_iz00_1561 = 1L;
									BgL_cz00_1562 = 0L;
								BgL_zc3z04anonymousza31437ze3z87_1563:
									if ((BgL_iz00_1561 < BgL_lenyz00_1557))
										{	/* Unsafe/bignumber.scm 501 */
											long BgL_wz00_1565;

											{	/* Unsafe/bignumber.scm 501 */
												long BgL_arg1446z00_1573;

												{	/* Unsafe/bignumber.scm 501 */
													long BgL_arg1447z00_1574;
													long BgL_arg1448z00_1575;

													{	/* Unsafe/bignumber.scm 501 */
														int BgL_iz00_3474;

														BgL_iz00_3474 = (int) (BgL_iz00_1561);
														{	/* Unsafe/bignumber.scm 267 */
															uint16_t BgL_arg1220z00_3475;

															{	/* Unsafe/bignumber.scm 267 */
																obj_t BgL_arg1221z00_3476;

																BgL_arg1221z00_3476 =
																	BGL_BIGNUM_U16VECT(BgL_xz00_1554);
																{	/* Unsafe/bignumber.scm 267 */
																	long BgL_tmpz00_6397;

																	BgL_tmpz00_6397 = (long) (BgL_iz00_3474);
																	BgL_arg1220z00_3475 =
																		BGL_U16VREF(BgL_arg1221z00_3476,
																		BgL_tmpz00_6397);
															}}
															{	/* Unsafe/bignumber.scm 267 */
																long BgL_arg2271z00_3478;

																BgL_arg2271z00_3478 =
																	(long) (BgL_arg1220z00_3475);
																BgL_arg1447z00_1574 =
																	(long) (BgL_arg2271z00_3478);
													}}}
													{	/* Unsafe/bignumber.scm 502 */
														int BgL_iz00_3481;

														BgL_iz00_3481 = (int) (BgL_iz00_1561);
														{	/* Unsafe/bignumber.scm 267 */
															uint16_t BgL_arg1220z00_3482;

															{	/* Unsafe/bignumber.scm 267 */
																obj_t BgL_arg1221z00_3483;

																BgL_arg1221z00_3483 =
																	BGL_BIGNUM_U16VECT(BgL_yz00_1555);
																{	/* Unsafe/bignumber.scm 267 */
																	long BgL_tmpz00_6404;

																	BgL_tmpz00_6404 = (long) (BgL_iz00_3481);
																	BgL_arg1220z00_3482 =
																		BGL_U16VREF(BgL_arg1221z00_3483,
																		BgL_tmpz00_6404);
															}}
															{	/* Unsafe/bignumber.scm 267 */
																long BgL_arg2271z00_3485;

																BgL_arg2271z00_3485 =
																	(long) (BgL_arg1220z00_3482);
																BgL_arg1448z00_1575 =
																	(long) (BgL_arg2271z00_3485);
													}}}
													BgL_arg1446z00_1573 =
														(BgL_arg1447z00_1574 + BgL_arg1448z00_1575);
												}
												BgL_wz00_1565 = (BgL_arg1446z00_1573 + BgL_cz00_1562);
											}
											if ((BgL_wz00_1565 < 16384L))
												{	/* Unsafe/bignumber.scm 504 */
													{	/* Unsafe/bignumber.scm 506 */
														int BgL_iz00_3494;
														int BgL_digitz00_3495;

														BgL_iz00_3494 = (int) (BgL_iz00_1561);
														BgL_digitz00_3495 = (int) (BgL_wz00_1565);
														{	/* Unsafe/bignumber.scm 269 */
															obj_t BgL_arg1223z00_3496;
															uint16_t BgL_arg1225z00_3497;

															BgL_arg1223z00_3496 =
																BGL_BIGNUM_U16VECT(BgL_rz00_1559);
															{	/* Unsafe/bignumber.scm 269 */
																long BgL_tmpz00_6416;

																BgL_tmpz00_6416 = (long) (BgL_digitz00_3495);
																BgL_arg1225z00_3497 =
																	(uint16_t) (BgL_tmpz00_6416);
															}
															{	/* Unsafe/bignumber.scm 269 */
																long BgL_tmpz00_6419;

																BgL_tmpz00_6419 = (long) (BgL_iz00_3494);
																BGL_U16VSET(BgL_arg1223z00_3496,
																	BgL_tmpz00_6419, BgL_arg1225z00_3497);
															} BUNSPEC;
													}}
													{
														long BgL_cz00_6424;
														long BgL_iz00_6422;

														BgL_iz00_6422 = (BgL_iz00_1561 + 1L);
														BgL_cz00_6424 = 0L;
														BgL_cz00_1562 = BgL_cz00_6424;
														BgL_iz00_1561 = BgL_iz00_6422;
														goto BgL_zc3z04anonymousza31437ze3z87_1563;
													}
												}
											else
												{	/* Unsafe/bignumber.scm 504 */
													{	/* Unsafe/bignumber.scm 509 */
														long BgL_arg1442z00_1569;

														BgL_arg1442z00_1569 = (BgL_wz00_1565 - 16384L);
														{	/* Unsafe/bignumber.scm 509 */
															int BgL_iz00_3503;
															int BgL_digitz00_3504;

															BgL_iz00_3503 = (int) (BgL_iz00_1561);
															BgL_digitz00_3504 = (int) (BgL_arg1442z00_1569);
															{	/* Unsafe/bignumber.scm 269 */
																obj_t BgL_arg1223z00_3505;
																uint16_t BgL_arg1225z00_3506;

																BgL_arg1223z00_3505 =
																	BGL_BIGNUM_U16VECT(BgL_rz00_1559);
																{	/* Unsafe/bignumber.scm 269 */
																	long BgL_tmpz00_6429;

																	BgL_tmpz00_6429 = (long) (BgL_digitz00_3504);
																	BgL_arg1225z00_3506 =
																		(uint16_t) (BgL_tmpz00_6429);
																}
																{	/* Unsafe/bignumber.scm 269 */
																	long BgL_tmpz00_6432;

																	BgL_tmpz00_6432 = (long) (BgL_iz00_3503);
																	BGL_U16VSET(BgL_arg1223z00_3505,
																		BgL_tmpz00_6432, BgL_arg1225z00_3506);
																} BUNSPEC;
													}}}
													{
														long BgL_cz00_6437;
														long BgL_iz00_6435;

														BgL_iz00_6435 = (BgL_iz00_1561 + 1L);
														BgL_cz00_6437 = 1L;
														BgL_cz00_1562 = BgL_cz00_6437;
														BgL_iz00_1561 = BgL_iz00_6435;
														goto BgL_zc3z04anonymousza31437ze3z87_1563;
													}
												}
										}
									else
										{
											long BgL_iz00_1577;
											long BgL_cz00_1578;

											BgL_iz00_1577 = BgL_iz00_1561;
											BgL_cz00_1578 = BgL_cz00_1562;
										BgL_zc3z04anonymousza31449ze3z87_1579:
											if ((BgL_iz00_1577 < BgL_lenxz00_1556))
												{	/* Unsafe/bignumber.scm 515 */
													long BgL_wz00_1581;

													{	/* Unsafe/bignumber.scm 515 */
														long BgL_arg1458z00_1589;

														{	/* Unsafe/bignumber.scm 515 */
															int BgL_iz00_3512;

															BgL_iz00_3512 = (int) (BgL_iz00_1577);
															{	/* Unsafe/bignumber.scm 267 */
																uint16_t BgL_arg1220z00_3513;

																{	/* Unsafe/bignumber.scm 267 */
																	obj_t BgL_arg1221z00_3514;

																	BgL_arg1221z00_3514 =
																		BGL_BIGNUM_U16VECT(BgL_xz00_1554);
																	{	/* Unsafe/bignumber.scm 267 */
																		long BgL_tmpz00_6442;

																		BgL_tmpz00_6442 = (long) (BgL_iz00_3512);
																		BgL_arg1220z00_3513 =
																			BGL_U16VREF(BgL_arg1221z00_3514,
																			BgL_tmpz00_6442);
																}}
																{	/* Unsafe/bignumber.scm 267 */
																	long BgL_arg2271z00_3516;

																	BgL_arg2271z00_3516 =
																		(long) (BgL_arg1220z00_3513);
																	BgL_arg1458z00_1589 =
																		(long) (BgL_arg2271z00_3516);
														}}}
														BgL_wz00_1581 =
															(BgL_arg1458z00_1589 + BgL_cz00_1578);
													}
													if ((BgL_wz00_1581 < 16384L))
														{	/* Unsafe/bignumber.scm 516 */
															{	/* Unsafe/bignumber.scm 518 */
																int BgL_iz00_3523;
																int BgL_digitz00_3524;

																BgL_iz00_3523 = (int) (BgL_iz00_1577);
																BgL_digitz00_3524 = (int) (BgL_wz00_1581);
																{	/* Unsafe/bignumber.scm 269 */
																	obj_t BgL_arg1223z00_3525;
																	uint16_t BgL_arg1225z00_3526;

																	BgL_arg1223z00_3525 =
																		BGL_BIGNUM_U16VECT(BgL_rz00_1559);
																	{	/* Unsafe/bignumber.scm 269 */
																		long BgL_tmpz00_6453;

																		BgL_tmpz00_6453 =
																			(long) (BgL_digitz00_3524);
																		BgL_arg1225z00_3526 =
																			(uint16_t) (BgL_tmpz00_6453);
																	}
																	{	/* Unsafe/bignumber.scm 269 */
																		long BgL_tmpz00_6456;

																		BgL_tmpz00_6456 = (long) (BgL_iz00_3523);
																		BGL_U16VSET(BgL_arg1223z00_3525,
																			BgL_tmpz00_6456, BgL_arg1225z00_3526);
																	} BUNSPEC;
															}}
															{
																long BgL_cz00_6461;
																long BgL_iz00_6459;

																BgL_iz00_6459 = (BgL_iz00_1577 + 1L);
																BgL_cz00_6461 = 0L;
																BgL_cz00_1578 = BgL_cz00_6461;
																BgL_iz00_1577 = BgL_iz00_6459;
																goto BgL_zc3z04anonymousza31449ze3z87_1579;
															}
														}
													else
														{	/* Unsafe/bignumber.scm 516 */
															{	/* Unsafe/bignumber.scm 521 */
																long BgL_arg1454z00_1585;

																BgL_arg1454z00_1585 = (BgL_wz00_1581 - 16384L);
																{	/* Unsafe/bignumber.scm 521 */
																	int BgL_iz00_3532;
																	int BgL_digitz00_3533;

																	BgL_iz00_3532 = (int) (BgL_iz00_1577);
																	BgL_digitz00_3533 =
																		(int) (BgL_arg1454z00_1585);
																	{	/* Unsafe/bignumber.scm 269 */
																		obj_t BgL_arg1223z00_3534;
																		uint16_t BgL_arg1225z00_3535;

																		BgL_arg1223z00_3534 =
																			BGL_BIGNUM_U16VECT(BgL_rz00_1559);
																		{	/* Unsafe/bignumber.scm 269 */
																			long BgL_tmpz00_6466;

																			BgL_tmpz00_6466 =
																				(long) (BgL_digitz00_3533);
																			BgL_arg1225z00_3535 =
																				(uint16_t) (BgL_tmpz00_6466);
																		}
																		{	/* Unsafe/bignumber.scm 269 */
																			long BgL_tmpz00_6469;

																			BgL_tmpz00_6469 = (long) (BgL_iz00_3532);
																			BGL_U16VSET(BgL_arg1223z00_3534,
																				BgL_tmpz00_6469, BgL_arg1225z00_3535);
																		} BUNSPEC;
															}}}
															{
																long BgL_cz00_6474;
																long BgL_iz00_6472;

																BgL_iz00_6472 = (BgL_iz00_1577 + 1L);
																BgL_cz00_6474 = 1L;
																BgL_cz00_1578 = BgL_cz00_6474;
																BgL_iz00_1577 = BgL_iz00_6472;
																goto BgL_zc3z04anonymousza31449ze3z87_1579;
															}
														}
												}
											else
												{	/* Unsafe/bignumber.scm 513 */
													if ((BgL_cz00_1578 == 0L))
														{	/* Unsafe/bignumber.scm 524 */
															return
																BGl_bignumzd2shrinkzd2zz__bignumz00
																(BgL_rz00_1559, BgL_lenxz00_1556);
														}
													else
														{	/* Unsafe/bignumber.scm 524 */
															{	/* Unsafe/bignumber.scm 527 */
																int BgL_iz00_3540;
																int BgL_digitz00_3541;

																BgL_iz00_3540 = (int) (BgL_lenxz00_1556);
																BgL_digitz00_3541 = (int) (BgL_cz00_1578);
																{	/* Unsafe/bignumber.scm 269 */
																	obj_t BgL_arg1223z00_3542;
																	uint16_t BgL_arg1225z00_3543;

																	BgL_arg1223z00_3542 =
																		BGL_BIGNUM_U16VECT(BgL_rz00_1559);
																	{	/* Unsafe/bignumber.scm 269 */
																		long BgL_tmpz00_6481;

																		BgL_tmpz00_6481 =
																			(long) (BgL_digitz00_3541);
																		BgL_arg1225z00_3543 =
																			(uint16_t) (BgL_tmpz00_6481);
																	}
																	{	/* Unsafe/bignumber.scm 269 */
																		long BgL_tmpz00_6484;

																		BgL_tmpz00_6484 = (long) (BgL_iz00_3540);
																		BGL_U16VSET(BgL_arg1223z00_3542,
																			BgL_tmpz00_6484, BgL_arg1225z00_3543);
																	} BUNSPEC;
															}}
															return BgL_rz00_1559;
														}
												}
										}
								}
							}
						}
					else
						{
							long BgL_lenyz00_6490;
							long BgL_lenxz00_6489;
							obj_t BgL_yz00_6488;
							obj_t BgL_xz00_6487;

							BgL_xz00_6487 = BgL_xz00_44;
							BgL_yz00_6488 = BgL_yz00_45;
							BgL_lenxz00_6489 = BgL_lenxz00_1551;
							BgL_lenyz00_6490 = BgL_lenyz00_1552;
							BgL_lenyz00_1557 = BgL_lenyz00_6490;
							BgL_lenxz00_1556 = BgL_lenxz00_6489;
							BgL_yz00_1555 = BgL_yz00_6488;
							BgL_xz00_1554 = BgL_xz00_6487;
							goto BgL_zc3z04anonymousza31436ze3z87_1558;
						}
				}
			}
		}

	}



/* bignum-sub-nonneg */
	obj_t BGl_bignumzd2subzd2nonnegz00zz__bignumz00(obj_t BgL_xz00_46,
		obj_t BgL_yz00_47)
	{
		{	/* Unsafe/bignumber.scm 536 */
			{
				obj_t BgL_xz00_1615;
				obj_t BgL_yz00_1616;
				long BgL_lenxz00_1617;
				long BgL_lenyz00_1618;
				obj_t BgL_rz00_1599;

				{	/* Unsafe/bignumber.scm 589 */
					long BgL_arg1461z00_1597;
					long BgL_arg1462z00_1598;

					{	/* Unsafe/bignumber.scm 261 */
						long BgL_arg1210z00_3673;

						{	/* Unsafe/bignumber.scm 261 */
							obj_t BgL_arg1212z00_3674;

							BgL_arg1212z00_3674 = BGL_BIGNUM_U16VECT(BgL_xz00_46);
							BgL_arg1210z00_3673 = BGL_HVECTOR_LENGTH(BgL_arg1212z00_3674);
						}
						{	/* Unsafe/bignumber.scm 261 */
							uint16_t BgL_xz00_3676;

							BgL_xz00_3676 = (uint16_t) (BgL_arg1210z00_3673);
							{	/* Unsafe/bignumber.scm 261 */
								long BgL_arg2271z00_3677;

								BgL_arg2271z00_3677 = (long) (BgL_xz00_3676);
								BgL_arg1461z00_1597 = (long) (BgL_arg2271z00_3677);
					}}}
					{	/* Unsafe/bignumber.scm 261 */
						long BgL_arg1210z00_3680;

						{	/* Unsafe/bignumber.scm 261 */
							obj_t BgL_arg1212z00_3681;

							BgL_arg1212z00_3681 = BGL_BIGNUM_U16VECT(BgL_yz00_47);
							BgL_arg1210z00_3680 = BGL_HVECTOR_LENGTH(BgL_arg1212z00_3681);
						}
						{	/* Unsafe/bignumber.scm 261 */
							uint16_t BgL_xz00_3683;

							BgL_xz00_3683 = (uint16_t) (BgL_arg1210z00_3680);
							{	/* Unsafe/bignumber.scm 261 */
								long BgL_arg2271z00_3684;

								BgL_arg2271z00_3684 = (long) (BgL_xz00_3683);
								BgL_arg1462z00_1598 = (long) (BgL_arg2271z00_3684);
					}}}
					BgL_xz00_1615 = BgL_xz00_46;
					BgL_yz00_1616 = BgL_yz00_47;
					BgL_lenxz00_1617 = BgL_arg1461z00_1597;
					BgL_lenyz00_1618 = BgL_arg1462z00_1598;
					{	/* Unsafe/bignumber.scm 553 */
						obj_t BgL_rz00_1620;

						{	/* Unsafe/bignumber.scm 553 */
							int BgL_lenz00_3594;

							BgL_lenz00_3594 = (int) (BgL_lenxz00_1617);
							{	/* Unsafe/bignumber.scm 259 */
								obj_t BgL_arg1208z00_3595;

								{	/* Unsafe/bignumber.scm 259 */

									BgL_arg1208z00_3595 =
										BGl_makezd2u16vectorzd2zz__srfi4z00(
										(long) (BgL_lenz00_3594), (uint16_t) (0));
								}
								BgL_rz00_1620 = bgl_make_bignum(BgL_arg1208z00_3595);
						}}
						{
							long BgL_iz00_1622;
							long BgL_bz00_1623;

							BgL_iz00_1622 = 1L;
							BgL_bz00_1623 = 0L;
						BgL_zc3z04anonymousza31475ze3z87_1624:
							if ((BgL_iz00_1622 < BgL_lenyz00_1618))
								{	/* Unsafe/bignumber.scm 558 */
									long BgL_wz00_1626;

									{	/* Unsafe/bignumber.scm 558 */
										long BgL_arg1482z00_1632;

										{	/* Unsafe/bignumber.scm 558 */
											long BgL_arg1483z00_1633;
											long BgL_arg1484z00_1634;

											{	/* Unsafe/bignumber.scm 558 */
												int BgL_iz00_3601;

												BgL_iz00_3601 = (int) (BgL_iz00_1622);
												{	/* Unsafe/bignumber.scm 267 */
													uint16_t BgL_arg1220z00_3602;

													{	/* Unsafe/bignumber.scm 267 */
														obj_t BgL_arg1221z00_3603;

														BgL_arg1221z00_3603 =
															BGL_BIGNUM_U16VECT(BgL_xz00_1615);
														{	/* Unsafe/bignumber.scm 267 */
															long BgL_tmpz00_6509;

															BgL_tmpz00_6509 = (long) (BgL_iz00_3601);
															BgL_arg1220z00_3602 =
																BGL_U16VREF(BgL_arg1221z00_3603,
																BgL_tmpz00_6509);
													}}
													{	/* Unsafe/bignumber.scm 267 */
														long BgL_arg2271z00_3605;

														BgL_arg2271z00_3605 = (long) (BgL_arg1220z00_3602);
														BgL_arg1483z00_1633 = (long) (BgL_arg2271z00_3605);
											}}}
											{	/* Unsafe/bignumber.scm 559 */
												int BgL_iz00_3608;

												BgL_iz00_3608 = (int) (BgL_iz00_1622);
												{	/* Unsafe/bignumber.scm 267 */
													uint16_t BgL_arg1220z00_3609;

													{	/* Unsafe/bignumber.scm 267 */
														obj_t BgL_arg1221z00_3610;

														BgL_arg1221z00_3610 =
															BGL_BIGNUM_U16VECT(BgL_yz00_1616);
														{	/* Unsafe/bignumber.scm 267 */
															long BgL_tmpz00_6516;

															BgL_tmpz00_6516 = (long) (BgL_iz00_3608);
															BgL_arg1220z00_3609 =
																BGL_U16VREF(BgL_arg1221z00_3610,
																BgL_tmpz00_6516);
													}}
													{	/* Unsafe/bignumber.scm 267 */
														long BgL_arg2271z00_3612;

														BgL_arg2271z00_3612 = (long) (BgL_arg1220z00_3609);
														BgL_arg1484z00_1634 = (long) (BgL_arg2271z00_3612);
											}}}
											BgL_arg1482z00_1632 =
												(BgL_arg1483z00_1633 - BgL_arg1484z00_1634);
										}
										BgL_wz00_1626 = (BgL_arg1482z00_1632 - BgL_bz00_1623);
									}
									if ((BgL_wz00_1626 < 0L))
										{	/* Unsafe/bignumber.scm 561 */
											{	/* Unsafe/bignumber.scm 563 */
												long BgL_arg1478z00_1628;

												BgL_arg1478z00_1628 = (BgL_wz00_1626 + 16384L);
												{	/* Unsafe/bignumber.scm 563 */
													int BgL_iz00_3622;
													int BgL_digitz00_3623;

													BgL_iz00_3622 = (int) (BgL_iz00_1622);
													BgL_digitz00_3623 = (int) (BgL_arg1478z00_1628);
													{	/* Unsafe/bignumber.scm 269 */
														obj_t BgL_arg1223z00_3624;
														uint16_t BgL_arg1225z00_3625;

														BgL_arg1223z00_3624 =
															BGL_BIGNUM_U16VECT(BgL_rz00_1620);
														{	/* Unsafe/bignumber.scm 269 */
															long BgL_tmpz00_6529;

															BgL_tmpz00_6529 = (long) (BgL_digitz00_3623);
															BgL_arg1225z00_3625 =
																(uint16_t) (BgL_tmpz00_6529);
														}
														{	/* Unsafe/bignumber.scm 269 */
															long BgL_tmpz00_6532;

															BgL_tmpz00_6532 = (long) (BgL_iz00_3622);
															BGL_U16VSET(BgL_arg1223z00_3624, BgL_tmpz00_6532,
																BgL_arg1225z00_3625);
														} BUNSPEC;
											}}}
											{
												long BgL_bz00_6537;
												long BgL_iz00_6535;

												BgL_iz00_6535 = (BgL_iz00_1622 + 1L);
												BgL_bz00_6537 = 1L;
												BgL_bz00_1623 = BgL_bz00_6537;
												BgL_iz00_1622 = BgL_iz00_6535;
												goto BgL_zc3z04anonymousza31475ze3z87_1624;
											}
										}
									else
										{	/* Unsafe/bignumber.scm 561 */
											{	/* Unsafe/bignumber.scm 566 */
												int BgL_iz00_3629;
												int BgL_digitz00_3630;

												BgL_iz00_3629 = (int) (BgL_iz00_1622);
												BgL_digitz00_3630 = (int) (BgL_wz00_1626);
												{	/* Unsafe/bignumber.scm 269 */
													obj_t BgL_arg1223z00_3631;
													uint16_t BgL_arg1225z00_3632;

													BgL_arg1223z00_3631 =
														BGL_BIGNUM_U16VECT(BgL_rz00_1620);
													{	/* Unsafe/bignumber.scm 269 */
														long BgL_tmpz00_6541;

														BgL_tmpz00_6541 = (long) (BgL_digitz00_3630);
														BgL_arg1225z00_3632 = (uint16_t) (BgL_tmpz00_6541);
													}
													{	/* Unsafe/bignumber.scm 269 */
														long BgL_tmpz00_6544;

														BgL_tmpz00_6544 = (long) (BgL_iz00_3629);
														BGL_U16VSET(BgL_arg1223z00_3631, BgL_tmpz00_6544,
															BgL_arg1225z00_3632);
													} BUNSPEC;
											}}
											{
												long BgL_bz00_6549;
												long BgL_iz00_6547;

												BgL_iz00_6547 = (BgL_iz00_1622 + 1L);
												BgL_bz00_6549 = 0L;
												BgL_bz00_1623 = BgL_bz00_6549;
												BgL_iz00_1622 = BgL_iz00_6547;
												goto BgL_zc3z04anonymousza31475ze3z87_1624;
											}
										}
								}
							else
								{
									long BgL_iz00_1636;
									long BgL_bz00_1637;

									BgL_iz00_1636 = BgL_iz00_1622;
									BgL_bz00_1637 = BgL_bz00_1623;
								BgL_zc3z04anonymousza31485ze3z87_1638:
									if ((BgL_iz00_1636 < BgL_lenxz00_1617))
										{	/* Unsafe/bignumber.scm 572 */
											long BgL_wz00_1640;

											{	/* Unsafe/bignumber.scm 572 */
												long BgL_arg1494z00_1646;

												{	/* Unsafe/bignumber.scm 572 */
													int BgL_iz00_3638;

													BgL_iz00_3638 = (int) (BgL_iz00_1636);
													{	/* Unsafe/bignumber.scm 267 */
														uint16_t BgL_arg1220z00_3639;

														{	/* Unsafe/bignumber.scm 267 */
															obj_t BgL_arg1221z00_3640;

															BgL_arg1221z00_3640 =
																BGL_BIGNUM_U16VECT(BgL_xz00_1615);
															{	/* Unsafe/bignumber.scm 267 */
																long BgL_tmpz00_6554;

																BgL_tmpz00_6554 = (long) (BgL_iz00_3638);
																BgL_arg1220z00_3639 =
																	BGL_U16VREF(BgL_arg1221z00_3640,
																	BgL_tmpz00_6554);
														}}
														{	/* Unsafe/bignumber.scm 267 */
															long BgL_arg2271z00_3642;

															BgL_arg2271z00_3642 =
																(long) (BgL_arg1220z00_3639);
															BgL_arg1494z00_1646 =
																(long) (BgL_arg2271z00_3642);
												}}}
												BgL_wz00_1640 = (BgL_arg1494z00_1646 - BgL_bz00_1637);
											}
											if ((BgL_wz00_1640 < 0L))
												{	/* Unsafe/bignumber.scm 573 */
													{	/* Unsafe/bignumber.scm 575 */
														long BgL_arg1488z00_1642;

														BgL_arg1488z00_1642 = (BgL_wz00_1640 + 16384L);
														{	/* Unsafe/bignumber.scm 575 */
															int BgL_iz00_3650;
															int BgL_digitz00_3651;

															BgL_iz00_3650 = (int) (BgL_iz00_1636);
															BgL_digitz00_3651 = (int) (BgL_arg1488z00_1642);
															{	/* Unsafe/bignumber.scm 269 */
																obj_t BgL_arg1223z00_3652;
																uint16_t BgL_arg1225z00_3653;

																BgL_arg1223z00_3652 =
																	BGL_BIGNUM_U16VECT(BgL_rz00_1620);
																{	/* Unsafe/bignumber.scm 269 */
																	long BgL_tmpz00_6566;

																	BgL_tmpz00_6566 = (long) (BgL_digitz00_3651);
																	BgL_arg1225z00_3653 =
																		(uint16_t) (BgL_tmpz00_6566);
																}
																{	/* Unsafe/bignumber.scm 269 */
																	long BgL_tmpz00_6569;

																	BgL_tmpz00_6569 = (long) (BgL_iz00_3650);
																	BGL_U16VSET(BgL_arg1223z00_3652,
																		BgL_tmpz00_6569, BgL_arg1225z00_3653);
																} BUNSPEC;
													}}}
													{
														long BgL_bz00_6574;
														long BgL_iz00_6572;

														BgL_iz00_6572 = (BgL_iz00_1636 + 1L);
														BgL_bz00_6574 = 1L;
														BgL_bz00_1637 = BgL_bz00_6574;
														BgL_iz00_1636 = BgL_iz00_6572;
														goto BgL_zc3z04anonymousza31485ze3z87_1638;
													}
												}
											else
												{	/* Unsafe/bignumber.scm 573 */
													{	/* Unsafe/bignumber.scm 578 */
														int BgL_iz00_3657;
														int BgL_digitz00_3658;

														BgL_iz00_3657 = (int) (BgL_iz00_1636);
														BgL_digitz00_3658 = (int) (BgL_wz00_1640);
														{	/* Unsafe/bignumber.scm 269 */
															obj_t BgL_arg1223z00_3659;
															uint16_t BgL_arg1225z00_3660;

															BgL_arg1223z00_3659 =
																BGL_BIGNUM_U16VECT(BgL_rz00_1620);
															{	/* Unsafe/bignumber.scm 269 */
																long BgL_tmpz00_6578;

																BgL_tmpz00_6578 = (long) (BgL_digitz00_3658);
																BgL_arg1225z00_3660 =
																	(uint16_t) (BgL_tmpz00_6578);
															}
															{	/* Unsafe/bignumber.scm 269 */
																long BgL_tmpz00_6581;

																BgL_tmpz00_6581 = (long) (BgL_iz00_3657);
																BGL_U16VSET(BgL_arg1223z00_3659,
																	BgL_tmpz00_6581, BgL_arg1225z00_3660);
															} BUNSPEC;
													}}
													{
														long BgL_bz00_6586;
														long BgL_iz00_6584;

														BgL_iz00_6584 = (BgL_iz00_1636 + 1L);
														BgL_bz00_6586 = 0L;
														BgL_bz00_1637 = BgL_bz00_6586;
														BgL_iz00_1636 = BgL_iz00_6584;
														goto BgL_zc3z04anonymousza31485ze3z87_1638;
													}
												}
										}
									else
										{	/* Unsafe/bignumber.scm 570 */
											if ((BgL_bz00_1637 == 0L))
												{	/* Unsafe/bignumber.scm 265 */
													obj_t BgL_arg1218z00_3666;
													uint16_t BgL_arg1219z00_3667;

													BgL_arg1218z00_3666 =
														BGL_BIGNUM_U16VECT(BgL_rz00_1620);
													BgL_arg1219z00_3667 = (uint16_t) (1L);
													BGL_U16VSET(BgL_arg1218z00_3666, 0L,
														BgL_arg1219z00_3667);
													BUNSPEC;
												}
											else
												{	/* Unsafe/bignumber.scm 581 */
													{	/* Unsafe/bignumber.scm 265 */
														obj_t BgL_arg1218z00_3670;
														uint16_t BgL_arg1219z00_3671;

														BgL_arg1218z00_3670 =
															BGL_BIGNUM_U16VECT(BgL_rz00_1620);
														BgL_arg1219z00_3671 = (uint16_t) (0L);
														BGL_U16VSET(BgL_arg1218z00_3670, 0L,
															BgL_arg1219z00_3671);
														BUNSPEC;
													}
													{	/* Unsafe/bignumber.scm 585 */
														bool_t BgL_tmpz00_6595;

														BgL_rz00_1599 = BgL_rz00_1620;
														{	/* Unsafe/bignumber.scm 539 */
															long BgL_lrz00_1601;

															{	/* Unsafe/bignumber.scm 261 */
																long BgL_arg1210z00_3562;

																{	/* Unsafe/bignumber.scm 261 */
																	obj_t BgL_arg1212z00_3563;

																	BgL_arg1212z00_3563 =
																		BGL_BIGNUM_U16VECT(BgL_rz00_1599);
																	BgL_arg1210z00_3562 =
																		BGL_HVECTOR_LENGTH(BgL_arg1212z00_3563);
																}
																{	/* Unsafe/bignumber.scm 261 */
																	uint16_t BgL_xz00_3565;

																	BgL_xz00_3565 =
																		(uint16_t) (BgL_arg1210z00_3562);
																	{	/* Unsafe/bignumber.scm 261 */
																		long BgL_arg2271z00_3566;

																		BgL_arg2271z00_3566 =
																			(long) (BgL_xz00_3565);
																		BgL_lrz00_1601 =
																			(long) (BgL_arg2271z00_3566);
															}}}
															{
																long BgL_iz00_1603;
																long BgL_cz00_1604;

																BgL_iz00_1603 = 1L;
																BgL_cz00_1604 = 0L;
															BgL_zc3z04anonymousza31464ze3z87_1605:
																if ((BgL_iz00_1603 < BgL_lrz00_1601))
																	{	/* Unsafe/bignumber.scm 543 */
																		long BgL_wz00_1607;

																		{	/* Unsafe/bignumber.scm 543 */
																			long BgL_arg1473z00_1613;

																			{	/* Unsafe/bignumber.scm 543 */
																				int BgL_iz00_3571;

																				BgL_iz00_3571 = (int) (BgL_iz00_1603);
																				{	/* Unsafe/bignumber.scm 267 */
																					uint16_t BgL_arg1220z00_3572;

																					{	/* Unsafe/bignumber.scm 267 */
																						obj_t BgL_arg1221z00_3573;

																						BgL_arg1221z00_3573 =
																							BGL_BIGNUM_U16VECT(BgL_rz00_1599);
																						{	/* Unsafe/bignumber.scm 267 */
																							long BgL_tmpz00_6605;

																							BgL_tmpz00_6605 =
																								(long) (BgL_iz00_3571);
																							BgL_arg1220z00_3572 =
																								BGL_U16VREF(BgL_arg1221z00_3573,
																								BgL_tmpz00_6605);
																					}}
																					{	/* Unsafe/bignumber.scm 267 */
																						long BgL_arg2271z00_3575;

																						BgL_arg2271z00_3575 =
																							(long) (BgL_arg1220z00_3572);
																						BgL_arg1473z00_1613 =
																							(long) (BgL_arg2271z00_3575);
																			}}}
																			BgL_wz00_1607 =
																				(BgL_arg1473z00_1613 + BgL_cz00_1604);
																		}
																		if ((0L < BgL_wz00_1607))
																			{	/* Unsafe/bignumber.scm 544 */
																				{	/* Unsafe/bignumber.scm 546 */
																					long BgL_arg1467z00_1609;

																					BgL_arg1467z00_1609 =
																						(16384L - BgL_wz00_1607);
																					{	/* Unsafe/bignumber.scm 546 */
																						int BgL_iz00_3583;
																						int BgL_digitz00_3584;

																						BgL_iz00_3583 =
																							(int) (BgL_iz00_1603);
																						BgL_digitz00_3584 =
																							(int) (BgL_arg1467z00_1609);
																						{	/* Unsafe/bignumber.scm 269 */
																							obj_t BgL_arg1223z00_3585;
																							uint16_t BgL_arg1225z00_3586;

																							BgL_arg1223z00_3585 =
																								BGL_BIGNUM_U16VECT
																								(BgL_rz00_1599);
																							{	/* Unsafe/bignumber.scm 269 */
																								long BgL_tmpz00_6617;

																								BgL_tmpz00_6617 =
																									(long) (BgL_digitz00_3584);
																								BgL_arg1225z00_3586 =
																									(uint16_t) (BgL_tmpz00_6617);
																							}
																							{	/* Unsafe/bignumber.scm 269 */
																								long BgL_tmpz00_6620;

																								BgL_tmpz00_6620 =
																									(long) (BgL_iz00_3583);
																								BGL_U16VSET(BgL_arg1223z00_3585,
																									BgL_tmpz00_6620,
																									BgL_arg1225z00_3586);
																							} BUNSPEC;
																				}}}
																				{
																					long BgL_cz00_6625;
																					long BgL_iz00_6623;

																					BgL_iz00_6623 = (BgL_iz00_1603 + 1L);
																					BgL_cz00_6625 = 1L;
																					BgL_cz00_1604 = BgL_cz00_6625;
																					BgL_iz00_1603 = BgL_iz00_6623;
																					goto
																						BgL_zc3z04anonymousza31464ze3z87_1605;
																				}
																			}
																		else
																			{	/* Unsafe/bignumber.scm 544 */
																				{	/* Unsafe/bignumber.scm 549 */
																					int BgL_iz00_3590;

																					BgL_iz00_3590 = (int) (BgL_iz00_1603);
																					{	/* Unsafe/bignumber.scm 269 */
																						obj_t BgL_arg1223z00_3591;
																						uint16_t BgL_arg1225z00_3592;

																						BgL_arg1223z00_3591 =
																							BGL_BIGNUM_U16VECT(BgL_rz00_1599);
																						BgL_arg1225z00_3592 =
																							(uint16_t) (0L);
																						{	/* Unsafe/bignumber.scm 269 */
																							long BgL_tmpz00_6629;

																							BgL_tmpz00_6629 =
																								(long) (BgL_iz00_3590);
																							BGL_U16VSET(BgL_arg1223z00_3591,
																								BgL_tmpz00_6629,
																								BgL_arg1225z00_3592);
																						} BUNSPEC;
																				}}
																				{
																					long BgL_cz00_6634;
																					long BgL_iz00_6632;

																					BgL_iz00_6632 = (BgL_iz00_1603 + 1L);
																					BgL_cz00_6634 = 0L;
																					BgL_cz00_1604 = BgL_cz00_6634;
																					BgL_iz00_1603 = BgL_iz00_6632;
																					goto
																						BgL_zc3z04anonymousza31464ze3z87_1605;
																				}
																			}
																	}
																else
																	{	/* Unsafe/bignumber.scm 541 */
																		BgL_tmpz00_6595 = ((bool_t) 0);
																	}
															}
														}
														BBOOL(BgL_tmpz00_6595);
													}
												}
										}
								}
						}
						return
							BGl_bignumzd2removezd2leadingzd2za7eroesz75zz__bignumz00
							(BgL_rz00_1620);
					}
				}
			}
		}

	}



/* bignum-sum2 */
	obj_t BGl_bignumzd2sum2zd2zz__bignumz00(obj_t BgL_xz00_48, obj_t BgL_yz00_49,
		long BgL_signzd2xzd2_50, long BgL_signzd2yzd2_51)
	{
		{	/* Unsafe/bignumber.scm 591 */
			if ((BgL_signzd2xzd2_50 == BgL_signzd2yzd2_51))
				{	/* Unsafe/bignumber.scm 600 */
					obj_t BgL_arg1497z00_1654;

					BgL_arg1497z00_1654 =
						BGl_bignumzd2addzd2nonnegz00zz__bignumz00(BgL_xz00_48, BgL_yz00_49);
					{	/* Unsafe/bignumber.scm 594 */
						bool_t BgL_test2719z00_6640;

						{	/* Unsafe/bignumber.scm 594 */
							long BgL_arg1508z00_3708;

							{	/* Unsafe/bignumber.scm 263 */
								uint16_t BgL_arg1215z00_3711;

								{	/* Unsafe/bignumber.scm 263 */
									obj_t BgL_arg1216z00_3712;

									BgL_arg1216z00_3712 = BGL_BIGNUM_U16VECT(BgL_arg1497z00_1654);
									BgL_arg1215z00_3711 = BGL_U16VREF(BgL_arg1216z00_3712, 0L);
								}
								{	/* Unsafe/bignumber.scm 263 */
									long BgL_arg2271z00_3714;

									BgL_arg2271z00_3714 = (long) (BgL_arg1215z00_3711);
									BgL_arg1508z00_3708 = (long) (BgL_arg2271z00_3714);
							}}
							BgL_test2719z00_6640 =
								(BgL_arg1508z00_3708 == BgL_signzd2xzd2_50);
						}
						if (BgL_test2719z00_6640)
							{	/* Unsafe/bignumber.scm 594 */
								BGl_bignumzd2signzd2setz12z12zz__bignumz00(BgL_arg1497z00_1654,
									(int) (1L));
							}
						else
							{	/* Unsafe/bignumber.scm 594 */
								BGl_bignumzd2signzd2setz12z12zz__bignumz00(BgL_arg1497z00_1654,
									(int) (0L));
					}}
					return
						BGl_bignumzd2removezd2leadingzd2za7eroesz75zz__bignumz00
						(BgL_arg1497z00_1654);
				}
			else
				{	/* Unsafe/bignumber.scm 601 */
					bool_t BgL_test2720z00_6651;

					{	/* Unsafe/bignumber.scm 601 */
						long BgL_arg1503z00_1660;
						long BgL_arg1504z00_1661;

						{	/* Unsafe/bignumber.scm 261 */
							long BgL_arg1210z00_3721;

							{	/* Unsafe/bignumber.scm 261 */
								obj_t BgL_arg1212z00_3722;

								BgL_arg1212z00_3722 = BGL_BIGNUM_U16VECT(BgL_xz00_48);
								BgL_arg1210z00_3721 = BGL_HVECTOR_LENGTH(BgL_arg1212z00_3722);
							}
							{	/* Unsafe/bignumber.scm 261 */
								uint16_t BgL_xz00_3724;

								BgL_xz00_3724 = (uint16_t) (BgL_arg1210z00_3721);
								{	/* Unsafe/bignumber.scm 261 */
									long BgL_arg2271z00_3725;

									BgL_arg2271z00_3725 = (long) (BgL_xz00_3724);
									BgL_arg1503z00_1660 = (long) (BgL_arg2271z00_3725);
						}}}
						{	/* Unsafe/bignumber.scm 261 */
							long BgL_arg1210z00_3728;

							{	/* Unsafe/bignumber.scm 261 */
								obj_t BgL_arg1212z00_3729;

								BgL_arg1212z00_3729 = BGL_BIGNUM_U16VECT(BgL_yz00_49);
								BgL_arg1210z00_3728 = BGL_HVECTOR_LENGTH(BgL_arg1212z00_3729);
							}
							{	/* Unsafe/bignumber.scm 261 */
								uint16_t BgL_xz00_3731;

								BgL_xz00_3731 = (uint16_t) (BgL_arg1210z00_3728);
								{	/* Unsafe/bignumber.scm 261 */
									long BgL_arg2271z00_3732;

									BgL_arg2271z00_3732 = (long) (BgL_xz00_3731);
									BgL_arg1504z00_1661 = (long) (BgL_arg2271z00_3732);
						}}}
						BgL_test2720z00_6651 = (BgL_arg1503z00_1660 < BgL_arg1504z00_1661);
					}
					if (BgL_test2720z00_6651)
						{	/* Unsafe/bignumber.scm 602 */
							obj_t BgL_arg1501z00_1658;

							BgL_arg1501z00_1658 =
								BGl_bignumzd2subzd2nonnegz00zz__bignumz00(BgL_yz00_49,
								BgL_xz00_48);
							{	/* Unsafe/bignumber.scm 594 */
								bool_t BgL_test2721z00_6664;

								{	/* Unsafe/bignumber.scm 594 */
									long BgL_arg1508z00_3737;

									{	/* Unsafe/bignumber.scm 263 */
										uint16_t BgL_arg1215z00_3740;

										{	/* Unsafe/bignumber.scm 263 */
											obj_t BgL_arg1216z00_3741;

											BgL_arg1216z00_3741 =
												BGL_BIGNUM_U16VECT(BgL_arg1501z00_1658);
											BgL_arg1215z00_3740 =
												BGL_U16VREF(BgL_arg1216z00_3741, 0L);
										}
										{	/* Unsafe/bignumber.scm 263 */
											long BgL_arg2271z00_3743;

											BgL_arg2271z00_3743 = (long) (BgL_arg1215z00_3740);
											BgL_arg1508z00_3737 = (long) (BgL_arg2271z00_3743);
									}}
									BgL_test2721z00_6664 =
										(BgL_arg1508z00_3737 == BgL_signzd2yzd2_51);
								}
								if (BgL_test2721z00_6664)
									{	/* Unsafe/bignumber.scm 594 */
										BGl_bignumzd2signzd2setz12z12zz__bignumz00
											(BgL_arg1501z00_1658, (int) (1L));
									}
								else
									{	/* Unsafe/bignumber.scm 594 */
										BGl_bignumzd2signzd2setz12z12zz__bignumz00
											(BgL_arg1501z00_1658, (int) (0L));
							}}
							return
								BGl_bignumzd2removezd2leadingzd2za7eroesz75zz__bignumz00
								(BgL_arg1501z00_1658);
						}
					else
						{	/* Unsafe/bignumber.scm 604 */
							obj_t BgL_arg1502z00_1659;

							BgL_arg1502z00_1659 =
								BGl_bignumzd2subzd2nonnegz00zz__bignumz00(BgL_xz00_48,
								BgL_yz00_49);
							{	/* Unsafe/bignumber.scm 594 */
								bool_t BgL_test2722z00_6676;

								{	/* Unsafe/bignumber.scm 594 */
									long BgL_arg1508z00_3750;

									{	/* Unsafe/bignumber.scm 263 */
										uint16_t BgL_arg1215z00_3753;

										{	/* Unsafe/bignumber.scm 263 */
											obj_t BgL_arg1216z00_3754;

											BgL_arg1216z00_3754 =
												BGL_BIGNUM_U16VECT(BgL_arg1502z00_1659);
											BgL_arg1215z00_3753 =
												BGL_U16VREF(BgL_arg1216z00_3754, 0L);
										}
										{	/* Unsafe/bignumber.scm 263 */
											long BgL_arg2271z00_3756;

											BgL_arg2271z00_3756 = (long) (BgL_arg1215z00_3753);
											BgL_arg1508z00_3750 = (long) (BgL_arg2271z00_3756);
									}}
									BgL_test2722z00_6676 =
										(BgL_arg1508z00_3750 == BgL_signzd2xzd2_50);
								}
								if (BgL_test2722z00_6676)
									{	/* Unsafe/bignumber.scm 594 */
										BGl_bignumzd2signzd2setz12z12zz__bignumz00
											(BgL_arg1502z00_1659, (int) (1L));
									}
								else
									{	/* Unsafe/bignumber.scm 594 */
										BGl_bignumzd2signzd2setz12z12zz__bignumz00
											(BgL_arg1502z00_1659, (int) (0L));
							}}
							return
								BGl_bignumzd2removezd2leadingzd2za7eroesz75zz__bignumz00
								(BgL_arg1502z00_1659);
						}
				}
		}

	}



/* $$+bx */
	BGL_EXPORTED_DEF obj_t bgl_bignum_add(obj_t BgL_xz00_52, obj_t BgL_yz00_53)
	{
		{	/* Unsafe/bignumber.scm 606 */
			{	/* Unsafe/bignumber.scm 607 */
				long BgL_arg1510z00_3762;
				long BgL_arg1511z00_3763;

				{	/* Unsafe/bignumber.scm 263 */
					uint16_t BgL_arg1215z00_3765;

					{	/* Unsafe/bignumber.scm 263 */
						obj_t BgL_arg1216z00_3766;

						BgL_arg1216z00_3766 = BGL_BIGNUM_U16VECT(BgL_xz00_52);
						BgL_arg1215z00_3765 = BGL_U16VREF(BgL_arg1216z00_3766, 0L);
					}
					{	/* Unsafe/bignumber.scm 263 */
						long BgL_arg2271z00_3768;

						BgL_arg2271z00_3768 = (long) (BgL_arg1215z00_3765);
						BgL_arg1510z00_3762 = (long) (BgL_arg2271z00_3768);
				}}
				{	/* Unsafe/bignumber.scm 263 */
					uint16_t BgL_arg1215z00_3771;

					{	/* Unsafe/bignumber.scm 263 */
						obj_t BgL_arg1216z00_3772;

						BgL_arg1216z00_3772 = BGL_BIGNUM_U16VECT(BgL_yz00_53);
						BgL_arg1215z00_3771 = BGL_U16VREF(BgL_arg1216z00_3772, 0L);
					}
					{	/* Unsafe/bignumber.scm 263 */
						long BgL_arg2271z00_3774;

						BgL_arg2271z00_3774 = (long) (BgL_arg1215z00_3771);
						BgL_arg1511z00_3763 = (long) (BgL_arg2271z00_3774);
				}}
				return
					BGl_bignumzd2sum2zd2zz__bignumz00(BgL_xz00_52, BgL_yz00_53,
					BgL_arg1510z00_3762, BgL_arg1511z00_3763);
			}
		}

	}



/* &$$+bx */
	obj_t BGl_z62z42z42zb2bxzd0zz__bignumz00(obj_t BgL_envz00_5529,
		obj_t BgL_xz00_5530, obj_t BgL_yz00_5531)
	{
		{	/* Unsafe/bignumber.scm 606 */
			{	/* Unsafe/bignumber.scm 607 */
				obj_t BgL_auxz00_6703;
				obj_t BgL_auxz00_6696;

				if (BIGNUMP(BgL_yz00_5531))
					{	/* Unsafe/bignumber.scm 607 */
						BgL_auxz00_6703 = BgL_yz00_5531;
					}
				else
					{
						obj_t BgL_auxz00_6706;

						BgL_auxz00_6706 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
							BINT(20617L), BGl_string2497z00zz__bignumz00,
							BGl_string2491z00zz__bignumz00, BgL_yz00_5531);
						FAILURE(BgL_auxz00_6706, BFALSE, BFALSE);
					}
				if (BIGNUMP(BgL_xz00_5530))
					{	/* Unsafe/bignumber.scm 607 */
						BgL_auxz00_6696 = BgL_xz00_5530;
					}
				else
					{
						obj_t BgL_auxz00_6699;

						BgL_auxz00_6699 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
							BINT(20617L), BGl_string2497z00zz__bignumz00,
							BGl_string2491z00zz__bignumz00, BgL_xz00_5530);
						FAILURE(BgL_auxz00_6699, BFALSE, BFALSE);
					}
				return bgl_bignum_add(BgL_auxz00_6696, BgL_auxz00_6703);
			}
		}

	}



/* $$-bx */
	BGL_EXPORTED_DEF obj_t bgl_bignum_sub(obj_t BgL_xz00_54, obj_t BgL_yz00_55)
	{
		{	/* Unsafe/bignumber.scm 609 */
			{	/* Unsafe/bignumber.scm 610 */
				long BgL_arg1513z00_3776;
				long BgL_arg1514z00_3777;

				{	/* Unsafe/bignumber.scm 263 */
					uint16_t BgL_arg1215z00_3780;

					{	/* Unsafe/bignumber.scm 263 */
						obj_t BgL_arg1216z00_3781;

						BgL_arg1216z00_3781 = BGL_BIGNUM_U16VECT(BgL_xz00_54);
						BgL_arg1215z00_3780 = BGL_U16VREF(BgL_arg1216z00_3781, 0L);
					}
					{	/* Unsafe/bignumber.scm 263 */
						long BgL_arg2271z00_3783;

						BgL_arg2271z00_3783 = (long) (BgL_arg1215z00_3780);
						BgL_arg1513z00_3776 = (long) (BgL_arg2271z00_3783);
				}}
				{	/* Unsafe/bignumber.scm 610 */
					long BgL_arg1516z00_3778;

					{	/* Unsafe/bignumber.scm 263 */
						uint16_t BgL_arg1215z00_3786;

						{	/* Unsafe/bignumber.scm 263 */
							obj_t BgL_arg1216z00_3787;

							BgL_arg1216z00_3787 = BGL_BIGNUM_U16VECT(BgL_yz00_55);
							BgL_arg1215z00_3786 = BGL_U16VREF(BgL_arg1216z00_3787, 0L);
						}
						{	/* Unsafe/bignumber.scm 263 */
							long BgL_arg2271z00_3789;

							BgL_arg2271z00_3789 = (long) (BgL_arg1215z00_3786);
							BgL_arg1516z00_3778 = (long) (BgL_arg2271z00_3789);
					}}
					BgL_arg1514z00_3777 = (1L - BgL_arg1516z00_3778);
				}
				return
					BGl_bignumzd2sum2zd2zz__bignumz00(BgL_xz00_54, BgL_yz00_55,
					BgL_arg1513z00_3776, BgL_arg1514z00_3777);
			}
		}

	}



/* &$$-bx */
	obj_t BGl_z62z42z42zd2bxzb0zz__bignumz00(obj_t BgL_envz00_5532,
		obj_t BgL_xz00_5533, obj_t BgL_yz00_5534)
	{
		{	/* Unsafe/bignumber.scm 609 */
			{	/* Unsafe/bignumber.scm 610 */
				obj_t BgL_auxz00_6728;
				obj_t BgL_auxz00_6721;

				if (BIGNUMP(BgL_yz00_5534))
					{	/* Unsafe/bignumber.scm 610 */
						BgL_auxz00_6728 = BgL_yz00_5534;
					}
				else
					{
						obj_t BgL_auxz00_6731;

						BgL_auxz00_6731 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
							BINT(20692L), BGl_string2498z00zz__bignumz00,
							BGl_string2491z00zz__bignumz00, BgL_yz00_5534);
						FAILURE(BgL_auxz00_6731, BFALSE, BFALSE);
					}
				if (BIGNUMP(BgL_xz00_5533))
					{	/* Unsafe/bignumber.scm 610 */
						BgL_auxz00_6721 = BgL_xz00_5533;
					}
				else
					{
						obj_t BgL_auxz00_6724;

						BgL_auxz00_6724 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
							BINT(20692L), BGl_string2498z00zz__bignumz00,
							BGl_string2491z00zz__bignumz00, BgL_xz00_5533);
						FAILURE(BgL_auxz00_6724, BFALSE, BFALSE);
					}
				return bgl_bignum_sub(BgL_auxz00_6721, BgL_auxz00_6728);
			}
		}

	}



/* $$negbx */
	BGL_EXPORTED_DEF obj_t bgl_bignum_neg(obj_t BgL_xz00_56)
	{
		{	/* Unsafe/bignumber.scm 612 */
			{	/* Unsafe/bignumber.scm 613 */
				obj_t BgL_res2330z00_3810;

				{	/* Unsafe/bignumber.scm 613 */
					obj_t BgL_xz00_3792;

					BgL_xz00_3792 = BGl_bignumzd2za7eroz75zz__bignumz00;
					{	/* Unsafe/bignumber.scm 610 */
						long BgL_arg1513z00_3794;
						long BgL_arg1514z00_3795;

						{	/* Unsafe/bignumber.scm 263 */
							uint16_t BgL_arg1215z00_3798;

							{	/* Unsafe/bignumber.scm 263 */
								obj_t BgL_arg1216z00_3799;

								BgL_arg1216z00_3799 = BGL_BIGNUM_U16VECT(BgL_xz00_3792);
								BgL_arg1215z00_3798 = BGL_U16VREF(BgL_arg1216z00_3799, 0L);
							}
							{	/* Unsafe/bignumber.scm 263 */
								long BgL_arg2271z00_3801;

								BgL_arg2271z00_3801 = (long) (BgL_arg1215z00_3798);
								BgL_arg1513z00_3794 = (long) (BgL_arg2271z00_3801);
						}}
						{	/* Unsafe/bignumber.scm 610 */
							long BgL_arg1516z00_3796;

							{	/* Unsafe/bignumber.scm 263 */
								uint16_t BgL_arg1215z00_3804;

								{	/* Unsafe/bignumber.scm 263 */
									obj_t BgL_arg1216z00_3805;

									BgL_arg1216z00_3805 = BGL_BIGNUM_U16VECT(BgL_xz00_56);
									BgL_arg1215z00_3804 = BGL_U16VREF(BgL_arg1216z00_3805, 0L);
								}
								{	/* Unsafe/bignumber.scm 263 */
									long BgL_arg2271z00_3807;

									BgL_arg2271z00_3807 = (long) (BgL_arg1215z00_3804);
									BgL_arg1516z00_3796 = (long) (BgL_arg2271z00_3807);
							}}
							BgL_arg1514z00_3795 = (1L - BgL_arg1516z00_3796);
						}
						BgL_res2330z00_3810 =
							BGl_bignumzd2sum2zd2zz__bignumz00(BgL_xz00_3792, BgL_xz00_56,
							BgL_arg1513z00_3794, BgL_arg1514z00_3795);
				}}
				return BgL_res2330z00_3810;
			}
		}

	}



/* &$$negbx */
	obj_t BGl_z62z42z42negbxz62zz__bignumz00(obj_t BgL_envz00_5535,
		obj_t BgL_xz00_5536)
	{
		{	/* Unsafe/bignumber.scm 612 */
			{	/* Unsafe/bignumber.scm 613 */
				obj_t BgL_auxz00_6746;

				if (BIGNUMP(BgL_xz00_5536))
					{	/* Unsafe/bignumber.scm 613 */
						BgL_auxz00_6746 = BgL_xz00_5536;
					}
				else
					{
						obj_t BgL_auxz00_6749;

						BgL_auxz00_6749 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
							BINT(20758L), BGl_string2499z00zz__bignumz00,
							BGl_string2491z00zz__bignumz00, BgL_xz00_5536);
						FAILURE(BgL_auxz00_6749, BFALSE, BFALSE);
					}
				return bgl_bignum_neg(BgL_auxz00_6746);
			}
		}

	}



/* $$*bx */
	BGL_EXPORTED_DEF obj_t bgl_bignum_mul(obj_t BgL_xz00_60, obj_t BgL_yz00_61)
	{
		{	/* Unsafe/bignumber.scm 634 */
			{
				obj_t BgL_xz00_1701;
				obj_t BgL_yz00_1702;
				long BgL_lenxz00_1703;
				long BgL_lenyz00_1704;

				{	/* Unsafe/bignumber.scm 665 */
					obj_t BgL_arg1530z00_1698;

					{	/* Unsafe/bignumber.scm 665 */
						long BgL_arg1531z00_1699;
						long BgL_arg1535z00_1700;

						{	/* Unsafe/bignumber.scm 261 */
							long BgL_arg1210z00_3891;

							{	/* Unsafe/bignumber.scm 261 */
								obj_t BgL_arg1212z00_3892;

								BgL_arg1212z00_3892 = BGL_BIGNUM_U16VECT(BgL_xz00_60);
								BgL_arg1210z00_3891 = BGL_HVECTOR_LENGTH(BgL_arg1212z00_3892);
							}
							{	/* Unsafe/bignumber.scm 261 */
								uint16_t BgL_xz00_3894;

								BgL_xz00_3894 = (uint16_t) (BgL_arg1210z00_3891);
								{	/* Unsafe/bignumber.scm 261 */
									long BgL_arg2271z00_3895;

									BgL_arg2271z00_3895 = (long) (BgL_xz00_3894);
									BgL_arg1531z00_1699 = (long) (BgL_arg2271z00_3895);
						}}}
						{	/* Unsafe/bignumber.scm 261 */
							long BgL_arg1210z00_3898;

							{	/* Unsafe/bignumber.scm 261 */
								obj_t BgL_arg1212z00_3899;

								BgL_arg1212z00_3899 = BGL_BIGNUM_U16VECT(BgL_yz00_61);
								BgL_arg1210z00_3898 = BGL_HVECTOR_LENGTH(BgL_arg1212z00_3899);
							}
							{	/* Unsafe/bignumber.scm 261 */
								uint16_t BgL_xz00_3901;

								BgL_xz00_3901 = (uint16_t) (BgL_arg1210z00_3898);
								{	/* Unsafe/bignumber.scm 261 */
									long BgL_arg2271z00_3902;

									BgL_arg2271z00_3902 = (long) (BgL_xz00_3901);
									BgL_arg1535z00_1700 = (long) (BgL_arg2271z00_3902);
						}}}
						BgL_xz00_1701 = BgL_xz00_60;
						BgL_yz00_1702 = BgL_yz00_61;
						BgL_lenxz00_1703 = BgL_arg1531z00_1699;
						BgL_lenyz00_1704 = BgL_arg1535z00_1700;
						{	/* Unsafe/bignumber.scm 637 */
							obj_t BgL_rz00_1706;

							{	/* Unsafe/bignumber.scm 637 */
								long BgL_arg1565z00_1739;

								BgL_arg1565z00_1739 =
									((BgL_lenxz00_1703 + BgL_lenyz00_1704) - 1L);
								{	/* Unsafe/bignumber.scm 637 */
									int BgL_lenz00_3814;

									BgL_lenz00_3814 = (int) (BgL_arg1565z00_1739);
									{	/* Unsafe/bignumber.scm 259 */
										obj_t BgL_arg1208z00_3815;

										{	/* Unsafe/bignumber.scm 259 */

											BgL_arg1208z00_3815 =
												BGl_makezd2u16vectorzd2zz__srfi4z00(
												(long) (BgL_lenz00_3814), (uint16_t) (0));
										}
										BgL_rz00_1706 = bgl_make_bignum(BgL_arg1208z00_3815);
							}}}
							{	/* Unsafe/bignumber.scm 639 */
								bool_t BgL_test2728z00_6770;

								{	/* Unsafe/bignumber.scm 639 */
									long BgL_arg1543z00_1710;
									long BgL_arg1544z00_1711;

									{	/* Unsafe/bignumber.scm 263 */
										uint16_t BgL_arg1215z00_3819;

										{	/* Unsafe/bignumber.scm 263 */
											obj_t BgL_arg1216z00_3820;

											BgL_arg1216z00_3820 = BGL_BIGNUM_U16VECT(BgL_xz00_1701);
											BgL_arg1215z00_3819 =
												BGL_U16VREF(BgL_arg1216z00_3820, 0L);
										}
										{	/* Unsafe/bignumber.scm 263 */
											long BgL_arg2271z00_3822;

											BgL_arg2271z00_3822 = (long) (BgL_arg1215z00_3819);
											BgL_arg1543z00_1710 = (long) (BgL_arg2271z00_3822);
									}}
									{	/* Unsafe/bignumber.scm 263 */
										uint16_t BgL_arg1215z00_3825;

										{	/* Unsafe/bignumber.scm 263 */
											obj_t BgL_arg1216z00_3826;

											BgL_arg1216z00_3826 = BGL_BIGNUM_U16VECT(BgL_yz00_1702);
											BgL_arg1215z00_3825 =
												BGL_U16VREF(BgL_arg1216z00_3826, 0L);
										}
										{	/* Unsafe/bignumber.scm 263 */
											long BgL_arg2271z00_3828;

											BgL_arg2271z00_3828 = (long) (BgL_arg1215z00_3825);
											BgL_arg1544z00_1711 = (long) (BgL_arg2271z00_3828);
									}}
									BgL_test2728z00_6770 =
										(BgL_arg1543z00_1710 == BgL_arg1544z00_1711);
								}
								if (BgL_test2728z00_6770)
									{	/* Unsafe/bignumber.scm 265 */
										obj_t BgL_arg1218z00_3834;
										uint16_t BgL_arg1219z00_3835;

										BgL_arg1218z00_3834 = BGL_BIGNUM_U16VECT(BgL_rz00_1706);
										BgL_arg1219z00_3835 = (uint16_t) (1L);
										BGL_U16VSET(BgL_arg1218z00_3834, 0L, BgL_arg1219z00_3835);
										BUNSPEC;
									}
								else
									{	/* Unsafe/bignumber.scm 265 */
										obj_t BgL_arg1218z00_3838;
										uint16_t BgL_arg1219z00_3839;

										BgL_arg1218z00_3838 = BGL_BIGNUM_U16VECT(BgL_rz00_1706);
										BgL_arg1219z00_3839 = (uint16_t) (0L);
										BGL_U16VSET(BgL_arg1218z00_3838, 0L, BgL_arg1219z00_3839);
										BUNSPEC;
									}
							}
							{
								long BgL_jz00_1713;

								BgL_jz00_1713 = 1L;
							BgL_zc3z04anonymousza31545ze3z87_1714:
								if ((BgL_jz00_1713 < BgL_lenyz00_1704))
									{	/* Unsafe/bignumber.scm 646 */
										long BgL_dz00_1716;

										{	/* Unsafe/bignumber.scm 646 */
											int BgL_iz00_3843;

											BgL_iz00_3843 = (int) (BgL_jz00_1713);
											{	/* Unsafe/bignumber.scm 267 */
												uint16_t BgL_arg1220z00_3844;

												{	/* Unsafe/bignumber.scm 267 */
													obj_t BgL_arg1221z00_3845;

													BgL_arg1221z00_3845 =
														BGL_BIGNUM_U16VECT(BgL_yz00_1702);
													{	/* Unsafe/bignumber.scm 267 */
														long BgL_tmpz00_6790;

														BgL_tmpz00_6790 = (long) (BgL_iz00_3843);
														BgL_arg1220z00_3844 =
															BGL_U16VREF(BgL_arg1221z00_3845, BgL_tmpz00_6790);
												}}
												{	/* Unsafe/bignumber.scm 267 */
													long BgL_arg2271z00_3847;

													BgL_arg2271z00_3847 = (long) (BgL_arg1220z00_3844);
													BgL_dz00_1716 = (long) (BgL_arg2271z00_3847);
										}}}
										if ((BgL_dz00_1716 == 0L))
											{
												long BgL_jz00_6797;

												BgL_jz00_6797 = (BgL_jz00_1713 + 1L);
												BgL_jz00_1713 = BgL_jz00_6797;
												goto BgL_zc3z04anonymousza31545ze3z87_1714;
											}
										else
											{
												long BgL_iz00_1720;
												long BgL_kz00_1721;
												long BgL_cz00_1722;

												BgL_iz00_1720 = 1L;
												BgL_kz00_1721 = BgL_jz00_1713;
												BgL_cz00_1722 = 0L;
											BgL_zc3z04anonymousza31550ze3z87_1723:
												if ((BgL_iz00_1720 < BgL_lenxz00_1703))
													{	/* Unsafe/bignumber.scm 652 */
														long BgL_wz00_1725;

														{	/* Unsafe/bignumber.scm 652 */
															long BgL_arg1558z00_1732;
															long BgL_arg1559z00_1733;

															{	/* Unsafe/bignumber.scm 652 */
																long BgL_arg1561z00_1734;

																{	/* Unsafe/bignumber.scm 652 */
																	int BgL_iz00_3854;

																	BgL_iz00_3854 = (int) (BgL_kz00_1721);
																	{	/* Unsafe/bignumber.scm 267 */
																		uint16_t BgL_arg1220z00_3855;

																		{	/* Unsafe/bignumber.scm 267 */
																			obj_t BgL_arg1221z00_3856;

																			BgL_arg1221z00_3856 =
																				BGL_BIGNUM_U16VECT(BgL_rz00_1706);
																			{	/* Unsafe/bignumber.scm 267 */
																				long BgL_tmpz00_6803;

																				BgL_tmpz00_6803 =
																					(long) (BgL_iz00_3854);
																				BgL_arg1220z00_3855 =
																					BGL_U16VREF(BgL_arg1221z00_3856,
																					BgL_tmpz00_6803);
																		}}
																		{	/* Unsafe/bignumber.scm 267 */
																			long BgL_arg2271z00_3858;

																			BgL_arg2271z00_3858 =
																				(long) (BgL_arg1220z00_3855);
																			BgL_arg1561z00_1734 =
																				(long) (BgL_arg2271z00_3858);
																}}}
																BgL_arg1558z00_1732 =
																	(BgL_arg1561z00_1734 + BgL_cz00_1722);
															}
															{	/* Unsafe/bignumber.scm 653 */
																long BgL_arg1562z00_1735;

																{	/* Unsafe/bignumber.scm 653 */
																	int BgL_iz00_3863;

																	BgL_iz00_3863 = (int) (BgL_iz00_1720);
																	{	/* Unsafe/bignumber.scm 267 */
																		uint16_t BgL_arg1220z00_3864;

																		{	/* Unsafe/bignumber.scm 267 */
																			obj_t BgL_arg1221z00_3865;

																			BgL_arg1221z00_3865 =
																				BGL_BIGNUM_U16VECT(BgL_xz00_1701);
																			{	/* Unsafe/bignumber.scm 267 */
																				long BgL_tmpz00_6811;

																				BgL_tmpz00_6811 =
																					(long) (BgL_iz00_3863);
																				BgL_arg1220z00_3864 =
																					BGL_U16VREF(BgL_arg1221z00_3865,
																					BgL_tmpz00_6811);
																		}}
																		{	/* Unsafe/bignumber.scm 267 */
																			long BgL_arg2271z00_3867;

																			BgL_arg2271z00_3867 =
																				(long) (BgL_arg1220z00_3864);
																			BgL_arg1562z00_1735 =
																				(long) (BgL_arg2271z00_3867);
																}}}
																BgL_arg1559z00_1733 =
																	(BgL_arg1562z00_1735 * BgL_dz00_1716);
															}
															BgL_wz00_1725 =
																(BgL_arg1558z00_1732 + BgL_arg1559z00_1733);
														}
														{	/* Unsafe/bignumber.scm 654 */
															obj_t BgL_arg1552z00_1726;

															BgL_arg1552z00_1726 =
																BGl_moduloz00zz__r4_numbers_6_5_fixnumz00(BINT
																(BgL_wz00_1725), BINT(16384L));
															{	/* Unsafe/bignumber.scm 654 */
																int BgL_iz00_3874;
																int BgL_digitz00_3875;

																BgL_iz00_3874 = (int) (BgL_kz00_1721);
																BgL_digitz00_3875 = CINT(BgL_arg1552z00_1726);
																{	/* Unsafe/bignumber.scm 269 */
																	obj_t BgL_arg1223z00_3876;
																	uint16_t BgL_arg1225z00_3877;

																	BgL_arg1223z00_3876 =
																		BGL_BIGNUM_U16VECT(BgL_rz00_1706);
																	{	/* Unsafe/bignumber.scm 269 */
																		long BgL_tmpz00_6824;

																		BgL_tmpz00_6824 =
																			(long) (BgL_digitz00_3875);
																		BgL_arg1225z00_3877 =
																			(uint16_t) (BgL_tmpz00_6824);
																	}
																	{	/* Unsafe/bignumber.scm 269 */
																		long BgL_tmpz00_6827;

																		BgL_tmpz00_6827 = (long) (BgL_iz00_3874);
																		BGL_U16VSET(BgL_arg1223z00_3876,
																			BgL_tmpz00_6827, BgL_arg1225z00_3877);
																	} BUNSPEC;
														}}}
														{	/* Unsafe/bignumber.scm 655 */
															long BgL_arg1554z00_1728;
															long BgL_arg1555z00_1729;
															long BgL_arg1556z00_1730;

															BgL_arg1554z00_1728 = (BgL_iz00_1720 + 1L);
															BgL_arg1555z00_1729 = (BgL_kz00_1721 + 1L);
															BgL_arg1556z00_1730 = (BgL_wz00_1725 / 16384L);
															{
																long BgL_cz00_6835;
																long BgL_kz00_6834;
																long BgL_iz00_6833;

																BgL_iz00_6833 = BgL_arg1554z00_1728;
																BgL_kz00_6834 = BgL_arg1555z00_1729;
																BgL_cz00_6835 = BgL_arg1556z00_1730;
																BgL_cz00_1722 = BgL_cz00_6835;
																BgL_kz00_1721 = BgL_kz00_6834;
																BgL_iz00_1720 = BgL_iz00_6833;
																goto BgL_zc3z04anonymousza31550ze3z87_1723;
															}
														}
													}
												else
													{	/* Unsafe/bignumber.scm 650 */
														{	/* Unsafe/bignumber.scm 660 */
															int BgL_iz00_3884;
															int BgL_digitz00_3885;

															BgL_iz00_3884 = (int) (BgL_kz00_1721);
															BgL_digitz00_3885 = (int) (BgL_cz00_1722);
															{	/* Unsafe/bignumber.scm 269 */
																obj_t BgL_arg1223z00_3886;
																uint16_t BgL_arg1225z00_3887;

																BgL_arg1223z00_3886 =
																	BGL_BIGNUM_U16VECT(BgL_rz00_1706);
																{	/* Unsafe/bignumber.scm 269 */
																	long BgL_tmpz00_6839;

																	BgL_tmpz00_6839 = (long) (BgL_digitz00_3885);
																	BgL_arg1225z00_3887 =
																		(uint16_t) (BgL_tmpz00_6839);
																}
																{	/* Unsafe/bignumber.scm 269 */
																	long BgL_tmpz00_6842;

																	BgL_tmpz00_6842 = (long) (BgL_iz00_3884);
																	BGL_U16VSET(BgL_arg1223z00_3886,
																		BgL_tmpz00_6842, BgL_arg1225z00_3887);
																} BUNSPEC;
														}}
														{
															long BgL_jz00_6845;

															BgL_jz00_6845 = (BgL_jz00_1713 + 1L);
															BgL_jz00_1713 = BgL_jz00_6845;
															goto BgL_zc3z04anonymousza31545ze3z87_1714;
														}
													}
											}
									}
								else
									{	/* Unsafe/bignumber.scm 644 */
										((bool_t) 0);
									}
							}
							BgL_arg1530z00_1698 =
								BGl_bignumzd2removezd2leadingzd2za7eroesz75zz__bignumz00
								(BgL_rz00_1706);
						}
					}
					return BgL_arg1530z00_1698;
				}
			}
		}

	}



/* &$$*bx */
	obj_t BGl_z62z42z42za2bxzc0zz__bignumz00(obj_t BgL_envz00_5537,
		obj_t BgL_xz00_5538, obj_t BgL_yz00_5539)
	{
		{	/* Unsafe/bignumber.scm 634 */
			{	/* Unsafe/bignumber.scm 637 */
				obj_t BgL_auxz00_6855;
				obj_t BgL_auxz00_6848;

				if (BIGNUMP(BgL_yz00_5539))
					{	/* Unsafe/bignumber.scm 637 */
						BgL_auxz00_6855 = BgL_yz00_5539;
					}
				else
					{
						obj_t BgL_auxz00_6858;

						BgL_auxz00_6858 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
							BINT(21436L), BGl_string2500z00zz__bignumz00,
							BGl_string2491z00zz__bignumz00, BgL_yz00_5539);
						FAILURE(BgL_auxz00_6858, BFALSE, BFALSE);
					}
				if (BIGNUMP(BgL_xz00_5538))
					{	/* Unsafe/bignumber.scm 637 */
						BgL_auxz00_6848 = BgL_xz00_5538;
					}
				else
					{
						obj_t BgL_auxz00_6851;

						BgL_auxz00_6851 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
							BINT(21436L), BGl_string2500z00zz__bignumz00,
							BGl_string2491z00zz__bignumz00, BgL_xz00_5538);
						FAILURE(BgL_auxz00_6851, BFALSE, BFALSE);
					}
				return bgl_bignum_mul(BgL_auxz00_6848, BgL_auxz00_6855);
			}
		}

	}



/* $$exptbx */
	BGL_EXPORTED_DEF obj_t bgl_bignum_expt(obj_t BgL_xz00_63, obj_t BgL_yz00_64)
	{
		{	/* Unsafe/bignumber.scm 678 */
		bgl_bignum_expt:
			{	/* Unsafe/bignumber.scm 680 */
				bool_t BgL_test2734z00_6863;

				{	/* Unsafe/bignumber.scm 468 */
					long BgL_arg1427z00_3905;

					{	/* Unsafe/bignumber.scm 261 */
						long BgL_arg1210z00_3907;

						{	/* Unsafe/bignumber.scm 261 */
							obj_t BgL_arg1212z00_3908;

							BgL_arg1212z00_3908 = BGL_BIGNUM_U16VECT(BgL_yz00_64);
							BgL_arg1210z00_3907 = BGL_HVECTOR_LENGTH(BgL_arg1212z00_3908);
						}
						{	/* Unsafe/bignumber.scm 261 */
							uint16_t BgL_xz00_3910;

							BgL_xz00_3910 = (uint16_t) (BgL_arg1210z00_3907);
							{	/* Unsafe/bignumber.scm 261 */
								long BgL_arg2271z00_3911;

								BgL_arg2271z00_3911 = (long) (BgL_xz00_3910);
								BgL_arg1427z00_3905 = (long) (BgL_arg2271z00_3911);
					}}}
					BgL_test2734z00_6863 = (BgL_arg1427z00_3905 == 1L);
				}
				if (BgL_test2734z00_6863)
					{	/* Unsafe/bignumber.scm 680 */
						obj_t BgL_res2331z00_3922;

						{	/* Unsafe/bignumber.scm 415 */
							bool_t BgL_test2735z00_6870;

							if ((1L < -16L))
								{	/* Unsafe/bignumber.scm 415 */
									BgL_test2735z00_6870 = ((bool_t) 1);
								}
							else
								{	/* Unsafe/bignumber.scm 415 */
									BgL_test2735z00_6870 = (16L < 1L);
								}
							if (BgL_test2735z00_6870)
								{	/* Unsafe/bignumber.scm 415 */
									BgL_res2331z00_3922 =
										BGl_z42z42fixnumzd2ze3bignumzd2freshze3zz__bignumz00(1L);
								}
							else
								{	/* Unsafe/bignumber.scm 415 */
									BgL_res2331z00_3922 =
										VECTOR_REF(BGl_preallocatedzd2bignumszd2zz__bignumz00,
										(1L + 16L));
								}
						}
						return BgL_res2331z00_3922;
					}
				else
					{	/* Unsafe/bignumber.scm 681 */
						bool_t BgL_test2737z00_6877;

						{	/* Unsafe/bignumber.scm 681 */
							bool_t BgL_res2332z00_3935;

							{	/* Unsafe/bignumber.scm 477 */
								bool_t BgL__ortest_1046z00_3924;

								BgL__ortest_1046z00_3924 =
									(BGl_bignumzd2lengthzd2zz__bignumz00(BgL_yz00_64) == 1L);
								if (BgL__ortest_1046z00_3924)
									{	/* Unsafe/bignumber.scm 477 */
										BgL_res2332z00_3935 = BgL__ortest_1046z00_3924;
									}
								else
									{	/* Unsafe/bignumber.scm 477 */
										long BgL_arg1431z00_3925;

										{	/* Unsafe/bignumber.scm 267 */
											uint16_t BgL_arg1220z00_3930;

											{	/* Unsafe/bignumber.scm 267 */
												obj_t BgL_arg1221z00_3931;

												BgL_arg1221z00_3931 = BGL_BIGNUM_U16VECT(BgL_yz00_64);
												BgL_arg1220z00_3930 =
													BGL_U16VREF(BgL_arg1221z00_3931, 1L);
											}
											{	/* Unsafe/bignumber.scm 267 */
												long BgL_arg2271z00_3933;

												BgL_arg2271z00_3933 = (long) (BgL_arg1220z00_3930);
												BgL_arg1431z00_3925 = (long) (BgL_arg2271z00_3933);
										}}
										BgL_res2332z00_3935 =
											BGl_evenzf3zf3zz__r4_numbers_6_5_fixnumz00(BINT
											(BgL_arg1431z00_3925));
							}}
							BgL_test2737z00_6877 = BgL_res2332z00_3935;
						}
						if (BgL_test2737z00_6877)
							{	/* Unsafe/bignumber.scm 681 */
								obj_t BgL_arg1578z00_1756;
								obj_t BgL_arg1579z00_1757;

								BgL_arg1578z00_1756 = bgl_bignum_mul(BgL_xz00_63, BgL_xz00_63);
								{	/* Unsafe/bignumber.scm 681 */
									obj_t BgL_arg1580z00_1758;

									{	/* Unsafe/bignumber.scm 681 */
										obj_t BgL_res2333z00_3944;

										{	/* Unsafe/bignumber.scm 415 */
											bool_t BgL_test2739z00_6888;

											if ((2L < -16L))
												{	/* Unsafe/bignumber.scm 415 */
													BgL_test2739z00_6888 = ((bool_t) 1);
												}
											else
												{	/* Unsafe/bignumber.scm 415 */
													BgL_test2739z00_6888 = (16L < 2L);
												}
											if (BgL_test2739z00_6888)
												{	/* Unsafe/bignumber.scm 415 */
													BgL_res2333z00_3944 =
														BGl_z42z42fixnumzd2ze3bignumzd2freshze3zz__bignumz00
														(2L);
												}
											else
												{	/* Unsafe/bignumber.scm 415 */
													BgL_res2333z00_3944 =
														VECTOR_REF
														(BGl_preallocatedzd2bignumszd2zz__bignumz00,
														(2L + 16L));
												}
										}
										BgL_arg1580z00_1758 = BgL_res2333z00_3944;
									}
									{	/* Unsafe/bignumber.scm 681 */
										obj_t BgL_res2334z00_3950;

										{	/* Unsafe/bignumber.scm 859 */
											obj_t BgL_arg1756z00_3947;

											{	/* Unsafe/bignumber.scm 859 */
												obj_t BgL_arg1757z00_3948;

												BgL_arg1757z00_3948 =
													BGl_bignumzd2divzd2zz__bignumz00(BgL_yz00_64,
													BgL_arg1580z00_1758);
												BgL_arg1756z00_3947 =
													CAR(((obj_t) BgL_arg1757z00_3948));
											}
											BgL_res2334z00_3950 = BgL_arg1756z00_3947;
										}
										BgL_arg1579z00_1757 = BgL_res2334z00_3950;
									}
								}
								{
									obj_t BgL_yz00_6899;
									obj_t BgL_xz00_6898;

									BgL_xz00_6898 = BgL_arg1578z00_1756;
									BgL_yz00_6899 = BgL_arg1579z00_1757;
									BgL_yz00_64 = BgL_yz00_6899;
									BgL_xz00_63 = BgL_xz00_6898;
									goto bgl_bignum_expt;
								}
							}
						else
							{	/* Unsafe/bignumber.scm 682 */
								obj_t BgL_arg1582z00_1759;

								{	/* Unsafe/bignumber.scm 682 */
									obj_t BgL_arg1583z00_1760;

									{	/* Unsafe/bignumber.scm 682 */
										obj_t BgL_arg1584z00_1761;

										{	/* Unsafe/bignumber.scm 682 */
											obj_t BgL_res2335z00_3959;

											{	/* Unsafe/bignumber.scm 415 */
												bool_t BgL_test2741z00_6900;

												if ((1L < -16L))
													{	/* Unsafe/bignumber.scm 415 */
														BgL_test2741z00_6900 = ((bool_t) 1);
													}
												else
													{	/* Unsafe/bignumber.scm 415 */
														BgL_test2741z00_6900 = (16L < 1L);
													}
												if (BgL_test2741z00_6900)
													{	/* Unsafe/bignumber.scm 415 */
														BgL_res2335z00_3959 =
															BGl_z42z42fixnumzd2ze3bignumzd2freshze3zz__bignumz00
															(1L);
													}
												else
													{	/* Unsafe/bignumber.scm 415 */
														BgL_res2335z00_3959 =
															VECTOR_REF
															(BGl_preallocatedzd2bignumszd2zz__bignumz00,
															(1L + 16L));
													}
											}
											BgL_arg1584z00_1761 = BgL_res2335z00_3959;
										}
										{	/* Unsafe/bignumber.scm 682 */
											obj_t BgL_res2336z00_3978;

											{	/* Unsafe/bignumber.scm 610 */
												long BgL_arg1513z00_3962;
												long BgL_arg1514z00_3963;

												{	/* Unsafe/bignumber.scm 263 */
													uint16_t BgL_arg1215z00_3966;

													{	/* Unsafe/bignumber.scm 263 */
														obj_t BgL_arg1216z00_3967;

														BgL_arg1216z00_3967 =
															BGL_BIGNUM_U16VECT(BgL_yz00_64);
														BgL_arg1215z00_3966 =
															BGL_U16VREF(BgL_arg1216z00_3967, 0L);
													}
													{	/* Unsafe/bignumber.scm 263 */
														long BgL_arg2271z00_3969;

														BgL_arg2271z00_3969 = (long) (BgL_arg1215z00_3966);
														BgL_arg1513z00_3962 = (long) (BgL_arg2271z00_3969);
												}}
												{	/* Unsafe/bignumber.scm 610 */
													long BgL_arg1516z00_3964;

													{	/* Unsafe/bignumber.scm 263 */
														uint16_t BgL_arg1215z00_3972;

														{	/* Unsafe/bignumber.scm 263 */
															obj_t BgL_arg1216z00_3973;

															BgL_arg1216z00_3973 =
																BGL_BIGNUM_U16VECT(BgL_arg1584z00_1761);
															BgL_arg1215z00_3972 =
																BGL_U16VREF(BgL_arg1216z00_3973, 0L);
														}
														{	/* Unsafe/bignumber.scm 263 */
															long BgL_arg2271z00_3975;

															BgL_arg2271z00_3975 =
																(long) (BgL_arg1215z00_3972);
															BgL_arg1516z00_3964 =
																(long) (BgL_arg2271z00_3975);
													}}
													BgL_arg1514z00_3963 = (1L - BgL_arg1516z00_3964);
												}
												BgL_res2336z00_3978 =
													BGl_bignumzd2sum2zd2zz__bignumz00(BgL_yz00_64,
													BgL_arg1584z00_1761, BgL_arg1513z00_3962,
													BgL_arg1514z00_3963);
											}
											BgL_arg1583z00_1760 = BgL_res2336z00_3978;
									}}
									BgL_arg1582z00_1759 =
										bgl_bignum_expt(BgL_xz00_63, BgL_arg1583z00_1760);
								}
								BGL_TAIL return
									bgl_bignum_mul(BgL_xz00_63, BgL_arg1582z00_1759);
							}
					}
			}
		}

	}



/* &$$exptbx */
	obj_t BGl_z62z42z42exptbxz62zz__bignumz00(obj_t BgL_envz00_5540,
		obj_t BgL_xz00_5541, obj_t BgL_yz00_5542)
	{
		{	/* Unsafe/bignumber.scm 678 */
			{	/* Unsafe/bignumber.scm 680 */
				obj_t BgL_auxz00_6926;
				obj_t BgL_auxz00_6919;

				if (BIGNUMP(BgL_yz00_5542))
					{	/* Unsafe/bignumber.scm 680 */
						BgL_auxz00_6926 = BgL_yz00_5542;
					}
				else
					{
						obj_t BgL_auxz00_6929;

						BgL_auxz00_6929 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
							BINT(22761L), BGl_string2501z00zz__bignumz00,
							BGl_string2491z00zz__bignumz00, BgL_yz00_5542);
						FAILURE(BgL_auxz00_6929, BFALSE, BFALSE);
					}
				if (BIGNUMP(BgL_xz00_5541))
					{	/* Unsafe/bignumber.scm 680 */
						BgL_auxz00_6919 = BgL_xz00_5541;
					}
				else
					{
						obj_t BgL_auxz00_6922;

						BgL_auxz00_6922 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
							BINT(22761L), BGl_string2501z00zz__bignumz00,
							BGl_string2491z00zz__bignumz00, BgL_xz00_5541);
						FAILURE(BgL_auxz00_6922, BFALSE, BFALSE);
					}
				return bgl_bignum_expt(BgL_auxz00_6919, BgL_auxz00_6926);
			}
		}

	}



/* bignum-div */
	obj_t BGl_bignumzd2divzd2zz__bignumz00(obj_t BgL_xz00_65, obj_t BgL_yz00_66)
	{
		{	/* Unsafe/bignumber.scm 687 */
			{
				obj_t BgL_xz00_1961;
				obj_t BgL_yz00_1962;
				long BgL_lenxz00_1963;
				long BgL_lenyz00_1964;
				obj_t BgL_xz00_1793;
				obj_t BgL_yz00_1794;
				long BgL_lenxz00_1795;
				long BgL_lenyz00_1796;
				obj_t BgL_rz00_1797;
				obj_t BgL_xz00_1768;
				obj_t BgL_yz00_1769;
				long BgL_lenxz00_1770;
				long BgL_lenyz00_1771;
				obj_t BgL_rz00_1772;

				{	/* Unsafe/bignumber.scm 850 */
					bool_t BgL_test2745z00_6934;

					{	/* Unsafe/bignumber.scm 468 */
						long BgL_arg1427z00_4455;

						{	/* Unsafe/bignumber.scm 261 */
							long BgL_arg1210z00_4457;

							{	/* Unsafe/bignumber.scm 261 */
								obj_t BgL_arg1212z00_4458;

								{	/* Unsafe/bignumber.scm 261 */
									obj_t BgL_tmpz00_6935;

									BgL_tmpz00_6935 = ((obj_t) BgL_yz00_66);
									BgL_arg1212z00_4458 = BGL_BIGNUM_U16VECT(BgL_tmpz00_6935);
								}
								BgL_arg1210z00_4457 = BGL_HVECTOR_LENGTH(BgL_arg1212z00_4458);
							}
							{	/* Unsafe/bignumber.scm 261 */
								uint16_t BgL_xz00_4460;

								BgL_xz00_4460 = (uint16_t) (BgL_arg1210z00_4457);
								{	/* Unsafe/bignumber.scm 261 */
									long BgL_arg2271z00_4461;

									BgL_arg2271z00_4461 = (long) (BgL_xz00_4460);
									BgL_arg1427z00_4455 = (long) (BgL_arg2271z00_4461);
						}}}
						BgL_test2745z00_6934 = (BgL_arg1427z00_4455 == 1L);
					}
					if (BgL_test2745z00_6934)
						{	/* Unsafe/bignumber.scm 850 */
							return
								BGl_errorz00zz__errorz00(BGl_string2502z00zz__bignumz00,
								BGl_string2503z00zz__bignumz00, BgL_xz00_65);
						}
					else
						{	/* Unsafe/bignumber.scm 852 */
							long BgL_arg1586z00_1766;
							long BgL_arg1587z00_1767;

							{	/* Unsafe/bignumber.scm 261 */
								long BgL_arg1210z00_4465;

								{	/* Unsafe/bignumber.scm 261 */
									obj_t BgL_arg1212z00_4466;

									{	/* Unsafe/bignumber.scm 261 */
										obj_t BgL_tmpz00_6944;

										BgL_tmpz00_6944 = ((obj_t) BgL_xz00_65);
										BgL_arg1212z00_4466 = BGL_BIGNUM_U16VECT(BgL_tmpz00_6944);
									}
									BgL_arg1210z00_4465 = BGL_HVECTOR_LENGTH(BgL_arg1212z00_4466);
								}
								{	/* Unsafe/bignumber.scm 261 */
									uint16_t BgL_xz00_4468;

									BgL_xz00_4468 = (uint16_t) (BgL_arg1210z00_4465);
									{	/* Unsafe/bignumber.scm 261 */
										long BgL_arg2271z00_4469;

										BgL_arg2271z00_4469 = (long) (BgL_xz00_4468);
										BgL_arg1586z00_1766 = (long) (BgL_arg2271z00_4469);
							}}}
							{	/* Unsafe/bignumber.scm 261 */
								long BgL_arg1210z00_4472;

								{	/* Unsafe/bignumber.scm 261 */
									obj_t BgL_arg1212z00_4473;

									{	/* Unsafe/bignumber.scm 261 */
										obj_t BgL_tmpz00_6951;

										BgL_tmpz00_6951 = ((obj_t) BgL_yz00_66);
										BgL_arg1212z00_4473 = BGL_BIGNUM_U16VECT(BgL_tmpz00_6951);
									}
									BgL_arg1210z00_4472 = BGL_HVECTOR_LENGTH(BgL_arg1212z00_4473);
								}
								{	/* Unsafe/bignumber.scm 261 */
									uint16_t BgL_xz00_4475;

									BgL_xz00_4475 = (uint16_t) (BgL_arg1210z00_4472);
									{	/* Unsafe/bignumber.scm 261 */
										long BgL_arg2271z00_4476;

										BgL_arg2271z00_4476 = (long) (BgL_xz00_4475);
										BgL_arg1587z00_1767 = (long) (BgL_arg2271z00_4476);
							}}}
							BgL_xz00_1961 = BgL_xz00_65;
							BgL_yz00_1962 = BgL_yz00_66;
							BgL_lenxz00_1963 = BgL_arg1586z00_1766;
							BgL_lenyz00_1964 = BgL_arg1587z00_1767;
							if ((BgL_lenxz00_1963 < BgL_lenyz00_1964))
								{	/* Unsafe/bignumber.scm 836 */
									return
										MAKE_YOUNG_PAIR(BGl_bignumzd2za7eroz75zz__bignumz00,
										BgL_xz00_1961);
								}
							else
								{	/* Unsafe/bignumber.scm 840 */
									obj_t BgL_rz00_1967;

									{	/* Unsafe/bignumber.scm 840 */
										long BgL_arg1754z00_1974;

										BgL_arg1754z00_1974 =
											((BgL_lenxz00_1963 - BgL_lenyz00_1964) + 2L);
										{	/* Unsafe/bignumber.scm 840 */
											int BgL_lenz00_4427;

											BgL_lenz00_4427 = (int) (BgL_arg1754z00_1974);
											{	/* Unsafe/bignumber.scm 259 */
												obj_t BgL_arg1208z00_4428;

												{	/* Unsafe/bignumber.scm 259 */

													BgL_arg1208z00_4428 =
														BGl_makezd2u16vectorzd2zz__srfi4z00(
														(long) (BgL_lenz00_4427), (uint16_t) (0));
												}
												BgL_rz00_1967 = bgl_make_bignum(BgL_arg1208z00_4428);
									}}}
									{	/* Unsafe/bignumber.scm 842 */
										bool_t BgL_test2747z00_6967;

										{	/* Unsafe/bignumber.scm 842 */
											long BgL_arg1751z00_1971;
											long BgL_arg1752z00_1972;

											{	/* Unsafe/bignumber.scm 263 */
												uint16_t BgL_arg1215z00_4432;

												{	/* Unsafe/bignumber.scm 263 */
													obj_t BgL_arg1216z00_4433;

													{	/* Unsafe/bignumber.scm 263 */
														obj_t BgL_tmpz00_6968;

														BgL_tmpz00_6968 = ((obj_t) BgL_xz00_1961);
														BgL_arg1216z00_4433 =
															BGL_BIGNUM_U16VECT(BgL_tmpz00_6968);
													}
													BgL_arg1215z00_4432 =
														BGL_U16VREF(BgL_arg1216z00_4433, 0L);
												}
												{	/* Unsafe/bignumber.scm 263 */
													long BgL_arg2271z00_4435;

													BgL_arg2271z00_4435 = (long) (BgL_arg1215z00_4432);
													BgL_arg1751z00_1971 = (long) (BgL_arg2271z00_4435);
											}}
											{	/* Unsafe/bignumber.scm 263 */
												uint16_t BgL_arg1215z00_4438;

												{	/* Unsafe/bignumber.scm 263 */
													obj_t BgL_arg1216z00_4439;

													{	/* Unsafe/bignumber.scm 263 */
														obj_t BgL_tmpz00_6974;

														BgL_tmpz00_6974 = ((obj_t) BgL_yz00_1962);
														BgL_arg1216z00_4439 =
															BGL_BIGNUM_U16VECT(BgL_tmpz00_6974);
													}
													BgL_arg1215z00_4438 =
														BGL_U16VREF(BgL_arg1216z00_4439, 0L);
												}
												{	/* Unsafe/bignumber.scm 263 */
													long BgL_arg2271z00_4441;

													BgL_arg2271z00_4441 = (long) (BgL_arg1215z00_4438);
													BgL_arg1752z00_1972 = (long) (BgL_arg2271z00_4441);
											}}
											BgL_test2747z00_6967 =
												(BgL_arg1751z00_1971 == BgL_arg1752z00_1972);
										}
										if (BgL_test2747z00_6967)
											{	/* Unsafe/bignumber.scm 265 */
												obj_t BgL_arg1218z00_4447;
												uint16_t BgL_arg1219z00_4448;

												BgL_arg1218z00_4447 = BGL_BIGNUM_U16VECT(BgL_rz00_1967);
												BgL_arg1219z00_4448 = (uint16_t) (1L);
												BGL_U16VSET(BgL_arg1218z00_4447, 0L,
													BgL_arg1219z00_4448);
												BUNSPEC;
											}
										else
											{	/* Unsafe/bignumber.scm 265 */
												obj_t BgL_arg1218z00_4451;
												uint16_t BgL_arg1219z00_4452;

												BgL_arg1218z00_4451 = BGL_BIGNUM_U16VECT(BgL_rz00_1967);
												BgL_arg1219z00_4452 = (uint16_t) (0L);
												BGL_U16VSET(BgL_arg1218z00_4451, 0L,
													BgL_arg1219z00_4452);
												BUNSPEC;
											}
									}
									if ((BgL_lenyz00_1964 == 2L))
										{	/* Unsafe/bignumber.scm 846 */
											BgL_xz00_1768 = BgL_xz00_1961;
											BgL_yz00_1769 = BgL_yz00_1962;
											BgL_lenxz00_1770 = BgL_lenxz00_1963;
											BgL_lenyz00_1771 = BgL_lenyz00_1964;
											BgL_rz00_1772 = BgL_rz00_1967;
											{	/* Unsafe/bignumber.scm 693 */
												long BgL_dz00_1774;

												{	/* Unsafe/bignumber.scm 267 */
													uint16_t BgL_arg1220z00_3980;

													{	/* Unsafe/bignumber.scm 267 */
														obj_t BgL_arg1221z00_3981;

														{	/* Unsafe/bignumber.scm 267 */
															obj_t BgL_tmpz00_6989;

															BgL_tmpz00_6989 = ((obj_t) BgL_yz00_1769);
															BgL_arg1221z00_3981 =
																BGL_BIGNUM_U16VECT(BgL_tmpz00_6989);
														}
														BgL_arg1220z00_3980 =
															BGL_U16VREF(BgL_arg1221z00_3981, 1L);
													}
													{	/* Unsafe/bignumber.scm 267 */
														long BgL_arg2271z00_3983;

														BgL_arg2271z00_3983 = (long) (BgL_arg1220z00_3980);
														BgL_dz00_1774 = (long) (BgL_arg2271z00_3983);
												}}
												{	/* Unsafe/bignumber.scm 694 */
													long BgL_g1051z00_1775;

													BgL_g1051z00_1775 = (BgL_lenxz00_1770 - 1L);
													{
														long BgL_iz00_1777;
														long BgL_kz00_1778;

														BgL_iz00_1777 = BgL_g1051z00_1775;
														BgL_kz00_1778 = 0L;
													BgL_zc3z04anonymousza31589ze3z87_1779:
														if ((0L < BgL_iz00_1777))
															{	/* Unsafe/bignumber.scm 696 */
																long BgL_wz00_1781;

																{	/* Unsafe/bignumber.scm 696 */
																	long BgL_arg1598z00_1785;
																	long BgL_arg1601z00_1786;

																	BgL_arg1598z00_1785 =
																		(BgL_kz00_1778 * 16384L);
																	{	/* Unsafe/bignumber.scm 696 */
																		int BgL_iz00_3990;

																		BgL_iz00_3990 = (int) (BgL_iz00_1777);
																		{	/* Unsafe/bignumber.scm 267 */
																			uint16_t BgL_arg1220z00_3991;

																			{	/* Unsafe/bignumber.scm 267 */
																				obj_t BgL_arg1221z00_3992;

																				{	/* Unsafe/bignumber.scm 267 */
																					obj_t BgL_tmpz00_7000;

																					BgL_tmpz00_7000 =
																						((obj_t) BgL_xz00_1768);
																					BgL_arg1221z00_3992 =
																						BGL_BIGNUM_U16VECT(BgL_tmpz00_7000);
																				}
																				{	/* Unsafe/bignumber.scm 267 */
																					long BgL_tmpz00_7003;

																					BgL_tmpz00_7003 =
																						(long) (BgL_iz00_3990);
																					BgL_arg1220z00_3991 =
																						BGL_U16VREF(BgL_arg1221z00_3992,
																						BgL_tmpz00_7003);
																			}}
																			{	/* Unsafe/bignumber.scm 267 */
																				long BgL_arg2271z00_3994;

																				BgL_arg2271z00_3994 =
																					(long) (BgL_arg1220z00_3991);
																				BgL_arg1601z00_1786 =
																					(long) (BgL_arg2271z00_3994);
																	}}}
																	BgL_wz00_1781 =
																		(BgL_arg1598z00_1785 + BgL_arg1601z00_1786);
																}
																{	/* Unsafe/bignumber.scm 697 */
																	long BgL_arg1593z00_1782;

																	BgL_arg1593z00_1782 =
																		(BgL_wz00_1781 / BgL_dz00_1774);
																	{	/* Unsafe/bignumber.scm 697 */
																		int BgL_iz00_4001;
																		int BgL_digitz00_4002;

																		BgL_iz00_4001 = (int) (BgL_iz00_1777);
																		BgL_digitz00_4002 =
																			(int) (BgL_arg1593z00_1782);
																		{	/* Unsafe/bignumber.scm 269 */
																			obj_t BgL_arg1223z00_4003;
																			uint16_t BgL_arg1225z00_4004;

																			BgL_arg1223z00_4003 =
																				BGL_BIGNUM_U16VECT(BgL_rz00_1772);
																			{	/* Unsafe/bignumber.scm 269 */
																				long BgL_tmpz00_7013;

																				BgL_tmpz00_7013 =
																					(long) (BgL_digitz00_4002);
																				BgL_arg1225z00_4004 =
																					(uint16_t) (BgL_tmpz00_7013);
																			}
																			{	/* Unsafe/bignumber.scm 269 */
																				long BgL_tmpz00_7016;

																				BgL_tmpz00_7016 =
																					(long) (BgL_iz00_4001);
																				BGL_U16VSET(BgL_arg1223z00_4003,
																					BgL_tmpz00_7016, BgL_arg1225z00_4004);
																			} BUNSPEC;
																}}}
																{	/* Unsafe/bignumber.scm 698 */
																	long BgL_arg1594z00_1783;
																	long BgL_arg1595z00_1784;

																	BgL_arg1594z00_1783 = (BgL_iz00_1777 - 1L);
																	{	/* Unsafe/bignumber.scm 698 */
																		long BgL_n1z00_4007;
																		long BgL_n2z00_4008;

																		BgL_n1z00_4007 = BgL_wz00_1781;
																		BgL_n2z00_4008 = BgL_dz00_1774;
																		{	/* Unsafe/bignumber.scm 698 */
																			bool_t BgL_test2750z00_7020;

																			{	/* Unsafe/bignumber.scm 698 */
																				long BgL_arg2169z00_4010;

																				BgL_arg2169z00_4010 =
																					(((BgL_n1z00_4007) | (BgL_n2z00_4008))
																					& -2147483648);
																				BgL_test2750z00_7020 =
																					(BgL_arg2169z00_4010 == 0L);
																			}
																			if (BgL_test2750z00_7020)
																				{	/* Unsafe/bignumber.scm 698 */
																					int32_t BgL_arg2166z00_4011;

																					{	/* Unsafe/bignumber.scm 698 */
																						int32_t BgL_arg2167z00_4012;
																						int32_t BgL_arg2168z00_4013;

																						BgL_arg2167z00_4012 =
																							(int32_t) (BgL_n1z00_4007);
																						BgL_arg2168z00_4013 =
																							(int32_t) (BgL_n2z00_4008);
																						BgL_arg2166z00_4011 =
																							(BgL_arg2167z00_4012 %
																							BgL_arg2168z00_4013);
																					}
																					{	/* Unsafe/bignumber.scm 698 */
																						long BgL_arg2270z00_4018;

																						BgL_arg2270z00_4018 =
																							(long) (BgL_arg2166z00_4011);
																						BgL_arg1595z00_1784 =
																							(long) (BgL_arg2270z00_4018);
																				}}
																			else
																				{	/* Unsafe/bignumber.scm 698 */
																					BgL_arg1595z00_1784 =
																						(BgL_n1z00_4007 % BgL_n2z00_4008);
																				}
																		}
																	}
																	{
																		long BgL_kz00_7030;
																		long BgL_iz00_7029;

																		BgL_iz00_7029 = BgL_arg1594z00_1783;
																		BgL_kz00_7030 = BgL_arg1595z00_1784;
																		BgL_kz00_1778 = BgL_kz00_7030;
																		BgL_iz00_1777 = BgL_iz00_7029;
																		goto BgL_zc3z04anonymousza31589ze3z87_1779;
																	}
																}
															}
														else
															{	/* Unsafe/bignumber.scm 699 */
																obj_t BgL_arg1603z00_1788;
																obj_t BgL_arg1605z00_1789;

																BgL_arg1603z00_1788 =
																	BGl_bignumzd2removezd2leadingzd2za7eroesz75zz__bignumz00
																	(BgL_rz00_1772);
																{	/* Unsafe/bignumber.scm 701 */
																	long BgL_arg1606z00_1790;

																	{	/* Unsafe/bignumber.scm 701 */
																		bool_t BgL_test2751z00_7032;

																		{	/* Unsafe/bignumber.scm 471 */
																			long BgL_arg1428z00_4021;

																			{	/* Unsafe/bignumber.scm 263 */
																				uint16_t BgL_arg1215z00_4023;

																				{	/* Unsafe/bignumber.scm 263 */
																					obj_t BgL_arg1216z00_4024;

																					{	/* Unsafe/bignumber.scm 263 */
																						obj_t BgL_tmpz00_7033;

																						BgL_tmpz00_7033 =
																							((obj_t) BgL_xz00_1768);
																						BgL_arg1216z00_4024 =
																							BGL_BIGNUM_U16VECT
																							(BgL_tmpz00_7033);
																					}
																					BgL_arg1215z00_4023 =
																						BGL_U16VREF(BgL_arg1216z00_4024,
																						0L);
																				}
																				{	/* Unsafe/bignumber.scm 263 */
																					long BgL_arg2271z00_4026;

																					BgL_arg2271z00_4026 =
																						(long) (BgL_arg1215z00_4023);
																					BgL_arg1428z00_4021 =
																						(long) (BgL_arg2271z00_4026);
																			}}
																			BgL_test2751z00_7032 =
																				(BgL_arg1428z00_4021 == 0L);
																		}
																		if (BgL_test2751z00_7032)
																			{	/* Unsafe/bignumber.scm 701 */
																				BgL_arg1606z00_1790 =
																					(0L - BgL_kz00_1778);
																			}
																		else
																			{	/* Unsafe/bignumber.scm 701 */
																				BgL_arg1606z00_1790 = BgL_kz00_1778;
																			}
																	}
																	BgL_arg1605z00_1789 =
																		bgl_long_to_bignum(BgL_arg1606z00_1790);
																}
																return
																	MAKE_YOUNG_PAIR(BgL_arg1603z00_1788,
																	BgL_arg1605z00_1789);
															}
													}
												}
											}
										}
									else
										{	/* Unsafe/bignumber.scm 846 */
											BgL_xz00_1793 = BgL_xz00_1961;
											BgL_yz00_1794 = BgL_yz00_1962;
											BgL_lenxz00_1795 = BgL_lenxz00_1963;
											BgL_lenyz00_1796 = BgL_lenyz00_1964;
											BgL_rz00_1797 = BgL_rz00_1967;
											{	/* Unsafe/bignumber.scm 709 */
												long BgL_g1052z00_1799;

												{	/* Unsafe/bignumber.scm 710 */
													long BgL_arg1744z00_1959;

													{	/* Unsafe/bignumber.scm 710 */
														long BgL_arg1745z00_1960;

														BgL_arg1745z00_1960 = (BgL_lenyz00_1796 - 1L);
														{	/* Unsafe/bignumber.scm 710 */
															int BgL_iz00_4032;

															BgL_iz00_4032 = (int) (BgL_arg1745z00_1960);
															{	/* Unsafe/bignumber.scm 267 */
																uint16_t BgL_arg1220z00_4033;

																{	/* Unsafe/bignumber.scm 267 */
																	obj_t BgL_arg1221z00_4034;

																	{	/* Unsafe/bignumber.scm 267 */
																		obj_t BgL_tmpz00_7045;

																		BgL_tmpz00_7045 = ((obj_t) BgL_yz00_1794);
																		BgL_arg1221z00_4034 =
																			BGL_BIGNUM_U16VECT(BgL_tmpz00_7045);
																	}
																	{	/* Unsafe/bignumber.scm 267 */
																		long BgL_tmpz00_7048;

																		BgL_tmpz00_7048 = (long) (BgL_iz00_4032);
																		BgL_arg1220z00_4033 =
																			BGL_U16VREF(BgL_arg1221z00_4034,
																			BgL_tmpz00_7048);
																}}
																{	/* Unsafe/bignumber.scm 267 */
																	long BgL_arg2271z00_4036;

																	BgL_arg2271z00_4036 =
																		(long) (BgL_arg1220z00_4033);
																	BgL_arg1744z00_1959 =
																		(long) (BgL_arg2271z00_4036);
													}}}}
													BgL_g1052z00_1799 = (BgL_arg1744z00_1959 * 2L);
												}
												{
													long BgL_shiftz00_1801;
													long BgL_nz00_1802;

													BgL_shiftz00_1801 = 1L;
													BgL_nz00_1802 = BgL_g1052z00_1799;
												BgL_zc3z04anonymousza31609ze3z87_1803:
													if ((BgL_nz00_1802 < 16384L))
														{
															long BgL_nz00_7058;
															long BgL_shiftz00_7056;

															BgL_shiftz00_7056 = (BgL_shiftz00_1801 * 2L);
															BgL_nz00_7058 = (BgL_nz00_1802 * 2L);
															BgL_nz00_1802 = BgL_nz00_7058;
															BgL_shiftz00_1801 = BgL_shiftz00_7056;
															goto BgL_zc3z04anonymousza31609ze3z87_1803;
														}
													else
														{	/* Unsafe/bignumber.scm 714 */
															obj_t BgL_nxz00_1808;
															obj_t BgL_nyz00_1809;

															{	/* Unsafe/bignumber.scm 714 */
																long BgL_arg1741z00_1956;

																BgL_arg1741z00_1956 = (BgL_lenxz00_1795 + 1L);
																{	/* Unsafe/bignumber.scm 714 */
																	int BgL_lenz00_4044;

																	BgL_lenz00_4044 = (int) (BgL_arg1741z00_1956);
																	{	/* Unsafe/bignumber.scm 259 */
																		obj_t BgL_arg1208z00_4045;

																		{	/* Unsafe/bignumber.scm 259 */

																			BgL_arg1208z00_4045 =
																				BGl_makezd2u16vectorzd2zz__srfi4z00(
																				(long) (BgL_lenz00_4044),
																				(uint16_t) (0));
																		}
																		BgL_nxz00_1808 =
																			bgl_make_bignum(BgL_arg1208z00_4045);
															}}}
															{	/* Unsafe/bignumber.scm 715 */
																int BgL_lenz00_4048;

																BgL_lenz00_4048 = (int) (BgL_lenyz00_1796);
																{	/* Unsafe/bignumber.scm 259 */
																	obj_t BgL_arg1208z00_4049;

																	{	/* Unsafe/bignumber.scm 259 */

																		BgL_arg1208z00_4049 =
																			BGl_makezd2u16vectorzd2zz__srfi4z00(
																			(long) (BgL_lenz00_4048), (uint16_t) (0));
																	}
																	BgL_nyz00_1809 =
																		bgl_make_bignum(BgL_arg1208z00_4049);
															}}
															{	/* Unsafe/bignumber.scm 717 */
																long BgL_arg1615z00_1810;

																{	/* Unsafe/bignumber.scm 263 */
																	uint16_t BgL_arg1215z00_4053;

																	{	/* Unsafe/bignumber.scm 263 */
																		obj_t BgL_arg1216z00_4054;

																		{	/* Unsafe/bignumber.scm 263 */
																			obj_t BgL_tmpz00_7069;

																			BgL_tmpz00_7069 = ((obj_t) BgL_xz00_1793);
																			BgL_arg1216z00_4054 =
																				BGL_BIGNUM_U16VECT(BgL_tmpz00_7069);
																		}
																		BgL_arg1215z00_4053 =
																			BGL_U16VREF(BgL_arg1216z00_4054, 0L);
																	}
																	{	/* Unsafe/bignumber.scm 263 */
																		long BgL_arg2271z00_4056;

																		BgL_arg2271z00_4056 =
																			(long) (BgL_arg1215z00_4053);
																		BgL_arg1615z00_1810 =
																			(long) (BgL_arg2271z00_4056);
																}}
																{	/* Unsafe/bignumber.scm 717 */
																	int BgL_signz00_4059;

																	BgL_signz00_4059 =
																		(int) (BgL_arg1615z00_1810);
																	{	/* Unsafe/bignumber.scm 265 */
																		obj_t BgL_arg1218z00_4060;
																		uint16_t BgL_arg1219z00_4061;

																		BgL_arg1218z00_4060 =
																			BGL_BIGNUM_U16VECT(BgL_nxz00_1808);
																		{	/* Unsafe/bignumber.scm 265 */
																			long BgL_tmpz00_7077;

																			BgL_tmpz00_7077 =
																				(long) (BgL_signz00_4059);
																			BgL_arg1219z00_4061 =
																				(uint16_t) (BgL_tmpz00_7077);
																		}
																		BGL_U16VSET(BgL_arg1218z00_4060, 0L,
																			BgL_arg1219z00_4061);
																		BUNSPEC;
															}}}
															{
																long BgL_iz00_4092;
																long BgL_cz00_4093;

																BgL_iz00_4092 = 1L;
																BgL_cz00_4093 = 0L;
															BgL_loop3z00_4091:
																if ((BgL_iz00_4092 < BgL_lenxz00_1795))
																	{	/* Unsafe/bignumber.scm 721 */
																		long BgL_wz00_4097;

																		{	/* Unsafe/bignumber.scm 721 */
																			long BgL_arg1623z00_4098;

																			{	/* Unsafe/bignumber.scm 721 */
																				long BgL_arg1624z00_4099;

																				{	/* Unsafe/bignumber.scm 721 */
																					int BgL_iz00_4101;

																					BgL_iz00_4101 = (int) (BgL_iz00_4092);
																					{	/* Unsafe/bignumber.scm 267 */
																						uint16_t BgL_arg1220z00_4102;

																						{	/* Unsafe/bignumber.scm 267 */
																							obj_t BgL_arg1221z00_4103;

																							{	/* Unsafe/bignumber.scm 267 */
																								obj_t BgL_tmpz00_7084;

																								BgL_tmpz00_7084 =
																									((obj_t) BgL_xz00_1793);
																								BgL_arg1221z00_4103 =
																									BGL_BIGNUM_U16VECT
																									(BgL_tmpz00_7084);
																							}
																							{	/* Unsafe/bignumber.scm 267 */
																								long BgL_tmpz00_7087;

																								BgL_tmpz00_7087 =
																									(long) (BgL_iz00_4101);
																								BgL_arg1220z00_4102 =
																									BGL_U16VREF
																									(BgL_arg1221z00_4103,
																									BgL_tmpz00_7087);
																						}}
																						{	/* Unsafe/bignumber.scm 267 */
																							long BgL_arg2271z00_4105;

																							BgL_arg2271z00_4105 =
																								(long) (BgL_arg1220z00_4102);
																							BgL_arg1624z00_4099 =
																								(long) (BgL_arg2271z00_4105);
																				}}}
																				BgL_arg1623z00_4098 =
																					(BgL_arg1624z00_4099 *
																					BgL_shiftz00_1801);
																			}
																			BgL_wz00_4097 =
																				(BgL_arg1623z00_4098 + BgL_cz00_4093);
																		}
																		{	/* Unsafe/bignumber.scm 722 */
																			obj_t BgL_arg1618z00_4111;

																			BgL_arg1618z00_4111 =
																				BGl_moduloz00zz__r4_numbers_6_5_fixnumz00
																				(BINT(BgL_wz00_4097), BINT(16384L));
																			{	/* Unsafe/bignumber.scm 722 */
																				int BgL_iz00_4114;
																				int BgL_digitz00_4115;

																				BgL_iz00_4114 = (int) (BgL_iz00_4092);
																				BgL_digitz00_4115 =
																					CINT(BgL_arg1618z00_4111);
																				{	/* Unsafe/bignumber.scm 269 */
																					obj_t BgL_arg1223z00_4116;
																					uint16_t BgL_arg1225z00_4117;

																					BgL_arg1223z00_4116 =
																						BGL_BIGNUM_U16VECT(BgL_nxz00_1808);
																					{	/* Unsafe/bignumber.scm 269 */
																						long BgL_tmpz00_7100;

																						BgL_tmpz00_7100 =
																							(long) (BgL_digitz00_4115);
																						BgL_arg1225z00_4117 =
																							(uint16_t) (BgL_tmpz00_7100);
																					}
																					{	/* Unsafe/bignumber.scm 269 */
																						long BgL_tmpz00_7103;

																						BgL_tmpz00_7103 =
																							(long) (BgL_iz00_4114);
																						BGL_U16VSET(BgL_arg1223z00_4116,
																							BgL_tmpz00_7103,
																							BgL_arg1225z00_4117);
																					} BUNSPEC;
																		}}}
																		{	/* Unsafe/bignumber.scm 723 */
																			long BgL_arg1620z00_4119;
																			long BgL_arg1621z00_4120;

																			BgL_arg1620z00_4119 =
																				(BgL_iz00_4092 + 1L);
																			BgL_arg1621z00_4120 =
																				(BgL_wz00_4097 / 16384L);
																			{
																				long BgL_cz00_7109;
																				long BgL_iz00_7108;

																				BgL_iz00_7108 = BgL_arg1620z00_4119;
																				BgL_cz00_7109 = BgL_arg1621z00_4120;
																				BgL_cz00_4093 = BgL_cz00_7109;
																				BgL_iz00_4092 = BgL_iz00_7108;
																				goto BgL_loop3z00_4091;
																			}
																		}
																	}
																else
																	{	/* Unsafe/bignumber.scm 724 */
																		int BgL_iz00_4126;
																		int BgL_digitz00_4127;

																		BgL_iz00_4126 = (int) (BgL_iz00_4092);
																		BgL_digitz00_4127 = (int) (BgL_cz00_4093);
																		{	/* Unsafe/bignumber.scm 269 */
																			obj_t BgL_arg1223z00_4128;
																			uint16_t BgL_arg1225z00_4129;

																			BgL_arg1223z00_4128 =
																				BGL_BIGNUM_U16VECT(BgL_nxz00_1808);
																			{	/* Unsafe/bignumber.scm 269 */
																				long BgL_tmpz00_7113;

																				BgL_tmpz00_7113 =
																					(long) (BgL_digitz00_4127);
																				BgL_arg1225z00_4129 =
																					(uint16_t) (BgL_tmpz00_7113);
																			}
																			{	/* Unsafe/bignumber.scm 269 */
																				long BgL_tmpz00_7116;

																				BgL_tmpz00_7116 =
																					(long) (BgL_iz00_4126);
																				BGL_U16VSET(BgL_arg1223z00_4128,
																					BgL_tmpz00_7116, BgL_arg1225z00_4129);
																			} BUNSPEC;
															}}}
															{
																long BgL_iz00_4154;
																long BgL_cz00_4155;

																BgL_iz00_4154 = 1L;
																BgL_cz00_4155 = 0L;
															BgL_loop4z00_4153:
																if ((BgL_iz00_4154 < BgL_lenyz00_1796))
																	{	/* Unsafe/bignumber.scm 728 */
																		long BgL_wz00_4159;

																		{	/* Unsafe/bignumber.scm 728 */
																			long BgL_arg1634z00_4160;

																			{	/* Unsafe/bignumber.scm 728 */
																				long BgL_arg1636z00_4161;

																				{	/* Unsafe/bignumber.scm 728 */
																					int BgL_iz00_4163;

																					BgL_iz00_4163 = (int) (BgL_iz00_4154);
																					{	/* Unsafe/bignumber.scm 267 */
																						uint16_t BgL_arg1220z00_4164;

																						{	/* Unsafe/bignumber.scm 267 */
																							obj_t BgL_arg1221z00_4165;

																							{	/* Unsafe/bignumber.scm 267 */
																								obj_t BgL_tmpz00_7122;

																								BgL_tmpz00_7122 =
																									((obj_t) BgL_yz00_1794);
																								BgL_arg1221z00_4165 =
																									BGL_BIGNUM_U16VECT
																									(BgL_tmpz00_7122);
																							}
																							{	/* Unsafe/bignumber.scm 267 */
																								long BgL_tmpz00_7125;

																								BgL_tmpz00_7125 =
																									(long) (BgL_iz00_4163);
																								BgL_arg1220z00_4164 =
																									BGL_U16VREF
																									(BgL_arg1221z00_4165,
																									BgL_tmpz00_7125);
																						}}
																						{	/* Unsafe/bignumber.scm 267 */
																							long BgL_arg2271z00_4167;

																							BgL_arg2271z00_4167 =
																								(long) (BgL_arg1220z00_4164);
																							BgL_arg1636z00_4161 =
																								(long) (BgL_arg2271z00_4167);
																				}}}
																				BgL_arg1634z00_4160 =
																					(BgL_arg1636z00_4161 *
																					BgL_shiftz00_1801);
																			}
																			BgL_wz00_4159 =
																				(BgL_arg1634z00_4160 + BgL_cz00_4155);
																		}
																		{	/* Unsafe/bignumber.scm 729 */
																			obj_t BgL_arg1627z00_4173;

																			BgL_arg1627z00_4173 =
																				BGl_moduloz00zz__r4_numbers_6_5_fixnumz00
																				(BINT(BgL_wz00_4159), BINT(16384L));
																			{	/* Unsafe/bignumber.scm 729 */
																				int BgL_iz00_4176;
																				int BgL_digitz00_4177;

																				BgL_iz00_4176 = (int) (BgL_iz00_4154);
																				BgL_digitz00_4177 =
																					CINT(BgL_arg1627z00_4173);
																				{	/* Unsafe/bignumber.scm 269 */
																					obj_t BgL_arg1223z00_4178;
																					uint16_t BgL_arg1225z00_4179;

																					BgL_arg1223z00_4178 =
																						BGL_BIGNUM_U16VECT(BgL_nyz00_1809);
																					{	/* Unsafe/bignumber.scm 269 */
																						long BgL_tmpz00_7138;

																						BgL_tmpz00_7138 =
																							(long) (BgL_digitz00_4177);
																						BgL_arg1225z00_4179 =
																							(uint16_t) (BgL_tmpz00_7138);
																					}
																					{	/* Unsafe/bignumber.scm 269 */
																						long BgL_tmpz00_7141;

																						BgL_tmpz00_7141 =
																							(long) (BgL_iz00_4176);
																						BGL_U16VSET(BgL_arg1223z00_4178,
																							BgL_tmpz00_7141,
																							BgL_arg1225z00_4179);
																					} BUNSPEC;
																		}}}
																		{	/* Unsafe/bignumber.scm 730 */
																			long BgL_arg1629z00_4181;
																			long BgL_arg1630z00_4182;

																			BgL_arg1629z00_4181 =
																				(BgL_iz00_4154 + 1L);
																			BgL_arg1630z00_4182 =
																				(BgL_wz00_4159 / 16384L);
																			{
																				long BgL_cz00_7147;
																				long BgL_iz00_7146;

																				BgL_iz00_7146 = BgL_arg1629z00_4181;
																				BgL_cz00_7147 = BgL_arg1630z00_4182;
																				BgL_cz00_4155 = BgL_cz00_7147;
																				BgL_iz00_4154 = BgL_iz00_7146;
																				goto BgL_loop4z00_4153;
																			}
																		}
																	}
																else
																	{	/* Unsafe/bignumber.scm 727 */
																		((bool_t) 0);
																	}
															}
															{
																long BgL_iz00_1840;

																BgL_iz00_1840 = BgL_lenxz00_1795;
															BgL_zc3z04anonymousza31637ze3z87_1841:
																if ((BgL_iz00_1840 < BgL_lenyz00_1796))
																	{	/* Unsafe/bignumber.scm 733 */
																		((bool_t) 0);
																	}
																else
																	{	/* Unsafe/bignumber.scm 737 */
																		long BgL_msdzd2ofzd2nyz00_1843;
																		long BgL_nextzd2msdzd2ofzd2nyzd2_1844;
																		long BgL_msdzd2ofzd2nxz00_1845;
																		long BgL_nextzd2msdzd2ofzd2nxzd2_1846;
																		long
																			BgL_nextzd2nextzd2msdzd2ofzd2nxz00_1847;
																		{	/* Unsafe/bignumber.scm 738 */
																			long BgL_arg1727z00_1935;

																			BgL_arg1727z00_1935 =
																				(BgL_lenyz00_1796 - 1L);
																			{	/* Unsafe/bignumber.scm 738 */
																				int BgL_iz00_4191;

																				BgL_iz00_4191 =
																					(int) (BgL_arg1727z00_1935);
																				{	/* Unsafe/bignumber.scm 267 */
																					uint16_t BgL_arg1220z00_4192;

																					{	/* Unsafe/bignumber.scm 267 */
																						obj_t BgL_arg1221z00_4193;

																						BgL_arg1221z00_4193 =
																							BGL_BIGNUM_U16VECT
																							(BgL_nyz00_1809);
																						{	/* Unsafe/bignumber.scm 267 */
																							long BgL_tmpz00_7153;

																							BgL_tmpz00_7153 =
																								(long) (BgL_iz00_4191);
																							BgL_arg1220z00_4192 =
																								BGL_U16VREF(BgL_arg1221z00_4193,
																								BgL_tmpz00_7153);
																					}}
																					{	/* Unsafe/bignumber.scm 267 */
																						long BgL_arg2271z00_4195;

																						BgL_arg2271z00_4195 =
																							(long) (BgL_arg1220z00_4192);
																						BgL_msdzd2ofzd2nyz00_1843 =
																							(long) (BgL_arg2271z00_4195);
																		}}}}
																		{	/* Unsafe/bignumber.scm 740 */
																			long BgL_arg1728z00_1936;

																			BgL_arg1728z00_1936 =
																				(BgL_lenyz00_1796 - 2L);
																			{	/* Unsafe/bignumber.scm 740 */
																				int BgL_iz00_4199;

																				BgL_iz00_4199 =
																					(int) (BgL_arg1728z00_1936);
																				{	/* Unsafe/bignumber.scm 267 */
																					uint16_t BgL_arg1220z00_4200;

																					{	/* Unsafe/bignumber.scm 267 */
																						obj_t BgL_arg1221z00_4201;

																						BgL_arg1221z00_4201 =
																							BGL_BIGNUM_U16VECT
																							(BgL_nyz00_1809);
																						{	/* Unsafe/bignumber.scm 267 */
																							long BgL_tmpz00_7161;

																							BgL_tmpz00_7161 =
																								(long) (BgL_iz00_4199);
																							BgL_arg1220z00_4200 =
																								BGL_U16VREF(BgL_arg1221z00_4201,
																								BgL_tmpz00_7161);
																					}}
																					{	/* Unsafe/bignumber.scm 267 */
																						long BgL_arg2271z00_4203;

																						BgL_arg2271z00_4203 =
																							(long) (BgL_arg1220z00_4200);
																						BgL_nextzd2msdzd2ofzd2nyzd2_1844 =
																							(long) (BgL_arg2271z00_4203);
																		}}}}
																		{	/* Unsafe/bignumber.scm 742 */
																			int BgL_iz00_4206;

																			BgL_iz00_4206 = (int) (BgL_iz00_1840);
																			{	/* Unsafe/bignumber.scm 267 */
																				uint16_t BgL_arg1220z00_4207;

																				{	/* Unsafe/bignumber.scm 267 */
																					obj_t BgL_arg1221z00_4208;

																					BgL_arg1221z00_4208 =
																						BGL_BIGNUM_U16VECT(BgL_nxz00_1808);
																					{	/* Unsafe/bignumber.scm 267 */
																						long BgL_tmpz00_7168;

																						BgL_tmpz00_7168 =
																							(long) (BgL_iz00_4206);
																						BgL_arg1220z00_4207 =
																							BGL_U16VREF(BgL_arg1221z00_4208,
																							BgL_tmpz00_7168);
																				}}
																				{	/* Unsafe/bignumber.scm 267 */
																					long BgL_arg2271z00_4210;

																					BgL_arg2271z00_4210 =
																						(long) (BgL_arg1220z00_4207);
																					BgL_msdzd2ofzd2nxz00_1845 =
																						(long) (BgL_arg2271z00_4210);
																		}}}
																		{	/* Unsafe/bignumber.scm 744 */
																			long BgL_arg1729z00_1937;

																			BgL_arg1729z00_1937 =
																				(BgL_iz00_1840 - 1L);
																			{	/* Unsafe/bignumber.scm 744 */
																				int BgL_iz00_4214;

																				BgL_iz00_4214 =
																					(int) (BgL_arg1729z00_1937);
																				{	/* Unsafe/bignumber.scm 267 */
																					uint16_t BgL_arg1220z00_4215;

																					{	/* Unsafe/bignumber.scm 267 */
																						obj_t BgL_arg1221z00_4216;

																						BgL_arg1221z00_4216 =
																							BGL_BIGNUM_U16VECT
																							(BgL_nxz00_1808);
																						{	/* Unsafe/bignumber.scm 267 */
																							long BgL_tmpz00_7176;

																							BgL_tmpz00_7176 =
																								(long) (BgL_iz00_4214);
																							BgL_arg1220z00_4215 =
																								BGL_U16VREF(BgL_arg1221z00_4216,
																								BgL_tmpz00_7176);
																					}}
																					{	/* Unsafe/bignumber.scm 267 */
																						long BgL_arg2271z00_4218;

																						BgL_arg2271z00_4218 =
																							(long) (BgL_arg1220z00_4215);
																						BgL_nextzd2msdzd2ofzd2nxzd2_1846 =
																							(long) (BgL_arg2271z00_4218);
																		}}}}
																		{	/* Unsafe/bignumber.scm 746 */
																			long BgL_arg1730z00_1938;

																			BgL_arg1730z00_1938 =
																				(BgL_iz00_1840 - 2L);
																			{	/* Unsafe/bignumber.scm 746 */
																				int BgL_iz00_4222;

																				BgL_iz00_4222 =
																					(int) (BgL_arg1730z00_1938);
																				{	/* Unsafe/bignumber.scm 267 */
																					uint16_t BgL_arg1220z00_4223;

																					{	/* Unsafe/bignumber.scm 267 */
																						obj_t BgL_arg1221z00_4224;

																						BgL_arg1221z00_4224 =
																							BGL_BIGNUM_U16VECT
																							(BgL_nxz00_1808);
																						{	/* Unsafe/bignumber.scm 267 */
																							long BgL_tmpz00_7184;

																							BgL_tmpz00_7184 =
																								(long) (BgL_iz00_4222);
																							BgL_arg1220z00_4223 =
																								BGL_U16VREF(BgL_arg1221z00_4224,
																								BgL_tmpz00_7184);
																					}}
																					{	/* Unsafe/bignumber.scm 267 */
																						long BgL_arg2271z00_4226;

																						BgL_arg2271z00_4226 =
																							(long) (BgL_arg1220z00_4223);
																						BgL_nextzd2nextzd2msdzd2ofzd2nxz00_1847
																							= (long) (BgL_arg2271z00_4226);
																		}}}}
																		{
																			long BgL_qz00_1914;
																			long BgL_uz00_1915;

																			{	/* Unsafe/bignumber.scm 760 */
																				long BgL_qz00_1849;

																				if (
																					(BgL_msdzd2ofzd2nxz00_1845 ==
																						BgL_msdzd2ofzd2nyz00_1843))
																					{	/* Unsafe/bignumber.scm 760 */
																						BgL_qz00_1914 = 16383L;
																						BgL_uz00_1915 =
																							(BgL_msdzd2ofzd2nyz00_1843 +
																							BgL_nextzd2msdzd2ofzd2nxzd2_1846);
																					BgL_zc3z04anonymousza31711ze3z87_1916:
																						if (
																							(BgL_uz00_1915 < 16384L))
																							{	/* Unsafe/bignumber.scm 750 */
																								long BgL_temp1z00_1919;

																								BgL_temp1z00_1919 =
																									(BgL_qz00_1914 *
																									BgL_nextzd2msdzd2ofzd2nyzd2_1844);
																								{	/* Unsafe/bignumber.scm 750 */
																									long BgL_temp2z00_1920;

																									BgL_temp2z00_1920 =
																										(BgL_temp1z00_1919 /
																										16384L);
																									{	/* Unsafe/bignumber.scm 751 */

																										{	/* Unsafe/bignumber.scm 752 */
																											bool_t
																												BgL_test2758z00_7195;
																											if ((BgL_uz00_1915 <
																													BgL_temp2z00_1920))
																												{	/* Unsafe/bignumber.scm 752 */
																													BgL_test2758z00_7195 =
																														((bool_t) 1);
																												}
																											else
																												{	/* Unsafe/bignumber.scm 752 */
																													if (
																														(BgL_temp2z00_1920
																															== BgL_uz00_1915))
																														{	/* Unsafe/bignumber.scm 755 */
																															long
																																BgL_arg1723z00_1930;
																															{	/* Unsafe/bignumber.scm 755 */
																																long
																																	BgL_n1z00_4238;
																																long
																																	BgL_n2z00_4239;
																																BgL_n1z00_4238 =
																																	BgL_temp1z00_1919;
																																BgL_n2z00_4239 =
																																	16384L;
																																{	/* Unsafe/bignumber.scm 755 */
																																	bool_t
																																		BgL_test2761z00_7200;
																																	{	/* Unsafe/bignumber.scm 755 */
																																		long
																																			BgL_arg2169z00_4241;
																																		BgL_arg2169z00_4241
																																			=
																																			(((BgL_n1z00_4238) | (BgL_n2z00_4239)) & -2147483648);
																																		BgL_test2761z00_7200
																																			=
																																			(BgL_arg2169z00_4241
																																			== 0L);
																																	}
																																	if (BgL_test2761z00_7200)
																																		{	/* Unsafe/bignumber.scm 755 */
																																			int32_t
																																				BgL_arg2166z00_4242;
																																			{	/* Unsafe/bignumber.scm 755 */
																																				int32_t
																																					BgL_arg2167z00_4243;
																																				int32_t
																																					BgL_arg2168z00_4244;
																																				BgL_arg2167z00_4243
																																					=
																																					(int32_t)
																																					(BgL_n1z00_4238);
																																				BgL_arg2168z00_4244
																																					=
																																					(int32_t)
																																					(BgL_n2z00_4239);
																																				BgL_arg2166z00_4242
																																					=
																																					(BgL_arg2167z00_4243
																																					%
																																					BgL_arg2168z00_4244);
																																			}
																																			{	/* Unsafe/bignumber.scm 755 */
																																				long
																																					BgL_arg2270z00_4249;
																																				BgL_arg2270z00_4249
																																					=
																																					(long)
																																					(BgL_arg2166z00_4242);
																																				BgL_arg1723z00_1930
																																					=
																																					(long)
																																					(BgL_arg2270z00_4249);
																																		}}
																																	else
																																		{	/* Unsafe/bignumber.scm 755 */
																																			BgL_arg1723z00_1930
																																				=
																																				(BgL_n1z00_4238
																																				%
																																				BgL_n2z00_4239);
																																		}
																																}
																															}
																															BgL_test2758z00_7195
																																=
																																(BgL_nextzd2nextzd2msdzd2ofzd2nxz00_1847
																																<
																																BgL_arg1723z00_1930);
																														}
																													else
																														{	/* Unsafe/bignumber.scm 753 */
																															BgL_test2758z00_7195
																																= ((bool_t) 0);
																														}
																												}
																											if (BgL_test2758z00_7195)
																												{
																													long BgL_uz00_7212;
																													long BgL_qz00_7210;

																													BgL_qz00_7210 =
																														(BgL_qz00_1914 -
																														1L);
																													BgL_uz00_7212 =
																														(BgL_uz00_1915 +
																														BgL_msdzd2ofzd2nyz00_1843);
																													BgL_uz00_1915 =
																														BgL_uz00_7212;
																													BgL_qz00_1914 =
																														BgL_qz00_7210;
																													goto
																														BgL_zc3z04anonymousza31711ze3z87_1916;
																												}
																											else
																												{	/* Unsafe/bignumber.scm 752 */
																													BgL_qz00_1849 =
																														BgL_qz00_1914;
																												}
																										}
																									}
																								}
																							}
																						else
																							{	/* Unsafe/bignumber.scm 749 */
																								BgL_qz00_1849 = BgL_qz00_1914;
																							}
																					}
																				else
																					{	/* Unsafe/bignumber.scm 763 */
																						long BgL_tempz00_1909;

																						BgL_tempz00_1909 =
																							(
																							(BgL_msdzd2ofzd2nxz00_1845 *
																								16384L) +
																							BgL_nextzd2msdzd2ofzd2nxzd2_1846);
																						{	/* Unsafe/bignumber.scm 765 */
																							long BgL_arg1707z00_1910;
																							long BgL_arg1708z00_1911;

																							BgL_arg1707z00_1910 =
																								(BgL_tempz00_1909 /
																								BgL_msdzd2ofzd2nyz00_1843);
																							BgL_arg1708z00_1911 =
																								BGl_modulofxz00zz__r4_numbers_6_5_fixnumz00
																								(BgL_tempz00_1909,
																								BgL_msdzd2ofzd2nyz00_1843);
																							{
																								long BgL_uz00_7220;
																								long BgL_qz00_7219;

																								BgL_qz00_7219 =
																									BgL_arg1707z00_1910;
																								BgL_uz00_7220 =
																									BgL_arg1708z00_1911;
																								BgL_uz00_1915 = BgL_uz00_7220;
																								BgL_qz00_1914 = BgL_qz00_7219;
																								goto
																									BgL_zc3z04anonymousza31711ze3z87_1916;
																							}
																						}
																					}
																				{	/* Unsafe/bignumber.scm 770 */
																					long BgL_g1054z00_1850;

																					BgL_g1054z00_1850 =
																						(BgL_iz00_1840 -
																						(BgL_lenyz00_1796 - 1L));
																					{
																						long BgL_jz00_1852;
																						long BgL_kz00_1853;
																						long BgL_bz00_1854;

																						BgL_jz00_1852 = 1L;
																						BgL_kz00_1853 = BgL_g1054z00_1850;
																						BgL_bz00_1854 = 0L;
																					BgL_zc3z04anonymousza31639ze3z87_1855:
																						if (
																							(BgL_jz00_1852 <
																								BgL_lenyz00_1796))
																							{	/* Unsafe/bignumber.scm 775 */
																								long BgL_wz00_1857;

																								{	/* Unsafe/bignumber.scm 775 */
																									long BgL_arg1650z00_1866;
																									long BgL_arg1651z00_1867;

																									{	/* Unsafe/bignumber.scm 775 */
																										long BgL_arg1652z00_1868;

																										{	/* Unsafe/bignumber.scm 775 */
																											int BgL_iz00_4272;

																											BgL_iz00_4272 =
																												(int) (BgL_kz00_1853);
																											{	/* Unsafe/bignumber.scm 267 */
																												uint16_t
																													BgL_arg1220z00_4273;
																												{	/* Unsafe/bignumber.scm 267 */
																													obj_t
																														BgL_arg1221z00_4274;
																													BgL_arg1221z00_4274 =
																														BGL_BIGNUM_U16VECT
																														(BgL_nxz00_1808);
																													{	/* Unsafe/bignumber.scm 267 */
																														long
																															BgL_tmpz00_7227;
																														BgL_tmpz00_7227 =
																															(long)
																															(BgL_iz00_4272);
																														BgL_arg1220z00_4273
																															=
																															BGL_U16VREF
																															(BgL_arg1221z00_4274,
																															BgL_tmpz00_7227);
																												}}
																												{	/* Unsafe/bignumber.scm 267 */
																													long
																														BgL_arg2271z00_4276;
																													BgL_arg2271z00_4276 =
																														(long)
																														(BgL_arg1220z00_4273);
																													BgL_arg1652z00_1868 =
																														(long)
																														(BgL_arg2271z00_4276);
																										}}}
																										BgL_arg1650z00_1866 =
																											(BgL_arg1652z00_1868 +
																											BgL_bz00_1854);
																									}
																									{	/* Unsafe/bignumber.scm 776 */
																										long BgL_arg1653z00_1869;

																										{	/* Unsafe/bignumber.scm 776 */
																											int BgL_iz00_4281;

																											BgL_iz00_4281 =
																												(int) (BgL_jz00_1852);
																											{	/* Unsafe/bignumber.scm 267 */
																												uint16_t
																													BgL_arg1220z00_4282;
																												{	/* Unsafe/bignumber.scm 267 */
																													obj_t
																														BgL_arg1221z00_4283;
																													BgL_arg1221z00_4283 =
																														BGL_BIGNUM_U16VECT
																														(BgL_nyz00_1809);
																													{	/* Unsafe/bignumber.scm 267 */
																														long
																															BgL_tmpz00_7235;
																														BgL_tmpz00_7235 =
																															(long)
																															(BgL_iz00_4281);
																														BgL_arg1220z00_4282
																															=
																															BGL_U16VREF
																															(BgL_arg1221z00_4283,
																															BgL_tmpz00_7235);
																												}}
																												{	/* Unsafe/bignumber.scm 267 */
																													long
																														BgL_arg2271z00_4285;
																													BgL_arg2271z00_4285 =
																														(long)
																														(BgL_arg1220z00_4282);
																													BgL_arg1653z00_1869 =
																														(long)
																														(BgL_arg2271z00_4285);
																										}}}
																										BgL_arg1651z00_1867 =
																											(BgL_arg1653z00_1869 *
																											BgL_qz00_1849);
																									}
																									BgL_wz00_1857 =
																										(BgL_arg1650z00_1866 -
																										BgL_arg1651z00_1867);
																								}
																								{	/* Unsafe/bignumber.scm 780 */
																									obj_t BgL_arg1641z00_1858;

																									BgL_arg1641z00_1858 =
																										BGl_moduloz00zz__r4_numbers_6_5_fixnumz00
																										(BINT(BgL_wz00_1857),
																										BINT(16384L));
																									{	/* Unsafe/bignumber.scm 777 */
																										int BgL_iz00_4292;
																										int BgL_digitz00_4293;

																										BgL_iz00_4292 =
																											(int) (BgL_kz00_1853);
																										BgL_digitz00_4293 =
																											CINT(BgL_arg1641z00_1858);
																										{	/* Unsafe/bignumber.scm 269 */
																											obj_t BgL_arg1223z00_4294;
																											uint16_t
																												BgL_arg1225z00_4295;
																											BgL_arg1223z00_4294 =
																												BGL_BIGNUM_U16VECT
																												(BgL_nxz00_1808);
																											{	/* Unsafe/bignumber.scm 269 */
																												long BgL_tmpz00_7248;

																												BgL_tmpz00_7248 =
																													(long)
																													(BgL_digitz00_4293);
																												BgL_arg1225z00_4295 =
																													(uint16_t)
																													(BgL_tmpz00_7248);
																											}
																											{	/* Unsafe/bignumber.scm 269 */
																												long BgL_tmpz00_7251;

																												BgL_tmpz00_7251 =
																													(long)
																													(BgL_iz00_4292);
																												BGL_U16VSET
																													(BgL_arg1223z00_4294,
																													BgL_tmpz00_7251,
																													BgL_arg1225z00_4295);
																											} BUNSPEC;
																								}}}
																								{	/* Unsafe/bignumber.scm 781 */
																									long BgL_arg1643z00_1860;
																									long BgL_arg1644z00_1861;
																									long BgL_arg1645z00_1862;

																									BgL_arg1643z00_1860 =
																										(BgL_jz00_1852 + 1L);
																									BgL_arg1644z00_1861 =
																										(BgL_kz00_1853 + 1L);
																									{	/* Unsafe/bignumber.scm 783 */
																										long BgL_tmpz00_7256;

																										BgL_tmpz00_7256 =
																											(BgL_wz00_1857 - 16383L);
																										BgL_arg1645z00_1862 =
																											(BgL_tmpz00_7256 /
																											16384L);
																									}
																									{
																										long BgL_bz00_7261;
																										long BgL_kz00_7260;
																										long BgL_jz00_7259;

																										BgL_jz00_7259 =
																											BgL_arg1643z00_1860;
																										BgL_kz00_7260 =
																											BgL_arg1644z00_1861;
																										BgL_bz00_7261 =
																											BgL_arg1645z00_1862;
																										BgL_bz00_1854 =
																											BgL_bz00_7261;
																										BgL_kz00_1853 =
																											BgL_kz00_7260;
																										BgL_jz00_1852 =
																											BgL_jz00_7259;
																										goto
																											BgL_zc3z04anonymousza31639ze3z87_1855;
																									}
																								}
																							}
																						else
																							{	/* Unsafe/bignumber.scm 786 */
																								long BgL_wz00_1870;

																								{	/* Unsafe/bignumber.scm 786 */
																									long BgL_arg1702z00_1903;

																									{	/* Unsafe/bignumber.scm 786 */
																										int BgL_iz00_4304;

																										BgL_iz00_4304 =
																											(int) (BgL_kz00_1853);
																										{	/* Unsafe/bignumber.scm 267 */
																											uint16_t
																												BgL_arg1220z00_4305;
																											{	/* Unsafe/bignumber.scm 267 */
																												obj_t
																													BgL_arg1221z00_4306;
																												BgL_arg1221z00_4306 =
																													BGL_BIGNUM_U16VECT
																													(BgL_nxz00_1808);
																												{	/* Unsafe/bignumber.scm 267 */
																													long BgL_tmpz00_7264;

																													BgL_tmpz00_7264 =
																														(long)
																														(BgL_iz00_4304);
																													BgL_arg1220z00_4305 =
																														BGL_U16VREF
																														(BgL_arg1221z00_4306,
																														BgL_tmpz00_7264);
																											}}
																											{	/* Unsafe/bignumber.scm 267 */
																												long
																													BgL_arg2271z00_4308;
																												BgL_arg2271z00_4308 =
																													(long)
																													(BgL_arg1220z00_4305);
																												BgL_arg1702z00_1903 =
																													(long)
																													(BgL_arg2271z00_4308);
																									}}}
																									BgL_wz00_1870 =
																										(BgL_arg1702z00_1903 +
																										BgL_bz00_1854);
																								}
																								{	/* Unsafe/bignumber.scm 790 */
																									obj_t BgL_arg1654z00_1871;

																									BgL_arg1654z00_1871 =
																										BGl_moduloz00zz__r4_numbers_6_5_fixnumz00
																										(BINT(BgL_wz00_1870),
																										BINT(16384L));
																									{	/* Unsafe/bignumber.scm 787 */
																										int BgL_iz00_4313;
																										int BgL_digitz00_4314;

																										BgL_iz00_4313 =
																											(int) (BgL_kz00_1853);
																										BgL_digitz00_4314 =
																											CINT(BgL_arg1654z00_1871);
																										{	/* Unsafe/bignumber.scm 269 */
																											obj_t BgL_arg1223z00_4315;
																											uint16_t
																												BgL_arg1225z00_4316;
																											BgL_arg1223z00_4315 =
																												BGL_BIGNUM_U16VECT
																												(BgL_nxz00_1808);
																											{	/* Unsafe/bignumber.scm 269 */
																												long BgL_tmpz00_7276;

																												BgL_tmpz00_7276 =
																													(long)
																													(BgL_digitz00_4314);
																												BgL_arg1225z00_4316 =
																													(uint16_t)
																													(BgL_tmpz00_7276);
																											}
																											{	/* Unsafe/bignumber.scm 269 */
																												long BgL_tmpz00_7279;

																												BgL_tmpz00_7279 =
																													(long)
																													(BgL_iz00_4313);
																												BGL_U16VSET
																													(BgL_arg1223z00_4315,
																													BgL_tmpz00_7279,
																													BgL_arg1225z00_4316);
																											} BUNSPEC;
																								}}}
																								if ((BgL_wz00_1870 < 0L))
																									{	/* Unsafe/bignumber.scm 791 */
																										{	/* Unsafe/bignumber.scm 795 */
																											long BgL_arg1658z00_1874;
																											long BgL_arg1661z00_1875;

																											BgL_arg1658z00_1874 =
																												(BgL_iz00_1840 -
																												(BgL_lenyz00_1796 -
																													1L));
																											BgL_arg1661z00_1875 =
																												(BgL_qz00_1849 - 1L);
																											{	/* Unsafe/bignumber.scm 793 */
																												int BgL_iz00_4324;
																												int BgL_digitz00_4325;

																												BgL_iz00_4324 =
																													(int)
																													(BgL_arg1658z00_1874);
																												BgL_digitz00_4325 =
																													(int)
																													(BgL_arg1661z00_1875);
																												{	/* Unsafe/bignumber.scm 269 */
																													obj_t
																														BgL_arg1223z00_4326;
																													uint16_t
																														BgL_arg1225z00_4327;
																													BgL_arg1223z00_4326 =
																														BGL_BIGNUM_U16VECT
																														(BgL_rz00_1797);
																													{	/* Unsafe/bignumber.scm 269 */
																														long
																															BgL_tmpz00_7290;
																														BgL_tmpz00_7290 =
																															(long)
																															(BgL_digitz00_4325);
																														BgL_arg1225z00_4327
																															=
																															(uint16_t)
																															(BgL_tmpz00_7290);
																													}
																													{	/* Unsafe/bignumber.scm 269 */
																														long
																															BgL_tmpz00_7293;
																														BgL_tmpz00_7293 =
																															(long)
																															(BgL_iz00_4324);
																														BGL_U16VSET
																															(BgL_arg1223z00_4326,
																															BgL_tmpz00_7293,
																															BgL_arg1225z00_4327);
																													} BUNSPEC;
																										}}}
																										{	/* Unsafe/bignumber.scm 797 */
																											long BgL_g1055z00_1877;

																											BgL_g1055z00_1877 =
																												(BgL_iz00_1840 -
																												(BgL_lenyz00_1796 -
																													1L));
																											{
																												long BgL_jz00_1879;
																												long BgL_kz00_1880;
																												long BgL_cz00_1881;

																												BgL_jz00_1879 = 1L;
																												BgL_kz00_1880 =
																													BgL_g1055z00_1877;
																												BgL_cz00_1881 = 0L;
																											BgL_zc3z04anonymousza31664ze3z87_1882:
																												if (
																													(BgL_jz00_1879 <
																														BgL_lenyz00_1796))
																													{	/* Unsafe/bignumber.scm 802 */
																														long BgL_wz00_1884;

																														{	/* Unsafe/bignumber.scm 804 */
																															long
																																BgL_arg1678z00_1891;
																															{	/* Unsafe/bignumber.scm 804 */
																																long
																																	BgL_arg1681z00_1892;
																																long
																																	BgL_arg1684z00_1893;
																																{	/* Unsafe/bignumber.scm 804 */
																																	int
																																		BgL_iz00_4335;
																																	BgL_iz00_4335
																																		=
																																		(int)
																																		(BgL_kz00_1880);
																																	{	/* Unsafe/bignumber.scm 267 */
																																		uint16_t
																																			BgL_arg1220z00_4336;
																																		{	/* Unsafe/bignumber.scm 267 */
																																			obj_t
																																				BgL_arg1221z00_4337;
																																			BgL_arg1221z00_4337
																																				=
																																				BGL_BIGNUM_U16VECT
																																				(BgL_nxz00_1808);
																																			{	/* Unsafe/bignumber.scm 267 */
																																				long
																																					BgL_tmpz00_7302;
																																				BgL_tmpz00_7302
																																					=
																																					(long)
																																					(BgL_iz00_4335);
																																				BgL_arg1220z00_4336
																																					=
																																					BGL_U16VREF
																																					(BgL_arg1221z00_4337,
																																					BgL_tmpz00_7302);
																																		}}
																																		{	/* Unsafe/bignumber.scm 267 */
																																			long
																																				BgL_arg2271z00_4339;
																																			BgL_arg2271z00_4339
																																				=
																																				(long)
																																				(BgL_arg1220z00_4336);
																																			BgL_arg1681z00_1892
																																				=
																																				(long)
																																				(BgL_arg2271z00_4339);
																																}}}
																																{	/* Unsafe/bignumber.scm 805 */
																																	int
																																		BgL_iz00_4342;
																																	BgL_iz00_4342
																																		=
																																		(int)
																																		(BgL_jz00_1879);
																																	{	/* Unsafe/bignumber.scm 267 */
																																		uint16_t
																																			BgL_arg1220z00_4343;
																																		{	/* Unsafe/bignumber.scm 267 */
																																			obj_t
																																				BgL_arg1221z00_4344;
																																			BgL_arg1221z00_4344
																																				=
																																				BGL_BIGNUM_U16VECT
																																				(BgL_nyz00_1809);
																																			{	/* Unsafe/bignumber.scm 267 */
																																				long
																																					BgL_tmpz00_7309;
																																				BgL_tmpz00_7309
																																					=
																																					(long)
																																					(BgL_iz00_4342);
																																				BgL_arg1220z00_4343
																																					=
																																					BGL_U16VREF
																																					(BgL_arg1221z00_4344,
																																					BgL_tmpz00_7309);
																																		}}
																																		{	/* Unsafe/bignumber.scm 267 */
																																			long
																																				BgL_arg2271z00_4346;
																																			BgL_arg2271z00_4346
																																				=
																																				(long)
																																				(BgL_arg1220z00_4343);
																																			BgL_arg1684z00_1893
																																				=
																																				(long)
																																				(BgL_arg2271z00_4346);
																																}}}
																																BgL_arg1678z00_1891
																																	=
																																	(BgL_arg1681z00_1892
																																	+
																																	BgL_arg1684z00_1893);
																															}
																															BgL_wz00_1884 =
																																(BgL_arg1678z00_1891
																																+
																																BgL_cz00_1881);
																														}
																														{	/* Unsafe/bignumber.scm 810 */
																															obj_t
																																BgL_arg1667z00_1885;
																															BgL_arg1667z00_1885
																																=
																																BGl_moduloz00zz__r4_numbers_6_5_fixnumz00
																																(BINT
																																(BgL_wz00_1884),
																																BINT(16384L));
																															{	/* Unsafe/bignumber.scm 807 */
																																int
																																	BgL_iz00_4353;
																																int
																																	BgL_digitz00_4354;
																																BgL_iz00_4353 =
																																	(int)
																																	(BgL_kz00_1880);
																																BgL_digitz00_4354
																																	=
																																	CINT
																																	(BgL_arg1667z00_1885);
																																{	/* Unsafe/bignumber.scm 269 */
																																	obj_t
																																		BgL_arg1223z00_4355;
																																	uint16_t
																																		BgL_arg1225z00_4356;
																																	BgL_arg1223z00_4355
																																		=
																																		BGL_BIGNUM_U16VECT
																																		(BgL_nxz00_1808);
																																	{	/* Unsafe/bignumber.scm 269 */
																																		long
																																			BgL_tmpz00_7322;
																																		BgL_tmpz00_7322
																																			=
																																			(long)
																																			(BgL_digitz00_4354);
																																		BgL_arg1225z00_4356
																																			=
																																			(uint16_t)
																																			(BgL_tmpz00_7322);
																																	}
																																	{	/* Unsafe/bignumber.scm 269 */
																																		long
																																			BgL_tmpz00_7325;
																																		BgL_tmpz00_7325
																																			=
																																			(long)
																																			(BgL_iz00_4353);
																																		BGL_U16VSET
																																			(BgL_arg1223z00_4355,
																																			BgL_tmpz00_7325,
																																			BgL_arg1225z00_4356);
																																	} BUNSPEC;
																														}}}
																														{	/* Unsafe/bignumber.scm 812 */
																															long
																																BgL_arg1669z00_1887;
																															long
																																BgL_arg1670z00_1888;
																															long
																																BgL_arg1675z00_1889;
																															BgL_arg1669z00_1887
																																=
																																(BgL_jz00_1879 +
																																1L);
																															BgL_arg1670z00_1888
																																=
																																(BgL_kz00_1880 +
																																1L);
																															BgL_arg1675z00_1889
																																=
																																(BgL_wz00_1884 /
																																16384L);
																															{
																																long
																																	BgL_cz00_7333;
																																long
																																	BgL_kz00_7332;
																																long
																																	BgL_jz00_7331;
																																BgL_jz00_7331 =
																																	BgL_arg1669z00_1887;
																																BgL_kz00_7332 =
																																	BgL_arg1670z00_1888;
																																BgL_cz00_7333 =
																																	BgL_arg1675z00_1889;
																																BgL_cz00_1881 =
																																	BgL_cz00_7333;
																																BgL_kz00_1880 =
																																	BgL_kz00_7332;
																																BgL_jz00_1879 =
																																	BgL_jz00_7331;
																																goto
																																	BgL_zc3z04anonymousza31664ze3z87_1882;
																															}
																														}
																													}
																												else
																													{	/* Unsafe/bignumber.scm 819 */
																														obj_t
																															BgL_arg1685z00_1894;
																														{	/* Unsafe/bignumber.scm 819 */
																															long
																																BgL_arg1688z00_1895;
																															{	/* Unsafe/bignumber.scm 819 */
																																long
																																	BgL_arg1691z00_1897;
																																{	/* Unsafe/bignumber.scm 819 */
																																	int
																																		BgL_iz00_4363;
																																	BgL_iz00_4363
																																		=
																																		(int)
																																		(BgL_kz00_1880);
																																	{	/* Unsafe/bignumber.scm 267 */
																																		uint16_t
																																			BgL_arg1220z00_4364;
																																		{	/* Unsafe/bignumber.scm 267 */
																																			obj_t
																																				BgL_arg1221z00_4365;
																																			BgL_arg1221z00_4365
																																				=
																																				BGL_BIGNUM_U16VECT
																																				(BgL_nxz00_1808);
																																			{	/* Unsafe/bignumber.scm 267 */
																																				long
																																					BgL_tmpz00_7336;
																																				BgL_tmpz00_7336
																																					=
																																					(long)
																																					(BgL_iz00_4363);
																																				BgL_arg1220z00_4364
																																					=
																																					BGL_U16VREF
																																					(BgL_arg1221z00_4365,
																																					BgL_tmpz00_7336);
																																		}}
																																		{	/* Unsafe/bignumber.scm 267 */
																																			long
																																				BgL_arg2271z00_4367;
																																			BgL_arg2271z00_4367
																																				=
																																				(long)
																																				(BgL_arg1220z00_4364);
																																			BgL_arg1691z00_1897
																																				=
																																				(long)
																																				(BgL_arg2271z00_4367);
																																}}}
																																BgL_arg1688z00_1895
																																	=
																																	(BgL_arg1691z00_1897
																																	+
																																	BgL_cz00_1881);
																															}
																															BgL_arg1685z00_1894
																																=
																																BGl_moduloz00zz__r4_numbers_6_5_fixnumz00
																																(BINT
																																(BgL_arg1688z00_1895),
																																BINT(16384L));
																														}
																														{	/* Unsafe/bignumber.scm 815 */
																															int BgL_iz00_4372;
																															int
																																BgL_digitz00_4373;
																															BgL_iz00_4372 =
																																(int)
																																(BgL_kz00_1880);
																															BgL_digitz00_4373
																																=
																																CINT
																																(BgL_arg1685z00_1894);
																															{	/* Unsafe/bignumber.scm 269 */
																																obj_t
																																	BgL_arg1223z00_4374;
																																uint16_t
																																	BgL_arg1225z00_4375;
																																BgL_arg1223z00_4374
																																	=
																																	BGL_BIGNUM_U16VECT
																																	(BgL_nxz00_1808);
																																{	/* Unsafe/bignumber.scm 269 */
																																	long
																																		BgL_tmpz00_7348;
																																	BgL_tmpz00_7348
																																		=
																																		(long)
																																		(BgL_digitz00_4373);
																																	BgL_arg1225z00_4375
																																		=
																																		(uint16_t)
																																		(BgL_tmpz00_7348);
																																}
																																{	/* Unsafe/bignumber.scm 269 */
																																	long
																																		BgL_tmpz00_7351;
																																	BgL_tmpz00_7351
																																		=
																																		(long)
																																		(BgL_iz00_4372);
																																	BGL_U16VSET
																																		(BgL_arg1223z00_4374,
																																		BgL_tmpz00_7351,
																																		BgL_arg1225z00_4375);
																																} BUNSPEC;
																									}}}}}}
																								else
																									{	/* Unsafe/bignumber.scm 821 */
																										long BgL_arg1699z00_1900;

																										BgL_arg1699z00_1900 =
																											(BgL_iz00_1840 -
																											(BgL_lenyz00_1796 - 1L));
																										{	/* Unsafe/bignumber.scm 821 */
																											int BgL_iz00_4381;
																											int BgL_digitz00_4382;

																											BgL_iz00_4381 =
																												(int)
																												(BgL_arg1699z00_1900);
																											BgL_digitz00_4382 =
																												(int) (BgL_qz00_1849);
																											{	/* Unsafe/bignumber.scm 269 */
																												obj_t
																													BgL_arg1223z00_4383;
																												uint16_t
																													BgL_arg1225z00_4384;
																												BgL_arg1223z00_4383 =
																													BGL_BIGNUM_U16VECT
																													(BgL_rz00_1797);
																												{	/* Unsafe/bignumber.scm 269 */
																													long BgL_tmpz00_7359;

																													BgL_tmpz00_7359 =
																														(long)
																														(BgL_digitz00_4382);
																													BgL_arg1225z00_4384 =
																														(uint16_t)
																														(BgL_tmpz00_7359);
																												}
																												{	/* Unsafe/bignumber.scm 269 */
																													long BgL_tmpz00_7362;

																													BgL_tmpz00_7362 =
																														(long)
																														(BgL_iz00_4381);
																													BGL_U16VSET
																														(BgL_arg1223z00_4383,
																														BgL_tmpz00_7362,
																														BgL_arg1225z00_4384);
																												} BUNSPEC;
																									}}}
																								{
																									long BgL_iz00_7365;

																									BgL_iz00_7365 =
																										(BgL_iz00_1840 - 1L);
																									BgL_iz00_1840 = BgL_iz00_7365;
																									goto
																										BgL_zc3z04anonymousza31637ze3z87_1841;
																								}
																							}
																					}
																				}
																			}
																		}
																	}
															}
															{	/* Unsafe/bignumber.scm 824 */
																long BgL_g1056z00_1940;

																BgL_g1056z00_1940 = (BgL_lenyz00_1796 - 1L);
																{
																	long BgL_iz00_1942;
																	long BgL_kz00_1943;

																	BgL_iz00_1942 = BgL_g1056z00_1940;
																	BgL_kz00_1943 = 0L;
																BgL_zc3z04anonymousza31731ze3z87_1944:
																	if ((0L < BgL_iz00_1942))
																		{	/* Unsafe/bignumber.scm 826 */
																			long BgL_wz00_1946;

																			{	/* Unsafe/bignumber.scm 826 */
																				long BgL_arg1736z00_1950;
																				long BgL_arg1737z00_1951;

																				BgL_arg1736z00_1950 =
																					(BgL_kz00_1943 * 16384L);
																				{	/* Unsafe/bignumber.scm 827 */
																					int BgL_iz00_4392;

																					BgL_iz00_4392 = (int) (BgL_iz00_1942);
																					{	/* Unsafe/bignumber.scm 267 */
																						uint16_t BgL_arg1220z00_4393;

																						{	/* Unsafe/bignumber.scm 267 */
																							obj_t BgL_arg1221z00_4394;

																							BgL_arg1221z00_4394 =
																								BGL_BIGNUM_U16VECT
																								(BgL_nxz00_1808);
																							{	/* Unsafe/bignumber.scm 267 */
																								long BgL_tmpz00_7373;

																								BgL_tmpz00_7373 =
																									(long) (BgL_iz00_4392);
																								BgL_arg1220z00_4393 =
																									BGL_U16VREF
																									(BgL_arg1221z00_4394,
																									BgL_tmpz00_7373);
																						}}
																						{	/* Unsafe/bignumber.scm 267 */
																							long BgL_arg2271z00_4396;

																							BgL_arg2271z00_4396 =
																								(long) (BgL_arg1220z00_4393);
																							BgL_arg1737z00_1951 =
																								(long) (BgL_arg2271z00_4396);
																				}}}
																				BgL_wz00_1946 =
																					(BgL_arg1736z00_1950 +
																					BgL_arg1737z00_1951);
																			}
																			{	/* Unsafe/bignumber.scm 828 */
																				long BgL_arg1733z00_1947;

																				BgL_arg1733z00_1947 =
																					(BgL_wz00_1946 / BgL_shiftz00_1801);
																				{	/* Unsafe/bignumber.scm 828 */
																					int BgL_iz00_4403;
																					int BgL_digitz00_4404;

																					BgL_iz00_4403 = (int) (BgL_iz00_1942);
																					BgL_digitz00_4404 =
																						(int) (BgL_arg1733z00_1947);
																					{	/* Unsafe/bignumber.scm 269 */
																						obj_t BgL_arg1223z00_4405;
																						uint16_t BgL_arg1225z00_4406;

																						BgL_arg1223z00_4405 =
																							BGL_BIGNUM_U16VECT
																							(BgL_nxz00_1808);
																						{	/* Unsafe/bignumber.scm 269 */
																							long BgL_tmpz00_7383;

																							BgL_tmpz00_7383 =
																								(long) (BgL_digitz00_4404);
																							BgL_arg1225z00_4406 =
																								(uint16_t) (BgL_tmpz00_7383);
																						}
																						{	/* Unsafe/bignumber.scm 269 */
																							long BgL_tmpz00_7386;

																							BgL_tmpz00_7386 =
																								(long) (BgL_iz00_4403);
																							BGL_U16VSET(BgL_arg1223z00_4405,
																								BgL_tmpz00_7386,
																								BgL_arg1225z00_4406);
																						} BUNSPEC;
																			}}}
																			{	/* Unsafe/bignumber.scm 829 */
																				long BgL_arg1734z00_1948;
																				long BgL_arg1735z00_1949;

																				BgL_arg1734z00_1948 =
																					(BgL_iz00_1942 - 1L);
																				{	/* Unsafe/bignumber.scm 830 */
																					long BgL_n1z00_4409;
																					long BgL_n2z00_4410;

																					BgL_n1z00_4409 = BgL_wz00_1946;
																					BgL_n2z00_4410 = BgL_shiftz00_1801;
																					{	/* Unsafe/bignumber.scm 830 */
																						bool_t BgL_test2766z00_7390;

																						{	/* Unsafe/bignumber.scm 830 */
																							long BgL_arg2169z00_4412;

																							BgL_arg2169z00_4412 =
																								(((BgL_n1z00_4409) |
																									(BgL_n2z00_4410)) &
																								-2147483648);
																							BgL_test2766z00_7390 =
																								(BgL_arg2169z00_4412 == 0L);
																						}
																						if (BgL_test2766z00_7390)
																							{	/* Unsafe/bignumber.scm 830 */
																								int32_t BgL_arg2166z00_4413;

																								{	/* Unsafe/bignumber.scm 830 */
																									int32_t BgL_arg2167z00_4414;
																									int32_t BgL_arg2168z00_4415;

																									BgL_arg2167z00_4414 =
																										(int32_t) (BgL_n1z00_4409);
																									BgL_arg2168z00_4415 =
																										(int32_t) (BgL_n2z00_4410);
																									BgL_arg2166z00_4413 =
																										(BgL_arg2167z00_4414 %
																										BgL_arg2168z00_4415);
																								}
																								{	/* Unsafe/bignumber.scm 830 */
																									long BgL_arg2270z00_4420;

																									BgL_arg2270z00_4420 =
																										(long)
																										(BgL_arg2166z00_4413);
																									BgL_arg1735z00_1949 =
																										(long)
																										(BgL_arg2270z00_4420);
																							}}
																						else
																							{	/* Unsafe/bignumber.scm 830 */
																								BgL_arg1735z00_1949 =
																									(BgL_n1z00_4409 %
																									BgL_n2z00_4410);
																							}
																					}
																				}
																				{
																					long BgL_kz00_7400;
																					long BgL_iz00_7399;

																					BgL_iz00_7399 = BgL_arg1734z00_1948;
																					BgL_kz00_7400 = BgL_arg1735z00_1949;
																					BgL_kz00_1943 = BgL_kz00_7400;
																					BgL_iz00_1942 = BgL_iz00_7399;
																					goto
																						BgL_zc3z04anonymousza31731ze3z87_1944;
																				}
																			}
																		}
																	else
																		{	/* Unsafe/bignumber.scm 825 */
																			((bool_t) 0);
																		}
																}
															}
															return
																MAKE_YOUNG_PAIR
																(BGl_bignumzd2removezd2leadingzd2za7eroesz75zz__bignumz00
																(BgL_rz00_1797),
																BGl_bignumzd2removezd2leadingzd2za7eroesz75zz__bignumz00
																(BgL_nxz00_1808));
														}
												}
											}
										}
								}
						}
				}
			}
		}

	}



/* $$divrembx */
	BGL_EXPORTED_DEF obj_t bgl_bignum_div(obj_t BgL_xz00_67, obj_t BgL_yz00_68)
	{
		{	/* Unsafe/bignumber.scm 854 */
			{	/* Unsafe/bignumber.scm 855 */
				obj_t BgL_vz00_4478;

				BgL_vz00_4478 =
					BGl_bignumzd2divzd2zz__bignumz00(BgL_xz00_67, BgL_yz00_68);
				{	/* Unsafe/bignumber.scm 856 */
					obj_t BgL_val0_1099z00_4479;
					obj_t BgL_val1_1100z00_4480;

					BgL_val0_1099z00_4479 = CAR(((obj_t) BgL_vz00_4478));
					BgL_val1_1100z00_4480 = CDR(((obj_t) BgL_vz00_4478));
					{	/* Unsafe/bignumber.scm 856 */
						int BgL_tmpz00_7409;

						BgL_tmpz00_7409 = (int) (2L);
						BGL_MVALUES_NUMBER_SET(BgL_tmpz00_7409);
					}
					{	/* Unsafe/bignumber.scm 856 */
						int BgL_tmpz00_7412;

						BgL_tmpz00_7412 = (int) (1L);
						BGL_MVALUES_VAL_SET(BgL_tmpz00_7412, BgL_val1_1100z00_4480);
					}
					return BgL_val0_1099z00_4479;
				}
			}
		}

	}



/* &$$divrembx */
	obj_t BGl_z62z42z42divrembxz62zz__bignumz00(obj_t BgL_envz00_5543,
		obj_t BgL_xz00_5544, obj_t BgL_yz00_5545)
	{
		{	/* Unsafe/bignumber.scm 854 */
			{	/* Unsafe/bignumber.scm 855 */
				obj_t BgL_auxz00_7422;
				obj_t BgL_auxz00_7415;

				if (BIGNUMP(BgL_yz00_5545))
					{	/* Unsafe/bignumber.scm 855 */
						BgL_auxz00_7422 = BgL_yz00_5545;
					}
				else
					{
						obj_t BgL_auxz00_7425;

						BgL_auxz00_7425 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
							BINT(27993L), BGl_string2504z00zz__bignumz00,
							BGl_string2491z00zz__bignumz00, BgL_yz00_5545);
						FAILURE(BgL_auxz00_7425, BFALSE, BFALSE);
					}
				if (BIGNUMP(BgL_xz00_5544))
					{	/* Unsafe/bignumber.scm 855 */
						BgL_auxz00_7415 = BgL_xz00_5544;
					}
				else
					{
						obj_t BgL_auxz00_7418;

						BgL_auxz00_7418 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
							BINT(27993L), BGl_string2504z00zz__bignumz00,
							BGl_string2491z00zz__bignumz00, BgL_xz00_5544);
						FAILURE(BgL_auxz00_7418, BFALSE, BFALSE);
					}
				return bgl_bignum_div(BgL_auxz00_7415, BgL_auxz00_7422);
			}
		}

	}



/* $$quotientbx */
	BGL_EXPORTED_DEF obj_t bgl_bignum_quotient(obj_t BgL_xz00_69,
		obj_t BgL_yz00_70)
	{
		{	/* Unsafe/bignumber.scm 858 */
			{	/* Unsafe/bignumber.scm 859 */
				obj_t BgL_arg1756z00_4483;

				{	/* Unsafe/bignumber.scm 859 */
					obj_t BgL_arg1757z00_4484;

					BgL_arg1757z00_4484 =
						BGl_bignumzd2divzd2zz__bignumz00(BgL_xz00_69, BgL_yz00_70);
					BgL_arg1756z00_4483 = CAR(((obj_t) BgL_arg1757z00_4484));
				}
				return BgL_arg1756z00_4483;
			}
		}

	}



/* &$$quotientbx */
	obj_t BGl_z62z42z42quotientbxz62zz__bignumz00(obj_t BgL_envz00_5546,
		obj_t BgL_xz00_5547, obj_t BgL_yz00_5548)
	{
		{	/* Unsafe/bignumber.scm 858 */
			{	/* Unsafe/bignumber.scm 859 */
				obj_t BgL_auxz00_7440;
				obj_t BgL_auxz00_7433;

				if (BIGNUMP(BgL_yz00_5548))
					{	/* Unsafe/bignumber.scm 859 */
						BgL_auxz00_7440 = BgL_yz00_5548;
					}
				else
					{
						obj_t BgL_auxz00_7443;

						BgL_auxz00_7443 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
							BINT(28108L), BGl_string2505z00zz__bignumz00,
							BGl_string2491z00zz__bignumz00, BgL_yz00_5548);
						FAILURE(BgL_auxz00_7443, BFALSE, BFALSE);
					}
				if (BIGNUMP(BgL_xz00_5547))
					{	/* Unsafe/bignumber.scm 859 */
						BgL_auxz00_7433 = BgL_xz00_5547;
					}
				else
					{
						obj_t BgL_auxz00_7436;

						BgL_auxz00_7436 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
							BINT(28108L), BGl_string2505z00zz__bignumz00,
							BGl_string2491z00zz__bignumz00, BgL_xz00_5547);
						FAILURE(BgL_auxz00_7436, BFALSE, BFALSE);
					}
				return bgl_bignum_quotient(BgL_auxz00_7433, BgL_auxz00_7440);
			}
		}

	}



/* $$remainderbx */
	BGL_EXPORTED_DEF obj_t bgl_bignum_remainder(obj_t BgL_xz00_71,
		obj_t BgL_yz00_72)
	{
		{	/* Unsafe/bignumber.scm 861 */
			{	/* Unsafe/bignumber.scm 862 */
				obj_t BgL_arg1758z00_4486;

				{	/* Unsafe/bignumber.scm 862 */
					obj_t BgL_arg1759z00_4487;

					BgL_arg1759z00_4487 =
						BGl_bignumzd2divzd2zz__bignumz00(BgL_xz00_71, BgL_yz00_72);
					BgL_arg1758z00_4486 = CDR(((obj_t) BgL_arg1759z00_4487));
				}
				return BgL_arg1758z00_4486;
			}
		}

	}



/* &$$remainderbx */
	obj_t BGl_z62z42z42remainderbxz62zz__bignumz00(obj_t BgL_envz00_5549,
		obj_t BgL_xz00_5550, obj_t BgL_yz00_5551)
	{
		{	/* Unsafe/bignumber.scm 861 */
			{	/* Unsafe/bignumber.scm 862 */
				obj_t BgL_auxz00_7458;
				obj_t BgL_auxz00_7451;

				if (BIGNUMP(BgL_yz00_5551))
					{	/* Unsafe/bignumber.scm 862 */
						BgL_auxz00_7458 = BgL_yz00_5551;
					}
				else
					{
						obj_t BgL_auxz00_7461;

						BgL_auxz00_7461 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
							BINT(28183L), BGl_string2506z00zz__bignumz00,
							BGl_string2491z00zz__bignumz00, BgL_yz00_5551);
						FAILURE(BgL_auxz00_7461, BFALSE, BFALSE);
					}
				if (BIGNUMP(BgL_xz00_5550))
					{	/* Unsafe/bignumber.scm 862 */
						BgL_auxz00_7451 = BgL_xz00_5550;
					}
				else
					{
						obj_t BgL_auxz00_7454;

						BgL_auxz00_7454 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
							BINT(28183L), BGl_string2506z00zz__bignumz00,
							BGl_string2491z00zz__bignumz00, BgL_xz00_5550);
						FAILURE(BgL_auxz00_7454, BFALSE, BFALSE);
					}
				return bgl_bignum_remainder(BgL_auxz00_7451, BgL_auxz00_7458);
			}
		}

	}



/* $$absbx */
	BGL_EXPORTED_DEF obj_t bgl_bignum_abs(obj_t BgL_xz00_83)
	{
		{	/* Unsafe/bignumber.scm 893 */
			{	/* Unsafe/bignumber.scm 894 */
				bool_t BgL_test2773z00_7466;

				{	/* Unsafe/bignumber.scm 471 */
					long BgL_arg1428z00_4491;

					{	/* Unsafe/bignumber.scm 263 */
						uint16_t BgL_arg1215z00_4493;

						{	/* Unsafe/bignumber.scm 263 */
							obj_t BgL_arg1216z00_4494;

							BgL_arg1216z00_4494 = BGL_BIGNUM_U16VECT(BgL_xz00_83);
							BgL_arg1215z00_4493 = BGL_U16VREF(BgL_arg1216z00_4494, 0L);
						}
						{	/* Unsafe/bignumber.scm 263 */
							long BgL_arg2271z00_4496;

							BgL_arg2271z00_4496 = (long) (BgL_arg1215z00_4493);
							BgL_arg1428z00_4491 = (long) (BgL_arg2271z00_4496);
					}}
					BgL_test2773z00_7466 = (BgL_arg1428z00_4491 == 0L);
				}
				if (BgL_test2773z00_7466)
					{	/* Unsafe/bignumber.scm 894 */
						return
							bgl_bignum_sub(BGl_bignumzd2za7eroz75zz__bignumz00, BgL_xz00_83);
					}
				else
					{	/* Unsafe/bignumber.scm 894 */
						return BgL_xz00_83;
					}
			}
		}

	}



/* &$$absbx */
	obj_t BGl_z62z42z42absbxz62zz__bignumz00(obj_t BgL_envz00_5552,
		obj_t BgL_xz00_5553)
	{
		{	/* Unsafe/bignumber.scm 893 */
			{	/* Unsafe/bignumber.scm 894 */
				obj_t BgL_auxz00_7473;

				if (BIGNUMP(BgL_xz00_5553))
					{	/* Unsafe/bignumber.scm 894 */
						BgL_auxz00_7473 = BgL_xz00_5553;
					}
				else
					{
						obj_t BgL_auxz00_7476;

						BgL_auxz00_7476 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
							BINT(29024L), BGl_string2507z00zz__bignumz00,
							BGl_string2491z00zz__bignumz00, BgL_xz00_5553);
						FAILURE(BgL_auxz00_7476, BFALSE, BFALSE);
					}
				return bgl_bignum_abs(BgL_auxz00_7473);
			}
		}

	}



/* $$gcdbx */
	BGL_EXPORTED_DEF obj_t bgl_bignum_gcd(obj_t BgL_xz00_84, obj_t BgL_yz00_85)
	{
		{	/* Unsafe/bignumber.scm 898 */
			{	/* Unsafe/bignumber.scm 899 */
				obj_t BgL_g1057z00_4500;
				obj_t BgL_g1058z00_4501;

				if ((BGl_bignumzd2signzd2zz__bignumz00(BgL_xz00_84) == 0L))
					{	/* Unsafe/bignumber.scm 894 */
						BgL_g1057z00_4500 =
							bgl_bignum_sub(BGl_bignumzd2za7eroz75zz__bignumz00, BgL_xz00_84);
					}
				else
					{	/* Unsafe/bignumber.scm 894 */
						BgL_g1057z00_4500 = BgL_xz00_84;
					}
				if ((BGl_bignumzd2signzd2zz__bignumz00(BgL_yz00_85) == 0L))
					{	/* Unsafe/bignumber.scm 894 */
						BgL_g1058z00_4501 =
							bgl_bignum_sub(BGl_bignumzd2za7eroz75zz__bignumz00, BgL_yz00_85);
					}
				else
					{	/* Unsafe/bignumber.scm 894 */
						BgL_g1058z00_4501 = BgL_yz00_85;
					}
				if (BXZERO(BgL_g1058z00_4501))
					{	/* Unsafe/bignumber.scm 900 */
						return BgL_g1057z00_4500;
					}
				else
					{	/* Unsafe/bignumber.scm 902 */
						obj_t BgL_arg1786z00_4569;

						BgL_arg1786z00_4569 =
							bgl_bignum_remainder(BgL_g1057z00_4500, BgL_g1058z00_4501);
						{
							obj_t BgL_xz00_4551;
							obj_t BgL_yz00_4552;

							BgL_xz00_4551 = BgL_g1058z00_4501;
							BgL_yz00_4552 = BgL_arg1786z00_4569;
						BgL_loopz00_4550:
							if ((BGl_bignumzd2lengthzd2zz__bignumz00(BgL_yz00_4552) == 1L))
								{	/* Unsafe/bignumber.scm 900 */
									return BgL_xz00_4551;
								}
							else
								{	/* Unsafe/bignumber.scm 902 */
									obj_t BgL_arg1786z00_4556;

									{	/* Unsafe/bignumber.scm 902 */
										obj_t BgL_res2339z00_4565;

										{	/* Unsafe/bignumber.scm 862 */
											obj_t BgL_arg1758z00_4562;

											{	/* Unsafe/bignumber.scm 862 */
												obj_t BgL_arg1759z00_4563;

												BgL_arg1759z00_4563 =
													BGl_bignumzd2divzd2zz__bignumz00(BgL_xz00_4551,
													BgL_yz00_4552);
												BgL_arg1758z00_4562 =
													CDR(((obj_t) BgL_arg1759z00_4563));
											}
											BgL_res2339z00_4565 = BgL_arg1758z00_4562;
										}
										BgL_arg1786z00_4556 = BgL_res2339z00_4565;
									}
									if (BXZERO(BgL_arg1786z00_4556))
										{	/* Unsafe/bignumber.scm 900 */
											return BgL_yz00_4552;
										}
									else
										{
											obj_t BgL_yz00_7501;
											obj_t BgL_xz00_7500;

											BgL_xz00_7500 = BgL_arg1786z00_4556;
											BgL_yz00_7501 =
												bgl_bignum_remainder(BgL_yz00_4552,
												BgL_arg1786z00_4556);
											BgL_yz00_4552 = BgL_yz00_7501;
											BgL_xz00_4551 = BgL_xz00_7500;
											goto BgL_loopz00_4550;
										}
								}
						}
					}
			}
		}

	}



/* &$$gcdbx */
	obj_t BGl_z62z42z42gcdbxz62zz__bignumz00(obj_t BgL_envz00_5554,
		obj_t BgL_xz00_5555, obj_t BgL_yz00_5556)
	{
		{	/* Unsafe/bignumber.scm 898 */
			{	/* Unsafe/bignumber.scm 899 */
				obj_t BgL_auxz00_7510;
				obj_t BgL_auxz00_7503;

				if (BIGNUMP(BgL_yz00_5556))
					{	/* Unsafe/bignumber.scm 899 */
						BgL_auxz00_7510 = BgL_yz00_5556;
					}
				else
					{
						obj_t BgL_auxz00_7513;

						BgL_auxz00_7513 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
							BINT(29102L), BGl_string2508z00zz__bignumz00,
							BGl_string2491z00zz__bignumz00, BgL_yz00_5556);
						FAILURE(BgL_auxz00_7513, BFALSE, BFALSE);
					}
				if (BIGNUMP(BgL_xz00_5555))
					{	/* Unsafe/bignumber.scm 899 */
						BgL_auxz00_7503 = BgL_xz00_5555;
					}
				else
					{
						obj_t BgL_auxz00_7506;

						BgL_auxz00_7506 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
							BINT(29102L), BGl_string2508z00zz__bignumz00,
							BGl_string2491z00zz__bignumz00, BgL_xz00_5555);
						FAILURE(BgL_auxz00_7506, BFALSE, BFALSE);
					}
				return bgl_bignum_gcd(BgL_auxz00_7503, BgL_auxz00_7510);
			}
		}

	}



/* $$lcmbx */
	BGL_EXPORTED_DEF obj_t bgl_bignum_lcm(obj_t BgL_xz00_86, obj_t BgL_yz00_87)
	{
		{	/* Unsafe/bignumber.scm 904 */
			{	/* Unsafe/bignumber.scm 905 */
				bool_t BgL_test2782z00_7518;

				{	/* Unsafe/bignumber.scm 905 */
					bool_t BgL_test2783z00_7519;

					{	/* Unsafe/bignumber.scm 468 */
						long BgL_arg1427z00_4576;

						{	/* Unsafe/bignumber.scm 261 */
							long BgL_arg1210z00_4578;

							{	/* Unsafe/bignumber.scm 261 */
								obj_t BgL_arg1212z00_4579;

								BgL_arg1212z00_4579 = BGL_BIGNUM_U16VECT(BgL_xz00_86);
								BgL_arg1210z00_4578 = BGL_HVECTOR_LENGTH(BgL_arg1212z00_4579);
							}
							{	/* Unsafe/bignumber.scm 261 */
								uint16_t BgL_xz00_4581;

								BgL_xz00_4581 = (uint16_t) (BgL_arg1210z00_4578);
								{	/* Unsafe/bignumber.scm 261 */
									long BgL_arg2271z00_4582;

									BgL_arg2271z00_4582 = (long) (BgL_xz00_4581);
									BgL_arg1427z00_4576 = (long) (BgL_arg2271z00_4582);
						}}}
						BgL_test2783z00_7519 = (BgL_arg1427z00_4576 == 1L);
					}
					if (BgL_test2783z00_7519)
						{	/* Unsafe/bignumber.scm 905 */
							BgL_test2782z00_7518 = ((bool_t) 1);
						}
					else
						{	/* Unsafe/bignumber.scm 468 */
							long BgL_arg1427z00_4586;

							{	/* Unsafe/bignumber.scm 261 */
								long BgL_arg1210z00_4588;

								{	/* Unsafe/bignumber.scm 261 */
									obj_t BgL_arg1212z00_4589;

									BgL_arg1212z00_4589 = BGL_BIGNUM_U16VECT(BgL_yz00_87);
									BgL_arg1210z00_4588 = BGL_HVECTOR_LENGTH(BgL_arg1212z00_4589);
								}
								{	/* Unsafe/bignumber.scm 261 */
									uint16_t BgL_xz00_4591;

									BgL_xz00_4591 = (uint16_t) (BgL_arg1210z00_4588);
									{	/* Unsafe/bignumber.scm 261 */
										long BgL_arg2271z00_4592;

										BgL_arg2271z00_4592 = (long) (BgL_xz00_4591);
										BgL_arg1427z00_4586 = (long) (BgL_arg2271z00_4592);
							}}}
							BgL_test2782z00_7518 = (BgL_arg1427z00_4586 == 1L);
				}}
				if (BgL_test2782z00_7518)
					{	/* Unsafe/bignumber.scm 905 */
						return BGl_bignumzd2za7eroz75zz__bignumz00;
					}
				else
					{	/* Unsafe/bignumber.scm 908 */
						obj_t BgL_arg1789z00_4572;
						obj_t BgL_arg1790z00_4573;

						{	/* Unsafe/bignumber.scm 908 */
							obj_t BgL_arg1791z00_4574;

							BgL_arg1791z00_4574 = bgl_bignum_mul(BgL_xz00_86, BgL_yz00_87);
							if (
								(BGl_bignumzd2signzd2zz__bignumz00(BgL_arg1791z00_4574) == 0L))
								{	/* Unsafe/bignumber.scm 894 */
									BgL_arg1789z00_4572 =
										bgl_bignum_sub(BGl_bignumzd2za7eroz75zz__bignumz00,
										BgL_arg1791z00_4574);
								}
							else
								{	/* Unsafe/bignumber.scm 894 */
									BgL_arg1789z00_4572 = BgL_arg1791z00_4574;
								}
						}
						{	/* Unsafe/bignumber.scm 909 */
							obj_t BgL_res2341z00_4636;

							{	/* Unsafe/bignumber.scm 899 */
								obj_t BgL_g1057z00_4603;
								obj_t BgL_g1058z00_4604;

								if (BXNEGATIVE(BgL_xz00_86))
									{	/* Unsafe/bignumber.scm 894 */
										BgL_g1057z00_4603 =
											bgl_bignum_sub(BGl_bignumzd2za7eroz75zz__bignumz00,
											BgL_xz00_86);
									}
								else
									{	/* Unsafe/bignumber.scm 894 */
										BgL_g1057z00_4603 = BgL_xz00_86;
									}
								if (BXNEGATIVE(BgL_yz00_87))
									{	/* Unsafe/bignumber.scm 894 */
										BgL_g1058z00_4604 =
											bgl_bignum_sub(BGl_bignumzd2za7eroz75zz__bignumz00,
											BgL_yz00_87);
									}
								else
									{	/* Unsafe/bignumber.scm 894 */
										BgL_g1058z00_4604 = BgL_yz00_87;
									}
								{
									obj_t BgL_xz00_4630;
									obj_t BgL_yz00_4631;

									BgL_xz00_4630 = BgL_g1057z00_4603;
									BgL_yz00_4631 = BgL_g1058z00_4604;
								BgL_loopz00_4629:
									if (BXZERO(BgL_yz00_4631))
										{	/* Unsafe/bignumber.scm 900 */
											BgL_res2341z00_4636 = BgL_xz00_4630;
										}
									else
										{
											obj_t BgL_yz00_7546;
											obj_t BgL_xz00_7545;

											BgL_xz00_7545 = BgL_yz00_4631;
											BgL_yz00_7546 =
												bgl_bignum_remainder(BgL_xz00_4630, BgL_yz00_4631);
											BgL_yz00_4631 = BgL_yz00_7546;
											BgL_xz00_4630 = BgL_xz00_7545;
											goto BgL_loopz00_4629;
										}
								}
							}
							BgL_arg1790z00_4573 = BgL_res2341z00_4636;
						}
						{	/* Unsafe/bignumber.scm 907 */
							obj_t BgL_res2342z00_4642;

							{	/* Unsafe/bignumber.scm 859 */
								obj_t BgL_arg1756z00_4639;

								{	/* Unsafe/bignumber.scm 859 */
									obj_t BgL_arg1757z00_4640;

									BgL_arg1757z00_4640 =
										BGl_bignumzd2divzd2zz__bignumz00(BgL_arg1789z00_4572,
										BgL_arg1790z00_4573);
									BgL_arg1756z00_4639 = CAR(((obj_t) BgL_arg1757z00_4640));
								}
								BgL_res2342z00_4642 = BgL_arg1756z00_4639;
							}
							return BgL_res2342z00_4642;
						}
					}
			}
		}

	}



/* &$$lcmbx */
	obj_t BGl_z62z42z42lcmbxz62zz__bignumz00(obj_t BgL_envz00_5557,
		obj_t BgL_xz00_5558, obj_t BgL_yz00_5559)
	{
		{	/* Unsafe/bignumber.scm 904 */
			{	/* Unsafe/bignumber.scm 905 */
				obj_t BgL_auxz00_7558;
				obj_t BgL_auxz00_7551;

				if (BIGNUMP(BgL_yz00_5559))
					{	/* Unsafe/bignumber.scm 905 */
						BgL_auxz00_7558 = BgL_yz00_5559;
					}
				else
					{
						obj_t BgL_auxz00_7561;

						BgL_auxz00_7561 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
							BINT(29236L), BGl_string2509z00zz__bignumz00,
							BGl_string2491z00zz__bignumz00, BgL_yz00_5559);
						FAILURE(BgL_auxz00_7561, BFALSE, BFALSE);
					}
				if (BIGNUMP(BgL_xz00_5558))
					{	/* Unsafe/bignumber.scm 905 */
						BgL_auxz00_7551 = BgL_xz00_5558;
					}
				else
					{
						obj_t BgL_auxz00_7554;

						BgL_auxz00_7554 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
							BINT(29236L), BGl_string2509z00zz__bignumz00,
							BGl_string2491z00zz__bignumz00, BgL_xz00_5558);
						FAILURE(BgL_auxz00_7554, BFALSE, BFALSE);
					}
				return bgl_bignum_lcm(BgL_auxz00_7551, BgL_auxz00_7558);
			}
		}

	}



/* bignum-expt */
	obj_t BGl_bignumzd2exptzd2zz__bignumz00(obj_t BgL_xz00_88, obj_t BgL_yz00_89)
	{
		{	/* Unsafe/bignumber.scm 914 */
		BGl_bignumzd2exptzd2zz__bignumz00:
			{	/* Unsafe/bignumber.scm 915 */
				bool_t BgL_test2790z00_7566;

				{	/* Unsafe/bignumber.scm 468 */
					long BgL_arg1427z00_4644;

					{	/* Unsafe/bignumber.scm 261 */
						long BgL_arg1210z00_4646;

						{	/* Unsafe/bignumber.scm 261 */
							obj_t BgL_arg1212z00_4647;

							BgL_arg1212z00_4647 = BGL_BIGNUM_U16VECT(BgL_yz00_89);
							BgL_arg1210z00_4646 = BGL_HVECTOR_LENGTH(BgL_arg1212z00_4647);
						}
						{	/* Unsafe/bignumber.scm 261 */
							uint16_t BgL_xz00_4649;

							BgL_xz00_4649 = (uint16_t) (BgL_arg1210z00_4646);
							{	/* Unsafe/bignumber.scm 261 */
								long BgL_arg2271z00_4650;

								BgL_arg2271z00_4650 = (long) (BgL_xz00_4649);
								BgL_arg1427z00_4644 = (long) (BgL_arg2271z00_4650);
					}}}
					BgL_test2790z00_7566 = (BgL_arg1427z00_4644 == 1L);
				}
				if (BgL_test2790z00_7566)
					{	/* Unsafe/bignumber.scm 916 */
						obj_t BgL_res2343z00_4661;

						{	/* Unsafe/bignumber.scm 415 */
							bool_t BgL_test2791z00_7573;

							if ((1L < -16L))
								{	/* Unsafe/bignumber.scm 415 */
									BgL_test2791z00_7573 = ((bool_t) 1);
								}
							else
								{	/* Unsafe/bignumber.scm 415 */
									BgL_test2791z00_7573 = (16L < 1L);
								}
							if (BgL_test2791z00_7573)
								{	/* Unsafe/bignumber.scm 415 */
									BgL_res2343z00_4661 =
										BGl_z42z42fixnumzd2ze3bignumzd2freshze3zz__bignumz00(1L);
								}
							else
								{	/* Unsafe/bignumber.scm 415 */
									BgL_res2343z00_4661 =
										VECTOR_REF(BGl_preallocatedzd2bignumszd2zz__bignumz00,
										(1L + 16L));
								}
						}
						return BgL_res2343z00_4661;
					}
				else
					{	/* Unsafe/bignumber.scm 917 */
						bool_t BgL_test2793z00_7580;

						{	/* Unsafe/bignumber.scm 917 */
							bool_t BgL_res2344z00_4674;

							{	/* Unsafe/bignumber.scm 477 */
								bool_t BgL__ortest_1046z00_4663;

								BgL__ortest_1046z00_4663 =
									(BGl_bignumzd2lengthzd2zz__bignumz00(BgL_yz00_89) == 1L);
								if (BgL__ortest_1046z00_4663)
									{	/* Unsafe/bignumber.scm 477 */
										BgL_res2344z00_4674 = BgL__ortest_1046z00_4663;
									}
								else
									{	/* Unsafe/bignumber.scm 477 */
										long BgL_arg1431z00_4664;

										{	/* Unsafe/bignumber.scm 267 */
											uint16_t BgL_arg1220z00_4669;

											{	/* Unsafe/bignumber.scm 267 */
												obj_t BgL_arg1221z00_4670;

												BgL_arg1221z00_4670 = BGL_BIGNUM_U16VECT(BgL_yz00_89);
												BgL_arg1220z00_4669 =
													BGL_U16VREF(BgL_arg1221z00_4670, 1L);
											}
											{	/* Unsafe/bignumber.scm 267 */
												long BgL_arg2271z00_4672;

												BgL_arg2271z00_4672 = (long) (BgL_arg1220z00_4669);
												BgL_arg1431z00_4664 = (long) (BgL_arg2271z00_4672);
										}}
										BgL_res2344z00_4674 =
											BGl_evenzf3zf3zz__r4_numbers_6_5_fixnumz00(BINT
											(BgL_arg1431z00_4664));
							}}
							BgL_test2793z00_7580 = BgL_res2344z00_4674;
						}
						if (BgL_test2793z00_7580)
							{	/* Unsafe/bignumber.scm 919 */
								obj_t BgL_arg1794z00_2035;
								obj_t BgL_arg1795z00_2036;

								BgL_arg1794z00_2035 = bgl_bignum_mul(BgL_xz00_88, BgL_xz00_88);
								{	/* Unsafe/bignumber.scm 920 */
									obj_t BgL_arg1796z00_2037;

									{	/* Unsafe/bignumber.scm 920 */
										obj_t BgL_res2345z00_4683;

										{	/* Unsafe/bignumber.scm 415 */
											bool_t BgL_test2795z00_7591;

											if ((2L < -16L))
												{	/* Unsafe/bignumber.scm 415 */
													BgL_test2795z00_7591 = ((bool_t) 1);
												}
											else
												{	/* Unsafe/bignumber.scm 415 */
													BgL_test2795z00_7591 = (16L < 2L);
												}
											if (BgL_test2795z00_7591)
												{	/* Unsafe/bignumber.scm 415 */
													BgL_res2345z00_4683 =
														BGl_z42z42fixnumzd2ze3bignumzd2freshze3zz__bignumz00
														(2L);
												}
											else
												{	/* Unsafe/bignumber.scm 415 */
													BgL_res2345z00_4683 =
														VECTOR_REF
														(BGl_preallocatedzd2bignumszd2zz__bignumz00,
														(2L + 16L));
												}
										}
										BgL_arg1796z00_2037 = BgL_res2345z00_4683;
									}
									{	/* Unsafe/bignumber.scm 920 */
										obj_t BgL_res2346z00_4689;

										{	/* Unsafe/bignumber.scm 859 */
											obj_t BgL_arg1756z00_4686;

											{	/* Unsafe/bignumber.scm 859 */
												obj_t BgL_arg1757z00_4687;

												BgL_arg1757z00_4687 =
													BGl_bignumzd2divzd2zz__bignumz00(BgL_yz00_89,
													BgL_arg1796z00_2037);
												BgL_arg1756z00_4686 =
													CAR(((obj_t) BgL_arg1757z00_4687));
											}
											BgL_res2346z00_4689 = BgL_arg1756z00_4686;
										}
										BgL_arg1795z00_2036 = BgL_res2346z00_4689;
									}
								}
								{
									obj_t BgL_yz00_7602;
									obj_t BgL_xz00_7601;

									BgL_xz00_7601 = BgL_arg1794z00_2035;
									BgL_yz00_7602 = BgL_arg1795z00_2036;
									BgL_yz00_89 = BgL_yz00_7602;
									BgL_xz00_88 = BgL_xz00_7601;
									goto BGl_bignumzd2exptzd2zz__bignumz00;
								}
							}
						else
							{	/* Unsafe/bignumber.scm 922 */
								obj_t BgL_arg1797z00_2038;

								{	/* Unsafe/bignumber.scm 922 */
									obj_t BgL_arg1798z00_2039;

									{	/* Unsafe/bignumber.scm 922 */
										obj_t BgL_arg1799z00_2040;

										{	/* Unsafe/bignumber.scm 922 */
											obj_t BgL_res2347z00_4698;

											{	/* Unsafe/bignumber.scm 415 */
												bool_t BgL_test2797z00_7603;

												if ((1L < -16L))
													{	/* Unsafe/bignumber.scm 415 */
														BgL_test2797z00_7603 = ((bool_t) 1);
													}
												else
													{	/* Unsafe/bignumber.scm 415 */
														BgL_test2797z00_7603 = (16L < 1L);
													}
												if (BgL_test2797z00_7603)
													{	/* Unsafe/bignumber.scm 415 */
														BgL_res2347z00_4698 =
															BGl_z42z42fixnumzd2ze3bignumzd2freshze3zz__bignumz00
															(1L);
													}
												else
													{	/* Unsafe/bignumber.scm 415 */
														BgL_res2347z00_4698 =
															VECTOR_REF
															(BGl_preallocatedzd2bignumszd2zz__bignumz00,
															(1L + 16L));
													}
											}
											BgL_arg1799z00_2040 = BgL_res2347z00_4698;
										}
										{	/* Unsafe/bignumber.scm 922 */
											obj_t BgL_res2348z00_4717;

											{	/* Unsafe/bignumber.scm 610 */
												long BgL_arg1513z00_4701;
												long BgL_arg1514z00_4702;

												{	/* Unsafe/bignumber.scm 263 */
													uint16_t BgL_arg1215z00_4705;

													{	/* Unsafe/bignumber.scm 263 */
														obj_t BgL_arg1216z00_4706;

														BgL_arg1216z00_4706 =
															BGL_BIGNUM_U16VECT(BgL_yz00_89);
														BgL_arg1215z00_4705 =
															BGL_U16VREF(BgL_arg1216z00_4706, 0L);
													}
													{	/* Unsafe/bignumber.scm 263 */
														long BgL_arg2271z00_4708;

														BgL_arg2271z00_4708 = (long) (BgL_arg1215z00_4705);
														BgL_arg1513z00_4701 = (long) (BgL_arg2271z00_4708);
												}}
												{	/* Unsafe/bignumber.scm 610 */
													long BgL_arg1516z00_4703;

													{	/* Unsafe/bignumber.scm 263 */
														uint16_t BgL_arg1215z00_4711;

														{	/* Unsafe/bignumber.scm 263 */
															obj_t BgL_arg1216z00_4712;

															BgL_arg1216z00_4712 =
																BGL_BIGNUM_U16VECT(BgL_arg1799z00_2040);
															BgL_arg1215z00_4711 =
																BGL_U16VREF(BgL_arg1216z00_4712, 0L);
														}
														{	/* Unsafe/bignumber.scm 263 */
															long BgL_arg2271z00_4714;

															BgL_arg2271z00_4714 =
																(long) (BgL_arg1215z00_4711);
															BgL_arg1516z00_4703 =
																(long) (BgL_arg2271z00_4714);
													}}
													BgL_arg1514z00_4702 = (1L - BgL_arg1516z00_4703);
												}
												BgL_res2348z00_4717 =
													BGl_bignumzd2sum2zd2zz__bignumz00(BgL_yz00_89,
													BgL_arg1799z00_2040, BgL_arg1513z00_4701,
													BgL_arg1514z00_4702);
											}
											BgL_arg1798z00_2039 = BgL_res2348z00_4717;
									}}
									BgL_arg1797z00_2038 =
										BGl_bignumzd2exptzd2zz__bignumz00(BgL_xz00_88,
										BgL_arg1798z00_2039);
								}
								BGL_TAIL return
									bgl_bignum_mul(BgL_xz00_88, BgL_arg1797z00_2038);
							}
					}
			}
		}

	}



/* bignum-integer-length */
	long BGl_bignumzd2integerzd2lengthz00zz__bignumz00(obj_t BgL_xz00_91)
	{
		{	/* Unsafe/bignumber.scm 933 */
			{	/* Unsafe/bignumber.scm 934 */
				obj_t BgL_nz00_2042;

				{	/* Unsafe/bignumber.scm 934 */
					bool_t BgL_test2799z00_7622;

					{	/* Unsafe/bignumber.scm 471 */
						long BgL_arg1428z00_4748;

						{	/* Unsafe/bignumber.scm 263 */
							uint16_t BgL_arg1215z00_4750;

							{	/* Unsafe/bignumber.scm 263 */
								obj_t BgL_arg1216z00_4751;

								BgL_arg1216z00_4751 = BGL_BIGNUM_U16VECT(BgL_xz00_91);
								BgL_arg1215z00_4750 = BGL_U16VREF(BgL_arg1216z00_4751, 0L);
							}
							{	/* Unsafe/bignumber.scm 263 */
								long BgL_arg2271z00_4753;

								BgL_arg2271z00_4753 = (long) (BgL_arg1215z00_4750);
								BgL_arg1428z00_4748 = (long) (BgL_arg2271z00_4753);
						}}
						BgL_test2799z00_7622 = (BgL_arg1428z00_4748 == 0L);
					}
					if (BgL_test2799z00_7622)
						{	/* Unsafe/bignumber.scm 934 */
							BgL_nz00_2042 =
								bgl_bignum_sub(bgl_long_to_bignum(-1L), BgL_xz00_91);
						}
					else
						{	/* Unsafe/bignumber.scm 934 */
							BgL_nz00_2042 = BgL_xz00_91;
						}
				}
				{	/* Unsafe/bignumber.scm 934 */
					long BgL_nbzd2digitszd2_2043;

					{	/* Unsafe/bignumber.scm 935 */
						long BgL_arg1808z00_2057;

						{	/* Unsafe/bignumber.scm 261 */
							long BgL_arg1210z00_4758;

							{	/* Unsafe/bignumber.scm 261 */
								obj_t BgL_arg1212z00_4759;

								BgL_arg1212z00_4759 = BGL_BIGNUM_U16VECT(BgL_nz00_2042);
								BgL_arg1210z00_4758 = BGL_HVECTOR_LENGTH(BgL_arg1212z00_4759);
							}
							{	/* Unsafe/bignumber.scm 261 */
								uint16_t BgL_xz00_4761;

								BgL_xz00_4761 = (uint16_t) (BgL_arg1210z00_4758);
								{	/* Unsafe/bignumber.scm 261 */
									long BgL_arg2271z00_4762;

									BgL_arg2271z00_4762 = (long) (BgL_xz00_4761);
									BgL_arg1808z00_2057 = (long) (BgL_arg2271z00_4762);
						}}}
						BgL_nbzd2digitszd2_2043 = (BgL_arg1808z00_2057 - 1L);
					}
					{	/* Unsafe/bignumber.scm 935 */

						if ((BgL_nbzd2digitszd2_2043 == 0L))
							{	/* Unsafe/bignumber.scm 936 */
								return 0L;
							}
						else
							{	/* Unsafe/bignumber.scm 938 */
								long BgL_g1059z00_2045;
								long BgL_g1060z00_2046;

								{	/* Unsafe/bignumber.scm 938 */
									int BgL_iz00_4767;

									BgL_iz00_4767 = (int) (BgL_nbzd2digitszd2_2043);
									{	/* Unsafe/bignumber.scm 267 */
										uint16_t BgL_arg1220z00_4768;

										{	/* Unsafe/bignumber.scm 267 */
											obj_t BgL_arg1221z00_4769;

											BgL_arg1221z00_4769 = BGL_BIGNUM_U16VECT(BgL_nz00_2042);
											{	/* Unsafe/bignumber.scm 267 */
												long BgL_tmpz00_7640;

												BgL_tmpz00_7640 = (long) (BgL_iz00_4767);
												BgL_arg1220z00_4768 =
													BGL_U16VREF(BgL_arg1221z00_4769, BgL_tmpz00_7640);
										}}
										{	/* Unsafe/bignumber.scm 267 */
											long BgL_arg2271z00_4771;

											BgL_arg2271z00_4771 = (long) (BgL_arg1220z00_4768);
											BgL_g1059z00_2045 = (long) (BgL_arg2271z00_4771);
								}}}
								BgL_g1060z00_2046 = ((BgL_nbzd2digitszd2_2043 - 1L) * 14L);
								{
									long BgL_dz00_4789;
									long BgL_lenz00_4790;

									BgL_dz00_4789 = BgL_g1059z00_2045;
									BgL_lenz00_4790 = BgL_g1060z00_2046;
								BgL_loopz00_4788:
									if ((0L < BgL_dz00_4789))
										{	/* Unsafe/bignumber.scm 941 */
											long BgL_arg1804z00_4795;
											long BgL_arg1805z00_4796;

											BgL_arg1804z00_4795 = (BgL_dz00_4789 / 2L);
											BgL_arg1805z00_4796 = (BgL_lenz00_4790 + 1L);
											{
												long BgL_lenz00_7652;
												long BgL_dz00_7651;

												BgL_dz00_7651 = BgL_arg1804z00_4795;
												BgL_lenz00_7652 = BgL_arg1805z00_4796;
												BgL_lenz00_4790 = BgL_lenz00_7652;
												BgL_dz00_4789 = BgL_dz00_7651;
												goto BgL_loopz00_4788;
											}
										}
									else
										{	/* Unsafe/bignumber.scm 940 */
											return BgL_lenz00_4790;
										}
								}
							}
					}
				}
			}
		}

	}



/* $$bignum->string */
	BGL_EXPORTED_DEF obj_t bgl_bignum_to_string(obj_t BgL_xz00_92,
		long BgL_radixz00_93)
	{
		{	/* Unsafe/bignumber.scm 948 */
			{
				obj_t BgL_signz00_2065;
				obj_t BgL_nz00_2066;

				{	/* Unsafe/bignumber.scm 961 */
					bool_t BgL_test2802z00_7653;

					{	/* Unsafe/bignumber.scm 471 */
						long BgL_arg1428z00_4856;

						{	/* Unsafe/bignumber.scm 263 */
							uint16_t BgL_arg1215z00_4858;

							{	/* Unsafe/bignumber.scm 263 */
								obj_t BgL_arg1216z00_4859;

								BgL_arg1216z00_4859 = BGL_BIGNUM_U16VECT(BgL_xz00_92);
								BgL_arg1215z00_4858 = BGL_U16VREF(BgL_arg1216z00_4859, 0L);
							}
							{	/* Unsafe/bignumber.scm 263 */
								long BgL_arg2271z00_4861;

								BgL_arg2271z00_4861 = (long) (BgL_arg1215z00_4858);
								BgL_arg1428z00_4856 = (long) (BgL_arg2271z00_4861);
						}}
						BgL_test2802z00_7653 = (BgL_arg1428z00_4856 == 0L);
					}
					if (BgL_test2802z00_7653)
						{	/* Unsafe/bignumber.scm 961 */
							BgL_signz00_2065 = BCHAR(((unsigned char) '-'));
							BgL_nz00_2066 =
								bgl_bignum_sub(BGl_bignumzd2za7eroz75zz__bignumz00,
								BgL_xz00_92);
						BgL_zc3z04anonymousza31813ze3z87_2067:
							{	/* Unsafe/bignumber.scm 954 */
								obj_t BgL_digitsz00_2068;

								{	/* Unsafe/bignumber.scm 955 */
									obj_t BgL_l1101z00_2070;

									{	/* Unsafe/bignumber.scm 956 */
										bool_t BgL_test2803z00_7659;

										{	/* Unsafe/bignumber.scm 468 */
											long BgL_arg1427z00_4803;

											{	/* Unsafe/bignumber.scm 261 */
												long BgL_arg1210z00_4805;

												{	/* Unsafe/bignumber.scm 261 */
													obj_t BgL_arg1212z00_4806;

													BgL_arg1212z00_4806 =
														BGL_BIGNUM_U16VECT(BgL_nz00_2066);
													BgL_arg1210z00_4805 =
														BGL_HVECTOR_LENGTH(BgL_arg1212z00_4806);
												}
												{	/* Unsafe/bignumber.scm 261 */
													uint16_t BgL_xz00_4808;

													BgL_xz00_4808 = (uint16_t) (BgL_arg1210z00_4805);
													{	/* Unsafe/bignumber.scm 261 */
														long BgL_arg2271z00_4809;

														BgL_arg2271z00_4809 = (long) (BgL_xz00_4808);
														BgL_arg1427z00_4803 = (long) (BgL_arg2271z00_4809);
											}}}
											BgL_test2803z00_7659 = (BgL_arg1427z00_4803 == 1L);
										}
										if (BgL_test2803z00_7659)
											{	/* Unsafe/bignumber.scm 956 */
												BgL_l1101z00_2070 = BGl_list2510z00zz__bignumz00;
											}
										else
											{	/* Unsafe/bignumber.scm 956 */
												BgL_l1101z00_2070 =
													bgl_reverse
													(BGl_bignumzd2ze3fixnumzd2listze3zz__bignumz00
													(BgL_nz00_2066, (BgL_radixz00_93 - 1L)));
											}
									}
									if (NULLP(BgL_l1101z00_2070))
										{	/* Unsafe/bignumber.scm 955 */
											BgL_digitsz00_2068 = BNIL;
										}
									else
										{	/* Unsafe/bignumber.scm 955 */
											obj_t BgL_head1103z00_2072;

											{	/* Unsafe/bignumber.scm 955 */
												unsigned char BgL_arg1822z00_2084;

												BgL_arg1822z00_2084 =
													STRING_REF(BGl_string2511z00zz__bignumz00,
													(long) CINT(CAR(BgL_l1101z00_2070)));
												BgL_head1103z00_2072 =
													MAKE_YOUNG_PAIR(BCHAR(BgL_arg1822z00_2084), BNIL);
											}
											{	/* Unsafe/bignumber.scm 955 */
												obj_t BgL_g1106z00_2073;

												BgL_g1106z00_2073 = CDR(BgL_l1101z00_2070);
												{
													obj_t BgL_l1101z00_4838;
													obj_t BgL_tail1104z00_4839;

													BgL_l1101z00_4838 = BgL_g1106z00_2073;
													BgL_tail1104z00_4839 = BgL_head1103z00_2072;
												BgL_zc3z04anonymousza31816ze3z87_4837:
													if (NULLP(BgL_l1101z00_4838))
														{	/* Unsafe/bignumber.scm 955 */
															BgL_digitsz00_2068 = BgL_head1103z00_2072;
														}
													else
														{	/* Unsafe/bignumber.scm 955 */
															obj_t BgL_newtail1105z00_4846;

															{	/* Unsafe/bignumber.scm 955 */
																unsigned char BgL_arg1819z00_4847;

																BgL_arg1819z00_4847 =
																	STRING_REF(BGl_string2511z00zz__bignumz00,
																	(long) CINT(CAR(
																			((obj_t) BgL_l1101z00_4838))));
																BgL_newtail1105z00_4846 =
																	MAKE_YOUNG_PAIR(BCHAR(BgL_arg1819z00_4847),
																	BNIL);
															}
															SET_CDR(BgL_tail1104z00_4839,
																BgL_newtail1105z00_4846);
															{	/* Unsafe/bignumber.scm 955 */
																obj_t BgL_arg1818z00_4849;

																BgL_arg1818z00_4849 =
																	CDR(((obj_t) BgL_l1101z00_4838));
																{
																	obj_t BgL_tail1104z00_7689;
																	obj_t BgL_l1101z00_7688;

																	BgL_l1101z00_7688 = BgL_arg1818z00_4849;
																	BgL_tail1104z00_7689 =
																		BgL_newtail1105z00_4846;
																	BgL_tail1104z00_4839 = BgL_tail1104z00_7689;
																	BgL_l1101z00_4838 = BgL_l1101z00_7688;
																	goto BgL_zc3z04anonymousza31816ze3z87_4837;
																}
															}
														}
												}
											}
										}
								}
								{	/* Unsafe/bignumber.scm 959 */
									obj_t BgL_arg1814z00_2069;

									if (CBOOL(BgL_signz00_2065))
										{	/* Unsafe/bignumber.scm 959 */
											BgL_arg1814z00_2069 =
												MAKE_YOUNG_PAIR(BgL_signz00_2065, BgL_digitsz00_2068);
										}
									else
										{	/* Unsafe/bignumber.scm 959 */
											BgL_arg1814z00_2069 = BgL_digitsz00_2068;
										}
									return
										BGl_listzd2ze3stringz31zz__r4_strings_6_7z00
										(BgL_arg1814z00_2069);
								}
							}
						}
					else
						{
							obj_t BgL_nz00_7697;
							obj_t BgL_signz00_7696;

							BgL_signz00_7696 = BFALSE;
							BgL_nz00_7697 = BgL_xz00_92;
							BgL_nz00_2066 = BgL_nz00_7697;
							BgL_signz00_2065 = BgL_signz00_7696;
							goto BgL_zc3z04anonymousza31813ze3z87_2067;
						}
				}
			}
		}

	}



/* &$$bignum->string */
	obj_t BGl_z62z42z42bignumzd2ze3stringz53zz__bignumz00(obj_t BgL_envz00_5560,
		obj_t BgL_xz00_5561, obj_t BgL_radixz00_5562)
	{
		{	/* Unsafe/bignumber.scm 948 */
			{	/* Unsafe/bignumber.scm 951 */
				long BgL_auxz00_7705;
				obj_t BgL_auxz00_7698;

				{	/* Unsafe/bignumber.scm 951 */
					obj_t BgL_tmpz00_7706;

					if (INTEGERP(BgL_radixz00_5562))
						{	/* Unsafe/bignumber.scm 951 */
							BgL_tmpz00_7706 = BgL_radixz00_5562;
						}
					else
						{
							obj_t BgL_auxz00_7709;

							BgL_auxz00_7709 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
								BINT(31011L), BGl_string2512z00zz__bignumz00,
								BGl_string2480z00zz__bignumz00, BgL_radixz00_5562);
							FAILURE(BgL_auxz00_7709, BFALSE, BFALSE);
						}
					BgL_auxz00_7705 = (long) CINT(BgL_tmpz00_7706);
				}
				if (BIGNUMP(BgL_xz00_5561))
					{	/* Unsafe/bignumber.scm 951 */
						BgL_auxz00_7698 = BgL_xz00_5561;
					}
				else
					{
						obj_t BgL_auxz00_7701;

						BgL_auxz00_7701 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
							BINT(31011L), BGl_string2512z00zz__bignumz00,
							BGl_string2491z00zz__bignumz00, BgL_xz00_5561);
						FAILURE(BgL_auxz00_7701, BFALSE, BFALSE);
					}
				return bgl_bignum_to_string(BgL_auxz00_7698, BgL_auxz00_7705);
			}
		}

	}



/* $$string->bignum */
	BGL_EXPORTED_DEF obj_t bgl_string_to_bignum(char *BgL_strz00_94,
		int BgL_radixz00_95)
	{
		{	/* Unsafe/bignumber.scm 965 */
			{
				long BgL_iz00_2161;
				obj_t BgL_radz00_2145;
				long BgL_iz00_2146;
				obj_t BgL_radz00_2120;
				obj_t BgL_signz00_2121;
				long BgL_iz00_2122;
				unsigned char BgL_cz00_2095;
				obj_t BgL_radz00_2096;

				BgL_iz00_2161 = 0L;
				{	/* Unsafe/bignumber.scm 1005 */
					obj_t BgL_g1064z00_2163;

					if (
						((BgL_iz00_2161 + 3L) <=
							STRING_LENGTH(string_to_bstring(BgL_strz00_94))))
						{	/* Unsafe/bignumber.scm 1005 */
							if (
								(STRING_REF(string_to_bstring(BgL_strz00_94),
										BgL_iz00_2161) == ((unsigned char) '#')))
								{	/* Unsafe/bignumber.scm 1006 */
									BgL_g1064z00_2163 =
										BGl_assvz00zz__r4_pairs_and_lists_6_3z00(BCHAR(tolower
											(STRING_REF(string_to_bstring(BgL_strz00_94),
													(BgL_iz00_2161 + 1L)))),
										BGl_list2514z00zz__bignumz00);
								}
							else
								{	/* Unsafe/bignumber.scm 1006 */
									BgL_g1064z00_2163 = BFALSE;
								}
						}
					else
						{	/* Unsafe/bignumber.scm 1005 */
							BgL_g1064z00_2163 = BFALSE;
						}
					if (CBOOL(BgL_g1064z00_2163))
						{	/* Unsafe/bignumber.scm 1014 */
							obj_t BgL_arg1878z00_2166;
							long BgL_arg1879z00_2167;

							BgL_arg1878z00_2166 = CDR(((obj_t) BgL_g1064z00_2163));
							BgL_arg1879z00_2167 = (BgL_iz00_2161 + 2L);
							BgL_radz00_2145 = BgL_arg1878z00_2166;
							BgL_iz00_2146 = BgL_arg1879z00_2167;
						BgL_zc3z04anonymousza31863ze3z87_2147:
							{	/* Unsafe/bignumber.scm 998 */
								bool_t BgL_test2812z00_7735;

								if (
									((BgL_iz00_2146 + 2L) <=
										STRING_LENGTH(string_to_bstring(BgL_strz00_94))))
									{	/* Unsafe/bignumber.scm 998 */
										BgL_test2812z00_7735 =
											CBOOL(BGl_memvz00zz__r4_pairs_and_lists_6_3z00(BCHAR
												(STRING_REF(string_to_bstring(BgL_strz00_94),
														BgL_iz00_2146)), BGl_list2513z00zz__bignumz00));
									}
								else
									{	/* Unsafe/bignumber.scm 998 */
										BgL_test2812z00_7735 = ((bool_t) 0);
									}
								if (BgL_test2812z00_7735)
									{	/* Unsafe/bignumber.scm 1000 */
										unsigned char BgL_arg1872z00_2155;
										long BgL_arg1873z00_2156;

										BgL_arg1872z00_2155 =
											STRING_REF(string_to_bstring(BgL_strz00_94),
											BgL_iz00_2146);
										BgL_arg1873z00_2156 = (BgL_iz00_2146 + 1L);
										BgL_radz00_2120 = BgL_radz00_2145;
										BgL_signz00_2121 = BCHAR(BgL_arg1872z00_2155);
										BgL_iz00_2122 = BgL_arg1873z00_2156;
									BgL_zc3z04anonymousza31846ze3z87_2123:
										if (
											((BgL_iz00_2122 + 1L) <=
												STRING_LENGTH(string_to_bstring(BgL_strz00_94))))
											{
												long BgL_iz00_2129;
												obj_t BgL_digitsz00_2130;

												BgL_iz00_2129 = BgL_iz00_2122;
												BgL_digitsz00_2130 = BNIL;
											BgL_zc3z04anonymousza31850ze3z87_2131:
												if (
													(BgL_iz00_2129 <
														STRING_LENGTH(string_to_bstring(BgL_strz00_94))))
													{	/* Unsafe/bignumber.scm 987 */
														obj_t BgL_dz00_2134;

														BgL_cz00_2095 =
															STRING_REF(string_to_bstring(BgL_strz00_94),
															BgL_iz00_2129);
														BgL_radz00_2096 = BgL_radz00_2120;
														{	/* Unsafe/bignumber.scm 974 */
															bool_t BgL_test2816z00_7758;

															if ((BgL_cz00_2095 >= ((unsigned char) '0')))
																{	/* Unsafe/bignumber.scm 974 */
																	BgL_test2816z00_7758 =
																		(BgL_cz00_2095 <= ((unsigned char) '9'));
																}
															else
																{	/* Unsafe/bignumber.scm 974 */
																	BgL_test2816z00_7758 = ((bool_t) 0);
																}
															if (BgL_test2816z00_7758)
																{	/* Unsafe/bignumber.scm 975 */
																	long BgL_arg1831z00_2101;

																	BgL_arg1831z00_2101 = ((BgL_cz00_2095) - 48L);
																	if (
																		(BgL_arg1831z00_2101 <
																			(long) CINT(BgL_radz00_2096)))
																		{	/* Unsafe/bignumber.scm 970 */
																			BgL_dz00_2134 = BINT(BgL_arg1831z00_2101);
																		}
																	else
																		{	/* Unsafe/bignumber.scm 970 */
																			BgL_dz00_2134 = BFALSE;
																		}
																}
															else
																{	/* Unsafe/bignumber.scm 976 */
																	bool_t BgL_test2819z00_7768;

																	if ((BgL_cz00_2095 >= ((unsigned char) 'a')))
																		{	/* Unsafe/bignumber.scm 976 */
																			BgL_test2819z00_7768 =
																				(BgL_cz00_2095 <=
																				((unsigned char) 'z'));
																		}
																	else
																		{	/* Unsafe/bignumber.scm 976 */
																			BgL_test2819z00_7768 = ((bool_t) 0);
																		}
																	if (BgL_test2819z00_7768)
																		{	/* Unsafe/bignumber.scm 977 */
																			long BgL_arg1835z00_2105;

																			BgL_arg1835z00_2105 =
																				(10L + ((BgL_cz00_2095) - 97L));
																			if (
																				(BgL_arg1835z00_2105 <
																					(long) CINT(BgL_radz00_2096)))
																				{	/* Unsafe/bignumber.scm 970 */
																					BgL_dz00_2134 =
																						BINT(BgL_arg1835z00_2105);
																				}
																			else
																				{	/* Unsafe/bignumber.scm 970 */
																					BgL_dz00_2134 = BFALSE;
																				}
																		}
																	else
																		{	/* Unsafe/bignumber.scm 978 */
																			bool_t BgL_test2822z00_7779;

																			if (
																				(BgL_cz00_2095 >=
																					((unsigned char) 'A')))
																				{	/* Unsafe/bignumber.scm 978 */
																					BgL_test2822z00_7779 =
																						(BgL_cz00_2095 <=
																						((unsigned char) 'Z'));
																				}
																			else
																				{	/* Unsafe/bignumber.scm 978 */
																					BgL_test2822z00_7779 = ((bool_t) 0);
																				}
																			if (BgL_test2822z00_7779)
																				{	/* Unsafe/bignumber.scm 979 */
																					long BgL_arg1840z00_2110;

																					BgL_arg1840z00_2110 =
																						(10L + ((BgL_cz00_2095) - 65L));
																					if (
																						(BgL_arg1840z00_2110 <
																							(long) CINT(BgL_radz00_2096)))
																						{	/* Unsafe/bignumber.scm 970 */
																							BgL_dz00_2134 =
																								BINT(BgL_arg1840z00_2110);
																						}
																					else
																						{	/* Unsafe/bignumber.scm 970 */
																							BgL_dz00_2134 = BFALSE;
																						}
																				}
																			else
																				{	/* Unsafe/bignumber.scm 978 */
																					BgL_dz00_2134 = BFALSE;
																				}
																		}
																}
														}
														if (CBOOL(BgL_dz00_2134))
															{	/* Unsafe/bignumber.scm 989 */
																long BgL_arg1853z00_2135;
																obj_t BgL_arg1854z00_2136;

																BgL_arg1853z00_2135 = (BgL_iz00_2129 + 1L);
																BgL_arg1854z00_2136 =
																	MAKE_YOUNG_PAIR(BgL_dz00_2134,
																	BgL_digitsz00_2130);
																{
																	obj_t BgL_digitsz00_7797;
																	long BgL_iz00_7796;

																	BgL_iz00_7796 = BgL_arg1853z00_2135;
																	BgL_digitsz00_7797 = BgL_arg1854z00_2136;
																	BgL_digitsz00_2130 = BgL_digitsz00_7797;
																	BgL_iz00_2129 = BgL_iz00_7796;
																	goto BgL_zc3z04anonymousza31850ze3z87_2131;
																}
															}
														else
															{	/* Unsafe/bignumber.scm 988 */
																return
																	BGl_z42z42fixnumzd2ze3bignumzd2freshze3zz__bignumz00
																	(0L);
															}
													}
												else
													{	/* Unsafe/bignumber.scm 991 */
														obj_t BgL_nz00_2138;

														BgL_nz00_2138 =
															BGl_fixnumzd2listzd2ze3bignumze3zz__bignumz00
															(BgL_digitsz00_2130,
															((long) CINT(BgL_radz00_2120) - 1L));
														{	/* Unsafe/bignumber.scm 992 */
															bool_t BgL_test2826z00_7802;

															if (CBOOL(BgL_signz00_2121))
																{	/* Unsafe/bignumber.scm 992 */
																	BgL_test2826z00_7802 =
																		(CCHAR(BgL_signz00_2121) ==
																		((unsigned char) '-'));
																}
															else
																{	/* Unsafe/bignumber.scm 992 */
																	BgL_test2826z00_7802 = ((bool_t) 0);
																}
															if (BgL_test2826z00_7802)
																{	/* Unsafe/bignumber.scm 992 */
																	return
																		bgl_bignum_sub
																		(BGl_bignumzd2za7eroz75zz__bignumz00,
																		BgL_nz00_2138);
																}
															else
																{	/* Unsafe/bignumber.scm 992 */
																	return BgL_nz00_2138;
																}
														}
													}
											}
										else
											{	/* Unsafe/bignumber.scm 984 */
												return
													BGl_z42z42fixnumzd2ze3bignumzd2freshze3zz__bignumz00
													(0L);
											}
									}
								else
									{
										long BgL_iz00_7812;
										obj_t BgL_signz00_7811;
										obj_t BgL_radz00_7810;

										BgL_radz00_7810 = BgL_radz00_2145;
										BgL_signz00_7811 = BFALSE;
										BgL_iz00_7812 = BgL_iz00_2146;
										BgL_iz00_2122 = BgL_iz00_7812;
										BgL_signz00_2121 = BgL_signz00_7811;
										BgL_radz00_2120 = BgL_radz00_7810;
										goto BgL_zc3z04anonymousza31846ze3z87_2123;
									}
							}
						}
					else
						{
							long BgL_iz00_7815;
							obj_t BgL_radz00_7813;

							BgL_radz00_7813 = BINT(BgL_radixz00_95);
							BgL_iz00_7815 = BgL_iz00_2161;
							BgL_iz00_2146 = BgL_iz00_7815;
							BgL_radz00_2145 = BgL_radz00_7813;
							goto BgL_zc3z04anonymousza31863ze3z87_2147;
						}
				}
			}
		}

	}



/* &$$string->bignum */
	obj_t BGl_z62z42z42stringzd2ze3bignumz53zz__bignumz00(obj_t BgL_envz00_5563,
		obj_t BgL_strz00_5564, obj_t BgL_radixz00_5565)
	{
		{	/* Unsafe/bignumber.scm 965 */
			{	/* Unsafe/bignumber.scm 970 */
				int BgL_auxz00_7825;
				char *BgL_auxz00_7816;

				{	/* Unsafe/bignumber.scm 970 */
					obj_t BgL_tmpz00_7826;

					if (INTEGERP(BgL_radixz00_5565))
						{	/* Unsafe/bignumber.scm 970 */
							BgL_tmpz00_7826 = BgL_radixz00_5565;
						}
					else
						{
							obj_t BgL_auxz00_7829;

							BgL_auxz00_7829 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
								BINT(31576L), BGl_string2519z00zz__bignumz00,
								BGl_string2480z00zz__bignumz00, BgL_radixz00_5565);
							FAILURE(BgL_auxz00_7829, BFALSE, BFALSE);
						}
					BgL_auxz00_7825 = CINT(BgL_tmpz00_7826);
				}
				{	/* Unsafe/bignumber.scm 970 */
					obj_t BgL_tmpz00_7817;

					if (STRINGP(BgL_strz00_5564))
						{	/* Unsafe/bignumber.scm 970 */
							BgL_tmpz00_7817 = BgL_strz00_5564;
						}
					else
						{
							obj_t BgL_auxz00_7820;

							BgL_auxz00_7820 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
								BINT(31576L), BGl_string2519z00zz__bignumz00,
								BGl_string2479z00zz__bignumz00, BgL_strz00_5564);
							FAILURE(BgL_auxz00_7820, BFALSE, BFALSE);
						}
					BgL_auxz00_7816 = BSTRING_TO_STRING(BgL_tmpz00_7817);
				}
				return bgl_string_to_bignum(BgL_auxz00_7816, BgL_auxz00_7825);
			}
		}

	}



/* bignum->fixnum-list */
	obj_t BGl_bignumzd2ze3fixnumzd2listze3zz__bignumz00(obj_t BgL_xz00_96,
		long BgL_radixzd2minuszd21z00_97)
	{
		{	/* Unsafe/bignumber.scm 1023 */
			{	/* Unsafe/bignumber.scm 1024 */
				obj_t BgL_bigzd2radixzd2_2180;

				{	/* Unsafe/bignumber.scm 1026 */
					obj_t BgL_arg1898z00_2209;
					obj_t BgL_arg1899z00_2210;

					BgL_arg1898z00_2209 = bgl_long_to_bignum(BgL_radixzd2minuszd21z00_97);
					{	/* Unsafe/bignumber.scm 1027 */
						obj_t BgL_res2351z00_4942;

						{	/* Unsafe/bignumber.scm 415 */
							bool_t BgL_test2830z00_7836;

							if ((1L < -16L))
								{	/* Unsafe/bignumber.scm 415 */
									BgL_test2830z00_7836 = ((bool_t) 1);
								}
							else
								{	/* Unsafe/bignumber.scm 415 */
									BgL_test2830z00_7836 = (16L < 1L);
								}
							if (BgL_test2830z00_7836)
								{	/* Unsafe/bignumber.scm 415 */
									BgL_res2351z00_4942 =
										BGl_z42z42fixnumzd2ze3bignumzd2freshze3zz__bignumz00(1L);
								}
							else
								{	/* Unsafe/bignumber.scm 415 */
									BgL_res2351z00_4942 =
										VECTOR_REF(BGl_preallocatedzd2bignumszd2zz__bignumz00,
										(1L + 16L));
								}
						}
						BgL_arg1899z00_2210 = BgL_res2351z00_4942;
					}
					{	/* Unsafe/bignumber.scm 1025 */
						obj_t BgL_res2352z00_4959;

						{	/* Unsafe/bignumber.scm 607 */
							long BgL_arg1510z00_4945;
							long BgL_arg1511z00_4946;

							{	/* Unsafe/bignumber.scm 263 */
								uint16_t BgL_arg1215z00_4948;

								{	/* Unsafe/bignumber.scm 263 */
									obj_t BgL_arg1216z00_4949;

									BgL_arg1216z00_4949 = BGL_BIGNUM_U16VECT(BgL_arg1898z00_2209);
									BgL_arg1215z00_4948 = BGL_U16VREF(BgL_arg1216z00_4949, 0L);
								}
								{	/* Unsafe/bignumber.scm 263 */
									long BgL_arg2271z00_4951;

									BgL_arg2271z00_4951 = (long) (BgL_arg1215z00_4948);
									BgL_arg1510z00_4945 = (long) (BgL_arg2271z00_4951);
							}}
							{	/* Unsafe/bignumber.scm 263 */
								uint16_t BgL_arg1215z00_4954;

								{	/* Unsafe/bignumber.scm 263 */
									obj_t BgL_arg1216z00_4955;

									BgL_arg1216z00_4955 = BGL_BIGNUM_U16VECT(BgL_arg1899z00_2210);
									BgL_arg1215z00_4954 = BGL_U16VREF(BgL_arg1216z00_4955, 0L);
								}
								{	/* Unsafe/bignumber.scm 263 */
									long BgL_arg2271z00_4957;

									BgL_arg2271z00_4957 = (long) (BgL_arg1215z00_4954);
									BgL_arg1511z00_4946 = (long) (BgL_arg2271z00_4957);
							}}
							BgL_res2352z00_4959 =
								BGl_bignumzd2sum2zd2zz__bignumz00(BgL_arg1898z00_2209,
								BgL_arg1899z00_2210, BgL_arg1510z00_4945, BgL_arg1511z00_4946);
						}
						BgL_bigzd2radixzd2_2180 = BgL_res2352z00_4959;
				}}
				{	/* Unsafe/bignumber.scm 1024 */
					obj_t BgL_squarezd2serieszd2_2181;

					{	/* Unsafe/bignumber.scm 1029 */
						obj_t BgL_g1066z00_2199;

						{	/* Unsafe/bignumber.scm 1030 */
							obj_t BgL_list1897z00_2208;

							BgL_list1897z00_2208 =
								MAKE_YOUNG_PAIR(BgL_bigzd2radixzd2_2180, BNIL);
							BgL_g1066z00_2199 = BgL_list1897z00_2208;
						}
						{
							obj_t BgL_squarez00_4968;
							obj_t BgL_squarezd2listzd2_4969;

							BgL_squarez00_4968 = BgL_bigzd2radixzd2_2180;
							BgL_squarezd2listzd2_4969 = BgL_g1066z00_2199;
						BgL_loopz00_4967:
							{	/* Unsafe/bignumber.scm 1031 */
								obj_t BgL_newzd2squarezd2_4973;

								BgL_newzd2squarezd2_4973 =
									bgl_bignum_mul(BgL_squarez00_4968, BgL_squarez00_4968);
								if (BGl_z42zc3bxz81zz__bignumz00(BgL_xz00_96,
										BgL_newzd2squarezd2_4973))
									{	/* Unsafe/bignumber.scm 1033 */
										BgL_squarezd2serieszd2_2181 = BgL_squarezd2listzd2_4969;
									}
								else
									{	/* Unsafe/bignumber.scm 1036 */
										obj_t BgL_arg1896z00_4975;

										BgL_arg1896z00_4975 =
											MAKE_YOUNG_PAIR(BgL_newzd2squarezd2_4973,
											BgL_squarezd2listzd2_4969);
										{
											obj_t BgL_squarezd2listzd2_7858;
											obj_t BgL_squarez00_7857;

											BgL_squarez00_7857 = BgL_newzd2squarezd2_4973;
											BgL_squarezd2listzd2_7858 = BgL_arg1896z00_4975;
											BgL_squarezd2listzd2_4969 = BgL_squarezd2listzd2_7858;
											BgL_squarez00_4968 = BgL_squarez00_7857;
											goto BgL_loopz00_4967;
										}
									}
							}
						}
					}
					{	/* Unsafe/bignumber.scm 1028 */

						return
							BGl_convertze70ze7zz__bignumz00(BgL_xz00_96,
							BgL_squarezd2serieszd2_2181, BNIL);
					}
				}
			}
		}

	}



/* convert~0 */
	obj_t BGl_convertze70ze7zz__bignumz00(obj_t BgL_nz00_2183,
		obj_t BgL_squarezd2serieszd2_2184, obj_t BgL_tailz00_2185)
	{
		{	/* Unsafe/bignumber.scm 1053 */
		BGl_convertze70ze7zz__bignumz00:
			if (PAIRP(BgL_squarezd2serieszd2_2184))
				{	/* Unsafe/bignumber.scm 1040 */
					obj_t BgL_qrz00_2188;

					BgL_qrz00_2188 =
						BGl_bignumzd2divzd2zz__bignumz00(BgL_nz00_2183,
						CAR(BgL_squarezd2serieszd2_2184));
					{	/* Unsafe/bignumber.scm 1040 */
						obj_t BgL_qz00_2189;

						BgL_qz00_2189 = CAR(((obj_t) BgL_qrz00_2188));
						{	/* Unsafe/bignumber.scm 1041 */
							obj_t BgL_rz00_2190;

							BgL_rz00_2190 = CDR(((obj_t) BgL_qrz00_2188));
							{	/* Unsafe/bignumber.scm 1042 */
								obj_t BgL_newzd2squarezd2seriesz00_2191;

								BgL_newzd2squarezd2seriesz00_2191 =
									CDR(BgL_squarezd2serieszd2_2184);
								{	/* Unsafe/bignumber.scm 1043 */

									{
										obj_t BgL_tailz00_7871;
										obj_t BgL_squarezd2serieszd2_7870;
										obj_t BgL_nz00_7869;

										BgL_nz00_7869 = BgL_rz00_2190;
										BgL_squarezd2serieszd2_7870 =
											BgL_newzd2squarezd2seriesz00_2191;
										BgL_tailz00_7871 =
											BGl_convertze70ze7zz__bignumz00(BgL_qz00_2189,
											BgL_newzd2squarezd2seriesz00_2191, BgL_tailz00_2185);
										BgL_tailz00_2185 = BgL_tailz00_7871;
										BgL_squarezd2serieszd2_2184 = BgL_squarezd2serieszd2_7870;
										BgL_nz00_2183 = BgL_nz00_7869;
										goto BGl_convertze70ze7zz__bignumz00;
									}
								}
							}
						}
					}
				}
			else
				{	/* Unsafe/bignumber.scm 1049 */
					long BgL_dz00_2194;

					BgL_dz00_2194 = bgl_bignum_to_long(((obj_t) BgL_nz00_2183));
					{	/* Unsafe/bignumber.scm 1050 */
						bool_t BgL_test2834z00_7875;

						if (NULLP(BgL_tailz00_2185))
							{	/* Unsafe/bignumber.scm 1050 */
								BgL_test2834z00_7875 = (BgL_dz00_2194 == 0L);
							}
						else
							{	/* Unsafe/bignumber.scm 1050 */
								BgL_test2834z00_7875 = ((bool_t) 0);
							}
						if (BgL_test2834z00_7875)
							{	/* Unsafe/bignumber.scm 1050 */
								return BgL_tailz00_2185;
							}
						else
							{	/* Unsafe/bignumber.scm 1050 */
								return MAKE_YOUNG_PAIR(BINT(BgL_dz00_2194), BgL_tailz00_2185);
							}
					}
				}
		}

	}



/* fixnum-list->bignum */
	obj_t BGl_fixnumzd2listzd2ze3bignumze3zz__bignumz00(obj_t
		BgL_digitzd2listzd2_98, long BgL_radixzd2minuszd21z00_99)
	{
		{	/* Unsafe/bignumber.scm 1057 */
			{	/* Unsafe/bignumber.scm 1061 */
				obj_t BgL_bigzd2radixzd2_2211;

				{	/* Unsafe/bignumber.scm 1063 */
					obj_t BgL_arg1911z00_2224;
					obj_t BgL_arg1912z00_2225;

					BgL_arg1911z00_2224 = bgl_long_to_bignum(BgL_radixzd2minuszd21z00_99);
					{	/* Unsafe/bignumber.scm 1064 */
						obj_t BgL_res2354z00_4990;

						{	/* Unsafe/bignumber.scm 415 */
							bool_t BgL_test2836z00_7882;

							if ((1L < -16L))
								{	/* Unsafe/bignumber.scm 415 */
									BgL_test2836z00_7882 = ((bool_t) 1);
								}
							else
								{	/* Unsafe/bignumber.scm 415 */
									BgL_test2836z00_7882 = (16L < 1L);
								}
							if (BgL_test2836z00_7882)
								{	/* Unsafe/bignumber.scm 415 */
									BgL_res2354z00_4990 =
										BGl_z42z42fixnumzd2ze3bignumzd2freshze3zz__bignumz00(1L);
								}
							else
								{	/* Unsafe/bignumber.scm 415 */
									BgL_res2354z00_4990 =
										VECTOR_REF(BGl_preallocatedzd2bignumszd2zz__bignumz00,
										(1L + 16L));
								}
						}
						BgL_arg1912z00_2225 = BgL_res2354z00_4990;
					}
					{	/* Unsafe/bignumber.scm 1062 */
						obj_t BgL_res2355z00_5007;

						{	/* Unsafe/bignumber.scm 607 */
							long BgL_arg1510z00_4993;
							long BgL_arg1511z00_4994;

							{	/* Unsafe/bignumber.scm 263 */
								uint16_t BgL_arg1215z00_4996;

								{	/* Unsafe/bignumber.scm 263 */
									obj_t BgL_arg1216z00_4997;

									BgL_arg1216z00_4997 = BGL_BIGNUM_U16VECT(BgL_arg1911z00_2224);
									BgL_arg1215z00_4996 = BGL_U16VREF(BgL_arg1216z00_4997, 0L);
								}
								{	/* Unsafe/bignumber.scm 263 */
									long BgL_arg2271z00_4999;

									BgL_arg2271z00_4999 = (long) (BgL_arg1215z00_4996);
									BgL_arg1510z00_4993 = (long) (BgL_arg2271z00_4999);
							}}
							{	/* Unsafe/bignumber.scm 263 */
								uint16_t BgL_arg1215z00_5002;

								{	/* Unsafe/bignumber.scm 263 */
									obj_t BgL_arg1216z00_5003;

									BgL_arg1216z00_5003 = BGL_BIGNUM_U16VECT(BgL_arg1912z00_2225);
									BgL_arg1215z00_5002 = BGL_U16VREF(BgL_arg1216z00_5003, 0L);
								}
								{	/* Unsafe/bignumber.scm 263 */
									long BgL_arg2271z00_5005;

									BgL_arg2271z00_5005 = (long) (BgL_arg1215z00_5002);
									BgL_arg1511z00_4994 = (long) (BgL_arg2271z00_5005);
							}}
							BgL_res2355z00_5007 =
								BGl_bignumzd2sum2zd2zz__bignumz00(BgL_arg1911z00_2224,
								BgL_arg1912z00_2225, BgL_arg1510z00_4993, BgL_arg1511z00_4994);
						}
						BgL_bigzd2radixzd2_2211 = BgL_res2355z00_5007;
				}}
				{	/* Unsafe/bignumber.scm 1065 */
					obj_t BgL_g1067z00_2212;

					BgL_g1067z00_2212 = bgl_reverse(BgL_digitzd2listzd2_98);
					{
						obj_t BgL_nz00_5047;
						obj_t BgL_lstz00_5048;

						BgL_nz00_5047 = BGl_bignumzd2za7eroz75zz__bignumz00;
						BgL_lstz00_5048 = BgL_g1067z00_2212;
					BgL_loopz00_5046:
						if (PAIRP(BgL_lstz00_5048))
							{	/* Unsafe/bignumber.scm 1067 */
								obj_t BgL_arg1902z00_5056;
								obj_t BgL_arg1903z00_5057;

								{	/* Unsafe/bignumber.scm 1067 */
									obj_t BgL_arg1904z00_5058;
									obj_t BgL_arg1906z00_5059;

									BgL_arg1904z00_5058 =
										bgl_bignum_mul(BgL_nz00_5047, BgL_bigzd2radixzd2_2211);
									{	/* Unsafe/bignumber.scm 1068 */
										obj_t BgL_arg1910z00_5060;

										BgL_arg1910z00_5060 = CAR(BgL_lstz00_5048);
										BgL_arg1906z00_5059 =
											bgl_long_to_bignum((long) CINT(BgL_arg1910z00_5060));
									}
									BgL_arg1902z00_5056 =
										BGl_bignumzd2sum2zd2zz__bignumz00(BgL_arg1904z00_5058,
										BgL_arg1906z00_5059,
										BGl_bignumzd2signzd2zz__bignumz00(BgL_arg1904z00_5058),
										BGl_bignumzd2signzd2zz__bignumz00(BgL_arg1906z00_5059));
								}
								BgL_arg1903z00_5057 = CDR(BgL_lstz00_5048);
								{
									obj_t BgL_lstz00_7910;
									obj_t BgL_nz00_7909;

									BgL_nz00_7909 = BgL_arg1902z00_5056;
									BgL_lstz00_7910 = BgL_arg1903z00_5057;
									BgL_lstz00_5048 = BgL_lstz00_7910;
									BgL_nz00_5047 = BgL_nz00_7909;
									goto BgL_loopz00_5046;
								}
							}
						else
							{	/* Unsafe/bignumber.scm 1066 */
								return BgL_nz00_5047;
							}
					}
				}
			}
		}

	}



/* $$bignum->fixnum */
	BGL_EXPORTED_DEF long bgl_bignum_to_long(obj_t BgL_xz00_100)
	{
		{	/* Unsafe/bignumber.scm 1072 */
			{	/* Unsafe/bignumber.scm 1073 */
				long BgL_lenxzd2minuszd21z00_2226;

				{	/* Unsafe/bignumber.scm 1073 */
					long BgL_arg1933z00_2250;

					{	/* Unsafe/bignumber.scm 261 */
						long BgL_arg1210z00_5069;

						{	/* Unsafe/bignumber.scm 261 */
							obj_t BgL_arg1212z00_5070;

							BgL_arg1212z00_5070 = BGL_BIGNUM_U16VECT(BgL_xz00_100);
							BgL_arg1210z00_5069 = BGL_HVECTOR_LENGTH(BgL_arg1212z00_5070);
						}
						{	/* Unsafe/bignumber.scm 261 */
							uint16_t BgL_xz00_5072;

							BgL_xz00_5072 = (uint16_t) (BgL_arg1210z00_5069);
							{	/* Unsafe/bignumber.scm 261 */
								long BgL_arg2271z00_5073;

								BgL_arg2271z00_5073 = (long) (BgL_xz00_5072);
								BgL_arg1933z00_2250 = (long) (BgL_arg2271z00_5073);
					}}}
					BgL_lenxzd2minuszd21z00_2226 = (BgL_arg1933z00_2250 - 1L);
				}
				{
					long BgL_nz00_2228;
					long BgL_iz00_2229;

					{	/* Unsafe/bignumber.scm 1074 */
						obj_t BgL_tmpz00_7917;

						BgL_nz00_2228 = 0L;
						BgL_iz00_2229 = BgL_lenxzd2minuszd21z00_2226;
					BgL_zc3z04anonymousza31913ze3z87_2230:
						if ((0L < BgL_iz00_2229))
							{	/* Unsafe/bignumber.scm 1076 */
								bool_t BgL_test2840z00_7920;

								{	/* Unsafe/bignumber.scm 1076 */
									long BgL_arg1928z00_2244;

									BgL_arg1928z00_2244 = (-536870912L / 16384L);
									BgL_test2840z00_7920 = (BgL_nz00_2228 < BgL_arg1928z00_2244);
								}
								if (BgL_test2840z00_7920)
									{	/* Unsafe/bignumber.scm 1076 */
										BgL_tmpz00_7917 = BFALSE;
									}
								else
									{	/* Unsafe/bignumber.scm 1078 */
										long BgL_mz00_2234;
										long BgL_dz00_2235;

										BgL_mz00_2234 = (BgL_nz00_2228 * 16384L);
										{	/* Unsafe/bignumber.scm 1079 */
											int BgL_iz00_5082;

											BgL_iz00_5082 = (int) (BgL_iz00_2229);
											{	/* Unsafe/bignumber.scm 267 */
												uint16_t BgL_arg1220z00_5083;

												{	/* Unsafe/bignumber.scm 267 */
													obj_t BgL_arg1221z00_5084;

													BgL_arg1221z00_5084 =
														BGL_BIGNUM_U16VECT(BgL_xz00_100);
													{	/* Unsafe/bignumber.scm 267 */
														long BgL_tmpz00_7926;

														BgL_tmpz00_7926 = (long) (BgL_iz00_5082);
														BgL_arg1220z00_5083 =
															BGL_U16VREF(BgL_arg1221z00_5084, BgL_tmpz00_7926);
												}}
												{	/* Unsafe/bignumber.scm 267 */
													long BgL_arg2271z00_5086;

													BgL_arg2271z00_5086 = (long) (BgL_arg1220z00_5083);
													BgL_dz00_2235 = (long) (BgL_arg2271z00_5086);
										}}}
										if ((BgL_mz00_2234 < (-536870912L + BgL_dz00_2235)))
											{	/* Unsafe/bignumber.scm 1080 */
												BgL_tmpz00_7917 = BFALSE;
											}
										else
											{
												long BgL_iz00_7936;
												long BgL_nz00_7934;

												BgL_nz00_7934 = (BgL_mz00_2234 - BgL_dz00_2235);
												BgL_iz00_7936 = (BgL_iz00_2229 - 1L);
												BgL_iz00_2229 = BgL_iz00_7936;
												BgL_nz00_2228 = BgL_nz00_7934;
												goto BgL_zc3z04anonymousza31913ze3z87_2230;
											}
									}
							}
						else
							{	/* Unsafe/bignumber.scm 1084 */
								bool_t BgL_test2842z00_7938;

								{	/* Unsafe/bignumber.scm 471 */
									long BgL_arg1428z00_5096;

									{	/* Unsafe/bignumber.scm 263 */
										uint16_t BgL_arg1215z00_5098;

										{	/* Unsafe/bignumber.scm 263 */
											obj_t BgL_arg1216z00_5099;

											BgL_arg1216z00_5099 = BGL_BIGNUM_U16VECT(BgL_xz00_100);
											BgL_arg1215z00_5098 =
												BGL_U16VREF(BgL_arg1216z00_5099, 0L);
										}
										{	/* Unsafe/bignumber.scm 263 */
											long BgL_arg2271z00_5101;

											BgL_arg2271z00_5101 = (long) (BgL_arg1215z00_5098);
											BgL_arg1428z00_5096 = (long) (BgL_arg2271z00_5101);
									}}
									BgL_test2842z00_7938 = (BgL_arg1428z00_5096 == 0L);
								}
								if (BgL_test2842z00_7938)
									{	/* Unsafe/bignumber.scm 1084 */
										BgL_tmpz00_7917 = BINT(BgL_nz00_2228);
									}
								else
									{	/* Unsafe/bignumber.scm 1084 */
										if ((BgL_nz00_2228 == -536870912L))
											{	/* Unsafe/bignumber.scm 1086 */
												BgL_tmpz00_7917 = BFALSE;
											}
										else
											{	/* Unsafe/bignumber.scm 1086 */
												BgL_tmpz00_7917 = BINT((0L - BgL_nz00_2228));
											}
									}
							}
						return (long) CINT(BgL_tmpz00_7917);
		}}}}

	}



/* &$$bignum->fixnum */
	obj_t BGl_z62z42z42bignumzd2ze3fixnumz53zz__bignumz00(obj_t BgL_envz00_5566,
		obj_t BgL_xz00_5567)
	{
		{	/* Unsafe/bignumber.scm 1072 */
			{	/* Unsafe/bignumber.scm 1073 */
				long BgL_tmpz00_7950;

				{	/* Unsafe/bignumber.scm 1073 */
					obj_t BgL_auxz00_7951;

					if (BIGNUMP(BgL_xz00_5567))
						{	/* Unsafe/bignumber.scm 1073 */
							BgL_auxz00_7951 = BgL_xz00_5567;
						}
					else
						{
							obj_t BgL_auxz00_7954;

							BgL_auxz00_7954 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
								BINT(34701L), BGl_string2520z00zz__bignumz00,
								BGl_string2491z00zz__bignumz00, BgL_xz00_5567);
							FAILURE(BgL_auxz00_7954, BFALSE, BFALSE);
						}
					BgL_tmpz00_7950 = bgl_bignum_to_long(BgL_auxz00_7951);
				}
				return BINT(BgL_tmpz00_7950);
			}
		}

	}



/* $$bignum->elong */
	BGL_EXPORTED_DEF long bgl_bignum_to_elong(obj_t BgL_xz00_101)
	{
		{	/* Unsafe/bignumber.scm 1091 */
			{	/* Unsafe/bignumber.scm 1092 */
				long BgL_lenxzd2minuszd21z00_2251;

				{	/* Unsafe/bignumber.scm 1092 */
					long BgL_arg1952z00_2276;

					{	/* Unsafe/bignumber.scm 261 */
						long BgL_arg1210z00_5108;

						{	/* Unsafe/bignumber.scm 261 */
							obj_t BgL_arg1212z00_5109;

							BgL_arg1212z00_5109 = BGL_BIGNUM_U16VECT(BgL_xz00_101);
							BgL_arg1210z00_5108 = BGL_HVECTOR_LENGTH(BgL_arg1212z00_5109);
						}
						{	/* Unsafe/bignumber.scm 261 */
							uint16_t BgL_xz00_5111;

							BgL_xz00_5111 = (uint16_t) (BgL_arg1210z00_5108);
							{	/* Unsafe/bignumber.scm 261 */
								long BgL_arg2271z00_5112;

								BgL_arg2271z00_5112 = (long) (BgL_xz00_5111);
								BgL_arg1952z00_2276 = (long) (BgL_arg2271z00_5112);
					}}}
					BgL_lenxzd2minuszd21z00_2251 = (BgL_arg1952z00_2276 - 1L);
				}
				{
					long BgL_nz00_2253;
					long BgL_iz00_2254;

					{	/* Unsafe/bignumber.scm 1093 */
						obj_t BgL_tmpz00_7966;

						BgL_nz00_2253 = ((long) 0);
						BgL_iz00_2254 = BgL_lenxzd2minuszd21z00_2251;
					BgL_zc3z04anonymousza31934ze3z87_2255:
						if ((0L < BgL_iz00_2254))
							{	/* Unsafe/bignumber.scm 1094 */
								if (
									(BgL_nz00_2253 <
										BGl_bignumzd2minzd2elongzd2divzd2radixz00zz__bignumz00()))
									{	/* Unsafe/bignumber.scm 1095 */
										BgL_tmpz00_7966 = BFALSE;
									}
								else
									{	/* Unsafe/bignumber.scm 1097 */
										long BgL_mz00_2259;
										long BgL_dz00_2260;

										BgL_mz00_2259 =
											(BgL_nz00_2253 * ((long) (1L) << (int) (15L)));
										{	/* Unsafe/bignumber.scm 1098 */
											long BgL_arg1946z00_2269;

											{	/* Unsafe/bignumber.scm 1098 */
												int BgL_iz00_5122;

												BgL_iz00_5122 = (int) (BgL_iz00_2254);
												{	/* Unsafe/bignumber.scm 267 */
													uint16_t BgL_arg1220z00_5123;

													{	/* Unsafe/bignumber.scm 267 */
														obj_t BgL_arg1221z00_5124;

														BgL_arg1221z00_5124 =
															BGL_BIGNUM_U16VECT(BgL_xz00_101);
														{	/* Unsafe/bignumber.scm 267 */
															long BgL_tmpz00_7978;

															BgL_tmpz00_7978 = (long) (BgL_iz00_5122);
															BgL_arg1220z00_5123 =
																BGL_U16VREF(BgL_arg1221z00_5124,
																BgL_tmpz00_7978);
													}}
													{	/* Unsafe/bignumber.scm 267 */
														long BgL_arg2271z00_5126;

														BgL_arg2271z00_5126 = (long) (BgL_arg1220z00_5123);
														BgL_arg1946z00_2269 = (long) (BgL_arg2271z00_5126);
											}}}
											BgL_dz00_2260 = (long) (BgL_arg1946z00_2269);
										}
										if (
											(BgL_mz00_2259 <
												((((long) -2) *
														((long) (1L) << (int) (30L))) + BgL_dz00_2260)))
											{	/* Unsafe/bignumber.scm 1099 */
												BgL_tmpz00_7966 = BFALSE;
											}
										else
											{	/* Unsafe/bignumber.scm 1101 */
												long BgL_arg1941z00_2264;
												long BgL_arg1942z00_2265;

												{	/* Unsafe/bignumber.scm 1101 */
													long BgL_res2359z00_5138;

													{	/* Unsafe/bignumber.scm 1101 */
														long BgL_tmpz00_7991;

														BgL_tmpz00_7991 = (BgL_mz00_2259 - BgL_dz00_2260);
														BgL_res2359z00_5138 = (long) (BgL_tmpz00_7991);
													}
													BgL_arg1941z00_2264 = BgL_res2359z00_5138;
												}
												BgL_arg1942z00_2265 = (BgL_iz00_2254 - 1L);
												{
													long BgL_iz00_7996;
													long BgL_nz00_7995;

													BgL_nz00_7995 = BgL_arg1941z00_2264;
													BgL_iz00_7996 = BgL_arg1942z00_2265;
													BgL_iz00_2254 = BgL_iz00_7996;
													BgL_nz00_2253 = BgL_nz00_7995;
													goto BgL_zc3z04anonymousza31934ze3z87_2255;
												}
											}
									}
							}
						else
							{	/* Unsafe/bignumber.scm 1103 */
								bool_t BgL_test2848z00_7997;

								{	/* Unsafe/bignumber.scm 471 */
									long BgL_arg1428z00_5141;

									{	/* Unsafe/bignumber.scm 263 */
										uint16_t BgL_arg1215z00_5143;

										{	/* Unsafe/bignumber.scm 263 */
											obj_t BgL_arg1216z00_5144;

											BgL_arg1216z00_5144 = BGL_BIGNUM_U16VECT(BgL_xz00_101);
											BgL_arg1215z00_5143 =
												BGL_U16VREF(BgL_arg1216z00_5144, 0L);
										}
										{	/* Unsafe/bignumber.scm 263 */
											long BgL_arg2271z00_5146;

											BgL_arg2271z00_5146 = (long) (BgL_arg1215z00_5143);
											BgL_arg1428z00_5141 = (long) (BgL_arg2271z00_5146);
									}}
									BgL_test2848z00_7997 = (BgL_arg1428z00_5141 == 0L);
								}
								if (BgL_test2848z00_7997)
									{	/* Unsafe/bignumber.scm 1103 */
										BgL_tmpz00_7966 = make_belong(BgL_nz00_2253);
									}
								else
									{	/* Unsafe/bignumber.scm 1103 */
										if (
											(BgL_nz00_2253 ==
												(((long) -2) * ((long) (1L) << (int) (30L)))))
											{	/* Unsafe/bignumber.scm 1105 */
												BgL_tmpz00_7966 = BFALSE;
											}
										else
											{	/* Unsafe/bignumber.scm 1106 */
												long BgL_res2360z00_5157;

												{	/* Unsafe/bignumber.scm 1106 */
													long BgL_tmpz00_8010;

													BgL_tmpz00_8010 = (((long) 0) - BgL_nz00_2253);
													BgL_res2360z00_5157 = (long) (BgL_tmpz00_8010);
												}
												BgL_tmpz00_7966 = make_belong(BgL_res2360z00_5157);
							}}}
						return BELONG_TO_LONG(BgL_tmpz00_7966);
					}
				}
			}
		}

	}



/* &$$bignum->elong */
	obj_t BGl_z62z42z42bignumzd2ze3elongz53zz__bignumz00(obj_t BgL_envz00_5568,
		obj_t BgL_xz00_5569)
	{
		{	/* Unsafe/bignumber.scm 1091 */
			{	/* Unsafe/bignumber.scm 1092 */
				long BgL_tmpz00_8015;

				{	/* Unsafe/bignumber.scm 1092 */
					obj_t BgL_auxz00_8016;

					if (BIGNUMP(BgL_xz00_5569))
						{	/* Unsafe/bignumber.scm 1092 */
							BgL_auxz00_8016 = BgL_xz00_5569;
						}
					else
						{
							obj_t BgL_auxz00_8019;

							BgL_auxz00_8019 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
								BINT(35211L), BGl_string2521z00zz__bignumz00,
								BGl_string2491z00zz__bignumz00, BgL_xz00_5569);
							FAILURE(BgL_auxz00_8019, BFALSE, BFALSE);
						}
					BgL_tmpz00_8015 = bgl_bignum_to_elong(BgL_auxz00_8016);
				}
				return make_belong(BgL_tmpz00_8015);
			}
		}

	}



/* $$bignum->llong */
	BGL_EXPORTED_DEF BGL_LONGLONG_T bgl_bignum_to_llong(obj_t BgL_xz00_102)
	{
		{	/* Unsafe/bignumber.scm 1110 */
			{	/* Unsafe/bignumber.scm 1111 */
				long BgL_lenxzd2minuszd21z00_2277;

				{	/* Unsafe/bignumber.scm 1111 */
					long BgL_arg1971z00_2302;

					{	/* Unsafe/bignumber.scm 261 */
						long BgL_arg1210z00_5159;

						{	/* Unsafe/bignumber.scm 261 */
							obj_t BgL_arg1212z00_5160;

							BgL_arg1212z00_5160 = BGL_BIGNUM_U16VECT(BgL_xz00_102);
							BgL_arg1210z00_5159 = BGL_HVECTOR_LENGTH(BgL_arg1212z00_5160);
						}
						{	/* Unsafe/bignumber.scm 261 */
							uint16_t BgL_xz00_5162;

							BgL_xz00_5162 = (uint16_t) (BgL_arg1210z00_5159);
							{	/* Unsafe/bignumber.scm 261 */
								long BgL_arg2271z00_5163;

								BgL_arg2271z00_5163 = (long) (BgL_xz00_5162);
								BgL_arg1971z00_2302 = (long) (BgL_arg2271z00_5163);
					}}}
					BgL_lenxzd2minuszd21z00_2277 = (BgL_arg1971z00_2302 - 1L);
				}
				{
					BGL_LONGLONG_T BgL_nz00_2279;
					long BgL_iz00_2280;

					{	/* Unsafe/bignumber.scm 1112 */
						obj_t BgL_tmpz00_8031;

						BgL_nz00_2279 = ((BGL_LONGLONG_T) 0);
						BgL_iz00_2280 = BgL_lenxzd2minuszd21z00_2277;
					BgL_zc3z04anonymousza31953ze3z87_2281:
						if ((0L < BgL_iz00_2280))
							{	/* Unsafe/bignumber.scm 1113 */
								if (
									(BgL_nz00_2279 <
										BGl_bignumzd2minzd2llongzd2divzd2radixz00zz__bignumz00()))
									{	/* Unsafe/bignumber.scm 1114 */
										BgL_tmpz00_8031 = BFALSE;
									}
								else
									{	/* Unsafe/bignumber.scm 1116 */
										BGL_LONGLONG_T BgL_mz00_2285;
										BGL_LONGLONG_T BgL_dz00_2286;

										BgL_mz00_2285 =
											(BgL_nz00_2279 * (LONG_TO_LLONG(1L) << (int) (30L)));
										{	/* Unsafe/bignumber.scm 1117 */
											long BgL_arg1965z00_2295;

											{	/* Unsafe/bignumber.scm 1117 */
												int BgL_iz00_5173;

												BgL_iz00_5173 = (int) (BgL_iz00_2280);
												{	/* Unsafe/bignumber.scm 267 */
													uint16_t BgL_arg1220z00_5174;

													{	/* Unsafe/bignumber.scm 267 */
														obj_t BgL_arg1221z00_5175;

														BgL_arg1221z00_5175 =
															BGL_BIGNUM_U16VECT(BgL_xz00_102);
														{	/* Unsafe/bignumber.scm 267 */
															long BgL_tmpz00_8043;

															BgL_tmpz00_8043 = (long) (BgL_iz00_5173);
															BgL_arg1220z00_5174 =
																BGL_U16VREF(BgL_arg1221z00_5175,
																BgL_tmpz00_8043);
													}}
													{	/* Unsafe/bignumber.scm 267 */
														long BgL_arg2271z00_5177;

														BgL_arg2271z00_5177 = (long) (BgL_arg1220z00_5174);
														BgL_arg1965z00_2295 = (long) (BgL_arg2271z00_5177);
											}}}
											BgL_dz00_2286 = LONG_TO_LLONG(BgL_arg1965z00_2295);
										}
										if (
											(BgL_mz00_2285 <
												(((-((BGL_LONGLONG_T) 2)) *
														(LONG_TO_LLONG(1L) <<
															(int) (30L))) + BgL_dz00_2286)))
											{	/* Unsafe/bignumber.scm 1118 */
												BgL_tmpz00_8031 = BFALSE;
											}
										else
											{
												long BgL_iz00_8058;
												BGL_LONGLONG_T BgL_nz00_8056;

												BgL_nz00_8056 = (BgL_mz00_2285 - BgL_dz00_2286);
												BgL_iz00_8058 = (BgL_iz00_2280 - 1L);
												BgL_iz00_2280 = BgL_iz00_8058;
												BgL_nz00_2279 = BgL_nz00_8056;
												goto BgL_zc3z04anonymousza31953ze3z87_2281;
											}
									}
							}
						else
							{	/* Unsafe/bignumber.scm 1122 */
								bool_t BgL_test2854z00_8060;

								{	/* Unsafe/bignumber.scm 471 */
									long BgL_arg1428z00_5191;

									{	/* Unsafe/bignumber.scm 263 */
										uint16_t BgL_arg1215z00_5193;

										{	/* Unsafe/bignumber.scm 263 */
											obj_t BgL_arg1216z00_5194;

											BgL_arg1216z00_5194 = BGL_BIGNUM_U16VECT(BgL_xz00_102);
											BgL_arg1215z00_5193 =
												BGL_U16VREF(BgL_arg1216z00_5194, 0L);
										}
										{	/* Unsafe/bignumber.scm 263 */
											long BgL_arg2271z00_5196;

											BgL_arg2271z00_5196 = (long) (BgL_arg1215z00_5193);
											BgL_arg1428z00_5191 = (long) (BgL_arg2271z00_5196);
									}}
									BgL_test2854z00_8060 = (BgL_arg1428z00_5191 == 0L);
								}
								if (BgL_test2854z00_8060)
									{	/* Unsafe/bignumber.scm 1122 */
										BgL_tmpz00_8031 = make_bllong(BgL_nz00_2279);
									}
								else
									{	/* Unsafe/bignumber.scm 1122 */
										if (
											(BgL_nz00_2279 ==
												((-((BGL_LONGLONG_T) 2)) *
													(LONG_TO_LLONG(1L) << (int) (30L)))))
											{	/* Unsafe/bignumber.scm 1124 */
												BgL_tmpz00_8031 = BFALSE;
											}
										else
											{	/* Unsafe/bignumber.scm 1124 */
												BgL_tmpz00_8031 =
													make_bllong((((BGL_LONGLONG_T) 0) - BgL_nz00_2279));
											}
									}
							}
						return BLLONG_TO_LLONG(BgL_tmpz00_8031);
					}
				}
			}
		}

	}



/* &$$bignum->llong */
	obj_t BGl_z62z42z42bignumzd2ze3llongz53zz__bignumz00(obj_t BgL_envz00_5570,
		obj_t BgL_xz00_5571)
	{
		{	/* Unsafe/bignumber.scm 1110 */
			{	/* Unsafe/bignumber.scm 1111 */
				BGL_LONGLONG_T BgL_tmpz00_8076;

				{	/* Unsafe/bignumber.scm 1111 */
					obj_t BgL_auxz00_8077;

					if (BIGNUMP(BgL_xz00_5571))
						{	/* Unsafe/bignumber.scm 1111 */
							BgL_auxz00_8077 = BgL_xz00_5571;
						}
					else
						{
							obj_t BgL_auxz00_8080;

							BgL_auxz00_8080 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
								BINT(35771L), BGl_string2522z00zz__bignumz00,
								BGl_string2491z00zz__bignumz00, BgL_xz00_5571);
							FAILURE(BgL_auxz00_8080, BFALSE, BFALSE);
						}
					BgL_tmpz00_8076 = bgl_bignum_to_llong(BgL_auxz00_8077);
				}
				return make_bllong(BgL_tmpz00_8076);
			}
		}

	}



/* $$bignum->int64 */
	BGL_EXPORTED_DEF int64_t bgl_bignum_to_int64(obj_t BgL_xz00_103)
	{
		{	/* Unsafe/bignumber.scm 1129 */
			{	/* Unsafe/bignumber.scm 1130 */
				long BgL_lenxzd2minuszd21z00_2303;

				{	/* Unsafe/bignumber.scm 1130 */
					long BgL_arg1990z00_2328;

					{	/* Unsafe/bignumber.scm 261 */
						long BgL_arg1210z00_5208;

						{	/* Unsafe/bignumber.scm 261 */
							obj_t BgL_arg1212z00_5209;

							BgL_arg1212z00_5209 = BGL_BIGNUM_U16VECT(BgL_xz00_103);
							BgL_arg1210z00_5208 = BGL_HVECTOR_LENGTH(BgL_arg1212z00_5209);
						}
						{	/* Unsafe/bignumber.scm 261 */
							uint16_t BgL_xz00_5211;

							BgL_xz00_5211 = (uint16_t) (BgL_arg1210z00_5208);
							{	/* Unsafe/bignumber.scm 261 */
								long BgL_arg2271z00_5212;

								BgL_arg2271z00_5212 = (long) (BgL_xz00_5211);
								BgL_arg1990z00_2328 = (long) (BgL_arg2271z00_5212);
					}}}
					BgL_lenxzd2minuszd21z00_2303 = (BgL_arg1990z00_2328 - 1L);
				}
				{
					int64_t BgL_nz00_2305;
					long BgL_iz00_2306;

					{	/* Unsafe/bignumber.scm 1131 */
						obj_t BgL_tmpz00_8092;

						BgL_nz00_2305 = (int64_t) (0);
						BgL_iz00_2306 = BgL_lenxzd2minuszd21z00_2303;
					BgL_zc3z04anonymousza31972ze3z87_2307:
						if ((0L < BgL_iz00_2306))
							{	/* Unsafe/bignumber.scm 1132 */
								if (
									(BgL_nz00_2305 <
										BGl_bignumzd2minzd2int64zd2divzd2radixz00zz__bignumz00()))
									{	/* Unsafe/bignumber.scm 1133 */
										BgL_tmpz00_8092 = BFALSE;
									}
								else
									{	/* Unsafe/bignumber.scm 1135 */
										int64_t BgL_mz00_2311;
										int64_t BgL_dz00_2312;

										BgL_mz00_2311 =
											(BgL_nz00_2305 *
											BGl_expts64z00zz__r4_numbers_6_5_fixnumz00((int64_t) (2),
												(int64_t) (30L)));
										{	/* Unsafe/bignumber.scm 1136 */
											long BgL_arg1984z00_2321;

											{	/* Unsafe/bignumber.scm 1136 */
												int BgL_iz00_5221;

												BgL_iz00_5221 = (int) (BgL_iz00_2306);
												{	/* Unsafe/bignumber.scm 267 */
													uint16_t BgL_arg1220z00_5222;

													{	/* Unsafe/bignumber.scm 267 */
														obj_t BgL_arg1221z00_5223;

														BgL_arg1221z00_5223 =
															BGL_BIGNUM_U16VECT(BgL_xz00_103);
														{	/* Unsafe/bignumber.scm 267 */
															long BgL_tmpz00_8103;

															BgL_tmpz00_8103 = (long) (BgL_iz00_5221);
															BgL_arg1220z00_5222 =
																BGL_U16VREF(BgL_arg1221z00_5223,
																BgL_tmpz00_8103);
													}}
													{	/* Unsafe/bignumber.scm 267 */
														long BgL_arg2271z00_5225;

														BgL_arg2271z00_5225 = (long) (BgL_arg1220z00_5222);
														BgL_arg1984z00_2321 = (long) (BgL_arg2271z00_5225);
											}}}
											BgL_dz00_2312 = (int64_t) (BgL_arg1984z00_2321);
										}
										if (
											(BgL_mz00_2311 <
												(((int64_t) (-2) *
														BGl_expts64z00zz__r4_numbers_6_5_fixnumz00((int64_t)
															(2), (int64_t) (30L))) + BgL_dz00_2312)))
											{	/* Unsafe/bignumber.scm 1137 */
												BgL_tmpz00_8092 = BFALSE;
											}
										else
											{
												long BgL_iz00_8117;
												int64_t BgL_nz00_8115;

												BgL_nz00_8115 = (BgL_mz00_2311 - BgL_dz00_2312);
												BgL_iz00_8117 = (BgL_iz00_2306 - 1L);
												BgL_iz00_2306 = BgL_iz00_8117;
												BgL_nz00_2305 = BgL_nz00_8115;
												goto BgL_zc3z04anonymousza31972ze3z87_2307;
											}
									}
							}
						else
							{	/* Unsafe/bignumber.scm 1141 */
								bool_t BgL_test2860z00_8119;

								{	/* Unsafe/bignumber.scm 471 */
									long BgL_arg1428z00_5238;

									{	/* Unsafe/bignumber.scm 263 */
										uint16_t BgL_arg1215z00_5240;

										{	/* Unsafe/bignumber.scm 263 */
											obj_t BgL_arg1216z00_5241;

											BgL_arg1216z00_5241 = BGL_BIGNUM_U16VECT(BgL_xz00_103);
											BgL_arg1215z00_5240 =
												BGL_U16VREF(BgL_arg1216z00_5241, 0L);
										}
										{	/* Unsafe/bignumber.scm 263 */
											long BgL_arg2271z00_5243;

											BgL_arg2271z00_5243 = (long) (BgL_arg1215z00_5240);
											BgL_arg1428z00_5238 = (long) (BgL_arg2271z00_5243);
									}}
									BgL_test2860z00_8119 = (BgL_arg1428z00_5238 == 0L);
								}
								if (BgL_test2860z00_8119)
									{	/* Unsafe/bignumber.scm 1141 */
										BgL_tmpz00_8092 = BGL_INT64_TO_BINT64(BgL_nz00_2305);
									}
								else
									{	/* Unsafe/bignumber.scm 1143 */
										bool_t BgL_test2861z00_8126;

										{	/* Unsafe/bignumber.scm 1143 */
											int64_t BgL_arg1989z00_2326;

											BgL_arg1989z00_2326 =
												((int64_t) (-2) *
												BGl_expts64z00zz__r4_numbers_6_5_fixnumz00((int64_t)
													(2), (int64_t) (30L)));
											BgL_test2861z00_8126 =
												(BgL_nz00_2305 == BgL_arg1989z00_2326);
										}
										if (BgL_test2861z00_8126)
											{	/* Unsafe/bignumber.scm 1143 */
												BgL_tmpz00_8092 = BFALSE;
											}
										else
											{	/* Unsafe/bignumber.scm 1144 */
												int64_t BgL_tmpz00_8131;

												BgL_tmpz00_8131 =
													((int64_t) (((BGL_LONGLONG_T) 0)) - BgL_nz00_2305);
												BgL_tmpz00_8092 = BGL_INT64_TO_BINT64(BgL_tmpz00_8131);
											}
									}
							}
						return BGL_BINT64_TO_INT64(BgL_tmpz00_8092);
					}
				}
			}
		}

	}



/* &$$bignum->int64 */
	obj_t BGl_z62z42z42bignumzd2ze3int64z53zz__bignumz00(obj_t BgL_envz00_5572,
		obj_t BgL_xz00_5573)
	{
		{	/* Unsafe/bignumber.scm 1129 */
			{	/* Unsafe/bignumber.scm 1130 */
				int64_t BgL_tmpz00_8136;

				{	/* Unsafe/bignumber.scm 1130 */
					obj_t BgL_auxz00_8137;

					if (BIGNUMP(BgL_xz00_5573))
						{	/* Unsafe/bignumber.scm 1130 */
							BgL_auxz00_8137 = BgL_xz00_5573;
						}
					else
						{
							obj_t BgL_auxz00_8140;

							BgL_auxz00_8140 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
								BINT(36331L), BGl_string2523z00zz__bignumz00,
								BGl_string2491z00zz__bignumz00, BgL_xz00_5573);
							FAILURE(BgL_auxz00_8140, BFALSE, BFALSE);
						}
					BgL_tmpz00_8136 = bgl_bignum_to_int64(BgL_auxz00_8137);
				}
				return BGL_INT64_TO_BINT64(BgL_tmpz00_8136);
			}
		}

	}



/* $$bignum->uint64 */
	BGL_EXPORTED_DEF uint64_t bgl_bignum_to_uint64(obj_t BgL_xz00_104)
	{
		{	/* Unsafe/bignumber.scm 1148 */
			{	/* Unsafe/bignumber.scm 1149 */
				int64_t BgL_tmpz00_8146;

				BgL_tmpz00_8146 = bgl_bignum_to_int64(BgL_xz00_104);
				return (uint64_t) (BgL_tmpz00_8146);
			}
		}

	}



/* &$$bignum->uint64 */
	obj_t BGl_z62z42z42bignumzd2ze3uint64z53zz__bignumz00(obj_t BgL_envz00_5574,
		obj_t BgL_xz00_5575)
	{
		{	/* Unsafe/bignumber.scm 1148 */
			{	/* Unsafe/bignumber.scm 1149 */
				uint64_t BgL_tmpz00_8149;

				{	/* Unsafe/bignumber.scm 1149 */
					obj_t BgL_auxz00_8150;

					if (BIGNUMP(BgL_xz00_5575))
						{	/* Unsafe/bignumber.scm 1149 */
							BgL_auxz00_8150 = BgL_xz00_5575;
						}
					else
						{
							obj_t BgL_auxz00_8153;

							BgL_auxz00_8153 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
								BINT(36896L), BGl_string2524z00zz__bignumz00,
								BGl_string2491z00zz__bignumz00, BgL_xz00_5575);
							FAILURE(BgL_auxz00_8153, BFALSE, BFALSE);
						}
					BgL_tmpz00_8149 = bgl_bignum_to_uint64(BgL_auxz00_8150);
				}
				return BGL_UINT64_TO_BUINT64(BgL_tmpz00_8149);
			}
		}

	}



/* $$seed-rand */
	BGL_EXPORTED_DEF obj_t bgl_seed_rand(long BgL_seedz00_105)
	{
		{	/* Unsafe/bignumber.scm 1154 */
			{	/* Unsafe/bignumber.scm 1155 */
				int BgL_tmpz00_8159;

				BgL_tmpz00_8159 = (int) (BgL_seedz00_105);
				srand(BgL_tmpz00_8159);
			} BUNSPEC;
			return BINT(BgL_seedz00_105);
		}

	}



/* &$$seed-rand */
	obj_t BGl_z62z42z42seedzd2randzb0zz__bignumz00(obj_t BgL_envz00_5576,
		obj_t BgL_seedz00_5577)
	{
		{	/* Unsafe/bignumber.scm 1154 */
			{	/* Unsafe/bignumber.scm 1155 */
				long BgL_auxz00_8163;

				{	/* Unsafe/bignumber.scm 1155 */
					obj_t BgL_tmpz00_8164;

					if (INTEGERP(BgL_seedz00_5577))
						{	/* Unsafe/bignumber.scm 1155 */
							BgL_tmpz00_8164 = BgL_seedz00_5577;
						}
					else
						{
							obj_t BgL_auxz00_8167;

							BgL_auxz00_8167 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
								BINT(37183L), BGl_string2525z00zz__bignumz00,
								BGl_string2480z00zz__bignumz00, BgL_seedz00_5577);
							FAILURE(BgL_auxz00_8167, BFALSE, BFALSE);
						}
					BgL_auxz00_8163 = (long) CINT(BgL_tmpz00_8164);
				}
				return bgl_seed_rand(BgL_auxz00_8163);
			}
		}

	}



/* make-random-u8vector */
	obj_t BGl_makezd2randomzd2u8vectorz00zz__bignumz00(obj_t BgL_lenz00_106)
	{
		{	/* Unsafe/bignumber.scm 1161 */
			{
				obj_t BgL_vecz00_2334;

				{	/* Unsafe/bignumber.scm 1169 */
					obj_t BgL_arg1992z00_2331;

					{	/* Llib/srfi4.scm 447 */

						BgL_arg1992z00_2331 =
							BGl_makezd2u8vectorzd2zz__srfi4z00(
							(long) CINT(BgL_lenz00_106), (uint8_t) (0));
					}
					BgL_vecz00_2334 = BgL_arg1992z00_2331;
					{	/* Unsafe/bignumber.scm 1163 */
						long BgL_g1068z00_2336;

						{	/* Unsafe/bignumber.scm 1163 */
							long BgL_arg1999z00_2344;

							BgL_arg1999z00_2344 = BGL_HVECTOR_LENGTH(BgL_vecz00_2334);
							BgL_g1068z00_2336 = (BgL_arg1999z00_2344 - 1L);
						}
						{
							long BgL_iz00_2338;

							BgL_iz00_2338 = BgL_g1068z00_2336;
						BgL_zc3z04anonymousza31995ze3z87_2339:
							if ((BgL_iz00_2338 == -1L))
								{	/* Unsafe/bignumber.scm 1164 */
									return BgL_vecz00_2334;
								}
							else
								{	/* Unsafe/bignumber.scm 1164 */
									{	/* Unsafe/bignumber.scm 1167 */
										long BgL_arg1997z00_2341;

										{	/* Unsafe/bignumber.scm 1167 */
											int BgL_arg2163z00_5259;

											BgL_arg2163z00_5259 = rand();
											BgL_arg1997z00_2341 =
												BGl_modulofxz00zz__r4_numbers_6_5_fixnumz00(
												(long) (BgL_arg2163z00_5259), 256L);
										}
										{	/* Unsafe/bignumber.scm 1167 */
											uint8_t BgL_tmpz00_8182;

											BgL_tmpz00_8182 = (uint8_t) (BgL_arg1997z00_2341);
											BGL_U8VSET(BgL_vecz00_2334, BgL_iz00_2338,
												BgL_tmpz00_8182);
										} BUNSPEC;
									}
									{
										long BgL_iz00_8185;

										BgL_iz00_8185 = (BgL_iz00_2338 - 1L);
										BgL_iz00_2338 = BgL_iz00_8185;
										goto BgL_zc3z04anonymousza31995ze3z87_2339;
									}
								}
						}
					}
				}
			}
		}

	}



/* $$randbx */
	BGL_EXPORTED_DEF obj_t bgl_rand_bignum(obj_t BgL_rangez00_107)
	{
		{	/* Unsafe/bignumber.scm 1174 */
			{	/* Unsafe/bignumber.scm 1175 */
				long BgL_rangezd2bitszd2_2346;

				BgL_rangezd2bitszd2_2346 =
					BGl_bignumzd2integerzd2lengthz00zz__bignumz00(BgL_rangez00_107);
				{	/* Unsafe/bignumber.scm 1175 */
					obj_t BgL_lenz00_2347;

					{	/* Unsafe/bignumber.scm 1176 */
						long BgL_arg2006z00_2360;

						BgL_arg2006z00_2360 = (BgL_rangezd2bitszd2_2346 + 20L);
						BgL_lenz00_2347 =
							BGl_quotientz00zz__r4_numbers_6_5_fixnumz00(BINT
							(BgL_arg2006z00_2360), BINT(8L));
					}
					{	/* Unsafe/bignumber.scm 1176 */
						obj_t BgL_nz00_2348;

						BgL_nz00_2348 =
							BGl_bignumzd2exptzd2zz__bignumz00(bgl_long_to_bignum(256L),
							bgl_long_to_bignum((long) CINT(BgL_lenz00_2347)));
						{	/* Unsafe/bignumber.scm 1177 */
							obj_t BgL_divisorz00_2349;

							{	/* Unsafe/bignumber.scm 1178 */
								obj_t BgL_res2361z00_5268;

								{	/* Unsafe/bignumber.scm 859 */
									obj_t BgL_arg1756z00_5265;

									{	/* Unsafe/bignumber.scm 859 */
										obj_t BgL_arg1757z00_5266;

										BgL_arg1757z00_5266 =
											BGl_bignumzd2divzd2zz__bignumz00(BgL_nz00_2348,
											BgL_rangez00_107);
										BgL_arg1756z00_5265 = CAR(((obj_t) BgL_arg1757z00_5266));
									}
									BgL_res2361z00_5268 = BgL_arg1756z00_5265;
								}
								BgL_divisorz00_2349 = BgL_res2361z00_5268;
							}
							{	/* Unsafe/bignumber.scm 1178 */
								obj_t BgL_limitz00_2350;

								BgL_limitz00_2350 =
									bgl_bignum_mul(BgL_divisorz00_2349, BgL_rangez00_107);
								{	/* Unsafe/bignumber.scm 1179 */

									{

									BgL_zc3z04anonymousza32000ze3z87_2352:
										{	/* Unsafe/bignumber.scm 1181 */
											obj_t BgL_u8vectz00_2353;

											BgL_u8vectz00_2353 =
												BGl_makezd2randomzd2u8vectorz00zz__bignumz00
												(BgL_lenz00_2347);
											{	/* Unsafe/bignumber.scm 1181 */
												obj_t BgL_xz00_2354;

												BgL_xz00_2354 =
													BGl_fixnumzd2listzd2ze3bignumze3zz__bignumz00
													(BGl_u8vectorzd2ze3listz31zz__srfi4z00
													(BgL_u8vectz00_2353), 255L);
												{	/* Unsafe/bignumber.scm 1182 */

													{	/* Unsafe/bignumber.scm 1183 */
														bool_t BgL_test2866z00_8203;

														if (BGl_z42zc3bxz81zz__bignumz00(BgL_xz00_2354,
																BgL_limitz00_2350))
															{	/* Unsafe/bignumber.scm 465 */
																BgL_test2866z00_8203 = ((bool_t) 0);
															}
														else
															{	/* Unsafe/bignumber.scm 465 */
																BgL_test2866z00_8203 = ((bool_t) 1);
															}
														if (BgL_test2866z00_8203)
															{	/* Unsafe/bignumber.scm 1183 */
																goto BgL_zc3z04anonymousza32000ze3z87_2352;
															}
														else
															{	/* Unsafe/bignumber.scm 1185 */
																obj_t BgL_res2362z00_5277;

																{	/* Unsafe/bignumber.scm 859 */
																	obj_t BgL_arg1756z00_5274;

																	{	/* Unsafe/bignumber.scm 859 */
																		obj_t BgL_arg1757z00_5275;

																		BgL_arg1757z00_5275 =
																			BGl_bignumzd2divzd2zz__bignumz00
																			(BgL_xz00_2354, BgL_divisorz00_2349);
																		BgL_arg1756z00_5274 =
																			CAR(((obj_t) BgL_arg1757z00_5275));
																	}
																	BgL_res2362z00_5277 = BgL_arg1756z00_5274;
																}
																return BgL_res2362z00_5277;
															}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &$$randbx */
	obj_t BGl_z62z42z42randbxz62zz__bignumz00(obj_t BgL_envz00_5578,
		obj_t BgL_rangez00_5579)
	{
		{	/* Unsafe/bignumber.scm 1174 */
			{	/* Unsafe/bignumber.scm 1175 */
				obj_t BgL_auxz00_8209;

				if (BIGNUMP(BgL_rangez00_5579))
					{	/* Unsafe/bignumber.scm 1175 */
						BgL_auxz00_8209 = BgL_rangez00_5579;
					}
				else
					{
						obj_t BgL_auxz00_8212;

						BgL_auxz00_8212 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
							BINT(37950L), BGl_string2526z00zz__bignumz00,
							BGl_string2491z00zz__bignumz00, BgL_rangez00_5579);
						FAILURE(BgL_auxz00_8212, BFALSE, BFALSE);
					}
				return bgl_rand_bignum(BgL_auxz00_8209);
			}
		}

	}



/* $$flonum->bignum */
	BGL_EXPORTED_DEF obj_t bgl_flonum_to_bignum(double BgL_nz00_108)
	{
		{	/* Unsafe/bignumber.scm 1190 */
			{	/* Unsafe/bignumber.scm 1191 */
				long BgL_arg2007z00_5278;

				BgL_arg2007z00_5278 = (long) (BgL_nz00_108);
				return bgl_long_to_bignum(BgL_arg2007z00_5278);
			}
		}

	}



/* &$$flonum->bignum */
	obj_t BGl_z62z42z42flonumzd2ze3bignumz53zz__bignumz00(obj_t BgL_envz00_5580,
		obj_t BgL_nz00_5581)
	{
		{	/* Unsafe/bignumber.scm 1190 */
			{	/* Unsafe/bignumber.scm 1191 */
				double BgL_auxz00_8219;

				{	/* Unsafe/bignumber.scm 1191 */
					obj_t BgL_tmpz00_8220;

					if (REALP(BgL_nz00_5581))
						{	/* Unsafe/bignumber.scm 1191 */
							BgL_tmpz00_8220 = BgL_nz00_5581;
						}
					else
						{
							obj_t BgL_auxz00_8223;

							BgL_auxz00_8223 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
								BINT(38631L), BGl_string2527z00zz__bignumz00,
								BGl_string2528z00zz__bignumz00, BgL_nz00_5581);
							FAILURE(BgL_auxz00_8223, BFALSE, BFALSE);
						}
					BgL_auxz00_8219 = REAL_TO_DOUBLE(BgL_tmpz00_8220);
				}
				return bgl_flonum_to_bignum(BgL_auxz00_8219);
			}
		}

	}



/* $$bignum->flonum */
	BGL_EXPORTED_DEF double bgl_bignum_to_flonum(obj_t BgL_nz00_109)
	{
		{	/* Unsafe/bignumber.scm 1193 */
			{	/* Unsafe/bignumber.scm 1194 */
				long BgL_tmpz00_8229;

				BgL_tmpz00_8229 = bgl_bignum_to_long(BgL_nz00_109);
				return (double) (BgL_tmpz00_8229);
		}}

	}



/* &$$bignum->flonum */
	obj_t BGl_z62z42z42bignumzd2ze3flonumz53zz__bignumz00(obj_t BgL_envz00_5582,
		obj_t BgL_nz00_5583)
	{
		{	/* Unsafe/bignumber.scm 1193 */
			{	/* Unsafe/bignumber.scm 1194 */
				double BgL_tmpz00_8232;

				{	/* Unsafe/bignumber.scm 1194 */
					obj_t BgL_auxz00_8233;

					if (BIGNUMP(BgL_nz00_5583))
						{	/* Unsafe/bignumber.scm 1194 */
							BgL_auxz00_8233 = BgL_nz00_5583;
						}
					else
						{
							obj_t BgL_auxz00_8236;

							BgL_auxz00_8236 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
								BINT(38703L), BGl_string2529z00zz__bignumz00,
								BGl_string2491z00zz__bignumz00, BgL_nz00_5583);
							FAILURE(BgL_auxz00_8236, BFALSE, BFALSE);
						}
					BgL_tmpz00_8232 = bgl_bignum_to_flonum(BgL_auxz00_8233);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_8232);
			}
		}

	}



/* $$bitlshbx */
	BGL_EXPORTED_DEF obj_t bgl_bignum_lsh(obj_t BgL_za7za7_110, long BgL_nz00_111)
	{
		{	/* Unsafe/bignumber.scm 1199 */
			{	/* Unsafe/bignumber.scm 1200 */
				obj_t BgL_arg2009z00_5281;

				{	/* Unsafe/bignumber.scm 1200 */
					obj_t BgL_arg2010z00_5282;
					obj_t BgL_arg2011z00_5283;

					{	/* Unsafe/bignumber.scm 1200 */
						obj_t BgL_res2363z00_5292;

						{	/* Unsafe/bignumber.scm 415 */
							bool_t BgL_test2871z00_8242;

							if ((2L < -16L))
								{	/* Unsafe/bignumber.scm 415 */
									BgL_test2871z00_8242 = ((bool_t) 1);
								}
							else
								{	/* Unsafe/bignumber.scm 415 */
									BgL_test2871z00_8242 = (16L < 2L);
								}
							if (BgL_test2871z00_8242)
								{	/* Unsafe/bignumber.scm 415 */
									BgL_res2363z00_5292 =
										BGl_z42z42fixnumzd2ze3bignumzd2freshze3zz__bignumz00(2L);
								}
							else
								{	/* Unsafe/bignumber.scm 415 */
									BgL_res2363z00_5292 =
										VECTOR_REF(BGl_preallocatedzd2bignumszd2zz__bignumz00,
										(2L + 16L));
								}
						}
						BgL_arg2010z00_5282 = BgL_res2363z00_5292;
					}
					BgL_arg2011z00_5283 = bgl_long_to_bignum(BgL_nz00_111);
					BgL_arg2009z00_5281 =
						bgl_bignum_expt(BgL_arg2010z00_5282, BgL_arg2011z00_5283);
				}
				return bgl_bignum_mul(BgL_za7za7_110, BgL_arg2009z00_5281);
			}
		}

	}



/* &$$bitlshbx */
	obj_t BGl_z62z42z42bitlshbxz62zz__bignumz00(obj_t BgL_envz00_5584,
		obj_t BgL_za7za7_5585, obj_t BgL_nz00_5586)
	{
		{	/* Unsafe/bignumber.scm 1199 */
			{	/* Unsafe/bignumber.scm 1200 */
				long BgL_auxz00_8259;
				obj_t BgL_auxz00_8252;

				{	/* Unsafe/bignumber.scm 1200 */
					obj_t BgL_tmpz00_8260;

					if (INTEGERP(BgL_nz00_5586))
						{	/* Unsafe/bignumber.scm 1200 */
							BgL_tmpz00_8260 = BgL_nz00_5586;
						}
					else
						{
							obj_t BgL_auxz00_8263;

							BgL_auxz00_8263 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
								BINT(38996L), BGl_string2530z00zz__bignumz00,
								BGl_string2480z00zz__bignumz00, BgL_nz00_5586);
							FAILURE(BgL_auxz00_8263, BFALSE, BFALSE);
						}
					BgL_auxz00_8259 = (long) CINT(BgL_tmpz00_8260);
				}
				if (BIGNUMP(BgL_za7za7_5585))
					{	/* Unsafe/bignumber.scm 1200 */
						BgL_auxz00_8252 = BgL_za7za7_5585;
					}
				else
					{
						obj_t BgL_auxz00_8255;

						BgL_auxz00_8255 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
							BINT(38996L), BGl_string2530z00zz__bignumz00,
							BGl_string2491z00zz__bignumz00, BgL_za7za7_5585);
						FAILURE(BgL_auxz00_8255, BFALSE, BFALSE);
					}
				return bgl_bignum_lsh(BgL_auxz00_8252, BgL_auxz00_8259);
			}
		}

	}



/* $$bitrshbx */
	BGL_EXPORTED_DEF obj_t bgl_bignum_rsh(obj_t BgL_za7za7_112, long BgL_nz00_113)
	{
		{	/* Unsafe/bignumber.scm 1202 */
			{	/* Unsafe/bignumber.scm 1203 */
				obj_t BgL_arg2012z00_5293;

				{	/* Unsafe/bignumber.scm 1203 */
					obj_t BgL_arg2013z00_5294;
					obj_t BgL_arg2014z00_5295;

					{	/* Unsafe/bignumber.scm 1203 */
						obj_t BgL_res2364z00_5304;

						{	/* Unsafe/bignumber.scm 415 */
							bool_t BgL_test2875z00_8269;

							if ((2L < -16L))
								{	/* Unsafe/bignumber.scm 415 */
									BgL_test2875z00_8269 = ((bool_t) 1);
								}
							else
								{	/* Unsafe/bignumber.scm 415 */
									BgL_test2875z00_8269 = (16L < 2L);
								}
							if (BgL_test2875z00_8269)
								{	/* Unsafe/bignumber.scm 415 */
									BgL_res2364z00_5304 =
										BGl_z42z42fixnumzd2ze3bignumzd2freshze3zz__bignumz00(2L);
								}
							else
								{	/* Unsafe/bignumber.scm 415 */
									BgL_res2364z00_5304 =
										VECTOR_REF(BGl_preallocatedzd2bignumszd2zz__bignumz00,
										(2L + 16L));
								}
						}
						BgL_arg2013z00_5294 = BgL_res2364z00_5304;
					}
					BgL_arg2014z00_5295 = bgl_long_to_bignum(BgL_nz00_113);
					BgL_arg2012z00_5293 =
						bgl_bignum_expt(BgL_arg2013z00_5294, BgL_arg2014z00_5295);
				}
				return
					BGl_bignumzd2divzd2zz__bignumz00(BgL_za7za7_112, BgL_arg2012z00_5293);
			}
		}

	}



/* &$$bitrshbx */
	obj_t BGl_z62z42z42bitrshbxz62zz__bignumz00(obj_t BgL_envz00_5587,
		obj_t BgL_za7za7_5588, obj_t BgL_nz00_5589)
	{
		{	/* Unsafe/bignumber.scm 1202 */
			{	/* Unsafe/bignumber.scm 1203 */
				long BgL_auxz00_8286;
				obj_t BgL_auxz00_8279;

				{	/* Unsafe/bignumber.scm 1203 */
					obj_t BgL_tmpz00_8287;

					if (INTEGERP(BgL_nz00_5589))
						{	/* Unsafe/bignumber.scm 1203 */
							BgL_tmpz00_8287 = BgL_nz00_5589;
						}
					else
						{
							obj_t BgL_auxz00_8290;

							BgL_auxz00_8290 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
								BINT(39094L), BGl_string2531z00zz__bignumz00,
								BGl_string2480z00zz__bignumz00, BgL_nz00_5589);
							FAILURE(BgL_auxz00_8290, BFALSE, BFALSE);
						}
					BgL_auxz00_8286 = (long) CINT(BgL_tmpz00_8287);
				}
				if (BIGNUMP(BgL_za7za7_5588))
					{	/* Unsafe/bignumber.scm 1203 */
						BgL_auxz00_8279 = BgL_za7za7_5588;
					}
				else
					{
						obj_t BgL_auxz00_8282;

						BgL_auxz00_8282 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
							BINT(39094L), BGl_string2531z00zz__bignumz00,
							BGl_string2491z00zz__bignumz00, BgL_za7za7_5588);
						FAILURE(BgL_auxz00_8282, BFALSE, BFALSE);
					}
				return bgl_bignum_rsh(BgL_auxz00_8279, BgL_auxz00_8286);
			}
		}

	}



/* $$bitmaskbx */
	BGL_EXPORTED_DEF obj_t bgl_bignum_mask(obj_t BgL_xz00_114, long BgL_yz00_115)
	{
		{	/* Unsafe/bignumber.scm 1205 */
			return
				BGl_errorz00zz__errorz00(BGl_string2532z00zz__bignumz00,
				BGl_string2533z00zz__bignumz00, BgL_xz00_114);
		}

	}



/* &$$bitmaskbx */
	obj_t BGl_z62z42z42bitmaskbxz62zz__bignumz00(obj_t BgL_envz00_5590,
		obj_t BgL_xz00_5591, obj_t BgL_yz00_5592)
	{
		{	/* Unsafe/bignumber.scm 1205 */
			{	/* Unsafe/bignumber.scm 1206 */
				long BgL_auxz00_8304;
				obj_t BgL_auxz00_8297;

				{	/* Unsafe/bignumber.scm 1206 */
					obj_t BgL_tmpz00_8305;

					if (INTEGERP(BgL_yz00_5592))
						{	/* Unsafe/bignumber.scm 1206 */
							BgL_tmpz00_8305 = BgL_yz00_5592;
						}
					else
						{
							obj_t BgL_auxz00_8308;

							BgL_auxz00_8308 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
								BINT(39169L), BGl_string2534z00zz__bignumz00,
								BGl_string2480z00zz__bignumz00, BgL_yz00_5592);
							FAILURE(BgL_auxz00_8308, BFALSE, BFALSE);
						}
					BgL_auxz00_8304 = (long) CINT(BgL_tmpz00_8305);
				}
				if (BIGNUMP(BgL_xz00_5591))
					{	/* Unsafe/bignumber.scm 1206 */
						BgL_auxz00_8297 = BgL_xz00_5591;
					}
				else
					{
						obj_t BgL_auxz00_8300;

						BgL_auxz00_8300 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
							BINT(39169L), BGl_string2534z00zz__bignumz00,
							BGl_string2491z00zz__bignumz00, BgL_xz00_5591);
						FAILURE(BgL_auxz00_8300, BFALSE, BFALSE);
					}
				return bgl_bignum_mask(BgL_auxz00_8297, BgL_auxz00_8304);
			}
		}

	}



/* $$bitorbx */
	BGL_EXPORTED_DEF obj_t bgl_bignum_or(obj_t BgL_xz00_116, obj_t BgL_yz00_117)
	{
		{	/* Unsafe/bignumber.scm 1208 */
			return
				BGl_errorz00zz__errorz00(BGl_string2535z00zz__bignumz00,
				BGl_string2533z00zz__bignumz00, BgL_xz00_116);
		}

	}



/* &$$bitorbx */
	obj_t BGl_z62z42z42bitorbxz62zz__bignumz00(obj_t BgL_envz00_5593,
		obj_t BgL_xz00_5594, obj_t BgL_yz00_5595)
	{
		{	/* Unsafe/bignumber.scm 1208 */
			{	/* Unsafe/bignumber.scm 1209 */
				obj_t BgL_auxz00_8322;
				obj_t BgL_auxz00_8315;

				if (BIGNUMP(BgL_yz00_5595))
					{	/* Unsafe/bignumber.scm 1209 */
						BgL_auxz00_8322 = BgL_yz00_5595;
					}
				else
					{
						obj_t BgL_auxz00_8325;

						BgL_auxz00_8325 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
							BINT(39238L), BGl_string2536z00zz__bignumz00,
							BGl_string2491z00zz__bignumz00, BgL_yz00_5595);
						FAILURE(BgL_auxz00_8325, BFALSE, BFALSE);
					}
				if (BIGNUMP(BgL_xz00_5594))
					{	/* Unsafe/bignumber.scm 1209 */
						BgL_auxz00_8315 = BgL_xz00_5594;
					}
				else
					{
						obj_t BgL_auxz00_8318;

						BgL_auxz00_8318 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
							BINT(39238L), BGl_string2536z00zz__bignumz00,
							BGl_string2491z00zz__bignumz00, BgL_xz00_5594);
						FAILURE(BgL_auxz00_8318, BFALSE, BFALSE);
					}
				return bgl_bignum_or(BgL_auxz00_8315, BgL_auxz00_8322);
			}
		}

	}



/* $$bitxorbx */
	BGL_EXPORTED_DEF obj_t bgl_bignum_xor(obj_t BgL_xz00_118, obj_t BgL_yz00_119)
	{
		{	/* Unsafe/bignumber.scm 1211 */
			return
				BGl_errorz00zz__errorz00(BGl_string2537z00zz__bignumz00,
				BGl_string2533z00zz__bignumz00, BgL_xz00_118);
		}

	}



/* &$$bitxorbx */
	obj_t BGl_z62z42z42bitxorbxz62zz__bignumz00(obj_t BgL_envz00_5596,
		obj_t BgL_xz00_5597, obj_t BgL_yz00_5598)
	{
		{	/* Unsafe/bignumber.scm 1211 */
			{	/* Unsafe/bignumber.scm 1212 */
				obj_t BgL_auxz00_8338;
				obj_t BgL_auxz00_8331;

				if (BIGNUMP(BgL_yz00_5598))
					{	/* Unsafe/bignumber.scm 1212 */
						BgL_auxz00_8338 = BgL_yz00_5598;
					}
				else
					{
						obj_t BgL_auxz00_8341;

						BgL_auxz00_8341 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
							BINT(39306L), BGl_string2538z00zz__bignumz00,
							BGl_string2491z00zz__bignumz00, BgL_yz00_5598);
						FAILURE(BgL_auxz00_8341, BFALSE, BFALSE);
					}
				if (BIGNUMP(BgL_xz00_5597))
					{	/* Unsafe/bignumber.scm 1212 */
						BgL_auxz00_8331 = BgL_xz00_5597;
					}
				else
					{
						obj_t BgL_auxz00_8334;

						BgL_auxz00_8334 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
							BINT(39306L), BGl_string2538z00zz__bignumz00,
							BGl_string2491z00zz__bignumz00, BgL_xz00_5597);
						FAILURE(BgL_auxz00_8334, BFALSE, BFALSE);
					}
				return bgl_bignum_xor(BgL_auxz00_8331, BgL_auxz00_8338);
			}
		}

	}



/* $$bitandbx */
	BGL_EXPORTED_DEF obj_t bgl_bignum_and(obj_t BgL_xz00_120, obj_t BgL_yz00_121)
	{
		{	/* Unsafe/bignumber.scm 1214 */
			return
				BGl_errorz00zz__errorz00(BGl_string2539z00zz__bignumz00,
				BGl_string2533z00zz__bignumz00, BgL_xz00_120);
		}

	}



/* &$$bitandbx */
	obj_t BGl_z62z42z42bitandbxz62zz__bignumz00(obj_t BgL_envz00_5599,
		obj_t BgL_xz00_5600, obj_t BgL_yz00_5601)
	{
		{	/* Unsafe/bignumber.scm 1214 */
			{	/* Unsafe/bignumber.scm 1215 */
				obj_t BgL_auxz00_8354;
				obj_t BgL_auxz00_8347;

				if (BIGNUMP(BgL_yz00_5601))
					{	/* Unsafe/bignumber.scm 1215 */
						BgL_auxz00_8354 = BgL_yz00_5601;
					}
				else
					{
						obj_t BgL_auxz00_8357;

						BgL_auxz00_8357 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
							BINT(39375L), BGl_string2540z00zz__bignumz00,
							BGl_string2491z00zz__bignumz00, BgL_yz00_5601);
						FAILURE(BgL_auxz00_8357, BFALSE, BFALSE);
					}
				if (BIGNUMP(BgL_xz00_5600))
					{	/* Unsafe/bignumber.scm 1215 */
						BgL_auxz00_8347 = BgL_xz00_5600;
					}
				else
					{
						obj_t BgL_auxz00_8350;

						BgL_auxz00_8350 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
							BINT(39375L), BGl_string2540z00zz__bignumz00,
							BGl_string2491z00zz__bignumz00, BgL_xz00_5600);
						FAILURE(BgL_auxz00_8350, BFALSE, BFALSE);
					}
				return bgl_bignum_and(BgL_auxz00_8347, BgL_auxz00_8354);
			}
		}

	}



/* $$bitnotbx */
	BGL_EXPORTED_DEF obj_t bgl_bignum_not(obj_t BgL_xz00_122)
	{
		{	/* Unsafe/bignumber.scm 1217 */
			return
				BGl_errorz00zz__errorz00(BGl_string2541z00zz__bignumz00,
				BGl_string2533z00zz__bignumz00, BgL_xz00_122);
		}

	}



/* &$$bitnotbx */
	obj_t BGl_z62z42z42bitnotbxz62zz__bignumz00(obj_t BgL_envz00_5602,
		obj_t BgL_xz00_5603)
	{
		{	/* Unsafe/bignumber.scm 1217 */
			{	/* Unsafe/bignumber.scm 1218 */
				obj_t BgL_auxz00_8363;

				if (BIGNUMP(BgL_xz00_5603))
					{	/* Unsafe/bignumber.scm 1218 */
						BgL_auxz00_8363 = BgL_xz00_5603;
					}
				else
					{
						obj_t BgL_auxz00_8366;

						BgL_auxz00_8366 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__bignumz00,
							BINT(39442L), BGl_string2542z00zz__bignumz00,
							BGl_string2491z00zz__bignumz00, BgL_xz00_5603);
						FAILURE(BgL_auxz00_8366, BFALSE, BFALSE);
					}
				return bgl_bignum_not(BgL_auxz00_8363);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__bignumz00(void)
	{
		{	/* Unsafe/bignumber.scm 45 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__bignumz00(void)
	{
		{	/* Unsafe/bignumber.scm 45 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__bignumz00(void)
	{
		{	/* Unsafe/bignumber.scm 45 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__bignumz00(void)
	{
		{	/* Unsafe/bignumber.scm 45 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string2543z00zz__bignumz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string2543z00zz__bignumz00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string2543z00zz__bignumz00));
			return
				BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string2543z00zz__bignumz00));
		}

	}

#ifdef __cplusplus
}
#endif
#endif
