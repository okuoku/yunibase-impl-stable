/*===========================================================================*/
/*   (Unsafe/rsa.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Unsafe/rsa.scm -indent -o objs/obj_u/Unsafe/rsa.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___RSA_TYPE_DEFINITIONS
#define BGL___RSA_TYPE_DEFINITIONS
#endif													// BGL___RSA_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_symbol2005z00zz__rsaz00 = BUNSPEC;
	extern obj_t BGl_u8vectorzd2ze3listz31zz__srfi4z00(obj_t);
	extern obj_t BGl_gcdbxz00zz__r4_numbers_6_5_fixnumz00(obj_t);
	static obj_t BGl_modzd2inversezd2zz__rsaz00(obj_t, obj_t);
	extern obj_t BGl_makezd2u8vectorzd2zz__srfi4z00(long, uint8_t);
	extern long BGl_modulofxz00zz__r4_numbers_6_5_fixnumz00(long, long);
	BGL_EXPORTED_DECL obj_t BGl_rsazd2encryptzd2stringz00zz__rsaz00(obj_t, obj_t);
	static obj_t BGl_z62PKCS1zd2padzb0zz__rsaz00(obj_t, obj_t, obj_t);
	static obj_t BGl_u8vectorzd2ze3bignumz31zz__rsaz00(obj_t);
	extern obj_t bstring_to_keyword(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__rsaz00 = BUNSPEC;
	extern obj_t bgl_bignum_quotient(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_makezd2rsazd2keyzd2pairzd2zz__rsaz00(obj_t,
		obj_t);
	static obj_t BGl_u8vectorzd2appendzd2zz__rsaz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_PKCS1zd2unpadzd2zz__rsaz00(obj_t);
	static obj_t BGl_gcdzd2extzd2zz__rsaz00(obj_t, obj_t);
	static obj_t BGl_z62publiczd2rsazd2keyz62zz__rsaz00(obj_t, obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__rsaz00(long,
		char *);
	static obj_t BGl_keyword1988z00zz__rsaz00 = BUNSPEC;
	static obj_t BGl_list2004z00zz__rsaz00 = BUNSPEC;
	static obj_t BGl_z62rsazd2encryptzd2stringz62zz__rsaz00(obj_t, obj_t, obj_t);
	static obj_t BGl_subu8vectorz00zz__rsaz00(obj_t, long, long);
	static obj_t BGl_keyword1990z00zz__rsaz00 = BUNSPEC;
	extern obj_t BGl_listzd2ze3u8vectorz31zz__srfi4z00(obj_t);
	static obj_t BGl_toplevelzd2initzd2zz__rsaz00(void);
	BGL_EXPORTED_DECL obj_t BGl_rsazd2keyzd3z01zz__rsaz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_PKCS1zd2padzd2zz__rsaz00(obj_t, obj_t);
	extern obj_t create_struct(obj_t, int);
	static obj_t BGl_randomzd2primezd2zz__rsaz00(obj_t, obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__rsaz00(void);
	static obj_t BGl_exptzd2modze70z35zz__rsaz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zz__rsaz00(void);
	extern long bgl_bignum_to_long(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zz__rsaz00(void);
	extern obj_t bgl_bignum_remainder(obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zz__rsaz00(void);
	extern obj_t BGl_listzd2ze3stringz31zz__r4_strings_6_7z00(obj_t);
	extern obj_t BGl_2zd2zd2zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t bgl_bignum_mul(obj_t, obj_t);
	static obj_t BGl_symbol1984z00zz__rsaz00 = BUNSPEC;
	extern bool_t BGl_2zc3zc3zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_rsazd2decryptzd2stringz00zz__rsaz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_publiczd2rsazd2keyz00zz__rsaz00(obj_t);
	static obj_t BGl_symbol1992z00zz__rsaz00 = BUNSPEC;
	static obj_t BGl_z62rsazd2encryptzd2u8vectorz62zz__rsaz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol1999z00zz__rsaz00 = BUNSPEC;
	static obj_t BGl__makezd2rsazd2keyzd2pairzd2zz__rsaz00(obj_t, obj_t);
	extern obj_t bgl_bignum_add(obj_t, obj_t);
	extern bool_t bgl_bignum_even(obj_t);
	extern bool_t bgl_bignum_odd(obj_t);
	static obj_t BGl_z62rsazd2decryptzd2stringz62zz__rsaz00(obj_t, obj_t, obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	extern obj_t bgl_rand_bignum(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_rsazd2encryptzd2u8vectorz00zz__rsaz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62PKCS1zd2unpadzb0zz__rsaz00(obj_t, obj_t);
	static obj_t BGl_list1987z00zz__rsaz00 = BUNSPEC;
	extern obj_t BGl_stringzd2ze3listz31zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_z62rsazd2keyzd3z63zz__rsaz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62rsazd2decryptzd2u8vectorz62zz__rsaz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_methodzd2initzd2zz__rsaz00(void);
	static obj_t BGl_bignumzd2ze3u8vectorz31zz__rsaz00(obj_t);
	extern obj_t bgl_bignum_expt(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_privatezd2rsazd2keyz00zz__rsaz00(obj_t);
	static obj_t BGl_z62privatezd2rsazd2keyz62zz__rsaz00(obj_t, obj_t);
	extern obj_t BGl_modulobxz00zz__r4_numbers_6_5_fixnumz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_rsazd2decryptzd2u8vectorz00zz__rsaz00(obj_t,
		obj_t);
	extern obj_t bgl_bignum_sub(obj_t, obj_t);
	extern int bgl_bignum_cmp(obj_t, obj_t);
	extern obj_t bgl_long_to_bignum(long);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2001z00zz__rsaz00 = BUNSPEC;
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rsazd2keyzd2pairzd2envz00zz__rsaz00,
		BgL_bgl__makeza7d2rsaza7d2ke2011z00, opt_generic_entry,
		BGl__makezd2rsazd2keyzd2pairzd2zz__rsaz00, BFALSE, -1);
	      DEFINE_STRING(BGl_string1982z00zz__rsaz00,
		BgL_bgl_string1982za700za7za7_2012za7, ".", 1);
	      DEFINE_STRING(BGl_string1983z00zz__rsaz00,
		BgL_bgl_string1983za700za7za7_2013za7, "+", 1);
	      DEFINE_STRING(BGl_string1985z00zz__rsaz00,
		BgL_bgl_string1985za700za7za7_2014za7, "mod-inverse", 11);
	      DEFINE_STRING(BGl_string1986z00zz__rsaz00,
		BgL_bgl_string1986za700za7za7_2015za7,
		"internal error, numbers are not relatively prime", 48);
	      DEFINE_STRING(BGl_string1989z00zz__rsaz00,
		BgL_bgl_string1989za700za7za7_2016za7, "show-trace", 10);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_PKCS1zd2padzd2envz00zz__rsaz00,
		BgL_bgl_za762pkcs1za7d2padza7b2017za7, BGl_z62PKCS1zd2padzb0zz__rsaz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1991z00zz__rsaz00,
		BgL_bgl_string1991za700za7za7_2018za7, "size", 4);
	      DEFINE_STRING(BGl_string1993z00zz__rsaz00,
		BgL_bgl_string1993za700za7za7_2019za7, "make-rsa-key-pair", 17);
	      DEFINE_STRING(BGl_string1994z00zz__rsaz00,
		BgL_bgl_string1994za700za7za7_2020za7, "Illegal keyword argument", 24);
	      DEFINE_STRING(BGl_string1995z00zz__rsaz00,
		BgL_bgl_string1995za700za7za7_2021za7,
		"wrong number of arguments: [0..2] expected, provided", 52);
	      DEFINE_STRING(BGl_string1996z00zz__rsaz00,
		BgL_bgl_string1996za700za7za7_2022za7, "/tmp/bigloo/runtime/Unsafe/rsa.scm",
		34);
	      DEFINE_STRING(BGl_string1997z00zz__rsaz00,
		BgL_bgl_string1997za700za7za7_2023za7, "_make-rsa-key-pair", 18);
	      DEFINE_STRING(BGl_string1998z00zz__rsaz00,
		BgL_bgl_string1998za700za7za7_2024za7, "bint", 4);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_publiczd2rsazd2keyzd2envzd2zz__rsaz00,
		BgL_bgl_za762publicza7d2rsaza72025za7,
		BGl_z62publiczd2rsazd2keyz62zz__rsaz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rsazd2keyzd3zd2envzd3zz__rsaz00,
		BgL_bgl_za762rsaza7d2keyza7d3za72026z00, BGl_z62rsazd2keyzd3z63zz__rsaz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rsazd2decryptzd2u8vectorzd2envzd2zz__rsaz00,
		BgL_bgl_za762rsaza7d2decrypt2027z00,
		BGl_z62rsazd2decryptzd2u8vectorz62zz__rsaz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_privatezd2rsazd2keyzd2envzd2zz__rsaz00,
		BgL_bgl_za762privateza7d2rsa2028z00,
		BGl_z62privatezd2rsazd2keyz62zz__rsaz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2000z00zz__rsaz00,
		BgL_bgl_string2000za700za7za7_2029za7, "rsa-key", 7);
	      DEFINE_STRING(BGl_string2002z00zz__rsaz00,
		BgL_bgl_string2002za700za7za7_2030za7, "PKCS1-pad", 9);
	      DEFINE_STRING(BGl_string2003z00zz__rsaz00,
		BgL_bgl_string2003za700za7za7_2031za7,
		"not enough space is available for proper padding", 48);
	      DEFINE_STRING(BGl_string2006z00zz__rsaz00,
		BgL_bgl_string2006za700za7za7_2032za7, "PKCS1-unpad", 11);
	      DEFINE_STRING(BGl_string2007z00zz__rsaz00,
		BgL_bgl_string2007za700za7za7_2033za7, "improperly padded message", 25);
	      DEFINE_STRING(BGl_string2008z00zz__rsaz00,
		BgL_bgl_string2008za700za7za7_2034za7, "&rsa-encrypt-string", 19);
	      DEFINE_STRING(BGl_string2009z00zz__rsaz00,
		BgL_bgl_string2009za700za7za7_2035za7, "bstring", 7);
	      DEFINE_STRING(BGl_string2010z00zz__rsaz00,
		BgL_bgl_string2010za700za7za7_2036za7, "&rsa-decrypt-string", 19);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rsazd2encryptzd2u8vectorzd2envzd2zz__rsaz00,
		BgL_bgl_za762rsaza7d2encrypt2037z00,
		BGl_z62rsazd2encryptzd2u8vectorz62zz__rsaz00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_PKCS1zd2unpadzd2envz00zz__rsaz00,
		BgL_bgl_za762pkcs1za7d2unpad2038z00, BGl_z62PKCS1zd2unpadzb0zz__rsaz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rsazd2decryptzd2stringzd2envzd2zz__rsaz00,
		BgL_bgl_za762rsaza7d2decrypt2039z00,
		BGl_z62rsazd2decryptzd2stringz62zz__rsaz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rsazd2encryptzd2stringzd2envzd2zz__rsaz00,
		BgL_bgl_za762rsaza7d2encrypt2040z00,
		BGl_z62rsazd2encryptzd2stringz62zz__rsaz00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_symbol2005z00zz__rsaz00));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__rsaz00));
		     ADD_ROOT((void *) (&BGl_keyword1988z00zz__rsaz00));
		     ADD_ROOT((void *) (&BGl_list2004z00zz__rsaz00));
		     ADD_ROOT((void *) (&BGl_keyword1990z00zz__rsaz00));
		     ADD_ROOT((void *) (&BGl_symbol1984z00zz__rsaz00));
		     ADD_ROOT((void *) (&BGl_symbol1992z00zz__rsaz00));
		     ADD_ROOT((void *) (&BGl_symbol1999z00zz__rsaz00));
		     ADD_ROOT((void *) (&BGl_list1987z00zz__rsaz00));
		     ADD_ROOT((void *) (&BGl_symbol2001z00zz__rsaz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__rsaz00(long
		BgL_checksumz00_3151, char *BgL_fromz00_3152)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__rsaz00))
				{
					BGl_requirezd2initializa7ationz75zz__rsaz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__rsaz00();
					BGl_cnstzd2initzd2zz__rsaz00();
					return BGl_toplevelzd2initzd2zz__rsaz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__rsaz00(void)
	{
		{	/* Unsafe/rsa.scm 16 */
			BGl_symbol1984z00zz__rsaz00 =
				bstring_to_symbol(BGl_string1985z00zz__rsaz00);
			BGl_keyword1988z00zz__rsaz00 =
				bstring_to_keyword(BGl_string1989z00zz__rsaz00);
			BGl_keyword1990z00zz__rsaz00 =
				bstring_to_keyword(BGl_string1991z00zz__rsaz00);
			BGl_list1987z00zz__rsaz00 =
				MAKE_YOUNG_PAIR(BGl_keyword1988z00zz__rsaz00,
				MAKE_YOUNG_PAIR(BGl_keyword1990z00zz__rsaz00, BNIL));
			BGl_symbol1992z00zz__rsaz00 =
				bstring_to_symbol(BGl_string1993z00zz__rsaz00);
			BGl_symbol1999z00zz__rsaz00 =
				bstring_to_symbol(BGl_string2000z00zz__rsaz00);
			BGl_symbol2001z00zz__rsaz00 =
				bstring_to_symbol(BGl_string2002z00zz__rsaz00);
			BGl_list2004z00zz__rsaz00 = MAKE_YOUNG_PAIR(BINT(0L), BNIL);
			return (BGl_symbol2005z00zz__rsaz00 =
				bstring_to_symbol(BGl_string2006z00zz__rsaz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__rsaz00(void)
	{
		{	/* Unsafe/rsa.scm 16 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__rsaz00(void)
	{
		{	/* Unsafe/rsa.scm 16 */
			return BUNSPEC;
		}

	}



/* random-prime */
	obj_t BGl_randomzd2primezd2zz__rsaz00(obj_t BgL_startz00_17,
		obj_t BgL_endz00_18, obj_t BgL_showzd2tracezd2_19)
	{
		{	/* Unsafe/rsa.scm 84 */
			{
				long BgL_nz00_1408;

				if (CBOOL(BgL_showzd2tracezd2_19))
					{	/* Unsafe/rsa.scm 96 */
						{	/* Unsafe/rsa.scm 97 */
							obj_t BgL_arg1268z00_1378;

							{	/* Unsafe/rsa.scm 97 */
								obj_t BgL_tmpz00_3173;

								BgL_tmpz00_3173 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_arg1268z00_1378 =
									BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_3173);
							}
							bgl_display_string(BGl_string1982z00zz__rsaz00,
								BgL_arg1268z00_1378);
						}
						{	/* Unsafe/rsa.scm 98 */
							obj_t BgL_arg1272z00_1379;

							{	/* Unsafe/rsa.scm 98 */
								obj_t BgL_tmpz00_3177;

								BgL_tmpz00_3177 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_arg1272z00_1379 =
									BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_3177);
							}
							bgl_flush_output_port(BgL_arg1272z00_1379);
						}
					}
				else
					{	/* Unsafe/rsa.scm 96 */
						BFALSE;
					}
				{	/* Unsafe/rsa.scm 100 */
					obj_t BgL_prodzd2smallzd2primesz00_1380;

					BgL_nz00_1408 = 300L;
					{	/* Unsafe/rsa.scm 87 */
						long BgL_g1042z00_1410;

						BgL_g1042z00_1410 = (BgL_nz00_1408 - 1L);
						{
							long BgL_nz00_2347;
							obj_t BgL_pz00_2348;
							long BgL_iz00_2349;

							BgL_nz00_2347 = BgL_g1042z00_1410;
							BgL_pz00_2348 = (bgl_string_to_bignum("2", 16));
							BgL_iz00_2349 = 3L;
						BgL_loopz00_2346:
							if ((BgL_nz00_2347 == 0L))
								{	/* Unsafe/rsa.scm 89 */
									BgL_prodzd2smallzd2primesz00_1380 = BgL_pz00_2348;
								}
							else
								{	/* Unsafe/rsa.scm 91 */
									bool_t BgL_test2044z00_3184;

									{	/* Unsafe/rsa.scm 91 */
										obj_t BgL_arg1333z00_2353;

										{	/* Unsafe/rsa.scm 91 */
											obj_t BgL_arg1334z00_2354;

											BgL_arg1334z00_2354 = bgl_long_to_bignum(BgL_iz00_2349);
											{	/* Unsafe/rsa.scm 91 */
												obj_t BgL_list1335z00_2356;

												{	/* Unsafe/rsa.scm 91 */
													obj_t BgL_arg1336z00_2357;

													BgL_arg1336z00_2357 =
														MAKE_YOUNG_PAIR(BgL_pz00_2348, BNIL);
													BgL_list1335z00_2356 =
														MAKE_YOUNG_PAIR(BgL_arg1334z00_2354,
														BgL_arg1336z00_2357);
												}
												BgL_arg1333z00_2353 =
													BGl_gcdbxz00zz__r4_numbers_6_5_fixnumz00
													(BgL_list1335z00_2356);
											}
										}
										BgL_test2044z00_3184 =
											(
											(long) (bgl_bignum_cmp((bgl_string_to_bignum("1", 16)),
													BgL_arg1333z00_2353)) == 0L);
									}
									if (BgL_test2044z00_3184)
										{
											long BgL_iz00_3197;
											obj_t BgL_pz00_3194;
											long BgL_nz00_3192;

											BgL_nz00_3192 = (BgL_nz00_2347 - 1L);
											BgL_pz00_3194 =
												bgl_bignum_mul(BgL_pz00_2348,
												bgl_long_to_bignum(BgL_iz00_2349));
											BgL_iz00_3197 = (BgL_iz00_2349 + 2L);
											BgL_iz00_2349 = BgL_iz00_3197;
											BgL_pz00_2348 = BgL_pz00_3194;
											BgL_nz00_2347 = BgL_nz00_3192;
											goto BgL_loopz00_2346;
										}
									else
										{
											long BgL_iz00_3199;

											BgL_iz00_3199 = (BgL_iz00_2349 + 2L);
											BgL_iz00_2349 = BgL_iz00_3199;
											goto BgL_loopz00_2346;
										}
								}
						}
					}
					{
						obj_t BgL_nz00_1399;

						{
							long BgL_iz00_1383;

							BgL_iz00_1383 = 1L;
						BgL_zc3z04anonymousza31273ze3z87_1384:
							if (CBOOL(BgL_showzd2tracezd2_19))
								{	/* Unsafe/rsa.scm 107 */
									{	/* Unsafe/rsa.scm 108 */
										obj_t BgL_arg1284z00_1385;

										{	/* Unsafe/rsa.scm 108 */
											obj_t BgL_tmpz00_3203;

											BgL_tmpz00_3203 = BGL_CURRENT_DYNAMIC_ENV();
											BgL_arg1284z00_1385 =
												BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_3203);
										}
										bgl_display_string(BGl_string1983z00zz__rsaz00,
											BgL_arg1284z00_1385);
									}
									{	/* Unsafe/rsa.scm 109 */
										obj_t BgL_arg1304z00_1386;

										{	/* Unsafe/rsa.scm 109 */
											obj_t BgL_tmpz00_3207;

											BgL_tmpz00_3207 = BGL_CURRENT_DYNAMIC_ENV();
											BgL_arg1304z00_1386 =
												BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_3207);
										}
										bgl_flush_output_port(BgL_arg1304z00_1386);
									}
								}
							else
								{	/* Unsafe/rsa.scm 107 */
									BFALSE;
								}
							{	/* Unsafe/rsa.scm 110 */
								obj_t BgL_xz00_1387;

								{	/* Unsafe/rsa.scm 110 */
									obj_t BgL_arg1310z00_1396;

									{	/* Unsafe/rsa.scm 110 */
										obj_t BgL_arg1311z00_1397;

										BgL_arg1311z00_1397 =
											bgl_bignum_sub(BgL_endz00_18, BgL_startz00_17);
										if ((bgl_bignum_to_long(BgL_arg1311z00_1397) == 0L))
											{	/* Unsafe/rsa.scm 110 */
												BgL_arg1310z00_1396 = (bgl_string_to_bignum("0", 16));
											}
										else
											{	/* Unsafe/rsa.scm 110 */
												BgL_arg1310z00_1396 =
													bgl_rand_bignum(BgL_arg1311z00_1397);
											}
									}
									BgL_xz00_1387 =
										bgl_bignum_add(BgL_startz00_17, BgL_arg1310z00_1396);
								}
								{	/* Unsafe/rsa.scm 110 */
									obj_t BgL_nz00_1388;

									if (bgl_bignum_odd(BgL_xz00_1387))
										{	/* Unsafe/rsa.scm 111 */
											BgL_nz00_1388 = BgL_xz00_1387;
										}
									else
										{	/* Unsafe/rsa.scm 111 */
											BgL_nz00_1388 =
												bgl_bignum_add(BgL_xz00_1387, (bgl_string_to_bignum("1",
														16)));
										}
									{	/* Unsafe/rsa.scm 111 */

										{	/* Unsafe/rsa.scm 112 */
											bool_t BgL_test2048z00_3220;

											if (
												((long) (bgl_bignum_cmp(BgL_nz00_1388,
															BgL_endz00_18)) >= 0L))
												{	/* Unsafe/rsa.scm 112 */
													BgL_test2048z00_3220 = ((bool_t) 1);
												}
											else
												{	/* Unsafe/rsa.scm 112 */
													bool_t BgL_test2050z00_3225;

													BgL_nz00_1399 = BgL_nz00_1388;
													{	/* Unsafe/rsa.scm 103 */
														bool_t BgL_test2051z00_3226;

														{	/* Unsafe/rsa.scm 103 */
															obj_t BgL_arg1316z00_1404;

															{	/* Unsafe/rsa.scm 103 */
																obj_t BgL_list1317z00_1405;

																{	/* Unsafe/rsa.scm 103 */
																	obj_t BgL_arg1318z00_1406;

																	BgL_arg1318z00_1406 =
																		MAKE_YOUNG_PAIR
																		(BgL_prodzd2smallzd2primesz00_1380, BNIL);
																	BgL_list1317z00_1405 =
																		MAKE_YOUNG_PAIR(BgL_nz00_1399,
																		BgL_arg1318z00_1406);
																}
																BgL_arg1316z00_1404 =
																	BGl_gcdbxz00zz__r4_numbers_6_5_fixnumz00
																	(BgL_list1317z00_1405);
															}
															BgL_test2051z00_3226 =
																(
																(long) (bgl_bignum_cmp((bgl_string_to_bignum
																			("1", 16)), BgL_arg1316z00_1404)) == 0L);
														}
														if (BgL_test2051z00_3226)
															{	/* Unsafe/rsa.scm 103 */
																BgL_test2050z00_3225 =
																	(
																	(long) (bgl_bignum_cmp((bgl_string_to_bignum
																				("1", 16)),
																			BGl_exptzd2modze70z35zz__rsaz00(
																				(bgl_string_to_bignum("2", 16)),
																				bgl_bignum_sub(BgL_nz00_1399,
																					(bgl_string_to_bignum("1", 16))),
																				BgL_nz00_1399))) == 0L);
															}
														else
															{	/* Unsafe/rsa.scm 103 */
																BgL_test2050z00_3225 = ((bool_t) 0);
															}
													}
													if (BgL_test2050z00_3225)
														{	/* Unsafe/rsa.scm 112 */
															BgL_test2048z00_3220 = ((bool_t) 0);
														}
													else
														{	/* Unsafe/rsa.scm 112 */
															BgL_test2048z00_3220 = ((bool_t) 1);
														}
												}
											if (BgL_test2048z00_3220)
												{
													long BgL_iz00_3238;

													BgL_iz00_3238 = (BgL_iz00_1383 + 1L);
													BgL_iz00_1383 = BgL_iz00_3238;
													goto BgL_zc3z04anonymousza31273ze3z87_1384;
												}
											else
												{	/* Unsafe/rsa.scm 112 */
													return BgL_nz00_1388;
												}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* gcd-ext */
	obj_t BGl_gcdzd2extzd2zz__rsaz00(obj_t BgL_xz00_20, obj_t BgL_yz00_21)
	{
		{	/* Unsafe/rsa.scm 119 */
			{
				obj_t BgL_xz00_2459;
				obj_t BgL_yz00_2460;
				obj_t BgL_u1z00_2461;
				obj_t BgL_u2z00_2462;
				obj_t BgL_v1z00_2463;
				obj_t BgL_v2z00_2464;

				BgL_xz00_2459 = BgL_xz00_20;
				BgL_yz00_2460 = BgL_yz00_21;
				BgL_u1z00_2461 = (bgl_string_to_bignum("1", 16));
				BgL_u2z00_2462 = (bgl_string_to_bignum("0", 16));
				BgL_v1z00_2463 = (bgl_string_to_bignum("0", 16));
				BgL_v2z00_2464 = (bgl_string_to_bignum("1", 16));
			BgL_loopz00_2458:
				if (BXZERO(BgL_yz00_2460))
					{	/* Unsafe/rsa.scm 127 */
						obj_t BgL_list1339z00_2476;

						{	/* Unsafe/rsa.scm 127 */
							obj_t BgL_arg1340z00_2477;

							{	/* Unsafe/rsa.scm 127 */
								obj_t BgL_arg1341z00_2478;

								BgL_arg1341z00_2478 = MAKE_YOUNG_PAIR(BgL_v1z00_2463, BNIL);
								BgL_arg1340z00_2477 =
									MAKE_YOUNG_PAIR(BgL_u1z00_2461, BgL_arg1341z00_2478);
							}
							BgL_list1339z00_2476 =
								MAKE_YOUNG_PAIR(BgL_xz00_2459, BgL_arg1340z00_2477);
						}
						return BgL_list1339z00_2476;
					}
				else
					{	/* Unsafe/rsa.scm 128 */
						obj_t BgL_qz00_2479;
						obj_t BgL_rz00_2480;

						BgL_qz00_2479 = bgl_bignum_quotient(BgL_xz00_2459, BgL_yz00_2460);
						BgL_rz00_2480 = bgl_bignum_remainder(BgL_xz00_2459, BgL_yz00_2460);
						{
							obj_t BgL_v2z00_3254;
							obj_t BgL_v1z00_3253;
							obj_t BgL_u2z00_3250;
							obj_t BgL_u1z00_3249;
							obj_t BgL_yz00_3248;
							obj_t BgL_xz00_3247;

							BgL_xz00_3247 = BgL_yz00_2460;
							BgL_yz00_3248 = BgL_rz00_2480;
							BgL_u1z00_3249 = BgL_u2z00_2462;
							BgL_u2z00_3250 =
								bgl_bignum_sub(BgL_u1z00_2461,
								bgl_bignum_mul(BgL_qz00_2479, BgL_u2z00_2462));
							BgL_v1z00_3253 = BgL_v2z00_2464;
							BgL_v2z00_3254 =
								bgl_bignum_sub(BgL_v1z00_2463,
								bgl_bignum_mul(BgL_qz00_2479, BgL_v2z00_2464));
							BgL_v2z00_2464 = BgL_v2z00_3254;
							BgL_v1z00_2463 = BgL_v1z00_3253;
							BgL_u2z00_2462 = BgL_u2z00_3250;
							BgL_u1z00_2461 = BgL_u1z00_3249;
							BgL_yz00_2460 = BgL_yz00_3248;
							BgL_xz00_2459 = BgL_xz00_3247;
							goto BgL_loopz00_2458;
						}
					}
			}
		}

	}



/* mod-inverse */
	obj_t BGl_modzd2inversezd2zz__rsaz00(obj_t BgL_xz00_22, obj_t BgL_bz00_23)
	{
		{	/* Unsafe/rsa.scm 135 */
			{	/* Unsafe/rsa.scm 136 */
				obj_t BgL_x1z00_1452;

				BgL_x1z00_1452 =
					BGl_modulobxz00zz__r4_numbers_6_5_fixnumz00(BgL_xz00_22, BgL_bz00_23);
				{	/* Unsafe/rsa.scm 136 */
					obj_t BgL_gz00_1453;

					BgL_gz00_1453 =
						BGl_gcdzd2extzd2zz__rsaz00(BgL_x1z00_1452, BgL_bz00_23);
					{	/* Unsafe/rsa.scm 137 */

						{	/* Unsafe/rsa.scm 138 */
							bool_t BgL_test2053z00_3259;

							{	/* Unsafe/rsa.scm 138 */
								obj_t BgL_n1z00_2500;

								BgL_n1z00_2500 = CAR(((obj_t) BgL_gz00_1453));
								BgL_test2053z00_3259 =
									(
									(long) (bgl_bignum_cmp(BgL_n1z00_2500,
											(bgl_string_to_bignum("1", 16)))) == 0L);
							}
							if (BgL_test2053z00_3259)
								{	/* Unsafe/rsa.scm 142 */
									obj_t BgL_auxz00_3265;

									{	/* Unsafe/rsa.scm 142 */
										obj_t BgL_pairz00_2507;

										BgL_pairz00_2507 = CDR(((obj_t) BgL_gz00_1453));
										BgL_auxz00_3265 = CAR(BgL_pairz00_2507);
									}
									BGL_TAIL return
										BGl_modulobxz00zz__r4_numbers_6_5_fixnumz00(BgL_auxz00_3265,
										BgL_bz00_23);
								}
							else
								{	/* Unsafe/rsa.scm 141 */
									obj_t BgL_arg1350z00_1457;

									BgL_arg1350z00_1457 =
										MAKE_YOUNG_PAIR(BgL_xz00_22, BgL_bz00_23);
									return
										BGl_errorz00zz__errorz00(BGl_symbol1984z00zz__rsaz00,
										BGl_string1986z00zz__rsaz00, BgL_arg1350z00_1457);
								}
						}
					}
				}
			}
		}

	}



/* _make-rsa-key-pair */
	obj_t BGl__makezd2rsazd2keyzd2pairzd2zz__rsaz00(obj_t BgL_env1148z00_27,
		obj_t BgL_opt1147z00_26)
	{
		{	/* Unsafe/rsa.scm 147 */
			{	/* Unsafe/rsa.scm 147 */

				{	/* Unsafe/rsa.scm 147 */
					obj_t BgL_showzd2tracezd2_1463;

					BgL_showzd2tracezd2_1463 = BFALSE;
					{	/* Unsafe/rsa.scm 147 */
						obj_t BgL_siza7eza7_1464;

						BgL_siza7eza7_1464 = BINT(1024L);
						{	/* Unsafe/rsa.scm 147 */

							{
								long BgL_iz00_1465;

								BgL_iz00_1465 = 0L;
							BgL_check1151z00_1466:
								if ((BgL_iz00_1465 == VECTOR_LENGTH(BgL_opt1147z00_26)))
									{	/* Unsafe/rsa.scm 147 */
										BNIL;
									}
								else
									{	/* Unsafe/rsa.scm 147 */
										bool_t BgL_test2055z00_3276;

										{	/* Unsafe/rsa.scm 147 */
											obj_t BgL_arg1358z00_1472;

											BgL_arg1358z00_1472 =
												VECTOR_REF(BgL_opt1147z00_26, BgL_iz00_1465);
											BgL_test2055z00_3276 =
												CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
												(BgL_arg1358z00_1472, BGl_list1987z00zz__rsaz00));
										}
										if (BgL_test2055z00_3276)
											{
												long BgL_iz00_3280;

												BgL_iz00_3280 = (BgL_iz00_1465 + 2L);
												BgL_iz00_1465 = BgL_iz00_3280;
												goto BgL_check1151z00_1466;
											}
										else
											{	/* Unsafe/rsa.scm 147 */
												obj_t BgL_arg1357z00_1471;

												BgL_arg1357z00_1471 =
													VECTOR_REF(BgL_opt1147z00_26, BgL_iz00_1465);
												BGl_errorz00zz__errorz00(BGl_symbol1992z00zz__rsaz00,
													BGl_string1994z00zz__rsaz00, BgL_arg1357z00_1471);
											}
									}
							}
							{	/* Unsafe/rsa.scm 147 */
								obj_t BgL_index1153z00_1473;

								{
									long BgL_iz00_2524;

									BgL_iz00_2524 = 0L;
								BgL_search1150z00_2523:
									if ((BgL_iz00_2524 == VECTOR_LENGTH(BgL_opt1147z00_26)))
										{	/* Unsafe/rsa.scm 147 */
											BgL_index1153z00_1473 = BINT(-1L);
										}
									else
										{	/* Unsafe/rsa.scm 147 */
											if (
												(BgL_iz00_2524 ==
													(VECTOR_LENGTH(BgL_opt1147z00_26) - 1L)))
												{	/* Unsafe/rsa.scm 147 */
													BgL_index1153z00_1473 =
														BGl_errorz00zz__errorz00
														(BGl_symbol1992z00zz__rsaz00,
														BGl_string1995z00zz__rsaz00,
														BINT(VECTOR_LENGTH(BgL_opt1147z00_26)));
												}
											else
												{	/* Unsafe/rsa.scm 147 */
													obj_t BgL_vz00_2534;

													BgL_vz00_2534 =
														VECTOR_REF(BgL_opt1147z00_26, BgL_iz00_2524);
													if ((BgL_vz00_2534 == BGl_keyword1988z00zz__rsaz00))
														{	/* Unsafe/rsa.scm 147 */
															BgL_index1153z00_1473 =
																BINT((BgL_iz00_2524 + 1L));
														}
													else
														{
															long BgL_iz00_3300;

															BgL_iz00_3300 = (BgL_iz00_2524 + 2L);
															BgL_iz00_2524 = BgL_iz00_3300;
															goto BgL_search1150z00_2523;
														}
												}
										}
								}
								{	/* Unsafe/rsa.scm 147 */
									bool_t BgL_test2059z00_3302;

									{	/* Unsafe/rsa.scm 147 */
										long BgL_n1z00_2538;

										{	/* Unsafe/rsa.scm 147 */
											obj_t BgL_tmpz00_3303;

											if (INTEGERP(BgL_index1153z00_1473))
												{	/* Unsafe/rsa.scm 147 */
													BgL_tmpz00_3303 = BgL_index1153z00_1473;
												}
											else
												{
													obj_t BgL_auxz00_3306;

													BgL_auxz00_3306 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string1996z00zz__rsaz00, BINT(5113L),
														BGl_string1997z00zz__rsaz00,
														BGl_string1998z00zz__rsaz00, BgL_index1153z00_1473);
													FAILURE(BgL_auxz00_3306, BFALSE, BFALSE);
												}
											BgL_n1z00_2538 = (long) CINT(BgL_tmpz00_3303);
										}
										BgL_test2059z00_3302 = (BgL_n1z00_2538 >= 0L);
									}
									if (BgL_test2059z00_3302)
										{
											long BgL_auxz00_3312;

											{	/* Unsafe/rsa.scm 147 */
												obj_t BgL_tmpz00_3313;

												if (INTEGERP(BgL_index1153z00_1473))
													{	/* Unsafe/rsa.scm 147 */
														BgL_tmpz00_3313 = BgL_index1153z00_1473;
													}
												else
													{
														obj_t BgL_auxz00_3316;

														BgL_auxz00_3316 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string1996z00zz__rsaz00, BINT(5113L),
															BGl_string1997z00zz__rsaz00,
															BGl_string1998z00zz__rsaz00,
															BgL_index1153z00_1473);
														FAILURE(BgL_auxz00_3316, BFALSE, BFALSE);
													}
												BgL_auxz00_3312 = (long) CINT(BgL_tmpz00_3313);
											}
											BgL_showzd2tracezd2_1463 =
												VECTOR_REF(BgL_opt1147z00_26, BgL_auxz00_3312);
										}
									else
										{	/* Unsafe/rsa.scm 147 */
											BFALSE;
										}
								}
							}
							{	/* Unsafe/rsa.scm 147 */
								obj_t BgL_index1154z00_1475;

								{
									long BgL_iz00_2540;

									BgL_iz00_2540 = 0L;
								BgL_search1150z00_2539:
									if ((BgL_iz00_2540 == VECTOR_LENGTH(BgL_opt1147z00_26)))
										{	/* Unsafe/rsa.scm 147 */
											BgL_index1154z00_1475 = BINT(-1L);
										}
									else
										{	/* Unsafe/rsa.scm 147 */
											if (
												(BgL_iz00_2540 ==
													(VECTOR_LENGTH(BgL_opt1147z00_26) - 1L)))
												{	/* Unsafe/rsa.scm 147 */
													BgL_index1154z00_1475 =
														BGl_errorz00zz__errorz00
														(BGl_symbol1992z00zz__rsaz00,
														BGl_string1995z00zz__rsaz00,
														BINT(VECTOR_LENGTH(BgL_opt1147z00_26)));
												}
											else
												{	/* Unsafe/rsa.scm 147 */
													obj_t BgL_vz00_2550;

													BgL_vz00_2550 =
														VECTOR_REF(BgL_opt1147z00_26, BgL_iz00_2540);
													if ((BgL_vz00_2550 == BGl_keyword1990z00zz__rsaz00))
														{	/* Unsafe/rsa.scm 147 */
															BgL_index1154z00_1475 =
																BINT((BgL_iz00_2540 + 1L));
														}
													else
														{
															long BgL_iz00_3338;

															BgL_iz00_3338 = (BgL_iz00_2540 + 2L);
															BgL_iz00_2540 = BgL_iz00_3338;
															goto BgL_search1150z00_2539;
														}
												}
										}
								}
								{	/* Unsafe/rsa.scm 147 */
									bool_t BgL_test2065z00_3340;

									{	/* Unsafe/rsa.scm 147 */
										long BgL_n1z00_2554;

										{	/* Unsafe/rsa.scm 147 */
											obj_t BgL_tmpz00_3341;

											if (INTEGERP(BgL_index1154z00_1475))
												{	/* Unsafe/rsa.scm 147 */
													BgL_tmpz00_3341 = BgL_index1154z00_1475;
												}
											else
												{
													obj_t BgL_auxz00_3344;

													BgL_auxz00_3344 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string1996z00zz__rsaz00, BINT(5113L),
														BGl_string1997z00zz__rsaz00,
														BGl_string1998z00zz__rsaz00, BgL_index1154z00_1475);
													FAILURE(BgL_auxz00_3344, BFALSE, BFALSE);
												}
											BgL_n1z00_2554 = (long) CINT(BgL_tmpz00_3341);
										}
										BgL_test2065z00_3340 = (BgL_n1z00_2554 >= 0L);
									}
									if (BgL_test2065z00_3340)
										{
											long BgL_auxz00_3350;

											{	/* Unsafe/rsa.scm 147 */
												obj_t BgL_tmpz00_3351;

												if (INTEGERP(BgL_index1154z00_1475))
													{	/* Unsafe/rsa.scm 147 */
														BgL_tmpz00_3351 = BgL_index1154z00_1475;
													}
												else
													{
														obj_t BgL_auxz00_3354;

														BgL_auxz00_3354 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string1996z00zz__rsaz00, BINT(5113L),
															BGl_string1997z00zz__rsaz00,
															BGl_string1998z00zz__rsaz00,
															BgL_index1154z00_1475);
														FAILURE(BgL_auxz00_3354, BFALSE, BFALSE);
													}
												BgL_auxz00_3350 = (long) CINT(BgL_tmpz00_3351);
											}
											BgL_siza7eza7_1464 =
												VECTOR_REF(BgL_opt1147z00_26, BgL_auxz00_3350);
										}
									else
										{	/* Unsafe/rsa.scm 147 */
											BFALSE;
										}
								}
							}
							{	/* Unsafe/rsa.scm 147 */
								obj_t BgL_showzd2tracezd2_1477;

								BgL_showzd2tracezd2_1477 = BgL_showzd2tracezd2_1463;
								return
									BGl_makezd2rsazd2keyzd2pairzd2zz__rsaz00
									(BgL_showzd2tracezd2_1477, BgL_siza7eza7_1464);
							}
						}
					}
				}
			}
		}

	}



/* make-rsa-key-pair */
	BGL_EXPORTED_DEF obj_t BGl_makezd2rsazd2keyzd2pairzd2zz__rsaz00(obj_t
		BgL_showzd2tracezd2_24, obj_t BgL_siza7eza7_25)
	{
		{	/* Unsafe/rsa.scm 147 */
			{	/* Unsafe/rsa.scm 148 */
				long BgL_siza7ezd2pz75_1486;

				{	/* Unsafe/rsa.scm 148 */
					long BgL_tmpz00_3361;

					BgL_tmpz00_3361 = (long) CINT(BgL_siza7eza7_25);
					BgL_siza7ezd2pz75_1486 = (BgL_tmpz00_3361 / 2L);
				}
				{	/* Unsafe/rsa.scm 148 */
					obj_t BgL_startzd2pzd2_1487;

					BgL_startzd2pzd2_1487 =
						bgl_bignum_expt((bgl_string_to_bignum("2", 16)),
						bgl_long_to_bignum(BgL_siza7ezd2pz75_1486));
					{	/* Unsafe/rsa.scm 149 */
						obj_t BgL_endzd2pzd2_1488;

						BgL_endzd2pzd2_1488 =
							bgl_bignum_mul(BgL_startzd2pzd2_1487, (bgl_string_to_bignum("2",
									16)));
						{	/* Unsafe/rsa.scm 150 */
							obj_t BgL_pz00_1489;

							BgL_pz00_1489 =
								BGl_randomzd2primezd2zz__rsaz00(BgL_startzd2pzd2_1487,
								BgL_endzd2pzd2_1488, BgL_showzd2tracezd2_24);
							{	/* Unsafe/rsa.scm 151 */
								obj_t BgL_startzd2nzd2_1490;

								BgL_startzd2nzd2_1490 =
									bgl_bignum_expt((bgl_string_to_bignum("2", 16)),
									bgl_long_to_bignum((long) CINT(BgL_siza7eza7_25)));
								{	/* Unsafe/rsa.scm 152 */
									obj_t BgL_endzd2nzd2_1491;

									BgL_endzd2nzd2_1491 =
										bgl_bignum_mul(BgL_startzd2nzd2_1490,
										(bgl_string_to_bignum("2", 16)));
									{	/* Unsafe/rsa.scm 153 */
										obj_t BgL_startzd2qzd2_1492;

										BgL_startzd2qzd2_1492 =
											bgl_bignum_add(bgl_bignum_quotient(bgl_bignum_sub
												(BgL_startzd2nzd2_1490, (bgl_string_to_bignum("1",
															16))), BgL_pz00_1489), (bgl_string_to_bignum("1",
													16)));
										{	/* Unsafe/rsa.scm 154 */
											obj_t BgL_endzd2qzd2_1493;

											BgL_endzd2qzd2_1493 =
												bgl_bignum_quotient(BgL_endzd2nzd2_1491, BgL_pz00_1489);
											{	/* Unsafe/rsa.scm 155 */

												{

												BgL_zc3z04anonymousza31367ze3z87_1495:
													{	/* Unsafe/rsa.scm 157 */
														obj_t BgL_qz00_1496;

														BgL_qz00_1496 =
															BGl_randomzd2primezd2zz__rsaz00
															(BgL_startzd2qzd2_1492, BgL_endzd2qzd2_1493,
															BgL_showzd2tracezd2_24);
														{	/* Unsafe/rsa.scm 158 */
															bool_t BgL_test2068z00_3377;

															{	/* Unsafe/rsa.scm 158 */
																obj_t BgL_arg1392z00_1526;

																{	/* Unsafe/rsa.scm 158 */
																	obj_t BgL_list1393z00_1527;

																	{	/* Unsafe/rsa.scm 158 */
																		obj_t BgL_arg1394z00_1528;

																		BgL_arg1394z00_1528 =
																			MAKE_YOUNG_PAIR(BgL_qz00_1496, BNIL);
																		BgL_list1393z00_1527 =
																			MAKE_YOUNG_PAIR(BgL_pz00_1489,
																			BgL_arg1394z00_1528);
																	}
																	BgL_arg1392z00_1526 =
																		BGl_gcdbxz00zz__r4_numbers_6_5_fixnumz00
																		(BgL_list1393z00_1527);
																}
																BgL_test2068z00_3377 =
																	(
																	(long) (bgl_bignum_cmp(BgL_arg1392z00_1526,
																			(bgl_string_to_bignum("1", 16)))) == 0L);
															}
															if (BgL_test2068z00_3377)
																{	/* Unsafe/rsa.scm 160 */
																	obj_t BgL_nz00_1501;

																	BgL_nz00_1501 =
																		bgl_bignum_mul(BgL_pz00_1489,
																		BgL_qz00_1496);
																	{	/* Unsafe/rsa.scm 160 */
																		obj_t BgL_pzd21zd2_1502;

																		BgL_pzd21zd2_1502 =
																			bgl_bignum_sub(BgL_pz00_1489,
																			(bgl_string_to_bignum("1", 16)));
																		{	/* Unsafe/rsa.scm 161 */
																			obj_t BgL_qzd21zd2_1503;

																			BgL_qzd21zd2_1503 =
																				bgl_bignum_sub(BgL_qz00_1496,
																				(bgl_string_to_bignum("1", 16)));
																			{	/* Unsafe/rsa.scm 162 */
																				obj_t BgL_phiz00_1504;

																				{	/* Unsafe/rsa.scm 163 */
																					obj_t BgL_arg1388z00_1522;
																					obj_t BgL_arg1389z00_1523;

																					BgL_arg1388z00_1522 =
																						bgl_bignum_mul(BgL_pzd21zd2_1502,
																						BgL_qzd21zd2_1503);
																					{	/* Unsafe/rsa.scm 163 */
																						obj_t BgL_list1390z00_1524;

																						{	/* Unsafe/rsa.scm 163 */
																							obj_t BgL_arg1391z00_1525;

																							BgL_arg1391z00_1525 =
																								MAKE_YOUNG_PAIR
																								(BgL_qzd21zd2_1503, BNIL);
																							BgL_list1390z00_1524 =
																								MAKE_YOUNG_PAIR
																								(BgL_pzd21zd2_1502,
																								BgL_arg1391z00_1525);
																						}
																						BgL_arg1389z00_1523 =
																							BGl_gcdbxz00zz__r4_numbers_6_5_fixnumz00
																							(BgL_list1390z00_1524);
																					}
																					BgL_phiz00_1504 =
																						bgl_bignum_quotient
																						(BgL_arg1388z00_1522,
																						BgL_arg1389z00_1523);
																				}
																				{	/* Unsafe/rsa.scm 163 */
																					obj_t BgL_ez00_1505;

																					{
																						obj_t BgL_ez00_2595;

																						BgL_ez00_2595 =
																							(bgl_string_to_bignum("10001",
																								16));
																					BgL_loopz00_2594:
																						{	/* Unsafe/rsa.scm 165 */
																							bool_t BgL_test2069z00_3392;

																							{	/* Unsafe/rsa.scm 165 */
																								obj_t BgL_arg1383z00_2597;

																								{	/* Unsafe/rsa.scm 165 */
																									obj_t BgL_list1384z00_2598;

																									{	/* Unsafe/rsa.scm 165 */
																										obj_t BgL_arg1387z00_2599;

																										BgL_arg1387z00_2599 =
																											MAKE_YOUNG_PAIR
																											(BgL_phiz00_1504, BNIL);
																										BgL_list1384z00_2598 =
																											MAKE_YOUNG_PAIR
																											(BgL_ez00_2595,
																											BgL_arg1387z00_2599);
																									}
																									BgL_arg1383z00_2597 =
																										BGl_gcdbxz00zz__r4_numbers_6_5_fixnumz00
																										(BgL_list1384z00_2598);
																								}
																								BgL_test2069z00_3392 =
																									(
																									(long) (bgl_bignum_cmp(
																											(bgl_string_to_bignum("1",
																													16)),
																											BgL_arg1383z00_2597)) ==
																									0L);
																							}
																							if (BgL_test2069z00_3392)
																								{	/* Unsafe/rsa.scm 165 */
																									BgL_ez00_1505 = BgL_ez00_2595;
																								}
																							else
																								{
																									obj_t BgL_ez00_3399;

																									BgL_ez00_3399 =
																										bgl_bignum_add
																										(BgL_ez00_2595,
																										(bgl_string_to_bignum("2",
																												16)));
																									BgL_ez00_2595 = BgL_ez00_3399;
																									goto BgL_loopz00_2594;
																								}
																						}
																					}
																					{	/* Unsafe/rsa.scm 164 */
																						obj_t BgL_dz00_1506;

																						BgL_dz00_1506 =
																							BGl_modzd2inversezd2zz__rsaz00
																							(BgL_ez00_1505, BgL_phiz00_1504);
																						{	/* Unsafe/rsa.scm 168 */

																							if (CBOOL(BgL_showzd2tracezd2_24))
																								{	/* Unsafe/rsa.scm 169 */
																									obj_t BgL_arg1372z00_1507;

																									{	/* Unsafe/rsa.scm 169 */
																										obj_t BgL_tmpz00_3404;

																										BgL_tmpz00_3404 =
																											BGL_CURRENT_DYNAMIC_ENV();
																										BgL_arg1372z00_1507 =
																											BGL_ENV_CURRENT_OUTPUT_PORT
																											(BgL_tmpz00_3404);
																									}
																									bgl_display_char(((unsigned
																												char) 10),
																										BgL_arg1372z00_1507);
																								}
																							else
																								{	/* Unsafe/rsa.scm 169 */
																									BFALSE;
																								}
																							{	/* Unsafe/rsa.scm 171 */
																								obj_t BgL_arg1373z00_1508;
																								obj_t BgL_arg1375z00_1509;

																								{	/* Unsafe/rsa.scm 79 */
																									obj_t BgL_newz00_2609;

																									BgL_newz00_2609 =
																										create_struct
																										(BGl_symbol1999z00zz__rsaz00,
																										(int) (3L));
																									{	/* Unsafe/rsa.scm 79 */
																										int BgL_tmpz00_3410;

																										BgL_tmpz00_3410 =
																											(int) (2L);
																										STRUCT_SET(BgL_newz00_2609,
																											BgL_tmpz00_3410,
																											BgL_ez00_1505);
																									}
																									{	/* Unsafe/rsa.scm 79 */
																										int BgL_tmpz00_3413;

																										BgL_tmpz00_3413 =
																											(int) (1L);
																										STRUCT_SET(BgL_newz00_2609,
																											BgL_tmpz00_3413,
																											BgL_nz00_1501);
																									}
																									{	/* Unsafe/rsa.scm 79 */
																										int BgL_tmpz00_3416;

																										BgL_tmpz00_3416 =
																											(int) (0L);
																										STRUCT_SET(BgL_newz00_2609,
																											BgL_tmpz00_3416,
																											BgL_siza7eza7_25);
																									}
																									BgL_arg1373z00_1508 =
																										BgL_newz00_2609;
																								}
																								{	/* Unsafe/rsa.scm 79 */
																									obj_t BgL_newz00_2613;

																									BgL_newz00_2613 =
																										create_struct
																										(BGl_symbol1999z00zz__rsaz00,
																										(int) (3L));
																									{	/* Unsafe/rsa.scm 79 */
																										int BgL_tmpz00_3421;

																										BgL_tmpz00_3421 =
																											(int) (2L);
																										STRUCT_SET(BgL_newz00_2613,
																											BgL_tmpz00_3421,
																											BgL_dz00_1506);
																									}
																									{	/* Unsafe/rsa.scm 79 */
																										int BgL_tmpz00_3424;

																										BgL_tmpz00_3424 =
																											(int) (1L);
																										STRUCT_SET(BgL_newz00_2613,
																											BgL_tmpz00_3424,
																											BgL_nz00_1501);
																									}
																									{	/* Unsafe/rsa.scm 79 */
																										int BgL_tmpz00_3427;

																										BgL_tmpz00_3427 =
																											(int) (0L);
																										STRUCT_SET(BgL_newz00_2613,
																											BgL_tmpz00_3427,
																											BgL_siza7eza7_25);
																									}
																									BgL_arg1375z00_1509 =
																										BgL_newz00_2613;
																								}
																								return
																									MAKE_YOUNG_PAIR
																									(BgL_arg1373z00_1508,
																									BgL_arg1375z00_1509);
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															else
																{	/* Unsafe/rsa.scm 158 */
																	goto BgL_zc3z04anonymousza31367ze3z87_1495;
																}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* public-rsa-key */
	BGL_EXPORTED_DEF obj_t BGl_publiczd2rsazd2keyz00zz__rsaz00(obj_t
		BgL_rsazd2keyzd2pairz00_28)
	{
		{	/* Unsafe/rsa.scm 177 */
			return CAR(((obj_t) BgL_rsazd2keyzd2pairz00_28));
		}

	}



/* &public-rsa-key */
	obj_t BGl_z62publiczd2rsazd2keyz62zz__rsaz00(obj_t BgL_envz00_3096,
		obj_t BgL_rsazd2keyzd2pairz00_3097)
	{
		{	/* Unsafe/rsa.scm 177 */
			return BGl_publiczd2rsazd2keyz00zz__rsaz00(BgL_rsazd2keyzd2pairz00_3097);
		}

	}



/* private-rsa-key */
	BGL_EXPORTED_DEF obj_t BGl_privatezd2rsazd2keyz00zz__rsaz00(obj_t
		BgL_rsazd2keyzd2pairz00_29)
	{
		{	/* Unsafe/rsa.scm 183 */
			return CDR(((obj_t) BgL_rsazd2keyzd2pairz00_29));
		}

	}



/* &private-rsa-key */
	obj_t BGl_z62privatezd2rsazd2keyz62zz__rsaz00(obj_t BgL_envz00_3098,
		obj_t BgL_rsazd2keyzd2pairz00_3099)
	{
		{	/* Unsafe/rsa.scm 183 */
			return BGl_privatezd2rsazd2keyz00zz__rsaz00(BgL_rsazd2keyzd2pairz00_3099);
		}

	}



/* rsa-key= */
	BGL_EXPORTED_DEF obj_t BGl_rsazd2keyzd3z01zz__rsaz00(obj_t
		BgL_rsazd2key1zd2_30, obj_t BgL_rsazd2key2zd2_31)
	{
		{	/* Unsafe/rsa.scm 189 */
			if (
				((long) CINT(STRUCT_REF(
							((obj_t) BgL_rsazd2key1zd2_30),
							(int) (0L))) ==
					(long) CINT(STRUCT_REF(((obj_t) BgL_rsazd2key2zd2_31), (int) (0L)))))
				{	/* Unsafe/rsa.scm 191 */
					bool_t BgL_test2072z00_3447;

					{	/* Unsafe/rsa.scm 191 */
						obj_t BgL_n1z00_2625;
						obj_t BgL_n2z00_2626;

						BgL_n1z00_2625 =
							STRUCT_REF(((obj_t) BgL_rsazd2key1zd2_30), (int) (1L));
						BgL_n2z00_2626 =
							STRUCT_REF(((obj_t) BgL_rsazd2key2zd2_31), (int) (1L));
						BgL_test2072z00_3447 =
							((long) (bgl_bignum_cmp(BgL_n1z00_2625, BgL_n2z00_2626)) == 0L);
					}
					if (BgL_test2072z00_3447)
						{	/* Unsafe/rsa.scm 192 */
							obj_t BgL_n1z00_2631;
							obj_t BgL_n2z00_2632;

							BgL_n1z00_2631 =
								STRUCT_REF(((obj_t) BgL_rsazd2key1zd2_30), (int) (2L));
							BgL_n2z00_2632 =
								STRUCT_REF(((obj_t) BgL_rsazd2key2zd2_31), (int) (2L));
							return
								BBOOL(
								((long) (bgl_bignum_cmp(BgL_n1z00_2631,
											BgL_n2z00_2632)) == 0L));
						}
					else
						{	/* Unsafe/rsa.scm 191 */
							return BFALSE;
						}
				}
			else
				{	/* Unsafe/rsa.scm 190 */
					return BFALSE;
				}
		}

	}



/* &rsa-key= */
	obj_t BGl_z62rsazd2keyzd3z63zz__rsaz00(obj_t BgL_envz00_3100,
		obj_t BgL_rsazd2key1zd2_3101, obj_t BgL_rsazd2key2zd2_3102)
	{
		{	/* Unsafe/rsa.scm 189 */
			return
				BGl_rsazd2keyzd3z01zz__rsaz00(BgL_rsazd2key1zd2_3101,
				BgL_rsazd2key2zd2_3102);
		}

	}



/* PKCS1-pad */
	BGL_EXPORTED_DEF obj_t BGl_PKCS1zd2padzd2zz__rsaz00(obj_t BgL_u8vectz00_32,
		obj_t BgL_finalzd2lenzd2_33)
	{
		{	/* Unsafe/rsa.scm 224 */
			{	/* Unsafe/rsa.scm 225 */
				long BgL_lenz00_1542;

				{	/* Unsafe/rsa.scm 225 */
					obj_t BgL_tmpz00_3468;

					BgL_tmpz00_3468 = ((obj_t) BgL_u8vectz00_32);
					BgL_lenz00_1542 = BGL_HVECTOR_LENGTH(BgL_tmpz00_3468);
				}
				{	/* Unsafe/rsa.scm 225 */
					obj_t BgL_nz00_1543;

					{	/* Unsafe/rsa.scm 226 */
						long BgL_b1101z00_1563;

						BgL_b1101z00_1563 = (BgL_lenz00_1542 + 3L);
						if (INTEGERP(BgL_finalzd2lenzd2_33))
							{	/* Unsafe/rsa.scm 226 */
								BgL_nz00_1543 =
									SUBFX(BgL_finalzd2lenzd2_33, BINT(BgL_b1101z00_1563));
							}
						else
							{	/* Unsafe/rsa.scm 226 */
								BgL_nz00_1543 =
									BGl_2zd2zd2zz__r4_numbers_6_5z00(BgL_finalzd2lenzd2_33,
									BINT(BgL_b1101z00_1563));
							}
					}
					{	/* Unsafe/rsa.scm 226 */

						{	/* Unsafe/rsa.scm 227 */
							bool_t BgL_test2074z00_3478;

							if (INTEGERP(BgL_nz00_1543))
								{	/* Unsafe/rsa.scm 227 */
									BgL_test2074z00_3478 = ((long) CINT(BgL_nz00_1543) < 8L);
								}
							else
								{	/* Unsafe/rsa.scm 227 */
									BgL_test2074z00_3478 =
										BGl_2zc3zc3zz__r4_numbers_6_5z00(BgL_nz00_1543, BINT(8L));
								}
							if (BgL_test2074z00_3478)
								{	/* Unsafe/rsa.scm 227 */
									return
										BGl_errorz00zz__errorz00(BGl_symbol2001z00zz__rsaz00,
										BGl_string2003z00zz__rsaz00, BgL_nz00_1543);
								}
							else
								{	/* Unsafe/rsa.scm 231 */
									obj_t BgL_padz00_1545;

									{	/* Unsafe/rsa.scm 232 */
										obj_t BgL_g1047z00_1546;

										BgL_g1047z00_1546 = BGl_list2004z00zz__rsaz00;
										{
											obj_t BgL_lstz00_1548;
											long BgL_iz00_1549;

											BgL_lstz00_1548 = BgL_g1047z00_1546;
											BgL_iz00_1549 = 0L;
										BgL_zc3z04anonymousza31407ze3z87_1550:
											{	/* Unsafe/rsa.scm 233 */
												bool_t BgL_test2076z00_3486;

												if (INTEGERP(BgL_nz00_1543))
													{	/* Unsafe/rsa.scm 233 */
														BgL_test2076z00_3486 =
															(BgL_iz00_1549 < (long) CINT(BgL_nz00_1543));
													}
												else
													{	/* Unsafe/rsa.scm 233 */
														BgL_test2076z00_3486 =
															BGl_2zc3zc3zz__r4_numbers_6_5z00(BINT
															(BgL_iz00_1549), BgL_nz00_1543);
													}
												if (BgL_test2076z00_3486)
													{	/* Unsafe/rsa.scm 234 */
														obj_t BgL_arg1410z00_1553;
														long BgL_arg1411z00_1554;

														{	/* Unsafe/rsa.scm 234 */
															long BgL_arg1412z00_1555;

															{	/* Unsafe/rsa.scm 234 */
																long BgL_b1103z00_1557;

																{	/* Unsafe/rsa.scm 234 */
																	int BgL_arg1802z00_2643;

																	BgL_arg1802z00_2643 = rand();
																	BgL_b1103z00_1557 =
																		BGl_modulofxz00zz__r4_numbers_6_5_fixnumz00(
																		(long) (BgL_arg1802z00_2643), 255L);
																}
																{	/* Unsafe/rsa.scm 234 */

																	BgL_arg1412z00_1555 =
																		(1L + BgL_b1103z00_1557);
															}}
															BgL_arg1410z00_1553 =
																MAKE_YOUNG_PAIR(BINT(BgL_arg1412z00_1555),
																BgL_lstz00_1548);
														}
														BgL_arg1411z00_1554 = (BgL_iz00_1549 + 1L);
														{
															long BgL_iz00_3501;
															obj_t BgL_lstz00_3500;

															BgL_lstz00_3500 = BgL_arg1410z00_1553;
															BgL_iz00_3501 = BgL_arg1411z00_1554;
															BgL_iz00_1549 = BgL_iz00_3501;
															BgL_lstz00_1548 = BgL_lstz00_3500;
															goto BgL_zc3z04anonymousza31407ze3z87_1550;
														}
													}
												else
													{	/* Unsafe/rsa.scm 235 */
														obj_t BgL_arg1414z00_1559;

														{	/* Unsafe/rsa.scm 235 */
															obj_t BgL_arg1415z00_1560;

															BgL_arg1415z00_1560 =
																MAKE_YOUNG_PAIR(BINT(2L), BgL_lstz00_1548);
															BgL_arg1414z00_1559 =
																MAKE_YOUNG_PAIR(BINT(0L), BgL_arg1415z00_1560);
														}
														BgL_padz00_1545 =
															BGl_listzd2ze3u8vectorz31zz__srfi4z00
															(BgL_arg1414z00_1559);
													}
											}
										}
									}
									return
										BGl_u8vectorzd2appendzd2zz__rsaz00(BgL_padz00_1545,
										BgL_u8vectz00_32);
								}
						}
					}
				}
			}
		}

	}



/* &PKCS1-pad */
	obj_t BGl_z62PKCS1zd2padzb0zz__rsaz00(obj_t BgL_envz00_3103,
		obj_t BgL_u8vectz00_3104, obj_t BgL_finalzd2lenzd2_3105)
	{
		{	/* Unsafe/rsa.scm 224 */
			return
				BGl_PKCS1zd2padzd2zz__rsaz00(BgL_u8vectz00_3104,
				BgL_finalzd2lenzd2_3105);
		}

	}



/* PKCS1-unpad */
	BGL_EXPORTED_DEF obj_t BGl_PKCS1zd2unpadzd2zz__rsaz00(obj_t BgL_u8vectz00_34)
	{
		{	/* Unsafe/rsa.scm 241 */
			{	/* Unsafe/rsa.scm 246 */
				long BgL_lenz00_1566;

				{	/* Unsafe/rsa.scm 246 */
					obj_t BgL_tmpz00_3509;

					BgL_tmpz00_3509 = ((obj_t) BgL_u8vectz00_34);
					BgL_lenz00_1566 = BGL_HVECTOR_LENGTH(BgL_tmpz00_3509);
				}
				{
					long BgL_iz00_1568;

					BgL_iz00_1568 = 0L;
				BgL_zc3z04anonymousza31417ze3z87_1569:
					if ((BgL_iz00_1568 >= BgL_lenz00_1566))
						{	/* Unsafe/rsa.scm 248 */
							return
								BGl_errorz00zz__errorz00(BGl_symbol2005z00zz__rsaz00,
								BGl_string2007z00zz__rsaz00, BgL_u8vectz00_34);
						}
					else
						{	/* Unsafe/rsa.scm 250 */
							long BgL_xz00_1572;

							{	/* Unsafe/rsa.scm 250 */
								uint8_t BgL_arg1435z00_1598;

								{	/* Unsafe/rsa.scm 250 */
									obj_t BgL_tmpz00_3515;

									BgL_tmpz00_3515 = ((obj_t) BgL_u8vectz00_34);
									BgL_arg1435z00_1598 =
										BGL_U8VREF(BgL_tmpz00_3515, BgL_iz00_1568);
								}
								{	/* Unsafe/rsa.scm 250 */
									long BgL_arg1930z00_2653;

									BgL_arg1930z00_2653 = (long) (BgL_arg1435z00_1598);
									BgL_xz00_1572 = (long) (BgL_arg1930z00_2653);
							}}
							if ((BgL_xz00_1572 == 0L))
								{
									long BgL_iz00_3522;

									BgL_iz00_3522 = (BgL_iz00_1568 + 1L);
									BgL_iz00_1568 = BgL_iz00_3522;
									goto BgL_zc3z04anonymousza31417ze3z87_1569;
								}
							else
								{	/* Unsafe/rsa.scm 251 */
									if ((BgL_xz00_1572 == 2L))
										{	/* Unsafe/rsa.scm 256 */
											long BgL_g1048z00_1576;

											BgL_g1048z00_1576 = (BgL_iz00_1568 + 1L);
											{
												long BgL_jz00_1578;

												BgL_jz00_1578 = BgL_g1048z00_1576;
											BgL_zc3z04anonymousza31423ze3z87_1579:
												if ((BgL_jz00_1578 >= BgL_lenz00_1566))
													{	/* Unsafe/rsa.scm 257 */
														return
															BGl_errorz00zz__errorz00
															(BGl_symbol2005z00zz__rsaz00,
															BGl_string2007z00zz__rsaz00, BgL_u8vectz00_34);
													}
												else
													{	/* Unsafe/rsa.scm 259 */
														long BgL_xz00_1582;

														{	/* Unsafe/rsa.scm 259 */
															uint8_t BgL_arg1434z00_1595;

															{	/* Unsafe/rsa.scm 259 */
																obj_t BgL_tmpz00_3530;

																BgL_tmpz00_3530 = ((obj_t) BgL_u8vectz00_34);
																BgL_arg1434z00_1595 =
																	BGL_U8VREF(BgL_tmpz00_3530, BgL_jz00_1578);
															}
															{	/* Unsafe/rsa.scm 259 */
																long BgL_arg1930z00_2664;

																BgL_arg1930z00_2664 =
																	(long) (BgL_arg1434z00_1595);
																BgL_xz00_1582 = (long) (BgL_arg1930z00_2664);
														}}
														if ((BgL_xz00_1582 == 0L))
															{	/* Unsafe/rsa.scm 263 */
																bool_t BgL_test2083z00_3537;

																{	/* Unsafe/rsa.scm 263 */

																	BgL_test2083z00_3537 =
																		((BgL_jz00_1578 - BgL_iz00_1568) < 8L);
																}
																if (BgL_test2083z00_3537)
																	{	/* Unsafe/rsa.scm 263 */
																		return
																			BGl_errorz00zz__errorz00
																			(BGl_symbol2005z00zz__rsaz00,
																			BGl_string2007z00zz__rsaz00,
																			BgL_u8vectz00_34);
																	}
																else
																	{	/* Unsafe/rsa.scm 263 */
																		return
																			BGl_subu8vectorz00zz__rsaz00
																			(BgL_u8vectz00_34, (BgL_jz00_1578 + 1L),
																			BgL_lenz00_1566);
																	}
															}
														else
															{
																long BgL_jz00_3543;

																BgL_jz00_3543 = (BgL_jz00_1578 + 1L);
																BgL_jz00_1578 = BgL_jz00_3543;
																goto BgL_zc3z04anonymousza31423ze3z87_1579;
															}
													}
											}
										}
									else
										{	/* Unsafe/rsa.scm 253 */
											return
												BGl_errorz00zz__errorz00(BGl_symbol2005z00zz__rsaz00,
												BGl_string2007z00zz__rsaz00, BgL_u8vectz00_34);
										}
								}
						}
				}
			}
		}

	}



/* &PKCS1-unpad */
	obj_t BGl_z62PKCS1zd2unpadzb0zz__rsaz00(obj_t BgL_envz00_3106,
		obj_t BgL_u8vectz00_3107)
	{
		{	/* Unsafe/rsa.scm 241 */
			return BGl_PKCS1zd2unpadzd2zz__rsaz00(BgL_u8vectz00_3107);
		}

	}



/* rsa-encrypt-u8vector */
	BGL_EXPORTED_DEF obj_t BGl_rsazd2encryptzd2u8vectorz00zz__rsaz00(obj_t
		BgL_u8vectz00_37, obj_t BgL_rsazd2keyzd2_38, obj_t BgL_finalzd2lenzd2_39)
	{
		{	/* Unsafe/rsa.scm 280 */
			{	/* Unsafe/rsa.scm 283 */
				obj_t BgL_arg1439z00_2677;

				{	/* Unsafe/rsa.scm 283 */
					obj_t BgL_arg1440z00_2678;

					BgL_arg1440z00_2678 =
						BGl_u8vectorzd2ze3bignumz31zz__rsaz00(BGl_PKCS1zd2padzd2zz__rsaz00
						(BgL_u8vectz00_37, BgL_finalzd2lenzd2_39));
					BgL_arg1439z00_2677 =
						BGl_exptzd2modze70z35zz__rsaz00(BgL_arg1440z00_2678,
						STRUCT_REF(((obj_t) BgL_rsazd2keyzd2_38), (int) (2L)),
						STRUCT_REF(((obj_t) BgL_rsazd2keyzd2_38), (int) (1L)));
				}
				return BGl_bignumzd2ze3u8vectorz31zz__rsaz00(BgL_arg1439z00_2677);
			}
		}

	}



/* &rsa-encrypt-u8vector */
	obj_t BGl_z62rsazd2encryptzd2u8vectorz62zz__rsaz00(obj_t BgL_envz00_3108,
		obj_t BgL_u8vectz00_3109, obj_t BgL_rsazd2keyzd2_3110,
		obj_t BgL_finalzd2lenzd2_3111)
	{
		{	/* Unsafe/rsa.scm 280 */
			return
				BGl_rsazd2encryptzd2u8vectorz00zz__rsaz00(BgL_u8vectz00_3109,
				BgL_rsazd2keyzd2_3110, BgL_finalzd2lenzd2_3111);
		}

	}



/* rsa-decrypt-u8vector */
	BGL_EXPORTED_DEF obj_t BGl_rsazd2decryptzd2u8vectorz00zz__rsaz00(obj_t
		BgL_u8vectz00_40, obj_t BgL_rsazd2keyzd2_41)
	{
		{	/* Unsafe/rsa.scm 289 */
			{	/* Unsafe/rsa.scm 293 */
				obj_t BgL_arg1442z00_2684;

				{	/* Unsafe/rsa.scm 293 */
					obj_t BgL_arg1443z00_2685;

					{	/* Unsafe/rsa.scm 293 */
						obj_t BgL_arg1444z00_2686;

						BgL_arg1444z00_2686 =
							BGl_u8vectorzd2ze3bignumz31zz__rsaz00(BgL_u8vectz00_40);
						BgL_arg1443z00_2685 =
							BGl_exptzd2modze70z35zz__rsaz00(BgL_arg1444z00_2686,
							STRUCT_REF(
								((obj_t) BgL_rsazd2keyzd2_41),
								(int) (2L)),
							STRUCT_REF(((obj_t) BgL_rsazd2keyzd2_41), (int) (1L)));
					}
					BgL_arg1442z00_2684 =
						BGl_bignumzd2ze3u8vectorz31zz__rsaz00(BgL_arg1443z00_2685);
				}
				return BGl_PKCS1zd2unpadzd2zz__rsaz00(BgL_arg1442z00_2684);
			}
		}

	}



/* &rsa-decrypt-u8vector */
	obj_t BGl_z62rsazd2decryptzd2u8vectorz62zz__rsaz00(obj_t BgL_envz00_3112,
		obj_t BgL_u8vectz00_3113, obj_t BgL_rsazd2keyzd2_3114)
	{
		{	/* Unsafe/rsa.scm 289 */
			return
				BGl_rsazd2decryptzd2u8vectorz00zz__rsaz00(BgL_u8vectz00_3113,
				BgL_rsazd2keyzd2_3114);
		}

	}



/* expt-mod~0 */
	obj_t BGl_exptzd2modze70z35zz__rsaz00(obj_t BgL_nz00_1639,
		obj_t BgL_ez00_1640, obj_t BgL_mz00_1641)
	{
		{	/* Unsafe/rsa.scm 402 */
		BGl_exptzd2modze70z35zz__rsaz00:
			{	/* Unsafe/rsa.scm 397 */
				bool_t BgL_test2084z00_3569;

				{	/* Unsafe/rsa.scm 397 */
					obj_t BgL_tmpz00_3570;

					BgL_tmpz00_3570 = ((obj_t) BgL_ez00_1640);
					BgL_test2084z00_3569 = BXZERO(BgL_tmpz00_3570);
				}
				if (BgL_test2084z00_3569)
					{	/* Unsafe/rsa.scm 397 */
						return (bgl_string_to_bignum("1", 16));
					}
				else
					{	/* Unsafe/rsa.scm 397 */
						if (bgl_bignum_even(((obj_t) BgL_ez00_1640)))
							{
								obj_t BgL_ez00_3579;
								obj_t BgL_nz00_3576;

								BgL_nz00_3576 =
									BGl_modulobxz00zz__r4_numbers_6_5_fixnumz00(bgl_bignum_mul
									(BgL_nz00_1639, BgL_nz00_1639), BgL_mz00_1641);
								BgL_ez00_3579 =
									bgl_bignum_quotient(((obj_t) BgL_ez00_1640),
									(bgl_string_to_bignum("2", 16)));
								BgL_ez00_1640 = BgL_ez00_3579;
								BgL_nz00_1639 = BgL_nz00_3576;
								goto BGl_exptzd2modze70z35zz__rsaz00;
							}
						else
							{	/* Unsafe/rsa.scm 399 */
								return
									BGl_modulobxz00zz__r4_numbers_6_5_fixnumz00(bgl_bignum_mul
									(BgL_nz00_1639, BGl_exptzd2modze70z35zz__rsaz00(BgL_nz00_1639,
											bgl_bignum_sub(((obj_t) BgL_ez00_1640),
												(bgl_string_to_bignum("1", 16))), BgL_mz00_1641)),
									BgL_mz00_1641);
							}
					}
			}
		}

	}



/* u8vector-append */
	obj_t BGl_u8vectorzd2appendzd2zz__rsaz00(obj_t BgL_v1z00_48,
		obj_t BgL_v2z00_49)
	{
		{	/* Unsafe/rsa.scm 409 */
			{	/* Unsafe/rsa.scm 410 */
				long BgL_len1z00_1652;

				BgL_len1z00_1652 = BGL_HVECTOR_LENGTH(BgL_v1z00_48);
				{	/* Unsafe/rsa.scm 410 */
					long BgL_len2z00_1653;

					BgL_len2z00_1653 = BGL_HVECTOR_LENGTH(BgL_v2z00_49);
					{	/* Unsafe/rsa.scm 411 */
						long BgL_lenz00_1654;

						BgL_lenz00_1654 = (BgL_len1z00_1652 + BgL_len2z00_1653);
						{	/* Unsafe/rsa.scm 412 */
							obj_t BgL_resz00_1655;

							{	/* Llib/srfi4.scm 447 */

								BgL_resz00_1655 =
									BGl_makezd2u8vectorzd2zz__srfi4z00(BgL_lenz00_1654,
									(uint8_t) (0));
							}
							{	/* Unsafe/rsa.scm 413 */

								{
									long BgL_iz00_1657;

									BgL_iz00_1657 = 0L;
								BgL_zc3z04anonymousza31470ze3z87_1658:
									if ((BgL_iz00_1657 < BgL_len1z00_1652))
										{	/* Unsafe/rsa.scm 415 */
											{	/* Unsafe/rsa.scm 417 */
												long BgL_arg1472z00_1660;

												{	/* Unsafe/rsa.scm 417 */
													uint8_t BgL_arg1473z00_1661;

													BgL_arg1473z00_1661 =
														BGL_U8VREF(BgL_v1z00_48, BgL_iz00_1657);
													{	/* Unsafe/rsa.scm 417 */
														long BgL_arg1930z00_2710;

														BgL_arg1930z00_2710 = (long) (BgL_arg1473z00_1661);
														BgL_arg1472z00_1660 = (long) (BgL_arg1930z00_2710);
												}}
												{	/* Unsafe/rsa.scm 417 */
													uint8_t BgL_tmpz00_3596;

													BgL_tmpz00_3596 = (uint8_t) (BgL_arg1472z00_1660);
													BGL_U8VSET(BgL_resz00_1655, BgL_iz00_1657,
														BgL_tmpz00_3596);
												} BUNSPEC;
											}
											{
												long BgL_iz00_3599;

												BgL_iz00_3599 = (BgL_iz00_1657 + 1L);
												BgL_iz00_1657 = BgL_iz00_3599;
												goto BgL_zc3z04anonymousza31470ze3z87_1658;
											}
										}
									else
										{
											long BgL_jz00_1664;

											BgL_jz00_1664 = 0L;
										BgL_zc3z04anonymousza31475ze3z87_1665:
											if ((BgL_jz00_1664 < BgL_len2z00_1653))
												{	/* Unsafe/rsa.scm 420 */
													{	/* Unsafe/rsa.scm 422 */
														long BgL_arg1477z00_1667;
														long BgL_arg1478z00_1668;

														BgL_arg1477z00_1667 =
															(BgL_jz00_1664 + BgL_iz00_1657);
														{	/* Unsafe/rsa.scm 422 */
															uint8_t BgL_arg1479z00_1669;

															BgL_arg1479z00_1669 =
																BGL_U8VREF(BgL_v2z00_49, BgL_jz00_1664);
															{	/* Unsafe/rsa.scm 422 */
																long BgL_arg1930z00_2720;

																BgL_arg1930z00_2720 =
																	(long) (BgL_arg1479z00_1669);
																BgL_arg1478z00_1668 =
																	(long) (BgL_arg1930z00_2720);
														}}
														{	/* Unsafe/rsa.scm 422 */
															uint8_t BgL_tmpz00_3607;

															BgL_tmpz00_3607 = (uint8_t) (BgL_arg1478z00_1668);
															BGL_U8VSET(BgL_resz00_1655, BgL_arg1477z00_1667,
																BgL_tmpz00_3607);
														} BUNSPEC;
													}
													{
														long BgL_jz00_3610;

														BgL_jz00_3610 = (BgL_jz00_1664 + 1L);
														BgL_jz00_1664 = BgL_jz00_3610;
														goto BgL_zc3z04anonymousza31475ze3z87_1665;
													}
												}
											else
												{	/* Unsafe/rsa.scm 420 */
													return BgL_resz00_1655;
												}
										}
								}
							}
						}
					}
				}
			}
		}

	}



/* subu8vector */
	obj_t BGl_subu8vectorz00zz__rsaz00(obj_t BgL_vz00_50, long BgL_startz00_51,
		long BgL_endz00_52)
	{
		{	/* Unsafe/rsa.scm 429 */
			{	/* Unsafe/rsa.scm 430 */
				long BgL_lenz00_1675;

				BgL_lenz00_1675 = (BgL_endz00_52 - BgL_startz00_51);
				{	/* Unsafe/rsa.scm 430 */
					obj_t BgL_resz00_1676;

					{	/* Llib/srfi4.scm 447 */

						BgL_resz00_1676 =
							BGl_makezd2u8vectorzd2zz__srfi4z00(BgL_lenz00_1675,
							(uint8_t) (0));
					}
					{	/* Unsafe/rsa.scm 431 */

						{
							long BgL_iz00_1678;

							BgL_iz00_1678 = BgL_startz00_51;
						BgL_zc3z04anonymousza31481ze3z87_1679:
							if ((BgL_iz00_1678 < BgL_endz00_52))
								{	/* Unsafe/rsa.scm 433 */
									{	/* Unsafe/rsa.scm 435 */
										long BgL_arg1483z00_1681;
										long BgL_arg1484z00_1682;

										BgL_arg1483z00_1681 = (BgL_iz00_1678 - BgL_startz00_51);
										{	/* Unsafe/rsa.scm 435 */
											uint8_t BgL_arg1485z00_1683;

											BgL_arg1485z00_1683 =
												BGL_U8VREF(BgL_vz00_50, BgL_iz00_1678);
											{	/* Unsafe/rsa.scm 435 */
												long BgL_arg1930z00_2732;

												BgL_arg1930z00_2732 = (long) (BgL_arg1485z00_1683);
												BgL_arg1484z00_1682 = (long) (BgL_arg1930z00_2732);
										}}
										{	/* Unsafe/rsa.scm 435 */
											uint8_t BgL_tmpz00_3620;

											BgL_tmpz00_3620 = (uint8_t) (BgL_arg1484z00_1682);
											BGL_U8VSET(BgL_resz00_1676, BgL_arg1483z00_1681,
												BgL_tmpz00_3620);
										} BUNSPEC;
									}
									{
										long BgL_iz00_3623;

										BgL_iz00_3623 = (BgL_iz00_1678 + 1L);
										BgL_iz00_1678 = BgL_iz00_3623;
										goto BgL_zc3z04anonymousza31481ze3z87_1679;
									}
								}
							else
								{	/* Unsafe/rsa.scm 433 */
									return BgL_resz00_1676;
								}
						}
					}
				}
			}
		}

	}



/* bignum->u8vector */
	obj_t BGl_bignumzd2ze3u8vectorz31zz__rsaz00(obj_t BgL_nz00_54)
	{
		{	/* Unsafe/rsa.scm 461 */
			{
				obj_t BgL_nz00_1743;

				{	/* Unsafe/rsa.scm 470 */
					long BgL_siza7eza7_1729;

					BgL_nz00_1743 = BgL_nz00_54;
					{
						long BgL_siza7eza7_2756;
						obj_t BgL_accz00_2757;

						BgL_siza7eza7_2756 = 1L;
						BgL_accz00_2757 = (bgl_string_to_bignum("ff", 16));
					BgL_loopz00_2755:
						if (((long) (bgl_bignum_cmp(BgL_nz00_1743, BgL_accz00_2757)) > 0L))
							{
								obj_t BgL_accz00_3631;
								long BgL_siza7eza7_3629;

								BgL_siza7eza7_3629 = (BgL_siza7eza7_2756 + 1L);
								BgL_accz00_3631 =
									bgl_bignum_mul(BgL_accz00_2757, (bgl_string_to_bignum("ff",
											16)));
								BgL_accz00_2757 = BgL_accz00_3631;
								BgL_siza7eza7_2756 = BgL_siza7eza7_3629;
								goto BgL_loopz00_2755;
							}
						else
							{	/* Unsafe/rsa.scm 466 */
								BgL_siza7eza7_1729 = BgL_siza7eza7_2756;
							}
					}
					{	/* Unsafe/rsa.scm 470 */
						obj_t BgL_resz00_1730;

						{	/* Llib/srfi4.scm 447 */

							BgL_resz00_1730 =
								BGl_makezd2u8vectorzd2zz__srfi4z00(BgL_siza7eza7_1729,
								(uint8_t) (0));
						}
						{	/* Unsafe/rsa.scm 471 */

							{
								long BgL_iz00_2780;
								obj_t BgL_vz00_2781;

								BgL_iz00_2780 = 0L;
								BgL_vz00_2781 = BgL_nz00_54;
							BgL_loopz00_2779:
								if ((BgL_iz00_2780 == BgL_siza7eza7_1729))
									{	/* Unsafe/rsa.scm 474 */
										return BgL_resz00_1730;
									}
								else
									{	/* Unsafe/rsa.scm 474 */
										{	/* Unsafe/rsa.scm 477 */
											long BgL_arg1511z00_2785;

											BgL_arg1511z00_2785 =
												bgl_bignum_to_long(bgl_bignum_remainder(BgL_vz00_2781,
													(bgl_string_to_bignum("100", 16))));
											{	/* Unsafe/rsa.scm 477 */
												uint8_t BgL_tmpz00_3638;

												BgL_tmpz00_3638 = (uint8_t) (BgL_arg1511z00_2785);
												BGL_U8VSET(BgL_resz00_1730, BgL_iz00_2780,
													BgL_tmpz00_3638);
											} BUNSPEC;
										}
										{
											obj_t BgL_vz00_3643;
											long BgL_iz00_3641;

											BgL_iz00_3641 = (BgL_iz00_2780 + 1L);
											BgL_vz00_3643 =
												bgl_bignum_quotient(BgL_vz00_2781,
												(bgl_string_to_bignum("100", 16)));
											BgL_vz00_2781 = BgL_vz00_3643;
											BgL_iz00_2780 = BgL_iz00_3641;
											goto BgL_loopz00_2779;
										}
									}
							}
						}
					}
				}
			}
		}

	}



/* u8vector->bignum */
	obj_t BGl_u8vectorzd2ze3bignumz31zz__rsaz00(obj_t BgL_u8vectz00_55)
	{
		{	/* Unsafe/rsa.scm 483 */
			{	/* Unsafe/rsa.scm 484 */
				long BgL_g1050z00_1754;

				{	/* Unsafe/rsa.scm 484 */
					long BgL_arg1530z00_1767;

					BgL_arg1530z00_1767 = BGL_HVECTOR_LENGTH(BgL_u8vectz00_55);
					BgL_g1050z00_1754 = (BgL_arg1530z00_1767 - 1L);
				}
				{
					long BgL_iz00_2810;
					obj_t BgL_az00_2811;

					BgL_iz00_2810 = BgL_g1050z00_1754;
					BgL_az00_2811 = (bgl_string_to_bignum("0", 16));
				BgL_loopz00_2809:
					if ((BgL_iz00_2810 == -1L))
						{	/* Unsafe/rsa.scm 486 */
							return BgL_az00_2811;
						}
					else
						{	/* Unsafe/rsa.scm 488 */
							obj_t BgL_dz00_2814;

							{	/* Unsafe/rsa.scm 488 */
								long BgL_arg1528z00_2815;

								{	/* Unsafe/rsa.scm 488 */
									uint8_t BgL_arg1529z00_2816;

									BgL_arg1529z00_2816 =
										BGL_U8VREF(BgL_u8vectz00_55, BgL_iz00_2810);
									{	/* Unsafe/rsa.scm 488 */
										long BgL_arg1930z00_2820;

										BgL_arg1930z00_2820 = (long) (BgL_arg1529z00_2816);
										BgL_arg1528z00_2815 = (long) (BgL_arg1930z00_2820);
								}}
								BgL_dz00_2814 = bgl_long_to_bignum(BgL_arg1528z00_2815);
							}
							{
								obj_t BgL_az00_3655;
								long BgL_iz00_3653;

								BgL_iz00_3653 = (BgL_iz00_2810 - 1L);
								BgL_az00_3655 =
									bgl_bignum_add(bgl_bignum_mul(BgL_az00_2811,
										(bgl_string_to_bignum("100", 16))), BgL_dz00_2814);
								BgL_az00_2811 = BgL_az00_3655;
								BgL_iz00_2810 = BgL_iz00_3653;
								goto BgL_loopz00_2809;
							}
						}
				}
			}
		}

	}



/* rsa-encrypt-string */
	BGL_EXPORTED_DEF obj_t BGl_rsazd2encryptzd2stringz00zz__rsaz00(obj_t
		BgL_strz00_57, obj_t BgL_keyz00_58)
	{
		{	/* Unsafe/rsa.scm 520 */
			{	/* Unsafe/rsa.scm 521 */
				obj_t BgL_runner1566z00_1827;

				{	/* Unsafe/rsa.scm 522 */
					obj_t BgL_l01120z00_1804;

					{	/* Unsafe/rsa.scm 525 */
						obj_t BgL_arg1555z00_1813;

						{	/* Unsafe/rsa.scm 525 */
							obj_t BgL_arg1556z00_1814;
							long BgL_arg1557z00_1815;

							{	/* Unsafe/rsa.scm 525 */
								obj_t BgL_arg1558z00_1816;

								{	/* Unsafe/rsa.scm 525 */
									obj_t BgL_l01117z00_1817;

									BgL_l01117z00_1817 =
										BGl_stringzd2ze3listz31zz__r4_strings_6_7z00(BgL_strz00_57);
									{
										obj_t BgL_l1116z00_1819;

										BgL_l1116z00_1819 = BgL_l01117z00_1817;
									BgL_zc3z04anonymousza31559ze3z87_1820:
										if (NULLP(BgL_l1116z00_1819))
											{	/* Unsafe/rsa.scm 525 */
												BgL_arg1558z00_1816 = BgL_l01117z00_1817;
											}
										else
											{	/* Unsafe/rsa.scm 525 */
												{	/* Unsafe/rsa.scm 525 */
													long BgL_arg1561z00_1822;

													BgL_arg1561z00_1822 =
														(CCHAR(CAR(((obj_t) BgL_l1116z00_1819))));
													{	/* Unsafe/rsa.scm 525 */
														obj_t BgL_auxz00_3667;
														obj_t BgL_tmpz00_3665;

														BgL_auxz00_3667 = BINT(BgL_arg1561z00_1822);
														BgL_tmpz00_3665 = ((obj_t) BgL_l1116z00_1819);
														SET_CAR(BgL_tmpz00_3665, BgL_auxz00_3667);
												}}
												{	/* Unsafe/rsa.scm 525 */
													obj_t BgL_arg1564z00_1824;

													BgL_arg1564z00_1824 =
														CDR(((obj_t) BgL_l1116z00_1819));
													{
														obj_t BgL_l1116z00_3672;

														BgL_l1116z00_3672 = BgL_arg1564z00_1824;
														BgL_l1116z00_1819 = BgL_l1116z00_3672;
														goto BgL_zc3z04anonymousza31559ze3z87_1820;
													}
												}
											}
									}
								}
								BgL_arg1556z00_1814 =
									BGl_listzd2ze3u8vectorz31zz__srfi4z00(BgL_arg1558z00_1816);
							}
							BgL_arg1557z00_1815 = (STRING_LENGTH(BgL_strz00_57) + 12L);
							{	/* Unsafe/rsa.scm 283 */
								obj_t BgL_arg1439z00_2837;

								{	/* Unsafe/rsa.scm 283 */
									obj_t BgL_arg1440z00_2838;

									BgL_arg1440z00_2838 =
										BGl_u8vectorzd2ze3bignumz31zz__rsaz00
										(BGl_PKCS1zd2padzd2zz__rsaz00(BgL_arg1556z00_1814,
											BINT(BgL_arg1557z00_1815)));
									BgL_arg1439z00_2837 =
										BGl_exptzd2modze70z35zz__rsaz00(BgL_arg1440z00_2838,
										STRUCT_REF(((obj_t) BgL_keyz00_58), (int) (2L)),
										STRUCT_REF(((obj_t) BgL_keyz00_58), (int) (1L)));
								}
								BgL_arg1555z00_1813 =
									BGl_bignumzd2ze3u8vectorz31zz__rsaz00(BgL_arg1439z00_2837);
						}}
						BgL_l01120z00_1804 =
							BGl_u8vectorzd2ze3listz31zz__srfi4z00(BgL_arg1555z00_1813);
					}
					{
						obj_t BgL_l1119z00_1806;

						BgL_l1119z00_1806 = BgL_l01120z00_1804;
					BgL_zc3z04anonymousza31548ze3z87_1807:
						if (NULLP(BgL_l1119z00_1806))
							{	/* Unsafe/rsa.scm 523 */
								BgL_runner1566z00_1827 = BgL_l01120z00_1804;
							}
						else
							{	/* Unsafe/rsa.scm 523 */
								{	/* Unsafe/rsa.scm 523 */
									unsigned char BgL_arg1552z00_1809;

									BgL_arg1552z00_1809 =
										((long) CINT(CAR(((obj_t) BgL_l1119z00_1806))));
									{	/* Unsafe/rsa.scm 523 */
										obj_t BgL_auxz00_3696;
										obj_t BgL_tmpz00_3694;

										BgL_auxz00_3696 = BCHAR(BgL_arg1552z00_1809);
										BgL_tmpz00_3694 = ((obj_t) BgL_l1119z00_1806);
										SET_CAR(BgL_tmpz00_3694, BgL_auxz00_3696);
								}}
								{	/* Unsafe/rsa.scm 523 */
									obj_t BgL_arg1554z00_1811;

									BgL_arg1554z00_1811 = CDR(((obj_t) BgL_l1119z00_1806));
									{
										obj_t BgL_l1119z00_3701;

										BgL_l1119z00_3701 = BgL_arg1554z00_1811;
										BgL_l1119z00_1806 = BgL_l1119z00_3701;
										goto BgL_zc3z04anonymousza31548ze3z87_1807;
									}
								}
							}
					}
				}
				return
					BGl_listzd2ze3stringz31zz__r4_strings_6_7z00(BgL_runner1566z00_1827);
			}
		}

	}



/* &rsa-encrypt-string */
	obj_t BGl_z62rsazd2encryptzd2stringz62zz__rsaz00(obj_t BgL_envz00_3115,
		obj_t BgL_strz00_3116, obj_t BgL_keyz00_3117)
	{
		{	/* Unsafe/rsa.scm 520 */
			{	/* Unsafe/rsa.scm 521 */
				obj_t BgL_auxz00_3703;

				if (STRINGP(BgL_strz00_3116))
					{	/* Unsafe/rsa.scm 521 */
						BgL_auxz00_3703 = BgL_strz00_3116;
					}
				else
					{
						obj_t BgL_auxz00_3706;

						BgL_auxz00_3706 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1996z00zz__rsaz00,
							BINT(21891L), BGl_string2008z00zz__rsaz00,
							BGl_string2009z00zz__rsaz00, BgL_strz00_3116);
						FAILURE(BgL_auxz00_3706, BFALSE, BFALSE);
					}
				return
					BGl_rsazd2encryptzd2stringz00zz__rsaz00(BgL_auxz00_3703,
					BgL_keyz00_3117);
			}
		}

	}



/* rsa-decrypt-string */
	BGL_EXPORTED_DEF obj_t BGl_rsazd2decryptzd2stringz00zz__rsaz00(obj_t
		BgL_strz00_59, obj_t BgL_keyz00_60)
	{
		{	/* Unsafe/rsa.scm 532 */
			{	/* Unsafe/rsa.scm 533 */
				obj_t BgL_runner1585z00_1849;

				{	/* Unsafe/rsa.scm 534 */
					obj_t BgL_l01126z00_1828;

					{	/* Unsafe/rsa.scm 537 */
						obj_t BgL_arg1576z00_1837;

						{	/* Unsafe/rsa.scm 537 */
							obj_t BgL_arg1578z00_1838;

							{	/* Unsafe/rsa.scm 537 */
								obj_t BgL_arg1579z00_1839;

								{	/* Unsafe/rsa.scm 537 */
									obj_t BgL_l01123z00_1840;

									BgL_l01123z00_1840 =
										BGl_stringzd2ze3listz31zz__r4_strings_6_7z00(BgL_strz00_59);
									{
										obj_t BgL_l1122z00_1842;

										BgL_l1122z00_1842 = BgL_l01123z00_1840;
									BgL_zc3z04anonymousza31580ze3z87_1843:
										if (NULLP(BgL_l1122z00_1842))
											{	/* Unsafe/rsa.scm 537 */
												BgL_arg1579z00_1839 = BgL_l01123z00_1840;
											}
										else
											{	/* Unsafe/rsa.scm 537 */
												{	/* Unsafe/rsa.scm 537 */
													long BgL_arg1582z00_1845;

													BgL_arg1582z00_1845 =
														(CCHAR(CAR(((obj_t) BgL_l1122z00_1842))));
													{	/* Unsafe/rsa.scm 537 */
														obj_t BgL_auxz00_3720;
														obj_t BgL_tmpz00_3718;

														BgL_auxz00_3720 = BINT(BgL_arg1582z00_1845);
														BgL_tmpz00_3718 = ((obj_t) BgL_l1122z00_1842);
														SET_CAR(BgL_tmpz00_3718, BgL_auxz00_3720);
												}}
												{	/* Unsafe/rsa.scm 537 */
													obj_t BgL_arg1584z00_1847;

													BgL_arg1584z00_1847 =
														CDR(((obj_t) BgL_l1122z00_1842));
													{
														obj_t BgL_l1122z00_3725;

														BgL_l1122z00_3725 = BgL_arg1584z00_1847;
														BgL_l1122z00_1842 = BgL_l1122z00_3725;
														goto BgL_zc3z04anonymousza31580ze3z87_1843;
													}
												}
											}
									}
								}
								BgL_arg1578z00_1838 =
									BGl_listzd2ze3u8vectorz31zz__srfi4z00(BgL_arg1579z00_1839);
							}
							{	/* Unsafe/rsa.scm 293 */
								obj_t BgL_arg1442z00_2852;

								{	/* Unsafe/rsa.scm 293 */
									obj_t BgL_arg1443z00_2853;

									{	/* Unsafe/rsa.scm 293 */
										obj_t BgL_arg1444z00_2854;

										BgL_arg1444z00_2854 =
											BGl_u8vectorzd2ze3bignumz31zz__rsaz00
											(BgL_arg1578z00_1838);
										BgL_arg1443z00_2853 =
											BGl_exptzd2modze70z35zz__rsaz00(BgL_arg1444z00_2854,
											STRUCT_REF(((obj_t) BgL_keyz00_60), (int) (2L)),
											STRUCT_REF(((obj_t) BgL_keyz00_60), (int) (1L)));
									}
									BgL_arg1442z00_2852 =
										BGl_bignumzd2ze3u8vectorz31zz__rsaz00(BgL_arg1443z00_2853);
								}
								BgL_arg1576z00_1837 =
									BGl_PKCS1zd2unpadzd2zz__rsaz00(BgL_arg1442z00_2852);
						}}
						BgL_l01126z00_1828 =
							BGl_u8vectorzd2ze3listz31zz__srfi4z00(BgL_arg1576z00_1837);
					}
					{
						obj_t BgL_l1125z00_1830;

						BgL_l1125z00_1830 = BgL_l01126z00_1828;
					BgL_zc3z04anonymousza31567ze3z87_1831:
						if (NULLP(BgL_l1125z00_1830))
							{	/* Unsafe/rsa.scm 535 */
								BgL_runner1585z00_1849 = BgL_l01126z00_1828;
							}
						else
							{	/* Unsafe/rsa.scm 535 */
								{	/* Unsafe/rsa.scm 535 */
									unsigned char BgL_arg1571z00_1833;

									BgL_arg1571z00_1833 =
										((long) CINT(CAR(((obj_t) BgL_l1125z00_1830))));
									{	/* Unsafe/rsa.scm 535 */
										obj_t BgL_auxz00_3746;
										obj_t BgL_tmpz00_3744;

										BgL_auxz00_3746 = BCHAR(BgL_arg1571z00_1833);
										BgL_tmpz00_3744 = ((obj_t) BgL_l1125z00_1830);
										SET_CAR(BgL_tmpz00_3744, BgL_auxz00_3746);
								}}
								{	/* Unsafe/rsa.scm 535 */
									obj_t BgL_arg1575z00_1835;

									BgL_arg1575z00_1835 = CDR(((obj_t) BgL_l1125z00_1830));
									{
										obj_t BgL_l1125z00_3751;

										BgL_l1125z00_3751 = BgL_arg1575z00_1835;
										BgL_l1125z00_1830 = BgL_l1125z00_3751;
										goto BgL_zc3z04anonymousza31567ze3z87_1831;
									}
								}
							}
					}
				}
				return
					BGl_listzd2ze3stringz31zz__r4_strings_6_7z00(BgL_runner1585z00_1849);
			}
		}

	}



/* &rsa-decrypt-string */
	obj_t BGl_z62rsazd2decryptzd2stringz62zz__rsaz00(obj_t BgL_envz00_3118,
		obj_t BgL_strz00_3119, obj_t BgL_keyz00_3120)
	{
		{	/* Unsafe/rsa.scm 532 */
			{	/* Unsafe/rsa.scm 533 */
				obj_t BgL_auxz00_3753;

				if (STRINGP(BgL_strz00_3119))
					{	/* Unsafe/rsa.scm 533 */
						BgL_auxz00_3753 = BgL_strz00_3119;
					}
				else
					{
						obj_t BgL_auxz00_3756;

						BgL_auxz00_3756 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1996z00zz__rsaz00,
							BINT(22352L), BGl_string2010z00zz__rsaz00,
							BGl_string2009z00zz__rsaz00, BgL_strz00_3119);
						FAILURE(BgL_auxz00_3756, BFALSE, BFALSE);
					}
				return
					BGl_rsazd2decryptzd2stringz00zz__rsaz00(BgL_auxz00_3753,
					BgL_keyz00_3120);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__rsaz00(void)
	{
		{	/* Unsafe/rsa.scm 16 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__rsaz00(void)
	{
		{	/* Unsafe/rsa.scm 16 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__rsaz00(void)
	{
		{	/* Unsafe/rsa.scm 16 */
			return BUNSPEC;
		}

	}

#ifdef __cplusplus
}
#endif
