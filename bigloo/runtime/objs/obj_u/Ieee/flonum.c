/*===========================================================================*/
/*   (Ieee/flonum.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Ieee/flonum.scm -indent -o objs/obj_u/Ieee/flonum.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___R4_NUMBERS_6_5_FLONUM_TYPE_DEFINITIONS
#define BGL___R4_NUMBERS_6_5_FLONUM_TYPE_DEFINITIONS
#endif													// BGL___R4_NUMBERS_6_5_FLONUM_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t
		BGl_realzd2ze3ieeezd2stringze3zz__r4_numbers_6_5_flonumz00(obj_t);
	static obj_t BGl_z62stringzd2ze3realz53zz__r4_numbers_6_5_flonumz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_ze3zd3flz30zz__r4_numbers_6_5_flonumz00(double,
		double);
	static obj_t BGl_z62remainderflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62negflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
	BGL_EXPORTED_DECL float
		BGl_ieeezd2stringzd2ze3floatze3zz__r4_numbers_6_5_flonumz00(obj_t);
	static obj_t BGl_z62za2flzc0zz__r4_numbers_6_5_flonumz00(obj_t, obj_t, obj_t);
	extern float bgl_ieee_string_to_float(obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_infiniteflzf3zf3zz__r4_numbers_6_5_flonumz00(double);
	extern obj_t the_failure(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL double BGl_ceilingflz00zz__r4_numbers_6_5_flonumz00(double);
	static obj_t BGl_z62oddflzf3z91zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
	static obj_t BGl_z62atanzd22flzd2urz62zz__r4_numbers_6_5_flonumz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_ze3flze3zz__r4_numbers_6_5_flonumz00(double,
		double);
	static obj_t BGl_requirezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00 =
		BUNSPEC;
	BGL_EXPORTED_DECL double BGl_randomflz00zz__r4_numbers_6_5_flonumz00(void);
	BGL_EXPORTED_DECL double BGl_exptflz00zz__r4_numbers_6_5_flonumz00(double,
		double);
	BGL_EXPORTED_DECL double
		BGl_atanzd21flzd2zz__r4_numbers_6_5_flonumz00(double);
	BGL_EXPORTED_DECL double BGl_sqrtflz00zz__r4_numbers_6_5_flonumz00(double);
	static obj_t BGl_z62asinflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
	BGL_EXPORTED_DECL double
		BGl_stringzd2ze3realz31zz__r4_numbers_6_5_flonumz00(char *);
	static obj_t
		BGl_z62doublezd2ze3ieeezd2stringz81zz__r4_numbers_6_5_flonumz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL double BGl_logflz00zz__r4_numbers_6_5_flonumz00(double);
	BGL_EXPORTED_DECL double BGl_minzd22flzd2zz__r4_numbers_6_5_flonumz00(double,
		double);
	BGL_EXPORTED_DECL double BGl_minflz00zz__r4_numbers_6_5_flonumz00(double,
		obj_t);
	BGL_EXPORTED_DECL double BGl_maxzd22flzd2zz__r4_numbers_6_5_flonumz00(double,
		double);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62minzd22flzb0zz__r4_numbers_6_5_flonumz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_z62maxzd22flzb0zz__r4_numbers_6_5_flonumz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62maxflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_evenflzf3zf3zz__r4_numbers_6_5_flonumz00(double);
	BGL_EXPORTED_DECL BGL_LONGLONG_T
		BGl_doublezd2ze3llongzd2bitsze3zz__r4_numbers_6_5_flonumz00(double);
	BGL_EXPORTED_DECL double BGl_zf2flzf2zz__r4_numbers_6_5_flonumz00(double,
		double);
	static obj_t
		BGl_z62ieeezd2stringzd2ze3floatz81zz__r4_numbers_6_5_flonumz00(obj_t,
		obj_t);
	static obj_t BGl_z62zb2flzd0zz__r4_numbers_6_5_flonumz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62evenflzf3z91zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_floatzd2ze3intzd2bitsze3zz__r4_numbers_6_5_flonumz00(float);
	static obj_t
		BGl_z62floatzd2ze3intzd2bitsz81zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
	static obj_t
		BGl_z62ieeezd2stringzd2ze3realz81zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_za7eroflzf3z54zz__r4_numbers_6_5_flonumz00(double);
	BGL_EXPORTED_DECL double BGl_atanzd22flzd2zz__r4_numbers_6_5_flonumz00(double,
		double);
	BGL_EXPORTED_DECL double BGl_atanflz00zz__r4_numbers_6_5_flonumz00(double,
		obj_t);
	static obj_t BGl_z62za7eroflzf3z36zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
	BGL_EXPORTED_DECL double BGl_acosflz00zz__r4_numbers_6_5_flonumz00(double);
	BGL_EXPORTED_DECL double BGl_expflz00zz__r4_numbers_6_5_flonumz00(double);
	BGL_EXPORTED_DECL double
		BGl_ieeezd2stringzd2ze3doubleze3zz__r4_numbers_6_5_flonumz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_nanflzf3zf3zz__r4_numbers_6_5_flonumz00(double);
	static obj_t BGl_z62finiteflzf3z91zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_realzf3zf3zz__r4_numbers_6_5_flonumz00(obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_positiveflzf3zf3zz__r4_numbers_6_5_flonumz00(double);
	static obj_t
		BGl_z62doublezd2ze3llongzd2bitsz81zz__r4_numbers_6_5_flonumz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3flza1zz__r4_numbers_6_5_flonumz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL double BGl_floorflz00zz__r4_numbers_6_5_flonumz00(double);
	BGL_EXPORTED_DECL double
		BGl_truncateflz00zz__r4_numbers_6_5_flonumz00(double);
	static obj_t BGl_z62floorflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
	BGL_EXPORTED_DECL double BGl_roundflz00zz__r4_numbers_6_5_flonumz00(double);
	static obj_t BGl_z62roundflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
	static obj_t BGl_z62negativeflzf3z91zz__r4_numbers_6_5_flonumz00(obj_t,
		obj_t);
	static obj_t
		BGl_importedzd2moduleszd2initz00zz__r4_numbers_6_5_flonumz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__r4_numbers_6_5_flonumz00(void);
	BGL_EXPORTED_DECL double BGl_tanflz00zz__r4_numbers_6_5_flonumz00(double);
	BGL_EXPORTED_DECL double BGl_log10flz00zz__r4_numbers_6_5_flonumz00(double);
	BGL_EXPORTED_DECL double BGl_cosflz00zz__r4_numbers_6_5_flonumz00(double);
	static obj_t BGl_z62logflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
	static obj_t BGl_z62log10flz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
	static obj_t BGl_z62minflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zd2flzb0zz__r4_numbers_6_5_flonumz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62signbitflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_oddflzf3zf3zz__r4_numbers_6_5_flonumz00(double);
	BGL_EXPORTED_DECL double BGl_absflz00zz__r4_numbers_6_5_flonumz00(double);
	BGL_EXPORTED_DECL obj_t
		BGl_ieeezd2stringzd2ze3realze3zz__r4_numbers_6_5_flonumz00(obj_t);
	static obj_t BGl_z62zd3flzb1zz__r4_numbers_6_5_flonumz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62randomflz62zz__r4_numbers_6_5_flonumz00(obj_t);
	static obj_t BGl_z62atanzd21flzb0zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
	BGL_EXPORTED_DECL double BGl_asinflz00zz__r4_numbers_6_5_flonumz00(double);
	static obj_t BGl_z62truncateflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
	static obj_t BGl_z62expflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
	BGL_EXPORTED_DECL double
		BGl_remainderflz00zz__r4_numbers_6_5_flonumz00(double, double);
	BGL_EXPORTED_DECL bool_t
		BGl_finiteflzf3zf3zz__r4_numbers_6_5_flonumz00(double);
	static obj_t BGl_z62zc3zd3flz72zz__r4_numbers_6_5_flonumz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62sqrtflzd2urzb0zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
	BGL_EXPORTED_DECL double BGl_za2flza2zz__r4_numbers_6_5_flonumz00(double,
		double);
	extern double bgl_ieee_string_to_double(obj_t);
	static obj_t BGl_z62realzf3z91zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
	BGL_EXPORTED_DECL double
		BGl_atanzd22flzd2urz00zz__r4_numbers_6_5_flonumz00(double, double);
	static obj_t BGl_z62log2flz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
	static obj_t BGl_z62infiniteflzf3z91zz__r4_numbers_6_5_flonumz00(obj_t,
		obj_t);
	static obj_t BGl_z62ze3flz81zz__r4_numbers_6_5_flonumz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62atanzd22flzb0zz__r4_numbers_6_5_flonumz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_integerflzf3zf3zz__r4_numbers_6_5_flonumz00(double);
	BGL_EXPORTED_DECL obj_t
		BGl_doublezd2ze3ieeezd2stringze3zz__r4_numbers_6_5_flonumz00(double);
	BGL_EXPORTED_DECL bool_t BGl_zc3zd3flz10zz__r4_numbers_6_5_flonumz00(double,
		double);
	static obj_t BGl_z62tanflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
	BGL_EXPORTED_DECL double BGl_sinflz00zz__r4_numbers_6_5_flonumz00(double);
	static obj_t BGl_z62cosflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
	BGL_EXPORTED_DECL double BGl_zb2flzb2zz__r4_numbers_6_5_flonumz00(double,
		double);
	BGL_EXPORTED_DECL double
		BGl_llongzd2bitszd2ze3doubleze3zz__r4_numbers_6_5_flonumz00(BGL_LONGLONG_T);
	BGL_EXPORTED_DECL long BGl_signbitflz00zz__r4_numbers_6_5_flonumz00(double);
	static obj_t BGl_z62zf2flz90zz__r4_numbers_6_5_flonumz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62absflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
	static obj_t BGl_z62exptflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62sqrtflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
	static obj_t BGl_z62integerflzf3z91zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
	static obj_t
		BGl_z62ieeezd2stringzd2ze3doublez81zz__r4_numbers_6_5_flonumz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL float
		BGl_intzd2bitszd2ze3floatze3zz__r4_numbers_6_5_flonumz00(int);
	BGL_EXPORTED_DECL double BGl_negflz00zz__r4_numbers_6_5_flonumz00(double);
	BGL_EXPORTED_DECL double
		BGl_sqrtflzd2urzd2zz__r4_numbers_6_5_flonumz00(double);
	static obj_t
		BGl_z62intzd2bitszd2ze3floatz81zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
	static obj_t
		BGl_z62realzd2ze3ieeezd2stringz81zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_floatzd2ze3ieeezd2stringze3zz__r4_numbers_6_5_flonumz00(float);
	extern obj_t bgl_float_to_ieee_string(float);
	static obj_t
		BGl_z62llongzd2bitszd2ze3doublez81zz__r4_numbers_6_5_flonumz00(obj_t,
		obj_t);
	static obj_t BGl_z62positiveflzf3z91zz__r4_numbers_6_5_flonumz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_zc3flzc3zz__r4_numbers_6_5_flonumz00(double,
		double);
	static obj_t BGl_z62ceilingflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
	static obj_t BGl_z62atanflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62acosflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_negativeflzf3zf3zz__r4_numbers_6_5_flonumz00(double);
	BGL_EXPORTED_DECL bool_t BGl_flonumzf3zf3zz__r4_numbers_6_5_flonumz00(obj_t);
	static obj_t BGl_z62ze3zd3flz52zz__r4_numbers_6_5_flonumz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62nanflzf3z91zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
	static obj_t BGl_z62flonumzf3z91zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
	BGL_EXPORTED_DECL double BGl_maxflz00zz__r4_numbers_6_5_flonumz00(double,
		obj_t);
	BGL_EXPORTED_DECL double BGl_zd2flzd2zz__r4_numbers_6_5_flonumz00(double,
		double);
	static obj_t BGl_z62sinflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
	static obj_t
		BGl_z62floatzd2ze3ieeezd2stringz81zz__r4_numbers_6_5_flonumz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL double BGl_log2flz00zz__r4_numbers_6_5_flonumz00(double);
	extern obj_t bgl_double_to_ieee_string(double);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_zd3flzd3zz__r4_numbers_6_5_flonumz00(double,
		double);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_ze3flzd2envz31zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762za7e3flza781za7za7__r1638za7,
		BGl_z62ze3flz81zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_atanzd21flzd2envz00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762atanza7d21flza7b01639za7,
		BGl_z62atanzd21flzb0zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_sqrtflzd2envzd2zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762sqrtflza762za7za7__1640z00,
		BGl_z62sqrtflz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_doublezd2ze3ieeezd2stringzd2envz31zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762doubleza7d2za7e3i1641za7,
		BGl_z62doublezd2ze3ieeezd2stringz81zz__r4_numbers_6_5_flonumz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_logflzd2envzd2zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762logflza762za7za7__r1642z00,
		BGl_z62logflz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cosflzd2envzd2zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762cosflza762za7za7__r1643z00,
		BGl_z62cosflz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_signbitflzd2envzd2zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762signbitflza762za71644za7,
		BGl_z62signbitflz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_ieeezd2stringzd2ze3realzd2envz31zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762ieeeza7d2string1645z00,
		BGl_z62ieeezd2stringzd2ze3realz81zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_acosflzd2envzd2zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762acosflza762za7za7__1646z00,
		BGl_z62acosflz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_ze3zd3flzd2envze2zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762za7e3za7d3flza752za7za71647z00,
		BGl_z62ze3zd3flz52zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 2);
	      DEFINE_REAL(BGl_real1606z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_real1606za700za7za7__r1648za7, 0.0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_ieeezd2stringzd2ze3floatzd2envz31zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762ieeeza7d2string1649z00,
		BGl_z62ieeezd2stringzd2ze3floatz81zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_positiveflzf3zd2envz21zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762positiveflza7f31650z00,
		BGl_z62positiveflzf3z91zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_ceilingflzd2envzd2zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762ceilingflza762za71651za7,
		BGl_z62ceilingflz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_zc3flzd2envz11zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762za7c3flza7a1za7za7__r1652za7,
		BGl_z62zc3flza1zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_floorflzd2envzd2zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762floorflza762za7za7_1653z00,
		BGl_z62floorflz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_truncateflzd2envzd2zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762truncateflza7621654z00,
		BGl_z62truncateflz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_integerflzf3zd2envz21zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762integerflza7f3za71655za7,
		BGl_z62integerflzf3z91zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1600z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1600za700za7za7_1656za7, "&asinfl", 7);
	      DEFINE_STRING(BGl_string1601z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1601za700za7za7_1657za7, "&acosfl", 7);
	      DEFINE_STRING(BGl_string1602z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1602za700za7za7_1658za7, "&atanfl", 7);
	      DEFINE_STRING(BGl_string1603z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1603za700za7za7_1659za7, "&atan-1fl", 9);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_zc3zd3flzd2envzc2zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762za7c3za7d3flza772za7za71660z00,
		BGl_z62zc3zd3flz72zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1604z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1604za700za7za7_1661za7, "atanfl", 6);
	      DEFINE_STRING(BGl_string1605z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1605za700za7za7_1662za7, "Domain error", 12);
	      DEFINE_STRING(BGl_string1607z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1607za700za7za7_1663za7, "&atan-2fl", 9);
	      DEFINE_STRING(BGl_string1608z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1608za700za7za7_1664za7, "&atan-2fl-ur", 12);
	      DEFINE_STRING(BGl_string1609z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1609za700za7za7_1665za7, "sqrtfl", 6);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_minzd22flzd2envz00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762minza7d22flza7b0za71666z00,
		BGl_z62minzd22flzb0zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_doublezd2ze3llongzd2bitszd2envz31zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762doubleza7d2za7e3l1667za7,
		BGl_z62doublezd2ze3llongzd2bitsz81zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_zf2flzd2envz20zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762za7f2flza790za7za7__r1668za7,
		BGl_z62zf2flz90zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_log2flzd2envzd2zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762log2flza762za7za7__1669z00,
		BGl_z62log2flz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_oddflzf3zd2envz21zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762oddflza7f3za791za7za71670za7,
		BGl_z62oddflzf3z91zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_absflzd2envzd2zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762absflza762za7za7__r1671z00,
		BGl_z62absflz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1610z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1610za700za7za7_1672za7, "&sqrtfl", 7);
	      DEFINE_STRING(BGl_string1611z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1611za700za7za7_1673za7, "&sqrtfl-ur", 10);
	      DEFINE_STRING(BGl_string1612z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1612za700za7za7_1674za7, "&exptfl", 7);
	      DEFINE_STRING(BGl_string1613z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1613za700za7za7_1675za7, "&signbitfl", 10);
	      DEFINE_STRING(BGl_string1614z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1614za700za7za7_1676za7, "&integerfl?", 11);
	      DEFINE_STRING(BGl_string1615z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1615za700za7za7_1677za7, "&evenfl?", 8);
	      DEFINE_STRING(BGl_string1616z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1616za700za7za7_1678za7, "&oddfl?", 7);
	      DEFINE_STRING(BGl_string1617z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1617za700za7za7_1679za7, "&finitefl?", 10);
	      DEFINE_STRING(BGl_string1618z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1618za700za7za7_1680za7, "&infinitefl?", 12);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_za7eroflzf3zd2envz86zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762za7a7eroflza7f3za731681z00,
		BGl_z62za7eroflzf3z36zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1619z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1619za700za7za7_1682za7, "&nanfl?", 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_atanzd22flzd2envz00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762atanza7d22flza7b01683za7,
		BGl_z62atanzd22flzb0zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1620z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1620za700za7za7_1684za7, "+nan.0", 6);
	      DEFINE_STRING(BGl_string1621z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1621za700za7za7_1685za7, "+inf.0", 6);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_intzd2bitszd2ze3floatzd2envz31zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762intza7d2bitsza7d21686za7,
		BGl_z62intzd2bitszd2ze3floatz81zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string1622z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1622za700za7za7_1687za7, "-inf.0", 6);
	      DEFINE_STRING(BGl_string1623z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1623za700za7za7_1688za7, "&string->real", 13);
	      DEFINE_STRING(BGl_string1624z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1624za700za7za7_1689za7, "bstring", 7);
	      DEFINE_STRING(BGl_string1625z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1625za700za7za7_1690za7, "&ieee-string->real", 18);
	      DEFINE_STRING(BGl_string1626z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1626za700za7za7_1691za7, "&real->ieee-string", 18);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_finiteflzf3zd2envz21zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762finiteflza7f3za791692za7,
		BGl_z62finiteflzf3z91zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1627z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1627za700za7za7_1693za7, "&ieee-string->double", 20);
	      DEFINE_STRING(BGl_string1628z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1628za700za7za7_1694za7, "&double->ieee-string", 20);
	      DEFINE_STRING(BGl_string1629z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1629za700za7za7_1695za7, "&ieee-string->float", 19);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_za2flzd2envz70zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762za7a2flza7c0za7za7__r1696za7,
		BGl_z62za2flzc0zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1630z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1630za700za7za7_1697za7, "&float->ieee-string", 19);
	      DEFINE_STRING(BGl_string1631z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1631za700za7za7_1698za7, "&double->llong-bits", 19);
	      DEFINE_STRING(BGl_string1632z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1632za700za7za7_1699za7, "&llong-bits->double", 19);
	      DEFINE_STRING(BGl_string1633z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1633za700za7za7_1700za7, "bllong", 6);
	      DEFINE_STRING(BGl_string1634z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1634za700za7za7_1701za7, "&float->int-bits", 16);
	      DEFINE_STRING(BGl_string1635z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1635za700za7za7_1702za7, "&int-bits->float", 16);
	      DEFINE_STRING(BGl_string1636z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1636za700za7za7_1703za7, "bint", 4);
	      DEFINE_STRING(BGl_string1637z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1637za700za7za7_1704za7, "__r4_numbers_6_5_flonum", 23);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2ze3realzd2envze3zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762stringza7d2za7e3r1705za7,
		BGl_z62stringzd2ze3realz53zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1568z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1568za700za7za7_1706za7,
		"/tmp/bigloo/runtime/Ieee/flonum.scm", 35);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_tanflzd2envzd2zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762tanflza762za7za7__r1707z00,
		BGl_z62tanflz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_log10flzd2envzd2zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762log10flza762za7za7_1708z00,
		BGl_z62log10flz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1569z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1569za700za7za7_1709za7, "&=fl", 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_minflzd2envzd2zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762minflza762za7za7__r1710z00, va_generic_entry,
		BGl_z62minflz62zz__r4_numbers_6_5_flonumz00, BUNSPEC, -2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_sinflzd2envzd2zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762sinflza762za7za7__r1711z00,
		BGl_z62sinflz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_zd2flzd2envz00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762za7d2flza7b0za7za7__r1712za7,
		BGl_z62zd2flzb0zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1570z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1570za700za7za7_1713za7, "real", 4);
	      DEFINE_STRING(BGl_string1571z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1571za700za7za7_1714za7, "&<fl", 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_zd3flzd2envz01zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762za7d3flza7b1za7za7__r1715za7,
		BGl_z62zd3flzb1zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1572z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1572za700za7za7_1716za7, "&>fl", 4);
	      DEFINE_STRING(BGl_string1573z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1573za700za7za7_1717za7, "&<=fl", 5);
	      DEFINE_STRING(BGl_string1574z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1574za700za7za7_1718za7, "&>=fl", 5);
	      DEFINE_STRING(BGl_string1575z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1575za700za7za7_1719za7, "&zerofl?", 8);
	      DEFINE_STRING(BGl_string1576z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1576za700za7za7_1720za7, "&positivefl?", 12);
	      DEFINE_STRING(BGl_string1577z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1577za700za7za7_1721za7, "&negativefl?", 12);
	      DEFINE_STRING(BGl_string1578z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1578za700za7za7_1722za7, "&+fl", 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_atanflzd2envzd2zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762atanflza762za7za7__1723z00, va_generic_entry,
		BGl_z62atanflz62zz__r4_numbers_6_5_flonumz00, BUNSPEC, -2);
	      DEFINE_STRING(BGl_string1579z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1579za700za7za7_1724za7, "&-fl", 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_asinflzd2envzd2zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762asinflza762za7za7__1725z00,
		BGl_z62asinflz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_ieeezd2stringzd2ze3doublezd2envz31zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762ieeeza7d2string1726z00,
		BGl_z62ieeezd2stringzd2ze3doublez81zz__r4_numbers_6_5_flonumz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_remainderflzd2envzd2zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762remainderflza761727z00,
		BGl_z62remainderflz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_realzd2ze3ieeezd2stringzd2envz31zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762realza7d2za7e3iee1728za7,
		BGl_z62realzd2ze3ieeezd2stringz81zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_negflzd2envzd2zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762negflza762za7za7__r1729z00,
		BGl_z62negflz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_nanflzf3zd2envz21zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762nanflza7f3za791za7za71730za7,
		BGl_z62nanflzf3z91zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1580z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1580za700za7za7_1731za7, "&*fl", 4);
	      DEFINE_STRING(BGl_string1581z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1581za700za7za7_1732za7, "&/fl", 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sqrtflzd2urzd2envz00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762sqrtflza7d2urza7b1733za7,
		BGl_z62sqrtflzd2urzb0zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1582z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1582za700za7za7_1734za7, "&negfl", 6);
	      DEFINE_STRING(BGl_string1583z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1583za700za7za7_1735za7, "&maxfl", 6);
	      DEFINE_STRING(BGl_string1584z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1584za700za7za7_1736za7, "&max-2fl", 8);
	      DEFINE_STRING(BGl_string1585z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1585za700za7za7_1737za7, "&min-2fl", 8);
	      DEFINE_STRING(BGl_string1586z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1586za700za7za7_1738za7, "&minfl", 6);
	      DEFINE_STRING(BGl_string1587z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1587za700za7za7_1739za7, "&absfl", 6);
	      DEFINE_STRING(BGl_string1588z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1588za700za7za7_1740za7, "&floorfl", 8);
	      DEFINE_STRING(BGl_string1589z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1589za700za7za7_1741za7, "&ceilingfl", 10);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_infiniteflzf3zd2envz21zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762infiniteflza7f31742z00,
		BGl_z62infiniteflzf3z91zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_atanzd22flzd2urzd2envzd2zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762atanza7d22flza7d21743za7,
		BGl_z62atanzd22flzd2urz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1590z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1590za700za7za7_1744za7, "&truncatefl", 11);
	      DEFINE_STRING(BGl_string1591z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1591za700za7za7_1745za7, "&roundfl", 8);
	      DEFINE_STRING(BGl_string1592z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1592za700za7za7_1746za7, "&remainderfl", 12);
	      DEFINE_STRING(BGl_string1593z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1593za700za7za7_1747za7, "&expfl", 6);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_randomflzd2envzd2zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762randomflza762za7za71748z00,
		BGl_z62randomflz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_exptflzd2envzd2zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762exptflza762za7za7__1749z00,
		BGl_z62exptflz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1594z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1594za700za7za7_1750za7, "&logfl", 6);
	      DEFINE_STRING(BGl_string1595z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1595za700za7za7_1751za7, "&log2fl", 7);
	      DEFINE_STRING(BGl_string1596z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1596za700za7za7_1752za7, "&log10fl", 8);
	      DEFINE_STRING(BGl_string1597z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1597za700za7za7_1753za7, "&sinfl", 6);
	      DEFINE_STRING(BGl_string1598z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1598za700za7za7_1754za7, "&cosfl", 6);
	      DEFINE_STRING(BGl_string1599z00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_string1599za700za7za7_1755za7, "&tanfl", 6);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_roundflzd2envzd2zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762roundflza762za7za7_1756z00,
		BGl_z62roundflz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_negativeflzf3zd2envz21zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762negativeflza7f31757z00,
		BGl_z62negativeflzf3z91zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_flonumzf3zd2envz21zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762flonumza7f3za791za71758z00,
		BGl_z62flonumzf3z91zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_maxzd22flzd2envz00zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762maxza7d22flza7b0za71759z00,
		BGl_z62maxzd22flzb0zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_maxflzd2envzd2zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762maxflza762za7za7__r1760z00, va_generic_entry,
		BGl_z62maxflz62zz__r4_numbers_6_5_flonumz00, BUNSPEC, -2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_zb2flzd2envz60zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762za7b2flza7d0za7za7__r1761za7,
		BGl_z62zb2flzd0zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_evenflzf3zd2envz21zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762evenflza7f3za791za71762z00,
		BGl_z62evenflzf3z91zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_llongzd2bitszd2ze3doublezd2envz31zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762llongza7d2bitsza71763za7,
		BGl_z62llongzd2bitszd2ze3doublez81zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_floatzd2ze3intzd2bitszd2envz31zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762floatza7d2za7e3in1764za7,
		BGl_z62floatzd2ze3intzd2bitsz81zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_expflzd2envzd2zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762expflza762za7za7__r1765z00,
		BGl_z62expflz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_floatzd2ze3ieeezd2stringzd2envz31zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762floatza7d2za7e3ie1766za7,
		BGl_z62floatzd2ze3ieeezd2stringz81zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_realzf3zd2envz21zz__r4_numbers_6_5_flonumz00,
		BgL_bgl_za762realza7f3za791za7za7_1767za7,
		BGl_z62realzf3z91zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long
		BgL_checksumz00_1564, char *BgL_fromz00_1565)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00))
				{
					BGl_requirezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__r4_numbers_6_5_flonumz00();
					return BGl_importedzd2moduleszd2initz00zz__r4_numbers_6_5_flonumz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__r4_numbers_6_5_flonumz00(void)
	{
		{	/* Ieee/flonum.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* real? */
	BGL_EXPORTED_DEF bool_t BGl_realzf3zf3zz__r4_numbers_6_5_flonumz00(obj_t
		BgL_objz00_3)
	{
		{	/* Ieee/flonum.scm 316 */
			if (INTEGERP(BgL_objz00_3))
				{	/* Ieee/flonum.scm 317 */
					return ((bool_t) 1);
				}
			else
				{	/* Ieee/flonum.scm 317 */
					return REALP(BgL_objz00_3);
				}
		}

	}



/* &real? */
	obj_t BGl_z62realzf3z91zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1263,
		obj_t BgL_objz00_1264)
	{
		{	/* Ieee/flonum.scm 316 */
			return BBOOL(BGl_realzf3zf3zz__r4_numbers_6_5_flonumz00(BgL_objz00_1264));
		}

	}



/* flonum? */
	BGL_EXPORTED_DEF bool_t BGl_flonumzf3zf3zz__r4_numbers_6_5_flonumz00(obj_t
		BgL_objz00_4)
	{
		{	/* Ieee/flonum.scm 324 */
			return REALP(BgL_objz00_4);
		}

	}



/* &flonum? */
	obj_t BGl_z62flonumzf3z91zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1265,
		obj_t BgL_objz00_1266)
	{
		{	/* Ieee/flonum.scm 324 */
			return
				BBOOL(BGl_flonumzf3zf3zz__r4_numbers_6_5_flonumz00(BgL_objz00_1266));
		}

	}



/* =fl */
	BGL_EXPORTED_DEF bool_t BGl_zd3flzd3zz__r4_numbers_6_5_flonumz00(double
		BgL_r1z00_5, double BgL_r2z00_6)
	{
		{	/* Ieee/flonum.scm 330 */
			return (BgL_r1z00_5 == BgL_r2z00_6);
		}

	}



/* &=fl */
	obj_t BGl_z62zd3flzb1zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1267,
		obj_t BgL_r1z00_1268, obj_t BgL_r2z00_1269)
	{
		{	/* Ieee/flonum.scm 330 */
			{	/* Ieee/flonum.scm 331 */
				bool_t BgL_tmpz00_1581;

				{	/* Ieee/flonum.scm 331 */
					double BgL_auxz00_1591;
					double BgL_auxz00_1582;

					{	/* Ieee/flonum.scm 331 */
						obj_t BgL_tmpz00_1592;

						if (REALP(BgL_r2z00_1269))
							{	/* Ieee/flonum.scm 331 */
								BgL_tmpz00_1592 = BgL_r2z00_1269;
							}
						else
							{
								obj_t BgL_auxz00_1595;

								BgL_auxz00_1595 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(15207L),
									BGl_string1569z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00,
									BgL_r2z00_1269);
								FAILURE(BgL_auxz00_1595, BFALSE, BFALSE);
							}
						BgL_auxz00_1591 = REAL_TO_DOUBLE(BgL_tmpz00_1592);
					}
					{	/* Ieee/flonum.scm 331 */
						obj_t BgL_tmpz00_1583;

						if (REALP(BgL_r1z00_1268))
							{	/* Ieee/flonum.scm 331 */
								BgL_tmpz00_1583 = BgL_r1z00_1268;
							}
						else
							{
								obj_t BgL_auxz00_1586;

								BgL_auxz00_1586 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(15207L),
									BGl_string1569z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00,
									BgL_r1z00_1268);
								FAILURE(BgL_auxz00_1586, BFALSE, BFALSE);
							}
						BgL_auxz00_1582 = REAL_TO_DOUBLE(BgL_tmpz00_1583);
					}
					BgL_tmpz00_1581 =
						BGl_zd3flzd3zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1582,
						BgL_auxz00_1591);
				}
				return BBOOL(BgL_tmpz00_1581);
			}
		}

	}



/* <fl */
	BGL_EXPORTED_DEF bool_t BGl_zc3flzc3zz__r4_numbers_6_5_flonumz00(double
		BgL_r1z00_7, double BgL_r2z00_8)
	{
		{	/* Ieee/flonum.scm 336 */
			return (BgL_r1z00_7 < BgL_r2z00_8);
		}

	}



/* &<fl */
	obj_t BGl_z62zc3flza1zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1270,
		obj_t BgL_r1z00_1271, obj_t BgL_r2z00_1272)
	{
		{	/* Ieee/flonum.scm 336 */
			{	/* Ieee/flonum.scm 337 */
				bool_t BgL_tmpz00_1603;

				{	/* Ieee/flonum.scm 337 */
					double BgL_auxz00_1613;
					double BgL_auxz00_1604;

					{	/* Ieee/flonum.scm 337 */
						obj_t BgL_tmpz00_1614;

						if (REALP(BgL_r2z00_1272))
							{	/* Ieee/flonum.scm 337 */
								BgL_tmpz00_1614 = BgL_r2z00_1272;
							}
						else
							{
								obj_t BgL_auxz00_1617;

								BgL_auxz00_1617 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(15474L),
									BGl_string1571z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00,
									BgL_r2z00_1272);
								FAILURE(BgL_auxz00_1617, BFALSE, BFALSE);
							}
						BgL_auxz00_1613 = REAL_TO_DOUBLE(BgL_tmpz00_1614);
					}
					{	/* Ieee/flonum.scm 337 */
						obj_t BgL_tmpz00_1605;

						if (REALP(BgL_r1z00_1271))
							{	/* Ieee/flonum.scm 337 */
								BgL_tmpz00_1605 = BgL_r1z00_1271;
							}
						else
							{
								obj_t BgL_auxz00_1608;

								BgL_auxz00_1608 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(15474L),
									BGl_string1571z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00,
									BgL_r1z00_1271);
								FAILURE(BgL_auxz00_1608, BFALSE, BFALSE);
							}
						BgL_auxz00_1604 = REAL_TO_DOUBLE(BgL_tmpz00_1605);
					}
					BgL_tmpz00_1603 =
						BGl_zc3flzc3zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1604,
						BgL_auxz00_1613);
				}
				return BBOOL(BgL_tmpz00_1603);
			}
		}

	}



/* >fl */
	BGL_EXPORTED_DEF bool_t BGl_ze3flze3zz__r4_numbers_6_5_flonumz00(double
		BgL_r1z00_9, double BgL_r2z00_10)
	{
		{	/* Ieee/flonum.scm 342 */
			return (BgL_r1z00_9 > BgL_r2z00_10);
		}

	}



/* &>fl */
	obj_t BGl_z62ze3flz81zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1273,
		obj_t BgL_r1z00_1274, obj_t BgL_r2z00_1275)
	{
		{	/* Ieee/flonum.scm 342 */
			{	/* Ieee/flonum.scm 343 */
				bool_t BgL_tmpz00_1625;

				{	/* Ieee/flonum.scm 343 */
					double BgL_auxz00_1635;
					double BgL_auxz00_1626;

					{	/* Ieee/flonum.scm 343 */
						obj_t BgL_tmpz00_1636;

						if (REALP(BgL_r2z00_1275))
							{	/* Ieee/flonum.scm 343 */
								BgL_tmpz00_1636 = BgL_r2z00_1275;
							}
						else
							{
								obj_t BgL_auxz00_1639;

								BgL_auxz00_1639 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(15741L),
									BGl_string1572z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00,
									BgL_r2z00_1275);
								FAILURE(BgL_auxz00_1639, BFALSE, BFALSE);
							}
						BgL_auxz00_1635 = REAL_TO_DOUBLE(BgL_tmpz00_1636);
					}
					{	/* Ieee/flonum.scm 343 */
						obj_t BgL_tmpz00_1627;

						if (REALP(BgL_r1z00_1274))
							{	/* Ieee/flonum.scm 343 */
								BgL_tmpz00_1627 = BgL_r1z00_1274;
							}
						else
							{
								obj_t BgL_auxz00_1630;

								BgL_auxz00_1630 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(15741L),
									BGl_string1572z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00,
									BgL_r1z00_1274);
								FAILURE(BgL_auxz00_1630, BFALSE, BFALSE);
							}
						BgL_auxz00_1626 = REAL_TO_DOUBLE(BgL_tmpz00_1627);
					}
					BgL_tmpz00_1625 =
						BGl_ze3flze3zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1626,
						BgL_auxz00_1635);
				}
				return BBOOL(BgL_tmpz00_1625);
			}
		}

	}



/* <=fl */
	BGL_EXPORTED_DEF bool_t BGl_zc3zd3flz10zz__r4_numbers_6_5_flonumz00(double
		BgL_r1z00_11, double BgL_r2z00_12)
	{
		{	/* Ieee/flonum.scm 348 */
			return (BgL_r1z00_11 <= BgL_r2z00_12);
		}

	}



/* &<=fl */
	obj_t BGl_z62zc3zd3flz72zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1276,
		obj_t BgL_r1z00_1277, obj_t BgL_r2z00_1278)
	{
		{	/* Ieee/flonum.scm 348 */
			{	/* Ieee/flonum.scm 349 */
				bool_t BgL_tmpz00_1647;

				{	/* Ieee/flonum.scm 349 */
					double BgL_auxz00_1657;
					double BgL_auxz00_1648;

					{	/* Ieee/flonum.scm 349 */
						obj_t BgL_tmpz00_1658;

						if (REALP(BgL_r2z00_1278))
							{	/* Ieee/flonum.scm 349 */
								BgL_tmpz00_1658 = BgL_r2z00_1278;
							}
						else
							{
								obj_t BgL_auxz00_1661;

								BgL_auxz00_1661 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(16009L),
									BGl_string1573z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00,
									BgL_r2z00_1278);
								FAILURE(BgL_auxz00_1661, BFALSE, BFALSE);
							}
						BgL_auxz00_1657 = REAL_TO_DOUBLE(BgL_tmpz00_1658);
					}
					{	/* Ieee/flonum.scm 349 */
						obj_t BgL_tmpz00_1649;

						if (REALP(BgL_r1z00_1277))
							{	/* Ieee/flonum.scm 349 */
								BgL_tmpz00_1649 = BgL_r1z00_1277;
							}
						else
							{
								obj_t BgL_auxz00_1652;

								BgL_auxz00_1652 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(16009L),
									BGl_string1573z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00,
									BgL_r1z00_1277);
								FAILURE(BgL_auxz00_1652, BFALSE, BFALSE);
							}
						BgL_auxz00_1648 = REAL_TO_DOUBLE(BgL_tmpz00_1649);
					}
					BgL_tmpz00_1647 =
						BGl_zc3zd3flz10zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1648,
						BgL_auxz00_1657);
				}
				return BBOOL(BgL_tmpz00_1647);
			}
		}

	}



/* >=fl */
	BGL_EXPORTED_DEF bool_t BGl_ze3zd3flz30zz__r4_numbers_6_5_flonumz00(double
		BgL_r1z00_13, double BgL_r2z00_14)
	{
		{	/* Ieee/flonum.scm 354 */
			return (BgL_r1z00_13 >= BgL_r2z00_14);
		}

	}



/* &>=fl */
	obj_t BGl_z62ze3zd3flz52zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1279,
		obj_t BgL_r1z00_1280, obj_t BgL_r2z00_1281)
	{
		{	/* Ieee/flonum.scm 354 */
			{	/* Ieee/flonum.scm 355 */
				bool_t BgL_tmpz00_1669;

				{	/* Ieee/flonum.scm 355 */
					double BgL_auxz00_1679;
					double BgL_auxz00_1670;

					{	/* Ieee/flonum.scm 355 */
						obj_t BgL_tmpz00_1680;

						if (REALP(BgL_r2z00_1281))
							{	/* Ieee/flonum.scm 355 */
								BgL_tmpz00_1680 = BgL_r2z00_1281;
							}
						else
							{
								obj_t BgL_auxz00_1683;

								BgL_auxz00_1683 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(16278L),
									BGl_string1574z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00,
									BgL_r2z00_1281);
								FAILURE(BgL_auxz00_1683, BFALSE, BFALSE);
							}
						BgL_auxz00_1679 = REAL_TO_DOUBLE(BgL_tmpz00_1680);
					}
					{	/* Ieee/flonum.scm 355 */
						obj_t BgL_tmpz00_1671;

						if (REALP(BgL_r1z00_1280))
							{	/* Ieee/flonum.scm 355 */
								BgL_tmpz00_1671 = BgL_r1z00_1280;
							}
						else
							{
								obj_t BgL_auxz00_1674;

								BgL_auxz00_1674 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(16278L),
									BGl_string1574z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00,
									BgL_r1z00_1280);
								FAILURE(BgL_auxz00_1674, BFALSE, BFALSE);
							}
						BgL_auxz00_1670 = REAL_TO_DOUBLE(BgL_tmpz00_1671);
					}
					BgL_tmpz00_1669 =
						BGl_ze3zd3flz30zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1670,
						BgL_auxz00_1679);
				}
				return BBOOL(BgL_tmpz00_1669);
			}
		}

	}



/* zerofl? */
	BGL_EXPORTED_DEF bool_t BGl_za7eroflzf3z54zz__r4_numbers_6_5_flonumz00(double
		BgL_rz00_15)
	{
		{	/* Ieee/flonum.scm 360 */
			return (BgL_rz00_15 == ((double) 0.0));
		}

	}



/* &zerofl? */
	obj_t BGl_z62za7eroflzf3z36zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1282,
		obj_t BgL_rz00_1283)
	{
		{	/* Ieee/flonum.scm 360 */
			{	/* Ieee/flonum.scm 331 */
				bool_t BgL_tmpz00_1691;

				{	/* Ieee/flonum.scm 331 */
					double BgL_auxz00_1692;

					{	/* Ieee/flonum.scm 331 */
						obj_t BgL_tmpz00_1693;

						if (REALP(BgL_rz00_1283))
							{	/* Ieee/flonum.scm 331 */
								BgL_tmpz00_1693 = BgL_rz00_1283;
							}
						else
							{
								obj_t BgL_auxz00_1696;

								BgL_auxz00_1696 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(15207L),
									BGl_string1575z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00, BgL_rz00_1283);
								FAILURE(BgL_auxz00_1696, BFALSE, BFALSE);
							}
						BgL_auxz00_1692 = REAL_TO_DOUBLE(BgL_tmpz00_1693);
					}
					BgL_tmpz00_1691 =
						BGl_za7eroflzf3z54zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1692);
				}
				return BBOOL(BgL_tmpz00_1691);
			}
		}

	}



/* positivefl? */
	BGL_EXPORTED_DEF bool_t
		BGl_positiveflzf3zf3zz__r4_numbers_6_5_flonumz00(double BgL_rz00_16)
	{
		{	/* Ieee/flonum.scm 366 */
			return (BgL_rz00_16 > ((double) 0.0));
		}

	}



/* &positivefl? */
	obj_t BGl_z62positiveflzf3z91zz__r4_numbers_6_5_flonumz00(obj_t
		BgL_envz00_1284, obj_t BgL_rz00_1285)
	{
		{	/* Ieee/flonum.scm 366 */
			{	/* Ieee/flonum.scm 343 */
				bool_t BgL_tmpz00_1704;

				{	/* Ieee/flonum.scm 343 */
					double BgL_auxz00_1705;

					{	/* Ieee/flonum.scm 343 */
						obj_t BgL_tmpz00_1706;

						if (REALP(BgL_rz00_1285))
							{	/* Ieee/flonum.scm 343 */
								BgL_tmpz00_1706 = BgL_rz00_1285;
							}
						else
							{
								obj_t BgL_auxz00_1709;

								BgL_auxz00_1709 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(15741L),
									BGl_string1576z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00, BgL_rz00_1285);
								FAILURE(BgL_auxz00_1709, BFALSE, BFALSE);
							}
						BgL_auxz00_1705 = REAL_TO_DOUBLE(BgL_tmpz00_1706);
					}
					BgL_tmpz00_1704 =
						BGl_positiveflzf3zf3zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1705);
				}
				return BBOOL(BgL_tmpz00_1704);
			}
		}

	}



/* negativefl? */
	BGL_EXPORTED_DEF bool_t
		BGl_negativeflzf3zf3zz__r4_numbers_6_5_flonumz00(double BgL_rz00_17)
	{
		{	/* Ieee/flonum.scm 372 */
			return (BgL_rz00_17 < ((double) 0.0));
		}

	}



/* &negativefl? */
	obj_t BGl_z62negativeflzf3z91zz__r4_numbers_6_5_flonumz00(obj_t
		BgL_envz00_1286, obj_t BgL_rz00_1287)
	{
		{	/* Ieee/flonum.scm 372 */
			{	/* Ieee/flonum.scm 337 */
				bool_t BgL_tmpz00_1717;

				{	/* Ieee/flonum.scm 337 */
					double BgL_auxz00_1718;

					{	/* Ieee/flonum.scm 337 */
						obj_t BgL_tmpz00_1719;

						if (REALP(BgL_rz00_1287))
							{	/* Ieee/flonum.scm 337 */
								BgL_tmpz00_1719 = BgL_rz00_1287;
							}
						else
							{
								obj_t BgL_auxz00_1722;

								BgL_auxz00_1722 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(15474L),
									BGl_string1577z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00, BgL_rz00_1287);
								FAILURE(BgL_auxz00_1722, BFALSE, BFALSE);
							}
						BgL_auxz00_1718 = REAL_TO_DOUBLE(BgL_tmpz00_1719);
					}
					BgL_tmpz00_1717 =
						BGl_negativeflzf3zf3zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1718);
				}
				return BBOOL(BgL_tmpz00_1717);
			}
		}

	}



/* +fl */
	BGL_EXPORTED_DEF double BGl_zb2flzb2zz__r4_numbers_6_5_flonumz00(double
		BgL_r1z00_18, double BgL_r2z00_19)
	{
		{	/* Ieee/flonum.scm 378 */
			return (BgL_r1z00_18 + BgL_r2z00_19);
		}

	}



/* &+fl */
	obj_t BGl_z62zb2flzd0zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1288,
		obj_t BgL_r1z00_1289, obj_t BgL_r2z00_1290)
	{
		{	/* Ieee/flonum.scm 378 */
			{	/* Ieee/flonum.scm 379 */
				double BgL_tmpz00_1730;

				{	/* Ieee/flonum.scm 379 */
					double BgL_auxz00_1740;
					double BgL_auxz00_1731;

					{	/* Ieee/flonum.scm 379 */
						obj_t BgL_tmpz00_1741;

						if (REALP(BgL_r2z00_1290))
							{	/* Ieee/flonum.scm 379 */
								BgL_tmpz00_1741 = BgL_r2z00_1290;
							}
						else
							{
								obj_t BgL_auxz00_1744;

								BgL_auxz00_1744 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(17352L),
									BGl_string1578z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00,
									BgL_r2z00_1290);
								FAILURE(BgL_auxz00_1744, BFALSE, BFALSE);
							}
						BgL_auxz00_1740 = REAL_TO_DOUBLE(BgL_tmpz00_1741);
					}
					{	/* Ieee/flonum.scm 379 */
						obj_t BgL_tmpz00_1732;

						if (REALP(BgL_r1z00_1289))
							{	/* Ieee/flonum.scm 379 */
								BgL_tmpz00_1732 = BgL_r1z00_1289;
							}
						else
							{
								obj_t BgL_auxz00_1735;

								BgL_auxz00_1735 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(17352L),
									BGl_string1578z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00,
									BgL_r1z00_1289);
								FAILURE(BgL_auxz00_1735, BFALSE, BFALSE);
							}
						BgL_auxz00_1731 = REAL_TO_DOUBLE(BgL_tmpz00_1732);
					}
					BgL_tmpz00_1730 =
						BGl_zb2flzb2zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1731,
						BgL_auxz00_1740);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_1730);
			}
		}

	}



/* -fl */
	BGL_EXPORTED_DEF double BGl_zd2flzd2zz__r4_numbers_6_5_flonumz00(double
		BgL_r1z00_20, double BgL_r2z00_21)
	{
		{	/* Ieee/flonum.scm 380 */
			return (BgL_r1z00_20 - BgL_r2z00_21);
		}

	}



/* &-fl */
	obj_t BGl_z62zd2flzb0zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1291,
		obj_t BgL_r1z00_1292, obj_t BgL_r2z00_1293)
	{
		{	/* Ieee/flonum.scm 380 */
			{	/* Ieee/flonum.scm 381 */
				double BgL_tmpz00_1752;

				{	/* Ieee/flonum.scm 381 */
					double BgL_auxz00_1762;
					double BgL_auxz00_1753;

					{	/* Ieee/flonum.scm 381 */
						obj_t BgL_tmpz00_1763;

						if (REALP(BgL_r2z00_1293))
							{	/* Ieee/flonum.scm 381 */
								BgL_tmpz00_1763 = BgL_r2z00_1293;
							}
						else
							{
								obj_t BgL_auxz00_1766;

								BgL_auxz00_1766 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(17396L),
									BGl_string1579z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00,
									BgL_r2z00_1293);
								FAILURE(BgL_auxz00_1766, BFALSE, BFALSE);
							}
						BgL_auxz00_1762 = REAL_TO_DOUBLE(BgL_tmpz00_1763);
					}
					{	/* Ieee/flonum.scm 381 */
						obj_t BgL_tmpz00_1754;

						if (REALP(BgL_r1z00_1292))
							{	/* Ieee/flonum.scm 381 */
								BgL_tmpz00_1754 = BgL_r1z00_1292;
							}
						else
							{
								obj_t BgL_auxz00_1757;

								BgL_auxz00_1757 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(17396L),
									BGl_string1579z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00,
									BgL_r1z00_1292);
								FAILURE(BgL_auxz00_1757, BFALSE, BFALSE);
							}
						BgL_auxz00_1753 = REAL_TO_DOUBLE(BgL_tmpz00_1754);
					}
					BgL_tmpz00_1752 =
						BGl_zd2flzd2zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1753,
						BgL_auxz00_1762);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_1752);
			}
		}

	}



/* *fl */
	BGL_EXPORTED_DEF double BGl_za2flza2zz__r4_numbers_6_5_flonumz00(double
		BgL_r1z00_22, double BgL_r2z00_23)
	{
		{	/* Ieee/flonum.scm 382 */
			return (BgL_r1z00_22 * BgL_r2z00_23);
		}

	}



/* &*fl */
	obj_t BGl_z62za2flzc0zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1294,
		obj_t BgL_r1z00_1295, obj_t BgL_r2z00_1296)
	{
		{	/* Ieee/flonum.scm 382 */
			{	/* Ieee/flonum.scm 383 */
				double BgL_tmpz00_1774;

				{	/* Ieee/flonum.scm 383 */
					double BgL_auxz00_1784;
					double BgL_auxz00_1775;

					{	/* Ieee/flonum.scm 383 */
						obj_t BgL_tmpz00_1785;

						if (REALP(BgL_r2z00_1296))
							{	/* Ieee/flonum.scm 383 */
								BgL_tmpz00_1785 = BgL_r2z00_1296;
							}
						else
							{
								obj_t BgL_auxz00_1788;

								BgL_auxz00_1788 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(17440L),
									BGl_string1580z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00,
									BgL_r2z00_1296);
								FAILURE(BgL_auxz00_1788, BFALSE, BFALSE);
							}
						BgL_auxz00_1784 = REAL_TO_DOUBLE(BgL_tmpz00_1785);
					}
					{	/* Ieee/flonum.scm 383 */
						obj_t BgL_tmpz00_1776;

						if (REALP(BgL_r1z00_1295))
							{	/* Ieee/flonum.scm 383 */
								BgL_tmpz00_1776 = BgL_r1z00_1295;
							}
						else
							{
								obj_t BgL_auxz00_1779;

								BgL_auxz00_1779 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(17440L),
									BGl_string1580z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00,
									BgL_r1z00_1295);
								FAILURE(BgL_auxz00_1779, BFALSE, BFALSE);
							}
						BgL_auxz00_1775 = REAL_TO_DOUBLE(BgL_tmpz00_1776);
					}
					BgL_tmpz00_1774 =
						BGl_za2flza2zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1775,
						BgL_auxz00_1784);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_1774);
			}
		}

	}



/* /fl */
	BGL_EXPORTED_DEF double BGl_zf2flzf2zz__r4_numbers_6_5_flonumz00(double
		BgL_r1z00_24, double BgL_r2z00_25)
	{
		{	/* Ieee/flonum.scm 384 */
			return (BgL_r1z00_24 / BgL_r2z00_25);
		}

	}



/* &/fl */
	obj_t BGl_z62zf2flz90zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1297,
		obj_t BgL_r1z00_1298, obj_t BgL_r2z00_1299)
	{
		{	/* Ieee/flonum.scm 384 */
			{	/* Ieee/flonum.scm 385 */
				double BgL_tmpz00_1796;

				{	/* Ieee/flonum.scm 385 */
					double BgL_auxz00_1806;
					double BgL_auxz00_1797;

					{	/* Ieee/flonum.scm 385 */
						obj_t BgL_tmpz00_1807;

						if (REALP(BgL_r2z00_1299))
							{	/* Ieee/flonum.scm 385 */
								BgL_tmpz00_1807 = BgL_r2z00_1299;
							}
						else
							{
								obj_t BgL_auxz00_1810;

								BgL_auxz00_1810 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(17484L),
									BGl_string1581z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00,
									BgL_r2z00_1299);
								FAILURE(BgL_auxz00_1810, BFALSE, BFALSE);
							}
						BgL_auxz00_1806 = REAL_TO_DOUBLE(BgL_tmpz00_1807);
					}
					{	/* Ieee/flonum.scm 385 */
						obj_t BgL_tmpz00_1798;

						if (REALP(BgL_r1z00_1298))
							{	/* Ieee/flonum.scm 385 */
								BgL_tmpz00_1798 = BgL_r1z00_1298;
							}
						else
							{
								obj_t BgL_auxz00_1801;

								BgL_auxz00_1801 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(17484L),
									BGl_string1581z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00,
									BgL_r1z00_1298);
								FAILURE(BgL_auxz00_1801, BFALSE, BFALSE);
							}
						BgL_auxz00_1797 = REAL_TO_DOUBLE(BgL_tmpz00_1798);
					}
					BgL_tmpz00_1796 =
						BGl_zf2flzf2zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1797,
						BgL_auxz00_1806);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_1796);
			}
		}

	}



/* negfl */
	BGL_EXPORTED_DEF double BGl_negflz00zz__r4_numbers_6_5_flonumz00(double
		BgL_r1z00_26)
	{
		{	/* Ieee/flonum.scm 390 */
			return NEG(BgL_r1z00_26);
		}

	}



/* &negfl */
	obj_t BGl_z62negflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1300,
		obj_t BgL_r1z00_1301)
	{
		{	/* Ieee/flonum.scm 390 */
			{	/* Ieee/flonum.scm 391 */
				double BgL_tmpz00_1818;

				{	/* Ieee/flonum.scm 391 */
					double BgL_auxz00_1819;

					{	/* Ieee/flonum.scm 391 */
						obj_t BgL_tmpz00_1820;

						if (REALP(BgL_r1z00_1301))
							{	/* Ieee/flonum.scm 391 */
								BgL_tmpz00_1820 = BgL_r1z00_1301;
							}
						else
							{
								obj_t BgL_auxz00_1823;

								BgL_auxz00_1823 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(17750L),
									BGl_string1582z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00,
									BgL_r1z00_1301);
								FAILURE(BgL_auxz00_1823, BFALSE, BFALSE);
							}
						BgL_auxz00_1819 = REAL_TO_DOUBLE(BgL_tmpz00_1820);
					}
					BgL_tmpz00_1818 =
						BGl_negflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1819);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_1818);
			}
		}

	}



/* maxfl */
	BGL_EXPORTED_DEF double BGl_maxflz00zz__r4_numbers_6_5_flonumz00(double
		BgL_r1z00_27, obj_t BgL_rnz00_28)
	{
		{	/* Ieee/flonum.scm 396 */
			{
				double BgL_maxz00_1051;
				obj_t BgL_rnz00_1052;

				BgL_maxz00_1051 = BgL_r1z00_27;
				BgL_rnz00_1052 = BgL_rnz00_28;
			BgL_loopz00_1050:
				if (NULLP(BgL_rnz00_1052))
					{	/* Ieee/flonum.scm 399 */
						return BgL_maxz00_1051;
					}
				else
					{
						obj_t BgL_rnz00_1837;
						double BgL_maxz00_1832;

						BgL_maxz00_1832 =
							BGL_FL_MAX2(REAL_TO_DOUBLE(CAR(
									((obj_t) BgL_rnz00_1052))), BgL_maxz00_1051);
						BgL_rnz00_1837 = CDR(((obj_t) BgL_rnz00_1052));
						BgL_rnz00_1052 = BgL_rnz00_1837;
						BgL_maxz00_1051 = BgL_maxz00_1832;
						goto BgL_loopz00_1050;
					}
			}
		}

	}



/* &maxfl */
	obj_t BGl_z62maxflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1302,
		obj_t BgL_r1z00_1303, obj_t BgL_rnz00_1304)
	{
		{	/* Ieee/flonum.scm 396 */
			{	/* Ieee/flonum.scm 397 */
				double BgL_tmpz00_1840;

				{	/* Ieee/flonum.scm 397 */
					double BgL_auxz00_1841;

					{	/* Ieee/flonum.scm 397 */
						obj_t BgL_tmpz00_1842;

						if (REALP(BgL_r1z00_1303))
							{	/* Ieee/flonum.scm 397 */
								BgL_tmpz00_1842 = BgL_r1z00_1303;
							}
						else
							{
								obj_t BgL_auxz00_1845;

								BgL_auxz00_1845 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(18017L),
									BGl_string1583z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00,
									BgL_r1z00_1303);
								FAILURE(BgL_auxz00_1845, BFALSE, BFALSE);
							}
						BgL_auxz00_1841 = REAL_TO_DOUBLE(BgL_tmpz00_1842);
					}
					BgL_tmpz00_1840 =
						BGl_maxflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1841,
						BgL_rnz00_1304);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_1840);
			}
		}

	}



/* max-2fl */
	BGL_EXPORTED_DEF double BGl_maxzd22flzd2zz__r4_numbers_6_5_flonumz00(double
		BgL_r1z00_29, double BgL_r2z00_30)
	{
		{	/* Ieee/flonum.scm 406 */
			return BGL_FL_MAX2(BgL_r1z00_29, BgL_r2z00_30);
		}

	}



/* &max-2fl */
	obj_t BGl_z62maxzd22flzb0zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1305,
		obj_t BgL_r1z00_1306, obj_t BgL_r2z00_1307)
	{
		{	/* Ieee/flonum.scm 406 */
			{	/* Ieee/flonum.scm 407 */
				double BgL_tmpz00_1853;

				{	/* Ieee/flonum.scm 407 */
					double BgL_auxz00_1863;
					double BgL_auxz00_1854;

					{	/* Ieee/flonum.scm 407 */
						obj_t BgL_tmpz00_1864;

						if (REALP(BgL_r2z00_1307))
							{	/* Ieee/flonum.scm 407 */
								BgL_tmpz00_1864 = BgL_r2z00_1307;
							}
						else
							{
								obj_t BgL_auxz00_1867;

								BgL_auxz00_1867 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(18384L),
									BGl_string1584z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00,
									BgL_r2z00_1307);
								FAILURE(BgL_auxz00_1867, BFALSE, BFALSE);
							}
						BgL_auxz00_1863 = REAL_TO_DOUBLE(BgL_tmpz00_1864);
					}
					{	/* Ieee/flonum.scm 407 */
						obj_t BgL_tmpz00_1855;

						if (REALP(BgL_r1z00_1306))
							{	/* Ieee/flonum.scm 407 */
								BgL_tmpz00_1855 = BgL_r1z00_1306;
							}
						else
							{
								obj_t BgL_auxz00_1858;

								BgL_auxz00_1858 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(18384L),
									BGl_string1584z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00,
									BgL_r1z00_1306);
								FAILURE(BgL_auxz00_1858, BFALSE, BFALSE);
							}
						BgL_auxz00_1854 = REAL_TO_DOUBLE(BgL_tmpz00_1855);
					}
					BgL_tmpz00_1853 =
						BGl_maxzd22flzd2zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1854,
						BgL_auxz00_1863);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_1853);
			}
		}

	}



/* min-2fl */
	BGL_EXPORTED_DEF double BGl_minzd22flzd2zz__r4_numbers_6_5_flonumz00(double
		BgL_r1z00_31, double BgL_r2z00_32)
	{
		{	/* Ieee/flonum.scm 412 */
			return BGL_FL_MIN2(BgL_r1z00_31, BgL_r2z00_32);
		}

	}



/* &min-2fl */
	obj_t BGl_z62minzd22flzb0zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1308,
		obj_t BgL_r1z00_1309, obj_t BgL_r2z00_1310)
	{
		{	/* Ieee/flonum.scm 412 */
			{	/* Ieee/flonum.scm 413 */
				double BgL_tmpz00_1875;

				{	/* Ieee/flonum.scm 413 */
					double BgL_auxz00_1885;
					double BgL_auxz00_1876;

					{	/* Ieee/flonum.scm 413 */
						obj_t BgL_tmpz00_1886;

						if (REALP(BgL_r2z00_1310))
							{	/* Ieee/flonum.scm 413 */
								BgL_tmpz00_1886 = BgL_r2z00_1310;
							}
						else
							{
								obj_t BgL_auxz00_1889;

								BgL_auxz00_1889 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(18657L),
									BGl_string1585z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00,
									BgL_r2z00_1310);
								FAILURE(BgL_auxz00_1889, BFALSE, BFALSE);
							}
						BgL_auxz00_1885 = REAL_TO_DOUBLE(BgL_tmpz00_1886);
					}
					{	/* Ieee/flonum.scm 413 */
						obj_t BgL_tmpz00_1877;

						if (REALP(BgL_r1z00_1309))
							{	/* Ieee/flonum.scm 413 */
								BgL_tmpz00_1877 = BgL_r1z00_1309;
							}
						else
							{
								obj_t BgL_auxz00_1880;

								BgL_auxz00_1880 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(18657L),
									BGl_string1585z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00,
									BgL_r1z00_1309);
								FAILURE(BgL_auxz00_1880, BFALSE, BFALSE);
							}
						BgL_auxz00_1876 = REAL_TO_DOUBLE(BgL_tmpz00_1877);
					}
					BgL_tmpz00_1875 =
						BGl_minzd22flzd2zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1876,
						BgL_auxz00_1885);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_1875);
			}
		}

	}



/* minfl */
	BGL_EXPORTED_DEF double BGl_minflz00zz__r4_numbers_6_5_flonumz00(double
		BgL_r1z00_33, obj_t BgL_rnz00_34)
	{
		{	/* Ieee/flonum.scm 418 */
			{
				double BgL_minz00_1089;
				obj_t BgL_rnz00_1090;

				BgL_minz00_1089 = BgL_r1z00_33;
				BgL_rnz00_1090 = BgL_rnz00_34;
			BgL_loopz00_1088:
				if (NULLP(BgL_rnz00_1090))
					{	/* Ieee/flonum.scm 421 */
						return BgL_minz00_1089;
					}
				else
					{
						obj_t BgL_rnz00_1903;
						double BgL_minz00_1898;

						BgL_minz00_1898 =
							BGL_FL_MIN2(REAL_TO_DOUBLE(CAR(
									((obj_t) BgL_rnz00_1090))), BgL_minz00_1089);
						BgL_rnz00_1903 = CDR(((obj_t) BgL_rnz00_1090));
						BgL_rnz00_1090 = BgL_rnz00_1903;
						BgL_minz00_1089 = BgL_minz00_1898;
						goto BgL_loopz00_1088;
					}
			}
		}

	}



/* &minfl */
	obj_t BGl_z62minflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1311,
		obj_t BgL_r1z00_1312, obj_t BgL_rnz00_1313)
	{
		{	/* Ieee/flonum.scm 418 */
			{	/* Ieee/flonum.scm 419 */
				double BgL_tmpz00_1906;

				{	/* Ieee/flonum.scm 419 */
					double BgL_auxz00_1907;

					{	/* Ieee/flonum.scm 419 */
						obj_t BgL_tmpz00_1908;

						if (REALP(BgL_r1z00_1312))
							{	/* Ieee/flonum.scm 419 */
								BgL_tmpz00_1908 = BgL_r1z00_1312;
							}
						else
							{
								obj_t BgL_auxz00_1911;

								BgL_auxz00_1911 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(18926L),
									BGl_string1586z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00,
									BgL_r1z00_1312);
								FAILURE(BgL_auxz00_1911, BFALSE, BFALSE);
							}
						BgL_auxz00_1907 = REAL_TO_DOUBLE(BgL_tmpz00_1908);
					}
					BgL_tmpz00_1906 =
						BGl_minflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1907,
						BgL_rnz00_1313);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_1906);
			}
		}

	}



/* absfl */
	BGL_EXPORTED_DEF double BGl_absflz00zz__r4_numbers_6_5_flonumz00(double
		BgL_rz00_35)
	{
		{	/* Ieee/flonum.scm 428 */
			return fabs(BgL_rz00_35);
		}

	}



/* &absfl */
	obj_t BGl_z62absflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1314,
		obj_t BgL_rz00_1315)
	{
		{	/* Ieee/flonum.scm 428 */
			{	/* Ieee/flonum.scm 429 */
				double BgL_tmpz00_1919;

				{	/* Ieee/flonum.scm 429 */
					double BgL_auxz00_1920;

					{	/* Ieee/flonum.scm 429 */
						obj_t BgL_tmpz00_1921;

						if (REALP(BgL_rz00_1315))
							{	/* Ieee/flonum.scm 429 */
								BgL_tmpz00_1921 = BgL_rz00_1315;
							}
						else
							{
								obj_t BgL_auxz00_1924;

								BgL_auxz00_1924 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(19290L),
									BGl_string1587z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00, BgL_rz00_1315);
								FAILURE(BgL_auxz00_1924, BFALSE, BFALSE);
							}
						BgL_auxz00_1920 = REAL_TO_DOUBLE(BgL_tmpz00_1921);
					}
					BgL_tmpz00_1919 =
						BGl_absflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1920);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_1919);
			}
		}

	}



/* floorfl */
	BGL_EXPORTED_DEF double BGl_floorflz00zz__r4_numbers_6_5_flonumz00(double
		BgL_rz00_36)
	{
		{	/* Ieee/flonum.scm 434 */
			return floor(BgL_rz00_36);
		}

	}



/* &floorfl */
	obj_t BGl_z62floorflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1316,
		obj_t BgL_rz00_1317)
	{
		{	/* Ieee/flonum.scm 434 */
			{	/* Ieee/flonum.scm 435 */
				double BgL_tmpz00_1932;

				{	/* Ieee/flonum.scm 435 */
					double BgL_auxz00_1933;

					{	/* Ieee/flonum.scm 435 */
						obj_t BgL_tmpz00_1934;

						if (REALP(BgL_rz00_1317))
							{	/* Ieee/flonum.scm 435 */
								BgL_tmpz00_1934 = BgL_rz00_1317;
							}
						else
							{
								obj_t BgL_auxz00_1937;

								BgL_auxz00_1937 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(19555L),
									BGl_string1588z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00, BgL_rz00_1317);
								FAILURE(BgL_auxz00_1937, BFALSE, BFALSE);
							}
						BgL_auxz00_1933 = REAL_TO_DOUBLE(BgL_tmpz00_1934);
					}
					BgL_tmpz00_1932 =
						BGl_floorflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1933);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_1932);
			}
		}

	}



/* ceilingfl */
	BGL_EXPORTED_DEF double BGl_ceilingflz00zz__r4_numbers_6_5_flonumz00(double
		BgL_rz00_37)
	{
		{	/* Ieee/flonum.scm 440 */
			return ceil(BgL_rz00_37);
		}

	}



/* &ceilingfl */
	obj_t BGl_z62ceilingflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1318,
		obj_t BgL_rz00_1319)
	{
		{	/* Ieee/flonum.scm 440 */
			{	/* Ieee/flonum.scm 441 */
				double BgL_tmpz00_1945;

				{	/* Ieee/flonum.scm 441 */
					double BgL_auxz00_1946;

					{	/* Ieee/flonum.scm 441 */
						obj_t BgL_tmpz00_1947;

						if (REALP(BgL_rz00_1319))
							{	/* Ieee/flonum.scm 441 */
								BgL_tmpz00_1947 = BgL_rz00_1319;
							}
						else
							{
								obj_t BgL_auxz00_1950;

								BgL_auxz00_1950 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(19822L),
									BGl_string1589z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00, BgL_rz00_1319);
								FAILURE(BgL_auxz00_1950, BFALSE, BFALSE);
							}
						BgL_auxz00_1946 = REAL_TO_DOUBLE(BgL_tmpz00_1947);
					}
					BgL_tmpz00_1945 =
						BGl_ceilingflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1946);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_1945);
			}
		}

	}



/* truncatefl */
	BGL_EXPORTED_DEF double BGl_truncateflz00zz__r4_numbers_6_5_flonumz00(double
		BgL_rz00_38)
	{
		{	/* Ieee/flonum.scm 446 */
			if ((BgL_rz00_38 < ((double) 0.0)))
				{	/* Ieee/flonum.scm 447 */
					return ceil(BgL_rz00_38);
				}
			else
				{	/* Ieee/flonum.scm 447 */
					return floor(BgL_rz00_38);
				}
		}

	}



/* &truncatefl */
	obj_t BGl_z62truncateflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1320,
		obj_t BgL_rz00_1321)
	{
		{	/* Ieee/flonum.scm 446 */
			{	/* Ieee/flonum.scm 447 */
				double BgL_tmpz00_1961;

				{	/* Ieee/flonum.scm 447 */
					double BgL_auxz00_1962;

					{	/* Ieee/flonum.scm 447 */
						obj_t BgL_tmpz00_1963;

						if (REALP(BgL_rz00_1321))
							{	/* Ieee/flonum.scm 447 */
								BgL_tmpz00_1963 = BgL_rz00_1321;
							}
						else
							{
								obj_t BgL_auxz00_1966;

								BgL_auxz00_1966 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(20092L),
									BGl_string1590z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00, BgL_rz00_1321);
								FAILURE(BgL_auxz00_1966, BFALSE, BFALSE);
							}
						BgL_auxz00_1962 = REAL_TO_DOUBLE(BgL_tmpz00_1963);
					}
					BgL_tmpz00_1961 =
						BGl_truncateflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1962);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_1961);
			}
		}

	}



/* roundfl */
	BGL_EXPORTED_DEF double BGl_roundflz00zz__r4_numbers_6_5_flonumz00(double
		BgL_rz00_39)
	{
		{	/* Ieee/flonum.scm 454 */
			return BGL_FL_ROUND(BgL_rz00_39);
		}

	}



/* &roundfl */
	obj_t BGl_z62roundflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1322,
		obj_t BgL_rz00_1323)
	{
		{	/* Ieee/flonum.scm 454 */
			{	/* Ieee/flonum.scm 455 */
				double BgL_tmpz00_1974;

				{	/* Ieee/flonum.scm 455 */
					double BgL_auxz00_1975;

					{	/* Ieee/flonum.scm 455 */
						obj_t BgL_tmpz00_1976;

						if (REALP(BgL_rz00_1323))
							{	/* Ieee/flonum.scm 455 */
								BgL_tmpz00_1976 = BgL_rz00_1323;
							}
						else
							{
								obj_t BgL_auxz00_1979;

								BgL_auxz00_1979 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(20400L),
									BGl_string1591z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00, BgL_rz00_1323);
								FAILURE(BgL_auxz00_1979, BFALSE, BFALSE);
							}
						BgL_auxz00_1975 = REAL_TO_DOUBLE(BgL_tmpz00_1976);
					}
					BgL_tmpz00_1974 =
						BGl_roundflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1975);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_1974);
			}
		}

	}



/* remainderfl */
	BGL_EXPORTED_DEF double BGl_remainderflz00zz__r4_numbers_6_5_flonumz00(double
		BgL_n1z00_40, double BgL_n2z00_41)
	{
		{	/* Ieee/flonum.scm 460 */
			return fmod(BgL_n1z00_40, BgL_n2z00_41);
		}

	}



/* &remainderfl */
	obj_t BGl_z62remainderflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1324,
		obj_t BgL_n1z00_1325, obj_t BgL_n2z00_1326)
	{
		{	/* Ieee/flonum.scm 460 */
			{	/* Ieee/flonum.scm 461 */
				double BgL_tmpz00_1987;

				{	/* Ieee/flonum.scm 461 */
					double BgL_auxz00_1997;
					double BgL_auxz00_1988;

					{	/* Ieee/flonum.scm 461 */
						obj_t BgL_tmpz00_1998;

						if (REALP(BgL_n2z00_1326))
							{	/* Ieee/flonum.scm 461 */
								BgL_tmpz00_1998 = BgL_n2z00_1326;
							}
						else
							{
								obj_t BgL_auxz00_2001;

								BgL_auxz00_2001 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(20675L),
									BGl_string1592z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00,
									BgL_n2z00_1326);
								FAILURE(BgL_auxz00_2001, BFALSE, BFALSE);
							}
						BgL_auxz00_1997 = REAL_TO_DOUBLE(BgL_tmpz00_1998);
					}
					{	/* Ieee/flonum.scm 461 */
						obj_t BgL_tmpz00_1989;

						if (REALP(BgL_n1z00_1325))
							{	/* Ieee/flonum.scm 461 */
								BgL_tmpz00_1989 = BgL_n1z00_1325;
							}
						else
							{
								obj_t BgL_auxz00_1992;

								BgL_auxz00_1992 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(20675L),
									BGl_string1592z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00,
									BgL_n1z00_1325);
								FAILURE(BgL_auxz00_1992, BFALSE, BFALSE);
							}
						BgL_auxz00_1988 = REAL_TO_DOUBLE(BgL_tmpz00_1989);
					}
					BgL_tmpz00_1987 =
						BGl_remainderflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1988,
						BgL_auxz00_1997);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_1987);
			}
		}

	}



/* expfl */
	BGL_EXPORTED_DEF double BGl_expflz00zz__r4_numbers_6_5_flonumz00(double
		BgL_xz00_42)
	{
		{	/* Ieee/flonum.scm 466 */
			return exp(BgL_xz00_42);
		}

	}



/* &expfl */
	obj_t BGl_z62expflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1327,
		obj_t BgL_xz00_1328)
	{
		{	/* Ieee/flonum.scm 466 */
			{	/* Ieee/flonum.scm 467 */
				double BgL_tmpz00_2009;

				{	/* Ieee/flonum.scm 467 */
					double BgL_auxz00_2010;

					{	/* Ieee/flonum.scm 467 */
						obj_t BgL_tmpz00_2011;

						if (REALP(BgL_xz00_1328))
							{	/* Ieee/flonum.scm 467 */
								BgL_tmpz00_2011 = BgL_xz00_1328;
							}
						else
							{
								obj_t BgL_auxz00_2014;

								BgL_auxz00_2014 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(20941L),
									BGl_string1593z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00, BgL_xz00_1328);
								FAILURE(BgL_auxz00_2014, BFALSE, BFALSE);
							}
						BgL_auxz00_2010 = REAL_TO_DOUBLE(BgL_tmpz00_2011);
					}
					BgL_tmpz00_2009 =
						BGl_expflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2010);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_2009);
			}
		}

	}



/* logfl */
	BGL_EXPORTED_DEF double BGl_logflz00zz__r4_numbers_6_5_flonumz00(double
		BgL_xz00_43)
	{
		{	/* Ieee/flonum.scm 472 */
			return log(BgL_xz00_43);
		}

	}



/* &logfl */
	obj_t BGl_z62logflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1329,
		obj_t BgL_xz00_1330)
	{
		{	/* Ieee/flonum.scm 472 */
			{	/* Ieee/flonum.scm 473 */
				double BgL_tmpz00_2022;

				{	/* Ieee/flonum.scm 473 */
					double BgL_auxz00_2023;

					{	/* Ieee/flonum.scm 473 */
						obj_t BgL_tmpz00_2024;

						if (REALP(BgL_xz00_1330))
							{	/* Ieee/flonum.scm 473 */
								BgL_tmpz00_2024 = BgL_xz00_1330;
							}
						else
							{
								obj_t BgL_auxz00_2027;

								BgL_auxz00_2027 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(21202L),
									BGl_string1594z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00, BgL_xz00_1330);
								FAILURE(BgL_auxz00_2027, BFALSE, BFALSE);
							}
						BgL_auxz00_2023 = REAL_TO_DOUBLE(BgL_tmpz00_2024);
					}
					BgL_tmpz00_2022 =
						BGl_logflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2023);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_2022);
			}
		}

	}



/* log2fl */
	BGL_EXPORTED_DEF double BGl_log2flz00zz__r4_numbers_6_5_flonumz00(double
		BgL_xz00_44)
	{
		{	/* Ieee/flonum.scm 478 */
			return log2(BgL_xz00_44);
		}

	}



/* &log2fl */
	obj_t BGl_z62log2flz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1331,
		obj_t BgL_xz00_1332)
	{
		{	/* Ieee/flonum.scm 478 */
			{	/* Ieee/flonum.scm 479 */
				double BgL_tmpz00_2035;

				{	/* Ieee/flonum.scm 479 */
					double BgL_auxz00_2036;

					{	/* Ieee/flonum.scm 479 */
						obj_t BgL_tmpz00_2037;

						if (REALP(BgL_xz00_1332))
							{	/* Ieee/flonum.scm 479 */
								BgL_tmpz00_2037 = BgL_xz00_1332;
							}
						else
							{
								obj_t BgL_auxz00_2040;

								BgL_auxz00_2040 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(21465L),
									BGl_string1595z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00, BgL_xz00_1332);
								FAILURE(BgL_auxz00_2040, BFALSE, BFALSE);
							}
						BgL_auxz00_2036 = REAL_TO_DOUBLE(BgL_tmpz00_2037);
					}
					BgL_tmpz00_2035 =
						BGl_log2flz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2036);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_2035);
			}
		}

	}



/* log10fl */
	BGL_EXPORTED_DEF double BGl_log10flz00zz__r4_numbers_6_5_flonumz00(double
		BgL_xz00_45)
	{
		{	/* Ieee/flonum.scm 484 */
			return log10(BgL_xz00_45);
		}

	}



/* &log10fl */
	obj_t BGl_z62log10flz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1333,
		obj_t BgL_xz00_1334)
	{
		{	/* Ieee/flonum.scm 484 */
			{	/* Ieee/flonum.scm 485 */
				double BgL_tmpz00_2048;

				{	/* Ieee/flonum.scm 485 */
					double BgL_auxz00_2049;

					{	/* Ieee/flonum.scm 485 */
						obj_t BgL_tmpz00_2050;

						if (REALP(BgL_xz00_1334))
							{	/* Ieee/flonum.scm 485 */
								BgL_tmpz00_2050 = BgL_xz00_1334;
							}
						else
							{
								obj_t BgL_auxz00_2053;

								BgL_auxz00_2053 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(21730L),
									BGl_string1596z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00, BgL_xz00_1334);
								FAILURE(BgL_auxz00_2053, BFALSE, BFALSE);
							}
						BgL_auxz00_2049 = REAL_TO_DOUBLE(BgL_tmpz00_2050);
					}
					BgL_tmpz00_2048 =
						BGl_log10flz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2049);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_2048);
			}
		}

	}



/* sinfl */
	BGL_EXPORTED_DEF double BGl_sinflz00zz__r4_numbers_6_5_flonumz00(double
		BgL_xz00_46)
	{
		{	/* Ieee/flonum.scm 490 */
			return sin(BgL_xz00_46);
		}

	}



/* &sinfl */
	obj_t BGl_z62sinflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1335,
		obj_t BgL_xz00_1336)
	{
		{	/* Ieee/flonum.scm 490 */
			{	/* Ieee/flonum.scm 491 */
				double BgL_tmpz00_2061;

				{	/* Ieee/flonum.scm 491 */
					double BgL_auxz00_2062;

					{	/* Ieee/flonum.scm 491 */
						obj_t BgL_tmpz00_2063;

						if (REALP(BgL_xz00_1336))
							{	/* Ieee/flonum.scm 491 */
								BgL_tmpz00_2063 = BgL_xz00_1336;
							}
						else
							{
								obj_t BgL_auxz00_2066;

								BgL_auxz00_2066 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(21994L),
									BGl_string1597z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00, BgL_xz00_1336);
								FAILURE(BgL_auxz00_2066, BFALSE, BFALSE);
							}
						BgL_auxz00_2062 = REAL_TO_DOUBLE(BgL_tmpz00_2063);
					}
					BgL_tmpz00_2061 =
						BGl_sinflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2062);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_2061);
			}
		}

	}



/* cosfl */
	BGL_EXPORTED_DEF double BGl_cosflz00zz__r4_numbers_6_5_flonumz00(double
		BgL_xz00_47)
	{
		{	/* Ieee/flonum.scm 496 */
			return cos(BgL_xz00_47);
		}

	}



/* &cosfl */
	obj_t BGl_z62cosflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1337,
		obj_t BgL_xz00_1338)
	{
		{	/* Ieee/flonum.scm 496 */
			{	/* Ieee/flonum.scm 497 */
				double BgL_tmpz00_2074;

				{	/* Ieee/flonum.scm 497 */
					double BgL_auxz00_2075;

					{	/* Ieee/flonum.scm 497 */
						obj_t BgL_tmpz00_2076;

						if (REALP(BgL_xz00_1338))
							{	/* Ieee/flonum.scm 497 */
								BgL_tmpz00_2076 = BgL_xz00_1338;
							}
						else
							{
								obj_t BgL_auxz00_2079;

								BgL_auxz00_2079 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(22255L),
									BGl_string1598z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00, BgL_xz00_1338);
								FAILURE(BgL_auxz00_2079, BFALSE, BFALSE);
							}
						BgL_auxz00_2075 = REAL_TO_DOUBLE(BgL_tmpz00_2076);
					}
					BgL_tmpz00_2074 =
						BGl_cosflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2075);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_2074);
			}
		}

	}



/* tanfl */
	BGL_EXPORTED_DEF double BGl_tanflz00zz__r4_numbers_6_5_flonumz00(double
		BgL_xz00_48)
	{
		{	/* Ieee/flonum.scm 502 */
			return tan(BgL_xz00_48);
		}

	}



/* &tanfl */
	obj_t BGl_z62tanflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1339,
		obj_t BgL_xz00_1340)
	{
		{	/* Ieee/flonum.scm 502 */
			{	/* Ieee/flonum.scm 503 */
				double BgL_tmpz00_2087;

				{	/* Ieee/flonum.scm 503 */
					double BgL_auxz00_2088;

					{	/* Ieee/flonum.scm 503 */
						obj_t BgL_tmpz00_2089;

						if (REALP(BgL_xz00_1340))
							{	/* Ieee/flonum.scm 503 */
								BgL_tmpz00_2089 = BgL_xz00_1340;
							}
						else
							{
								obj_t BgL_auxz00_2092;

								BgL_auxz00_2092 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(22516L),
									BGl_string1599z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00, BgL_xz00_1340);
								FAILURE(BgL_auxz00_2092, BFALSE, BFALSE);
							}
						BgL_auxz00_2088 = REAL_TO_DOUBLE(BgL_tmpz00_2089);
					}
					BgL_tmpz00_2087 =
						BGl_tanflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2088);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_2087);
			}
		}

	}



/* asinfl */
	BGL_EXPORTED_DEF double BGl_asinflz00zz__r4_numbers_6_5_flonumz00(double
		BgL_xz00_49)
	{
		{	/* Ieee/flonum.scm 508 */
			return asin(BgL_xz00_49);
		}

	}



/* &asinfl */
	obj_t BGl_z62asinflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1341,
		obj_t BgL_xz00_1342)
	{
		{	/* Ieee/flonum.scm 508 */
			{	/* Ieee/flonum.scm 509 */
				double BgL_tmpz00_2100;

				{	/* Ieee/flonum.scm 509 */
					double BgL_auxz00_2101;

					{	/* Ieee/flonum.scm 509 */
						obj_t BgL_tmpz00_2102;

						if (REALP(BgL_xz00_1342))
							{	/* Ieee/flonum.scm 509 */
								BgL_tmpz00_2102 = BgL_xz00_1342;
							}
						else
							{
								obj_t BgL_auxz00_2105;

								BgL_auxz00_2105 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(22778L),
									BGl_string1600z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00, BgL_xz00_1342);
								FAILURE(BgL_auxz00_2105, BFALSE, BFALSE);
							}
						BgL_auxz00_2101 = REAL_TO_DOUBLE(BgL_tmpz00_2102);
					}
					BgL_tmpz00_2100 =
						BGl_asinflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2101);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_2100);
			}
		}

	}



/* acosfl */
	BGL_EXPORTED_DEF double BGl_acosflz00zz__r4_numbers_6_5_flonumz00(double
		BgL_xz00_50)
	{
		{	/* Ieee/flonum.scm 514 */
			return acos(BgL_xz00_50);
		}

	}



/* &acosfl */
	obj_t BGl_z62acosflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1343,
		obj_t BgL_xz00_1344)
	{
		{	/* Ieee/flonum.scm 514 */
			{	/* Ieee/flonum.scm 515 */
				double BgL_tmpz00_2113;

				{	/* Ieee/flonum.scm 515 */
					double BgL_auxz00_2114;

					{	/* Ieee/flonum.scm 515 */
						obj_t BgL_tmpz00_2115;

						if (REALP(BgL_xz00_1344))
							{	/* Ieee/flonum.scm 515 */
								BgL_tmpz00_2115 = BgL_xz00_1344;
							}
						else
							{
								obj_t BgL_auxz00_2118;

								BgL_auxz00_2118 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(23041L),
									BGl_string1601z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00, BgL_xz00_1344);
								FAILURE(BgL_auxz00_2118, BFALSE, BFALSE);
							}
						BgL_auxz00_2114 = REAL_TO_DOUBLE(BgL_tmpz00_2115);
					}
					BgL_tmpz00_2113 =
						BGl_acosflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2114);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_2113);
			}
		}

	}



/* atanfl */
	BGL_EXPORTED_DEF double BGl_atanflz00zz__r4_numbers_6_5_flonumz00(double
		BgL_xz00_51, obj_t BgL_yz00_52)
	{
		{	/* Ieee/flonum.scm 520 */
			if (NULLP(BgL_yz00_52))
				{	/* Ieee/flonum.scm 521 */
					return atan(BgL_xz00_51);
				}
			else
				{	/* Ieee/flonum.scm 523 */
					obj_t BgL_yz00_1110;

					BgL_yz00_1110 = CAR(((obj_t) BgL_yz00_52));
					{	/* Ieee/flonum.scm 524 */
						double BgL_yz00_1113;

						BgL_yz00_1113 = REAL_TO_DOUBLE(BgL_yz00_1110);
						return atan2(BgL_xz00_51, BgL_yz00_1113);
					}
				}
		}

	}



/* &atanfl */
	obj_t BGl_z62atanflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1345,
		obj_t BgL_xz00_1346, obj_t BgL_yz00_1347)
	{
		{	/* Ieee/flonum.scm 520 */
			{	/* Ieee/flonum.scm 521 */
				double BgL_tmpz00_2132;

				{	/* Ieee/flonum.scm 521 */
					double BgL_auxz00_2133;

					{	/* Ieee/flonum.scm 521 */
						obj_t BgL_tmpz00_2134;

						if (REALP(BgL_xz00_1346))
							{	/* Ieee/flonum.scm 521 */
								BgL_tmpz00_2134 = BgL_xz00_1346;
							}
						else
							{
								obj_t BgL_auxz00_2137;

								BgL_auxz00_2137 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(23301L),
									BGl_string1602z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00, BgL_xz00_1346);
								FAILURE(BgL_auxz00_2137, BFALSE, BFALSE);
							}
						BgL_auxz00_2133 = REAL_TO_DOUBLE(BgL_tmpz00_2134);
					}
					BgL_tmpz00_2132 =
						BGl_atanflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2133,
						BgL_yz00_1347);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_2132);
			}
		}

	}



/* atan-1fl */
	BGL_EXPORTED_DEF double BGl_atanzd21flzd2zz__r4_numbers_6_5_flonumz00(double
		BgL_xz00_53)
	{
		{	/* Ieee/flonum.scm 529 */
			return atan(BgL_xz00_53);
		}

	}



/* &atan-1fl */
	obj_t BGl_z62atanzd21flzb0zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1348,
		obj_t BgL_xz00_1349)
	{
		{	/* Ieee/flonum.scm 529 */
			{	/* Ieee/flonum.scm 530 */
				double BgL_tmpz00_2145;

				{	/* Ieee/flonum.scm 530 */
					double BgL_auxz00_2146;

					{	/* Ieee/flonum.scm 530 */
						obj_t BgL_tmpz00_2147;

						if (REALP(BgL_xz00_1349))
							{	/* Ieee/flonum.scm 530 */
								BgL_tmpz00_2147 = BgL_xz00_1349;
							}
						else
							{
								obj_t BgL_auxz00_2150;

								BgL_auxz00_2150 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(23633L),
									BGl_string1603z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00, BgL_xz00_1349);
								FAILURE(BgL_auxz00_2150, BFALSE, BFALSE);
							}
						BgL_auxz00_2146 = REAL_TO_DOUBLE(BgL_tmpz00_2147);
					}
					BgL_tmpz00_2145 =
						BGl_atanzd21flzd2zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2146);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_2145);
			}
		}

	}



/* atan-2fl */
	BGL_EXPORTED_DEF double BGl_atanzd22flzd2zz__r4_numbers_6_5_flonumz00(double
		BgL_xz00_54, double BgL_yz00_55)
	{
		{	/* Ieee/flonum.scm 535 */
			{	/* Ieee/flonum.scm 539 */
				bool_t BgL_test1820z00_2157;

				if ((BgL_xz00_54 == ((double) 0.0)))
					{	/* Ieee/flonum.scm 536 */
						BgL_test1820z00_2157 = (BgL_yz00_55 == ((double) 0.0));
					}
				else
					{	/* Ieee/flonum.scm 536 */
						BgL_test1820z00_2157 = ((bool_t) 0);
					}
				if (BgL_test1820z00_2157)
					{	/* Ieee/flonum.scm 540 */
						obj_t BgL_procz00_1543;
						obj_t BgL_msgz00_1544;
						obj_t BgL_objz00_1545;

						BgL_procz00_1543 =
							string_to_bstring(BSTRING_TO_STRING
							(BGl_string1604z00zz__r4_numbers_6_5_flonumz00));
						BgL_msgz00_1544 =
							string_to_bstring(BSTRING_TO_STRING
							(BGl_string1605z00zz__r4_numbers_6_5_flonumz00));
						BgL_objz00_1545 =
							BGL_REAL_CNST(BGl_real1606z00zz__r4_numbers_6_5_flonumz00);
						the_failure(BgL_procz00_1543, BgL_msgz00_1544, BgL_objz00_1545);
						return ((double) 0.0);
					}
				else
					{	/* Ieee/flonum.scm 539 */
						return atan2(BgL_xz00_54, BgL_yz00_55);
					}
			}
		}

	}



/* &atan-2fl */
	obj_t BGl_z62atanzd22flzb0zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1350,
		obj_t BgL_xz00_1351, obj_t BgL_yz00_1352)
	{
		{	/* Ieee/flonum.scm 535 */
			{	/* Ieee/flonum.scm 536 */
				double BgL_tmpz00_2167;

				{	/* Ieee/flonum.scm 536 */
					double BgL_auxz00_2177;
					double BgL_auxz00_2168;

					{	/* Ieee/flonum.scm 536 */
						obj_t BgL_tmpz00_2178;

						if (REALP(BgL_yz00_1352))
							{	/* Ieee/flonum.scm 536 */
								BgL_tmpz00_2178 = BgL_yz00_1352;
							}
						else
							{
								obj_t BgL_auxz00_2181;

								BgL_auxz00_2181 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(23900L),
									BGl_string1607z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00, BgL_yz00_1352);
								FAILURE(BgL_auxz00_2181, BFALSE, BFALSE);
							}
						BgL_auxz00_2177 = REAL_TO_DOUBLE(BgL_tmpz00_2178);
					}
					{	/* Ieee/flonum.scm 536 */
						obj_t BgL_tmpz00_2169;

						if (REALP(BgL_xz00_1351))
							{	/* Ieee/flonum.scm 536 */
								BgL_tmpz00_2169 = BgL_xz00_1351;
							}
						else
							{
								obj_t BgL_auxz00_2172;

								BgL_auxz00_2172 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(23900L),
									BGl_string1607z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00, BgL_xz00_1351);
								FAILURE(BgL_auxz00_2172, BFALSE, BFALSE);
							}
						BgL_auxz00_2168 = REAL_TO_DOUBLE(BgL_tmpz00_2169);
					}
					BgL_tmpz00_2167 =
						BGl_atanzd22flzd2zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2168,
						BgL_auxz00_2177);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_2167);
			}
		}

	}



/* atan-2fl-ur */
	BGL_EXPORTED_DEF double
		BGl_atanzd22flzd2urz00zz__r4_numbers_6_5_flonumz00(double BgL_xz00_56,
		double BgL_yz00_57)
	{
		{	/* Ieee/flonum.scm 553 */
			return atan2(BgL_xz00_56, BgL_yz00_57);
		}

	}



/* &atan-2fl-ur */
	obj_t BGl_z62atanzd22flzd2urz62zz__r4_numbers_6_5_flonumz00(obj_t
		BgL_envz00_1353, obj_t BgL_xz00_1354, obj_t BgL_yz00_1355)
	{
		{	/* Ieee/flonum.scm 553 */
			{	/* Ieee/flonum.scm 554 */
				double BgL_tmpz00_2189;

				{	/* Ieee/flonum.scm 554 */
					double BgL_auxz00_2199;
					double BgL_auxz00_2190;

					{	/* Ieee/flonum.scm 554 */
						obj_t BgL_tmpz00_2200;

						if (REALP(BgL_yz00_1355))
							{	/* Ieee/flonum.scm 554 */
								BgL_tmpz00_2200 = BgL_yz00_1355;
							}
						else
							{
								obj_t BgL_auxz00_2203;

								BgL_auxz00_2203 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(24538L),
									BGl_string1608z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00, BgL_yz00_1355);
								FAILURE(BgL_auxz00_2203, BFALSE, BFALSE);
							}
						BgL_auxz00_2199 = REAL_TO_DOUBLE(BgL_tmpz00_2200);
					}
					{	/* Ieee/flonum.scm 554 */
						obj_t BgL_tmpz00_2191;

						if (REALP(BgL_xz00_1354))
							{	/* Ieee/flonum.scm 554 */
								BgL_tmpz00_2191 = BgL_xz00_1354;
							}
						else
							{
								obj_t BgL_auxz00_2194;

								BgL_auxz00_2194 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(24538L),
									BGl_string1608z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00, BgL_xz00_1354);
								FAILURE(BgL_auxz00_2194, BFALSE, BFALSE);
							}
						BgL_auxz00_2190 = REAL_TO_DOUBLE(BgL_tmpz00_2191);
					}
					BgL_tmpz00_2189 =
						BGl_atanzd22flzd2urz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2190,
						BgL_auxz00_2199);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_2189);
			}
		}

	}



/* sqrtfl */
	BGL_EXPORTED_DEF double BGl_sqrtflz00zz__r4_numbers_6_5_flonumz00(double
		BgL_rz00_58)
	{
		{	/* Ieee/flonum.scm 559 */
			if ((BgL_rz00_58 < ((double) 0.0)))
				{	/* Ieee/flonum.scm 561 */
					obj_t BgL_procz00_1546;
					obj_t BgL_msgz00_1547;
					obj_t BgL_objz00_1548;

					BgL_procz00_1546 =
						string_to_bstring(BSTRING_TO_STRING
						(BGl_string1609z00zz__r4_numbers_6_5_flonumz00));
					BgL_msgz00_1547 =
						string_to_bstring(BSTRING_TO_STRING
						(BGl_string1605z00zz__r4_numbers_6_5_flonumz00));
					BgL_objz00_1548 = DOUBLE_TO_REAL(BgL_rz00_58);
					BGl_errorz00zz__errorz00(BgL_procz00_1546, BgL_msgz00_1547,
						BgL_objz00_1548);
					return ((double) 0.0);
				}
			else
				{	/* Ieee/flonum.scm 560 */
					return sqrt(BgL_rz00_58);
				}
		}

	}



/* &sqrtfl */
	obj_t BGl_z62sqrtflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1356,
		obj_t BgL_rz00_1357)
	{
		{	/* Ieee/flonum.scm 559 */
			{	/* Ieee/flonum.scm 560 */
				double BgL_tmpz00_2219;

				{	/* Ieee/flonum.scm 560 */
					double BgL_auxz00_2220;

					{	/* Ieee/flonum.scm 560 */
						obj_t BgL_tmpz00_2221;

						if (REALP(BgL_rz00_1357))
							{	/* Ieee/flonum.scm 560 */
								BgL_tmpz00_2221 = BgL_rz00_1357;
							}
						else
							{
								obj_t BgL_auxz00_2224;

								BgL_auxz00_2224 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(24804L),
									BGl_string1610z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00, BgL_rz00_1357);
								FAILURE(BgL_auxz00_2224, BFALSE, BFALSE);
							}
						BgL_auxz00_2220 = REAL_TO_DOUBLE(BgL_tmpz00_2221);
					}
					BgL_tmpz00_2219 =
						BGl_sqrtflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2220);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_2219);
			}
		}

	}



/* sqrtfl-ur */
	BGL_EXPORTED_DEF double BGl_sqrtflzd2urzd2zz__r4_numbers_6_5_flonumz00(double
		BgL_rz00_59)
	{
		{	/* Ieee/flonum.scm 572 */
			return sqrt(BgL_rz00_59);
		}

	}



/* &sqrtfl-ur */
	obj_t BGl_z62sqrtflzd2urzb0zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1358,
		obj_t BgL_rz00_1359)
	{
		{	/* Ieee/flonum.scm 572 */
			{	/* Ieee/flonum.scm 573 */
				double BgL_tmpz00_2232;

				{	/* Ieee/flonum.scm 573 */
					double BgL_auxz00_2233;

					{	/* Ieee/flonum.scm 573 */
						obj_t BgL_tmpz00_2234;

						if (REALP(BgL_rz00_1359))
							{	/* Ieee/flonum.scm 573 */
								BgL_tmpz00_2234 = BgL_rz00_1359;
							}
						else
							{
								obj_t BgL_auxz00_2237;

								BgL_auxz00_2237 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(25283L),
									BGl_string1611z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00, BgL_rz00_1359);
								FAILURE(BgL_auxz00_2237, BFALSE, BFALSE);
							}
						BgL_auxz00_2233 = REAL_TO_DOUBLE(BgL_tmpz00_2234);
					}
					BgL_tmpz00_2232 =
						BGl_sqrtflzd2urzd2zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2233);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_2232);
			}
		}

	}



/* exptfl */
	BGL_EXPORTED_DEF double BGl_exptflz00zz__r4_numbers_6_5_flonumz00(double
		BgL_r1z00_60, double BgL_r2z00_61)
	{
		{	/* Ieee/flonum.scm 578 */
			return pow(BgL_r1z00_60, BgL_r2z00_61);
		}

	}



/* &exptfl */
	obj_t BGl_z62exptflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1360,
		obj_t BgL_r1z00_1361, obj_t BgL_r2z00_1362)
	{
		{	/* Ieee/flonum.scm 578 */
			{	/* Ieee/flonum.scm 579 */
				double BgL_tmpz00_2245;

				{	/* Ieee/flonum.scm 579 */
					double BgL_auxz00_2255;
					double BgL_auxz00_2246;

					{	/* Ieee/flonum.scm 579 */
						obj_t BgL_tmpz00_2256;

						if (REALP(BgL_r2z00_1362))
							{	/* Ieee/flonum.scm 579 */
								BgL_tmpz00_2256 = BgL_r2z00_1362;
							}
						else
							{
								obj_t BgL_auxz00_2259;

								BgL_auxz00_2259 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(25550L),
									BGl_string1612z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00,
									BgL_r2z00_1362);
								FAILURE(BgL_auxz00_2259, BFALSE, BFALSE);
							}
						BgL_auxz00_2255 = REAL_TO_DOUBLE(BgL_tmpz00_2256);
					}
					{	/* Ieee/flonum.scm 579 */
						obj_t BgL_tmpz00_2247;

						if (REALP(BgL_r1z00_1361))
							{	/* Ieee/flonum.scm 579 */
								BgL_tmpz00_2247 = BgL_r1z00_1361;
							}
						else
							{
								obj_t BgL_auxz00_2250;

								BgL_auxz00_2250 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(25550L),
									BGl_string1612z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00,
									BgL_r1z00_1361);
								FAILURE(BgL_auxz00_2250, BFALSE, BFALSE);
							}
						BgL_auxz00_2246 = REAL_TO_DOUBLE(BgL_tmpz00_2247);
					}
					BgL_tmpz00_2245 =
						BGl_exptflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2246,
						BgL_auxz00_2255);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_2245);
			}
		}

	}



/* signbitfl */
	BGL_EXPORTED_DEF long BGl_signbitflz00zz__r4_numbers_6_5_flonumz00(double
		BgL_rz00_62)
	{
		{	/* Ieee/flonum.scm 584 */
			return BGL_SIGNBIT(BgL_rz00_62);
		}

	}



/* &signbitfl */
	obj_t BGl_z62signbitflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1363,
		obj_t BgL_rz00_1364)
	{
		{	/* Ieee/flonum.scm 584 */
			{	/* Ieee/flonum.scm 585 */
				long BgL_tmpz00_2267;

				{	/* Ieee/flonum.scm 585 */
					double BgL_auxz00_2268;

					{	/* Ieee/flonum.scm 585 */
						obj_t BgL_tmpz00_2269;

						if (REALP(BgL_rz00_1364))
							{	/* Ieee/flonum.scm 585 */
								BgL_tmpz00_2269 = BgL_rz00_1364;
							}
						else
							{
								obj_t BgL_auxz00_2272;

								BgL_auxz00_2272 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(25819L),
									BGl_string1613z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00, BgL_rz00_1364);
								FAILURE(BgL_auxz00_2272, BFALSE, BFALSE);
							}
						BgL_auxz00_2268 = REAL_TO_DOUBLE(BgL_tmpz00_2269);
					}
					BgL_tmpz00_2267 =
						BGl_signbitflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2268);
				}
				return BINT(BgL_tmpz00_2267);
			}
		}

	}



/* integerfl? */
	BGL_EXPORTED_DEF bool_t BGl_integerflzf3zf3zz__r4_numbers_6_5_flonumz00(double
		BgL_rz00_63)
	{
		{	/* Ieee/flonum.scm 590 */
			if (BGL_IS_FINITE(BgL_rz00_63))
				{	/* Ieee/flonum.scm 591 */
					double BgL_arg1074z00_1549;

					BgL_arg1074z00_1549 = floor(BgL_rz00_63);
					return (BgL_rz00_63 == BgL_arg1074z00_1549);
				}
			else
				{	/* Ieee/flonum.scm 591 */
					return ((bool_t) 0);
				}
		}

	}



/* &integerfl? */
	obj_t BGl_z62integerflzf3z91zz__r4_numbers_6_5_flonumz00(obj_t
		BgL_envz00_1365, obj_t BgL_rz00_1366)
	{
		{	/* Ieee/flonum.scm 590 */
			{	/* Ieee/flonum.scm 591 */
				bool_t BgL_tmpz00_2283;

				{	/* Ieee/flonum.scm 591 */
					double BgL_auxz00_2284;

					{	/* Ieee/flonum.scm 591 */
						obj_t BgL_tmpz00_2285;

						if (REALP(BgL_rz00_1366))
							{	/* Ieee/flonum.scm 591 */
								BgL_tmpz00_2285 = BgL_rz00_1366;
							}
						else
							{
								obj_t BgL_auxz00_2288;

								BgL_auxz00_2288 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(26094L),
									BGl_string1614z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00, BgL_rz00_1366);
								FAILURE(BgL_auxz00_2288, BFALSE, BFALSE);
							}
						BgL_auxz00_2284 = REAL_TO_DOUBLE(BgL_tmpz00_2285);
					}
					BgL_tmpz00_2283 =
						BGl_integerflzf3zf3zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2284);
				}
				return BBOOL(BgL_tmpz00_2283);
			}
		}

	}



/* evenfl? */
	BGL_EXPORTED_DEF bool_t BGl_evenflzf3zf3zz__r4_numbers_6_5_flonumz00(double
		BgL_rz00_64)
	{
		{	/* Ieee/flonum.scm 596 */
			{	/* Ieee/flonum.scm 597 */
				double BgL_arg1075z00_1550;

				BgL_arg1075z00_1550 = (BgL_rz00_64 / ((double) 2.0));
				if (BGL_IS_FINITE(BgL_arg1075z00_1550))
					{	/* Ieee/flonum.scm 591 */
						double BgL_arg1074z00_1551;

						BgL_arg1074z00_1551 = floor(BgL_arg1075z00_1550);
						return (BgL_arg1075z00_1550 == BgL_arg1074z00_1551);
					}
				else
					{	/* Ieee/flonum.scm 591 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &evenfl? */
	obj_t BGl_z62evenflzf3z91zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1367,
		obj_t BgL_rz00_1368)
	{
		{	/* Ieee/flonum.scm 596 */
			{	/* Ieee/flonum.scm 597 */
				bool_t BgL_tmpz00_2300;

				{	/* Ieee/flonum.scm 597 */
					double BgL_auxz00_2301;

					{	/* Ieee/flonum.scm 597 */
						obj_t BgL_tmpz00_2302;

						if (REALP(BgL_rz00_1368))
							{	/* Ieee/flonum.scm 597 */
								BgL_tmpz00_2302 = BgL_rz00_1368;
							}
						else
							{
								obj_t BgL_auxz00_2305;

								BgL_auxz00_2305 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(26395L),
									BGl_string1615z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00, BgL_rz00_1368);
								FAILURE(BgL_auxz00_2305, BFALSE, BFALSE);
							}
						BgL_auxz00_2301 = REAL_TO_DOUBLE(BgL_tmpz00_2302);
					}
					BgL_tmpz00_2300 =
						BGl_evenflzf3zf3zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2301);
				}
				return BBOOL(BgL_tmpz00_2300);
			}
		}

	}



/* oddfl? */
	BGL_EXPORTED_DEF bool_t BGl_oddflzf3zf3zz__r4_numbers_6_5_flonumz00(double
		BgL_rz00_65)
	{
		{	/* Ieee/flonum.scm 602 */
			{	/* Ieee/flonum.scm 603 */
				bool_t BgL_test1836z00_2312;

				if (BGL_IS_FINITE(BgL_rz00_65))
					{	/* Ieee/flonum.scm 591 */
						double BgL_arg1074z00_1552;

						BgL_arg1074z00_1552 = floor(BgL_rz00_65);
						BgL_test1836z00_2312 = (BgL_rz00_65 == BgL_arg1074z00_1552);
					}
				else
					{	/* Ieee/flonum.scm 591 */
						BgL_test1836z00_2312 = ((bool_t) 0);
					}
				if (BgL_test1836z00_2312)
					{	/* Ieee/flonum.scm 604 */
						bool_t BgL_test1838z00_2317;

						{	/* Ieee/flonum.scm 597 */
							double BgL_arg1075z00_1553;

							BgL_arg1075z00_1553 = (BgL_rz00_65 / ((double) 2.0));
							if (BGL_IS_FINITE(BgL_arg1075z00_1553))
								{	/* Ieee/flonum.scm 591 */
									double BgL_arg1074z00_1554;

									BgL_arg1074z00_1554 = floor(BgL_arg1075z00_1553);
									BgL_test1838z00_2317 =
										(BgL_arg1075z00_1553 == BgL_arg1074z00_1554);
								}
							else
								{	/* Ieee/flonum.scm 591 */
									BgL_test1838z00_2317 = ((bool_t) 0);
								}
						}
						if (BgL_test1838z00_2317)
							{	/* Ieee/flonum.scm 604 */
								return ((bool_t) 0);
							}
						else
							{	/* Ieee/flonum.scm 604 */
								return ((bool_t) 1);
							}
					}
				else
					{	/* Ieee/flonum.scm 603 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &oddfl? */
	obj_t BGl_z62oddflzf3z91zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1369,
		obj_t BgL_rz00_1370)
	{
		{	/* Ieee/flonum.scm 602 */
			{	/* Ieee/flonum.scm 603 */
				bool_t BgL_tmpz00_2323;

				{	/* Ieee/flonum.scm 603 */
					double BgL_auxz00_2324;

					{	/* Ieee/flonum.scm 603 */
						obj_t BgL_tmpz00_2325;

						if (REALP(BgL_rz00_1370))
							{	/* Ieee/flonum.scm 603 */
								BgL_tmpz00_2325 = BgL_rz00_1370;
							}
						else
							{
								obj_t BgL_auxz00_2328;

								BgL_auxz00_2328 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(26666L),
									BGl_string1616z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00, BgL_rz00_1370);
								FAILURE(BgL_auxz00_2328, BFALSE, BFALSE);
							}
						BgL_auxz00_2324 = REAL_TO_DOUBLE(BgL_tmpz00_2325);
					}
					BgL_tmpz00_2323 =
						BGl_oddflzf3zf3zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2324);
				}
				return BBOOL(BgL_tmpz00_2323);
			}
		}

	}



/* finitefl? */
	BGL_EXPORTED_DEF bool_t BGl_finiteflzf3zf3zz__r4_numbers_6_5_flonumz00(double
		BgL_rz00_66)
	{
		{	/* Ieee/flonum.scm 609 */
			return BGL_IS_FINITE(BgL_rz00_66);
		}

	}



/* &finitefl? */
	obj_t BGl_z62finiteflzf3z91zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1371,
		obj_t BgL_rz00_1372)
	{
		{	/* Ieee/flonum.scm 609 */
			{	/* Ieee/flonum.scm 610 */
				bool_t BgL_tmpz00_2336;

				{	/* Ieee/flonum.scm 610 */
					double BgL_auxz00_2337;

					{	/* Ieee/flonum.scm 610 */
						obj_t BgL_tmpz00_2338;

						if (REALP(BgL_rz00_1372))
							{	/* Ieee/flonum.scm 610 */
								BgL_tmpz00_2338 = BgL_rz00_1372;
							}
						else
							{
								obj_t BgL_auxz00_2341;

								BgL_auxz00_2341 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(26957L),
									BGl_string1617z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00, BgL_rz00_1372);
								FAILURE(BgL_auxz00_2341, BFALSE, BFALSE);
							}
						BgL_auxz00_2337 = REAL_TO_DOUBLE(BgL_tmpz00_2338);
					}
					BgL_tmpz00_2336 =
						BGl_finiteflzf3zf3zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2337);
				}
				return BBOOL(BgL_tmpz00_2336);
			}
		}

	}



/* infinitefl? */
	BGL_EXPORTED_DEF bool_t
		BGl_infiniteflzf3zf3zz__r4_numbers_6_5_flonumz00(double BgL_rz00_67)
	{
		{	/* Ieee/flonum.scm 615 */
			return BGL_IS_INF(BgL_rz00_67);
		}

	}



/* &infinitefl? */
	obj_t BGl_z62infiniteflzf3z91zz__r4_numbers_6_5_flonumz00(obj_t
		BgL_envz00_1373, obj_t BgL_rz00_1374)
	{
		{	/* Ieee/flonum.scm 615 */
			{	/* Ieee/flonum.scm 616 */
				bool_t BgL_tmpz00_2349;

				{	/* Ieee/flonum.scm 616 */
					double BgL_auxz00_2350;

					{	/* Ieee/flonum.scm 616 */
						obj_t BgL_tmpz00_2351;

						if (REALP(BgL_rz00_1374))
							{	/* Ieee/flonum.scm 616 */
								BgL_tmpz00_2351 = BgL_rz00_1374;
							}
						else
							{
								obj_t BgL_auxz00_2354;

								BgL_auxz00_2354 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(27229L),
									BGl_string1618z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00, BgL_rz00_1374);
								FAILURE(BgL_auxz00_2354, BFALSE, BFALSE);
							}
						BgL_auxz00_2350 = REAL_TO_DOUBLE(BgL_tmpz00_2351);
					}
					BgL_tmpz00_2349 =
						BGl_infiniteflzf3zf3zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2350);
				}
				return BBOOL(BgL_tmpz00_2349);
			}
		}

	}



/* nanfl? */
	BGL_EXPORTED_DEF bool_t BGl_nanflzf3zf3zz__r4_numbers_6_5_flonumz00(double
		BgL_rz00_68)
	{
		{	/* Ieee/flonum.scm 621 */
			return BGL_IS_NAN(BgL_rz00_68);
		}

	}



/* &nanfl? */
	obj_t BGl_z62nanflzf3z91zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1375,
		obj_t BgL_rz00_1376)
	{
		{	/* Ieee/flonum.scm 621 */
			{	/* Ieee/flonum.scm 622 */
				bool_t BgL_tmpz00_2362;

				{	/* Ieee/flonum.scm 622 */
					double BgL_auxz00_2363;

					{	/* Ieee/flonum.scm 622 */
						obj_t BgL_tmpz00_2364;

						if (REALP(BgL_rz00_1376))
							{	/* Ieee/flonum.scm 622 */
								BgL_tmpz00_2364 = BgL_rz00_1376;
							}
						else
							{
								obj_t BgL_auxz00_2367;

								BgL_auxz00_2367 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(27493L),
									BGl_string1619z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00, BgL_rz00_1376);
								FAILURE(BgL_auxz00_2367, BFALSE, BFALSE);
							}
						BgL_auxz00_2363 = REAL_TO_DOUBLE(BgL_tmpz00_2364);
					}
					BgL_tmpz00_2362 =
						BGl_nanflzf3zf3zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2363);
				}
				return BBOOL(BgL_tmpz00_2362);
			}
		}

	}



/* string->real */
	BGL_EXPORTED_DEF double
		BGl_stringzd2ze3realz31zz__r4_numbers_6_5_flonumz00(char *BgL_stringz00_69)
	{
		{	/* Ieee/flonum.scm 627 */
			{	/* Ieee/flonum.scm 629 */
				bool_t BgL_test1844z00_2374;

				{	/* Ieee/flonum.scm 629 */
					obj_t BgL_string1z00_1555;

					BgL_string1z00_1555 = string_to_bstring(BgL_stringz00_69);
					{	/* Ieee/flonum.scm 629 */
						long BgL_l1z00_1556;

						BgL_l1z00_1556 = STRING_LENGTH(BgL_string1z00_1555);
						if ((BgL_l1z00_1556 == 6L))
							{	/* Ieee/flonum.scm 629 */
								int BgL_arg1422z00_1557;

								{	/* Ieee/flonum.scm 629 */
									char *BgL_auxz00_2381;
									char *BgL_tmpz00_2379;

									BgL_auxz00_2381 =
										BSTRING_TO_STRING
										(BGl_string1620z00zz__r4_numbers_6_5_flonumz00);
									BgL_tmpz00_2379 = BSTRING_TO_STRING(BgL_string1z00_1555);
									BgL_arg1422z00_1557 =
										memcmp(BgL_tmpz00_2379, BgL_auxz00_2381, BgL_l1z00_1556);
								}
								BgL_test1844z00_2374 = ((long) (BgL_arg1422z00_1557) == 0L);
							}
						else
							{	/* Ieee/flonum.scm 629 */
								BgL_test1844z00_2374 = ((bool_t) 0);
							}
					}
				}
				if (BgL_test1844z00_2374)
					{	/* Ieee/flonum.scm 629 */
						return BGL_NAN;
					}
				else
					{	/* Ieee/flonum.scm 631 */
						bool_t BgL_test1846z00_2386;

						{	/* Ieee/flonum.scm 631 */
							obj_t BgL_string1z00_1558;

							BgL_string1z00_1558 = string_to_bstring(BgL_stringz00_69);
							{	/* Ieee/flonum.scm 631 */
								long BgL_l1z00_1559;

								BgL_l1z00_1559 = STRING_LENGTH(BgL_string1z00_1558);
								if ((BgL_l1z00_1559 == 6L))
									{	/* Ieee/flonum.scm 631 */
										int BgL_arg1422z00_1560;

										{	/* Ieee/flonum.scm 631 */
											char *BgL_auxz00_2393;
											char *BgL_tmpz00_2391;

											BgL_auxz00_2393 =
												BSTRING_TO_STRING
												(BGl_string1621z00zz__r4_numbers_6_5_flonumz00);
											BgL_tmpz00_2391 = BSTRING_TO_STRING(BgL_string1z00_1558);
											BgL_arg1422z00_1560 =
												memcmp(BgL_tmpz00_2391, BgL_auxz00_2393,
												BgL_l1z00_1559);
										}
										BgL_test1846z00_2386 = ((long) (BgL_arg1422z00_1560) == 0L);
									}
								else
									{	/* Ieee/flonum.scm 631 */
										BgL_test1846z00_2386 = ((bool_t) 0);
									}
							}
						}
						if (BgL_test1846z00_2386)
							{	/* Ieee/flonum.scm 631 */
								return BGL_INFINITY;
							}
						else
							{	/* Ieee/flonum.scm 633 */
								bool_t BgL_test1848z00_2398;

								{	/* Ieee/flonum.scm 633 */
									obj_t BgL_string1z00_1561;

									BgL_string1z00_1561 = string_to_bstring(BgL_stringz00_69);
									{	/* Ieee/flonum.scm 633 */
										long BgL_l1z00_1562;

										BgL_l1z00_1562 = STRING_LENGTH(BgL_string1z00_1561);
										if ((BgL_l1z00_1562 == 6L))
											{	/* Ieee/flonum.scm 633 */
												int BgL_arg1422z00_1563;

												{	/* Ieee/flonum.scm 633 */
													char *BgL_auxz00_2405;
													char *BgL_tmpz00_2403;

													BgL_auxz00_2405 =
														BSTRING_TO_STRING
														(BGl_string1622z00zz__r4_numbers_6_5_flonumz00);
													BgL_tmpz00_2403 =
														BSTRING_TO_STRING(BgL_string1z00_1561);
													BgL_arg1422z00_1563 =
														memcmp(BgL_tmpz00_2403, BgL_auxz00_2405,
														BgL_l1z00_1562);
												}
												BgL_test1848z00_2398 =
													((long) (BgL_arg1422z00_1563) == 0L);
											}
										else
											{	/* Ieee/flonum.scm 633 */
												BgL_test1848z00_2398 = ((bool_t) 0);
											}
									}
								}
								if (BgL_test1848z00_2398)
									{	/* Ieee/flonum.scm 633 */
										return (-BGL_INFINITY);
									}
								else
									{	/* Ieee/flonum.scm 633 */
										return STRTOD(BgL_stringz00_69);
									}
							}
					}
			}
		}

	}



/* &string->real */
	obj_t BGl_z62stringzd2ze3realz53zz__r4_numbers_6_5_flonumz00(obj_t
		BgL_envz00_1377, obj_t BgL_stringz00_1378)
	{
		{	/* Ieee/flonum.scm 627 */
			{	/* Ieee/flonum.scm 629 */
				double BgL_tmpz00_2411;

				{	/* Ieee/flonum.scm 629 */
					char *BgL_auxz00_2412;

					{	/* Ieee/flonum.scm 629 */
						obj_t BgL_tmpz00_2413;

						if (STRINGP(BgL_stringz00_1378))
							{	/* Ieee/flonum.scm 629 */
								BgL_tmpz00_2413 = BgL_stringz00_1378;
							}
						else
							{
								obj_t BgL_auxz00_2416;

								BgL_auxz00_2416 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(27780L),
									BGl_string1623z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1624z00zz__r4_numbers_6_5_flonumz00,
									BgL_stringz00_1378);
								FAILURE(BgL_auxz00_2416, BFALSE, BFALSE);
							}
						BgL_auxz00_2412 = BSTRING_TO_STRING(BgL_tmpz00_2413);
					}
					BgL_tmpz00_2411 =
						BGl_stringzd2ze3realz31zz__r4_numbers_6_5_flonumz00
						(BgL_auxz00_2412);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_2411);
			}
		}

	}



/* ieee-string->real */
	BGL_EXPORTED_DEF obj_t
		BGl_ieeezd2stringzd2ze3realze3zz__r4_numbers_6_5_flonumz00(obj_t
		BgL_stringz00_70)
	{
		{	/* Ieee/flonum.scm 641 */
			return DOUBLE_TO_REAL(bgl_ieee_string_to_double(BgL_stringz00_70));
		}

	}



/* &ieee-string->real */
	obj_t BGl_z62ieeezd2stringzd2ze3realz81zz__r4_numbers_6_5_flonumz00(obj_t
		BgL_envz00_1379, obj_t BgL_stringz00_1380)
	{
		{	/* Ieee/flonum.scm 641 */
			{	/* Ieee/flonum.scm 642 */
				obj_t BgL_auxz00_2425;

				if (STRINGP(BgL_stringz00_1380))
					{	/* Ieee/flonum.scm 642 */
						BgL_auxz00_2425 = BgL_stringz00_1380;
					}
				else
					{
						obj_t BgL_auxz00_2428;

						BgL_auxz00_2428 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(28228L),
							BGl_string1625z00zz__r4_numbers_6_5_flonumz00,
							BGl_string1624z00zz__r4_numbers_6_5_flonumz00,
							BgL_stringz00_1380);
						FAILURE(BgL_auxz00_2428, BFALSE, BFALSE);
					}
				return
					BGl_ieeezd2stringzd2ze3realze3zz__r4_numbers_6_5_flonumz00
					(BgL_auxz00_2425);
			}
		}

	}



/* real->ieee-string */
	BGL_EXPORTED_DEF obj_t
		BGl_realzd2ze3ieeezd2stringze3zz__r4_numbers_6_5_flonumz00(obj_t
		BgL_realz00_71)
	{
		{	/* Ieee/flonum.scm 647 */
			return bgl_double_to_ieee_string(REAL_TO_DOUBLE(BgL_realz00_71));
		}

	}



/* &real->ieee-string */
	obj_t BGl_z62realzd2ze3ieeezd2stringz81zz__r4_numbers_6_5_flonumz00(obj_t
		BgL_envz00_1381, obj_t BgL_realz00_1382)
	{
		{	/* Ieee/flonum.scm 647 */
			{	/* Ieee/flonum.scm 648 */
				obj_t BgL_auxz00_2435;

				if (REALP(BgL_realz00_1382))
					{	/* Ieee/flonum.scm 648 */
						BgL_auxz00_2435 = BgL_realz00_1382;
					}
				else
					{
						obj_t BgL_auxz00_2438;

						BgL_auxz00_2438 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(28525L),
							BGl_string1626z00zz__r4_numbers_6_5_flonumz00,
							BGl_string1570z00zz__r4_numbers_6_5_flonumz00, BgL_realz00_1382);
						FAILURE(BgL_auxz00_2438, BFALSE, BFALSE);
					}
				return
					BGl_realzd2ze3ieeezd2stringze3zz__r4_numbers_6_5_flonumz00
					(BgL_auxz00_2435);
			}
		}

	}



/* ieee-string->double */
	BGL_EXPORTED_DEF double
		BGl_ieeezd2stringzd2ze3doubleze3zz__r4_numbers_6_5_flonumz00(obj_t
		BgL_stringz00_72)
	{
		{	/* Ieee/flonum.scm 653 */
			BGL_TAIL return bgl_ieee_string_to_double(BgL_stringz00_72);
		}

	}



/* &ieee-string->double */
	obj_t BGl_z62ieeezd2stringzd2ze3doublez81zz__r4_numbers_6_5_flonumz00(obj_t
		BgL_envz00_1383, obj_t BgL_stringz00_1384)
	{
		{	/* Ieee/flonum.scm 653 */
			{	/* Ieee/flonum.scm 654 */
				double BgL_tmpz00_2444;

				{	/* Ieee/flonum.scm 654 */
					obj_t BgL_auxz00_2445;

					if (STRINGP(BgL_stringz00_1384))
						{	/* Ieee/flonum.scm 654 */
							BgL_auxz00_2445 = BgL_stringz00_1384;
						}
					else
						{
							obj_t BgL_auxz00_2448;

							BgL_auxz00_2448 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(28824L),
								BGl_string1627z00zz__r4_numbers_6_5_flonumz00,
								BGl_string1624z00zz__r4_numbers_6_5_flonumz00,
								BgL_stringz00_1384);
							FAILURE(BgL_auxz00_2448, BFALSE, BFALSE);
						}
					BgL_tmpz00_2444 =
						BGl_ieeezd2stringzd2ze3doubleze3zz__r4_numbers_6_5_flonumz00
						(BgL_auxz00_2445);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_2444);
			}
		}

	}



/* double->ieee-string */
	BGL_EXPORTED_DEF obj_t
		BGl_doublezd2ze3ieeezd2stringze3zz__r4_numbers_6_5_flonumz00(double
		BgL_doublez00_73)
	{
		{	/* Ieee/flonum.scm 659 */
			BGL_TAIL return bgl_double_to_ieee_string(BgL_doublez00_73);
		}

	}



/* &double->ieee-string */
	obj_t BGl_z62doublezd2ze3ieeezd2stringz81zz__r4_numbers_6_5_flonumz00(obj_t
		BgL_envz00_1385, obj_t BgL_doublez00_1386)
	{
		{	/* Ieee/flonum.scm 659 */
			{	/* Ieee/flonum.scm 660 */
				double BgL_auxz00_2455;

				{	/* Ieee/flonum.scm 660 */
					obj_t BgL_tmpz00_2456;

					if (REALP(BgL_doublez00_1386))
						{	/* Ieee/flonum.scm 660 */
							BgL_tmpz00_2456 = BgL_doublez00_1386;
						}
					else
						{
							obj_t BgL_auxz00_2459;

							BgL_auxz00_2459 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(29125L),
								BGl_string1628z00zz__r4_numbers_6_5_flonumz00,
								BGl_string1570z00zz__r4_numbers_6_5_flonumz00,
								BgL_doublez00_1386);
							FAILURE(BgL_auxz00_2459, BFALSE, BFALSE);
						}
					BgL_auxz00_2455 = REAL_TO_DOUBLE(BgL_tmpz00_2456);
				}
				return
					BGl_doublezd2ze3ieeezd2stringze3zz__r4_numbers_6_5_flonumz00
					(BgL_auxz00_2455);
			}
		}

	}



/* ieee-string->float */
	BGL_EXPORTED_DEF float
		BGl_ieeezd2stringzd2ze3floatze3zz__r4_numbers_6_5_flonumz00(obj_t
		BgL_stringz00_74)
	{
		{	/* Ieee/flonum.scm 665 */
			BGL_TAIL return bgl_ieee_string_to_float(BgL_stringz00_74);
		}

	}



/* &ieee-string->float */
	obj_t BGl_z62ieeezd2stringzd2ze3floatz81zz__r4_numbers_6_5_flonumz00(obj_t
		BgL_envz00_1387, obj_t BgL_stringz00_1388)
	{
		{	/* Ieee/flonum.scm 665 */
			{	/* Ieee/flonum.scm 666 */
				float BgL_tmpz00_2466;

				{	/* Ieee/flonum.scm 666 */
					obj_t BgL_auxz00_2467;

					if (STRINGP(BgL_stringz00_1388))
						{	/* Ieee/flonum.scm 666 */
							BgL_auxz00_2467 = BgL_stringz00_1388;
						}
					else
						{
							obj_t BgL_auxz00_2470;

							BgL_auxz00_2470 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(29425L),
								BGl_string1629z00zz__r4_numbers_6_5_flonumz00,
								BGl_string1624z00zz__r4_numbers_6_5_flonumz00,
								BgL_stringz00_1388);
							FAILURE(BgL_auxz00_2470, BFALSE, BFALSE);
						}
					BgL_tmpz00_2466 =
						BGl_ieeezd2stringzd2ze3floatze3zz__r4_numbers_6_5_flonumz00
						(BgL_auxz00_2467);
				}
				return FLOAT_TO_REAL(BgL_tmpz00_2466);
			}
		}

	}



/* float->ieee-string */
	BGL_EXPORTED_DEF obj_t
		BGl_floatzd2ze3ieeezd2stringze3zz__r4_numbers_6_5_flonumz00(float
		BgL_floatz00_75)
	{
		{	/* Ieee/flonum.scm 671 */
			BGL_TAIL return bgl_float_to_ieee_string(BgL_floatz00_75);
		}

	}



/* &float->ieee-string */
	obj_t BGl_z62floatzd2ze3ieeezd2stringz81zz__r4_numbers_6_5_flonumz00(obj_t
		BgL_envz00_1389, obj_t BgL_floatz00_1390)
	{
		{	/* Ieee/flonum.scm 671 */
			{	/* Ieee/flonum.scm 672 */
				float BgL_auxz00_2477;

				{	/* Ieee/flonum.scm 672 */
					obj_t BgL_tmpz00_2478;

					if (REALP(BgL_floatz00_1390))
						{	/* Ieee/flonum.scm 672 */
							BgL_tmpz00_2478 = BgL_floatz00_1390;
						}
					else
						{
							obj_t BgL_auxz00_2481;

							BgL_auxz00_2481 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(29723L),
								BGl_string1630z00zz__r4_numbers_6_5_flonumz00,
								BGl_string1570z00zz__r4_numbers_6_5_flonumz00,
								BgL_floatz00_1390);
							FAILURE(BgL_auxz00_2481, BFALSE, BFALSE);
						}
					BgL_auxz00_2477 = REAL_TO_FLOAT(BgL_tmpz00_2478);
				}
				return
					BGl_floatzd2ze3ieeezd2stringze3zz__r4_numbers_6_5_flonumz00
					(BgL_auxz00_2477);
			}
		}

	}



/* double->llong-bits */
	BGL_EXPORTED_DEF BGL_LONGLONG_T
		BGl_doublezd2ze3llongzd2bitsze3zz__r4_numbers_6_5_flonumz00(double
		BgL_nz00_76)
	{
		{	/* Ieee/flonum.scm 677 */
			return DOUBLE_TO_LLONG_BITS(BgL_nz00_76);
		}

	}



/* &double->llong-bits */
	obj_t BGl_z62doublezd2ze3llongzd2bitsz81zz__r4_numbers_6_5_flonumz00(obj_t
		BgL_envz00_1391, obj_t BgL_nz00_1392)
	{
		{	/* Ieee/flonum.scm 677 */
			{	/* Ieee/flonum.scm 678 */
				BGL_LONGLONG_T BgL_tmpz00_2488;

				{	/* Ieee/flonum.scm 678 */
					double BgL_auxz00_2489;

					{	/* Ieee/flonum.scm 678 */
						obj_t BgL_tmpz00_2490;

						if (REALP(BgL_nz00_1392))
							{	/* Ieee/flonum.scm 678 */
								BgL_tmpz00_2490 = BgL_nz00_1392;
							}
						else
							{
								obj_t BgL_auxz00_2493;

								BgL_auxz00_2493 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(30031L),
									BGl_string1631z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00, BgL_nz00_1392);
								FAILURE(BgL_auxz00_2493, BFALSE, BFALSE);
							}
						BgL_auxz00_2489 = REAL_TO_DOUBLE(BgL_tmpz00_2490);
					}
					BgL_tmpz00_2488 =
						BGl_doublezd2ze3llongzd2bitsze3zz__r4_numbers_6_5_flonumz00
						(BgL_auxz00_2489);
				}
				return make_bllong(BgL_tmpz00_2488);
			}
		}

	}



/* llong-bits->double */
	BGL_EXPORTED_DEF double
		BGl_llongzd2bitszd2ze3doubleze3zz__r4_numbers_6_5_flonumz00(BGL_LONGLONG_T
		BgL_nz00_77)
	{
		{	/* Ieee/flonum.scm 683 */
			return LLONG_BITS_TO_DOUBLE(BgL_nz00_77);
		}

	}



/* &llong-bits->double */
	obj_t BGl_z62llongzd2bitszd2ze3doublez81zz__r4_numbers_6_5_flonumz00(obj_t
		BgL_envz00_1393, obj_t BgL_nz00_1394)
	{
		{	/* Ieee/flonum.scm 683 */
			{	/* Ieee/flonum.scm 684 */
				double BgL_tmpz00_2501;

				{	/* Ieee/flonum.scm 684 */
					BGL_LONGLONG_T BgL_auxz00_2502;

					{	/* Ieee/flonum.scm 684 */
						obj_t BgL_tmpz00_2503;

						if (LLONGP(BgL_nz00_1394))
							{	/* Ieee/flonum.scm 684 */
								BgL_tmpz00_2503 = BgL_nz00_1394;
							}
						else
							{
								obj_t BgL_auxz00_2506;

								BgL_auxz00_2506 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(30335L),
									BGl_string1632z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1633z00zz__r4_numbers_6_5_flonumz00, BgL_nz00_1394);
								FAILURE(BgL_auxz00_2506, BFALSE, BFALSE);
							}
						BgL_auxz00_2502 = BLLONG_TO_LLONG(BgL_tmpz00_2503);
					}
					BgL_tmpz00_2501 =
						BGl_llongzd2bitszd2ze3doubleze3zz__r4_numbers_6_5_flonumz00
						(BgL_auxz00_2502);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_2501);
			}
		}

	}



/* float->int-bits */
	BGL_EXPORTED_DEF int
		BGl_floatzd2ze3intzd2bitsze3zz__r4_numbers_6_5_flonumz00(float BgL_nz00_78)
	{
		{	/* Ieee/flonum.scm 689 */
			return FLOAT_TO_INT_BITS(BgL_nz00_78);
		}

	}



/* &float->int-bits */
	obj_t BGl_z62floatzd2ze3intzd2bitsz81zz__r4_numbers_6_5_flonumz00(obj_t
		BgL_envz00_1395, obj_t BgL_nz00_1396)
	{
		{	/* Ieee/flonum.scm 689 */
			{	/* Ieee/flonum.scm 690 */
				int BgL_tmpz00_2514;

				{	/* Ieee/flonum.scm 690 */
					float BgL_auxz00_2515;

					{	/* Ieee/flonum.scm 690 */
						obj_t BgL_tmpz00_2516;

						if (REALP(BgL_nz00_1396))
							{	/* Ieee/flonum.scm 690 */
								BgL_tmpz00_2516 = BgL_nz00_1396;
							}
						else
							{
								obj_t BgL_auxz00_2519;

								BgL_auxz00_2519 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(30633L),
									BGl_string1634z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1570z00zz__r4_numbers_6_5_flonumz00, BgL_nz00_1396);
								FAILURE(BgL_auxz00_2519, BFALSE, BFALSE);
							}
						BgL_auxz00_2515 = REAL_TO_FLOAT(BgL_tmpz00_2516);
					}
					BgL_tmpz00_2514 =
						BGl_floatzd2ze3intzd2bitsze3zz__r4_numbers_6_5_flonumz00
						(BgL_auxz00_2515);
				}
				return BINT(BgL_tmpz00_2514);
			}
		}

	}



/* int-bits->float */
	BGL_EXPORTED_DEF float
		BGl_intzd2bitszd2ze3floatze3zz__r4_numbers_6_5_flonumz00(int BgL_nz00_79)
	{
		{	/* Ieee/flonum.scm 695 */
			return INT_BITS_TO_FLOAT(BgL_nz00_79);
		}

	}



/* &int-bits->float */
	obj_t BGl_z62intzd2bitszd2ze3floatz81zz__r4_numbers_6_5_flonumz00(obj_t
		BgL_envz00_1397, obj_t BgL_nz00_1398)
	{
		{	/* Ieee/flonum.scm 695 */
			{	/* Ieee/flonum.scm 696 */
				float BgL_tmpz00_2527;

				{	/* Ieee/flonum.scm 696 */
					int BgL_auxz00_2528;

					{	/* Ieee/flonum.scm 696 */
						obj_t BgL_tmpz00_2529;

						if (INTEGERP(BgL_nz00_1398))
							{	/* Ieee/flonum.scm 696 */
								BgL_tmpz00_2529 = BgL_nz00_1398;
							}
						else
							{
								obj_t BgL_auxz00_2532;

								BgL_auxz00_2532 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1568z00zz__r4_numbers_6_5_flonumz00, BINT(30928L),
									BGl_string1635z00zz__r4_numbers_6_5_flonumz00,
									BGl_string1636z00zz__r4_numbers_6_5_flonumz00, BgL_nz00_1398);
								FAILURE(BgL_auxz00_2532, BFALSE, BFALSE);
							}
						BgL_auxz00_2528 = CINT(BgL_tmpz00_2529);
					}
					BgL_tmpz00_2527 =
						BGl_intzd2bitszd2ze3floatze3zz__r4_numbers_6_5_flonumz00
						(BgL_auxz00_2528);
				}
				return FLOAT_TO_REAL(BgL_tmpz00_2527);
			}
		}

	}



/* randomfl */
	BGL_EXPORTED_DEF double BGl_randomflz00zz__r4_numbers_6_5_flonumz00(void)
	{
		{	/* Ieee/flonum.scm 701 */
			return RANDOMFL();
		}

	}



/* &randomfl */
	obj_t BGl_z62randomflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1399)
	{
		{	/* Ieee/flonum.scm 701 */
			return DOUBLE_TO_REAL(BGl_randomflz00zz__r4_numbers_6_5_flonumz00());
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__r4_numbers_6_5_flonumz00(void)
	{
		{	/* Ieee/flonum.scm 14 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1637z00zz__r4_numbers_6_5_flonumz00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1637z00zz__r4_numbers_6_5_flonumz00));
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(473309430L,
				BSTRING_TO_STRING(BGl_string1637z00zz__r4_numbers_6_5_flonumz00));
			return
				BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00
				(268155843L,
				BSTRING_TO_STRING(BGl_string1637z00zz__r4_numbers_6_5_flonumz00));
		}

	}

#ifdef __cplusplus
}
#endif
