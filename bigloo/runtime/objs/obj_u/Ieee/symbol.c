/*===========================================================================*/
/*   (Ieee/symbol.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Ieee/symbol.scm -indent -o objs/obj_u/Ieee/symbol.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___R4_SYMBOLS_6_4_TYPE_DEFINITIONS
#define BGL___R4_SYMBOLS_6_4_TYPE_DEFINITIONS
#endif													// BGL___R4_SYMBOLS_6_4_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t BGl_putpropz12z12zz__r4_symbols_6_4z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_keywordzd2ze3stringz12z23zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_z62rempropz12z70zz__r4_symbols_6_4z00(obj_t, obj_t, obj_t);
	extern obj_t bstring_to_keyword(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__r4_symbols_6_4z00 = BUNSPEC;
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_EXPORTED_DECL bool_t BGl_keywordzf3zf3zz__r4_symbols_6_4z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_keywordzd2ze3stringz31zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_z62keywordzd2ze3stringz53zz__r4_symbols_6_4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_stringzd2ze3symbolz31zz__r4_symbols_6_4z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_symbolzd2ze3keywordz31zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_z62symbolzd2ze3keywordz53zz__r4_symbols_6_4z00(obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zz__r4_symbols_6_4z00(void);
	static obj_t BGl_z62symbolzd2plistzb0zz__r4_symbols_6_4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_symbolzf3zf3zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_z62symbolzf3z91zz__r4_symbols_6_4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_stringzd2ze3keywordz31zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_z62stringzd2ze3keywordz53zz__r4_symbols_6_4z00(obj_t, obj_t);
	static obj_t BGl_z62symbolzd2ze3stringz53zz__r4_symbols_6_4z00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zz__r4_symbols_6_4z00(void);
	extern obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_z62putpropz12z70zz__r4_symbols_6_4z00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__r4_symbols_6_4z00(void);
	static obj_t BGl_z62keywordzd2ze3stringz12z41zz__r4_symbols_6_4z00(obj_t,
		obj_t);
	static obj_t BGl_gczd2rootszd2initz00zz__r4_symbols_6_4z00(void);
	BGL_EXPORTED_DECL obj_t BGl_symbolzd2plistzd2zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_objectzd2initzd2zz__r4_symbols_6_4z00(void);
	static obj_t BGl_symbolzd2appendze70z35zz__r4_symbols_6_4z00(obj_t);
	extern obj_t bgl_gensym(obj_t);
	static obj_t BGl_z62symbolzd2appendzb0zz__r4_symbols_6_4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_getpropz00zz__r4_symbols_6_4z00(obj_t, obj_t);
	static obj_t BGl_z62getpropz62zz__r4_symbols_6_4z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62keywordzf3z91zz__r4_symbols_6_4z00(obj_t, obj_t);
	static long BGl_za2gensymzd2counterza2zd2zz__r4_symbols_6_4z00 = 0L;
	BGL_EXPORTED_DECL obj_t
		BGl_symbolzd2ze3stringz12z23zz__r4_symbols_6_4z00(obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_z62symbolzd2ze3stringz12z41zz__r4_symbols_6_4z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_rempropz12z12zz__r4_symbols_6_4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_methodzd2initzd2zz__r4_symbols_6_4z00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_keywordzd2ze3symbolz31zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_z62keywordzd2ze3symbolz53zz__r4_symbols_6_4z00(obj_t, obj_t);
	static obj_t BGl_z62stringzd2ze3symbolzd2ciz81zz__r4_symbols_6_4z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_symbolzd2ze3stringz31zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl__gensymz00zz__r4_symbols_6_4z00(obj_t, obj_t);
	static obj_t BGl_z62stringzd2ze3symbolz53zz__r4_symbols_6_4z00(obj_t, obj_t);
	extern obj_t string_append(obj_t, obj_t);
	extern obj_t BGl_stringzd2upcasezd2zz__r4_strings_6_7z00(obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_stringzd2ze3symbolzd2cize3zz__r4_symbols_6_4z00(obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2ze3keywordzd2envze3zz__r4_symbols_6_4z00,
		BgL_bgl_za762stringza7d2za7e3k1670za7,
		BGl_z62stringzd2ze3keywordz53zz__r4_symbols_6_4z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rempropz12zd2envzc0zz__r4_symbols_6_4z00,
		BgL_bgl_za762rempropza712za7701671za7,
		BGl_z62rempropz12z70zz__r4_symbols_6_4z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_symbolzd2plistzd2envz00zz__r4_symbols_6_4z00,
		BgL_bgl_za762symbolza7d2plis1672z00,
		BGl_z62symbolzd2plistzb0zz__r4_symbols_6_4z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_keywordzf3zd2envz21zz__r4_symbols_6_4z00,
		BgL_bgl_za762keywordza7f3za7911673za7,
		BGl_z62keywordzf3z91zz__r4_symbols_6_4z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_keywordzd2ze3symbolzd2envze3zz__r4_symbols_6_4z00,
		BgL_bgl_za762keywordza7d2za7e31674za7,
		BGl_z62keywordzd2ze3symbolz53zz__r4_symbols_6_4z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2ze3symbolzd2cizd2envz31zz__r4_symbols_6_4z00,
		BgL_bgl_za762stringza7d2za7e3s1675za7,
		BGl_z62stringzd2ze3symbolzd2ciz81zz__r4_symbols_6_4z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2ze3symbolzd2envze3zz__r4_symbols_6_4z00,
		BgL_bgl_za762stringza7d2za7e3s1676za7,
		BGl_z62stringzd2ze3symbolz53zz__r4_symbols_6_4z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_gensymzd2envzd2zz__r4_symbols_6_4z00,
		BgL_bgl__gensymza700za7za7__r41677za7, opt_generic_entry,
		BGl__gensymz00zz__r4_symbols_6_4z00, BFALSE, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_symbolzf3zd2envz21zz__r4_symbols_6_4z00,
		BgL_bgl_za762symbolza7f3za791za71678z00,
		BGl_z62symbolzf3z91zz__r4_symbols_6_4z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1650z00zz__r4_symbols_6_4z00,
		BgL_bgl_string1650za700za7za7_1679za7,
		"/tmp/bigloo/runtime/Ieee/symbol.scm", 35);
	      DEFINE_STRING(BGl_string1651z00zz__r4_symbols_6_4z00,
		BgL_bgl_string1651za700za7za7_1680za7, "&symbol->string", 15);
	      DEFINE_STRING(BGl_string1652z00zz__r4_symbols_6_4z00,
		BgL_bgl_string1652za700za7za7_1681za7, "symbol", 6);
	      DEFINE_STRING(BGl_string1653z00zz__r4_symbols_6_4z00,
		BgL_bgl_string1653za700za7za7_1682za7, "&symbol->string!", 16);
	      DEFINE_STRING(BGl_string1654z00zz__r4_symbols_6_4z00,
		BgL_bgl_string1654za700za7za7_1683za7, "&string->symbol", 15);
	      DEFINE_STRING(BGl_string1655z00zz__r4_symbols_6_4z00,
		BgL_bgl_string1655za700za7za7_1684za7, "bstring", 7);
	      DEFINE_STRING(BGl_string1656z00zz__r4_symbols_6_4z00,
		BgL_bgl_string1656za700za7za7_1685za7, "&string->symbol-ci", 18);
	      DEFINE_STRING(BGl_string1657z00zz__r4_symbols_6_4z00,
		BgL_bgl_string1657za700za7za7_1686za7, "", 0);
	      DEFINE_STRING(BGl_string1658z00zz__r4_symbols_6_4z00,
		BgL_bgl_string1658za700za7za7_1687za7, "gensym", 6);
	      DEFINE_STRING(BGl_string1659z00zz__r4_symbols_6_4z00,
		BgL_bgl_string1659za700za7za7_1688za7, "Illegal argument", 16);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_symbolzd2ze3stringzd2envze3zz__r4_symbols_6_4z00,
		BgL_bgl_za762symbolza7d2za7e3s1689za7,
		BGl_z62symbolzd2ze3stringz53zz__r4_symbols_6_4z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_putpropz12zd2envzc0zz__r4_symbols_6_4z00,
		BgL_bgl_za762putpropza712za7701690za7,
		BGl_z62putpropz12z70zz__r4_symbols_6_4z00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_symbolzd2ze3keywordzd2envze3zz__r4_symbols_6_4z00,
		BgL_bgl_za762symbolza7d2za7e3k1691za7,
		BGl_z62symbolzd2ze3keywordz53zz__r4_symbols_6_4z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1660z00zz__r4_symbols_6_4z00,
		BgL_bgl_string1660za700za7za7_1692za7, "symbol-plist", 12);
	      DEFINE_STRING(BGl_string1661z00zz__r4_symbols_6_4z00,
		BgL_bgl_string1661za700za7za7_1693za7,
		"argument is neither a symbol nor a keyword", 42);
	      DEFINE_STRING(BGl_string1662z00zz__r4_symbols_6_4z00,
		BgL_bgl_string1662za700za7za7_1694za7, "getprop", 7);
	      DEFINE_STRING(BGl_string1663z00zz__r4_symbols_6_4z00,
		BgL_bgl_string1663za700za7za7_1695za7, "&keyword->string", 16);
	      DEFINE_STRING(BGl_string1664z00zz__r4_symbols_6_4z00,
		BgL_bgl_string1664za700za7za7_1696za7, "keyword", 7);
	      DEFINE_STRING(BGl_string1665z00zz__r4_symbols_6_4z00,
		BgL_bgl_string1665za700za7za7_1697za7, "&keyword->string!", 17);
	      DEFINE_STRING(BGl_string1666z00zz__r4_symbols_6_4z00,
		BgL_bgl_string1666za700za7za7_1698za7, "&string->keyword", 16);
	      DEFINE_STRING(BGl_string1667z00zz__r4_symbols_6_4z00,
		BgL_bgl_string1667za700za7za7_1699za7, "&symbol->keyword", 16);
	      DEFINE_STRING(BGl_string1668z00zz__r4_symbols_6_4z00,
		BgL_bgl_string1668za700za7za7_1700za7, "&keyword->symbol", 16);
	      DEFINE_STRING(BGl_string1669z00zz__r4_symbols_6_4z00,
		BgL_bgl_string1669za700za7za7_1701za7, "__r4_symbols_6_4", 16);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_getpropzd2envzd2zz__r4_symbols_6_4z00,
		BgL_bgl_za762getpropza762za7za7_1702z00,
		BGl_z62getpropz62zz__r4_symbols_6_4z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_symbolzd2ze3stringz12zd2envzf1zz__r4_symbols_6_4z00,
		BgL_bgl_za762symbolza7d2za7e3s1703za7,
		BGl_z62symbolzd2ze3stringz12z41zz__r4_symbols_6_4z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_keywordzd2ze3stringzd2envze3zz__r4_symbols_6_4z00,
		BgL_bgl_za762keywordza7d2za7e31704za7,
		BGl_z62keywordzd2ze3stringz53zz__r4_symbols_6_4z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_keywordzd2ze3stringz12zd2envzf1zz__r4_symbols_6_4z00,
		BgL_bgl_za762keywordza7d2za7e31705za7,
		BGl_z62keywordzd2ze3stringz12z41zz__r4_symbols_6_4z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_symbolzd2appendzd2envz00zz__r4_symbols_6_4z00,
		BgL_bgl_za762symbolza7d2appe1706z00, va_generic_entry,
		BGl_z62symbolzd2appendzb0zz__r4_symbols_6_4z00, BUNSPEC, -1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__r4_symbols_6_4z00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long
		BgL_checksumz00_1862, char *BgL_fromz00_1863)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__r4_symbols_6_4z00))
				{
					BGl_requirezd2initializa7ationz75zz__r4_symbols_6_4z00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__r4_symbols_6_4z00();
					BGl_importedzd2moduleszd2initz00zz__r4_symbols_6_4z00();
					return BGl_toplevelzd2initzd2zz__r4_symbols_6_4z00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__r4_symbols_6_4z00(void)
	{
		{	/* Ieee/symbol.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__r4_symbols_6_4z00(void)
	{
		{	/* Ieee/symbol.scm 14 */
			return (BGl_za2gensymzd2counterza2zd2zz__r4_symbols_6_4z00 =
				999L, BUNSPEC);
		}

	}



/* symbol? */
	BGL_EXPORTED_DEF bool_t BGl_symbolzf3zf3zz__r4_symbols_6_4z00(obj_t
		BgL_objz00_3)
	{
		{	/* Ieee/symbol.scm 166 */
			return SYMBOLP(BgL_objz00_3);
		}

	}



/* &symbol? */
	obj_t BGl_z62symbolzf3z91zz__r4_symbols_6_4z00(obj_t BgL_envz00_1806,
		obj_t BgL_objz00_1807)
	{
		{	/* Ieee/symbol.scm 166 */
			return BBOOL(BGl_symbolzf3zf3zz__r4_symbols_6_4z00(BgL_objz00_1807));
		}

	}



/* symbol->string */
	BGL_EXPORTED_DEF obj_t BGl_symbolzd2ze3stringz31zz__r4_symbols_6_4z00(obj_t
		BgL_symbolz00_4)
	{
		{	/* Ieee/symbol.scm 172 */
			{	/* Ieee/symbol.scm 173 */
				obj_t BgL_arg1166z00_1860;

				BgL_arg1166z00_1860 = SYMBOL_TO_STRING(BgL_symbolz00_4);
				return BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1166z00_1860);
			}
		}

	}



/* &symbol->string */
	obj_t BGl_z62symbolzd2ze3stringz53zz__r4_symbols_6_4z00(obj_t BgL_envz00_1808,
		obj_t BgL_symbolz00_1809)
	{
		{	/* Ieee/symbol.scm 172 */
			{	/* Ieee/symbol.scm 173 */
				obj_t BgL_auxz00_1876;

				if (SYMBOLP(BgL_symbolz00_1809))
					{	/* Ieee/symbol.scm 173 */
						BgL_auxz00_1876 = BgL_symbolz00_1809;
					}
				else
					{
						obj_t BgL_auxz00_1879;

						BgL_auxz00_1879 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1650z00zz__r4_symbols_6_4z00, BINT(7250L),
							BGl_string1651z00zz__r4_symbols_6_4z00,
							BGl_string1652z00zz__r4_symbols_6_4z00, BgL_symbolz00_1809);
						FAILURE(BgL_auxz00_1879, BFALSE, BFALSE);
					}
				return BGl_symbolzd2ze3stringz31zz__r4_symbols_6_4z00(BgL_auxz00_1876);
			}
		}

	}



/* symbol->string! */
	BGL_EXPORTED_DEF obj_t BGl_symbolzd2ze3stringz12z23zz__r4_symbols_6_4z00(obj_t
		BgL_symbolz00_5)
	{
		{	/* Ieee/symbol.scm 178 */
			return SYMBOL_TO_STRING(BgL_symbolz00_5);
		}

	}



/* &symbol->string! */
	obj_t BGl_z62symbolzd2ze3stringz12z41zz__r4_symbols_6_4z00(obj_t
		BgL_envz00_1810, obj_t BgL_symbolz00_1811)
	{
		{	/* Ieee/symbol.scm 178 */
			{	/* Ieee/symbol.scm 179 */
				obj_t BgL_auxz00_1885;

				if (SYMBOLP(BgL_symbolz00_1811))
					{	/* Ieee/symbol.scm 179 */
						BgL_auxz00_1885 = BgL_symbolz00_1811;
					}
				else
					{
						obj_t BgL_auxz00_1888;

						BgL_auxz00_1888 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1650z00zz__r4_symbols_6_4z00, BINT(7544L),
							BGl_string1653z00zz__r4_symbols_6_4z00,
							BGl_string1652z00zz__r4_symbols_6_4z00, BgL_symbolz00_1811);
						FAILURE(BgL_auxz00_1888, BFALSE, BFALSE);
					}
				return
					BGl_symbolzd2ze3stringz12z23zz__r4_symbols_6_4z00(BgL_auxz00_1885);
			}
		}

	}



/* string->symbol */
	BGL_EXPORTED_DEF obj_t BGl_stringzd2ze3symbolz31zz__r4_symbols_6_4z00(obj_t
		BgL_stringz00_6)
	{
		{	/* Ieee/symbol.scm 184 */
			BGL_TAIL return bstring_to_symbol(BgL_stringz00_6);
		}

	}



/* &string->symbol */
	obj_t BGl_z62stringzd2ze3symbolz53zz__r4_symbols_6_4z00(obj_t BgL_envz00_1812,
		obj_t BgL_stringz00_1813)
	{
		{	/* Ieee/symbol.scm 184 */
			{	/* Ieee/symbol.scm 187 */
				obj_t BgL_auxz00_1894;

				if (STRINGP(BgL_stringz00_1813))
					{	/* Ieee/symbol.scm 187 */
						BgL_auxz00_1894 = BgL_stringz00_1813;
					}
				else
					{
						obj_t BgL_auxz00_1897;

						BgL_auxz00_1897 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1650z00zz__r4_symbols_6_4z00, BINT(7888L),
							BGl_string1654z00zz__r4_symbols_6_4z00,
							BGl_string1655z00zz__r4_symbols_6_4z00, BgL_stringz00_1813);
						FAILURE(BgL_auxz00_1897, BFALSE, BFALSE);
					}
				return BGl_stringzd2ze3symbolz31zz__r4_symbols_6_4z00(BgL_auxz00_1894);
			}
		}

	}



/* string->symbol-ci */
	BGL_EXPORTED_DEF obj_t
		BGl_stringzd2ze3symbolzd2cize3zz__r4_symbols_6_4z00(obj_t BgL_stringz00_7)
	{
		{	/* Ieee/symbol.scm 194 */
			BGL_TAIL return
				bstring_to_symbol(BGl_stringzd2upcasezd2zz__r4_strings_6_7z00
				(BgL_stringz00_7));
		}

	}



/* &string->symbol-ci */
	obj_t BGl_z62stringzd2ze3symbolzd2ciz81zz__r4_symbols_6_4z00(obj_t
		BgL_envz00_1814, obj_t BgL_stringz00_1815)
	{
		{	/* Ieee/symbol.scm 194 */
			{	/* Ieee/symbol.scm 195 */
				obj_t BgL_auxz00_1904;

				if (STRINGP(BgL_stringz00_1815))
					{	/* Ieee/symbol.scm 195 */
						BgL_auxz00_1904 = BgL_stringz00_1815;
					}
				else
					{
						obj_t BgL_auxz00_1907;

						BgL_auxz00_1907 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1650z00zz__r4_symbols_6_4z00, BINT(8241L),
							BGl_string1656z00zz__r4_symbols_6_4z00,
							BGl_string1655z00zz__r4_symbols_6_4z00, BgL_stringz00_1815);
						FAILURE(BgL_auxz00_1907, BFALSE, BFALSE);
					}
				return
					BGl_stringzd2ze3symbolzd2cize3zz__r4_symbols_6_4z00(BgL_auxz00_1904);
			}
		}

	}



/* symbol-append */
	BGL_EXPORTED_DEF obj_t BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(obj_t
		BgL_listz00_8)
	{
		{	/* Ieee/symbol.scm 200 */
			{	/* Ieee/symbol.scm 202 */
				obj_t BgL_arg1172z00_1086;

				if (NULLP(BgL_listz00_8))
					{	/* Ieee/symbol.scm 202 */
						BgL_arg1172z00_1086 = BGl_string1657z00zz__r4_symbols_6_4z00;
					}
				else
					{	/* Ieee/symbol.scm 202 */
						BgL_arg1172z00_1086 =
							BGl_symbolzd2appendze70z35zz__r4_symbols_6_4z00(BgL_listz00_8);
					}
				return bstring_to_symbol(BgL_arg1172z00_1086);
			}
		}

	}



/* symbol-append~0 */
	obj_t BGl_symbolzd2appendze70z35zz__r4_symbols_6_4z00(obj_t BgL_listz00_1089)
	{
		{	/* Ieee/symbol.scm 204 */
			if (NULLP(CDR(((obj_t) BgL_listz00_1089))))
				{	/* Ieee/symbol.scm 206 */
					obj_t BgL_arg1187z00_1093;

					BgL_arg1187z00_1093 = CAR(((obj_t) BgL_listz00_1089));
					{	/* Ieee/symbol.scm 173 */
						obj_t BgL_arg1166z00_1562;

						BgL_arg1166z00_1562 =
							SYMBOL_TO_STRING(((obj_t) BgL_arg1187z00_1093));
						return
							BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1166z00_1562);
					}
				}
			else
				{	/* Ieee/symbol.scm 207 */
					obj_t BgL_arg1188z00_1094;
					obj_t BgL_arg1189z00_1095;

					{	/* Ieee/symbol.scm 207 */
						obj_t BgL_arg1190z00_1096;

						BgL_arg1190z00_1096 = CAR(((obj_t) BgL_listz00_1089));
						{	/* Ieee/symbol.scm 173 */
							obj_t BgL_arg1166z00_1565;

							BgL_arg1166z00_1565 =
								SYMBOL_TO_STRING(((obj_t) BgL_arg1190z00_1096));
							BgL_arg1188z00_1094 =
								BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1166z00_1565);
						}
					}
					{	/* Ieee/symbol.scm 208 */
						obj_t BgL_arg1191z00_1097;

						BgL_arg1191z00_1097 = CDR(((obj_t) BgL_listz00_1089));
						BgL_arg1189z00_1095 =
							BGl_symbolzd2appendze70z35zz__r4_symbols_6_4z00
							(BgL_arg1191z00_1097);
					}
					return string_append(BgL_arg1188z00_1094, BgL_arg1189z00_1095);
				}
		}

	}



/* &symbol-append */
	obj_t BGl_z62symbolzd2appendzb0zz__r4_symbols_6_4z00(obj_t BgL_envz00_1816,
		obj_t BgL_listz00_1817)
	{
		{	/* Ieee/symbol.scm 200 */
			return BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(BgL_listz00_1817);
		}

	}



/* _gensym */
	obj_t BGl__gensymz00zz__r4_symbols_6_4z00(obj_t BgL_env1089z00_11,
		obj_t BgL_opt1088z00_10)
	{
		{	/* Ieee/symbol.scm 218 */
			{	/* Ieee/symbol.scm 218 */

				switch (VECTOR_LENGTH(BgL_opt1088z00_10))
					{
					case 0L:

						{	/* Ieee/symbol.scm 218 */

							return BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
						}
						break;
					case 1L:

						{	/* Ieee/symbol.scm 218 */
							obj_t BgL_argz00_1103;

							BgL_argz00_1103 = VECTOR_REF(BgL_opt1088z00_10, 0L);
							{	/* Ieee/symbol.scm 218 */

								return BGl_gensymz00zz__r4_symbols_6_4z00(BgL_argz00_1103);
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* gensym */
	BGL_EXPORTED_DEF obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t BgL_argz00_9)
	{
		{	/* Ieee/symbol.scm 218 */
			{	/* Ieee/symbol.scm 221 */
				obj_t BgL_argz00_1104;

				if (CBOOL(BgL_argz00_9))
					{	/* Ieee/symbol.scm 222 */
						if (SYMBOLP(BgL_argz00_9))
							{	/* Ieee/symbol.scm 173 */
								obj_t BgL_arg1166z00_1569;

								BgL_arg1166z00_1569 = SYMBOL_TO_STRING(BgL_argz00_9);
								BgL_argz00_1104 =
									BGl_stringzd2copyzd2zz__r4_strings_6_7z00
									(BgL_arg1166z00_1569);
							}
						else
							{	/* Ieee/symbol.scm 223 */
								if (STRINGP(BgL_argz00_9))
									{	/* Ieee/symbol.scm 224 */
										BgL_argz00_1104 = BgL_argz00_9;
									}
								else
									{	/* Ieee/symbol.scm 224 */
										BgL_argz00_1104 =
											BGl_errorz00zz__errorz00
											(BGl_string1658z00zz__r4_symbols_6_4z00,
											BGl_string1659z00zz__r4_symbols_6_4z00, BgL_argz00_9);
									}
							}
					}
				else
					{	/* Ieee/symbol.scm 222 */
						BgL_argz00_1104 = BgL_argz00_9;
					}
				BGL_TAIL return bgl_gensym(BgL_argz00_1104);
			}
		}

	}



/* symbol-plist */
	BGL_EXPORTED_DEF obj_t BGl_symbolzd2plistzd2zz__r4_symbols_6_4z00(obj_t
		BgL_symbolz00_12)
	{
		{	/* Ieee/symbol.scm 244 */
			if (SYMBOLP(BgL_symbolz00_12))
				{	/* Ieee/symbol.scm 246 */
					return GET_SYMBOL_PLIST(BgL_symbolz00_12);
				}
			else
				{	/* Ieee/symbol.scm 246 */
					if (KEYWORDP(BgL_symbolz00_12))
						{	/* Ieee/symbol.scm 248 */
							return GET_KEYWORD_PLIST(BgL_symbolz00_12);
						}
					else
						{	/* Ieee/symbol.scm 248 */
							return
								BGl_errorz00zz__errorz00(BGl_string1660z00zz__r4_symbols_6_4z00,
								BGl_string1661z00zz__r4_symbols_6_4z00, BgL_symbolz00_12);
						}
				}
		}

	}



/* &symbol-plist */
	obj_t BGl_z62symbolzd2plistzb0zz__r4_symbols_6_4z00(obj_t BgL_envz00_1818,
		obj_t BgL_symbolz00_1819)
	{
		{	/* Ieee/symbol.scm 244 */
			return BGl_symbolzd2plistzd2zz__r4_symbols_6_4z00(BgL_symbolz00_1819);
		}

	}



/* getprop */
	BGL_EXPORTED_DEF obj_t BGl_getpropz00zz__r4_symbols_6_4z00(obj_t
		BgL_symbolz00_13, obj_t BgL_keyz00_14)
	{
		{	/* Ieee/symbol.scm 258 */
			{	/* Ieee/symbol.scm 259 */
				bool_t BgL_test1719z00_1958;

				if (SYMBOLP(BgL_symbolz00_13))
					{	/* Ieee/symbol.scm 259 */
						BgL_test1719z00_1958 = ((bool_t) 1);
					}
				else
					{	/* Ieee/symbol.scm 259 */
						BgL_test1719z00_1958 = KEYWORDP(BgL_symbolz00_13);
					}
				if (BgL_test1719z00_1958)
					{	/* Ieee/symbol.scm 260 */
						obj_t BgL_g1040z00_1111;

						if (SYMBOLP(BgL_symbolz00_13))
							{	/* Ieee/symbol.scm 246 */
								BgL_g1040z00_1111 = GET_SYMBOL_PLIST(BgL_symbolz00_13);
							}
						else
							{	/* Ieee/symbol.scm 246 */
								if (KEYWORDP(BgL_symbolz00_13))
									{	/* Ieee/symbol.scm 248 */
										BgL_g1040z00_1111 = GET_KEYWORD_PLIST(BgL_symbolz00_13);
									}
								else
									{	/* Ieee/symbol.scm 248 */
										BgL_g1040z00_1111 =
											BGl_errorz00zz__errorz00
											(BGl_string1660z00zz__r4_symbols_6_4z00,
											BGl_string1661z00zz__r4_symbols_6_4z00, BgL_symbolz00_13);
									}
							}
						{
							obj_t BgL_plz00_1113;

							BgL_plz00_1113 = BgL_g1040z00_1111;
						BgL_zc3z04anonymousza31200ze3z87_1114:
							if (NULLP(BgL_plz00_1113))
								{	/* Ieee/symbol.scm 262 */
									return BFALSE;
								}
							else
								{	/* Ieee/symbol.scm 262 */
									if ((CAR(((obj_t) BgL_plz00_1113)) == BgL_keyz00_14))
										{	/* Ieee/symbol.scm 265 */
											obj_t BgL_pairz00_1578;

											BgL_pairz00_1578 = CDR(((obj_t) BgL_plz00_1113));
											return CAR(BgL_pairz00_1578);
										}
									else
										{	/* Ieee/symbol.scm 267 */
											obj_t BgL_arg1206z00_1118;

											{	/* Ieee/symbol.scm 267 */
												obj_t BgL_pairz00_1582;

												BgL_pairz00_1582 = CDR(((obj_t) BgL_plz00_1113));
												BgL_arg1206z00_1118 = CDR(BgL_pairz00_1582);
											}
											{
												obj_t BgL_plz00_1981;

												BgL_plz00_1981 = BgL_arg1206z00_1118;
												BgL_plz00_1113 = BgL_plz00_1981;
												goto BgL_zc3z04anonymousza31200ze3z87_1114;
											}
										}
								}
						}
					}
				else
					{	/* Ieee/symbol.scm 259 */
						return
							BGl_errorz00zz__errorz00(BGl_string1662z00zz__r4_symbols_6_4z00,
							BGl_string1661z00zz__r4_symbols_6_4z00, BgL_symbolz00_13);
					}
			}
		}

	}



/* &getprop */
	obj_t BGl_z62getpropz62zz__r4_symbols_6_4z00(obj_t BgL_envz00_1820,
		obj_t BgL_symbolz00_1821, obj_t BgL_keyz00_1822)
	{
		{	/* Ieee/symbol.scm 258 */
			return
				BGl_getpropz00zz__r4_symbols_6_4z00(BgL_symbolz00_1821,
				BgL_keyz00_1822);
		}

	}



/* putprop! */
	BGL_EXPORTED_DEF obj_t BGl_putpropz12z12zz__r4_symbols_6_4z00(obj_t
		BgL_symbolz00_15, obj_t BgL_keyz00_16, obj_t BgL_valz00_17)
	{
		{	/* Ieee/symbol.scm 273 */
			{	/* Ieee/symbol.scm 274 */
				bool_t BgL_test1725z00_1984;

				if (SYMBOLP(BgL_symbolz00_15))
					{	/* Ieee/symbol.scm 274 */
						BgL_test1725z00_1984 = ((bool_t) 1);
					}
				else
					{	/* Ieee/symbol.scm 274 */
						BgL_test1725z00_1984 = KEYWORDP(BgL_symbolz00_15);
					}
				if (BgL_test1725z00_1984)
					{	/* Ieee/symbol.scm 275 */
						obj_t BgL_g1041z00_1124;

						if (SYMBOLP(BgL_symbolz00_15))
							{	/* Ieee/symbol.scm 246 */
								BgL_g1041z00_1124 = GET_SYMBOL_PLIST(BgL_symbolz00_15);
							}
						else
							{	/* Ieee/symbol.scm 246 */
								if (KEYWORDP(BgL_symbolz00_15))
									{	/* Ieee/symbol.scm 248 */
										BgL_g1041z00_1124 = GET_KEYWORD_PLIST(BgL_symbolz00_15);
									}
								else
									{	/* Ieee/symbol.scm 248 */
										BgL_g1041z00_1124 =
											BGl_errorz00zz__errorz00
											(BGl_string1660z00zz__r4_symbols_6_4z00,
											BGl_string1661z00zz__r4_symbols_6_4z00, BgL_symbolz00_15);
									}
							}
						{
							obj_t BgL_plz00_1126;

							BgL_plz00_1126 = BgL_g1041z00_1124;
						BgL_zc3z04anonymousza31211ze3z87_1127:
							if (NULLP(BgL_plz00_1126))
								{	/* Ieee/symbol.scm 278 */
									obj_t BgL_newz00_1129;

									{	/* Ieee/symbol.scm 278 */
										obj_t BgL_arg1215z00_1131;

										{	/* Ieee/symbol.scm 278 */
											obj_t BgL_arg1216z00_1132;

											if (SYMBOLP(BgL_symbolz00_15))
												{	/* Ieee/symbol.scm 246 */
													BgL_arg1216z00_1132 =
														GET_SYMBOL_PLIST(BgL_symbolz00_15);
												}
											else
												{	/* Ieee/symbol.scm 246 */
													if (KEYWORDP(BgL_symbolz00_15))
														{	/* Ieee/symbol.scm 248 */
															BgL_arg1216z00_1132 =
																GET_KEYWORD_PLIST(BgL_symbolz00_15);
														}
													else
														{	/* Ieee/symbol.scm 248 */
															BgL_arg1216z00_1132 =
																BGl_errorz00zz__errorz00
																(BGl_string1660z00zz__r4_symbols_6_4z00,
																BGl_string1661z00zz__r4_symbols_6_4z00,
																BgL_symbolz00_15);
														}
												}
											BgL_arg1215z00_1131 =
												MAKE_YOUNG_PAIR(BgL_valz00_17, BgL_arg1216z00_1132);
										}
										BgL_newz00_1129 =
											MAKE_YOUNG_PAIR(BgL_keyz00_16, BgL_arg1215z00_1131);
									}
									if (SYMBOLP(BgL_symbolz00_15))
										{	/* Ieee/symbol.scm 279 */
											SET_SYMBOL_PLIST(BgL_symbolz00_15, BgL_newz00_1129);
										}
									else
										{	/* Ieee/symbol.scm 279 */
											SET_KEYWORD_PLIST(BgL_symbolz00_15, BgL_newz00_1129);
										}
									return BgL_newz00_1129;
								}
							else
								{	/* Ieee/symbol.scm 277 */
									if ((CAR(((obj_t) BgL_plz00_1126)) == BgL_keyz00_16))
										{	/* Ieee/symbol.scm 284 */
											obj_t BgL_arg1220z00_1135;

											BgL_arg1220z00_1135 = CDR(((obj_t) BgL_plz00_1126));
											{	/* Ieee/symbol.scm 284 */
												obj_t BgL_tmpz00_2016;

												BgL_tmpz00_2016 = ((obj_t) BgL_arg1220z00_1135);
												return SET_CAR(BgL_tmpz00_2016, BgL_valz00_17);
											}
										}
									else
										{	/* Ieee/symbol.scm 286 */
											obj_t BgL_arg1221z00_1136;

											{	/* Ieee/symbol.scm 286 */
												obj_t BgL_pairz00_1593;

												BgL_pairz00_1593 = CDR(((obj_t) BgL_plz00_1126));
												BgL_arg1221z00_1136 = CDR(BgL_pairz00_1593);
											}
											{
												obj_t BgL_plz00_2022;

												BgL_plz00_2022 = BgL_arg1221z00_1136;
												BgL_plz00_1126 = BgL_plz00_2022;
												goto BgL_zc3z04anonymousza31211ze3z87_1127;
											}
										}
								}
						}
					}
				else
					{	/* Ieee/symbol.scm 274 */
						BGL_TAIL return
							BGl_errorz00zz__errorz00(BGl_string1662z00zz__r4_symbols_6_4z00,
							BGl_string1661z00zz__r4_symbols_6_4z00, BgL_symbolz00_15);
					}
			}
		}

	}



/* &putprop! */
	obj_t BGl_z62putpropz12z70zz__r4_symbols_6_4z00(obj_t BgL_envz00_1823,
		obj_t BgL_symbolz00_1824, obj_t BgL_keyz00_1825, obj_t BgL_valz00_1826)
	{
		{	/* Ieee/symbol.scm 273 */
			return
				BGl_putpropz12z12zz__r4_symbols_6_4z00(BgL_symbolz00_1824,
				BgL_keyz00_1825, BgL_valz00_1826);
		}

	}



/* remprop! */
	BGL_EXPORTED_DEF obj_t BGl_rempropz12z12zz__r4_symbols_6_4z00(obj_t
		BgL_symbolz00_18, obj_t BgL_keyz00_19)
	{
		{	/* Ieee/symbol.scm 292 */
			{	/* Ieee/symbol.scm 293 */
				bool_t BgL_test1734z00_2025;

				if (SYMBOLP(BgL_symbolz00_18))
					{	/* Ieee/symbol.scm 293 */
						BgL_test1734z00_2025 = ((bool_t) 1);
					}
				else
					{	/* Ieee/symbol.scm 293 */
						BgL_test1734z00_2025 = KEYWORDP(BgL_symbolz00_18);
					}
				if (BgL_test1734z00_2025)
					{	/* Ieee/symbol.scm 294 */
						obj_t BgL_g1043z00_1143;

						if (SYMBOLP(BgL_symbolz00_18))
							{	/* Ieee/symbol.scm 246 */
								BgL_g1043z00_1143 = GET_SYMBOL_PLIST(BgL_symbolz00_18);
							}
						else
							{	/* Ieee/symbol.scm 246 */
								if (KEYWORDP(BgL_symbolz00_18))
									{	/* Ieee/symbol.scm 248 */
										BgL_g1043z00_1143 = GET_KEYWORD_PLIST(BgL_symbolz00_18);
									}
								else
									{	/* Ieee/symbol.scm 248 */
										BgL_g1043z00_1143 =
											BGl_errorz00zz__errorz00
											(BGl_string1660z00zz__r4_symbols_6_4z00,
											BGl_string1661z00zz__r4_symbols_6_4z00, BgL_symbolz00_18);
									}
							}
						{
							obj_t BgL_oldz00_1145;
							obj_t BgL_lz00_1146;

							BgL_oldz00_1145 = BNIL;
							BgL_lz00_1146 = BgL_g1043z00_1143;
						BgL_zc3z04anonymousza31226ze3z87_1147:
							if (NULLP(BgL_lz00_1146))
								{	/* Ieee/symbol.scm 297 */
									return BFALSE;
								}
							else
								{	/* Ieee/symbol.scm 297 */
									if ((CAR(((obj_t) BgL_lz00_1146)) == BgL_keyz00_19))
										{	/* Ieee/symbol.scm 299 */
											if (PAIRP(BgL_oldz00_1145))
												{	/* Ieee/symbol.scm 302 */
													obj_t BgL_arg1231z00_1152;
													obj_t BgL_arg1232z00_1153;

													BgL_arg1231z00_1152 = CDR(BgL_oldz00_1145);
													{	/* Ieee/symbol.scm 302 */
														obj_t BgL_pairz00_1601;

														BgL_pairz00_1601 = CDR(((obj_t) BgL_lz00_1146));
														BgL_arg1232z00_1153 = CDR(BgL_pairz00_1601);
													}
													{	/* Ieee/symbol.scm 302 */
														obj_t BgL_tmpz00_2048;

														BgL_tmpz00_2048 = ((obj_t) BgL_arg1231z00_1152);
														return
															SET_CDR(BgL_tmpz00_2048, BgL_arg1232z00_1153);
													}
												}
											else
												{	/* Ieee/symbol.scm 301 */
													if (SYMBOLP(BgL_symbolz00_18))
														{	/* Ieee/symbol.scm 305 */
															obj_t BgL_arg1234z00_1155;

															{	/* Ieee/symbol.scm 305 */
																obj_t BgL_pairz00_1606;

																BgL_pairz00_1606 = CDR(((obj_t) BgL_lz00_1146));
																BgL_arg1234z00_1155 = CDR(BgL_pairz00_1606);
															}
															return
																SET_SYMBOL_PLIST(BgL_symbolz00_18,
																BgL_arg1234z00_1155);
														}
													else
														{	/* Ieee/symbol.scm 306 */
															obj_t BgL_arg1236z00_1156;

															{	/* Ieee/symbol.scm 306 */
																obj_t BgL_pairz00_1610;

																BgL_pairz00_1610 = CDR(((obj_t) BgL_lz00_1146));
																BgL_arg1236z00_1156 = CDR(BgL_pairz00_1610);
															}
															return
																SET_KEYWORD_PLIST(BgL_symbolz00_18,
																BgL_arg1236z00_1156);
														}
												}
										}
									else
										{	/* Ieee/symbol.scm 308 */
											obj_t BgL_arg1238z00_1157;

											{	/* Ieee/symbol.scm 308 */
												obj_t BgL_pairz00_1614;

												BgL_pairz00_1614 = CDR(((obj_t) BgL_lz00_1146));
												BgL_arg1238z00_1157 = CDR(BgL_pairz00_1614);
											}
											{
												obj_t BgL_lz00_2065;
												obj_t BgL_oldz00_2064;

												BgL_oldz00_2064 = BgL_lz00_1146;
												BgL_lz00_2065 = BgL_arg1238z00_1157;
												BgL_lz00_1146 = BgL_lz00_2065;
												BgL_oldz00_1145 = BgL_oldz00_2064;
												goto BgL_zc3z04anonymousza31226ze3z87_1147;
											}
										}
								}
						}
					}
				else
					{	/* Ieee/symbol.scm 293 */
						return
							BGl_errorz00zz__errorz00(BGl_string1662z00zz__r4_symbols_6_4z00,
							BGl_string1661z00zz__r4_symbols_6_4z00, BgL_symbolz00_18);
					}
			}
		}

	}



/* &remprop! */
	obj_t BGl_z62rempropz12z70zz__r4_symbols_6_4z00(obj_t BgL_envz00_1827,
		obj_t BgL_symbolz00_1828, obj_t BgL_keyz00_1829)
	{
		{	/* Ieee/symbol.scm 292 */
			return
				BGl_rempropz12z12zz__r4_symbols_6_4z00(BgL_symbolz00_1828,
				BgL_keyz00_1829);
		}

	}



/* keyword? */
	BGL_EXPORTED_DEF bool_t BGl_keywordzf3zf3zz__r4_symbols_6_4z00(obj_t
		BgL_objz00_20)
	{
		{	/* Ieee/symbol.scm 314 */
			return KEYWORDP(BgL_objz00_20);
		}

	}



/* &keyword? */
	obj_t BGl_z62keywordzf3z91zz__r4_symbols_6_4z00(obj_t BgL_envz00_1830,
		obj_t BgL_objz00_1831)
	{
		{	/* Ieee/symbol.scm 314 */
			return BBOOL(BGl_keywordzf3zf3zz__r4_symbols_6_4z00(BgL_objz00_1831));
		}

	}



/* keyword->string */
	BGL_EXPORTED_DEF obj_t BGl_keywordzd2ze3stringz31zz__r4_symbols_6_4z00(obj_t
		BgL_keywordz00_21)
	{
		{	/* Ieee/symbol.scm 320 */
			{	/* Ieee/symbol.scm 321 */
				obj_t BgL_arg1242z00_1861;

				BgL_arg1242z00_1861 = KEYWORD_TO_STRING(BgL_keywordz00_21);
				return BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1242z00_1861);
			}
		}

	}



/* &keyword->string */
	obj_t BGl_z62keywordzd2ze3stringz53zz__r4_symbols_6_4z00(obj_t
		BgL_envz00_1832, obj_t BgL_keywordz00_1833)
	{
		{	/* Ieee/symbol.scm 320 */
			{	/* Ieee/symbol.scm 321 */
				obj_t BgL_auxz00_2073;

				if (KEYWORDP(BgL_keywordz00_1833))
					{	/* Ieee/symbol.scm 321 */
						BgL_auxz00_2073 = BgL_keywordz00_1833;
					}
				else
					{
						obj_t BgL_auxz00_2076;

						BgL_auxz00_2076 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1650z00zz__r4_symbols_6_4z00, BINT(12945L),
							BGl_string1663z00zz__r4_symbols_6_4z00,
							BGl_string1664z00zz__r4_symbols_6_4z00, BgL_keywordz00_1833);
						FAILURE(BgL_auxz00_2076, BFALSE, BFALSE);
					}
				return BGl_keywordzd2ze3stringz31zz__r4_symbols_6_4z00(BgL_auxz00_2073);
			}
		}

	}



/* keyword->string! */
	BGL_EXPORTED_DEF obj_t
		BGl_keywordzd2ze3stringz12z23zz__r4_symbols_6_4z00(obj_t BgL_keywordz00_22)
	{
		{	/* Ieee/symbol.scm 326 */
			return KEYWORD_TO_STRING(BgL_keywordz00_22);
		}

	}



/* &keyword->string! */
	obj_t BGl_z62keywordzd2ze3stringz12z41zz__r4_symbols_6_4z00(obj_t
		BgL_envz00_1834, obj_t BgL_keywordz00_1835)
	{
		{	/* Ieee/symbol.scm 326 */
			{	/* Ieee/symbol.scm 327 */
				obj_t BgL_auxz00_2082;

				if (KEYWORDP(BgL_keywordz00_1835))
					{	/* Ieee/symbol.scm 327 */
						BgL_auxz00_2082 = BgL_keywordz00_1835;
					}
				else
					{
						obj_t BgL_auxz00_2085;

						BgL_auxz00_2085 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1650z00zz__r4_symbols_6_4z00, BINT(13243L),
							BGl_string1665z00zz__r4_symbols_6_4z00,
							BGl_string1664z00zz__r4_symbols_6_4z00, BgL_keywordz00_1835);
						FAILURE(BgL_auxz00_2085, BFALSE, BFALSE);
					}
				return
					BGl_keywordzd2ze3stringz12z23zz__r4_symbols_6_4z00(BgL_auxz00_2082);
			}
		}

	}



/* string->keyword */
	BGL_EXPORTED_DEF obj_t BGl_stringzd2ze3keywordz31zz__r4_symbols_6_4z00(obj_t
		BgL_stringz00_23)
	{
		{	/* Ieee/symbol.scm 332 */
			BGL_TAIL return bstring_to_keyword(BgL_stringz00_23);
		}

	}



/* &string->keyword */
	obj_t BGl_z62stringzd2ze3keywordz53zz__r4_symbols_6_4z00(obj_t
		BgL_envz00_1836, obj_t BgL_stringz00_1837)
	{
		{	/* Ieee/symbol.scm 332 */
			{	/* Ieee/symbol.scm 335 */
				obj_t BgL_auxz00_2091;

				if (STRINGP(BgL_stringz00_1837))
					{	/* Ieee/symbol.scm 335 */
						BgL_auxz00_2091 = BgL_stringz00_1837;
					}
				else
					{
						obj_t BgL_auxz00_2094;

						BgL_auxz00_2094 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1650z00zz__r4_symbols_6_4z00, BINT(13590L),
							BGl_string1666z00zz__r4_symbols_6_4z00,
							BGl_string1655z00zz__r4_symbols_6_4z00, BgL_stringz00_1837);
						FAILURE(BgL_auxz00_2094, BFALSE, BFALSE);
					}
				return BGl_stringzd2ze3keywordz31zz__r4_symbols_6_4z00(BgL_auxz00_2091);
			}
		}

	}



/* symbol->keyword */
	BGL_EXPORTED_DEF obj_t BGl_symbolzd2ze3keywordz31zz__r4_symbols_6_4z00(obj_t
		BgL_symbolz00_24)
	{
		{	/* Ieee/symbol.scm 342 */
			{	/* Ieee/symbol.scm 343 */
				obj_t BgL_arg1244z00_1616;

				{	/* Ieee/symbol.scm 173 */
					obj_t BgL_arg1166z00_1618;

					BgL_arg1166z00_1618 = SYMBOL_TO_STRING(BgL_symbolz00_24);
					BgL_arg1244z00_1616 =
						BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1166z00_1618);
				}
				return bstring_to_keyword(BgL_arg1244z00_1616);
			}
		}

	}



/* &symbol->keyword */
	obj_t BGl_z62symbolzd2ze3keywordz53zz__r4_symbols_6_4z00(obj_t
		BgL_envz00_1838, obj_t BgL_symbolz00_1839)
	{
		{	/* Ieee/symbol.scm 342 */
			{	/* Ieee/symbol.scm 343 */
				obj_t BgL_auxz00_2102;

				if (SYMBOLP(BgL_symbolz00_1839))
					{	/* Ieee/symbol.scm 343 */
						BgL_auxz00_2102 = BgL_symbolz00_1839;
					}
				else
					{
						obj_t BgL_auxz00_2105;

						BgL_auxz00_2105 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1650z00zz__r4_symbols_6_4z00, BINT(13944L),
							BGl_string1667z00zz__r4_symbols_6_4z00,
							BGl_string1652z00zz__r4_symbols_6_4z00, BgL_symbolz00_1839);
						FAILURE(BgL_auxz00_2105, BFALSE, BFALSE);
					}
				return BGl_symbolzd2ze3keywordz31zz__r4_symbols_6_4z00(BgL_auxz00_2102);
			}
		}

	}



/* keyword->symbol */
	BGL_EXPORTED_DEF obj_t BGl_keywordzd2ze3symbolz31zz__r4_symbols_6_4z00(obj_t
		BgL_keywordz00_25)
	{
		{	/* Ieee/symbol.scm 348 */
			{	/* Ieee/symbol.scm 349 */
				obj_t BgL_arg1248z00_1620;

				{	/* Ieee/symbol.scm 321 */
					obj_t BgL_arg1242z00_1622;

					BgL_arg1242z00_1622 = KEYWORD_TO_STRING(BgL_keywordz00_25);
					BgL_arg1248z00_1620 =
						BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1242z00_1622);
				}
				return bstring_to_symbol(BgL_arg1248z00_1620);
			}
		}

	}



/* &keyword->symbol */
	obj_t BGl_z62keywordzd2ze3symbolz53zz__r4_symbols_6_4z00(obj_t
		BgL_envz00_1840, obj_t BgL_keywordz00_1841)
	{
		{	/* Ieee/symbol.scm 348 */
			{	/* Ieee/symbol.scm 349 */
				obj_t BgL_auxz00_2113;

				if (KEYWORDP(BgL_keywordz00_1841))
					{	/* Ieee/symbol.scm 349 */
						BgL_auxz00_2113 = BgL_keywordz00_1841;
					}
				else
					{
						obj_t BgL_auxz00_2116;

						BgL_auxz00_2116 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1650z00zz__r4_symbols_6_4z00, BINT(14246L),
							BGl_string1668z00zz__r4_symbols_6_4z00,
							BGl_string1664z00zz__r4_symbols_6_4z00, BgL_keywordz00_1841);
						FAILURE(BgL_auxz00_2116, BFALSE, BFALSE);
					}
				return BGl_keywordzd2ze3symbolz31zz__r4_symbols_6_4z00(BgL_auxz00_2113);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__r4_symbols_6_4z00(void)
	{
		{	/* Ieee/symbol.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__r4_symbols_6_4z00(void)
	{
		{	/* Ieee/symbol.scm 14 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__r4_symbols_6_4z00(void)
	{
		{	/* Ieee/symbol.scm 14 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__r4_symbols_6_4z00(void)
	{
		{	/* Ieee/symbol.scm 14 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1669z00zz__r4_symbols_6_4z00));
			return
				BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1669z00zz__r4_symbols_6_4z00));
		}

	}

#ifdef __cplusplus
}
#endif
