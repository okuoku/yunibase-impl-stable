/*===========================================================================*/
/*   (Ieee/pairlist.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Ieee/pairlist.scm -indent -o objs/obj_u/Ieee/pairlist.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___R4_PAIRS_AND_LISTS_6_3_TYPE_DEFINITIONS
#define BGL___R4_PAIRS_AND_LISTS_6_3_TYPE_DEFINITIONS
#endif													// BGL___R4_PAIRS_AND_LISTS_6_3_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62carz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62nullzf3z91zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62cdaarz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_z62caddrz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cddddrz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_z62lengthz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62takez62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t bgl_append2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_reducez00zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	static obj_t BGl_z62appendz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cdaadrz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_caddarz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_ereversez00zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_z62listz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62cddaarz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62cdaddrz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_anyz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_deletezd2duplicatesz12zc0zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00 =
		BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_listzd2tailzd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		long);
	BGL_EXPORTED_DECL obj_t bgl_remq_bang(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_treezd2copyzd2zz__r4_pairs_and_lists_6_3z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_caaaarz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cddarz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_z62caadarz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_listzd2copyzd2zz__r4_pairs_and_lists_6_3z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_takez00zz__r4_pairs_and_lists_6_3z00(obj_t, long);
	BGL_EXPORTED_DECL obj_t bgl_list_ref(obj_t, long);
	static obj_t BGl_z62epairzf3z91zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_findzd2tailzd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_caaarz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_EXPORTED_DECL obj_t BGl_listz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_z62caarz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62cdrz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62cdadrz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_appendzd2listze70z35zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_recurze70ze7zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_appendzd2listze71z35zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_appendzd2listze72z35zz__r4_pairs_and_lists_6_3z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_deletez00zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_assocz00zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	static obj_t BGl_z62assqz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cdadarz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_z62cerz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cadddrz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_z62assvz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62pairzd2orzd2nullzf3z91zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	static obj_t BGl_z62cddadrz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_everyz00zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	static obj_t BGl_z62makezd2listzb0zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl__deletezd2duplicateszd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	static obj_t BGl_z62iotaz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_setzd2cdrz12zc0zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62setzd2carz12za2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_listzd2splitz12zc0zz__r4_pairs_and_lists_6_3z00(obj_t, int, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_caarz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_appendz12z12zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_z62remqz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
	static obj_t BGl_consza21ze70z45zz__r4_pairs_and_lists_6_3z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_caaadrz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_z62appendz12z70zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cdddrz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_z62cadaarz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62caaddrz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl__deletez00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62memqz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zz__r4_pairs_and_lists_6_3z00(void);
	static obj_t BGl_z62memvz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_caadrz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_z62remqz12z70zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t bgl_reverse(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_assvz00zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	static obj_t BGl_z62cadrz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62reversez62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL long bgl_list_length(obj_t);
	static obj_t BGl_z62cddarz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t
		BGl_importedzd2moduleszd2initz00zz__r4_pairs_and_lists_6_3z00(void);
	BGL_EXPORTED_DECL obj_t BGl_iotaz00zz__r4_pairs_and_lists_6_3z00(int, obj_t);
	static obj_t BGl_gczd2rootszd2initz00zz__r4_pairs_and_lists_6_3z00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_setzd2cerz12zc0zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t bgl_remq(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_appendz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_z62dropz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
	extern obj_t BGl_2za2za2zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62appendzd22zb0zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_2zb2zb2zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62caaarz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_2zd2zd2zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cddaarz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cdaddrz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_z62ereversez62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62cdddarz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_pairzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t
		BGl__deletezd2duplicatesz12zc0zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_memvz00zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_carz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cadrz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_caadarz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t bgl_reverse_bang(obj_t);
	static obj_t BGl_z62assocz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62cdaaarz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62cadadrz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_makezd2listzd2zz__r4_pairs_and_lists_6_3z00(int,
		obj_t);
	static obj_t BGl_z62listzd2refzb0zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_epairzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_z62everyz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_dropz00zz__r4_pairs_and_lists_6_3z00(obj_t, long);
	BGL_EXPORTED_DECL obj_t BGl_cadarz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_z62memberz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62cdarz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_symbol2092z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
	static obj_t BGl_z62findz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62cdddrz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62anyz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lastzd2pairzb0zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_listzd2splitzd2zz__r4_pairs_and_lists_6_3z00(obj_t, int, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_listzd2tabulatezd2zz__r4_pairs_and_lists_6_3z00(int, obj_t);
	static obj_t BGl_z62appendzd22z12za2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t, obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_z62listzf3z91zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62caadrz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cddadrz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_z62cddddrz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_econsz00zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62setzd2cdrz12za2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cdrz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_appendzd221011zd2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cdarz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cadaarz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_z62reducez62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_caaddrz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_findz00zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	extern obj_t BGl_bigloozd2typezd2errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62cdaadrz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62caddarz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62pairzf3z91zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_nullzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cerz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_z62listzd2splitzb0zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cdaarz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_z62listzd2setz12za2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_caddrz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_z62caaaarz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62consza2zc0zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62cddrz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_loopze70ze7zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_deletez12z12zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62consz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62setzd2cerz12za2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62listzd2splitz12za2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_eappendz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_z62cadarz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62eappendzd22zb0zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cdddarz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_z62eappendz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_deletezd2duplicateszd2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl__deletez12z12zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_appendzd22z12zc0zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62listzd2tailzb0zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cdaaarz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cddrz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_z62treezd2copyzb0zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cadadrz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_z62listzd2tabulatezb0zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62reversez12z70zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62cdadarz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62cadddrz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62listzd2copyzb0zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_setzd2carz12zc0zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_consz00zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_memberz00zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	static obj_t BGl_z62findzd2tailzb0zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62econsz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cdadrz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_z62caaadrz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern bool_t BGl_eqvzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_listzd2setz12zc0zz__r4_pairs_and_lists_6_3z00(obj_t, long, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string2040z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2040za700za7za7_2095za7, "&caddar", 7);
	      DEFINE_STRING(BGl_string2041z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2041za700za7za7_2096za7, "&cadadr", 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_anyzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762anyza762za7za7__r4_2097z00, va_generic_entry,
		BGl_z62anyz62zz__r4_pairs_and_lists_6_3z00, BUNSPEC, -2);
	      DEFINE_STRING(BGl_string2042z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2042za700za7za7_2098za7, "&cadddr", 7);
	      DEFINE_STRING(BGl_string2043z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2043za700za7za7_2099za7, "&cdaadr", 7);
	      DEFINE_STRING(BGl_string2044z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2044za700za7za7_2100za7, "&cdaddr", 7);
	      DEFINE_STRING(BGl_string2045z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2045za700za7za7_2101za7, "&cddaar", 7);
	      DEFINE_STRING(BGl_string2046z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2046za700za7za7_2102za7, "&cddadr", 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_lastzd2pairzd2envz00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762lastza7d2pairza7b2103za7,
		BGl_z62lastzd2pairzb0zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2047z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2047za700za7za7_2104za7, "&cdadar", 7);
	      DEFINE_STRING(BGl_string2048z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2048za700za7za7_2105za7, "&cdddar", 7);
	      DEFINE_STRING(BGl_string2049z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2049za700za7za7_2106za7, "&cddddr", 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_treezd2copyzd2envz00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762treeza7d2copyza7b2107za7,
		BGl_z62treezd2copyzb0zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_listzf3zd2envz21zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762listza7f3za791za7za7_2108za7,
		BGl_z62listzf3z91zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_caadrzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762caadrza762za7za7__r2109z00,
		BGl_z62caadrz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cddrzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762cddrza762za7za7__r42110z00,
		BGl_z62cddrz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cddadrzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762cddadrza762za7za7__2111z00,
		BGl_z62cddadrz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_listzd2copyzd2envz00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762listza7d2copyza7b2112za7,
		BGl_z62listzd2copyzb0zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2050z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2050za700za7za7_2113za7, "&set-car!", 9);
	      DEFINE_STRING(BGl_string2051z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2051za700za7za7_2114za7, "&set-cdr!", 9);
	      DEFINE_STRING(BGl_string2052z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2052za700za7za7_2115za7, "&set-cer!", 9);
	      DEFINE_STRING(BGl_string2053z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2053za700za7za7_2116za7, "&append-2", 9);
	      DEFINE_STRING(BGl_string2054z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2054za700za7za7_2117za7, "pair-nil", 8);
	      DEFINE_STRING(BGl_string2055z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2055za700za7za7_2118za7, "&eappend-2", 10);
	      DEFINE_STRING(BGl_string2056z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2056za700za7za7_2119za7, "&append-2!", 10);
	      DEFINE_STRING(BGl_string2057z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2057za700za7za7_2120za7, "&length", 7);
	      DEFINE_STRING(BGl_string2058z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2058za700za7za7_2121za7, "&reverse", 8);
	      DEFINE_STRING(BGl_string2059z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2059za700za7za7_2122za7, "&ereverse", 9);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_findzd2tailzd2envz00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762findza7d2tailza7b2123za7,
		BGl_z62findzd2tailzb0zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_caaarzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762caaarza762za7za7__r2124z00,
		BGl_z62caaarz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cdarzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762cdarza762za7za7__r42125z00,
		BGl_z62cdarz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_listzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762listza762za7za7__r42126z00, va_generic_entry,
		BGl_z62listz62zz__r4_pairs_and_lists_6_3z00, BUNSPEC, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cddaarzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762cddaarza762za7za7__2127z00,
		BGl_z62cddaarz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2060z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2060za700za7za7_2128za7, "&take", 5);
	      DEFINE_STRING(BGl_string2061z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2061za700za7za7_2129za7, "bint", 4);
	      DEFINE_STRING(BGl_string2062z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2062za700za7za7_2130za7, "&drop", 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_pairzf3zd2envz21zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762pairza7f3za791za7za7_2131za7,
		BGl_z62pairzf3z91zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2063z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2063za700za7za7_2132za7, "&list-tail", 10);
	      DEFINE_STRING(BGl_string2064z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2064za700za7za7_2133za7, "&list-ref", 9);
	      DEFINE_STRING(BGl_string2065z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2065za700za7za7_2134za7, "&list-set!", 10);
	      DEFINE_STRING(BGl_string2066z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2066za700za7za7_2135za7, "&last-pair", 10);
	      DEFINE_STRING(BGl_string2067z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2067za700za7za7_2136za7, "&memq", 5);
	      DEFINE_STRING(BGl_string2068z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2068za700za7za7_2137za7, "&memv", 5);
	      DEFINE_STRING(BGl_string2069z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2069za700za7za7_2138za7, "&member", 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_nullzf3zd2envz21zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762nullza7f3za791za7za7_2139za7,
		BGl_z62nullzf3z91zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cerzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762cerza762za7za7__r4_2140z00,
		BGl_z62cerz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_caddrzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762caddrza762za7za7__r2141z00,
		BGl_z62caddrz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_pairzd2orzd2nullzf3zd2envz21zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762pairza7d2orza7d2n2142za7,
		BGl_z62pairzd2orzd2nullzf3z91zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cddddrzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762cddddrza762za7za7__2143z00,
		BGl_z62cddddrz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2listzd2envz00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762makeza7d2listza7b2144za7, va_generic_entry,
		BGl_z62makezd2listzb0zz__r4_pairs_and_lists_6_3z00, BUNSPEC, -2);
	      DEFINE_STRING(BGl_string2070z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2070za700za7za7_2145za7, "&assq", 5);
	      DEFINE_STRING(BGl_string2071z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2071za700za7za7_2146za7, "&assv", 5);
	      DEFINE_STRING(BGl_string2072z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2072za700za7za7_2147za7, "&assoc", 6);
	      DEFINE_STRING(BGl_string2073z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2073za700za7za7_2148za7, "&remq", 5);
	      DEFINE_STRING(BGl_string2074z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2074za700za7za7_2149za7, "&remq!", 6);
	      DEFINE_STRING(BGl_string2075z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2075za700za7za7_2150za7, "_delete", 7);
	      DEFINE_STRING(BGl_string2076z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2076za700za7za7_2151za7, "_delete!", 8);
	      DEFINE_STRING(BGl_string2077z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2077za700za7za7_2152za7, "&reverse!", 9);
	      DEFINE_STRING(BGl_string2078z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2078za700za7za7_2153za7, "&every", 6);
	      DEFINE_STRING(BGl_string2079z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2079za700za7za7_2154za7, "procedure", 9);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cadarzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762cadarza762za7za7__r2155z00,
		BGl_z62cadarz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_caaadrzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762caaadrza762za7za7__2156z00,
		BGl_z62caaadrz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_ereversezd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762ereverseza762za7za72157z00,
		BGl_z62ereversez62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cdddarzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762cdddarza762za7za7__2158z00,
		BGl_z62cdddarz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2080z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2080za700za7za7_2159za7, "&any", 4);
	      DEFINE_STRING(BGl_string2081z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2081za700za7za7_2160za7, "&find", 5);
	      DEFINE_STRING(BGl_string2082z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2082za700za7za7_2161za7, "&find-tail", 10);
	      DEFINE_STRING(BGl_string2083z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2083za700za7za7_2162za7, "&reduce", 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_deletezd2duplicateszd2envz00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl__deleteza7d2duplic2163za7, opt_generic_entry,
		BGl__deletezd2duplicateszd2zz__r4_pairs_and_lists_6_3z00, BFALSE, -1);
	      DEFINE_STRING(BGl_string2084z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2084za700za7za7_2164za7, "&make-list", 10);
	      DEFINE_STRING(BGl_string2085z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2085za700za7za7_2165za7, "&list-tabulate", 14);
	      DEFINE_STRING(BGl_string2086z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2086za700za7za7_2166za7, "&list-split", 11);
	      DEFINE_STRING(BGl_string2087z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2087za700za7za7_2167za7, "&list-split!", 12);
	      DEFINE_STRING(BGl_string2088z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2088za700za7za7_2168za7, "&iota", 5);
	      DEFINE_STRING(BGl_string2089z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2089za700za7za7_2169za7, "&list-copy", 10);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_assqzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762assqza762za7za7__r42170z00,
		BGl_z62assqz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_caaaarzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762caaaarza762za7za7__2171z00,
		BGl_z62caaaarz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_listzd2tabulatezd2envz00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762listza7d2tabula2172z00,
		BGl_z62listzd2tabulatezb0zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reversezd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762reverseza762za7za7_2173z00,
		BGl_z62reversez62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_takezd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762takeza762za7za7__r42174z00,
		BGl_z62takez62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2090z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2090za700za7za7_2175za7, "_delete-duplicates", 18);
	      DEFINE_STRING(BGl_string2091z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2091za700za7za7_2176za7, "_delete-duplicates!", 19);
	      DEFINE_STRING(BGl_string2093z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2093za700za7za7_2177za7, "delete-duplicates!", 18);
	      DEFINE_STRING(BGl_string2094z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2094za700za7za7_2178za7, "__r4_pairs_and_lists_6_3", 24);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setzd2cerz12zd2envz12zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762setza7d2cerza712za72179z00,
		BGl_z62setzd2cerz12za2zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_appendzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762appendza762za7za7__2180z00, va_generic_entry,
		BGl_z62appendz62zz__r4_pairs_and_lists_6_3z00, BUNSPEC, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cdadrzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762cdadrza762za7za7__r2181z00,
		BGl_z62cdadrz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_caaddrzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762caaddrza762za7za7__2182z00,
		BGl_z62caaddrz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_memqzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762memqza762za7za7__r42183z00,
		BGl_z62memqz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_listzd2setz12zd2envz12zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762listza7d2setza7122184za7,
		BGl_z62listzd2setz12za2zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_assoczd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762assocza762za7za7__r2185z00,
		BGl_z62assocz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cdaarzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762cdaarza762za7za7__r2186z00,
		BGl_z62cdaarz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_caadarzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762caadarza762za7za7__2187z00,
		BGl_z62caadarz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_epairzf3zd2envz21zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762epairza7f3za791za7za72188za7,
		BGl_z62epairzf3z91zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_appendz12zd2envzc0zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762appendza712za770za72189z00, va_generic_entry,
		BGl_z62appendz12z70zz__r4_pairs_and_lists_6_3z00, BUNSPEC, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cdddrzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762cdddrza762za7za7__r2190z00,
		BGl_z62cdddrz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_remqz12zd2envzc0zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762remqza712za770za7za7_2191za7,
		BGl_z62remqz12z70zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_assvzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762assvza762za7za7__r42192z00,
		BGl_z62assvz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cddarzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762cddarza762za7za7__r2193z00,
		BGl_z62cddarz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cadadrzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762cadadrza762za7za7__2194z00,
		BGl_z62cadadrz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_listzd2refzd2envz00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762listza7d2refza7b02195za7,
		BGl_z62listzd2refzb0zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_iotazd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762iotaza762za7za7__r42196z00, va_generic_entry,
		BGl_z62iotaz62zz__r4_pairs_and_lists_6_3z00, BUNSPEC, -2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_conszd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762consza762za7za7__r42197z00,
		BGl_z62consz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_remqzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762remqza762za7za7__r42198z00,
		BGl_z62remqz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_econszd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762econsza762za7za7__r2199z00,
		BGl_z62econsz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cadaarzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762cadaarza762za7za7__2200z00,
		BGl_z62cadaarz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_memvzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762memvza762za7za7__r42201z00,
		BGl_z62memvz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reversez12zd2envzc0zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762reverseza712za7702202za7,
		BGl_z62reversez12z70zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_consza2zd2envz70zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762consza7a2za7c0za7za7_2203za7, va_generic_entry,
		BGl_z62consza2zc0zz__r4_pairs_and_lists_6_3z00, BUNSPEC, -2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cadddrzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762cadddrza762za7za7__2204z00,
		BGl_z62cadddrz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_deletez12zd2envzc0zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl__deleteza712za712za7za7_2205z00, opt_generic_entry,
		BGl__deletez12z12zz__r4_pairs_and_lists_6_3z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_dropzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762dropza762za7za7__r42206z00,
		BGl_z62dropz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_listzd2splitz12zd2envz12zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762listza7d2splitza72207za7, va_generic_entry,
		BGl_z62listzd2splitz12za2zz__r4_pairs_and_lists_6_3z00, BUNSPEC, -3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_appendzd22zd2envz00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762appendza7d22za7b02208za7,
		BGl_z62appendzd22zb0zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cdaadrzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762cdaadrza762za7za7__2209z00,
		BGl_z62cdaadrz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_caddarzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762caddarza762za7za7__2210z00,
		BGl_z62caddarz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_deletezd2duplicatesz12zd2envz12zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl__deleteza7d2duplic2211za7, opt_generic_entry,
		BGl__deletezd2duplicatesz12zc0zz__r4_pairs_and_lists_6_3z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_listzd2tailzd2envz00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762listza7d2tailza7b2212za7,
		BGl_z62listzd2tailzb0zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_listzd2splitzd2envz00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762listza7d2splitza72213za7, va_generic_entry,
		BGl_z62listzd2splitzb0zz__r4_pairs_and_lists_6_3z00, BUNSPEC, -3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_appendzd22z12zd2envz12zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762appendza7d22za7122214za7,
		BGl_z62appendzd22z12za2zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cdaaarzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762cdaaarza762za7za7__2215z00,
		BGl_z62cdaaarz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_lengthzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762lengthza762za7za7__2216z00,
		BGl_z62lengthz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2016za700za7za7_2217za7,
		"/tmp/bigloo/runtime/Ieee/pairlist.scm", 37);
	      DEFINE_STRING(BGl_string2017z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2017za700za7za7_2218za7, "&car", 4);
	      DEFINE_STRING(BGl_string2018z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2018za700za7za7_2219za7, "pair", 4);
	      DEFINE_STRING(BGl_string2019z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2019za700za7za7_2220za7, "&cdr", 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setzd2carz12zd2envz12zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762setza7d2carza712za72221z00,
		BGl_z62setzd2carz12za2zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2);
	extern obj_t BGl_equalzf3zd2envz21zz__r4_equivalence_6_2z00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_memberzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762memberza762za7za7__2222z00,
		BGl_z62memberz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cdrzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762cdrza762za7za7__r4_2223z00,
		BGl_z62cdrz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_findzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762findza762za7za7__r42224z00,
		BGl_z62findz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cdaddrzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762cdaddrza762za7za7__2225z00,
		BGl_z62cdaddrz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2020z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2020za700za7za7_2226za7, "&cer", 4);
	      DEFINE_STRING(BGl_string2021z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2021za700za7za7_2227za7, "epair", 5);
	      DEFINE_STRING(BGl_string2022z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2022za700za7za7_2228za7, "&caar", 5);
	      DEFINE_STRING(BGl_string2023z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2023za700za7za7_2229za7, "&cadr", 5);
	      DEFINE_STRING(BGl_string2024z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2024za700za7za7_2230za7, "&cdar", 5);
	      DEFINE_STRING(BGl_string2025z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2025za700za7za7_2231za7, "&cddr", 5);
	      DEFINE_STRING(BGl_string2026z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2026za700za7za7_2232za7, "&caaar", 6);
	      DEFINE_STRING(BGl_string2027z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2027za700za7za7_2233za7, "&caadr", 6);
	      DEFINE_STRING(BGl_string2028z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2028za700za7za7_2234za7, "&cadar", 6);
	      DEFINE_STRING(BGl_string2029z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2029za700za7za7_2235za7, "&caddr", 6);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_deletezd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl__deleteza700za7za7__r42236za7, opt_generic_entry,
		BGl__deletez00zz__r4_pairs_and_lists_6_3z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_carzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762carza762za7za7__r4_2237z00,
		BGl_z62carz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cadrzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762cadrza762za7za7__r42238z00,
		BGl_z62cadrz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cdadarzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762cdadarza762za7za7__2239z00,
		BGl_z62cdadarz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2030z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2030za700za7za7_2240za7, "&cdaar", 6);
	      DEFINE_STRING(BGl_string2031z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2031za700za7za7_2241za7, "&cddar", 6);
	      DEFINE_STRING(BGl_string2032z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2032za700za7za7_2242za7, "&cdadr", 6);
	      DEFINE_STRING(BGl_string2033z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2033za700za7za7_2243za7, "&cdddr", 6);
	      DEFINE_STRING(BGl_string2034z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2034za700za7za7_2244za7, "&caaaar", 7);
	      DEFINE_STRING(BGl_string2035z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2035za700za7za7_2245za7, "&caaadr", 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_everyzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762everyza762za7za7__r2246z00, va_generic_entry,
		BGl_z62everyz62zz__r4_pairs_and_lists_6_3z00, BUNSPEC, -2);
	      DEFINE_STRING(BGl_string2036z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2036za700za7za7_2247za7, "&caadar", 7);
	      DEFINE_STRING(BGl_string2037z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2037za700za7za7_2248za7, "&cadaar", 7);
	      DEFINE_STRING(BGl_string2038z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2038za700za7za7_2249za7, "&cdaaar", 7);
	      DEFINE_STRING(BGl_string2039z00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_string2039za700za7za7_2250za7, "&caaddr", 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setzd2cdrz12zd2envz12zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762setza7d2cdrza712za72251z00,
		BGl_z62setzd2cdrz12za2zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_caarzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762caarza762za7za7__r42252z00,
		BGl_z62caarz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reducezd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762reduceza762za7za7__2253z00,
		BGl_z62reducez62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_eappendzd22zd2envz00zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762eappendza7d22za7b2254za7,
		BGl_z62eappendzd22zb0zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_eappendzd2envzd2zz__r4_pairs_and_lists_6_3z00,
		BgL_bgl_za762eappendza762za7za7_2255z00, va_generic_entry,
		BGl_z62eappendz62zz__r4_pairs_and_lists_6_3z00, BUNSPEC, -1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00));
		     ADD_ROOT((void *) (&BGl_symbol2092z00zz__r4_pairs_and_lists_6_3z00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long
		BgL_checksumz00_2969, char *BgL_fromz00_2970)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00))
				{
					BGl_requirezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__r4_pairs_and_lists_6_3z00();
					BGl_cnstzd2initzd2zz__r4_pairs_and_lists_6_3z00();
					return
						BGl_importedzd2moduleszd2initz00zz__r4_pairs_and_lists_6_3z00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__r4_pairs_and_lists_6_3z00(void)
	{
		{	/* Ieee/pairlist.scm 18 */
			return (BGl_symbol2092z00zz__r4_pairs_and_lists_6_3z00 =
				bstring_to_symbol(BGl_string2093z00zz__r4_pairs_and_lists_6_3z00),
				BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__r4_pairs_and_lists_6_3z00(void)
	{
		{	/* Ieee/pairlist.scm 18 */
			return bgl_gc_roots_register();
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zz__r4_pairs_and_lists_6_3z00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_806;

				BgL_headz00_806 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1646;
					obj_t BgL_tailz00_1647;

					BgL_prevz00_1646 = BgL_headz00_806;
					BgL_tailz00_1647 = BgL_l1z00_1;
				BgL_loopz00_1645:
					if (PAIRP(BgL_tailz00_1647))
						{
							obj_t BgL_newzd2prevzd2_1653;

							BgL_newzd2prevzd2_1653 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1647), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1646, BgL_newzd2prevzd2_1653);
							{
								obj_t BgL_tailz00_2986;
								obj_t BgL_prevz00_2985;

								BgL_prevz00_2985 = BgL_newzd2prevzd2_1653;
								BgL_tailz00_2986 = CDR(BgL_tailz00_1647);
								BgL_tailz00_1647 = BgL_tailz00_2986;
								BgL_prevz00_1646 = BgL_prevz00_2985;
								goto BgL_loopz00_1645;
							}
						}
					else
						{
							BNIL;
						}
				}
				return CDR(BgL_headz00_806);
			}
		}

	}



/* pair? */
	BGL_EXPORTED_DEF bool_t BGl_pairzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_objz00_3)
	{
		{	/* Ieee/pairlist.scm 228 */
			return PAIRP(BgL_objz00_3);
		}

	}



/* &pair? */
	obj_t BGl_z62pairzf3z91zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2606,
		obj_t BgL_objz00_2607)
	{
		{	/* Ieee/pairlist.scm 228 */
			return
				BBOOL(BGl_pairzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_objz00_2607));
		}

	}



/* epair? */
	BGL_EXPORTED_DEF bool_t BGl_epairzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_objz00_4)
	{
		{	/* Ieee/pairlist.scm 234 */
			return EPAIRP(BgL_objz00_4);
		}

	}



/* &epair? */
	obj_t BGl_z62epairzf3z91zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2608,
		obj_t BgL_objz00_2609)
	{
		{	/* Ieee/pairlist.scm 234 */
			return
				BBOOL(BGl_epairzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_objz00_2609));
		}

	}



/* pair-or-null? */
	BGL_EXPORTED_DEF bool_t
		BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t BgL_objz00_5)
	{
		{	/* Ieee/pairlist.scm 240 */
			if (PAIRP(BgL_objz00_5))
				{	/* Ieee/pairlist.scm 241 */
					return ((bool_t) 1);
				}
			else
				{	/* Ieee/pairlist.scm 241 */
					return NULLP(BgL_objz00_5);
				}
		}

	}



/* &pair-or-null? */
	obj_t BGl_z62pairzd2orzd2nullzf3z91zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_envz00_2610, obj_t BgL_objz00_2611)
	{
		{	/* Ieee/pairlist.scm 240 */
			return
				BBOOL(BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
				(BgL_objz00_2611));
		}

	}



/* cons */
	BGL_EXPORTED_DEF obj_t BGl_consz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_obj1z00_6, obj_t BgL_obj2z00_7)
	{
		{	/* Ieee/pairlist.scm 248 */
			return MAKE_YOUNG_PAIR(BgL_obj1z00_6, BgL_obj2z00_7);
		}

	}



/* &cons */
	obj_t BGl_z62consz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2612,
		obj_t BgL_obj1z00_2613, obj_t BgL_obj2z00_2614)
	{
		{	/* Ieee/pairlist.scm 248 */
			return
				BGl_consz00zz__r4_pairs_and_lists_6_3z00(BgL_obj1z00_2613,
				BgL_obj2z00_2614);
		}

	}



/* econs */
	BGL_EXPORTED_DEF obj_t BGl_econsz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_obj1z00_8, obj_t BgL_obj2z00_9, obj_t BgL_obj3z00_10)
	{
		{	/* Ieee/pairlist.scm 254 */
			return MAKE_YOUNG_EPAIR(BgL_obj1z00_8, BgL_obj2z00_9, BgL_obj3z00_10);
		}

	}



/* &econs */
	obj_t BGl_z62econsz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2615,
		obj_t BgL_obj1z00_2616, obj_t BgL_obj2z00_2617, obj_t BgL_obj3z00_2618)
	{
		{	/* Ieee/pairlist.scm 254 */
			return
				BGl_econsz00zz__r4_pairs_and_lists_6_3z00(BgL_obj1z00_2616,
				BgL_obj2z00_2617, BgL_obj3z00_2618);
		}

	}



/* car */
	BGL_EXPORTED_DEF obj_t BGl_carz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_pairz00_11)
	{
		{	/* Ieee/pairlist.scm 260 */
			return CAR(BgL_pairz00_11);
		}

	}



/* &car */
	obj_t BGl_z62carz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2619,
		obj_t BgL_pairz00_2620)
	{
		{	/* Ieee/pairlist.scm 260 */
			{	/* Ieee/pairlist.scm 261 */
				obj_t BgL_auxz00_3005;

				if (PAIRP(BgL_pairz00_2620))
					{	/* Ieee/pairlist.scm 261 */
						BgL_auxz00_3005 = BgL_pairz00_2620;
					}
				else
					{
						obj_t BgL_auxz00_3008;

						BgL_auxz00_3008 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(10711L),
							BGl_string2017z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2018z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2620);
						FAILURE(BgL_auxz00_3008, BFALSE, BFALSE);
					}
				return BGl_carz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3005);
			}
		}

	}



/* cdr */
	BGL_EXPORTED_DEF obj_t BGl_cdrz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_pairz00_12)
	{
		{	/* Ieee/pairlist.scm 266 */
			return CDR(BgL_pairz00_12);
		}

	}



/* &cdr */
	obj_t BGl_z62cdrz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2621,
		obj_t BgL_pairz00_2622)
	{
		{	/* Ieee/pairlist.scm 266 */
			{	/* Ieee/pairlist.scm 267 */
				obj_t BgL_auxz00_3014;

				if (PAIRP(BgL_pairz00_2622))
					{	/* Ieee/pairlist.scm 267 */
						BgL_auxz00_3014 = BgL_pairz00_2622;
					}
				else
					{
						obj_t BgL_auxz00_3017;

						BgL_auxz00_3017 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(10976L),
							BGl_string2019z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2018z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2622);
						FAILURE(BgL_auxz00_3017, BFALSE, BFALSE);
					}
				return BGl_cdrz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3014);
			}
		}

	}



/* cer */
	BGL_EXPORTED_DEF obj_t BGl_cerz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_objz00_13)
	{
		{	/* Ieee/pairlist.scm 272 */
			return CER(BgL_objz00_13);
		}

	}



/* &cer */
	obj_t BGl_z62cerz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2623,
		obj_t BgL_objz00_2624)
	{
		{	/* Ieee/pairlist.scm 272 */
			{	/* Ieee/pairlist.scm 273 */
				obj_t BgL_auxz00_3023;

				if (EPAIRP(BgL_objz00_2624))
					{	/* Ieee/pairlist.scm 273 */
						BgL_auxz00_3023 = BgL_objz00_2624;
					}
				else
					{
						obj_t BgL_auxz00_3026;

						BgL_auxz00_3026 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(11240L),
							BGl_string2020z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2021z00zz__r4_pairs_and_lists_6_3z00, BgL_objz00_2624);
						FAILURE(BgL_auxz00_3026, BFALSE, BFALSE);
					}
				return BGl_cerz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3023);
			}
		}

	}



/* caar */
	BGL_EXPORTED_DEF obj_t BGl_caarz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_pairz00_14)
	{
		{	/* Ieee/pairlist.scm 278 */
			return CAR(CAR(BgL_pairz00_14));
		}

	}



/* &caar */
	obj_t BGl_z62caarz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2625,
		obj_t BgL_pairz00_2626)
	{
		{	/* Ieee/pairlist.scm 278 */
			{	/* Ieee/pairlist.scm 279 */
				obj_t BgL_auxz00_3033;

				if (PAIRP(BgL_pairz00_2626))
					{	/* Ieee/pairlist.scm 279 */
						BgL_auxz00_3033 = BgL_pairz00_2626;
					}
				else
					{
						obj_t BgL_auxz00_3036;

						BgL_auxz00_3036 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(11510L),
							BGl_string2022z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2018z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2626);
						FAILURE(BgL_auxz00_3036, BFALSE, BFALSE);
					}
				return BGl_caarz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3033);
			}
		}

	}



/* cadr */
	BGL_EXPORTED_DEF obj_t BGl_cadrz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_pairz00_15)
	{
		{	/* Ieee/pairlist.scm 284 */
			return CAR(CDR(BgL_pairz00_15));
		}

	}



/* &cadr */
	obj_t BGl_z62cadrz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2627,
		obj_t BgL_pairz00_2628)
	{
		{	/* Ieee/pairlist.scm 284 */
			{	/* Ieee/pairlist.scm 285 */
				obj_t BgL_auxz00_3043;

				if (PAIRP(BgL_pairz00_2628))
					{	/* Ieee/pairlist.scm 285 */
						BgL_auxz00_3043 = BgL_pairz00_2628;
					}
				else
					{
						obj_t BgL_auxz00_3046;

						BgL_auxz00_3046 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(11781L),
							BGl_string2023z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2018z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2628);
						FAILURE(BgL_auxz00_3046, BFALSE, BFALSE);
					}
				return BGl_cadrz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3043);
			}
		}

	}



/* cdar */
	BGL_EXPORTED_DEF obj_t BGl_cdarz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_pairz00_16)
	{
		{	/* Ieee/pairlist.scm 290 */
			return CDR(CAR(BgL_pairz00_16));
		}

	}



/* &cdar */
	obj_t BGl_z62cdarz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2629,
		obj_t BgL_pairz00_2630)
	{
		{	/* Ieee/pairlist.scm 290 */
			{	/* Ieee/pairlist.scm 291 */
				obj_t BgL_auxz00_3053;

				if (PAIRP(BgL_pairz00_2630))
					{	/* Ieee/pairlist.scm 291 */
						BgL_auxz00_3053 = BgL_pairz00_2630;
					}
				else
					{
						obj_t BgL_auxz00_3056;

						BgL_auxz00_3056 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(12052L),
							BGl_string2024z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2018z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2630);
						FAILURE(BgL_auxz00_3056, BFALSE, BFALSE);
					}
				return BGl_cdarz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3053);
			}
		}

	}



/* cddr */
	BGL_EXPORTED_DEF obj_t BGl_cddrz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_pairz00_17)
	{
		{	/* Ieee/pairlist.scm 296 */
			return CDR(CDR(BgL_pairz00_17));
		}

	}



/* &cddr */
	obj_t BGl_z62cddrz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2631,
		obj_t BgL_pairz00_2632)
	{
		{	/* Ieee/pairlist.scm 296 */
			{	/* Ieee/pairlist.scm 297 */
				obj_t BgL_auxz00_3063;

				if (PAIRP(BgL_pairz00_2632))
					{	/* Ieee/pairlist.scm 297 */
						BgL_auxz00_3063 = BgL_pairz00_2632;
					}
				else
					{
						obj_t BgL_auxz00_3066;

						BgL_auxz00_3066 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(12323L),
							BGl_string2025z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2018z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2632);
						FAILURE(BgL_auxz00_3066, BFALSE, BFALSE);
					}
				return BGl_cddrz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3063);
			}
		}

	}



/* caaar */
	BGL_EXPORTED_DEF obj_t BGl_caaarz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_pairz00_18)
	{
		{	/* Ieee/pairlist.scm 302 */
			return CAR(CAR(CAR(BgL_pairz00_18)));
		}

	}



/* &caaar */
	obj_t BGl_z62caaarz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2633,
		obj_t BgL_pairz00_2634)
	{
		{	/* Ieee/pairlist.scm 302 */
			{	/* Ieee/pairlist.scm 303 */
				obj_t BgL_auxz00_3074;

				if (PAIRP(BgL_pairz00_2634))
					{	/* Ieee/pairlist.scm 303 */
						BgL_auxz00_3074 = BgL_pairz00_2634;
					}
				else
					{
						obj_t BgL_auxz00_3077;

						BgL_auxz00_3077 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(12600L),
							BGl_string2026z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2018z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2634);
						FAILURE(BgL_auxz00_3077, BFALSE, BFALSE);
					}
				return BGl_caaarz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3074);
			}
		}

	}



/* caadr */
	BGL_EXPORTED_DEF obj_t BGl_caadrz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_pairz00_19)
	{
		{	/* Ieee/pairlist.scm 308 */
			return CAR(CAR(CDR(BgL_pairz00_19)));
		}

	}



/* &caadr */
	obj_t BGl_z62caadrz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2635,
		obj_t BgL_pairz00_2636)
	{
		{	/* Ieee/pairlist.scm 308 */
			{	/* Ieee/pairlist.scm 309 */
				obj_t BgL_auxz00_3085;

				if (PAIRP(BgL_pairz00_2636))
					{	/* Ieee/pairlist.scm 309 */
						BgL_auxz00_3085 = BgL_pairz00_2636;
					}
				else
					{
						obj_t BgL_auxz00_3088;

						BgL_auxz00_3088 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(12878L),
							BGl_string2027z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2018z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2636);
						FAILURE(BgL_auxz00_3088, BFALSE, BFALSE);
					}
				return BGl_caadrz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3085);
			}
		}

	}



/* cadar */
	BGL_EXPORTED_DEF obj_t BGl_cadarz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_pairz00_20)
	{
		{	/* Ieee/pairlist.scm 314 */
			return CAR(CDR(CAR(BgL_pairz00_20)));
		}

	}



/* &cadar */
	obj_t BGl_z62cadarz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2637,
		obj_t BgL_pairz00_2638)
	{
		{	/* Ieee/pairlist.scm 314 */
			{	/* Ieee/pairlist.scm 315 */
				obj_t BgL_auxz00_3096;

				if (PAIRP(BgL_pairz00_2638))
					{	/* Ieee/pairlist.scm 315 */
						BgL_auxz00_3096 = BgL_pairz00_2638;
					}
				else
					{
						obj_t BgL_auxz00_3099;

						BgL_auxz00_3099 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(13156L),
							BGl_string2028z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2018z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2638);
						FAILURE(BgL_auxz00_3099, BFALSE, BFALSE);
					}
				return BGl_cadarz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3096);
			}
		}

	}



/* caddr */
	BGL_EXPORTED_DEF obj_t BGl_caddrz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_pairz00_21)
	{
		{	/* Ieee/pairlist.scm 320 */
			return CAR(CDR(CDR(BgL_pairz00_21)));
		}

	}



/* &caddr */
	obj_t BGl_z62caddrz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2639,
		obj_t BgL_pairz00_2640)
	{
		{	/* Ieee/pairlist.scm 320 */
			{	/* Ieee/pairlist.scm 321 */
				obj_t BgL_auxz00_3107;

				if (PAIRP(BgL_pairz00_2640))
					{	/* Ieee/pairlist.scm 321 */
						BgL_auxz00_3107 = BgL_pairz00_2640;
					}
				else
					{
						obj_t BgL_auxz00_3110;

						BgL_auxz00_3110 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(13434L),
							BGl_string2029z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2018z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2640);
						FAILURE(BgL_auxz00_3110, BFALSE, BFALSE);
					}
				return BGl_caddrz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3107);
			}
		}

	}



/* cdaar */
	BGL_EXPORTED_DEF obj_t BGl_cdaarz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_pairz00_22)
	{
		{	/* Ieee/pairlist.scm 326 */
			return CDR(CAR(CAR(BgL_pairz00_22)));
		}

	}



/* &cdaar */
	obj_t BGl_z62cdaarz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2641,
		obj_t BgL_pairz00_2642)
	{
		{	/* Ieee/pairlist.scm 326 */
			{	/* Ieee/pairlist.scm 327 */
				obj_t BgL_auxz00_3118;

				if (PAIRP(BgL_pairz00_2642))
					{	/* Ieee/pairlist.scm 327 */
						BgL_auxz00_3118 = BgL_pairz00_2642;
					}
				else
					{
						obj_t BgL_auxz00_3121;

						BgL_auxz00_3121 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(13712L),
							BGl_string2030z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2018z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2642);
						FAILURE(BgL_auxz00_3121, BFALSE, BFALSE);
					}
				return BGl_cdaarz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3118);
			}
		}

	}



/* cddar */
	BGL_EXPORTED_DEF obj_t BGl_cddarz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_pairz00_23)
	{
		{	/* Ieee/pairlist.scm 332 */
			return CDR(CDR(CAR(BgL_pairz00_23)));
		}

	}



/* &cddar */
	obj_t BGl_z62cddarz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2643,
		obj_t BgL_pairz00_2644)
	{
		{	/* Ieee/pairlist.scm 332 */
			{	/* Ieee/pairlist.scm 333 */
				obj_t BgL_auxz00_3129;

				if (PAIRP(BgL_pairz00_2644))
					{	/* Ieee/pairlist.scm 333 */
						BgL_auxz00_3129 = BgL_pairz00_2644;
					}
				else
					{
						obj_t BgL_auxz00_3132;

						BgL_auxz00_3132 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(13990L),
							BGl_string2031z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2018z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2644);
						FAILURE(BgL_auxz00_3132, BFALSE, BFALSE);
					}
				return BGl_cddarz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3129);
			}
		}

	}



/* cdadr */
	BGL_EXPORTED_DEF obj_t BGl_cdadrz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_pairz00_24)
	{
		{	/* Ieee/pairlist.scm 338 */
			return CDR(CAR(CDR(BgL_pairz00_24)));
		}

	}



/* &cdadr */
	obj_t BGl_z62cdadrz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2645,
		obj_t BgL_pairz00_2646)
	{
		{	/* Ieee/pairlist.scm 338 */
			{	/* Ieee/pairlist.scm 339 */
				obj_t BgL_auxz00_3140;

				if (PAIRP(BgL_pairz00_2646))
					{	/* Ieee/pairlist.scm 339 */
						BgL_auxz00_3140 = BgL_pairz00_2646;
					}
				else
					{
						obj_t BgL_auxz00_3143;

						BgL_auxz00_3143 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(14268L),
							BGl_string2032z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2018z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2646);
						FAILURE(BgL_auxz00_3143, BFALSE, BFALSE);
					}
				return BGl_cdadrz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3140);
			}
		}

	}



/* cdddr */
	BGL_EXPORTED_DEF obj_t BGl_cdddrz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_pairz00_25)
	{
		{	/* Ieee/pairlist.scm 344 */
			return CDR(CDR(CDR(BgL_pairz00_25)));
		}

	}



/* &cdddr */
	obj_t BGl_z62cdddrz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2647,
		obj_t BgL_pairz00_2648)
	{
		{	/* Ieee/pairlist.scm 344 */
			{	/* Ieee/pairlist.scm 345 */
				obj_t BgL_auxz00_3151;

				if (PAIRP(BgL_pairz00_2648))
					{	/* Ieee/pairlist.scm 345 */
						BgL_auxz00_3151 = BgL_pairz00_2648;
					}
				else
					{
						obj_t BgL_auxz00_3154;

						BgL_auxz00_3154 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(14546L),
							BGl_string2033z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2018z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2648);
						FAILURE(BgL_auxz00_3154, BFALSE, BFALSE);
					}
				return BGl_cdddrz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3151);
			}
		}

	}



/* caaaar */
	BGL_EXPORTED_DEF obj_t BGl_caaaarz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_pairz00_26)
	{
		{	/* Ieee/pairlist.scm 350 */
			return CAR(CAR(CAR(CAR(BgL_pairz00_26))));
		}

	}



/* &caaaar */
	obj_t BGl_z62caaaarz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2649,
		obj_t BgL_pairz00_2650)
	{
		{	/* Ieee/pairlist.scm 350 */
			{	/* Ieee/pairlist.scm 351 */
				obj_t BgL_auxz00_3163;

				if (PAIRP(BgL_pairz00_2650))
					{	/* Ieee/pairlist.scm 351 */
						BgL_auxz00_3163 = BgL_pairz00_2650;
					}
				else
					{
						obj_t BgL_auxz00_3166;

						BgL_auxz00_3166 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(14830L),
							BGl_string2034z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2018z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2650);
						FAILURE(BgL_auxz00_3166, BFALSE, BFALSE);
					}
				return BGl_caaaarz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3163);
			}
		}

	}



/* caaadr */
	BGL_EXPORTED_DEF obj_t BGl_caaadrz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_pairz00_27)
	{
		{	/* Ieee/pairlist.scm 356 */
			return CAR(CAR(CAR(CDR(BgL_pairz00_27))));
		}

	}



/* &caaadr */
	obj_t BGl_z62caaadrz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2651,
		obj_t BgL_pairz00_2652)
	{
		{	/* Ieee/pairlist.scm 356 */
			{	/* Ieee/pairlist.scm 357 */
				obj_t BgL_auxz00_3175;

				if (PAIRP(BgL_pairz00_2652))
					{	/* Ieee/pairlist.scm 357 */
						BgL_auxz00_3175 = BgL_pairz00_2652;
					}
				else
					{
						obj_t BgL_auxz00_3178;

						BgL_auxz00_3178 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(15115L),
							BGl_string2035z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2018z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2652);
						FAILURE(BgL_auxz00_3178, BFALSE, BFALSE);
					}
				return BGl_caaadrz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3175);
			}
		}

	}



/* caadar */
	BGL_EXPORTED_DEF obj_t BGl_caadarz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_pairz00_28)
	{
		{	/* Ieee/pairlist.scm 362 */
			return CAR(CAR(CDR(CAR(BgL_pairz00_28))));
		}

	}



/* &caadar */
	obj_t BGl_z62caadarz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2653,
		obj_t BgL_pairz00_2654)
	{
		{	/* Ieee/pairlist.scm 362 */
			{	/* Ieee/pairlist.scm 363 */
				obj_t BgL_auxz00_3187;

				if (PAIRP(BgL_pairz00_2654))
					{	/* Ieee/pairlist.scm 363 */
						BgL_auxz00_3187 = BgL_pairz00_2654;
					}
				else
					{
						obj_t BgL_auxz00_3190;

						BgL_auxz00_3190 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(15400L),
							BGl_string2036z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2018z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2654);
						FAILURE(BgL_auxz00_3190, BFALSE, BFALSE);
					}
				return BGl_caadarz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3187);
			}
		}

	}



/* cadaar */
	BGL_EXPORTED_DEF obj_t BGl_cadaarz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_pairz00_29)
	{
		{	/* Ieee/pairlist.scm 368 */
			return CAR(CDR(CAR(CAR(BgL_pairz00_29))));
		}

	}



/* &cadaar */
	obj_t BGl_z62cadaarz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2655,
		obj_t BgL_pairz00_2656)
	{
		{	/* Ieee/pairlist.scm 368 */
			{	/* Ieee/pairlist.scm 369 */
				obj_t BgL_auxz00_3199;

				if (PAIRP(BgL_pairz00_2656))
					{	/* Ieee/pairlist.scm 369 */
						BgL_auxz00_3199 = BgL_pairz00_2656;
					}
				else
					{
						obj_t BgL_auxz00_3202;

						BgL_auxz00_3202 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(15685L),
							BGl_string2037z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2018z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2656);
						FAILURE(BgL_auxz00_3202, BFALSE, BFALSE);
					}
				return BGl_cadaarz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3199);
			}
		}

	}



/* cdaaar */
	BGL_EXPORTED_DEF obj_t BGl_cdaaarz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_pairz00_30)
	{
		{	/* Ieee/pairlist.scm 374 */
			return CDR(CAR(CAR(CAR(BgL_pairz00_30))));
		}

	}



/* &cdaaar */
	obj_t BGl_z62cdaaarz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2657,
		obj_t BgL_pairz00_2658)
	{
		{	/* Ieee/pairlist.scm 374 */
			{	/* Ieee/pairlist.scm 375 */
				obj_t BgL_auxz00_3211;

				if (PAIRP(BgL_pairz00_2658))
					{	/* Ieee/pairlist.scm 375 */
						BgL_auxz00_3211 = BgL_pairz00_2658;
					}
				else
					{
						obj_t BgL_auxz00_3214;

						BgL_auxz00_3214 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(15970L),
							BGl_string2038z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2018z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2658);
						FAILURE(BgL_auxz00_3214, BFALSE, BFALSE);
					}
				return BGl_cdaaarz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3211);
			}
		}

	}



/* caaddr */
	BGL_EXPORTED_DEF obj_t BGl_caaddrz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_pairz00_31)
	{
		{	/* Ieee/pairlist.scm 380 */
			return CAR(CAR(CDR(CDR(BgL_pairz00_31))));
		}

	}



/* &caaddr */
	obj_t BGl_z62caaddrz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2659,
		obj_t BgL_pairz00_2660)
	{
		{	/* Ieee/pairlist.scm 380 */
			{	/* Ieee/pairlist.scm 381 */
				obj_t BgL_auxz00_3223;

				if (PAIRP(BgL_pairz00_2660))
					{	/* Ieee/pairlist.scm 381 */
						BgL_auxz00_3223 = BgL_pairz00_2660;
					}
				else
					{
						obj_t BgL_auxz00_3226;

						BgL_auxz00_3226 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(16255L),
							BGl_string2039z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2018z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2660);
						FAILURE(BgL_auxz00_3226, BFALSE, BFALSE);
					}
				return BGl_caaddrz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3223);
			}
		}

	}



/* caddar */
	BGL_EXPORTED_DEF obj_t BGl_caddarz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_pairz00_32)
	{
		{	/* Ieee/pairlist.scm 386 */
			return CAR(CDR(CDR(CAR(BgL_pairz00_32))));
		}

	}



/* &caddar */
	obj_t BGl_z62caddarz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2661,
		obj_t BgL_pairz00_2662)
	{
		{	/* Ieee/pairlist.scm 386 */
			{	/* Ieee/pairlist.scm 387 */
				obj_t BgL_auxz00_3235;

				if (PAIRP(BgL_pairz00_2662))
					{	/* Ieee/pairlist.scm 387 */
						BgL_auxz00_3235 = BgL_pairz00_2662;
					}
				else
					{
						obj_t BgL_auxz00_3238;

						BgL_auxz00_3238 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(16540L),
							BGl_string2040z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2018z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2662);
						FAILURE(BgL_auxz00_3238, BFALSE, BFALSE);
					}
				return BGl_caddarz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3235);
			}
		}

	}



/* cadadr */
	BGL_EXPORTED_DEF obj_t BGl_cadadrz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_pairz00_33)
	{
		{	/* Ieee/pairlist.scm 392 */
			return CAR(CDR(CAR(CDR(BgL_pairz00_33))));
		}

	}



/* &cadadr */
	obj_t BGl_z62cadadrz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2663,
		obj_t BgL_pairz00_2664)
	{
		{	/* Ieee/pairlist.scm 392 */
			{	/* Ieee/pairlist.scm 393 */
				obj_t BgL_auxz00_3247;

				if (PAIRP(BgL_pairz00_2664))
					{	/* Ieee/pairlist.scm 393 */
						BgL_auxz00_3247 = BgL_pairz00_2664;
					}
				else
					{
						obj_t BgL_auxz00_3250;

						BgL_auxz00_3250 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(16825L),
							BGl_string2041z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2018z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2664);
						FAILURE(BgL_auxz00_3250, BFALSE, BFALSE);
					}
				return BGl_cadadrz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3247);
			}
		}

	}



/* cadddr */
	BGL_EXPORTED_DEF obj_t BGl_cadddrz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_pairz00_34)
	{
		{	/* Ieee/pairlist.scm 398 */
			return CAR(CDR(CDR(CDR(BgL_pairz00_34))));
		}

	}



/* &cadddr */
	obj_t BGl_z62cadddrz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2665,
		obj_t BgL_pairz00_2666)
	{
		{	/* Ieee/pairlist.scm 398 */
			{	/* Ieee/pairlist.scm 399 */
				obj_t BgL_auxz00_3259;

				if (PAIRP(BgL_pairz00_2666))
					{	/* Ieee/pairlist.scm 399 */
						BgL_auxz00_3259 = BgL_pairz00_2666;
					}
				else
					{
						obj_t BgL_auxz00_3262;

						BgL_auxz00_3262 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(17110L),
							BGl_string2042z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2018z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2666);
						FAILURE(BgL_auxz00_3262, BFALSE, BFALSE);
					}
				return BGl_cadddrz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3259);
			}
		}

	}



/* cdaadr */
	BGL_EXPORTED_DEF obj_t BGl_cdaadrz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_pairz00_35)
	{
		{	/* Ieee/pairlist.scm 404 */
			return CDR(CAR(CAR(CDR(BgL_pairz00_35))));
		}

	}



/* &cdaadr */
	obj_t BGl_z62cdaadrz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2667,
		obj_t BgL_pairz00_2668)
	{
		{	/* Ieee/pairlist.scm 404 */
			{	/* Ieee/pairlist.scm 405 */
				obj_t BgL_auxz00_3271;

				if (PAIRP(BgL_pairz00_2668))
					{	/* Ieee/pairlist.scm 405 */
						BgL_auxz00_3271 = BgL_pairz00_2668;
					}
				else
					{
						obj_t BgL_auxz00_3274;

						BgL_auxz00_3274 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(17395L),
							BGl_string2043z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2018z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2668);
						FAILURE(BgL_auxz00_3274, BFALSE, BFALSE);
					}
				return BGl_cdaadrz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3271);
			}
		}

	}



/* cdaddr */
	BGL_EXPORTED_DEF obj_t BGl_cdaddrz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_pairz00_36)
	{
		{	/* Ieee/pairlist.scm 410 */
			return CDR(CAR(CDR(CDR(BgL_pairz00_36))));
		}

	}



/* &cdaddr */
	obj_t BGl_z62cdaddrz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2669,
		obj_t BgL_pairz00_2670)
	{
		{	/* Ieee/pairlist.scm 410 */
			{	/* Ieee/pairlist.scm 411 */
				obj_t BgL_auxz00_3283;

				if (PAIRP(BgL_pairz00_2670))
					{	/* Ieee/pairlist.scm 411 */
						BgL_auxz00_3283 = BgL_pairz00_2670;
					}
				else
					{
						obj_t BgL_auxz00_3286;

						BgL_auxz00_3286 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(17680L),
							BGl_string2044z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2018z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2670);
						FAILURE(BgL_auxz00_3286, BFALSE, BFALSE);
					}
				return BGl_cdaddrz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3283);
			}
		}

	}



/* cddaar */
	BGL_EXPORTED_DEF obj_t BGl_cddaarz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_pairz00_37)
	{
		{	/* Ieee/pairlist.scm 416 */
			return CDR(CDR(CAR(CAR(BgL_pairz00_37))));
		}

	}



/* &cddaar */
	obj_t BGl_z62cddaarz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2671,
		obj_t BgL_pairz00_2672)
	{
		{	/* Ieee/pairlist.scm 416 */
			{	/* Ieee/pairlist.scm 417 */
				obj_t BgL_auxz00_3295;

				if (PAIRP(BgL_pairz00_2672))
					{	/* Ieee/pairlist.scm 417 */
						BgL_auxz00_3295 = BgL_pairz00_2672;
					}
				else
					{
						obj_t BgL_auxz00_3298;

						BgL_auxz00_3298 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(17965L),
							BGl_string2045z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2018z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2672);
						FAILURE(BgL_auxz00_3298, BFALSE, BFALSE);
					}
				return BGl_cddaarz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3295);
			}
		}

	}



/* cddadr */
	BGL_EXPORTED_DEF obj_t BGl_cddadrz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_pairz00_38)
	{
		{	/* Ieee/pairlist.scm 422 */
			return CDR(CDR(CAR(CDR(BgL_pairz00_38))));
		}

	}



/* &cddadr */
	obj_t BGl_z62cddadrz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2673,
		obj_t BgL_pairz00_2674)
	{
		{	/* Ieee/pairlist.scm 422 */
			{	/* Ieee/pairlist.scm 423 */
				obj_t BgL_auxz00_3307;

				if (PAIRP(BgL_pairz00_2674))
					{	/* Ieee/pairlist.scm 423 */
						BgL_auxz00_3307 = BgL_pairz00_2674;
					}
				else
					{
						obj_t BgL_auxz00_3310;

						BgL_auxz00_3310 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(18250L),
							BGl_string2046z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2018z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2674);
						FAILURE(BgL_auxz00_3310, BFALSE, BFALSE);
					}
				return BGl_cddadrz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3307);
			}
		}

	}



/* cdadar */
	BGL_EXPORTED_DEF obj_t BGl_cdadarz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_pairz00_39)
	{
		{	/* Ieee/pairlist.scm 428 */
			return CDR(CAR(CDR(CAR(BgL_pairz00_39))));
		}

	}



/* &cdadar */
	obj_t BGl_z62cdadarz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2675,
		obj_t BgL_pairz00_2676)
	{
		{	/* Ieee/pairlist.scm 428 */
			{	/* Ieee/pairlist.scm 429 */
				obj_t BgL_auxz00_3319;

				if (PAIRP(BgL_pairz00_2676))
					{	/* Ieee/pairlist.scm 429 */
						BgL_auxz00_3319 = BgL_pairz00_2676;
					}
				else
					{
						obj_t BgL_auxz00_3322;

						BgL_auxz00_3322 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(18535L),
							BGl_string2047z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2018z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2676);
						FAILURE(BgL_auxz00_3322, BFALSE, BFALSE);
					}
				return BGl_cdadarz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3319);
			}
		}

	}



/* cdddar */
	BGL_EXPORTED_DEF obj_t BGl_cdddarz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_pairz00_40)
	{
		{	/* Ieee/pairlist.scm 434 */
			return CDR(CDR(CDR(CAR(BgL_pairz00_40))));
		}

	}



/* &cdddar */
	obj_t BGl_z62cdddarz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2677,
		obj_t BgL_pairz00_2678)
	{
		{	/* Ieee/pairlist.scm 434 */
			{	/* Ieee/pairlist.scm 435 */
				obj_t BgL_auxz00_3331;

				if (PAIRP(BgL_pairz00_2678))
					{	/* Ieee/pairlist.scm 435 */
						BgL_auxz00_3331 = BgL_pairz00_2678;
					}
				else
					{
						obj_t BgL_auxz00_3334;

						BgL_auxz00_3334 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(18820L),
							BGl_string2048z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2018z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2678);
						FAILURE(BgL_auxz00_3334, BFALSE, BFALSE);
					}
				return BGl_cdddarz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3331);
			}
		}

	}



/* cddddr */
	BGL_EXPORTED_DEF obj_t BGl_cddddrz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_pairz00_41)
	{
		{	/* Ieee/pairlist.scm 441 */
			return CDR(CDR(CDR(CDR(BgL_pairz00_41))));
		}

	}



/* &cddddr */
	obj_t BGl_z62cddddrz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2679,
		obj_t BgL_pairz00_2680)
	{
		{	/* Ieee/pairlist.scm 441 */
			{	/* Ieee/pairlist.scm 442 */
				obj_t BgL_auxz00_3343;

				if (PAIRP(BgL_pairz00_2680))
					{	/* Ieee/pairlist.scm 442 */
						BgL_auxz00_3343 = BgL_pairz00_2680;
					}
				else
					{
						obj_t BgL_auxz00_3346;

						BgL_auxz00_3346 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(19106L),
							BGl_string2049z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2018z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2680);
						FAILURE(BgL_auxz00_3346, BFALSE, BFALSE);
					}
				return BGl_cddddrz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3343);
			}
		}

	}



/* set-car! */
	BGL_EXPORTED_DEF obj_t BGl_setzd2carz12zc0zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_pairz00_42, obj_t BgL_objz00_43)
	{
		{	/* Ieee/pairlist.scm 447 */
			return SET_CAR(BgL_pairz00_42, BgL_objz00_43);
		}

	}



/* &set-car! */
	obj_t BGl_z62setzd2carz12za2zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_envz00_2681, obj_t BgL_pairz00_2682, obj_t BgL_objz00_2683)
	{
		{	/* Ieee/pairlist.scm 447 */
			{	/* Ieee/pairlist.scm 448 */
				obj_t BgL_auxz00_3352;

				if (PAIRP(BgL_pairz00_2682))
					{	/* Ieee/pairlist.scm 448 */
						BgL_auxz00_3352 = BgL_pairz00_2682;
					}
				else
					{
						obj_t BgL_auxz00_3355;

						BgL_auxz00_3355 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(19382L),
							BGl_string2050z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2018z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2682);
						FAILURE(BgL_auxz00_3355, BFALSE, BFALSE);
					}
				return
					BGl_setzd2carz12zc0zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3352,
					BgL_objz00_2683);
			}
		}

	}



/* set-cdr! */
	BGL_EXPORTED_DEF obj_t BGl_setzd2cdrz12zc0zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_pairz00_44, obj_t BgL_objz00_45)
	{
		{	/* Ieee/pairlist.scm 453 */
			return SET_CDR(BgL_pairz00_44, BgL_objz00_45);
		}

	}



/* &set-cdr! */
	obj_t BGl_z62setzd2cdrz12za2zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_envz00_2684, obj_t BgL_pairz00_2685, obj_t BgL_objz00_2686)
	{
		{	/* Ieee/pairlist.scm 453 */
			{	/* Ieee/pairlist.scm 454 */
				obj_t BgL_auxz00_3361;

				if (PAIRP(BgL_pairz00_2685))
					{	/* Ieee/pairlist.scm 454 */
						BgL_auxz00_3361 = BgL_pairz00_2685;
					}
				else
					{
						obj_t BgL_auxz00_3364;

						BgL_auxz00_3364 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(19665L),
							BGl_string2051z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2018z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2685);
						FAILURE(BgL_auxz00_3364, BFALSE, BFALSE);
					}
				return
					BGl_setzd2cdrz12zc0zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3361,
					BgL_objz00_2686);
			}
		}

	}



/* set-cer! */
	BGL_EXPORTED_DEF obj_t BGl_setzd2cerz12zc0zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_epairz00_46, obj_t BgL_objz00_47)
	{
		{	/* Ieee/pairlist.scm 459 */
			return SET_CER(BgL_epairz00_46, BgL_objz00_47);
		}

	}



/* &set-cer! */
	obj_t BGl_z62setzd2cerz12za2zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_envz00_2687, obj_t BgL_epairz00_2688, obj_t BgL_objz00_2689)
	{
		{	/* Ieee/pairlist.scm 459 */
			{	/* Ieee/pairlist.scm 460 */
				obj_t BgL_auxz00_3370;

				if (EPAIRP(BgL_epairz00_2688))
					{	/* Ieee/pairlist.scm 460 */
						BgL_auxz00_3370 = BgL_epairz00_2688;
					}
				else
					{
						obj_t BgL_auxz00_3373;

						BgL_auxz00_3373 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(19949L),
							BGl_string2052z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2021z00zz__r4_pairs_and_lists_6_3z00,
							BgL_epairz00_2688);
						FAILURE(BgL_auxz00_3373, BFALSE, BFALSE);
					}
				return
					BGl_setzd2cerz12zc0zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3370,
					BgL_objz00_2689);
			}
		}

	}



/* null? */
	BGL_EXPORTED_DEF bool_t BGl_nullzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_objz00_48)
	{
		{	/* Ieee/pairlist.scm 465 */
			return NULLP(BgL_objz00_48);
		}

	}



/* &null? */
	obj_t BGl_z62nullzf3z91zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2690,
		obj_t BgL_objz00_2691)
	{
		{	/* Ieee/pairlist.scm 465 */
			return
				BBOOL(BGl_nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_objz00_2691));
		}

	}



/* list */
	BGL_EXPORTED_DEF obj_t BGl_listz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_lz00_49)
	{
		{	/* Ieee/pairlist.scm 471 */
			return BgL_lz00_49;
		}

	}



/* &list */
	obj_t BGl_z62listz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2692,
		obj_t BgL_lz00_2693)
	{
		{	/* Ieee/pairlist.scm 471 */
			return BGl_listz00zz__r4_pairs_and_lists_6_3z00(BgL_lz00_2693);
		}

	}



/* list? */
	BGL_EXPORTED_DEF bool_t BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_xz00_50)
	{
		{	/* Ieee/pairlist.scm 477 */
			{
				obj_t BgL_xz00_886;
				obj_t BgL_prevz00_887;

				if (NULLP(BgL_xz00_50))
					{	/* Ieee/pairlist.scm 494 */
						return ((bool_t) 1);
					}
				else
					{	/* Ieee/pairlist.scm 494 */
						if (PAIRP(BgL_xz00_50))
							{	/* Ieee/pairlist.scm 497 */
								obj_t BgL_arg1318z00_891;

								BgL_arg1318z00_891 = CDR(BgL_xz00_50);
								if (NULLP(BgL_arg1318z00_891))
									{	/* Ieee/pairlist.scm 479 */
										return ((bool_t) 1);
									}
								else
									{	/* Ieee/pairlist.scm 479 */
										if (PAIRP(BgL_arg1318z00_891))
											{	/* Ieee/pairlist.scm 481 */
												if ((BgL_arg1318z00_891 == BgL_xz00_50))
													{	/* Ieee/pairlist.scm 482 */
														return ((bool_t) 0);
													}
												else
													{	/* Ieee/pairlist.scm 482 */
														BgL_xz00_886 = CDR(BgL_arg1318z00_891);
														BgL_prevz00_887 = BgL_xz00_50;
													BgL_l2z00_888:
														if (NULLP(BgL_xz00_886))
															{	/* Ieee/pairlist.scm 487 */
																return ((bool_t) 1);
															}
														else
															{	/* Ieee/pairlist.scm 487 */
																if (PAIRP(BgL_xz00_886))
																	{	/* Ieee/pairlist.scm 489 */
																		if ((BgL_xz00_886 == BgL_prevz00_887))
																			{	/* Ieee/pairlist.scm 490 */
																				return ((bool_t) 0);
																			}
																		else
																			{	/* Ieee/pairlist.scm 492 */
																				obj_t BgL_arg1325z00_1837;
																				obj_t BgL_arg1326z00_1838;

																				BgL_arg1325z00_1837 = CDR(BgL_xz00_886);
																				BgL_arg1326z00_1838 =
																					CDR(((obj_t) BgL_prevz00_887));
																				if (NULLP(BgL_arg1325z00_1837))
																					{	/* Ieee/pairlist.scm 479 */
																						return ((bool_t) 1);
																					}
																				else
																					{	/* Ieee/pairlist.scm 479 */
																						if (PAIRP(BgL_arg1325z00_1837))
																							{	/* Ieee/pairlist.scm 481 */
																								if (
																									(BgL_arg1325z00_1837 ==
																										BgL_arg1326z00_1838))
																									{	/* Ieee/pairlist.scm 482 */
																										return ((bool_t) 0);
																									}
																								else
																									{
																										obj_t BgL_prevz00_3410;
																										obj_t BgL_xz00_3408;

																										BgL_xz00_3408 =
																											CDR(BgL_arg1325z00_1837);
																										BgL_prevz00_3410 =
																											BgL_arg1326z00_1838;
																										BgL_prevz00_887 =
																											BgL_prevz00_3410;
																										BgL_xz00_886 =
																											BgL_xz00_3408;
																										goto BgL_l2z00_888;
																									}
																							}
																						else
																							{	/* Ieee/pairlist.scm 481 */
																								return ((bool_t) 0);
																							}
																					}
																			}
																	}
																else
																	{	/* Ieee/pairlist.scm 489 */
																		return ((bool_t) 0);
																	}
															}
													}
											}
										else
											{	/* Ieee/pairlist.scm 481 */
												return ((bool_t) 0);
											}
									}
							}
						else
							{	/* Ieee/pairlist.scm 496 */
								return ((bool_t) 0);
							}
					}
			}
		}

	}



/* &list? */
	obj_t BGl_z62listzf3z91zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2694,
		obj_t BgL_xz00_2695)
	{
		{	/* Ieee/pairlist.scm 477 */
			return BBOOL(BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_xz00_2695));
		}

	}



/* append-2 */
	BGL_EXPORTED_DEF obj_t bgl_append2(obj_t BgL_l1z00_51, obj_t BgL_l2z00_52)
	{
		{	/* Ieee/pairlist.scm 504 */
			{	/* Ieee/pairlist.scm 505 */
				obj_t BgL_headz00_899;

				BgL_headz00_899 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_52);
				{
					obj_t BgL_prevz00_1865;
					obj_t BgL_tailz00_1866;

					BgL_prevz00_1865 = BgL_headz00_899;
					BgL_tailz00_1866 = BgL_l1z00_51;
				BgL_loopz00_1864:
					if (NULLP(BgL_tailz00_1866))
						{	/* Ieee/pairlist.scm 507 */
							BNIL;
						}
					else
						{	/* Ieee/pairlist.scm 509 */
							obj_t BgL_newzd2prevzd2_1872;

							{	/* Ieee/pairlist.scm 509 */
								obj_t BgL_arg1329z00_1873;

								BgL_arg1329z00_1873 = CAR(((obj_t) BgL_tailz00_1866));
								BgL_newzd2prevzd2_1872 =
									MAKE_YOUNG_PAIR(BgL_arg1329z00_1873, BgL_l2z00_52);
							}
							SET_CDR(BgL_prevz00_1865, BgL_newzd2prevzd2_1872);
							{	/* Ieee/pairlist.scm 511 */
								obj_t BgL_arg1328z00_1874;

								BgL_arg1328z00_1874 = CDR(((obj_t) BgL_tailz00_1866));
								{
									obj_t BgL_tailz00_3424;
									obj_t BgL_prevz00_3423;

									BgL_prevz00_3423 = BgL_newzd2prevzd2_1872;
									BgL_tailz00_3424 = BgL_arg1328z00_1874;
									BgL_tailz00_1866 = BgL_tailz00_3424;
									BgL_prevz00_1865 = BgL_prevz00_3423;
									goto BgL_loopz00_1864;
								}
							}
						}
				}
				return CDR(BgL_headz00_899);
			}
		}

	}



/* &append-2 */
	obj_t BGl_z62appendzd22zb0zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2696,
		obj_t BgL_l1z00_2697, obj_t BgL_l2z00_2698)
	{
		{	/* Ieee/pairlist.scm 504 */
			{	/* Ieee/pairlist.scm 505 */
				obj_t BgL_auxz00_3426;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_l1z00_2697))
					{	/* Ieee/pairlist.scm 505 */
						BgL_auxz00_3426 = BgL_l1z00_2697;
					}
				else
					{
						obj_t BgL_auxz00_3429;

						BgL_auxz00_3429 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(21406L),
							BGl_string2053z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2054z00zz__r4_pairs_and_lists_6_3z00, BgL_l1z00_2697);
						FAILURE(BgL_auxz00_3429, BFALSE, BFALSE);
					}
				return bgl_append2(BgL_auxz00_3426, BgL_l2z00_2698);
			}
		}

	}



/* eappend-2 */
	BGL_EXPORTED_DEF obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_l1z00_53, obj_t BgL_l2z00_54)
	{
		{	/* Ieee/pairlist.scm 518 */
			{	/* Ieee/pairlist.scm 519 */
				obj_t BgL_headz00_907;

				if (EPAIRP(BgL_l2z00_54))
					{	/* Ieee/pairlist.scm 520 */
						obj_t BgL_arg1337z00_919;

						BgL_arg1337z00_919 = CER(((obj_t) BgL_l2z00_54));
						{	/* Ieee/pairlist.scm 520 */
							obj_t BgL_res1848z00_1880;

							BgL_res1848z00_1880 =
								MAKE_YOUNG_EPAIR(BNIL, BgL_l2z00_54, BgL_arg1337z00_919);
							BgL_headz00_907 = BgL_res1848z00_1880;
						}
					}
				else
					{	/* Ieee/pairlist.scm 519 */
						BgL_headz00_907 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_54);
					}
				{
					obj_t BgL_prevz00_908;
					obj_t BgL_tailz00_909;

					BgL_prevz00_908 = BgL_headz00_907;
					BgL_tailz00_909 = BgL_l1z00_53;
				BgL_loopz00_910:
					if (NULLP(BgL_tailz00_909))
						{	/* Ieee/pairlist.scm 523 */
							BNIL;
						}
					else
						{	/* Ieee/pairlist.scm 525 */
							obj_t BgL_newzd2prevzd2_912;

							if (EPAIRP(BgL_tailz00_909))
								{	/* Ieee/pairlist.scm 526 */
									obj_t BgL_arg1333z00_915;
									obj_t BgL_arg1334z00_916;

									BgL_arg1333z00_915 = CAR(((obj_t) BgL_tailz00_909));
									BgL_arg1334z00_916 = CER(((obj_t) BgL_tailz00_909));
									{	/* Ieee/pairlist.scm 526 */
										obj_t BgL_res1849z00_1883;

										BgL_res1849z00_1883 =
											MAKE_YOUNG_EPAIR(BgL_arg1333z00_915, BgL_l2z00_54,
											BgL_arg1334z00_916);
										BgL_newzd2prevzd2_912 = BgL_res1849z00_1883;
									}
								}
							else
								{	/* Ieee/pairlist.scm 527 */
									obj_t BgL_arg1335z00_917;

									BgL_arg1335z00_917 = CAR(((obj_t) BgL_tailz00_909));
									BgL_newzd2prevzd2_912 =
										MAKE_YOUNG_PAIR(BgL_arg1335z00_917, BgL_l2z00_54);
								}
							SET_CDR(BgL_prevz00_908, BgL_newzd2prevzd2_912);
							{	/* Ieee/pairlist.scm 529 */
								obj_t BgL_arg1331z00_913;

								BgL_arg1331z00_913 = CDR(((obj_t) BgL_tailz00_909));
								{
									obj_t BgL_tailz00_3456;
									obj_t BgL_prevz00_3455;

									BgL_prevz00_3455 = BgL_newzd2prevzd2_912;
									BgL_tailz00_3456 = BgL_arg1331z00_913;
									BgL_tailz00_909 = BgL_tailz00_3456;
									BgL_prevz00_908 = BgL_prevz00_3455;
									goto BgL_loopz00_910;
								}
							}
						}
					return CDR(BgL_headz00_907);
				}
			}
		}

	}



/* &eappend-2 */
	obj_t BGl_z62eappendzd22zb0zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_envz00_2699, obj_t BgL_l1z00_2700, obj_t BgL_l2z00_2701)
	{
		{	/* Ieee/pairlist.scm 518 */
			{	/* Ieee/pairlist.scm 519 */
				obj_t BgL_auxz00_3458;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_l1z00_2700))
					{	/* Ieee/pairlist.scm 519 */
						BgL_auxz00_3458 = BgL_l1z00_2700;
					}
				else
					{
						obj_t BgL_auxz00_3461;

						BgL_auxz00_3461 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(21897L),
							BGl_string2055z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2054z00zz__r4_pairs_and_lists_6_3z00, BgL_l1z00_2700);
						FAILURE(BgL_auxz00_3461, BFALSE, BFALSE);
					}
				return
					BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3458,
					BgL_l2z00_2701);
			}
		}

	}



/* append */
	BGL_EXPORTED_DEF obj_t BGl_appendz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_lz00_55)
	{
		{	/* Ieee/pairlist.scm 536 */
			BGL_TAIL return
				BGl_appendzd2listze72z35zz__r4_pairs_and_lists_6_3z00(BgL_lz00_55);
		}

	}



/* append-list~2 */
	obj_t BGl_appendzd2listze72z35zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_lz00_920)
	{
		{	/* Ieee/pairlist.scm 537 */
			{	/* Ieee/pairlist.scm 538 */
				long BgL_lenz00_922;

				BgL_lenz00_922 = bgl_list_length(BgL_lz00_920);
				{

					switch (BgL_lenz00_922)
						{
						case 0L:

							return BNIL;
							break;
						case 1L:

							return CAR(((obj_t) BgL_lz00_920));
							break;
						case 2L:

							{	/* Ieee/pairlist.scm 544 */
								obj_t BgL_arg1338z00_925;
								obj_t BgL_arg1339z00_926;

								BgL_arg1338z00_925 = CAR(((obj_t) BgL_lz00_920));
								{	/* Ieee/pairlist.scm 545 */
									obj_t BgL_pairz00_1893;

									BgL_pairz00_1893 = CDR(((obj_t) BgL_lz00_920));
									BgL_arg1339z00_926 = CAR(BgL_pairz00_1893);
								}
								return
									BGl_appendzd221011zd2zz__r4_pairs_and_lists_6_3z00
									(BgL_arg1338z00_925, BgL_arg1339z00_926);
							}
							break;
						default:
							{	/* Ieee/pairlist.scm 546 */
								obj_t BgL_arg1341z00_928;
								obj_t BgL_arg1342z00_929;

								BgL_arg1341z00_928 = CAR(((obj_t) BgL_lz00_920));
								{	/* Ieee/pairlist.scm 547 */
									obj_t BgL_arg1343z00_930;

									BgL_arg1343z00_930 = CDR(((obj_t) BgL_lz00_920));
									BgL_arg1342z00_929 =
										BGl_appendzd2listze72z35zz__r4_pairs_and_lists_6_3z00
										(BgL_arg1343z00_930);
								}
								return
									BGl_appendzd221011zd2zz__r4_pairs_and_lists_6_3z00
									(BgL_arg1341z00_928, BgL_arg1342z00_929);
							}
						}
				}
			}
		}

	}



/* &append */
	obj_t BGl_z62appendz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2702,
		obj_t BgL_lz00_2703)
	{
		{	/* Ieee/pairlist.scm 536 */
			return BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_lz00_2703);
		}

	}



/* eappend */
	BGL_EXPORTED_DEF obj_t BGl_eappendz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_lz00_56)
	{
		{	/* Ieee/pairlist.scm 553 */
			BGL_TAIL return
				BGl_appendzd2listze71z35zz__r4_pairs_and_lists_6_3z00(BgL_lz00_56);
		}

	}



/* append-list~1 */
	obj_t BGl_appendzd2listze71z35zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_lz00_931)
	{
		{	/* Ieee/pairlist.scm 554 */
			{	/* Ieee/pairlist.scm 555 */
				long BgL_lenz00_933;

				BgL_lenz00_933 = bgl_list_length(BgL_lz00_931);
				{

					switch (BgL_lenz00_933)
						{
						case 0L:

							return BNIL;
							break;
						case 1L:

							return CAR(((obj_t) BgL_lz00_931));
							break;
						case 2L:

							{	/* Ieee/pairlist.scm 561 */
								obj_t BgL_arg1344z00_936;
								obj_t BgL_arg1346z00_937;

								BgL_arg1344z00_936 = CAR(((obj_t) BgL_lz00_931));
								{	/* Ieee/pairlist.scm 562 */
									obj_t BgL_pairz00_1899;

									BgL_pairz00_1899 = CDR(((obj_t) BgL_lz00_931));
									BgL_arg1346z00_937 = CAR(BgL_pairz00_1899);
								}
								return
									BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
									(BgL_arg1344z00_936, BgL_arg1346z00_937);
							}
							break;
						default:
							{	/* Ieee/pairlist.scm 563 */
								obj_t BgL_arg1348z00_939;
								obj_t BgL_arg1349z00_940;

								BgL_arg1348z00_939 = CAR(((obj_t) BgL_lz00_931));
								BgL_arg1349z00_940 =
									BGl_appendzd2listze71z35zz__r4_pairs_and_lists_6_3z00(CDR(
										((obj_t) BgL_lz00_931)));
								return
									BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
									(BgL_arg1348z00_939, BgL_arg1349z00_940);
							}
						}
				}
			}
		}

	}



/* &eappend */
	obj_t BGl_z62eappendz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2704,
		obj_t BgL_lz00_2705)
	{
		{	/* Ieee/pairlist.scm 553 */
			return BGl_eappendz00zz__r4_pairs_and_lists_6_3z00(BgL_lz00_2705);
		}

	}



/* append! */
	BGL_EXPORTED_DEF obj_t BGl_appendz12z12zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_lz00_57)
	{
		{	/* Ieee/pairlist.scm 570 */
			BGL_TAIL return
				BGl_appendzd2listze70z35zz__r4_pairs_and_lists_6_3z00(BgL_lz00_57);
		}

	}



/* append-list~0 */
	obj_t BGl_appendzd2listze70z35zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_lz00_942)
	{
		{	/* Ieee/pairlist.scm 571 */
			{	/* Ieee/pairlist.scm 572 */
				long BgL_lenz00_944;

				BgL_lenz00_944 = bgl_list_length(BgL_lz00_942);
				{

					switch (BgL_lenz00_944)
						{
						case 0L:

							return BNIL;
							break;
						case 1L:

							return CAR(((obj_t) BgL_lz00_942));
							break;
						case 2L:

							{	/* Ieee/pairlist.scm 578 */
								obj_t BgL_arg1351z00_947;
								obj_t BgL_arg1352z00_948;

								BgL_arg1351z00_947 = CAR(((obj_t) BgL_lz00_942));
								{	/* Ieee/pairlist.scm 579 */
									obj_t BgL_pairz00_1905;

									BgL_pairz00_1905 = CDR(((obj_t) BgL_lz00_942));
									BgL_arg1352z00_948 = CAR(BgL_pairz00_1905);
								}
								return
									BGl_appendzd22z12zc0zz__r4_pairs_and_lists_6_3z00
									(BgL_arg1351z00_947, BgL_arg1352z00_948);
							}
							break;
						default:
							{	/* Ieee/pairlist.scm 580 */
								obj_t BgL_arg1356z00_950;
								obj_t BgL_arg1357z00_951;

								BgL_arg1356z00_950 = CAR(((obj_t) BgL_lz00_942));
								{	/* Ieee/pairlist.scm 581 */
									obj_t BgL_arg1358z00_952;

									BgL_arg1358z00_952 = CDR(((obj_t) BgL_lz00_942));
									BgL_arg1357z00_951 =
										BGl_appendzd2listze70z35zz__r4_pairs_and_lists_6_3z00
										(BgL_arg1358z00_952);
								}
								return
									BGl_appendzd22z12zc0zz__r4_pairs_and_lists_6_3z00
									(BgL_arg1356z00_950, BgL_arg1357z00_951);
							}
						}
				}
			}
		}

	}



/* &append! */
	obj_t BGl_z62appendz12z70zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2706,
		obj_t BgL_lz00_2707)
	{
		{	/* Ieee/pairlist.scm 570 */
			return BGl_appendz12z12zz__r4_pairs_and_lists_6_3z00(BgL_lz00_2707);
		}

	}



/* append-2! */
	BGL_EXPORTED_DEF obj_t BGl_appendzd22z12zc0zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_xz00_58, obj_t BgL_yz00_59)
	{
		{	/* Ieee/pairlist.scm 587 */
			if (NULLP(BgL_xz00_58))
				{	/* Ieee/pairlist.scm 588 */
					return BgL_yz00_59;
				}
			else
				{	/* Ieee/pairlist.scm 591 */
					obj_t BgL_arg1360z00_955;

					BgL_arg1360z00_955 = CDR(BgL_xz00_58);
					{
						obj_t BgL_az00_1916;
						obj_t BgL_bz00_1917;

						BgL_az00_1916 = BgL_xz00_58;
						BgL_bz00_1917 = BgL_arg1360z00_955;
					BgL_dozd2loopzd2zd21018zd2_1915:
						if (NULLP(BgL_bz00_1917))
							{	/* Ieee/pairlist.scm 590 */
								{	/* Ieee/pairlist.scm 454 */
									obj_t BgL_tmpz00_3525;

									BgL_tmpz00_3525 = ((obj_t) BgL_az00_1916);
									SET_CDR(BgL_tmpz00_3525, BgL_yz00_59);
								}
								return BgL_xz00_58;
							}
						else
							{	/* Ieee/pairlist.scm 591 */
								obj_t BgL_arg1363z00_1921;

								BgL_arg1363z00_1921 = CDR(((obj_t) BgL_bz00_1917));
								{
									obj_t BgL_bz00_3531;
									obj_t BgL_az00_3530;

									BgL_az00_3530 = BgL_bz00_1917;
									BgL_bz00_3531 = BgL_arg1363z00_1921;
									BgL_bz00_1917 = BgL_bz00_3531;
									BgL_az00_1916 = BgL_az00_3530;
									goto BgL_dozd2loopzd2zd21018zd2_1915;
								}
							}
					}
				}
		}

	}



/* &append-2! */
	obj_t BGl_z62appendzd22z12za2zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_envz00_2708, obj_t BgL_xz00_2709, obj_t BgL_yz00_2710)
	{
		{	/* Ieee/pairlist.scm 587 */
			{	/* Ieee/pairlist.scm 588 */
				obj_t BgL_auxz00_3539;
				obj_t BgL_auxz00_3532;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_yz00_2710))
					{	/* Ieee/pairlist.scm 588 */
						BgL_auxz00_3539 = BgL_yz00_2710;
					}
				else
					{
						obj_t BgL_auxz00_3542;

						BgL_auxz00_3542 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(24084L),
							BGl_string2056z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2054z00zz__r4_pairs_and_lists_6_3z00, BgL_yz00_2710);
						FAILURE(BgL_auxz00_3542, BFALSE, BFALSE);
					}
				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_xz00_2709))
					{	/* Ieee/pairlist.scm 588 */
						BgL_auxz00_3532 = BgL_xz00_2709;
					}
				else
					{
						obj_t BgL_auxz00_3535;

						BgL_auxz00_3535 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(24084L),
							BGl_string2056z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2054z00zz__r4_pairs_and_lists_6_3z00, BgL_xz00_2709);
						FAILURE(BgL_auxz00_3535, BFALSE, BFALSE);
					}
				return
					BGl_appendzd22z12zc0zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3532,
					BgL_auxz00_3539);
			}
		}

	}



/* length */
	BGL_EXPORTED_DEF long bgl_list_length(obj_t BgL_listz00_60)
	{
		{	/* Ieee/pairlist.scm 599 */
			{
				obj_t BgL_lz00_1936;
				long BgL_resz00_1937;

				BgL_lz00_1936 = BgL_listz00_60;
				BgL_resz00_1937 = 0L;
			BgL_loopz00_1935:
				if (NULLP(BgL_lz00_1936))
					{	/* Ieee/pairlist.scm 603 */
						return BgL_resz00_1937;
					}
				else
					{
						long BgL_resz00_3552;
						obj_t BgL_lz00_3549;

						BgL_lz00_3549 = CDR(((obj_t) BgL_lz00_1936));
						BgL_resz00_3552 = (1L + BgL_resz00_1937);
						BgL_resz00_1937 = BgL_resz00_3552;
						BgL_lz00_1936 = BgL_lz00_3549;
						goto BgL_loopz00_1935;
					}
			}
		}

	}



/* &length */
	obj_t BGl_z62lengthz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2711,
		obj_t BgL_listz00_2712)
	{
		{	/* Ieee/pairlist.scm 599 */
			{	/* Ieee/pairlist.scm 600 */
				long BgL_tmpz00_3554;

				{	/* Ieee/pairlist.scm 600 */
					obj_t BgL_auxz00_3555;

					if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
						(BgL_listz00_2712))
						{	/* Ieee/pairlist.scm 600 */
							BgL_auxz00_3555 = BgL_listz00_2712;
						}
					else
						{
							obj_t BgL_auxz00_3558;

							BgL_auxz00_3558 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(24469L),
								BGl_string2057z00zz__r4_pairs_and_lists_6_3z00,
								BGl_string2054z00zz__r4_pairs_and_lists_6_3z00,
								BgL_listz00_2712);
							FAILURE(BgL_auxz00_3558, BFALSE, BFALSE);
						}
					BgL_tmpz00_3554 = bgl_list_length(BgL_auxz00_3555);
				}
				return BINT(BgL_tmpz00_3554);
			}
		}

	}



/* reverse */
	BGL_EXPORTED_DEF obj_t bgl_reverse(obj_t BgL_lz00_61)
	{
		{	/* Ieee/pairlist.scm 611 */
			{
				obj_t BgL_lz00_1959;
				obj_t BgL_accz00_1960;

				BgL_lz00_1959 = BgL_lz00_61;
				BgL_accz00_1960 = BNIL;
			BgL_loopz00_1958:
				if (NULLP(BgL_lz00_1959))
					{	/* Ieee/pairlist.scm 614 */
						return BgL_accz00_1960;
					}
				else
					{	/* Ieee/pairlist.scm 616 */
						obj_t BgL_arg1370z00_1966;
						obj_t BgL_arg1371z00_1967;

						BgL_arg1370z00_1966 = CDR(((obj_t) BgL_lz00_1959));
						{	/* Ieee/pairlist.scm 616 */
							obj_t BgL_arg1372z00_1968;

							BgL_arg1372z00_1968 = CAR(((obj_t) BgL_lz00_1959));
							BgL_arg1371z00_1967 =
								MAKE_YOUNG_PAIR(BgL_arg1372z00_1968, BgL_accz00_1960);
						}
						{
							obj_t BgL_accz00_3572;
							obj_t BgL_lz00_3571;

							BgL_lz00_3571 = BgL_arg1370z00_1966;
							BgL_accz00_3572 = BgL_arg1371z00_1967;
							BgL_accz00_1960 = BgL_accz00_3572;
							BgL_lz00_1959 = BgL_lz00_3571;
							goto BgL_loopz00_1958;
						}
					}
			}
		}

	}



/* &reverse */
	obj_t BGl_z62reversez62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2713,
		obj_t BgL_lz00_2714)
	{
		{	/* Ieee/pairlist.scm 611 */
			{	/* Ieee/pairlist.scm 612 */
				obj_t BgL_auxz00_3573;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_lz00_2714))
					{	/* Ieee/pairlist.scm 612 */
						BgL_auxz00_3573 = BgL_lz00_2714;
					}
				else
					{
						obj_t BgL_auxz00_3576;

						BgL_auxz00_3576 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(24839L),
							BGl_string2058z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2054z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_2714);
						FAILURE(BgL_auxz00_3576, BFALSE, BFALSE);
					}
				return bgl_reverse(BgL_auxz00_3573);
			}
		}

	}



/* ereverse */
	BGL_EXPORTED_DEF obj_t BGl_ereversez00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_lz00_62)
	{
		{	/* Ieee/pairlist.scm 621 */
			{
				obj_t BgL_lz00_982;
				obj_t BgL_accz00_983;

				BgL_lz00_982 = BgL_lz00_62;
				BgL_accz00_983 = BNIL;
			BgL_zc3z04anonymousza31373ze3z87_984:
				if (NULLP(BgL_lz00_982))
					{	/* Ieee/pairlist.scm 624 */
						return BgL_accz00_983;
					}
				else
					{	/* Ieee/pairlist.scm 626 */
						obj_t BgL_arg1375z00_986;
						obj_t BgL_arg1376z00_987;

						BgL_arg1375z00_986 = CDR(((obj_t) BgL_lz00_982));
						if (EPAIRP(BgL_lz00_982))
							{	/* Ieee/pairlist.scm 628 */
								obj_t BgL_arg1378z00_989;
								obj_t BgL_arg1379z00_990;

								BgL_arg1378z00_989 = CAR(((obj_t) BgL_lz00_982));
								BgL_arg1379z00_990 = CER(((obj_t) BgL_lz00_982));
								{	/* Ieee/pairlist.scm 628 */
									obj_t BgL_res1850z00_1974;

									BgL_res1850z00_1974 =
										MAKE_YOUNG_EPAIR(BgL_arg1378z00_989, BgL_accz00_983,
										BgL_arg1379z00_990);
									BgL_arg1376z00_987 = BgL_res1850z00_1974;
								}
							}
						else
							{	/* Ieee/pairlist.scm 629 */
								obj_t BgL_arg1380z00_991;

								BgL_arg1380z00_991 = CAR(((obj_t) BgL_lz00_982));
								BgL_arg1376z00_987 =
									MAKE_YOUNG_PAIR(BgL_arg1380z00_991, BgL_accz00_983);
							}
						{
							obj_t BgL_accz00_3596;
							obj_t BgL_lz00_3595;

							BgL_lz00_3595 = BgL_arg1375z00_986;
							BgL_accz00_3596 = BgL_arg1376z00_987;
							BgL_accz00_983 = BgL_accz00_3596;
							BgL_lz00_982 = BgL_lz00_3595;
							goto BgL_zc3z04anonymousza31373ze3z87_984;
						}
					}
			}
		}

	}



/* &ereverse */
	obj_t BGl_z62ereversez62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2715,
		obj_t BgL_lz00_2716)
	{
		{	/* Ieee/pairlist.scm 621 */
			{	/* Ieee/pairlist.scm 624 */
				obj_t BgL_auxz00_3597;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_lz00_2716))
					{	/* Ieee/pairlist.scm 624 */
						BgL_auxz00_3597 = BgL_lz00_2716;
					}
				else
					{
						obj_t BgL_auxz00_3600;

						BgL_auxz00_3600 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(25233L),
							BGl_string2059z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2054z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_2716);
						FAILURE(BgL_auxz00_3600, BFALSE, BFALSE);
					}
				return BGl_ereversez00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3597);
			}
		}

	}



/* take */
	BGL_EXPORTED_DEF obj_t BGl_takez00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_listz00_63, long BgL_kz00_64)
	{
		{	/* Ieee/pairlist.scm 634 */
			{
				obj_t BgL_listz00_1997;
				long BgL_kz00_1998;
				obj_t BgL_resz00_1999;

				BgL_listz00_1997 = BgL_listz00_63;
				BgL_kz00_1998 = BgL_kz00_64;
				BgL_resz00_1999 = BNIL;
			BgL_loopz00_1996:
				if ((BgL_kz00_1998 == 0L))
					{	/* Ieee/pairlist.scm 638 */
						return bgl_reverse_bang(BgL_resz00_1999);
					}
				else
					{	/* Ieee/pairlist.scm 640 */
						obj_t BgL_arg1383z00_2006;
						long BgL_arg1384z00_2007;
						obj_t BgL_arg1387z00_2008;

						BgL_arg1383z00_2006 = CDR(((obj_t) BgL_listz00_1997));
						BgL_arg1384z00_2007 = (BgL_kz00_1998 - 1L);
						{	/* Ieee/pairlist.scm 640 */
							obj_t BgL_arg1388z00_2009;

							BgL_arg1388z00_2009 = CAR(((obj_t) BgL_listz00_1997));
							BgL_arg1387z00_2008 =
								MAKE_YOUNG_PAIR(BgL_arg1388z00_2009, BgL_resz00_1999);
						}
						{
							obj_t BgL_resz00_3616;
							long BgL_kz00_3615;
							obj_t BgL_listz00_3614;

							BgL_listz00_3614 = BgL_arg1383z00_2006;
							BgL_kz00_3615 = BgL_arg1384z00_2007;
							BgL_resz00_3616 = BgL_arg1387z00_2008;
							BgL_resz00_1999 = BgL_resz00_3616;
							BgL_kz00_1998 = BgL_kz00_3615;
							BgL_listz00_1997 = BgL_listz00_3614;
							goto BgL_loopz00_1996;
						}
					}
			}
		}

	}



/* &take */
	obj_t BGl_z62takez62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2717,
		obj_t BgL_listz00_2718, obj_t BgL_kz00_2719)
	{
		{	/* Ieee/pairlist.scm 634 */
			{	/* Ieee/pairlist.scm 635 */
				long BgL_auxz00_3624;
				obj_t BgL_auxz00_3617;

				{	/* Ieee/pairlist.scm 635 */
					obj_t BgL_tmpz00_3625;

					if (INTEGERP(BgL_kz00_2719))
						{	/* Ieee/pairlist.scm 635 */
							BgL_tmpz00_3625 = BgL_kz00_2719;
						}
					else
						{
							obj_t BgL_auxz00_3628;

							BgL_auxz00_3628 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(25600L),
								BGl_string2060z00zz__r4_pairs_and_lists_6_3z00,
								BGl_string2061z00zz__r4_pairs_and_lists_6_3z00, BgL_kz00_2719);
							FAILURE(BgL_auxz00_3628, BFALSE, BFALSE);
						}
					BgL_auxz00_3624 = (long) CINT(BgL_tmpz00_3625);
				}
				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_listz00_2718))
					{	/* Ieee/pairlist.scm 635 */
						BgL_auxz00_3617 = BgL_listz00_2718;
					}
				else
					{
						obj_t BgL_auxz00_3620;

						BgL_auxz00_3620 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(25600L),
							BGl_string2060z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2054z00zz__r4_pairs_and_lists_6_3z00, BgL_listz00_2718);
						FAILURE(BgL_auxz00_3620, BFALSE, BFALSE);
					}
				return
					BGl_takez00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3617,
					BgL_auxz00_3624);
			}
		}

	}



/* drop */
	BGL_EXPORTED_DEF obj_t BGl_dropz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_listz00_65, long BgL_kz00_66)
	{
		{	/* Ieee/pairlist.scm 645 */
		BGl_dropz00zz__r4_pairs_and_lists_6_3z00:
			if ((BgL_kz00_66 == 0L))
				{	/* Ieee/pairlist.scm 646 */
					return BgL_listz00_65;
				}
			else
				{	/* Ieee/pairlist.scm 648 */
					obj_t BgL_listz00_2022;
					long BgL_kz00_2023;

					BgL_listz00_2022 = CDR(((obj_t) BgL_listz00_65));
					BgL_kz00_2023 = (BgL_kz00_66 - 1L);
					if ((BgL_kz00_2023 == 0L))
						{	/* Ieee/pairlist.scm 646 */
							return BgL_listz00_2022;
						}
					else
						{
							long BgL_kz00_3644;
							obj_t BgL_listz00_3641;

							BgL_listz00_3641 = CDR(((obj_t) BgL_listz00_2022));
							BgL_kz00_3644 = (BgL_kz00_2023 - 1L);
							BgL_kz00_66 = BgL_kz00_3644;
							BgL_listz00_65 = BgL_listz00_3641;
							goto BGl_dropz00zz__r4_pairs_and_lists_6_3z00;
						}
				}
		}

	}



/* &drop */
	obj_t BGl_z62dropz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2720,
		obj_t BgL_listz00_2721, obj_t BgL_kz00_2722)
	{
		{	/* Ieee/pairlist.scm 645 */
			{	/* Ieee/pairlist.scm 646 */
				long BgL_auxz00_3653;
				obj_t BgL_auxz00_3646;

				{	/* Ieee/pairlist.scm 646 */
					obj_t BgL_tmpz00_3654;

					if (INTEGERP(BgL_kz00_2722))
						{	/* Ieee/pairlist.scm 646 */
							BgL_tmpz00_3654 = BgL_kz00_2722;
						}
					else
						{
							obj_t BgL_auxz00_3657;

							BgL_auxz00_3657 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(25998L),
								BGl_string2062z00zz__r4_pairs_and_lists_6_3z00,
								BGl_string2061z00zz__r4_pairs_and_lists_6_3z00, BgL_kz00_2722);
							FAILURE(BgL_auxz00_3657, BFALSE, BFALSE);
						}
					BgL_auxz00_3653 = (long) CINT(BgL_tmpz00_3654);
				}
				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_listz00_2721))
					{	/* Ieee/pairlist.scm 646 */
						BgL_auxz00_3646 = BgL_listz00_2721;
					}
				else
					{
						obj_t BgL_auxz00_3649;

						BgL_auxz00_3649 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(25998L),
							BGl_string2062z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2054z00zz__r4_pairs_and_lists_6_3z00, BgL_listz00_2721);
						FAILURE(BgL_auxz00_3649, BFALSE, BFALSE);
					}
				return
					BGl_dropz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3646,
					BgL_auxz00_3653);
			}
		}

	}



/* list-tail */
	BGL_EXPORTED_DEF obj_t BGl_listzd2tailzd2zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_listz00_67, long BgL_kz00_68)
	{
		{	/* Ieee/pairlist.scm 653 */
			{
				obj_t BgL_listz00_2032;
				long BgL_kz00_2033;

				BgL_listz00_2032 = BgL_listz00_67;
				BgL_kz00_2033 = BgL_kz00_68;
			BgL_dropz00_2031:
				if ((BgL_kz00_2033 == 0L))
					{	/* Ieee/pairlist.scm 646 */
						return BgL_listz00_2032;
					}
				else
					{
						long BgL_kz00_3668;
						obj_t BgL_listz00_3665;

						BgL_listz00_3665 = CDR(((obj_t) BgL_listz00_2032));
						BgL_kz00_3668 = (BgL_kz00_2033 - 1L);
						BgL_kz00_2033 = BgL_kz00_3668;
						BgL_listz00_2032 = BgL_listz00_3665;
						goto BgL_dropz00_2031;
					}
			}
		}

	}



/* &list-tail */
	obj_t BGl_z62listzd2tailzb0zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_envz00_2723, obj_t BgL_listz00_2724, obj_t BgL_kz00_2725)
	{
		{	/* Ieee/pairlist.scm 653 */
			{	/* Ieee/pairlist.scm 654 */
				long BgL_auxz00_3677;
				obj_t BgL_auxz00_3670;

				{	/* Ieee/pairlist.scm 654 */
					obj_t BgL_tmpz00_3678;

					if (INTEGERP(BgL_kz00_2725))
						{	/* Ieee/pairlist.scm 654 */
							BgL_tmpz00_3678 = BgL_kz00_2725;
						}
					else
						{
							obj_t BgL_auxz00_3681;

							BgL_auxz00_3681 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(26316L),
								BGl_string2063z00zz__r4_pairs_and_lists_6_3z00,
								BGl_string2061z00zz__r4_pairs_and_lists_6_3z00, BgL_kz00_2725);
							FAILURE(BgL_auxz00_3681, BFALSE, BFALSE);
						}
					BgL_auxz00_3677 = (long) CINT(BgL_tmpz00_3678);
				}
				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_listz00_2724))
					{	/* Ieee/pairlist.scm 654 */
						BgL_auxz00_3670 = BgL_listz00_2724;
					}
				else
					{
						obj_t BgL_auxz00_3673;

						BgL_auxz00_3673 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(26316L),
							BGl_string2063z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2054z00zz__r4_pairs_and_lists_6_3z00, BgL_listz00_2724);
						FAILURE(BgL_auxz00_3673, BFALSE, BFALSE);
					}
				return
					BGl_listzd2tailzd2zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3670,
					BgL_auxz00_3677);
			}
		}

	}



/* list-ref */
	BGL_EXPORTED_DEF obj_t bgl_list_ref(obj_t BgL_listz00_69, long BgL_kz00_70)
	{
		{	/* Ieee/pairlist.scm 659 */
		bgl_list_ref:
			if ((BgL_kz00_70 == 0L))
				{	/* Ieee/pairlist.scm 660 */
					return CAR(((obj_t) BgL_listz00_69));
				}
			else
				{	/* Ieee/pairlist.scm 662 */
					obj_t BgL_listz00_2052;
					long BgL_kz00_2053;

					BgL_listz00_2052 = CDR(((obj_t) BgL_listz00_69));
					BgL_kz00_2053 = (BgL_kz00_70 - 1L);
					if ((BgL_kz00_2053 == 0L))
						{	/* Ieee/pairlist.scm 660 */
							return CAR(((obj_t) BgL_listz00_2052));
						}
					else
						{
							long BgL_kz00_3701;
							obj_t BgL_listz00_3698;

							BgL_listz00_3698 = CDR(((obj_t) BgL_listz00_2052));
							BgL_kz00_3701 = (BgL_kz00_2053 - 1L);
							BgL_kz00_70 = BgL_kz00_3701;
							BgL_listz00_69 = BgL_listz00_3698;
							goto bgl_list_ref;
						}
				}
		}

	}



/* &list-ref */
	obj_t BGl_z62listzd2refzb0zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2726,
		obj_t BgL_listz00_2727, obj_t BgL_kz00_2728)
	{
		{	/* Ieee/pairlist.scm 659 */
			{	/* Ieee/pairlist.scm 660 */
				long BgL_auxz00_3710;
				obj_t BgL_auxz00_3703;

				{	/* Ieee/pairlist.scm 660 */
					obj_t BgL_tmpz00_3711;

					if (INTEGERP(BgL_kz00_2728))
						{	/* Ieee/pairlist.scm 660 */
							BgL_tmpz00_3711 = BgL_kz00_2728;
						}
					else
						{
							obj_t BgL_auxz00_3714;

							BgL_auxz00_3714 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(26583L),
								BGl_string2064z00zz__r4_pairs_and_lists_6_3z00,
								BGl_string2061z00zz__r4_pairs_and_lists_6_3z00, BgL_kz00_2728);
							FAILURE(BgL_auxz00_3714, BFALSE, BFALSE);
						}
					BgL_auxz00_3710 = (long) CINT(BgL_tmpz00_3711);
				}
				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_listz00_2727))
					{	/* Ieee/pairlist.scm 660 */
						BgL_auxz00_3703 = BgL_listz00_2727;
					}
				else
					{
						obj_t BgL_auxz00_3706;

						BgL_auxz00_3706 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(26583L),
							BGl_string2064z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2054z00zz__r4_pairs_and_lists_6_3z00, BgL_listz00_2727);
						FAILURE(BgL_auxz00_3706, BFALSE, BFALSE);
					}
				return bgl_list_ref(BgL_auxz00_3703, BgL_auxz00_3710);
			}
		}

	}



/* list-set! */
	BGL_EXPORTED_DEF obj_t BGl_listzd2setz12zc0zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_listz00_71, long BgL_kz00_72, obj_t BgL_valz00_73)
	{
		{	/* Ieee/pairlist.scm 667 */
		BGl_listzd2setz12zc0zz__r4_pairs_and_lists_6_3z00:
			if ((BgL_kz00_72 == 0L))
				{	/* Ieee/pairlist.scm 448 */
					obj_t BgL_tmpz00_3722;

					BgL_tmpz00_3722 = ((obj_t) BgL_listz00_71);
					return SET_CAR(BgL_tmpz00_3722, BgL_valz00_73);
				}
			else
				{	/* Ieee/pairlist.scm 670 */
					obj_t BgL_arg1396z00_2063;
					long BgL_arg1397z00_2064;

					BgL_arg1396z00_2063 = CDR(((obj_t) BgL_listz00_71));
					BgL_arg1397z00_2064 = (BgL_kz00_72 - 1L);
					if ((BgL_arg1397z00_2064 == 0L))
						{	/* Ieee/pairlist.scm 448 */
							obj_t BgL_tmpz00_3730;

							BgL_tmpz00_3730 = ((obj_t) ((obj_t) BgL_arg1396z00_2063));
							return SET_CAR(BgL_tmpz00_3730, BgL_valz00_73);
						}
					else
						{	/* Ieee/pairlist.scm 670 */
							obj_t BgL_arg1396z00_2073;
							long BgL_arg1397z00_2074;

							BgL_arg1396z00_2073 =
								CDR(((obj_t) ((obj_t) BgL_arg1396z00_2063)));
							BgL_arg1397z00_2074 = (BgL_arg1397z00_2064 - 1L);
							{
								long BgL_kz00_3739;
								obj_t BgL_listz00_3738;

								BgL_listz00_3738 = BgL_arg1396z00_2073;
								BgL_kz00_3739 = BgL_arg1397z00_2074;
								BgL_kz00_72 = BgL_kz00_3739;
								BgL_listz00_71 = BgL_listz00_3738;
								goto BGl_listzd2setz12zc0zz__r4_pairs_and_lists_6_3z00;
							}
						}
				}
		}

	}



/* &list-set! */
	obj_t BGl_z62listzd2setz12za2zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_envz00_2729, obj_t BgL_listz00_2730, obj_t BgL_kz00_2731,
		obj_t BgL_valz00_2732)
	{
		{	/* Ieee/pairlist.scm 667 */
			{	/* Ieee/pairlist.scm 668 */
				long BgL_auxz00_3747;
				obj_t BgL_auxz00_3740;

				{	/* Ieee/pairlist.scm 668 */
					obj_t BgL_tmpz00_3748;

					if (INTEGERP(BgL_kz00_2731))
						{	/* Ieee/pairlist.scm 668 */
							BgL_tmpz00_3748 = BgL_kz00_2731;
						}
					else
						{
							obj_t BgL_auxz00_3751;

							BgL_auxz00_3751 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(26915L),
								BGl_string2065z00zz__r4_pairs_and_lists_6_3z00,
								BGl_string2061z00zz__r4_pairs_and_lists_6_3z00, BgL_kz00_2731);
							FAILURE(BgL_auxz00_3751, BFALSE, BFALSE);
						}
					BgL_auxz00_3747 = (long) CINT(BgL_tmpz00_3748);
				}
				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_listz00_2730))
					{	/* Ieee/pairlist.scm 668 */
						BgL_auxz00_3740 = BgL_listz00_2730;
					}
				else
					{
						obj_t BgL_auxz00_3743;

						BgL_auxz00_3743 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(26915L),
							BGl_string2065z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2054z00zz__r4_pairs_and_lists_6_3z00, BgL_listz00_2730);
						FAILURE(BgL_auxz00_3743, BFALSE, BFALSE);
					}
				return
					BGl_listzd2setz12zc0zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3740,
					BgL_auxz00_3747, BgL_valz00_2732);
			}
		}

	}



/* last-pair */
	BGL_EXPORTED_DEF obj_t BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_xz00_74)
	{
		{	/* Ieee/pairlist.scm 675 */
		BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00:
			{	/* Ieee/pairlist.scm 676 */
				bool_t BgL_test2339z00_3757;

				{	/* Ieee/pairlist.scm 229 */
					obj_t BgL_tmpz00_3758;

					BgL_tmpz00_3758 = CDR(BgL_xz00_74);
					BgL_test2339z00_3757 = PAIRP(BgL_tmpz00_3758);
				}
				if (BgL_test2339z00_3757)
					{	/* Ieee/pairlist.scm 677 */
						obj_t BgL_xz00_2085;

						BgL_xz00_2085 = CDR(BgL_xz00_74);
						{	/* Ieee/pairlist.scm 676 */
							bool_t BgL_test2340z00_3762;

							{	/* Ieee/pairlist.scm 229 */
								obj_t BgL_tmpz00_3763;

								BgL_tmpz00_3763 = CDR(BgL_xz00_2085);
								BgL_test2340z00_3762 = PAIRP(BgL_tmpz00_3763);
							}
							if (BgL_test2340z00_3762)
								{
									obj_t BgL_xz00_3766;

									BgL_xz00_3766 = CDR(BgL_xz00_2085);
									BgL_xz00_74 = BgL_xz00_3766;
									goto BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00;
								}
							else
								{	/* Ieee/pairlist.scm 676 */
									return BgL_xz00_2085;
								}
						}
					}
				else
					{	/* Ieee/pairlist.scm 676 */
						return BgL_xz00_74;
					}
			}
		}

	}



/* &last-pair */
	obj_t BGl_z62lastzd2pairzb0zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_envz00_2733, obj_t BgL_xz00_2734)
	{
		{	/* Ieee/pairlist.scm 675 */
			{	/* Ieee/pairlist.scm 676 */
				obj_t BgL_auxz00_3768;

				if (PAIRP(BgL_xz00_2734))
					{	/* Ieee/pairlist.scm 676 */
						BgL_auxz00_3768 = BgL_xz00_2734;
					}
				else
					{
						obj_t BgL_auxz00_3771;

						BgL_auxz00_3771 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(27252L),
							BGl_string2066z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2018z00zz__r4_pairs_and_lists_6_3z00, BgL_xz00_2734);
						FAILURE(BgL_auxz00_3771, BFALSE, BFALSE);
					}
				return BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3768);
			}
		}

	}



/* memq */
	BGL_EXPORTED_DEF obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_objz00_75, obj_t BgL_listz00_76)
	{
		{	/* Ieee/pairlist.scm 683 */
			{
				obj_t BgL_listz00_2092;

				BgL_listz00_2092 = BgL_listz00_76;
			BgL_loopz00_2091:
				if (PAIRP(BgL_listz00_2092))
					{	/* Ieee/pairlist.scm 685 */
						if ((CAR(BgL_listz00_2092) == BgL_objz00_75))
							{	/* Ieee/pairlist.scm 686 */
								return BgL_listz00_2092;
							}
						else
							{
								obj_t BgL_listz00_3781;

								BgL_listz00_3781 = CDR(BgL_listz00_2092);
								BgL_listz00_2092 = BgL_listz00_3781;
								goto BgL_loopz00_2091;
							}
					}
				else
					{	/* Ieee/pairlist.scm 685 */
						return BFALSE;
					}
			}
		}

	}



/* &memq */
	obj_t BGl_z62memqz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2735,
		obj_t BgL_objz00_2736, obj_t BgL_listz00_2737)
	{
		{	/* Ieee/pairlist.scm 683 */
			{	/* Ieee/pairlist.scm 685 */
				obj_t BgL_auxz00_3783;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_listz00_2737))
					{	/* Ieee/pairlist.scm 685 */
						BgL_auxz00_3783 = BgL_listz00_2737;
					}
				else
					{
						obj_t BgL_auxz00_3786;

						BgL_auxz00_3786 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(27590L),
							BGl_string2067z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2054z00zz__r4_pairs_and_lists_6_3z00, BgL_listz00_2737);
						FAILURE(BgL_auxz00_3786, BFALSE, BFALSE);
					}
				return
					BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_objz00_2736,
					BgL_auxz00_3783);
			}
		}

	}



/* memv */
	BGL_EXPORTED_DEF obj_t BGl_memvz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_objz00_77, obj_t BgL_listz00_78)
	{
		{	/* Ieee/pairlist.scm 694 */
			{
				obj_t BgL_listz00_2100;

				BgL_listz00_2100 = BgL_listz00_78;
			BgL_loopz00_2099:
				if (PAIRP(BgL_listz00_2100))
					{	/* Ieee/pairlist.scm 696 */
						if (BGl_eqvzf3zf3zz__r4_equivalence_6_2z00(CAR(BgL_listz00_2100),
								BgL_objz00_77))
							{	/* Ieee/pairlist.scm 697 */
								return BgL_listz00_2100;
							}
						else
							{
								obj_t BgL_listz00_3796;

								BgL_listz00_3796 = CDR(BgL_listz00_2100);
								BgL_listz00_2100 = BgL_listz00_3796;
								goto BgL_loopz00_2099;
							}
					}
				else
					{	/* Ieee/pairlist.scm 696 */
						return BFALSE;
					}
			}
		}

	}



/* &memv */
	obj_t BGl_z62memvz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2738,
		obj_t BgL_objz00_2739, obj_t BgL_listz00_2740)
	{
		{	/* Ieee/pairlist.scm 694 */
			{	/* Ieee/pairlist.scm 696 */
				obj_t BgL_auxz00_3798;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_listz00_2740))
					{	/* Ieee/pairlist.scm 696 */
						BgL_auxz00_3798 = BgL_listz00_2740;
					}
				else
					{
						obj_t BgL_auxz00_3801;

						BgL_auxz00_3801 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(27962L),
							BGl_string2068z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2054z00zz__r4_pairs_and_lists_6_3z00, BgL_listz00_2740);
						FAILURE(BgL_auxz00_3801, BFALSE, BFALSE);
					}
				return
					BGl_memvz00zz__r4_pairs_and_lists_6_3z00(BgL_objz00_2739,
					BgL_auxz00_3798);
			}
		}

	}



/* member */
	BGL_EXPORTED_DEF obj_t BGl_memberz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_objz00_79, obj_t BgL_listz00_80)
	{
		{	/* Ieee/pairlist.scm 705 */
			{
				obj_t BgL_listz00_2108;

				BgL_listz00_2108 = BgL_listz00_80;
			BgL_loopz00_2107:
				if (PAIRP(BgL_listz00_2108))
					{	/* Ieee/pairlist.scm 708 */
						if (BGl_equalzf3zf3zz__r4_equivalence_6_2z00(BgL_objz00_79,
								CAR(BgL_listz00_2108)))
							{	/* Ieee/pairlist.scm 709 */
								return BgL_listz00_2108;
							}
						else
							{
								obj_t BgL_listz00_3811;

								BgL_listz00_3811 = CDR(BgL_listz00_2108);
								BgL_listz00_2108 = BgL_listz00_3811;
								goto BgL_loopz00_2107;
							}
					}
				else
					{	/* Ieee/pairlist.scm 708 */
						return BFALSE;
					}
			}
		}

	}



/* &member */
	obj_t BGl_z62memberz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2741,
		obj_t BgL_objz00_2742, obj_t BgL_listz00_2743)
	{
		{	/* Ieee/pairlist.scm 705 */
			{	/* Ieee/pairlist.scm 708 */
				obj_t BgL_auxz00_3813;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_listz00_2743))
					{	/* Ieee/pairlist.scm 708 */
						BgL_auxz00_3813 = BgL_listz00_2743;
					}
				else
					{
						obj_t BgL_auxz00_3816;

						BgL_auxz00_3816 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(28345L),
							BGl_string2069z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2054z00zz__r4_pairs_and_lists_6_3z00, BgL_listz00_2743);
						FAILURE(BgL_auxz00_3816, BFALSE, BFALSE);
					}
				return
					BGl_memberz00zz__r4_pairs_and_lists_6_3z00(BgL_objz00_2742,
					BgL_auxz00_3813);
			}
		}

	}



/* assq */
	BGL_EXPORTED_DEF obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_objz00_81, obj_t BgL_alistz00_82)
	{
		{	/* Ieee/pairlist.scm 715 */
			{
				obj_t BgL_alistz00_1046;

				BgL_alistz00_1046 = BgL_alistz00_82;
			BgL_zc3z04anonymousza31421ze3z87_1047:
				if (PAIRP(BgL_alistz00_1046))
					{	/* Ieee/pairlist.scm 717 */
						if ((CAR(CAR(BgL_alistz00_1046)) == BgL_objz00_81))
							{	/* Ieee/pairlist.scm 718 */
								return CAR(BgL_alistz00_1046);
							}
						else
							{
								obj_t BgL_alistz00_3828;

								BgL_alistz00_3828 = CDR(BgL_alistz00_1046);
								BgL_alistz00_1046 = BgL_alistz00_3828;
								goto BgL_zc3z04anonymousza31421ze3z87_1047;
							}
					}
				else
					{	/* Ieee/pairlist.scm 717 */
						return BFALSE;
					}
			}
		}

	}



/* &assq */
	obj_t BGl_z62assqz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2744,
		obj_t BgL_objz00_2745, obj_t BgL_alistz00_2746)
	{
		{	/* Ieee/pairlist.scm 715 */
			{	/* Ieee/pairlist.scm 717 */
				obj_t BgL_auxz00_3830;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_alistz00_2746))
					{	/* Ieee/pairlist.scm 717 */
						BgL_auxz00_3830 = BgL_alistz00_2746;
					}
				else
					{
						obj_t BgL_auxz00_3833;

						BgL_auxz00_3833 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(28716L),
							BGl_string2070z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2054z00zz__r4_pairs_and_lists_6_3z00,
							BgL_alistz00_2746);
						FAILURE(BgL_auxz00_3833, BFALSE, BFALSE);
					}
				return
					BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_objz00_2745,
					BgL_auxz00_3830);
			}
		}

	}



/* assv */
	BGL_EXPORTED_DEF obj_t BGl_assvz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_objz00_83, obj_t BgL_alistz00_84)
	{
		{	/* Ieee/pairlist.scm 726 */
			{
				obj_t BgL_alistz00_1057;

				BgL_alistz00_1057 = BgL_alistz00_84;
			BgL_zc3z04anonymousza31429ze3z87_1058:
				if (PAIRP(BgL_alistz00_1057))
					{	/* Ieee/pairlist.scm 728 */
						if (BGl_eqvzf3zf3zz__r4_equivalence_6_2z00(CAR(CAR
									(BgL_alistz00_1057)), BgL_objz00_83))
							{	/* Ieee/pairlist.scm 729 */
								return CAR(BgL_alistz00_1057);
							}
						else
							{
								obj_t BgL_alistz00_3845;

								BgL_alistz00_3845 = CDR(BgL_alistz00_1057);
								BgL_alistz00_1057 = BgL_alistz00_3845;
								goto BgL_zc3z04anonymousza31429ze3z87_1058;
							}
					}
				else
					{	/* Ieee/pairlist.scm 728 */
						return BFALSE;
					}
			}
		}

	}



/* &assv */
	obj_t BGl_z62assvz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2747,
		obj_t BgL_objz00_2748, obj_t BgL_alistz00_2749)
	{
		{	/* Ieee/pairlist.scm 726 */
			{	/* Ieee/pairlist.scm 728 */
				obj_t BgL_auxz00_3847;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_alistz00_2749))
					{	/* Ieee/pairlist.scm 728 */
						BgL_auxz00_3847 = BgL_alistz00_2749;
					}
				else
					{
						obj_t BgL_auxz00_3850;

						BgL_auxz00_3850 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(29135L),
							BGl_string2071z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2054z00zz__r4_pairs_and_lists_6_3z00,
							BgL_alistz00_2749);
						FAILURE(BgL_auxz00_3850, BFALSE, BFALSE);
					}
				return
					BGl_assvz00zz__r4_pairs_and_lists_6_3z00(BgL_objz00_2748,
					BgL_auxz00_3847);
			}
		}

	}



/* assoc */
	BGL_EXPORTED_DEF obj_t BGl_assocz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_objz00_85, obj_t BgL_alistz00_86)
	{
		{	/* Ieee/pairlist.scm 737 */
			{
				obj_t BgL_alistz00_1068;

				BgL_alistz00_1068 = BgL_alistz00_86;
			BgL_zc3z04anonymousza31439ze3z87_1069:
				if (PAIRP(BgL_alistz00_1068))
					{	/* Ieee/pairlist.scm 739 */
						if (BGl_equalzf3zf3zz__r4_equivalence_6_2z00(CAR(CAR
									(BgL_alistz00_1068)), BgL_objz00_85))
							{	/* Ieee/pairlist.scm 740 */
								return CAR(BgL_alistz00_1068);
							}
						else
							{
								obj_t BgL_alistz00_3862;

								BgL_alistz00_3862 = CDR(BgL_alistz00_1068);
								BgL_alistz00_1068 = BgL_alistz00_3862;
								goto BgL_zc3z04anonymousza31439ze3z87_1069;
							}
					}
				else
					{	/* Ieee/pairlist.scm 739 */
						return BFALSE;
					}
			}
		}

	}



/* &assoc */
	obj_t BGl_z62assocz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2750,
		obj_t BgL_objz00_2751, obj_t BgL_alistz00_2752)
	{
		{	/* Ieee/pairlist.scm 737 */
			{	/* Ieee/pairlist.scm 739 */
				obj_t BgL_auxz00_3864;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_alistz00_2752))
					{	/* Ieee/pairlist.scm 739 */
						BgL_auxz00_3864 = BgL_alistz00_2752;
					}
				else
					{
						obj_t BgL_auxz00_3867;

						BgL_auxz00_3867 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(29549L),
							BGl_string2072z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2054z00zz__r4_pairs_and_lists_6_3z00,
							BgL_alistz00_2752);
						FAILURE(BgL_auxz00_3867, BFALSE, BFALSE);
					}
				return
					BGl_assocz00zz__r4_pairs_and_lists_6_3z00(BgL_objz00_2751,
					BgL_auxz00_3864);
			}
		}

	}



/* remq */
	BGL_EXPORTED_DEF obj_t bgl_remq(obj_t BgL_xz00_87, obj_t BgL_yz00_88)
	{
		{	/* Ieee/pairlist.scm 751 */
		bgl_remq:
			if (NULLP(BgL_yz00_88))
				{	/* Ieee/pairlist.scm 753 */
					return BgL_yz00_88;
				}
			else
				{	/* Ieee/pairlist.scm 753 */
					if ((BgL_xz00_87 == CAR(BgL_yz00_88)))
						{
							obj_t BgL_yz00_3877;

							BgL_yz00_3877 = CDR(BgL_yz00_88);
							BgL_yz00_88 = BgL_yz00_3877;
							goto bgl_remq;
						}
					else
						{	/* Ieee/pairlist.scm 754 */
							return
								MAKE_YOUNG_PAIR(CAR(BgL_yz00_88),
								bgl_remq(BgL_xz00_87, CDR(BgL_yz00_88)));
						}
				}
		}

	}



/* &remq */
	obj_t BGl_z62remqz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2753,
		obj_t BgL_xz00_2754, obj_t BgL_yz00_2755)
	{
		{	/* Ieee/pairlist.scm 751 */
			{	/* Ieee/pairlist.scm 753 */
				obj_t BgL_auxz00_3883;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_yz00_2755))
					{	/* Ieee/pairlist.scm 753 */
						BgL_auxz00_3883 = BgL_yz00_2755;
					}
				else
					{
						obj_t BgL_auxz00_3886;

						BgL_auxz00_3886 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(30160L),
							BGl_string2073z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2054z00zz__r4_pairs_and_lists_6_3z00, BgL_yz00_2755);
						FAILURE(BgL_auxz00_3886, BFALSE, BFALSE);
					}
				return bgl_remq(BgL_xz00_2754, BgL_auxz00_3883);
			}
		}

	}



/* remq! */
	BGL_EXPORTED_DEF obj_t bgl_remq_bang(obj_t BgL_xz00_89, obj_t BgL_yz00_90)
	{
		{	/* Ieee/pairlist.scm 760 */
		bgl_remq_bang:
			if (NULLP(BgL_yz00_90))
				{	/* Ieee/pairlist.scm 762 */
					return BgL_yz00_90;
				}
			else
				{	/* Ieee/pairlist.scm 762 */
					if ((BgL_xz00_89 == CAR(BgL_yz00_90)))
						{
							obj_t BgL_yz00_3896;

							BgL_yz00_3896 = CDR(BgL_yz00_90);
							BgL_yz00_90 = BgL_yz00_3896;
							goto bgl_remq_bang;
						}
					else
						{
							obj_t BgL_prevz00_1091;

							BgL_prevz00_1091 = BgL_yz00_90;
						BgL_zc3z04anonymousza31459ze3z87_1092:
							if (NULLP(CDR(((obj_t) BgL_prevz00_1091))))
								{	/* Ieee/pairlist.scm 765 */
									return BgL_yz00_90;
								}
							else
								{	/* Ieee/pairlist.scm 767 */
									bool_t BgL_test2366z00_3902;

									{	/* Ieee/pairlist.scm 767 */
										obj_t BgL_tmpz00_3903;

										{	/* Ieee/pairlist.scm 285 */
											obj_t BgL_pairz00_2137;

											BgL_pairz00_2137 = CDR(((obj_t) BgL_prevz00_1091));
											BgL_tmpz00_3903 = CAR(BgL_pairz00_2137);
										}
										BgL_test2366z00_3902 = (BgL_tmpz00_3903 == BgL_xz00_89);
									}
									if (BgL_test2366z00_3902)
										{	/* Ieee/pairlist.scm 767 */
											{	/* Ieee/pairlist.scm 768 */
												obj_t BgL_arg1464z00_1097;

												{	/* Ieee/pairlist.scm 297 */
													obj_t BgL_pairz00_2141;

													BgL_pairz00_2141 = CDR(((obj_t) BgL_prevz00_1091));
													BgL_arg1464z00_1097 = CDR(BgL_pairz00_2141);
												}
												{	/* Ieee/pairlist.scm 454 */
													obj_t BgL_tmpz00_3911;

													BgL_tmpz00_3911 = ((obj_t) BgL_prevz00_1091);
													SET_CDR(BgL_tmpz00_3911, BgL_arg1464z00_1097);
												}
											}
											{

												goto BgL_zc3z04anonymousza31459ze3z87_1092;
											}
										}
									else
										{	/* Ieee/pairlist.scm 770 */
											obj_t BgL_arg1465z00_1098;

											BgL_arg1465z00_1098 = CDR(((obj_t) BgL_prevz00_1091));
											{
												obj_t BgL_prevz00_3916;

												BgL_prevz00_3916 = BgL_arg1465z00_1098;
												BgL_prevz00_1091 = BgL_prevz00_3916;
												goto BgL_zc3z04anonymousza31459ze3z87_1092;
											}
										}
								}
						}
				}
		}

	}



/* &remq! */
	obj_t BGl_z62remqz12z70zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2756,
		obj_t BgL_xz00_2757, obj_t BgL_yz00_2758)
	{
		{	/* Ieee/pairlist.scm 760 */
			{	/* Ieee/pairlist.scm 762 */
				obj_t BgL_auxz00_3917;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_yz00_2758))
					{	/* Ieee/pairlist.scm 762 */
						BgL_auxz00_3917 = BgL_yz00_2758;
					}
				else
					{
						obj_t BgL_auxz00_3920;

						BgL_auxz00_3920 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(30520L),
							BGl_string2074z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2054z00zz__r4_pairs_and_lists_6_3z00, BgL_yz00_2758);
						FAILURE(BgL_auxz00_3920, BFALSE, BFALSE);
					}
				return bgl_remq_bang(BgL_xz00_2757, BgL_auxz00_3917);
			}
		}

	}



/* _delete */
	obj_t BGl__deletez00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_env1106z00_95,
		obj_t BgL_opt1105z00_94)
	{
		{	/* Ieee/pairlist.scm 775 */
			{	/* Ieee/pairlist.scm 775 */
				obj_t BgL_g1107z00_1103;
				obj_t BgL_g1108z00_1104;

				BgL_g1107z00_1103 = VECTOR_REF(BgL_opt1105z00_94, 0L);
				BgL_g1108z00_1104 = VECTOR_REF(BgL_opt1105z00_94, 1L);
				switch (VECTOR_LENGTH(BgL_opt1105z00_94))
					{
					case 2L:

						{	/* Ieee/pairlist.scm 775 */

							{	/* Ieee/pairlist.scm 775 */
								obj_t BgL_auxz00_3927;

								if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
									(BgL_g1108z00_1104))
									{	/* Ieee/pairlist.scm 775 */
										BgL_auxz00_3927 = BgL_g1108z00_1104;
									}
								else
									{
										obj_t BgL_auxz00_3930;

										BgL_auxz00_3930 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00,
											BINT(31076L),
											BGl_string2075z00zz__r4_pairs_and_lists_6_3z00,
											BGl_string2054z00zz__r4_pairs_and_lists_6_3z00,
											BgL_g1108z00_1104);
										FAILURE(BgL_auxz00_3930, BFALSE, BFALSE);
									}
								return
									BGl_deletez00zz__r4_pairs_and_lists_6_3z00(BgL_g1107z00_1103,
									BgL_auxz00_3927,
									BGl_equalzf3zd2envz21zz__r4_equivalence_6_2z00);
							}
						}
						break;
					case 3L:

						{	/* Ieee/pairlist.scm 775 */
							obj_t BgL_eqz00_1108;

							BgL_eqz00_1108 = VECTOR_REF(BgL_opt1105z00_94, 2L);
							{	/* Ieee/pairlist.scm 775 */

								{	/* Ieee/pairlist.scm 775 */
									obj_t BgL_auxz00_3936;

									if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
										(BgL_g1108z00_1104))
										{	/* Ieee/pairlist.scm 775 */
											BgL_auxz00_3936 = BgL_g1108z00_1104;
										}
									else
										{
											obj_t BgL_auxz00_3939;

											BgL_auxz00_3939 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00,
												BINT(31076L),
												BGl_string2075z00zz__r4_pairs_and_lists_6_3z00,
												BGl_string2054z00zz__r4_pairs_and_lists_6_3z00,
												BgL_g1108z00_1104);
											FAILURE(BgL_auxz00_3939, BFALSE, BFALSE);
										}
									return
										BGl_deletez00zz__r4_pairs_and_lists_6_3z00
										(BgL_g1107z00_1103, BgL_auxz00_3936, BgL_eqz00_1108);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* delete */
	BGL_EXPORTED_DEF obj_t BGl_deletez00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_xz00_91, obj_t BgL_yz00_92, obj_t BgL_eqz00_93)
	{
		{	/* Ieee/pairlist.scm 775 */
			return
				BGl_loopze70ze7zz__r4_pairs_and_lists_6_3z00(BgL_eqz00_93, BgL_xz00_91,
				BgL_yz00_92);
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zz__r4_pairs_and_lists_6_3z00(obj_t BgL_eqz00_2805,
		obj_t BgL_xz00_1110, obj_t BgL_yz00_1111)
	{
		{	/* Ieee/pairlist.scm 776 */
		BGl_loopze70ze7zz__r4_pairs_and_lists_6_3z00:
			if (NULLP(BgL_yz00_1111))
				{	/* Ieee/pairlist.scm 779 */
					return BgL_yz00_1111;
				}
			else
				{	/* Ieee/pairlist.scm 780 */
					bool_t BgL_test2371z00_3949;

					{	/* Ieee/pairlist.scm 780 */
						obj_t BgL_arg1479z00_1120;

						BgL_arg1479z00_1120 = CAR(((obj_t) BgL_yz00_1111));
						BgL_test2371z00_3949 =
							CBOOL(BGL_PROCEDURE_CALL2(BgL_eqz00_2805, BgL_xz00_1110,
								BgL_arg1479z00_1120));
					}
					if (BgL_test2371z00_3949)
						{	/* Ieee/pairlist.scm 780 */
							obj_t BgL_arg1474z00_1116;

							BgL_arg1474z00_1116 = CDR(((obj_t) BgL_yz00_1111));
							{
								obj_t BgL_yz00_3960;

								BgL_yz00_3960 = BgL_arg1474z00_1116;
								BgL_yz00_1111 = BgL_yz00_3960;
								goto BGl_loopze70ze7zz__r4_pairs_and_lists_6_3z00;
							}
						}
					else
						{	/* Ieee/pairlist.scm 781 */
							obj_t BgL_arg1476z00_1117;
							obj_t BgL_arg1477z00_1118;

							BgL_arg1476z00_1117 = CAR(((obj_t) BgL_yz00_1111));
							{	/* Ieee/pairlist.scm 781 */
								obj_t BgL_arg1478z00_1119;

								BgL_arg1478z00_1119 = CDR(((obj_t) BgL_yz00_1111));
								BgL_arg1477z00_1118 =
									BGl_loopze70ze7zz__r4_pairs_and_lists_6_3z00(BgL_eqz00_2805,
									BgL_xz00_1110, BgL_arg1478z00_1119);
							}
							return MAKE_YOUNG_PAIR(BgL_arg1476z00_1117, BgL_arg1477z00_1118);
						}
				}
		}

	}



/* _delete! */
	obj_t BGl__deletez12z12zz__r4_pairs_and_lists_6_3z00(obj_t BgL_env1112z00_100,
		obj_t BgL_opt1111z00_99)
	{
		{	/* Ieee/pairlist.scm 786 */
			{	/* Ieee/pairlist.scm 786 */
				obj_t BgL_g1113z00_1122;
				obj_t BgL_g1114z00_1123;

				BgL_g1113z00_1122 = VECTOR_REF(BgL_opt1111z00_99, 0L);
				BgL_g1114z00_1123 = VECTOR_REF(BgL_opt1111z00_99, 1L);
				switch (VECTOR_LENGTH(BgL_opt1111z00_99))
					{
					case 2L:

						{	/* Ieee/pairlist.scm 786 */

							{	/* Ieee/pairlist.scm 786 */
								obj_t BgL_auxz00_3969;

								if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
									(BgL_g1114z00_1123))
									{	/* Ieee/pairlist.scm 786 */
										BgL_auxz00_3969 = BgL_g1114z00_1123;
									}
								else
									{
										obj_t BgL_auxz00_3972;

										BgL_auxz00_3972 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00,
											BINT(31485L),
											BGl_string2076z00zz__r4_pairs_and_lists_6_3z00,
											BGl_string2054z00zz__r4_pairs_and_lists_6_3z00,
											BgL_g1114z00_1123);
										FAILURE(BgL_auxz00_3972, BFALSE, BFALSE);
									}
								return
									BGl_deletez12z12zz__r4_pairs_and_lists_6_3z00
									(BgL_g1113z00_1122, BgL_auxz00_3969,
									BGl_equalzf3zd2envz21zz__r4_equivalence_6_2z00);
							}
						}
						break;
					case 3L:

						{	/* Ieee/pairlist.scm 786 */
							obj_t BgL_eqz00_1127;

							BgL_eqz00_1127 = VECTOR_REF(BgL_opt1111z00_99, 2L);
							{	/* Ieee/pairlist.scm 786 */

								{	/* Ieee/pairlist.scm 786 */
									obj_t BgL_auxz00_3978;

									if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
										(BgL_g1114z00_1123))
										{	/* Ieee/pairlist.scm 786 */
											BgL_auxz00_3978 = BgL_g1114z00_1123;
										}
									else
										{
											obj_t BgL_auxz00_3981;

											BgL_auxz00_3981 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00,
												BINT(31485L),
												BGl_string2076z00zz__r4_pairs_and_lists_6_3z00,
												BGl_string2054z00zz__r4_pairs_and_lists_6_3z00,
												BgL_g1114z00_1123);
											FAILURE(BgL_auxz00_3981, BFALSE, BFALSE);
										}
									return
										BGl_deletez12z12zz__r4_pairs_and_lists_6_3z00
										(BgL_g1113z00_1122, BgL_auxz00_3978, BgL_eqz00_1127);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* delete! */
	BGL_EXPORTED_DEF obj_t BGl_deletez12z12zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_xz00_96, obj_t BgL_yz00_97, obj_t BgL_eqz00_98)
	{
		{	/* Ieee/pairlist.scm 786 */
			{
				obj_t BgL_xz00_1129;
				obj_t BgL_yz00_1130;

				BgL_xz00_1129 = BgL_xz00_96;
				BgL_yz00_1130 = BgL_yz00_97;
			BgL_zc3z04anonymousza31480ze3z87_1131:
				if (NULLP(BgL_yz00_1130))
					{	/* Ieee/pairlist.scm 790 */
						return BgL_yz00_1130;
					}
				else
					{	/* Ieee/pairlist.scm 791 */
						bool_t BgL_test2375z00_3990;

						{	/* Ieee/pairlist.scm 791 */
							obj_t BgL_arg1497z00_1148;

							BgL_arg1497z00_1148 = CAR(((obj_t) BgL_yz00_1130));
							BgL_test2375z00_3990 =
								CBOOL(BGL_PROCEDURE_CALL2(BgL_eqz00_98, BgL_xz00_1129,
									BgL_arg1497z00_1148));
						}
						if (BgL_test2375z00_3990)
							{	/* Ieee/pairlist.scm 791 */
								obj_t BgL_arg1484z00_1135;

								BgL_arg1484z00_1135 = CDR(((obj_t) BgL_yz00_1130));
								{
									obj_t BgL_yz00_4001;

									BgL_yz00_4001 = BgL_arg1484z00_1135;
									BgL_yz00_1130 = BgL_yz00_4001;
									goto BgL_zc3z04anonymousza31480ze3z87_1131;
								}
							}
						else
							{
								obj_t BgL_prevz00_1137;

								BgL_prevz00_1137 = BgL_yz00_1130;
							BgL_zc3z04anonymousza31485ze3z87_1138:
								if (NULLP(CDR(((obj_t) BgL_prevz00_1137))))
									{	/* Ieee/pairlist.scm 793 */
										return BgL_yz00_1130;
									}
								else
									{	/* Ieee/pairlist.scm 795 */
										bool_t BgL_test2377z00_4006;

										{	/* Ieee/pairlist.scm 795 */
											obj_t BgL_arg1494z00_1145;

											{	/* Ieee/pairlist.scm 285 */
												obj_t BgL_pairz00_2154;

												BgL_pairz00_2154 = CDR(((obj_t) BgL_prevz00_1137));
												BgL_arg1494z00_1145 = CAR(BgL_pairz00_2154);
											}
											BgL_test2377z00_4006 =
												CBOOL(BGL_PROCEDURE_CALL2(BgL_eqz00_98,
													BgL_arg1494z00_1145, BgL_xz00_1129));
										}
										if (BgL_test2377z00_4006)
											{	/* Ieee/pairlist.scm 795 */
												{	/* Ieee/pairlist.scm 796 */
													obj_t BgL_arg1490z00_1143;

													{	/* Ieee/pairlist.scm 297 */
														obj_t BgL_pairz00_2158;

														BgL_pairz00_2158 = CDR(((obj_t) BgL_prevz00_1137));
														BgL_arg1490z00_1143 = CDR(BgL_pairz00_2158);
													}
													{	/* Ieee/pairlist.scm 454 */
														obj_t BgL_tmpz00_4019;

														BgL_tmpz00_4019 = ((obj_t) BgL_prevz00_1137);
														SET_CDR(BgL_tmpz00_4019, BgL_arg1490z00_1143);
													}
												}
												{

													goto BgL_zc3z04anonymousza31485ze3z87_1138;
												}
											}
										else
											{	/* Ieee/pairlist.scm 798 */
												obj_t BgL_arg1492z00_1144;

												BgL_arg1492z00_1144 = CDR(((obj_t) BgL_prevz00_1137));
												{
													obj_t BgL_prevz00_4024;

													BgL_prevz00_4024 = BgL_arg1492z00_1144;
													BgL_prevz00_1137 = BgL_prevz00_4024;
													goto BgL_zc3z04anonymousza31485ze3z87_1138;
												}
											}
									}
							}
					}
			}
		}

	}



/* cons* */
	BGL_EXPORTED_DEF obj_t BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_xz00_101, obj_t BgL_yz00_102)
	{
		{	/* Ieee/pairlist.scm 803 */
			if (NULLP(BgL_yz00_102))
				{	/* Ieee/pairlist.scm 808 */
					return BgL_xz00_101;
				}
			else
				{	/* Ieee/pairlist.scm 808 */
					return
						MAKE_YOUNG_PAIR(BgL_xz00_101,
						BGl_consza21ze70z45zz__r4_pairs_and_lists_6_3z00(BgL_yz00_102));
				}
		}

	}



/* cons*1~0 */
	obj_t BGl_consza21ze70z45zz__r4_pairs_and_lists_6_3z00(obj_t BgL_xz00_1150)
	{
		{	/* Ieee/pairlist.scm 804 */
			if (NULLP(CDR(((obj_t) BgL_xz00_1150))))
				{	/* Ieee/pairlist.scm 804 */
					return CAR(((obj_t) BgL_xz00_1150));
				}
			else
				{	/* Ieee/pairlist.scm 807 */
					obj_t BgL_arg1502z00_1156;
					obj_t BgL_arg1503z00_1157;

					BgL_arg1502z00_1156 = CAR(((obj_t) BgL_xz00_1150));
					{	/* Ieee/pairlist.scm 807 */
						obj_t BgL_arg1504z00_1158;

						BgL_arg1504z00_1158 = CDR(((obj_t) BgL_xz00_1150));
						BgL_arg1503z00_1157 =
							BGl_consza21ze70z45zz__r4_pairs_and_lists_6_3z00
							(BgL_arg1504z00_1158);
					}
					return MAKE_YOUNG_PAIR(BgL_arg1502z00_1156, BgL_arg1503z00_1157);
				}
		}

	}



/* &cons* */
	obj_t BGl_z62consza2zc0zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2762,
		obj_t BgL_xz00_2763, obj_t BgL_yz00_2764)
	{
		{	/* Ieee/pairlist.scm 803 */
			return
				BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(BgL_xz00_2763,
				BgL_yz00_2764);
		}

	}



/* reverse! */
	BGL_EXPORTED_DEF obj_t bgl_reverse_bang(obj_t BgL_lz00_103)
	{
		{	/* Ieee/pairlist.scm 815 */
			if (NULLP(BgL_lz00_103))
				{	/* Ieee/pairlist.scm 816 */
					return BgL_lz00_103;
				}
			else
				{
					obj_t BgL_lz00_2182;
					obj_t BgL_rz00_2183;

					BgL_lz00_2182 = BgL_lz00_103;
					BgL_rz00_2183 = BNIL;
				BgL_nrz00_2181:
					if (NULLP(CDR(((obj_t) BgL_lz00_2182))))
						{	/* Ieee/pairlist.scm 819 */
							{	/* Ieee/pairlist.scm 454 */
								obj_t BgL_tmpz00_4048;

								BgL_tmpz00_4048 = ((obj_t) BgL_lz00_2182);
								SET_CDR(BgL_tmpz00_4048, BgL_rz00_2183);
							}
							return BgL_lz00_2182;
						}
					else
						{	/* Ieee/pairlist.scm 823 */
							obj_t BgL_cdrlz00_2190;

							BgL_cdrlz00_2190 = CDR(((obj_t) BgL_lz00_2182));
							{	/* Ieee/pairlist.scm 825 */
								obj_t BgL_arg1510z00_2191;

								{	/* Ieee/pairlist.scm 454 */
									obj_t BgL_tmpz00_4053;

									BgL_tmpz00_4053 = ((obj_t) BgL_lz00_2182);
									SET_CDR(BgL_tmpz00_4053, BgL_rz00_2183);
								}
								BgL_arg1510z00_2191 = BgL_lz00_2182;
								{
									obj_t BgL_rz00_4057;
									obj_t BgL_lz00_4056;

									BgL_lz00_4056 = BgL_cdrlz00_2190;
									BgL_rz00_4057 = BgL_arg1510z00_2191;
									BgL_rz00_2183 = BgL_rz00_4057;
									BgL_lz00_2182 = BgL_lz00_4056;
									goto BgL_nrz00_2181;
								}
							}
						}
				}
		}

	}



/* &reverse! */
	obj_t BGl_z62reversez12z70zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2765,
		obj_t BgL_lz00_2766)
	{
		{	/* Ieee/pairlist.scm 815 */
			{	/* Ieee/pairlist.scm 816 */
				obj_t BgL_auxz00_4058;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_lz00_2766))
					{	/* Ieee/pairlist.scm 816 */
						BgL_auxz00_4058 = BgL_lz00_2766;
					}
				else
					{
						obj_t BgL_auxz00_4061;

						BgL_auxz00_4061 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(32477L),
							BGl_string2077z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2054z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_2766);
						FAILURE(BgL_auxz00_4061, BFALSE, BFALSE);
					}
				return bgl_reverse_bang(BgL_auxz00_4058);
			}
		}

	}



/* every */
	BGL_EXPORTED_DEF obj_t BGl_everyz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_predz00_104, obj_t BgL_lz00_105)
	{
		{	/* Ieee/pairlist.scm 831 */
			if (NULLP(BgL_lz00_105))
				{	/* Ieee/pairlist.scm 833 */
					return BTRUE;
				}
			else
				{	/* Ieee/pairlist.scm 833 */
					if (NULLP(CDR(((obj_t) BgL_lz00_105))))
						{	/* Ieee/pairlist.scm 836 */
							obj_t BgL_g1025z00_1175;

							BgL_g1025z00_1175 = CAR(((obj_t) BgL_lz00_105));
							{
								obj_t BgL_lz00_1177;

								{	/* Ieee/pairlist.scm 836 */
									bool_t BgL_tmpz00_4074;

									BgL_lz00_1177 = BgL_g1025z00_1175;
								BgL_zc3z04anonymousza31515ze3z87_1178:
									{	/* Ieee/pairlist.scm 837 */
										bool_t BgL__ortest_1026z00_1179;

										BgL__ortest_1026z00_1179 = NULLP(BgL_lz00_1177);
										if (BgL__ortest_1026z00_1179)
											{	/* Ieee/pairlist.scm 837 */
												BgL_tmpz00_4074 = BgL__ortest_1026z00_1179;
											}
										else
											{	/* Ieee/pairlist.scm 838 */
												obj_t BgL__andtest_1027z00_1180;

												{	/* Ieee/pairlist.scm 838 */
													obj_t BgL_arg1517z00_1182;

													BgL_arg1517z00_1182 = CAR(((obj_t) BgL_lz00_1177));
													BgL__andtest_1027z00_1180 =
														BGL_PROCEDURE_CALL1(BgL_predz00_104,
														BgL_arg1517z00_1182);
												}
												if (CBOOL(BgL__andtest_1027z00_1180))
													{	/* Ieee/pairlist.scm 838 */
														obj_t BgL_arg1516z00_1181;

														BgL_arg1516z00_1181 = CDR(((obj_t) BgL_lz00_1177));
														{
															obj_t BgL_lz00_4087;

															BgL_lz00_4087 = BgL_arg1516z00_1181;
															BgL_lz00_1177 = BgL_lz00_4087;
															goto BgL_zc3z04anonymousza31515ze3z87_1178;
														}
													}
												else
													{	/* Ieee/pairlist.scm 838 */
														BgL_tmpz00_4074 = ((bool_t) 0);
													}
											}
									}
									return BBOOL(BgL_tmpz00_4074);
								}
							}
						}
					else
						{
							obj_t BgL_lz00_1185;

							{	/* Ieee/pairlist.scm 840 */
								bool_t BgL_tmpz00_4089;

								BgL_lz00_1185 = BgL_lz00_105;
							BgL_zc3z04anonymousza31518ze3z87_1186:
								{	/* Ieee/pairlist.scm 841 */
									bool_t BgL__ortest_1028z00_1187;

									BgL__ortest_1028z00_1187 =
										NULLP(CAR(((obj_t) BgL_lz00_1185)));
									if (BgL__ortest_1028z00_1187)
										{	/* Ieee/pairlist.scm 841 */
											BgL_tmpz00_4089 = BgL__ortest_1028z00_1187;
										}
									else
										{	/* Ieee/pairlist.scm 842 */
											obj_t BgL__andtest_1029z00_1188;

											{	/* Ieee/pairlist.scm 842 */
												obj_t BgL_auxz00_4094;

												if (NULLP(BgL_lz00_1185))
													{	/* Ieee/pairlist.scm 842 */
														BgL_auxz00_4094 = BNIL;
													}
												else
													{	/* Ieee/pairlist.scm 842 */
														obj_t BgL_head1074z00_1208;

														{	/* Ieee/pairlist.scm 842 */
															obj_t BgL_arg1539z00_1220;

															{	/* Ieee/pairlist.scm 842 */
																obj_t BgL_pairz00_2202;

																BgL_pairz00_2202 = CAR(((obj_t) BgL_lz00_1185));
																BgL_arg1539z00_1220 = CAR(BgL_pairz00_2202);
															}
															BgL_head1074z00_1208 =
																MAKE_YOUNG_PAIR(BgL_arg1539z00_1220, BNIL);
														}
														{	/* Ieee/pairlist.scm 842 */
															obj_t BgL_g1077z00_1209;

															BgL_g1077z00_1209 = CDR(((obj_t) BgL_lz00_1185));
															{
																obj_t BgL_l1072z00_2223;
																obj_t BgL_tail1075z00_2224;

																BgL_l1072z00_2223 = BgL_g1077z00_1209;
																BgL_tail1075z00_2224 = BgL_head1074z00_1208;
															BgL_zc3z04anonymousza31531ze3z87_2222:
																if (NULLP(BgL_l1072z00_2223))
																	{	/* Ieee/pairlist.scm 842 */
																		BgL_auxz00_4094 = BgL_head1074z00_1208;
																	}
																else
																	{	/* Ieee/pairlist.scm 842 */
																		obj_t BgL_newtail1076z00_2231;

																		{	/* Ieee/pairlist.scm 842 */
																			obj_t BgL_arg1536z00_2232;

																			{	/* Ieee/pairlist.scm 842 */
																				obj_t BgL_pairz00_2236;

																				BgL_pairz00_2236 =
																					CAR(((obj_t) BgL_l1072z00_2223));
																				BgL_arg1536z00_2232 =
																					CAR(BgL_pairz00_2236);
																			}
																			BgL_newtail1076z00_2231 =
																				MAKE_YOUNG_PAIR(BgL_arg1536z00_2232,
																				BNIL);
																		}
																		SET_CDR(BgL_tail1075z00_2224,
																			BgL_newtail1076z00_2231);
																		{	/* Ieee/pairlist.scm 842 */
																			obj_t BgL_arg1535z00_2234;

																			BgL_arg1535z00_2234 =
																				CDR(((obj_t) BgL_l1072z00_2223));
																			{
																				obj_t BgL_tail1075z00_4113;
																				obj_t BgL_l1072z00_4112;

																				BgL_l1072z00_4112 = BgL_arg1535z00_2234;
																				BgL_tail1075z00_4113 =
																					BgL_newtail1076z00_2231;
																				BgL_tail1075z00_2224 =
																					BgL_tail1075z00_4113;
																				BgL_l1072z00_2223 = BgL_l1072z00_4112;
																				goto
																					BgL_zc3z04anonymousza31531ze3z87_2222;
																			}
																		}
																	}
															}
														}
													}
												BgL__andtest_1029z00_1188 =
													apply(BgL_predz00_104, BgL_auxz00_4094);
											}
											if (CBOOL(BgL__andtest_1029z00_1188))
												{	/* Ieee/pairlist.scm 842 */
													obj_t BgL_arg1521z00_1189;

													if (NULLP(BgL_lz00_1185))
														{	/* Ieee/pairlist.scm 842 */
															BgL_arg1521z00_1189 = BNIL;
														}
													else
														{	/* Ieee/pairlist.scm 842 */
															obj_t BgL_head1080z00_1192;

															{	/* Ieee/pairlist.scm 842 */
																obj_t BgL_arg1528z00_1204;

																{	/* Ieee/pairlist.scm 842 */
																	obj_t BgL_pairz00_2240;

																	BgL_pairz00_2240 =
																		CAR(((obj_t) BgL_lz00_1185));
																	BgL_arg1528z00_1204 = CDR(BgL_pairz00_2240);
																}
																BgL_head1080z00_1192 =
																	MAKE_YOUNG_PAIR(BgL_arg1528z00_1204, BNIL);
															}
															{	/* Ieee/pairlist.scm 842 */
																obj_t BgL_g1083z00_1193;

																BgL_g1083z00_1193 =
																	CDR(((obj_t) BgL_lz00_1185));
																{
																	obj_t BgL_l1078z00_2261;
																	obj_t BgL_tail1081z00_2262;

																	BgL_l1078z00_2261 = BgL_g1083z00_1193;
																	BgL_tail1081z00_2262 = BgL_head1080z00_1192;
																BgL_zc3z04anonymousza31523ze3z87_2260:
																	if (NULLP(BgL_l1078z00_2261))
																		{	/* Ieee/pairlist.scm 842 */
																			BgL_arg1521z00_1189 =
																				BgL_head1080z00_1192;
																		}
																	else
																		{	/* Ieee/pairlist.scm 842 */
																			obj_t BgL_newtail1082z00_2269;

																			{	/* Ieee/pairlist.scm 842 */
																				obj_t BgL_arg1526z00_2270;

																				{	/* Ieee/pairlist.scm 842 */
																					obj_t BgL_pairz00_2274;

																					BgL_pairz00_2274 =
																						CAR(((obj_t) BgL_l1078z00_2261));
																					BgL_arg1526z00_2270 =
																						CDR(BgL_pairz00_2274);
																				}
																				BgL_newtail1082z00_2269 =
																					MAKE_YOUNG_PAIR(BgL_arg1526z00_2270,
																					BNIL);
																			}
																			SET_CDR(BgL_tail1081z00_2262,
																				BgL_newtail1082z00_2269);
																			{	/* Ieee/pairlist.scm 842 */
																				obj_t BgL_arg1525z00_2272;

																				BgL_arg1525z00_2272 =
																					CDR(((obj_t) BgL_l1078z00_2261));
																				{
																					obj_t BgL_tail1081z00_4135;
																					obj_t BgL_l1078z00_4134;

																					BgL_l1078z00_4134 =
																						BgL_arg1525z00_2272;
																					BgL_tail1081z00_4135 =
																						BgL_newtail1082z00_2269;
																					BgL_tail1081z00_2262 =
																						BgL_tail1081z00_4135;
																					BgL_l1078z00_2261 = BgL_l1078z00_4134;
																					goto
																						BgL_zc3z04anonymousza31523ze3z87_2260;
																				}
																			}
																		}
																}
															}
														}
													{
														obj_t BgL_lz00_4136;

														BgL_lz00_4136 = BgL_arg1521z00_1189;
														BgL_lz00_1185 = BgL_lz00_4136;
														goto BgL_zc3z04anonymousza31518ze3z87_1186;
													}
												}
											else
												{	/* Ieee/pairlist.scm 842 */
													BgL_tmpz00_4089 = ((bool_t) 0);
												}
										}
								}
								return BBOOL(BgL_tmpz00_4089);
							}
						}
				}
		}

	}



/* &every */
	obj_t BGl_z62everyz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2767,
		obj_t BgL_predz00_2768, obj_t BgL_lz00_2769)
	{
		{	/* Ieee/pairlist.scm 831 */
			{	/* Ieee/pairlist.scm 833 */
				obj_t BgL_auxz00_4138;

				if (PROCEDUREP(BgL_predz00_2768))
					{	/* Ieee/pairlist.scm 833 */
						BgL_auxz00_4138 = BgL_predz00_2768;
					}
				else
					{
						obj_t BgL_auxz00_4141;

						BgL_auxz00_4141 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(32978L),
							BGl_string2078z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2079z00zz__r4_pairs_and_lists_6_3z00, BgL_predz00_2768);
						FAILURE(BgL_auxz00_4141, BFALSE, BFALSE);
					}
				return
					BGl_everyz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4138,
					BgL_lz00_2769);
			}
		}

	}



/* any */
	BGL_EXPORTED_DEF obj_t BGl_anyz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_predz00_106, obj_t BgL_lz00_107)
	{
		{	/* Ieee/pairlist.scm 847 */
			if (NULLP(BgL_lz00_107))
				{	/* Ieee/pairlist.scm 849 */
					return BFALSE;
				}
			else
				{	/* Ieee/pairlist.scm 849 */
					if (NULLP(CDR(((obj_t) BgL_lz00_107))))
						{	/* Ieee/pairlist.scm 852 */
							obj_t BgL_g1030z00_1228;

							BgL_g1030z00_1228 = CAR(((obj_t) BgL_lz00_107));
							{
								obj_t BgL_lz00_1230;

								BgL_lz00_1230 = BgL_g1030z00_1228;
							BgL_zc3z04anonymousza31548ze3z87_1231:
								if (PAIRP(BgL_lz00_1230))
									{	/* Ieee/pairlist.scm 854 */
										obj_t BgL__ortest_1032z00_1233;

										{	/* Ieee/pairlist.scm 854 */
											obj_t BgL_arg1552z00_1235;

											BgL_arg1552z00_1235 = CAR(BgL_lz00_1230);
											BgL__ortest_1032z00_1233 =
												BGL_PROCEDURE_CALL1(BgL_predz00_106,
												BgL_arg1552z00_1235);
										}
										if (CBOOL(BgL__ortest_1032z00_1233))
											{	/* Ieee/pairlist.scm 854 */
												return BgL__ortest_1032z00_1233;
											}
										else
											{
												obj_t BgL_lz00_4163;

												BgL_lz00_4163 = CDR(BgL_lz00_1230);
												BgL_lz00_1230 = BgL_lz00_4163;
												goto BgL_zc3z04anonymousza31548ze3z87_1231;
											}
									}
								else
									{	/* Ieee/pairlist.scm 853 */
										return BFALSE;
									}
							}
						}
					else
						{
							obj_t BgL_lz00_1238;

							BgL_lz00_1238 = BgL_lz00_107;
						BgL_zc3z04anonymousza31553ze3z87_1239:
							{	/* Ieee/pairlist.scm 857 */
								bool_t BgL_test2398z00_4165;

								{	/* Ieee/pairlist.scm 229 */
									obj_t BgL_tmpz00_4166;

									BgL_tmpz00_4166 = CAR(((obj_t) BgL_lz00_1238));
									BgL_test2398z00_4165 = PAIRP(BgL_tmpz00_4166);
								}
								if (BgL_test2398z00_4165)
									{	/* Ieee/pairlist.scm 858 */
										obj_t BgL__ortest_1034z00_1241;

										{	/* Ieee/pairlist.scm 858 */
											obj_t BgL_auxz00_4170;

											if (NULLP(BgL_lz00_1238))
												{	/* Ieee/pairlist.scm 858 */
													BgL_auxz00_4170 = BNIL;
												}
											else
												{	/* Ieee/pairlist.scm 858 */
													obj_t BgL_head1086z00_1261;

													{	/* Ieee/pairlist.scm 858 */
														obj_t BgL_arg1576z00_1273;

														{	/* Ieee/pairlist.scm 858 */
															obj_t BgL_pairz00_2283;

															BgL_pairz00_2283 = CAR(((obj_t) BgL_lz00_1238));
															BgL_arg1576z00_1273 = CAR(BgL_pairz00_2283);
														}
														BgL_head1086z00_1261 =
															MAKE_YOUNG_PAIR(BgL_arg1576z00_1273, BNIL);
													}
													{	/* Ieee/pairlist.scm 858 */
														obj_t BgL_g1089z00_1262;

														BgL_g1089z00_1262 = CDR(((obj_t) BgL_lz00_1238));
														{
															obj_t BgL_l1084z00_2304;
															obj_t BgL_tail1087z00_2305;

															BgL_l1084z00_2304 = BgL_g1089z00_1262;
															BgL_tail1087z00_2305 = BgL_head1086z00_1261;
														BgL_zc3z04anonymousza31566ze3z87_2303:
															if (NULLP(BgL_l1084z00_2304))
																{	/* Ieee/pairlist.scm 858 */
																	BgL_auxz00_4170 = BgL_head1086z00_1261;
																}
															else
																{	/* Ieee/pairlist.scm 858 */
																	obj_t BgL_newtail1088z00_2312;

																	{	/* Ieee/pairlist.scm 858 */
																		obj_t BgL_arg1573z00_2313;

																		{	/* Ieee/pairlist.scm 858 */
																			obj_t BgL_pairz00_2317;

																			BgL_pairz00_2317 =
																				CAR(((obj_t) BgL_l1084z00_2304));
																			BgL_arg1573z00_2313 =
																				CAR(BgL_pairz00_2317);
																		}
																		BgL_newtail1088z00_2312 =
																			MAKE_YOUNG_PAIR(BgL_arg1573z00_2313,
																			BNIL);
																	}
																	SET_CDR(BgL_tail1087z00_2305,
																		BgL_newtail1088z00_2312);
																	{	/* Ieee/pairlist.scm 858 */
																		obj_t BgL_arg1571z00_2315;

																		BgL_arg1571z00_2315 =
																			CDR(((obj_t) BgL_l1084z00_2304));
																		{
																			obj_t BgL_tail1087z00_4189;
																			obj_t BgL_l1084z00_4188;

																			BgL_l1084z00_4188 = BgL_arg1571z00_2315;
																			BgL_tail1087z00_4189 =
																				BgL_newtail1088z00_2312;
																			BgL_tail1087z00_2305 =
																				BgL_tail1087z00_4189;
																			BgL_l1084z00_2304 = BgL_l1084z00_4188;
																			goto
																				BgL_zc3z04anonymousza31566ze3z87_2303;
																		}
																	}
																}
														}
													}
												}
											BgL__ortest_1034z00_1241 =
												apply(BgL_predz00_106, BgL_auxz00_4170);
										}
										if (CBOOL(BgL__ortest_1034z00_1241))
											{	/* Ieee/pairlist.scm 858 */
												return BgL__ortest_1034z00_1241;
											}
										else
											{	/* Ieee/pairlist.scm 858 */
												obj_t BgL_arg1554z00_1242;

												if (NULLP(BgL_lz00_1238))
													{	/* Ieee/pairlist.scm 858 */
														BgL_arg1554z00_1242 = BNIL;
													}
												else
													{	/* Ieee/pairlist.scm 858 */
														obj_t BgL_head1092z00_1245;

														{	/* Ieee/pairlist.scm 858 */
															obj_t BgL_arg1562z00_1257;

															{	/* Ieee/pairlist.scm 858 */
																obj_t BgL_pairz00_2321;

																BgL_pairz00_2321 = CAR(((obj_t) BgL_lz00_1238));
																BgL_arg1562z00_1257 = CDR(BgL_pairz00_2321);
															}
															BgL_head1092z00_1245 =
																MAKE_YOUNG_PAIR(BgL_arg1562z00_1257, BNIL);
														}
														{	/* Ieee/pairlist.scm 858 */
															obj_t BgL_g1095z00_1246;

															BgL_g1095z00_1246 = CDR(((obj_t) BgL_lz00_1238));
															{
																obj_t BgL_l1090z00_2342;
																obj_t BgL_tail1093z00_2343;

																BgL_l1090z00_2342 = BgL_g1095z00_1246;
																BgL_tail1093z00_2343 = BgL_head1092z00_1245;
															BgL_zc3z04anonymousza31556ze3z87_2341:
																if (NULLP(BgL_l1090z00_2342))
																	{	/* Ieee/pairlist.scm 858 */
																		BgL_arg1554z00_1242 = BgL_head1092z00_1245;
																	}
																else
																	{	/* Ieee/pairlist.scm 858 */
																		obj_t BgL_newtail1094z00_2350;

																		{	/* Ieee/pairlist.scm 858 */
																			obj_t BgL_arg1559z00_2351;

																			{	/* Ieee/pairlist.scm 858 */
																				obj_t BgL_pairz00_2355;

																				BgL_pairz00_2355 =
																					CAR(((obj_t) BgL_l1090z00_2342));
																				BgL_arg1559z00_2351 =
																					CDR(BgL_pairz00_2355);
																			}
																			BgL_newtail1094z00_2350 =
																				MAKE_YOUNG_PAIR(BgL_arg1559z00_2351,
																				BNIL);
																		}
																		SET_CDR(BgL_tail1093z00_2343,
																			BgL_newtail1094z00_2350);
																		{	/* Ieee/pairlist.scm 858 */
																			obj_t BgL_arg1558z00_2353;

																			BgL_arg1558z00_2353 =
																				CDR(((obj_t) BgL_l1090z00_2342));
																			{
																				obj_t BgL_tail1093z00_4211;
																				obj_t BgL_l1090z00_4210;

																				BgL_l1090z00_4210 = BgL_arg1558z00_2353;
																				BgL_tail1093z00_4211 =
																					BgL_newtail1094z00_2350;
																				BgL_tail1093z00_2343 =
																					BgL_tail1093z00_4211;
																				BgL_l1090z00_2342 = BgL_l1090z00_4210;
																				goto
																					BgL_zc3z04anonymousza31556ze3z87_2341;
																			}
																		}
																	}
															}
														}
													}
												{
													obj_t BgL_lz00_4212;

													BgL_lz00_4212 = BgL_arg1554z00_1242;
													BgL_lz00_1238 = BgL_lz00_4212;
													goto BgL_zc3z04anonymousza31553ze3z87_1239;
												}
											}
									}
								else
									{	/* Ieee/pairlist.scm 857 */
										return BFALSE;
									}
							}
						}
				}
		}

	}



/* &any */
	obj_t BGl_z62anyz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2770,
		obj_t BgL_predz00_2771, obj_t BgL_lz00_2772)
	{
		{	/* Ieee/pairlist.scm 847 */
			{	/* Ieee/pairlist.scm 849 */
				obj_t BgL_auxz00_4213;

				if (PROCEDUREP(BgL_predz00_2771))
					{	/* Ieee/pairlist.scm 849 */
						BgL_auxz00_4213 = BgL_predz00_2771;
					}
				else
					{
						obj_t BgL_auxz00_4216;

						BgL_auxz00_4216 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(33500L),
							BGl_string2080z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2079z00zz__r4_pairs_and_lists_6_3z00, BgL_predz00_2771);
						FAILURE(BgL_auxz00_4216, BFALSE, BFALSE);
					}
				return
					BGl_anyz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4213,
					BgL_lz00_2772);
			}
		}

	}



/* find */
	BGL_EXPORTED_DEF obj_t BGl_findz00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_predz00_108, obj_t BgL_listz00_109)
	{
		{	/* Ieee/pairlist.scm 863 */
			{	/* Ieee/pairlist.scm 864 */
				obj_t BgL_g1035z00_2358;

				{
					obj_t BgL_listz00_2363;

					BgL_listz00_2363 = BgL_listz00_109;
				BgL_lpz00_2362:
					if (PAIRP(BgL_listz00_2363))
						{	/* Ieee/pairlist.scm 873 */
							bool_t BgL_test2406z00_4223;

							{	/* Ieee/pairlist.scm 873 */
								obj_t BgL_arg1587z00_2366;

								BgL_arg1587z00_2366 = CAR(BgL_listz00_2363);
								BgL_test2406z00_4223 =
									CBOOL(BGL_PROCEDURE_CALL1(BgL_predz00_108,
										BgL_arg1587z00_2366));
							}
							if (BgL_test2406z00_4223)
								{	/* Ieee/pairlist.scm 873 */
									BgL_g1035z00_2358 = BgL_listz00_2363;
								}
							else
								{
									obj_t BgL_listz00_4230;

									BgL_listz00_4230 = CDR(BgL_listz00_2363);
									BgL_listz00_2363 = BgL_listz00_4230;
									goto BgL_lpz00_2362;
								}
						}
					else
						{	/* Ieee/pairlist.scm 872 */
							BgL_g1035z00_2358 = BFALSE;
						}
				}
				if (CBOOL(BgL_g1035z00_2358))
					{	/* Ieee/pairlist.scm 864 */
						return CAR(((obj_t) BgL_g1035z00_2358));
					}
				else
					{	/* Ieee/pairlist.scm 864 */
						return BFALSE;
					}
			}
		}

	}



/* &find */
	obj_t BGl_z62findz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2773,
		obj_t BgL_predz00_2774, obj_t BgL_listz00_2775)
	{
		{	/* Ieee/pairlist.scm 863 */
			{	/* Ieee/pairlist.scm 864 */
				obj_t BgL_auxz00_4243;
				obj_t BgL_auxz00_4236;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_listz00_2775))
					{	/* Ieee/pairlist.scm 864 */
						BgL_auxz00_4243 = BgL_listz00_2775;
					}
				else
					{
						obj_t BgL_auxz00_4246;

						BgL_auxz00_4246 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(34021L),
							BGl_string2081z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2054z00zz__r4_pairs_and_lists_6_3z00, BgL_listz00_2775);
						FAILURE(BgL_auxz00_4246, BFALSE, BFALSE);
					}
				if (PROCEDUREP(BgL_predz00_2774))
					{	/* Ieee/pairlist.scm 864 */
						BgL_auxz00_4236 = BgL_predz00_2774;
					}
				else
					{
						obj_t BgL_auxz00_4239;

						BgL_auxz00_4239 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(34021L),
							BGl_string2081z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2079z00zz__r4_pairs_and_lists_6_3z00, BgL_predz00_2774);
						FAILURE(BgL_auxz00_4239, BFALSE, BFALSE);
					}
				return
					BGl_findz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4236,
					BgL_auxz00_4243);
			}
		}

	}



/* find-tail */
	BGL_EXPORTED_DEF obj_t BGl_findzd2tailzd2zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_predz00_110, obj_t BgL_listz00_111)
	{
		{	/* Ieee/pairlist.scm 870 */
			{
				obj_t BgL_listz00_2372;

				BgL_listz00_2372 = BgL_listz00_111;
			BgL_lpz00_2371:
				if (PAIRP(BgL_listz00_2372))
					{	/* Ieee/pairlist.scm 873 */
						bool_t BgL_test2411z00_4253;

						{	/* Ieee/pairlist.scm 873 */
							obj_t BgL_arg1587z00_2375;

							BgL_arg1587z00_2375 = CAR(BgL_listz00_2372);
							BgL_test2411z00_4253 =
								CBOOL(BGL_PROCEDURE_CALL1(BgL_predz00_110,
									BgL_arg1587z00_2375));
						}
						if (BgL_test2411z00_4253)
							{	/* Ieee/pairlist.scm 873 */
								return BgL_listz00_2372;
							}
						else
							{
								obj_t BgL_listz00_4260;

								BgL_listz00_4260 = CDR(BgL_listz00_2372);
								BgL_listz00_2372 = BgL_listz00_4260;
								goto BgL_lpz00_2371;
							}
					}
				else
					{	/* Ieee/pairlist.scm 872 */
						return BFALSE;
					}
			}
		}

	}



/* &find-tail */
	obj_t BGl_z62findzd2tailzb0zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_envz00_2776, obj_t BgL_predz00_2777, obj_t BgL_listz00_2778)
	{
		{	/* Ieee/pairlist.scm 870 */
			{	/* Ieee/pairlist.scm 872 */
				obj_t BgL_auxz00_4269;
				obj_t BgL_auxz00_4262;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_listz00_2778))
					{	/* Ieee/pairlist.scm 872 */
						BgL_auxz00_4269 = BgL_listz00_2778;
					}
				else
					{
						obj_t BgL_auxz00_4272;

						BgL_auxz00_4272 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(34349L),
							BGl_string2082z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2054z00zz__r4_pairs_and_lists_6_3z00, BgL_listz00_2778);
						FAILURE(BgL_auxz00_4272, BFALSE, BFALSE);
					}
				if (PROCEDUREP(BgL_predz00_2777))
					{	/* Ieee/pairlist.scm 872 */
						BgL_auxz00_4262 = BgL_predz00_2777;
					}
				else
					{
						obj_t BgL_auxz00_4265;

						BgL_auxz00_4265 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(34349L),
							BGl_string2082z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2079z00zz__r4_pairs_and_lists_6_3z00, BgL_predz00_2777);
						FAILURE(BgL_auxz00_4265, BFALSE, BFALSE);
					}
				return
					BGl_findzd2tailzd2zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4262,
					BgL_auxz00_4269);
			}
		}

	}



/* reduce */
	BGL_EXPORTED_DEF obj_t BGl_reducez00zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_fz00_112, obj_t BgL_ridentifyz00_113, obj_t BgL_listz00_114)
	{
		{	/* Ieee/pairlist.scm 880 */
			if (NULLP(BgL_listz00_114))
				{	/* Ieee/pairlist.scm 881 */
					return BgL_ridentifyz00_113;
				}
			else
				{	/* Ieee/pairlist.scm 883 */
					obj_t BgL_g1037z00_2380;
					obj_t BgL_g1038z00_2381;

					BgL_g1037z00_2380 = CDR(BgL_listz00_114);
					BgL_g1038z00_2381 = CAR(BgL_listz00_114);
					{
						obj_t BgL_listz00_2404;
						obj_t BgL_ansz00_2405;

						BgL_listz00_2404 = BgL_g1037z00_2380;
						BgL_ansz00_2405 = BgL_g1038z00_2381;
					BgL_loopz00_2403:
						if (PAIRP(BgL_listz00_2404))
							{	/* Ieee/pairlist.scm 887 */
								obj_t BgL_arg1593z00_2411;
								obj_t BgL_arg1594z00_2412;

								BgL_arg1593z00_2411 = CDR(BgL_listz00_2404);
								{	/* Ieee/pairlist.scm 887 */
									obj_t BgL_arg1595z00_2413;

									BgL_arg1595z00_2413 = CAR(BgL_listz00_2404);
									BgL_arg1594z00_2412 =
										BGL_PROCEDURE_CALL2(BgL_fz00_112, BgL_arg1595z00_2413,
										BgL_ansz00_2405);
								}
								{
									obj_t BgL_ansz00_4291;
									obj_t BgL_listz00_4290;

									BgL_listz00_4290 = BgL_arg1593z00_2411;
									BgL_ansz00_4291 = BgL_arg1594z00_2412;
									BgL_ansz00_2405 = BgL_ansz00_4291;
									BgL_listz00_2404 = BgL_listz00_4290;
									goto BgL_loopz00_2403;
								}
							}
						else
							{	/* Ieee/pairlist.scm 885 */
								return BgL_ansz00_2405;
							}
					}
				}
		}

	}



/* &reduce */
	obj_t BGl_z62reducez62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2779,
		obj_t BgL_fz00_2780, obj_t BgL_ridentifyz00_2781, obj_t BgL_listz00_2782)
	{
		{	/* Ieee/pairlist.scm 880 */
			{	/* Ieee/pairlist.scm 881 */
				obj_t BgL_auxz00_4299;
				obj_t BgL_auxz00_4292;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_listz00_2782))
					{	/* Ieee/pairlist.scm 881 */
						BgL_auxz00_4299 = BgL_listz00_2782;
					}
				else
					{
						obj_t BgL_auxz00_4302;

						BgL_auxz00_4302 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(34689L),
							BGl_string2083z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2054z00zz__r4_pairs_and_lists_6_3z00, BgL_listz00_2782);
						FAILURE(BgL_auxz00_4302, BFALSE, BFALSE);
					}
				if (PROCEDUREP(BgL_fz00_2780))
					{	/* Ieee/pairlist.scm 881 */
						BgL_auxz00_4292 = BgL_fz00_2780;
					}
				else
					{
						obj_t BgL_auxz00_4295;

						BgL_auxz00_4295 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(34689L),
							BGl_string2083z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2079z00zz__r4_pairs_and_lists_6_3z00, BgL_fz00_2780);
						FAILURE(BgL_auxz00_4295, BFALSE, BFALSE);
					}
				return
					BGl_reducez00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4292,
					BgL_ridentifyz00_2781, BgL_auxz00_4299);
			}
		}

	}



/* make-list */
	BGL_EXPORTED_DEF obj_t BGl_makezd2listzd2zz__r4_pairs_and_lists_6_3z00(int
		BgL_nz00_115, obj_t BgL_oz00_116)
	{
		{	/* Ieee/pairlist.scm 892 */
			{	/* Ieee/pairlist.scm 893 */
				obj_t BgL_fillz00_1301;

				if (PAIRP(BgL_oz00_116))
					{	/* Ieee/pairlist.scm 893 */
						BgL_fillz00_1301 = CAR(BgL_oz00_116);
					}
				else
					{	/* Ieee/pairlist.scm 893 */
						BgL_fillz00_1301 = BUNSPEC;
					}
				{
					int BgL_iz00_2428;
					obj_t BgL_rz00_2429;

					BgL_iz00_2428 = BgL_nz00_115;
					BgL_rz00_2429 = BNIL;
				BgL_walkz00_2427:
					if (((long) (BgL_iz00_2428) <= 0L))
						{	/* Ieee/pairlist.scm 895 */
							return BgL_rz00_2429;
						}
					else
						{	/* Ieee/pairlist.scm 897 */
							long BgL_arg1598z00_2434;
							obj_t BgL_arg1601z00_2435;

							BgL_arg1598z00_2434 = ((long) (BgL_iz00_2428) - 1L);
							BgL_arg1601z00_2435 =
								MAKE_YOUNG_PAIR(BgL_fillz00_1301, BgL_rz00_2429);
							{
								obj_t BgL_rz00_4318;
								int BgL_iz00_4316;

								BgL_iz00_4316 = (int) (BgL_arg1598z00_2434);
								BgL_rz00_4318 = BgL_arg1601z00_2435;
								BgL_rz00_2429 = BgL_rz00_4318;
								BgL_iz00_2428 = BgL_iz00_4316;
								goto BgL_walkz00_2427;
							}
						}
				}
			}
		}

	}



/* &make-list */
	obj_t BGl_z62makezd2listzb0zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_envz00_2783, obj_t BgL_nz00_2784, obj_t BgL_oz00_2785)
	{
		{	/* Ieee/pairlist.scm 892 */
			{	/* Ieee/pairlist.scm 893 */
				int BgL_auxz00_4319;

				{	/* Ieee/pairlist.scm 893 */
					obj_t BgL_tmpz00_4320;

					if (INTEGERP(BgL_nz00_2784))
						{	/* Ieee/pairlist.scm 893 */
							BgL_tmpz00_4320 = BgL_nz00_2784;
						}
					else
						{
							obj_t BgL_auxz00_4323;

							BgL_auxz00_4323 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(35118L),
								BGl_string2084z00zz__r4_pairs_and_lists_6_3z00,
								BGl_string2061z00zz__r4_pairs_and_lists_6_3z00, BgL_nz00_2784);
							FAILURE(BgL_auxz00_4323, BFALSE, BFALSE);
						}
					BgL_auxz00_4319 = CINT(BgL_tmpz00_4320);
				}
				return
					BGl_makezd2listzd2zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4319,
					BgL_oz00_2785);
			}
		}

	}



/* list-tabulate */
	BGL_EXPORTED_DEF obj_t BGl_listzd2tabulatezd2zz__r4_pairs_and_lists_6_3z00(int
		BgL_nz00_117, obj_t BgL_initzd2proczd2_118)
	{
		{	/* Ieee/pairlist.scm 902 */
			{	/* Ieee/pairlist.scm 903 */
				long BgL_g1040z00_1312;

				BgL_g1040z00_1312 = ((long) (BgL_nz00_117) - 1L);
				{
					long BgL_iz00_2452;
					obj_t BgL_rz00_2453;

					BgL_iz00_2452 = BgL_g1040z00_1312;
					BgL_rz00_2453 = BNIL;
				BgL_walkz00_2451:
					if ((BgL_iz00_2452 < 0L))
						{	/* Ieee/pairlist.scm 904 */
							return BgL_rz00_2453;
						}
					else
						{	/* Ieee/pairlist.scm 906 */
							long BgL_arg1605z00_2459;
							obj_t BgL_arg1606z00_2460;

							BgL_arg1605z00_2459 = (BgL_iz00_2452 - 1L);
							{	/* Ieee/pairlist.scm 906 */
								obj_t BgL_arg1607z00_2461;

								BgL_arg1607z00_2461 =
									BGL_PROCEDURE_CALL1(BgL_initzd2proczd2_118,
									BINT(BgL_iz00_2452));
								BgL_arg1606z00_2460 =
									MAKE_YOUNG_PAIR(BgL_arg1607z00_2461, BgL_rz00_2453);
							}
							{
								obj_t BgL_rz00_4341;
								long BgL_iz00_4340;

								BgL_iz00_4340 = BgL_arg1605z00_2459;
								BgL_rz00_4341 = BgL_arg1606z00_2460;
								BgL_rz00_2453 = BgL_rz00_4341;
								BgL_iz00_2452 = BgL_iz00_4340;
								goto BgL_walkz00_2451;
							}
						}
				}
			}
		}

	}



/* &list-tabulate */
	obj_t BGl_z62listzd2tabulatezb0zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_envz00_2786, obj_t BgL_nz00_2787, obj_t BgL_initzd2proczd2_2788)
	{
		{	/* Ieee/pairlist.scm 902 */
			{	/* Ieee/pairlist.scm 903 */
				obj_t BgL_auxz00_4351;
				int BgL_auxz00_4342;

				if (PROCEDUREP(BgL_initzd2proczd2_2788))
					{	/* Ieee/pairlist.scm 903 */
						BgL_auxz00_4351 = BgL_initzd2proczd2_2788;
					}
				else
					{
						obj_t BgL_auxz00_4354;

						BgL_auxz00_4354 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(35529L),
							BGl_string2085z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2079z00zz__r4_pairs_and_lists_6_3z00,
							BgL_initzd2proczd2_2788);
						FAILURE(BgL_auxz00_4354, BFALSE, BFALSE);
					}
				{	/* Ieee/pairlist.scm 903 */
					obj_t BgL_tmpz00_4343;

					if (INTEGERP(BgL_nz00_2787))
						{	/* Ieee/pairlist.scm 903 */
							BgL_tmpz00_4343 = BgL_nz00_2787;
						}
					else
						{
							obj_t BgL_auxz00_4346;

							BgL_auxz00_4346 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(35529L),
								BGl_string2085z00zz__r4_pairs_and_lists_6_3z00,
								BGl_string2061z00zz__r4_pairs_and_lists_6_3z00, BgL_nz00_2787);
							FAILURE(BgL_auxz00_4346, BFALSE, BFALSE);
						}
					BgL_auxz00_4342 = CINT(BgL_tmpz00_4343);
				}
				return
					BGl_listzd2tabulatezd2zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4342,
					BgL_auxz00_4351);
			}
		}

	}



/* list-split */
	BGL_EXPORTED_DEF obj_t BGl_listzd2splitzd2zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_lz00_119, int BgL_numz00_120, obj_t BgL_fillz00_121)
	{
		{	/* Ieee/pairlist.scm 911 */
			{
				obj_t BgL_lz00_1326;
				long BgL_iz00_1327;
				obj_t BgL_accz00_1328;
				obj_t BgL_resz00_1329;

				BgL_lz00_1326 = BgL_lz00_119;
				BgL_iz00_1327 = 0L;
				BgL_accz00_1328 = BNIL;
				BgL_resz00_1329 = BNIL;
			BgL_zc3z04anonymousza31608ze3z87_1330:
				if (NULLP(BgL_lz00_1326))
					{	/* Ieee/pairlist.scm 918 */
						obj_t BgL_arg1610z00_1332;

						{	/* Ieee/pairlist.scm 918 */
							obj_t BgL_arg1611z00_1333;

							{	/* Ieee/pairlist.scm 918 */
								bool_t BgL_test2425z00_4361;

								if (NULLP(BgL_fillz00_121))
									{	/* Ieee/pairlist.scm 918 */
										BgL_test2425z00_4361 = ((bool_t) 1);
									}
								else
									{	/* Ieee/pairlist.scm 918 */
										if ((BgL_iz00_1327 == (long) (BgL_numz00_120)))
											{	/* Ieee/pairlist.scm 918 */
												BgL_test2425z00_4361 = ((bool_t) 1);
											}
										else
											{	/* Ieee/pairlist.scm 918 */
												BgL_test2425z00_4361 = (BgL_iz00_1327 == 0L);
											}
									}
								if (BgL_test2425z00_4361)
									{	/* Ieee/pairlist.scm 918 */
										BgL_arg1611z00_1333 = bgl_reverse_bang(BgL_accz00_1328);
									}
								else
									{	/* Ieee/pairlist.scm 920 */
										obj_t BgL_arg1615z00_1337;
										obj_t BgL_arg1616z00_1338;

										BgL_arg1615z00_1337 = bgl_reverse_bang(BgL_accz00_1328);
										{	/* Ieee/pairlist.scm 921 */
											long BgL_arg1617z00_1339;
											obj_t BgL_arg1618z00_1340;

											BgL_arg1617z00_1339 =
												((long) (BgL_numz00_120) - BgL_iz00_1327);
											BgL_arg1618z00_1340 = CAR(((obj_t) BgL_fillz00_121));
											{	/* Ieee/pairlist.scm 921 */
												obj_t BgL_list1619z00_1341;

												BgL_list1619z00_1341 =
													MAKE_YOUNG_PAIR(BgL_arg1618z00_1340, BNIL);
												BgL_arg1616z00_1338 =
													BGl_makezd2listzd2zz__r4_pairs_and_lists_6_3z00(
													(int) (BgL_arg1617z00_1339), BgL_list1619z00_1341);
										}}
										BgL_arg1611z00_1333 =
											BGl_appendzd22z12zc0zz__r4_pairs_and_lists_6_3z00
											(BgL_arg1615z00_1337, BgL_arg1616z00_1338);
							}}
							BgL_arg1610z00_1332 =
								MAKE_YOUNG_PAIR(BgL_arg1611z00_1333, BgL_resz00_1329);
						}
						return bgl_reverse_bang(BgL_arg1610z00_1332);
					}
				else
					{	/* Ieee/pairlist.scm 917 */
						if ((BgL_iz00_1327 == (long) (BgL_numz00_120)))
							{	/* Ieee/pairlist.scm 924 */
								obj_t BgL_arg1621z00_1345;

								BgL_arg1621z00_1345 =
									MAKE_YOUNG_PAIR(bgl_reverse_bang(BgL_accz00_1328),
									BgL_resz00_1329);
								{
									obj_t BgL_resz00_4387;
									obj_t BgL_accz00_4386;
									long BgL_iz00_4385;

									BgL_iz00_4385 = 0L;
									BgL_accz00_4386 = BNIL;
									BgL_resz00_4387 = BgL_arg1621z00_1345;
									BgL_resz00_1329 = BgL_resz00_4387;
									BgL_accz00_1328 = BgL_accz00_4386;
									BgL_iz00_1327 = BgL_iz00_4385;
									goto BgL_zc3z04anonymousza31608ze3z87_1330;
								}
							}
						else
							{	/* Ieee/pairlist.scm 926 */
								obj_t BgL_arg1623z00_1347;
								long BgL_arg1624z00_1348;
								obj_t BgL_arg1625z00_1349;

								BgL_arg1623z00_1347 = CDR(((obj_t) BgL_lz00_1326));
								BgL_arg1624z00_1348 = (BgL_iz00_1327 + 1L);
								{	/* Ieee/pairlist.scm 926 */
									obj_t BgL_arg1626z00_1350;

									BgL_arg1626z00_1350 = CAR(((obj_t) BgL_lz00_1326));
									BgL_arg1625z00_1349 =
										MAKE_YOUNG_PAIR(BgL_arg1626z00_1350, BgL_accz00_1328);
								}
								{
									obj_t BgL_accz00_4396;
									long BgL_iz00_4395;
									obj_t BgL_lz00_4394;

									BgL_lz00_4394 = BgL_arg1623z00_1347;
									BgL_iz00_4395 = BgL_arg1624z00_1348;
									BgL_accz00_4396 = BgL_arg1625z00_1349;
									BgL_accz00_1328 = BgL_accz00_4396;
									BgL_iz00_1327 = BgL_iz00_4395;
									BgL_lz00_1326 = BgL_lz00_4394;
									goto BgL_zc3z04anonymousza31608ze3z87_1330;
								}
							}
					}
			}
		}

	}



/* &list-split */
	obj_t BGl_z62listzd2splitzb0zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_envz00_2789, obj_t BgL_lz00_2790, obj_t BgL_numz00_2791,
		obj_t BgL_fillz00_2792)
	{
		{	/* Ieee/pairlist.scm 911 */
			{	/* Ieee/pairlist.scm 917 */
				int BgL_auxz00_4404;
				obj_t BgL_auxz00_4397;

				{	/* Ieee/pairlist.scm 917 */
					obj_t BgL_tmpz00_4405;

					if (INTEGERP(BgL_numz00_2791))
						{	/* Ieee/pairlist.scm 917 */
							BgL_tmpz00_4405 = BgL_numz00_2791;
						}
					else
						{
							obj_t BgL_auxz00_4408;

							BgL_auxz00_4408 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(35973L),
								BGl_string2086z00zz__r4_pairs_and_lists_6_3z00,
								BGl_string2061z00zz__r4_pairs_and_lists_6_3z00,
								BgL_numz00_2791);
							FAILURE(BgL_auxz00_4408, BFALSE, BFALSE);
						}
					BgL_auxz00_4404 = CINT(BgL_tmpz00_4405);
				}
				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_lz00_2790))
					{	/* Ieee/pairlist.scm 917 */
						BgL_auxz00_4397 = BgL_lz00_2790;
					}
				else
					{
						obj_t BgL_auxz00_4400;

						BgL_auxz00_4400 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(35973L),
							BGl_string2086z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2054z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_2790);
						FAILURE(BgL_auxz00_4400, BFALSE, BFALSE);
					}
				return
					BGl_listzd2splitzd2zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4397,
					BgL_auxz00_4404, BgL_fillz00_2792);
			}
		}

	}



/* list-split! */
	BGL_EXPORTED_DEF obj_t
		BGl_listzd2splitz12zc0zz__r4_pairs_and_lists_6_3z00(obj_t BgL_lz00_122,
		int BgL_numz00_123, obj_t BgL_fillz00_124)
	{
		{	/* Ieee/pairlist.scm 931 */
			{
				obj_t BgL_lz00_1354;
				long BgL_iz00_1355;
				obj_t BgL_lastz00_1356;
				obj_t BgL_accz00_1357;
				obj_t BgL_rowsz00_1358;

				BgL_lz00_1354 = BgL_lz00_122;
				BgL_iz00_1355 = 0L;
				BgL_lastz00_1356 = BFALSE;
				BgL_accz00_1357 = BgL_lz00_122;
				BgL_rowsz00_1358 = BNIL;
			BgL_zc3z04anonymousza31627ze3z87_1359:
				if (NULLP(BgL_lz00_1354))
					{	/* Ieee/pairlist.scm 939 */
						obj_t BgL_lrowz00_1361;

						{	/* Ieee/pairlist.scm 939 */
							bool_t BgL_test2432z00_4416;

							if (NULLP(BgL_fillz00_124))
								{	/* Ieee/pairlist.scm 939 */
									BgL_test2432z00_4416 = ((bool_t) 1);
								}
							else
								{	/* Ieee/pairlist.scm 939 */
									if ((BgL_iz00_1355 == (long) (BgL_numz00_123)))
										{	/* Ieee/pairlist.scm 939 */
											BgL_test2432z00_4416 = ((bool_t) 1);
										}
									else
										{	/* Ieee/pairlist.scm 939 */
											BgL_test2432z00_4416 = (BgL_iz00_1355 == 0L);
										}
								}
							if (BgL_test2432z00_4416)
								{	/* Ieee/pairlist.scm 939 */
									BgL_lrowz00_1361 = BgL_accz00_1357;
								}
							else
								{	/* Ieee/pairlist.scm 939 */
									{	/* Ieee/pairlist.scm 943 */
										obj_t BgL_arg1634z00_1366;

										{	/* Ieee/pairlist.scm 943 */
											long BgL_arg1636z00_1367;
											obj_t BgL_arg1637z00_1368;

											BgL_arg1636z00_1367 =
												((long) (BgL_numz00_123) - BgL_iz00_1355);
											BgL_arg1637z00_1368 = CAR(((obj_t) BgL_fillz00_124));
											{	/* Ieee/pairlist.scm 943 */
												obj_t BgL_list1638z00_1369;

												BgL_list1638z00_1369 =
													MAKE_YOUNG_PAIR(BgL_arg1637z00_1368, BNIL);
												BgL_arg1634z00_1366 =
													BGl_makezd2listzd2zz__r4_pairs_and_lists_6_3z00(
													(int) (BgL_arg1636z00_1367), BgL_list1638z00_1369);
										}}
										{	/* Ieee/pairlist.scm 454 */
											obj_t BgL_tmpz00_4430;

											BgL_tmpz00_4430 = ((obj_t) BgL_lastz00_1356);
											SET_CDR(BgL_tmpz00_4430, BgL_arg1634z00_1366);
									}}
									BgL_lrowz00_1361 = BgL_accz00_1357;
						}}
						{	/* Ieee/pairlist.scm 945 */
							obj_t BgL_arg1629z00_1362;

							BgL_arg1629z00_1362 =
								MAKE_YOUNG_PAIR(BgL_lrowz00_1361, BgL_rowsz00_1358);
							return bgl_reverse_bang(BgL_arg1629z00_1362);
						}
					}
				else
					{	/* Ieee/pairlist.scm 938 */
						if ((BgL_iz00_1355 == (long) (BgL_numz00_123)))
							{	/* Ieee/pairlist.scm 946 */
								{	/* Ieee/pairlist.scm 454 */
									obj_t BgL_tmpz00_4438;

									BgL_tmpz00_4438 = ((obj_t) BgL_lastz00_1356);
									SET_CDR(BgL_tmpz00_4438, BNIL);
								}
								{	/* Ieee/pairlist.scm 948 */
									obj_t BgL_arg1640z00_1373;

									BgL_arg1640z00_1373 =
										MAKE_YOUNG_PAIR(BgL_accz00_1357, BgL_rowsz00_1358);
									{
										obj_t BgL_rowsz00_4445;
										obj_t BgL_accz00_4444;
										obj_t BgL_lastz00_4443;
										long BgL_iz00_4442;

										BgL_iz00_4442 = 0L;
										BgL_lastz00_4443 = BgL_lz00_1354;
										BgL_accz00_4444 = BgL_lz00_1354;
										BgL_rowsz00_4445 = BgL_arg1640z00_1373;
										BgL_rowsz00_1358 = BgL_rowsz00_4445;
										BgL_accz00_1357 = BgL_accz00_4444;
										BgL_lastz00_1356 = BgL_lastz00_4443;
										BgL_iz00_1355 = BgL_iz00_4442;
										goto BgL_zc3z04anonymousza31627ze3z87_1359;
									}
								}
							}
						else
							{	/* Ieee/pairlist.scm 950 */
								obj_t BgL_arg1641z00_1374;
								long BgL_arg1642z00_1375;

								BgL_arg1641z00_1374 = CDR(((obj_t) BgL_lz00_1354));
								BgL_arg1642z00_1375 = (BgL_iz00_1355 + 1L);
								{
									obj_t BgL_lastz00_4451;
									long BgL_iz00_4450;
									obj_t BgL_lz00_4449;

									BgL_lz00_4449 = BgL_arg1641z00_1374;
									BgL_iz00_4450 = BgL_arg1642z00_1375;
									BgL_lastz00_4451 = BgL_lz00_1354;
									BgL_lastz00_1356 = BgL_lastz00_4451;
									BgL_iz00_1355 = BgL_iz00_4450;
									BgL_lz00_1354 = BgL_lz00_4449;
									goto BgL_zc3z04anonymousza31627ze3z87_1359;
								}
							}
					}
			}
		}

	}



/* &list-split! */
	obj_t BGl_z62listzd2splitz12za2zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_envz00_2793, obj_t BgL_lz00_2794, obj_t BgL_numz00_2795,
		obj_t BgL_fillz00_2796)
	{
		{	/* Ieee/pairlist.scm 931 */
			{	/* Ieee/pairlist.scm 938 */
				int BgL_auxz00_4459;
				obj_t BgL_auxz00_4452;

				{	/* Ieee/pairlist.scm 938 */
					obj_t BgL_tmpz00_4460;

					if (INTEGERP(BgL_numz00_2795))
						{	/* Ieee/pairlist.scm 938 */
							BgL_tmpz00_4460 = BgL_numz00_2795;
						}
					else
						{
							obj_t BgL_auxz00_4463;

							BgL_auxz00_4463 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(36643L),
								BGl_string2087z00zz__r4_pairs_and_lists_6_3z00,
								BGl_string2061z00zz__r4_pairs_and_lists_6_3z00,
								BgL_numz00_2795);
							FAILURE(BgL_auxz00_4463, BFALSE, BFALSE);
						}
					BgL_auxz00_4459 = CINT(BgL_tmpz00_4460);
				}
				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_lz00_2794))
					{	/* Ieee/pairlist.scm 938 */
						BgL_auxz00_4452 = BgL_lz00_2794;
					}
				else
					{
						obj_t BgL_auxz00_4455;

						BgL_auxz00_4455 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(36643L),
							BGl_string2087z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2054z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_2794);
						FAILURE(BgL_auxz00_4455, BFALSE, BFALSE);
					}
				return
					BGl_listzd2splitz12zc0zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4452,
					BgL_auxz00_4459, BgL_fillz00_2796);
			}
		}

	}



/* iota */
	BGL_EXPORTED_DEF obj_t BGl_iotaz00zz__r4_pairs_and_lists_6_3z00(int
		BgL_countz00_125, obj_t BgL_restz00_126)
	{
		{	/* Ieee/pairlist.scm 955 */
			{	/* Ieee/pairlist.scm 956 */
				obj_t BgL_startz00_1377;
				obj_t BgL_stepz00_1378;

				BgL_startz00_1377 = BINT(0L);
				BgL_stepz00_1378 = BINT(1L);
				if (PAIRP(BgL_restz00_126))
					{	/* Ieee/pairlist.scm 958 */
						BgL_startz00_1377 = CAR(BgL_restz00_126);
						{	/* Ieee/pairlist.scm 961 */
							bool_t BgL_test2439z00_4474;

							{	/* Ieee/pairlist.scm 229 */
								obj_t BgL_tmpz00_4475;

								BgL_tmpz00_4475 = CDR(BgL_restz00_126);
								BgL_test2439z00_4474 = PAIRP(BgL_tmpz00_4475);
							}
							if (BgL_test2439z00_4474)
								{	/* Ieee/pairlist.scm 961 */
									BgL_stepz00_1378 = CAR(CDR(BgL_restz00_126));
								}
							else
								{	/* Ieee/pairlist.scm 961 */
									BFALSE;
								}
						}
					}
				else
					{	/* Ieee/pairlist.scm 958 */
						BFALSE;
					}
				{	/* Ieee/pairlist.scm 963 */
					obj_t BgL_g1045z00_1383;

					{	/* Ieee/pairlist.scm 963 */
						obj_t BgL_b1097z00_1396;

						{	/* Ieee/pairlist.scm 963 */
							long BgL_a1096z00_1398;

							BgL_a1096z00_1398 = ((long) (BgL_countz00_125) - 1L);
							if (INTEGERP(BgL_stepz00_1378))
								{	/* Ieee/pairlist.scm 963 */
									long BgL_za72za7_2495;

									BgL_za72za7_2495 = (long) CINT(BgL_stepz00_1378);
									BgL_b1097z00_1396 =
										BINT((BgL_a1096z00_1398 * BgL_za72za7_2495));
								}
							else
								{	/* Ieee/pairlist.scm 963 */
									BgL_b1097z00_1396 =
										BGl_2za2za2zz__r4_numbers_6_5z00(BINT(BgL_a1096z00_1398),
										BgL_stepz00_1378);
								}
						}
						{	/* Ieee/pairlist.scm 963 */
							bool_t BgL_test2441z00_4489;

							if (INTEGERP(BgL_startz00_1377))
								{	/* Ieee/pairlist.scm 963 */
									BgL_test2441z00_4489 = INTEGERP(BgL_b1097z00_1396);
								}
							else
								{	/* Ieee/pairlist.scm 963 */
									BgL_test2441z00_4489 = ((bool_t) 0);
								}
							if (BgL_test2441z00_4489)
								{	/* Ieee/pairlist.scm 963 */
									BgL_g1045z00_1383 =
										ADDFX(BgL_startz00_1377, BgL_b1097z00_1396);
								}
							else
								{	/* Ieee/pairlist.scm 963 */
									BgL_g1045z00_1383 =
										BGl_2zb2zb2zz__r4_numbers_6_5z00(BgL_startz00_1377,
										BgL_b1097z00_1396);
								}
						}
					}
					{
						int BgL_iz00_2517;
						obj_t BgL_vz00_2518;
						obj_t BgL_rz00_2519;

						BgL_iz00_2517 = BgL_countz00_125;
						BgL_vz00_2518 = BgL_g1045z00_1383;
						BgL_rz00_2519 = BNIL;
					BgL_walkz00_2516:
						if (((long) (BgL_iz00_2517) <= 0L))
							{	/* Ieee/pairlist.scm 964 */
								return BgL_rz00_2519;
							}
						else
							{	/* Ieee/pairlist.scm 966 */
								long BgL_arg1649z00_2526;
								obj_t BgL_arg1650z00_2527;
								obj_t BgL_arg1651z00_2528;

								BgL_arg1649z00_2526 = ((long) (BgL_iz00_2517) - 1L);
								{	/* Ieee/pairlist.scm 966 */
									bool_t BgL_test2444z00_4500;

									if (INTEGERP(BgL_vz00_2518))
										{	/* Ieee/pairlist.scm 966 */
											BgL_test2444z00_4500 = INTEGERP(BgL_stepz00_1378);
										}
									else
										{	/* Ieee/pairlist.scm 966 */
											BgL_test2444z00_4500 = ((bool_t) 0);
										}
									if (BgL_test2444z00_4500)
										{	/* Ieee/pairlist.scm 966 */
											BgL_arg1650z00_2527 =
												SUBFX(BgL_vz00_2518, BgL_stepz00_1378);
										}
									else
										{	/* Ieee/pairlist.scm 966 */
											BgL_arg1650z00_2527 =
												BGl_2zd2zd2zz__r4_numbers_6_5z00(BgL_vz00_2518,
												BgL_stepz00_1378);
										}
								}
								BgL_arg1651z00_2528 =
									MAKE_YOUNG_PAIR(BgL_vz00_2518, BgL_rz00_2519);
								{
									obj_t BgL_rz00_4510;
									obj_t BgL_vz00_4509;
									int BgL_iz00_4507;

									BgL_iz00_4507 = (int) (BgL_arg1649z00_2526);
									BgL_vz00_4509 = BgL_arg1650z00_2527;
									BgL_rz00_4510 = BgL_arg1651z00_2528;
									BgL_rz00_2519 = BgL_rz00_4510;
									BgL_vz00_2518 = BgL_vz00_4509;
									BgL_iz00_2517 = BgL_iz00_4507;
									goto BgL_walkz00_2516;
								}
							}
					}
				}
			}
		}

	}



/* &iota */
	obj_t BGl_z62iotaz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2797,
		obj_t BgL_countz00_2798, obj_t BgL_restz00_2799)
	{
		{	/* Ieee/pairlist.scm 955 */
			{	/* Ieee/pairlist.scm 956 */
				int BgL_auxz00_4511;

				{	/* Ieee/pairlist.scm 956 */
					obj_t BgL_tmpz00_4512;

					if (INTEGERP(BgL_countz00_2798))
						{	/* Ieee/pairlist.scm 956 */
							BgL_tmpz00_4512 = BgL_countz00_2798;
						}
					else
						{
							obj_t BgL_auxz00_4515;

							BgL_auxz00_4515 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(37234L),
								BGl_string2088z00zz__r4_pairs_and_lists_6_3z00,
								BGl_string2061z00zz__r4_pairs_and_lists_6_3z00,
								BgL_countz00_2798);
							FAILURE(BgL_auxz00_4515, BFALSE, BFALSE);
						}
					BgL_auxz00_4511 = CINT(BgL_tmpz00_4512);
				}
				return
					BGl_iotaz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4511,
					BgL_restz00_2799);
			}
		}

	}



/* list-copy */
	BGL_EXPORTED_DEF obj_t BGl_listzd2copyzd2zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_lz00_127)
	{
		{	/* Ieee/pairlist.scm 971 */
			if (NULLP(BgL_lz00_127))
				{	/* Ieee/pairlist.scm 972 */
					return BgL_lz00_127;
				}
			else
				{	/* Ieee/pairlist.scm 972 */
					return
						MAKE_YOUNG_PAIR(CAR(BgL_lz00_127),
						BGl_listzd2copyzd2zz__r4_pairs_and_lists_6_3z00(CDR(BgL_lz00_127)));
				}
		}

	}



/* &list-copy */
	obj_t BGl_z62listzd2copyzb0zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_envz00_2800, obj_t BgL_lz00_2801)
	{
		{	/* Ieee/pairlist.scm 971 */
			{	/* Ieee/pairlist.scm 972 */
				obj_t BgL_auxz00_4527;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_lz00_2801))
					{	/* Ieee/pairlist.scm 972 */
						BgL_auxz00_4527 = BgL_lz00_2801;
					}
				else
					{
						obj_t BgL_auxz00_4530;

						BgL_auxz00_4530 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00, BINT(37778L),
							BGl_string2089z00zz__r4_pairs_and_lists_6_3z00,
							BGl_string2054z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_2801);
						FAILURE(BgL_auxz00_4530, BFALSE, BFALSE);
					}
				return BGl_listzd2copyzd2zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4527);
			}
		}

	}



/* tree-copy */
	BGL_EXPORTED_DEF obj_t BGl_treezd2copyzd2zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_lz00_128)
	{
		{	/* Ieee/pairlist.scm 979 */
			if (EPAIRP(BgL_lz00_128))
				{	/* Ieee/pairlist.scm 982 */
					obj_t BgL_arg1661z00_1405;
					obj_t BgL_arg1663z00_1406;
					obj_t BgL_arg1664z00_1407;

					{	/* Ieee/pairlist.scm 982 */
						obj_t BgL_arg1667z00_1408;

						BgL_arg1667z00_1408 = CAR(((obj_t) BgL_lz00_128));
						BgL_arg1661z00_1405 =
							BGl_treezd2copyzd2zz__r4_pairs_and_lists_6_3z00
							(BgL_arg1667z00_1408);
					}
					{	/* Ieee/pairlist.scm 982 */
						obj_t BgL_arg1668z00_1409;

						BgL_arg1668z00_1409 = CDR(((obj_t) BgL_lz00_128));
						BgL_arg1663z00_1406 =
							BGl_treezd2copyzd2zz__r4_pairs_and_lists_6_3z00
							(BgL_arg1668z00_1409);
					}
					{	/* Ieee/pairlist.scm 982 */
						obj_t BgL_arg1669z00_1410;

						BgL_arg1669z00_1410 = CER(((obj_t) BgL_lz00_128));
						BgL_arg1664z00_1407 =
							BGl_treezd2copyzd2zz__r4_pairs_and_lists_6_3z00
							(BgL_arg1669z00_1410);
					}
					{	/* Ieee/pairlist.scm 982 */
						obj_t BgL_res1851z00_2543;

						BgL_res1851z00_2543 =
							MAKE_YOUNG_EPAIR(BgL_arg1661z00_1405, BgL_arg1663z00_1406,
							BgL_arg1664z00_1407);
						return BgL_res1851z00_2543;
					}
				}
			else
				{	/* Ieee/pairlist.scm 981 */
					if (PAIRP(BgL_lz00_128))
						{	/* Ieee/pairlist.scm 983 */
							return
								MAKE_YOUNG_PAIR(BGl_treezd2copyzd2zz__r4_pairs_and_lists_6_3z00
								(CAR(BgL_lz00_128)),
								BGl_treezd2copyzd2zz__r4_pairs_and_lists_6_3z00(CDR
									(BgL_lz00_128)));
						}
					else
						{	/* Ieee/pairlist.scm 983 */
							return BgL_lz00_128;
						}
				}
		}

	}



/* &tree-copy */
	obj_t BGl_z62treezd2copyzb0zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_envz00_2802, obj_t BgL_lz00_2803)
	{
		{	/* Ieee/pairlist.scm 979 */
			return BGl_treezd2copyzd2zz__r4_pairs_and_lists_6_3z00(BgL_lz00_2803);
		}

	}



/* _delete-duplicates */
	obj_t BGl__deletezd2duplicateszd2zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_env1118z00_132, obj_t BgL_opt1117z00_131)
	{
		{	/* Ieee/pairlist.scm 991 */
			{	/* Ieee/pairlist.scm 991 */
				obj_t BgL_g1119z00_1416;

				BgL_g1119z00_1416 = VECTOR_REF(BgL_opt1117z00_131, 0L);
				switch (VECTOR_LENGTH(BgL_opt1117z00_131))
					{
					case 1L:

						{	/* Ieee/pairlist.scm 991 */

							{	/* Ieee/pairlist.scm 991 */
								obj_t BgL_lisz00_2546;

								if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
									(BgL_g1119z00_1416))
									{	/* Ieee/pairlist.scm 991 */
										BgL_lisz00_2546 = BgL_g1119z00_1416;
									}
								else
									{
										obj_t BgL_auxz00_4558;

										BgL_auxz00_4558 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00,
											BINT(38512L),
											BGl_string2090z00zz__r4_pairs_and_lists_6_3z00,
											BGl_string2054z00zz__r4_pairs_and_lists_6_3z00,
											BgL_g1119z00_1416);
										FAILURE(BgL_auxz00_4558, BFALSE, BFALSE);
									}
								return
									BGl_deletezd2duplicatesz12zc0zz__r4_pairs_and_lists_6_3z00
									(BGl_listzd2copyzd2zz__r4_pairs_and_lists_6_3z00
									(BgL_lisz00_2546),
									BGl_equalzf3zd2envz21zz__r4_equivalence_6_2z00);
							}
						}
						break;
					case 2L:

						{	/* Ieee/pairlist.scm 991 */
							obj_t BgL_eqz00_1420;

							BgL_eqz00_1420 = VECTOR_REF(BgL_opt1117z00_131, 1L);
							{	/* Ieee/pairlist.scm 991 */

								{	/* Ieee/pairlist.scm 991 */
									obj_t BgL_lisz00_2548;

									if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
										(BgL_g1119z00_1416))
										{	/* Ieee/pairlist.scm 991 */
											BgL_lisz00_2548 = BgL_g1119z00_1416;
										}
									else
										{
											obj_t BgL_auxz00_4567;

											BgL_auxz00_4567 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00,
												BINT(38512L),
												BGl_string2090z00zz__r4_pairs_and_lists_6_3z00,
												BGl_string2054z00zz__r4_pairs_and_lists_6_3z00,
												BgL_g1119z00_1416);
											FAILURE(BgL_auxz00_4567, BFALSE, BFALSE);
										}
									return
										BGl_deletezd2duplicatesz12zc0zz__r4_pairs_and_lists_6_3z00
										(BGl_listzd2copyzd2zz__r4_pairs_and_lists_6_3z00
										(BgL_lisz00_2548), BgL_eqz00_1420);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* delete-duplicates */
	BGL_EXPORTED_DEF obj_t
		BGl_deletezd2duplicateszd2zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_lisz00_129, obj_t BgL_eqz00_130)
	{
		{	/* Ieee/pairlist.scm 991 */
			BGL_TAIL return
				BGl_deletezd2duplicatesz12zc0zz__r4_pairs_and_lists_6_3z00
				(BGl_listzd2copyzd2zz__r4_pairs_and_lists_6_3z00(BgL_lisz00_129),
				BgL_eqz00_130);
		}

	}



/* _delete-duplicates! */
	obj_t BGl__deletezd2duplicatesz12zc0zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_env1123z00_136, obj_t BgL_opt1122z00_135)
	{
		{	/* Ieee/pairlist.scm 997 */
			{	/* Ieee/pairlist.scm 997 */
				obj_t BgL_g1124z00_1422;

				BgL_g1124z00_1422 = VECTOR_REF(BgL_opt1122z00_135, 0L);
				switch (VECTOR_LENGTH(BgL_opt1122z00_135))
					{
					case 1L:

						{	/* Ieee/pairlist.scm 997 */

							{	/* Ieee/pairlist.scm 997 */
								obj_t BgL_auxz00_4578;

								if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
									(BgL_g1124z00_1422))
									{	/* Ieee/pairlist.scm 997 */
										BgL_auxz00_4578 = BgL_g1124z00_1422;
									}
								else
									{
										obj_t BgL_auxz00_4581;

										BgL_auxz00_4581 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00,
											BINT(38834L),
											BGl_string2091z00zz__r4_pairs_and_lists_6_3z00,
											BGl_string2054z00zz__r4_pairs_and_lists_6_3z00,
											BgL_g1124z00_1422);
										FAILURE(BgL_auxz00_4581, BFALSE, BFALSE);
									}
								return
									BGl_deletezd2duplicatesz12zc0zz__r4_pairs_and_lists_6_3z00
									(BgL_auxz00_4578,
									BGl_equalzf3zd2envz21zz__r4_equivalence_6_2z00);
							}
						}
						break;
					case 2L:

						{	/* Ieee/pairlist.scm 997 */
							obj_t BgL_eqz00_1426;

							BgL_eqz00_1426 = VECTOR_REF(BgL_opt1122z00_135, 1L);
							{	/* Ieee/pairlist.scm 997 */

								{	/* Ieee/pairlist.scm 997 */
									obj_t BgL_auxz00_4587;

									if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
										(BgL_g1124z00_1422))
										{	/* Ieee/pairlist.scm 997 */
											BgL_auxz00_4587 = BgL_g1124z00_1422;
										}
									else
										{
											obj_t BgL_auxz00_4590;

											BgL_auxz00_4590 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2016z00zz__r4_pairs_and_lists_6_3z00,
												BINT(38834L),
												BGl_string2091z00zz__r4_pairs_and_lists_6_3z00,
												BGl_string2054z00zz__r4_pairs_and_lists_6_3z00,
												BgL_g1124z00_1422);
											FAILURE(BgL_auxz00_4590, BFALSE, BFALSE);
										}
									return
										BGl_deletezd2duplicatesz12zc0zz__r4_pairs_and_lists_6_3z00
										(BgL_auxz00_4587, BgL_eqz00_1426);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* delete-duplicates! */
	BGL_EXPORTED_DEF obj_t
		BGl_deletezd2duplicatesz12zc0zz__r4_pairs_and_lists_6_3z00(obj_t
		BgL_lisz00_133, obj_t BgL_eqz00_134)
	{
		{	/* Ieee/pairlist.scm 997 */
			if (PROCEDUREP(BgL_eqz00_134))
				{	/* Ieee/pairlist.scm 998 */
					BFALSE;
				}
			else
				{	/* Ieee/pairlist.scm 998 */
					BGl_bigloozd2typezd2errorz00zz__errorz00
						(BGl_symbol2092z00zz__r4_pairs_and_lists_6_3z00,
						BGl_string2079z00zz__r4_pairs_and_lists_6_3z00, BgL_eqz00_134);
				}
			return
				BGl_recurze70ze7zz__r4_pairs_and_lists_6_3z00(BgL_eqz00_134,
				BgL_lisz00_133);
		}

	}



/* recur~0 */
	obj_t BGl_recurze70ze7zz__r4_pairs_and_lists_6_3z00(obj_t BgL_eqz00_2804,
		obj_t BgL_lisz00_1429)
	{
		{	/* Ieee/pairlist.scm 1000 */
			if (NULLP(BgL_lisz00_1429))
				{	/* Ieee/pairlist.scm 1001 */
					return BgL_lisz00_1429;
				}
			else
				{	/* Ieee/pairlist.scm 1002 */
					obj_t BgL_xz00_1432;

					BgL_xz00_1432 = CAR(BgL_lisz00_1429);
					{	/* Ieee/pairlist.scm 1002 */
						obj_t BgL_tailz00_1433;

						BgL_tailz00_1433 = CDR(BgL_lisz00_1429);
						{	/* Ieee/pairlist.scm 1003 */
							obj_t BgL_newzd2tailzd2_1434;

							BgL_newzd2tailzd2_1434 =
								BGl_recurze70ze7zz__r4_pairs_and_lists_6_3z00(BgL_eqz00_2804,
								BGl_deletez12z12zz__r4_pairs_and_lists_6_3z00(BgL_xz00_1432,
									BgL_tailz00_1433, BgL_eqz00_2804));
							{	/* Ieee/pairlist.scm 1004 */

								if ((BgL_tailz00_1433 == BgL_newzd2tailzd2_1434))
									{	/* Ieee/pairlist.scm 1005 */
										return BgL_lisz00_1429;
									}
								else
									{	/* Ieee/pairlist.scm 1005 */
										return
											MAKE_YOUNG_PAIR(BgL_xz00_1432, BgL_newzd2tailzd2_1434);
									}
							}
						}
					}
				}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__r4_pairs_and_lists_6_3z00(void)
	{
		{	/* Ieee/pairlist.scm 18 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string2094z00zz__r4_pairs_and_lists_6_3z00));
			return
				BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string2094z00zz__r4_pairs_and_lists_6_3z00));
		}

	}

#ifdef __cplusplus
}
#endif
