/*===========================================================================*/
/*   (Ieee/vector.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Ieee/vector.scm -indent -o objs/obj_u/Ieee/vector.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___R4_VECTORS_6_8_TYPE_DEFINITIONS
#define BGL___R4_VECTORS_6_8_TYPE_DEFINITIONS
#endif													// BGL___R4_VECTORS_6_8_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_vectorzd2copyz12zc0zz__r4_vectors_6_8z00(obj_t,
		long, obj_t, obj_t, obj_t);
	extern bool_t BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl__vectorzd2fillz12zc0zz__r4_vectors_6_8z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_vectorzd2copy3zd2zz__r4_vectors_6_8z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_vectorzd2mapNz12zc0zz__r4_vectors_6_8z00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62sortz62zz__r4_vectors_6_8z00(obj_t, obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__r4_vectors_6_8z00 = BUNSPEC;
	static obj_t BGl_z62listzd2ze3vectorz53zz__r4_vectors_6_8z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_vectorzd2shrinkz12zc0zz__r4_vectors_6_8z00(obj_t,
		long);
	static obj_t BGl_innerzd2sortzd2zz__r4_vectors_6_8z00(obj_t, obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_EXPORTED_DECL obj_t BGl_sortz00zz__r4_vectors_6_8z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(obj_t);
	BGL_EXPORTED_DECL long BGl_vectorzd2lengthzd2zz__r4_vectors_6_8z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_vectorzd2fillz12zc0zz__r4_vectors_6_8z00(obj_t,
		obj_t, long, long);
	extern obj_t BGl_everyz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62vectorzd2setz12za2zz__r4_vectors_6_8z00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_vectorzd2tagzd2setz12z12zz__r4_vectors_6_8z00(obj_t, int);
	BGL_EXPORTED_DECL obj_t BGl_vectorzd2appendzd2zz__r4_vectors_6_8z00(obj_t,
		obj_t);
	static obj_t BGl_z62vectorzd2tagzd2setz12z70zz__r4_vectors_6_8z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl__makezd2vectorzd2zz__r4_vectors_6_8z00(obj_t, obj_t);
	static obj_t BGl_z62vectorzd2setzd2urz12z70zz__r4_vectors_6_8z00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_vectorzd2refzd2urz00zz__r4_vectors_6_8z00(obj_t,
		long);
	extern long bgl_list_length(obj_t);
	static obj_t BGl_z62vectorzd2mapz12za2zz__r4_vectors_6_8z00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__r4_vectors_6_8z00(void);
	extern obj_t sort_vector(obj_t, obj_t);
	static obj_t BGl_gczd2rootszd2initz00zz__r4_vectors_6_8z00(void);
	BGL_EXPORTED_DECL bool_t BGl_vectorzf3zf3zz__r4_vectors_6_8z00(obj_t);
	static obj_t BGl_z62vectorzf3z91zz__r4_vectors_6_8z00(obj_t, obj_t);
	static obj_t BGl_z62vectorz62zz__r4_vectors_6_8z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_copyzd2vectorzd2zz__r4_vectors_6_8z00(obj_t,
		long);
	static obj_t BGl_z62vectorzd2copyzb0zz__r4_vectors_6_8z00(obj_t, obj_t,
		obj_t);
	static bool_t BGl_vectorzd2forzd2each2z00zz__r4_vectors_6_8z00(obj_t, obj_t);
	BGL_EXPORTED_DECL int BGl_vectorzd2tagzd2zz__r4_vectors_6_8z00(obj_t);
	extern obj_t BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_vectorzd2refzd2zz__r4_vectors_6_8z00(obj_t, long);
	BGL_EXPORTED_DECL obj_t BGl_makezd2vectorzd2zz__r4_vectors_6_8z00(long,
		obj_t);
	static obj_t BGl_z62vectorzd2lengthzb0zz__r4_vectors_6_8z00(obj_t, obj_t);
	static bool_t BGl_vectorzd2forzd2eachNz00zz__r4_vectors_6_8z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62vectorzd2appendzb0zz__r4_vectors_6_8z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31314ze3ze5zz__r4_vectors_6_8z00(obj_t,
		obj_t);
	static obj_t BGl_z62vectorzd2refzd2urz62zz__r4_vectors_6_8z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_vectorzd2mapzd2zz__r4_vectors_6_8z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62vectorzd2tagzb0zz__r4_vectors_6_8z00(obj_t, obj_t);
	extern obj_t make_vector(long, obj_t);
	static obj_t BGl_z62vectorzd2refzb0zz__r4_vectors_6_8z00(obj_t, obj_t, obj_t);
	extern obj_t bgl_fill_vector(obj_t, long, long, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_vectorzd2setzd2urz12z12zz__r4_vectors_6_8z00(obj_t, long, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31235ze3ze5zz__r4_vectors_6_8z00(obj_t,
		obj_t);
	static obj_t BGl__vectorzd2copyz12zc0zz__r4_vectors_6_8z00(obj_t, obj_t);
	static obj_t BGl_z62vectorzd2mapzb0zz__r4_vectors_6_8z00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_vectorzd2forzd2eachz00zz__r4_vectors_6_8z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62vectorzd2forzd2eachz62zz__r4_vectors_6_8z00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_vectorzd2setz12zc0zz__r4_vectors_6_8z00(obj_t,
		long, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_vectorz00zz__r4_vectors_6_8z00(obj_t);
	static obj_t BGl_z62vectorzd2shrinkz12za2zz__r4_vectors_6_8z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_vectorzd2mapz12zc0zz__r4_vectors_6_8z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62vectorzd2ze3listz53zz__r4_vectors_6_8z00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31246ze3ze5zz__r4_vectors_6_8z00(obj_t,
		obj_t);
	static obj_t BGl_z62vectorzd2copy3zb0zz__r4_vectors_6_8z00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_vectorzd2copyzd2zz__r4_vectors_6_8z00(obj_t,
		obj_t);
	static obj_t BGl_z62copyzd2vectorzb0zz__r4_vectors_6_8z00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_vectorzd2mapzd2envz00zz__r4_vectors_6_8z00,
		BgL_bgl_za762vectorza7d2mapza71702za7, va_generic_entry,
		BGl_z62vectorzd2mapzb0zz__r4_vectors_6_8z00, BUNSPEC, -3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vectorzd2shrinkz12zd2envz12zz__r4_vectors_6_8z00,
		BgL_bgl_za762vectorza7d2shri1703z00,
		BGl_z62vectorzd2shrinkz12za2zz__r4_vectors_6_8z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_copyzd2vectorzd2envz00zz__r4_vectors_6_8z00,
		BgL_bgl_za762copyza7d2vector1704z00,
		BGl_z62copyzd2vectorzb0zz__r4_vectors_6_8z00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sortzd2envzd2zz__r4_vectors_6_8z00,
		BgL_bgl_za762sortza762za7za7__r41705z00,
		BGl_z62sortz62zz__r4_vectors_6_8z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vectorzd2fillz12zd2envz12zz__r4_vectors_6_8z00,
		BgL_bgl__vectorza7d2fillza711706z00, opt_generic_entry,
		BGl__vectorzd2fillz12zc0zz__r4_vectors_6_8z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vectorzd2appendzd2envz00zz__r4_vectors_6_8z00,
		BgL_bgl_za762vectorza7d2appe1707z00, va_generic_entry,
		BGl_z62vectorzd2appendzb0zz__r4_vectors_6_8z00, BUNSPEC, -2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vectorzd2ze3listzd2envze3zz__r4_vectors_6_8z00,
		BgL_bgl_za762vectorza7d2za7e3l1708za7,
		BGl_z62vectorzd2ze3listz53zz__r4_vectors_6_8z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_vectorzd2tagzd2envz00zz__r4_vectors_6_8z00,
		BgL_bgl_za762vectorza7d2tagza71709za7,
		BGl_z62vectorzd2tagzb0zz__r4_vectors_6_8z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1700z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1700za700za7za7_1710za7, "&vector-shrink!", 15);
	      DEFINE_STRING(BGl_string1701z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1701za700za7za7_1711za7, "__r4_vectors_6_8", 16);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vectorzd2copy3zd2envz00zz__r4_vectors_6_8z00,
		BgL_bgl_za762vectorza7d2copy1712z00,
		BGl_z62vectorzd2copy3zb0zz__r4_vectors_6_8z00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_vectorzd2envzd2zz__r4_vectors_6_8z00,
		BgL_bgl_za762vectorza762za7za7__1713z00, va_generic_entry,
		BGl_z62vectorz62zz__r4_vectors_6_8z00, BUNSPEC, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vectorzd2refzd2urzd2envzd2zz__r4_vectors_6_8z00,
		BgL_bgl_za762vectorza7d2refza71714za7,
		BGl_z62vectorzd2refzd2urz62zz__r4_vectors_6_8z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vectorzd2mapz12zd2envz12zz__r4_vectors_6_8z00,
		BgL_bgl_za762vectorza7d2mapza71715za7, va_generic_entry,
		BGl_z62vectorzd2mapz12za2zz__r4_vectors_6_8z00, BUNSPEC, -3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_vectorzd2copyzd2envz00zz__r4_vectors_6_8z00,
		BgL_bgl_za762vectorza7d2copy1716z00, va_generic_entry,
		BGl_z62vectorzd2copyzb0zz__r4_vectors_6_8z00, BUNSPEC, -2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_listzd2ze3vectorzd2envze3zz__r4_vectors_6_8z00,
		BgL_bgl_za762listza7d2za7e3vec1717za7,
		BGl_z62listzd2ze3vectorz53zz__r4_vectors_6_8z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_vectorzd2refzd2envz00zz__r4_vectors_6_8z00,
		BgL_bgl_za762vectorza7d2refza71718za7,
		BGl_z62vectorzd2refzb0zz__r4_vectors_6_8z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vectorzd2lengthzd2envz00zz__r4_vectors_6_8z00,
		BgL_bgl_za762vectorza7d2leng1719z00,
		BGl_z62vectorzd2lengthzb0zz__r4_vectors_6_8z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1663z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1663za700za7za7_1720za7,
		"/tmp/bigloo/runtime/Ieee/vector.scm", 35);
	      DEFINE_STRING(BGl_string1664z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1664za700za7za7_1721za7, "_make-vector", 12);
	      DEFINE_STRING(BGl_string1665z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1665za700za7za7_1722za7, "bint", 4);
	      DEFINE_STRING(BGl_string1666z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1666za700za7za7_1723za7, "&vector-length", 14);
	      DEFINE_STRING(BGl_string1667z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1667za700za7za7_1724za7, "vector", 6);
	      DEFINE_STRING(BGl_string1668z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1668za700za7za7_1725za7, "&vector-ref", 11);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vectorzd2tagzd2setz12zd2envzc0zz__r4_vectors_6_8z00,
		BgL_bgl_za762vectorza7d2tagza71726za7,
		BGl_z62vectorzd2tagzd2setz12z70zz__r4_vectors_6_8z00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1669z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1669za700za7za7_1727za7, "&vector-set!", 12);
	      DEFINE_STRING(BGl_string1670z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1670za700za7za7_1728za7, "&vector-ref-ur", 14);
	      DEFINE_STRING(BGl_string1671z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1671za700za7za7_1729za7, "&vector-set-ur!", 15);
	      DEFINE_STRING(BGl_string1672z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1672za700za7za7_1730za7, "&vector->list", 13);
	      DEFINE_STRING(BGl_string1673z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1673za700za7za7_1731za7, "&list->vector", 13);
	      DEFINE_STRING(BGl_string1674z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1674za700za7za7_1732za7, "pair-nil", 8);
	      DEFINE_STRING(BGl_string1675z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1675za700za7za7_1733za7, "_vector-fill!", 13);
	      DEFINE_STRING(BGl_string1676z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1676za700za7za7_1734za7, "vector-fill!", 12);
	      DEFINE_STRING(BGl_string1677z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1677za700za7za7_1735za7, "wrong negative start", 20);
	      DEFINE_STRING(BGl_string1678z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1678za700za7za7_1736za7, "end index too large", 19);
	      DEFINE_STRING(BGl_string1679z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1679za700za7za7_1737za7, "start index larger than end", 27);
	      DEFINE_STRING(BGl_string1680z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1680za700za7za7_1738za7, "&vector-tag", 11);
	      DEFINE_STRING(BGl_string1681z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1681za700za7za7_1739za7, "&vector-tag-set!", 16);
	      DEFINE_STRING(BGl_string1682z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1682za700za7za7_1740za7, "&copy-vector", 12);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_vectorzf3zd2envz21zz__r4_vectors_6_8z00,
		BgL_bgl_za762vectorza7f3za791za71741z00,
		BGl_z62vectorzf3z91zz__r4_vectors_6_8z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1683z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1683za700za7za7_1742za7, "vector-copy", 11);
	      DEFINE_STRING(BGl_string1684z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1684za700za7za7_1743za7, "Illegal indexes", 15);
	      DEFINE_STRING(BGl_string1685z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1685za700za7za7_1744za7, "&vector-copy3", 13);
	      DEFINE_STRING(BGl_string1686z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1686za700za7za7_1745za7, "Illegal argument", 16);
	      DEFINE_STRING(BGl_string1687z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1687za700za7za7_1746za7, "&vector-copy", 12);
	      DEFINE_STRING(BGl_string1688z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1688za700za7za7_1747za7, "_vector-copy!", 13);
	      DEFINE_STRING(BGl_string1689z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1689za700za7za7_1748za7, "&vector-append", 14);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vectorzd2setzd2urz12zd2envzc0zz__r4_vectors_6_8z00,
		BgL_bgl_za762vectorza7d2setza71749za7,
		BGl_z62vectorzd2setzd2urz12z70zz__r4_vectors_6_8z00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string1690z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1690za700za7za7_1750za7, "sort", 4);
	      DEFINE_STRING(BGl_string1691z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1691za700za7za7_1751za7, "Object must be a list or a vector",
		33);
	      DEFINE_STRING(BGl_string1692z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1692za700za7za7_1752za7, "vector-map", 10);
	      DEFINE_STRING(BGl_string1693z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1693za700za7za7_1753za7, "Illegal arguments", 17);
	      DEFINE_STRING(BGl_string1694z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1694za700za7za7_1754za7, "&vector-map", 11);
	      DEFINE_STRING(BGl_string1695z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1695za700za7za7_1755za7, "procedure", 9);
	      DEFINE_STRING(BGl_string1696z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1696za700za7za7_1756za7, "vector-map!", 11);
	      DEFINE_STRING(BGl_string1697z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1697za700za7za7_1757za7, "&vector-map!", 12);
	      DEFINE_STRING(BGl_string1698z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1698za700za7za7_1758za7, "vector-for-each", 15);
	      DEFINE_STRING(BGl_string1699z00zz__r4_vectors_6_8z00,
		BgL_bgl_string1699za700za7za7_1759za7, "&vector-for-each", 16);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vectorzd2copyz12zd2envz12zz__r4_vectors_6_8z00,
		BgL_bgl__vectorza7d2copyza711760z00, opt_generic_entry,
		BGl__vectorzd2copyz12zc0zz__r4_vectors_6_8z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2vectorzd2envz00zz__r4_vectors_6_8z00,
		BgL_bgl__makeza7d2vectorza7d1761z00, opt_generic_entry,
		BGl__makezd2vectorzd2zz__r4_vectors_6_8z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vectorzd2forzd2eachzd2envzd2zz__r4_vectors_6_8z00,
		BgL_bgl_za762vectorza7d2forza71762za7, va_generic_entry,
		BGl_z62vectorzd2forzd2eachz62zz__r4_vectors_6_8z00, BUNSPEC, -3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vectorzd2setz12zd2envz12zz__r4_vectors_6_8z00,
		BgL_bgl_za762vectorza7d2setza71763za7,
		BGl_z62vectorzd2setz12za2zz__r4_vectors_6_8z00, 0L, BUNSPEC, 3);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__r4_vectors_6_8z00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long
		BgL_checksumz00_2042, char *BgL_fromz00_2043)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__r4_vectors_6_8z00))
				{
					BGl_requirezd2initializa7ationz75zz__r4_vectors_6_8z00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__r4_vectors_6_8z00();
					return BGl_importedzd2moduleszd2initz00zz__r4_vectors_6_8z00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__r4_vectors_6_8z00(void)
	{
		{	/* Ieee/vector.scm 18 */
			return bgl_gc_roots_register();
		}

	}



/* vector? */
	BGL_EXPORTED_DEF bool_t BGl_vectorzf3zf3zz__r4_vectors_6_8z00(obj_t
		BgL_objz00_3)
	{
		{	/* Ieee/vector.scm 136 */
			return VECTORP(BgL_objz00_3);
		}

	}



/* &vector? */
	obj_t BGl_z62vectorzf3z91zz__r4_vectors_6_8z00(obj_t BgL_envz00_1862,
		obj_t BgL_objz00_1863)
	{
		{	/* Ieee/vector.scm 136 */
			return BBOOL(BGl_vectorzf3zf3zz__r4_vectors_6_8z00(BgL_objz00_1863));
		}

	}



/* _make-vector */
	obj_t BGl__makezd2vectorzd2zz__r4_vectors_6_8z00(obj_t BgL_env1049z00_7,
		obj_t BgL_opt1048z00_6)
	{
		{	/* Ieee/vector.scm 142 */
			{	/* Ieee/vector.scm 142 */
				obj_t BgL_g1050z00_2038;

				BgL_g1050z00_2038 = VECTOR_REF(BgL_opt1048z00_6, 0L);
				switch (VECTOR_LENGTH(BgL_opt1048z00_6))
					{
					case 1L:

						{	/* Ieee/vector.scm 142 */

							{	/* Ieee/vector.scm 142 */
								long BgL_intz00_2039;

								{	/* Ieee/vector.scm 142 */
									obj_t BgL_tmpz00_2054;

									if (INTEGERP(BgL_g1050z00_2038))
										{	/* Ieee/vector.scm 142 */
											BgL_tmpz00_2054 = BgL_g1050z00_2038;
										}
									else
										{
											obj_t BgL_auxz00_2057;

											BgL_auxz00_2057 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(6640L),
												BGl_string1664z00zz__r4_vectors_6_8z00,
												BGl_string1665z00zz__r4_vectors_6_8z00,
												BgL_g1050z00_2038);
											FAILURE(BgL_auxz00_2057, BFALSE, BFALSE);
										}
									BgL_intz00_2039 = (long) CINT(BgL_tmpz00_2054);
								}
								return make_vector(BgL_intz00_2039, BUNSPEC);
							}
						}
						break;
					case 2L:

						{	/* Ieee/vector.scm 142 */
							obj_t BgL_fillz00_2040;

							BgL_fillz00_2040 = VECTOR_REF(BgL_opt1048z00_6, 1L);
							{	/* Ieee/vector.scm 142 */

								{	/* Ieee/vector.scm 142 */
									long BgL_intz00_2041;

									{	/* Ieee/vector.scm 142 */
										obj_t BgL_tmpz00_2064;

										if (INTEGERP(BgL_g1050z00_2038))
											{	/* Ieee/vector.scm 142 */
												BgL_tmpz00_2064 = BgL_g1050z00_2038;
											}
										else
											{
												obj_t BgL_auxz00_2067;

												BgL_auxz00_2067 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(6640L),
													BGl_string1664z00zz__r4_vectors_6_8z00,
													BGl_string1665z00zz__r4_vectors_6_8z00,
													BgL_g1050z00_2038);
												FAILURE(BgL_auxz00_2067, BFALSE, BFALSE);
											}
										BgL_intz00_2041 = (long) CINT(BgL_tmpz00_2064);
									}
									return make_vector(BgL_intz00_2041, BgL_fillz00_2040);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* make-vector */
	BGL_EXPORTED_DEF obj_t BGl_makezd2vectorzd2zz__r4_vectors_6_8z00(long
		BgL_intz00_4, obj_t BgL_fillz00_5)
	{
		{	/* Ieee/vector.scm 142 */
			BGL_TAIL return make_vector(BgL_intz00_4, BgL_fillz00_5);
		}

	}



/* vector */
	BGL_EXPORTED_DEF obj_t BGl_vectorz00zz__r4_vectors_6_8z00(obj_t BgL_argsz00_8)
	{
		{	/* Ieee/vector.scm 148 */
			return BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(BgL_argsz00_8);
		}

	}



/* &vector */
	obj_t BGl_z62vectorz62zz__r4_vectors_6_8z00(obj_t BgL_envz00_1864,
		obj_t BgL_argsz00_1865)
	{
		{	/* Ieee/vector.scm 148 */
			return BGl_vectorz00zz__r4_vectors_6_8z00(BgL_argsz00_1865);
		}

	}



/* vector-length */
	BGL_EXPORTED_DEF long BGl_vectorzd2lengthzd2zz__r4_vectors_6_8z00(obj_t
		BgL_vectorz00_9)
	{
		{	/* Ieee/vector.scm 154 */
			return VECTOR_LENGTH(BgL_vectorz00_9);
		}

	}



/* &vector-length */
	obj_t BGl_z62vectorzd2lengthzb0zz__r4_vectors_6_8z00(obj_t BgL_envz00_1866,
		obj_t BgL_vectorz00_1867)
	{
		{	/* Ieee/vector.scm 154 */
			{	/* Ieee/vector.scm 155 */
				long BgL_tmpz00_2079;

				{	/* Ieee/vector.scm 155 */
					obj_t BgL_auxz00_2080;

					if (VECTORP(BgL_vectorz00_1867))
						{	/* Ieee/vector.scm 155 */
							BgL_auxz00_2080 = BgL_vectorz00_1867;
						}
					else
						{
							obj_t BgL_auxz00_2083;

							BgL_auxz00_2083 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(7274L),
								BGl_string1666z00zz__r4_vectors_6_8z00,
								BGl_string1667z00zz__r4_vectors_6_8z00, BgL_vectorz00_1867);
							FAILURE(BgL_auxz00_2083, BFALSE, BFALSE);
						}
					BgL_tmpz00_2079 =
						BGl_vectorzd2lengthzd2zz__r4_vectors_6_8z00(BgL_auxz00_2080);
				}
				return BINT(BgL_tmpz00_2079);
			}
		}

	}



/* vector-ref */
	BGL_EXPORTED_DEF obj_t BGl_vectorzd2refzd2zz__r4_vectors_6_8z00(obj_t
		BgL_vectorz00_10, long BgL_kz00_11)
	{
		{	/* Ieee/vector.scm 160 */
			return VECTOR_REF(BgL_vectorz00_10, BgL_kz00_11);
		}

	}



/* &vector-ref */
	obj_t BGl_z62vectorzd2refzb0zz__r4_vectors_6_8z00(obj_t BgL_envz00_1868,
		obj_t BgL_vectorz00_1869, obj_t BgL_kz00_1870)
	{
		{	/* Ieee/vector.scm 160 */
			{	/* Ieee/vector.scm 161 */
				long BgL_auxz00_2097;
				obj_t BgL_auxz00_2090;

				{	/* Ieee/vector.scm 161 */
					obj_t BgL_tmpz00_2098;

					if (INTEGERP(BgL_kz00_1870))
						{	/* Ieee/vector.scm 161 */
							BgL_tmpz00_2098 = BgL_kz00_1870;
						}
					else
						{
							obj_t BgL_auxz00_2101;

							BgL_auxz00_2101 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(7562L),
								BGl_string1668z00zz__r4_vectors_6_8z00,
								BGl_string1665z00zz__r4_vectors_6_8z00, BgL_kz00_1870);
							FAILURE(BgL_auxz00_2101, BFALSE, BFALSE);
						}
					BgL_auxz00_2097 = (long) CINT(BgL_tmpz00_2098);
				}
				if (VECTORP(BgL_vectorz00_1869))
					{	/* Ieee/vector.scm 161 */
						BgL_auxz00_2090 = BgL_vectorz00_1869;
					}
				else
					{
						obj_t BgL_auxz00_2093;

						BgL_auxz00_2093 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(7562L),
							BGl_string1668z00zz__r4_vectors_6_8z00,
							BGl_string1667z00zz__r4_vectors_6_8z00, BgL_vectorz00_1869);
						FAILURE(BgL_auxz00_2093, BFALSE, BFALSE);
					}
				return
					BGl_vectorzd2refzd2zz__r4_vectors_6_8z00(BgL_auxz00_2090,
					BgL_auxz00_2097);
			}
		}

	}



/* vector-set! */
	BGL_EXPORTED_DEF obj_t BGl_vectorzd2setz12zc0zz__r4_vectors_6_8z00(obj_t
		BgL_vectorz00_12, long BgL_kz00_13, obj_t BgL_objz00_14)
	{
		{	/* Ieee/vector.scm 166 */
			return VECTOR_SET(BgL_vectorz00_12, BgL_kz00_13, BgL_objz00_14);
		}

	}



/* &vector-set! */
	obj_t BGl_z62vectorzd2setz12za2zz__r4_vectors_6_8z00(obj_t BgL_envz00_1871,
		obj_t BgL_vectorz00_1872, obj_t BgL_kz00_1873, obj_t BgL_objz00_1874)
	{
		{	/* Ieee/vector.scm 166 */
			{	/* Ieee/vector.scm 167 */
				long BgL_auxz00_2115;
				obj_t BgL_auxz00_2108;

				{	/* Ieee/vector.scm 167 */
					obj_t BgL_tmpz00_2116;

					if (INTEGERP(BgL_kz00_1873))
						{	/* Ieee/vector.scm 167 */
							BgL_tmpz00_2116 = BgL_kz00_1873;
						}
					else
						{
							obj_t BgL_auxz00_2119;

							BgL_auxz00_2119 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(7854L),
								BGl_string1669z00zz__r4_vectors_6_8z00,
								BGl_string1665z00zz__r4_vectors_6_8z00, BgL_kz00_1873);
							FAILURE(BgL_auxz00_2119, BFALSE, BFALSE);
						}
					BgL_auxz00_2115 = (long) CINT(BgL_tmpz00_2116);
				}
				if (VECTORP(BgL_vectorz00_1872))
					{	/* Ieee/vector.scm 167 */
						BgL_auxz00_2108 = BgL_vectorz00_1872;
					}
				else
					{
						obj_t BgL_auxz00_2111;

						BgL_auxz00_2111 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(7854L),
							BGl_string1669z00zz__r4_vectors_6_8z00,
							BGl_string1667z00zz__r4_vectors_6_8z00, BgL_vectorz00_1872);
						FAILURE(BgL_auxz00_2111, BFALSE, BFALSE);
					}
				return
					BGl_vectorzd2setz12zc0zz__r4_vectors_6_8z00(BgL_auxz00_2108,
					BgL_auxz00_2115, BgL_objz00_1874);
			}
		}

	}



/* vector-ref-ur */
	BGL_EXPORTED_DEF obj_t BGl_vectorzd2refzd2urz00zz__r4_vectors_6_8z00(obj_t
		BgL_vectorz00_15, long BgL_kz00_16)
	{
		{	/* Ieee/vector.scm 172 */
			return VECTOR_REF(BgL_vectorz00_15, BgL_kz00_16);
		}

	}



/* &vector-ref-ur */
	obj_t BGl_z62vectorzd2refzd2urz62zz__r4_vectors_6_8z00(obj_t BgL_envz00_1875,
		obj_t BgL_vectorz00_1876, obj_t BgL_kz00_1877)
	{
		{	/* Ieee/vector.scm 172 */
			{	/* Ieee/vector.scm 173 */
				long BgL_auxz00_2133;
				obj_t BgL_auxz00_2126;

				{	/* Ieee/vector.scm 173 */
					obj_t BgL_tmpz00_2134;

					if (INTEGERP(BgL_kz00_1877))
						{	/* Ieee/vector.scm 173 */
							BgL_tmpz00_2134 = BgL_kz00_1877;
						}
					else
						{
							obj_t BgL_auxz00_2137;

							BgL_auxz00_2137 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(8149L),
								BGl_string1670z00zz__r4_vectors_6_8z00,
								BGl_string1665z00zz__r4_vectors_6_8z00, BgL_kz00_1877);
							FAILURE(BgL_auxz00_2137, BFALSE, BFALSE);
						}
					BgL_auxz00_2133 = (long) CINT(BgL_tmpz00_2134);
				}
				if (VECTORP(BgL_vectorz00_1876))
					{	/* Ieee/vector.scm 173 */
						BgL_auxz00_2126 = BgL_vectorz00_1876;
					}
				else
					{
						obj_t BgL_auxz00_2129;

						BgL_auxz00_2129 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(8149L),
							BGl_string1670z00zz__r4_vectors_6_8z00,
							BGl_string1667z00zz__r4_vectors_6_8z00, BgL_vectorz00_1876);
						FAILURE(BgL_auxz00_2129, BFALSE, BFALSE);
					}
				return
					BGl_vectorzd2refzd2urz00zz__r4_vectors_6_8z00(BgL_auxz00_2126,
					BgL_auxz00_2133);
			}
		}

	}



/* vector-set-ur! */
	BGL_EXPORTED_DEF obj_t BGl_vectorzd2setzd2urz12z12zz__r4_vectors_6_8z00(obj_t
		BgL_vectorz00_17, long BgL_kz00_18, obj_t BgL_objz00_19)
	{
		{	/* Ieee/vector.scm 178 */
			return VECTOR_SET(BgL_vectorz00_17, BgL_kz00_18, BgL_objz00_19);
		}

	}



/* &vector-set-ur! */
	obj_t BGl_z62vectorzd2setzd2urz12z70zz__r4_vectors_6_8z00(obj_t
		BgL_envz00_1878, obj_t BgL_vectorz00_1879, obj_t BgL_kz00_1880,
		obj_t BgL_objz00_1881)
	{
		{	/* Ieee/vector.scm 178 */
			{	/* Ieee/vector.scm 179 */
				long BgL_auxz00_2151;
				obj_t BgL_auxz00_2144;

				{	/* Ieee/vector.scm 179 */
					obj_t BgL_tmpz00_2152;

					if (INTEGERP(BgL_kz00_1880))
						{	/* Ieee/vector.scm 179 */
							BgL_tmpz00_2152 = BgL_kz00_1880;
						}
					else
						{
							obj_t BgL_auxz00_2155;

							BgL_auxz00_2155 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(8447L),
								BGl_string1671z00zz__r4_vectors_6_8z00,
								BGl_string1665z00zz__r4_vectors_6_8z00, BgL_kz00_1880);
							FAILURE(BgL_auxz00_2155, BFALSE, BFALSE);
						}
					BgL_auxz00_2151 = (long) CINT(BgL_tmpz00_2152);
				}
				if (VECTORP(BgL_vectorz00_1879))
					{	/* Ieee/vector.scm 179 */
						BgL_auxz00_2144 = BgL_vectorz00_1879;
					}
				else
					{
						obj_t BgL_auxz00_2147;

						BgL_auxz00_2147 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(8447L),
							BGl_string1671z00zz__r4_vectors_6_8z00,
							BGl_string1667z00zz__r4_vectors_6_8z00, BgL_vectorz00_1879);
						FAILURE(BgL_auxz00_2147, BFALSE, BFALSE);
					}
				return
					BGl_vectorzd2setzd2urz12z12zz__r4_vectors_6_8z00(BgL_auxz00_2144,
					BgL_auxz00_2151, BgL_objz00_1881);
			}
		}

	}



/* vector->list */
	BGL_EXPORTED_DEF obj_t BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00(obj_t
		BgL_vectorz00_20)
	{
		{	/* Ieee/vector.scm 184 */
			if ((VECTOR_LENGTH(BgL_vectorz00_20) == 0L))
				{	/* Ieee/vector.scm 186 */
					return BNIL;
				}
			else
				{	/* Ieee/vector.scm 188 */
					long BgL_g1014z00_757;

					BgL_g1014z00_757 = (VECTOR_LENGTH(BgL_vectorz00_20) - 1L);
					{
						long BgL_iz00_1314;
						obj_t BgL_accz00_1315;

						BgL_iz00_1314 = BgL_g1014z00_757;
						BgL_accz00_1315 = BNIL;
					BgL_loopz00_1313:
						if ((BgL_iz00_1314 == 0L))
							{	/* Ieee/vector.scm 190 */
								return
									MAKE_YOUNG_PAIR(VECTOR_REF(BgL_vectorz00_20, BgL_iz00_1314),
									BgL_accz00_1315);
							}
						else
							{	/* Ieee/vector.scm 192 */
								long BgL_arg1090z00_1323;
								obj_t BgL_arg1092z00_1324;

								BgL_arg1090z00_1323 = (BgL_iz00_1314 - 1L);
								BgL_arg1092z00_1324 =
									MAKE_YOUNG_PAIR(VECTOR_REF(BgL_vectorz00_20, BgL_iz00_1314),
									BgL_accz00_1315);
								{
									obj_t BgL_accz00_2174;
									long BgL_iz00_2173;

									BgL_iz00_2173 = BgL_arg1090z00_1323;
									BgL_accz00_2174 = BgL_arg1092z00_1324;
									BgL_accz00_1315 = BgL_accz00_2174;
									BgL_iz00_1314 = BgL_iz00_2173;
									goto BgL_loopz00_1313;
								}
							}
					}
				}
		}

	}



/* &vector->list */
	obj_t BGl_z62vectorzd2ze3listz53zz__r4_vectors_6_8z00(obj_t BgL_envz00_1882,
		obj_t BgL_vectorz00_1883)
	{
		{	/* Ieee/vector.scm 184 */
			{	/* Ieee/vector.scm 185 */
				obj_t BgL_auxz00_2175;

				if (VECTORP(BgL_vectorz00_1883))
					{	/* Ieee/vector.scm 185 */
						BgL_auxz00_2175 = BgL_vectorz00_1883;
					}
				else
					{
						obj_t BgL_auxz00_2178;

						BgL_auxz00_2178 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(8735L),
							BGl_string1672z00zz__r4_vectors_6_8z00,
							BGl_string1667z00zz__r4_vectors_6_8z00, BgL_vectorz00_1883);
						FAILURE(BgL_auxz00_2178, BFALSE, BFALSE);
					}
				return BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00(BgL_auxz00_2175);
			}
		}

	}



/* list->vector */
	BGL_EXPORTED_DEF obj_t BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(obj_t
		BgL_listz00_21)
	{
		{	/* Ieee/vector.scm 197 */
			{	/* Ieee/vector.scm 198 */
				long BgL_lenz00_769;

				BgL_lenz00_769 = bgl_list_length(BgL_listz00_21);
				{	/* Ieee/vector.scm 198 */
					obj_t BgL_vecz00_770;

					BgL_vecz00_770 = create_vector(BgL_lenz00_769);
					{	/* Ieee/vector.scm 199 */

						{
							long BgL_iz00_1355;
							obj_t BgL_lz00_1356;

							BgL_iz00_1355 = 0L;
							BgL_lz00_1356 = BgL_listz00_21;
						BgL_loopz00_1354:
							if ((BgL_iz00_1355 == BgL_lenz00_769))
								{	/* Ieee/vector.scm 202 */
									return BgL_vecz00_770;
								}
							else
								{	/* Ieee/vector.scm 202 */
									{	/* Ieee/vector.scm 205 */
										obj_t BgL_arg1102z00_1362;

										BgL_arg1102z00_1362 = CAR(((obj_t) BgL_lz00_1356));
										VECTOR_SET(BgL_vecz00_770, BgL_iz00_1355,
											BgL_arg1102z00_1362);
									}
									{	/* Ieee/vector.scm 206 */
										long BgL_arg1103z00_1363;
										obj_t BgL_arg1104z00_1364;

										BgL_arg1103z00_1363 = (BgL_iz00_1355 + 1L);
										BgL_arg1104z00_1364 = CDR(((obj_t) BgL_lz00_1356));
										{
											obj_t BgL_lz00_2194;
											long BgL_iz00_2193;

											BgL_iz00_2193 = BgL_arg1103z00_1363;
											BgL_lz00_2194 = BgL_arg1104z00_1364;
											BgL_lz00_1356 = BgL_lz00_2194;
											BgL_iz00_1355 = BgL_iz00_2193;
											goto BgL_loopz00_1354;
										}
									}
								}
						}
					}
				}
			}
		}

	}



/* &list->vector */
	obj_t BGl_z62listzd2ze3vectorz53zz__r4_vectors_6_8z00(obj_t BgL_envz00_1884,
		obj_t BgL_listz00_1885)
	{
		{	/* Ieee/vector.scm 197 */
			{	/* Ieee/vector.scm 198 */
				obj_t BgL_auxz00_2195;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_listz00_1885))
					{	/* Ieee/vector.scm 198 */
						BgL_auxz00_2195 = BgL_listz00_1885;
					}
				else
					{
						obj_t BgL_auxz00_2198;

						BgL_auxz00_2198 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(9225L),
							BGl_string1673z00zz__r4_vectors_6_8z00,
							BGl_string1674z00zz__r4_vectors_6_8z00, BgL_listz00_1885);
						FAILURE(BgL_auxz00_2198, BFALSE, BFALSE);
					}
				return BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(BgL_auxz00_2195);
			}
		}

	}



/* _vector-fill! */
	obj_t BGl__vectorzd2fillz12zc0zz__r4_vectors_6_8z00(obj_t BgL_env1054z00_27,
		obj_t BgL_opt1053z00_26)
	{
		{	/* Ieee/vector.scm 211 */
			{	/* Ieee/vector.scm 211 */
				obj_t BgL_vecz00_780;
				obj_t BgL_fillz00_781;

				BgL_vecz00_780 = VECTOR_REF(BgL_opt1053z00_26, 0L);
				BgL_fillz00_781 = VECTOR_REF(BgL_opt1053z00_26, 1L);
				switch (VECTOR_LENGTH(BgL_opt1053z00_26))
					{
					case 2L:

						{	/* Ieee/vector.scm 212 */
							long BgL_endz00_785;

							{	/* Ieee/vector.scm 212 */
								obj_t BgL_vectorz00_1372;

								if (VECTORP(BgL_vecz00_780))
									{	/* Ieee/vector.scm 212 */
										BgL_vectorz00_1372 = BgL_vecz00_780;
									}
								else
									{
										obj_t BgL_auxz00_2207;

										BgL_auxz00_2207 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(9736L),
											BGl_string1675z00zz__r4_vectors_6_8z00,
											BGl_string1667z00zz__r4_vectors_6_8z00, BgL_vecz00_780);
										FAILURE(BgL_auxz00_2207, BFALSE, BFALSE);
									}
								BgL_endz00_785 = VECTOR_LENGTH(BgL_vectorz00_1372);
							}
							{	/* Ieee/vector.scm 211 */

								{	/* Ieee/vector.scm 211 */
									obj_t BgL_auxz00_2212;

									if (VECTORP(BgL_vecz00_780))
										{	/* Ieee/vector.scm 211 */
											BgL_auxz00_2212 = BgL_vecz00_780;
										}
									else
										{
											obj_t BgL_auxz00_2215;

											BgL_auxz00_2215 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(9648L),
												BGl_string1675z00zz__r4_vectors_6_8z00,
												BGl_string1667z00zz__r4_vectors_6_8z00, BgL_vecz00_780);
											FAILURE(BgL_auxz00_2215, BFALSE, BFALSE);
										}
									return
										BGl_vectorzd2fillz12zc0zz__r4_vectors_6_8z00
										(BgL_auxz00_2212, BgL_fillz00_781, 0L, BgL_endz00_785);
								}
							}
						}
						break;
					case 3L:

						{	/* Ieee/vector.scm 211 */
							obj_t BgL_startz00_786;

							BgL_startz00_786 = VECTOR_REF(BgL_opt1053z00_26, 2L);
							{	/* Ieee/vector.scm 212 */
								long BgL_endz00_787;

								{	/* Ieee/vector.scm 212 */
									obj_t BgL_vectorz00_1373;

									if (VECTORP(BgL_vecz00_780))
										{	/* Ieee/vector.scm 212 */
											BgL_vectorz00_1373 = BgL_vecz00_780;
										}
									else
										{
											obj_t BgL_auxz00_2223;

											BgL_auxz00_2223 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(9736L),
												BGl_string1675z00zz__r4_vectors_6_8z00,
												BGl_string1667z00zz__r4_vectors_6_8z00, BgL_vecz00_780);
											FAILURE(BgL_auxz00_2223, BFALSE, BFALSE);
										}
									BgL_endz00_787 = VECTOR_LENGTH(BgL_vectorz00_1373);
								}
								{	/* Ieee/vector.scm 211 */

									{	/* Ieee/vector.scm 211 */
										long BgL_auxz00_2235;
										obj_t BgL_auxz00_2228;

										{	/* Ieee/vector.scm 211 */
											obj_t BgL_tmpz00_2236;

											if (INTEGERP(BgL_startz00_786))
												{	/* Ieee/vector.scm 211 */
													BgL_tmpz00_2236 = BgL_startz00_786;
												}
											else
												{
													obj_t BgL_auxz00_2239;

													BgL_auxz00_2239 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string1663z00zz__r4_vectors_6_8z00,
														BINT(9648L), BGl_string1675z00zz__r4_vectors_6_8z00,
														BGl_string1665z00zz__r4_vectors_6_8z00,
														BgL_startz00_786);
													FAILURE(BgL_auxz00_2239, BFALSE, BFALSE);
												}
											BgL_auxz00_2235 = (long) CINT(BgL_tmpz00_2236);
										}
										if (VECTORP(BgL_vecz00_780))
											{	/* Ieee/vector.scm 211 */
												BgL_auxz00_2228 = BgL_vecz00_780;
											}
										else
											{
												obj_t BgL_auxz00_2231;

												BgL_auxz00_2231 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(9648L),
													BGl_string1675z00zz__r4_vectors_6_8z00,
													BGl_string1667z00zz__r4_vectors_6_8z00,
													BgL_vecz00_780);
												FAILURE(BgL_auxz00_2231, BFALSE, BFALSE);
											}
										return
											BGl_vectorzd2fillz12zc0zz__r4_vectors_6_8z00
											(BgL_auxz00_2228, BgL_fillz00_781, BgL_auxz00_2235,
											BgL_endz00_787);
									}
								}
							}
						}
						break;
					case 4L:

						{	/* Ieee/vector.scm 211 */
							obj_t BgL_startz00_788;

							BgL_startz00_788 = VECTOR_REF(BgL_opt1053z00_26, 2L);
							{	/* Ieee/vector.scm 211 */
								obj_t BgL_endz00_789;

								BgL_endz00_789 = VECTOR_REF(BgL_opt1053z00_26, 3L);
								{	/* Ieee/vector.scm 211 */

									{	/* Ieee/vector.scm 211 */
										long BgL_auxz00_2263;
										long BgL_auxz00_2254;
										obj_t BgL_auxz00_2247;

										{	/* Ieee/vector.scm 211 */
											obj_t BgL_tmpz00_2264;

											if (INTEGERP(BgL_endz00_789))
												{	/* Ieee/vector.scm 211 */
													BgL_tmpz00_2264 = BgL_endz00_789;
												}
											else
												{
													obj_t BgL_auxz00_2267;

													BgL_auxz00_2267 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string1663z00zz__r4_vectors_6_8z00,
														BINT(9648L), BGl_string1675z00zz__r4_vectors_6_8z00,
														BGl_string1665z00zz__r4_vectors_6_8z00,
														BgL_endz00_789);
													FAILURE(BgL_auxz00_2267, BFALSE, BFALSE);
												}
											BgL_auxz00_2263 = (long) CINT(BgL_tmpz00_2264);
										}
										{	/* Ieee/vector.scm 211 */
											obj_t BgL_tmpz00_2255;

											if (INTEGERP(BgL_startz00_788))
												{	/* Ieee/vector.scm 211 */
													BgL_tmpz00_2255 = BgL_startz00_788;
												}
											else
												{
													obj_t BgL_auxz00_2258;

													BgL_auxz00_2258 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string1663z00zz__r4_vectors_6_8z00,
														BINT(9648L), BGl_string1675z00zz__r4_vectors_6_8z00,
														BGl_string1665z00zz__r4_vectors_6_8z00,
														BgL_startz00_788);
													FAILURE(BgL_auxz00_2258, BFALSE, BFALSE);
												}
											BgL_auxz00_2254 = (long) CINT(BgL_tmpz00_2255);
										}
										if (VECTORP(BgL_vecz00_780))
											{	/* Ieee/vector.scm 211 */
												BgL_auxz00_2247 = BgL_vecz00_780;
											}
										else
											{
												obj_t BgL_auxz00_2250;

												BgL_auxz00_2250 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(9648L),
													BGl_string1675z00zz__r4_vectors_6_8z00,
													BGl_string1667z00zz__r4_vectors_6_8z00,
													BgL_vecz00_780);
												FAILURE(BgL_auxz00_2250, BFALSE, BFALSE);
											}
										return
											BGl_vectorzd2fillz12zc0zz__r4_vectors_6_8z00
											(BgL_auxz00_2247, BgL_fillz00_781, BgL_auxz00_2254,
											BgL_auxz00_2263);
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* vector-fill! */
	BGL_EXPORTED_DEF obj_t BGl_vectorzd2fillz12zc0zz__r4_vectors_6_8z00(obj_t
		BgL_vecz00_22, obj_t BgL_fillz00_23, long BgL_startz00_24,
		long BgL_endz00_25)
	{
		{	/* Ieee/vector.scm 211 */
			if ((BgL_startz00_24 < 0L))
				{	/* Ieee/vector.scm 214 */
					return
						BGl_errorz00zz__errorz00(BGl_string1676z00zz__r4_vectors_6_8z00,
						BGl_string1677z00zz__r4_vectors_6_8z00, BINT(BgL_startz00_24));
				}
			else
				{	/* Ieee/vector.scm 214 */
					if ((BgL_endz00_25 > VECTOR_LENGTH(BgL_vecz00_22)))
						{	/* Ieee/vector.scm 216 */
							return
								BGl_errorz00zz__errorz00(BGl_string1676z00zz__r4_vectors_6_8z00,
								BGl_string1678z00zz__r4_vectors_6_8z00, BINT(BgL_endz00_25));
						}
					else
						{	/* Ieee/vector.scm 216 */
							if ((BgL_startz00_24 >= BgL_endz00_25))
								{	/* Ieee/vector.scm 219 */
									bool_t BgL_test1792z00_2286;

									if ((BgL_startz00_24 == BgL_endz00_25))
										{	/* Ieee/vector.scm 219 */
											BgL_test1792z00_2286 = (BgL_startz00_24 == 0L);
										}
									else
										{	/* Ieee/vector.scm 219 */
											BgL_test1792z00_2286 = ((bool_t) 0);
										}
									if (BgL_test1792z00_2286)
										{	/* Ieee/vector.scm 219 */
											return BUNSPEC;
										}
									else
										{	/* Ieee/vector.scm 222 */
											obj_t BgL_arg1122z00_796;

											BgL_arg1122z00_796 =
												MAKE_YOUNG_PAIR(BINT(BgL_startz00_24),
												BINT(BgL_endz00_25));
											return
												BGl_errorz00zz__errorz00
												(BGl_string1676z00zz__r4_vectors_6_8z00,
												BGl_string1679z00zz__r4_vectors_6_8z00,
												BgL_arg1122z00_796);
										}
								}
							else
								{	/* Ieee/vector.scm 218 */
									return
										bgl_fill_vector(BgL_vecz00_22, BgL_startz00_24,
										BgL_endz00_25, BgL_fillz00_23);
								}
						}
				}
		}

	}



/* vector-tag */
	BGL_EXPORTED_DEF int BGl_vectorzd2tagzd2zz__r4_vectors_6_8z00(obj_t
		BgL_vectorz00_28)
	{
		{	/* Ieee/vector.scm 229 */
			return VECTOR_TAG(BgL_vectorz00_28);
		}

	}



/* &vector-tag */
	obj_t BGl_z62vectorzd2tagzb0zz__r4_vectors_6_8z00(obj_t BgL_envz00_1886,
		obj_t BgL_vectorz00_1887)
	{
		{	/* Ieee/vector.scm 229 */
			{	/* Ieee/vector.scm 230 */
				int BgL_tmpz00_2296;

				{	/* Ieee/vector.scm 230 */
					obj_t BgL_auxz00_2297;

					if (VECTORP(BgL_vectorz00_1887))
						{	/* Ieee/vector.scm 230 */
							BgL_auxz00_2297 = BgL_vectorz00_1887;
						}
					else
						{
							obj_t BgL_auxz00_2300;

							BgL_auxz00_2300 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(10416L),
								BGl_string1680z00zz__r4_vectors_6_8z00,
								BGl_string1667z00zz__r4_vectors_6_8z00, BgL_vectorz00_1887);
							FAILURE(BgL_auxz00_2300, BFALSE, BFALSE);
						}
					BgL_tmpz00_2296 =
						BGl_vectorzd2tagzd2zz__r4_vectors_6_8z00(BgL_auxz00_2297);
				}
				return BINT(BgL_tmpz00_2296);
			}
		}

	}



/* vector-tag-set! */
	BGL_EXPORTED_DEF obj_t BGl_vectorzd2tagzd2setz12z12zz__r4_vectors_6_8z00(obj_t
		BgL_vectorz00_29, int BgL_tagz00_30)
	{
		{	/* Ieee/vector.scm 235 */
			return VECTOR_TAG_SET(BgL_vectorz00_29, BgL_tagz00_30);
		}

	}



/* &vector-tag-set! */
	obj_t BGl_z62vectorzd2tagzd2setz12z70zz__r4_vectors_6_8z00(obj_t
		BgL_envz00_1888, obj_t BgL_vectorz00_1889, obj_t BgL_tagz00_1890)
	{
		{	/* Ieee/vector.scm 235 */
			{	/* Ieee/vector.scm 236 */
				int BgL_auxz00_2314;
				obj_t BgL_auxz00_2307;

				{	/* Ieee/vector.scm 236 */
					obj_t BgL_tmpz00_2315;

					if (INTEGERP(BgL_tagz00_1890))
						{	/* Ieee/vector.scm 236 */
							BgL_tmpz00_2315 = BgL_tagz00_1890;
						}
					else
						{
							obj_t BgL_auxz00_2318;

							BgL_auxz00_2318 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(10708L),
								BGl_string1681z00zz__r4_vectors_6_8z00,
								BGl_string1665z00zz__r4_vectors_6_8z00, BgL_tagz00_1890);
							FAILURE(BgL_auxz00_2318, BFALSE, BFALSE);
						}
					BgL_auxz00_2314 = CINT(BgL_tmpz00_2315);
				}
				if (VECTORP(BgL_vectorz00_1889))
					{	/* Ieee/vector.scm 236 */
						BgL_auxz00_2307 = BgL_vectorz00_1889;
					}
				else
					{
						obj_t BgL_auxz00_2310;

						BgL_auxz00_2310 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(10708L),
							BGl_string1681z00zz__r4_vectors_6_8z00,
							BGl_string1667z00zz__r4_vectors_6_8z00, BgL_vectorz00_1889);
						FAILURE(BgL_auxz00_2310, BFALSE, BFALSE);
					}
				return
					BGl_vectorzd2tagzd2setz12z12zz__r4_vectors_6_8z00(BgL_auxz00_2307,
					BgL_auxz00_2314);
			}
		}

	}



/* copy-vector */
	BGL_EXPORTED_DEF obj_t BGl_copyzd2vectorzd2zz__r4_vectors_6_8z00(obj_t
		BgL_oldzd2veczd2_31, long BgL_newzd2lenzd2_32)
	{
		{	/* Ieee/vector.scm 241 */
			{	/* Ieee/vector.scm 242 */
				obj_t BgL_newzd2veczd2_1384;

				BgL_newzd2veczd2_1384 = make_vector(BgL_newzd2lenzd2_32, BUNSPEC);
				{	/* Ieee/vector.scm 243 */
					long BgL_minz00_1386;

					if ((BgL_newzd2lenzd2_32 < VECTOR_LENGTH(BgL_oldzd2veczd2_31)))
						{	/* Ieee/vector.scm 244 */
							BgL_minz00_1386 = BgL_newzd2lenzd2_32;
						}
					else
						{	/* Ieee/vector.scm 244 */
							BgL_minz00_1386 = VECTOR_LENGTH(BgL_oldzd2veczd2_31);
						}
					{	/* Ieee/vector.scm 244 */

						return
							BGL_VECTOR_BLIT_NO_OVERLAP(BgL_newzd2veczd2_1384,
							BgL_oldzd2veczd2_31, 0L, 0L, BgL_minz00_1386);
					}
				}
			}
		}

	}



/* &copy-vector */
	obj_t BGl_z62copyzd2vectorzb0zz__r4_vectors_6_8z00(obj_t BgL_envz00_1891,
		obj_t BgL_oldzd2veczd2_1892, obj_t BgL_newzd2lenzd2_1893)
	{
		{	/* Ieee/vector.scm 241 */
			{	/* Ieee/vector.scm 242 */
				long BgL_auxz00_2337;
				obj_t BgL_auxz00_2330;

				{	/* Ieee/vector.scm 242 */
					obj_t BgL_tmpz00_2338;

					if (INTEGERP(BgL_newzd2lenzd2_1893))
						{	/* Ieee/vector.scm 242 */
							BgL_tmpz00_2338 = BgL_newzd2lenzd2_1893;
						}
					else
						{
							obj_t BgL_auxz00_2341;

							BgL_auxz00_2341 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(11025L),
								BGl_string1682z00zz__r4_vectors_6_8z00,
								BGl_string1665z00zz__r4_vectors_6_8z00, BgL_newzd2lenzd2_1893);
							FAILURE(BgL_auxz00_2341, BFALSE, BFALSE);
						}
					BgL_auxz00_2337 = (long) CINT(BgL_tmpz00_2338);
				}
				if (VECTORP(BgL_oldzd2veczd2_1892))
					{	/* Ieee/vector.scm 242 */
						BgL_auxz00_2330 = BgL_oldzd2veczd2_1892;
					}
				else
					{
						obj_t BgL_auxz00_2333;

						BgL_auxz00_2333 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(11025L),
							BGl_string1682z00zz__r4_vectors_6_8z00,
							BGl_string1667z00zz__r4_vectors_6_8z00, BgL_oldzd2veczd2_1892);
						FAILURE(BgL_auxz00_2333, BFALSE, BFALSE);
					}
				return
					BGl_copyzd2vectorzd2zz__r4_vectors_6_8z00(BgL_auxz00_2330,
					BgL_auxz00_2337);
			}
		}

	}



/* vector-copy3 */
	BGL_EXPORTED_DEF obj_t BGl_vectorzd2copy3zd2zz__r4_vectors_6_8z00(obj_t
		BgL_oldzd2veczd2_33, obj_t BgL_startz00_34, obj_t BgL_stopz00_35)
	{
		{	/* Ieee/vector.scm 261 */
			{	/* Ieee/vector.scm 262 */
				long BgL_newzd2lenzd2_805;

				BgL_newzd2lenzd2_805 =
					((long) CINT(BgL_stopz00_35) - (long) CINT(BgL_startz00_34));
				{	/* Ieee/vector.scm 263 */
					obj_t BgL_newzd2veczd2_806;

					BgL_newzd2veczd2_806 = make_vector(BgL_newzd2lenzd2_805, BUNSPEC);
					{	/* Ieee/vector.scm 265 */

						{	/* Ieee/vector.scm 268 */
							bool_t BgL_test1800z00_2351;

							if ((BgL_newzd2lenzd2_805 < 0L))
								{	/* Ieee/vector.scm 268 */
									BgL_test1800z00_2351 = ((bool_t) 1);
								}
							else
								{	/* Ieee/vector.scm 268 */
									if (
										((long) CINT(BgL_startz00_34) >
											VECTOR_LENGTH(BgL_oldzd2veczd2_33)))
										{	/* Ieee/vector.scm 268 */
											BgL_test1800z00_2351 = ((bool_t) 1);
										}
									else
										{	/* Ieee/vector.scm 268 */
											BgL_test1800z00_2351 =
												(
												(long) CINT(BgL_stopz00_35) >
												VECTOR_LENGTH(BgL_oldzd2veczd2_33));
								}}
							if (BgL_test1800z00_2351)
								{	/* Ieee/vector.scm 269 */
									obj_t BgL_arg1129z00_811;

									BgL_arg1129z00_811 =
										MAKE_YOUNG_PAIR(BgL_startz00_34, BgL_stopz00_35);
									return
										BGl_errorz00zz__errorz00
										(BGl_string1683z00zz__r4_vectors_6_8z00,
										BGl_string1684z00zz__r4_vectors_6_8z00, BgL_arg1129z00_811);
								}
							else
								{	/* Ieee/vector.scm 272 */
									long BgL_auxz00_2365;
									long BgL_tmpz00_2363;

									BgL_auxz00_2365 =
										(
										(long) CINT(BgL_stopz00_35) - (long) CINT(BgL_startz00_34));
									BgL_tmpz00_2363 = (long) CINT(BgL_startz00_34);
									return
										BGL_VECTOR_BLIT_NO_OVERLAP(BgL_newzd2veczd2_806,
										BgL_oldzd2veczd2_33, 0L, BgL_tmpz00_2363, BgL_auxz00_2365);
								}
						}
					}
				}
			}
		}

	}



/* &vector-copy3 */
	obj_t BGl_z62vectorzd2copy3zb0zz__r4_vectors_6_8z00(obj_t BgL_envz00_1894,
		obj_t BgL_oldzd2veczd2_1895, obj_t BgL_startz00_1896,
		obj_t BgL_stopz00_1897)
	{
		{	/* Ieee/vector.scm 261 */
			{	/* Ieee/vector.scm 262 */
				obj_t BgL_auxz00_2370;

				if (VECTORP(BgL_oldzd2veczd2_1895))
					{	/* Ieee/vector.scm 262 */
						BgL_auxz00_2370 = BgL_oldzd2veczd2_1895;
					}
				else
					{
						obj_t BgL_auxz00_2373;

						BgL_auxz00_2373 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(11707L),
							BGl_string1685z00zz__r4_vectors_6_8z00,
							BGl_string1667z00zz__r4_vectors_6_8z00, BgL_oldzd2veczd2_1895);
						FAILURE(BgL_auxz00_2373, BFALSE, BFALSE);
					}
				return
					BGl_vectorzd2copy3zd2zz__r4_vectors_6_8z00(BgL_auxz00_2370,
					BgL_startz00_1896, BgL_stopz00_1897);
			}
		}

	}



/* vector-copy */
	BGL_EXPORTED_DEF obj_t BGl_vectorzd2copyzd2zz__r4_vectors_6_8z00(obj_t
		BgL_oldzd2veczd2_36, obj_t BgL_argsz00_37)
	{
		{	/* Ieee/vector.scm 285 */
			{	/* Ieee/vector.scm 286 */
				obj_t BgL_startz00_818;

				if (PAIRP(BgL_argsz00_37))
					{	/* Ieee/vector.scm 287 */
						if (INTEGERP(CAR(BgL_argsz00_37)))
							{	/* Ieee/vector.scm 288 */
								BgL_startz00_818 = CAR(BgL_argsz00_37);
							}
						else
							{	/* Ieee/vector.scm 288 */
								BgL_startz00_818 =
									BGl_errorz00zz__errorz00
									(BGl_string1683z00zz__r4_vectors_6_8z00,
									BGl_string1686z00zz__r4_vectors_6_8z00, CAR(BgL_argsz00_37));
							}
					}
				else
					{	/* Ieee/vector.scm 287 */
						BgL_startz00_818 = BINT(0L);
					}
				{	/* Ieee/vector.scm 287 */
					obj_t BgL_stopz00_819;

					{	/* Ieee/vector.scm 292 */
						bool_t BgL_test1806z00_2387;

						if (PAIRP(BgL_argsz00_37))
							{	/* Ieee/vector.scm 292 */
								obj_t BgL_tmpz00_2390;

								BgL_tmpz00_2390 = CDR(BgL_argsz00_37);
								BgL_test1806z00_2387 = PAIRP(BgL_tmpz00_2390);
							}
						else
							{	/* Ieee/vector.scm 292 */
								BgL_test1806z00_2387 = ((bool_t) 0);
							}
						if (BgL_test1806z00_2387)
							{	/* Ieee/vector.scm 293 */
								bool_t BgL_test1808z00_2393;

								{	/* Ieee/vector.scm 293 */
									bool_t BgL_test1809z00_2394;

									{	/* Ieee/vector.scm 293 */
										obj_t BgL_tmpz00_2395;

										BgL_tmpz00_2395 = CDR(CDR(BgL_argsz00_37));
										BgL_test1809z00_2394 = PAIRP(BgL_tmpz00_2395);
									}
									if (BgL_test1809z00_2394)
										{	/* Ieee/vector.scm 293 */
											BgL_test1808z00_2393 = ((bool_t) 1);
										}
									else
										{	/* Ieee/vector.scm 293 */
											if (INTEGERP(CAR(CDR(BgL_argsz00_37))))
												{	/* Ieee/vector.scm 294 */
													BgL_test1808z00_2393 = ((bool_t) 0);
												}
											else
												{	/* Ieee/vector.scm 294 */
													BgL_test1808z00_2393 = ((bool_t) 1);
												}
										}
								}
								if (BgL_test1808z00_2393)
									{	/* Ieee/vector.scm 293 */
										BgL_stopz00_819 =
											BGl_errorz00zz__errorz00
											(BGl_string1683z00zz__r4_vectors_6_8z00,
											BGl_string1686z00zz__r4_vectors_6_8z00,
											CDR(BgL_argsz00_37));
									}
								else
									{	/* Ieee/vector.scm 293 */
										BgL_stopz00_819 = CAR(CDR(BgL_argsz00_37));
									}
							}
						else
							{	/* Ieee/vector.scm 292 */
								BgL_stopz00_819 = BINT(VECTOR_LENGTH(BgL_oldzd2veczd2_36));
							}
					}
					{	/* Ieee/vector.scm 292 */

						return
							BGl_vectorzd2copy3zd2zz__r4_vectors_6_8z00(BgL_oldzd2veczd2_36,
							BgL_startz00_818, BgL_stopz00_819);
					}
				}
			}
		}

	}



/* &vector-copy */
	obj_t BGl_z62vectorzd2copyzb0zz__r4_vectors_6_8z00(obj_t BgL_envz00_1898,
		obj_t BgL_oldzd2veczd2_1899, obj_t BgL_argsz00_1900)
	{
		{	/* Ieee/vector.scm 285 */
			{	/* Ieee/vector.scm 286 */
				obj_t BgL_auxz00_2410;

				if (VECTORP(BgL_oldzd2veczd2_1899))
					{	/* Ieee/vector.scm 286 */
						BgL_auxz00_2410 = BgL_oldzd2veczd2_1899;
					}
				else
					{
						obj_t BgL_auxz00_2413;

						BgL_auxz00_2413 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(12588L),
							BGl_string1687z00zz__r4_vectors_6_8z00,
							BGl_string1667z00zz__r4_vectors_6_8z00, BgL_oldzd2veczd2_1899);
						FAILURE(BgL_auxz00_2413, BFALSE, BFALSE);
					}
				return
					BGl_vectorzd2copyzd2zz__r4_vectors_6_8z00(BgL_auxz00_2410,
					BgL_argsz00_1900);
			}
		}

	}



/* _vector-copy! */
	obj_t BGl__vectorzd2copyz12zc0zz__r4_vectors_6_8z00(obj_t BgL_env1058z00_44,
		obj_t BgL_opt1057z00_43)
	{
		{	/* Ieee/vector.scm 303 */
			{	/* Ieee/vector.scm 303 */
				obj_t BgL_g1059z00_842;
				obj_t BgL_g1060z00_843;
				obj_t BgL_sourcez00_844;

				BgL_g1059z00_842 = VECTOR_REF(BgL_opt1057z00_43, 0L);
				BgL_g1060z00_843 = VECTOR_REF(BgL_opt1057z00_43, 1L);
				BgL_sourcez00_844 = VECTOR_REF(BgL_opt1057z00_43, 2L);
				switch (VECTOR_LENGTH(BgL_opt1057z00_43))
					{
					case 3L:

						{	/* Ieee/vector.scm 304 */
							long BgL_sendz00_848;

							{	/* Ieee/vector.scm 304 */
								obj_t BgL_vectorz00_1423;

								if (VECTORP(BgL_sourcez00_844))
									{	/* Ieee/vector.scm 304 */
										BgL_vectorz00_1423 = BgL_sourcez00_844;
									}
								else
									{
										obj_t BgL_auxz00_2423;

										BgL_auxz00_2423 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(13335L),
											BGl_string1688z00zz__r4_vectors_6_8z00,
											BGl_string1667z00zz__r4_vectors_6_8z00,
											BgL_sourcez00_844);
										FAILURE(BgL_auxz00_2423, BFALSE, BFALSE);
									}
								BgL_sendz00_848 = VECTOR_LENGTH(BgL_vectorz00_1423);
							}
							{	/* Ieee/vector.scm 303 */

								{	/* Ieee/vector.scm 303 */
									obj_t BgL_targetz00_1424;
									long BgL_tstartz00_1425;

									if (VECTORP(BgL_g1059z00_842))
										{	/* Ieee/vector.scm 303 */
											BgL_targetz00_1424 = BgL_g1059z00_842;
										}
									else
										{
											obj_t BgL_auxz00_2430;

											BgL_auxz00_2430 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(13245L),
												BGl_string1688z00zz__r4_vectors_6_8z00,
												BGl_string1667z00zz__r4_vectors_6_8z00,
												BgL_g1059z00_842);
											FAILURE(BgL_auxz00_2430, BFALSE, BFALSE);
										}
									{	/* Ieee/vector.scm 303 */
										obj_t BgL_tmpz00_2434;

										if (INTEGERP(BgL_g1060z00_843))
											{	/* Ieee/vector.scm 303 */
												BgL_tmpz00_2434 = BgL_g1060z00_843;
											}
										else
											{
												obj_t BgL_auxz00_2437;

												BgL_auxz00_2437 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(13245L),
													BGl_string1688z00zz__r4_vectors_6_8z00,
													BGl_string1665z00zz__r4_vectors_6_8z00,
													BgL_g1060z00_843);
												FAILURE(BgL_auxz00_2437, BFALSE, BFALSE);
											}
										BgL_tstartz00_1425 = (long) CINT(BgL_tmpz00_2434);
									}
									{	/* Ieee/vector.scm 305 */
										long BgL_endz00_1426;

										{	/* Ieee/vector.scm 305 */
											long BgL_bz00_1428;

											{	/* Ieee/vector.scm 305 */
												obj_t BgL_vectorz00_1437;

												if (VECTORP(BgL_sourcez00_844))
													{	/* Ieee/vector.scm 305 */
														BgL_vectorz00_1437 = BgL_sourcez00_844;
													}
												else
													{
														obj_t BgL_auxz00_2444;

														BgL_auxz00_2444 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string1663z00zz__r4_vectors_6_8z00,
															BINT(13387L),
															BGl_string1688z00zz__r4_vectors_6_8z00,
															BGl_string1667z00zz__r4_vectors_6_8z00,
															BgL_sourcez00_844);
														FAILURE(BgL_auxz00_2444, BFALSE, BFALSE);
													}
												BgL_bz00_1428 = VECTOR_LENGTH(BgL_vectorz00_1437);
											}
											if ((BgL_sendz00_848 < BgL_bz00_1428))
												{	/* Ieee/vector.scm 305 */
													BgL_endz00_1426 = BgL_sendz00_848;
												}
											else
												{	/* Ieee/vector.scm 305 */
													BgL_endz00_1426 = BgL_bz00_1428;
												}
										}
										{	/* Ieee/vector.scm 305 */
											long BgL_countz00_1430;

											BgL_countz00_1430 = (BgL_endz00_1426 - 0L);
											{	/* Ieee/vector.scm 306 */
												long BgL_tendz00_1431;

												{	/* Ieee/vector.scm 307 */
													long BgL_az00_1432;

													BgL_az00_1432 =
														(BgL_tstartz00_1425 + BgL_countz00_1430);
													if (
														(BgL_az00_1432 < VECTOR_LENGTH(BgL_targetz00_1424)))
														{	/* Ieee/vector.scm 307 */
															BgL_tendz00_1431 = BgL_az00_1432;
														}
													else
														{	/* Ieee/vector.scm 307 */
															BgL_tendz00_1431 =
																VECTOR_LENGTH(BgL_targetz00_1424);
														}
												}
												{	/* Ieee/vector.scm 307 */

													if ((BgL_targetz00_1424 == BgL_sourcez00_844))
														{	/* Ieee/vector.scm 311 */
															long BgL_arg1171z00_1435;

															BgL_arg1171z00_1435 =
																(BgL_tendz00_1431 - BgL_tstartz00_1425);
															{	/* Ieee/vector.scm 311 */
																obj_t BgL_tmpz00_2460;

																if (VECTORP(BgL_sourcez00_844))
																	{	/* Ieee/vector.scm 311 */
																		BgL_tmpz00_2460 = BgL_sourcez00_844;
																	}
																else
																	{
																		obj_t BgL_auxz00_2463;

																		BgL_auxz00_2463 =
																			BGl_typezd2errorzd2zz__errorz00
																			(BGl_string1663z00zz__r4_vectors_6_8z00,
																			BINT(13595L),
																			BGl_string1688z00zz__r4_vectors_6_8z00,
																			BGl_string1667z00zz__r4_vectors_6_8z00,
																			BgL_sourcez00_844);
																		FAILURE(BgL_auxz00_2463, BFALSE, BFALSE);
																	}
																return
																	BGL_VECTOR_BLIT_OVERLAP(BgL_targetz00_1424,
																	BgL_tmpz00_2460, BgL_tstartz00_1425, 0L,
																	BgL_arg1171z00_1435);
															}
														}
													else
														{	/* Ieee/vector.scm 312 */
															long BgL_arg1172z00_1436;

															BgL_arg1172z00_1436 =
																(BgL_tendz00_1431 - BgL_tstartz00_1425);
															{	/* Ieee/vector.scm 312 */
																obj_t BgL_tmpz00_2469;

																if (VECTORP(BgL_sourcez00_844))
																	{	/* Ieee/vector.scm 312 */
																		BgL_tmpz00_2469 = BgL_sourcez00_844;
																	}
																else
																	{
																		obj_t BgL_auxz00_2472;

																		BgL_auxz00_2472 =
																			BGl_typezd2errorzd2zz__errorz00
																			(BGl_string1663z00zz__r4_vectors_6_8z00,
																			BINT(13675L),
																			BGl_string1688z00zz__r4_vectors_6_8z00,
																			BGl_string1667z00zz__r4_vectors_6_8z00,
																			BgL_sourcez00_844);
																		FAILURE(BgL_auxz00_2472, BFALSE, BFALSE);
																	}
																return
																	BGL_VECTOR_BLIT_NO_OVERLAP(BgL_targetz00_1424,
																	BgL_tmpz00_2469, BgL_tstartz00_1425, 0L,
																	BgL_arg1172z00_1436);
															}
														}
												}
											}
										}
									}
								}
							}
						}
						break;
					case 4L:

						{	/* Ieee/vector.scm 303 */
							obj_t BgL_sstartz00_849;

							BgL_sstartz00_849 = VECTOR_REF(BgL_opt1057z00_43, 3L);
							{	/* Ieee/vector.scm 304 */
								long BgL_sendz00_850;

								{	/* Ieee/vector.scm 304 */
									obj_t BgL_vectorz00_1451;

									if (VECTORP(BgL_sourcez00_844))
										{	/* Ieee/vector.scm 304 */
											BgL_vectorz00_1451 = BgL_sourcez00_844;
										}
									else
										{
											obj_t BgL_auxz00_2480;

											BgL_auxz00_2480 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(13335L),
												BGl_string1688z00zz__r4_vectors_6_8z00,
												BGl_string1667z00zz__r4_vectors_6_8z00,
												BgL_sourcez00_844);
											FAILURE(BgL_auxz00_2480, BFALSE, BFALSE);
										}
									BgL_sendz00_850 = VECTOR_LENGTH(BgL_vectorz00_1451);
								}
								{	/* Ieee/vector.scm 303 */

									{	/* Ieee/vector.scm 303 */
										obj_t BgL_targetz00_1452;
										long BgL_tstartz00_1453;

										if (VECTORP(BgL_g1059z00_842))
											{	/* Ieee/vector.scm 303 */
												BgL_targetz00_1452 = BgL_g1059z00_842;
											}
										else
											{
												obj_t BgL_auxz00_2487;

												BgL_auxz00_2487 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(13245L),
													BGl_string1688z00zz__r4_vectors_6_8z00,
													BGl_string1667z00zz__r4_vectors_6_8z00,
													BgL_g1059z00_842);
												FAILURE(BgL_auxz00_2487, BFALSE, BFALSE);
											}
										{	/* Ieee/vector.scm 303 */
											obj_t BgL_tmpz00_2491;

											if (INTEGERP(BgL_g1060z00_843))
												{	/* Ieee/vector.scm 303 */
													BgL_tmpz00_2491 = BgL_g1060z00_843;
												}
											else
												{
													obj_t BgL_auxz00_2494;

													BgL_auxz00_2494 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string1663z00zz__r4_vectors_6_8z00,
														BINT(13245L),
														BGl_string1688z00zz__r4_vectors_6_8z00,
														BGl_string1665z00zz__r4_vectors_6_8z00,
														BgL_g1060z00_843);
													FAILURE(BgL_auxz00_2494, BFALSE, BFALSE);
												}
											BgL_tstartz00_1453 = (long) CINT(BgL_tmpz00_2491);
										}
										{	/* Ieee/vector.scm 305 */
											long BgL_endz00_1454;

											{	/* Ieee/vector.scm 305 */
												long BgL_bz00_1456;

												{	/* Ieee/vector.scm 305 */
													obj_t BgL_vectorz00_1465;

													if (VECTORP(BgL_sourcez00_844))
														{	/* Ieee/vector.scm 305 */
															BgL_vectorz00_1465 = BgL_sourcez00_844;
														}
													else
														{
															obj_t BgL_auxz00_2501;

															BgL_auxz00_2501 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string1663z00zz__r4_vectors_6_8z00,
																BINT(13387L),
																BGl_string1688z00zz__r4_vectors_6_8z00,
																BGl_string1667z00zz__r4_vectors_6_8z00,
																BgL_sourcez00_844);
															FAILURE(BgL_auxz00_2501, BFALSE, BFALSE);
														}
													BgL_bz00_1456 = VECTOR_LENGTH(BgL_vectorz00_1465);
												}
												if ((BgL_sendz00_850 < BgL_bz00_1456))
													{	/* Ieee/vector.scm 305 */
														BgL_endz00_1454 = BgL_sendz00_850;
													}
												else
													{	/* Ieee/vector.scm 305 */
														BgL_endz00_1454 = BgL_bz00_1456;
													}
											}
											{	/* Ieee/vector.scm 305 */
												long BgL_countz00_1458;

												{	/* Ieee/vector.scm 306 */
													long BgL_za72za7_1469;

													{	/* Ieee/vector.scm 306 */
														obj_t BgL_tmpz00_2508;

														if (INTEGERP(BgL_sstartz00_849))
															{	/* Ieee/vector.scm 306 */
																BgL_tmpz00_2508 = BgL_sstartz00_849;
															}
														else
															{
																obj_t BgL_auxz00_2511;

																BgL_auxz00_2511 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string1663z00zz__r4_vectors_6_8z00,
																	BINT(13423L),
																	BGl_string1688z00zz__r4_vectors_6_8z00,
																	BGl_string1665z00zz__r4_vectors_6_8z00,
																	BgL_sstartz00_849);
																FAILURE(BgL_auxz00_2511, BFALSE, BFALSE);
															}
														BgL_za72za7_1469 = (long) CINT(BgL_tmpz00_2508);
													}
													BgL_countz00_1458 =
														(BgL_endz00_1454 - BgL_za72za7_1469);
												}
												{	/* Ieee/vector.scm 306 */
													long BgL_tendz00_1459;

													{	/* Ieee/vector.scm 307 */
														long BgL_az00_1460;

														BgL_az00_1460 =
															(BgL_tstartz00_1453 + BgL_countz00_1458);
														if (
															(BgL_az00_1460 <
																VECTOR_LENGTH(BgL_targetz00_1452)))
															{	/* Ieee/vector.scm 307 */
																BgL_tendz00_1459 = BgL_az00_1460;
															}
														else
															{	/* Ieee/vector.scm 307 */
																BgL_tendz00_1459 =
																	VECTOR_LENGTH(BgL_targetz00_1452);
															}
													}
													{	/* Ieee/vector.scm 307 */

														if ((BgL_targetz00_1452 == BgL_sourcez00_844))
															{	/* Ieee/vector.scm 311 */
																long BgL_arg1171z00_1463;

																BgL_arg1171z00_1463 =
																	(BgL_tendz00_1459 - BgL_tstartz00_1453);
																{	/* Ieee/vector.scm 311 */
																	long BgL_auxz00_2532;
																	obj_t BgL_tmpz00_2525;

																	{	/* Ieee/vector.scm 311 */
																		obj_t BgL_tmpz00_2533;

																		if (INTEGERP(BgL_sstartz00_849))
																			{	/* Ieee/vector.scm 311 */
																				BgL_tmpz00_2533 = BgL_sstartz00_849;
																			}
																		else
																			{
																				obj_t BgL_auxz00_2536;

																				BgL_auxz00_2536 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string1663z00zz__r4_vectors_6_8z00,
																					BINT(13609L),
																					BGl_string1688z00zz__r4_vectors_6_8z00,
																					BGl_string1665z00zz__r4_vectors_6_8z00,
																					BgL_sstartz00_849);
																				FAILURE(BgL_auxz00_2536, BFALSE,
																					BFALSE);
																			}
																		BgL_auxz00_2532 =
																			(long) CINT(BgL_tmpz00_2533);
																	}
																	if (VECTORP(BgL_sourcez00_844))
																		{	/* Ieee/vector.scm 311 */
																			BgL_tmpz00_2525 = BgL_sourcez00_844;
																		}
																	else
																		{
																			obj_t BgL_auxz00_2528;

																			BgL_auxz00_2528 =
																				BGl_typezd2errorzd2zz__errorz00
																				(BGl_string1663z00zz__r4_vectors_6_8z00,
																				BINT(13595L),
																				BGl_string1688z00zz__r4_vectors_6_8z00,
																				BGl_string1667z00zz__r4_vectors_6_8z00,
																				BgL_sourcez00_844);
																			FAILURE(BgL_auxz00_2528, BFALSE, BFALSE);
																		}
																	return
																		BGL_VECTOR_BLIT_OVERLAP(BgL_targetz00_1452,
																		BgL_tmpz00_2525, BgL_tstartz00_1453,
																		BgL_auxz00_2532, BgL_arg1171z00_1463);
																}
															}
														else
															{	/* Ieee/vector.scm 312 */
																long BgL_arg1172z00_1464;

																BgL_arg1172z00_1464 =
																	(BgL_tendz00_1459 - BgL_tstartz00_1453);
																{	/* Ieee/vector.scm 312 */
																	long BgL_auxz00_2550;
																	obj_t BgL_tmpz00_2543;

																	{	/* Ieee/vector.scm 312 */
																		obj_t BgL_tmpz00_2551;

																		if (INTEGERP(BgL_sstartz00_849))
																			{	/* Ieee/vector.scm 312 */
																				BgL_tmpz00_2551 = BgL_sstartz00_849;
																			}
																		else
																			{
																				obj_t BgL_auxz00_2554;

																				BgL_auxz00_2554 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string1663z00zz__r4_vectors_6_8z00,
																					BINT(13689L),
																					BGl_string1688z00zz__r4_vectors_6_8z00,
																					BGl_string1665z00zz__r4_vectors_6_8z00,
																					BgL_sstartz00_849);
																				FAILURE(BgL_auxz00_2554, BFALSE,
																					BFALSE);
																			}
																		BgL_auxz00_2550 =
																			(long) CINT(BgL_tmpz00_2551);
																	}
																	if (VECTORP(BgL_sourcez00_844))
																		{	/* Ieee/vector.scm 312 */
																			BgL_tmpz00_2543 = BgL_sourcez00_844;
																		}
																	else
																		{
																			obj_t BgL_auxz00_2546;

																			BgL_auxz00_2546 =
																				BGl_typezd2errorzd2zz__errorz00
																				(BGl_string1663z00zz__r4_vectors_6_8z00,
																				BINT(13675L),
																				BGl_string1688z00zz__r4_vectors_6_8z00,
																				BGl_string1667z00zz__r4_vectors_6_8z00,
																				BgL_sourcez00_844);
																			FAILURE(BgL_auxz00_2546, BFALSE, BFALSE);
																		}
																	return
																		BGL_VECTOR_BLIT_NO_OVERLAP
																		(BgL_targetz00_1452, BgL_tmpz00_2543,
																		BgL_tstartz00_1453, BgL_auxz00_2550,
																		BgL_arg1172z00_1464);
																}
															}
													}
												}
											}
										}
									}
								}
							}
						}
						break;
					case 5L:

						{	/* Ieee/vector.scm 303 */
							obj_t BgL_sstartz00_851;

							BgL_sstartz00_851 = VECTOR_REF(BgL_opt1057z00_43, 3L);
							{	/* Ieee/vector.scm 303 */
								obj_t BgL_sendz00_852;

								BgL_sendz00_852 = VECTOR_REF(BgL_opt1057z00_43, 4L);
								{	/* Ieee/vector.scm 303 */

									{	/* Ieee/vector.scm 303 */
										obj_t BgL_targetz00_1479;
										long BgL_tstartz00_1480;

										if (VECTORP(BgL_g1059z00_842))
											{	/* Ieee/vector.scm 303 */
												BgL_targetz00_1479 = BgL_g1059z00_842;
											}
										else
											{
												obj_t BgL_auxz00_2564;

												BgL_auxz00_2564 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(13245L),
													BGl_string1688z00zz__r4_vectors_6_8z00,
													BGl_string1667z00zz__r4_vectors_6_8z00,
													BgL_g1059z00_842);
												FAILURE(BgL_auxz00_2564, BFALSE, BFALSE);
											}
										{	/* Ieee/vector.scm 303 */
											obj_t BgL_tmpz00_2568;

											if (INTEGERP(BgL_g1060z00_843))
												{	/* Ieee/vector.scm 303 */
													BgL_tmpz00_2568 = BgL_g1060z00_843;
												}
											else
												{
													obj_t BgL_auxz00_2571;

													BgL_auxz00_2571 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string1663z00zz__r4_vectors_6_8z00,
														BINT(13245L),
														BGl_string1688z00zz__r4_vectors_6_8z00,
														BGl_string1665z00zz__r4_vectors_6_8z00,
														BgL_g1060z00_843);
													FAILURE(BgL_auxz00_2571, BFALSE, BFALSE);
												}
											BgL_tstartz00_1480 = (long) CINT(BgL_tmpz00_2568);
										}
										{	/* Ieee/vector.scm 305 */
											obj_t BgL_endz00_1481;

											{	/* Ieee/vector.scm 305 */
												long BgL_bz00_1483;

												{	/* Ieee/vector.scm 305 */
													obj_t BgL_vectorz00_1492;

													if (VECTORP(BgL_sourcez00_844))
														{	/* Ieee/vector.scm 305 */
															BgL_vectorz00_1492 = BgL_sourcez00_844;
														}
													else
														{
															obj_t BgL_auxz00_2578;

															BgL_auxz00_2578 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string1663z00zz__r4_vectors_6_8z00,
																BINT(13387L),
																BGl_string1688z00zz__r4_vectors_6_8z00,
																BGl_string1667z00zz__r4_vectors_6_8z00,
																BgL_sourcez00_844);
															FAILURE(BgL_auxz00_2578, BFALSE, BFALSE);
														}
													BgL_bz00_1483 = VECTOR_LENGTH(BgL_vectorz00_1492);
												}
												{	/* Ieee/vector.scm 305 */
													bool_t BgL_test1836z00_2583;

													{	/* Ieee/vector.scm 305 */
														long BgL_n1z00_1493;

														{	/* Ieee/vector.scm 305 */
															obj_t BgL_tmpz00_2584;

															if (INTEGERP(BgL_sendz00_852))
																{	/* Ieee/vector.scm 305 */
																	BgL_tmpz00_2584 = BgL_sendz00_852;
																}
															else
																{
																	obj_t BgL_auxz00_2587;

																	BgL_auxz00_2587 =
																		BGl_typezd2errorzd2zz__errorz00
																		(BGl_string1663z00zz__r4_vectors_6_8z00,
																		BINT(13372L),
																		BGl_string1688z00zz__r4_vectors_6_8z00,
																		BGl_string1665z00zz__r4_vectors_6_8z00,
																		BgL_sendz00_852);
																	FAILURE(BgL_auxz00_2587, BFALSE, BFALSE);
																}
															BgL_n1z00_1493 = (long) CINT(BgL_tmpz00_2584);
														}
														BgL_test1836z00_2583 =
															(BgL_n1z00_1493 < BgL_bz00_1483);
													}
													if (BgL_test1836z00_2583)
														{	/* Ieee/vector.scm 305 */
															BgL_endz00_1481 = BgL_sendz00_852;
														}
													else
														{	/* Ieee/vector.scm 305 */
															BgL_endz00_1481 = BINT(BgL_bz00_1483);
														}
												}
											}
											{	/* Ieee/vector.scm 305 */
												long BgL_countz00_1485;

												{	/* Ieee/vector.scm 306 */
													long BgL_za71za7_1495;
													long BgL_za72za7_1496;

													{	/* Ieee/vector.scm 306 */
														obj_t BgL_tmpz00_2594;

														if (INTEGERP(BgL_endz00_1481))
															{	/* Ieee/vector.scm 306 */
																BgL_tmpz00_2594 = BgL_endz00_1481;
															}
														else
															{
																obj_t BgL_auxz00_2597;

																BgL_auxz00_2597 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string1663z00zz__r4_vectors_6_8z00,
																	BINT(13419L),
																	BGl_string1688z00zz__r4_vectors_6_8z00,
																	BGl_string1665z00zz__r4_vectors_6_8z00,
																	BgL_endz00_1481);
																FAILURE(BgL_auxz00_2597, BFALSE, BFALSE);
															}
														BgL_za71za7_1495 = (long) CINT(BgL_tmpz00_2594);
													}
													{	/* Ieee/vector.scm 306 */
														obj_t BgL_tmpz00_2602;

														if (INTEGERP(BgL_sstartz00_851))
															{	/* Ieee/vector.scm 306 */
																BgL_tmpz00_2602 = BgL_sstartz00_851;
															}
														else
															{
																obj_t BgL_auxz00_2605;

																BgL_auxz00_2605 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string1663z00zz__r4_vectors_6_8z00,
																	BINT(13423L),
																	BGl_string1688z00zz__r4_vectors_6_8z00,
																	BGl_string1665z00zz__r4_vectors_6_8z00,
																	BgL_sstartz00_851);
																FAILURE(BgL_auxz00_2605, BFALSE, BFALSE);
															}
														BgL_za72za7_1496 = (long) CINT(BgL_tmpz00_2602);
													}
													BgL_countz00_1485 =
														(BgL_za71za7_1495 - BgL_za72za7_1496);
												}
												{	/* Ieee/vector.scm 306 */
													long BgL_tendz00_1486;

													{	/* Ieee/vector.scm 307 */
														long BgL_az00_1487;

														BgL_az00_1487 =
															(BgL_tstartz00_1480 + BgL_countz00_1485);
														if (
															(BgL_az00_1487 <
																VECTOR_LENGTH(BgL_targetz00_1479)))
															{	/* Ieee/vector.scm 307 */
																BgL_tendz00_1486 = BgL_az00_1487;
															}
														else
															{	/* Ieee/vector.scm 307 */
																BgL_tendz00_1486 =
																	VECTOR_LENGTH(BgL_targetz00_1479);
															}
													}
													{	/* Ieee/vector.scm 307 */

														if ((BgL_targetz00_1479 == BgL_sourcez00_844))
															{	/* Ieee/vector.scm 311 */
																long BgL_arg1171z00_1490;

																BgL_arg1171z00_1490 =
																	(BgL_tendz00_1486 - BgL_tstartz00_1480);
																{	/* Ieee/vector.scm 311 */
																	long BgL_auxz00_2626;
																	obj_t BgL_tmpz00_2619;

																	{	/* Ieee/vector.scm 311 */
																		obj_t BgL_tmpz00_2627;

																		if (INTEGERP(BgL_sstartz00_851))
																			{	/* Ieee/vector.scm 311 */
																				BgL_tmpz00_2627 = BgL_sstartz00_851;
																			}
																		else
																			{
																				obj_t BgL_auxz00_2630;

																				BgL_auxz00_2630 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string1663z00zz__r4_vectors_6_8z00,
																					BINT(13609L),
																					BGl_string1688z00zz__r4_vectors_6_8z00,
																					BGl_string1665z00zz__r4_vectors_6_8z00,
																					BgL_sstartz00_851);
																				FAILURE(BgL_auxz00_2630, BFALSE,
																					BFALSE);
																			}
																		BgL_auxz00_2626 =
																			(long) CINT(BgL_tmpz00_2627);
																	}
																	if (VECTORP(BgL_sourcez00_844))
																		{	/* Ieee/vector.scm 311 */
																			BgL_tmpz00_2619 = BgL_sourcez00_844;
																		}
																	else
																		{
																			obj_t BgL_auxz00_2622;

																			BgL_auxz00_2622 =
																				BGl_typezd2errorzd2zz__errorz00
																				(BGl_string1663z00zz__r4_vectors_6_8z00,
																				BINT(13595L),
																				BGl_string1688z00zz__r4_vectors_6_8z00,
																				BGl_string1667z00zz__r4_vectors_6_8z00,
																				BgL_sourcez00_844);
																			FAILURE(BgL_auxz00_2622, BFALSE, BFALSE);
																		}
																	return
																		BGL_VECTOR_BLIT_OVERLAP(BgL_targetz00_1479,
																		BgL_tmpz00_2619, BgL_tstartz00_1480,
																		BgL_auxz00_2626, BgL_arg1171z00_1490);
																}
															}
														else
															{	/* Ieee/vector.scm 312 */
																long BgL_arg1172z00_1491;

																BgL_arg1172z00_1491 =
																	(BgL_tendz00_1486 - BgL_tstartz00_1480);
																{	/* Ieee/vector.scm 312 */
																	long BgL_auxz00_2644;
																	obj_t BgL_tmpz00_2637;

																	{	/* Ieee/vector.scm 312 */
																		obj_t BgL_tmpz00_2645;

																		if (INTEGERP(BgL_sstartz00_851))
																			{	/* Ieee/vector.scm 312 */
																				BgL_tmpz00_2645 = BgL_sstartz00_851;
																			}
																		else
																			{
																				obj_t BgL_auxz00_2648;

																				BgL_auxz00_2648 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string1663z00zz__r4_vectors_6_8z00,
																					BINT(13689L),
																					BGl_string1688z00zz__r4_vectors_6_8z00,
																					BGl_string1665z00zz__r4_vectors_6_8z00,
																					BgL_sstartz00_851);
																				FAILURE(BgL_auxz00_2648, BFALSE,
																					BFALSE);
																			}
																		BgL_auxz00_2644 =
																			(long) CINT(BgL_tmpz00_2645);
																	}
																	if (VECTORP(BgL_sourcez00_844))
																		{	/* Ieee/vector.scm 312 */
																			BgL_tmpz00_2637 = BgL_sourcez00_844;
																		}
																	else
																		{
																			obj_t BgL_auxz00_2640;

																			BgL_auxz00_2640 =
																				BGl_typezd2errorzd2zz__errorz00
																				(BGl_string1663z00zz__r4_vectors_6_8z00,
																				BINT(13675L),
																				BGl_string1688z00zz__r4_vectors_6_8z00,
																				BGl_string1667z00zz__r4_vectors_6_8z00,
																				BgL_sourcez00_844);
																			FAILURE(BgL_auxz00_2640, BFALSE, BFALSE);
																		}
																	return
																		BGL_VECTOR_BLIT_NO_OVERLAP
																		(BgL_targetz00_1479, BgL_tmpz00_2637,
																		BgL_tstartz00_1480, BgL_auxz00_2644,
																		BgL_arg1172z00_1491);
																}
															}
													}
												}
											}
										}
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* vector-copy! */
	BGL_EXPORTED_DEF obj_t BGl_vectorzd2copyz12zc0zz__r4_vectors_6_8z00(obj_t
		BgL_targetz00_38, long BgL_tstartz00_39, obj_t BgL_sourcez00_40,
		obj_t BgL_sstartz00_41, obj_t BgL_sendz00_42)
	{
		{	/* Ieee/vector.scm 303 */
			{	/* Ieee/vector.scm 305 */
				obj_t BgL_endz00_1506;

				if (
					((long) CINT(BgL_sendz00_42) <
						VECTOR_LENGTH(((obj_t) BgL_sourcez00_40))))
					{	/* Ieee/vector.scm 305 */
						BgL_endz00_1506 = BgL_sendz00_42;
					}
				else
					{	/* Ieee/vector.scm 305 */
						BgL_endz00_1506 = BINT(VECTOR_LENGTH(((obj_t) BgL_sourcez00_40)));
					}
				{	/* Ieee/vector.scm 305 */
					long BgL_countz00_1510;

					BgL_countz00_1510 =
						((long) CINT(BgL_endz00_1506) - (long) CINT(BgL_sstartz00_41));
					{	/* Ieee/vector.scm 306 */
						long BgL_tendz00_1511;

						{	/* Ieee/vector.scm 307 */
							long BgL_az00_1512;

							BgL_az00_1512 = (BgL_tstartz00_39 + BgL_countz00_1510);
							if ((BgL_az00_1512 < VECTOR_LENGTH(BgL_targetz00_38)))
								{	/* Ieee/vector.scm 307 */
									BgL_tendz00_1511 = BgL_az00_1512;
								}
							else
								{	/* Ieee/vector.scm 307 */
									BgL_tendz00_1511 = VECTOR_LENGTH(BgL_targetz00_38);
								}
						}
						{	/* Ieee/vector.scm 307 */

							if ((BgL_targetz00_38 == BgL_sourcez00_40))
								{	/* Ieee/vector.scm 311 */
									long BgL_auxz00_2676;
									long BgL_tmpz00_2674;

									BgL_auxz00_2676 = (BgL_tendz00_1511 - BgL_tstartz00_39);
									BgL_tmpz00_2674 = (long) CINT(BgL_sstartz00_41);
									return
										BGL_VECTOR_BLIT_OVERLAP(BgL_targetz00_38, BgL_sourcez00_40,
										BgL_tstartz00_39, BgL_tmpz00_2674, BgL_auxz00_2676);
								}
							else
								{	/* Ieee/vector.scm 312 */
									long BgL_auxz00_2681;
									long BgL_tmpz00_2679;

									BgL_auxz00_2681 = (BgL_tendz00_1511 - BgL_tstartz00_39);
									BgL_tmpz00_2679 = (long) CINT(BgL_sstartz00_41);
									return
										BGL_VECTOR_BLIT_NO_OVERLAP(BgL_targetz00_38,
										BgL_sourcez00_40, BgL_tstartz00_39, BgL_tmpz00_2679,
										BgL_auxz00_2681);
								}
						}
					}
				}
			}
		}

	}



/* vector-append */
	BGL_EXPORTED_DEF obj_t BGl_vectorzd2appendzd2zz__r4_vectors_6_8z00(obj_t
		BgL_vecz00_45, obj_t BgL_argsz00_46)
	{
		{	/* Ieee/vector.scm 331 */
			{
				long BgL_lenz00_866;
				obj_t BgL_vectsz00_867;

				BgL_lenz00_866 = VECTOR_LENGTH(BgL_vecz00_45);
				BgL_vectsz00_867 = BgL_argsz00_46;
			BgL_zc3z04anonymousza31175ze3z87_868:
				if (NULLP(BgL_vectsz00_867))
					{	/* Ieee/vector.scm 335 */
						obj_t BgL_resz00_870;

						BgL_resz00_870 = make_vector(BgL_lenz00_866, BUNSPEC);
						{	/* Ieee/vector.scm 112 */

							{	/* Ieee/vector.scm 305 */
								long BgL_endz00_1536;

								if (
									(VECTOR_LENGTH(BgL_vecz00_45) < VECTOR_LENGTH(BgL_vecz00_45)))
									{	/* Ieee/vector.scm 305 */
										BgL_endz00_1536 = VECTOR_LENGTH(BgL_vecz00_45);
									}
								else
									{	/* Ieee/vector.scm 305 */
										BgL_endz00_1536 = VECTOR_LENGTH(BgL_vecz00_45);
									}
								{	/* Ieee/vector.scm 305 */
									long BgL_countz00_1540;

									BgL_countz00_1540 = (BgL_endz00_1536 - 0L);
									{	/* Ieee/vector.scm 306 */
										long BgL_tendz00_1541;

										{	/* Ieee/vector.scm 307 */
											long BgL_az00_1542;

											BgL_az00_1542 = (0L + BgL_countz00_1540);
											if ((BgL_az00_1542 < VECTOR_LENGTH(BgL_resz00_870)))
												{	/* Ieee/vector.scm 307 */
													BgL_tendz00_1541 = BgL_az00_1542;
												}
											else
												{	/* Ieee/vector.scm 307 */
													BgL_tendz00_1541 = VECTOR_LENGTH(BgL_resz00_870);
												}
										}
										{	/* Ieee/vector.scm 307 */

											if ((BgL_resz00_870 == BgL_vecz00_45))
												{	/* Ieee/vector.scm 311 */
													long BgL_tmpz00_2701;

													BgL_tmpz00_2701 = (BgL_tendz00_1541 - 0L);
													BGL_VECTOR_BLIT_OVERLAP(BgL_resz00_870, BgL_vecz00_45,
														0L, 0L, BgL_tmpz00_2701);
												}
											else
												{	/* Ieee/vector.scm 312 */
													long BgL_tmpz00_2704;

													BgL_tmpz00_2704 = (BgL_tendz00_1541 - 0L);
													BGL_VECTOR_BLIT_NO_OVERLAP(BgL_resz00_870,
														BgL_vecz00_45, 0L, 0L, BgL_tmpz00_2704);
						}}}}}}
						{
							long BgL_iz00_878;
							obj_t BgL_vectsz00_879;

							BgL_iz00_878 = VECTOR_LENGTH(BgL_vecz00_45);
							BgL_vectsz00_879 = BgL_argsz00_46;
						BgL_zc3z04anonymousza31177ze3z87_880:
							if (NULLP(BgL_vectsz00_879))
								{	/* Ieee/vector.scm 339 */
									return BgL_resz00_870;
								}
							else
								{	/* Ieee/vector.scm 341 */
									obj_t BgL_vecz00_882;

									BgL_vecz00_882 = CAR(((obj_t) BgL_vectsz00_879));
									{	/* Ieee/vector.scm 112 */

										{	/* Ieee/vector.scm 305 */
											long BgL_endz00_1566;

											if (
												(VECTOR_LENGTH(
														((obj_t) BgL_vecz00_882)) <
													VECTOR_LENGTH(((obj_t) BgL_vecz00_882))))
												{	/* Ieee/vector.scm 305 */
													BgL_endz00_1566 =
														VECTOR_LENGTH(((obj_t) BgL_vecz00_882));
												}
											else
												{	/* Ieee/vector.scm 305 */
													BgL_endz00_1566 =
														VECTOR_LENGTH(((obj_t) BgL_vecz00_882));
												}
											{	/* Ieee/vector.scm 305 */
												long BgL_countz00_1570;

												BgL_countz00_1570 = (BgL_endz00_1566 - 0L);
												{	/* Ieee/vector.scm 306 */
													long BgL_tendz00_1571;

													{	/* Ieee/vector.scm 307 */
														long BgL_az00_1572;

														BgL_az00_1572 = (BgL_iz00_878 + BgL_countz00_1570);
														if ((BgL_az00_1572 < VECTOR_LENGTH(BgL_resz00_870)))
															{	/* Ieee/vector.scm 307 */
																BgL_tendz00_1571 = BgL_az00_1572;
															}
														else
															{	/* Ieee/vector.scm 307 */
																BgL_tendz00_1571 =
																	VECTOR_LENGTH(BgL_resz00_870);
															}
													}
													{	/* Ieee/vector.scm 307 */

														if ((BgL_resz00_870 == BgL_vecz00_882))
															{	/* Ieee/vector.scm 311 */
																long BgL_tmpz00_2729;

																BgL_tmpz00_2729 =
																	(BgL_tendz00_1571 - BgL_iz00_878);
																BGL_VECTOR_BLIT_OVERLAP(BgL_resz00_870,
																	BgL_vecz00_882, BgL_iz00_878, 0L,
																	BgL_tmpz00_2729);
															}
														else
															{	/* Ieee/vector.scm 312 */
																long BgL_tmpz00_2732;

																BgL_tmpz00_2732 =
																	(BgL_tendz00_1571 - BgL_iz00_878);
																BGL_VECTOR_BLIT_NO_OVERLAP(BgL_resz00_870,
																	BgL_vecz00_882, BgL_iz00_878, 0L,
																	BgL_tmpz00_2732);
									}}}}}}
									{	/* Ieee/vector.scm 343 */
										long BgL_arg1182z00_888;
										obj_t BgL_arg1183z00_889;

										BgL_arg1182z00_888 =
											(BgL_iz00_878 + VECTOR_LENGTH(((obj_t) BgL_vecz00_882)));
										BgL_arg1183z00_889 = CDR(((obj_t) BgL_vectsz00_879));
										{
											obj_t BgL_vectsz00_2741;
											long BgL_iz00_2740;

											BgL_iz00_2740 = BgL_arg1182z00_888;
											BgL_vectsz00_2741 = BgL_arg1183z00_889;
											BgL_vectsz00_879 = BgL_vectsz00_2741;
											BgL_iz00_878 = BgL_iz00_2740;
											goto BgL_zc3z04anonymousza31177ze3z87_880;
										}
									}
								}
						}
					}
				else
					{	/* Ieee/vector.scm 344 */
						long BgL_arg1189z00_893;
						obj_t BgL_arg1190z00_894;

						{	/* Ieee/vector.scm 344 */
							long BgL_arg1191z00_895;

							{	/* Ieee/vector.scm 344 */
								obj_t BgL_arg1193z00_896;

								BgL_arg1193z00_896 = CAR(((obj_t) BgL_vectsz00_867));
								BgL_arg1191z00_895 =
									VECTOR_LENGTH(((obj_t) BgL_arg1193z00_896));
							}
							BgL_arg1189z00_893 = (BgL_arg1191z00_895 + BgL_lenz00_866);
						}
						BgL_arg1190z00_894 = CDR(((obj_t) BgL_vectsz00_867));
						{
							obj_t BgL_vectsz00_2751;
							long BgL_lenz00_2750;

							BgL_lenz00_2750 = BgL_arg1189z00_893;
							BgL_vectsz00_2751 = BgL_arg1190z00_894;
							BgL_vectsz00_867 = BgL_vectsz00_2751;
							BgL_lenz00_866 = BgL_lenz00_2750;
							goto BgL_zc3z04anonymousza31175ze3z87_868;
						}
					}
			}
		}

	}



/* &vector-append */
	obj_t BGl_z62vectorzd2appendzb0zz__r4_vectors_6_8z00(obj_t BgL_envz00_1901,
		obj_t BgL_vecz00_1902, obj_t BgL_argsz00_1903)
	{
		{	/* Ieee/vector.scm 331 */
			{	/* Ieee/vector.scm 332 */
				obj_t BgL_auxz00_2753;

				if (VECTORP(BgL_vecz00_1902))
					{	/* Ieee/vector.scm 332 */
						BgL_auxz00_2753 = BgL_vecz00_1902;
					}
				else
					{
						obj_t BgL_auxz00_2756;

						BgL_auxz00_2756 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(14486L),
							BGl_string1689z00zz__r4_vectors_6_8z00,
							BGl_string1667z00zz__r4_vectors_6_8z00, BgL_vecz00_1902);
						FAILURE(BgL_auxz00_2756, BFALSE, BFALSE);
					}
				return
					BGl_vectorzd2appendzd2zz__r4_vectors_6_8z00(BgL_auxz00_2753,
					BgL_argsz00_1903);
			}
		}

	}



/* sort */
	BGL_EXPORTED_DEF obj_t BGl_sortz00zz__r4_vectors_6_8z00(obj_t BgL_o1z00_47,
		obj_t BgL_o2z00_48)
	{
		{	/* Ieee/vector.scm 349 */
			if (PROCEDUREP(BgL_o1z00_47))
				{	/* Ieee/vector.scm 350 */
					return
						BGl_innerzd2sortzd2zz__r4_vectors_6_8z00(BgL_o2z00_48,
						BgL_o1z00_47);
				}
			else
				{	/* Ieee/vector.scm 350 */
					return
						BGl_innerzd2sortzd2zz__r4_vectors_6_8z00(BgL_o1z00_47,
						BgL_o2z00_48);
				}
		}

	}



/* &sort */
	obj_t BGl_z62sortz62zz__r4_vectors_6_8z00(obj_t BgL_envz00_1904,
		obj_t BgL_o1z00_1905, obj_t BgL_o2z00_1906)
	{
		{	/* Ieee/vector.scm 349 */
			return BGl_sortz00zz__r4_vectors_6_8z00(BgL_o1z00_1905, BgL_o2z00_1906);
		}

	}



/* inner-sort */
	obj_t BGl_innerzd2sortzd2zz__r4_vectors_6_8z00(obj_t BgL_objz00_49,
		obj_t BgL_procz00_50)
	{
		{	/* Ieee/vector.scm 357 */
			if (NULLP(BgL_objz00_49))
				{	/* Ieee/vector.scm 359 */
					return BgL_objz00_49;
				}
			else
				{	/* Ieee/vector.scm 361 */
					bool_t BgL_test1860z00_2768;

					if (PAIRP(BgL_objz00_49))
						{	/* Ieee/vector.scm 361 */
							BgL_test1860z00_2768 = NULLP(CDR(BgL_objz00_49));
						}
					else
						{	/* Ieee/vector.scm 361 */
							BgL_test1860z00_2768 = ((bool_t) 0);
						}
					if (BgL_test1860z00_2768)
						{	/* Ieee/vector.scm 361 */
							return BgL_objz00_49;
						}
					else
						{	/* Ieee/vector.scm 364 */
							obj_t BgL_vecz00_903;

							if (VECTORP(BgL_objz00_49))
								{	/* Ieee/vector.scm 366 */
									obj_t BgL_newz00_908;

									BgL_newz00_908 = create_vector(VECTOR_LENGTH(BgL_objz00_49));
									{	/* Ieee/vector.scm 367 */

										{
											long BgL_iz00_1611;

											BgL_iz00_1611 = 0L;
										BgL_loopz00_1610:
											if ((BgL_iz00_1611 < VECTOR_LENGTH(BgL_objz00_49)))
												{	/* Ieee/vector.scm 369 */
													{	/* Ieee/vector.scm 371 */
														obj_t BgL_arg1203z00_1615;

														BgL_arg1203z00_1615 =
															VECTOR_REF(
															((obj_t) BgL_objz00_49), BgL_iz00_1611);
														VECTOR_SET(BgL_newz00_908, BgL_iz00_1611,
															BgL_arg1203z00_1615);
													}
													{
														long BgL_iz00_2783;

														BgL_iz00_2783 = (BgL_iz00_1611 + 1L);
														BgL_iz00_1611 = BgL_iz00_2783;
														goto BgL_loopz00_1610;
													}
												}
											else
												{	/* Ieee/vector.scm 369 */
													((bool_t) 0);
												}
										}
										BgL_vecz00_903 = BgL_newz00_908;
									}
								}
							else
								{	/* Ieee/vector.scm 365 */
									if (PAIRP(BgL_objz00_49))
										{	/* Ieee/vector.scm 374 */
											BgL_vecz00_903 =
												BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00
												(BgL_objz00_49);
										}
									else
										{	/* Ieee/vector.scm 374 */
											BgL_vecz00_903 =
												BGl_errorz00zz__errorz00
												(BGl_string1690z00zz__r4_vectors_6_8z00,
												BGl_string1691z00zz__r4_vectors_6_8z00, BgL_objz00_49);
										}
								}
							{	/* Ieee/vector.scm 380 */
								obj_t BgL_resz00_904;

								BgL_resz00_904 = sort_vector(BgL_vecz00_903, BgL_procz00_50);
								if (PAIRP(BgL_objz00_49))
									{	/* Ieee/vector.scm 381 */
										return
											BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00
											(BgL_resz00_904);
									}
								else
									{	/* Ieee/vector.scm 381 */
										return BgL_resz00_904;
									}
							}
						}
				}
		}

	}



/* vector-mapN! */
	obj_t BGl_vectorzd2mapNz12zc0zz__r4_vectors_6_8z00(obj_t BgL_procz00_54,
		obj_t BgL_vdestz00_55, obj_t BgL_vsrcz00_56, obj_t BgL_vrestz00_57)
	{
		{	/* Ieee/vector.scm 400 */
			{
				long BgL_iz00_930;

				BgL_iz00_930 = 0L;
			BgL_zc3z04anonymousza31217ze3z87_931:
				if ((BgL_iz00_930 < VECTOR_LENGTH(((obj_t) BgL_vdestz00_55))))
					{	/* Ieee/vector.scm 404 */
						obj_t BgL_argsz00_933;

						if (NULLP(BgL_vrestz00_57))
							{	/* Ieee/vector.scm 404 */
								BgL_argsz00_933 = BNIL;
							}
						else
							{	/* Ieee/vector.scm 404 */
								obj_t BgL_head1036z00_940;

								BgL_head1036z00_940 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								{
									obj_t BgL_l1034z00_1676;
									obj_t BgL_tail1037z00_1677;

									BgL_l1034z00_1676 = BgL_vrestz00_57;
									BgL_tail1037z00_1677 = BgL_head1036z00_940;
								BgL_zc3z04anonymousza31223ze3z87_1675:
									if (NULLP(BgL_l1034z00_1676))
										{	/* Ieee/vector.scm 404 */
											BgL_argsz00_933 = CDR(BgL_head1036z00_940);
										}
									else
										{	/* Ieee/vector.scm 404 */
											obj_t BgL_newtail1038z00_1684;

											{	/* Ieee/vector.scm 404 */
												obj_t BgL_arg1226z00_1685;

												{	/* Ieee/vector.scm 404 */
													obj_t BgL_vz00_1686;

													BgL_vz00_1686 = CAR(((obj_t) BgL_l1034z00_1676));
													BgL_arg1226z00_1685 =
														VECTOR_REF(((obj_t) BgL_vz00_1686), BgL_iz00_930);
												}
												BgL_newtail1038z00_1684 =
													MAKE_YOUNG_PAIR(BgL_arg1226z00_1685, BNIL);
											}
											SET_CDR(BgL_tail1037z00_1677, BgL_newtail1038z00_1684);
											{	/* Ieee/vector.scm 404 */
												obj_t BgL_arg1225z00_1687;

												BgL_arg1225z00_1687 = CDR(((obj_t) BgL_l1034z00_1676));
												{
													obj_t BgL_tail1037z00_2812;
													obj_t BgL_l1034z00_2811;

													BgL_l1034z00_2811 = BgL_arg1225z00_1687;
													BgL_tail1037z00_2812 = BgL_newtail1038z00_1684;
													BgL_tail1037z00_1677 = BgL_tail1037z00_2812;
													BgL_l1034z00_1676 = BgL_l1034z00_2811;
													goto BgL_zc3z04anonymousza31223ze3z87_1675;
												}
											}
										}
								}
							}
						{	/* Ieee/vector.scm 404 */
							obj_t BgL_nvalz00_934;

							{	/* Ieee/vector.scm 405 */
								obj_t BgL_auxz00_2813;

								{	/* Ieee/vector.scm 405 */
									obj_t BgL_arg1220z00_936;

									BgL_arg1220z00_936 = VECTOR_REF(BgL_vsrcz00_56, BgL_iz00_930);
									{	/* Ieee/vector.scm 405 */
										obj_t BgL_list1221z00_937;

										BgL_list1221z00_937 =
											MAKE_YOUNG_PAIR(BgL_argsz00_933, BNIL);
										BgL_auxz00_2813 =
											BGl_consza2za2zz__r4_pairs_and_lists_6_3z00
											(BgL_arg1220z00_936, BgL_list1221z00_937);
									}
								}
								BgL_nvalz00_934 = apply(BgL_procz00_54, BgL_auxz00_2813);
							}
							{	/* Ieee/vector.scm 405 */

								VECTOR_SET(
									((obj_t) BgL_vdestz00_55), BgL_iz00_930, BgL_nvalz00_934);
								{
									long BgL_iz00_2820;

									BgL_iz00_2820 = (BgL_iz00_930 + 1L);
									BgL_iz00_930 = BgL_iz00_2820;
									goto BgL_zc3z04anonymousza31217ze3z87_931;
								}
							}
						}
					}
				else
					{	/* Ieee/vector.scm 403 */
						return BgL_vdestz00_55;
					}
			}
		}

	}



/* vector-map */
	BGL_EXPORTED_DEF obj_t BGl_vectorzd2mapzd2zz__r4_vectors_6_8z00(obj_t
		BgL_procz00_58, obj_t BgL_vz00_59, obj_t BgL_restz00_60)
	{
		{	/* Ieee/vector.scm 413 */
			{	/* Ieee/vector.scm 414 */
				obj_t BgL_nvz00_953;

				BgL_nvz00_953 = create_vector(VECTOR_LENGTH(BgL_vz00_59));
				{	/* Ieee/vector.scm 415 */

					if (NULLP(BgL_restz00_60))
						{
							long BgL_iz00_1702;

							BgL_iz00_1702 = 0L;
						BgL_loopz00_1701:
							if ((BgL_iz00_1702 < VECTOR_LENGTH(BgL_nvz00_953)))
								{	/* Ieee/vector.scm 391 */
									{	/* Ieee/vector.scm 393 */
										obj_t BgL_arg1212z00_1704;

										{	/* Ieee/vector.scm 393 */
											obj_t BgL_arg1215z00_1705;

											BgL_arg1215z00_1705 =
												VECTOR_REF(BgL_vz00_59, BgL_iz00_1702);
											BgL_arg1212z00_1704 =
												BGL_PROCEDURE_CALL1(BgL_procz00_58,
												BgL_arg1215z00_1705);
										}
										VECTOR_SET(BgL_nvz00_953, BgL_iz00_1702,
											BgL_arg1212z00_1704);
									}
									{
										long BgL_iz00_2835;

										BgL_iz00_2835 = (BgL_iz00_1702 + 1L);
										BgL_iz00_1702 = BgL_iz00_2835;
										goto BgL_loopz00_1701;
									}
								}
							else
								{	/* Ieee/vector.scm 391 */
									return BgL_nvz00_953;
								}
						}
					else
						{	/* Ieee/vector.scm 419 */
							bool_t BgL_test1871z00_2837;

							{	/* Ieee/vector.scm 419 */
								obj_t BgL_zc3z04anonymousza31235ze3z87_1907;

								BgL_zc3z04anonymousza31235ze3z87_1907 =
									MAKE_FX_PROCEDURE
									(BGl_z62zc3z04anonymousza31235ze3ze5zz__r4_vectors_6_8z00,
									(int) (1L), (int) (1L));
								PROCEDURE_SET(BgL_zc3z04anonymousza31235ze3z87_1907, (int) (0L),
									BINT(VECTOR_LENGTH(BgL_vz00_59)));
								BgL_test1871z00_2837 =
									CBOOL(BGl_everyz00zz__r4_pairs_and_lists_6_3z00
									(BgL_zc3z04anonymousza31235ze3z87_1907, BNIL));
							}
							if (BgL_test1871z00_2837)
								{	/* Ieee/vector.scm 419 */
									return
										BGl_vectorzd2mapNz12zc0zz__r4_vectors_6_8z00(BgL_procz00_58,
										BgL_nvz00_953, BgL_vz00_59, BgL_restz00_60);
								}
							else
								{	/* Ieee/vector.scm 419 */
									return
										BGl_errorz00zz__errorz00
										(BGl_string1692z00zz__r4_vectors_6_8z00,
										BGl_string1693z00zz__r4_vectors_6_8z00, BgL_restz00_60);
								}
						}
				}
			}
		}

	}



/* &vector-map */
	obj_t BGl_z62vectorzd2mapzb0zz__r4_vectors_6_8z00(obj_t BgL_envz00_1908,
		obj_t BgL_procz00_1909, obj_t BgL_vz00_1910, obj_t BgL_restz00_1911)
	{
		{	/* Ieee/vector.scm 413 */
			{	/* Ieee/vector.scm 414 */
				obj_t BgL_auxz00_2856;
				obj_t BgL_auxz00_2849;

				if (VECTORP(BgL_vz00_1910))
					{	/* Ieee/vector.scm 414 */
						BgL_auxz00_2856 = BgL_vz00_1910;
					}
				else
					{
						obj_t BgL_auxz00_2859;

						BgL_auxz00_2859 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(17349L),
							BGl_string1694z00zz__r4_vectors_6_8z00,
							BGl_string1667z00zz__r4_vectors_6_8z00, BgL_vz00_1910);
						FAILURE(BgL_auxz00_2859, BFALSE, BFALSE);
					}
				if (PROCEDUREP(BgL_procz00_1909))
					{	/* Ieee/vector.scm 414 */
						BgL_auxz00_2849 = BgL_procz00_1909;
					}
				else
					{
						obj_t BgL_auxz00_2852;

						BgL_auxz00_2852 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(17349L),
							BGl_string1694z00zz__r4_vectors_6_8z00,
							BGl_string1695z00zz__r4_vectors_6_8z00, BgL_procz00_1909);
						FAILURE(BgL_auxz00_2852, BFALSE, BFALSE);
					}
				return
					BGl_vectorzd2mapzd2zz__r4_vectors_6_8z00(BgL_auxz00_2849,
					BgL_auxz00_2856, BgL_restz00_1911);
			}
		}

	}



/* &<@anonymous:1235> */
	obj_t BGl_z62zc3z04anonymousza31235ze3ze5zz__r4_vectors_6_8z00(obj_t
		BgL_envz00_1912, obj_t BgL_vz00_1914)
	{
		{	/* Ieee/vector.scm 419 */
			{	/* Ieee/vector.scm 419 */
				long BgL_lenz00_1913;

				BgL_lenz00_1913 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_1912, (int) (0L)));
				{	/* Ieee/vector.scm 419 */
					bool_t BgL_tmpz00_2867;

					if (VECTORP(BgL_vz00_1914))
						{	/* Ieee/vector.scm 419 */
							BgL_tmpz00_2867 =
								(VECTOR_LENGTH(BgL_vz00_1914) == BgL_lenz00_1913);
						}
					else
						{	/* Ieee/vector.scm 419 */
							BgL_tmpz00_2867 = ((bool_t) 0);
						}
					return BBOOL(BgL_tmpz00_2867);
				}
			}
		}

	}



/* vector-map! */
	BGL_EXPORTED_DEF obj_t BGl_vectorzd2mapz12zc0zz__r4_vectors_6_8z00(obj_t
		BgL_procz00_61, obj_t BgL_vz00_62, obj_t BgL_restz00_63)
	{
		{	/* Ieee/vector.scm 427 */
			if (NULLP(BgL_restz00_63))
				{
					long BgL_iz00_1723;

					BgL_iz00_1723 = 0L;
				BgL_loopz00_1722:
					if ((BgL_iz00_1723 < VECTOR_LENGTH(BgL_vz00_62)))
						{	/* Ieee/vector.scm 391 */
							{	/* Ieee/vector.scm 393 */
								obj_t BgL_arg1212z00_1725;

								{	/* Ieee/vector.scm 393 */
									obj_t BgL_arg1215z00_1726;

									BgL_arg1215z00_1726 = VECTOR_REF(BgL_vz00_62, BgL_iz00_1723);
									BgL_arg1212z00_1725 =
										BGL_PROCEDURE_CALL1(BgL_procz00_61, BgL_arg1215z00_1726);
								}
								VECTOR_SET(BgL_vz00_62, BgL_iz00_1723, BgL_arg1212z00_1725);
							}
							{
								long BgL_iz00_2884;

								BgL_iz00_2884 = (BgL_iz00_1723 + 1L);
								BgL_iz00_1723 = BgL_iz00_2884;
								goto BgL_loopz00_1722;
							}
						}
					else
						{	/* Ieee/vector.scm 391 */
							return BgL_vz00_62;
						}
				}
			else
				{	/* Ieee/vector.scm 432 */
					bool_t BgL_test1877z00_2886;

					{	/* Ieee/vector.scm 432 */
						obj_t BgL_zc3z04anonymousza31246ze3z87_1915;

						BgL_zc3z04anonymousza31246ze3z87_1915 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31246ze3ze5zz__r4_vectors_6_8z00,
							(int) (1L), (int) (1L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31246ze3z87_1915, (int) (0L),
							BINT(VECTOR_LENGTH(BgL_vz00_62)));
						BgL_test1877z00_2886 =
							CBOOL(BGl_everyz00zz__r4_pairs_and_lists_6_3z00
							(BgL_zc3z04anonymousza31246ze3z87_1915, BNIL));
					}
					if (BgL_test1877z00_2886)
						{	/* Ieee/vector.scm 432 */
							return
								BGl_vectorzd2mapNz12zc0zz__r4_vectors_6_8z00(BgL_procz00_61,
								BgL_vz00_62, BgL_vz00_62, BgL_restz00_63);
						}
					else
						{	/* Ieee/vector.scm 432 */
							return
								BGl_errorz00zz__errorz00(BGl_string1696z00zz__r4_vectors_6_8z00,
								BGl_string1693z00zz__r4_vectors_6_8z00, BgL_restz00_63);
						}
				}
		}

	}



/* &vector-map! */
	obj_t BGl_z62vectorzd2mapz12za2zz__r4_vectors_6_8z00(obj_t BgL_envz00_1916,
		obj_t BgL_procz00_1917, obj_t BgL_vz00_1918, obj_t BgL_restz00_1919)
	{
		{	/* Ieee/vector.scm 427 */
			{	/* Ieee/vector.scm 428 */
				obj_t BgL_auxz00_2905;
				obj_t BgL_auxz00_2898;

				if (VECTORP(BgL_vz00_1918))
					{	/* Ieee/vector.scm 428 */
						BgL_auxz00_2905 = BgL_vz00_1918;
					}
				else
					{
						obj_t BgL_auxz00_2908;

						BgL_auxz00_2908 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(17894L),
							BGl_string1697z00zz__r4_vectors_6_8z00,
							BGl_string1667z00zz__r4_vectors_6_8z00, BgL_vz00_1918);
						FAILURE(BgL_auxz00_2908, BFALSE, BFALSE);
					}
				if (PROCEDUREP(BgL_procz00_1917))
					{	/* Ieee/vector.scm 428 */
						BgL_auxz00_2898 = BgL_procz00_1917;
					}
				else
					{
						obj_t BgL_auxz00_2901;

						BgL_auxz00_2901 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(17894L),
							BGl_string1697z00zz__r4_vectors_6_8z00,
							BGl_string1695z00zz__r4_vectors_6_8z00, BgL_procz00_1917);
						FAILURE(BgL_auxz00_2901, BFALSE, BFALSE);
					}
				return
					BGl_vectorzd2mapz12zc0zz__r4_vectors_6_8z00(BgL_auxz00_2898,
					BgL_auxz00_2905, BgL_restz00_1919);
			}
		}

	}



/* &<@anonymous:1246> */
	obj_t BGl_z62zc3z04anonymousza31246ze3ze5zz__r4_vectors_6_8z00(obj_t
		BgL_envz00_1920, obj_t BgL_vz00_1922)
	{
		{	/* Ieee/vector.scm 432 */
			{	/* Ieee/vector.scm 432 */
				long BgL_lenz00_1921;

				BgL_lenz00_1921 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_1920, (int) (0L)));
				{	/* Ieee/vector.scm 432 */
					bool_t BgL_tmpz00_2916;

					if (VECTORP(BgL_vz00_1922))
						{	/* Ieee/vector.scm 432 */
							BgL_tmpz00_2916 =
								(VECTOR_LENGTH(BgL_vz00_1922) == BgL_lenz00_1921);
						}
					else
						{	/* Ieee/vector.scm 432 */
							BgL_tmpz00_2916 = ((bool_t) 0);
						}
					return BBOOL(BgL_tmpz00_2916);
				}
			}
		}

	}



/* vector-for-each2 */
	bool_t BGl_vectorzd2forzd2each2z00zz__r4_vectors_6_8z00(obj_t BgL_procz00_64,
		obj_t BgL_vsrcz00_65)
	{
		{	/* Ieee/vector.scm 440 */
			{
				long BgL_iz00_1748;

				BgL_iz00_1748 = 0L;
			BgL_loopz00_1747:
				if ((BgL_iz00_1748 < VECTOR_LENGTH(BgL_vsrcz00_65)))
					{	/* Ieee/vector.scm 443 */
						{	/* Ieee/vector.scm 444 */
							obj_t BgL_arg1252z00_1752;

							BgL_arg1252z00_1752 = VECTOR_REF(BgL_vsrcz00_65, BgL_iz00_1748);
							BGL_PROCEDURE_CALL1(BgL_procz00_64, BgL_arg1252z00_1752);
						}
						{
							long BgL_iz00_2930;

							BgL_iz00_2930 = (BgL_iz00_1748 + 1L);
							BgL_iz00_1748 = BgL_iz00_2930;
							goto BgL_loopz00_1747;
						}
					}
				else
					{	/* Ieee/vector.scm 443 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* vector-for-eachN */
	bool_t BGl_vectorzd2forzd2eachNz00zz__r4_vectors_6_8z00(obj_t BgL_procz00_66,
		obj_t BgL_vsrcz00_67, obj_t BgL_vrestz00_68)
	{
		{	/* Ieee/vector.scm 450 */
			{
				long BgL_iz00_997;

				BgL_iz00_997 = 0L;
			BgL_zc3z04anonymousza31269ze3z87_998:
				if ((BgL_iz00_997 < VECTOR_LENGTH(BgL_vsrcz00_67)))
					{	/* Ieee/vector.scm 454 */
						obj_t BgL_argsz00_1000;

						if (NULLP(BgL_vrestz00_68))
							{	/* Ieee/vector.scm 454 */
								BgL_argsz00_1000 = BNIL;
							}
						else
							{	/* Ieee/vector.scm 454 */
								obj_t BgL_head1041z00_1006;

								BgL_head1041z00_1006 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								{
									obj_t BgL_l1039z00_1783;
									obj_t BgL_tail1042z00_1784;

									BgL_l1039z00_1783 = BgL_vrestz00_68;
									BgL_tail1042z00_1784 = BgL_head1041z00_1006;
								BgL_zc3z04anonymousza31286ze3z87_1782:
									if (NULLP(BgL_l1039z00_1783))
										{	/* Ieee/vector.scm 454 */
											BgL_argsz00_1000 = CDR(BgL_head1041z00_1006);
										}
									else
										{	/* Ieee/vector.scm 454 */
											obj_t BgL_newtail1043z00_1791;

											{	/* Ieee/vector.scm 454 */
												obj_t BgL_arg1305z00_1792;

												{	/* Ieee/vector.scm 454 */
													obj_t BgL_vz00_1793;

													BgL_vz00_1793 = CAR(((obj_t) BgL_l1039z00_1783));
													BgL_arg1305z00_1792 =
														VECTOR_REF(((obj_t) BgL_vz00_1793), BgL_iz00_997);
												}
												BgL_newtail1043z00_1791 =
													MAKE_YOUNG_PAIR(BgL_arg1305z00_1792, BNIL);
											}
											SET_CDR(BgL_tail1042z00_1784, BgL_newtail1043z00_1791);
											{	/* Ieee/vector.scm 454 */
												obj_t BgL_arg1304z00_1794;

												BgL_arg1304z00_1794 = CDR(((obj_t) BgL_l1039z00_1783));
												{
													obj_t BgL_tail1042z00_2950;
													obj_t BgL_l1039z00_2949;

													BgL_l1039z00_2949 = BgL_arg1304z00_1794;
													BgL_tail1042z00_2950 = BgL_newtail1043z00_1791;
													BgL_tail1042z00_1784 = BgL_tail1042z00_2950;
													BgL_l1039z00_1783 = BgL_l1039z00_2949;
													goto BgL_zc3z04anonymousza31286ze3z87_1782;
												}
											}
										}
								}
							}
						{	/* Ieee/vector.scm 455 */
							obj_t BgL_auxz00_2951;

							{	/* Ieee/vector.scm 455 */
								obj_t BgL_arg1272z00_1001;

								BgL_arg1272z00_1001 = VECTOR_REF(BgL_vsrcz00_67, BgL_iz00_997);
								{	/* Ieee/vector.scm 455 */
									obj_t BgL_list1273z00_1002;

									BgL_list1273z00_1002 =
										MAKE_YOUNG_PAIR(BgL_argsz00_1000, BNIL);
									BgL_auxz00_2951 =
										BGl_consza2za2zz__r4_pairs_and_lists_6_3z00
										(BgL_arg1272z00_1001, BgL_list1273z00_1002);
								}
							}
							apply(BgL_procz00_66, BgL_auxz00_2951);
						}
						{
							long BgL_iz00_2956;

							BgL_iz00_2956 = (BgL_iz00_997 + 1L);
							BgL_iz00_997 = BgL_iz00_2956;
							goto BgL_zc3z04anonymousza31269ze3z87_998;
						}
					}
				else
					{	/* Ieee/vector.scm 453 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* vector-for-each */
	BGL_EXPORTED_DEF obj_t BGl_vectorzd2forzd2eachz00zz__r4_vectors_6_8z00(obj_t
		BgL_procz00_69, obj_t BgL_vz00_70, obj_t BgL_restz00_71)
	{
		{	/* Ieee/vector.scm 461 */
			if (NULLP(BgL_restz00_71))
				{	/* Ieee/vector.scm 464 */
					return
						BBOOL(BGl_vectorzd2forzd2each2z00zz__r4_vectors_6_8z00
						(BgL_procz00_69, BgL_vz00_70));
				}
			else
				{	/* Ieee/vector.scm 466 */
					bool_t BgL_test1886z00_2962;

					{	/* Ieee/vector.scm 466 */
						obj_t BgL_zc3z04anonymousza31314ze3z87_1923;

						BgL_zc3z04anonymousza31314ze3z87_1923 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31314ze3ze5zz__r4_vectors_6_8z00,
							(int) (1L), (int) (1L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31314ze3z87_1923, (int) (0L),
							BINT(VECTOR_LENGTH(BgL_vz00_70)));
						BgL_test1886z00_2962 =
							CBOOL(BGl_everyz00zz__r4_pairs_and_lists_6_3z00
							(BgL_zc3z04anonymousza31314ze3z87_1923, BNIL));
					}
					if (BgL_test1886z00_2962)
						{	/* Ieee/vector.scm 466 */
							return
								BBOOL(BGl_vectorzd2forzd2eachNz00zz__r4_vectors_6_8z00
								(BgL_procz00_69, BgL_vz00_70, BgL_restz00_71));
						}
					else
						{	/* Ieee/vector.scm 466 */
							return
								BGl_errorz00zz__errorz00(BGl_string1698z00zz__r4_vectors_6_8z00,
								BGl_string1693z00zz__r4_vectors_6_8z00, BgL_restz00_71);
						}
				}
		}

	}



/* &vector-for-each */
	obj_t BGl_z62vectorzd2forzd2eachz62zz__r4_vectors_6_8z00(obj_t
		BgL_envz00_1924, obj_t BgL_procz00_1925, obj_t BgL_vz00_1926,
		obj_t BgL_restz00_1927)
	{
		{	/* Ieee/vector.scm 461 */
			{	/* Ieee/vector.scm 462 */
				obj_t BgL_auxz00_2982;
				obj_t BgL_auxz00_2975;

				if (VECTORP(BgL_vz00_1926))
					{	/* Ieee/vector.scm 462 */
						BgL_auxz00_2982 = BgL_vz00_1926;
					}
				else
					{
						obj_t BgL_auxz00_2985;

						BgL_auxz00_2985 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(19298L),
							BGl_string1699z00zz__r4_vectors_6_8z00,
							BGl_string1667z00zz__r4_vectors_6_8z00, BgL_vz00_1926);
						FAILURE(BgL_auxz00_2985, BFALSE, BFALSE);
					}
				if (PROCEDUREP(BgL_procz00_1925))
					{	/* Ieee/vector.scm 462 */
						BgL_auxz00_2975 = BgL_procz00_1925;
					}
				else
					{
						obj_t BgL_auxz00_2978;

						BgL_auxz00_2978 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(19298L),
							BGl_string1699z00zz__r4_vectors_6_8z00,
							BGl_string1695z00zz__r4_vectors_6_8z00, BgL_procz00_1925);
						FAILURE(BgL_auxz00_2978, BFALSE, BFALSE);
					}
				return
					BGl_vectorzd2forzd2eachz00zz__r4_vectors_6_8z00(BgL_auxz00_2975,
					BgL_auxz00_2982, BgL_restz00_1927);
			}
		}

	}



/* &<@anonymous:1314> */
	obj_t BGl_z62zc3z04anonymousza31314ze3ze5zz__r4_vectors_6_8z00(obj_t
		BgL_envz00_1928, obj_t BgL_vz00_1930)
	{
		{	/* Ieee/vector.scm 466 */
			{	/* Ieee/vector.scm 466 */
				long BgL_lenz00_1929;

				BgL_lenz00_1929 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_1928, (int) (0L)));
				{	/* Ieee/vector.scm 466 */
					bool_t BgL_tmpz00_2993;

					if (VECTORP(BgL_vz00_1930))
						{	/* Ieee/vector.scm 466 */
							BgL_tmpz00_2993 =
								(VECTOR_LENGTH(BgL_vz00_1930) == BgL_lenz00_1929);
						}
					else
						{	/* Ieee/vector.scm 466 */
							BgL_tmpz00_2993 = ((bool_t) 0);
						}
					return BBOOL(BgL_tmpz00_2993);
				}
			}
		}

	}



/* vector-shrink! */
	BGL_EXPORTED_DEF obj_t BGl_vectorzd2shrinkz12zc0zz__r4_vectors_6_8z00(obj_t
		BgL_vecz00_72, long BgL_nlenz00_73)
	{
		{	/* Ieee/vector.scm 474 */
			return BGL_VECTOR_SHRINK(BgL_vecz00_72, BgL_nlenz00_73);
		}

	}



/* &vector-shrink! */
	obj_t BGl_z62vectorzd2shrinkz12za2zz__r4_vectors_6_8z00(obj_t BgL_envz00_1931,
		obj_t BgL_vecz00_1932, obj_t BgL_nlenz00_1933)
	{
		{	/* Ieee/vector.scm 474 */
			{	/* Ieee/vector.scm 475 */
				long BgL_auxz00_3007;
				obj_t BgL_auxz00_3000;

				{	/* Ieee/vector.scm 475 */
					obj_t BgL_tmpz00_3008;

					if (INTEGERP(BgL_nlenz00_1933))
						{	/* Ieee/vector.scm 475 */
							BgL_tmpz00_3008 = BgL_nlenz00_1933;
						}
					else
						{
							obj_t BgL_auxz00_3011;

							BgL_auxz00_3011 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(19825L),
								BGl_string1700z00zz__r4_vectors_6_8z00,
								BGl_string1665z00zz__r4_vectors_6_8z00, BgL_nlenz00_1933);
							FAILURE(BgL_auxz00_3011, BFALSE, BFALSE);
						}
					BgL_auxz00_3007 = (long) CINT(BgL_tmpz00_3008);
				}
				if (VECTORP(BgL_vecz00_1932))
					{	/* Ieee/vector.scm 475 */
						BgL_auxz00_3000 = BgL_vecz00_1932;
					}
				else
					{
						obj_t BgL_auxz00_3003;

						BgL_auxz00_3003 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1663z00zz__r4_vectors_6_8z00, BINT(19825L),
							BGl_string1700z00zz__r4_vectors_6_8z00,
							BGl_string1667z00zz__r4_vectors_6_8z00, BgL_vecz00_1932);
						FAILURE(BgL_auxz00_3003, BFALSE, BFALSE);
					}
				return
					BGl_vectorzd2shrinkz12zc0zz__r4_vectors_6_8z00(BgL_auxz00_3000,
					BgL_auxz00_3007);
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__r4_vectors_6_8z00(void)
	{
		{	/* Ieee/vector.scm 18 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1701z00zz__r4_vectors_6_8z00));
			return
				BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1701z00zz__r4_vectors_6_8z00));
		}

	}

#ifdef __cplusplus
}
#endif
