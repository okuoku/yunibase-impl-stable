/*===========================================================================*/
/*   (Ieee/control5.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Ieee/control5.scm -indent -o objs/obj_u/Ieee/control5.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___R5_CONTROL_FEATURES_6_4_TYPE_DEFINITIONS
#define BGL___R5_CONTROL_FEATURES_6_4_TYPE_DEFINITIONS
#endif													// BGL___R5_CONTROL_FEATURES_6_4_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t
		BGl_z52getzd2mvalueszd2valz52zz__r5_control_features_6_4z00(int);
	static obj_t BGl_requirezd2initializa7ationz75zz__r5_control_features_6_4z00 =
		BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_callzd2withzd2valuesz00zz__r5_control_features_6_4z00(obj_t, obj_t);
	static obj_t
		BGl_z62z52getzd2mvalueszd2numberz30zz__r5_control_features_6_4z00(obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t
		BGl_z62z52setzd2mvalueszd2valz12z22zz__r5_control_features_6_4z00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62z52getzd2mvalueszd2valz30zz__r5_control_features_6_4z00(obj_t,
		obj_t);
	static obj_t
		BGl_importedzd2moduleszd2initz00zz__r5_control_features_6_4z00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__r5_control_features_6_4z00(void);
	static obj_t
		BGl_z62callzd2withzd2valuesz62zz__r5_control_features_6_4z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_z52setzd2mvalueszd2valz12z40zz__r5_control_features_6_4z00(int, obj_t);
	static obj_t BGl_z62valuesz62zz__r5_control_features_6_4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_z52getzd2mvalueszd2numberz52zz__r5_control_features_6_4z00(void);
	static obj_t
		BGl_z62z52setzd2mvalueszd2numberz12z22zz__r5_control_features_6_4z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL int
		BGl_z52setzd2mvalueszd2numberz12z40zz__r5_control_features_6_4z00(int);
	BGL_EXPORTED_DECL obj_t BGl_valuesz00zz__r5_control_features_6_4z00(obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_z52setzd2mvalueszd2numberz12zd2envz92zz__r5_control_features_6_4z00,
		BgL_bgl_za762za752setza7d2mval1592za7,
		BGl_z62z52setzd2mvalueszd2numberz12z22zz__r5_control_features_6_4z00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_z52setzd2mvalueszd2valz12zd2envz92zz__r5_control_features_6_4z00,
		BgL_bgl_za762za752setza7d2mval1593za7,
		BGl_z62z52setzd2mvalueszd2valz12z22zz__r5_control_features_6_4z00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_callzd2withzd2valueszd2envzd2zz__r5_control_features_6_4z00,
		BgL_bgl_za762callza7d2withza7d1594za7,
		BGl_z62callzd2withzd2valuesz62zz__r5_control_features_6_4z00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_z52getzd2mvalueszd2valzd2envz80zz__r5_control_features_6_4z00,
		BgL_bgl_za762za752getza7d2mval1595za7,
		BGl_z62z52getzd2mvalueszd2valz30zz__r5_control_features_6_4z00, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string1584z00zz__r5_control_features_6_4z00,
		BgL_bgl_string1584za700za7za7_1596za7,
		"/tmp/bigloo/runtime/Ieee/control5.scm", 37);
	      DEFINE_STRING(BGl_string1585z00zz__r5_control_features_6_4z00,
		BgL_bgl_string1585za700za7za7_1597za7, "&%set-mvalues-number!", 21);
	      DEFINE_STRING(BGl_string1586z00zz__r5_control_features_6_4z00,
		BgL_bgl_string1586za700za7za7_1598za7, "bint", 4);
	      DEFINE_STRING(BGl_string1587z00zz__r5_control_features_6_4z00,
		BgL_bgl_string1587za700za7za7_1599za7, "&%get-mvalues-val", 17);
	      DEFINE_STRING(BGl_string1588z00zz__r5_control_features_6_4z00,
		BgL_bgl_string1588za700za7za7_1600za7, "&%set-mvalues-val!", 18);
	      DEFINE_STRING(BGl_string1589z00zz__r5_control_features_6_4z00,
		BgL_bgl_string1589za700za7za7_1601za7, "&call-with-values", 17);
	      DEFINE_STRING(BGl_string1590z00zz__r5_control_features_6_4z00,
		BgL_bgl_string1590za700za7za7_1602za7, "procedure", 9);
	      DEFINE_STRING(BGl_string1591z00zz__r5_control_features_6_4z00,
		BgL_bgl_string1591za700za7za7_1603za7, "__r5_control_features_6_4", 25);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_z52getzd2mvalueszd2numberzd2envz80zz__r5_control_features_6_4z00,
		BgL_bgl_za762za752getza7d2mval1604za7,
		BGl_z62z52getzd2mvalueszd2numberz30zz__r5_control_features_6_4z00, 0L,
		BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valueszd2envzd2zz__r5_control_features_6_4z00,
		BgL_bgl_za762valuesza762za7za7__1605z00, va_generic_entry,
		BGl_z62valuesz62zz__r5_control_features_6_4z00, BUNSPEC, -1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__r5_control_features_6_4z00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(long
		BgL_checksumz00_1321, char *BgL_fromz00_1322)
	{
		{
			if (CBOOL
				(BGl_requirezd2initializa7ationz75zz__r5_control_features_6_4z00))
				{
					BGl_requirezd2initializa7ationz75zz__r5_control_features_6_4z00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__r5_control_features_6_4z00();
					return
						BGl_importedzd2moduleszd2initz00zz__r5_control_features_6_4z00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__r5_control_features_6_4z00(void)
	{
		{	/* Ieee/control5.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* %get-mvalues-number */
	BGL_EXPORTED_DEF int
		BGl_z52getzd2mvalueszd2numberz52zz__r5_control_features_6_4z00(void)
	{
		{	/* Ieee/control5.scm 66 */
			return BGL_MVALUES_NUMBER();
		}

	}



/* &%get-mvalues-number */
	obj_t BGl_z62z52getzd2mvalueszd2numberz30zz__r5_control_features_6_4z00(obj_t
		BgL_envz00_1300)
	{
		{	/* Ieee/control5.scm 66 */
			return
				BINT(BGl_z52getzd2mvalueszd2numberz52zz__r5_control_features_6_4z00());
		}

	}



/* %set-mvalues-number! */
	BGL_EXPORTED_DEF int
		BGl_z52setzd2mvalueszd2numberz12z40zz__r5_control_features_6_4z00(int
		BgL_nz00_3)
	{
		{	/* Ieee/control5.scm 72 */
			return BGL_MVALUES_NUMBER_SET(BgL_nz00_3);
		}

	}



/* &%set-mvalues-number! */
	obj_t
		BGl_z62z52setzd2mvalueszd2numberz12z22zz__r5_control_features_6_4z00(obj_t
		BgL_envz00_1301, obj_t BgL_nz00_1302)
	{
		{	/* Ieee/control5.scm 72 */
			{	/* Ieee/control5.scm 73 */
				int BgL_tmpz00_1333;

				{	/* Ieee/control5.scm 73 */
					int BgL_auxz00_1334;

					{	/* Ieee/control5.scm 73 */
						obj_t BgL_tmpz00_1335;

						if (INTEGERP(BgL_nz00_1302))
							{	/* Ieee/control5.scm 73 */
								BgL_tmpz00_1335 = BgL_nz00_1302;
							}
						else
							{
								obj_t BgL_auxz00_1338;

								BgL_auxz00_1338 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1584z00zz__r5_control_features_6_4z00, BINT(2768L),
									BGl_string1585z00zz__r5_control_features_6_4z00,
									BGl_string1586z00zz__r5_control_features_6_4z00,
									BgL_nz00_1302);
								FAILURE(BgL_auxz00_1338, BFALSE, BFALSE);
							}
						BgL_auxz00_1334 = CINT(BgL_tmpz00_1335);
					}
					BgL_tmpz00_1333 =
						BGl_z52setzd2mvalueszd2numberz12z40zz__r5_control_features_6_4z00
						(BgL_auxz00_1334);
				}
				return BINT(BgL_tmpz00_1333);
			}
		}

	}



/* %get-mvalues-val */
	BGL_EXPORTED_DEF obj_t
		BGl_z52getzd2mvalueszd2valz52zz__r5_control_features_6_4z00(int BgL_nz00_4)
	{
		{	/* Ieee/control5.scm 78 */
			{	/* Ieee/control5.scm 79 */
				obj_t BgL_tmpz00_1320;

				BgL_tmpz00_1320 = BGL_MVALUES_VAL(BgL_nz00_4);
				BGL_MVALUES_VAL_SET(BgL_nz00_4, BUNSPEC);
				return BgL_tmpz00_1320;
			}
		}

	}



/* &%get-mvalues-val */
	obj_t BGl_z62z52getzd2mvalueszd2valz30zz__r5_control_features_6_4z00(obj_t
		BgL_envz00_1303, obj_t BgL_nz00_1304)
	{
		{	/* Ieee/control5.scm 78 */
			{	/* Ieee/control5.scm 79 */
				int BgL_auxz00_1347;

				{	/* Ieee/control5.scm 79 */
					obj_t BgL_tmpz00_1348;

					if (INTEGERP(BgL_nz00_1304))
						{	/* Ieee/control5.scm 79 */
							BgL_tmpz00_1348 = BgL_nz00_1304;
						}
					else
						{
							obj_t BgL_auxz00_1351;

							BgL_auxz00_1351 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string1584z00zz__r5_control_features_6_4z00, BINT(3056L),
								BGl_string1587z00zz__r5_control_features_6_4z00,
								BGl_string1586z00zz__r5_control_features_6_4z00, BgL_nz00_1304);
							FAILURE(BgL_auxz00_1351, BFALSE, BFALSE);
						}
					BgL_auxz00_1347 = CINT(BgL_tmpz00_1348);
				}
				return
					BGl_z52getzd2mvalueszd2valz52zz__r5_control_features_6_4z00
					(BgL_auxz00_1347);
			}
		}

	}



/* %set-mvalues-val! */
	BGL_EXPORTED_DEF obj_t
		BGl_z52setzd2mvalueszd2valz12z40zz__r5_control_features_6_4z00(int
		BgL_nz00_5, obj_t BgL_oz00_6)
	{
		{	/* Ieee/control5.scm 86 */
			return BGL_MVALUES_VAL_SET(BgL_nz00_5, BgL_oz00_6);
		}

	}



/* &%set-mvalues-val! */
	obj_t BGl_z62z52setzd2mvalueszd2valz12z22zz__r5_control_features_6_4z00(obj_t
		BgL_envz00_1305, obj_t BgL_nz00_1306, obj_t BgL_oz00_1307)
	{
		{	/* Ieee/control5.scm 86 */
			{	/* Ieee/control5.scm 87 */
				int BgL_auxz00_1358;

				{	/* Ieee/control5.scm 87 */
					obj_t BgL_tmpz00_1359;

					if (INTEGERP(BgL_nz00_1306))
						{	/* Ieee/control5.scm 87 */
							BgL_tmpz00_1359 = BgL_nz00_1306;
						}
					else
						{
							obj_t BgL_auxz00_1362;

							BgL_auxz00_1362 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string1584z00zz__r5_control_features_6_4z00, BINT(3408L),
								BGl_string1588z00zz__r5_control_features_6_4z00,
								BGl_string1586z00zz__r5_control_features_6_4z00, BgL_nz00_1306);
							FAILURE(BgL_auxz00_1362, BFALSE, BFALSE);
						}
					BgL_auxz00_1358 = CINT(BgL_tmpz00_1359);
				}
				return
					BGl_z52setzd2mvalueszd2valz12z40zz__r5_control_features_6_4z00
					(BgL_auxz00_1358, BgL_oz00_1307);
			}
		}

	}



/* values */
	BGL_EXPORTED_DEF obj_t BGl_valuesz00zz__r5_control_features_6_4z00(obj_t
		BgL_argsz00_7)
	{
		{	/* Ieee/control5.scm 94 */
			if (NULLP(BgL_argsz00_7))
				{	/* Ieee/control5.scm 73 */
					int BgL_tmpz00_1370;

					{	/* Ieee/control5.scm 73 */
						int BgL_tmpz00_1371;

						BgL_tmpz00_1371 = (int) (0L);
						BgL_tmpz00_1370 = BGL_MVALUES_NUMBER_SET(BgL_tmpz00_1371);
					}
					return BINT(BgL_tmpz00_1370);
				}
			else
				{	/* Ieee/control5.scm 95 */
					if (NULLP(CDR(((obj_t) BgL_argsz00_7))))
						{	/* Ieee/control5.scm 97 */
							{	/* Ieee/control5.scm 73 */
								int BgL_tmpz00_1379;

								BgL_tmpz00_1379 = (int) (1L);
								BGL_MVALUES_NUMBER_SET(BgL_tmpz00_1379);
							}
							return CAR(((obj_t) BgL_argsz00_7));
						}
					else
						{	/* Ieee/control5.scm 101 */
							obj_t BgL_res0z00_712;

							BgL_res0z00_712 = CAR(((obj_t) BgL_argsz00_7));
							{	/* Ieee/control5.scm 103 */
								obj_t BgL_g1012z00_714;

								BgL_g1012z00_714 = CDR(((obj_t) BgL_argsz00_7));
								{
									long BgL_iz00_1114;
									obj_t BgL_argsz00_1115;

									BgL_iz00_1114 = 1L;
									BgL_argsz00_1115 = BgL_g1012z00_714;
								BgL_loopz00_1113:
									if (NULLP(BgL_argsz00_1115))
										{	/* Ieee/control5.scm 106 */
											{	/* Ieee/control5.scm 73 */
												int BgL_tmpz00_1390;

												BgL_tmpz00_1390 = (int) (BgL_iz00_1114);
												BGL_MVALUES_NUMBER_SET(BgL_tmpz00_1390);
											}
											return BgL_res0z00_712;
										}
									else
										{	/* Ieee/control5.scm 106 */
											if ((BgL_iz00_1114 == 16L))
												{	/* Ieee/control5.scm 109 */
													{	/* Ieee/control5.scm 73 */
														int BgL_tmpz00_1395;

														BgL_tmpz00_1395 = (int) (-1L);
														BGL_MVALUES_NUMBER_SET(BgL_tmpz00_1395);
													}
													return BgL_argsz00_7;
												}
											else
												{	/* Ieee/control5.scm 109 */
													{	/* Ieee/control5.scm 113 */
														obj_t BgL_arg1063z00_1120;

														BgL_arg1063z00_1120 =
															CAR(((obj_t) BgL_argsz00_1115));
														{	/* Ieee/control5.scm 87 */
															int BgL_tmpz00_1400;

															BgL_tmpz00_1400 = (int) (BgL_iz00_1114);
															BGL_MVALUES_VAL_SET(BgL_tmpz00_1400,
																BgL_arg1063z00_1120);
													}}
													{	/* Ieee/control5.scm 114 */
														long BgL_arg1065z00_1123;
														obj_t BgL_arg1066z00_1124;

														BgL_arg1065z00_1123 = (BgL_iz00_1114 + 1L);
														BgL_arg1066z00_1124 =
															CDR(((obj_t) BgL_argsz00_1115));
														{
															obj_t BgL_argsz00_1407;
															long BgL_iz00_1406;

															BgL_iz00_1406 = BgL_arg1065z00_1123;
															BgL_argsz00_1407 = BgL_arg1066z00_1124;
															BgL_argsz00_1115 = BgL_argsz00_1407;
															BgL_iz00_1114 = BgL_iz00_1406;
															goto BgL_loopz00_1113;
														}
													}
												}
										}
								}
							}
						}
				}
		}

	}



/* &values */
	obj_t BGl_z62valuesz62zz__r5_control_features_6_4z00(obj_t BgL_envz00_1308,
		obj_t BgL_argsz00_1309)
	{
		{	/* Ieee/control5.scm 94 */
			return BGl_valuesz00zz__r5_control_features_6_4z00(BgL_argsz00_1309);
		}

	}



/* call-with-values */
	BGL_EXPORTED_DEF obj_t
		BGl_callzd2withzd2valuesz00zz__r5_control_features_6_4z00(obj_t
		BgL_producerz00_8, obj_t BgL_consumerz00_9)
	{
		{	/* Ieee/control5.scm 119 */
			{	/* Ieee/control5.scm 73 */
				int BgL_tmpz00_1409;

				BgL_tmpz00_1409 = (int) (1L);
				BGL_MVALUES_NUMBER_SET(BgL_tmpz00_1409);
			}
			{	/* Ieee/control5.scm 121 */
				obj_t BgL_res0z00_726;

				BgL_res0z00_726 = BGL_PROCEDURE_CALL0(BgL_producerz00_8);
				{	/* Ieee/control5.scm 121 */
					int BgL_numzd2valueszd2_727;

					BgL_numzd2valueszd2_727 = BGL_MVALUES_NUMBER();
					{	/* Ieee/control5.scm 122 */

						{	/* Ieee/control5.scm 73 */
							int BgL_tmpz00_1416;

							BgL_tmpz00_1416 = (int) (1L);
							BGL_MVALUES_NUMBER_SET(BgL_tmpz00_1416);
						}
						switch ((long) (BgL_numzd2valueszd2_727))
							{
							case -1L:

								return apply(BgL_consumerz00_9, BgL_res0z00_726);
								break;
							case 0L:

								return BGL_PROCEDURE_CALL0(BgL_consumerz00_9);
								break;
							case 1L:

								return BGL_PROCEDURE_CALL1(BgL_consumerz00_9, BgL_res0z00_726);
								break;
							case 2L:

								{	/* Ieee/control5.scm 142 */
									obj_t BgL_arg1074z00_730;

									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1127;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1428;

											BgL_tmpz00_1428 = (int) (1L);
											BgL_tmpz00_1127 = BGL_MVALUES_VAL(BgL_tmpz00_1428);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1431;

											BgL_tmpz00_1431 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1431, BUNSPEC);
										}
										BgL_arg1074z00_730 = BgL_tmpz00_1127;
									}
									return
										BGL_PROCEDURE_CALL2(BgL_consumerz00_9, BgL_res0z00_726,
										BgL_arg1074z00_730);
								}
								break;
							case 3L:

								{	/* Ieee/control5.scm 145 */
									obj_t BgL_arg1075z00_731;
									obj_t BgL_arg1076z00_732;

									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1128;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1439;

											BgL_tmpz00_1439 = (int) (1L);
											BgL_tmpz00_1128 = BGL_MVALUES_VAL(BgL_tmpz00_1439);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1442;

											BgL_tmpz00_1442 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1442, BUNSPEC);
										}
										BgL_arg1075z00_731 = BgL_tmpz00_1128;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1129;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1445;

											BgL_tmpz00_1445 = (int) (2L);
											BgL_tmpz00_1129 = BGL_MVALUES_VAL(BgL_tmpz00_1445);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1448;

											BgL_tmpz00_1448 = (int) (2L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1448, BUNSPEC);
										}
										BgL_arg1076z00_732 = BgL_tmpz00_1129;
									}
									return
										BGL_PROCEDURE_CALL3(BgL_consumerz00_9, BgL_res0z00_726,
										BgL_arg1075z00_731, BgL_arg1076z00_732);
								}
								break;
							case 4L:

								{	/* Ieee/control5.scm 149 */
									obj_t BgL_arg1078z00_733;
									obj_t BgL_arg1079z00_734;
									obj_t BgL_arg1080z00_735;

									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1130;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1457;

											BgL_tmpz00_1457 = (int) (1L);
											BgL_tmpz00_1130 = BGL_MVALUES_VAL(BgL_tmpz00_1457);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1460;

											BgL_tmpz00_1460 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1460, BUNSPEC);
										}
										BgL_arg1078z00_733 = BgL_tmpz00_1130;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1131;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1463;

											BgL_tmpz00_1463 = (int) (2L);
											BgL_tmpz00_1131 = BGL_MVALUES_VAL(BgL_tmpz00_1463);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1466;

											BgL_tmpz00_1466 = (int) (2L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1466, BUNSPEC);
										}
										BgL_arg1079z00_734 = BgL_tmpz00_1131;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1132;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1469;

											BgL_tmpz00_1469 = (int) (3L);
											BgL_tmpz00_1132 = BGL_MVALUES_VAL(BgL_tmpz00_1469);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1472;

											BgL_tmpz00_1472 = (int) (3L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1472, BUNSPEC);
										}
										BgL_arg1080z00_735 = BgL_tmpz00_1132;
									}
									return
										BGL_PROCEDURE_CALL4(BgL_consumerz00_9, BgL_res0z00_726,
										BgL_arg1078z00_733, BgL_arg1079z00_734, BgL_arg1080z00_735);
								}
								break;
							case 5L:

								{	/* Ieee/control5.scm 154 */
									obj_t BgL_arg1082z00_736;
									obj_t BgL_arg1083z00_737;
									obj_t BgL_arg1084z00_738;
									obj_t BgL_arg1085z00_739;

									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1133;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1482;

											BgL_tmpz00_1482 = (int) (1L);
											BgL_tmpz00_1133 = BGL_MVALUES_VAL(BgL_tmpz00_1482);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1485;

											BgL_tmpz00_1485 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1485, BUNSPEC);
										}
										BgL_arg1082z00_736 = BgL_tmpz00_1133;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1134;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1488;

											BgL_tmpz00_1488 = (int) (2L);
											BgL_tmpz00_1134 = BGL_MVALUES_VAL(BgL_tmpz00_1488);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1491;

											BgL_tmpz00_1491 = (int) (2L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1491, BUNSPEC);
										}
										BgL_arg1083z00_737 = BgL_tmpz00_1134;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1135;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1494;

											BgL_tmpz00_1494 = (int) (3L);
											BgL_tmpz00_1135 = BGL_MVALUES_VAL(BgL_tmpz00_1494);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1497;

											BgL_tmpz00_1497 = (int) (3L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1497, BUNSPEC);
										}
										BgL_arg1084z00_738 = BgL_tmpz00_1135;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1136;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1500;

											BgL_tmpz00_1500 = (int) (4L);
											BgL_tmpz00_1136 = BGL_MVALUES_VAL(BgL_tmpz00_1500);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1503;

											BgL_tmpz00_1503 = (int) (4L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1503, BUNSPEC);
										}
										BgL_arg1085z00_739 = BgL_tmpz00_1136;
									}
									return
										BGL_PROCEDURE_CALL5(BgL_consumerz00_9, BgL_res0z00_726,
										BgL_arg1082z00_736, BgL_arg1083z00_737, BgL_arg1084z00_738,
										BgL_arg1085z00_739);
								}
								break;
							case 6L:

								{	/* Ieee/control5.scm 160 */
									obj_t BgL_arg1087z00_740;
									obj_t BgL_arg1088z00_741;
									obj_t BgL_arg1090z00_742;
									obj_t BgL_arg1092z00_743;
									obj_t BgL_arg1097z00_744;

									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1137;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1514;

											BgL_tmpz00_1514 = (int) (1L);
											BgL_tmpz00_1137 = BGL_MVALUES_VAL(BgL_tmpz00_1514);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1517;

											BgL_tmpz00_1517 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1517, BUNSPEC);
										}
										BgL_arg1087z00_740 = BgL_tmpz00_1137;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1138;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1520;

											BgL_tmpz00_1520 = (int) (2L);
											BgL_tmpz00_1138 = BGL_MVALUES_VAL(BgL_tmpz00_1520);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1523;

											BgL_tmpz00_1523 = (int) (2L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1523, BUNSPEC);
										}
										BgL_arg1088z00_741 = BgL_tmpz00_1138;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1139;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1526;

											BgL_tmpz00_1526 = (int) (3L);
											BgL_tmpz00_1139 = BGL_MVALUES_VAL(BgL_tmpz00_1526);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1529;

											BgL_tmpz00_1529 = (int) (3L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1529, BUNSPEC);
										}
										BgL_arg1090z00_742 = BgL_tmpz00_1139;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1140;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1532;

											BgL_tmpz00_1532 = (int) (4L);
											BgL_tmpz00_1140 = BGL_MVALUES_VAL(BgL_tmpz00_1532);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1535;

											BgL_tmpz00_1535 = (int) (4L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1535, BUNSPEC);
										}
										BgL_arg1092z00_743 = BgL_tmpz00_1140;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1141;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1538;

											BgL_tmpz00_1538 = (int) (5L);
											BgL_tmpz00_1141 = BGL_MVALUES_VAL(BgL_tmpz00_1538);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1541;

											BgL_tmpz00_1541 = (int) (5L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1541, BUNSPEC);
										}
										BgL_arg1097z00_744 = BgL_tmpz00_1141;
									}
									return
										BGL_PROCEDURE_CALL6(BgL_consumerz00_9, BgL_res0z00_726,
										BgL_arg1087z00_740, BgL_arg1088z00_741, BgL_arg1090z00_742,
										BgL_arg1092z00_743, BgL_arg1097z00_744);
								}
								break;
							case 7L:

								{	/* Ieee/control5.scm 167 */
									obj_t BgL_arg1102z00_745;
									obj_t BgL_arg1103z00_746;
									obj_t BgL_arg1104z00_747;
									obj_t BgL_arg1114z00_748;
									obj_t BgL_arg1115z00_749;
									obj_t BgL_arg1122z00_750;

									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1142;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1553;

											BgL_tmpz00_1553 = (int) (1L);
											BgL_tmpz00_1142 = BGL_MVALUES_VAL(BgL_tmpz00_1553);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1556;

											BgL_tmpz00_1556 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1556, BUNSPEC);
										}
										BgL_arg1102z00_745 = BgL_tmpz00_1142;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1143;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1559;

											BgL_tmpz00_1559 = (int) (2L);
											BgL_tmpz00_1143 = BGL_MVALUES_VAL(BgL_tmpz00_1559);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1562;

											BgL_tmpz00_1562 = (int) (2L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1562, BUNSPEC);
										}
										BgL_arg1103z00_746 = BgL_tmpz00_1143;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1144;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1565;

											BgL_tmpz00_1565 = (int) (3L);
											BgL_tmpz00_1144 = BGL_MVALUES_VAL(BgL_tmpz00_1565);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1568;

											BgL_tmpz00_1568 = (int) (3L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1568, BUNSPEC);
										}
										BgL_arg1104z00_747 = BgL_tmpz00_1144;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1145;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1571;

											BgL_tmpz00_1571 = (int) (4L);
											BgL_tmpz00_1145 = BGL_MVALUES_VAL(BgL_tmpz00_1571);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1574;

											BgL_tmpz00_1574 = (int) (4L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1574, BUNSPEC);
										}
										BgL_arg1114z00_748 = BgL_tmpz00_1145;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1146;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1577;

											BgL_tmpz00_1577 = (int) (5L);
											BgL_tmpz00_1146 = BGL_MVALUES_VAL(BgL_tmpz00_1577);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1580;

											BgL_tmpz00_1580 = (int) (5L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1580, BUNSPEC);
										}
										BgL_arg1115z00_749 = BgL_tmpz00_1146;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1147;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1583;

											BgL_tmpz00_1583 = (int) (6L);
											BgL_tmpz00_1147 = BGL_MVALUES_VAL(BgL_tmpz00_1583);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1586;

											BgL_tmpz00_1586 = (int) (6L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1586, BUNSPEC);
										}
										BgL_arg1122z00_750 = BgL_tmpz00_1147;
									}
									return
										BGL_PROCEDURE_CALL7(BgL_consumerz00_9, BgL_res0z00_726,
										BgL_arg1102z00_745, BgL_arg1103z00_746, BgL_arg1104z00_747,
										BgL_arg1114z00_748, BgL_arg1115z00_749, BgL_arg1122z00_750);
								}
								break;
							case 8L:

								{	/* Ieee/control5.scm 175 */
									obj_t BgL_arg1123z00_751;
									obj_t BgL_arg1125z00_752;
									obj_t BgL_arg1126z00_753;
									obj_t BgL_arg1127z00_754;
									obj_t BgL_arg1129z00_755;
									obj_t BgL_arg1131z00_756;
									obj_t BgL_arg1132z00_757;

									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1148;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1599;

											BgL_tmpz00_1599 = (int) (1L);
											BgL_tmpz00_1148 = BGL_MVALUES_VAL(BgL_tmpz00_1599);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1602;

											BgL_tmpz00_1602 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1602, BUNSPEC);
										}
										BgL_arg1123z00_751 = BgL_tmpz00_1148;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1149;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1605;

											BgL_tmpz00_1605 = (int) (2L);
											BgL_tmpz00_1149 = BGL_MVALUES_VAL(BgL_tmpz00_1605);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1608;

											BgL_tmpz00_1608 = (int) (2L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1608, BUNSPEC);
										}
										BgL_arg1125z00_752 = BgL_tmpz00_1149;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1150;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1611;

											BgL_tmpz00_1611 = (int) (3L);
											BgL_tmpz00_1150 = BGL_MVALUES_VAL(BgL_tmpz00_1611);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1614;

											BgL_tmpz00_1614 = (int) (3L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1614, BUNSPEC);
										}
										BgL_arg1126z00_753 = BgL_tmpz00_1150;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1151;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1617;

											BgL_tmpz00_1617 = (int) (4L);
											BgL_tmpz00_1151 = BGL_MVALUES_VAL(BgL_tmpz00_1617);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1620;

											BgL_tmpz00_1620 = (int) (4L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1620, BUNSPEC);
										}
										BgL_arg1127z00_754 = BgL_tmpz00_1151;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1152;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1623;

											BgL_tmpz00_1623 = (int) (5L);
											BgL_tmpz00_1152 = BGL_MVALUES_VAL(BgL_tmpz00_1623);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1626;

											BgL_tmpz00_1626 = (int) (5L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1626, BUNSPEC);
										}
										BgL_arg1129z00_755 = BgL_tmpz00_1152;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1153;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1629;

											BgL_tmpz00_1629 = (int) (6L);
											BgL_tmpz00_1153 = BGL_MVALUES_VAL(BgL_tmpz00_1629);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1632;

											BgL_tmpz00_1632 = (int) (6L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1632, BUNSPEC);
										}
										BgL_arg1131z00_756 = BgL_tmpz00_1153;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1154;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1635;

											BgL_tmpz00_1635 = (int) (7L);
											BgL_tmpz00_1154 = BGL_MVALUES_VAL(BgL_tmpz00_1635);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1638;

											BgL_tmpz00_1638 = (int) (7L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1638, BUNSPEC);
										}
										BgL_arg1132z00_757 = BgL_tmpz00_1154;
									}
									return
										BGL_PROCEDURE_CALL8(BgL_consumerz00_9, BgL_res0z00_726,
										BgL_arg1123z00_751, BgL_arg1125z00_752, BgL_arg1126z00_753,
										BgL_arg1127z00_754, BgL_arg1129z00_755, BgL_arg1131z00_756,
										BgL_arg1132z00_757);
								}
								break;
							case 9L:

								{	/* Ieee/control5.scm 184 */
									obj_t BgL_arg1137z00_758;
									obj_t BgL_arg1138z00_759;
									obj_t BgL_arg1140z00_760;
									obj_t BgL_arg1141z00_761;
									obj_t BgL_arg1142z00_762;
									obj_t BgL_arg1143z00_763;
									obj_t BgL_arg1145z00_764;
									obj_t BgL_arg1148z00_765;

									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1155;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1652;

											BgL_tmpz00_1652 = (int) (1L);
											BgL_tmpz00_1155 = BGL_MVALUES_VAL(BgL_tmpz00_1652);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1655;

											BgL_tmpz00_1655 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1655, BUNSPEC);
										}
										BgL_arg1137z00_758 = BgL_tmpz00_1155;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1156;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1658;

											BgL_tmpz00_1658 = (int) (2L);
											BgL_tmpz00_1156 = BGL_MVALUES_VAL(BgL_tmpz00_1658);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1661;

											BgL_tmpz00_1661 = (int) (2L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1661, BUNSPEC);
										}
										BgL_arg1138z00_759 = BgL_tmpz00_1156;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1157;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1664;

											BgL_tmpz00_1664 = (int) (3L);
											BgL_tmpz00_1157 = BGL_MVALUES_VAL(BgL_tmpz00_1664);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1667;

											BgL_tmpz00_1667 = (int) (3L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1667, BUNSPEC);
										}
										BgL_arg1140z00_760 = BgL_tmpz00_1157;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1158;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1670;

											BgL_tmpz00_1670 = (int) (4L);
											BgL_tmpz00_1158 = BGL_MVALUES_VAL(BgL_tmpz00_1670);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1673;

											BgL_tmpz00_1673 = (int) (4L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1673, BUNSPEC);
										}
										BgL_arg1141z00_761 = BgL_tmpz00_1158;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1159;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1676;

											BgL_tmpz00_1676 = (int) (5L);
											BgL_tmpz00_1159 = BGL_MVALUES_VAL(BgL_tmpz00_1676);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1679;

											BgL_tmpz00_1679 = (int) (5L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1679, BUNSPEC);
										}
										BgL_arg1142z00_762 = BgL_tmpz00_1159;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1160;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1682;

											BgL_tmpz00_1682 = (int) (6L);
											BgL_tmpz00_1160 = BGL_MVALUES_VAL(BgL_tmpz00_1682);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1685;

											BgL_tmpz00_1685 = (int) (6L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1685, BUNSPEC);
										}
										BgL_arg1143z00_763 = BgL_tmpz00_1160;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1161;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1688;

											BgL_tmpz00_1688 = (int) (7L);
											BgL_tmpz00_1161 = BGL_MVALUES_VAL(BgL_tmpz00_1688);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1691;

											BgL_tmpz00_1691 = (int) (7L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1691, BUNSPEC);
										}
										BgL_arg1145z00_764 = BgL_tmpz00_1161;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1162;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1694;

											BgL_tmpz00_1694 = (int) (8L);
											BgL_tmpz00_1162 = BGL_MVALUES_VAL(BgL_tmpz00_1694);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1697;

											BgL_tmpz00_1697 = (int) (8L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1697, BUNSPEC);
										}
										BgL_arg1148z00_765 = BgL_tmpz00_1162;
									}
									return
										BGL_PROCEDURE_CALL9(BgL_consumerz00_9, BgL_res0z00_726,
										BgL_arg1137z00_758, BgL_arg1138z00_759, BgL_arg1140z00_760,
										BgL_arg1141z00_761, BgL_arg1142z00_762, BgL_arg1143z00_763,
										BgL_arg1145z00_764, BgL_arg1148z00_765);
								}
								break;
							case 10L:

								{	/* Ieee/control5.scm 194 */
									obj_t BgL_arg1149z00_766;
									obj_t BgL_arg1152z00_767;
									obj_t BgL_arg1153z00_768;
									obj_t BgL_arg1154z00_769;
									obj_t BgL_arg1157z00_770;
									obj_t BgL_arg1158z00_771;
									obj_t BgL_arg1162z00_772;
									obj_t BgL_arg1164z00_773;
									obj_t BgL_arg1166z00_774;

									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1163;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1712;

											BgL_tmpz00_1712 = (int) (1L);
											BgL_tmpz00_1163 = BGL_MVALUES_VAL(BgL_tmpz00_1712);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1715;

											BgL_tmpz00_1715 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1715, BUNSPEC);
										}
										BgL_arg1149z00_766 = BgL_tmpz00_1163;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1164;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1718;

											BgL_tmpz00_1718 = (int) (2L);
											BgL_tmpz00_1164 = BGL_MVALUES_VAL(BgL_tmpz00_1718);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1721;

											BgL_tmpz00_1721 = (int) (2L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1721, BUNSPEC);
										}
										BgL_arg1152z00_767 = BgL_tmpz00_1164;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1165;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1724;

											BgL_tmpz00_1724 = (int) (3L);
											BgL_tmpz00_1165 = BGL_MVALUES_VAL(BgL_tmpz00_1724);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1727;

											BgL_tmpz00_1727 = (int) (3L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1727, BUNSPEC);
										}
										BgL_arg1153z00_768 = BgL_tmpz00_1165;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1166;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1730;

											BgL_tmpz00_1730 = (int) (4L);
											BgL_tmpz00_1166 = BGL_MVALUES_VAL(BgL_tmpz00_1730);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1733;

											BgL_tmpz00_1733 = (int) (4L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1733, BUNSPEC);
										}
										BgL_arg1154z00_769 = BgL_tmpz00_1166;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1167;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1736;

											BgL_tmpz00_1736 = (int) (5L);
											BgL_tmpz00_1167 = BGL_MVALUES_VAL(BgL_tmpz00_1736);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1739;

											BgL_tmpz00_1739 = (int) (5L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1739, BUNSPEC);
										}
										BgL_arg1157z00_770 = BgL_tmpz00_1167;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1168;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1742;

											BgL_tmpz00_1742 = (int) (6L);
											BgL_tmpz00_1168 = BGL_MVALUES_VAL(BgL_tmpz00_1742);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1745;

											BgL_tmpz00_1745 = (int) (6L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1745, BUNSPEC);
										}
										BgL_arg1158z00_771 = BgL_tmpz00_1168;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1169;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1748;

											BgL_tmpz00_1748 = (int) (7L);
											BgL_tmpz00_1169 = BGL_MVALUES_VAL(BgL_tmpz00_1748);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1751;

											BgL_tmpz00_1751 = (int) (7L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1751, BUNSPEC);
										}
										BgL_arg1162z00_772 = BgL_tmpz00_1169;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1170;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1754;

											BgL_tmpz00_1754 = (int) (8L);
											BgL_tmpz00_1170 = BGL_MVALUES_VAL(BgL_tmpz00_1754);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1757;

											BgL_tmpz00_1757 = (int) (8L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1757, BUNSPEC);
										}
										BgL_arg1164z00_773 = BgL_tmpz00_1170;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1171;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1760;

											BgL_tmpz00_1760 = (int) (9L);
											BgL_tmpz00_1171 = BGL_MVALUES_VAL(BgL_tmpz00_1760);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1763;

											BgL_tmpz00_1763 = (int) (9L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1763, BUNSPEC);
										}
										BgL_arg1166z00_774 = BgL_tmpz00_1171;
									}
									return
										BGL_PROCEDURE_CALL10(BgL_consumerz00_9, BgL_res0z00_726,
										BgL_arg1149z00_766, BgL_arg1152z00_767, BgL_arg1153z00_768,
										BgL_arg1154z00_769, BgL_arg1157z00_770, BgL_arg1158z00_771,
										BgL_arg1162z00_772, BgL_arg1164z00_773, BgL_arg1166z00_774);
								}
								break;
							case 11L:

								{	/* Ieee/control5.scm 205 */
									obj_t BgL_arg1171z00_775;
									obj_t BgL_arg1172z00_776;
									obj_t BgL_arg1182z00_777;
									obj_t BgL_arg1183z00_778;
									obj_t BgL_arg1187z00_779;
									obj_t BgL_arg1188z00_780;
									obj_t BgL_arg1189z00_781;
									obj_t BgL_arg1190z00_782;
									obj_t BgL_arg1191z00_783;
									obj_t BgL_arg1193z00_784;

									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1172;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1779;

											BgL_tmpz00_1779 = (int) (1L);
											BgL_tmpz00_1172 = BGL_MVALUES_VAL(BgL_tmpz00_1779);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1782;

											BgL_tmpz00_1782 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1782, BUNSPEC);
										}
										BgL_arg1171z00_775 = BgL_tmpz00_1172;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1173;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1785;

											BgL_tmpz00_1785 = (int) (2L);
											BgL_tmpz00_1173 = BGL_MVALUES_VAL(BgL_tmpz00_1785);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1788;

											BgL_tmpz00_1788 = (int) (2L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1788, BUNSPEC);
										}
										BgL_arg1172z00_776 = BgL_tmpz00_1173;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1174;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1791;

											BgL_tmpz00_1791 = (int) (3L);
											BgL_tmpz00_1174 = BGL_MVALUES_VAL(BgL_tmpz00_1791);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1794;

											BgL_tmpz00_1794 = (int) (3L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1794, BUNSPEC);
										}
										BgL_arg1182z00_777 = BgL_tmpz00_1174;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1175;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1797;

											BgL_tmpz00_1797 = (int) (4L);
											BgL_tmpz00_1175 = BGL_MVALUES_VAL(BgL_tmpz00_1797);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1800;

											BgL_tmpz00_1800 = (int) (4L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1800, BUNSPEC);
										}
										BgL_arg1183z00_778 = BgL_tmpz00_1175;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1176;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1803;

											BgL_tmpz00_1803 = (int) (5L);
											BgL_tmpz00_1176 = BGL_MVALUES_VAL(BgL_tmpz00_1803);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1806;

											BgL_tmpz00_1806 = (int) (5L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1806, BUNSPEC);
										}
										BgL_arg1187z00_779 = BgL_tmpz00_1176;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1177;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1809;

											BgL_tmpz00_1809 = (int) (6L);
											BgL_tmpz00_1177 = BGL_MVALUES_VAL(BgL_tmpz00_1809);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1812;

											BgL_tmpz00_1812 = (int) (6L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1812, BUNSPEC);
										}
										BgL_arg1188z00_780 = BgL_tmpz00_1177;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1178;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1815;

											BgL_tmpz00_1815 = (int) (7L);
											BgL_tmpz00_1178 = BGL_MVALUES_VAL(BgL_tmpz00_1815);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1818;

											BgL_tmpz00_1818 = (int) (7L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1818, BUNSPEC);
										}
										BgL_arg1189z00_781 = BgL_tmpz00_1178;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1179;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1821;

											BgL_tmpz00_1821 = (int) (8L);
											BgL_tmpz00_1179 = BGL_MVALUES_VAL(BgL_tmpz00_1821);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1824;

											BgL_tmpz00_1824 = (int) (8L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1824, BUNSPEC);
										}
										BgL_arg1190z00_782 = BgL_tmpz00_1179;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1180;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1827;

											BgL_tmpz00_1827 = (int) (9L);
											BgL_tmpz00_1180 = BGL_MVALUES_VAL(BgL_tmpz00_1827);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1830;

											BgL_tmpz00_1830 = (int) (9L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1830, BUNSPEC);
										}
										BgL_arg1191z00_783 = BgL_tmpz00_1180;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1181;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1833;

											BgL_tmpz00_1833 = (int) (10L);
											BgL_tmpz00_1181 = BGL_MVALUES_VAL(BgL_tmpz00_1833);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1836;

											BgL_tmpz00_1836 = (int) (10L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1836, BUNSPEC);
										}
										BgL_arg1193z00_784 = BgL_tmpz00_1181;
									}
									return
										BGL_PROCEDURE_CALL11(BgL_consumerz00_9, BgL_res0z00_726,
										BgL_arg1171z00_775, BgL_arg1172z00_776, BgL_arg1182z00_777,
										BgL_arg1183z00_778, BgL_arg1187z00_779, BgL_arg1188z00_780,
										BgL_arg1189z00_781, BgL_arg1190z00_782, BgL_arg1191z00_783,
										BgL_arg1193z00_784);
								}
								break;
							case 12L:

								{	/* Ieee/control5.scm 217 */
									obj_t BgL_arg1194z00_785;
									obj_t BgL_arg1196z00_786;
									obj_t BgL_arg1197z00_787;
									obj_t BgL_arg1198z00_788;
									obj_t BgL_arg1199z00_789;
									obj_t BgL_arg1200z00_790;
									obj_t BgL_arg1201z00_791;
									obj_t BgL_arg1202z00_792;
									obj_t BgL_arg1203z00_793;
									obj_t BgL_arg1206z00_794;
									obj_t BgL_arg1208z00_795;

									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1182;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1853;

											BgL_tmpz00_1853 = (int) (1L);
											BgL_tmpz00_1182 = BGL_MVALUES_VAL(BgL_tmpz00_1853);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1856;

											BgL_tmpz00_1856 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1856, BUNSPEC);
										}
										BgL_arg1194z00_785 = BgL_tmpz00_1182;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1183;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1859;

											BgL_tmpz00_1859 = (int) (2L);
											BgL_tmpz00_1183 = BGL_MVALUES_VAL(BgL_tmpz00_1859);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1862;

											BgL_tmpz00_1862 = (int) (2L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1862, BUNSPEC);
										}
										BgL_arg1196z00_786 = BgL_tmpz00_1183;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1184;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1865;

											BgL_tmpz00_1865 = (int) (3L);
											BgL_tmpz00_1184 = BGL_MVALUES_VAL(BgL_tmpz00_1865);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1868;

											BgL_tmpz00_1868 = (int) (3L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1868, BUNSPEC);
										}
										BgL_arg1197z00_787 = BgL_tmpz00_1184;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1185;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1871;

											BgL_tmpz00_1871 = (int) (4L);
											BgL_tmpz00_1185 = BGL_MVALUES_VAL(BgL_tmpz00_1871);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1874;

											BgL_tmpz00_1874 = (int) (4L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1874, BUNSPEC);
										}
										BgL_arg1198z00_788 = BgL_tmpz00_1185;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1186;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1877;

											BgL_tmpz00_1877 = (int) (5L);
											BgL_tmpz00_1186 = BGL_MVALUES_VAL(BgL_tmpz00_1877);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1880;

											BgL_tmpz00_1880 = (int) (5L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1880, BUNSPEC);
										}
										BgL_arg1199z00_789 = BgL_tmpz00_1186;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1187;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1883;

											BgL_tmpz00_1883 = (int) (6L);
											BgL_tmpz00_1187 = BGL_MVALUES_VAL(BgL_tmpz00_1883);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1886;

											BgL_tmpz00_1886 = (int) (6L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1886, BUNSPEC);
										}
										BgL_arg1200z00_790 = BgL_tmpz00_1187;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1188;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1889;

											BgL_tmpz00_1889 = (int) (7L);
											BgL_tmpz00_1188 = BGL_MVALUES_VAL(BgL_tmpz00_1889);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1892;

											BgL_tmpz00_1892 = (int) (7L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1892, BUNSPEC);
										}
										BgL_arg1201z00_791 = BgL_tmpz00_1188;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1189;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1895;

											BgL_tmpz00_1895 = (int) (8L);
											BgL_tmpz00_1189 = BGL_MVALUES_VAL(BgL_tmpz00_1895);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1898;

											BgL_tmpz00_1898 = (int) (8L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1898, BUNSPEC);
										}
										BgL_arg1202z00_792 = BgL_tmpz00_1189;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1190;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1901;

											BgL_tmpz00_1901 = (int) (9L);
											BgL_tmpz00_1190 = BGL_MVALUES_VAL(BgL_tmpz00_1901);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1904;

											BgL_tmpz00_1904 = (int) (9L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1904, BUNSPEC);
										}
										BgL_arg1203z00_793 = BgL_tmpz00_1190;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1191;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1907;

											BgL_tmpz00_1907 = (int) (10L);
											BgL_tmpz00_1191 = BGL_MVALUES_VAL(BgL_tmpz00_1907);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1910;

											BgL_tmpz00_1910 = (int) (10L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1910, BUNSPEC);
										}
										BgL_arg1206z00_794 = BgL_tmpz00_1191;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1192;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1913;

											BgL_tmpz00_1913 = (int) (11L);
											BgL_tmpz00_1192 = BGL_MVALUES_VAL(BgL_tmpz00_1913);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1916;

											BgL_tmpz00_1916 = (int) (11L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1916, BUNSPEC);
										}
										BgL_arg1208z00_795 = BgL_tmpz00_1192;
									}
									return
										BGL_PROCEDURE_CALL12(BgL_consumerz00_9, BgL_res0z00_726,
										BgL_arg1194z00_785, BgL_arg1196z00_786, BgL_arg1197z00_787,
										BgL_arg1198z00_788, BgL_arg1199z00_789, BgL_arg1200z00_790,
										BgL_arg1201z00_791, BgL_arg1202z00_792, BgL_arg1203z00_793,
										BgL_arg1206z00_794, BgL_arg1208z00_795);
								}
								break;
							case 13L:

								{	/* Ieee/control5.scm 230 */
									obj_t BgL_arg1209z00_796;
									obj_t BgL_arg1210z00_797;
									obj_t BgL_arg1212z00_798;
									obj_t BgL_arg1215z00_799;
									obj_t BgL_arg1216z00_800;
									obj_t BgL_arg1218z00_801;
									obj_t BgL_arg1219z00_802;
									obj_t BgL_arg1220z00_803;
									obj_t BgL_arg1221z00_804;
									obj_t BgL_arg1223z00_805;
									obj_t BgL_arg1225z00_806;
									obj_t BgL_arg1226z00_807;

									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1193;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1934;

											BgL_tmpz00_1934 = (int) (1L);
											BgL_tmpz00_1193 = BGL_MVALUES_VAL(BgL_tmpz00_1934);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1937;

											BgL_tmpz00_1937 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1937, BUNSPEC);
										}
										BgL_arg1209z00_796 = BgL_tmpz00_1193;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1194;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1940;

											BgL_tmpz00_1940 = (int) (2L);
											BgL_tmpz00_1194 = BGL_MVALUES_VAL(BgL_tmpz00_1940);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1943;

											BgL_tmpz00_1943 = (int) (2L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1943, BUNSPEC);
										}
										BgL_arg1210z00_797 = BgL_tmpz00_1194;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1195;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1946;

											BgL_tmpz00_1946 = (int) (3L);
											BgL_tmpz00_1195 = BGL_MVALUES_VAL(BgL_tmpz00_1946);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1949;

											BgL_tmpz00_1949 = (int) (3L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1949, BUNSPEC);
										}
										BgL_arg1212z00_798 = BgL_tmpz00_1195;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1196;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1952;

											BgL_tmpz00_1952 = (int) (4L);
											BgL_tmpz00_1196 = BGL_MVALUES_VAL(BgL_tmpz00_1952);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1955;

											BgL_tmpz00_1955 = (int) (4L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1955, BUNSPEC);
										}
										BgL_arg1215z00_799 = BgL_tmpz00_1196;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1197;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1958;

											BgL_tmpz00_1958 = (int) (5L);
											BgL_tmpz00_1197 = BGL_MVALUES_VAL(BgL_tmpz00_1958);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1961;

											BgL_tmpz00_1961 = (int) (5L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1961, BUNSPEC);
										}
										BgL_arg1216z00_800 = BgL_tmpz00_1197;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1198;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1964;

											BgL_tmpz00_1964 = (int) (6L);
											BgL_tmpz00_1198 = BGL_MVALUES_VAL(BgL_tmpz00_1964);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1967;

											BgL_tmpz00_1967 = (int) (6L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1967, BUNSPEC);
										}
										BgL_arg1218z00_801 = BgL_tmpz00_1198;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1199;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1970;

											BgL_tmpz00_1970 = (int) (7L);
											BgL_tmpz00_1199 = BGL_MVALUES_VAL(BgL_tmpz00_1970);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1973;

											BgL_tmpz00_1973 = (int) (7L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1973, BUNSPEC);
										}
										BgL_arg1219z00_802 = BgL_tmpz00_1199;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1200;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1976;

											BgL_tmpz00_1976 = (int) (8L);
											BgL_tmpz00_1200 = BGL_MVALUES_VAL(BgL_tmpz00_1976);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1979;

											BgL_tmpz00_1979 = (int) (8L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1979, BUNSPEC);
										}
										BgL_arg1220z00_803 = BgL_tmpz00_1200;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1201;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1982;

											BgL_tmpz00_1982 = (int) (9L);
											BgL_tmpz00_1201 = BGL_MVALUES_VAL(BgL_tmpz00_1982);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1985;

											BgL_tmpz00_1985 = (int) (9L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1985, BUNSPEC);
										}
										BgL_arg1221z00_804 = BgL_tmpz00_1201;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1202;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1988;

											BgL_tmpz00_1988 = (int) (10L);
											BgL_tmpz00_1202 = BGL_MVALUES_VAL(BgL_tmpz00_1988);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1991;

											BgL_tmpz00_1991 = (int) (10L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1991, BUNSPEC);
										}
										BgL_arg1223z00_805 = BgL_tmpz00_1202;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1203;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_1994;

											BgL_tmpz00_1994 = (int) (11L);
											BgL_tmpz00_1203 = BGL_MVALUES_VAL(BgL_tmpz00_1994);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_1997;

											BgL_tmpz00_1997 = (int) (11L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_1997, BUNSPEC);
										}
										BgL_arg1225z00_806 = BgL_tmpz00_1203;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1204;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2000;

											BgL_tmpz00_2000 = (int) (12L);
											BgL_tmpz00_1204 = BGL_MVALUES_VAL(BgL_tmpz00_2000);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2003;

											BgL_tmpz00_2003 = (int) (12L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2003, BUNSPEC);
										}
										BgL_arg1226z00_807 = BgL_tmpz00_1204;
									}
									return
										BGL_PROCEDURE_CALL13(BgL_consumerz00_9, BgL_res0z00_726,
										BgL_arg1209z00_796, BgL_arg1210z00_797, BgL_arg1212z00_798,
										BgL_arg1215z00_799, BgL_arg1216z00_800, BgL_arg1218z00_801,
										BgL_arg1219z00_802, BgL_arg1220z00_803, BgL_arg1221z00_804,
										BgL_arg1223z00_805, BgL_arg1225z00_806, BgL_arg1226z00_807);
								}
								break;
							case 14L:

								{	/* Ieee/control5.scm 244 */
									obj_t BgL_arg1227z00_808;
									obj_t BgL_arg1228z00_809;
									obj_t BgL_arg1229z00_810;
									obj_t BgL_arg1230z00_811;
									obj_t BgL_arg1231z00_812;
									obj_t BgL_arg1232z00_813;
									obj_t BgL_arg1233z00_814;
									obj_t BgL_arg1234z00_815;
									obj_t BgL_arg1236z00_816;
									obj_t BgL_arg1238z00_817;
									obj_t BgL_arg1239z00_818;
									obj_t BgL_arg1242z00_819;
									obj_t BgL_arg1244z00_820;

									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1205;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2022;

											BgL_tmpz00_2022 = (int) (1L);
											BgL_tmpz00_1205 = BGL_MVALUES_VAL(BgL_tmpz00_2022);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2025;

											BgL_tmpz00_2025 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2025, BUNSPEC);
										}
										BgL_arg1227z00_808 = BgL_tmpz00_1205;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1206;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2028;

											BgL_tmpz00_2028 = (int) (2L);
											BgL_tmpz00_1206 = BGL_MVALUES_VAL(BgL_tmpz00_2028);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2031;

											BgL_tmpz00_2031 = (int) (2L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2031, BUNSPEC);
										}
										BgL_arg1228z00_809 = BgL_tmpz00_1206;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1207;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2034;

											BgL_tmpz00_2034 = (int) (3L);
											BgL_tmpz00_1207 = BGL_MVALUES_VAL(BgL_tmpz00_2034);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2037;

											BgL_tmpz00_2037 = (int) (3L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2037, BUNSPEC);
										}
										BgL_arg1229z00_810 = BgL_tmpz00_1207;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1208;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2040;

											BgL_tmpz00_2040 = (int) (4L);
											BgL_tmpz00_1208 = BGL_MVALUES_VAL(BgL_tmpz00_2040);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2043;

											BgL_tmpz00_2043 = (int) (4L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2043, BUNSPEC);
										}
										BgL_arg1230z00_811 = BgL_tmpz00_1208;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1209;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2046;

											BgL_tmpz00_2046 = (int) (5L);
											BgL_tmpz00_1209 = BGL_MVALUES_VAL(BgL_tmpz00_2046);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2049;

											BgL_tmpz00_2049 = (int) (5L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2049, BUNSPEC);
										}
										BgL_arg1231z00_812 = BgL_tmpz00_1209;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1210;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2052;

											BgL_tmpz00_2052 = (int) (6L);
											BgL_tmpz00_1210 = BGL_MVALUES_VAL(BgL_tmpz00_2052);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2055;

											BgL_tmpz00_2055 = (int) (6L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2055, BUNSPEC);
										}
										BgL_arg1232z00_813 = BgL_tmpz00_1210;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1211;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2058;

											BgL_tmpz00_2058 = (int) (7L);
											BgL_tmpz00_1211 = BGL_MVALUES_VAL(BgL_tmpz00_2058);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2061;

											BgL_tmpz00_2061 = (int) (7L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2061, BUNSPEC);
										}
										BgL_arg1233z00_814 = BgL_tmpz00_1211;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1212;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2064;

											BgL_tmpz00_2064 = (int) (8L);
											BgL_tmpz00_1212 = BGL_MVALUES_VAL(BgL_tmpz00_2064);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2067;

											BgL_tmpz00_2067 = (int) (8L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2067, BUNSPEC);
										}
										BgL_arg1234z00_815 = BgL_tmpz00_1212;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1213;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2070;

											BgL_tmpz00_2070 = (int) (9L);
											BgL_tmpz00_1213 = BGL_MVALUES_VAL(BgL_tmpz00_2070);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2073;

											BgL_tmpz00_2073 = (int) (9L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2073, BUNSPEC);
										}
										BgL_arg1236z00_816 = BgL_tmpz00_1213;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1214;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2076;

											BgL_tmpz00_2076 = (int) (10L);
											BgL_tmpz00_1214 = BGL_MVALUES_VAL(BgL_tmpz00_2076);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2079;

											BgL_tmpz00_2079 = (int) (10L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2079, BUNSPEC);
										}
										BgL_arg1238z00_817 = BgL_tmpz00_1214;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1215;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2082;

											BgL_tmpz00_2082 = (int) (11L);
											BgL_tmpz00_1215 = BGL_MVALUES_VAL(BgL_tmpz00_2082);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2085;

											BgL_tmpz00_2085 = (int) (11L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2085, BUNSPEC);
										}
										BgL_arg1239z00_818 = BgL_tmpz00_1215;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1216;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2088;

											BgL_tmpz00_2088 = (int) (12L);
											BgL_tmpz00_1216 = BGL_MVALUES_VAL(BgL_tmpz00_2088);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2091;

											BgL_tmpz00_2091 = (int) (12L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2091, BUNSPEC);
										}
										BgL_arg1242z00_819 = BgL_tmpz00_1216;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1217;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2094;

											BgL_tmpz00_2094 = (int) (13L);
											BgL_tmpz00_1217 = BGL_MVALUES_VAL(BgL_tmpz00_2094);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2097;

											BgL_tmpz00_2097 = (int) (13L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2097, BUNSPEC);
										}
										BgL_arg1244z00_820 = BgL_tmpz00_1217;
									}
									return
										BGL_PROCEDURE_CALL14(BgL_consumerz00_9, BgL_res0z00_726,
										BgL_arg1227z00_808, BgL_arg1228z00_809, BgL_arg1229z00_810,
										BgL_arg1230z00_811, BgL_arg1231z00_812, BgL_arg1232z00_813,
										BgL_arg1233z00_814, BgL_arg1234z00_815, BgL_arg1236z00_816,
										BgL_arg1238z00_817, BgL_arg1239z00_818, BgL_arg1242z00_819,
										BgL_arg1244z00_820);
								}
								break;
							case 15L:

								{	/* Ieee/control5.scm 259 */
									obj_t BgL_arg1248z00_821;
									obj_t BgL_arg1249z00_822;
									obj_t BgL_arg1252z00_823;
									obj_t BgL_arg1268z00_824;
									obj_t BgL_arg1272z00_825;
									obj_t BgL_arg1284z00_826;
									obj_t BgL_arg1304z00_827;
									obj_t BgL_arg1305z00_828;
									obj_t BgL_arg1306z00_829;
									obj_t BgL_arg1307z00_830;
									obj_t BgL_arg1308z00_831;
									obj_t BgL_arg1309z00_832;
									obj_t BgL_arg1310z00_833;
									obj_t BgL_arg1311z00_834;

									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1218;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2117;

											BgL_tmpz00_2117 = (int) (1L);
											BgL_tmpz00_1218 = BGL_MVALUES_VAL(BgL_tmpz00_2117);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2120;

											BgL_tmpz00_2120 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2120, BUNSPEC);
										}
										BgL_arg1248z00_821 = BgL_tmpz00_1218;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1219;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2123;

											BgL_tmpz00_2123 = (int) (2L);
											BgL_tmpz00_1219 = BGL_MVALUES_VAL(BgL_tmpz00_2123);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2126;

											BgL_tmpz00_2126 = (int) (2L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2126, BUNSPEC);
										}
										BgL_arg1249z00_822 = BgL_tmpz00_1219;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1220;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2129;

											BgL_tmpz00_2129 = (int) (3L);
											BgL_tmpz00_1220 = BGL_MVALUES_VAL(BgL_tmpz00_2129);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2132;

											BgL_tmpz00_2132 = (int) (3L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2132, BUNSPEC);
										}
										BgL_arg1252z00_823 = BgL_tmpz00_1220;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1221;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2135;

											BgL_tmpz00_2135 = (int) (4L);
											BgL_tmpz00_1221 = BGL_MVALUES_VAL(BgL_tmpz00_2135);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2138;

											BgL_tmpz00_2138 = (int) (4L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2138, BUNSPEC);
										}
										BgL_arg1268z00_824 = BgL_tmpz00_1221;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1222;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2141;

											BgL_tmpz00_2141 = (int) (5L);
											BgL_tmpz00_1222 = BGL_MVALUES_VAL(BgL_tmpz00_2141);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2144;

											BgL_tmpz00_2144 = (int) (5L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2144, BUNSPEC);
										}
										BgL_arg1272z00_825 = BgL_tmpz00_1222;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1223;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2147;

											BgL_tmpz00_2147 = (int) (6L);
											BgL_tmpz00_1223 = BGL_MVALUES_VAL(BgL_tmpz00_2147);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2150;

											BgL_tmpz00_2150 = (int) (6L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2150, BUNSPEC);
										}
										BgL_arg1284z00_826 = BgL_tmpz00_1223;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1224;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2153;

											BgL_tmpz00_2153 = (int) (7L);
											BgL_tmpz00_1224 = BGL_MVALUES_VAL(BgL_tmpz00_2153);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2156;

											BgL_tmpz00_2156 = (int) (7L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2156, BUNSPEC);
										}
										BgL_arg1304z00_827 = BgL_tmpz00_1224;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1225;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2159;

											BgL_tmpz00_2159 = (int) (8L);
											BgL_tmpz00_1225 = BGL_MVALUES_VAL(BgL_tmpz00_2159);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2162;

											BgL_tmpz00_2162 = (int) (8L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2162, BUNSPEC);
										}
										BgL_arg1305z00_828 = BgL_tmpz00_1225;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1226;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2165;

											BgL_tmpz00_2165 = (int) (9L);
											BgL_tmpz00_1226 = BGL_MVALUES_VAL(BgL_tmpz00_2165);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2168;

											BgL_tmpz00_2168 = (int) (9L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2168, BUNSPEC);
										}
										BgL_arg1306z00_829 = BgL_tmpz00_1226;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1227;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2171;

											BgL_tmpz00_2171 = (int) (10L);
											BgL_tmpz00_1227 = BGL_MVALUES_VAL(BgL_tmpz00_2171);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2174;

											BgL_tmpz00_2174 = (int) (10L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2174, BUNSPEC);
										}
										BgL_arg1307z00_830 = BgL_tmpz00_1227;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1228;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2177;

											BgL_tmpz00_2177 = (int) (11L);
											BgL_tmpz00_1228 = BGL_MVALUES_VAL(BgL_tmpz00_2177);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2180;

											BgL_tmpz00_2180 = (int) (11L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2180, BUNSPEC);
										}
										BgL_arg1308z00_831 = BgL_tmpz00_1228;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1229;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2183;

											BgL_tmpz00_2183 = (int) (12L);
											BgL_tmpz00_1229 = BGL_MVALUES_VAL(BgL_tmpz00_2183);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2186;

											BgL_tmpz00_2186 = (int) (12L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2186, BUNSPEC);
										}
										BgL_arg1309z00_832 = BgL_tmpz00_1229;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1230;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2189;

											BgL_tmpz00_2189 = (int) (13L);
											BgL_tmpz00_1230 = BGL_MVALUES_VAL(BgL_tmpz00_2189);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2192;

											BgL_tmpz00_2192 = (int) (13L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2192, BUNSPEC);
										}
										BgL_arg1310z00_833 = BgL_tmpz00_1230;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1231;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2195;

											BgL_tmpz00_2195 = (int) (14L);
											BgL_tmpz00_1231 = BGL_MVALUES_VAL(BgL_tmpz00_2195);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2198;

											BgL_tmpz00_2198 = (int) (14L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2198, BUNSPEC);
										}
										BgL_arg1311z00_834 = BgL_tmpz00_1231;
									}
									return
										BGL_PROCEDURE_CALL15(BgL_consumerz00_9, BgL_res0z00_726,
										BgL_arg1248z00_821, BgL_arg1249z00_822, BgL_arg1252z00_823,
										BgL_arg1268z00_824, BgL_arg1272z00_825, BgL_arg1284z00_826,
										BgL_arg1304z00_827, BgL_arg1305z00_828, BgL_arg1306z00_829,
										BgL_arg1307z00_830, BgL_arg1308z00_831, BgL_arg1309z00_832,
										BgL_arg1310z00_833, BgL_arg1311z00_834);
								}
								break;
							case 16L:

								{	/* Ieee/control5.scm 275 */
									obj_t BgL_arg1312z00_835;
									obj_t BgL_arg1314z00_836;
									obj_t BgL_arg1315z00_837;
									obj_t BgL_arg1316z00_838;
									obj_t BgL_arg1317z00_839;
									obj_t BgL_arg1318z00_840;
									obj_t BgL_arg1319z00_841;
									obj_t BgL_arg1320z00_842;
									obj_t BgL_arg1321z00_843;
									obj_t BgL_arg1322z00_844;
									obj_t BgL_arg1323z00_845;
									obj_t BgL_arg1325z00_846;
									obj_t BgL_arg1326z00_847;
									obj_t BgL_arg1327z00_848;
									obj_t BgL_arg1328z00_849;

									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1232;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2219;

											BgL_tmpz00_2219 = (int) (1L);
											BgL_tmpz00_1232 = BGL_MVALUES_VAL(BgL_tmpz00_2219);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2222;

											BgL_tmpz00_2222 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2222, BUNSPEC);
										}
										BgL_arg1312z00_835 = BgL_tmpz00_1232;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1233;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2225;

											BgL_tmpz00_2225 = (int) (2L);
											BgL_tmpz00_1233 = BGL_MVALUES_VAL(BgL_tmpz00_2225);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2228;

											BgL_tmpz00_2228 = (int) (2L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2228, BUNSPEC);
										}
										BgL_arg1314z00_836 = BgL_tmpz00_1233;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1234;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2231;

											BgL_tmpz00_2231 = (int) (3L);
											BgL_tmpz00_1234 = BGL_MVALUES_VAL(BgL_tmpz00_2231);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2234;

											BgL_tmpz00_2234 = (int) (3L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2234, BUNSPEC);
										}
										BgL_arg1315z00_837 = BgL_tmpz00_1234;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1235;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2237;

											BgL_tmpz00_2237 = (int) (4L);
											BgL_tmpz00_1235 = BGL_MVALUES_VAL(BgL_tmpz00_2237);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2240;

											BgL_tmpz00_2240 = (int) (4L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2240, BUNSPEC);
										}
										BgL_arg1316z00_838 = BgL_tmpz00_1235;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1236;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2243;

											BgL_tmpz00_2243 = (int) (5L);
											BgL_tmpz00_1236 = BGL_MVALUES_VAL(BgL_tmpz00_2243);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2246;

											BgL_tmpz00_2246 = (int) (5L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2246, BUNSPEC);
										}
										BgL_arg1317z00_839 = BgL_tmpz00_1236;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1237;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2249;

											BgL_tmpz00_2249 = (int) (6L);
											BgL_tmpz00_1237 = BGL_MVALUES_VAL(BgL_tmpz00_2249);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2252;

											BgL_tmpz00_2252 = (int) (6L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2252, BUNSPEC);
										}
										BgL_arg1318z00_840 = BgL_tmpz00_1237;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1238;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2255;

											BgL_tmpz00_2255 = (int) (7L);
											BgL_tmpz00_1238 = BGL_MVALUES_VAL(BgL_tmpz00_2255);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2258;

											BgL_tmpz00_2258 = (int) (7L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2258, BUNSPEC);
										}
										BgL_arg1319z00_841 = BgL_tmpz00_1238;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1239;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2261;

											BgL_tmpz00_2261 = (int) (8L);
											BgL_tmpz00_1239 = BGL_MVALUES_VAL(BgL_tmpz00_2261);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2264;

											BgL_tmpz00_2264 = (int) (8L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2264, BUNSPEC);
										}
										BgL_arg1320z00_842 = BgL_tmpz00_1239;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1240;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2267;

											BgL_tmpz00_2267 = (int) (9L);
											BgL_tmpz00_1240 = BGL_MVALUES_VAL(BgL_tmpz00_2267);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2270;

											BgL_tmpz00_2270 = (int) (9L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2270, BUNSPEC);
										}
										BgL_arg1321z00_843 = BgL_tmpz00_1240;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1241;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2273;

											BgL_tmpz00_2273 = (int) (10L);
											BgL_tmpz00_1241 = BGL_MVALUES_VAL(BgL_tmpz00_2273);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2276;

											BgL_tmpz00_2276 = (int) (10L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2276, BUNSPEC);
										}
										BgL_arg1322z00_844 = BgL_tmpz00_1241;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1242;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2279;

											BgL_tmpz00_2279 = (int) (11L);
											BgL_tmpz00_1242 = BGL_MVALUES_VAL(BgL_tmpz00_2279);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2282;

											BgL_tmpz00_2282 = (int) (11L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2282, BUNSPEC);
										}
										BgL_arg1323z00_845 = BgL_tmpz00_1242;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1243;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2285;

											BgL_tmpz00_2285 = (int) (12L);
											BgL_tmpz00_1243 = BGL_MVALUES_VAL(BgL_tmpz00_2285);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2288;

											BgL_tmpz00_2288 = (int) (12L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2288, BUNSPEC);
										}
										BgL_arg1325z00_846 = BgL_tmpz00_1243;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1244;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2291;

											BgL_tmpz00_2291 = (int) (13L);
											BgL_tmpz00_1244 = BGL_MVALUES_VAL(BgL_tmpz00_2291);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2294;

											BgL_tmpz00_2294 = (int) (13L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2294, BUNSPEC);
										}
										BgL_arg1326z00_847 = BgL_tmpz00_1244;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1245;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2297;

											BgL_tmpz00_2297 = (int) (14L);
											BgL_tmpz00_1245 = BGL_MVALUES_VAL(BgL_tmpz00_2297);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2300;

											BgL_tmpz00_2300 = (int) (14L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2300, BUNSPEC);
										}
										BgL_arg1327z00_848 = BgL_tmpz00_1245;
									}
									{	/* Ieee/control5.scm 79 */
										obj_t BgL_tmpz00_1246;

										{	/* Ieee/control5.scm 79 */
											int BgL_tmpz00_2303;

											BgL_tmpz00_2303 = (int) (15L);
											BgL_tmpz00_1246 = BGL_MVALUES_VAL(BgL_tmpz00_2303);
										}
										{	/* Ieee/control5.scm 87 */
											int BgL_tmpz00_2306;

											BgL_tmpz00_2306 = (int) (15L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2306, BUNSPEC);
										}
										BgL_arg1328z00_849 = BgL_tmpz00_1246;
									}
									return
										BGL_PROCEDURE_CALL16(BgL_consumerz00_9, BgL_res0z00_726,
										BgL_arg1312z00_835, BgL_arg1314z00_836, BgL_arg1315z00_837,
										BgL_arg1316z00_838, BgL_arg1317z00_839, BgL_arg1318z00_840,
										BgL_arg1319z00_841, BgL_arg1320z00_842, BgL_arg1321z00_843,
										BgL_arg1322z00_844, BgL_arg1323z00_845, BgL_arg1325z00_846,
										BgL_arg1326z00_847, BgL_arg1327z00_848, BgL_arg1328z00_849);
								}
								break;
							default:
								return apply(BgL_consumerz00_9, BgL_res0z00_726);
							}
					}
				}
			}
		}

	}



/* &call-with-values */
	obj_t BGl_z62callzd2withzd2valuesz62zz__r5_control_features_6_4z00(obj_t
		BgL_envz00_1310, obj_t BgL_producerz00_1311, obj_t BgL_consumerz00_1312)
	{
		{	/* Ieee/control5.scm 119 */
			{	/* Ieee/control5.scm 120 */
				obj_t BgL_auxz00_2339;
				obj_t BgL_auxz00_2332;

				if (PROCEDUREP(BgL_consumerz00_1312))
					{	/* Ieee/control5.scm 120 */
						BgL_auxz00_2339 = BgL_consumerz00_1312;
					}
				else
					{
						obj_t BgL_auxz00_2342;

						BgL_auxz00_2342 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1584z00zz__r5_control_features_6_4z00, BINT(4604L),
							BGl_string1589z00zz__r5_control_features_6_4z00,
							BGl_string1590z00zz__r5_control_features_6_4z00,
							BgL_consumerz00_1312);
						FAILURE(BgL_auxz00_2342, BFALSE, BFALSE);
					}
				if (PROCEDUREP(BgL_producerz00_1311))
					{	/* Ieee/control5.scm 120 */
						BgL_auxz00_2332 = BgL_producerz00_1311;
					}
				else
					{
						obj_t BgL_auxz00_2335;

						BgL_auxz00_2335 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1584z00zz__r5_control_features_6_4z00, BINT(4604L),
							BGl_string1589z00zz__r5_control_features_6_4z00,
							BGl_string1590z00zz__r5_control_features_6_4z00,
							BgL_producerz00_1311);
						FAILURE(BgL_auxz00_2335, BFALSE, BFALSE);
					}
				return
					BGl_callzd2withzd2valuesz00zz__r5_control_features_6_4z00
					(BgL_auxz00_2332, BgL_auxz00_2339);
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__r5_control_features_6_4z00(void)
	{
		{	/* Ieee/control5.scm 14 */
			return
				BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1591z00zz__r5_control_features_6_4z00));
		}

	}

#ifdef __cplusplus
}
#endif
