/*===========================================================================*/
/*   (Ieee/control.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Ieee/control.scm -indent -o objs/obj_u/Ieee/control.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___R4_CONTROL_FEATURES_6_9_TYPE_DEFINITIONS
#define BGL___R4_CONTROL_FEATURES_6_9_TYPE_DEFINITIONS
#endif													// BGL___R4_CONTROL_FEATURES_6_9_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t BGl_mapz12z12zz__r4_control_features_6_9z00(obj_t,
		obj_t);
	extern bool_t BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_appendzd2mapz12zc0zz__r4_control_features_6_9z00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__r4_control_features_6_9z00 =
		BUNSPEC;
	static obj_t
		BGl_z62zc3z04anonymousza31338ze3ze5zz__r4_control_features_6_9z00(obj_t);
	static obj_t BGl_z62forcez62zz__r4_control_features_6_9z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_mapzd22zd2zz__r4_control_features_6_9z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_forzd2eachzd22z00zz__r4_control_features_6_9z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_applyz00zz__r4_control_features_6_9z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_filterzd2mapzd22z00zz__r4_control_features_6_9z00(obj_t,
		obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evaluatez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_EXPORTED_DECL obj_t
		BGl_callzd2withzd2currentzd2continuationzd2zz__r4_control_features_6_9z00
		(obj_t);
	extern obj_t BGl_setzd2evaluationzd2contextz12z12zz__evaluatez00(obj_t);
	static obj_t BGl_z62mapz62zz__r4_control_features_6_9z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62forzd2eachzd22z62zz__r4_control_features_6_9z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62forzd2eachzb0zz__r4_control_features_6_9z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_toplevelzd2initzd2zz__r4_control_features_6_9z00(void);
	static obj_t BGl_z62mapzd22zb0zz__r4_control_features_6_9z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_procedurezf3zf3zz__r4_control_features_6_9z00(obj_t);
	static obj_t
		BGl_importedzd2moduleszd2initz00zz__r4_control_features_6_9z00(void);
	static obj_t BGl_z62applyz62zz__r4_control_features_6_9z00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_gczd2rootszd2initz00zz__r4_control_features_6_9z00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_filterzd2mapzd2zz__r4_control_features_6_9z00(obj_t, obj_t);
	static obj_t BGl_z62filterz62zz__r4_control_features_6_9z00(obj_t, obj_t,
		obj_t);
	extern obj_t call_cc(obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31198ze3ze5zz__r4_control_features_6_9z00(obj_t,
		obj_t);
	extern obj_t BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t bgl_reverse_bang(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_appendzd2mapzd2zz__r4_control_features_6_9z00(obj_t, obj_t);
	static obj_t BGl_z62procedurezf3z91zz__r4_control_features_6_9z00(obj_t,
		obj_t);
	static obj_t BGl_z62filterzd2mapzb0zz__r4_control_features_6_9z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_filterz12z12zz__r4_control_features_6_9z00(obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31330ze3ze5zz__r4_control_features_6_9z00(obj_t,
		obj_t);
	static obj_t BGl_z62appendzd2mapz12za2zz__r4_control_features_6_9z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62filterz12z70zz__r4_control_features_6_9z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62makezd2promisezb0zz__r4_control_features_6_9z00(obj_t,
		obj_t);
	static obj_t BGl_z62appendzd2mapzb0zz__r4_control_features_6_9z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zz__r4_control_features_6_9z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2promisezd2zz__r4_control_features_6_9z00(obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31332ze3ze5zz__r4_control_features_6_9z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_callzf2cczf2zz__r4_control_features_6_9z00(obj_t);
	static obj_t BGl_z62callzf2ccz90zz__r4_control_features_6_9z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_mapz00zz__r4_control_features_6_9z00(obj_t,
		obj_t);
	static obj_t BGl_loopze70ze7zz__r4_control_features_6_9z00(obj_t, obj_t);
	static obj_t BGl_loopze71ze7zz__r4_control_features_6_9z00(obj_t, obj_t);
	static obj_t BGl_loopze72ze7zz__r4_control_features_6_9z00(obj_t, obj_t);
	static obj_t BGl_loopze73ze7zz__r4_control_features_6_9z00(obj_t, obj_t);
	static obj_t BGl_loopze74ze7zz__r4_control_features_6_9z00(obj_t);
	static obj_t BGl_appendzd2map2z12zc0zz__r4_control_features_6_9z00(obj_t,
		obj_t);
	static obj_t
		BGl_z62callzd2withzd2currentzd2continuationzb0zz__r4_control_features_6_9z00
		(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_filterz00zz__r4_control_features_6_9z00(obj_t,
		obj_t);
	static obj_t BGl_z62dynamiczd2windzb0zz__r4_control_features_6_9z00(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_forcez00zz__r4_control_features_6_9z00(obj_t);
	extern obj_t BGl_getzd2evaluationzd2contextz00zz__evaluatez00(void);
	static obj_t BGl_z62mapz12z70zz__r4_control_features_6_9z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_forzd2eachzd2zz__r4_control_features_6_9z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_dynamiczd2windzd2zz__r4_control_features_6_9z00(obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31328ze3ze5zz__r4_control_features_6_9z00(obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_appendzd2mapz12zd2envz12zz__r4_control_features_6_9z00,
		BgL_bgl_za762appendza7d2mapza71669za7, va_generic_entry,
		BGl_z62appendzd2mapz12za2zz__r4_control_features_6_9z00, BUNSPEC, -2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_forzd2eachzd22zd2envzd2zz__r4_control_features_6_9z00,
		BgL_bgl_za762forza7d2eachza7d21670za7,
		BGl_z62forzd2eachzd22z62zz__r4_control_features_6_9z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_forzd2eachzd2envz00zz__r4_control_features_6_9z00,
		BgL_bgl_za762forza7d2eachza7b01671za7, va_generic_entry,
		BGl_z62forzd2eachzb0zz__r4_control_features_6_9z00, BUNSPEC, -2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2promisezd2envz00zz__r4_control_features_6_9z00,
		BgL_bgl_za762makeza7d2promis1672z00,
		BGl_z62makezd2promisezb0zz__r4_control_features_6_9z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_mapzd2envzd2zz__r4_control_features_6_9z00,
		BgL_bgl_za762mapza762za7za7__r4_1673z00, va_generic_entry,
		BGl_z62mapz62zz__r4_control_features_6_9z00, BUNSPEC, -2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_appendzd2mapzd2envz00zz__r4_control_features_6_9z00,
		BgL_bgl_za762appendza7d2mapza71674za7, va_generic_entry,
		BGl_z62appendzd2mapzb0zz__r4_control_features_6_9z00, BUNSPEC, -2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_filterzd2envzd2zz__r4_control_features_6_9z00,
		BgL_bgl_za762filterza762za7za7__1675z00,
		BGl_z62filterz62zz__r4_control_features_6_9z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_filterz12zd2envzc0zz__r4_control_features_6_9z00,
		BgL_bgl_za762filterza712za770za71676z00,
		BGl_z62filterz12z70zz__r4_control_features_6_9z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_mapzd22zd2envz00zz__r4_control_features_6_9z00,
		BgL_bgl_za762mapza7d22za7b0za7za7_1677za7,
		BGl_z62mapzd22zb0zz__r4_control_features_6_9z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_procedurezf3zd2envz21zz__r4_control_features_6_9z00,
		BgL_bgl_za762procedureza7f3za71678za7,
		BGl_z62procedurezf3z91zz__r4_control_features_6_9z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_callzd2withzd2currentzd2continuationzd2envz00zz__r4_control_features_6_9z00,
		BgL_bgl_za762callza7d2withza7d1679za7,
		BGl_z62callzd2withzd2currentzd2continuationzb0zz__r4_control_features_6_9z00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_forcezd2envzd2zz__r4_control_features_6_9z00,
		BgL_bgl_za762forceza762za7za7__r1680z00,
		BGl_z62forcez62zz__r4_control_features_6_9z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_applyzd2envzd2zz__r4_control_features_6_9z00,
		BgL_bgl_za762applyza762za7za7__r1681z00, va_generic_entry,
		BGl_z62applyz62zz__r4_control_features_6_9z00, BUNSPEC, -3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_filterzd2mapzd2envz00zz__r4_control_features_6_9z00,
		BgL_bgl_za762filterza7d2mapza71682za7, va_generic_entry,
		BGl_z62filterzd2mapzb0zz__r4_control_features_6_9z00, BUNSPEC, -2);
	      DEFINE_STRING(BGl_string1650z00zz__r4_control_features_6_9z00,
		BgL_bgl_string1650za700za7za7_1683za7,
		"/tmp/bigloo/runtime/Ieee/control.scm", 36);
	      DEFINE_STRING(BGl_string1651z00zz__r4_control_features_6_9z00,
		BgL_bgl_string1651za700za7za7_1684za7, "&apply", 6);
	      DEFINE_STRING(BGl_string1652z00zz__r4_control_features_6_9z00,
		BgL_bgl_string1652za700za7za7_1685za7, "procedure", 9);
	      DEFINE_STRING(BGl_string1653z00zz__r4_control_features_6_9z00,
		BgL_bgl_string1653za700za7za7_1686za7, "&map-2", 6);
	      DEFINE_STRING(BGl_string1654z00zz__r4_control_features_6_9z00,
		BgL_bgl_string1654za700za7za7_1687za7, "pair-nil", 8);
	      DEFINE_STRING(BGl_string1655z00zz__r4_control_features_6_9z00,
		BgL_bgl_string1655za700za7za7_1688za7, "&map", 4);
	      DEFINE_STRING(BGl_string1656z00zz__r4_control_features_6_9z00,
		BgL_bgl_string1656za700za7za7_1689za7, "&map!", 5);
	      DEFINE_STRING(BGl_string1657z00zz__r4_control_features_6_9z00,
		BgL_bgl_string1657za700za7za7_1690za7, "&append-map", 11);
	      DEFINE_STRING(BGl_string1658z00zz__r4_control_features_6_9z00,
		BgL_bgl_string1658za700za7za7_1691za7, "&append-map!", 12);
	      DEFINE_STRING(BGl_string1659z00zz__r4_control_features_6_9z00,
		BgL_bgl_string1659za700za7za7_1692za7, "&filter-map", 11);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_mapz12zd2envzc0zz__r4_control_features_6_9z00,
		BgL_bgl_za762mapza712za770za7za7__1693za7, va_generic_entry,
		BGl_z62mapz12z70zz__r4_control_features_6_9z00, BUNSPEC, -2);
	      DEFINE_STRING(BGl_string1660z00zz__r4_control_features_6_9z00,
		BgL_bgl_string1660za700za7za7_1694za7, "&for-each-2", 11);
	      DEFINE_STRING(BGl_string1661z00zz__r4_control_features_6_9z00,
		BgL_bgl_string1661za700za7za7_1695za7, "&for-each", 9);
	      DEFINE_STRING(BGl_string1662z00zz__r4_control_features_6_9z00,
		BgL_bgl_string1662za700za7za7_1696za7, "&filter", 7);
	      DEFINE_STRING(BGl_string1663z00zz__r4_control_features_6_9z00,
		BgL_bgl_string1663za700za7za7_1697za7, "&filter!", 8);
	      DEFINE_STRING(BGl_string1664z00zz__r4_control_features_6_9z00,
		BgL_bgl_string1664za700za7za7_1698za7, "&make-promise", 13);
	      DEFINE_STRING(BGl_string1665z00zz__r4_control_features_6_9z00,
		BgL_bgl_string1665za700za7za7_1699za7, "&call/cc", 8);
	      DEFINE_STRING(BGl_string1666z00zz__r4_control_features_6_9z00,
		BgL_bgl_string1666za700za7za7_1700za7, "&call-with-current-continuation",
		31);
	      DEFINE_STRING(BGl_string1667z00zz__r4_control_features_6_9z00,
		BgL_bgl_string1667za700za7za7_1701za7, "&dynamic-wind", 13);
	      DEFINE_STRING(BGl_string1668z00zz__r4_control_features_6_9z00,
		BgL_bgl_string1668za700za7za7_1702za7, "__r4_control_features_6_9", 25);
	extern obj_t BGl_cdrzd2envzd2zz__r4_pairs_and_lists_6_3z00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_dynamiczd2windzd2envz00zz__r4_control_features_6_9z00,
		BgL_bgl_za762dynamicza7d2win1703z00,
		BGl_z62dynamiczd2windzb0zz__r4_control_features_6_9z00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_callzf2cczd2envz20zz__r4_control_features_6_9z00,
		BgL_bgl_za762callza7f2ccza790za71704z00,
		BGl_z62callzf2ccz90zz__r4_control_features_6_9z00, 0L, BUNSPEC, 1);
	extern obj_t BGl_carzd2envzd2zz__r4_pairs_and_lists_6_3z00;

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__r4_control_features_6_9z00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long
		BgL_checksumz00_1692, char *BgL_fromz00_1693)
	{
		{
			if (CBOOL
				(BGl_requirezd2initializa7ationz75zz__r4_control_features_6_9z00))
				{
					BGl_requirezd2initializa7ationz75zz__r4_control_features_6_9z00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__r4_control_features_6_9z00();
					BGl_importedzd2moduleszd2initz00zz__r4_control_features_6_9z00();
					return BGl_toplevelzd2initzd2zz__r4_control_features_6_9z00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__r4_control_features_6_9z00(void)
	{
		{	/* Ieee/control.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__r4_control_features_6_9z00(void)
	{
		{	/* Ieee/control.scm 14 */
			return BUNSPEC;
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zz__r4_control_features_6_9z00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_741;

				BgL_headz00_741 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1265;
					obj_t BgL_tailz00_1266;

					BgL_prevz00_1265 = BgL_headz00_741;
					BgL_tailz00_1266 = BgL_l1z00_1;
				BgL_loopz00_1264:
					if (PAIRP(BgL_tailz00_1266))
						{
							obj_t BgL_newzd2prevzd2_1272;

							BgL_newzd2prevzd2_1272 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1266), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1265, BgL_newzd2prevzd2_1272);
							{
								obj_t BgL_tailz00_1708;
								obj_t BgL_prevz00_1707;

								BgL_prevz00_1707 = BgL_newzd2prevzd2_1272;
								BgL_tailz00_1708 = CDR(BgL_tailz00_1266);
								BgL_tailz00_1266 = BgL_tailz00_1708;
								BgL_prevz00_1265 = BgL_prevz00_1707;
								goto BgL_loopz00_1264;
							}
						}
					else
						{
							BNIL;
						}
				}
				return CDR(BgL_headz00_741);
			}
		}

	}



/* procedure? */
	BGL_EXPORTED_DEF bool_t
		BGl_procedurezf3zf3zz__r4_control_features_6_9z00(obj_t BgL_objz00_3)
	{
		{	/* Ieee/control.scm 79 */
			return PROCEDUREP(BgL_objz00_3);
		}

	}



/* &procedure? */
	obj_t BGl_z62procedurezf3z91zz__r4_control_features_6_9z00(obj_t
		BgL_envz00_1555, obj_t BgL_objz00_1556)
	{
		{	/* Ieee/control.scm 79 */
			return
				BBOOL(BGl_procedurezf3zf3zz__r4_control_features_6_9z00
				(BgL_objz00_1556));
		}

	}



/* apply */
	BGL_EXPORTED_DEF obj_t BGl_applyz00zz__r4_control_features_6_9z00(obj_t
		BgL_procz00_4, obj_t BgL_argsz00_5, obj_t BgL_optz00_6)
	{
		{	/* Ieee/control.scm 85 */
			{	/* Ieee/control.scm 86 */
				obj_t BgL_argsz00_749;

				if (PAIRP(BgL_optz00_6))
					{	/* Ieee/control.scm 86 */
						BgL_argsz00_749 =
							MAKE_YOUNG_PAIR(BgL_argsz00_5,
							BGl_loopze74ze7zz__r4_control_features_6_9z00(BgL_optz00_6));
					}
				else
					{	/* Ieee/control.scm 86 */
						BgL_argsz00_749 = BgL_argsz00_5;
					}
				return apply(BgL_procz00_4, BgL_argsz00_749);
			}
		}

	}



/* loop~4 */
	obj_t BGl_loopze74ze7zz__r4_control_features_6_9z00(obj_t BgL_optz00_753)
	{
		{	/* Ieee/control.scm 87 */
			{	/* Ieee/control.scm 88 */
				bool_t BgL_test1708z00_1720;

				{	/* Ieee/control.scm 88 */
					obj_t BgL_tmpz00_1721;

					BgL_tmpz00_1721 = CDR(((obj_t) BgL_optz00_753));
					BgL_test1708z00_1720 = PAIRP(BgL_tmpz00_1721);
				}
				if (BgL_test1708z00_1720)
					{	/* Ieee/control.scm 89 */
						obj_t BgL_arg1074z00_757;
						obj_t BgL_arg1075z00_758;

						BgL_arg1074z00_757 = CAR(((obj_t) BgL_optz00_753));
						{	/* Ieee/control.scm 89 */
							obj_t BgL_arg1076z00_759;

							BgL_arg1076z00_759 = CDR(((obj_t) BgL_optz00_753));
							BgL_arg1075z00_758 =
								BGl_loopze74ze7zz__r4_control_features_6_9z00
								(BgL_arg1076z00_759);
						}
						return MAKE_YOUNG_PAIR(BgL_arg1074z00_757, BgL_arg1075z00_758);
					}
				else
					{	/* Ieee/control.scm 88 */
						return CAR(((obj_t) BgL_optz00_753));
					}
			}
		}

	}



/* &apply */
	obj_t BGl_z62applyz62zz__r4_control_features_6_9z00(obj_t BgL_envz00_1557,
		obj_t BgL_procz00_1558, obj_t BgL_argsz00_1559, obj_t BgL_optz00_1560)
	{
		{	/* Ieee/control.scm 85 */
			{	/* Ieee/control.scm 86 */
				obj_t BgL_auxz00_1733;

				if (PROCEDUREP(BgL_procz00_1558))
					{	/* Ieee/control.scm 86 */
						BgL_auxz00_1733 = BgL_procz00_1558;
					}
				else
					{
						obj_t BgL_auxz00_1736;

						BgL_auxz00_1736 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1650z00zz__r4_control_features_6_9z00, BINT(3301L),
							BGl_string1651z00zz__r4_control_features_6_9z00,
							BGl_string1652z00zz__r4_control_features_6_9z00,
							BgL_procz00_1558);
						FAILURE(BgL_auxz00_1736, BFALSE, BFALSE);
					}
				return
					BGl_applyz00zz__r4_control_features_6_9z00(BgL_auxz00_1733,
					BgL_argsz00_1559, BgL_optz00_1560);
			}
		}

	}



/* map-2 */
	BGL_EXPORTED_DEF obj_t BGl_mapzd22zd2zz__r4_control_features_6_9z00(obj_t
		BgL_fz00_7, obj_t BgL_lz00_8)
	{
		{	/* Ieee/control.scm 97 */
			{
				obj_t BgL_lz00_1298;
				obj_t BgL_resz00_1299;

				BgL_lz00_1298 = BgL_lz00_8;
				BgL_resz00_1299 = BNIL;
			BgL_loopz00_1297:
				if (NULLP(BgL_lz00_1298))
					{	/* Ieee/control.scm 100 */
						return bgl_reverse_bang(BgL_resz00_1299);
					}
				else
					{	/* Ieee/control.scm 102 */
						obj_t BgL_arg1082z00_1306;
						obj_t BgL_arg1083z00_1307;

						BgL_arg1082z00_1306 = CDR(((obj_t) BgL_lz00_1298));
						{	/* Ieee/control.scm 102 */
							obj_t BgL_arg1084z00_1308;

							{	/* Ieee/control.scm 102 */
								obj_t BgL_arg1085z00_1309;

								BgL_arg1085z00_1309 = CAR(((obj_t) BgL_lz00_1298));
								BgL_arg1084z00_1308 =
									BGL_PROCEDURE_CALL1(BgL_fz00_7, BgL_arg1085z00_1309);
							}
							BgL_arg1083z00_1307 =
								MAKE_YOUNG_PAIR(BgL_arg1084z00_1308, BgL_resz00_1299);
						}
						{
							obj_t BgL_resz00_1754;
							obj_t BgL_lz00_1753;

							BgL_lz00_1753 = BgL_arg1082z00_1306;
							BgL_resz00_1754 = BgL_arg1083z00_1307;
							BgL_resz00_1299 = BgL_resz00_1754;
							BgL_lz00_1298 = BgL_lz00_1753;
							goto BgL_loopz00_1297;
						}
					}
			}
		}

	}



/* &map-2 */
	obj_t BGl_z62mapzd22zb0zz__r4_control_features_6_9z00(obj_t BgL_envz00_1561,
		obj_t BgL_fz00_1562, obj_t BgL_lz00_1563)
	{
		{	/* Ieee/control.scm 97 */
			{	/* Ieee/control.scm 98 */
				obj_t BgL_auxz00_1762;
				obj_t BgL_auxz00_1755;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_lz00_1563))
					{	/* Ieee/control.scm 98 */
						BgL_auxz00_1762 = BgL_lz00_1563;
					}
				else
					{
						obj_t BgL_auxz00_1765;

						BgL_auxz00_1765 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1650z00zz__r4_control_features_6_9z00, BINT(3744L),
							BGl_string1653z00zz__r4_control_features_6_9z00,
							BGl_string1654z00zz__r4_control_features_6_9z00, BgL_lz00_1563);
						FAILURE(BgL_auxz00_1765, BFALSE, BFALSE);
					}
				if (PROCEDUREP(BgL_fz00_1562))
					{	/* Ieee/control.scm 98 */
						BgL_auxz00_1755 = BgL_fz00_1562;
					}
				else
					{
						obj_t BgL_auxz00_1758;

						BgL_auxz00_1758 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1650z00zz__r4_control_features_6_9z00, BINT(3744L),
							BGl_string1653z00zz__r4_control_features_6_9z00,
							BGl_string1652z00zz__r4_control_features_6_9z00, BgL_fz00_1562);
						FAILURE(BgL_auxz00_1758, BFALSE, BFALSE);
					}
				return
					BGl_mapzd22zd2zz__r4_control_features_6_9z00(BgL_auxz00_1755,
					BgL_auxz00_1762);
			}
		}

	}



/* map */
	BGL_EXPORTED_DEF obj_t BGl_mapz00zz__r4_control_features_6_9z00(obj_t
		BgL_fz00_9, obj_t BgL_lz00_10)
	{
		{	/* Ieee/control.scm 107 */
			if (NULLP(BgL_lz00_10))
				{	/* Ieee/control.scm 109 */
					return BNIL;
				}
			else
				{	/* Ieee/control.scm 109 */
					if (NULLP(CDR(((obj_t) BgL_lz00_10))))
						{	/* Ieee/control.scm 112 */
							obj_t BgL_arg1090z00_776;

							BgL_arg1090z00_776 = CAR(((obj_t) BgL_lz00_10));
							return
								BGl_mapzd22zd2zz__r4_control_features_6_9z00(BgL_fz00_9,
								BgL_arg1090z00_776);
						}
					else
						{	/* Ieee/control.scm 111 */
							BGL_TAIL return
								BGl_loopze73ze7zz__r4_control_features_6_9z00(BgL_fz00_9,
								BgL_lz00_10);
						}
				}
		}

	}



/* loop~3 */
	obj_t BGl_loopze73ze7zz__r4_control_features_6_9z00(obj_t BgL_fz00_1641,
		obj_t BgL_lz00_778)
	{
		{	/* Ieee/control.scm 114 */
			if (NULLP(CAR(((obj_t) BgL_lz00_778))))
				{	/* Ieee/control.scm 115 */
					return BNIL;
				}
			else
				{	/* Ieee/control.scm 117 */
					obj_t BgL_arg1097z00_782;
					obj_t BgL_arg1102z00_783;

					BgL_arg1097z00_782 =
						apply(BgL_fz00_1641,
						BGl_mapzd22zd2zz__r4_control_features_6_9z00
						(BGl_carzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_lz00_778));
					BgL_arg1102z00_783 =
						BGl_loopze73ze7zz__r4_control_features_6_9z00(BgL_fz00_1641,
						BGl_mapzd22zd2zz__r4_control_features_6_9z00
						(BGl_cdrzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_lz00_778));
					return MAKE_YOUNG_PAIR(BgL_arg1097z00_782, BgL_arg1102z00_783);
				}
		}

	}



/* &map */
	obj_t BGl_z62mapz62zz__r4_control_features_6_9z00(obj_t BgL_envz00_1564,
		obj_t BgL_fz00_1565, obj_t BgL_lz00_1566)
	{
		{	/* Ieee/control.scm 107 */
			{	/* Ieee/control.scm 109 */
				obj_t BgL_auxz00_1790;

				if (PROCEDUREP(BgL_fz00_1565))
					{	/* Ieee/control.scm 109 */
						BgL_auxz00_1790 = BgL_fz00_1565;
					}
				else
					{
						obj_t BgL_auxz00_1793;

						BgL_auxz00_1793 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1650z00zz__r4_control_features_6_9z00, BINT(4121L),
							BGl_string1655z00zz__r4_control_features_6_9z00,
							BGl_string1652z00zz__r4_control_features_6_9z00, BgL_fz00_1565);
						FAILURE(BgL_auxz00_1793, BFALSE, BFALSE);
					}
				return
					BGl_mapz00zz__r4_control_features_6_9z00(BgL_auxz00_1790,
					BgL_lz00_1566);
			}
		}

	}



/* map! */
	BGL_EXPORTED_DEF obj_t BGl_mapz12z12zz__r4_control_features_6_9z00(obj_t
		BgL_fz00_13, obj_t BgL_lz00_14)
	{
		{	/* Ieee/control.scm 134 */
			if (NULLP(BgL_lz00_14))
				{	/* Ieee/control.scm 136 */
					return BNIL;
				}
			else
				{	/* Ieee/control.scm 136 */
					if (NULLP(CDR(((obj_t) BgL_lz00_14))))
						{	/* Ieee/control.scm 139 */
							obj_t BgL_arg1129z00_799;

							BgL_arg1129z00_799 = CAR(((obj_t) BgL_lz00_14));
							{
								obj_t BgL_lz00_1327;

								BgL_lz00_1327 = BgL_arg1129z00_799;
							BgL_loopz00_1326:
								if (NULLP(BgL_lz00_1327))
									{	/* Ieee/control.scm 125 */
										return BgL_arg1129z00_799;
									}
								else
									{	/* Ieee/control.scm 125 */
										{	/* Ieee/control.scm 128 */
											obj_t BgL_arg1122z00_1329;

											{	/* Ieee/control.scm 128 */
												obj_t BgL_arg1123z00_1330;

												BgL_arg1123z00_1330 = CAR(((obj_t) BgL_lz00_1327));
												BgL_arg1122z00_1329 =
													BGL_PROCEDURE_CALL1(BgL_fz00_13, BgL_arg1123z00_1330);
											}
											{	/* Ieee/control.scm 128 */
												obj_t BgL_tmpz00_1814;

												BgL_tmpz00_1814 = ((obj_t) BgL_lz00_1327);
												SET_CAR(BgL_tmpz00_1814, BgL_arg1122z00_1329);
											}
										}
										{	/* Ieee/control.scm 129 */
											obj_t BgL_arg1125z00_1331;

											BgL_arg1125z00_1331 = CDR(((obj_t) BgL_lz00_1327));
											{
												obj_t BgL_lz00_1819;

												BgL_lz00_1819 = BgL_arg1125z00_1331;
												BgL_lz00_1327 = BgL_lz00_1819;
												goto BgL_loopz00_1326;
											}
										}
									}
							}
						}
					else
						{	/* Ieee/control.scm 141 */
							obj_t BgL_l0z00_800;

							BgL_l0z00_800 = CAR(((obj_t) BgL_lz00_14));
							{
								obj_t BgL_lz00_802;

								BgL_lz00_802 = BgL_lz00_14;
							BgL_zc3z04anonymousza31130ze3z87_803:
								if (NULLP(CAR(((obj_t) BgL_lz00_802))))
									{	/* Ieee/control.scm 143 */
										return BgL_l0z00_800;
									}
								else
									{	/* Ieee/control.scm 143 */
										{	/* Ieee/control.scm 146 */
											obj_t BgL_arg1137z00_806;
											obj_t BgL_arg1138z00_807;

											BgL_arg1137z00_806 = CAR(((obj_t) BgL_lz00_802));
											BgL_arg1138z00_807 =
												apply(BgL_fz00_13,
												BGl_mapzd22zd2zz__r4_control_features_6_9z00
												(BGl_carzd2envzd2zz__r4_pairs_and_lists_6_3z00,
													BgL_lz00_802));
											{	/* Ieee/control.scm 146 */
												obj_t BgL_tmpz00_1831;

												BgL_tmpz00_1831 = ((obj_t) BgL_arg1137z00_806);
												SET_CAR(BgL_tmpz00_1831, BgL_arg1138z00_807);
											}
										}
										{
											obj_t BgL_lz00_1834;

											BgL_lz00_1834 =
												BGl_mapzd22zd2zz__r4_control_features_6_9z00
												(BGl_cdrzd2envzd2zz__r4_pairs_and_lists_6_3z00,
												BgL_lz00_802);
											BgL_lz00_802 = BgL_lz00_1834;
											goto BgL_zc3z04anonymousza31130ze3z87_803;
										}
									}
							}
						}
				}
		}

	}



/* &map! */
	obj_t BGl_z62mapz12z70zz__r4_control_features_6_9z00(obj_t BgL_envz00_1571,
		obj_t BgL_fz00_1572, obj_t BgL_lz00_1573)
	{
		{	/* Ieee/control.scm 134 */
			{	/* Ieee/control.scm 136 */
				obj_t BgL_auxz00_1836;

				if (PROCEDUREP(BgL_fz00_1572))
					{	/* Ieee/control.scm 136 */
						BgL_auxz00_1836 = BgL_fz00_1572;
					}
				else
					{
						obj_t BgL_auxz00_1839;

						BgL_auxz00_1839 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1650z00zz__r4_control_features_6_9z00, BINT(4952L),
							BGl_string1656z00zz__r4_control_features_6_9z00,
							BGl_string1652z00zz__r4_control_features_6_9z00, BgL_fz00_1572);
						FAILURE(BgL_auxz00_1839, BFALSE, BFALSE);
					}
				return
					BGl_mapz12z12zz__r4_control_features_6_9z00(BgL_auxz00_1836,
					BgL_lz00_1573);
			}
		}

	}



/* append-map */
	BGL_EXPORTED_DEF obj_t BGl_appendzd2mapzd2zz__r4_control_features_6_9z00(obj_t
		BgL_fz00_17, obj_t BgL_lz00_18)
	{
		{	/* Ieee/control.scm 161 */
			if (NULLP(BgL_lz00_18))
				{	/* Ieee/control.scm 163 */
					return BNIL;
				}
			else
				{	/* Ieee/control.scm 163 */
					if (NULLP(CDR(((obj_t) BgL_lz00_18))))
						{	/* Ieee/control.scm 166 */
							obj_t BgL_arg1157z00_824;

							BgL_arg1157z00_824 = CAR(((obj_t) BgL_lz00_18));
							BGL_TAIL return
								BGl_loopze72ze7zz__r4_control_features_6_9z00(BgL_fz00_17,
								BgL_arg1157z00_824);
						}
					else
						{	/* Ieee/control.scm 165 */
							BGL_TAIL return
								BGl_loopze71ze7zz__r4_control_features_6_9z00(BgL_fz00_17,
								BgL_lz00_18);
						}
				}
		}

	}



/* loop~1 */
	obj_t BGl_loopze71ze7zz__r4_control_features_6_9z00(obj_t BgL_fz00_1639,
		obj_t BgL_lz00_826)
	{
		{	/* Ieee/control.scm 168 */
			if (NULLP(CAR(((obj_t) BgL_lz00_826))))
				{	/* Ieee/control.scm 169 */
					return BNIL;
				}
			else
				{	/* Ieee/control.scm 171 */
					obj_t BgL_arg1164z00_830;
					obj_t BgL_arg1166z00_831;

					BgL_arg1164z00_830 =
						apply(BgL_fz00_1639,
						BGl_mapzd22zd2zz__r4_control_features_6_9z00
						(BGl_carzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_lz00_826));
					BgL_arg1166z00_831 =
						BGl_loopze71ze7zz__r4_control_features_6_9z00(BgL_fz00_1639,
						BGl_mapzd22zd2zz__r4_control_features_6_9z00
						(BGl_cdrzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_lz00_826));
					return
						BGl_appendzd221011zd2zz__r4_control_features_6_9z00
						(BgL_arg1164z00_830, BgL_arg1166z00_831);
				}
		}

	}



/* loop~2 */
	obj_t BGl_loopze72ze7zz__r4_control_features_6_9z00(obj_t BgL_fz00_1640,
		obj_t BgL_lz00_1351)
	{
		{	/* Ieee/control.scm 153 */
			if (NULLP(BgL_lz00_1351))
				{	/* Ieee/control.scm 154 */
					return BNIL;
				}
			else
				{	/* Ieee/control.scm 156 */
					obj_t BgL_arg1145z00_1353;
					obj_t BgL_arg1148z00_1354;

					{	/* Ieee/control.scm 156 */
						obj_t BgL_arg1149z00_1355;

						BgL_arg1149z00_1355 = CAR(((obj_t) BgL_lz00_1351));
						BgL_arg1145z00_1353 =
							BGL_PROCEDURE_CALL1(BgL_fz00_1640, BgL_arg1149z00_1355);
					}
					{	/* Ieee/control.scm 156 */
						obj_t BgL_arg1152z00_1356;

						BgL_arg1152z00_1356 = CDR(((obj_t) BgL_lz00_1351));
						BgL_arg1148z00_1354 =
							BGl_loopze72ze7zz__r4_control_features_6_9z00(BgL_fz00_1640,
							BgL_arg1152z00_1356);
					}
					return
						BGl_appendzd221011zd2zz__r4_control_features_6_9z00
						(BgL_arg1145z00_1353, BgL_arg1148z00_1354);
				}
		}

	}



/* &append-map */
	obj_t BGl_z62appendzd2mapzb0zz__r4_control_features_6_9z00(obj_t
		BgL_envz00_1574, obj_t BgL_fz00_1575, obj_t BgL_lz00_1576)
	{
		{	/* Ieee/control.scm 161 */
			{	/* Ieee/control.scm 163 */
				obj_t BgL_auxz00_1876;

				if (PROCEDUREP(BgL_fz00_1575))
					{	/* Ieee/control.scm 163 */
						BgL_auxz00_1876 = BgL_fz00_1575;
					}
				else
					{
						obj_t BgL_auxz00_1879;

						BgL_auxz00_1879 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1650z00zz__r4_control_features_6_9z00, BINT(5814L),
							BGl_string1657z00zz__r4_control_features_6_9z00,
							BGl_string1652z00zz__r4_control_features_6_9z00, BgL_fz00_1575);
						FAILURE(BgL_auxz00_1879, BFALSE, BFALSE);
					}
				return
					BGl_appendzd2mapzd2zz__r4_control_features_6_9z00(BgL_auxz00_1876,
					BgL_lz00_1576);
			}
		}

	}



/* append-map2! */
	obj_t BGl_appendzd2map2z12zc0zz__r4_control_features_6_9z00(obj_t BgL_fz00_19,
		obj_t BgL_lz00_20)
	{
		{	/* Ieee/control.scm 176 */
			if (NULLP(BgL_lz00_20))
				{	/* Ieee/control.scm 177 */
					return BNIL;
				}
			else
				{	/* Ieee/control.scm 179 */
					obj_t BgL_resultz00_837;

					{	/* Ieee/control.scm 179 */
						obj_t BgL_list1188z00_848;

						BgL_list1188z00_848 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
						BgL_resultz00_837 = BgL_list1188z00_848;
					}
					{	/* Ieee/control.scm 179 */
						obj_t BgL_lpairz00_838;

						BgL_lpairz00_838 = BgL_resultz00_837;
						{	/* Ieee/control.scm 180 */

							{
								obj_t BgL_l1028z00_840;

								BgL_l1028z00_840 = BgL_lz00_20;
							BgL_zc3z04anonymousza31184ze3z87_841:
								if (PAIRP(BgL_l1028z00_840))
									{	/* Ieee/control.scm 181 */
										{	/* Ieee/control.scm 182 */
											obj_t BgL_xz00_843;

											BgL_xz00_843 = CAR(BgL_l1028z00_840);
											{	/* Ieee/control.scm 182 */
												obj_t BgL_result2z00_844;

												BgL_result2z00_844 =
													BGL_PROCEDURE_CALL1(BgL_fz00_19, BgL_xz00_843);
												if (PAIRP(BgL_result2z00_844))
													{	/* Ieee/control.scm 183 */
														SET_CDR(BgL_lpairz00_838, BgL_result2z00_844);
														BgL_lpairz00_838 =
															BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00
															(BgL_result2z00_844);
													}
												else
													{	/* Ieee/control.scm 183 */
														BFALSE;
													}
											}
										}
										{
											obj_t BgL_l1028z00_1898;

											BgL_l1028z00_1898 = CDR(BgL_l1028z00_840);
											BgL_l1028z00_840 = BgL_l1028z00_1898;
											goto BgL_zc3z04anonymousza31184ze3z87_841;
										}
									}
								else
									{	/* Ieee/control.scm 181 */
										((bool_t) 1);
									}
							}
							return CDR(((obj_t) BgL_resultz00_837));
						}
					}
				}
		}

	}



/* append-map! */
	BGL_EXPORTED_DEF obj_t
		BGl_appendzd2mapz12zc0zz__r4_control_features_6_9z00(obj_t BgL_fz00_21,
		obj_t BgL_lz00_22)
	{
		{	/* Ieee/control.scm 192 */
			if (NULLP(BgL_lz00_22))
				{	/* Ieee/control.scm 194 */
					return BNIL;
				}
			else
				{	/* Ieee/control.scm 194 */
					if (NULLP(CDR(((obj_t) BgL_lz00_22))))
						{	/* Ieee/control.scm 197 */
							obj_t BgL_arg1193z00_852;

							BgL_arg1193z00_852 = CAR(((obj_t) BgL_lz00_22));
							BGL_TAIL return
								BGl_appendzd2map2z12zc0zz__r4_control_features_6_9z00
								(BgL_fz00_21, BgL_arg1193z00_852);
						}
					else
						{	/* Ieee/control.scm 196 */
							if (NULLP(CAR(((obj_t) BgL_lz00_22))))
								{	/* Ieee/control.scm 199 */
									return BNIL;
								}
							else
								{	/* Ieee/control.scm 201 */
									obj_t BgL_resultz00_855;

									{	/* Ieee/control.scm 201 */
										obj_t BgL_list1202z00_866;

										BgL_list1202z00_866 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
										BgL_resultz00_855 = BgL_list1202z00_866;
									}
									{	/* Ieee/control.scm 201 */
										obj_t BgL_lpairz00_1586;

										BgL_lpairz00_1586 = MAKE_CELL(BgL_resultz00_855);
										{	/* Ieee/control.scm 202 */

											{	/* Ieee/control.scm 203 */
												obj_t BgL_runner1201z00_865;

												{	/* Ieee/control.scm 204 */
													obj_t BgL_zc3z04anonymousza31198ze3z87_1577;

													BgL_zc3z04anonymousza31198ze3z87_1577 =
														MAKE_VA_PROCEDURE
														(BGl_z62zc3z04anonymousza31198ze3ze5zz__r4_control_features_6_9z00,
														(int) (-1L), (int) (2L));
													PROCEDURE_SET(BgL_zc3z04anonymousza31198ze3z87_1577,
														(int) (0L), BgL_fz00_21);
													PROCEDURE_SET(BgL_zc3z04anonymousza31198ze3z87_1577,
														(int) (1L), ((obj_t) BgL_lpairz00_1586));
													{	/* Ieee/control.scm 203 */
														obj_t BgL_list1197z00_858;

														BgL_list1197z00_858 =
															MAKE_YOUNG_PAIR(BgL_lz00_22, BNIL);
														BgL_runner1201z00_865 =
															BGl_consza2za2zz__r4_pairs_and_lists_6_3z00
															(BgL_zc3z04anonymousza31198ze3z87_1577,
															BgL_list1197z00_858);
												}}
												{	/* Ieee/control.scm 203 */
													obj_t BgL_aux1200z00_864;

													BgL_aux1200z00_864 = CAR(BgL_runner1201z00_865);
													BgL_runner1201z00_865 = CDR(BgL_runner1201z00_865);
													BGl_forzd2eachzd2zz__r4_control_features_6_9z00
														(BgL_aux1200z00_864, BgL_runner1201z00_865);
											}}
											return CDR(((obj_t) BgL_resultz00_855));
										}
									}
								}
						}
				}
		}

	}



/* &append-map! */
	obj_t BGl_z62appendzd2mapz12za2zz__r4_control_features_6_9z00(obj_t
		BgL_envz00_1578, obj_t BgL_fz00_1579, obj_t BgL_lz00_1580)
	{
		{	/* Ieee/control.scm 192 */
			{	/* Ieee/control.scm 194 */
				obj_t BgL_auxz00_1931;

				if (PROCEDUREP(BgL_fz00_1579))
					{	/* Ieee/control.scm 194 */
						BgL_auxz00_1931 = BgL_fz00_1579;
					}
				else
					{
						obj_t BgL_auxz00_1934;

						BgL_auxz00_1934 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1650z00zz__r4_control_features_6_9z00, BINT(6818L),
							BGl_string1658z00zz__r4_control_features_6_9z00,
							BGl_string1652z00zz__r4_control_features_6_9z00, BgL_fz00_1579);
						FAILURE(BgL_auxz00_1934, BFALSE, BFALSE);
					}
				return
					BGl_appendzd2mapz12zc0zz__r4_control_features_6_9z00(BgL_auxz00_1931,
					BgL_lz00_1580);
			}
		}

	}



/* &<@anonymous:1198> */
	obj_t BGl_z62zc3z04anonymousza31198ze3ze5zz__r4_control_features_6_9z00(obj_t
		BgL_envz00_1581, obj_t BgL_xsz00_1584)
	{
		{	/* Ieee/control.scm 203 */
			{	/* Ieee/control.scm 204 */
				obj_t BgL_fz00_1582;
				obj_t BgL_lpairz00_1583;

				BgL_fz00_1582 = ((obj_t) PROCEDURE_REF(BgL_envz00_1581, (int) (0L)));
				BgL_lpairz00_1583 = PROCEDURE_REF(BgL_envz00_1581, (int) (1L));
				{	/* Ieee/control.scm 204 */
					obj_t BgL_result2z00_1684;

					BgL_result2z00_1684 = apply(BgL_fz00_1582, BgL_xsz00_1584);
					if (PAIRP(BgL_result2z00_1684))
						{	/* Ieee/control.scm 205 */
							{	/* Ieee/control.scm 206 */
								obj_t BgL_pairz00_1685;

								BgL_pairz00_1685 = CELL_REF(BgL_lpairz00_1583);
								SET_CDR(BgL_pairz00_1685, BgL_result2z00_1684);
							}
							{	/* Ieee/control.scm 207 */
								obj_t BgL_auxz00_1686;

								BgL_auxz00_1686 =
									BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00
									(BgL_result2z00_1684);
								return CELL_SET(BgL_lpairz00_1583, BgL_auxz00_1686);
							}
						}
					else
						{	/* Ieee/control.scm 205 */
							return BFALSE;
						}
				}
			}
		}

	}



/* filter-map-2 */
	obj_t BGl_filterzd2mapzd22z00zz__r4_control_features_6_9z00(obj_t BgL_fz00_23,
		obj_t BgL_lz00_24)
	{
		{	/* Ieee/control.scm 214 */
			{
				obj_t BgL_lz00_871;
				obj_t BgL_resz00_872;

				BgL_lz00_871 = BgL_lz00_24;
				BgL_resz00_872 = BNIL;
			BgL_zc3z04anonymousza31207ze3z87_873:
				if (NULLP(BgL_lz00_871))
					{	/* Ieee/control.scm 217 */
						return bgl_reverse_bang(BgL_resz00_872);
					}
				else
					{	/* Ieee/control.scm 219 */
						obj_t BgL_hdz00_875;

						{	/* Ieee/control.scm 219 */
							obj_t BgL_arg1215z00_879;

							BgL_arg1215z00_879 = CAR(((obj_t) BgL_lz00_871));
							BgL_hdz00_875 =
								BGL_PROCEDURE_CALL1(BgL_fz00_23, BgL_arg1215z00_879);
						}
						if (CBOOL(BgL_hdz00_875))
							{	/* Ieee/control.scm 221 */
								obj_t BgL_arg1209z00_876;
								obj_t BgL_arg1210z00_877;

								BgL_arg1209z00_876 = CDR(((obj_t) BgL_lz00_871));
								BgL_arg1210z00_877 =
									MAKE_YOUNG_PAIR(BgL_hdz00_875, BgL_resz00_872);
								{
									obj_t BgL_resz00_1965;
									obj_t BgL_lz00_1964;

									BgL_lz00_1964 = BgL_arg1209z00_876;
									BgL_resz00_1965 = BgL_arg1210z00_877;
									BgL_resz00_872 = BgL_resz00_1965;
									BgL_lz00_871 = BgL_lz00_1964;
									goto BgL_zc3z04anonymousza31207ze3z87_873;
								}
							}
						else
							{	/* Ieee/control.scm 222 */
								obj_t BgL_arg1212z00_878;

								BgL_arg1212z00_878 = CDR(((obj_t) BgL_lz00_871));
								{
									obj_t BgL_lz00_1968;

									BgL_lz00_1968 = BgL_arg1212z00_878;
									BgL_lz00_871 = BgL_lz00_1968;
									goto BgL_zc3z04anonymousza31207ze3z87_873;
								}
							}
					}
			}
		}

	}



/* filter-map */
	BGL_EXPORTED_DEF obj_t BGl_filterzd2mapzd2zz__r4_control_features_6_9z00(obj_t
		BgL_fz00_25, obj_t BgL_lz00_26)
	{
		{	/* Ieee/control.scm 227 */
			if (NULLP(BgL_lz00_26))
				{	/* Ieee/control.scm 229 */
					return BNIL;
				}
			else
				{	/* Ieee/control.scm 229 */
					if (NULLP(CDR(((obj_t) BgL_lz00_26))))
						{	/* Ieee/control.scm 232 */
							obj_t BgL_arg1220z00_884;

							BgL_arg1220z00_884 = CAR(((obj_t) BgL_lz00_26));
							BGL_TAIL return
								BGl_filterzd2mapzd22z00zz__r4_control_features_6_9z00
								(BgL_fz00_25, BgL_arg1220z00_884);
						}
					else
						{	/* Ieee/control.scm 231 */
							BGL_TAIL return
								BGl_loopze70ze7zz__r4_control_features_6_9z00(BgL_fz00_25,
								BgL_lz00_26);
						}
				}
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zz__r4_control_features_6_9z00(obj_t BgL_fz00_1638,
		obj_t BgL_lz00_886)
	{
		{	/* Ieee/control.scm 234 */
		BGl_loopze70ze7zz__r4_control_features_6_9z00:
			if (NULLP(CAR(((obj_t) BgL_lz00_886))))
				{	/* Ieee/control.scm 235 */
					return BNIL;
				}
			else
				{	/* Ieee/control.scm 237 */
					obj_t BgL_hdz00_890;

					BgL_hdz00_890 =
						apply(BgL_fz00_1638,
						BGl_mapzd22zd2zz__r4_control_features_6_9z00
						(BGl_carzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_lz00_886));
					if (CBOOL(BgL_hdz00_890))
						{	/* Ieee/control.scm 238 */
							return
								MAKE_YOUNG_PAIR(BgL_hdz00_890,
								BGl_loopze70ze7zz__r4_control_features_6_9z00(BgL_fz00_1638,
									BGl_mapzd22zd2zz__r4_control_features_6_9z00
									(BGl_cdrzd2envzd2zz__r4_pairs_and_lists_6_3z00,
										BgL_lz00_886)));
						}
					else
						{
							obj_t BgL_lz00_1991;

							BgL_lz00_1991 =
								BGl_mapzd22zd2zz__r4_control_features_6_9z00
								(BGl_cdrzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_lz00_886);
							BgL_lz00_886 = BgL_lz00_1991;
							goto BGl_loopze70ze7zz__r4_control_features_6_9z00;
						}
				}
		}

	}



/* &filter-map */
	obj_t BGl_z62filterzd2mapzb0zz__r4_control_features_6_9z00(obj_t
		BgL_envz00_1588, obj_t BgL_fz00_1589, obj_t BgL_lz00_1590)
	{
		{	/* Ieee/control.scm 227 */
			{	/* Ieee/control.scm 229 */
				obj_t BgL_auxz00_1993;

				if (PROCEDUREP(BgL_fz00_1589))
					{	/* Ieee/control.scm 229 */
						BgL_auxz00_1993 = BgL_fz00_1589;
					}
				else
					{
						obj_t BgL_auxz00_1996;

						BgL_auxz00_1996 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1650z00zz__r4_control_features_6_9z00, BINT(7913L),
							BGl_string1659z00zz__r4_control_features_6_9z00,
							BGl_string1652z00zz__r4_control_features_6_9z00, BgL_fz00_1589);
						FAILURE(BgL_auxz00_1996, BFALSE, BFALSE);
					}
				return
					BGl_filterzd2mapzd2zz__r4_control_features_6_9z00(BgL_auxz00_1993,
					BgL_lz00_1590);
			}
		}

	}



/* for-each-2 */
	BGL_EXPORTED_DEF obj_t
		BGl_forzd2eachzd22z00zz__r4_control_features_6_9z00(obj_t BgL_fz00_27,
		obj_t BgL_lz00_28)
	{
		{	/* Ieee/control.scm 245 */
			{
				obj_t BgL_lz00_1397;

				BgL_lz00_1397 = BgL_lz00_28;
			BgL_loopz00_1396:
				if (NULLP(BgL_lz00_1397))
					{	/* Ieee/control.scm 247 */
						return BUNSPEC;
					}
				else
					{	/* Ieee/control.scm 247 */
						{	/* Ieee/control.scm 250 */
							obj_t BgL_arg1232z00_1402;

							BgL_arg1232z00_1402 = CAR(((obj_t) BgL_lz00_1397));
							BGL_PROCEDURE_CALL1(BgL_fz00_27, BgL_arg1232z00_1402);
						}
						{	/* Ieee/control.scm 251 */
							obj_t BgL_arg1233z00_1403;

							BgL_arg1233z00_1403 = CDR(((obj_t) BgL_lz00_1397));
							{
								obj_t BgL_lz00_2011;

								BgL_lz00_2011 = BgL_arg1233z00_1403;
								BgL_lz00_1397 = BgL_lz00_2011;
								goto BgL_loopz00_1396;
							}
						}
					}
			}
		}

	}



/* &for-each-2 */
	obj_t BGl_z62forzd2eachzd22z62zz__r4_control_features_6_9z00(obj_t
		BgL_envz00_1591, obj_t BgL_fz00_1592, obj_t BgL_lz00_1593)
	{
		{	/* Ieee/control.scm 245 */
			{	/* Ieee/control.scm 246 */
				obj_t BgL_auxz00_2019;
				obj_t BgL_auxz00_2012;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_lz00_1593))
					{	/* Ieee/control.scm 246 */
						BgL_auxz00_2019 = BgL_lz00_1593;
					}
				else
					{
						obj_t BgL_auxz00_2022;

						BgL_auxz00_2022 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1650z00zz__r4_control_features_6_9z00, BINT(8440L),
							BGl_string1660z00zz__r4_control_features_6_9z00,
							BGl_string1654z00zz__r4_control_features_6_9z00, BgL_lz00_1593);
						FAILURE(BgL_auxz00_2022, BFALSE, BFALSE);
					}
				if (PROCEDUREP(BgL_fz00_1592))
					{	/* Ieee/control.scm 246 */
						BgL_auxz00_2012 = BgL_fz00_1592;
					}
				else
					{
						obj_t BgL_auxz00_2015;

						BgL_auxz00_2015 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1650z00zz__r4_control_features_6_9z00, BINT(8440L),
							BGl_string1660z00zz__r4_control_features_6_9z00,
							BGl_string1652z00zz__r4_control_features_6_9z00, BgL_fz00_1592);
						FAILURE(BgL_auxz00_2015, BFALSE, BFALSE);
					}
				return
					BGl_forzd2eachzd22z00zz__r4_control_features_6_9z00(BgL_auxz00_2012,
					BgL_auxz00_2019);
			}
		}

	}



/* for-each */
	BGL_EXPORTED_DEF obj_t BGl_forzd2eachzd2zz__r4_control_features_6_9z00(obj_t
		BgL_fz00_29, obj_t BgL_lz00_30)
	{
		{	/* Ieee/control.scm 256 */
			if (NULLP(BgL_lz00_30))
				{	/* Ieee/control.scm 258 */
					return BUNSPEC;
				}
			else
				{	/* Ieee/control.scm 258 */
					if (NULLP(CDR(((obj_t) BgL_lz00_30))))
						{	/* Ieee/control.scm 261 */
							obj_t BgL_arg1238z00_907;

							BgL_arg1238z00_907 = CAR(((obj_t) BgL_lz00_30));
							{
								obj_t BgL_lz00_1411;

								BgL_lz00_1411 = ((obj_t) BgL_arg1238z00_907);
							BgL_loopz00_1410:
								if (NULLP(BgL_lz00_1411))
									{	/* Ieee/control.scm 247 */
										return BUNSPEC;
									}
								else
									{	/* Ieee/control.scm 247 */
										{	/* Ieee/control.scm 250 */
											obj_t BgL_arg1232z00_1416;

											BgL_arg1232z00_1416 = CAR(((obj_t) BgL_lz00_1411));
											BGL_PROCEDURE_CALL1(BgL_fz00_29, BgL_arg1232z00_1416);
										}
										{	/* Ieee/control.scm 251 */
											obj_t BgL_arg1233z00_1417;

											BgL_arg1233z00_1417 = CDR(((obj_t) BgL_lz00_1411));
											{
												obj_t BgL_lz00_2045;

												BgL_lz00_2045 = BgL_arg1233z00_1417;
												BgL_lz00_1411 = BgL_lz00_2045;
												goto BgL_loopz00_1410;
											}
										}
									}
							}
						}
					else
						{
							obj_t BgL_lz00_909;

							BgL_lz00_909 = BgL_lz00_30;
						BgL_zc3z04anonymousza31239ze3z87_910:
							if (NULLP(CAR(((obj_t) BgL_lz00_909))))
								{	/* Ieee/control.scm 264 */
									return BUNSPEC;
								}
							else
								{	/* Ieee/control.scm 264 */
									apply(BgL_fz00_29,
										BGl_mapzd22zd2zz__r4_control_features_6_9z00
										(BGl_carzd2envzd2zz__r4_pairs_and_lists_6_3z00,
											BgL_lz00_909));
									{
										obj_t BgL_lz00_2054;

										BgL_lz00_2054 =
											BGl_mapzd22zd2zz__r4_control_features_6_9z00
											(BGl_cdrzd2envzd2zz__r4_pairs_and_lists_6_3z00,
											BgL_lz00_909);
										BgL_lz00_909 = BgL_lz00_2054;
										goto BgL_zc3z04anonymousza31239ze3z87_910;
									}
								}
						}
				}
		}

	}



/* &for-each */
	obj_t BGl_z62forzd2eachzb0zz__r4_control_features_6_9z00(obj_t
		BgL_envz00_1594, obj_t BgL_fz00_1595, obj_t BgL_lz00_1596)
	{
		{	/* Ieee/control.scm 256 */
			{	/* Ieee/control.scm 258 */
				obj_t BgL_auxz00_2056;

				if (PROCEDUREP(BgL_fz00_1595))
					{	/* Ieee/control.scm 258 */
						BgL_auxz00_2056 = BgL_fz00_1595;
					}
				else
					{
						obj_t BgL_auxz00_2059;

						BgL_auxz00_2059 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1650z00zz__r4_control_features_6_9z00, BINT(8810L),
							BGl_string1661z00zz__r4_control_features_6_9z00,
							BGl_string1652z00zz__r4_control_features_6_9z00, BgL_fz00_1595);
						FAILURE(BgL_auxz00_2059, BFALSE, BFALSE);
					}
				return
					BGl_forzd2eachzd2zz__r4_control_features_6_9z00(BgL_auxz00_2056,
					BgL_lz00_1596);
			}
		}

	}



/* filter */
	BGL_EXPORTED_DEF obj_t BGl_filterz00zz__r4_control_features_6_9z00(obj_t
		BgL_predz00_31, obj_t BgL_lz00_32)
	{
		{	/* Ieee/control.scm 273 */
			{	/* Ieee/control.scm 274 */
				obj_t BgL_hookz00_917;

				BgL_hookz00_917 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
				{
					obj_t BgL_lz00_919;
					obj_t BgL_hz00_920;

					BgL_lz00_919 = BgL_lz00_32;
					BgL_hz00_920 = BgL_hookz00_917;
				BgL_zc3z04anonymousza31250ze3z87_921:
					if (NULLP(BgL_lz00_919))
						{	/* Ieee/control.scm 278 */
							return CDR(BgL_hookz00_917);
						}
					else
						{	/* Ieee/control.scm 280 */
							bool_t BgL_test1751z00_2068;

							{	/* Ieee/control.scm 280 */
								obj_t BgL_arg1304z00_929;

								BgL_arg1304z00_929 = CAR(((obj_t) BgL_lz00_919));
								BgL_test1751z00_2068 =
									CBOOL(BGL_PROCEDURE_CALL1(BgL_predz00_31,
										BgL_arg1304z00_929));
							}
							if (BgL_test1751z00_2068)
								{	/* Ieee/control.scm 281 */
									obj_t BgL_nhz00_925;

									{	/* Ieee/control.scm 281 */
										obj_t BgL_arg1272z00_927;

										BgL_arg1272z00_927 = CAR(((obj_t) BgL_lz00_919));
										BgL_nhz00_925 = MAKE_YOUNG_PAIR(BgL_arg1272z00_927, BNIL);
									}
									SET_CDR(BgL_hz00_920, BgL_nhz00_925);
									{	/* Ieee/control.scm 283 */
										obj_t BgL_arg1268z00_926;

										BgL_arg1268z00_926 = CDR(((obj_t) BgL_lz00_919));
										{
											obj_t BgL_hz00_2083;
											obj_t BgL_lz00_2082;

											BgL_lz00_2082 = BgL_arg1268z00_926;
											BgL_hz00_2083 = BgL_nhz00_925;
											BgL_hz00_920 = BgL_hz00_2083;
											BgL_lz00_919 = BgL_lz00_2082;
											goto BgL_zc3z04anonymousza31250ze3z87_921;
										}
									}
								}
							else
								{	/* Ieee/control.scm 285 */
									obj_t BgL_arg1284z00_928;

									BgL_arg1284z00_928 = CDR(((obj_t) BgL_lz00_919));
									{
										obj_t BgL_lz00_2086;

										BgL_lz00_2086 = BgL_arg1284z00_928;
										BgL_lz00_919 = BgL_lz00_2086;
										goto BgL_zc3z04anonymousza31250ze3z87_921;
									}
								}
						}
				}
			}
		}

	}



/* &filter */
	obj_t BGl_z62filterz62zz__r4_control_features_6_9z00(obj_t BgL_envz00_1597,
		obj_t BgL_predz00_1598, obj_t BgL_lz00_1599)
	{
		{	/* Ieee/control.scm 273 */
			{	/* Ieee/control.scm 274 */
				obj_t BgL_auxz00_2094;
				obj_t BgL_auxz00_2087;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_lz00_1599))
					{	/* Ieee/control.scm 274 */
						BgL_auxz00_2094 = BgL_lz00_1599;
					}
				else
					{
						obj_t BgL_auxz00_2097;

						BgL_auxz00_2097 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1650z00zz__r4_control_features_6_9z00, BINT(9297L),
							BGl_string1662z00zz__r4_control_features_6_9z00,
							BGl_string1654z00zz__r4_control_features_6_9z00, BgL_lz00_1599);
						FAILURE(BgL_auxz00_2097, BFALSE, BFALSE);
					}
				if (PROCEDUREP(BgL_predz00_1598))
					{	/* Ieee/control.scm 274 */
						BgL_auxz00_2087 = BgL_predz00_1598;
					}
				else
					{
						obj_t BgL_auxz00_2090;

						BgL_auxz00_2090 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1650z00zz__r4_control_features_6_9z00, BINT(9297L),
							BGl_string1662z00zz__r4_control_features_6_9z00,
							BGl_string1652z00zz__r4_control_features_6_9z00,
							BgL_predz00_1598);
						FAILURE(BgL_auxz00_2090, BFALSE, BFALSE);
					}
				return
					BGl_filterz00zz__r4_control_features_6_9z00(BgL_auxz00_2087,
					BgL_auxz00_2094);
			}
		}

	}



/* filter! */
	BGL_EXPORTED_DEF obj_t BGl_filterz12z12zz__r4_control_features_6_9z00(obj_t
		BgL_predz00_33, obj_t BgL_lisz00_34)
	{
		{	/* Ieee/control.scm 290 */
			{
				obj_t BgL_ansz00_932;

				BgL_ansz00_932 = BgL_lisz00_34;
			BgL_zc3z04anonymousza31305ze3z87_933:
				if (NULLP(BgL_ansz00_932))
					{	/* Ieee/control.scm 293 */
						return BgL_ansz00_932;
					}
				else
					{	/* Ieee/control.scm 295 */
						bool_t BgL_test1755z00_2104;

						{	/* Ieee/control.scm 295 */
							obj_t BgL_arg1327z00_965;

							BgL_arg1327z00_965 = CAR(((obj_t) BgL_ansz00_932));
							BgL_test1755z00_2104 =
								CBOOL(BGL_PROCEDURE_CALL1(BgL_predz00_33, BgL_arg1327z00_965));
						}
						if (BgL_test1755z00_2104)
							{
								obj_t BgL_prevz00_949;
								obj_t BgL_lisz00_950;

								{	/* Ieee/control.scm 312 */
									obj_t BgL_arg1309z00_939;

									BgL_arg1309z00_939 = CDR(((obj_t) BgL_ansz00_932));
									{
										obj_t BgL_prevz00_1467;
										obj_t BgL_lisz00_1468;

										BgL_prevz00_1467 = BgL_ansz00_932;
										BgL_lisz00_1468 = BgL_arg1309z00_939;
									BgL_scanzd2inzd2_1466:
										if (PAIRP(BgL_lisz00_1468))
											{	/* Ieee/control.scm 300 */
												bool_t BgL_test1757z00_2116;

												{	/* Ieee/control.scm 300 */
													obj_t BgL_arg1316z00_1476;

													BgL_arg1316z00_1476 = CAR(BgL_lisz00_1468);
													BgL_test1757z00_2116 =
														CBOOL(BGL_PROCEDURE_CALL1(BgL_predz00_33,
															BgL_arg1316z00_1476));
												}
												if (BgL_test1757z00_2116)
													{
														obj_t BgL_lisz00_2124;
														obj_t BgL_prevz00_2123;

														BgL_prevz00_2123 = BgL_lisz00_1468;
														BgL_lisz00_2124 = CDR(BgL_lisz00_1468);
														BgL_lisz00_1468 = BgL_lisz00_2124;
														BgL_prevz00_1467 = BgL_prevz00_2123;
														goto BgL_scanzd2inzd2_1466;
													}
												else
													{	/* Ieee/control.scm 300 */
														BgL_prevz00_949 = BgL_prevz00_1467;
														BgL_lisz00_950 = CDR(BgL_lisz00_1468);
													BgL_zc3z04anonymousza31317ze3z87_951:
														{
															obj_t BgL_lisz00_953;

															BgL_lisz00_953 = BgL_lisz00_950;
														BgL_zc3z04anonymousza31318ze3z87_954:
															if (PAIRP(BgL_lisz00_953))
																{	/* Ieee/control.scm 306 */
																	bool_t BgL_test1759z00_2128;

																	{	/* Ieee/control.scm 306 */
																		obj_t BgL_arg1325z00_960;

																		BgL_arg1325z00_960 = CAR(BgL_lisz00_953);
																		BgL_test1759z00_2128 =
																			CBOOL(BGL_PROCEDURE_CALL1(BgL_predz00_33,
																				BgL_arg1325z00_960));
																	}
																	if (BgL_test1759z00_2128)
																		{	/* Ieee/control.scm 306 */
																			{	/* Ieee/control.scm 308 */
																				obj_t BgL_tmpz00_2135;

																				BgL_tmpz00_2135 =
																					((obj_t) BgL_prevz00_949);
																				SET_CDR(BgL_tmpz00_2135,
																					BgL_lisz00_953);
																			}
																			{	/* Ieee/control.scm 309 */
																				obj_t BgL_arg1322z00_958;

																				BgL_arg1322z00_958 =
																					CDR(BgL_lisz00_953);
																				{
																					obj_t BgL_prevz00_1448;
																					obj_t BgL_lisz00_1449;

																					BgL_prevz00_1448 = BgL_lisz00_953;
																					BgL_lisz00_1449 = BgL_arg1322z00_958;
																				BgL_scanzd2inzd2_1447:
																					if (PAIRP(BgL_lisz00_1449))
																						{	/* Ieee/control.scm 300 */
																							bool_t BgL_test1761z00_2141;

																							{	/* Ieee/control.scm 300 */
																								obj_t BgL_arg1316z00_1457;

																								BgL_arg1316z00_1457 =
																									CAR(BgL_lisz00_1449);
																								BgL_test1761z00_2141 =
																									CBOOL(BGL_PROCEDURE_CALL1
																									(BgL_predz00_33,
																										BgL_arg1316z00_1457));
																							}
																							if (BgL_test1761z00_2141)
																								{
																									obj_t BgL_lisz00_2149;
																									obj_t BgL_prevz00_2148;

																									BgL_prevz00_2148 =
																										BgL_lisz00_1449;
																									BgL_lisz00_2149 =
																										CDR(BgL_lisz00_1449);
																									BgL_lisz00_1449 =
																										BgL_lisz00_2149;
																									BgL_prevz00_1448 =
																										BgL_prevz00_2148;
																									goto BgL_scanzd2inzd2_1447;
																								}
																							else
																								{
																									obj_t BgL_lisz00_2152;
																									obj_t BgL_prevz00_2151;

																									BgL_prevz00_2151 =
																										BgL_prevz00_1448;
																									BgL_lisz00_2152 =
																										CDR(BgL_lisz00_1449);
																									BgL_lisz00_950 =
																										BgL_lisz00_2152;
																									BgL_prevz00_949 =
																										BgL_prevz00_2151;
																									goto
																										BgL_zc3z04anonymousza31317ze3z87_951;
																								}
																						}
																					else
																						{	/* Ieee/control.scm 299 */
																							BFALSE;
																						}
																				}
																			}
																		}
																	else
																		{
																			obj_t BgL_lisz00_2154;

																			BgL_lisz00_2154 = CDR(BgL_lisz00_953);
																			BgL_lisz00_953 = BgL_lisz00_2154;
																			goto BgL_zc3z04anonymousza31318ze3z87_954;
																		}
																}
															else
																{	/* Ieee/control.scm 311 */
																	obj_t BgL_tmpz00_2156;

																	BgL_tmpz00_2156 = ((obj_t) BgL_prevz00_949);
																	SET_CDR(BgL_tmpz00_2156, BgL_lisz00_953);
																}
														}
													}
											}
										else
											{	/* Ieee/control.scm 299 */
												BFALSE;
											}
									}
								}
								return BgL_ansz00_932;
							}
						else
							{	/* Ieee/control.scm 296 */
								obj_t BgL_arg1326z00_964;

								BgL_arg1326z00_964 = CDR(((obj_t) BgL_ansz00_932));
								{
									obj_t BgL_ansz00_2162;

									BgL_ansz00_2162 = BgL_arg1326z00_964;
									BgL_ansz00_932 = BgL_ansz00_2162;
									goto BgL_zc3z04anonymousza31305ze3z87_933;
								}
							}
					}
			}
		}

	}



/* &filter! */
	obj_t BGl_z62filterz12z70zz__r4_control_features_6_9z00(obj_t BgL_envz00_1600,
		obj_t BgL_predz00_1601, obj_t BgL_lisz00_1602)
	{
		{	/* Ieee/control.scm 290 */
			{	/* Ieee/control.scm 293 */
				obj_t BgL_auxz00_2170;
				obj_t BgL_auxz00_2163;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_lisz00_1602))
					{	/* Ieee/control.scm 293 */
						BgL_auxz00_2170 = BgL_lisz00_1602;
					}
				else
					{
						obj_t BgL_auxz00_2173;

						BgL_auxz00_2173 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1650z00zz__r4_control_features_6_9z00, BINT(9827L),
							BGl_string1663z00zz__r4_control_features_6_9z00,
							BGl_string1654z00zz__r4_control_features_6_9z00, BgL_lisz00_1602);
						FAILURE(BgL_auxz00_2173, BFALSE, BFALSE);
					}
				if (PROCEDUREP(BgL_predz00_1601))
					{	/* Ieee/control.scm 293 */
						BgL_auxz00_2163 = BgL_predz00_1601;
					}
				else
					{
						obj_t BgL_auxz00_2166;

						BgL_auxz00_2166 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1650z00zz__r4_control_features_6_9z00, BINT(9827L),
							BGl_string1663z00zz__r4_control_features_6_9z00,
							BGl_string1652z00zz__r4_control_features_6_9z00,
							BgL_predz00_1601);
						FAILURE(BgL_auxz00_2166, BFALSE, BFALSE);
					}
				return
					BGl_filterz12z12zz__r4_control_features_6_9z00(BgL_auxz00_2163,
					BgL_auxz00_2170);
			}
		}

	}



/* force */
	BGL_EXPORTED_DEF obj_t BGl_forcez00zz__r4_control_features_6_9z00(obj_t
		BgL_promisez00_35)
	{
		{	/* Ieee/control.scm 318 */
			return BGL_PROCEDURE_CALL0(BgL_promisez00_35);
		}

	}



/* &force */
	obj_t BGl_z62forcez62zz__r4_control_features_6_9z00(obj_t BgL_envz00_1603,
		obj_t BgL_promisez00_1604)
	{
		{	/* Ieee/control.scm 318 */
			return BGl_forcez00zz__r4_control_features_6_9z00(BgL_promisez00_1604);
		}

	}



/* make-promise */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2promisezd2zz__r4_control_features_6_9z00(obj_t BgL_procz00_36)
	{
		{	/* Ieee/control.scm 324 */
			{	/* Ieee/control.scm 325 */
				obj_t BgL_resultzd2readyzf3z21_1614;
				obj_t BgL_resultz00_1615;

				BgL_resultzd2readyzf3z21_1614 = MAKE_CELL(BFALSE);
				BgL_resultz00_1615 = MAKE_CELL(BFALSE);
				{	/* Ieee/control.scm 327 */
					obj_t BgL_zc3z04anonymousza31328ze3z87_1605;

					BgL_zc3z04anonymousza31328ze3z87_1605 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza31328ze3ze5zz__r4_control_features_6_9z00,
						(int) (0L), (int) (3L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31328ze3z87_1605, (int) (0L),
						BgL_procz00_36);
					PROCEDURE_SET(BgL_zc3z04anonymousza31328ze3z87_1605, (int) (1L),
						((obj_t) BgL_resultzd2readyzf3z21_1614));
					PROCEDURE_SET(BgL_zc3z04anonymousza31328ze3z87_1605, (int) (2L),
						((obj_t) BgL_resultz00_1615));
					return BgL_zc3z04anonymousza31328ze3z87_1605;
				}
			}
		}

	}



/* &make-promise */
	obj_t BGl_z62makezd2promisezb0zz__r4_control_features_6_9z00(obj_t
		BgL_envz00_1606, obj_t BgL_procz00_1607)
	{
		{	/* Ieee/control.scm 324 */
			{	/* Ieee/control.scm 325 */
				obj_t BgL_auxz00_2193;

				if (PROCEDUREP(BgL_procz00_1607))
					{	/* Ieee/control.scm 325 */
						BgL_auxz00_2193 = BgL_procz00_1607;
					}
				else
					{
						obj_t BgL_auxz00_2196;

						BgL_auxz00_2196 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1650z00zz__r4_control_features_6_9z00, BINT(10893L),
							BGl_string1664z00zz__r4_control_features_6_9z00,
							BGl_string1652z00zz__r4_control_features_6_9z00,
							BgL_procz00_1607);
						FAILURE(BgL_auxz00_2196, BFALSE, BFALSE);
					}
				return
					BGl_makezd2promisezd2zz__r4_control_features_6_9z00(BgL_auxz00_2193);
			}
		}

	}



/* &<@anonymous:1328> */
	obj_t BGl_z62zc3z04anonymousza31328ze3ze5zz__r4_control_features_6_9z00(obj_t
		BgL_envz00_1608)
	{
		{	/* Ieee/control.scm 327 */
			{	/* Ieee/control.scm 328 */
				obj_t BgL_procz00_1609;
				obj_t BgL_resultzd2readyzf3z21_1610;
				obj_t BgL_resultz00_1611;

				BgL_procz00_1609 = ((obj_t) PROCEDURE_REF(BgL_envz00_1608, (int) (0L)));
				BgL_resultzd2readyzf3z21_1610 =
					PROCEDURE_REF(BgL_envz00_1608, (int) (1L));
				BgL_resultz00_1611 = PROCEDURE_REF(BgL_envz00_1608, (int) (2L));
				if (CBOOL(CELL_REF(BgL_resultzd2readyzf3z21_1610)))
					{	/* Ieee/control.scm 328 */
						return CELL_REF(BgL_resultz00_1611);
					}
				else
					{	/* Ieee/control.scm 330 */
						obj_t BgL_xz00_1687;

						BgL_xz00_1687 = BGL_PROCEDURE_CALL0(BgL_procz00_1609);
						if (CBOOL(CELL_REF(BgL_resultzd2readyzf3z21_1610)))
							{	/* Ieee/control.scm 331 */
								return CELL_REF(BgL_resultz00_1611);
							}
						else
							{	/* Ieee/control.scm 331 */
								{	/* Ieee/control.scm 334 */
									obj_t BgL_auxz00_1688;

									BgL_auxz00_1688 = BTRUE;
									CELL_SET(BgL_resultzd2readyzf3z21_1610, BgL_auxz00_1688);
								}
								CELL_SET(BgL_resultz00_1611, BgL_xz00_1687);
								return CELL_REF(BgL_resultz00_1611);
							}
					}
			}
		}

	}



/* call/cc */
	BGL_EXPORTED_DEF obj_t BGl_callzf2cczf2zz__r4_control_features_6_9z00(obj_t
		BgL_procz00_37)
	{
		{	/* Ieee/control.scm 341 */
			{	/* Ieee/control.scm 343 */
				obj_t BgL_zc3z04anonymousza31330ze3z87_1619;

				BgL_zc3z04anonymousza31330ze3z87_1619 =
					MAKE_FX_PROCEDURE
					(BGl_z62zc3z04anonymousza31330ze3ze5zz__r4_control_features_6_9z00,
					(int) (1L), (int) (1L));
				PROCEDURE_SET(BgL_zc3z04anonymousza31330ze3z87_1619, (int) (0L),
					BgL_procz00_37);
				BGL_TAIL return call_cc(BgL_zc3z04anonymousza31330ze3z87_1619);
			}
		}

	}



/* &call/cc */
	obj_t BGl_z62callzf2ccz90zz__r4_control_features_6_9z00(obj_t BgL_envz00_1620,
		obj_t BgL_procz00_1621)
	{
		{	/* Ieee/control.scm 341 */
			{	/* Ieee/control.scm 343 */
				obj_t BgL_auxz00_2221;

				if (PROCEDUREP(BgL_procz00_1621))
					{	/* Ieee/control.scm 343 */
						BgL_auxz00_2221 = BgL_procz00_1621;
					}
				else
					{
						obj_t BgL_auxz00_2224;

						BgL_auxz00_2224 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1650z00zz__r4_control_features_6_9z00, BINT(11423L),
							BGl_string1665z00zz__r4_control_features_6_9z00,
							BGl_string1652z00zz__r4_control_features_6_9z00,
							BgL_procz00_1621);
						FAILURE(BgL_auxz00_2224, BFALSE, BFALSE);
					}
				return BGl_callzf2cczf2zz__r4_control_features_6_9z00(BgL_auxz00_2221);
			}
		}

	}



/* &<@anonymous:1330> */
	obj_t BGl_z62zc3z04anonymousza31330ze3ze5zz__r4_control_features_6_9z00(obj_t
		BgL_envz00_1622, obj_t BgL_contz00_1624)
	{
		{	/* Ieee/control.scm 342 */
			{	/* Ieee/control.scm 343 */
				obj_t BgL_procz00_1623;

				BgL_procz00_1623 = ((obj_t) PROCEDURE_REF(BgL_envz00_1622, (int) (0L)));
				{	/* Ieee/control.scm 343 */
					obj_t BgL_evcz00_1689;

					BgL_evcz00_1689 = BGl_getzd2evaluationzd2contextz00zz__evaluatez00();
					{	/* Ieee/control.scm 345 */
						obj_t BgL_zc3z04anonymousza31332ze3z87_1690;

						BgL_zc3z04anonymousza31332ze3z87_1690 =
							MAKE_VA_PROCEDURE
							(BGl_z62zc3z04anonymousza31332ze3ze5zz__r4_control_features_6_9z00,
							(int) (-1L), (int) (2L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31332ze3z87_1690, (int) (0L),
							BgL_evcz00_1689);
						PROCEDURE_SET(BgL_zc3z04anonymousza31332ze3z87_1690, (int) (1L),
							BgL_contz00_1624);
						return BGL_PROCEDURE_CALL1(BgL_procz00_1623,
							BgL_zc3z04anonymousza31332ze3z87_1690);
					}
				}
			}
		}

	}



/* &<@anonymous:1332> */
	obj_t BGl_z62zc3z04anonymousza31332ze3ze5zz__r4_control_features_6_9z00(obj_t
		BgL_envz00_1625, obj_t BgL_valsz00_1628)
	{
		{	/* Ieee/control.scm 344 */
			{	/* Ieee/control.scm 345 */
				obj_t BgL_evcz00_1626;
				obj_t BgL_contz00_1627;

				BgL_evcz00_1626 = PROCEDURE_REF(BgL_envz00_1625, (int) (0L));
				BgL_contz00_1627 = PROCEDURE_REF(BgL_envz00_1625, (int) (1L));
				BGl_setzd2evaluationzd2contextz12z12zz__evaluatez00(BgL_evcz00_1626);
				{	/* Ieee/control.scm 346 */
					bool_t BgL_test1768z00_2249;

					if (PAIRP(BgL_valsz00_1628))
						{	/* Ieee/control.scm 346 */
							BgL_test1768z00_2249 = NULLP(CDR(BgL_valsz00_1628));
						}
					else
						{	/* Ieee/control.scm 346 */
							BgL_test1768z00_2249 = ((bool_t) 0);
						}
					if (BgL_test1768z00_2249)
						{	/* Ieee/control.scm 347 */
							obj_t BgL_arg1336z00_1691;

							BgL_arg1336z00_1691 = CAR(BgL_valsz00_1628);
							return BGL_PROCEDURE_CALL1(BgL_contz00_1627, BgL_arg1336z00_1691);
						}
					else
						{	/* Ieee/control.scm 346 */
							{	/* Ieee/control.scm 349 */
								int BgL_tmpz00_2259;

								BgL_tmpz00_2259 = (int) (-1L);
								BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2259);
							}
							return BGL_PROCEDURE_CALL1(BgL_contz00_1627, BgL_valsz00_1628);
						}
				}
			}
		}

	}



/* call-with-current-continuation */
	BGL_EXPORTED_DEF obj_t
		BGl_callzd2withzd2currentzd2continuationzd2zz__r4_control_features_6_9z00
		(obj_t BgL_procz00_38)
	{
		{	/* Ieee/control.scm 359 */
			BGL_TAIL return
				BGl_callzf2cczf2zz__r4_control_features_6_9z00(BgL_procz00_38);
		}

	}



/* &call-with-current-continuation */
	obj_t
		BGl_z62callzd2withzd2currentzd2continuationzb0zz__r4_control_features_6_9z00
		(obj_t BgL_envz00_1629, obj_t BgL_procz00_1630)
	{
		{	/* Ieee/control.scm 359 */
			{	/* Ieee/control.scm 360 */
				obj_t BgL_auxz00_2267;

				if (PROCEDUREP(BgL_procz00_1630))
					{	/* Ieee/control.scm 360 */
						BgL_auxz00_2267 = BgL_procz00_1630;
					}
				else
					{
						obj_t BgL_auxz00_2270;

						BgL_auxz00_2270 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1650z00zz__r4_control_features_6_9z00, BINT(12040L),
							BGl_string1666z00zz__r4_control_features_6_9z00,
							BGl_string1652z00zz__r4_control_features_6_9z00,
							BgL_procz00_1630);
						FAILURE(BgL_auxz00_2270, BFALSE, BFALSE);
					}
				return
					BGl_callzd2withzd2currentzd2continuationzd2zz__r4_control_features_6_9z00
					(BgL_auxz00_2267);
			}
		}

	}



/* dynamic-wind */
	BGL_EXPORTED_DEF obj_t
		BGl_dynamiczd2windzd2zz__r4_control_features_6_9z00(obj_t BgL_beforez00_39,
		obj_t BgL_thunkz00_40, obj_t BgL_afterz00_41)
	{
		{	/* Ieee/control.scm 365 */
			BGL_PROCEDURE_CALL0(BgL_beforez00_39);
			{	/* Ieee/control.scm 367 */

				PUSH_BEFORE(BgL_beforez00_39);
				{	/* Ieee/control.scm 369 */
					obj_t BgL_exitd1014z00_1485;

					BgL_exitd1014z00_1485 = BGL_EXITD_TOP_AS_OBJ();
					{	/* Ieee/control.scm 372 */
						obj_t BgL_zc3z04anonymousza31338ze3z87_1631;

						BgL_zc3z04anonymousza31338ze3z87_1631 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31338ze3ze5zz__r4_control_features_6_9z00,
							(int) (0L), (int) (1L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31338ze3z87_1631, (int) (0L),
							BgL_afterz00_41);
						{	/* Ieee/control.scm 369 */
							obj_t BgL_arg1603z00_1488;

							{	/* Ieee/control.scm 369 */
								obj_t BgL_arg1605z00_1489;

								BgL_arg1605z00_1489 = BGL_EXITD_PROTECT(BgL_exitd1014z00_1485);
								BgL_arg1603z00_1488 =
									MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31338ze3z87_1631,
									BgL_arg1605z00_1489);
							}
							BGL_EXITD_PROTECT_SET(BgL_exitd1014z00_1485, BgL_arg1603z00_1488);
							BUNSPEC;
						}
						{	/* Ieee/control.scm 370 */
							obj_t BgL_tmp1016z00_1487;

							BgL_tmp1016z00_1487 = BGL_PROCEDURE_CALL0(BgL_thunkz00_40);
							{	/* Ieee/control.scm 369 */
								bool_t BgL_test1771z00_2291;

								{	/* Ieee/control.scm 369 */
									obj_t BgL_arg1602z00_1491;

									BgL_arg1602z00_1491 =
										BGL_EXITD_PROTECT(BgL_exitd1014z00_1485);
									BgL_test1771z00_2291 = PAIRP(BgL_arg1602z00_1491);
								}
								if (BgL_test1771z00_2291)
									{	/* Ieee/control.scm 369 */
										obj_t BgL_arg1598z00_1492;

										{	/* Ieee/control.scm 369 */
											obj_t BgL_arg1601z00_1493;

											BgL_arg1601z00_1493 =
												BGL_EXITD_PROTECT(BgL_exitd1014z00_1485);
											BgL_arg1598z00_1492 = CDR(((obj_t) BgL_arg1601z00_1493));
										}
										BGL_EXITD_PROTECT_SET(BgL_exitd1014z00_1485,
											BgL_arg1598z00_1492);
										BUNSPEC;
									}
								else
									{	/* Ieee/control.scm 369 */
										BFALSE;
									}
							}
							BGL_PROCEDURE_CALL0(BgL_afterz00_41);
							POP_BEFORE();
							return BgL_tmp1016z00_1487;
						}
					}
				}
			}
		}

	}



/* &dynamic-wind */
	obj_t BGl_z62dynamiczd2windzb0zz__r4_control_features_6_9z00(obj_t
		BgL_envz00_1632, obj_t BgL_beforez00_1633, obj_t BgL_thunkz00_1634,
		obj_t BgL_afterz00_1635)
	{
		{	/* Ieee/control.scm 365 */
			{	/* Ieee/control.scm 366 */
				obj_t BgL_auxz00_2316;
				obj_t BgL_auxz00_2309;
				obj_t BgL_auxz00_2302;

				if (PROCEDUREP(BgL_afterz00_1635))
					{	/* Ieee/control.scm 366 */
						BgL_auxz00_2316 = BgL_afterz00_1635;
					}
				else
					{
						obj_t BgL_auxz00_2319;

						BgL_auxz00_2319 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1650z00zz__r4_control_features_6_9z00, BINT(12364L),
							BGl_string1667z00zz__r4_control_features_6_9z00,
							BGl_string1652z00zz__r4_control_features_6_9z00,
							BgL_afterz00_1635);
						FAILURE(BgL_auxz00_2319, BFALSE, BFALSE);
					}
				if (PROCEDUREP(BgL_thunkz00_1634))
					{	/* Ieee/control.scm 366 */
						BgL_auxz00_2309 = BgL_thunkz00_1634;
					}
				else
					{
						obj_t BgL_auxz00_2312;

						BgL_auxz00_2312 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1650z00zz__r4_control_features_6_9z00, BINT(12364L),
							BGl_string1667z00zz__r4_control_features_6_9z00,
							BGl_string1652z00zz__r4_control_features_6_9z00,
							BgL_thunkz00_1634);
						FAILURE(BgL_auxz00_2312, BFALSE, BFALSE);
					}
				if (PROCEDUREP(BgL_beforez00_1633))
					{	/* Ieee/control.scm 366 */
						BgL_auxz00_2302 = BgL_beforez00_1633;
					}
				else
					{
						obj_t BgL_auxz00_2305;

						BgL_auxz00_2305 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1650z00zz__r4_control_features_6_9z00, BINT(12364L),
							BGl_string1667z00zz__r4_control_features_6_9z00,
							BGl_string1652z00zz__r4_control_features_6_9z00,
							BgL_beforez00_1633);
						FAILURE(BgL_auxz00_2305, BFALSE, BFALSE);
					}
				return
					BGl_dynamiczd2windzd2zz__r4_control_features_6_9z00(BgL_auxz00_2302,
					BgL_auxz00_2309, BgL_auxz00_2316);
			}
		}

	}



/* &<@anonymous:1338> */
	obj_t BGl_z62zc3z04anonymousza31338ze3ze5zz__r4_control_features_6_9z00(obj_t
		BgL_envz00_1636)
	{
		{	/* Ieee/control.scm 369 */
			{	/* Ieee/control.scm 372 */
				obj_t BgL_afterz00_1637;

				BgL_afterz00_1637 =
					((obj_t) PROCEDURE_REF(BgL_envz00_1636, (int) (0L)));
				BGL_PROCEDURE_CALL0(BgL_afterz00_1637);
				return POP_BEFORE();
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__r4_control_features_6_9z00(void)
	{
		{	/* Ieee/control.scm 14 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1668z00zz__r4_control_features_6_9z00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1668z00zz__r4_control_features_6_9z00));
			return
				BGl_modulezd2initializa7ationz75zz__evaluatez00(398574045L,
				BSTRING_TO_STRING(BGl_string1668z00zz__r4_control_features_6_9z00));
		}

	}

#ifdef __cplusplus
}
#endif
