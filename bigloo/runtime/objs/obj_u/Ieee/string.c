/*===========================================================================*/
/*   (Ieee/string.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Ieee/string.scm -indent -o objs/obj_u/Ieee/string.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___R4_STRINGS_6_7_TYPE_DEFINITIONS
#define BGL___R4_STRINGS_6_7_TYPE_DEFINITIONS
#endif													// BGL___R4_STRINGS_6_7_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62stringzd2shrinkz12za2zz__r4_strings_6_7z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62stringze3zf3z72zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62substringzd2cizd3zf3z90zz__r4_strings_6_7z00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62escapezd2schemezd2stringz62zz__r4_strings_6_7z00(obj_t,
		obj_t);
	extern bool_t bigloo_strncmp_ci_at(obj_t, obj_t, long, long);
	static obj_t BGl__stringzd2suffixzd2lengthzd2cizd2zz__r4_strings_6_7z00(obj_t,
		obj_t);
	static obj_t BGl__stringzd2naturalzd2compare3z00zz__r4_strings_6_7z00(obj_t,
		obj_t);
	extern bool_t BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_substringz00zz__r4_strings_6_7z00(obj_t, long,
		long);
	BGL_EXPORTED_DECL obj_t
		BGl_stringzd2charzd2indexz00zz__r4_strings_6_7z00(obj_t, unsigned char,
		obj_t, obj_t);
	static obj_t BGl_z62stringzd2cizc3zd3zf3z53zz__r4_strings_6_7z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl__stringzd2suffixzd2cizf3zf3zz__r4_strings_6_7z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_emptyzd2stringzf3z21zz__r4_strings_6_7z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_stringzd2forzd2readz00zz__r4_strings_6_7z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_stringzd2deletezd2zz__r4_strings_6_7z00(obj_t,
		obj_t, int, long);
	extern bool_t bigloo_string_cile(obj_t, obj_t);
	static obj_t BGl_z62stringzd2forzd2readz62zz__r4_strings_6_7z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_stringzd2replacezd2zz__r4_strings_6_7z00(obj_t,
		unsigned char, unsigned char);
	static obj_t BGl__stringzd2containszd2zz__r4_strings_6_7z00(obj_t, obj_t);
	extern bool_t bigloo_string_cilt(obj_t, obj_t);
	static obj_t BGl_symbol2739z00zz__r4_strings_6_7z00 = BUNSPEC;
	extern bool_t bigloo_string_le(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__r4_strings_6_7z00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_escapezd2schemezd2stringz00zz__r4_strings_6_7z00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_stringzd2suffixzf3z21zz__r4_strings_6_7z00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_fixnumzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(long,
		long);
	static obj_t BGl__substringzd2cizd2atzf3zf3zz__r4_strings_6_7z00(obj_t,
		obj_t);
	extern bool_t bigloo_string_lt(obj_t, obj_t);
	static obj_t BGl__stringzd2indexzd2zz__r4_strings_6_7z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_stringzd2cizc3zd3zf3z31zz__r4_strings_6_7z00(obj_t, obj_t);
	extern bool_t bigloo_strcicmp(obj_t, obj_t);
	static obj_t BGl_z62stringzd2upcasezb0zz__r4_strings_6_7z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_stringzd2cizc3zf3ze2zz__r4_strings_6_7z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_stringze3zd3zf3zc3zz__r4_strings_6_7z00(obj_t,
		obj_t);
	extern obj_t string_for_read(obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_z62stringzd2downcasez12za2zz__r4_strings_6_7z00(obj_t,
		obj_t);
	static obj_t BGl_z62stringzd2nullzf3z43zz__r4_strings_6_7z00(obj_t, obj_t);
	static obj_t BGl_symbol2751z00zz__r4_strings_6_7z00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_stringzd2skipzd2rightz00zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL long BGl_stringzd2lengthzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_symbol2754z00zz__r4_strings_6_7z00 = BUNSPEC;
	BGL_EXPORTED_DECL int
		BGl_stringzd2suffixzd2lengthz00zz__r4_strings_6_7z00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static bool_t BGl_delimzf3zf3zz__r4_strings_6_7z00(obj_t, unsigned char);
	static obj_t BGl_symbol2757z00zz__r4_strings_6_7z00 = BUNSPEC;
	static obj_t BGl_z62stringzd2upcasez12za2zz__r4_strings_6_7z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_stringzd2cizd3zf3zf2zz__r4_strings_6_7z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_stringzd2setz12zc0zz__r4_strings_6_7z00(obj_t,
		long, unsigned char);
	static obj_t BGl__stringzd2skipzd2zz__r4_strings_6_7z00(obj_t, obj_t);
	extern bool_t bigloo_strncmp_ci(obj_t, obj_t, long);
	BGL_EXPORTED_DECL obj_t BGl_stringzd2appendzd2zz__r4_strings_6_7z00(obj_t);
	BGL_EXPORTED_DECL int
		BGl_stringzd2prefixzd2lengthz00zz__r4_strings_6_7z00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2760z00zz__r4_strings_6_7z00 = BUNSPEC;
	static obj_t BGl_symbol2763z00zz__r4_strings_6_7z00 = BUNSPEC;
	static obj_t BGl_symbol2766z00zz__r4_strings_6_7z00 = BUNSPEC;
	BGL_EXPORTED_DECL bool_t BGl_stringzd2cize3zf3zc2zz__r4_strings_6_7z00(obj_t,
		obj_t);
	static obj_t BGl_z62stringzd2cize3zd3zf3z73zz__r4_strings_6_7z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL unsigned char
		BGl_stringzd2refzd2urz00zz__r4_strings_6_7z00(obj_t, long);
	static obj_t BGl_z62emptyzd2stringzf3z43zz__r4_strings_6_7z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_stringzd2containszd2ciz00zz__r4_strings_6_7z00(obj_t, obj_t, int);
	BGL_EXPORTED_DECL bool_t BGl_stringzd2nullzf3z21zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_z62substringzd3zf3z42zz__r4_strings_6_7z00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62listzd2ze3stringz53zz__r4_strings_6_7z00(obj_t, obj_t);
	static obj_t BGl_symbol2771z00zz__r4_strings_6_7z00 = BUNSPEC;
	static obj_t BGl_z62stringzd2aszd2readz62zz__r4_strings_6_7z00(obj_t, obj_t);
	static obj_t BGl__stringzd2hexzd2externz00zz__r4_strings_6_7z00(obj_t, obj_t);
	extern bool_t bigloo_strcmp_ci_at(obj_t, obj_t, long);
	extern bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	static obj_t BGl_z62stringzd2capitaliza7ez17zz__r4_strings_6_7z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_stringzd2shrinkz12zc0zz__r4_strings_6_7z00(obj_t,
		long);
	BGL_EXPORTED_DECL bool_t
		BGl_substringzd2cizd3zf3zf2zz__r4_strings_6_7z00(obj_t, obj_t, long);
	static obj_t BGl_cnstzd2initzd2zz__r4_strings_6_7z00(void);
	BGL_EXPORTED_DECL obj_t BGl_stringzd2indexzd2zz__r4_strings_6_7z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	extern long bgl_list_length(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blitzd2stringzd2urz12z12zz__r4_strings_6_7z00(obj_t, long, obj_t, long,
		long);
	BGL_EXPORTED_DECL obj_t
		BGl_stringzd2hexzd2internz12z12zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl__stringzd2prefixzd2cizf3zf3zz__r4_strings_6_7z00(obj_t,
		obj_t);
	static obj_t BGl_z62blitzd2stringzd2urz12z70zz__r4_strings_6_7z00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_stringzd2cize3zd3zf3z11zz__r4_strings_6_7z00(obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__r4_strings_6_7z00(void);
	static obj_t BGl_z62stringzd2hexzd2internz62zz__r4_strings_6_7z00(obj_t,
		obj_t);
	static obj_t BGl_gczd2rootszd2initz00zz__r4_strings_6_7z00(void);
	static obj_t BGl__stringzd2prefixzf3z21zz__r4_strings_6_7z00(obj_t, obj_t);
	static obj_t BGl_z62stringzd2splitzb0zz__r4_strings_6_7z00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl__stringzd2naturalzd2compare3zd2cizd2zz__r4_strings_6_7z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_stringzd2skipzd2zz__r4_strings_6_7z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_listzd2ze3stringz31zz__r4_strings_6_7z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_stringzd2replacez12zc0zz__r4_strings_6_7z00(obj_t,
		unsigned char, unsigned char);
	static obj_t BGl__stringzd2charzd2indexz00zz__r4_strings_6_7z00(obj_t, obj_t);
	static obj_t BGl_z62stringzd2replacez12za2zz__r4_strings_6_7z00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl__stringzd2suffixzd2lengthz00zz__r4_strings_6_7z00(obj_t,
		obj_t);
	static obj_t BGl_z62stringzd2lengthzb0zz__r4_strings_6_7z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_stringzc3zf3z30zz__r4_strings_6_7z00(obj_t,
		obj_t);
	extern bool_t bigloo_strcmp_at(obj_t, obj_t, long);
	extern obj_t BGl_memvz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL long
		BGl_stringzd2compare3zd2ciz00zz__r4_strings_6_7z00(obj_t, obj_t);
	static obj_t BGl_z62substringzd2urzb0zz__r4_strings_6_7z00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t bgl_escape_scheme_string(char *, long, long);
	extern obj_t bgl_reverse_bang(obj_t);
	static obj_t BGl__stringzd2prefixzd2lengthz00zz__r4_strings_6_7z00(obj_t,
		obj_t);
	extern obj_t string_append_3(obj_t, obj_t, obj_t);
	static obj_t BGl__makezd2stringzd2zz__r4_strings_6_7z00(obj_t, obj_t);
	static obj_t BGl_z62stringzd2appendzb0zz__r4_strings_6_7z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_stringzd2containszd2zz__r4_strings_6_7z00(obj_t,
		obj_t, int);
	BGL_EXPORTED_DECL obj_t
		BGl_stringzd2indexzd2rightz00zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62stringzd2setzd2urz12z70zz__r4_strings_6_7z00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62stringzd2hexzd2internz12z70zz__r4_strings_6_7z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_stringzd2splitzd2zz__r4_strings_6_7z00(obj_t,
		obj_t);
	static obj_t BGl_hexzd2stringzd2refz00zz__r4_strings_6_7z00(obj_t, long);
	BGL_EXPORTED_DECL bool_t BGl_stringzd3zf3z20zz__r4_strings_6_7z00(obj_t,
		obj_t);
	static obj_t BGl_z62stringzd2refzd2urz62zz__r4_strings_6_7z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL int
		BGl_stringzd2naturalzd2compare3zd2cizd2zz__r4_strings_6_7z00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62stringzd2ze3listz53zz__r4_strings_6_7z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_escapezd2Czd2stringz00zz__r4_strings_6_7z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_stringzd2upcasez12zc0zz__r4_strings_6_7z00(obj_t);
	extern bool_t bigloo_strncmp_at(obj_t, obj_t, long, long);
	BGL_EXPORTED_DECL obj_t
		BGl_stringzd2capitaliza7ez75zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_z62escapezd2Czd2stringz62zz__r4_strings_6_7z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_stringzd2capitaliza7ez12z67zz__r4_strings_6_7z00(obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_stringzd2suffixzd2cizf3zf3zz__r4_strings_6_7z00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_stringze3zf3z10zz__r4_strings_6_7z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_substringzd2urzd2zz__r4_strings_6_7z00(obj_t,
		long, long);
	BGL_EXPORTED_DECL bool_t
		BGl_substringzd2cizd2atzf3zf3zz__r4_strings_6_7z00(obj_t, obj_t, long,
		obj_t);
	static obj_t BGl_z62stringzd2compare3zd2ciz62zz__r4_strings_6_7z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62stringz62zz__r4_strings_6_7z00(obj_t, obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	BGL_EXPORTED_DECL long BGl_stringzd2compare3zd2zz__r4_strings_6_7z00(obj_t,
		obj_t);
	static obj_t BGl_comparezd2leftzd2zz__r4_strings_6_7z00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62stringzd2charzd2indexzd2urzb0zz__r4_strings_6_7z00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl__substringzd2atzf3z21zz__r4_strings_6_7z00(obj_t, obj_t);
	static obj_t BGl_z62stringzd2compare3zb0zz__r4_strings_6_7z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl__stringzd2skipzd2rightz00zz__r4_strings_6_7z00(obj_t, obj_t);
	extern obj_t c_substring(obj_t, long, long);
	BGL_EXPORTED_DECL obj_t
		BGl_stringzd2hexzd2internz00zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_z62stringzd2cizc3zf3z80zz__r4_strings_6_7z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62stringzc3zd3zf3z81zz__r4_strings_6_7z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL int
		BGl_stringzd2prefixzd2lengthzd2cizd2zz__r4_strings_6_7z00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl__stringzd2containszd2ciz00zz__r4_strings_6_7z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_stringzd2ze3listz31zz__r4_strings_6_7z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_stringzd2aszd2readz00zz__r4_strings_6_7z00(obj_t);
	BGL_EXPORTED_DECL unsigned char
		BGl_stringzd2refzd2zz__r4_strings_6_7z00(obj_t, long);
	BGL_EXPORTED_DECL obj_t BGl_makezd2stringzd2zz__r4_strings_6_7z00(long,
		obj_t);
	static obj_t BGl_z62stringzd2replacezb0zz__r4_strings_6_7z00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t bgl_escape_C_string(char *, long, long);
	static obj_t BGl_z62stringzd2capitaliza7ez12z05zz__r4_strings_6_7z00(obj_t,
		obj_t);
	static obj_t BGl_z62stringzd2cizd3zf3z90zz__r4_strings_6_7z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_comparezd2rightzd2zz__r4_strings_6_7z00(obj_t, obj_t, obj_t,
		obj_t);
	extern bool_t bigloo_string_cige(obj_t, obj_t);
	extern obj_t blit_string(obj_t, long, obj_t, long, long);
	static obj_t BGl__stringzd2deletezd2zz__r4_strings_6_7z00(obj_t, obj_t);
	static obj_t BGl_z62stringzd2setz12za2zz__r4_strings_6_7z00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl__stringzd2suffixzf3z21zz__r4_strings_6_7z00(obj_t, obj_t);
	static obj_t BGl__stringzd2prefixzd2lengthzd2cizd2zz__r4_strings_6_7z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_stringzd2charzd2indexzd2urzd2zz__r4_strings_6_7z00(obj_t, unsigned char,
		long, long);
	extern bool_t bigloo_string_cigt(obj_t, obj_t);
	extern bool_t bigloo_string_ge(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_stringzd2prefixzf3z21zz__r4_strings_6_7z00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62stringzd2cize3zf3za0zz__r4_strings_6_7z00(obj_t, obj_t,
		obj_t);
	extern bool_t bigloo_string_gt(obj_t, obj_t);
	static obj_t BGl_z62blitzd2stringz12za2zz__r4_strings_6_7z00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_stringzd2downcasez12zc0zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_strnatcmpz00zz__r4_strings_6_7z00(obj_t, obj_t, bool_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_substringzd3zf3z20zz__r4_strings_6_7z00(obj_t,
		obj_t, long);
	extern obj_t make_string(long, unsigned char);
	static obj_t BGl_z62stringzd2refzb0zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_stringzd2cutzd2zz__r4_strings_6_7z00(obj_t,
		obj_t);
	static obj_t BGl_z62stringzd2fillz12za2zz__r4_strings_6_7z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_stringzf3zf3zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_z62stringzc3zf3z52zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62stringzf3z91zz__r4_strings_6_7z00(obj_t, obj_t);
	extern obj_t string_append(obj_t, obj_t);
	static obj_t BGl_z62stringzd2copyzb0zz__r4_strings_6_7z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_stringzd2upcasezd2zz__r4_strings_6_7z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_stringzd2setzd2urz12z12zz__r4_strings_6_7z00(obj_t, long,
		unsigned char);
	static obj_t BGl__stringzd2indexzd2rightz00zz__r4_strings_6_7z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_stringzd2hexzd2externz00zz__r4_strings_6_7z00(obj_t, int, long);
	BGL_EXPORTED_DECL bool_t BGl_stringzc3zd3zf3ze3zz__r4_strings_6_7z00(obj_t,
		obj_t);
	extern obj_t make_string_sans_fill(long);
	BGL_EXPORTED_DECL obj_t BGl_blitzd2stringz12zc0zz__r4_strings_6_7z00(obj_t,
		long, obj_t, long, long);
	static obj_t BGl_z62stringze3zd3zf3za1zz__r4_strings_6_7z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_stringzd2downcasezd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_z62stringzd3zf3z42zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62stringzd2downcasezb0zz__r4_strings_6_7z00(obj_t, obj_t);
	static obj_t BGl__substringz00zz__r4_strings_6_7z00(obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_stringzd2suffixzd2lengthzd2cizd2zz__r4_strings_6_7z00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	extern bool_t bigloo_strncmp(obj_t, obj_t, long);
	BGL_EXPORTED_DECL int
		BGl_stringzd2naturalzd2compare3z00zz__r4_strings_6_7z00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_substringzd2atzf3z21zz__r4_strings_6_7z00(obj_t,
		obj_t, long, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_stringzd2prefixzd2cizf3zf3zz__r4_strings_6_7z00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_stringzd2fillz12zc0zz__r4_strings_6_7z00(obj_t,
		unsigned char);
	static obj_t BGl_z62stringzd2cutzb0zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_stringz00zz__r4_strings_6_7z00(obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2upcasez12zd2envz12zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7d2upca2784z00,
		BGl_z62stringzd2upcasez12za2zz__r4_strings_6_7z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2capitaliza7ezd2envza7zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7d2capi2785z00,
		BGl_z62stringzd2capitaliza7ez17zz__r4_strings_6_7z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2761z00zz__r4_strings_6_7z00,
		BgL_bgl_string2761za700za7za7_2786za7, "string-prefix?", 14);
	      DEFINE_STRING(BGl_string2680z00zz__r4_strings_6_7z00,
		BgL_bgl_string2680za700za7za7_2787za7, "&string-ci<?", 12);
	      DEFINE_STRING(BGl_string2762z00zz__r4_strings_6_7z00,
		BgL_bgl_string2762za700za7za7_2788za7, "_string-prefix-ci?", 18);
	      DEFINE_STRING(BGl_string2681z00zz__r4_strings_6_7z00,
		BgL_bgl_string2681za700za7za7_2789za7, "&string-ci>?", 12);
	      DEFINE_STRING(BGl_string2682z00zz__r4_strings_6_7z00,
		BgL_bgl_string2682za700za7za7_2790za7, "&string-ci<=?", 13);
	      DEFINE_STRING(BGl_string2764z00zz__r4_strings_6_7z00,
		BgL_bgl_string2764za700za7za7_2791za7, "string-prefix-ci?", 17);
	      DEFINE_STRING(BGl_string2683z00zz__r4_strings_6_7z00,
		BgL_bgl_string2683za700za7za7_2792za7, "&string-ci>=?", 13);
	      DEFINE_STRING(BGl_string2765z00zz__r4_strings_6_7z00,
		BgL_bgl_string2765za700za7za7_2793za7, "_string-suffix?", 15);
	      DEFINE_STRING(BGl_string2684z00zz__r4_strings_6_7z00,
		BgL_bgl_string2684za700za7za7_2794za7, "_substring", 10);
	      DEFINE_STRING(BGl_string2685z00zz__r4_strings_6_7z00,
		BgL_bgl_string2685za700za7za7_2795za7, "Illegal start index ", 20);
	      DEFINE_STRING(BGl_string2767z00zz__r4_strings_6_7z00,
		BgL_bgl_string2767za700za7za7_2796za7, "string-suffix?", 14);
	      DEFINE_STRING(BGl_string2686z00zz__r4_strings_6_7z00,
		BgL_bgl_string2686za700za7za7_2797za7, "substring", 9);
	      DEFINE_STRING(BGl_string2768z00zz__r4_strings_6_7z00,
		BgL_bgl_string2768za700za7za7_2798za7, "_string-suffix-ci?", 18);
	      DEFINE_STRING(BGl_string2687z00zz__r4_strings_6_7z00,
		BgL_bgl_string2687za700za7za7_2799za7, "Illegal end index ", 18);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2suffixzf3zd2envzf3zz__r4_strings_6_7z00,
		BgL_bgl__stringza7d2suffix2800za7, opt_generic_entry,
		BGl__stringzd2suffixzf3z21zz__r4_strings_6_7z00, BFALSE, -1);
	      DEFINE_STRING(BGl_string2769z00zz__r4_strings_6_7z00,
		BgL_bgl_string2769za700za7za7_2801za7, "_string-natural-compare3", 24);
	      DEFINE_STRING(BGl_string2688z00zz__r4_strings_6_7z00,
		BgL_bgl_string2688za700za7za7_2802za7, "&substring-ur", 13);
	      DEFINE_STRING(BGl_string2689z00zz__r4_strings_6_7z00,
		BgL_bgl_string2689za700za7za7_2803za7, "_string-contains", 16);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2compare3zd2envz00zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7d2comp2804z00,
		BGl_z62stringzd2compare3zb0zz__r4_strings_6_7z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_stringzd2copyzd2envz00zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7d2copy2805z00,
		BGl_z62stringzd2copyzb0zz__r4_strings_6_7z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2upcasezd2envz00zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7d2upca2806z00,
		BGl_z62stringzd2upcasezb0zz__r4_strings_6_7z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2setzd2urz12zd2envzc0zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7d2setza72807za7,
		BGl_z62stringzd2setzd2urz12z70zz__r4_strings_6_7z00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2hexzd2internzd2envzd2zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7d2hexza72808za7,
		BGl_z62stringzd2hexzd2internz62zz__r4_strings_6_7z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2hexzd2internz12zd2envzc0zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7d2hexza72809za7,
		BGl_z62stringzd2hexzd2internz12z70zz__r4_strings_6_7z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2cizc3zd3zf3zd2envze3zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7d2ciza7c2810za7,
		BGl_z62stringzd2cizc3zd3zf3z53zz__r4_strings_6_7z00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2770z00zz__r4_strings_6_7z00,
		BgL_bgl_string2770za700za7za7_2811za7, "_string-natural-compare3-ci", 27);
	      DEFINE_STRING(BGl_string2690z00zz__r4_strings_6_7z00,
		BgL_bgl_string2690za700za7za7_2812za7, "_string-contains-ci", 19);
	      DEFINE_STRING(BGl_string2772z00zz__r4_strings_6_7z00,
		BgL_bgl_string2772za700za7za7_2813za7, "an-awful-hack", 13);
	      DEFINE_STRING(BGl_string2691z00zz__r4_strings_6_7z00,
		BgL_bgl_string2691za700za7za7_2814za7, "&string-compare3", 16);
	      DEFINE_STRING(BGl_string2773z00zz__r4_strings_6_7z00,
		BgL_bgl_string2773za700za7za7_2815za7, "hex-string->string", 18);
	      DEFINE_STRING(BGl_string2692z00zz__r4_strings_6_7z00,
		BgL_bgl_string2692za700za7za7_2816za7, "&string-compare3-ci", 19);
	      DEFINE_STRING(BGl_string2774z00zz__r4_strings_6_7z00,
		BgL_bgl_string2774za700za7za7_2817za7, "Illegal string (illegal character)",
		34);
	      DEFINE_STRING(BGl_string2693z00zz__r4_strings_6_7z00,
		BgL_bgl_string2693za700za7za7_2818za7, "", 0);
	      DEFINE_STRING(BGl_string2775z00zz__r4_strings_6_7z00,
		BgL_bgl_string2775za700za7za7_2819za7, "string-hex-intern", 17);
	      DEFINE_STRING(BGl_string2694z00zz__r4_strings_6_7z00,
		BgL_bgl_string2694za700za7za7_2820za7, "&list->string", 13);
	      DEFINE_STRING(BGl_string2776z00zz__r4_strings_6_7z00,
		BgL_bgl_string2776za700za7za7_2821za7, "Illegal string (length is odd)",
		30);
	      DEFINE_STRING(BGl_string2695z00zz__r4_strings_6_7z00,
		BgL_bgl_string2695za700za7za7_2822za7, "pair-nil", 8);
	      DEFINE_STRING(BGl_string2777z00zz__r4_strings_6_7z00,
		BgL_bgl_string2777za700za7za7_2823za7, "&string-hex-intern", 18);
	      DEFINE_STRING(BGl_string2696z00zz__r4_strings_6_7z00,
		BgL_bgl_string2696za700za7za7_2824za7, "&string->list", 13);
	      DEFINE_STRING(BGl_string2778z00zz__r4_strings_6_7z00,
		BgL_bgl_string2778za700za7za7_2825za7, "string-hex-intern!", 18);
	      DEFINE_STRING(BGl_string2697z00zz__r4_strings_6_7z00,
		BgL_bgl_string2697za700za7za7_2826za7, "&string-copy", 12);
	      DEFINE_STRING(BGl_string2779z00zz__r4_strings_6_7z00,
		BgL_bgl_string2779za700za7za7_2827za7, "&string-hex-intern!", 19);
	      DEFINE_STRING(BGl_string2698z00zz__r4_strings_6_7z00,
		BgL_bgl_string2698za700za7za7_2828za7, "&string-fill!", 13);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2prefixzd2lengthzd2cizd2envz00zz__r4_strings_6_7z00,
		BgL_bgl__stringza7d2prefix2829za7, opt_generic_entry,
		BGl__stringzd2prefixzd2lengthzd2cizd2zz__r4_strings_6_7z00, BFALSE, -1);
	      DEFINE_STRING(BGl_string2699z00zz__r4_strings_6_7z00,
		BgL_bgl_string2699za700za7za7_2830za7, "&string-upcase", 14);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_stringzd2skipzd2envz00zz__r4_strings_6_7z00,
		BgL_bgl__stringza7d2skipza7d2831z00, opt_generic_entry,
		BGl__stringzd2skipzd2zz__r4_strings_6_7z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2ze3listzd2envze3zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7d2za7e3l2832za7,
		BGl_z62stringzd2ze3listz53zz__r4_strings_6_7z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2suffixzd2lengthzd2cizd2envz00zz__r4_strings_6_7z00,
		BgL_bgl__stringza7d2suffix2833za7, opt_generic_entry,
		BGl__stringzd2suffixzd2lengthzd2cizd2zz__r4_strings_6_7z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2stringzd2envz00zz__r4_strings_6_7z00,
		BgL_bgl__makeza7d2stringza7d2834z00, opt_generic_entry,
		BGl__makezd2stringzd2zz__r4_strings_6_7z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2suffixzd2lengthzd2envzd2zz__r4_strings_6_7z00,
		BgL_bgl__stringza7d2suffix2835za7, opt_generic_entry,
		BGl__stringzd2suffixzd2lengthz00zz__r4_strings_6_7z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2cizd3zf3zd2envz20zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7d2ciza7d2836za7,
		BGl_z62stringzd2cizd3zf3z90zz__r4_strings_6_7z00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2780z00zz__r4_strings_6_7z00,
		BgL_bgl_string2780za700za7za7_2837za7, "_string-hex-extern", 18);
	      DEFINE_STRING(BGl_string2781z00zz__r4_strings_6_7z00,
		BgL_bgl_string2781za700za7za7_2838za7, "string-hex-extern", 17);
	      DEFINE_STRING(BGl_string2782z00zz__r4_strings_6_7z00,
		BgL_bgl_string2782za700za7za7_2839za7, "0123456789abcdef", 16);
	      DEFINE_STRING(BGl_string2783z00zz__r4_strings_6_7z00,
		BgL_bgl_string2783za700za7za7_2840za7, "__r4_strings_6_7", 16);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2compare3zd2cizd2envzd2zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7d2comp2841z00,
		BGl_z62stringzd2compare3zd2ciz62zz__r4_strings_6_7z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2appendzd2envz00zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7d2appe2842z00, va_generic_entry,
		BGl_z62stringzd2appendzb0zz__r4_strings_6_7z00, BUNSPEC, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_substringzd2envzd2zz__r4_strings_6_7z00,
		BgL_bgl__substringza700za7za7_2843za7, opt_generic_entry,
		BGl__substringz00zz__r4_strings_6_7z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2containszd2envz00zz__r4_strings_6_7z00,
		BgL_bgl__stringza7d2contai2844za7, opt_generic_entry,
		BGl__stringzd2containszd2zz__r4_strings_6_7z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2indexzd2rightzd2envzd2zz__r4_strings_6_7z00,
		BgL_bgl__stringza7d2indexza72845z00, opt_generic_entry,
		BGl__stringzd2indexzd2rightz00zz__r4_strings_6_7z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_stringzd3zf3zd2envzf2zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7d3za7f3za72846z00,
		BGl_z62stringzd3zf3z42zz__r4_strings_6_7z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2naturalzd2compare3zd2cizd2envz00zz__r4_strings_6_7z00,
		BgL_bgl__stringza7d2natura2847za7, opt_generic_entry,
		BGl__stringzd2naturalzd2compare3zd2cizd2zz__r4_strings_6_7z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2containszd2cizd2envzd2zz__r4_strings_6_7z00,
		BgL_bgl__stringza7d2contai2848za7, opt_generic_entry,
		BGl__stringzd2containszd2ciz00zz__r4_strings_6_7z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2capitaliza7ez12zd2envzb5zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7d2capi2849z00,
		BGl_z62stringzd2capitaliza7ez12z05zz__r4_strings_6_7z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2shrinkz12zd2envz12zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7d2shri2850z00,
		BGl_z62stringzd2shrinkz12za2zz__r4_strings_6_7z00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_stringzf3zd2envz21zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7f3za791za72851z00,
		BGl_z62stringzf3z91zz__r4_strings_6_7z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_substringzd2cizd2atzf3zd2envz21zz__r4_strings_6_7z00,
		BgL_bgl__substringza7d2ciza72852z00, opt_generic_entry,
		BGl__substringzd2cizd2atzf3zf3zz__r4_strings_6_7z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2indexzd2envz00zz__r4_strings_6_7z00,
		BgL_bgl__stringza7d2indexza72853z00, opt_generic_entry,
		BGl__stringzd2indexzd2zz__r4_strings_6_7z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blitzd2stringzd2urz12zd2envzc0zz__r4_strings_6_7z00,
		BgL_bgl_za762blitza7d2string2854z00,
		BGl_z62blitzd2stringzd2urz12z70zz__r4_strings_6_7z00, 0L, BUNSPEC, 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringze3zd3zf3zd2envz11zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7e3za7d3za72855z00,
		BGl_z62stringze3zd3zf3za1zz__r4_strings_6_7z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2naturalzd2compare3zd2envzd2zz__r4_strings_6_7z00,
		BgL_bgl__stringza7d2natura2856za7, opt_generic_entry,
		BGl__stringzd2naturalzd2compare3z00zz__r4_strings_6_7z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_substringzd2atzf3zd2envzf3zz__r4_strings_6_7z00,
		BgL_bgl__substringza7d2atza72857z00, opt_generic_entry,
		BGl__substringzd2atzf3z21zz__r4_strings_6_7z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2prefixzd2cizf3zd2envz21zz__r4_strings_6_7z00,
		BgL_bgl__stringza7d2prefix2858za7, opt_generic_entry,
		BGl__stringzd2prefixzd2cizf3zf3zz__r4_strings_6_7z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2setz12zd2envz12zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7d2setza72859za7,
		BGl_z62stringzd2setz12za2zz__r4_strings_6_7z00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2charzd2indexzd2urzd2envz00zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7d2char2860z00,
		BGl_z62stringzd2charzd2indexzd2urzb0zz__r4_strings_6_7z00, 0L, BUNSPEC, 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2prefixzd2lengthzd2envzd2zz__r4_strings_6_7z00,
		BgL_bgl__stringza7d2prefix2861za7, opt_generic_entry,
		BGl__stringzd2prefixzd2lengthz00zz__r4_strings_6_7z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2cize3zf3zd2envz10zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7d2ciza7e2862za7,
		BGl_z62stringzd2cize3zf3za0zz__r4_strings_6_7z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2splitzd2envz00zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7d2spli2863z00, va_generic_entry,
		BGl_z62stringzd2splitzb0zz__r4_strings_6_7z00, BUNSPEC, -2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_emptyzd2stringzf3zd2envzf3zz__r4_strings_6_7z00,
		BgL_bgl_za762emptyza7d2strin2864z00,
		BGl_z62emptyzd2stringzf3z43zz__r4_strings_6_7z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2refzd2urzd2envzd2zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7d2refza72865za7,
		BGl_z62stringzd2refzd2urz62zz__r4_strings_6_7z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_stringze3zf3zd2envzc2zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7e3za7f3za72866z00,
		BGl_z62stringze3zf3z72zz__r4_strings_6_7z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzc3zd3zf3zd2envz31zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7c3za7d3za72867z00,
		BGl_z62stringzc3zd3zf3z81zz__r4_strings_6_7z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2replacez12zd2envz12zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7d2repl2868z00,
		BGl_z62stringzd2replacez12za2zz__r4_strings_6_7z00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2skipzd2rightzd2envzd2zz__r4_strings_6_7z00,
		BgL_bgl__stringza7d2skipza7d2869z00, opt_generic_entry,
		BGl__stringzd2skipzd2rightz00zz__r4_strings_6_7z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2lengthzd2envz00zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7d2leng2870z00,
		BGl_z62stringzd2lengthzb0zz__r4_strings_6_7z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2700z00zz__r4_strings_6_7z00,
		BgL_bgl_string2700za700za7za7_2871za7, "&string-downcase", 16);
	      DEFINE_STRING(BGl_string2701z00zz__r4_strings_6_7z00,
		BgL_bgl_string2701za700za7za7_2872za7, "&string-upcase!", 15);
	      DEFINE_STRING(BGl_string2702z00zz__r4_strings_6_7z00,
		BgL_bgl_string2702za700za7za7_2873za7, "&string-downcase!", 17);
	      DEFINE_STRING(BGl_string2703z00zz__r4_strings_6_7z00,
		BgL_bgl_string2703za700za7za7_2874za7, "&string-capitalize!", 19);
	      DEFINE_STRING(BGl_string2704z00zz__r4_strings_6_7z00,
		BgL_bgl_string2704za700za7za7_2875za7, "&string-capitalize", 18);
	      DEFINE_STRING(BGl_string2705z00zz__r4_strings_6_7z00,
		BgL_bgl_string2705za700za7za7_2876za7, "&string-for-read", 16);
	      DEFINE_STRING(BGl_string2706z00zz__r4_strings_6_7z00,
		BgL_bgl_string2706za700za7za7_2877za7, "&escape-C-string", 16);
	      DEFINE_STRING(BGl_string2707z00zz__r4_strings_6_7z00,
		BgL_bgl_string2707za700za7za7_2878za7, "&escape-scheme-string", 21);
	      DEFINE_STRING(BGl_string2708z00zz__r4_strings_6_7z00,
		BgL_bgl_string2708za700za7za7_2879za7, "&string-as-read", 15);
	      DEFINE_STRING(BGl_string2709z00zz__r4_strings_6_7z00,
		BgL_bgl_string2709za700za7za7_2880za7, "&blit-string-ur!", 16);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_stringzd2envzd2zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza762za7za7__2881z00, va_generic_entry,
		BGl_z62stringz62zz__r4_strings_6_7z00, BUNSPEC, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2charzd2indexzd2envzd2zz__r4_strings_6_7z00,
		BgL_bgl__stringza7d2charza7d2882z00, opt_generic_entry,
		BGl__stringzd2charzd2indexz00zz__r4_strings_6_7z00, BFALSE, -1);
	      DEFINE_STRING(BGl_string2710z00zz__r4_strings_6_7z00,
		BgL_bgl_string2710za700za7za7_2883za7, "]", 1);
	      DEFINE_STRING(BGl_string2711z00zz__r4_strings_6_7z00,
		BgL_bgl_string2711za700za7za7_2884za7, "] [dest:", 8);
	      DEFINE_STRING(BGl_string2712z00zz__r4_strings_6_7z00,
		BgL_bgl_string2712za700za7za7_2885za7, "[src:", 5);
	      DEFINE_STRING(BGl_string2713z00zz__r4_strings_6_7z00,
		BgL_bgl_string2713za700za7za7_2886za7,
		"blit-string!:Index and length out of range", 42);
	      DEFINE_STRING(BGl_string2714z00zz__r4_strings_6_7z00,
		BgL_bgl_string2714za700za7za7_2887za7, "&blit-string!", 13);
	      DEFINE_STRING(BGl_string2715z00zz__r4_strings_6_7z00,
		BgL_bgl_string2715za700za7za7_2888za7, "&string-shrink!", 15);
	      DEFINE_STRING(BGl_string2716z00zz__r4_strings_6_7z00,
		BgL_bgl_string2716za700za7za7_2889za7, "&string-replace", 15);
	      DEFINE_STRING(BGl_string2717z00zz__r4_strings_6_7z00,
		BgL_bgl_string2717za700za7za7_2890za7, "&string-replace!", 16);
	      DEFINE_STRING(BGl_string2718z00zz__r4_strings_6_7z00,
		BgL_bgl_string2718za700za7za7_2891za7, "_string-delete", 14);
	      DEFINE_STRING(BGl_string2719z00zz__r4_strings_6_7z00,
		BgL_bgl_string2719za700za7za7_2892za7, "string-delete", 13);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2deletezd2envz00zz__r4_strings_6_7z00,
		BgL_bgl__stringza7d2delete2893za7, opt_generic_entry,
		BGl__stringzd2deletezd2zz__r4_strings_6_7z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2nullzf3zd2envzf3zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7d2null2894z00,
		BGl_z62stringzd2nullzf3z43zz__r4_strings_6_7z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_escapezd2Czd2stringzd2envzd2zz__r4_strings_6_7z00,
		BgL_bgl_za762escapeza7d2cza7d22895za7,
		BGl_z62escapezd2Czd2stringz62zz__r4_strings_6_7z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2720z00zz__r4_strings_6_7z00,
		BgL_bgl_string2720za700za7za7_2896za7, "start index out of range", 24);
	      DEFINE_STRING(BGl_string2721z00zz__r4_strings_6_7z00,
		BgL_bgl_string2721za700za7za7_2897za7, "end index out of range", 22);
	      DEFINE_STRING(BGl_string2722z00zz__r4_strings_6_7z00,
		BgL_bgl_string2722za700za7za7_2898za7, "Illegal indices", 15);
	      DEFINE_STRING(BGl_string2723z00zz__r4_strings_6_7z00,
		BgL_bgl_string2723za700za7za7_2899za7, "Illegal char/charset/predicate",
		30);
	      DEFINE_STRING(BGl_string2724z00zz__r4_strings_6_7z00,
		BgL_bgl_string2724za700za7za7_2900za7, " \t\n", 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_stringzd2cutzd2envz00zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7d2cutza72901za7, va_generic_entry,
		BGl_z62stringzd2cutzb0zz__r4_strings_6_7z00, BUNSPEC, -2);
	      DEFINE_STRING(BGl_string2725z00zz__r4_strings_6_7z00,
		BgL_bgl_string2725za700za7za7_2902za7, "&string-split", 13);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2suffixzd2cizf3zd2envz21zz__r4_strings_6_7z00,
		BgL_bgl__stringza7d2suffix2903za7, opt_generic_entry,
		BGl__stringzd2suffixzd2cizf3zf3zz__r4_strings_6_7z00, BFALSE, -1);
	      DEFINE_STRING(BGl_string2726z00zz__r4_strings_6_7z00,
		BgL_bgl_string2726za700za7za7_2904za7, "&string-cut", 11);
	      DEFINE_STRING(BGl_string2727z00zz__r4_strings_6_7z00,
		BgL_bgl_string2727za700za7za7_2905za7, "&string-char-index-ur", 21);
	      DEFINE_STRING(BGl_string2728z00zz__r4_strings_6_7z00,
		BgL_bgl_string2728za700za7za7_2906za7, "_string-char-index", 18);
	      DEFINE_STRING(BGl_string2729z00zz__r4_strings_6_7z00,
		BgL_bgl_string2729za700za7za7_2907za7, "_string-index", 13);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_substringzd2urzd2envz00zz__r4_strings_6_7z00,
		BgL_bgl_za762substringza7d2u2908z00,
		BGl_z62substringzd2urzb0zz__r4_strings_6_7z00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_substringzd2cizd3zf3zd2envz20zz__r4_strings_6_7z00,
		BgL_bgl_za762substringza7d2c2909z00,
		BGl_z62substringzd2cizd3zf3z90zz__r4_strings_6_7z00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_escapezd2schemezd2stringzd2envzd2zz__r4_strings_6_7z00,
		BgL_bgl_za762escapeza7d2sche2910z00,
		BGl_z62escapezd2schemezd2stringz62zz__r4_strings_6_7z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2cize3zd3zf3zd2envzc3zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7d2ciza7e2911za7,
		BGl_z62stringzd2cize3zd3zf3z73zz__r4_strings_6_7z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2cizc3zf3zd2envz30zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7d2ciza7c2912za7,
		BGl_z62stringzd2cizc3zf3z80zz__r4_strings_6_7z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2hexzd2externzd2envzd2zz__r4_strings_6_7z00,
		BgL_bgl__stringza7d2hexza7d22913z00, opt_generic_entry,
		BGl__stringzd2hexzd2externz00zz__r4_strings_6_7z00, BFALSE, -1);
	      DEFINE_STRING(BGl_string2730z00zz__r4_strings_6_7z00,
		BgL_bgl_string2730za700za7za7_2914za7, "string-index", 12);
	      DEFINE_STRING(BGl_string2731z00zz__r4_strings_6_7z00,
		BgL_bgl_string2731za700za7za7_2915za7, "Illegal regset", 14);
	      DEFINE_STRING(BGl_string2732z00zz__r4_strings_6_7z00,
		BgL_bgl_string2732za700za7za7_2916za7, "_string-index-right", 19);
	      DEFINE_STRING(BGl_string2733z00zz__r4_strings_6_7z00,
		BgL_bgl_string2733za700za7za7_2917za7, "string-index-right", 18);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blitzd2stringz12zd2envz12zz__r4_strings_6_7z00,
		BgL_bgl_za762blitza7d2string2918z00,
		BGl_z62blitzd2stringz12za2zz__r4_strings_6_7z00, 0L, BUNSPEC, 5);
	      DEFINE_STRING(BGl_string2734z00zz__r4_strings_6_7z00,
		BgL_bgl_string2734za700za7za7_2919za7, "index out of bound", 18);
	      DEFINE_STRING(BGl_string2735z00zz__r4_strings_6_7z00,
		BgL_bgl_string2735za700za7za7_2920za7, "_string-skip", 12);
	      DEFINE_STRING(BGl_string2736z00zz__r4_strings_6_7z00,
		BgL_bgl_string2736za700za7za7_2921za7, "string-skip", 11);
	      DEFINE_STRING(BGl_string2737z00zz__r4_strings_6_7z00,
		BgL_bgl_string2737za700za7za7_2922za7, "_string-skip-right", 18);
	      DEFINE_STRING(BGl_string2738z00zz__r4_strings_6_7z00,
		BgL_bgl_string2738za700za7za7_2923za7, "_string-prefix-length", 21);
	      DEFINE_STRING(BGl_string2658z00zz__r4_strings_6_7z00,
		BgL_bgl_string2658za700za7za7_2924za7,
		"/tmp/bigloo/runtime/Ieee/string.scm", 35);
	      DEFINE_STRING(BGl_string2659z00zz__r4_strings_6_7z00,
		BgL_bgl_string2659za700za7za7_2925za7, "&string-null?", 13);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2downcasezd2envz00zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7d2down2926z00,
		BGl_z62stringzd2downcasezb0zz__r4_strings_6_7z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_listzd2ze3stringzd2envze3zz__r4_strings_6_7z00,
		BgL_bgl_za762listza7d2za7e3str2927za7,
		BGl_z62listzd2ze3stringz53zz__r4_strings_6_7z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2aszd2readzd2envzd2zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7d2asza7d2928za7,
		BGl_z62stringzd2aszd2readz62zz__r4_strings_6_7z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_stringzd2refzd2envz00zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7d2refza72929za7,
		BGl_z62stringzd2refzb0zz__r4_strings_6_7z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2fillz12zd2envz12zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7d2fill2930z00,
		BGl_z62stringzd2fillz12za2zz__r4_strings_6_7z00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2740z00zz__r4_strings_6_7z00,
		BgL_bgl_string2740za700za7za7_2931za7, "string-prefix-length", 20);
	      DEFINE_STRING(BGl_string2741z00zz__r4_strings_6_7z00,
		BgL_bgl_string2741za700za7za7_2932za7, "Index negative end index `", 26);
	      DEFINE_STRING(BGl_string2660z00zz__r4_strings_6_7z00,
		BgL_bgl_string2660za700za7za7_2933za7, "bstring", 7);
	      DEFINE_STRING(BGl_string2742z00zz__r4_strings_6_7z00,
		BgL_bgl_string2742za700za7za7_2934za7, "end1", 4);
	      DEFINE_STRING(BGl_string2661z00zz__r4_strings_6_7z00,
		BgL_bgl_string2661za700za7za7_2935za7, "_make-string", 12);
	      DEFINE_STRING(BGl_string2743z00zz__r4_strings_6_7z00,
		BgL_bgl_string2743za700za7za7_2936za7, "'", 1);
	      DEFINE_STRING(BGl_string2662z00zz__r4_strings_6_7z00,
		BgL_bgl_string2662za700za7za7_2937za7, "bint", 4);
	      DEFINE_STRING(BGl_string2744z00zz__r4_strings_6_7z00,
		BgL_bgl_string2744za700za7za7_2938za7, "Too large end index `", 21);
	      DEFINE_STRING(BGl_string2663z00zz__r4_strings_6_7z00,
		BgL_bgl_string2663za700za7za7_2939za7, "bchar", 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_stringzc3zf3zd2envze2zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7c3za7f3za72940z00,
		BGl_z62stringzc3zf3z52zz__r4_strings_6_7z00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2745z00zz__r4_strings_6_7z00,
		BgL_bgl_string2745za700za7za7_2941za7, "end2", 4);
	      DEFINE_STRING(BGl_string2664z00zz__r4_strings_6_7z00,
		BgL_bgl_string2664za700za7za7_2942za7, "&string-length", 14);
	      DEFINE_STRING(BGl_string2746z00zz__r4_strings_6_7z00,
		BgL_bgl_string2746za700za7za7_2943za7, "Index negative start index `", 28);
	      DEFINE_STRING(BGl_string2665z00zz__r4_strings_6_7z00,
		BgL_bgl_string2665za700za7za7_2944za7, "&string-ref", 11);
	      DEFINE_STRING(BGl_string2747z00zz__r4_strings_6_7z00,
		BgL_bgl_string2747za700za7za7_2945za7, "start1", 6);
	      DEFINE_STRING(BGl_string2666z00zz__r4_strings_6_7z00,
		BgL_bgl_string2666za700za7za7_2946za7, "&string-set!", 12);
	      DEFINE_STRING(BGl_string2748z00zz__r4_strings_6_7z00,
		BgL_bgl_string2748za700za7za7_2947za7, "Too large start index `", 23);
	      DEFINE_STRING(BGl_string2667z00zz__r4_strings_6_7z00,
		BgL_bgl_string2667za700za7za7_2948za7, "&string-ref-ur", 14);
	      DEFINE_STRING(BGl_string2749z00zz__r4_strings_6_7z00,
		BgL_bgl_string2749za700za7za7_2949za7, "start2", 6);
	      DEFINE_STRING(BGl_string2668z00zz__r4_strings_6_7z00,
		BgL_bgl_string2668za700za7za7_2950za7, "&string-set-ur!", 15);
	      DEFINE_STRING(BGl_string2669z00zz__r4_strings_6_7z00,
		BgL_bgl_string2669za700za7za7_2951za7, "&string=?", 9);
	      DEFINE_STRING(BGl_string2750z00zz__r4_strings_6_7z00,
		BgL_bgl_string2750za700za7za7_2952za7, "_string-prefix-length-ci", 24);
	      DEFINE_STRING(BGl_string2670z00zz__r4_strings_6_7z00,
		BgL_bgl_string2670za700za7za7_2953za7, "&substring=?", 12);
	      DEFINE_STRING(BGl_string2752z00zz__r4_strings_6_7z00,
		BgL_bgl_string2752za700za7za7_2954za7, "string-prefix-length-ci", 23);
	      DEFINE_STRING(BGl_string2671z00zz__r4_strings_6_7z00,
		BgL_bgl_string2671za700za7za7_2955za7, "_substring-at?", 14);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2prefixzf3zd2envzf3zz__r4_strings_6_7z00,
		BgL_bgl__stringza7d2prefix2956za7, opt_generic_entry,
		BGl__stringzd2prefixzf3z21zz__r4_strings_6_7z00, BFALSE, -1);
	      DEFINE_STRING(BGl_string2753z00zz__r4_strings_6_7z00,
		BgL_bgl_string2753za700za7za7_2957za7, "_string-suffix-length", 21);
	      DEFINE_STRING(BGl_string2672z00zz__r4_strings_6_7z00,
		BgL_bgl_string2672za700za7za7_2958za7, "&substring-ci=?", 15);
	      DEFINE_STRING(BGl_string2673z00zz__r4_strings_6_7z00,
		BgL_bgl_string2673za700za7za7_2959za7, "_substring-ci-at?", 17);
	      DEFINE_STRING(BGl_string2755z00zz__r4_strings_6_7z00,
		BgL_bgl_string2755za700za7za7_2960za7, "string-suffix-length", 20);
	      DEFINE_STRING(BGl_string2674z00zz__r4_strings_6_7z00,
		BgL_bgl_string2674za700za7za7_2961za7, "&empty-string?", 14);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2forzd2readzd2envzd2zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7d2forza72962za7,
		BGl_z62stringzd2forzd2readz62zz__r4_strings_6_7z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2756z00zz__r4_strings_6_7z00,
		BgL_bgl_string2756za700za7za7_2963za7, "_string-suffix-length-ci", 24);
	      DEFINE_STRING(BGl_string2675z00zz__r4_strings_6_7z00,
		BgL_bgl_string2675za700za7za7_2964za7, "&string-ci=?", 12);
	      DEFINE_STRING(BGl_string2676z00zz__r4_strings_6_7z00,
		BgL_bgl_string2676za700za7za7_2965za7, "&string<?", 9);
	      DEFINE_STRING(BGl_string2758z00zz__r4_strings_6_7z00,
		BgL_bgl_string2758za700za7za7_2966za7, "string-suffix-length-ci", 23);
	      DEFINE_STRING(BGl_string2677z00zz__r4_strings_6_7z00,
		BgL_bgl_string2677za700za7za7_2967za7, "&string>?", 9);
	      DEFINE_STRING(BGl_string2759z00zz__r4_strings_6_7z00,
		BgL_bgl_string2759za700za7za7_2968za7, "_string-prefix?", 15);
	      DEFINE_STRING(BGl_string2678z00zz__r4_strings_6_7z00,
		BgL_bgl_string2678za700za7za7_2969za7, "&string<=?", 10);
	      DEFINE_STRING(BGl_string2679z00zz__r4_strings_6_7z00,
		BgL_bgl_string2679za700za7za7_2970za7, "&string>=?", 10);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2downcasez12zd2envz12zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7d2down2971z00,
		BGl_z62stringzd2downcasez12za2zz__r4_strings_6_7z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_substringzd3zf3zd2envzf2zz__r4_strings_6_7z00,
		BgL_bgl_za762substringza7d3za72972za7,
		BGl_z62substringzd3zf3z42zz__r4_strings_6_7z00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2replacezd2envz00zz__r4_strings_6_7z00,
		BgL_bgl_za762stringza7d2repl2973z00,
		BGl_z62stringzd2replacezb0zz__r4_strings_6_7z00, 0L, BUNSPEC, 3);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_symbol2739z00zz__r4_strings_6_7z00));
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__r4_strings_6_7z00));
		     ADD_ROOT((void *) (&BGl_symbol2751z00zz__r4_strings_6_7z00));
		     ADD_ROOT((void *) (&BGl_symbol2754z00zz__r4_strings_6_7z00));
		     ADD_ROOT((void *) (&BGl_symbol2757z00zz__r4_strings_6_7z00));
		     ADD_ROOT((void *) (&BGl_symbol2760z00zz__r4_strings_6_7z00));
		     ADD_ROOT((void *) (&BGl_symbol2763z00zz__r4_strings_6_7z00));
		     ADD_ROOT((void *) (&BGl_symbol2766z00zz__r4_strings_6_7z00));
		     ADD_ROOT((void *) (&BGl_symbol2771z00zz__r4_strings_6_7z00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long
		BgL_checksumz00_4998, char *BgL_fromz00_4999)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__r4_strings_6_7z00))
				{
					BGl_requirezd2initializa7ationz75zz__r4_strings_6_7z00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__r4_strings_6_7z00();
					BGl_cnstzd2initzd2zz__r4_strings_6_7z00();
					return BGl_importedzd2moduleszd2initz00zz__r4_strings_6_7z00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__r4_strings_6_7z00(void)
	{
		{	/* Ieee/string.scm 18 */
			BGl_symbol2739z00zz__r4_strings_6_7z00 =
				bstring_to_symbol(BGl_string2740z00zz__r4_strings_6_7z00);
			BGl_symbol2751z00zz__r4_strings_6_7z00 =
				bstring_to_symbol(BGl_string2752z00zz__r4_strings_6_7z00);
			BGl_symbol2754z00zz__r4_strings_6_7z00 =
				bstring_to_symbol(BGl_string2755z00zz__r4_strings_6_7z00);
			BGl_symbol2757z00zz__r4_strings_6_7z00 =
				bstring_to_symbol(BGl_string2758z00zz__r4_strings_6_7z00);
			BGl_symbol2760z00zz__r4_strings_6_7z00 =
				bstring_to_symbol(BGl_string2761z00zz__r4_strings_6_7z00);
			BGl_symbol2763z00zz__r4_strings_6_7z00 =
				bstring_to_symbol(BGl_string2764z00zz__r4_strings_6_7z00);
			BGl_symbol2766z00zz__r4_strings_6_7z00 =
				bstring_to_symbol(BGl_string2767z00zz__r4_strings_6_7z00);
			return (BGl_symbol2771z00zz__r4_strings_6_7z00 =
				bstring_to_symbol(BGl_string2772z00zz__r4_strings_6_7z00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__r4_strings_6_7z00(void)
	{
		{	/* Ieee/string.scm 18 */
			return bgl_gc_roots_register();
		}

	}



/* string? */
	BGL_EXPORTED_DEF bool_t BGl_stringzf3zf3zz__r4_strings_6_7z00(obj_t
		BgL_objz00_3)
	{
		{	/* Ieee/string.scm 300 */
			return STRINGP(BgL_objz00_3);
		}

	}



/* &string? */
	obj_t BGl_z62stringzf3z91zz__r4_strings_6_7z00(obj_t BgL_envz00_4348,
		obj_t BgL_objz00_4349)
	{
		{	/* Ieee/string.scm 300 */
			return BBOOL(BGl_stringzf3zf3zz__r4_strings_6_7z00(BgL_objz00_4349));
		}

	}



/* string-null? */
	BGL_EXPORTED_DEF bool_t BGl_stringzd2nullzf3z21zz__r4_strings_6_7z00(obj_t
		BgL_strz00_4)
	{
		{	/* Ieee/string.scm 306 */
			return (STRING_LENGTH(BgL_strz00_4) == 0L);
		}

	}



/* &string-null? */
	obj_t BGl_z62stringzd2nullzf3z43zz__r4_strings_6_7z00(obj_t BgL_envz00_4350,
		obj_t BgL_strz00_4351)
	{
		{	/* Ieee/string.scm 306 */
			{	/* Ieee/string.scm 307 */
				bool_t BgL_tmpz00_5020;

				{	/* Ieee/string.scm 307 */
					obj_t BgL_auxz00_5021;

					if (STRINGP(BgL_strz00_4351))
						{	/* Ieee/string.scm 307 */
							BgL_auxz00_5021 = BgL_strz00_4351;
						}
					else
						{
							obj_t BgL_auxz00_5024;

							BgL_auxz00_5024 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(15062L),
								BGl_string2659z00zz__r4_strings_6_7z00,
								BGl_string2660z00zz__r4_strings_6_7z00, BgL_strz00_4351);
							FAILURE(BgL_auxz00_5024, BFALSE, BFALSE);
						}
					BgL_tmpz00_5020 =
						BGl_stringzd2nullzf3z21zz__r4_strings_6_7z00(BgL_auxz00_5021);
				}
				return BBOOL(BgL_tmpz00_5020);
			}
		}

	}



/* _make-string */
	obj_t BGl__makezd2stringzd2zz__r4_strings_6_7z00(obj_t BgL_env1093z00_8,
		obj_t BgL_opt1092z00_7)
	{
		{	/* Ieee/string.scm 312 */
			{	/* Ieee/string.scm 312 */
				obj_t BgL_g1094z00_4968;

				BgL_g1094z00_4968 = VECTOR_REF(BgL_opt1092z00_7, 0L);
				switch (VECTOR_LENGTH(BgL_opt1092z00_7))
					{
					case 1L:

						{	/* Ieee/string.scm 312 */

							{	/* Ieee/string.scm 312 */
								long BgL_kz00_4969;

								{	/* Ieee/string.scm 312 */
									obj_t BgL_tmpz00_5031;

									if (INTEGERP(BgL_g1094z00_4968))
										{	/* Ieee/string.scm 312 */
											BgL_tmpz00_5031 = BgL_g1094z00_4968;
										}
									else
										{
											obj_t BgL_auxz00_5034;

											BgL_auxz00_5034 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(15309L),
												BGl_string2661z00zz__r4_strings_6_7z00,
												BGl_string2662z00zz__r4_strings_6_7z00,
												BgL_g1094z00_4968);
											FAILURE(BgL_auxz00_5034, BFALSE, BFALSE);
										}
									BgL_kz00_4969 = (long) CINT(BgL_tmpz00_5031);
								}
								return make_string(BgL_kz00_4969, ((unsigned char) ' '));
						}} break;
					case 2L:

						{	/* Ieee/string.scm 312 */
							obj_t BgL_charz00_4970;

							BgL_charz00_4970 = VECTOR_REF(BgL_opt1092z00_7, 1L);
							{	/* Ieee/string.scm 312 */

								{	/* Ieee/string.scm 312 */
									long BgL_kz00_4971;

									{	/* Ieee/string.scm 312 */
										obj_t BgL_tmpz00_5041;

										if (INTEGERP(BgL_g1094z00_4968))
											{	/* Ieee/string.scm 312 */
												BgL_tmpz00_5041 = BgL_g1094z00_4968;
											}
										else
											{
												obj_t BgL_auxz00_5044;

												BgL_auxz00_5044 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2658z00zz__r4_strings_6_7z00, BINT(15309L),
													BGl_string2661z00zz__r4_strings_6_7z00,
													BGl_string2662z00zz__r4_strings_6_7z00,
													BgL_g1094z00_4968);
												FAILURE(BgL_auxz00_5044, BFALSE, BFALSE);
											}
										BgL_kz00_4971 = (long) CINT(BgL_tmpz00_5041);
									}
									{	/* Ieee/string.scm 313 */
										unsigned char BgL_tmpz00_5049;

										{	/* Ieee/string.scm 313 */
											obj_t BgL_tmpz00_5050;

											if (CHARP(BgL_charz00_4970))
												{	/* Ieee/string.scm 313 */
													BgL_tmpz00_5050 = BgL_charz00_4970;
												}
											else
												{
													obj_t BgL_auxz00_5053;

													BgL_auxz00_5053 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2658z00zz__r4_strings_6_7z00,
														BINT(15385L),
														BGl_string2661z00zz__r4_strings_6_7z00,
														BGl_string2663z00zz__r4_strings_6_7z00,
														BgL_charz00_4970);
													FAILURE(BgL_auxz00_5053, BFALSE, BFALSE);
												}
											BgL_tmpz00_5049 = CCHAR(BgL_tmpz00_5050);
										}
										return make_string(BgL_kz00_4971, BgL_tmpz00_5049);
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* make-string */
	BGL_EXPORTED_DEF obj_t BGl_makezd2stringzd2zz__r4_strings_6_7z00(long
		BgL_kz00_5, obj_t BgL_charz00_6)
	{
		{	/* Ieee/string.scm 312 */
			return make_string(BgL_kz00_5, CCHAR(BgL_charz00_6));
		}

	}



/* string */
	BGL_EXPORTED_DEF obj_t BGl_stringz00zz__r4_strings_6_7z00(obj_t
		BgL_charsz00_9)
	{
		{	/* Ieee/string.scm 318 */
			return BGl_listzd2ze3stringz31zz__r4_strings_6_7z00(BgL_charsz00_9);
		}

	}



/* &string */
	obj_t BGl_z62stringz62zz__r4_strings_6_7z00(obj_t BgL_envz00_4352,
		obj_t BgL_charsz00_4353)
	{
		{	/* Ieee/string.scm 318 */
			return BGl_stringz00zz__r4_strings_6_7z00(BgL_charsz00_4353);
		}

	}



/* string-length */
	BGL_EXPORTED_DEF long BGl_stringzd2lengthzd2zz__r4_strings_6_7z00(obj_t
		BgL_stringz00_10)
	{
		{	/* Ieee/string.scm 324 */
			return STRING_LENGTH(BgL_stringz00_10);
		}

	}



/* &string-length */
	obj_t BGl_z62stringzd2lengthzb0zz__r4_strings_6_7z00(obj_t BgL_envz00_4354,
		obj_t BgL_stringz00_4355)
	{
		{	/* Ieee/string.scm 324 */
			{	/* Ieee/string.scm 325 */
				long BgL_tmpz00_5066;

				{	/* Ieee/string.scm 325 */
					obj_t BgL_auxz00_5067;

					if (STRINGP(BgL_stringz00_4355))
						{	/* Ieee/string.scm 325 */
							BgL_auxz00_5067 = BgL_stringz00_4355;
						}
					else
						{
							obj_t BgL_auxz00_5070;

							BgL_auxz00_5070 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(15937L),
								BGl_string2664z00zz__r4_strings_6_7z00,
								BGl_string2660z00zz__r4_strings_6_7z00, BgL_stringz00_4355);
							FAILURE(BgL_auxz00_5070, BFALSE, BFALSE);
						}
					BgL_tmpz00_5066 =
						BGl_stringzd2lengthzd2zz__r4_strings_6_7z00(BgL_auxz00_5067);
				}
				return BINT(BgL_tmpz00_5066);
			}
		}

	}



/* string-ref */
	BGL_EXPORTED_DEF unsigned char BGl_stringzd2refzd2zz__r4_strings_6_7z00(obj_t
		BgL_stringz00_11, long BgL_kz00_12)
	{
		{	/* Ieee/string.scm 330 */
			return STRING_REF(BgL_stringz00_11, BgL_kz00_12);
		}

	}



/* &string-ref */
	obj_t BGl_z62stringzd2refzb0zz__r4_strings_6_7z00(obj_t BgL_envz00_4356,
		obj_t BgL_stringz00_4357, obj_t BgL_kz00_4358)
	{
		{	/* Ieee/string.scm 330 */
			{	/* Ieee/string.scm 331 */
				unsigned char BgL_tmpz00_5077;

				{	/* Ieee/string.scm 331 */
					long BgL_auxz00_5085;
					obj_t BgL_auxz00_5078;

					{	/* Ieee/string.scm 331 */
						obj_t BgL_tmpz00_5086;

						if (INTEGERP(BgL_kz00_4358))
							{	/* Ieee/string.scm 331 */
								BgL_tmpz00_5086 = BgL_kz00_4358;
							}
						else
							{
								obj_t BgL_auxz00_5089;

								BgL_auxz00_5089 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string2658z00zz__r4_strings_6_7z00, BINT(16225L),
									BGl_string2665z00zz__r4_strings_6_7z00,
									BGl_string2662z00zz__r4_strings_6_7z00, BgL_kz00_4358);
								FAILURE(BgL_auxz00_5089, BFALSE, BFALSE);
							}
						BgL_auxz00_5085 = (long) CINT(BgL_tmpz00_5086);
					}
					if (STRINGP(BgL_stringz00_4357))
						{	/* Ieee/string.scm 331 */
							BgL_auxz00_5078 = BgL_stringz00_4357;
						}
					else
						{
							obj_t BgL_auxz00_5081;

							BgL_auxz00_5081 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(16225L),
								BGl_string2665z00zz__r4_strings_6_7z00,
								BGl_string2660z00zz__r4_strings_6_7z00, BgL_stringz00_4357);
							FAILURE(BgL_auxz00_5081, BFALSE, BFALSE);
						}
					BgL_tmpz00_5077 =
						BGl_stringzd2refzd2zz__r4_strings_6_7z00(BgL_auxz00_5078,
						BgL_auxz00_5085);
				}
				return BCHAR(BgL_tmpz00_5077);
			}
		}

	}



/* string-set! */
	BGL_EXPORTED_DEF obj_t BGl_stringzd2setz12zc0zz__r4_strings_6_7z00(obj_t
		BgL_stringz00_13, long BgL_kz00_14, unsigned char BgL_charz00_15)
	{
		{	/* Ieee/string.scm 336 */
			return STRING_SET(BgL_stringz00_13, BgL_kz00_14, BgL_charz00_15);
		}

	}



/* &string-set! */
	obj_t BGl_z62stringzd2setz12za2zz__r4_strings_6_7z00(obj_t BgL_envz00_4359,
		obj_t BgL_stringz00_4360, obj_t BgL_kz00_4361, obj_t BgL_charz00_4362)
	{
		{	/* Ieee/string.scm 336 */
			{	/* Ieee/string.scm 337 */
				unsigned char BgL_auxz00_5113;
				long BgL_auxz00_5104;
				obj_t BgL_auxz00_5097;

				{	/* Ieee/string.scm 337 */
					obj_t BgL_tmpz00_5114;

					if (CHARP(BgL_charz00_4362))
						{	/* Ieee/string.scm 337 */
							BgL_tmpz00_5114 = BgL_charz00_4362;
						}
					else
						{
							obj_t BgL_auxz00_5117;

							BgL_auxz00_5117 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(16519L),
								BGl_string2666z00zz__r4_strings_6_7z00,
								BGl_string2663z00zz__r4_strings_6_7z00, BgL_charz00_4362);
							FAILURE(BgL_auxz00_5117, BFALSE, BFALSE);
						}
					BgL_auxz00_5113 = CCHAR(BgL_tmpz00_5114);
				}
				{	/* Ieee/string.scm 337 */
					obj_t BgL_tmpz00_5105;

					if (INTEGERP(BgL_kz00_4361))
						{	/* Ieee/string.scm 337 */
							BgL_tmpz00_5105 = BgL_kz00_4361;
						}
					else
						{
							obj_t BgL_auxz00_5108;

							BgL_auxz00_5108 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(16519L),
								BGl_string2666z00zz__r4_strings_6_7z00,
								BGl_string2662z00zz__r4_strings_6_7z00, BgL_kz00_4361);
							FAILURE(BgL_auxz00_5108, BFALSE, BFALSE);
						}
					BgL_auxz00_5104 = (long) CINT(BgL_tmpz00_5105);
				}
				if (STRINGP(BgL_stringz00_4360))
					{	/* Ieee/string.scm 337 */
						BgL_auxz00_5097 = BgL_stringz00_4360;
					}
				else
					{
						obj_t BgL_auxz00_5100;

						BgL_auxz00_5100 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2658z00zz__r4_strings_6_7z00, BINT(16519L),
							BGl_string2666z00zz__r4_strings_6_7z00,
							BGl_string2660z00zz__r4_strings_6_7z00, BgL_stringz00_4360);
						FAILURE(BgL_auxz00_5100, BFALSE, BFALSE);
					}
				return
					BGl_stringzd2setz12zc0zz__r4_strings_6_7z00(BgL_auxz00_5097,
					BgL_auxz00_5104, BgL_auxz00_5113);
			}
		}

	}



/* string-ref-ur */
	BGL_EXPORTED_DEF unsigned char
		BGl_stringzd2refzd2urz00zz__r4_strings_6_7z00(obj_t BgL_stringz00_16,
		long BgL_kz00_17)
	{
		{	/* Ieee/string.scm 342 */
			return STRING_REF(BgL_stringz00_16, BgL_kz00_17);
		}

	}



/* &string-ref-ur */
	obj_t BGl_z62stringzd2refzd2urz62zz__r4_strings_6_7z00(obj_t BgL_envz00_4363,
		obj_t BgL_stringz00_4364, obj_t BgL_kz00_4365)
	{
		{	/* Ieee/string.scm 342 */
			{	/* Ieee/string.scm 343 */
				unsigned char BgL_tmpz00_5124;

				{	/* Ieee/string.scm 343 */
					long BgL_auxz00_5132;
					obj_t BgL_auxz00_5125;

					{	/* Ieee/string.scm 343 */
						obj_t BgL_tmpz00_5133;

						if (INTEGERP(BgL_kz00_4365))
							{	/* Ieee/string.scm 343 */
								BgL_tmpz00_5133 = BgL_kz00_4365;
							}
						else
							{
								obj_t BgL_auxz00_5136;

								BgL_auxz00_5136 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string2658z00zz__r4_strings_6_7z00, BINT(16815L),
									BGl_string2667z00zz__r4_strings_6_7z00,
									BGl_string2662z00zz__r4_strings_6_7z00, BgL_kz00_4365);
								FAILURE(BgL_auxz00_5136, BFALSE, BFALSE);
							}
						BgL_auxz00_5132 = (long) CINT(BgL_tmpz00_5133);
					}
					if (STRINGP(BgL_stringz00_4364))
						{	/* Ieee/string.scm 343 */
							BgL_auxz00_5125 = BgL_stringz00_4364;
						}
					else
						{
							obj_t BgL_auxz00_5128;

							BgL_auxz00_5128 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(16815L),
								BGl_string2667z00zz__r4_strings_6_7z00,
								BGl_string2660z00zz__r4_strings_6_7z00, BgL_stringz00_4364);
							FAILURE(BgL_auxz00_5128, BFALSE, BFALSE);
						}
					BgL_tmpz00_5124 =
						BGl_stringzd2refzd2urz00zz__r4_strings_6_7z00(BgL_auxz00_5125,
						BgL_auxz00_5132);
				}
				return BCHAR(BgL_tmpz00_5124);
			}
		}

	}



/* string-set-ur! */
	BGL_EXPORTED_DEF obj_t BGl_stringzd2setzd2urz12z12zz__r4_strings_6_7z00(obj_t
		BgL_stringz00_18, long BgL_kz00_19, unsigned char BgL_charz00_20)
	{
		{	/* Ieee/string.scm 348 */
			return STRING_SET(BgL_stringz00_18, BgL_kz00_19, BgL_charz00_20);
		}

	}



/* &string-set-ur! */
	obj_t BGl_z62stringzd2setzd2urz12z70zz__r4_strings_6_7z00(obj_t
		BgL_envz00_4366, obj_t BgL_stringz00_4367, obj_t BgL_kz00_4368,
		obj_t BgL_charz00_4369)
	{
		{	/* Ieee/string.scm 348 */
			{	/* Ieee/string.scm 349 */
				unsigned char BgL_auxz00_5160;
				long BgL_auxz00_5151;
				obj_t BgL_auxz00_5144;

				{	/* Ieee/string.scm 349 */
					obj_t BgL_tmpz00_5161;

					if (CHARP(BgL_charz00_4369))
						{	/* Ieee/string.scm 349 */
							BgL_tmpz00_5161 = BgL_charz00_4369;
						}
					else
						{
							obj_t BgL_auxz00_5164;

							BgL_auxz00_5164 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(17112L),
								BGl_string2668z00zz__r4_strings_6_7z00,
								BGl_string2663z00zz__r4_strings_6_7z00, BgL_charz00_4369);
							FAILURE(BgL_auxz00_5164, BFALSE, BFALSE);
						}
					BgL_auxz00_5160 = CCHAR(BgL_tmpz00_5161);
				}
				{	/* Ieee/string.scm 349 */
					obj_t BgL_tmpz00_5152;

					if (INTEGERP(BgL_kz00_4368))
						{	/* Ieee/string.scm 349 */
							BgL_tmpz00_5152 = BgL_kz00_4368;
						}
					else
						{
							obj_t BgL_auxz00_5155;

							BgL_auxz00_5155 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(17112L),
								BGl_string2668z00zz__r4_strings_6_7z00,
								BGl_string2662z00zz__r4_strings_6_7z00, BgL_kz00_4368);
							FAILURE(BgL_auxz00_5155, BFALSE, BFALSE);
						}
					BgL_auxz00_5151 = (long) CINT(BgL_tmpz00_5152);
				}
				if (STRINGP(BgL_stringz00_4367))
					{	/* Ieee/string.scm 349 */
						BgL_auxz00_5144 = BgL_stringz00_4367;
					}
				else
					{
						obj_t BgL_auxz00_5147;

						BgL_auxz00_5147 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2658z00zz__r4_strings_6_7z00, BINT(17112L),
							BGl_string2668z00zz__r4_strings_6_7z00,
							BGl_string2660z00zz__r4_strings_6_7z00, BgL_stringz00_4367);
						FAILURE(BgL_auxz00_5147, BFALSE, BFALSE);
					}
				return
					BGl_stringzd2setzd2urz12z12zz__r4_strings_6_7z00(BgL_auxz00_5144,
					BgL_auxz00_5151, BgL_auxz00_5160);
			}
		}

	}



/* string=? */
	BGL_EXPORTED_DEF bool_t BGl_stringzd3zf3z20zz__r4_strings_6_7z00(obj_t
		BgL_string1z00_21, obj_t BgL_string2z00_22)
	{
		{	/* Ieee/string.scm 354 */
			{	/* Ieee/string.scm 357 */
				long BgL_l1z00_4972;

				BgL_l1z00_4972 = STRING_LENGTH(BgL_string1z00_21);
				if ((BgL_l1z00_4972 == STRING_LENGTH(BgL_string2z00_22)))
					{	/* Ieee/string.scm 359 */
						int BgL_arg1221z00_4973;

						{	/* Ieee/string.scm 359 */
							char *BgL_auxz00_5176;
							char *BgL_tmpz00_5174;

							BgL_auxz00_5176 = BSTRING_TO_STRING(BgL_string2z00_22);
							BgL_tmpz00_5174 = BSTRING_TO_STRING(BgL_string1z00_21);
							BgL_arg1221z00_4973 =
								memcmp(BgL_tmpz00_5174, BgL_auxz00_5176, BgL_l1z00_4972);
						}
						return ((long) (BgL_arg1221z00_4973) == 0L);
					}
				else
					{	/* Ieee/string.scm 358 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &string=? */
	obj_t BGl_z62stringzd3zf3z42zz__r4_strings_6_7z00(obj_t BgL_envz00_4370,
		obj_t BgL_string1z00_4371, obj_t BgL_string2z00_4372)
	{
		{	/* Ieee/string.scm 354 */
			{	/* Ieee/string.scm 357 */
				bool_t BgL_tmpz00_5181;

				{	/* Ieee/string.scm 357 */
					obj_t BgL_auxz00_5189;
					obj_t BgL_auxz00_5182;

					if (STRINGP(BgL_string2z00_4372))
						{	/* Ieee/string.scm 357 */
							BgL_auxz00_5189 = BgL_string2z00_4372;
						}
					else
						{
							obj_t BgL_auxz00_5192;

							BgL_auxz00_5192 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(17446L),
								BGl_string2669z00zz__r4_strings_6_7z00,
								BGl_string2660z00zz__r4_strings_6_7z00, BgL_string2z00_4372);
							FAILURE(BgL_auxz00_5192, BFALSE, BFALSE);
						}
					if (STRINGP(BgL_string1z00_4371))
						{	/* Ieee/string.scm 357 */
							BgL_auxz00_5182 = BgL_string1z00_4371;
						}
					else
						{
							obj_t BgL_auxz00_5185;

							BgL_auxz00_5185 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(17446L),
								BGl_string2669z00zz__r4_strings_6_7z00,
								BGl_string2660z00zz__r4_strings_6_7z00, BgL_string1z00_4371);
							FAILURE(BgL_auxz00_5185, BFALSE, BFALSE);
						}
					BgL_tmpz00_5181 =
						BGl_stringzd3zf3z20zz__r4_strings_6_7z00(BgL_auxz00_5182,
						BgL_auxz00_5189);
				}
				return BBOOL(BgL_tmpz00_5181);
			}
		}

	}



/* substring=? */
	BGL_EXPORTED_DEF bool_t BGl_substringzd3zf3z20zz__r4_strings_6_7z00(obj_t
		BgL_string1z00_23, obj_t BgL_string2z00_24, long BgL_lenz00_25)
	{
		{	/* Ieee/string.scm 368 */
			BGL_TAIL return
				bigloo_strncmp(BgL_string1z00_23, BgL_string2z00_24, BgL_lenz00_25);
		}

	}



/* &substring=? */
	obj_t BGl_z62substringzd3zf3z42zz__r4_strings_6_7z00(obj_t BgL_envz00_4373,
		obj_t BgL_string1z00_4374, obj_t BgL_string2z00_4375, obj_t BgL_lenz00_4376)
	{
		{	/* Ieee/string.scm 368 */
			{	/* Ieee/string.scm 369 */
				bool_t BgL_tmpz00_5199;

				{	/* Ieee/string.scm 369 */
					long BgL_auxz00_5214;
					obj_t BgL_auxz00_5207;
					obj_t BgL_auxz00_5200;

					{	/* Ieee/string.scm 369 */
						obj_t BgL_tmpz00_5215;

						if (INTEGERP(BgL_lenz00_4376))
							{	/* Ieee/string.scm 369 */
								BgL_tmpz00_5215 = BgL_lenz00_4376;
							}
						else
							{
								obj_t BgL_auxz00_5218;

								BgL_auxz00_5218 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string2658z00zz__r4_strings_6_7z00, BINT(17949L),
									BGl_string2670z00zz__r4_strings_6_7z00,
									BGl_string2662z00zz__r4_strings_6_7z00, BgL_lenz00_4376);
								FAILURE(BgL_auxz00_5218, BFALSE, BFALSE);
							}
						BgL_auxz00_5214 = (long) CINT(BgL_tmpz00_5215);
					}
					if (STRINGP(BgL_string2z00_4375))
						{	/* Ieee/string.scm 369 */
							BgL_auxz00_5207 = BgL_string2z00_4375;
						}
					else
						{
							obj_t BgL_auxz00_5210;

							BgL_auxz00_5210 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(17949L),
								BGl_string2670z00zz__r4_strings_6_7z00,
								BGl_string2660z00zz__r4_strings_6_7z00, BgL_string2z00_4375);
							FAILURE(BgL_auxz00_5210, BFALSE, BFALSE);
						}
					if (STRINGP(BgL_string1z00_4374))
						{	/* Ieee/string.scm 369 */
							BgL_auxz00_5200 = BgL_string1z00_4374;
						}
					else
						{
							obj_t BgL_auxz00_5203;

							BgL_auxz00_5203 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(17949L),
								BGl_string2670z00zz__r4_strings_6_7z00,
								BGl_string2660z00zz__r4_strings_6_7z00, BgL_string1z00_4374);
							FAILURE(BgL_auxz00_5203, BFALSE, BFALSE);
						}
					BgL_tmpz00_5199 =
						BGl_substringzd3zf3z20zz__r4_strings_6_7z00(BgL_auxz00_5200,
						BgL_auxz00_5207, BgL_auxz00_5214);
				}
				return BBOOL(BgL_tmpz00_5199);
			}
		}

	}



/* _substring-at? */
	obj_t BGl__substringzd2atzf3z21zz__r4_strings_6_7z00(obj_t BgL_env1098z00_31,
		obj_t BgL_opt1097z00_30)
	{
		{	/* Ieee/string.scm 374 */
			{	/* Ieee/string.scm 374 */
				obj_t BgL_g1099z00_4974;
				obj_t BgL_g1100z00_4975;
				obj_t BgL_g1101z00_4976;

				BgL_g1099z00_4974 = VECTOR_REF(BgL_opt1097z00_30, 0L);
				BgL_g1100z00_4975 = VECTOR_REF(BgL_opt1097z00_30, 1L);
				BgL_g1101z00_4976 = VECTOR_REF(BgL_opt1097z00_30, 2L);
				switch (VECTOR_LENGTH(BgL_opt1097z00_30))
					{
					case 3L:

						{	/* Ieee/string.scm 374 */

							{	/* Ieee/string.scm 374 */
								obj_t BgL_string1z00_4977;
								obj_t BgL_string2z00_4978;
								long BgL_offz00_4979;

								if (STRINGP(BgL_g1099z00_4974))
									{	/* Ieee/string.scm 374 */
										BgL_string1z00_4977 = BgL_g1099z00_4974;
									}
								else
									{
										obj_t BgL_auxz00_5230;

										BgL_auxz00_5230 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2658z00zz__r4_strings_6_7z00, BINT(18208L),
											BGl_string2671z00zz__r4_strings_6_7z00,
											BGl_string2660z00zz__r4_strings_6_7z00,
											BgL_g1099z00_4974);
										FAILURE(BgL_auxz00_5230, BFALSE, BFALSE);
									}
								if (STRINGP(BgL_g1100z00_4975))
									{	/* Ieee/string.scm 374 */
										BgL_string2z00_4978 = BgL_g1100z00_4975;
									}
								else
									{
										obj_t BgL_auxz00_5236;

										BgL_auxz00_5236 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2658z00zz__r4_strings_6_7z00, BINT(18208L),
											BGl_string2671z00zz__r4_strings_6_7z00,
											BGl_string2660z00zz__r4_strings_6_7z00,
											BgL_g1100z00_4975);
										FAILURE(BgL_auxz00_5236, BFALSE, BFALSE);
									}
								{	/* Ieee/string.scm 374 */
									obj_t BgL_tmpz00_5240;

									if (INTEGERP(BgL_g1101z00_4976))
										{	/* Ieee/string.scm 374 */
											BgL_tmpz00_5240 = BgL_g1101z00_4976;
										}
									else
										{
											obj_t BgL_auxz00_5243;

											BgL_auxz00_5243 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(18208L),
												BGl_string2671z00zz__r4_strings_6_7z00,
												BGl_string2662z00zz__r4_strings_6_7z00,
												BgL_g1101z00_4976);
											FAILURE(BgL_auxz00_5243, BFALSE, BFALSE);
										}
									BgL_offz00_4979 = (long) CINT(BgL_tmpz00_5240);
								}
								return
									BBOOL(bigloo_strcmp_at(BgL_string1z00_4977,
										BgL_string2z00_4978, BgL_offz00_4979));
							}
						}
						break;
					case 4L:

						{	/* Ieee/string.scm 374 */
							obj_t BgL_lenz00_4980;

							BgL_lenz00_4980 = VECTOR_REF(BgL_opt1097z00_30, 3L);
							{	/* Ieee/string.scm 374 */

								{	/* Ieee/string.scm 374 */
									obj_t BgL_string1z00_4981;
									obj_t BgL_string2z00_4982;
									long BgL_offz00_4983;

									if (STRINGP(BgL_g1099z00_4974))
										{	/* Ieee/string.scm 374 */
											BgL_string1z00_4981 = BgL_g1099z00_4974;
										}
									else
										{
											obj_t BgL_auxz00_5253;

											BgL_auxz00_5253 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(18208L),
												BGl_string2671z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00,
												BgL_g1099z00_4974);
											FAILURE(BgL_auxz00_5253, BFALSE, BFALSE);
										}
									if (STRINGP(BgL_g1100z00_4975))
										{	/* Ieee/string.scm 374 */
											BgL_string2z00_4982 = BgL_g1100z00_4975;
										}
									else
										{
											obj_t BgL_auxz00_5259;

											BgL_auxz00_5259 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(18208L),
												BGl_string2671z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00,
												BgL_g1100z00_4975);
											FAILURE(BgL_auxz00_5259, BFALSE, BFALSE);
										}
									{	/* Ieee/string.scm 374 */
										obj_t BgL_tmpz00_5263;

										if (INTEGERP(BgL_g1101z00_4976))
											{	/* Ieee/string.scm 374 */
												BgL_tmpz00_5263 = BgL_g1101z00_4976;
											}
										else
											{
												obj_t BgL_auxz00_5266;

												BgL_auxz00_5266 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2658z00zz__r4_strings_6_7z00, BINT(18208L),
													BGl_string2671z00zz__r4_strings_6_7z00,
													BGl_string2662z00zz__r4_strings_6_7z00,
													BgL_g1101z00_4976);
												FAILURE(BgL_auxz00_5266, BFALSE, BFALSE);
											}
										BgL_offz00_4983 = (long) CINT(BgL_tmpz00_5263);
									}
									{	/* Ieee/string.scm 375 */
										bool_t BgL_test3002z00_5271;

										{	/* Ieee/string.scm 375 */
											long BgL_n1z00_4984;

											{	/* Ieee/string.scm 375 */
												obj_t BgL_tmpz00_5272;

												if (INTEGERP(BgL_lenz00_4980))
													{	/* Ieee/string.scm 375 */
														BgL_tmpz00_5272 = BgL_lenz00_4980;
													}
												else
													{
														obj_t BgL_auxz00_5275;

														BgL_auxz00_5275 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2658z00zz__r4_strings_6_7z00,
															BINT(18291L),
															BGl_string2671z00zz__r4_strings_6_7z00,
															BGl_string2662z00zz__r4_strings_6_7z00,
															BgL_lenz00_4980);
														FAILURE(BgL_auxz00_5275, BFALSE, BFALSE);
													}
												BgL_n1z00_4984 = (long) CINT(BgL_tmpz00_5272);
											}
											BgL_test3002z00_5271 = (BgL_n1z00_4984 == -1L);
										}
										if (BgL_test3002z00_5271)
											{	/* Ieee/string.scm 375 */
												return
													BBOOL(bigloo_strcmp_at(BgL_string1z00_4981,
														BgL_string2z00_4982, BgL_offz00_4983));
											}
										else
											{	/* Ieee/string.scm 377 */
												bool_t BgL_tmpz00_5283;

												{	/* Ieee/string.scm 377 */
													long BgL_tmpz00_5284;

													{	/* Ieee/string.scm 377 */
														obj_t BgL_tmpz00_5285;

														if (INTEGERP(BgL_lenz00_4980))
															{	/* Ieee/string.scm 377 */
																BgL_tmpz00_5285 = BgL_lenz00_4980;
															}
														else
															{
																obj_t BgL_auxz00_5288;

																BgL_auxz00_5288 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string2658z00zz__r4_strings_6_7z00,
																	BINT(18383L),
																	BGl_string2671z00zz__r4_strings_6_7z00,
																	BGl_string2662z00zz__r4_strings_6_7z00,
																	BgL_lenz00_4980);
																FAILURE(BgL_auxz00_5288, BFALSE, BFALSE);
															}
														BgL_tmpz00_5284 = (long) CINT(BgL_tmpz00_5285);
													}
													BgL_tmpz00_5283 =
														bigloo_strncmp_at(BgL_string1z00_4981,
														BgL_string2z00_4982, BgL_offz00_4983,
														BgL_tmpz00_5284);
												}
												return BBOOL(BgL_tmpz00_5283);
											}
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* substring-at? */
	BGL_EXPORTED_DEF bool_t BGl_substringzd2atzf3z21zz__r4_strings_6_7z00(obj_t
		BgL_string1z00_26, obj_t BgL_string2z00_27, long BgL_offz00_28,
		obj_t BgL_lenz00_29)
	{
		{	/* Ieee/string.scm 374 */
			if (((long) CINT(BgL_lenz00_29) == -1L))
				{	/* Ieee/string.scm 375 */
					return
						bigloo_strcmp_at(BgL_string1z00_26, BgL_string2z00_27,
						BgL_offz00_28);
				}
			else
				{	/* Ieee/string.scm 375 */
					return
						bigloo_strncmp_at(BgL_string1z00_26, BgL_string2z00_27,
						BgL_offz00_28, (long) CINT(BgL_lenz00_29));
		}}

	}



/* substring-ci=? */
	BGL_EXPORTED_DEF bool_t BGl_substringzd2cizd3zf3zf2zz__r4_strings_6_7z00(obj_t
		BgL_string1z00_32, obj_t BgL_string2z00_33, long BgL_lenz00_34)
	{
		{	/* Ieee/string.scm 382 */
			BGL_TAIL return
				bigloo_strncmp_ci(BgL_string1z00_32, BgL_string2z00_33, BgL_lenz00_34);
		}

	}



/* &substring-ci=? */
	obj_t BGl_z62substringzd2cizd3zf3z90zz__r4_strings_6_7z00(obj_t
		BgL_envz00_4377, obj_t BgL_string1z00_4378, obj_t BgL_string2z00_4379,
		obj_t BgL_lenz00_4380)
	{
		{	/* Ieee/string.scm 382 */
			{	/* Ieee/string.scm 383 */
				bool_t BgL_tmpz00_5304;

				{	/* Ieee/string.scm 383 */
					long BgL_auxz00_5319;
					obj_t BgL_auxz00_5312;
					obj_t BgL_auxz00_5305;

					{	/* Ieee/string.scm 383 */
						obj_t BgL_tmpz00_5320;

						if (INTEGERP(BgL_lenz00_4380))
							{	/* Ieee/string.scm 383 */
								BgL_tmpz00_5320 = BgL_lenz00_4380;
							}
						else
							{
								obj_t BgL_auxz00_5323;

								BgL_auxz00_5323 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string2658z00zz__r4_strings_6_7z00, BINT(18668L),
									BGl_string2672z00zz__r4_strings_6_7z00,
									BGl_string2662z00zz__r4_strings_6_7z00, BgL_lenz00_4380);
								FAILURE(BgL_auxz00_5323, BFALSE, BFALSE);
							}
						BgL_auxz00_5319 = (long) CINT(BgL_tmpz00_5320);
					}
					if (STRINGP(BgL_string2z00_4379))
						{	/* Ieee/string.scm 383 */
							BgL_auxz00_5312 = BgL_string2z00_4379;
						}
					else
						{
							obj_t BgL_auxz00_5315;

							BgL_auxz00_5315 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(18668L),
								BGl_string2672z00zz__r4_strings_6_7z00,
								BGl_string2660z00zz__r4_strings_6_7z00, BgL_string2z00_4379);
							FAILURE(BgL_auxz00_5315, BFALSE, BFALSE);
						}
					if (STRINGP(BgL_string1z00_4378))
						{	/* Ieee/string.scm 383 */
							BgL_auxz00_5305 = BgL_string1z00_4378;
						}
					else
						{
							obj_t BgL_auxz00_5308;

							BgL_auxz00_5308 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(18668L),
								BGl_string2672z00zz__r4_strings_6_7z00,
								BGl_string2660z00zz__r4_strings_6_7z00, BgL_string1z00_4378);
							FAILURE(BgL_auxz00_5308, BFALSE, BFALSE);
						}
					BgL_tmpz00_5304 =
						BGl_substringzd2cizd3zf3zf2zz__r4_strings_6_7z00(BgL_auxz00_5305,
						BgL_auxz00_5312, BgL_auxz00_5319);
				}
				return BBOOL(BgL_tmpz00_5304);
			}
		}

	}



/* _substring-ci-at? */
	obj_t BGl__substringzd2cizd2atzf3zf3zz__r4_strings_6_7z00(obj_t
		BgL_env1105z00_40, obj_t BgL_opt1104z00_39)
	{
		{	/* Ieee/string.scm 388 */
			{	/* Ieee/string.scm 388 */
				obj_t BgL_g1106z00_4985;
				obj_t BgL_g1107z00_4986;
				obj_t BgL_g1108z00_4987;

				BgL_g1106z00_4985 = VECTOR_REF(BgL_opt1104z00_39, 0L);
				BgL_g1107z00_4986 = VECTOR_REF(BgL_opt1104z00_39, 1L);
				BgL_g1108z00_4987 = VECTOR_REF(BgL_opt1104z00_39, 2L);
				switch (VECTOR_LENGTH(BgL_opt1104z00_39))
					{
					case 3L:

						{	/* Ieee/string.scm 388 */

							{	/* Ieee/string.scm 388 */
								obj_t BgL_string1z00_4988;
								obj_t BgL_string2z00_4989;
								long BgL_offz00_4990;

								if (STRINGP(BgL_g1106z00_4985))
									{	/* Ieee/string.scm 388 */
										BgL_string1z00_4988 = BgL_g1106z00_4985;
									}
								else
									{
										obj_t BgL_auxz00_5335;

										BgL_auxz00_5335 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2658z00zz__r4_strings_6_7z00, BINT(18930L),
											BGl_string2673z00zz__r4_strings_6_7z00,
											BGl_string2660z00zz__r4_strings_6_7z00,
											BgL_g1106z00_4985);
										FAILURE(BgL_auxz00_5335, BFALSE, BFALSE);
									}
								if (STRINGP(BgL_g1107z00_4986))
									{	/* Ieee/string.scm 388 */
										BgL_string2z00_4989 = BgL_g1107z00_4986;
									}
								else
									{
										obj_t BgL_auxz00_5341;

										BgL_auxz00_5341 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2658z00zz__r4_strings_6_7z00, BINT(18930L),
											BGl_string2673z00zz__r4_strings_6_7z00,
											BGl_string2660z00zz__r4_strings_6_7z00,
											BgL_g1107z00_4986);
										FAILURE(BgL_auxz00_5341, BFALSE, BFALSE);
									}
								{	/* Ieee/string.scm 388 */
									obj_t BgL_tmpz00_5345;

									if (INTEGERP(BgL_g1108z00_4987))
										{	/* Ieee/string.scm 388 */
											BgL_tmpz00_5345 = BgL_g1108z00_4987;
										}
									else
										{
											obj_t BgL_auxz00_5348;

											BgL_auxz00_5348 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(18930L),
												BGl_string2673z00zz__r4_strings_6_7z00,
												BGl_string2662z00zz__r4_strings_6_7z00,
												BgL_g1108z00_4987);
											FAILURE(BgL_auxz00_5348, BFALSE, BFALSE);
										}
									BgL_offz00_4990 = (long) CINT(BgL_tmpz00_5345);
								}
								return
									BBOOL(bigloo_strcmp_ci_at(BgL_string1z00_4988,
										BgL_string2z00_4989, BgL_offz00_4990));
							}
						}
						break;
					case 4L:

						{	/* Ieee/string.scm 388 */
							obj_t BgL_lenz00_4991;

							BgL_lenz00_4991 = VECTOR_REF(BgL_opt1104z00_39, 3L);
							{	/* Ieee/string.scm 388 */

								{	/* Ieee/string.scm 388 */
									obj_t BgL_string1z00_4992;
									obj_t BgL_string2z00_4993;
									long BgL_offz00_4994;

									if (STRINGP(BgL_g1106z00_4985))
										{	/* Ieee/string.scm 388 */
											BgL_string1z00_4992 = BgL_g1106z00_4985;
										}
									else
										{
											obj_t BgL_auxz00_5358;

											BgL_auxz00_5358 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(18930L),
												BGl_string2673z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00,
												BgL_g1106z00_4985);
											FAILURE(BgL_auxz00_5358, BFALSE, BFALSE);
										}
									if (STRINGP(BgL_g1107z00_4986))
										{	/* Ieee/string.scm 388 */
											BgL_string2z00_4993 = BgL_g1107z00_4986;
										}
									else
										{
											obj_t BgL_auxz00_5364;

											BgL_auxz00_5364 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(18930L),
												BGl_string2673z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00,
												BgL_g1107z00_4986);
											FAILURE(BgL_auxz00_5364, BFALSE, BFALSE);
										}
									{	/* Ieee/string.scm 388 */
										obj_t BgL_tmpz00_5368;

										if (INTEGERP(BgL_g1108z00_4987))
											{	/* Ieee/string.scm 388 */
												BgL_tmpz00_5368 = BgL_g1108z00_4987;
											}
										else
											{
												obj_t BgL_auxz00_5371;

												BgL_auxz00_5371 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2658z00zz__r4_strings_6_7z00, BINT(18930L),
													BGl_string2673z00zz__r4_strings_6_7z00,
													BGl_string2662z00zz__r4_strings_6_7z00,
													BgL_g1108z00_4987);
												FAILURE(BgL_auxz00_5371, BFALSE, BFALSE);
											}
										BgL_offz00_4994 = (long) CINT(BgL_tmpz00_5368);
									}
									{	/* Ieee/string.scm 389 */
										bool_t BgL_test3015z00_5376;

										{	/* Ieee/string.scm 389 */
											long BgL_n1z00_4995;

											{	/* Ieee/string.scm 389 */
												obj_t BgL_tmpz00_5377;

												if (INTEGERP(BgL_lenz00_4991))
													{	/* Ieee/string.scm 389 */
														BgL_tmpz00_5377 = BgL_lenz00_4991;
													}
												else
													{
														obj_t BgL_auxz00_5380;

														BgL_auxz00_5380 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2658z00zz__r4_strings_6_7z00,
															BINT(19016L),
															BGl_string2673z00zz__r4_strings_6_7z00,
															BGl_string2662z00zz__r4_strings_6_7z00,
															BgL_lenz00_4991);
														FAILURE(BgL_auxz00_5380, BFALSE, BFALSE);
													}
												BgL_n1z00_4995 = (long) CINT(BgL_tmpz00_5377);
											}
											BgL_test3015z00_5376 = (BgL_n1z00_4995 == -1L);
										}
										if (BgL_test3015z00_5376)
											{	/* Ieee/string.scm 389 */
												return
													BBOOL(bigloo_strcmp_ci_at(BgL_string1z00_4992,
														BgL_string2z00_4993, BgL_offz00_4994));
											}
										else
											{	/* Ieee/string.scm 391 */
												bool_t BgL_tmpz00_5388;

												{	/* Ieee/string.scm 391 */
													long BgL_tmpz00_5389;

													{	/* Ieee/string.scm 391 */
														obj_t BgL_tmpz00_5390;

														if (INTEGERP(BgL_lenz00_4991))
															{	/* Ieee/string.scm 391 */
																BgL_tmpz00_5390 = BgL_lenz00_4991;
															}
														else
															{
																obj_t BgL_auxz00_5393;

																BgL_auxz00_5393 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string2658z00zz__r4_strings_6_7z00,
																	BINT(19108L),
																	BGl_string2673z00zz__r4_strings_6_7z00,
																	BGl_string2662z00zz__r4_strings_6_7z00,
																	BgL_lenz00_4991);
																FAILURE(BgL_auxz00_5393, BFALSE, BFALSE);
															}
														BgL_tmpz00_5389 = (long) CINT(BgL_tmpz00_5390);
													}
													BgL_tmpz00_5388 =
														bigloo_strncmp_ci_at(BgL_string1z00_4992,
														BgL_string2z00_4993, BgL_offz00_4994,
														BgL_tmpz00_5389);
												}
												return BBOOL(BgL_tmpz00_5388);
											}
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* substring-ci-at? */
	BGL_EXPORTED_DEF bool_t
		BGl_substringzd2cizd2atzf3zf3zz__r4_strings_6_7z00(obj_t BgL_string1z00_35,
		obj_t BgL_string2z00_36, long BgL_offz00_37, obj_t BgL_lenz00_38)
	{
		{	/* Ieee/string.scm 388 */
			if (((long) CINT(BgL_lenz00_38) == -1L))
				{	/* Ieee/string.scm 389 */
					return
						bigloo_strcmp_ci_at(BgL_string1z00_35, BgL_string2z00_36,
						BgL_offz00_37);
				}
			else
				{	/* Ieee/string.scm 389 */
					return
						bigloo_strncmp_ci_at(BgL_string1z00_35, BgL_string2z00_36,
						BgL_offz00_37, (long) CINT(BgL_lenz00_38));
		}}

	}



/* empty-string? */
	BGL_EXPORTED_DEF bool_t BGl_emptyzd2stringzf3z21zz__r4_strings_6_7z00(obj_t
		BgL_stringz00_41)
	{
		{	/* Ieee/string.scm 396 */
			return (STRING_LENGTH(BgL_stringz00_41) == 0L);
		}

	}



/* &empty-string? */
	obj_t BGl_z62emptyzd2stringzf3z43zz__r4_strings_6_7z00(obj_t BgL_envz00_4381,
		obj_t BgL_stringz00_4382)
	{
		{	/* Ieee/string.scm 396 */
			{	/* Ieee/string.scm 397 */
				bool_t BgL_tmpz00_5410;

				{	/* Ieee/string.scm 397 */
					obj_t BgL_auxz00_5411;

					if (STRINGP(BgL_stringz00_4382))
						{	/* Ieee/string.scm 397 */
							BgL_auxz00_5411 = BgL_stringz00_4382;
						}
					else
						{
							obj_t BgL_auxz00_5414;

							BgL_auxz00_5414 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(19384L),
								BGl_string2674z00zz__r4_strings_6_7z00,
								BGl_string2660z00zz__r4_strings_6_7z00, BgL_stringz00_4382);
							FAILURE(BgL_auxz00_5414, BFALSE, BFALSE);
						}
					BgL_tmpz00_5410 =
						BGl_emptyzd2stringzf3z21zz__r4_strings_6_7z00(BgL_auxz00_5411);
				}
				return BBOOL(BgL_tmpz00_5410);
			}
		}

	}



/* string-ci=? */
	BGL_EXPORTED_DEF bool_t BGl_stringzd2cizd3zf3zf2zz__r4_strings_6_7z00(obj_t
		BgL_string1z00_42, obj_t BgL_string2z00_43)
	{
		{	/* Ieee/string.scm 402 */
			BGL_TAIL return bigloo_strcicmp(BgL_string1z00_42, BgL_string2z00_43);
		}

	}



/* &string-ci=? */
	obj_t BGl_z62stringzd2cizd3zf3z90zz__r4_strings_6_7z00(obj_t BgL_envz00_4383,
		obj_t BgL_string1z00_4384, obj_t BgL_string2z00_4385)
	{
		{	/* Ieee/string.scm 402 */
			{	/* Ieee/string.scm 403 */
				bool_t BgL_tmpz00_5421;

				{	/* Ieee/string.scm 403 */
					obj_t BgL_auxz00_5429;
					obj_t BgL_auxz00_5422;

					if (STRINGP(BgL_string2z00_4385))
						{	/* Ieee/string.scm 403 */
							BgL_auxz00_5429 = BgL_string2z00_4385;
						}
					else
						{
							obj_t BgL_auxz00_5432;

							BgL_auxz00_5432 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(19682L),
								BGl_string2675z00zz__r4_strings_6_7z00,
								BGl_string2660z00zz__r4_strings_6_7z00, BgL_string2z00_4385);
							FAILURE(BgL_auxz00_5432, BFALSE, BFALSE);
						}
					if (STRINGP(BgL_string1z00_4384))
						{	/* Ieee/string.scm 403 */
							BgL_auxz00_5422 = BgL_string1z00_4384;
						}
					else
						{
							obj_t BgL_auxz00_5425;

							BgL_auxz00_5425 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(19682L),
								BGl_string2675z00zz__r4_strings_6_7z00,
								BGl_string2660z00zz__r4_strings_6_7z00, BgL_string1z00_4384);
							FAILURE(BgL_auxz00_5425, BFALSE, BFALSE);
						}
					BgL_tmpz00_5421 =
						BGl_stringzd2cizd3zf3zf2zz__r4_strings_6_7z00(BgL_auxz00_5422,
						BgL_auxz00_5429);
				}
				return BBOOL(BgL_tmpz00_5421);
			}
		}

	}



/* string<? */
	BGL_EXPORTED_DEF bool_t BGl_stringzc3zf3z30zz__r4_strings_6_7z00(obj_t
		BgL_string1z00_44, obj_t BgL_string2z00_45)
	{
		{	/* Ieee/string.scm 408 */
			BGL_TAIL return bigloo_string_lt(BgL_string1z00_44, BgL_string2z00_45);
		}

	}



/* &string<? */
	obj_t BGl_z62stringzc3zf3z52zz__r4_strings_6_7z00(obj_t BgL_envz00_4386,
		obj_t BgL_string1z00_4387, obj_t BgL_string2z00_4388)
	{
		{	/* Ieee/string.scm 408 */
			{	/* Ieee/string.scm 409 */
				bool_t BgL_tmpz00_5439;

				{	/* Ieee/string.scm 409 */
					obj_t BgL_auxz00_5447;
					obj_t BgL_auxz00_5440;

					if (STRINGP(BgL_string2z00_4388))
						{	/* Ieee/string.scm 409 */
							BgL_auxz00_5447 = BgL_string2z00_4388;
						}
					else
						{
							obj_t BgL_auxz00_5450;

							BgL_auxz00_5450 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(19979L),
								BGl_string2676z00zz__r4_strings_6_7z00,
								BGl_string2660z00zz__r4_strings_6_7z00, BgL_string2z00_4388);
							FAILURE(BgL_auxz00_5450, BFALSE, BFALSE);
						}
					if (STRINGP(BgL_string1z00_4387))
						{	/* Ieee/string.scm 409 */
							BgL_auxz00_5440 = BgL_string1z00_4387;
						}
					else
						{
							obj_t BgL_auxz00_5443;

							BgL_auxz00_5443 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(19979L),
								BGl_string2676z00zz__r4_strings_6_7z00,
								BGl_string2660z00zz__r4_strings_6_7z00, BgL_string1z00_4387);
							FAILURE(BgL_auxz00_5443, BFALSE, BFALSE);
						}
					BgL_tmpz00_5439 =
						BGl_stringzc3zf3z30zz__r4_strings_6_7z00(BgL_auxz00_5440,
						BgL_auxz00_5447);
				}
				return BBOOL(BgL_tmpz00_5439);
			}
		}

	}



/* string>? */
	BGL_EXPORTED_DEF bool_t BGl_stringze3zf3z10zz__r4_strings_6_7z00(obj_t
		BgL_string1z00_46, obj_t BgL_string2z00_47)
	{
		{	/* Ieee/string.scm 414 */
			BGL_TAIL return bigloo_string_gt(BgL_string1z00_46, BgL_string2z00_47);
		}

	}



/* &string>? */
	obj_t BGl_z62stringze3zf3z72zz__r4_strings_6_7z00(obj_t BgL_envz00_4389,
		obj_t BgL_string1z00_4390, obj_t BgL_string2z00_4391)
	{
		{	/* Ieee/string.scm 414 */
			{	/* Ieee/string.scm 415 */
				bool_t BgL_tmpz00_5457;

				{	/* Ieee/string.scm 415 */
					obj_t BgL_auxz00_5465;
					obj_t BgL_auxz00_5458;

					if (STRINGP(BgL_string2z00_4391))
						{	/* Ieee/string.scm 415 */
							BgL_auxz00_5465 = BgL_string2z00_4391;
						}
					else
						{
							obj_t BgL_auxz00_5468;

							BgL_auxz00_5468 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(20277L),
								BGl_string2677z00zz__r4_strings_6_7z00,
								BGl_string2660z00zz__r4_strings_6_7z00, BgL_string2z00_4391);
							FAILURE(BgL_auxz00_5468, BFALSE, BFALSE);
						}
					if (STRINGP(BgL_string1z00_4390))
						{	/* Ieee/string.scm 415 */
							BgL_auxz00_5458 = BgL_string1z00_4390;
						}
					else
						{
							obj_t BgL_auxz00_5461;

							BgL_auxz00_5461 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(20277L),
								BGl_string2677z00zz__r4_strings_6_7z00,
								BGl_string2660z00zz__r4_strings_6_7z00, BgL_string1z00_4390);
							FAILURE(BgL_auxz00_5461, BFALSE, BFALSE);
						}
					BgL_tmpz00_5457 =
						BGl_stringze3zf3z10zz__r4_strings_6_7z00(BgL_auxz00_5458,
						BgL_auxz00_5465);
				}
				return BBOOL(BgL_tmpz00_5457);
			}
		}

	}



/* string<=? */
	BGL_EXPORTED_DEF bool_t BGl_stringzc3zd3zf3ze3zz__r4_strings_6_7z00(obj_t
		BgL_string1z00_48, obj_t BgL_string2z00_49)
	{
		{	/* Ieee/string.scm 420 */
			BGL_TAIL return bigloo_string_le(BgL_string1z00_48, BgL_string2z00_49);
		}

	}



/* &string<=? */
	obj_t BGl_z62stringzc3zd3zf3z81zz__r4_strings_6_7z00(obj_t BgL_envz00_4392,
		obj_t BgL_string1z00_4393, obj_t BgL_string2z00_4394)
	{
		{	/* Ieee/string.scm 420 */
			{	/* Ieee/string.scm 421 */
				bool_t BgL_tmpz00_5475;

				{	/* Ieee/string.scm 421 */
					obj_t BgL_auxz00_5483;
					obj_t BgL_auxz00_5476;

					if (STRINGP(BgL_string2z00_4394))
						{	/* Ieee/string.scm 421 */
							BgL_auxz00_5483 = BgL_string2z00_4394;
						}
					else
						{
							obj_t BgL_auxz00_5486;

							BgL_auxz00_5486 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(20576L),
								BGl_string2678z00zz__r4_strings_6_7z00,
								BGl_string2660z00zz__r4_strings_6_7z00, BgL_string2z00_4394);
							FAILURE(BgL_auxz00_5486, BFALSE, BFALSE);
						}
					if (STRINGP(BgL_string1z00_4393))
						{	/* Ieee/string.scm 421 */
							BgL_auxz00_5476 = BgL_string1z00_4393;
						}
					else
						{
							obj_t BgL_auxz00_5479;

							BgL_auxz00_5479 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(20576L),
								BGl_string2678z00zz__r4_strings_6_7z00,
								BGl_string2660z00zz__r4_strings_6_7z00, BgL_string1z00_4393);
							FAILURE(BgL_auxz00_5479, BFALSE, BFALSE);
						}
					BgL_tmpz00_5475 =
						BGl_stringzc3zd3zf3ze3zz__r4_strings_6_7z00(BgL_auxz00_5476,
						BgL_auxz00_5483);
				}
				return BBOOL(BgL_tmpz00_5475);
			}
		}

	}



/* string>=? */
	BGL_EXPORTED_DEF bool_t BGl_stringze3zd3zf3zc3zz__r4_strings_6_7z00(obj_t
		BgL_string1z00_50, obj_t BgL_string2z00_51)
	{
		{	/* Ieee/string.scm 426 */
			BGL_TAIL return bigloo_string_ge(BgL_string1z00_50, BgL_string2z00_51);
		}

	}



/* &string>=? */
	obj_t BGl_z62stringze3zd3zf3za1zz__r4_strings_6_7z00(obj_t BgL_envz00_4395,
		obj_t BgL_string1z00_4396, obj_t BgL_string2z00_4397)
	{
		{	/* Ieee/string.scm 426 */
			{	/* Ieee/string.scm 427 */
				bool_t BgL_tmpz00_5493;

				{	/* Ieee/string.scm 427 */
					obj_t BgL_auxz00_5501;
					obj_t BgL_auxz00_5494;

					if (STRINGP(BgL_string2z00_4397))
						{	/* Ieee/string.scm 427 */
							BgL_auxz00_5501 = BgL_string2z00_4397;
						}
					else
						{
							obj_t BgL_auxz00_5504;

							BgL_auxz00_5504 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(20875L),
								BGl_string2679z00zz__r4_strings_6_7z00,
								BGl_string2660z00zz__r4_strings_6_7z00, BgL_string2z00_4397);
							FAILURE(BgL_auxz00_5504, BFALSE, BFALSE);
						}
					if (STRINGP(BgL_string1z00_4396))
						{	/* Ieee/string.scm 427 */
							BgL_auxz00_5494 = BgL_string1z00_4396;
						}
					else
						{
							obj_t BgL_auxz00_5497;

							BgL_auxz00_5497 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(20875L),
								BGl_string2679z00zz__r4_strings_6_7z00,
								BGl_string2660z00zz__r4_strings_6_7z00, BgL_string1z00_4396);
							FAILURE(BgL_auxz00_5497, BFALSE, BFALSE);
						}
					BgL_tmpz00_5493 =
						BGl_stringze3zd3zf3zc3zz__r4_strings_6_7z00(BgL_auxz00_5494,
						BgL_auxz00_5501);
				}
				return BBOOL(BgL_tmpz00_5493);
			}
		}

	}



/* string-ci<? */
	BGL_EXPORTED_DEF bool_t BGl_stringzd2cizc3zf3ze2zz__r4_strings_6_7z00(obj_t
		BgL_string1z00_52, obj_t BgL_string2z00_53)
	{
		{	/* Ieee/string.scm 432 */
			BGL_TAIL return bigloo_string_cilt(BgL_string1z00_52, BgL_string2z00_53);
		}

	}



/* &string-ci<? */
	obj_t BGl_z62stringzd2cizc3zf3z80zz__r4_strings_6_7z00(obj_t BgL_envz00_4398,
		obj_t BgL_string1z00_4399, obj_t BgL_string2z00_4400)
	{
		{	/* Ieee/string.scm 432 */
			{	/* Ieee/string.scm 433 */
				bool_t BgL_tmpz00_5511;

				{	/* Ieee/string.scm 433 */
					obj_t BgL_auxz00_5519;
					obj_t BgL_auxz00_5512;

					if (STRINGP(BgL_string2z00_4400))
						{	/* Ieee/string.scm 433 */
							BgL_auxz00_5519 = BgL_string2z00_4400;
						}
					else
						{
							obj_t BgL_auxz00_5522;

							BgL_auxz00_5522 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(21176L),
								BGl_string2680z00zz__r4_strings_6_7z00,
								BGl_string2660z00zz__r4_strings_6_7z00, BgL_string2z00_4400);
							FAILURE(BgL_auxz00_5522, BFALSE, BFALSE);
						}
					if (STRINGP(BgL_string1z00_4399))
						{	/* Ieee/string.scm 433 */
							BgL_auxz00_5512 = BgL_string1z00_4399;
						}
					else
						{
							obj_t BgL_auxz00_5515;

							BgL_auxz00_5515 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(21176L),
								BGl_string2680z00zz__r4_strings_6_7z00,
								BGl_string2660z00zz__r4_strings_6_7z00, BgL_string1z00_4399);
							FAILURE(BgL_auxz00_5515, BFALSE, BFALSE);
						}
					BgL_tmpz00_5511 =
						BGl_stringzd2cizc3zf3ze2zz__r4_strings_6_7z00(BgL_auxz00_5512,
						BgL_auxz00_5519);
				}
				return BBOOL(BgL_tmpz00_5511);
			}
		}

	}



/* string-ci>? */
	BGL_EXPORTED_DEF bool_t BGl_stringzd2cize3zf3zc2zz__r4_strings_6_7z00(obj_t
		BgL_string1z00_54, obj_t BgL_string2z00_55)
	{
		{	/* Ieee/string.scm 438 */
			BGL_TAIL return bigloo_string_cigt(BgL_string1z00_54, BgL_string2z00_55);
		}

	}



/* &string-ci>? */
	obj_t BGl_z62stringzd2cize3zf3za0zz__r4_strings_6_7z00(obj_t BgL_envz00_4401,
		obj_t BgL_string1z00_4402, obj_t BgL_string2z00_4403)
	{
		{	/* Ieee/string.scm 438 */
			{	/* Ieee/string.scm 439 */
				bool_t BgL_tmpz00_5529;

				{	/* Ieee/string.scm 439 */
					obj_t BgL_auxz00_5537;
					obj_t BgL_auxz00_5530;

					if (STRINGP(BgL_string2z00_4403))
						{	/* Ieee/string.scm 439 */
							BgL_auxz00_5537 = BgL_string2z00_4403;
						}
					else
						{
							obj_t BgL_auxz00_5540;

							BgL_auxz00_5540 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(21479L),
								BGl_string2681z00zz__r4_strings_6_7z00,
								BGl_string2660z00zz__r4_strings_6_7z00, BgL_string2z00_4403);
							FAILURE(BgL_auxz00_5540, BFALSE, BFALSE);
						}
					if (STRINGP(BgL_string1z00_4402))
						{	/* Ieee/string.scm 439 */
							BgL_auxz00_5530 = BgL_string1z00_4402;
						}
					else
						{
							obj_t BgL_auxz00_5533;

							BgL_auxz00_5533 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(21479L),
								BGl_string2681z00zz__r4_strings_6_7z00,
								BGl_string2660z00zz__r4_strings_6_7z00, BgL_string1z00_4402);
							FAILURE(BgL_auxz00_5533, BFALSE, BFALSE);
						}
					BgL_tmpz00_5529 =
						BGl_stringzd2cize3zf3zc2zz__r4_strings_6_7z00(BgL_auxz00_5530,
						BgL_auxz00_5537);
				}
				return BBOOL(BgL_tmpz00_5529);
			}
		}

	}



/* string-ci<=? */
	BGL_EXPORTED_DEF bool_t BGl_stringzd2cizc3zd3zf3z31zz__r4_strings_6_7z00(obj_t
		BgL_string1z00_56, obj_t BgL_string2z00_57)
	{
		{	/* Ieee/string.scm 444 */
			BGL_TAIL return bigloo_string_cile(BgL_string1z00_56, BgL_string2z00_57);
		}

	}



/* &string-ci<=? */
	obj_t BGl_z62stringzd2cizc3zd3zf3z53zz__r4_strings_6_7z00(obj_t
		BgL_envz00_4404, obj_t BgL_string1z00_4405, obj_t BgL_string2z00_4406)
	{
		{	/* Ieee/string.scm 444 */
			{	/* Ieee/string.scm 445 */
				bool_t BgL_tmpz00_5547;

				{	/* Ieee/string.scm 445 */
					obj_t BgL_auxz00_5555;
					obj_t BgL_auxz00_5548;

					if (STRINGP(BgL_string2z00_4406))
						{	/* Ieee/string.scm 445 */
							BgL_auxz00_5555 = BgL_string2z00_4406;
						}
					else
						{
							obj_t BgL_auxz00_5558;

							BgL_auxz00_5558 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(21783L),
								BGl_string2682z00zz__r4_strings_6_7z00,
								BGl_string2660z00zz__r4_strings_6_7z00, BgL_string2z00_4406);
							FAILURE(BgL_auxz00_5558, BFALSE, BFALSE);
						}
					if (STRINGP(BgL_string1z00_4405))
						{	/* Ieee/string.scm 445 */
							BgL_auxz00_5548 = BgL_string1z00_4405;
						}
					else
						{
							obj_t BgL_auxz00_5551;

							BgL_auxz00_5551 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(21783L),
								BGl_string2682z00zz__r4_strings_6_7z00,
								BGl_string2660z00zz__r4_strings_6_7z00, BgL_string1z00_4405);
							FAILURE(BgL_auxz00_5551, BFALSE, BFALSE);
						}
					BgL_tmpz00_5547 =
						BGl_stringzd2cizc3zd3zf3z31zz__r4_strings_6_7z00(BgL_auxz00_5548,
						BgL_auxz00_5555);
				}
				return BBOOL(BgL_tmpz00_5547);
			}
		}

	}



/* string-ci>=? */
	BGL_EXPORTED_DEF bool_t BGl_stringzd2cize3zd3zf3z11zz__r4_strings_6_7z00(obj_t
		BgL_string1z00_58, obj_t BgL_string2z00_59)
	{
		{	/* Ieee/string.scm 450 */
			BGL_TAIL return bigloo_string_cige(BgL_string1z00_58, BgL_string2z00_59);
		}

	}



/* &string-ci>=? */
	obj_t BGl_z62stringzd2cize3zd3zf3z73zz__r4_strings_6_7z00(obj_t
		BgL_envz00_4407, obj_t BgL_string1z00_4408, obj_t BgL_string2z00_4409)
	{
		{	/* Ieee/string.scm 450 */
			{	/* Ieee/string.scm 451 */
				bool_t BgL_tmpz00_5565;

				{	/* Ieee/string.scm 451 */
					obj_t BgL_auxz00_5573;
					obj_t BgL_auxz00_5566;

					if (STRINGP(BgL_string2z00_4409))
						{	/* Ieee/string.scm 451 */
							BgL_auxz00_5573 = BgL_string2z00_4409;
						}
					else
						{
							obj_t BgL_auxz00_5576;

							BgL_auxz00_5576 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(22087L),
								BGl_string2683z00zz__r4_strings_6_7z00,
								BGl_string2660z00zz__r4_strings_6_7z00, BgL_string2z00_4409);
							FAILURE(BgL_auxz00_5576, BFALSE, BFALSE);
						}
					if (STRINGP(BgL_string1z00_4408))
						{	/* Ieee/string.scm 451 */
							BgL_auxz00_5566 = BgL_string1z00_4408;
						}
					else
						{
							obj_t BgL_auxz00_5569;

							BgL_auxz00_5569 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(22087L),
								BGl_string2683z00zz__r4_strings_6_7z00,
								BGl_string2660z00zz__r4_strings_6_7z00, BgL_string1z00_4408);
							FAILURE(BgL_auxz00_5569, BFALSE, BFALSE);
						}
					BgL_tmpz00_5565 =
						BGl_stringzd2cize3zd3zf3z11zz__r4_strings_6_7z00(BgL_auxz00_5566,
						BgL_auxz00_5573);
				}
				return BBOOL(BgL_tmpz00_5565);
			}
		}

	}



/* _substring */
	obj_t BGl__substringz00zz__r4_strings_6_7z00(obj_t BgL_env1112z00_64,
		obj_t BgL_opt1111z00_63)
	{
		{	/* Ieee/string.scm 456 */
			{	/* Ieee/string.scm 456 */
				obj_t BgL_stringz00_966;
				obj_t BgL_g1113z00_967;

				BgL_stringz00_966 = VECTOR_REF(BgL_opt1111z00_63, 0L);
				BgL_g1113z00_967 = VECTOR_REF(BgL_opt1111z00_63, 1L);
				switch (VECTOR_LENGTH(BgL_opt1111z00_63))
					{
					case 2L:

						{	/* Ieee/string.scm 456 */
							long BgL_endz00_970;

							{	/* Ieee/string.scm 456 */
								obj_t BgL_stringz00_2613;

								if (STRINGP(BgL_stringz00_966))
									{	/* Ieee/string.scm 456 */
										BgL_stringz00_2613 = BgL_stringz00_966;
									}
								else
									{
										obj_t BgL_auxz00_5586;

										BgL_auxz00_5586 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2658z00zz__r4_strings_6_7z00, BINT(22396L),
											BGl_string2684z00zz__r4_strings_6_7z00,
											BGl_string2660z00zz__r4_strings_6_7z00,
											BgL_stringz00_966);
										FAILURE(BgL_auxz00_5586, BFALSE, BFALSE);
									}
								BgL_endz00_970 = STRING_LENGTH(BgL_stringz00_2613);
							}
							{	/* Ieee/string.scm 456 */

								{	/* Ieee/string.scm 456 */
									long BgL_auxz00_5598;
									obj_t BgL_auxz00_5591;

									{	/* Ieee/string.scm 456 */
										obj_t BgL_tmpz00_5599;

										if (INTEGERP(BgL_g1113z00_967))
											{	/* Ieee/string.scm 456 */
												BgL_tmpz00_5599 = BgL_g1113z00_967;
											}
										else
											{
												obj_t BgL_auxz00_5602;

												BgL_auxz00_5602 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2658z00zz__r4_strings_6_7z00, BINT(22342L),
													BGl_string2684z00zz__r4_strings_6_7z00,
													BGl_string2662z00zz__r4_strings_6_7z00,
													BgL_g1113z00_967);
												FAILURE(BgL_auxz00_5602, BFALSE, BFALSE);
											}
										BgL_auxz00_5598 = (long) CINT(BgL_tmpz00_5599);
									}
									if (STRINGP(BgL_stringz00_966))
										{	/* Ieee/string.scm 456 */
											BgL_auxz00_5591 = BgL_stringz00_966;
										}
									else
										{
											obj_t BgL_auxz00_5594;

											BgL_auxz00_5594 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(22342L),
												BGl_string2684z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00,
												BgL_stringz00_966);
											FAILURE(BgL_auxz00_5594, BFALSE, BFALSE);
										}
									return
										BGl_substringz00zz__r4_strings_6_7z00(BgL_auxz00_5591,
										BgL_auxz00_5598, BgL_endz00_970);
								}
							}
						}
						break;
					case 3L:

						{	/* Ieee/string.scm 456 */
							obj_t BgL_endz00_971;

							BgL_endz00_971 = VECTOR_REF(BgL_opt1111z00_63, 2L);
							{	/* Ieee/string.scm 456 */

								{	/* Ieee/string.scm 456 */
									long BgL_auxz00_5625;
									long BgL_auxz00_5616;
									obj_t BgL_auxz00_5609;

									{	/* Ieee/string.scm 456 */
										obj_t BgL_tmpz00_5626;

										if (INTEGERP(BgL_endz00_971))
											{	/* Ieee/string.scm 456 */
												BgL_tmpz00_5626 = BgL_endz00_971;
											}
										else
											{
												obj_t BgL_auxz00_5629;

												BgL_auxz00_5629 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2658z00zz__r4_strings_6_7z00, BINT(22342L),
													BGl_string2684z00zz__r4_strings_6_7z00,
													BGl_string2662z00zz__r4_strings_6_7z00,
													BgL_endz00_971);
												FAILURE(BgL_auxz00_5629, BFALSE, BFALSE);
											}
										BgL_auxz00_5625 = (long) CINT(BgL_tmpz00_5626);
									}
									{	/* Ieee/string.scm 456 */
										obj_t BgL_tmpz00_5617;

										if (INTEGERP(BgL_g1113z00_967))
											{	/* Ieee/string.scm 456 */
												BgL_tmpz00_5617 = BgL_g1113z00_967;
											}
										else
											{
												obj_t BgL_auxz00_5620;

												BgL_auxz00_5620 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2658z00zz__r4_strings_6_7z00, BINT(22342L),
													BGl_string2684z00zz__r4_strings_6_7z00,
													BGl_string2662z00zz__r4_strings_6_7z00,
													BgL_g1113z00_967);
												FAILURE(BgL_auxz00_5620, BFALSE, BFALSE);
											}
										BgL_auxz00_5616 = (long) CINT(BgL_tmpz00_5617);
									}
									if (STRINGP(BgL_stringz00_966))
										{	/* Ieee/string.scm 456 */
											BgL_auxz00_5609 = BgL_stringz00_966;
										}
									else
										{
											obj_t BgL_auxz00_5612;

											BgL_auxz00_5612 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(22342L),
												BGl_string2684z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00,
												BgL_stringz00_966);
											FAILURE(BgL_auxz00_5612, BFALSE, BFALSE);
										}
									return
										BGl_substringz00zz__r4_strings_6_7z00(BgL_auxz00_5609,
										BgL_auxz00_5616, BgL_auxz00_5625);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* substring */
	BGL_EXPORTED_DEF obj_t BGl_substringz00zz__r4_strings_6_7z00(obj_t
		BgL_stringz00_60, long BgL_startz00_61, long BgL_endz00_62)
	{
		{	/* Ieee/string.scm 456 */
			{	/* Ieee/string.scm 457 */
				long BgL_lenz00_972;

				BgL_lenz00_972 = STRING_LENGTH(BgL_stringz00_60);
				{	/* Ieee/string.scm 459 */
					bool_t BgL_test3044z00_5638;

					if ((BgL_startz00_61 < 0L))
						{	/* Ieee/string.scm 459 */
							BgL_test3044z00_5638 = ((bool_t) 1);
						}
					else
						{	/* Ieee/string.scm 459 */
							BgL_test3044z00_5638 = (BgL_startz00_61 > BgL_lenz00_972);
						}
					if (BgL_test3044z00_5638)
						{	/* Ieee/string.scm 461 */
							obj_t BgL_arg1229z00_975;
							obj_t BgL_arg1230z00_976;

							{	/* Ieee/string.scm 461 */
								obj_t BgL_arg1231z00_977;

								{	/* Ieee/fixnum.scm 1002 */

									BgL_arg1231z00_977 =
										BGl_fixnumzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
										(BgL_startz00_61, 10L);
								}
								BgL_arg1229z00_975 =
									string_append(BGl_string2685z00zz__r4_strings_6_7z00,
									BgL_arg1231z00_977);
							}
							{	/* Ieee/string.scm 462 */
								obj_t BgL_list1234z00_981;

								{	/* Ieee/string.scm 462 */
									obj_t BgL_arg1236z00_982;

									BgL_arg1236z00_982 = MAKE_YOUNG_PAIR(BgL_stringz00_60, BNIL);
									BgL_list1234z00_981 =
										MAKE_YOUNG_PAIR(BINT(BgL_lenz00_972), BgL_arg1236z00_982);
								}
								BgL_arg1230z00_976 = BgL_list1234z00_981;
							}
							return
								BGl_errorz00zz__errorz00(BGl_string2686z00zz__r4_strings_6_7z00,
								BgL_arg1229z00_975, BgL_arg1230z00_976);
						}
					else
						{	/* Ieee/string.scm 463 */
							bool_t BgL_test3046z00_5648;

							if ((BgL_endz00_62 < BgL_startz00_61))
								{	/* Ieee/string.scm 463 */
									BgL_test3046z00_5648 = ((bool_t) 1);
								}
							else
								{	/* Ieee/string.scm 463 */
									BgL_test3046z00_5648 = (BgL_endz00_62 > BgL_lenz00_972);
								}
							if (BgL_test3046z00_5648)
								{	/* Ieee/string.scm 465 */
									obj_t BgL_arg1239z00_985;
									obj_t BgL_arg1242z00_986;

									{	/* Ieee/string.scm 465 */
										obj_t BgL_arg1244z00_987;

										{	/* Ieee/fixnum.scm 1002 */

											BgL_arg1244z00_987 =
												BGl_fixnumzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
												(BgL_endz00_62, 10L);
										}
										BgL_arg1239z00_985 =
											string_append(BGl_string2687z00zz__r4_strings_6_7z00,
											BgL_arg1244z00_987);
									}
									{	/* Ieee/string.scm 466 */
										obj_t BgL_list1249z00_991;

										{	/* Ieee/string.scm 466 */
											obj_t BgL_arg1252z00_992;

											BgL_arg1252z00_992 =
												MAKE_YOUNG_PAIR(BgL_stringz00_60, BNIL);
											BgL_list1249z00_991 =
												MAKE_YOUNG_PAIR(BINT(BgL_lenz00_972),
												BgL_arg1252z00_992);
										}
										BgL_arg1242z00_986 = BgL_list1249z00_991;
									}
									return
										BGl_errorz00zz__errorz00
										(BGl_string2686z00zz__r4_strings_6_7z00, BgL_arg1239z00_985,
										BgL_arg1242z00_986);
								}
							else
								{	/* Ieee/string.scm 463 */
									BGL_TAIL return
										c_substring(BgL_stringz00_60, BgL_startz00_61,
										BgL_endz00_62);
								}
						}
				}
			}
		}

	}



/* substring-ur */
	BGL_EXPORTED_DEF obj_t BGl_substringzd2urzd2zz__r4_strings_6_7z00(obj_t
		BgL_stringz00_65, long BgL_startz00_66, long BgL_endz00_67)
	{
		{	/* Ieee/string.scm 473 */
			BGL_TAIL return
				c_substring(BgL_stringz00_65, BgL_startz00_66, BgL_endz00_67);
		}

	}



/* &substring-ur */
	obj_t BGl_z62substringzd2urzb0zz__r4_strings_6_7z00(obj_t BgL_envz00_4410,
		obj_t BgL_stringz00_4411, obj_t BgL_startz00_4412, obj_t BgL_endz00_4413)
	{
		{	/* Ieee/string.scm 473 */
			{	/* Ieee/string.scm 474 */
				long BgL_auxz00_5676;
				long BgL_auxz00_5667;
				obj_t BgL_auxz00_5660;

				{	/* Ieee/string.scm 474 */
					obj_t BgL_tmpz00_5677;

					if (INTEGERP(BgL_endz00_4413))
						{	/* Ieee/string.scm 474 */
							BgL_tmpz00_5677 = BgL_endz00_4413;
						}
					else
						{
							obj_t BgL_auxz00_5680;

							BgL_auxz00_5680 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(23120L),
								BGl_string2688z00zz__r4_strings_6_7z00,
								BGl_string2662z00zz__r4_strings_6_7z00, BgL_endz00_4413);
							FAILURE(BgL_auxz00_5680, BFALSE, BFALSE);
						}
					BgL_auxz00_5676 = (long) CINT(BgL_tmpz00_5677);
				}
				{	/* Ieee/string.scm 474 */
					obj_t BgL_tmpz00_5668;

					if (INTEGERP(BgL_startz00_4412))
						{	/* Ieee/string.scm 474 */
							BgL_tmpz00_5668 = BgL_startz00_4412;
						}
					else
						{
							obj_t BgL_auxz00_5671;

							BgL_auxz00_5671 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(23120L),
								BGl_string2688z00zz__r4_strings_6_7z00,
								BGl_string2662z00zz__r4_strings_6_7z00, BgL_startz00_4412);
							FAILURE(BgL_auxz00_5671, BFALSE, BFALSE);
						}
					BgL_auxz00_5667 = (long) CINT(BgL_tmpz00_5668);
				}
				if (STRINGP(BgL_stringz00_4411))
					{	/* Ieee/string.scm 474 */
						BgL_auxz00_5660 = BgL_stringz00_4411;
					}
				else
					{
						obj_t BgL_auxz00_5663;

						BgL_auxz00_5663 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2658z00zz__r4_strings_6_7z00, BINT(23120L),
							BGl_string2688z00zz__r4_strings_6_7z00,
							BGl_string2660z00zz__r4_strings_6_7z00, BgL_stringz00_4411);
						FAILURE(BgL_auxz00_5663, BFALSE, BFALSE);
					}
				return
					BGl_substringzd2urzd2zz__r4_strings_6_7z00(BgL_auxz00_5660,
					BgL_auxz00_5667, BgL_auxz00_5676);
			}
		}

	}



/* _string-contains */
	obj_t BGl__stringzd2containszd2zz__r4_strings_6_7z00(obj_t BgL_env1117z00_72,
		obj_t BgL_opt1116z00_71)
	{
		{	/* Ieee/string.scm 479 */
			{	/* Ieee/string.scm 479 */
				obj_t BgL_g1118z00_995;
				obj_t BgL_g1119z00_996;

				BgL_g1118z00_995 = VECTOR_REF(BgL_opt1116z00_71, 0L);
				BgL_g1119z00_996 = VECTOR_REF(BgL_opt1116z00_71, 1L);
				switch (VECTOR_LENGTH(BgL_opt1116z00_71))
					{
					case 2L:

						{	/* Ieee/string.scm 479 */

							{	/* Ieee/string.scm 479 */
								obj_t BgL_auxz00_5695;
								obj_t BgL_auxz00_5688;

								if (STRINGP(BgL_g1119z00_996))
									{	/* Ieee/string.scm 479 */
										BgL_auxz00_5695 = BgL_g1119z00_996;
									}
								else
									{
										obj_t BgL_auxz00_5698;

										BgL_auxz00_5698 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2658z00zz__r4_strings_6_7z00, BINT(23374L),
											BGl_string2689z00zz__r4_strings_6_7z00,
											BGl_string2660z00zz__r4_strings_6_7z00, BgL_g1119z00_996);
										FAILURE(BgL_auxz00_5698, BFALSE, BFALSE);
									}
								if (STRINGP(BgL_g1118z00_995))
									{	/* Ieee/string.scm 479 */
										BgL_auxz00_5688 = BgL_g1118z00_995;
									}
								else
									{
										obj_t BgL_auxz00_5691;

										BgL_auxz00_5691 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2658z00zz__r4_strings_6_7z00, BINT(23374L),
											BGl_string2689z00zz__r4_strings_6_7z00,
											BGl_string2660z00zz__r4_strings_6_7z00, BgL_g1118z00_995);
										FAILURE(BgL_auxz00_5691, BFALSE, BFALSE);
									}
								return
									BGl_stringzd2containszd2zz__r4_strings_6_7z00(BgL_auxz00_5688,
									BgL_auxz00_5695, (int) (0L));
						}} break;
					case 3L:

						{	/* Ieee/string.scm 479 */
							obj_t BgL_startz00_1000;

							BgL_startz00_1000 = VECTOR_REF(BgL_opt1116z00_71, 2L);
							{	/* Ieee/string.scm 479 */

								{	/* Ieee/string.scm 479 */
									int BgL_auxz00_5719;
									obj_t BgL_auxz00_5712;
									obj_t BgL_auxz00_5705;

									{	/* Ieee/string.scm 479 */
										obj_t BgL_tmpz00_5720;

										if (INTEGERP(BgL_startz00_1000))
											{	/* Ieee/string.scm 479 */
												BgL_tmpz00_5720 = BgL_startz00_1000;
											}
										else
											{
												obj_t BgL_auxz00_5723;

												BgL_auxz00_5723 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2658z00zz__r4_strings_6_7z00, BINT(23374L),
													BGl_string2689z00zz__r4_strings_6_7z00,
													BGl_string2662z00zz__r4_strings_6_7z00,
													BgL_startz00_1000);
												FAILURE(BgL_auxz00_5723, BFALSE, BFALSE);
											}
										BgL_auxz00_5719 = CINT(BgL_tmpz00_5720);
									}
									if (STRINGP(BgL_g1119z00_996))
										{	/* Ieee/string.scm 479 */
											BgL_auxz00_5712 = BgL_g1119z00_996;
										}
									else
										{
											obj_t BgL_auxz00_5715;

											BgL_auxz00_5715 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(23374L),
												BGl_string2689z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00,
												BgL_g1119z00_996);
											FAILURE(BgL_auxz00_5715, BFALSE, BFALSE);
										}
									if (STRINGP(BgL_g1118z00_995))
										{	/* Ieee/string.scm 479 */
											BgL_auxz00_5705 = BgL_g1118z00_995;
										}
									else
										{
											obj_t BgL_auxz00_5708;

											BgL_auxz00_5708 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(23374L),
												BGl_string2689z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00,
												BgL_g1118z00_995);
											FAILURE(BgL_auxz00_5708, BFALSE, BFALSE);
										}
									return
										BGl_stringzd2containszd2zz__r4_strings_6_7z00
										(BgL_auxz00_5705, BgL_auxz00_5712, BgL_auxz00_5719);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* string-contains */
	BGL_EXPORTED_DEF obj_t BGl_stringzd2containszd2zz__r4_strings_6_7z00(obj_t
		BgL_s1z00_68, obj_t BgL_s2z00_69, int BgL_startz00_70)
	{
		{	/* Ieee/string.scm 479 */
			{	/* Ieee/string.scm 480 */
				long BgL_l2z00_1001;

				BgL_l2z00_1001 = STRING_LENGTH(BgL_s2z00_69);
				if ((BgL_l2z00_1001 == 1L))
					{	/* Ieee/string.scm 482 */
						unsigned char BgL_arg1268z00_1003;

						BgL_arg1268z00_1003 = STRING_REF(BgL_s2z00_69, 0L);
						return
							BGl_stringzd2indexzd2zz__r4_strings_6_7z00(BgL_s1z00_68,
							BCHAR(BgL_arg1268z00_1003), BINT(BgL_startz00_70));
					}
				else
					{	/* Ieee/string.scm 483 */
						long BgL_l1z00_1004;
						long BgL_i0z00_1005;

						BgL_l1z00_1004 = STRING_LENGTH(BgL_s1z00_68);
						if (((long) (BgL_startz00_70) < 0L))
							{	/* Ieee/string.scm 484 */
								BgL_i0z00_1005 = 0L;
							}
						else
							{	/* Ieee/string.scm 484 */
								BgL_i0z00_1005 = (long) (BgL_startz00_70);
							}
						if ((BgL_l1z00_1004 < (BgL_i0z00_1005 + BgL_l2z00_1001)))
							{	/* Ieee/string.scm 485 */
								return BFALSE;
							}
						else
							{	/* Ieee/string.scm 487 */
								long BgL_stopz00_1008;

								BgL_stopz00_1008 = (BgL_l1z00_1004 - BgL_l2z00_1001);
								{
									long BgL_iz00_1010;

									BgL_iz00_1010 = BgL_i0z00_1005;
								BgL_zc3z04anonymousza31274ze3z87_1011:
									if (bigloo_strcmp_at(BgL_s1z00_68, BgL_s2z00_69,
											BgL_iz00_1010))
										{	/* Ieee/string.scm 490 */
											return BINT(BgL_iz00_1010);
										}
									else
										{	/* Ieee/string.scm 490 */
											if ((BgL_iz00_1010 == BgL_stopz00_1008))
												{	/* Ieee/string.scm 492 */
													return BFALSE;
												}
											else
												{
													long BgL_iz00_5752;

													BgL_iz00_5752 = (BgL_iz00_1010 + 1L);
													BgL_iz00_1010 = BgL_iz00_5752;
													goto BgL_zc3z04anonymousza31274ze3z87_1011;
												}
										}
								}
							}
					}
			}
		}

	}



/* _string-contains-ci */
	obj_t BGl__stringzd2containszd2ciz00zz__r4_strings_6_7z00(obj_t
		BgL_env1123z00_77, obj_t BgL_opt1122z00_76)
	{
		{	/* Ieee/string.scm 500 */
			{	/* Ieee/string.scm 500 */
				obj_t BgL_g1124z00_1018;
				obj_t BgL_g1125z00_1019;

				BgL_g1124z00_1018 = VECTOR_REF(BgL_opt1122z00_76, 0L);
				BgL_g1125z00_1019 = VECTOR_REF(BgL_opt1122z00_76, 1L);
				switch (VECTOR_LENGTH(BgL_opt1122z00_76))
					{
					case 2L:

						{	/* Ieee/string.scm 500 */

							{	/* Ieee/string.scm 500 */
								obj_t BgL_auxz00_5763;
								obj_t BgL_auxz00_5756;

								if (STRINGP(BgL_g1125z00_1019))
									{	/* Ieee/string.scm 500 */
										BgL_auxz00_5763 = BgL_g1125z00_1019;
									}
								else
									{
										obj_t BgL_auxz00_5766;

										BgL_auxz00_5766 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2658z00zz__r4_strings_6_7z00, BINT(24040L),
											BGl_string2690z00zz__r4_strings_6_7z00,
											BGl_string2660z00zz__r4_strings_6_7z00,
											BgL_g1125z00_1019);
										FAILURE(BgL_auxz00_5766, BFALSE, BFALSE);
									}
								if (STRINGP(BgL_g1124z00_1018))
									{	/* Ieee/string.scm 500 */
										BgL_auxz00_5756 = BgL_g1124z00_1018;
									}
								else
									{
										obj_t BgL_auxz00_5759;

										BgL_auxz00_5759 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2658z00zz__r4_strings_6_7z00, BINT(24040L),
											BGl_string2690z00zz__r4_strings_6_7z00,
											BGl_string2660z00zz__r4_strings_6_7z00,
											BgL_g1124z00_1018);
										FAILURE(BgL_auxz00_5759, BFALSE, BFALSE);
									}
								return
									BGl_stringzd2containszd2ciz00zz__r4_strings_6_7z00
									(BgL_auxz00_5756, BgL_auxz00_5763, (int) (0L));
						}} break;
					case 3L:

						{	/* Ieee/string.scm 500 */
							obj_t BgL_startz00_1023;

							BgL_startz00_1023 = VECTOR_REF(BgL_opt1122z00_76, 2L);
							{	/* Ieee/string.scm 500 */

								{	/* Ieee/string.scm 500 */
									int BgL_auxz00_5787;
									obj_t BgL_auxz00_5780;
									obj_t BgL_auxz00_5773;

									{	/* Ieee/string.scm 500 */
										obj_t BgL_tmpz00_5788;

										if (INTEGERP(BgL_startz00_1023))
											{	/* Ieee/string.scm 500 */
												BgL_tmpz00_5788 = BgL_startz00_1023;
											}
										else
											{
												obj_t BgL_auxz00_5791;

												BgL_auxz00_5791 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2658z00zz__r4_strings_6_7z00, BINT(24040L),
													BGl_string2690z00zz__r4_strings_6_7z00,
													BGl_string2662z00zz__r4_strings_6_7z00,
													BgL_startz00_1023);
												FAILURE(BgL_auxz00_5791, BFALSE, BFALSE);
											}
										BgL_auxz00_5787 = CINT(BgL_tmpz00_5788);
									}
									if (STRINGP(BgL_g1125z00_1019))
										{	/* Ieee/string.scm 500 */
											BgL_auxz00_5780 = BgL_g1125z00_1019;
										}
									else
										{
											obj_t BgL_auxz00_5783;

											BgL_auxz00_5783 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(24040L),
												BGl_string2690z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00,
												BgL_g1125z00_1019);
											FAILURE(BgL_auxz00_5783, BFALSE, BFALSE);
										}
									if (STRINGP(BgL_g1124z00_1018))
										{	/* Ieee/string.scm 500 */
											BgL_auxz00_5773 = BgL_g1124z00_1018;
										}
									else
										{
											obj_t BgL_auxz00_5776;

											BgL_auxz00_5776 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(24040L),
												BGl_string2690z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00,
												BgL_g1124z00_1018);
											FAILURE(BgL_auxz00_5776, BFALSE, BFALSE);
										}
									return
										BGl_stringzd2containszd2ciz00zz__r4_strings_6_7z00
										(BgL_auxz00_5773, BgL_auxz00_5780, BgL_auxz00_5787);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* string-contains-ci */
	BGL_EXPORTED_DEF obj_t
		BGl_stringzd2containszd2ciz00zz__r4_strings_6_7z00(obj_t BgL_s1z00_73,
		obj_t BgL_s2z00_74, int BgL_startz00_75)
	{
		{	/* Ieee/string.scm 500 */
			{	/* Ieee/string.scm 501 */
				long BgL_l1z00_1024;
				long BgL_l2z00_1025;
				long BgL_i0z00_1026;

				BgL_l1z00_1024 = STRING_LENGTH(BgL_s1z00_73);
				BgL_l2z00_1025 = STRING_LENGTH(BgL_s2z00_74);
				if (((long) (BgL_startz00_75) < 0L))
					{	/* Ieee/string.scm 503 */
						BgL_i0z00_1026 = 0L;
					}
				else
					{	/* Ieee/string.scm 503 */
						BgL_i0z00_1026 = (long) (BgL_startz00_75);
					}
				if ((BgL_l1z00_1024 < (BgL_i0z00_1026 + BgL_l2z00_1025)))
					{	/* Ieee/string.scm 504 */
						return BFALSE;
					}
				else
					{	/* Ieee/string.scm 506 */
						long BgL_stopz00_1029;

						BgL_stopz00_1029 = (BgL_l1z00_1024 - BgL_l2z00_1025);
						{
							long BgL_iz00_1031;

							BgL_iz00_1031 = BgL_i0z00_1026;
						BgL_zc3z04anonymousza31308ze3z87_1032:
							if (bigloo_strcmp_ci_at(BgL_s1z00_73, BgL_s2z00_74,
									BgL_iz00_1031))
								{	/* Ieee/string.scm 509 */
									return BINT(BgL_iz00_1031);
								}
							else
								{	/* Ieee/string.scm 509 */
									if ((BgL_iz00_1031 == BgL_stopz00_1029))
										{	/* Ieee/string.scm 511 */
											return BFALSE;
										}
									else
										{
											long BgL_iz00_5814;

											BgL_iz00_5814 = (BgL_iz00_1031 + 1L);
											BgL_iz00_1031 = BgL_iz00_5814;
											goto BgL_zc3z04anonymousza31308ze3z87_1032;
										}
								}
						}
					}
			}
		}

	}



/* string-compare3 */
	BGL_EXPORTED_DEF long BGl_stringzd2compare3zd2zz__r4_strings_6_7z00(obj_t
		BgL_az00_78, obj_t BgL_bz00_79)
	{
		{	/* Ieee/string.scm 523 */
			{	/* Ieee/string.scm 524 */
				long BgL_alz00_1039;

				BgL_alz00_1039 = STRING_LENGTH(BgL_az00_78);
				{	/* Ieee/string.scm 524 */
					long BgL_blz00_1040;

					BgL_blz00_1040 = STRING_LENGTH(BgL_bz00_79);
					{	/* Ieee/string.scm 525 */
						long BgL_lz00_1041;

						if ((BgL_alz00_1039 < BgL_blz00_1040))
							{	/* Ieee/string.scm 526 */
								BgL_lz00_1041 = BgL_alz00_1039;
							}
						else
							{	/* Ieee/string.scm 526 */
								BgL_lz00_1041 = BgL_blz00_1040;
							}
						{	/* Ieee/string.scm 526 */

							{
								long BgL_rz00_1043;

								BgL_rz00_1043 = 0L;
							BgL_zc3z04anonymousza31314ze3z87_1044:
								if ((BgL_rz00_1043 == BgL_lz00_1041))
									{	/* Ieee/string.scm 528 */
										return (BgL_alz00_1039 - BgL_blz00_1040);
									}
								else
									{	/* Ieee/string.scm 530 */
										long BgL_cz00_1046;

										BgL_cz00_1046 =
											(
											(STRING_REF(BgL_az00_78, BgL_rz00_1043)) -
											(STRING_REF(BgL_bz00_79, BgL_rz00_1043)));
										if ((BgL_cz00_1046 == 0L))
											{
												long BgL_rz00_5830;

												BgL_rz00_5830 = (BgL_rz00_1043 + 1L);
												BgL_rz00_1043 = BgL_rz00_5830;
												goto BgL_zc3z04anonymousza31314ze3z87_1044;
											}
										else
											{	/* Ieee/string.scm 532 */
												return BgL_cz00_1046;
											}
									}
							}
						}
					}
				}
			}
		}

	}



/* &string-compare3 */
	obj_t BGl_z62stringzd2compare3zb0zz__r4_strings_6_7z00(obj_t BgL_envz00_4414,
		obj_t BgL_az00_4415, obj_t BgL_bz00_4416)
	{
		{	/* Ieee/string.scm 523 */
			{	/* Ieee/string.scm 524 */
				long BgL_tmpz00_5832;

				{	/* Ieee/string.scm 524 */
					obj_t BgL_auxz00_5840;
					obj_t BgL_auxz00_5833;

					if (STRINGP(BgL_bz00_4416))
						{	/* Ieee/string.scm 524 */
							BgL_auxz00_5840 = BgL_bz00_4416;
						}
					else
						{
							obj_t BgL_auxz00_5843;

							BgL_auxz00_5843 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(24981L),
								BGl_string2691z00zz__r4_strings_6_7z00,
								BGl_string2660z00zz__r4_strings_6_7z00, BgL_bz00_4416);
							FAILURE(BgL_auxz00_5843, BFALSE, BFALSE);
						}
					if (STRINGP(BgL_az00_4415))
						{	/* Ieee/string.scm 524 */
							BgL_auxz00_5833 = BgL_az00_4415;
						}
					else
						{
							obj_t BgL_auxz00_5836;

							BgL_auxz00_5836 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(24981L),
								BGl_string2691z00zz__r4_strings_6_7z00,
								BGl_string2660z00zz__r4_strings_6_7z00, BgL_az00_4415);
							FAILURE(BgL_auxz00_5836, BFALSE, BFALSE);
						}
					BgL_tmpz00_5832 =
						BGl_stringzd2compare3zd2zz__r4_strings_6_7z00(BgL_auxz00_5833,
						BgL_auxz00_5840);
				}
				return BINT(BgL_tmpz00_5832);
			}
		}

	}



/* string-compare3-ci */
	BGL_EXPORTED_DEF long BGl_stringzd2compare3zd2ciz00zz__r4_strings_6_7z00(obj_t
		BgL_az00_80, obj_t BgL_bz00_81)
	{
		{	/* Ieee/string.scm 539 */
			{	/* Ieee/string.scm 540 */
				long BgL_alz00_1057;

				BgL_alz00_1057 = STRING_LENGTH(BgL_az00_80);
				{	/* Ieee/string.scm 540 */
					long BgL_blz00_1058;

					BgL_blz00_1058 = STRING_LENGTH(BgL_bz00_81);
					{	/* Ieee/string.scm 541 */
						long BgL_lz00_1059;

						if ((BgL_alz00_1057 < BgL_blz00_1058))
							{	/* Ieee/string.scm 542 */
								BgL_lz00_1059 = BgL_alz00_1057;
							}
						else
							{	/* Ieee/string.scm 542 */
								BgL_lz00_1059 = BgL_blz00_1058;
							}
						{	/* Ieee/string.scm 542 */

							{
								long BgL_rz00_1061;

								BgL_rz00_1061 = 0L;
							BgL_zc3z04anonymousza31323ze3z87_1062:
								if ((BgL_rz00_1061 == BgL_lz00_1059))
									{	/* Ieee/string.scm 544 */
										return (BgL_alz00_1057 - BgL_blz00_1058);
									}
								else
									{	/* Ieee/string.scm 546 */
										long BgL_cz00_1064;

										BgL_cz00_1064 =
											(
											(tolower(STRING_REF(BgL_az00_80, BgL_rz00_1061))) -
											(tolower(STRING_REF(BgL_bz00_81, BgL_rz00_1061))));
										if ((BgL_cz00_1064 == 0L))
											{
												long BgL_rz00_5865;

												BgL_rz00_5865 = (BgL_rz00_1061 + 1L);
												BgL_rz00_1061 = BgL_rz00_5865;
												goto BgL_zc3z04anonymousza31323ze3z87_1062;
											}
										else
											{	/* Ieee/string.scm 548 */
												return BgL_cz00_1064;
											}
									}
							}
						}
					}
				}
			}
		}

	}



/* &string-compare3-ci */
	obj_t BGl_z62stringzd2compare3zd2ciz62zz__r4_strings_6_7z00(obj_t
		BgL_envz00_4417, obj_t BgL_az00_4418, obj_t BgL_bz00_4419)
	{
		{	/* Ieee/string.scm 539 */
			{	/* Ieee/string.scm 540 */
				long BgL_tmpz00_5867;

				{	/* Ieee/string.scm 540 */
					obj_t BgL_auxz00_5875;
					obj_t BgL_auxz00_5868;

					if (STRINGP(BgL_bz00_4419))
						{	/* Ieee/string.scm 540 */
							BgL_auxz00_5875 = BgL_bz00_4419;
						}
					else
						{
							obj_t BgL_auxz00_5878;

							BgL_auxz00_5878 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(25633L),
								BGl_string2692z00zz__r4_strings_6_7z00,
								BGl_string2660z00zz__r4_strings_6_7z00, BgL_bz00_4419);
							FAILURE(BgL_auxz00_5878, BFALSE, BFALSE);
						}
					if (STRINGP(BgL_az00_4418))
						{	/* Ieee/string.scm 540 */
							BgL_auxz00_5868 = BgL_az00_4418;
						}
					else
						{
							obj_t BgL_auxz00_5871;

							BgL_auxz00_5871 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(25633L),
								BGl_string2692z00zz__r4_strings_6_7z00,
								BGl_string2660z00zz__r4_strings_6_7z00, BgL_az00_4418);
							FAILURE(BgL_auxz00_5871, BFALSE, BFALSE);
						}
					BgL_tmpz00_5867 =
						BGl_stringzd2compare3zd2ciz00zz__r4_strings_6_7z00(BgL_auxz00_5868,
						BgL_auxz00_5875);
				}
				return BINT(BgL_tmpz00_5867);
			}
		}

	}



/* string-append */
	BGL_EXPORTED_DEF obj_t BGl_stringzd2appendzd2zz__r4_strings_6_7z00(obj_t
		BgL_listz00_82)
	{
		{	/* Ieee/string.scm 559 */
			if (NULLP(BgL_listz00_82))
				{	/* Ieee/string.scm 560 */
					return BGl_string2693z00zz__r4_strings_6_7z00;
				}
			else
				{	/* Ieee/string.scm 562 */
					long BgL_lenz00_1078;

					{
						obj_t BgL_listz00_2711;
						long BgL_resz00_2712;

						BgL_listz00_2711 = BgL_listz00_82;
						BgL_resz00_2712 = 0L;
					BgL_stringzd2appendzd2_2710:
						if (NULLP(BgL_listz00_2711))
							{	/* Ieee/string.scm 564 */
								BgL_lenz00_1078 = BgL_resz00_2712;
							}
						else
							{
								long BgL_resz00_5891;
								obj_t BgL_listz00_5888;

								BgL_listz00_5888 = CDR(((obj_t) BgL_listz00_2711));
								{	/* Ieee/string.scm 567 */
									long BgL_tmpz00_5892;

									{	/* Ieee/string.scm 568 */
										obj_t BgL_stringz00_2725;

										BgL_stringz00_2725 = CAR(((obj_t) BgL_listz00_2711));
										BgL_tmpz00_5892 = STRING_LENGTH(BgL_stringz00_2725);
									}
									BgL_resz00_5891 = (BgL_resz00_2712 + BgL_tmpz00_5892);
								}
								BgL_resz00_2712 = BgL_resz00_5891;
								BgL_listz00_2711 = BgL_listz00_5888;
								goto BgL_stringzd2appendzd2_2710;
							}
					}
					{	/* Ieee/string.scm 562 */
						obj_t BgL_resz00_1079;

						BgL_resz00_1079 = make_string_sans_fill(BgL_lenz00_1078);
						{	/* Ieee/string.scm 569 */

							{
								obj_t BgL_listz00_2757;
								long BgL_wz00_2758;

								BgL_listz00_2757 = BgL_listz00_82;
								BgL_wz00_2758 = 0L;
							BgL_stringzd2appendzd2_2756:
								if (NULLP(BgL_listz00_2757))
									{	/* Ieee/string.scm 572 */
										return BgL_resz00_1079;
									}
								else
									{	/* Ieee/string.scm 574 */
										obj_t BgL_sz00_2765;

										BgL_sz00_2765 = CAR(((obj_t) BgL_listz00_2757));
										{	/* Ieee/string.scm 574 */
											long BgL_lz00_2766;

											BgL_lz00_2766 = STRING_LENGTH(((obj_t) BgL_sz00_2765));
											{	/* Ieee/string.scm 575 */

												blit_string(
													((obj_t) BgL_sz00_2765), 0L, BgL_resz00_1079,
													BgL_wz00_2758, BgL_lz00_2766);
												{	/* Ieee/string.scm 577 */
													obj_t BgL_arg1338z00_2767;
													long BgL_arg1339z00_2768;

													BgL_arg1338z00_2767 = CDR(((obj_t) BgL_listz00_2757));
													BgL_arg1339z00_2768 = (BgL_wz00_2758 + BgL_lz00_2766);
													{
														long BgL_wz00_5910;
														obj_t BgL_listz00_5909;

														BgL_listz00_5909 = BgL_arg1338z00_2767;
														BgL_wz00_5910 = BgL_arg1339z00_2768;
														BgL_wz00_2758 = BgL_wz00_5910;
														BgL_listz00_2757 = BgL_listz00_5909;
														goto BgL_stringzd2appendzd2_2756;
													}
												}
											}
										}
									}
							}
						}
					}
				}
		}

	}



/* &string-append */
	obj_t BGl_z62stringzd2appendzb0zz__r4_strings_6_7z00(obj_t BgL_envz00_4420,
		obj_t BgL_listz00_4421)
	{
		{	/* Ieee/string.scm 559 */
			return BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_listz00_4421);
		}

	}



/* list->string */
	BGL_EXPORTED_DEF obj_t BGl_listzd2ze3stringz31zz__r4_strings_6_7z00(obj_t
		BgL_listz00_83)
	{
		{	/* Ieee/string.scm 582 */
			{	/* Ieee/string.scm 583 */
				long BgL_lenz00_1100;

				BgL_lenz00_1100 = bgl_list_length(BgL_listz00_83);
				{	/* Ieee/string.scm 583 */
					obj_t BgL_stringz00_1101;

					BgL_stringz00_1101 = make_string_sans_fill(BgL_lenz00_1100);
					{	/* Ieee/string.scm 584 */

						{
							long BgL_iz00_2803;
							obj_t BgL_lz00_2804;

							BgL_iz00_2803 = 0L;
							BgL_lz00_2804 = BgL_listz00_83;
						BgL_loopz00_2802:
							if ((BgL_iz00_2803 == BgL_lenz00_1100))
								{	/* Ieee/string.scm 587 */
									return BgL_stringz00_1101;
								}
							else
								{	/* Ieee/string.scm 587 */
									{	/* Ieee/string.scm 590 */
										obj_t BgL_arg1349z00_2810;

										BgL_arg1349z00_2810 = CAR(((obj_t) BgL_lz00_2804));
										{	/* Ieee/string.scm 349 */
											unsigned char BgL_tmpz00_5918;

											BgL_tmpz00_5918 = CCHAR(BgL_arg1349z00_2810);
											STRING_SET(BgL_stringz00_1101, BgL_iz00_2803,
												BgL_tmpz00_5918);
									}}
									{	/* Ieee/string.scm 591 */
										long BgL_arg1350z00_2811;
										obj_t BgL_arg1351z00_2812;

										BgL_arg1350z00_2811 = (BgL_iz00_2803 + 1L);
										BgL_arg1351z00_2812 = CDR(((obj_t) BgL_lz00_2804));
										{
											obj_t BgL_lz00_5925;
											long BgL_iz00_5924;

											BgL_iz00_5924 = BgL_arg1350z00_2811;
											BgL_lz00_5925 = BgL_arg1351z00_2812;
											BgL_lz00_2804 = BgL_lz00_5925;
											BgL_iz00_2803 = BgL_iz00_5924;
											goto BgL_loopz00_2802;
										}
									}
								}
						}
					}
				}
			}
		}

	}



/* &list->string */
	obj_t BGl_z62listzd2ze3stringz53zz__r4_strings_6_7z00(obj_t BgL_envz00_4422,
		obj_t BgL_listz00_4423)
	{
		{	/* Ieee/string.scm 582 */
			{	/* Ieee/string.scm 583 */
				obj_t BgL_auxz00_5926;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_listz00_4423))
					{	/* Ieee/string.scm 583 */
						BgL_auxz00_5926 = BgL_listz00_4423;
					}
				else
					{
						obj_t BgL_auxz00_5929;

						BgL_auxz00_5929 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2658z00zz__r4_strings_6_7z00, BINT(27318L),
							BGl_string2694z00zz__r4_strings_6_7z00,
							BGl_string2695z00zz__r4_strings_6_7z00, BgL_listz00_4423);
						FAILURE(BgL_auxz00_5929, BFALSE, BFALSE);
					}
				return BGl_listzd2ze3stringz31zz__r4_strings_6_7z00(BgL_auxz00_5926);
			}
		}

	}



/* string->list */
	BGL_EXPORTED_DEF obj_t BGl_stringzd2ze3listz31zz__r4_strings_6_7z00(obj_t
		BgL_stringz00_84)
	{
		{	/* Ieee/string.scm 596 */
			{	/* Ieee/string.scm 597 */
				long BgL_lenz00_1111;

				BgL_lenz00_1111 = STRING_LENGTH(BgL_stringz00_84);
				{	/* Ieee/string.scm 598 */
					long BgL_g1021z00_1112;

					BgL_g1021z00_1112 = (BgL_lenz00_1111 - 1L);
					{
						long BgL_iz00_2840;
						obj_t BgL_resz00_2841;

						BgL_iz00_2840 = BgL_g1021z00_1112;
						BgL_resz00_2841 = BNIL;
					BgL_loopz00_2839:
						if ((BgL_iz00_2840 == -1L))
							{	/* Ieee/string.scm 600 */
								return BgL_resz00_2841;
							}
						else
							{	/* Ieee/string.scm 602 */
								long BgL_arg1354z00_2847;
								obj_t BgL_arg1356z00_2848;

								BgL_arg1354z00_2847 = (BgL_iz00_2840 - 1L);
								{	/* Ieee/string.scm 603 */
									unsigned char BgL_arg1357z00_2849;

									BgL_arg1357z00_2849 =
										STRING_REF(BgL_stringz00_84, BgL_iz00_2840);
									BgL_arg1356z00_2848 =
										MAKE_YOUNG_PAIR(BCHAR(BgL_arg1357z00_2849),
										BgL_resz00_2841);
								}
								{
									obj_t BgL_resz00_5943;
									long BgL_iz00_5942;

									BgL_iz00_5942 = BgL_arg1354z00_2847;
									BgL_resz00_5943 = BgL_arg1356z00_2848;
									BgL_resz00_2841 = BgL_resz00_5943;
									BgL_iz00_2840 = BgL_iz00_5942;
									goto BgL_loopz00_2839;
								}
							}
					}
				}
			}
		}

	}



/* &string->list */
	obj_t BGl_z62stringzd2ze3listz53zz__r4_strings_6_7z00(obj_t BgL_envz00_4424,
		obj_t BgL_stringz00_4425)
	{
		{	/* Ieee/string.scm 596 */
			{	/* Ieee/string.scm 597 */
				obj_t BgL_auxz00_5944;

				if (STRINGP(BgL_stringz00_4425))
					{	/* Ieee/string.scm 597 */
						BgL_auxz00_5944 = BgL_stringz00_4425;
					}
				else
					{
						obj_t BgL_auxz00_5947;

						BgL_auxz00_5947 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2658z00zz__r4_strings_6_7z00, BINT(27792L),
							BGl_string2696z00zz__r4_strings_6_7z00,
							BGl_string2660z00zz__r4_strings_6_7z00, BgL_stringz00_4425);
						FAILURE(BgL_auxz00_5947, BFALSE, BFALSE);
					}
				return BGl_stringzd2ze3listz31zz__r4_strings_6_7z00(BgL_auxz00_5944);
			}
		}

	}



/* string-copy */
	BGL_EXPORTED_DEF obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t
		BgL_stringz00_85)
	{
		{	/* Ieee/string.scm 608 */
			{	/* Ieee/string.scm 609 */
				long BgL_lenz00_1123;

				BgL_lenz00_1123 = STRING_LENGTH(BgL_stringz00_85);
				{	/* Ieee/string.scm 609 */
					obj_t BgL_newz00_1124;

					BgL_newz00_1124 = make_string_sans_fill(BgL_lenz00_1123);
					{	/* Ieee/string.scm 610 */

						{	/* Ieee/string.scm 611 */
							long BgL_g1023z00_1125;

							BgL_g1023z00_1125 = (BgL_lenz00_1123 - 1L);
							{
								long BgL_iz00_1127;

								BgL_iz00_1127 = BgL_g1023z00_1125;
							BgL_zc3z04anonymousza31358ze3z87_1128:
								if ((BgL_iz00_1127 == -1L))
									{	/* Ieee/string.scm 612 */
										return BgL_newz00_1124;
									}
								else
									{	/* Ieee/string.scm 612 */
										{	/* Ieee/string.scm 349 */
											unsigned char BgL_tmpz00_5957;

											BgL_tmpz00_5957 =
												STRING_REF(BgL_stringz00_85, BgL_iz00_1127);
											STRING_SET(BgL_newz00_1124, BgL_iz00_1127,
												BgL_tmpz00_5957);
										}
										{
											long BgL_iz00_5960;

											BgL_iz00_5960 = (BgL_iz00_1127 - 1L);
											BgL_iz00_1127 = BgL_iz00_5960;
											goto BgL_zc3z04anonymousza31358ze3z87_1128;
										}
									}
							}
						}
					}
				}
			}
		}

	}



/* &string-copy */
	obj_t BGl_z62stringzd2copyzb0zz__r4_strings_6_7z00(obj_t BgL_envz00_4426,
		obj_t BgL_stringz00_4427)
	{
		{	/* Ieee/string.scm 608 */
			{	/* Ieee/string.scm 609 */
				obj_t BgL_auxz00_5962;

				if (STRINGP(BgL_stringz00_4427))
					{	/* Ieee/string.scm 609 */
						BgL_auxz00_5962 = BgL_stringz00_4427;
					}
				else
					{
						obj_t BgL_auxz00_5965;

						BgL_auxz00_5965 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2658z00zz__r4_strings_6_7z00, BINT(28227L),
							BGl_string2697z00zz__r4_strings_6_7z00,
							BGl_string2660z00zz__r4_strings_6_7z00, BgL_stringz00_4427);
						FAILURE(BgL_auxz00_5965, BFALSE, BFALSE);
					}
				return BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_auxz00_5962);
			}
		}

	}



/* string-fill! */
	BGL_EXPORTED_DEF obj_t BGl_stringzd2fillz12zc0zz__r4_strings_6_7z00(obj_t
		BgL_stringz00_86, unsigned char BgL_charz00_87)
	{
		{	/* Ieee/string.scm 621 */
			{	/* Ieee/string.scm 622 */
				long BgL_lenz00_2863;

				BgL_lenz00_2863 = STRING_LENGTH(BgL_stringz00_86);
				{
					long BgL_iz00_2886;

					BgL_iz00_2886 = 0L;
				BgL_loopz00_2885:
					if ((BgL_iz00_2886 == BgL_lenz00_2863))
						{	/* Ieee/string.scm 624 */
							return BUNSPEC;
						}
					else
						{	/* Ieee/string.scm 624 */
							STRING_SET(BgL_stringz00_86, BgL_iz00_2886, BgL_charz00_87);
							{
								long BgL_iz00_5974;

								BgL_iz00_5974 = (BgL_iz00_2886 + 1L);
								BgL_iz00_2886 = BgL_iz00_5974;
								goto BgL_loopz00_2885;
							}
						}
				}
			}
		}

	}



/* &string-fill! */
	obj_t BGl_z62stringzd2fillz12za2zz__r4_strings_6_7z00(obj_t BgL_envz00_4428,
		obj_t BgL_stringz00_4429, obj_t BgL_charz00_4430)
	{
		{	/* Ieee/string.scm 621 */
			{	/* Ieee/string.scm 622 */
				unsigned char BgL_auxz00_5983;
				obj_t BgL_auxz00_5976;

				{	/* Ieee/string.scm 622 */
					obj_t BgL_tmpz00_5984;

					if (CHARP(BgL_charz00_4430))
						{	/* Ieee/string.scm 622 */
							BgL_tmpz00_5984 = BgL_charz00_4430;
						}
					else
						{
							obj_t BgL_auxz00_5987;

							BgL_auxz00_5987 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(28709L),
								BGl_string2698z00zz__r4_strings_6_7z00,
								BGl_string2663z00zz__r4_strings_6_7z00, BgL_charz00_4430);
							FAILURE(BgL_auxz00_5987, BFALSE, BFALSE);
						}
					BgL_auxz00_5983 = CCHAR(BgL_tmpz00_5984);
				}
				if (STRINGP(BgL_stringz00_4429))
					{	/* Ieee/string.scm 622 */
						BgL_auxz00_5976 = BgL_stringz00_4429;
					}
				else
					{
						obj_t BgL_auxz00_5979;

						BgL_auxz00_5979 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2658z00zz__r4_strings_6_7z00, BINT(28709L),
							BGl_string2698z00zz__r4_strings_6_7z00,
							BGl_string2660z00zz__r4_strings_6_7z00, BgL_stringz00_4429);
						FAILURE(BgL_auxz00_5979, BFALSE, BFALSE);
					}
				return
					BGl_stringzd2fillz12zc0zz__r4_strings_6_7z00(BgL_auxz00_5976,
					BgL_auxz00_5983);
			}
		}

	}



/* string-upcase */
	BGL_EXPORTED_DEF obj_t BGl_stringzd2upcasezd2zz__r4_strings_6_7z00(obj_t
		BgL_stringz00_88)
	{
		{	/* Ieee/string.scm 633 */
			{	/* Ieee/string.scm 634 */
				long BgL_lenz00_1140;

				BgL_lenz00_1140 = STRING_LENGTH(BgL_stringz00_88);
				{	/* Ieee/string.scm 634 */
					obj_t BgL_resz00_1141;

					BgL_resz00_1141 = make_string_sans_fill(BgL_lenz00_1140);
					{	/* Ieee/string.scm 635 */

						{
							long BgL_iz00_2908;

							BgL_iz00_2908 = 0L;
						BgL_loopz00_2907:
							if ((BgL_iz00_2908 == BgL_lenz00_1140))
								{	/* Ieee/string.scm 637 */
									return BgL_resz00_1141;
								}
							else
								{	/* Ieee/string.scm 637 */
									{	/* Ieee/string.scm 349 */
										unsigned char BgL_tmpz00_5997;

										BgL_tmpz00_5997 =
											toupper(STRING_REF(BgL_stringz00_88, BgL_iz00_2908));
										STRING_SET(BgL_resz00_1141, BgL_iz00_2908, BgL_tmpz00_5997);
									}
									{
										long BgL_iz00_6001;

										BgL_iz00_6001 = (BgL_iz00_2908 + 1L);
										BgL_iz00_2908 = BgL_iz00_6001;
										goto BgL_loopz00_2907;
									}
								}
						}
					}
				}
			}
		}

	}



/* &string-upcase */
	obj_t BGl_z62stringzd2upcasezb0zz__r4_strings_6_7z00(obj_t BgL_envz00_4431,
		obj_t BgL_stringz00_4432)
	{
		{	/* Ieee/string.scm 633 */
			{	/* Ieee/string.scm 634 */
				obj_t BgL_auxz00_6003;

				if (STRINGP(BgL_stringz00_4432))
					{	/* Ieee/string.scm 634 */
						BgL_auxz00_6003 = BgL_stringz00_4432;
					}
				else
					{
						obj_t BgL_auxz00_6006;

						BgL_auxz00_6006 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2658z00zz__r4_strings_6_7z00, BINT(29133L),
							BGl_string2699z00zz__r4_strings_6_7z00,
							BGl_string2660z00zz__r4_strings_6_7z00, BgL_stringz00_4432);
						FAILURE(BgL_auxz00_6006, BFALSE, BFALSE);
					}
				return BGl_stringzd2upcasezd2zz__r4_strings_6_7z00(BgL_auxz00_6003);
			}
		}

	}



/* string-downcase */
	BGL_EXPORTED_DEF obj_t BGl_stringzd2downcasezd2zz__r4_strings_6_7z00(obj_t
		BgL_stringz00_89)
	{
		{	/* Ieee/string.scm 646 */
			{	/* Ieee/string.scm 647 */
				long BgL_lenz00_1150;

				BgL_lenz00_1150 = STRING_LENGTH(BgL_stringz00_89);
				{	/* Ieee/string.scm 647 */
					obj_t BgL_resz00_1151;

					BgL_resz00_1151 = make_string_sans_fill(BgL_lenz00_1150);
					{	/* Ieee/string.scm 648 */

						{
							long BgL_iz00_2933;

							BgL_iz00_2933 = 0L;
						BgL_loopz00_2932:
							if ((BgL_iz00_2933 == BgL_lenz00_1150))
								{	/* Ieee/string.scm 650 */
									return BgL_resz00_1151;
								}
							else
								{	/* Ieee/string.scm 650 */
									{	/* Ieee/string.scm 349 */
										unsigned char BgL_tmpz00_6015;

										BgL_tmpz00_6015 =
											tolower(STRING_REF(BgL_stringz00_89, BgL_iz00_2933));
										STRING_SET(BgL_resz00_1151, BgL_iz00_2933, BgL_tmpz00_6015);
									}
									{
										long BgL_iz00_6019;

										BgL_iz00_6019 = (BgL_iz00_2933 + 1L);
										BgL_iz00_2933 = BgL_iz00_6019;
										goto BgL_loopz00_2932;
									}
								}
						}
					}
				}
			}
		}

	}



/* &string-downcase */
	obj_t BGl_z62stringzd2downcasezb0zz__r4_strings_6_7z00(obj_t BgL_envz00_4433,
		obj_t BgL_stringz00_4434)
	{
		{	/* Ieee/string.scm 646 */
			{	/* Ieee/string.scm 647 */
				obj_t BgL_auxz00_6021;

				if (STRINGP(BgL_stringz00_4434))
					{	/* Ieee/string.scm 647 */
						BgL_auxz00_6021 = BgL_stringz00_4434;
					}
				else
					{
						obj_t BgL_auxz00_6024;

						BgL_auxz00_6024 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2658z00zz__r4_strings_6_7z00, BINT(29618L),
							BGl_string2700z00zz__r4_strings_6_7z00,
							BGl_string2660z00zz__r4_strings_6_7z00, BgL_stringz00_4434);
						FAILURE(BgL_auxz00_6024, BFALSE, BFALSE);
					}
				return BGl_stringzd2downcasezd2zz__r4_strings_6_7z00(BgL_auxz00_6021);
			}
		}

	}



/* string-upcase! */
	BGL_EXPORTED_DEF obj_t BGl_stringzd2upcasez12zc0zz__r4_strings_6_7z00(obj_t
		BgL_stringz00_90)
	{
		{	/* Ieee/string.scm 659 */
			{	/* Ieee/string.scm 660 */
				long BgL_lenz00_1160;

				BgL_lenz00_1160 = STRING_LENGTH(BgL_stringz00_90);
				{
					long BgL_iz00_2958;

					BgL_iz00_2958 = 0L;
				BgL_loopz00_2957:
					if ((BgL_iz00_2958 == BgL_lenz00_1160))
						{	/* Ieee/string.scm 662 */
							return BgL_stringz00_90;
						}
					else
						{	/* Ieee/string.scm 662 */
							{	/* Ieee/string.scm 349 */
								unsigned char BgL_tmpz00_6032;

								BgL_tmpz00_6032 =
									toupper(STRING_REF(BgL_stringz00_90, BgL_iz00_2958));
								STRING_SET(BgL_stringz00_90, BgL_iz00_2958, BgL_tmpz00_6032);
							}
							{
								long BgL_iz00_6036;

								BgL_iz00_6036 = (BgL_iz00_2958 + 1L);
								BgL_iz00_2958 = BgL_iz00_6036;
								goto BgL_loopz00_2957;
							}
						}
				}
			}
		}

	}



/* &string-upcase! */
	obj_t BGl_z62stringzd2upcasez12za2zz__r4_strings_6_7z00(obj_t BgL_envz00_4435,
		obj_t BgL_stringz00_4436)
	{
		{	/* Ieee/string.scm 659 */
			{	/* Ieee/string.scm 660 */
				obj_t BgL_auxz00_6038;

				if (STRINGP(BgL_stringz00_4436))
					{	/* Ieee/string.scm 660 */
						BgL_auxz00_6038 = BgL_stringz00_4436;
					}
				else
					{
						obj_t BgL_auxz00_6041;

						BgL_auxz00_6041 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2658z00zz__r4_strings_6_7z00, BINT(30104L),
							BGl_string2701z00zz__r4_strings_6_7z00,
							BGl_string2660z00zz__r4_strings_6_7z00, BgL_stringz00_4436);
						FAILURE(BgL_auxz00_6041, BFALSE, BFALSE);
					}
				return BGl_stringzd2upcasez12zc0zz__r4_strings_6_7z00(BgL_auxz00_6038);
			}
		}

	}



/* string-downcase! */
	BGL_EXPORTED_DEF obj_t BGl_stringzd2downcasez12zc0zz__r4_strings_6_7z00(obj_t
		BgL_stringz00_91)
	{
		{	/* Ieee/string.scm 671 */
			{	/* Ieee/string.scm 672 */
				long BgL_lenz00_1169;

				BgL_lenz00_1169 = STRING_LENGTH(BgL_stringz00_91);
				{
					long BgL_iz00_2983;

					BgL_iz00_2983 = 0L;
				BgL_loopz00_2982:
					if ((BgL_iz00_2983 == BgL_lenz00_1169))
						{	/* Ieee/string.scm 674 */
							return BgL_stringz00_91;
						}
					else
						{	/* Ieee/string.scm 674 */
							{	/* Ieee/string.scm 349 */
								unsigned char BgL_tmpz00_6049;

								BgL_tmpz00_6049 =
									tolower(STRING_REF(BgL_stringz00_91, BgL_iz00_2983));
								STRING_SET(BgL_stringz00_91, BgL_iz00_2983, BgL_tmpz00_6049);
							}
							{
								long BgL_iz00_6053;

								BgL_iz00_6053 = (BgL_iz00_2983 + 1L);
								BgL_iz00_2983 = BgL_iz00_6053;
								goto BgL_loopz00_2982;
							}
						}
				}
			}
		}

	}



/* &string-downcase! */
	obj_t BGl_z62stringzd2downcasez12za2zz__r4_strings_6_7z00(obj_t
		BgL_envz00_4437, obj_t BgL_stringz00_4438)
	{
		{	/* Ieee/string.scm 671 */
			{	/* Ieee/string.scm 672 */
				obj_t BgL_auxz00_6055;

				if (STRINGP(BgL_stringz00_4438))
					{	/* Ieee/string.scm 672 */
						BgL_auxz00_6055 = BgL_stringz00_4438;
					}
				else
					{
						obj_t BgL_auxz00_6058;

						BgL_auxz00_6058 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2658z00zz__r4_strings_6_7z00, BINT(30559L),
							BGl_string2702z00zz__r4_strings_6_7z00,
							BGl_string2660z00zz__r4_strings_6_7z00, BgL_stringz00_4438);
						FAILURE(BgL_auxz00_6058, BFALSE, BFALSE);
					}
				return
					BGl_stringzd2downcasez12zc0zz__r4_strings_6_7z00(BgL_auxz00_6055);
			}
		}

	}



/* string-capitalize! */
	BGL_EXPORTED_DEF obj_t
		BGl_stringzd2capitaliza7ez12z67zz__r4_strings_6_7z00(obj_t BgL_stringz00_92)
	{
		{	/* Ieee/string.scm 687 */
			{	/* Ieee/string.scm 688 */
				bool_t BgL_nonzd2firstzd2alphaz00_1178;
				long BgL_stringzd2lenzd2_1179;

				BgL_nonzd2firstzd2alphaz00_1178 = ((bool_t) 0);
				BgL_stringzd2lenzd2_1179 = STRING_LENGTH(BgL_stringz00_92);
				{
					long BgL_iz00_1181;

					BgL_iz00_1181 = 0L;
				BgL_zc3z04anonymousza31388ze3z87_1182:
					if ((BgL_iz00_1181 == BgL_stringzd2lenzd2_1179))
						{	/* Ieee/string.scm 690 */
							return BgL_stringz00_92;
						}
					else
						{	/* Ieee/string.scm 690 */
							{	/* Ieee/string.scm 692 */
								unsigned char BgL_cz00_1184;

								BgL_cz00_1184 = STRING_REF(BgL_stringz00_92, BgL_iz00_1181);
								{	/* Ieee/string.scm 693 */
									bool_t BgL_test3101z00_6067;

									if (isalpha(BgL_cz00_1184))
										{	/* Ieee/string.scm 693 */
											BgL_test3101z00_6067 = ((bool_t) 1);
										}
									else
										{	/* Ieee/string.scm 693 */
											BgL_test3101z00_6067 = ((BgL_cz00_1184) > 127L);
										}
									if (BgL_test3101z00_6067)
										{	/* Ieee/string.scm 693 */
											if (BgL_nonzd2firstzd2alphaz00_1178)
												{	/* Ieee/string.scm 349 */
													unsigned char BgL_tmpz00_6073;

													BgL_tmpz00_6073 = tolower(BgL_cz00_1184);
													STRING_SET(BgL_stringz00_92, BgL_iz00_1181,
														BgL_tmpz00_6073);
												}
											else
												{	/* Ieee/string.scm 694 */
													BgL_nonzd2firstzd2alphaz00_1178 = ((bool_t) 1);
													{	/* Ieee/string.scm 349 */
														unsigned char BgL_tmpz00_6076;

														BgL_tmpz00_6076 = toupper(BgL_cz00_1184);
														STRING_SET(BgL_stringz00_92, BgL_iz00_1181,
															BgL_tmpz00_6076);
										}}}
									else
										{	/* Ieee/string.scm 693 */
											BgL_nonzd2firstzd2alphaz00_1178 = ((bool_t) 0);
										}
								}
							}
							{
								long BgL_iz00_6079;

								BgL_iz00_6079 = (BgL_iz00_1181 + 1L);
								BgL_iz00_1181 = BgL_iz00_6079;
								goto BgL_zc3z04anonymousza31388ze3z87_1182;
							}
						}
				}
			}
		}

	}



/* &string-capitalize! */
	obj_t BGl_z62stringzd2capitaliza7ez12z05zz__r4_strings_6_7z00(obj_t
		BgL_envz00_4439, obj_t BgL_stringz00_4440)
	{
		{	/* Ieee/string.scm 687 */
			{	/* Ieee/string.scm 688 */
				obj_t BgL_auxz00_6081;

				if (STRINGP(BgL_stringz00_4440))
					{	/* Ieee/string.scm 688 */
						BgL_auxz00_6081 = BgL_stringz00_4440;
					}
				else
					{
						obj_t BgL_auxz00_6084;

						BgL_auxz00_6084 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2658z00zz__r4_strings_6_7z00, BINT(31314L),
							BGl_string2703z00zz__r4_strings_6_7z00,
							BGl_string2660z00zz__r4_strings_6_7z00, BgL_stringz00_4440);
						FAILURE(BgL_auxz00_6084, BFALSE, BFALSE);
					}
				return
					BGl_stringzd2capitaliza7ez12z67zz__r4_strings_6_7z00(BgL_auxz00_6081);
			}
		}

	}



/* string-capitalize */
	BGL_EXPORTED_DEF obj_t BGl_stringzd2capitaliza7ez75zz__r4_strings_6_7z00(obj_t
		BgL_stringz00_93)
	{
		{	/* Ieee/string.scm 704 */
			BGL_TAIL return
				BGl_stringzd2capitaliza7ez12z67zz__r4_strings_6_7z00
				(BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_stringz00_93));
		}

	}



/* &string-capitalize */
	obj_t BGl_z62stringzd2capitaliza7ez17zz__r4_strings_6_7z00(obj_t
		BgL_envz00_4441, obj_t BgL_stringz00_4442)
	{
		{	/* Ieee/string.scm 704 */
			{	/* Ieee/string.scm 705 */
				obj_t BgL_auxz00_6091;

				if (STRINGP(BgL_stringz00_4442))
					{	/* Ieee/string.scm 705 */
						BgL_auxz00_6091 = BgL_stringz00_4442;
					}
				else
					{
						obj_t BgL_auxz00_6094;

						BgL_auxz00_6094 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2658z00zz__r4_strings_6_7z00, BINT(32038L),
							BGl_string2704z00zz__r4_strings_6_7z00,
							BGl_string2660z00zz__r4_strings_6_7z00, BgL_stringz00_4442);
						FAILURE(BgL_auxz00_6094, BFALSE, BFALSE);
					}
				return
					BGl_stringzd2capitaliza7ez75zz__r4_strings_6_7z00(BgL_auxz00_6091);
			}
		}

	}



/* string-for-read */
	BGL_EXPORTED_DEF obj_t BGl_stringzd2forzd2readz00zz__r4_strings_6_7z00(obj_t
		BgL_stringz00_94)
	{
		{	/* Ieee/string.scm 710 */
			BGL_TAIL return string_for_read(BgL_stringz00_94);
		}

	}



/* &string-for-read */
	obj_t BGl_z62stringzd2forzd2readz62zz__r4_strings_6_7z00(obj_t
		BgL_envz00_4443, obj_t BgL_stringz00_4444)
	{
		{	/* Ieee/string.scm 710 */
			{	/* Ieee/string.scm 711 */
				obj_t BgL_auxz00_6100;

				if (STRINGP(BgL_stringz00_4444))
					{	/* Ieee/string.scm 711 */
						BgL_auxz00_6100 = BgL_stringz00_4444;
					}
				else
					{
						obj_t BgL_auxz00_6103;

						BgL_auxz00_6103 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2658z00zz__r4_strings_6_7z00, BINT(32327L),
							BGl_string2705z00zz__r4_strings_6_7z00,
							BGl_string2660z00zz__r4_strings_6_7z00, BgL_stringz00_4444);
						FAILURE(BgL_auxz00_6103, BFALSE, BFALSE);
					}
				return BGl_stringzd2forzd2readz00zz__r4_strings_6_7z00(BgL_auxz00_6100);
			}
		}

	}



/* escape-C-string */
	BGL_EXPORTED_DEF obj_t BGl_escapezd2Czd2stringz00zz__r4_strings_6_7z00(obj_t
		BgL_strz00_95)
	{
		{	/* Ieee/string.scm 718 */
			{	/* Ieee/string.scm 719 */
				obj_t BgL_arg1399z00_4996;

				{	/* Ieee/string.scm 194 */
					long BgL_endz00_4997;

					BgL_endz00_4997 = STRING_LENGTH(BgL_strz00_95);
					{	/* Ieee/string.scm 194 */

						BgL_arg1399z00_4996 =
							BGl_substringz00zz__r4_strings_6_7z00(BgL_strz00_95, 1L,
							BgL_endz00_4997);
				}}
				return
					bgl_escape_C_string(BSTRING_TO_STRING(BgL_arg1399z00_4996), 0L,
					STRING_LENGTH(BgL_arg1399z00_4996));
			}
		}

	}



/* &escape-C-string */
	obj_t BGl_z62escapezd2Czd2stringz62zz__r4_strings_6_7z00(obj_t
		BgL_envz00_4445, obj_t BgL_strz00_4446)
	{
		{	/* Ieee/string.scm 718 */
			{	/* Ieee/string.scm 719 */
				obj_t BgL_auxz00_6113;

				if (STRINGP(BgL_strz00_4446))
					{	/* Ieee/string.scm 719 */
						BgL_auxz00_6113 = BgL_strz00_4446;
					}
				else
					{
						obj_t BgL_auxz00_6116;

						BgL_auxz00_6116 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2658z00zz__r4_strings_6_7z00, BINT(32781L),
							BGl_string2706z00zz__r4_strings_6_7z00,
							BGl_string2660z00zz__r4_strings_6_7z00, BgL_strz00_4446);
						FAILURE(BgL_auxz00_6116, BFALSE, BFALSE);
					}
				return BGl_escapezd2Czd2stringz00zz__r4_strings_6_7z00(BgL_auxz00_6113);
			}
		}

	}



/* escape-scheme-string */
	BGL_EXPORTED_DEF obj_t
		BGl_escapezd2schemezd2stringz00zz__r4_strings_6_7z00(obj_t BgL_strz00_96)
	{
		{	/* Ieee/string.scm 726 */
			return
				bgl_escape_scheme_string(BSTRING_TO_STRING(BgL_strz00_96), 0L,
				STRING_LENGTH(BgL_strz00_96));
		}

	}



/* &escape-scheme-string */
	obj_t BGl_z62escapezd2schemezd2stringz62zz__r4_strings_6_7z00(obj_t
		BgL_envz00_4447, obj_t BgL_strz00_4448)
	{
		{	/* Ieee/string.scm 726 */
			{	/* Ieee/string.scm 727 */
				obj_t BgL_auxz00_6124;

				if (STRINGP(BgL_strz00_4448))
					{	/* Ieee/string.scm 727 */
						BgL_auxz00_6124 = BgL_strz00_4448;
					}
				else
					{
						obj_t BgL_auxz00_6127;

						BgL_auxz00_6127 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2658z00zz__r4_strings_6_7z00, BINT(33246L),
							BGl_string2707z00zz__r4_strings_6_7z00,
							BGl_string2660z00zz__r4_strings_6_7z00, BgL_strz00_4448);
						FAILURE(BgL_auxz00_6127, BFALSE, BFALSE);
					}
				return
					BGl_escapezd2schemezd2stringz00zz__r4_strings_6_7z00(BgL_auxz00_6124);
			}
		}

	}



/* string-as-read */
	BGL_EXPORTED_DEF obj_t BGl_stringzd2aszd2readz00zz__r4_strings_6_7z00(obj_t
		BgL_strz00_97)
	{
		{	/* Ieee/string.scm 732 */
			return
				bgl_escape_C_string(BSTRING_TO_STRING(BgL_strz00_97), 0L,
				STRING_LENGTH(BgL_strz00_97));
		}

	}



/* &string-as-read */
	obj_t BGl_z62stringzd2aszd2readz62zz__r4_strings_6_7z00(obj_t BgL_envz00_4449,
		obj_t BgL_strz00_4450)
	{
		{	/* Ieee/string.scm 732 */
			{	/* Ieee/string.scm 733 */
				obj_t BgL_auxz00_6135;

				if (STRINGP(BgL_strz00_4450))
					{	/* Ieee/string.scm 733 */
						BgL_auxz00_6135 = BgL_strz00_4450;
					}
				else
					{
						obj_t BgL_auxz00_6138;

						BgL_auxz00_6138 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2658z00zz__r4_strings_6_7z00, BINT(33554L),
							BGl_string2708z00zz__r4_strings_6_7z00,
							BGl_string2660z00zz__r4_strings_6_7z00, BgL_strz00_4450);
						FAILURE(BgL_auxz00_6138, BFALSE, BFALSE);
					}
				return BGl_stringzd2aszd2readz00zz__r4_strings_6_7z00(BgL_auxz00_6135);
			}
		}

	}



/* blit-string-ur! */
	BGL_EXPORTED_DEF obj_t BGl_blitzd2stringzd2urz12z12zz__r4_strings_6_7z00(obj_t
		BgL_s1z00_98, long BgL_o1z00_99, obj_t BgL_s2z00_100, long BgL_o2z00_101,
		long BgL_lz00_102)
	{
		{	/* Ieee/string.scm 738 */
			BGL_TAIL return
				blit_string(BgL_s1z00_98, BgL_o1z00_99, BgL_s2z00_100, BgL_o2z00_101,
				BgL_lz00_102);
		}

	}



/* &blit-string-ur! */
	obj_t BGl_z62blitzd2stringzd2urz12z70zz__r4_strings_6_7z00(obj_t
		BgL_envz00_4451, obj_t BgL_s1z00_4452, obj_t BgL_o1z00_4453,
		obj_t BgL_s2z00_4454, obj_t BgL_o2z00_4455, obj_t BgL_lz00_4456)
	{
		{	/* Ieee/string.scm 738 */
			{	/* Ieee/string.scm 739 */
				long BgL_auxz00_6176;
				long BgL_auxz00_6167;
				obj_t BgL_auxz00_6160;
				long BgL_auxz00_6151;
				obj_t BgL_auxz00_6144;

				{	/* Ieee/string.scm 739 */
					obj_t BgL_tmpz00_6177;

					if (INTEGERP(BgL_lz00_4456))
						{	/* Ieee/string.scm 739 */
							BgL_tmpz00_6177 = BgL_lz00_4456;
						}
					else
						{
							obj_t BgL_auxz00_6180;

							BgL_auxz00_6180 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(33849L),
								BGl_string2709z00zz__r4_strings_6_7z00,
								BGl_string2662z00zz__r4_strings_6_7z00, BgL_lz00_4456);
							FAILURE(BgL_auxz00_6180, BFALSE, BFALSE);
						}
					BgL_auxz00_6176 = (long) CINT(BgL_tmpz00_6177);
				}
				{	/* Ieee/string.scm 739 */
					obj_t BgL_tmpz00_6168;

					if (INTEGERP(BgL_o2z00_4455))
						{	/* Ieee/string.scm 739 */
							BgL_tmpz00_6168 = BgL_o2z00_4455;
						}
					else
						{
							obj_t BgL_auxz00_6171;

							BgL_auxz00_6171 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(33849L),
								BGl_string2709z00zz__r4_strings_6_7z00,
								BGl_string2662z00zz__r4_strings_6_7z00, BgL_o2z00_4455);
							FAILURE(BgL_auxz00_6171, BFALSE, BFALSE);
						}
					BgL_auxz00_6167 = (long) CINT(BgL_tmpz00_6168);
				}
				if (STRINGP(BgL_s2z00_4454))
					{	/* Ieee/string.scm 739 */
						BgL_auxz00_6160 = BgL_s2z00_4454;
					}
				else
					{
						obj_t BgL_auxz00_6163;

						BgL_auxz00_6163 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2658z00zz__r4_strings_6_7z00, BINT(33849L),
							BGl_string2709z00zz__r4_strings_6_7z00,
							BGl_string2660z00zz__r4_strings_6_7z00, BgL_s2z00_4454);
						FAILURE(BgL_auxz00_6163, BFALSE, BFALSE);
					}
				{	/* Ieee/string.scm 739 */
					obj_t BgL_tmpz00_6152;

					if (INTEGERP(BgL_o1z00_4453))
						{	/* Ieee/string.scm 739 */
							BgL_tmpz00_6152 = BgL_o1z00_4453;
						}
					else
						{
							obj_t BgL_auxz00_6155;

							BgL_auxz00_6155 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(33849L),
								BGl_string2709z00zz__r4_strings_6_7z00,
								BGl_string2662z00zz__r4_strings_6_7z00, BgL_o1z00_4453);
							FAILURE(BgL_auxz00_6155, BFALSE, BFALSE);
						}
					BgL_auxz00_6151 = (long) CINT(BgL_tmpz00_6152);
				}
				if (STRINGP(BgL_s1z00_4452))
					{	/* Ieee/string.scm 739 */
						BgL_auxz00_6144 = BgL_s1z00_4452;
					}
				else
					{
						obj_t BgL_auxz00_6147;

						BgL_auxz00_6147 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2658z00zz__r4_strings_6_7z00, BINT(33849L),
							BGl_string2709z00zz__r4_strings_6_7z00,
							BGl_string2660z00zz__r4_strings_6_7z00, BgL_s1z00_4452);
						FAILURE(BgL_auxz00_6147, BFALSE, BFALSE);
					}
				return
					BGl_blitzd2stringzd2urz12z12zz__r4_strings_6_7z00(BgL_auxz00_6144,
					BgL_auxz00_6151, BgL_auxz00_6160, BgL_auxz00_6167, BgL_auxz00_6176);
			}
		}

	}



/* blit-string! */
	BGL_EXPORTED_DEF obj_t BGl_blitzd2stringz12zc0zz__r4_strings_6_7z00(obj_t
		BgL_s1z00_103, long BgL_o1z00_104, obj_t BgL_s2z00_105, long BgL_o2z00_106,
		long BgL_lz00_107)
	{
		{	/* Ieee/string.scm 744 */
			{	/* Ieee/string.scm 745 */
				bool_t BgL_test3115z00_6186;

				{	/* Ieee/string.scm 745 */
					bool_t BgL_test3116z00_6187;

					{	/* Ieee/string.scm 745 */
						long BgL_auxz00_6190;
						long BgL_tmpz00_6188;

						BgL_auxz00_6190 = (STRING_LENGTH(BgL_s1z00_103) + 1L);
						BgL_tmpz00_6188 = (BgL_lz00_107 + BgL_o1z00_104);
						BgL_test3116z00_6187 =
							BOUND_CHECK(BgL_tmpz00_6188, BgL_auxz00_6190);
					}
					if (BgL_test3116z00_6187)
						{	/* Ieee/string.scm 746 */
							long BgL_auxz00_6196;
							long BgL_tmpz00_6194;

							BgL_auxz00_6196 = (STRING_LENGTH(BgL_s2z00_105) + 1L);
							BgL_tmpz00_6194 = (BgL_lz00_107 + BgL_o2z00_106);
							BgL_test3115z00_6186 =
								BOUND_CHECK(BgL_tmpz00_6194, BgL_auxz00_6196);
						}
					else
						{	/* Ieee/string.scm 745 */
							BgL_test3115z00_6186 = ((bool_t) 0);
						}
				}
				if (BgL_test3115z00_6186)
					{	/* Ieee/string.scm 745 */
						BGL_TAIL return
							blit_string(BgL_s1z00_103, BgL_o1z00_104, BgL_s2z00_105,
							BgL_o2z00_106, BgL_lz00_107);
					}
				else
					{	/* Ieee/string.scm 749 */
						obj_t BgL_arg1414z00_1212;
						obj_t BgL_arg1415z00_1213;

						{	/* Ieee/string.scm 749 */
							obj_t BgL_list1416z00_1214;

							{	/* Ieee/string.scm 749 */
								obj_t BgL_arg1417z00_1215;

								{	/* Ieee/string.scm 749 */
									obj_t BgL_arg1418z00_1216;

									{	/* Ieee/string.scm 749 */
										obj_t BgL_arg1419z00_1217;

										{	/* Ieee/string.scm 749 */
											obj_t BgL_arg1420z00_1218;

											BgL_arg1420z00_1218 =
												MAKE_YOUNG_PAIR(BGl_string2710z00zz__r4_strings_6_7z00,
												BNIL);
											BgL_arg1419z00_1217 =
												MAKE_YOUNG_PAIR(BgL_s2z00_105, BgL_arg1420z00_1218);
										}
										BgL_arg1418z00_1216 =
											MAKE_YOUNG_PAIR(BGl_string2711z00zz__r4_strings_6_7z00,
											BgL_arg1419z00_1217);
									}
									BgL_arg1417z00_1215 =
										MAKE_YOUNG_PAIR(BgL_s1z00_103, BgL_arg1418z00_1216);
								}
								BgL_list1416z00_1214 =
									MAKE_YOUNG_PAIR(BGl_string2712z00zz__r4_strings_6_7z00,
									BgL_arg1417z00_1215);
							}
							BgL_arg1414z00_1212 =
								BGl_stringzd2appendzd2zz__r4_strings_6_7z00
								(BgL_list1416z00_1214);
						}
						{	/* Ieee/string.scm 750 */
							long BgL_arg1421z00_1219;
							long BgL_arg1422z00_1220;

							BgL_arg1421z00_1219 = STRING_LENGTH(BgL_s1z00_103);
							BgL_arg1422z00_1220 = STRING_LENGTH(BgL_s2z00_105);
							{	/* Ieee/string.scm 750 */
								obj_t BgL_list1423z00_1221;

								{	/* Ieee/string.scm 750 */
									obj_t BgL_arg1424z00_1222;

									{	/* Ieee/string.scm 750 */
										obj_t BgL_arg1425z00_1223;

										{	/* Ieee/string.scm 750 */
											obj_t BgL_arg1426z00_1224;

											{	/* Ieee/string.scm 750 */
												obj_t BgL_arg1427z00_1225;

												BgL_arg1427z00_1225 =
													MAKE_YOUNG_PAIR(BINT(BgL_lz00_107), BNIL);
												BgL_arg1426z00_1224 =
													MAKE_YOUNG_PAIR(BINT(BgL_o2z00_106),
													BgL_arg1427z00_1225);
											}
											BgL_arg1425z00_1223 =
												MAKE_YOUNG_PAIR(BINT(BgL_arg1422z00_1220),
												BgL_arg1426z00_1224);
										}
										BgL_arg1424z00_1222 =
											MAKE_YOUNG_PAIR(BINT(BgL_o1z00_104), BgL_arg1425z00_1223);
									}
									BgL_list1423z00_1221 =
										MAKE_YOUNG_PAIR(BINT(BgL_arg1421z00_1219),
										BgL_arg1424z00_1222);
								}
								BgL_arg1415z00_1213 = BgL_list1423z00_1221;
						}}
						return
							BGl_errorz00zz__errorz00(BGl_string2713z00zz__r4_strings_6_7z00,
							BgL_arg1414z00_1212, BgL_arg1415z00_1213);
					}
			}
		}

	}



/* &blit-string! */
	obj_t BGl_z62blitzd2stringz12za2zz__r4_strings_6_7z00(obj_t BgL_envz00_4457,
		obj_t BgL_s1z00_4458, obj_t BgL_o1z00_4459, obj_t BgL_s2z00_4460,
		obj_t BgL_o2z00_4461, obj_t BgL_lz00_4462)
	{
		{	/* Ieee/string.scm 744 */
			{	/* Ieee/string.scm 745 */
				long BgL_auxz00_6252;
				long BgL_auxz00_6243;
				obj_t BgL_auxz00_6236;
				long BgL_auxz00_6227;
				obj_t BgL_auxz00_6220;

				{	/* Ieee/string.scm 745 */
					obj_t BgL_tmpz00_6253;

					if (INTEGERP(BgL_lz00_4462))
						{	/* Ieee/string.scm 745 */
							BgL_tmpz00_6253 = BgL_lz00_4462;
						}
					else
						{
							obj_t BgL_auxz00_6256;

							BgL_auxz00_6256 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(34142L),
								BGl_string2714z00zz__r4_strings_6_7z00,
								BGl_string2662z00zz__r4_strings_6_7z00, BgL_lz00_4462);
							FAILURE(BgL_auxz00_6256, BFALSE, BFALSE);
						}
					BgL_auxz00_6252 = (long) CINT(BgL_tmpz00_6253);
				}
				{	/* Ieee/string.scm 745 */
					obj_t BgL_tmpz00_6244;

					if (INTEGERP(BgL_o2z00_4461))
						{	/* Ieee/string.scm 745 */
							BgL_tmpz00_6244 = BgL_o2z00_4461;
						}
					else
						{
							obj_t BgL_auxz00_6247;

							BgL_auxz00_6247 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(34142L),
								BGl_string2714z00zz__r4_strings_6_7z00,
								BGl_string2662z00zz__r4_strings_6_7z00, BgL_o2z00_4461);
							FAILURE(BgL_auxz00_6247, BFALSE, BFALSE);
						}
					BgL_auxz00_6243 = (long) CINT(BgL_tmpz00_6244);
				}
				if (STRINGP(BgL_s2z00_4460))
					{	/* Ieee/string.scm 745 */
						BgL_auxz00_6236 = BgL_s2z00_4460;
					}
				else
					{
						obj_t BgL_auxz00_6239;

						BgL_auxz00_6239 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2658z00zz__r4_strings_6_7z00, BINT(34142L),
							BGl_string2714z00zz__r4_strings_6_7z00,
							BGl_string2660z00zz__r4_strings_6_7z00, BgL_s2z00_4460);
						FAILURE(BgL_auxz00_6239, BFALSE, BFALSE);
					}
				{	/* Ieee/string.scm 745 */
					obj_t BgL_tmpz00_6228;

					if (INTEGERP(BgL_o1z00_4459))
						{	/* Ieee/string.scm 745 */
							BgL_tmpz00_6228 = BgL_o1z00_4459;
						}
					else
						{
							obj_t BgL_auxz00_6231;

							BgL_auxz00_6231 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(34142L),
								BGl_string2714z00zz__r4_strings_6_7z00,
								BGl_string2662z00zz__r4_strings_6_7z00, BgL_o1z00_4459);
							FAILURE(BgL_auxz00_6231, BFALSE, BFALSE);
						}
					BgL_auxz00_6227 = (long) CINT(BgL_tmpz00_6228);
				}
				if (STRINGP(BgL_s1z00_4458))
					{	/* Ieee/string.scm 745 */
						BgL_auxz00_6220 = BgL_s1z00_4458;
					}
				else
					{
						obj_t BgL_auxz00_6223;

						BgL_auxz00_6223 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2658z00zz__r4_strings_6_7z00, BINT(34142L),
							BGl_string2714z00zz__r4_strings_6_7z00,
							BGl_string2660z00zz__r4_strings_6_7z00, BgL_s1z00_4458);
						FAILURE(BgL_auxz00_6223, BFALSE, BFALSE);
					}
				return
					BGl_blitzd2stringz12zc0zz__r4_strings_6_7z00(BgL_auxz00_6220,
					BgL_auxz00_6227, BgL_auxz00_6236, BgL_auxz00_6243, BgL_auxz00_6252);
			}
		}

	}



/* string-shrink! */
	BGL_EXPORTED_DEF obj_t BGl_stringzd2shrinkz12zc0zz__r4_strings_6_7z00(obj_t
		BgL_sz00_108, long BgL_lz00_109)
	{
		{	/* Ieee/string.scm 755 */
			return bgl_string_shrink(BgL_sz00_108, BgL_lz00_109);
		}

	}



/* &string-shrink! */
	obj_t BGl_z62stringzd2shrinkz12za2zz__r4_strings_6_7z00(obj_t BgL_envz00_4463,
		obj_t BgL_sz00_4464, obj_t BgL_lz00_4465)
	{
		{	/* Ieee/string.scm 755 */
			{	/* Ieee/string.scm 756 */
				long BgL_auxz00_6270;
				obj_t BgL_auxz00_6263;

				{	/* Ieee/string.scm 756 */
					obj_t BgL_tmpz00_6271;

					if (INTEGERP(BgL_lz00_4465))
						{	/* Ieee/string.scm 756 */
							BgL_tmpz00_6271 = BgL_lz00_4465;
						}
					else
						{
							obj_t BgL_auxz00_6274;

							BgL_auxz00_6274 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(34751L),
								BGl_string2715z00zz__r4_strings_6_7z00,
								BGl_string2662z00zz__r4_strings_6_7z00, BgL_lz00_4465);
							FAILURE(BgL_auxz00_6274, BFALSE, BFALSE);
						}
					BgL_auxz00_6270 = (long) CINT(BgL_tmpz00_6271);
				}
				if (STRINGP(BgL_sz00_4464))
					{	/* Ieee/string.scm 756 */
						BgL_auxz00_6263 = BgL_sz00_4464;
					}
				else
					{
						obj_t BgL_auxz00_6266;

						BgL_auxz00_6266 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2658z00zz__r4_strings_6_7z00, BINT(34751L),
							BGl_string2715z00zz__r4_strings_6_7z00,
							BGl_string2660z00zz__r4_strings_6_7z00, BgL_sz00_4464);
						FAILURE(BgL_auxz00_6266, BFALSE, BFALSE);
					}
				return
					BGl_stringzd2shrinkz12zc0zz__r4_strings_6_7z00(BgL_auxz00_6263,
					BgL_auxz00_6270);
			}
		}

	}



/* string-replace */
	BGL_EXPORTED_DEF obj_t BGl_stringzd2replacezd2zz__r4_strings_6_7z00(obj_t
		BgL_strz00_110, unsigned char BgL_c1z00_111, unsigned char BgL_c2z00_112)
	{
		{	/* Ieee/string.scm 761 */
			{	/* Ieee/string.scm 762 */
				long BgL_lenz00_1233;

				BgL_lenz00_1233 = STRING_LENGTH(BgL_strz00_110);
				{	/* Ieee/string.scm 762 */
					obj_t BgL_newz00_1234;

					{	/* Ieee/string.scm 172 */

						BgL_newz00_1234 =
							make_string(BgL_lenz00_1233, ((unsigned char) ' '));
					}
					{	/* Ieee/string.scm 763 */

						{
							long BgL_iz00_1236;

							BgL_iz00_1236 = 0L;
						BgL_zc3z04anonymousza31436ze3z87_1237:
							if ((BgL_iz00_1236 == BgL_lenz00_1233))
								{	/* Ieee/string.scm 765 */
									return BgL_newz00_1234;
								}
							else
								{	/* Ieee/string.scm 767 */
									unsigned char BgL_cz00_1239;

									BgL_cz00_1239 = STRING_REF(BgL_strz00_110, BgL_iz00_1236);
									if ((BgL_cz00_1239 == (unsigned char) (BgL_c1z00_111)))
										{	/* Ieee/string.scm 349 */
											unsigned char BgL_tmpz00_6288;

											BgL_tmpz00_6288 = (unsigned char) (BgL_c2z00_112);
											STRING_SET(BgL_newz00_1234, BgL_iz00_1236,
												BgL_tmpz00_6288);
										}
									else
										{	/* Ieee/string.scm 768 */
											STRING_SET(BgL_newz00_1234, BgL_iz00_1236, BgL_cz00_1239);
										}
									{
										long BgL_iz00_6292;

										BgL_iz00_6292 = (BgL_iz00_1236 + 1L);
										BgL_iz00_1236 = BgL_iz00_6292;
										goto BgL_zc3z04anonymousza31436ze3z87_1237;
									}
								}
						}
					}
				}
			}
		}

	}



/* &string-replace */
	obj_t BGl_z62stringzd2replacezb0zz__r4_strings_6_7z00(obj_t BgL_envz00_4466,
		obj_t BgL_strz00_4467, obj_t BgL_c1z00_4468, obj_t BgL_c2z00_4469)
	{
		{	/* Ieee/string.scm 761 */
			{	/* Ieee/string.scm 762 */
				unsigned char BgL_auxz00_6310;
				unsigned char BgL_auxz00_6301;
				obj_t BgL_auxz00_6294;

				{	/* Ieee/string.scm 762 */
					obj_t BgL_tmpz00_6311;

					if (CHARP(BgL_c2z00_4469))
						{	/* Ieee/string.scm 762 */
							BgL_tmpz00_6311 = BgL_c2z00_4469;
						}
					else
						{
							obj_t BgL_auxz00_6314;

							BgL_auxz00_6314 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(35035L),
								BGl_string2716z00zz__r4_strings_6_7z00,
								BGl_string2663z00zz__r4_strings_6_7z00, BgL_c2z00_4469);
							FAILURE(BgL_auxz00_6314, BFALSE, BFALSE);
						}
					BgL_auxz00_6310 = CCHAR(BgL_tmpz00_6311);
				}
				{	/* Ieee/string.scm 762 */
					obj_t BgL_tmpz00_6302;

					if (CHARP(BgL_c1z00_4468))
						{	/* Ieee/string.scm 762 */
							BgL_tmpz00_6302 = BgL_c1z00_4468;
						}
					else
						{
							obj_t BgL_auxz00_6305;

							BgL_auxz00_6305 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(35035L),
								BGl_string2716z00zz__r4_strings_6_7z00,
								BGl_string2663z00zz__r4_strings_6_7z00, BgL_c1z00_4468);
							FAILURE(BgL_auxz00_6305, BFALSE, BFALSE);
						}
					BgL_auxz00_6301 = CCHAR(BgL_tmpz00_6302);
				}
				if (STRINGP(BgL_strz00_4467))
					{	/* Ieee/string.scm 762 */
						BgL_auxz00_6294 = BgL_strz00_4467;
					}
				else
					{
						obj_t BgL_auxz00_6297;

						BgL_auxz00_6297 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2658z00zz__r4_strings_6_7z00, BINT(35035L),
							BGl_string2716z00zz__r4_strings_6_7z00,
							BGl_string2660z00zz__r4_strings_6_7z00, BgL_strz00_4467);
						FAILURE(BgL_auxz00_6297, BFALSE, BFALSE);
					}
				return
					BGl_stringzd2replacezd2zz__r4_strings_6_7z00(BgL_auxz00_6294,
					BgL_auxz00_6301, BgL_auxz00_6310);
			}
		}

	}



/* string-replace! */
	BGL_EXPORTED_DEF obj_t BGl_stringzd2replacez12zc0zz__r4_strings_6_7z00(obj_t
		BgL_strz00_113, unsigned char BgL_c1z00_114, unsigned char BgL_c2z00_115)
	{
		{	/* Ieee/string.scm 776 */
			{	/* Ieee/string.scm 777 */
				long BgL_lenz00_1245;

				BgL_lenz00_1245 = STRING_LENGTH(BgL_strz00_113);
				{
					long BgL_iz00_1247;

					BgL_iz00_1247 = 0L;
				BgL_zc3z04anonymousza31440ze3z87_1248:
					if ((BgL_iz00_1247 == BgL_lenz00_1245))
						{	/* Ieee/string.scm 780 */
							return BgL_strz00_113;
						}
					else
						{	/* Ieee/string.scm 780 */
							if (
								(STRING_REF(BgL_strz00_113, BgL_iz00_1247) ==
									(unsigned char) (BgL_c1z00_114)))
								{	/* Ieee/string.scm 782 */
									{	/* Ieee/string.scm 349 */
										unsigned char BgL_tmpz00_6327;

										BgL_tmpz00_6327 = (unsigned char) (BgL_c2z00_115);
										STRING_SET(BgL_strz00_113, BgL_iz00_1247, BgL_tmpz00_6327);
									}
									{
										long BgL_iz00_6330;

										BgL_iz00_6330 = (BgL_iz00_1247 + 1L);
										BgL_iz00_1247 = BgL_iz00_6330;
										goto BgL_zc3z04anonymousza31440ze3z87_1248;
									}
								}
							else
								{
									long BgL_iz00_6332;

									BgL_iz00_6332 = (BgL_iz00_1247 + 1L);
									BgL_iz00_1247 = BgL_iz00_6332;
									goto BgL_zc3z04anonymousza31440ze3z87_1248;
								}
						}
				}
			}
		}

	}



/* &string-replace! */
	obj_t BGl_z62stringzd2replacez12za2zz__r4_strings_6_7z00(obj_t
		BgL_envz00_4470, obj_t BgL_strz00_4471, obj_t BgL_c1z00_4472,
		obj_t BgL_c2z00_4473)
	{
		{	/* Ieee/string.scm 776 */
			{	/* Ieee/string.scm 777 */
				unsigned char BgL_auxz00_6350;
				unsigned char BgL_auxz00_6341;
				obj_t BgL_auxz00_6334;

				{	/* Ieee/string.scm 777 */
					obj_t BgL_tmpz00_6351;

					if (CHARP(BgL_c2z00_4473))
						{	/* Ieee/string.scm 777 */
							BgL_tmpz00_6351 = BgL_c2z00_4473;
						}
					else
						{
							obj_t BgL_auxz00_6354;

							BgL_auxz00_6354 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(35557L),
								BGl_string2717z00zz__r4_strings_6_7z00,
								BGl_string2663z00zz__r4_strings_6_7z00, BgL_c2z00_4473);
							FAILURE(BgL_auxz00_6354, BFALSE, BFALSE);
						}
					BgL_auxz00_6350 = CCHAR(BgL_tmpz00_6351);
				}
				{	/* Ieee/string.scm 777 */
					obj_t BgL_tmpz00_6342;

					if (CHARP(BgL_c1z00_4472))
						{	/* Ieee/string.scm 777 */
							BgL_tmpz00_6342 = BgL_c1z00_4472;
						}
					else
						{
							obj_t BgL_auxz00_6345;

							BgL_auxz00_6345 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(35557L),
								BGl_string2717z00zz__r4_strings_6_7z00,
								BGl_string2663z00zz__r4_strings_6_7z00, BgL_c1z00_4472);
							FAILURE(BgL_auxz00_6345, BFALSE, BFALSE);
						}
					BgL_auxz00_6341 = CCHAR(BgL_tmpz00_6342);
				}
				if (STRINGP(BgL_strz00_4471))
					{	/* Ieee/string.scm 777 */
						BgL_auxz00_6334 = BgL_strz00_4471;
					}
				else
					{
						obj_t BgL_auxz00_6337;

						BgL_auxz00_6337 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2658z00zz__r4_strings_6_7z00, BINT(35557L),
							BGl_string2717z00zz__r4_strings_6_7z00,
							BGl_string2660z00zz__r4_strings_6_7z00, BgL_strz00_4471);
						FAILURE(BgL_auxz00_6337, BFALSE, BFALSE);
					}
				return
					BGl_stringzd2replacez12zc0zz__r4_strings_6_7z00(BgL_auxz00_6334,
					BgL_auxz00_6341, BgL_auxz00_6350);
			}
		}

	}



/* _string-delete */
	obj_t BGl__stringzd2deletezd2zz__r4_strings_6_7z00(obj_t BgL_env1129z00_121,
		obj_t BgL_opt1128z00_120)
	{
		{	/* Ieee/string.scm 791 */
			{	/* Ieee/string.scm 791 */
				obj_t BgL_stringz00_1256;
				obj_t BgL_g1130z00_1257;

				BgL_stringz00_1256 = VECTOR_REF(BgL_opt1128z00_120, 0L);
				BgL_g1130z00_1257 = VECTOR_REF(BgL_opt1128z00_120, 1L);
				switch (VECTOR_LENGTH(BgL_opt1128z00_120))
					{
					case 2L:

						{	/* Ieee/string.scm 791 */
							long BgL_endz00_1261;

							{	/* Ieee/string.scm 791 */
								obj_t BgL_stringz00_3065;

								if (STRINGP(BgL_stringz00_1256))
									{	/* Ieee/string.scm 791 */
										BgL_stringz00_3065 = BgL_stringz00_1256;
									}
								else
									{
										obj_t BgL_auxz00_6364;

										BgL_auxz00_6364 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2658z00zz__r4_strings_6_7z00, BINT(36080L),
											BGl_string2718z00zz__r4_strings_6_7z00,
											BGl_string2660z00zz__r4_strings_6_7z00,
											BgL_stringz00_1256);
										FAILURE(BgL_auxz00_6364, BFALSE, BFALSE);
									}
								BgL_endz00_1261 = STRING_LENGTH(BgL_stringz00_3065);
							}
							{	/* Ieee/string.scm 791 */

								{	/* Ieee/string.scm 791 */
									obj_t BgL_auxz00_6369;

									if (STRINGP(BgL_stringz00_1256))
										{	/* Ieee/string.scm 791 */
											BgL_auxz00_6369 = BgL_stringz00_1256;
										}
									else
										{
											obj_t BgL_auxz00_6372;

											BgL_auxz00_6372 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(36009L),
												BGl_string2718z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00,
												BgL_stringz00_1256);
											FAILURE(BgL_auxz00_6372, BFALSE, BFALSE);
										}
									return
										BGl_stringzd2deletezd2zz__r4_strings_6_7z00(BgL_auxz00_6369,
										BgL_g1130z00_1257, (int) (0L), BgL_endz00_1261);
						}}} break;
					case 3L:

						{	/* Ieee/string.scm 791 */
							obj_t BgL_startz00_1262;

							BgL_startz00_1262 = VECTOR_REF(BgL_opt1128z00_120, 2L);
							{	/* Ieee/string.scm 791 */
								long BgL_endz00_1263;

								{	/* Ieee/string.scm 791 */
									obj_t BgL_stringz00_3066;

									if (STRINGP(BgL_stringz00_1256))
										{	/* Ieee/string.scm 791 */
											BgL_stringz00_3066 = BgL_stringz00_1256;
										}
									else
										{
											obj_t BgL_auxz00_6381;

											BgL_auxz00_6381 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(36080L),
												BGl_string2718z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00,
												BgL_stringz00_1256);
											FAILURE(BgL_auxz00_6381, BFALSE, BFALSE);
										}
									BgL_endz00_1263 = STRING_LENGTH(BgL_stringz00_3066);
								}
								{	/* Ieee/string.scm 791 */

									{	/* Ieee/string.scm 791 */
										int BgL_auxz00_6393;
										obj_t BgL_auxz00_6386;

										{	/* Ieee/string.scm 791 */
											obj_t BgL_tmpz00_6394;

											if (INTEGERP(BgL_startz00_1262))
												{	/* Ieee/string.scm 791 */
													BgL_tmpz00_6394 = BgL_startz00_1262;
												}
											else
												{
													obj_t BgL_auxz00_6397;

													BgL_auxz00_6397 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2658z00zz__r4_strings_6_7z00,
														BINT(36009L),
														BGl_string2718z00zz__r4_strings_6_7z00,
														BGl_string2662z00zz__r4_strings_6_7z00,
														BgL_startz00_1262);
													FAILURE(BgL_auxz00_6397, BFALSE, BFALSE);
												}
											BgL_auxz00_6393 = CINT(BgL_tmpz00_6394);
										}
										if (STRINGP(BgL_stringz00_1256))
											{	/* Ieee/string.scm 791 */
												BgL_auxz00_6386 = BgL_stringz00_1256;
											}
										else
											{
												obj_t BgL_auxz00_6389;

												BgL_auxz00_6389 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2658z00zz__r4_strings_6_7z00, BINT(36009L),
													BGl_string2718z00zz__r4_strings_6_7z00,
													BGl_string2660z00zz__r4_strings_6_7z00,
													BgL_stringz00_1256);
												FAILURE(BgL_auxz00_6389, BFALSE, BFALSE);
											}
										return
											BGl_stringzd2deletezd2zz__r4_strings_6_7z00
											(BgL_auxz00_6386, BgL_g1130z00_1257, BgL_auxz00_6393,
											BgL_endz00_1263);
									}
								}
							}
						}
						break;
					case 4L:

						{	/* Ieee/string.scm 791 */
							obj_t BgL_startz00_1264;

							BgL_startz00_1264 = VECTOR_REF(BgL_opt1128z00_120, 2L);
							{	/* Ieee/string.scm 791 */
								obj_t BgL_endz00_1265;

								BgL_endz00_1265 = VECTOR_REF(BgL_opt1128z00_120, 3L);
								{	/* Ieee/string.scm 791 */

									{	/* Ieee/string.scm 791 */
										long BgL_auxz00_6421;
										int BgL_auxz00_6412;
										obj_t BgL_auxz00_6405;

										{	/* Ieee/string.scm 791 */
											obj_t BgL_tmpz00_6422;

											if (INTEGERP(BgL_endz00_1265))
												{	/* Ieee/string.scm 791 */
													BgL_tmpz00_6422 = BgL_endz00_1265;
												}
											else
												{
													obj_t BgL_auxz00_6425;

													BgL_auxz00_6425 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2658z00zz__r4_strings_6_7z00,
														BINT(36009L),
														BGl_string2718z00zz__r4_strings_6_7z00,
														BGl_string2662z00zz__r4_strings_6_7z00,
														BgL_endz00_1265);
													FAILURE(BgL_auxz00_6425, BFALSE, BFALSE);
												}
											BgL_auxz00_6421 = (long) CINT(BgL_tmpz00_6422);
										}
										{	/* Ieee/string.scm 791 */
											obj_t BgL_tmpz00_6413;

											if (INTEGERP(BgL_startz00_1264))
												{	/* Ieee/string.scm 791 */
													BgL_tmpz00_6413 = BgL_startz00_1264;
												}
											else
												{
													obj_t BgL_auxz00_6416;

													BgL_auxz00_6416 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2658z00zz__r4_strings_6_7z00,
														BINT(36009L),
														BGl_string2718z00zz__r4_strings_6_7z00,
														BGl_string2662z00zz__r4_strings_6_7z00,
														BgL_startz00_1264);
													FAILURE(BgL_auxz00_6416, BFALSE, BFALSE);
												}
											BgL_auxz00_6412 = CINT(BgL_tmpz00_6413);
										}
										if (STRINGP(BgL_stringz00_1256))
											{	/* Ieee/string.scm 791 */
												BgL_auxz00_6405 = BgL_stringz00_1256;
											}
										else
											{
												obj_t BgL_auxz00_6408;

												BgL_auxz00_6408 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2658z00zz__r4_strings_6_7z00, BINT(36009L),
													BGl_string2718z00zz__r4_strings_6_7z00,
													BGl_string2660z00zz__r4_strings_6_7z00,
													BgL_stringz00_1256);
												FAILURE(BgL_auxz00_6408, BFALSE, BFALSE);
											}
										return
											BGl_stringzd2deletezd2zz__r4_strings_6_7z00
											(BgL_auxz00_6405, BgL_g1130z00_1257, BgL_auxz00_6412,
											BgL_auxz00_6421);
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* string-delete */
	BGL_EXPORTED_DEF obj_t BGl_stringzd2deletezd2zz__r4_strings_6_7z00(obj_t
		BgL_stringz00_116, obj_t BgL_objz00_117, int BgL_startz00_118,
		long BgL_endz00_119)
	{
		{	/* Ieee/string.scm 791 */
			{
				obj_t BgL_newz00_1313;
				obj_t BgL_predz00_1314;
				int BgL_startz00_1315;
				long BgL_endz00_1316;
				obj_t BgL_newz00_1297;
				obj_t BgL_lz00_1298;
				int BgL_startz00_1299;
				long BgL_endz00_1300;
				obj_t BgL_newz00_1281;
				obj_t BgL_cz00_1282;
				int BgL_startz00_1283;
				long BgL_endz00_1284;

				if (((long) (BgL_startz00_118) < 0L))
					{	/* Ieee/string.scm 830 */
						return
							BGl_errorz00zz__errorz00(BGl_string2719z00zz__r4_strings_6_7z00,
							BGl_string2720z00zz__r4_strings_6_7z00, BINT(BgL_startz00_118));
					}
				else
					{	/* Ieee/string.scm 830 */
						if ((BgL_endz00_119 > STRING_LENGTH(BgL_stringz00_116)))
							{	/* Ieee/string.scm 832 */
								return
									BGl_errorz00zz__errorz00
									(BGl_string2719z00zz__r4_strings_6_7z00,
									BGl_string2721z00zz__r4_strings_6_7z00, BINT(BgL_endz00_119));
							}
						else
							{	/* Ieee/string.scm 832 */
								if ((BgL_endz00_119 < (long) (BgL_startz00_118)))
									{	/* Ieee/string.scm 835 */
										obj_t BgL_arg1451z00_1273;

										BgL_arg1451z00_1273 =
											MAKE_YOUNG_PAIR(BINT(BgL_startz00_118),
											BINT(BgL_endz00_119));
										return
											BGl_errorz00zz__errorz00
											(BGl_string2719z00zz__r4_strings_6_7z00,
											BGl_string2722z00zz__r4_strings_6_7z00,
											BgL_arg1451z00_1273);
									}
								else
									{	/* Ieee/string.scm 837 */
										obj_t BgL_newz00_1275;

										BgL_newz00_1275 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_stringz00_116);
										{	/* Ieee/string.scm 838 */

											if (CHARP(BgL_objz00_117))
												{	/* Ieee/string.scm 840 */
													BgL_newz00_1281 = BgL_newz00_1275;
													BgL_cz00_1282 = BgL_objz00_117;
													BgL_startz00_1283 = BgL_startz00_118;
													BgL_endz00_1284 = BgL_endz00_119;
													{
														int BgL_iz00_1287;
														long BgL_jz00_1288;

														BgL_iz00_1287 = BgL_startz00_1283;
														BgL_jz00_1288 = 0L;
													BgL_zc3z04anonymousza31458ze3z87_1289:
														if (((long) (BgL_iz00_1287) == BgL_endz00_1284))
															{	/* Ieee/string.scm 796 */
																return
																	bgl_string_shrink(BgL_newz00_1281,
																	BgL_jz00_1288);
															}
														else
															{	/* Ieee/string.scm 798 */
																unsigned char BgL_ccz00_1291;

																BgL_ccz00_1291 =
																	STRING_REF(BgL_stringz00_116,
																	(long) (BgL_iz00_1287));
																if ((BgL_ccz00_1291 == CCHAR(BgL_cz00_1282)))
																	{	/* Ieee/string.scm 800 */
																		long BgL_arg1461z00_1293;

																		BgL_arg1461z00_1293 =
																			((long) (BgL_iz00_1287) + 1L);
																		{
																			int BgL_iz00_6464;

																			BgL_iz00_6464 =
																				(int) (BgL_arg1461z00_1293);
																			BgL_iz00_1287 = BgL_iz00_6464;
																			goto
																				BgL_zc3z04anonymousza31458ze3z87_1289;
																		}
																	}
																else
																	{	/* Ieee/string.scm 799 */
																		STRING_SET(BgL_newz00_1281, BgL_jz00_1288,
																			BgL_ccz00_1291);
																		{	/* Ieee/string.scm 803 */
																			long BgL_arg1462z00_1294;
																			long BgL_arg1463z00_1295;

																			BgL_arg1462z00_1294 =
																				((long) (BgL_iz00_1287) + 1L);
																			BgL_arg1463z00_1295 =
																				(BgL_jz00_1288 + 1L);
																			{
																				long BgL_jz00_6472;
																				int BgL_iz00_6470;

																				BgL_iz00_6470 =
																					(int) (BgL_arg1462z00_1294);
																				BgL_jz00_6472 = BgL_arg1463z00_1295;
																				BgL_jz00_1288 = BgL_jz00_6472;
																				BgL_iz00_1287 = BgL_iz00_6470;
																				goto
																					BgL_zc3z04anonymousza31458ze3z87_1289;
																			}
																		}
																	}
															}
													}
												}
											else
												{	/* Ieee/string.scm 840 */
													if (STRINGP(BgL_objz00_117))
														{	/* Ieee/string.scm 842 */
															BgL_newz00_1297 = BgL_newz00_1275;
															BgL_lz00_1298 =
																BGl_stringzd2ze3listz31zz__r4_strings_6_7z00
																(BgL_objz00_117);
															BgL_startz00_1299 = BgL_startz00_118;
															BgL_endz00_1300 = BgL_endz00_119;
															{
																int BgL_iz00_1303;
																long BgL_jz00_1304;

																BgL_iz00_1303 = BgL_startz00_1299;
																BgL_jz00_1304 = 0L;
															BgL_zc3z04anonymousza31465ze3z87_1305:
																if (((long) (BgL_iz00_1303) == BgL_endz00_1300))
																	{	/* Ieee/string.scm 808 */
																		return
																			bgl_string_shrink(BgL_newz00_1297,
																			BgL_jz00_1304);
																	}
																else
																	{	/* Ieee/string.scm 810 */
																		unsigned char BgL_ccz00_1307;

																		BgL_ccz00_1307 =
																			STRING_REF(BgL_stringz00_116,
																			(long) (BgL_iz00_1303));
																		if (CBOOL
																			(BGl_memvz00zz__r4_pairs_and_lists_6_3z00
																				(BCHAR(BgL_ccz00_1307), BgL_lz00_1298)))
																			{	/* Ieee/string.scm 812 */
																				long BgL_arg1468z00_1309;

																				BgL_arg1468z00_1309 =
																					((long) (BgL_iz00_1303) + 1L);
																				{
																					int BgL_iz00_6487;

																					BgL_iz00_6487 =
																						(int) (BgL_arg1468z00_1309);
																					BgL_iz00_1303 = BgL_iz00_6487;
																					goto
																						BgL_zc3z04anonymousza31465ze3z87_1305;
																				}
																			}
																		else
																			{	/* Ieee/string.scm 811 */
																				STRING_SET(BgL_newz00_1297,
																					BgL_jz00_1304, BgL_ccz00_1307);
																				{	/* Ieee/string.scm 815 */
																					long BgL_arg1469z00_1310;
																					long BgL_arg1472z00_1311;

																					BgL_arg1469z00_1310 =
																						((long) (BgL_iz00_1303) + 1L);
																					BgL_arg1472z00_1311 =
																						(BgL_jz00_1304 + 1L);
																					{
																						long BgL_jz00_6495;
																						int BgL_iz00_6493;

																						BgL_iz00_6493 =
																							(int) (BgL_arg1469z00_1310);
																						BgL_jz00_6495 = BgL_arg1472z00_1311;
																						BgL_jz00_1304 = BgL_jz00_6495;
																						BgL_iz00_1303 = BgL_iz00_6493;
																						goto
																							BgL_zc3z04anonymousza31465ze3z87_1305;
																					}
																				}
																			}
																	}
															}
														}
													else
														{	/* Ieee/string.scm 842 */
															if (PROCEDUREP(BgL_objz00_117))
																{	/* Ieee/string.scm 844 */
																	BgL_newz00_1313 = BgL_newz00_1275;
																	BgL_predz00_1314 = BgL_objz00_117;
																	BgL_startz00_1315 = BgL_startz00_118;
																	BgL_endz00_1316 = BgL_endz00_119;
																	{
																		int BgL_iz00_1319;
																		long BgL_jz00_1320;

																		BgL_iz00_1319 = BgL_startz00_1315;
																		BgL_jz00_1320 = 0L;
																	BgL_zc3z04anonymousza31474ze3z87_1321:
																		if (
																			((long) (BgL_iz00_1319) ==
																				BgL_endz00_1316))
																			{	/* Ieee/string.scm 820 */
																				return
																					bgl_string_shrink(BgL_newz00_1313,
																					BgL_jz00_1320);
																			}
																		else
																			{	/* Ieee/string.scm 822 */
																				unsigned char BgL_ccz00_1323;

																				BgL_ccz00_1323 =
																					STRING_REF(BgL_stringz00_116,
																					(long) (BgL_iz00_1319));
																				if (CBOOL(BGL_PROCEDURE_CALL1
																						(BgL_predz00_1314,
																							BCHAR(BgL_ccz00_1323))))
																					{	/* Ieee/string.scm 824 */
																						long BgL_arg1477z00_1325;

																						BgL_arg1477z00_1325 =
																							((long) (BgL_iz00_1319) + 1L);
																						{
																							int BgL_iz00_6514;

																							BgL_iz00_6514 =
																								(int) (BgL_arg1477z00_1325);
																							BgL_iz00_1319 = BgL_iz00_6514;
																							goto
																								BgL_zc3z04anonymousza31474ze3z87_1321;
																						}
																					}
																				else
																					{	/* Ieee/string.scm 823 */
																						STRING_SET(BgL_newz00_1313,
																							BgL_jz00_1320, BgL_ccz00_1323);
																						{	/* Ieee/string.scm 827 */
																							long BgL_arg1478z00_1326;
																							long BgL_arg1479z00_1327;

																							BgL_arg1478z00_1326 =
																								((long) (BgL_iz00_1319) + 1L);
																							BgL_arg1479z00_1327 =
																								(BgL_jz00_1320 + 1L);
																							{
																								long BgL_jz00_6522;
																								int BgL_iz00_6520;

																								BgL_iz00_6520 =
																									(int) (BgL_arg1478z00_1326);
																								BgL_jz00_6522 =
																									BgL_arg1479z00_1327;
																								BgL_jz00_1320 = BgL_jz00_6522;
																								BgL_iz00_1319 = BgL_iz00_6520;
																								goto
																									BgL_zc3z04anonymousza31474ze3z87_1321;
																							}
																						}
																					}
																			}
																	}
																}
															else
																{	/* Ieee/string.scm 844 */
																	return
																		BGl_errorz00zz__errorz00
																		(BGl_string2719z00zz__r4_strings_6_7z00,
																		BGl_string2723z00zz__r4_strings_6_7z00,
																		BgL_objz00_117);
																}
														}
												}
										}
									}
							}
					}
			}
		}

	}



/* delim? */
	bool_t BGl_delimzf3zf3zz__r4_strings_6_7z00(obj_t BgL_delimsz00_122,
		unsigned char BgL_charz00_123)
	{
		{	/* Ieee/string.scm 852 */
			{	/* Ieee/string.scm 853 */
				long BgL_lenz00_1332;

				BgL_lenz00_1332 = STRING_LENGTH(((obj_t) BgL_delimsz00_122));
				{
					long BgL_iz00_3122;

					BgL_iz00_3122 = 0L;
				BgL_loopz00_3121:
					if ((BgL_iz00_3122 == BgL_lenz00_1332))
						{	/* Ieee/string.scm 856 */
							return ((bool_t) 0);
						}
					else
						{	/* Ieee/string.scm 856 */
							if (
								(BgL_charz00_123 ==
									STRING_REF(((obj_t) BgL_delimsz00_122), BgL_iz00_3122)))
								{	/* Ieee/string.scm 858 */
									return ((bool_t) 1);
								}
							else
								{
									long BgL_iz00_6532;

									BgL_iz00_6532 = (BgL_iz00_3122 + 1L);
									BgL_iz00_3122 = BgL_iz00_6532;
									goto BgL_loopz00_3121;
								}
						}
				}
			}
		}

	}



/* string-split */
	BGL_EXPORTED_DEF obj_t BGl_stringzd2splitzd2zz__r4_strings_6_7z00(obj_t
		BgL_stringz00_132, obj_t BgL_delimitersz00_133)
	{
		{	/* Ieee/string.scm 890 */
			{	/* Ieee/string.scm 891 */
				obj_t BgL_dz00_1352;

				if (PAIRP(BgL_delimitersz00_133))
					{	/* Ieee/string.scm 891 */
						BgL_dz00_1352 = CAR(BgL_delimitersz00_133);
					}
				else
					{	/* Ieee/string.scm 891 */
						BgL_dz00_1352 = BGl_string2724z00zz__r4_strings_6_7z00;
					}
				{	/* Ieee/string.scm 891 */
					long BgL_lenz00_1353;

					BgL_lenz00_1353 = STRING_LENGTH(BgL_stringz00_132);
					{	/* Ieee/string.scm 894 */
						long BgL_iz00_1354;

						{
							long BgL_iz00_3173;

							BgL_iz00_3173 = 0L;
						BgL_skipzd2separatorzd2_3172:
							if ((BgL_iz00_3173 == BgL_lenz00_1353))
								{	/* Ieee/string.scm 868 */
									BgL_iz00_1354 = BgL_lenz00_1353;
								}
							else
								{	/* Ieee/string.scm 868 */
									if (BGl_delimzf3zf3zz__r4_strings_6_7z00(BgL_dz00_1352,
											STRING_REF(BgL_stringz00_132, BgL_iz00_3173)))
										{
											long BgL_iz00_6543;

											BgL_iz00_6543 = (BgL_iz00_3173 + 1L);
											BgL_iz00_3173 = BgL_iz00_6543;
											goto BgL_skipzd2separatorzd2_3172;
										}
									else
										{	/* Ieee/string.scm 870 */
											BgL_iz00_1354 = BgL_iz00_3173;
										}
								}
						}
						{	/* Ieee/string.scm 895 */

							{
								long BgL_iz00_1357;
								obj_t BgL_resz00_1358;

								BgL_iz00_1357 = BgL_iz00_1354;
								BgL_resz00_1358 = BNIL;
							BgL_zc3z04anonymousza31496ze3z87_1359:
								if ((BgL_iz00_1357 == BgL_lenz00_1353))
									{	/* Ieee/string.scm 898 */
										return bgl_reverse_bang(BgL_resz00_1358);
									}
								else
									{	/* Ieee/string.scm 900 */
										long BgL_ez00_1361;

										{
											long BgL_iz00_3187;

											BgL_iz00_3187 = (BgL_iz00_1357 + 1L);
										BgL_skipzd2nonzd2separatorz00_3186:
											if ((BgL_iz00_3187 == BgL_lenz00_1353))
												{	/* Ieee/string.scm 880 */
													BgL_ez00_1361 = BgL_lenz00_1353;
												}
											else
												{	/* Ieee/string.scm 880 */
													if (BGl_delimzf3zf3zz__r4_strings_6_7z00
														(BgL_dz00_1352, STRING_REF(BgL_stringz00_132,
																BgL_iz00_3187)))
														{	/* Ieee/string.scm 882 */
															BgL_ez00_1361 = BgL_iz00_3187;
														}
													else
														{
															long BgL_iz00_6553;

															BgL_iz00_6553 = (BgL_iz00_3187 + 1L);
															BgL_iz00_3187 = BgL_iz00_6553;
															goto BgL_skipzd2nonzd2separatorz00_3186;
														}
												}
										}
										{	/* Ieee/string.scm 900 */
											obj_t BgL_nresz00_1362;

											BgL_nresz00_1362 =
												MAKE_YOUNG_PAIR(c_substring(BgL_stringz00_132,
													BgL_iz00_1357, BgL_ez00_1361), BgL_resz00_1358);
											{	/* Ieee/string.scm 901 */

												if ((BgL_ez00_1361 == BgL_lenz00_1353))
													{	/* Ieee/string.scm 902 */
														return bgl_reverse_bang(BgL_nresz00_1362);
													}
												else
													{	/* Ieee/string.scm 904 */
														long BgL_arg1499z00_1364;

														{
															long BgL_iz00_3204;

															BgL_iz00_3204 = (BgL_ez00_1361 + 1L);
														BgL_skipzd2separatorzd2_3203:
															if ((BgL_iz00_3204 == BgL_lenz00_1353))
																{	/* Ieee/string.scm 868 */
																	BgL_arg1499z00_1364 = BgL_lenz00_1353;
																}
															else
																{	/* Ieee/string.scm 868 */
																	if (BGl_delimzf3zf3zz__r4_strings_6_7z00
																		(BgL_dz00_1352,
																			STRING_REF(BgL_stringz00_132,
																				BgL_iz00_3204)))
																		{
																			long BgL_iz00_6566;

																			BgL_iz00_6566 = (BgL_iz00_3204 + 1L);
																			BgL_iz00_3204 = BgL_iz00_6566;
																			goto BgL_skipzd2separatorzd2_3203;
																		}
																	else
																		{	/* Ieee/string.scm 870 */
																			BgL_arg1499z00_1364 = BgL_iz00_3204;
																		}
																}
														}
														{
															obj_t BgL_resz00_6570;
															long BgL_iz00_6569;

															BgL_iz00_6569 = BgL_arg1499z00_1364;
															BgL_resz00_6570 = BgL_nresz00_1362;
															BgL_resz00_1358 = BgL_resz00_6570;
															BgL_iz00_1357 = BgL_iz00_6569;
															goto BgL_zc3z04anonymousza31496ze3z87_1359;
														}
													}
											}
										}
									}
							}
						}
					}
				}
			}
		}

	}



/* &string-split */
	obj_t BGl_z62stringzd2splitzb0zz__r4_strings_6_7z00(obj_t BgL_envz00_4474,
		obj_t BgL_stringz00_4475, obj_t BgL_delimitersz00_4476)
	{
		{	/* Ieee/string.scm 890 */
			{	/* Ieee/string.scm 891 */
				obj_t BgL_auxz00_6571;

				if (STRINGP(BgL_stringz00_4475))
					{	/* Ieee/string.scm 891 */
						BgL_auxz00_6571 = BgL_stringz00_4475;
					}
				else
					{
						obj_t BgL_auxz00_6574;

						BgL_auxz00_6574 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2658z00zz__r4_strings_6_7z00, BINT(39316L),
							BGl_string2725z00zz__r4_strings_6_7z00,
							BGl_string2660z00zz__r4_strings_6_7z00, BgL_stringz00_4475);
						FAILURE(BgL_auxz00_6574, BFALSE, BFALSE);
					}
				return
					BGl_stringzd2splitzd2zz__r4_strings_6_7z00(BgL_auxz00_6571,
					BgL_delimitersz00_4476);
			}
		}

	}



/* string-cut */
	BGL_EXPORTED_DEF obj_t BGl_stringzd2cutzd2zz__r4_strings_6_7z00(obj_t
		BgL_stringz00_134, obj_t BgL_delimitersz00_135)
	{
		{	/* Ieee/string.scm 910 */
			{	/* Ieee/string.scm 911 */
				obj_t BgL_dz00_1370;

				if (PAIRP(BgL_delimitersz00_135))
					{	/* Ieee/string.scm 911 */
						BgL_dz00_1370 = CAR(BgL_delimitersz00_135);
					}
				else
					{	/* Ieee/string.scm 911 */
						BgL_dz00_1370 = BGl_string2724z00zz__r4_strings_6_7z00;
					}
				{	/* Ieee/string.scm 911 */
					long BgL_lenz00_1371;

					BgL_lenz00_1371 = STRING_LENGTH(BgL_stringz00_134);
					{	/* Ieee/string.scm 914 */

						{
							long BgL_iz00_1374;
							obj_t BgL_resz00_1375;

							BgL_iz00_1374 = 0L;
							BgL_resz00_1375 = BNIL;
						BgL_zc3z04anonymousza31504ze3z87_1376:
							if ((BgL_iz00_1374 >= BgL_lenz00_1371))
								{	/* Ieee/string.scm 918 */
									obj_t BgL_arg1506z00_1378;

									BgL_arg1506z00_1378 =
										MAKE_YOUNG_PAIR(BGl_string2693z00zz__r4_strings_6_7z00,
										BgL_resz00_1375);
									return bgl_reverse_bang(BgL_arg1506z00_1378);
								}
							else
								{	/* Ieee/string.scm 919 */
									long BgL_ez00_1379;

									{
										long BgL_iz00_3219;

										BgL_iz00_3219 = BgL_iz00_1374;
									BgL_skipzd2nonzd2separatorz00_3218:
										if ((BgL_iz00_3219 == BgL_lenz00_1371))
											{	/* Ieee/string.scm 880 */
												BgL_ez00_1379 = BgL_lenz00_1371;
											}
										else
											{	/* Ieee/string.scm 880 */
												if (BGl_delimzf3zf3zz__r4_strings_6_7z00(BgL_dz00_1370,
														STRING_REF(BgL_stringz00_134, BgL_iz00_3219)))
													{	/* Ieee/string.scm 882 */
														BgL_ez00_1379 = BgL_iz00_3219;
													}
												else
													{
														long BgL_iz00_6592;

														BgL_iz00_6592 = (BgL_iz00_3219 + 1L);
														BgL_iz00_3219 = BgL_iz00_6592;
														goto BgL_skipzd2nonzd2separatorz00_3218;
													}
											}
									}
									{	/* Ieee/string.scm 919 */
										obj_t BgL_sz00_1380;

										BgL_sz00_1380 =
											c_substring(BgL_stringz00_134, BgL_iz00_1374,
											BgL_ez00_1379);
										{	/* Ieee/string.scm 920 */
											obj_t BgL_nrz00_1381;

											BgL_nrz00_1381 =
												MAKE_YOUNG_PAIR(BgL_sz00_1380, BgL_resz00_1375);
											{	/* Ieee/string.scm 921 */

												if ((BgL_ez00_1379 == BgL_lenz00_1371))
													{	/* Ieee/string.scm 922 */
														return bgl_reverse_bang(BgL_nrz00_1381);
													}
												else
													{
														obj_t BgL_resz00_6601;
														long BgL_iz00_6599;

														BgL_iz00_6599 = (BgL_ez00_1379 + 1L);
														BgL_resz00_6601 = BgL_nrz00_1381;
														BgL_resz00_1375 = BgL_resz00_6601;
														BgL_iz00_1374 = BgL_iz00_6599;
														goto BgL_zc3z04anonymousza31504ze3z87_1376;
													}
											}
										}
									}
								}
						}
					}
				}
			}
		}

	}



/* &string-cut */
	obj_t BGl_z62stringzd2cutzb0zz__r4_strings_6_7z00(obj_t BgL_envz00_4477,
		obj_t BgL_stringz00_4478, obj_t BgL_delimitersz00_4479)
	{
		{	/* Ieee/string.scm 910 */
			{	/* Ieee/string.scm 911 */
				obj_t BgL_auxz00_6602;

				if (STRINGP(BgL_stringz00_4478))
					{	/* Ieee/string.scm 911 */
						BgL_auxz00_6602 = BgL_stringz00_4478;
					}
				else
					{
						obj_t BgL_auxz00_6605;

						BgL_auxz00_6605 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2658z00zz__r4_strings_6_7z00, BINT(40015L),
							BGl_string2726z00zz__r4_strings_6_7z00,
							BGl_string2660z00zz__r4_strings_6_7z00, BgL_stringz00_4478);
						FAILURE(BgL_auxz00_6605, BFALSE, BFALSE);
					}
				return
					BGl_stringzd2cutzd2zz__r4_strings_6_7z00(BgL_auxz00_6602,
					BgL_delimitersz00_4479);
			}
		}

	}



/* string-char-index-ur */
	BGL_EXPORTED_DEF obj_t
		BGl_stringzd2charzd2indexzd2urzd2zz__r4_strings_6_7z00(obj_t
		BgL_stringz00_136, unsigned char BgL_charz00_137, long BgL_startz00_138,
		long BgL_nz00_139)
	{
		{	/* Ieee/string.scm 929 */
			{	/* Ieee/string.scm 932 */
				char *BgL_s0z00_3235;

				BgL_s0z00_3235 = BSTRING_TO_STRING(BgL_stringz00_136);
				{	/* Ieee/string.scm 932 */
					char *BgL_s1z00_3236;

					{	/* Ieee/string.scm 933 */
						int BgL_tmpz00_6611;

						BgL_tmpz00_6611 = (int) (((unsigned char) (BgL_charz00_137)));
						BgL_s1z00_3236 =
							BGL_MEMCHR(BgL_s0z00_3235, BgL_tmpz00_6611, BgL_nz00_139,
							BgL_startz00_138);
					}
					{	/* Ieee/string.scm 933 */

						if (BGL_MEMCHR_ZERO(BgL_s1z00_3236))
							{	/* Ieee/string.scm 934 */
								return BFALSE;
							}
						else
							{	/* Ieee/string.scm 934 */
								return BINT(BGL_MEMCHR_DIFF(BgL_s1z00_3236, BgL_s0z00_3235));
							}
					}
				}
			}
		}

	}



/* &string-char-index-ur */
	obj_t BGl_z62stringzd2charzd2indexzd2urzb0zz__r4_strings_6_7z00(obj_t
		BgL_envz00_4480, obj_t BgL_stringz00_4481, obj_t BgL_charz00_4482,
		obj_t BgL_startz00_4483, obj_t BgL_nz00_4484)
	{
		{	/* Ieee/string.scm 929 */
			{	/* Ieee/string.scm 932 */
				long BgL_auxz00_6645;
				long BgL_auxz00_6636;
				unsigned char BgL_auxz00_6627;
				obj_t BgL_auxz00_6620;

				{	/* Ieee/string.scm 932 */
					obj_t BgL_tmpz00_6646;

					if (INTEGERP(BgL_nz00_4484))
						{	/* Ieee/string.scm 932 */
							BgL_tmpz00_6646 = BgL_nz00_4484;
						}
					else
						{
							obj_t BgL_auxz00_6649;

							BgL_auxz00_6649 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(40707L),
								BGl_string2727z00zz__r4_strings_6_7z00,
								BGl_string2662z00zz__r4_strings_6_7z00, BgL_nz00_4484);
							FAILURE(BgL_auxz00_6649, BFALSE, BFALSE);
						}
					BgL_auxz00_6645 = (long) CINT(BgL_tmpz00_6646);
				}
				{	/* Ieee/string.scm 932 */
					obj_t BgL_tmpz00_6637;

					if (INTEGERP(BgL_startz00_4483))
						{	/* Ieee/string.scm 932 */
							BgL_tmpz00_6637 = BgL_startz00_4483;
						}
					else
						{
							obj_t BgL_auxz00_6640;

							BgL_auxz00_6640 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(40707L),
								BGl_string2727z00zz__r4_strings_6_7z00,
								BGl_string2662z00zz__r4_strings_6_7z00, BgL_startz00_4483);
							FAILURE(BgL_auxz00_6640, BFALSE, BFALSE);
						}
					BgL_auxz00_6636 = (long) CINT(BgL_tmpz00_6637);
				}
				{	/* Ieee/string.scm 932 */
					obj_t BgL_tmpz00_6628;

					if (CHARP(BgL_charz00_4482))
						{	/* Ieee/string.scm 932 */
							BgL_tmpz00_6628 = BgL_charz00_4482;
						}
					else
						{
							obj_t BgL_auxz00_6631;

							BgL_auxz00_6631 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2658z00zz__r4_strings_6_7z00, BINT(40707L),
								BGl_string2727z00zz__r4_strings_6_7z00,
								BGl_string2663z00zz__r4_strings_6_7z00, BgL_charz00_4482);
							FAILURE(BgL_auxz00_6631, BFALSE, BFALSE);
						}
					BgL_auxz00_6627 = CCHAR(BgL_tmpz00_6628);
				}
				if (STRINGP(BgL_stringz00_4481))
					{	/* Ieee/string.scm 932 */
						BgL_auxz00_6620 = BgL_stringz00_4481;
					}
				else
					{
						obj_t BgL_auxz00_6623;

						BgL_auxz00_6623 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2658z00zz__r4_strings_6_7z00, BINT(40707L),
							BGl_string2727z00zz__r4_strings_6_7z00,
							BGl_string2660z00zz__r4_strings_6_7z00, BgL_stringz00_4481);
						FAILURE(BgL_auxz00_6623, BFALSE, BFALSE);
					}
				return
					BGl_stringzd2charzd2indexzd2urzd2zz__r4_strings_6_7z00
					(BgL_auxz00_6620, BgL_auxz00_6627, BgL_auxz00_6636, BgL_auxz00_6645);
			}
		}

	}



/* _string-char-index */
	obj_t BGl__stringzd2charzd2indexz00zz__r4_strings_6_7z00(obj_t
		BgL_env1134z00_145, obj_t BgL_opt1133z00_144)
	{
		{	/* Ieee/string.scm 950 */
			{	/* Ieee/string.scm 950 */
				obj_t BgL_g1135z00_1390;
				obj_t BgL_g1136z00_1391;

				BgL_g1135z00_1390 = VECTOR_REF(BgL_opt1133z00_144, 0L);
				BgL_g1136z00_1391 = VECTOR_REF(BgL_opt1133z00_144, 1L);
				switch (VECTOR_LENGTH(BgL_opt1133z00_144))
					{
					case 2L:

						{	/* Ieee/string.scm 950 */

							{	/* Ieee/string.scm 950 */
								obj_t BgL_stringz00_3240;
								unsigned char BgL_charz00_3241;

								if (STRINGP(BgL_g1135z00_1390))
									{	/* Ieee/string.scm 950 */
										BgL_stringz00_3240 = BgL_g1135z00_1390;
									}
								else
									{
										obj_t BgL_auxz00_6659;

										BgL_auxz00_6659 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2658z00zz__r4_strings_6_7z00, BINT(41269L),
											BGl_string2728z00zz__r4_strings_6_7z00,
											BGl_string2660z00zz__r4_strings_6_7z00,
											BgL_g1135z00_1390);
										FAILURE(BgL_auxz00_6659, BFALSE, BFALSE);
									}
								{	/* Ieee/string.scm 950 */
									obj_t BgL_tmpz00_6663;

									if (CHARP(BgL_g1136z00_1391))
										{	/* Ieee/string.scm 950 */
											BgL_tmpz00_6663 = BgL_g1136z00_1391;
										}
									else
										{
											obj_t BgL_auxz00_6666;

											BgL_auxz00_6666 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(41269L),
												BGl_string2728z00zz__r4_strings_6_7z00,
												BGl_string2663z00zz__r4_strings_6_7z00,
												BgL_g1136z00_1391);
											FAILURE(BgL_auxz00_6666, BFALSE, BFALSE);
										}
									BgL_charz00_3241 = CCHAR(BgL_tmpz00_6663);
								}
								{	/* Ieee/string.scm 951 */
									long BgL_lenz00_3242;

									BgL_lenz00_3242 = STRING_LENGTH(BgL_stringz00_3240);
									{	/* Ieee/string.scm 952 */

										if ((BgL_lenz00_3242 > 0L))
											{	/* Ieee/string.scm 932 */
												char *BgL_s0z00_3263;

												BgL_s0z00_3263 = BSTRING_TO_STRING(BgL_stringz00_3240);
												{	/* Ieee/string.scm 932 */
													char *BgL_s1z00_3264;

													{	/* Ieee/string.scm 933 */
														long BgL_auxz00_6679;
														int BgL_tmpz00_6675;

														{	/* Ieee/string.scm 952 */
															bool_t BgL_test3180z00_6680;

															if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
																(BINT(-1L)))
																{	/* Ieee/string.scm 952 */
																	if ((-1L >= 0L))
																		{	/* Ieee/string.scm 953 */
																			BgL_test3180z00_6680 =
																				(-1L <= (BgL_lenz00_3242 - 0L));
																		}
																	else
																		{	/* Ieee/string.scm 953 */
																			BgL_test3180z00_6680 = ((bool_t) 0);
																		}
																}
															else
																{	/* Ieee/string.scm 952 */
																	BgL_test3180z00_6680 = ((bool_t) 0);
																}
															if (BgL_test3180z00_6680)
																{	/* Ieee/string.scm 952 */
																	BgL_auxz00_6679 = -1L;
																}
															else
																{	/* Ieee/string.scm 952 */
																	BgL_auxz00_6679 = (BgL_lenz00_3242 - 0L);
																}
														}
														BgL_tmpz00_6675 =
															(int) (((unsigned char) (BgL_charz00_3241)));
														BgL_s1z00_3264 =
															BGL_MEMCHR(BgL_s0z00_3263, BgL_tmpz00_6675,
															BgL_auxz00_6679, 0L);
													}
													{	/* Ieee/string.scm 933 */

														if (BGL_MEMCHR_ZERO(BgL_s1z00_3264))
															{	/* Ieee/string.scm 934 */
																return BFALSE;
															}
														else
															{	/* Ieee/string.scm 934 */
																return
																	BINT(BGL_MEMCHR_DIFF(BgL_s1z00_3264,
																		BgL_s0z00_3263));
															}
													}
												}
											}
										else
											{	/* Ieee/string.scm 956 */
												return BFALSE;
											}
									}
								}
							}
						}
						break;
					case 3L:

						{	/* Ieee/string.scm 950 */
							obj_t BgL_startz00_1396;

							BgL_startz00_1396 = VECTOR_REF(BgL_opt1133z00_144, 2L);
							{	/* Ieee/string.scm 950 */

								{	/* Ieee/string.scm 950 */
									obj_t BgL_stringz00_3268;
									unsigned char BgL_charz00_3269;

									if (STRINGP(BgL_g1135z00_1390))
										{	/* Ieee/string.scm 950 */
											BgL_stringz00_3268 = BgL_g1135z00_1390;
										}
									else
										{
											obj_t BgL_auxz00_6697;

											BgL_auxz00_6697 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(41269L),
												BGl_string2728z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00,
												BgL_g1135z00_1390);
											FAILURE(BgL_auxz00_6697, BFALSE, BFALSE);
										}
									{	/* Ieee/string.scm 950 */
										obj_t BgL_tmpz00_6701;

										if (CHARP(BgL_g1136z00_1391))
											{	/* Ieee/string.scm 950 */
												BgL_tmpz00_6701 = BgL_g1136z00_1391;
											}
										else
											{
												obj_t BgL_auxz00_6704;

												BgL_auxz00_6704 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2658z00zz__r4_strings_6_7z00, BINT(41269L),
													BGl_string2728z00zz__r4_strings_6_7z00,
													BGl_string2663z00zz__r4_strings_6_7z00,
													BgL_g1136z00_1391);
												FAILURE(BgL_auxz00_6704, BFALSE, BFALSE);
											}
										BgL_charz00_3269 = CCHAR(BgL_tmpz00_6701);
									}
									{	/* Ieee/string.scm 951 */
										long BgL_lenz00_3270;

										BgL_lenz00_3270 = STRING_LENGTH(BgL_stringz00_3268);
										{	/* Ieee/string.scm 952 */

											{	/* Ieee/string.scm 956 */
												bool_t BgL_test3186z00_6710;

												{	/* Ieee/string.scm 956 */
													long BgL_n2z00_3286;

													{	/* Ieee/string.scm 956 */
														obj_t BgL_tmpz00_6711;

														if (INTEGERP(BgL_startz00_1396))
															{	/* Ieee/string.scm 956 */
																BgL_tmpz00_6711 = BgL_startz00_1396;
															}
														else
															{
																obj_t BgL_auxz00_6714;

																BgL_auxz00_6714 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string2658z00zz__r4_strings_6_7z00,
																	BINT(41517L),
																	BGl_string2728z00zz__r4_strings_6_7z00,
																	BGl_string2662z00zz__r4_strings_6_7z00,
																	BgL_startz00_1396);
																FAILURE(BgL_auxz00_6714, BFALSE, BFALSE);
															}
														BgL_n2z00_3286 = (long) CINT(BgL_tmpz00_6711);
													}
													BgL_test3186z00_6710 =
														(BgL_lenz00_3270 > BgL_n2z00_3286);
												}
												if (BgL_test3186z00_6710)
													{	/* Ieee/string.scm 957 */
														long BgL_startz00_3289;

														{	/* Ieee/string.scm 957 */
															obj_t BgL_tmpz00_6720;

															if (INTEGERP(BgL_startz00_1396))
																{	/* Ieee/string.scm 957 */
																	BgL_tmpz00_6720 = BgL_startz00_1396;
																}
															else
																{
																	obj_t BgL_auxz00_6723;

																	BgL_auxz00_6723 =
																		BGl_typezd2errorzd2zz__errorz00
																		(BGl_string2658z00zz__r4_strings_6_7z00,
																		BINT(41560L),
																		BGl_string2728z00zz__r4_strings_6_7z00,
																		BGl_string2662z00zz__r4_strings_6_7z00,
																		BgL_startz00_1396);
																	FAILURE(BgL_auxz00_6723, BFALSE, BFALSE);
																}
															BgL_startz00_3289 = (long) CINT(BgL_tmpz00_6720);
														}
														{	/* Ieee/string.scm 932 */
															char *BgL_s0z00_3291;

															BgL_s0z00_3291 =
																BSTRING_TO_STRING(BgL_stringz00_3268);
															{	/* Ieee/string.scm 932 */
																char *BgL_s1z00_3292;

																{	/* Ieee/string.scm 933 */
																	long BgL_auxz00_6733;
																	int BgL_tmpz00_6729;

																	{	/* Ieee/string.scm 952 */
																		bool_t BgL_test3189z00_6734;

																		if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BINT(-1L)))
																			{	/* Ieee/string.scm 952 */
																				if ((-1L >= 0L))
																					{	/* Ieee/string.scm 953 */
																						long BgL_tmpz00_6740;

																						{	/* Ieee/string.scm 953 */
																							long BgL_za72za7_3280;

																							{	/* Ieee/string.scm 953 */
																								obj_t BgL_tmpz00_6741;

																								if (INTEGERP(BgL_startz00_1396))
																									{	/* Ieee/string.scm 953 */
																										BgL_tmpz00_6741 =
																											BgL_startz00_1396;
																									}
																								else
																									{
																										obj_t BgL_auxz00_6744;

																										BgL_auxz00_6744 =
																											BGl_typezd2errorzd2zz__errorz00
																											(BGl_string2658z00zz__r4_strings_6_7z00,
																											BINT(41456L),
																											BGl_string2728z00zz__r4_strings_6_7z00,
																											BGl_string2662z00zz__r4_strings_6_7z00,
																											BgL_startz00_1396);
																										FAILURE(BgL_auxz00_6744,
																											BFALSE, BFALSE);
																									}
																								BgL_za72za7_3280 =
																									(long) CINT(BgL_tmpz00_6741);
																							}
																							BgL_tmpz00_6740 =
																								(BgL_lenz00_3270 -
																								BgL_za72za7_3280);
																						}
																						BgL_test3189z00_6734 =
																							(-1L <= BgL_tmpz00_6740);
																					}
																				else
																					{	/* Ieee/string.scm 953 */
																						BgL_test3189z00_6734 = ((bool_t) 0);
																					}
																			}
																		else
																			{	/* Ieee/string.scm 952 */
																				BgL_test3189z00_6734 = ((bool_t) 0);
																			}
																		if (BgL_test3189z00_6734)
																			{	/* Ieee/string.scm 952 */
																				BgL_auxz00_6733 = -1L;
																			}
																		else
																			{	/* Ieee/string.scm 955 */
																				long BgL_za72za7_3284;

																				{	/* Ieee/string.scm 955 */
																					obj_t BgL_tmpz00_6751;

																					if (INTEGERP(BgL_startz00_1396))
																						{	/* Ieee/string.scm 955 */
																							BgL_tmpz00_6751 =
																								BgL_startz00_1396;
																						}
																					else
																						{
																							obj_t BgL_auxz00_6754;

																							BgL_auxz00_6754 =
																								BGl_typezd2errorzd2zz__errorz00
																								(BGl_string2658z00zz__r4_strings_6_7z00,
																								BINT(41486L),
																								BGl_string2728z00zz__r4_strings_6_7z00,
																								BGl_string2662z00zz__r4_strings_6_7z00,
																								BgL_startz00_1396);
																							FAILURE(BgL_auxz00_6754, BFALSE,
																								BFALSE);
																						}
																					BgL_za72za7_3284 =
																						(long) CINT(BgL_tmpz00_6751);
																				}
																				BgL_auxz00_6733 =
																					(BgL_lenz00_3270 - BgL_za72za7_3284);
																	}}
																	BgL_tmpz00_6729 =
																		(int) (
																		((unsigned char) (BgL_charz00_3269)));
																	BgL_s1z00_3292 =
																		BGL_MEMCHR(BgL_s0z00_3291, BgL_tmpz00_6729,
																		BgL_auxz00_6733, BgL_startz00_3289);
																}
																{	/* Ieee/string.scm 933 */

																	if (BGL_MEMCHR_ZERO(BgL_s1z00_3292))
																		{	/* Ieee/string.scm 934 */
																			return BFALSE;
																		}
																	else
																		{	/* Ieee/string.scm 934 */
																			return
																				BINT(BGL_MEMCHR_DIFF(BgL_s1z00_3292,
																					BgL_s0z00_3291));
																		}
																}
															}
														}
													}
												else
													{	/* Ieee/string.scm 956 */
														return BFALSE;
													}
											}
										}
									}
								}
							}
						}
						break;
					case 4L:

						{	/* Ieee/string.scm 950 */
							obj_t BgL_startz00_1398;

							BgL_startz00_1398 = VECTOR_REF(BgL_opt1133z00_144, 2L);
							{	/* Ieee/string.scm 950 */
								obj_t BgL_countz00_1399;

								BgL_countz00_1399 = VECTOR_REF(BgL_opt1133z00_144, 3L);
								{	/* Ieee/string.scm 950 */

									{	/* Ieee/string.scm 950 */
										obj_t BgL_stringz00_3296;
										unsigned char BgL_charz00_3297;

										if (STRINGP(BgL_g1135z00_1390))
											{	/* Ieee/string.scm 950 */
												BgL_stringz00_3296 = BgL_g1135z00_1390;
											}
										else
											{
												obj_t BgL_auxz00_6769;

												BgL_auxz00_6769 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2658z00zz__r4_strings_6_7z00, BINT(41269L),
													BGl_string2728z00zz__r4_strings_6_7z00,
													BGl_string2660z00zz__r4_strings_6_7z00,
													BgL_g1135z00_1390);
												FAILURE(BgL_auxz00_6769, BFALSE, BFALSE);
											}
										{	/* Ieee/string.scm 950 */
											obj_t BgL_tmpz00_6773;

											if (CHARP(BgL_g1136z00_1391))
												{	/* Ieee/string.scm 950 */
													BgL_tmpz00_6773 = BgL_g1136z00_1391;
												}
											else
												{
													obj_t BgL_auxz00_6776;

													BgL_auxz00_6776 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2658z00zz__r4_strings_6_7z00,
														BINT(41269L),
														BGl_string2728z00zz__r4_strings_6_7z00,
														BGl_string2663z00zz__r4_strings_6_7z00,
														BgL_g1136z00_1391);
													FAILURE(BgL_auxz00_6776, BFALSE, BFALSE);
												}
											BgL_charz00_3297 = CCHAR(BgL_tmpz00_6773);
										}
										{	/* Ieee/string.scm 951 */
											long BgL_lenz00_3298;

											BgL_lenz00_3298 = STRING_LENGTH(BgL_stringz00_3296);
											{	/* Ieee/string.scm 952 */

												{	/* Ieee/string.scm 956 */
													bool_t BgL_test3197z00_6782;

													{	/* Ieee/string.scm 956 */
														long BgL_n2z00_3314;

														{	/* Ieee/string.scm 956 */
															obj_t BgL_tmpz00_6783;

															if (INTEGERP(BgL_startz00_1398))
																{	/* Ieee/string.scm 956 */
																	BgL_tmpz00_6783 = BgL_startz00_1398;
																}
															else
																{
																	obj_t BgL_auxz00_6786;

																	BgL_auxz00_6786 =
																		BGl_typezd2errorzd2zz__errorz00
																		(BGl_string2658z00zz__r4_strings_6_7z00,
																		BINT(41517L),
																		BGl_string2728z00zz__r4_strings_6_7z00,
																		BGl_string2662z00zz__r4_strings_6_7z00,
																		BgL_startz00_1398);
																	FAILURE(BgL_auxz00_6786, BFALSE, BFALSE);
																}
															BgL_n2z00_3314 = (long) CINT(BgL_tmpz00_6783);
														}
														BgL_test3197z00_6782 =
															(BgL_lenz00_3298 > BgL_n2z00_3314);
													}
													if (BgL_test3197z00_6782)
														{	/* Ieee/string.scm 957 */
															long BgL_startz00_3317;
															long BgL_nz00_3318;

															{	/* Ieee/string.scm 957 */
																obj_t BgL_tmpz00_6792;

																if (INTEGERP(BgL_startz00_1398))
																	{	/* Ieee/string.scm 957 */
																		BgL_tmpz00_6792 = BgL_startz00_1398;
																	}
																else
																	{
																		obj_t BgL_auxz00_6795;

																		BgL_auxz00_6795 =
																			BGl_typezd2errorzd2zz__errorz00
																			(BGl_string2658z00zz__r4_strings_6_7z00,
																			BINT(41560L),
																			BGl_string2728z00zz__r4_strings_6_7z00,
																			BGl_string2662z00zz__r4_strings_6_7z00,
																			BgL_startz00_1398);
																		FAILURE(BgL_auxz00_6795, BFALSE, BFALSE);
																	}
																BgL_startz00_3317 =
																	(long) CINT(BgL_tmpz00_6792);
															}
															{	/* Ieee/string.scm 952 */
																bool_t BgL_test3200z00_6800;

																if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BgL_countz00_1399))
																	{	/* Ieee/string.scm 953 */
																		bool_t BgL_test3202z00_6803;

																		{	/* Ieee/string.scm 953 */
																			long BgL_n1z00_3306;

																			{	/* Ieee/string.scm 953 */
																				obj_t BgL_tmpz00_6804;

																				if (INTEGERP(BgL_countz00_1399))
																					{	/* Ieee/string.scm 953 */
																						BgL_tmpz00_6804 = BgL_countz00_1399;
																					}
																				else
																					{
																						obj_t BgL_auxz00_6807;

																						BgL_auxz00_6807 =
																							BGl_typezd2errorzd2zz__errorz00
																							(BGl_string2658z00zz__r4_strings_6_7z00,
																							BINT(41426L),
																							BGl_string2728z00zz__r4_strings_6_7z00,
																							BGl_string2662z00zz__r4_strings_6_7z00,
																							BgL_countz00_1399);
																						FAILURE(BgL_auxz00_6807, BFALSE,
																							BFALSE);
																					}
																				BgL_n1z00_3306 =
																					(long) CINT(BgL_tmpz00_6804);
																			}
																			BgL_test3202z00_6803 =
																				(BgL_n1z00_3306 >= 0L);
																		}
																		if (BgL_test3202z00_6803)
																			{	/* Ieee/string.scm 953 */
																				long BgL_n1z00_3309;

																				{	/* Ieee/string.scm 953 */
																					obj_t BgL_tmpz00_6813;

																					if (INTEGERP(BgL_countz00_1399))
																						{	/* Ieee/string.scm 953 */
																							BgL_tmpz00_6813 =
																								BgL_countz00_1399;
																						}
																					else
																						{
																							obj_t BgL_auxz00_6816;

																							BgL_auxz00_6816 =
																								BGl_typezd2errorzd2zz__errorz00
																								(BGl_string2658z00zz__r4_strings_6_7z00,
																								BINT(41441L),
																								BGl_string2728z00zz__r4_strings_6_7z00,
																								BGl_string2662z00zz__r4_strings_6_7z00,
																								BgL_countz00_1399);
																							FAILURE(BgL_auxz00_6816, BFALSE,
																								BFALSE);
																						}
																					BgL_n1z00_3309 =
																						(long) CINT(BgL_tmpz00_6813);
																				}
																				{	/* Ieee/string.scm 953 */
																					long BgL_tmpz00_6821;

																					{	/* Ieee/string.scm 953 */
																						long BgL_za72za7_3308;

																						{	/* Ieee/string.scm 953 */
																							obj_t BgL_tmpz00_6822;

																							if (INTEGERP(BgL_startz00_1398))
																								{	/* Ieee/string.scm 953 */
																									BgL_tmpz00_6822 =
																										BgL_startz00_1398;
																								}
																							else
																								{
																									obj_t BgL_auxz00_6825;

																									BgL_auxz00_6825 =
																										BGl_typezd2errorzd2zz__errorz00
																										(BGl_string2658z00zz__r4_strings_6_7z00,
																										BINT(41456L),
																										BGl_string2728z00zz__r4_strings_6_7z00,
																										BGl_string2662z00zz__r4_strings_6_7z00,
																										BgL_startz00_1398);
																									FAILURE(BgL_auxz00_6825,
																										BFALSE, BFALSE);
																								}
																							BgL_za72za7_3308 =
																								(long) CINT(BgL_tmpz00_6822);
																						}
																						BgL_tmpz00_6821 =
																							(BgL_lenz00_3298 -
																							BgL_za72za7_3308);
																					}
																					BgL_test3200z00_6800 =
																						(BgL_n1z00_3309 <= BgL_tmpz00_6821);
																			}}
																		else
																			{	/* Ieee/string.scm 953 */
																				BgL_test3200z00_6800 = ((bool_t) 0);
																			}
																	}
																else
																	{	/* Ieee/string.scm 952 */
																		BgL_test3200z00_6800 = ((bool_t) 0);
																	}
																if (BgL_test3200z00_6800)
																	{	/* Ieee/string.scm 954 */
																		obj_t BgL_tmpz00_6832;

																		if (INTEGERP(BgL_countz00_1399))
																			{	/* Ieee/string.scm 954 */
																				BgL_tmpz00_6832 = BgL_countz00_1399;
																			}
																		else
																			{
																				obj_t BgL_auxz00_6835;

																				BgL_auxz00_6835 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string2658z00zz__r4_strings_6_7z00,
																					BINT(41468L),
																					BGl_string2728z00zz__r4_strings_6_7z00,
																					BGl_string2662z00zz__r4_strings_6_7z00,
																					BgL_countz00_1399);
																				FAILURE(BgL_auxz00_6835, BFALSE,
																					BFALSE);
																			}
																		BgL_nz00_3318 =
																			(long) CINT(BgL_tmpz00_6832);
																	}
																else
																	{	/* Ieee/string.scm 955 */
																		long BgL_za72za7_3312;

																		{	/* Ieee/string.scm 955 */
																			obj_t BgL_tmpz00_6840;

																			if (INTEGERP(BgL_startz00_1398))
																				{	/* Ieee/string.scm 955 */
																					BgL_tmpz00_6840 = BgL_startz00_1398;
																				}
																			else
																				{
																					obj_t BgL_auxz00_6843;

																					BgL_auxz00_6843 =
																						BGl_typezd2errorzd2zz__errorz00
																						(BGl_string2658z00zz__r4_strings_6_7z00,
																						BINT(41486L),
																						BGl_string2728z00zz__r4_strings_6_7z00,
																						BGl_string2662z00zz__r4_strings_6_7z00,
																						BgL_startz00_1398);
																					FAILURE(BgL_auxz00_6843, BFALSE,
																						BFALSE);
																				}
																			BgL_za72za7_3312 =
																				(long) CINT(BgL_tmpz00_6840);
																		}
																		BgL_nz00_3318 =
																			(BgL_lenz00_3298 - BgL_za72za7_3312);
															}}
															{	/* Ieee/string.scm 932 */
																char *BgL_s0z00_3319;

																BgL_s0z00_3319 =
																	BSTRING_TO_STRING(BgL_stringz00_3296);
																{	/* Ieee/string.scm 932 */
																	char *BgL_s1z00_3320;

																	{	/* Ieee/string.scm 933 */
																		int BgL_tmpz00_6850;

																		BgL_tmpz00_6850 =
																			(int) (
																			((unsigned char) (BgL_charz00_3297)));
																		BgL_s1z00_3320 =
																			BGL_MEMCHR(BgL_s0z00_3319,
																			BgL_tmpz00_6850, BgL_nz00_3318,
																			BgL_startz00_3317);
																	}
																	{	/* Ieee/string.scm 933 */

																		if (BGL_MEMCHR_ZERO(BgL_s1z00_3320))
																			{	/* Ieee/string.scm 934 */
																				return BFALSE;
																			}
																		else
																			{	/* Ieee/string.scm 934 */
																				return
																					BINT(BGL_MEMCHR_DIFF(BgL_s1z00_3320,
																						BgL_s0z00_3319));
																			}
																	}
																}
															}
														}
													else
														{	/* Ieee/string.scm 956 */
															return BFALSE;
														}
												}
											}
										}
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* string-char-index */
	BGL_EXPORTED_DEF obj_t BGl_stringzd2charzd2indexz00zz__r4_strings_6_7z00(obj_t
		BgL_stringz00_140, unsigned char BgL_charz00_141, obj_t BgL_startz00_142,
		obj_t BgL_countz00_143)
	{
		{	/* Ieee/string.scm 950 */
			{	/* Ieee/string.scm 951 */
				long BgL_lenz00_3324;

				BgL_lenz00_3324 = STRING_LENGTH(BgL_stringz00_140);
				{	/* Ieee/string.scm 952 */

					if ((BgL_lenz00_3324 > (long) CINT(BgL_startz00_142)))
						{	/* Ieee/string.scm 932 */
							char *BgL_s0z00_3345;

							BgL_s0z00_3345 = BSTRING_TO_STRING(BgL_stringz00_140);
							{	/* Ieee/string.scm 932 */
								char *BgL_s1z00_3346;

								{	/* Ieee/string.scm 933 */
									long BgL_auxz00_6884;
									long BgL_auxz00_6870;
									int BgL_tmpz00_6866;

									BgL_auxz00_6884 = (long) CINT(BgL_startz00_142);
									{	/* Ieee/string.scm 952 */
										bool_t BgL_test3210z00_6871;

										if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
											(BgL_countz00_143))
											{	/* Ieee/string.scm 952 */
												if (((long) CINT(BgL_countz00_143) >= 0L))
													{	/* Ieee/string.scm 953 */
														BgL_test3210z00_6871 =
															(
															(long) CINT(BgL_countz00_143) <=
															(BgL_lenz00_3324 -
																(long) CINT(BgL_startz00_142)));
													}
												else
													{	/* Ieee/string.scm 953 */
														BgL_test3210z00_6871 = ((bool_t) 0);
													}
											}
										else
											{	/* Ieee/string.scm 952 */
												BgL_test3210z00_6871 = ((bool_t) 0);
											}
										if (BgL_test3210z00_6871)
											{	/* Ieee/string.scm 952 */
												BgL_auxz00_6870 = (long) CINT(BgL_countz00_143);
											}
										else
											{	/* Ieee/string.scm 952 */
												BgL_auxz00_6870 =
													(BgL_lenz00_3324 - (long) CINT(BgL_startz00_142));
									}}
									BgL_tmpz00_6866 = (int) (((unsigned char) (BgL_charz00_141)));
									BgL_s1z00_3346 =
										BGL_MEMCHR(BgL_s0z00_3345, BgL_tmpz00_6866, BgL_auxz00_6870,
										BgL_auxz00_6884);
								}
								{	/* Ieee/string.scm 933 */

									if (BGL_MEMCHR_ZERO(BgL_s1z00_3346))
										{	/* Ieee/string.scm 934 */
											return BFALSE;
										}
									else
										{	/* Ieee/string.scm 934 */
											return
												BINT(BGL_MEMCHR_DIFF(BgL_s1z00_3346, BgL_s0z00_3345));
										}
								}
							}
						}
					else
						{	/* Ieee/string.scm 956 */
							return BFALSE;
						}
				}
			}
		}

	}



/* _string-index */
	obj_t BGl__stringzd2indexzd2zz__r4_strings_6_7z00(obj_t BgL_env1140z00_150,
		obj_t BgL_opt1139z00_149)
	{
		{	/* Ieee/string.scm 962 */
			{	/* Ieee/string.scm 962 */
				obj_t BgL_g1141z00_1410;
				obj_t BgL_g1142z00_1411;

				BgL_g1141z00_1410 = VECTOR_REF(BgL_opt1139z00_149, 0L);
				BgL_g1142z00_1411 = VECTOR_REF(BgL_opt1139z00_149, 1L);
				switch (VECTOR_LENGTH(BgL_opt1139z00_149))
					{
					case 2L:

						{	/* Ieee/string.scm 962 */

							{	/* Ieee/string.scm 962 */
								obj_t BgL_auxz00_6893;

								if (STRINGP(BgL_g1141z00_1410))
									{	/* Ieee/string.scm 962 */
										BgL_auxz00_6893 = BgL_g1141z00_1410;
									}
								else
									{
										obj_t BgL_auxz00_6896;

										BgL_auxz00_6896 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2658z00zz__r4_strings_6_7z00, BINT(41795L),
											BGl_string2729z00zz__r4_strings_6_7z00,
											BGl_string2660z00zz__r4_strings_6_7z00,
											BgL_g1141z00_1410);
										FAILURE(BgL_auxz00_6896, BFALSE, BFALSE);
									}
								return
									BGl_stringzd2indexzd2zz__r4_strings_6_7z00(BgL_auxz00_6893,
									BgL_g1142z00_1411, BINT(0L));
							}
						}
						break;
					case 3L:

						{	/* Ieee/string.scm 962 */
							obj_t BgL_startz00_1415;

							BgL_startz00_1415 = VECTOR_REF(BgL_opt1139z00_149, 2L);
							{	/* Ieee/string.scm 962 */

								{	/* Ieee/string.scm 962 */
									obj_t BgL_auxz00_6903;

									if (STRINGP(BgL_g1141z00_1410))
										{	/* Ieee/string.scm 962 */
											BgL_auxz00_6903 = BgL_g1141z00_1410;
										}
									else
										{
											obj_t BgL_auxz00_6906;

											BgL_auxz00_6906 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(41795L),
												BGl_string2729z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00,
												BgL_g1141z00_1410);
											FAILURE(BgL_auxz00_6906, BFALSE, BFALSE);
										}
									return
										BGl_stringzd2indexzd2zz__r4_strings_6_7z00(BgL_auxz00_6903,
										BgL_g1142z00_1411, BgL_startz00_1415);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* string-index */
	BGL_EXPORTED_DEF obj_t BGl_stringzd2indexzd2zz__r4_strings_6_7z00(obj_t
		BgL_stringz00_146, obj_t BgL_rsz00_147, obj_t BgL_startz00_148)
	{
		{	/* Ieee/string.scm 962 */
			if (CHARP(BgL_rsz00_147))
				{	/* Ieee/string.scm 224 */

					{	/* Ieee/string.scm 951 */
						long BgL_lenz00_3352;

						BgL_lenz00_3352 = STRING_LENGTH(BgL_stringz00_146);
						{	/* Ieee/string.scm 952 */

							if ((BgL_lenz00_3352 > (long) CINT(BgL_startz00_148)))
								{	/* Ieee/string.scm 957 */
									long BgL_startz00_3371;

									BgL_startz00_3371 = (long) CINT(BgL_startz00_148);
									{	/* Ieee/string.scm 932 */
										char *BgL_s0z00_3373;

										BgL_s0z00_3373 = BSTRING_TO_STRING(BgL_stringz00_146);
										{	/* Ieee/string.scm 932 */
											char *BgL_s1z00_3374;

											{	/* Ieee/string.scm 933 */
												long BgL_auxz00_6926;
												int BgL_tmpz00_6921;

												{	/* Ieee/string.scm 952 */
													bool_t BgL_test3218z00_6927;

													if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BINT
															(-1L)))
														{	/* Ieee/string.scm 952 */
															if ((-1L >= 0L))
																{	/* Ieee/string.scm 953 */
																	BgL_test3218z00_6927 =
																		(-1L <=
																		(BgL_lenz00_3352 - BgL_startz00_3371));
																}
															else
																{	/* Ieee/string.scm 953 */
																	BgL_test3218z00_6927 = ((bool_t) 0);
																}
														}
													else
														{	/* Ieee/string.scm 952 */
															BgL_test3218z00_6927 = ((bool_t) 0);
														}
													if (BgL_test3218z00_6927)
														{	/* Ieee/string.scm 952 */
															BgL_auxz00_6926 = -1L;
														}
													else
														{	/* Ieee/string.scm 952 */
															BgL_auxz00_6926 =
																(BgL_lenz00_3352 - BgL_startz00_3371);
														}
												}
												BgL_tmpz00_6921 =
													(int) (((unsigned char) (CCHAR(BgL_rsz00_147))));
												BgL_s1z00_3374 =
													BGL_MEMCHR(BgL_s0z00_3373, BgL_tmpz00_6921,
													BgL_auxz00_6926, BgL_startz00_3371);
											}
											{	/* Ieee/string.scm 933 */

												if (BGL_MEMCHR_ZERO(BgL_s1z00_3374))
													{	/* Ieee/string.scm 934 */
														return BFALSE;
													}
												else
													{	/* Ieee/string.scm 934 */
														return
															BINT(BGL_MEMCHR_DIFF(BgL_s1z00_3374,
																BgL_s0z00_3373));
													}
											}
										}
									}
								}
							else
								{	/* Ieee/string.scm 956 */
									return BFALSE;
								}
						}
					}
				}
			else
				{	/* Ieee/string.scm 964 */
					if (STRINGP(BgL_rsz00_147))
						{	/* Ieee/string.scm 966 */
							if ((STRING_LENGTH(BgL_rsz00_147) == 1L))
								{	/* Ieee/string.scm 224 */

									{	/* Ieee/string.scm 951 */
										long BgL_lenz00_3383;

										BgL_lenz00_3383 = STRING_LENGTH(BgL_stringz00_146);
										{	/* Ieee/string.scm 952 */

											if ((BgL_lenz00_3383 > (long) CINT(BgL_startz00_148)))
												{	/* Ieee/string.scm 957 */
													long BgL_startz00_3402;

													BgL_startz00_3402 = (long) CINT(BgL_startz00_148);
													{	/* Ieee/string.scm 932 */
														char *BgL_s0z00_3404;

														BgL_s0z00_3404 =
															BSTRING_TO_STRING(BgL_stringz00_146);
														{	/* Ieee/string.scm 932 */
															char *BgL_s1z00_3405;

															{	/* Ieee/string.scm 933 */
																long BgL_auxz00_6958;
																int BgL_tmpz00_6952;

																{	/* Ieee/string.scm 952 */
																	bool_t BgL_test3225z00_6959;

																	if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BINT(-1L)))
																		{	/* Ieee/string.scm 952 */
																			if ((-1L >= 0L))
																				{	/* Ieee/string.scm 953 */
																					BgL_test3225z00_6959 =
																						(-1L <=
																						(BgL_lenz00_3383 -
																							BgL_startz00_3402));
																				}
																			else
																				{	/* Ieee/string.scm 953 */
																					BgL_test3225z00_6959 = ((bool_t) 0);
																				}
																		}
																	else
																		{	/* Ieee/string.scm 952 */
																			BgL_test3225z00_6959 = ((bool_t) 0);
																		}
																	if (BgL_test3225z00_6959)
																		{	/* Ieee/string.scm 952 */
																			BgL_auxz00_6958 = -1L;
																		}
																	else
																		{	/* Ieee/string.scm 952 */
																			BgL_auxz00_6958 =
																				(BgL_lenz00_3383 - BgL_startz00_3402);
																		}
																}
																BgL_tmpz00_6952 =
																	(int) (
																	((unsigned char) (
																			(char) (STRING_REF(BgL_rsz00_147, 0L)))));
																BgL_s1z00_3405 =
																	BGL_MEMCHR(BgL_s0z00_3404, BgL_tmpz00_6952,
																	BgL_auxz00_6958, BgL_startz00_3402);
															}
															{	/* Ieee/string.scm 933 */

																if (BGL_MEMCHR_ZERO(BgL_s1z00_3405))
																	{	/* Ieee/string.scm 934 */
																		return BFALSE;
																	}
																else
																	{	/* Ieee/string.scm 934 */
																		return
																			BINT(BGL_MEMCHR_DIFF(BgL_s1z00_3405,
																				BgL_s0z00_3404));
																	}
															}
														}
													}
												}
											else
												{	/* Ieee/string.scm 956 */
													return BFALSE;
												}
										}
									}
								}
							else
								{	/* Ieee/string.scm 968 */
									if ((STRING_LENGTH(BgL_rsz00_147) <= 10L))
										{	/* Ieee/string.scm 971 */
											long BgL_lenz00_1431;
											long BgL_lenjz00_1432;

											BgL_lenz00_1431 = STRING_LENGTH(BgL_stringz00_146);
											BgL_lenjz00_1432 = STRING_LENGTH(BgL_rsz00_147);
											{
												obj_t BgL_iz00_1434;

												BgL_iz00_1434 = BgL_startz00_148;
											BgL_zc3z04anonymousza31529ze3z87_1435:
												if (((long) CINT(BgL_iz00_1434) >= BgL_lenz00_1431))
													{	/* Ieee/string.scm 974 */
														return BFALSE;
													}
												else
													{	/* Ieee/string.scm 976 */
														unsigned char BgL_cz00_1437;

														BgL_cz00_1437 =
															STRING_REF(BgL_stringz00_146,
															(long) CINT(BgL_iz00_1434));
														{
															long BgL_jz00_1439;

															BgL_jz00_1439 = 0L;
														BgL_zc3z04anonymousza31531ze3z87_1440:
															if ((BgL_jz00_1439 == BgL_lenjz00_1432))
																{
																	obj_t BgL_iz00_6985;

																	BgL_iz00_6985 =
																		ADDFX(BgL_iz00_1434, BINT(1L));
																	BgL_iz00_1434 = BgL_iz00_6985;
																	goto BgL_zc3z04anonymousza31529ze3z87_1435;
																}
															else
																{	/* Ieee/string.scm 978 */
																	if (
																		(BgL_cz00_1437 ==
																			STRING_REF(
																				((obj_t) BgL_rsz00_147),
																				BgL_jz00_1439)))
																		{	/* Ieee/string.scm 980 */
																			return BgL_iz00_1434;
																		}
																	else
																		{
																			long BgL_jz00_6992;

																			BgL_jz00_6992 = (BgL_jz00_1439 + 1L);
																			BgL_jz00_1439 = BgL_jz00_6992;
																			goto
																				BgL_zc3z04anonymousza31531ze3z87_1440;
																		}
																}
														}
													}
											}
										}
									else
										{	/* Ieee/string.scm 984 */
											obj_t BgL_tz00_1449;
											long BgL_lenz00_1450;

											BgL_tz00_1449 = make_string(256L, ((unsigned char) 'n'));
											BgL_lenz00_1450 = STRING_LENGTH(BgL_stringz00_146);
											{	/* Ieee/string.scm 986 */
												long BgL_g1030z00_1451;

												BgL_g1030z00_1451 = (STRING_LENGTH(BgL_rsz00_147) - 1L);
												{
													long BgL_iz00_1453;

													BgL_iz00_1453 = BgL_g1030z00_1451;
												BgL_zc3z04anonymousza31541ze3z87_1454:
													if ((BgL_iz00_1453 == -1L))
														{
															obj_t BgL_iz00_1457;

															BgL_iz00_1457 = BgL_startz00_148;
														BgL_zc3z04anonymousza31543ze3z87_1458:
															if (
																((long) CINT(BgL_iz00_1457) >= BgL_lenz00_1450))
																{	/* Ieee/string.scm 990 */
																	return BFALSE;
																}
															else
																{	/* Ieee/string.scm 990 */
																	if (
																		(STRING_REF(BgL_tz00_1449,
																				(STRING_REF(BgL_stringz00_146,
																						(long) CINT(BgL_iz00_1457)))) ==
																			((unsigned char) 'y')))
																		{	/* Ieee/string.scm 992 */
																			return BgL_iz00_1457;
																		}
																	else
																		{
																			obj_t BgL_iz00_7009;

																			BgL_iz00_7009 =
																				ADDFX(BgL_iz00_1457, BINT(1L));
																			BgL_iz00_1457 = BgL_iz00_7009;
																			goto
																				BgL_zc3z04anonymousza31543ze3z87_1458;
																		}
																}
														}
													else
														{	/* Ieee/string.scm 987 */
															{	/* Ieee/string.scm 999 */
																long BgL_arg1556z00_1469;

																BgL_arg1556z00_1469 =
																	(STRING_REF(
																		((obj_t) BgL_rsz00_147), BgL_iz00_1453));
																STRING_SET(BgL_tz00_1449, BgL_arg1556z00_1469,
																	((unsigned char) 'y'));
															}
															{
																long BgL_iz00_7016;

																BgL_iz00_7016 = (BgL_iz00_1453 - 1L);
																BgL_iz00_1453 = BgL_iz00_7016;
																goto BgL_zc3z04anonymousza31541ze3z87_1454;
															}
														}
												}
											}
										}
								}
						}
					else
						{	/* Ieee/string.scm 966 */
							return
								BGl_errorz00zz__errorz00(BGl_string2730z00zz__r4_strings_6_7z00,
								BGl_string2731z00zz__r4_strings_6_7z00, BgL_rsz00_147);
						}
				}
		}

	}



/* _string-index-right */
	obj_t BGl__stringzd2indexzd2rightz00zz__r4_strings_6_7z00(obj_t
		BgL_env1146z00_155, obj_t BgL_opt1145z00_154)
	{
		{	/* Ieee/string.scm 1005 */
			{	/* Ieee/string.scm 1005 */
				obj_t BgL_sz00_1476;
				obj_t BgL_g1147z00_1477;

				BgL_sz00_1476 = VECTOR_REF(BgL_opt1145z00_154, 0L);
				BgL_g1147z00_1477 = VECTOR_REF(BgL_opt1145z00_154, 1L);
				switch (VECTOR_LENGTH(BgL_opt1145z00_154))
					{
					case 2L:

						{	/* Ieee/string.scm 1005 */
							long BgL_startz00_1480;

							{	/* Ieee/string.scm 1005 */
								obj_t BgL_stringz00_3446;

								if (STRINGP(BgL_sz00_1476))
									{	/* Ieee/string.scm 1005 */
										BgL_stringz00_3446 = BgL_sz00_1476;
									}
								else
									{
										obj_t BgL_auxz00_7023;

										BgL_auxz00_7023 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2658z00zz__r4_strings_6_7z00, BINT(43190L),
											BGl_string2732z00zz__r4_strings_6_7z00,
											BGl_string2660z00zz__r4_strings_6_7z00, BgL_sz00_1476);
										FAILURE(BgL_auxz00_7023, BFALSE, BFALSE);
									}
								BgL_startz00_1480 = STRING_LENGTH(BgL_stringz00_3446);
							}
							{	/* Ieee/string.scm 1005 */

								{	/* Ieee/string.scm 1005 */
									obj_t BgL_auxz00_7028;

									if (STRINGP(BgL_sz00_1476))
										{	/* Ieee/string.scm 1005 */
											BgL_auxz00_7028 = BgL_sz00_1476;
										}
									else
										{
											obj_t BgL_auxz00_7031;

											BgL_auxz00_7031 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(43139L),
												BGl_string2732z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00, BgL_sz00_1476);
											FAILURE(BgL_auxz00_7031, BFALSE, BFALSE);
										}
									return
										BGl_stringzd2indexzd2rightz00zz__r4_strings_6_7z00
										(BgL_auxz00_7028, BgL_g1147z00_1477,
										BINT(BgL_startz00_1480));
								}
							}
						}
						break;
					case 3L:

						{	/* Ieee/string.scm 1005 */
							obj_t BgL_startz00_1481;

							BgL_startz00_1481 = VECTOR_REF(BgL_opt1145z00_154, 2L);
							{	/* Ieee/string.scm 1005 */

								{	/* Ieee/string.scm 1005 */
									obj_t BgL_auxz00_7038;

									if (STRINGP(BgL_sz00_1476))
										{	/* Ieee/string.scm 1005 */
											BgL_auxz00_7038 = BgL_sz00_1476;
										}
									else
										{
											obj_t BgL_auxz00_7041;

											BgL_auxz00_7041 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(43139L),
												BGl_string2732z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00, BgL_sz00_1476);
											FAILURE(BgL_auxz00_7041, BFALSE, BFALSE);
										}
									return
										BGl_stringzd2indexzd2rightz00zz__r4_strings_6_7z00
										(BgL_auxz00_7038, BgL_g1147z00_1477, BgL_startz00_1481);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* string-index-right */
	BGL_EXPORTED_DEF obj_t
		BGl_stringzd2indexzd2rightz00zz__r4_strings_6_7z00(obj_t BgL_sz00_151,
		obj_t BgL_rsz00_152, obj_t BgL_startz00_153)
	{
		{	/* Ieee/string.scm 1005 */
			{
				obj_t BgL_sz00_1540;
				obj_t BgL_cz00_1541;

				if (((long) CINT(BgL_startz00_153) > STRING_LENGTH(BgL_sz00_151)))
					{	/* Ieee/string.scm 1016 */
						return
							BGl_errorz00zz__errorz00(BGl_string2733z00zz__r4_strings_6_7z00,
							BGl_string2734z00zz__r4_strings_6_7z00, BgL_startz00_153);
					}
				else
					{	/* Ieee/string.scm 1016 */
						if (CHARP(BgL_rsz00_152))
							{	/* Ieee/string.scm 1018 */
								BgL_sz00_1540 = BgL_sz00_151;
								BgL_cz00_1541 = BgL_rsz00_152;
							BgL_zc3z04anonymousza31612ze3z87_1542:
								{
									long BgL_iz00_1545;

									BgL_iz00_1545 = ((long) CINT(BgL_startz00_153) - 1L);
								BgL_zc3z04anonymousza31613ze3z87_1546:
									if ((BgL_iz00_1545 < 0L))
										{	/* Ieee/string.scm 1009 */
											return BFALSE;
										}
									else
										{	/* Ieee/string.scm 1009 */
											if (
												(STRING_REF(BgL_sz00_1540, BgL_iz00_1545) ==
													CCHAR(BgL_cz00_1541)))
												{	/* Ieee/string.scm 1011 */
													return BINT(BgL_iz00_1545);
												}
											else
												{
													long BgL_iz00_7062;

													BgL_iz00_7062 = (BgL_iz00_1545 - 1L);
													BgL_iz00_1545 = BgL_iz00_7062;
													goto BgL_zc3z04anonymousza31613ze3z87_1546;
												}
										}
								}
							}
						else
							{	/* Ieee/string.scm 1018 */
								if (STRINGP(BgL_rsz00_152))
									{	/* Ieee/string.scm 1020 */
										if ((STRING_LENGTH(BgL_rsz00_152) == 1L))
											{
												obj_t BgL_cz00_7072;
												obj_t BgL_sz00_7071;

												BgL_sz00_7071 = BgL_sz00_151;
												BgL_cz00_7072 = BCHAR(STRING_REF(BgL_rsz00_152, 0L));
												BgL_cz00_1541 = BgL_cz00_7072;
												BgL_sz00_1540 = BgL_sz00_7071;
												goto BgL_zc3z04anonymousza31612ze3z87_1542;
											}
										else
											{	/* Ieee/string.scm 1022 */
												if ((STRING_LENGTH(BgL_rsz00_152) <= 10L))
													{	/* Ieee/string.scm 1025 */
														long BgL_lenjz00_1493;

														BgL_lenjz00_1493 = STRING_LENGTH(BgL_rsz00_152);
														{
															long BgL_iz00_1496;

															BgL_iz00_1496 =
																((long) CINT(BgL_startz00_153) - 1L);
														BgL_zc3z04anonymousza31577ze3z87_1497:
															if ((BgL_iz00_1496 < 0L))
																{	/* Ieee/string.scm 1028 */
																	return BFALSE;
																}
															else
																{	/* Ieee/string.scm 1030 */
																	unsigned char BgL_cz00_1499;

																	BgL_cz00_1499 =
																		STRING_REF(BgL_sz00_151, BgL_iz00_1496);
																	{
																		long BgL_jz00_1501;

																		BgL_jz00_1501 = 0L;
																	BgL_zc3z04anonymousza31579ze3z87_1502:
																		if ((BgL_jz00_1501 == BgL_lenjz00_1493))
																			{
																				long BgL_iz00_7084;

																				BgL_iz00_7084 = (BgL_iz00_1496 - 1L);
																				BgL_iz00_1496 = BgL_iz00_7084;
																				goto
																					BgL_zc3z04anonymousza31577ze3z87_1497;
																			}
																		else
																			{	/* Ieee/string.scm 1032 */
																				if (
																					(BgL_cz00_1499 ==
																						STRING_REF(
																							((obj_t) BgL_rsz00_152),
																							BgL_jz00_1501)))
																					{	/* Ieee/string.scm 1034 */
																						return BINT(BgL_iz00_1496);
																					}
																				else
																					{
																						long BgL_jz00_7091;

																						BgL_jz00_7091 =
																							(BgL_jz00_1501 + 1L);
																						BgL_jz00_1501 = BgL_jz00_7091;
																						goto
																							BgL_zc3z04anonymousza31579ze3z87_1502;
																					}
																			}
																	}
																}
														}
													}
												else
													{	/* Ieee/string.scm 1038 */
														obj_t BgL_tz00_1511;

														BgL_tz00_1511 =
															make_string(256L, ((unsigned char) 'n'));
														{	/* Ieee/string.scm 1040 */
															long BgL_g1034z00_1513;

															BgL_g1034z00_1513 =
																(STRING_LENGTH(BgL_rsz00_152) - 1L);
															{
																long BgL_iz00_1515;

																BgL_iz00_1515 = BgL_g1034z00_1513;
															BgL_zc3z04anonymousza31587ze3z87_1516:
																if ((BgL_iz00_1515 == -1L))
																	{
																		long BgL_iz00_1520;

																		BgL_iz00_1520 =
																			((long) CINT(BgL_startz00_153) - 1L);
																	BgL_zc3z04anonymousza31589ze3z87_1521:
																		if ((BgL_iz00_1520 < 0L))
																			{	/* Ieee/string.scm 1044 */
																				return BFALSE;
																			}
																		else
																			{	/* Ieee/string.scm 1044 */
																				if (
																					(STRING_REF(BgL_tz00_1511,
																							(STRING_REF(BgL_sz00_151,
																									BgL_iz00_1520))) ==
																						((unsigned char) 'y')))
																					{	/* Ieee/string.scm 1046 */
																						return BINT(BgL_iz00_1520);
																					}
																				else
																					{
																						long BgL_iz00_7108;

																						BgL_iz00_7108 =
																							(BgL_iz00_1520 - 1L);
																						BgL_iz00_1520 = BgL_iz00_7108;
																						goto
																							BgL_zc3z04anonymousza31589ze3z87_1521;
																					}
																			}
																	}
																else
																	{	/* Ieee/string.scm 1041 */
																		{	/* Ieee/string.scm 1053 */
																			long BgL_arg1605z00_1532;

																			BgL_arg1605z00_1532 =
																				(STRING_REF(
																					((obj_t) BgL_rsz00_152),
																					BgL_iz00_1515));
																			STRING_SET(BgL_tz00_1511,
																				BgL_arg1605z00_1532,
																				((unsigned char) 'y'));
																		}
																		{
																			long BgL_iz00_7116;

																			BgL_iz00_7116 = (BgL_iz00_1515 - 1L);
																			BgL_iz00_1515 = BgL_iz00_7116;
																			goto
																				BgL_zc3z04anonymousza31587ze3z87_1516;
																		}
																	}
															}
														}
													}
											}
									}
								else
									{	/* Ieee/string.scm 1020 */
										return
											BGl_errorz00zz__errorz00
											(BGl_string2733z00zz__r4_strings_6_7z00,
											BGl_string2731z00zz__r4_strings_6_7z00, BgL_rsz00_152);
									}
							}
					}
			}
		}

	}



/* _string-skip */
	obj_t BGl__stringzd2skipzd2zz__r4_strings_6_7z00(obj_t BgL_env1151z00_160,
		obj_t BgL_opt1150z00_159)
	{
		{	/* Ieee/string.scm 1059 */
			{	/* Ieee/string.scm 1059 */
				obj_t BgL_g1152z00_1554;
				obj_t BgL_g1153z00_1555;

				BgL_g1152z00_1554 = VECTOR_REF(BgL_opt1150z00_159, 0L);
				BgL_g1153z00_1555 = VECTOR_REF(BgL_opt1150z00_159, 1L);
				switch (VECTOR_LENGTH(BgL_opt1150z00_159))
					{
					case 2L:

						{	/* Ieee/string.scm 1059 */

							{	/* Ieee/string.scm 1059 */
								obj_t BgL_auxz00_7121;

								if (STRINGP(BgL_g1152z00_1554))
									{	/* Ieee/string.scm 1059 */
										BgL_auxz00_7121 = BgL_g1152z00_1554;
									}
								else
									{
										obj_t BgL_auxz00_7124;

										BgL_auxz00_7124 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2658z00zz__r4_strings_6_7z00, BINT(44760L),
											BGl_string2735z00zz__r4_strings_6_7z00,
											BGl_string2660z00zz__r4_strings_6_7z00,
											BgL_g1152z00_1554);
										FAILURE(BgL_auxz00_7124, BFALSE, BFALSE);
									}
								return
									BGl_stringzd2skipzd2zz__r4_strings_6_7z00(BgL_auxz00_7121,
									BgL_g1153z00_1555, BINT(0L));
							}
						}
						break;
					case 3L:

						{	/* Ieee/string.scm 1059 */
							obj_t BgL_startz00_1559;

							BgL_startz00_1559 = VECTOR_REF(BgL_opt1150z00_159, 2L);
							{	/* Ieee/string.scm 1059 */

								{	/* Ieee/string.scm 1059 */
									obj_t BgL_auxz00_7131;

									if (STRINGP(BgL_g1152z00_1554))
										{	/* Ieee/string.scm 1059 */
											BgL_auxz00_7131 = BgL_g1152z00_1554;
										}
									else
										{
											obj_t BgL_auxz00_7134;

											BgL_auxz00_7134 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(44760L),
												BGl_string2735z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00,
												BgL_g1152z00_1554);
											FAILURE(BgL_auxz00_7134, BFALSE, BFALSE);
										}
									return
										BGl_stringzd2skipzd2zz__r4_strings_6_7z00(BgL_auxz00_7131,
										BgL_g1153z00_1555, BgL_startz00_1559);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* string-skip */
	BGL_EXPORTED_DEF obj_t BGl_stringzd2skipzd2zz__r4_strings_6_7z00(obj_t
		BgL_stringz00_156, obj_t BgL_rsz00_157, obj_t BgL_startz00_158)
	{
		{	/* Ieee/string.scm 1059 */
			{
				obj_t BgL_sz00_1628;
				obj_t BgL_predz00_1629;
				obj_t BgL_sz00_1615;
				obj_t BgL_cz00_1616;

				if (CHARP(BgL_rsz00_157))
					{	/* Ieee/string.scm 1083 */
						BgL_sz00_1615 = BgL_stringz00_156;
						BgL_cz00_1616 = BgL_rsz00_157;
					BgL_zc3z04anonymousza31658ze3z87_1617:
						{	/* Ieee/string.scm 1062 */
							long BgL_lenz00_1618;

							BgL_lenz00_1618 = STRING_LENGTH(BgL_sz00_1615);
							{
								obj_t BgL_iz00_1620;

								BgL_iz00_1620 = BgL_startz00_158;
							BgL_zc3z04anonymousza31659ze3z87_1621:
								if (((long) CINT(BgL_iz00_1620) >= BgL_lenz00_1618))
									{	/* Ieee/string.scm 1065 */
										return BFALSE;
									}
								else
									{	/* Ieee/string.scm 1065 */
										if (
											(STRING_REF(BgL_sz00_1615,
													(long) CINT(BgL_iz00_1620)) == CCHAR(BgL_cz00_1616)))
											{
												obj_t BgL_iz00_7152;

												BgL_iz00_7152 = ADDFX(BgL_iz00_1620, BINT(1L));
												BgL_iz00_1620 = BgL_iz00_7152;
												goto BgL_zc3z04anonymousza31659ze3z87_1621;
											}
										else
											{	/* Ieee/string.scm 1067 */
												return BgL_iz00_1620;
											}
									}
							}
						}
					}
				else
					{	/* Ieee/string.scm 1083 */
						if (PROCEDUREP(BgL_rsz00_157))
							{	/* Ieee/string.scm 1085 */
								BgL_sz00_1628 = BgL_stringz00_156;
								BgL_predz00_1629 = BgL_rsz00_157;
								{	/* Ieee/string.scm 1072 */
									long BgL_lenz00_1631;

									BgL_lenz00_1631 = STRING_LENGTH(BgL_sz00_1628);
									{
										obj_t BgL_iz00_1633;

										BgL_iz00_1633 = BgL_startz00_158;
									BgL_zc3z04anonymousza31666ze3z87_1634:
										if (((long) CINT(BgL_iz00_1633) >= BgL_lenz00_1631))
											{	/* Ieee/string.scm 1075 */
												return BFALSE;
											}
										else
											{	/* Ieee/string.scm 1077 */
												bool_t BgL_test3259z00_7161;

												{	/* Ieee/string.scm 1077 */
													unsigned char BgL_arg1675z00_1639;

													BgL_arg1675z00_1639 =
														STRING_REF(BgL_sz00_1628,
														(long) CINT(BgL_iz00_1633));
													BgL_test3259z00_7161 =
														CBOOL(BGL_PROCEDURE_CALL1(BgL_predz00_1629,
															BCHAR(BgL_arg1675z00_1639)));
												}
												if (BgL_test3259z00_7161)
													{	/* Ieee/string.scm 1078 */
														long BgL_arg1670z00_1638;

														BgL_arg1670z00_1638 =
															((long) CINT(BgL_iz00_1633) + 1L);
														{
															obj_t BgL_iz00_7172;

															BgL_iz00_7172 = BINT(BgL_arg1670z00_1638);
															BgL_iz00_1633 = BgL_iz00_7172;
															goto BgL_zc3z04anonymousza31666ze3z87_1634;
														}
													}
												else
													{	/* Ieee/string.scm 1077 */
														return BgL_iz00_1633;
													}
											}
									}
								}
							}
						else
							{	/* Ieee/string.scm 1085 */
								if (STRINGP(BgL_rsz00_157))
									{	/* Ieee/string.scm 1087 */
										if ((STRING_LENGTH(BgL_rsz00_157) == 1L))
											{
												obj_t BgL_cz00_7180;
												obj_t BgL_sz00_7179;

												BgL_sz00_7179 = BgL_stringz00_156;
												BgL_cz00_7180 = BCHAR(STRING_REF(BgL_rsz00_157, 0L));
												BgL_cz00_1616 = BgL_cz00_7180;
												BgL_sz00_1615 = BgL_sz00_7179;
												goto BgL_zc3z04anonymousza31658ze3z87_1617;
											}
										else
											{	/* Ieee/string.scm 1089 */
												if ((STRING_LENGTH(BgL_rsz00_157) <= 10L))
													{	/* Ieee/string.scm 1092 */
														long BgL_lenz00_1570;
														long BgL_lenjz00_1571;

														BgL_lenz00_1570 = STRING_LENGTH(BgL_stringz00_156);
														BgL_lenjz00_1571 = STRING_LENGTH(BgL_rsz00_157);
														{
															obj_t BgL_iz00_1573;

															BgL_iz00_1573 = BgL_startz00_158;
														BgL_zc3z04anonymousza31627ze3z87_1574:
															if (
																((long) CINT(BgL_iz00_1573) >= BgL_lenz00_1570))
																{	/* Ieee/string.scm 1095 */
																	return BFALSE;
																}
															else
																{	/* Ieee/string.scm 1097 */
																	unsigned char BgL_cz00_1576;

																	BgL_cz00_1576 =
																		STRING_REF(BgL_stringz00_156,
																		(long) CINT(BgL_iz00_1573));
																	{
																		long BgL_jz00_1578;

																		BgL_jz00_1578 = 0L;
																	BgL_zc3z04anonymousza31629ze3z87_1579:
																		if ((BgL_jz00_1578 == BgL_lenjz00_1571))
																			{	/* Ieee/string.scm 1099 */
																				return BgL_iz00_1573;
																			}
																		else
																			{	/* Ieee/string.scm 1099 */
																				if (
																					(BgL_cz00_1576 ==
																						STRING_REF(
																							((obj_t) BgL_rsz00_157),
																							BgL_jz00_1578)))
																					{
																						obj_t BgL_iz00_7199;

																						BgL_iz00_7199 =
																							ADDFX(BgL_iz00_1573, BINT(1L));
																						BgL_iz00_1573 = BgL_iz00_7199;
																						goto
																							BgL_zc3z04anonymousza31627ze3z87_1574;
																					}
																				else
																					{
																						long BgL_jz00_7202;

																						BgL_jz00_7202 =
																							(BgL_jz00_1578 + 1L);
																						BgL_jz00_1578 = BgL_jz00_7202;
																						goto
																							BgL_zc3z04anonymousza31629ze3z87_1579;
																					}
																			}
																	}
																}
														}
													}
												else
													{	/* Ieee/string.scm 1105 */
														obj_t BgL_tz00_1588;
														long BgL_lenz00_1589;

														BgL_tz00_1588 =
															make_string(256L, ((unsigned char) 'n'));
														BgL_lenz00_1589 = STRING_LENGTH(BgL_stringz00_156);
														{	/* Ieee/string.scm 1107 */
															long BgL_g1037z00_1590;

															BgL_g1037z00_1590 =
																(STRING_LENGTH(BgL_rsz00_157) - 1L);
															{
																long BgL_iz00_1592;

																BgL_iz00_1592 = BgL_g1037z00_1590;
															BgL_zc3z04anonymousza31638ze3z87_1593:
																if ((BgL_iz00_1592 == -1L))
																	{
																		obj_t BgL_iz00_1596;

																		BgL_iz00_1596 = BgL_startz00_158;
																	BgL_zc3z04anonymousza31640ze3z87_1597:
																		if (
																			((long) CINT(BgL_iz00_1596) >=
																				BgL_lenz00_1589))
																			{	/* Ieee/string.scm 1111 */
																				return BFALSE;
																			}
																		else
																			{	/* Ieee/string.scm 1111 */
																				if (
																					(STRING_REF(BgL_tz00_1588,
																							(STRING_REF(BgL_stringz00_156,
																									(long) CINT(BgL_iz00_1596))))
																						== ((unsigned char) 'y')))
																					{
																						obj_t BgL_iz00_7219;

																						BgL_iz00_7219 =
																							ADDFX(BgL_iz00_1596, BINT(1L));
																						BgL_iz00_1596 = BgL_iz00_7219;
																						goto
																							BgL_zc3z04anonymousza31640ze3z87_1597;
																					}
																				else
																					{	/* Ieee/string.scm 1113 */
																						return BgL_iz00_1596;
																					}
																			}
																	}
																else
																	{	/* Ieee/string.scm 1108 */
																		{	/* Ieee/string.scm 1120 */
																			long BgL_arg1651z00_1608;

																			BgL_arg1651z00_1608 =
																				(STRING_REF(
																					((obj_t) BgL_rsz00_157),
																					BgL_iz00_1592));
																			STRING_SET(BgL_tz00_1588,
																				BgL_arg1651z00_1608,
																				((unsigned char) 'y'));
																		}
																		{
																			long BgL_iz00_7226;

																			BgL_iz00_7226 = (BgL_iz00_1592 - 1L);
																			BgL_iz00_1592 = BgL_iz00_7226;
																			goto
																				BgL_zc3z04anonymousza31638ze3z87_1593;
																		}
																	}
															}
														}
													}
											}
									}
								else
									{	/* Ieee/string.scm 1087 */
										return
											BGl_errorz00zz__errorz00
											(BGl_string2736z00zz__r4_strings_6_7z00,
											BGl_string2731z00zz__r4_strings_6_7z00, BgL_rsz00_157);
									}
							}
					}
			}
		}

	}



/* _string-skip-right */
	obj_t BGl__stringzd2skipzd2rightz00zz__r4_strings_6_7z00(obj_t
		BgL_env1157z00_165, obj_t BgL_opt1156z00_164)
	{
		{	/* Ieee/string.scm 1126 */
			{	/* Ieee/string.scm 1126 */
				obj_t BgL_sz00_1643;
				obj_t BgL_g1158z00_1644;

				BgL_sz00_1643 = VECTOR_REF(BgL_opt1156z00_164, 0L);
				BgL_g1158z00_1644 = VECTOR_REF(BgL_opt1156z00_164, 1L);
				switch (VECTOR_LENGTH(BgL_opt1156z00_164))
					{
					case 2L:

						{	/* Ieee/string.scm 1126 */
							long BgL_startz00_1647;

							{	/* Ieee/string.scm 1126 */
								obj_t BgL_stringz00_3551;

								if (STRINGP(BgL_sz00_1643))
									{	/* Ieee/string.scm 1126 */
										BgL_stringz00_3551 = BgL_sz00_1643;
									}
								else
									{
										obj_t BgL_auxz00_7233;

										BgL_auxz00_7233 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2658z00zz__r4_strings_6_7z00, BINT(46635L),
											BGl_string2737z00zz__r4_strings_6_7z00,
											BGl_string2660z00zz__r4_strings_6_7z00, BgL_sz00_1643);
										FAILURE(BgL_auxz00_7233, BFALSE, BFALSE);
									}
								BgL_startz00_1647 = STRING_LENGTH(BgL_stringz00_3551);
							}
							{	/* Ieee/string.scm 1126 */

								{	/* Ieee/string.scm 1126 */
									obj_t BgL_auxz00_7238;

									if (STRINGP(BgL_sz00_1643))
										{	/* Ieee/string.scm 1126 */
											BgL_auxz00_7238 = BgL_sz00_1643;
										}
									else
										{
											obj_t BgL_auxz00_7241;

											BgL_auxz00_7241 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(46585L),
												BGl_string2737z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00, BgL_sz00_1643);
											FAILURE(BgL_auxz00_7241, BFALSE, BFALSE);
										}
									return
										BGl_stringzd2skipzd2rightz00zz__r4_strings_6_7z00
										(BgL_auxz00_7238, BgL_g1158z00_1644,
										BINT(BgL_startz00_1647));
								}
							}
						}
						break;
					case 3L:

						{	/* Ieee/string.scm 1126 */
							obj_t BgL_startz00_1648;

							BgL_startz00_1648 = VECTOR_REF(BgL_opt1156z00_164, 2L);
							{	/* Ieee/string.scm 1126 */

								{	/* Ieee/string.scm 1126 */
									obj_t BgL_auxz00_7248;

									if (STRINGP(BgL_sz00_1643))
										{	/* Ieee/string.scm 1126 */
											BgL_auxz00_7248 = BgL_sz00_1643;
										}
									else
										{
											obj_t BgL_auxz00_7251;

											BgL_auxz00_7251 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(46585L),
												BGl_string2737z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00, BgL_sz00_1643);
											FAILURE(BgL_auxz00_7251, BFALSE, BFALSE);
										}
									return
										BGl_stringzd2skipzd2rightz00zz__r4_strings_6_7z00
										(BgL_auxz00_7248, BgL_g1158z00_1644, BgL_startz00_1648);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* string-skip-right */
	BGL_EXPORTED_DEF obj_t BGl_stringzd2skipzd2rightz00zz__r4_strings_6_7z00(obj_t
		BgL_sz00_161, obj_t BgL_rsz00_162, obj_t BgL_startz00_163)
	{
		{	/* Ieee/string.scm 1126 */
			{
				obj_t BgL_sz00_1722;
				obj_t BgL_predz00_1723;
				obj_t BgL_sz00_1709;
				obj_t BgL_cz00_1710;

				if (((long) CINT(BgL_startz00_163) > STRING_LENGTH(BgL_sz00_161)))
					{	/* Ieee/string.scm 1149 */
						return
							BGl_errorz00zz__errorz00(BGl_string2730z00zz__r4_strings_6_7z00,
							BGl_string2734z00zz__r4_strings_6_7z00, BgL_startz00_163);
					}
				else
					{	/* Ieee/string.scm 1149 */
						if (CHARP(BgL_rsz00_162))
							{	/* Ieee/string.scm 1151 */
								BgL_sz00_1709 = BgL_sz00_161;
								BgL_cz00_1710 = BgL_rsz00_162;
							BgL_zc3z04anonymousza31726ze3z87_1711:
								{
									long BgL_iz00_1714;

									BgL_iz00_1714 = ((long) CINT(BgL_startz00_163) - 1L);
								BgL_zc3z04anonymousza31727ze3z87_1715:
									if ((BgL_iz00_1714 < 0L))
										{	/* Ieee/string.scm 1131 */
											return BFALSE;
										}
									else
										{	/* Ieee/string.scm 1131 */
											if (
												(STRING_REF(BgL_sz00_1709, BgL_iz00_1714) ==
													CCHAR(BgL_cz00_1710)))
												{
													long BgL_iz00_7271;

													BgL_iz00_7271 = (BgL_iz00_1714 - 1L);
													BgL_iz00_1714 = BgL_iz00_7271;
													goto BgL_zc3z04anonymousza31727ze3z87_1715;
												}
											else
												{	/* Ieee/string.scm 1133 */
													return BINT(BgL_iz00_1714);
												}
										}
								}
							}
						else
							{	/* Ieee/string.scm 1151 */
								if (PROCEDUREP(BgL_rsz00_162))
									{	/* Ieee/string.scm 1153 */
										BgL_sz00_1722 = BgL_sz00_161;
										BgL_predz00_1723 = BgL_rsz00_162;
										{	/* Ieee/string.scm 1139 */
											long BgL_g1040z00_1725;

											BgL_g1040z00_1725 = ((long) CINT(BgL_startz00_163) - 1L);
											{
												long BgL_iz00_1727;

												BgL_iz00_1727 = BgL_g1040z00_1725;
											BgL_zc3z04anonymousza31735ze3z87_1728:
												if ((BgL_iz00_1727 < 0L))
													{	/* Ieee/string.scm 1141 */
														return BFALSE;
													}
												else
													{	/* Ieee/string.scm 1143 */
														bool_t BgL_test3278z00_7282;

														{	/* Ieee/string.scm 1143 */
															unsigned char BgL_arg1740z00_1733;

															BgL_arg1740z00_1733 =
																STRING_REF(BgL_sz00_1722, BgL_iz00_1727);
															BgL_test3278z00_7282 =
																CBOOL(BGL_PROCEDURE_CALL1(BgL_predz00_1723,
																	BCHAR(BgL_arg1740z00_1733)));
														}
														if (BgL_test3278z00_7282)
															{
																long BgL_iz00_7290;

																BgL_iz00_7290 = (BgL_iz00_1727 - 1L);
																BgL_iz00_1727 = BgL_iz00_7290;
																goto BgL_zc3z04anonymousza31735ze3z87_1728;
															}
														else
															{	/* Ieee/string.scm 1143 */
																return BINT(BgL_iz00_1727);
															}
													}
											}
										}
									}
								else
									{	/* Ieee/string.scm 1153 */
										if (STRINGP(BgL_rsz00_162))
											{	/* Ieee/string.scm 1155 */
												if ((STRING_LENGTH(BgL_rsz00_162) == 1L))
													{
														obj_t BgL_cz00_7299;
														obj_t BgL_sz00_7298;

														BgL_sz00_7298 = BgL_sz00_161;
														BgL_cz00_7299 =
															BCHAR(STRING_REF(BgL_rsz00_162, 0L));
														BgL_cz00_1710 = BgL_cz00_7299;
														BgL_sz00_1709 = BgL_sz00_7298;
														goto BgL_zc3z04anonymousza31726ze3z87_1711;
													}
												else
													{	/* Ieee/string.scm 1157 */
														if ((STRING_LENGTH(BgL_rsz00_162) <= 10L))
															{	/* Ieee/string.scm 1160 */
																long BgL_lenjz00_1662;

																BgL_lenjz00_1662 = STRING_LENGTH(BgL_rsz00_162);
																{
																	long BgL_iz00_1665;

																	BgL_iz00_1665 =
																		((long) CINT(BgL_startz00_163) - 1L);
																BgL_zc3z04anonymousza31687ze3z87_1666:
																	if ((BgL_iz00_1665 < 0L))
																		{	/* Ieee/string.scm 1163 */
																			return BFALSE;
																		}
																	else
																		{	/* Ieee/string.scm 1165 */
																			unsigned char BgL_cz00_1668;

																			BgL_cz00_1668 =
																				STRING_REF(BgL_sz00_161, BgL_iz00_1665);
																			{
																				long BgL_jz00_1670;

																				BgL_jz00_1670 = 0L;
																			BgL_zc3z04anonymousza31689ze3z87_1671:
																				if ((BgL_jz00_1670 == BgL_lenjz00_1662))
																					{	/* Ieee/string.scm 1167 */
																						return BINT(BgL_iz00_1665);
																					}
																				else
																					{	/* Ieee/string.scm 1167 */
																						if (
																							(BgL_cz00_1668 ==
																								STRING_REF(
																									((obj_t) BgL_rsz00_162),
																									BgL_jz00_1670)))
																							{
																								long BgL_iz00_7316;

																								BgL_iz00_7316 =
																									(BgL_iz00_1665 - 1L);
																								BgL_iz00_1665 = BgL_iz00_7316;
																								goto
																									BgL_zc3z04anonymousza31687ze3z87_1666;
																							}
																						else
																							{
																								long BgL_jz00_7318;

																								BgL_jz00_7318 =
																									(BgL_jz00_1670 + 1L);
																								BgL_jz00_1670 = BgL_jz00_7318;
																								goto
																									BgL_zc3z04anonymousza31689ze3z87_1671;
																							}
																					}
																			}
																		}
																}
															}
														else
															{	/* Ieee/string.scm 1173 */
																obj_t BgL_tz00_1680;

																BgL_tz00_1680 =
																	make_string(256L, ((unsigned char) 'n'));
																{	/* Ieee/string.scm 1175 */
																	long BgL_g1042z00_1682;

																	BgL_g1042z00_1682 =
																		(STRING_LENGTH(BgL_rsz00_162) - 1L);
																	{
																		long BgL_iz00_1684;

																		BgL_iz00_1684 = BgL_g1042z00_1682;
																	BgL_zc3z04anonymousza31702ze3z87_1685:
																		if ((BgL_iz00_1684 == -1L))
																			{
																				long BgL_iz00_1689;

																				BgL_iz00_1689 =
																					((long) CINT(BgL_startz00_163) - 1L);
																			BgL_zc3z04anonymousza31704ze3z87_1690:
																				if ((BgL_iz00_1689 < 0L))
																					{	/* Ieee/string.scm 1179 */
																						return BFALSE;
																					}
																				else
																					{	/* Ieee/string.scm 1179 */
																						if (
																							(STRING_REF(BgL_tz00_1680,
																									(STRING_REF(BgL_sz00_161,
																											BgL_iz00_1689))) ==
																								((unsigned char) 'y')))
																							{
																								long BgL_iz00_7334;

																								BgL_iz00_7334 =
																									(BgL_iz00_1689 - 1L);
																								BgL_iz00_1689 = BgL_iz00_7334;
																								goto
																									BgL_zc3z04anonymousza31704ze3z87_1690;
																							}
																						else
																							{	/* Ieee/string.scm 1181 */
																								return BINT(BgL_iz00_1689);
																							}
																					}
																			}
																		else
																			{	/* Ieee/string.scm 1176 */
																				{	/* Ieee/string.scm 1188 */
																					long BgL_arg1717z00_1701;

																					BgL_arg1717z00_1701 =
																						(STRING_REF(
																							((obj_t) BgL_rsz00_162),
																							BgL_iz00_1684));
																					STRING_SET(BgL_tz00_1680,
																						BgL_arg1717z00_1701,
																						((unsigned char) 'y'));
																				}
																				{
																					long BgL_iz00_7343;

																					BgL_iz00_7343 = (BgL_iz00_1684 - 1L);
																					BgL_iz00_1684 = BgL_iz00_7343;
																					goto
																						BgL_zc3z04anonymousza31702ze3z87_1685;
																				}
																			}
																	}
																}
															}
													}
											}
										else
											{	/* Ieee/string.scm 1155 */
												return
													BGl_errorz00zz__errorz00
													(BGl_string2733z00zz__r4_strings_6_7z00,
													BGl_string2731z00zz__r4_strings_6_7z00,
													BgL_rsz00_162);
											}
									}
							}
					}
			}
		}

	}



/* _string-prefix-length */
	obj_t BGl__stringzd2prefixzd2lengthz00zz__r4_strings_6_7z00(obj_t
		BgL_env1162z00_183, obj_t BgL_opt1161z00_182)
	{
		{	/* Ieee/string.scm 1222 */
			{	/* Ieee/string.scm 1222 */
				obj_t BgL_s1z00_1745;
				obj_t BgL_s2z00_1746;

				BgL_s1z00_1745 = VECTOR_REF(BgL_opt1161z00_182, 0L);
				BgL_s2z00_1746 = VECTOR_REF(BgL_opt1161z00_182, 1L);
				switch (VECTOR_LENGTH(BgL_opt1161z00_182))
					{
					case 2L:

						{	/* Ieee/string.scm 1222 */

							{	/* Ieee/string.scm 1222 */
								int BgL_tmpz00_7348;

								{	/* Ieee/string.scm 1222 */
									obj_t BgL_auxz00_7356;
									obj_t BgL_auxz00_7349;

									if (STRINGP(BgL_s2z00_1746))
										{	/* Ieee/string.scm 1222 */
											BgL_auxz00_7356 = BgL_s2z00_1746;
										}
									else
										{
											obj_t BgL_auxz00_7359;

											BgL_auxz00_7359 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(49488L),
												BGl_string2738z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00, BgL_s2z00_1746);
											FAILURE(BgL_auxz00_7359, BFALSE, BFALSE);
										}
									if (STRINGP(BgL_s1z00_1745))
										{	/* Ieee/string.scm 1222 */
											BgL_auxz00_7349 = BgL_s1z00_1745;
										}
									else
										{
											obj_t BgL_auxz00_7352;

											BgL_auxz00_7352 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(49488L),
												BGl_string2738z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00, BgL_s1z00_1745);
											FAILURE(BgL_auxz00_7352, BFALSE, BFALSE);
										}
									BgL_tmpz00_7348 =
										BGl_stringzd2prefixzd2lengthz00zz__r4_strings_6_7z00
										(BgL_auxz00_7349, BgL_auxz00_7356, BFALSE, BFALSE, BFALSE,
										BFALSE);
								}
								return BINT(BgL_tmpz00_7348);
							}
						}
						break;
					case 3L:

						{	/* Ieee/string.scm 1222 */
							obj_t BgL_start1z00_1753;

							BgL_start1z00_1753 = VECTOR_REF(BgL_opt1161z00_182, 2L);
							{	/* Ieee/string.scm 1222 */

								{	/* Ieee/string.scm 1222 */
									int BgL_tmpz00_7366;

									{	/* Ieee/string.scm 1222 */
										obj_t BgL_auxz00_7374;
										obj_t BgL_auxz00_7367;

										if (STRINGP(BgL_s2z00_1746))
											{	/* Ieee/string.scm 1222 */
												BgL_auxz00_7374 = BgL_s2z00_1746;
											}
										else
											{
												obj_t BgL_auxz00_7377;

												BgL_auxz00_7377 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2658z00zz__r4_strings_6_7z00, BINT(49488L),
													BGl_string2738z00zz__r4_strings_6_7z00,
													BGl_string2660z00zz__r4_strings_6_7z00,
													BgL_s2z00_1746);
												FAILURE(BgL_auxz00_7377, BFALSE, BFALSE);
											}
										if (STRINGP(BgL_s1z00_1745))
											{	/* Ieee/string.scm 1222 */
												BgL_auxz00_7367 = BgL_s1z00_1745;
											}
										else
											{
												obj_t BgL_auxz00_7370;

												BgL_auxz00_7370 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2658z00zz__r4_strings_6_7z00, BINT(49488L),
													BGl_string2738z00zz__r4_strings_6_7z00,
													BGl_string2660z00zz__r4_strings_6_7z00,
													BgL_s1z00_1745);
												FAILURE(BgL_auxz00_7370, BFALSE, BFALSE);
											}
										BgL_tmpz00_7366 =
											BGl_stringzd2prefixzd2lengthz00zz__r4_strings_6_7z00
											(BgL_auxz00_7367, BgL_auxz00_7374, BgL_start1z00_1753,
											BFALSE, BFALSE, BFALSE);
									}
									return BINT(BgL_tmpz00_7366);
								}
							}
						}
						break;
					case 4L:

						{	/* Ieee/string.scm 1222 */
							obj_t BgL_start1z00_1757;

							BgL_start1z00_1757 = VECTOR_REF(BgL_opt1161z00_182, 2L);
							{	/* Ieee/string.scm 1222 */
								obj_t BgL_end1z00_1758;

								BgL_end1z00_1758 = VECTOR_REF(BgL_opt1161z00_182, 3L);
								{	/* Ieee/string.scm 1222 */

									{	/* Ieee/string.scm 1222 */
										int BgL_tmpz00_7385;

										{	/* Ieee/string.scm 1222 */
											obj_t BgL_auxz00_7393;
											obj_t BgL_auxz00_7386;

											if (STRINGP(BgL_s2z00_1746))
												{	/* Ieee/string.scm 1222 */
													BgL_auxz00_7393 = BgL_s2z00_1746;
												}
											else
												{
													obj_t BgL_auxz00_7396;

													BgL_auxz00_7396 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2658z00zz__r4_strings_6_7z00,
														BINT(49488L),
														BGl_string2738z00zz__r4_strings_6_7z00,
														BGl_string2660z00zz__r4_strings_6_7z00,
														BgL_s2z00_1746);
													FAILURE(BgL_auxz00_7396, BFALSE, BFALSE);
												}
											if (STRINGP(BgL_s1z00_1745))
												{	/* Ieee/string.scm 1222 */
													BgL_auxz00_7386 = BgL_s1z00_1745;
												}
											else
												{
													obj_t BgL_auxz00_7389;

													BgL_auxz00_7389 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2658z00zz__r4_strings_6_7z00,
														BINT(49488L),
														BGl_string2738z00zz__r4_strings_6_7z00,
														BGl_string2660z00zz__r4_strings_6_7z00,
														BgL_s1z00_1745);
													FAILURE(BgL_auxz00_7389, BFALSE, BFALSE);
												}
											BgL_tmpz00_7385 =
												BGl_stringzd2prefixzd2lengthz00zz__r4_strings_6_7z00
												(BgL_auxz00_7386, BgL_auxz00_7393, BgL_start1z00_1757,
												BgL_end1z00_1758, BFALSE, BFALSE);
										}
										return BINT(BgL_tmpz00_7385);
									}
								}
							}
						}
						break;
					case 5L:

						{	/* Ieee/string.scm 1222 */
							obj_t BgL_start1z00_1761;

							BgL_start1z00_1761 = VECTOR_REF(BgL_opt1161z00_182, 2L);
							{	/* Ieee/string.scm 1222 */
								obj_t BgL_end1z00_1762;

								BgL_end1z00_1762 = VECTOR_REF(BgL_opt1161z00_182, 3L);
								{	/* Ieee/string.scm 1222 */
									obj_t BgL_start2z00_1763;

									BgL_start2z00_1763 = VECTOR_REF(BgL_opt1161z00_182, 4L);
									{	/* Ieee/string.scm 1222 */

										{	/* Ieee/string.scm 1222 */
											int BgL_tmpz00_7405;

											{	/* Ieee/string.scm 1222 */
												obj_t BgL_auxz00_7413;
												obj_t BgL_auxz00_7406;

												if (STRINGP(BgL_s2z00_1746))
													{	/* Ieee/string.scm 1222 */
														BgL_auxz00_7413 = BgL_s2z00_1746;
													}
												else
													{
														obj_t BgL_auxz00_7416;

														BgL_auxz00_7416 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2658z00zz__r4_strings_6_7z00,
															BINT(49488L),
															BGl_string2738z00zz__r4_strings_6_7z00,
															BGl_string2660z00zz__r4_strings_6_7z00,
															BgL_s2z00_1746);
														FAILURE(BgL_auxz00_7416, BFALSE, BFALSE);
													}
												if (STRINGP(BgL_s1z00_1745))
													{	/* Ieee/string.scm 1222 */
														BgL_auxz00_7406 = BgL_s1z00_1745;
													}
												else
													{
														obj_t BgL_auxz00_7409;

														BgL_auxz00_7409 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2658z00zz__r4_strings_6_7z00,
															BINT(49488L),
															BGl_string2738z00zz__r4_strings_6_7z00,
															BGl_string2660z00zz__r4_strings_6_7z00,
															BgL_s1z00_1745);
														FAILURE(BgL_auxz00_7409, BFALSE, BFALSE);
													}
												BgL_tmpz00_7405 =
													BGl_stringzd2prefixzd2lengthz00zz__r4_strings_6_7z00
													(BgL_auxz00_7406, BgL_auxz00_7413, BgL_start1z00_1761,
													BgL_end1z00_1762, BgL_start2z00_1763, BFALSE);
											}
											return BINT(BgL_tmpz00_7405);
										}
									}
								}
							}
						}
						break;
					case 6L:

						{	/* Ieee/string.scm 1222 */
							obj_t BgL_start1z00_1765;

							BgL_start1z00_1765 = VECTOR_REF(BgL_opt1161z00_182, 2L);
							{	/* Ieee/string.scm 1222 */
								obj_t BgL_end1z00_1766;

								BgL_end1z00_1766 = VECTOR_REF(BgL_opt1161z00_182, 3L);
								{	/* Ieee/string.scm 1222 */
									obj_t BgL_start2z00_1767;

									BgL_start2z00_1767 = VECTOR_REF(BgL_opt1161z00_182, 4L);
									{	/* Ieee/string.scm 1222 */
										obj_t BgL_end2z00_1768;

										BgL_end2z00_1768 = VECTOR_REF(BgL_opt1161z00_182, 5L);
										{	/* Ieee/string.scm 1222 */

											{	/* Ieee/string.scm 1222 */
												int BgL_tmpz00_7426;

												{	/* Ieee/string.scm 1222 */
													obj_t BgL_auxz00_7434;
													obj_t BgL_auxz00_7427;

													if (STRINGP(BgL_s2z00_1746))
														{	/* Ieee/string.scm 1222 */
															BgL_auxz00_7434 = BgL_s2z00_1746;
														}
													else
														{
															obj_t BgL_auxz00_7437;

															BgL_auxz00_7437 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2658z00zz__r4_strings_6_7z00,
																BINT(49488L),
																BGl_string2738z00zz__r4_strings_6_7z00,
																BGl_string2660z00zz__r4_strings_6_7z00,
																BgL_s2z00_1746);
															FAILURE(BgL_auxz00_7437, BFALSE, BFALSE);
														}
													if (STRINGP(BgL_s1z00_1745))
														{	/* Ieee/string.scm 1222 */
															BgL_auxz00_7427 = BgL_s1z00_1745;
														}
													else
														{
															obj_t BgL_auxz00_7430;

															BgL_auxz00_7430 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2658z00zz__r4_strings_6_7z00,
																BINT(49488L),
																BGl_string2738z00zz__r4_strings_6_7z00,
																BGl_string2660z00zz__r4_strings_6_7z00,
																BgL_s1z00_1745);
															FAILURE(BgL_auxz00_7430, BFALSE, BFALSE);
														}
													BgL_tmpz00_7426 =
														BGl_stringzd2prefixzd2lengthz00zz__r4_strings_6_7z00
														(BgL_auxz00_7427, BgL_auxz00_7434,
														BgL_start1z00_1765, BgL_end1z00_1766,
														BgL_start2z00_1767, BgL_end2z00_1768);
												}
												return BINT(BgL_tmpz00_7426);
											}
										}
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* string-prefix-length */
	BGL_EXPORTED_DEF int
		BGl_stringzd2prefixzd2lengthz00zz__r4_strings_6_7z00(obj_t BgL_s1z00_176,
		obj_t BgL_s2z00_177, obj_t BgL_start1z00_178, obj_t BgL_end1z00_179,
		obj_t BgL_start2z00_180, obj_t BgL_end2z00_181)
	{
		{	/* Ieee/string.scm 1222 */
			{	/* Ieee/string.scm 1224 */
				long BgL_l1z00_1769;

				BgL_l1z00_1769 = STRING_LENGTH(BgL_s1z00_176);
				{	/* Ieee/string.scm 1224 */
					long BgL_l2z00_1770;

					BgL_l2z00_1770 = STRING_LENGTH(BgL_s2z00_177);
					{	/* Ieee/string.scm 1225 */
						obj_t BgL_e1z00_1771;

						{	/* Ieee/string.scm 1226 */
							obj_t BgL_procz00_3623;

							BgL_procz00_3623 = BGl_symbol2739z00zz__r4_strings_6_7z00;
							if (CBOOL(BgL_end1z00_179))
								{	/* Ieee/string.scm 1210 */
									if (((long) CINT(BgL_end1z00_179) <= 0L))
										{	/* Ieee/string.scm 1212 */
											BgL_e1z00_1771 =
												BGl_errorz00zz__errorz00(BgL_procz00_3623,
												string_append_3(BGl_string2741z00zz__r4_strings_6_7z00,
													BGl_string2742z00zz__r4_strings_6_7z00,
													BGl_string2743z00zz__r4_strings_6_7z00),
												BgL_end1z00_179);
										}
									else
										{	/* Ieee/string.scm 1212 */
											if (((long) CINT(BgL_end1z00_179) > BgL_l1z00_1769))
												{	/* Ieee/string.scm 1214 */
													BgL_e1z00_1771 =
														BGl_errorz00zz__errorz00(BgL_procz00_3623,
														string_append_3
														(BGl_string2744z00zz__r4_strings_6_7z00,
															BGl_string2742z00zz__r4_strings_6_7z00,
															BGl_string2743z00zz__r4_strings_6_7z00),
														BgL_end1z00_179);
												}
											else
												{	/* Ieee/string.scm 1214 */
													BgL_e1z00_1771 = BgL_end1z00_179;
												}
										}
								}
							else
								{	/* Ieee/string.scm 1210 */
									BgL_e1z00_1771 = BINT(BgL_l1z00_1769);
								}
						}
						{	/* Ieee/string.scm 1226 */
							obj_t BgL_e2z00_1772;

							{	/* Ieee/string.scm 1227 */
								obj_t BgL_procz00_3631;

								BgL_procz00_3631 = BGl_symbol2739z00zz__r4_strings_6_7z00;
								if (CBOOL(BgL_end2z00_181))
									{	/* Ieee/string.scm 1210 */
										if (((long) CINT(BgL_end2z00_181) <= 0L))
											{	/* Ieee/string.scm 1212 */
												BgL_e2z00_1772 =
													BGl_errorz00zz__errorz00(BgL_procz00_3631,
													string_append_3
													(BGl_string2741z00zz__r4_strings_6_7z00,
														BGl_string2745z00zz__r4_strings_6_7z00,
														BGl_string2743z00zz__r4_strings_6_7z00),
													BgL_end2z00_181);
											}
										else
											{	/* Ieee/string.scm 1212 */
												if (((long) CINT(BgL_end2z00_181) > BgL_l2z00_1770))
													{	/* Ieee/string.scm 1214 */
														BgL_e2z00_1772 =
															BGl_errorz00zz__errorz00(BgL_procz00_3631,
															string_append_3
															(BGl_string2744z00zz__r4_strings_6_7z00,
																BGl_string2745z00zz__r4_strings_6_7z00,
																BGl_string2743z00zz__r4_strings_6_7z00),
															BgL_end2z00_181);
													}
												else
													{	/* Ieee/string.scm 1214 */
														BgL_e2z00_1772 = BgL_end2z00_181;
													}
											}
									}
								else
									{	/* Ieee/string.scm 1210 */
										BgL_e2z00_1772 = BINT(BgL_l2z00_1770);
									}
							}
							{	/* Ieee/string.scm 1227 */
								obj_t BgL_b1z00_1773;

								{	/* Ieee/string.scm 1228 */
									obj_t BgL_procz00_3639;

									BgL_procz00_3639 = BGl_symbol2739z00zz__r4_strings_6_7z00;
									if (CBOOL(BgL_start1z00_178))
										{	/* Ieee/string.scm 1196 */
											if (((long) CINT(BgL_start1z00_178) < 0L))
												{	/* Ieee/string.scm 1198 */
													BgL_b1z00_1773 =
														BGl_errorz00zz__errorz00(BgL_procz00_3639,
														string_append_3
														(BGl_string2746z00zz__r4_strings_6_7z00,
															BGl_string2747z00zz__r4_strings_6_7z00,
															BGl_string2743z00zz__r4_strings_6_7z00),
														BgL_start1z00_178);
												}
											else
												{	/* Ieee/string.scm 1198 */
													if (
														((long) CINT(BgL_start1z00_178) >= BgL_l1z00_1769))
														{	/* Ieee/string.scm 1200 */
															BgL_b1z00_1773 =
																BGl_errorz00zz__errorz00(BgL_procz00_3639,
																string_append_3
																(BGl_string2748z00zz__r4_strings_6_7z00,
																	BGl_string2747z00zz__r4_strings_6_7z00,
																	BGl_string2743z00zz__r4_strings_6_7z00),
																BgL_start1z00_178);
														}
													else
														{	/* Ieee/string.scm 1200 */
															BgL_b1z00_1773 = BgL_start1z00_178;
														}
												}
										}
									else
										{	/* Ieee/string.scm 1196 */
											BgL_b1z00_1773 = BINT(0L);
										}
								}
								{	/* Ieee/string.scm 1228 */
									obj_t BgL_b2z00_1774;

									{	/* Ieee/string.scm 1229 */
										obj_t BgL_procz00_3647;

										BgL_procz00_3647 = BGl_symbol2739z00zz__r4_strings_6_7z00;
										if (CBOOL(BgL_start2z00_180))
											{	/* Ieee/string.scm 1196 */
												if (((long) CINT(BgL_start2z00_180) < 0L))
													{	/* Ieee/string.scm 1198 */
														BgL_b2z00_1774 =
															BGl_errorz00zz__errorz00(BgL_procz00_3647,
															string_append_3
															(BGl_string2746z00zz__r4_strings_6_7z00,
																BGl_string2749z00zz__r4_strings_6_7z00,
																BGl_string2743z00zz__r4_strings_6_7z00),
															BgL_start2z00_180);
													}
												else
													{	/* Ieee/string.scm 1198 */
														if (
															((long) CINT(BgL_start2z00_180) >=
																BgL_l2z00_1770))
															{	/* Ieee/string.scm 1200 */
																BgL_b2z00_1774 =
																	BGl_errorz00zz__errorz00(BgL_procz00_3647,
																	string_append_3
																	(BGl_string2748z00zz__r4_strings_6_7z00,
																		BGl_string2749z00zz__r4_strings_6_7z00,
																		BGl_string2743z00zz__r4_strings_6_7z00),
																	BgL_start2z00_180);
															}
														else
															{	/* Ieee/string.scm 1200 */
																BgL_b2z00_1774 = BgL_start2z00_180;
															}
													}
											}
										else
											{	/* Ieee/string.scm 1196 */
												BgL_b2z00_1774 = BINT(0L);
											}
									}
									{	/* Ieee/string.scm 1229 */

										{
											obj_t BgL_i1z00_1776;
											obj_t BgL_i2z00_1777;

											{	/* Ieee/string.scm 1230 */
												long BgL_tmpz00_7499;

												BgL_i1z00_1776 = BgL_b1z00_1773;
												BgL_i2z00_1777 = BgL_b2z00_1774;
											BgL_zc3z04anonymousza31750ze3z87_1778:
												{	/* Ieee/string.scm 1233 */
													bool_t BgL_test3310z00_7500;

													if (
														((long) CINT(BgL_i1z00_1776) ==
															(long) CINT(BgL_e1z00_1771)))
														{	/* Ieee/string.scm 1233 */
															BgL_test3310z00_7500 = ((bool_t) 1);
														}
													else
														{	/* Ieee/string.scm 1233 */
															BgL_test3310z00_7500 =
																(
																(long) CINT(BgL_i2z00_1777) ==
																(long) CINT(BgL_e2z00_1772));
														}
													if (BgL_test3310z00_7500)
														{	/* Ieee/string.scm 1233 */
															BgL_tmpz00_7499 =
																(
																(long) CINT(BgL_i1z00_1776) -
																(long) CINT(BgL_b1z00_1773));
														}
													else
														{	/* Ieee/string.scm 1233 */
															if (
																(STRING_REF(BgL_s1z00_176,
																		(long) CINT(BgL_i1z00_1776)) ==
																	STRING_REF(BgL_s2z00_177,
																		(long) CINT(BgL_i2z00_1777))))
																{
																	obj_t BgL_i2z00_7520;
																	obj_t BgL_i1z00_7517;

																	BgL_i1z00_7517 =
																		ADDFX(BgL_i1z00_1776, BINT(1L));
																	BgL_i2z00_7520 =
																		ADDFX(BgL_i2z00_1777, BINT(1L));
																	BgL_i2z00_1777 = BgL_i2z00_7520;
																	BgL_i1z00_1776 = BgL_i1z00_7517;
																	goto BgL_zc3z04anonymousza31750ze3z87_1778;
																}
															else
																{	/* Ieee/string.scm 1235 */
																	BgL_tmpz00_7499 =
																		(
																		(long) CINT(BgL_i1z00_1776) -
																		(long) CINT(BgL_b1z00_1773));
												}}}
												return (int) (BgL_tmpz00_7499);
		}}}}}}}}}}

	}



/* _string-prefix-length-ci */
	obj_t BGl__stringzd2prefixzd2lengthzd2cizd2zz__r4_strings_6_7z00(obj_t
		BgL_env1166z00_191, obj_t BgL_opt1165z00_190)
	{
		{	/* Ieee/string.scm 1243 */
			{	/* Ieee/string.scm 1243 */
				obj_t BgL_s1z00_1790;
				obj_t BgL_s2z00_1791;

				BgL_s1z00_1790 = VECTOR_REF(BgL_opt1165z00_190, 0L);
				BgL_s2z00_1791 = VECTOR_REF(BgL_opt1165z00_190, 1L);
				switch (VECTOR_LENGTH(BgL_opt1165z00_190))
					{
					case 2L:

						{	/* Ieee/string.scm 1243 */

							{	/* Ieee/string.scm 1243 */
								int BgL_tmpz00_7529;

								{	/* Ieee/string.scm 1243 */
									obj_t BgL_auxz00_7537;
									obj_t BgL_auxz00_7530;

									if (STRINGP(BgL_s2z00_1791))
										{	/* Ieee/string.scm 1243 */
											BgL_auxz00_7537 = BgL_s2z00_1791;
										}
									else
										{
											obj_t BgL_auxz00_7540;

											BgL_auxz00_7540 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(50369L),
												BGl_string2750z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00, BgL_s2z00_1791);
											FAILURE(BgL_auxz00_7540, BFALSE, BFALSE);
										}
									if (STRINGP(BgL_s1z00_1790))
										{	/* Ieee/string.scm 1243 */
											BgL_auxz00_7530 = BgL_s1z00_1790;
										}
									else
										{
											obj_t BgL_auxz00_7533;

											BgL_auxz00_7533 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(50369L),
												BGl_string2750z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00, BgL_s1z00_1790);
											FAILURE(BgL_auxz00_7533, BFALSE, BFALSE);
										}
									BgL_tmpz00_7529 =
										BGl_stringzd2prefixzd2lengthzd2cizd2zz__r4_strings_6_7z00
										(BgL_auxz00_7530, BgL_auxz00_7537, BFALSE, BFALSE, BFALSE,
										BFALSE);
								}
								return BINT(BgL_tmpz00_7529);
							}
						}
						break;
					case 3L:

						{	/* Ieee/string.scm 1243 */
							obj_t BgL_start1z00_1798;

							BgL_start1z00_1798 = VECTOR_REF(BgL_opt1165z00_190, 2L);
							{	/* Ieee/string.scm 1243 */

								{	/* Ieee/string.scm 1243 */
									int BgL_tmpz00_7547;

									{	/* Ieee/string.scm 1243 */
										obj_t BgL_auxz00_7555;
										obj_t BgL_auxz00_7548;

										if (STRINGP(BgL_s2z00_1791))
											{	/* Ieee/string.scm 1243 */
												BgL_auxz00_7555 = BgL_s2z00_1791;
											}
										else
											{
												obj_t BgL_auxz00_7558;

												BgL_auxz00_7558 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2658z00zz__r4_strings_6_7z00, BINT(50369L),
													BGl_string2750z00zz__r4_strings_6_7z00,
													BGl_string2660z00zz__r4_strings_6_7z00,
													BgL_s2z00_1791);
												FAILURE(BgL_auxz00_7558, BFALSE, BFALSE);
											}
										if (STRINGP(BgL_s1z00_1790))
											{	/* Ieee/string.scm 1243 */
												BgL_auxz00_7548 = BgL_s1z00_1790;
											}
										else
											{
												obj_t BgL_auxz00_7551;

												BgL_auxz00_7551 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2658z00zz__r4_strings_6_7z00, BINT(50369L),
													BGl_string2750z00zz__r4_strings_6_7z00,
													BGl_string2660z00zz__r4_strings_6_7z00,
													BgL_s1z00_1790);
												FAILURE(BgL_auxz00_7551, BFALSE, BFALSE);
											}
										BgL_tmpz00_7547 =
											BGl_stringzd2prefixzd2lengthzd2cizd2zz__r4_strings_6_7z00
											(BgL_auxz00_7548, BgL_auxz00_7555, BgL_start1z00_1798,
											BFALSE, BFALSE, BFALSE);
									}
									return BINT(BgL_tmpz00_7547);
								}
							}
						}
						break;
					case 4L:

						{	/* Ieee/string.scm 1243 */
							obj_t BgL_start1z00_1802;

							BgL_start1z00_1802 = VECTOR_REF(BgL_opt1165z00_190, 2L);
							{	/* Ieee/string.scm 1243 */
								obj_t BgL_end1z00_1803;

								BgL_end1z00_1803 = VECTOR_REF(BgL_opt1165z00_190, 3L);
								{	/* Ieee/string.scm 1243 */

									{	/* Ieee/string.scm 1243 */
										int BgL_tmpz00_7566;

										{	/* Ieee/string.scm 1243 */
											obj_t BgL_auxz00_7574;
											obj_t BgL_auxz00_7567;

											if (STRINGP(BgL_s2z00_1791))
												{	/* Ieee/string.scm 1243 */
													BgL_auxz00_7574 = BgL_s2z00_1791;
												}
											else
												{
													obj_t BgL_auxz00_7577;

													BgL_auxz00_7577 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2658z00zz__r4_strings_6_7z00,
														BINT(50369L),
														BGl_string2750z00zz__r4_strings_6_7z00,
														BGl_string2660z00zz__r4_strings_6_7z00,
														BgL_s2z00_1791);
													FAILURE(BgL_auxz00_7577, BFALSE, BFALSE);
												}
											if (STRINGP(BgL_s1z00_1790))
												{	/* Ieee/string.scm 1243 */
													BgL_auxz00_7567 = BgL_s1z00_1790;
												}
											else
												{
													obj_t BgL_auxz00_7570;

													BgL_auxz00_7570 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2658z00zz__r4_strings_6_7z00,
														BINT(50369L),
														BGl_string2750z00zz__r4_strings_6_7z00,
														BGl_string2660z00zz__r4_strings_6_7z00,
														BgL_s1z00_1790);
													FAILURE(BgL_auxz00_7570, BFALSE, BFALSE);
												}
											BgL_tmpz00_7566 =
												BGl_stringzd2prefixzd2lengthzd2cizd2zz__r4_strings_6_7z00
												(BgL_auxz00_7567, BgL_auxz00_7574, BgL_start1z00_1802,
												BgL_end1z00_1803, BFALSE, BFALSE);
										}
										return BINT(BgL_tmpz00_7566);
									}
								}
							}
						}
						break;
					case 5L:

						{	/* Ieee/string.scm 1243 */
							obj_t BgL_start1z00_1806;

							BgL_start1z00_1806 = VECTOR_REF(BgL_opt1165z00_190, 2L);
							{	/* Ieee/string.scm 1243 */
								obj_t BgL_end1z00_1807;

								BgL_end1z00_1807 = VECTOR_REF(BgL_opt1165z00_190, 3L);
								{	/* Ieee/string.scm 1243 */
									obj_t BgL_start2z00_1808;

									BgL_start2z00_1808 = VECTOR_REF(BgL_opt1165z00_190, 4L);
									{	/* Ieee/string.scm 1243 */

										{	/* Ieee/string.scm 1243 */
											int BgL_tmpz00_7586;

											{	/* Ieee/string.scm 1243 */
												obj_t BgL_auxz00_7594;
												obj_t BgL_auxz00_7587;

												if (STRINGP(BgL_s2z00_1791))
													{	/* Ieee/string.scm 1243 */
														BgL_auxz00_7594 = BgL_s2z00_1791;
													}
												else
													{
														obj_t BgL_auxz00_7597;

														BgL_auxz00_7597 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2658z00zz__r4_strings_6_7z00,
															BINT(50369L),
															BGl_string2750z00zz__r4_strings_6_7z00,
															BGl_string2660z00zz__r4_strings_6_7z00,
															BgL_s2z00_1791);
														FAILURE(BgL_auxz00_7597, BFALSE, BFALSE);
													}
												if (STRINGP(BgL_s1z00_1790))
													{	/* Ieee/string.scm 1243 */
														BgL_auxz00_7587 = BgL_s1z00_1790;
													}
												else
													{
														obj_t BgL_auxz00_7590;

														BgL_auxz00_7590 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2658z00zz__r4_strings_6_7z00,
															BINT(50369L),
															BGl_string2750z00zz__r4_strings_6_7z00,
															BGl_string2660z00zz__r4_strings_6_7z00,
															BgL_s1z00_1790);
														FAILURE(BgL_auxz00_7590, BFALSE, BFALSE);
													}
												BgL_tmpz00_7586 =
													BGl_stringzd2prefixzd2lengthzd2cizd2zz__r4_strings_6_7z00
													(BgL_auxz00_7587, BgL_auxz00_7594, BgL_start1z00_1806,
													BgL_end1z00_1807, BgL_start2z00_1808, BFALSE);
											}
											return BINT(BgL_tmpz00_7586);
										}
									}
								}
							}
						}
						break;
					case 6L:

						{	/* Ieee/string.scm 1243 */
							obj_t BgL_start1z00_1810;

							BgL_start1z00_1810 = VECTOR_REF(BgL_opt1165z00_190, 2L);
							{	/* Ieee/string.scm 1243 */
								obj_t BgL_end1z00_1811;

								BgL_end1z00_1811 = VECTOR_REF(BgL_opt1165z00_190, 3L);
								{	/* Ieee/string.scm 1243 */
									obj_t BgL_start2z00_1812;

									BgL_start2z00_1812 = VECTOR_REF(BgL_opt1165z00_190, 4L);
									{	/* Ieee/string.scm 1243 */
										obj_t BgL_end2z00_1813;

										BgL_end2z00_1813 = VECTOR_REF(BgL_opt1165z00_190, 5L);
										{	/* Ieee/string.scm 1243 */

											{	/* Ieee/string.scm 1243 */
												int BgL_tmpz00_7607;

												{	/* Ieee/string.scm 1243 */
													obj_t BgL_auxz00_7615;
													obj_t BgL_auxz00_7608;

													if (STRINGP(BgL_s2z00_1791))
														{	/* Ieee/string.scm 1243 */
															BgL_auxz00_7615 = BgL_s2z00_1791;
														}
													else
														{
															obj_t BgL_auxz00_7618;

															BgL_auxz00_7618 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2658z00zz__r4_strings_6_7z00,
																BINT(50369L),
																BGl_string2750z00zz__r4_strings_6_7z00,
																BGl_string2660z00zz__r4_strings_6_7z00,
																BgL_s2z00_1791);
															FAILURE(BgL_auxz00_7618, BFALSE, BFALSE);
														}
													if (STRINGP(BgL_s1z00_1790))
														{	/* Ieee/string.scm 1243 */
															BgL_auxz00_7608 = BgL_s1z00_1790;
														}
													else
														{
															obj_t BgL_auxz00_7611;

															BgL_auxz00_7611 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2658z00zz__r4_strings_6_7z00,
																BINT(50369L),
																BGl_string2750z00zz__r4_strings_6_7z00,
																BGl_string2660z00zz__r4_strings_6_7z00,
																BgL_s1z00_1790);
															FAILURE(BgL_auxz00_7611, BFALSE, BFALSE);
														}
													BgL_tmpz00_7607 =
														BGl_stringzd2prefixzd2lengthzd2cizd2zz__r4_strings_6_7z00
														(BgL_auxz00_7608, BgL_auxz00_7615,
														BgL_start1z00_1810, BgL_end1z00_1811,
														BgL_start2z00_1812, BgL_end2z00_1813);
												}
												return BINT(BgL_tmpz00_7607);
											}
										}
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* string-prefix-length-ci */
	BGL_EXPORTED_DEF int
		BGl_stringzd2prefixzd2lengthzd2cizd2zz__r4_strings_6_7z00(obj_t
		BgL_s1z00_184, obj_t BgL_s2z00_185, obj_t BgL_start1z00_186,
		obj_t BgL_end1z00_187, obj_t BgL_start2z00_188, obj_t BgL_end2z00_189)
	{
		{	/* Ieee/string.scm 1243 */
			{	/* Ieee/string.scm 1245 */
				long BgL_l1z00_1814;

				BgL_l1z00_1814 = STRING_LENGTH(BgL_s1z00_184);
				{	/* Ieee/string.scm 1245 */
					long BgL_l2z00_1815;

					BgL_l2z00_1815 = STRING_LENGTH(BgL_s2z00_185);
					{	/* Ieee/string.scm 1246 */
						obj_t BgL_e1z00_1816;

						{	/* Ieee/string.scm 1247 */
							obj_t BgL_procz00_3673;

							BgL_procz00_3673 = BGl_symbol2751z00zz__r4_strings_6_7z00;
							if (CBOOL(BgL_end1z00_187))
								{	/* Ieee/string.scm 1210 */
									if (((long) CINT(BgL_end1z00_187) <= 0L))
										{	/* Ieee/string.scm 1212 */
											BgL_e1z00_1816 =
												BGl_errorz00zz__errorz00(BgL_procz00_3673,
												string_append_3(BGl_string2741z00zz__r4_strings_6_7z00,
													BGl_string2742z00zz__r4_strings_6_7z00,
													BGl_string2743z00zz__r4_strings_6_7z00),
												BgL_end1z00_187);
										}
									else
										{	/* Ieee/string.scm 1212 */
											if (((long) CINT(BgL_end1z00_187) > BgL_l1z00_1814))
												{	/* Ieee/string.scm 1214 */
													BgL_e1z00_1816 =
														BGl_errorz00zz__errorz00(BgL_procz00_3673,
														string_append_3
														(BGl_string2744z00zz__r4_strings_6_7z00,
															BGl_string2742z00zz__r4_strings_6_7z00,
															BGl_string2743z00zz__r4_strings_6_7z00),
														BgL_end1z00_187);
												}
											else
												{	/* Ieee/string.scm 1214 */
													BgL_e1z00_1816 = BgL_end1z00_187;
												}
										}
								}
							else
								{	/* Ieee/string.scm 1210 */
									BgL_e1z00_1816 = BINT(BgL_l1z00_1814);
								}
						}
						{	/* Ieee/string.scm 1247 */
							obj_t BgL_e2z00_1817;

							{	/* Ieee/string.scm 1248 */
								obj_t BgL_procz00_3681;

								BgL_procz00_3681 = BGl_symbol2751z00zz__r4_strings_6_7z00;
								if (CBOOL(BgL_end2z00_189))
									{	/* Ieee/string.scm 1210 */
										if (((long) CINT(BgL_end2z00_189) <= 0L))
											{	/* Ieee/string.scm 1212 */
												BgL_e2z00_1817 =
													BGl_errorz00zz__errorz00(BgL_procz00_3681,
													string_append_3
													(BGl_string2741z00zz__r4_strings_6_7z00,
														BGl_string2745z00zz__r4_strings_6_7z00,
														BGl_string2743z00zz__r4_strings_6_7z00),
													BgL_end2z00_189);
											}
										else
											{	/* Ieee/string.scm 1212 */
												if (((long) CINT(BgL_end2z00_189) > BgL_l2z00_1815))
													{	/* Ieee/string.scm 1214 */
														BgL_e2z00_1817 =
															BGl_errorz00zz__errorz00(BgL_procz00_3681,
															string_append_3
															(BGl_string2744z00zz__r4_strings_6_7z00,
																BGl_string2745z00zz__r4_strings_6_7z00,
																BGl_string2743z00zz__r4_strings_6_7z00),
															BgL_end2z00_189);
													}
												else
													{	/* Ieee/string.scm 1214 */
														BgL_e2z00_1817 = BgL_end2z00_189;
													}
											}
									}
								else
									{	/* Ieee/string.scm 1210 */
										BgL_e2z00_1817 = BINT(BgL_l2z00_1815);
									}
							}
							{	/* Ieee/string.scm 1248 */
								obj_t BgL_b1z00_1818;

								{	/* Ieee/string.scm 1249 */
									obj_t BgL_procz00_3689;

									BgL_procz00_3689 = BGl_symbol2751z00zz__r4_strings_6_7z00;
									if (CBOOL(BgL_start1z00_186))
										{	/* Ieee/string.scm 1196 */
											if (((long) CINT(BgL_start1z00_186) < 0L))
												{	/* Ieee/string.scm 1198 */
													BgL_b1z00_1818 =
														BGl_errorz00zz__errorz00(BgL_procz00_3689,
														string_append_3
														(BGl_string2746z00zz__r4_strings_6_7z00,
															BGl_string2747z00zz__r4_strings_6_7z00,
															BGl_string2743z00zz__r4_strings_6_7z00),
														BgL_start1z00_186);
												}
											else
												{	/* Ieee/string.scm 1198 */
													if (
														((long) CINT(BgL_start1z00_186) >= BgL_l1z00_1814))
														{	/* Ieee/string.scm 1200 */
															BgL_b1z00_1818 =
																BGl_errorz00zz__errorz00(BgL_procz00_3689,
																string_append_3
																(BGl_string2748z00zz__r4_strings_6_7z00,
																	BGl_string2747z00zz__r4_strings_6_7z00,
																	BGl_string2743z00zz__r4_strings_6_7z00),
																BgL_start1z00_186);
														}
													else
														{	/* Ieee/string.scm 1200 */
															BgL_b1z00_1818 = BgL_start1z00_186;
														}
												}
										}
									else
										{	/* Ieee/string.scm 1196 */
											BgL_b1z00_1818 = BINT(0L);
										}
								}
								{	/* Ieee/string.scm 1249 */
									obj_t BgL_b2z00_1819;

									{	/* Ieee/string.scm 1250 */
										obj_t BgL_procz00_3697;

										BgL_procz00_3697 = BGl_symbol2751z00zz__r4_strings_6_7z00;
										if (CBOOL(BgL_start2z00_188))
											{	/* Ieee/string.scm 1196 */
												if (((long) CINT(BgL_start2z00_188) < 0L))
													{	/* Ieee/string.scm 1198 */
														BgL_b2z00_1819 =
															BGl_errorz00zz__errorz00(BgL_procz00_3697,
															string_append_3
															(BGl_string2746z00zz__r4_strings_6_7z00,
																BGl_string2749z00zz__r4_strings_6_7z00,
																BGl_string2743z00zz__r4_strings_6_7z00),
															BgL_start2z00_188);
													}
												else
													{	/* Ieee/string.scm 1198 */
														if (
															((long) CINT(BgL_start2z00_188) >=
																BgL_l2z00_1815))
															{	/* Ieee/string.scm 1200 */
																BgL_b2z00_1819 =
																	BGl_errorz00zz__errorz00(BgL_procz00_3697,
																	string_append_3
																	(BGl_string2748z00zz__r4_strings_6_7z00,
																		BGl_string2749z00zz__r4_strings_6_7z00,
																		BGl_string2743z00zz__r4_strings_6_7z00),
																	BgL_start2z00_188);
															}
														else
															{	/* Ieee/string.scm 1200 */
																BgL_b2z00_1819 = BgL_start2z00_188;
															}
													}
											}
										else
											{	/* Ieee/string.scm 1196 */
												BgL_b2z00_1819 = BINT(0L);
											}
									}
									{	/* Ieee/string.scm 1250 */

										{
											obj_t BgL_i1z00_1821;
											obj_t BgL_i2z00_1822;

											{	/* Ieee/string.scm 1251 */
												long BgL_tmpz00_7680;

												BgL_i1z00_1821 = BgL_b1z00_1818;
												BgL_i2z00_1822 = BgL_b2z00_1819;
											BgL_zc3z04anonymousza31760ze3z87_1823:
												{	/* Ieee/string.scm 1254 */
													bool_t BgL_test3335z00_7681;

													if (
														((long) CINT(BgL_i1z00_1821) ==
															(long) CINT(BgL_e1z00_1816)))
														{	/* Ieee/string.scm 1254 */
															BgL_test3335z00_7681 = ((bool_t) 1);
														}
													else
														{	/* Ieee/string.scm 1254 */
															BgL_test3335z00_7681 =
																(
																(long) CINT(BgL_i2z00_1822) ==
																(long) CINT(BgL_e2z00_1817));
														}
													if (BgL_test3335z00_7681)
														{	/* Ieee/string.scm 1254 */
															BgL_tmpz00_7680 =
																(
																(long) CINT(BgL_i1z00_1821) -
																(long) CINT(BgL_b1z00_1818));
														}
													else
														{	/* Ieee/string.scm 1254 */
															if (
																(toupper(STRING_REF(BgL_s1z00_184,
																			(long) CINT(BgL_i1z00_1821))) ==
																	toupper(STRING_REF(BgL_s2z00_185,
																			(long) CINT(BgL_i2z00_1822)))))
																{
																	obj_t BgL_i2z00_7703;
																	obj_t BgL_i1z00_7700;

																	BgL_i1z00_7700 =
																		ADDFX(BgL_i1z00_1821, BINT(1L));
																	BgL_i2z00_7703 =
																		ADDFX(BgL_i2z00_1822, BINT(1L));
																	BgL_i2z00_1822 = BgL_i2z00_7703;
																	BgL_i1z00_1821 = BgL_i1z00_7700;
																	goto BgL_zc3z04anonymousza31760ze3z87_1823;
																}
															else
																{	/* Ieee/string.scm 1256 */
																	BgL_tmpz00_7680 =
																		(
																		(long) CINT(BgL_i1z00_1821) -
																		(long) CINT(BgL_b1z00_1818));
												}}}
												return (int) (BgL_tmpz00_7680);
		}}}}}}}}}}

	}



/* _string-suffix-length */
	obj_t BGl__stringzd2suffixzd2lengthz00zz__r4_strings_6_7z00(obj_t
		BgL_env1170z00_199, obj_t BgL_opt1169z00_198)
	{
		{	/* Ieee/string.scm 1264 */
			{	/* Ieee/string.scm 1264 */
				obj_t BgL_s1z00_1835;
				obj_t BgL_s2z00_1836;

				BgL_s1z00_1835 = VECTOR_REF(BgL_opt1169z00_198, 0L);
				BgL_s2z00_1836 = VECTOR_REF(BgL_opt1169z00_198, 1L);
				switch (VECTOR_LENGTH(BgL_opt1169z00_198))
					{
					case 2L:

						{	/* Ieee/string.scm 1264 */

							{	/* Ieee/string.scm 1264 */
								int BgL_tmpz00_7712;

								{	/* Ieee/string.scm 1264 */
									obj_t BgL_auxz00_7720;
									obj_t BgL_auxz00_7713;

									if (STRINGP(BgL_s2z00_1836))
										{	/* Ieee/string.scm 1264 */
											BgL_auxz00_7720 = BgL_s2z00_1836;
										}
									else
										{
											obj_t BgL_auxz00_7723;

											BgL_auxz00_7723 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(51271L),
												BGl_string2753z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00, BgL_s2z00_1836);
											FAILURE(BgL_auxz00_7723, BFALSE, BFALSE);
										}
									if (STRINGP(BgL_s1z00_1835))
										{	/* Ieee/string.scm 1264 */
											BgL_auxz00_7713 = BgL_s1z00_1835;
										}
									else
										{
											obj_t BgL_auxz00_7716;

											BgL_auxz00_7716 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(51271L),
												BGl_string2753z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00, BgL_s1z00_1835);
											FAILURE(BgL_auxz00_7716, BFALSE, BFALSE);
										}
									BgL_tmpz00_7712 =
										BGl_stringzd2suffixzd2lengthz00zz__r4_strings_6_7z00
										(BgL_auxz00_7713, BgL_auxz00_7720, BFALSE, BFALSE, BFALSE,
										BFALSE);
								}
								return BINT(BgL_tmpz00_7712);
							}
						}
						break;
					case 3L:

						{	/* Ieee/string.scm 1264 */
							obj_t BgL_start1z00_1843;

							BgL_start1z00_1843 = VECTOR_REF(BgL_opt1169z00_198, 2L);
							{	/* Ieee/string.scm 1264 */

								{	/* Ieee/string.scm 1264 */
									int BgL_tmpz00_7730;

									{	/* Ieee/string.scm 1264 */
										obj_t BgL_auxz00_7738;
										obj_t BgL_auxz00_7731;

										if (STRINGP(BgL_s2z00_1836))
											{	/* Ieee/string.scm 1264 */
												BgL_auxz00_7738 = BgL_s2z00_1836;
											}
										else
											{
												obj_t BgL_auxz00_7741;

												BgL_auxz00_7741 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2658z00zz__r4_strings_6_7z00, BINT(51271L),
													BGl_string2753z00zz__r4_strings_6_7z00,
													BGl_string2660z00zz__r4_strings_6_7z00,
													BgL_s2z00_1836);
												FAILURE(BgL_auxz00_7741, BFALSE, BFALSE);
											}
										if (STRINGP(BgL_s1z00_1835))
											{	/* Ieee/string.scm 1264 */
												BgL_auxz00_7731 = BgL_s1z00_1835;
											}
										else
											{
												obj_t BgL_auxz00_7734;

												BgL_auxz00_7734 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2658z00zz__r4_strings_6_7z00, BINT(51271L),
													BGl_string2753z00zz__r4_strings_6_7z00,
													BGl_string2660z00zz__r4_strings_6_7z00,
													BgL_s1z00_1835);
												FAILURE(BgL_auxz00_7734, BFALSE, BFALSE);
											}
										BgL_tmpz00_7730 =
											BGl_stringzd2suffixzd2lengthz00zz__r4_strings_6_7z00
											(BgL_auxz00_7731, BgL_auxz00_7738, BgL_start1z00_1843,
											BFALSE, BFALSE, BFALSE);
									}
									return BINT(BgL_tmpz00_7730);
								}
							}
						}
						break;
					case 4L:

						{	/* Ieee/string.scm 1264 */
							obj_t BgL_start1z00_1847;

							BgL_start1z00_1847 = VECTOR_REF(BgL_opt1169z00_198, 2L);
							{	/* Ieee/string.scm 1264 */
								obj_t BgL_end1z00_1848;

								BgL_end1z00_1848 = VECTOR_REF(BgL_opt1169z00_198, 3L);
								{	/* Ieee/string.scm 1264 */

									{	/* Ieee/string.scm 1264 */
										int BgL_tmpz00_7749;

										{	/* Ieee/string.scm 1264 */
											obj_t BgL_auxz00_7757;
											obj_t BgL_auxz00_7750;

											if (STRINGP(BgL_s2z00_1836))
												{	/* Ieee/string.scm 1264 */
													BgL_auxz00_7757 = BgL_s2z00_1836;
												}
											else
												{
													obj_t BgL_auxz00_7760;

													BgL_auxz00_7760 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2658z00zz__r4_strings_6_7z00,
														BINT(51271L),
														BGl_string2753z00zz__r4_strings_6_7z00,
														BGl_string2660z00zz__r4_strings_6_7z00,
														BgL_s2z00_1836);
													FAILURE(BgL_auxz00_7760, BFALSE, BFALSE);
												}
											if (STRINGP(BgL_s1z00_1835))
												{	/* Ieee/string.scm 1264 */
													BgL_auxz00_7750 = BgL_s1z00_1835;
												}
											else
												{
													obj_t BgL_auxz00_7753;

													BgL_auxz00_7753 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2658z00zz__r4_strings_6_7z00,
														BINT(51271L),
														BGl_string2753z00zz__r4_strings_6_7z00,
														BGl_string2660z00zz__r4_strings_6_7z00,
														BgL_s1z00_1835);
													FAILURE(BgL_auxz00_7753, BFALSE, BFALSE);
												}
											BgL_tmpz00_7749 =
												BGl_stringzd2suffixzd2lengthz00zz__r4_strings_6_7z00
												(BgL_auxz00_7750, BgL_auxz00_7757, BgL_start1z00_1847,
												BgL_end1z00_1848, BFALSE, BFALSE);
										}
										return BINT(BgL_tmpz00_7749);
									}
								}
							}
						}
						break;
					case 5L:

						{	/* Ieee/string.scm 1264 */
							obj_t BgL_start1z00_1851;

							BgL_start1z00_1851 = VECTOR_REF(BgL_opt1169z00_198, 2L);
							{	/* Ieee/string.scm 1264 */
								obj_t BgL_end1z00_1852;

								BgL_end1z00_1852 = VECTOR_REF(BgL_opt1169z00_198, 3L);
								{	/* Ieee/string.scm 1264 */
									obj_t BgL_start2z00_1853;

									BgL_start2z00_1853 = VECTOR_REF(BgL_opt1169z00_198, 4L);
									{	/* Ieee/string.scm 1264 */

										{	/* Ieee/string.scm 1264 */
											int BgL_tmpz00_7769;

											{	/* Ieee/string.scm 1264 */
												obj_t BgL_auxz00_7777;
												obj_t BgL_auxz00_7770;

												if (STRINGP(BgL_s2z00_1836))
													{	/* Ieee/string.scm 1264 */
														BgL_auxz00_7777 = BgL_s2z00_1836;
													}
												else
													{
														obj_t BgL_auxz00_7780;

														BgL_auxz00_7780 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2658z00zz__r4_strings_6_7z00,
															BINT(51271L),
															BGl_string2753z00zz__r4_strings_6_7z00,
															BGl_string2660z00zz__r4_strings_6_7z00,
															BgL_s2z00_1836);
														FAILURE(BgL_auxz00_7780, BFALSE, BFALSE);
													}
												if (STRINGP(BgL_s1z00_1835))
													{	/* Ieee/string.scm 1264 */
														BgL_auxz00_7770 = BgL_s1z00_1835;
													}
												else
													{
														obj_t BgL_auxz00_7773;

														BgL_auxz00_7773 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2658z00zz__r4_strings_6_7z00,
															BINT(51271L),
															BGl_string2753z00zz__r4_strings_6_7z00,
															BGl_string2660z00zz__r4_strings_6_7z00,
															BgL_s1z00_1835);
														FAILURE(BgL_auxz00_7773, BFALSE, BFALSE);
													}
												BgL_tmpz00_7769 =
													BGl_stringzd2suffixzd2lengthz00zz__r4_strings_6_7z00
													(BgL_auxz00_7770, BgL_auxz00_7777, BgL_start1z00_1851,
													BgL_end1z00_1852, BgL_start2z00_1853, BFALSE);
											}
											return BINT(BgL_tmpz00_7769);
										}
									}
								}
							}
						}
						break;
					case 6L:

						{	/* Ieee/string.scm 1264 */
							obj_t BgL_start1z00_1855;

							BgL_start1z00_1855 = VECTOR_REF(BgL_opt1169z00_198, 2L);
							{	/* Ieee/string.scm 1264 */
								obj_t BgL_end1z00_1856;

								BgL_end1z00_1856 = VECTOR_REF(BgL_opt1169z00_198, 3L);
								{	/* Ieee/string.scm 1264 */
									obj_t BgL_start2z00_1857;

									BgL_start2z00_1857 = VECTOR_REF(BgL_opt1169z00_198, 4L);
									{	/* Ieee/string.scm 1264 */
										obj_t BgL_end2z00_1858;

										BgL_end2z00_1858 = VECTOR_REF(BgL_opt1169z00_198, 5L);
										{	/* Ieee/string.scm 1264 */

											{	/* Ieee/string.scm 1264 */
												int BgL_tmpz00_7790;

												{	/* Ieee/string.scm 1264 */
													obj_t BgL_auxz00_7798;
													obj_t BgL_auxz00_7791;

													if (STRINGP(BgL_s2z00_1836))
														{	/* Ieee/string.scm 1264 */
															BgL_auxz00_7798 = BgL_s2z00_1836;
														}
													else
														{
															obj_t BgL_auxz00_7801;

															BgL_auxz00_7801 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2658z00zz__r4_strings_6_7z00,
																BINT(51271L),
																BGl_string2753z00zz__r4_strings_6_7z00,
																BGl_string2660z00zz__r4_strings_6_7z00,
																BgL_s2z00_1836);
															FAILURE(BgL_auxz00_7801, BFALSE, BFALSE);
														}
													if (STRINGP(BgL_s1z00_1835))
														{	/* Ieee/string.scm 1264 */
															BgL_auxz00_7791 = BgL_s1z00_1835;
														}
													else
														{
															obj_t BgL_auxz00_7794;

															BgL_auxz00_7794 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2658z00zz__r4_strings_6_7z00,
																BINT(51271L),
																BGl_string2753z00zz__r4_strings_6_7z00,
																BGl_string2660z00zz__r4_strings_6_7z00,
																BgL_s1z00_1835);
															FAILURE(BgL_auxz00_7794, BFALSE, BFALSE);
														}
													BgL_tmpz00_7790 =
														BGl_stringzd2suffixzd2lengthz00zz__r4_strings_6_7z00
														(BgL_auxz00_7791, BgL_auxz00_7798,
														BgL_start1z00_1855, BgL_end1z00_1856,
														BgL_start2z00_1857, BgL_end2z00_1858);
												}
												return BINT(BgL_tmpz00_7790);
											}
										}
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* string-suffix-length */
	BGL_EXPORTED_DEF int
		BGl_stringzd2suffixzd2lengthz00zz__r4_strings_6_7z00(obj_t BgL_s1z00_192,
		obj_t BgL_s2z00_193, obj_t BgL_start1z00_194, obj_t BgL_end1z00_195,
		obj_t BgL_start2z00_196, obj_t BgL_end2z00_197)
	{
		{	/* Ieee/string.scm 1264 */
			{	/* Ieee/string.scm 1266 */
				long BgL_l1z00_1859;

				BgL_l1z00_1859 = STRING_LENGTH(BgL_s1z00_192);
				{	/* Ieee/string.scm 1266 */
					long BgL_l2z00_1860;

					BgL_l2z00_1860 = STRING_LENGTH(BgL_s2z00_193);
					{	/* Ieee/string.scm 1267 */
						obj_t BgL_b1z00_1861;

						{	/* Ieee/string.scm 1268 */
							obj_t BgL_procz00_3729;

							BgL_procz00_3729 = BGl_symbol2754z00zz__r4_strings_6_7z00;
							if (CBOOL(BgL_end1z00_195))
								{	/* Ieee/string.scm 1210 */
									if (((long) CINT(BgL_end1z00_195) <= 0L))
										{	/* Ieee/string.scm 1212 */
											BgL_b1z00_1861 =
												BGl_errorz00zz__errorz00(BgL_procz00_3729,
												string_append_3(BGl_string2741z00zz__r4_strings_6_7z00,
													BGl_string2742z00zz__r4_strings_6_7z00,
													BGl_string2743z00zz__r4_strings_6_7z00),
												BgL_end1z00_195);
										}
									else
										{	/* Ieee/string.scm 1212 */
											if (((long) CINT(BgL_end1z00_195) > BgL_l1z00_1859))
												{	/* Ieee/string.scm 1214 */
													BgL_b1z00_1861 =
														BGl_errorz00zz__errorz00(BgL_procz00_3729,
														string_append_3
														(BGl_string2744z00zz__r4_strings_6_7z00,
															BGl_string2742z00zz__r4_strings_6_7z00,
															BGl_string2743z00zz__r4_strings_6_7z00),
														BgL_end1z00_195);
												}
											else
												{	/* Ieee/string.scm 1214 */
													BgL_b1z00_1861 = BgL_end1z00_195;
												}
										}
								}
							else
								{	/* Ieee/string.scm 1210 */
									BgL_b1z00_1861 = BINT(BgL_l1z00_1859);
								}
						}
						{	/* Ieee/string.scm 1268 */
							obj_t BgL_b2z00_1862;

							{	/* Ieee/string.scm 1269 */
								obj_t BgL_procz00_3737;

								BgL_procz00_3737 = BGl_symbol2754z00zz__r4_strings_6_7z00;
								if (CBOOL(BgL_end2z00_197))
									{	/* Ieee/string.scm 1210 */
										if (((long) CINT(BgL_end2z00_197) <= 0L))
											{	/* Ieee/string.scm 1212 */
												BgL_b2z00_1862 =
													BGl_errorz00zz__errorz00(BgL_procz00_3737,
													string_append_3
													(BGl_string2741z00zz__r4_strings_6_7z00,
														BGl_string2745z00zz__r4_strings_6_7z00,
														BGl_string2743z00zz__r4_strings_6_7z00),
													BgL_end2z00_197);
											}
										else
											{	/* Ieee/string.scm 1212 */
												if (((long) CINT(BgL_end2z00_197) > BgL_l2z00_1860))
													{	/* Ieee/string.scm 1214 */
														BgL_b2z00_1862 =
															BGl_errorz00zz__errorz00(BgL_procz00_3737,
															string_append_3
															(BGl_string2744z00zz__r4_strings_6_7z00,
																BGl_string2745z00zz__r4_strings_6_7z00,
																BGl_string2743z00zz__r4_strings_6_7z00),
															BgL_end2z00_197);
													}
												else
													{	/* Ieee/string.scm 1214 */
														BgL_b2z00_1862 = BgL_end2z00_197;
													}
											}
									}
								else
									{	/* Ieee/string.scm 1210 */
										BgL_b2z00_1862 = BINT(BgL_l2z00_1860);
									}
							}
							{	/* Ieee/string.scm 1269 */
								obj_t BgL_e1z00_1863;

								{	/* Ieee/string.scm 1270 */
									obj_t BgL_procz00_3745;

									BgL_procz00_3745 = BGl_symbol2754z00zz__r4_strings_6_7z00;
									if (CBOOL(BgL_start1z00_194))
										{	/* Ieee/string.scm 1196 */
											if (((long) CINT(BgL_start1z00_194) < 0L))
												{	/* Ieee/string.scm 1198 */
													BgL_e1z00_1863 =
														BGl_errorz00zz__errorz00(BgL_procz00_3745,
														string_append_3
														(BGl_string2746z00zz__r4_strings_6_7z00,
															BGl_string2747z00zz__r4_strings_6_7z00,
															BGl_string2743z00zz__r4_strings_6_7z00),
														BgL_start1z00_194);
												}
											else
												{	/* Ieee/string.scm 1198 */
													if (
														((long) CINT(BgL_start1z00_194) >= BgL_l1z00_1859))
														{	/* Ieee/string.scm 1200 */
															BgL_e1z00_1863 =
																BGl_errorz00zz__errorz00(BgL_procz00_3745,
																string_append_3
																(BGl_string2748z00zz__r4_strings_6_7z00,
																	BGl_string2747z00zz__r4_strings_6_7z00,
																	BGl_string2743z00zz__r4_strings_6_7z00),
																BgL_start1z00_194);
														}
													else
														{	/* Ieee/string.scm 1200 */
															BgL_e1z00_1863 = BgL_start1z00_194;
														}
												}
										}
									else
										{	/* Ieee/string.scm 1196 */
											BgL_e1z00_1863 = BINT(0L);
										}
								}
								{	/* Ieee/string.scm 1270 */
									obj_t BgL_e2z00_1864;

									{	/* Ieee/string.scm 1271 */
										obj_t BgL_procz00_3753;

										BgL_procz00_3753 = BGl_symbol2754z00zz__r4_strings_6_7z00;
										if (CBOOL(BgL_start2z00_196))
											{	/* Ieee/string.scm 1196 */
												if (((long) CINT(BgL_start2z00_196) < 0L))
													{	/* Ieee/string.scm 1198 */
														BgL_e2z00_1864 =
															BGl_errorz00zz__errorz00(BgL_procz00_3753,
															string_append_3
															(BGl_string2746z00zz__r4_strings_6_7z00,
																BGl_string2749z00zz__r4_strings_6_7z00,
																BGl_string2743z00zz__r4_strings_6_7z00),
															BgL_start2z00_196);
													}
												else
													{	/* Ieee/string.scm 1198 */
														if (
															((long) CINT(BgL_start2z00_196) >=
																BgL_l2z00_1860))
															{	/* Ieee/string.scm 1200 */
																BgL_e2z00_1864 =
																	BGl_errorz00zz__errorz00(BgL_procz00_3753,
																	string_append_3
																	(BGl_string2748z00zz__r4_strings_6_7z00,
																		BGl_string2749z00zz__r4_strings_6_7z00,
																		BGl_string2743z00zz__r4_strings_6_7z00),
																	BgL_start2z00_196);
															}
														else
															{	/* Ieee/string.scm 1200 */
																BgL_e2z00_1864 = BgL_start2z00_196;
															}
													}
											}
										else
											{	/* Ieee/string.scm 1196 */
												BgL_e2z00_1864 = BINT(0L);
											}
									}
									{	/* Ieee/string.scm 1271 */

										{
											long BgL_i1z00_1868;
											long BgL_i2z00_1869;

											{	/* Ieee/string.scm 1272 */
												long BgL_tmpz00_7863;

												BgL_i1z00_1868 = ((long) CINT(BgL_b1z00_1861) - 1L);
												BgL_i2z00_1869 = ((long) CINT(BgL_b2z00_1862) - 1L);
											BgL_zc3z04anonymousza31770ze3z87_1870:
												{	/* Ieee/string.scm 1275 */
													bool_t BgL_test3360z00_7864;

													if ((BgL_i1z00_1868 < (long) CINT(BgL_e1z00_1863)))
														{	/* Ieee/string.scm 1275 */
															BgL_test3360z00_7864 = ((bool_t) 1);
														}
													else
														{	/* Ieee/string.scm 1275 */
															BgL_test3360z00_7864 =
																(BgL_i2z00_1869 < (long) CINT(BgL_e2z00_1864));
														}
													if (BgL_test3360z00_7864)
														{	/* Ieee/string.scm 1275 */
															BgL_tmpz00_7863 =
																(
																(long) CINT(BgL_b1z00_1861) -
																(BgL_i1z00_1868 + 1L));
														}
													else
														{	/* Ieee/string.scm 1275 */
															if (
																(STRING_REF(BgL_s1z00_192, BgL_i1z00_1868) ==
																	STRING_REF(BgL_s2z00_193, BgL_i2z00_1869)))
																{
																	long BgL_i2z00_7879;
																	long BgL_i1z00_7877;

																	BgL_i1z00_7877 = (BgL_i1z00_1868 - 1L);
																	BgL_i2z00_7879 = (BgL_i2z00_1869 - 1L);
																	BgL_i2z00_1869 = BgL_i2z00_7879;
																	BgL_i1z00_1868 = BgL_i1z00_7877;
																	goto BgL_zc3z04anonymousza31770ze3z87_1870;
																}
															else
																{	/* Ieee/string.scm 1277 */
																	BgL_tmpz00_7863 =
																		(
																		(long) CINT(BgL_b1z00_1861) -
																		(BgL_i1z00_1868 + 1L));
												}}}
												return (int) (BgL_tmpz00_7863);
		}}}}}}}}}}

	}



/* _string-suffix-length-ci */
	obj_t BGl__stringzd2suffixzd2lengthzd2cizd2zz__r4_strings_6_7z00(obj_t
		BgL_env1174z00_207, obj_t BgL_opt1173z00_206)
	{
		{	/* Ieee/string.scm 1285 */
			{	/* Ieee/string.scm 1285 */
				obj_t BgL_s1z00_1884;
				obj_t BgL_s2z00_1885;

				BgL_s1z00_1884 = VECTOR_REF(BgL_opt1173z00_206, 0L);
				BgL_s2z00_1885 = VECTOR_REF(BgL_opt1173z00_206, 1L);
				switch (VECTOR_LENGTH(BgL_opt1173z00_206))
					{
					case 2L:

						{	/* Ieee/string.scm 1285 */

							{	/* Ieee/string.scm 1285 */
								int BgL_tmpz00_7891;

								{	/* Ieee/string.scm 1285 */
									obj_t BgL_auxz00_7899;
									obj_t BgL_auxz00_7892;

									if (STRINGP(BgL_s2z00_1885))
										{	/* Ieee/string.scm 1285 */
											BgL_auxz00_7899 = BgL_s2z00_1885;
										}
									else
										{
											obj_t BgL_auxz00_7902;

											BgL_auxz00_7902 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(52182L),
												BGl_string2756z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00, BgL_s2z00_1885);
											FAILURE(BgL_auxz00_7902, BFALSE, BFALSE);
										}
									if (STRINGP(BgL_s1z00_1884))
										{	/* Ieee/string.scm 1285 */
											BgL_auxz00_7892 = BgL_s1z00_1884;
										}
									else
										{
											obj_t BgL_auxz00_7895;

											BgL_auxz00_7895 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(52182L),
												BGl_string2756z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00, BgL_s1z00_1884);
											FAILURE(BgL_auxz00_7895, BFALSE, BFALSE);
										}
									BgL_tmpz00_7891 =
										BGl_stringzd2suffixzd2lengthzd2cizd2zz__r4_strings_6_7z00
										(BgL_auxz00_7892, BgL_auxz00_7899, BFALSE, BFALSE, BFALSE,
										BFALSE);
								}
								return BINT(BgL_tmpz00_7891);
							}
						}
						break;
					case 3L:

						{	/* Ieee/string.scm 1285 */
							obj_t BgL_start1z00_1892;

							BgL_start1z00_1892 = VECTOR_REF(BgL_opt1173z00_206, 2L);
							{	/* Ieee/string.scm 1285 */

								{	/* Ieee/string.scm 1285 */
									int BgL_tmpz00_7909;

									{	/* Ieee/string.scm 1285 */
										obj_t BgL_auxz00_7917;
										obj_t BgL_auxz00_7910;

										if (STRINGP(BgL_s2z00_1885))
											{	/* Ieee/string.scm 1285 */
												BgL_auxz00_7917 = BgL_s2z00_1885;
											}
										else
											{
												obj_t BgL_auxz00_7920;

												BgL_auxz00_7920 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2658z00zz__r4_strings_6_7z00, BINT(52182L),
													BGl_string2756z00zz__r4_strings_6_7z00,
													BGl_string2660z00zz__r4_strings_6_7z00,
													BgL_s2z00_1885);
												FAILURE(BgL_auxz00_7920, BFALSE, BFALSE);
											}
										if (STRINGP(BgL_s1z00_1884))
											{	/* Ieee/string.scm 1285 */
												BgL_auxz00_7910 = BgL_s1z00_1884;
											}
										else
											{
												obj_t BgL_auxz00_7913;

												BgL_auxz00_7913 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2658z00zz__r4_strings_6_7z00, BINT(52182L),
													BGl_string2756z00zz__r4_strings_6_7z00,
													BGl_string2660z00zz__r4_strings_6_7z00,
													BgL_s1z00_1884);
												FAILURE(BgL_auxz00_7913, BFALSE, BFALSE);
											}
										BgL_tmpz00_7909 =
											BGl_stringzd2suffixzd2lengthzd2cizd2zz__r4_strings_6_7z00
											(BgL_auxz00_7910, BgL_auxz00_7917, BgL_start1z00_1892,
											BFALSE, BFALSE, BFALSE);
									}
									return BINT(BgL_tmpz00_7909);
								}
							}
						}
						break;
					case 4L:

						{	/* Ieee/string.scm 1285 */
							obj_t BgL_start1z00_1896;

							BgL_start1z00_1896 = VECTOR_REF(BgL_opt1173z00_206, 2L);
							{	/* Ieee/string.scm 1285 */
								obj_t BgL_end1z00_1897;

								BgL_end1z00_1897 = VECTOR_REF(BgL_opt1173z00_206, 3L);
								{	/* Ieee/string.scm 1285 */

									{	/* Ieee/string.scm 1285 */
										int BgL_tmpz00_7928;

										{	/* Ieee/string.scm 1285 */
											obj_t BgL_auxz00_7936;
											obj_t BgL_auxz00_7929;

											if (STRINGP(BgL_s2z00_1885))
												{	/* Ieee/string.scm 1285 */
													BgL_auxz00_7936 = BgL_s2z00_1885;
												}
											else
												{
													obj_t BgL_auxz00_7939;

													BgL_auxz00_7939 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2658z00zz__r4_strings_6_7z00,
														BINT(52182L),
														BGl_string2756z00zz__r4_strings_6_7z00,
														BGl_string2660z00zz__r4_strings_6_7z00,
														BgL_s2z00_1885);
													FAILURE(BgL_auxz00_7939, BFALSE, BFALSE);
												}
											if (STRINGP(BgL_s1z00_1884))
												{	/* Ieee/string.scm 1285 */
													BgL_auxz00_7929 = BgL_s1z00_1884;
												}
											else
												{
													obj_t BgL_auxz00_7932;

													BgL_auxz00_7932 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2658z00zz__r4_strings_6_7z00,
														BINT(52182L),
														BGl_string2756z00zz__r4_strings_6_7z00,
														BGl_string2660z00zz__r4_strings_6_7z00,
														BgL_s1z00_1884);
													FAILURE(BgL_auxz00_7932, BFALSE, BFALSE);
												}
											BgL_tmpz00_7928 =
												BGl_stringzd2suffixzd2lengthzd2cizd2zz__r4_strings_6_7z00
												(BgL_auxz00_7929, BgL_auxz00_7936, BgL_start1z00_1896,
												BgL_end1z00_1897, BFALSE, BFALSE);
										}
										return BINT(BgL_tmpz00_7928);
									}
								}
							}
						}
						break;
					case 5L:

						{	/* Ieee/string.scm 1285 */
							obj_t BgL_start1z00_1900;

							BgL_start1z00_1900 = VECTOR_REF(BgL_opt1173z00_206, 2L);
							{	/* Ieee/string.scm 1285 */
								obj_t BgL_end1z00_1901;

								BgL_end1z00_1901 = VECTOR_REF(BgL_opt1173z00_206, 3L);
								{	/* Ieee/string.scm 1285 */
									obj_t BgL_start2z00_1902;

									BgL_start2z00_1902 = VECTOR_REF(BgL_opt1173z00_206, 4L);
									{	/* Ieee/string.scm 1285 */

										{	/* Ieee/string.scm 1285 */
											int BgL_tmpz00_7948;

											{	/* Ieee/string.scm 1285 */
												obj_t BgL_auxz00_7956;
												obj_t BgL_auxz00_7949;

												if (STRINGP(BgL_s2z00_1885))
													{	/* Ieee/string.scm 1285 */
														BgL_auxz00_7956 = BgL_s2z00_1885;
													}
												else
													{
														obj_t BgL_auxz00_7959;

														BgL_auxz00_7959 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2658z00zz__r4_strings_6_7z00,
															BINT(52182L),
															BGl_string2756z00zz__r4_strings_6_7z00,
															BGl_string2660z00zz__r4_strings_6_7z00,
															BgL_s2z00_1885);
														FAILURE(BgL_auxz00_7959, BFALSE, BFALSE);
													}
												if (STRINGP(BgL_s1z00_1884))
													{	/* Ieee/string.scm 1285 */
														BgL_auxz00_7949 = BgL_s1z00_1884;
													}
												else
													{
														obj_t BgL_auxz00_7952;

														BgL_auxz00_7952 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2658z00zz__r4_strings_6_7z00,
															BINT(52182L),
															BGl_string2756z00zz__r4_strings_6_7z00,
															BGl_string2660z00zz__r4_strings_6_7z00,
															BgL_s1z00_1884);
														FAILURE(BgL_auxz00_7952, BFALSE, BFALSE);
													}
												BgL_tmpz00_7948 =
													BGl_stringzd2suffixzd2lengthzd2cizd2zz__r4_strings_6_7z00
													(BgL_auxz00_7949, BgL_auxz00_7956, BgL_start1z00_1900,
													BgL_end1z00_1901, BgL_start2z00_1902, BFALSE);
											}
											return BINT(BgL_tmpz00_7948);
										}
									}
								}
							}
						}
						break;
					case 6L:

						{	/* Ieee/string.scm 1285 */
							obj_t BgL_start1z00_1904;

							BgL_start1z00_1904 = VECTOR_REF(BgL_opt1173z00_206, 2L);
							{	/* Ieee/string.scm 1285 */
								obj_t BgL_end1z00_1905;

								BgL_end1z00_1905 = VECTOR_REF(BgL_opt1173z00_206, 3L);
								{	/* Ieee/string.scm 1285 */
									obj_t BgL_start2z00_1906;

									BgL_start2z00_1906 = VECTOR_REF(BgL_opt1173z00_206, 4L);
									{	/* Ieee/string.scm 1285 */
										obj_t BgL_end2z00_1907;

										BgL_end2z00_1907 = VECTOR_REF(BgL_opt1173z00_206, 5L);
										{	/* Ieee/string.scm 1285 */

											{	/* Ieee/string.scm 1285 */
												int BgL_tmpz00_7969;

												{	/* Ieee/string.scm 1285 */
													obj_t BgL_auxz00_7977;
													obj_t BgL_auxz00_7970;

													if (STRINGP(BgL_s2z00_1885))
														{	/* Ieee/string.scm 1285 */
															BgL_auxz00_7977 = BgL_s2z00_1885;
														}
													else
														{
															obj_t BgL_auxz00_7980;

															BgL_auxz00_7980 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2658z00zz__r4_strings_6_7z00,
																BINT(52182L),
																BGl_string2756z00zz__r4_strings_6_7z00,
																BGl_string2660z00zz__r4_strings_6_7z00,
																BgL_s2z00_1885);
															FAILURE(BgL_auxz00_7980, BFALSE, BFALSE);
														}
													if (STRINGP(BgL_s1z00_1884))
														{	/* Ieee/string.scm 1285 */
															BgL_auxz00_7970 = BgL_s1z00_1884;
														}
													else
														{
															obj_t BgL_auxz00_7973;

															BgL_auxz00_7973 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2658z00zz__r4_strings_6_7z00,
																BINT(52182L),
																BGl_string2756z00zz__r4_strings_6_7z00,
																BGl_string2660z00zz__r4_strings_6_7z00,
																BgL_s1z00_1884);
															FAILURE(BgL_auxz00_7973, BFALSE, BFALSE);
														}
													BgL_tmpz00_7969 =
														BGl_stringzd2suffixzd2lengthzd2cizd2zz__r4_strings_6_7z00
														(BgL_auxz00_7970, BgL_auxz00_7977,
														BgL_start1z00_1904, BgL_end1z00_1905,
														BgL_start2z00_1906, BgL_end2z00_1907);
												}
												return BINT(BgL_tmpz00_7969);
											}
										}
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* string-suffix-length-ci */
	BGL_EXPORTED_DEF int
		BGl_stringzd2suffixzd2lengthzd2cizd2zz__r4_strings_6_7z00(obj_t
		BgL_s1z00_200, obj_t BgL_s2z00_201, obj_t BgL_start1z00_202,
		obj_t BgL_end1z00_203, obj_t BgL_start2z00_204, obj_t BgL_end2z00_205)
	{
		{	/* Ieee/string.scm 1285 */
			{	/* Ieee/string.scm 1287 */
				long BgL_l1z00_1908;

				BgL_l1z00_1908 = STRING_LENGTH(BgL_s1z00_200);
				{	/* Ieee/string.scm 1287 */
					long BgL_l2z00_1909;

					BgL_l2z00_1909 = STRING_LENGTH(BgL_s2z00_201);
					{	/* Ieee/string.scm 1288 */
						obj_t BgL_b1z00_1910;

						{	/* Ieee/string.scm 1289 */
							obj_t BgL_procz00_3783;

							BgL_procz00_3783 = BGl_symbol2757z00zz__r4_strings_6_7z00;
							if (CBOOL(BgL_end1z00_203))
								{	/* Ieee/string.scm 1210 */
									if (((long) CINT(BgL_end1z00_203) <= 0L))
										{	/* Ieee/string.scm 1212 */
											BgL_b1z00_1910 =
												BGl_errorz00zz__errorz00(BgL_procz00_3783,
												string_append_3(BGl_string2741z00zz__r4_strings_6_7z00,
													BGl_string2742z00zz__r4_strings_6_7z00,
													BGl_string2743z00zz__r4_strings_6_7z00),
												BgL_end1z00_203);
										}
									else
										{	/* Ieee/string.scm 1212 */
											if (((long) CINT(BgL_end1z00_203) > BgL_l1z00_1908))
												{	/* Ieee/string.scm 1214 */
													BgL_b1z00_1910 =
														BGl_errorz00zz__errorz00(BgL_procz00_3783,
														string_append_3
														(BGl_string2744z00zz__r4_strings_6_7z00,
															BGl_string2742z00zz__r4_strings_6_7z00,
															BGl_string2743z00zz__r4_strings_6_7z00),
														BgL_end1z00_203);
												}
											else
												{	/* Ieee/string.scm 1214 */
													BgL_b1z00_1910 = BgL_end1z00_203;
												}
										}
								}
							else
								{	/* Ieee/string.scm 1210 */
									BgL_b1z00_1910 = BINT(BgL_l1z00_1908);
								}
						}
						{	/* Ieee/string.scm 1289 */
							obj_t BgL_b2z00_1911;

							{	/* Ieee/string.scm 1290 */
								obj_t BgL_procz00_3791;

								BgL_procz00_3791 = BGl_symbol2757z00zz__r4_strings_6_7z00;
								if (CBOOL(BgL_end2z00_205))
									{	/* Ieee/string.scm 1210 */
										if (((long) CINT(BgL_end2z00_205) <= 0L))
											{	/* Ieee/string.scm 1212 */
												BgL_b2z00_1911 =
													BGl_errorz00zz__errorz00(BgL_procz00_3791,
													string_append_3
													(BGl_string2741z00zz__r4_strings_6_7z00,
														BGl_string2745z00zz__r4_strings_6_7z00,
														BGl_string2743z00zz__r4_strings_6_7z00),
													BgL_end2z00_205);
											}
										else
											{	/* Ieee/string.scm 1212 */
												if (((long) CINT(BgL_end2z00_205) > BgL_l2z00_1909))
													{	/* Ieee/string.scm 1214 */
														BgL_b2z00_1911 =
															BGl_errorz00zz__errorz00(BgL_procz00_3791,
															string_append_3
															(BGl_string2744z00zz__r4_strings_6_7z00,
																BGl_string2745z00zz__r4_strings_6_7z00,
																BGl_string2743z00zz__r4_strings_6_7z00),
															BgL_end2z00_205);
													}
												else
													{	/* Ieee/string.scm 1214 */
														BgL_b2z00_1911 = BgL_end2z00_205;
													}
											}
									}
								else
									{	/* Ieee/string.scm 1210 */
										BgL_b2z00_1911 = BINT(BgL_l2z00_1909);
									}
							}
							{	/* Ieee/string.scm 1290 */
								obj_t BgL_e1z00_1912;

								{	/* Ieee/string.scm 1291 */
									obj_t BgL_procz00_3799;

									BgL_procz00_3799 = BGl_symbol2757z00zz__r4_strings_6_7z00;
									if (CBOOL(BgL_start1z00_202))
										{	/* Ieee/string.scm 1196 */
											if (((long) CINT(BgL_start1z00_202) < 0L))
												{	/* Ieee/string.scm 1198 */
													BgL_e1z00_1912 =
														BGl_errorz00zz__errorz00(BgL_procz00_3799,
														string_append_3
														(BGl_string2746z00zz__r4_strings_6_7z00,
															BGl_string2747z00zz__r4_strings_6_7z00,
															BGl_string2743z00zz__r4_strings_6_7z00),
														BgL_start1z00_202);
												}
											else
												{	/* Ieee/string.scm 1198 */
													if (
														((long) CINT(BgL_start1z00_202) >= BgL_l1z00_1908))
														{	/* Ieee/string.scm 1200 */
															BgL_e1z00_1912 =
																BGl_errorz00zz__errorz00(BgL_procz00_3799,
																string_append_3
																(BGl_string2748z00zz__r4_strings_6_7z00,
																	BGl_string2747z00zz__r4_strings_6_7z00,
																	BGl_string2743z00zz__r4_strings_6_7z00),
																BgL_start1z00_202);
														}
													else
														{	/* Ieee/string.scm 1200 */
															BgL_e1z00_1912 = BgL_start1z00_202;
														}
												}
										}
									else
										{	/* Ieee/string.scm 1196 */
											BgL_e1z00_1912 = BINT(0L);
										}
								}
								{	/* Ieee/string.scm 1291 */
									obj_t BgL_e2z00_1913;

									{	/* Ieee/string.scm 1292 */
										obj_t BgL_procz00_3807;

										BgL_procz00_3807 = BGl_symbol2757z00zz__r4_strings_6_7z00;
										if (CBOOL(BgL_start2z00_204))
											{	/* Ieee/string.scm 1196 */
												if (((long) CINT(BgL_start2z00_204) < 0L))
													{	/* Ieee/string.scm 1198 */
														BgL_e2z00_1913 =
															BGl_errorz00zz__errorz00(BgL_procz00_3807,
															string_append_3
															(BGl_string2746z00zz__r4_strings_6_7z00,
																BGl_string2749z00zz__r4_strings_6_7z00,
																BGl_string2743z00zz__r4_strings_6_7z00),
															BgL_start2z00_204);
													}
												else
													{	/* Ieee/string.scm 1198 */
														if (
															((long) CINT(BgL_start2z00_204) >=
																BgL_l2z00_1909))
															{	/* Ieee/string.scm 1200 */
																BgL_e2z00_1913 =
																	BGl_errorz00zz__errorz00(BgL_procz00_3807,
																	string_append_3
																	(BGl_string2748z00zz__r4_strings_6_7z00,
																		BGl_string2749z00zz__r4_strings_6_7z00,
																		BGl_string2743z00zz__r4_strings_6_7z00),
																	BgL_start2z00_204);
															}
														else
															{	/* Ieee/string.scm 1200 */
																BgL_e2z00_1913 = BgL_start2z00_204;
															}
													}
											}
										else
											{	/* Ieee/string.scm 1196 */
												BgL_e2z00_1913 = BINT(0L);
											}
									}
									{	/* Ieee/string.scm 1292 */

										{
											long BgL_i1z00_1917;
											long BgL_i2z00_1918;

											{	/* Ieee/string.scm 1293 */
												long BgL_tmpz00_8042;

												BgL_i1z00_1917 = ((long) CINT(BgL_b1z00_1910) - 1L);
												BgL_i2z00_1918 = ((long) CINT(BgL_b2z00_1911) - 1L);
											BgL_zc3z04anonymousza31784ze3z87_1919:
												{	/* Ieee/string.scm 1296 */
													bool_t BgL_test3385z00_8043;

													if ((BgL_i1z00_1917 < (long) CINT(BgL_e1z00_1912)))
														{	/* Ieee/string.scm 1296 */
															BgL_test3385z00_8043 = ((bool_t) 1);
														}
													else
														{	/* Ieee/string.scm 1296 */
															BgL_test3385z00_8043 =
																(BgL_i2z00_1918 < (long) CINT(BgL_e2z00_1913));
														}
													if (BgL_test3385z00_8043)
														{	/* Ieee/string.scm 1296 */
															BgL_tmpz00_8042 =
																(
																(long) CINT(BgL_b1z00_1910) -
																(BgL_i1z00_1917 + 1L));
														}
													else
														{	/* Ieee/string.scm 1296 */
															if (
																(toupper(STRING_REF(BgL_s1z00_200,
																			BgL_i1z00_1917)) ==
																	toupper(STRING_REF(BgL_s2z00_201,
																			BgL_i2z00_1918))))
																{
																	long BgL_i2z00_8060;
																	long BgL_i1z00_8058;

																	BgL_i1z00_8058 = (BgL_i1z00_1917 - 1L);
																	BgL_i2z00_8060 = (BgL_i2z00_1918 - 1L);
																	BgL_i2z00_1918 = BgL_i2z00_8060;
																	BgL_i1z00_1917 = BgL_i1z00_8058;
																	goto BgL_zc3z04anonymousza31784ze3z87_1919;
																}
															else
																{	/* Ieee/string.scm 1298 */
																	BgL_tmpz00_8042 =
																		(
																		(long) CINT(BgL_b1z00_1910) -
																		(BgL_i1z00_1917 + 1L));
												}}}
												return (int) (BgL_tmpz00_8042);
		}}}}}}}}}}

	}



/* _string-prefix? */
	obj_t BGl__stringzd2prefixzf3z21zz__r4_strings_6_7z00(obj_t
		BgL_env1178z00_215, obj_t BgL_opt1177z00_214)
	{
		{	/* Ieee/string.scm 1306 */
			{	/* Ieee/string.scm 1306 */
				obj_t BgL_s1z00_1933;
				obj_t BgL_s2z00_1934;

				BgL_s1z00_1933 = VECTOR_REF(BgL_opt1177z00_214, 0L);
				BgL_s2z00_1934 = VECTOR_REF(BgL_opt1177z00_214, 1L);
				switch (VECTOR_LENGTH(BgL_opt1177z00_214))
					{
					case 2L:

						{	/* Ieee/string.scm 1306 */

							{	/* Ieee/string.scm 1306 */
								bool_t BgL_tmpz00_8072;

								{	/* Ieee/string.scm 1306 */
									obj_t BgL_auxz00_8080;
									obj_t BgL_auxz00_8073;

									if (STRINGP(BgL_s2z00_1934))
										{	/* Ieee/string.scm 1306 */
											BgL_auxz00_8080 = BgL_s2z00_1934;
										}
									else
										{
											obj_t BgL_auxz00_8083;

											BgL_auxz00_8083 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(53114L),
												BGl_string2759z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00, BgL_s2z00_1934);
											FAILURE(BgL_auxz00_8083, BFALSE, BFALSE);
										}
									if (STRINGP(BgL_s1z00_1933))
										{	/* Ieee/string.scm 1306 */
											BgL_auxz00_8073 = BgL_s1z00_1933;
										}
									else
										{
											obj_t BgL_auxz00_8076;

											BgL_auxz00_8076 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(53114L),
												BGl_string2759z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00, BgL_s1z00_1933);
											FAILURE(BgL_auxz00_8076, BFALSE, BFALSE);
										}
									BgL_tmpz00_8072 =
										BGl_stringzd2prefixzf3z21zz__r4_strings_6_7z00
										(BgL_auxz00_8073, BgL_auxz00_8080, BFALSE, BFALSE, BFALSE,
										BFALSE);
								}
								return BBOOL(BgL_tmpz00_8072);
							}
						}
						break;
					case 3L:

						{	/* Ieee/string.scm 1306 */
							obj_t BgL_start1z00_1941;

							BgL_start1z00_1941 = VECTOR_REF(BgL_opt1177z00_214, 2L);
							{	/* Ieee/string.scm 1306 */

								{	/* Ieee/string.scm 1306 */
									bool_t BgL_tmpz00_8090;

									{	/* Ieee/string.scm 1306 */
										obj_t BgL_auxz00_8098;
										obj_t BgL_auxz00_8091;

										if (STRINGP(BgL_s2z00_1934))
											{	/* Ieee/string.scm 1306 */
												BgL_auxz00_8098 = BgL_s2z00_1934;
											}
										else
											{
												obj_t BgL_auxz00_8101;

												BgL_auxz00_8101 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2658z00zz__r4_strings_6_7z00, BINT(53114L),
													BGl_string2759z00zz__r4_strings_6_7z00,
													BGl_string2660z00zz__r4_strings_6_7z00,
													BgL_s2z00_1934);
												FAILURE(BgL_auxz00_8101, BFALSE, BFALSE);
											}
										if (STRINGP(BgL_s1z00_1933))
											{	/* Ieee/string.scm 1306 */
												BgL_auxz00_8091 = BgL_s1z00_1933;
											}
										else
											{
												obj_t BgL_auxz00_8094;

												BgL_auxz00_8094 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2658z00zz__r4_strings_6_7z00, BINT(53114L),
													BGl_string2759z00zz__r4_strings_6_7z00,
													BGl_string2660z00zz__r4_strings_6_7z00,
													BgL_s1z00_1933);
												FAILURE(BgL_auxz00_8094, BFALSE, BFALSE);
											}
										BgL_tmpz00_8090 =
											BGl_stringzd2prefixzf3z21zz__r4_strings_6_7z00
											(BgL_auxz00_8091, BgL_auxz00_8098, BgL_start1z00_1941,
											BFALSE, BFALSE, BFALSE);
									}
									return BBOOL(BgL_tmpz00_8090);
								}
							}
						}
						break;
					case 4L:

						{	/* Ieee/string.scm 1306 */
							obj_t BgL_start1z00_1945;

							BgL_start1z00_1945 = VECTOR_REF(BgL_opt1177z00_214, 2L);
							{	/* Ieee/string.scm 1306 */
								obj_t BgL_end1z00_1946;

								BgL_end1z00_1946 = VECTOR_REF(BgL_opt1177z00_214, 3L);
								{	/* Ieee/string.scm 1306 */

									{	/* Ieee/string.scm 1306 */
										bool_t BgL_tmpz00_8109;

										{	/* Ieee/string.scm 1306 */
											obj_t BgL_auxz00_8117;
											obj_t BgL_auxz00_8110;

											if (STRINGP(BgL_s2z00_1934))
												{	/* Ieee/string.scm 1306 */
													BgL_auxz00_8117 = BgL_s2z00_1934;
												}
											else
												{
													obj_t BgL_auxz00_8120;

													BgL_auxz00_8120 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2658z00zz__r4_strings_6_7z00,
														BINT(53114L),
														BGl_string2759z00zz__r4_strings_6_7z00,
														BGl_string2660z00zz__r4_strings_6_7z00,
														BgL_s2z00_1934);
													FAILURE(BgL_auxz00_8120, BFALSE, BFALSE);
												}
											if (STRINGP(BgL_s1z00_1933))
												{	/* Ieee/string.scm 1306 */
													BgL_auxz00_8110 = BgL_s1z00_1933;
												}
											else
												{
													obj_t BgL_auxz00_8113;

													BgL_auxz00_8113 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2658z00zz__r4_strings_6_7z00,
														BINT(53114L),
														BGl_string2759z00zz__r4_strings_6_7z00,
														BGl_string2660z00zz__r4_strings_6_7z00,
														BgL_s1z00_1933);
													FAILURE(BgL_auxz00_8113, BFALSE, BFALSE);
												}
											BgL_tmpz00_8109 =
												BGl_stringzd2prefixzf3z21zz__r4_strings_6_7z00
												(BgL_auxz00_8110, BgL_auxz00_8117, BgL_start1z00_1945,
												BgL_end1z00_1946, BFALSE, BFALSE);
										}
										return BBOOL(BgL_tmpz00_8109);
									}
								}
							}
						}
						break;
					case 5L:

						{	/* Ieee/string.scm 1306 */
							obj_t BgL_start1z00_1949;

							BgL_start1z00_1949 = VECTOR_REF(BgL_opt1177z00_214, 2L);
							{	/* Ieee/string.scm 1306 */
								obj_t BgL_end1z00_1950;

								BgL_end1z00_1950 = VECTOR_REF(BgL_opt1177z00_214, 3L);
								{	/* Ieee/string.scm 1306 */
									obj_t BgL_start2z00_1951;

									BgL_start2z00_1951 = VECTOR_REF(BgL_opt1177z00_214, 4L);
									{	/* Ieee/string.scm 1306 */

										{	/* Ieee/string.scm 1306 */
											bool_t BgL_tmpz00_8129;

											{	/* Ieee/string.scm 1306 */
												obj_t BgL_auxz00_8137;
												obj_t BgL_auxz00_8130;

												if (STRINGP(BgL_s2z00_1934))
													{	/* Ieee/string.scm 1306 */
														BgL_auxz00_8137 = BgL_s2z00_1934;
													}
												else
													{
														obj_t BgL_auxz00_8140;

														BgL_auxz00_8140 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2658z00zz__r4_strings_6_7z00,
															BINT(53114L),
															BGl_string2759z00zz__r4_strings_6_7z00,
															BGl_string2660z00zz__r4_strings_6_7z00,
															BgL_s2z00_1934);
														FAILURE(BgL_auxz00_8140, BFALSE, BFALSE);
													}
												if (STRINGP(BgL_s1z00_1933))
													{	/* Ieee/string.scm 1306 */
														BgL_auxz00_8130 = BgL_s1z00_1933;
													}
												else
													{
														obj_t BgL_auxz00_8133;

														BgL_auxz00_8133 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2658z00zz__r4_strings_6_7z00,
															BINT(53114L),
															BGl_string2759z00zz__r4_strings_6_7z00,
															BGl_string2660z00zz__r4_strings_6_7z00,
															BgL_s1z00_1933);
														FAILURE(BgL_auxz00_8133, BFALSE, BFALSE);
													}
												BgL_tmpz00_8129 =
													BGl_stringzd2prefixzf3z21zz__r4_strings_6_7z00
													(BgL_auxz00_8130, BgL_auxz00_8137, BgL_start1z00_1949,
													BgL_end1z00_1950, BgL_start2z00_1951, BFALSE);
											}
											return BBOOL(BgL_tmpz00_8129);
										}
									}
								}
							}
						}
						break;
					case 6L:

						{	/* Ieee/string.scm 1306 */
							obj_t BgL_start1z00_1953;

							BgL_start1z00_1953 = VECTOR_REF(BgL_opt1177z00_214, 2L);
							{	/* Ieee/string.scm 1306 */
								obj_t BgL_end1z00_1954;

								BgL_end1z00_1954 = VECTOR_REF(BgL_opt1177z00_214, 3L);
								{	/* Ieee/string.scm 1306 */
									obj_t BgL_start2z00_1955;

									BgL_start2z00_1955 = VECTOR_REF(BgL_opt1177z00_214, 4L);
									{	/* Ieee/string.scm 1306 */
										obj_t BgL_end2z00_1956;

										BgL_end2z00_1956 = VECTOR_REF(BgL_opt1177z00_214, 5L);
										{	/* Ieee/string.scm 1306 */

											{	/* Ieee/string.scm 1306 */
												bool_t BgL_tmpz00_8150;

												{	/* Ieee/string.scm 1306 */
													obj_t BgL_auxz00_8158;
													obj_t BgL_auxz00_8151;

													if (STRINGP(BgL_s2z00_1934))
														{	/* Ieee/string.scm 1306 */
															BgL_auxz00_8158 = BgL_s2z00_1934;
														}
													else
														{
															obj_t BgL_auxz00_8161;

															BgL_auxz00_8161 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2658z00zz__r4_strings_6_7z00,
																BINT(53114L),
																BGl_string2759z00zz__r4_strings_6_7z00,
																BGl_string2660z00zz__r4_strings_6_7z00,
																BgL_s2z00_1934);
															FAILURE(BgL_auxz00_8161, BFALSE, BFALSE);
														}
													if (STRINGP(BgL_s1z00_1933))
														{	/* Ieee/string.scm 1306 */
															BgL_auxz00_8151 = BgL_s1z00_1933;
														}
													else
														{
															obj_t BgL_auxz00_8154;

															BgL_auxz00_8154 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2658z00zz__r4_strings_6_7z00,
																BINT(53114L),
																BGl_string2759z00zz__r4_strings_6_7z00,
																BGl_string2660z00zz__r4_strings_6_7z00,
																BgL_s1z00_1933);
															FAILURE(BgL_auxz00_8154, BFALSE, BFALSE);
														}
													BgL_tmpz00_8150 =
														BGl_stringzd2prefixzf3z21zz__r4_strings_6_7z00
														(BgL_auxz00_8151, BgL_auxz00_8158,
														BgL_start1z00_1953, BgL_end1z00_1954,
														BgL_start2z00_1955, BgL_end2z00_1956);
												}
												return BBOOL(BgL_tmpz00_8150);
											}
										}
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* string-prefix? */
	BGL_EXPORTED_DEF bool_t BGl_stringzd2prefixzf3z21zz__r4_strings_6_7z00(obj_t
		BgL_s1z00_208, obj_t BgL_s2z00_209, obj_t BgL_start1z00_210,
		obj_t BgL_end1z00_211, obj_t BgL_start2z00_212, obj_t BgL_end2z00_213)
	{
		{	/* Ieee/string.scm 1306 */
			{	/* Ieee/string.scm 1308 */
				long BgL_l1z00_1957;

				BgL_l1z00_1957 = STRING_LENGTH(BgL_s1z00_208);
				{	/* Ieee/string.scm 1308 */
					long BgL_l2z00_1958;

					BgL_l2z00_1958 = STRING_LENGTH(BgL_s2z00_209);
					{	/* Ieee/string.scm 1309 */
						obj_t BgL_e1z00_1959;

						{	/* Ieee/string.scm 1310 */
							obj_t BgL_procz00_3843;

							BgL_procz00_3843 = BGl_symbol2760z00zz__r4_strings_6_7z00;
							if (CBOOL(BgL_end1z00_211))
								{	/* Ieee/string.scm 1210 */
									if (((long) CINT(BgL_end1z00_211) <= 0L))
										{	/* Ieee/string.scm 1212 */
											BgL_e1z00_1959 =
												BGl_errorz00zz__errorz00(BgL_procz00_3843,
												string_append_3(BGl_string2741z00zz__r4_strings_6_7z00,
													BGl_string2742z00zz__r4_strings_6_7z00,
													BGl_string2743z00zz__r4_strings_6_7z00),
												BgL_end1z00_211);
										}
									else
										{	/* Ieee/string.scm 1212 */
											if (((long) CINT(BgL_end1z00_211) > BgL_l1z00_1957))
												{	/* Ieee/string.scm 1214 */
													BgL_e1z00_1959 =
														BGl_errorz00zz__errorz00(BgL_procz00_3843,
														string_append_3
														(BGl_string2744z00zz__r4_strings_6_7z00,
															BGl_string2742z00zz__r4_strings_6_7z00,
															BGl_string2743z00zz__r4_strings_6_7z00),
														BgL_end1z00_211);
												}
											else
												{	/* Ieee/string.scm 1214 */
													BgL_e1z00_1959 = BgL_end1z00_211;
												}
										}
								}
							else
								{	/* Ieee/string.scm 1210 */
									BgL_e1z00_1959 = BINT(BgL_l1z00_1957);
								}
						}
						{	/* Ieee/string.scm 1310 */
							obj_t BgL_e2z00_1960;

							{	/* Ieee/string.scm 1311 */
								obj_t BgL_procz00_3851;

								BgL_procz00_3851 = BGl_symbol2760z00zz__r4_strings_6_7z00;
								if (CBOOL(BgL_end2z00_213))
									{	/* Ieee/string.scm 1210 */
										if (((long) CINT(BgL_end2z00_213) <= 0L))
											{	/* Ieee/string.scm 1212 */
												BgL_e2z00_1960 =
													BGl_errorz00zz__errorz00(BgL_procz00_3851,
													string_append_3
													(BGl_string2741z00zz__r4_strings_6_7z00,
														BGl_string2745z00zz__r4_strings_6_7z00,
														BGl_string2743z00zz__r4_strings_6_7z00),
													BgL_end2z00_213);
											}
										else
											{	/* Ieee/string.scm 1212 */
												if (((long) CINT(BgL_end2z00_213) > BgL_l2z00_1958))
													{	/* Ieee/string.scm 1214 */
														BgL_e2z00_1960 =
															BGl_errorz00zz__errorz00(BgL_procz00_3851,
															string_append_3
															(BGl_string2744z00zz__r4_strings_6_7z00,
																BGl_string2745z00zz__r4_strings_6_7z00,
																BGl_string2743z00zz__r4_strings_6_7z00),
															BgL_end2z00_213);
													}
												else
													{	/* Ieee/string.scm 1214 */
														BgL_e2z00_1960 = BgL_end2z00_213;
													}
											}
									}
								else
									{	/* Ieee/string.scm 1210 */
										BgL_e2z00_1960 = BINT(BgL_l2z00_1958);
									}
							}
							{	/* Ieee/string.scm 1311 */
								obj_t BgL_b1z00_1961;

								{	/* Ieee/string.scm 1312 */
									obj_t BgL_procz00_3859;

									BgL_procz00_3859 = BGl_symbol2760z00zz__r4_strings_6_7z00;
									if (CBOOL(BgL_start1z00_210))
										{	/* Ieee/string.scm 1196 */
											if (((long) CINT(BgL_start1z00_210) < 0L))
												{	/* Ieee/string.scm 1198 */
													BgL_b1z00_1961 =
														BGl_errorz00zz__errorz00(BgL_procz00_3859,
														string_append_3
														(BGl_string2746z00zz__r4_strings_6_7z00,
															BGl_string2747z00zz__r4_strings_6_7z00,
															BGl_string2743z00zz__r4_strings_6_7z00),
														BgL_start1z00_210);
												}
											else
												{	/* Ieee/string.scm 1198 */
													if (
														((long) CINT(BgL_start1z00_210) >= BgL_l1z00_1957))
														{	/* Ieee/string.scm 1200 */
															BgL_b1z00_1961 =
																BGl_errorz00zz__errorz00(BgL_procz00_3859,
																string_append_3
																(BGl_string2748z00zz__r4_strings_6_7z00,
																	BGl_string2747z00zz__r4_strings_6_7z00,
																	BGl_string2743z00zz__r4_strings_6_7z00),
																BgL_start1z00_210);
														}
													else
														{	/* Ieee/string.scm 1200 */
															BgL_b1z00_1961 = BgL_start1z00_210;
														}
												}
										}
									else
										{	/* Ieee/string.scm 1196 */
											BgL_b1z00_1961 = BINT(0L);
										}
								}
								{	/* Ieee/string.scm 1312 */
									obj_t BgL_b2z00_1962;

									{	/* Ieee/string.scm 1313 */
										obj_t BgL_procz00_3867;

										BgL_procz00_3867 = BGl_symbol2760z00zz__r4_strings_6_7z00;
										if (CBOOL(BgL_start2z00_212))
											{	/* Ieee/string.scm 1196 */
												if (((long) CINT(BgL_start2z00_212) < 0L))
													{	/* Ieee/string.scm 1198 */
														BgL_b2z00_1962 =
															BGl_errorz00zz__errorz00(BgL_procz00_3867,
															string_append_3
															(BGl_string2746z00zz__r4_strings_6_7z00,
																BGl_string2749z00zz__r4_strings_6_7z00,
																BGl_string2743z00zz__r4_strings_6_7z00),
															BgL_start2z00_212);
													}
												else
													{	/* Ieee/string.scm 1198 */
														if (
															((long) CINT(BgL_start2z00_212) >=
																BgL_l2z00_1958))
															{	/* Ieee/string.scm 1200 */
																BgL_b2z00_1962 =
																	BGl_errorz00zz__errorz00(BgL_procz00_3867,
																	string_append_3
																	(BGl_string2748z00zz__r4_strings_6_7z00,
																		BGl_string2749z00zz__r4_strings_6_7z00,
																		BGl_string2743z00zz__r4_strings_6_7z00),
																	BgL_start2z00_212);
															}
														else
															{	/* Ieee/string.scm 1200 */
																BgL_b2z00_1962 = BgL_start2z00_212;
															}
													}
											}
										else
											{	/* Ieee/string.scm 1196 */
												BgL_b2z00_1962 = BINT(0L);
											}
									}
									{	/* Ieee/string.scm 1313 */

										{
											obj_t BgL_i1z00_1964;
											obj_t BgL_i2z00_1965;

											BgL_i1z00_1964 = BgL_b1z00_1961;
											BgL_i2z00_1965 = BgL_b2z00_1962;
										BgL_zc3z04anonymousza31796ze3z87_1966:
											if (
												((long) CINT(BgL_i1z00_1964) ==
													(long) CINT(BgL_e1z00_1959)))
												{	/* Ieee/string.scm 1317 */
													return ((bool_t) 1);
												}
											else
												{	/* Ieee/string.scm 1317 */
													if (
														((long) CINT(BgL_i2z00_1965) ==
															(long) CINT(BgL_e2z00_1960)))
														{	/* Ieee/string.scm 1319 */
															return ((bool_t) 0);
														}
													else
														{	/* Ieee/string.scm 1319 */
															if (
																(STRING_REF(BgL_s1z00_208,
																		(long) CINT(BgL_i1z00_1964)) ==
																	STRING_REF(BgL_s2z00_209,
																		(long) CINT(BgL_i2z00_1965))))
																{
																	obj_t BgL_i2z00_8240;
																	obj_t BgL_i1z00_8237;

																	BgL_i1z00_8237 =
																		ADDFX(BgL_i1z00_1964, BINT(1L));
																	BgL_i2z00_8240 =
																		ADDFX(BgL_i2z00_1965, BINT(1L));
																	BgL_i2z00_1965 = BgL_i2z00_8240;
																	BgL_i1z00_1964 = BgL_i1z00_8237;
																	goto BgL_zc3z04anonymousza31796ze3z87_1966;
																}
															else
																{	/* Ieee/string.scm 1321 */
																	return ((bool_t) 0);
																}
														}
												}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* _string-prefix-ci? */
	obj_t BGl__stringzd2prefixzd2cizf3zf3zz__r4_strings_6_7z00(obj_t
		BgL_env1182z00_223, obj_t BgL_opt1181z00_222)
	{
		{	/* Ieee/string.scm 1329 */
			{	/* Ieee/string.scm 1329 */
				obj_t BgL_s1z00_1977;
				obj_t BgL_s2z00_1978;

				BgL_s1z00_1977 = VECTOR_REF(BgL_opt1181z00_222, 0L);
				BgL_s2z00_1978 = VECTOR_REF(BgL_opt1181z00_222, 1L);
				switch (VECTOR_LENGTH(BgL_opt1181z00_222))
					{
					case 2L:

						{	/* Ieee/string.scm 1329 */

							{	/* Ieee/string.scm 1329 */
								bool_t BgL_tmpz00_8245;

								{	/* Ieee/string.scm 1329 */
									obj_t BgL_auxz00_8253;
									obj_t BgL_auxz00_8246;

									if (STRINGP(BgL_s2z00_1978))
										{	/* Ieee/string.scm 1329 */
											BgL_auxz00_8253 = BgL_s2z00_1978;
										}
									else
										{
											obj_t BgL_auxz00_8256;

											BgL_auxz00_8256 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(53959L),
												BGl_string2762z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00, BgL_s2z00_1978);
											FAILURE(BgL_auxz00_8256, BFALSE, BFALSE);
										}
									if (STRINGP(BgL_s1z00_1977))
										{	/* Ieee/string.scm 1329 */
											BgL_auxz00_8246 = BgL_s1z00_1977;
										}
									else
										{
											obj_t BgL_auxz00_8249;

											BgL_auxz00_8249 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(53959L),
												BGl_string2762z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00, BgL_s1z00_1977);
											FAILURE(BgL_auxz00_8249, BFALSE, BFALSE);
										}
									BgL_tmpz00_8245 =
										BGl_stringzd2prefixzd2cizf3zf3zz__r4_strings_6_7z00
										(BgL_auxz00_8246, BgL_auxz00_8253, BFALSE, BFALSE, BFALSE,
										BFALSE);
								}
								return BBOOL(BgL_tmpz00_8245);
							}
						}
						break;
					case 3L:

						{	/* Ieee/string.scm 1329 */
							obj_t BgL_start1z00_1985;

							BgL_start1z00_1985 = VECTOR_REF(BgL_opt1181z00_222, 2L);
							{	/* Ieee/string.scm 1329 */

								{	/* Ieee/string.scm 1329 */
									bool_t BgL_tmpz00_8263;

									{	/* Ieee/string.scm 1329 */
										obj_t BgL_auxz00_8271;
										obj_t BgL_auxz00_8264;

										if (STRINGP(BgL_s2z00_1978))
											{	/* Ieee/string.scm 1329 */
												BgL_auxz00_8271 = BgL_s2z00_1978;
											}
										else
											{
												obj_t BgL_auxz00_8274;

												BgL_auxz00_8274 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2658z00zz__r4_strings_6_7z00, BINT(53959L),
													BGl_string2762z00zz__r4_strings_6_7z00,
													BGl_string2660z00zz__r4_strings_6_7z00,
													BgL_s2z00_1978);
												FAILURE(BgL_auxz00_8274, BFALSE, BFALSE);
											}
										if (STRINGP(BgL_s1z00_1977))
											{	/* Ieee/string.scm 1329 */
												BgL_auxz00_8264 = BgL_s1z00_1977;
											}
										else
											{
												obj_t BgL_auxz00_8267;

												BgL_auxz00_8267 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2658z00zz__r4_strings_6_7z00, BINT(53959L),
													BGl_string2762z00zz__r4_strings_6_7z00,
													BGl_string2660z00zz__r4_strings_6_7z00,
													BgL_s1z00_1977);
												FAILURE(BgL_auxz00_8267, BFALSE, BFALSE);
											}
										BgL_tmpz00_8263 =
											BGl_stringzd2prefixzd2cizf3zf3zz__r4_strings_6_7z00
											(BgL_auxz00_8264, BgL_auxz00_8271, BgL_start1z00_1985,
											BFALSE, BFALSE, BFALSE);
									}
									return BBOOL(BgL_tmpz00_8263);
								}
							}
						}
						break;
					case 4L:

						{	/* Ieee/string.scm 1329 */
							obj_t BgL_start1z00_1989;

							BgL_start1z00_1989 = VECTOR_REF(BgL_opt1181z00_222, 2L);
							{	/* Ieee/string.scm 1329 */
								obj_t BgL_end1z00_1990;

								BgL_end1z00_1990 = VECTOR_REF(BgL_opt1181z00_222, 3L);
								{	/* Ieee/string.scm 1329 */

									{	/* Ieee/string.scm 1329 */
										bool_t BgL_tmpz00_8282;

										{	/* Ieee/string.scm 1329 */
											obj_t BgL_auxz00_8290;
											obj_t BgL_auxz00_8283;

											if (STRINGP(BgL_s2z00_1978))
												{	/* Ieee/string.scm 1329 */
													BgL_auxz00_8290 = BgL_s2z00_1978;
												}
											else
												{
													obj_t BgL_auxz00_8293;

													BgL_auxz00_8293 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2658z00zz__r4_strings_6_7z00,
														BINT(53959L),
														BGl_string2762z00zz__r4_strings_6_7z00,
														BGl_string2660z00zz__r4_strings_6_7z00,
														BgL_s2z00_1978);
													FAILURE(BgL_auxz00_8293, BFALSE, BFALSE);
												}
											if (STRINGP(BgL_s1z00_1977))
												{	/* Ieee/string.scm 1329 */
													BgL_auxz00_8283 = BgL_s1z00_1977;
												}
											else
												{
													obj_t BgL_auxz00_8286;

													BgL_auxz00_8286 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2658z00zz__r4_strings_6_7z00,
														BINT(53959L),
														BGl_string2762z00zz__r4_strings_6_7z00,
														BGl_string2660z00zz__r4_strings_6_7z00,
														BgL_s1z00_1977);
													FAILURE(BgL_auxz00_8286, BFALSE, BFALSE);
												}
											BgL_tmpz00_8282 =
												BGl_stringzd2prefixzd2cizf3zf3zz__r4_strings_6_7z00
												(BgL_auxz00_8283, BgL_auxz00_8290, BgL_start1z00_1989,
												BgL_end1z00_1990, BFALSE, BFALSE);
										}
										return BBOOL(BgL_tmpz00_8282);
									}
								}
							}
						}
						break;
					case 5L:

						{	/* Ieee/string.scm 1329 */
							obj_t BgL_start1z00_1993;

							BgL_start1z00_1993 = VECTOR_REF(BgL_opt1181z00_222, 2L);
							{	/* Ieee/string.scm 1329 */
								obj_t BgL_end1z00_1994;

								BgL_end1z00_1994 = VECTOR_REF(BgL_opt1181z00_222, 3L);
								{	/* Ieee/string.scm 1329 */
									obj_t BgL_start2z00_1995;

									BgL_start2z00_1995 = VECTOR_REF(BgL_opt1181z00_222, 4L);
									{	/* Ieee/string.scm 1329 */

										{	/* Ieee/string.scm 1329 */
											bool_t BgL_tmpz00_8302;

											{	/* Ieee/string.scm 1329 */
												obj_t BgL_auxz00_8310;
												obj_t BgL_auxz00_8303;

												if (STRINGP(BgL_s2z00_1978))
													{	/* Ieee/string.scm 1329 */
														BgL_auxz00_8310 = BgL_s2z00_1978;
													}
												else
													{
														obj_t BgL_auxz00_8313;

														BgL_auxz00_8313 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2658z00zz__r4_strings_6_7z00,
															BINT(53959L),
															BGl_string2762z00zz__r4_strings_6_7z00,
															BGl_string2660z00zz__r4_strings_6_7z00,
															BgL_s2z00_1978);
														FAILURE(BgL_auxz00_8313, BFALSE, BFALSE);
													}
												if (STRINGP(BgL_s1z00_1977))
													{	/* Ieee/string.scm 1329 */
														BgL_auxz00_8303 = BgL_s1z00_1977;
													}
												else
													{
														obj_t BgL_auxz00_8306;

														BgL_auxz00_8306 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2658z00zz__r4_strings_6_7z00,
															BINT(53959L),
															BGl_string2762z00zz__r4_strings_6_7z00,
															BGl_string2660z00zz__r4_strings_6_7z00,
															BgL_s1z00_1977);
														FAILURE(BgL_auxz00_8306, BFALSE, BFALSE);
													}
												BgL_tmpz00_8302 =
													BGl_stringzd2prefixzd2cizf3zf3zz__r4_strings_6_7z00
													(BgL_auxz00_8303, BgL_auxz00_8310, BgL_start1z00_1993,
													BgL_end1z00_1994, BgL_start2z00_1995, BFALSE);
											}
											return BBOOL(BgL_tmpz00_8302);
										}
									}
								}
							}
						}
						break;
					case 6L:

						{	/* Ieee/string.scm 1329 */
							obj_t BgL_start1z00_1997;

							BgL_start1z00_1997 = VECTOR_REF(BgL_opt1181z00_222, 2L);
							{	/* Ieee/string.scm 1329 */
								obj_t BgL_end1z00_1998;

								BgL_end1z00_1998 = VECTOR_REF(BgL_opt1181z00_222, 3L);
								{	/* Ieee/string.scm 1329 */
									obj_t BgL_start2z00_1999;

									BgL_start2z00_1999 = VECTOR_REF(BgL_opt1181z00_222, 4L);
									{	/* Ieee/string.scm 1329 */
										obj_t BgL_end2z00_2000;

										BgL_end2z00_2000 = VECTOR_REF(BgL_opt1181z00_222, 5L);
										{	/* Ieee/string.scm 1329 */

											{	/* Ieee/string.scm 1329 */
												bool_t BgL_tmpz00_8323;

												{	/* Ieee/string.scm 1329 */
													obj_t BgL_auxz00_8331;
													obj_t BgL_auxz00_8324;

													if (STRINGP(BgL_s2z00_1978))
														{	/* Ieee/string.scm 1329 */
															BgL_auxz00_8331 = BgL_s2z00_1978;
														}
													else
														{
															obj_t BgL_auxz00_8334;

															BgL_auxz00_8334 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2658z00zz__r4_strings_6_7z00,
																BINT(53959L),
																BGl_string2762z00zz__r4_strings_6_7z00,
																BGl_string2660z00zz__r4_strings_6_7z00,
																BgL_s2z00_1978);
															FAILURE(BgL_auxz00_8334, BFALSE, BFALSE);
														}
													if (STRINGP(BgL_s1z00_1977))
														{	/* Ieee/string.scm 1329 */
															BgL_auxz00_8324 = BgL_s1z00_1977;
														}
													else
														{
															obj_t BgL_auxz00_8327;

															BgL_auxz00_8327 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2658z00zz__r4_strings_6_7z00,
																BINT(53959L),
																BGl_string2762z00zz__r4_strings_6_7z00,
																BGl_string2660z00zz__r4_strings_6_7z00,
																BgL_s1z00_1977);
															FAILURE(BgL_auxz00_8327, BFALSE, BFALSE);
														}
													BgL_tmpz00_8323 =
														BGl_stringzd2prefixzd2cizf3zf3zz__r4_strings_6_7z00
														(BgL_auxz00_8324, BgL_auxz00_8331,
														BgL_start1z00_1997, BgL_end1z00_1998,
														BgL_start2z00_1999, BgL_end2z00_2000);
												}
												return BBOOL(BgL_tmpz00_8323);
											}
										}
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* string-prefix-ci? */
	BGL_EXPORTED_DEF bool_t
		BGl_stringzd2prefixzd2cizf3zf3zz__r4_strings_6_7z00(obj_t BgL_s1z00_216,
		obj_t BgL_s2z00_217, obj_t BgL_start1z00_218, obj_t BgL_end1z00_219,
		obj_t BgL_start2z00_220, obj_t BgL_end2z00_221)
	{
		{	/* Ieee/string.scm 1329 */
			{	/* Ieee/string.scm 1331 */
				long BgL_l1z00_2001;

				BgL_l1z00_2001 = STRING_LENGTH(BgL_s1z00_216);
				{	/* Ieee/string.scm 1331 */
					long BgL_l2z00_2002;

					BgL_l2z00_2002 = STRING_LENGTH(BgL_s2z00_217);
					{	/* Ieee/string.scm 1332 */
						obj_t BgL_e1z00_2003;

						{	/* Ieee/string.scm 1333 */
							obj_t BgL_procz00_3889;

							BgL_procz00_3889 = BGl_symbol2763z00zz__r4_strings_6_7z00;
							if (CBOOL(BgL_end1z00_219))
								{	/* Ieee/string.scm 1210 */
									if (((long) CINT(BgL_end1z00_219) <= 0L))
										{	/* Ieee/string.scm 1212 */
											BgL_e1z00_2003 =
												BGl_errorz00zz__errorz00(BgL_procz00_3889,
												string_append_3(BGl_string2741z00zz__r4_strings_6_7z00,
													BGl_string2742z00zz__r4_strings_6_7z00,
													BGl_string2743z00zz__r4_strings_6_7z00),
												BgL_end1z00_219);
										}
									else
										{	/* Ieee/string.scm 1212 */
											if (((long) CINT(BgL_end1z00_219) > BgL_l1z00_2001))
												{	/* Ieee/string.scm 1214 */
													BgL_e1z00_2003 =
														BGl_errorz00zz__errorz00(BgL_procz00_3889,
														string_append_3
														(BGl_string2744z00zz__r4_strings_6_7z00,
															BGl_string2742z00zz__r4_strings_6_7z00,
															BGl_string2743z00zz__r4_strings_6_7z00),
														BgL_end1z00_219);
												}
											else
												{	/* Ieee/string.scm 1214 */
													BgL_e1z00_2003 = BgL_end1z00_219;
												}
										}
								}
							else
								{	/* Ieee/string.scm 1210 */
									BgL_e1z00_2003 = BINT(BgL_l1z00_2001);
								}
						}
						{	/* Ieee/string.scm 1333 */
							obj_t BgL_e2z00_2004;

							{	/* Ieee/string.scm 1334 */
								obj_t BgL_procz00_3897;

								BgL_procz00_3897 = BGl_symbol2763z00zz__r4_strings_6_7z00;
								if (CBOOL(BgL_end2z00_221))
									{	/* Ieee/string.scm 1210 */
										if (((long) CINT(BgL_end2z00_221) <= 0L))
											{	/* Ieee/string.scm 1212 */
												BgL_e2z00_2004 =
													BGl_errorz00zz__errorz00(BgL_procz00_3897,
													string_append_3
													(BGl_string2741z00zz__r4_strings_6_7z00,
														BGl_string2745z00zz__r4_strings_6_7z00,
														BGl_string2743z00zz__r4_strings_6_7z00),
													BgL_end2z00_221);
											}
										else
											{	/* Ieee/string.scm 1212 */
												if (((long) CINT(BgL_end2z00_221) > BgL_l2z00_2002))
													{	/* Ieee/string.scm 1214 */
														BgL_e2z00_2004 =
															BGl_errorz00zz__errorz00(BgL_procz00_3897,
															string_append_3
															(BGl_string2744z00zz__r4_strings_6_7z00,
																BGl_string2745z00zz__r4_strings_6_7z00,
																BGl_string2743z00zz__r4_strings_6_7z00),
															BgL_end2z00_221);
													}
												else
													{	/* Ieee/string.scm 1214 */
														BgL_e2z00_2004 = BgL_end2z00_221;
													}
											}
									}
								else
									{	/* Ieee/string.scm 1210 */
										BgL_e2z00_2004 = BINT(BgL_l2z00_2002);
									}
							}
							{	/* Ieee/string.scm 1334 */
								obj_t BgL_b1z00_2005;

								{	/* Ieee/string.scm 1335 */
									obj_t BgL_procz00_3905;

									BgL_procz00_3905 = BGl_symbol2763z00zz__r4_strings_6_7z00;
									if (CBOOL(BgL_start1z00_218))
										{	/* Ieee/string.scm 1196 */
											if (((long) CINT(BgL_start1z00_218) < 0L))
												{	/* Ieee/string.scm 1198 */
													BgL_b1z00_2005 =
														BGl_errorz00zz__errorz00(BgL_procz00_3905,
														string_append_3
														(BGl_string2746z00zz__r4_strings_6_7z00,
															BGl_string2747z00zz__r4_strings_6_7z00,
															BGl_string2743z00zz__r4_strings_6_7z00),
														BgL_start1z00_218);
												}
											else
												{	/* Ieee/string.scm 1198 */
													if (
														((long) CINT(BgL_start1z00_218) >= BgL_l1z00_2001))
														{	/* Ieee/string.scm 1200 */
															BgL_b1z00_2005 =
																BGl_errorz00zz__errorz00(BgL_procz00_3905,
																string_append_3
																(BGl_string2748z00zz__r4_strings_6_7z00,
																	BGl_string2747z00zz__r4_strings_6_7z00,
																	BGl_string2743z00zz__r4_strings_6_7z00),
																BgL_start1z00_218);
														}
													else
														{	/* Ieee/string.scm 1200 */
															BgL_b1z00_2005 = BgL_start1z00_218;
														}
												}
										}
									else
										{	/* Ieee/string.scm 1196 */
											BgL_b1z00_2005 = BINT(0L);
										}
								}
								{	/* Ieee/string.scm 1335 */
									obj_t BgL_b2z00_2006;

									{	/* Ieee/string.scm 1336 */
										obj_t BgL_procz00_3913;

										BgL_procz00_3913 = BGl_symbol2763z00zz__r4_strings_6_7z00;
										if (CBOOL(BgL_start2z00_220))
											{	/* Ieee/string.scm 1196 */
												if (((long) CINT(BgL_start2z00_220) < 0L))
													{	/* Ieee/string.scm 1198 */
														BgL_b2z00_2006 =
															BGl_errorz00zz__errorz00(BgL_procz00_3913,
															string_append_3
															(BGl_string2746z00zz__r4_strings_6_7z00,
																BGl_string2749z00zz__r4_strings_6_7z00,
																BGl_string2743z00zz__r4_strings_6_7z00),
															BgL_start2z00_220);
													}
												else
													{	/* Ieee/string.scm 1198 */
														if (
															((long) CINT(BgL_start2z00_220) >=
																BgL_l2z00_2002))
															{	/* Ieee/string.scm 1200 */
																BgL_b2z00_2006 =
																	BGl_errorz00zz__errorz00(BgL_procz00_3913,
																	string_append_3
																	(BGl_string2748z00zz__r4_strings_6_7z00,
																		BGl_string2749z00zz__r4_strings_6_7z00,
																		BGl_string2743z00zz__r4_strings_6_7z00),
																	BgL_start2z00_220);
															}
														else
															{	/* Ieee/string.scm 1200 */
																BgL_b2z00_2006 = BgL_start2z00_220;
															}
													}
											}
										else
											{	/* Ieee/string.scm 1196 */
												BgL_b2z00_2006 = BINT(0L);
											}
									}
									{	/* Ieee/string.scm 1336 */

										{
											obj_t BgL_i1z00_2008;
											obj_t BgL_i2z00_2009;

											BgL_i1z00_2008 = BgL_b1z00_2005;
											BgL_i2z00_2009 = BgL_b2z00_2006;
										BgL_zc3z04anonymousza31806ze3z87_2010:
											if (
												((long) CINT(BgL_i1z00_2008) ==
													(long) CINT(BgL_e1z00_2003)))
												{	/* Ieee/string.scm 1340 */
													return ((bool_t) 1);
												}
											else
												{	/* Ieee/string.scm 1340 */
													if (
														((long) CINT(BgL_i2z00_2009) ==
															(long) CINT(BgL_e2z00_2004)))
														{	/* Ieee/string.scm 1342 */
															return ((bool_t) 0);
														}
													else
														{	/* Ieee/string.scm 1342 */
															if (
																(toupper(STRING_REF(BgL_s1z00_216,
																			(long) CINT(BgL_i1z00_2008))) ==
																	toupper(STRING_REF(BgL_s2z00_217,
																			(long) CINT(BgL_i2z00_2009)))))
																{
																	obj_t BgL_i2z00_8415;
																	obj_t BgL_i1z00_8412;

																	BgL_i1z00_8412 =
																		ADDFX(BgL_i1z00_2008, BINT(1L));
																	BgL_i2z00_8415 =
																		ADDFX(BgL_i2z00_2009, BINT(1L));
																	BgL_i2z00_2009 = BgL_i2z00_8415;
																	BgL_i1z00_2008 = BgL_i1z00_8412;
																	goto BgL_zc3z04anonymousza31806ze3z87_2010;
																}
															else
																{	/* Ieee/string.scm 1344 */
																	return ((bool_t) 0);
																}
														}
												}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* _string-suffix? */
	obj_t BGl__stringzd2suffixzf3z21zz__r4_strings_6_7z00(obj_t
		BgL_env1186z00_231, obj_t BgL_opt1185z00_230)
	{
		{	/* Ieee/string.scm 1352 */
			{	/* Ieee/string.scm 1352 */
				obj_t BgL_s1z00_2021;
				obj_t BgL_s2z00_2022;

				BgL_s1z00_2021 = VECTOR_REF(BgL_opt1185z00_230, 0L);
				BgL_s2z00_2022 = VECTOR_REF(BgL_opt1185z00_230, 1L);
				switch (VECTOR_LENGTH(BgL_opt1185z00_230))
					{
					case 2L:

						{	/* Ieee/string.scm 1352 */

							{	/* Ieee/string.scm 1352 */
								bool_t BgL_tmpz00_8420;

								{	/* Ieee/string.scm 1352 */
									obj_t BgL_auxz00_8428;
									obj_t BgL_auxz00_8421;

									if (STRINGP(BgL_s2z00_2022))
										{	/* Ieee/string.scm 1352 */
											BgL_auxz00_8428 = BgL_s2z00_2022;
										}
									else
										{
											obj_t BgL_auxz00_8431;

											BgL_auxz00_8431 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(54818L),
												BGl_string2765z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00, BgL_s2z00_2022);
											FAILURE(BgL_auxz00_8431, BFALSE, BFALSE);
										}
									if (STRINGP(BgL_s1z00_2021))
										{	/* Ieee/string.scm 1352 */
											BgL_auxz00_8421 = BgL_s1z00_2021;
										}
									else
										{
											obj_t BgL_auxz00_8424;

											BgL_auxz00_8424 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(54818L),
												BGl_string2765z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00, BgL_s1z00_2021);
											FAILURE(BgL_auxz00_8424, BFALSE, BFALSE);
										}
									BgL_tmpz00_8420 =
										BGl_stringzd2suffixzf3z21zz__r4_strings_6_7z00
										(BgL_auxz00_8421, BgL_auxz00_8428, BFALSE, BFALSE, BFALSE,
										BFALSE);
								}
								return BBOOL(BgL_tmpz00_8420);
							}
						}
						break;
					case 3L:

						{	/* Ieee/string.scm 1352 */
							obj_t BgL_start1z00_2029;

							BgL_start1z00_2029 = VECTOR_REF(BgL_opt1185z00_230, 2L);
							{	/* Ieee/string.scm 1352 */

								{	/* Ieee/string.scm 1352 */
									bool_t BgL_tmpz00_8438;

									{	/* Ieee/string.scm 1352 */
										obj_t BgL_auxz00_8446;
										obj_t BgL_auxz00_8439;

										if (STRINGP(BgL_s2z00_2022))
											{	/* Ieee/string.scm 1352 */
												BgL_auxz00_8446 = BgL_s2z00_2022;
											}
										else
											{
												obj_t BgL_auxz00_8449;

												BgL_auxz00_8449 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2658z00zz__r4_strings_6_7z00, BINT(54818L),
													BGl_string2765z00zz__r4_strings_6_7z00,
													BGl_string2660z00zz__r4_strings_6_7z00,
													BgL_s2z00_2022);
												FAILURE(BgL_auxz00_8449, BFALSE, BFALSE);
											}
										if (STRINGP(BgL_s1z00_2021))
											{	/* Ieee/string.scm 1352 */
												BgL_auxz00_8439 = BgL_s1z00_2021;
											}
										else
											{
												obj_t BgL_auxz00_8442;

												BgL_auxz00_8442 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2658z00zz__r4_strings_6_7z00, BINT(54818L),
													BGl_string2765z00zz__r4_strings_6_7z00,
													BGl_string2660z00zz__r4_strings_6_7z00,
													BgL_s1z00_2021);
												FAILURE(BgL_auxz00_8442, BFALSE, BFALSE);
											}
										BgL_tmpz00_8438 =
											BGl_stringzd2suffixzf3z21zz__r4_strings_6_7z00
											(BgL_auxz00_8439, BgL_auxz00_8446, BgL_start1z00_2029,
											BFALSE, BFALSE, BFALSE);
									}
									return BBOOL(BgL_tmpz00_8438);
								}
							}
						}
						break;
					case 4L:

						{	/* Ieee/string.scm 1352 */
							obj_t BgL_start1z00_2033;

							BgL_start1z00_2033 = VECTOR_REF(BgL_opt1185z00_230, 2L);
							{	/* Ieee/string.scm 1352 */
								obj_t BgL_end1z00_2034;

								BgL_end1z00_2034 = VECTOR_REF(BgL_opt1185z00_230, 3L);
								{	/* Ieee/string.scm 1352 */

									{	/* Ieee/string.scm 1352 */
										bool_t BgL_tmpz00_8457;

										{	/* Ieee/string.scm 1352 */
											obj_t BgL_auxz00_8465;
											obj_t BgL_auxz00_8458;

											if (STRINGP(BgL_s2z00_2022))
												{	/* Ieee/string.scm 1352 */
													BgL_auxz00_8465 = BgL_s2z00_2022;
												}
											else
												{
													obj_t BgL_auxz00_8468;

													BgL_auxz00_8468 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2658z00zz__r4_strings_6_7z00,
														BINT(54818L),
														BGl_string2765z00zz__r4_strings_6_7z00,
														BGl_string2660z00zz__r4_strings_6_7z00,
														BgL_s2z00_2022);
													FAILURE(BgL_auxz00_8468, BFALSE, BFALSE);
												}
											if (STRINGP(BgL_s1z00_2021))
												{	/* Ieee/string.scm 1352 */
													BgL_auxz00_8458 = BgL_s1z00_2021;
												}
											else
												{
													obj_t BgL_auxz00_8461;

													BgL_auxz00_8461 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2658z00zz__r4_strings_6_7z00,
														BINT(54818L),
														BGl_string2765z00zz__r4_strings_6_7z00,
														BGl_string2660z00zz__r4_strings_6_7z00,
														BgL_s1z00_2021);
													FAILURE(BgL_auxz00_8461, BFALSE, BFALSE);
												}
											BgL_tmpz00_8457 =
												BGl_stringzd2suffixzf3z21zz__r4_strings_6_7z00
												(BgL_auxz00_8458, BgL_auxz00_8465, BgL_start1z00_2033,
												BgL_end1z00_2034, BFALSE, BFALSE);
										}
										return BBOOL(BgL_tmpz00_8457);
									}
								}
							}
						}
						break;
					case 5L:

						{	/* Ieee/string.scm 1352 */
							obj_t BgL_start1z00_2037;

							BgL_start1z00_2037 = VECTOR_REF(BgL_opt1185z00_230, 2L);
							{	/* Ieee/string.scm 1352 */
								obj_t BgL_end1z00_2038;

								BgL_end1z00_2038 = VECTOR_REF(BgL_opt1185z00_230, 3L);
								{	/* Ieee/string.scm 1352 */
									obj_t BgL_start2z00_2039;

									BgL_start2z00_2039 = VECTOR_REF(BgL_opt1185z00_230, 4L);
									{	/* Ieee/string.scm 1352 */

										{	/* Ieee/string.scm 1352 */
											bool_t BgL_tmpz00_8477;

											{	/* Ieee/string.scm 1352 */
												obj_t BgL_auxz00_8485;
												obj_t BgL_auxz00_8478;

												if (STRINGP(BgL_s2z00_2022))
													{	/* Ieee/string.scm 1352 */
														BgL_auxz00_8485 = BgL_s2z00_2022;
													}
												else
													{
														obj_t BgL_auxz00_8488;

														BgL_auxz00_8488 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2658z00zz__r4_strings_6_7z00,
															BINT(54818L),
															BGl_string2765z00zz__r4_strings_6_7z00,
															BGl_string2660z00zz__r4_strings_6_7z00,
															BgL_s2z00_2022);
														FAILURE(BgL_auxz00_8488, BFALSE, BFALSE);
													}
												if (STRINGP(BgL_s1z00_2021))
													{	/* Ieee/string.scm 1352 */
														BgL_auxz00_8478 = BgL_s1z00_2021;
													}
												else
													{
														obj_t BgL_auxz00_8481;

														BgL_auxz00_8481 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2658z00zz__r4_strings_6_7z00,
															BINT(54818L),
															BGl_string2765z00zz__r4_strings_6_7z00,
															BGl_string2660z00zz__r4_strings_6_7z00,
															BgL_s1z00_2021);
														FAILURE(BgL_auxz00_8481, BFALSE, BFALSE);
													}
												BgL_tmpz00_8477 =
													BGl_stringzd2suffixzf3z21zz__r4_strings_6_7z00
													(BgL_auxz00_8478, BgL_auxz00_8485, BgL_start1z00_2037,
													BgL_end1z00_2038, BgL_start2z00_2039, BFALSE);
											}
											return BBOOL(BgL_tmpz00_8477);
										}
									}
								}
							}
						}
						break;
					case 6L:

						{	/* Ieee/string.scm 1352 */
							obj_t BgL_start1z00_2041;

							BgL_start1z00_2041 = VECTOR_REF(BgL_opt1185z00_230, 2L);
							{	/* Ieee/string.scm 1352 */
								obj_t BgL_end1z00_2042;

								BgL_end1z00_2042 = VECTOR_REF(BgL_opt1185z00_230, 3L);
								{	/* Ieee/string.scm 1352 */
									obj_t BgL_start2z00_2043;

									BgL_start2z00_2043 = VECTOR_REF(BgL_opt1185z00_230, 4L);
									{	/* Ieee/string.scm 1352 */
										obj_t BgL_end2z00_2044;

										BgL_end2z00_2044 = VECTOR_REF(BgL_opt1185z00_230, 5L);
										{	/* Ieee/string.scm 1352 */

											{	/* Ieee/string.scm 1352 */
												bool_t BgL_tmpz00_8498;

												{	/* Ieee/string.scm 1352 */
													obj_t BgL_auxz00_8506;
													obj_t BgL_auxz00_8499;

													if (STRINGP(BgL_s2z00_2022))
														{	/* Ieee/string.scm 1352 */
															BgL_auxz00_8506 = BgL_s2z00_2022;
														}
													else
														{
															obj_t BgL_auxz00_8509;

															BgL_auxz00_8509 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2658z00zz__r4_strings_6_7z00,
																BINT(54818L),
																BGl_string2765z00zz__r4_strings_6_7z00,
																BGl_string2660z00zz__r4_strings_6_7z00,
																BgL_s2z00_2022);
															FAILURE(BgL_auxz00_8509, BFALSE, BFALSE);
														}
													if (STRINGP(BgL_s1z00_2021))
														{	/* Ieee/string.scm 1352 */
															BgL_auxz00_8499 = BgL_s1z00_2021;
														}
													else
														{
															obj_t BgL_auxz00_8502;

															BgL_auxz00_8502 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2658z00zz__r4_strings_6_7z00,
																BINT(54818L),
																BGl_string2765z00zz__r4_strings_6_7z00,
																BGl_string2660z00zz__r4_strings_6_7z00,
																BgL_s1z00_2021);
															FAILURE(BgL_auxz00_8502, BFALSE, BFALSE);
														}
													BgL_tmpz00_8498 =
														BGl_stringzd2suffixzf3z21zz__r4_strings_6_7z00
														(BgL_auxz00_8499, BgL_auxz00_8506,
														BgL_start1z00_2041, BgL_end1z00_2042,
														BgL_start2z00_2043, BgL_end2z00_2044);
												}
												return BBOOL(BgL_tmpz00_8498);
											}
										}
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* string-suffix? */
	BGL_EXPORTED_DEF bool_t BGl_stringzd2suffixzf3z21zz__r4_strings_6_7z00(obj_t
		BgL_s1z00_224, obj_t BgL_s2z00_225, obj_t BgL_start1z00_226,
		obj_t BgL_end1z00_227, obj_t BgL_start2z00_228, obj_t BgL_end2z00_229)
	{
		{	/* Ieee/string.scm 1352 */
			{	/* Ieee/string.scm 1354 */
				long BgL_l1z00_2045;

				BgL_l1z00_2045 = STRING_LENGTH(BgL_s1z00_224);
				{	/* Ieee/string.scm 1354 */
					long BgL_l2z00_2046;

					BgL_l2z00_2046 = STRING_LENGTH(BgL_s2z00_225);
					{	/* Ieee/string.scm 1355 */
						obj_t BgL_b1z00_2047;

						{	/* Ieee/string.scm 1356 */
							obj_t BgL_procz00_3941;

							BgL_procz00_3941 = BGl_symbol2766z00zz__r4_strings_6_7z00;
							if (CBOOL(BgL_end1z00_227))
								{	/* Ieee/string.scm 1210 */
									if (((long) CINT(BgL_end1z00_227) <= 0L))
										{	/* Ieee/string.scm 1212 */
											BgL_b1z00_2047 =
												BGl_errorz00zz__errorz00(BgL_procz00_3941,
												string_append_3(BGl_string2741z00zz__r4_strings_6_7z00,
													BGl_string2742z00zz__r4_strings_6_7z00,
													BGl_string2743z00zz__r4_strings_6_7z00),
												BgL_end1z00_227);
										}
									else
										{	/* Ieee/string.scm 1212 */
											if (((long) CINT(BgL_end1z00_227) > BgL_l1z00_2045))
												{	/* Ieee/string.scm 1214 */
													BgL_b1z00_2047 =
														BGl_errorz00zz__errorz00(BgL_procz00_3941,
														string_append_3
														(BGl_string2744z00zz__r4_strings_6_7z00,
															BGl_string2742z00zz__r4_strings_6_7z00,
															BGl_string2743z00zz__r4_strings_6_7z00),
														BgL_end1z00_227);
												}
											else
												{	/* Ieee/string.scm 1214 */
													BgL_b1z00_2047 = BgL_end1z00_227;
												}
										}
								}
							else
								{	/* Ieee/string.scm 1210 */
									BgL_b1z00_2047 = BINT(BgL_l1z00_2045);
								}
						}
						{	/* Ieee/string.scm 1356 */
							obj_t BgL_b2z00_2048;

							{	/* Ieee/string.scm 1357 */
								obj_t BgL_procz00_3949;

								BgL_procz00_3949 = BGl_symbol2766z00zz__r4_strings_6_7z00;
								if (CBOOL(BgL_end2z00_229))
									{	/* Ieee/string.scm 1210 */
										if (((long) CINT(BgL_end2z00_229) <= 0L))
											{	/* Ieee/string.scm 1212 */
												BgL_b2z00_2048 =
													BGl_errorz00zz__errorz00(BgL_procz00_3949,
													string_append_3
													(BGl_string2741z00zz__r4_strings_6_7z00,
														BGl_string2745z00zz__r4_strings_6_7z00,
														BGl_string2743z00zz__r4_strings_6_7z00),
													BgL_end2z00_229);
											}
										else
											{	/* Ieee/string.scm 1212 */
												if (((long) CINT(BgL_end2z00_229) > BgL_l2z00_2046))
													{	/* Ieee/string.scm 1214 */
														BgL_b2z00_2048 =
															BGl_errorz00zz__errorz00(BgL_procz00_3949,
															string_append_3
															(BGl_string2744z00zz__r4_strings_6_7z00,
																BGl_string2745z00zz__r4_strings_6_7z00,
																BGl_string2743z00zz__r4_strings_6_7z00),
															BgL_end2z00_229);
													}
												else
													{	/* Ieee/string.scm 1214 */
														BgL_b2z00_2048 = BgL_end2z00_229;
													}
											}
									}
								else
									{	/* Ieee/string.scm 1210 */
										BgL_b2z00_2048 = BINT(BgL_l2z00_2046);
									}
							}
							{	/* Ieee/string.scm 1357 */
								obj_t BgL_e1z00_2049;

								{	/* Ieee/string.scm 1358 */
									obj_t BgL_procz00_3957;

									BgL_procz00_3957 = BGl_symbol2766z00zz__r4_strings_6_7z00;
									if (CBOOL(BgL_start1z00_226))
										{	/* Ieee/string.scm 1196 */
											if (((long) CINT(BgL_start1z00_226) < 0L))
												{	/* Ieee/string.scm 1198 */
													BgL_e1z00_2049 =
														BGl_errorz00zz__errorz00(BgL_procz00_3957,
														string_append_3
														(BGl_string2746z00zz__r4_strings_6_7z00,
															BGl_string2747z00zz__r4_strings_6_7z00,
															BGl_string2743z00zz__r4_strings_6_7z00),
														BgL_start1z00_226);
												}
											else
												{	/* Ieee/string.scm 1198 */
													if (
														((long) CINT(BgL_start1z00_226) >= BgL_l1z00_2045))
														{	/* Ieee/string.scm 1200 */
															BgL_e1z00_2049 =
																BGl_errorz00zz__errorz00(BgL_procz00_3957,
																string_append_3
																(BGl_string2748z00zz__r4_strings_6_7z00,
																	BGl_string2747z00zz__r4_strings_6_7z00,
																	BGl_string2743z00zz__r4_strings_6_7z00),
																BgL_start1z00_226);
														}
													else
														{	/* Ieee/string.scm 1200 */
															BgL_e1z00_2049 = BgL_start1z00_226;
														}
												}
										}
									else
										{	/* Ieee/string.scm 1196 */
											BgL_e1z00_2049 = BINT(0L);
										}
								}
								{	/* Ieee/string.scm 1358 */
									obj_t BgL_e2z00_2050;

									{	/* Ieee/string.scm 1359 */
										obj_t BgL_procz00_3965;

										BgL_procz00_3965 = BGl_symbol2766z00zz__r4_strings_6_7z00;
										if (CBOOL(BgL_start2z00_228))
											{	/* Ieee/string.scm 1196 */
												if (((long) CINT(BgL_start2z00_228) < 0L))
													{	/* Ieee/string.scm 1198 */
														BgL_e2z00_2050 =
															BGl_errorz00zz__errorz00(BgL_procz00_3965,
															string_append_3
															(BGl_string2746z00zz__r4_strings_6_7z00,
																BGl_string2749z00zz__r4_strings_6_7z00,
																BGl_string2743z00zz__r4_strings_6_7z00),
															BgL_start2z00_228);
													}
												else
													{	/* Ieee/string.scm 1198 */
														if (
															((long) CINT(BgL_start2z00_228) >=
																BgL_l2z00_2046))
															{	/* Ieee/string.scm 1200 */
																BgL_e2z00_2050 =
																	BGl_errorz00zz__errorz00(BgL_procz00_3965,
																	string_append_3
																	(BGl_string2748z00zz__r4_strings_6_7z00,
																		BGl_string2749z00zz__r4_strings_6_7z00,
																		BGl_string2743z00zz__r4_strings_6_7z00),
																	BgL_start2z00_228);
															}
														else
															{	/* Ieee/string.scm 1200 */
																BgL_e2z00_2050 = BgL_start2z00_228;
															}
													}
											}
										else
											{	/* Ieee/string.scm 1196 */
												BgL_e2z00_2050 = BINT(0L);
											}
									}
									{	/* Ieee/string.scm 1359 */

										{
											long BgL_i1z00_2054;
											long BgL_i2z00_2055;

											BgL_i1z00_2054 = ((long) CINT(BgL_b1z00_2047) - 1L);
											BgL_i2z00_2055 = ((long) CINT(BgL_b2z00_2048) - 1L);
										BgL_zc3z04anonymousza31816ze3z87_2056:
											if ((BgL_i1z00_2054 < (long) CINT(BgL_e1z00_2049)))
												{	/* Ieee/string.scm 1363 */
													return ((bool_t) 1);
												}
											else
												{	/* Ieee/string.scm 1363 */
													if ((BgL_i2z00_2055 < (long) CINT(BgL_e2z00_2050)))
														{	/* Ieee/string.scm 1365 */
															return ((bool_t) 0);
														}
													else
														{	/* Ieee/string.scm 1365 */
															if (
																(STRING_REF(BgL_s1z00_224, BgL_i1z00_2054) ==
																	STRING_REF(BgL_s2z00_225, BgL_i2z00_2055)))
																{
																	long BgL_i2z00_8583;
																	long BgL_i1z00_8581;

																	BgL_i1z00_8581 = (BgL_i1z00_2054 - 1L);
																	BgL_i2z00_8583 = (BgL_i2z00_2055 - 1L);
																	BgL_i2z00_2055 = BgL_i2z00_8583;
																	BgL_i1z00_2054 = BgL_i1z00_8581;
																	goto BgL_zc3z04anonymousza31816ze3z87_2056;
																}
															else
																{	/* Ieee/string.scm 1367 */
																	return ((bool_t) 0);
																}
														}
												}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* _string-suffix-ci? */
	obj_t BGl__stringzd2suffixzd2cizf3zf3zz__r4_strings_6_7z00(obj_t
		BgL_env1190z00_239, obj_t BgL_opt1189z00_238)
	{
		{	/* Ieee/string.scm 1375 */
			{	/* Ieee/string.scm 1375 */
				obj_t BgL_s1z00_2067;
				obj_t BgL_s2z00_2068;

				BgL_s1z00_2067 = VECTOR_REF(BgL_opt1189z00_238, 0L);
				BgL_s2z00_2068 = VECTOR_REF(BgL_opt1189z00_238, 1L);
				switch (VECTOR_LENGTH(BgL_opt1189z00_238))
					{
					case 2L:

						{	/* Ieee/string.scm 1375 */

							{	/* Ieee/string.scm 1375 */
								bool_t BgL_tmpz00_8591;

								{	/* Ieee/string.scm 1375 */
									obj_t BgL_auxz00_8599;
									obj_t BgL_auxz00_8592;

									if (STRINGP(BgL_s2z00_2068))
										{	/* Ieee/string.scm 1375 */
											BgL_auxz00_8599 = BgL_s2z00_2068;
										}
									else
										{
											obj_t BgL_auxz00_8602;

											BgL_auxz00_8602 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(55679L),
												BGl_string2768z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00, BgL_s2z00_2068);
											FAILURE(BgL_auxz00_8602, BFALSE, BFALSE);
										}
									if (STRINGP(BgL_s1z00_2067))
										{	/* Ieee/string.scm 1375 */
											BgL_auxz00_8592 = BgL_s1z00_2067;
										}
									else
										{
											obj_t BgL_auxz00_8595;

											BgL_auxz00_8595 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(55679L),
												BGl_string2768z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00, BgL_s1z00_2067);
											FAILURE(BgL_auxz00_8595, BFALSE, BFALSE);
										}
									BgL_tmpz00_8591 =
										BGl_stringzd2suffixzd2cizf3zf3zz__r4_strings_6_7z00
										(BgL_auxz00_8592, BgL_auxz00_8599, BFALSE, BFALSE, BFALSE,
										BFALSE);
								}
								return BBOOL(BgL_tmpz00_8591);
							}
						}
						break;
					case 3L:

						{	/* Ieee/string.scm 1375 */
							obj_t BgL_start1z00_2075;

							BgL_start1z00_2075 = VECTOR_REF(BgL_opt1189z00_238, 2L);
							{	/* Ieee/string.scm 1375 */

								{	/* Ieee/string.scm 1375 */
									bool_t BgL_tmpz00_8609;

									{	/* Ieee/string.scm 1375 */
										obj_t BgL_auxz00_8617;
										obj_t BgL_auxz00_8610;

										if (STRINGP(BgL_s2z00_2068))
											{	/* Ieee/string.scm 1375 */
												BgL_auxz00_8617 = BgL_s2z00_2068;
											}
										else
											{
												obj_t BgL_auxz00_8620;

												BgL_auxz00_8620 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2658z00zz__r4_strings_6_7z00, BINT(55679L),
													BGl_string2768z00zz__r4_strings_6_7z00,
													BGl_string2660z00zz__r4_strings_6_7z00,
													BgL_s2z00_2068);
												FAILURE(BgL_auxz00_8620, BFALSE, BFALSE);
											}
										if (STRINGP(BgL_s1z00_2067))
											{	/* Ieee/string.scm 1375 */
												BgL_auxz00_8610 = BgL_s1z00_2067;
											}
										else
											{
												obj_t BgL_auxz00_8613;

												BgL_auxz00_8613 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2658z00zz__r4_strings_6_7z00, BINT(55679L),
													BGl_string2768z00zz__r4_strings_6_7z00,
													BGl_string2660z00zz__r4_strings_6_7z00,
													BgL_s1z00_2067);
												FAILURE(BgL_auxz00_8613, BFALSE, BFALSE);
											}
										BgL_tmpz00_8609 =
											BGl_stringzd2suffixzd2cizf3zf3zz__r4_strings_6_7z00
											(BgL_auxz00_8610, BgL_auxz00_8617, BgL_start1z00_2075,
											BFALSE, BFALSE, BFALSE);
									}
									return BBOOL(BgL_tmpz00_8609);
								}
							}
						}
						break;
					case 4L:

						{	/* Ieee/string.scm 1375 */
							obj_t BgL_start1z00_2079;

							BgL_start1z00_2079 = VECTOR_REF(BgL_opt1189z00_238, 2L);
							{	/* Ieee/string.scm 1375 */
								obj_t BgL_end1z00_2080;

								BgL_end1z00_2080 = VECTOR_REF(BgL_opt1189z00_238, 3L);
								{	/* Ieee/string.scm 1375 */

									{	/* Ieee/string.scm 1375 */
										bool_t BgL_tmpz00_8628;

										{	/* Ieee/string.scm 1375 */
											obj_t BgL_auxz00_8636;
											obj_t BgL_auxz00_8629;

											if (STRINGP(BgL_s2z00_2068))
												{	/* Ieee/string.scm 1375 */
													BgL_auxz00_8636 = BgL_s2z00_2068;
												}
											else
												{
													obj_t BgL_auxz00_8639;

													BgL_auxz00_8639 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2658z00zz__r4_strings_6_7z00,
														BINT(55679L),
														BGl_string2768z00zz__r4_strings_6_7z00,
														BGl_string2660z00zz__r4_strings_6_7z00,
														BgL_s2z00_2068);
													FAILURE(BgL_auxz00_8639, BFALSE, BFALSE);
												}
											if (STRINGP(BgL_s1z00_2067))
												{	/* Ieee/string.scm 1375 */
													BgL_auxz00_8629 = BgL_s1z00_2067;
												}
											else
												{
													obj_t BgL_auxz00_8632;

													BgL_auxz00_8632 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2658z00zz__r4_strings_6_7z00,
														BINT(55679L),
														BGl_string2768z00zz__r4_strings_6_7z00,
														BGl_string2660z00zz__r4_strings_6_7z00,
														BgL_s1z00_2067);
													FAILURE(BgL_auxz00_8632, BFALSE, BFALSE);
												}
											BgL_tmpz00_8628 =
												BGl_stringzd2suffixzd2cizf3zf3zz__r4_strings_6_7z00
												(BgL_auxz00_8629, BgL_auxz00_8636, BgL_start1z00_2079,
												BgL_end1z00_2080, BFALSE, BFALSE);
										}
										return BBOOL(BgL_tmpz00_8628);
									}
								}
							}
						}
						break;
					case 5L:

						{	/* Ieee/string.scm 1375 */
							obj_t BgL_start1z00_2083;

							BgL_start1z00_2083 = VECTOR_REF(BgL_opt1189z00_238, 2L);
							{	/* Ieee/string.scm 1375 */
								obj_t BgL_end1z00_2084;

								BgL_end1z00_2084 = VECTOR_REF(BgL_opt1189z00_238, 3L);
								{	/* Ieee/string.scm 1375 */
									obj_t BgL_start2z00_2085;

									BgL_start2z00_2085 = VECTOR_REF(BgL_opt1189z00_238, 4L);
									{	/* Ieee/string.scm 1375 */

										{	/* Ieee/string.scm 1375 */
											bool_t BgL_tmpz00_8648;

											{	/* Ieee/string.scm 1375 */
												obj_t BgL_auxz00_8656;
												obj_t BgL_auxz00_8649;

												if (STRINGP(BgL_s2z00_2068))
													{	/* Ieee/string.scm 1375 */
														BgL_auxz00_8656 = BgL_s2z00_2068;
													}
												else
													{
														obj_t BgL_auxz00_8659;

														BgL_auxz00_8659 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2658z00zz__r4_strings_6_7z00,
															BINT(55679L),
															BGl_string2768z00zz__r4_strings_6_7z00,
															BGl_string2660z00zz__r4_strings_6_7z00,
															BgL_s2z00_2068);
														FAILURE(BgL_auxz00_8659, BFALSE, BFALSE);
													}
												if (STRINGP(BgL_s1z00_2067))
													{	/* Ieee/string.scm 1375 */
														BgL_auxz00_8649 = BgL_s1z00_2067;
													}
												else
													{
														obj_t BgL_auxz00_8652;

														BgL_auxz00_8652 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2658z00zz__r4_strings_6_7z00,
															BINT(55679L),
															BGl_string2768z00zz__r4_strings_6_7z00,
															BGl_string2660z00zz__r4_strings_6_7z00,
															BgL_s1z00_2067);
														FAILURE(BgL_auxz00_8652, BFALSE, BFALSE);
													}
												BgL_tmpz00_8648 =
													BGl_stringzd2suffixzd2cizf3zf3zz__r4_strings_6_7z00
													(BgL_auxz00_8649, BgL_auxz00_8656, BgL_start1z00_2083,
													BgL_end1z00_2084, BgL_start2z00_2085, BFALSE);
											}
											return BBOOL(BgL_tmpz00_8648);
										}
									}
								}
							}
						}
						break;
					case 6L:

						{	/* Ieee/string.scm 1375 */
							obj_t BgL_start1z00_2087;

							BgL_start1z00_2087 = VECTOR_REF(BgL_opt1189z00_238, 2L);
							{	/* Ieee/string.scm 1375 */
								obj_t BgL_end1z00_2088;

								BgL_end1z00_2088 = VECTOR_REF(BgL_opt1189z00_238, 3L);
								{	/* Ieee/string.scm 1375 */
									obj_t BgL_start2z00_2089;

									BgL_start2z00_2089 = VECTOR_REF(BgL_opt1189z00_238, 4L);
									{	/* Ieee/string.scm 1375 */
										obj_t BgL_end2z00_2090;

										BgL_end2z00_2090 = VECTOR_REF(BgL_opt1189z00_238, 5L);
										{	/* Ieee/string.scm 1375 */

											{	/* Ieee/string.scm 1375 */
												bool_t BgL_tmpz00_8669;

												{	/* Ieee/string.scm 1375 */
													obj_t BgL_auxz00_8677;
													obj_t BgL_auxz00_8670;

													if (STRINGP(BgL_s2z00_2068))
														{	/* Ieee/string.scm 1375 */
															BgL_auxz00_8677 = BgL_s2z00_2068;
														}
													else
														{
															obj_t BgL_auxz00_8680;

															BgL_auxz00_8680 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2658z00zz__r4_strings_6_7z00,
																BINT(55679L),
																BGl_string2768z00zz__r4_strings_6_7z00,
																BGl_string2660z00zz__r4_strings_6_7z00,
																BgL_s2z00_2068);
															FAILURE(BgL_auxz00_8680, BFALSE, BFALSE);
														}
													if (STRINGP(BgL_s1z00_2067))
														{	/* Ieee/string.scm 1375 */
															BgL_auxz00_8670 = BgL_s1z00_2067;
														}
													else
														{
															obj_t BgL_auxz00_8673;

															BgL_auxz00_8673 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2658z00zz__r4_strings_6_7z00,
																BINT(55679L),
																BGl_string2768z00zz__r4_strings_6_7z00,
																BGl_string2660z00zz__r4_strings_6_7z00,
																BgL_s1z00_2067);
															FAILURE(BgL_auxz00_8673, BFALSE, BFALSE);
														}
													BgL_tmpz00_8669 =
														BGl_stringzd2suffixzd2cizf3zf3zz__r4_strings_6_7z00
														(BgL_auxz00_8670, BgL_auxz00_8677,
														BgL_start1z00_2087, BgL_end1z00_2088,
														BgL_start2z00_2089, BgL_end2z00_2090);
												}
												return BBOOL(BgL_tmpz00_8669);
											}
										}
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* string-suffix-ci? */
	BGL_EXPORTED_DEF bool_t
		BGl_stringzd2suffixzd2cizf3zf3zz__r4_strings_6_7z00(obj_t BgL_s1z00_232,
		obj_t BgL_s2z00_233, obj_t BgL_start1z00_234, obj_t BgL_end1z00_235,
		obj_t BgL_start2z00_236, obj_t BgL_end2z00_237)
	{
		{	/* Ieee/string.scm 1375 */
			{	/* Ieee/string.scm 1377 */
				long BgL_l1z00_2091;

				BgL_l1z00_2091 = STRING_LENGTH(BgL_s1z00_232);
				{	/* Ieee/string.scm 1377 */
					long BgL_l2z00_2092;

					BgL_l2z00_2092 = STRING_LENGTH(BgL_s2z00_233);
					{	/* Ieee/string.scm 1378 */
						obj_t BgL_b1z00_2093;

						{	/* Ieee/string.scm 1379 */
							obj_t BgL_procz00_3989;

							BgL_procz00_3989 = BGl_symbol2739z00zz__r4_strings_6_7z00;
							if (CBOOL(BgL_end1z00_235))
								{	/* Ieee/string.scm 1210 */
									if (((long) CINT(BgL_end1z00_235) <= 0L))
										{	/* Ieee/string.scm 1212 */
											BgL_b1z00_2093 =
												BGl_errorz00zz__errorz00(BgL_procz00_3989,
												string_append_3(BGl_string2741z00zz__r4_strings_6_7z00,
													BGl_string2742z00zz__r4_strings_6_7z00,
													BGl_string2743z00zz__r4_strings_6_7z00),
												BgL_end1z00_235);
										}
									else
										{	/* Ieee/string.scm 1212 */
											if (((long) CINT(BgL_end1z00_235) > BgL_l1z00_2091))
												{	/* Ieee/string.scm 1214 */
													BgL_b1z00_2093 =
														BGl_errorz00zz__errorz00(BgL_procz00_3989,
														string_append_3
														(BGl_string2744z00zz__r4_strings_6_7z00,
															BGl_string2742z00zz__r4_strings_6_7z00,
															BGl_string2743z00zz__r4_strings_6_7z00),
														BgL_end1z00_235);
												}
											else
												{	/* Ieee/string.scm 1214 */
													BgL_b1z00_2093 = BgL_end1z00_235;
												}
										}
								}
							else
								{	/* Ieee/string.scm 1210 */
									BgL_b1z00_2093 = BINT(BgL_l1z00_2091);
								}
						}
						{	/* Ieee/string.scm 1379 */
							obj_t BgL_b2z00_2094;

							{	/* Ieee/string.scm 1380 */
								obj_t BgL_procz00_3997;

								BgL_procz00_3997 = BGl_symbol2739z00zz__r4_strings_6_7z00;
								if (CBOOL(BgL_end2z00_237))
									{	/* Ieee/string.scm 1210 */
										if (((long) CINT(BgL_end2z00_237) <= 0L))
											{	/* Ieee/string.scm 1212 */
												BgL_b2z00_2094 =
													BGl_errorz00zz__errorz00(BgL_procz00_3997,
													string_append_3
													(BGl_string2741z00zz__r4_strings_6_7z00,
														BGl_string2745z00zz__r4_strings_6_7z00,
														BGl_string2743z00zz__r4_strings_6_7z00),
													BgL_end2z00_237);
											}
										else
											{	/* Ieee/string.scm 1212 */
												if (((long) CINT(BgL_end2z00_237) > BgL_l2z00_2092))
													{	/* Ieee/string.scm 1214 */
														BgL_b2z00_2094 =
															BGl_errorz00zz__errorz00(BgL_procz00_3997,
															string_append_3
															(BGl_string2744z00zz__r4_strings_6_7z00,
																BGl_string2745z00zz__r4_strings_6_7z00,
																BGl_string2743z00zz__r4_strings_6_7z00),
															BgL_end2z00_237);
													}
												else
													{	/* Ieee/string.scm 1214 */
														BgL_b2z00_2094 = BgL_end2z00_237;
													}
											}
									}
								else
									{	/* Ieee/string.scm 1210 */
										BgL_b2z00_2094 = BINT(BgL_l2z00_2092);
									}
							}
							{	/* Ieee/string.scm 1380 */
								obj_t BgL_e1z00_2095;

								{	/* Ieee/string.scm 1381 */
									obj_t BgL_procz00_4005;

									BgL_procz00_4005 = BGl_symbol2739z00zz__r4_strings_6_7z00;
									if (CBOOL(BgL_start1z00_234))
										{	/* Ieee/string.scm 1196 */
											if (((long) CINT(BgL_start1z00_234) < 0L))
												{	/* Ieee/string.scm 1198 */
													BgL_e1z00_2095 =
														BGl_errorz00zz__errorz00(BgL_procz00_4005,
														string_append_3
														(BGl_string2746z00zz__r4_strings_6_7z00,
															BGl_string2747z00zz__r4_strings_6_7z00,
															BGl_string2743z00zz__r4_strings_6_7z00),
														BgL_start1z00_234);
												}
											else
												{	/* Ieee/string.scm 1198 */
													if (
														((long) CINT(BgL_start1z00_234) >= BgL_l1z00_2091))
														{	/* Ieee/string.scm 1200 */
															BgL_e1z00_2095 =
																BGl_errorz00zz__errorz00(BgL_procz00_4005,
																string_append_3
																(BGl_string2748z00zz__r4_strings_6_7z00,
																	BGl_string2747z00zz__r4_strings_6_7z00,
																	BGl_string2743z00zz__r4_strings_6_7z00),
																BgL_start1z00_234);
														}
													else
														{	/* Ieee/string.scm 1200 */
															BgL_e1z00_2095 = BgL_start1z00_234;
														}
												}
										}
									else
										{	/* Ieee/string.scm 1196 */
											BgL_e1z00_2095 = BINT(0L);
										}
								}
								{	/* Ieee/string.scm 1381 */
									obj_t BgL_e2z00_2096;

									{	/* Ieee/string.scm 1382 */
										obj_t BgL_procz00_4013;

										BgL_procz00_4013 = BGl_symbol2739z00zz__r4_strings_6_7z00;
										if (CBOOL(BgL_start2z00_236))
											{	/* Ieee/string.scm 1196 */
												if (((long) CINT(BgL_start2z00_236) < 0L))
													{	/* Ieee/string.scm 1198 */
														BgL_e2z00_2096 =
															BGl_errorz00zz__errorz00(BgL_procz00_4013,
															string_append_3
															(BGl_string2746z00zz__r4_strings_6_7z00,
																BGl_string2749z00zz__r4_strings_6_7z00,
																BGl_string2743z00zz__r4_strings_6_7z00),
															BgL_start2z00_236);
													}
												else
													{	/* Ieee/string.scm 1198 */
														if (
															((long) CINT(BgL_start2z00_236) >=
																BgL_l2z00_2092))
															{	/* Ieee/string.scm 1200 */
																BgL_e2z00_2096 =
																	BGl_errorz00zz__errorz00(BgL_procz00_4013,
																	string_append_3
																	(BGl_string2748z00zz__r4_strings_6_7z00,
																		BGl_string2749z00zz__r4_strings_6_7z00,
																		BGl_string2743z00zz__r4_strings_6_7z00),
																	BgL_start2z00_236);
															}
														else
															{	/* Ieee/string.scm 1200 */
																BgL_e2z00_2096 = BgL_start2z00_236;
															}
													}
											}
										else
											{	/* Ieee/string.scm 1196 */
												BgL_e2z00_2096 = BINT(0L);
											}
									}
									{	/* Ieee/string.scm 1382 */

										{
											long BgL_i1z00_2100;
											long BgL_i2z00_2101;

											BgL_i1z00_2100 = ((long) CINT(BgL_b1z00_2093) - 1L);
											BgL_i2z00_2101 = ((long) CINT(BgL_b2z00_2094) - 1L);
										BgL_zc3z04anonymousza31828ze3z87_2102:
											if ((BgL_i1z00_2100 < (long) CINT(BgL_e1z00_2095)))
												{	/* Ieee/string.scm 1386 */
													return ((bool_t) 1);
												}
											else
												{	/* Ieee/string.scm 1386 */
													if ((BgL_i2z00_2101 < (long) CINT(BgL_e2z00_2096)))
														{	/* Ieee/string.scm 1388 */
															return ((bool_t) 0);
														}
													else
														{	/* Ieee/string.scm 1388 */
															if (
																(toupper(STRING_REF(BgL_s1z00_232,
																			BgL_i1z00_2100)) ==
																	toupper(STRING_REF(BgL_s2z00_233,
																			BgL_i2z00_2101))))
																{
																	long BgL_i2z00_8756;
																	long BgL_i1z00_8754;

																	BgL_i1z00_8754 = (BgL_i1z00_2100 - 1L);
																	BgL_i2z00_8756 = (BgL_i2z00_2101 - 1L);
																	BgL_i2z00_2101 = BgL_i2z00_8756;
																	BgL_i1z00_2100 = BgL_i1z00_8754;
																	goto BgL_zc3z04anonymousza31828ze3z87_2102;
																}
															else
																{	/* Ieee/string.scm 1390 */
																	return ((bool_t) 0);
																}
														}
												}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* _string-natural-compare3 */
	obj_t BGl__stringzd2naturalzd2compare3z00zz__r4_strings_6_7z00(obj_t
		BgL_env1194z00_245, obj_t BgL_opt1193z00_244)
	{
		{	/* Ieee/string.scm 1398 */
			{	/* Ieee/string.scm 1398 */
				obj_t BgL_g1195z00_2113;
				obj_t BgL_g1196z00_2114;

				BgL_g1195z00_2113 = VECTOR_REF(BgL_opt1193z00_244, 0L);
				BgL_g1196z00_2114 = VECTOR_REF(BgL_opt1193z00_244, 1L);
				switch (VECTOR_LENGTH(BgL_opt1193z00_244))
					{
					case 2L:

						{	/* Ieee/string.scm 1398 */

							{	/* Ieee/string.scm 1398 */
								int BgL_res2179z00_4043;

								{	/* Ieee/string.scm 1398 */
									obj_t BgL_az00_4041;
									obj_t BgL_bz00_4042;

									if (STRINGP(BgL_g1195z00_2113))
										{	/* Ieee/string.scm 1398 */
											BgL_az00_4041 = BgL_g1195z00_2113;
										}
									else
										{
											obj_t BgL_auxz00_8766;

											BgL_auxz00_8766 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(56566L),
												BGl_string2769z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00,
												BgL_g1195z00_2113);
											FAILURE(BgL_auxz00_8766, BFALSE, BFALSE);
										}
									if (STRINGP(BgL_g1196z00_2114))
										{	/* Ieee/string.scm 1398 */
											BgL_bz00_4042 = BgL_g1196z00_2114;
										}
									else
										{
											obj_t BgL_auxz00_8772;

											BgL_auxz00_8772 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(56566L),
												BGl_string2769z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00,
												BgL_g1196z00_2114);
											FAILURE(BgL_auxz00_8772, BFALSE, BFALSE);
										}
									{	/* Ieee/string.scm 1399 */
										obj_t BgL_tmpz00_8776;

										{	/* Ieee/string.scm 1399 */
											obj_t BgL_aux2615z00_4917;

											BgL_aux2615z00_4917 =
												BGl_strnatcmpz00zz__r4_strings_6_7z00(BgL_az00_4041,
												BgL_bz00_4042, ((bool_t) 0), BINT(0L), BINT(0L));
											if (INTEGERP(BgL_aux2615z00_4917))
												{	/* Ieee/string.scm 1399 */
													BgL_tmpz00_8776 = BgL_aux2615z00_4917;
												}
											else
												{
													obj_t BgL_auxz00_8782;

													BgL_auxz00_8782 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2658z00zz__r4_strings_6_7z00,
														BINT(56640L),
														BGl_string2769z00zz__r4_strings_6_7z00,
														BGl_string2662z00zz__r4_strings_6_7z00,
														BgL_aux2615z00_4917);
													FAILURE(BgL_auxz00_8782, BFALSE, BFALSE);
												}
										}
										BgL_res2179z00_4043 = CINT(BgL_tmpz00_8776);
									}
								}
								return BINT(BgL_res2179z00_4043);
							}
						}
						break;
					case 3L:

						{	/* Ieee/string.scm 1398 */
							obj_t BgL_start1z00_2119;

							BgL_start1z00_2119 = VECTOR_REF(BgL_opt1193z00_244, 2L);
							{	/* Ieee/string.scm 1398 */

								{	/* Ieee/string.scm 1398 */
									int BgL_res2180z00_4046;

									{	/* Ieee/string.scm 1398 */
										obj_t BgL_az00_4044;
										obj_t BgL_bz00_4045;

										if (STRINGP(BgL_g1195z00_2113))
											{	/* Ieee/string.scm 1398 */
												BgL_az00_4044 = BgL_g1195z00_2113;
											}
										else
											{
												obj_t BgL_auxz00_8791;

												BgL_auxz00_8791 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2658z00zz__r4_strings_6_7z00, BINT(56566L),
													BGl_string2769z00zz__r4_strings_6_7z00,
													BGl_string2660z00zz__r4_strings_6_7z00,
													BgL_g1195z00_2113);
												FAILURE(BgL_auxz00_8791, BFALSE, BFALSE);
											}
										if (STRINGP(BgL_g1196z00_2114))
											{	/* Ieee/string.scm 1398 */
												BgL_bz00_4045 = BgL_g1196z00_2114;
											}
										else
											{
												obj_t BgL_auxz00_8797;

												BgL_auxz00_8797 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2658z00zz__r4_strings_6_7z00, BINT(56566L),
													BGl_string2769z00zz__r4_strings_6_7z00,
													BGl_string2660z00zz__r4_strings_6_7z00,
													BgL_g1196z00_2114);
												FAILURE(BgL_auxz00_8797, BFALSE, BFALSE);
											}
										{	/* Ieee/string.scm 1399 */
											obj_t BgL_tmpz00_8801;

											{	/* Ieee/string.scm 1399 */
												obj_t BgL_aux2620z00_4922;

												BgL_aux2620z00_4922 =
													BGl_strnatcmpz00zz__r4_strings_6_7z00(BgL_az00_4044,
													BgL_bz00_4045, ((bool_t) 0), BgL_start1z00_2119,
													BINT(0L));
												if (INTEGERP(BgL_aux2620z00_4922))
													{	/* Ieee/string.scm 1399 */
														BgL_tmpz00_8801 = BgL_aux2620z00_4922;
													}
												else
													{
														obj_t BgL_auxz00_8806;

														BgL_auxz00_8806 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2658z00zz__r4_strings_6_7z00,
															BINT(56640L),
															BGl_string2769z00zz__r4_strings_6_7z00,
															BGl_string2662z00zz__r4_strings_6_7z00,
															BgL_aux2620z00_4922);
														FAILURE(BgL_auxz00_8806, BFALSE, BFALSE);
													}
											}
											BgL_res2180z00_4046 = CINT(BgL_tmpz00_8801);
										}
									}
									return BINT(BgL_res2180z00_4046);
								}
							}
						}
						break;
					case 4L:

						{	/* Ieee/string.scm 1398 */
							obj_t BgL_start1z00_2121;

							BgL_start1z00_2121 = VECTOR_REF(BgL_opt1193z00_244, 2L);
							{	/* Ieee/string.scm 1398 */
								obj_t BgL_start2z00_2122;

								BgL_start2z00_2122 = VECTOR_REF(BgL_opt1193z00_244, 3L);
								{	/* Ieee/string.scm 1398 */

									{	/* Ieee/string.scm 1398 */
										int BgL_res2181z00_4049;

										{	/* Ieee/string.scm 1398 */
											obj_t BgL_az00_4047;
											obj_t BgL_bz00_4048;

											if (STRINGP(BgL_g1195z00_2113))
												{	/* Ieee/string.scm 1398 */
													BgL_az00_4047 = BgL_g1195z00_2113;
												}
											else
												{
													obj_t BgL_auxz00_8816;

													BgL_auxz00_8816 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2658z00zz__r4_strings_6_7z00,
														BINT(56566L),
														BGl_string2769z00zz__r4_strings_6_7z00,
														BGl_string2660z00zz__r4_strings_6_7z00,
														BgL_g1195z00_2113);
													FAILURE(BgL_auxz00_8816, BFALSE, BFALSE);
												}
											if (STRINGP(BgL_g1196z00_2114))
												{	/* Ieee/string.scm 1398 */
													BgL_bz00_4048 = BgL_g1196z00_2114;
												}
											else
												{
													obj_t BgL_auxz00_8822;

													BgL_auxz00_8822 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2658z00zz__r4_strings_6_7z00,
														BINT(56566L),
														BGl_string2769z00zz__r4_strings_6_7z00,
														BGl_string2660z00zz__r4_strings_6_7z00,
														BgL_g1196z00_2114);
													FAILURE(BgL_auxz00_8822, BFALSE, BFALSE);
												}
											{	/* Ieee/string.scm 1399 */
												obj_t BgL_tmpz00_8826;

												{	/* Ieee/string.scm 1399 */
													obj_t BgL_aux2625z00_4927;

													BgL_aux2625z00_4927 =
														BGl_strnatcmpz00zz__r4_strings_6_7z00(BgL_az00_4047,
														BgL_bz00_4048, ((bool_t) 0), BgL_start1z00_2121,
														BgL_start2z00_2122);
													if (INTEGERP(BgL_aux2625z00_4927))
														{	/* Ieee/string.scm 1399 */
															BgL_tmpz00_8826 = BgL_aux2625z00_4927;
														}
													else
														{
															obj_t BgL_auxz00_8830;

															BgL_auxz00_8830 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2658z00zz__r4_strings_6_7z00,
																BINT(56640L),
																BGl_string2769z00zz__r4_strings_6_7z00,
																BGl_string2662z00zz__r4_strings_6_7z00,
																BgL_aux2625z00_4927);
															FAILURE(BgL_auxz00_8830, BFALSE, BFALSE);
														}
												}
												BgL_res2181z00_4049 = CINT(BgL_tmpz00_8826);
											}
										}
										return BINT(BgL_res2181z00_4049);
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* string-natural-compare3 */
	BGL_EXPORTED_DEF int
		BGl_stringzd2naturalzd2compare3z00zz__r4_strings_6_7z00(obj_t BgL_az00_240,
		obj_t BgL_bz00_241, obj_t BgL_start1z00_242, obj_t BgL_start2z00_243)
	{
		{	/* Ieee/string.scm 1398 */
			return
				CINT(BGl_strnatcmpz00zz__r4_strings_6_7z00(BgL_az00_240, BgL_bz00_241,
					((bool_t) 0), BgL_start1z00_242, BgL_start2z00_243));
		}

	}



/* _string-natural-compare3-ci */
	obj_t BGl__stringzd2naturalzd2compare3zd2cizd2zz__r4_strings_6_7z00(obj_t
		BgL_env1200z00_251, obj_t BgL_opt1199z00_250)
	{
		{	/* Ieee/string.scm 1404 */
			{	/* Ieee/string.scm 1404 */
				obj_t BgL_g1201z00_2123;
				obj_t BgL_g1202z00_2124;

				BgL_g1201z00_2123 = VECTOR_REF(BgL_opt1199z00_250, 0L);
				BgL_g1202z00_2124 = VECTOR_REF(BgL_opt1199z00_250, 1L);
				switch (VECTOR_LENGTH(BgL_opt1199z00_250))
					{
					case 2L:

						{	/* Ieee/string.scm 1404 */

							{	/* Ieee/string.scm 1404 */
								int BgL_res2182z00_4052;

								{	/* Ieee/string.scm 1404 */
									obj_t BgL_az00_4050;
									obj_t BgL_bz00_4051;

									if (STRINGP(BgL_g1201z00_2123))
										{	/* Ieee/string.scm 1404 */
											BgL_az00_4050 = BgL_g1201z00_2123;
										}
									else
										{
											obj_t BgL_auxz00_8844;

											BgL_auxz00_8844 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(56897L),
												BGl_string2770z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00,
												BgL_g1201z00_2123);
											FAILURE(BgL_auxz00_8844, BFALSE, BFALSE);
										}
									if (STRINGP(BgL_g1202z00_2124))
										{	/* Ieee/string.scm 1404 */
											BgL_bz00_4051 = BgL_g1202z00_2124;
										}
									else
										{
											obj_t BgL_auxz00_8850;

											BgL_auxz00_8850 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(56897L),
												BGl_string2770z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00,
												BgL_g1202z00_2124);
											FAILURE(BgL_auxz00_8850, BFALSE, BFALSE);
										}
									{	/* Ieee/string.scm 1405 */
										obj_t BgL_tmpz00_8854;

										{	/* Ieee/string.scm 1405 */
											obj_t BgL_aux2630z00_4932;

											BgL_aux2630z00_4932 =
												BGl_strnatcmpz00zz__r4_strings_6_7z00(BgL_az00_4050,
												BgL_bz00_4051, ((bool_t) 1), BINT(0L), BINT(0L));
											if (INTEGERP(BgL_aux2630z00_4932))
												{	/* Ieee/string.scm 1405 */
													BgL_tmpz00_8854 = BgL_aux2630z00_4932;
												}
											else
												{
													obj_t BgL_auxz00_8860;

													BgL_auxz00_8860 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2658z00zz__r4_strings_6_7z00,
														BINT(56974L),
														BGl_string2770z00zz__r4_strings_6_7z00,
														BGl_string2662z00zz__r4_strings_6_7z00,
														BgL_aux2630z00_4932);
													FAILURE(BgL_auxz00_8860, BFALSE, BFALSE);
												}
										}
										BgL_res2182z00_4052 = CINT(BgL_tmpz00_8854);
									}
								}
								return BINT(BgL_res2182z00_4052);
							}
						}
						break;
					case 3L:

						{	/* Ieee/string.scm 1404 */
							obj_t BgL_start1z00_2129;

							BgL_start1z00_2129 = VECTOR_REF(BgL_opt1199z00_250, 2L);
							{	/* Ieee/string.scm 1404 */

								{	/* Ieee/string.scm 1404 */
									int BgL_res2183z00_4055;

									{	/* Ieee/string.scm 1404 */
										obj_t BgL_az00_4053;
										obj_t BgL_bz00_4054;

										if (STRINGP(BgL_g1201z00_2123))
											{	/* Ieee/string.scm 1404 */
												BgL_az00_4053 = BgL_g1201z00_2123;
											}
										else
											{
												obj_t BgL_auxz00_8869;

												BgL_auxz00_8869 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2658z00zz__r4_strings_6_7z00, BINT(56897L),
													BGl_string2770z00zz__r4_strings_6_7z00,
													BGl_string2660z00zz__r4_strings_6_7z00,
													BgL_g1201z00_2123);
												FAILURE(BgL_auxz00_8869, BFALSE, BFALSE);
											}
										if (STRINGP(BgL_g1202z00_2124))
											{	/* Ieee/string.scm 1404 */
												BgL_bz00_4054 = BgL_g1202z00_2124;
											}
										else
											{
												obj_t BgL_auxz00_8875;

												BgL_auxz00_8875 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2658z00zz__r4_strings_6_7z00, BINT(56897L),
													BGl_string2770z00zz__r4_strings_6_7z00,
													BGl_string2660z00zz__r4_strings_6_7z00,
													BgL_g1202z00_2124);
												FAILURE(BgL_auxz00_8875, BFALSE, BFALSE);
											}
										{	/* Ieee/string.scm 1405 */
											obj_t BgL_tmpz00_8879;

											{	/* Ieee/string.scm 1405 */
												obj_t BgL_aux2635z00_4937;

												BgL_aux2635z00_4937 =
													BGl_strnatcmpz00zz__r4_strings_6_7z00(BgL_az00_4053,
													BgL_bz00_4054, ((bool_t) 1), BgL_start1z00_2129,
													BINT(0L));
												if (INTEGERP(BgL_aux2635z00_4937))
													{	/* Ieee/string.scm 1405 */
														BgL_tmpz00_8879 = BgL_aux2635z00_4937;
													}
												else
													{
														obj_t BgL_auxz00_8884;

														BgL_auxz00_8884 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2658z00zz__r4_strings_6_7z00,
															BINT(56974L),
															BGl_string2770z00zz__r4_strings_6_7z00,
															BGl_string2662z00zz__r4_strings_6_7z00,
															BgL_aux2635z00_4937);
														FAILURE(BgL_auxz00_8884, BFALSE, BFALSE);
													}
											}
											BgL_res2183z00_4055 = CINT(BgL_tmpz00_8879);
										}
									}
									return BINT(BgL_res2183z00_4055);
								}
							}
						}
						break;
					case 4L:

						{	/* Ieee/string.scm 1404 */
							obj_t BgL_start1z00_2131;

							BgL_start1z00_2131 = VECTOR_REF(BgL_opt1199z00_250, 2L);
							{	/* Ieee/string.scm 1404 */
								obj_t BgL_start2z00_2132;

								BgL_start2z00_2132 = VECTOR_REF(BgL_opt1199z00_250, 3L);
								{	/* Ieee/string.scm 1404 */

									{	/* Ieee/string.scm 1404 */
										int BgL_res2184z00_4058;

										{	/* Ieee/string.scm 1404 */
											obj_t BgL_az00_4056;
											obj_t BgL_bz00_4057;

											if (STRINGP(BgL_g1201z00_2123))
												{	/* Ieee/string.scm 1404 */
													BgL_az00_4056 = BgL_g1201z00_2123;
												}
											else
												{
													obj_t BgL_auxz00_8894;

													BgL_auxz00_8894 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2658z00zz__r4_strings_6_7z00,
														BINT(56897L),
														BGl_string2770z00zz__r4_strings_6_7z00,
														BGl_string2660z00zz__r4_strings_6_7z00,
														BgL_g1201z00_2123);
													FAILURE(BgL_auxz00_8894, BFALSE, BFALSE);
												}
											if (STRINGP(BgL_g1202z00_2124))
												{	/* Ieee/string.scm 1404 */
													BgL_bz00_4057 = BgL_g1202z00_2124;
												}
											else
												{
													obj_t BgL_auxz00_8900;

													BgL_auxz00_8900 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2658z00zz__r4_strings_6_7z00,
														BINT(56897L),
														BGl_string2770z00zz__r4_strings_6_7z00,
														BGl_string2660z00zz__r4_strings_6_7z00,
														BgL_g1202z00_2124);
													FAILURE(BgL_auxz00_8900, BFALSE, BFALSE);
												}
											{	/* Ieee/string.scm 1405 */
												obj_t BgL_tmpz00_8904;

												{	/* Ieee/string.scm 1405 */
													obj_t BgL_aux2640z00_4942;

													BgL_aux2640z00_4942 =
														BGl_strnatcmpz00zz__r4_strings_6_7z00(BgL_az00_4056,
														BgL_bz00_4057, ((bool_t) 1), BgL_start1z00_2131,
														BgL_start2z00_2132);
													if (INTEGERP(BgL_aux2640z00_4942))
														{	/* Ieee/string.scm 1405 */
															BgL_tmpz00_8904 = BgL_aux2640z00_4942;
														}
													else
														{
															obj_t BgL_auxz00_8908;

															BgL_auxz00_8908 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2658z00zz__r4_strings_6_7z00,
																BINT(56974L),
																BGl_string2770z00zz__r4_strings_6_7z00,
																BGl_string2662z00zz__r4_strings_6_7z00,
																BgL_aux2640z00_4942);
															FAILURE(BgL_auxz00_8908, BFALSE, BFALSE);
														}
												}
												BgL_res2184z00_4058 = CINT(BgL_tmpz00_8904);
											}
										}
										return BINT(BgL_res2184z00_4058);
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* string-natural-compare3-ci */
	BGL_EXPORTED_DEF int
		BGl_stringzd2naturalzd2compare3zd2cizd2zz__r4_strings_6_7z00(obj_t
		BgL_az00_246, obj_t BgL_bz00_247, obj_t BgL_start1z00_248,
		obj_t BgL_start2z00_249)
	{
		{	/* Ieee/string.scm 1404 */
			return
				CINT(BGl_strnatcmpz00zz__r4_strings_6_7z00(BgL_az00_246, BgL_bz00_247,
					((bool_t) 1), BgL_start1z00_248, BgL_start2z00_249));
		}

	}



/* strnatcmp */
	obj_t BGl_strnatcmpz00zz__r4_strings_6_7z00(obj_t BgL_az00_252,
		obj_t BgL_bz00_253, bool_t BgL_foldcasez00_254, obj_t BgL_start1z00_255,
		obj_t BgL_start2z00_256)
	{
		{	/* Ieee/string.scm 1410 */
			{
				obj_t BgL_iaz00_2134;
				obj_t BgL_ibz00_2135;

				BgL_iaz00_2134 = BgL_start1z00_255;
				BgL_ibz00_2135 = BgL_start2z00_256;
			BgL_zc3z04anonymousza31838ze3z87_2136:
				{	/* Ieee/string.scm 1413 */
					unsigned char BgL_caz00_2137;
					unsigned char BgL_cbz00_2138;

					{	/* Ieee/string.scm 1413 */
						obj_t BgL_iz00_4059;

						BgL_iz00_4059 = BgL_iaz00_2134;
						if (((long) CINT(BgL_iz00_4059) >= STRING_LENGTH(BgL_az00_252)))
							{	/* Ieee/string.scm 1500 */
								BgL_caz00_2137 = ((unsigned char) '\000');
							}
						else
							{	/* Ieee/string.scm 1500 */
								BgL_caz00_2137 =
									STRING_REF(BgL_az00_252, (long) CINT(BgL_iz00_4059));
					}}
					{	/* Ieee/string.scm 1414 */
						obj_t BgL_iz00_4067;

						BgL_iz00_4067 = BgL_ibz00_2135;
						if (((long) CINT(BgL_iz00_4067) >= STRING_LENGTH(BgL_bz00_253)))
							{	/* Ieee/string.scm 1500 */
								BgL_cbz00_2138 = ((unsigned char) '\000');
							}
						else
							{	/* Ieee/string.scm 1500 */
								BgL_cbz00_2138 =
									STRING_REF(BgL_bz00_253, (long) CINT(BgL_iz00_4067));
					}}
					{

						if (isspace(BgL_caz00_2137))
							{	/* Ieee/string.scm 1416 */
								BgL_iaz00_2134 = ADDFX(BgL_iaz00_2134, BINT(1L));
								{	/* Ieee/string.scm 1418 */
									long BgL_iz00_4077;

									BgL_iz00_4077 = (long) CINT(BgL_iaz00_2134);
									if ((BgL_iz00_4077 >= STRING_LENGTH(BgL_az00_252)))
										{	/* Ieee/string.scm 1500 */
											BgL_caz00_2137 = ((unsigned char) '\000');
										}
									else
										{	/* Ieee/string.scm 1500 */
											BgL_caz00_2137 = STRING_REF(BgL_az00_252, BgL_iz00_4077);
										}
								}
							}
						else
							{	/* Ieee/string.scm 1416 */
								BFALSE;
							}
					}
					{

						if (isspace(BgL_cbz00_2138))
							{	/* Ieee/string.scm 1420 */
								BgL_ibz00_2135 = ADDFX(BgL_ibz00_2135, BINT(1L));
								{	/* Ieee/string.scm 1422 */
									long BgL_iz00_4087;

									BgL_iz00_4087 = (long) CINT(BgL_ibz00_2135);
									if ((BgL_iz00_4087 >= STRING_LENGTH(BgL_bz00_253)))
										{	/* Ieee/string.scm 1500 */
											BgL_cbz00_2138 = ((unsigned char) '\000');
										}
									else
										{	/* Ieee/string.scm 1500 */
											BgL_cbz00_2138 = STRING_REF(BgL_bz00_253, BgL_iz00_4087);
										}
								}
							}
						else
							{	/* Ieee/string.scm 1420 */
								BFALSE;
							}
					}
					{	/* Ieee/string.scm 1424 */
						bool_t BgL_test3512z00_8948;

						if (isdigit(BgL_caz00_2137))
							{	/* Ieee/string.scm 1424 */
								BgL_test3512z00_8948 = isdigit(BgL_cbz00_2138);
							}
						else
							{	/* Ieee/string.scm 1424 */
								BgL_test3512z00_8948 = ((bool_t) 0);
							}
						if (BgL_test3512z00_8948)
							{	/* Ieee/string.scm 1425 */
								bool_t BgL_test3514z00_8952;

								if ((BgL_caz00_2137 == ((unsigned char) '0')))
									{	/* Ieee/string.scm 1425 */
										BgL_test3514z00_8952 =
											(BgL_cbz00_2138 == ((unsigned char) '0'));
									}
								else
									{	/* Ieee/string.scm 1425 */
										BgL_test3514z00_8952 = ((bool_t) 0);
									}
								if (BgL_test3514z00_8952)
									{	/* Ieee/string.scm 1426 */
										long BgL_arg1847z00_2151;
										long BgL_arg1848z00_2152;

										BgL_arg1847z00_2151 = ((long) CINT(BgL_iaz00_2134) + 1L);
										BgL_arg1848z00_2152 = ((long) CINT(BgL_ibz00_2135) + 1L);
										{
											obj_t BgL_ibz00_8962;
											obj_t BgL_iaz00_8960;

											BgL_iaz00_8960 = BINT(BgL_arg1847z00_2151);
											BgL_ibz00_8962 = BINT(BgL_arg1848z00_2152);
											BgL_ibz00_2135 = BgL_ibz00_8962;
											BgL_iaz00_2134 = BgL_iaz00_8960;
											goto BgL_zc3z04anonymousza31838ze3z87_2136;
										}
									}
								else
									{	/* Ieee/string.scm 1427 */
										obj_t BgL_resultz00_2153;

										{	/* Ieee/string.scm 1427 */
											bool_t BgL_test3516z00_8964;

											if ((BgL_caz00_2137 == ((unsigned char) '0')))
												{	/* Ieee/string.scm 1427 */
													BgL_test3516z00_8964 = ((bool_t) 1);
												}
											else
												{	/* Ieee/string.scm 1427 */
													BgL_test3516z00_8964 =
														(BgL_cbz00_2138 == ((unsigned char) '0'));
												}
											if (BgL_test3516z00_8964)
												{	/* Ieee/string.scm 1427 */
													BgL_resultz00_2153 =
														BGl_comparezd2leftzd2zz__r4_strings_6_7z00
														(BgL_az00_252, BgL_iaz00_2134, BgL_bz00_253,
														BgL_ibz00_2135);
												}
											else
												{	/* Ieee/string.scm 1427 */
													BgL_resultz00_2153 =
														BGl_comparezd2rightzd2zz__r4_strings_6_7z00
														(BgL_az00_252, BgL_iaz00_2134, BgL_bz00_253,
														BgL_ibz00_2135);
												}
										}
										if (INTEGERP(BgL_resultz00_2153))
											{	/* Ieee/string.scm 1432 */
												long BgL_arg1850z00_2155;
												long BgL_arg1851z00_2156;

												BgL_arg1850z00_2155 =
													(
													(long) CINT(BgL_iaz00_2134) +
													(long) CINT(BgL_resultz00_2153));
												BgL_arg1851z00_2156 =
													(
													(long) CINT(BgL_ibz00_2135) +
													(long) CINT(BgL_resultz00_2153));
												{
													obj_t BgL_ibz00_8980;
													obj_t BgL_iaz00_8978;

													BgL_iaz00_8978 = BINT(BgL_arg1850z00_2155);
													BgL_ibz00_8980 = BINT(BgL_arg1851z00_2156);
													BgL_ibz00_2135 = BgL_ibz00_8980;
													BgL_iaz00_2134 = BgL_iaz00_8978;
													goto BgL_zc3z04anonymousza31838ze3z87_2136;
												}
											}
										else
											{	/* Ieee/string.scm 1431 */
												if (CBOOL(BgL_resultz00_2153))
													{	/* Ieee/string.scm 1433 */
														return BINT(1L);
													}
												else
													{	/* Ieee/string.scm 1433 */
														return BINT(-1L);
													}
											}
									}
							}
						else
							{	/* Ieee/string.scm 1437 */
								bool_t BgL_test3520z00_8986;

								if ((BgL_caz00_2137 == ((unsigned char) '\000')))
									{	/* Ieee/string.scm 1437 */
										BgL_test3520z00_8986 =
											(BgL_cbz00_2138 == ((unsigned char) '\000'));
									}
								else
									{	/* Ieee/string.scm 1437 */
										BgL_test3520z00_8986 = ((bool_t) 0);
									}
								if (BgL_test3520z00_8986)
									{	/* Ieee/string.scm 1437 */
										return BINT(0L);
									}
								else
									{	/* Ieee/string.scm 1439 */
										bool_t BgL_test3522z00_8991;

										if (BgL_foldcasez00_254)
											{	/* Ieee/string.scm 1439 */
												BgL_caz00_2137 = toupper(BgL_caz00_2137);
												BgL_cbz00_2138 = toupper(BgL_cbz00_2138);
												BgL_test3522z00_8991 = ((bool_t) 0);
											}
										else
											{	/* Ieee/string.scm 1439 */
												BgL_test3522z00_8991 = ((bool_t) 0);
											}
										if (BgL_test3522z00_8991)
											{	/* Ieee/string.scm 1439 */
												return BGl_symbol2771z00zz__r4_strings_6_7z00;
											}
										else
											{	/* Ieee/string.scm 1439 */
												if ((BgL_caz00_2137 < BgL_cbz00_2138))
													{	/* Ieee/string.scm 1444 */
														return BINT(-1L);
													}
												else
													{	/* Ieee/string.scm 1444 */
														if ((BgL_caz00_2137 > BgL_cbz00_2138))
															{	/* Ieee/string.scm 1446 */
																return BINT(1L);
															}
														else
															{	/* Ieee/string.scm 1449 */
																long BgL_arg1859z00_2166;
																long BgL_arg1860z00_2167;

																BgL_arg1859z00_2166 =
																	((long) CINT(BgL_iaz00_2134) + 1L);
																BgL_arg1860z00_2167 =
																	((long) CINT(BgL_ibz00_2135) + 1L);
																{
																	obj_t BgL_ibz00_9007;
																	obj_t BgL_iaz00_9005;

																	BgL_iaz00_9005 = BINT(BgL_arg1859z00_2166);
																	BgL_ibz00_9007 = BINT(BgL_arg1860z00_2167);
																	BgL_ibz00_2135 = BgL_ibz00_9007;
																	BgL_iaz00_2134 = BgL_iaz00_9005;
																	goto BgL_zc3z04anonymousza31838ze3z87_2136;
																}
															}
													}
											}
									}
							}
					}
				}
			}
		}

	}



/* compare-right */
	obj_t BGl_comparezd2rightzd2zz__r4_strings_6_7z00(obj_t BgL_az00_257,
		obj_t BgL_iaz00_258, obj_t BgL_bz00_259, obj_t BgL_ibz00_260)
	{
		{	/* Ieee/string.scm 1454 */
			{
				obj_t BgL_biasz00_2172;
				long BgL_iz00_2173;

				BgL_biasz00_2172 = BUNSPEC;
				BgL_iz00_2173 = 0L;
			BgL_zc3z04anonymousza31861ze3z87_2174:
				{	/* Ieee/string.scm 1457 */
					unsigned char BgL_caz00_2175;
					unsigned char BgL_cbz00_2176;

					{	/* Ieee/string.scm 1457 */
						long BgL_arg1877z00_2194;

						BgL_arg1877z00_2194 = (BgL_iz00_2173 + (long) CINT(BgL_iaz00_258));
						if ((BgL_arg1877z00_2194 >= STRING_LENGTH(BgL_az00_257)))
							{	/* Ieee/string.scm 1500 */
								BgL_caz00_2175 = ((unsigned char) '\000');
							}
						else
							{	/* Ieee/string.scm 1500 */
								BgL_caz00_2175 = STRING_REF(BgL_az00_257, BgL_arg1877z00_2194);
							}
					}
					{	/* Ieee/string.scm 1458 */
						long BgL_arg1878z00_2195;

						BgL_arg1878z00_2195 = (BgL_iz00_2173 + (long) CINT(BgL_ibz00_260));
						if ((BgL_arg1878z00_2195 >= STRING_LENGTH(BgL_bz00_259)))
							{	/* Ieee/string.scm 1500 */
								BgL_cbz00_2176 = ((unsigned char) '\000');
							}
						else
							{	/* Ieee/string.scm 1500 */
								BgL_cbz00_2176 = STRING_REF(BgL_bz00_259, BgL_arg1878z00_2195);
							}
					}
					{	/* Ieee/string.scm 1460 */
						bool_t BgL_test3528z00_9021;

						if (isdigit(BgL_caz00_2175))
							{	/* Ieee/string.scm 1460 */
								BgL_test3528z00_9021 = ((bool_t) 0);
							}
						else
							{	/* Ieee/string.scm 1460 */
								if (isdigit(BgL_cbz00_2176))
									{	/* Ieee/string.scm 1460 */
										BgL_test3528z00_9021 = ((bool_t) 0);
									}
								else
									{	/* Ieee/string.scm 1460 */
										BgL_test3528z00_9021 = ((bool_t) 1);
									}
							}
						if (BgL_test3528z00_9021)
							{	/* Ieee/string.scm 1460 */
								if ((BgL_biasz00_2172 == BUNSPEC))
									{	/* Ieee/string.scm 1461 */
										return BINT(BgL_iz00_2173);
									}
								else
									{	/* Ieee/string.scm 1461 */
										return BgL_biasz00_2172;
									}
							}
						else
							{	/* Ieee/string.scm 1460 */
								if (isdigit(BgL_caz00_2175))
									{	/* Ieee/string.scm 1462 */
										if (isdigit(BgL_cbz00_2176))
											{	/* Ieee/string.scm 1464 */
												if ((BgL_caz00_2175 < BgL_cbz00_2176))
													{
														long BgL_iz00_9038;
														obj_t BgL_biasz00_9035;

														if ((BgL_biasz00_2172 == BUNSPEC))
															{	/* Ieee/string.scm 1467 */
																BgL_biasz00_9035 = BFALSE;
															}
														else
															{	/* Ieee/string.scm 1467 */
																BgL_biasz00_9035 = BgL_biasz00_2172;
															}
														BgL_iz00_9038 = (BgL_iz00_2173 + 1L);
														BgL_iz00_2173 = BgL_iz00_9038;
														BgL_biasz00_2172 = BgL_biasz00_9035;
														goto BgL_zc3z04anonymousza31861ze3z87_2174;
													}
												else
													{	/* Ieee/string.scm 1466 */
														if ((BgL_caz00_2175 > BgL_cbz00_2176))
															{
																long BgL_iz00_9045;
																obj_t BgL_biasz00_9042;

																if ((BgL_biasz00_2172 == BUNSPEC))
																	{	/* Ieee/string.scm 1469 */
																		BgL_biasz00_9042 = BTRUE;
																	}
																else
																	{	/* Ieee/string.scm 1469 */
																		BgL_biasz00_9042 = BgL_biasz00_2172;
																	}
																BgL_iz00_9045 = (BgL_iz00_2173 + 1L);
																BgL_iz00_2173 = BgL_iz00_9045;
																BgL_biasz00_2172 = BgL_biasz00_9042;
																goto BgL_zc3z04anonymousza31861ze3z87_2174;
															}
														else
															{	/* Ieee/string.scm 1470 */
																bool_t BgL_test3538z00_9047;

																if (
																	(BgL_caz00_2175 == ((unsigned char) '\000')))
																	{	/* Ieee/string.scm 1470 */
																		BgL_test3538z00_9047 =
																			(BgL_cbz00_2176 ==
																			((unsigned char) '\000'));
																	}
																else
																	{	/* Ieee/string.scm 1470 */
																		BgL_test3538z00_9047 = ((bool_t) 0);
																	}
																if (BgL_test3538z00_9047)
																	{	/* Ieee/string.scm 1470 */
																		if ((BgL_biasz00_2172 == BUNSPEC))
																			{	/* Ieee/string.scm 1471 */
																				return BINT(BgL_iz00_2173);
																			}
																		else
																			{	/* Ieee/string.scm 1471 */
																				return BgL_biasz00_2172;
																			}
																	}
																else
																	{
																		long BgL_iz00_9054;

																		BgL_iz00_9054 = (BgL_iz00_2173 + 1L);
																		BgL_iz00_2173 = BgL_iz00_9054;
																		goto BgL_zc3z04anonymousza31861ze3z87_2174;
																	}
															}
													}
											}
										else
											{	/* Ieee/string.scm 1464 */
												return BTRUE;
											}
									}
								else
									{	/* Ieee/string.scm 1462 */
										return BFALSE;
									}
							}
					}
				}
			}
		}

	}



/* compare-left */
	obj_t BGl_comparezd2leftzd2zz__r4_strings_6_7z00(obj_t BgL_az00_261,
		obj_t BgL_iaz00_262, obj_t BgL_bz00_263, obj_t BgL_ibz00_264)
	{
		{	/* Ieee/string.scm 1478 */
			{
				long BgL_iz00_2198;

				BgL_iz00_2198 = 0L;
			BgL_zc3z04anonymousza31879ze3z87_2199:
				{	/* Ieee/string.scm 1480 */
					unsigned char BgL_caz00_2200;
					unsigned char BgL_cbz00_2201;

					{	/* Ieee/string.scm 1480 */
						long BgL_arg1888z00_2212;

						BgL_arg1888z00_2212 = ((long) CINT(BgL_iaz00_262) + BgL_iz00_2198);
						if ((BgL_arg1888z00_2212 >= STRING_LENGTH(BgL_az00_261)))
							{	/* Ieee/string.scm 1500 */
								BgL_caz00_2200 = ((unsigned char) '\000');
							}
						else
							{	/* Ieee/string.scm 1500 */
								BgL_caz00_2200 = STRING_REF(BgL_az00_261, BgL_arg1888z00_2212);
							}
					}
					{	/* Ieee/string.scm 1481 */
						long BgL_arg1889z00_2213;

						BgL_arg1889z00_2213 = ((long) CINT(BgL_ibz00_264) + BgL_iz00_2198);
						if ((BgL_arg1889z00_2213 >= STRING_LENGTH(BgL_bz00_263)))
							{	/* Ieee/string.scm 1500 */
								BgL_cbz00_2201 = ((unsigned char) '\000');
							}
						else
							{	/* Ieee/string.scm 1500 */
								BgL_cbz00_2201 = STRING_REF(BgL_bz00_263, BgL_arg1889z00_2213);
							}
					}
					{	/* Ieee/string.scm 1483 */
						bool_t BgL_test3543z00_9068;

						if (isdigit(BgL_caz00_2200))
							{	/* Ieee/string.scm 1483 */
								BgL_test3543z00_9068 = ((bool_t) 0);
							}
						else
							{	/* Ieee/string.scm 1483 */
								if (isdigit(BgL_cbz00_2201))
									{	/* Ieee/string.scm 1483 */
										BgL_test3543z00_9068 = ((bool_t) 0);
									}
								else
									{	/* Ieee/string.scm 1483 */
										BgL_test3543z00_9068 = ((bool_t) 1);
									}
							}
						if (BgL_test3543z00_9068)
							{	/* Ieee/string.scm 1483 */
								return BINT(BgL_iz00_2198);
							}
						else
							{	/* Ieee/string.scm 1483 */
								if (isdigit(BgL_caz00_2200))
									{	/* Ieee/string.scm 1485 */
										if (isdigit(BgL_cbz00_2201))
											{	/* Ieee/string.scm 1487 */
												if ((BgL_caz00_2200 < BgL_cbz00_2201))
													{	/* Ieee/string.scm 1489 */
														return BFALSE;
													}
												else
													{	/* Ieee/string.scm 1489 */
														if ((BgL_caz00_2200 > BgL_cbz00_2201))
															{	/* Ieee/string.scm 1491 */
																return BTRUE;
															}
														else
															{
																long BgL_iz00_9082;

																BgL_iz00_9082 = (BgL_iz00_2198 + 1L);
																BgL_iz00_2198 = BgL_iz00_9082;
																goto BgL_zc3z04anonymousza31879ze3z87_2199;
															}
													}
											}
										else
											{	/* Ieee/string.scm 1487 */
												return BTRUE;
											}
									}
								else
									{	/* Ieee/string.scm 1485 */
										return BFALSE;
									}
							}
					}
				}
			}
		}

	}



/* hex-string-ref */
	obj_t BGl_hexzd2stringzd2refz00zz__r4_strings_6_7z00(obj_t BgL_strz00_267,
		long BgL_iz00_268)
	{
		{	/* Ieee/string.scm 1507 */
			{	/* Ieee/string.scm 1508 */
				unsigned char BgL_nz00_2218;

				BgL_nz00_2218 = STRING_REF(BgL_strz00_267, BgL_iz00_268);
				{	/* Ieee/string.scm 1510 */
					bool_t BgL_test3550z00_9085;

					if ((BgL_nz00_2218 >= ((unsigned char) '0')))
						{	/* Ieee/string.scm 1510 */
							BgL_test3550z00_9085 = (BgL_nz00_2218 <= ((unsigned char) '9'));
						}
					else
						{	/* Ieee/string.scm 1510 */
							BgL_test3550z00_9085 = ((bool_t) 0);
						}
					if (BgL_test3550z00_9085)
						{	/* Ieee/string.scm 1510 */
							return BINT(((BgL_nz00_2218) - 48L));
						}
					else
						{	/* Ieee/string.scm 1512 */
							bool_t BgL_test3552z00_9092;

							if ((BgL_nz00_2218 >= ((unsigned char) 'a')))
								{	/* Ieee/string.scm 1512 */
									BgL_test3552z00_9092 =
										(BgL_nz00_2218 <= ((unsigned char) 'f'));
								}
							else
								{	/* Ieee/string.scm 1512 */
									BgL_test3552z00_9092 = ((bool_t) 0);
								}
							if (BgL_test3552z00_9092)
								{	/* Ieee/string.scm 1512 */
									return BINT((10L + ((BgL_nz00_2218) - 97L)));
								}
							else
								{	/* Ieee/string.scm 1514 */
									bool_t BgL_test3554z00_9100;

									if ((BgL_nz00_2218 >= ((unsigned char) 'A')))
										{	/* Ieee/string.scm 1514 */
											BgL_test3554z00_9100 =
												(BgL_nz00_2218 <= ((unsigned char) 'F'));
										}
									else
										{	/* Ieee/string.scm 1514 */
											BgL_test3554z00_9100 = ((bool_t) 0);
										}
									if (BgL_test3554z00_9100)
										{	/* Ieee/string.scm 1514 */
											return BINT((10L + ((BgL_nz00_2218) - 65L)));
										}
									else
										{	/* Ieee/string.scm 1514 */
											return
												BGl_errorz00zz__errorz00
												(BGl_string2773z00zz__r4_strings_6_7z00,
												BGl_string2774z00zz__r4_strings_6_7z00, BgL_strz00_267);
										}
								}
						}
				}
			}
		}

	}



/* string-hex-intern */
	BGL_EXPORTED_DEF obj_t BGl_stringzd2hexzd2internz00zz__r4_strings_6_7z00(obj_t
		BgL_strz00_269)
	{
		{	/* Ieee/string.scm 1524 */
			{	/* Ieee/string.scm 1525 */
				long BgL_lenz00_2233;

				BgL_lenz00_2233 = STRING_LENGTH(BgL_strz00_269);
				if (ODDP_FX(BgL_lenz00_2233))
					{	/* Ieee/string.scm 1526 */
						return
							BGl_errorz00zz__errorz00(BGl_string2775z00zz__r4_strings_6_7z00,
							BGl_string2776z00zz__r4_strings_6_7z00, BgL_strz00_269);
					}
				else
					{	/* Ieee/string.scm 1528 */
						obj_t BgL_resz00_2235;

						{	/* Ieee/string.scm 1528 */
							long BgL_arg1916z00_2250;

							BgL_arg1916z00_2250 = (BgL_lenz00_2233 / 2L);
							{	/* Ieee/string.scm 172 */

								BgL_resz00_2235 =
									make_string(BgL_arg1916z00_2250, ((unsigned char) ' '));
						}}
						{
							long BgL_iz00_4229;
							long BgL_jz00_4230;

							BgL_iz00_4229 = 0L;
							BgL_jz00_4230 = 0L;
						BgL_loopz00_4228:
							if ((BgL_iz00_4229 == BgL_lenz00_2233))
								{	/* Ieee/string.scm 1531 */
									return BgL_resz00_2235;
								}
							else
								{	/* Ieee/string.scm 1533 */
									obj_t BgL_c1z00_4234;

									BgL_c1z00_4234 =
										BGl_hexzd2stringzd2refz00zz__r4_strings_6_7z00
										(BgL_strz00_269, BgL_iz00_4229);
									{	/* Ieee/string.scm 1533 */
										obj_t BgL_c2z00_4235;

										BgL_c2z00_4235 =
											BGl_hexzd2stringzd2refz00zz__r4_strings_6_7z00
											(BgL_strz00_269, (BgL_iz00_4229 + 1L));
										{	/* Ieee/string.scm 1534 */
											long BgL_cz00_4238;

											BgL_cz00_4238 =
												(
												((long) CINT(BgL_c1z00_4234) * 16L) +
												(long) CINT(BgL_c2z00_4235));
											{	/* Ieee/string.scm 1535 */

												{	/* Ieee/string.scm 337 */
													unsigned char BgL_tmpz00_9124;

													BgL_tmpz00_9124 = (BgL_cz00_4238);
													STRING_SET(BgL_resz00_2235, BgL_jz00_4230,
														BgL_tmpz00_9124);
												}
												{
													long BgL_jz00_9129;
													long BgL_iz00_9127;

													BgL_iz00_9127 = (BgL_iz00_4229 + 2L);
													BgL_jz00_9129 = (BgL_jz00_4230 + 1L);
													BgL_jz00_4230 = BgL_jz00_9129;
													BgL_iz00_4229 = BgL_iz00_9127;
													goto BgL_loopz00_4228;
												}
											}
										}
									}
								}
						}
					}
			}
		}

	}



/* &string-hex-intern */
	obj_t BGl_z62stringzd2hexzd2internz62zz__r4_strings_6_7z00(obj_t
		BgL_envz00_4485, obj_t BgL_strz00_4486)
	{
		{	/* Ieee/string.scm 1524 */
			{	/* Ieee/string.scm 1525 */
				obj_t BgL_auxz00_9131;

				if (STRINGP(BgL_strz00_4486))
					{	/* Ieee/string.scm 1525 */
						BgL_auxz00_9131 = BgL_strz00_4486;
					}
				else
					{
						obj_t BgL_auxz00_9134;

						BgL_auxz00_9134 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2658z00zz__r4_strings_6_7z00, BINT(61043L),
							BGl_string2777z00zz__r4_strings_6_7z00,
							BGl_string2660z00zz__r4_strings_6_7z00, BgL_strz00_4486);
						FAILURE(BgL_auxz00_9134, BFALSE, BFALSE);
					}
				return
					BGl_stringzd2hexzd2internz00zz__r4_strings_6_7z00(BgL_auxz00_9131);
			}
		}

	}



/* string-hex-intern! */
	BGL_EXPORTED_DEF obj_t
		BGl_stringzd2hexzd2internz12z12zz__r4_strings_6_7z00(obj_t BgL_strz00_270)
	{
		{	/* Ieee/string.scm 1542 */
			{	/* Ieee/string.scm 1543 */
				long BgL_lenz00_2253;

				BgL_lenz00_2253 = STRING_LENGTH(BgL_strz00_270);
				if (ODDP_FX(BgL_lenz00_2253))
					{	/* Ieee/string.scm 1544 */
						return
							BGl_errorz00zz__errorz00(BGl_string2778z00zz__r4_strings_6_7z00,
							BGl_string2776z00zz__r4_strings_6_7z00, BgL_strz00_270);
					}
				else
					{
						long BgL_iz00_2256;
						long BgL_jz00_2257;

						BgL_iz00_2256 = 0L;
						BgL_jz00_2257 = 0L;
					BgL_zc3z04anonymousza31918ze3z87_2258:
						if ((BgL_iz00_2256 == BgL_lenz00_2253))
							{	/* Ieee/string.scm 756 */
								long BgL_tmpz00_9145;

								BgL_tmpz00_9145 = (BgL_lenz00_2253 / 2L);
								return bgl_string_shrink(BgL_strz00_270, BgL_tmpz00_9145);
							}
						else
							{	/* Ieee/string.scm 1550 */
								obj_t BgL_c1z00_2261;

								BgL_c1z00_2261 =
									BGl_hexzd2stringzd2refz00zz__r4_strings_6_7z00(BgL_strz00_270,
									BgL_iz00_2256);
								{	/* Ieee/string.scm 1550 */
									obj_t BgL_c2z00_2262;

									BgL_c2z00_2262 =
										BGl_hexzd2stringzd2refz00zz__r4_strings_6_7z00
										(BgL_strz00_270, (BgL_iz00_2256 + 1L));
									{	/* Ieee/string.scm 1551 */
										long BgL_cz00_2263;

										BgL_cz00_2263 =
											(
											((long) CINT(BgL_c1z00_2261) * 16L) +
											(long) CINT(BgL_c2z00_2262));
										{	/* Ieee/string.scm 1552 */

											{	/* Ieee/string.scm 337 */
												unsigned char BgL_tmpz00_9155;

												BgL_tmpz00_9155 = (BgL_cz00_2263);
												STRING_SET(BgL_strz00_270, BgL_jz00_2257,
													BgL_tmpz00_9155);
											}
											{
												long BgL_jz00_9160;
												long BgL_iz00_9158;

												BgL_iz00_9158 = (BgL_iz00_2256 + 2L);
												BgL_jz00_9160 = (BgL_jz00_2257 + 1L);
												BgL_jz00_2257 = BgL_jz00_9160;
												BgL_iz00_2256 = BgL_iz00_9158;
												goto BgL_zc3z04anonymousza31918ze3z87_2258;
											}
										}
									}
								}
							}
					}
			}
		}

	}



/* &string-hex-intern! */
	obj_t BGl_z62stringzd2hexzd2internz12z70zz__r4_strings_6_7z00(obj_t
		BgL_envz00_4487, obj_t BgL_strz00_4488)
	{
		{	/* Ieee/string.scm 1542 */
			{	/* Ieee/string.scm 1543 */
				obj_t BgL_auxz00_9162;

				if (STRINGP(BgL_strz00_4488))
					{	/* Ieee/string.scm 1543 */
						BgL_auxz00_9162 = BgL_strz00_4488;
					}
				else
					{
						obj_t BgL_auxz00_9165;

						BgL_auxz00_9165 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2658z00zz__r4_strings_6_7z00, BINT(61734L),
							BGl_string2779z00zz__r4_strings_6_7z00,
							BGl_string2660z00zz__r4_strings_6_7z00, BgL_strz00_4488);
						FAILURE(BgL_auxz00_9165, BFALSE, BFALSE);
					}
				return
					BGl_stringzd2hexzd2internz12z12zz__r4_strings_6_7z00(BgL_auxz00_9162);
			}
		}

	}



/* _string-hex-extern */
	obj_t BGl__stringzd2hexzd2externz00zz__r4_strings_6_7z00(obj_t
		BgL_env1206z00_275, obj_t BgL_opt1205z00_274)
	{
		{	/* Ieee/string.scm 1559 */
			{	/* Ieee/string.scm 1559 */
				obj_t BgL_strz00_2270;

				BgL_strz00_2270 = VECTOR_REF(BgL_opt1205z00_274, 0L);
				switch (VECTOR_LENGTH(BgL_opt1205z00_274))
					{
					case 1L:

						{	/* Ieee/string.scm 1560 */
							long BgL_endz00_2274;

							{	/* Ieee/string.scm 1560 */
								obj_t BgL_stringz00_4269;

								if (STRINGP(BgL_strz00_2270))
									{	/* Ieee/string.scm 1560 */
										BgL_stringz00_4269 = BgL_strz00_2270;
									}
								else
									{
										obj_t BgL_auxz00_9173;

										BgL_auxz00_9173 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2658z00zz__r4_strings_6_7z00, BINT(62453L),
											BGl_string2780z00zz__r4_strings_6_7z00,
											BGl_string2660z00zz__r4_strings_6_7z00, BgL_strz00_2270);
										FAILURE(BgL_auxz00_9173, BFALSE, BFALSE);
									}
								BgL_endz00_2274 = STRING_LENGTH(BgL_stringz00_4269);
							}
							{	/* Ieee/string.scm 1559 */

								{	/* Ieee/string.scm 1559 */
									obj_t BgL_auxz00_9178;

									if (STRINGP(BgL_strz00_2270))
										{	/* Ieee/string.scm 1559 */
											BgL_auxz00_9178 = BgL_strz00_2270;
										}
									else
										{
											obj_t BgL_auxz00_9181;

											BgL_auxz00_9181 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(62363L),
												BGl_string2780z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00,
												BgL_strz00_2270);
											FAILURE(BgL_auxz00_9181, BFALSE, BFALSE);
										}
									return
										BGl_stringzd2hexzd2externz00zz__r4_strings_6_7z00
										(BgL_auxz00_9178, (int) (0L), BgL_endz00_2274);
						}}} break;
					case 2L:

						{	/* Ieee/string.scm 1559 */
							obj_t BgL_startz00_2275;

							BgL_startz00_2275 = VECTOR_REF(BgL_opt1205z00_274, 1L);
							{	/* Ieee/string.scm 1560 */
								long BgL_endz00_2276;

								{	/* Ieee/string.scm 1560 */
									obj_t BgL_stringz00_4270;

									if (STRINGP(BgL_strz00_2270))
										{	/* Ieee/string.scm 1560 */
											BgL_stringz00_4270 = BgL_strz00_2270;
										}
									else
										{
											obj_t BgL_auxz00_9190;

											BgL_auxz00_9190 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2658z00zz__r4_strings_6_7z00, BINT(62453L),
												BGl_string2780z00zz__r4_strings_6_7z00,
												BGl_string2660z00zz__r4_strings_6_7z00,
												BgL_strz00_2270);
											FAILURE(BgL_auxz00_9190, BFALSE, BFALSE);
										}
									BgL_endz00_2276 = STRING_LENGTH(BgL_stringz00_4270);
								}
								{	/* Ieee/string.scm 1559 */

									{	/* Ieee/string.scm 1559 */
										int BgL_auxz00_9202;
										obj_t BgL_auxz00_9195;

										{	/* Ieee/string.scm 1559 */
											obj_t BgL_tmpz00_9203;

											if (INTEGERP(BgL_startz00_2275))
												{	/* Ieee/string.scm 1559 */
													BgL_tmpz00_9203 = BgL_startz00_2275;
												}
											else
												{
													obj_t BgL_auxz00_9206;

													BgL_auxz00_9206 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2658z00zz__r4_strings_6_7z00,
														BINT(62363L),
														BGl_string2780z00zz__r4_strings_6_7z00,
														BGl_string2662z00zz__r4_strings_6_7z00,
														BgL_startz00_2275);
													FAILURE(BgL_auxz00_9206, BFALSE, BFALSE);
												}
											BgL_auxz00_9202 = CINT(BgL_tmpz00_9203);
										}
										if (STRINGP(BgL_strz00_2270))
											{	/* Ieee/string.scm 1559 */
												BgL_auxz00_9195 = BgL_strz00_2270;
											}
										else
											{
												obj_t BgL_auxz00_9198;

												BgL_auxz00_9198 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2658z00zz__r4_strings_6_7z00, BINT(62363L),
													BGl_string2780z00zz__r4_strings_6_7z00,
													BGl_string2660z00zz__r4_strings_6_7z00,
													BgL_strz00_2270);
												FAILURE(BgL_auxz00_9198, BFALSE, BFALSE);
											}
										return
											BGl_stringzd2hexzd2externz00zz__r4_strings_6_7z00
											(BgL_auxz00_9195, BgL_auxz00_9202, BgL_endz00_2276);
									}
								}
							}
						}
						break;
					case 3L:

						{	/* Ieee/string.scm 1559 */
							obj_t BgL_startz00_2277;

							BgL_startz00_2277 = VECTOR_REF(BgL_opt1205z00_274, 1L);
							{	/* Ieee/string.scm 1559 */
								obj_t BgL_endz00_2278;

								BgL_endz00_2278 = VECTOR_REF(BgL_opt1205z00_274, 2L);
								{	/* Ieee/string.scm 1559 */

									{	/* Ieee/string.scm 1559 */
										long BgL_auxz00_9230;
										int BgL_auxz00_9221;
										obj_t BgL_auxz00_9214;

										{	/* Ieee/string.scm 1559 */
											obj_t BgL_tmpz00_9231;

											if (INTEGERP(BgL_endz00_2278))
												{	/* Ieee/string.scm 1559 */
													BgL_tmpz00_9231 = BgL_endz00_2278;
												}
											else
												{
													obj_t BgL_auxz00_9234;

													BgL_auxz00_9234 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2658z00zz__r4_strings_6_7z00,
														BINT(62363L),
														BGl_string2780z00zz__r4_strings_6_7z00,
														BGl_string2662z00zz__r4_strings_6_7z00,
														BgL_endz00_2278);
													FAILURE(BgL_auxz00_9234, BFALSE, BFALSE);
												}
											BgL_auxz00_9230 = (long) CINT(BgL_tmpz00_9231);
										}
										{	/* Ieee/string.scm 1559 */
											obj_t BgL_tmpz00_9222;

											if (INTEGERP(BgL_startz00_2277))
												{	/* Ieee/string.scm 1559 */
													BgL_tmpz00_9222 = BgL_startz00_2277;
												}
											else
												{
													obj_t BgL_auxz00_9225;

													BgL_auxz00_9225 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2658z00zz__r4_strings_6_7z00,
														BINT(62363L),
														BGl_string2780z00zz__r4_strings_6_7z00,
														BGl_string2662z00zz__r4_strings_6_7z00,
														BgL_startz00_2277);
													FAILURE(BgL_auxz00_9225, BFALSE, BFALSE);
												}
											BgL_auxz00_9221 = CINT(BgL_tmpz00_9222);
										}
										if (STRINGP(BgL_strz00_2270))
											{	/* Ieee/string.scm 1559 */
												BgL_auxz00_9214 = BgL_strz00_2270;
											}
										else
											{
												obj_t BgL_auxz00_9217;

												BgL_auxz00_9217 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2658z00zz__r4_strings_6_7z00, BINT(62363L),
													BGl_string2780z00zz__r4_strings_6_7z00,
													BGl_string2660z00zz__r4_strings_6_7z00,
													BgL_strz00_2270);
												FAILURE(BgL_auxz00_9217, BFALSE, BFALSE);
											}
										return
											BGl_stringzd2hexzd2externz00zz__r4_strings_6_7z00
											(BgL_auxz00_9214, BgL_auxz00_9221, BgL_auxz00_9230);
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* string-hex-extern */
	BGL_EXPORTED_DEF obj_t BGl_stringzd2hexzd2externz00zz__r4_strings_6_7z00(obj_t
		BgL_strz00_271, int BgL_startz00_272, long BgL_endz00_273)
	{
		{	/* Ieee/string.scm 1559 */
			{	/* Ieee/string.scm 1564 */
				long BgL_lenz00_2280;

				BgL_lenz00_2280 = STRING_LENGTH(BgL_strz00_271);
				{	/* Ieee/string.scm 1566 */
					bool_t BgL_test3570z00_9243;

					if (((long) (BgL_startz00_272) < 0L))
						{	/* Ieee/string.scm 1566 */
							BgL_test3570z00_9243 = ((bool_t) 1);
						}
					else
						{	/* Ieee/string.scm 1566 */
							BgL_test3570z00_9243 =
								((long) (BgL_startz00_272) > BgL_lenz00_2280);
						}
					if (BgL_test3570z00_9243)
						{	/* Ieee/string.scm 1568 */
							obj_t BgL_arg1930z00_2283;
							obj_t BgL_arg1931z00_2284;

							{	/* Ieee/string.scm 1568 */
								obj_t BgL_arg1932z00_2285;

								{	/* Ieee/fixnum.scm 1002 */

									BgL_arg1932z00_2285 =
										BGl_fixnumzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
										(long) (BgL_startz00_272), 10L);
								}
								BgL_arg1930z00_2283 =
									string_append(BGl_string2685z00zz__r4_strings_6_7z00,
									BgL_arg1932z00_2285);
							}
							{	/* Ieee/string.scm 1569 */
								obj_t BgL_list1934z00_2289;

								{	/* Ieee/string.scm 1569 */
									obj_t BgL_arg1935z00_2290;

									BgL_arg1935z00_2290 = MAKE_YOUNG_PAIR(BgL_strz00_271, BNIL);
									BgL_list1934z00_2289 =
										MAKE_YOUNG_PAIR(BINT(BgL_lenz00_2280), BgL_arg1935z00_2290);
								}
								BgL_arg1931z00_2284 = BgL_list1934z00_2289;
							}
							return
								BGl_errorz00zz__errorz00(BGl_string2781z00zz__r4_strings_6_7z00,
								BgL_arg1930z00_2283, BgL_arg1931z00_2284);
						}
					else
						{	/* Ieee/string.scm 1570 */
							bool_t BgL_test3572z00_9256;

							if ((BgL_endz00_273 < (long) (BgL_startz00_272)))
								{	/* Ieee/string.scm 1570 */
									BgL_test3572z00_9256 = ((bool_t) 1);
								}
							else
								{	/* Ieee/string.scm 1570 */
									BgL_test3572z00_9256 = (BgL_endz00_273 > BgL_lenz00_2280);
								}
							if (BgL_test3572z00_9256)
								{	/* Ieee/string.scm 1572 */
									obj_t BgL_arg1938z00_2293;
									obj_t BgL_arg1939z00_2294;

									{	/* Ieee/string.scm 1572 */
										obj_t BgL_arg1940z00_2295;

										{	/* Ieee/fixnum.scm 1002 */

											BgL_arg1940z00_2295 =
												BGl_fixnumzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
												(BgL_endz00_273, 10L);
										}
										BgL_arg1938z00_2293 =
											string_append(BGl_string2687z00zz__r4_strings_6_7z00,
											BgL_arg1940z00_2295);
									}
									{	/* Ieee/string.scm 1573 */
										obj_t BgL_list1942z00_2299;

										{	/* Ieee/string.scm 1573 */
											obj_t BgL_arg1943z00_2300;

											BgL_arg1943z00_2300 =
												MAKE_YOUNG_PAIR(BgL_strz00_271, BNIL);
											BgL_list1942z00_2299 =
												MAKE_YOUNG_PAIR(BINT(BgL_lenz00_2280),
												BgL_arg1943z00_2300);
										}
										BgL_arg1939z00_2294 = BgL_list1942z00_2299;
									}
									return
										BGl_errorz00zz__errorz00
										(BGl_string2781z00zz__r4_strings_6_7z00,
										BgL_arg1938z00_2293, BgL_arg1939z00_2294);
								}
							else
								{	/* Ieee/string.scm 1575 */
									obj_t BgL_resz00_2301;

									{	/* Ieee/string.scm 1575 */
										long BgL_arg1952z00_2317;

										BgL_arg1952z00_2317 =
											(2L * (BgL_endz00_273 - (long) (BgL_startz00_272)));
										{	/* Ieee/string.scm 172 */

											BgL_resz00_2301 =
												make_string(BgL_arg1952z00_2317, ((unsigned char) ' '));
									}}
									{
										int BgL_iz00_2303;
										long BgL_jz00_2304;

										BgL_iz00_2303 = BgL_startz00_272;
										BgL_jz00_2304 = 0L;
									BgL_zc3z04anonymousza31944ze3z87_2305:
										if (((long) (BgL_iz00_2303) == BgL_endz00_273))
											{	/* Ieee/string.scm 1578 */
												return BgL_resz00_2301;
											}
										else
											{	/* Ieee/string.scm 1580 */
												long BgL_nz00_2307;

												BgL_nz00_2307 =
													(STRING_REF(BgL_strz00_271, (long) (BgL_iz00_2303)));
												{	/* Ieee/string.scm 1580 */
													long BgL_d0z00_2308;

													{	/* Ieee/string.scm 1581 */
														long BgL_n1z00_4294;
														long BgL_n2z00_4295;

														BgL_n1z00_4294 = BgL_nz00_2307;
														BgL_n2z00_4295 = 16L;
														{	/* Ieee/string.scm 1581 */
															bool_t BgL_test3575z00_9277;

															{	/* Ieee/string.scm 1581 */
																long BgL_arg2065z00_4297;

																BgL_arg2065z00_4297 =
																	(((BgL_n1z00_4294) | (BgL_n2z00_4295)) &
																	-2147483648);
																BgL_test3575z00_9277 =
																	(BgL_arg2065z00_4297 == 0L);
															}
															if (BgL_test3575z00_9277)
																{	/* Ieee/string.scm 1581 */
																	int32_t BgL_arg2062z00_4298;

																	{	/* Ieee/string.scm 1581 */
																		int32_t BgL_arg2063z00_4299;
																		int32_t BgL_arg2064z00_4300;

																		BgL_arg2063z00_4299 =
																			(int32_t) (BgL_n1z00_4294);
																		BgL_arg2064z00_4300 =
																			(int32_t) (BgL_n2z00_4295);
																		BgL_arg2062z00_4298 =
																			(BgL_arg2063z00_4299 %
																			BgL_arg2064z00_4300);
																	}
																	{	/* Ieee/string.scm 1581 */
																		long BgL_arg2171z00_4305;

																		BgL_arg2171z00_4305 =
																			(long) (BgL_arg2062z00_4298);
																		BgL_d0z00_2308 =
																			(long) (BgL_arg2171z00_4305);
																}}
															else
																{	/* Ieee/string.scm 1581 */
																	BgL_d0z00_2308 =
																		(BgL_n1z00_4294 % BgL_n2z00_4295);
																}
														}
													}
													{	/* Ieee/string.scm 1581 */
														long BgL_d1z00_2309;

														BgL_d1z00_2309 = (BgL_nz00_2307 / 16L);
														{	/* Ieee/string.scm 1582 */

															{	/* Ieee/string.scm 337 */
																unsigned char BgL_tmpz00_9287;

																BgL_tmpz00_9287 =
																	STRING_REF
																	(BGl_string2782z00zz__r4_strings_6_7z00,
																	BgL_d1z00_2309);
																STRING_SET(BgL_resz00_2301, BgL_jz00_2304,
																	BgL_tmpz00_9287);
															}
															{	/* Ieee/string.scm 337 */
																unsigned char BgL_auxz00_9292;
																long BgL_tmpz00_9290;

																BgL_auxz00_9292 =
																	STRING_REF
																	(BGl_string2782z00zz__r4_strings_6_7z00,
																	BgL_d0z00_2308);
																BgL_tmpz00_9290 = (BgL_jz00_2304 + 1L);
																STRING_SET(BgL_resz00_2301, BgL_tmpz00_9290,
																	BgL_auxz00_9292);
															}
															{	/* Ieee/string.scm 1585 */
																long BgL_arg1949z00_2313;
																long BgL_arg1950z00_2314;

																BgL_arg1949z00_2313 =
																	((long) (BgL_iz00_2303) + 1L);
																BgL_arg1950z00_2314 = (BgL_jz00_2304 + 2L);
																{
																	long BgL_jz00_9300;
																	int BgL_iz00_9298;

																	BgL_iz00_9298 = (int) (BgL_arg1949z00_2313);
																	BgL_jz00_9300 = BgL_arg1950z00_2314;
																	BgL_jz00_2304 = BgL_jz00_9300;
																	BgL_iz00_2303 = BgL_iz00_9298;
																	goto BgL_zc3z04anonymousza31944ze3z87_2305;
																}
															}
														}
													}
												}
											}
									}
								}
						}
				}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__r4_strings_6_7z00(void)
	{
		{	/* Ieee/string.scm 18 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string2783z00zz__r4_strings_6_7z00));
			return
				BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string2783z00zz__r4_strings_6_7z00));
		}

	}

#ifdef __cplusplus
}
#endif
