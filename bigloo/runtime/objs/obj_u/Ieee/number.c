/*===========================================================================*/
/*   (Ieee/number.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Ieee/number.scm -indent -o objs/obj_u/Ieee/number.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___R4_NUMBERS_6_5_TYPE_DEFINITIONS
#define BGL___R4_NUMBERS_6_5_TYPE_DEFINITIONS
#endif													// BGL___R4_NUMBERS_6_5_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL int64_t
		BGl_bignumzd2ze3int64z31zz__r4_numbers_6_5z00(obj_t);
	static obj_t BGl_z62logz62zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL double BGl_sinz00zz__r4_numbers_6_5z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL double BGl_log2z00zz__r4_numbers_6_5z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_2minz00zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62flonumzd2ze3elongz53zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62llongzd2ze3flonumz53zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_flonumzd2ze3bignumz31zz__r4_numbers_6_5z00(double);
	BGL_EXPORTED_DECL double BGl_acosz00zz__r4_numbers_6_5z00(obj_t);
	static obj_t BGl_z62numberzd2ze3flonumz53zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62uint64zd2ze3flonumz53zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL int64_t
		BGl_flonumzd2ze3int64z31zz__r4_numbers_6_5z00(double);
	static obj_t BGl_requirezd2initializa7ationz75zz__r4_numbers_6_5z00 = BUNSPEC;
	static obj_t BGl_z62int64zd2ze3flonumz53zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_elongzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(long,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_zc3zd3z10zz__r4_numbers_6_5z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rationalzf3zf3zz__r4_numbers_6_5z00(obj_t);
	extern long BGl_exptfxz00zz__r4_numbers_6_5_fixnumz00(long, long);
	extern obj_t BGl_bignumzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(obj_t,
		long);
	static obj_t BGl_z62tanz62zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_inexactzf3zf3zz__r4_numbers_6_5z00(obj_t);
	BGL_EXPORTED_DECL double
		BGl_int32zd2ze3flonumz31zz__r4_numbers_6_5z00(int32_t);
	static obj_t BGl_z62flonumzd2ze3int32z53zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62positivezf3z91zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t bgl_bignum_neg(obj_t);
	static obj_t BGl_z62fixnumzd2ze3flonumz53zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62flonumzd2ze3fixnumz53zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_z62bignumzd2ze3flonumz53zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_truncatez00zz__r4_numbers_6_5z00(obj_t);
	static obj_t BGl_z62atanz62zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL double BGl_expz00zz__r4_numbers_6_5z00(obj_t);
	static obj_t BGl_z62roundz62zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_ze3zd3z30zz__r4_numbers_6_5z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3zd3z72zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_minz00zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t bgl_bignum_div(obj_t, obj_t);
	static obj_t BGl_z62bignumzd2ze3int64z53zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_numberzf3zf3zz__r4_numbers_6_5z00(obj_t);
	static obj_t BGl_z62maxz62zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zz__r4_numbers_6_5z00(void);
	static obj_t BGl_z62numberzf3z91zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(long,
		long);
	static obj_t BGl_z622zc3zd3z72zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_absz00zz__r4_numbers_6_5z00(obj_t);
	extern obj_t
		BGl_stringzd2ze3integerzd2objze3zz__r4_numbers_6_5_fixnumz00(obj_t, long);
	BGL_EXPORTED_DECL obj_t BGl_ceilingz00zz__r4_numbers_6_5z00(obj_t);
	static obj_t BGl_z62ceilingz62zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_int64zd2ze3bignumz31zz__r4_numbers_6_5z00(int64_t);
	BGL_EXPORTED_DECL double BGl_atanz00zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	static obj_t BGl_z62sqrtz62zz__r4_numbers_6_5z00(obj_t, obj_t);
	static double BGl_za2minintflza2z00zz__r4_numbers_6_5z00;
	static obj_t BGl_z62flonumzd2ze3int64z53zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__r4_numbers_6_5z00(void);
	extern double BGl_roundflz00zz__r4_numbers_6_5_flonumz00(double);
	static obj_t BGl_z62complexzf3z91zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t
		BGl_llongzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(BGL_LONGLONG_T, obj_t);
	static obj_t BGl_z62ze3zd3z52zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(obj_t);
	BGL_EXPORTED_DECL double BGl_cosz00zz__r4_numbers_6_5z00(obj_t);
	static obj_t BGl_z62uint32zd2ze3flonumz53zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62flonumzd2ze3uint32z53zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__r4_numbers_6_5z00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__r4_numbers_6_5z00(void);
	static obj_t BGl_z62int32zd2ze3flonumz53zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_positivezf3zf3zz__r4_numbers_6_5z00(obj_t);
	static obj_t BGl_z622ze3zd3z52zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_2za2za2zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_2zb2zb2zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z622maxz62zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_2zd2zd2zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_2zf2zf2zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern double bgl_bignum_to_flonum(obj_t);
	BGL_EXPORTED_DECL double
		BGl_numberzd2ze3flonumz31zz__r4_numbers_6_5z00(obj_t);
	BGL_EXPORTED_DECL double
		BGl_uint64zd2ze3flonumz31zz__r4_numbers_6_5z00(uint64_t);
	static obj_t BGl_z62exptz62zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
	extern obj_t bgl_bignum_mul(obj_t, obj_t);
	BGL_EXPORTED_DECL double BGl_sqrtz00zz__r4_numbers_6_5z00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_2zc3zc3zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_2zd3zd3zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL double BGl_logz00zz__r4_numbers_6_5z00(obj_t);
	static obj_t BGl_z62sinz62zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62inexactzf3z91zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL double BGl_elongzd2ze3flonumz31zz__r4_numbers_6_5z00(long);
	static obj_t BGl_z62exactzd2ze3inexactz53zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL double BGl_fixnumzd2ze3flonumz31zz__r4_numbers_6_5z00(long);
	BGL_EXPORTED_DECL long BGl_flonumzd2ze3fixnumz31zz__r4_numbers_6_5z00(double);
	BGL_EXPORTED_DECL obj_t BGl_2maxz00zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL BGL_LONGLONG_T
		BGl_flonumzd2ze3llongz31zz__r4_numbers_6_5z00(double);
	static obj_t BGl_z62exactzf3z91zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62bignumzd2ze3uint64z53zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62uint64zd2ze3bignumz53zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL double
		BGl_bignumzd2ze3flonumz31zz__r4_numbers_6_5z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_exptz00zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z622za2zc0zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z622zb2zd0zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z622zd2zb0zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z622zf2z90zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62int64zd2ze3bignumz53zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t bgl_bignum_add(obj_t, obj_t);
	static obj_t BGl_z62truncatez62zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62flonumzd2ze3uint64z53zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62asinz62zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62inexactzd2ze3exactz53zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z622zc3za1zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z622zd3zb1zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z622ze3z81zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
	static double BGl_za2maxintflza2z00zz__r4_numbers_6_5z00;
	extern obj_t bstring_to_symbol(obj_t);
	BGL_EXPORTED_DECL double BGl_tanz00zz__r4_numbers_6_5z00(obj_t);
	BGL_EXPORTED_DECL double BGl_log10z00zz__r4_numbers_6_5z00(obj_t);
	extern int64_t bgl_bignum_to_int64(obj_t);
	extern obj_t bgl_long_to_bignum(long);
	static obj_t BGl__stringzd2ze3numberz31zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl__numberzd2ze3stringz31zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62za2zc0zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62zb2zd0zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62zd2zb0zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zf2z90zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_za7erozf3z54zz__r4_numbers_6_5z00(obj_t);
	static obj_t BGl_z62zc3za1zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_floorz00zz__r4_numbers_6_5z00(obj_t);
	extern obj_t BGl_bigloozd2typezd2errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zd3zb1zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62ze3z81zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62flonumzd2ze3bignumz53zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL double BGl_asinz00zz__r4_numbers_6_5z00(obj_t);
	static obj_t BGl_z62expz62zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62negativezf3z91zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t bgl_bignum_expt(obj_t, obj_t);
	BGL_EXPORTED_DECL double
		BGl_uint32zd2ze3flonumz31zz__r4_numbers_6_5z00(uint32_t);
	BGL_EXPORTED_DECL uint32_t
		BGl_flonumzd2ze3uint32z31zz__r4_numbers_6_5z00(double);
	static bool_t BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_maxz00zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62minz62zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_2zc3zd3z10zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL long BGl_flonumzd2ze3elongz31zz__r4_numbers_6_5z00(double);
	static obj_t BGl_z62elongzd2ze3flonumz53zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL double
		BGl_llongzd2ze3flonumz31zz__r4_numbers_6_5z00(BGL_LONGLONG_T);
	extern uint64_t bgl_bignum_to_uint64(obj_t);
	extern obj_t bgl_uint64_to_bignum(uint64_t);
	static obj_t BGl_z62absz62zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62flonumzd2ze3llongz53zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t bgl_bignum_sub(obj_t, obj_t);
	BGL_EXPORTED_DECL double
		BGl_int64zd2ze3flonumz31zz__r4_numbers_6_5z00(int64_t);
	extern int bgl_bignum_cmp(obj_t, obj_t);
	extern obj_t bgl_bignum_abs(obj_t);
	static obj_t BGl_z62rationalzf3z91zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t bgl_real_to_string(double);
	extern obj_t bgl_long_to_bignum(long);
	static obj_t BGl_z62log10z62zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_za2za2zz__r4_numbers_6_5z00(obj_t);
	static obj_t BGl_z62cosz62zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_zb2zb2zz__r4_numbers_6_5z00(obj_t);
	BGL_EXPORTED_DECL obj_t bgl_exact_to_inexact(obj_t);
	BGL_EXPORTED_DECL int32_t
		BGl_flonumzd2ze3int32z31zz__r4_numbers_6_5z00(double);
	BGL_EXPORTED_DECL obj_t BGl_zd2zd2zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_2ze3zd3z30zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t bgl_llong_to_bignum(BGL_LONGLONG_T);
	BGL_EXPORTED_DECL obj_t BGl_zf2zf2zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_exactzf3zf3zz__r4_numbers_6_5z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_uint64zd2ze3bignumz31zz__r4_numbers_6_5z00(uint64_t);
	BGL_EXPORTED_DECL uint64_t
		BGl_bignumzd2ze3uint64z31zz__r4_numbers_6_5z00(obj_t);
	static obj_t BGl_z62log2z62zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z622minz62zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_zc3zc3zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_zd3zd3zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
	extern obj_t bgl_flonum_to_bignum(double);
	static obj_t BGl_symbol3363z00zz__r4_numbers_6_5z00 = BUNSPEC;
	static obj_t BGl_z62za7erozf3z36zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL uint64_t
		BGl_flonumzd2ze3uint64z31zz__r4_numbers_6_5z00(double);
	extern obj_t bgl_int64_to_bignum(uint64_t);
	static obj_t BGl_z62floorz62zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_roundz00zz__r4_numbers_6_5z00(obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62acosz62zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t bgl_inexact_to_exact(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_negativezf3zf3zz__r4_numbers_6_5z00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_complexzf3zf3zz__r4_numbers_6_5z00(obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_int64zd2ze3flonumzd2envze3zz__r4_numbers_6_5z00,
		BgL_bgl_za762int64za7d2za7e3fl3377za7,
		BGl_z62int64zd2ze3flonumz53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_int32zd2ze3flonumzd2envze3zz__r4_numbers_6_5z00,
		BgL_bgl_za762int32za7d2za7e3fl3378za7,
		BGl_z62int32zd2ze3flonumz53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_zd2zd2envz00zz__r4_numbers_6_5z00,
		BgL_bgl_za762za7d2za7b0za7za7__r4_3379za7, va_generic_entry,
		BGl_z62zd2zb0zz__r4_numbers_6_5z00, BUNSPEC, -2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_2ze3zd3zd2envze2zz__r4_numbers_6_5z00,
		BgL_bgl_za7622za7e3za7d3za752za7za7_3380z00,
		BGl_z622ze3zd3z52zz__r4_numbers_6_5z00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_zd3zd2envz01zz__r4_numbers_6_5z00,
		BgL_bgl_za762za7d3za7b1za7za7__r4_3381za7, va_generic_entry,
		BGl_z62zd3zb1zz__r4_numbers_6_5z00, BUNSPEC, -3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_2zd2zd2envz00zz__r4_numbers_6_5z00,
		BgL_bgl_za7622za7d2za7b0za7za7__r43382za7,
		BGl_z622zd2zb0zz__r4_numbers_6_5z00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_floorzd2envzd2zz__r4_numbers_6_5z00,
		BgL_bgl_za762floorza762za7za7__r3383z00,
		BGl_z62floorz62zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_2zd3zd2envz01zz__r4_numbers_6_5z00,
		BgL_bgl_za7622za7d3za7b1za7za7__r43384za7,
		BGl_z622zd3zb1zz__r4_numbers_6_5z00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_minzd2envzd2zz__r4_numbers_6_5z00,
		BgL_bgl_za762minza762za7za7__r4_3385z00, va_generic_entry,
		BGl_z62minz62zz__r4_numbers_6_5z00, BUNSPEC, -2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_maxzd2envzd2zz__r4_numbers_6_5z00,
		BgL_bgl_za762maxza762za7za7__r4_3386z00, va_generic_entry,
		BGl_z62maxz62zz__r4_numbers_6_5z00, BUNSPEC, -2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_numberzf3zd2envz21zz__r4_numbers_6_5z00,
		BgL_bgl_za762numberza7f3za791za73387z00,
		BGl_z62numberzf3z91zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_fixnumzd2ze3flonumzd2envze3zz__r4_numbers_6_5z00,
		BgL_bgl_za762fixnumza7d2za7e3f3388za7,
		BGl_z62fixnumzd2ze3flonumz53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_flonumzd2ze3fixnumzd2envze3zz__r4_numbers_6_5z00,
		BgL_bgl_za762flonumza7d2za7e3f3389za7,
		BGl_z62flonumzd2ze3fixnumz53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_atanzd2envzd2zz__r4_numbers_6_5z00,
		BgL_bgl_za762atanza762za7za7__r43390z00, va_generic_entry,
		BGl_z62atanz62zz__r4_numbers_6_5z00, BUNSPEC, -2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_flonumzd2ze3int64zd2envze3zz__r4_numbers_6_5z00,
		BgL_bgl_za762flonumza7d2za7e3i3391za7,
		BGl_z62flonumzd2ze3int64z53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_zc3zd3zd2envzc2zz__r4_numbers_6_5z00,
		BgL_bgl_za762za7c3za7d3za772za7za7__3392z00, va_generic_entry,
		BGl_z62zc3zd3z72zz__r4_numbers_6_5z00, BUNSPEC, -3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rationalzf3zd2envz21zz__r4_numbers_6_5z00,
		BgL_bgl_za762rationalza7f3za793393za7,
		BGl_z62rationalzf3z91zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_tanzd2envzd2zz__r4_numbers_6_5z00,
		BgL_bgl_za762tanza762za7za7__r4_3394z00, BGl_z62tanz62zz__r4_numbers_6_5z00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_log10zd2envzd2zz__r4_numbers_6_5z00,
		BgL_bgl_za762log10za762za7za7__r3395z00,
		BGl_z62log10z62zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3300z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3300za700za7za7_3396za7, "&fixnum->flonum", 15);
	      DEFINE_STRING(BGl_string3301z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3301za700za7za7_3397za7, "bint", 4);
	      DEFINE_STRING(BGl_string3302z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3302za700za7za7_3398za7, "&flonum->elong", 14);
	      DEFINE_STRING(BGl_string3303z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3303za700za7za7_3399za7, "&elong->flonum", 14);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_za2zd2envz70zz__r4_numbers_6_5z00,
		BgL_bgl_za762za7a2za7c0za7za7__r4_3400za7, va_generic_entry,
		BGl_z62za2zc0zz__r4_numbers_6_5z00, BUNSPEC, -1);
	      DEFINE_STRING(BGl_string3304z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3304za700za7za7_3401za7, "belong", 6);
	      DEFINE_STRING(BGl_string3305z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3305za700za7za7_3402za7, "&flonum->llong", 14);
	      DEFINE_STRING(BGl_string3306z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3306za700za7za7_3403za7, "&llong->flonum", 14);
	      DEFINE_STRING(BGl_string3307z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3307za700za7za7_3404za7, "bllong", 6);
	      DEFINE_STRING(BGl_string3308z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3308za700za7za7_3405za7, "&flonum->bignum", 15);
	      DEFINE_STRING(BGl_string3309z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3309za700za7za7_3406za7, "&bignum->flonum", 15);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_2za2zd2envz70zz__r4_numbers_6_5z00,
		BgL_bgl_za7622za7a2za7c0za7za7__r43407za7,
		BGl_z622za2zc0zz__r4_numbers_6_5z00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ze3zd2envz31zz__r4_numbers_6_5z00,
		BgL_bgl_za762za7e3za781za7za7__r4_3408za7, va_generic_entry,
		BGl_z62ze3z81zz__r4_numbers_6_5z00, BUNSPEC, -3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_za7erozf3zd2envz86zz__r4_numbers_6_5z00,
		BgL_bgl_za762za7a7eroza7f3za736za73409za7,
		BGl_z62za7erozf3z36zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_flonumzd2ze3uint64zd2envze3zz__r4_numbers_6_5z00,
		BgL_bgl_za762flonumza7d2za7e3u3410za7,
		BGl_z62flonumzd2ze3uint64z53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3310z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3310za700za7za7_3411za7, "bignum", 6);
	      DEFINE_STRING(BGl_string3311z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3311za700za7za7_3412za7, "&int64->bignum", 14);
	      DEFINE_STRING(BGl_string3312z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3312za700za7za7_3413za7, "bint64", 6);
	      DEFINE_STRING(BGl_string3313z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3313za700za7za7_3414za7, "&bignum->int64", 14);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_asinzd2envzd2zz__r4_numbers_6_5z00,
		BgL_bgl_za762asinza762za7za7__r43415z00,
		BGl_z62asinz62zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3314z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3314za700za7za7_3416za7, "&uint64->bignum", 15);
	      DEFINE_STRING(BGl_string3315z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3315za700za7za7_3417za7, "buint64", 7);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_expzd2envzd2zz__r4_numbers_6_5z00,
		BgL_bgl_za762expza762za7za7__r4_3418z00, BGl_z62expz62zz__r4_numbers_6_5z00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3316z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3316za700za7za7_3419za7, "&bignum->uint64", 15);
	      DEFINE_STRING(BGl_string3317z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3317za700za7za7_3420za7, "&flonum->int32", 14);
	      DEFINE_STRING(BGl_string3318z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3318za700za7za7_3421za7, "&int32->flonum", 14);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_2ze3zd2envz31zz__r4_numbers_6_5z00,
		BgL_bgl_za7622za7e3za781za7za7__r43422za7,
		BGl_z622ze3z81zz__r4_numbers_6_5z00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string3319z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3319za700za7za7_3423za7, "bint32", 6);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_negativezf3zd2envz21zz__r4_numbers_6_5z00,
		BgL_bgl_za762negativeza7f3za793424za7,
		BGl_z62negativezf3z91zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bignumzd2ze3int64zd2envze3zz__r4_numbers_6_5z00,
		BgL_bgl_za762bignumza7d2za7e3i3425za7,
		BGl_z62bignumzd2ze3int64z53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3320z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3320za700za7za7_3426za7, "&flonum->uint32", 15);
	      DEFINE_STRING(BGl_string3321z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3321za700za7za7_3427za7, "&uint32->flonum", 15);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sinzd2envzd2zz__r4_numbers_6_5z00,
		BgL_bgl_za762sinza762za7za7__r4_3428z00, BGl_z62sinz62zz__r4_numbers_6_5z00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3322z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3322za700za7za7_3429za7, "buint32", 7);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_2zc3zd3zd2envzc2zz__r4_numbers_6_5z00,
		BgL_bgl_za7622za7c3za7d3za772za7za7_3430z00,
		BGl_z622zc3zd3z72zz__r4_numbers_6_5z00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string3323z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3323za700za7za7_3431za7, "&flonum->int64", 14);
	      DEFINE_STRING(BGl_string3324z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3324za700za7za7_3432za7, "&int64->flonum", 14);
	      DEFINE_STRING(BGl_string3325z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3325za700za7za7_3433za7, "&flonum->uint64", 15);
	      DEFINE_STRING(BGl_string3326z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3326za700za7za7_3434za7, "&uint64->flonum", 15);
	      DEFINE_STRING(BGl_string3327z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3327za700za7za7_3435za7, "=", 1);
	      DEFINE_STRING(BGl_string3328z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3328za700za7za7_3436za7, "not a number", 12);
	      DEFINE_STRING(BGl_string3329z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3329za700za7za7_3437za7, "not a number5", 13);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_2minzd2envzd2zz__r4_numbers_6_5z00,
		BgL_bgl_za7622minza762za7za7__r43438z00,
		BGl_z622minz62zz__r4_numbers_6_5z00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_2maxzd2envzd2zz__r4_numbers_6_5z00,
		BgL_bgl_za7622maxza762za7za7__r43439z00,
		BGl_z622maxz62zz__r4_numbers_6_5z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_flonumzd2ze3llongzd2envze3zz__r4_numbers_6_5z00,
		BgL_bgl_za762flonumza7d2za7e3l3440za7,
		BGl_z62flonumzd2ze3llongz53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_flonumzd2ze3bignumzd2envze3zz__r4_numbers_6_5z00,
		BgL_bgl_za762flonumza7d2za7e3b3441za7,
		BGl_z62flonumzd2ze3bignumz53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_acoszd2envzd2zz__r4_numbers_6_5z00,
		BgL_bgl_za762acosza762za7za7__r43442z00,
		BGl_z62acosz62zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3330z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3330za700za7za7_3443za7, "<", 1);
	      DEFINE_STRING(BGl_string3331z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3331za700za7za7_3444za7, ">", 1);
	      DEFINE_STRING(BGl_string3332z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3332za700za7za7_3445za7, "<=", 2);
	      DEFINE_STRING(BGl_string3333z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3333za700za7za7_3446za7, ">=", 2);
	      DEFINE_STRING(BGl_string3334z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3334za700za7za7_3447za7, "zero", 4);
	      DEFINE_STRING(BGl_string3335z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3335za700za7za7_3448za7, "positive", 8);
	      DEFINE_STRING(BGl_string3336z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3336za700za7za7_3449za7, "negative", 8);
	      DEFINE_STRING(BGl_string3337z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3337za700za7za7_3450za7, "2max", 4);
	      DEFINE_STRING(BGl_string3338z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3338za700za7za7_3451za7, "2min", 4);
	      DEFINE_STRING(BGl_string3339z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3339za700za7za7_3452za7, "+", 1);
	      DEFINE_REAL(BGl_real3359z00zz__r4_numbers_6_5z00,
		BgL_bgl_real3359za700za7za7__r3453za7, 1.0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_coszd2envzd2zz__r4_numbers_6_5z00,
		BgL_bgl_za762cosza762za7za7__r4_3454z00, BGl_z62cosz62zz__r4_numbers_6_5z00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3340z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3340za700za7za7_3455za7, "*", 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_zb2zd2envz60zz__r4_numbers_6_5z00,
		BgL_bgl_za762za7b2za7d0za7za7__r4_3456za7, va_generic_entry,
		BGl_z62zb2zd0zz__r4_numbers_6_5z00, BUNSPEC, -1);
	      DEFINE_STRING(BGl_string3341z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3341za700za7za7_3457za7, "-", 1);
	      DEFINE_STRING(BGl_string3342z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3342za700za7za7_3458za7, "/", 1);
	      DEFINE_STRING(BGl_string3343z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3343za700za7za7_3459za7, "abs", 3);
	      DEFINE_STRING(BGl_string3344z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3344za700za7za7_3460za7, "floor", 5);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_zf2zd2envz20zz__r4_numbers_6_5z00,
		BgL_bgl_za762za7f2za790za7za7__r4_3461za7, va_generic_entry,
		BGl_z62zf2z90zz__r4_numbers_6_5z00, BUNSPEC, -2);
	      DEFINE_STRING(BGl_string3345z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3345za700za7za7_3462za7, "ceiling", 7);
	      DEFINE_STRING(BGl_string3346z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3346za700za7za7_3463za7, "truncate", 8);
	      DEFINE_STRING(BGl_string3347z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3347za700za7za7_3464za7, "round", 5);
	      DEFINE_STRING(BGl_string3348z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3348za700za7za7_3465za7, "exp", 3);
	      DEFINE_STRING(BGl_string3349z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3349za700za7za7_3466za7, "log", 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bignumzd2ze3uint64zd2envze3zz__r4_numbers_6_5z00,
		BgL_bgl_za762bignumza7d2za7e3u3467za7,
		BGl_z62bignumzd2ze3uint64z53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_uint64zd2ze3bignumzd2envze3zz__r4_numbers_6_5z00,
		BgL_bgl_za762uint64za7d2za7e3b3468za7,
		BGl_z62uint64zd2ze3bignumz53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_2zb2zd2envz60zz__r4_numbers_6_5z00,
		BgL_bgl_za7622za7b2za7d0za7za7__r43469za7,
		BGl_z622zb2zd0zz__r4_numbers_6_5z00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_2zf2zd2envz20zz__r4_numbers_6_5z00,
		BgL_bgl_za7622za7f2za790za7za7__r43470za7,
		BGl_z622zf2z90zz__r4_numbers_6_5z00, 0L, BUNSPEC, 2);
#define BGl_real3368z00zz__r4_numbers_6_5z00 bigloo_nan
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_numberzd2ze3flonumzd2envze3zz__r4_numbers_6_5z00,
		BgL_bgl_za762numberza7d2za7e3f3471za7,
		BGl_z62numberzd2ze3flonumz53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_truncatezd2envzd2zz__r4_numbers_6_5z00,
		BgL_bgl_za762truncateza762za7za73472z00,
		BGl_z62truncatez62zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3350z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3350za700za7za7_3473za7, "log2", 4);
	      DEFINE_STRING(BGl_string3351z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3351za700za7za7_3474za7, "log10", 5);
	      DEFINE_STRING(BGl_string3352z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3352za700za7za7_3475za7, "sin", 3);
	      DEFINE_STRING(BGl_string3353z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3353za700za7za7_3476za7, "cos", 3);
	      DEFINE_STRING(BGl_string3354z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3354za700za7za7_3477za7, "tan", 3);
	      DEFINE_STRING(BGl_string3355z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3355za700za7za7_3478za7, "asin", 4);
	      DEFINE_STRING(BGl_string3356z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3356za700za7za7_3479za7, "acos", 4);
	      DEFINE_STRING(BGl_string3357z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3357za700za7za7_3480za7, "atan", 4);
	      DEFINE_STRING(BGl_string3358z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3358za700za7za7_3481za7, "sqrt", 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_complexzf3zd2envz21zz__r4_numbers_6_5z00,
		BgL_bgl_za762complexza7f3za7913482za7,
		BGl_z62complexzf3z91zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
#define BGl_real3370z00zz__r4_numbers_6_5z00 bigloo_infinity
#define BGl_real3372z00zz__r4_numbers_6_5z00 bigloo_minfinity
#define BGl_real3373z00zz__r4_numbers_6_5z00 bigloo_nan
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2ze3numberzd2envze3zz__r4_numbers_6_5z00,
		BgL_bgl__stringza7d2za7e3num3483z00, opt_generic_entry,
		BGl__stringzd2ze3numberz31zz__r4_numbers_6_5z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_numberzd2ze3stringzd2envze3zz__r4_numbers_6_5z00,
		BgL_bgl__numberza7d2za7e3str3484z00, opt_generic_entry,
		BGl__numberzd2ze3stringz31zz__r4_numbers_6_5z00, BFALSE, -1);
	      DEFINE_STRING(BGl_string3360z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3360za700za7za7_3485za7, "expt", 4);
	      DEFINE_STRING(BGl_string3361z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3361za700za7za7_3486za7, "number->string", 14);
	      DEFINE_STRING(BGl_string3362z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3362za700za7za7_3487za7, "Argument not a number", 21);
	      DEFINE_STRING(BGl_string3364z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3364za700za7za7_3488za7, "Illegal radix", 13);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_elongzd2ze3flonumzd2envze3zz__r4_numbers_6_5z00,
		BgL_bgl_za762elongza7d2za7e3fl3489za7,
		BGl_z62elongzd2ze3flonumz53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3365z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3365za700za7za7_3490za7, "_string->number", 15);
	      DEFINE_STRING(BGl_string3366z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3366za700za7za7_3491za7, "bstring", 7);
	      DEFINE_STRING(BGl_string3367z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3367za700za7za7_3492za7, "+nan.0", 6);
	      DEFINE_STRING(BGl_string3369z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3369za700za7za7_3493za7, "+inf.0", 6);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_abszd2envzd2zz__r4_numbers_6_5z00,
		BgL_bgl_za762absza762za7za7__r4_3494z00, BGl_z62absz62zz__r4_numbers_6_5z00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_llongzd2ze3flonumzd2envze3zz__r4_numbers_6_5z00,
		BgL_bgl_za762llongza7d2za7e3fl3495za7,
		BGl_z62llongzd2ze3flonumz53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_exptzd2envzd2zz__r4_numbers_6_5z00,
		BgL_bgl_za762exptza762za7za7__r43496z00,
		BGl_z62exptz62zz__r4_numbers_6_5z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_int64zd2ze3bignumzd2envze3zz__r4_numbers_6_5z00,
		BgL_bgl_za762int64za7d2za7e3bi3497za7,
		BGl_z62int64zd2ze3bignumz53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3371z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3371za700za7za7_3498za7, "-inf.0", 6);
	      DEFINE_STRING(BGl_string3374z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3374za700za7za7_3499za7, "string->number", 14);
	      DEFINE_STRING(BGl_string3375z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3375za700za7za7_3500za7,
		"Only radix `10' is legal for floating point number", 50);
	      DEFINE_STRING(BGl_string3376z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3376za700za7za7_3501za7, "__r4_numbers_6_5", 16);
	      DEFINE_STRING(BGl_string3295z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3295za700za7za7_3502za7, "number->flonum", 14);
	      DEFINE_STRING(BGl_string3296z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3296za700za7za7_3503za7, "number", 6);
	      DEFINE_STRING(BGl_string3297z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3297za700za7za7_3504za7,
		"/tmp/bigloo/runtime/Ieee/number.scm", 35);
	      DEFINE_STRING(BGl_string3298z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3298za700za7za7_3505za7, "&flonum->fixnum", 15);
	      DEFINE_STRING(BGl_string3299z00zz__r4_numbers_6_5z00,
		BgL_bgl_string3299za700za7za7_3506za7, "real", 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_inexactzf3zd2envz21zz__r4_numbers_6_5z00,
		BgL_bgl_za762inexactza7f3za7913507za7,
		BGl_z62inexactzf3z91zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_exactzd2ze3inexactzd2envze3zz__r4_numbers_6_5z00,
		BgL_bgl_za762exactza7d2za7e3in3508za7,
		BGl_z62exactzd2ze3inexactz53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_flonumzd2ze3int32zd2envze3zz__r4_numbers_6_5z00,
		BgL_bgl_za762flonumza7d2za7e3i3509za7,
		BGl_z62flonumzd2ze3int32z53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_positivezf3zd2envz21zz__r4_numbers_6_5z00,
		BgL_bgl_za762positiveza7f3za793510za7,
		BGl_z62positivezf3z91zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_exactzf3zd2envz21zz__r4_numbers_6_5z00,
		BgL_bgl_za762exactza7f3za791za7za73511za7,
		BGl_z62exactzf3z91zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_zc3zd2envz11zz__r4_numbers_6_5z00,
		BgL_bgl_za762za7c3za7a1za7za7__r4_3512za7, va_generic_entry,
		BGl_z62zc3za1zz__r4_numbers_6_5z00, BUNSPEC, -3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_uint64zd2ze3flonumzd2envze3zz__r4_numbers_6_5z00,
		BgL_bgl_za762uint64za7d2za7e3f3513za7,
		BGl_z62uint64zd2ze3flonumz53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_roundzd2envzd2zz__r4_numbers_6_5z00,
		BgL_bgl_za762roundza762za7za7__r3514z00,
		BGl_z62roundz62zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sqrtzd2envzd2zz__r4_numbers_6_5z00,
		BgL_bgl_za762sqrtza762za7za7__r43515z00,
		BGl_z62sqrtz62zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_inexactzd2ze3exactzd2envze3zz__r4_numbers_6_5z00,
		BgL_bgl_za762inexactza7d2za7e33516za7,
		BGl_z62inexactzd2ze3exactz53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_2zc3zd2envz11zz__r4_numbers_6_5z00,
		BgL_bgl_za7622za7c3za7a1za7za7__r43517za7,
		BGl_z622zc3za1zz__r4_numbers_6_5z00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ze3zd3zd2envze2zz__r4_numbers_6_5z00,
		BgL_bgl_za762za7e3za7d3za752za7za7__3518z00, va_generic_entry,
		BGl_z62ze3zd3z52zz__r4_numbers_6_5z00, BUNSPEC, -3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_uint32zd2ze3flonumzd2envze3zz__r4_numbers_6_5z00,
		BgL_bgl_za762uint32za7d2za7e3f3519za7,
		BGl_z62uint32zd2ze3flonumz53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_flonumzd2ze3uint32zd2envze3zz__r4_numbers_6_5z00,
		BgL_bgl_za762flonumza7d2za7e3u3520za7,
		BGl_z62flonumzd2ze3uint32z53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_logzd2envzd2zz__r4_numbers_6_5z00,
		BgL_bgl_za762logza762za7za7__r4_3521z00, BGl_z62logz62zz__r4_numbers_6_5z00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_flonumzd2ze3elongzd2envze3zz__r4_numbers_6_5z00,
		BgL_bgl_za762flonumza7d2za7e3e3522za7,
		BGl_z62flonumzd2ze3elongz53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_log2zd2envzd2zz__r4_numbers_6_5z00,
		BgL_bgl_za762log2za762za7za7__r43523z00,
		BGl_z62log2z62zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ceilingzd2envzd2zz__r4_numbers_6_5z00,
		BgL_bgl_za762ceilingza762za7za7_3524z00,
		BGl_z62ceilingz62zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bignumzd2ze3flonumzd2envze3zz__r4_numbers_6_5z00,
		BgL_bgl_za762bignumza7d2za7e3f3525za7,
		BGl_z62bignumzd2ze3flonumz53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__r4_numbers_6_5z00));
		     ADD_ROOT((void *) (&BGl_symbol3363z00zz__r4_numbers_6_5z00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long
		BgL_checksumz00_5176, char *BgL_fromz00_5177)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__r4_numbers_6_5z00))
				{
					BGl_requirezd2initializa7ationz75zz__r4_numbers_6_5z00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__r4_numbers_6_5z00();
					BGl_cnstzd2initzd2zz__r4_numbers_6_5z00();
					BGl_importedzd2moduleszd2initz00zz__r4_numbers_6_5z00();
					return BGl_toplevelzd2initzd2zz__r4_numbers_6_5z00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__r4_numbers_6_5z00(void)
	{
		{	/* Ieee/number.scm 18 */
			return (BGl_symbol3363z00zz__r4_numbers_6_5z00 =
				bstring_to_symbol(BGl_string3361z00zz__r4_numbers_6_5z00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__r4_numbers_6_5z00(void)
	{
		{	/* Ieee/number.scm 18 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__r4_numbers_6_5z00(void)
	{
		{	/* Ieee/number.scm 18 */
			BGl_za2maxintflza2z00zz__r4_numbers_6_5z00 = (double) (BGL_LONG_MAX);
			return (BGl_za2minintflza2z00zz__r4_numbers_6_5z00 =
				(double) (BGL_LONG_MIN), BUNSPEC);
		}

	}



/* number? */
	BGL_EXPORTED_DEF bool_t BGl_numberzf3zf3zz__r4_numbers_6_5z00(obj_t
		BgL_objz00_3)
	{
		{	/* Ieee/number.scm 247 */
			{	/* Ieee/number.scm 248 */
				bool_t BgL__ortest_1012z00_871;

				BgL__ortest_1012z00_871 = INTEGERP(BgL_objz00_3);
				if (BgL__ortest_1012z00_871)
					{	/* Ieee/number.scm 248 */
						return BgL__ortest_1012z00_871;
					}
				else
					{	/* Ieee/number.scm 249 */
						bool_t BgL__ortest_1013z00_872;

						BgL__ortest_1013z00_872 = REALP(BgL_objz00_3);
						if (BgL__ortest_1013z00_872)
							{	/* Ieee/number.scm 249 */
								return BgL__ortest_1013z00_872;
							}
						else
							{	/* Ieee/number.scm 250 */
								bool_t BgL__ortest_1014z00_873;

								BgL__ortest_1014z00_873 = ELONGP(BgL_objz00_3);
								if (BgL__ortest_1014z00_873)
									{	/* Ieee/number.scm 250 */
										return BgL__ortest_1014z00_873;
									}
								else
									{	/* Ieee/number.scm 251 */
										bool_t BgL__ortest_1015z00_874;

										BgL__ortest_1015z00_874 = LLONGP(BgL_objz00_3);
										if (BgL__ortest_1015z00_874)
											{	/* Ieee/number.scm 251 */
												return BgL__ortest_1015z00_874;
											}
										else
											{	/* Ieee/number.scm 252 */
												bool_t BgL__ortest_1016z00_875;

												BgL__ortest_1016z00_875 = BGL_INT8P(BgL_objz00_3);
												if (BgL__ortest_1016z00_875)
													{	/* Ieee/number.scm 252 */
														return BgL__ortest_1016z00_875;
													}
												else
													{	/* Ieee/number.scm 253 */
														bool_t BgL__ortest_1017z00_876;

														BgL__ortest_1017z00_876 = BGL_UINT8P(BgL_objz00_3);
														if (BgL__ortest_1017z00_876)
															{	/* Ieee/number.scm 253 */
																return BgL__ortest_1017z00_876;
															}
														else
															{	/* Ieee/number.scm 254 */
																bool_t BgL__ortest_1018z00_877;

																BgL__ortest_1018z00_877 =
																	BGL_INT16P(BgL_objz00_3);
																if (BgL__ortest_1018z00_877)
																	{	/* Ieee/number.scm 254 */
																		return BgL__ortest_1018z00_877;
																	}
																else
																	{	/* Ieee/number.scm 255 */
																		bool_t BgL__ortest_1019z00_878;

																		BgL__ortest_1019z00_878 =
																			BGL_UINT16P(BgL_objz00_3);
																		if (BgL__ortest_1019z00_878)
																			{	/* Ieee/number.scm 255 */
																				return BgL__ortest_1019z00_878;
																			}
																		else
																			{	/* Ieee/number.scm 256 */
																				bool_t BgL__ortest_1020z00_879;

																				BgL__ortest_1020z00_879 =
																					BGL_INT32P(BgL_objz00_3);
																				if (BgL__ortest_1020z00_879)
																					{	/* Ieee/number.scm 256 */
																						return BgL__ortest_1020z00_879;
																					}
																				else
																					{	/* Ieee/number.scm 257 */
																						bool_t BgL__ortest_1021z00_880;

																						BgL__ortest_1021z00_880 =
																							BGL_UINT32P(BgL_objz00_3);
																						if (BgL__ortest_1021z00_880)
																							{	/* Ieee/number.scm 257 */
																								return BgL__ortest_1021z00_880;
																							}
																						else
																							{	/* Ieee/number.scm 258 */
																								bool_t BgL__ortest_1022z00_881;

																								BgL__ortest_1022z00_881 =
																									BGL_INT64P(BgL_objz00_3);
																								if (BgL__ortest_1022z00_881)
																									{	/* Ieee/number.scm 258 */
																										return
																											BgL__ortest_1022z00_881;
																									}
																								else
																									{	/* Ieee/number.scm 259 */
																										bool_t
																											BgL__ortest_1023z00_882;
																										BgL__ortest_1023z00_882 =
																											BGL_UINT64P(BgL_objz00_3);
																										if (BgL__ortest_1023z00_882)
																											{	/* Ieee/number.scm 259 */
																												return
																													BgL__ortest_1023z00_882;
																											}
																										else
																											{	/* Ieee/number.scm 259 */
																												return
																													BIGNUMP(BgL_objz00_3);
																											}
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
									}
							}
					}
			}
		}

	}



/* &number? */
	obj_t BGl_z62numberzf3z91zz__r4_numbers_6_5z00(obj_t BgL_envz00_4954,
		obj_t BgL_objz00_4955)
	{
		{	/* Ieee/number.scm 247 */
			return BBOOL(BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_objz00_4955));
		}

	}



/* exact? */
	BGL_EXPORTED_DEF bool_t BGl_exactzf3zf3zz__r4_numbers_6_5z00(obj_t
		BgL_za7za7_4)
	{
		{	/* Ieee/number.scm 265 */
			{	/* Ieee/number.scm 266 */
				bool_t BgL__ortest_1024z00_5165;

				BgL__ortest_1024z00_5165 = INTEGERP(BgL_za7za7_4);
				if (BgL__ortest_1024z00_5165)
					{	/* Ieee/number.scm 266 */
						return BgL__ortest_1024z00_5165;
					}
				else
					{	/* Ieee/number.scm 267 */
						bool_t BgL__ortest_1025z00_5166;

						BgL__ortest_1025z00_5166 = ELONGP(BgL_za7za7_4);
						if (BgL__ortest_1025z00_5166)
							{	/* Ieee/number.scm 267 */
								return BgL__ortest_1025z00_5166;
							}
						else
							{	/* Ieee/number.scm 268 */
								bool_t BgL__ortest_1026z00_5167;

								BgL__ortest_1026z00_5167 = LLONGP(BgL_za7za7_4);
								if (BgL__ortest_1026z00_5167)
									{	/* Ieee/number.scm 268 */
										return BgL__ortest_1026z00_5167;
									}
								else
									{	/* Ieee/number.scm 269 */
										bool_t BgL__ortest_1027z00_5168;

										BgL__ortest_1027z00_5168 = BGL_INT8P(BgL_za7za7_4);
										if (BgL__ortest_1027z00_5168)
											{	/* Ieee/number.scm 269 */
												return BgL__ortest_1027z00_5168;
											}
										else
											{	/* Ieee/number.scm 270 */
												bool_t BgL__ortest_1028z00_5169;

												BgL__ortest_1028z00_5169 = BGL_UINT8P(BgL_za7za7_4);
												if (BgL__ortest_1028z00_5169)
													{	/* Ieee/number.scm 270 */
														return BgL__ortest_1028z00_5169;
													}
												else
													{	/* Ieee/number.scm 271 */
														bool_t BgL__ortest_1029z00_5170;

														BgL__ortest_1029z00_5170 = BGL_INT16P(BgL_za7za7_4);
														if (BgL__ortest_1029z00_5170)
															{	/* Ieee/number.scm 271 */
																return BgL__ortest_1029z00_5170;
															}
														else
															{	/* Ieee/number.scm 272 */
																bool_t BgL__ortest_1030z00_5171;

																BgL__ortest_1030z00_5171 =
																	BGL_UINT16P(BgL_za7za7_4);
																if (BgL__ortest_1030z00_5171)
																	{	/* Ieee/number.scm 272 */
																		return BgL__ortest_1030z00_5171;
																	}
																else
																	{	/* Ieee/number.scm 273 */
																		bool_t BgL__ortest_1031z00_5172;

																		BgL__ortest_1031z00_5172 =
																			BGL_INT32P(BgL_za7za7_4);
																		if (BgL__ortest_1031z00_5172)
																			{	/* Ieee/number.scm 273 */
																				return BgL__ortest_1031z00_5172;
																			}
																		else
																			{	/* Ieee/number.scm 274 */
																				bool_t BgL__ortest_1032z00_5173;

																				BgL__ortest_1032z00_5173 =
																					BGL_UINT32P(BgL_za7za7_4);
																				if (BgL__ortest_1032z00_5173)
																					{	/* Ieee/number.scm 274 */
																						return BgL__ortest_1032z00_5173;
																					}
																				else
																					{	/* Ieee/number.scm 275 */
																						bool_t BgL__ortest_1033z00_5174;

																						BgL__ortest_1033z00_5174 =
																							BGL_INT64P(BgL_za7za7_4);
																						if (BgL__ortest_1033z00_5174)
																							{	/* Ieee/number.scm 275 */
																								return BgL__ortest_1033z00_5174;
																							}
																						else
																							{	/* Ieee/number.scm 276 */
																								bool_t BgL__ortest_1034z00_5175;

																								BgL__ortest_1034z00_5175 =
																									BGL_UINT64P(BgL_za7za7_4);
																								if (BgL__ortest_1034z00_5175)
																									{	/* Ieee/number.scm 276 */
																										return
																											BgL__ortest_1034z00_5175;
																									}
																								else
																									{	/* Ieee/number.scm 276 */
																										return
																											BIGNUMP(BgL_za7za7_4);
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
									}
							}
					}
			}
		}

	}



/* &exact? */
	obj_t BGl_z62exactzf3z91zz__r4_numbers_6_5z00(obj_t BgL_envz00_4956,
		obj_t BgL_za7za7_4957)
	{
		{	/* Ieee/number.scm 265 */
			return BBOOL(BGl_exactzf3zf3zz__r4_numbers_6_5z00(BgL_za7za7_4957));
		}

	}



/* inexact? */
	BGL_EXPORTED_DEF bool_t BGl_inexactzf3zf3zz__r4_numbers_6_5z00(obj_t
		BgL_za7za7_5)
	{
		{	/* Ieee/number.scm 282 */
			return REALP(BgL_za7za7_5);
		}

	}



/* &inexact? */
	obj_t BGl_z62inexactzf3z91zz__r4_numbers_6_5z00(obj_t BgL_envz00_4958,
		obj_t BgL_za7za7_4959)
	{
		{	/* Ieee/number.scm 282 */
			return BBOOL(BGl_inexactzf3zf3zz__r4_numbers_6_5z00(BgL_za7za7_4959));
		}

	}



/* complex? */
	BGL_EXPORTED_DEF bool_t BGl_complexzf3zf3zz__r4_numbers_6_5z00(obj_t
		BgL_xz00_6)
	{
		{	/* Ieee/number.scm 288 */
			BGL_TAIL return BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_xz00_6);
		}

	}



/* &complex? */
	obj_t BGl_z62complexzf3z91zz__r4_numbers_6_5z00(obj_t BgL_envz00_4960,
		obj_t BgL_xz00_4961)
	{
		{	/* Ieee/number.scm 288 */
			return BBOOL(BGl_complexzf3zf3zz__r4_numbers_6_5z00(BgL_xz00_4961));
		}

	}



/* rational? */
	BGL_EXPORTED_DEF bool_t BGl_rationalzf3zf3zz__r4_numbers_6_5z00(obj_t
		BgL_xz00_7)
	{
		{	/* Ieee/number.scm 294 */
			if (INTEGERP(BgL_xz00_7))
				{	/* Ieee/number.scm 295 */
					return ((bool_t) 1);
				}
			else
				{	/* Ieee/number.scm 295 */
					return REALP(BgL_xz00_7);
				}
		}

	}



/* &rational? */
	obj_t BGl_z62rationalzf3z91zz__r4_numbers_6_5z00(obj_t BgL_envz00_4962,
		obj_t BgL_xz00_4963)
	{
		{	/* Ieee/number.scm 294 */
			return BBOOL(BGl_rationalzf3zf3zz__r4_numbers_6_5z00(BgL_xz00_4963));
		}

	}



/* number->flonum */
	BGL_EXPORTED_DEF double BGl_numberzd2ze3flonumz31zz__r4_numbers_6_5z00(obj_t
		BgL_xz00_8)
	{
		{	/* Ieee/number.scm 300 */
			if (INTEGERP(BgL_xz00_8))
				{	/* Ieee/number.scm 302 */
					return (double) ((long) CINT(BgL_xz00_8));
				}
			else
				{	/* Ieee/number.scm 302 */
					if (BIGNUMP(BgL_xz00_8))
						{	/* Ieee/number.scm 303 */
							return bgl_bignum_to_flonum(BgL_xz00_8);
						}
					else
						{	/* Ieee/number.scm 303 */
							if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_xz00_8))
								{	/* Ieee/number.scm 304 */
									obj_t BgL_arg1122z00_897;

									BgL_arg1122z00_897 =
										BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
										(BgL_xz00_8);
									{	/* Ieee/number.scm 304 */
										long BgL_xz00_2829;

										BgL_xz00_2829 = BELONG_TO_LONG(BgL_arg1122z00_897);
										return (double) (BgL_xz00_2829);
								}}
							else
								{	/* Ieee/number.scm 304 */
									if (LLONGP(BgL_xz00_8))
										{	/* Ieee/number.scm 305 */
											return (double) (BLLONG_TO_LLONG(BgL_xz00_8));
										}
									else
										{	/* Ieee/number.scm 305 */
											if (REALP(BgL_xz00_8))
												{	/* Ieee/number.scm 306 */
													return REAL_TO_DOUBLE(BgL_xz00_8);
												}
											else
												{	/* Ieee/number.scm 306 */
													return
														REAL_TO_DOUBLE
														(BGl_bigloozd2typezd2errorz00zz__errorz00
														(BGl_string3295z00zz__r4_numbers_6_5z00,
															BGl_string3296z00zz__r4_numbers_6_5z00,
															BgL_xz00_8));
												}
										}
								}
						}
				}
		}

	}



/* &number->flonum */
	obj_t BGl_z62numberzd2ze3flonumz53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4964,
		obj_t BgL_xz00_4965)
	{
		{	/* Ieee/number.scm 300 */
			return
				DOUBLE_TO_REAL(BGl_numberzd2ze3flonumz31zz__r4_numbers_6_5z00
				(BgL_xz00_4965));
		}

	}



/* flonum->fixnum */
	BGL_EXPORTED_DEF long BGl_flonumzd2ze3fixnumz31zz__r4_numbers_6_5z00(double
		BgL_xz00_9)
	{
		{	/* Ieee/number.scm 312 */
			return (long) (BgL_xz00_9);
		}

	}



/* &flonum->fixnum */
	obj_t BGl_z62flonumzd2ze3fixnumz53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4966,
		obj_t BgL_xz00_4967)
	{
		{	/* Ieee/number.scm 312 */
			{	/* Ieee/number.scm 312 */
				long BgL_tmpz00_5276;

				{	/* Ieee/number.scm 312 */
					double BgL_auxz00_5277;

					{	/* Ieee/number.scm 312 */
						obj_t BgL_tmpz00_5278;

						if (REALP(BgL_xz00_4967))
							{	/* Ieee/number.scm 312 */
								BgL_tmpz00_5278 = BgL_xz00_4967;
							}
						else
							{
								obj_t BgL_auxz00_5281;

								BgL_auxz00_5281 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string3297z00zz__r4_numbers_6_5z00, BINT(12176L),
									BGl_string3298z00zz__r4_numbers_6_5z00,
									BGl_string3299z00zz__r4_numbers_6_5z00, BgL_xz00_4967);
								FAILURE(BgL_auxz00_5281, BFALSE, BFALSE);
							}
						BgL_auxz00_5277 = REAL_TO_DOUBLE(BgL_tmpz00_5278);
					}
					BgL_tmpz00_5276 =
						BGl_flonumzd2ze3fixnumz31zz__r4_numbers_6_5z00(BgL_auxz00_5277);
				}
				return BINT(BgL_tmpz00_5276);
			}
		}

	}



/* fixnum->flonum */
	BGL_EXPORTED_DEF double BGl_fixnumzd2ze3flonumz31zz__r4_numbers_6_5z00(long
		BgL_xz00_10)
	{
		{	/* Ieee/number.scm 313 */
			return (double) (BgL_xz00_10);
		}

	}



/* &fixnum->flonum */
	obj_t BGl_z62fixnumzd2ze3flonumz53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4968,
		obj_t BgL_xz00_4969)
	{
		{	/* Ieee/number.scm 313 */
			{	/* Ieee/number.scm 313 */
				double BgL_tmpz00_5289;

				{	/* Ieee/number.scm 313 */
					long BgL_auxz00_5290;

					{	/* Ieee/number.scm 313 */
						obj_t BgL_tmpz00_5291;

						if (INTEGERP(BgL_xz00_4969))
							{	/* Ieee/number.scm 313 */
								BgL_tmpz00_5291 = BgL_xz00_4969;
							}
						else
							{
								obj_t BgL_auxz00_5294;

								BgL_auxz00_5294 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string3297z00zz__r4_numbers_6_5z00, BINT(12231L),
									BGl_string3300z00zz__r4_numbers_6_5z00,
									BGl_string3301z00zz__r4_numbers_6_5z00, BgL_xz00_4969);
								FAILURE(BgL_auxz00_5294, BFALSE, BFALSE);
							}
						BgL_auxz00_5290 = (long) CINT(BgL_tmpz00_5291);
					}
					BgL_tmpz00_5289 =
						BGl_fixnumzd2ze3flonumz31zz__r4_numbers_6_5z00(BgL_auxz00_5290);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_5289);
			}
		}

	}



/* flonum->elong */
	BGL_EXPORTED_DEF long BGl_flonumzd2ze3elongz31zz__r4_numbers_6_5z00(double
		BgL_xz00_11)
	{
		{	/* Ieee/number.scm 318 */
			{	/* Ieee/number.scm 318 */
				long BgL_tmpz00_5301;

				BgL_tmpz00_5301 = (long) (BgL_xz00_11);
				return (long) (BgL_tmpz00_5301);
		}}

	}



/* &flonum->elong */
	obj_t BGl_z62flonumzd2ze3elongz53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4970,
		obj_t BgL_xz00_4971)
	{
		{	/* Ieee/number.scm 318 */
			{	/* Ieee/number.scm 318 */
				long BgL_tmpz00_5304;

				{	/* Ieee/number.scm 318 */
					double BgL_auxz00_5305;

					{	/* Ieee/number.scm 318 */
						obj_t BgL_tmpz00_5306;

						if (REALP(BgL_xz00_4971))
							{	/* Ieee/number.scm 318 */
								BgL_tmpz00_5306 = BgL_xz00_4971;
							}
						else
							{
								obj_t BgL_auxz00_5309;

								BgL_auxz00_5309 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string3297z00zz__r4_numbers_6_5z00, BINT(12517L),
									BGl_string3302z00zz__r4_numbers_6_5z00,
									BGl_string3299z00zz__r4_numbers_6_5z00, BgL_xz00_4971);
								FAILURE(BgL_auxz00_5309, BFALSE, BFALSE);
							}
						BgL_auxz00_5305 = REAL_TO_DOUBLE(BgL_tmpz00_5306);
					}
					BgL_tmpz00_5304 =
						BGl_flonumzd2ze3elongz31zz__r4_numbers_6_5z00(BgL_auxz00_5305);
				}
				return make_belong(BgL_tmpz00_5304);
			}
		}

	}



/* elong->flonum */
	BGL_EXPORTED_DEF double BGl_elongzd2ze3flonumz31zz__r4_numbers_6_5z00(long
		BgL_xz00_12)
	{
		{	/* Ieee/number.scm 319 */
			return (double) (BgL_xz00_12);
		}

	}



/* &elong->flonum */
	obj_t BGl_z62elongzd2ze3flonumz53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4972,
		obj_t BgL_xz00_4973)
	{
		{	/* Ieee/number.scm 319 */
			{	/* Ieee/number.scm 319 */
				double BgL_tmpz00_5317;

				{	/* Ieee/number.scm 319 */
					long BgL_auxz00_5318;

					{	/* Ieee/number.scm 319 */
						obj_t BgL_tmpz00_5319;

						if (ELONGP(BgL_xz00_4973))
							{	/* Ieee/number.scm 319 */
								BgL_tmpz00_5319 = BgL_xz00_4973;
							}
						else
							{
								obj_t BgL_auxz00_5322;

								BgL_auxz00_5322 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string3297z00zz__r4_numbers_6_5z00, BINT(12570L),
									BGl_string3303z00zz__r4_numbers_6_5z00,
									BGl_string3304z00zz__r4_numbers_6_5z00, BgL_xz00_4973);
								FAILURE(BgL_auxz00_5322, BFALSE, BFALSE);
							}
						BgL_auxz00_5318 = BELONG_TO_LONG(BgL_tmpz00_5319);
					}
					BgL_tmpz00_5317 =
						BGl_elongzd2ze3flonumz31zz__r4_numbers_6_5z00(BgL_auxz00_5318);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_5317);
			}
		}

	}



/* flonum->llong */
	BGL_EXPORTED_DEF BGL_LONGLONG_T
		BGl_flonumzd2ze3llongz31zz__r4_numbers_6_5z00(double BgL_xz00_13)
	{
		{	/* Ieee/number.scm 324 */
			return (BGL_LONGLONG_T) (BgL_xz00_13);
		}

	}



/* &flonum->llong */
	obj_t BGl_z62flonumzd2ze3llongz53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4974,
		obj_t BgL_xz00_4975)
	{
		{	/* Ieee/number.scm 324 */
			{	/* Ieee/number.scm 324 */
				BGL_LONGLONG_T BgL_tmpz00_5330;

				{	/* Ieee/number.scm 324 */
					double BgL_auxz00_5331;

					{	/* Ieee/number.scm 324 */
						obj_t BgL_tmpz00_5332;

						if (REALP(BgL_xz00_4975))
							{	/* Ieee/number.scm 324 */
								BgL_tmpz00_5332 = BgL_xz00_4975;
							}
						else
							{
								obj_t BgL_auxz00_5335;

								BgL_auxz00_5335 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string3297z00zz__r4_numbers_6_5z00, BINT(12855L),
									BGl_string3305z00zz__r4_numbers_6_5z00,
									BGl_string3299z00zz__r4_numbers_6_5z00, BgL_xz00_4975);
								FAILURE(BgL_auxz00_5335, BFALSE, BFALSE);
							}
						BgL_auxz00_5331 = REAL_TO_DOUBLE(BgL_tmpz00_5332);
					}
					BgL_tmpz00_5330 =
						BGl_flonumzd2ze3llongz31zz__r4_numbers_6_5z00(BgL_auxz00_5331);
				}
				return make_bllong(BgL_tmpz00_5330);
			}
		}

	}



/* llong->flonum */
	BGL_EXPORTED_DEF double
		BGl_llongzd2ze3flonumz31zz__r4_numbers_6_5z00(BGL_LONGLONG_T BgL_xz00_14)
	{
		{	/* Ieee/number.scm 325 */
			return (double) (BgL_xz00_14);
		}

	}



/* &llong->flonum */
	obj_t BGl_z62llongzd2ze3flonumz53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4976,
		obj_t BgL_xz00_4977)
	{
		{	/* Ieee/number.scm 325 */
			{	/* Ieee/number.scm 325 */
				double BgL_tmpz00_5343;

				{	/* Ieee/number.scm 325 */
					BGL_LONGLONG_T BgL_auxz00_5344;

					{	/* Ieee/number.scm 325 */
						obj_t BgL_tmpz00_5345;

						if (LLONGP(BgL_xz00_4977))
							{	/* Ieee/number.scm 325 */
								BgL_tmpz00_5345 = BgL_xz00_4977;
							}
						else
							{
								obj_t BgL_auxz00_5348;

								BgL_auxz00_5348 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string3297z00zz__r4_numbers_6_5z00, BINT(12908L),
									BGl_string3306z00zz__r4_numbers_6_5z00,
									BGl_string3307z00zz__r4_numbers_6_5z00, BgL_xz00_4977);
								FAILURE(BgL_auxz00_5348, BFALSE, BFALSE);
							}
						BgL_auxz00_5344 = BLLONG_TO_LLONG(BgL_tmpz00_5345);
					}
					BgL_tmpz00_5343 =
						BGl_llongzd2ze3flonumz31zz__r4_numbers_6_5z00(BgL_auxz00_5344);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_5343);
			}
		}

	}



/* flonum->bignum */
	BGL_EXPORTED_DEF obj_t BGl_flonumzd2ze3bignumz31zz__r4_numbers_6_5z00(double
		BgL_xz00_15)
	{
		{	/* Ieee/number.scm 330 */
			BGL_TAIL return bgl_flonum_to_bignum(BgL_xz00_15);
		}

	}



/* &flonum->bignum */
	obj_t BGl_z62flonumzd2ze3bignumz53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4978,
		obj_t BgL_xz00_4979)
	{
		{	/* Ieee/number.scm 330 */
			{	/* Ieee/number.scm 330 */
				double BgL_auxz00_5356;

				{	/* Ieee/number.scm 330 */
					obj_t BgL_tmpz00_5357;

					if (REALP(BgL_xz00_4979))
						{	/* Ieee/number.scm 330 */
							BgL_tmpz00_5357 = BgL_xz00_4979;
						}
					else
						{
							obj_t BgL_auxz00_5360;

							BgL_auxz00_5360 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string3297z00zz__r4_numbers_6_5z00, BINT(13194L),
								BGl_string3308z00zz__r4_numbers_6_5z00,
								BGl_string3299z00zz__r4_numbers_6_5z00, BgL_xz00_4979);
							FAILURE(BgL_auxz00_5360, BFALSE, BFALSE);
						}
					BgL_auxz00_5356 = REAL_TO_DOUBLE(BgL_tmpz00_5357);
				}
				return BGl_flonumzd2ze3bignumz31zz__r4_numbers_6_5z00(BgL_auxz00_5356);
			}
		}

	}



/* bignum->flonum */
	BGL_EXPORTED_DEF double BGl_bignumzd2ze3flonumz31zz__r4_numbers_6_5z00(obj_t
		BgL_xz00_16)
	{
		{	/* Ieee/number.scm 331 */
			BGL_TAIL return bgl_bignum_to_flonum(BgL_xz00_16);
		}

	}



/* &bignum->flonum */
	obj_t BGl_z62bignumzd2ze3flonumz53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4980,
		obj_t BgL_xz00_4981)
	{
		{	/* Ieee/number.scm 331 */
			{	/* Ieee/number.scm 331 */
				double BgL_tmpz00_5367;

				{	/* Ieee/number.scm 331 */
					obj_t BgL_auxz00_5368;

					if (BIGNUMP(BgL_xz00_4981))
						{	/* Ieee/number.scm 331 */
							BgL_auxz00_5368 = BgL_xz00_4981;
						}
					else
						{
							obj_t BgL_auxz00_5371;

							BgL_auxz00_5371 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string3297z00zz__r4_numbers_6_5z00, BINT(13249L),
								BGl_string3309z00zz__r4_numbers_6_5z00,
								BGl_string3310z00zz__r4_numbers_6_5z00, BgL_xz00_4981);
							FAILURE(BgL_auxz00_5371, BFALSE, BFALSE);
						}
					BgL_tmpz00_5367 =
						BGl_bignumzd2ze3flonumz31zz__r4_numbers_6_5z00(BgL_auxz00_5368);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_5367);
			}
		}

	}



/* int64->bignum */
	BGL_EXPORTED_DEF obj_t BGl_int64zd2ze3bignumz31zz__r4_numbers_6_5z00(int64_t
		BgL_xz00_17)
	{
		{	/* Ieee/number.scm 336 */
			return bgl_int64_to_bignum((uint64_t) (BgL_xz00_17));
		}

	}



/* &int64->bignum */
	obj_t BGl_z62int64zd2ze3bignumz53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4982,
		obj_t BgL_xz00_4983)
	{
		{	/* Ieee/number.scm 336 */
			{	/* Ieee/number.scm 336 */
				int64_t BgL_auxz00_5379;

				{	/* Ieee/number.scm 336 */
					obj_t BgL_tmpz00_5380;

					if (BGL_INT64P(BgL_xz00_4983))
						{	/* Ieee/number.scm 336 */
							BgL_tmpz00_5380 = BgL_xz00_4983;
						}
					else
						{
							obj_t BgL_auxz00_5383;

							BgL_auxz00_5383 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string3297z00zz__r4_numbers_6_5z00, BINT(13526L),
								BGl_string3311z00zz__r4_numbers_6_5z00,
								BGl_string3312z00zz__r4_numbers_6_5z00, BgL_xz00_4983);
							FAILURE(BgL_auxz00_5383, BFALSE, BFALSE);
						}
					BgL_auxz00_5379 = BGL_BINT64_TO_INT64(BgL_tmpz00_5380);
				}
				return BGl_int64zd2ze3bignumz31zz__r4_numbers_6_5z00(BgL_auxz00_5379);
			}
		}

	}



/* bignum->int64 */
	BGL_EXPORTED_DEF int64_t BGl_bignumzd2ze3int64z31zz__r4_numbers_6_5z00(obj_t
		BgL_xz00_18)
	{
		{	/* Ieee/number.scm 337 */
			BGL_TAIL return bgl_bignum_to_int64(BgL_xz00_18);
		}

	}



/* &bignum->int64 */
	obj_t BGl_z62bignumzd2ze3int64z53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4984,
		obj_t BgL_xz00_4985)
	{
		{	/* Ieee/number.scm 337 */
			{	/* Ieee/number.scm 337 */
				int64_t BgL_tmpz00_5390;

				{	/* Ieee/number.scm 337 */
					obj_t BgL_auxz00_5391;

					if (BIGNUMP(BgL_xz00_4985))
						{	/* Ieee/number.scm 337 */
							BgL_auxz00_5391 = BgL_xz00_4985;
						}
					else
						{
							obj_t BgL_auxz00_5394;

							BgL_auxz00_5394 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string3297z00zz__r4_numbers_6_5z00, BINT(13579L),
								BGl_string3313z00zz__r4_numbers_6_5z00,
								BGl_string3310z00zz__r4_numbers_6_5z00, BgL_xz00_4985);
							FAILURE(BgL_auxz00_5394, BFALSE, BFALSE);
						}
					BgL_tmpz00_5390 =
						BGl_bignumzd2ze3int64z31zz__r4_numbers_6_5z00(BgL_auxz00_5391);
				}
				return BGL_INT64_TO_BINT64(BgL_tmpz00_5390);
			}
		}

	}



/* uint64->bignum */
	BGL_EXPORTED_DEF obj_t BGl_uint64zd2ze3bignumz31zz__r4_numbers_6_5z00(uint64_t
		BgL_xz00_19)
	{
		{	/* Ieee/number.scm 339 */
			BGL_TAIL return bgl_uint64_to_bignum(BgL_xz00_19);
		}

	}



/* &uint64->bignum */
	obj_t BGl_z62uint64zd2ze3bignumz53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4986,
		obj_t BgL_xz00_4987)
	{
		{	/* Ieee/number.scm 339 */
			{	/* Ieee/number.scm 339 */
				uint64_t BgL_auxz00_5401;

				{	/* Ieee/number.scm 339 */
					obj_t BgL_tmpz00_5402;

					if (BGL_UINT64P(BgL_xz00_4987))
						{	/* Ieee/number.scm 339 */
							BgL_tmpz00_5402 = BgL_xz00_4987;
						}
					else
						{
							obj_t BgL_auxz00_5405;

							BgL_auxz00_5405 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string3297z00zz__r4_numbers_6_5z00, BINT(13634L),
								BGl_string3314z00zz__r4_numbers_6_5z00,
								BGl_string3315z00zz__r4_numbers_6_5z00, BgL_xz00_4987);
							FAILURE(BgL_auxz00_5405, BFALSE, BFALSE);
						}
					BgL_auxz00_5401 = BGL_BINT64_TO_INT64(BgL_tmpz00_5402);
				}
				return BGl_uint64zd2ze3bignumz31zz__r4_numbers_6_5z00(BgL_auxz00_5401);
			}
		}

	}



/* bignum->uint64 */
	BGL_EXPORTED_DEF uint64_t BGl_bignumzd2ze3uint64z31zz__r4_numbers_6_5z00(obj_t
		BgL_xz00_20)
	{
		{	/* Ieee/number.scm 340 */
			BGL_TAIL return bgl_bignum_to_uint64(BgL_xz00_20);
		}

	}



/* &bignum->uint64 */
	obj_t BGl_z62bignumzd2ze3uint64z53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4988,
		obj_t BgL_xz00_4989)
	{
		{	/* Ieee/number.scm 340 */
			{	/* Ieee/number.scm 340 */
				uint64_t BgL_tmpz00_5412;

				{	/* Ieee/number.scm 340 */
					obj_t BgL_auxz00_5413;

					if (BIGNUMP(BgL_xz00_4989))
						{	/* Ieee/number.scm 340 */
							BgL_auxz00_5413 = BgL_xz00_4989;
						}
					else
						{
							obj_t BgL_auxz00_5416;

							BgL_auxz00_5416 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string3297z00zz__r4_numbers_6_5z00, BINT(13689L),
								BGl_string3316z00zz__r4_numbers_6_5z00,
								BGl_string3310z00zz__r4_numbers_6_5z00, BgL_xz00_4989);
							FAILURE(BgL_auxz00_5416, BFALSE, BFALSE);
						}
					BgL_tmpz00_5412 =
						BGl_bignumzd2ze3uint64z31zz__r4_numbers_6_5z00(BgL_auxz00_5413);
				}
				return BGL_UINT64_TO_BUINT64(BgL_tmpz00_5412);
			}
		}

	}



/* flonum->int32 */
	BGL_EXPORTED_DEF int32_t BGl_flonumzd2ze3int32z31zz__r4_numbers_6_5z00(double
		BgL_xz00_21)
	{
		{	/* Ieee/number.scm 345 */
			return (int32_t) (BgL_xz00_21);
		}

	}



/* &flonum->int32 */
	obj_t BGl_z62flonumzd2ze3int32z53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4990,
		obj_t BgL_xz00_4991)
	{
		{	/* Ieee/number.scm 345 */
			{	/* Ieee/number.scm 345 */
				int32_t BgL_tmpz00_5423;

				{	/* Ieee/number.scm 345 */
					double BgL_auxz00_5424;

					{	/* Ieee/number.scm 345 */
						obj_t BgL_tmpz00_5425;

						if (REALP(BgL_xz00_4991))
							{	/* Ieee/number.scm 345 */
								BgL_tmpz00_5425 = BgL_xz00_4991;
							}
						else
							{
								obj_t BgL_auxz00_5428;

								BgL_auxz00_5428 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string3297z00zz__r4_numbers_6_5z00, BINT(13966L),
									BGl_string3317z00zz__r4_numbers_6_5z00,
									BGl_string3299z00zz__r4_numbers_6_5z00, BgL_xz00_4991);
								FAILURE(BgL_auxz00_5428, BFALSE, BFALSE);
							}
						BgL_auxz00_5424 = REAL_TO_DOUBLE(BgL_tmpz00_5425);
					}
					BgL_tmpz00_5423 =
						BGl_flonumzd2ze3int32z31zz__r4_numbers_6_5z00(BgL_auxz00_5424);
				}
				return BGL_INT32_TO_BINT32(BgL_tmpz00_5423);
			}
		}

	}



/* int32->flonum */
	BGL_EXPORTED_DEF double BGl_int32zd2ze3flonumz31zz__r4_numbers_6_5z00(int32_t
		BgL_xz00_22)
	{
		{	/* Ieee/number.scm 346 */
			return (double) (BgL_xz00_22);
		}

	}



/* &int32->flonum */
	obj_t BGl_z62int32zd2ze3flonumz53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4992,
		obj_t BgL_xz00_4993)
	{
		{	/* Ieee/number.scm 346 */
			{	/* Ieee/number.scm 346 */
				double BgL_tmpz00_5436;

				{	/* Ieee/number.scm 346 */
					int32_t BgL_auxz00_5437;

					{	/* Ieee/number.scm 346 */
						obj_t BgL_tmpz00_5438;

						if (BGL_INT32P(BgL_xz00_4993))
							{	/* Ieee/number.scm 346 */
								BgL_tmpz00_5438 = BgL_xz00_4993;
							}
						else
							{
								obj_t BgL_auxz00_5441;

								BgL_auxz00_5441 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string3297z00zz__r4_numbers_6_5z00, BINT(14019L),
									BGl_string3318z00zz__r4_numbers_6_5z00,
									BGl_string3319z00zz__r4_numbers_6_5z00, BgL_xz00_4993);
								FAILURE(BgL_auxz00_5441, BFALSE, BFALSE);
							}
						BgL_auxz00_5437 = BGL_BINT32_TO_INT32(BgL_tmpz00_5438);
					}
					BgL_tmpz00_5436 =
						BGl_int32zd2ze3flonumz31zz__r4_numbers_6_5z00(BgL_auxz00_5437);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_5436);
			}
		}

	}



/* flonum->uint32 */
	BGL_EXPORTED_DEF uint32_t
		BGl_flonumzd2ze3uint32z31zz__r4_numbers_6_5z00(double BgL_xz00_23)
	{
		{	/* Ieee/number.scm 348 */
			return (uint32_t) (BgL_xz00_23);
		}

	}



/* &flonum->uint32 */
	obj_t BGl_z62flonumzd2ze3uint32z53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4994,
		obj_t BgL_xz00_4995)
	{
		{	/* Ieee/number.scm 348 */
			{	/* Ieee/number.scm 348 */
				uint32_t BgL_tmpz00_5449;

				{	/* Ieee/number.scm 348 */
					double BgL_auxz00_5450;

					{	/* Ieee/number.scm 348 */
						obj_t BgL_tmpz00_5451;

						if (REALP(BgL_xz00_4995))
							{	/* Ieee/number.scm 348 */
								BgL_tmpz00_5451 = BgL_xz00_4995;
							}
						else
							{
								obj_t BgL_auxz00_5454;

								BgL_auxz00_5454 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string3297z00zz__r4_numbers_6_5z00, BINT(14074L),
									BGl_string3320z00zz__r4_numbers_6_5z00,
									BGl_string3299z00zz__r4_numbers_6_5z00, BgL_xz00_4995);
								FAILURE(BgL_auxz00_5454, BFALSE, BFALSE);
							}
						BgL_auxz00_5450 = REAL_TO_DOUBLE(BgL_tmpz00_5451);
					}
					BgL_tmpz00_5449 =
						BGl_flonumzd2ze3uint32z31zz__r4_numbers_6_5z00(BgL_auxz00_5450);
				}
				return BGL_UINT32_TO_BUINT32(BgL_tmpz00_5449);
			}
		}

	}



/* uint32->flonum */
	BGL_EXPORTED_DEF double
		BGl_uint32zd2ze3flonumz31zz__r4_numbers_6_5z00(uint32_t BgL_xz00_24)
	{
		{	/* Ieee/number.scm 349 */
			return (double) (BgL_xz00_24);
		}

	}



/* &uint32->flonum */
	obj_t BGl_z62uint32zd2ze3flonumz53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4996,
		obj_t BgL_xz00_4997)
	{
		{	/* Ieee/number.scm 349 */
			{	/* Ieee/number.scm 349 */
				double BgL_tmpz00_5462;

				{	/* Ieee/number.scm 349 */
					uint32_t BgL_auxz00_5463;

					{	/* Ieee/number.scm 349 */
						obj_t BgL_tmpz00_5464;

						if (BGL_UINT32P(BgL_xz00_4997))
							{	/* Ieee/number.scm 349 */
								BgL_tmpz00_5464 = BgL_xz00_4997;
							}
						else
							{
								obj_t BgL_auxz00_5467;

								BgL_auxz00_5467 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string3297z00zz__r4_numbers_6_5z00, BINT(14129L),
									BGl_string3321z00zz__r4_numbers_6_5z00,
									BGl_string3322z00zz__r4_numbers_6_5z00, BgL_xz00_4997);
								FAILURE(BgL_auxz00_5467, BFALSE, BFALSE);
							}
						BgL_auxz00_5463 = BGL_BUINT32_TO_UINT32(BgL_tmpz00_5464);
					}
					BgL_tmpz00_5462 =
						BGl_uint32zd2ze3flonumz31zz__r4_numbers_6_5z00(BgL_auxz00_5463);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_5462);
			}
		}

	}



/* flonum->int64 */
	BGL_EXPORTED_DEF int64_t BGl_flonumzd2ze3int64z31zz__r4_numbers_6_5z00(double
		BgL_xz00_25)
	{
		{	/* Ieee/number.scm 354 */
			return (int64_t) (BgL_xz00_25);
		}

	}



/* &flonum->int64 */
	obj_t BGl_z62flonumzd2ze3int64z53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4998,
		obj_t BgL_xz00_4999)
	{
		{	/* Ieee/number.scm 354 */
			{	/* Ieee/number.scm 354 */
				int64_t BgL_tmpz00_5475;

				{	/* Ieee/number.scm 354 */
					double BgL_auxz00_5476;

					{	/* Ieee/number.scm 354 */
						obj_t BgL_tmpz00_5477;

						if (REALP(BgL_xz00_4999))
							{	/* Ieee/number.scm 354 */
								BgL_tmpz00_5477 = BgL_xz00_4999;
							}
						else
							{
								obj_t BgL_auxz00_5480;

								BgL_auxz00_5480 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string3297z00zz__r4_numbers_6_5z00, BINT(14406L),
									BGl_string3323z00zz__r4_numbers_6_5z00,
									BGl_string3299z00zz__r4_numbers_6_5z00, BgL_xz00_4999);
								FAILURE(BgL_auxz00_5480, BFALSE, BFALSE);
							}
						BgL_auxz00_5476 = REAL_TO_DOUBLE(BgL_tmpz00_5477);
					}
					BgL_tmpz00_5475 =
						BGl_flonumzd2ze3int64z31zz__r4_numbers_6_5z00(BgL_auxz00_5476);
				}
				return BGL_INT64_TO_BINT64(BgL_tmpz00_5475);
			}
		}

	}



/* int64->flonum */
	BGL_EXPORTED_DEF double BGl_int64zd2ze3flonumz31zz__r4_numbers_6_5z00(int64_t
		BgL_xz00_26)
	{
		{	/* Ieee/number.scm 355 */
			return (double) (BgL_xz00_26);
		}

	}



/* &int64->flonum */
	obj_t BGl_z62int64zd2ze3flonumz53zz__r4_numbers_6_5z00(obj_t BgL_envz00_5000,
		obj_t BgL_xz00_5001)
	{
		{	/* Ieee/number.scm 355 */
			{	/* Ieee/number.scm 355 */
				double BgL_tmpz00_5488;

				{	/* Ieee/number.scm 355 */
					int64_t BgL_auxz00_5489;

					{	/* Ieee/number.scm 355 */
						obj_t BgL_tmpz00_5490;

						if (BGL_INT64P(BgL_xz00_5001))
							{	/* Ieee/number.scm 355 */
								BgL_tmpz00_5490 = BgL_xz00_5001;
							}
						else
							{
								obj_t BgL_auxz00_5493;

								BgL_auxz00_5493 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string3297z00zz__r4_numbers_6_5z00, BINT(14459L),
									BGl_string3324z00zz__r4_numbers_6_5z00,
									BGl_string3312z00zz__r4_numbers_6_5z00, BgL_xz00_5001);
								FAILURE(BgL_auxz00_5493, BFALSE, BFALSE);
							}
						BgL_auxz00_5489 = BGL_BINT64_TO_INT64(BgL_tmpz00_5490);
					}
					BgL_tmpz00_5488 =
						BGl_int64zd2ze3flonumz31zz__r4_numbers_6_5z00(BgL_auxz00_5489);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_5488);
			}
		}

	}



/* flonum->uint64 */
	BGL_EXPORTED_DEF uint64_t
		BGl_flonumzd2ze3uint64z31zz__r4_numbers_6_5z00(double BgL_xz00_27)
	{
		{	/* Ieee/number.scm 357 */
			return (uint64_t) (BgL_xz00_27);
		}

	}



/* &flonum->uint64 */
	obj_t BGl_z62flonumzd2ze3uint64z53zz__r4_numbers_6_5z00(obj_t BgL_envz00_5002,
		obj_t BgL_xz00_5003)
	{
		{	/* Ieee/number.scm 357 */
			{	/* Ieee/number.scm 357 */
				uint64_t BgL_tmpz00_5501;

				{	/* Ieee/number.scm 357 */
					double BgL_auxz00_5502;

					{	/* Ieee/number.scm 357 */
						obj_t BgL_tmpz00_5503;

						if (REALP(BgL_xz00_5003))
							{	/* Ieee/number.scm 357 */
								BgL_tmpz00_5503 = BgL_xz00_5003;
							}
						else
							{
								obj_t BgL_auxz00_5506;

								BgL_auxz00_5506 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string3297z00zz__r4_numbers_6_5z00, BINT(14514L),
									BGl_string3325z00zz__r4_numbers_6_5z00,
									BGl_string3299z00zz__r4_numbers_6_5z00, BgL_xz00_5003);
								FAILURE(BgL_auxz00_5506, BFALSE, BFALSE);
							}
						BgL_auxz00_5502 = REAL_TO_DOUBLE(BgL_tmpz00_5503);
					}
					BgL_tmpz00_5501 =
						BGl_flonumzd2ze3uint64z31zz__r4_numbers_6_5z00(BgL_auxz00_5502);
				}
				return BGL_UINT64_TO_BUINT64(BgL_tmpz00_5501);
			}
		}

	}



/* uint64->flonum */
	BGL_EXPORTED_DEF double
		BGl_uint64zd2ze3flonumz31zz__r4_numbers_6_5z00(uint64_t BgL_xz00_28)
	{
		{	/* Ieee/number.scm 358 */
			return (double) (BgL_xz00_28);
		}

	}



/* &uint64->flonum */
	obj_t BGl_z62uint64zd2ze3flonumz53zz__r4_numbers_6_5z00(obj_t BgL_envz00_5004,
		obj_t BgL_xz00_5005)
	{
		{	/* Ieee/number.scm 358 */
			{	/* Ieee/number.scm 358 */
				double BgL_tmpz00_5514;

				{	/* Ieee/number.scm 358 */
					uint64_t BgL_auxz00_5515;

					{	/* Ieee/number.scm 358 */
						obj_t BgL_tmpz00_5516;

						if (BGL_UINT64P(BgL_xz00_5005))
							{	/* Ieee/number.scm 358 */
								BgL_tmpz00_5516 = BgL_xz00_5005;
							}
						else
							{
								obj_t BgL_auxz00_5519;

								BgL_auxz00_5519 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string3297z00zz__r4_numbers_6_5z00, BINT(14569L),
									BGl_string3326z00zz__r4_numbers_6_5z00,
									BGl_string3315z00zz__r4_numbers_6_5z00, BgL_xz00_5005);
								FAILURE(BgL_auxz00_5519, BFALSE, BFALSE);
							}
						BgL_auxz00_5515 = BGL_BINT64_TO_INT64(BgL_tmpz00_5516);
					}
					BgL_tmpz00_5514 =
						BGl_uint64zd2ze3flonumz31zz__r4_numbers_6_5z00(BgL_auxz00_5515);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_5514);
			}
		}

	}



/* $subelong->elong */
	obj_t BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(obj_t BgL_xz00_29)
	{
		{	/* Ieee/number.scm 363 */
			if (ELONGP(BgL_xz00_29))
				{	/* Ieee/number.scm 367 */
					return BgL_xz00_29;
				}
			else
				{	/* Ieee/number.scm 367 */
					if (BGL_INT8P(BgL_xz00_29))
						{	/* Ieee/number.scm 368 */
							long BgL_arg1129z00_903;

							{	/* Ieee/number.scm 368 */
								int8_t BgL_xz00_2831;

								BgL_xz00_2831 = BGL_BINT8_TO_INT8(BgL_xz00_29);
								{	/* Ieee/number.scm 368 */
									long BgL_arg3251z00_2832;

									BgL_arg3251z00_2832 = (long) (BgL_xz00_2831);
									BgL_arg1129z00_903 = (long) (BgL_arg3251z00_2832);
							}}
							{	/* Ieee/number.scm 368 */
								long BgL_tmpz00_5533;

								BgL_tmpz00_5533 = (long) (BgL_arg1129z00_903);
								return make_belong(BgL_tmpz00_5533);
							}
						}
					else
						{	/* Ieee/number.scm 368 */
							if (BGL_UINT8P(BgL_xz00_29))
								{	/* Ieee/number.scm 369 */
									long BgL_arg1131z00_905;

									{	/* Ieee/number.scm 369 */
										uint8_t BgL_xz00_2835;

										BgL_xz00_2835 = BGL_BUINT8_TO_UINT8(BgL_xz00_29);
										{	/* Ieee/number.scm 369 */
											long BgL_arg3250z00_2836;

											BgL_arg3250z00_2836 = (long) (BgL_xz00_2835);
											BgL_arg1131z00_905 = (long) (BgL_arg3250z00_2836);
									}}
									{	/* Ieee/number.scm 369 */
										long BgL_tmpz00_5541;

										BgL_tmpz00_5541 = (long) (BgL_arg1131z00_905);
										return make_belong(BgL_tmpz00_5541);
									}
								}
							else
								{	/* Ieee/number.scm 369 */
									if (BGL_INT16P(BgL_xz00_29))
										{	/* Ieee/number.scm 370 */
											long BgL_arg1137z00_907;

											{	/* Ieee/number.scm 370 */
												int16_t BgL_xz00_2839;

												BgL_xz00_2839 = BGL_BINT16_TO_INT16(BgL_xz00_29);
												{	/* Ieee/number.scm 370 */
													long BgL_arg3249z00_2840;

													BgL_arg3249z00_2840 = (long) (BgL_xz00_2839);
													BgL_arg1137z00_907 = (long) (BgL_arg3249z00_2840);
											}}
											{	/* Ieee/number.scm 370 */
												long BgL_tmpz00_5549;

												BgL_tmpz00_5549 = (long) (BgL_arg1137z00_907);
												return make_belong(BgL_tmpz00_5549);
											}
										}
									else
										{	/* Ieee/number.scm 370 */
											if (BGL_UINT16P(BgL_xz00_29))
												{	/* Ieee/number.scm 371 */
													long BgL_arg1140z00_909;

													{	/* Ieee/number.scm 371 */
														uint16_t BgL_xz00_2843;

														BgL_xz00_2843 = BGL_BUINT16_TO_UINT16(BgL_xz00_29);
														{	/* Ieee/number.scm 371 */
															long BgL_arg3247z00_2844;

															BgL_arg3247z00_2844 = (long) (BgL_xz00_2843);
															BgL_arg1140z00_909 = (long) (BgL_arg3247z00_2844);
													}}
													{	/* Ieee/number.scm 371 */
														long BgL_tmpz00_5557;

														BgL_tmpz00_5557 = (long) (BgL_arg1140z00_909);
														return make_belong(BgL_tmpz00_5557);
													}
												}
											else
												{	/* Ieee/number.scm 371 */
													if (BGL_INT32P(BgL_xz00_29))
														{	/* Ieee/number.scm 372 */
															long BgL_arg1142z00_911;

															{	/* Ieee/number.scm 372 */
																int32_t BgL_xz00_2847;

																BgL_xz00_2847 =
																	BGL_BINT32_TO_INT32(BgL_xz00_29);
																{	/* Ieee/number.scm 372 */
																	long BgL_arg3246z00_2848;

																	BgL_arg3246z00_2848 = (long) (BgL_xz00_2847);
																	BgL_arg1142z00_911 =
																		(long) (BgL_arg3246z00_2848);
															}}
															{	/* Ieee/number.scm 372 */
																long BgL_tmpz00_5565;

																BgL_tmpz00_5565 = (long) (BgL_arg1142z00_911);
																return make_belong(BgL_tmpz00_5565);
															}
														}
													else
														{	/* Ieee/number.scm 372 */
															if (BGL_UINT32P(BgL_xz00_29))
																{	/* Ieee/number.scm 373 */
																	long BgL_arg1145z00_913;

																	{	/* Ieee/number.scm 373 */
																		uint32_t BgL_xz00_2851;

																		BgL_xz00_2851 =
																			BGL_BUINT32_TO_UINT32(BgL_xz00_29);
																		BgL_arg1145z00_913 = (long) (BgL_xz00_2851);
																	}
																	{	/* Ieee/number.scm 373 */
																		long BgL_tmpz00_5572;

																		BgL_tmpz00_5572 =
																			(long) (BgL_arg1145z00_913);
																		return make_belong(BgL_tmpz00_5572);
																	}
																}
															else
																{	/* Ieee/number.scm 373 */
																	if (BGL_INT64P(BgL_xz00_29))
																		{	/* Ieee/number.scm 374 */
																			long BgL_arg1148z00_915;

																			{	/* Ieee/number.scm 374 */
																				int64_t BgL_xz00_2853;

																				BgL_xz00_2853 =
																					BGL_BINT64_TO_INT64(BgL_xz00_29);
																				{	/* Ieee/number.scm 374 */
																					long BgL_arg3245z00_2854;

																					BgL_arg3245z00_2854 =
																						(long) (BgL_xz00_2853);
																					BgL_arg1148z00_915 =
																						(long) (BgL_arg3245z00_2854);
																			}}
																			{	/* Ieee/number.scm 374 */
																				long BgL_tmpz00_5580;

																				BgL_tmpz00_5580 =
																					(long) (BgL_arg1148z00_915);
																				return make_belong(BgL_tmpz00_5580);
																			}
																		}
																	else
																		{	/* Ieee/number.scm 374 */
																			return BFALSE;
																		}
																}
														}
												}
										}
								}
						}
				}
		}

	}



/* $subelong? */
	bool_t BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(obj_t BgL_xz00_31)
	{
		{	/* Ieee/number.scm 394 */
			{	/* Ieee/number.scm 397 */
				bool_t BgL__ortest_1035z00_916;

				BgL__ortest_1035z00_916 = ELONGP(BgL_xz00_31);
				if (BgL__ortest_1035z00_916)
					{	/* Ieee/number.scm 397 */
						return BgL__ortest_1035z00_916;
					}
				else
					{	/* Ieee/number.scm 397 */
						bool_t BgL__ortest_1036z00_917;

						BgL__ortest_1036z00_917 = BGL_INT8P(BgL_xz00_31);
						if (BgL__ortest_1036z00_917)
							{	/* Ieee/number.scm 397 */
								return BgL__ortest_1036z00_917;
							}
						else
							{	/* Ieee/number.scm 397 */
								bool_t BgL__ortest_1037z00_918;

								BgL__ortest_1037z00_918 = BGL_UINT8P(BgL_xz00_31);
								if (BgL__ortest_1037z00_918)
									{	/* Ieee/number.scm 397 */
										return BgL__ortest_1037z00_918;
									}
								else
									{	/* Ieee/number.scm 397 */
										bool_t BgL__ortest_1038z00_919;

										BgL__ortest_1038z00_919 = BGL_INT16P(BgL_xz00_31);
										if (BgL__ortest_1038z00_919)
											{	/* Ieee/number.scm 397 */
												return BgL__ortest_1038z00_919;
											}
										else
											{	/* Ieee/number.scm 397 */
												bool_t BgL__ortest_1039z00_920;

												BgL__ortest_1039z00_920 = BGL_UINT16P(BgL_xz00_31);
												if (BgL__ortest_1039z00_920)
													{	/* Ieee/number.scm 397 */
														return BgL__ortest_1039z00_920;
													}
												else
													{	/* Ieee/number.scm 397 */
														bool_t BgL__ortest_1040z00_921;

														BgL__ortest_1040z00_921 = BGL_INT32P(BgL_xz00_31);
														if (BgL__ortest_1040z00_921)
															{	/* Ieee/number.scm 397 */
																return BgL__ortest_1040z00_921;
															}
														else
															{	/* Ieee/number.scm 398 */
																bool_t BgL__ortest_1041z00_922;

																BgL__ortest_1041z00_922 =
																	BGL_UINT32P(BgL_xz00_31);
																if (BgL__ortest_1041z00_922)
																	{	/* Ieee/number.scm 398 */
																		return BgL__ortest_1041z00_922;
																	}
																else
																	{	/* Ieee/number.scm 398 */
																		return BGL_INT64P(BgL_xz00_31);
																	}
															}
													}
											}
									}
							}
					}
			}
		}

	}



/* 2= */
	BGL_EXPORTED_DEF bool_t BGl_2zd3zd3zz__r4_numbers_6_5z00(obj_t BgL_xz00_33,
		obj_t BgL_yz00_34)
	{
		{	/* Ieee/number.scm 529 */
			if (INTEGERP(BgL_xz00_33))
				{	/* Ieee/number.scm 530 */
					if (INTEGERP(BgL_yz00_34))
						{	/* Ieee/number.scm 530 */
							return ((long) CINT(BgL_xz00_33) == (long) CINT(BgL_yz00_34));
						}
					else
						{	/* Ieee/number.scm 530 */
							if (REALP(BgL_yz00_34))
								{	/* Ieee/number.scm 530 */
									return
										(
										(double) (
											(long) CINT(BgL_xz00_33)) == REAL_TO_DOUBLE(BgL_yz00_34));
								}
							else
								{	/* Ieee/number.scm 530 */
									if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_34))
										{	/* Ieee/number.scm 530 */
											long BgL_arg1154z00_928;
											obj_t BgL_arg1157z00_929;

											{	/* Ieee/number.scm 530 */
												long BgL_tmpz00_5613;

												BgL_tmpz00_5613 = (long) CINT(BgL_xz00_33);
												BgL_arg1154z00_928 = (long) (BgL_tmpz00_5613);
											}
											BgL_arg1157z00_929 =
												BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
												(BgL_yz00_34);
											{	/* Ieee/number.scm 530 */
												long BgL_n2z00_2863;

												BgL_n2z00_2863 = BELONG_TO_LONG(BgL_arg1157z00_929);
												return (BgL_arg1154z00_928 == BgL_n2z00_2863);
											}
										}
									else
										{	/* Ieee/number.scm 530 */
											if (LLONGP(BgL_yz00_34))
												{	/* Ieee/number.scm 530 */
													BGL_LONGLONG_T BgL_arg1162z00_931;

													{	/* Ieee/number.scm 530 */
														long BgL_tmpz00_5621;

														BgL_tmpz00_5621 = (long) CINT(BgL_xz00_33);
														BgL_arg1162z00_931 = LONG_TO_LLONG(BgL_tmpz00_5621);
													}
													return
														(BgL_arg1162z00_931 ==
														BLLONG_TO_LLONG(BgL_yz00_34));
												}
											else
												{	/* Ieee/number.scm 530 */
													if (BGL_UINT64P(BgL_yz00_34))
														{	/* Ieee/number.scm 530 */
															uint64_t BgL_arg1166z00_934;

															{	/* Ieee/number.scm 530 */
																BGL_LONGLONG_T BgL_arg1171z00_935;

																{	/* Ieee/number.scm 530 */
																	long BgL_tmpz00_5628;

																	BgL_tmpz00_5628 = (long) CINT(BgL_xz00_33);
																	BgL_arg1171z00_935 =
																		LONG_TO_LLONG(BgL_tmpz00_5628);
																}
																BgL_arg1166z00_934 =
																	(uint64_t) (BgL_arg1171z00_935);
															}
															{	/* Ieee/number.scm 530 */
																uint64_t BgL_n2z00_2870;

																BgL_n2z00_2870 =
																	BGL_BINT64_TO_INT64(BgL_yz00_34);
																return (BgL_arg1166z00_934 == BgL_n2z00_2870);
															}
														}
													else
														{	/* Ieee/number.scm 530 */
															if (BIGNUMP(BgL_yz00_34))
																{	/* Ieee/number.scm 530 */
																	return
																		(
																		(long) (bgl_bignum_cmp(bgl_long_to_bignum(
																					(long) CINT(BgL_xz00_33)),
																				BgL_yz00_34)) == 0L);
																}
															else
																{	/* Ieee/number.scm 530 */
																	return
																		CBOOL(BGl_errorz00zz__errorz00
																		(BGl_string3327z00zz__r4_numbers_6_5z00,
																			BGl_string3328z00zz__r4_numbers_6_5z00,
																			BgL_yz00_34));
																}
														}
												}
										}
								}
						}
				}
			else
				{	/* Ieee/number.scm 530 */
					if (REALP(BgL_xz00_33))
						{	/* Ieee/number.scm 530 */
							if (REALP(BgL_yz00_34))
								{	/* Ieee/number.scm 530 */
									return
										(REAL_TO_DOUBLE(BgL_xz00_33) ==
										REAL_TO_DOUBLE(BgL_yz00_34));
								}
							else
								{	/* Ieee/number.scm 530 */
									if (INTEGERP(BgL_yz00_34))
										{	/* Ieee/number.scm 530 */
											return
												(REAL_TO_DOUBLE(BgL_xz00_33) ==
												(double) ((long) CINT(BgL_yz00_34)));
										}
									else
										{	/* Ieee/number.scm 530 */
											if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00
												(BgL_yz00_34))
												{	/* Ieee/number.scm 530 */
													double BgL_arg1189z00_943;

													{	/* Ieee/number.scm 530 */
														obj_t BgL_arg1190z00_944;

														BgL_arg1190z00_944 =
															BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
															(BgL_yz00_34);
														BgL_arg1189z00_943 =
															(double) (BELONG_TO_LONG(BgL_arg1190z00_944));
													}
													return
														(REAL_TO_DOUBLE(BgL_xz00_33) == BgL_arg1189z00_943);
												}
											else
												{	/* Ieee/number.scm 530 */
													if (LLONGP(BgL_yz00_34))
														{	/* Ieee/number.scm 530 */
															return
																(REAL_TO_DOUBLE(BgL_xz00_33) ==
																(double) (BLLONG_TO_LLONG(BgL_yz00_34)));
														}
													else
														{	/* Ieee/number.scm 530 */
															if (BGL_UINT64P(BgL_yz00_34))
																{	/* Ieee/number.scm 530 */
																	double BgL_arg1196z00_949;

																	{	/* Ieee/number.scm 530 */
																		uint64_t BgL_xz00_2884;

																		BgL_xz00_2884 =
																			BGL_BINT64_TO_INT64(BgL_yz00_34);
																		BgL_arg1196z00_949 =
																			(double) (BgL_xz00_2884);
																	}
																	return
																		(REAL_TO_DOUBLE(BgL_xz00_33) ==
																		BgL_arg1196z00_949);
																}
															else
																{	/* Ieee/number.scm 530 */
																	if (BIGNUMP(BgL_yz00_34))
																		{	/* Ieee/number.scm 530 */
																			return
																				(REAL_TO_DOUBLE(BgL_xz00_33) ==
																				bgl_bignum_to_flonum(BgL_yz00_34));
																		}
																	else
																		{	/* Ieee/number.scm 530 */
																			return
																				CBOOL(BGl_errorz00zz__errorz00
																				(BGl_string3327z00zz__r4_numbers_6_5z00,
																					BGl_string3328z00zz__r4_numbers_6_5z00,
																					BgL_yz00_34));
																		}
																}
														}
												}
										}
								}
						}
					else
						{	/* Ieee/number.scm 530 */
							if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_xz00_33))
								{	/* Ieee/number.scm 530 */
									if (INTEGERP(BgL_yz00_34))
										{	/* Ieee/number.scm 530 */
											obj_t BgL_arg1201z00_954;
											long BgL_arg1202z00_955;

											BgL_arg1201z00_954 =
												BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
												(BgL_xz00_33);
											{	/* Ieee/number.scm 530 */
												long BgL_tmpz00_5687;

												BgL_tmpz00_5687 = (long) CINT(BgL_yz00_34);
												BgL_arg1202z00_955 = (long) (BgL_tmpz00_5687);
											}
											{	/* Ieee/number.scm 530 */
												long BgL_n1z00_2891;

												BgL_n1z00_2891 = BELONG_TO_LONG(BgL_arg1201z00_954);
												return (BgL_n1z00_2891 == BgL_arg1202z00_955);
											}
										}
									else
										{	/* Ieee/number.scm 530 */
											if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00
												(BgL_yz00_34))
												{	/* Ieee/number.scm 530 */
													obj_t BgL_arg1206z00_957;
													obj_t BgL_arg1208z00_958;

													BgL_arg1206z00_957 =
														BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
														(BgL_xz00_33);
													BgL_arg1208z00_958 =
														BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
														(BgL_yz00_34);
													{	/* Ieee/number.scm 530 */
														long BgL_n1z00_2893;
														long BgL_n2z00_2894;

														BgL_n1z00_2893 = BELONG_TO_LONG(BgL_arg1206z00_957);
														BgL_n2z00_2894 = BELONG_TO_LONG(BgL_arg1208z00_958);
														return (BgL_n1z00_2893 == BgL_n2z00_2894);
													}
												}
											else
												{	/* Ieee/number.scm 530 */
													if (REALP(BgL_yz00_34))
														{	/* Ieee/number.scm 530 */
															double BgL_arg1210z00_960;

															{	/* Ieee/number.scm 530 */
																obj_t BgL_arg1212z00_961;

																BgL_arg1212z00_961 =
																	BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																	(BgL_xz00_33);
																BgL_arg1210z00_960 =
																	(double) (BELONG_TO_LONG(BgL_arg1212z00_961));
															}
															return
																(BgL_arg1210z00_960 ==
																REAL_TO_DOUBLE(BgL_yz00_34));
														}
													else
														{	/* Ieee/number.scm 530 */
															if (LLONGP(BgL_yz00_34))
																{	/* Ieee/number.scm 530 */
																	BGL_LONGLONG_T BgL_arg1215z00_963;

																	{	/* Ieee/number.scm 530 */
																		obj_t BgL_arg1218z00_965;

																		BgL_arg1218z00_965 =
																			BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																			(BgL_xz00_33);
																		BgL_arg1215z00_963 =
																			(BGL_LONGLONG_T) (BELONG_TO_LONG
																			(BgL_arg1218z00_965));
																	}
																	return
																		(BgL_arg1215z00_963 ==
																		BLLONG_TO_LLONG(BgL_yz00_34));
																}
															else
																{	/* Ieee/number.scm 530 */
																	if (BGL_UINT64P(BgL_yz00_34))
																		{	/* Ieee/number.scm 530 */
																			uint64_t BgL_arg1220z00_967;

																			{	/* Ieee/number.scm 530 */
																				BGL_LONGLONG_T BgL_arg1221z00_968;

																				{	/* Ieee/number.scm 530 */
																					obj_t BgL_arg1223z00_969;

																					BgL_arg1223z00_969 =
																						BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																						(BgL_xz00_33);
																					BgL_arg1221z00_968 =
																						(BGL_LONGLONG_T) (BELONG_TO_LONG
																						(BgL_arg1223z00_969));
																				}
																				BgL_arg1220z00_967 =
																					(uint64_t) (BgL_arg1221z00_968);
																			}
																			{	/* Ieee/number.scm 530 */
																				uint64_t BgL_n2z00_2901;

																				BgL_n2z00_2901 =
																					BGL_BINT64_TO_INT64(BgL_yz00_34);
																				return
																					(BgL_arg1220z00_967 ==
																					BgL_n2z00_2901);
																			}
																		}
																	else
																		{	/* Ieee/number.scm 530 */
																			if (BIGNUMP(BgL_yz00_34))
																				{	/* Ieee/number.scm 530 */
																					obj_t BgL_arg1225z00_971;

																					{	/* Ieee/number.scm 530 */
																						obj_t BgL_arg1226z00_972;

																						BgL_arg1226z00_972 =
																							BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																							(BgL_xz00_33);
																						{	/* Ieee/number.scm 530 */
																							long BgL_xz00_2902;

																							BgL_xz00_2902 =
																								BELONG_TO_LONG
																								(BgL_arg1226z00_972);
																							BgL_arg1225z00_971 =
																								bgl_long_to_bignum
																								(BgL_xz00_2902);
																					}}
																					return
																						(
																						(long) (bgl_bignum_cmp
																							(BgL_arg1225z00_971,
																								BgL_yz00_34)) == 0L);
																				}
																			else
																				{	/* Ieee/number.scm 530 */
																					return
																						CBOOL(BGl_errorz00zz__errorz00
																						(BGl_string3327z00zz__r4_numbers_6_5z00,
																							BGl_string3328z00zz__r4_numbers_6_5z00,
																							BgL_yz00_34));
																				}
																		}
																}
														}
												}
										}
								}
							else
								{	/* Ieee/number.scm 530 */
									if (LLONGP(BgL_xz00_33))
										{	/* Ieee/number.scm 530 */
											if (INTEGERP(BgL_yz00_34))
												{	/* Ieee/number.scm 530 */
													BGL_LONGLONG_T BgL_arg1230z00_976;

													{	/* Ieee/number.scm 530 */
														long BgL_tmpz00_5735;

														BgL_tmpz00_5735 = (long) CINT(BgL_yz00_34);
														BgL_arg1230z00_976 = LONG_TO_LLONG(BgL_tmpz00_5735);
													}
													return
														(BLLONG_TO_LLONG(BgL_xz00_33) ==
														BgL_arg1230z00_976);
												}
											else
												{	/* Ieee/number.scm 530 */
													if (REALP(BgL_yz00_34))
														{	/* Ieee/number.scm 530 */
															return
																(
																(double) (BLLONG_TO_LLONG(BgL_xz00_33)) ==
																REAL_TO_DOUBLE(BgL_yz00_34));
														}
													else
														{	/* Ieee/number.scm 530 */
															if (LLONGP(BgL_yz00_34))
																{	/* Ieee/number.scm 530 */
																	return
																		(BLLONG_TO_LLONG(BgL_xz00_33) ==
																		BLLONG_TO_LLONG(BgL_yz00_34));
																}
															else
																{	/* Ieee/number.scm 530 */
																	if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00
																		(BgL_yz00_34))
																		{	/* Ieee/number.scm 530 */
																			BGL_LONGLONG_T BgL_arg1239z00_984;

																			{	/* Ieee/number.scm 530 */
																				obj_t BgL_arg1242z00_985;

																				BgL_arg1242z00_985 =
																					BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																					(BgL_yz00_34);
																				BgL_arg1239z00_984 =
																					(BGL_LONGLONG_T) (BELONG_TO_LONG
																					(BgL_arg1242z00_985));
																			}
																			return
																				(BLLONG_TO_LLONG(BgL_xz00_33) ==
																				BgL_arg1239z00_984);
																		}
																	else
																		{	/* Ieee/number.scm 530 */
																			if (BIGNUMP(BgL_yz00_34))
																				{	/* Ieee/number.scm 530 */
																					return
																						(
																						(long) (bgl_bignum_cmp
																							(bgl_llong_to_bignum
																								(BLLONG_TO_LLONG(BgL_xz00_33)),
																								BgL_yz00_34)) == 0L);
																				}
																			else
																				{	/* Ieee/number.scm 530 */
																					if (BGL_UINT64P(BgL_yz00_34))
																						{	/* Ieee/number.scm 530 */
																							uint64_t BgL_arg1252z00_990;

																							{	/* Ieee/number.scm 530 */
																								BGL_LONGLONG_T BgL_tmpz00_5767;

																								BgL_tmpz00_5767 =
																									BLLONG_TO_LLONG(BgL_xz00_33);
																								BgL_arg1252z00_990 =
																									(uint64_t) (BgL_tmpz00_5767);
																							}
																							{	/* Ieee/number.scm 530 */
																								uint64_t BgL_n2z00_2923;

																								BgL_n2z00_2923 =
																									BGL_BINT64_TO_INT64
																									(BgL_yz00_34);
																								return (BgL_arg1252z00_990 ==
																									BgL_n2z00_2923);
																							}
																						}
																					else
																						{	/* Ieee/number.scm 530 */
																							return
																								CBOOL(BGl_errorz00zz__errorz00
																								(BGl_string3327z00zz__r4_numbers_6_5z00,
																									BGl_string3328z00zz__r4_numbers_6_5z00,
																									BgL_yz00_34));
																						}
																				}
																		}
																}
														}
												}
										}
									else
										{	/* Ieee/number.scm 530 */
											if (BGL_UINT64P(BgL_xz00_33))
												{	/* Ieee/number.scm 530 */
													if (INTEGERP(BgL_yz00_34))
														{	/* Ieee/number.scm 530 */
															uint64_t BgL_arg1268z00_993;

															{	/* Ieee/number.scm 530 */
																long BgL_tmpz00_5778;

																BgL_tmpz00_5778 = (long) CINT(BgL_yz00_34);
																BgL_arg1268z00_993 =
																	(uint64_t) (BgL_tmpz00_5778);
															}
															{	/* Ieee/number.scm 530 */
																uint64_t BgL_n1z00_2925;

																BgL_n1z00_2925 =
																	BGL_BINT64_TO_INT64(BgL_xz00_33);
																return (BgL_n1z00_2925 == BgL_arg1268z00_993);
															}
														}
													else
														{	/* Ieee/number.scm 530 */
															if (BGL_UINT64P(BgL_yz00_34))
																{	/* Ieee/number.scm 530 */
																	uint64_t BgL_n1z00_2927;
																	uint64_t BgL_n2z00_2928;

																	BgL_n1z00_2927 =
																		BGL_BINT64_TO_INT64(BgL_xz00_33);
																	BgL_n2z00_2928 =
																		BGL_BINT64_TO_INT64(BgL_yz00_34);
																	return (BgL_n1z00_2927 == BgL_n2z00_2928);
																}
															else
																{	/* Ieee/number.scm 530 */
																	if (REALP(BgL_yz00_34))
																		{	/* Ieee/number.scm 530 */
																			double BgL_arg1272z00_996;

																			{	/* Ieee/number.scm 530 */
																				uint64_t BgL_tmpz00_5790;

																				BgL_tmpz00_5790 =
																					BGL_BINT64_TO_INT64(BgL_xz00_33);
																				BgL_arg1272z00_996 =
																					(double) (BgL_tmpz00_5790);
																			}
																			return
																				(BgL_arg1272z00_996 ==
																				REAL_TO_DOUBLE(BgL_yz00_34));
																		}
																	else
																		{	/* Ieee/number.scm 530 */
																			if (LLONGP(BgL_yz00_34))
																				{	/* Ieee/number.scm 530 */
																					uint64_t BgL_arg1284z00_998;

																					{	/* Ieee/number.scm 530 */
																						BGL_LONGLONG_T BgL_tmpz00_5797;

																						BgL_tmpz00_5797 =
																							BLLONG_TO_LLONG(BgL_yz00_34);
																						BgL_arg1284z00_998 =
																							(uint64_t) (BgL_tmpz00_5797);
																					}
																					{	/* Ieee/number.scm 530 */
																						uint64_t BgL_n1z00_2932;

																						BgL_n1z00_2932 =
																							BGL_BINT64_TO_INT64(BgL_xz00_33);
																						return
																							(BgL_n1z00_2932 ==
																							BgL_arg1284z00_998);
																					}
																				}
																			else
																				{	/* Ieee/number.scm 530 */
																					if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_34))
																						{	/* Ieee/number.scm 530 */
																							uint64_t BgL_arg1306z00_1001;

																							{	/* Ieee/number.scm 530 */
																								BGL_LONGLONG_T
																									BgL_arg1307z00_1002;
																								{	/* Ieee/number.scm 530 */
																									obj_t BgL_arg1308z00_1003;

																									BgL_arg1308z00_1003 =
																										BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																										(BgL_yz00_34);
																									BgL_arg1307z00_1002 =
																										(BGL_LONGLONG_T)
																										(BELONG_TO_LONG
																										(BgL_arg1308z00_1003));
																								}
																								BgL_arg1306z00_1001 =
																									(uint64_t)
																									(BgL_arg1307z00_1002);
																							}
																							{	/* Ieee/number.scm 530 */
																								uint64_t BgL_n1z00_2935;

																								BgL_n1z00_2935 =
																									BGL_BINT64_TO_INT64
																									(BgL_xz00_33);
																								return (BgL_n1z00_2935 ==
																									BgL_arg1306z00_1001);
																							}
																						}
																					else
																						{	/* Ieee/number.scm 530 */
																							if (BIGNUMP(BgL_yz00_34))
																								{	/* Ieee/number.scm 530 */
																									long BgL_n1z00_2940;

																									BgL_n1z00_2940 =
																										(long) (bgl_bignum_cmp
																										(bgl_uint64_to_bignum
																											(BGL_BINT64_TO_INT64
																												(BgL_xz00_33)),
																											BgL_yz00_34));
																									return (BgL_n1z00_2940 == 0L);
																								}
																							else
																								{	/* Ieee/number.scm 530 */
																									return
																										CBOOL
																										(BGl_errorz00zz__errorz00
																										(BGl_string3327z00zz__r4_numbers_6_5z00,
																											BGl_string3329z00zz__r4_numbers_6_5z00,
																											BgL_yz00_34));
																								}
																						}
																				}
																		}
																}
														}
												}
											else
												{	/* Ieee/number.scm 530 */
													if (BIGNUMP(BgL_xz00_33))
														{	/* Ieee/number.scm 530 */
															if (BIGNUMP(BgL_yz00_34))
																{	/* Ieee/number.scm 530 */
																	return
																		(
																		(long) (bgl_bignum_cmp(BgL_xz00_33,
																				BgL_yz00_34)) == 0L);
																}
															else
																{	/* Ieee/number.scm 530 */
																	if (INTEGERP(BgL_yz00_34))
																		{	/* Ieee/number.scm 530 */
																			return
																				(
																				(long) (bgl_bignum_cmp(BgL_xz00_33,
																						bgl_long_to_bignum(
																							(long) CINT(BgL_yz00_34)))) ==
																				0L);
																		}
																	else
																		{	/* Ieee/number.scm 530 */
																			if (REALP(BgL_yz00_34))
																				{	/* Ieee/number.scm 530 */
																					return
																						(bgl_bignum_to_flonum(BgL_xz00_33)
																						== REAL_TO_DOUBLE(BgL_yz00_34));
																				}
																			else
																				{	/* Ieee/number.scm 530 */
																					if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_34))
																						{	/* Ieee/number.scm 530 */
																							obj_t BgL_arg1318z00_1013;

																							{	/* Ieee/number.scm 530 */
																								obj_t BgL_arg1319z00_1014;

																								BgL_arg1319z00_1014 =
																									BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																									(BgL_yz00_34);
																								{	/* Ieee/number.scm 530 */
																									long BgL_xz00_2953;

																									BgL_xz00_2953 =
																										BELONG_TO_LONG
																										(BgL_arg1319z00_1014);
																									BgL_arg1318z00_1013 =
																										bgl_long_to_bignum
																										(BgL_xz00_2953);
																							}}
																							return
																								(
																								(long) (bgl_bignum_cmp
																									(BgL_xz00_33,
																										BgL_arg1318z00_1013)) ==
																								0L);
																						}
																					else
																						{	/* Ieee/number.scm 530 */
																							if (LLONGP(BgL_yz00_34))
																								{	/* Ieee/number.scm 530 */
																									return
																										(
																										(long) (bgl_bignum_cmp
																											(BgL_xz00_33,
																												bgl_llong_to_bignum
																												(BLLONG_TO_LLONG
																													(BgL_yz00_34)))) ==
																										0L);
																								}
																							else
																								{	/* Ieee/number.scm 530 */
																									if (BGL_UINT64P(BgL_yz00_34))
																										{	/* Ieee/number.scm 530 */
																											long BgL_n1z00_2966;

																											BgL_n1z00_2966 =
																												(long) (bgl_bignum_cmp
																												(BgL_xz00_33,
																													bgl_uint64_to_bignum
																													(BGL_BINT64_TO_INT64
																														(BgL_yz00_34))));
																											return (BgL_n1z00_2966 ==
																												0L);
																										}
																									else
																										{	/* Ieee/number.scm 530 */
																											return
																												CBOOL
																												(BGl_errorz00zz__errorz00
																												(BGl_string3327z00zz__r4_numbers_6_5z00,
																													BGl_string3328z00zz__r4_numbers_6_5z00,
																													BgL_yz00_34));
																										}
																								}
																						}
																				}
																		}
																}
														}
													else
														{	/* Ieee/number.scm 530 */
															return
																CBOOL(BGl_errorz00zz__errorz00
																(BGl_string3327z00zz__r4_numbers_6_5z00,
																	BGl_string3328z00zz__r4_numbers_6_5z00,
																	BgL_xz00_33));
														}
												}
										}
								}
						}
				}
		}

	}



/* &2= */
	obj_t BGl_z622zd3zb1zz__r4_numbers_6_5z00(obj_t BgL_envz00_5006,
		obj_t BgL_xz00_5007, obj_t BgL_yz00_5008)
	{
		{	/* Ieee/number.scm 529 */
			return
				BBOOL(BGl_2zd3zd3zz__r4_numbers_6_5z00(BgL_xz00_5007, BgL_yz00_5008));
		}

	}



/* = */
	BGL_EXPORTED_DEF bool_t BGl_zd3zd3zz__r4_numbers_6_5z00(obj_t BgL_xz00_35,
		obj_t BgL_yz00_36, obj_t BgL_za7za7_37)
	{
		{	/* Ieee/number.scm 535 */
			if (BGl_2zd3zd3zz__r4_numbers_6_5z00(BgL_xz00_35, BgL_yz00_36))
				{
					obj_t BgL_za7za7_2988;

					BgL_za7za7_2988 = BgL_za7za7_37;
				BgL_zd3zd2listz01_2987:
					if (NULLP(BgL_za7za7_2988))
						{	/* Ieee/number.scm 538 */
							return ((bool_t) 1);
						}
					else
						{	/* Ieee/number.scm 539 */
							bool_t BgL_test3635z00_5870;

							{	/* Ieee/number.scm 539 */
								obj_t BgL_arg1332z00_2991;

								BgL_arg1332z00_2991 = CAR(((obj_t) BgL_za7za7_2988));
								BgL_test3635z00_5870 =
									BGl_2zd3zd3zz__r4_numbers_6_5z00(BgL_yz00_36,
									BgL_arg1332z00_2991);
							}
							if (BgL_test3635z00_5870)
								{
									obj_t BgL_za7za7_5874;

									BgL_za7za7_5874 = CDR(((obj_t) BgL_za7za7_2988));
									BgL_za7za7_2988 = BgL_za7za7_5874;
									goto BgL_zd3zd2listz01_2987;
								}
							else
								{	/* Ieee/number.scm 539 */
									return ((bool_t) 0);
								}
						}
				}
			else
				{	/* Ieee/number.scm 541 */
					return ((bool_t) 0);
				}
		}

	}



/* &= */
	obj_t BGl_z62zd3zb1zz__r4_numbers_6_5z00(obj_t BgL_envz00_5009,
		obj_t BgL_xz00_5010, obj_t BgL_yz00_5011, obj_t BgL_za7za7_5012)
	{
		{	/* Ieee/number.scm 535 */
			return
				BBOOL(BGl_zd3zd3zz__r4_numbers_6_5z00(BgL_xz00_5010, BgL_yz00_5011,
					BgL_za7za7_5012));
		}

	}



/* 2< */
	BGL_EXPORTED_DEF bool_t BGl_2zc3zc3zz__r4_numbers_6_5z00(obj_t BgL_xz00_38,
		obj_t BgL_yz00_39)
	{
		{	/* Ieee/number.scm 546 */
			if (INTEGERP(BgL_xz00_38))
				{	/* Ieee/number.scm 547 */
					if (INTEGERP(BgL_yz00_39))
						{	/* Ieee/number.scm 547 */
							return ((long) CINT(BgL_xz00_38) < (long) CINT(BgL_yz00_39));
						}
					else
						{	/* Ieee/number.scm 547 */
							if (REALP(BgL_yz00_39))
								{	/* Ieee/number.scm 547 */
									return
										(
										(double) (
											(long) CINT(BgL_xz00_38)) < REAL_TO_DOUBLE(BgL_yz00_39));
								}
							else
								{	/* Ieee/number.scm 547 */
									if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_39))
										{	/* Ieee/number.scm 547 */
											long BgL_arg1338z00_1036;
											obj_t BgL_arg1339z00_1037;

											{	/* Ieee/number.scm 547 */
												long BgL_tmpz00_5894;

												BgL_tmpz00_5894 = (long) CINT(BgL_xz00_38);
												BgL_arg1338z00_1036 = (long) (BgL_tmpz00_5894);
											}
											BgL_arg1339z00_1037 =
												BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
												(BgL_yz00_39);
											{	/* Ieee/number.scm 547 */
												long BgL_n2z00_3001;

												BgL_n2z00_3001 = BELONG_TO_LONG(BgL_arg1339z00_1037);
												return (BgL_arg1338z00_1036 < BgL_n2z00_3001);
											}
										}
									else
										{	/* Ieee/number.scm 547 */
											if (LLONGP(BgL_yz00_39))
												{	/* Ieee/number.scm 547 */
													BGL_LONGLONG_T BgL_arg1341z00_1039;

													{	/* Ieee/number.scm 547 */
														long BgL_tmpz00_5902;

														BgL_tmpz00_5902 = (long) CINT(BgL_xz00_38);
														BgL_arg1341z00_1039 =
															LONG_TO_LLONG(BgL_tmpz00_5902);
													}
													return
														(BgL_arg1341z00_1039 <
														BLLONG_TO_LLONG(BgL_yz00_39));
												}
											else
												{	/* Ieee/number.scm 547 */
													if (BGL_UINT64P(BgL_yz00_39))
														{	/* Ieee/number.scm 547 */
															uint64_t BgL_arg1344z00_1042;

															{	/* Ieee/number.scm 547 */
																BGL_LONGLONG_T BgL_arg1346z00_1043;

																{	/* Ieee/number.scm 547 */
																	long BgL_tmpz00_5909;

																	BgL_tmpz00_5909 = (long) CINT(BgL_xz00_38);
																	BgL_arg1346z00_1043 =
																		LONG_TO_LLONG(BgL_tmpz00_5909);
																}
																BgL_arg1344z00_1042 =
																	(uint64_t) (BgL_arg1346z00_1043);
															}
															{	/* Ieee/number.scm 547 */
																uint64_t BgL_n2z00_3008;

																BgL_n2z00_3008 =
																	BGL_BINT64_TO_INT64(BgL_yz00_39);
																return (BgL_arg1344z00_1042 < BgL_n2z00_3008);
															}
														}
													else
														{	/* Ieee/number.scm 547 */
															if (BIGNUMP(BgL_yz00_39))
																{	/* Ieee/number.scm 547 */
																	return
																		(
																		(long) (bgl_bignum_cmp(bgl_long_to_bignum(
																					(long) CINT(BgL_xz00_38)),
																				BgL_yz00_39)) < 0L);
																}
															else
																{	/* Ieee/number.scm 547 */
																	return
																		CBOOL(BGl_errorz00zz__errorz00
																		(BGl_string3330z00zz__r4_numbers_6_5z00,
																			BGl_string3328z00zz__r4_numbers_6_5z00,
																			BgL_yz00_39));
																}
														}
												}
										}
								}
						}
				}
			else
				{	/* Ieee/number.scm 547 */
					if (REALP(BgL_xz00_38))
						{	/* Ieee/number.scm 547 */
							if (REALP(BgL_yz00_39))
								{	/* Ieee/number.scm 547 */
									return
										(REAL_TO_DOUBLE(BgL_xz00_38) < REAL_TO_DOUBLE(BgL_yz00_39));
								}
							else
								{	/* Ieee/number.scm 547 */
									if (INTEGERP(BgL_yz00_39))
										{	/* Ieee/number.scm 547 */
											return
												(REAL_TO_DOUBLE(BgL_xz00_38) <
												(double) ((long) CINT(BgL_yz00_39)));
										}
									else
										{	/* Ieee/number.scm 547 */
											if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00
												(BgL_yz00_39))
												{	/* Ieee/number.scm 547 */
													double BgL_arg1354z00_1051;

													{	/* Ieee/number.scm 547 */
														obj_t BgL_arg1356z00_1052;

														BgL_arg1356z00_1052 =
															BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
															(BgL_yz00_39);
														BgL_arg1354z00_1051 =
															(double) (BELONG_TO_LONG(BgL_arg1356z00_1052));
													}
													return
														(REAL_TO_DOUBLE(BgL_xz00_38) < BgL_arg1354z00_1051);
												}
											else
												{	/* Ieee/number.scm 547 */
													if (LLONGP(BgL_yz00_39))
														{	/* Ieee/number.scm 547 */
															return
																(REAL_TO_DOUBLE(BgL_xz00_38) <
																(double) (BLLONG_TO_LLONG(BgL_yz00_39)));
														}
													else
														{	/* Ieee/number.scm 547 */
															if (BGL_UINT64P(BgL_yz00_39))
																{	/* Ieee/number.scm 547 */
																	double BgL_arg1361z00_1057;

																	{	/* Ieee/number.scm 547 */
																		uint64_t BgL_xz00_3022;

																		BgL_xz00_3022 =
																			BGL_BINT64_TO_INT64(BgL_yz00_39);
																		BgL_arg1361z00_1057 =
																			(double) (BgL_xz00_3022);
																	}
																	return
																		(REAL_TO_DOUBLE(BgL_xz00_38) <
																		BgL_arg1361z00_1057);
																}
															else
																{	/* Ieee/number.scm 547 */
																	if (BIGNUMP(BgL_yz00_39))
																		{	/* Ieee/number.scm 547 */
																			return
																				(REAL_TO_DOUBLE(BgL_xz00_38) <
																				bgl_bignum_to_flonum(BgL_yz00_39));
																		}
																	else
																		{	/* Ieee/number.scm 547 */
																			return
																				CBOOL(BGl_errorz00zz__errorz00
																				(BGl_string3330z00zz__r4_numbers_6_5z00,
																					BGl_string3328z00zz__r4_numbers_6_5z00,
																					BgL_yz00_39));
																		}
																}
														}
												}
										}
								}
						}
					else
						{	/* Ieee/number.scm 547 */
							if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_xz00_38))
								{	/* Ieee/number.scm 547 */
									if (INTEGERP(BgL_yz00_39))
										{	/* Ieee/number.scm 547 */
											obj_t BgL_arg1366z00_1062;
											long BgL_arg1367z00_1063;

											BgL_arg1366z00_1062 =
												BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
												(BgL_xz00_38);
											{	/* Ieee/number.scm 547 */
												long BgL_tmpz00_5968;

												BgL_tmpz00_5968 = (long) CINT(BgL_yz00_39);
												BgL_arg1367z00_1063 = (long) (BgL_tmpz00_5968);
											}
											{	/* Ieee/number.scm 547 */
												long BgL_n1z00_3029;

												BgL_n1z00_3029 = BELONG_TO_LONG(BgL_arg1366z00_1062);
												return (BgL_n1z00_3029 < BgL_arg1367z00_1063);
											}
										}
									else
										{	/* Ieee/number.scm 547 */
											if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00
												(BgL_yz00_39))
												{	/* Ieee/number.scm 547 */
													obj_t BgL_arg1369z00_1065;
													obj_t BgL_arg1370z00_1066;

													BgL_arg1369z00_1065 =
														BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
														(BgL_xz00_38);
													BgL_arg1370z00_1066 =
														BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
														(BgL_yz00_39);
													{	/* Ieee/number.scm 547 */
														long BgL_n1z00_3031;
														long BgL_n2z00_3032;

														BgL_n1z00_3031 =
															BELONG_TO_LONG(BgL_arg1369z00_1065);
														BgL_n2z00_3032 =
															BELONG_TO_LONG(BgL_arg1370z00_1066);
														return (BgL_n1z00_3031 < BgL_n2z00_3032);
													}
												}
											else
												{	/* Ieee/number.scm 547 */
													if (REALP(BgL_yz00_39))
														{	/* Ieee/number.scm 547 */
															double BgL_arg1372z00_1068;

															{	/* Ieee/number.scm 547 */
																obj_t BgL_arg1373z00_1069;

																BgL_arg1373z00_1069 =
																	BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																	(BgL_xz00_38);
																BgL_arg1372z00_1068 =
																	(double) (BELONG_TO_LONG
																	(BgL_arg1373z00_1069));
															}
															return
																(BgL_arg1372z00_1068 <
																REAL_TO_DOUBLE(BgL_yz00_39));
														}
													else
														{	/* Ieee/number.scm 547 */
															if (LLONGP(BgL_yz00_39))
																{	/* Ieee/number.scm 547 */
																	BGL_LONGLONG_T BgL_arg1375z00_1071;

																	{	/* Ieee/number.scm 547 */
																		obj_t BgL_arg1377z00_1073;

																		BgL_arg1377z00_1073 =
																			BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																			(BgL_xz00_38);
																		BgL_arg1375z00_1071 =
																			(BGL_LONGLONG_T) (BELONG_TO_LONG
																			(BgL_arg1377z00_1073));
																	}
																	return
																		(BgL_arg1375z00_1071 <
																		BLLONG_TO_LLONG(BgL_yz00_39));
																}
															else
																{	/* Ieee/number.scm 547 */
																	if (BGL_UINT64P(BgL_yz00_39))
																		{	/* Ieee/number.scm 547 */
																			uint64_t BgL_arg1379z00_1075;

																			{	/* Ieee/number.scm 547 */
																				BGL_LONGLONG_T BgL_arg1380z00_1076;

																				{	/* Ieee/number.scm 547 */
																					obj_t BgL_arg1382z00_1077;

																					BgL_arg1382z00_1077 =
																						BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																						(BgL_xz00_38);
																					BgL_arg1380z00_1076 =
																						(BGL_LONGLONG_T) (BELONG_TO_LONG
																						(BgL_arg1382z00_1077));
																				}
																				BgL_arg1379z00_1075 =
																					(uint64_t) (BgL_arg1380z00_1076);
																			}
																			{	/* Ieee/number.scm 547 */
																				uint64_t BgL_n2z00_3039;

																				BgL_n2z00_3039 =
																					BGL_BINT64_TO_INT64(BgL_yz00_39);
																				return
																					(BgL_arg1379z00_1075 <
																					BgL_n2z00_3039);
																			}
																		}
																	else
																		{	/* Ieee/number.scm 547 */
																			if (BIGNUMP(BgL_yz00_39))
																				{	/* Ieee/number.scm 547 */
																					obj_t BgL_arg1384z00_1079;

																					{	/* Ieee/number.scm 547 */
																						obj_t BgL_arg1387z00_1080;

																						BgL_arg1387z00_1080 =
																							BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																							(BgL_xz00_38);
																						{	/* Ieee/number.scm 547 */
																							long BgL_xz00_3040;

																							BgL_xz00_3040 =
																								BELONG_TO_LONG
																								(BgL_arg1387z00_1080);
																							BgL_arg1384z00_1079 =
																								bgl_long_to_bignum
																								(BgL_xz00_3040);
																					}}
																					return
																						(
																						(long) (bgl_bignum_cmp
																							(BgL_arg1384z00_1079,
																								BgL_yz00_39)) < 0L);
																				}
																			else
																				{	/* Ieee/number.scm 547 */
																					return
																						CBOOL(BGl_errorz00zz__errorz00
																						(BGl_string3330z00zz__r4_numbers_6_5z00,
																							BGl_string3328z00zz__r4_numbers_6_5z00,
																							BgL_yz00_39));
																				}
																		}
																}
														}
												}
										}
								}
							else
								{	/* Ieee/number.scm 547 */
									if (LLONGP(BgL_xz00_38))
										{	/* Ieee/number.scm 547 */
											if (INTEGERP(BgL_yz00_39))
												{	/* Ieee/number.scm 547 */
													BGL_LONGLONG_T BgL_arg1391z00_1084;

													{	/* Ieee/number.scm 547 */
														long BgL_tmpz00_6016;

														BgL_tmpz00_6016 = (long) CINT(BgL_yz00_39);
														BgL_arg1391z00_1084 =
															LONG_TO_LLONG(BgL_tmpz00_6016);
													}
													return
														(BLLONG_TO_LLONG(BgL_xz00_38) <
														BgL_arg1391z00_1084);
												}
											else
												{	/* Ieee/number.scm 547 */
													if (REALP(BgL_yz00_39))
														{	/* Ieee/number.scm 547 */
															return
																(
																(double) (BLLONG_TO_LLONG(BgL_xz00_38)) <
																REAL_TO_DOUBLE(BgL_yz00_39));
														}
													else
														{	/* Ieee/number.scm 547 */
															if (LLONGP(BgL_yz00_39))
																{	/* Ieee/number.scm 547 */
																	return
																		(BLLONG_TO_LLONG(BgL_xz00_38) <
																		BLLONG_TO_LLONG(BgL_yz00_39));
																}
															else
																{	/* Ieee/number.scm 547 */
																	if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00
																		(BgL_yz00_39))
																		{	/* Ieee/number.scm 547 */
																			BGL_LONGLONG_T BgL_arg1400z00_1092;

																			{	/* Ieee/number.scm 547 */
																				obj_t BgL_arg1401z00_1093;

																				BgL_arg1401z00_1093 =
																					BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																					(BgL_yz00_39);
																				BgL_arg1400z00_1092 =
																					(BGL_LONGLONG_T) (BELONG_TO_LONG
																					(BgL_arg1401z00_1093));
																			}
																			return
																				(BLLONG_TO_LLONG(BgL_xz00_38) <
																				BgL_arg1400z00_1092);
																		}
																	else
																		{	/* Ieee/number.scm 547 */
																			if (BIGNUMP(BgL_yz00_39))
																				{	/* Ieee/number.scm 547 */
																					return
																						(
																						(long) (bgl_bignum_cmp
																							(bgl_llong_to_bignum
																								(BLLONG_TO_LLONG(BgL_xz00_38)),
																								BgL_yz00_39)) < 0L);
																				}
																			else
																				{	/* Ieee/number.scm 547 */
																					if (BGL_UINT64P(BgL_yz00_39))
																						{	/* Ieee/number.scm 547 */
																							uint64_t BgL_arg1406z00_1098;

																							{	/* Ieee/number.scm 547 */
																								BGL_LONGLONG_T BgL_tmpz00_6048;

																								BgL_tmpz00_6048 =
																									BLLONG_TO_LLONG(BgL_xz00_38);
																								BgL_arg1406z00_1098 =
																									(uint64_t) (BgL_tmpz00_6048);
																							}
																							{	/* Ieee/number.scm 547 */
																								uint64_t BgL_n2z00_3061;

																								BgL_n2z00_3061 =
																									BGL_BINT64_TO_INT64
																									(BgL_yz00_39);
																								return (BgL_arg1406z00_1098 <
																									BgL_n2z00_3061);
																							}
																						}
																					else
																						{	/* Ieee/number.scm 547 */
																							return
																								CBOOL(BGl_errorz00zz__errorz00
																								(BGl_string3330z00zz__r4_numbers_6_5z00,
																									BGl_string3328z00zz__r4_numbers_6_5z00,
																									BgL_yz00_39));
																						}
																				}
																		}
																}
														}
												}
										}
									else
										{	/* Ieee/number.scm 547 */
											if (BGL_UINT64P(BgL_xz00_38))
												{	/* Ieee/number.scm 547 */
													if (INTEGERP(BgL_yz00_39))
														{	/* Ieee/number.scm 547 */
															uint64_t BgL_arg1410z00_1101;

															{	/* Ieee/number.scm 547 */
																long BgL_tmpz00_6059;

																BgL_tmpz00_6059 = (long) CINT(BgL_yz00_39);
																BgL_arg1410z00_1101 =
																	(uint64_t) (BgL_tmpz00_6059);
															}
															{	/* Ieee/number.scm 547 */
																uint64_t BgL_n1z00_3063;

																BgL_n1z00_3063 =
																	BGL_BINT64_TO_INT64(BgL_xz00_38);
																return (BgL_n1z00_3063 < BgL_arg1410z00_1101);
															}
														}
													else
														{	/* Ieee/number.scm 547 */
															if (BGL_UINT64P(BgL_yz00_39))
																{	/* Ieee/number.scm 547 */
																	uint64_t BgL_n1z00_3065;
																	uint64_t BgL_n2z00_3066;

																	BgL_n1z00_3065 =
																		BGL_BINT64_TO_INT64(BgL_xz00_38);
																	BgL_n2z00_3066 =
																		BGL_BINT64_TO_INT64(BgL_yz00_39);
																	return (BgL_n1z00_3065 < BgL_n2z00_3066);
																}
															else
																{	/* Ieee/number.scm 547 */
																	if (REALP(BgL_yz00_39))
																		{	/* Ieee/number.scm 547 */
																			double BgL_arg1413z00_1104;

																			{	/* Ieee/number.scm 547 */
																				uint64_t BgL_tmpz00_6071;

																				BgL_tmpz00_6071 =
																					BGL_BINT64_TO_INT64(BgL_xz00_38);
																				BgL_arg1413z00_1104 =
																					(double) (BgL_tmpz00_6071);
																			}
																			return
																				(BgL_arg1413z00_1104 <
																				REAL_TO_DOUBLE(BgL_yz00_39));
																		}
																	else
																		{	/* Ieee/number.scm 547 */
																			if (LLONGP(BgL_yz00_39))
																				{	/* Ieee/number.scm 547 */
																					uint64_t BgL_arg1415z00_1106;

																					{	/* Ieee/number.scm 547 */
																						BGL_LONGLONG_T BgL_tmpz00_6078;

																						BgL_tmpz00_6078 =
																							BLLONG_TO_LLONG(BgL_yz00_39);
																						BgL_arg1415z00_1106 =
																							(uint64_t) (BgL_tmpz00_6078);
																					}
																					{	/* Ieee/number.scm 547 */
																						uint64_t BgL_n1z00_3070;

																						BgL_n1z00_3070 =
																							BGL_BINT64_TO_INT64(BgL_xz00_38);
																						return
																							(BgL_n1z00_3070 <
																							BgL_arg1415z00_1106);
																					}
																				}
																			else
																				{	/* Ieee/number.scm 547 */
																					if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_39))
																						{	/* Ieee/number.scm 547 */
																							uint64_t BgL_arg1418z00_1109;

																							{	/* Ieee/number.scm 547 */
																								BGL_LONGLONG_T
																									BgL_arg1419z00_1110;
																								{	/* Ieee/number.scm 547 */
																									obj_t BgL_arg1420z00_1111;

																									BgL_arg1420z00_1111 =
																										BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																										(BgL_yz00_39);
																									BgL_arg1419z00_1110 =
																										(BGL_LONGLONG_T)
																										(BELONG_TO_LONG
																										(BgL_arg1420z00_1111));
																								}
																								BgL_arg1418z00_1109 =
																									(uint64_t)
																									(BgL_arg1419z00_1110);
																							}
																							{	/* Ieee/number.scm 547 */
																								uint64_t BgL_n1z00_3073;

																								BgL_n1z00_3073 =
																									BGL_BINT64_TO_INT64
																									(BgL_xz00_38);
																								return (BgL_n1z00_3073 <
																									BgL_arg1418z00_1109);
																							}
																						}
																					else
																						{	/* Ieee/number.scm 547 */
																							if (BIGNUMP(BgL_yz00_39))
																								{	/* Ieee/number.scm 547 */
																									long BgL_n1z00_3078;

																									BgL_n1z00_3078 =
																										(long) (bgl_bignum_cmp
																										(bgl_uint64_to_bignum
																											(BGL_BINT64_TO_INT64
																												(BgL_xz00_38)),
																											BgL_yz00_39));
																									return (BgL_n1z00_3078 < 0L);
																								}
																							else
																								{	/* Ieee/number.scm 547 */
																									return
																										CBOOL
																										(BGl_errorz00zz__errorz00
																										(BGl_string3330z00zz__r4_numbers_6_5z00,
																											BGl_string3329z00zz__r4_numbers_6_5z00,
																											BgL_yz00_39));
																								}
																						}
																				}
																		}
																}
														}
												}
											else
												{	/* Ieee/number.scm 547 */
													if (BIGNUMP(BgL_xz00_38))
														{	/* Ieee/number.scm 547 */
															if (BIGNUMP(BgL_yz00_39))
																{	/* Ieee/number.scm 547 */
																	return
																		(
																		(long) (bgl_bignum_cmp(BgL_xz00_38,
																				BgL_yz00_39)) < 0L);
																}
															else
																{	/* Ieee/number.scm 547 */
																	if (INTEGERP(BgL_yz00_39))
																		{	/* Ieee/number.scm 547 */
																			return
																				(
																				(long) (bgl_bignum_cmp(BgL_xz00_38,
																						bgl_long_to_bignum(
																							(long) CINT(BgL_yz00_39)))) < 0L);
																		}
																	else
																		{	/* Ieee/number.scm 547 */
																			if (REALP(BgL_yz00_39))
																				{	/* Ieee/number.scm 547 */
																					return
																						(bgl_bignum_to_flonum(BgL_xz00_38) <
																						REAL_TO_DOUBLE(BgL_yz00_39));
																				}
																			else
																				{	/* Ieee/number.scm 547 */
																					if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_39))
																						{	/* Ieee/number.scm 547 */
																							obj_t BgL_arg1430z00_1121;

																							{	/* Ieee/number.scm 547 */
																								obj_t BgL_arg1431z00_1122;

																								BgL_arg1431z00_1122 =
																									BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																									(BgL_yz00_39);
																								{	/* Ieee/number.scm 547 */
																									long BgL_xz00_3091;

																									BgL_xz00_3091 =
																										BELONG_TO_LONG
																										(BgL_arg1431z00_1122);
																									BgL_arg1430z00_1121 =
																										bgl_long_to_bignum
																										(BgL_xz00_3091);
																							}}
																							return
																								(
																								(long) (bgl_bignum_cmp
																									(BgL_xz00_38,
																										BgL_arg1430z00_1121)) < 0L);
																						}
																					else
																						{	/* Ieee/number.scm 547 */
																							if (LLONGP(BgL_yz00_39))
																								{	/* Ieee/number.scm 547 */
																									return
																										(
																										(long) (bgl_bignum_cmp
																											(BgL_xz00_38,
																												bgl_llong_to_bignum
																												(BLLONG_TO_LLONG
																													(BgL_yz00_39)))) <
																										0L);
																								}
																							else
																								{	/* Ieee/number.scm 547 */
																									if (BGL_UINT64P(BgL_yz00_39))
																										{	/* Ieee/number.scm 547 */
																											long BgL_n1z00_3104;

																											BgL_n1z00_3104 =
																												(long) (bgl_bignum_cmp
																												(BgL_xz00_38,
																													bgl_uint64_to_bignum
																													(BGL_BINT64_TO_INT64
																														(BgL_yz00_39))));
																											return (BgL_n1z00_3104 <
																												0L);
																										}
																									else
																										{	/* Ieee/number.scm 547 */
																											return
																												CBOOL
																												(BGl_errorz00zz__errorz00
																												(BGl_string3330z00zz__r4_numbers_6_5z00,
																													BGl_string3328z00zz__r4_numbers_6_5z00,
																													BgL_yz00_39));
																										}
																								}
																						}
																				}
																		}
																}
														}
													else
														{	/* Ieee/number.scm 547 */
															return
																CBOOL(BGl_errorz00zz__errorz00
																(BGl_string3330z00zz__r4_numbers_6_5z00,
																	BGl_string3328z00zz__r4_numbers_6_5z00,
																	BgL_xz00_38));
														}
												}
										}
								}
						}
				}
		}

	}



/* &2< */
	obj_t BGl_z622zc3za1zz__r4_numbers_6_5z00(obj_t BgL_envz00_5013,
		obj_t BgL_xz00_5014, obj_t BgL_yz00_5015)
	{
		{	/* Ieee/number.scm 546 */
			return
				BBOOL(BGl_2zc3zc3zz__r4_numbers_6_5z00(BgL_xz00_5014, BgL_yz00_5015));
		}

	}



/* < */
	BGL_EXPORTED_DEF bool_t BGl_zc3zc3zz__r4_numbers_6_5z00(obj_t BgL_xz00_40,
		obj_t BgL_yz00_41, obj_t BgL_za7za7_42)
	{
		{	/* Ieee/number.scm 552 */
			if (BGl_2zc3zc3zz__r4_numbers_6_5z00(BgL_xz00_40, BgL_yz00_41))
				{
					obj_t BgL_xz00_3122;
					obj_t BgL_za7za7_3123;

					BgL_xz00_3122 = BgL_yz00_41;
					BgL_za7za7_3123 = BgL_za7za7_42;
				BgL_zc3zd2listz11_3121:
					if (NULLP(BgL_za7za7_3123))
						{	/* Ieee/number.scm 555 */
							return ((bool_t) 1);
						}
					else
						{	/* Ieee/number.scm 556 */
							bool_t BgL_test3680z00_6151;

							{	/* Ieee/number.scm 556 */
								obj_t BgL_arg1444z00_3131;

								BgL_arg1444z00_3131 = CAR(((obj_t) BgL_za7za7_3123));
								BgL_test3680z00_6151 =
									BGl_2zc3zc3zz__r4_numbers_6_5z00(BgL_xz00_3122,
									BgL_arg1444z00_3131);
							}
							if (BgL_test3680z00_6151)
								{
									obj_t BgL_za7za7_6158;
									obj_t BgL_xz00_6155;

									BgL_xz00_6155 = CAR(((obj_t) BgL_za7za7_3123));
									BgL_za7za7_6158 = CDR(((obj_t) BgL_za7za7_3123));
									BgL_za7za7_3123 = BgL_za7za7_6158;
									BgL_xz00_3122 = BgL_xz00_6155;
									goto BgL_zc3zd2listz11_3121;
								}
							else
								{	/* Ieee/number.scm 556 */
									return ((bool_t) 0);
								}
						}
				}
			else
				{	/* Ieee/number.scm 558 */
					return ((bool_t) 0);
				}
		}

	}



/* &< */
	obj_t BGl_z62zc3za1zz__r4_numbers_6_5z00(obj_t BgL_envz00_5016,
		obj_t BgL_xz00_5017, obj_t BgL_yz00_5018, obj_t BgL_za7za7_5019)
	{
		{	/* Ieee/number.scm 552 */
			return
				BBOOL(BGl_zc3zc3zz__r4_numbers_6_5z00(BgL_xz00_5017, BgL_yz00_5018,
					BgL_za7za7_5019));
		}

	}



/* 2> */
	BGL_EXPORTED_DEF bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t BgL_xz00_43,
		obj_t BgL_yz00_44)
	{
		{	/* Ieee/number.scm 563 */
			if (INTEGERP(BgL_xz00_43))
				{	/* Ieee/number.scm 564 */
					if (INTEGERP(BgL_yz00_44))
						{	/* Ieee/number.scm 564 */
							return ((long) CINT(BgL_xz00_43) > (long) CINT(BgL_yz00_44));
						}
					else
						{	/* Ieee/number.scm 564 */
							if (REALP(BgL_yz00_44))
								{	/* Ieee/number.scm 564 */
									return
										(
										(double) (
											(long) CINT(BgL_xz00_43)) > REAL_TO_DOUBLE(BgL_yz00_44));
								}
							else
								{	/* Ieee/number.scm 564 */
									if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_44))
										{	/* Ieee/number.scm 564 */
											long BgL_arg1450z00_1145;
											obj_t BgL_arg1451z00_1146;

											{	/* Ieee/number.scm 564 */
												long BgL_tmpz00_6178;

												BgL_tmpz00_6178 = (long) CINT(BgL_xz00_43);
												BgL_arg1450z00_1145 = (long) (BgL_tmpz00_6178);
											}
											BgL_arg1451z00_1146 =
												BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
												(BgL_yz00_44);
											{	/* Ieee/number.scm 564 */
												long BgL_n2z00_3143;

												BgL_n2z00_3143 = BELONG_TO_LONG(BgL_arg1451z00_1146);
												return (BgL_arg1450z00_1145 > BgL_n2z00_3143);
											}
										}
									else
										{	/* Ieee/number.scm 564 */
											if (LLONGP(BgL_yz00_44))
												{	/* Ieee/number.scm 564 */
													BGL_LONGLONG_T BgL_arg1453z00_1148;

													{	/* Ieee/number.scm 564 */
														long BgL_tmpz00_6186;

														BgL_tmpz00_6186 = (long) CINT(BgL_xz00_43);
														BgL_arg1453z00_1148 =
															LONG_TO_LLONG(BgL_tmpz00_6186);
													}
													return
														(BgL_arg1453z00_1148 >
														BLLONG_TO_LLONG(BgL_yz00_44));
												}
											else
												{	/* Ieee/number.scm 564 */
													if (BGL_UINT64P(BgL_yz00_44))
														{	/* Ieee/number.scm 564 */
															uint64_t BgL_arg1456z00_1151;

															{	/* Ieee/number.scm 564 */
																BGL_LONGLONG_T BgL_arg1457z00_1152;

																{	/* Ieee/number.scm 564 */
																	long BgL_tmpz00_6193;

																	BgL_tmpz00_6193 = (long) CINT(BgL_xz00_43);
																	BgL_arg1457z00_1152 =
																		LONG_TO_LLONG(BgL_tmpz00_6193);
																}
																BgL_arg1456z00_1151 =
																	(uint64_t) (BgL_arg1457z00_1152);
															}
															{	/* Ieee/number.scm 564 */
																uint64_t BgL_n2z00_3150;

																BgL_n2z00_3150 =
																	BGL_BINT64_TO_INT64(BgL_yz00_44);
																return (BgL_arg1456z00_1151 > BgL_n2z00_3150);
															}
														}
													else
														{	/* Ieee/number.scm 564 */
															if (BIGNUMP(BgL_yz00_44))
																{	/* Ieee/number.scm 564 */
																	return
																		(
																		(long) (bgl_bignum_cmp(bgl_long_to_bignum(
																					(long) CINT(BgL_xz00_43)),
																				BgL_yz00_44)) > 0L);
																}
															else
																{	/* Ieee/number.scm 564 */
																	return
																		CBOOL(BGl_errorz00zz__errorz00
																		(BGl_string3331z00zz__r4_numbers_6_5z00,
																			BGl_string3328z00zz__r4_numbers_6_5z00,
																			BgL_yz00_44));
																}
														}
												}
										}
								}
						}
				}
			else
				{	/* Ieee/number.scm 564 */
					if (REALP(BgL_xz00_43))
						{	/* Ieee/number.scm 564 */
							if (REALP(BgL_yz00_44))
								{	/* Ieee/number.scm 564 */
									return
										(REAL_TO_DOUBLE(BgL_xz00_43) > REAL_TO_DOUBLE(BgL_yz00_44));
								}
							else
								{	/* Ieee/number.scm 564 */
									if (INTEGERP(BgL_yz00_44))
										{	/* Ieee/number.scm 564 */
											return
												(REAL_TO_DOUBLE(BgL_xz00_43) >
												(double) ((long) CINT(BgL_yz00_44)));
										}
									else
										{	/* Ieee/number.scm 564 */
											if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00
												(BgL_yz00_44))
												{	/* Ieee/number.scm 564 */
													double BgL_arg1465z00_1160;

													{	/* Ieee/number.scm 564 */
														obj_t BgL_arg1466z00_1161;

														BgL_arg1466z00_1161 =
															BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
															(BgL_yz00_44);
														BgL_arg1465z00_1160 =
															(double) (BELONG_TO_LONG(BgL_arg1466z00_1161));
													}
													return
														(REAL_TO_DOUBLE(BgL_xz00_43) > BgL_arg1465z00_1160);
												}
											else
												{	/* Ieee/number.scm 564 */
													if (LLONGP(BgL_yz00_44))
														{	/* Ieee/number.scm 564 */
															return
																(REAL_TO_DOUBLE(BgL_xz00_43) >
																(double) (BLLONG_TO_LLONG(BgL_yz00_44)));
														}
													else
														{	/* Ieee/number.scm 564 */
															if (BGL_UINT64P(BgL_yz00_44))
																{	/* Ieee/number.scm 564 */
																	double BgL_arg1472z00_1166;

																	{	/* Ieee/number.scm 564 */
																		uint64_t BgL_xz00_3164;

																		BgL_xz00_3164 =
																			BGL_BINT64_TO_INT64(BgL_yz00_44);
																		BgL_arg1472z00_1166 =
																			(double) (BgL_xz00_3164);
																	}
																	return
																		(REAL_TO_DOUBLE(BgL_xz00_43) >
																		BgL_arg1472z00_1166);
																}
															else
																{	/* Ieee/number.scm 564 */
																	if (BIGNUMP(BgL_yz00_44))
																		{	/* Ieee/number.scm 564 */
																			return
																				(REAL_TO_DOUBLE(BgL_xz00_43) >
																				bgl_bignum_to_flonum(BgL_yz00_44));
																		}
																	else
																		{	/* Ieee/number.scm 564 */
																			return
																				CBOOL(BGl_errorz00zz__errorz00
																				(BGl_string3331z00zz__r4_numbers_6_5z00,
																					BGl_string3328z00zz__r4_numbers_6_5z00,
																					BgL_yz00_44));
																		}
																}
														}
												}
										}
								}
						}
					else
						{	/* Ieee/number.scm 564 */
							if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_xz00_43))
								{	/* Ieee/number.scm 564 */
									if (INTEGERP(BgL_yz00_44))
										{	/* Ieee/number.scm 564 */
											obj_t BgL_arg1477z00_1171;
											long BgL_arg1478z00_1172;

											BgL_arg1477z00_1171 =
												BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
												(BgL_xz00_43);
											{	/* Ieee/number.scm 564 */
												long BgL_tmpz00_6252;

												BgL_tmpz00_6252 = (long) CINT(BgL_yz00_44);
												BgL_arg1478z00_1172 = (long) (BgL_tmpz00_6252);
											}
											{	/* Ieee/number.scm 564 */
												long BgL_n1z00_3171;

												BgL_n1z00_3171 = BELONG_TO_LONG(BgL_arg1477z00_1171);
												return (BgL_n1z00_3171 > BgL_arg1478z00_1172);
											}
										}
									else
										{	/* Ieee/number.scm 564 */
											if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00
												(BgL_yz00_44))
												{	/* Ieee/number.scm 564 */
													obj_t BgL_arg1480z00_1174;
													obj_t BgL_arg1481z00_1175;

													BgL_arg1480z00_1174 =
														BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
														(BgL_xz00_43);
													BgL_arg1481z00_1175 =
														BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
														(BgL_yz00_44);
													{	/* Ieee/number.scm 564 */
														long BgL_n1z00_3173;
														long BgL_n2z00_3174;

														BgL_n1z00_3173 =
															BELONG_TO_LONG(BgL_arg1480z00_1174);
														BgL_n2z00_3174 =
															BELONG_TO_LONG(BgL_arg1481z00_1175);
														return (BgL_n1z00_3173 > BgL_n2z00_3174);
													}
												}
											else
												{	/* Ieee/number.scm 564 */
													if (REALP(BgL_yz00_44))
														{	/* Ieee/number.scm 564 */
															double BgL_arg1483z00_1177;

															{	/* Ieee/number.scm 564 */
																obj_t BgL_arg1484z00_1178;

																BgL_arg1484z00_1178 =
																	BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																	(BgL_xz00_43);
																BgL_arg1483z00_1177 =
																	(double) (BELONG_TO_LONG
																	(BgL_arg1484z00_1178));
															}
															return
																(BgL_arg1483z00_1177 >
																REAL_TO_DOUBLE(BgL_yz00_44));
														}
													else
														{	/* Ieee/number.scm 564 */
															if (LLONGP(BgL_yz00_44))
																{	/* Ieee/number.scm 564 */
																	BGL_LONGLONG_T BgL_arg1486z00_1180;

																	{	/* Ieee/number.scm 564 */
																		obj_t BgL_arg1488z00_1182;

																		BgL_arg1488z00_1182 =
																			BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																			(BgL_xz00_43);
																		BgL_arg1486z00_1180 =
																			(BGL_LONGLONG_T) (BELONG_TO_LONG
																			(BgL_arg1488z00_1182));
																	}
																	return
																		(BgL_arg1486z00_1180 >
																		BLLONG_TO_LLONG(BgL_yz00_44));
																}
															else
																{	/* Ieee/number.scm 564 */
																	if (BGL_UINT64P(BgL_yz00_44))
																		{	/* Ieee/number.scm 564 */
																			uint64_t BgL_arg1490z00_1184;

																			{	/* Ieee/number.scm 564 */
																				BGL_LONGLONG_T BgL_arg1492z00_1185;

																				{	/* Ieee/number.scm 564 */
																					obj_t BgL_arg1494z00_1186;

																					BgL_arg1494z00_1186 =
																						BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																						(BgL_xz00_43);
																					BgL_arg1492z00_1185 =
																						(BGL_LONGLONG_T) (BELONG_TO_LONG
																						(BgL_arg1494z00_1186));
																				}
																				BgL_arg1490z00_1184 =
																					(uint64_t) (BgL_arg1492z00_1185);
																			}
																			{	/* Ieee/number.scm 564 */
																				uint64_t BgL_n2z00_3181;

																				BgL_n2z00_3181 =
																					BGL_BINT64_TO_INT64(BgL_yz00_44);
																				return
																					(BgL_arg1490z00_1184 >
																					BgL_n2z00_3181);
																			}
																		}
																	else
																		{	/* Ieee/number.scm 564 */
																			if (BIGNUMP(BgL_yz00_44))
																				{	/* Ieee/number.scm 564 */
																					obj_t BgL_arg1497z00_1188;

																					{	/* Ieee/number.scm 564 */
																						obj_t BgL_arg1498z00_1189;

																						BgL_arg1498z00_1189 =
																							BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																							(BgL_xz00_43);
																						{	/* Ieee/number.scm 564 */
																							long BgL_xz00_3182;

																							BgL_xz00_3182 =
																								BELONG_TO_LONG
																								(BgL_arg1498z00_1189);
																							BgL_arg1497z00_1188 =
																								bgl_long_to_bignum
																								(BgL_xz00_3182);
																					}}
																					return
																						(
																						(long) (bgl_bignum_cmp
																							(BgL_arg1497z00_1188,
																								BgL_yz00_44)) > 0L);
																				}
																			else
																				{	/* Ieee/number.scm 564 */
																					return
																						CBOOL(BGl_errorz00zz__errorz00
																						(BGl_string3331z00zz__r4_numbers_6_5z00,
																							BGl_string3328z00zz__r4_numbers_6_5z00,
																							BgL_yz00_44));
																				}
																		}
																}
														}
												}
										}
								}
							else
								{	/* Ieee/number.scm 564 */
									if (LLONGP(BgL_xz00_43))
										{	/* Ieee/number.scm 564 */
											if (INTEGERP(BgL_yz00_44))
												{	/* Ieee/number.scm 564 */
													BGL_LONGLONG_T BgL_arg1502z00_1193;

													{	/* Ieee/number.scm 564 */
														long BgL_tmpz00_6300;

														BgL_tmpz00_6300 = (long) CINT(BgL_yz00_44);
														BgL_arg1502z00_1193 =
															LONG_TO_LLONG(BgL_tmpz00_6300);
													}
													return
														(BLLONG_TO_LLONG(BgL_xz00_43) >
														BgL_arg1502z00_1193);
												}
											else
												{	/* Ieee/number.scm 564 */
													if (REALP(BgL_yz00_44))
														{	/* Ieee/number.scm 564 */
															return
																(
																(double) (BLLONG_TO_LLONG(BgL_xz00_43)) >
																REAL_TO_DOUBLE(BgL_yz00_44));
														}
													else
														{	/* Ieee/number.scm 564 */
															if (LLONGP(BgL_yz00_44))
																{	/* Ieee/number.scm 564 */
																	return
																		(BLLONG_TO_LLONG(BgL_xz00_43) >
																		BLLONG_TO_LLONG(BgL_yz00_44));
																}
															else
																{	/* Ieee/number.scm 564 */
																	if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00
																		(BgL_yz00_44))
																		{	/* Ieee/number.scm 564 */
																			BGL_LONGLONG_T BgL_arg1510z00_1201;

																			{	/* Ieee/number.scm 564 */
																				obj_t BgL_arg1511z00_1202;

																				BgL_arg1511z00_1202 =
																					BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																					(BgL_yz00_44);
																				BgL_arg1510z00_1201 =
																					(BGL_LONGLONG_T) (BELONG_TO_LONG
																					(BgL_arg1511z00_1202));
																			}
																			return
																				(BLLONG_TO_LLONG(BgL_xz00_43) >
																				BgL_arg1510z00_1201);
																		}
																	else
																		{	/* Ieee/number.scm 564 */
																			if (BIGNUMP(BgL_yz00_44))
																				{	/* Ieee/number.scm 564 */
																					return
																						(
																						(long) (bgl_bignum_cmp
																							(bgl_llong_to_bignum
																								(BLLONG_TO_LLONG(BgL_xz00_43)),
																								BgL_yz00_44)) > 0L);
																				}
																			else
																				{	/* Ieee/number.scm 564 */
																					if (BGL_UINT64P(BgL_yz00_44))
																						{	/* Ieee/number.scm 564 */
																							uint64_t BgL_arg1516z00_1207;

																							{	/* Ieee/number.scm 564 */
																								BGL_LONGLONG_T BgL_tmpz00_6332;

																								BgL_tmpz00_6332 =
																									BLLONG_TO_LLONG(BgL_xz00_43);
																								BgL_arg1516z00_1207 =
																									(uint64_t) (BgL_tmpz00_6332);
																							}
																							{	/* Ieee/number.scm 564 */
																								uint64_t BgL_n2z00_3203;

																								BgL_n2z00_3203 =
																									BGL_BINT64_TO_INT64
																									(BgL_yz00_44);
																								return (BgL_arg1516z00_1207 >
																									BgL_n2z00_3203);
																							}
																						}
																					else
																						{	/* Ieee/number.scm 564 */
																							return
																								CBOOL(BGl_errorz00zz__errorz00
																								(BGl_string3331z00zz__r4_numbers_6_5z00,
																									BGl_string3328z00zz__r4_numbers_6_5z00,
																									BgL_yz00_44));
																						}
																				}
																		}
																}
														}
												}
										}
									else
										{	/* Ieee/number.scm 564 */
											if (BGL_UINT64P(BgL_xz00_43))
												{	/* Ieee/number.scm 564 */
													if (INTEGERP(BgL_yz00_44))
														{	/* Ieee/number.scm 564 */
															uint64_t BgL_arg1521z00_1210;

															{	/* Ieee/number.scm 564 */
																long BgL_tmpz00_6343;

																BgL_tmpz00_6343 = (long) CINT(BgL_yz00_44);
																BgL_arg1521z00_1210 =
																	(uint64_t) (BgL_tmpz00_6343);
															}
															{	/* Ieee/number.scm 564 */
																uint64_t BgL_n1z00_3205;

																BgL_n1z00_3205 =
																	BGL_BINT64_TO_INT64(BgL_xz00_43);
																return (BgL_n1z00_3205 > BgL_arg1521z00_1210);
															}
														}
													else
														{	/* Ieee/number.scm 564 */
															if (BGL_UINT64P(BgL_yz00_44))
																{	/* Ieee/number.scm 564 */
																	uint64_t BgL_n1z00_3207;
																	uint64_t BgL_n2z00_3208;

																	BgL_n1z00_3207 =
																		BGL_BINT64_TO_INT64(BgL_xz00_43);
																	BgL_n2z00_3208 =
																		BGL_BINT64_TO_INT64(BgL_yz00_44);
																	return (BgL_n1z00_3207 > BgL_n2z00_3208);
																}
															else
																{	/* Ieee/number.scm 564 */
																	if (REALP(BgL_yz00_44))
																		{	/* Ieee/number.scm 564 */
																			double BgL_arg1524z00_1213;

																			{	/* Ieee/number.scm 564 */
																				uint64_t BgL_tmpz00_6355;

																				BgL_tmpz00_6355 =
																					BGL_BINT64_TO_INT64(BgL_xz00_43);
																				BgL_arg1524z00_1213 =
																					(double) (BgL_tmpz00_6355);
																			}
																			return
																				(BgL_arg1524z00_1213 >
																				REAL_TO_DOUBLE(BgL_yz00_44));
																		}
																	else
																		{	/* Ieee/number.scm 564 */
																			if (LLONGP(BgL_yz00_44))
																				{	/* Ieee/number.scm 564 */
																					uint64_t BgL_arg1526z00_1215;

																					{	/* Ieee/number.scm 564 */
																						BGL_LONGLONG_T BgL_tmpz00_6362;

																						BgL_tmpz00_6362 =
																							BLLONG_TO_LLONG(BgL_yz00_44);
																						BgL_arg1526z00_1215 =
																							(uint64_t) (BgL_tmpz00_6362);
																					}
																					{	/* Ieee/number.scm 564 */
																						uint64_t BgL_n1z00_3212;

																						BgL_n1z00_3212 =
																							BGL_BINT64_TO_INT64(BgL_xz00_43);
																						return
																							(BgL_n1z00_3212 >
																							BgL_arg1526z00_1215);
																					}
																				}
																			else
																				{	/* Ieee/number.scm 564 */
																					if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_44))
																						{	/* Ieee/number.scm 564 */
																							uint64_t BgL_arg1529z00_1218;

																							{	/* Ieee/number.scm 564 */
																								BGL_LONGLONG_T
																									BgL_arg1530z00_1219;
																								{	/* Ieee/number.scm 564 */
																									obj_t BgL_arg1531z00_1220;

																									BgL_arg1531z00_1220 =
																										BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																										(BgL_yz00_44);
																									BgL_arg1530z00_1219 =
																										(BGL_LONGLONG_T)
																										(BELONG_TO_LONG
																										(BgL_arg1531z00_1220));
																								}
																								BgL_arg1529z00_1218 =
																									(uint64_t)
																									(BgL_arg1530z00_1219);
																							}
																							{	/* Ieee/number.scm 564 */
																								uint64_t BgL_n1z00_3215;

																								BgL_n1z00_3215 =
																									BGL_BINT64_TO_INT64
																									(BgL_xz00_43);
																								return (BgL_n1z00_3215 >
																									BgL_arg1529z00_1218);
																							}
																						}
																					else
																						{	/* Ieee/number.scm 564 */
																							if (BIGNUMP(BgL_yz00_44))
																								{	/* Ieee/number.scm 564 */
																									long BgL_n1z00_3220;

																									BgL_n1z00_3220 =
																										(long) (bgl_bignum_cmp
																										(bgl_uint64_to_bignum
																											(BGL_BINT64_TO_INT64
																												(BgL_xz00_43)),
																											BgL_yz00_44));
																									return (BgL_n1z00_3220 > 0L);
																								}
																							else
																								{	/* Ieee/number.scm 564 */
																									return
																										CBOOL
																										(BGl_errorz00zz__errorz00
																										(BGl_string3331z00zz__r4_numbers_6_5z00,
																											BGl_string3329z00zz__r4_numbers_6_5z00,
																											BgL_yz00_44));
																								}
																						}
																				}
																		}
																}
														}
												}
											else
												{	/* Ieee/number.scm 564 */
													if (BIGNUMP(BgL_xz00_43))
														{	/* Ieee/number.scm 564 */
															if (BIGNUMP(BgL_yz00_44))
																{	/* Ieee/number.scm 564 */
																	return
																		(
																		(long) (bgl_bignum_cmp(BgL_xz00_43,
																				BgL_yz00_44)) > 0L);
																}
															else
																{	/* Ieee/number.scm 564 */
																	if (INTEGERP(BgL_yz00_44))
																		{	/* Ieee/number.scm 564 */
																			return
																				(
																				(long) (bgl_bignum_cmp(BgL_xz00_43,
																						bgl_long_to_bignum(
																							(long) CINT(BgL_yz00_44)))) > 0L);
																		}
																	else
																		{	/* Ieee/number.scm 564 */
																			if (REALP(BgL_yz00_44))
																				{	/* Ieee/number.scm 564 */
																					return
																						(bgl_bignum_to_flonum(BgL_xz00_43) >
																						REAL_TO_DOUBLE(BgL_yz00_44));
																				}
																			else
																				{	/* Ieee/number.scm 564 */
																					if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_44))
																						{	/* Ieee/number.scm 564 */
																							obj_t BgL_arg1546z00_1230;

																							{	/* Ieee/number.scm 564 */
																								obj_t BgL_arg1547z00_1231;

																								BgL_arg1547z00_1231 =
																									BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																									(BgL_yz00_44);
																								{	/* Ieee/number.scm 564 */
																									long BgL_xz00_3233;

																									BgL_xz00_3233 =
																										BELONG_TO_LONG
																										(BgL_arg1547z00_1231);
																									BgL_arg1546z00_1230 =
																										bgl_long_to_bignum
																										(BgL_xz00_3233);
																							}}
																							return
																								(
																								(long) (bgl_bignum_cmp
																									(BgL_xz00_43,
																										BgL_arg1546z00_1230)) > 0L);
																						}
																					else
																						{	/* Ieee/number.scm 564 */
																							if (LLONGP(BgL_yz00_44))
																								{	/* Ieee/number.scm 564 */
																									return
																										(
																										(long) (bgl_bignum_cmp
																											(BgL_xz00_43,
																												bgl_llong_to_bignum
																												(BLLONG_TO_LLONG
																													(BgL_yz00_44)))) >
																										0L);
																								}
																							else
																								{	/* Ieee/number.scm 564 */
																									if (BGL_UINT64P(BgL_yz00_44))
																										{	/* Ieee/number.scm 564 */
																											long BgL_n1z00_3246;

																											BgL_n1z00_3246 =
																												(long) (bgl_bignum_cmp
																												(BgL_xz00_43,
																													bgl_uint64_to_bignum
																													(BGL_BINT64_TO_INT64
																														(BgL_yz00_44))));
																											return (BgL_n1z00_3246 >
																												0L);
																										}
																									else
																										{	/* Ieee/number.scm 564 */
																											return
																												CBOOL
																												(BGl_errorz00zz__errorz00
																												(BGl_string3331z00zz__r4_numbers_6_5z00,
																													BGl_string3328z00zz__r4_numbers_6_5z00,
																													BgL_yz00_44));
																										}
																								}
																						}
																				}
																		}
																}
														}
													else
														{	/* Ieee/number.scm 564 */
															return
																CBOOL(BGl_errorz00zz__errorz00
																(BGl_string3331z00zz__r4_numbers_6_5z00,
																	BGl_string3328z00zz__r4_numbers_6_5z00,
																	BgL_xz00_43));
														}
												}
										}
								}
						}
				}
		}

	}



/* &2> */
	obj_t BGl_z622ze3z81zz__r4_numbers_6_5z00(obj_t BgL_envz00_5020,
		obj_t BgL_xz00_5021, obj_t BgL_yz00_5022)
	{
		{	/* Ieee/number.scm 563 */
			return
				BBOOL(BGl_2ze3ze3zz__r4_numbers_6_5z00(BgL_xz00_5021, BgL_yz00_5022));
		}

	}



/* > */
	BGL_EXPORTED_DEF bool_t BGl_ze3ze3zz__r4_numbers_6_5z00(obj_t BgL_xz00_45,
		obj_t BgL_yz00_46, obj_t BgL_za7za7_47)
	{
		{	/* Ieee/number.scm 569 */
			if (BGl_2ze3ze3zz__r4_numbers_6_5z00(BgL_xz00_45, BgL_yz00_46))
				{
					obj_t BgL_xz00_3264;
					obj_t BgL_za7za7_3265;

					BgL_xz00_3264 = BgL_yz00_46;
					BgL_za7za7_3265 = BgL_za7za7_47;
				BgL_ze3zd2listz31_3263:
					if (NULLP(BgL_za7za7_3265))
						{	/* Ieee/number.scm 572 */
							return ((bool_t) 1);
						}
					else
						{	/* Ieee/number.scm 573 */
							bool_t BgL_test3725z00_6435;

							{	/* Ieee/number.scm 573 */
								obj_t BgL_arg1562z00_3273;

								BgL_arg1562z00_3273 = CAR(((obj_t) BgL_za7za7_3265));
								BgL_test3725z00_6435 =
									BGl_2ze3ze3zz__r4_numbers_6_5z00(BgL_xz00_3264,
									BgL_arg1562z00_3273);
							}
							if (BgL_test3725z00_6435)
								{
									obj_t BgL_za7za7_6442;
									obj_t BgL_xz00_6439;

									BgL_xz00_6439 = CAR(((obj_t) BgL_za7za7_3265));
									BgL_za7za7_6442 = CDR(((obj_t) BgL_za7za7_3265));
									BgL_za7za7_3265 = BgL_za7za7_6442;
									BgL_xz00_3264 = BgL_xz00_6439;
									goto BgL_ze3zd2listz31_3263;
								}
							else
								{	/* Ieee/number.scm 573 */
									return ((bool_t) 0);
								}
						}
				}
			else
				{	/* Ieee/number.scm 575 */
					return ((bool_t) 0);
				}
		}

	}



/* &> */
	obj_t BGl_z62ze3z81zz__r4_numbers_6_5z00(obj_t BgL_envz00_5023,
		obj_t BgL_xz00_5024, obj_t BgL_yz00_5025, obj_t BgL_za7za7_5026)
	{
		{	/* Ieee/number.scm 569 */
			return
				BBOOL(BGl_ze3ze3zz__r4_numbers_6_5z00(BgL_xz00_5024, BgL_yz00_5025,
					BgL_za7za7_5026));
		}

	}



/* 2<= */
	BGL_EXPORTED_DEF bool_t BGl_2zc3zd3z10zz__r4_numbers_6_5z00(obj_t BgL_xz00_48,
		obj_t BgL_yz00_49)
	{
		{	/* Ieee/number.scm 580 */
			if (INTEGERP(BgL_xz00_48))
				{	/* Ieee/number.scm 581 */
					if (INTEGERP(BgL_yz00_49))
						{	/* Ieee/number.scm 581 */
							return ((long) CINT(BgL_xz00_48) <= (long) CINT(BgL_yz00_49));
						}
					else
						{	/* Ieee/number.scm 581 */
							if (REALP(BgL_yz00_49))
								{	/* Ieee/number.scm 581 */
									return
										(
										(double) (
											(long) CINT(BgL_xz00_48)) <= REAL_TO_DOUBLE(BgL_yz00_49));
								}
							else
								{	/* Ieee/number.scm 581 */
									if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_49))
										{	/* Ieee/number.scm 581 */
											long BgL_arg1571z00_1254;
											obj_t BgL_arg1573z00_1255;

											{	/* Ieee/number.scm 581 */
												long BgL_tmpz00_6462;

												BgL_tmpz00_6462 = (long) CINT(BgL_xz00_48);
												BgL_arg1571z00_1254 = (long) (BgL_tmpz00_6462);
											}
											BgL_arg1573z00_1255 =
												BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
												(BgL_yz00_49);
											{	/* Ieee/number.scm 581 */
												long BgL_n2z00_3285;

												BgL_n2z00_3285 = BELONG_TO_LONG(BgL_arg1573z00_1255);
												return (BgL_arg1571z00_1254 <= BgL_n2z00_3285);
											}
										}
									else
										{	/* Ieee/number.scm 581 */
											if (LLONGP(BgL_yz00_49))
												{	/* Ieee/number.scm 581 */
													BGL_LONGLONG_T BgL_arg1575z00_1257;

													{	/* Ieee/number.scm 581 */
														long BgL_tmpz00_6470;

														BgL_tmpz00_6470 = (long) CINT(BgL_xz00_48);
														BgL_arg1575z00_1257 =
															LONG_TO_LLONG(BgL_tmpz00_6470);
													}
													return
														(BgL_arg1575z00_1257 <=
														BLLONG_TO_LLONG(BgL_yz00_49));
												}
											else
												{	/* Ieee/number.scm 581 */
													if (BGL_UINT64P(BgL_yz00_49))
														{	/* Ieee/number.scm 581 */
															uint64_t BgL_arg1578z00_1260;

															{	/* Ieee/number.scm 581 */
																BGL_LONGLONG_T BgL_arg1579z00_1261;

																{	/* Ieee/number.scm 581 */
																	long BgL_tmpz00_6477;

																	BgL_tmpz00_6477 = (long) CINT(BgL_xz00_48);
																	BgL_arg1579z00_1261 =
																		LONG_TO_LLONG(BgL_tmpz00_6477);
																}
																BgL_arg1578z00_1260 =
																	(uint64_t) (BgL_arg1579z00_1261);
															}
															{	/* Ieee/number.scm 581 */
																uint64_t BgL_n2z00_3292;

																BgL_n2z00_3292 =
																	BGL_BINT64_TO_INT64(BgL_yz00_49);
																return (BgL_arg1578z00_1260 <= BgL_n2z00_3292);
															}
														}
													else
														{	/* Ieee/number.scm 581 */
															if (BIGNUMP(BgL_yz00_49))
																{	/* Ieee/number.scm 581 */
																	return
																		(
																		(long) (bgl_bignum_cmp(bgl_long_to_bignum(
																					(long) CINT(BgL_xz00_48)),
																				BgL_yz00_49)) <= 0L);
																}
															else
																{	/* Ieee/number.scm 581 */
																	return
																		CBOOL(BGl_errorz00zz__errorz00
																		(BGl_string3332z00zz__r4_numbers_6_5z00,
																			BGl_string3328z00zz__r4_numbers_6_5z00,
																			BgL_yz00_49));
																}
														}
												}
										}
								}
						}
				}
			else
				{	/* Ieee/number.scm 581 */
					if (REALP(BgL_xz00_48))
						{	/* Ieee/number.scm 581 */
							if (REALP(BgL_yz00_49))
								{	/* Ieee/number.scm 581 */
									return
										(REAL_TO_DOUBLE(BgL_xz00_48) <=
										REAL_TO_DOUBLE(BgL_yz00_49));
								}
							else
								{	/* Ieee/number.scm 581 */
									if (INTEGERP(BgL_yz00_49))
										{	/* Ieee/number.scm 581 */
											return
												(REAL_TO_DOUBLE(BgL_xz00_48) <=
												(double) ((long) CINT(BgL_yz00_49)));
										}
									else
										{	/* Ieee/number.scm 581 */
											if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00
												(BgL_yz00_49))
												{	/* Ieee/number.scm 581 */
													double BgL_arg1589z00_1269;

													{	/* Ieee/number.scm 581 */
														obj_t BgL_arg1591z00_1270;

														BgL_arg1591z00_1270 =
															BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
															(BgL_yz00_49);
														BgL_arg1589z00_1269 =
															(double) (BELONG_TO_LONG(BgL_arg1591z00_1270));
													}
													return
														(REAL_TO_DOUBLE(BgL_xz00_48) <=
														BgL_arg1589z00_1269);
												}
											else
												{	/* Ieee/number.scm 581 */
													if (LLONGP(BgL_yz00_49))
														{	/* Ieee/number.scm 581 */
															return
																(REAL_TO_DOUBLE(BgL_xz00_48) <=
																(double) (BLLONG_TO_LLONG(BgL_yz00_49)));
														}
													else
														{	/* Ieee/number.scm 581 */
															if (BGL_UINT64P(BgL_yz00_49))
																{	/* Ieee/number.scm 581 */
																	double BgL_arg1598z00_1275;

																	{	/* Ieee/number.scm 581 */
																		uint64_t BgL_xz00_3306;

																		BgL_xz00_3306 =
																			BGL_BINT64_TO_INT64(BgL_yz00_49);
																		BgL_arg1598z00_1275 =
																			(double) (BgL_xz00_3306);
																	}
																	return
																		(REAL_TO_DOUBLE(BgL_xz00_48) <=
																		BgL_arg1598z00_1275);
																}
															else
																{	/* Ieee/number.scm 581 */
																	if (BIGNUMP(BgL_yz00_49))
																		{	/* Ieee/number.scm 581 */
																			return
																				(REAL_TO_DOUBLE(BgL_xz00_48) <=
																				bgl_bignum_to_flonum(BgL_yz00_49));
																		}
																	else
																		{	/* Ieee/number.scm 581 */
																			return
																				CBOOL(BGl_errorz00zz__errorz00
																				(BGl_string3332z00zz__r4_numbers_6_5z00,
																					BGl_string3328z00zz__r4_numbers_6_5z00,
																					BgL_yz00_49));
																		}
																}
														}
												}
										}
								}
						}
					else
						{	/* Ieee/number.scm 581 */
							if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_xz00_48))
								{	/* Ieee/number.scm 581 */
									if (INTEGERP(BgL_yz00_49))
										{	/* Ieee/number.scm 581 */
											obj_t BgL_arg1605z00_1280;
											long BgL_arg1606z00_1281;

											BgL_arg1605z00_1280 =
												BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
												(BgL_xz00_48);
											{	/* Ieee/number.scm 581 */
												long BgL_tmpz00_6536;

												BgL_tmpz00_6536 = (long) CINT(BgL_yz00_49);
												BgL_arg1606z00_1281 = (long) (BgL_tmpz00_6536);
											}
											{	/* Ieee/number.scm 581 */
												long BgL_n1z00_3313;

												BgL_n1z00_3313 = BELONG_TO_LONG(BgL_arg1605z00_1280);
												return (BgL_n1z00_3313 <= BgL_arg1606z00_1281);
											}
										}
									else
										{	/* Ieee/number.scm 581 */
											if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00
												(BgL_yz00_49))
												{	/* Ieee/number.scm 581 */
													obj_t BgL_arg1608z00_1283;
													obj_t BgL_arg1609z00_1284;

													BgL_arg1608z00_1283 =
														BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
														(BgL_xz00_48);
													BgL_arg1609z00_1284 =
														BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
														(BgL_yz00_49);
													{	/* Ieee/number.scm 581 */
														long BgL_n1z00_3315;
														long BgL_n2z00_3316;

														BgL_n1z00_3315 =
															BELONG_TO_LONG(BgL_arg1608z00_1283);
														BgL_n2z00_3316 =
															BELONG_TO_LONG(BgL_arg1609z00_1284);
														return (BgL_n1z00_3315 <= BgL_n2z00_3316);
													}
												}
											else
												{	/* Ieee/number.scm 581 */
													if (REALP(BgL_yz00_49))
														{	/* Ieee/number.scm 581 */
															double BgL_arg1611z00_1286;

															{	/* Ieee/number.scm 581 */
																obj_t BgL_arg1612z00_1287;

																BgL_arg1612z00_1287 =
																	BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																	(BgL_xz00_48);
																BgL_arg1611z00_1286 =
																	(double) (BELONG_TO_LONG
																	(BgL_arg1612z00_1287));
															}
															return
																(BgL_arg1611z00_1286 <=
																REAL_TO_DOUBLE(BgL_yz00_49));
														}
													else
														{	/* Ieee/number.scm 581 */
															if (LLONGP(BgL_yz00_49))
																{	/* Ieee/number.scm 581 */
																	BGL_LONGLONG_T BgL_arg1615z00_1289;

																	{	/* Ieee/number.scm 581 */
																		obj_t BgL_arg1617z00_1291;

																		BgL_arg1617z00_1291 =
																			BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																			(BgL_xz00_48);
																		BgL_arg1615z00_1289 =
																			(BGL_LONGLONG_T) (BELONG_TO_LONG
																			(BgL_arg1617z00_1291));
																	}
																	return
																		(BgL_arg1615z00_1289 <=
																		BLLONG_TO_LLONG(BgL_yz00_49));
																}
															else
																{	/* Ieee/number.scm 581 */
																	if (BGL_UINT64P(BgL_yz00_49))
																		{	/* Ieee/number.scm 581 */
																			uint64_t BgL_arg1619z00_1293;

																			{	/* Ieee/number.scm 581 */
																				BGL_LONGLONG_T BgL_arg1620z00_1294;

																				{	/* Ieee/number.scm 581 */
																					obj_t BgL_arg1621z00_1295;

																					BgL_arg1621z00_1295 =
																						BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																						(BgL_xz00_48);
																					BgL_arg1620z00_1294 =
																						(BGL_LONGLONG_T) (BELONG_TO_LONG
																						(BgL_arg1621z00_1295));
																				}
																				BgL_arg1619z00_1293 =
																					(uint64_t) (BgL_arg1620z00_1294);
																			}
																			{	/* Ieee/number.scm 581 */
																				uint64_t BgL_n2z00_3323;

																				BgL_n2z00_3323 =
																					BGL_BINT64_TO_INT64(BgL_yz00_49);
																				return
																					(BgL_arg1619z00_1293 <=
																					BgL_n2z00_3323);
																			}
																		}
																	else
																		{	/* Ieee/number.scm 581 */
																			if (BIGNUMP(BgL_yz00_49))
																				{	/* Ieee/number.scm 581 */
																					obj_t BgL_arg1623z00_1297;

																					{	/* Ieee/number.scm 581 */
																						obj_t BgL_arg1624z00_1298;

																						BgL_arg1624z00_1298 =
																							BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																							(BgL_xz00_48);
																						{	/* Ieee/number.scm 581 */
																							long BgL_xz00_3324;

																							BgL_xz00_3324 =
																								BELONG_TO_LONG
																								(BgL_arg1624z00_1298);
																							BgL_arg1623z00_1297 =
																								bgl_long_to_bignum
																								(BgL_xz00_3324);
																					}}
																					return
																						(
																						(long) (bgl_bignum_cmp
																							(BgL_arg1623z00_1297,
																								BgL_yz00_49)) <= 0L);
																				}
																			else
																				{	/* Ieee/number.scm 581 */
																					return
																						CBOOL(BGl_errorz00zz__errorz00
																						(BGl_string3332z00zz__r4_numbers_6_5z00,
																							BGl_string3328z00zz__r4_numbers_6_5z00,
																							BgL_yz00_49));
																				}
																		}
																}
														}
												}
										}
								}
							else
								{	/* Ieee/number.scm 581 */
									if (LLONGP(BgL_xz00_48))
										{	/* Ieee/number.scm 581 */
											if (INTEGERP(BgL_yz00_49))
												{	/* Ieee/number.scm 581 */
													BGL_LONGLONG_T BgL_arg1628z00_1302;

													{	/* Ieee/number.scm 581 */
														long BgL_tmpz00_6584;

														BgL_tmpz00_6584 = (long) CINT(BgL_yz00_49);
														BgL_arg1628z00_1302 =
															LONG_TO_LLONG(BgL_tmpz00_6584);
													}
													return
														(BLLONG_TO_LLONG(BgL_xz00_48) <=
														BgL_arg1628z00_1302);
												}
											else
												{	/* Ieee/number.scm 581 */
													if (REALP(BgL_yz00_49))
														{	/* Ieee/number.scm 581 */
															return
																(
																(double) (BLLONG_TO_LLONG(BgL_xz00_48)) <=
																REAL_TO_DOUBLE(BgL_yz00_49));
														}
													else
														{	/* Ieee/number.scm 581 */
															if (LLONGP(BgL_yz00_49))
																{	/* Ieee/number.scm 581 */
																	return
																		(BLLONG_TO_LLONG(BgL_xz00_48) <=
																		BLLONG_TO_LLONG(BgL_yz00_49));
																}
															else
																{	/* Ieee/number.scm 581 */
																	if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00
																		(BgL_yz00_49))
																		{	/* Ieee/number.scm 581 */
																			BGL_LONGLONG_T BgL_arg1639z00_1310;

																			{	/* Ieee/number.scm 581 */
																				obj_t BgL_arg1640z00_1311;

																				BgL_arg1640z00_1311 =
																					BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																					(BgL_yz00_49);
																				BgL_arg1639z00_1310 =
																					(BGL_LONGLONG_T) (BELONG_TO_LONG
																					(BgL_arg1640z00_1311));
																			}
																			return
																				(BLLONG_TO_LLONG(BgL_xz00_48) <=
																				BgL_arg1639z00_1310);
																		}
																	else
																		{	/* Ieee/number.scm 581 */
																			if (BIGNUMP(BgL_yz00_49))
																				{	/* Ieee/number.scm 581 */
																					return
																						(
																						(long) (bgl_bignum_cmp
																							(bgl_llong_to_bignum
																								(BLLONG_TO_LLONG(BgL_xz00_48)),
																								BgL_yz00_49)) <= 0L);
																				}
																			else
																				{	/* Ieee/number.scm 581 */
																					if (BGL_UINT64P(BgL_yz00_49))
																						{	/* Ieee/number.scm 581 */
																							uint64_t BgL_arg1645z00_1316;

																							{	/* Ieee/number.scm 581 */
																								BGL_LONGLONG_T BgL_tmpz00_6616;

																								BgL_tmpz00_6616 =
																									BLLONG_TO_LLONG(BgL_xz00_48);
																								BgL_arg1645z00_1316 =
																									(uint64_t) (BgL_tmpz00_6616);
																							}
																							{	/* Ieee/number.scm 581 */
																								uint64_t BgL_n2z00_3345;

																								BgL_n2z00_3345 =
																									BGL_BINT64_TO_INT64
																									(BgL_yz00_49);
																								return (BgL_arg1645z00_1316 <=
																									BgL_n2z00_3345);
																							}
																						}
																					else
																						{	/* Ieee/number.scm 581 */
																							return
																								CBOOL(BGl_errorz00zz__errorz00
																								(BGl_string3332z00zz__r4_numbers_6_5z00,
																									BGl_string3328z00zz__r4_numbers_6_5z00,
																									BgL_yz00_49));
																						}
																				}
																		}
																}
														}
												}
										}
									else
										{	/* Ieee/number.scm 581 */
											if (BGL_UINT64P(BgL_xz00_48))
												{	/* Ieee/number.scm 581 */
													if (INTEGERP(BgL_yz00_49))
														{	/* Ieee/number.scm 581 */
															uint64_t BgL_arg1648z00_1319;

															{	/* Ieee/number.scm 581 */
																long BgL_tmpz00_6627;

																BgL_tmpz00_6627 = (long) CINT(BgL_yz00_49);
																BgL_arg1648z00_1319 =
																	(uint64_t) (BgL_tmpz00_6627);
															}
															{	/* Ieee/number.scm 581 */
																uint64_t BgL_n1z00_3347;

																BgL_n1z00_3347 =
																	BGL_BINT64_TO_INT64(BgL_xz00_48);
																return (BgL_n1z00_3347 <= BgL_arg1648z00_1319);
															}
														}
													else
														{	/* Ieee/number.scm 581 */
															if (BGL_UINT64P(BgL_yz00_49))
																{	/* Ieee/number.scm 581 */
																	uint64_t BgL_n1z00_3349;
																	uint64_t BgL_n2z00_3350;

																	BgL_n1z00_3349 =
																		BGL_BINT64_TO_INT64(BgL_xz00_48);
																	BgL_n2z00_3350 =
																		BGL_BINT64_TO_INT64(BgL_yz00_49);
																	return (BgL_n1z00_3349 <= BgL_n2z00_3350);
																}
															else
																{	/* Ieee/number.scm 581 */
																	if (REALP(BgL_yz00_49))
																		{	/* Ieee/number.scm 581 */
																			double BgL_arg1651z00_1322;

																			{	/* Ieee/number.scm 581 */
																				uint64_t BgL_tmpz00_6639;

																				BgL_tmpz00_6639 =
																					BGL_BINT64_TO_INT64(BgL_xz00_48);
																				BgL_arg1651z00_1322 =
																					(double) (BgL_tmpz00_6639);
																			}
																			return
																				(BgL_arg1651z00_1322 <=
																				REAL_TO_DOUBLE(BgL_yz00_49));
																		}
																	else
																		{	/* Ieee/number.scm 581 */
																			if (LLONGP(BgL_yz00_49))
																				{	/* Ieee/number.scm 581 */
																					uint64_t BgL_arg1653z00_1324;

																					{	/* Ieee/number.scm 581 */
																						BGL_LONGLONG_T BgL_tmpz00_6646;

																						BgL_tmpz00_6646 =
																							BLLONG_TO_LLONG(BgL_yz00_49);
																						BgL_arg1653z00_1324 =
																							(uint64_t) (BgL_tmpz00_6646);
																					}
																					{	/* Ieee/number.scm 581 */
																						uint64_t BgL_n1z00_3354;

																						BgL_n1z00_3354 =
																							BGL_BINT64_TO_INT64(BgL_xz00_48);
																						return
																							(BgL_n1z00_3354 <=
																							BgL_arg1653z00_1324);
																					}
																				}
																			else
																				{	/* Ieee/number.scm 581 */
																					if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_49))
																						{	/* Ieee/number.scm 581 */
																							uint64_t BgL_arg1656z00_1327;

																							{	/* Ieee/number.scm 581 */
																								BGL_LONGLONG_T
																									BgL_arg1657z00_1328;
																								{	/* Ieee/number.scm 581 */
																									obj_t BgL_arg1658z00_1329;

																									BgL_arg1658z00_1329 =
																										BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																										(BgL_yz00_49);
																									BgL_arg1657z00_1328 =
																										(BGL_LONGLONG_T)
																										(BELONG_TO_LONG
																										(BgL_arg1658z00_1329));
																								}
																								BgL_arg1656z00_1327 =
																									(uint64_t)
																									(BgL_arg1657z00_1328);
																							}
																							{	/* Ieee/number.scm 581 */
																								uint64_t BgL_n1z00_3357;

																								BgL_n1z00_3357 =
																									BGL_BINT64_TO_INT64
																									(BgL_xz00_48);
																								return (BgL_n1z00_3357 <=
																									BgL_arg1656z00_1327);
																							}
																						}
																					else
																						{	/* Ieee/number.scm 581 */
																							if (BIGNUMP(BgL_yz00_49))
																								{	/* Ieee/number.scm 581 */
																									long BgL_n1z00_3362;

																									BgL_n1z00_3362 =
																										(long) (bgl_bignum_cmp
																										(bgl_uint64_to_bignum
																											(BGL_BINT64_TO_INT64
																												(BgL_xz00_48)),
																											BgL_yz00_49));
																									return (BgL_n1z00_3362 <= 0L);
																								}
																							else
																								{	/* Ieee/number.scm 581 */
																									return
																										CBOOL
																										(BGl_errorz00zz__errorz00
																										(BGl_string3332z00zz__r4_numbers_6_5z00,
																											BGl_string3329z00zz__r4_numbers_6_5z00,
																											BgL_yz00_49));
																								}
																						}
																				}
																		}
																}
														}
												}
											else
												{	/* Ieee/number.scm 581 */
													if (BIGNUMP(BgL_xz00_48))
														{	/* Ieee/number.scm 581 */
															if (BIGNUMP(BgL_yz00_49))
																{	/* Ieee/number.scm 581 */
																	return
																		(
																		(long) (bgl_bignum_cmp(BgL_xz00_48,
																				BgL_yz00_49)) <= 0L);
																}
															else
																{	/* Ieee/number.scm 581 */
																	if (INTEGERP(BgL_yz00_49))
																		{	/* Ieee/number.scm 581 */
																			return
																				(
																				(long) (bgl_bignum_cmp(BgL_xz00_48,
																						bgl_long_to_bignum(
																							(long) CINT(BgL_yz00_49)))) <=
																				0L);
																		}
																	else
																		{	/* Ieee/number.scm 581 */
																			if (REALP(BgL_yz00_49))
																				{	/* Ieee/number.scm 581 */
																					return
																						(bgl_bignum_to_flonum(BgL_xz00_48)
																						<= REAL_TO_DOUBLE(BgL_yz00_49));
																				}
																			else
																				{	/* Ieee/number.scm 581 */
																					if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_49))
																						{	/* Ieee/number.scm 581 */
																							obj_t BgL_arg1675z00_1339;

																							{	/* Ieee/number.scm 581 */
																								obj_t BgL_arg1676z00_1340;

																								BgL_arg1676z00_1340 =
																									BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																									(BgL_yz00_49);
																								{	/* Ieee/number.scm 581 */
																									long BgL_xz00_3375;

																									BgL_xz00_3375 =
																										BELONG_TO_LONG
																										(BgL_arg1676z00_1340);
																									BgL_arg1675z00_1339 =
																										bgl_long_to_bignum
																										(BgL_xz00_3375);
																							}}
																							return
																								(
																								(long) (bgl_bignum_cmp
																									(BgL_xz00_48,
																										BgL_arg1675z00_1339)) <=
																								0L);
																						}
																					else
																						{	/* Ieee/number.scm 581 */
																							if (LLONGP(BgL_yz00_49))
																								{	/* Ieee/number.scm 581 */
																									return
																										(
																										(long) (bgl_bignum_cmp
																											(BgL_xz00_48,
																												bgl_llong_to_bignum
																												(BLLONG_TO_LLONG
																													(BgL_yz00_49)))) <=
																										0L);
																								}
																							else
																								{	/* Ieee/number.scm 581 */
																									if (BGL_UINT64P(BgL_yz00_49))
																										{	/* Ieee/number.scm 581 */
																											long BgL_n1z00_3388;

																											BgL_n1z00_3388 =
																												(long) (bgl_bignum_cmp
																												(BgL_xz00_48,
																													bgl_uint64_to_bignum
																													(BGL_BINT64_TO_INT64
																														(BgL_yz00_49))));
																											return (BgL_n1z00_3388 <=
																												0L);
																										}
																									else
																										{	/* Ieee/number.scm 581 */
																											return
																												CBOOL
																												(BGl_errorz00zz__errorz00
																												(BGl_string3332z00zz__r4_numbers_6_5z00,
																													BGl_string3328z00zz__r4_numbers_6_5z00,
																													BgL_yz00_49));
																										}
																								}
																						}
																				}
																		}
																}
														}
													else
														{	/* Ieee/number.scm 581 */
															return
																CBOOL(BGl_errorz00zz__errorz00
																(BGl_string3332z00zz__r4_numbers_6_5z00,
																	BGl_string3328z00zz__r4_numbers_6_5z00,
																	BgL_xz00_48));
														}
												}
										}
								}
						}
				}
		}

	}



/* &2<= */
	obj_t BGl_z622zc3zd3z72zz__r4_numbers_6_5z00(obj_t BgL_envz00_5027,
		obj_t BgL_xz00_5028, obj_t BgL_yz00_5029)
	{
		{	/* Ieee/number.scm 580 */
			return
				BBOOL(BGl_2zc3zd3z10zz__r4_numbers_6_5z00(BgL_xz00_5028,
					BgL_yz00_5029));
		}

	}



/* <= */
	BGL_EXPORTED_DEF bool_t BGl_zc3zd3z10zz__r4_numbers_6_5z00(obj_t BgL_xz00_50,
		obj_t BgL_yz00_51, obj_t BgL_za7za7_52)
	{
		{	/* Ieee/number.scm 586 */
			if (BGl_2zc3zd3z10zz__r4_numbers_6_5z00(BgL_xz00_50, BgL_yz00_51))
				{
					obj_t BgL_xz00_3406;
					obj_t BgL_za7za7_3407;

					BgL_xz00_3406 = BgL_yz00_51;
					BgL_za7za7_3407 = BgL_za7za7_52;
				BgL_zc3zd3zd2listzc2_3405:
					if (NULLP(BgL_za7za7_3407))
						{	/* Ieee/number.scm 589 */
							return ((bool_t) 1);
						}
					else
						{	/* Ieee/number.scm 590 */
							bool_t BgL_test3770z00_6719;

							{	/* Ieee/number.scm 590 */
								obj_t BgL_arg1699z00_3415;

								BgL_arg1699z00_3415 = CAR(((obj_t) BgL_za7za7_3407));
								BgL_test3770z00_6719 =
									BGl_2zc3zd3z10zz__r4_numbers_6_5z00(BgL_xz00_3406,
									BgL_arg1699z00_3415);
							}
							if (BgL_test3770z00_6719)
								{
									obj_t BgL_za7za7_6726;
									obj_t BgL_xz00_6723;

									BgL_xz00_6723 = CAR(((obj_t) BgL_za7za7_3407));
									BgL_za7za7_6726 = CDR(((obj_t) BgL_za7za7_3407));
									BgL_za7za7_3407 = BgL_za7za7_6726;
									BgL_xz00_3406 = BgL_xz00_6723;
									goto BgL_zc3zd3zd2listzc2_3405;
								}
							else
								{	/* Ieee/number.scm 590 */
									return ((bool_t) 0);
								}
						}
				}
			else
				{	/* Ieee/number.scm 592 */
					return ((bool_t) 0);
				}
		}

	}



/* &<= */
	obj_t BGl_z62zc3zd3z72zz__r4_numbers_6_5z00(obj_t BgL_envz00_5030,
		obj_t BgL_xz00_5031, obj_t BgL_yz00_5032, obj_t BgL_za7za7_5033)
	{
		{	/* Ieee/number.scm 586 */
			return
				BBOOL(BGl_zc3zd3z10zz__r4_numbers_6_5z00(BgL_xz00_5031, BgL_yz00_5032,
					BgL_za7za7_5033));
		}

	}



/* 2>= */
	BGL_EXPORTED_DEF bool_t BGl_2ze3zd3z30zz__r4_numbers_6_5z00(obj_t BgL_xz00_53,
		obj_t BgL_yz00_54)
	{
		{	/* Ieee/number.scm 597 */
			if (INTEGERP(BgL_xz00_53))
				{	/* Ieee/number.scm 598 */
					if (INTEGERP(BgL_yz00_54))
						{	/* Ieee/number.scm 598 */
							return ((long) CINT(BgL_xz00_53) >= (long) CINT(BgL_yz00_54));
						}
					else
						{	/* Ieee/number.scm 598 */
							if (REALP(BgL_yz00_54))
								{	/* Ieee/number.scm 598 */
									return
										(
										(double) (
											(long) CINT(BgL_xz00_53)) >= REAL_TO_DOUBLE(BgL_yz00_54));
								}
							else
								{	/* Ieee/number.scm 598 */
									if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_54))
										{	/* Ieee/number.scm 598 */
											long BgL_arg1705z00_1363;
											obj_t BgL_arg1706z00_1364;

											{	/* Ieee/number.scm 598 */
												long BgL_tmpz00_6746;

												BgL_tmpz00_6746 = (long) CINT(BgL_xz00_53);
												BgL_arg1705z00_1363 = (long) (BgL_tmpz00_6746);
											}
											BgL_arg1706z00_1364 =
												BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
												(BgL_yz00_54);
											{	/* Ieee/number.scm 598 */
												long BgL_n2z00_3427;

												BgL_n2z00_3427 = BELONG_TO_LONG(BgL_arg1706z00_1364);
												return (BgL_arg1705z00_1363 >= BgL_n2z00_3427);
											}
										}
									else
										{	/* Ieee/number.scm 598 */
											if (LLONGP(BgL_yz00_54))
												{	/* Ieee/number.scm 598 */
													BGL_LONGLONG_T BgL_arg1708z00_1366;

													{	/* Ieee/number.scm 598 */
														long BgL_tmpz00_6754;

														BgL_tmpz00_6754 = (long) CINT(BgL_xz00_53);
														BgL_arg1708z00_1366 =
															LONG_TO_LLONG(BgL_tmpz00_6754);
													}
													return
														(BgL_arg1708z00_1366 >=
														BLLONG_TO_LLONG(BgL_yz00_54));
												}
											else
												{	/* Ieee/number.scm 598 */
													if (BGL_UINT64P(BgL_yz00_54))
														{	/* Ieee/number.scm 598 */
															uint64_t BgL_arg1711z00_1369;

															{	/* Ieee/number.scm 598 */
																BGL_LONGLONG_T BgL_arg1714z00_1370;

																{	/* Ieee/number.scm 598 */
																	long BgL_tmpz00_6761;

																	BgL_tmpz00_6761 = (long) CINT(BgL_xz00_53);
																	BgL_arg1714z00_1370 =
																		LONG_TO_LLONG(BgL_tmpz00_6761);
																}
																BgL_arg1711z00_1369 =
																	(uint64_t) (BgL_arg1714z00_1370);
															}
															{	/* Ieee/number.scm 598 */
																uint64_t BgL_n2z00_3434;

																BgL_n2z00_3434 =
																	BGL_BINT64_TO_INT64(BgL_yz00_54);
																return (BgL_arg1711z00_1369 >= BgL_n2z00_3434);
															}
														}
													else
														{	/* Ieee/number.scm 598 */
															if (BIGNUMP(BgL_yz00_54))
																{	/* Ieee/number.scm 598 */
																	return
																		(
																		(long) (bgl_bignum_cmp(bgl_long_to_bignum(
																					(long) CINT(BgL_xz00_53)),
																				BgL_yz00_54)) >= 0L);
																}
															else
																{	/* Ieee/number.scm 598 */
																	return
																		CBOOL(BGl_errorz00zz__errorz00
																		(BGl_string3333z00zz__r4_numbers_6_5z00,
																			BGl_string3328z00zz__r4_numbers_6_5z00,
																			BgL_yz00_54));
																}
														}
												}
										}
								}
						}
				}
			else
				{	/* Ieee/number.scm 598 */
					if (REALP(BgL_xz00_53))
						{	/* Ieee/number.scm 598 */
							if (REALP(BgL_yz00_54))
								{	/* Ieee/number.scm 598 */
									return
										(REAL_TO_DOUBLE(BgL_xz00_53) >=
										REAL_TO_DOUBLE(BgL_yz00_54));
								}
							else
								{	/* Ieee/number.scm 598 */
									if (INTEGERP(BgL_yz00_54))
										{	/* Ieee/number.scm 598 */
											return
												(REAL_TO_DOUBLE(BgL_xz00_53) >=
												(double) ((long) CINT(BgL_yz00_54)));
										}
									else
										{	/* Ieee/number.scm 598 */
											if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00
												(BgL_yz00_54))
												{	/* Ieee/number.scm 598 */
													double BgL_arg1724z00_1378;

													{	/* Ieee/number.scm 598 */
														obj_t BgL_arg1725z00_1379;

														BgL_arg1725z00_1379 =
															BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
															(BgL_yz00_54);
														BgL_arg1724z00_1378 =
															(double) (BELONG_TO_LONG(BgL_arg1725z00_1379));
													}
													return
														(REAL_TO_DOUBLE(BgL_xz00_53) >=
														BgL_arg1724z00_1378);
												}
											else
												{	/* Ieee/number.scm 598 */
													if (LLONGP(BgL_yz00_54))
														{	/* Ieee/number.scm 598 */
															return
																(REAL_TO_DOUBLE(BgL_xz00_53) >=
																(double) (BLLONG_TO_LLONG(BgL_yz00_54)));
														}
													else
														{	/* Ieee/number.scm 598 */
															if (BGL_UINT64P(BgL_yz00_54))
																{	/* Ieee/number.scm 598 */
																	double BgL_arg1730z00_1384;

																	{	/* Ieee/number.scm 598 */
																		uint64_t BgL_xz00_3448;

																		BgL_xz00_3448 =
																			BGL_BINT64_TO_INT64(BgL_yz00_54);
																		BgL_arg1730z00_1384 =
																			(double) (BgL_xz00_3448);
																	}
																	return
																		(REAL_TO_DOUBLE(BgL_xz00_53) >=
																		BgL_arg1730z00_1384);
																}
															else
																{	/* Ieee/number.scm 598 */
																	if (BIGNUMP(BgL_yz00_54))
																		{	/* Ieee/number.scm 598 */
																			return
																				(REAL_TO_DOUBLE(BgL_xz00_53) >=
																				bgl_bignum_to_flonum(BgL_yz00_54));
																		}
																	else
																		{	/* Ieee/number.scm 598 */
																			return
																				CBOOL(BGl_errorz00zz__errorz00
																				(BGl_string3333z00zz__r4_numbers_6_5z00,
																					BGl_string3328z00zz__r4_numbers_6_5z00,
																					BgL_yz00_54));
																		}
																}
														}
												}
										}
								}
						}
					else
						{	/* Ieee/number.scm 598 */
							if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_xz00_53))
								{	/* Ieee/number.scm 598 */
									if (INTEGERP(BgL_yz00_54))
										{	/* Ieee/number.scm 598 */
											obj_t BgL_arg1736z00_1389;
											long BgL_arg1737z00_1390;

											BgL_arg1736z00_1389 =
												BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
												(BgL_xz00_53);
											{	/* Ieee/number.scm 598 */
												long BgL_tmpz00_6820;

												BgL_tmpz00_6820 = (long) CINT(BgL_yz00_54);
												BgL_arg1737z00_1390 = (long) (BgL_tmpz00_6820);
											}
											{	/* Ieee/number.scm 598 */
												long BgL_n1z00_3455;

												BgL_n1z00_3455 = BELONG_TO_LONG(BgL_arg1736z00_1389);
												return (BgL_n1z00_3455 >= BgL_arg1737z00_1390);
											}
										}
									else
										{	/* Ieee/number.scm 598 */
											if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00
												(BgL_yz00_54))
												{	/* Ieee/number.scm 598 */
													obj_t BgL_arg1739z00_1392;
													obj_t BgL_arg1740z00_1393;

													BgL_arg1739z00_1392 =
														BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
														(BgL_xz00_53);
													BgL_arg1740z00_1393 =
														BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
														(BgL_yz00_54);
													{	/* Ieee/number.scm 598 */
														long BgL_n1z00_3457;
														long BgL_n2z00_3458;

														BgL_n1z00_3457 =
															BELONG_TO_LONG(BgL_arg1739z00_1392);
														BgL_n2z00_3458 =
															BELONG_TO_LONG(BgL_arg1740z00_1393);
														return (BgL_n1z00_3457 >= BgL_n2z00_3458);
													}
												}
											else
												{	/* Ieee/number.scm 598 */
													if (REALP(BgL_yz00_54))
														{	/* Ieee/number.scm 598 */
															double BgL_arg1743z00_1395;

															{	/* Ieee/number.scm 598 */
																obj_t BgL_arg1744z00_1396;

																BgL_arg1744z00_1396 =
																	BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																	(BgL_xz00_53);
																BgL_arg1743z00_1395 =
																	(double) (BELONG_TO_LONG
																	(BgL_arg1744z00_1396));
															}
															return
																(BgL_arg1743z00_1395 >=
																REAL_TO_DOUBLE(BgL_yz00_54));
														}
													else
														{	/* Ieee/number.scm 598 */
															if (LLONGP(BgL_yz00_54))
																{	/* Ieee/number.scm 598 */
																	BGL_LONGLONG_T BgL_arg1746z00_1398;

																	{	/* Ieee/number.scm 598 */
																		obj_t BgL_arg1748z00_1400;

																		BgL_arg1748z00_1400 =
																			BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																			(BgL_xz00_53);
																		BgL_arg1746z00_1398 =
																			(BGL_LONGLONG_T) (BELONG_TO_LONG
																			(BgL_arg1748z00_1400));
																	}
																	return
																		(BgL_arg1746z00_1398 >=
																		BLLONG_TO_LLONG(BgL_yz00_54));
																}
															else
																{	/* Ieee/number.scm 598 */
																	if (BGL_UINT64P(BgL_yz00_54))
																		{	/* Ieee/number.scm 598 */
																			uint64_t BgL_arg1750z00_1402;

																			{	/* Ieee/number.scm 598 */
																				BGL_LONGLONG_T BgL_arg1751z00_1403;

																				{	/* Ieee/number.scm 598 */
																					obj_t BgL_arg1752z00_1404;

																					BgL_arg1752z00_1404 =
																						BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																						(BgL_xz00_53);
																					BgL_arg1751z00_1403 =
																						(BGL_LONGLONG_T) (BELONG_TO_LONG
																						(BgL_arg1752z00_1404));
																				}
																				BgL_arg1750z00_1402 =
																					(uint64_t) (BgL_arg1751z00_1403);
																			}
																			{	/* Ieee/number.scm 598 */
																				uint64_t BgL_n2z00_3465;

																				BgL_n2z00_3465 =
																					BGL_BINT64_TO_INT64(BgL_yz00_54);
																				return
																					(BgL_arg1750z00_1402 >=
																					BgL_n2z00_3465);
																			}
																		}
																	else
																		{	/* Ieee/number.scm 598 */
																			if (BIGNUMP(BgL_yz00_54))
																				{	/* Ieee/number.scm 598 */
																					obj_t BgL_arg1754z00_1406;

																					{	/* Ieee/number.scm 598 */
																						obj_t BgL_arg1755z00_1407;

																						BgL_arg1755z00_1407 =
																							BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																							(BgL_xz00_53);
																						{	/* Ieee/number.scm 598 */
																							long BgL_xz00_3466;

																							BgL_xz00_3466 =
																								BELONG_TO_LONG
																								(BgL_arg1755z00_1407);
																							BgL_arg1754z00_1406 =
																								bgl_long_to_bignum
																								(BgL_xz00_3466);
																					}}
																					return
																						(
																						(long) (bgl_bignum_cmp
																							(BgL_arg1754z00_1406,
																								BgL_yz00_54)) >= 0L);
																				}
																			else
																				{	/* Ieee/number.scm 598 */
																					return
																						CBOOL(BGl_errorz00zz__errorz00
																						(BGl_string3333z00zz__r4_numbers_6_5z00,
																							BGl_string3328z00zz__r4_numbers_6_5z00,
																							BgL_yz00_54));
																				}
																		}
																}
														}
												}
										}
								}
							else
								{	/* Ieee/number.scm 598 */
									if (LLONGP(BgL_xz00_53))
										{	/* Ieee/number.scm 598 */
											if (INTEGERP(BgL_yz00_54))
												{	/* Ieee/number.scm 598 */
													BGL_LONGLONG_T BgL_arg1759z00_1411;

													{	/* Ieee/number.scm 598 */
														long BgL_tmpz00_6868;

														BgL_tmpz00_6868 = (long) CINT(BgL_yz00_54);
														BgL_arg1759z00_1411 =
															LONG_TO_LLONG(BgL_tmpz00_6868);
													}
													return
														(BLLONG_TO_LLONG(BgL_xz00_53) >=
														BgL_arg1759z00_1411);
												}
											else
												{	/* Ieee/number.scm 598 */
													if (REALP(BgL_yz00_54))
														{	/* Ieee/number.scm 598 */
															return
																(
																(double) (BLLONG_TO_LLONG(BgL_xz00_53)) >=
																REAL_TO_DOUBLE(BgL_yz00_54));
														}
													else
														{	/* Ieee/number.scm 598 */
															if (LLONGP(BgL_yz00_54))
																{	/* Ieee/number.scm 598 */
																	return
																		(BLLONG_TO_LLONG(BgL_xz00_53) >=
																		BLLONG_TO_LLONG(BgL_yz00_54));
																}
															else
																{	/* Ieee/number.scm 598 */
																	if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00
																		(BgL_yz00_54))
																		{	/* Ieee/number.scm 598 */
																			BGL_LONGLONG_T BgL_arg1767z00_1419;

																			{	/* Ieee/number.scm 598 */
																				obj_t BgL_arg1768z00_1420;

																				BgL_arg1768z00_1420 =
																					BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																					(BgL_yz00_54);
																				BgL_arg1767z00_1419 =
																					(BGL_LONGLONG_T) (BELONG_TO_LONG
																					(BgL_arg1768z00_1420));
																			}
																			return
																				(BLLONG_TO_LLONG(BgL_xz00_53) >=
																				BgL_arg1767z00_1419);
																		}
																	else
																		{	/* Ieee/number.scm 598 */
																			if (BIGNUMP(BgL_yz00_54))
																				{	/* Ieee/number.scm 598 */
																					return
																						(
																						(long) (bgl_bignum_cmp
																							(bgl_llong_to_bignum
																								(BLLONG_TO_LLONG(BgL_xz00_53)),
																								BgL_yz00_54)) >= 0L);
																				}
																			else
																				{	/* Ieee/number.scm 598 */
																					if (BGL_UINT64P(BgL_yz00_54))
																						{	/* Ieee/number.scm 598 */
																							uint64_t BgL_arg1773z00_1425;

																							{	/* Ieee/number.scm 598 */
																								BGL_LONGLONG_T BgL_tmpz00_6900;

																								BgL_tmpz00_6900 =
																									BLLONG_TO_LLONG(BgL_xz00_53);
																								BgL_arg1773z00_1425 =
																									(uint64_t) (BgL_tmpz00_6900);
																							}
																							{	/* Ieee/number.scm 598 */
																								uint64_t BgL_n2z00_3487;

																								BgL_n2z00_3487 =
																									BGL_BINT64_TO_INT64
																									(BgL_yz00_54);
																								return (BgL_arg1773z00_1425 >=
																									BgL_n2z00_3487);
																							}
																						}
																					else
																						{	/* Ieee/number.scm 598 */
																							return
																								CBOOL(BGl_errorz00zz__errorz00
																								(BGl_string3333z00zz__r4_numbers_6_5z00,
																									BGl_string3328z00zz__r4_numbers_6_5z00,
																									BgL_yz00_54));
																						}
																				}
																		}
																}
														}
												}
										}
									else
										{	/* Ieee/number.scm 598 */
											if (BGL_UINT64P(BgL_xz00_53))
												{	/* Ieee/number.scm 598 */
													if (INTEGERP(BgL_yz00_54))
														{	/* Ieee/number.scm 598 */
															uint64_t BgL_arg1777z00_1428;

															{	/* Ieee/number.scm 598 */
																long BgL_tmpz00_6911;

																BgL_tmpz00_6911 = (long) CINT(BgL_yz00_54);
																BgL_arg1777z00_1428 =
																	(uint64_t) (BgL_tmpz00_6911);
															}
															{	/* Ieee/number.scm 598 */
																uint64_t BgL_n1z00_3489;

																BgL_n1z00_3489 =
																	BGL_BINT64_TO_INT64(BgL_xz00_53);
																return (BgL_n1z00_3489 >= BgL_arg1777z00_1428);
															}
														}
													else
														{	/* Ieee/number.scm 598 */
															if (BGL_UINT64P(BgL_yz00_54))
																{	/* Ieee/number.scm 598 */
																	uint64_t BgL_n1z00_3491;
																	uint64_t BgL_n2z00_3492;

																	BgL_n1z00_3491 =
																		BGL_BINT64_TO_INT64(BgL_xz00_53);
																	BgL_n2z00_3492 =
																		BGL_BINT64_TO_INT64(BgL_yz00_54);
																	return (BgL_n1z00_3491 >= BgL_n2z00_3492);
																}
															else
																{	/* Ieee/number.scm 598 */
																	if (REALP(BgL_yz00_54))
																		{	/* Ieee/number.scm 598 */
																			double BgL_arg1781z00_1431;

																			{	/* Ieee/number.scm 598 */
																				uint64_t BgL_tmpz00_6923;

																				BgL_tmpz00_6923 =
																					BGL_BINT64_TO_INT64(BgL_xz00_53);
																				BgL_arg1781z00_1431 =
																					(double) (BgL_tmpz00_6923);
																			}
																			return
																				(BgL_arg1781z00_1431 >=
																				REAL_TO_DOUBLE(BgL_yz00_54));
																		}
																	else
																		{	/* Ieee/number.scm 598 */
																			if (LLONGP(BgL_yz00_54))
																				{	/* Ieee/number.scm 598 */
																					uint64_t BgL_arg1783z00_1433;

																					{	/* Ieee/number.scm 598 */
																						BGL_LONGLONG_T BgL_tmpz00_6930;

																						BgL_tmpz00_6930 =
																							BLLONG_TO_LLONG(BgL_yz00_54);
																						BgL_arg1783z00_1433 =
																							(uint64_t) (BgL_tmpz00_6930);
																					}
																					{	/* Ieee/number.scm 598 */
																						uint64_t BgL_n1z00_3496;

																						BgL_n1z00_3496 =
																							BGL_BINT64_TO_INT64(BgL_xz00_53);
																						return
																							(BgL_n1z00_3496 >=
																							BgL_arg1783z00_1433);
																					}
																				}
																			else
																				{	/* Ieee/number.scm 598 */
																					if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_54))
																						{	/* Ieee/number.scm 598 */
																							uint64_t BgL_arg1787z00_1436;

																							{	/* Ieee/number.scm 598 */
																								BGL_LONGLONG_T
																									BgL_arg1788z00_1437;
																								{	/* Ieee/number.scm 598 */
																									obj_t BgL_arg1789z00_1438;

																									BgL_arg1789z00_1438 =
																										BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																										(BgL_yz00_54);
																									BgL_arg1788z00_1437 =
																										(BGL_LONGLONG_T)
																										(BELONG_TO_LONG
																										(BgL_arg1789z00_1438));
																								}
																								BgL_arg1787z00_1436 =
																									(uint64_t)
																									(BgL_arg1788z00_1437);
																							}
																							{	/* Ieee/number.scm 598 */
																								uint64_t BgL_n1z00_3499;

																								BgL_n1z00_3499 =
																									BGL_BINT64_TO_INT64
																									(BgL_xz00_53);
																								return (BgL_n1z00_3499 >=
																									BgL_arg1787z00_1436);
																							}
																						}
																					else
																						{	/* Ieee/number.scm 598 */
																							if (BIGNUMP(BgL_yz00_54))
																								{	/* Ieee/number.scm 598 */
																									long BgL_n1z00_3504;

																									BgL_n1z00_3504 =
																										(long) (bgl_bignum_cmp
																										(bgl_uint64_to_bignum
																											(BGL_BINT64_TO_INT64
																												(BgL_xz00_53)),
																											BgL_yz00_54));
																									return (BgL_n1z00_3504 >= 0L);
																								}
																							else
																								{	/* Ieee/number.scm 598 */
																									return
																										CBOOL
																										(BGl_errorz00zz__errorz00
																										(BGl_string3333z00zz__r4_numbers_6_5z00,
																											BGl_string3329z00zz__r4_numbers_6_5z00,
																											BgL_yz00_54));
																								}
																						}
																				}
																		}
																}
														}
												}
											else
												{	/* Ieee/number.scm 598 */
													if (BIGNUMP(BgL_xz00_53))
														{	/* Ieee/number.scm 598 */
															if (BIGNUMP(BgL_yz00_54))
																{	/* Ieee/number.scm 598 */
																	return
																		(
																		(long) (bgl_bignum_cmp(BgL_xz00_53,
																				BgL_yz00_54)) >= 0L);
																}
															else
																{	/* Ieee/number.scm 598 */
																	if (INTEGERP(BgL_yz00_54))
																		{	/* Ieee/number.scm 598 */
																			return
																				(
																				(long) (bgl_bignum_cmp(BgL_xz00_53,
																						bgl_long_to_bignum(
																							(long) CINT(BgL_yz00_54)))) >=
																				0L);
																		}
																	else
																		{	/* Ieee/number.scm 598 */
																			if (REALP(BgL_yz00_54))
																				{	/* Ieee/number.scm 598 */
																					return
																						(bgl_bignum_to_flonum(BgL_xz00_53)
																						>= REAL_TO_DOUBLE(BgL_yz00_54));
																				}
																			else
																				{	/* Ieee/number.scm 598 */
																					if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_54))
																						{	/* Ieee/number.scm 598 */
																							obj_t BgL_arg1799z00_1448;

																							{	/* Ieee/number.scm 598 */
																								obj_t BgL_arg1800z00_1449;

																								BgL_arg1800z00_1449 =
																									BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																									(BgL_yz00_54);
																								{	/* Ieee/number.scm 598 */
																									long BgL_xz00_3517;

																									BgL_xz00_3517 =
																										BELONG_TO_LONG
																										(BgL_arg1800z00_1449);
																									BgL_arg1799z00_1448 =
																										bgl_long_to_bignum
																										(BgL_xz00_3517);
																							}}
																							return
																								(
																								(long) (bgl_bignum_cmp
																									(BgL_xz00_53,
																										BgL_arg1799z00_1448)) >=
																								0L);
																						}
																					else
																						{	/* Ieee/number.scm 598 */
																							if (LLONGP(BgL_yz00_54))
																								{	/* Ieee/number.scm 598 */
																									return
																										(
																										(long) (bgl_bignum_cmp
																											(BgL_xz00_53,
																												bgl_llong_to_bignum
																												(BLLONG_TO_LLONG
																													(BgL_yz00_54)))) >=
																										0L);
																								}
																							else
																								{	/* Ieee/number.scm 598 */
																									if (BGL_UINT64P(BgL_yz00_54))
																										{	/* Ieee/number.scm 598 */
																											long BgL_n1z00_3530;

																											BgL_n1z00_3530 =
																												(long) (bgl_bignum_cmp
																												(BgL_xz00_53,
																													bgl_uint64_to_bignum
																													(BGL_BINT64_TO_INT64
																														(BgL_yz00_54))));
																											return (BgL_n1z00_3530 >=
																												0L);
																										}
																									else
																										{	/* Ieee/number.scm 598 */
																											return
																												CBOOL
																												(BGl_errorz00zz__errorz00
																												(BGl_string3333z00zz__r4_numbers_6_5z00,
																													BGl_string3328z00zz__r4_numbers_6_5z00,
																													BgL_yz00_54));
																										}
																								}
																						}
																				}
																		}
																}
														}
													else
														{	/* Ieee/number.scm 598 */
															return
																CBOOL(BGl_errorz00zz__errorz00
																(BGl_string3333z00zz__r4_numbers_6_5z00,
																	BGl_string3328z00zz__r4_numbers_6_5z00,
																	BgL_xz00_53));
														}
												}
										}
								}
						}
				}
		}

	}



/* &2>= */
	obj_t BGl_z622ze3zd3z52zz__r4_numbers_6_5z00(obj_t BgL_envz00_5034,
		obj_t BgL_xz00_5035, obj_t BgL_yz00_5036)
	{
		{	/* Ieee/number.scm 597 */
			return
				BBOOL(BGl_2ze3zd3z30zz__r4_numbers_6_5z00(BgL_xz00_5035,
					BgL_yz00_5036));
		}

	}



/* >= */
	BGL_EXPORTED_DEF bool_t BGl_ze3zd3z30zz__r4_numbers_6_5z00(obj_t BgL_xz00_55,
		obj_t BgL_yz00_56, obj_t BgL_za7za7_57)
	{
		{	/* Ieee/number.scm 603 */
			if (BGl_2ze3zd3z30zz__r4_numbers_6_5z00(BgL_xz00_55, BgL_yz00_56))
				{
					obj_t BgL_xz00_3548;
					obj_t BgL_za7za7_3549;

					BgL_xz00_3548 = BgL_yz00_56;
					BgL_za7za7_3549 = BgL_za7za7_57;
				BgL_ze3zd3zd2listze2_3547:
					if (NULLP(BgL_za7za7_3549))
						{	/* Ieee/number.scm 606 */
							return ((bool_t) 1);
						}
					else
						{	/* Ieee/number.scm 607 */
							bool_t BgL_test3815z00_7003;

							{	/* Ieee/number.scm 607 */
								obj_t BgL_arg1812z00_3557;

								BgL_arg1812z00_3557 = CAR(((obj_t) BgL_za7za7_3549));
								BgL_test3815z00_7003 =
									BGl_2ze3zd3z30zz__r4_numbers_6_5z00(BgL_xz00_3548,
									BgL_arg1812z00_3557);
							}
							if (BgL_test3815z00_7003)
								{
									obj_t BgL_za7za7_7010;
									obj_t BgL_xz00_7007;

									BgL_xz00_7007 = CAR(((obj_t) BgL_za7za7_3549));
									BgL_za7za7_7010 = CDR(((obj_t) BgL_za7za7_3549));
									BgL_za7za7_3549 = BgL_za7za7_7010;
									BgL_xz00_3548 = BgL_xz00_7007;
									goto BgL_ze3zd3zd2listze2_3547;
								}
							else
								{	/* Ieee/number.scm 607 */
									return ((bool_t) 0);
								}
						}
				}
			else
				{	/* Ieee/number.scm 609 */
					return ((bool_t) 0);
				}
		}

	}



/* &>= */
	obj_t BGl_z62ze3zd3z52zz__r4_numbers_6_5z00(obj_t BgL_envz00_5037,
		obj_t BgL_xz00_5038, obj_t BgL_yz00_5039, obj_t BgL_za7za7_5040)
	{
		{	/* Ieee/number.scm 603 */
			return
				BBOOL(BGl_ze3zd3z30zz__r4_numbers_6_5z00(BgL_xz00_5038, BgL_yz00_5039,
					BgL_za7za7_5040));
		}

	}



/* zero? */
	BGL_EXPORTED_DEF bool_t BGl_za7erozf3z54zz__r4_numbers_6_5z00(obj_t
		BgL_xz00_58)
	{
		{	/* Ieee/number.scm 614 */
			if (INTEGERP(BgL_xz00_58))
				{	/* Ieee/number.scm 616 */
					return ((long) CINT(BgL_xz00_58) == 0L);
				}
			else
				{	/* Ieee/number.scm 616 */
					if (REALP(BgL_xz00_58))
						{	/* Ieee/number.scm 617 */
							return (REAL_TO_DOUBLE(BgL_xz00_58) == ((double) 0.0));
						}
					else
						{	/* Ieee/number.scm 617 */
							if (ELONGP(BgL_xz00_58))
								{	/* Ieee/number.scm 618 */
									long BgL_n1z00_3568;

									BgL_n1z00_3568 = BELONG_TO_LONG(BgL_xz00_58);
									return (BgL_n1z00_3568 == ((long) 0));
								}
							else
								{	/* Ieee/number.scm 618 */
									if (LLONGP(BgL_xz00_58))
										{	/* Ieee/number.scm 619 */
											return
												(BLLONG_TO_LLONG(BgL_xz00_58) == ((BGL_LONGLONG_T) 0));
										}
									else
										{	/* Ieee/number.scm 619 */
											if (BIGNUMP(BgL_xz00_58))
												{	/* Ieee/number.scm 620 */
													return BXZERO(BgL_xz00_58);
												}
											else
												{	/* Ieee/number.scm 620 */
													return
														CBOOL(BGl_errorz00zz__errorz00
														(BGl_string3334z00zz__r4_numbers_6_5z00,
															BGl_string3328z00zz__r4_numbers_6_5z00,
															BgL_xz00_58));
												}
										}
								}
						}
				}
		}

	}



/* &zero? */
	obj_t BGl_z62za7erozf3z36zz__r4_numbers_6_5z00(obj_t BgL_envz00_5041,
		obj_t BgL_xz00_5042)
	{
		{	/* Ieee/number.scm 614 */
			return BBOOL(BGl_za7erozf3z54zz__r4_numbers_6_5z00(BgL_xz00_5042));
		}

	}



/* positive? */
	BGL_EXPORTED_DEF bool_t BGl_positivezf3zf3zz__r4_numbers_6_5z00(obj_t
		BgL_xz00_59)
	{
		{	/* Ieee/number.scm 626 */
			if (INTEGERP(BgL_xz00_59))
				{	/* Ieee/number.scm 628 */
					return ((long) CINT(BgL_xz00_59) > 0L);
				}
			else
				{	/* Ieee/number.scm 628 */
					if (REALP(BgL_xz00_59))
						{	/* Ieee/number.scm 629 */
							return (REAL_TO_DOUBLE(BgL_xz00_59) > ((double) 0.0));
						}
					else
						{	/* Ieee/number.scm 629 */
							if (ELONGP(BgL_xz00_59))
								{	/* Ieee/number.scm 630 */
									long BgL_n1z00_3578;

									BgL_n1z00_3578 = BELONG_TO_LONG(BgL_xz00_59);
									return (BgL_n1z00_3578 > ((long) 0));
								}
							else
								{	/* Ieee/number.scm 630 */
									if (LLONGP(BgL_xz00_59))
										{	/* Ieee/number.scm 631 */
											return
												(BLLONG_TO_LLONG(BgL_xz00_59) > ((BGL_LONGLONG_T) 0));
										}
									else
										{	/* Ieee/number.scm 631 */
											if (BIGNUMP(BgL_xz00_59))
												{	/* Ieee/number.scm 632 */
													return BXPOSITIVE(BgL_xz00_59);
												}
											else
												{	/* Ieee/number.scm 632 */
													return
														CBOOL(BGl_errorz00zz__errorz00
														(BGl_string3335z00zz__r4_numbers_6_5z00,
															BGl_string3328z00zz__r4_numbers_6_5z00,
															BgL_xz00_59));
												}
										}
								}
						}
				}
		}

	}



/* &positive? */
	obj_t BGl_z62positivezf3z91zz__r4_numbers_6_5z00(obj_t BgL_envz00_5043,
		obj_t BgL_xz00_5044)
	{
		{	/* Ieee/number.scm 626 */
			return BBOOL(BGl_positivezf3zf3zz__r4_numbers_6_5z00(BgL_xz00_5044));
		}

	}



/* negative? */
	BGL_EXPORTED_DEF bool_t BGl_negativezf3zf3zz__r4_numbers_6_5z00(obj_t
		BgL_xz00_60)
	{
		{	/* Ieee/number.scm 638 */
			if (INTEGERP(BgL_xz00_60))
				{	/* Ieee/number.scm 640 */
					return ((long) CINT(BgL_xz00_60) < 0L);
				}
			else
				{	/* Ieee/number.scm 640 */
					if (REALP(BgL_xz00_60))
						{	/* Ieee/number.scm 641 */
							return (REAL_TO_DOUBLE(BgL_xz00_60) < ((double) 0.0));
						}
					else
						{	/* Ieee/number.scm 641 */
							if (ELONGP(BgL_xz00_60))
								{	/* Ieee/number.scm 642 */
									long BgL_n1z00_3588;

									BgL_n1z00_3588 = BELONG_TO_LONG(BgL_xz00_60);
									return (BgL_n1z00_3588 < ((long) 0));
								}
							else
								{	/* Ieee/number.scm 642 */
									if (LLONGP(BgL_xz00_60))
										{	/* Ieee/number.scm 643 */
											return
												(BLLONG_TO_LLONG(BgL_xz00_60) < ((BGL_LONGLONG_T) 0));
										}
									else
										{	/* Ieee/number.scm 643 */
											if (BIGNUMP(BgL_xz00_60))
												{	/* Ieee/number.scm 644 */
													return BXNEGATIVE(BgL_xz00_60);
												}
											else
												{	/* Ieee/number.scm 644 */
													return
														CBOOL(BGl_errorz00zz__errorz00
														(BGl_string3336z00zz__r4_numbers_6_5z00,
															BGl_string3328z00zz__r4_numbers_6_5z00,
															BgL_xz00_60));
												}
										}
								}
						}
				}
		}

	}



/* &negative? */
	obj_t BGl_z62negativezf3z91zz__r4_numbers_6_5z00(obj_t BgL_envz00_5045,
		obj_t BgL_xz00_5046)
	{
		{	/* Ieee/number.scm 638 */
			return BBOOL(BGl_negativezf3zf3zz__r4_numbers_6_5z00(BgL_xz00_5046));
		}

	}



/* 2max */
	BGL_EXPORTED_DEF obj_t BGl_2maxz00zz__r4_numbers_6_5z00(obj_t BgL_xz00_73,
		obj_t BgL_yz00_74)
	{
		{	/* Ieee/number.scm 666 */
			if (INTEGERP(BgL_xz00_73))
				{	/* Ieee/number.scm 667 */
					if (INTEGERP(BgL_yz00_74))
						{	/* Ieee/number.scm 667 */
							if (((long) CINT(BgL_xz00_73) > (long) CINT(BgL_yz00_74)))
								{	/* Ieee/number.scm 651 */
									return BgL_xz00_73;
								}
							else
								{	/* Ieee/number.scm 651 */
									return BgL_yz00_74;
								}
						}
					else
						{	/* Ieee/number.scm 667 */
							if (REALP(BgL_yz00_74))
								{	/* Ieee/number.scm 667 */
									double BgL_arg1837z00_1491;

									BgL_arg1837z00_1491 = (double) ((long) CINT(BgL_xz00_73));
									if ((BgL_arg1837z00_1491 > REAL_TO_DOUBLE(BgL_yz00_74)))
										{	/* Ieee/number.scm 653 */
											return DOUBLE_TO_REAL(BgL_arg1837z00_1491);
										}
									else
										{	/* Ieee/number.scm 653 */
											return BgL_yz00_74;
										}
								}
							else
								{	/* Ieee/number.scm 667 */
									if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_74))
										{	/* Ieee/number.scm 667 */
											long BgL_arg1839z00_1493;
											obj_t BgL_arg1840z00_1494;

											{	/* Ieee/number.scm 667 */
												long BgL_tmpz00_7102;

												BgL_tmpz00_7102 = (long) CINT(BgL_xz00_73);
												BgL_arg1839z00_1493 = (long) (BgL_tmpz00_7102);
											}
											BgL_arg1840z00_1494 =
												BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
												(BgL_yz00_74);
											{	/* Ieee/number.scm 655 */
												bool_t BgL_test3837z00_7106;

												{	/* Ieee/number.scm 655 */
													long BgL_n2z00_3622;

													BgL_n2z00_3622 = BELONG_TO_LONG(BgL_arg1840z00_1494);
													BgL_test3837z00_7106 =
														(BgL_arg1839z00_1493 > BgL_n2z00_3622);
												}
												if (BgL_test3837z00_7106)
													{	/* Ieee/number.scm 655 */
														return make_belong(BgL_arg1839z00_1493);
													}
												else
													{	/* Ieee/number.scm 655 */
														return BgL_arg1840z00_1494;
													}
											}
										}
									else
										{	/* Ieee/number.scm 667 */
											if (LLONGP(BgL_yz00_74))
												{	/* Ieee/number.scm 667 */
													BGL_LONGLONG_T BgL_arg1842z00_1496;

													{	/* Ieee/number.scm 667 */
														long BgL_tmpz00_7112;

														BgL_tmpz00_7112 = (long) CINT(BgL_xz00_73);
														BgL_arg1842z00_1496 =
															LONG_TO_LLONG(BgL_tmpz00_7112);
													}
													if (
														(BgL_arg1842z00_1496 >
															BLLONG_TO_LLONG(BgL_yz00_74)))
														{	/* Ieee/number.scm 657 */
															return make_bllong(BgL_arg1842z00_1496);
														}
													else
														{	/* Ieee/number.scm 657 */
															return BgL_yz00_74;
														}
												}
											else
												{	/* Ieee/number.scm 667 */
													if (BGL_UINT64P(BgL_yz00_74))
														{	/* Ieee/number.scm 667 */
															uint64_t BgL_arg1845z00_1499;

															{	/* Ieee/number.scm 667 */
																BGL_LONGLONG_T BgL_arg1846z00_1500;

																{	/* Ieee/number.scm 667 */
																	long BgL_tmpz00_7121;

																	BgL_tmpz00_7121 = (long) CINT(BgL_xz00_73);
																	BgL_arg1846z00_1500 =
																		LONG_TO_LLONG(BgL_tmpz00_7121);
																}
																BgL_arg1845z00_1499 =
																	(uint64_t) (BgL_arg1846z00_1500);
															}
															{	/* Ieee/number.scm 661 */
																bool_t BgL_test3841z00_7125;

																{	/* Ieee/number.scm 661 */
																	uint64_t BgL_n2z00_3631;

																	BgL_n2z00_3631 =
																		BGL_BINT64_TO_INT64(BgL_yz00_74);
																	BgL_test3841z00_7125 =
																		(BgL_arg1845z00_1499 > BgL_n2z00_3631);
																}
																if (BgL_test3841z00_7125)
																	{	/* Ieee/number.scm 661 */
																		return
																			BGL_UINT64_TO_BUINT64
																			(BgL_arg1845z00_1499);
																	}
																else
																	{	/* Ieee/number.scm 661 */
																		return BgL_yz00_74;
																	}
															}
														}
													else
														{	/* Ieee/number.scm 667 */
															if (BIGNUMP(BgL_yz00_74))
																{	/* Ieee/number.scm 667 */
																	obj_t BgL_arg1848z00_1502;

																	BgL_arg1848z00_1502 =
																		bgl_long_to_bignum(
																		(long) CINT(BgL_xz00_73));
																	if (
																		((long) (bgl_bignum_cmp(BgL_arg1848z00_1502,
																					BgL_yz00_74)) > 0L))
																		{	/* Ieee/number.scm 659 */
																			return BgL_arg1848z00_1502;
																		}
																	else
																		{	/* Ieee/number.scm 659 */
																			return BgL_yz00_74;
																		}
																}
															else
																{	/* Ieee/number.scm 667 */
																	return
																		BGl_errorz00zz__errorz00
																		(BGl_string3337z00zz__r4_numbers_6_5z00,
																		BGl_string3328z00zz__r4_numbers_6_5z00,
																		BgL_yz00_74);
																}
														}
												}
										}
								}
						}
				}
			else
				{	/* Ieee/number.scm 667 */
					if (REALP(BgL_xz00_73))
						{	/* Ieee/number.scm 667 */
							if (REALP(BgL_yz00_74))
								{	/* Ieee/number.scm 667 */
									if (
										(REAL_TO_DOUBLE(BgL_xz00_73) > REAL_TO_DOUBLE(BgL_yz00_74)))
										{	/* Ieee/number.scm 653 */
											return BgL_xz00_73;
										}
									else
										{	/* Ieee/number.scm 653 */
											return BgL_yz00_74;
										}
								}
							else
								{	/* Ieee/number.scm 667 */
									if (INTEGERP(BgL_yz00_74))
										{	/* Ieee/number.scm 667 */
											double BgL_arg1852z00_1506;

											BgL_arg1852z00_1506 = (double) ((long) CINT(BgL_yz00_74));
											if ((REAL_TO_DOUBLE(BgL_xz00_73) > BgL_arg1852z00_1506))
												{	/* Ieee/number.scm 653 */
													return BgL_xz00_73;
												}
											else
												{	/* Ieee/number.scm 653 */
													return DOUBLE_TO_REAL(BgL_arg1852z00_1506);
												}
										}
									else
										{	/* Ieee/number.scm 667 */
											if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00
												(BgL_yz00_74))
												{	/* Ieee/number.scm 667 */
													double BgL_arg1854z00_1508;

													{	/* Ieee/number.scm 667 */
														obj_t BgL_arg1856z00_1509;

														BgL_arg1856z00_1509 =
															BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
															(BgL_yz00_74);
														BgL_arg1854z00_1508 =
															(double) (BELONG_TO_LONG(BgL_arg1856z00_1509));
													}
													if (
														(REAL_TO_DOUBLE(BgL_xz00_73) > BgL_arg1854z00_1508))
														{	/* Ieee/number.scm 653 */
															return BgL_xz00_73;
														}
													else
														{	/* Ieee/number.scm 653 */
															return DOUBLE_TO_REAL(BgL_arg1854z00_1508);
														}
												}
											else
												{	/* Ieee/number.scm 667 */
													if (LLONGP(BgL_yz00_74))
														{	/* Ieee/number.scm 667 */
															double BgL_arg1858z00_1511;

															BgL_arg1858z00_1511 =
																(double) (BLLONG_TO_LLONG(BgL_yz00_74));
															if (
																(REAL_TO_DOUBLE(BgL_xz00_73) >
																	BgL_arg1858z00_1511))
																{	/* Ieee/number.scm 653 */
																	return BgL_xz00_73;
																}
															else
																{	/* Ieee/number.scm 653 */
																	return DOUBLE_TO_REAL(BgL_arg1858z00_1511);
																}
														}
													else
														{	/* Ieee/number.scm 667 */
															if (BGL_UINT64P(BgL_yz00_74))
																{	/* Ieee/number.scm 667 */
																	double BgL_arg1862z00_1514;

																	{	/* Ieee/number.scm 667 */
																		uint64_t BgL_xz00_3650;

																		BgL_xz00_3650 =
																			BGL_BINT64_TO_INT64(BgL_yz00_74);
																		BgL_arg1862z00_1514 =
																			(double) (BgL_xz00_3650);
																	}
																	if (
																		(REAL_TO_DOUBLE(BgL_xz00_73) >
																			BgL_arg1862z00_1514))
																		{	/* Ieee/number.scm 653 */
																			return BgL_xz00_73;
																		}
																	else
																		{	/* Ieee/number.scm 653 */
																			return
																				DOUBLE_TO_REAL(BgL_arg1862z00_1514);
																		}
																}
															else
																{	/* Ieee/number.scm 667 */
																	if (BIGNUMP(BgL_yz00_74))
																		{	/* Ieee/number.scm 667 */
																			double BgL_arg1864z00_1516;

																			BgL_arg1864z00_1516 =
																				bgl_bignum_to_flonum(BgL_yz00_74);
																			if (
																				(REAL_TO_DOUBLE(BgL_xz00_73) >
																					BgL_arg1864z00_1516))
																				{	/* Ieee/number.scm 653 */
																					return BgL_xz00_73;
																				}
																			else
																				{	/* Ieee/number.scm 653 */
																					return
																						DOUBLE_TO_REAL(BgL_arg1864z00_1516);
																				}
																		}
																	else
																		{	/* Ieee/number.scm 667 */
																			return
																				BGl_errorz00zz__errorz00
																				(BGl_string3337z00zz__r4_numbers_6_5z00,
																				BGl_string3328z00zz__r4_numbers_6_5z00,
																				BgL_yz00_74);
																		}
																}
														}
												}
										}
								}
						}
					else
						{	/* Ieee/number.scm 667 */
							if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_xz00_73))
								{	/* Ieee/number.scm 667 */
									if (INTEGERP(BgL_yz00_74))
										{	/* Ieee/number.scm 667 */
											obj_t BgL_arg1868z00_1519;
											long BgL_arg1869z00_1520;

											BgL_arg1868z00_1519 =
												BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
												(BgL_xz00_73);
											{	/* Ieee/number.scm 667 */
												long BgL_tmpz00_7192;

												BgL_tmpz00_7192 = (long) CINT(BgL_yz00_74);
												BgL_arg1869z00_1520 = (long) (BgL_tmpz00_7192);
											}
											{	/* Ieee/number.scm 655 */
												bool_t BgL_test3859z00_7195;

												{	/* Ieee/number.scm 655 */
													long BgL_n1z00_3660;

													BgL_n1z00_3660 = BELONG_TO_LONG(BgL_arg1868z00_1519);
													BgL_test3859z00_7195 =
														(BgL_n1z00_3660 > BgL_arg1869z00_1520);
												}
												if (BgL_test3859z00_7195)
													{	/* Ieee/number.scm 655 */
														return BgL_arg1868z00_1519;
													}
												else
													{	/* Ieee/number.scm 655 */
														return make_belong(BgL_arg1869z00_1520);
													}
											}
										}
									else
										{	/* Ieee/number.scm 667 */
											if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00
												(BgL_yz00_74))
												{	/* Ieee/number.scm 667 */
													obj_t BgL_arg1872z00_1522;
													obj_t BgL_arg1873z00_1523;

													BgL_arg1872z00_1522 =
														BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
														(BgL_xz00_73);
													BgL_arg1873z00_1523 =
														BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
														(BgL_yz00_74);
													{	/* Ieee/number.scm 655 */
														bool_t BgL_test3861z00_7203;

														{	/* Ieee/number.scm 655 */
															long BgL_n1z00_3663;
															long BgL_n2z00_3664;

															BgL_n1z00_3663 =
																BELONG_TO_LONG(BgL_arg1872z00_1522);
															BgL_n2z00_3664 =
																BELONG_TO_LONG(BgL_arg1873z00_1523);
															BgL_test3861z00_7203 =
																(BgL_n1z00_3663 > BgL_n2z00_3664);
														}
														if (BgL_test3861z00_7203)
															{	/* Ieee/number.scm 655 */
																return BgL_arg1872z00_1522;
															}
														else
															{	/* Ieee/number.scm 655 */
																return BgL_arg1873z00_1523;
															}
													}
												}
											else
												{	/* Ieee/number.scm 667 */
													if (REALP(BgL_yz00_74))
														{	/* Ieee/number.scm 667 */
															double BgL_arg1875z00_1525;

															{	/* Ieee/number.scm 667 */
																obj_t BgL_arg1876z00_1526;

																BgL_arg1876z00_1526 =
																	BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																	(BgL_xz00_73);
																BgL_arg1875z00_1525 =
																	(double) (BELONG_TO_LONG
																	(BgL_arg1876z00_1526));
															}
															if (
																(BgL_arg1875z00_1525 >
																	REAL_TO_DOUBLE(BgL_yz00_74)))
																{	/* Ieee/number.scm 653 */
																	return DOUBLE_TO_REAL(BgL_arg1875z00_1525);
																}
															else
																{	/* Ieee/number.scm 653 */
																	return BgL_yz00_74;
																}
														}
													else
														{	/* Ieee/number.scm 667 */
															if (LLONGP(BgL_yz00_74))
																{	/* Ieee/number.scm 667 */
																	BGL_LONGLONG_T BgL_arg1878z00_1528;

																	{	/* Ieee/number.scm 667 */
																		obj_t BgL_arg1880z00_1530;

																		BgL_arg1880z00_1530 =
																			BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																			(BgL_xz00_73);
																		BgL_arg1878z00_1528 =
																			(BGL_LONGLONG_T) (BELONG_TO_LONG
																			(BgL_arg1880z00_1530));
																	}
																	if (
																		(BgL_arg1878z00_1528 >
																			BLLONG_TO_LLONG(BgL_yz00_74)))
																		{	/* Ieee/number.scm 657 */
																			return make_bllong(BgL_arg1878z00_1528);
																		}
																	else
																		{	/* Ieee/number.scm 657 */
																			return BgL_yz00_74;
																		}
																}
															else
																{	/* Ieee/number.scm 667 */
																	if (BGL_UINT64P(BgL_yz00_74))
																		{	/* Ieee/number.scm 667 */
																			uint64_t BgL_arg1882z00_1532;

																			{	/* Ieee/number.scm 667 */
																				BGL_LONGLONG_T BgL_arg1883z00_1533;

																				{	/* Ieee/number.scm 667 */
																					obj_t BgL_arg1884z00_1534;

																					BgL_arg1884z00_1534 =
																						BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																						(BgL_xz00_73);
																					BgL_arg1883z00_1533 =
																						(BGL_LONGLONG_T) (BELONG_TO_LONG
																						(BgL_arg1884z00_1534));
																				}
																				BgL_arg1882z00_1532 =
																					(uint64_t) (BgL_arg1883z00_1533);
																			}
																			{	/* Ieee/number.scm 661 */
																				bool_t BgL_test3867z00_7231;

																				{	/* Ieee/number.scm 661 */
																					uint64_t BgL_n2z00_3674;

																					BgL_n2z00_3674 =
																						BGL_BINT64_TO_INT64(BgL_yz00_74);
																					BgL_test3867z00_7231 =
																						(BgL_arg1882z00_1532 >
																						BgL_n2z00_3674);
																				}
																				if (BgL_test3867z00_7231)
																					{	/* Ieee/number.scm 661 */
																						return
																							BGL_UINT64_TO_BUINT64
																							(BgL_arg1882z00_1532);
																					}
																				else
																					{	/* Ieee/number.scm 661 */
																						return BgL_yz00_74;
																					}
																			}
																		}
																	else
																		{	/* Ieee/number.scm 667 */
																			if (BIGNUMP(BgL_yz00_74))
																				{	/* Ieee/number.scm 667 */
																					obj_t BgL_arg1887z00_1536;

																					{	/* Ieee/number.scm 667 */
																						obj_t BgL_arg1888z00_1537;

																						BgL_arg1888z00_1537 =
																							BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																							(BgL_xz00_73);
																						{	/* Ieee/number.scm 667 */
																							long BgL_xz00_3675;

																							BgL_xz00_3675 =
																								BELONG_TO_LONG
																								(BgL_arg1888z00_1537);
																							BgL_arg1887z00_1536 =
																								bgl_long_to_bignum
																								(BgL_xz00_3675);
																					}}
																					if (
																						((long) (bgl_bignum_cmp
																								(BgL_arg1887z00_1536,
																									BgL_yz00_74)) > 0L))
																						{	/* Ieee/number.scm 659 */
																							return BgL_arg1887z00_1536;
																						}
																					else
																						{	/* Ieee/number.scm 659 */
																							return BgL_yz00_74;
																						}
																				}
																			else
																				{	/* Ieee/number.scm 667 */
																					return
																						BGl_errorz00zz__errorz00
																						(BGl_string3337z00zz__r4_numbers_6_5z00,
																						BGl_string3328z00zz__r4_numbers_6_5z00,
																						BgL_yz00_74);
																				}
																		}
																}
														}
												}
										}
								}
							else
								{	/* Ieee/number.scm 667 */
									if (LLONGP(BgL_xz00_73))
										{	/* Ieee/number.scm 667 */
											if (INTEGERP(BgL_yz00_74))
												{	/* Ieee/number.scm 667 */
													BGL_LONGLONG_T BgL_arg1892z00_1541;

													{	/* Ieee/number.scm 667 */
														long BgL_tmpz00_7249;

														BgL_tmpz00_7249 = (long) CINT(BgL_yz00_74);
														BgL_arg1892z00_1541 =
															LONG_TO_LLONG(BgL_tmpz00_7249);
													}
													if (
														(BLLONG_TO_LLONG(BgL_xz00_73) >
															BgL_arg1892z00_1541))
														{	/* Ieee/number.scm 657 */
															return BgL_xz00_73;
														}
													else
														{	/* Ieee/number.scm 657 */
															return make_bllong(BgL_arg1892z00_1541);
														}
												}
											else
												{	/* Ieee/number.scm 667 */
													if (REALP(BgL_yz00_74))
														{	/* Ieee/number.scm 667 */
															double BgL_arg1894z00_1543;

															BgL_arg1894z00_1543 =
																(double) (BLLONG_TO_LLONG(BgL_xz00_73));
															if (
																(BgL_arg1894z00_1543 >
																	REAL_TO_DOUBLE(BgL_yz00_74)))
																{	/* Ieee/number.scm 653 */
																	return DOUBLE_TO_REAL(BgL_arg1894z00_1543);
																}
															else
																{	/* Ieee/number.scm 653 */
																	return BgL_yz00_74;
																}
														}
													else
														{	/* Ieee/number.scm 667 */
															if (LLONGP(BgL_yz00_74))
																{	/* Ieee/number.scm 667 */
																	if (
																		(BLLONG_TO_LLONG(BgL_xz00_73) >
																			BLLONG_TO_LLONG(BgL_yz00_74)))
																		{	/* Ieee/number.scm 657 */
																			return BgL_xz00_73;
																		}
																	else
																		{	/* Ieee/number.scm 657 */
																			return BgL_yz00_74;
																		}
																}
															else
																{	/* Ieee/number.scm 667 */
																	if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00
																		(BgL_yz00_74))
																		{	/* Ieee/number.scm 667 */
																			BGL_LONGLONG_T BgL_arg1901z00_1549;

																			{	/* Ieee/number.scm 667 */
																				obj_t BgL_arg1902z00_1550;

																				BgL_arg1902z00_1550 =
																					BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																					(BgL_yz00_74);
																				BgL_arg1901z00_1549 =
																					(BGL_LONGLONG_T) (BELONG_TO_LONG
																					(BgL_arg1902z00_1550));
																			}
																			if (
																				(BLLONG_TO_LLONG(BgL_xz00_73) >
																					BgL_arg1901z00_1549))
																				{	/* Ieee/number.scm 657 */
																					return BgL_xz00_73;
																				}
																			else
																				{	/* Ieee/number.scm 657 */
																					return
																						make_bllong(BgL_arg1901z00_1549);
																				}
																		}
																	else
																		{	/* Ieee/number.scm 667 */
																			if (BIGNUMP(BgL_yz00_74))
																				{	/* Ieee/number.scm 667 */
																					obj_t BgL_arg1904z00_1552;

																					BgL_arg1904z00_1552 =
																						bgl_llong_to_bignum(BLLONG_TO_LLONG
																						(BgL_xz00_73));
																					if (((long) (bgl_bignum_cmp
																								(BgL_arg1904z00_1552,
																									BgL_yz00_74)) > 0L))
																						{	/* Ieee/number.scm 659 */
																							return BgL_arg1904z00_1552;
																						}
																					else
																						{	/* Ieee/number.scm 659 */
																							return BgL_yz00_74;
																						}
																				}
																			else
																				{	/* Ieee/number.scm 667 */
																					if (BGL_UINT64P(BgL_yz00_74))
																						{	/* Ieee/number.scm 667 */
																							uint64_t BgL_arg1910z00_1555;

																							{	/* Ieee/number.scm 667 */
																								BGL_LONGLONG_T BgL_tmpz00_7289;

																								BgL_tmpz00_7289 =
																									BLLONG_TO_LLONG(BgL_xz00_73);
																								BgL_arg1910z00_1555 =
																									(uint64_t) (BgL_tmpz00_7289);
																							}
																							{	/* Ieee/number.scm 661 */
																								bool_t BgL_test3882z00_7292;

																								{	/* Ieee/number.scm 661 */
																									uint64_t BgL_n2z00_3703;

																									BgL_n2z00_3703 =
																										BGL_BINT64_TO_INT64
																										(BgL_yz00_74);
																									BgL_test3882z00_7292 =
																										(BgL_arg1910z00_1555 >
																										BgL_n2z00_3703);
																								}
																								if (BgL_test3882z00_7292)
																									{	/* Ieee/number.scm 661 */
																										return
																											BGL_UINT64_TO_BUINT64
																											(BgL_arg1910z00_1555);
																									}
																								else
																									{	/* Ieee/number.scm 661 */
																										return BgL_yz00_74;
																									}
																							}
																						}
																					else
																						{	/* Ieee/number.scm 667 */
																							return
																								BGl_errorz00zz__errorz00
																								(BGl_string3337z00zz__r4_numbers_6_5z00,
																								BGl_string3328z00zz__r4_numbers_6_5z00,
																								BgL_yz00_74);
																						}
																				}
																		}
																}
														}
												}
										}
									else
										{	/* Ieee/number.scm 667 */
											if (BGL_UINT64P(BgL_xz00_73))
												{	/* Ieee/number.scm 667 */
													if (INTEGERP(BgL_yz00_74))
														{	/* Ieee/number.scm 667 */
															uint64_t BgL_arg1913z00_1558;

															{	/* Ieee/number.scm 667 */
																long BgL_tmpz00_7301;

																BgL_tmpz00_7301 = (long) CINT(BgL_yz00_74);
																BgL_arg1913z00_1558 =
																	(uint64_t) (BgL_tmpz00_7301);
															}
															{	/* Ieee/number.scm 661 */
																bool_t BgL_test3885z00_7304;

																{	/* Ieee/number.scm 661 */
																	uint64_t BgL_n1z00_3706;

																	BgL_n1z00_3706 =
																		BGL_BINT64_TO_INT64(BgL_xz00_73);
																	BgL_test3885z00_7304 =
																		(BgL_n1z00_3706 > BgL_arg1913z00_1558);
																}
																if (BgL_test3885z00_7304)
																	{	/* Ieee/number.scm 661 */
																		return BgL_xz00_73;
																	}
																else
																	{	/* Ieee/number.scm 661 */
																		return
																			BGL_UINT64_TO_BUINT64
																			(BgL_arg1913z00_1558);
																	}
															}
														}
													else
														{	/* Ieee/number.scm 667 */
															if (BGL_UINT64P(BgL_yz00_74))
																{	/* Ieee/number.scm 661 */
																	bool_t BgL_test3887z00_7310;

																	{	/* Ieee/number.scm 661 */
																		uint64_t BgL_n1z00_3709;
																		uint64_t BgL_n2z00_3710;

																		BgL_n1z00_3709 =
																			BGL_BINT64_TO_INT64(BgL_xz00_73);
																		BgL_n2z00_3710 =
																			BGL_BINT64_TO_INT64(BgL_yz00_74);
																		BgL_test3887z00_7310 =
																			(BgL_n1z00_3709 > BgL_n2z00_3710);
																	}
																	if (BgL_test3887z00_7310)
																		{	/* Ieee/number.scm 661 */
																			return BgL_xz00_73;
																		}
																	else
																		{	/* Ieee/number.scm 661 */
																			return BgL_yz00_74;
																		}
																}
															else
																{	/* Ieee/number.scm 667 */
																	if (REALP(BgL_yz00_74))
																		{	/* Ieee/number.scm 667 */
																			double BgL_arg1916z00_1561;

																			{	/* Ieee/number.scm 667 */
																				uint64_t BgL_tmpz00_7316;

																				BgL_tmpz00_7316 =
																					BGL_BINT64_TO_INT64(BgL_xz00_73);
																				BgL_arg1916z00_1561 =
																					(double) (BgL_tmpz00_7316);
																			}
																			if (
																				(BgL_arg1916z00_1561 >
																					REAL_TO_DOUBLE(BgL_yz00_74)))
																				{	/* Ieee/number.scm 653 */
																					return
																						DOUBLE_TO_REAL(BgL_arg1916z00_1561);
																				}
																			else
																				{	/* Ieee/number.scm 653 */
																					return BgL_yz00_74;
																				}
																		}
																	else
																		{	/* Ieee/number.scm 667 */
																			if (LLONGP(BgL_yz00_74))
																				{	/* Ieee/number.scm 667 */
																					uint64_t BgL_arg1918z00_1563;

																					{	/* Ieee/number.scm 667 */
																						BGL_LONGLONG_T BgL_tmpz00_7325;

																						BgL_tmpz00_7325 =
																							BLLONG_TO_LLONG(BgL_yz00_74);
																						BgL_arg1918z00_1563 =
																							(uint64_t) (BgL_tmpz00_7325);
																					}
																					{	/* Ieee/number.scm 661 */
																						bool_t BgL_test3891z00_7328;

																						{	/* Ieee/number.scm 661 */
																							uint64_t BgL_n1z00_3716;

																							BgL_n1z00_3716 =
																								BGL_BINT64_TO_INT64
																								(BgL_xz00_73);
																							BgL_test3891z00_7328 =
																								(BgL_n1z00_3716 >
																								BgL_arg1918z00_1563);
																						}
																						if (BgL_test3891z00_7328)
																							{	/* Ieee/number.scm 661 */
																								return BgL_xz00_73;
																							}
																						else
																							{	/* Ieee/number.scm 661 */
																								return
																									BGL_UINT64_TO_BUINT64
																									(BgL_arg1918z00_1563);
																							}
																					}
																				}
																			else
																				{	/* Ieee/number.scm 667 */
																					if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_74))
																						{	/* Ieee/number.scm 667 */
																							uint64_t BgL_arg1923z00_1566;

																							{	/* Ieee/number.scm 667 */
																								BGL_LONGLONG_T
																									BgL_arg1924z00_1567;
																								{	/* Ieee/number.scm 667 */
																									obj_t BgL_arg1925z00_1568;

																									BgL_arg1925z00_1568 =
																										BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																										(BgL_yz00_74);
																									BgL_arg1924z00_1567 =
																										(BGL_LONGLONG_T)
																										(BELONG_TO_LONG
																										(BgL_arg1925z00_1568));
																								}
																								BgL_arg1923z00_1566 =
																									(uint64_t)
																									(BgL_arg1924z00_1567);
																							}
																							{	/* Ieee/number.scm 661 */
																								bool_t BgL_test3893z00_7338;

																								{	/* Ieee/number.scm 661 */
																									uint64_t BgL_n1z00_3720;

																									BgL_n1z00_3720 =
																										BGL_BINT64_TO_INT64
																										(BgL_xz00_73);
																									BgL_test3893z00_7338 =
																										(BgL_n1z00_3720 >
																										BgL_arg1923z00_1566);
																								}
																								if (BgL_test3893z00_7338)
																									{	/* Ieee/number.scm 661 */
																										return BgL_xz00_73;
																									}
																								else
																									{	/* Ieee/number.scm 661 */
																										return
																											BGL_UINT64_TO_BUINT64
																											(BgL_arg1923z00_1566);
																									}
																							}
																						}
																					else
																						{	/* Ieee/number.scm 667 */
																							if (BIGNUMP(BgL_yz00_74))
																								{	/* Ieee/number.scm 667 */
																									obj_t BgL_arg1927z00_1570;

																									BgL_arg1927z00_1570 =
																										bgl_uint64_to_bignum
																										(BGL_BINT64_TO_INT64
																										(BgL_xz00_73));
																									if (((long) (bgl_bignum_cmp
																												(BgL_arg1927z00_1570,
																													BgL_yz00_74)) > 0L))
																										{	/* Ieee/number.scm 659 */
																											return
																												BgL_arg1927z00_1570;
																										}
																									else
																										{	/* Ieee/number.scm 659 */
																											return BgL_yz00_74;
																										}
																								}
																							else
																								{	/* Ieee/number.scm 667 */
																									return
																										BGl_errorz00zz__errorz00
																										(BGl_string3337z00zz__r4_numbers_6_5z00,
																										BGl_string3329z00zz__r4_numbers_6_5z00,
																										BgL_yz00_74);
																								}
																						}
																				}
																		}
																}
														}
												}
											else
												{	/* Ieee/number.scm 667 */
													if (BIGNUMP(BgL_xz00_73))
														{	/* Ieee/number.scm 667 */
															if (BIGNUMP(BgL_yz00_74))
																{	/* Ieee/number.scm 667 */
																	if (
																		((long) (bgl_bignum_cmp(BgL_xz00_73,
																					BgL_yz00_74)) > 0L))
																		{	/* Ieee/number.scm 659 */
																			return BgL_xz00_73;
																		}
																	else
																		{	/* Ieee/number.scm 659 */
																			return BgL_yz00_74;
																		}
																}
															else
																{	/* Ieee/number.scm 667 */
																	if (INTEGERP(BgL_yz00_74))
																		{	/* Ieee/number.scm 667 */
																			obj_t BgL_arg1931z00_1574;

																			BgL_arg1931z00_1574 =
																				bgl_long_to_bignum(
																				(long) CINT(BgL_yz00_74));
																			if (
																				((long) (bgl_bignum_cmp(BgL_xz00_73,
																							BgL_arg1931z00_1574)) > 0L))
																				{	/* Ieee/number.scm 659 */
																					return BgL_xz00_73;
																				}
																			else
																				{	/* Ieee/number.scm 659 */
																					return BgL_arg1931z00_1574;
																				}
																		}
																	else
																		{	/* Ieee/number.scm 667 */
																			if (REALP(BgL_yz00_74))
																				{	/* Ieee/number.scm 667 */
																					double BgL_arg1933z00_1576;

																					BgL_arg1933z00_1576 =
																						bgl_bignum_to_flonum(BgL_xz00_73);
																					if (
																						(BgL_arg1933z00_1576 >
																							REAL_TO_DOUBLE(BgL_yz00_74)))
																						{	/* Ieee/number.scm 653 */
																							return
																								DOUBLE_TO_REAL
																								(BgL_arg1933z00_1576);
																						}
																					else
																						{	/* Ieee/number.scm 653 */
																							return BgL_yz00_74;
																						}
																				}
																			else
																				{	/* Ieee/number.scm 667 */
																					if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_74))
																						{	/* Ieee/number.scm 667 */
																							obj_t BgL_arg1935z00_1578;

																							{	/* Ieee/number.scm 667 */
																								obj_t BgL_arg1936z00_1579;

																								BgL_arg1936z00_1579 =
																									BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																									(BgL_yz00_74);
																								{	/* Ieee/number.scm 667 */
																									long BgL_xz00_3742;

																									BgL_xz00_3742 =
																										BELONG_TO_LONG
																										(BgL_arg1936z00_1579);
																									BgL_arg1935z00_1578 =
																										bgl_long_to_bignum
																										(BgL_xz00_3742);
																							}}
																							if (
																								((long) (bgl_bignum_cmp
																										(BgL_xz00_73,
																											BgL_arg1935z00_1578)) >
																									0L))
																								{	/* Ieee/number.scm 659 */
																									return BgL_xz00_73;
																								}
																							else
																								{	/* Ieee/number.scm 659 */
																									return BgL_arg1935z00_1578;
																								}
																						}
																					else
																						{	/* Ieee/number.scm 667 */
																							if (LLONGP(BgL_yz00_74))
																								{	/* Ieee/number.scm 667 */
																									obj_t BgL_arg1938z00_1581;

																									BgL_arg1938z00_1581 =
																										bgl_llong_to_bignum
																										(BLLONG_TO_LLONG
																										(BgL_yz00_74));
																									if (((long) (bgl_bignum_cmp
																												(BgL_xz00_73,
																													BgL_arg1938z00_1581))
																											> 0L))
																										{	/* Ieee/number.scm 659 */
																											return BgL_xz00_73;
																										}
																									else
																										{	/* Ieee/number.scm 659 */
																											return
																												BgL_arg1938z00_1581;
																										}
																								}
																							else
																								{	/* Ieee/number.scm 667 */
																									if (BGL_UINT64P(BgL_yz00_74))
																										{	/* Ieee/number.scm 667 */
																											obj_t BgL_arg1941z00_1584;

																											BgL_arg1941z00_1584 =
																												bgl_uint64_to_bignum
																												(BGL_BINT64_TO_INT64
																												(BgL_yz00_74));
																											if (((long)
																													(bgl_bignum_cmp
																														(BgL_xz00_73,
																															BgL_arg1941z00_1584))
																													> 0L))
																												{	/* Ieee/number.scm 659 */
																													return BgL_xz00_73;
																												}
																											else
																												{	/* Ieee/number.scm 659 */
																													return
																														BgL_arg1941z00_1584;
																												}
																										}
																									else
																										{	/* Ieee/number.scm 667 */
																											return
																												BGl_errorz00zz__errorz00
																												(BGl_string3337z00zz__r4_numbers_6_5z00,
																												BGl_string3328z00zz__r4_numbers_6_5z00,
																												BgL_yz00_74);
																										}
																								}
																						}
																				}
																		}
																}
														}
													else
														{	/* Ieee/number.scm 667 */
															return
																BGl_errorz00zz__errorz00
																(BGl_string3337z00zz__r4_numbers_6_5z00,
																BGl_string3328z00zz__r4_numbers_6_5z00,
																BgL_xz00_73);
														}
												}
										}
								}
						}
				}
		}

	}



/* &2max */
	obj_t BGl_z622maxz62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5047,
		obj_t BgL_xz00_5048, obj_t BgL_yz00_5049)
	{
		{	/* Ieee/number.scm 666 */
			return BGl_2maxz00zz__r4_numbers_6_5z00(BgL_xz00_5048, BgL_yz00_5049);
		}

	}



/* max */
	BGL_EXPORTED_DEF obj_t BGl_maxz00zz__r4_numbers_6_5z00(obj_t BgL_xz00_75,
		obj_t BgL_yz00_76)
	{
		{	/* Ieee/number.scm 672 */
			{
				obj_t BgL_xz00_3779;
				obj_t BgL_yz00_3780;

				BgL_xz00_3779 = BgL_xz00_75;
				BgL_yz00_3780 = BgL_yz00_76;
			BgL_loopz00_3778:
				if (PAIRP(BgL_yz00_3780))
					{
						obj_t BgL_yz00_7407;
						obj_t BgL_xz00_7404;

						BgL_xz00_7404 =
							BGl_2maxz00zz__r4_numbers_6_5z00(BgL_xz00_3779,
							CAR(BgL_yz00_3780));
						BgL_yz00_7407 = CDR(BgL_yz00_3780);
						BgL_yz00_3780 = BgL_yz00_7407;
						BgL_xz00_3779 = BgL_xz00_7404;
						goto BgL_loopz00_3778;
					}
				else
					{	/* Ieee/number.scm 675 */
						return BgL_xz00_3779;
					}
			}
		}

	}



/* &max */
	obj_t BGl_z62maxz62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5050,
		obj_t BgL_xz00_5051, obj_t BgL_yz00_5052)
	{
		{	/* Ieee/number.scm 672 */
			return BGl_maxz00zz__r4_numbers_6_5z00(BgL_xz00_5051, BgL_yz00_5052);
		}

	}



/* 2min */
	BGL_EXPORTED_DEF obj_t BGl_2minz00zz__r4_numbers_6_5z00(obj_t BgL_xz00_89,
		obj_t BgL_yz00_90)
	{
		{	/* Ieee/number.scm 698 */
			if (INTEGERP(BgL_xz00_89))
				{	/* Ieee/number.scm 699 */
					if (INTEGERP(BgL_yz00_90))
						{	/* Ieee/number.scm 699 */
							if (((long) CINT(BgL_xz00_89) > (long) CINT(BgL_yz00_90)))
								{	/* Ieee/number.scm 683 */
									return BgL_yz00_90;
								}
							else
								{	/* Ieee/number.scm 683 */
									return BgL_xz00_89;
								}
						}
					else
						{	/* Ieee/number.scm 699 */
							if (REALP(BgL_yz00_90))
								{	/* Ieee/number.scm 699 */
									double BgL_arg1956z00_1603;

									BgL_arg1956z00_1603 = (double) ((long) CINT(BgL_xz00_89));
									if ((BgL_arg1956z00_1603 > REAL_TO_DOUBLE(BgL_yz00_90)))
										{	/* Ieee/number.scm 685 */
											return BgL_yz00_90;
										}
									else
										{	/* Ieee/number.scm 685 */
											return DOUBLE_TO_REAL(BgL_arg1956z00_1603);
										}
								}
							else
								{	/* Ieee/number.scm 699 */
									if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_90))
										{	/* Ieee/number.scm 699 */
											long BgL_arg1958z00_1605;
											obj_t BgL_arg1959z00_1606;

											{	/* Ieee/number.scm 699 */
												long BgL_tmpz00_7428;

												BgL_tmpz00_7428 = (long) CINT(BgL_xz00_89);
												BgL_arg1958z00_1605 = (long) (BgL_tmpz00_7428);
											}
											BgL_arg1959z00_1606 =
												BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
												(BgL_yz00_90);
											{	/* Ieee/number.scm 687 */
												bool_t BgL_test3916z00_7432;

												{	/* Ieee/number.scm 687 */
													long BgL_n2z00_3820;

													BgL_n2z00_3820 = BELONG_TO_LONG(BgL_arg1959z00_1606);
													BgL_test3916z00_7432 =
														(BgL_arg1958z00_1605 > BgL_n2z00_3820);
												}
												if (BgL_test3916z00_7432)
													{	/* Ieee/number.scm 687 */
														return BgL_arg1959z00_1606;
													}
												else
													{	/* Ieee/number.scm 687 */
														return make_belong(BgL_arg1958z00_1605);
													}
											}
										}
									else
										{	/* Ieee/number.scm 699 */
											if (LLONGP(BgL_yz00_90))
												{	/* Ieee/number.scm 699 */
													BGL_LONGLONG_T BgL_arg1961z00_1608;

													{	/* Ieee/number.scm 699 */
														long BgL_tmpz00_7438;

														BgL_tmpz00_7438 = (long) CINT(BgL_xz00_89);
														BgL_arg1961z00_1608 =
															LONG_TO_LLONG(BgL_tmpz00_7438);
													}
													if (
														(BgL_arg1961z00_1608 >
															BLLONG_TO_LLONG(BgL_yz00_90)))
														{	/* Ieee/number.scm 689 */
															return BgL_yz00_90;
														}
													else
														{	/* Ieee/number.scm 689 */
															return make_bllong(BgL_arg1961z00_1608);
														}
												}
											else
												{	/* Ieee/number.scm 699 */
													if (BGL_UINT64P(BgL_yz00_90))
														{	/* Ieee/number.scm 699 */
															uint64_t BgL_arg1964z00_1611;

															{	/* Ieee/number.scm 699 */
																BGL_LONGLONG_T BgL_arg1965z00_1612;

																{	/* Ieee/number.scm 699 */
																	long BgL_tmpz00_7447;

																	BgL_tmpz00_7447 = (long) CINT(BgL_xz00_89);
																	BgL_arg1965z00_1612 =
																		LONG_TO_LLONG(BgL_tmpz00_7447);
																}
																BgL_arg1964z00_1611 =
																	(uint64_t) (BgL_arg1965z00_1612);
															}
															{	/* Ieee/number.scm 693 */
																bool_t BgL_test3920z00_7451;

																{	/* Ieee/number.scm 693 */
																	uint64_t BgL_n2z00_3829;

																	BgL_n2z00_3829 =
																		BGL_BINT64_TO_INT64(BgL_yz00_90);
																	BgL_test3920z00_7451 =
																		(BgL_arg1964z00_1611 > BgL_n2z00_3829);
																}
																if (BgL_test3920z00_7451)
																	{	/* Ieee/number.scm 693 */
																		return BgL_yz00_90;
																	}
																else
																	{	/* Ieee/number.scm 693 */
																		return
																			BGL_UINT64_TO_BUINT64
																			(BgL_arg1964z00_1611);
																	}
															}
														}
													else
														{	/* Ieee/number.scm 699 */
															if (BIGNUMP(BgL_yz00_90))
																{	/* Ieee/number.scm 699 */
																	obj_t BgL_arg1967z00_1614;

																	BgL_arg1967z00_1614 =
																		bgl_long_to_bignum(
																		(long) CINT(BgL_xz00_89));
																	if (
																		((long) (bgl_bignum_cmp(BgL_arg1967z00_1614,
																					BgL_yz00_90)) > 0L))
																		{	/* Ieee/number.scm 691 */
																			return BgL_yz00_90;
																		}
																	else
																		{	/* Ieee/number.scm 691 */
																			return BgL_arg1967z00_1614;
																		}
																}
															else
																{	/* Ieee/number.scm 699 */
																	return
																		BGl_errorz00zz__errorz00
																		(BGl_string3338z00zz__r4_numbers_6_5z00,
																		BGl_string3328z00zz__r4_numbers_6_5z00,
																		BgL_yz00_90);
																}
														}
												}
										}
								}
						}
				}
			else
				{	/* Ieee/number.scm 699 */
					if (REALP(BgL_xz00_89))
						{	/* Ieee/number.scm 699 */
							if (REALP(BgL_yz00_90))
								{	/* Ieee/number.scm 699 */
									if (
										(REAL_TO_DOUBLE(BgL_xz00_89) > REAL_TO_DOUBLE(BgL_yz00_90)))
										{	/* Ieee/number.scm 685 */
											return BgL_yz00_90;
										}
									else
										{	/* Ieee/number.scm 685 */
											return BgL_xz00_89;
										}
								}
							else
								{	/* Ieee/number.scm 699 */
									if (INTEGERP(BgL_yz00_90))
										{	/* Ieee/number.scm 699 */
											double BgL_arg1971z00_1618;

											BgL_arg1971z00_1618 = (double) ((long) CINT(BgL_yz00_90));
											if ((REAL_TO_DOUBLE(BgL_xz00_89) > BgL_arg1971z00_1618))
												{	/* Ieee/number.scm 685 */
													return DOUBLE_TO_REAL(BgL_arg1971z00_1618);
												}
											else
												{	/* Ieee/number.scm 685 */
													return BgL_xz00_89;
												}
										}
									else
										{	/* Ieee/number.scm 699 */
											if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00
												(BgL_yz00_90))
												{	/* Ieee/number.scm 699 */
													double BgL_arg1973z00_1620;

													{	/* Ieee/number.scm 699 */
														obj_t BgL_arg1974z00_1621;

														BgL_arg1974z00_1621 =
															BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
															(BgL_yz00_90);
														BgL_arg1973z00_1620 =
															(double) (BELONG_TO_LONG(BgL_arg1974z00_1621));
													}
													if (
														(REAL_TO_DOUBLE(BgL_xz00_89) > BgL_arg1973z00_1620))
														{	/* Ieee/number.scm 685 */
															return DOUBLE_TO_REAL(BgL_arg1973z00_1620);
														}
													else
														{	/* Ieee/number.scm 685 */
															return BgL_xz00_89;
														}
												}
											else
												{	/* Ieee/number.scm 699 */
													if (LLONGP(BgL_yz00_90))
														{	/* Ieee/number.scm 699 */
															double BgL_arg1976z00_1623;

															BgL_arg1976z00_1623 =
																(double) (BLLONG_TO_LLONG(BgL_yz00_90));
															if (
																(REAL_TO_DOUBLE(BgL_xz00_89) >
																	BgL_arg1976z00_1623))
																{	/* Ieee/number.scm 685 */
																	return DOUBLE_TO_REAL(BgL_arg1976z00_1623);
																}
															else
																{	/* Ieee/number.scm 685 */
																	return BgL_xz00_89;
																}
														}
													else
														{	/* Ieee/number.scm 699 */
															if (BGL_UINT64P(BgL_yz00_90))
																{	/* Ieee/number.scm 699 */
																	double BgL_arg1979z00_1626;

																	{	/* Ieee/number.scm 699 */
																		uint64_t BgL_xz00_3848;

																		BgL_xz00_3848 =
																			BGL_BINT64_TO_INT64(BgL_yz00_90);
																		BgL_arg1979z00_1626 =
																			(double) (BgL_xz00_3848);
																	}
																	if (
																		(REAL_TO_DOUBLE(BgL_xz00_89) >
																			BgL_arg1979z00_1626))
																		{	/* Ieee/number.scm 685 */
																			return
																				DOUBLE_TO_REAL(BgL_arg1979z00_1626);
																		}
																	else
																		{	/* Ieee/number.scm 685 */
																			return BgL_xz00_89;
																		}
																}
															else
																{	/* Ieee/number.scm 699 */
																	if (BIGNUMP(BgL_yz00_90))
																		{	/* Ieee/number.scm 699 */
																			double BgL_arg1981z00_1628;

																			BgL_arg1981z00_1628 =
																				bgl_bignum_to_flonum(BgL_yz00_90);
																			if (
																				(REAL_TO_DOUBLE(BgL_xz00_89) >
																					BgL_arg1981z00_1628))
																				{	/* Ieee/number.scm 685 */
																					return
																						DOUBLE_TO_REAL(BgL_arg1981z00_1628);
																				}
																			else
																				{	/* Ieee/number.scm 685 */
																					return BgL_xz00_89;
																				}
																		}
																	else
																		{	/* Ieee/number.scm 699 */
																			return
																				BGl_errorz00zz__errorz00
																				(BGl_string3338z00zz__r4_numbers_6_5z00,
																				BGl_string3328z00zz__r4_numbers_6_5z00,
																				BgL_yz00_90);
																		}
																}
														}
												}
										}
								}
						}
					else
						{	/* Ieee/number.scm 699 */
							if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_xz00_89))
								{	/* Ieee/number.scm 699 */
									if (INTEGERP(BgL_yz00_90))
										{	/* Ieee/number.scm 699 */
											obj_t BgL_arg1984z00_1631;
											long BgL_arg1985z00_1632;

											BgL_arg1984z00_1631 =
												BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
												(BgL_xz00_89);
											{	/* Ieee/number.scm 699 */
												long BgL_tmpz00_7518;

												BgL_tmpz00_7518 = (long) CINT(BgL_yz00_90);
												BgL_arg1985z00_1632 = (long) (BgL_tmpz00_7518);
											}
											{	/* Ieee/number.scm 687 */
												bool_t BgL_test3938z00_7521;

												{	/* Ieee/number.scm 687 */
													long BgL_n1z00_3858;

													BgL_n1z00_3858 = BELONG_TO_LONG(BgL_arg1984z00_1631);
													BgL_test3938z00_7521 =
														(BgL_n1z00_3858 > BgL_arg1985z00_1632);
												}
												if (BgL_test3938z00_7521)
													{	/* Ieee/number.scm 687 */
														return make_belong(BgL_arg1985z00_1632);
													}
												else
													{	/* Ieee/number.scm 687 */
														return BgL_arg1984z00_1631;
													}
											}
										}
									else
										{	/* Ieee/number.scm 699 */
											if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00
												(BgL_yz00_90))
												{	/* Ieee/number.scm 699 */
													obj_t BgL_arg1987z00_1634;
													obj_t BgL_arg1988z00_1635;

													BgL_arg1987z00_1634 =
														BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
														(BgL_xz00_89);
													BgL_arg1988z00_1635 =
														BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
														(BgL_yz00_90);
													{	/* Ieee/number.scm 687 */
														bool_t BgL_test3940z00_7529;

														{	/* Ieee/number.scm 687 */
															long BgL_n1z00_3861;
															long BgL_n2z00_3862;

															BgL_n1z00_3861 =
																BELONG_TO_LONG(BgL_arg1987z00_1634);
															BgL_n2z00_3862 =
																BELONG_TO_LONG(BgL_arg1988z00_1635);
															BgL_test3940z00_7529 =
																(BgL_n1z00_3861 > BgL_n2z00_3862);
														}
														if (BgL_test3940z00_7529)
															{	/* Ieee/number.scm 687 */
																return BgL_arg1988z00_1635;
															}
														else
															{	/* Ieee/number.scm 687 */
																return BgL_arg1987z00_1634;
															}
													}
												}
											else
												{	/* Ieee/number.scm 699 */
													if (REALP(BgL_yz00_90))
														{	/* Ieee/number.scm 699 */
															double BgL_arg1990z00_1637;

															{	/* Ieee/number.scm 699 */
																obj_t BgL_arg1991z00_1638;

																BgL_arg1991z00_1638 =
																	BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																	(BgL_xz00_89);
																BgL_arg1990z00_1637 =
																	(double) (BELONG_TO_LONG
																	(BgL_arg1991z00_1638));
															}
															if (
																(BgL_arg1990z00_1637 >
																	REAL_TO_DOUBLE(BgL_yz00_90)))
																{	/* Ieee/number.scm 685 */
																	return BgL_yz00_90;
																}
															else
																{	/* Ieee/number.scm 685 */
																	return DOUBLE_TO_REAL(BgL_arg1990z00_1637);
																}
														}
													else
														{	/* Ieee/number.scm 699 */
															if (LLONGP(BgL_yz00_90))
																{	/* Ieee/number.scm 699 */
																	BGL_LONGLONG_T BgL_arg1993z00_1640;

																	{	/* Ieee/number.scm 699 */
																		obj_t BgL_arg1995z00_1642;

																		BgL_arg1995z00_1642 =
																			BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																			(BgL_xz00_89);
																		BgL_arg1993z00_1640 =
																			(BGL_LONGLONG_T) (BELONG_TO_LONG
																			(BgL_arg1995z00_1642));
																	}
																	if (
																		(BgL_arg1993z00_1640 >
																			BLLONG_TO_LLONG(BgL_yz00_90)))
																		{	/* Ieee/number.scm 689 */
																			return BgL_yz00_90;
																		}
																	else
																		{	/* Ieee/number.scm 689 */
																			return make_bllong(BgL_arg1993z00_1640);
																		}
																}
															else
																{	/* Ieee/number.scm 699 */
																	if (BGL_UINT64P(BgL_yz00_90))
																		{	/* Ieee/number.scm 699 */
																			uint64_t BgL_arg1997z00_1644;

																			{	/* Ieee/number.scm 699 */
																				BGL_LONGLONG_T BgL_arg1998z00_1645;

																				{	/* Ieee/number.scm 699 */
																					obj_t BgL_arg1999z00_1646;

																					BgL_arg1999z00_1646 =
																						BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																						(BgL_xz00_89);
																					BgL_arg1998z00_1645 =
																						(BGL_LONGLONG_T) (BELONG_TO_LONG
																						(BgL_arg1999z00_1646));
																				}
																				BgL_arg1997z00_1644 =
																					(uint64_t) (BgL_arg1998z00_1645);
																			}
																			{	/* Ieee/number.scm 693 */
																				bool_t BgL_test3946z00_7557;

																				{	/* Ieee/number.scm 693 */
																					uint64_t BgL_n2z00_3872;

																					BgL_n2z00_3872 =
																						BGL_BINT64_TO_INT64(BgL_yz00_90);
																					BgL_test3946z00_7557 =
																						(BgL_arg1997z00_1644 >
																						BgL_n2z00_3872);
																				}
																				if (BgL_test3946z00_7557)
																					{	/* Ieee/number.scm 693 */
																						return BgL_yz00_90;
																					}
																				else
																					{	/* Ieee/number.scm 693 */
																						return
																							BGL_UINT64_TO_BUINT64
																							(BgL_arg1997z00_1644);
																					}
																			}
																		}
																	else
																		{	/* Ieee/number.scm 699 */
																			if (BIGNUMP(BgL_yz00_90))
																				{	/* Ieee/number.scm 699 */
																					obj_t BgL_arg2001z00_1648;

																					{	/* Ieee/number.scm 699 */
																						obj_t BgL_arg2002z00_1649;

																						BgL_arg2002z00_1649 =
																							BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																							(BgL_xz00_89);
																						{	/* Ieee/number.scm 699 */
																							long BgL_xz00_3873;

																							BgL_xz00_3873 =
																								BELONG_TO_LONG
																								(BgL_arg2002z00_1649);
																							BgL_arg2001z00_1648 =
																								bgl_long_to_bignum
																								(BgL_xz00_3873);
																					}}
																					if (
																						((long) (bgl_bignum_cmp
																								(BgL_arg2001z00_1648,
																									BgL_yz00_90)) > 0L))
																						{	/* Ieee/number.scm 691 */
																							return BgL_yz00_90;
																						}
																					else
																						{	/* Ieee/number.scm 691 */
																							return BgL_arg2001z00_1648;
																						}
																				}
																			else
																				{	/* Ieee/number.scm 699 */
																					return
																						BGl_errorz00zz__errorz00
																						(BGl_string3338z00zz__r4_numbers_6_5z00,
																						BGl_string3328z00zz__r4_numbers_6_5z00,
																						BgL_yz00_90);
																				}
																		}
																}
														}
												}
										}
								}
							else
								{	/* Ieee/number.scm 699 */
									if (LLONGP(BgL_xz00_89))
										{	/* Ieee/number.scm 699 */
											if (INTEGERP(BgL_yz00_90))
												{	/* Ieee/number.scm 699 */
													BGL_LONGLONG_T BgL_arg2007z00_1653;

													{	/* Ieee/number.scm 699 */
														long BgL_tmpz00_7575;

														BgL_tmpz00_7575 = (long) CINT(BgL_yz00_90);
														BgL_arg2007z00_1653 =
															LONG_TO_LLONG(BgL_tmpz00_7575);
													}
													if (
														(BLLONG_TO_LLONG(BgL_xz00_89) >
															BgL_arg2007z00_1653))
														{	/* Ieee/number.scm 689 */
															return make_bllong(BgL_arg2007z00_1653);
														}
													else
														{	/* Ieee/number.scm 689 */
															return BgL_xz00_89;
														}
												}
											else
												{	/* Ieee/number.scm 699 */
													if (REALP(BgL_yz00_90))
														{	/* Ieee/number.scm 699 */
															double BgL_arg2009z00_1655;

															BgL_arg2009z00_1655 =
																(double) (BLLONG_TO_LLONG(BgL_xz00_89));
															if (
																(BgL_arg2009z00_1655 >
																	REAL_TO_DOUBLE(BgL_yz00_90)))
																{	/* Ieee/number.scm 685 */
																	return BgL_yz00_90;
																}
															else
																{	/* Ieee/number.scm 685 */
																	return DOUBLE_TO_REAL(BgL_arg2009z00_1655);
																}
														}
													else
														{	/* Ieee/number.scm 699 */
															if (LLONGP(BgL_yz00_90))
																{	/* Ieee/number.scm 699 */
																	if (
																		(BLLONG_TO_LLONG(BgL_xz00_89) >
																			BLLONG_TO_LLONG(BgL_yz00_90)))
																		{	/* Ieee/number.scm 689 */
																			return BgL_yz00_90;
																		}
																	else
																		{	/* Ieee/number.scm 689 */
																			return BgL_xz00_89;
																		}
																}
															else
																{	/* Ieee/number.scm 699 */
																	if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00
																		(BgL_yz00_90))
																		{	/* Ieee/number.scm 699 */
																			BGL_LONGLONG_T BgL_arg2015z00_1661;

																			{	/* Ieee/number.scm 699 */
																				obj_t BgL_arg2016z00_1662;

																				BgL_arg2016z00_1662 =
																					BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																					(BgL_yz00_90);
																				BgL_arg2015z00_1661 =
																					(BGL_LONGLONG_T) (BELONG_TO_LONG
																					(BgL_arg2016z00_1662));
																			}
																			if (
																				(BLLONG_TO_LLONG(BgL_xz00_89) >
																					BgL_arg2015z00_1661))
																				{	/* Ieee/number.scm 689 */
																					return
																						make_bllong(BgL_arg2015z00_1661);
																				}
																			else
																				{	/* Ieee/number.scm 689 */
																					return BgL_xz00_89;
																				}
																		}
																	else
																		{	/* Ieee/number.scm 699 */
																			if (BIGNUMP(BgL_yz00_90))
																				{	/* Ieee/number.scm 699 */
																					obj_t BgL_arg2018z00_1664;

																					BgL_arg2018z00_1664 =
																						bgl_llong_to_bignum(BLLONG_TO_LLONG
																						(BgL_xz00_89));
																					if (((long) (bgl_bignum_cmp
																								(BgL_arg2018z00_1664,
																									BgL_yz00_90)) > 0L))
																						{	/* Ieee/number.scm 691 */
																							return BgL_yz00_90;
																						}
																					else
																						{	/* Ieee/number.scm 691 */
																							return BgL_arg2018z00_1664;
																						}
																				}
																			else
																				{	/* Ieee/number.scm 699 */
																					if (BGL_UINT64P(BgL_yz00_90))
																						{	/* Ieee/number.scm 699 */
																							uint64_t BgL_arg2021z00_1667;

																							{	/* Ieee/number.scm 699 */
																								BGL_LONGLONG_T BgL_tmpz00_7615;

																								BgL_tmpz00_7615 =
																									BLLONG_TO_LLONG(BgL_xz00_89);
																								BgL_arg2021z00_1667 =
																									(uint64_t) (BgL_tmpz00_7615);
																							}
																							{	/* Ieee/number.scm 693 */
																								bool_t BgL_test3961z00_7618;

																								{	/* Ieee/number.scm 693 */
																									uint64_t BgL_n2z00_3901;

																									BgL_n2z00_3901 =
																										BGL_BINT64_TO_INT64
																										(BgL_yz00_90);
																									BgL_test3961z00_7618 =
																										(BgL_arg2021z00_1667 >
																										BgL_n2z00_3901);
																								}
																								if (BgL_test3961z00_7618)
																									{	/* Ieee/number.scm 693 */
																										return BgL_yz00_90;
																									}
																								else
																									{	/* Ieee/number.scm 693 */
																										return
																											BGL_UINT64_TO_BUINT64
																											(BgL_arg2021z00_1667);
																									}
																							}
																						}
																					else
																						{	/* Ieee/number.scm 699 */
																							return
																								BGl_errorz00zz__errorz00
																								(BGl_string3338z00zz__r4_numbers_6_5z00,
																								BGl_string3328z00zz__r4_numbers_6_5z00,
																								BgL_yz00_90);
																						}
																				}
																		}
																}
														}
												}
										}
									else
										{	/* Ieee/number.scm 699 */
											if (BGL_UINT64P(BgL_xz00_89))
												{	/* Ieee/number.scm 699 */
													if (INTEGERP(BgL_yz00_90))
														{	/* Ieee/number.scm 699 */
															uint64_t BgL_arg2024z00_1670;

															{	/* Ieee/number.scm 699 */
																long BgL_tmpz00_7627;

																BgL_tmpz00_7627 = (long) CINT(BgL_yz00_90);
																BgL_arg2024z00_1670 =
																	(uint64_t) (BgL_tmpz00_7627);
															}
															{	/* Ieee/number.scm 693 */
																bool_t BgL_test3964z00_7630;

																{	/* Ieee/number.scm 693 */
																	uint64_t BgL_n1z00_3904;

																	BgL_n1z00_3904 =
																		BGL_BINT64_TO_INT64(BgL_xz00_89);
																	BgL_test3964z00_7630 =
																		(BgL_n1z00_3904 > BgL_arg2024z00_1670);
																}
																if (BgL_test3964z00_7630)
																	{	/* Ieee/number.scm 693 */
																		return
																			BGL_UINT64_TO_BUINT64
																			(BgL_arg2024z00_1670);
																	}
																else
																	{	/* Ieee/number.scm 693 */
																		return BgL_xz00_89;
																	}
															}
														}
													else
														{	/* Ieee/number.scm 699 */
															if (BGL_UINT64P(BgL_yz00_90))
																{	/* Ieee/number.scm 693 */
																	bool_t BgL_test3966z00_7636;

																	{	/* Ieee/number.scm 693 */
																		uint64_t BgL_n1z00_3907;
																		uint64_t BgL_n2z00_3908;

																		BgL_n1z00_3907 =
																			BGL_BINT64_TO_INT64(BgL_xz00_89);
																		BgL_n2z00_3908 =
																			BGL_BINT64_TO_INT64(BgL_yz00_90);
																		BgL_test3966z00_7636 =
																			(BgL_n1z00_3907 > BgL_n2z00_3908);
																	}
																	if (BgL_test3966z00_7636)
																		{	/* Ieee/number.scm 693 */
																			return BgL_yz00_90;
																		}
																	else
																		{	/* Ieee/number.scm 693 */
																			return BgL_xz00_89;
																		}
																}
															else
																{	/* Ieee/number.scm 699 */
																	if (REALP(BgL_yz00_90))
																		{	/* Ieee/number.scm 699 */
																			double BgL_arg2027z00_1673;

																			{	/* Ieee/number.scm 699 */
																				uint64_t BgL_tmpz00_7642;

																				BgL_tmpz00_7642 =
																					BGL_BINT64_TO_INT64(BgL_xz00_89);
																				BgL_arg2027z00_1673 =
																					(double) (BgL_tmpz00_7642);
																			}
																			if (
																				(BgL_arg2027z00_1673 >
																					REAL_TO_DOUBLE(BgL_yz00_90)))
																				{	/* Ieee/number.scm 685 */
																					return BgL_yz00_90;
																				}
																			else
																				{	/* Ieee/number.scm 685 */
																					return
																						DOUBLE_TO_REAL(BgL_arg2027z00_1673);
																				}
																		}
																	else
																		{	/* Ieee/number.scm 699 */
																			if (LLONGP(BgL_yz00_90))
																				{	/* Ieee/number.scm 699 */
																					uint64_t BgL_arg2029z00_1675;

																					{	/* Ieee/number.scm 699 */
																						BGL_LONGLONG_T BgL_tmpz00_7651;

																						BgL_tmpz00_7651 =
																							BLLONG_TO_LLONG(BgL_yz00_90);
																						BgL_arg2029z00_1675 =
																							(uint64_t) (BgL_tmpz00_7651);
																					}
																					{	/* Ieee/number.scm 693 */
																						bool_t BgL_test3970z00_7654;

																						{	/* Ieee/number.scm 693 */
																							uint64_t BgL_n1z00_3914;

																							BgL_n1z00_3914 =
																								BGL_BINT64_TO_INT64
																								(BgL_xz00_89);
																							BgL_test3970z00_7654 =
																								(BgL_n1z00_3914 >
																								BgL_arg2029z00_1675);
																						}
																						if (BgL_test3970z00_7654)
																							{	/* Ieee/number.scm 693 */
																								return
																									BGL_UINT64_TO_BUINT64
																									(BgL_arg2029z00_1675);
																							}
																						else
																							{	/* Ieee/number.scm 693 */
																								return BgL_xz00_89;
																							}
																					}
																				}
																			else
																				{	/* Ieee/number.scm 699 */
																					if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_90))
																						{	/* Ieee/number.scm 699 */
																							uint64_t BgL_arg2033z00_1678;

																							{	/* Ieee/number.scm 699 */
																								BGL_LONGLONG_T
																									BgL_arg2034z00_1679;
																								{	/* Ieee/number.scm 699 */
																									obj_t BgL_arg2036z00_1680;

																									BgL_arg2036z00_1680 =
																										BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																										(BgL_yz00_90);
																									BgL_arg2034z00_1679 =
																										(BGL_LONGLONG_T)
																										(BELONG_TO_LONG
																										(BgL_arg2036z00_1680));
																								}
																								BgL_arg2033z00_1678 =
																									(uint64_t)
																									(BgL_arg2034z00_1679);
																							}
																							{	/* Ieee/number.scm 693 */
																								bool_t BgL_test3972z00_7664;

																								{	/* Ieee/number.scm 693 */
																									uint64_t BgL_n1z00_3918;

																									BgL_n1z00_3918 =
																										BGL_BINT64_TO_INT64
																										(BgL_xz00_89);
																									BgL_test3972z00_7664 =
																										(BgL_n1z00_3918 >
																										BgL_arg2033z00_1678);
																								}
																								if (BgL_test3972z00_7664)
																									{	/* Ieee/number.scm 693 */
																										return
																											BGL_UINT64_TO_BUINT64
																											(BgL_arg2033z00_1678);
																									}
																								else
																									{	/* Ieee/number.scm 693 */
																										return BgL_xz00_89;
																									}
																							}
																						}
																					else
																						{	/* Ieee/number.scm 699 */
																							if (BIGNUMP(BgL_yz00_90))
																								{	/* Ieee/number.scm 699 */
																									obj_t BgL_arg2038z00_1682;

																									BgL_arg2038z00_1682 =
																										bgl_uint64_to_bignum
																										(BGL_BINT64_TO_INT64
																										(BgL_xz00_89));
																									if (((long) (bgl_bignum_cmp
																												(BgL_arg2038z00_1682,
																													BgL_yz00_90)) > 0L))
																										{	/* Ieee/number.scm 691 */
																											return BgL_yz00_90;
																										}
																									else
																										{	/* Ieee/number.scm 691 */
																											return
																												BgL_arg2038z00_1682;
																										}
																								}
																							else
																								{	/* Ieee/number.scm 699 */
																									return
																										BGl_errorz00zz__errorz00
																										(BGl_string3338z00zz__r4_numbers_6_5z00,
																										BGl_string3329z00zz__r4_numbers_6_5z00,
																										BgL_yz00_90);
																								}
																						}
																				}
																		}
																}
														}
												}
											else
												{	/* Ieee/number.scm 699 */
													if (BIGNUMP(BgL_xz00_89))
														{	/* Ieee/number.scm 699 */
															if (BIGNUMP(BgL_yz00_90))
																{	/* Ieee/number.scm 699 */
																	if (
																		((long) (bgl_bignum_cmp(BgL_xz00_89,
																					BgL_yz00_90)) > 0L))
																		{	/* Ieee/number.scm 691 */
																			return BgL_yz00_90;
																		}
																	else
																		{	/* Ieee/number.scm 691 */
																			return BgL_xz00_89;
																		}
																}
															else
																{	/* Ieee/number.scm 699 */
																	if (INTEGERP(BgL_yz00_90))
																		{	/* Ieee/number.scm 699 */
																			obj_t BgL_arg2042z00_1686;

																			BgL_arg2042z00_1686 =
																				bgl_long_to_bignum(
																				(long) CINT(BgL_yz00_90));
																			if (
																				((long) (bgl_bignum_cmp(BgL_xz00_89,
																							BgL_arg2042z00_1686)) > 0L))
																				{	/* Ieee/number.scm 691 */
																					return BgL_arg2042z00_1686;
																				}
																			else
																				{	/* Ieee/number.scm 691 */
																					return BgL_xz00_89;
																				}
																		}
																	else
																		{	/* Ieee/number.scm 699 */
																			if (REALP(BgL_yz00_90))
																				{	/* Ieee/number.scm 699 */
																					double BgL_arg2044z00_1688;

																					BgL_arg2044z00_1688 =
																						bgl_bignum_to_flonum(BgL_xz00_89);
																					if (
																						(BgL_arg2044z00_1688 >
																							REAL_TO_DOUBLE(BgL_yz00_90)))
																						{	/* Ieee/number.scm 685 */
																							return BgL_yz00_90;
																						}
																					else
																						{	/* Ieee/number.scm 685 */
																							return
																								DOUBLE_TO_REAL
																								(BgL_arg2044z00_1688);
																						}
																				}
																			else
																				{	/* Ieee/number.scm 699 */
																					if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_90))
																						{	/* Ieee/number.scm 699 */
																							obj_t BgL_arg2046z00_1690;

																							{	/* Ieee/number.scm 699 */
																								obj_t BgL_arg2047z00_1691;

																								BgL_arg2047z00_1691 =
																									BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																									(BgL_yz00_90);
																								{	/* Ieee/number.scm 699 */
																									long BgL_xz00_3940;

																									BgL_xz00_3940 =
																										BELONG_TO_LONG
																										(BgL_arg2047z00_1691);
																									BgL_arg2046z00_1690 =
																										bgl_long_to_bignum
																										(BgL_xz00_3940);
																							}}
																							if (
																								((long) (bgl_bignum_cmp
																										(BgL_xz00_89,
																											BgL_arg2046z00_1690)) >
																									0L))
																								{	/* Ieee/number.scm 691 */
																									return BgL_arg2046z00_1690;
																								}
																							else
																								{	/* Ieee/number.scm 691 */
																									return BgL_xz00_89;
																								}
																						}
																					else
																						{	/* Ieee/number.scm 699 */
																							if (LLONGP(BgL_yz00_90))
																								{	/* Ieee/number.scm 699 */
																									obj_t BgL_arg2049z00_1693;

																									BgL_arg2049z00_1693 =
																										bgl_llong_to_bignum
																										(BLLONG_TO_LLONG
																										(BgL_yz00_90));
																									if (((long) (bgl_bignum_cmp
																												(BgL_xz00_89,
																													BgL_arg2049z00_1693))
																											> 0L))
																										{	/* Ieee/number.scm 691 */
																											return
																												BgL_arg2049z00_1693;
																										}
																									else
																										{	/* Ieee/number.scm 691 */
																											return BgL_xz00_89;
																										}
																								}
																							else
																								{	/* Ieee/number.scm 699 */
																									if (BGL_UINT64P(BgL_yz00_90))
																										{	/* Ieee/number.scm 699 */
																											obj_t BgL_arg2052z00_1696;

																											BgL_arg2052z00_1696 =
																												bgl_uint64_to_bignum
																												(BGL_BINT64_TO_INT64
																												(BgL_yz00_90));
																											if (((long)
																													(bgl_bignum_cmp
																														(BgL_xz00_89,
																															BgL_arg2052z00_1696))
																													> 0L))
																												{	/* Ieee/number.scm 691 */
																													return
																														BgL_arg2052z00_1696;
																												}
																											else
																												{	/* Ieee/number.scm 691 */
																													return BgL_xz00_89;
																												}
																										}
																									else
																										{	/* Ieee/number.scm 699 */
																											return
																												BGl_errorz00zz__errorz00
																												(BGl_string3338z00zz__r4_numbers_6_5z00,
																												BGl_string3328z00zz__r4_numbers_6_5z00,
																												BgL_yz00_90);
																										}
																								}
																						}
																				}
																		}
																}
														}
													else
														{	/* Ieee/number.scm 699 */
															return
																BGl_errorz00zz__errorz00
																(BGl_string3338z00zz__r4_numbers_6_5z00,
																BGl_string3328z00zz__r4_numbers_6_5z00,
																BgL_xz00_89);
														}
												}
										}
								}
						}
				}
		}

	}



/* &2min */
	obj_t BGl_z622minz62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5053,
		obj_t BgL_xz00_5054, obj_t BgL_yz00_5055)
	{
		{	/* Ieee/number.scm 698 */
			return BGl_2minz00zz__r4_numbers_6_5z00(BgL_xz00_5054, BgL_yz00_5055);
		}

	}



/* min */
	BGL_EXPORTED_DEF obj_t BGl_minz00zz__r4_numbers_6_5z00(obj_t BgL_xz00_91,
		obj_t BgL_yz00_92)
	{
		{	/* Ieee/number.scm 704 */
			{
				obj_t BgL_xz00_3977;
				obj_t BgL_yz00_3978;

				BgL_xz00_3977 = BgL_xz00_91;
				BgL_yz00_3978 = BgL_yz00_92;
			BgL_loopz00_3976:
				if (PAIRP(BgL_yz00_3978))
					{
						obj_t BgL_yz00_7733;
						obj_t BgL_xz00_7730;

						BgL_xz00_7730 =
							BGl_2minz00zz__r4_numbers_6_5z00(BgL_xz00_3977,
							CAR(BgL_yz00_3978));
						BgL_yz00_7733 = CDR(BgL_yz00_3978);
						BgL_yz00_3978 = BgL_yz00_7733;
						BgL_xz00_3977 = BgL_xz00_7730;
						goto BgL_loopz00_3976;
					}
				else
					{	/* Ieee/number.scm 707 */
						return BgL_xz00_3977;
					}
			}
		}

	}



/* &min */
	obj_t BGl_z62minz62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5056,
		obj_t BgL_xz00_5057, obj_t BgL_yz00_5058)
	{
		{	/* Ieee/number.scm 704 */
			return BGl_minz00zz__r4_numbers_6_5z00(BgL_xz00_5057, BgL_yz00_5058);
		}

	}



/* 2+ */
	BGL_EXPORTED_DEF obj_t BGl_2zb2zb2zz__r4_numbers_6_5z00(obj_t BgL_xz00_93,
		obj_t BgL_yz00_94)
	{
		{	/* Ieee/number.scm 714 */
			if (INTEGERP(BgL_xz00_93))
				{	/* Ieee/number.scm 715 */
					if (INTEGERP(BgL_yz00_94))
						{	/* Ieee/number.scm 715 */
							long BgL_auxz00_7742;
							long BgL_tmpz00_7740;

							BgL_auxz00_7742 = (long) CINT(BgL_yz00_94);
							BgL_tmpz00_7740 = (long) CINT(BgL_xz00_93);
							return BGL_SAFE_PLUS_FX(BgL_tmpz00_7740, BgL_auxz00_7742);
						}
					else
						{	/* Ieee/number.scm 715 */
							if (REALP(BgL_yz00_94))
								{	/* Ieee/number.scm 715 */
									return
										DOUBLE_TO_REAL(
										((double) (
												(long) CINT(BgL_xz00_93)) +
											REAL_TO_DOUBLE(BgL_yz00_94)));
								}
							else
								{	/* Ieee/number.scm 715 */
									if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_94))
										{	/* Ieee/number.scm 715 */
											long BgL_arg2063z00_1711;
											obj_t BgL_arg2064z00_1712;

											{	/* Ieee/number.scm 715 */
												long BgL_tmpz00_7754;

												BgL_tmpz00_7754 = (long) CINT(BgL_xz00_93);
												BgL_arg2063z00_1711 = (long) (BgL_tmpz00_7754);
											}
											BgL_arg2064z00_1712 =
												BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
												(BgL_yz00_94);
											{	/* Ieee/number.scm 715 */
												long BgL_tmpz00_7758;

												BgL_tmpz00_7758 = BELONG_TO_LONG(BgL_arg2064z00_1712);
												return
													BGL_SAFE_PLUS_ELONG(BgL_arg2063z00_1711,
													BgL_tmpz00_7758);
											}
										}
									else
										{	/* Ieee/number.scm 715 */
											if (LLONGP(BgL_yz00_94))
												{	/* Ieee/number.scm 715 */
													BGL_LONGLONG_T BgL_arg2067z00_1714;

													{	/* Ieee/number.scm 715 */
														long BgL_tmpz00_7763;

														BgL_tmpz00_7763 = (long) CINT(BgL_xz00_93);
														BgL_arg2067z00_1714 =
															LONG_TO_LLONG(BgL_tmpz00_7763);
													}
													{	/* Ieee/number.scm 715 */
														BGL_LONGLONG_T BgL_tmpz00_7766;

														BgL_tmpz00_7766 = BLLONG_TO_LLONG(BgL_yz00_94);
														return
															BGL_SAFE_PLUS_LLONG(BgL_arg2067z00_1714,
															BgL_tmpz00_7766);
													}
												}
											else
												{	/* Ieee/number.scm 715 */
													if (BGL_UINT64P(BgL_yz00_94))
														{	/* Ieee/number.scm 715 */
															uint64_t BgL_arg2070z00_1717;

															{	/* Ieee/number.scm 715 */
																BGL_LONGLONG_T BgL_arg2072z00_1718;

																{	/* Ieee/number.scm 715 */
																	long BgL_tmpz00_7771;

																	BgL_tmpz00_7771 = (long) CINT(BgL_xz00_93);
																	BgL_arg2072z00_1718 =
																		LONG_TO_LLONG(BgL_tmpz00_7771);
																}
																BgL_arg2070z00_1717 =
																	(uint64_t) (BgL_arg2072z00_1718);
															}
															{	/* Ieee/number.scm 715 */
																uint64_t BgL_za72za7_3996;

																BgL_za72za7_3996 =
																	BGL_BINT64_TO_INT64(BgL_yz00_94);
																{	/* Ieee/number.scm 715 */
																	uint64_t BgL_tmpz00_7776;

																	BgL_tmpz00_7776 =
																		(BgL_arg2070z00_1717 + BgL_za72za7_3996);
																	return BGL_UINT64_TO_BUINT64(BgL_tmpz00_7776);
																}
															}
														}
													else
														{	/* Ieee/number.scm 715 */
															if (BIGNUMP(BgL_yz00_94))
																{	/* Ieee/number.scm 715 */
																	obj_t BgL_tmpz00_7781;

																	BgL_tmpz00_7781 =
																		bgl_bignum_add(bgl_long_to_bignum(
																			(long) CINT(BgL_xz00_93)), BgL_yz00_94);
																	return BGL_SAFE_BX_TO_FX(BgL_tmpz00_7781);
																}
															else
																{	/* Ieee/number.scm 715 */
																	return
																		BGl_errorz00zz__errorz00
																		(BGl_string3339z00zz__r4_numbers_6_5z00,
																		BGl_string3328z00zz__r4_numbers_6_5z00,
																		BgL_yz00_94);
																}
														}
												}
										}
								}
						}
				}
			else
				{	/* Ieee/number.scm 715 */
					if (REALP(BgL_xz00_93))
						{	/* Ieee/number.scm 715 */
							if (REALP(BgL_yz00_94))
								{	/* Ieee/number.scm 715 */
									return
										DOUBLE_TO_REAL(
										(REAL_TO_DOUBLE(BgL_xz00_93) +
											REAL_TO_DOUBLE(BgL_yz00_94)));
								}
							else
								{	/* Ieee/number.scm 715 */
									if (INTEGERP(BgL_yz00_94))
										{	/* Ieee/number.scm 715 */
											return
												DOUBLE_TO_REAL(
												(REAL_TO_DOUBLE(BgL_xz00_93) +
													(double) ((long) CINT(BgL_yz00_94))));
										}
									else
										{	/* Ieee/number.scm 715 */
											if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00
												(BgL_yz00_94))
												{	/* Ieee/number.scm 715 */
													double BgL_arg2081z00_1727;

													{	/* Ieee/number.scm 715 */
														obj_t BgL_arg2082z00_1728;

														BgL_arg2082z00_1728 =
															BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
															(BgL_yz00_94);
														BgL_arg2081z00_1727 =
															(double) (BELONG_TO_LONG(BgL_arg2082z00_1728));
													}
													return
														DOUBLE_TO_REAL(
														(REAL_TO_DOUBLE(BgL_xz00_93) +
															BgL_arg2081z00_1727));
												}
											else
												{	/* Ieee/number.scm 715 */
													if (LLONGP(BgL_yz00_94))
														{	/* Ieee/number.scm 715 */
															return
																DOUBLE_TO_REAL(
																(REAL_TO_DOUBLE(BgL_xz00_93) +
																	(double) (BLLONG_TO_LLONG(BgL_yz00_94))));
														}
													else
														{	/* Ieee/number.scm 715 */
															if (BGL_UINT64P(BgL_yz00_94))
																{	/* Ieee/number.scm 715 */
																	double BgL_arg2088z00_1733;

																	{	/* Ieee/number.scm 715 */
																		uint64_t BgL_xz00_4008;

																		BgL_xz00_4008 =
																			BGL_BINT64_TO_INT64(BgL_yz00_94);
																		BgL_arg2088z00_1733 =
																			(double) (BgL_xz00_4008);
																	}
																	return
																		DOUBLE_TO_REAL(
																		(REAL_TO_DOUBLE(BgL_xz00_93) +
																			BgL_arg2088z00_1733));
																}
															else
																{	/* Ieee/number.scm 715 */
																	if (BIGNUMP(BgL_yz00_94))
																		{	/* Ieee/number.scm 715 */
																			return
																				DOUBLE_TO_REAL(
																				(REAL_TO_DOUBLE(BgL_xz00_93) +
																					bgl_bignum_to_flonum(BgL_yz00_94)));
																		}
																	else
																		{	/* Ieee/number.scm 715 */
																			return
																				BGl_errorz00zz__errorz00
																				(BGl_string3339z00zz__r4_numbers_6_5z00,
																				BGl_string3328z00zz__r4_numbers_6_5z00,
																				BgL_yz00_94);
																		}
																}
														}
												}
										}
								}
						}
					else
						{	/* Ieee/number.scm 715 */
							if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_xz00_93))
								{	/* Ieee/number.scm 715 */
									if (INTEGERP(BgL_yz00_94))
										{	/* Ieee/number.scm 715 */
											obj_t BgL_arg2093z00_1738;
											long BgL_arg2094z00_1739;

											BgL_arg2093z00_1738 =
												BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
												(BgL_xz00_93);
											{	/* Ieee/number.scm 715 */
												long BgL_tmpz00_7836;

												BgL_tmpz00_7836 = (long) CINT(BgL_yz00_94);
												BgL_arg2094z00_1739 = (long) (BgL_tmpz00_7836);
											}
											{	/* Ieee/number.scm 715 */
												long BgL_tmpz00_7839;

												BgL_tmpz00_7839 = BELONG_TO_LONG(BgL_arg2093z00_1738);
												return
													BGL_SAFE_PLUS_ELONG(BgL_tmpz00_7839,
													BgL_arg2094z00_1739);
											}
										}
									else
										{	/* Ieee/number.scm 715 */
											if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00
												(BgL_yz00_94))
												{	/* Ieee/number.scm 715 */
													obj_t BgL_arg2096z00_1741;
													obj_t BgL_arg2097z00_1742;

													BgL_arg2096z00_1741 =
														BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
														(BgL_xz00_93);
													BgL_arg2097z00_1742 =
														BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
														(BgL_yz00_94);
													{	/* Ieee/number.scm 715 */
														long BgL_auxz00_7848;
														long BgL_tmpz00_7846;

														BgL_auxz00_7848 =
															BELONG_TO_LONG(BgL_arg2097z00_1742);
														BgL_tmpz00_7846 =
															BELONG_TO_LONG(BgL_arg2096z00_1741);
														return
															BGL_SAFE_PLUS_ELONG(BgL_tmpz00_7846,
															BgL_auxz00_7848);
													}
												}
											else
												{	/* Ieee/number.scm 715 */
													if (REALP(BgL_yz00_94))
														{	/* Ieee/number.scm 715 */
															double BgL_arg2099z00_1744;

															{	/* Ieee/number.scm 715 */
																obj_t BgL_arg2100z00_1745;

																BgL_arg2100z00_1745 =
																	BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																	(BgL_xz00_93);
																BgL_arg2099z00_1744 =
																	(double) (BELONG_TO_LONG
																	(BgL_arg2100z00_1745));
															}
															return
																DOUBLE_TO_REAL(
																(BgL_arg2099z00_1744 +
																	REAL_TO_DOUBLE(BgL_yz00_94)));
														}
													else
														{	/* Ieee/number.scm 715 */
															if (LLONGP(BgL_yz00_94))
																{	/* Ieee/number.scm 715 */
																	BGL_LONGLONG_T BgL_arg2102z00_1747;

																	{	/* Ieee/number.scm 715 */
																		obj_t BgL_arg2104z00_1749;

																		BgL_arg2104z00_1749 =
																			BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																			(BgL_xz00_93);
																		BgL_arg2102z00_1747 =
																			(BGL_LONGLONG_T) (BELONG_TO_LONG
																			(BgL_arg2104z00_1749));
																	}
																	{	/* Ieee/number.scm 715 */
																		BGL_LONGLONG_T BgL_tmpz00_7864;

																		BgL_tmpz00_7864 =
																			BLLONG_TO_LLONG(BgL_yz00_94);
																		return
																			BGL_SAFE_PLUS_LLONG(BgL_arg2102z00_1747,
																			BgL_tmpz00_7864);
																	}
																}
															else
																{	/* Ieee/number.scm 715 */
																	if (BGL_UINT64P(BgL_yz00_94))
																		{	/* Ieee/number.scm 715 */
																			uint64_t BgL_arg2106z00_1751;

																			{	/* Ieee/number.scm 715 */
																				BGL_LONGLONG_T BgL_arg2107z00_1752;

																				{	/* Ieee/number.scm 715 */
																					obj_t BgL_arg2108z00_1753;

																					BgL_arg2108z00_1753 =
																						BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																						(BgL_xz00_93);
																					BgL_arg2107z00_1752 =
																						(BGL_LONGLONG_T) (BELONG_TO_LONG
																						(BgL_arg2108z00_1753));
																				}
																				BgL_arg2106z00_1751 =
																					(uint64_t) (BgL_arg2107z00_1752);
																			}
																			{	/* Ieee/number.scm 715 */
																				uint64_t BgL_za72za7_4019;

																				BgL_za72za7_4019 =
																					BGL_BINT64_TO_INT64(BgL_yz00_94);
																				{	/* Ieee/number.scm 715 */
																					uint64_t BgL_tmpz00_7874;

																					BgL_tmpz00_7874 =
																						(BgL_arg2106z00_1751 +
																						BgL_za72za7_4019);
																					return
																						BGL_UINT64_TO_BUINT64
																						(BgL_tmpz00_7874);
																				}
																			}
																		}
																	else
																		{	/* Ieee/number.scm 715 */
																			if (BIGNUMP(BgL_yz00_94))
																				{	/* Ieee/number.scm 715 */
																					obj_t BgL_arg2110z00_1755;

																					{	/* Ieee/number.scm 715 */
																						obj_t BgL_arg2111z00_1756;

																						BgL_arg2111z00_1756 =
																							BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																							(BgL_xz00_93);
																						{	/* Ieee/number.scm 715 */
																							long BgL_xz00_4020;

																							BgL_xz00_4020 =
																								BELONG_TO_LONG
																								(BgL_arg2111z00_1756);
																							BgL_arg2110z00_1755 =
																								bgl_long_to_bignum
																								(BgL_xz00_4020);
																					}}
																					return
																						bgl_bignum_add(BgL_arg2110z00_1755,
																						BgL_yz00_94);
																				}
																			else
																				{	/* Ieee/number.scm 715 */
																					return
																						BGl_errorz00zz__errorz00
																						(BGl_string3339z00zz__r4_numbers_6_5z00,
																						BGl_string3328z00zz__r4_numbers_6_5z00,
																						BgL_yz00_94);
																				}
																		}
																}
														}
												}
										}
								}
							else
								{	/* Ieee/number.scm 715 */
									if (LLONGP(BgL_xz00_93))
										{	/* Ieee/number.scm 715 */
											if (INTEGERP(BgL_yz00_94))
												{	/* Ieee/number.scm 715 */
													BGL_LONGLONG_T BgL_arg2115z00_1760;

													{	/* Ieee/number.scm 715 */
														long BgL_tmpz00_7888;

														BgL_tmpz00_7888 = (long) CINT(BgL_yz00_94);
														BgL_arg2115z00_1760 =
															LONG_TO_LLONG(BgL_tmpz00_7888);
													}
													{	/* Ieee/number.scm 715 */
														BGL_LONGLONG_T BgL_tmpz00_7891;

														BgL_tmpz00_7891 = BLLONG_TO_LLONG(BgL_xz00_93);
														return
															BGL_SAFE_PLUS_LLONG(BgL_tmpz00_7891,
															BgL_arg2115z00_1760);
													}
												}
											else
												{	/* Ieee/number.scm 715 */
													if (REALP(BgL_yz00_94))
														{	/* Ieee/number.scm 715 */
															return
																DOUBLE_TO_REAL(
																((double) (BLLONG_TO_LLONG(BgL_xz00_93)) +
																	REAL_TO_DOUBLE(BgL_yz00_94)));
														}
													else
														{	/* Ieee/number.scm 715 */
															if (LLONGP(BgL_yz00_94))
																{	/* Ieee/number.scm 715 */
																	BGL_LONGLONG_T BgL_auxz00_7905;
																	BGL_LONGLONG_T BgL_tmpz00_7903;

																	BgL_auxz00_7905 =
																		BLLONG_TO_LLONG(BgL_yz00_94);
																	BgL_tmpz00_7903 =
																		BLLONG_TO_LLONG(BgL_xz00_93);
																	return
																		BGL_SAFE_PLUS_LLONG(BgL_tmpz00_7903,
																		BgL_auxz00_7905);
																}
															else
																{	/* Ieee/number.scm 715 */
																	if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00
																		(BgL_yz00_94))
																		{	/* Ieee/number.scm 715 */
																			BGL_LONGLONG_T BgL_arg2123z00_1768;

																			{	/* Ieee/number.scm 715 */
																				obj_t BgL_arg2124z00_1769;

																				BgL_arg2124z00_1769 =
																					BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																					(BgL_yz00_94);
																				BgL_arg2123z00_1768 =
																					(BGL_LONGLONG_T) (BELONG_TO_LONG
																					(BgL_arg2124z00_1769));
																			}
																			{	/* Ieee/number.scm 715 */
																				BGL_LONGLONG_T BgL_tmpz00_7913;

																				BgL_tmpz00_7913 =
																					BLLONG_TO_LLONG(BgL_xz00_93);
																				return
																					BGL_SAFE_PLUS_LLONG(BgL_tmpz00_7913,
																					BgL_arg2123z00_1768);
																			}
																		}
																	else
																		{	/* Ieee/number.scm 715 */
																			if (BIGNUMP(BgL_yz00_94))
																				{	/* Ieee/number.scm 715 */
																					return
																						bgl_bignum_add(bgl_llong_to_bignum
																						(BLLONG_TO_LLONG(BgL_xz00_93)),
																						BgL_yz00_94);
																				}
																			else
																				{	/* Ieee/number.scm 715 */
																					if (BGL_UINT64P(BgL_yz00_94))
																						{	/* Ieee/number.scm 715 */
																							uint64_t BgL_arg2129z00_1774;

																							{	/* Ieee/number.scm 715 */
																								BGL_LONGLONG_T BgL_tmpz00_7923;

																								BgL_tmpz00_7923 =
																									BLLONG_TO_LLONG(BgL_xz00_93);
																								BgL_arg2129z00_1774 =
																									(uint64_t) (BgL_tmpz00_7923);
																							}
																							{	/* Ieee/number.scm 715 */
																								uint64_t BgL_za72za7_4031;

																								BgL_za72za7_4031 =
																									BGL_BINT64_TO_INT64
																									(BgL_yz00_94);
																								{	/* Ieee/number.scm 715 */
																									uint64_t BgL_tmpz00_7927;

																									BgL_tmpz00_7927 =
																										(BgL_arg2129z00_1774 +
																										BgL_za72za7_4031);
																									return
																										BGL_UINT64_TO_BUINT64
																										(BgL_tmpz00_7927);
																								}
																							}
																						}
																					else
																						{	/* Ieee/number.scm 715 */
																							return
																								BGl_errorz00zz__errorz00
																								(BGl_string3339z00zz__r4_numbers_6_5z00,
																								BGl_string3328z00zz__r4_numbers_6_5z00,
																								BgL_yz00_94);
																						}
																				}
																		}
																}
														}
												}
										}
									else
										{	/* Ieee/number.scm 715 */
											if (BGL_UINT64P(BgL_xz00_93))
												{	/* Ieee/number.scm 715 */
													if (INTEGERP(BgL_yz00_94))
														{	/* Ieee/number.scm 715 */
															uint64_t BgL_arg2132z00_1777;

															{	/* Ieee/number.scm 715 */
																long BgL_tmpz00_7935;

																BgL_tmpz00_7935 = (long) CINT(BgL_yz00_94);
																BgL_arg2132z00_1777 =
																	(uint64_t) (BgL_tmpz00_7935);
															}
															{	/* Ieee/number.scm 715 */
																uint64_t BgL_za71za7_4033;

																BgL_za71za7_4033 =
																	BGL_BINT64_TO_INT64(BgL_xz00_93);
																{	/* Ieee/number.scm 715 */
																	uint64_t BgL_tmpz00_7939;

																	BgL_tmpz00_7939 =
																		(BgL_za71za7_4033 + BgL_arg2132z00_1777);
																	return BGL_UINT64_TO_BUINT64(BgL_tmpz00_7939);
																}
															}
														}
													else
														{	/* Ieee/number.scm 715 */
															if (BGL_UINT64P(BgL_yz00_94))
																{	/* Ieee/number.scm 715 */
																	uint64_t BgL_za71za7_4035;
																	uint64_t BgL_za72za7_4036;

																	BgL_za71za7_4035 =
																		BGL_BINT64_TO_INT64(BgL_xz00_93);
																	BgL_za72za7_4036 =
																		BGL_BINT64_TO_INT64(BgL_yz00_94);
																	{	/* Ieee/number.scm 715 */
																		uint64_t BgL_tmpz00_7946;

																		BgL_tmpz00_7946 =
																			(BgL_za71za7_4035 + BgL_za72za7_4036);
																		return
																			BGL_UINT64_TO_BUINT64(BgL_tmpz00_7946);
																	}
																}
															else
																{	/* Ieee/number.scm 715 */
																	if (REALP(BgL_yz00_94))
																		{	/* Ieee/number.scm 715 */
																			double BgL_arg2135z00_1780;

																			{	/* Ieee/number.scm 715 */
																				uint64_t BgL_tmpz00_7951;

																				BgL_tmpz00_7951 =
																					BGL_BINT64_TO_INT64(BgL_xz00_93);
																				BgL_arg2135z00_1780 =
																					(double) (BgL_tmpz00_7951);
																			}
																			return
																				DOUBLE_TO_REAL(
																				(BgL_arg2135z00_1780 +
																					REAL_TO_DOUBLE(BgL_yz00_94)));
																		}
																	else
																		{	/* Ieee/number.scm 715 */
																			if (LLONGP(BgL_yz00_94))
																				{	/* Ieee/number.scm 715 */
																					uint64_t BgL_arg2137z00_1782;

																					{	/* Ieee/number.scm 715 */
																						BGL_LONGLONG_T BgL_tmpz00_7959;

																						BgL_tmpz00_7959 =
																							BLLONG_TO_LLONG(BgL_yz00_94);
																						BgL_arg2137z00_1782 =
																							(uint64_t) (BgL_tmpz00_7959);
																					}
																					{	/* Ieee/number.scm 715 */
																						uint64_t BgL_za71za7_4040;

																						BgL_za71za7_4040 =
																							BGL_BINT64_TO_INT64(BgL_xz00_93);
																						{	/* Ieee/number.scm 715 */
																							uint64_t BgL_tmpz00_7963;

																							BgL_tmpz00_7963 =
																								(BgL_za71za7_4040 +
																								BgL_arg2137z00_1782);
																							return
																								BGL_UINT64_TO_BUINT64
																								(BgL_tmpz00_7963);
																						}
																					}
																				}
																			else
																				{	/* Ieee/number.scm 715 */
																					if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_94))
																						{	/* Ieee/number.scm 715 */
																							uint64_t BgL_arg2141z00_1785;

																							{	/* Ieee/number.scm 715 */
																								BGL_LONGLONG_T
																									BgL_arg2142z00_1786;
																								{	/* Ieee/number.scm 715 */
																									obj_t BgL_arg2143z00_1787;

																									BgL_arg2143z00_1787 =
																										BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																										(BgL_yz00_94);
																									BgL_arg2142z00_1786 =
																										(BGL_LONGLONG_T)
																										(BELONG_TO_LONG
																										(BgL_arg2143z00_1787));
																								}
																								BgL_arg2141z00_1785 =
																									(uint64_t)
																									(BgL_arg2142z00_1786);
																							}
																							{	/* Ieee/number.scm 715 */
																								uint64_t BgL_za71za7_4043;

																								BgL_za71za7_4043 =
																									BGL_BINT64_TO_INT64
																									(BgL_xz00_93);
																								{	/* Ieee/number.scm 715 */
																									uint64_t BgL_tmpz00_7973;

																									BgL_tmpz00_7973 =
																										(BgL_za71za7_4043 +
																										BgL_arg2141z00_1785);
																									return
																										BGL_UINT64_TO_BUINT64
																										(BgL_tmpz00_7973);
																								}
																							}
																						}
																					else
																						{	/* Ieee/number.scm 715 */
																							if (BIGNUMP(BgL_yz00_94))
																								{	/* Ieee/number.scm 715 */
																									return
																										bgl_bignum_add
																										(bgl_uint64_to_bignum
																										(BGL_BINT64_TO_INT64
																											(BgL_xz00_93)),
																										BgL_yz00_94);
																								}
																							else
																								{	/* Ieee/number.scm 715 */
																									return
																										BGl_errorz00zz__errorz00
																										(BGl_string3339z00zz__r4_numbers_6_5z00,
																										BGl_string3329z00zz__r4_numbers_6_5z00,
																										BgL_yz00_94);
																								}
																						}
																				}
																		}
																}
														}
												}
											else
												{	/* Ieee/number.scm 715 */
													if (BIGNUMP(BgL_xz00_93))
														{	/* Ieee/number.scm 715 */
															if (BIGNUMP(BgL_yz00_94))
																{	/* Ieee/number.scm 715 */
																	obj_t BgL_tmpz00_7986;

																	BgL_tmpz00_7986 =
																		bgl_bignum_add(BgL_xz00_93, BgL_yz00_94);
																	return BGL_SAFE_BX_TO_FX(BgL_tmpz00_7986);
																}
															else
																{	/* Ieee/number.scm 715 */
																	if (INTEGERP(BgL_yz00_94))
																		{	/* Ieee/number.scm 715 */
																			obj_t BgL_tmpz00_7991;

																			BgL_tmpz00_7991 =
																				bgl_bignum_add(BgL_xz00_93,
																				bgl_long_to_bignum(
																					(long) CINT(BgL_yz00_94)));
																			return BGL_SAFE_BX_TO_FX(BgL_tmpz00_7991);
																		}
																	else
																		{	/* Ieee/number.scm 715 */
																			if (REALP(BgL_yz00_94))
																				{	/* Ieee/number.scm 715 */
																					return
																						DOUBLE_TO_REAL(
																						(bgl_bignum_to_flonum(BgL_xz00_93) +
																							REAL_TO_DOUBLE(BgL_yz00_94)));
																				}
																			else
																				{	/* Ieee/number.scm 715 */
																					if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_94))
																						{	/* Ieee/number.scm 715 */
																							obj_t BgL_arg2156z00_1799;

																							{	/* Ieee/number.scm 715 */
																								obj_t BgL_arg2157z00_1800;

																								BgL_arg2157z00_1800 =
																									BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																									(BgL_yz00_94);
																								{	/* Ieee/number.scm 715 */
																									long BgL_xz00_4055;

																									BgL_xz00_4055 =
																										BELONG_TO_LONG
																										(BgL_arg2157z00_1800);
																									BgL_arg2156z00_1799 =
																										bgl_long_to_bignum
																										(BgL_xz00_4055);
																							}}
																							return
																								bgl_bignum_add(BgL_xz00_93,
																								BgL_arg2156z00_1799);
																						}
																					else
																						{	/* Ieee/number.scm 715 */
																							if (LLONGP(BgL_yz00_94))
																								{	/* Ieee/number.scm 715 */
																									return
																										bgl_bignum_add(BgL_xz00_93,
																										bgl_llong_to_bignum
																										(BLLONG_TO_LLONG
																											(BgL_yz00_94)));
																								}
																							else
																								{	/* Ieee/number.scm 715 */
																									if (BGL_UINT64P(BgL_yz00_94))
																										{	/* Ieee/number.scm 715 */
																											return
																												bgl_bignum_add
																												(BgL_xz00_93,
																												bgl_uint64_to_bignum
																												(BGL_BINT64_TO_INT64
																													(BgL_yz00_94)));
																										}
																									else
																										{	/* Ieee/number.scm 715 */
																											return
																												BGl_errorz00zz__errorz00
																												(BGl_string3339z00zz__r4_numbers_6_5z00,
																												BGl_string3328z00zz__r4_numbers_6_5z00,
																												BgL_yz00_94);
																										}
																								}
																						}
																				}
																		}
																}
														}
													else
														{	/* Ieee/number.scm 715 */
															return
																BGl_errorz00zz__errorz00
																(BGl_string3339z00zz__r4_numbers_6_5z00,
																BGl_string3328z00zz__r4_numbers_6_5z00,
																BgL_xz00_93);
														}
												}
										}
								}
						}
				}
		}

	}



/* &2+ */
	obj_t BGl_z622zb2zd0zz__r4_numbers_6_5z00(obj_t BgL_envz00_5059,
		obj_t BgL_xz00_5060, obj_t BgL_yz00_5061)
	{
		{	/* Ieee/number.scm 714 */
			return BGl_2zb2zb2zz__r4_numbers_6_5z00(BgL_xz00_5060, BgL_yz00_5061);
		}

	}



/* + */
	BGL_EXPORTED_DEF obj_t BGl_zb2zb2zz__r4_numbers_6_5z00(obj_t BgL_xz00_95)
	{
		{	/* Ieee/number.scm 720 */
			if (NULLP(BgL_xz00_95))
				{	/* Ieee/number.scm 723 */
					return BINT(0L);
				}
			else
				{
					obj_t BgL_sumz00_4078;
					obj_t BgL_xz00_4079;

					BgL_sumz00_4078 = CAR(((obj_t) BgL_xz00_95));
					BgL_xz00_4079 = CDR(((obj_t) BgL_xz00_95));
				BgL_loopz00_4077:
					if (PAIRP(BgL_xz00_4079))
						{
							obj_t BgL_xz00_8029;
							obj_t BgL_sumz00_8026;

							BgL_sumz00_8026 =
								BGl_2zb2zb2zz__r4_numbers_6_5z00(BgL_sumz00_4078,
								CAR(BgL_xz00_4079));
							BgL_xz00_8029 = CDR(BgL_xz00_4079);
							BgL_xz00_4079 = BgL_xz00_8029;
							BgL_sumz00_4078 = BgL_sumz00_8026;
							goto BgL_loopz00_4077;
						}
					else
						{	/* Ieee/number.scm 727 */
							return BgL_sumz00_4078;
						}
				}
		}

	}



/* &+ */
	obj_t BGl_z62zb2zd0zz__r4_numbers_6_5z00(obj_t BgL_envz00_5062,
		obj_t BgL_xz00_5063)
	{
		{	/* Ieee/number.scm 720 */
			return BGl_zb2zb2zz__r4_numbers_6_5z00(BgL_xz00_5063);
		}

	}



/* 2* */
	BGL_EXPORTED_DEF obj_t BGl_2za2za2zz__r4_numbers_6_5z00(obj_t BgL_xz00_96,
		obj_t BgL_yz00_97)
	{
		{	/* Ieee/number.scm 734 */
			if (INTEGERP(BgL_xz00_96))
				{	/* Ieee/number.scm 735 */
					if (INTEGERP(BgL_yz00_97))
						{	/* Ieee/number.scm 735 */
							long BgL_auxz00_8042;
							long BgL_tmpz00_8040;

							BgL_auxz00_8042 = (long) CINT(BgL_yz00_97);
							BgL_tmpz00_8040 = (long) CINT(BgL_xz00_96);
							return BGL_SAFE_MUL_FX(BgL_tmpz00_8040, BgL_auxz00_8042);
						}
					else
						{	/* Ieee/number.scm 735 */
							if (REALP(BgL_yz00_97))
								{	/* Ieee/number.scm 735 */
									return
										DOUBLE_TO_REAL(
										((double) (
												(long) CINT(BgL_xz00_96)) *
											REAL_TO_DOUBLE(BgL_yz00_97)));
								}
							else
								{	/* Ieee/number.scm 735 */
									if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_97))
										{	/* Ieee/number.scm 735 */
											long BgL_arg2174z00_1823;
											obj_t BgL_arg2175z00_1824;

											{	/* Ieee/number.scm 735 */
												long BgL_tmpz00_8054;

												BgL_tmpz00_8054 = (long) CINT(BgL_xz00_96);
												BgL_arg2174z00_1823 = (long) (BgL_tmpz00_8054);
											}
											BgL_arg2175z00_1824 =
												BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
												(BgL_yz00_97);
											{	/* Ieee/number.scm 735 */
												long BgL_tmpz00_8058;

												BgL_tmpz00_8058 = BELONG_TO_LONG(BgL_arg2175z00_1824);
												return
													BGL_SAFE_MUL_ELONG(BgL_arg2174z00_1823,
													BgL_tmpz00_8058);
											}
										}
									else
										{	/* Ieee/number.scm 735 */
											if (LLONGP(BgL_yz00_97))
												{	/* Ieee/number.scm 735 */
													BGL_LONGLONG_T BgL_arg2177z00_1826;

													{	/* Ieee/number.scm 735 */
														long BgL_tmpz00_8063;

														BgL_tmpz00_8063 = (long) CINT(BgL_xz00_96);
														BgL_arg2177z00_1826 =
															LONG_TO_LLONG(BgL_tmpz00_8063);
													}
													{	/* Ieee/number.scm 735 */
														BGL_LONGLONG_T BgL_tmpz00_8066;

														BgL_tmpz00_8066 = BLLONG_TO_LLONG(BgL_yz00_97);
														return
															BGL_SAFE_MUL_LLONG(BgL_arg2177z00_1826,
															BgL_tmpz00_8066);
													}
												}
											else
												{	/* Ieee/number.scm 735 */
													if (BGL_UINT64P(BgL_yz00_97))
														{	/* Ieee/number.scm 735 */
															uint64_t BgL_arg2180z00_1829;

															{	/* Ieee/number.scm 735 */
																BGL_LONGLONG_T BgL_arg2181z00_1830;

																{	/* Ieee/number.scm 735 */
																	long BgL_tmpz00_8071;

																	BgL_tmpz00_8071 = (long) CINT(BgL_xz00_96);
																	BgL_arg2181z00_1830 =
																		LONG_TO_LLONG(BgL_tmpz00_8071);
																}
																BgL_arg2180z00_1829 =
																	(uint64_t) (BgL_arg2181z00_1830);
															}
															{	/* Ieee/number.scm 735 */
																uint64_t BgL_za72za7_4097;

																BgL_za72za7_4097 =
																	BGL_BINT64_TO_INT64(BgL_yz00_97);
																{	/* Ieee/number.scm 735 */
																	uint64_t BgL_tmpz00_8076;

																	BgL_tmpz00_8076 =
																		(BgL_arg2180z00_1829 * BgL_za72za7_4097);
																	return BGL_UINT64_TO_BUINT64(BgL_tmpz00_8076);
																}
															}
														}
													else
														{	/* Ieee/number.scm 735 */
															if (BIGNUMP(BgL_yz00_97))
																{	/* Ieee/number.scm 735 */
																	obj_t BgL_tmpz00_8081;

																	BgL_tmpz00_8081 =
																		bgl_bignum_mul(bgl_long_to_bignum(
																			(long) CINT(BgL_xz00_96)), BgL_yz00_97);
																	return BGL_SAFE_BX_TO_FX(BgL_tmpz00_8081);
																}
															else
																{	/* Ieee/number.scm 735 */
																	return
																		BGl_errorz00zz__errorz00
																		(BGl_string3340z00zz__r4_numbers_6_5z00,
																		BGl_string3328z00zz__r4_numbers_6_5z00,
																		BgL_yz00_97);
																}
														}
												}
										}
								}
						}
				}
			else
				{	/* Ieee/number.scm 735 */
					if (REALP(BgL_xz00_96))
						{	/* Ieee/number.scm 735 */
							if (REALP(BgL_yz00_97))
								{	/* Ieee/number.scm 735 */
									return
										DOUBLE_TO_REAL(
										(REAL_TO_DOUBLE(BgL_xz00_96) *
											REAL_TO_DOUBLE(BgL_yz00_97)));
								}
							else
								{	/* Ieee/number.scm 735 */
									if (INTEGERP(BgL_yz00_97))
										{	/* Ieee/number.scm 735 */
											return
												DOUBLE_TO_REAL(
												(REAL_TO_DOUBLE(BgL_xz00_96) *
													(double) ((long) CINT(BgL_yz00_97))));
										}
									else
										{	/* Ieee/number.scm 735 */
											if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00
												(BgL_yz00_97))
												{	/* Ieee/number.scm 735 */
													double BgL_arg2190z00_1839;

													{	/* Ieee/number.scm 735 */
														obj_t BgL_arg2191z00_1840;

														BgL_arg2191z00_1840 =
															BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
															(BgL_yz00_97);
														BgL_arg2190z00_1839 =
															(double) (BELONG_TO_LONG(BgL_arg2191z00_1840));
													}
													return
														DOUBLE_TO_REAL(
														(REAL_TO_DOUBLE(BgL_xz00_96) *
															BgL_arg2190z00_1839));
												}
											else
												{	/* Ieee/number.scm 735 */
													if (LLONGP(BgL_yz00_97))
														{	/* Ieee/number.scm 735 */
															return
																DOUBLE_TO_REAL(
																(REAL_TO_DOUBLE(BgL_xz00_96) *
																	(double) (BLLONG_TO_LLONG(BgL_yz00_97))));
														}
													else
														{	/* Ieee/number.scm 735 */
															if (BGL_UINT64P(BgL_yz00_97))
																{	/* Ieee/number.scm 735 */
																	double BgL_arg2196z00_1845;

																	{	/* Ieee/number.scm 735 */
																		uint64_t BgL_xz00_4109;

																		BgL_xz00_4109 =
																			BGL_BINT64_TO_INT64(BgL_yz00_97);
																		BgL_arg2196z00_1845 =
																			(double) (BgL_xz00_4109);
																	}
																	return
																		DOUBLE_TO_REAL(
																		(REAL_TO_DOUBLE(BgL_xz00_96) *
																			BgL_arg2196z00_1845));
																}
															else
																{	/* Ieee/number.scm 735 */
																	if (BIGNUMP(BgL_yz00_97))
																		{	/* Ieee/number.scm 735 */
																			return
																				DOUBLE_TO_REAL(
																				(REAL_TO_DOUBLE(BgL_xz00_96) *
																					bgl_bignum_to_flonum(BgL_yz00_97)));
																		}
																	else
																		{	/* Ieee/number.scm 735 */
																			return
																				BGl_errorz00zz__errorz00
																				(BGl_string3340z00zz__r4_numbers_6_5z00,
																				BGl_string3328z00zz__r4_numbers_6_5z00,
																				BgL_yz00_97);
																		}
																}
														}
												}
										}
								}
						}
					else
						{	/* Ieee/number.scm 735 */
							if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_xz00_96))
								{	/* Ieee/number.scm 735 */
									if (INTEGERP(BgL_yz00_97))
										{	/* Ieee/number.scm 735 */
											obj_t BgL_arg2201z00_1850;
											long BgL_arg2202z00_1851;

											BgL_arg2201z00_1850 =
												BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
												(BgL_xz00_96);
											{	/* Ieee/number.scm 735 */
												long BgL_tmpz00_8136;

												BgL_tmpz00_8136 = (long) CINT(BgL_yz00_97);
												BgL_arg2202z00_1851 = (long) (BgL_tmpz00_8136);
											}
											{	/* Ieee/number.scm 735 */
												long BgL_tmpz00_8139;

												BgL_tmpz00_8139 = BELONG_TO_LONG(BgL_arg2201z00_1850);
												return
													BGL_SAFE_MUL_ELONG(BgL_tmpz00_8139,
													BgL_arg2202z00_1851);
											}
										}
									else
										{	/* Ieee/number.scm 735 */
											if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00
												(BgL_yz00_97))
												{	/* Ieee/number.scm 735 */
													obj_t BgL_arg2204z00_1853;
													obj_t BgL_arg2205z00_1854;

													BgL_arg2204z00_1853 =
														BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
														(BgL_xz00_96);
													BgL_arg2205z00_1854 =
														BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
														(BgL_yz00_97);
													{	/* Ieee/number.scm 735 */
														long BgL_auxz00_8148;
														long BgL_tmpz00_8146;

														BgL_auxz00_8148 =
															BELONG_TO_LONG(BgL_arg2205z00_1854);
														BgL_tmpz00_8146 =
															BELONG_TO_LONG(BgL_arg2204z00_1853);
														return
															BGL_SAFE_MUL_ELONG(BgL_tmpz00_8146,
															BgL_auxz00_8148);
													}
												}
											else
												{	/* Ieee/number.scm 735 */
													if (REALP(BgL_yz00_97))
														{	/* Ieee/number.scm 735 */
															double BgL_arg2207z00_1856;

															{	/* Ieee/number.scm 735 */
																obj_t BgL_arg2208z00_1857;

																BgL_arg2208z00_1857 =
																	BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																	(BgL_xz00_96);
																BgL_arg2207z00_1856 =
																	(double) (BELONG_TO_LONG
																	(BgL_arg2208z00_1857));
															}
															return
																DOUBLE_TO_REAL(
																(BgL_arg2207z00_1856 *
																	REAL_TO_DOUBLE(BgL_yz00_97)));
														}
													else
														{	/* Ieee/number.scm 735 */
															if (LLONGP(BgL_yz00_97))
																{	/* Ieee/number.scm 735 */
																	BGL_LONGLONG_T BgL_arg2210z00_1859;

																	{	/* Ieee/number.scm 735 */
																		obj_t BgL_arg2212z00_1861;

																		BgL_arg2212z00_1861 =
																			BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																			(BgL_xz00_96);
																		BgL_arg2210z00_1859 =
																			(BGL_LONGLONG_T) (BELONG_TO_LONG
																			(BgL_arg2212z00_1861));
																	}
																	{	/* Ieee/number.scm 735 */
																		BGL_LONGLONG_T BgL_tmpz00_8164;

																		BgL_tmpz00_8164 =
																			BLLONG_TO_LLONG(BgL_yz00_97);
																		return
																			BGL_SAFE_MUL_LLONG(BgL_arg2210z00_1859,
																			BgL_tmpz00_8164);
																	}
																}
															else
																{	/* Ieee/number.scm 735 */
																	if (BGL_UINT64P(BgL_yz00_97))
																		{	/* Ieee/number.scm 735 */
																			uint64_t BgL_arg2214z00_1863;

																			{	/* Ieee/number.scm 735 */
																				BGL_LONGLONG_T BgL_arg2215z00_1864;

																				{	/* Ieee/number.scm 735 */
																					obj_t BgL_arg2216z00_1865;

																					BgL_arg2216z00_1865 =
																						BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																						(BgL_xz00_96);
																					BgL_arg2215z00_1864 =
																						(BGL_LONGLONG_T) (BELONG_TO_LONG
																						(BgL_arg2216z00_1865));
																				}
																				BgL_arg2214z00_1863 =
																					(uint64_t) (BgL_arg2215z00_1864);
																			}
																			{	/* Ieee/number.scm 735 */
																				uint64_t BgL_za72za7_4120;

																				BgL_za72za7_4120 =
																					BGL_BINT64_TO_INT64(BgL_yz00_97);
																				{	/* Ieee/number.scm 735 */
																					uint64_t BgL_tmpz00_8174;

																					BgL_tmpz00_8174 =
																						(BgL_arg2214z00_1863 *
																						BgL_za72za7_4120);
																					return
																						BGL_UINT64_TO_BUINT64
																						(BgL_tmpz00_8174);
																				}
																			}
																		}
																	else
																		{	/* Ieee/number.scm 735 */
																			if (BIGNUMP(BgL_yz00_97))
																				{	/* Ieee/number.scm 735 */
																					obj_t BgL_arg2218z00_1867;

																					{	/* Ieee/number.scm 735 */
																						obj_t BgL_arg2219z00_1868;

																						BgL_arg2219z00_1868 =
																							BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																							(BgL_xz00_96);
																						{	/* Ieee/number.scm 735 */
																							long BgL_xz00_4121;

																							BgL_xz00_4121 =
																								BELONG_TO_LONG
																								(BgL_arg2219z00_1868);
																							BgL_arg2218z00_1867 =
																								bgl_long_to_bignum
																								(BgL_xz00_4121);
																					}}
																					return
																						bgl_bignum_mul(BgL_arg2218z00_1867,
																						BgL_yz00_97);
																				}
																			else
																				{	/* Ieee/number.scm 735 */
																					return
																						BGl_errorz00zz__errorz00
																						(BGl_string3340z00zz__r4_numbers_6_5z00,
																						BGl_string3328z00zz__r4_numbers_6_5z00,
																						BgL_yz00_97);
																				}
																		}
																}
														}
												}
										}
								}
							else
								{	/* Ieee/number.scm 735 */
									if (LLONGP(BgL_xz00_96))
										{	/* Ieee/number.scm 735 */
											if (INTEGERP(BgL_yz00_97))
												{	/* Ieee/number.scm 735 */
													BGL_LONGLONG_T BgL_arg2223z00_1872;

													{	/* Ieee/number.scm 735 */
														long BgL_tmpz00_8188;

														BgL_tmpz00_8188 = (long) CINT(BgL_yz00_97);
														BgL_arg2223z00_1872 =
															LONG_TO_LLONG(BgL_tmpz00_8188);
													}
													{	/* Ieee/number.scm 735 */
														BGL_LONGLONG_T BgL_tmpz00_8191;

														BgL_tmpz00_8191 = BLLONG_TO_LLONG(BgL_xz00_96);
														return
															BGL_SAFE_MUL_LLONG(BgL_tmpz00_8191,
															BgL_arg2223z00_1872);
													}
												}
											else
												{	/* Ieee/number.scm 735 */
													if (REALP(BgL_yz00_97))
														{	/* Ieee/number.scm 735 */
															return
																DOUBLE_TO_REAL(
																((double) (BLLONG_TO_LLONG(BgL_xz00_96)) *
																	REAL_TO_DOUBLE(BgL_yz00_97)));
														}
													else
														{	/* Ieee/number.scm 735 */
															if (LLONGP(BgL_yz00_97))
																{	/* Ieee/number.scm 735 */
																	BGL_LONGLONG_T BgL_auxz00_8205;
																	BGL_LONGLONG_T BgL_tmpz00_8203;

																	BgL_auxz00_8205 =
																		BLLONG_TO_LLONG(BgL_yz00_97);
																	BgL_tmpz00_8203 =
																		BLLONG_TO_LLONG(BgL_xz00_96);
																	return
																		BGL_SAFE_MUL_LLONG(BgL_tmpz00_8203,
																		BgL_auxz00_8205);
																}
															else
																{	/* Ieee/number.scm 735 */
																	if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00
																		(BgL_yz00_97))
																		{	/* Ieee/number.scm 735 */
																			BGL_LONGLONG_T BgL_arg2231z00_1880;

																			{	/* Ieee/number.scm 735 */
																				obj_t BgL_arg2232z00_1881;

																				BgL_arg2232z00_1881 =
																					BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																					(BgL_yz00_97);
																				BgL_arg2231z00_1880 =
																					(BGL_LONGLONG_T) (BELONG_TO_LONG
																					(BgL_arg2232z00_1881));
																			}
																			{	/* Ieee/number.scm 735 */
																				BGL_LONGLONG_T BgL_tmpz00_8213;

																				BgL_tmpz00_8213 =
																					BLLONG_TO_LLONG(BgL_xz00_96);
																				return
																					BGL_SAFE_MUL_LLONG(BgL_tmpz00_8213,
																					BgL_arg2231z00_1880);
																			}
																		}
																	else
																		{	/* Ieee/number.scm 735 */
																			if (BIGNUMP(BgL_yz00_97))
																				{	/* Ieee/number.scm 735 */
																					return
																						bgl_bignum_mul(bgl_llong_to_bignum
																						(BLLONG_TO_LLONG(BgL_xz00_96)),
																						BgL_yz00_97);
																				}
																			else
																				{	/* Ieee/number.scm 735 */
																					if (BGL_UINT64P(BgL_yz00_97))
																						{	/* Ieee/number.scm 735 */
																							uint64_t BgL_arg2237z00_1886;

																							{	/* Ieee/number.scm 735 */
																								BGL_LONGLONG_T BgL_tmpz00_8223;

																								BgL_tmpz00_8223 =
																									BLLONG_TO_LLONG(BgL_xz00_96);
																								BgL_arg2237z00_1886 =
																									(uint64_t) (BgL_tmpz00_8223);
																							}
																							{	/* Ieee/number.scm 735 */
																								uint64_t BgL_za72za7_4132;

																								BgL_za72za7_4132 =
																									BGL_BINT64_TO_INT64
																									(BgL_yz00_97);
																								{	/* Ieee/number.scm 735 */
																									uint64_t BgL_tmpz00_8227;

																									BgL_tmpz00_8227 =
																										(BgL_arg2237z00_1886 *
																										BgL_za72za7_4132);
																									return
																										BGL_UINT64_TO_BUINT64
																										(BgL_tmpz00_8227);
																								}
																							}
																						}
																					else
																						{	/* Ieee/number.scm 735 */
																							return
																								BGl_errorz00zz__errorz00
																								(BGl_string3340z00zz__r4_numbers_6_5z00,
																								BGl_string3328z00zz__r4_numbers_6_5z00,
																								BgL_yz00_97);
																						}
																				}
																		}
																}
														}
												}
										}
									else
										{	/* Ieee/number.scm 735 */
											if (BGL_UINT64P(BgL_xz00_96))
												{	/* Ieee/number.scm 735 */
													if (INTEGERP(BgL_yz00_97))
														{	/* Ieee/number.scm 735 */
															uint64_t BgL_arg2240z00_1889;

															{	/* Ieee/number.scm 735 */
																long BgL_tmpz00_8235;

																BgL_tmpz00_8235 = (long) CINT(BgL_yz00_97);
																BgL_arg2240z00_1889 =
																	(uint64_t) (BgL_tmpz00_8235);
															}
															{	/* Ieee/number.scm 735 */
																uint64_t BgL_za71za7_4134;

																BgL_za71za7_4134 =
																	BGL_BINT64_TO_INT64(BgL_xz00_96);
																{	/* Ieee/number.scm 735 */
																	uint64_t BgL_tmpz00_8239;

																	BgL_tmpz00_8239 =
																		(BgL_za71za7_4134 * BgL_arg2240z00_1889);
																	return BGL_UINT64_TO_BUINT64(BgL_tmpz00_8239);
																}
															}
														}
													else
														{	/* Ieee/number.scm 735 */
															if (BGL_UINT64P(BgL_yz00_97))
																{	/* Ieee/number.scm 735 */
																	uint64_t BgL_za71za7_4136;
																	uint64_t BgL_za72za7_4137;

																	BgL_za71za7_4136 =
																		BGL_BINT64_TO_INT64(BgL_xz00_96);
																	BgL_za72za7_4137 =
																		BGL_BINT64_TO_INT64(BgL_yz00_97);
																	{	/* Ieee/number.scm 735 */
																		uint64_t BgL_tmpz00_8246;

																		BgL_tmpz00_8246 =
																			(BgL_za71za7_4136 * BgL_za72za7_4137);
																		return
																			BGL_UINT64_TO_BUINT64(BgL_tmpz00_8246);
																	}
																}
															else
																{	/* Ieee/number.scm 735 */
																	if (REALP(BgL_yz00_97))
																		{	/* Ieee/number.scm 735 */
																			double BgL_arg2243z00_1892;

																			{	/* Ieee/number.scm 735 */
																				uint64_t BgL_tmpz00_8251;

																				BgL_tmpz00_8251 =
																					BGL_BINT64_TO_INT64(BgL_xz00_96);
																				BgL_arg2243z00_1892 =
																					(double) (BgL_tmpz00_8251);
																			}
																			return
																				DOUBLE_TO_REAL(
																				(BgL_arg2243z00_1892 *
																					REAL_TO_DOUBLE(BgL_yz00_97)));
																		}
																	else
																		{	/* Ieee/number.scm 735 */
																			if (LLONGP(BgL_yz00_97))
																				{	/* Ieee/number.scm 735 */
																					uint64_t BgL_arg2245z00_1894;

																					{	/* Ieee/number.scm 735 */
																						BGL_LONGLONG_T BgL_tmpz00_8259;

																						BgL_tmpz00_8259 =
																							BLLONG_TO_LLONG(BgL_yz00_97);
																						BgL_arg2245z00_1894 =
																							(uint64_t) (BgL_tmpz00_8259);
																					}
																					{	/* Ieee/number.scm 735 */
																						uint64_t BgL_za71za7_4141;

																						BgL_za71za7_4141 =
																							BGL_BINT64_TO_INT64(BgL_xz00_96);
																						{	/* Ieee/number.scm 735 */
																							uint64_t BgL_tmpz00_8263;

																							BgL_tmpz00_8263 =
																								(BgL_za71za7_4141 *
																								BgL_arg2245z00_1894);
																							return
																								BGL_UINT64_TO_BUINT64
																								(BgL_tmpz00_8263);
																						}
																					}
																				}
																			else
																				{	/* Ieee/number.scm 735 */
																					if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_97))
																						{	/* Ieee/number.scm 735 */
																							uint64_t BgL_arg2248z00_1897;

																							{	/* Ieee/number.scm 735 */
																								BGL_LONGLONG_T
																									BgL_arg2249z00_1898;
																								{	/* Ieee/number.scm 735 */
																									obj_t BgL_arg2250z00_1899;

																									BgL_arg2250z00_1899 =
																										BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																										(BgL_yz00_97);
																									BgL_arg2249z00_1898 =
																										(BGL_LONGLONG_T)
																										(BELONG_TO_LONG
																										(BgL_arg2250z00_1899));
																								}
																								BgL_arg2248z00_1897 =
																									(uint64_t)
																									(BgL_arg2249z00_1898);
																							}
																							{	/* Ieee/number.scm 735 */
																								uint64_t BgL_za71za7_4144;

																								BgL_za71za7_4144 =
																									BGL_BINT64_TO_INT64
																									(BgL_xz00_96);
																								{	/* Ieee/number.scm 735 */
																									uint64_t BgL_tmpz00_8273;

																									BgL_tmpz00_8273 =
																										(BgL_za71za7_4144 *
																										BgL_arg2248z00_1897);
																									return
																										BGL_UINT64_TO_BUINT64
																										(BgL_tmpz00_8273);
																								}
																							}
																						}
																					else
																						{	/* Ieee/number.scm 735 */
																							if (BIGNUMP(BgL_yz00_97))
																								{	/* Ieee/number.scm 735 */
																									return
																										bgl_bignum_mul
																										(bgl_uint64_to_bignum
																										(BGL_BINT64_TO_INT64
																											(BgL_xz00_96)),
																										BgL_yz00_97);
																								}
																							else
																								{	/* Ieee/number.scm 735 */
																									return
																										BGl_errorz00zz__errorz00
																										(BGl_string3340z00zz__r4_numbers_6_5z00,
																										BGl_string3329z00zz__r4_numbers_6_5z00,
																										BgL_yz00_97);
																								}
																						}
																				}
																		}
																}
														}
												}
											else
												{	/* Ieee/number.scm 735 */
													if (BIGNUMP(BgL_xz00_96))
														{	/* Ieee/number.scm 735 */
															if (BIGNUMP(BgL_yz00_97))
																{	/* Ieee/number.scm 735 */
																	obj_t BgL_tmpz00_8286;

																	BgL_tmpz00_8286 =
																		bgl_bignum_mul(BgL_xz00_96, BgL_yz00_97);
																	return BGL_SAFE_BX_TO_FX(BgL_tmpz00_8286);
																}
															else
																{	/* Ieee/number.scm 735 */
																	if (INTEGERP(BgL_yz00_97))
																		{	/* Ieee/number.scm 735 */
																			obj_t BgL_tmpz00_8291;

																			BgL_tmpz00_8291 =
																				bgl_bignum_mul(BgL_xz00_96,
																				bgl_long_to_bignum(
																					(long) CINT(BgL_yz00_97)));
																			return BGL_SAFE_BX_TO_FX(BgL_tmpz00_8291);
																		}
																	else
																		{	/* Ieee/number.scm 735 */
																			if (REALP(BgL_yz00_97))
																				{	/* Ieee/number.scm 735 */
																					return
																						DOUBLE_TO_REAL(
																						(bgl_bignum_to_flonum(BgL_xz00_96) *
																							REAL_TO_DOUBLE(BgL_yz00_97)));
																				}
																			else
																				{	/* Ieee/number.scm 735 */
																					if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_97))
																						{	/* Ieee/number.scm 735 */
																							obj_t BgL_arg2262z00_1911;

																							{	/* Ieee/number.scm 735 */
																								obj_t BgL_arg2263z00_1912;

																								BgL_arg2263z00_1912 =
																									BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																									(BgL_yz00_97);
																								{	/* Ieee/number.scm 735 */
																									long BgL_xz00_4156;

																									BgL_xz00_4156 =
																										BELONG_TO_LONG
																										(BgL_arg2263z00_1912);
																									BgL_arg2262z00_1911 =
																										bgl_long_to_bignum
																										(BgL_xz00_4156);
																							}}
																							return
																								bgl_bignum_mul(BgL_xz00_96,
																								BgL_arg2262z00_1911);
																						}
																					else
																						{	/* Ieee/number.scm 735 */
																							if (LLONGP(BgL_yz00_97))
																								{	/* Ieee/number.scm 735 */
																									return
																										bgl_bignum_mul(BgL_xz00_96,
																										bgl_llong_to_bignum
																										(BLLONG_TO_LLONG
																											(BgL_yz00_97)));
																								}
																							else
																								{	/* Ieee/number.scm 735 */
																									if (BGL_UINT64P(BgL_yz00_97))
																										{	/* Ieee/number.scm 735 */
																											return
																												bgl_bignum_mul
																												(BgL_xz00_96,
																												bgl_uint64_to_bignum
																												(BGL_BINT64_TO_INT64
																													(BgL_yz00_97)));
																										}
																									else
																										{	/* Ieee/number.scm 735 */
																											return
																												BGl_errorz00zz__errorz00
																												(BGl_string3340z00zz__r4_numbers_6_5z00,
																												BGl_string3328z00zz__r4_numbers_6_5z00,
																												BgL_yz00_97);
																										}
																								}
																						}
																				}
																		}
																}
														}
													else
														{	/* Ieee/number.scm 735 */
															return
																BGl_errorz00zz__errorz00
																(BGl_string3340z00zz__r4_numbers_6_5z00,
																BGl_string3328z00zz__r4_numbers_6_5z00,
																BgL_xz00_96);
														}
												}
										}
								}
						}
				}
		}

	}



/* &2* */
	obj_t BGl_z622za2zc0zz__r4_numbers_6_5z00(obj_t BgL_envz00_5064,
		obj_t BgL_xz00_5065, obj_t BgL_yz00_5066)
	{
		{	/* Ieee/number.scm 734 */
			return BGl_2za2za2zz__r4_numbers_6_5z00(BgL_xz00_5065, BgL_yz00_5066);
		}

	}



/* * */
	BGL_EXPORTED_DEF obj_t BGl_za2za2zz__r4_numbers_6_5z00(obj_t BgL_xz00_98)
	{
		{	/* Ieee/number.scm 740 */
			{
				obj_t BgL_productz00_4177;
				obj_t BgL_xz00_4178;

				BgL_productz00_4177 = BINT(1L);
				BgL_xz00_4178 = BgL_xz00_98;
			BgL_loopz00_4176:
				if (PAIRP(BgL_xz00_4178))
					{
						obj_t BgL_xz00_8326;
						obj_t BgL_productz00_8323;

						BgL_productz00_8323 =
							BGl_2za2za2zz__r4_numbers_6_5z00(BgL_productz00_4177,
							CAR(BgL_xz00_4178));
						BgL_xz00_8326 = CDR(BgL_xz00_4178);
						BgL_xz00_4178 = BgL_xz00_8326;
						BgL_productz00_4177 = BgL_productz00_8323;
						goto BgL_loopz00_4176;
					}
				else
					{	/* Ieee/number.scm 743 */
						return BgL_productz00_4177;
					}
			}
		}

	}



/* &* */
	obj_t BGl_z62za2zc0zz__r4_numbers_6_5z00(obj_t BgL_envz00_5067,
		obj_t BgL_xz00_5068)
	{
		{	/* Ieee/number.scm 740 */
			return BGl_za2za2zz__r4_numbers_6_5z00(BgL_xz00_5068);
		}

	}



/* 2- */
	BGL_EXPORTED_DEF obj_t BGl_2zd2zd2zz__r4_numbers_6_5z00(obj_t BgL_xz00_99,
		obj_t BgL_yz00_100)
	{
		{	/* Ieee/number.scm 750 */
			if (INTEGERP(BgL_xz00_99))
				{	/* Ieee/number.scm 751 */
					if (INTEGERP(BgL_yz00_100))
						{	/* Ieee/number.scm 751 */
							long BgL_auxz00_8336;
							long BgL_tmpz00_8334;

							BgL_auxz00_8336 = (long) CINT(BgL_yz00_100);
							BgL_tmpz00_8334 = (long) CINT(BgL_xz00_99);
							return BGL_SAFE_MINUS_FX(BgL_tmpz00_8334, BgL_auxz00_8336);
						}
					else
						{	/* Ieee/number.scm 751 */
							if (REALP(BgL_yz00_100))
								{	/* Ieee/number.scm 751 */
									return
										DOUBLE_TO_REAL(
										((double) (
												(long) CINT(BgL_xz00_99)) -
											REAL_TO_DOUBLE(BgL_yz00_100)));
								}
							else
								{	/* Ieee/number.scm 751 */
									if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_100))
										{	/* Ieee/number.scm 751 */
											long BgL_arg2279z00_1932;
											obj_t BgL_arg2280z00_1933;

											{	/* Ieee/number.scm 751 */
												long BgL_tmpz00_8348;

												BgL_tmpz00_8348 = (long) CINT(BgL_xz00_99);
												BgL_arg2279z00_1932 = (long) (BgL_tmpz00_8348);
											}
											BgL_arg2280z00_1933 =
												BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
												(BgL_yz00_100);
											{	/* Ieee/number.scm 751 */
												long BgL_tmpz00_8352;

												BgL_tmpz00_8352 = BELONG_TO_LONG(BgL_arg2280z00_1933);
												return
													BGL_SAFE_MINUS_ELONG(BgL_arg2279z00_1932,
													BgL_tmpz00_8352);
											}
										}
									else
										{	/* Ieee/number.scm 751 */
											if (LLONGP(BgL_yz00_100))
												{	/* Ieee/number.scm 751 */
													BGL_LONGLONG_T BgL_arg2282z00_1935;

													{	/* Ieee/number.scm 751 */
														long BgL_tmpz00_8357;

														BgL_tmpz00_8357 = (long) CINT(BgL_xz00_99);
														BgL_arg2282z00_1935 =
															LONG_TO_LLONG(BgL_tmpz00_8357);
													}
													{	/* Ieee/number.scm 751 */
														BGL_LONGLONG_T BgL_tmpz00_8360;

														BgL_tmpz00_8360 = BLLONG_TO_LLONG(BgL_yz00_100);
														return
															BGL_SAFE_MINUS_LLONG(BgL_arg2282z00_1935,
															BgL_tmpz00_8360);
													}
												}
											else
												{	/* Ieee/number.scm 751 */
													if (BGL_UINT64P(BgL_yz00_100))
														{	/* Ieee/number.scm 751 */
															uint64_t BgL_arg2286z00_1938;

															{	/* Ieee/number.scm 751 */
																BGL_LONGLONG_T BgL_arg2287z00_1939;

																{	/* Ieee/number.scm 751 */
																	long BgL_tmpz00_8365;

																	BgL_tmpz00_8365 = (long) CINT(BgL_xz00_99);
																	BgL_arg2287z00_1939 =
																		LONG_TO_LLONG(BgL_tmpz00_8365);
																}
																BgL_arg2286z00_1938 =
																	(uint64_t) (BgL_arg2287z00_1939);
															}
															{	/* Ieee/number.scm 751 */
																uint64_t BgL_za72za7_4196;

																BgL_za72za7_4196 =
																	BGL_BINT64_TO_INT64(BgL_yz00_100);
																{	/* Ieee/number.scm 751 */
																	uint64_t BgL_tmpz00_8370;

																	BgL_tmpz00_8370 =
																		(BgL_arg2286z00_1938 - BgL_za72za7_4196);
																	return BGL_UINT64_TO_BUINT64(BgL_tmpz00_8370);
																}
															}
														}
													else
														{	/* Ieee/number.scm 751 */
															if (BIGNUMP(BgL_yz00_100))
																{	/* Ieee/number.scm 751 */
																	obj_t BgL_tmpz00_8375;

																	BgL_tmpz00_8375 =
																		bgl_bignum_sub(bgl_long_to_bignum(
																			(long) CINT(BgL_xz00_99)), BgL_yz00_100);
																	return BGL_SAFE_BX_TO_FX(BgL_tmpz00_8375);
																}
															else
																{	/* Ieee/number.scm 751 */
																	return
																		BGl_errorz00zz__errorz00
																		(BGl_string3341z00zz__r4_numbers_6_5z00,
																		BGl_string3328z00zz__r4_numbers_6_5z00,
																		BgL_yz00_100);
																}
														}
												}
										}
								}
						}
				}
			else
				{	/* Ieee/number.scm 751 */
					if (REALP(BgL_xz00_99))
						{	/* Ieee/number.scm 751 */
							if (REALP(BgL_yz00_100))
								{	/* Ieee/number.scm 751 */
									return
										DOUBLE_TO_REAL(
										(REAL_TO_DOUBLE(BgL_xz00_99) -
											REAL_TO_DOUBLE(BgL_yz00_100)));
								}
							else
								{	/* Ieee/number.scm 751 */
									if (INTEGERP(BgL_yz00_100))
										{	/* Ieee/number.scm 751 */
											return
												DOUBLE_TO_REAL(
												(REAL_TO_DOUBLE(BgL_xz00_99) -
													(double) ((long) CINT(BgL_yz00_100))));
										}
									else
										{	/* Ieee/number.scm 751 */
											if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00
												(BgL_yz00_100))
												{	/* Ieee/number.scm 751 */
													double BgL_arg2296z00_1948;

													{	/* Ieee/number.scm 751 */
														obj_t BgL_arg2297z00_1949;

														BgL_arg2297z00_1949 =
															BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
															(BgL_yz00_100);
														BgL_arg2296z00_1948 =
															(double) (BELONG_TO_LONG(BgL_arg2297z00_1949));
													}
													return
														DOUBLE_TO_REAL(
														(REAL_TO_DOUBLE(BgL_xz00_99) -
															BgL_arg2296z00_1948));
												}
											else
												{	/* Ieee/number.scm 751 */
													if (LLONGP(BgL_yz00_100))
														{	/* Ieee/number.scm 751 */
															return
																DOUBLE_TO_REAL(
																(REAL_TO_DOUBLE(BgL_xz00_99) -
																	(double) (BLLONG_TO_LLONG(BgL_yz00_100))));
														}
													else
														{	/* Ieee/number.scm 751 */
															if (BGL_UINT64P(BgL_yz00_100))
																{	/* Ieee/number.scm 751 */
																	double BgL_arg2304z00_1954;

																	{	/* Ieee/number.scm 751 */
																		uint64_t BgL_xz00_4208;

																		BgL_xz00_4208 =
																			BGL_BINT64_TO_INT64(BgL_yz00_100);
																		BgL_arg2304z00_1954 =
																			(double) (BgL_xz00_4208);
																	}
																	return
																		DOUBLE_TO_REAL(
																		(REAL_TO_DOUBLE(BgL_xz00_99) -
																			BgL_arg2304z00_1954));
																}
															else
																{	/* Ieee/number.scm 751 */
																	if (BIGNUMP(BgL_yz00_100))
																		{	/* Ieee/number.scm 751 */
																			return
																				DOUBLE_TO_REAL(
																				(REAL_TO_DOUBLE(BgL_xz00_99) -
																					bgl_bignum_to_flonum(BgL_yz00_100)));
																		}
																	else
																		{	/* Ieee/number.scm 751 */
																			return
																				BGl_errorz00zz__errorz00
																				(BGl_string3341z00zz__r4_numbers_6_5z00,
																				BGl_string3328z00zz__r4_numbers_6_5z00,
																				BgL_yz00_100);
																		}
																}
														}
												}
										}
								}
						}
					else
						{	/* Ieee/number.scm 751 */
							if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_xz00_99))
								{	/* Ieee/number.scm 751 */
									if (INTEGERP(BgL_yz00_100))
										{	/* Ieee/number.scm 751 */
											obj_t BgL_arg2309z00_1959;
											long BgL_arg2310z00_1960;

											BgL_arg2309z00_1959 =
												BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
												(BgL_xz00_99);
											{	/* Ieee/number.scm 751 */
												long BgL_tmpz00_8430;

												BgL_tmpz00_8430 = (long) CINT(BgL_yz00_100);
												BgL_arg2310z00_1960 = (long) (BgL_tmpz00_8430);
											}
											{	/* Ieee/number.scm 751 */
												long BgL_tmpz00_8433;

												BgL_tmpz00_8433 = BELONG_TO_LONG(BgL_arg2309z00_1959);
												return
													BGL_SAFE_MINUS_ELONG(BgL_tmpz00_8433,
													BgL_arg2310z00_1960);
											}
										}
									else
										{	/* Ieee/number.scm 751 */
											if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00
												(BgL_yz00_100))
												{	/* Ieee/number.scm 751 */
													obj_t BgL_arg2312z00_1962;
													obj_t BgL_arg2313z00_1963;

													BgL_arg2312z00_1962 =
														BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
														(BgL_xz00_99);
													BgL_arg2313z00_1963 =
														BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
														(BgL_yz00_100);
													{	/* Ieee/number.scm 751 */
														long BgL_auxz00_8442;
														long BgL_tmpz00_8440;

														BgL_auxz00_8442 =
															BELONG_TO_LONG(BgL_arg2313z00_1963);
														BgL_tmpz00_8440 =
															BELONG_TO_LONG(BgL_arg2312z00_1962);
														return
															BGL_SAFE_MINUS_ELONG(BgL_tmpz00_8440,
															BgL_auxz00_8442);
													}
												}
											else
												{	/* Ieee/number.scm 751 */
													if (REALP(BgL_yz00_100))
														{	/* Ieee/number.scm 751 */
															double BgL_arg2315z00_1965;

															{	/* Ieee/number.scm 751 */
																obj_t BgL_arg2316z00_1966;

																BgL_arg2316z00_1966 =
																	BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																	(BgL_xz00_99);
																BgL_arg2315z00_1965 =
																	(double) (BELONG_TO_LONG
																	(BgL_arg2316z00_1966));
															}
															return
																DOUBLE_TO_REAL(
																(BgL_arg2315z00_1965 -
																	REAL_TO_DOUBLE(BgL_yz00_100)));
														}
													else
														{	/* Ieee/number.scm 751 */
															if (LLONGP(BgL_yz00_100))
																{	/* Ieee/number.scm 751 */
																	BGL_LONGLONG_T BgL_arg2318z00_1968;

																	{	/* Ieee/number.scm 751 */
																		obj_t BgL_arg2320z00_1970;

																		BgL_arg2320z00_1970 =
																			BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																			(BgL_xz00_99);
																		BgL_arg2318z00_1968 =
																			(BGL_LONGLONG_T) (BELONG_TO_LONG
																			(BgL_arg2320z00_1970));
																	}
																	{	/* Ieee/number.scm 751 */
																		BGL_LONGLONG_T BgL_tmpz00_8458;

																		BgL_tmpz00_8458 =
																			BLLONG_TO_LLONG(BgL_yz00_100);
																		return
																			BGL_SAFE_MINUS_LLONG(BgL_arg2318z00_1968,
																			BgL_tmpz00_8458);
																	}
																}
															else
																{	/* Ieee/number.scm 751 */
																	if (BGL_UINT64P(BgL_yz00_100))
																		{	/* Ieee/number.scm 751 */
																			uint64_t BgL_arg2323z00_1972;

																			{	/* Ieee/number.scm 751 */
																				BGL_LONGLONG_T BgL_arg2324z00_1973;

																				{	/* Ieee/number.scm 751 */
																					obj_t BgL_arg2325z00_1974;

																					BgL_arg2325z00_1974 =
																						BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																						(BgL_xz00_99);
																					BgL_arg2324z00_1973 =
																						(BGL_LONGLONG_T) (BELONG_TO_LONG
																						(BgL_arg2325z00_1974));
																				}
																				BgL_arg2323z00_1972 =
																					(uint64_t) (BgL_arg2324z00_1973);
																			}
																			{	/* Ieee/number.scm 751 */
																				uint64_t BgL_za72za7_4219;

																				BgL_za72za7_4219 =
																					BGL_BINT64_TO_INT64(BgL_yz00_100);
																				{	/* Ieee/number.scm 751 */
																					uint64_t BgL_tmpz00_8468;

																					BgL_tmpz00_8468 =
																						(BgL_arg2323z00_1972 -
																						BgL_za72za7_4219);
																					return
																						BGL_UINT64_TO_BUINT64
																						(BgL_tmpz00_8468);
																				}
																			}
																		}
																	else
																		{	/* Ieee/number.scm 751 */
																			if (BIGNUMP(BgL_yz00_100))
																				{	/* Ieee/number.scm 751 */
																					obj_t BgL_arg2327z00_1976;

																					{	/* Ieee/number.scm 751 */
																						obj_t BgL_arg2328z00_1977;

																						BgL_arg2328z00_1977 =
																							BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																							(BgL_xz00_99);
																						{	/* Ieee/number.scm 751 */
																							long BgL_xz00_4220;

																							BgL_xz00_4220 =
																								BELONG_TO_LONG
																								(BgL_arg2328z00_1977);
																							BgL_arg2327z00_1976 =
																								bgl_long_to_bignum
																								(BgL_xz00_4220);
																					}}
																					return
																						bgl_bignum_sub(BgL_arg2327z00_1976,
																						BgL_yz00_100);
																				}
																			else
																				{	/* Ieee/number.scm 751 */
																					return
																						BGl_errorz00zz__errorz00
																						(BGl_string3341z00zz__r4_numbers_6_5z00,
																						BGl_string3328z00zz__r4_numbers_6_5z00,
																						BgL_yz00_100);
																				}
																		}
																}
														}
												}
										}
								}
							else
								{	/* Ieee/number.scm 751 */
									if (LLONGP(BgL_xz00_99))
										{	/* Ieee/number.scm 751 */
											if (INTEGERP(BgL_yz00_100))
												{	/* Ieee/number.scm 751 */
													BGL_LONGLONG_T BgL_arg2333z00_1981;

													{	/* Ieee/number.scm 751 */
														long BgL_tmpz00_8482;

														BgL_tmpz00_8482 = (long) CINT(BgL_yz00_100);
														BgL_arg2333z00_1981 =
															LONG_TO_LLONG(BgL_tmpz00_8482);
													}
													{	/* Ieee/number.scm 751 */
														BGL_LONGLONG_T BgL_tmpz00_8485;

														BgL_tmpz00_8485 = BLLONG_TO_LLONG(BgL_xz00_99);
														return
															BGL_SAFE_MINUS_LLONG(BgL_tmpz00_8485,
															BgL_arg2333z00_1981);
													}
												}
											else
												{	/* Ieee/number.scm 751 */
													if (REALP(BgL_yz00_100))
														{	/* Ieee/number.scm 751 */
															return
																DOUBLE_TO_REAL(
																((double) (BLLONG_TO_LLONG(BgL_xz00_99)) -
																	REAL_TO_DOUBLE(BgL_yz00_100)));
														}
													else
														{	/* Ieee/number.scm 751 */
															if (LLONGP(BgL_yz00_100))
																{	/* Ieee/number.scm 751 */
																	BGL_LONGLONG_T BgL_auxz00_8499;
																	BGL_LONGLONG_T BgL_tmpz00_8497;

																	BgL_auxz00_8499 =
																		BLLONG_TO_LLONG(BgL_yz00_100);
																	BgL_tmpz00_8497 =
																		BLLONG_TO_LLONG(BgL_xz00_99);
																	return
																		BGL_SAFE_MINUS_LLONG(BgL_tmpz00_8497,
																		BgL_auxz00_8499);
																}
															else
																{	/* Ieee/number.scm 751 */
																	if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00
																		(BgL_yz00_100))
																		{	/* Ieee/number.scm 751 */
																			BGL_LONGLONG_T BgL_arg2341z00_1989;

																			{	/* Ieee/number.scm 751 */
																				obj_t BgL_arg2342z00_1990;

																				BgL_arg2342z00_1990 =
																					BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																					(BgL_yz00_100);
																				BgL_arg2341z00_1989 =
																					(BGL_LONGLONG_T) (BELONG_TO_LONG
																					(BgL_arg2342z00_1990));
																			}
																			{	/* Ieee/number.scm 751 */
																				BGL_LONGLONG_T BgL_tmpz00_8507;

																				BgL_tmpz00_8507 =
																					BLLONG_TO_LLONG(BgL_xz00_99);
																				return
																					BGL_SAFE_MINUS_LLONG(BgL_tmpz00_8507,
																					BgL_arg2341z00_1989);
																			}
																		}
																	else
																		{	/* Ieee/number.scm 751 */
																			if (BIGNUMP(BgL_yz00_100))
																				{	/* Ieee/number.scm 751 */
																					return
																						bgl_bignum_sub(bgl_llong_to_bignum
																						(BLLONG_TO_LLONG(BgL_xz00_99)),
																						BgL_yz00_100);
																				}
																			else
																				{	/* Ieee/number.scm 751 */
																					if (BGL_UINT64P(BgL_yz00_100))
																						{	/* Ieee/number.scm 751 */
																							uint64_t BgL_arg2348z00_1995;

																							{	/* Ieee/number.scm 751 */
																								BGL_LONGLONG_T BgL_tmpz00_8517;

																								BgL_tmpz00_8517 =
																									BLLONG_TO_LLONG(BgL_xz00_99);
																								BgL_arg2348z00_1995 =
																									(uint64_t) (BgL_tmpz00_8517);
																							}
																							{	/* Ieee/number.scm 751 */
																								uint64_t BgL_za72za7_4231;

																								BgL_za72za7_4231 =
																									BGL_BINT64_TO_INT64
																									(BgL_yz00_100);
																								{	/* Ieee/number.scm 751 */
																									uint64_t BgL_tmpz00_8521;

																									BgL_tmpz00_8521 =
																										(BgL_arg2348z00_1995 -
																										BgL_za72za7_4231);
																									return
																										BGL_UINT64_TO_BUINT64
																										(BgL_tmpz00_8521);
																								}
																							}
																						}
																					else
																						{	/* Ieee/number.scm 751 */
																							return
																								BGl_errorz00zz__errorz00
																								(BGl_string3341z00zz__r4_numbers_6_5z00,
																								BGl_string3328z00zz__r4_numbers_6_5z00,
																								BgL_yz00_100);
																						}
																				}
																		}
																}
														}
												}
										}
									else
										{	/* Ieee/number.scm 751 */
											if (BGL_UINT64P(BgL_xz00_99))
												{	/* Ieee/number.scm 751 */
													if (INTEGERP(BgL_yz00_100))
														{	/* Ieee/number.scm 751 */
															uint64_t BgL_arg2351z00_1998;

															{	/* Ieee/number.scm 751 */
																long BgL_tmpz00_8529;

																BgL_tmpz00_8529 = (long) CINT(BgL_yz00_100);
																BgL_arg2351z00_1998 =
																	(uint64_t) (BgL_tmpz00_8529);
															}
															{	/* Ieee/number.scm 751 */
																uint64_t BgL_za71za7_4233;

																BgL_za71za7_4233 =
																	BGL_BINT64_TO_INT64(BgL_xz00_99);
																{	/* Ieee/number.scm 751 */
																	uint64_t BgL_tmpz00_8533;

																	BgL_tmpz00_8533 =
																		(BgL_za71za7_4233 - BgL_arg2351z00_1998);
																	return BGL_UINT64_TO_BUINT64(BgL_tmpz00_8533);
																}
															}
														}
													else
														{	/* Ieee/number.scm 751 */
															if (BGL_UINT64P(BgL_yz00_100))
																{	/* Ieee/number.scm 751 */
																	uint64_t BgL_za71za7_4235;
																	uint64_t BgL_za72za7_4236;

																	BgL_za71za7_4235 =
																		BGL_BINT64_TO_INT64(BgL_xz00_99);
																	BgL_za72za7_4236 =
																		BGL_BINT64_TO_INT64(BgL_yz00_100);
																	{	/* Ieee/number.scm 751 */
																		uint64_t BgL_tmpz00_8540;

																		BgL_tmpz00_8540 =
																			(BgL_za71za7_4235 - BgL_za72za7_4236);
																		return
																			BGL_UINT64_TO_BUINT64(BgL_tmpz00_8540);
																	}
																}
															else
																{	/* Ieee/number.scm 751 */
																	if (REALP(BgL_yz00_100))
																		{	/* Ieee/number.scm 751 */
																			double BgL_arg2354z00_2001;

																			{	/* Ieee/number.scm 751 */
																				uint64_t BgL_tmpz00_8545;

																				BgL_tmpz00_8545 =
																					BGL_BINT64_TO_INT64(BgL_xz00_99);
																				BgL_arg2354z00_2001 =
																					(double) (BgL_tmpz00_8545);
																			}
																			return
																				DOUBLE_TO_REAL(
																				(BgL_arg2354z00_2001 -
																					REAL_TO_DOUBLE(BgL_yz00_100)));
																		}
																	else
																		{	/* Ieee/number.scm 751 */
																			if (LLONGP(BgL_yz00_100))
																				{	/* Ieee/number.scm 751 */
																					uint64_t BgL_arg2356z00_2003;

																					{	/* Ieee/number.scm 751 */
																						BGL_LONGLONG_T BgL_tmpz00_8553;

																						BgL_tmpz00_8553 =
																							BLLONG_TO_LLONG(BgL_yz00_100);
																						BgL_arg2356z00_2003 =
																							(uint64_t) (BgL_tmpz00_8553);
																					}
																					{	/* Ieee/number.scm 751 */
																						uint64_t BgL_za71za7_4240;

																						BgL_za71za7_4240 =
																							BGL_BINT64_TO_INT64(BgL_xz00_99);
																						{	/* Ieee/number.scm 751 */
																							uint64_t BgL_tmpz00_8557;

																							BgL_tmpz00_8557 =
																								(BgL_za71za7_4240 -
																								BgL_arg2356z00_2003);
																							return
																								BGL_UINT64_TO_BUINT64
																								(BgL_tmpz00_8557);
																						}
																					}
																				}
																			else
																				{	/* Ieee/number.scm 751 */
																					if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_100))
																						{	/* Ieee/number.scm 751 */
																							uint64_t BgL_arg2361z00_2006;

																							{	/* Ieee/number.scm 751 */
																								BGL_LONGLONG_T
																									BgL_arg2363z00_2007;
																								{	/* Ieee/number.scm 751 */
																									obj_t BgL_arg2364z00_2008;

																									BgL_arg2364z00_2008 =
																										BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																										(BgL_yz00_100);
																									BgL_arg2363z00_2007 =
																										(BGL_LONGLONG_T)
																										(BELONG_TO_LONG
																										(BgL_arg2364z00_2008));
																								}
																								BgL_arg2361z00_2006 =
																									(uint64_t)
																									(BgL_arg2363z00_2007);
																							}
																							{	/* Ieee/number.scm 751 */
																								uint64_t BgL_za71za7_4243;

																								BgL_za71za7_4243 =
																									BGL_BINT64_TO_INT64
																									(BgL_xz00_99);
																								{	/* Ieee/number.scm 751 */
																									uint64_t BgL_tmpz00_8567;

																									BgL_tmpz00_8567 =
																										(BgL_za71za7_4243 -
																										BgL_arg2361z00_2006);
																									return
																										BGL_UINT64_TO_BUINT64
																										(BgL_tmpz00_8567);
																								}
																							}
																						}
																					else
																						{	/* Ieee/number.scm 751 */
																							if (BIGNUMP(BgL_yz00_100))
																								{	/* Ieee/number.scm 751 */
																									return
																										bgl_bignum_sub
																										(bgl_uint64_to_bignum
																										(BGL_BINT64_TO_INT64
																											(BgL_xz00_99)),
																										BgL_yz00_100);
																								}
																							else
																								{	/* Ieee/number.scm 751 */
																									return
																										BGl_errorz00zz__errorz00
																										(BGl_string3341z00zz__r4_numbers_6_5z00,
																										BGl_string3329z00zz__r4_numbers_6_5z00,
																										BgL_yz00_100);
																								}
																						}
																				}
																		}
																}
														}
												}
											else
												{	/* Ieee/number.scm 751 */
													if (BIGNUMP(BgL_xz00_99))
														{	/* Ieee/number.scm 751 */
															if (BIGNUMP(BgL_yz00_100))
																{	/* Ieee/number.scm 751 */
																	obj_t BgL_tmpz00_8580;

																	BgL_tmpz00_8580 =
																		bgl_bignum_sub(BgL_xz00_99, BgL_yz00_100);
																	return BGL_SAFE_BX_TO_FX(BgL_tmpz00_8580);
																}
															else
																{	/* Ieee/number.scm 751 */
																	if (INTEGERP(BgL_yz00_100))
																		{	/* Ieee/number.scm 751 */
																			obj_t BgL_tmpz00_8585;

																			BgL_tmpz00_8585 =
																				bgl_bignum_sub(BgL_xz00_99,
																				bgl_long_to_bignum(
																					(long) CINT(BgL_yz00_100)));
																			return BGL_SAFE_BX_TO_FX(BgL_tmpz00_8585);
																		}
																	else
																		{	/* Ieee/number.scm 751 */
																			if (REALP(BgL_yz00_100))
																				{	/* Ieee/number.scm 751 */
																					return
																						DOUBLE_TO_REAL(
																						(bgl_bignum_to_flonum(BgL_xz00_99) -
																							REAL_TO_DOUBLE(BgL_yz00_100)));
																				}
																			else
																				{	/* Ieee/number.scm 751 */
																					if (BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_100))
																						{	/* Ieee/number.scm 751 */
																							obj_t BgL_arg2377z00_2020;

																							{	/* Ieee/number.scm 751 */
																								obj_t BgL_arg2378z00_2021;

																								BgL_arg2378z00_2021 =
																									BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00
																									(BgL_yz00_100);
																								{	/* Ieee/number.scm 751 */
																									long BgL_xz00_4255;

																									BgL_xz00_4255 =
																										BELONG_TO_LONG
																										(BgL_arg2378z00_2021);
																									BgL_arg2377z00_2020 =
																										bgl_long_to_bignum
																										(BgL_xz00_4255);
																							}}
																							return
																								bgl_bignum_sub(BgL_xz00_99,
																								BgL_arg2377z00_2020);
																						}
																					else
																						{	/* Ieee/number.scm 751 */
																							if (LLONGP(BgL_yz00_100))
																								{	/* Ieee/number.scm 751 */
																									return
																										bgl_bignum_sub(BgL_xz00_99,
																										bgl_llong_to_bignum
																										(BLLONG_TO_LLONG
																											(BgL_yz00_100)));
																								}
																							else
																								{	/* Ieee/number.scm 751 */
																									if (BGL_UINT64P(BgL_yz00_100))
																										{	/* Ieee/number.scm 751 */
																											return
																												bgl_bignum_sub
																												(BgL_xz00_99,
																												bgl_uint64_to_bignum
																												(BGL_BINT64_TO_INT64
																													(BgL_yz00_100)));
																										}
																									else
																										{	/* Ieee/number.scm 751 */
																											return
																												BGl_errorz00zz__errorz00
																												(BGl_string3341z00zz__r4_numbers_6_5z00,
																												BGl_string3328z00zz__r4_numbers_6_5z00,
																												BgL_yz00_100);
																										}
																								}
																						}
																				}
																		}
																}
														}
													else
														{	/* Ieee/number.scm 751 */
															return
																BGl_errorz00zz__errorz00
																(BGl_string3341z00zz__r4_numbers_6_5z00,
																BGl_string3328z00zz__r4_numbers_6_5z00,
																BgL_xz00_99);
														}
												}
										}
								}
						}
				}
		}

	}



/* &2- */
	obj_t BGl_z622zd2zb0zz__r4_numbers_6_5z00(obj_t BgL_envz00_5069,
		obj_t BgL_xz00_5070, obj_t BgL_yz00_5071)
	{
		{	/* Ieee/number.scm 750 */
			return BGl_2zd2zd2zz__r4_numbers_6_5z00(BgL_xz00_5070, BgL_yz00_5071);
		}

	}



/* - */
	BGL_EXPORTED_DEF obj_t BGl_zd2zd2zz__r4_numbers_6_5z00(obj_t BgL_xz00_101,
		obj_t BgL_yz00_102)
	{
		{	/* Ieee/number.scm 756 */
			if (PAIRP(BgL_yz00_102))
				{	/* Ieee/number.scm 758 */
					obj_t BgL_g1052z00_2028;
					obj_t BgL_g1053z00_2029;

					BgL_g1052z00_2028 =
						BGl_2zd2zd2zz__r4_numbers_6_5z00(BgL_xz00_101, CAR(BgL_yz00_102));
					BgL_g1053z00_2029 = CDR(BgL_yz00_102);
					{
						obj_t BgL_resultz00_4278;
						obj_t BgL_argsz00_4279;

						BgL_resultz00_4278 = BgL_g1052z00_2028;
						BgL_argsz00_4279 = BgL_g1053z00_2029;
					BgL_loopz00_4277:
						if (PAIRP(BgL_argsz00_4279))
							{
								obj_t BgL_argsz00_8625;
								obj_t BgL_resultz00_8622;

								BgL_resultz00_8622 =
									BGl_2zd2zd2zz__r4_numbers_6_5z00(BgL_resultz00_4278,
									CAR(BgL_argsz00_4279));
								BgL_argsz00_8625 = CDR(BgL_argsz00_4279);
								BgL_argsz00_4279 = BgL_argsz00_8625;
								BgL_resultz00_4278 = BgL_resultz00_8622;
								goto BgL_loopz00_4277;
							}
						else
							{	/* Ieee/number.scm 760 */
								return BgL_resultz00_4278;
							}
					}
				}
			else
				{	/* Ieee/number.scm 757 */
					BGL_TAIL return
						BGl_2zd2zd2zz__r4_numbers_6_5z00(BINT(0L), BgL_xz00_101);
				}
		}

	}



/* &- */
	obj_t BGl_z62zd2zb0zz__r4_numbers_6_5z00(obj_t BgL_envz00_5072,
		obj_t BgL_xz00_5073, obj_t BgL_yz00_5074)
	{
		{	/* Ieee/number.scm 756 */
			return BGl_zd2zd2zz__r4_numbers_6_5z00(BgL_xz00_5073, BgL_yz00_5074);
		}

	}



/* 2/ */
	BGL_EXPORTED_DEF obj_t BGl_2zf2zf2zz__r4_numbers_6_5z00(obj_t BgL_xz00_103,
		obj_t BgL_yz00_104)
	{
		{	/* Ieee/number.scm 768 */
			if (INTEGERP(BgL_xz00_103))
				{	/* Ieee/number.scm 770 */
					if (INTEGERP(BgL_yz00_104))
						{	/* Ieee/number.scm 773 */
							bool_t BgL_test4122z00_8634;

							{	/* Ieee/number.scm 773 */
								long BgL_arg2397z00_2046;

								{	/* Ieee/number.scm 773 */
									long BgL_n1z00_4290;
									long BgL_n2z00_4291;

									BgL_n1z00_4290 = (long) CINT(BgL_xz00_103);
									BgL_n2z00_4291 = (long) CINT(BgL_yz00_104);
									{	/* Ieee/number.scm 773 */
										bool_t BgL_test4123z00_8637;

										{	/* Ieee/number.scm 773 */
											long BgL_arg3083z00_4293;

											BgL_arg3083z00_4293 =
												(((BgL_n1z00_4290) | (BgL_n2z00_4291)) & -2147483648);
											BgL_test4123z00_8637 = (BgL_arg3083z00_4293 == 0L);
										}
										if (BgL_test4123z00_8637)
											{	/* Ieee/number.scm 773 */
												int32_t BgL_arg3080z00_4294;

												{	/* Ieee/number.scm 773 */
													int32_t BgL_arg3081z00_4295;
													int32_t BgL_arg3082z00_4296;

													BgL_arg3081z00_4295 = (int32_t) (BgL_n1z00_4290);
													BgL_arg3082z00_4296 = (int32_t) (BgL_n2z00_4291);
													BgL_arg3080z00_4294 =
														(BgL_arg3081z00_4295 % BgL_arg3082z00_4296);
												}
												{	/* Ieee/number.scm 773 */
													long BgL_arg3246z00_4301;

													BgL_arg3246z00_4301 = (long) (BgL_arg3080z00_4294);
													BgL_arg2397z00_2046 = (long) (BgL_arg3246z00_4301);
											}}
										else
											{	/* Ieee/number.scm 773 */
												BgL_arg2397z00_2046 = (BgL_n1z00_4290 % BgL_n2z00_4291);
											}
									}
								}
								BgL_test4122z00_8634 = (BgL_arg2397z00_2046 == 0L);
							}
							if (BgL_test4122z00_8634)
								{	/* Ieee/number.scm 773 */
									return
										BINT(
										((long) CINT(BgL_xz00_103) / (long) CINT(BgL_yz00_104)));
								}
							else
								{	/* Ieee/number.scm 773 */
									return
										DOUBLE_TO_REAL(
										((double) (
												(long) CINT(BgL_xz00_103)) /
											(double) ((long) CINT(BgL_yz00_104))));
						}}
					else
						{	/* Ieee/number.scm 772 */
							if (REALP(BgL_yz00_104))
								{	/* Ieee/number.scm 776 */
									return
										DOUBLE_TO_REAL(
										((double) (
												(long) CINT(BgL_xz00_103)) /
											REAL_TO_DOUBLE(BgL_yz00_104)));
								}
							else
								{	/* Ieee/number.scm 776 */
									if (ELONGP(BgL_yz00_104))
										{	/* Ieee/number.scm 779 */
											long BgL_exz00_2050;

											{	/* Ieee/number.scm 779 */
												long BgL_tmpz00_8666;

												BgL_tmpz00_8666 = (long) CINT(BgL_xz00_103);
												BgL_exz00_2050 = (long) (BgL_tmpz00_8666);
											}
											{	/* Ieee/number.scm 780 */
												bool_t BgL_test4126z00_8669;

												{	/* Ieee/number.scm 780 */
													long BgL_arg2405z00_2055;

													{	/* Ieee/number.scm 780 */
														long BgL_n2z00_4312;

														BgL_n2z00_4312 = BELONG_TO_LONG(BgL_yz00_104);
														BgL_arg2405z00_2055 =
															(BgL_exz00_2050 % BgL_n2z00_4312);
													}
													BgL_test4126z00_8669 =
														(BgL_arg2405z00_2055 == ((long) 0));
												}
												if (BgL_test4126z00_8669)
													{	/* Ieee/number.scm 781 */
														long BgL_za72za7_4316;

														BgL_za72za7_4316 = BELONG_TO_LONG(BgL_yz00_104);
														{	/* Ieee/number.scm 781 */
															long BgL_tmpz00_8674;

															BgL_tmpz00_8674 =
																(BgL_exz00_2050 / BgL_za72za7_4316);
															return make_belong(BgL_tmpz00_8674);
														}
													}
												else
													{	/* Ieee/number.scm 780 */
														return
															DOUBLE_TO_REAL(
															((double) (
																	(long) CINT(BgL_xz00_103)) /
																(double) (BELONG_TO_LONG(BgL_yz00_104))));
										}}}
									else
										{	/* Ieee/number.scm 778 */
											if (LLONGP(BgL_yz00_104))
												{	/* Ieee/number.scm 784 */
													BGL_LONGLONG_T BgL_lxz00_2057;

													{	/* Ieee/number.scm 784 */
														long BgL_tmpz00_8685;

														BgL_tmpz00_8685 = (long) CINT(BgL_xz00_103);
														BgL_lxz00_2057 = LONG_TO_LLONG(BgL_tmpz00_8685);
													}
													{	/* Ieee/number.scm 785 */
														bool_t BgL_test4128z00_8688;

														{	/* Ieee/number.scm 785 */
															BGL_LONGLONG_T BgL_arg2412z00_2062;

															{	/* Ieee/number.scm 785 */
																BGL_LONGLONG_T BgL_tmpz00_8689;

																BgL_tmpz00_8689 = BLLONG_TO_LLONG(BgL_yz00_104);
																BgL_arg2412z00_2062 =
																	(BgL_lxz00_2057 % BgL_tmpz00_8689);
															}
															BgL_test4128z00_8688 =
																(BgL_arg2412z00_2062 == ((BGL_LONGLONG_T) 0));
														}
														if (BgL_test4128z00_8688)
															{	/* Ieee/number.scm 786 */
																BGL_LONGLONG_T BgL_za72za7_4325;

																BgL_za72za7_4325 =
																	BLLONG_TO_LLONG(BgL_yz00_104);
																return
																	make_bllong(
																	(BgL_lxz00_2057 / BgL_za72za7_4325));
															}
														else
															{	/* Ieee/number.scm 785 */
																return
																	DOUBLE_TO_REAL(
																	((double) (
																			(long) CINT(BgL_xz00_103)) /
																		(double) (BLLONG_TO_LLONG(BgL_yz00_104))));
												}}}
											else
												{	/* Ieee/number.scm 783 */
													if (BIGNUMP(BgL_yz00_104))
														{	/* Ieee/number.scm 789 */
															obj_t BgL_qz00_2064;

															BgL_qz00_2064 =
																bgl_bignum_div(bgl_long_to_bignum(
																	(long) CINT(BgL_xz00_103)), BgL_yz00_104);
															{	/* Ieee/number.scm 790 */
																obj_t BgL_rz00_2065;

																{	/* Ieee/number.scm 791 */
																	obj_t BgL_tmpz00_4329;

																	{	/* Ieee/number.scm 791 */
																		int BgL_tmpz00_8707;

																		BgL_tmpz00_8707 = (int) (1L);
																		BgL_tmpz00_4329 =
																			BGL_MVALUES_VAL(BgL_tmpz00_8707);
																	}
																	{	/* Ieee/number.scm 791 */
																		int BgL_tmpz00_8710;

																		BgL_tmpz00_8710 = (int) (1L);
																		BGL_MVALUES_VAL_SET(BgL_tmpz00_8710,
																			BUNSPEC);
																	}
																	BgL_rz00_2065 = BgL_tmpz00_4329;
																}
																{	/* Ieee/number.scm 791 */
																	bool_t BgL_test4130z00_8713;

																	{	/* Ieee/number.scm 791 */
																		obj_t BgL_tmpz00_8714;

																		BgL_tmpz00_8714 = ((obj_t) BgL_rz00_2065);
																		BgL_test4130z00_8713 =
																			BXZERO(BgL_tmpz00_8714);
																	}
																	if (BgL_test4130z00_8713)
																		{	/* Ieee/number.scm 791 */
																			return BgL_qz00_2064;
																		}
																	else
																		{	/* Ieee/number.scm 791 */
																			return
																				DOUBLE_TO_REAL(
																				((double) (
																						(long) CINT(BgL_xz00_103)) /
																					bgl_bignum_to_flonum(BgL_yz00_104)));
														}}}}
													else
														{	/* Ieee/number.scm 788 */
															return
																BGl_errorz00zz__errorz00
																(BGl_string3342z00zz__r4_numbers_6_5z00,
																BGl_string3328z00zz__r4_numbers_6_5z00,
																BgL_yz00_104);
														}
												}
										}
								}
						}
				}
			else
				{	/* Ieee/number.scm 770 */
					if (REALP(BgL_xz00_103))
						{	/* Ieee/number.scm 796 */
							if (REALP(BgL_yz00_104))
								{	/* Ieee/number.scm 798 */
									return
										DOUBLE_TO_REAL(
										(REAL_TO_DOUBLE(BgL_xz00_103) /
											REAL_TO_DOUBLE(BgL_yz00_104)));
								}
							else
								{	/* Ieee/number.scm 798 */
									if (INTEGERP(BgL_yz00_104))
										{	/* Ieee/number.scm 800 */
											return
												DOUBLE_TO_REAL(
												(REAL_TO_DOUBLE(BgL_xz00_103) /
													(double) ((long) CINT(BgL_yz00_104))));
										}
									else
										{	/* Ieee/number.scm 800 */
											if (ELONGP(BgL_yz00_104))
												{	/* Ieee/number.scm 802 */
													return
														DOUBLE_TO_REAL(
														(REAL_TO_DOUBLE(BgL_xz00_103) /
															(double) (BELONG_TO_LONG(BgL_yz00_104))));
												}
											else
												{	/* Ieee/number.scm 802 */
													if (LLONGP(BgL_yz00_104))
														{	/* Ieee/number.scm 804 */
															return
																DOUBLE_TO_REAL(
																(REAL_TO_DOUBLE(BgL_xz00_103) /
																	(double) (BLLONG_TO_LLONG(BgL_yz00_104))));
														}
													else
														{	/* Ieee/number.scm 804 */
															if (BIGNUMP(BgL_yz00_104))
																{	/* Ieee/number.scm 806 */
																	return
																		DOUBLE_TO_REAL(
																		(REAL_TO_DOUBLE(BgL_xz00_103) /
																			bgl_bignum_to_flonum(BgL_yz00_104)));
																}
															else
																{	/* Ieee/number.scm 806 */
																	return
																		BGl_errorz00zz__errorz00
																		(BGl_string3342z00zz__r4_numbers_6_5z00,
																		BGl_string3328z00zz__r4_numbers_6_5z00,
																		BgL_yz00_104);
																}
														}
												}
										}
								}
						}
					else
						{	/* Ieee/number.scm 796 */
							if (ELONGP(BgL_xz00_103))
								{	/* Ieee/number.scm 810 */
									if (INTEGERP(BgL_yz00_104))
										{	/* Ieee/number.scm 813 */
											long BgL_eyz00_2082;

											{	/* Ieee/number.scm 813 */
												long BgL_tmpz00_8763;

												BgL_tmpz00_8763 = (long) CINT(BgL_yz00_104);
												BgL_eyz00_2082 = (long) (BgL_tmpz00_8763);
											}
											{	/* Ieee/number.scm 814 */
												bool_t BgL_test4139z00_8766;

												{	/* Ieee/number.scm 814 */
													long BgL_arg2439z00_2087;

													{	/* Ieee/number.scm 814 */
														long BgL_n1z00_4346;

														BgL_n1z00_4346 = BELONG_TO_LONG(BgL_xz00_103);
														BgL_arg2439z00_2087 =
															(BgL_n1z00_4346 % BgL_eyz00_2082);
													}
													BgL_test4139z00_8766 =
														(BgL_arg2439z00_2087 == ((long) 0));
												}
												if (BgL_test4139z00_8766)
													{	/* Ieee/number.scm 815 */
														long BgL_za71za7_4350;

														BgL_za71za7_4350 = BELONG_TO_LONG(BgL_xz00_103);
														{	/* Ieee/number.scm 815 */
															long BgL_tmpz00_8771;

															BgL_tmpz00_8771 =
																(BgL_za71za7_4350 / BgL_eyz00_2082);
															return make_belong(BgL_tmpz00_8771);
														}
													}
												else
													{	/* Ieee/number.scm 814 */
														return
															DOUBLE_TO_REAL(
															((double) (BELONG_TO_LONG(BgL_xz00_103)) /
																(double) ((long) CINT(BgL_yz00_104))));
										}}}
									else
										{	/* Ieee/number.scm 812 */
											if (REALP(BgL_yz00_104))
												{	/* Ieee/number.scm 817 */
													return
														DOUBLE_TO_REAL(
														((double) (BELONG_TO_LONG(BgL_xz00_103)) /
															REAL_TO_DOUBLE(BgL_yz00_104)));
												}
											else
												{	/* Ieee/number.scm 817 */
													if (ELONGP(BgL_yz00_104))
														{	/* Ieee/number.scm 820 */
															bool_t BgL_test4142z00_8789;

															{	/* Ieee/number.scm 820 */
																long BgL_arg2449z00_2095;

																{	/* Ieee/number.scm 820 */
																	long BgL_n1z00_4356;
																	long BgL_n2z00_4357;

																	BgL_n1z00_4356 = BELONG_TO_LONG(BgL_xz00_103);
																	BgL_n2z00_4357 = BELONG_TO_LONG(BgL_yz00_104);
																	BgL_arg2449z00_2095 =
																		(BgL_n1z00_4356 % BgL_n2z00_4357);
																}
																BgL_test4142z00_8789 =
																	(BgL_arg2449z00_2095 == ((long) 0));
															}
															if (BgL_test4142z00_8789)
																{	/* Ieee/number.scm 821 */
																	long BgL_za71za7_4360;
																	long BgL_za72za7_4361;

																	BgL_za71za7_4360 =
																		BELONG_TO_LONG(BgL_xz00_103);
																	BgL_za72za7_4361 =
																		BELONG_TO_LONG(BgL_yz00_104);
																	{	/* Ieee/number.scm 821 */
																		long BgL_tmpz00_8796;

																		BgL_tmpz00_8796 =
																			(BgL_za71za7_4360 / BgL_za72za7_4361);
																		return make_belong(BgL_tmpz00_8796);
																	}
																}
															else
																{	/* Ieee/number.scm 820 */
																	return
																		DOUBLE_TO_REAL(
																		((double) (BELONG_TO_LONG(BgL_xz00_103)) /
																			(double) (BELONG_TO_LONG(BgL_yz00_104))));
														}}
													else
														{	/* Ieee/number.scm 819 */
															if (LLONGP(BgL_yz00_104))
																{	/* Ieee/number.scm 824 */
																	double BgL_fxz00_2097;

																	BgL_fxz00_2097 =
																		(double) (BELONG_TO_LONG(BgL_xz00_103));
																	{	/* Ieee/number.scm 824 */
																		BGL_LONGLONG_T BgL_lxz00_2098;

																		BgL_lxz00_2098 =
																			(BGL_LONGLONG_T) (BgL_fxz00_2097);
																		{	/* Ieee/number.scm 825 */

																			{	/* Ieee/number.scm 826 */
																				bool_t BgL_test4144z00_8810;

																				{	/* Ieee/number.scm 826 */
																					BGL_LONGLONG_T BgL_arg2455z00_2102;

																					{	/* Ieee/number.scm 826 */
																						BGL_LONGLONG_T BgL_tmpz00_8811;

																						BgL_tmpz00_8811 =
																							BLLONG_TO_LLONG(BgL_yz00_104);
																						BgL_arg2455z00_2102 =
																							(BgL_lxz00_2098 %
																							BgL_tmpz00_8811);
																					}
																					BgL_test4144z00_8810 =
																						(BgL_arg2455z00_2102 ==
																						((BGL_LONGLONG_T) 0));
																				}
																				if (BgL_test4144z00_8810)
																					{	/* Ieee/number.scm 827 */
																						BGL_LONGLONG_T BgL_za72za7_4369;

																						BgL_za72za7_4369 =
																							BLLONG_TO_LLONG(BgL_yz00_104);
																						return
																							make_bllong(
																							(BgL_lxz00_2098 /
																								BgL_za72za7_4369));
																					}
																				else
																					{	/* Ieee/number.scm 826 */
																						return
																							DOUBLE_TO_REAL(
																							(BgL_fxz00_2097 /
																								(double) (BLLONG_TO_LLONG
																									(BgL_yz00_104))));
																}}}}}
															else
																{	/* Ieee/number.scm 823 */
																	if (BIGNUMP(BgL_yz00_104))
																		{	/* Ieee/number.scm 830 */
																			obj_t BgL_qz00_2104;

																			{	/* Ieee/number.scm 831 */
																				obj_t BgL_arg2460z00_2109;

																				{	/* Ieee/number.scm 831 */
																					long BgL_xz00_4372;

																					BgL_xz00_4372 =
																						BELONG_TO_LONG(BgL_xz00_103);
																					BgL_arg2460z00_2109 =
																						bgl_long_to_bignum(BgL_xz00_4372);
																				}
																				BgL_qz00_2104 =
																					bgl_bignum_div(BgL_arg2460z00_2109,
																					BgL_yz00_104);
																			}
																			{	/* Ieee/number.scm 831 */
																				obj_t BgL_rz00_2105;

																				{	/* Ieee/number.scm 832 */
																					obj_t BgL_tmpz00_4373;

																					{	/* Ieee/number.scm 832 */
																						int BgL_tmpz00_8827;

																						BgL_tmpz00_8827 = (int) (1L);
																						BgL_tmpz00_4373 =
																							BGL_MVALUES_VAL(BgL_tmpz00_8827);
																					}
																					{	/* Ieee/number.scm 832 */
																						int BgL_tmpz00_8830;

																						BgL_tmpz00_8830 = (int) (1L);
																						BGL_MVALUES_VAL_SET(BgL_tmpz00_8830,
																							BUNSPEC);
																					}
																					BgL_rz00_2105 = BgL_tmpz00_4373;
																				}
																				{	/* Ieee/number.scm 832 */
																					bool_t BgL_test4146z00_8833;

																					{	/* Ieee/number.scm 832 */
																						obj_t BgL_tmpz00_8834;

																						BgL_tmpz00_8834 =
																							((obj_t) BgL_rz00_2105);
																						BgL_test4146z00_8833 =
																							BXZERO(BgL_tmpz00_8834);
																					}
																					if (BgL_test4146z00_8833)
																						{	/* Ieee/number.scm 832 */
																							return BgL_qz00_2104;
																						}
																					else
																						{	/* Ieee/number.scm 832 */
																							return
																								DOUBLE_TO_REAL(
																								((double) (BELONG_TO_LONG
																										(BgL_xz00_103)) /
																									bgl_bignum_to_flonum
																									(BgL_yz00_104)));
																		}}}}
																	else
																		{	/* Ieee/number.scm 829 */
																			return
																				BGl_errorz00zz__errorz00
																				(BGl_string3342z00zz__r4_numbers_6_5z00,
																				BGl_string3328z00zz__r4_numbers_6_5z00,
																				BgL_yz00_104);
																		}
																}
														}
												}
										}
								}
							else
								{	/* Ieee/number.scm 810 */
									if (LLONGP(BgL_xz00_103))
										{	/* Ieee/number.scm 837 */
											if (INTEGERP(BgL_yz00_104))
												{	/* Ieee/number.scm 840 */
													BGL_LONGLONG_T BgL_lyz00_2112;

													{	/* Ieee/number.scm 840 */
														long BgL_tmpz00_8847;

														BgL_tmpz00_8847 = (long) CINT(BgL_yz00_104);
														BgL_lyz00_2112 = LONG_TO_LLONG(BgL_tmpz00_8847);
													}
													{	/* Ieee/number.scm 841 */
														bool_t BgL_test4149z00_8850;

														{	/* Ieee/number.scm 841 */
															BGL_LONGLONG_T BgL_arg2469z00_2117;

															{	/* Ieee/number.scm 841 */
																BGL_LONGLONG_T BgL_tmpz00_8851;

																BgL_tmpz00_8851 = BLLONG_TO_LLONG(BgL_xz00_103);
																BgL_arg2469z00_2117 =
																	(BgL_tmpz00_8851 % BgL_lyz00_2112);
															}
															BgL_test4149z00_8850 =
																(BgL_arg2469z00_2117 == ((BGL_LONGLONG_T) 0));
														}
														if (BgL_test4149z00_8850)
															{	/* Ieee/number.scm 842 */
																BGL_LONGLONG_T BgL_za71za7_4383;

																BgL_za71za7_4383 =
																	BLLONG_TO_LLONG(BgL_xz00_103);
																return
																	make_bllong(
																	(BgL_za71za7_4383 / BgL_lyz00_2112));
															}
														else
															{	/* Ieee/number.scm 841 */
																return
																	DOUBLE_TO_REAL(
																	((double) (BLLONG_TO_LLONG(BgL_xz00_103)) /
																		(double) ((long) CINT(BgL_yz00_104))));
												}}}
											else
												{	/* Ieee/number.scm 839 */
													if (REALP(BgL_yz00_104))
														{	/* Ieee/number.scm 844 */
															return
																DOUBLE_TO_REAL(
																((double) (BLLONG_TO_LLONG(BgL_xz00_103)) /
																	REAL_TO_DOUBLE(BgL_yz00_104)));
														}
													else
														{	/* Ieee/number.scm 844 */
															if (ELONGP(BgL_yz00_104))
																{	/* Ieee/number.scm 847 */
																	double BgL_fyz00_2121;

																	BgL_fyz00_2121 =
																		(double) (BELONG_TO_LONG(BgL_yz00_104));
																	{	/* Ieee/number.scm 847 */
																		BGL_LONGLONG_T BgL_lyz00_2122;

																		BgL_lyz00_2122 =
																			(BGL_LONGLONG_T) (BgL_fyz00_2121);
																		{	/* Ieee/number.scm 848 */

																			{	/* Ieee/number.scm 849 */
																				bool_t BgL_test4152z00_8876;

																				{	/* Ieee/number.scm 849 */
																					BGL_LONGLONG_T BgL_arg2476z00_2126;

																					{	/* Ieee/number.scm 849 */
																						BGL_LONGLONG_T BgL_tmpz00_8877;

																						BgL_tmpz00_8877 =
																							BLLONG_TO_LLONG(BgL_xz00_103);
																						BgL_arg2476z00_2126 =
																							(BgL_tmpz00_8877 %
																							BgL_lyz00_2122);
																					}
																					BgL_test4152z00_8876 =
																						(BgL_arg2476z00_2126 ==
																						((BGL_LONGLONG_T) 0));
																				}
																				if (BgL_test4152z00_8876)
																					{	/* Ieee/number.scm 850 */
																						BGL_LONGLONG_T BgL_za71za7_4393;

																						BgL_za71za7_4393 =
																							BLLONG_TO_LLONG(BgL_xz00_103);
																						return
																							make_bllong(
																							(BgL_za71za7_4393 /
																								BgL_lyz00_2122));
																					}
																				else
																					{	/* Ieee/number.scm 849 */
																						return
																							DOUBLE_TO_REAL(
																							((double) (BLLONG_TO_LLONG
																									(BgL_xz00_103)) /
																								BgL_fyz00_2121));
																}}}}}
															else
																{	/* Ieee/number.scm 846 */
																	if (LLONGP(BgL_yz00_104))
																		{	/* Ieee/number.scm 853 */
																			bool_t BgL_test4154z00_8890;

																			{	/* Ieee/number.scm 853 */
																				BGL_LONGLONG_T BgL_arg2483z00_2132;

																				{	/* Ieee/number.scm 853 */
																					BGL_LONGLONG_T BgL_auxz00_8893;
																					BGL_LONGLONG_T BgL_tmpz00_8891;

																					BgL_auxz00_8893 =
																						BLLONG_TO_LLONG(BgL_yz00_104);
																					BgL_tmpz00_8891 =
																						BLLONG_TO_LLONG(BgL_xz00_103);
																					BgL_arg2483z00_2132 =
																						(BgL_tmpz00_8891 % BgL_auxz00_8893);
																				}
																				BgL_test4154z00_8890 =
																					(BgL_arg2483z00_2132 ==
																					((BGL_LONGLONG_T) 0));
																			}
																			if (BgL_test4154z00_8890)
																				{	/* Ieee/number.scm 854 */
																					BGL_LONGLONG_T BgL_za71za7_4401;
																					BGL_LONGLONG_T BgL_za72za7_4402;

																					BgL_za71za7_4401 =
																						BLLONG_TO_LLONG(BgL_xz00_103);
																					BgL_za72za7_4402 =
																						BLLONG_TO_LLONG(BgL_yz00_104);
																					return
																						make_bllong(
																						(BgL_za71za7_4401 /
																							BgL_za72za7_4402));
																				}
																			else
																				{	/* Ieee/number.scm 853 */
																					return
																						DOUBLE_TO_REAL(
																						((double) (BLLONG_TO_LLONG
																								(BgL_xz00_103)) /
																							(double) (BLLONG_TO_LLONG
																								(BgL_yz00_104))));
																		}}
																	else
																		{	/* Ieee/number.scm 852 */
																			if (BIGNUMP(BgL_yz00_104))
																				{	/* Ieee/number.scm 857 */
																					obj_t BgL_qz00_2134;

																					BgL_qz00_2134 =
																						bgl_bignum_div(bgl_llong_to_bignum
																						(BLLONG_TO_LLONG(BgL_xz00_103)),
																						BgL_yz00_104);
																					{	/* Ieee/number.scm 858 */
																						obj_t BgL_rz00_2135;

																						{	/* Ieee/number.scm 859 */
																							obj_t BgL_tmpz00_4406;

																							{	/* Ieee/number.scm 859 */
																								int BgL_tmpz00_8912;

																								BgL_tmpz00_8912 = (int) (1L);
																								BgL_tmpz00_4406 =
																									BGL_MVALUES_VAL
																									(BgL_tmpz00_8912);
																							}
																							{	/* Ieee/number.scm 859 */
																								int BgL_tmpz00_8915;

																								BgL_tmpz00_8915 = (int) (1L);
																								BGL_MVALUES_VAL_SET
																									(BgL_tmpz00_8915, BUNSPEC);
																							}
																							BgL_rz00_2135 = BgL_tmpz00_4406;
																						}
																						{	/* Ieee/number.scm 859 */
																							bool_t BgL_test4156z00_8918;

																							{	/* Ieee/number.scm 859 */
																								obj_t BgL_tmpz00_8919;

																								BgL_tmpz00_8919 =
																									((obj_t) BgL_rz00_2135);
																								BgL_test4156z00_8918 =
																									BXZERO(BgL_tmpz00_8919);
																							}
																							if (BgL_test4156z00_8918)
																								{	/* Ieee/number.scm 859 */
																									return BgL_qz00_2134;
																								}
																							else
																								{	/* Ieee/number.scm 859 */
																									return
																										DOUBLE_TO_REAL(
																										((double) (BLLONG_TO_LLONG
																												(BgL_xz00_103)) /
																											bgl_bignum_to_flonum
																											(BgL_yz00_104)));
																				}}}}
																			else
																				{	/* Ieee/number.scm 856 */
																					return
																						BGl_errorz00zz__errorz00
																						(BGl_string3342z00zz__r4_numbers_6_5z00,
																						BGl_string3328z00zz__r4_numbers_6_5z00,
																						BgL_yz00_104);
																				}
																		}
																}
														}
												}
										}
									else
										{	/* Ieee/number.scm 837 */
											if (BIGNUMP(BgL_xz00_103))
												{	/* Ieee/number.scm 864 */
													if (INTEGERP(BgL_yz00_104))
														{	/* Ieee/number.scm 867 */
															obj_t BgL_qz00_2142;

															BgL_qz00_2142 =
																bgl_bignum_div(BgL_xz00_103,
																bgl_long_to_bignum((long) CINT(BgL_yz00_104)));
															{	/* Ieee/number.scm 868 */
																obj_t BgL_rz00_2143;

																{	/* Ieee/number.scm 869 */
																	obj_t BgL_tmpz00_4412;

																	{	/* Ieee/number.scm 869 */
																		int BgL_tmpz00_8935;

																		BgL_tmpz00_8935 = (int) (1L);
																		BgL_tmpz00_4412 =
																			BGL_MVALUES_VAL(BgL_tmpz00_8935);
																	}
																	{	/* Ieee/number.scm 869 */
																		int BgL_tmpz00_8938;

																		BgL_tmpz00_8938 = (int) (1L);
																		BGL_MVALUES_VAL_SET(BgL_tmpz00_8938,
																			BUNSPEC);
																	}
																	BgL_rz00_2143 = BgL_tmpz00_4412;
																}
																{	/* Ieee/number.scm 869 */
																	bool_t BgL_test4159z00_8941;

																	{	/* Ieee/number.scm 869 */
																		obj_t BgL_tmpz00_8942;

																		BgL_tmpz00_8942 = ((obj_t) BgL_rz00_2143);
																		BgL_test4159z00_8941 =
																			BXZERO(BgL_tmpz00_8942);
																	}
																	if (BgL_test4159z00_8941)
																		{	/* Ieee/number.scm 869 */
																			return BgL_qz00_2142;
																		}
																	else
																		{	/* Ieee/number.scm 869 */
																			return
																				DOUBLE_TO_REAL(
																				(bgl_bignum_to_flonum(BgL_xz00_103) /
																					(double) (
																						(long) CINT(BgL_yz00_104))));
														}}}}
													else
														{	/* Ieee/number.scm 866 */
															if (REALP(BgL_yz00_104))
																{	/* Ieee/number.scm 872 */
																	return
																		DOUBLE_TO_REAL(
																		(bgl_bignum_to_flonum(BgL_xz00_103) /
																			REAL_TO_DOUBLE(BgL_yz00_104)));
																}
															else
																{	/* Ieee/number.scm 872 */
																	if (ELONGP(BgL_yz00_104))
																		{	/* Ieee/number.scm 875 */
																			obj_t BgL_qz00_2151;

																			{	/* Ieee/number.scm 876 */
																				obj_t BgL_arg2502z00_2156;

																				{	/* Ieee/number.scm 876 */
																					long BgL_xz00_4420;

																					BgL_xz00_4420 =
																						BELONG_TO_LONG(BgL_yz00_104);
																					BgL_arg2502z00_2156 =
																						bgl_long_to_bignum(BgL_xz00_4420);
																				}
																				BgL_qz00_2151 =
																					bgl_bignum_div(BgL_xz00_103,
																					BgL_arg2502z00_2156);
																			}
																			{	/* Ieee/number.scm 876 */
																				obj_t BgL_rz00_2152;

																				{	/* Ieee/number.scm 877 */
																					obj_t BgL_tmpz00_4421;

																					{	/* Ieee/number.scm 877 */
																						int BgL_tmpz00_8961;

																						BgL_tmpz00_8961 = (int) (1L);
																						BgL_tmpz00_4421 =
																							BGL_MVALUES_VAL(BgL_tmpz00_8961);
																					}
																					{	/* Ieee/number.scm 877 */
																						int BgL_tmpz00_8964;

																						BgL_tmpz00_8964 = (int) (1L);
																						BGL_MVALUES_VAL_SET(BgL_tmpz00_8964,
																							BUNSPEC);
																					}
																					BgL_rz00_2152 = BgL_tmpz00_4421;
																				}
																				{	/* Ieee/number.scm 877 */
																					bool_t BgL_test4162z00_8967;

																					{	/* Ieee/number.scm 877 */
																						obj_t BgL_tmpz00_8968;

																						BgL_tmpz00_8968 =
																							((obj_t) BgL_rz00_2152);
																						BgL_test4162z00_8967 =
																							BXZERO(BgL_tmpz00_8968);
																					}
																					if (BgL_test4162z00_8967)
																						{	/* Ieee/number.scm 877 */
																							return BgL_qz00_2151;
																						}
																					else
																						{	/* Ieee/number.scm 877 */
																							return
																								DOUBLE_TO_REAL(
																								(bgl_bignum_to_flonum
																									(BgL_xz00_103) /
																									(double) (BELONG_TO_LONG
																										(BgL_yz00_104))));
																		}}}}
																	else
																		{	/* Ieee/number.scm 874 */
																			if (LLONGP(BgL_yz00_104))
																				{	/* Ieee/number.scm 881 */
																					obj_t BgL_qz00_2158;

																					BgL_qz00_2158 =
																						bgl_bignum_div(BgL_xz00_103,
																						bgl_llong_to_bignum(BLLONG_TO_LLONG
																							(BgL_yz00_104)));
																					{	/* Ieee/number.scm 882 */
																						obj_t BgL_rz00_2159;

																						{	/* Ieee/number.scm 883 */
																							obj_t BgL_tmpz00_4427;

																							{	/* Ieee/number.scm 883 */
																								int BgL_tmpz00_8981;

																								BgL_tmpz00_8981 = (int) (1L);
																								BgL_tmpz00_4427 =
																									BGL_MVALUES_VAL
																									(BgL_tmpz00_8981);
																							}
																							{	/* Ieee/number.scm 883 */
																								int BgL_tmpz00_8984;

																								BgL_tmpz00_8984 = (int) (1L);
																								BGL_MVALUES_VAL_SET
																									(BgL_tmpz00_8984, BUNSPEC);
																							}
																							BgL_rz00_2159 = BgL_tmpz00_4427;
																						}
																						{	/* Ieee/number.scm 883 */
																							bool_t BgL_test4164z00_8987;

																							{	/* Ieee/number.scm 883 */
																								obj_t BgL_tmpz00_8988;

																								BgL_tmpz00_8988 =
																									((obj_t) BgL_rz00_2159);
																								BgL_test4164z00_8987 =
																									BXZERO(BgL_tmpz00_8988);
																							}
																							if (BgL_test4164z00_8987)
																								{	/* Ieee/number.scm 883 */
																									return BgL_qz00_2158;
																								}
																							else
																								{	/* Ieee/number.scm 883 */
																									return
																										DOUBLE_TO_REAL(
																										(bgl_bignum_to_flonum
																											(BgL_xz00_103) /
																											(double) (BLLONG_TO_LLONG
																												(BgL_yz00_104))));
																				}}}}
																			else
																				{	/* Ieee/number.scm 880 */
																					if (BIGNUMP(BgL_yz00_104))
																						{	/* Ieee/number.scm 887 */
																							obj_t BgL_qz00_2165;

																							BgL_qz00_2165 =
																								bgl_bignum_div(BgL_xz00_103,
																								BgL_yz00_104);
																							{	/* Ieee/number.scm 888 */
																								obj_t BgL_rz00_2166;

																								{	/* Ieee/number.scm 889 */
																									obj_t BgL_tmpz00_4432;

																									{	/* Ieee/number.scm 889 */
																										int BgL_tmpz00_8999;

																										BgL_tmpz00_8999 =
																											(int) (1L);
																										BgL_tmpz00_4432 =
																											BGL_MVALUES_VAL
																											(BgL_tmpz00_8999);
																									}
																									{	/* Ieee/number.scm 889 */
																										int BgL_tmpz00_9002;

																										BgL_tmpz00_9002 =
																											(int) (1L);
																										BGL_MVALUES_VAL_SET
																											(BgL_tmpz00_9002,
																											BUNSPEC);
																									}
																									BgL_rz00_2166 =
																										BgL_tmpz00_4432;
																								}
																								{	/* Ieee/number.scm 889 */
																									bool_t BgL_test4166z00_9005;

																									{	/* Ieee/number.scm 889 */
																										obj_t BgL_tmpz00_9006;

																										BgL_tmpz00_9006 =
																											((obj_t) BgL_rz00_2166);
																										BgL_test4166z00_9005 =
																											BXZERO(BgL_tmpz00_9006);
																									}
																									if (BgL_test4166z00_9005)
																										{	/* Ieee/number.scm 889 */
																											return BgL_qz00_2165;
																										}
																									else
																										{	/* Ieee/number.scm 889 */
																											return
																												DOUBLE_TO_REAL(
																												(bgl_bignum_to_flonum
																													(BgL_xz00_103) /
																													bgl_bignum_to_flonum
																													(BgL_yz00_104)));
																										}
																								}
																							}
																						}
																					else
																						{	/* Ieee/number.scm 886 */
																							return
																								BGl_errorz00zz__errorz00
																								(BGl_string3342z00zz__r4_numbers_6_5z00,
																								BGl_string3328z00zz__r4_numbers_6_5z00,
																								BgL_yz00_104);
																						}
																				}
																		}
																}
														}
												}
											else
												{	/* Ieee/number.scm 864 */
													return
														BGl_errorz00zz__errorz00
														(BGl_string3342z00zz__r4_numbers_6_5z00,
														BGl_string3328z00zz__r4_numbers_6_5z00,
														BgL_xz00_103);
												}
										}
								}
						}
				}
		}

	}



/* &2/ */
	obj_t BGl_z622zf2z90zz__r4_numbers_6_5z00(obj_t BgL_envz00_5075,
		obj_t BgL_xz00_5076, obj_t BgL_yz00_5077)
	{
		{	/* Ieee/number.scm 768 */
			return BGl_2zf2zf2zz__r4_numbers_6_5z00(BgL_xz00_5076, BgL_yz00_5077);
		}

	}



/* / */
	BGL_EXPORTED_DEF obj_t BGl_zf2zf2zz__r4_numbers_6_5z00(obj_t BgL_xz00_105,
		obj_t BgL_yz00_106)
	{
		{	/* Ieee/number.scm 900 */
			if (PAIRP(BgL_yz00_106))
				{	/* Ieee/number.scm 902 */
					obj_t BgL_g1054z00_2171;
					obj_t BgL_g1055z00_2172;

					BgL_g1054z00_2171 =
						BGl_2zf2zf2zz__r4_numbers_6_5z00(BgL_xz00_105, CAR(BgL_yz00_106));
					BgL_g1055z00_2172 = CDR(BgL_yz00_106);
					{
						obj_t BgL_resultz00_4453;
						obj_t BgL_za7za7_4454;

						BgL_resultz00_4453 = BgL_g1054z00_2171;
						BgL_za7za7_4454 = BgL_g1055z00_2172;
					BgL_loopz00_4452:
						if (PAIRP(BgL_za7za7_4454))
							{
								obj_t BgL_za7za7_9026;
								obj_t BgL_resultz00_9023;

								BgL_resultz00_9023 =
									BGl_2zf2zf2zz__r4_numbers_6_5z00(BgL_resultz00_4453,
									CAR(BgL_za7za7_4454));
								BgL_za7za7_9026 = CDR(BgL_za7za7_4454);
								BgL_za7za7_4454 = BgL_za7za7_9026;
								BgL_resultz00_4453 = BgL_resultz00_9023;
								goto BgL_loopz00_4452;
							}
						else
							{	/* Ieee/number.scm 904 */
								return BgL_resultz00_4453;
							}
					}
				}
			else
				{	/* Ieee/number.scm 901 */
					BGL_TAIL return
						BGl_2zf2zf2zz__r4_numbers_6_5z00(BINT(1L), BgL_xz00_105);
				}
		}

	}



/* &/ */
	obj_t BGl_z62zf2z90zz__r4_numbers_6_5z00(obj_t BgL_envz00_5078,
		obj_t BgL_xz00_5079, obj_t BgL_yz00_5080)
	{
		{	/* Ieee/number.scm 900 */
			return BGl_zf2zf2zz__r4_numbers_6_5z00(BgL_xz00_5079, BgL_yz00_5080);
		}

	}



/* abs */
	BGL_EXPORTED_DEF obj_t BGl_absz00zz__r4_numbers_6_5z00(obj_t BgL_xz00_107)
	{
		{	/* Ieee/number.scm 913 */
			if (INTEGERP(BgL_xz00_107))
				{	/* Ieee/number.scm 915 */
					if (((long) CINT(BgL_xz00_107) == BGL_LONG_MIN))
						{	/* Ieee/number.scm 916 */
							return
								bgl_bignum_neg(bgl_long_to_bignum((long) CINT(BgL_xz00_107)));
						}
					else
						{	/* Ieee/number.scm 918 */
							long BgL_nz00_4469;

							BgL_nz00_4469 = (long) CINT(BgL_xz00_107);
							if ((BgL_nz00_4469 < 0L))
								{	/* Ieee/number.scm 918 */
									return BINT(NEG(BgL_nz00_4469));
								}
							else
								{	/* Ieee/number.scm 918 */
									return BINT(BgL_nz00_4469);
								}
						}
				}
			else
				{	/* Ieee/number.scm 915 */
					if (REALP(BgL_xz00_107))
						{	/* Ieee/number.scm 919 */
							return DOUBLE_TO_REAL(fabs(REAL_TO_DOUBLE(BgL_xz00_107)));
						}
					else
						{	/* Ieee/number.scm 919 */
							if (ELONGP(BgL_xz00_107))
								{	/* Ieee/number.scm 922 */
									bool_t BgL_test4174z00_9052;

									{	/* Ieee/number.scm 922 */
										long BgL_n1z00_4474;

										BgL_n1z00_4474 = BELONG_TO_LONG(BgL_xz00_107);
										BgL_test4174z00_9052 = (BgL_n1z00_4474 == LONG_MIN);
									}
									if (BgL_test4174z00_9052)
										{	/* Ieee/number.scm 923 */
											obj_t BgL_tmpz00_9055;

											{	/* Ieee/number.scm 923 */
												long BgL_xz00_4476;

												BgL_xz00_4476 = BELONG_TO_LONG(BgL_xz00_107);
												BgL_tmpz00_9055 = bgl_long_to_bignum(BgL_xz00_4476);
											}
											return bgl_bignum_neg(BgL_tmpz00_9055);
										}
									else
										{	/* Ieee/number.scm 924 */
											long BgL_nz00_4478;

											BgL_nz00_4478 = BELONG_TO_LONG(BgL_xz00_107);
											if ((BgL_nz00_4478 < ((long) 0)))
												{	/* Ieee/number.scm 924 */
													long BgL_tmpz00_9062;

													BgL_tmpz00_9062 = NEG(BgL_nz00_4478);
													return make_belong(BgL_tmpz00_9062);
												}
											else
												{	/* Ieee/number.scm 924 */
													return make_belong(BgL_nz00_4478);
												}
										}
								}
							else
								{	/* Ieee/number.scm 921 */
									if (LLONGP(BgL_xz00_107))
										{	/* Ieee/number.scm 925 */
											if ((BLLONG_TO_LLONG(BgL_xz00_107) == BGL_LONGLONG_MIN))
												{	/* Ieee/number.scm 926 */
													return
														bgl_bignum_neg(bgl_llong_to_bignum(BLLONG_TO_LLONG
															(BgL_xz00_107)));
												}
											else
												{	/* Ieee/number.scm 928 */
													BGL_LONGLONG_T BgL_nz00_4487;

													BgL_nz00_4487 = BLLONG_TO_LLONG(BgL_xz00_107);
													if ((BgL_nz00_4487 < ((BGL_LONGLONG_T) 0)))
														{	/* Ieee/number.scm 928 */
															return make_bllong(NEG(BgL_nz00_4487));
														}
													else
														{	/* Ieee/number.scm 928 */
															return make_bllong(BgL_nz00_4487);
														}
												}
										}
									else
										{	/* Ieee/number.scm 925 */
											if (BIGNUMP(BgL_xz00_107))
												{	/* Ieee/number.scm 929 */
													return bgl_bignum_abs(BgL_xz00_107);
												}
											else
												{	/* Ieee/number.scm 929 */
													return
														BGl_errorz00zz__errorz00
														(BGl_string3343z00zz__r4_numbers_6_5z00,
														BGl_string3328z00zz__r4_numbers_6_5z00,
														BgL_xz00_107);
												}
										}
								}
						}
				}
		}

	}



/* &abs */
	obj_t BGl_z62absz62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5081,
		obj_t BgL_xz00_5082)
	{
		{	/* Ieee/number.scm 913 */
			return BGl_absz00zz__r4_numbers_6_5z00(BgL_xz00_5082);
		}

	}



/* floor */
	BGL_EXPORTED_DEF obj_t BGl_floorz00zz__r4_numbers_6_5z00(obj_t BgL_xz00_108)
	{
		{	/* Ieee/number.scm 937 */
			if (INTEGERP(BgL_xz00_108))
				{	/* Ieee/number.scm 939 */
					return BgL_xz00_108;
				}
			else
				{	/* Ieee/number.scm 939 */
					if (REALP(BgL_xz00_108))
						{	/* Ieee/number.scm 940 */
							double BgL_rz00_4493;

							BgL_rz00_4493 = REAL_TO_DOUBLE(BgL_xz00_108);
							return DOUBLE_TO_REAL(floor(BgL_rz00_4493));
						}
					else
						{	/* Ieee/number.scm 940 */
							if (ELONGP(BgL_xz00_108))
								{	/* Ieee/number.scm 941 */
									return BgL_xz00_108;
								}
							else
								{	/* Ieee/number.scm 941 */
									if (LLONGP(BgL_xz00_108))
										{	/* Ieee/number.scm 942 */
											return BgL_xz00_108;
										}
									else
										{	/* Ieee/number.scm 942 */
											if (BIGNUMP(BgL_xz00_108))
												{	/* Ieee/number.scm 943 */
													return BgL_xz00_108;
												}
											else
												{	/* Ieee/number.scm 943 */
													return
														BGl_errorz00zz__errorz00
														(BGl_string3344z00zz__r4_numbers_6_5z00,
														BGl_string3328z00zz__r4_numbers_6_5z00,
														BgL_xz00_108);
												}
										}
								}
						}
				}
		}

	}



/* &floor */
	obj_t BGl_z62floorz62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5083,
		obj_t BgL_xz00_5084)
	{
		{	/* Ieee/number.scm 937 */
			return BGl_floorz00zz__r4_numbers_6_5z00(BgL_xz00_5084);
		}

	}



/* ceiling */
	BGL_EXPORTED_DEF obj_t BGl_ceilingz00zz__r4_numbers_6_5z00(obj_t BgL_xz00_109)
	{
		{	/* Ieee/number.scm 949 */
			if (INTEGERP(BgL_xz00_109))
				{	/* Ieee/number.scm 951 */
					return BgL_xz00_109;
				}
			else
				{	/* Ieee/number.scm 951 */
					if (REALP(BgL_xz00_109))
						{	/* Ieee/number.scm 952 */
							double BgL_rz00_4494;

							BgL_rz00_4494 = REAL_TO_DOUBLE(BgL_xz00_109);
							return DOUBLE_TO_REAL(ceil(BgL_rz00_4494));
						}
					else
						{	/* Ieee/number.scm 952 */
							if (ELONGP(BgL_xz00_109))
								{	/* Ieee/number.scm 953 */
									return BgL_xz00_109;
								}
							else
								{	/* Ieee/number.scm 953 */
									if (LLONGP(BgL_xz00_109))
										{	/* Ieee/number.scm 954 */
											return BgL_xz00_109;
										}
									else
										{	/* Ieee/number.scm 954 */
											if (BIGNUMP(BgL_xz00_109))
												{	/* Ieee/number.scm 955 */
													return BgL_xz00_109;
												}
											else
												{	/* Ieee/number.scm 955 */
													return
														BGl_errorz00zz__errorz00
														(BGl_string3345z00zz__r4_numbers_6_5z00,
														BGl_string3328z00zz__r4_numbers_6_5z00,
														BgL_xz00_109);
												}
										}
								}
						}
				}
		}

	}



/* &ceiling */
	obj_t BGl_z62ceilingz62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5085,
		obj_t BgL_xz00_5086)
	{
		{	/* Ieee/number.scm 949 */
			return BGl_ceilingz00zz__r4_numbers_6_5z00(BgL_xz00_5086);
		}

	}



/* truncate */
	BGL_EXPORTED_DEF obj_t BGl_truncatez00zz__r4_numbers_6_5z00(obj_t
		BgL_xz00_110)
	{
		{	/* Ieee/number.scm 961 */
			if (INTEGERP(BgL_xz00_110))
				{	/* Ieee/number.scm 963 */
					return BgL_xz00_110;
				}
			else
				{	/* Ieee/number.scm 963 */
					if (REALP(BgL_xz00_110))
						{	/* Ieee/number.scm 964 */
							double BgL_rz00_4495;

							BgL_rz00_4495 = REAL_TO_DOUBLE(BgL_xz00_110);
							if ((BgL_rz00_4495 < ((double) 0.0)))
								{	/* Ieee/number.scm 964 */
									return DOUBLE_TO_REAL(ceil(BgL_rz00_4495));
								}
							else
								{	/* Ieee/number.scm 964 */
									return DOUBLE_TO_REAL(floor(BgL_rz00_4495));
								}
						}
					else
						{	/* Ieee/number.scm 964 */
							if (ELONGP(BgL_xz00_110))
								{	/* Ieee/number.scm 965 */
									return BgL_xz00_110;
								}
							else
								{	/* Ieee/number.scm 965 */
									if (LLONGP(BgL_xz00_110))
										{	/* Ieee/number.scm 966 */
											return BgL_xz00_110;
										}
									else
										{	/* Ieee/number.scm 966 */
											return
												BGl_errorz00zz__errorz00
												(BGl_string3346z00zz__r4_numbers_6_5z00,
												BGl_string3328z00zz__r4_numbers_6_5z00, BgL_xz00_110);
										}
								}
						}
				}
		}

	}



/* &truncate */
	obj_t BGl_z62truncatez62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5087,
		obj_t BgL_xz00_5088)
	{
		{	/* Ieee/number.scm 961 */
			return BGl_truncatez00zz__r4_numbers_6_5z00(BgL_xz00_5088);
		}

	}



/* round */
	BGL_EXPORTED_DEF obj_t BGl_roundz00zz__r4_numbers_6_5z00(obj_t BgL_xz00_111)
	{
		{	/* Ieee/number.scm 972 */
			if (INTEGERP(BgL_xz00_111))
				{	/* Ieee/number.scm 974 */
					return BgL_xz00_111;
				}
			else
				{	/* Ieee/number.scm 974 */
					if (REALP(BgL_xz00_111))
						{	/* Ieee/number.scm 975 */
							return
								DOUBLE_TO_REAL(BGl_roundflz00zz__r4_numbers_6_5_flonumz00
								(REAL_TO_DOUBLE(BgL_xz00_111)));
						}
					else
						{	/* Ieee/number.scm 975 */
							if (ELONGP(BgL_xz00_111))
								{	/* Ieee/number.scm 976 */
									return BgL_xz00_111;
								}
							else
								{	/* Ieee/number.scm 976 */
									if (LLONGP(BgL_xz00_111))
										{	/* Ieee/number.scm 977 */
											return BgL_xz00_111;
										}
									else
										{	/* Ieee/number.scm 977 */
											if (BIGNUMP(BgL_xz00_111))
												{	/* Ieee/number.scm 978 */
													return BgL_xz00_111;
												}
											else
												{	/* Ieee/number.scm 978 */
													return
														BGl_errorz00zz__errorz00
														(BGl_string3347z00zz__r4_numbers_6_5z00,
														BGl_string3328z00zz__r4_numbers_6_5z00,
														BgL_xz00_111);
												}
										}
								}
						}
				}
		}

	}



/* &round */
	obj_t BGl_z62roundz62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5089,
		obj_t BgL_xz00_5090)
	{
		{	/* Ieee/number.scm 972 */
			return BGl_roundz00zz__r4_numbers_6_5z00(BgL_xz00_5090);
		}

	}



/* exp */
	BGL_EXPORTED_DEF double BGl_expz00zz__r4_numbers_6_5z00(obj_t BgL_xz00_112)
	{
		{	/* Ieee/number.scm 984 */
			if (REALP(BgL_xz00_112))
				{	/* Ieee/number.scm 986 */
					return exp(REAL_TO_DOUBLE(BgL_xz00_112));
				}
			else
				{	/* Ieee/number.scm 986 */
					if (INTEGERP(BgL_xz00_112))
						{	/* Ieee/number.scm 987 */
							return exp((double) ((long) CINT(BgL_xz00_112)));
						}
					else
						{	/* Ieee/number.scm 987 */
							if (ELONGP(BgL_xz00_112))
								{	/* Ieee/number.scm 988 */
									return exp((double) (BELONG_TO_LONG(BgL_xz00_112)));
								}
							else
								{	/* Ieee/number.scm 988 */
									if (LLONGP(BgL_xz00_112))
										{	/* Ieee/number.scm 989 */
											return exp((double) (BLLONG_TO_LLONG(BgL_xz00_112)));
										}
									else
										{	/* Ieee/number.scm 989 */
											if (BIGNUMP(BgL_xz00_112))
												{	/* Ieee/number.scm 990 */
													return exp(bgl_bignum_to_flonum(BgL_xz00_112));
												}
											else
												{	/* Ieee/number.scm 990 */
													return
														REAL_TO_DOUBLE(BGl_errorz00zz__errorz00
														(BGl_string3348z00zz__r4_numbers_6_5z00,
															BGl_string3328z00zz__r4_numbers_6_5z00,
															BgL_xz00_112));
												}
										}
								}
						}
				}
		}

	}



/* &exp */
	obj_t BGl_z62expz62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5091,
		obj_t BgL_xz00_5092)
	{
		{	/* Ieee/number.scm 984 */
			return DOUBLE_TO_REAL(BGl_expz00zz__r4_numbers_6_5z00(BgL_xz00_5092));
		}

	}



/* log */
	BGL_EXPORTED_DEF double BGl_logz00zz__r4_numbers_6_5z00(obj_t BgL_xz00_113)
	{
		{	/* Ieee/number.scm 996 */
			if (REALP(BgL_xz00_113))
				{	/* Ieee/number.scm 998 */
					return log(REAL_TO_DOUBLE(BgL_xz00_113));
				}
			else
				{	/* Ieee/number.scm 998 */
					if (INTEGERP(BgL_xz00_113))
						{	/* Ieee/number.scm 999 */
							return log((double) ((long) CINT(BgL_xz00_113)));
						}
					else
						{	/* Ieee/number.scm 999 */
							if (ELONGP(BgL_xz00_113))
								{	/* Ieee/number.scm 1000 */
									return log((double) (BELONG_TO_LONG(BgL_xz00_113)));
								}
							else
								{	/* Ieee/number.scm 1000 */
									if (LLONGP(BgL_xz00_113))
										{	/* Ieee/number.scm 1001 */
											return log((double) (BLLONG_TO_LLONG(BgL_xz00_113)));
										}
									else
										{	/* Ieee/number.scm 1001 */
											if (BIGNUMP(BgL_xz00_113))
												{	/* Ieee/number.scm 1002 */
													return log(bgl_bignum_to_flonum(BgL_xz00_113));
												}
											else
												{	/* Ieee/number.scm 1002 */
													return
														REAL_TO_DOUBLE(BGl_errorz00zz__errorz00
														(BGl_string3349z00zz__r4_numbers_6_5z00,
															BGl_string3328z00zz__r4_numbers_6_5z00,
															BgL_xz00_113));
												}
										}
								}
						}
				}
		}

	}



/* &log */
	obj_t BGl_z62logz62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5093,
		obj_t BgL_xz00_5094)
	{
		{	/* Ieee/number.scm 996 */
			return DOUBLE_TO_REAL(BGl_logz00zz__r4_numbers_6_5z00(BgL_xz00_5094));
		}

	}



/* log2 */
	BGL_EXPORTED_DEF double BGl_log2z00zz__r4_numbers_6_5z00(obj_t BgL_xz00_114)
	{
		{	/* Ieee/number.scm 1008 */
			if (REALP(BgL_xz00_114))
				{	/* Ieee/number.scm 1010 */
					return log2(REAL_TO_DOUBLE(BgL_xz00_114));
				}
			else
				{	/* Ieee/number.scm 1010 */
					if (INTEGERP(BgL_xz00_114))
						{	/* Ieee/number.scm 1011 */
							return log2((double) ((long) CINT(BgL_xz00_114)));
						}
					else
						{	/* Ieee/number.scm 1011 */
							if (ELONGP(BgL_xz00_114))
								{	/* Ieee/number.scm 1012 */
									return log2((double) (BELONG_TO_LONG(BgL_xz00_114)));
								}
							else
								{	/* Ieee/number.scm 1012 */
									if (LLONGP(BgL_xz00_114))
										{	/* Ieee/number.scm 1013 */
											return log2((double) (BLLONG_TO_LLONG(BgL_xz00_114)));
										}
									else
										{	/* Ieee/number.scm 1013 */
											if (BIGNUMP(BgL_xz00_114))
												{	/* Ieee/number.scm 1014 */
													return log2(bgl_bignum_to_flonum(BgL_xz00_114));
												}
											else
												{	/* Ieee/number.scm 1014 */
													return
														REAL_TO_DOUBLE(BGl_errorz00zz__errorz00
														(BGl_string3350z00zz__r4_numbers_6_5z00,
															BGl_string3328z00zz__r4_numbers_6_5z00,
															BgL_xz00_114));
												}
										}
								}
						}
				}
		}

	}



/* &log2 */
	obj_t BGl_z62log2z62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5095,
		obj_t BgL_xz00_5096)
	{
		{	/* Ieee/number.scm 1008 */
			return DOUBLE_TO_REAL(BGl_log2z00zz__r4_numbers_6_5z00(BgL_xz00_5096));
		}

	}



/* log10 */
	BGL_EXPORTED_DEF double BGl_log10z00zz__r4_numbers_6_5z00(obj_t BgL_xz00_115)
	{
		{	/* Ieee/number.scm 1020 */
			if (REALP(BgL_xz00_115))
				{	/* Ieee/number.scm 1022 */
					return log10(REAL_TO_DOUBLE(BgL_xz00_115));
				}
			else
				{	/* Ieee/number.scm 1022 */
					if (INTEGERP(BgL_xz00_115))
						{	/* Ieee/number.scm 1023 */
							return log10((double) ((long) CINT(BgL_xz00_115)));
						}
					else
						{	/* Ieee/number.scm 1023 */
							if (ELONGP(BgL_xz00_115))
								{	/* Ieee/number.scm 1024 */
									return log10((double) (BELONG_TO_LONG(BgL_xz00_115)));
								}
							else
								{	/* Ieee/number.scm 1024 */
									if (LLONGP(BgL_xz00_115))
										{	/* Ieee/number.scm 1025 */
											return log10((double) (BLLONG_TO_LLONG(BgL_xz00_115)));
										}
									else
										{	/* Ieee/number.scm 1025 */
											if (BIGNUMP(BgL_xz00_115))
												{	/* Ieee/number.scm 1026 */
													return log10(bgl_bignum_to_flonum(BgL_xz00_115));
												}
											else
												{	/* Ieee/number.scm 1026 */
													return
														REAL_TO_DOUBLE(BGl_errorz00zz__errorz00
														(BGl_string3351z00zz__r4_numbers_6_5z00,
															BGl_string3328z00zz__r4_numbers_6_5z00,
															BgL_xz00_115));
												}
										}
								}
						}
				}
		}

	}



/* &log10 */
	obj_t BGl_z62log10z62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5097,
		obj_t BgL_xz00_5098)
	{
		{	/* Ieee/number.scm 1020 */
			return DOUBLE_TO_REAL(BGl_log10z00zz__r4_numbers_6_5z00(BgL_xz00_5098));
		}

	}



/* sin */
	BGL_EXPORTED_DEF double BGl_sinz00zz__r4_numbers_6_5z00(obj_t BgL_xz00_116)
	{
		{	/* Ieee/number.scm 1032 */
			if (REALP(BgL_xz00_116))
				{	/* Ieee/number.scm 1034 */
					return sin(REAL_TO_DOUBLE(BgL_xz00_116));
				}
			else
				{	/* Ieee/number.scm 1034 */
					if (INTEGERP(BgL_xz00_116))
						{	/* Ieee/number.scm 1035 */
							return sin((double) ((long) CINT(BgL_xz00_116)));
						}
					else
						{	/* Ieee/number.scm 1035 */
							if (ELONGP(BgL_xz00_116))
								{	/* Ieee/number.scm 1036 */
									return sin((double) (BELONG_TO_LONG(BgL_xz00_116)));
								}
							else
								{	/* Ieee/number.scm 1036 */
									if (LLONGP(BgL_xz00_116))
										{	/* Ieee/number.scm 1037 */
											return sin((double) (BLLONG_TO_LLONG(BgL_xz00_116)));
										}
									else
										{	/* Ieee/number.scm 1037 */
											if (BIGNUMP(BgL_xz00_116))
												{	/* Ieee/number.scm 1038 */
													return sin(bgl_bignum_to_flonum(BgL_xz00_116));
												}
											else
												{	/* Ieee/number.scm 1038 */
													return
														REAL_TO_DOUBLE(BGl_errorz00zz__errorz00
														(BGl_string3352z00zz__r4_numbers_6_5z00,
															BGl_string3328z00zz__r4_numbers_6_5z00,
															BgL_xz00_116));
												}
										}
								}
						}
				}
		}

	}



/* &sin */
	obj_t BGl_z62sinz62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5099,
		obj_t BgL_xz00_5100)
	{
		{	/* Ieee/number.scm 1032 */
			return DOUBLE_TO_REAL(BGl_sinz00zz__r4_numbers_6_5z00(BgL_xz00_5100));
		}

	}



/* cos */
	BGL_EXPORTED_DEF double BGl_cosz00zz__r4_numbers_6_5z00(obj_t BgL_xz00_117)
	{
		{	/* Ieee/number.scm 1044 */
			if (REALP(BgL_xz00_117))
				{	/* Ieee/number.scm 1046 */
					return cos(REAL_TO_DOUBLE(BgL_xz00_117));
				}
			else
				{	/* Ieee/number.scm 1046 */
					if (INTEGERP(BgL_xz00_117))
						{	/* Ieee/number.scm 1047 */
							return cos((double) ((long) CINT(BgL_xz00_117)));
						}
					else
						{	/* Ieee/number.scm 1047 */
							if (ELONGP(BgL_xz00_117))
								{	/* Ieee/number.scm 1048 */
									return cos((double) (BELONG_TO_LONG(BgL_xz00_117)));
								}
							else
								{	/* Ieee/number.scm 1048 */
									if (LLONGP(BgL_xz00_117))
										{	/* Ieee/number.scm 1049 */
											return cos((double) (BLLONG_TO_LLONG(BgL_xz00_117)));
										}
									else
										{	/* Ieee/number.scm 1049 */
											if (BIGNUMP(BgL_xz00_117))
												{	/* Ieee/number.scm 1050 */
													return cos(bgl_bignum_to_flonum(BgL_xz00_117));
												}
											else
												{	/* Ieee/number.scm 1050 */
													return
														REAL_TO_DOUBLE(BGl_errorz00zz__errorz00
														(BGl_string3353z00zz__r4_numbers_6_5z00,
															BGl_string3328z00zz__r4_numbers_6_5z00,
															BgL_xz00_117));
												}
										}
								}
						}
				}
		}

	}



/* &cos */
	obj_t BGl_z62cosz62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5101,
		obj_t BgL_xz00_5102)
	{
		{	/* Ieee/number.scm 1044 */
			return DOUBLE_TO_REAL(BGl_cosz00zz__r4_numbers_6_5z00(BgL_xz00_5102));
		}

	}



/* tan */
	BGL_EXPORTED_DEF double BGl_tanz00zz__r4_numbers_6_5z00(obj_t BgL_xz00_118)
	{
		{	/* Ieee/number.scm 1056 */
			if (REALP(BgL_xz00_118))
				{	/* Ieee/number.scm 1058 */
					return tan(REAL_TO_DOUBLE(BgL_xz00_118));
				}
			else
				{	/* Ieee/number.scm 1058 */
					if (INTEGERP(BgL_xz00_118))
						{	/* Ieee/number.scm 1059 */
							return tan((double) ((long) CINT(BgL_xz00_118)));
						}
					else
						{	/* Ieee/number.scm 1059 */
							if (ELONGP(BgL_xz00_118))
								{	/* Ieee/number.scm 1060 */
									return tan((double) (BELONG_TO_LONG(BgL_xz00_118)));
								}
							else
								{	/* Ieee/number.scm 1060 */
									if (LLONGP(BgL_xz00_118))
										{	/* Ieee/number.scm 1061 */
											return tan((double) (BLLONG_TO_LLONG(BgL_xz00_118)));
										}
									else
										{	/* Ieee/number.scm 1061 */
											if (BIGNUMP(BgL_xz00_118))
												{	/* Ieee/number.scm 1062 */
													return tan(bgl_bignum_to_flonum(BgL_xz00_118));
												}
											else
												{	/* Ieee/number.scm 1062 */
													return
														REAL_TO_DOUBLE(BGl_errorz00zz__errorz00
														(BGl_string3354z00zz__r4_numbers_6_5z00,
															BGl_string3328z00zz__r4_numbers_6_5z00,
															BgL_xz00_118));
												}
										}
								}
						}
				}
		}

	}



/* &tan */
	obj_t BGl_z62tanz62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5103,
		obj_t BgL_xz00_5104)
	{
		{	/* Ieee/number.scm 1056 */
			return DOUBLE_TO_REAL(BGl_tanz00zz__r4_numbers_6_5z00(BgL_xz00_5104));
		}

	}



/* asin */
	BGL_EXPORTED_DEF double BGl_asinz00zz__r4_numbers_6_5z00(obj_t BgL_xz00_119)
	{
		{	/* Ieee/number.scm 1068 */
			if (REALP(BgL_xz00_119))
				{	/* Ieee/number.scm 1070 */
					return asin(REAL_TO_DOUBLE(BgL_xz00_119));
				}
			else
				{	/* Ieee/number.scm 1070 */
					if (INTEGERP(BgL_xz00_119))
						{	/* Ieee/number.scm 1071 */
							return asin((double) ((long) CINT(BgL_xz00_119)));
						}
					else
						{	/* Ieee/number.scm 1071 */
							if (ELONGP(BgL_xz00_119))
								{	/* Ieee/number.scm 1072 */
									return asin((double) (BELONG_TO_LONG(BgL_xz00_119)));
								}
							else
								{	/* Ieee/number.scm 1072 */
									if (LLONGP(BgL_xz00_119))
										{	/* Ieee/number.scm 1073 */
											return asin((double) (BLLONG_TO_LLONG(BgL_xz00_119)));
										}
									else
										{	/* Ieee/number.scm 1073 */
											if (BIGNUMP(BgL_xz00_119))
												{	/* Ieee/number.scm 1074 */
													return asin(bgl_bignum_to_flonum(BgL_xz00_119));
												}
											else
												{	/* Ieee/number.scm 1074 */
													return
														REAL_TO_DOUBLE(BGl_errorz00zz__errorz00
														(BGl_string3355z00zz__r4_numbers_6_5z00,
															BGl_string3328z00zz__r4_numbers_6_5z00,
															BgL_xz00_119));
												}
										}
								}
						}
				}
		}

	}



/* &asin */
	obj_t BGl_z62asinz62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5105,
		obj_t BgL_xz00_5106)
	{
		{	/* Ieee/number.scm 1068 */
			return DOUBLE_TO_REAL(BGl_asinz00zz__r4_numbers_6_5z00(BgL_xz00_5106));
		}

	}



/* acos */
	BGL_EXPORTED_DEF double BGl_acosz00zz__r4_numbers_6_5z00(obj_t BgL_xz00_120)
	{
		{	/* Ieee/number.scm 1080 */
			if (REALP(BgL_xz00_120))
				{	/* Ieee/number.scm 1082 */
					return acos(REAL_TO_DOUBLE(BgL_xz00_120));
				}
			else
				{	/* Ieee/number.scm 1082 */
					if (INTEGERP(BgL_xz00_120))
						{	/* Ieee/number.scm 1083 */
							return acos((double) ((long) CINT(BgL_xz00_120)));
						}
					else
						{	/* Ieee/number.scm 1083 */
							if (ELONGP(BgL_xz00_120))
								{	/* Ieee/number.scm 1084 */
									return acos((double) (BELONG_TO_LONG(BgL_xz00_120)));
								}
							else
								{	/* Ieee/number.scm 1084 */
									if (LLONGP(BgL_xz00_120))
										{	/* Ieee/number.scm 1085 */
											return acos((double) (BLLONG_TO_LLONG(BgL_xz00_120)));
										}
									else
										{	/* Ieee/number.scm 1085 */
											if (BIGNUMP(BgL_xz00_120))
												{	/* Ieee/number.scm 1086 */
													return acos(bgl_bignum_to_flonum(BgL_xz00_120));
												}
											else
												{	/* Ieee/number.scm 1086 */
													return
														REAL_TO_DOUBLE(BGl_errorz00zz__errorz00
														(BGl_string3356z00zz__r4_numbers_6_5z00,
															BGl_string3328z00zz__r4_numbers_6_5z00,
															BgL_xz00_120));
												}
										}
								}
						}
				}
		}

	}



/* &acos */
	obj_t BGl_z62acosz62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5107,
		obj_t BgL_xz00_5108)
	{
		{	/* Ieee/number.scm 1080 */
			return DOUBLE_TO_REAL(BGl_acosz00zz__r4_numbers_6_5z00(BgL_xz00_5108));
		}

	}



/* atan */
	BGL_EXPORTED_DEF double BGl_atanz00zz__r4_numbers_6_5z00(obj_t BgL_xz00_121,
		obj_t BgL_yz00_122)
	{
		{	/* Ieee/number.scm 1092 */
			{	/* Ieee/number.scm 1093 */
				obj_t BgL_yz00_2300;

				if (PAIRP(BgL_yz00_122))
					{	/* Ieee/number.scm 1094 */
						obj_t BgL_yz00_2316;

						BgL_yz00_2316 = CAR(BgL_yz00_122);
						if (INTEGERP(BgL_yz00_2316))
							{	/* Ieee/number.scm 1096 */
								BgL_yz00_2300 =
									DOUBLE_TO_REAL((double) ((long) CINT(BgL_yz00_2316)));
							}
						else
							{	/* Ieee/number.scm 1096 */
								if (REALP(BgL_yz00_2316))
									{	/* Ieee/number.scm 1097 */
										BgL_yz00_2300 = BgL_yz00_2316;
									}
								else
									{	/* Ieee/number.scm 1097 */
										BgL_yz00_2300 =
											BGl_errorz00zz__errorz00
											(BGl_string3357z00zz__r4_numbers_6_5z00,
											BGl_string3328z00zz__r4_numbers_6_5z00, BgL_yz00_2316);
									}
							}
					}
				else
					{	/* Ieee/number.scm 1093 */
						BgL_yz00_2300 = BFALSE;
					}
				if (REALP(BgL_xz00_121))
					{	/* Ieee/number.scm 1105 */
						if (BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_yz00_2300))
							{	/* Ieee/number.scm 1102 */
								double BgL_xz00_4559;
								double BgL_yz00_4560;

								BgL_xz00_4559 = REAL_TO_DOUBLE(BgL_xz00_121);
								BgL_yz00_4560 = REAL_TO_DOUBLE(BgL_yz00_2300);
								return atan2(BgL_xz00_4559, BgL_yz00_4560);
							}
						else
							{	/* Ieee/number.scm 1101 */
								return atan(REAL_TO_DOUBLE(BgL_xz00_121));
							}
					}
				else
					{	/* Ieee/number.scm 1105 */
						if (INTEGERP(BgL_xz00_121))
							{	/* Ieee/number.scm 1106 */
								double BgL_arg2664z00_2304;

								BgL_arg2664z00_2304 = (double) ((long) CINT(BgL_xz00_121));
								if (BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_yz00_2300))
									{	/* Ieee/number.scm 1102 */
										double BgL_yz00_4564;

										BgL_yz00_4564 = REAL_TO_DOUBLE(BgL_yz00_2300);
										return atan2(BgL_arg2664z00_2304, BgL_yz00_4564);
									}
								else
									{	/* Ieee/number.scm 1101 */
										return atan(BgL_arg2664z00_2304);
									}
							}
						else
							{	/* Ieee/number.scm 1106 */
								if (ELONGP(BgL_xz00_121))
									{	/* Ieee/number.scm 1107 */
										double BgL_arg2666z00_2306;

										BgL_arg2666z00_2306 =
											(double) (BELONG_TO_LONG(BgL_xz00_121));
										if (BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_yz00_2300))
											{	/* Ieee/number.scm 1102 */
												double BgL_yz00_4568;

												BgL_yz00_4568 = REAL_TO_DOUBLE(BgL_yz00_2300);
												return atan2(BgL_arg2666z00_2306, BgL_yz00_4568);
											}
										else
											{	/* Ieee/number.scm 1101 */
												return atan(BgL_arg2666z00_2306);
											}
									}
								else
									{	/* Ieee/number.scm 1107 */
										if (LLONGP(BgL_xz00_121))
											{	/* Ieee/number.scm 1108 */
												double BgL_arg2669z00_2308;

												BgL_arg2669z00_2308 =
													(double) (BLLONG_TO_LLONG(BgL_xz00_121));
												if (BGl_numberzf3zf3zz__r4_numbers_6_5z00
													(BgL_yz00_2300))
													{	/* Ieee/number.scm 1102 */
														double BgL_yz00_4572;

														BgL_yz00_4572 = REAL_TO_DOUBLE(BgL_yz00_2300);
														return atan2(BgL_arg2669z00_2308, BgL_yz00_4572);
													}
												else
													{	/* Ieee/number.scm 1101 */
														return atan(BgL_arg2669z00_2308);
													}
											}
										else
											{	/* Ieee/number.scm 1108 */
												if (BIGNUMP(BgL_xz00_121))
													{	/* Ieee/number.scm 1109 */
														double BgL_arg2672z00_2310;

														BgL_arg2672z00_2310 =
															bgl_bignum_to_flonum(BgL_xz00_121);
														if (BGl_numberzf3zf3zz__r4_numbers_6_5z00
															(BgL_yz00_2300))
															{	/* Ieee/number.scm 1102 */
																double BgL_yz00_4577;

																BgL_yz00_4577 = REAL_TO_DOUBLE(BgL_yz00_2300);
																return
																	atan2(BgL_arg2672z00_2310, BgL_yz00_4577);
															}
														else
															{	/* Ieee/number.scm 1101 */
																return atan(BgL_arg2672z00_2310);
															}
													}
												else
													{	/* Ieee/number.scm 1109 */
														return
															REAL_TO_DOUBLE(BGl_errorz00zz__errorz00
															(BGl_string3357z00zz__r4_numbers_6_5z00,
																BGl_string3328z00zz__r4_numbers_6_5z00,
																BgL_xz00_121));
													}
											}
									}
							}
					}
			}
		}

	}



/* &atan */
	obj_t BGl_z62atanz62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5109,
		obj_t BgL_xz00_5110, obj_t BgL_yz00_5111)
	{
		{	/* Ieee/number.scm 1092 */
			return
				DOUBLE_TO_REAL(BGl_atanz00zz__r4_numbers_6_5z00(BgL_xz00_5110,
					BgL_yz00_5111));
		}

	}



/* sqrt */
	BGL_EXPORTED_DEF double BGl_sqrtz00zz__r4_numbers_6_5z00(obj_t BgL_xz00_123)
	{
		{	/* Ieee/number.scm 1115 */
			if (INTEGERP(BgL_xz00_123))
				{	/* Ieee/number.scm 1117 */
					return sqrt((double) ((long) CINT(BgL_xz00_123)));
				}
			else
				{	/* Ieee/number.scm 1117 */
					if (REALP(BgL_xz00_123))
						{	/* Ieee/number.scm 1118 */
							return sqrt(REAL_TO_DOUBLE(BgL_xz00_123));
						}
					else
						{	/* Ieee/number.scm 1118 */
							if (ELONGP(BgL_xz00_123))
								{	/* Ieee/number.scm 1119 */
									return sqrt((double) (BELONG_TO_LONG(BgL_xz00_123)));
								}
							else
								{	/* Ieee/number.scm 1119 */
									if (LLONGP(BgL_xz00_123))
										{	/* Ieee/number.scm 1120 */
											return sqrt((double) (BLLONG_TO_LLONG(BgL_xz00_123)));
										}
									else
										{	/* Ieee/number.scm 1120 */
											if (BIGNUMP(BgL_xz00_123))
												{	/* Ieee/number.scm 1121 */
													return sqrt(bgl_bignum_to_flonum(BgL_xz00_123));
												}
											else
												{	/* Ieee/number.scm 1121 */
													return
														REAL_TO_DOUBLE(BGl_errorz00zz__errorz00
														(BGl_string3358z00zz__r4_numbers_6_5z00,
															BGl_string3328z00zz__r4_numbers_6_5z00,
															BgL_xz00_123));
												}
										}
								}
						}
				}
		}

	}



/* &sqrt */
	obj_t BGl_z62sqrtz62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5112,
		obj_t BgL_xz00_5113)
	{
		{	/* Ieee/number.scm 1115 */
			return DOUBLE_TO_REAL(BGl_sqrtz00zz__r4_numbers_6_5z00(BgL_xz00_5113));
		}

	}



/* expt */
	BGL_EXPORTED_DEF obj_t BGl_exptz00zz__r4_numbers_6_5z00(obj_t BgL_xz00_124,
		obj_t BgL_yz00_125)
	{
		{	/* Ieee/number.scm 1127 */
			{	/* Ieee/number.scm 1129 */
				bool_t BgL_test4263z00_9476;

				if (REALP(BgL_xz00_124))
					{	/* Ieee/number.scm 1129 */
						if (REALP(BgL_yz00_125))
							{	/* Ieee/number.scm 1129 */
								if ((REAL_TO_DOUBLE(BgL_xz00_124) == ((double) 0.0)))
									{	/* Ieee/number.scm 1129 */
										BgL_test4263z00_9476 =
											(REAL_TO_DOUBLE(BgL_yz00_125) == ((double) 0.0));
									}
								else
									{	/* Ieee/number.scm 1129 */
										BgL_test4263z00_9476 = ((bool_t) 0);
									}
							}
						else
							{	/* Ieee/number.scm 1129 */
								BgL_test4263z00_9476 = ((bool_t) 0);
							}
					}
				else
					{	/* Ieee/number.scm 1129 */
						BgL_test4263z00_9476 = ((bool_t) 0);
					}
				if (BgL_test4263z00_9476)
					{	/* Ieee/number.scm 1129 */
						return BGL_REAL_CNST(BGl_real3359z00zz__r4_numbers_6_5z00);
					}
				else
					{	/* Ieee/number.scm 1131 */
						bool_t BgL_test4267z00_9486;

						if (INTEGERP(BgL_xz00_124))
							{	/* Ieee/number.scm 1131 */
								if (INTEGERP(BgL_yz00_125))
									{	/* Ieee/number.scm 1131 */
										BgL_test4267z00_9486 = ((long) CINT(BgL_yz00_125) >= 0L);
									}
								else
									{	/* Ieee/number.scm 1131 */
										BgL_test4267z00_9486 = ((bool_t) 0);
									}
							}
						else
							{	/* Ieee/number.scm 1131 */
								BgL_test4267z00_9486 = ((bool_t) 0);
							}
						if (BgL_test4267z00_9486)
							{	/* Ieee/number.scm 1131 */
								return
									BINT(BGl_exptfxz00zz__r4_numbers_6_5_fixnumz00(
										(long) CINT(BgL_xz00_124), (long) CINT(BgL_yz00_125)));
							}
						else
							{	/* Ieee/number.scm 1133 */
								bool_t BgL_test4270z00_9497;

								if (BIGNUMP(BgL_xz00_124))
									{	/* Ieee/number.scm 1133 */
										if (BIGNUMP(BgL_yz00_125))
											{	/* Ieee/number.scm 1133 */
												BgL_test4270z00_9497 = BXPOSITIVE(BgL_yz00_125);
											}
										else
											{	/* Ieee/number.scm 1133 */
												BgL_test4270z00_9497 = ((bool_t) 0);
											}
									}
								else
									{	/* Ieee/number.scm 1133 */
										BgL_test4270z00_9497 = ((bool_t) 0);
									}
								if (BgL_test4270z00_9497)
									{	/* Ieee/number.scm 1133 */
										return bgl_bignum_expt(BgL_xz00_124, BgL_yz00_125);
									}
								else
									{	/* Ieee/number.scm 1133 */
										if (BIGNUMP(BgL_xz00_124))
											{	/* Ieee/number.scm 1136 */
												obj_t BgL_y1z00_2339;

												if (REALP(BgL_yz00_125))
													{	/* Ieee/number.scm 1137 */
														BgL_y1z00_2339 =
															bgl_long_to_bignum(
															(long) (REAL_TO_DOUBLE(BgL_yz00_125)));
													}
												else
													{	/* Ieee/number.scm 1137 */
														if (INTEGERP(BgL_yz00_125))
															{	/* Ieee/number.scm 1138 */
																BgL_y1z00_2339 =
																	bgl_long_to_bignum((long) CINT(BgL_yz00_125));
															}
														else
															{	/* Ieee/number.scm 1138 */
																if (ELONGP(BgL_yz00_125))
																	{	/* Ieee/number.scm 1139 */
																		BgL_y1z00_2339 =
																			bgl_long_to_bignum(BELONG_TO_LONG
																			(BgL_yz00_125));
																	}
																else
																	{	/* Ieee/number.scm 1139 */
																		if (LLONGP(BgL_yz00_125))
																			{	/* Ieee/number.scm 1140 */
																				BgL_y1z00_2339 =
																					bgl_llong_to_bignum(BLLONG_TO_LLONG
																					(BgL_yz00_125));
																			}
																		else
																			{	/* Ieee/number.scm 1140 */
																				if (BIGNUMP(BgL_yz00_125))
																					{	/* Ieee/number.scm 1141 */
																						BgL_y1z00_2339 = BgL_yz00_125;
																					}
																				else
																					{	/* Ieee/number.scm 1141 */
																						BgL_y1z00_2339 =
																							BGl_errorz00zz__errorz00
																							(BGl_string3360z00zz__r4_numbers_6_5z00,
																							BGl_string3328z00zz__r4_numbers_6_5z00,
																							BgL_yz00_125);
																					}
																			}
																	}
															}
													}
												return
													bgl_bignum_expt(BgL_xz00_124,
													((obj_t) BgL_y1z00_2339));
											}
										else
											{	/* Ieee/number.scm 1145 */
												obj_t BgL_x1z00_2346;
												obj_t BgL_y1z00_2347;

												if (REALP(BgL_xz00_124))
													{	/* Ieee/number.scm 1146 */
														BgL_x1z00_2346 = BgL_xz00_124;
													}
												else
													{	/* Ieee/number.scm 1146 */
														if (INTEGERP(BgL_xz00_124))
															{	/* Ieee/number.scm 1147 */
																BgL_x1z00_2346 =
																	DOUBLE_TO_REAL(
																	(double) ((long) CINT(BgL_xz00_124)));
															}
														else
															{	/* Ieee/number.scm 1147 */
																if (ELONGP(BgL_xz00_124))
																	{	/* Ieee/number.scm 1148 */
																		BgL_x1z00_2346 =
																			DOUBLE_TO_REAL(
																			(double) (BELONG_TO_LONG(BgL_xz00_124)));
																	}
																else
																	{	/* Ieee/number.scm 1148 */
																		if (LLONGP(BgL_xz00_124))
																			{	/* Ieee/number.scm 1149 */
																				BgL_x1z00_2346 =
																					DOUBLE_TO_REAL(
																					(double) (BLLONG_TO_LLONG
																						(BgL_xz00_124)));
																			}
																		else
																			{	/* Ieee/number.scm 1149 */
																				if (BIGNUMP(BgL_xz00_124))
																					{	/* Ieee/number.scm 1150 */
																						BgL_x1z00_2346 =
																							DOUBLE_TO_REAL
																							(bgl_bignum_to_flonum
																							(BgL_xz00_124));
																					}
																				else
																					{	/* Ieee/number.scm 1150 */
																						BgL_x1z00_2346 =
																							BGl_errorz00zz__errorz00
																							(BGl_string3360z00zz__r4_numbers_6_5z00,
																							BGl_string3328z00zz__r4_numbers_6_5z00,
																							BgL_xz00_124);
																					}
																			}
																	}
															}
													}
												if (REALP(BgL_yz00_125))
													{	/* Ieee/number.scm 1153 */
														BgL_y1z00_2347 = BgL_yz00_125;
													}
												else
													{	/* Ieee/number.scm 1153 */
														if (INTEGERP(BgL_yz00_125))
															{	/* Ieee/number.scm 1154 */
																BgL_y1z00_2347 =
																	DOUBLE_TO_REAL(
																	(double) ((long) CINT(BgL_yz00_125)));
															}
														else
															{	/* Ieee/number.scm 1154 */
																if (ELONGP(BgL_yz00_125))
																	{	/* Ieee/number.scm 1155 */
																		BgL_y1z00_2347 =
																			DOUBLE_TO_REAL(
																			(double) (BELONG_TO_LONG(BgL_yz00_125)));
																	}
																else
																	{	/* Ieee/number.scm 1155 */
																		if (LLONGP(BgL_yz00_125))
																			{	/* Ieee/number.scm 1156 */
																				BgL_y1z00_2347 =
																					DOUBLE_TO_REAL(
																					(double) (BLLONG_TO_LLONG
																						(BgL_yz00_125)));
																			}
																		else
																			{	/* Ieee/number.scm 1156 */
																				if (BIGNUMP(BgL_yz00_125))
																					{	/* Ieee/number.scm 1157 */
																						BgL_y1z00_2347 =
																							DOUBLE_TO_REAL
																							(bgl_bignum_to_flonum
																							(BgL_yz00_125));
																					}
																				else
																					{	/* Ieee/number.scm 1157 */
																						BgL_y1z00_2347 =
																							BGl_errorz00zz__errorz00
																							(BGl_string3360z00zz__r4_numbers_6_5z00,
																							BGl_string3328z00zz__r4_numbers_6_5z00,
																							BgL_yz00_125);
																					}
																			}
																	}
															}
													}
												{	/* Ieee/number.scm 1159 */
													double BgL_r1z00_4597;
													double BgL_r2z00_4598;

													BgL_r1z00_4597 = REAL_TO_DOUBLE(BgL_x1z00_2346);
													BgL_r2z00_4598 = REAL_TO_DOUBLE(BgL_y1z00_2347);
													return
														DOUBLE_TO_REAL(pow(BgL_r1z00_4597, BgL_r2z00_4598));
												}
											}
									}
							}
					}
			}
		}

	}



/* &expt */
	obj_t BGl_z62exptz62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5114,
		obj_t BgL_xz00_5115, obj_t BgL_yz00_5116)
	{
		{	/* Ieee/number.scm 1127 */
			return BGl_exptz00zz__r4_numbers_6_5z00(BgL_xz00_5115, BgL_yz00_5116);
		}

	}



/* exact->inexact */
	BGL_EXPORTED_DEF obj_t bgl_exact_to_inexact(obj_t BgL_za7za7_126)
	{
		{	/* Ieee/number.scm 1164 */
			if (INTEGERP(BgL_za7za7_126))
				{	/* Ieee/number.scm 1166 */
					return DOUBLE_TO_REAL((double) ((long) CINT(BgL_za7za7_126)));
				}
			else
				{	/* Ieee/number.scm 1166 */
					if (REALP(BgL_za7za7_126))
						{	/* Ieee/number.scm 1167 */
							return BgL_za7za7_126;
						}
					else
						{	/* Ieee/number.scm 1167 */
							if (ELONGP(BgL_za7za7_126))
								{	/* Ieee/number.scm 1168 */
									return
										DOUBLE_TO_REAL((double) (BELONG_TO_LONG(BgL_za7za7_126)));
								}
							else
								{	/* Ieee/number.scm 1168 */
									if (LLONGP(BgL_za7za7_126))
										{	/* Ieee/number.scm 1169 */
											return
												DOUBLE_TO_REAL(
												(double) (BLLONG_TO_LLONG(BgL_za7za7_126)));
										}
									else
										{	/* Ieee/number.scm 1169 */
											if (BIGNUMP(BgL_za7za7_126))
												{	/* Ieee/number.scm 1170 */
													return
														DOUBLE_TO_REAL(bgl_bignum_to_flonum
														(BgL_za7za7_126));
												}
											else
												{	/* Ieee/number.scm 1170 */
													return BgL_za7za7_126;
												}
										}
								}
						}
				}
		}

	}



/* &exact->inexact */
	obj_t BGl_z62exactzd2ze3inexactz53zz__r4_numbers_6_5z00(obj_t BgL_envz00_5117,
		obj_t BgL_za7za7_5118)
	{
		{	/* Ieee/number.scm 1164 */
			return bgl_exact_to_inexact(BgL_za7za7_5118);
		}

	}



/* inexact->exact */
	BGL_EXPORTED_DEF obj_t bgl_inexact_to_exact(obj_t BgL_za7za7_127)
	{
		{	/* Ieee/number.scm 1182 */
			if (REALP(BgL_za7za7_127))
				{	/* Ieee/number.scm 1184 */
					bool_t BgL_test4295z00_9601;

					{	/* Ieee/number.scm 1184 */
						bool_t BgL_test4296z00_9602;

						{	/* Ieee/number.scm 1184 */
							double BgL_r2z00_4601;

							BgL_r2z00_4601 = BGl_za2minintflza2z00zz__r4_numbers_6_5z00;
							BgL_test4296z00_9602 =
								(REAL_TO_DOUBLE(BgL_za7za7_127) >= BgL_r2z00_4601);
						}
						if (BgL_test4296z00_9602)
							{	/* Ieee/number.scm 1184 */
								double BgL_r2z00_4603;

								BgL_r2z00_4603 = BGl_za2maxintflza2z00zz__r4_numbers_6_5z00;
								BgL_test4295z00_9601 =
									(REAL_TO_DOUBLE(BgL_za7za7_127) <= BgL_r2z00_4603);
							}
						else
							{	/* Ieee/number.scm 1184 */
								BgL_test4295z00_9601 = ((bool_t) 0);
							}
					}
					if (BgL_test4295z00_9601)
						{	/* Ieee/number.scm 1184 */
							return BINT((long) (REAL_TO_DOUBLE(BgL_za7za7_127)));
						}
					else
						{	/* Ieee/number.scm 1184 */
							return bgl_flonum_to_bignum(REAL_TO_DOUBLE(BgL_za7za7_127));
						}
				}
			else
				{	/* Ieee/number.scm 1183 */
					return BgL_za7za7_127;
				}
		}

	}



/* &inexact->exact */
	obj_t BGl_z62inexactzd2ze3exactz53zz__r4_numbers_6_5z00(obj_t BgL_envz00_5119,
		obj_t BgL_za7za7_5120)
	{
		{	/* Ieee/number.scm 1182 */
			return bgl_inexact_to_exact(BgL_za7za7_5120);
		}

	}



/* _number->string */
	obj_t BGl__numberzd2ze3stringz31zz__r4_numbers_6_5z00(obj_t
		BgL_env1080z00_132, obj_t BgL_opt1079z00_131)
	{
		{	/* Ieee/number.scm 1197 */
			{	/* Ieee/number.scm 1197 */
				obj_t BgL_xz00_2375;

				BgL_xz00_2375 = VECTOR_REF(BgL_opt1079z00_131, 0L);
				switch (VECTOR_LENGTH(BgL_opt1079z00_131))
					{
					case 1L:

						{	/* Ieee/number.scm 1197 */

							return
								BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(BgL_xz00_2375,
								BINT(10L));
						}
						break;
					case 2L:

						{	/* Ieee/number.scm 1197 */
							obj_t BgL_radixz00_2379;

							BgL_radixz00_2379 = VECTOR_REF(BgL_opt1079z00_131, 1L);
							{	/* Ieee/number.scm 1197 */

								return
									BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(BgL_xz00_2375,
									BgL_radixz00_2379);
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* number->string */
	BGL_EXPORTED_DEF obj_t BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(obj_t
		BgL_xz00_129, obj_t BgL_radixz00_130)
	{
		{	/* Ieee/number.scm 1197 */
			if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BgL_radixz00_130))
				{	/* Ieee/number.scm 1199 */
					if (INTEGERP(BgL_xz00_129))
						{	/* Ieee/number.scm 1200 */
							return
								BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
								(long) CINT(BgL_xz00_129), (long) CINT(BgL_radixz00_130));
						}
					else
						{	/* Ieee/number.scm 1200 */
							if (REALP(BgL_xz00_129))
								{	/* Ieee/number.scm 1201 */
									return bgl_real_to_string(REAL_TO_DOUBLE(BgL_xz00_129));
								}
							else
								{	/* Ieee/number.scm 1201 */
									if (ELONGP(BgL_xz00_129))
										{	/* Ieee/number.scm 1202 */
											obj_t BgL_list2729z00_2384;

											BgL_list2729z00_2384 =
												MAKE_YOUNG_PAIR(BgL_radixz00_130, BNIL);
											return
												BGl_elongzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
												(BELONG_TO_LONG(BgL_xz00_129), BgL_list2729z00_2384);
										}
									else
										{	/* Ieee/number.scm 1202 */
											if (LLONGP(BgL_xz00_129))
												{	/* Ieee/number.scm 1203 */
													obj_t BgL_list2731z00_2386;

													BgL_list2731z00_2386 =
														MAKE_YOUNG_PAIR(BgL_radixz00_130, BNIL);
													return
														BGl_llongzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
														(BLLONG_TO_LLONG(BgL_xz00_129),
														BgL_list2731z00_2386);
												}
											else
												{	/* Ieee/number.scm 1203 */
													if (BIGNUMP(BgL_xz00_129))
														{	/* Ieee/number.scm 1204 */
															return
																BGl_bignumzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
																(BgL_xz00_129, (long) CINT(BgL_radixz00_130));
														}
													else
														{	/* Ieee/number.scm 1204 */
															if (BGL_INT8P(BgL_xz00_129))
																{	/* Ieee/number.scm 1205 */
																	long BgL_arg2734z00_2389;

																	{	/* Ieee/number.scm 1205 */
																		int8_t BgL_xz00_4604;

																		BgL_xz00_4604 =
																			BGL_BINT8_TO_INT8(BgL_xz00_129);
																		{	/* Ieee/number.scm 1205 */
																			long BgL_arg3251z00_4605;

																			BgL_arg3251z00_4605 =
																				(long) (BgL_xz00_4604);
																			BgL_arg2734z00_2389 =
																				(long) (BgL_arg3251z00_4605);
																	}}
																	return
																		BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
																		(BgL_arg2734z00_2389,
																		(long) CINT(BgL_radixz00_130));
																}
															else
																{	/* Ieee/number.scm 1205 */
																	if (BGL_UINT8P(BgL_xz00_129))
																		{	/* Ieee/number.scm 1206 */
																			long BgL_arg2736z00_2391;

																			{	/* Ieee/number.scm 1206 */
																				uint8_t BgL_xz00_4607;

																				BgL_xz00_4607 =
																					BGL_BUINT8_TO_UINT8(BgL_xz00_129);
																				{	/* Ieee/number.scm 1206 */
																					long BgL_arg3250z00_4608;

																					BgL_arg3250z00_4608 =
																						(long) (BgL_xz00_4607);
																					BgL_arg2736z00_2391 =
																						(long) (BgL_arg3250z00_4608);
																			}}
																			return
																				BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
																				(BgL_arg2736z00_2391,
																				(long) CINT(BgL_radixz00_130));
																		}
																	else
																		{	/* Ieee/number.scm 1206 */
																			if (BGL_INT16P(BgL_xz00_129))
																				{	/* Ieee/number.scm 1207 */
																					long BgL_arg2738z00_2393;

																					{	/* Ieee/number.scm 1207 */
																						int16_t BgL_xz00_4610;

																						BgL_xz00_4610 =
																							BGL_BINT16_TO_INT16(BgL_xz00_129);
																						{	/* Ieee/number.scm 1207 */
																							long BgL_arg3249z00_4611;

																							BgL_arg3249z00_4611 =
																								(long) (BgL_xz00_4610);
																							BgL_arg2738z00_2393 =
																								(long) (BgL_arg3249z00_4611);
																					}}
																					return
																						BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
																						(BgL_arg2738z00_2393,
																						(long) CINT(BgL_radixz00_130));
																				}
																			else
																				{	/* Ieee/number.scm 1207 */
																					if (BGL_UINT16P(BgL_xz00_129))
																						{	/* Ieee/number.scm 1208 */
																							long BgL_arg2740z00_2395;

																							{	/* Ieee/number.scm 1208 */
																								uint16_t BgL_xz00_4613;

																								BgL_xz00_4613 =
																									BGL_BUINT16_TO_UINT16
																									(BgL_xz00_129);
																								{	/* Ieee/number.scm 1208 */
																									long BgL_arg3247z00_4614;

																									BgL_arg3247z00_4614 =
																										(long) (BgL_xz00_4613);
																									BgL_arg2740z00_2395 =
																										(long)
																										(BgL_arg3247z00_4614);
																							}}
																							return
																								BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
																								(BgL_arg2740z00_2395,
																								(long) CINT(BgL_radixz00_130));
																						}
																					else
																						{	/* Ieee/number.scm 1208 */
																							if (BGL_INT32P(BgL_xz00_129))
																								{	/* Ieee/number.scm 1209 */
																									BGL_LONGLONG_T
																										BgL_arg2742z00_2397;
																									{	/* Ieee/number.scm 1209 */
																										int32_t BgL_nz00_4616;

																										BgL_nz00_4616 =
																											BGL_BINT32_TO_INT32
																											(BgL_xz00_129);
																										BgL_arg2742z00_2397 =
																											(BGL_LONGLONG_T)
																											(BgL_nz00_4616);
																									}
																									{	/* Ieee/number.scm 1209 */
																										obj_t BgL_list2743z00_2398;

																										BgL_list2743z00_2398 =
																											MAKE_YOUNG_PAIR
																											(BgL_radixz00_130, BNIL);
																										return
																											BGl_llongzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
																											(BgL_arg2742z00_2397,
																											BgL_list2743z00_2398);
																									}
																								}
																							else
																								{	/* Ieee/number.scm 1209 */
																									if (BGL_UINT32P(BgL_xz00_129))
																										{	/* Ieee/number.scm 1210 */
																											BGL_LONGLONG_T
																												BgL_arg2746z00_2400;
																											{	/* Ieee/number.scm 1210 */
																												uint32_t BgL_nz00_4617;

																												BgL_nz00_4617 =
																													BGL_BUINT32_TO_UINT32
																													(BgL_xz00_129);
																												BgL_arg2746z00_2400 =
																													(BGL_LONGLONG_T)
																													(BgL_nz00_4617);
																											}
																											{	/* Ieee/number.scm 1210 */
																												obj_t
																													BgL_list2747z00_2401;
																												BgL_list2747z00_2401 =
																													MAKE_YOUNG_PAIR
																													(BgL_radixz00_130,
																													BNIL);
																												return
																													BGl_llongzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
																													(BgL_arg2746z00_2400,
																													BgL_list2747z00_2401);
																											}
																										}
																									else
																										{	/* Ieee/number.scm 1210 */
																											if (BGL_INT64P
																												(BgL_xz00_129))
																												{	/* Ieee/number.scm 1211 */
																													BGL_LONGLONG_T
																														BgL_arg2749z00_2403;
																													{	/* Ieee/number.scm 1211 */
																														int64_t
																															BgL_nz00_4618;
																														BgL_nz00_4618 =
																															BGL_BINT64_TO_INT64
																															(BgL_xz00_129);
																														BgL_arg2749z00_2403
																															=
																															(BGL_LONGLONG_T)
																															(BgL_nz00_4618);
																													}
																													{	/* Ieee/number.scm 1211 */
																														obj_t
																															BgL_list2750z00_2404;
																														BgL_list2750z00_2404
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_radixz00_130,
																															BNIL);
																														return
																															BGl_llongzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
																															(BgL_arg2749z00_2403,
																															BgL_list2750z00_2404);
																													}
																												}
																											else
																												{	/* Ieee/number.scm 1211 */
																													if (BGL_UINT64P
																														(BgL_xz00_129))
																														{	/* Ieee/number.scm 1212 */
																															BGL_LONGLONG_T
																																BgL_arg2753z00_2406;
																															{	/* Ieee/number.scm 1212 */
																																uint64_t
																																	BgL_nz00_4619;
																																BgL_nz00_4619 =
																																	BGL_BINT64_TO_INT64
																																	(BgL_xz00_129);
																																BgL_arg2753z00_2406
																																	=
																																	(BGL_LONGLONG_T)
																																	(BgL_nz00_4619);
																															}
																															{	/* Ieee/number.scm 1212 */
																																obj_t
																																	BgL_list2754z00_2407;
																																BgL_list2754z00_2407
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_radixz00_130,
																																	BNIL);
																																return
																																	BGl_llongzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
																																	(BgL_arg2753z00_2406,
																																	BgL_list2754z00_2407);
																															}
																														}
																													else
																														{	/* Ieee/number.scm 1212 */
																															return
																																BGl_errorz00zz__errorz00
																																(BGl_string3361z00zz__r4_numbers_6_5z00,
																																BGl_string3362z00zz__r4_numbers_6_5z00,
																																BgL_xz00_129);
																														}
																												}
																										}
																								}
																						}
																				}
																		}
																}
														}
												}
										}
								}
						}
				}
			else
				{	/* Ieee/number.scm 1199 */
					return
						BGl_errorz00zz__errorz00(BGl_symbol3363z00zz__r4_numbers_6_5z00,
						BGl_string3364z00zz__r4_numbers_6_5z00, BgL_radixz00_130);
				}
		}

	}



/* _string->number */
	obj_t BGl__stringzd2ze3numberz31zz__r4_numbers_6_5z00(obj_t
		BgL_env1084z00_136, obj_t BgL_opt1083z00_135)
	{
		{	/* Ieee/number.scm 1218 */
			{	/* Ieee/number.scm 1218 */
				obj_t BgL_g1085z00_2408;

				BgL_g1085z00_2408 = VECTOR_REF(BgL_opt1083z00_135, 0L);
				switch (VECTOR_LENGTH(BgL_opt1083z00_135))
					{
					case 1L:

						{	/* Ieee/number.scm 1218 */

							{	/* Ieee/number.scm 1218 */
								obj_t BgL_auxz00_9700;

								if (STRINGP(BgL_g1085z00_2408))
									{	/* Ieee/number.scm 1218 */
										BgL_auxz00_9700 = BgL_g1085z00_2408;
									}
								else
									{
										obj_t BgL_auxz00_9703;

										BgL_auxz00_9703 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string3297z00zz__r4_numbers_6_5z00, BINT(45386L),
											BGl_string3365z00zz__r4_numbers_6_5z00,
											BGl_string3366z00zz__r4_numbers_6_5z00,
											BgL_g1085z00_2408);
										FAILURE(BgL_auxz00_9703, BFALSE, BFALSE);
									}
								return
									BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00
									(BgL_auxz00_9700, BINT(10L));
							}
						}
						break;
					case 2L:

						{	/* Ieee/number.scm 1218 */
							obj_t BgL_radixz00_2412;

							BgL_radixz00_2412 = VECTOR_REF(BgL_opt1083z00_135, 1L);
							{	/* Ieee/number.scm 1218 */

								{	/* Ieee/number.scm 1218 */
									obj_t BgL_auxz00_9710;

									if (STRINGP(BgL_g1085z00_2408))
										{	/* Ieee/number.scm 1218 */
											BgL_auxz00_9710 = BgL_g1085z00_2408;
										}
									else
										{
											obj_t BgL_auxz00_9713;

											BgL_auxz00_9713 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3297z00zz__r4_numbers_6_5z00, BINT(45386L),
												BGl_string3365z00zz__r4_numbers_6_5z00,
												BGl_string3366z00zz__r4_numbers_6_5z00,
												BgL_g1085z00_2408);
											FAILURE(BgL_auxz00_9713, BFALSE, BFALSE);
										}
									return
										BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00
										(BgL_auxz00_9710, BgL_radixz00_2412);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* string->number */
	BGL_EXPORTED_DEF obj_t BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00(obj_t
		BgL_xz00_133, obj_t BgL_radixz00_134)
	{
		{	/* Ieee/number.scm 1218 */
			{
				obj_t BgL_xz00_2504;
				obj_t BgL_xz00_2425;
				obj_t BgL_rz00_2426;

				if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BgL_radixz00_134))
					{	/* Ieee/number.scm 1287 */
						if ((STRING_LENGTH(BgL_xz00_133) == 0L))
							{	/* Ieee/number.scm 1289 */
								return BFALSE;
							}
						else
							{	/* Ieee/number.scm 1291 */
								bool_t BgL_test4315z00_9725;

								BgL_xz00_2425 = BgL_xz00_133;
								BgL_rz00_2426 = BgL_radixz00_134;
								{	/* Ieee/number.scm 1221 */
									long BgL_lenz00_2428;

									BgL_lenz00_2428 = STRING_LENGTH(BgL_xz00_2425);
									{
										long BgL_iz00_2431;

										BgL_iz00_2431 = (BgL_lenz00_2428 - 1L);
									BgL_zc3z04anonymousza32766ze3z87_2432:
										if ((-1L == BgL_iz00_2431))
											{	/* Ieee/number.scm 1223 */
												BgL_test4315z00_9725 = ((bool_t) 1);
											}
										else
											{	/* Ieee/number.scm 1225 */
												bool_t BgL_test4317z00_9729;

												if (
													(STRING_REF(BgL_xz00_2425,
															BgL_iz00_2431) >= ((unsigned char) '0')))
													{	/* Ieee/number.scm 1225 */
														if (
															(STRING_REF(BgL_xz00_2425,
																	BgL_iz00_2431) <= ((unsigned char) '1')))
															{	/* Ieee/number.scm 1226 */
																BgL_test4317z00_9729 =
																	((long) CINT(BgL_rz00_2426) >= 2L);
															}
														else
															{	/* Ieee/number.scm 1226 */
																BgL_test4317z00_9729 = ((bool_t) 0);
															}
													}
												else
													{	/* Ieee/number.scm 1225 */
														BgL_test4317z00_9729 = ((bool_t) 0);
													}
												if (BgL_test4317z00_9729)
													{
														long BgL_iz00_9738;

														BgL_iz00_9738 = (BgL_iz00_2431 - 1L);
														BgL_iz00_2431 = BgL_iz00_9738;
														goto BgL_zc3z04anonymousza32766ze3z87_2432;
													}
												else
													{	/* Ieee/number.scm 1229 */
														bool_t BgL_test4320z00_9740;

														if (
															(STRING_REF(BgL_xz00_2425,
																	BgL_iz00_2431) >= ((unsigned char) '2')))
															{	/* Ieee/number.scm 1229 */
																if (
																	(STRING_REF(BgL_xz00_2425,
																			BgL_iz00_2431) <= ((unsigned char) '7')))
																	{	/* Ieee/number.scm 1230 */
																		BgL_test4320z00_9740 =
																			((long) CINT(BgL_rz00_2426) >= 8L);
																	}
																else
																	{	/* Ieee/number.scm 1230 */
																		BgL_test4320z00_9740 = ((bool_t) 0);
																	}
															}
														else
															{	/* Ieee/number.scm 1229 */
																BgL_test4320z00_9740 = ((bool_t) 0);
															}
														if (BgL_test4320z00_9740)
															{
																long BgL_iz00_9749;

																BgL_iz00_9749 = (BgL_iz00_2431 - 1L);
																BgL_iz00_2431 = BgL_iz00_9749;
																goto BgL_zc3z04anonymousza32766ze3z87_2432;
															}
														else
															{	/* Ieee/number.scm 1233 */
																bool_t BgL_test4323z00_9751;

																if (
																	(STRING_REF(BgL_xz00_2425,
																			BgL_iz00_2431) >= ((unsigned char) '8')))
																	{	/* Ieee/number.scm 1233 */
																		if (
																			(STRING_REF(BgL_xz00_2425,
																					BgL_iz00_2431) <=
																				((unsigned char) '9')))
																			{	/* Ieee/number.scm 1234 */
																				BgL_test4323z00_9751 =
																					((long) CINT(BgL_rz00_2426) >= 10L);
																			}
																		else
																			{	/* Ieee/number.scm 1234 */
																				BgL_test4323z00_9751 = ((bool_t) 0);
																			}
																	}
																else
																	{	/* Ieee/number.scm 1233 */
																		BgL_test4323z00_9751 = ((bool_t) 0);
																	}
																if (BgL_test4323z00_9751)
																	{
																		long BgL_iz00_9760;

																		BgL_iz00_9760 = (BgL_iz00_2431 - 1L);
																		BgL_iz00_2431 = BgL_iz00_9760;
																		goto BgL_zc3z04anonymousza32766ze3z87_2432;
																	}
																else
																	{	/* Ieee/number.scm 1237 */
																		bool_t BgL_test4326z00_9762;

																		if (
																			(STRING_REF(BgL_xz00_2425,
																					BgL_iz00_2431) >=
																				((unsigned char) 'a')))
																			{	/* Ieee/number.scm 1237 */
																				if (
																					(STRING_REF(BgL_xz00_2425,
																							BgL_iz00_2431) <=
																						((unsigned char) 'f')))
																					{	/* Ieee/number.scm 1238 */
																						BgL_test4326z00_9762 =
																							(
																							(long) CINT(BgL_rz00_2426) ==
																							16L);
																					}
																				else
																					{	/* Ieee/number.scm 1238 */
																						BgL_test4326z00_9762 = ((bool_t) 0);
																					}
																			}
																		else
																			{	/* Ieee/number.scm 1237 */
																				BgL_test4326z00_9762 = ((bool_t) 0);
																			}
																		if (BgL_test4326z00_9762)
																			{
																				long BgL_iz00_9771;

																				BgL_iz00_9771 = (BgL_iz00_2431 - 1L);
																				BgL_iz00_2431 = BgL_iz00_9771;
																				goto
																					BgL_zc3z04anonymousza32766ze3z87_2432;
																			}
																		else
																			{	/* Ieee/number.scm 1241 */
																				bool_t BgL_test4329z00_9773;

																				if (
																					(STRING_REF(BgL_xz00_2425,
																							BgL_iz00_2431) >=
																						((unsigned char) 'A')))
																					{	/* Ieee/number.scm 1241 */
																						if (
																							(STRING_REF(BgL_xz00_2425,
																									BgL_iz00_2431) <=
																								((unsigned char) 'F')))
																							{	/* Ieee/number.scm 1242 */
																								BgL_test4329z00_9773 =
																									(
																									(long) CINT(BgL_rz00_2426) ==
																									16L);
																							}
																						else
																							{	/* Ieee/number.scm 1242 */
																								BgL_test4329z00_9773 =
																									((bool_t) 0);
																							}
																					}
																				else
																					{	/* Ieee/number.scm 1241 */
																						BgL_test4329z00_9773 = ((bool_t) 0);
																					}
																				if (BgL_test4329z00_9773)
																					{
																						long BgL_iz00_9782;

																						BgL_iz00_9782 =
																							(BgL_iz00_2431 - 1L);
																						BgL_iz00_2431 = BgL_iz00_9782;
																						goto
																							BgL_zc3z04anonymousza32766ze3z87_2432;
																					}
																				else
																					{	/* Ieee/number.scm 1245 */
																						bool_t BgL_test4332z00_9784;

																						if (
																							(STRING_REF(BgL_xz00_2425,
																									BgL_iz00_2431) ==
																								((unsigned char) '-')))
																							{	/* Ieee/number.scm 1245 */
																								BgL_test4332z00_9784 =
																									((bool_t) 1);
																							}
																						else
																							{	/* Ieee/number.scm 1245 */
																								BgL_test4332z00_9784 =
																									(STRING_REF(BgL_xz00_2425,
																										BgL_iz00_2431) ==
																									((unsigned char) '+'));
																							}
																						if (BgL_test4332z00_9784)
																							{	/* Ieee/number.scm 1245 */
																								if ((BgL_iz00_2431 == 0L))
																									{	/* Ieee/number.scm 1247 */
																										BgL_test4315z00_9725 =
																											(BgL_lenz00_2428 > 1L);
																									}
																								else
																									{	/* Ieee/number.scm 1247 */
																										BgL_test4315z00_9725 =
																											((bool_t) 0);
																									}
																							}
																						else
																							{	/* Ieee/number.scm 1245 */
																								BgL_test4315z00_9725 =
																									((bool_t) 0);
																							}
																					}
																			}
																	}
															}
													}
											}
									}
								}
								if (BgL_test4315z00_9725)
									{	/* Ieee/number.scm 1291 */
										return
											BGl_stringzd2ze3integerzd2objze3zz__r4_numbers_6_5_fixnumz00
											(BgL_xz00_133, (long) CINT(BgL_radixz00_134));
									}
								else
									{	/* Ieee/number.scm 1293 */
										bool_t BgL_test4335z00_9796;

										{	/* Ieee/number.scm 1293 */
											long BgL_l1z00_4726;

											BgL_l1z00_4726 = STRING_LENGTH(BgL_xz00_133);
											if ((BgL_l1z00_4726 == 6L))
												{	/* Ieee/number.scm 1293 */
													int BgL_arg2917z00_4729;

													{	/* Ieee/number.scm 1293 */
														char *BgL_auxz00_9802;
														char *BgL_tmpz00_9800;

														BgL_auxz00_9802 =
															BSTRING_TO_STRING
															(BGl_string3367z00zz__r4_numbers_6_5z00);
														BgL_tmpz00_9800 = BSTRING_TO_STRING(BgL_xz00_133);
														BgL_arg2917z00_4729 =
															memcmp(BgL_tmpz00_9800, BgL_auxz00_9802,
															BgL_l1z00_4726);
													}
													BgL_test4335z00_9796 =
														((long) (BgL_arg2917z00_4729) == 0L);
												}
											else
												{	/* Ieee/number.scm 1293 */
													BgL_test4335z00_9796 = ((bool_t) 0);
												}
										}
										if (BgL_test4335z00_9796)
											{	/* Ieee/number.scm 1293 */
												return
													BGL_REAL_CNST(BGl_real3368z00zz__r4_numbers_6_5z00);
											}
										else
											{	/* Ieee/number.scm 1295 */
												bool_t BgL_test4337z00_9807;

												{	/* Ieee/number.scm 1295 */
													long BgL_l1z00_4737;

													BgL_l1z00_4737 = STRING_LENGTH(BgL_xz00_133);
													if ((BgL_l1z00_4737 == 6L))
														{	/* Ieee/number.scm 1295 */
															int BgL_arg2917z00_4740;

															{	/* Ieee/number.scm 1295 */
																char *BgL_auxz00_9813;
																char *BgL_tmpz00_9811;

																BgL_auxz00_9813 =
																	BSTRING_TO_STRING
																	(BGl_string3369z00zz__r4_numbers_6_5z00);
																BgL_tmpz00_9811 =
																	BSTRING_TO_STRING(BgL_xz00_133);
																BgL_arg2917z00_4740 =
																	memcmp(BgL_tmpz00_9811, BgL_auxz00_9813,
																	BgL_l1z00_4737);
															}
															BgL_test4337z00_9807 =
																((long) (BgL_arg2917z00_4740) == 0L);
														}
													else
														{	/* Ieee/number.scm 1295 */
															BgL_test4337z00_9807 = ((bool_t) 0);
														}
												}
												if (BgL_test4337z00_9807)
													{	/* Ieee/number.scm 1295 */
														return
															BGL_REAL_CNST
															(BGl_real3370z00zz__r4_numbers_6_5z00);
													}
												else
													{	/* Ieee/number.scm 1297 */
														bool_t BgL_test4339z00_9818;

														{	/* Ieee/number.scm 1297 */
															long BgL_l1z00_4748;

															BgL_l1z00_4748 = STRING_LENGTH(BgL_xz00_133);
															if ((BgL_l1z00_4748 == 6L))
																{	/* Ieee/number.scm 1297 */
																	int BgL_arg2917z00_4751;

																	{	/* Ieee/number.scm 1297 */
																		char *BgL_auxz00_9824;
																		char *BgL_tmpz00_9822;

																		BgL_auxz00_9824 =
																			BSTRING_TO_STRING
																			(BGl_string3371z00zz__r4_numbers_6_5z00);
																		BgL_tmpz00_9822 =
																			BSTRING_TO_STRING(BgL_xz00_133);
																		BgL_arg2917z00_4751 =
																			memcmp(BgL_tmpz00_9822, BgL_auxz00_9824,
																			BgL_l1z00_4748);
																	}
																	BgL_test4339z00_9818 =
																		((long) (BgL_arg2917z00_4751) == 0L);
																}
															else
																{	/* Ieee/number.scm 1297 */
																	BgL_test4339z00_9818 = ((bool_t) 0);
																}
														}
														if (BgL_test4339z00_9818)
															{	/* Ieee/number.scm 1297 */
																return
																	BGL_REAL_CNST
																	(BGl_real3372z00zz__r4_numbers_6_5z00);
															}
														else
															{	/* Ieee/number.scm 1299 */
																bool_t BgL_test4341z00_9829;

																BgL_xz00_2504 = BgL_xz00_133;
																{	/* Ieee/number.scm 1251 */
																	long BgL_lenz00_2506;

																	BgL_lenz00_2506 =
																		STRING_LENGTH(BgL_xz00_2504);
																	{
																		long BgL_iz00_2508;
																		bool_t BgL_ez00_2509;
																		long BgL_pz00_2510;
																		bool_t BgL_dz00_2511;

																		BgL_iz00_2508 = 0L;
																		BgL_ez00_2509 = ((bool_t) 0);
																		BgL_pz00_2510 = 0L;
																		BgL_dz00_2511 = ((bool_t) 0);
																	BgL_zc3z04anonymousza32861ze3z87_2512:
																		if ((BgL_iz00_2508 == BgL_lenz00_2506))
																			{	/* Ieee/number.scm 1256 */
																				BgL_test4341z00_9829 = BgL_dz00_2511;
																			}
																		else
																			{	/* Ieee/number.scm 1258 */
																				bool_t BgL_test4343z00_9833;

																				if (
																					(STRING_REF(BgL_xz00_2504,
																							BgL_iz00_2508) >=
																						((unsigned char) '0')))
																					{	/* Ieee/number.scm 1258 */
																						BgL_test4343z00_9833 =
																							(STRING_REF(BgL_xz00_2504,
																								BgL_iz00_2508) <=
																							((unsigned char) '9'));
																					}
																				else
																					{	/* Ieee/number.scm 1258 */
																						BgL_test4343z00_9833 = ((bool_t) 0);
																					}
																				if (BgL_test4343z00_9833)
																					{
																						bool_t BgL_dz00_9842;
																						long BgL_pz00_9841;
																						long BgL_iz00_9839;

																						BgL_iz00_9839 =
																							(BgL_iz00_2508 + 1L);
																						BgL_pz00_9841 = 0L;
																						BgL_dz00_9842 = ((bool_t) 1);
																						BgL_dz00_2511 = BgL_dz00_9842;
																						BgL_pz00_2510 = BgL_pz00_9841;
																						BgL_iz00_2508 = BgL_iz00_9839;
																						goto
																							BgL_zc3z04anonymousza32861ze3z87_2512;
																					}
																				else
																					{	/* Ieee/number.scm 1258 */
																						if (
																							(STRING_REF(BgL_xz00_2504,
																									BgL_iz00_2508) ==
																								((unsigned char) '.')))
																							{
																								long BgL_pz00_9848;
																								long BgL_iz00_9846;

																								BgL_iz00_9846 =
																									(BgL_iz00_2508 + 1L);
																								BgL_pz00_9848 = 0L;
																								BgL_pz00_2510 = BgL_pz00_9848;
																								BgL_iz00_2508 = BgL_iz00_9846;
																								goto
																									BgL_zc3z04anonymousza32861ze3z87_2512;
																							}
																						else
																							{	/* Ieee/number.scm 1269 */
																								bool_t BgL_test4346z00_9849;

																								if (
																									(STRING_REF(BgL_xz00_2504,
																											BgL_iz00_2508) ==
																										((unsigned char) 'e')))
																									{	/* Ieee/number.scm 1269 */
																										BgL_test4346z00_9849 =
																											((bool_t) 1);
																									}
																								else
																									{	/* Ieee/number.scm 1269 */
																										BgL_test4346z00_9849 =
																											(STRING_REF(BgL_xz00_2504,
																												BgL_iz00_2508) ==
																											((unsigned char) 'E'));
																									}
																								if (BgL_test4346z00_9849)
																									{	/* Ieee/number.scm 1271 */
																										bool_t BgL_test4348z00_9855;

																										if (BgL_ez00_2509)
																											{	/* Ieee/number.scm 1271 */
																												BgL_test4348z00_9855 =
																													((bool_t) 1);
																											}
																										else
																											{	/* Ieee/number.scm 1271 */
																												if (BgL_dz00_2511)
																													{	/* Ieee/number.scm 1271 */
																														BgL_test4348z00_9855
																															= ((bool_t) 0);
																													}
																												else
																													{	/* Ieee/number.scm 1271 */
																														BgL_test4348z00_9855
																															= ((bool_t) 1);
																													}
																											}
																										if (BgL_test4348z00_9855)
																											{	/* Ieee/number.scm 1271 */
																												BgL_test4341z00_9829 =
																													((bool_t) 0);
																											}
																										else
																											{
																												long BgL_pz00_9861;
																												bool_t BgL_ez00_9860;
																												long BgL_iz00_9858;

																												BgL_iz00_9858 =
																													(BgL_iz00_2508 + 1L);
																												BgL_ez00_9860 =
																													((bool_t) 1);
																												BgL_pz00_9861 =
																													(BgL_iz00_2508 + 1L);
																												BgL_pz00_2510 =
																													BgL_pz00_9861;
																												BgL_ez00_2509 =
																													BgL_ez00_9860;
																												BgL_iz00_2508 =
																													BgL_iz00_9858;
																												goto
																													BgL_zc3z04anonymousza32861ze3z87_2512;
																											}
																									}
																								else
																									{	/* Ieee/number.scm 1277 */
																										bool_t BgL_test4351z00_9863;

																										if (
																											(STRING_REF(BgL_xz00_2504,
																													BgL_iz00_2508) ==
																												((unsigned char) '-')))
																											{	/* Ieee/number.scm 1277 */
																												BgL_test4351z00_9863 =
																													((bool_t) 1);
																											}
																										else
																											{	/* Ieee/number.scm 1277 */
																												BgL_test4351z00_9863 =
																													(STRING_REF
																													(BgL_xz00_2504,
																														BgL_iz00_2508) ==
																													((unsigned char)
																														'+'));
																											}
																										if (BgL_test4351z00_9863)
																											{	/* Ieee/number.scm 1279 */
																												bool_t
																													BgL_test4353z00_9869;
																												{	/* Ieee/number.scm 1279 */
																													bool_t
																														BgL__ortest_1061z00_2538;
																													BgL__ortest_1061z00_2538
																														=
																														(BgL_iz00_2508 ==
																														0L);
																													if (BgL__ortest_1061z00_2538)
																														{	/* Ieee/number.scm 1279 */
																															BgL_test4353z00_9869
																																=
																																BgL__ortest_1061z00_2538;
																														}
																													else
																														{	/* Ieee/number.scm 1279 */
																															BgL_test4353z00_9869
																																=
																																(BgL_iz00_2508
																																==
																																BgL_pz00_2510);
																														}
																												}
																												if (BgL_test4353z00_9869)
																													{
																														long BgL_pz00_9875;
																														long BgL_iz00_9873;

																														BgL_iz00_9873 =
																															(BgL_iz00_2508 +
																															1L);
																														BgL_pz00_9875 = 0L;
																														BgL_pz00_2510 =
																															BgL_pz00_9875;
																														BgL_iz00_2508 =
																															BgL_iz00_9873;
																														goto
																															BgL_zc3z04anonymousza32861ze3z87_2512;
																													}
																												else
																													{	/* Ieee/number.scm 1279 */
																														BgL_test4341z00_9829
																															= ((bool_t) 0);
																													}
																											}
																										else
																											{	/* Ieee/number.scm 1277 */
																												BgL_test4341z00_9829 =
																													((bool_t) 0);
																											}
																									}
																							}
																					}
																			}
																	}
																}
																if (BgL_test4341z00_9829)
																	{	/* Ieee/number.scm 1299 */
																		if (((long) CINT(BgL_radixz00_134) == 10L))
																			{	/* Ieee/number.scm 1301 */
																				char *BgL_stringz00_4758;

																				BgL_stringz00_4758 =
																					BSTRING_TO_STRING(BgL_xz00_133);
																				{	/* Ieee/number.scm 1301 */
																					bool_t BgL_test4356z00_9880;

																					{	/* Ieee/number.scm 1301 */
																						obj_t BgL_string1z00_4762;

																						BgL_string1z00_4762 =
																							string_to_bstring
																							(BgL_stringz00_4758);
																						{	/* Ieee/number.scm 1301 */
																							long BgL_l1z00_4764;

																							BgL_l1z00_4764 =
																								STRING_LENGTH
																								(BgL_string1z00_4762);
																							if ((BgL_l1z00_4764 == 6L))
																								{	/* Ieee/number.scm 1301 */
																									int BgL_arg2917z00_4767;

																									{	/* Ieee/number.scm 1301 */
																										char *BgL_auxz00_9887;
																										char *BgL_tmpz00_9885;

																										BgL_auxz00_9887 =
																											BSTRING_TO_STRING
																											(BGl_string3367z00zz__r4_numbers_6_5z00);
																										BgL_tmpz00_9885 =
																											BSTRING_TO_STRING
																											(BgL_string1z00_4762);
																										BgL_arg2917z00_4767 =
																											memcmp(BgL_tmpz00_9885,
																											BgL_auxz00_9887,
																											BgL_l1z00_4764);
																									}
																									BgL_test4356z00_9880 =
																										(
																										(long) (BgL_arg2917z00_4767)
																										== 0L);
																								}
																							else
																								{	/* Ieee/number.scm 1301 */
																									BgL_test4356z00_9880 =
																										((bool_t) 0);
																								}
																						}
																					}
																					if (BgL_test4356z00_9880)
																						{	/* Ieee/number.scm 1301 */
																							return
																								BGL_REAL_CNST
																								(BGl_real3373z00zz__r4_numbers_6_5z00);
																						}
																					else
																						{	/* Ieee/number.scm 1301 */
																							bool_t BgL_test4358z00_9892;

																							{	/* Ieee/number.scm 1301 */
																								obj_t BgL_string1z00_4773;

																								BgL_string1z00_4773 =
																									string_to_bstring
																									(BgL_stringz00_4758);
																								{	/* Ieee/number.scm 1301 */
																									long BgL_l1z00_4775;

																									BgL_l1z00_4775 =
																										STRING_LENGTH
																										(BgL_string1z00_4773);
																									if ((BgL_l1z00_4775 == 6L))
																										{	/* Ieee/number.scm 1301 */
																											int BgL_arg2917z00_4778;

																											{	/* Ieee/number.scm 1301 */
																												char *BgL_auxz00_9899;
																												char *BgL_tmpz00_9897;

																												BgL_auxz00_9899 =
																													BSTRING_TO_STRING
																													(BGl_string3369z00zz__r4_numbers_6_5z00);
																												BgL_tmpz00_9897 =
																													BSTRING_TO_STRING
																													(BgL_string1z00_4773);
																												BgL_arg2917z00_4778 =
																													memcmp
																													(BgL_tmpz00_9897,
																													BgL_auxz00_9899,
																													BgL_l1z00_4775);
																											}
																											BgL_test4358z00_9892 =
																												(
																												(long)
																												(BgL_arg2917z00_4778) ==
																												0L);
																										}
																									else
																										{	/* Ieee/number.scm 1301 */
																											BgL_test4358z00_9892 =
																												((bool_t) 0);
																										}
																								}
																							}
																							if (BgL_test4358z00_9892)
																								{	/* Ieee/number.scm 1301 */
																									return
																										BGL_REAL_CNST
																										(BGl_real3370z00zz__r4_numbers_6_5z00);
																								}
																							else
																								{	/* Ieee/number.scm 1301 */
																									bool_t BgL_test4360z00_9904;

																									{	/* Ieee/number.scm 1301 */
																										obj_t BgL_string1z00_4784;

																										BgL_string1z00_4784 =
																											string_to_bstring
																											(BgL_stringz00_4758);
																										{	/* Ieee/number.scm 1301 */
																											long BgL_l1z00_4786;

																											BgL_l1z00_4786 =
																												STRING_LENGTH
																												(BgL_string1z00_4784);
																											if ((BgL_l1z00_4786 ==
																													6L))
																												{	/* Ieee/number.scm 1301 */
																													int
																														BgL_arg2917z00_4789;
																													{	/* Ieee/number.scm 1301 */
																														char
																															*BgL_auxz00_9911;
																														char
																															*BgL_tmpz00_9909;
																														BgL_auxz00_9911 =
																															BSTRING_TO_STRING
																															(BGl_string3371z00zz__r4_numbers_6_5z00);
																														BgL_tmpz00_9909 =
																															BSTRING_TO_STRING
																															(BgL_string1z00_4784);
																														BgL_arg2917z00_4789
																															=
																															memcmp
																															(BgL_tmpz00_9909,
																															BgL_auxz00_9911,
																															BgL_l1z00_4786);
																													}
																													BgL_test4360z00_9904 =
																														(
																														(long)
																														(BgL_arg2917z00_4789)
																														== 0L);
																												}
																											else
																												{	/* Ieee/number.scm 1301 */
																													BgL_test4360z00_9904 =
																														((bool_t) 0);
																												}
																										}
																									}
																									if (BgL_test4360z00_9904)
																										{	/* Ieee/number.scm 1301 */
																											return
																												BGL_REAL_CNST
																												(BGl_real3372z00zz__r4_numbers_6_5z00);
																										}
																									else
																										{	/* Ieee/number.scm 1301 */
																											return
																												DOUBLE_TO_REAL(STRTOD
																												(BgL_stringz00_4758));
																										}
																								}
																						}
																				}
																			}
																		else
																			{	/* Ieee/number.scm 1300 */
																				return
																					BGl_errorz00zz__errorz00
																					(BGl_string3374z00zz__r4_numbers_6_5z00,
																					BGl_string3375z00zz__r4_numbers_6_5z00,
																					BgL_radixz00_134);
																			}
																	}
																else
																	{	/* Ieee/number.scm 1299 */
																		return BFALSE;
																	}
															}
													}
											}
									}
							}
					}
				else
					{	/* Ieee/number.scm 1287 */
						return
							BGl_errorz00zz__errorz00(BGl_symbol3363z00zz__r4_numbers_6_5z00,
							BGl_string3364z00zz__r4_numbers_6_5z00, BgL_radixz00_134);
					}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__r4_numbers_6_5z00(void)
	{
		{	/* Ieee/number.scm 18 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string3376z00zz__r4_numbers_6_5z00));
			return
				BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string3376z00zz__r4_numbers_6_5z00));
		}

	}

#ifdef __cplusplus
}
#endif
