/*===========================================================================*/
/*   (Ieee/char.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Ieee/char.scm -indent -o objs/obj_u/Ieee/char.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___R4_CHARACTERS_6_6_TYPE_DEFINITIONS
#define BGL___R4_CHARACTERS_6_6_TYPE_DEFINITIONS
#endif													// BGL___R4_CHARACTERS_6_6_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62charzd2lowerzd2casezf3z91zz__r4_characters_6_6z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL long
		BGl_charzd2ze3integerz31zz__r4_characters_6_6z00(unsigned char);
	BGL_EXPORTED_DECL bool_t
		BGl_charzd2numericzf3z21zz__r4_characters_6_6z00(unsigned char);
	BGL_EXPORTED_DECL unsigned char
		BGl_charzd2andzd2zz__r4_characters_6_6z00(unsigned char, unsigned char);
	BGL_EXPORTED_DECL unsigned char
		BGl_charzd2downcasezd2zz__r4_characters_6_6z00(unsigned char);
	BGL_EXPORTED_DECL unsigned char
		BGl_integerzd2ze3charzd2urze3zz__r4_characters_6_6z00(long);
	static obj_t BGl_requirezd2initializa7ationz75zz__r4_characters_6_6z00 =
		BUNSPEC;
	static obj_t BGl_z62charzd2cizc3zd3zf3z53zz__r4_characters_6_6z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL unsigned char
		BGl_charzd2upcasezd2zz__r4_characters_6_6z00(unsigned char);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_z62charzd2alphabeticzf3z43zz__r4_characters_6_6z00(obj_t,
		obj_t);
	static obj_t BGl_z62charzc3zf3z52zz__r4_characters_6_6z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62charzd2notzb0zz__r4_characters_6_6z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_charzd2cize3zd3zf3z11zz__r4_characters_6_6z00(unsigned char,
		unsigned char);
	static obj_t BGl_z62charzd3zf3z42zz__r4_characters_6_6z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62charzd2cizc3zf3z80zz__r4_characters_6_6z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62charzd2ze3integerz53zz__r4_characters_6_6z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_charzd2upperzd2casezf3zf3zz__r4_characters_6_6z00(unsigned char);
	static obj_t BGl_z62charze3zf3z72zz__r4_characters_6_6z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62charzd2numericzf3z43zz__r4_characters_6_6z00(obj_t,
		obj_t);
	static obj_t BGl_z62charzd2downcasezb0zz__r4_characters_6_6z00(obj_t, obj_t);
	static obj_t BGl_z62charzd2cize3zd3zf3z73zz__r4_characters_6_6z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL unsigned char
		BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(long);
	static obj_t BGl_z62charzd2cizd3zf3z90zz__r4_characters_6_6z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__r4_characters_6_6z00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__r4_characters_6_6z00(void);
	BGL_EXPORTED_DECL bool_t BGl_charzc3zd3zf3ze3zz__r4_characters_6_6z00(unsigned
		char, unsigned char);
	static obj_t BGl_z62charzd2cize3zf3za0zz__r4_characters_6_6z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62charzc3zd3zf3z81zz__r4_characters_6_6z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62charzd2andzb0zz__r4_characters_6_6z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62integerzd2ze3charzd2urz81zz__r4_characters_6_6z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_charzd2whitespacezf3z21zz__r4_characters_6_6z00(unsigned char);
	BGL_EXPORTED_DECL bool_t BGl_charzf3zf3zz__r4_characters_6_6z00(obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_charzd2cizc3zf3ze2zz__r4_characters_6_6z00(unsigned char,
		unsigned char);
	BGL_EXPORTED_DECL bool_t BGl_charzc3zf3z30zz__r4_characters_6_6z00(unsigned
		char, unsigned char);
	BGL_EXPORTED_DECL bool_t
		BGl_charzd2cizd3zf3zf2zz__r4_characters_6_6z00(unsigned char,
		unsigned char);
	BGL_EXPORTED_DECL bool_t BGl_charzd3zf3z20zz__r4_characters_6_6z00(unsigned
		char, unsigned char);
	BGL_EXPORTED_DECL bool_t
		BGl_charzd2lowerzd2casezf3zf3zz__r4_characters_6_6z00(unsigned char);
	static obj_t BGl_z62integerzd2ze3charz53zz__r4_characters_6_6z00(obj_t,
		obj_t);
	static obj_t BGl_z62charzd2upcasezb0zz__r4_characters_6_6z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_charzd2cize3zf3zc2zz__r4_characters_6_6z00(unsigned char,
		unsigned char);
	BGL_EXPORTED_DECL bool_t BGl_charze3zd3zf3zc3zz__r4_characters_6_6z00(unsigned
		char, unsigned char);
	BGL_EXPORTED_DECL bool_t BGl_charze3zf3z10zz__r4_characters_6_6z00(unsigned
		char, unsigned char);
	static obj_t BGl_z62charze3zd3zf3za1zz__r4_characters_6_6z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62charzd2upperzd2casezf3z91zz__r4_characters_6_6z00(obj_t,
		obj_t);
	static obj_t BGl_z62charzf3z91zz__r4_characters_6_6z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_charzd2alphabeticzf3z21zz__r4_characters_6_6z00(unsigned char);
	BGL_EXPORTED_DECL unsigned char
		BGl_charzd2notzd2zz__r4_characters_6_6z00(unsigned char);
	BGL_EXPORTED_DECL unsigned char
		BGl_charzd2orzd2zz__r4_characters_6_6z00(unsigned char, unsigned char);
	static obj_t BGl_z62charzd2orzb0zz__r4_characters_6_6z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62charzd2whitespacezf3z43zz__r4_characters_6_6z00(obj_t,
		obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_charzd2cizc3zd3zf3z31zz__r4_characters_6_6z00(unsigned char,
		unsigned char);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_integerzd2ze3charzd2envze3zz__r4_characters_6_6z00,
		BgL_bgl_za762integerza7d2za7e31516za7,
		BGl_z62integerzd2ze3charz53zz__r4_characters_6_6z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_charzd3zf3zd2envzf2zz__r4_characters_6_6z00,
		BgL_bgl_za762charza7d3za7f3za7421517z00,
		BGl_z62charzd3zf3z42zz__r4_characters_6_6z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_charzd2upcasezd2envz00zz__r4_characters_6_6z00,
		BgL_bgl_za762charza7d2upcase1518z00,
		BGl_z62charzd2upcasezb0zz__r4_characters_6_6z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_charzd2whitespacezf3zd2envzf3zz__r4_characters_6_6z00,
		BgL_bgl_za762charza7d2whites1519z00,
		BGl_z62charzd2whitespacezf3z43zz__r4_characters_6_6z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1500z00zz__r4_characters_6_6z00,
		BgL_bgl_string1500za700za7za7_1520za7, "&char-numeric?", 14);
	      DEFINE_STRING(BGl_string1501z00zz__r4_characters_6_6z00,
		BgL_bgl_string1501za700za7za7_1521za7, "&char-whitespace?", 17);
	      DEFINE_STRING(BGl_string1502z00zz__r4_characters_6_6z00,
		BgL_bgl_string1502za700za7za7_1522za7, "&char-upper-case?", 17);
	      DEFINE_STRING(BGl_string1503z00zz__r4_characters_6_6z00,
		BgL_bgl_string1503za700za7za7_1523za7, "&char-lower-case?", 17);
	      DEFINE_STRING(BGl_string1504z00zz__r4_characters_6_6z00,
		BgL_bgl_string1504za700za7za7_1524za7, "&char->integer", 14);
	      DEFINE_STRING(BGl_string1505z00zz__r4_characters_6_6z00,
		BgL_bgl_string1505za700za7za7_1525za7, "integer->char", 13);
	      DEFINE_STRING(BGl_string1506z00zz__r4_characters_6_6z00,
		BgL_bgl_string1506za700za7za7_1526za7, "integer out of range", 20);
	      DEFINE_STRING(BGl_string1507z00zz__r4_characters_6_6z00,
		BgL_bgl_string1507za700za7za7_1527za7, "&integer->char", 14);
	      DEFINE_STRING(BGl_string1508z00zz__r4_characters_6_6z00,
		BgL_bgl_string1508za700za7za7_1528za7, "bint", 4);
	      DEFINE_STRING(BGl_string1509z00zz__r4_characters_6_6z00,
		BgL_bgl_string1509za700za7za7_1529za7, "&integer->char-ur", 17);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_charzd2numericzf3zd2envzf3zz__r4_characters_6_6z00,
		BgL_bgl_za762charza7d2numeri1530z00,
		BGl_z62charzd2numericzf3z43zz__r4_characters_6_6z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1510z00zz__r4_characters_6_6z00,
		BgL_bgl_string1510za700za7za7_1531za7, "&char-upcase", 12);
	      DEFINE_STRING(BGl_string1511z00zz__r4_characters_6_6z00,
		BgL_bgl_string1511za700za7za7_1532za7, "&char-downcase", 14);
	      DEFINE_STRING(BGl_string1512z00zz__r4_characters_6_6z00,
		BgL_bgl_string1512za700za7za7_1533za7, "&char-or", 8);
	      DEFINE_STRING(BGl_string1513z00zz__r4_characters_6_6z00,
		BgL_bgl_string1513za700za7za7_1534za7, "&char-and", 9);
	      DEFINE_STRING(BGl_string1514z00zz__r4_characters_6_6z00,
		BgL_bgl_string1514za700za7za7_1535za7, "&char-not", 9);
	      DEFINE_STRING(BGl_string1515z00zz__r4_characters_6_6z00,
		BgL_bgl_string1515za700za7za7_1536za7, "__r4_characters_6_6", 19);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_charzd2cizd3zf3zd2envz20zz__r4_characters_6_6z00,
		BgL_bgl_za762charza7d2ciza7d3za71537z00,
		BGl_z62charzd2cizd3zf3z90zz__r4_characters_6_6z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_charze3zf3zd2envzc2zz__r4_characters_6_6z00,
		BgL_bgl_za762charza7e3za7f3za7721538z00,
		BGl_z62charze3zf3z72zz__r4_characters_6_6z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_charzd2cize3zd3zf3zd2envzc3zz__r4_characters_6_6z00,
		BgL_bgl_za762charza7d2ciza7e3za71539z00,
		BGl_z62charzd2cize3zd3zf3z73zz__r4_characters_6_6z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_charzd2alphabeticzf3zd2envzf3zz__r4_characters_6_6z00,
		BgL_bgl_za762charza7d2alphab1540z00,
		BGl_z62charzd2alphabeticzf3z43zz__r4_characters_6_6z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_charzd2andzd2envz00zz__r4_characters_6_6z00,
		BgL_bgl_za762charza7d2andza7b01541za7,
		BGl_z62charzd2andzb0zz__r4_characters_6_6z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_charzd2lowerzd2casezf3zd2envz21zz__r4_characters_6_6z00,
		BgL_bgl_za762charza7d2lowerza71542za7,
		BGl_z62charzd2lowerzd2casezf3z91zz__r4_characters_6_6z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_charzd2cize3zf3zd2envz10zz__r4_characters_6_6z00,
		BgL_bgl_za762charza7d2ciza7e3za71543z00,
		BGl_z62charzd2cize3zf3za0zz__r4_characters_6_6z00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1487z00zz__r4_characters_6_6z00,
		BgL_bgl_string1487za700za7za7_1544za7, "/tmp/bigloo/runtime/Ieee/char.scm",
		33);
	      DEFINE_STRING(BGl_string1488z00zz__r4_characters_6_6z00,
		BgL_bgl_string1488za700za7za7_1545za7, "&char=?", 7);
	      DEFINE_STRING(BGl_string1489z00zz__r4_characters_6_6z00,
		BgL_bgl_string1489za700za7za7_1546za7, "bchar", 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_charze3zd3zf3zd2envz11zz__r4_characters_6_6z00,
		BgL_bgl_za762charza7e3za7d3za7f31547z00,
		BGl_z62charze3zd3zf3za1zz__r4_characters_6_6z00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1490z00zz__r4_characters_6_6z00,
		BgL_bgl_string1490za700za7za7_1548za7, "&char<?", 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_charzd2cizc3zd3zf3zd2envze3zz__r4_characters_6_6z00,
		BgL_bgl_za762charza7d2ciza7c3za71549z00,
		BGl_z62charzd2cizc3zd3zf3z53zz__r4_characters_6_6z00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1491z00zz__r4_characters_6_6z00,
		BgL_bgl_string1491za700za7za7_1550za7, "&char>?", 7);
	      DEFINE_STRING(BGl_string1492z00zz__r4_characters_6_6z00,
		BgL_bgl_string1492za700za7za7_1551za7, "&char<=?", 8);
	      DEFINE_STRING(BGl_string1493z00zz__r4_characters_6_6z00,
		BgL_bgl_string1493za700za7za7_1552za7, "&char>=?", 8);
	      DEFINE_STRING(BGl_string1494z00zz__r4_characters_6_6z00,
		BgL_bgl_string1494za700za7za7_1553za7, "&char-ci=?", 10);
	      DEFINE_STRING(BGl_string1495z00zz__r4_characters_6_6z00,
		BgL_bgl_string1495za700za7za7_1554za7, "&char-ci<?", 10);
	      DEFINE_STRING(BGl_string1496z00zz__r4_characters_6_6z00,
		BgL_bgl_string1496za700za7za7_1555za7, "&char-ci>?", 10);
	      DEFINE_STRING(BGl_string1497z00zz__r4_characters_6_6z00,
		BgL_bgl_string1497za700za7za7_1556za7, "&char-ci<=?", 11);
	      DEFINE_STRING(BGl_string1498z00zz__r4_characters_6_6z00,
		BgL_bgl_string1498za700za7za7_1557za7, "&char-ci>=?", 11);
	      DEFINE_STRING(BGl_string1499z00zz__r4_characters_6_6z00,
		BgL_bgl_string1499za700za7za7_1558za7, "&char-alphabetic?", 17);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_charzf3zd2envz21zz__r4_characters_6_6z00,
		BgL_bgl_za762charza7f3za791za7za7_1559za7,
		BGl_z62charzf3z91zz__r4_characters_6_6z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_charzd2ze3integerzd2envze3zz__r4_characters_6_6z00,
		BgL_bgl_za762charza7d2za7e3int1560za7,
		BGl_z62charzd2ze3integerz53zz__r4_characters_6_6z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_charzc3zf3zd2envze2zz__r4_characters_6_6z00,
		BgL_bgl_za762charza7c3za7f3za7521561z00,
		BGl_z62charzc3zf3z52zz__r4_characters_6_6z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_charzd2notzd2envz00zz__r4_characters_6_6z00,
		BgL_bgl_za762charza7d2notza7b01562za7,
		BGl_z62charzd2notzb0zz__r4_characters_6_6z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_charzd2downcasezd2envz00zz__r4_characters_6_6z00,
		BgL_bgl_za762charza7d2downca1563z00,
		BGl_z62charzd2downcasezb0zz__r4_characters_6_6z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_integerzd2ze3charzd2urzd2envz31zz__r4_characters_6_6z00,
		BgL_bgl_za762integerza7d2za7e31564za7,
		BGl_z62integerzd2ze3charzd2urz81zz__r4_characters_6_6z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_charzd2orzd2envz00zz__r4_characters_6_6z00,
		BgL_bgl_za762charza7d2orza7b0za71565z00,
		BGl_z62charzd2orzb0zz__r4_characters_6_6z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_charzc3zd3zf3zd2envz31zz__r4_characters_6_6z00,
		BgL_bgl_za762charza7c3za7d3za7f31566z00,
		BGl_z62charzc3zd3zf3z81zz__r4_characters_6_6z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_charzd2cizc3zf3zd2envz30zz__r4_characters_6_6z00,
		BgL_bgl_za762charza7d2ciza7c3za71567z00,
		BGl_z62charzd2cizc3zf3z80zz__r4_characters_6_6z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_charzd2upperzd2casezf3zd2envz21zz__r4_characters_6_6z00,
		BgL_bgl_za762charza7d2upperza71568za7,
		BGl_z62charzd2upperzd2casezf3z91zz__r4_characters_6_6z00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__r4_characters_6_6z00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long
		BgL_checksumz00_1203, char *BgL_fromz00_1204)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__r4_characters_6_6z00))
				{
					BGl_requirezd2initializa7ationz75zz__r4_characters_6_6z00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__r4_characters_6_6z00();
					return BGl_importedzd2moduleszd2initz00zz__r4_characters_6_6z00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__r4_characters_6_6z00(void)
	{
		{	/* Ieee/char.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* char? */
	BGL_EXPORTED_DEF bool_t BGl_charzf3zf3zz__r4_characters_6_6z00(obj_t
		BgL_objz00_3)
	{
		{	/* Ieee/char.scm 131 */
			return CHARP(BgL_objz00_3);
		}

	}



/* &char? */
	obj_t BGl_z62charzf3z91zz__r4_characters_6_6z00(obj_t BgL_envz00_1075,
		obj_t BgL_objz00_1076)
	{
		{	/* Ieee/char.scm 131 */
			return BBOOL(BGl_charzf3zf3zz__r4_characters_6_6z00(BgL_objz00_1076));
		}

	}



/* char=? */
	BGL_EXPORTED_DEF bool_t BGl_charzd3zf3z20zz__r4_characters_6_6z00(unsigned
		char BgL_char1z00_4, unsigned char BgL_char2z00_5)
	{
		{	/* Ieee/char.scm 137 */
			return (BgL_char1z00_4 == BgL_char2z00_5);
		}

	}



/* &char=? */
	obj_t BGl_z62charzd3zf3z42zz__r4_characters_6_6z00(obj_t BgL_envz00_1077,
		obj_t BgL_char1z00_1078, obj_t BgL_char2z00_1079)
	{
		{	/* Ieee/char.scm 137 */
			{	/* Ieee/char.scm 138 */
				bool_t BgL_tmpz00_1215;

				{	/* Ieee/char.scm 138 */
					unsigned char BgL_auxz00_1225;
					unsigned char BgL_auxz00_1216;

					{	/* Ieee/char.scm 138 */
						obj_t BgL_tmpz00_1226;

						if (CHARP(BgL_char2z00_1079))
							{	/* Ieee/char.scm 138 */
								BgL_tmpz00_1226 = BgL_char2z00_1079;
							}
						else
							{
								obj_t BgL_auxz00_1229;

								BgL_auxz00_1229 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1487z00zz__r4_characters_6_6z00, BINT(6542L),
									BGl_string1488z00zz__r4_characters_6_6z00,
									BGl_string1489z00zz__r4_characters_6_6z00, BgL_char2z00_1079);
								FAILURE(BgL_auxz00_1229, BFALSE, BFALSE);
							}
						BgL_auxz00_1225 = CCHAR(BgL_tmpz00_1226);
					}
					{	/* Ieee/char.scm 138 */
						obj_t BgL_tmpz00_1217;

						if (CHARP(BgL_char1z00_1078))
							{	/* Ieee/char.scm 138 */
								BgL_tmpz00_1217 = BgL_char1z00_1078;
							}
						else
							{
								obj_t BgL_auxz00_1220;

								BgL_auxz00_1220 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1487z00zz__r4_characters_6_6z00, BINT(6542L),
									BGl_string1488z00zz__r4_characters_6_6z00,
									BGl_string1489z00zz__r4_characters_6_6z00, BgL_char1z00_1078);
								FAILURE(BgL_auxz00_1220, BFALSE, BFALSE);
							}
						BgL_auxz00_1216 = CCHAR(BgL_tmpz00_1217);
					}
					BgL_tmpz00_1215 =
						BGl_charzd3zf3z20zz__r4_characters_6_6z00(BgL_auxz00_1216,
						BgL_auxz00_1225);
				}
				return BBOOL(BgL_tmpz00_1215);
			}
		}

	}



/* char<? */
	BGL_EXPORTED_DEF bool_t BGl_charzc3zf3z30zz__r4_characters_6_6z00(unsigned
		char BgL_char1z00_6, unsigned char BgL_char2z00_7)
	{
		{	/* Ieee/char.scm 143 */
			return (BgL_char1z00_6 < BgL_char2z00_7);
		}

	}



/* &char<? */
	obj_t BGl_z62charzc3zf3z52zz__r4_characters_6_6z00(obj_t BgL_envz00_1080,
		obj_t BgL_char1z00_1081, obj_t BgL_char2z00_1082)
	{
		{	/* Ieee/char.scm 143 */
			{	/* Ieee/char.scm 144 */
				bool_t BgL_tmpz00_1237;

				{	/* Ieee/char.scm 144 */
					unsigned char BgL_auxz00_1247;
					unsigned char BgL_auxz00_1238;

					{	/* Ieee/char.scm 144 */
						obj_t BgL_tmpz00_1248;

						if (CHARP(BgL_char2z00_1082))
							{	/* Ieee/char.scm 144 */
								BgL_tmpz00_1248 = BgL_char2z00_1082;
							}
						else
							{
								obj_t BgL_auxz00_1251;

								BgL_auxz00_1251 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1487z00zz__r4_characters_6_6z00, BINT(6828L),
									BGl_string1490z00zz__r4_characters_6_6z00,
									BGl_string1489z00zz__r4_characters_6_6z00, BgL_char2z00_1082);
								FAILURE(BgL_auxz00_1251, BFALSE, BFALSE);
							}
						BgL_auxz00_1247 = CCHAR(BgL_tmpz00_1248);
					}
					{	/* Ieee/char.scm 144 */
						obj_t BgL_tmpz00_1239;

						if (CHARP(BgL_char1z00_1081))
							{	/* Ieee/char.scm 144 */
								BgL_tmpz00_1239 = BgL_char1z00_1081;
							}
						else
							{
								obj_t BgL_auxz00_1242;

								BgL_auxz00_1242 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1487z00zz__r4_characters_6_6z00, BINT(6828L),
									BGl_string1490z00zz__r4_characters_6_6z00,
									BGl_string1489z00zz__r4_characters_6_6z00, BgL_char1z00_1081);
								FAILURE(BgL_auxz00_1242, BFALSE, BFALSE);
							}
						BgL_auxz00_1238 = CCHAR(BgL_tmpz00_1239);
					}
					BgL_tmpz00_1237 =
						BGl_charzc3zf3z30zz__r4_characters_6_6z00(BgL_auxz00_1238,
						BgL_auxz00_1247);
				}
				return BBOOL(BgL_tmpz00_1237);
			}
		}

	}



/* char>? */
	BGL_EXPORTED_DEF bool_t BGl_charze3zf3z10zz__r4_characters_6_6z00(unsigned
		char BgL_char1z00_8, unsigned char BgL_char2z00_9)
	{
		{	/* Ieee/char.scm 149 */
			return (BgL_char1z00_8 > BgL_char2z00_9);
		}

	}



/* &char>? */
	obj_t BGl_z62charze3zf3z72zz__r4_characters_6_6z00(obj_t BgL_envz00_1083,
		obj_t BgL_char1z00_1084, obj_t BgL_char2z00_1085)
	{
		{	/* Ieee/char.scm 149 */
			{	/* Ieee/char.scm 150 */
				bool_t BgL_tmpz00_1259;

				{	/* Ieee/char.scm 150 */
					unsigned char BgL_auxz00_1269;
					unsigned char BgL_auxz00_1260;

					{	/* Ieee/char.scm 150 */
						obj_t BgL_tmpz00_1270;

						if (CHARP(BgL_char2z00_1085))
							{	/* Ieee/char.scm 150 */
								BgL_tmpz00_1270 = BgL_char2z00_1085;
							}
						else
							{
								obj_t BgL_auxz00_1273;

								BgL_auxz00_1273 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1487z00zz__r4_characters_6_6z00, BINT(7115L),
									BGl_string1491z00zz__r4_characters_6_6z00,
									BGl_string1489z00zz__r4_characters_6_6z00, BgL_char2z00_1085);
								FAILURE(BgL_auxz00_1273, BFALSE, BFALSE);
							}
						BgL_auxz00_1269 = CCHAR(BgL_tmpz00_1270);
					}
					{	/* Ieee/char.scm 150 */
						obj_t BgL_tmpz00_1261;

						if (CHARP(BgL_char1z00_1084))
							{	/* Ieee/char.scm 150 */
								BgL_tmpz00_1261 = BgL_char1z00_1084;
							}
						else
							{
								obj_t BgL_auxz00_1264;

								BgL_auxz00_1264 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1487z00zz__r4_characters_6_6z00, BINT(7115L),
									BGl_string1491z00zz__r4_characters_6_6z00,
									BGl_string1489z00zz__r4_characters_6_6z00, BgL_char1z00_1084);
								FAILURE(BgL_auxz00_1264, BFALSE, BFALSE);
							}
						BgL_auxz00_1260 = CCHAR(BgL_tmpz00_1261);
					}
					BgL_tmpz00_1259 =
						BGl_charze3zf3z10zz__r4_characters_6_6z00(BgL_auxz00_1260,
						BgL_auxz00_1269);
				}
				return BBOOL(BgL_tmpz00_1259);
			}
		}

	}



/* char<=? */
	BGL_EXPORTED_DEF bool_t BGl_charzc3zd3zf3ze3zz__r4_characters_6_6z00(unsigned
		char BgL_char1z00_10, unsigned char BgL_char2z00_11)
	{
		{	/* Ieee/char.scm 155 */
			return (BgL_char1z00_10 <= BgL_char2z00_11);
		}

	}



/* &char<=? */
	obj_t BGl_z62charzc3zd3zf3z81zz__r4_characters_6_6z00(obj_t BgL_envz00_1086,
		obj_t BgL_char1z00_1087, obj_t BgL_char2z00_1088)
	{
		{	/* Ieee/char.scm 155 */
			{	/* Ieee/char.scm 156 */
				bool_t BgL_tmpz00_1281;

				{	/* Ieee/char.scm 156 */
					unsigned char BgL_auxz00_1291;
					unsigned char BgL_auxz00_1282;

					{	/* Ieee/char.scm 156 */
						obj_t BgL_tmpz00_1292;

						if (CHARP(BgL_char2z00_1088))
							{	/* Ieee/char.scm 156 */
								BgL_tmpz00_1292 = BgL_char2z00_1088;
							}
						else
							{
								obj_t BgL_auxz00_1295;

								BgL_auxz00_1295 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1487z00zz__r4_characters_6_6z00, BINT(7402L),
									BGl_string1492z00zz__r4_characters_6_6z00,
									BGl_string1489z00zz__r4_characters_6_6z00, BgL_char2z00_1088);
								FAILURE(BgL_auxz00_1295, BFALSE, BFALSE);
							}
						BgL_auxz00_1291 = CCHAR(BgL_tmpz00_1292);
					}
					{	/* Ieee/char.scm 156 */
						obj_t BgL_tmpz00_1283;

						if (CHARP(BgL_char1z00_1087))
							{	/* Ieee/char.scm 156 */
								BgL_tmpz00_1283 = BgL_char1z00_1087;
							}
						else
							{
								obj_t BgL_auxz00_1286;

								BgL_auxz00_1286 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1487z00zz__r4_characters_6_6z00, BINT(7402L),
									BGl_string1492z00zz__r4_characters_6_6z00,
									BGl_string1489z00zz__r4_characters_6_6z00, BgL_char1z00_1087);
								FAILURE(BgL_auxz00_1286, BFALSE, BFALSE);
							}
						BgL_auxz00_1282 = CCHAR(BgL_tmpz00_1283);
					}
					BgL_tmpz00_1281 =
						BGl_charzc3zd3zf3ze3zz__r4_characters_6_6z00(BgL_auxz00_1282,
						BgL_auxz00_1291);
				}
				return BBOOL(BgL_tmpz00_1281);
			}
		}

	}



/* char>=? */
	BGL_EXPORTED_DEF bool_t BGl_charze3zd3zf3zc3zz__r4_characters_6_6z00(unsigned
		char BgL_char1z00_12, unsigned char BgL_char2z00_13)
	{
		{	/* Ieee/char.scm 161 */
			return (BgL_char1z00_12 >= BgL_char2z00_13);
		}

	}



/* &char>=? */
	obj_t BGl_z62charze3zd3zf3za1zz__r4_characters_6_6z00(obj_t BgL_envz00_1089,
		obj_t BgL_char1z00_1090, obj_t BgL_char2z00_1091)
	{
		{	/* Ieee/char.scm 161 */
			{	/* Ieee/char.scm 162 */
				bool_t BgL_tmpz00_1303;

				{	/* Ieee/char.scm 162 */
					unsigned char BgL_auxz00_1313;
					unsigned char BgL_auxz00_1304;

					{	/* Ieee/char.scm 162 */
						obj_t BgL_tmpz00_1314;

						if (CHARP(BgL_char2z00_1091))
							{	/* Ieee/char.scm 162 */
								BgL_tmpz00_1314 = BgL_char2z00_1091;
							}
						else
							{
								obj_t BgL_auxz00_1317;

								BgL_auxz00_1317 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1487z00zz__r4_characters_6_6z00, BINT(7690L),
									BGl_string1493z00zz__r4_characters_6_6z00,
									BGl_string1489z00zz__r4_characters_6_6z00, BgL_char2z00_1091);
								FAILURE(BgL_auxz00_1317, BFALSE, BFALSE);
							}
						BgL_auxz00_1313 = CCHAR(BgL_tmpz00_1314);
					}
					{	/* Ieee/char.scm 162 */
						obj_t BgL_tmpz00_1305;

						if (CHARP(BgL_char1z00_1090))
							{	/* Ieee/char.scm 162 */
								BgL_tmpz00_1305 = BgL_char1z00_1090;
							}
						else
							{
								obj_t BgL_auxz00_1308;

								BgL_auxz00_1308 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1487z00zz__r4_characters_6_6z00, BINT(7690L),
									BGl_string1493z00zz__r4_characters_6_6z00,
									BGl_string1489z00zz__r4_characters_6_6z00, BgL_char1z00_1090);
								FAILURE(BgL_auxz00_1308, BFALSE, BFALSE);
							}
						BgL_auxz00_1304 = CCHAR(BgL_tmpz00_1305);
					}
					BgL_tmpz00_1303 =
						BGl_charze3zd3zf3zc3zz__r4_characters_6_6z00(BgL_auxz00_1304,
						BgL_auxz00_1313);
				}
				return BBOOL(BgL_tmpz00_1303);
			}
		}

	}



/* char-ci=? */
	BGL_EXPORTED_DEF bool_t
		BGl_charzd2cizd3zf3zf2zz__r4_characters_6_6z00(unsigned char
		BgL_char1z00_14, unsigned char BgL_char2z00_15)
	{
		{	/* Ieee/char.scm 167 */
			return (toupper(BgL_char1z00_14) == toupper(BgL_char2z00_15));
		}

	}



/* &char-ci=? */
	obj_t BGl_z62charzd2cizd3zf3z90zz__r4_characters_6_6z00(obj_t BgL_envz00_1092,
		obj_t BgL_char1z00_1093, obj_t BgL_char2z00_1094)
	{
		{	/* Ieee/char.scm 167 */
			{	/* Ieee/char.scm 168 */
				bool_t BgL_tmpz00_1327;

				{	/* Ieee/char.scm 168 */
					unsigned char BgL_auxz00_1337;
					unsigned char BgL_auxz00_1328;

					{	/* Ieee/char.scm 168 */
						obj_t BgL_tmpz00_1338;

						if (CHARP(BgL_char2z00_1094))
							{	/* Ieee/char.scm 168 */
								BgL_tmpz00_1338 = BgL_char2z00_1094;
							}
						else
							{
								obj_t BgL_auxz00_1341;

								BgL_auxz00_1341 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1487z00zz__r4_characters_6_6z00, BINT(7988L),
									BGl_string1494z00zz__r4_characters_6_6z00,
									BGl_string1489z00zz__r4_characters_6_6z00, BgL_char2z00_1094);
								FAILURE(BgL_auxz00_1341, BFALSE, BFALSE);
							}
						BgL_auxz00_1337 = CCHAR(BgL_tmpz00_1338);
					}
					{	/* Ieee/char.scm 168 */
						obj_t BgL_tmpz00_1329;

						if (CHARP(BgL_char1z00_1093))
							{	/* Ieee/char.scm 168 */
								BgL_tmpz00_1329 = BgL_char1z00_1093;
							}
						else
							{
								obj_t BgL_auxz00_1332;

								BgL_auxz00_1332 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1487z00zz__r4_characters_6_6z00, BINT(7988L),
									BGl_string1494z00zz__r4_characters_6_6z00,
									BGl_string1489z00zz__r4_characters_6_6z00, BgL_char1z00_1093);
								FAILURE(BgL_auxz00_1332, BFALSE, BFALSE);
							}
						BgL_auxz00_1328 = CCHAR(BgL_tmpz00_1329);
					}
					BgL_tmpz00_1327 =
						BGl_charzd2cizd3zf3zf2zz__r4_characters_6_6z00(BgL_auxz00_1328,
						BgL_auxz00_1337);
				}
				return BBOOL(BgL_tmpz00_1327);
			}
		}

	}



/* char-ci<? */
	BGL_EXPORTED_DEF bool_t
		BGl_charzd2cizc3zf3ze2zz__r4_characters_6_6z00(unsigned char
		BgL_char1z00_16, unsigned char BgL_char2z00_17)
	{
		{	/* Ieee/char.scm 173 */
			return (toupper(BgL_char1z00_16) < toupper(BgL_char2z00_17));
		}

	}



/* &char-ci<? */
	obj_t BGl_z62charzd2cizc3zf3z80zz__r4_characters_6_6z00(obj_t BgL_envz00_1095,
		obj_t BgL_char1z00_1096, obj_t BgL_char2z00_1097)
	{
		{	/* Ieee/char.scm 173 */
			{	/* Ieee/char.scm 174 */
				bool_t BgL_tmpz00_1351;

				{	/* Ieee/char.scm 174 */
					unsigned char BgL_auxz00_1361;
					unsigned char BgL_auxz00_1352;

					{	/* Ieee/char.scm 174 */
						obj_t BgL_tmpz00_1362;

						if (CHARP(BgL_char2z00_1097))
							{	/* Ieee/char.scm 174 */
								BgL_tmpz00_1362 = BgL_char2z00_1097;
							}
						else
							{
								obj_t BgL_auxz00_1365;

								BgL_auxz00_1365 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1487z00zz__r4_characters_6_6z00, BINT(8305L),
									BGl_string1495z00zz__r4_characters_6_6z00,
									BGl_string1489z00zz__r4_characters_6_6z00, BgL_char2z00_1097);
								FAILURE(BgL_auxz00_1365, BFALSE, BFALSE);
							}
						BgL_auxz00_1361 = CCHAR(BgL_tmpz00_1362);
					}
					{	/* Ieee/char.scm 174 */
						obj_t BgL_tmpz00_1353;

						if (CHARP(BgL_char1z00_1096))
							{	/* Ieee/char.scm 174 */
								BgL_tmpz00_1353 = BgL_char1z00_1096;
							}
						else
							{
								obj_t BgL_auxz00_1356;

								BgL_auxz00_1356 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1487z00zz__r4_characters_6_6z00, BINT(8305L),
									BGl_string1495z00zz__r4_characters_6_6z00,
									BGl_string1489z00zz__r4_characters_6_6z00, BgL_char1z00_1096);
								FAILURE(BgL_auxz00_1356, BFALSE, BFALSE);
							}
						BgL_auxz00_1352 = CCHAR(BgL_tmpz00_1353);
					}
					BgL_tmpz00_1351 =
						BGl_charzd2cizc3zf3ze2zz__r4_characters_6_6z00(BgL_auxz00_1352,
						BgL_auxz00_1361);
				}
				return BBOOL(BgL_tmpz00_1351);
			}
		}

	}



/* char-ci>? */
	BGL_EXPORTED_DEF bool_t
		BGl_charzd2cize3zf3zc2zz__r4_characters_6_6z00(unsigned char
		BgL_char1z00_18, unsigned char BgL_char2z00_19)
	{
		{	/* Ieee/char.scm 179 */
			return (toupper(BgL_char1z00_18) > toupper(BgL_char2z00_19));
		}

	}



/* &char-ci>? */
	obj_t BGl_z62charzd2cize3zf3za0zz__r4_characters_6_6z00(obj_t BgL_envz00_1098,
		obj_t BgL_char1z00_1099, obj_t BgL_char2z00_1100)
	{
		{	/* Ieee/char.scm 179 */
			{	/* Ieee/char.scm 180 */
				bool_t BgL_tmpz00_1375;

				{	/* Ieee/char.scm 180 */
					unsigned char BgL_auxz00_1385;
					unsigned char BgL_auxz00_1376;

					{	/* Ieee/char.scm 180 */
						obj_t BgL_tmpz00_1386;

						if (CHARP(BgL_char2z00_1100))
							{	/* Ieee/char.scm 180 */
								BgL_tmpz00_1386 = BgL_char2z00_1100;
							}
						else
							{
								obj_t BgL_auxz00_1389;

								BgL_auxz00_1389 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1487z00zz__r4_characters_6_6z00, BINT(8624L),
									BGl_string1496z00zz__r4_characters_6_6z00,
									BGl_string1489z00zz__r4_characters_6_6z00, BgL_char2z00_1100);
								FAILURE(BgL_auxz00_1389, BFALSE, BFALSE);
							}
						BgL_auxz00_1385 = CCHAR(BgL_tmpz00_1386);
					}
					{	/* Ieee/char.scm 180 */
						obj_t BgL_tmpz00_1377;

						if (CHARP(BgL_char1z00_1099))
							{	/* Ieee/char.scm 180 */
								BgL_tmpz00_1377 = BgL_char1z00_1099;
							}
						else
							{
								obj_t BgL_auxz00_1380;

								BgL_auxz00_1380 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1487z00zz__r4_characters_6_6z00, BINT(8624L),
									BGl_string1496z00zz__r4_characters_6_6z00,
									BGl_string1489z00zz__r4_characters_6_6z00, BgL_char1z00_1099);
								FAILURE(BgL_auxz00_1380, BFALSE, BFALSE);
							}
						BgL_auxz00_1376 = CCHAR(BgL_tmpz00_1377);
					}
					BgL_tmpz00_1375 =
						BGl_charzd2cize3zf3zc2zz__r4_characters_6_6z00(BgL_auxz00_1376,
						BgL_auxz00_1385);
				}
				return BBOOL(BgL_tmpz00_1375);
			}
		}

	}



/* char-ci<=? */
	BGL_EXPORTED_DEF bool_t
		BGl_charzd2cizc3zd3zf3z31zz__r4_characters_6_6z00(unsigned char
		BgL_char1z00_20, unsigned char BgL_char2z00_21)
	{
		{	/* Ieee/char.scm 185 */
			return (toupper(BgL_char1z00_20) <= toupper(BgL_char2z00_21));
		}

	}



/* &char-ci<=? */
	obj_t BGl_z62charzd2cizc3zd3zf3z53zz__r4_characters_6_6z00(obj_t
		BgL_envz00_1101, obj_t BgL_char1z00_1102, obj_t BgL_char2z00_1103)
	{
		{	/* Ieee/char.scm 185 */
			{	/* Ieee/char.scm 186 */
				bool_t BgL_tmpz00_1399;

				{	/* Ieee/char.scm 186 */
					unsigned char BgL_auxz00_1409;
					unsigned char BgL_auxz00_1400;

					{	/* Ieee/char.scm 186 */
						obj_t BgL_tmpz00_1410;

						if (CHARP(BgL_char2z00_1103))
							{	/* Ieee/char.scm 186 */
								BgL_tmpz00_1410 = BgL_char2z00_1103;
							}
						else
							{
								obj_t BgL_auxz00_1413;

								BgL_auxz00_1413 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1487z00zz__r4_characters_6_6z00, BINT(8943L),
									BGl_string1497z00zz__r4_characters_6_6z00,
									BGl_string1489z00zz__r4_characters_6_6z00, BgL_char2z00_1103);
								FAILURE(BgL_auxz00_1413, BFALSE, BFALSE);
							}
						BgL_auxz00_1409 = CCHAR(BgL_tmpz00_1410);
					}
					{	/* Ieee/char.scm 186 */
						obj_t BgL_tmpz00_1401;

						if (CHARP(BgL_char1z00_1102))
							{	/* Ieee/char.scm 186 */
								BgL_tmpz00_1401 = BgL_char1z00_1102;
							}
						else
							{
								obj_t BgL_auxz00_1404;

								BgL_auxz00_1404 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1487z00zz__r4_characters_6_6z00, BINT(8943L),
									BGl_string1497z00zz__r4_characters_6_6z00,
									BGl_string1489z00zz__r4_characters_6_6z00, BgL_char1z00_1102);
								FAILURE(BgL_auxz00_1404, BFALSE, BFALSE);
							}
						BgL_auxz00_1400 = CCHAR(BgL_tmpz00_1401);
					}
					BgL_tmpz00_1399 =
						BGl_charzd2cizc3zd3zf3z31zz__r4_characters_6_6z00(BgL_auxz00_1400,
						BgL_auxz00_1409);
				}
				return BBOOL(BgL_tmpz00_1399);
			}
		}

	}



/* char-ci>=? */
	BGL_EXPORTED_DEF bool_t
		BGl_charzd2cize3zd3zf3z11zz__r4_characters_6_6z00(unsigned char
		BgL_char1z00_22, unsigned char BgL_char2z00_23)
	{
		{	/* Ieee/char.scm 191 */
			return (toupper(BgL_char1z00_22) >= toupper(BgL_char2z00_23));
		}

	}



/* &char-ci>=? */
	obj_t BGl_z62charzd2cize3zd3zf3z73zz__r4_characters_6_6z00(obj_t
		BgL_envz00_1104, obj_t BgL_char1z00_1105, obj_t BgL_char2z00_1106)
	{
		{	/* Ieee/char.scm 191 */
			{	/* Ieee/char.scm 192 */
				bool_t BgL_tmpz00_1423;

				{	/* Ieee/char.scm 192 */
					unsigned char BgL_auxz00_1433;
					unsigned char BgL_auxz00_1424;

					{	/* Ieee/char.scm 192 */
						obj_t BgL_tmpz00_1434;

						if (CHARP(BgL_char2z00_1106))
							{	/* Ieee/char.scm 192 */
								BgL_tmpz00_1434 = BgL_char2z00_1106;
							}
						else
							{
								obj_t BgL_auxz00_1437;

								BgL_auxz00_1437 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1487z00zz__r4_characters_6_6z00, BINT(9262L),
									BGl_string1498z00zz__r4_characters_6_6z00,
									BGl_string1489z00zz__r4_characters_6_6z00, BgL_char2z00_1106);
								FAILURE(BgL_auxz00_1437, BFALSE, BFALSE);
							}
						BgL_auxz00_1433 = CCHAR(BgL_tmpz00_1434);
					}
					{	/* Ieee/char.scm 192 */
						obj_t BgL_tmpz00_1425;

						if (CHARP(BgL_char1z00_1105))
							{	/* Ieee/char.scm 192 */
								BgL_tmpz00_1425 = BgL_char1z00_1105;
							}
						else
							{
								obj_t BgL_auxz00_1428;

								BgL_auxz00_1428 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1487z00zz__r4_characters_6_6z00, BINT(9262L),
									BGl_string1498z00zz__r4_characters_6_6z00,
									BGl_string1489z00zz__r4_characters_6_6z00, BgL_char1z00_1105);
								FAILURE(BgL_auxz00_1428, BFALSE, BFALSE);
							}
						BgL_auxz00_1424 = CCHAR(BgL_tmpz00_1425);
					}
					BgL_tmpz00_1423 =
						BGl_charzd2cize3zd3zf3z11zz__r4_characters_6_6z00(BgL_auxz00_1424,
						BgL_auxz00_1433);
				}
				return BBOOL(BgL_tmpz00_1423);
			}
		}

	}



/* char-alphabetic? */
	BGL_EXPORTED_DEF bool_t
		BGl_charzd2alphabeticzf3z21zz__r4_characters_6_6z00(unsigned char
		BgL_charz00_24)
	{
		{	/* Ieee/char.scm 197 */
			return isalpha(BgL_charz00_24);
		}

	}



/* &char-alphabetic? */
	obj_t BGl_z62charzd2alphabeticzf3z43zz__r4_characters_6_6z00(obj_t
		BgL_envz00_1107, obj_t BgL_charz00_1108)
	{
		{	/* Ieee/char.scm 197 */
			{	/* Ieee/char.scm 199 */
				bool_t BgL_tmpz00_1445;

				{	/* Ieee/char.scm 199 */
					unsigned char BgL_auxz00_1446;

					{	/* Ieee/char.scm 199 */
						obj_t BgL_tmpz00_1447;

						if (CHARP(BgL_charz00_1108))
							{	/* Ieee/char.scm 199 */
								BgL_tmpz00_1447 = BgL_charz00_1108;
							}
						else
							{
								obj_t BgL_auxz00_1450;

								BgL_auxz00_1450 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1487z00zz__r4_characters_6_6z00, BINT(9598L),
									BGl_string1499z00zz__r4_characters_6_6z00,
									BGl_string1489z00zz__r4_characters_6_6z00, BgL_charz00_1108);
								FAILURE(BgL_auxz00_1450, BFALSE, BFALSE);
							}
						BgL_auxz00_1446 = CCHAR(BgL_tmpz00_1447);
					}
					BgL_tmpz00_1445 =
						BGl_charzd2alphabeticzf3z21zz__r4_characters_6_6z00
						(BgL_auxz00_1446);
				}
				return BBOOL(BgL_tmpz00_1445);
			}
		}

	}



/* char-numeric? */
	BGL_EXPORTED_DEF bool_t
		BGl_charzd2numericzf3z21zz__r4_characters_6_6z00(unsigned char
		BgL_charz00_25)
	{
		{	/* Ieee/char.scm 208 */
			return isdigit(BgL_charz00_25);
		}

	}



/* &char-numeric? */
	obj_t BGl_z62charzd2numericzf3z43zz__r4_characters_6_6z00(obj_t
		BgL_envz00_1109, obj_t BgL_charz00_1110)
	{
		{	/* Ieee/char.scm 208 */
			{	/* Ieee/char.scm 210 */
				bool_t BgL_tmpz00_1458;

				{	/* Ieee/char.scm 210 */
					unsigned char BgL_auxz00_1459;

					{	/* Ieee/char.scm 210 */
						obj_t BgL_tmpz00_1460;

						if (CHARP(BgL_charz00_1110))
							{	/* Ieee/char.scm 210 */
								BgL_tmpz00_1460 = BgL_charz00_1110;
							}
						else
							{
								obj_t BgL_auxz00_1463;

								BgL_auxz00_1463 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1487z00zz__r4_characters_6_6z00, BINT(10040L),
									BGl_string1500z00zz__r4_characters_6_6z00,
									BGl_string1489z00zz__r4_characters_6_6z00, BgL_charz00_1110);
								FAILURE(BgL_auxz00_1463, BFALSE, BFALSE);
							}
						BgL_auxz00_1459 = CCHAR(BgL_tmpz00_1460);
					}
					BgL_tmpz00_1458 =
						BGl_charzd2numericzf3z21zz__r4_characters_6_6z00(BgL_auxz00_1459);
				}
				return BBOOL(BgL_tmpz00_1458);
			}
		}

	}



/* char-whitespace? */
	BGL_EXPORTED_DEF bool_t
		BGl_charzd2whitespacezf3z21zz__r4_characters_6_6z00(unsigned char
		BgL_charz00_26)
	{
		{	/* Ieee/char.scm 218 */
			return isspace(BgL_charz00_26);
		}

	}



/* &char-whitespace? */
	obj_t BGl_z62charzd2whitespacezf3z43zz__r4_characters_6_6z00(obj_t
		BgL_envz00_1111, obj_t BgL_charz00_1112)
	{
		{	/* Ieee/char.scm 218 */
			{	/* Ieee/char.scm 220 */
				bool_t BgL_tmpz00_1471;

				{	/* Ieee/char.scm 220 */
					unsigned char BgL_auxz00_1472;

					{	/* Ieee/char.scm 220 */
						obj_t BgL_tmpz00_1473;

						if (CHARP(BgL_charz00_1112))
							{	/* Ieee/char.scm 220 */
								BgL_tmpz00_1473 = BgL_charz00_1112;
							}
						else
							{
								obj_t BgL_auxz00_1476;

								BgL_auxz00_1476 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1487z00zz__r4_characters_6_6z00, BINT(10423L),
									BGl_string1501z00zz__r4_characters_6_6z00,
									BGl_string1489z00zz__r4_characters_6_6z00, BgL_charz00_1112);
								FAILURE(BgL_auxz00_1476, BFALSE, BFALSE);
							}
						BgL_auxz00_1472 = CCHAR(BgL_tmpz00_1473);
					}
					BgL_tmpz00_1471 =
						BGl_charzd2whitespacezf3z21zz__r4_characters_6_6z00
						(BgL_auxz00_1472);
				}
				return BBOOL(BgL_tmpz00_1471);
			}
		}

	}



/* char-upper-case? */
	BGL_EXPORTED_DEF bool_t
		BGl_charzd2upperzd2casezf3zf3zz__r4_characters_6_6z00(unsigned char
		BgL_charz00_27)
	{
		{	/* Ieee/char.scm 229 */
			return isupper(BgL_charz00_27);
		}

	}



/* &char-upper-case? */
	obj_t BGl_z62charzd2upperzd2casezf3z91zz__r4_characters_6_6z00(obj_t
		BgL_envz00_1113, obj_t BgL_charz00_1114)
	{
		{	/* Ieee/char.scm 229 */
			{	/* Ieee/char.scm 231 */
				bool_t BgL_tmpz00_1484;

				{	/* Ieee/char.scm 231 */
					unsigned char BgL_auxz00_1485;

					{	/* Ieee/char.scm 231 */
						obj_t BgL_tmpz00_1486;

						if (CHARP(BgL_charz00_1114))
							{	/* Ieee/char.scm 231 */
								BgL_tmpz00_1486 = BgL_charz00_1114;
							}
						else
							{
								obj_t BgL_auxz00_1489;

								BgL_auxz00_1489 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1487z00zz__r4_characters_6_6z00, BINT(10859L),
									BGl_string1502z00zz__r4_characters_6_6z00,
									BGl_string1489z00zz__r4_characters_6_6z00, BgL_charz00_1114);
								FAILURE(BgL_auxz00_1489, BFALSE, BFALSE);
							}
						BgL_auxz00_1485 = CCHAR(BgL_tmpz00_1486);
					}
					BgL_tmpz00_1484 =
						BGl_charzd2upperzd2casezf3zf3zz__r4_characters_6_6z00
						(BgL_auxz00_1485);
				}
				return BBOOL(BgL_tmpz00_1484);
			}
		}

	}



/* char-lower-case? */
	BGL_EXPORTED_DEF bool_t
		BGl_charzd2lowerzd2casezf3zf3zz__r4_characters_6_6z00(unsigned char
		BgL_charz00_28)
	{
		{	/* Ieee/char.scm 239 */
			return islower(BgL_charz00_28);
		}

	}



/* &char-lower-case? */
	obj_t BGl_z62charzd2lowerzd2casezf3z91zz__r4_characters_6_6z00(obj_t
		BgL_envz00_1115, obj_t BgL_charz00_1116)
	{
		{	/* Ieee/char.scm 239 */
			{	/* Ieee/char.scm 241 */
				bool_t BgL_tmpz00_1497;

				{	/* Ieee/char.scm 241 */
					unsigned char BgL_auxz00_1498;

					{	/* Ieee/char.scm 241 */
						obj_t BgL_tmpz00_1499;

						if (CHARP(BgL_charz00_1116))
							{	/* Ieee/char.scm 241 */
								BgL_tmpz00_1499 = BgL_charz00_1116;
							}
						else
							{
								obj_t BgL_auxz00_1502;

								BgL_auxz00_1502 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1487z00zz__r4_characters_6_6z00, BINT(11245L),
									BGl_string1503z00zz__r4_characters_6_6z00,
									BGl_string1489z00zz__r4_characters_6_6z00, BgL_charz00_1116);
								FAILURE(BgL_auxz00_1502, BFALSE, BFALSE);
							}
						BgL_auxz00_1498 = CCHAR(BgL_tmpz00_1499);
					}
					BgL_tmpz00_1497 =
						BGl_charzd2lowerzd2casezf3zf3zz__r4_characters_6_6z00
						(BgL_auxz00_1498);
				}
				return BBOOL(BgL_tmpz00_1497);
			}
		}

	}



/* char->integer */
	BGL_EXPORTED_DEF long
		BGl_charzd2ze3integerz31zz__r4_characters_6_6z00(unsigned char
		BgL_charz00_29)
	{
		{	/* Ieee/char.scm 249 */
			return (BgL_charz00_29);
		}

	}



/* &char->integer */
	obj_t BGl_z62charzd2ze3integerz53zz__r4_characters_6_6z00(obj_t
		BgL_envz00_1117, obj_t BgL_charz00_1118)
	{
		{	/* Ieee/char.scm 249 */
			{	/* Ieee/char.scm 250 */
				long BgL_tmpz00_1510;

				{	/* Ieee/char.scm 250 */
					unsigned char BgL_auxz00_1511;

					{	/* Ieee/char.scm 250 */
						obj_t BgL_tmpz00_1512;

						if (CHARP(BgL_charz00_1118))
							{	/* Ieee/char.scm 250 */
								BgL_tmpz00_1512 = BgL_charz00_1118;
							}
						else
							{
								obj_t BgL_auxz00_1515;

								BgL_auxz00_1515 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1487z00zz__r4_characters_6_6z00, BINT(11599L),
									BGl_string1504z00zz__r4_characters_6_6z00,
									BGl_string1489z00zz__r4_characters_6_6z00, BgL_charz00_1118);
								FAILURE(BgL_auxz00_1515, BFALSE, BFALSE);
							}
						BgL_auxz00_1511 = CCHAR(BgL_tmpz00_1512);
					}
					BgL_tmpz00_1510 =
						BGl_charzd2ze3integerz31zz__r4_characters_6_6z00(BgL_auxz00_1511);
				}
				return BINT(BgL_tmpz00_1510);
			}
		}

	}



/* integer->char */
	BGL_EXPORTED_DEF unsigned char
		BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(long BgL_intz00_30)
	{
		{	/* Ieee/char.scm 255 */
			{	/* Ieee/char.scm 256 */
				bool_t BgL_test1598z00_1522;

				if ((BgL_intz00_30 >= 0L))
					{	/* Ieee/char.scm 256 */
						BgL_test1598z00_1522 = (BgL_intz00_30 <= 255L);
					}
				else
					{	/* Ieee/char.scm 256 */
						BgL_test1598z00_1522 = ((bool_t) 0);
					}
				if (BgL_test1598z00_1522)
					{	/* Ieee/char.scm 256 */
						return (BgL_intz00_30);
					}
				else
					{	/* Ieee/char.scm 256 */
						return
							CCHAR(BGl_errorz00zz__errorz00
							(BGl_string1505z00zz__r4_characters_6_6z00,
								BGl_string1506z00zz__r4_characters_6_6z00,
								BINT(BgL_intz00_30)));
					}
			}
		}

	}



/* &integer->char */
	obj_t BGl_z62integerzd2ze3charz53zz__r4_characters_6_6z00(obj_t
		BgL_envz00_1119, obj_t BgL_intz00_1120)
	{
		{	/* Ieee/char.scm 255 */
			{	/* Ieee/char.scm 256 */
				unsigned char BgL_tmpz00_1530;

				{	/* Ieee/char.scm 256 */
					long BgL_auxz00_1531;

					{	/* Ieee/char.scm 256 */
						obj_t BgL_tmpz00_1532;

						if (INTEGERP(BgL_intz00_1120))
							{	/* Ieee/char.scm 256 */
								BgL_tmpz00_1532 = BgL_intz00_1120;
							}
						else
							{
								obj_t BgL_auxz00_1535;

								BgL_auxz00_1535 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1487z00zz__r4_characters_6_6z00, BINT(11877L),
									BGl_string1507z00zz__r4_characters_6_6z00,
									BGl_string1508z00zz__r4_characters_6_6z00, BgL_intz00_1120);
								FAILURE(BgL_auxz00_1535, BFALSE, BFALSE);
							}
						BgL_auxz00_1531 = (long) CINT(BgL_tmpz00_1532);
					}
					BgL_tmpz00_1530 =
						BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(BgL_auxz00_1531);
				}
				return BCHAR(BgL_tmpz00_1530);
			}
		}

	}



/* integer->char-ur */
	BGL_EXPORTED_DEF unsigned char
		BGl_integerzd2ze3charzd2urze3zz__r4_characters_6_6z00(long BgL_intz00_31)
	{
		{	/* Ieee/char.scm 263 */
			return (BgL_intz00_31);
		}

	}



/* &integer->char-ur */
	obj_t BGl_z62integerzd2ze3charzd2urz81zz__r4_characters_6_6z00(obj_t
		BgL_envz00_1121, obj_t BgL_intz00_1122)
	{
		{	/* Ieee/char.scm 263 */
			{	/* Ieee/char.scm 264 */
				unsigned char BgL_tmpz00_1543;

				{	/* Ieee/char.scm 264 */
					long BgL_auxz00_1544;

					{	/* Ieee/char.scm 264 */
						obj_t BgL_tmpz00_1545;

						if (INTEGERP(BgL_intz00_1122))
							{	/* Ieee/char.scm 264 */
								BgL_tmpz00_1545 = BgL_intz00_1122;
							}
						else
							{
								obj_t BgL_auxz00_1548;

								BgL_auxz00_1548 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1487z00zz__r4_characters_6_6z00, BINT(12268L),
									BGl_string1509z00zz__r4_characters_6_6z00,
									BGl_string1508z00zz__r4_characters_6_6z00, BgL_intz00_1122);
								FAILURE(BgL_auxz00_1548, BFALSE, BFALSE);
							}
						BgL_auxz00_1544 = (long) CINT(BgL_tmpz00_1545);
					}
					BgL_tmpz00_1543 =
						BGl_integerzd2ze3charzd2urze3zz__r4_characters_6_6z00
						(BgL_auxz00_1544);
				}
				return BCHAR(BgL_tmpz00_1543);
			}
		}

	}



/* char-upcase */
	BGL_EXPORTED_DEF unsigned char
		BGl_charzd2upcasezd2zz__r4_characters_6_6z00(unsigned char BgL_charz00_32)
	{
		{	/* Ieee/char.scm 269 */
			return toupper(BgL_charz00_32);
		}

	}



/* &char-upcase */
	obj_t BGl_z62charzd2upcasezb0zz__r4_characters_6_6z00(obj_t BgL_envz00_1123,
		obj_t BgL_charz00_1124)
	{
		{	/* Ieee/char.scm 269 */
			{	/* Ieee/char.scm 270 */
				unsigned char BgL_tmpz00_1556;

				{	/* Ieee/char.scm 270 */
					unsigned char BgL_auxz00_1557;

					{	/* Ieee/char.scm 270 */
						obj_t BgL_tmpz00_1558;

						if (CHARP(BgL_charz00_1124))
							{	/* Ieee/char.scm 270 */
								BgL_tmpz00_1558 = BgL_charz00_1124;
							}
						else
							{
								obj_t BgL_auxz00_1561;

								BgL_auxz00_1561 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1487z00zz__r4_characters_6_6z00, BINT(12552L),
									BGl_string1510z00zz__r4_characters_6_6z00,
									BGl_string1489z00zz__r4_characters_6_6z00, BgL_charz00_1124);
								FAILURE(BgL_auxz00_1561, BFALSE, BFALSE);
							}
						BgL_auxz00_1557 = CCHAR(BgL_tmpz00_1558);
					}
					BgL_tmpz00_1556 =
						BGl_charzd2upcasezd2zz__r4_characters_6_6z00(BgL_auxz00_1557);
				}
				return BCHAR(BgL_tmpz00_1556);
			}
		}

	}



/* char-downcase */
	BGL_EXPORTED_DEF unsigned char
		BGl_charzd2downcasezd2zz__r4_characters_6_6z00(unsigned char BgL_charz00_33)
	{
		{	/* Ieee/char.scm 275 */
			return tolower(BgL_charz00_33);
		}

	}



/* &char-downcase */
	obj_t BGl_z62charzd2downcasezb0zz__r4_characters_6_6z00(obj_t BgL_envz00_1125,
		obj_t BgL_charz00_1126)
	{
		{	/* Ieee/char.scm 275 */
			{	/* Ieee/char.scm 276 */
				unsigned char BgL_tmpz00_1569;

				{	/* Ieee/char.scm 276 */
					unsigned char BgL_auxz00_1570;

					{	/* Ieee/char.scm 276 */
						obj_t BgL_tmpz00_1571;

						if (CHARP(BgL_charz00_1126))
							{	/* Ieee/char.scm 276 */
								BgL_tmpz00_1571 = BgL_charz00_1126;
							}
						else
							{
								obj_t BgL_auxz00_1574;

								BgL_auxz00_1574 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1487z00zz__r4_characters_6_6z00, BINT(12837L),
									BGl_string1511z00zz__r4_characters_6_6z00,
									BGl_string1489z00zz__r4_characters_6_6z00, BgL_charz00_1126);
								FAILURE(BgL_auxz00_1574, BFALSE, BFALSE);
							}
						BgL_auxz00_1570 = CCHAR(BgL_tmpz00_1571);
					}
					BgL_tmpz00_1569 =
						BGl_charzd2downcasezd2zz__r4_characters_6_6z00(BgL_auxz00_1570);
				}
				return BCHAR(BgL_tmpz00_1569);
			}
		}

	}



/* char-or */
	BGL_EXPORTED_DEF unsigned char
		BGl_charzd2orzd2zz__r4_characters_6_6z00(unsigned char BgL_char1z00_34,
		unsigned char BgL_char2z00_35)
	{
		{	/* Ieee/char.scm 281 */
			return (BgL_char1z00_34 | BgL_char2z00_35);
		}

	}



/* &char-or */
	obj_t BGl_z62charzd2orzb0zz__r4_characters_6_6z00(obj_t BgL_envz00_1127,
		obj_t BgL_char1z00_1128, obj_t BgL_char2z00_1129)
	{
		{	/* Ieee/char.scm 281 */
			{	/* Ieee/char.scm 282 */
				unsigned char BgL_tmpz00_1582;

				{	/* Ieee/char.scm 282 */
					unsigned char BgL_auxz00_1592;
					unsigned char BgL_auxz00_1583;

					{	/* Ieee/char.scm 282 */
						obj_t BgL_tmpz00_1593;

						if (CHARP(BgL_char2z00_1129))
							{	/* Ieee/char.scm 282 */
								BgL_tmpz00_1593 = BgL_char2z00_1129;
							}
						else
							{
								obj_t BgL_auxz00_1596;

								BgL_auxz00_1596 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1487z00zz__r4_characters_6_6z00, BINT(13131L),
									BGl_string1512z00zz__r4_characters_6_6z00,
									BGl_string1489z00zz__r4_characters_6_6z00, BgL_char2z00_1129);
								FAILURE(BgL_auxz00_1596, BFALSE, BFALSE);
							}
						BgL_auxz00_1592 = CCHAR(BgL_tmpz00_1593);
					}
					{	/* Ieee/char.scm 282 */
						obj_t BgL_tmpz00_1584;

						if (CHARP(BgL_char1z00_1128))
							{	/* Ieee/char.scm 282 */
								BgL_tmpz00_1584 = BgL_char1z00_1128;
							}
						else
							{
								obj_t BgL_auxz00_1587;

								BgL_auxz00_1587 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1487z00zz__r4_characters_6_6z00, BINT(13131L),
									BGl_string1512z00zz__r4_characters_6_6z00,
									BGl_string1489z00zz__r4_characters_6_6z00, BgL_char1z00_1128);
								FAILURE(BgL_auxz00_1587, BFALSE, BFALSE);
							}
						BgL_auxz00_1583 = CCHAR(BgL_tmpz00_1584);
					}
					BgL_tmpz00_1582 =
						BGl_charzd2orzd2zz__r4_characters_6_6z00(BgL_auxz00_1583,
						BgL_auxz00_1592);
				}
				return BCHAR(BgL_tmpz00_1582);
			}
		}

	}



/* char-and */
	BGL_EXPORTED_DEF unsigned char
		BGl_charzd2andzd2zz__r4_characters_6_6z00(unsigned char BgL_char1z00_36,
		unsigned char BgL_char2z00_37)
	{
		{	/* Ieee/char.scm 287 */
			return (BgL_char1z00_36 & BgL_char2z00_37);
		}

	}



/* &char-and */
	obj_t BGl_z62charzd2andzb0zz__r4_characters_6_6z00(obj_t BgL_envz00_1130,
		obj_t BgL_char1z00_1131, obj_t BgL_char2z00_1132)
	{
		{	/* Ieee/char.scm 287 */
			{	/* Ieee/char.scm 288 */
				unsigned char BgL_tmpz00_1604;

				{	/* Ieee/char.scm 288 */
					unsigned char BgL_auxz00_1614;
					unsigned char BgL_auxz00_1605;

					{	/* Ieee/char.scm 288 */
						obj_t BgL_tmpz00_1615;

						if (CHARP(BgL_char2z00_1132))
							{	/* Ieee/char.scm 288 */
								BgL_tmpz00_1615 = BgL_char2z00_1132;
							}
						else
							{
								obj_t BgL_auxz00_1618;

								BgL_auxz00_1618 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1487z00zz__r4_characters_6_6z00, BINT(13420L),
									BGl_string1513z00zz__r4_characters_6_6z00,
									BGl_string1489z00zz__r4_characters_6_6z00, BgL_char2z00_1132);
								FAILURE(BgL_auxz00_1618, BFALSE, BFALSE);
							}
						BgL_auxz00_1614 = CCHAR(BgL_tmpz00_1615);
					}
					{	/* Ieee/char.scm 288 */
						obj_t BgL_tmpz00_1606;

						if (CHARP(BgL_char1z00_1131))
							{	/* Ieee/char.scm 288 */
								BgL_tmpz00_1606 = BgL_char1z00_1131;
							}
						else
							{
								obj_t BgL_auxz00_1609;

								BgL_auxz00_1609 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1487z00zz__r4_characters_6_6z00, BINT(13420L),
									BGl_string1513z00zz__r4_characters_6_6z00,
									BGl_string1489z00zz__r4_characters_6_6z00, BgL_char1z00_1131);
								FAILURE(BgL_auxz00_1609, BFALSE, BFALSE);
							}
						BgL_auxz00_1605 = CCHAR(BgL_tmpz00_1606);
					}
					BgL_tmpz00_1604 =
						BGl_charzd2andzd2zz__r4_characters_6_6z00(BgL_auxz00_1605,
						BgL_auxz00_1614);
				}
				return BCHAR(BgL_tmpz00_1604);
			}
		}

	}



/* char-not */
	BGL_EXPORTED_DEF unsigned char
		BGl_charzd2notzd2zz__r4_characters_6_6z00(unsigned char BgL_char1z00_38)
	{
		{	/* Ieee/char.scm 293 */
			return ~(BgL_char1z00_38);
		}

	}



/* &char-not */
	obj_t BGl_z62charzd2notzb0zz__r4_characters_6_6z00(obj_t BgL_envz00_1133,
		obj_t BgL_char1z00_1134)
	{
		{	/* Ieee/char.scm 293 */
			{	/* Ieee/char.scm 294 */
				unsigned char BgL_tmpz00_1626;

				{	/* Ieee/char.scm 294 */
					unsigned char BgL_auxz00_1627;

					{	/* Ieee/char.scm 294 */
						obj_t BgL_tmpz00_1628;

						if (CHARP(BgL_char1z00_1134))
							{	/* Ieee/char.scm 294 */
								BgL_tmpz00_1628 = BgL_char1z00_1134;
							}
						else
							{
								obj_t BgL_auxz00_1631;

								BgL_auxz00_1631 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1487z00zz__r4_characters_6_6z00, BINT(13704L),
									BGl_string1514z00zz__r4_characters_6_6z00,
									BGl_string1489z00zz__r4_characters_6_6z00, BgL_char1z00_1134);
								FAILURE(BgL_auxz00_1631, BFALSE, BFALSE);
							}
						BgL_auxz00_1627 = CCHAR(BgL_tmpz00_1628);
					}
					BgL_tmpz00_1626 =
						BGl_charzd2notzd2zz__r4_characters_6_6z00(BgL_auxz00_1627);
				}
				return BCHAR(BgL_tmpz00_1626);
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__r4_characters_6_6z00(void)
	{
		{	/* Ieee/char.scm 14 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1515z00zz__r4_characters_6_6z00));
			return
				BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1515z00zz__r4_characters_6_6z00));
		}

	}

#ifdef __cplusplus
}
#endif
