/*===========================================================================*/
/*   (Ieee/input.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Ieee/input.scm -indent -o objs/obj_u/Ieee/input.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___R4_INPUT_6_10_2_TYPE_DEFINITIONS
#define BGL___R4_INPUT_6_10_2_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_z62exceptionz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
	}                      *BgL_z62exceptionz62_bglt;

	typedef struct BgL_z62errorz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                  *BgL_z62errorz62_bglt;

	typedef struct BgL_z62iozd2errorzb0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                       *BgL_z62iozd2errorzb0_bglt;


#endif													// BGL___R4_INPUT_6_10_2_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_symbol3454z00zz__r4_input_6_10_2z00 = BUNSPEC;
	BGL_EXPORTED_DECL long
		BGl_sendzd2charszf2siza7ez87zz__r4_input_6_10_2z00(obj_t, obj_t, long,
		long);
	static obj_t BGl_z62sendzd2charszf2siza7eze5zz__r4_input_6_10_2z00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static int BGl_z52sendcharsz52zz__r4_input_6_10_2z00(obj_t, obj_t, long,
		long);
	static obj_t BGl_symbol3460z00zz__r4_input_6_10_2z00 = BUNSPEC;
	static obj_t BGl_z62eofzd2objectzf3z43zz__r4_input_6_10_2z00(obj_t, obj_t);
	static obj_t BGl_symbol3466z00zz__r4_input_6_10_2z00 = BUNSPEC;
	static obj_t BGl_requirezd2initializa7ationz75zz__r4_input_6_10_2z00 =
		BUNSPEC;
	static obj_t BGl__passwordz00zz__r4_input_6_10_2z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_passwordz00zz__r4_input_6_10_2z00(obj_t);
	extern obj_t BGl_raisez00zz__errorz00(obj_t);
	static obj_t BGl_symbol3470z00zz__r4_input_6_10_2z00 = BUNSPEC;
	static obj_t BGl_symbol3474z00zz__r4_input_6_10_2z00 = BUNSPEC;
	static obj_t BGl_symbol3477z00zz__r4_input_6_10_2z00 = BUNSPEC;
	extern obj_t bgl_display_obj(obj_t, obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__gunza7ipza7(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(obj_t, obj_t,
		obj_t);
	extern obj_t bgl_file_to_string(char *);
	static obj_t BGl_symbol3486z00zz__r4_input_6_10_2z00 = BUNSPEC;
	extern bool_t fexists(char *);
	BGL_EXPORTED_DECL obj_t BGl_eofzd2objectzd2zz__r4_input_6_10_2z00(void);
	static obj_t BGl__unreadzd2stringz12zc0zz__r4_input_6_10_2z00(obj_t, obj_t);
	static obj_t BGl_symbol3493z00zz__r4_input_6_10_2z00 = BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zz__r4_input_6_10_2z00(void);
	static obj_t BGl__readzd2linezd2zz__r4_input_6_10_2z00(obj_t, obj_t);
	extern bool_t rgc_buffer_insert_substring(obj_t, obj_t, long, long);
	static obj_t BGl_z62filezd2positionzd2ze3linez81zz__r4_input_6_10_2z00(obj_t,
		obj_t, obj_t);
	extern obj_t
		BGl_setzd2inputzd2portzd2positionz12zc0zz__r4_ports_6_10_1z00(obj_t, long);
	extern obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	extern bool_t rgc_buffer_eof_p(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_readzf2rpzf2zz__r4_input_6_10_2z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl__peekzd2charzd2zz__r4_input_6_10_2z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_readzd2linezd2zz__r4_input_6_10_2z00(obj_t);
	static obj_t BGl_z62readzf2rpz90zz__r4_input_6_10_2z00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zz__r4_input_6_10_2z00(void);
	extern obj_t BGl_stringzd2indexzd2zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zz__r4_input_6_10_2z00(void);
	static obj_t BGl_z62eofzd2objectzb0zz__r4_input_6_10_2z00(obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza32558ze33319ze5zz__r4_input_6_10_2z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_filezd2lineszd2zz__r4_input_6_10_2z00(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__r4_input_6_10_2z00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__r4_input_6_10_2z00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_readzd2linezd2newlinez00zz__r4_input_6_10_2z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_peekzd2charzd2zz__r4_input_6_10_2z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_readzd2ofzd2stringsz00zz__r4_input_6_10_2z00(obj_t);
	extern obj_t BGl_z62iozd2errorzb0zz__objectz00;
	static obj_t BGl__sendzd2filezd2zz__r4_input_6_10_2z00(obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zz__r4_input_6_10_2z00(void);
	extern long bgl_rgc_blit_string(obj_t, char *, long, long);
	static obj_t
		BGl_z62zc3z04anonymousza32558ze33320ze5zz__r4_input_6_10_2z00(obj_t);
	static obj_t BGl__readzd2stringzd2zz__r4_input_6_10_2z00(obj_t, obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza32558ze33321ze5zz__r4_input_6_10_2z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_readzd2fillzd2stringz12z12zz__r4_input_6_10_2z00(obj_t, long, long,
		obj_t);
	BGL_EXPORTED_DECL long BGl_sendzd2filezd2zz__r4_input_6_10_2z00(obj_t, obj_t,
		long, long);
	extern obj_t bgl_password(char *);
	static obj_t BGl__readzd2charsz12zc0zz__r4_input_6_10_2z00(obj_t, obj_t);
	extern obj_t BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t bgl_reverse_bang(obj_t);
	extern obj_t bgl_sendchars(obj_t, obj_t, long, long);
	static obj_t BGl__peekzd2bytezd2zz__r4_input_6_10_2z00(obj_t, obj_t);
	static obj_t BGl__unreadzd2charz12zc0zz__r4_input_6_10_2z00(obj_t, obj_t);
	static obj_t BGl_z62filezd2lineszb0zz__r4_input_6_10_2z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_unreadzd2substringz12zc0zz__r4_input_6_10_2z00(obj_t, long, long,
		obj_t);
	static obj_t BGl__readzd2charszd2zz__r4_input_6_10_2z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_eofzd2objectzf3z21zz__r4_input_6_10_2z00(obj_t);
	static obj_t BGl__charzd2readyzf3z21zz__r4_input_6_10_2z00(obj_t, obj_t);
	extern obj_t BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_peekzd2bytezd2zz__r4_input_6_10_2z00(obj_t);
	extern obj_t bgl_sendfile(obj_t, obj_t, long, long);
	static obj_t BGl_z62zc3z04anonymousza32706ze3ze5zz__r4_input_6_10_2z00(obj_t);
	extern bool_t rgc_buffer_insert_char(obj_t, int);
	static obj_t BGl_za2readzd2ofzd2stringszd2grammarza2zd2zz__r4_input_6_10_2z00
		= BUNSPEC;
	extern obj_t bstring_to_symbol(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_readzd2stringzd2zz__r4_input_6_10_2z00(obj_t);
	extern int rgc_buffer_unget_char(obj_t, int);
	BGL_EXPORTED_DECL obj_t BGl_unreadzd2stringz12zc0zz__r4_input_6_10_2z00(obj_t,
		obj_t);
	extern obj_t c_substring(obj_t, long, long);
	static obj_t BGl__readzd2charzd2zz__r4_input_6_10_2z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_readzf2lalrpzf2zz__r4_input_6_10_2z00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32561ze3ze5zz__r4_input_6_10_2z00(obj_t);
	static obj_t BGl_z62portzd2ze3stringzd2listz81zz__r4_input_6_10_2z00(obj_t,
		obj_t);
	extern long default_io_bufsiz;
	BGL_EXPORTED_DECL obj_t BGl_readzd2charszd2zz__r4_input_6_10_2z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_readzd2charsz12zc0zz__r4_input_6_10_2z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zz__r4_input_6_10_2z00(void);
	extern obj_t BGl_bigloozd2typezd2errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_readzd2charzd2zz__r4_input_6_10_2z00(obj_t);
	static obj_t BGl_z62filezd2ze3stringz53zz__r4_input_6_10_2z00(obj_t, obj_t);
	extern obj_t blit_string(obj_t, long, obj_t, long, long);
	BGL_EXPORTED_DECL bool_t BGl_charzd2readyzf3z21zz__r4_input_6_10_2z00(obj_t);
	extern obj_t bgl_find_runtime_type(obj_t);
	extern obj_t BGl_gunza7ipzd2sendcharsz75zz__gunza7ipza7(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_unreadzd2charz12zc0zz__r4_input_6_10_2z00(unsigned
		char, obj_t);
	static obj_t BGl_z62readzf2lalrpz90zz__r4_input_6_10_2z00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern bool_t BGl_stringzd2prefixzf3z21zz__r4_strings_6_7z00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32547ze3ze5zz__r4_input_6_10_2z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_filezd2positionzd2ze3lineze3zz__r4_input_6_10_2z00(int, obj_t);
	extern bool_t rgc_fill_buffer(obj_t);
	static obj_t BGl__readzd2bytezd2zz__r4_input_6_10_2z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_filezd2ze3stringz31zz__r4_input_6_10_2z00(obj_t);
	static obj_t BGl__readzd2lineszd2zz__r4_input_6_10_2z00(obj_t, obj_t);
	extern obj_t make_string(long, unsigned char);
	static obj_t BGl__sendzd2charszd2zz__r4_input_6_10_2z00(obj_t, obj_t);
	extern obj_t BGl_withzd2inputzd2fromzd2filezd2zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	extern obj_t bgl_close_input_port(obj_t);
	static obj_t BGl__readzd2linezd2newlinez00zz__r4_input_6_10_2z00(obj_t,
		obj_t);
	extern bool_t bgl_rgc_charready(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31398ze3ze5zz__r4_input_6_10_2z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_readzd2bytezd2zz__r4_input_6_10_2z00(obj_t);
	extern obj_t make_string_sans_fill(long);
	BGL_EXPORTED_DECL obj_t
		BGl_portzd2ze3stringzd2listze3zz__r4_input_6_10_2z00(obj_t);
	static obj_t BGl__readzd2fillzd2stringz12z12zz__r4_input_6_10_2z00(obj_t,
		obj_t);
	extern obj_t rgc_buffer_substring(obj_t, long, long);
	BGL_EXPORTED_DECL obj_t BGl_readzd2lineszd2zz__r4_input_6_10_2z00(obj_t);
	static obj_t BGl_symbol3444z00zz__r4_input_6_10_2z00 = BUNSPEC;
	BGL_EXPORTED_DECL long BGl_sendzd2charszd2zz__r4_input_6_10_2z00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl__unreadzd2substringz12zc0zz__r4_input_6_10_2z00(obj_t,
		obj_t);
	static obj_t BGl__readzd2ofzd2stringsz00zz__r4_input_6_10_2z00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32558ze3ze5zz__r4_input_6_10_2z00(obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_peekzd2bytezd2envz00zz__r4_input_6_10_2z00,
		BgL_bgl__peekza7d2byteza7d2za73499za7, opt_generic_entry,
		BGl__peekzd2bytezd2zz__r4_input_6_10_2z00, BFALSE, -1);
	      DEFINE_STRING(BGl_string3490z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3490za700za7za7_3500za7, "_send-file", 10);
	      DEFINE_STRING(BGl_string3491z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3491za700za7za7_3501za7, "pair", 4);
	      DEFINE_STRING(BGl_string3492z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3492za700za7za7_3502za7, "&file-lines", 11);
	      DEFINE_STRING(BGl_string3494z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3494za700za7za7_3503za7, "file-lines", 10);
	      DEFINE_STRING(BGl_string3495z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3495za700za7za7_3504za7, "Illegal files", 13);
	      DEFINE_STRING(BGl_string3496z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3496za700za7za7_3505za7, "&file-position->line", 20);
	      DEFINE_STRING(BGl_string3497z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3497za700za7za7_3506za7, "_password", 9);
	      DEFINE_STRING(BGl_string3498z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3498za700za7za7_3507za7, "__r4_input_6_10_2", 17);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_readzd2bytezd2envz00zz__r4_input_6_10_2z00,
		BgL_bgl__readza7d2byteza7d2za73508za7, opt_generic_entry,
		BGl__readzd2bytezd2zz__r4_input_6_10_2z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_readzf2lalrpzd2envz20zz__r4_input_6_10_2z00,
		BgL_bgl_za762readza7f2lalrpza73509za7, va_generic_entry,
		BGl_z62readzf2lalrpz90zz__r4_input_6_10_2z00, BUNSPEC, -4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_readzd2charszd2envz00zz__r4_input_6_10_2z00,
		BgL_bgl__readza7d2charsza7d23510z00, opt_generic_entry,
		BGl__readzd2charszd2zz__r4_input_6_10_2z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_sendzd2filezd2envz00zz__r4_input_6_10_2z00,
		BgL_bgl__sendza7d2fileza7d2za73511za7, opt_generic_entry,
		BGl__sendzd2filezd2zz__r4_input_6_10_2z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_filezd2ze3stringzd2envze3zz__r4_input_6_10_2z00,
		BgL_bgl_za762fileza7d2za7e3str3512za7,
		BGl_z62filezd2ze3stringz53zz__r4_input_6_10_2z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_filezd2lineszd2envz00zz__r4_input_6_10_2z00,
		BgL_bgl_za762fileza7d2linesza73513za7,
		BGl_z62filezd2lineszb0zz__r4_input_6_10_2z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_readzd2linezd2newlinezd2envzd2zz__r4_input_6_10_2z00,
		BgL_bgl__readza7d2lineza7d2n3514z00, opt_generic_entry,
		BGl__readzd2linezd2newlinez00zz__r4_input_6_10_2z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_peekzd2charzd2envz00zz__r4_input_6_10_2z00,
		BgL_bgl__peekza7d2charza7d2za73515za7, opt_generic_entry,
		BGl__peekzd2charzd2zz__r4_input_6_10_2z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_readzd2charsz12zd2envz12zz__r4_input_6_10_2z00,
		BgL_bgl__readza7d2charsza7123516z00, opt_generic_entry,
		BGl__readzd2charsz12zc0zz__r4_input_6_10_2z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_readzd2charzd2envz00zz__r4_input_6_10_2z00,
		BgL_bgl__readza7d2charza7d2za73517za7, opt_generic_entry,
		BGl__readzd2charzd2zz__r4_input_6_10_2z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_charzd2readyzf3zd2envzf3zz__r4_input_6_10_2z00,
		BgL_bgl__charza7d2readyza7f33518z00, opt_generic_entry,
		BGl__charzd2readyzf3z21zz__r4_input_6_10_2z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_unreadzd2charz12zd2envz12zz__r4_input_6_10_2z00,
		BgL_bgl__unreadza7d2charza713519z00, opt_generic_entry,
		BGl__unreadzd2charz12zc0zz__r4_input_6_10_2z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sendzd2charszf2siza7ezd2envz55zz__r4_input_6_10_2z00,
		BgL_bgl_za762sendza7d2charsza73520za7,
		BGl_z62sendzd2charszf2siza7eze5zz__r4_input_6_10_2z00, 0L, BUNSPEC, 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_readzf2rpzd2envz20zz__r4_input_6_10_2z00,
		BgL_bgl_za762readza7f2rpza790za73521z00, va_generic_entry,
		BGl_z62readzf2rpz90zz__r4_input_6_10_2z00, BUNSPEC, -3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_readzd2linezd2envz00zz__r4_input_6_10_2z00,
		BgL_bgl__readza7d2lineza7d2za73522za7, opt_generic_entry,
		BGl__readzd2linezd2zz__r4_input_6_10_2z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_readzd2stringzd2envz00zz__r4_input_6_10_2z00,
		BgL_bgl__readza7d2stringza7d3523z00, opt_generic_entry,
		BGl__readzd2stringzd2zz__r4_input_6_10_2z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_unreadzd2stringz12zd2envz12zz__r4_input_6_10_2z00,
		BgL_bgl__unreadza7d2string3524za7, opt_generic_entry,
		BGl__unreadzd2stringz12zc0zz__r4_input_6_10_2z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_readzd2ofzd2stringszd2envzd2zz__r4_input_6_10_2z00,
		BgL_bgl__readza7d2ofza7d2str3525z00, opt_generic_entry,
		BGl__readzd2ofzd2stringsz00zz__r4_input_6_10_2z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_readzd2fillzd2stringz12zd2envzc0zz__r4_input_6_10_2z00,
		BgL_bgl__readza7d2fillza7d2s3526z00, opt_generic_entry,
		BGl__readzd2fillzd2stringz12z12zz__r4_input_6_10_2z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_eofzd2objectzd2envz00zz__r4_input_6_10_2z00,
		BgL_bgl_za762eofza7d2objectza73527za7,
		BGl_z62eofzd2objectzb0zz__r4_input_6_10_2z00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string3440z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3440za700za7za7_3528za7, "regular-grammar", 15);
	      DEFINE_STRING(BGl_string3441z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3441za700za7za7_3529za7, "Illegal match", 13);
	      DEFINE_STRING(BGl_string3442z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3442za700za7za7_3530za7, "Illegal range `~a'", 18);
	      DEFINE_STRING(BGl_string3443z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3443za700za7za7_3531za7, "the-substring", 13);
	      DEFINE_STRING(BGl_string3445z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3445za700za7za7_3532za7, "read/rp", 7);
	      DEFINE_STRING(BGl_string3446z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3446za700za7za7_3533za7, "Grammar arity mismatch", 22);
	      DEFINE_STRING(BGl_string3447z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3447za700za7za7_3534za7, "/tmp/bigloo/runtime/Ieee/input.scm",
		34);
	      DEFINE_STRING(BGl_string3448z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3448za700za7za7_3535za7, "&read/rp", 8);
	      DEFINE_STRING(BGl_string3449z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3449za700za7za7_3536za7, "procedure", 9);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_eofzd2objectzf3zd2envzf3zz__r4_input_6_10_2z00,
		BgL_bgl_za762eofza7d2objectza73537za7,
		BGl_z62eofzd2objectzf3z43zz__r4_input_6_10_2z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3450z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3450za700za7za7_3538za7, "input-port", 10);
	      DEFINE_STRING(BGl_string3451z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3451za700za7za7_3539za7, "&read/lalrp", 11);
	      DEFINE_STRING(BGl_string3452z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3452za700za7za7_3540za7, "_char-ready?", 12);
	      DEFINE_STRING(BGl_string3453z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3453za700za7za7_3541za7, "", 0);
	      DEFINE_STRING(BGl_string3455z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3455za700za7za7_3542za7, "read-chars", 10);
	      DEFINE_STRING(BGl_string3456z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3456za700za7za7_3543za7, "integer", 7);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_passwordzd2envzd2zz__r4_input_6_10_2z00,
		BgL_bgl__passwordza700za7za7__3544za7, opt_generic_entry,
		BGl__passwordz00zz__r4_input_6_10_2z00, BFALSE, -1);
	      DEFINE_STRING(BGl_string3457z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3457za700za7za7_3545za7, "Illegal negative length", 23);
	      DEFINE_STRING(BGl_string3458z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3458za700za7za7_3546za7, "_read-chars!", 12);
	      DEFINE_STRING(BGl_string3459z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3459za700za7za7_3547za7, "bstring", 7);
	      DEFINE_STRING(BGl_string3461z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3461za700za7za7_3548za7, "read-chars!", 11);
	      DEFINE_STRING(BGl_string3462z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3462za700za7za7_3549za7, "_read-fill-string!", 18);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_portzd2ze3stringzd2listzd2envz31zz__r4_input_6_10_2z00,
		BgL_bgl_za762portza7d2za7e3str3550za7,
		BGl_z62portzd2ze3stringzd2listz81zz__r4_input_6_10_2z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3463z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3463za700za7za7_3551za7, "bint", 4);
	      DEFINE_STRING(BGl_string3464z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3464za700za7za7_3552za7, "_unread-char!", 13);
	      DEFINE_STRING(BGl_string3465z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3465za700za7za7_3553za7, "bchar", 5);
	      DEFINE_STRING(BGl_string3467z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3467za700za7za7_3554za7, "unread-char!", 12);
	      DEFINE_STRING(BGl_string3468z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3468za700za7za7_3555za7, "Unread char failed", 18);
	      DEFINE_STRING(BGl_string3469z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3469za700za7za7_3556za7, "_unread-string!", 15);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_readzd2lineszd2envz00zz__r4_input_6_10_2z00,
		BgL_bgl__readza7d2linesza7d23557z00, opt_generic_entry,
		BGl__readzd2lineszd2zz__r4_input_6_10_2z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_sendzd2charszd2envz00zz__r4_input_6_10_2z00,
		BgL_bgl__sendza7d2charsza7d23558z00, opt_generic_entry,
		BGl__sendzd2charszd2zz__r4_input_6_10_2z00, BFALSE, -1);
	      DEFINE_STRING(BGl_string3471z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3471za700za7za7_3559za7, "unread-string!", 14);
	      DEFINE_STRING(BGl_string3472z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3472za700za7za7_3560za7, "Unread string failed", 20);
	      DEFINE_STRING(BGl_string3473z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3473za700za7za7_3561za7, "_unread-substring!", 18);
	      DEFINE_STRING(BGl_string3475z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3475za700za7za7_3562za7, "unread-substring!", 17);
	      DEFINE_STRING(BGl_string3476z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3476za700za7za7_3563za7, "Invalid positional parameters", 29);
	      DEFINE_STRING(BGl_string3478z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3478za700za7za7_3564za7, "unread-sustring!", 16);
	      DEFINE_STRING(BGl_string3479z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3479za700za7za7_3565za7, "&port->string-list", 18);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_unreadzd2substringz12zd2envz12zz__r4_input_6_10_2z00,
		BgL_bgl__unreadza7d2substr3566za7, opt_generic_entry,
		BGl__unreadzd2substringz12zc0zz__r4_input_6_10_2z00, BFALSE, -1);
	      DEFINE_STRING(BGl_string3480z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3480za700za7za7_3567za7, "file:", 5);
	      DEFINE_STRING(BGl_string3481z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3481za700za7za7_3568za7, "&file->string", 13);
	      DEFINE_STRING(BGl_string3482z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3482za700za7za7_3569za7, "&send-chars/size", 16);
	      DEFINE_STRING(BGl_string3483z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3483za700za7za7_3570za7, "output-port", 11);
	      DEFINE_STRING(BGl_string3484z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3484za700za7za7_3571za7, "belong", 6);
	      DEFINE_STRING(BGl_string3485z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3485za700za7za7_3572za7, "_send-chars", 11);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_filezd2positionzd2ze3linezd2envz31zz__r4_input_6_10_2z00,
		BgL_bgl_za762fileza7d2positi3573z00,
		BGl_z62filezd2positionzd2ze3linez81zz__r4_input_6_10_2z00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string3487z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3487za700za7za7_3574za7, "send-chars", 10);
	      DEFINE_STRING(BGl_string3488z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3488za700za7za7_3575za7, "Illegal size", 12);
	      DEFINE_STRING(BGl_string3489z00zz__r4_input_6_10_2z00,
		BgL_bgl_string3489za700za7za7_3576za7, "Illegal offset", 14);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_symbol3454z00zz__r4_input_6_10_2z00));
		     ADD_ROOT((void *) (&BGl_symbol3460z00zz__r4_input_6_10_2z00));
		     ADD_ROOT((void *) (&BGl_symbol3466z00zz__r4_input_6_10_2z00));
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__r4_input_6_10_2z00));
		     ADD_ROOT((void *) (&BGl_symbol3470z00zz__r4_input_6_10_2z00));
		     ADD_ROOT((void *) (&BGl_symbol3474z00zz__r4_input_6_10_2z00));
		     ADD_ROOT((void *) (&BGl_symbol3477z00zz__r4_input_6_10_2z00));
		     ADD_ROOT((void *) (&BGl_symbol3486z00zz__r4_input_6_10_2z00));
		     ADD_ROOT((void *) (&BGl_symbol3493z00zz__r4_input_6_10_2z00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2readzd2ofzd2stringszd2grammarza2zd2zz__r4_input_6_10_2z00));
		     ADD_ROOT((void *) (&BGl_symbol3444z00zz__r4_input_6_10_2z00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(long
		BgL_checksumz00_6617, char *BgL_fromz00_6618)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__r4_input_6_10_2z00))
				{
					BGl_requirezd2initializa7ationz75zz__r4_input_6_10_2z00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__r4_input_6_10_2z00();
					BGl_cnstzd2initzd2zz__r4_input_6_10_2z00();
					BGl_importedzd2moduleszd2initz00zz__r4_input_6_10_2z00();
					return BGl_toplevelzd2initzd2zz__r4_input_6_10_2z00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__r4_input_6_10_2z00(void)
	{
		{	/* Ieee/input.scm 14 */
			BGl_symbol3444z00zz__r4_input_6_10_2z00 =
				bstring_to_symbol(BGl_string3445z00zz__r4_input_6_10_2z00);
			BGl_symbol3454z00zz__r4_input_6_10_2z00 =
				bstring_to_symbol(BGl_string3455z00zz__r4_input_6_10_2z00);
			BGl_symbol3460z00zz__r4_input_6_10_2z00 =
				bstring_to_symbol(BGl_string3461z00zz__r4_input_6_10_2z00);
			BGl_symbol3466z00zz__r4_input_6_10_2z00 =
				bstring_to_symbol(BGl_string3467z00zz__r4_input_6_10_2z00);
			BGl_symbol3470z00zz__r4_input_6_10_2z00 =
				bstring_to_symbol(BGl_string3471z00zz__r4_input_6_10_2z00);
			BGl_symbol3474z00zz__r4_input_6_10_2z00 =
				bstring_to_symbol(BGl_string3475z00zz__r4_input_6_10_2z00);
			BGl_symbol3477z00zz__r4_input_6_10_2z00 =
				bstring_to_symbol(BGl_string3478z00zz__r4_input_6_10_2z00);
			BGl_symbol3486z00zz__r4_input_6_10_2z00 =
				bstring_to_symbol(BGl_string3487z00zz__r4_input_6_10_2z00);
			return (BGl_symbol3493z00zz__r4_input_6_10_2z00 =
				bstring_to_symbol(BGl_string3494z00zz__r4_input_6_10_2z00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__r4_input_6_10_2z00(void)
	{
		{	/* Ieee/input.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__r4_input_6_10_2z00(void)
	{
		{	/* Ieee/input.scm 14 */
			{	/* Ieee/input.scm 307 */
				obj_t BgL_zc3z04anonymousza31398ze3z87_6243;

				{
					int BgL_tmpz00_6636;

					BgL_tmpz00_6636 = (int) (0L);
					BgL_zc3z04anonymousza31398ze3z87_6243 =
						MAKE_EL_PROCEDURE(BgL_tmpz00_6636);
				}
				return (BGl_za2readzd2ofzd2stringszd2grammarza2zd2zz__r4_input_6_10_2z00
					= BgL_zc3z04anonymousza31398ze3z87_6243, BUNSPEC);
			}
		}

	}



/* &<@anonymous:1398> */
	obj_t BGl_z62zc3z04anonymousza31398ze3ze5zz__r4_input_6_10_2z00(obj_t
		BgL_envz00_6244, obj_t BgL_iportz00_6245)
	{
		{	/* Ieee/input.scm 307 */
			{
				obj_t BgL_iportz00_6512;
				long BgL_lastzd2matchzd2_6513;
				long BgL_forwardz00_6514;
				long BgL_bufposz00_6515;
				obj_t BgL_iportz00_6495;
				long BgL_lastzd2matchzd2_6496;
				long BgL_forwardz00_6497;
				long BgL_bufposz00_6498;
				obj_t BgL_iportz00_6487;
				long BgL_lastzd2matchzd2_6488;
				long BgL_forwardz00_6489;
				long BgL_bufposz00_6490;
				obj_t BgL_iportz00_6471;
				long BgL_lastzd2matchzd2_6472;
				long BgL_forwardz00_6473;
				long BgL_bufposz00_6474;
				obj_t BgL_iportz00_6464;
				long BgL_lastzd2matchzd2_6465;
				long BgL_forwardz00_6466;
				long BgL_bufposz00_6467;
				obj_t BgL_iportz00_6456;
				long BgL_lastzd2matchzd2_6457;
				long BgL_forwardz00_6458;
				long BgL_bufposz00_6459;
				obj_t BgL_iportz00_6448;
				long BgL_lastzd2matchzd2_6449;
				long BgL_forwardz00_6450;
				long BgL_bufposz00_6451;
				int BgL_minz00_6436;
				int BgL_maxz00_6437;

			BgL_ignorez00_6420:
				{	/* Ieee/input.scm 307 */
					obj_t BgL_tmpz00_6639;

					BgL_tmpz00_6639 = ((obj_t) BgL_iportz00_6245);
					RGC_START_MATCH(BgL_tmpz00_6639);
				}
				{	/* Ieee/input.scm 307 */
					long BgL_matchz00_6430;

					{	/* Ieee/input.scm 307 */
						long BgL_arg1578z00_6431;
						long BgL_arg1579z00_6432;

						{	/* Ieee/input.scm 307 */
							obj_t BgL_tmpz00_6642;

							BgL_tmpz00_6642 = ((obj_t) BgL_iportz00_6245);
							BgL_arg1578z00_6431 = RGC_BUFFER_FORWARD(BgL_tmpz00_6642);
						}
						{	/* Ieee/input.scm 307 */
							obj_t BgL_tmpz00_6645;

							BgL_tmpz00_6645 = ((obj_t) BgL_iportz00_6245);
							BgL_arg1579z00_6432 = RGC_BUFFER_BUFPOS(BgL_tmpz00_6645);
						}
						BgL_iportz00_6464 = BgL_iportz00_6245;
						BgL_lastzd2matchzd2_6465 = 4L;
						BgL_forwardz00_6466 = BgL_arg1578z00_6431;
						BgL_bufposz00_6467 = BgL_arg1579z00_6432;
					BgL_statezd20zd21103z00_6425:
						if ((BgL_forwardz00_6466 == BgL_bufposz00_6467))
							{	/* Ieee/input.scm 307 */
								if (rgc_fill_buffer(((obj_t) BgL_iportz00_6464)))
									{	/* Ieee/input.scm 307 */
										long BgL_arg1446z00_6468;
										long BgL_arg1447z00_6469;

										{	/* Ieee/input.scm 307 */
											obj_t BgL_tmpz00_6653;

											BgL_tmpz00_6653 = ((obj_t) BgL_iportz00_6464);
											BgL_arg1446z00_6468 = RGC_BUFFER_FORWARD(BgL_tmpz00_6653);
										}
										{	/* Ieee/input.scm 307 */
											obj_t BgL_tmpz00_6656;

											BgL_tmpz00_6656 = ((obj_t) BgL_iportz00_6464);
											BgL_arg1447z00_6469 = RGC_BUFFER_BUFPOS(BgL_tmpz00_6656);
										}
										{
											long BgL_bufposz00_6660;
											long BgL_forwardz00_6659;

											BgL_forwardz00_6659 = BgL_arg1446z00_6468;
											BgL_bufposz00_6660 = BgL_arg1447z00_6469;
											BgL_bufposz00_6467 = BgL_bufposz00_6660;
											BgL_forwardz00_6466 = BgL_forwardz00_6659;
											goto BgL_statezd20zd21103z00_6425;
										}
									}
								else
									{	/* Ieee/input.scm 307 */
										BgL_matchz00_6430 = BgL_lastzd2matchzd2_6465;
									}
							}
						else
							{	/* Ieee/input.scm 307 */
								int BgL_curz00_6470;

								{	/* Ieee/input.scm 307 */
									obj_t BgL_tmpz00_6661;

									BgL_tmpz00_6661 = ((obj_t) BgL_iportz00_6464);
									BgL_curz00_6470 =
										RGC_BUFFER_GET_CHAR(BgL_tmpz00_6661, BgL_forwardz00_6466);
								}
								{	/* Ieee/input.scm 307 */

									if (((long) (BgL_curz00_6470) == 34L))
										{	/* Ieee/input.scm 307 */
											BgL_iportz00_6495 = BgL_iportz00_6464;
											BgL_lastzd2matchzd2_6496 = BgL_lastzd2matchzd2_6465;
											BgL_forwardz00_6497 = (1L + BgL_forwardz00_6466);
											BgL_bufposz00_6498 = BgL_bufposz00_6467;
										BgL_statezd23zd21106z00_6428:
											{	/* Ieee/input.scm 307 */
												long BgL_newzd2matchzd2_6499;

												{	/* Ieee/input.scm 307 */
													obj_t BgL_tmpz00_6667;

													BgL_tmpz00_6667 = ((obj_t) BgL_iportz00_6495);
													RGC_STOP_MATCH(BgL_tmpz00_6667, BgL_forwardz00_6497);
												}
												BgL_newzd2matchzd2_6499 = 1L;
												if ((BgL_forwardz00_6497 == BgL_bufposz00_6498))
													{	/* Ieee/input.scm 307 */
														if (rgc_fill_buffer(((obj_t) BgL_iportz00_6495)))
															{	/* Ieee/input.scm 307 */
																long BgL_arg1412z00_6500;
																long BgL_arg1413z00_6501;

																{	/* Ieee/input.scm 307 */
																	obj_t BgL_tmpz00_6675;

																	BgL_tmpz00_6675 = ((obj_t) BgL_iportz00_6495);
																	BgL_arg1412z00_6500 =
																		RGC_BUFFER_FORWARD(BgL_tmpz00_6675);
																}
																{	/* Ieee/input.scm 307 */
																	obj_t BgL_tmpz00_6678;

																	BgL_tmpz00_6678 = ((obj_t) BgL_iportz00_6495);
																	BgL_arg1413z00_6501 =
																		RGC_BUFFER_BUFPOS(BgL_tmpz00_6678);
																}
																{
																	long BgL_bufposz00_6682;
																	long BgL_forwardz00_6681;

																	BgL_forwardz00_6681 = BgL_arg1412z00_6500;
																	BgL_bufposz00_6682 = BgL_arg1413z00_6501;
																	BgL_bufposz00_6498 = BgL_bufposz00_6682;
																	BgL_forwardz00_6497 = BgL_forwardz00_6681;
																	goto BgL_statezd23zd21106z00_6428;
																}
															}
														else
															{	/* Ieee/input.scm 307 */
																BgL_matchz00_6430 = BgL_newzd2matchzd2_6499;
															}
													}
												else
													{	/* Ieee/input.scm 307 */
														int BgL_curz00_6502;

														{	/* Ieee/input.scm 307 */
															obj_t BgL_tmpz00_6683;

															BgL_tmpz00_6683 = ((obj_t) BgL_iportz00_6495);
															BgL_curz00_6502 =
																RGC_BUFFER_GET_CHAR(BgL_tmpz00_6683,
																BgL_forwardz00_6497);
														}
														{	/* Ieee/input.scm 307 */

															if (((long) (BgL_curz00_6502) == 92L))
																{	/* Ieee/input.scm 307 */
																	long BgL_arg1415z00_6503;

																	BgL_arg1415z00_6503 =
																		(1L + BgL_forwardz00_6497);
																	{
																		long BgL_forwardz00_6505;
																		long BgL_bufposz00_6506;

																		BgL_forwardz00_6505 = BgL_arg1415z00_6503;
																		BgL_bufposz00_6506 = BgL_bufposz00_6498;
																	BgL_statezd26zd21109z00_6504:
																		if (
																			(BgL_forwardz00_6505 ==
																				BgL_bufposz00_6506))
																			{	/* Ieee/input.scm 307 */
																				if (rgc_fill_buffer(
																						((obj_t) BgL_iportz00_6495)))
																					{	/* Ieee/input.scm 307 */
																						long BgL_arg1468z00_6507;
																						long BgL_arg1469z00_6508;

																						{	/* Ieee/input.scm 307 */
																							obj_t BgL_tmpz00_6695;

																							BgL_tmpz00_6695 =
																								((obj_t) BgL_iportz00_6495);
																							BgL_arg1468z00_6507 =
																								RGC_BUFFER_FORWARD
																								(BgL_tmpz00_6695);
																						}
																						{	/* Ieee/input.scm 307 */
																							obj_t BgL_tmpz00_6698;

																							BgL_tmpz00_6698 =
																								((obj_t) BgL_iportz00_6495);
																							BgL_arg1469z00_6508 =
																								RGC_BUFFER_BUFPOS
																								(BgL_tmpz00_6698);
																						}
																						{
																							long BgL_bufposz00_6702;
																							long BgL_forwardz00_6701;

																							BgL_forwardz00_6701 =
																								BgL_arg1468z00_6507;
																							BgL_bufposz00_6702 =
																								BgL_arg1469z00_6508;
																							BgL_bufposz00_6506 =
																								BgL_bufposz00_6702;
																							BgL_forwardz00_6505 =
																								BgL_forwardz00_6701;
																							goto BgL_statezd26zd21109z00_6504;
																						}
																					}
																				else
																					{	/* Ieee/input.scm 307 */
																						BgL_matchz00_6430 =
																							BgL_newzd2matchzd2_6499;
																					}
																			}
																		else
																			{	/* Ieee/input.scm 307 */
																				int BgL_curz00_6509;

																				{	/* Ieee/input.scm 307 */
																					obj_t BgL_tmpz00_6703;

																					BgL_tmpz00_6703 =
																						((obj_t) BgL_iportz00_6495);
																					BgL_curz00_6509 =
																						RGC_BUFFER_GET_CHAR(BgL_tmpz00_6703,
																						BgL_forwardz00_6505);
																				}
																				{	/* Ieee/input.scm 307 */

																					if (((long) (BgL_curz00_6509) == 10L))
																						{	/* Ieee/input.scm 307 */
																							BgL_matchz00_6430 =
																								BgL_newzd2matchzd2_6499;
																						}
																					else
																						{	/* Ieee/input.scm 307 */
																							BgL_iportz00_6471 =
																								BgL_iportz00_6495;
																							BgL_lastzd2matchzd2_6472 =
																								BgL_newzd2matchzd2_6499;
																							BgL_forwardz00_6473 =
																								(1L + BgL_forwardz00_6505);
																							BgL_bufposz00_6474 =
																								BgL_bufposz00_6506;
																						BgL_statezd24zd21107z00_6426:
																							if (
																								(BgL_forwardz00_6473 ==
																									BgL_bufposz00_6474))
																								{	/* Ieee/input.scm 307 */
																									if (rgc_fill_buffer(
																											((obj_t)
																												BgL_iportz00_6471)))
																										{	/* Ieee/input.scm 307 */
																											long BgL_arg1434z00_6475;
																											long BgL_arg1435z00_6476;

																											{	/* Ieee/input.scm 307 */
																												obj_t BgL_tmpz00_6714;

																												BgL_tmpz00_6714 =
																													((obj_t)
																													BgL_iportz00_6471);
																												BgL_arg1434z00_6475 =
																													RGC_BUFFER_FORWARD
																													(BgL_tmpz00_6714);
																											}
																											{	/* Ieee/input.scm 307 */
																												obj_t BgL_tmpz00_6717;

																												BgL_tmpz00_6717 =
																													((obj_t)
																													BgL_iportz00_6471);
																												BgL_arg1435z00_6476 =
																													RGC_BUFFER_BUFPOS
																													(BgL_tmpz00_6717);
																											}
																											{
																												long BgL_bufposz00_6721;
																												long
																													BgL_forwardz00_6720;
																												BgL_forwardz00_6720 =
																													BgL_arg1434z00_6475;
																												BgL_bufposz00_6721 =
																													BgL_arg1435z00_6476;
																												BgL_bufposz00_6474 =
																													BgL_bufposz00_6721;
																												BgL_forwardz00_6473 =
																													BgL_forwardz00_6720;
																												goto
																													BgL_statezd24zd21107z00_6426;
																											}
																										}
																									else
																										{	/* Ieee/input.scm 307 */
																											BgL_matchz00_6430 =
																												BgL_lastzd2matchzd2_6472;
																										}
																								}
																							else
																								{	/* Ieee/input.scm 307 */
																									int BgL_curz00_6477;

																									{	/* Ieee/input.scm 307 */
																										obj_t BgL_tmpz00_6722;

																										BgL_tmpz00_6722 =
																											((obj_t)
																											BgL_iportz00_6471);
																										BgL_curz00_6477 =
																											RGC_BUFFER_GET_CHAR
																											(BgL_tmpz00_6722,
																											BgL_forwardz00_6473);
																									}
																									{	/* Ieee/input.scm 307 */

																										if (
																											((long) (BgL_curz00_6477)
																												== 92L))
																											{	/* Ieee/input.scm 307 */
																												long
																													BgL_arg1437z00_6478;
																												BgL_arg1437z00_6478 =
																													(1L +
																													BgL_forwardz00_6473);
																												{
																													long
																														BgL_forwardz00_6480;
																													long
																														BgL_bufposz00_6481;
																													BgL_forwardz00_6480 =
																														BgL_arg1437z00_6478;
																													BgL_bufposz00_6481 =
																														BgL_bufposz00_6474;
																												BgL_statezd26zd21109z00_6479:
																													if (
																														(BgL_forwardz00_6480
																															==
																															BgL_bufposz00_6481))
																														{	/* Ieee/input.scm 307 */
																															if (rgc_fill_buffer(((obj_t) BgL_iportz00_6471)))
																																{	/* Ieee/input.scm 307 */
																																	long
																																		BgL_arg1468z00_6482;
																																	long
																																		BgL_arg1469z00_6483;
																																	{	/* Ieee/input.scm 307 */
																																		obj_t
																																			BgL_tmpz00_6734;
																																		BgL_tmpz00_6734
																																			=
																																			((obj_t)
																																			BgL_iportz00_6471);
																																		BgL_arg1468z00_6482
																																			=
																																			RGC_BUFFER_FORWARD
																																			(BgL_tmpz00_6734);
																																	}
																																	{	/* Ieee/input.scm 307 */
																																		obj_t
																																			BgL_tmpz00_6737;
																																		BgL_tmpz00_6737
																																			=
																																			((obj_t)
																																			BgL_iportz00_6471);
																																		BgL_arg1469z00_6483
																																			=
																																			RGC_BUFFER_BUFPOS
																																			(BgL_tmpz00_6737);
																																	}
																																	{
																																		long
																																			BgL_bufposz00_6741;
																																		long
																																			BgL_forwardz00_6740;
																																		BgL_forwardz00_6740
																																			=
																																			BgL_arg1468z00_6482;
																																		BgL_bufposz00_6741
																																			=
																																			BgL_arg1469z00_6483;
																																		BgL_bufposz00_6481
																																			=
																																			BgL_bufposz00_6741;
																																		BgL_forwardz00_6480
																																			=
																																			BgL_forwardz00_6740;
																																		goto
																																			BgL_statezd26zd21109z00_6479;
																																	}
																																}
																															else
																																{	/* Ieee/input.scm 307 */
																																	BgL_matchz00_6430
																																		=
																																		BgL_lastzd2matchzd2_6472;
																																}
																														}
																													else
																														{	/* Ieee/input.scm 307 */
																															int
																																BgL_curz00_6484;
																															{	/* Ieee/input.scm 307 */
																																obj_t
																																	BgL_tmpz00_6742;
																																BgL_tmpz00_6742
																																	=
																																	((obj_t)
																																	BgL_iportz00_6471);
																																BgL_curz00_6484
																																	=
																																	RGC_BUFFER_GET_CHAR
																																	(BgL_tmpz00_6742,
																																	BgL_forwardz00_6480);
																															}
																															{	/* Ieee/input.scm 307 */

																																if (
																																	((long)
																																		(BgL_curz00_6484)
																																		== 10L))
																																	{	/* Ieee/input.scm 307 */
																																		BgL_matchz00_6430
																																			=
																																			BgL_lastzd2matchzd2_6472;
																																	}
																																else
																																	{
																																		long
																																			BgL_bufposz00_6750;
																																		long
																																			BgL_forwardz00_6748;
																																		BgL_forwardz00_6748
																																			=
																																			(1L +
																																			BgL_forwardz00_6480);
																																		BgL_bufposz00_6750
																																			=
																																			BgL_bufposz00_6481;
																																		BgL_bufposz00_6474
																																			=
																																			BgL_bufposz00_6750;
																																		BgL_forwardz00_6473
																																			=
																																			BgL_forwardz00_6748;
																																		goto
																																			BgL_statezd24zd21107z00_6426;
																																	}
																															}
																														}
																												}
																											}
																										else
																											{	/* Ieee/input.scm 307 */
																												if (
																													((long)
																														(BgL_curz00_6477) ==
																														34L))
																													{	/* Ieee/input.scm 307 */
																														long
																															BgL_arg1439z00_6485;
																														BgL_arg1439z00_6485
																															=
																															(1L +
																															BgL_forwardz00_6473);
																														{	/* Ieee/input.scm 307 */
																															long
																																BgL_newzd2matchzd2_6486;
																															{	/* Ieee/input.scm 307 */
																																obj_t
																																	BgL_tmpz00_6755;
																																BgL_tmpz00_6755
																																	=
																																	((obj_t)
																																	BgL_iportz00_6471);
																																RGC_STOP_MATCH
																																	(BgL_tmpz00_6755,
																																	BgL_arg1439z00_6485);
																															}
																															BgL_newzd2matchzd2_6486
																																= 2L;
																															BgL_matchz00_6430
																																=
																																BgL_newzd2matchzd2_6486;
																													}}
																												else
																													{	/* Ieee/input.scm 307 */
																														bool_t
																															BgL_test3594z00_6758;
																														if (((long)
																																(BgL_curz00_6477)
																																== 34L))
																															{	/* Ieee/input.scm 307 */
																																BgL_test3594z00_6758
																																	=
																																	((bool_t) 1);
																															}
																														else
																															{	/* Ieee/input.scm 307 */
																																BgL_test3594z00_6758
																																	=
																																	((long)
																																	(BgL_curz00_6477)
																																	== 92L);
																															}
																														if (BgL_test3594z00_6758)
																															{	/* Ieee/input.scm 307 */
																																BgL_matchz00_6430
																																	=
																																	BgL_lastzd2matchzd2_6472;
																															}
																														else
																															{
																																long
																																	BgL_forwardz00_6764;
																																BgL_forwardz00_6764
																																	=
																																	(1L +
																																	BgL_forwardz00_6473);
																																BgL_forwardz00_6473
																																	=
																																	BgL_forwardz00_6764;
																																goto
																																	BgL_statezd24zd21107z00_6426;
																															}
																													}
																											}
																									}
																								}
																						}
																				}
																			}
																	}
																}
															else
																{	/* Ieee/input.scm 307 */
																	if (((long) (BgL_curz00_6502) == 34L))
																		{	/* Ieee/input.scm 307 */
																			long BgL_arg1417z00_6510;

																			BgL_arg1417z00_6510 =
																				(1L + BgL_forwardz00_6497);
																			{	/* Ieee/input.scm 307 */
																				long BgL_newzd2matchzd2_6511;

																				{	/* Ieee/input.scm 307 */
																					obj_t BgL_tmpz00_6771;

																					BgL_tmpz00_6771 =
																						((obj_t) BgL_iportz00_6495);
																					RGC_STOP_MATCH(BgL_tmpz00_6771,
																						BgL_arg1417z00_6510);
																				}
																				BgL_newzd2matchzd2_6511 = 2L;
																				BgL_matchz00_6430 =
																					BgL_newzd2matchzd2_6511;
																		}}
																	else
																		{	/* Ieee/input.scm 307 */
																			bool_t BgL_test3597z00_6774;

																			if (((long) (BgL_curz00_6502) == 34L))
																				{	/* Ieee/input.scm 307 */
																					BgL_test3597z00_6774 = ((bool_t) 1);
																				}
																			else
																				{	/* Ieee/input.scm 307 */
																					BgL_test3597z00_6774 =
																						((long) (BgL_curz00_6502) == 92L);
																				}
																			if (BgL_test3597z00_6774)
																				{	/* Ieee/input.scm 307 */
																					BgL_matchz00_6430 =
																						BgL_newzd2matchzd2_6499;
																				}
																			else
																				{
																					long BgL_bufposz00_6784;
																					long BgL_forwardz00_6782;
																					long BgL_lastzd2matchzd2_6781;
																					obj_t BgL_iportz00_6780;

																					BgL_iportz00_6780 = BgL_iportz00_6495;
																					BgL_lastzd2matchzd2_6781 =
																						BgL_newzd2matchzd2_6499;
																					BgL_forwardz00_6782 =
																						(1L + BgL_forwardz00_6497);
																					BgL_bufposz00_6784 =
																						BgL_bufposz00_6498;
																					BgL_bufposz00_6474 =
																						BgL_bufposz00_6784;
																					BgL_forwardz00_6473 =
																						BgL_forwardz00_6782;
																					BgL_lastzd2matchzd2_6472 =
																						BgL_lastzd2matchzd2_6781;
																					BgL_iportz00_6471 = BgL_iportz00_6780;
																					goto BgL_statezd24zd21107z00_6426;
																				}
																		}
																}
														}
													}
											}
										}
									else
										{	/* Ieee/input.scm 307 */
											bool_t BgL_test3599z00_6786;

											{	/* Ieee/input.scm 307 */
												bool_t BgL_test3600z00_6787;

												if (((long) (BgL_curz00_6470) == 10L))
													{	/* Ieee/input.scm 307 */
														BgL_test3600z00_6787 = ((bool_t) 1);
													}
												else
													{	/* Ieee/input.scm 307 */
														BgL_test3600z00_6787 =
															((long) (BgL_curz00_6470) == 9L);
													}
												if (BgL_test3600z00_6787)
													{	/* Ieee/input.scm 307 */
														BgL_test3599z00_6786 = ((bool_t) 1);
													}
												else
													{	/* Ieee/input.scm 307 */
														BgL_test3599z00_6786 =
															((long) (BgL_curz00_6470) == 32L);
											}}
											if (BgL_test3599z00_6786)
												{	/* Ieee/input.scm 307 */
													BgL_iportz00_6487 = BgL_iportz00_6464;
													BgL_lastzd2matchzd2_6488 = BgL_lastzd2matchzd2_6465;
													BgL_forwardz00_6489 = (1L + BgL_forwardz00_6466);
													BgL_bufposz00_6490 = BgL_bufposz00_6467;
												BgL_statezd22zd21105z00_6427:
													{	/* Ieee/input.scm 307 */
														long BgL_newzd2matchzd2_6491;

														{	/* Ieee/input.scm 307 */
															obj_t BgL_tmpz00_6795;

															BgL_tmpz00_6795 = ((obj_t) BgL_iportz00_6487);
															RGC_STOP_MATCH(BgL_tmpz00_6795,
																BgL_forwardz00_6489);
														}
														BgL_newzd2matchzd2_6491 = 0L;
														if ((BgL_forwardz00_6489 == BgL_bufposz00_6490))
															{	/* Ieee/input.scm 307 */
																if (rgc_fill_buffer(
																		((obj_t) BgL_iportz00_6487)))
																	{	/* Ieee/input.scm 307 */
																		long BgL_arg1424z00_6492;
																		long BgL_arg1425z00_6493;

																		{	/* Ieee/input.scm 307 */
																			obj_t BgL_tmpz00_6803;

																			BgL_tmpz00_6803 =
																				((obj_t) BgL_iportz00_6487);
																			BgL_arg1424z00_6492 =
																				RGC_BUFFER_FORWARD(BgL_tmpz00_6803);
																		}
																		{	/* Ieee/input.scm 307 */
																			obj_t BgL_tmpz00_6806;

																			BgL_tmpz00_6806 =
																				((obj_t) BgL_iportz00_6487);
																			BgL_arg1425z00_6493 =
																				RGC_BUFFER_BUFPOS(BgL_tmpz00_6806);
																		}
																		{
																			long BgL_bufposz00_6810;
																			long BgL_forwardz00_6809;

																			BgL_forwardz00_6809 = BgL_arg1424z00_6492;
																			BgL_bufposz00_6810 = BgL_arg1425z00_6493;
																			BgL_bufposz00_6490 = BgL_bufposz00_6810;
																			BgL_forwardz00_6489 = BgL_forwardz00_6809;
																			goto BgL_statezd22zd21105z00_6427;
																		}
																	}
																else
																	{	/* Ieee/input.scm 307 */
																		BgL_matchz00_6430 = BgL_newzd2matchzd2_6491;
																	}
															}
														else
															{	/* Ieee/input.scm 307 */
																int BgL_curz00_6494;

																{	/* Ieee/input.scm 307 */
																	obj_t BgL_tmpz00_6811;

																	BgL_tmpz00_6811 = ((obj_t) BgL_iportz00_6487);
																	BgL_curz00_6494 =
																		RGC_BUFFER_GET_CHAR(BgL_tmpz00_6811,
																		BgL_forwardz00_6489);
																}
																{	/* Ieee/input.scm 307 */

																	{	/* Ieee/input.scm 307 */
																		bool_t BgL_test3604z00_6814;

																		{	/* Ieee/input.scm 307 */
																			bool_t BgL_test3605z00_6815;

																			if (((long) (BgL_curz00_6494) == 10L))
																				{	/* Ieee/input.scm 307 */
																					BgL_test3605z00_6815 = ((bool_t) 1);
																				}
																			else
																				{	/* Ieee/input.scm 307 */
																					BgL_test3605z00_6815 =
																						((long) (BgL_curz00_6494) == 9L);
																				}
																			if (BgL_test3605z00_6815)
																				{	/* Ieee/input.scm 307 */
																					BgL_test3604z00_6814 = ((bool_t) 1);
																				}
																			else
																				{	/* Ieee/input.scm 307 */
																					BgL_test3604z00_6814 =
																						((long) (BgL_curz00_6494) == 32L);
																		}}
																		if (BgL_test3604z00_6814)
																			{	/* Ieee/input.scm 307 */
																				BgL_iportz00_6448 = BgL_iportz00_6487;
																				BgL_lastzd2matchzd2_6449 =
																					BgL_newzd2matchzd2_6491;
																				BgL_forwardz00_6450 =
																					(1L + BgL_forwardz00_6489);
																				BgL_bufposz00_6451 = BgL_bufposz00_6490;
																			BgL_statezd28zd21111z00_6423:
																				{	/* Ieee/input.scm 307 */
																					long BgL_newzd2matchzd2_6452;

																					{	/* Ieee/input.scm 307 */
																						obj_t BgL_tmpz00_6823;

																						BgL_tmpz00_6823 =
																							((obj_t) BgL_iportz00_6448);
																						RGC_STOP_MATCH(BgL_tmpz00_6823,
																							BgL_forwardz00_6450);
																					}
																					BgL_newzd2matchzd2_6452 = 0L;
																					if (
																						(BgL_forwardz00_6450 ==
																							BgL_bufposz00_6451))
																						{	/* Ieee/input.scm 307 */
																							if (rgc_fill_buffer(
																									((obj_t) BgL_iportz00_6448)))
																								{	/* Ieee/input.scm 307 */
																									long BgL_arg1477z00_6453;
																									long BgL_arg1478z00_6454;

																									{	/* Ieee/input.scm 307 */
																										obj_t BgL_tmpz00_6831;

																										BgL_tmpz00_6831 =
																											((obj_t)
																											BgL_iportz00_6448);
																										BgL_arg1477z00_6453 =
																											RGC_BUFFER_FORWARD
																											(BgL_tmpz00_6831);
																									}
																									{	/* Ieee/input.scm 307 */
																										obj_t BgL_tmpz00_6834;

																										BgL_tmpz00_6834 =
																											((obj_t)
																											BgL_iportz00_6448);
																										BgL_arg1478z00_6454 =
																											RGC_BUFFER_BUFPOS
																											(BgL_tmpz00_6834);
																									}
																									{
																										long BgL_bufposz00_6838;
																										long BgL_forwardz00_6837;

																										BgL_forwardz00_6837 =
																											BgL_arg1477z00_6453;
																										BgL_bufposz00_6838 =
																											BgL_arg1478z00_6454;
																										BgL_bufposz00_6451 =
																											BgL_bufposz00_6838;
																										BgL_forwardz00_6450 =
																											BgL_forwardz00_6837;
																										goto
																											BgL_statezd28zd21111z00_6423;
																									}
																								}
																							else
																								{	/* Ieee/input.scm 307 */
																									BgL_matchz00_6430 =
																										BgL_newzd2matchzd2_6452;
																								}
																						}
																					else
																						{	/* Ieee/input.scm 307 */
																							int BgL_curz00_6455;

																							{	/* Ieee/input.scm 307 */
																								obj_t BgL_tmpz00_6839;

																								BgL_tmpz00_6839 =
																									((obj_t) BgL_iportz00_6448);
																								BgL_curz00_6455 =
																									RGC_BUFFER_GET_CHAR
																									(BgL_tmpz00_6839,
																									BgL_forwardz00_6450);
																							}
																							{	/* Ieee/input.scm 307 */

																								{	/* Ieee/input.scm 307 */
																									bool_t BgL_test3609z00_6842;

																									{	/* Ieee/input.scm 307 */
																										bool_t BgL_test3610z00_6843;

																										if (
																											((long) (BgL_curz00_6455)
																												== 10L))
																											{	/* Ieee/input.scm 307 */
																												BgL_test3610z00_6843 =
																													((bool_t) 1);
																											}
																										else
																											{	/* Ieee/input.scm 307 */
																												BgL_test3610z00_6843 =
																													(
																													(long)
																													(BgL_curz00_6455) ==
																													9L);
																											}
																										if (BgL_test3610z00_6843)
																											{	/* Ieee/input.scm 307 */
																												BgL_test3609z00_6842 =
																													((bool_t) 1);
																											}
																										else
																											{	/* Ieee/input.scm 307 */
																												BgL_test3609z00_6842 =
																													(
																													(long)
																													(BgL_curz00_6455) ==
																													32L);
																									}}
																									if (BgL_test3609z00_6842)
																										{
																											long BgL_forwardz00_6852;
																											long
																												BgL_lastzd2matchzd2_6851;
																											BgL_lastzd2matchzd2_6851 =
																												BgL_newzd2matchzd2_6452;
																											BgL_forwardz00_6852 =
																												(1L +
																												BgL_forwardz00_6450);
																											BgL_forwardz00_6450 =
																												BgL_forwardz00_6852;
																											BgL_lastzd2matchzd2_6449 =
																												BgL_lastzd2matchzd2_6851;
																											goto
																												BgL_statezd28zd21111z00_6423;
																										}
																									else
																										{	/* Ieee/input.scm 307 */
																											BgL_matchz00_6430 =
																												BgL_newzd2matchzd2_6452;
																										}
																								}
																							}
																						}
																				}
																			}
																		else
																			{	/* Ieee/input.scm 307 */
																				BgL_matchz00_6430 =
																					BgL_newzd2matchzd2_6491;
																			}
																	}
																}
															}
													}
												}
											else
												{	/* Ieee/input.scm 307 */
													BgL_iportz00_6512 = BgL_iportz00_6464;
													BgL_lastzd2matchzd2_6513 = BgL_lastzd2matchzd2_6465;
													BgL_forwardz00_6514 = (1L + BgL_forwardz00_6466);
													BgL_bufposz00_6515 = BgL_bufposz00_6467;
												BgL_statezd21zd21104z00_6429:
													{	/* Ieee/input.scm 307 */
														long BgL_newzd2matchzd2_6516;

														{	/* Ieee/input.scm 307 */
															obj_t BgL_tmpz00_6856;

															BgL_tmpz00_6856 = ((obj_t) BgL_iportz00_6512);
															RGC_STOP_MATCH(BgL_tmpz00_6856,
																BgL_forwardz00_6514);
														}
														BgL_newzd2matchzd2_6516 = 3L;
														if ((BgL_forwardz00_6514 == BgL_bufposz00_6515))
															{	/* Ieee/input.scm 307 */
																if (rgc_fill_buffer(
																		((obj_t) BgL_iportz00_6512)))
																	{	/* Ieee/input.scm 307 */
																		long BgL_arg1402z00_6517;
																		long BgL_arg1403z00_6518;

																		{	/* Ieee/input.scm 307 */
																			obj_t BgL_tmpz00_6864;

																			BgL_tmpz00_6864 =
																				((obj_t) BgL_iportz00_6512);
																			BgL_arg1402z00_6517 =
																				RGC_BUFFER_FORWARD(BgL_tmpz00_6864);
																		}
																		{	/* Ieee/input.scm 307 */
																			obj_t BgL_tmpz00_6867;

																			BgL_tmpz00_6867 =
																				((obj_t) BgL_iportz00_6512);
																			BgL_arg1403z00_6518 =
																				RGC_BUFFER_BUFPOS(BgL_tmpz00_6867);
																		}
																		{
																			long BgL_bufposz00_6871;
																			long BgL_forwardz00_6870;

																			BgL_forwardz00_6870 = BgL_arg1402z00_6517;
																			BgL_bufposz00_6871 = BgL_arg1403z00_6518;
																			BgL_bufposz00_6515 = BgL_bufposz00_6871;
																			BgL_forwardz00_6514 = BgL_forwardz00_6870;
																			goto BgL_statezd21zd21104z00_6429;
																		}
																	}
																else
																	{	/* Ieee/input.scm 307 */
																		BgL_matchz00_6430 = BgL_newzd2matchzd2_6516;
																	}
															}
														else
															{	/* Ieee/input.scm 307 */
																int BgL_curz00_6519;

																{	/* Ieee/input.scm 307 */
																	obj_t BgL_tmpz00_6872;

																	BgL_tmpz00_6872 = ((obj_t) BgL_iportz00_6512);
																	BgL_curz00_6519 =
																		RGC_BUFFER_GET_CHAR(BgL_tmpz00_6872,
																		BgL_forwardz00_6514);
																}
																{	/* Ieee/input.scm 307 */

																	{	/* Ieee/input.scm 307 */
																		bool_t BgL_test3614z00_6875;

																		{	/* Ieee/input.scm 307 */
																			bool_t BgL_test3615z00_6876;

																			if (((long) (BgL_curz00_6519) == 10L))
																				{	/* Ieee/input.scm 307 */
																					BgL_test3615z00_6876 = ((bool_t) 1);
																				}
																			else
																				{	/* Ieee/input.scm 307 */
																					BgL_test3615z00_6876 =
																						((long) (BgL_curz00_6519) == 9L);
																				}
																			if (BgL_test3615z00_6876)
																				{	/* Ieee/input.scm 307 */
																					BgL_test3614z00_6875 = ((bool_t) 1);
																				}
																			else
																				{	/* Ieee/input.scm 307 */
																					if (((long) (BgL_curz00_6519) == 32L))
																						{	/* Ieee/input.scm 307 */
																							BgL_test3614z00_6875 =
																								((bool_t) 1);
																						}
																					else
																						{	/* Ieee/input.scm 307 */
																							BgL_test3614z00_6875 =
																								(
																								(long) (BgL_curz00_6519) ==
																								34L);
																		}}}
																		if (BgL_test3614z00_6875)
																			{	/* Ieee/input.scm 307 */
																				BgL_matchz00_6430 =
																					BgL_newzd2matchzd2_6516;
																			}
																		else
																			{	/* Ieee/input.scm 307 */
																				BgL_iportz00_6456 = BgL_iportz00_6512;
																				BgL_lastzd2matchzd2_6457 =
																					BgL_newzd2matchzd2_6516;
																				BgL_forwardz00_6458 =
																					(1L + BgL_forwardz00_6514);
																				BgL_bufposz00_6459 = BgL_bufposz00_6515;
																			BgL_statezd29zd21112z00_6424:
																				{	/* Ieee/input.scm 307 */
																					long BgL_newzd2matchzd2_6460;

																					{	/* Ieee/input.scm 307 */
																						obj_t BgL_tmpz00_6887;

																						BgL_tmpz00_6887 =
																							((obj_t) BgL_iportz00_6456);
																						RGC_STOP_MATCH(BgL_tmpz00_6887,
																							BgL_forwardz00_6458);
																					}
																					BgL_newzd2matchzd2_6460 = 3L;
																					if (
																						(BgL_forwardz00_6458 ==
																							BgL_bufposz00_6459))
																						{	/* Ieee/input.scm 307 */
																							if (rgc_fill_buffer(
																									((obj_t) BgL_iportz00_6456)))
																								{	/* Ieee/input.scm 307 */
																									long BgL_arg1458z00_6461;
																									long BgL_arg1459z00_6462;

																									{	/* Ieee/input.scm 307 */
																										obj_t BgL_tmpz00_6895;

																										BgL_tmpz00_6895 =
																											((obj_t)
																											BgL_iportz00_6456);
																										BgL_arg1458z00_6461 =
																											RGC_BUFFER_FORWARD
																											(BgL_tmpz00_6895);
																									}
																									{	/* Ieee/input.scm 307 */
																										obj_t BgL_tmpz00_6898;

																										BgL_tmpz00_6898 =
																											((obj_t)
																											BgL_iportz00_6456);
																										BgL_arg1459z00_6462 =
																											RGC_BUFFER_BUFPOS
																											(BgL_tmpz00_6898);
																									}
																									{
																										long BgL_bufposz00_6902;
																										long BgL_forwardz00_6901;

																										BgL_forwardz00_6901 =
																											BgL_arg1458z00_6461;
																										BgL_bufposz00_6902 =
																											BgL_arg1459z00_6462;
																										BgL_bufposz00_6459 =
																											BgL_bufposz00_6902;
																										BgL_forwardz00_6458 =
																											BgL_forwardz00_6901;
																										goto
																											BgL_statezd29zd21112z00_6424;
																									}
																								}
																							else
																								{	/* Ieee/input.scm 307 */
																									BgL_matchz00_6430 =
																										BgL_newzd2matchzd2_6460;
																								}
																						}
																					else
																						{	/* Ieee/input.scm 307 */
																							int BgL_curz00_6463;

																							{	/* Ieee/input.scm 307 */
																								obj_t BgL_tmpz00_6903;

																								BgL_tmpz00_6903 =
																									((obj_t) BgL_iportz00_6456);
																								BgL_curz00_6463 =
																									RGC_BUFFER_GET_CHAR
																									(BgL_tmpz00_6903,
																									BgL_forwardz00_6458);
																							}
																							{	/* Ieee/input.scm 307 */

																								{	/* Ieee/input.scm 307 */
																									bool_t BgL_test3620z00_6906;

																									{	/* Ieee/input.scm 307 */
																										bool_t BgL_test3621z00_6907;

																										if (
																											((long) (BgL_curz00_6463)
																												== 10L))
																											{	/* Ieee/input.scm 307 */
																												BgL_test3621z00_6907 =
																													((bool_t) 1);
																											}
																										else
																											{	/* Ieee/input.scm 307 */
																												BgL_test3621z00_6907 =
																													(
																													(long)
																													(BgL_curz00_6463) ==
																													9L);
																											}
																										if (BgL_test3621z00_6907)
																											{	/* Ieee/input.scm 307 */
																												BgL_test3620z00_6906 =
																													((bool_t) 1);
																											}
																										else
																											{	/* Ieee/input.scm 307 */
																												if (
																													((long)
																														(BgL_curz00_6463) ==
																														32L))
																													{	/* Ieee/input.scm 307 */
																														BgL_test3620z00_6906
																															= ((bool_t) 1);
																													}
																												else
																													{	/* Ieee/input.scm 307 */
																														BgL_test3620z00_6906
																															=
																															((long)
																															(BgL_curz00_6463)
																															== 34L);
																									}}}
																									if (BgL_test3620z00_6906)
																										{	/* Ieee/input.scm 307 */
																											BgL_matchz00_6430 =
																												BgL_newzd2matchzd2_6460;
																										}
																									else
																										{
																											long BgL_forwardz00_6919;
																											long
																												BgL_lastzd2matchzd2_6918;
																											BgL_lastzd2matchzd2_6918 =
																												BgL_newzd2matchzd2_6460;
																											BgL_forwardz00_6919 =
																												(1L +
																												BgL_forwardz00_6458);
																											BgL_forwardz00_6458 =
																												BgL_forwardz00_6919;
																											BgL_lastzd2matchzd2_6457 =
																												BgL_lastzd2matchzd2_6918;
																											goto
																												BgL_statezd29zd21112z00_6424;
																										}
																								}
																							}
																						}
																				}
																			}
																	}
																}
															}
													}
												}
										}
								}
							}
					}
					{	/* Ieee/input.scm 307 */
						obj_t BgL_tmpz00_6923;

						BgL_tmpz00_6923 = ((obj_t) BgL_iportz00_6245);
						RGC_SET_FILEPOS(BgL_tmpz00_6923);
					}
					switch (BgL_matchz00_6430)
						{
						case 4L:

							{	/* Ieee/input.scm 307 */
								bool_t BgL_test3624z00_6926;

								{	/* Ieee/input.scm 307 */
									long BgL_arg1571z00_6435;

									{	/* Ieee/input.scm 307 */
										obj_t BgL_tmpz00_6927;

										BgL_tmpz00_6927 = ((obj_t) BgL_iportz00_6245);
										BgL_arg1571z00_6435 =
											RGC_BUFFER_MATCH_LENGTH(BgL_tmpz00_6927);
									}
									BgL_test3624z00_6926 = (BgL_arg1571z00_6435 == 0L);
								}
								if (BgL_test3624z00_6926)
									{	/* Ieee/input.scm 307 */
										return BEOF;
									}
								else
									{	/* Ieee/input.scm 307 */
										unsigned char BgL_tmpz00_6931;

										{	/* Ieee/input.scm 307 */
											obj_t BgL_tmpz00_6932;

											BgL_tmpz00_6932 = ((obj_t) BgL_iportz00_6245);
											BgL_tmpz00_6931 = RGC_BUFFER_CHARACTER(BgL_tmpz00_6932);
										}
										return BCHAR(BgL_tmpz00_6931);
									}
							}
							break;
						case 3L:

							{	/* Ieee/input.scm 307 */
								long BgL_arg1489z00_6433;

								{	/* Ieee/input.scm 307 */
									obj_t BgL_tmpz00_6936;

									BgL_tmpz00_6936 = ((obj_t) BgL_iportz00_6245);
									BgL_arg1489z00_6433 =
										RGC_BUFFER_MATCH_LENGTH(BgL_tmpz00_6936);
								}
								return
									rgc_buffer_substring(
									((obj_t) BgL_iportz00_6245), 0L, BgL_arg1489z00_6433);
							}
							break;
						case 2L:

							BgL_minz00_6436 = (int) (1L);
							BgL_maxz00_6437 = (int) (-1L);
							if (((long) (BgL_maxz00_6437) < (long) (BgL_minz00_6436)))
								{	/* Ieee/input.scm 307 */
									long BgL_arg1492z00_6438;

									{	/* Ieee/input.scm 307 */
										obj_t BgL_tmpz00_6945;

										BgL_tmpz00_6945 = ((obj_t) BgL_iportz00_6245);
										BgL_arg1492z00_6438 =
											RGC_BUFFER_MATCH_LENGTH(BgL_tmpz00_6945);
									}
									{	/* Ieee/input.scm 307 */
										long BgL_za72za7_6439;

										BgL_za72za7_6439 = (long) (BgL_maxz00_6437);
										BgL_maxz00_6437 =
											(int) ((BgL_arg1492z00_6438 + BgL_za72za7_6439));
								}}
							else
								{	/* Ieee/input.scm 307 */
									BFALSE;
								}
							{	/* Ieee/input.scm 307 */
								bool_t BgL_test3626z00_6951;

								if (((long) (BgL_minz00_6436) >= 0L))
									{	/* Ieee/input.scm 307 */
										if (((long) (BgL_maxz00_6437) >= (long) (BgL_minz00_6436)))
											{	/* Ieee/input.scm 307 */
												long BgL_arg1501z00_6440;

												{	/* Ieee/input.scm 307 */
													obj_t BgL_tmpz00_6959;

													BgL_tmpz00_6959 = ((obj_t) BgL_iportz00_6245);
													BgL_arg1501z00_6440 =
														RGC_BUFFER_MATCH_LENGTH(BgL_tmpz00_6959);
												}
												BgL_test3626z00_6951 =
													((long) (BgL_maxz00_6437) <= BgL_arg1501z00_6440);
											}
										else
											{	/* Ieee/input.scm 307 */
												BgL_test3626z00_6951 = ((bool_t) 0);
											}
									}
								else
									{	/* Ieee/input.scm 307 */
										BgL_test3626z00_6951 = ((bool_t) 0);
									}
								if (BgL_test3626z00_6951)
									{	/* Ieee/input.scm 307 */
										long BgL_startz00_6441;
										long BgL_stopz00_6442;

										BgL_startz00_6441 = (long) (BgL_minz00_6436);
										BgL_stopz00_6442 = (long) (BgL_maxz00_6437);
										return
											rgc_buffer_substring(
											((obj_t) BgL_iportz00_6245), BgL_startz00_6441,
											BgL_stopz00_6442);
									}
								else
									{	/* Ieee/input.scm 307 */
										obj_t BgL_arg1497z00_6443;
										obj_t BgL_arg1498z00_6444;

										{	/* Ieee/input.scm 307 */
											obj_t BgL_arg1499z00_6445;

											{	/* Ieee/input.scm 307 */
												long BgL_arg1489z00_6446;

												{	/* Ieee/input.scm 307 */
													obj_t BgL_tmpz00_6968;

													BgL_tmpz00_6968 = ((obj_t) BgL_iportz00_6245);
													BgL_arg1489z00_6446 =
														RGC_BUFFER_MATCH_LENGTH(BgL_tmpz00_6968);
												}
												BgL_arg1499z00_6445 =
													rgc_buffer_substring(
													((obj_t) BgL_iportz00_6245), 0L, BgL_arg1489z00_6446);
											}
											{	/* Ieee/input.scm 307 */
												obj_t BgL_list1500z00_6447;

												BgL_list1500z00_6447 =
													MAKE_YOUNG_PAIR(BgL_arg1499z00_6445, BNIL);
												BgL_arg1497z00_6443 =
													BGl_formatz00zz__r4_output_6_10_3z00
													(BGl_string3442z00zz__r4_input_6_10_2z00,
													BgL_list1500z00_6447);
										}}
										BgL_arg1498z00_6444 =
											MAKE_YOUNG_PAIR(BINT(BgL_minz00_6436),
											BINT(BgL_maxz00_6437));
										return
											BGl_errorz00zz__errorz00
											(BGl_string3443z00zz__r4_input_6_10_2z00,
											BgL_arg1497z00_6443, BgL_arg1498z00_6444);
									}
							}
							break;
						case 1L:

							{	/* Ieee/input.scm 307 */
								long BgL_arg1489z00_6434;

								{	/* Ieee/input.scm 307 */
									obj_t BgL_tmpz00_6981;

									BgL_tmpz00_6981 = ((obj_t) BgL_iportz00_6245);
									BgL_arg1489z00_6434 =
										RGC_BUFFER_MATCH_LENGTH(BgL_tmpz00_6981);
								}
								return
									rgc_buffer_substring(
									((obj_t) BgL_iportz00_6245), 0L, BgL_arg1489z00_6434);
							}
							break;
						case 0L:

							goto BgL_ignorez00_6420;
							break;
						default:
							return
								BGl_errorz00zz__errorz00
								(BGl_string3440z00zz__r4_input_6_10_2z00,
								BGl_string3441z00zz__r4_input_6_10_2z00,
								BINT(BgL_matchz00_6430));
						}
				}
			}
		}

	}



/* read/rp */
	BGL_EXPORTED_DEF obj_t BGl_readzf2rpzf2zz__r4_input_6_10_2z00(obj_t
		BgL_grammarz00_3, obj_t BgL_portz00_4, obj_t BgL_optsz00_5)
	{
		{	/* Ieee/input.scm 106 */
			if (PAIRP(BgL_optsz00_5))
				{	/* Ieee/input.scm 109 */
					obj_t BgL_auxz00_6991;

					{	/* Ieee/input.scm 109 */
						obj_t BgL_list1585z00_4734;

						BgL_list1585z00_4734 = MAKE_YOUNG_PAIR(BgL_optsz00_5, BNIL);
						BgL_auxz00_6991 =
							BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(BgL_portz00_4,
							BgL_list1585z00_4734);
					}
					return apply(BgL_grammarz00_3, BgL_auxz00_6991);
				}
			else
				{	/* Ieee/input.scm 108 */
					if (PROCEDURE_CORRECT_ARITYP(BgL_grammarz00_3, (int) (1L)))
						{	/* Ieee/input.scm 110 */
							return BGL_PROCEDURE_CALL1(BgL_grammarz00_3, BgL_portz00_4);
						}
					else
						{	/* Ieee/input.scm 110 */
							if (PROCEDURE_CORRECT_ARITYP(BgL_grammarz00_3, (int) (2L)))
								{	/* Ieee/input.scm 112 */
									return
										BGL_PROCEDURE_CALL2(BgL_grammarz00_3, BgL_portz00_4,
										BUNSPEC);
								}
							else
								{	/* Ieee/input.scm 112 */
									return
										BGl_errorz00zz__errorz00
										(BGl_symbol3444z00zz__r4_input_6_10_2z00,
										BGl_string3446z00zz__r4_input_6_10_2z00, BgL_grammarz00_3);
								}
						}
				}
		}

	}



/* &read/rp */
	obj_t BGl_z62readzf2rpz90zz__r4_input_6_10_2z00(obj_t BgL_envz00_6246,
		obj_t BgL_grammarz00_6247, obj_t BgL_portz00_6248, obj_t BgL_optsz00_6249)
	{
		{	/* Ieee/input.scm 106 */
			{	/* Ieee/input.scm 108 */
				obj_t BgL_auxz00_7018;
				obj_t BgL_auxz00_7011;

				if (INPUT_PORTP(BgL_portz00_6248))
					{	/* Ieee/input.scm 108 */
						BgL_auxz00_7018 = BgL_portz00_6248;
					}
				else
					{
						obj_t BgL_auxz00_7021;

						BgL_auxz00_7021 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string3447z00zz__r4_input_6_10_2z00, BINT(4665L),
							BGl_string3448z00zz__r4_input_6_10_2z00,
							BGl_string3450z00zz__r4_input_6_10_2z00, BgL_portz00_6248);
						FAILURE(BgL_auxz00_7021, BFALSE, BFALSE);
					}
				if (PROCEDUREP(BgL_grammarz00_6247))
					{	/* Ieee/input.scm 108 */
						BgL_auxz00_7011 = BgL_grammarz00_6247;
					}
				else
					{
						obj_t BgL_auxz00_7014;

						BgL_auxz00_7014 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string3447z00zz__r4_input_6_10_2z00, BINT(4665L),
							BGl_string3448z00zz__r4_input_6_10_2z00,
							BGl_string3449z00zz__r4_input_6_10_2z00, BgL_grammarz00_6247);
						FAILURE(BgL_auxz00_7014, BFALSE, BFALSE);
					}
				return
					BGl_readzf2rpzf2zz__r4_input_6_10_2z00(BgL_auxz00_7011,
					BgL_auxz00_7018, BgL_optsz00_6249);
			}
		}

	}



/* read/lalrp */
	BGL_EXPORTED_DEF obj_t BGl_readzf2lalrpzf2zz__r4_input_6_10_2z00(obj_t
		BgL_lalrz00_6, obj_t BgL_rgcz00_7, obj_t BgL_portz00_8,
		obj_t BgL_eofzd2funzf3z21_9)
	{
		{	/* Ieee/input.scm 120 */
			if (NULLP(BgL_eofzd2funzf3z21_9))
				{	/* Ieee/input.scm 121 */
					return
						BGL_PROCEDURE_CALL3(BgL_lalrz00_6, BgL_rgcz00_7, BgL_portz00_8,
						BGl_eofzd2objectzf3zd2envzf3zz__r4_input_6_10_2z00);
				}
			else
				{	/* Ieee/input.scm 123 */
					obj_t BgL_arg1587z00_4736;

					BgL_arg1587z00_4736 = CAR(((obj_t) BgL_eofzd2funzf3z21_9));
					return
						BGL_PROCEDURE_CALL3(BgL_lalrz00_6, BgL_rgcz00_7, BgL_portz00_8,
						BgL_arg1587z00_4736);
				}
		}

	}



/* &read/lalrp */
	obj_t BGl_z62readzf2lalrpz90zz__r4_input_6_10_2z00(obj_t BgL_envz00_6250,
		obj_t BgL_lalrz00_6251, obj_t BgL_rgcz00_6252, obj_t BgL_portz00_6253,
		obj_t BgL_eofzd2funzf3z21_6254)
	{
		{	/* Ieee/input.scm 120 */
			{	/* Ieee/input.scm 121 */
				obj_t BgL_auxz00_7056;
				obj_t BgL_auxz00_7049;
				obj_t BgL_auxz00_7042;

				if (INPUT_PORTP(BgL_portz00_6253))
					{	/* Ieee/input.scm 121 */
						BgL_auxz00_7056 = BgL_portz00_6253;
					}
				else
					{
						obj_t BgL_auxz00_7059;

						BgL_auxz00_7059 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string3447z00zz__r4_input_6_10_2z00, BINT(5184L),
							BGl_string3451z00zz__r4_input_6_10_2z00,
							BGl_string3450z00zz__r4_input_6_10_2z00, BgL_portz00_6253);
						FAILURE(BgL_auxz00_7059, BFALSE, BFALSE);
					}
				if (PROCEDUREP(BgL_rgcz00_6252))
					{	/* Ieee/input.scm 121 */
						BgL_auxz00_7049 = BgL_rgcz00_6252;
					}
				else
					{
						obj_t BgL_auxz00_7052;

						BgL_auxz00_7052 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string3447z00zz__r4_input_6_10_2z00, BINT(5184L),
							BGl_string3451z00zz__r4_input_6_10_2z00,
							BGl_string3449z00zz__r4_input_6_10_2z00, BgL_rgcz00_6252);
						FAILURE(BgL_auxz00_7052, BFALSE, BFALSE);
					}
				if (PROCEDUREP(BgL_lalrz00_6251))
					{	/* Ieee/input.scm 121 */
						BgL_auxz00_7042 = BgL_lalrz00_6251;
					}
				else
					{
						obj_t BgL_auxz00_7045;

						BgL_auxz00_7045 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string3447z00zz__r4_input_6_10_2z00, BINT(5184L),
							BGl_string3451z00zz__r4_input_6_10_2z00,
							BGl_string3449z00zz__r4_input_6_10_2z00, BgL_lalrz00_6251);
						FAILURE(BgL_auxz00_7045, BFALSE, BFALSE);
					}
				return
					BGl_readzf2lalrpzf2zz__r4_input_6_10_2z00(BgL_auxz00_7042,
					BgL_auxz00_7049, BgL_auxz00_7056, BgL_eofzd2funzf3z21_6254);
			}
		}

	}



/* _read-char */
	obj_t BGl__readzd2charzd2zz__r4_input_6_10_2z00(obj_t BgL_env1244z00_12,
		obj_t BgL_opt1243z00_11)
	{
		{	/* Ieee/input.scm 128 */
			{	/* Ieee/input.scm 128 */

				switch (VECTOR_LENGTH(BgL_opt1243z00_11))
					{
					case 0L:

						{	/* Ieee/input.scm 128 */
							obj_t BgL_ipz00_1611;

							{	/* Ieee/input.scm 128 */
								obj_t BgL_tmpz00_7064;

								BgL_tmpz00_7064 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_ipz00_1611 = BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_7064);
							}
							{	/* Ieee/input.scm 128 */

								return BGl_readzd2charzd2zz__r4_input_6_10_2z00(BgL_ipz00_1611);
							}
						}
						break;
					case 1L:

						{	/* Ieee/input.scm 128 */
							obj_t BgL_ipz00_1612;

							BgL_ipz00_1612 = VECTOR_REF(BgL_opt1243z00_11, 0L);
							{	/* Ieee/input.scm 128 */

								return BGl_readzd2charzd2zz__r4_input_6_10_2z00(BgL_ipz00_1612);
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* read-char */
	BGL_EXPORTED_DEF obj_t BGl_readzd2charzd2zz__r4_input_6_10_2z00(obj_t
		BgL_ipz00_10)
	{
		{	/* Ieee/input.scm 128 */
			{
				obj_t BgL_iportz00_1615;

				BgL_iportz00_1615 = BgL_ipz00_10;
				{

					{	/* Ieee/input.scm 129 */
						obj_t BgL_tmpz00_7072;

						BgL_tmpz00_7072 = ((obj_t) BgL_iportz00_1615);
						RGC_START_MATCH(BgL_tmpz00_7072);
					}
					{	/* Ieee/input.scm 129 */
						long BgL_matchz00_1782;

						{	/* Ieee/input.scm 129 */
							long BgL_arg1691z00_1785;
							long BgL_arg1692z00_1786;

							{	/* Ieee/input.scm 129 */
								obj_t BgL_tmpz00_7075;

								BgL_tmpz00_7075 = ((obj_t) BgL_iportz00_1615);
								BgL_arg1691z00_1785 = RGC_BUFFER_FORWARD(BgL_tmpz00_7075);
							}
							{	/* Ieee/input.scm 129 */
								obj_t BgL_tmpz00_7078;

								BgL_tmpz00_7078 = ((obj_t) BgL_iportz00_1615);
								BgL_arg1692z00_1786 = RGC_BUFFER_BUFPOS(BgL_tmpz00_7078);
							}
							{
								long BgL_forwardz00_4784;
								long BgL_bufposz00_4785;

								BgL_forwardz00_4784 = BgL_arg1691z00_1785;
								BgL_bufposz00_4785 = BgL_arg1692z00_1786;
							BgL_statezd20zd21040z00_4783:
								if ((BgL_forwardz00_4784 == BgL_bufposz00_4785))
									{	/* Ieee/input.scm 129 */
										if (rgc_fill_buffer(((obj_t) BgL_iportz00_1615)))
											{	/* Ieee/input.scm 129 */
												long BgL_arg1593z00_4788;
												long BgL_arg1594z00_4789;

												{	/* Ieee/input.scm 129 */
													obj_t BgL_tmpz00_7086;

													BgL_tmpz00_7086 = ((obj_t) BgL_iportz00_1615);
													BgL_arg1593z00_4788 =
														RGC_BUFFER_FORWARD(BgL_tmpz00_7086);
												}
												{	/* Ieee/input.scm 129 */
													obj_t BgL_tmpz00_7089;

													BgL_tmpz00_7089 = ((obj_t) BgL_iportz00_1615);
													BgL_arg1594z00_4789 =
														RGC_BUFFER_BUFPOS(BgL_tmpz00_7089);
												}
												{
													long BgL_bufposz00_7093;
													long BgL_forwardz00_7092;

													BgL_forwardz00_7092 = BgL_arg1593z00_4788;
													BgL_bufposz00_7093 = BgL_arg1594z00_4789;
													BgL_bufposz00_4785 = BgL_bufposz00_7093;
													BgL_forwardz00_4784 = BgL_forwardz00_7092;
													goto BgL_statezd20zd21040z00_4783;
												}
											}
										else
											{	/* Ieee/input.scm 129 */
												BgL_matchz00_1782 = 1L;
											}
									}
								else
									{	/* Ieee/input.scm 129 */
										int BgL_curz00_4790;

										{	/* Ieee/input.scm 129 */
											obj_t BgL_tmpz00_7094;

											BgL_tmpz00_7094 = ((obj_t) BgL_iportz00_1615);
											BgL_curz00_4790 =
												RGC_BUFFER_GET_CHAR(BgL_tmpz00_7094,
												BgL_forwardz00_4784);
										}
										{	/* Ieee/input.scm 129 */

											{	/* Ieee/input.scm 129 */
												long BgL_arg1595z00_4791;

												BgL_arg1595z00_4791 = (1L + BgL_forwardz00_4784);
												{	/* Ieee/input.scm 129 */
													long BgL_newzd2matchzd2_4800;

													{	/* Ieee/input.scm 129 */
														obj_t BgL_tmpz00_7098;

														BgL_tmpz00_7098 = ((obj_t) BgL_iportz00_1615);
														RGC_STOP_MATCH(BgL_tmpz00_7098,
															BgL_arg1595z00_4791);
													}
													BgL_newzd2matchzd2_4800 = 0L;
													BgL_matchz00_1782 = BgL_newzd2matchzd2_4800;
						}}}}}}
						{	/* Ieee/input.scm 129 */
							obj_t BgL_tmpz00_7101;

							BgL_tmpz00_7101 = ((obj_t) BgL_iportz00_1615);
							RGC_SET_FILEPOS(BgL_tmpz00_7101);
						}
						switch (BgL_matchz00_1782)
							{
							case 1L:

								{	/* Ieee/input.scm 129 */
									bool_t BgL_test3640z00_7104;

									{	/* Ieee/input.scm 129 */
										long BgL_arg1684z00_1772;

										{	/* Ieee/input.scm 129 */
											obj_t BgL_tmpz00_7105;

											BgL_tmpz00_7105 = ((obj_t) BgL_iportz00_1615);
											BgL_arg1684z00_1772 =
												RGC_BUFFER_MATCH_LENGTH(BgL_tmpz00_7105);
										}
										BgL_test3640z00_7104 = (BgL_arg1684z00_1772 == 0L);
									}
									if (BgL_test3640z00_7104)
										{	/* Ieee/input.scm 129 */
											return BEOF;
										}
									else
										{	/* Ieee/input.scm 129 */
											unsigned char BgL_tmpz00_7109;

											{	/* Ieee/input.scm 129 */
												obj_t BgL_tmpz00_7110;

												BgL_tmpz00_7110 = ((obj_t) BgL_iportz00_1615);
												BgL_tmpz00_7109 = RGC_BUFFER_CHARACTER(BgL_tmpz00_7110);
											}
											return BCHAR(BgL_tmpz00_7109);
										}
								}
								break;
							case 0L:

								{	/* Ieee/input.scm 129 */
									unsigned char BgL_tmpz00_7114;

									{	/* Ieee/input.scm 129 */
										obj_t BgL_tmpz00_7115;

										BgL_tmpz00_7115 = ((obj_t) BgL_iportz00_1615);
										BgL_tmpz00_7114 = RGC_BUFFER_CHARACTER(BgL_tmpz00_7115);
									}
									return BCHAR(BgL_tmpz00_7114);
								}
								break;
							default:
								return
									BGl_errorz00zz__errorz00
									(BGl_string3440z00zz__r4_input_6_10_2z00,
									BGl_string3441z00zz__r4_input_6_10_2z00,
									BINT(BgL_matchz00_1782));
							}
					}
				}
			}
		}

	}



/* _peek-char */
	obj_t BGl__peekzd2charzd2zz__r4_input_6_10_2z00(obj_t BgL_env1248z00_15,
		obj_t BgL_opt1247z00_14)
	{
		{	/* Ieee/input.scm 136 */
			{	/* Ieee/input.scm 136 */

				switch (VECTOR_LENGTH(BgL_opt1247z00_14))
					{
					case 0L:

						{	/* Ieee/input.scm 136 */
							obj_t BgL_ipz00_1819;

							{	/* Ieee/input.scm 136 */
								obj_t BgL_tmpz00_7122;

								BgL_tmpz00_7122 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_ipz00_1819 = BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_7122);
							}
							{	/* Ieee/input.scm 136 */

								return BGl_peekzd2charzd2zz__r4_input_6_10_2z00(BgL_ipz00_1819);
							}
						}
						break;
					case 1L:

						{	/* Ieee/input.scm 136 */
							obj_t BgL_ipz00_1820;

							BgL_ipz00_1820 = VECTOR_REF(BgL_opt1247z00_14, 0L);
							{	/* Ieee/input.scm 136 */

								return BGl_peekzd2charzd2zz__r4_input_6_10_2z00(BgL_ipz00_1820);
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* peek-char */
	BGL_EXPORTED_DEF obj_t BGl_peekzd2charzd2zz__r4_input_6_10_2z00(obj_t
		BgL_ipz00_13)
	{
		{	/* Ieee/input.scm 136 */
			{
				obj_t BgL_iportz00_1823;

				BgL_iportz00_1823 = BgL_ipz00_13;
				{

					{	/* Ieee/input.scm 137 */
						obj_t BgL_tmpz00_7130;

						BgL_tmpz00_7130 = ((obj_t) BgL_iportz00_1823);
						RGC_START_MATCH(BgL_tmpz00_7130);
					}
					{	/* Ieee/input.scm 137 */
						long BgL_matchz00_1990;

						{	/* Ieee/input.scm 137 */
							long BgL_arg1796z00_1996;
							long BgL_arg1797z00_1997;

							{	/* Ieee/input.scm 137 */
								obj_t BgL_tmpz00_7133;

								BgL_tmpz00_7133 = ((obj_t) BgL_iportz00_1823);
								BgL_arg1796z00_1996 = RGC_BUFFER_FORWARD(BgL_tmpz00_7133);
							}
							{	/* Ieee/input.scm 137 */
								obj_t BgL_tmpz00_7136;

								BgL_tmpz00_7136 = ((obj_t) BgL_iportz00_1823);
								BgL_arg1797z00_1997 = RGC_BUFFER_BUFPOS(BgL_tmpz00_7136);
							}
							{
								long BgL_forwardz00_4851;
								long BgL_bufposz00_4852;

								BgL_forwardz00_4851 = BgL_arg1796z00_1996;
								BgL_bufposz00_4852 = BgL_arg1797z00_1997;
							BgL_statezd20zd21046z00_4850:
								if ((BgL_forwardz00_4851 == BgL_bufposz00_4852))
									{	/* Ieee/input.scm 137 */
										if (rgc_fill_buffer(((obj_t) BgL_iportz00_1823)))
											{	/* Ieee/input.scm 137 */
												long BgL_arg1699z00_4855;
												long BgL_arg1700z00_4856;

												{	/* Ieee/input.scm 137 */
													obj_t BgL_tmpz00_7144;

													BgL_tmpz00_7144 = ((obj_t) BgL_iportz00_1823);
													BgL_arg1699z00_4855 =
														RGC_BUFFER_FORWARD(BgL_tmpz00_7144);
												}
												{	/* Ieee/input.scm 137 */
													obj_t BgL_tmpz00_7147;

													BgL_tmpz00_7147 = ((obj_t) BgL_iportz00_1823);
													BgL_arg1700z00_4856 =
														RGC_BUFFER_BUFPOS(BgL_tmpz00_7147);
												}
												{
													long BgL_bufposz00_7151;
													long BgL_forwardz00_7150;

													BgL_forwardz00_7150 = BgL_arg1699z00_4855;
													BgL_bufposz00_7151 = BgL_arg1700z00_4856;
													BgL_bufposz00_4852 = BgL_bufposz00_7151;
													BgL_forwardz00_4851 = BgL_forwardz00_7150;
													goto BgL_statezd20zd21046z00_4850;
												}
											}
										else
											{	/* Ieee/input.scm 137 */
												BgL_matchz00_1990 = 1L;
											}
									}
								else
									{	/* Ieee/input.scm 137 */
										int BgL_curz00_4857;

										{	/* Ieee/input.scm 137 */
											obj_t BgL_tmpz00_7152;

											BgL_tmpz00_7152 = ((obj_t) BgL_iportz00_1823);
											BgL_curz00_4857 =
												RGC_BUFFER_GET_CHAR(BgL_tmpz00_7152,
												BgL_forwardz00_4851);
										}
										{	/* Ieee/input.scm 137 */

											{	/* Ieee/input.scm 137 */
												long BgL_arg1701z00_4858;

												BgL_arg1701z00_4858 = (1L + BgL_forwardz00_4851);
												{	/* Ieee/input.scm 137 */
													long BgL_newzd2matchzd2_4867;

													{	/* Ieee/input.scm 137 */
														obj_t BgL_tmpz00_7156;

														BgL_tmpz00_7156 = ((obj_t) BgL_iportz00_1823);
														RGC_STOP_MATCH(BgL_tmpz00_7156,
															BgL_arg1701z00_4858);
													}
													BgL_newzd2matchzd2_4867 = 0L;
													BgL_matchz00_1990 = BgL_newzd2matchzd2_4867;
						}}}}}}
						{	/* Ieee/input.scm 137 */
							obj_t BgL_tmpz00_7159;

							BgL_tmpz00_7159 = ((obj_t) BgL_iportz00_1823);
							RGC_SET_FILEPOS(BgL_tmpz00_7159);
						}
						switch (BgL_matchz00_1990)
							{
							case 1L:

								{	/* Ieee/input.scm 137 */
									bool_t BgL_test3643z00_7162;

									{	/* Ieee/input.scm 137 */
										long BgL_arg1787z00_1980;

										{	/* Ieee/input.scm 137 */
											obj_t BgL_tmpz00_7163;

											BgL_tmpz00_7163 = ((obj_t) BgL_iportz00_1823);
											BgL_arg1787z00_1980 =
												RGC_BUFFER_MATCH_LENGTH(BgL_tmpz00_7163);
										}
										BgL_test3643z00_7162 = (BgL_arg1787z00_1980 == 0L);
									}
									if (BgL_test3643z00_7162)
										{	/* Ieee/input.scm 137 */
											return BEOF;
										}
									else
										{	/* Ieee/input.scm 137 */
											unsigned char BgL_tmpz00_7167;

											{	/* Ieee/input.scm 137 */
												obj_t BgL_tmpz00_7168;

												BgL_tmpz00_7168 = ((obj_t) BgL_iportz00_1823);
												BgL_tmpz00_7167 = RGC_BUFFER_CHARACTER(BgL_tmpz00_7168);
											}
											return BCHAR(BgL_tmpz00_7167);
										}
								}
								break;
							case 0L:

								{	/* Ieee/input.scm 139 */
									unsigned char BgL_cz00_1993;

									{	/* Ieee/input.scm 137 */
										obj_t BgL_tmpz00_7172;

										BgL_tmpz00_7172 = ((obj_t) BgL_iportz00_1823);
										BgL_cz00_1993 = RGC_BUFFER_CHARACTER(BgL_tmpz00_7172);
									}
									{	/* Ieee/input.scm 140 */
										long BgL_arg1795z00_1995;

										BgL_arg1795z00_1995 = ((unsigned char) (BgL_cz00_1993));
										{	/* Ieee/input.scm 140 */
											int BgL_charz00_4875;

											BgL_charz00_4875 = (int) (BgL_arg1795z00_1995);
											rgc_buffer_unget_char(
												((obj_t) BgL_iportz00_1823), BgL_charz00_4875);
									}}
									return BCHAR(BgL_cz00_1993);
								}
								break;
							default:
								return
									BGl_errorz00zz__errorz00
									(BGl_string3440z00zz__r4_input_6_10_2z00,
									BGl_string3441z00zz__r4_input_6_10_2z00,
									BINT(BgL_matchz00_1990));
							}
					}
				}
			}
		}

	}



/* _read-byte */
	obj_t BGl__readzd2bytezd2zz__r4_input_6_10_2z00(obj_t BgL_env1252z00_18,
		obj_t BgL_opt1251z00_17)
	{
		{	/* Ieee/input.scm 147 */
			{	/* Ieee/input.scm 147 */

				switch (VECTOR_LENGTH(BgL_opt1251z00_17))
					{
					case 0L:

						{	/* Ieee/input.scm 147 */
							obj_t BgL_ipz00_2030;

							{	/* Ieee/input.scm 147 */
								obj_t BgL_tmpz00_7184;

								BgL_tmpz00_7184 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_ipz00_2030 = BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_7184);
							}
							{	/* Ieee/input.scm 147 */

								return BGl_readzd2bytezd2zz__r4_input_6_10_2z00(BgL_ipz00_2030);
							}
						}
						break;
					case 1L:

						{	/* Ieee/input.scm 147 */
							obj_t BgL_ipz00_2031;

							BgL_ipz00_2031 = VECTOR_REF(BgL_opt1251z00_17, 0L);
							{	/* Ieee/input.scm 147 */

								return BGl_readzd2bytezd2zz__r4_input_6_10_2z00(BgL_ipz00_2031);
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* read-byte */
	BGL_EXPORTED_DEF obj_t BGl_readzd2bytezd2zz__r4_input_6_10_2z00(obj_t
		BgL_ipz00_16)
	{
		{	/* Ieee/input.scm 147 */
			{
				obj_t BgL_iportz00_2034;

				BgL_iportz00_2034 = BgL_ipz00_16;
				{

					{	/* Ieee/input.scm 148 */
						obj_t BgL_tmpz00_7192;

						BgL_tmpz00_7192 = ((obj_t) BgL_iportz00_2034);
						RGC_START_MATCH(BgL_tmpz00_7192);
					}
					{	/* Ieee/input.scm 148 */
						long BgL_matchz00_2201;

						{	/* Ieee/input.scm 148 */
							long BgL_arg1899z00_2204;
							long BgL_arg1901z00_2205;

							{	/* Ieee/input.scm 148 */
								obj_t BgL_tmpz00_7195;

								BgL_tmpz00_7195 = ((obj_t) BgL_iportz00_2034);
								BgL_arg1899z00_2204 = RGC_BUFFER_FORWARD(BgL_tmpz00_7195);
							}
							{	/* Ieee/input.scm 148 */
								obj_t BgL_tmpz00_7198;

								BgL_tmpz00_7198 = ((obj_t) BgL_iportz00_2034);
								BgL_arg1901z00_2205 = RGC_BUFFER_BUFPOS(BgL_tmpz00_7198);
							}
							{
								long BgL_forwardz00_4922;
								long BgL_bufposz00_4923;

								BgL_forwardz00_4922 = BgL_arg1899z00_2204;
								BgL_bufposz00_4923 = BgL_arg1901z00_2205;
							BgL_statezd20zd21052z00_4921:
								if ((BgL_forwardz00_4922 == BgL_bufposz00_4923))
									{	/* Ieee/input.scm 148 */
										if (rgc_fill_buffer(((obj_t) BgL_iportz00_2034)))
											{	/* Ieee/input.scm 148 */
												long BgL_arg1803z00_4926;
												long BgL_arg1804z00_4927;

												{	/* Ieee/input.scm 148 */
													obj_t BgL_tmpz00_7206;

													BgL_tmpz00_7206 = ((obj_t) BgL_iportz00_2034);
													BgL_arg1803z00_4926 =
														RGC_BUFFER_FORWARD(BgL_tmpz00_7206);
												}
												{	/* Ieee/input.scm 148 */
													obj_t BgL_tmpz00_7209;

													BgL_tmpz00_7209 = ((obj_t) BgL_iportz00_2034);
													BgL_arg1804z00_4927 =
														RGC_BUFFER_BUFPOS(BgL_tmpz00_7209);
												}
												{
													long BgL_bufposz00_7213;
													long BgL_forwardz00_7212;

													BgL_forwardz00_7212 = BgL_arg1803z00_4926;
													BgL_bufposz00_7213 = BgL_arg1804z00_4927;
													BgL_bufposz00_4923 = BgL_bufposz00_7213;
													BgL_forwardz00_4922 = BgL_forwardz00_7212;
													goto BgL_statezd20zd21052z00_4921;
												}
											}
										else
											{	/* Ieee/input.scm 148 */
												BgL_matchz00_2201 = 1L;
											}
									}
								else
									{	/* Ieee/input.scm 148 */
										int BgL_curz00_4928;

										{	/* Ieee/input.scm 148 */
											obj_t BgL_tmpz00_7214;

											BgL_tmpz00_7214 = ((obj_t) BgL_iportz00_2034);
											BgL_curz00_4928 =
												RGC_BUFFER_GET_CHAR(BgL_tmpz00_7214,
												BgL_forwardz00_4922);
										}
										{	/* Ieee/input.scm 148 */

											{	/* Ieee/input.scm 148 */
												long BgL_arg1805z00_4929;

												BgL_arg1805z00_4929 = (1L + BgL_forwardz00_4922);
												{	/* Ieee/input.scm 148 */
													long BgL_newzd2matchzd2_4938;

													{	/* Ieee/input.scm 148 */
														obj_t BgL_tmpz00_7218;

														BgL_tmpz00_7218 = ((obj_t) BgL_iportz00_2034);
														RGC_STOP_MATCH(BgL_tmpz00_7218,
															BgL_arg1805z00_4929);
													}
													BgL_newzd2matchzd2_4938 = 0L;
													BgL_matchz00_2201 = BgL_newzd2matchzd2_4938;
						}}}}}}
						{	/* Ieee/input.scm 148 */
							obj_t BgL_tmpz00_7221;

							BgL_tmpz00_7221 = ((obj_t) BgL_iportz00_2034);
							RGC_SET_FILEPOS(BgL_tmpz00_7221);
						}
						switch (BgL_matchz00_2201)
							{
							case 1L:

								{	/* Ieee/input.scm 148 */
									bool_t BgL_test3646z00_7224;

									{	/* Ieee/input.scm 148 */
										long BgL_arg1892z00_2191;

										{	/* Ieee/input.scm 148 */
											obj_t BgL_tmpz00_7225;

											BgL_tmpz00_7225 = ((obj_t) BgL_iportz00_2034);
											BgL_arg1892z00_2191 =
												RGC_BUFFER_MATCH_LENGTH(BgL_tmpz00_7225);
										}
										BgL_test3646z00_7224 = (BgL_arg1892z00_2191 == 0L);
									}
									if (BgL_test3646z00_7224)
										{	/* Ieee/input.scm 148 */
											return BEOF;
										}
									else
										{	/* Ieee/input.scm 148 */
											unsigned char BgL_tmpz00_7229;

											{	/* Ieee/input.scm 148 */
												obj_t BgL_tmpz00_7230;

												BgL_tmpz00_7230 = ((obj_t) BgL_iportz00_2034);
												BgL_tmpz00_7229 = RGC_BUFFER_CHARACTER(BgL_tmpz00_7230);
											}
											return BCHAR(BgL_tmpz00_7229);
										}
								}
								break;
							case 0L:

								{	/* Ieee/input.scm 148 */
									int BgL_tmpz00_7234;

									{	/* Ieee/input.scm 148 */
										obj_t BgL_tmpz00_7235;

										BgL_tmpz00_7235 = ((obj_t) BgL_iportz00_2034);
										BgL_tmpz00_7234 = RGC_BUFFER_BYTE(BgL_tmpz00_7235);
									}
									return BINT(BgL_tmpz00_7234);
								}
								break;
							default:
								return
									BGl_errorz00zz__errorz00
									(BGl_string3440z00zz__r4_input_6_10_2z00,
									BGl_string3441z00zz__r4_input_6_10_2z00,
									BINT(BgL_matchz00_2201));
							}
					}
				}
			}
		}

	}



/* _peek-byte */
	obj_t BGl__peekzd2bytezd2zz__r4_input_6_10_2z00(obj_t BgL_env1256z00_21,
		obj_t BgL_opt1255z00_20)
	{
		{	/* Ieee/input.scm 155 */
			{	/* Ieee/input.scm 155 */

				switch (VECTOR_LENGTH(BgL_opt1255z00_20))
					{
					case 0L:

						{	/* Ieee/input.scm 155 */
							obj_t BgL_ipz00_2238;

							{	/* Ieee/input.scm 155 */
								obj_t BgL_tmpz00_7242;

								BgL_tmpz00_7242 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_ipz00_2238 = BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_7242);
							}
							{	/* Ieee/input.scm 155 */

								return BGl_peekzd2bytezd2zz__r4_input_6_10_2z00(BgL_ipz00_2238);
							}
						}
						break;
					case 1L:

						{	/* Ieee/input.scm 155 */
							obj_t BgL_ipz00_2239;

							BgL_ipz00_2239 = VECTOR_REF(BgL_opt1255z00_20, 0L);
							{	/* Ieee/input.scm 155 */

								return BGl_peekzd2bytezd2zz__r4_input_6_10_2z00(BgL_ipz00_2239);
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* peek-byte */
	BGL_EXPORTED_DEF obj_t BGl_peekzd2bytezd2zz__r4_input_6_10_2z00(obj_t
		BgL_ipz00_19)
	{
		{	/* Ieee/input.scm 155 */
			{
				obj_t BgL_iportz00_2242;

				BgL_iportz00_2242 = BgL_ipz00_19;
				{

					{	/* Ieee/input.scm 156 */
						obj_t BgL_tmpz00_7250;

						BgL_tmpz00_7250 = ((obj_t) BgL_iportz00_2242);
						RGC_START_MATCH(BgL_tmpz00_7250);
					}
					{	/* Ieee/input.scm 156 */
						long BgL_matchz00_2409;

						{	/* Ieee/input.scm 156 */
							long BgL_arg2002z00_2414;
							long BgL_arg2003z00_2415;

							{	/* Ieee/input.scm 156 */
								obj_t BgL_tmpz00_7253;

								BgL_tmpz00_7253 = ((obj_t) BgL_iportz00_2242);
								BgL_arg2002z00_2414 = RGC_BUFFER_FORWARD(BgL_tmpz00_7253);
							}
							{	/* Ieee/input.scm 156 */
								obj_t BgL_tmpz00_7256;

								BgL_tmpz00_7256 = ((obj_t) BgL_iportz00_2242);
								BgL_arg2003z00_2415 = RGC_BUFFER_BUFPOS(BgL_tmpz00_7256);
							}
							{
								long BgL_forwardz00_4989;
								long BgL_bufposz00_4990;

								BgL_forwardz00_4989 = BgL_arg2002z00_2414;
								BgL_bufposz00_4990 = BgL_arg2003z00_2415;
							BgL_statezd20zd21058z00_4988:
								if ((BgL_forwardz00_4989 == BgL_bufposz00_4990))
									{	/* Ieee/input.scm 156 */
										if (rgc_fill_buffer(((obj_t) BgL_iportz00_2242)))
											{	/* Ieee/input.scm 156 */
												long BgL_arg1910z00_4993;
												long BgL_arg1911z00_4994;

												{	/* Ieee/input.scm 156 */
													obj_t BgL_tmpz00_7264;

													BgL_tmpz00_7264 = ((obj_t) BgL_iportz00_2242);
													BgL_arg1910z00_4993 =
														RGC_BUFFER_FORWARD(BgL_tmpz00_7264);
												}
												{	/* Ieee/input.scm 156 */
													obj_t BgL_tmpz00_7267;

													BgL_tmpz00_7267 = ((obj_t) BgL_iportz00_2242);
													BgL_arg1911z00_4994 =
														RGC_BUFFER_BUFPOS(BgL_tmpz00_7267);
												}
												{
													long BgL_bufposz00_7271;
													long BgL_forwardz00_7270;

													BgL_forwardz00_7270 = BgL_arg1910z00_4993;
													BgL_bufposz00_7271 = BgL_arg1911z00_4994;
													BgL_bufposz00_4990 = BgL_bufposz00_7271;
													BgL_forwardz00_4989 = BgL_forwardz00_7270;
													goto BgL_statezd20zd21058z00_4988;
												}
											}
										else
											{	/* Ieee/input.scm 156 */
												BgL_matchz00_2409 = 1L;
											}
									}
								else
									{	/* Ieee/input.scm 156 */
										int BgL_curz00_4995;

										{	/* Ieee/input.scm 156 */
											obj_t BgL_tmpz00_7272;

											BgL_tmpz00_7272 = ((obj_t) BgL_iportz00_2242);
											BgL_curz00_4995 =
												RGC_BUFFER_GET_CHAR(BgL_tmpz00_7272,
												BgL_forwardz00_4989);
										}
										{	/* Ieee/input.scm 156 */

											{	/* Ieee/input.scm 156 */
												long BgL_arg1912z00_4996;

												BgL_arg1912z00_4996 = (1L + BgL_forwardz00_4989);
												{	/* Ieee/input.scm 156 */
													long BgL_newzd2matchzd2_5005;

													{	/* Ieee/input.scm 156 */
														obj_t BgL_tmpz00_7276;

														BgL_tmpz00_7276 = ((obj_t) BgL_iportz00_2242);
														RGC_STOP_MATCH(BgL_tmpz00_7276,
															BgL_arg1912z00_4996);
													}
													BgL_newzd2matchzd2_5005 = 0L;
													BgL_matchz00_2409 = BgL_newzd2matchzd2_5005;
						}}}}}}
						{	/* Ieee/input.scm 156 */
							obj_t BgL_tmpz00_7279;

							BgL_tmpz00_7279 = ((obj_t) BgL_iportz00_2242);
							RGC_SET_FILEPOS(BgL_tmpz00_7279);
						}
						switch (BgL_matchz00_2409)
							{
							case 1L:

								{	/* Ieee/input.scm 156 */
									bool_t BgL_test3649z00_7282;

									{	/* Ieee/input.scm 156 */
										long BgL_arg1994z00_2399;

										{	/* Ieee/input.scm 156 */
											obj_t BgL_tmpz00_7283;

											BgL_tmpz00_7283 = ((obj_t) BgL_iportz00_2242);
											BgL_arg1994z00_2399 =
												RGC_BUFFER_MATCH_LENGTH(BgL_tmpz00_7283);
										}
										BgL_test3649z00_7282 = (BgL_arg1994z00_2399 == 0L);
									}
									if (BgL_test3649z00_7282)
										{	/* Ieee/input.scm 156 */
											return BEOF;
										}
									else
										{	/* Ieee/input.scm 156 */
											unsigned char BgL_tmpz00_7287;

											{	/* Ieee/input.scm 156 */
												obj_t BgL_tmpz00_7288;

												BgL_tmpz00_7288 = ((obj_t) BgL_iportz00_2242);
												BgL_tmpz00_7287 = RGC_BUFFER_CHARACTER(BgL_tmpz00_7288);
											}
											return BCHAR(BgL_tmpz00_7287);
										}
								}
								break;
							case 0L:

								{	/* Ieee/input.scm 158 */
									int BgL_cz00_2412;

									{	/* Ieee/input.scm 156 */
										obj_t BgL_tmpz00_7292;

										BgL_tmpz00_7292 = ((obj_t) BgL_iportz00_2242);
										BgL_cz00_2412 = RGC_BUFFER_BYTE(BgL_tmpz00_7292);
									}
									rgc_buffer_unget_char(
										((obj_t) BgL_iportz00_2242), BgL_cz00_2412);
									return BINT(BgL_cz00_2412);
								}
								break;
							default:
								return
									BGl_errorz00zz__errorz00
									(BGl_string3440z00zz__r4_input_6_10_2z00,
									BGl_string3441z00zz__r4_input_6_10_2z00,
									BINT(BgL_matchz00_2409));
							}
					}
				}
			}
		}

	}



/* eof-object */
	BGL_EXPORTED_DEF obj_t BGl_eofzd2objectzd2zz__r4_input_6_10_2z00(void)
	{
		{	/* Ieee/input.scm 166 */
			return BEOF;
		}

	}



/* &eof-object */
	obj_t BGl_z62eofzd2objectzb0zz__r4_input_6_10_2z00(obj_t BgL_envz00_6257)
	{
		{	/* Ieee/input.scm 166 */
			return BGl_eofzd2objectzd2zz__r4_input_6_10_2z00();
		}

	}



/* eof-object? */
	BGL_EXPORTED_DEF bool_t BGl_eofzd2objectzf3z21zz__r4_input_6_10_2z00(obj_t
		BgL_objectz00_22)
	{
		{	/* Ieee/input.scm 172 */
			return EOF_OBJECTP(BgL_objectz00_22);
		}

	}



/* &eof-object? */
	obj_t BGl_z62eofzd2objectzf3z43zz__r4_input_6_10_2z00(obj_t BgL_envz00_6255,
		obj_t BgL_objectz00_6256)
	{
		{	/* Ieee/input.scm 172 */
			return BBOOL(EOF_OBJECTP(BgL_objectz00_6256));
		}

	}



/* _char-ready? */
	obj_t BGl__charzd2readyzf3z21zz__r4_input_6_10_2z00(obj_t BgL_env1260z00_25,
		obj_t BgL_opt1259z00_24)
	{
		{	/* Ieee/input.scm 178 */
			{	/* Ieee/input.scm 178 */

				switch (VECTOR_LENGTH(BgL_opt1259z00_24))
					{
					case 0L:

						{	/* Ieee/input.scm 178 */
							obj_t BgL_ipz00_6520;

							{	/* Ieee/input.scm 178 */
								obj_t BgL_tmpz00_7305;

								BgL_tmpz00_7305 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_ipz00_6520 = BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_7305);
							}
							{	/* Ieee/input.scm 178 */

								return BBOOL(bgl_rgc_charready(BgL_ipz00_6520));
							}
						}
						break;
					case 1L:

						{	/* Ieee/input.scm 178 */
							obj_t BgL_ipz00_6521;

							BgL_ipz00_6521 = VECTOR_REF(BgL_opt1259z00_24, 0L);
							{	/* Ieee/input.scm 178 */

								{	/* Ieee/input.scm 179 */
									bool_t BgL_tmpz00_7311;

									{	/* Ieee/input.scm 179 */
										obj_t BgL_tmpz00_7312;

										if (INPUT_PORTP(BgL_ipz00_6521))
											{	/* Ieee/input.scm 179 */
												BgL_tmpz00_7312 = BgL_ipz00_6521;
											}
										else
											{
												obj_t BgL_auxz00_7315;

												BgL_auxz00_7315 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3447z00zz__r4_input_6_10_2z00, BINT(7849L),
													BGl_string3452z00zz__r4_input_6_10_2z00,
													BGl_string3450z00zz__r4_input_6_10_2z00,
													BgL_ipz00_6521);
												FAILURE(BgL_auxz00_7315, BFALSE, BFALSE);
											}
										BgL_tmpz00_7311 = bgl_rgc_charready(BgL_tmpz00_7312);
									}
									return BBOOL(BgL_tmpz00_7311);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* char-ready? */
	BGL_EXPORTED_DEF bool_t BGl_charzd2readyzf3z21zz__r4_input_6_10_2z00(obj_t
		BgL_ipz00_23)
	{
		{	/* Ieee/input.scm 178 */
			return bgl_rgc_charready(BgL_ipz00_23);
		}

	}



/* _read-line */
	obj_t BGl__readzd2linezd2zz__r4_input_6_10_2z00(obj_t BgL_env1264z00_28,
		obj_t BgL_opt1263z00_27)
	{
		{	/* Ieee/input.scm 184 */
			{	/* Ieee/input.scm 184 */

				switch (VECTOR_LENGTH(BgL_opt1263z00_27))
					{
					case 0L:

						{	/* Ieee/input.scm 184 */
							obj_t BgL_ipz00_2452;

							{	/* Ieee/input.scm 184 */
								obj_t BgL_tmpz00_7324;

								BgL_tmpz00_7324 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_ipz00_2452 = BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_7324);
							}
							{	/* Ieee/input.scm 184 */

								return BGl_readzd2linezd2zz__r4_input_6_10_2z00(BgL_ipz00_2452);
							}
						}
						break;
					case 1L:

						{	/* Ieee/input.scm 184 */
							obj_t BgL_ipz00_2453;

							BgL_ipz00_2453 = VECTOR_REF(BgL_opt1263z00_27, 0L);
							{	/* Ieee/input.scm 184 */

								return BGl_readzd2linezd2zz__r4_input_6_10_2z00(BgL_ipz00_2453);
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* read-line */
	BGL_EXPORTED_DEF obj_t BGl_readzd2linezd2zz__r4_input_6_10_2z00(obj_t
		BgL_ipz00_26)
	{
		{	/* Ieee/input.scm 184 */
			{	/* Ieee/input.scm 185 */
				bool_t BgL_test3651z00_7332;

				{	/* Ieee/input.scm 185 */
					long BgL_arg2171z00_2785;

					BgL_arg2171z00_2785 = BGL_INPUT_PORT_BUFSIZ(BgL_ipz00_26);
					BgL_test3651z00_7332 = (BgL_arg2171z00_2785 > 2L);
				}
				if (BgL_test3651z00_7332)
					{
						obj_t BgL_iportz00_2458;

						BgL_iportz00_2458 = BgL_ipz00_26;
						{
							int BgL_minz00_2605;
							int BgL_maxz00_2606;
							obj_t BgL_iportz00_2568;
							long BgL_lastzd2matchzd2_2569;
							long BgL_forwardz00_2570;
							long BgL_bufposz00_2571;
							obj_t BgL_iportz00_2540;
							long BgL_lastzd2matchzd2_2541;
							long BgL_forwardz00_2542;
							long BgL_bufposz00_2543;
							obj_t BgL_iportz00_2527;
							long BgL_lastzd2matchzd2_2528;
							long BgL_forwardz00_2529;
							long BgL_bufposz00_2530;
							obj_t BgL_iportz00_2512;
							long BgL_lastzd2matchzd2_2513;
							long BgL_forwardz00_2514;
							long BgL_bufposz00_2515;
							obj_t BgL_iportz00_2496;
							long BgL_lastzd2matchzd2_2497;
							long BgL_forwardz00_2498;
							long BgL_bufposz00_2499;

							{	/* Ieee/input.scm 186 */
								obj_t BgL_tmpz00_7335;

								BgL_tmpz00_7335 = ((obj_t) BgL_iportz00_2458);
								RGC_START_MATCH(BgL_tmpz00_7335);
							}
							{	/* Ieee/input.scm 186 */
								long BgL_matchz00_2712;

								{	/* Ieee/input.scm 186 */
									long BgL_arg2156z00_2719;
									long BgL_arg2157z00_2720;

									{	/* Ieee/input.scm 186 */
										obj_t BgL_tmpz00_7338;

										BgL_tmpz00_7338 = ((obj_t) BgL_iportz00_2458);
										BgL_arg2156z00_2719 = RGC_BUFFER_FORWARD(BgL_tmpz00_7338);
									}
									{	/* Ieee/input.scm 186 */
										obj_t BgL_tmpz00_7341;

										BgL_tmpz00_7341 = ((obj_t) BgL_iportz00_2458);
										BgL_arg2157z00_2720 = RGC_BUFFER_BUFPOS(BgL_tmpz00_7341);
									}
									BgL_iportz00_2512 = BgL_iportz00_2458;
									BgL_lastzd2matchzd2_2513 = 4L;
									BgL_forwardz00_2514 = BgL_arg2156z00_2719;
									BgL_bufposz00_2515 = BgL_arg2157z00_2720;
								BgL_zc3z04anonymousza32017ze3z87_2516:
									if ((BgL_forwardz00_2514 == BgL_bufposz00_2515))
										{	/* Ieee/input.scm 186 */
											if (rgc_fill_buffer(((obj_t) BgL_iportz00_2512)))
												{	/* Ieee/input.scm 186 */
													long BgL_arg2020z00_2519;
													long BgL_arg2021z00_2520;

													{	/* Ieee/input.scm 186 */
														obj_t BgL_tmpz00_7349;

														BgL_tmpz00_7349 = ((obj_t) BgL_iportz00_2512);
														BgL_arg2020z00_2519 =
															RGC_BUFFER_FORWARD(BgL_tmpz00_7349);
													}
													{	/* Ieee/input.scm 186 */
														obj_t BgL_tmpz00_7352;

														BgL_tmpz00_7352 = ((obj_t) BgL_iportz00_2512);
														BgL_arg2021z00_2520 =
															RGC_BUFFER_BUFPOS(BgL_tmpz00_7352);
													}
													{
														long BgL_bufposz00_7356;
														long BgL_forwardz00_7355;

														BgL_forwardz00_7355 = BgL_arg2020z00_2519;
														BgL_bufposz00_7356 = BgL_arg2021z00_2520;
														BgL_bufposz00_2515 = BgL_bufposz00_7356;
														BgL_forwardz00_2514 = BgL_forwardz00_7355;
														goto BgL_zc3z04anonymousza32017ze3z87_2516;
													}
												}
											else
												{	/* Ieee/input.scm 186 */
													BgL_matchz00_2712 = BgL_lastzd2matchzd2_2513;
												}
										}
									else
										{	/* Ieee/input.scm 186 */
											int BgL_curz00_2521;

											{	/* Ieee/input.scm 186 */
												obj_t BgL_tmpz00_7357;

												BgL_tmpz00_7357 = ((obj_t) BgL_iportz00_2512);
												BgL_curz00_2521 =
													RGC_BUFFER_GET_CHAR(BgL_tmpz00_7357,
													BgL_forwardz00_2514);
											}
											{	/* Ieee/input.scm 186 */

												if (((long) (BgL_curz00_2521) == 13L))
													{	/* Ieee/input.scm 186 */
														BgL_iportz00_2527 = BgL_iportz00_2512;
														BgL_lastzd2matchzd2_2528 = BgL_lastzd2matchzd2_2513;
														BgL_forwardz00_2529 = (1L + BgL_forwardz00_2514);
														BgL_bufposz00_2530 = BgL_bufposz00_2515;
													BgL_zc3z04anonymousza32028ze3z87_2531:
														{	/* Ieee/input.scm 186 */
															long BgL_newzd2matchzd2_2532;

															{	/* Ieee/input.scm 186 */
																obj_t BgL_tmpz00_7363;

																BgL_tmpz00_7363 = ((obj_t) BgL_iportz00_2527);
																RGC_STOP_MATCH(BgL_tmpz00_7363,
																	BgL_forwardz00_2529);
															}
															BgL_newzd2matchzd2_2532 = 3L;
															if ((BgL_forwardz00_2529 == BgL_bufposz00_2530))
																{	/* Ieee/input.scm 186 */
																	if (rgc_fill_buffer(
																			((obj_t) BgL_iportz00_2527)))
																		{	/* Ieee/input.scm 186 */
																			long BgL_arg2031z00_2535;
																			long BgL_arg2033z00_2536;

																			{	/* Ieee/input.scm 186 */
																				obj_t BgL_tmpz00_7371;

																				BgL_tmpz00_7371 =
																					((obj_t) BgL_iportz00_2527);
																				BgL_arg2031z00_2535 =
																					RGC_BUFFER_FORWARD(BgL_tmpz00_7371);
																			}
																			{	/* Ieee/input.scm 186 */
																				obj_t BgL_tmpz00_7374;

																				BgL_tmpz00_7374 =
																					((obj_t) BgL_iportz00_2527);
																				BgL_arg2033z00_2536 =
																					RGC_BUFFER_BUFPOS(BgL_tmpz00_7374);
																			}
																			{
																				long BgL_bufposz00_7378;
																				long BgL_forwardz00_7377;

																				BgL_forwardz00_7377 =
																					BgL_arg2031z00_2535;
																				BgL_bufposz00_7378 =
																					BgL_arg2033z00_2536;
																				BgL_bufposz00_2530 = BgL_bufposz00_7378;
																				BgL_forwardz00_2529 =
																					BgL_forwardz00_7377;
																				goto
																					BgL_zc3z04anonymousza32028ze3z87_2531;
																			}
																		}
																	else
																		{	/* Ieee/input.scm 186 */
																			BgL_matchz00_2712 =
																				BgL_newzd2matchzd2_2532;
																		}
																}
															else
																{	/* Ieee/input.scm 186 */
																	int BgL_curz00_2537;

																	{	/* Ieee/input.scm 186 */
																		obj_t BgL_tmpz00_7379;

																		BgL_tmpz00_7379 =
																			((obj_t) BgL_iportz00_2527);
																		BgL_curz00_2537 =
																			RGC_BUFFER_GET_CHAR(BgL_tmpz00_7379,
																			BgL_forwardz00_2529);
																	}
																	{	/* Ieee/input.scm 186 */

																		if (((long) (BgL_curz00_2537) == 10L))
																			{	/* Ieee/input.scm 186 */
																				long BgL_arg2036z00_2539;

																				BgL_arg2036z00_2539 =
																					(1L + BgL_forwardz00_2529);
																				{	/* Ieee/input.scm 186 */
																					long BgL_newzd2matchzd2_5063;

																					{	/* Ieee/input.scm 186 */
																						obj_t BgL_tmpz00_7386;

																						BgL_tmpz00_7386 =
																							((obj_t) BgL_iportz00_2527);
																						RGC_STOP_MATCH(BgL_tmpz00_7386,
																							BgL_arg2036z00_2539);
																					}
																					BgL_newzd2matchzd2_5063 = 3L;
																					BgL_matchz00_2712 =
																						BgL_newzd2matchzd2_5063;
																			}}
																		else
																			{	/* Ieee/input.scm 186 */
																				BgL_matchz00_2712 =
																					BgL_newzd2matchzd2_2532;
																			}
																	}
																}
														}
													}
												else
													{	/* Ieee/input.scm 186 */
														if (((long) (BgL_curz00_2521) == 10L))
															{	/* Ieee/input.scm 186 */
																long BgL_arg2026z00_2525;

																BgL_arg2026z00_2525 =
																	(1L + BgL_forwardz00_2514);
																{	/* Ieee/input.scm 186 */
																	long BgL_newzd2matchzd2_5048;

																	{	/* Ieee/input.scm 186 */
																		obj_t BgL_tmpz00_7394;

																		BgL_tmpz00_7394 =
																			((obj_t) BgL_iportz00_2512);
																		RGC_STOP_MATCH(BgL_tmpz00_7394,
																			BgL_arg2026z00_2525);
																	}
																	BgL_newzd2matchzd2_5048 = 3L;
																	BgL_matchz00_2712 = BgL_newzd2matchzd2_5048;
															}}
														else
															{	/* Ieee/input.scm 186 */
																BgL_iportz00_2496 = BgL_iportz00_2512;
																BgL_lastzd2matchzd2_2497 =
																	BgL_lastzd2matchzd2_2513;
																BgL_forwardz00_2498 =
																	(1L + BgL_forwardz00_2514);
																BgL_bufposz00_2499 = BgL_bufposz00_2515;
															BgL_zc3z04anonymousza32007ze3z87_2500:
																{	/* Ieee/input.scm 186 */
																	long BgL_newzd2matchzd2_2501;

																	{	/* Ieee/input.scm 186 */
																		obj_t BgL_tmpz00_7397;

																		BgL_tmpz00_7397 =
																			((obj_t) BgL_iportz00_2496);
																		RGC_STOP_MATCH(BgL_tmpz00_7397,
																			BgL_forwardz00_2498);
																	}
																	BgL_newzd2matchzd2_2501 = 2L;
																	if (
																		(BgL_forwardz00_2498 == BgL_bufposz00_2499))
																		{	/* Ieee/input.scm 186 */
																			if (rgc_fill_buffer(
																					((obj_t) BgL_iportz00_2496)))
																				{	/* Ieee/input.scm 186 */
																					long BgL_arg2010z00_2504;
																					long BgL_arg2011z00_2505;

																					{	/* Ieee/input.scm 186 */
																						obj_t BgL_tmpz00_7405;

																						BgL_tmpz00_7405 =
																							((obj_t) BgL_iportz00_2496);
																						BgL_arg2010z00_2504 =
																							RGC_BUFFER_FORWARD
																							(BgL_tmpz00_7405);
																					}
																					{	/* Ieee/input.scm 186 */
																						obj_t BgL_tmpz00_7408;

																						BgL_tmpz00_7408 =
																							((obj_t) BgL_iportz00_2496);
																						BgL_arg2011z00_2505 =
																							RGC_BUFFER_BUFPOS
																							(BgL_tmpz00_7408);
																					}
																					{
																						long BgL_bufposz00_7412;
																						long BgL_forwardz00_7411;

																						BgL_forwardz00_7411 =
																							BgL_arg2010z00_2504;
																						BgL_bufposz00_7412 =
																							BgL_arg2011z00_2505;
																						BgL_bufposz00_2499 =
																							BgL_bufposz00_7412;
																						BgL_forwardz00_2498 =
																							BgL_forwardz00_7411;
																						goto
																							BgL_zc3z04anonymousza32007ze3z87_2500;
																					}
																				}
																			else
																				{	/* Ieee/input.scm 186 */
																					BgL_matchz00_2712 =
																						BgL_newzd2matchzd2_2501;
																				}
																		}
																	else
																		{	/* Ieee/input.scm 186 */
																			int BgL_curz00_2506;

																			{	/* Ieee/input.scm 186 */
																				obj_t BgL_tmpz00_7413;

																				BgL_tmpz00_7413 =
																					((obj_t) BgL_iportz00_2496);
																				BgL_curz00_2506 =
																					RGC_BUFFER_GET_CHAR(BgL_tmpz00_7413,
																					BgL_forwardz00_2498);
																			}
																			{	/* Ieee/input.scm 186 */

																				if (((long) (BgL_curz00_2506) == 13L))
																					{	/* Ieee/input.scm 186 */
																						BgL_iportz00_2568 =
																							BgL_iportz00_2496;
																						BgL_lastzd2matchzd2_2569 =
																							BgL_newzd2matchzd2_2501;
																						BgL_forwardz00_2570 =
																							(1L + BgL_forwardz00_2498);
																						BgL_bufposz00_2571 =
																							BgL_bufposz00_2499;
																					BgL_zc3z04anonymousza32050ze3z87_2572:
																						{	/* Ieee/input.scm 186 */
																							long BgL_newzd2matchzd2_2573;

																							{	/* Ieee/input.scm 186 */
																								obj_t BgL_tmpz00_7419;

																								BgL_tmpz00_7419 =
																									((obj_t) BgL_iportz00_2568);
																								RGC_STOP_MATCH(BgL_tmpz00_7419,
																									BgL_forwardz00_2570);
																							}
																							BgL_newzd2matchzd2_2573 = 0L;
																							if (
																								(BgL_forwardz00_2570 ==
																									BgL_bufposz00_2571))
																								{	/* Ieee/input.scm 186 */
																									if (rgc_fill_buffer(
																											((obj_t)
																												BgL_iportz00_2568)))
																										{	/* Ieee/input.scm 186 */
																											long BgL_arg2055z00_2576;
																											long BgL_arg2056z00_2577;

																											{	/* Ieee/input.scm 186 */
																												obj_t BgL_tmpz00_7427;

																												BgL_tmpz00_7427 =
																													((obj_t)
																													BgL_iportz00_2568);
																												BgL_arg2055z00_2576 =
																													RGC_BUFFER_FORWARD
																													(BgL_tmpz00_7427);
																											}
																											{	/* Ieee/input.scm 186 */
																												obj_t BgL_tmpz00_7430;

																												BgL_tmpz00_7430 =
																													((obj_t)
																													BgL_iportz00_2568);
																												BgL_arg2056z00_2577 =
																													RGC_BUFFER_BUFPOS
																													(BgL_tmpz00_7430);
																											}
																											{
																												long BgL_bufposz00_7434;
																												long
																													BgL_forwardz00_7433;
																												BgL_forwardz00_7433 =
																													BgL_arg2055z00_2576;
																												BgL_bufposz00_7434 =
																													BgL_arg2056z00_2577;
																												BgL_bufposz00_2571 =
																													BgL_bufposz00_7434;
																												BgL_forwardz00_2570 =
																													BgL_forwardz00_7433;
																												goto
																													BgL_zc3z04anonymousza32050ze3z87_2572;
																											}
																										}
																									else
																										{	/* Ieee/input.scm 186 */
																											BgL_matchz00_2712 =
																												BgL_newzd2matchzd2_2573;
																										}
																								}
																							else
																								{	/* Ieee/input.scm 186 */
																									int BgL_curz00_2578;

																									{	/* Ieee/input.scm 186 */
																										obj_t BgL_tmpz00_7435;

																										BgL_tmpz00_7435 =
																											((obj_t)
																											BgL_iportz00_2568);
																										BgL_curz00_2578 =
																											RGC_BUFFER_GET_CHAR
																											(BgL_tmpz00_7435,
																											BgL_forwardz00_2570);
																									}
																									{	/* Ieee/input.scm 186 */

																										if (
																											((long) (BgL_curz00_2578)
																												== 10L))
																											{	/* Ieee/input.scm 186 */
																												long
																													BgL_arg2058z00_2580;
																												BgL_arg2058z00_2580 =
																													(1L +
																													BgL_forwardz00_2570);
																												{	/* Ieee/input.scm 186 */
																													long
																														BgL_newzd2matchzd2_5100;
																													{	/* Ieee/input.scm 186 */
																														obj_t
																															BgL_tmpz00_7442;
																														BgL_tmpz00_7442 =
																															((obj_t)
																															BgL_iportz00_2568);
																														RGC_STOP_MATCH
																															(BgL_tmpz00_7442,
																															BgL_arg2058z00_2580);
																													}
																													BgL_newzd2matchzd2_5100
																														= 1L;
																													BgL_matchz00_2712 =
																														BgL_newzd2matchzd2_5100;
																											}}
																										else
																											{	/* Ieee/input.scm 186 */
																												BgL_matchz00_2712 =
																													BgL_newzd2matchzd2_2573;
																											}
																									}
																								}
																						}
																					}
																				else
																					{	/* Ieee/input.scm 186 */
																						if (
																							((long) (BgL_curz00_2506) == 10L))
																							{	/* Ieee/input.scm 186 */
																								long BgL_arg2015z00_2510;

																								BgL_arg2015z00_2510 =
																									(1L + BgL_forwardz00_2498);
																								{	/* Ieee/input.scm 186 */
																									long BgL_newzd2matchzd2_5033;

																									{	/* Ieee/input.scm 186 */
																										obj_t BgL_tmpz00_7450;

																										BgL_tmpz00_7450 =
																											((obj_t)
																											BgL_iportz00_2496);
																										RGC_STOP_MATCH
																											(BgL_tmpz00_7450,
																											BgL_arg2015z00_2510);
																									}
																									BgL_newzd2matchzd2_5033 = 0L;
																									BgL_matchz00_2712 =
																										BgL_newzd2matchzd2_5033;
																							}}
																						else
																							{	/* Ieee/input.scm 186 */
																								BgL_iportz00_2540 =
																									BgL_iportz00_2496;
																								BgL_lastzd2matchzd2_2541 =
																									BgL_newzd2matchzd2_2501;
																								BgL_forwardz00_2542 =
																									(1L + BgL_forwardz00_2498);
																								BgL_bufposz00_2543 =
																									BgL_bufposz00_2499;
																							BgL_zc3z04anonymousza32037ze3z87_2544:
																								{	/* Ieee/input.scm 186 */
																									long BgL_newzd2matchzd2_2545;

																									{	/* Ieee/input.scm 186 */
																										obj_t BgL_tmpz00_7453;

																										BgL_tmpz00_7453 =
																											((obj_t)
																											BgL_iportz00_2540);
																										RGC_STOP_MATCH
																											(BgL_tmpz00_7453,
																											BgL_forwardz00_2542);
																									}
																									BgL_newzd2matchzd2_2545 = 2L;
																									if (
																										(BgL_forwardz00_2542 ==
																											BgL_bufposz00_2543))
																										{	/* Ieee/input.scm 186 */
																											if (rgc_fill_buffer(
																													((obj_t)
																														BgL_iportz00_2540)))
																												{	/* Ieee/input.scm 186 */
																													long
																														BgL_arg2040z00_2548;
																													long
																														BgL_arg2041z00_2549;
																													{	/* Ieee/input.scm 186 */
																														obj_t
																															BgL_tmpz00_7461;
																														BgL_tmpz00_7461 =
																															((obj_t)
																															BgL_iportz00_2540);
																														BgL_arg2040z00_2548
																															=
																															RGC_BUFFER_FORWARD
																															(BgL_tmpz00_7461);
																													}
																													{	/* Ieee/input.scm 186 */
																														obj_t
																															BgL_tmpz00_7464;
																														BgL_tmpz00_7464 =
																															((obj_t)
																															BgL_iportz00_2540);
																														BgL_arg2041z00_2549
																															=
																															RGC_BUFFER_BUFPOS
																															(BgL_tmpz00_7464);
																													}
																													{
																														long
																															BgL_bufposz00_7468;
																														long
																															BgL_forwardz00_7467;
																														BgL_forwardz00_7467
																															=
																															BgL_arg2040z00_2548;
																														BgL_bufposz00_7468 =
																															BgL_arg2041z00_2549;
																														BgL_bufposz00_2543 =
																															BgL_bufposz00_7468;
																														BgL_forwardz00_2542
																															=
																															BgL_forwardz00_7467;
																														goto
																															BgL_zc3z04anonymousza32037ze3z87_2544;
																													}
																												}
																											else
																												{	/* Ieee/input.scm 186 */
																													BgL_matchz00_2712 =
																														BgL_newzd2matchzd2_2545;
																												}
																										}
																									else
																										{	/* Ieee/input.scm 186 */
																											int BgL_curz00_2550;

																											{	/* Ieee/input.scm 186 */
																												obj_t BgL_tmpz00_7469;

																												BgL_tmpz00_7469 =
																													((obj_t)
																													BgL_iportz00_2540);
																												BgL_curz00_2550 =
																													RGC_BUFFER_GET_CHAR
																													(BgL_tmpz00_7469,
																													BgL_forwardz00_2542);
																											}
																											{	/* Ieee/input.scm 186 */

																												if (
																													((long)
																														(BgL_curz00_2550) ==
																														13L))
																													{
																														long
																															BgL_bufposz00_7479;
																														long
																															BgL_forwardz00_7477;
																														long
																															BgL_lastzd2matchzd2_7476;
																														obj_t
																															BgL_iportz00_7475;
																														BgL_iportz00_7475 =
																															BgL_iportz00_2540;
																														BgL_lastzd2matchzd2_7476
																															=
																															BgL_newzd2matchzd2_2545;
																														BgL_forwardz00_7477
																															=
																															(1L +
																															BgL_forwardz00_2542);
																														BgL_bufposz00_7479 =
																															BgL_bufposz00_2543;
																														BgL_bufposz00_2571 =
																															BgL_bufposz00_7479;
																														BgL_forwardz00_2570
																															=
																															BgL_forwardz00_7477;
																														BgL_lastzd2matchzd2_2569
																															=
																															BgL_lastzd2matchzd2_7476;
																														BgL_iportz00_2568 =
																															BgL_iportz00_7475;
																														goto
																															BgL_zc3z04anonymousza32050ze3z87_2572;
																													}
																												else
																													{	/* Ieee/input.scm 186 */
																														if (
																															((long)
																																(BgL_curz00_2550)
																																== 10L))
																															{	/* Ieee/input.scm 186 */
																																long
																																	BgL_arg2046z00_2554;
																																BgL_arg2046z00_2554
																																	=
																																	(1L +
																																	BgL_forwardz00_2542);
																																{	/* Ieee/input.scm 186 */
																																	long
																																		BgL_newzd2matchzd2_5079;
																																	{	/* Ieee/input.scm 186 */
																																		obj_t
																																			BgL_tmpz00_7484;
																																		BgL_tmpz00_7484
																																			=
																																			((obj_t)
																																			BgL_iportz00_2540);
																																		RGC_STOP_MATCH
																																			(BgL_tmpz00_7484,
																																			BgL_arg2046z00_2554);
																																	}
																																	BgL_newzd2matchzd2_5079
																																		= 0L;
																																	BgL_matchz00_2712
																																		=
																																		BgL_newzd2matchzd2_5079;
																															}}
																														else
																															{
																																long
																																	BgL_forwardz00_7488;
																																long
																																	BgL_lastzd2matchzd2_7487;
																																BgL_lastzd2matchzd2_7487
																																	=
																																	BgL_newzd2matchzd2_2545;
																																BgL_forwardz00_7488
																																	=
																																	(1L +
																																	BgL_forwardz00_2542);
																																BgL_forwardz00_2542
																																	=
																																	BgL_forwardz00_7488;
																																BgL_lastzd2matchzd2_2541
																																	=
																																	BgL_lastzd2matchzd2_7487;
																																goto
																																	BgL_zc3z04anonymousza32037ze3z87_2544;
																															}
																													}
																											}
																										}
																								}
																							}
																					}
																			}
																		}
																}
															}
													}
											}
										}
								}
								{	/* Ieee/input.scm 186 */
									obj_t BgL_tmpz00_7492;

									BgL_tmpz00_7492 = ((obj_t) BgL_iportz00_2458);
									RGC_SET_FILEPOS(BgL_tmpz00_7492);
								}
								switch (BgL_matchz00_2712)
									{
									case 4L:

										{	/* Ieee/input.scm 186 */
											bool_t BgL_test3670z00_7495;

											{	/* Ieee/input.scm 186 */
												long BgL_arg2144z00_2702;

												{	/* Ieee/input.scm 186 */
													obj_t BgL_tmpz00_7496;

													BgL_tmpz00_7496 = ((obj_t) BgL_iportz00_2458);
													BgL_arg2144z00_2702 =
														RGC_BUFFER_MATCH_LENGTH(BgL_tmpz00_7496);
												}
												BgL_test3670z00_7495 = (BgL_arg2144z00_2702 == 0L);
											}
											if (BgL_test3670z00_7495)
												{	/* Ieee/input.scm 186 */
													return BEOF;
												}
											else
												{	/* Ieee/input.scm 186 */
													unsigned char BgL_tmpz00_7500;

													{	/* Ieee/input.scm 186 */
														obj_t BgL_tmpz00_7501;

														BgL_tmpz00_7501 = ((obj_t) BgL_iportz00_2458);
														BgL_tmpz00_7500 =
															RGC_BUFFER_CHARACTER(BgL_tmpz00_7501);
													}
													return BCHAR(BgL_tmpz00_7500);
												}
										}
										break;
									case 3L:

										return BGl_string3453z00zz__r4_input_6_10_2z00;
										break;
									case 2L:

										{	/* Ieee/input.scm 186 */
											long BgL_arg2067z00_5139;

											{	/* Ieee/input.scm 186 */
												obj_t BgL_tmpz00_7505;

												BgL_tmpz00_7505 = ((obj_t) BgL_iportz00_2458);
												BgL_arg2067z00_5139 =
													RGC_BUFFER_MATCH_LENGTH(BgL_tmpz00_7505);
											}
											return
												rgc_buffer_substring(
												((obj_t) BgL_iportz00_2458), 0L, BgL_arg2067z00_5139);
										}
										break;
									case 1L:

										{	/* Ieee/input.scm 191 */
											long BgL_arg2151z00_2715;

											{	/* Ieee/input.scm 191 */
												long BgL_arg2152z00_2716;

												{	/* Ieee/input.scm 186 */
													obj_t BgL_tmpz00_7510;

													BgL_tmpz00_7510 = ((obj_t) BgL_iportz00_2458);
													BgL_arg2152z00_2716 =
														RGC_BUFFER_MATCH_LENGTH(BgL_tmpz00_7510);
												}
												BgL_arg2151z00_2715 = (BgL_arg2152z00_2716 - 2L);
											}
											BgL_minz00_2605 = (int) (0L);
											BgL_maxz00_2606 = (int) (BgL_arg2151z00_2715);
										BgL_lambda2068z00_2607:
											if (((long) (BgL_maxz00_2606) < (long) (BgL_minz00_2605)))
												{	/* Ieee/input.scm 186 */
													long BgL_arg2070z00_2609;

													{	/* Ieee/input.scm 186 */
														obj_t BgL_tmpz00_7518;

														BgL_tmpz00_7518 = ((obj_t) BgL_iportz00_2458);
														BgL_arg2070z00_2609 =
															RGC_BUFFER_MATCH_LENGTH(BgL_tmpz00_7518);
													}
													{	/* Ieee/input.scm 186 */
														long BgL_za72za7_5117;

														BgL_za72za7_5117 = (long) (BgL_maxz00_2606);
														BgL_maxz00_2606 =
															(int) ((BgL_arg2070z00_2609 + BgL_za72za7_5117));
												}}
											else
												{	/* Ieee/input.scm 186 */
													BFALSE;
												}
											{	/* Ieee/input.scm 186 */
												bool_t BgL_test3672z00_7524;

												if (((long) (BgL_minz00_2605) >= 0L))
													{	/* Ieee/input.scm 186 */
														if (
															((long) (BgL_maxz00_2606) >=
																(long) (BgL_minz00_2605)))
															{	/* Ieee/input.scm 186 */
																long BgL_arg2080z00_2620;

																{	/* Ieee/input.scm 186 */
																	obj_t BgL_tmpz00_7532;

																	BgL_tmpz00_7532 = ((obj_t) BgL_iportz00_2458);
																	BgL_arg2080z00_2620 =
																		RGC_BUFFER_MATCH_LENGTH(BgL_tmpz00_7532);
																}
																BgL_test3672z00_7524 =
																	(
																	(long) (BgL_maxz00_2606) <=
																	BgL_arg2080z00_2620);
															}
														else
															{	/* Ieee/input.scm 186 */
																BgL_test3672z00_7524 = ((bool_t) 0);
															}
													}
												else
													{	/* Ieee/input.scm 186 */
														BgL_test3672z00_7524 = ((bool_t) 0);
													}
												if (BgL_test3672z00_7524)
													{	/* Ieee/input.scm 186 */
														long BgL_startz00_5125;
														long BgL_stopz00_5126;

														BgL_startz00_5125 = (long) (BgL_minz00_2605);
														BgL_stopz00_5126 = (long) (BgL_maxz00_2606);
														return
															rgc_buffer_substring(
															((obj_t) BgL_iportz00_2458), BgL_startz00_5125,
															BgL_stopz00_5126);
													}
												else
													{	/* Ieee/input.scm 186 */
														obj_t BgL_arg2076z00_2614;
														obj_t BgL_arg2077z00_2615;

														{	/* Ieee/input.scm 186 */
															obj_t BgL_arg2078z00_2616;

															{	/* Ieee/input.scm 186 */
																long BgL_arg2067z00_5127;

																{	/* Ieee/input.scm 186 */
																	obj_t BgL_tmpz00_7541;

																	BgL_tmpz00_7541 = ((obj_t) BgL_iportz00_2458);
																	BgL_arg2067z00_5127 =
																		RGC_BUFFER_MATCH_LENGTH(BgL_tmpz00_7541);
																}
																BgL_arg2078z00_2616 =
																	rgc_buffer_substring(
																	((obj_t) BgL_iportz00_2458), 0L,
																	BgL_arg2067z00_5127);
															}
															{	/* Ieee/input.scm 186 */
																obj_t BgL_list2079z00_2617;

																BgL_list2079z00_2617 =
																	MAKE_YOUNG_PAIR(BgL_arg2078z00_2616, BNIL);
																BgL_arg2076z00_2614 =
																	BGl_formatz00zz__r4_output_6_10_3z00
																	(BGl_string3442z00zz__r4_input_6_10_2z00,
																	BgL_list2079z00_2617);
														}}
														BgL_arg2077z00_2615 =
															MAKE_YOUNG_PAIR(BINT(BgL_minz00_2605),
															BINT(BgL_maxz00_2606));
														return
															BGl_errorz00zz__errorz00
															(BGl_string3443z00zz__r4_input_6_10_2z00,
															BgL_arg2076z00_2614, BgL_arg2077z00_2615);
													}
											}
										}
										break;
									case 0L:

										{	/* Ieee/input.scm 189 */
											long BgL_arg2154z00_2717;

											{	/* Ieee/input.scm 189 */
												long BgL_arg2155z00_2718;

												{	/* Ieee/input.scm 186 */
													obj_t BgL_tmpz00_7554;

													BgL_tmpz00_7554 = ((obj_t) BgL_iportz00_2458);
													BgL_arg2155z00_2718 =
														RGC_BUFFER_MATCH_LENGTH(BgL_tmpz00_7554);
												}
												BgL_arg2154z00_2717 = (BgL_arg2155z00_2718 - 1L);
											}
											{
												int BgL_maxz00_7560;
												int BgL_minz00_7558;

												BgL_minz00_7558 = (int) (0L);
												BgL_maxz00_7560 = (int) (BgL_arg2154z00_2717);
												BgL_maxz00_2606 = BgL_maxz00_7560;
												BgL_minz00_2605 = BgL_minz00_7558;
												goto BgL_lambda2068z00_2607;
											}
										}
										break;
									default:
										return
											BGl_errorz00zz__errorz00
											(BGl_string3440z00zz__r4_input_6_10_2z00,
											BGl_string3441z00zz__r4_input_6_10_2z00,
											BINT(BgL_matchz00_2712));
									}
							}
						}
					}
				else
					{	/* Ieee/input.scm 201 */
						obj_t BgL_g1078z00_2758;
						obj_t BgL_g1079z00_2759;

						BgL_g1078z00_2758 =
							BGl_readzd2charzd2zz__r4_input_6_10_2z00(BgL_ipz00_26);
						{	/* Ieee/string.scm 172 */

							BgL_g1079z00_2759 = make_string(100L, ((unsigned char) ' '));
						}
						{
							obj_t BgL_cz00_2761;
							long BgL_wz00_2762;
							long BgL_mz00_2763;
							obj_t BgL_accz00_2764;

							BgL_cz00_2761 = BgL_g1078z00_2758;
							BgL_wz00_2762 = 0L;
							BgL_mz00_2763 = 100L;
							BgL_accz00_2764 = BgL_g1079z00_2759;
						BgL_zc3z04anonymousza32158ze3z87_2765:
							if (EOF_OBJECTP(BgL_cz00_2761))
								{	/* Ieee/input.scm 206 */
									if ((BgL_wz00_2762 == 0L))
										{	/* Ieee/input.scm 208 */
											return BgL_cz00_2761;
										}
									else
										{	/* Ieee/input.scm 208 */
											return c_substring(BgL_accz00_2764, 0L, BgL_wz00_2762);
										}
								}
							else
								{	/* Ieee/input.scm 206 */
									if ((BgL_wz00_2762 == BgL_mz00_2763))
										{	/* Ieee/input.scm 213 */
											long BgL_arg2162z00_2769;
											obj_t BgL_arg2163z00_2770;

											BgL_arg2162z00_2769 = (BgL_mz00_2763 * 2L);
											{	/* Ieee/input.scm 214 */
												obj_t BgL_newzd2acczd2_2771;

												{	/* Ieee/input.scm 214 */
													long BgL_arg2164z00_2772;

													BgL_arg2164z00_2772 = (BgL_mz00_2763 * 2L);
													{	/* Ieee/string.scm 172 */

														BgL_newzd2acczd2_2771 =
															make_string(BgL_arg2164z00_2772,
															((unsigned char) ' '));
												}}
												blit_string(BgL_accz00_2764, 0L, BgL_newzd2acczd2_2771,
													0L, BgL_mz00_2763);
												BgL_arg2163z00_2770 = BgL_newzd2acczd2_2771;
											}
											{
												obj_t BgL_accz00_7579;
												long BgL_mz00_7578;

												BgL_mz00_7578 = BgL_arg2162z00_2769;
												BgL_accz00_7579 = BgL_arg2163z00_2770;
												BgL_accz00_2764 = BgL_accz00_7579;
												BgL_mz00_2763 = BgL_mz00_7578;
												goto BgL_zc3z04anonymousza32158ze3z87_2765;
											}
										}
									else
										{	/* Ieee/input.scm 209 */
											if ((CCHAR(BgL_cz00_2761) == ((unsigned char) 13)))
												{	/* Ieee/input.scm 218 */
													obj_t BgL_c2z00_2776;

													BgL_c2z00_2776 =
														BGl_readzd2charzd2zz__r4_input_6_10_2z00
														(BgL_ipz00_26);
													if ((CCHAR(BgL_c2z00_2776) == ((unsigned char) 10)))
														{	/* Ieee/input.scm 219 */
															return
																c_substring(BgL_accz00_2764, 0L, BgL_wz00_2762);
														}
													else
														{	/* Ieee/input.scm 219 */
															{	/* Ieee/input.scm 222 */
																unsigned char BgL_tmpz00_7588;

																BgL_tmpz00_7588 = CCHAR(BgL_cz00_2761);
																STRING_SET(BgL_accz00_2764, BgL_wz00_2762,
																	BgL_tmpz00_7588);
															}
															{
																long BgL_wz00_7592;
																obj_t BgL_cz00_7591;

																BgL_cz00_7591 = BgL_c2z00_2776;
																BgL_wz00_7592 = (BgL_wz00_2762 + 1L);
																BgL_wz00_2762 = BgL_wz00_7592;
																BgL_cz00_2761 = BgL_cz00_7591;
																goto BgL_zc3z04anonymousza32158ze3z87_2765;
															}
														}
												}
											else
												{	/* Ieee/input.scm 217 */
													if ((CCHAR(BgL_cz00_2761) == ((unsigned char) 10)))
														{	/* Ieee/input.scm 224 */
															return
																c_substring(BgL_accz00_2764, 0L, BgL_wz00_2762);
														}
													else
														{	/* Ieee/input.scm 224 */
															{	/* Ieee/input.scm 229 */
																unsigned char BgL_tmpz00_7598;

																BgL_tmpz00_7598 = CCHAR(BgL_cz00_2761);
																STRING_SET(BgL_accz00_2764, BgL_wz00_2762,
																	BgL_tmpz00_7598);
															}
															{
																long BgL_wz00_7603;
																obj_t BgL_cz00_7601;

																BgL_cz00_7601 =
																	BGl_readzd2charzd2zz__r4_input_6_10_2z00
																	(BgL_ipz00_26);
																BgL_wz00_7603 = (BgL_wz00_2762 + 1L);
																BgL_wz00_2762 = BgL_wz00_7603;
																BgL_cz00_2761 = BgL_cz00_7601;
																goto BgL_zc3z04anonymousza32158ze3z87_2765;
															}
														}
												}
										}
								}
						}
					}
			}
		}

	}



/* _read-line-newline */
	obj_t BGl__readzd2linezd2newlinez00zz__r4_input_6_10_2z00(obj_t
		BgL_env1268z00_31, obj_t BgL_opt1267z00_30)
	{
		{	/* Ieee/input.scm 235 */
			{	/* Ieee/input.scm 235 */

				switch (VECTOR_LENGTH(BgL_opt1267z00_30))
					{
					case 0L:

						{	/* Ieee/input.scm 235 */
							obj_t BgL_ipz00_2788;

							{	/* Ieee/input.scm 235 */
								obj_t BgL_tmpz00_7605;

								BgL_tmpz00_7605 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_ipz00_2788 = BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_7605);
							}
							{	/* Ieee/input.scm 235 */

								return
									BGl_readzd2linezd2newlinez00zz__r4_input_6_10_2z00
									(BgL_ipz00_2788);
							}
						}
						break;
					case 1L:

						{	/* Ieee/input.scm 235 */
							obj_t BgL_ipz00_2789;

							BgL_ipz00_2789 = VECTOR_REF(BgL_opt1267z00_30, 0L);
							{	/* Ieee/input.scm 235 */

								return
									BGl_readzd2linezd2newlinez00zz__r4_input_6_10_2z00
									(BgL_ipz00_2789);
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* read-line-newline */
	BGL_EXPORTED_DEF obj_t
		BGl_readzd2linezd2newlinez00zz__r4_input_6_10_2z00(obj_t BgL_ipz00_29)
	{
		{	/* Ieee/input.scm 235 */
			{	/* Ieee/input.scm 236 */
				bool_t BgL_test3681z00_7613;

				{	/* Ieee/input.scm 236 */
					long BgL_arg2333z00_3106;

					BgL_arg2333z00_3106 = BGL_INPUT_PORT_BUFSIZ(BgL_ipz00_29);
					BgL_test3681z00_7613 = (BgL_arg2333z00_3106 > 2L);
				}
				if (BgL_test3681z00_7613)
					{
						obj_t BgL_iportz00_2794;

						BgL_iportz00_2794 = BgL_ipz00_29;
						{
							obj_t BgL_iportz00_2896;
							long BgL_lastzd2matchzd2_2897;
							long BgL_forwardz00_2898;
							long BgL_bufposz00_2899;
							obj_t BgL_iportz00_2880;
							long BgL_lastzd2matchzd2_2881;
							long BgL_forwardz00_2882;
							long BgL_bufposz00_2883;
							obj_t BgL_iportz00_2865;
							long BgL_lastzd2matchzd2_2866;
							long BgL_forwardz00_2867;
							long BgL_bufposz00_2868;
							obj_t BgL_iportz00_2852;
							long BgL_lastzd2matchzd2_2853;
							long BgL_forwardz00_2854;
							long BgL_bufposz00_2855;
							obj_t BgL_iportz00_2830;
							long BgL_lastzd2matchzd2_2831;
							long BgL_forwardz00_2832;
							long BgL_bufposz00_2833;

							{	/* Ieee/input.scm 237 */
								obj_t BgL_tmpz00_7616;

								BgL_tmpz00_7616 = ((obj_t) BgL_iportz00_2794);
								RGC_START_MATCH(BgL_tmpz00_7616);
							}
							{	/* Ieee/input.scm 237 */
								long BgL_matchz00_3034;

								{	/* Ieee/input.scm 237 */
									long BgL_arg2311z00_3037;
									long BgL_arg2312z00_3038;

									{	/* Ieee/input.scm 237 */
										obj_t BgL_tmpz00_7619;

										BgL_tmpz00_7619 = ((obj_t) BgL_iportz00_2794);
										BgL_arg2311z00_3037 = RGC_BUFFER_FORWARD(BgL_tmpz00_7619);
									}
									{	/* Ieee/input.scm 237 */
										obj_t BgL_tmpz00_7622;

										BgL_tmpz00_7622 = ((obj_t) BgL_iportz00_2794);
										BgL_arg2312z00_3038 = RGC_BUFFER_BUFPOS(BgL_tmpz00_7622);
									}
									BgL_iportz00_2865 = BgL_iportz00_2794;
									BgL_lastzd2matchzd2_2866 = 1L;
									BgL_forwardz00_2867 = BgL_arg2311z00_3037;
									BgL_bufposz00_2868 = BgL_arg2312z00_3038;
								BgL_zc3z04anonymousza32193ze3z87_2869:
									if ((BgL_forwardz00_2867 == BgL_bufposz00_2868))
										{	/* Ieee/input.scm 237 */
											if (rgc_fill_buffer(((obj_t) BgL_iportz00_2865)))
												{	/* Ieee/input.scm 237 */
													long BgL_arg2196z00_2872;
													long BgL_arg2197z00_2873;

													{	/* Ieee/input.scm 237 */
														obj_t BgL_tmpz00_7630;

														BgL_tmpz00_7630 = ((obj_t) BgL_iportz00_2865);
														BgL_arg2196z00_2872 =
															RGC_BUFFER_FORWARD(BgL_tmpz00_7630);
													}
													{	/* Ieee/input.scm 237 */
														obj_t BgL_tmpz00_7633;

														BgL_tmpz00_7633 = ((obj_t) BgL_iportz00_2865);
														BgL_arg2197z00_2873 =
															RGC_BUFFER_BUFPOS(BgL_tmpz00_7633);
													}
													{
														long BgL_bufposz00_7637;
														long BgL_forwardz00_7636;

														BgL_forwardz00_7636 = BgL_arg2196z00_2872;
														BgL_bufposz00_7637 = BgL_arg2197z00_2873;
														BgL_bufposz00_2868 = BgL_bufposz00_7637;
														BgL_forwardz00_2867 = BgL_forwardz00_7636;
														goto BgL_zc3z04anonymousza32193ze3z87_2869;
													}
												}
											else
												{	/* Ieee/input.scm 237 */
													BgL_matchz00_3034 = BgL_lastzd2matchzd2_2866;
												}
										}
									else
										{	/* Ieee/input.scm 237 */
											int BgL_curz00_2874;

											{	/* Ieee/input.scm 237 */
												obj_t BgL_tmpz00_7638;

												BgL_tmpz00_7638 = ((obj_t) BgL_iportz00_2865);
												BgL_curz00_2874 =
													RGC_BUFFER_GET_CHAR(BgL_tmpz00_7638,
													BgL_forwardz00_2867);
											}
											{	/* Ieee/input.scm 237 */

												if (((long) (BgL_curz00_2874) == 13L))
													{	/* Ieee/input.scm 237 */
														BgL_iportz00_2852 = BgL_iportz00_2865;
														BgL_lastzd2matchzd2_2853 = BgL_lastzd2matchzd2_2866;
														BgL_forwardz00_2854 = (1L + BgL_forwardz00_2867);
														BgL_bufposz00_2855 = BgL_bufposz00_2868;
													BgL_zc3z04anonymousza32186ze3z87_2856:
														{	/* Ieee/input.scm 237 */
															long BgL_newzd2matchzd2_2857;

															{	/* Ieee/input.scm 237 */
																obj_t BgL_tmpz00_7644;

																BgL_tmpz00_7644 = ((obj_t) BgL_iportz00_2852);
																RGC_STOP_MATCH(BgL_tmpz00_7644,
																	BgL_forwardz00_2854);
															}
															BgL_newzd2matchzd2_2857 = 0L;
															if ((BgL_forwardz00_2854 == BgL_bufposz00_2855))
																{	/* Ieee/input.scm 237 */
																	if (rgc_fill_buffer(
																			((obj_t) BgL_iportz00_2852)))
																		{	/* Ieee/input.scm 237 */
																			long BgL_arg2189z00_2860;
																			long BgL_arg2190z00_2861;

																			{	/* Ieee/input.scm 237 */
																				obj_t BgL_tmpz00_7652;

																				BgL_tmpz00_7652 =
																					((obj_t) BgL_iportz00_2852);
																				BgL_arg2189z00_2860 =
																					RGC_BUFFER_FORWARD(BgL_tmpz00_7652);
																			}
																			{	/* Ieee/input.scm 237 */
																				obj_t BgL_tmpz00_7655;

																				BgL_tmpz00_7655 =
																					((obj_t) BgL_iportz00_2852);
																				BgL_arg2190z00_2861 =
																					RGC_BUFFER_BUFPOS(BgL_tmpz00_7655);
																			}
																			{
																				long BgL_bufposz00_7659;
																				long BgL_forwardz00_7658;

																				BgL_forwardz00_7658 =
																					BgL_arg2189z00_2860;
																				BgL_bufposz00_7659 =
																					BgL_arg2190z00_2861;
																				BgL_bufposz00_2855 = BgL_bufposz00_7659;
																				BgL_forwardz00_2854 =
																					BgL_forwardz00_7658;
																				goto
																					BgL_zc3z04anonymousza32186ze3z87_2856;
																			}
																		}
																	else
																		{	/* Ieee/input.scm 237 */
																			BgL_matchz00_3034 =
																				BgL_newzd2matchzd2_2857;
																		}
																}
															else
																{	/* Ieee/input.scm 237 */
																	int BgL_curz00_2862;

																	{	/* Ieee/input.scm 237 */
																		obj_t BgL_tmpz00_7660;

																		BgL_tmpz00_7660 =
																			((obj_t) BgL_iportz00_2852);
																		BgL_curz00_2862 =
																			RGC_BUFFER_GET_CHAR(BgL_tmpz00_7660,
																			BgL_forwardz00_2854);
																	}
																	{	/* Ieee/input.scm 237 */

																		if (((long) (BgL_curz00_2862) == 10L))
																			{	/* Ieee/input.scm 237 */
																				long BgL_arg2192z00_2864;

																				BgL_arg2192z00_2864 =
																					(1L + BgL_forwardz00_2854);
																				{	/* Ieee/input.scm 237 */
																					long BgL_newzd2matchzd2_5210;

																					{	/* Ieee/input.scm 237 */
																						obj_t BgL_tmpz00_7667;

																						BgL_tmpz00_7667 =
																							((obj_t) BgL_iportz00_2852);
																						RGC_STOP_MATCH(BgL_tmpz00_7667,
																							BgL_arg2192z00_2864);
																					}
																					BgL_newzd2matchzd2_5210 = 0L;
																					BgL_matchz00_3034 =
																						BgL_newzd2matchzd2_5210;
																			}}
																		else
																			{	/* Ieee/input.scm 237 */
																				BgL_matchz00_3034 =
																					BgL_newzd2matchzd2_2857;
																			}
																	}
																}
														}
													}
												else
													{	/* Ieee/input.scm 237 */
														if (((long) (BgL_curz00_2874) == 10L))
															{	/* Ieee/input.scm 237 */
																long BgL_arg2201z00_2878;

																BgL_arg2201z00_2878 =
																	(1L + BgL_forwardz00_2867);
																{	/* Ieee/input.scm 237 */
																	long BgL_newzd2matchzd2_5224;

																	{	/* Ieee/input.scm 237 */
																		obj_t BgL_tmpz00_7675;

																		BgL_tmpz00_7675 =
																			((obj_t) BgL_iportz00_2865);
																		RGC_STOP_MATCH(BgL_tmpz00_7675,
																			BgL_arg2201z00_2878);
																	}
																	BgL_newzd2matchzd2_5224 = 0L;
																	BgL_matchz00_3034 = BgL_newzd2matchzd2_5224;
															}}
														else
															{	/* Ieee/input.scm 237 */
																BgL_iportz00_2880 = BgL_iportz00_2865;
																BgL_lastzd2matchzd2_2881 =
																	BgL_lastzd2matchzd2_2866;
																BgL_forwardz00_2882 =
																	(1L + BgL_forwardz00_2867);
																BgL_bufposz00_2883 = BgL_bufposz00_2868;
															BgL_zc3z04anonymousza32203ze3z87_2884:
																{	/* Ieee/input.scm 237 */
																	long BgL_newzd2matchzd2_2885;

																	{	/* Ieee/input.scm 237 */
																		obj_t BgL_tmpz00_7678;

																		BgL_tmpz00_7678 =
																			((obj_t) BgL_iportz00_2880);
																		RGC_STOP_MATCH(BgL_tmpz00_7678,
																			BgL_forwardz00_2882);
																	}
																	BgL_newzd2matchzd2_2885 = 0L;
																	if (
																		(BgL_forwardz00_2882 == BgL_bufposz00_2883))
																		{	/* Ieee/input.scm 237 */
																			if (rgc_fill_buffer(
																					((obj_t) BgL_iportz00_2880)))
																				{	/* Ieee/input.scm 237 */
																					long BgL_arg2206z00_2888;
																					long BgL_arg2207z00_2889;

																					{	/* Ieee/input.scm 237 */
																						obj_t BgL_tmpz00_7686;

																						BgL_tmpz00_7686 =
																							((obj_t) BgL_iportz00_2880);
																						BgL_arg2206z00_2888 =
																							RGC_BUFFER_FORWARD
																							(BgL_tmpz00_7686);
																					}
																					{	/* Ieee/input.scm 237 */
																						obj_t BgL_tmpz00_7689;

																						BgL_tmpz00_7689 =
																							((obj_t) BgL_iportz00_2880);
																						BgL_arg2207z00_2889 =
																							RGC_BUFFER_BUFPOS
																							(BgL_tmpz00_7689);
																					}
																					{
																						long BgL_bufposz00_7693;
																						long BgL_forwardz00_7692;

																						BgL_forwardz00_7692 =
																							BgL_arg2206z00_2888;
																						BgL_bufposz00_7693 =
																							BgL_arg2207z00_2889;
																						BgL_bufposz00_2883 =
																							BgL_bufposz00_7693;
																						BgL_forwardz00_2882 =
																							BgL_forwardz00_7692;
																						goto
																							BgL_zc3z04anonymousza32203ze3z87_2884;
																					}
																				}
																			else
																				{	/* Ieee/input.scm 237 */
																					BgL_matchz00_3034 =
																						BgL_newzd2matchzd2_2885;
																				}
																		}
																	else
																		{	/* Ieee/input.scm 237 */
																			int BgL_curz00_2890;

																			{	/* Ieee/input.scm 237 */
																				obj_t BgL_tmpz00_7694;

																				BgL_tmpz00_7694 =
																					((obj_t) BgL_iportz00_2880);
																				BgL_curz00_2890 =
																					RGC_BUFFER_GET_CHAR(BgL_tmpz00_7694,
																					BgL_forwardz00_2882);
																			}
																			{	/* Ieee/input.scm 237 */

																				if (((long) (BgL_curz00_2890) == 13L))
																					{	/* Ieee/input.scm 237 */
																						BgL_iportz00_2896 =
																							BgL_iportz00_2880;
																						BgL_lastzd2matchzd2_2897 =
																							BgL_newzd2matchzd2_2885;
																						BgL_forwardz00_2898 =
																							(1L + BgL_forwardz00_2882);
																						BgL_bufposz00_2899 =
																							BgL_bufposz00_2883;
																					BgL_zc3z04anonymousza32213ze3z87_2900:
																						{	/* Ieee/input.scm 237 */
																							long BgL_newzd2matchzd2_2901;

																							{	/* Ieee/input.scm 237 */
																								obj_t BgL_tmpz00_7700;

																								BgL_tmpz00_7700 =
																									((obj_t) BgL_iportz00_2896);
																								RGC_STOP_MATCH(BgL_tmpz00_7700,
																									BgL_forwardz00_2898);
																							}
																							BgL_newzd2matchzd2_2901 = 0L;
																							if (
																								(BgL_forwardz00_2898 ==
																									BgL_bufposz00_2899))
																								{	/* Ieee/input.scm 237 */
																									if (rgc_fill_buffer(
																											((obj_t)
																												BgL_iportz00_2896)))
																										{	/* Ieee/input.scm 237 */
																											long BgL_arg2216z00_2904;
																											long BgL_arg2217z00_2905;

																											{	/* Ieee/input.scm 237 */
																												obj_t BgL_tmpz00_7708;

																												BgL_tmpz00_7708 =
																													((obj_t)
																													BgL_iportz00_2896);
																												BgL_arg2216z00_2904 =
																													RGC_BUFFER_FORWARD
																													(BgL_tmpz00_7708);
																											}
																											{	/* Ieee/input.scm 237 */
																												obj_t BgL_tmpz00_7711;

																												BgL_tmpz00_7711 =
																													((obj_t)
																													BgL_iportz00_2896);
																												BgL_arg2217z00_2905 =
																													RGC_BUFFER_BUFPOS
																													(BgL_tmpz00_7711);
																											}
																											{
																												long BgL_bufposz00_7715;
																												long
																													BgL_forwardz00_7714;
																												BgL_forwardz00_7714 =
																													BgL_arg2216z00_2904;
																												BgL_bufposz00_7715 =
																													BgL_arg2217z00_2905;
																												BgL_bufposz00_2899 =
																													BgL_bufposz00_7715;
																												BgL_forwardz00_2898 =
																													BgL_forwardz00_7714;
																												goto
																													BgL_zc3z04anonymousza32213ze3z87_2900;
																											}
																										}
																									else
																										{	/* Ieee/input.scm 237 */
																											BgL_matchz00_3034 =
																												BgL_newzd2matchzd2_2901;
																										}
																								}
																							else
																								{	/* Ieee/input.scm 237 */
																									int BgL_curz00_2906;

																									{	/* Ieee/input.scm 237 */
																										obj_t BgL_tmpz00_7716;

																										BgL_tmpz00_7716 =
																											((obj_t)
																											BgL_iportz00_2896);
																										BgL_curz00_2906 =
																											RGC_BUFFER_GET_CHAR
																											(BgL_tmpz00_7716,
																											BgL_forwardz00_2898);
																									}
																									{	/* Ieee/input.scm 237 */

																										if (
																											((long) (BgL_curz00_2906)
																												== 10L))
																											{	/* Ieee/input.scm 237 */
																												long
																													BgL_arg2219z00_2908;
																												BgL_arg2219z00_2908 =
																													(1L +
																													BgL_forwardz00_2898);
																												{	/* Ieee/input.scm 237 */
																													long
																														BgL_newzd2matchzd2_5256;
																													{	/* Ieee/input.scm 237 */
																														obj_t
																															BgL_tmpz00_7723;
																														BgL_tmpz00_7723 =
																															((obj_t)
																															BgL_iportz00_2896);
																														RGC_STOP_MATCH
																															(BgL_tmpz00_7723,
																															BgL_arg2219z00_2908);
																													}
																													BgL_newzd2matchzd2_5256
																														= 0L;
																													BgL_matchz00_3034 =
																														BgL_newzd2matchzd2_5256;
																											}}
																										else
																											{	/* Ieee/input.scm 237 */
																												BgL_matchz00_3034 =
																													BgL_newzd2matchzd2_2901;
																											}
																									}
																								}
																						}
																					}
																				else
																					{	/* Ieee/input.scm 237 */
																						if (
																							((long) (BgL_curz00_2890) == 10L))
																							{	/* Ieee/input.scm 237 */
																								long BgL_arg2211z00_2894;

																								BgL_arg2211z00_2894 =
																									(1L + BgL_forwardz00_2882);
																								{	/* Ieee/input.scm 237 */
																									long BgL_newzd2matchzd2_5241;

																									{	/* Ieee/input.scm 237 */
																										obj_t BgL_tmpz00_7731;

																										BgL_tmpz00_7731 =
																											((obj_t)
																											BgL_iportz00_2880);
																										RGC_STOP_MATCH
																											(BgL_tmpz00_7731,
																											BgL_arg2211z00_2894);
																									}
																									BgL_newzd2matchzd2_5241 = 0L;
																									BgL_matchz00_3034 =
																										BgL_newzd2matchzd2_5241;
																							}}
																						else
																							{	/* Ieee/input.scm 237 */
																								BgL_iportz00_2830 =
																									BgL_iportz00_2880;
																								BgL_lastzd2matchzd2_2831 =
																									BgL_newzd2matchzd2_2885;
																								BgL_forwardz00_2832 =
																									(1L + BgL_forwardz00_2882);
																								BgL_bufposz00_2833 =
																									BgL_bufposz00_2883;
																							BgL_zc3z04anonymousza32175ze3z87_2834:
																								{	/* Ieee/input.scm 237 */
																									long BgL_newzd2matchzd2_2835;

																									{	/* Ieee/input.scm 237 */
																										obj_t BgL_tmpz00_7734;

																										BgL_tmpz00_7734 =
																											((obj_t)
																											BgL_iportz00_2830);
																										RGC_STOP_MATCH
																											(BgL_tmpz00_7734,
																											BgL_forwardz00_2832);
																									}
																									BgL_newzd2matchzd2_2835 = 0L;
																									if (
																										(BgL_forwardz00_2832 ==
																											BgL_bufposz00_2833))
																										{	/* Ieee/input.scm 237 */
																											if (rgc_fill_buffer(
																													((obj_t)
																														BgL_iportz00_2830)))
																												{	/* Ieee/input.scm 237 */
																													long
																														BgL_arg2178z00_2838;
																													long
																														BgL_arg2179z00_2839;
																													{	/* Ieee/input.scm 237 */
																														obj_t
																															BgL_tmpz00_7742;
																														BgL_tmpz00_7742 =
																															((obj_t)
																															BgL_iportz00_2830);
																														BgL_arg2178z00_2838
																															=
																															RGC_BUFFER_FORWARD
																															(BgL_tmpz00_7742);
																													}
																													{	/* Ieee/input.scm 237 */
																														obj_t
																															BgL_tmpz00_7745;
																														BgL_tmpz00_7745 =
																															((obj_t)
																															BgL_iportz00_2830);
																														BgL_arg2179z00_2839
																															=
																															RGC_BUFFER_BUFPOS
																															(BgL_tmpz00_7745);
																													}
																													{
																														long
																															BgL_bufposz00_7749;
																														long
																															BgL_forwardz00_7748;
																														BgL_forwardz00_7748
																															=
																															BgL_arg2178z00_2838;
																														BgL_bufposz00_7749 =
																															BgL_arg2179z00_2839;
																														BgL_bufposz00_2833 =
																															BgL_bufposz00_7749;
																														BgL_forwardz00_2832
																															=
																															BgL_forwardz00_7748;
																														goto
																															BgL_zc3z04anonymousza32175ze3z87_2834;
																													}
																												}
																											else
																												{	/* Ieee/input.scm 237 */
																													BgL_matchz00_3034 =
																														BgL_newzd2matchzd2_2835;
																												}
																										}
																									else
																										{	/* Ieee/input.scm 237 */
																											int BgL_curz00_2840;

																											{	/* Ieee/input.scm 237 */
																												obj_t BgL_tmpz00_7750;

																												BgL_tmpz00_7750 =
																													((obj_t)
																													BgL_iportz00_2830);
																												BgL_curz00_2840 =
																													RGC_BUFFER_GET_CHAR
																													(BgL_tmpz00_7750,
																													BgL_forwardz00_2832);
																											}
																											{	/* Ieee/input.scm 237 */

																												if (
																													((long)
																														(BgL_curz00_2840) ==
																														13L))
																													{
																														long
																															BgL_bufposz00_7760;
																														long
																															BgL_forwardz00_7758;
																														long
																															BgL_lastzd2matchzd2_7757;
																														obj_t
																															BgL_iportz00_7756;
																														BgL_iportz00_7756 =
																															BgL_iportz00_2830;
																														BgL_lastzd2matchzd2_7757
																															=
																															BgL_newzd2matchzd2_2835;
																														BgL_forwardz00_7758
																															=
																															(1L +
																															BgL_forwardz00_2832);
																														BgL_bufposz00_7760 =
																															BgL_bufposz00_2833;
																														BgL_bufposz00_2899 =
																															BgL_bufposz00_7760;
																														BgL_forwardz00_2898
																															=
																															BgL_forwardz00_7758;
																														BgL_lastzd2matchzd2_2897
																															=
																															BgL_lastzd2matchzd2_7757;
																														BgL_iportz00_2896 =
																															BgL_iportz00_7756;
																														goto
																															BgL_zc3z04anonymousza32213ze3z87_2900;
																													}
																												else
																													{	/* Ieee/input.scm 237 */
																														if (
																															((long)
																																(BgL_curz00_2840)
																																== 10L))
																															{	/* Ieee/input.scm 237 */
																																long
																																	BgL_arg2183z00_2844;
																																BgL_arg2183z00_2844
																																	=
																																	(1L +
																																	BgL_forwardz00_2832);
																																{	/* Ieee/input.scm 237 */
																																	long
																																		BgL_newzd2matchzd2_5192;
																																	{	/* Ieee/input.scm 237 */
																																		obj_t
																																			BgL_tmpz00_7765;
																																		BgL_tmpz00_7765
																																			=
																																			((obj_t)
																																			BgL_iportz00_2830);
																																		RGC_STOP_MATCH
																																			(BgL_tmpz00_7765,
																																			BgL_arg2183z00_2844);
																																	}
																																	BgL_newzd2matchzd2_5192
																																		= 0L;
																																	BgL_matchz00_3034
																																		=
																																		BgL_newzd2matchzd2_5192;
																															}}
																														else
																															{
																																long
																																	BgL_forwardz00_7769;
																																long
																																	BgL_lastzd2matchzd2_7768;
																																BgL_lastzd2matchzd2_7768
																																	=
																																	BgL_newzd2matchzd2_2835;
																																BgL_forwardz00_7769
																																	=
																																	(1L +
																																	BgL_forwardz00_2832);
																																BgL_forwardz00_2832
																																	=
																																	BgL_forwardz00_7769;
																																BgL_lastzd2matchzd2_2831
																																	=
																																	BgL_lastzd2matchzd2_7768;
																																goto
																																	BgL_zc3z04anonymousza32175ze3z87_2834;
																															}
																													}
																											}
																										}
																								}
																							}
																					}
																			}
																		}
																}
															}
													}
											}
										}
								}
								{	/* Ieee/input.scm 237 */
									obj_t BgL_tmpz00_7773;

									BgL_tmpz00_7773 = ((obj_t) BgL_iportz00_2794);
									RGC_SET_FILEPOS(BgL_tmpz00_7773);
								}
								switch (BgL_matchz00_3034)
									{
									case 1L:

										{	/* Ieee/input.scm 237 */
											bool_t BgL_test3700z00_7776;

											{	/* Ieee/input.scm 237 */
												long BgL_arg2304z00_3024;

												{	/* Ieee/input.scm 237 */
													obj_t BgL_tmpz00_7777;

													BgL_tmpz00_7777 = ((obj_t) BgL_iportz00_2794);
													BgL_arg2304z00_3024 =
														RGC_BUFFER_MATCH_LENGTH(BgL_tmpz00_7777);
												}
												BgL_test3700z00_7776 = (BgL_arg2304z00_3024 == 0L);
											}
											if (BgL_test3700z00_7776)
												{	/* Ieee/input.scm 237 */
													return BEOF;
												}
											else
												{	/* Ieee/input.scm 237 */
													unsigned char BgL_tmpz00_7781;

													{	/* Ieee/input.scm 237 */
														obj_t BgL_tmpz00_7782;

														BgL_tmpz00_7782 = ((obj_t) BgL_iportz00_2794);
														BgL_tmpz00_7781 =
															RGC_BUFFER_CHARACTER(BgL_tmpz00_7782);
													}
													return BCHAR(BgL_tmpz00_7781);
												}
										}
										break;
									case 0L:

										{	/* Ieee/input.scm 237 */
											long BgL_arg2227z00_5274;

											{	/* Ieee/input.scm 237 */
												obj_t BgL_tmpz00_7786;

												BgL_tmpz00_7786 = ((obj_t) BgL_iportz00_2794);
												BgL_arg2227z00_5274 =
													RGC_BUFFER_MATCH_LENGTH(BgL_tmpz00_7786);
											}
											return
												rgc_buffer_substring(
												((obj_t) BgL_iportz00_2794), 0L, BgL_arg2227z00_5274);
										}
										break;
									default:
										return
											BGl_errorz00zz__errorz00
											(BGl_string3440z00zz__r4_input_6_10_2z00,
											BGl_string3441z00zz__r4_input_6_10_2z00,
											BINT(BgL_matchz00_3034));
									}
							}
						}
					}
				else
					{	/* Ieee/input.scm 249 */
						obj_t BgL_g1091z00_3074;
						obj_t BgL_g1092z00_3075;

						BgL_g1091z00_3074 =
							BGl_readzd2charzd2zz__r4_input_6_10_2z00(BgL_ipz00_29);
						{	/* Ieee/string.scm 172 */

							BgL_g1092z00_3075 = make_string(100L, ((unsigned char) ' '));
						}
						{
							obj_t BgL_cz00_3077;
							long BgL_wz00_3078;
							long BgL_mz00_3079;
							obj_t BgL_accz00_3080;

							BgL_cz00_3077 = BgL_g1091z00_3074;
							BgL_wz00_3078 = 0L;
							BgL_mz00_3079 = 100L;
							BgL_accz00_3080 = BgL_g1092z00_3075;
						BgL_zc3z04anonymousza32313ze3z87_3081:
							if (EOF_OBJECTP(BgL_cz00_3077))
								{	/* Ieee/input.scm 254 */
									if ((BgL_wz00_3078 == 0L))
										{	/* Ieee/input.scm 256 */
											return BgL_cz00_3077;
										}
									else
										{	/* Ieee/input.scm 256 */
											return c_substring(BgL_accz00_3080, 0L, BgL_wz00_3078);
										}
								}
							else
								{	/* Ieee/input.scm 254 */
									if ((BgL_wz00_3078 == (BgL_mz00_3079 - 2L)))
										{	/* Ieee/input.scm 261 */
											long BgL_arg2318z00_3086;
											obj_t BgL_arg2319z00_3087;

											BgL_arg2318z00_3086 = (BgL_mz00_3079 * 2L);
											{	/* Ieee/input.scm 262 */
												obj_t BgL_newzd2acczd2_3088;

												{	/* Ieee/input.scm 262 */
													long BgL_arg2320z00_3089;

													BgL_arg2320z00_3089 = (BgL_mz00_3079 * 2L);
													{	/* Ieee/string.scm 172 */

														BgL_newzd2acczd2_3088 =
															make_string(BgL_arg2320z00_3089,
															((unsigned char) ' '));
												}}
												blit_string(BgL_accz00_3080, 0L, BgL_newzd2acczd2_3088,
													0L, BgL_mz00_3079);
												BgL_arg2319z00_3087 = BgL_newzd2acczd2_3088;
											}
											{
												obj_t BgL_accz00_7809;
												long BgL_mz00_7808;

												BgL_mz00_7808 = BgL_arg2318z00_3086;
												BgL_accz00_7809 = BgL_arg2319z00_3087;
												BgL_accz00_3080 = BgL_accz00_7809;
												BgL_mz00_3079 = BgL_mz00_7808;
												goto BgL_zc3z04anonymousza32313ze3z87_3081;
											}
										}
									else
										{	/* Ieee/input.scm 257 */
											if ((CCHAR(BgL_cz00_3077) == ((unsigned char) 13)))
												{	/* Ieee/input.scm 266 */
													obj_t BgL_c2z00_3093;

													BgL_c2z00_3093 =
														BGl_readzd2charzd2zz__r4_input_6_10_2z00
														(BgL_ipz00_29);
													if ((CCHAR(BgL_c2z00_3093) == ((unsigned char) 10)))
														{	/* Ieee/input.scm 267 */
															STRING_SET(BgL_accz00_3080, BgL_wz00_3078,
																((unsigned char) 13));
															{	/* Ieee/input.scm 270 */
																long BgL_tmpz00_7818;

																BgL_tmpz00_7818 = (1L + BgL_wz00_3078);
																STRING_SET(BgL_accz00_3080, BgL_tmpz00_7818,
																	((unsigned char) 10));
															}
															return
																c_substring(BgL_accz00_3080, 0L,
																(BgL_wz00_3078 + 2L));
														}
													else
														{	/* Ieee/input.scm 267 */
															{	/* Ieee/input.scm 273 */
																unsigned char BgL_tmpz00_7823;

																BgL_tmpz00_7823 = CCHAR(BgL_cz00_3077);
																STRING_SET(BgL_accz00_3080, BgL_wz00_3078,
																	BgL_tmpz00_7823);
															}
															{
																long BgL_wz00_7827;
																obj_t BgL_cz00_7826;

																BgL_cz00_7826 = BgL_c2z00_3093;
																BgL_wz00_7827 = (BgL_wz00_3078 + 1L);
																BgL_wz00_3078 = BgL_wz00_7827;
																BgL_cz00_3077 = BgL_cz00_7826;
																goto BgL_zc3z04anonymousza32313ze3z87_3081;
															}
														}
												}
											else
												{	/* Ieee/input.scm 265 */
													if ((CCHAR(BgL_cz00_3077) == ((unsigned char) 10)))
														{	/* Ieee/input.scm 275 */
															STRING_SET(BgL_accz00_3080, BgL_wz00_3078,
																((unsigned char) 10));
															return c_substring(BgL_accz00_3080, 0L,
																(BgL_wz00_3078 + 1L));
														}
													else
														{	/* Ieee/input.scm 275 */
															{	/* Ieee/input.scm 281 */
																unsigned char BgL_tmpz00_7835;

																BgL_tmpz00_7835 = CCHAR(BgL_cz00_3077);
																STRING_SET(BgL_accz00_3080, BgL_wz00_3078,
																	BgL_tmpz00_7835);
															}
															{
																long BgL_wz00_7840;
																obj_t BgL_cz00_7838;

																BgL_cz00_7838 =
																	BGl_readzd2charzd2zz__r4_input_6_10_2z00
																	(BgL_ipz00_29);
																BgL_wz00_7840 = (BgL_wz00_3078 + 1L);
																BgL_wz00_3078 = BgL_wz00_7840;
																BgL_cz00_3077 = BgL_cz00_7838;
																goto BgL_zc3z04anonymousza32313ze3z87_3081;
															}
														}
												}
										}
								}
						}
					}
			}
		}

	}



/* _read-lines */
	obj_t BGl__readzd2lineszd2zz__r4_input_6_10_2z00(obj_t BgL_env1272z00_34,
		obj_t BgL_opt1271z00_33)
	{
		{	/* Ieee/input.scm 287 */
			{	/* Ieee/input.scm 287 */

				switch (VECTOR_LENGTH(BgL_opt1271z00_33))
					{
					case 0L:

						{	/* Ieee/input.scm 287 */
							obj_t BgL_ipz00_3109;

							{	/* Ieee/input.scm 287 */
								obj_t BgL_tmpz00_7842;

								BgL_tmpz00_7842 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_ipz00_3109 = BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_7842);
							}
							{	/* Ieee/input.scm 287 */

								return
									BGl_readzd2lineszd2zz__r4_input_6_10_2z00(BgL_ipz00_3109);
							}
						}
						break;
					case 1L:

						{	/* Ieee/input.scm 287 */
							obj_t BgL_ipz00_3110;

							BgL_ipz00_3110 = VECTOR_REF(BgL_opt1271z00_33, 0L);
							{	/* Ieee/input.scm 287 */

								return
									BGl_readzd2lineszd2zz__r4_input_6_10_2z00(BgL_ipz00_3110);
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* read-lines */
	BGL_EXPORTED_DEF obj_t BGl_readzd2lineszd2zz__r4_input_6_10_2z00(obj_t
		BgL_ipz00_32)
	{
		{	/* Ieee/input.scm 287 */
			{	/* Ieee/input.scm 288 */
				obj_t BgL_g1094z00_3111;

				BgL_g1094z00_3111 =
					BGl_readzd2linezd2zz__r4_input_6_10_2z00(BgL_ipz00_32);
				{
					obj_t BgL_lz00_5329;
					obj_t BgL_lsz00_5330;

					BgL_lz00_5329 = BgL_g1094z00_3111;
					BgL_lsz00_5330 = BNIL;
				BgL_loopz00_5328:
					if (EOF_OBJECTP(BgL_lz00_5329))
						{	/* Ieee/input.scm 290 */
							return bgl_reverse_bang(BgL_lsz00_5330);
						}
					else
						{	/* Ieee/input.scm 292 */
							obj_t BgL_arg2336z00_5335;
							obj_t BgL_arg2337z00_5336;

							BgL_arg2336z00_5335 =
								BGl_readzd2linezd2zz__r4_input_6_10_2z00(BgL_ipz00_32);
							BgL_arg2337z00_5336 =
								MAKE_YOUNG_PAIR(BgL_lz00_5329, BgL_lsz00_5330);
							{
								obj_t BgL_lsz00_7857;
								obj_t BgL_lz00_7856;

								BgL_lz00_7856 = BgL_arg2336z00_5335;
								BgL_lsz00_7857 = BgL_arg2337z00_5336;
								BgL_lsz00_5330 = BgL_lsz00_7857;
								BgL_lz00_5329 = BgL_lz00_7856;
								goto BgL_loopz00_5328;
							}
						}
				}
			}
		}

	}



/* _read-string */
	obj_t BGl__readzd2stringzd2zz__r4_input_6_10_2z00(obj_t BgL_env1276z00_37,
		obj_t BgL_opt1275z00_36)
	{
		{	/* Ieee/input.scm 297 */
			{	/* Ieee/input.scm 297 */

				switch (VECTOR_LENGTH(BgL_opt1275z00_36))
					{
					case 0L:

						{	/* Ieee/input.scm 297 */
							obj_t BgL_ipz00_3123;

							{	/* Ieee/input.scm 297 */
								obj_t BgL_tmpz00_7858;

								BgL_tmpz00_7858 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_ipz00_3123 = BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_7858);
							}
							{	/* Ieee/input.scm 297 */

								return
									BGl_readzd2stringzd2zz__r4_input_6_10_2z00(BgL_ipz00_3123);
							}
						}
						break;
					case 1L:

						{	/* Ieee/input.scm 297 */
							obj_t BgL_ipz00_3124;

							BgL_ipz00_3124 = VECTOR_REF(BgL_opt1275z00_36, 0L);
							{	/* Ieee/input.scm 297 */

								return
									BGl_readzd2stringzd2zz__r4_input_6_10_2z00(BgL_ipz00_3124);
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* read-string */
	BGL_EXPORTED_DEF obj_t BGl_readzd2stringzd2zz__r4_input_6_10_2z00(obj_t
		BgL_ipz00_35)
	{
		{	/* Ieee/input.scm 297 */
			{
				obj_t BgL_iportz00_3180;
				long BgL_lastzd2matchzd2_3181;
				long BgL_forwardz00_3182;
				long BgL_bufposz00_3183;
				obj_t BgL_iportz00_3168;
				long BgL_lastzd2matchzd2_3169;
				long BgL_forwardz00_3170;
				long BgL_bufposz00_3171;

				{	/* Ieee/input.scm 298 */
					obj_t BgL_tmpz00_7866;

					BgL_tmpz00_7866 = ((obj_t) BgL_ipz00_35);
					RGC_START_MATCH(BgL_tmpz00_7866);
				}
				{	/* Ieee/input.scm 298 */
					long BgL_matchz00_3311;

					{	/* Ieee/input.scm 298 */
						long BgL_arg2452z00_3316;
						long BgL_arg2453z00_3317;

						{	/* Ieee/input.scm 298 */
							obj_t BgL_tmpz00_7869;

							BgL_tmpz00_7869 = ((obj_t) BgL_ipz00_35);
							BgL_arg2452z00_3316 = RGC_BUFFER_FORWARD(BgL_tmpz00_7869);
						}
						{	/* Ieee/input.scm 298 */
							obj_t BgL_tmpz00_7872;

							BgL_tmpz00_7872 = ((obj_t) BgL_ipz00_35);
							BgL_arg2453z00_3317 = RGC_BUFFER_BUFPOS(BgL_tmpz00_7872);
						}
						{
							long BgL_forwardz00_5501;
							long BgL_bufposz00_5502;

							BgL_forwardz00_5501 = BgL_arg2452z00_3316;
							BgL_bufposz00_5502 = BgL_arg2453z00_3317;
						BgL_statezd20zd21097z00_5500:
							if ((BgL_forwardz00_5501 == BgL_bufposz00_5502))
								{	/* Ieee/input.scm 298 */
									if (rgc_fill_buffer(((obj_t) BgL_ipz00_35)))
										{	/* Ieee/input.scm 298 */
											long BgL_arg2341z00_5505;
											long BgL_arg2342z00_5506;

											{	/* Ieee/input.scm 298 */
												obj_t BgL_tmpz00_7880;

												BgL_tmpz00_7880 = ((obj_t) BgL_ipz00_35);
												BgL_arg2341z00_5505 =
													RGC_BUFFER_FORWARD(BgL_tmpz00_7880);
											}
											{	/* Ieee/input.scm 298 */
												obj_t BgL_tmpz00_7883;

												BgL_tmpz00_7883 = ((obj_t) BgL_ipz00_35);
												BgL_arg2342z00_5506 =
													RGC_BUFFER_BUFPOS(BgL_tmpz00_7883);
											}
											{
												long BgL_bufposz00_7887;
												long BgL_forwardz00_7886;

												BgL_forwardz00_7886 = BgL_arg2341z00_5505;
												BgL_bufposz00_7887 = BgL_arg2342z00_5506;
												BgL_bufposz00_5502 = BgL_bufposz00_7887;
												BgL_forwardz00_5501 = BgL_forwardz00_7886;
												goto BgL_statezd20zd21097z00_5500;
											}
										}
									else
										{	/* Ieee/input.scm 298 */
											BgL_matchz00_3311 = 1L;
										}
								}
							else
								{	/* Ieee/input.scm 298 */
									int BgL_curz00_5507;

									{	/* Ieee/input.scm 298 */
										obj_t BgL_tmpz00_7888;

										BgL_tmpz00_7888 = ((obj_t) BgL_ipz00_35);
										BgL_curz00_5507 =
											RGC_BUFFER_GET_CHAR(BgL_tmpz00_7888, BgL_forwardz00_5501);
									}
									{	/* Ieee/input.scm 298 */

										BgL_iportz00_3180 = BgL_ipz00_35;
										BgL_lastzd2matchzd2_3181 = 1L;
										BgL_forwardz00_3182 = (1L + BgL_forwardz00_5501);
										BgL_bufposz00_3183 = BgL_bufposz00_5502;
									BgL_zc3z04anonymousza32352ze3z87_3184:
										{	/* Ieee/input.scm 298 */
											long BgL_newzd2matchzd2_5437;

											{	/* Ieee/input.scm 298 */
												obj_t BgL_tmpz00_7891;

												BgL_tmpz00_7891 = ((obj_t) BgL_iportz00_3180);
												RGC_STOP_MATCH(BgL_tmpz00_7891, BgL_forwardz00_3182);
											}
											BgL_newzd2matchzd2_5437 = 0L;
											if ((BgL_forwardz00_3182 == BgL_bufposz00_3183))
												{	/* Ieee/input.scm 298 */
													if (rgc_fill_buffer(((obj_t) BgL_iportz00_3180)))
														{	/* Ieee/input.scm 298 */
															long BgL_arg2355z00_5440;
															long BgL_arg2356z00_5441;

															{	/* Ieee/input.scm 298 */
																obj_t BgL_tmpz00_7899;

																BgL_tmpz00_7899 = ((obj_t) BgL_iportz00_3180);
																BgL_arg2355z00_5440 =
																	RGC_BUFFER_FORWARD(BgL_tmpz00_7899);
															}
															{	/* Ieee/input.scm 298 */
																obj_t BgL_tmpz00_7902;

																BgL_tmpz00_7902 = ((obj_t) BgL_iportz00_3180);
																BgL_arg2356z00_5441 =
																	RGC_BUFFER_BUFPOS(BgL_tmpz00_7902);
															}
															{	/* Ieee/input.scm 298 */
																long BgL_newzd2matchzd2_5451;

																{	/* Ieee/input.scm 298 */
																	obj_t BgL_tmpz00_7905;

																	BgL_tmpz00_7905 = ((obj_t) BgL_iportz00_3180);
																	RGC_STOP_MATCH(BgL_tmpz00_7905,
																		BgL_arg2355z00_5440);
																}
																BgL_newzd2matchzd2_5451 = 0L;
																if (
																	(BgL_arg2355z00_5440 == BgL_arg2356z00_5441))
																	{	/* Ieee/input.scm 298 */
																		if (rgc_fill_buffer(
																				((obj_t) BgL_iportz00_3180)))
																			{	/* Ieee/input.scm 298 */
																				long BgL_arg2355z00_5454;
																				long BgL_arg2356z00_5455;

																				{	/* Ieee/input.scm 298 */
																					obj_t BgL_tmpz00_7913;

																					BgL_tmpz00_7913 =
																						((obj_t) BgL_iportz00_3180);
																					BgL_arg2355z00_5454 =
																						RGC_BUFFER_FORWARD(BgL_tmpz00_7913);
																				}
																				{	/* Ieee/input.scm 298 */
																					obj_t BgL_tmpz00_7916;

																					BgL_tmpz00_7916 =
																						((obj_t) BgL_iportz00_3180);
																					BgL_arg2356z00_5455 =
																						RGC_BUFFER_BUFPOS(BgL_tmpz00_7916);
																				}
																				{
																					long BgL_bufposz00_7920;
																					long BgL_forwardz00_7919;

																					BgL_forwardz00_7919 =
																						BgL_arg2355z00_5454;
																					BgL_bufposz00_7920 =
																						BgL_arg2356z00_5455;
																					BgL_bufposz00_3183 =
																						BgL_bufposz00_7920;
																					BgL_forwardz00_3182 =
																						BgL_forwardz00_7919;
																					goto
																						BgL_zc3z04anonymousza32352ze3z87_3184;
																				}
																			}
																		else
																			{	/* Ieee/input.scm 298 */
																				BgL_matchz00_3311 =
																					BgL_newzd2matchzd2_5451;
																			}
																	}
																else
																	{	/* Ieee/input.scm 298 */
																		int BgL_curz00_5456;

																		{	/* Ieee/input.scm 298 */
																			obj_t BgL_tmpz00_7921;

																			BgL_tmpz00_7921 =
																				((obj_t) BgL_iportz00_3180);
																			BgL_curz00_5456 =
																				RGC_BUFFER_GET_CHAR(BgL_tmpz00_7921,
																				BgL_arg2355z00_5440);
																		}
																		{	/* Ieee/input.scm 298 */

																			BgL_iportz00_3168 = BgL_iportz00_3180;
																			BgL_lastzd2matchzd2_3169 =
																				BgL_newzd2matchzd2_5451;
																			BgL_forwardz00_3170 =
																				(1L + BgL_arg2355z00_5440);
																			BgL_bufposz00_3171 = BgL_arg2356z00_5441;
																		BgL_zc3z04anonymousza32346ze3z87_3172:
																			{	/* Ieee/input.scm 298 */
																				long BgL_newzd2matchzd2_5386;

																				{	/* Ieee/input.scm 298 */
																					obj_t BgL_tmpz00_7924;

																					BgL_tmpz00_7924 =
																						((obj_t) BgL_iportz00_3168);
																					RGC_STOP_MATCH(BgL_tmpz00_7924,
																						BgL_forwardz00_3170);
																				}
																				BgL_newzd2matchzd2_5386 = 0L;
																				if (
																					(BgL_forwardz00_3170 ==
																						BgL_bufposz00_3171))
																					{	/* Ieee/input.scm 298 */
																						if (rgc_fill_buffer(
																								((obj_t) BgL_iportz00_3168)))
																							{	/* Ieee/input.scm 298 */
																								long BgL_arg2349z00_5389;
																								long BgL_arg2350z00_5390;

																								{	/* Ieee/input.scm 298 */
																									obj_t BgL_tmpz00_7932;

																									BgL_tmpz00_7932 =
																										((obj_t) BgL_iportz00_3168);
																									BgL_arg2349z00_5389 =
																										RGC_BUFFER_FORWARD
																										(BgL_tmpz00_7932);
																								}
																								{	/* Ieee/input.scm 298 */
																									obj_t BgL_tmpz00_7935;

																									BgL_tmpz00_7935 =
																										((obj_t) BgL_iportz00_3168);
																									BgL_arg2350z00_5390 =
																										RGC_BUFFER_BUFPOS
																										(BgL_tmpz00_7935);
																								}
																								{	/* Ieee/input.scm 298 */
																									long BgL_newzd2matchzd2_5400;

																									{	/* Ieee/input.scm 298 */
																										obj_t BgL_tmpz00_7938;

																										BgL_tmpz00_7938 =
																											((obj_t)
																											BgL_iportz00_3168);
																										RGC_STOP_MATCH
																											(BgL_tmpz00_7938,
																											BgL_arg2349z00_5389);
																									}
																									BgL_newzd2matchzd2_5400 = 0L;
																									if (
																										(BgL_arg2349z00_5389 ==
																											BgL_arg2350z00_5390))
																										{	/* Ieee/input.scm 298 */
																											if (rgc_fill_buffer(
																													((obj_t)
																														BgL_iportz00_3168)))
																												{	/* Ieee/input.scm 298 */
																													long
																														BgL_arg2349z00_5403;
																													long
																														BgL_arg2350z00_5404;
																													{	/* Ieee/input.scm 298 */
																														obj_t
																															BgL_tmpz00_7946;
																														BgL_tmpz00_7946 =
																															((obj_t)
																															BgL_iportz00_3168);
																														BgL_arg2349z00_5403
																															=
																															RGC_BUFFER_FORWARD
																															(BgL_tmpz00_7946);
																													}
																													{	/* Ieee/input.scm 298 */
																														obj_t
																															BgL_tmpz00_7949;
																														BgL_tmpz00_7949 =
																															((obj_t)
																															BgL_iportz00_3168);
																														BgL_arg2350z00_5404
																															=
																															RGC_BUFFER_BUFPOS
																															(BgL_tmpz00_7949);
																													}
																													{
																														long
																															BgL_bufposz00_7953;
																														long
																															BgL_forwardz00_7952;
																														BgL_forwardz00_7952
																															=
																															BgL_arg2349z00_5403;
																														BgL_bufposz00_7953 =
																															BgL_arg2350z00_5404;
																														BgL_bufposz00_3171 =
																															BgL_bufposz00_7953;
																														BgL_forwardz00_3170
																															=
																															BgL_forwardz00_7952;
																														goto
																															BgL_zc3z04anonymousza32346ze3z87_3172;
																													}
																												}
																											else
																												{	/* Ieee/input.scm 298 */
																													BgL_matchz00_3311 =
																														BgL_newzd2matchzd2_5400;
																												}
																										}
																									else
																										{	/* Ieee/input.scm 298 */
																											int BgL_curz00_5405;

																											{	/* Ieee/input.scm 298 */
																												obj_t BgL_tmpz00_7954;

																												BgL_tmpz00_7954 =
																													((obj_t)
																													BgL_iportz00_3168);
																												BgL_curz00_5405 =
																													RGC_BUFFER_GET_CHAR
																													(BgL_tmpz00_7954,
																													BgL_arg2349z00_5389);
																											}
																											{	/* Ieee/input.scm 298 */

																												{
																													long
																														BgL_bufposz00_7960;
																													long
																														BgL_forwardz00_7958;
																													long
																														BgL_lastzd2matchzd2_7957;
																													BgL_lastzd2matchzd2_7957
																														=
																														BgL_newzd2matchzd2_5400;
																													BgL_forwardz00_7958 =
																														(1L +
																														BgL_arg2349z00_5389);
																													BgL_bufposz00_7960 =
																														BgL_arg2350z00_5390;
																													BgL_bufposz00_3171 =
																														BgL_bufposz00_7960;
																													BgL_forwardz00_3170 =
																														BgL_forwardz00_7958;
																													BgL_lastzd2matchzd2_3169
																														=
																														BgL_lastzd2matchzd2_7957;
																													goto
																														BgL_zc3z04anonymousza32346ze3z87_3172;
																												}
																											}
																										}
																								}
																							}
																						else
																							{	/* Ieee/input.scm 298 */
																								BgL_matchz00_3311 =
																									BgL_newzd2matchzd2_5386;
																							}
																					}
																				else
																					{	/* Ieee/input.scm 298 */
																						int BgL_curz00_5391;

																						{	/* Ieee/input.scm 298 */
																							obj_t BgL_tmpz00_7961;

																							BgL_tmpz00_7961 =
																								((obj_t) BgL_iportz00_3168);
																							BgL_curz00_5391 =
																								RGC_BUFFER_GET_CHAR
																								(BgL_tmpz00_7961,
																								BgL_forwardz00_3170);
																						}
																						{	/* Ieee/input.scm 298 */

																							{	/* Ieee/input.scm 298 */
																								long BgL_arg2351z00_5392;

																								BgL_arg2351z00_5392 =
																									(1L + BgL_forwardz00_3170);
																								{	/* Ieee/input.scm 298 */
																									long BgL_newzd2matchzd2_5420;

																									{	/* Ieee/input.scm 298 */
																										obj_t BgL_tmpz00_7965;

																										BgL_tmpz00_7965 =
																											((obj_t)
																											BgL_iportz00_3168);
																										RGC_STOP_MATCH
																											(BgL_tmpz00_7965,
																											BgL_arg2351z00_5392);
																									}
																									BgL_newzd2matchzd2_5420 = 0L;
																									if (
																										(BgL_arg2351z00_5392 ==
																											BgL_bufposz00_3171))
																										{	/* Ieee/input.scm 298 */
																											if (rgc_fill_buffer(
																													((obj_t)
																														BgL_iportz00_3168)))
																												{	/* Ieee/input.scm 298 */
																													long
																														BgL_arg2349z00_5423;
																													long
																														BgL_arg2350z00_5424;
																													{	/* Ieee/input.scm 298 */
																														obj_t
																															BgL_tmpz00_7973;
																														BgL_tmpz00_7973 =
																															((obj_t)
																															BgL_iportz00_3168);
																														BgL_arg2349z00_5423
																															=
																															RGC_BUFFER_FORWARD
																															(BgL_tmpz00_7973);
																													}
																													{	/* Ieee/input.scm 298 */
																														obj_t
																															BgL_tmpz00_7976;
																														BgL_tmpz00_7976 =
																															((obj_t)
																															BgL_iportz00_3168);
																														BgL_arg2350z00_5424
																															=
																															RGC_BUFFER_BUFPOS
																															(BgL_tmpz00_7976);
																													}
																													{
																														long
																															BgL_bufposz00_7981;
																														long
																															BgL_forwardz00_7980;
																														long
																															BgL_lastzd2matchzd2_7979;
																														BgL_lastzd2matchzd2_7979
																															=
																															BgL_newzd2matchzd2_5386;
																														BgL_forwardz00_7980
																															=
																															BgL_arg2349z00_5423;
																														BgL_bufposz00_7981 =
																															BgL_arg2350z00_5424;
																														BgL_bufposz00_3171 =
																															BgL_bufposz00_7981;
																														BgL_forwardz00_3170
																															=
																															BgL_forwardz00_7980;
																														BgL_lastzd2matchzd2_3169
																															=
																															BgL_lastzd2matchzd2_7979;
																														goto
																															BgL_zc3z04anonymousza32346ze3z87_3172;
																													}
																												}
																											else
																												{	/* Ieee/input.scm 298 */
																													BgL_matchz00_3311 =
																														BgL_newzd2matchzd2_5420;
																												}
																										}
																									else
																										{	/* Ieee/input.scm 298 */
																											int BgL_curz00_5425;

																											{	/* Ieee/input.scm 298 */
																												obj_t BgL_tmpz00_7982;

																												BgL_tmpz00_7982 =
																													((obj_t)
																													BgL_iportz00_3168);
																												BgL_curz00_5425 =
																													RGC_BUFFER_GET_CHAR
																													(BgL_tmpz00_7982,
																													BgL_arg2351z00_5392);
																											}
																											{	/* Ieee/input.scm 298 */

																												{
																													long
																														BgL_forwardz00_7986;
																													long
																														BgL_lastzd2matchzd2_7985;
																													BgL_lastzd2matchzd2_7985
																														=
																														BgL_newzd2matchzd2_5420;
																													BgL_forwardz00_7986 =
																														(1L +
																														BgL_arg2351z00_5392);
																													BgL_forwardz00_3170 =
																														BgL_forwardz00_7986;
																													BgL_lastzd2matchzd2_3169
																														=
																														BgL_lastzd2matchzd2_7985;
																													goto
																														BgL_zc3z04anonymousza32346ze3z87_3172;
																												}
																											}
																										}
																								}
																							}
																						}
																					}
																			}
																		}
																	}
															}
														}
													else
														{	/* Ieee/input.scm 298 */
															BgL_matchz00_3311 = BgL_newzd2matchzd2_5437;
														}
												}
											else
												{	/* Ieee/input.scm 298 */
													int BgL_curz00_5442;

													{	/* Ieee/input.scm 298 */
														obj_t BgL_tmpz00_7989;

														BgL_tmpz00_7989 = ((obj_t) BgL_iportz00_3180);
														BgL_curz00_5442 =
															RGC_BUFFER_GET_CHAR(BgL_tmpz00_7989,
															BgL_forwardz00_3182);
													}
													{	/* Ieee/input.scm 298 */

														{	/* Ieee/input.scm 298 */
															long BgL_arg2357z00_5443;

															BgL_arg2357z00_5443 = (1L + BgL_forwardz00_3182);
															{
																long BgL_lastzd2matchzd2_5472;
																long BgL_forwardz00_5473;
																long BgL_bufposz00_5474;

																BgL_lastzd2matchzd2_5472 =
																	BgL_newzd2matchzd2_5437;
																BgL_forwardz00_5473 = BgL_arg2357z00_5443;
																BgL_bufposz00_5474 = BgL_bufposz00_3183;
															BgL_statezd22zd21099z00_5471:
																{	/* Ieee/input.scm 298 */
																	long BgL_newzd2matchzd2_5475;

																	{	/* Ieee/input.scm 298 */
																		obj_t BgL_tmpz00_7993;

																		BgL_tmpz00_7993 =
																			((obj_t) BgL_iportz00_3180);
																		RGC_STOP_MATCH(BgL_tmpz00_7993,
																			BgL_forwardz00_5473);
																	}
																	BgL_newzd2matchzd2_5475 = 0L;
																	if (
																		(BgL_forwardz00_5473 == BgL_bufposz00_5474))
																		{	/* Ieee/input.scm 298 */
																			if (rgc_fill_buffer(
																					((obj_t) BgL_iportz00_3180)))
																				{	/* Ieee/input.scm 298 */
																					long BgL_arg2349z00_5478;
																					long BgL_arg2350z00_5479;

																					{	/* Ieee/input.scm 298 */
																						obj_t BgL_tmpz00_8001;

																						BgL_tmpz00_8001 =
																							((obj_t) BgL_iportz00_3180);
																						BgL_arg2349z00_5478 =
																							RGC_BUFFER_FORWARD
																							(BgL_tmpz00_8001);
																					}
																					{	/* Ieee/input.scm 298 */
																						obj_t BgL_tmpz00_8004;

																						BgL_tmpz00_8004 =
																							((obj_t) BgL_iportz00_3180);
																						BgL_arg2350z00_5479 =
																							RGC_BUFFER_BUFPOS
																							(BgL_tmpz00_8004);
																					}
																					{
																						long BgL_bufposz00_8008;
																						long BgL_forwardz00_8007;

																						BgL_forwardz00_8007 =
																							BgL_arg2349z00_5478;
																						BgL_bufposz00_8008 =
																							BgL_arg2350z00_5479;
																						BgL_bufposz00_5474 =
																							BgL_bufposz00_8008;
																						BgL_forwardz00_5473 =
																							BgL_forwardz00_8007;
																						goto BgL_statezd22zd21099z00_5471;
																					}
																				}
																			else
																				{	/* Ieee/input.scm 298 */
																					BgL_matchz00_3311 =
																						BgL_newzd2matchzd2_5475;
																				}
																		}
																	else
																		{	/* Ieee/input.scm 298 */
																			int BgL_curz00_5480;

																			{	/* Ieee/input.scm 298 */
																				obj_t BgL_tmpz00_8009;

																				BgL_tmpz00_8009 =
																					((obj_t) BgL_iportz00_3180);
																				BgL_curz00_5480 =
																					RGC_BUFFER_GET_CHAR(BgL_tmpz00_8009,
																					BgL_forwardz00_5473);
																			}
																			{	/* Ieee/input.scm 298 */

																				{
																					long BgL_forwardz00_8013;
																					long BgL_lastzd2matchzd2_8012;

																					BgL_lastzd2matchzd2_8012 =
																						BgL_newzd2matchzd2_5475;
																					BgL_forwardz00_8013 =
																						(1L + BgL_forwardz00_5473);
																					BgL_forwardz00_5473 =
																						BgL_forwardz00_8013;
																					BgL_lastzd2matchzd2_5472 =
																						BgL_lastzd2matchzd2_8012;
																					goto BgL_statezd22zd21099z00_5471;
																				}
																			}
																		}
																}
															}
														}
													}
												}
										}
									}
								}
						}
					}
					{	/* Ieee/input.scm 298 */
						obj_t BgL_tmpz00_8016;

						BgL_tmpz00_8016 = ((obj_t) BgL_ipz00_35);
						RGC_SET_FILEPOS(BgL_tmpz00_8016);
					}
					{

						switch (BgL_matchz00_3311)
							{
							case 1L:

								return BGl_string3453z00zz__r4_input_6_10_2z00;
								break;
							case 0L:

								{	/* Ieee/input.scm 298 */
									long BgL_arg2364z00_5518;

									{	/* Ieee/input.scm 298 */
										obj_t BgL_tmpz00_8019;

										BgL_tmpz00_8019 = ((obj_t) BgL_ipz00_35);
										BgL_arg2364z00_5518 =
											RGC_BUFFER_MATCH_LENGTH(BgL_tmpz00_8019);
									}
									return
										rgc_buffer_substring(
										((obj_t) BgL_ipz00_35), 0L, BgL_arg2364z00_5518);
								}
								break;
							default:
								return
									BGl_errorz00zz__errorz00
									(BGl_string3440z00zz__r4_input_6_10_2z00,
									BGl_string3441z00zz__r4_input_6_10_2z00,
									BINT(BgL_matchz00_3311));
							}
					}
				}
			}
		}

	}



/* _read-of-strings */
	obj_t BGl__readzd2ofzd2stringsz00zz__r4_input_6_10_2z00(obj_t
		BgL_env1280z00_40, obj_t BgL_opt1279z00_39)
	{
		{	/* Ieee/input.scm 320 */
			{	/* Ieee/input.scm 320 */

				switch (VECTOR_LENGTH(BgL_opt1279z00_39))
					{
					case 0L:

						{	/* Ieee/input.scm 320 */
							obj_t BgL_ipz00_3350;

							{	/* Ieee/input.scm 320 */
								obj_t BgL_tmpz00_8027;

								BgL_tmpz00_8027 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_ipz00_3350 = BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_8027);
							}
							{	/* Ieee/input.scm 320 */

								return
									BGl_z62zc3z04anonymousza31398ze3ze5zz__r4_input_6_10_2z00
									(BGl_za2readzd2ofzd2stringszd2grammarza2zd2zz__r4_input_6_10_2z00,
									BgL_ipz00_3350);
							}
						}
						break;
					case 1L:

						{	/* Ieee/input.scm 320 */
							obj_t BgL_ipz00_3351;

							BgL_ipz00_3351 = VECTOR_REF(BgL_opt1279z00_39, 0L);
							{	/* Ieee/input.scm 320 */

								return
									BGl_z62zc3z04anonymousza31398ze3ze5zz__r4_input_6_10_2z00
									(BGl_za2readzd2ofzd2stringszd2grammarza2zd2zz__r4_input_6_10_2z00,
									BgL_ipz00_3351);
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* read-of-strings */
	BGL_EXPORTED_DEF obj_t BGl_readzd2ofzd2stringsz00zz__r4_input_6_10_2z00(obj_t
		BgL_ipz00_38)
	{
		{	/* Ieee/input.scm 320 */
			return
				BGl_z62zc3z04anonymousza31398ze3ze5zz__r4_input_6_10_2z00
				(BGl_za2readzd2ofzd2stringszd2grammarza2zd2zz__r4_input_6_10_2z00,
				BgL_ipz00_38);
		}

	}



/* _read-chars */
	obj_t BGl__readzd2charszd2zz__r4_input_6_10_2z00(obj_t BgL_env1284z00_44,
		obj_t BgL_opt1283z00_43)
	{
		{	/* Ieee/input.scm 326 */
			{	/* Ieee/input.scm 326 */
				obj_t BgL_g1285z00_3352;

				BgL_g1285z00_3352 = VECTOR_REF(BgL_opt1283z00_43, 0L);
				switch (VECTOR_LENGTH(BgL_opt1283z00_43))
					{
					case 1L:

						{	/* Ieee/input.scm 326 */
							obj_t BgL_ipz00_3355;

							{	/* Ieee/input.scm 326 */
								obj_t BgL_tmpz00_8046;

								BgL_tmpz00_8046 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_ipz00_3355 = BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_8046);
							}
							{	/* Ieee/input.scm 326 */

								return
									BGl_readzd2charszd2zz__r4_input_6_10_2z00(BgL_g1285z00_3352,
									BgL_ipz00_3355);
							}
						}
						break;
					case 2L:

						{	/* Ieee/input.scm 326 */
							obj_t BgL_ipz00_3356;

							BgL_ipz00_3356 = VECTOR_REF(BgL_opt1283z00_43, 1L);
							{	/* Ieee/input.scm 326 */

								return
									BGl_readzd2charszd2zz__r4_input_6_10_2z00(BgL_g1285z00_3352,
									BgL_ipz00_3356);
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* read-chars */
	BGL_EXPORTED_DEF obj_t BGl_readzd2charszd2zz__r4_input_6_10_2z00(obj_t
		BgL_lz00_41, obj_t BgL_ipz00_42)
	{
		{	/* Ieee/input.scm 326 */
			{	/* Ieee/input.scm 327 */
				obj_t BgL_lenz00_3357;

				if (INTEGERP(BgL_lz00_41))
					{	/* Ieee/input.scm 328 */
						BgL_lenz00_3357 = BgL_lz00_41;
					}
				else
					{	/* Ieee/input.scm 328 */
						if (ELONGP(BgL_lz00_41))
							{	/* Ieee/input.scm 329 */
								long BgL_xz00_5524;

								BgL_xz00_5524 = BELONG_TO_LONG(BgL_lz00_41);
								BgL_lenz00_3357 = BINT((long) (BgL_xz00_5524));
							}
						else
							{	/* Ieee/input.scm 329 */
								if (LLONGP(BgL_lz00_41))
									{	/* Ieee/input.scm 330 */
										BGL_LONGLONG_T BgL_xz00_5525;

										BgL_xz00_5525 = BLLONG_TO_LLONG(BgL_lz00_41);
										BgL_lenz00_3357 = BINT(LLONG_TO_LONG(BgL_xz00_5525));
									}
								else
									{	/* Ieee/input.scm 332 */
										obj_t BgL_arg2466z00_3374;

										BgL_arg2466z00_3374 = bgl_find_runtime_type(BgL_lz00_41);
										BgL_lenz00_3357 =
											BGl_bigloozd2typezd2errorz00zz__errorz00
											(BGl_symbol3454z00zz__r4_input_6_10_2z00,
											BGl_string3456z00zz__r4_input_6_10_2z00,
											BgL_arg2466z00_3374);
									}
							}
					}
				if (((long) CINT(BgL_lenz00_3357) <= 0L))
					{	/* Ieee/input.scm 334 */
						if (((long) CINT(BgL_lenz00_3357) == 0L))
							{	/* Ieee/input.scm 335 */
								return BGl_string3453z00zz__r4_input_6_10_2z00;
							}
						else
							{	/* Ieee/input.scm 338 */
								BgL_z62iozd2errorzb0_bglt BgL_arg2456z00_3360;

								{	/* Ieee/input.scm 338 */
									BgL_z62iozd2errorzb0_bglt BgL_new1119z00_3361;

									{	/* Ieee/input.scm 338 */
										BgL_z62iozd2errorzb0_bglt BgL_new1118z00_3364;

										BgL_new1118z00_3364 =
											((BgL_z62iozd2errorzb0_bglt)
											BOBJECT(GC_MALLOC(sizeof(struct
														BgL_z62iozd2errorzb0_bgl))));
										{	/* Ieee/input.scm 338 */
											long BgL_arg2459z00_3365;

											BgL_arg2459z00_3365 =
												BGL_CLASS_NUM(BGl_z62iozd2errorzb0zz__objectz00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1118z00_3364),
												BgL_arg2459z00_3365);
										}
										BgL_new1119z00_3361 = BgL_new1118z00_3364;
									}
									((((BgL_z62exceptionz62_bglt) COBJECT(
													((BgL_z62exceptionz62_bglt) BgL_new1119z00_3361)))->
											BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
									((((BgL_z62exceptionz62_bglt)
												COBJECT(((BgL_z62exceptionz62_bglt)
														BgL_new1119z00_3361)))->BgL_locationz00) =
										((obj_t) BFALSE), BUNSPEC);
									{
										obj_t BgL_auxz00_8082;

										{	/* Ieee/input.scm 338 */
											obj_t BgL_arg2457z00_3362;

											{	/* Ieee/input.scm 338 */
												obj_t BgL_arg2458z00_3363;

												{	/* Ieee/input.scm 338 */
													obj_t BgL_classz00_5531;

													BgL_classz00_5531 = BGl_z62iozd2errorzb0zz__objectz00;
													BgL_arg2458z00_3363 =
														BGL_CLASS_ALL_FIELDS(BgL_classz00_5531);
												}
												BgL_arg2457z00_3362 =
													VECTOR_REF(BgL_arg2458z00_3363, 2L);
											}
											BgL_auxz00_8082 =
												BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
												(BgL_arg2457z00_3362);
										}
										((((BgL_z62exceptionz62_bglt) COBJECT(
														((BgL_z62exceptionz62_bglt) BgL_new1119z00_3361)))->
												BgL_stackz00) = ((obj_t) BgL_auxz00_8082), BUNSPEC);
									}
									((((BgL_z62errorz62_bglt) COBJECT(
													((BgL_z62errorz62_bglt) BgL_new1119z00_3361)))->
											BgL_procz00) =
										((obj_t) BGl_symbol3454z00zz__r4_input_6_10_2z00), BUNSPEC);
									((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
														BgL_new1119z00_3361)))->BgL_msgz00) =
										((obj_t) BGl_string3457z00zz__r4_input_6_10_2z00), BUNSPEC);
									((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
														BgL_new1119z00_3361)))->BgL_objz00) =
										((obj_t) BgL_lenz00_3357), BUNSPEC);
									BgL_arg2456z00_3360 = BgL_new1119z00_3361;
								}
								return BGl_raisez00zz__errorz00(((obj_t) BgL_arg2456z00_3360));
							}
					}
				else
					{	/* Ieee/input.scm 343 */
						obj_t BgL_sz00_3366;

						BgL_sz00_3366 = make_string_sans_fill((long) CINT(BgL_lenz00_3357));
						{	/* Ieee/input.scm 343 */
							long BgL_nz00_3367;

							BgL_nz00_3367 =
								bgl_rgc_blit_string(BgL_ipz00_42,
								BSTRING_TO_STRING(BgL_sz00_3366), 0L,
								(long) CINT(BgL_lenz00_3357));
							{	/* Ieee/input.scm 344 */

								if ((BgL_nz00_3367 == 0L))
									{	/* Ieee/input.scm 346 */
										if (rgc_buffer_eof_p(((obj_t) BgL_ipz00_42)))
											{	/* Ieee/input.scm 347 */
												return BEOF;
											}
										else
											{	/* Ieee/input.scm 347 */
												return BGl_string3453z00zz__r4_input_6_10_2z00;
											}
									}
								else
									{	/* Ieee/input.scm 346 */
										if ((BgL_nz00_3367 < (long) CINT(BgL_lenz00_3357)))
											{	/* Ieee/input.scm 350 */
												return bgl_string_shrink(BgL_sz00_3366, BgL_nz00_3367);
											}
										else
											{	/* Ieee/input.scm 350 */
												return BgL_sz00_3366;
											}
									}
							}
						}
					}
			}
		}

	}



/* _read-chars! */
	obj_t BGl__readzd2charsz12zc0zz__r4_input_6_10_2z00(obj_t BgL_env1289z00_49,
		obj_t BgL_opt1288z00_48)
	{
		{	/* Ieee/input.scm 358 */
			{	/* Ieee/input.scm 358 */
				obj_t BgL_g1290z00_3375;
				obj_t BgL_g1291z00_3376;

				BgL_g1290z00_3375 = VECTOR_REF(BgL_opt1288z00_48, 0L);
				BgL_g1291z00_3376 = VECTOR_REF(BgL_opt1288z00_48, 1L);
				switch (VECTOR_LENGTH(BgL_opt1288z00_48))
					{
					case 2L:

						{	/* Ieee/input.scm 358 */
							obj_t BgL_ipz00_3379;

							{	/* Ieee/input.scm 358 */
								obj_t BgL_tmpz00_8112;

								BgL_tmpz00_8112 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_ipz00_3379 = BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_8112);
							}
							{	/* Ieee/input.scm 358 */

								{	/* Ieee/input.scm 358 */
									obj_t BgL_auxz00_8115;

									if (STRINGP(BgL_g1290z00_3375))
										{	/* Ieee/input.scm 358 */
											BgL_auxz00_8115 = BgL_g1290z00_3375;
										}
									else
										{
											obj_t BgL_auxz00_8118;

											BgL_auxz00_8118 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3447z00zz__r4_input_6_10_2z00, BINT(13935L),
												BGl_string3458z00zz__r4_input_6_10_2z00,
												BGl_string3459z00zz__r4_input_6_10_2z00,
												BgL_g1290z00_3375);
											FAILURE(BgL_auxz00_8118, BFALSE, BFALSE);
										}
									return
										BGl_readzd2charsz12zc0zz__r4_input_6_10_2z00
										(BgL_auxz00_8115, BgL_g1291z00_3376, BgL_ipz00_3379);
								}
							}
						}
						break;
					case 3L:

						{	/* Ieee/input.scm 358 */
							obj_t BgL_ipz00_3380;

							BgL_ipz00_3380 = VECTOR_REF(BgL_opt1288z00_48, 2L);
							{	/* Ieee/input.scm 358 */

								{	/* Ieee/input.scm 358 */
									obj_t BgL_auxz00_8124;

									if (STRINGP(BgL_g1290z00_3375))
										{	/* Ieee/input.scm 358 */
											BgL_auxz00_8124 = BgL_g1290z00_3375;
										}
									else
										{
											obj_t BgL_auxz00_8127;

											BgL_auxz00_8127 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3447z00zz__r4_input_6_10_2z00, BINT(13935L),
												BGl_string3458z00zz__r4_input_6_10_2z00,
												BGl_string3459z00zz__r4_input_6_10_2z00,
												BgL_g1290z00_3375);
											FAILURE(BgL_auxz00_8127, BFALSE, BFALSE);
										}
									return
										BGl_readzd2charsz12zc0zz__r4_input_6_10_2z00
										(BgL_auxz00_8124, BgL_g1291z00_3376, BgL_ipz00_3380);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* read-chars! */
	BGL_EXPORTED_DEF obj_t BGl_readzd2charsz12zc0zz__r4_input_6_10_2z00(obj_t
		BgL_bufz00_45, obj_t BgL_lz00_46, obj_t BgL_ipz00_47)
	{
		{	/* Ieee/input.scm 358 */
			{	/* Ieee/input.scm 359 */
				obj_t BgL_lenz00_3381;

				if (INTEGERP(BgL_lz00_46))
					{	/* Ieee/input.scm 360 */
						BgL_lenz00_3381 = BgL_lz00_46;
					}
				else
					{	/* Ieee/input.scm 360 */
						if (ELONGP(BgL_lz00_46))
							{	/* Ieee/input.scm 361 */
								long BgL_xz00_5540;

								BgL_xz00_5540 = BELONG_TO_LONG(BgL_lz00_46);
								BgL_lenz00_3381 = BINT((long) (BgL_xz00_5540));
							}
						else
							{	/* Ieee/input.scm 361 */
								if (LLONGP(BgL_lz00_46))
									{	/* Ieee/input.scm 362 */
										BGL_LONGLONG_T BgL_xz00_5541;

										BgL_xz00_5541 = BLLONG_TO_LLONG(BgL_lz00_46);
										BgL_lenz00_3381 = BINT(LLONG_TO_LONG(BgL_xz00_5541));
									}
								else
									{	/* Ieee/input.scm 364 */
										obj_t BgL_arg2479z00_3397;

										BgL_arg2479z00_3397 = bgl_find_runtime_type(BgL_lz00_46);
										BgL_lenz00_3381 =
											BGl_bigloozd2typezd2errorz00zz__errorz00
											(BGl_symbol3460z00zz__r4_input_6_10_2z00,
											BGl_string3456z00zz__r4_input_6_10_2z00,
											BgL_arg2479z00_3397);
									}
							}
					}
				if (((long) CINT(BgL_lenz00_3381) <= 0L))
					{	/* Ieee/input.scm 365 */
						if (((long) CINT(BgL_lenz00_3381) == 0L))
							{	/* Ieee/input.scm 366 */
								return BINT(0L);
							}
						else
							{	/* Ieee/input.scm 369 */
								BgL_z62iozd2errorzb0_bglt BgL_arg2469z00_3384;

								{	/* Ieee/input.scm 369 */
									BgL_z62iozd2errorzb0_bglt BgL_new1123z00_3385;

									{	/* Ieee/input.scm 369 */
										BgL_z62iozd2errorzb0_bglt BgL_new1122z00_3388;

										BgL_new1122z00_3388 =
											((BgL_z62iozd2errorzb0_bglt)
											BOBJECT(GC_MALLOC(sizeof(struct
														BgL_z62iozd2errorzb0_bgl))));
										{	/* Ieee/input.scm 369 */
											long BgL_arg2473z00_3389;

											BgL_arg2473z00_3389 =
												BGL_CLASS_NUM(BGl_z62iozd2errorzb0zz__objectz00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1122z00_3388),
												BgL_arg2473z00_3389);
										}
										BgL_new1123z00_3385 = BgL_new1122z00_3388;
									}
									((((BgL_z62exceptionz62_bglt) COBJECT(
													((BgL_z62exceptionz62_bglt) BgL_new1123z00_3385)))->
											BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
									((((BgL_z62exceptionz62_bglt)
												COBJECT(((BgL_z62exceptionz62_bglt)
														BgL_new1123z00_3385)))->BgL_locationz00) =
										((obj_t) BFALSE), BUNSPEC);
									{
										obj_t BgL_auxz00_8163;

										{	/* Ieee/input.scm 369 */
											obj_t BgL_arg2470z00_3386;

											{	/* Ieee/input.scm 369 */
												obj_t BgL_arg2471z00_3387;

												{	/* Ieee/input.scm 369 */
													obj_t BgL_classz00_5547;

													BgL_classz00_5547 = BGl_z62iozd2errorzb0zz__objectz00;
													BgL_arg2471z00_3387 =
														BGL_CLASS_ALL_FIELDS(BgL_classz00_5547);
												}
												BgL_arg2470z00_3386 =
													VECTOR_REF(BgL_arg2471z00_3387, 2L);
											}
											BgL_auxz00_8163 =
												BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
												(BgL_arg2470z00_3386);
										}
										((((BgL_z62exceptionz62_bglt) COBJECT(
														((BgL_z62exceptionz62_bglt) BgL_new1123z00_3385)))->
												BgL_stackz00) = ((obj_t) BgL_auxz00_8163), BUNSPEC);
									}
									((((BgL_z62errorz62_bglt) COBJECT(
													((BgL_z62errorz62_bglt) BgL_new1123z00_3385)))->
											BgL_procz00) =
										((obj_t) BGl_symbol3454z00zz__r4_input_6_10_2z00), BUNSPEC);
									((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
														BgL_new1123z00_3385)))->BgL_msgz00) =
										((obj_t) BGl_string3457z00zz__r4_input_6_10_2z00), BUNSPEC);
									((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
														BgL_new1123z00_3385)))->BgL_objz00) =
										((obj_t) BgL_lenz00_3381), BUNSPEC);
									BgL_arg2469z00_3384 = BgL_new1123z00_3385;
								}
								return BGl_raisez00zz__errorz00(((obj_t) BgL_arg2469z00_3384));
							}
					}
				else
					{	/* Ieee/input.scm 373 */
						obj_t BgL_arg2474z00_3390;

						{	/* Ieee/input.scm 373 */
							long BgL_az00_3391;

							BgL_az00_3391 = STRING_LENGTH(BgL_bufz00_45);
							if ((BgL_az00_3391 < (long) CINT(BgL_lenz00_3381)))
								{	/* Ieee/input.scm 373 */
									BgL_arg2474z00_3390 = BINT(BgL_az00_3391);
								}
							else
								{	/* Ieee/input.scm 373 */
									BgL_arg2474z00_3390 = BgL_lenz00_3381;
								}
						}
						return
							BINT(bgl_rgc_blit_string(BgL_ipz00_47,
								BSTRING_TO_STRING(BgL_bufz00_45), 0L,
								(long) CINT(BgL_arg2474z00_3390)));
		}}}

	}



/* _read-fill-string! */
	obj_t BGl__readzd2fillzd2stringz12z12zz__r4_input_6_10_2z00(obj_t
		BgL_env1295z00_55, obj_t BgL_opt1294z00_54)
	{
		{	/* Ieee/input.scm 378 */
			{	/* Ieee/input.scm 378 */
				obj_t BgL_g1296z00_6522;
				obj_t BgL_g1297z00_6523;
				obj_t BgL_g1298z00_6524;

				BgL_g1296z00_6522 = VECTOR_REF(BgL_opt1294z00_54, 0L);
				BgL_g1297z00_6523 = VECTOR_REF(BgL_opt1294z00_54, 1L);
				BgL_g1298z00_6524 = VECTOR_REF(BgL_opt1294z00_54, 2L);
				switch (VECTOR_LENGTH(BgL_opt1294z00_54))
					{
					case 3L:

						{	/* Ieee/input.scm 378 */
							obj_t BgL_ipz00_6525;

							{	/* Ieee/input.scm 378 */
								obj_t BgL_tmpz00_8189;

								BgL_tmpz00_8189 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_ipz00_6525 = BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_8189);
							}
							{	/* Ieee/input.scm 378 */

								{	/* Ieee/input.scm 378 */
									obj_t BgL_sz00_6526;
									long BgL_oz00_6527;
									long BgL_lenz00_6528;

									if (STRINGP(BgL_g1296z00_6522))
										{	/* Ieee/input.scm 378 */
											BgL_sz00_6526 = BgL_g1296z00_6522;
										}
									else
										{
											obj_t BgL_auxz00_8194;

											BgL_auxz00_8194 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3447z00zz__r4_input_6_10_2z00, BINT(14652L),
												BGl_string3462z00zz__r4_input_6_10_2z00,
												BGl_string3459z00zz__r4_input_6_10_2z00,
												BgL_g1296z00_6522);
											FAILURE(BgL_auxz00_8194, BFALSE, BFALSE);
										}
									{	/* Ieee/input.scm 378 */
										obj_t BgL_tmpz00_8198;

										if (INTEGERP(BgL_g1297z00_6523))
											{	/* Ieee/input.scm 378 */
												BgL_tmpz00_8198 = BgL_g1297z00_6523;
											}
										else
											{
												obj_t BgL_auxz00_8201;

												BgL_auxz00_8201 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3447z00zz__r4_input_6_10_2z00,
													BINT(14652L), BGl_string3462z00zz__r4_input_6_10_2z00,
													BGl_string3463z00zz__r4_input_6_10_2z00,
													BgL_g1297z00_6523);
												FAILURE(BgL_auxz00_8201, BFALSE, BFALSE);
											}
										BgL_oz00_6527 = (long) CINT(BgL_tmpz00_8198);
									}
									{	/* Ieee/input.scm 378 */
										obj_t BgL_tmpz00_8206;

										if (INTEGERP(BgL_g1298z00_6524))
											{	/* Ieee/input.scm 378 */
												BgL_tmpz00_8206 = BgL_g1298z00_6524;
											}
										else
											{
												obj_t BgL_auxz00_8209;

												BgL_auxz00_8209 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3447z00zz__r4_input_6_10_2z00,
													BINT(14652L), BGl_string3462z00zz__r4_input_6_10_2z00,
													BGl_string3463z00zz__r4_input_6_10_2z00,
													BgL_g1298z00_6524);
												FAILURE(BgL_auxz00_8209, BFALSE, BFALSE);
											}
										BgL_lenz00_6528 = (long) CINT(BgL_tmpz00_8206);
									}
									{	/* Ieee/input.scm 382 */
										long BgL_nz00_6529;

										BgL_nz00_6529 =
											bgl_rgc_blit_string(BgL_ipz00_6525,
											BSTRING_TO_STRING(BgL_sz00_6526), BgL_oz00_6527,
											BgL_lenz00_6528);
										{	/* Ieee/input.scm 383 */
											bool_t BgL_test3741z00_8216;

											if ((BgL_nz00_6529 == 0L))
												{	/* Ieee/input.scm 383 */
													BgL_test3741z00_8216 =
														rgc_buffer_eof_p(BgL_ipz00_6525);
												}
											else
												{	/* Ieee/input.scm 383 */
													BgL_test3741z00_8216 = ((bool_t) 0);
												}
											if (BgL_test3741z00_8216)
												{	/* Ieee/input.scm 383 */
													return BEOF;
												}
											else
												{	/* Ieee/input.scm 383 */
													return BINT(BgL_nz00_6529);
												}
										}
									}
								}
							}
						}
						break;
					case 4L:

						{	/* Ieee/input.scm 378 */
							obj_t BgL_ipz00_6530;

							BgL_ipz00_6530 = VECTOR_REF(BgL_opt1294z00_54, 3L);
							{	/* Ieee/input.scm 378 */

								{	/* Ieee/input.scm 378 */
									obj_t BgL_sz00_6531;
									long BgL_oz00_6532;
									long BgL_lenz00_6533;

									if (STRINGP(BgL_g1296z00_6522))
										{	/* Ieee/input.scm 378 */
											BgL_sz00_6531 = BgL_g1296z00_6522;
										}
									else
										{
											obj_t BgL_auxz00_8224;

											BgL_auxz00_8224 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3447z00zz__r4_input_6_10_2z00, BINT(14652L),
												BGl_string3462z00zz__r4_input_6_10_2z00,
												BGl_string3459z00zz__r4_input_6_10_2z00,
												BgL_g1296z00_6522);
											FAILURE(BgL_auxz00_8224, BFALSE, BFALSE);
										}
									{	/* Ieee/input.scm 378 */
										obj_t BgL_tmpz00_8228;

										if (INTEGERP(BgL_g1297z00_6523))
											{	/* Ieee/input.scm 378 */
												BgL_tmpz00_8228 = BgL_g1297z00_6523;
											}
										else
											{
												obj_t BgL_auxz00_8231;

												BgL_auxz00_8231 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3447z00zz__r4_input_6_10_2z00,
													BINT(14652L), BGl_string3462z00zz__r4_input_6_10_2z00,
													BGl_string3463z00zz__r4_input_6_10_2z00,
													BgL_g1297z00_6523);
												FAILURE(BgL_auxz00_8231, BFALSE, BFALSE);
											}
										BgL_oz00_6532 = (long) CINT(BgL_tmpz00_8228);
									}
									{	/* Ieee/input.scm 378 */
										obj_t BgL_tmpz00_8236;

										if (INTEGERP(BgL_g1298z00_6524))
											{	/* Ieee/input.scm 378 */
												BgL_tmpz00_8236 = BgL_g1298z00_6524;
											}
										else
											{
												obj_t BgL_auxz00_8239;

												BgL_auxz00_8239 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3447z00zz__r4_input_6_10_2z00,
													BINT(14652L), BGl_string3462z00zz__r4_input_6_10_2z00,
													BGl_string3463z00zz__r4_input_6_10_2z00,
													BgL_g1298z00_6524);
												FAILURE(BgL_auxz00_8239, BFALSE, BFALSE);
											}
										BgL_lenz00_6533 = (long) CINT(BgL_tmpz00_8236);
									}
									{	/* Ieee/input.scm 382 */
										long BgL_nz00_6534;

										{	/* Ieee/input.scm 382 */
											obj_t BgL_tmpz00_8244;

											if (INPUT_PORTP(BgL_ipz00_6530))
												{	/* Ieee/input.scm 382 */
													BgL_tmpz00_8244 = BgL_ipz00_6530;
												}
											else
												{
													obj_t BgL_auxz00_8247;

													BgL_auxz00_8247 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string3447z00zz__r4_input_6_10_2z00,
														BINT(14866L),
														BGl_string3462z00zz__r4_input_6_10_2z00,
														BGl_string3450z00zz__r4_input_6_10_2z00,
														BgL_ipz00_6530);
													FAILURE(BgL_auxz00_8247, BFALSE, BFALSE);
												}
											BgL_nz00_6534 =
												bgl_rgc_blit_string(BgL_tmpz00_8244,
												BSTRING_TO_STRING(BgL_sz00_6531), BgL_oz00_6532,
												BgL_lenz00_6533);
										}
										{	/* Ieee/input.scm 383 */
											bool_t BgL_test3747z00_8253;

											if ((BgL_nz00_6534 == 0L))
												{	/* Ieee/input.scm 383 */
													obj_t BgL_tmpz00_8256;

													if (INPUT_PORTP(BgL_ipz00_6530))
														{	/* Ieee/input.scm 383 */
															BgL_tmpz00_8256 = BgL_ipz00_6530;
														}
													else
														{
															obj_t BgL_auxz00_8259;

															BgL_auxz00_8259 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string3447z00zz__r4_input_6_10_2z00,
																BINT(14920L),
																BGl_string3462z00zz__r4_input_6_10_2z00,
																BGl_string3450z00zz__r4_input_6_10_2z00,
																BgL_ipz00_6530);
															FAILURE(BgL_auxz00_8259, BFALSE, BFALSE);
														}
													BgL_test3747z00_8253 =
														rgc_buffer_eof_p(BgL_tmpz00_8256);
												}
											else
												{	/* Ieee/input.scm 383 */
													BgL_test3747z00_8253 = ((bool_t) 0);
												}
											if (BgL_test3747z00_8253)
												{	/* Ieee/input.scm 383 */
													return BEOF;
												}
											else
												{	/* Ieee/input.scm 383 */
													return BINT(BgL_nz00_6534);
												}
										}
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* read-fill-string! */
	BGL_EXPORTED_DEF obj_t
		BGl_readzd2fillzd2stringz12z12zz__r4_input_6_10_2z00(obj_t BgL_sz00_50,
		long BgL_oz00_51, long BgL_lenz00_52, obj_t BgL_ipz00_53)
	{
		{	/* Ieee/input.scm 378 */
			{	/* Ieee/input.scm 382 */
				long BgL_nz00_6535;

				BgL_nz00_6535 =
					bgl_rgc_blit_string(BgL_ipz00_53,
					BSTRING_TO_STRING(BgL_sz00_50), BgL_oz00_51, BgL_lenz00_52);
				{	/* Ieee/input.scm 383 */
					bool_t BgL_test3750z00_8269;

					if ((BgL_nz00_6535 == 0L))
						{	/* Ieee/input.scm 383 */
							BgL_test3750z00_8269 = rgc_buffer_eof_p(BgL_ipz00_53);
						}
					else
						{	/* Ieee/input.scm 383 */
							BgL_test3750z00_8269 = ((bool_t) 0);
						}
					if (BgL_test3750z00_8269)
						{	/* Ieee/input.scm 383 */
							return BEOF;
						}
					else
						{	/* Ieee/input.scm 383 */
							return BINT(BgL_nz00_6535);
						}
				}
			}
		}

	}



/* _unread-char! */
	obj_t BGl__unreadzd2charz12zc0zz__r4_input_6_10_2z00(obj_t BgL_env1302z00_59,
		obj_t BgL_opt1301z00_58)
	{
		{	/* Ieee/input.scm 401 */
			{	/* Ieee/input.scm 401 */
				obj_t BgL_g1303z00_3409;

				BgL_g1303z00_3409 = VECTOR_REF(BgL_opt1301z00_58, 0L);
				switch (VECTOR_LENGTH(BgL_opt1301z00_58))
					{
					case 1L:

						{	/* Ieee/input.scm 401 */
							obj_t BgL_ipz00_3412;

							{	/* Ieee/input.scm 401 */
								obj_t BgL_tmpz00_8275;

								BgL_tmpz00_8275 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_ipz00_3412 = BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_8275);
							}
							{	/* Ieee/input.scm 401 */

								{	/* Ieee/input.scm 401 */
									unsigned char BgL_auxz00_8278;

									{	/* Ieee/input.scm 401 */
										obj_t BgL_tmpz00_8279;

										if (CHARP(BgL_g1303z00_3409))
											{	/* Ieee/input.scm 401 */
												BgL_tmpz00_8279 = BgL_g1303z00_3409;
											}
										else
											{
												obj_t BgL_auxz00_8282;

												BgL_auxz00_8282 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3447z00zz__r4_input_6_10_2z00,
													BINT(15535L), BGl_string3464z00zz__r4_input_6_10_2z00,
													BGl_string3465z00zz__r4_input_6_10_2z00,
													BgL_g1303z00_3409);
												FAILURE(BgL_auxz00_8282, BFALSE, BFALSE);
											}
										BgL_auxz00_8278 = CCHAR(BgL_tmpz00_8279);
									}
									return
										BGl_unreadzd2charz12zc0zz__r4_input_6_10_2z00
										(BgL_auxz00_8278, BgL_ipz00_3412);
								}
							}
						}
						break;
					case 2L:

						{	/* Ieee/input.scm 401 */
							obj_t BgL_ipz00_3413;

							BgL_ipz00_3413 = VECTOR_REF(BgL_opt1301z00_58, 1L);
							{	/* Ieee/input.scm 401 */

								{	/* Ieee/input.scm 401 */
									unsigned char BgL_auxz00_8289;

									{	/* Ieee/input.scm 401 */
										obj_t BgL_tmpz00_8290;

										if (CHARP(BgL_g1303z00_3409))
											{	/* Ieee/input.scm 401 */
												BgL_tmpz00_8290 = BgL_g1303z00_3409;
											}
										else
											{
												obj_t BgL_auxz00_8293;

												BgL_auxz00_8293 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3447z00zz__r4_input_6_10_2z00,
													BINT(15535L), BGl_string3464z00zz__r4_input_6_10_2z00,
													BGl_string3465z00zz__r4_input_6_10_2z00,
													BgL_g1303z00_3409);
												FAILURE(BgL_auxz00_8293, BFALSE, BFALSE);
											}
										BgL_auxz00_8289 = CCHAR(BgL_tmpz00_8290);
									}
									return
										BGl_unreadzd2charz12zc0zz__r4_input_6_10_2z00
										(BgL_auxz00_8289, BgL_ipz00_3413);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* unread-char! */
	BGL_EXPORTED_DEF obj_t BGl_unreadzd2charz12zc0zz__r4_input_6_10_2z00(unsigned
		char BgL_cz00_56, obj_t BgL_ipz00_57)
	{
		{	/* Ieee/input.scm 401 */
			{	/* Ieee/input.scm 402 */
				bool_t BgL_test3754z00_8301;

				{	/* Ieee/input.scm 402 */
					long BgL_arg2490z00_3422;

					BgL_arg2490z00_3422 = ((unsigned char) (BgL_cz00_56));
					BgL_test3754z00_8301 =
						rgc_buffer_insert_char(
						((obj_t) BgL_ipz00_57), (int) (BgL_arg2490z00_3422));
				}
				if (BgL_test3754z00_8301)
					{	/* Ieee/input.scm 402 */
						return BFALSE;
					}
				else
					{	/* Ieee/input.scm 404 */
						BgL_z62iozd2errorzb0_bglt BgL_arg2484z00_3416;

						{	/* Ieee/input.scm 404 */
							BgL_z62iozd2errorzb0_bglt BgL_new1129z00_3417;

							{	/* Ieee/input.scm 404 */
								BgL_z62iozd2errorzb0_bglt BgL_new1127z00_3420;

								BgL_new1127z00_3420 =
									((BgL_z62iozd2errorzb0_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_z62iozd2errorzb0_bgl))));
								{	/* Ieee/input.scm 404 */
									long BgL_arg2488z00_3421;

									BgL_arg2488z00_3421 =
										BGL_CLASS_NUM(BGl_z62iozd2errorzb0zz__objectz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1127z00_3420),
										BgL_arg2488z00_3421);
								}
								BgL_new1129z00_3417 = BgL_new1127z00_3420;
							}
							((((BgL_z62exceptionz62_bglt) COBJECT(
											((BgL_z62exceptionz62_bglt) BgL_new1129z00_3417)))->
									BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
							((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
												BgL_new1129z00_3417)))->BgL_locationz00) =
								((obj_t) BFALSE), BUNSPEC);
							{
								obj_t BgL_auxz00_8315;

								{	/* Ieee/input.scm 404 */
									obj_t BgL_arg2486z00_3418;

									{	/* Ieee/input.scm 404 */
										obj_t BgL_arg2487z00_3419;

										{	/* Ieee/input.scm 404 */
											obj_t BgL_classz00_5585;

											BgL_classz00_5585 = BGl_z62iozd2errorzb0zz__objectz00;
											BgL_arg2487z00_3419 =
												BGL_CLASS_ALL_FIELDS(BgL_classz00_5585);
										}
										BgL_arg2486z00_3418 = VECTOR_REF(BgL_arg2487z00_3419, 2L);
									}
									BgL_auxz00_8315 =
										BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
										(BgL_arg2486z00_3418);
								}
								((((BgL_z62exceptionz62_bglt) COBJECT(
												((BgL_z62exceptionz62_bglt) BgL_new1129z00_3417)))->
										BgL_stackz00) = ((obj_t) BgL_auxz00_8315), BUNSPEC);
							}
							((((BgL_z62errorz62_bglt) COBJECT(
											((BgL_z62errorz62_bglt) BgL_new1129z00_3417)))->
									BgL_procz00) =
								((obj_t) BGl_symbol3466z00zz__r4_input_6_10_2z00), BUNSPEC);
							((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
												BgL_new1129z00_3417)))->BgL_msgz00) =
								((obj_t) BGl_string3468z00zz__r4_input_6_10_2z00), BUNSPEC);
							((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
												BgL_new1129z00_3417)))->BgL_objz00) =
								((obj_t) BCHAR(BgL_cz00_56)), BUNSPEC);
							BgL_arg2484z00_3416 = BgL_new1129z00_3417;
						}
						return BGl_raisez00zz__errorz00(((obj_t) BgL_arg2484z00_3416));
					}
			}
		}

	}



/* _unread-string! */
	obj_t BGl__unreadzd2stringz12zc0zz__r4_input_6_10_2z00(obj_t
		BgL_env1307z00_63, obj_t BgL_opt1306z00_62)
	{
		{	/* Ieee/input.scm 412 */
			{	/* Ieee/input.scm 412 */
				obj_t BgL_g1308z00_3423;

				BgL_g1308z00_3423 = VECTOR_REF(BgL_opt1306z00_62, 0L);
				switch (VECTOR_LENGTH(BgL_opt1306z00_62))
					{
					case 1L:

						{	/* Ieee/input.scm 412 */
							obj_t BgL_ipz00_3426;

							{	/* Ieee/input.scm 412 */
								obj_t BgL_tmpz00_8331;

								BgL_tmpz00_8331 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_ipz00_3426 = BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_8331);
							}
							{	/* Ieee/input.scm 412 */

								{	/* Ieee/input.scm 412 */
									obj_t BgL_auxz00_8334;

									if (STRINGP(BgL_g1308z00_3423))
										{	/* Ieee/input.scm 412 */
											BgL_auxz00_8334 = BgL_g1308z00_3423;
										}
									else
										{
											obj_t BgL_auxz00_8337;

											BgL_auxz00_8337 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3447z00zz__r4_input_6_10_2z00, BINT(15998L),
												BGl_string3469z00zz__r4_input_6_10_2z00,
												BGl_string3459z00zz__r4_input_6_10_2z00,
												BgL_g1308z00_3423);
											FAILURE(BgL_auxz00_8337, BFALSE, BFALSE);
										}
									return
										BGl_unreadzd2stringz12zc0zz__r4_input_6_10_2z00
										(BgL_auxz00_8334, BgL_ipz00_3426);
								}
							}
						}
						break;
					case 2L:

						{	/* Ieee/input.scm 412 */
							obj_t BgL_ipz00_3427;

							BgL_ipz00_3427 = VECTOR_REF(BgL_opt1306z00_62, 1L);
							{	/* Ieee/input.scm 412 */

								{	/* Ieee/input.scm 412 */
									obj_t BgL_auxz00_8343;

									if (STRINGP(BgL_g1308z00_3423))
										{	/* Ieee/input.scm 412 */
											BgL_auxz00_8343 = BgL_g1308z00_3423;
										}
									else
										{
											obj_t BgL_auxz00_8346;

											BgL_auxz00_8346 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3447z00zz__r4_input_6_10_2z00, BINT(15998L),
												BGl_string3469z00zz__r4_input_6_10_2z00,
												BGl_string3459z00zz__r4_input_6_10_2z00,
												BgL_g1308z00_3423);
											FAILURE(BgL_auxz00_8346, BFALSE, BFALSE);
										}
									return
										BGl_unreadzd2stringz12zc0zz__r4_input_6_10_2z00
										(BgL_auxz00_8343, BgL_ipz00_3427);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* unread-string! */
	BGL_EXPORTED_DEF obj_t BGl_unreadzd2stringz12zc0zz__r4_input_6_10_2z00(obj_t
		BgL_strz00_60, obj_t BgL_ipz00_61)
	{
		{	/* Ieee/input.scm 412 */
			{	/* Ieee/input.scm 413 */
				bool_t BgL_test3757z00_8353;

				{	/* Ieee/input.scm 413 */
					long BgL_arg2501z00_3436;

					BgL_arg2501z00_3436 = STRING_LENGTH(BgL_strz00_60);
					BgL_test3757z00_8353 =
						rgc_buffer_insert_substring(
						((obj_t) BgL_ipz00_61), BgL_strz00_60, 0L, BgL_arg2501z00_3436);
				}
				if (BgL_test3757z00_8353)
					{	/* Ieee/input.scm 413 */
						return BFALSE;
					}
				else
					{	/* Ieee/input.scm 415 */
						BgL_z62iozd2errorzb0_bglt BgL_arg2493z00_3430;

						{	/* Ieee/input.scm 415 */
							BgL_z62iozd2errorzb0_bglt BgL_new1133z00_3431;

							{	/* Ieee/input.scm 415 */
								BgL_z62iozd2errorzb0_bglt BgL_new1131z00_3434;

								BgL_new1131z00_3434 =
									((BgL_z62iozd2errorzb0_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_z62iozd2errorzb0_bgl))));
								{	/* Ieee/input.scm 415 */
									long BgL_arg2500z00_3435;

									BgL_arg2500z00_3435 =
										BGL_CLASS_NUM(BGl_z62iozd2errorzb0zz__objectz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1131z00_3434),
										BgL_arg2500z00_3435);
								}
								BgL_new1133z00_3431 = BgL_new1131z00_3434;
							}
							((((BgL_z62exceptionz62_bglt) COBJECT(
											((BgL_z62exceptionz62_bglt) BgL_new1133z00_3431)))->
									BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
							((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
												BgL_new1133z00_3431)))->BgL_locationz00) =
								((obj_t) BFALSE), BUNSPEC);
							{
								obj_t BgL_auxz00_8365;

								{	/* Ieee/input.scm 415 */
									obj_t BgL_arg2495z00_3432;

									{	/* Ieee/input.scm 415 */
										obj_t BgL_arg2497z00_3433;

										{	/* Ieee/input.scm 415 */
											obj_t BgL_classz00_5595;

											BgL_classz00_5595 = BGl_z62iozd2errorzb0zz__objectz00;
											BgL_arg2497z00_3433 =
												BGL_CLASS_ALL_FIELDS(BgL_classz00_5595);
										}
										BgL_arg2495z00_3432 = VECTOR_REF(BgL_arg2497z00_3433, 2L);
									}
									BgL_auxz00_8365 =
										BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
										(BgL_arg2495z00_3432);
								}
								((((BgL_z62exceptionz62_bglt) COBJECT(
												((BgL_z62exceptionz62_bglt) BgL_new1133z00_3431)))->
										BgL_stackz00) = ((obj_t) BgL_auxz00_8365), BUNSPEC);
							}
							((((BgL_z62errorz62_bglt) COBJECT(
											((BgL_z62errorz62_bglt) BgL_new1133z00_3431)))->
									BgL_procz00) =
								((obj_t) BGl_symbol3470z00zz__r4_input_6_10_2z00), BUNSPEC);
							((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
												BgL_new1133z00_3431)))->BgL_msgz00) =
								((obj_t) BGl_string3472z00zz__r4_input_6_10_2z00), BUNSPEC);
							((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
												BgL_new1133z00_3431)))->BgL_objz00) =
								((obj_t) BgL_strz00_60), BUNSPEC);
							BgL_arg2493z00_3430 = BgL_new1133z00_3431;
						}
						return BGl_raisez00zz__errorz00(((obj_t) BgL_arg2493z00_3430));
					}
			}
		}

	}



/* _unread-substring! */
	obj_t BGl__unreadzd2substringz12zc0zz__r4_input_6_10_2z00(obj_t
		BgL_env1312z00_69, obj_t BgL_opt1311z00_68)
	{
		{	/* Ieee/input.scm 423 */
			{	/* Ieee/input.scm 423 */
				obj_t BgL_g1313z00_3437;
				obj_t BgL_g1314z00_3438;
				obj_t BgL_g1315z00_3439;

				BgL_g1313z00_3437 = VECTOR_REF(BgL_opt1311z00_68, 0L);
				BgL_g1314z00_3438 = VECTOR_REF(BgL_opt1311z00_68, 1L);
				BgL_g1315z00_3439 = VECTOR_REF(BgL_opt1311z00_68, 2L);
				switch (VECTOR_LENGTH(BgL_opt1311z00_68))
					{
					case 3L:

						{	/* Ieee/input.scm 424 */
							obj_t BgL_ipz00_3442;

							{	/* Ieee/input.scm 424 */
								obj_t BgL_tmpz00_8382;

								BgL_tmpz00_8382 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_ipz00_3442 = BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_8382);
							}
							{	/* Ieee/input.scm 423 */

								{	/* Ieee/input.scm 423 */
									long BgL_auxz00_8401;
									long BgL_auxz00_8392;
									obj_t BgL_auxz00_8385;

									{	/* Ieee/input.scm 423 */
										obj_t BgL_tmpz00_8402;

										if (INTEGERP(BgL_g1315z00_3439))
											{	/* Ieee/input.scm 423 */
												BgL_tmpz00_8402 = BgL_g1315z00_3439;
											}
										else
											{
												obj_t BgL_auxz00_8405;

												BgL_auxz00_8405 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3447z00zz__r4_input_6_10_2z00,
													BINT(16490L), BGl_string3473z00zz__r4_input_6_10_2z00,
													BGl_string3463z00zz__r4_input_6_10_2z00,
													BgL_g1315z00_3439);
												FAILURE(BgL_auxz00_8405, BFALSE, BFALSE);
											}
										BgL_auxz00_8401 = (long) CINT(BgL_tmpz00_8402);
									}
									{	/* Ieee/input.scm 423 */
										obj_t BgL_tmpz00_8393;

										if (INTEGERP(BgL_g1314z00_3438))
											{	/* Ieee/input.scm 423 */
												BgL_tmpz00_8393 = BgL_g1314z00_3438;
											}
										else
											{
												obj_t BgL_auxz00_8396;

												BgL_auxz00_8396 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3447z00zz__r4_input_6_10_2z00,
													BINT(16490L), BGl_string3473z00zz__r4_input_6_10_2z00,
													BGl_string3463z00zz__r4_input_6_10_2z00,
													BgL_g1314z00_3438);
												FAILURE(BgL_auxz00_8396, BFALSE, BFALSE);
											}
										BgL_auxz00_8392 = (long) CINT(BgL_tmpz00_8393);
									}
									if (STRINGP(BgL_g1313z00_3437))
										{	/* Ieee/input.scm 423 */
											BgL_auxz00_8385 = BgL_g1313z00_3437;
										}
									else
										{
											obj_t BgL_auxz00_8388;

											BgL_auxz00_8388 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3447z00zz__r4_input_6_10_2z00, BINT(16490L),
												BGl_string3473z00zz__r4_input_6_10_2z00,
												BGl_string3459z00zz__r4_input_6_10_2z00,
												BgL_g1313z00_3437);
											FAILURE(BgL_auxz00_8388, BFALSE, BFALSE);
										}
									return
										BGl_unreadzd2substringz12zc0zz__r4_input_6_10_2z00
										(BgL_auxz00_8385, BgL_auxz00_8392, BgL_auxz00_8401,
										BgL_ipz00_3442);
								}
							}
						}
						break;
					case 4L:

						{	/* Ieee/input.scm 423 */
							obj_t BgL_ipz00_3443;

							BgL_ipz00_3443 = VECTOR_REF(BgL_opt1311z00_68, 3L);
							{	/* Ieee/input.scm 423 */

								{	/* Ieee/input.scm 423 */
									long BgL_auxz00_8428;
									long BgL_auxz00_8419;
									obj_t BgL_auxz00_8412;

									{	/* Ieee/input.scm 423 */
										obj_t BgL_tmpz00_8429;

										if (INTEGERP(BgL_g1315z00_3439))
											{	/* Ieee/input.scm 423 */
												BgL_tmpz00_8429 = BgL_g1315z00_3439;
											}
										else
											{
												obj_t BgL_auxz00_8432;

												BgL_auxz00_8432 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3447z00zz__r4_input_6_10_2z00,
													BINT(16490L), BGl_string3473z00zz__r4_input_6_10_2z00,
													BGl_string3463z00zz__r4_input_6_10_2z00,
													BgL_g1315z00_3439);
												FAILURE(BgL_auxz00_8432, BFALSE, BFALSE);
											}
										BgL_auxz00_8428 = (long) CINT(BgL_tmpz00_8429);
									}
									{	/* Ieee/input.scm 423 */
										obj_t BgL_tmpz00_8420;

										if (INTEGERP(BgL_g1314z00_3438))
											{	/* Ieee/input.scm 423 */
												BgL_tmpz00_8420 = BgL_g1314z00_3438;
											}
										else
											{
												obj_t BgL_auxz00_8423;

												BgL_auxz00_8423 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3447z00zz__r4_input_6_10_2z00,
													BINT(16490L), BGl_string3473z00zz__r4_input_6_10_2z00,
													BGl_string3463z00zz__r4_input_6_10_2z00,
													BgL_g1314z00_3438);
												FAILURE(BgL_auxz00_8423, BFALSE, BFALSE);
											}
										BgL_auxz00_8419 = (long) CINT(BgL_tmpz00_8420);
									}
									if (STRINGP(BgL_g1313z00_3437))
										{	/* Ieee/input.scm 423 */
											BgL_auxz00_8412 = BgL_g1313z00_3437;
										}
									else
										{
											obj_t BgL_auxz00_8415;

											BgL_auxz00_8415 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3447z00zz__r4_input_6_10_2z00, BINT(16490L),
												BGl_string3473z00zz__r4_input_6_10_2z00,
												BGl_string3459z00zz__r4_input_6_10_2z00,
												BgL_g1313z00_3437);
											FAILURE(BgL_auxz00_8415, BFALSE, BFALSE);
										}
									return
										BGl_unreadzd2substringz12zc0zz__r4_input_6_10_2z00
										(BgL_auxz00_8412, BgL_auxz00_8419, BgL_auxz00_8428,
										BgL_ipz00_3443);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* unread-substring! */
	BGL_EXPORTED_DEF obj_t
		BGl_unreadzd2substringz12zc0zz__r4_input_6_10_2z00(obj_t BgL_strz00_64,
		long BgL_fromz00_65, long BgL_toz00_66, obj_t BgL_ipz00_67)
	{
		{	/* Ieee/input.scm 423 */
			{	/* Ieee/input.scm 425 */
				bool_t BgL_test3764z00_8440;

				if ((BgL_toz00_66 >= BgL_fromz00_65))
					{	/* Ieee/input.scm 425 */
						if ((BgL_fromz00_65 < 0L))
							{	/* Ieee/input.scm 426 */
								BgL_test3764z00_8440 = ((bool_t) 1);
							}
						else
							{	/* Ieee/input.scm 426 */
								BgL_test3764z00_8440 =
									(BgL_toz00_66 > STRING_LENGTH(BgL_strz00_64));
							}
					}
				else
					{	/* Ieee/input.scm 425 */
						BgL_test3764z00_8440 = ((bool_t) 1);
					}
				if (BgL_test3764z00_8440)
					{	/* Ieee/input.scm 428 */
						BgL_z62iozd2errorzb0_bglt BgL_arg2508z00_3448;

						{	/* Ieee/input.scm 428 */
							BgL_z62iozd2errorzb0_bglt BgL_new1136z00_3449;

							{	/* Ieee/input.scm 428 */
								BgL_z62iozd2errorzb0_bglt BgL_new1135z00_3456;

								BgL_new1135z00_3456 =
									((BgL_z62iozd2errorzb0_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_z62iozd2errorzb0_bgl))));
								{	/* Ieee/input.scm 428 */
									long BgL_arg2515z00_3457;

									BgL_arg2515z00_3457 =
										BGL_CLASS_NUM(BGl_z62iozd2errorzb0zz__objectz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1135z00_3456),
										BgL_arg2515z00_3457);
								}
								BgL_new1136z00_3449 = BgL_new1135z00_3456;
							}
							((((BgL_z62exceptionz62_bglt) COBJECT(
											((BgL_z62exceptionz62_bglt) BgL_new1136z00_3449)))->
									BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
							((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
												BgL_new1136z00_3449)))->BgL_locationz00) =
								((obj_t) BFALSE), BUNSPEC);
							{
								obj_t BgL_auxz00_8455;

								{	/* Ieee/input.scm 428 */
									obj_t BgL_arg2509z00_3450;

									{	/* Ieee/input.scm 428 */
										obj_t BgL_arg2510z00_3451;

										{	/* Ieee/input.scm 428 */
											obj_t BgL_classz00_5607;

											BgL_classz00_5607 = BGl_z62iozd2errorzb0zz__objectz00;
											BgL_arg2510z00_3451 =
												BGL_CLASS_ALL_FIELDS(BgL_classz00_5607);
										}
										BgL_arg2509z00_3450 = VECTOR_REF(BgL_arg2510z00_3451, 2L);
									}
									BgL_auxz00_8455 =
										BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
										(BgL_arg2509z00_3450);
								}
								((((BgL_z62exceptionz62_bglt) COBJECT(
												((BgL_z62exceptionz62_bglt) BgL_new1136z00_3449)))->
										BgL_stackz00) = ((obj_t) BgL_auxz00_8455), BUNSPEC);
							}
							((((BgL_z62errorz62_bglt) COBJECT(
											((BgL_z62errorz62_bglt) BgL_new1136z00_3449)))->
									BgL_procz00) =
								((obj_t) BGl_symbol3474z00zz__r4_input_6_10_2z00), BUNSPEC);
							((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
												BgL_new1136z00_3449)))->BgL_msgz00) =
								((obj_t) BGl_string3476z00zz__r4_input_6_10_2z00), BUNSPEC);
							{
								obj_t BgL_auxz00_8465;

								{	/* Ieee/input.scm 431 */
									long BgL_arg2511z00_3452;

									BgL_arg2511z00_3452 = STRING_LENGTH(BgL_strz00_64);
									{	/* Ieee/input.scm 431 */
										obj_t BgL_list2512z00_3453;

										{	/* Ieee/input.scm 431 */
											obj_t BgL_arg2513z00_3454;

											{	/* Ieee/input.scm 431 */
												obj_t BgL_arg2514z00_3455;

												BgL_arg2514z00_3455 =
													MAKE_YOUNG_PAIR(BINT(BgL_arg2511z00_3452), BNIL);
												BgL_arg2513z00_3454 =
													MAKE_YOUNG_PAIR(BINT(BgL_toz00_66),
													BgL_arg2514z00_3455);
											}
											BgL_list2512z00_3453 =
												MAKE_YOUNG_PAIR(BINT(BgL_fromz00_65),
												BgL_arg2513z00_3454);
										}
										BgL_auxz00_8465 = BgL_list2512z00_3453;
								}}
								((((BgL_z62errorz62_bglt) COBJECT(
												((BgL_z62errorz62_bglt) BgL_new1136z00_3449)))->
										BgL_objz00) = ((obj_t) BgL_auxz00_8465), BUNSPEC);
							}
							BgL_arg2508z00_3448 = BgL_new1136z00_3449;
						}
						BGl_raisez00zz__errorz00(((obj_t) BgL_arg2508z00_3448));
					}
				else
					{	/* Ieee/input.scm 425 */
						BFALSE;
					}
			}
			if (rgc_buffer_insert_substring(
					((obj_t) BgL_ipz00_67), BgL_strz00_64, BgL_fromz00_65, BgL_toz00_66))
				{	/* Ieee/input.scm 432 */
					return BFALSE;
				}
			else
				{	/* Ieee/input.scm 433 */
					BgL_z62iozd2errorzb0_bglt BgL_arg2518z00_3462;

					{	/* Ieee/input.scm 433 */
						BgL_z62iozd2errorzb0_bglt BgL_new1138z00_3463;

						{	/* Ieee/input.scm 433 */
							BgL_z62iozd2errorzb0_bglt BgL_new1137z00_3466;

							BgL_new1137z00_3466 =
								((BgL_z62iozd2errorzb0_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_z62iozd2errorzb0_bgl))));
							{	/* Ieee/input.scm 433 */
								long BgL_arg2524z00_3467;

								BgL_arg2524z00_3467 =
									BGL_CLASS_NUM(BGl_z62iozd2errorzb0zz__objectz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1137z00_3466),
									BgL_arg2524z00_3467);
							}
							BgL_new1138z00_3463 = BgL_new1137z00_3466;
						}
						((((BgL_z62exceptionz62_bglt) COBJECT(
										((BgL_z62exceptionz62_bglt) BgL_new1138z00_3463)))->
								BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
						((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
											BgL_new1138z00_3463)))->BgL_locationz00) =
							((obj_t) BFALSE), BUNSPEC);
						{
							obj_t BgL_auxz00_8488;

							{	/* Ieee/input.scm 433 */
								obj_t BgL_arg2519z00_3464;

								{	/* Ieee/input.scm 433 */
									obj_t BgL_arg2521z00_3465;

									{	/* Ieee/input.scm 433 */
										obj_t BgL_classz00_5618;

										BgL_classz00_5618 = BGl_z62iozd2errorzb0zz__objectz00;
										BgL_arg2521z00_3465 =
											BGL_CLASS_ALL_FIELDS(BgL_classz00_5618);
									}
									BgL_arg2519z00_3464 = VECTOR_REF(BgL_arg2521z00_3465, 2L);
								}
								BgL_auxz00_8488 =
									BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
									(BgL_arg2519z00_3464);
							}
							((((BgL_z62exceptionz62_bglt) COBJECT(
											((BgL_z62exceptionz62_bglt) BgL_new1138z00_3463)))->
									BgL_stackz00) = ((obj_t) BgL_auxz00_8488), BUNSPEC);
						}
						((((BgL_z62errorz62_bglt) COBJECT(
										((BgL_z62errorz62_bglt) BgL_new1138z00_3463)))->
								BgL_procz00) =
							((obj_t) BGl_symbol3477z00zz__r4_input_6_10_2z00), BUNSPEC);
						((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
											BgL_new1138z00_3463)))->BgL_msgz00) =
							((obj_t) BGl_string3472z00zz__r4_input_6_10_2z00), BUNSPEC);
						((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
											BgL_new1138z00_3463)))->BgL_objz00) =
							((obj_t) BgL_strz00_64), BUNSPEC);
						BgL_arg2518z00_3462 = BgL_new1138z00_3463;
					}
					return BGl_raisez00zz__errorz00(((obj_t) BgL_arg2518z00_3462));
				}
		}

	}



/* port->string-list */
	BGL_EXPORTED_DEF obj_t
		BGl_portzd2ze3stringzd2listze3zz__r4_input_6_10_2z00(obj_t BgL_ipz00_70)
	{
		{	/* Ieee/input.scm 441 */
			{
				obj_t BgL_resz00_5627;

				BgL_resz00_5627 = BNIL;
			BgL_loopz00_5626:
				{	/* Ieee/input.scm 443 */
					obj_t BgL_expz00_5631;

					BgL_expz00_5631 =
						BGl_z62zc3z04anonymousza31398ze3ze5zz__r4_input_6_10_2z00
						(BGl_za2readzd2ofzd2stringszd2grammarza2zd2zz__r4_input_6_10_2z00,
						BgL_ipz00_70);
					if (EOF_OBJECTP(BgL_expz00_5631))
						{	/* Ieee/input.scm 444 */
							return bgl_reverse_bang(BgL_resz00_5627);
						}
					else
						{	/* Ieee/input.scm 446 */
							obj_t BgL_arg2527z00_5633;

							BgL_arg2527z00_5633 =
								MAKE_YOUNG_PAIR(BgL_expz00_5631, BgL_resz00_5627);
							{
								obj_t BgL_resz00_8510;

								BgL_resz00_8510 = BgL_arg2527z00_5633;
								BgL_resz00_5627 = BgL_resz00_8510;
								goto BgL_loopz00_5626;
							}
						}
				}
			}
		}

	}



/* &port->string-list */
	obj_t BGl_z62portzd2ze3stringzd2listz81zz__r4_input_6_10_2z00(obj_t
		BgL_envz00_6258, obj_t BgL_ipz00_6259)
	{
		{	/* Ieee/input.scm 441 */
			{	/* Ieee/input.scm 442 */
				obj_t BgL_auxz00_8511;

				if (INPUT_PORTP(BgL_ipz00_6259))
					{	/* Ieee/input.scm 442 */
						BgL_auxz00_8511 = BgL_ipz00_6259;
					}
				else
					{
						obj_t BgL_auxz00_8514;

						BgL_auxz00_8514 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string3447z00zz__r4_input_6_10_2z00, BINT(17251L),
							BGl_string3479z00zz__r4_input_6_10_2z00,
							BGl_string3450z00zz__r4_input_6_10_2z00, BgL_ipz00_6259);
						FAILURE(BgL_auxz00_8514, BFALSE, BFALSE);
					}
				return
					BGl_portzd2ze3stringzd2listze3zz__r4_input_6_10_2z00(BgL_auxz00_8511);
			}
		}

	}



/* %sendchars */
	int BGl_z52sendcharsz52zz__r4_input_6_10_2z00(obj_t BgL_ipz00_71,
		obj_t BgL_opz00_72, long BgL_sza7za7_73, long BgL_offsetz00_74)
	{
		{	/* Ieee/input.scm 455 */
			if ((BgL_offsetz00_74 >= 0L))
				{	/* Ieee/input.scm 456 */
					BGl_setzd2inputzd2portzd2positionz12zc0zz__r4_ports_6_10_1z00
						(BgL_ipz00_71, BgL_offsetz00_74);
				}
			else
				{	/* Ieee/input.scm 456 */
					BFALSE;
				}
			{	/* Ieee/input.scm 457 */
				long BgL_bufsiza7eza7_3477;

				if ((BgL_sza7za7_73 == -1L))
					{	/* Ieee/input.scm 458 */
						BgL_bufsiza7eza7_3477 = BGL_INPUT_PORT_BUFSIZ(BgL_ipz00_71);
					}
				else
					{	/* Ieee/input.scm 458 */
						if ((default_io_bufsiz < BgL_sza7za7_73))
							{	/* Ieee/input.scm 460 */
								BgL_bufsiza7eza7_3477 = default_io_bufsiz;
							}
						else
							{	/* Ieee/input.scm 460 */
								BgL_bufsiza7eza7_3477 = BgL_sza7za7_73;
							}
					}
				{	/* Ieee/input.scm 457 */
					obj_t BgL_bufz00_3478;

					{	/* Ieee/string.scm 172 */

						BgL_bufz00_3478 =
							make_string(BgL_bufsiza7eza7_3477, ((unsigned char) ' '));
					}
					{	/* Ieee/input.scm 466 */

						if ((BgL_sza7za7_73 < 0L))
							{
								long BgL_charszd2readzd2_3483;

								{	/* Ieee/input.scm 468 */
									long BgL_tmpz00_8530;

									BgL_charszd2readzd2_3483 = 0L;
								BgL_zc3z04anonymousza32530ze3z87_3484:
									{	/* Ieee/input.scm 469 */
										obj_t BgL_nz00_3485;

										BgL_nz00_3485 =
											BGl_readzd2charsz12zc0zz__r4_input_6_10_2z00
											(BgL_bufz00_3478, BINT(BgL_bufsiza7eza7_3477),
											BgL_ipz00_71);
										if (((long) CINT(BgL_nz00_3485) == 0L))
											{	/* Ieee/input.scm 470 */
												bgl_flush_output_port(BgL_opz00_72);
												BgL_tmpz00_8530 = BgL_charszd2readzd2_3483;
											}
										else
											{	/* Ieee/input.scm 474 */
												obj_t BgL_sz00_3487;

												if (
													((long) CINT(BgL_nz00_3485) < BgL_bufsiza7eza7_3477))
													{	/* Ieee/input.scm 474 */
														BgL_sz00_3487 =
															c_substring(BgL_bufz00_3478, 0L,
															(long) CINT(BgL_nz00_3485));
													}
												else
													{	/* Ieee/input.scm 474 */
														BgL_sz00_3487 = BgL_bufz00_3478;
													}
												bgl_display_obj(BgL_sz00_3487, BgL_opz00_72);
												{
													long BgL_charszd2readzd2_8543;

													BgL_charszd2readzd2_8543 =
														(BgL_charszd2readzd2_3483 +
														(long) CINT(BgL_nz00_3485));
													BgL_charszd2readzd2_3483 = BgL_charszd2readzd2_8543;
													goto BgL_zc3z04anonymousza32530ze3z87_3484;
												}
											}
									}
									return (int) (BgL_tmpz00_8530);
							}}
						else
							{
								long BgL_charszd2readzd2_3492;
								long BgL_charszd2tozd2readz00_3493;
								long BgL_sza7za7_3494;

								{	/* Ieee/input.scm 477 */
									long BgL_tmpz00_8547;

									BgL_charszd2readzd2_3492 = 0L;
									BgL_charszd2tozd2readz00_3493 = BgL_bufsiza7eza7_3477;
									BgL_sza7za7_3494 = BgL_sza7za7_73;
								BgL_zc3z04anonymousza32536ze3z87_3495:
									if ((BgL_charszd2tozd2readz00_3493 == 0L))
										{	/* Ieee/input.scm 480 */
											BgL_tmpz00_8547 = BgL_charszd2readzd2_3492;
										}
									else
										{	/* Ieee/input.scm 482 */
											obj_t BgL_nz00_3497;

											BgL_nz00_3497 =
												BGl_readzd2charsz12zc0zz__r4_input_6_10_2z00
												(BgL_bufz00_3478, BINT(BgL_charszd2tozd2readz00_3493),
												BgL_ipz00_71);
											if (((long) CINT(BgL_nz00_3497) == 0L))
												{	/* Ieee/input.scm 483 */
													bgl_flush_output_port(BgL_opz00_72);
													BgL_tmpz00_8547 = BgL_charszd2readzd2_3492;
												}
											else
												{	/* Ieee/input.scm 487 */
													obj_t BgL_sz00_3499;

													if (
														((long) CINT(BgL_nz00_3497) <
															BgL_bufsiza7eza7_3477))
														{	/* Ieee/input.scm 487 */
															BgL_sz00_3499 =
																c_substring(BgL_bufz00_3478, 0L,
																(long) CINT(BgL_nz00_3497));
														}
													else
														{	/* Ieee/input.scm 487 */
															BgL_sz00_3499 = BgL_bufz00_3478;
														}
													bgl_display_obj(BgL_sz00_3499, BgL_opz00_72);
													{	/* Ieee/input.scm 489 */
														long BgL_sza7za7_3500;

														BgL_sza7za7_3500 =
															(BgL_sza7za7_3494 - (long) CINT(BgL_nz00_3497));
														{	/* Ieee/input.scm 489 */
															long BgL_ctrz00_3501;

															if ((BgL_sza7za7_3500 < BgL_bufsiza7eza7_3477))
																{	/* Ieee/input.scm 490 */
																	BgL_ctrz00_3501 = BgL_sza7za7_3500;
																}
															else
																{	/* Ieee/input.scm 490 */
																	BgL_ctrz00_3501 = BgL_bufsiza7eza7_3477;
																}
															{	/* Ieee/input.scm 490 */

																{
																	long BgL_sza7za7_8570;
																	long BgL_charszd2tozd2readz00_8569;
																	long BgL_charszd2readzd2_8566;

																	BgL_charszd2readzd2_8566 =
																		(BgL_charszd2readzd2_3492 +
																		(long) CINT(BgL_nz00_3497));
																	BgL_charszd2tozd2readz00_8569 =
																		BgL_ctrz00_3501;
																	BgL_sza7za7_8570 = BgL_sza7za7_3500;
																	BgL_sza7za7_3494 = BgL_sza7za7_8570;
																	BgL_charszd2tozd2readz00_3493 =
																		BgL_charszd2tozd2readz00_8569;
																	BgL_charszd2readzd2_3492 =
																		BgL_charszd2readzd2_8566;
																	goto BgL_zc3z04anonymousza32536ze3z87_3495;
																}
															}
														}
													}
												}
										}
									return (int) (BgL_tmpz00_8547);
		}}}}}}

	}



/* file->string */
	BGL_EXPORTED_DEF obj_t BGl_filezd2ze3stringz31zz__r4_input_6_10_2z00(obj_t
		BgL_pathz00_75)
	{
		{	/* Ieee/input.scm 496 */
			{	/* Ieee/input.scm 499 */
				obj_t BgL_iz00_3510;

				{	/* Ieee/string.scm 223 */

					BgL_iz00_3510 =
						BGl_stringzd2indexzd2zz__r4_strings_6_7z00(BgL_pathz00_75,
						BCHAR(((unsigned char) ':')), BINT(0L));
				}
				if (CBOOL(BgL_iz00_3510))
					{	/* Ieee/input.scm 503 */
						bool_t BgL_test3781z00_8577;

						{	/* Ieee/input.scm 503 */

							BgL_test3781z00_8577 =
								BGl_stringzd2prefixzf3z21zz__r4_strings_6_7z00
								(BGl_string3480z00zz__r4_input_6_10_2z00, BgL_pathz00_75,
								BFALSE, BFALSE, BFALSE, BFALSE);
						}
						if (BgL_test3781z00_8577)
							{	/* Ieee/input.scm 504 */
								obj_t BgL_arg2545z00_3518;

								BgL_arg2545z00_3518 =
									c_substring(BgL_pathz00_75, 5L,
									STRING_LENGTH(BgL_pathz00_75));
								return
									bgl_file_to_string(BSTRING_TO_STRING(BgL_arg2545z00_3518));
							}
						else
							{	/* Ieee/input.scm 506 */
								obj_t BgL_ipz00_3520;

								{	/* Ieee/port.scm 466 */

									BgL_ipz00_3520 =
										BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00
										(BgL_pathz00_75, BTRUE, BINT(5000000L));
								}
								{	/* Ieee/input.scm 507 */
									obj_t BgL_exitd1140z00_3521;

									BgL_exitd1140z00_3521 = BGL_EXITD_TOP_AS_OBJ();
									{	/* Ieee/input.scm 509 */
										obj_t BgL_zc3z04anonymousza32547ze3z87_6260;

										BgL_zc3z04anonymousza32547ze3z87_6260 =
											MAKE_FX_PROCEDURE
											(BGl_z62zc3z04anonymousza32547ze3ze5zz__r4_input_6_10_2z00,
											(int) (0L), (int) (1L));
										PROCEDURE_SET(BgL_zc3z04anonymousza32547ze3z87_6260,
											(int) (0L), BgL_ipz00_3520);
										{	/* Ieee/input.scm 507 */
											obj_t BgL_arg3269z00_5665;

											{	/* Ieee/input.scm 507 */
												obj_t BgL_arg3271z00_5666;

												BgL_arg3271z00_5666 =
													BGL_EXITD_PROTECT(BgL_exitd1140z00_3521);
												BgL_arg3269z00_5665 =
													MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza32547ze3z87_6260,
													BgL_arg3271z00_5666);
											}
											BGL_EXITD_PROTECT_SET(BgL_exitd1140z00_3521,
												BgL_arg3269z00_5665);
											BUNSPEC;
										}
										{	/* Ieee/input.scm 508 */
											obj_t BgL_tmp1142z00_3523;

											BgL_tmp1142z00_3523 =
												BGl_readzd2stringzd2zz__r4_input_6_10_2z00
												(BgL_ipz00_3520);
											{	/* Ieee/input.scm 507 */
												bool_t BgL_test3782z00_8595;

												{	/* Ieee/input.scm 507 */
													obj_t BgL_arg3268z00_5668;

													BgL_arg3268z00_5668 =
														BGL_EXITD_PROTECT(BgL_exitd1140z00_3521);
													BgL_test3782z00_8595 = PAIRP(BgL_arg3268z00_5668);
												}
												if (BgL_test3782z00_8595)
													{	/* Ieee/input.scm 507 */
														obj_t BgL_arg3266z00_5669;

														{	/* Ieee/input.scm 507 */
															obj_t BgL_arg3267z00_5670;

															BgL_arg3267z00_5670 =
																BGL_EXITD_PROTECT(BgL_exitd1140z00_3521);
															BgL_arg3266z00_5669 =
																CDR(((obj_t) BgL_arg3267z00_5670));
														}
														BGL_EXITD_PROTECT_SET(BgL_exitd1140z00_3521,
															BgL_arg3266z00_5669);
														BUNSPEC;
													}
												else
													{	/* Ieee/input.scm 507 */
														BFALSE;
													}
											}
											bgl_close_input_port(((obj_t) BgL_ipz00_3520));
											return BgL_tmp1142z00_3523;
										}
									}
								}
							}
					}
				else
					{	/* Ieee/input.scm 501 */
						return bgl_file_to_string(BSTRING_TO_STRING(BgL_pathz00_75));
					}
			}
		}

	}



/* &file->string */
	obj_t BGl_z62filezd2ze3stringz53zz__r4_input_6_10_2z00(obj_t BgL_envz00_6261,
		obj_t BgL_pathz00_6262)
	{
		{	/* Ieee/input.scm 496 */
			{	/* Ieee/input.scm 499 */
				obj_t BgL_auxz00_8606;

				if (STRINGP(BgL_pathz00_6262))
					{	/* Ieee/input.scm 499 */
						BgL_auxz00_8606 = BgL_pathz00_6262;
					}
				else
					{
						obj_t BgL_auxz00_8609;

						BgL_auxz00_8609 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string3447z00zz__r4_input_6_10_2z00, BINT(19354L),
							BGl_string3481z00zz__r4_input_6_10_2z00,
							BGl_string3459z00zz__r4_input_6_10_2z00, BgL_pathz00_6262);
						FAILURE(BgL_auxz00_8609, BFALSE, BFALSE);
					}
				return BGl_filezd2ze3stringz31zz__r4_input_6_10_2z00(BgL_auxz00_8606);
			}
		}

	}



/* &<@anonymous:2547> */
	obj_t BGl_z62zc3z04anonymousza32547ze3ze5zz__r4_input_6_10_2z00(obj_t
		BgL_envz00_6263)
	{
		{	/* Ieee/input.scm 507 */
			{	/* Ieee/input.scm 509 */
				obj_t BgL_ipz00_6264;

				BgL_ipz00_6264 = PROCEDURE_REF(BgL_envz00_6263, (int) (0L));
				return bgl_close_input_port(((obj_t) BgL_ipz00_6264));
			}
		}

	}



/* send-chars/size */
	BGL_EXPORTED_DEF long BGl_sendzd2charszf2siza7ez87zz__r4_input_6_10_2z00(obj_t
		BgL_ipz00_76, obj_t BgL_opz00_77, long BgL_siza7eza7_78,
		long BgL_offsetz00_79)
	{
		{	/* Ieee/input.scm 519 */
			{	/* Ieee/input.scm 521 */
				long BgL_sza7za7_5673;
				long BgL_offz00_5674;

				BgL_sza7za7_5673 = (long) (BgL_siza7eza7_78);
				BgL_offz00_5674 = (long) (BgL_offsetz00_79);
				{	/* Ieee/input.scm 525 */
					obj_t BgL__ortest_1144z00_5675;

					BgL__ortest_1144z00_5675 =
						bgl_sendchars(BgL_ipz00_76, BgL_opz00_77, BgL_sza7za7_5673,
						BgL_offz00_5674);
					if (CBOOL(BgL__ortest_1144z00_5675))
						{	/* Ieee/input.scm 525 */
							return (long) CINT(BgL__ortest_1144z00_5675);
						}
					else
						{	/* Ieee/input.scm 527 */
							bool_t BgL_test3785z00_8624;

							if (INPUT_GZIP_PORTP(BgL_ipz00_76))
								{	/* Ieee/input.scm 527 */
									if ((BgL_sza7za7_5673 == -1L))
										{	/* Ieee/input.scm 527 */
											BgL_test3785z00_8624 = (BgL_offz00_5674 == -1L);
										}
									else
										{	/* Ieee/input.scm 527 */
											BgL_test3785z00_8624 = ((bool_t) 0);
										}
								}
							else
								{	/* Ieee/input.scm 527 */
									BgL_test3785z00_8624 = ((bool_t) 0);
								}
							if (BgL_test3785z00_8624)
								{	/* Ieee/input.scm 527 */
									return
										(long)
										CINT(BGl_gunza7ipzd2sendcharsz75zz__gunza7ipza7
										(BgL_ipz00_76, BgL_opz00_77));
								}
							else
								{	/* Ieee/input.scm 527 */
									return
										(long) (BGl_z52sendcharsz52zz__r4_input_6_10_2z00
										(BgL_ipz00_76, BgL_opz00_77, BgL_sza7za7_5673,
											BgL_offz00_5674));
		}}}}}

	}



/* &send-chars/size */
	obj_t BGl_z62sendzd2charszf2siza7eze5zz__r4_input_6_10_2z00(obj_t
		BgL_envz00_6265, obj_t BgL_ipz00_6266, obj_t BgL_opz00_6267,
		obj_t BgL_siza7eza7_6268, obj_t BgL_offsetz00_6269)
	{
		{	/* Ieee/input.scm 519 */
			{	/* Ieee/input.scm 521 */
				long BgL_tmpz00_8634;

				{	/* Ieee/input.scm 521 */
					long BgL_auxz00_8658;
					long BgL_auxz00_8649;
					obj_t BgL_auxz00_8642;
					obj_t BgL_auxz00_8635;

					{	/* Ieee/input.scm 521 */
						obj_t BgL_tmpz00_8659;

						if (ELONGP(BgL_offsetz00_6269))
							{	/* Ieee/input.scm 521 */
								BgL_tmpz00_8659 = BgL_offsetz00_6269;
							}
						else
							{
								obj_t BgL_auxz00_8662;

								BgL_auxz00_8662 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string3447z00zz__r4_input_6_10_2z00, BINT(20115L),
									BGl_string3482z00zz__r4_input_6_10_2z00,
									BGl_string3484z00zz__r4_input_6_10_2z00, BgL_offsetz00_6269);
								FAILURE(BgL_auxz00_8662, BFALSE, BFALSE);
							}
						BgL_auxz00_8658 = BELONG_TO_LONG(BgL_tmpz00_8659);
					}
					{	/* Ieee/input.scm 521 */
						obj_t BgL_tmpz00_8650;

						if (ELONGP(BgL_siza7eza7_6268))
							{	/* Ieee/input.scm 521 */
								BgL_tmpz00_8650 = BgL_siza7eza7_6268;
							}
						else
							{
								obj_t BgL_auxz00_8653;

								BgL_auxz00_8653 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string3447z00zz__r4_input_6_10_2z00, BINT(20115L),
									BGl_string3482z00zz__r4_input_6_10_2z00,
									BGl_string3484z00zz__r4_input_6_10_2z00, BgL_siza7eza7_6268);
								FAILURE(BgL_auxz00_8653, BFALSE, BFALSE);
							}
						BgL_auxz00_8649 = BELONG_TO_LONG(BgL_tmpz00_8650);
					}
					if (OUTPUT_PORTP(BgL_opz00_6267))
						{	/* Ieee/input.scm 521 */
							BgL_auxz00_8642 = BgL_opz00_6267;
						}
					else
						{
							obj_t BgL_auxz00_8645;

							BgL_auxz00_8645 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string3447z00zz__r4_input_6_10_2z00, BINT(20115L),
								BGl_string3482z00zz__r4_input_6_10_2z00,
								BGl_string3483z00zz__r4_input_6_10_2z00, BgL_opz00_6267);
							FAILURE(BgL_auxz00_8645, BFALSE, BFALSE);
						}
					if (INPUT_PORTP(BgL_ipz00_6266))
						{	/* Ieee/input.scm 521 */
							BgL_auxz00_8635 = BgL_ipz00_6266;
						}
					else
						{
							obj_t BgL_auxz00_8638;

							BgL_auxz00_8638 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string3447z00zz__r4_input_6_10_2z00, BINT(20115L),
								BGl_string3482z00zz__r4_input_6_10_2z00,
								BGl_string3450z00zz__r4_input_6_10_2z00, BgL_ipz00_6266);
							FAILURE(BgL_auxz00_8638, BFALSE, BFALSE);
						}
					BgL_tmpz00_8634 =
						BGl_sendzd2charszf2siza7ez87zz__r4_input_6_10_2z00(BgL_auxz00_8635,
						BgL_auxz00_8642, BgL_auxz00_8649, BgL_auxz00_8658);
				}
				return BINT(BgL_tmpz00_8634);
			}
		}

	}



/* _send-chars */
	obj_t BGl__sendzd2charszd2zz__r4_input_6_10_2z00(obj_t BgL_env1319z00_85,
		obj_t BgL_opt1318z00_84)
	{
		{	/* Ieee/input.scm 541 */
			{	/* Ieee/input.scm 541 */
				obj_t BgL_g1320z00_3546;
				obj_t BgL_g1321z00_3547;

				BgL_g1320z00_3546 = VECTOR_REF(BgL_opt1318z00_84, 0L);
				BgL_g1321z00_3547 = VECTOR_REF(BgL_opt1318z00_84, 1L);
				switch (VECTOR_LENGTH(BgL_opt1318z00_84))
					{
					case 2L:

						{	/* Ieee/input.scm 541 */

							{	/* Ieee/input.scm 541 */
								obj_t BgL_ipz00_5683;
								obj_t BgL_opz00_5684;

								if (INPUT_PORTP(BgL_g1320z00_3546))
									{	/* Ieee/input.scm 541 */
										BgL_ipz00_5683 = BgL_g1320z00_3546;
									}
								else
									{
										obj_t BgL_auxz00_8673;

										BgL_auxz00_8673 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string3447z00zz__r4_input_6_10_2z00, BINT(20782L),
											BGl_string3485z00zz__r4_input_6_10_2z00,
											BGl_string3450z00zz__r4_input_6_10_2z00,
											BgL_g1320z00_3546);
										FAILURE(BgL_auxz00_8673, BFALSE, BFALSE);
									}
								if (OUTPUT_PORTP(BgL_g1321z00_3547))
									{	/* Ieee/input.scm 541 */
										BgL_opz00_5684 = BgL_g1321z00_3547;
									}
								else
									{
										obj_t BgL_auxz00_8679;

										BgL_auxz00_8679 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string3447z00zz__r4_input_6_10_2z00, BINT(20782L),
											BGl_string3485z00zz__r4_input_6_10_2z00,
											BGl_string3483z00zz__r4_input_6_10_2z00,
											BgL_g1321z00_3547);
										FAILURE(BgL_auxz00_8679, BFALSE, BFALSE);
									}
								{	/* Ieee/input.scm 543 */
									long BgL_sza7za7_5685;
									long BgL_offz00_5686;

									BgL_sza7za7_5685 = (long) (-1L);
									BgL_offz00_5686 = (long) (-1L);
									return
										BINT(BGl_sendzd2charszf2siza7ez87zz__r4_input_6_10_2z00
										(BgL_ipz00_5683, BgL_opz00_5684, BgL_sza7za7_5685,
											BgL_offz00_5686));
								}
							}
						}
						break;
					case 3L:

						{	/* Ieee/input.scm 541 */
							obj_t BgL_siza7eza7_3552;

							BgL_siza7eza7_3552 = VECTOR_REF(BgL_opt1318z00_84, 2L);
							{	/* Ieee/input.scm 541 */

								{	/* Ieee/input.scm 541 */
									obj_t BgL_ipz00_5693;
									obj_t BgL_opz00_5694;

									if (INPUT_PORTP(BgL_g1320z00_3546))
										{	/* Ieee/input.scm 541 */
											BgL_ipz00_5693 = BgL_g1320z00_3546;
										}
									else
										{
											obj_t BgL_auxz00_8690;

											BgL_auxz00_8690 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3447z00zz__r4_input_6_10_2z00, BINT(20782L),
												BGl_string3485z00zz__r4_input_6_10_2z00,
												BGl_string3450z00zz__r4_input_6_10_2z00,
												BgL_g1320z00_3546);
											FAILURE(BgL_auxz00_8690, BFALSE, BFALSE);
										}
									if (OUTPUT_PORTP(BgL_g1321z00_3547))
										{	/* Ieee/input.scm 541 */
											BgL_opz00_5694 = BgL_g1321z00_3547;
										}
									else
										{
											obj_t BgL_auxz00_8696;

											BgL_auxz00_8696 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3447z00zz__r4_input_6_10_2z00, BINT(20782L),
												BGl_string3485z00zz__r4_input_6_10_2z00,
												BGl_string3483z00zz__r4_input_6_10_2z00,
												BgL_g1321z00_3547);
											FAILURE(BgL_auxz00_8696, BFALSE, BFALSE);
										}
									{	/* Ieee/input.scm 543 */
										long BgL_sza7za7_5695;
										long BgL_offz00_5696;

										if (INTEGERP(BgL_siza7eza7_3552))
											{	/* Ieee/input.scm 544 */
												long BgL_tmpz00_8702;

												BgL_tmpz00_8702 = (long) CINT(BgL_siza7eza7_3552);
												BgL_sza7za7_5695 = (long) (BgL_tmpz00_8702);
											}
										else
											{	/* Ieee/input.scm 544 */
												if (ELONGP(BgL_siza7eza7_3552))
													{	/* Ieee/input.scm 545 */
														BgL_sza7za7_5695 =
															BELONG_TO_LONG(BgL_siza7eza7_3552);
													}
												else
													{	/* Ieee/input.scm 546 */
														obj_t BgL_tmpz00_8708;

														{	/* Ieee/input.scm 546 */
															obj_t BgL_aux3386z00_6357;

															BgL_aux3386z00_6357 =
																BGl_errorz00zz__errorz00
																(BGl_symbol3486z00zz__r4_input_6_10_2z00,
																BGl_string3488z00zz__r4_input_6_10_2z00,
																BgL_siza7eza7_3552);
															if (ELONGP(BgL_aux3386z00_6357))
																{	/* Ieee/input.scm 546 */
																	BgL_tmpz00_8708 = BgL_aux3386z00_6357;
																}
															else
																{
																	obj_t BgL_auxz00_8712;

																	BgL_auxz00_8712 =
																		BGl_typezd2errorzd2zz__errorz00
																		(BGl_string3447z00zz__r4_input_6_10_2z00,
																		BINT(20996L),
																		BGl_string3485z00zz__r4_input_6_10_2z00,
																		BGl_string3484z00zz__r4_input_6_10_2z00,
																		BgL_aux3386z00_6357);
																	FAILURE(BgL_auxz00_8712, BFALSE, BFALSE);
																}
														}
														BgL_sza7za7_5695 = BELONG_TO_LONG(BgL_tmpz00_8708);
													}
											}
										BgL_offz00_5696 = (long) (-1L);
										return
											BINT(BGl_sendzd2charszf2siza7ez87zz__r4_input_6_10_2z00
											(BgL_ipz00_5693, BgL_opz00_5694, BgL_sza7za7_5695,
												BgL_offz00_5696));
									}
								}
							}
						}
						break;
					case 4L:

						{	/* Ieee/input.scm 541 */
							obj_t BgL_siza7eza7_3554;

							BgL_siza7eza7_3554 = VECTOR_REF(BgL_opt1318z00_84, 2L);
							{	/* Ieee/input.scm 541 */
								obj_t BgL_offsetz00_3555;

								BgL_offsetz00_3555 = VECTOR_REF(BgL_opt1318z00_84, 3L);
								{	/* Ieee/input.scm 541 */

									{	/* Ieee/input.scm 541 */
										obj_t BgL_ipz00_5703;
										obj_t BgL_opz00_5704;

										if (INPUT_PORTP(BgL_g1320z00_3546))
											{	/* Ieee/input.scm 541 */
												BgL_ipz00_5703 = BgL_g1320z00_3546;
											}
										else
											{
												obj_t BgL_auxz00_8724;

												BgL_auxz00_8724 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3447z00zz__r4_input_6_10_2z00,
													BINT(20782L), BGl_string3485z00zz__r4_input_6_10_2z00,
													BGl_string3450z00zz__r4_input_6_10_2z00,
													BgL_g1320z00_3546);
												FAILURE(BgL_auxz00_8724, BFALSE, BFALSE);
											}
										if (OUTPUT_PORTP(BgL_g1321z00_3547))
											{	/* Ieee/input.scm 541 */
												BgL_opz00_5704 = BgL_g1321z00_3547;
											}
										else
											{
												obj_t BgL_auxz00_8730;

												BgL_auxz00_8730 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3447z00zz__r4_input_6_10_2z00,
													BINT(20782L), BGl_string3485z00zz__r4_input_6_10_2z00,
													BGl_string3483z00zz__r4_input_6_10_2z00,
													BgL_g1321z00_3547);
												FAILURE(BgL_auxz00_8730, BFALSE, BFALSE);
											}
										{	/* Ieee/input.scm 543 */
											long BgL_sza7za7_5705;
											long BgL_offz00_5706;

											if (INTEGERP(BgL_siza7eza7_3554))
												{	/* Ieee/input.scm 544 */
													long BgL_tmpz00_8736;

													BgL_tmpz00_8736 = (long) CINT(BgL_siza7eza7_3554);
													BgL_sza7za7_5705 = (long) (BgL_tmpz00_8736);
												}
											else
												{	/* Ieee/input.scm 544 */
													if (ELONGP(BgL_siza7eza7_3554))
														{	/* Ieee/input.scm 545 */
															BgL_sza7za7_5705 =
																BELONG_TO_LONG(BgL_siza7eza7_3554);
														}
													else
														{	/* Ieee/input.scm 546 */
															obj_t BgL_tmpz00_8742;

															{	/* Ieee/input.scm 546 */
																obj_t BgL_aux3392z00_6363;

																BgL_aux3392z00_6363 =
																	BGl_errorz00zz__errorz00
																	(BGl_symbol3486z00zz__r4_input_6_10_2z00,
																	BGl_string3488z00zz__r4_input_6_10_2z00,
																	BgL_siza7eza7_3554);
																if (ELONGP(BgL_aux3392z00_6363))
																	{	/* Ieee/input.scm 546 */
																		BgL_tmpz00_8742 = BgL_aux3392z00_6363;
																	}
																else
																	{
																		obj_t BgL_auxz00_8746;

																		BgL_auxz00_8746 =
																			BGl_typezd2errorzd2zz__errorz00
																			(BGl_string3447z00zz__r4_input_6_10_2z00,
																			BINT(20996L),
																			BGl_string3485z00zz__r4_input_6_10_2z00,
																			BGl_string3484z00zz__r4_input_6_10_2z00,
																			BgL_aux3392z00_6363);
																		FAILURE(BgL_auxz00_8746, BFALSE, BFALSE);
																	}
															}
															BgL_sza7za7_5705 =
																BELONG_TO_LONG(BgL_tmpz00_8742);
														}
												}
											if (INTEGERP(BgL_offsetz00_3555))
												{	/* Ieee/input.scm 548 */
													long BgL_tmpz00_8753;

													BgL_tmpz00_8753 = (long) CINT(BgL_offsetz00_3555);
													BgL_offz00_5706 = (long) (BgL_tmpz00_8753);
												}
											else
												{	/* Ieee/input.scm 548 */
													if (ELONGP(BgL_offsetz00_3555))
														{	/* Ieee/input.scm 549 */
															BgL_offz00_5706 =
																BELONG_TO_LONG(BgL_offsetz00_3555);
														}
													else
														{	/* Ieee/input.scm 550 */
															obj_t BgL_tmpz00_8759;

															{	/* Ieee/input.scm 550 */
																obj_t BgL_aux3394z00_6365;

																BgL_aux3394z00_6365 =
																	BGl_errorz00zz__errorz00
																	(BGl_symbol3486z00zz__r4_input_6_10_2z00,
																	BGl_string3489z00zz__r4_input_6_10_2z00,
																	BgL_offsetz00_3555);
																if (ELONGP(BgL_aux3394z00_6365))
																	{	/* Ieee/input.scm 550 */
																		BgL_tmpz00_8759 = BgL_aux3394z00_6365;
																	}
																else
																	{
																		obj_t BgL_auxz00_8763;

																		BgL_auxz00_8763 =
																			BGl_typezd2errorzd2zz__errorz00
																			(BGl_string3447z00zz__r4_input_6_10_2z00,
																			BINT(21141L),
																			BGl_string3485z00zz__r4_input_6_10_2z00,
																			BGl_string3484z00zz__r4_input_6_10_2z00,
																			BgL_aux3394z00_6365);
																		FAILURE(BgL_auxz00_8763, BFALSE, BFALSE);
																	}
															}
															BgL_offz00_5706 = BELONG_TO_LONG(BgL_tmpz00_8759);
														}
												}
											return
												BINT(BGl_sendzd2charszf2siza7ez87zz__r4_input_6_10_2z00
												(BgL_ipz00_5703, BgL_opz00_5704, BgL_sza7za7_5705,
													BgL_offz00_5706));
										}
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* send-chars */
	BGL_EXPORTED_DEF long BGl_sendzd2charszd2zz__r4_input_6_10_2z00(obj_t
		BgL_ipz00_80, obj_t BgL_opz00_81, obj_t BgL_siza7eza7_82,
		obj_t BgL_offsetz00_83)
	{
		{	/* Ieee/input.scm 541 */
			{	/* Ieee/input.scm 543 */
				long BgL_sza7za7_5713;
				long BgL_offz00_5714;

				if (INTEGERP(BgL_siza7eza7_82))
					{	/* Ieee/input.scm 544 */
						long BgL_tmpz00_8774;

						BgL_tmpz00_8774 = (long) CINT(BgL_siza7eza7_82);
						BgL_sza7za7_5713 = (long) (BgL_tmpz00_8774);
					}
				else
					{	/* Ieee/input.scm 544 */
						if (ELONGP(BgL_siza7eza7_82))
							{	/* Ieee/input.scm 545 */
								BgL_sza7za7_5713 = BELONG_TO_LONG(BgL_siza7eza7_82);
							}
						else
							{	/* Ieee/input.scm 546 */
								obj_t BgL_tmpz00_8780;

								BgL_tmpz00_8780 =
									BGl_errorz00zz__errorz00
									(BGl_symbol3486z00zz__r4_input_6_10_2z00,
									BGl_string3488z00zz__r4_input_6_10_2z00, BgL_siza7eza7_82);
								BgL_sza7za7_5713 = BELONG_TO_LONG(BgL_tmpz00_8780);
							}
					}
				if (INTEGERP(BgL_offsetz00_83))
					{	/* Ieee/input.scm 548 */
						long BgL_tmpz00_8785;

						BgL_tmpz00_8785 = (long) CINT(BgL_offsetz00_83);
						BgL_offz00_5714 = (long) (BgL_tmpz00_8785);
					}
				else
					{	/* Ieee/input.scm 548 */
						if (ELONGP(BgL_offsetz00_83))
							{	/* Ieee/input.scm 549 */
								BgL_offz00_5714 = BELONG_TO_LONG(BgL_offsetz00_83);
							}
						else
							{	/* Ieee/input.scm 550 */
								obj_t BgL_tmpz00_8791;

								BgL_tmpz00_8791 =
									BGl_errorz00zz__errorz00
									(BGl_symbol3486z00zz__r4_input_6_10_2z00,
									BGl_string3489z00zz__r4_input_6_10_2z00, BgL_offsetz00_83);
								BgL_offz00_5714 = BELONG_TO_LONG(BgL_tmpz00_8791);
							}
					}
				{	/* Ieee/input.scm 551 */
					long BgL_res3311z00_5735;

					{	/* Ieee/input.scm 521 */
						long BgL_sza7za7_5725;
						long BgL_offz00_5726;

						BgL_sza7za7_5725 = (long) (BgL_sza7za7_5713);
						BgL_offz00_5726 = (long) (BgL_offz00_5714);
						{	/* Ieee/input.scm 525 */
							obj_t BgL__ortest_1144z00_5727;

							BgL__ortest_1144z00_5727 =
								bgl_sendchars(BgL_ipz00_80, BgL_opz00_81, BgL_sza7za7_5725,
								BgL_offz00_5726);
							if (CBOOL(BgL__ortest_1144z00_5727))
								{	/* Ieee/input.scm 525 */
									BgL_res3311z00_5735 = (long) CINT(BgL__ortest_1144z00_5727);
								}
							else
								{	/* Ieee/input.scm 527 */
									bool_t BgL_test3812z00_8800;

									if (INPUT_GZIP_PORTP(BgL_ipz00_80))
										{	/* Ieee/input.scm 527 */
											if ((BgL_sza7za7_5725 == -1L))
												{	/* Ieee/input.scm 527 */
													BgL_test3812z00_8800 = (BgL_offz00_5726 == -1L);
												}
											else
												{	/* Ieee/input.scm 527 */
													BgL_test3812z00_8800 = ((bool_t) 0);
												}
										}
									else
										{	/* Ieee/input.scm 527 */
											BgL_test3812z00_8800 = ((bool_t) 0);
										}
									if (BgL_test3812z00_8800)
										{	/* Ieee/input.scm 527 */
											BgL_res3311z00_5735 =
												(long)
												CINT(BGl_gunza7ipzd2sendcharsz75zz__gunza7ipza7
												(BgL_ipz00_80, BgL_opz00_81));
										}
									else
										{	/* Ieee/input.scm 527 */
											BgL_res3311z00_5735 =
												(long) (BGl_z52sendcharsz52zz__r4_input_6_10_2z00
												(BgL_ipz00_80, BgL_opz00_81, BgL_sza7za7_5725,
													BgL_offz00_5726));
					}}}}
					return BgL_res3311z00_5735;
				}
			}
		}

	}



/* _send-file */
	obj_t BGl__sendzd2filezd2zz__r4_input_6_10_2z00(obj_t BgL_env1325z00_91,
		obj_t BgL_opt1324z00_90)
	{
		{	/* Ieee/input.scm 556 */
			{	/* Ieee/input.scm 556 */
				obj_t BgL_filez00_3562;
				obj_t BgL_opz00_3563;

				BgL_filez00_3562 = VECTOR_REF(BgL_opt1324z00_90, 0L);
				BgL_opz00_3563 = VECTOR_REF(BgL_opt1324z00_90, 1L);
				switch (VECTOR_LENGTH(BgL_opt1324z00_90))
					{
					case 2L:

						{	/* Ieee/input.scm 556 */

							{	/* Ieee/input.scm 556 */
								long BgL_res3312z00_5761;

								{	/* Ieee/input.scm 556 */
									obj_t BgL_filez00_5736;
									obj_t BgL_opz00_5737;

									if (STRINGP(BgL_filez00_3562))
										{	/* Ieee/input.scm 556 */
											BgL_filez00_5736 = BgL_filez00_3562;
										}
									else
										{
											obj_t BgL_auxz00_8814;

											BgL_auxz00_8814 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3447z00zz__r4_input_6_10_2z00, BINT(21451L),
												BGl_string3490z00zz__r4_input_6_10_2z00,
												BGl_string3459z00zz__r4_input_6_10_2z00,
												BgL_filez00_3562);
											FAILURE(BgL_auxz00_8814, BFALSE, BFALSE);
										}
									if (OUTPUT_PORTP(BgL_opz00_3563))
										{	/* Ieee/input.scm 556 */
											BgL_opz00_5737 = BgL_opz00_3563;
										}
									else
										{
											obj_t BgL_auxz00_8820;

											BgL_auxz00_8820 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3447z00zz__r4_input_6_10_2z00, BINT(21451L),
												BGl_string3490z00zz__r4_input_6_10_2z00,
												BGl_string3483z00zz__r4_input_6_10_2z00,
												BgL_opz00_3563);
											FAILURE(BgL_auxz00_8820, BFALSE, BFALSE);
										}
									{	/* Ieee/input.scm 560 */
										long BgL_sza7za7_5740;
										long BgL_offz00_5741;

										BgL_sza7za7_5740 = (long) (((long) -1));
										BgL_offz00_5741 = (long) (((long) -1));
										{	/* Ieee/input.scm 564 */
											obj_t BgL__ortest_1147z00_5742;

											BgL__ortest_1147z00_5742 =
												bgl_sendfile(BgL_filez00_5736, BgL_opz00_5737,
												(long) (((long) -1)), (long) (((long) -1)));
											if (CBOOL(BgL__ortest_1147z00_5742))
												{	/* Ieee/input.scm 564 */
													obj_t BgL_tmpz00_8831;

													if (INTEGERP(BgL__ortest_1147z00_5742))
														{	/* Ieee/input.scm 564 */
															BgL_tmpz00_8831 = BgL__ortest_1147z00_5742;
														}
													else
														{
															obj_t BgL_auxz00_8834;

															BgL_auxz00_8834 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string3447z00zz__r4_input_6_10_2z00,
																BINT(21690L),
																BGl_string3490z00zz__r4_input_6_10_2z00,
																BGl_string3463z00zz__r4_input_6_10_2z00,
																BgL__ortest_1147z00_5742);
															FAILURE(BgL_auxz00_8834, BFALSE, BFALSE);
														}
													BgL_res3312z00_5761 = (long) CINT(BgL_tmpz00_8831);
												}
											else
												{	/* Ieee/input.scm 565 */
													obj_t BgL_ipz00_5743;

													{	/* Ieee/input.scm 565 */

														BgL_ipz00_5743 =
															BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00
															(BgL_filez00_5736, BTRUE, BINT(5000000L));
													}
													{	/* Ieee/input.scm 566 */
														obj_t BgL_exitd1148z00_5747;

														BgL_exitd1148z00_5747 = BGL_EXITD_TOP_AS_OBJ();
														{	/* Ieee/input.scm 568 */
															obj_t BgL_zc3z04anonymousza32558ze3z87_6272;

															BgL_zc3z04anonymousza32558ze3z87_6272 =
																MAKE_FX_PROCEDURE
																(BGl_z62zc3z04anonymousza32558ze33320ze5zz__r4_input_6_10_2z00,
																(int) (0L), (int) (1L));
															PROCEDURE_SET
																(BgL_zc3z04anonymousza32558ze3z87_6272,
																(int) (0L), BgL_ipz00_5743);
															{	/* Ieee/input.scm 566 */
																obj_t BgL_arg3269z00_5753;

																{	/* Ieee/input.scm 566 */
																	obj_t BgL_arg3271z00_5754;

																	BgL_arg3271z00_5754 =
																		BGL_EXITD_PROTECT(BgL_exitd1148z00_5747);
																	BgL_arg3269z00_5753 =
																		MAKE_YOUNG_PAIR
																		(BgL_zc3z04anonymousza32558ze3z87_6272,
																		BgL_arg3271z00_5754);
																}
																BGL_EXITD_PROTECT_SET(BgL_exitd1148z00_5747,
																	BgL_arg3269z00_5753);
																BUNSPEC;
															}
															{	/* Ieee/input.scm 567 */
																long BgL_tmp1150z00_5749;

																{	/* Ieee/input.scm 567 */
																	obj_t BgL_auxz00_8850;

																	if (INPUT_PORTP(BgL_ipz00_5743))
																		{	/* Ieee/input.scm 567 */
																			BgL_auxz00_8850 = BgL_ipz00_5743;
																		}
																	else
																		{
																			obj_t BgL_auxz00_8853;

																			BgL_auxz00_8853 =
																				BGl_typezd2errorzd2zz__errorz00
																				(BGl_string3447z00zz__r4_input_6_10_2z00,
																				BINT(21806L),
																				BGl_string3490z00zz__r4_input_6_10_2z00,
																				BGl_string3450z00zz__r4_input_6_10_2z00,
																				BgL_ipz00_5743);
																			FAILURE(BgL_auxz00_8853, BFALSE, BFALSE);
																		}
																	BgL_tmp1150z00_5749 =
																		BGl_sendzd2charszf2siza7ez87zz__r4_input_6_10_2z00
																		(BgL_auxz00_8850, BgL_opz00_5737,
																		((long) -1), ((long) -1));
																}
																{	/* Ieee/input.scm 566 */
																	bool_t BgL_test3820z00_8858;

																	{	/* Ieee/input.scm 566 */
																		obj_t BgL_arg3268z00_5756;

																		BgL_arg3268z00_5756 =
																			BGL_EXITD_PROTECT(BgL_exitd1148z00_5747);
																		BgL_test3820z00_8858 =
																			PAIRP(BgL_arg3268z00_5756);
																	}
																	if (BgL_test3820z00_8858)
																		{	/* Ieee/input.scm 566 */
																			obj_t BgL_arg3266z00_5757;

																			{	/* Ieee/input.scm 566 */
																				obj_t BgL_arg3267z00_5758;

																				BgL_arg3267z00_5758 =
																					BGL_EXITD_PROTECT
																					(BgL_exitd1148z00_5747);
																				{	/* Ieee/input.scm 566 */
																					obj_t BgL_pairz00_5759;

																					if (PAIRP(BgL_arg3267z00_5758))
																						{	/* Ieee/input.scm 566 */
																							BgL_pairz00_5759 =
																								BgL_arg3267z00_5758;
																						}
																					else
																						{
																							obj_t BgL_auxz00_8864;

																							BgL_auxz00_8864 =
																								BGl_typezd2errorzd2zz__errorz00
																								(BGl_string3447z00zz__r4_input_6_10_2z00,
																								BINT(21767L),
																								BGl_string3490z00zz__r4_input_6_10_2z00,
																								BGl_string3491z00zz__r4_input_6_10_2z00,
																								BgL_arg3267z00_5758);
																							FAILURE(BgL_auxz00_8864, BFALSE,
																								BFALSE);
																						}
																					BgL_arg3266z00_5757 =
																						CDR(BgL_pairz00_5759);
																				}
																			}
																			BGL_EXITD_PROTECT_SET
																				(BgL_exitd1148z00_5747,
																				BgL_arg3266z00_5757);
																			BUNSPEC;
																		}
																	else
																		{	/* Ieee/input.scm 566 */
																			BFALSE;
																		}
																}
																{	/* Ieee/input.scm 568 */
																	obj_t BgL_portz00_5760;

																	if (INPUT_PORTP(BgL_ipz00_5743))
																		{	/* Ieee/input.scm 568 */
																			BgL_portz00_5760 = BgL_ipz00_5743;
																		}
																	else
																		{
																			obj_t BgL_auxz00_8872;

																			BgL_auxz00_8872 =
																				BGl_typezd2errorzd2zz__errorz00
																				(BGl_string3447z00zz__r4_input_6_10_2z00,
																				BINT(21849L),
																				BGl_string3490z00zz__r4_input_6_10_2z00,
																				BGl_string3450z00zz__r4_input_6_10_2z00,
																				BgL_ipz00_5743);
																			FAILURE(BgL_auxz00_8872, BFALSE, BFALSE);
																		}
																	bgl_close_input_port(BgL_portz00_5760);
																}
																BgL_res3312z00_5761 = BgL_tmp1150z00_5749;
															}
														}
													}
												}
										}
									}
								}
								return BINT(BgL_res3312z00_5761);
							}
						}
						break;
					case 3L:

						{	/* Ieee/input.scm 556 */
							obj_t BgL_siza7eza7_3568;

							BgL_siza7eza7_3568 = VECTOR_REF(BgL_opt1324z00_90, 2L);
							{	/* Ieee/input.scm 556 */

								{	/* Ieee/input.scm 556 */
									long BgL_res3313z00_5787;

									{	/* Ieee/input.scm 556 */
										obj_t BgL_filez00_5762;
										obj_t BgL_opz00_5763;
										long BgL_siza7eza7_5764;

										if (STRINGP(BgL_filez00_3562))
											{	/* Ieee/input.scm 556 */
												BgL_filez00_5762 = BgL_filez00_3562;
											}
										else
											{
												obj_t BgL_auxz00_8881;

												BgL_auxz00_8881 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3447z00zz__r4_input_6_10_2z00,
													BINT(21451L), BGl_string3490z00zz__r4_input_6_10_2z00,
													BGl_string3459z00zz__r4_input_6_10_2z00,
													BgL_filez00_3562);
												FAILURE(BgL_auxz00_8881, BFALSE, BFALSE);
											}
										if (OUTPUT_PORTP(BgL_opz00_3563))
											{	/* Ieee/input.scm 556 */
												BgL_opz00_5763 = BgL_opz00_3563;
											}
										else
											{
												obj_t BgL_auxz00_8887;

												BgL_auxz00_8887 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3447z00zz__r4_input_6_10_2z00,
													BINT(21451L), BGl_string3490z00zz__r4_input_6_10_2z00,
													BGl_string3483z00zz__r4_input_6_10_2z00,
													BgL_opz00_3563);
												FAILURE(BgL_auxz00_8887, BFALSE, BFALSE);
											}
										{	/* Ieee/input.scm 556 */
											obj_t BgL_tmpz00_8891;

											if (ELONGP(BgL_siza7eza7_3568))
												{	/* Ieee/input.scm 556 */
													BgL_tmpz00_8891 = BgL_siza7eza7_3568;
												}
											else
												{
													obj_t BgL_auxz00_8894;

													BgL_auxz00_8894 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string3447z00zz__r4_input_6_10_2z00,
														BINT(21451L),
														BGl_string3490z00zz__r4_input_6_10_2z00,
														BGl_string3484z00zz__r4_input_6_10_2z00,
														BgL_siza7eza7_3568);
													FAILURE(BgL_auxz00_8894, BFALSE, BFALSE);
												}
											BgL_siza7eza7_5764 = BELONG_TO_LONG(BgL_tmpz00_8891);
										}
										{	/* Ieee/input.scm 560 */
											long BgL_sza7za7_5766;
											long BgL_offz00_5767;

											BgL_sza7za7_5766 = (long) (BgL_siza7eza7_5764);
											BgL_offz00_5767 = (long) (((long) -1));
											{	/* Ieee/input.scm 564 */
												obj_t BgL__ortest_1147z00_5768;

												BgL__ortest_1147z00_5768 =
													bgl_sendfile(BgL_filez00_5762, BgL_opz00_5763,
													(long) (BgL_siza7eza7_5764), (long) (((long) -1)));
												if (CBOOL(BgL__ortest_1147z00_5768))
													{	/* Ieee/input.scm 564 */
														obj_t BgL_tmpz00_8906;

														if (INTEGERP(BgL__ortest_1147z00_5768))
															{	/* Ieee/input.scm 564 */
																BgL_tmpz00_8906 = BgL__ortest_1147z00_5768;
															}
														else
															{
																obj_t BgL_auxz00_8909;

																BgL_auxz00_8909 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string3447z00zz__r4_input_6_10_2z00,
																	BINT(21690L),
																	BGl_string3490z00zz__r4_input_6_10_2z00,
																	BGl_string3463z00zz__r4_input_6_10_2z00,
																	BgL__ortest_1147z00_5768);
																FAILURE(BgL_auxz00_8909, BFALSE, BFALSE);
															}
														BgL_res3313z00_5787 = (long) CINT(BgL_tmpz00_8906);
													}
												else
													{	/* Ieee/input.scm 565 */
														obj_t BgL_ipz00_5769;

														{	/* Ieee/input.scm 565 */

															BgL_ipz00_5769 =
																BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00
																(BgL_filez00_5762, BTRUE, BINT(5000000L));
														}
														{	/* Ieee/input.scm 566 */
															obj_t BgL_exitd1148z00_5773;

															BgL_exitd1148z00_5773 = BGL_EXITD_TOP_AS_OBJ();
															{	/* Ieee/input.scm 568 */
																obj_t BgL_zc3z04anonymousza32558ze3z87_6271;

																BgL_zc3z04anonymousza32558ze3z87_6271 =
																	MAKE_FX_PROCEDURE
																	(BGl_z62zc3z04anonymousza32558ze33319ze5zz__r4_input_6_10_2z00,
																	(int) (0L), (int) (1L));
																PROCEDURE_SET
																	(BgL_zc3z04anonymousza32558ze3z87_6271,
																	(int) (0L), BgL_ipz00_5769);
																{	/* Ieee/input.scm 566 */
																	obj_t BgL_arg3269z00_5779;

																	{	/* Ieee/input.scm 566 */
																		obj_t BgL_arg3271z00_5780;

																		BgL_arg3271z00_5780 =
																			BGL_EXITD_PROTECT(BgL_exitd1148z00_5773);
																		BgL_arg3269z00_5779 =
																			MAKE_YOUNG_PAIR
																			(BgL_zc3z04anonymousza32558ze3z87_6271,
																			BgL_arg3271z00_5780);
																	}
																	BGL_EXITD_PROTECT_SET(BgL_exitd1148z00_5773,
																		BgL_arg3269z00_5779);
																	BUNSPEC;
																}
																{	/* Ieee/input.scm 567 */
																	long BgL_tmp1150z00_5775;

																	{	/* Ieee/input.scm 567 */
																		obj_t BgL_auxz00_8925;

																		if (INPUT_PORTP(BgL_ipz00_5769))
																			{	/* Ieee/input.scm 567 */
																				BgL_auxz00_8925 = BgL_ipz00_5769;
																			}
																		else
																			{
																				obj_t BgL_auxz00_8928;

																				BgL_auxz00_8928 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string3447z00zz__r4_input_6_10_2z00,
																					BINT(21806L),
																					BGl_string3490z00zz__r4_input_6_10_2z00,
																					BGl_string3450z00zz__r4_input_6_10_2z00,
																					BgL_ipz00_5769);
																				FAILURE(BgL_auxz00_8928, BFALSE,
																					BFALSE);
																			}
																		BgL_tmp1150z00_5775 =
																			BGl_sendzd2charszf2siza7ez87zz__r4_input_6_10_2z00
																			(BgL_auxz00_8925, BgL_opz00_5763,
																			BgL_siza7eza7_5764, ((long) -1));
																	}
																	{	/* Ieee/input.scm 566 */
																		bool_t BgL_test3829z00_8933;

																		{	/* Ieee/input.scm 566 */
																			obj_t BgL_arg3268z00_5782;

																			BgL_arg3268z00_5782 =
																				BGL_EXITD_PROTECT
																				(BgL_exitd1148z00_5773);
																			BgL_test3829z00_8933 =
																				PAIRP(BgL_arg3268z00_5782);
																		}
																		if (BgL_test3829z00_8933)
																			{	/* Ieee/input.scm 566 */
																				obj_t BgL_arg3266z00_5783;

																				{	/* Ieee/input.scm 566 */
																					obj_t BgL_arg3267z00_5784;

																					BgL_arg3267z00_5784 =
																						BGL_EXITD_PROTECT
																						(BgL_exitd1148z00_5773);
																					{	/* Ieee/input.scm 566 */
																						obj_t BgL_pairz00_5785;

																						if (PAIRP(BgL_arg3267z00_5784))
																							{	/* Ieee/input.scm 566 */
																								BgL_pairz00_5785 =
																									BgL_arg3267z00_5784;
																							}
																						else
																							{
																								obj_t BgL_auxz00_8939;

																								BgL_auxz00_8939 =
																									BGl_typezd2errorzd2zz__errorz00
																									(BGl_string3447z00zz__r4_input_6_10_2z00,
																									BINT(21767L),
																									BGl_string3490z00zz__r4_input_6_10_2z00,
																									BGl_string3491z00zz__r4_input_6_10_2z00,
																									BgL_arg3267z00_5784);
																								FAILURE(BgL_auxz00_8939, BFALSE,
																									BFALSE);
																							}
																						BgL_arg3266z00_5783 =
																							CDR(BgL_pairz00_5785);
																					}
																				}
																				BGL_EXITD_PROTECT_SET
																					(BgL_exitd1148z00_5773,
																					BgL_arg3266z00_5783);
																				BUNSPEC;
																			}
																		else
																			{	/* Ieee/input.scm 566 */
																				BFALSE;
																			}
																	}
																	{	/* Ieee/input.scm 568 */
																		obj_t BgL_portz00_5786;

																		if (INPUT_PORTP(BgL_ipz00_5769))
																			{	/* Ieee/input.scm 568 */
																				BgL_portz00_5786 = BgL_ipz00_5769;
																			}
																		else
																			{
																				obj_t BgL_auxz00_8947;

																				BgL_auxz00_8947 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string3447z00zz__r4_input_6_10_2z00,
																					BINT(21849L),
																					BGl_string3490z00zz__r4_input_6_10_2z00,
																					BGl_string3450z00zz__r4_input_6_10_2z00,
																					BgL_ipz00_5769);
																				FAILURE(BgL_auxz00_8947, BFALSE,
																					BFALSE);
																			}
																		bgl_close_input_port(BgL_portz00_5786);
																	}
																	BgL_res3313z00_5787 = BgL_tmp1150z00_5775;
																}
															}
														}
													}
											}
										}
									}
									return BINT(BgL_res3313z00_5787);
								}
							}
						}
						break;
					case 4L:

						{	/* Ieee/input.scm 556 */
							obj_t BgL_siza7eza7_3570;

							BgL_siza7eza7_3570 = VECTOR_REF(BgL_opt1324z00_90, 2L);
							{	/* Ieee/input.scm 556 */
								obj_t BgL_offsetz00_3571;

								BgL_offsetz00_3571 = VECTOR_REF(BgL_opt1324z00_90, 3L);
								{	/* Ieee/input.scm 556 */

									{	/* Ieee/input.scm 556 */
										long BgL_res3314z00_5813;

										{	/* Ieee/input.scm 556 */
											obj_t BgL_filez00_5788;
											obj_t BgL_opz00_5789;
											long BgL_siza7eza7_5790;
											long BgL_offsetz00_5791;

											if (STRINGP(BgL_filez00_3562))
												{	/* Ieee/input.scm 556 */
													BgL_filez00_5788 = BgL_filez00_3562;
												}
											else
												{
													obj_t BgL_auxz00_8957;

													BgL_auxz00_8957 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string3447z00zz__r4_input_6_10_2z00,
														BINT(21451L),
														BGl_string3490z00zz__r4_input_6_10_2z00,
														BGl_string3459z00zz__r4_input_6_10_2z00,
														BgL_filez00_3562);
													FAILURE(BgL_auxz00_8957, BFALSE, BFALSE);
												}
											if (OUTPUT_PORTP(BgL_opz00_3563))
												{	/* Ieee/input.scm 556 */
													BgL_opz00_5789 = BgL_opz00_3563;
												}
											else
												{
													obj_t BgL_auxz00_8963;

													BgL_auxz00_8963 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string3447z00zz__r4_input_6_10_2z00,
														BINT(21451L),
														BGl_string3490z00zz__r4_input_6_10_2z00,
														BGl_string3483z00zz__r4_input_6_10_2z00,
														BgL_opz00_3563);
													FAILURE(BgL_auxz00_8963, BFALSE, BFALSE);
												}
											{	/* Ieee/input.scm 556 */
												obj_t BgL_tmpz00_8967;

												if (ELONGP(BgL_siza7eza7_3570))
													{	/* Ieee/input.scm 556 */
														BgL_tmpz00_8967 = BgL_siza7eza7_3570;
													}
												else
													{
														obj_t BgL_auxz00_8970;

														BgL_auxz00_8970 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string3447z00zz__r4_input_6_10_2z00,
															BINT(21451L),
															BGl_string3490z00zz__r4_input_6_10_2z00,
															BGl_string3484z00zz__r4_input_6_10_2z00,
															BgL_siza7eza7_3570);
														FAILURE(BgL_auxz00_8970, BFALSE, BFALSE);
													}
												BgL_siza7eza7_5790 = BELONG_TO_LONG(BgL_tmpz00_8967);
											}
											{	/* Ieee/input.scm 556 */
												obj_t BgL_tmpz00_8975;

												if (ELONGP(BgL_offsetz00_3571))
													{	/* Ieee/input.scm 556 */
														BgL_tmpz00_8975 = BgL_offsetz00_3571;
													}
												else
													{
														obj_t BgL_auxz00_8978;

														BgL_auxz00_8978 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string3447z00zz__r4_input_6_10_2z00,
															BINT(21451L),
															BGl_string3490z00zz__r4_input_6_10_2z00,
															BGl_string3484z00zz__r4_input_6_10_2z00,
															BgL_offsetz00_3571);
														FAILURE(BgL_auxz00_8978, BFALSE, BFALSE);
													}
												BgL_offsetz00_5791 = BELONG_TO_LONG(BgL_tmpz00_8975);
											}
											{	/* Ieee/input.scm 560 */
												long BgL_sza7za7_5792;
												long BgL_offz00_5793;

												BgL_sza7za7_5792 = (long) (BgL_siza7eza7_5790);
												BgL_offz00_5793 = (long) (BgL_offsetz00_5791);
												{	/* Ieee/input.scm 564 */
													obj_t BgL__ortest_1147z00_5794;

													BgL__ortest_1147z00_5794 =
														bgl_sendfile(BgL_filez00_5788, BgL_opz00_5789,
														(long) (BgL_siza7eza7_5790),
														(long) (BgL_offsetz00_5791));
													if (CBOOL(BgL__ortest_1147z00_5794))
														{	/* Ieee/input.scm 564 */
															obj_t BgL_tmpz00_8990;

															if (INTEGERP(BgL__ortest_1147z00_5794))
																{	/* Ieee/input.scm 564 */
																	BgL_tmpz00_8990 = BgL__ortest_1147z00_5794;
																}
															else
																{
																	obj_t BgL_auxz00_8993;

																	BgL_auxz00_8993 =
																		BGl_typezd2errorzd2zz__errorz00
																		(BGl_string3447z00zz__r4_input_6_10_2z00,
																		BINT(21690L),
																		BGl_string3490z00zz__r4_input_6_10_2z00,
																		BGl_string3463z00zz__r4_input_6_10_2z00,
																		BgL__ortest_1147z00_5794);
																	FAILURE(BgL_auxz00_8993, BFALSE, BFALSE);
																}
															BgL_res3314z00_5813 =
																(long) CINT(BgL_tmpz00_8990);
														}
													else
														{	/* Ieee/input.scm 565 */
															obj_t BgL_ipz00_5795;

															{	/* Ieee/input.scm 565 */

																BgL_ipz00_5795 =
																	BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00
																	(BgL_filez00_5788, BTRUE, BINT(5000000L));
															}
															{	/* Ieee/input.scm 566 */
																obj_t BgL_exitd1148z00_5799;

																BgL_exitd1148z00_5799 = BGL_EXITD_TOP_AS_OBJ();
																{	/* Ieee/input.scm 568 */
																	obj_t BgL_zc3z04anonymousza32558ze3z87_6270;

																	BgL_zc3z04anonymousza32558ze3z87_6270 =
																		MAKE_FX_PROCEDURE
																		(BGl_z62zc3z04anonymousza32558ze3ze5zz__r4_input_6_10_2z00,
																		(int) (0L), (int) (1L));
																	PROCEDURE_SET
																		(BgL_zc3z04anonymousza32558ze3z87_6270,
																		(int) (0L), BgL_ipz00_5795);
																	{	/* Ieee/input.scm 566 */
																		obj_t BgL_arg3269z00_5805;

																		{	/* Ieee/input.scm 566 */
																			obj_t BgL_arg3271z00_5806;

																			BgL_arg3271z00_5806 =
																				BGL_EXITD_PROTECT
																				(BgL_exitd1148z00_5799);
																			BgL_arg3269z00_5805 =
																				MAKE_YOUNG_PAIR
																				(BgL_zc3z04anonymousza32558ze3z87_6270,
																				BgL_arg3271z00_5806);
																		}
																		BGL_EXITD_PROTECT_SET(BgL_exitd1148z00_5799,
																			BgL_arg3269z00_5805);
																		BUNSPEC;
																	}
																	{	/* Ieee/input.scm 567 */
																		long BgL_tmp1150z00_5801;

																		{	/* Ieee/input.scm 567 */
																			obj_t BgL_auxz00_9009;

																			if (INPUT_PORTP(BgL_ipz00_5795))
																				{	/* Ieee/input.scm 567 */
																					BgL_auxz00_9009 = BgL_ipz00_5795;
																				}
																			else
																				{
																					obj_t BgL_auxz00_9012;

																					BgL_auxz00_9012 =
																						BGl_typezd2errorzd2zz__errorz00
																						(BGl_string3447z00zz__r4_input_6_10_2z00,
																						BINT(21806L),
																						BGl_string3490z00zz__r4_input_6_10_2z00,
																						BGl_string3450z00zz__r4_input_6_10_2z00,
																						BgL_ipz00_5795);
																					FAILURE(BgL_auxz00_9012, BFALSE,
																						BFALSE);
																				}
																			BgL_tmp1150z00_5801 =
																				BGl_sendzd2charszf2siza7ez87zz__r4_input_6_10_2z00
																				(BgL_auxz00_9009, BgL_opz00_5789,
																				BgL_siza7eza7_5790, BgL_offsetz00_5791);
																		}
																		{	/* Ieee/input.scm 566 */
																			bool_t BgL_test3839z00_9017;

																			{	/* Ieee/input.scm 566 */
																				obj_t BgL_arg3268z00_5808;

																				BgL_arg3268z00_5808 =
																					BGL_EXITD_PROTECT
																					(BgL_exitd1148z00_5799);
																				BgL_test3839z00_9017 =
																					PAIRP(BgL_arg3268z00_5808);
																			}
																			if (BgL_test3839z00_9017)
																				{	/* Ieee/input.scm 566 */
																					obj_t BgL_arg3266z00_5809;

																					{	/* Ieee/input.scm 566 */
																						obj_t BgL_arg3267z00_5810;

																						BgL_arg3267z00_5810 =
																							BGL_EXITD_PROTECT
																							(BgL_exitd1148z00_5799);
																						{	/* Ieee/input.scm 566 */
																							obj_t BgL_pairz00_5811;

																							if (PAIRP(BgL_arg3267z00_5810))
																								{	/* Ieee/input.scm 566 */
																									BgL_pairz00_5811 =
																										BgL_arg3267z00_5810;
																								}
																							else
																								{
																									obj_t BgL_auxz00_9023;

																									BgL_auxz00_9023 =
																										BGl_typezd2errorzd2zz__errorz00
																										(BGl_string3447z00zz__r4_input_6_10_2z00,
																										BINT(21767L),
																										BGl_string3490z00zz__r4_input_6_10_2z00,
																										BGl_string3491z00zz__r4_input_6_10_2z00,
																										BgL_arg3267z00_5810);
																									FAILURE(BgL_auxz00_9023,
																										BFALSE, BFALSE);
																								}
																							BgL_arg3266z00_5809 =
																								CDR(BgL_pairz00_5811);
																						}
																					}
																					BGL_EXITD_PROTECT_SET
																						(BgL_exitd1148z00_5799,
																						BgL_arg3266z00_5809);
																					BUNSPEC;
																				}
																			else
																				{	/* Ieee/input.scm 566 */
																					BFALSE;
																				}
																		}
																		{	/* Ieee/input.scm 568 */
																			obj_t BgL_portz00_5812;

																			if (INPUT_PORTP(BgL_ipz00_5795))
																				{	/* Ieee/input.scm 568 */
																					BgL_portz00_5812 = BgL_ipz00_5795;
																				}
																			else
																				{
																					obj_t BgL_auxz00_9031;

																					BgL_auxz00_9031 =
																						BGl_typezd2errorzd2zz__errorz00
																						(BGl_string3447z00zz__r4_input_6_10_2z00,
																						BINT(21849L),
																						BGl_string3490z00zz__r4_input_6_10_2z00,
																						BGl_string3450z00zz__r4_input_6_10_2z00,
																						BgL_ipz00_5795);
																					FAILURE(BgL_auxz00_9031, BFALSE,
																						BFALSE);
																				}
																			bgl_close_input_port(BgL_portz00_5812);
																		}
																		BgL_res3314z00_5813 = BgL_tmp1150z00_5801;
																	}
																}
															}
														}
												}
											}
										}
										return BINT(BgL_res3314z00_5813);
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* &<@anonymous:2558> */
	obj_t BGl_z62zc3z04anonymousza32558ze3ze5zz__r4_input_6_10_2z00(obj_t
		BgL_envz00_6273)
	{
		{	/* Ieee/input.scm 566 */
			{	/* Ieee/input.scm 568 */
				obj_t BgL_ipz00_6274;

				BgL_ipz00_6274 = PROCEDURE_REF(BgL_envz00_6273, (int) (0L));
				return bgl_close_input_port(((obj_t) BgL_ipz00_6274));
			}
		}

	}



/* &<@anonymous:2558>3319 */
	obj_t BGl_z62zc3z04anonymousza32558ze33319ze5zz__r4_input_6_10_2z00(obj_t
		BgL_envz00_6275)
	{
		{	/* Ieee/input.scm 566 */
			{	/* Ieee/input.scm 568 */
				obj_t BgL_ipz00_6276;

				BgL_ipz00_6276 = PROCEDURE_REF(BgL_envz00_6275, (int) (0L));
				return bgl_close_input_port(((obj_t) BgL_ipz00_6276));
			}
		}

	}



/* &<@anonymous:2558>3320 */
	obj_t BGl_z62zc3z04anonymousza32558ze33320ze5zz__r4_input_6_10_2z00(obj_t
		BgL_envz00_6277)
	{
		{	/* Ieee/input.scm 566 */
			{	/* Ieee/input.scm 568 */
				obj_t BgL_ipz00_6278;

				BgL_ipz00_6278 = PROCEDURE_REF(BgL_envz00_6277, (int) (0L));
				return bgl_close_input_port(((obj_t) BgL_ipz00_6278));
			}
		}

	}



/* send-file */
	BGL_EXPORTED_DEF long BGl_sendzd2filezd2zz__r4_input_6_10_2z00(obj_t
		BgL_filez00_86, obj_t BgL_opz00_87, long BgL_siza7eza7_88,
		long BgL_offsetz00_89)
	{
		{	/* Ieee/input.scm 556 */
			{	/* Ieee/input.scm 560 */
				long BgL_sza7za7_5814;
				long BgL_offz00_5815;

				BgL_sza7za7_5814 = (long) (BgL_siza7eza7_88);
				BgL_offz00_5815 = (long) (BgL_offsetz00_89);
				{	/* Ieee/input.scm 564 */
					obj_t BgL__ortest_1147z00_5816;

					BgL__ortest_1147z00_5816 =
						bgl_sendfile(BgL_filez00_86, BgL_opz00_87,
						(long) (BgL_siza7eza7_88), (long) (BgL_offsetz00_89));
					if (CBOOL(BgL__ortest_1147z00_5816))
						{	/* Ieee/input.scm 564 */
							return (long) CINT(BgL__ortest_1147z00_5816);
						}
					else
						{	/* Ieee/input.scm 565 */
							obj_t BgL_ipz00_5817;

							{	/* Ieee/input.scm 565 */

								BgL_ipz00_5817 =
									BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00
									(BgL_filez00_86, BTRUE, BINT(5000000L));
							}
							{	/* Ieee/input.scm 566 */
								obj_t BgL_exitd1148z00_5821;

								BgL_exitd1148z00_5821 = BGL_EXITD_TOP_AS_OBJ();
								{	/* Ieee/input.scm 568 */
									obj_t BgL_zc3z04anonymousza32558ze3z87_6279;

									BgL_zc3z04anonymousza32558ze3z87_6279 =
										MAKE_FX_PROCEDURE
										(BGl_z62zc3z04anonymousza32558ze33321ze5zz__r4_input_6_10_2z00,
										(int) (0L), (int) (1L));
									PROCEDURE_SET(BgL_zc3z04anonymousza32558ze3z87_6279,
										(int) (0L), BgL_ipz00_5817);
									{	/* Ieee/input.scm 566 */
										obj_t BgL_arg3269z00_5827;

										{	/* Ieee/input.scm 566 */
											obj_t BgL_arg3271z00_5828;

											BgL_arg3271z00_5828 =
												BGL_EXITD_PROTECT(BgL_exitd1148z00_5821);
											BgL_arg3269z00_5827 =
												MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza32558ze3z87_6279,
												BgL_arg3271z00_5828);
										}
										BGL_EXITD_PROTECT_SET(BgL_exitd1148z00_5821,
											BgL_arg3269z00_5827);
										BUNSPEC;
									}
									{	/* Ieee/input.scm 567 */
										long BgL_tmp1150z00_5823;

										{	/* Ieee/input.scm 567 */
											long BgL_res3315z00_5843;

											{	/* Ieee/input.scm 521 */
												long BgL_sza7za7_5833;
												long BgL_offz00_5834;

												BgL_sza7za7_5833 = (long) (BgL_siza7eza7_88);
												BgL_offz00_5834 = (long) (BgL_offsetz00_89);
												{	/* Ieee/input.scm 525 */
													obj_t BgL__ortest_1144z00_5835;

													BgL__ortest_1144z00_5835 =
														bgl_sendchars(
														((obj_t) BgL_ipz00_5817), BgL_opz00_87,
														BgL_sza7za7_5833, BgL_offz00_5834);
													if (CBOOL(BgL__ortest_1144z00_5835))
														{	/* Ieee/input.scm 525 */
															BgL_res3315z00_5843 =
																(long) CINT(BgL__ortest_1144z00_5835);
														}
													else
														{	/* Ieee/input.scm 527 */
															bool_t BgL_test3844z00_9077;

															{	/* Ieee/input.scm 527 */
																bool_t BgL_test3845z00_9078;

																{	/* Ieee/input.scm 527 */
																	obj_t BgL_tmpz00_9079;

																	BgL_tmpz00_9079 = ((obj_t) BgL_ipz00_5817);
																	BgL_test3845z00_9078 =
																		INPUT_GZIP_PORTP(BgL_tmpz00_9079);
																}
																if (BgL_test3845z00_9078)
																	{	/* Ieee/input.scm 527 */
																		if ((BgL_sza7za7_5833 == -1L))
																			{	/* Ieee/input.scm 527 */
																				BgL_test3844z00_9077 =
																					(BgL_offz00_5834 == -1L);
																			}
																		else
																			{	/* Ieee/input.scm 527 */
																				BgL_test3844z00_9077 = ((bool_t) 0);
																			}
																	}
																else
																	{	/* Ieee/input.scm 527 */
																		BgL_test3844z00_9077 = ((bool_t) 0);
																	}
															}
															if (BgL_test3844z00_9077)
																{	/* Ieee/input.scm 527 */
																	BgL_res3315z00_5843 =
																		(long)
																		CINT
																		(BGl_gunza7ipzd2sendcharsz75zz__gunza7ipza7(
																			((obj_t) BgL_ipz00_5817), BgL_opz00_87));
																}
															else
																{	/* Ieee/input.scm 527 */
																	BgL_res3315z00_5843 =
																		(long)
																		(BGl_z52sendcharsz52zz__r4_input_6_10_2z00((
																				(obj_t) BgL_ipz00_5817), BgL_opz00_87,
																			BgL_sza7za7_5833, BgL_offz00_5834));
											}}}}
											BgL_tmp1150z00_5823 = BgL_res3315z00_5843;
										}
										{	/* Ieee/input.scm 566 */
											bool_t BgL_test3847z00_9091;

											{	/* Ieee/input.scm 566 */
												obj_t BgL_arg3268z00_5845;

												BgL_arg3268z00_5845 =
													BGL_EXITD_PROTECT(BgL_exitd1148z00_5821);
												BgL_test3847z00_9091 = PAIRP(BgL_arg3268z00_5845);
											}
											if (BgL_test3847z00_9091)
												{	/* Ieee/input.scm 566 */
													obj_t BgL_arg3266z00_5846;

													{	/* Ieee/input.scm 566 */
														obj_t BgL_arg3267z00_5847;

														BgL_arg3267z00_5847 =
															BGL_EXITD_PROTECT(BgL_exitd1148z00_5821);
														BgL_arg3266z00_5846 =
															CDR(((obj_t) BgL_arg3267z00_5847));
													}
													BGL_EXITD_PROTECT_SET(BgL_exitd1148z00_5821,
														BgL_arg3266z00_5846);
													BUNSPEC;
												}
											else
												{	/* Ieee/input.scm 566 */
													BFALSE;
												}
										}
										bgl_close_input_port(((obj_t) BgL_ipz00_5817));
										return BgL_tmp1150z00_5823;
									}
								}
							}
						}
				}
			}
		}

	}



/* &<@anonymous:2558>3321 */
	obj_t BGl_z62zc3z04anonymousza32558ze33321ze5zz__r4_input_6_10_2z00(obj_t
		BgL_envz00_6280)
	{
		{	/* Ieee/input.scm 566 */
			{	/* Ieee/input.scm 568 */
				obj_t BgL_ipz00_6281;

				BgL_ipz00_6281 = PROCEDURE_REF(BgL_envz00_6280, (int) (0L));
				return bgl_close_input_port(((obj_t) BgL_ipz00_6281));
			}
		}

	}



/* file-lines */
	BGL_EXPORTED_DEF obj_t BGl_filezd2lineszd2zz__r4_input_6_10_2z00(obj_t
		BgL_filez00_92)
	{
		{	/* Ieee/input.scm 580 */
			if (fexists(BSTRING_TO_STRING(BgL_filez00_92)))
				{	/* Ieee/input.scm 605 */
					obj_t BgL_zc3z04anonymousza32561ze3z87_6282;

					BgL_zc3z04anonymousza32561ze3z87_6282 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza32561ze3ze5zz__r4_input_6_10_2z00,
						(int) (0L), (int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza32561ze3z87_6282, (int) (0L),
						BgL_filez00_92);
					return
						BGl_withzd2inputzd2fromzd2filezd2zz__r4_ports_6_10_1z00
						(BgL_filez00_92, BgL_zc3z04anonymousza32561ze3z87_6282);
				}
			else
				{	/* Ieee/input.scm 601 */
					return BFALSE;
				}
		}

	}



/* &file-lines */
	obj_t BGl_z62filezd2lineszb0zz__r4_input_6_10_2z00(obj_t BgL_envz00_6283,
		obj_t BgL_filez00_6284)
	{
		{	/* Ieee/input.scm 580 */
			{	/* Ieee/input.scm 583 */
				obj_t BgL_auxz00_9113;

				if (STRINGP(BgL_filez00_6284))
					{	/* Ieee/input.scm 583 */
						BgL_auxz00_9113 = BgL_filez00_6284;
					}
				else
					{
						obj_t BgL_auxz00_9116;

						BgL_auxz00_9116 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string3447z00zz__r4_input_6_10_2z00, BINT(22416L),
							BGl_string3492z00zz__r4_input_6_10_2z00,
							BGl_string3459z00zz__r4_input_6_10_2z00, BgL_filez00_6284);
						FAILURE(BgL_auxz00_9116, BFALSE, BFALSE);
					}
				return BGl_filezd2lineszd2zz__r4_input_6_10_2z00(BgL_auxz00_9113);
			}
		}

	}



/* &<@anonymous:2561> */
	obj_t BGl_z62zc3z04anonymousza32561ze3ze5zz__r4_input_6_10_2z00(obj_t
		BgL_envz00_6285)
	{
		{	/* Ieee/input.scm 604 */
			{	/* Ieee/input.scm 605 */
				obj_t BgL_filez00_6286;

				BgL_filez00_6286 = ((obj_t) PROCEDURE_REF(BgL_envz00_6285, (int) (0L)));
				{
					obj_t BgL_iportz00_6537;
					long BgL_startz00_6538;
					obj_t BgL_accz00_6539;

					{	/* Ieee/input.scm 605 */
						obj_t BgL_arg2562z00_6578;

						{	/* Ieee/input.scm 605 */
							obj_t BgL_tmpz00_9124;

							BgL_tmpz00_9124 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_arg2562z00_6578 = BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_9124);
						}
						BgL_iportz00_6537 = BgL_arg2562z00_6578;
						BgL_startz00_6538 = 0L;
						BgL_accz00_6539 = BNIL;
						{
							obj_t BgL_iportz00_6570;
							long BgL_lastzd2matchzd2_6571;
							long BgL_forwardz00_6572;
							long BgL_bufposz00_6573;
							obj_t BgL_iportz00_6562;
							long BgL_lastzd2matchzd2_6563;
							long BgL_forwardz00_6564;
							long BgL_bufposz00_6565;

						BgL_ignorez00_6540:
							RGC_START_MATCH(BgL_iportz00_6537);
							{	/* Ieee/input.scm 583 */
								long BgL_matchz00_6544;

								{	/* Ieee/input.scm 583 */
									long BgL_arg2693z00_6545;
									long BgL_arg2694z00_6546;

									BgL_arg2693z00_6545 = RGC_BUFFER_FORWARD(BgL_iportz00_6537);
									BgL_arg2694z00_6546 = RGC_BUFFER_BUFPOS(BgL_iportz00_6537);
									{
										long BgL_forwardz00_6548;
										long BgL_bufposz00_6549;

										BgL_forwardz00_6548 = BgL_arg2693z00_6545;
										BgL_bufposz00_6549 = BgL_arg2694z00_6546;
									BgL_statezd20zd21152z00_6547:
										if ((BgL_forwardz00_6548 == BgL_bufposz00_6549))
											{	/* Ieee/input.scm 583 */
												if (rgc_fill_buffer(BgL_iportz00_6537))
													{	/* Ieee/input.scm 583 */
														long BgL_arg2589z00_6550;
														long BgL_arg2590z00_6551;

														BgL_arg2589z00_6550 =
															RGC_BUFFER_FORWARD(BgL_iportz00_6537);
														BgL_arg2590z00_6551 =
															RGC_BUFFER_BUFPOS(BgL_iportz00_6537);
														{
															long BgL_bufposz00_9137;
															long BgL_forwardz00_9136;

															BgL_forwardz00_9136 = BgL_arg2589z00_6550;
															BgL_bufposz00_9137 = BgL_arg2590z00_6551;
															BgL_bufposz00_6549 = BgL_bufposz00_9137;
															BgL_forwardz00_6548 = BgL_forwardz00_9136;
															goto BgL_statezd20zd21152z00_6547;
														}
													}
												else
													{	/* Ieee/input.scm 583 */
														BgL_matchz00_6544 = 2L;
													}
											}
										else
											{	/* Ieee/input.scm 583 */
												int BgL_curz00_6552;

												BgL_curz00_6552 =
													RGC_BUFFER_GET_CHAR(BgL_iportz00_6537,
													BgL_forwardz00_6548);
												{	/* Ieee/input.scm 583 */

													if (((long) (BgL_curz00_6552) == 10L))
														{	/* Ieee/input.scm 583 */
															long BgL_arg2592z00_6553;

															BgL_arg2592z00_6553 = (1L + BgL_forwardz00_6548);
															{	/* Ieee/input.scm 583 */
																long BgL_newzd2matchzd2_6554;

																RGC_STOP_MATCH(BgL_iportz00_6537,
																	BgL_arg2592z00_6553);
																BgL_newzd2matchzd2_6554 = 0L;
																BgL_matchz00_6544 = BgL_newzd2matchzd2_6554;
														}}
													else
														{	/* Ieee/input.scm 583 */
															BgL_iportz00_6562 = BgL_iportz00_6537;
															BgL_lastzd2matchzd2_6563 = 2L;
															BgL_forwardz00_6564 = (1L + BgL_forwardz00_6548);
															BgL_bufposz00_6565 = BgL_bufposz00_6549;
														BgL_statezd21zd21153z00_6542:
															{	/* Ieee/input.scm 583 */
																long BgL_newzd2matchzd2_6566;

																RGC_STOP_MATCH(BgL_iportz00_6562,
																	BgL_forwardz00_6564);
																BgL_newzd2matchzd2_6566 = 1L;
																if ((BgL_forwardz00_6564 == BgL_bufposz00_6565))
																	{	/* Ieee/input.scm 583 */
																		if (rgc_fill_buffer(BgL_iportz00_6562))
																			{	/* Ieee/input.scm 583 */
																				long BgL_arg2578z00_6567;
																				long BgL_arg2579z00_6568;

																				BgL_arg2578z00_6567 =
																					RGC_BUFFER_FORWARD(BgL_iportz00_6562);
																				BgL_arg2579z00_6568 =
																					RGC_BUFFER_BUFPOS(BgL_iportz00_6562);
																				{
																					long BgL_bufposz00_9152;
																					long BgL_forwardz00_9151;

																					BgL_forwardz00_9151 =
																						BgL_arg2578z00_6567;
																					BgL_bufposz00_9152 =
																						BgL_arg2579z00_6568;
																					BgL_bufposz00_6565 =
																						BgL_bufposz00_9152;
																					BgL_forwardz00_6564 =
																						BgL_forwardz00_9151;
																					goto BgL_statezd21zd21153z00_6542;
																				}
																			}
																		else
																			{	/* Ieee/input.scm 583 */
																				BgL_matchz00_6544 =
																					BgL_newzd2matchzd2_6566;
																			}
																	}
																else
																	{	/* Ieee/input.scm 583 */
																		int BgL_curz00_6569;

																		BgL_curz00_6569 =
																			RGC_BUFFER_GET_CHAR(BgL_iportz00_6562,
																			BgL_forwardz00_6564);
																		{	/* Ieee/input.scm 583 */

																			if (((long) (BgL_curz00_6569) == 10L))
																				{	/* Ieee/input.scm 583 */
																					BgL_matchz00_6544 =
																						BgL_newzd2matchzd2_6566;
																				}
																			else
																				{	/* Ieee/input.scm 583 */
																					BgL_iportz00_6570 = BgL_iportz00_6562;
																					BgL_lastzd2matchzd2_6571 =
																						BgL_newzd2matchzd2_6566;
																					BgL_forwardz00_6572 =
																						(1L + BgL_forwardz00_6564);
																					BgL_bufposz00_6573 =
																						BgL_bufposz00_6565;
																				BgL_statezd24zd21156z00_6543:
																					{	/* Ieee/input.scm 583 */
																						long BgL_newzd2matchzd2_6574;

																						RGC_STOP_MATCH(BgL_iportz00_6570,
																							BgL_forwardz00_6572);
																						BgL_newzd2matchzd2_6574 = 1L;
																						if (
																							(BgL_forwardz00_6572 ==
																								BgL_bufposz00_6573))
																							{	/* Ieee/input.scm 583 */
																								if (rgc_fill_buffer
																									(BgL_iportz00_6570))
																									{	/* Ieee/input.scm 583 */
																										long BgL_arg2567z00_6575;
																										long BgL_arg2568z00_6576;

																										BgL_arg2567z00_6575 =
																											RGC_BUFFER_FORWARD
																											(BgL_iportz00_6570);
																										BgL_arg2568z00_6576 =
																											RGC_BUFFER_BUFPOS
																											(BgL_iportz00_6570);
																										{
																											long BgL_bufposz00_9165;
																											long BgL_forwardz00_9164;

																											BgL_forwardz00_9164 =
																												BgL_arg2567z00_6575;
																											BgL_bufposz00_9165 =
																												BgL_arg2568z00_6576;
																											BgL_bufposz00_6573 =
																												BgL_bufposz00_9165;
																											BgL_forwardz00_6572 =
																												BgL_forwardz00_9164;
																											goto
																												BgL_statezd24zd21156z00_6543;
																										}
																									}
																								else
																									{	/* Ieee/input.scm 583 */
																										BgL_matchz00_6544 =
																											BgL_newzd2matchzd2_6574;
																									}
																							}
																						else
																							{	/* Ieee/input.scm 583 */
																								int BgL_curz00_6577;

																								BgL_curz00_6577 =
																									RGC_BUFFER_GET_CHAR
																									(BgL_iportz00_6570,
																									BgL_forwardz00_6572);
																								{	/* Ieee/input.scm 583 */

																									if (
																										((long) (BgL_curz00_6577) ==
																											10L))
																										{	/* Ieee/input.scm 583 */
																											BgL_matchz00_6544 =
																												BgL_newzd2matchzd2_6574;
																										}
																									else
																										{
																											long BgL_forwardz00_9171;
																											long
																												BgL_lastzd2matchzd2_9170;
																											BgL_lastzd2matchzd2_9170 =
																												BgL_newzd2matchzd2_6574;
																											BgL_forwardz00_9171 =
																												(1L +
																												BgL_forwardz00_6572);
																											BgL_forwardz00_6572 =
																												BgL_forwardz00_9171;
																											BgL_lastzd2matchzd2_6571 =
																												BgL_lastzd2matchzd2_9170;
																											goto
																												BgL_statezd24zd21156z00_6543;
																										}
																								}
																							}
																					}
																				}
																		}
																	}
															}
														}
												}
											}
									}
								}
								RGC_SET_FILEPOS(BgL_iportz00_6537);
								switch (BgL_matchz00_6544)
									{
									case 2L:

										{	/* Ieee/input.scm 593 */
											obj_t BgL_cz00_6555;

											{	/* Ieee/input.scm 583 */
												bool_t BgL_test3859z00_9176;

												{	/* Ieee/input.scm 583 */
													long BgL_arg2680z00_6561;

													BgL_arg2680z00_6561 =
														RGC_BUFFER_MATCH_LENGTH(BgL_iportz00_6537);
													BgL_test3859z00_9176 = (BgL_arg2680z00_6561 == 0L);
												}
												if (BgL_test3859z00_9176)
													{	/* Ieee/input.scm 583 */
														BgL_cz00_6555 = BEOF;
													}
												else
													{	/* Ieee/input.scm 583 */
														BgL_cz00_6555 =
															BCHAR(RGC_BUFFER_CHARACTER(BgL_iportz00_6537));
													}
											}
											if (EOF_OBJECTP(BgL_cz00_6555))
												{	/* Ieee/input.scm 595 */
													long BgL_stopz00_6556;

													BgL_stopz00_6556 =
														INPUT_PORT_FILEPOS(BgL_iportz00_6537);
													if ((BgL_stopz00_6556 > BgL_startz00_6538))
														{	/* Ieee/input.scm 597 */
															obj_t BgL_arg2689z00_6557;

															{	/* Ieee/input.scm 597 */
																obj_t BgL_arg2690z00_6558;

																BgL_arg2690z00_6558 =
																	MAKE_YOUNG_PAIR(BINT(BgL_startz00_6538),
																	BINT(BgL_stopz00_6556));
																BgL_arg2689z00_6557 =
																	MAKE_YOUNG_PAIR(BgL_arg2690z00_6558,
																	BgL_accz00_6539);
															}
															return bgl_reverse_bang(BgL_arg2689z00_6557);
														}
													else
														{	/* Ieee/input.scm 596 */
															return bgl_reverse_bang(BgL_accz00_6539);
														}
												}
											else
												{	/* Ieee/input.scm 594 */
													return
														BGl_errorz00zz__errorz00
														(BGl_symbol3493z00zz__r4_input_6_10_2z00,
														BGl_string3495z00zz__r4_input_6_10_2z00,
														BgL_filez00_6286);
												}
										}
										break;
									case 1L:

										goto BgL_ignorez00_6540;
										break;
									case 0L:

										{	/* Ieee/input.scm 585 */
											long BgL_stopz00_6559;

											BgL_stopz00_6559 = INPUT_PORT_FILEPOS(BgL_iportz00_6537);
											{	/* Ieee/input.scm 585 */
												obj_t BgL_descz00_6560;

												BgL_descz00_6560 =
													MAKE_YOUNG_PAIR(BINT(BgL_startz00_6538),
													BINT(BgL_stopz00_6559));
												{	/* Ieee/input.scm 586 */

													BgL_startz00_6538 = (1L + BgL_stopz00_6559);
													BgL_accz00_6539 =
														MAKE_YOUNG_PAIR(BgL_descz00_6560, BgL_accz00_6539);
													goto BgL_ignorez00_6540;
												}
											}
										}
										break;
									default:
										return
											BGl_errorz00zz__errorz00
											(BGl_string3440z00zz__r4_input_6_10_2z00,
											BGl_string3441z00zz__r4_input_6_10_2z00,
											BINT(BgL_matchz00_6544));
									}
							}
						}
					}
				}
			}
		}

	}



/* file-position->line */
	BGL_EXPORTED_DEF obj_t
		BGl_filezd2positionzd2ze3lineze3zz__r4_input_6_10_2z00(int BgL_posz00_93,
		obj_t BgL_fdescz00_94)
	{
		{	/* Ieee/input.scm 610 */
			if (PAIRP(BgL_fdescz00_94))
				{
					obj_t BgL_flinesz00_5956;
					long BgL_linez00_5957;

					BgL_flinesz00_5956 = BgL_fdescz00_94;
					BgL_linez00_5957 = 1L;
				BgL_loopz00_5955:
					if (NULLP(BgL_flinesz00_5956))
						{	/* Ieee/input.scm 616 */
							return BFALSE;
						}
					else
						{	/* Ieee/input.scm 618 */
							bool_t BgL_test3864z00_9206;

							{	/* Ieee/input.scm 618 */
								long BgL_tmpz00_9207;

								{	/* Ieee/input.scm 618 */
									obj_t BgL_pairz00_5971;

									BgL_pairz00_5971 = CAR(((obj_t) BgL_flinesz00_5956));
									BgL_tmpz00_9207 = (long) CINT(CDR(BgL_pairz00_5971));
								}
								BgL_test3864z00_9206 =
									((long) (BgL_posz00_93) >= BgL_tmpz00_9207);
							}
							if (BgL_test3864z00_9206)
								{	/* Ieee/input.scm 619 */
									obj_t BgL_arg2700z00_5966;
									long BgL_arg2701z00_5967;

									BgL_arg2700z00_5966 = CDR(((obj_t) BgL_flinesz00_5956));
									BgL_arg2701z00_5967 = (BgL_linez00_5957 + 1L);
									{
										long BgL_linez00_9218;
										obj_t BgL_flinesz00_9217;

										BgL_flinesz00_9217 = BgL_arg2700z00_5966;
										BgL_linez00_9218 = BgL_arg2701z00_5967;
										BgL_linez00_5957 = BgL_linez00_9218;
										BgL_flinesz00_5956 = BgL_flinesz00_9217;
										goto BgL_loopz00_5955;
									}
								}
							else
								{	/* Ieee/input.scm 618 */
									return BINT(BgL_linez00_5957);
								}
						}
				}
			else
				{	/* Ieee/input.scm 612 */
					if (STRINGP(BgL_fdescz00_94))
						{	/* Ieee/input.scm 622 */
							if (fexists(BSTRING_TO_STRING(BgL_fdescz00_94)))
								{	/* Ieee/input.scm 638 */
									obj_t BgL_zc3z04anonymousza32706ze3z87_6287;

									BgL_zc3z04anonymousza32706ze3z87_6287 =
										MAKE_FX_PROCEDURE
										(BGl_z62zc3z04anonymousza32706ze3ze5zz__r4_input_6_10_2z00,
										(int) (0L), (int) (1L));
									PROCEDURE_SET(BgL_zc3z04anonymousza32706ze3z87_6287,
										(int) (0L), BINT(BgL_posz00_93));
									return
										BGl_withzd2inputzd2fromzd2filezd2zz__r4_ports_6_10_1z00
										(BgL_fdescz00_94, BgL_zc3z04anonymousza32706ze3z87_6287);
								}
							else
								{	/* Ieee/input.scm 634 */
									return BFALSE;
								}
						}
					else
						{	/* Ieee/input.scm 622 */
							return BFALSE;
						}
				}
		}

	}



/* &file-position->line */
	obj_t BGl_z62filezd2positionzd2ze3linez81zz__r4_input_6_10_2z00(obj_t
		BgL_envz00_6288, obj_t BgL_posz00_6289, obj_t BgL_fdescz00_6290)
	{
		{	/* Ieee/input.scm 610 */
			{	/* Ieee/input.scm 612 */
				int BgL_auxz00_9232;

				{	/* Ieee/input.scm 612 */
					obj_t BgL_tmpz00_9233;

					if (INTEGERP(BgL_posz00_6289))
						{	/* Ieee/input.scm 612 */
							BgL_tmpz00_9233 = BgL_posz00_6289;
						}
					else
						{
							obj_t BgL_auxz00_9236;

							BgL_auxz00_9236 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string3447z00zz__r4_input_6_10_2z00, BINT(23319L),
								BGl_string3496z00zz__r4_input_6_10_2z00,
								BGl_string3463z00zz__r4_input_6_10_2z00, BgL_posz00_6289);
							FAILURE(BgL_auxz00_9236, BFALSE, BFALSE);
						}
					BgL_auxz00_9232 = CINT(BgL_tmpz00_9233);
				}
				return
					BGl_filezd2positionzd2ze3lineze3zz__r4_input_6_10_2z00
					(BgL_auxz00_9232, BgL_fdescz00_6290);
			}
		}

	}



/* &<@anonymous:2706> */
	obj_t BGl_z62zc3z04anonymousza32706ze3ze5zz__r4_input_6_10_2z00(obj_t
		BgL_envz00_6291)
	{
		{	/* Ieee/input.scm 637 */
			{	/* Ieee/input.scm 638 */
				int BgL_posz00_6292;

				BgL_posz00_6292 = CINT(PROCEDURE_REF(BgL_envz00_6291, (int) (0L)));
				{
					obj_t BgL_iportz00_6580;
					obj_t BgL_posz00_6581;
					long BgL_linez00_6582;

					{	/* Ieee/input.scm 638 */
						obj_t BgL_arg2707z00_6614;

						{	/* Ieee/input.scm 638 */
							obj_t BgL_tmpz00_9245;

							BgL_tmpz00_9245 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_arg2707z00_6614 = BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_9245);
						}
						BgL_iportz00_6580 = BgL_arg2707z00_6614;
						BgL_posz00_6581 = BINT(BgL_posz00_6292);
						BgL_linez00_6582 = 1L;
						{
							obj_t BgL_iportz00_6606;
							long BgL_lastzd2matchzd2_6607;
							long BgL_forwardz00_6608;
							long BgL_bufposz00_6609;
							obj_t BgL_iportz00_6598;
							long BgL_lastzd2matchzd2_6599;
							long BgL_forwardz00_6600;
							long BgL_bufposz00_6601;

						BgL_ignorez00_6583:
							RGC_START_MATCH(BgL_iportz00_6580);
							{	/* Ieee/input.scm 623 */
								long BgL_matchz00_6586;

								{	/* Ieee/input.scm 623 */
									long BgL_arg2847z00_6587;
									long BgL_arg2848z00_6588;

									BgL_arg2847z00_6587 = RGC_BUFFER_FORWARD(BgL_iportz00_6580);
									BgL_arg2848z00_6588 = RGC_BUFFER_BUFPOS(BgL_iportz00_6580);
									{
										long BgL_forwardz00_6590;
										long BgL_bufposz00_6591;

										BgL_forwardz00_6590 = BgL_arg2847z00_6587;
										BgL_bufposz00_6591 = BgL_arg2848z00_6588;
									BgL_statezd20zd21159z00_6589:
										if ((BgL_forwardz00_6590 == BgL_bufposz00_6591))
											{	/* Ieee/input.scm 623 */
												if (rgc_fill_buffer(BgL_iportz00_6580))
													{	/* Ieee/input.scm 623 */
														long BgL_arg2728z00_6592;
														long BgL_arg2729z00_6593;

														BgL_arg2728z00_6592 =
															RGC_BUFFER_FORWARD(BgL_iportz00_6580);
														BgL_arg2729z00_6593 =
															RGC_BUFFER_BUFPOS(BgL_iportz00_6580);
														{
															long BgL_bufposz00_9258;
															long BgL_forwardz00_9257;

															BgL_forwardz00_9257 = BgL_arg2728z00_6592;
															BgL_bufposz00_9258 = BgL_arg2729z00_6593;
															BgL_bufposz00_6591 = BgL_bufposz00_9258;
															BgL_forwardz00_6590 = BgL_forwardz00_9257;
															goto BgL_statezd20zd21159z00_6589;
														}
													}
												else
													{	/* Ieee/input.scm 623 */
														BgL_matchz00_6586 = 2L;
													}
											}
										else
											{	/* Ieee/input.scm 623 */
												int BgL_curz00_6594;

												BgL_curz00_6594 =
													RGC_BUFFER_GET_CHAR(BgL_iportz00_6580,
													BgL_forwardz00_6590);
												{	/* Ieee/input.scm 623 */

													if (((long) (BgL_curz00_6594) == 10L))
														{	/* Ieee/input.scm 623 */
															long BgL_arg2731z00_6595;

															BgL_arg2731z00_6595 = (1L + BgL_forwardz00_6590);
															{	/* Ieee/input.scm 623 */
																long BgL_newzd2matchzd2_6596;

																RGC_STOP_MATCH(BgL_iportz00_6580,
																	BgL_arg2731z00_6595);
																BgL_newzd2matchzd2_6596 = 0L;
																BgL_matchz00_6586 = BgL_newzd2matchzd2_6596;
														}}
													else
														{	/* Ieee/input.scm 623 */
															BgL_iportz00_6598 = BgL_iportz00_6580;
															BgL_lastzd2matchzd2_6599 = 2L;
															BgL_forwardz00_6600 = (1L + BgL_forwardz00_6590);
															BgL_bufposz00_6601 = BgL_bufposz00_6591;
														BgL_statezd21zd21160z00_6584:
															{	/* Ieee/input.scm 623 */
																long BgL_newzd2matchzd2_6602;

																RGC_STOP_MATCH(BgL_iportz00_6598,
																	BgL_forwardz00_6600);
																BgL_newzd2matchzd2_6602 = 1L;
																if ((BgL_forwardz00_6600 == BgL_bufposz00_6601))
																	{	/* Ieee/input.scm 623 */
																		if (rgc_fill_buffer(BgL_iportz00_6598))
																			{	/* Ieee/input.scm 623 */
																				long BgL_arg2721z00_6603;
																				long BgL_arg2722z00_6604;

																				BgL_arg2721z00_6603 =
																					RGC_BUFFER_FORWARD(BgL_iportz00_6598);
																				BgL_arg2722z00_6604 =
																					RGC_BUFFER_BUFPOS(BgL_iportz00_6598);
																				{
																					long BgL_bufposz00_9273;
																					long BgL_forwardz00_9272;

																					BgL_forwardz00_9272 =
																						BgL_arg2721z00_6603;
																					BgL_bufposz00_9273 =
																						BgL_arg2722z00_6604;
																					BgL_bufposz00_6601 =
																						BgL_bufposz00_9273;
																					BgL_forwardz00_6600 =
																						BgL_forwardz00_9272;
																					goto BgL_statezd21zd21160z00_6584;
																				}
																			}
																		else
																			{	/* Ieee/input.scm 623 */
																				BgL_matchz00_6586 =
																					BgL_newzd2matchzd2_6602;
																			}
																	}
																else
																	{	/* Ieee/input.scm 623 */
																		int BgL_curz00_6605;

																		BgL_curz00_6605 =
																			RGC_BUFFER_GET_CHAR(BgL_iportz00_6598,
																			BgL_forwardz00_6600);
																		{	/* Ieee/input.scm 623 */

																			if (((long) (BgL_curz00_6605) == 10L))
																				{	/* Ieee/input.scm 623 */
																					BgL_matchz00_6586 =
																						BgL_newzd2matchzd2_6602;
																				}
																			else
																				{	/* Ieee/input.scm 623 */
																					BgL_iportz00_6606 = BgL_iportz00_6598;
																					BgL_lastzd2matchzd2_6607 =
																						BgL_newzd2matchzd2_6602;
																					BgL_forwardz00_6608 =
																						(1L + BgL_forwardz00_6600);
																					BgL_bufposz00_6609 =
																						BgL_bufposz00_6601;
																				BgL_statezd24zd21163z00_6585:
																					{	/* Ieee/input.scm 623 */
																						long BgL_newzd2matchzd2_6610;

																						RGC_STOP_MATCH(BgL_iportz00_6606,
																							BgL_forwardz00_6608);
																						BgL_newzd2matchzd2_6610 = 1L;
																						if (
																							(BgL_forwardz00_6608 ==
																								BgL_bufposz00_6609))
																							{	/* Ieee/input.scm 623 */
																								if (rgc_fill_buffer
																									(BgL_iportz00_6606))
																									{	/* Ieee/input.scm 623 */
																										long BgL_arg2712z00_6611;
																										long BgL_arg2714z00_6612;

																										BgL_arg2712z00_6611 =
																											RGC_BUFFER_FORWARD
																											(BgL_iportz00_6606);
																										BgL_arg2714z00_6612 =
																											RGC_BUFFER_BUFPOS
																											(BgL_iportz00_6606);
																										{
																											long BgL_bufposz00_9286;
																											long BgL_forwardz00_9285;

																											BgL_forwardz00_9285 =
																												BgL_arg2712z00_6611;
																											BgL_bufposz00_9286 =
																												BgL_arg2714z00_6612;
																											BgL_bufposz00_6609 =
																												BgL_bufposz00_9286;
																											BgL_forwardz00_6608 =
																												BgL_forwardz00_9285;
																											goto
																												BgL_statezd24zd21163z00_6585;
																										}
																									}
																								else
																									{	/* Ieee/input.scm 623 */
																										BgL_matchz00_6586 =
																											BgL_newzd2matchzd2_6610;
																									}
																							}
																						else
																							{	/* Ieee/input.scm 623 */
																								int BgL_curz00_6613;

																								BgL_curz00_6613 =
																									RGC_BUFFER_GET_CHAR
																									(BgL_iportz00_6606,
																									BgL_forwardz00_6608);
																								{	/* Ieee/input.scm 623 */

																									if (
																										((long) (BgL_curz00_6613) ==
																											10L))
																										{	/* Ieee/input.scm 623 */
																											BgL_matchz00_6586 =
																												BgL_newzd2matchzd2_6610;
																										}
																									else
																										{
																											long BgL_forwardz00_9292;
																											long
																												BgL_lastzd2matchzd2_9291;
																											BgL_lastzd2matchzd2_9291 =
																												BgL_newzd2matchzd2_6610;
																											BgL_forwardz00_9292 =
																												(1L +
																												BgL_forwardz00_6608);
																											BgL_forwardz00_6608 =
																												BgL_forwardz00_9292;
																											BgL_lastzd2matchzd2_6607 =
																												BgL_lastzd2matchzd2_9291;
																											goto
																												BgL_statezd24zd21163z00_6585;
																										}
																								}
																							}
																					}
																				}
																		}
																	}
															}
														}
												}
											}
									}
								}
								RGC_SET_FILEPOS(BgL_iportz00_6580);
								switch (BgL_matchz00_6586)
									{
									case 2L:

										return BFALSE;
										break;
									case 1L:

										goto BgL_ignorez00_6583;
										break;
									case 0L:

										{	/* Ieee/input.scm 625 */
											bool_t BgL_test3877z00_9297;

											{	/* Ieee/input.scm 625 */
												long BgL_arg2844z00_6597;

												BgL_arg2844z00_6597 =
													INPUT_PORT_FILEPOS(BgL_iportz00_6580);
												BgL_test3877z00_9297 =
													(BgL_arg2844z00_6597 >= (long) CINT(BgL_posz00_6581));
											}
											if (BgL_test3877z00_9297)
												{	/* Ieee/input.scm 625 */
													return BINT(BgL_linez00_6582);
												}
											else
												{	/* Ieee/input.scm 625 */
													BgL_linez00_6582 = (BgL_linez00_6582 + 1L);
													goto BgL_ignorez00_6583;
												}
										}
										break;
									default:
										return
											BGl_errorz00zz__errorz00
											(BGl_string3440z00zz__r4_input_6_10_2z00,
											BGl_string3441z00zz__r4_input_6_10_2z00,
											BINT(BgL_matchz00_6586));
									}
							}
						}
					}
				}
			}
		}

	}



/* _password */
	obj_t BGl__passwordz00zz__r4_input_6_10_2z00(obj_t BgL_env1329z00_97,
		obj_t BgL_opt1328z00_96)
	{
		{	/* Ieee/input.scm 645 */
			{	/* Ieee/input.scm 645 */

				switch (VECTOR_LENGTH(BgL_opt1328z00_96))
					{
					case 0L:

						{	/* Ieee/input.scm 645 */

							return
								bgl_password(BSTRING_TO_STRING
								(BGl_string3453z00zz__r4_input_6_10_2z00));
						}
						break;
					case 1L:

						{	/* Ieee/input.scm 645 */
							obj_t BgL_promptz00_6615;

							BgL_promptz00_6615 = VECTOR_REF(BgL_opt1328z00_96, 0L);
							{	/* Ieee/input.scm 645 */

								{	/* Ieee/input.scm 645 */
									obj_t BgL_promptz00_6616;

									if (STRINGP(BgL_promptz00_6615))
										{	/* Ieee/input.scm 645 */
											BgL_promptz00_6616 = BgL_promptz00_6615;
										}
									else
										{
											obj_t BgL_auxz00_9312;

											BgL_auxz00_9312 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3447z00zz__r4_input_6_10_2z00, BINT(24216L),
												BGl_string3497z00zz__r4_input_6_10_2z00,
												BGl_string3459z00zz__r4_input_6_10_2z00,
												BgL_promptz00_6615);
											FAILURE(BgL_auxz00_9312, BFALSE, BFALSE);
										}
									return bgl_password(BSTRING_TO_STRING(BgL_promptz00_6616));
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* password */
	BGL_EXPORTED_DEF obj_t BGl_passwordz00zz__r4_input_6_10_2z00(obj_t
		BgL_promptz00_95)
	{
		{	/* Ieee/input.scm 645 */
			return bgl_password(BSTRING_TO_STRING(BgL_promptz00_95));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__r4_input_6_10_2z00(void)
	{
		{	/* Ieee/input.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__r4_input_6_10_2z00(void)
	{
		{	/* Ieee/input.scm 14 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__r4_input_6_10_2z00(void)
	{
		{	/* Ieee/input.scm 14 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__r4_input_6_10_2z00(void)
	{
		{	/* Ieee/input.scm 14 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string3498z00zz__r4_input_6_10_2z00));
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L,
				BSTRING_TO_STRING(BGl_string3498z00zz__r4_input_6_10_2z00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string3498z00zz__r4_input_6_10_2z00));
			return
				BGl_modulezd2initializa7ationz75zz__gunza7ipza7(224363699L,
				BSTRING_TO_STRING(BGl_string3498z00zz__r4_input_6_10_2z00));
		}

	}

#ifdef __cplusplus
}
#endif
