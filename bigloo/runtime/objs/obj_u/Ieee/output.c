/*===========================================================================*/
/*   (Ieee/output.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Ieee/output.scm -indent -o objs/obj_u/Ieee/output.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___R4_OUTPUT_6_10_3_TYPE_DEFINITIONS
#define BGL___R4_OUTPUT_6_10_3_TYPE_DEFINITIONS

/* object type definitions */

#endif													// BGL___R4_OUTPUT_6_10_3_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t bgl_write_utf8string(obj_t, obj_t);
	static obj_t BGl_displayzd2pairzd2zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__r4_output_6_10_3z00 =
		BUNSPEC;
	extern obj_t bgl_write_binary_port(obj_t, obj_t);
	extern obj_t symbol_for_read(obj_t);
	extern obj_t bgl_write_ucs2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t bgl_display_obj(obj_t, obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_displayzd2stringzd2zz__r4_output_6_10_3z00(obj_t,
		obj_t);
	extern obj_t bgl_display_ucs2string(obj_t, obj_t);
	static obj_t BGl_z62illegalzd2charzd2repz62zz__r4_output_6_10_3z00(obj_t,
		obj_t);
	extern obj_t bgl_write_output_port(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_writezd2stringzd2zz__r4_output_6_10_3z00(obj_t,
		obj_t);
	extern obj_t bgl_write_cnst(obj_t, obj_t);
	static obj_t BGl_z62displayzd2flonumzb0zz__r4_output_6_10_3z00(obj_t, obj_t,
		obj_t);
	extern obj_t bgl_write_procedure(obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zz__r4_output_6_10_3z00(void);
	extern obj_t bgl_write_char(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	extern obj_t BGl_objectzd2displayzd2zz__objectz00(BgL_objectz00_bglt, obj_t);
	static obj_t BGl_z62writeza2zc0zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zz__r4_output_6_10_3z00(void);
	static obj_t BGl_objectzd2initzd2zz__r4_output_6_10_3z00(void);
	extern obj_t BGl_stringzd2skipzd2zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62displayzd2ucs2stringzb0zz__r4_output_6_10_3z00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_listzd2ze3stringz31zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl__writezd2bytezd2zz__r4_output_6_10_3z00(obj_t, obj_t);
	extern obj_t bgl_write_custom(obj_t, obj_t);
	static obj_t BGl_z62printfz62zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
	extern obj_t bgl_write_bignum(obj_t, obj_t);
	extern obj_t string_append_3(obj_t, obj_t, obj_t);
	extern obj_t bgl_weakptr_data(obj_t);
	static obj_t BGl_z62displayzd2substringzb0zz__r4_output_6_10_3z00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	extern obj_t bgl_write_unknown(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_printz00zz__r4_output_6_10_3z00(obj_t);
	extern obj_t BGl_tvectorzd2refzd2zz__tvectorz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_writezd2bytezd2zz__r4_output_6_10_3z00(unsigned
		char, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_writezd2ucs2stringzd2zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_z62writezd2bytezd22z62zz__r4_output_6_10_3z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62writezd2symbolzb0zz__r4_output_6_10_3z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_displayzd2flonumzd2zz__r4_output_6_10_3z00(obj_t,
		obj_t);
	static obj_t BGl_z62newlinezd21zb0zz__r4_output_6_10_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_fprintfz00zz__r4_output_6_10_3z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62displayzd2elongzb0zz__r4_output_6_10_3z00(obj_t, obj_t,
		obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_writez00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_z62writezd22zb0zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62writezd2charzd22z62zz__r4_output_6_10_3z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_methodzd2initzd2zz__r4_output_6_10_3z00(void);
	static obj_t BGl_writezf2displayzd2structurez20zz__r4_output_6_10_3z00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_objectzd2writezd2zz__objectz00(BgL_objectz00_bglt, obj_t);
	extern obj_t bgl_write_input_port(obj_t, obj_t);
	extern obj_t BGl_tvectorzd2idzd2zz__tvectorz00(obj_t);
	extern obj_t bgl_display_llong(BGL_LONGLONG_T, obj_t);
	static obj_t BGl_z62printz62zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_z62displayz62zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
	static obj_t BGl_tprintzd2mutexzd2zz__r4_output_6_10_3z00 = BUNSPEC;
	extern obj_t make_string(long, unsigned char);
	extern obj_t bgl_write_mmap(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_newlinezd21zd2zz__r4_output_6_10_3z00(obj_t);
	extern obj_t BGl_openzd2outputzd2stringz00zz__r4_ports_6_10_1z00(obj_t);
	extern obj_t bgl_write_dynamic_env(obj_t, obj_t);
	static obj_t BGl_z62writez62zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_fprintz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	extern obj_t BGl_datezd2ze3stringz31zz__datez00(obj_t);
	extern obj_t bgl_ill_char_rep(unsigned char);
	static obj_t BGl_z62displayza2zc0zz__r4_output_6_10_3z00(obj_t, obj_t);
	extern obj_t bgl_display_bignum(obj_t, obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t bgl_close_output_port(obj_t);
	static obj_t BGl_z62formatz62zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62writezd2ucs2stringzb0zz__r4_output_6_10_3z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_newlinez00zz__r4_output_6_10_3z00(obj_t);
	extern obj_t bgl_write_llong(BGL_LONGLONG_T, obj_t);
	extern obj_t bgl_display_elong(long, obj_t);
	extern bool_t BGl_bigloozd2strictzd2r5rszd2stringszd2zz__paramz00(void);
	extern obj_t bgl_write_datagram_socket(obj_t, obj_t);
	static obj_t BGl_z62writezd2stringzb0zz__r4_output_6_10_3z00(obj_t, obj_t,
		obj_t);
	extern obj_t string_for_read(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__srfi4z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_z62displayzd2fixnumzb0zz__r4_output_6_10_3z00(obj_t, obj_t,
		obj_t);
	extern obj_t bgl_display_ucs2(obj_t, obj_t);
	extern obj_t bgl_write_socket(obj_t, obj_t);
	extern obj_t ucs2_string_to_utf8_string(obj_t);
	extern obj_t bgl_write_elong(long, obj_t);
	extern bool_t BGl_classzf3zf3zz__objectz00(obj_t);
	extern bool_t BGl_numberzf3zf3zz__r4_numbers_6_5z00(obj_t);
	static obj_t BGl__writezd2charzd2zz__r4_output_6_10_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_displayzd2ucs2stringzd2zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_writezf2displayzd2hvectorz20zz__r4_output_6_10_3z00(obj_t,
		obj_t, obj_t);
	extern obj_t bgl_write_process(obj_t, obj_t);
	static obj_t BGl_z62displayzd2symbolzb0zz__r4_output_6_10_3z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_tprintz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_displayzd2elongzd2zz__r4_output_6_10_3z00(long,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zz__r4_output_6_10_3z00(void);
	extern obj_t BGl_stringzd2indexzd2zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__r4_output_6_10_3z00(void);
	static obj_t BGl_xprintfz00zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_gczd2rootszd2initz00zz__r4_output_6_10_3z00(void);
	BGL_EXPORTED_DECL obj_t BGl_writezd2charzd2zz__r4_output_6_10_3z00(unsigned
		char, obj_t);
	extern obj_t bgl_display_fixnum(obj_t, obj_t);
	static obj_t BGl_symbol2313z00zz__r4_output_6_10_3z00 = BUNSPEC;
	extern obj_t bgl_write_semaphore(obj_t, obj_t);
	static obj_t BGl_symbol2315z00zz__r4_output_6_10_3z00 = BUNSPEC;
	extern obj_t BGl_homogeneouszd2vectorzd2infoz00zz__srfi4z00(obj_t);
	extern obj_t bgl_write_foreign(obj_t, obj_t);
	extern obj_t BGl_displayzd2circlezd2zz__pp_circlez00(obj_t, obj_t);
	extern obj_t BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_symbol2321z00zz__r4_output_6_10_3z00 = BUNSPEC;
	static obj_t BGl_writezd2pairzd2zz__r4_output_6_10_3z00(obj_t, obj_t);
	extern obj_t BGl_writezd2circlezd2zz__pp_circlez00(obj_t, obj_t);
	static obj_t BGl_z62fprintz62zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_displayzd2fixnumzd2zz__r4_output_6_10_3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_displayza2za2zz__r4_output_6_10_3z00(obj_t);
	extern obj_t BGl_classzd2namezd2zz__objectz00(obj_t);
	extern obj_t bgl_write_regexp(obj_t, obj_t);
	extern obj_t bgl_write_opaque(obj_t, obj_t);
	extern obj_t bgl_write_string(obj_t, bool_t, obj_t);
	static obj_t BGl_z62fprintfz62zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t c_substring(obj_t, long, long);
	BGL_EXPORTED_DECL obj_t bgl_write_obj(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_displayzd2symbolzd2zz__r4_output_6_10_3z00(obj_t,
		obj_t);
	extern obj_t BGl_bigloozd2typezd2errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2275z00zz__r4_output_6_10_3z00 = BUNSPEC;
	static obj_t BGl_symbol2279z00zz__r4_output_6_10_3z00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_writezd2bytezd22z00zz__r4_output_6_10_3z00(unsigned char, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_writezd2symbolzd2zz__r4_output_6_10_3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_writeza2za2zz__r4_output_6_10_3z00(obj_t);
	static obj_t BGl_printzd2flatzd2listze70ze7zz__r4_output_6_10_3z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_symbol2282z00zz__r4_output_6_10_3z00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_displayzd2substringzd2zz__r4_output_6_10_3z00(obj_t, long, long, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_writezd2charzd22z00zz__r4_output_6_10_3z00(unsigned char, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_displayz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_illegalzd2charzd2repz00zz__r4_output_6_10_3z00(unsigned char);
	static obj_t BGl_z62displayzd2stringzb0zz__r4_output_6_10_3z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_writezf2displayzd2tvectorz20zz__r4_output_6_10_3z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_printfz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	extern obj_t bgl_real_to_string(double);
	static obj_t BGl_z62displayzd22zb0zz__r4_output_6_10_3z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62tprintz62zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
	static obj_t BGl_writezf2displayzd2vectorz20zz__r4_output_6_10_3z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62newlinez62zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_writezd22zd2envz00zz__r4_output_6_10_3z00,
		BgL_bgl_za762writeza7d22za7b0za72335z00,
		BGl_z62writezd22zb0zz__r4_output_6_10_3z00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2300z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2300za700za7za7_2336za7, "#<output_procedure_port>", 24);
	      DEFINE_STRING(BGl_string2301z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2301za700za7za7_2337za7, "#<weakptr:", 10);
	      DEFINE_STRING(BGl_string2302z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2302za700za7za7_2338za7, "#<date:", 7);
	      DEFINE_STRING(BGl_string2303z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2303za700za7za7_2339za7, "#s8:", 4);
	      DEFINE_STRING(BGl_string2304z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2304za700za7za7_2340za7, "#u8:", 4);
	      DEFINE_STRING(BGl_string2305z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2305za700za7za7_2341za7, "#s16:", 5);
	      DEFINE_STRING(BGl_string2306z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2306za700za7za7_2342za7, "#u16:", 5);
	      DEFINE_STRING(BGl_string2307z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2307za700za7za7_2343za7, "#s32:", 5);
	      DEFINE_STRING(BGl_string2308z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2308za700za7za7_2344za7, "#u32:", 5);
	      DEFINE_STRING(BGl_string2309z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2309za700za7za7_2345za7, "#s64:", 5);
	      DEFINE_STRING(BGl_string2310z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2310za700za7za7_2346za7, "#u64:", 5);
	      DEFINE_STRING(BGl_string2311z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2311za700za7za7_2347za7, "&display-symbol", 15);
	      DEFINE_STRING(BGl_string2312z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2312za700za7za7_2348za7, "symbol", 6);
	      DEFINE_STRING(BGl_string2314z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2314za700za7za7_2349za7, "+", 1);
	      DEFINE_STRING(BGl_string2316z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2316za700za7za7_2350za7, "-", 1);
	      DEFINE_STRING(BGl_string2317z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2317za700za7za7_2351za7, "|", 1);
	      DEFINE_STRING(BGl_string2318z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2318za700za7za7_2352za7, "&write-symbol", 13);
	      DEFINE_STRING(BGl_string2319z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2319za700za7za7_2353za7, "&display-string", 15);
	      DEFINE_STRING(BGl_string2320z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2320za700za7za7_2354za7, "Illegal index, start=~a end=~a",
		30);
	      DEFINE_STRING(BGl_string2322z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2322za700za7za7_2355za7, "display-substring", 17);
	      DEFINE_STRING(BGl_string2323z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2323za700za7za7_2356za7, "&display-substring", 18);
	      DEFINE_STRING(BGl_string2324z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2324za700za7za7_2357za7, "&write-string", 13);
	      DEFINE_STRING(BGl_string2325z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2325za700za7za7_2358za7, "&display-fixnum", 15);
	      DEFINE_STRING(BGl_string2244z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2244za700za7za7_2359za7, "tprint", 6);
	      DEFINE_STRING(BGl_string2326z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2326za700za7za7_2360za7, "&display-elong", 14);
	      DEFINE_STRING(BGl_string2245z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2245za700za7za7_2361za7, "newline", 7);
	      DEFINE_STRING(BGl_string2327z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2327za700za7za7_2362za7, "belong", 6);
	      DEFINE_STRING(BGl_string2246z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2246za700za7za7_2363za7, "wrong number of optional arguments",
		34);
	      DEFINE_STRING(BGl_string2328z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2328za700za7za7_2364za7, "&display-flonum", 15);
	      DEFINE_STRING(BGl_string2247z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2247za700za7za7_2365za7,
		"/tmp/bigloo/runtime/Ieee/output.scm", 35);
	      DEFINE_STRING(BGl_string2329z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2329za700za7za7_2366za7, "real", 4);
	      DEFINE_STRING(BGl_string2248z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2248za700za7za7_2367za7, "&newline-1", 10);
	      DEFINE_STRING(BGl_string2249z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2249za700za7za7_2368za7, "output-port", 11);
	      DEFINE_STRING(BGl_string2330z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2330za700za7za7_2369za7, "&display-ucs2string", 19);
	      DEFINE_STRING(BGl_string2331z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2331za700za7za7_2370za7, "ucs2string", 10);
	      DEFINE_STRING(BGl_string2250z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2250za700za7za7_2371za7, "display", 7);
	      DEFINE_STRING(BGl_string2332z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2332za700za7za7_2372za7, "&write-ucs2string", 17);
	      DEFINE_STRING(BGl_string2251z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2251za700za7za7_2373za7, "write", 5);
	      DEFINE_STRING(BGl_string2333z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2333za700za7za7_2374za7, "...)", 4);
	      DEFINE_STRING(BGl_string2252z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2252za700za7za7_2375za7, "_write-char", 11);
	      DEFINE_STRING(BGl_string2334z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2334za700za7za7_2376za7, "__r4_output_6_10_3", 18);
	      DEFINE_STRING(BGl_string2253z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2253za700za7za7_2377za7, "bchar", 5);
	      DEFINE_STRING(BGl_string2254z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2254za700za7za7_2378za7, "&write-char-2", 13);
	      DEFINE_STRING(BGl_string2255z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2255za700za7za7_2379za7, "_write-byte", 11);
	      DEFINE_STRING(BGl_string2256z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2256za700za7za7_2380za7, "bint", 4);
	      DEFINE_STRING(BGl_string2257z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2257za700za7za7_2381za7, "&write-byte-2", 13);
	      DEFINE_STRING(BGl_string2258z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2258za700za7za7_2382za7, "#Newline", 8);
	      DEFINE_STRING(BGl_string2259z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2259za700za7za7_2383za7, "#Return", 7);
	      DEFINE_STRING(BGl_string2260z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2260za700za7za7_2384za7, "#Space", 6);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_writezd2bytezd22zd2envzd2zz__r4_output_6_10_3z00,
		BgL_bgl_za762writeza7d2byteza72385za7,
		BGl_z62writezd2bytezd22z62zz__r4_output_6_10_3z00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2261z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2261za700za7za7_2386za7, "#Tab", 4);
	      DEFINE_STRING(BGl_string2262z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2262za700za7za7_2387za7, "&illegal-char-rep", 17);
	      DEFINE_STRING(BGl_string2263z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2263za700za7za7_2388za7, "&tprint", 7);
	      DEFINE_STRING(BGl_string2264z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2264za700za7za7_2389za7, "&fprint", 7);
	      DEFINE_STRING(BGl_string2265z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2265za700za7za7_2390za7, "Insufficient number of arguments",
		32);
	      DEFINE_STRING(BGl_string2266z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2266za700za7za7_2391za7, "Illegal tag \"", 13);
	      DEFINE_STRING(BGl_string2267z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2267za700za7za7_2392za7, "\"", 1);
	      DEFINE_STRING(BGl_string2268z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2268za700za7za7_2393za7, "Illegal char", 12);
	      DEFINE_STRING(BGl_string2269z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2269za700za7za7_2394za7, "number", 6);
	      DEFINE_STRING(BGl_string2270z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2270za700za7za7_2395za7, " ", 1);
	      DEFINE_STRING(BGl_string2271z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2271za700za7za7_2396za7, "Illegal tag", 11);
	      DEFINE_STRING(BGl_string2272z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2272za700za7za7_2397za7, "0123456789", 10);
	      DEFINE_STRING(BGl_string2273z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2273za700za7za7_2398za7, "Tag not allowed here", 20);
	      DEFINE_STRING(BGl_string2274z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2274za700za7za7_2399za7, " . ", 3);
	      DEFINE_STRING(BGl_string2276z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2276za700za7za7_2400za7, "format", 6);
	      DEFINE_STRING(BGl_string2277z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2277za700za7za7_2401za7, "&format", 7);
	      DEFINE_STRING(BGl_string2278z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2278za700za7za7_2402za7, "bstring", 7);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_formatzd2envzd2zz__r4_output_6_10_3z00,
		BgL_bgl_za762formatza762za7za7__2403z00, va_generic_entry,
		BGl_z62formatz62zz__r4_output_6_10_3z00, BUNSPEC, -2);
	      DEFINE_STRING(BGl_string2280z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2280za700za7za7_2404za7, "printf", 6);
	      DEFINE_STRING(BGl_string2281z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2281za700za7za7_2405za7, "&printf", 7);
	      DEFINE_STRING(BGl_string2283z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2283za700za7za7_2406za7, "fprintf", 7);
	      DEFINE_STRING(BGl_string2284z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2284za700za7za7_2407za7, "&fprintf", 8);
	      DEFINE_STRING(BGl_string2285z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2285za700za7za7_2408za7, "()", 2);
	      DEFINE_STRING(BGl_string2286z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2286za700za7za7_2409za7, "#f", 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_printfzd2envzd2zz__r4_output_6_10_3z00,
		BgL_bgl_za762printfza762za7za7__2410z00, va_generic_entry,
		BGl_z62printfz62zz__r4_output_6_10_3z00, BUNSPEC, -2);
	      DEFINE_STRING(BGl_string2287z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2287za700za7za7_2411za7, "#t", 2);
	      DEFINE_STRING(BGl_string2288z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2288za700za7za7_2412za7, "#unspecified", 12);
	      DEFINE_STRING(BGl_string2289z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2289za700za7za7_2413za7, "#<class:", 8);
	      DEFINE_STRING(BGl_string2290z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2290za700za7za7_2414za7, ">", 1);
	      DEFINE_STRING(BGl_string2291z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2291za700za7za7_2415za7, "#<mutex:", 8);
	      DEFINE_STRING(BGl_string2292z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2292za700za7za7_2416za7, ":", 1);
	      DEFINE_STRING(BGl_string2293z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2293za700za7za7_2417za7, "#<condition-variable:", 21);
	      DEFINE_STRING(BGl_string2294z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2294za700za7za7_2418za7, "#<cell:", 7);
	      DEFINE_STRING(BGl_string2295z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2295za700za7za7_2419za7, "#eof-object", 11);
	      DEFINE_STRING(BGl_string2296z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2296za700za7za7_2420za7, "#!optional", 10);
	      DEFINE_STRING(BGl_string2297z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2297za700za7za7_2421za7, "#!rest", 6);
	      DEFINE_STRING(BGl_string2298z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2298za700za7za7_2422za7, "#!key", 5);
	      DEFINE_STRING(BGl_string2299z00zz__r4_output_6_10_3z00,
		BgL_bgl_string2299za700za7za7_2423za7, "#<output_string_port>", 21);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_displayzd2stringzd2envz00zz__r4_output_6_10_3z00,
		BgL_bgl_za762displayza7d2str2424z00,
		BGl_z62displayzd2stringzb0zz__r4_output_6_10_3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_displayzd2substringzd2envz00zz__r4_output_6_10_3z00,
		BgL_bgl_za762displayza7d2sub2425z00,
		BGl_z62displayzd2substringzb0zz__r4_output_6_10_3z00, 0L, BUNSPEC, 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_newlinezd21zd2envz00zz__r4_output_6_10_3z00,
		BgL_bgl_za762newlineza7d21za7b2426za7,
		BGl_z62newlinezd21zb0zz__r4_output_6_10_3z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_fprintfzd2envzd2zz__r4_output_6_10_3z00,
		BgL_bgl_za762fprintfza762za7za7_2427z00, va_generic_entry,
		BGl_z62fprintfz62zz__r4_output_6_10_3z00, BUNSPEC, -3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_writezd2charzd2envz00zz__r4_output_6_10_3z00,
		BgL_bgl__writeza7d2charza7d22428z00, opt_generic_entry,
		BGl__writezd2charzd2zz__r4_output_6_10_3z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_writeza2zd2envz70zz__r4_output_6_10_3z00,
		BgL_bgl_za762writeza7a2za7c0za7za72429za7, va_generic_entry,
		BGl_z62writeza2zc0zz__r4_output_6_10_3z00, BUNSPEC, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_writezd2charzd22zd2envzd2zz__r4_output_6_10_3z00,
		BgL_bgl_za762writeza7d2charza72430za7,
		BGl_z62writezd2charzd22z62zz__r4_output_6_10_3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_displayzd2ucs2stringzd2envz00zz__r4_output_6_10_3z00,
		BgL_bgl_za762displayza7d2ucs2431z00,
		BGl_z62displayzd2ucs2stringzb0zz__r4_output_6_10_3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_writezd2symbolzd2envz00zz__r4_output_6_10_3z00,
		BgL_bgl_za762writeza7d2symbo2432z00,
		BGl_z62writezd2symbolzb0zz__r4_output_6_10_3z00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_displayzd2envzd2zz__r4_output_6_10_3z00,
		BgL_bgl_za762displayza762za7za7_2433z00, bgl_va_stack_entry,
		BGl_z62displayz62zz__r4_output_6_10_3z00, BUNSPEC, -2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_displayzd2elongzd2envz00zz__r4_output_6_10_3z00,
		BgL_bgl_za762displayza7d2elo2434z00,
		BGl_z62displayzd2elongzb0zz__r4_output_6_10_3z00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_writezd2envzd2zz__r4_output_6_10_3z00,
		BgL_bgl_za762writeza762za7za7__r2435z00, bgl_va_stack_entry,
		BGl_z62writez62zz__r4_output_6_10_3z00, BUNSPEC, -2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_displayzd22zd2envz00zz__r4_output_6_10_3z00,
		BgL_bgl_za762displayza7d22za7b2436za7,
		BGl_z62displayzd22zb0zz__r4_output_6_10_3z00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_newlinezd2envzd2zz__r4_output_6_10_3z00,
		BgL_bgl_za762newlineza762za7za7_2437z00, va_generic_entry,
		BGl_z62newlinez62zz__r4_output_6_10_3z00, BUNSPEC, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_displayza2zd2envz70zz__r4_output_6_10_3z00,
		BgL_bgl_za762displayza7a2za7c02438za7, va_generic_entry,
		BGl_z62displayza2zc0zz__r4_output_6_10_3z00, BUNSPEC, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_tprintzd2envzd2zz__r4_output_6_10_3z00,
		BgL_bgl_za762tprintza762za7za7__2439z00, va_generic_entry,
		BGl_z62tprintz62zz__r4_output_6_10_3z00, BUNSPEC, -2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_writezd2ucs2stringzd2envz00zz__r4_output_6_10_3z00,
		BgL_bgl_za762writeza7d2ucs2s2440z00,
		BGl_z62writezd2ucs2stringzb0zz__r4_output_6_10_3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_writezd2bytezd2envz00zz__r4_output_6_10_3z00,
		BgL_bgl__writeza7d2byteza7d22441z00, opt_generic_entry,
		BGl__writezd2bytezd2zz__r4_output_6_10_3z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_displayzd2flonumzd2envz00zz__r4_output_6_10_3z00,
		BgL_bgl_za762displayza7d2flo2442z00,
		BGl_z62displayzd2flonumzb0zz__r4_output_6_10_3z00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_fprintzd2envzd2zz__r4_output_6_10_3z00,
		BgL_bgl_za762fprintza762za7za7__2443z00, va_generic_entry,
		BGl_z62fprintz62zz__r4_output_6_10_3z00, BUNSPEC, -2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_printzd2envzd2zz__r4_output_6_10_3z00,
		BgL_bgl_za762printza762za7za7__r2444z00, bgl_va_stack_entry,
		BGl_z62printz62zz__r4_output_6_10_3z00, BUNSPEC, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_displayzd2symbolzd2envz00zz__r4_output_6_10_3z00,
		BgL_bgl_za762displayza7d2sym2445z00,
		BGl_z62displayzd2symbolzb0zz__r4_output_6_10_3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_writezd2stringzd2envz00zz__r4_output_6_10_3z00,
		BgL_bgl_za762writeza7d2strin2446z00,
		BGl_z62writezd2stringzb0zz__r4_output_6_10_3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_illegalzd2charzd2repzd2envzd2zz__r4_output_6_10_3z00,
		BgL_bgl_za762illegalza7d2cha2447z00,
		BGl_z62illegalzd2charzd2repz62zz__r4_output_6_10_3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_displayzd2fixnumzd2envz00zz__r4_output_6_10_3z00,
		BgL_bgl_za762displayza7d2fix2448z00,
		BGl_z62displayzd2fixnumzb0zz__r4_output_6_10_3z00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__r4_output_6_10_3z00));
		     ADD_ROOT((void *) (&BGl_tprintzd2mutexzd2zz__r4_output_6_10_3z00));
		     ADD_ROOT((void *) (&BGl_symbol2313z00zz__r4_output_6_10_3z00));
		     ADD_ROOT((void *) (&BGl_symbol2315z00zz__r4_output_6_10_3z00));
		     ADD_ROOT((void *) (&BGl_symbol2321z00zz__r4_output_6_10_3z00));
		     ADD_ROOT((void *) (&BGl_symbol2275z00zz__r4_output_6_10_3z00));
		     ADD_ROOT((void *) (&BGl_symbol2279z00zz__r4_output_6_10_3z00));
		     ADD_ROOT((void *) (&BGl_symbol2282z00zz__r4_output_6_10_3z00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long
		BgL_checksumz00_4005, char *BgL_fromz00_4006)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__r4_output_6_10_3z00))
				{
					BGl_requirezd2initializa7ationz75zz__r4_output_6_10_3z00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__r4_output_6_10_3z00();
					BGl_cnstzd2initzd2zz__r4_output_6_10_3z00();
					BGl_importedzd2moduleszd2initz00zz__r4_output_6_10_3z00();
					return BGl_toplevelzd2initzd2zz__r4_output_6_10_3z00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__r4_output_6_10_3z00(void)
	{
		{	/* Ieee/output.scm 24 */
			BGl_symbol2275z00zz__r4_output_6_10_3z00 =
				bstring_to_symbol(BGl_string2276z00zz__r4_output_6_10_3z00);
			BGl_symbol2279z00zz__r4_output_6_10_3z00 =
				bstring_to_symbol(BGl_string2280z00zz__r4_output_6_10_3z00);
			BGl_symbol2282z00zz__r4_output_6_10_3z00 =
				bstring_to_symbol(BGl_string2283z00zz__r4_output_6_10_3z00);
			BGl_symbol2313z00zz__r4_output_6_10_3z00 =
				bstring_to_symbol(BGl_string2314z00zz__r4_output_6_10_3z00);
			BGl_symbol2315z00zz__r4_output_6_10_3z00 =
				bstring_to_symbol(BGl_string2316z00zz__r4_output_6_10_3z00);
			return (BGl_symbol2321z00zz__r4_output_6_10_3z00 =
				bstring_to_symbol(BGl_string2322z00zz__r4_output_6_10_3z00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__r4_output_6_10_3z00(void)
	{
		{	/* Ieee/output.scm 24 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__r4_output_6_10_3z00(void)
	{
		{	/* Ieee/output.scm 24 */
			return (BGl_tprintzd2mutexzd2zz__r4_output_6_10_3z00 =
				bgl_make_mutex(BGl_string2244z00zz__r4_output_6_10_3z00), BUNSPEC);
		}

	}



/* newline */
	BGL_EXPORTED_DEF obj_t BGl_newlinez00zz__r4_output_6_10_3z00(obj_t
		BgL_portz00_3)
	{
		{	/* Ieee/output.scm 259 */
			{	/* Ieee/output.scm 260 */
				obj_t BgL_portz00_1613;

				if (NULLP(BgL_portz00_3))
					{	/* Ieee/output.scm 262 */
						obj_t BgL_tmpz00_4024;

						BgL_tmpz00_4024 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_portz00_1613 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4024);
					}
				else
					{	/* Ieee/output.scm 260 */
						if (PAIRP(BgL_portz00_3))
							{	/* Ieee/output.scm 260 */
								if (NULLP(CDR(((obj_t) BgL_portz00_3))))
									{	/* Ieee/output.scm 260 */
										BgL_portz00_1613 = CAR(BgL_portz00_3);
									}
								else
									{	/* Ieee/output.scm 260 */
										BgL_portz00_1613 =
											BGl_errorz00zz__errorz00
											(BGl_string2245z00zz__r4_output_6_10_3z00,
											BGl_string2246z00zz__r4_output_6_10_3z00, BgL_portz00_3);
									}
							}
						else
							{	/* Ieee/output.scm 260 */
								BgL_portz00_1613 =
									BGl_errorz00zz__errorz00
									(BGl_string2245z00zz__r4_output_6_10_3z00,
									BGl_string2246z00zz__r4_output_6_10_3z00, BgL_portz00_3);
							}
					}
				{	/* Ieee/output.scm 275 */
					obj_t BgL_tmpz00_4036;

					BgL_tmpz00_4036 = ((obj_t) BgL_portz00_1613);
					return bgl_display_char(((unsigned char) 10), BgL_tmpz00_4036);
		}}}

	}



/* &newline */
	obj_t BGl_z62newlinez62zz__r4_output_6_10_3z00(obj_t BgL_envz00_3844,
		obj_t BgL_portz00_3845)
	{
		{	/* Ieee/output.scm 259 */
			return BGl_newlinez00zz__r4_output_6_10_3z00(BgL_portz00_3845);
		}

	}



/* newline-1 */
	BGL_EXPORTED_DEF obj_t BGl_newlinezd21zd2zz__r4_output_6_10_3z00(obj_t
		BgL_portz00_4)
	{
		{	/* Ieee/output.scm 274 */
			return bgl_display_char(((unsigned char) 10), BgL_portz00_4);
		}

	}



/* &newline-1 */
	obj_t BGl_z62newlinezd21zb0zz__r4_output_6_10_3z00(obj_t BgL_envz00_3846,
		obj_t BgL_portz00_3847)
	{
		{	/* Ieee/output.scm 274 */
			{	/* Ieee/output.scm 275 */
				obj_t BgL_auxz00_4041;

				if (OUTPUT_PORTP(BgL_portz00_3847))
					{	/* Ieee/output.scm 275 */
						BgL_auxz00_4041 = BgL_portz00_3847;
					}
				else
					{
						obj_t BgL_auxz00_4044;

						BgL_auxz00_4044 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2247z00zz__r4_output_6_10_3z00, BINT(10587L),
							BGl_string2248z00zz__r4_output_6_10_3z00,
							BGl_string2249z00zz__r4_output_6_10_3z00, BgL_portz00_3847);
						FAILURE(BgL_auxz00_4044, BFALSE, BFALSE);
					}
				return BGl_newlinezd21zd2zz__r4_output_6_10_3z00(BgL_auxz00_4041);
			}
		}

	}



/* display */
	BGL_EXPORTED_DEF obj_t BGl_displayz00zz__r4_output_6_10_3z00(obj_t
		BgL_objz00_5, obj_t BgL_portz00_6)
	{
		{	/* Ieee/output.scm 280 */
			{	/* Ieee/output.scm 281 */
				obj_t BgL_portz00_1623;

				if (NULLP(BgL_portz00_6))
					{	/* Ieee/output.scm 283 */
						obj_t BgL_tmpz00_4051;

						BgL_tmpz00_4051 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_portz00_1623 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4051);
					}
				else
					{	/* Ieee/output.scm 281 */
						if (PAIRP(BgL_portz00_6))
							{	/* Ieee/output.scm 281 */
								if (NULLP(CDR(((obj_t) BgL_portz00_6))))
									{	/* Ieee/output.scm 281 */
										BgL_portz00_1623 = CAR(BgL_portz00_6);
									}
								else
									{	/* Ieee/output.scm 281 */
										BgL_portz00_1623 =
											BGl_errorz00zz__errorz00
											(BGl_string2250z00zz__r4_output_6_10_3z00,
											BGl_string2246z00zz__r4_output_6_10_3z00, BgL_portz00_6);
									}
							}
						else
							{	/* Ieee/output.scm 281 */
								BgL_portz00_1623 =
									BGl_errorz00zz__errorz00
									(BGl_string2250z00zz__r4_output_6_10_3z00,
									BGl_string2246z00zz__r4_output_6_10_3z00, BgL_portz00_6);
							}
					}
				return bgl_display_obj(BgL_objz00_5, BgL_portz00_1623);
			}
		}

	}



/* &display */
	obj_t BGl_z62displayz62zz__r4_output_6_10_3z00(obj_t BgL_envz00_3848,
		obj_t BgL_objz00_3849, obj_t BgL_portz00_3850)
	{
		{	/* Ieee/output.scm 280 */
			return
				BGl_displayz00zz__r4_output_6_10_3z00(BgL_objz00_3849,
				BgL_portz00_3850);
		}

	}



/* write */
	BGL_EXPORTED_DEF obj_t BGl_writez00zz__r4_output_6_10_3z00(obj_t BgL_objz00_7,
		obj_t BgL_portz00_8)
	{
		{	/* Ieee/output.scm 295 */
			{	/* Ieee/output.scm 296 */
				obj_t BgL_portz00_1633;

				if (NULLP(BgL_portz00_8))
					{	/* Ieee/output.scm 298 */
						obj_t BgL_tmpz00_4067;

						BgL_tmpz00_4067 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_portz00_1633 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4067);
					}
				else
					{	/* Ieee/output.scm 296 */
						if (PAIRP(BgL_portz00_8))
							{	/* Ieee/output.scm 296 */
								if (NULLP(CDR(((obj_t) BgL_portz00_8))))
									{	/* Ieee/output.scm 296 */
										BgL_portz00_1633 = CAR(BgL_portz00_8);
									}
								else
									{	/* Ieee/output.scm 296 */
										BgL_portz00_1633 =
											BGl_errorz00zz__errorz00
											(BGl_string2251z00zz__r4_output_6_10_3z00,
											BGl_string2246z00zz__r4_output_6_10_3z00, BgL_portz00_8);
									}
							}
						else
							{	/* Ieee/output.scm 296 */
								BgL_portz00_1633 =
									BGl_errorz00zz__errorz00
									(BGl_string2251z00zz__r4_output_6_10_3z00,
									BGl_string2246z00zz__r4_output_6_10_3z00, BgL_portz00_8);
							}
					}
				return bgl_write_obj(BgL_objz00_7, BgL_portz00_1633);
			}
		}

	}



/* &write */
	obj_t BGl_z62writez62zz__r4_output_6_10_3z00(obj_t BgL_envz00_3851,
		obj_t BgL_objz00_3852, obj_t BgL_portz00_3853)
	{
		{	/* Ieee/output.scm 295 */
			return
				BGl_writez00zz__r4_output_6_10_3z00(BgL_objz00_3852, BgL_portz00_3853);
		}

	}



/* _write-char */
	obj_t BGl__writezd2charzd2zz__r4_output_6_10_3z00(obj_t BgL_env1161z00_12,
		obj_t BgL_opt1160z00_11)
	{
		{	/* Ieee/output.scm 310 */
			{	/* Ieee/output.scm 310 */
				obj_t BgL_g1162z00_1643;

				BgL_g1162z00_1643 = VECTOR_REF(BgL_opt1160z00_11, 0L);
				switch (VECTOR_LENGTH(BgL_opt1160z00_11))
					{
					case 1L:

						{	/* Ieee/output.scm 310 */
							obj_t BgL_portz00_1646;

							{	/* Ieee/output.scm 310 */
								obj_t BgL_tmpz00_4082;

								BgL_tmpz00_4082 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_portz00_1646 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4082);
							}
							{	/* Ieee/output.scm 310 */

								{	/* Ieee/output.scm 310 */
									unsigned char BgL_charz00_2787;

									{	/* Ieee/output.scm 310 */
										obj_t BgL_tmpz00_4085;

										if (CHARP(BgL_g1162z00_1643))
											{	/* Ieee/output.scm 310 */
												BgL_tmpz00_4085 = BgL_g1162z00_1643;
											}
										else
											{
												obj_t BgL_auxz00_4088;

												BgL_auxz00_4088 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2247z00zz__r4_output_6_10_3z00,
													BINT(11766L),
													BGl_string2252z00zz__r4_output_6_10_3z00,
													BGl_string2253z00zz__r4_output_6_10_3z00,
													BgL_g1162z00_1643);
												FAILURE(BgL_auxz00_4088, BFALSE, BFALSE);
											}
										BgL_charz00_2787 = CCHAR(BgL_tmpz00_4085);
									}
									return bgl_display_char(BgL_charz00_2787, BgL_portz00_1646);
								}
							}
						}
						break;
					case 2L:

						{	/* Ieee/output.scm 310 */
							obj_t BgL_portz00_1647;

							BgL_portz00_1647 = VECTOR_REF(BgL_opt1160z00_11, 1L);
							{	/* Ieee/output.scm 310 */

								{	/* Ieee/output.scm 310 */
									unsigned char BgL_charz00_2790;

									{	/* Ieee/output.scm 310 */
										obj_t BgL_tmpz00_4095;

										if (CHARP(BgL_g1162z00_1643))
											{	/* Ieee/output.scm 310 */
												BgL_tmpz00_4095 = BgL_g1162z00_1643;
											}
										else
											{
												obj_t BgL_auxz00_4098;

												BgL_auxz00_4098 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2247z00zz__r4_output_6_10_3z00,
													BINT(11766L),
													BGl_string2252z00zz__r4_output_6_10_3z00,
													BGl_string2253z00zz__r4_output_6_10_3z00,
													BgL_g1162z00_1643);
												FAILURE(BgL_auxz00_4098, BFALSE, BFALSE);
											}
										BgL_charz00_2790 = CCHAR(BgL_tmpz00_4095);
									}
									{	/* Ieee/output.scm 311 */
										obj_t BgL_portz00_2792;

										if (OUTPUT_PORTP(BgL_portz00_1647))
											{	/* Ieee/output.scm 311 */
												BgL_portz00_2792 = BgL_portz00_1647;
											}
										else
											{
												obj_t BgL_auxz00_4105;

												BgL_auxz00_4105 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2247z00zz__r4_output_6_10_3z00,
													BINT(11854L),
													BGl_string2252z00zz__r4_output_6_10_3z00,
													BGl_string2249z00zz__r4_output_6_10_3z00,
													BgL_portz00_1647);
												FAILURE(BgL_auxz00_4105, BFALSE, BFALSE);
											}
										return bgl_display_char(BgL_charz00_2790, BgL_portz00_2792);
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* write-char */
	BGL_EXPORTED_DEF obj_t BGl_writezd2charzd2zz__r4_output_6_10_3z00(unsigned
		char BgL_charz00_9, obj_t BgL_portz00_10)
	{
		{	/* Ieee/output.scm 310 */
			{	/* Ieee/output.scm 317 */
				obj_t BgL_tmpz00_4112;

				BgL_tmpz00_4112 = ((obj_t) BgL_portz00_10);
				return bgl_display_char(BgL_charz00_9, BgL_tmpz00_4112);
			}
		}

	}



/* write-char-2 */
	BGL_EXPORTED_DEF obj_t BGl_writezd2charzd22z00zz__r4_output_6_10_3z00(unsigned
		char BgL_charz00_13, obj_t BgL_portz00_14)
	{
		{	/* Ieee/output.scm 316 */
			return bgl_display_char(BgL_charz00_13, BgL_portz00_14);
		}

	}



/* &write-char-2 */
	obj_t BGl_z62writezd2charzd22z62zz__r4_output_6_10_3z00(obj_t BgL_envz00_3854,
		obj_t BgL_charz00_3855, obj_t BgL_portz00_3856)
	{
		{	/* Ieee/output.scm 316 */
			{	/* Ieee/output.scm 317 */
				obj_t BgL_auxz00_4125;
				unsigned char BgL_auxz00_4116;

				if (OUTPUT_PORTP(BgL_portz00_3856))
					{	/* Ieee/output.scm 317 */
						BgL_auxz00_4125 = BgL_portz00_3856;
					}
				else
					{
						obj_t BgL_auxz00_4128;

						BgL_auxz00_4128 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2247z00zz__r4_output_6_10_3z00, BINT(12147L),
							BGl_string2254z00zz__r4_output_6_10_3z00,
							BGl_string2249z00zz__r4_output_6_10_3z00, BgL_portz00_3856);
						FAILURE(BgL_auxz00_4128, BFALSE, BFALSE);
					}
				{	/* Ieee/output.scm 317 */
					obj_t BgL_tmpz00_4117;

					if (CHARP(BgL_charz00_3855))
						{	/* Ieee/output.scm 317 */
							BgL_tmpz00_4117 = BgL_charz00_3855;
						}
					else
						{
							obj_t BgL_auxz00_4120;

							BgL_auxz00_4120 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2247z00zz__r4_output_6_10_3z00, BINT(12147L),
								BGl_string2254z00zz__r4_output_6_10_3z00,
								BGl_string2253z00zz__r4_output_6_10_3z00, BgL_charz00_3855);
							FAILURE(BgL_auxz00_4120, BFALSE, BFALSE);
						}
					BgL_auxz00_4116 = CCHAR(BgL_tmpz00_4117);
				}
				return
					BGl_writezd2charzd22z00zz__r4_output_6_10_3z00(BgL_auxz00_4116,
					BgL_auxz00_4125);
			}
		}

	}



/* _write-byte */
	obj_t BGl__writezd2bytezd2zz__r4_output_6_10_3z00(obj_t BgL_env1166z00_18,
		obj_t BgL_opt1165z00_17)
	{
		{	/* Ieee/output.scm 322 */
			{	/* Ieee/output.scm 322 */
				obj_t BgL_g1167z00_1648;

				BgL_g1167z00_1648 = VECTOR_REF(BgL_opt1165z00_17, 0L);
				switch (VECTOR_LENGTH(BgL_opt1165z00_17))
					{
					case 1L:

						{	/* Ieee/output.scm 322 */
							obj_t BgL_portz00_1651;

							{	/* Ieee/output.scm 322 */
								obj_t BgL_tmpz00_4134;

								BgL_tmpz00_4134 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_portz00_1651 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4134);
							}
							{	/* Ieee/output.scm 322 */

								{	/* Ieee/output.scm 322 */
									unsigned char BgL_bytez00_2796;

									{	/* Ieee/output.scm 322 */
										obj_t BgL_tmpz00_4137;

										if (INTEGERP(BgL_g1167z00_1648))
											{	/* Ieee/output.scm 322 */
												BgL_tmpz00_4137 = BgL_g1167z00_1648;
											}
										else
											{
												obj_t BgL_auxz00_4140;

												BgL_auxz00_4140 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2247z00zz__r4_output_6_10_3z00,
													BINT(12398L),
													BGl_string2255z00zz__r4_output_6_10_3z00,
													BGl_string2256z00zz__r4_output_6_10_3z00,
													BgL_g1167z00_1648);
												FAILURE(BgL_auxz00_4140, BFALSE, BFALSE);
											}
										BgL_bytez00_2796 = (unsigned char) CINT(BgL_tmpz00_4137);
									}
									return bgl_display_char(BgL_bytez00_2796, BgL_portz00_1651);
								}
							}
						}
						break;
					case 2L:

						{	/* Ieee/output.scm 322 */
							obj_t BgL_portz00_1652;

							BgL_portz00_1652 = VECTOR_REF(BgL_opt1165z00_17, 1L);
							{	/* Ieee/output.scm 322 */

								{	/* Ieee/output.scm 322 */
									unsigned char BgL_bytez00_2799;

									{	/* Ieee/output.scm 322 */
										obj_t BgL_tmpz00_4147;

										if (INTEGERP(BgL_g1167z00_1648))
											{	/* Ieee/output.scm 322 */
												BgL_tmpz00_4147 = BgL_g1167z00_1648;
											}
										else
											{
												obj_t BgL_auxz00_4150;

												BgL_auxz00_4150 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2247z00zz__r4_output_6_10_3z00,
													BINT(12398L),
													BGl_string2255z00zz__r4_output_6_10_3z00,
													BGl_string2256z00zz__r4_output_6_10_3z00,
													BgL_g1167z00_1648);
												FAILURE(BgL_auxz00_4150, BFALSE, BFALSE);
											}
										BgL_bytez00_2799 = (unsigned char) CINT(BgL_tmpz00_4147);
									}
									{	/* Ieee/output.scm 323 */
										obj_t BgL_portz00_2801;

										if (OUTPUT_PORTP(BgL_portz00_1652))
											{	/* Ieee/output.scm 323 */
												BgL_portz00_2801 = BgL_portz00_1652;
											}
										else
											{
												obj_t BgL_auxz00_4157;

												BgL_auxz00_4157 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2247z00zz__r4_output_6_10_3z00,
													BINT(12486L),
													BGl_string2255z00zz__r4_output_6_10_3z00,
													BGl_string2249z00zz__r4_output_6_10_3z00,
													BgL_portz00_1652);
												FAILURE(BgL_auxz00_4157, BFALSE, BFALSE);
											}
										return bgl_display_char(BgL_bytez00_2799, BgL_portz00_2801);
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* write-byte */
	BGL_EXPORTED_DEF obj_t BGl_writezd2bytezd2zz__r4_output_6_10_3z00(unsigned
		char BgL_bytez00_15, obj_t BgL_portz00_16)
	{
		{	/* Ieee/output.scm 322 */
			{	/* Ieee/output.scm 329 */
				obj_t BgL_tmpz00_4164;

				BgL_tmpz00_4164 = ((obj_t) BgL_portz00_16);
				return bgl_display_char(BgL_bytez00_15, BgL_tmpz00_4164);
			}
		}

	}



/* write-byte-2 */
	BGL_EXPORTED_DEF obj_t BGl_writezd2bytezd22z00zz__r4_output_6_10_3z00(unsigned
		char BgL_bytez00_19, obj_t BgL_portz00_20)
	{
		{	/* Ieee/output.scm 328 */
			return bgl_display_char(BgL_bytez00_19, BgL_portz00_20);
		}

	}



/* &write-byte-2 */
	obj_t BGl_z62writezd2bytezd22z62zz__r4_output_6_10_3z00(obj_t BgL_envz00_3857,
		obj_t BgL_bytez00_3858, obj_t BgL_portz00_3859)
	{
		{	/* Ieee/output.scm 328 */
			{	/* Ieee/output.scm 329 */
				obj_t BgL_auxz00_4177;
				unsigned char BgL_auxz00_4168;

				if (OUTPUT_PORTP(BgL_portz00_3859))
					{	/* Ieee/output.scm 329 */
						BgL_auxz00_4177 = BgL_portz00_3859;
					}
				else
					{
						obj_t BgL_auxz00_4180;

						BgL_auxz00_4180 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2247z00zz__r4_output_6_10_3z00, BINT(12779L),
							BGl_string2257z00zz__r4_output_6_10_3z00,
							BGl_string2249z00zz__r4_output_6_10_3z00, BgL_portz00_3859);
						FAILURE(BgL_auxz00_4180, BFALSE, BFALSE);
					}
				{	/* Ieee/output.scm 329 */
					obj_t BgL_tmpz00_4169;

					if (INTEGERP(BgL_bytez00_3858))
						{	/* Ieee/output.scm 329 */
							BgL_tmpz00_4169 = BgL_bytez00_3858;
						}
					else
						{
							obj_t BgL_auxz00_4172;

							BgL_auxz00_4172 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2247z00zz__r4_output_6_10_3z00, BINT(12779L),
								BGl_string2257z00zz__r4_output_6_10_3z00,
								BGl_string2256z00zz__r4_output_6_10_3z00, BgL_bytez00_3858);
							FAILURE(BgL_auxz00_4172, BFALSE, BFALSE);
						}
					BgL_auxz00_4168 = (unsigned char) CINT(BgL_tmpz00_4169);
				}
				return
					BGl_writezd2bytezd22z00zz__r4_output_6_10_3z00(BgL_auxz00_4168,
					BgL_auxz00_4177);
			}
		}

	}



/* illegal-char-rep */
	BGL_EXPORTED_DEF obj_t
		BGl_illegalzd2charzd2repz00zz__r4_output_6_10_3z00(unsigned char
		BgL_charz00_21)
	{
		{	/* Ieee/output.scm 334 */
			{	/* Ieee/output.scm 335 */
				bool_t BgL_test2470z00_4185;

				if (isalpha(BgL_charz00_21))
					{	/* Ieee/output.scm 335 */
						BgL_test2470z00_4185 = ((bool_t) 1);
					}
				else
					{	/* Ieee/output.scm 335 */
						BgL_test2470z00_4185 = isdigit(BgL_charz00_21);
					}
				if (BgL_test2470z00_4185)
					{	/* Ieee/output.scm 335 */
						return BCHAR(BgL_charz00_21);
					}
				else
					{

						switch (BgL_charz00_21)
							{
							case ((unsigned char) 10):

								return BGl_string2258z00zz__r4_output_6_10_3z00;
								break;
							case ((unsigned char) 13):

								return BGl_string2259z00zz__r4_output_6_10_3z00;
								break;
							case ((unsigned char) ' '):

								return BGl_string2260z00zz__r4_output_6_10_3z00;
								break;
							case ((unsigned char) 9):

								return BGl_string2261z00zz__r4_output_6_10_3z00;
								break;
							default:
								{	/* Ieee/output.scm 347 */
									long BgL_iz00_1658;

									BgL_iz00_1658 = (BgL_charz00_21);
									if ((BgL_iz00_1658 < 33L))
										{	/* Ieee/output.scm 348 */
											BGL_TAIL return bgl_ill_char_rep(BgL_charz00_21);
										}
									else
										{	/* Ieee/output.scm 348 */
											return BCHAR(BgL_charz00_21);
										}
								}
							}
					}
			}
		}

	}



/* &illegal-char-rep */
	obj_t BGl_z62illegalzd2charzd2repz62zz__r4_output_6_10_3z00(obj_t
		BgL_envz00_3860, obj_t BgL_charz00_3861)
	{
		{	/* Ieee/output.scm 334 */
			{	/* Ieee/output.scm 335 */
				unsigned char BgL_auxz00_4196;

				{	/* Ieee/output.scm 335 */
					obj_t BgL_tmpz00_4197;

					if (CHARP(BgL_charz00_3861))
						{	/* Ieee/output.scm 335 */
							BgL_tmpz00_4197 = BgL_charz00_3861;
						}
					else
						{
							obj_t BgL_auxz00_4200;

							BgL_auxz00_4200 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2247z00zz__r4_output_6_10_3z00, BINT(13064L),
								BGl_string2262z00zz__r4_output_6_10_3z00,
								BGl_string2253z00zz__r4_output_6_10_3z00, BgL_charz00_3861);
							FAILURE(BgL_auxz00_4200, BFALSE, BFALSE);
						}
					BgL_auxz00_4196 = CCHAR(BgL_tmpz00_4197);
				}
				return
					BGl_illegalzd2charzd2repz00zz__r4_output_6_10_3z00(BgL_auxz00_4196);
			}
		}

	}



/* print */
	BGL_EXPORTED_DEF obj_t BGl_printz00zz__r4_output_6_10_3z00(obj_t
		BgL_objz00_22)
	{
		{	/* Ieee/output.scm 355 */
			{	/* Ieee/output.scm 356 */
				obj_t BgL_portz00_1661;

				{	/* Ieee/output.scm 356 */
					obj_t BgL_tmpz00_4206;

					BgL_tmpz00_4206 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_portz00_1661 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4206);
				}
				{
					obj_t BgL_lz00_2820;
					obj_t BgL_resz00_2821;

					BgL_lz00_2820 = BgL_objz00_22;
					BgL_resz00_2821 = BNIL;
				BgL_loopz00_2819:
					if (NULLP(BgL_lz00_2820))
						{	/* Ieee/output.scm 359 */
							bgl_display_char(((unsigned char) 10), BgL_portz00_1661);
							return BgL_resz00_2821;
						}
					else
						{	/* Ieee/output.scm 363 */
							obj_t BgL_vz00_2826;

							BgL_vz00_2826 = CAR(((obj_t) BgL_lz00_2820));
							bgl_display_obj(BgL_vz00_2826, BgL_portz00_1661);
							{	/* Ieee/output.scm 365 */
								obj_t BgL_arg1328z00_2827;

								BgL_arg1328z00_2827 = CDR(((obj_t) BgL_lz00_2820));
								{
									obj_t BgL_resz00_4218;
									obj_t BgL_lz00_4217;

									BgL_lz00_4217 = BgL_arg1328z00_2827;
									BgL_resz00_4218 = BgL_vz00_2826;
									BgL_resz00_2821 = BgL_resz00_4218;
									BgL_lz00_2820 = BgL_lz00_4217;
									goto BgL_loopz00_2819;
								}
							}
						}
				}
			}
		}

	}



/* &print */
	obj_t BGl_z62printz62zz__r4_output_6_10_3z00(obj_t BgL_envz00_3862,
		obj_t BgL_objz00_3863)
	{
		{	/* Ieee/output.scm 355 */
			return BGl_printz00zz__r4_output_6_10_3z00(BgL_objz00_3863);
		}

	}



/* display* */
	BGL_EXPORTED_DEF obj_t BGl_displayza2za2zz__r4_output_6_10_3z00(obj_t
		BgL_objz00_23)
	{
		{	/* Ieee/output.scm 370 */
			{	/* Ieee/output.scm 371 */
				obj_t BgL_portz00_1671;

				{	/* Ieee/output.scm 371 */
					obj_t BgL_tmpz00_4220;

					BgL_tmpz00_4220 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_portz00_1671 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4220);
				}
				{
					obj_t BgL_lz00_1673;

					BgL_lz00_1673 = BgL_objz00_23;
				BgL_zc3z04anonymousza31329ze3z87_1674:
					if (NULLP(BgL_lz00_1673))
						{	/* Ieee/output.scm 373 */
							return BUNSPEC;
						}
					else
						{	/* Ieee/output.scm 373 */
							{	/* Ieee/output.scm 376 */
								obj_t BgL_arg1331z00_1677;

								BgL_arg1331z00_1677 = CAR(((obj_t) BgL_lz00_1673));
								bgl_display_obj(BgL_arg1331z00_1677, BgL_portz00_1671);
							}
							{	/* Ieee/output.scm 377 */
								obj_t BgL_arg1332z00_1678;

								BgL_arg1332z00_1678 = CDR(((obj_t) BgL_lz00_1673));
								{
									obj_t BgL_lz00_4230;

									BgL_lz00_4230 = BgL_arg1332z00_1678;
									BgL_lz00_1673 = BgL_lz00_4230;
									goto BgL_zc3z04anonymousza31329ze3z87_1674;
								}
							}
						}
				}
			}
		}

	}



/* &display* */
	obj_t BGl_z62displayza2zc0zz__r4_output_6_10_3z00(obj_t BgL_envz00_3864,
		obj_t BgL_objz00_3865)
	{
		{	/* Ieee/output.scm 370 */
			return BGl_displayza2za2zz__r4_output_6_10_3z00(BgL_objz00_3865);
		}

	}



/* write* */
	BGL_EXPORTED_DEF obj_t BGl_writeza2za2zz__r4_output_6_10_3z00(obj_t
		BgL_objz00_24)
	{
		{	/* Ieee/output.scm 382 */
			{	/* Ieee/output.scm 383 */
				obj_t BgL_portz00_1680;

				{	/* Ieee/output.scm 383 */
					obj_t BgL_tmpz00_4232;

					BgL_tmpz00_4232 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_portz00_1680 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4232);
				}
				{
					obj_t BgL_lz00_1682;

					BgL_lz00_1682 = BgL_objz00_24;
				BgL_zc3z04anonymousza31333ze3z87_1683:
					if (NULLP(BgL_lz00_1682))
						{	/* Ieee/output.scm 385 */
							return BUNSPEC;
						}
					else
						{	/* Ieee/output.scm 385 */
							{	/* Ieee/output.scm 388 */
								obj_t BgL_arg1335z00_1686;

								BgL_arg1335z00_1686 = CAR(((obj_t) BgL_lz00_1682));
								bgl_write_obj(BgL_arg1335z00_1686, BgL_portz00_1680);
							}
							{	/* Ieee/output.scm 389 */
								obj_t BgL_arg1336z00_1687;

								BgL_arg1336z00_1687 = CDR(((obj_t) BgL_lz00_1682));
								{
									obj_t BgL_lz00_4242;

									BgL_lz00_4242 = BgL_arg1336z00_1687;
									BgL_lz00_1682 = BgL_lz00_4242;
									goto BgL_zc3z04anonymousza31333ze3z87_1683;
								}
							}
						}
				}
			}
		}

	}



/* &write* */
	obj_t BGl_z62writeza2zc0zz__r4_output_6_10_3z00(obj_t BgL_envz00_3866,
		obj_t BgL_objz00_3867)
	{
		{	/* Ieee/output.scm 382 */
			return BGl_writeza2za2zz__r4_output_6_10_3z00(BgL_objz00_3867);
		}

	}



/* tprint */
	BGL_EXPORTED_DEF obj_t BGl_tprintz00zz__r4_output_6_10_3z00(obj_t
		BgL_portz00_25, obj_t BgL_objz00_26)
	{
		{	/* Ieee/output.scm 399 */
			{	/* Ieee/output.scm 400 */
				obj_t BgL_top2478z00_4245;

				BgL_top2478z00_4245 = BGL_EXITD_TOP_AS_OBJ();
				BGL_MUTEX_LOCK(BGl_tprintzd2mutexzd2zz__r4_output_6_10_3z00);
				BGL_EXITD_PUSH_PROTECT(BgL_top2478z00_4245,
					BGl_tprintzd2mutexzd2zz__r4_output_6_10_3z00);
				BUNSPEC;
				{	/* Ieee/output.scm 400 */
					obj_t BgL_tmp2477z00_4244;

					{	/* Ieee/output.scm 401 */
						obj_t BgL_runner1339z00_2838;

						{	/* Ieee/output.scm 401 */
							obj_t BgL_list1337z00_2839;

							BgL_list1337z00_2839 = MAKE_YOUNG_PAIR(BgL_objz00_26, BNIL);
							BgL_runner1339z00_2838 =
								BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(BgL_portz00_25,
								BgL_list1337z00_2839);
						}
						{	/* Ieee/output.scm 401 */
							obj_t BgL_aux1338z00_2840;

							BgL_aux1338z00_2840 = CAR(BgL_runner1339z00_2838);
							BgL_runner1339z00_2838 = CDR(BgL_runner1339z00_2838);
							BGl_fprintz00zz__r4_output_6_10_3z00(BgL_aux1338z00_2840,
								BgL_runner1339z00_2838);
						}
					}
					BgL_tmp2477z00_4244 = bgl_flush_output_port(BgL_portz00_25);
					BGL_EXITD_POP_PROTECT(BgL_top2478z00_4245);
					BUNSPEC;
					BGL_MUTEX_UNLOCK(BGl_tprintzd2mutexzd2zz__r4_output_6_10_3z00);
					return BgL_tmp2477z00_4244;
				}
			}
		}

	}



/* &tprint */
	obj_t BGl_z62tprintz62zz__r4_output_6_10_3z00(obj_t BgL_envz00_3868,
		obj_t BgL_portz00_3869, obj_t BgL_objz00_3870)
	{
		{	/* Ieee/output.scm 399 */
			{	/* Ieee/output.scm 400 */
				obj_t BgL_auxz00_4257;

				if (OUTPUT_PORTP(BgL_portz00_3869))
					{	/* Ieee/output.scm 400 */
						BgL_auxz00_4257 = BgL_portz00_3869;
					}
				else
					{
						obj_t BgL_auxz00_4260;

						BgL_auxz00_4260 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2247z00zz__r4_output_6_10_3z00, BINT(15213L),
							BGl_string2263z00zz__r4_output_6_10_3z00,
							BGl_string2249z00zz__r4_output_6_10_3z00, BgL_portz00_3869);
						FAILURE(BgL_auxz00_4260, BFALSE, BFALSE);
					}
				return
					BGl_tprintz00zz__r4_output_6_10_3z00(BgL_auxz00_4257,
					BgL_objz00_3870);
			}
		}

	}



/* fprint */
	BGL_EXPORTED_DEF obj_t BGl_fprintz00zz__r4_output_6_10_3z00(obj_t
		BgL_portz00_27, obj_t BgL_objz00_28)
	{
		{	/* Ieee/output.scm 407 */
			{
				obj_t BgL_lz00_2859;
				obj_t BgL_resz00_2860;

				BgL_lz00_2859 = BgL_objz00_28;
				BgL_resz00_2860 = BNIL;
			BgL_loopz00_2858:
				if (NULLP(BgL_lz00_2859))
					{	/* Ieee/output.scm 410 */
						bgl_display_char(((unsigned char) 10), BgL_portz00_27);
						return BgL_resz00_2860;
					}
				else
					{	/* Ieee/output.scm 414 */
						obj_t BgL_vz00_2866;

						BgL_vz00_2866 = CAR(((obj_t) BgL_lz00_2859));
						bgl_display_obj(BgL_vz00_2866, BgL_portz00_27);
						{	/* Ieee/output.scm 416 */
							obj_t BgL_arg1343z00_2868;

							BgL_arg1343z00_2868 = CDR(((obj_t) BgL_lz00_2859));
							{
								obj_t BgL_resz00_4274;
								obj_t BgL_lz00_4273;

								BgL_lz00_4273 = BgL_arg1343z00_2868;
								BgL_resz00_4274 = BgL_vz00_2866;
								BgL_resz00_2860 = BgL_resz00_4274;
								BgL_lz00_2859 = BgL_lz00_4273;
								goto BgL_loopz00_2858;
							}
						}
					}
			}
		}

	}



/* &fprint */
	obj_t BGl_z62fprintz62zz__r4_output_6_10_3z00(obj_t BgL_envz00_3871,
		obj_t BgL_portz00_3872, obj_t BgL_objz00_3873)
	{
		{	/* Ieee/output.scm 407 */
			{	/* Ieee/output.scm 408 */
				obj_t BgL_auxz00_4275;

				if (OUTPUT_PORTP(BgL_portz00_3872))
					{	/* Ieee/output.scm 408 */
						BgL_auxz00_4275 = BgL_portz00_3872;
					}
				else
					{
						obj_t BgL_auxz00_4278;

						BgL_auxz00_4278 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2247z00zz__r4_output_6_10_3z00, BINT(15556L),
							BGl_string2264z00zz__r4_output_6_10_3z00,
							BGl_string2249z00zz__r4_output_6_10_3z00, BgL_portz00_3872);
						FAILURE(BgL_auxz00_4278, BFALSE, BFALSE);
					}
				return
					BGl_fprintz00zz__r4_output_6_10_3z00(BgL_auxz00_4275,
					BgL_objz00_3873);
			}
		}

	}



/* xprintf */
	obj_t BGl_xprintfz00zz__r4_output_6_10_3z00(obj_t BgL_procnamez00_29,
		obj_t BgL_pz00_30, obj_t BgL__fmtz00_31, obj_t BgL_objsz00_32)
	{
		{	/* Ieee/output.scm 421 */
			{	/* Ieee/output.scm 422 */
				long BgL_lenz00_1702;

				BgL_lenz00_1702 = STRING_LENGTH(BgL__fmtz00_31);
				{
					obj_t BgL_iz00_1704;
					obj_t BgL_osz00_1705;

					BgL_iz00_1704 = BINT(0L);
					BgL_osz00_1705 = BgL_objsz00_32;
				BgL_zc3z04anonymousza31344ze3z87_1706:
					{
						obj_t BgL_iz00_1772;
						obj_t BgL_numz00_1773;
						long BgL_mincolz00_1774;
						unsigned char BgL_paddingz00_1775;
						long BgL_iz00_1786;
						obj_t BgL_numz00_1787;
						obj_t BgL_pz00_1788;
						unsigned char BgL_fz00_1805;
						long BgL_iz00_1806;
						bool_t BgL_altzf3zf3_1807;

						if (((long) CINT(BgL_iz00_1704) < BgL_lenz00_1702))
							{	/* Ieee/output.scm 560 */
								unsigned char BgL_cz00_1715;

								BgL_cz00_1715 =
									STRING_REF(BgL__fmtz00_31, (long) CINT(BgL_iz00_1704));
								if ((BgL_cz00_1715 == ((unsigned char) '~')))
									{	/* Ieee/output.scm 561 */
										if (((long) CINT(BgL_iz00_1704) == (BgL_lenz00_1702 - 1L)))
											{	/* Ieee/output.scm 563 */
												return
													BGl_errorz00zz__errorz00(BgL_procnamez00_29,
													BGl_string2273z00zz__r4_output_6_10_3z00,
													c_substring(BgL__fmtz00_31,
														(long) CINT(BgL_iz00_1704), BgL_lenz00_1702));
											}
										else
											{	/* Ieee/output.scm 563 */
												if (
													(((unsigned char) ':') ==
														STRING_REF(BgL__fmtz00_31,
															((long) CINT(BgL_iz00_1704) + 1L))))
													{	/* Ieee/output.scm 567 */
														if (
															((long) CINT(BgL_iz00_1704) ==
																(BgL_lenz00_1702 - 2L)))
															{	/* Ieee/output.scm 568 */
																return
																	BGl_errorz00zz__errorz00(BgL_procnamez00_29,
																	BGl_string2273z00zz__r4_output_6_10_3z00,
																	c_substring(BgL__fmtz00_31,
																		(long) CINT(BgL_iz00_1704),
																		BgL_lenz00_1702));
															}
														else
															{	/* Ieee/output.scm 568 */
																BgL_fz00_1805 =
																	STRING_REF(BgL__fmtz00_31,
																	((long) CINT(BgL_iz00_1704) + 2L));
																BgL_iz00_1806 =
																	((long) CINT(BgL_iz00_1704) + 2L);
																BgL_altzf3zf3_1807 = ((bool_t) 1);
															BgL_zc3z04anonymousza31411ze3z87_1808:
																{

																	switch (BgL_fz00_1805)
																		{
																		case ((unsigned char) 'a'):
																		case ((unsigned char) 'A'):

																			if (BgL_altzf3zf3_1807)
																				{	/* Ieee/output.scm 503 */
																					obj_t BgL_arg1413z00_1812;

																					if (NULLP(BgL_osz00_1705))
																						{	/* Ieee/output.scm 427 */
																							BgL_arg1413z00_1812 =
																								BGl_errorz00zz__errorz00
																								(BgL_procnamez00_29,
																								BGl_string2265z00zz__r4_output_6_10_3z00,
																								BCHAR(BgL_fz00_1805));
																						}
																					else
																						{	/* Ieee/output.scm 427 */
																							BgL_arg1413z00_1812 =
																								CAR(((obj_t) BgL_osz00_1705));
																						}
																					BGl_displayzd2circlezd2zz__pp_circlez00
																						(BgL_arg1413z00_1812, BgL_pz00_30);
																				}
																			else
																				{	/* Ieee/output.scm 504 */
																					obj_t BgL_arg1414z00_1813;

																					if (NULLP(BgL_osz00_1705))
																						{	/* Ieee/output.scm 427 */
																							BgL_arg1414z00_1813 =
																								BGl_errorz00zz__errorz00
																								(BgL_procnamez00_29,
																								BGl_string2265z00zz__r4_output_6_10_3z00,
																								BCHAR(BgL_fz00_1805));
																						}
																					else
																						{	/* Ieee/output.scm 427 */
																							BgL_arg1414z00_1813 =
																								CAR(((obj_t) BgL_osz00_1705));
																						}
																					bgl_display_obj(BgL_arg1414z00_1813,
																						BgL_pz00_30);
																				}
																			{	/* Ieee/output.scm 505 */
																				long BgL_arg1415z00_1814;
																				obj_t BgL_arg1416z00_1815;

																				BgL_arg1415z00_1814 =
																					(BgL_iz00_1806 + 1L);
																				BgL_arg1416z00_1815 =
																					CDR(((obj_t) BgL_osz00_1705));
																				{
																					obj_t BgL_osz00_4330;
																					obj_t BgL_iz00_4328;

																					BgL_iz00_4328 =
																						BINT(BgL_arg1415z00_1814);
																					BgL_osz00_4330 = BgL_arg1416z00_1815;
																					BgL_osz00_1705 = BgL_osz00_4330;
																					BgL_iz00_1704 = BgL_iz00_4328;
																					goto
																						BgL_zc3z04anonymousza31344ze3z87_1706;
																				}
																			}
																			break;
																		case ((unsigned char) 's'):
																		case ((unsigned char) 'S'):

																			if (BgL_altzf3zf3_1807)
																				{	/* Ieee/output.scm 508 */
																					obj_t BgL_arg1417z00_1816;

																					if (NULLP(BgL_osz00_1705))
																						{	/* Ieee/output.scm 427 */
																							BgL_arg1417z00_1816 =
																								BGl_errorz00zz__errorz00
																								(BgL_procnamez00_29,
																								BGl_string2265z00zz__r4_output_6_10_3z00,
																								BCHAR(BgL_fz00_1805));
																						}
																					else
																						{	/* Ieee/output.scm 427 */
																							BgL_arg1417z00_1816 =
																								CAR(((obj_t) BgL_osz00_1705));
																						}
																					BGl_writezd2circlezd2zz__pp_circlez00
																						(BgL_arg1417z00_1816, BgL_pz00_30);
																				}
																			else
																				{	/* Ieee/output.scm 509 */
																					obj_t BgL_arg1418z00_1817;

																					if (NULLP(BgL_osz00_1705))
																						{	/* Ieee/output.scm 427 */
																							BgL_arg1418z00_1817 =
																								BGl_errorz00zz__errorz00
																								(BgL_procnamez00_29,
																								BGl_string2265z00zz__r4_output_6_10_3z00,
																								BCHAR(BgL_fz00_1805));
																						}
																					else
																						{	/* Ieee/output.scm 427 */
																							BgL_arg1418z00_1817 =
																								CAR(((obj_t) BgL_osz00_1705));
																						}
																					{	/* Ieee/output.scm 509 */
																						obj_t BgL_list1419z00_1818;

																						BgL_list1419z00_1818 =
																							MAKE_YOUNG_PAIR(BgL_pz00_30,
																							BNIL);
																						BGl_writez00zz__r4_output_6_10_3z00
																							(BgL_arg1418z00_1817,
																							BgL_list1419z00_1818);
																					}
																				}
																			{	/* Ieee/output.scm 510 */
																				long BgL_arg1420z00_1819;
																				obj_t BgL_arg1421z00_1820;

																				BgL_arg1420z00_1819 =
																					(BgL_iz00_1806 + 1L);
																				BgL_arg1421z00_1820 =
																					CDR(((obj_t) BgL_osz00_1705));
																				{
																					obj_t BgL_osz00_4352;
																					obj_t BgL_iz00_4350;

																					BgL_iz00_4350 =
																						BINT(BgL_arg1420z00_1819);
																					BgL_osz00_4352 = BgL_arg1421z00_1820;
																					BgL_osz00_1705 = BgL_osz00_4352;
																					BgL_iz00_1704 = BgL_iz00_4350;
																					goto
																						BgL_zc3z04anonymousza31344ze3z87_1706;
																				}
																			}
																			break;
																		case ((unsigned char) 'v'):
																		case ((unsigned char) 'V'):

																			if (BgL_altzf3zf3_1807)
																				{	/* Ieee/output.scm 513 */
																					obj_t BgL_arg1422z00_1821;

																					if (NULLP(BgL_osz00_1705))
																						{	/* Ieee/output.scm 427 */
																							BgL_arg1422z00_1821 =
																								BGl_errorz00zz__errorz00
																								(BgL_procnamez00_29,
																								BGl_string2265z00zz__r4_output_6_10_3z00,
																								BCHAR(BgL_fz00_1805));
																						}
																					else
																						{	/* Ieee/output.scm 427 */
																							BgL_arg1422z00_1821 =
																								CAR(((obj_t) BgL_osz00_1705));
																						}
																					BGl_displayzd2circlezd2zz__pp_circlez00
																						(BgL_arg1422z00_1821, BgL_pz00_30);
																				}
																			else
																				{	/* Ieee/output.scm 514 */
																					obj_t BgL_arg1423z00_1822;

																					if (NULLP(BgL_osz00_1705))
																						{	/* Ieee/output.scm 427 */
																							BgL_arg1423z00_1822 =
																								BGl_errorz00zz__errorz00
																								(BgL_procnamez00_29,
																								BGl_string2265z00zz__r4_output_6_10_3z00,
																								BCHAR(BgL_fz00_1805));
																						}
																					else
																						{	/* Ieee/output.scm 427 */
																							BgL_arg1423z00_1822 =
																								CAR(((obj_t) BgL_osz00_1705));
																						}
																					bgl_display_obj(BgL_arg1423z00_1822,
																						BgL_pz00_30);
																				}
																			bgl_display_char(((unsigned char) 10),
																				BgL_pz00_30);
																			{	/* Ieee/output.scm 516 */
																				long BgL_arg1424z00_1823;
																				obj_t BgL_arg1425z00_1824;

																				BgL_arg1424z00_1823 =
																					(BgL_iz00_1806 + 1L);
																				BgL_arg1425z00_1824 =
																					CDR(((obj_t) BgL_osz00_1705));
																				{
																					obj_t BgL_osz00_4374;
																					obj_t BgL_iz00_4372;

																					BgL_iz00_4372 =
																						BINT(BgL_arg1424z00_1823);
																					BgL_osz00_4374 = BgL_arg1425z00_1824;
																					BgL_osz00_1705 = BgL_osz00_4374;
																					BgL_iz00_1704 = BgL_iz00_4372;
																					goto
																						BgL_zc3z04anonymousza31344ze3z87_1706;
																				}
																			}
																			break;
																		case ((unsigned char) 'c'):
																		case ((unsigned char) 'C'):

																			{	/* Ieee/output.scm 518 */
																				obj_t BgL_oz00_1825;

																				if (NULLP(BgL_osz00_1705))
																					{	/* Ieee/output.scm 427 */
																						BgL_oz00_1825 =
																							BGl_errorz00zz__errorz00
																							(BgL_procnamez00_29,
																							BGl_string2265z00zz__r4_output_6_10_3z00,
																							BCHAR(BgL_fz00_1805));
																					}
																				else
																					{	/* Ieee/output.scm 427 */
																						BgL_oz00_1825 =
																							CAR(((obj_t) BgL_osz00_1705));
																					}
																				if (CHARP(BgL_oz00_1825))
																					{	/* Ieee/output.scm 519 */
																						{	/* Ieee/output.scm 317 */
																							unsigned char BgL_tmpz00_4383;

																							BgL_tmpz00_4383 =
																								CCHAR(BgL_oz00_1825);
																							bgl_display_char(BgL_tmpz00_4383,
																								BgL_pz00_30);
																						}
																						{	/* Ieee/output.scm 523 */
																							long BgL_arg1427z00_1827;
																							obj_t BgL_arg1428z00_1828;

																							BgL_arg1427z00_1827 =
																								(BgL_iz00_1806 + 1L);
																							BgL_arg1428z00_1828 =
																								CDR(((obj_t) BgL_osz00_1705));
																							{
																								obj_t BgL_osz00_4391;
																								obj_t BgL_iz00_4389;

																								BgL_iz00_4389 =
																									BINT(BgL_arg1427z00_1827);
																								BgL_osz00_4391 =
																									BgL_arg1428z00_1828;
																								BgL_osz00_1705 = BgL_osz00_4391;
																								BgL_iz00_1704 = BgL_iz00_4389;
																								goto
																									BgL_zc3z04anonymousza31344ze3z87_1706;
																							}
																						}
																					}
																				else
																					{	/* Ieee/output.scm 519 */
																						return
																							BGl_errorz00zz__errorz00
																							(BgL_procnamez00_29,
																							BGl_string2268z00zz__r4_output_6_10_3z00,
																							BgL_oz00_1825);
																					}
																			}
																			break;
																		case ((unsigned char) 'd'):
																		case ((unsigned char) 'D'):

																			{	/* Ieee/output.scm 525 */
																				obj_t BgL_arg1429z00_1829;

																				if (NULLP(BgL_osz00_1705))
																					{	/* Ieee/output.scm 427 */
																						BgL_arg1429z00_1829 =
																							BGl_errorz00zz__errorz00
																							(BgL_procnamez00_29,
																							BGl_string2265z00zz__r4_output_6_10_3z00,
																							BCHAR(BgL_fz00_1805));
																					}
																				else
																					{	/* Ieee/output.scm 427 */
																						BgL_arg1429z00_1829 =
																							CAR(((obj_t) BgL_osz00_1705));
																					}
																				if (BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_arg1429z00_1829))
																					{	/* Ieee/output.scm 432 */
																						bgl_display_obj
																							(BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00
																							(BgL_arg1429z00_1829, BINT(10L)),
																							BgL_pz00_30);
																					}
																				else
																					{	/* Ieee/output.scm 432 */
																						BGl_bigloozd2typezd2errorz00zz__errorz00
																							(BgL_procnamez00_29,
																							BGl_string2269z00zz__r4_output_6_10_3z00,
																							BgL_arg1429z00_1829);
																					}
																			}
																			{	/* Ieee/output.scm 526 */
																				long BgL_arg1430z00_1830;
																				obj_t BgL_arg1431z00_1831;

																				BgL_arg1430z00_1830 =
																					(BgL_iz00_1806 + 1L);
																				BgL_arg1431z00_1831 =
																					CDR(((obj_t) BgL_osz00_1705));
																				{
																					obj_t BgL_osz00_4410;
																					obj_t BgL_iz00_4408;

																					BgL_iz00_4408 =
																						BINT(BgL_arg1430z00_1830);
																					BgL_osz00_4410 = BgL_arg1431z00_1831;
																					BgL_osz00_1705 = BgL_osz00_4410;
																					BgL_iz00_1704 = BgL_iz00_4408;
																					goto
																						BgL_zc3z04anonymousza31344ze3z87_1706;
																				}
																			}
																			break;
																		case ((unsigned char) 'x'):
																		case ((unsigned char) 'X'):

																			{	/* Ieee/output.scm 528 */
																				obj_t BgL_arg1434z00_1832;

																				if (NULLP(BgL_osz00_1705))
																					{	/* Ieee/output.scm 427 */
																						BgL_arg1434z00_1832 =
																							BGl_errorz00zz__errorz00
																							(BgL_procnamez00_29,
																							BGl_string2265z00zz__r4_output_6_10_3z00,
																							BCHAR(BgL_fz00_1805));
																					}
																				else
																					{	/* Ieee/output.scm 427 */
																						BgL_arg1434z00_1832 =
																							CAR(((obj_t) BgL_osz00_1705));
																					}
																				if (BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_arg1434z00_1832))
																					{	/* Ieee/output.scm 432 */
																						bgl_display_obj
																							(BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00
																							(BgL_arg1434z00_1832, BINT(16L)),
																							BgL_pz00_30);
																					}
																				else
																					{	/* Ieee/output.scm 432 */
																						BGl_bigloozd2typezd2errorz00zz__errorz00
																							(BgL_procnamez00_29,
																							BGl_string2269z00zz__r4_output_6_10_3z00,
																							BgL_arg1434z00_1832);
																					}
																			}
																			{	/* Ieee/output.scm 529 */
																				long BgL_arg1435z00_1833;
																				obj_t BgL_arg1436z00_1834;

																				BgL_arg1435z00_1833 =
																					(BgL_iz00_1806 + 1L);
																				BgL_arg1436z00_1834 =
																					CDR(((obj_t) BgL_osz00_1705));
																				{
																					obj_t BgL_osz00_4428;
																					obj_t BgL_iz00_4426;

																					BgL_iz00_4426 =
																						BINT(BgL_arg1435z00_1833);
																					BgL_osz00_4428 = BgL_arg1436z00_1834;
																					BgL_osz00_1705 = BgL_osz00_4428;
																					BgL_iz00_1704 = BgL_iz00_4426;
																					goto
																						BgL_zc3z04anonymousza31344ze3z87_1706;
																				}
																			}
																			break;
																		case ((unsigned char) 'o'):
																		case ((unsigned char) 'O'):

																			{	/* Ieee/output.scm 531 */
																				obj_t BgL_arg1437z00_1835;

																				if (NULLP(BgL_osz00_1705))
																					{	/* Ieee/output.scm 427 */
																						BgL_arg1437z00_1835 =
																							BGl_errorz00zz__errorz00
																							(BgL_procnamez00_29,
																							BGl_string2265z00zz__r4_output_6_10_3z00,
																							BCHAR(BgL_fz00_1805));
																					}
																				else
																					{	/* Ieee/output.scm 427 */
																						BgL_arg1437z00_1835 =
																							CAR(((obj_t) BgL_osz00_1705));
																					}
																				if (BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_arg1437z00_1835))
																					{	/* Ieee/output.scm 432 */
																						bgl_display_obj
																							(BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00
																							(BgL_arg1437z00_1835, BINT(8L)),
																							BgL_pz00_30);
																					}
																				else
																					{	/* Ieee/output.scm 432 */
																						BGl_bigloozd2typezd2errorz00zz__errorz00
																							(BgL_procnamez00_29,
																							BGl_string2269z00zz__r4_output_6_10_3z00,
																							BgL_arg1437z00_1835);
																					}
																			}
																			{	/* Ieee/output.scm 532 */
																				long BgL_arg1438z00_1836;
																				obj_t BgL_arg1439z00_1837;

																				BgL_arg1438z00_1836 =
																					(BgL_iz00_1806 + 1L);
																				BgL_arg1439z00_1837 =
																					CDR(((obj_t) BgL_osz00_1705));
																				{
																					obj_t BgL_osz00_4446;
																					obj_t BgL_iz00_4444;

																					BgL_iz00_4444 =
																						BINT(BgL_arg1438z00_1836);
																					BgL_osz00_4446 = BgL_arg1439z00_1837;
																					BgL_osz00_1705 = BgL_osz00_4446;
																					BgL_iz00_1704 = BgL_iz00_4444;
																					goto
																						BgL_zc3z04anonymousza31344ze3z87_1706;
																				}
																			}
																			break;
																		case ((unsigned char) 'b'):
																		case ((unsigned char) 'B'):

																			{	/* Ieee/output.scm 534 */
																				obj_t BgL_arg1440z00_1838;

																				if (NULLP(BgL_osz00_1705))
																					{	/* Ieee/output.scm 427 */
																						BgL_arg1440z00_1838 =
																							BGl_errorz00zz__errorz00
																							(BgL_procnamez00_29,
																							BGl_string2265z00zz__r4_output_6_10_3z00,
																							BCHAR(BgL_fz00_1805));
																					}
																				else
																					{	/* Ieee/output.scm 427 */
																						BgL_arg1440z00_1838 =
																							CAR(((obj_t) BgL_osz00_1705));
																					}
																				if (BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_arg1440z00_1838))
																					{	/* Ieee/output.scm 432 */
																						bgl_display_obj
																							(BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00
																							(BgL_arg1440z00_1838, BINT(2L)),
																							BgL_pz00_30);
																					}
																				else
																					{	/* Ieee/output.scm 432 */
																						BGl_bigloozd2typezd2errorz00zz__errorz00
																							(BgL_procnamez00_29,
																							BGl_string2269z00zz__r4_output_6_10_3z00,
																							BgL_arg1440z00_1838);
																					}
																			}
																			{	/* Ieee/output.scm 535 */
																				long BgL_arg1441z00_1839;
																				obj_t BgL_arg1442z00_1840;

																				BgL_arg1441z00_1839 =
																					(BgL_iz00_1806 + 1L);
																				BgL_arg1442z00_1840 =
																					CDR(((obj_t) BgL_osz00_1705));
																				{
																					obj_t BgL_osz00_4464;
																					obj_t BgL_iz00_4462;

																					BgL_iz00_4462 =
																						BINT(BgL_arg1441z00_1839);
																					BgL_osz00_4464 = BgL_arg1442z00_1840;
																					BgL_osz00_1705 = BgL_osz00_4464;
																					BgL_iz00_1704 = BgL_iz00_4462;
																					goto
																						BgL_zc3z04anonymousza31344ze3z87_1706;
																				}
																			}
																			break;
																		case ((unsigned char) '%'):
																		case ((unsigned char) 'n'):

																			bgl_display_char(((unsigned char) 10),
																				BgL_pz00_30);
																			{	/* Ieee/output.scm 538 */
																				long BgL_arg1443z00_1841;

																				BgL_arg1443z00_1841 =
																					(BgL_iz00_1806 + 1L);
																				{
																					obj_t BgL_iz00_4467;

																					BgL_iz00_4467 =
																						BINT(BgL_arg1443z00_1841);
																					BgL_iz00_1704 = BgL_iz00_4467;
																					goto
																						BgL_zc3z04anonymousza31344ze3z87_1706;
																				}
																			}
																			break;
																		case ((unsigned char) 'r'):

																			bgl_display_char(((unsigned char) 13),
																				BgL_pz00_30);
																			{	/* Ieee/output.scm 541 */
																				long BgL_arg1444z00_1842;

																				BgL_arg1444z00_1842 =
																					(BgL_iz00_1806 + 1L);
																				{
																					obj_t BgL_iz00_4471;

																					BgL_iz00_4471 =
																						BINT(BgL_arg1444z00_1842);
																					BgL_iz00_1704 = BgL_iz00_4471;
																					goto
																						BgL_zc3z04anonymousza31344ze3z87_1706;
																				}
																			}
																			break;
																		case ((unsigned char) 'l'):
																		case ((unsigned char) 'L'):

																			{	/* Ieee/output.scm 543 */
																				obj_t BgL_arg1445z00_1843;

																				if (NULLP(BgL_osz00_1705))
																					{	/* Ieee/output.scm 427 */
																						BgL_arg1445z00_1843 =
																							BGl_errorz00zz__errorz00
																							(BgL_procnamez00_29,
																							BGl_string2265z00zz__r4_output_6_10_3z00,
																							BCHAR(BgL_fz00_1805));
																					}
																				else
																					{	/* Ieee/output.scm 427 */
																						BgL_arg1445z00_1843 =
																							CAR(((obj_t) BgL_osz00_1705));
																					}
																				BGl_printzd2flatzd2listze70ze7zz__r4_output_6_10_3z00
																					(BgL_arg1445z00_1843, BgL_pz00_30,
																					BGl_string2270z00zz__r4_output_6_10_3z00);
																			}
																			{	/* Ieee/output.scm 544 */
																				long BgL_arg1446z00_1844;
																				obj_t BgL_arg1447z00_1845;

																				BgL_arg1446z00_1844 =
																					(BgL_iz00_1806 + 1L);
																				BgL_arg1447z00_1845 =
																					CDR(((obj_t) BgL_osz00_1705));
																				{
																					obj_t BgL_osz00_4485;
																					obj_t BgL_iz00_4483;

																					BgL_iz00_4483 =
																						BINT(BgL_arg1446z00_1844);
																					BgL_osz00_4485 = BgL_arg1447z00_1845;
																					BgL_osz00_1705 = BgL_osz00_4485;
																					BgL_iz00_1704 = BgL_iz00_4483;
																					goto
																						BgL_zc3z04anonymousza31344ze3z87_1706;
																				}
																			}
																			break;
																		case ((unsigned char) '('):

																			{	/* Ieee/output.scm 546 */
																				obj_t BgL_niz00_1846;

																				{	/* Ieee/output.scm 546 */
																					obj_t BgL_arg1449z00_1848;

																					if (NULLP(BgL_osz00_1705))
																						{	/* Ieee/output.scm 427 */
																							BgL_arg1449z00_1848 =
																								BGl_errorz00zz__errorz00
																								(BgL_procnamez00_29,
																								BGl_string2265z00zz__r4_output_6_10_3z00,
																								BCHAR(BgL_fz00_1805));
																						}
																					else
																						{	/* Ieee/output.scm 427 */
																							BgL_arg1449z00_1848 =
																								CAR(((obj_t) BgL_osz00_1705));
																						}
																					{	/* Ieee/output.scm 452 */
																						obj_t BgL_jz00_2984;

																						BgL_jz00_2984 =
																							BGl_stringzd2indexzd2zz__r4_strings_6_7z00
																							(BgL__fmtz00_31,
																							BCHAR(((unsigned char) ')')),
																							BINT(BgL_iz00_1806));
																						if (CBOOL(BgL_jz00_2984))
																							{	/* Ieee/output.scm 455 */
																								obj_t BgL_sepz00_2985;

																								BgL_sepz00_2985 =
																									c_substring(BgL__fmtz00_31,
																									(BgL_iz00_1806 + 1L),
																									(long) CINT(BgL_jz00_2984));
																								BGl_printzd2flatzd2listze70ze7zz__r4_output_6_10_3z00
																									(BgL_arg1449z00_1848,
																									BgL_pz00_30, BgL_sepz00_2985);
																								BgL_niz00_1846 =
																									ADDFX(BgL_jz00_2984,
																									BINT(1L));
																							}
																						else
																							{	/* Ieee/output.scm 453 */
																								BgL_niz00_1846 =
																									BGl_errorz00zz__errorz00
																									(BgL_procnamez00_29,
																									BGl_string2271z00zz__r4_output_6_10_3z00,
																									BgL__fmtz00_31);
																							}
																					}
																				}
																				{	/* Ieee/output.scm 547 */
																					obj_t BgL_arg1448z00_1847;

																					BgL_arg1448z00_1847 =
																						CDR(((obj_t) BgL_osz00_1705));
																					{
																						obj_t BgL_osz00_4507;
																						obj_t BgL_iz00_4506;

																						BgL_iz00_4506 = BgL_niz00_1846;
																						BgL_osz00_4507 =
																							BgL_arg1448z00_1847;
																						BgL_osz00_1705 = BgL_osz00_4507;
																						BgL_iz00_1704 = BgL_iz00_4506;
																						goto
																							BgL_zc3z04anonymousza31344ze3z87_1706;
																					}
																				}
																			}
																			break;
																		case ((unsigned char) '~'):

																			bgl_display_char(((unsigned char) '~'),
																				BgL_pz00_30);
																			{	/* Ieee/output.scm 550 */
																				long BgL_arg1450z00_1849;

																				BgL_arg1450z00_1849 =
																					(BgL_iz00_1806 + 1L);
																				{
																					obj_t BgL_iz00_4510;

																					BgL_iz00_4510 =
																						BINT(BgL_arg1450z00_1849);
																					BgL_iz00_1704 = BgL_iz00_4510;
																					goto
																						BgL_zc3z04anonymousza31344ze3z87_1706;
																				}
																			}
																			break;
																		default:
																			if (isdigit(BgL_fz00_1805))
																				{	/* Ieee/output.scm 553 */
																					obj_t BgL_niz00_1851;

																					{	/* Ieee/output.scm 553 */
																						obj_t BgL_arg1453z00_1853;

																						if (NULLP(BgL_osz00_1705))
																							{	/* Ieee/output.scm 427 */
																								BgL_arg1453z00_1853 =
																									BGl_errorz00zz__errorz00
																									(BgL_procnamez00_29,
																									BGl_string2265z00zz__r4_output_6_10_3z00,
																									BCHAR(BgL_fz00_1805));
																							}
																						else
																							{	/* Ieee/output.scm 427 */
																								BgL_arg1453z00_1853 =
																									CAR(((obj_t) BgL_osz00_1705));
																							}
																						BgL_iz00_1786 = BgL_iz00_1806;
																						BgL_numz00_1787 =
																							BgL_arg1453z00_1853;
																						BgL_pz00_1788 = BgL_pz00_30;
																						if (BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_numz00_1787))
																							{	/* Ieee/output.scm 482 */
																								obj_t BgL_jz00_1791;

																								BgL_jz00_1791 =
																									BGl_stringzd2skipzd2zz__r4_strings_6_7z00
																									(BgL__fmtz00_31,
																									BGl_string2272z00zz__r4_output_6_10_3z00,
																									BINT(BgL_iz00_1786));
																								if (CBOOL(BgL_jz00_1791))
																									{	/* Ieee/output.scm 484 */
																										if (
																											(STRING_REF
																												(BgL__fmtz00_31,
																													(long)
																													CINT(BgL_jz00_1791))
																												==
																												((unsigned char) ',')))
																											{	/* Ieee/output.scm 486 */
																												if (
																													((long)
																														CINT(BgL_jz00_1791)
																														==
																														(BgL_lenz00_1702 -
																															1L)))
																													{	/* Ieee/output.scm 487 */
																														BgL_niz00_1851 =
																															BGl_errorz00zz__errorz00
																															(BgL_procnamez00_29,
																															BGl_string2271z00zz__r4_output_6_10_3z00,
																															BgL__fmtz00_31);
																													}
																												else
																													{	/* Ieee/output.scm 489 */
																														long
																															BgL_arg1401z00_1796;
																														long
																															BgL_arg1402z00_1797;
																														unsigned char
																															BgL_arg1403z00_1798;
																														BgL_arg1401z00_1796
																															=
																															((long)
																															CINT
																															(BgL_jz00_1791) +
																															2L);
																														{	/* Ieee/output.scm 491 */
																															obj_t
																																BgL_arg1404z00_1799;
																															BgL_arg1404z00_1799
																																=
																																c_substring
																																(BgL__fmtz00_31,
																																BgL_iz00_1786,
																																(long)
																																CINT
																																(BgL_jz00_1791));
																															{	/* Ieee/output.scm 491 */
																																char
																																	*BgL_tmpz00_4539;
																																BgL_tmpz00_4539
																																	=
																																	BSTRING_TO_STRING
																																	(BgL_arg1404z00_1799);
																																BgL_arg1402z00_1797
																																	=
																																	BGL_STRTOL
																																	(BgL_tmpz00_4539,
																																	0L, 10L);
																														}}
																														BgL_arg1403z00_1798
																															=
																															STRING_REF
																															(BgL__fmtz00_31,
																															((long)
																																CINT
																																(BgL_jz00_1791)
																																+ 1L));
																														BgL_iz00_1772 =
																															BINT
																															(BgL_arg1401z00_1796);
																														BgL_numz00_1773 =
																															BgL_numz00_1787;
																														BgL_mincolz00_1774 =
																															BgL_arg1402z00_1797;
																														BgL_paddingz00_1775
																															=
																															BgL_arg1403z00_1798;
																													BgL_zc3z04anonymousza31388ze3z87_1776:
																														if (
																															((long)
																																CINT
																																(BgL_iz00_1772)
																																==
																																BgL_lenz00_1702))
																															{	/* Ieee/output.scm 460 */
																																BgL_niz00_1851 =
																																	BGl_errorz00zz__errorz00
																																	(BgL_procnamez00_29,
																																	BGl_string2271z00zz__r4_output_6_10_3z00,
																																	BgL__fmtz00_31);
																															}
																														else
																															{	/* Ieee/output.scm 462 */
																																obj_t
																																	BgL_sz00_1778;
																																{	/* Ieee/output.scm 462 */
																																	unsigned char
																																		BgL_aux1046z00_1784;
																																	BgL_aux1046z00_1784
																																		=
																																		STRING_REF
																																		(BgL__fmtz00_31,
																																		(long)
																																		CINT
																																		(BgL_iz00_1772));
																																	switch
																																		(BgL_aux1046z00_1784)
																																		{
																																		case ((unsigned char) 'd'):
																																		case ((unsigned char) 'D'):

																																			BgL_sz00_1778 = BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(BgL_numz00_1773, BINT(10L));
																																			break;
																																		case ((unsigned char) 'x'):
																																		case ((unsigned char) 'X'):

																																			BgL_sz00_1778 = BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(BgL_numz00_1773, BINT(16L));
																																			break;
																																		case ((unsigned char) 'o'):
																																		case ((unsigned char) 'O'):

																																			BgL_sz00_1778 = BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(BgL_numz00_1773, BINT(8L));
																																			break;
																																		case ((unsigned char) 'b'):
																																		case ((unsigned char) 'B'):

																																			BgL_sz00_1778 = BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(BgL_numz00_1773, BINT(2L));
																																			break;
																																		default:
																																			BgL_sz00_1778 = BGl_errorz00zz__errorz00(BgL_procnamez00_29, BGl_string2271z00zz__r4_output_6_10_3z00, BgL__fmtz00_31);
																																		}
																																}
																																{	/* Ieee/output.scm 462 */
																																	long
																																		BgL_lz00_1779;
																																	BgL_lz00_1779
																																		=
																																		STRING_LENGTH
																																		(((obj_t)
																																			BgL_sz00_1778));
																																	{	/* Ieee/output.scm 473 */

																																		if (
																																			(BgL_lz00_1779
																																				<
																																				BgL_mincolz00_1774))
																																			{	/* Ieee/output.scm 474 */
																																				bgl_display_obj
																																					(make_string
																																					((BgL_mincolz00_1774 - BgL_lz00_1779), BgL_paddingz00_1775), BgL_pz00_30);
																																			}
																																		else
																																			{	/* Ieee/output.scm 474 */
																																				BFALSE;
																																			}
																																		bgl_display_obj
																																			(BgL_sz00_1778,
																																			BgL_pz00_30);
																																		BgL_niz00_1851
																																			=
																																			ADDFX
																																			(BgL_iz00_1772,
																																			BINT(1L));
																																	}
																																}
																															}
																													}
																											}
																										else
																											{	/* Ieee/output.scm 496 */
																												long
																													BgL_arg1407z00_1802;
																												{	/* Ieee/output.scm 496 */
																													obj_t
																														BgL_arg1408z00_1803;
																													BgL_arg1408z00_1803 =
																														c_substring
																														(BgL__fmtz00_31,
																														BgL_iz00_1786,
																														(long)
																														CINT
																														(BgL_jz00_1791));
																													{	/* Ieee/output.scm 496 */
																														char
																															*BgL_tmpz00_4574;
																														BgL_tmpz00_4574 =
																															BSTRING_TO_STRING
																															(BgL_arg1408z00_1803);
																														BgL_arg1407z00_1802
																															=
																															BGL_STRTOL
																															(BgL_tmpz00_4574,
																															0L, 10L);
																												}}
																												{
																													unsigned char
																														BgL_paddingz00_4580;
																													long
																														BgL_mincolz00_4579;
																													obj_t BgL_numz00_4578;
																													obj_t BgL_iz00_4577;

																													BgL_iz00_4577 =
																														BgL_jz00_1791;
																													BgL_numz00_4578 =
																														BgL_numz00_1787;
																													BgL_mincolz00_4579 =
																														BgL_arg1407z00_1802;
																													BgL_paddingz00_4580 =
																														((unsigned char)
																														' ');
																													BgL_paddingz00_1775 =
																														BgL_paddingz00_4580;
																													BgL_mincolz00_1774 =
																														BgL_mincolz00_4579;
																													BgL_numz00_1773 =
																														BgL_numz00_4578;
																													BgL_iz00_1772 =
																														BgL_iz00_4577;
																													goto
																														BgL_zc3z04anonymousza31388ze3z87_1776;
																												}
																											}
																									}
																								else
																									{	/* Ieee/output.scm 484 */
																										BgL_niz00_1851 =
																											BGl_errorz00zz__errorz00
																											(BgL_procnamez00_29,
																											BGl_string2271z00zz__r4_output_6_10_3z00,
																											BgL__fmtz00_31);
																									}
																							}
																						else
																							{	/* Ieee/output.scm 480 */
																								BgL_niz00_1851 =
																									BGl_bigloozd2typezd2errorz00zz__errorz00
																									(BgL_procnamez00_29,
																									BGl_string2269z00zz__r4_output_6_10_3z00,
																									BgL_numz00_1787);
																							}
																					}
																					{	/* Ieee/output.scm 554 */
																						obj_t BgL_arg1452z00_1852;

																						BgL_arg1452z00_1852 =
																							CDR(((obj_t) BgL_osz00_1705));
																						{
																							obj_t BgL_osz00_4586;
																							obj_t BgL_iz00_4585;

																							BgL_iz00_4585 = BgL_niz00_1851;
																							BgL_osz00_4586 =
																								BgL_arg1452z00_1852;
																							BgL_osz00_1705 = BgL_osz00_4586;
																							BgL_iz00_1704 = BgL_iz00_4585;
																							goto
																								BgL_zc3z04anonymousza31344ze3z87_1706;
																						}
																					}
																				}
																			else
																				{	/* Ieee/output.scm 557 */
																					obj_t BgL_arg1454z00_1854;

																					{	/* Ieee/output.scm 557 */
																						obj_t BgL_arg1455z00_1855;

																						{	/* Ieee/output.scm 557 */
																							obj_t BgL_list1456z00_1856;

																							BgL_list1456z00_1856 =
																								MAKE_YOUNG_PAIR(BCHAR
																								(BgL_fz00_1805), BNIL);
																							BgL_arg1455z00_1855 =
																								BGl_listzd2ze3stringz31zz__r4_strings_6_7z00
																								(BgL_list1456z00_1856);
																						}
																						BgL_arg1454z00_1854 =
																							string_append_3
																							(BGl_string2266z00zz__r4_output_6_10_3z00,
																							BgL_arg1455z00_1855,
																							BGl_string2267z00zz__r4_output_6_10_3z00);
																					}
																					return
																						BGl_errorz00zz__errorz00
																						(BgL_procnamez00_29,
																						BgL_arg1454z00_1854,
																						BgL__fmtz00_31);
																				}
																		}
																}
															}
													}
												else
													{
														bool_t BgL_altzf3zf3_4605;
														long BgL_iz00_4602;
														unsigned char BgL_fz00_4598;

														BgL_fz00_4598 =
															STRING_REF(BgL__fmtz00_31,
															((long) CINT(BgL_iz00_1704) + 1L));
														BgL_iz00_4602 = ((long) CINT(BgL_iz00_1704) + 1L);
														BgL_altzf3zf3_4605 = ((bool_t) 0);
														BgL_altzf3zf3_1807 = BgL_altzf3zf3_4605;
														BgL_iz00_1806 = BgL_iz00_4602;
														BgL_fz00_1805 = BgL_fz00_4598;
														goto BgL_zc3z04anonymousza31411ze3z87_1808;
													}
											}
									}
								else
									{	/* Ieee/output.scm 561 */
										bgl_display_char(BgL_cz00_1715, BgL_pz00_30);
										{	/* Ieee/output.scm 581 */
											long BgL_arg1367z00_1736;

											BgL_arg1367z00_1736 = ((long) CINT(BgL_iz00_1704) + 1L);
											{
												obj_t BgL_iz00_4609;

												BgL_iz00_4609 = BINT(BgL_arg1367z00_1736);
												BgL_iz00_1704 = BgL_iz00_4609;
												goto BgL_zc3z04anonymousza31344ze3z87_1706;
											}
										}
									}
							}
						else
							{	/* Ieee/output.scm 559 */
								return BFALSE;
							}
					}
				}
			}
		}

	}



/* print-flat-list~0 */
	obj_t BGl_printzd2flatzd2listze70ze7zz__r4_output_6_10_3z00(obj_t
		BgL_lz00_1746, obj_t BgL_pz00_1747, obj_t BgL_sepz00_1748)
	{
		{	/* Ieee/output.scm 449 */
		BGl_printzd2flatzd2listze70ze7zz__r4_output_6_10_3z00:
			if (PAIRP(BgL_lz00_1746))
				{
					obj_t BgL_lz00_1752;

					BgL_lz00_1752 = BgL_lz00_1746;
				BgL_zc3z04anonymousza31375ze3z87_1753:
					{	/* Ieee/output.scm 440 */
						obj_t BgL_arg1376z00_1754;

						BgL_arg1376z00_1754 = CAR(((obj_t) BgL_lz00_1752));
						BGl_printzd2flatzd2listze70ze7zz__r4_output_6_10_3z00
							(BgL_arg1376z00_1754, BgL_pz00_1747, BgL_sepz00_1748);
					}
					{	/* Ieee/output.scm 442 */
						bool_t BgL_test2518z00_4617;

						{	/* Ieee/output.scm 442 */
							obj_t BgL_tmpz00_4618;

							BgL_tmpz00_4618 = CDR(((obj_t) BgL_lz00_1752));
							BgL_test2518z00_4617 = PAIRP(BgL_tmpz00_4618);
						}
						if (BgL_test2518z00_4617)
							{	/* Ieee/output.scm 442 */
								bgl_display_obj(BgL_sepz00_1748, BgL_pz00_1747);
								{	/* Ieee/output.scm 444 */
									obj_t BgL_arg1379z00_1757;

									BgL_arg1379z00_1757 = CDR(((obj_t) BgL_lz00_1752));
									{
										obj_t BgL_lz00_4625;

										BgL_lz00_4625 = BgL_arg1379z00_1757;
										BgL_lz00_1752 = BgL_lz00_4625;
										goto BgL_zc3z04anonymousza31375ze3z87_1753;
									}
								}
							}
						else
							{	/* Ieee/output.scm 442 */
								if (NULLP(CDR(((obj_t) BgL_lz00_1752))))
									{	/* Ieee/output.scm 445 */
										return BFALSE;
									}
								else
									{	/* Ieee/output.scm 445 */
										bgl_display_string(BGl_string2274z00zz__r4_output_6_10_3z00,
											BgL_pz00_1747);
										{	/* Ieee/output.scm 447 */
											obj_t BgL_arg1382z00_1760;

											BgL_arg1382z00_1760 = CDR(((obj_t) BgL_lz00_1752));
											{
												obj_t BgL_lz00_4633;

												BgL_lz00_4633 = BgL_arg1382z00_1760;
												BgL_lz00_1746 = BgL_lz00_4633;
												goto
													BGl_printzd2flatzd2listze70ze7zz__r4_output_6_10_3z00;
											}
										}
									}
							}
					}
				}
			else
				{	/* Ieee/output.scm 438 */
					if (NULLP(BgL_lz00_1746))
						{	/* Ieee/output.scm 448 */
							return BFALSE;
						}
					else
						{	/* Ieee/output.scm 448 */
							return bgl_display_obj(BgL_lz00_1746, BgL_pz00_1747);
						}
				}
		}

	}



/* format */
	BGL_EXPORTED_DEF obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t
		BgL_fmtz00_33, obj_t BgL_objz00_34)
	{
		{	/* Ieee/output.scm 586 */
			{	/* Ieee/output.scm 587 */
				obj_t BgL_pz00_3030;

				{	/* Ieee/output.scm 587 */

					{	/* Ieee/output.scm 587 */

						BgL_pz00_3030 =
							BGl_openzd2outputzd2stringz00zz__r4_ports_6_10_1z00(BTRUE);
					}
				}
				BGl_xprintfz00zz__r4_output_6_10_3z00
					(BGl_symbol2275z00zz__r4_output_6_10_3z00, BgL_pz00_3030,
					BgL_fmtz00_33, BgL_objz00_34);
				return bgl_close_output_port(BgL_pz00_3030);
			}
		}

	}



/* &format */
	obj_t BGl_z62formatz62zz__r4_output_6_10_3z00(obj_t BgL_envz00_3874,
		obj_t BgL_fmtz00_3875, obj_t BgL_objz00_3876)
	{
		{	/* Ieee/output.scm 586 */
			{	/* Ieee/output.scm 587 */
				obj_t BgL_auxz00_4640;

				if (STRINGP(BgL_fmtz00_3875))
					{	/* Ieee/output.scm 587 */
						BgL_auxz00_4640 = BgL_fmtz00_3875;
					}
				else
					{
						obj_t BgL_auxz00_4643;

						BgL_auxz00_4643 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2247z00zz__r4_output_6_10_3z00, BINT(20551L),
							BGl_string2277z00zz__r4_output_6_10_3z00,
							BGl_string2278z00zz__r4_output_6_10_3z00, BgL_fmtz00_3875);
						FAILURE(BgL_auxz00_4643, BFALSE, BFALSE);
					}
				return
					BGl_formatz00zz__r4_output_6_10_3z00(BgL_auxz00_4640,
					BgL_objz00_3876);
			}
		}

	}



/* printf */
	BGL_EXPORTED_DEF obj_t BGl_printfz00zz__r4_output_6_10_3z00(obj_t
		BgL_fmtz00_35, obj_t BgL_objz00_36)
	{
		{	/* Ieee/output.scm 594 */
			{	/* Ieee/output.scm 595 */
				obj_t BgL_arg1457z00_3033;

				{	/* Ieee/output.scm 595 */
					obj_t BgL_tmpz00_4648;

					BgL_tmpz00_4648 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg1457z00_3033 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4648);
				}
				return
					BGl_xprintfz00zz__r4_output_6_10_3z00
					(BGl_symbol2279z00zz__r4_output_6_10_3z00, BgL_arg1457z00_3033,
					BgL_fmtz00_35, BgL_objz00_36);
			}
		}

	}



/* &printf */
	obj_t BGl_z62printfz62zz__r4_output_6_10_3z00(obj_t BgL_envz00_3877,
		obj_t BgL_fmtz00_3878, obj_t BgL_objz00_3879)
	{
		{	/* Ieee/output.scm 594 */
			{	/* Ieee/output.scm 595 */
				obj_t BgL_auxz00_4652;

				if (STRINGP(BgL_fmtz00_3878))
					{	/* Ieee/output.scm 595 */
						BgL_auxz00_4652 = BgL_fmtz00_3878;
					}
				else
					{
						obj_t BgL_auxz00_4655;

						BgL_auxz00_4655 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2247z00zz__r4_output_6_10_3z00, BINT(20917L),
							BGl_string2281z00zz__r4_output_6_10_3z00,
							BGl_string2278z00zz__r4_output_6_10_3z00, BgL_fmtz00_3878);
						FAILURE(BgL_auxz00_4655, BFALSE, BFALSE);
					}
				return
					BGl_printfz00zz__r4_output_6_10_3z00(BgL_auxz00_4652,
					BgL_objz00_3879);
			}
		}

	}



/* fprintf */
	BGL_EXPORTED_DEF obj_t BGl_fprintfz00zz__r4_output_6_10_3z00(obj_t
		BgL_portz00_37, obj_t BgL_fmtz00_38, obj_t BgL_objz00_39)
	{
		{	/* Ieee/output.scm 600 */
			return
				BGl_xprintfz00zz__r4_output_6_10_3z00
				(BGl_symbol2282z00zz__r4_output_6_10_3z00, BgL_portz00_37,
				BgL_fmtz00_38, BgL_objz00_39);
		}

	}



/* &fprintf */
	obj_t BGl_z62fprintfz62zz__r4_output_6_10_3z00(obj_t BgL_envz00_3880,
		obj_t BgL_portz00_3881, obj_t BgL_fmtz00_3882, obj_t BgL_objz00_3883)
	{
		{	/* Ieee/output.scm 600 */
			{	/* Ieee/output.scm 601 */
				obj_t BgL_auxz00_4668;
				obj_t BgL_auxz00_4661;

				if (STRINGP(BgL_fmtz00_3882))
					{	/* Ieee/output.scm 601 */
						BgL_auxz00_4668 = BgL_fmtz00_3882;
					}
				else
					{
						obj_t BgL_auxz00_4671;

						BgL_auxz00_4671 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2247z00zz__r4_output_6_10_3z00, BINT(21208L),
							BGl_string2284z00zz__r4_output_6_10_3z00,
							BGl_string2278z00zz__r4_output_6_10_3z00, BgL_fmtz00_3882);
						FAILURE(BgL_auxz00_4671, BFALSE, BFALSE);
					}
				if (OUTPUT_PORTP(BgL_portz00_3881))
					{	/* Ieee/output.scm 601 */
						BgL_auxz00_4661 = BgL_portz00_3881;
					}
				else
					{
						obj_t BgL_auxz00_4664;

						BgL_auxz00_4664 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2247z00zz__r4_output_6_10_3z00, BINT(21208L),
							BGl_string2284z00zz__r4_output_6_10_3z00,
							BGl_string2249z00zz__r4_output_6_10_3z00, BgL_portz00_3881);
						FAILURE(BgL_auxz00_4664, BFALSE, BFALSE);
					}
				return
					BGl_fprintfz00zz__r4_output_6_10_3z00(BgL_auxz00_4661,
					BgL_auxz00_4668, BgL_objz00_3883);
			}
		}

	}



/* display-2 */
	BGL_EXPORTED_DEF obj_t bgl_display_obj(obj_t BgL_objz00_40,
		obj_t BgL_portz00_41)
	{
		{	/* Ieee/output.scm 760 */
		bgl_display_obj:
			if (STRINGP(BgL_objz00_40))
				{	/* Ieee/output.scm 761 */
					return bgl_display_string(BgL_objz00_40, BgL_portz00_41);
				}
			else
				{	/* Ieee/output.scm 761 */
					if (SYMBOLP(BgL_objz00_40))
						{	/* Ieee/output.scm 773 */
							obj_t BgL_arg1598z00_3039;

							BgL_arg1598z00_3039 = SYMBOL_TO_STRING(BgL_objz00_40);
							return bgl_display_string(BgL_arg1598z00_3039, BgL_portz00_41);
						}
					else
						{	/* Ieee/output.scm 761 */
							if (INTEGERP(BgL_objz00_40))
								{	/* Ieee/output.scm 761 */
									return bgl_display_fixnum(BgL_objz00_40, BgL_portz00_41);
								}
							else
								{	/* Ieee/output.scm 761 */
									if (CHARP(BgL_objz00_40))
										{	/* Ieee/output.scm 761 */
											unsigned char BgL_tmpz00_4688;

											BgL_tmpz00_4688 = CCHAR(BgL_objz00_40);
											return bgl_display_char(BgL_tmpz00_4688, BgL_portz00_41);
										}
									else
										{	/* Ieee/output.scm 761 */
											if (PAIRP(BgL_objz00_40))
												{	/* Ieee/output.scm 761 */
													return
														BGl_displayzd2pairzd2zz__r4_output_6_10_3z00
														(BgL_objz00_40, BgL_portz00_41);
												}
											else
												{	/* Ieee/output.scm 761 */
													if (NULLP(BgL_objz00_40))
														{	/* Ieee/output.scm 761 */
															return
																bgl_display_string
																(BGl_string2285z00zz__r4_output_6_10_3z00,
																BgL_portz00_41);
														}
													else
														{	/* Ieee/output.scm 761 */
															if ((BgL_objz00_40 == BFALSE))
																{	/* Ieee/output.scm 761 */
																	return
																		bgl_display_string
																		(BGl_string2286z00zz__r4_output_6_10_3z00,
																		BgL_portz00_41);
																}
															else
																{	/* Ieee/output.scm 761 */
																	if ((BgL_objz00_40 == BTRUE))
																		{	/* Ieee/output.scm 761 */
																			return
																				bgl_display_string
																				(BGl_string2287z00zz__r4_output_6_10_3z00,
																				BgL_portz00_41);
																		}
																	else
																		{	/* Ieee/output.scm 761 */
																			if ((BgL_objz00_40 == BUNSPEC))
																				{	/* Ieee/output.scm 761 */
																					return
																						bgl_display_string
																						(BGl_string2288z00zz__r4_output_6_10_3z00,
																						BgL_portz00_41);
																				}
																			else
																				{	/* Ieee/output.scm 761 */
																					if (ELONGP(BgL_objz00_40))
																						{	/* Ieee/output.scm 761 */
																							return
																								bgl_display_elong(BELONG_TO_LONG
																								(BgL_objz00_40),
																								BgL_portz00_41);
																						}
																					else
																						{	/* Ieee/output.scm 761 */
																							if (REALP(BgL_objz00_40))
																								{	/* Ieee/output.scm 761 */
																									obj_t BgL_objz00_3051;

																									BgL_objz00_3051 =
																										DOUBLE_TO_REAL
																										(REAL_TO_DOUBLE
																										(BgL_objz00_40));
																									{	/* Ieee/output.scm 884 */
																										obj_t BgL_arg1641z00_3053;

																										BgL_arg1641z00_3053 =
																											bgl_real_to_string
																											(REAL_TO_DOUBLE
																											(BgL_objz00_3051));
																										return
																											bgl_display_string
																											(BgL_arg1641z00_3053,
																											BgL_portz00_41);
																									}
																								}
																							else
																								{	/* Ieee/output.scm 761 */
																									if (KEYWORDP(BgL_objz00_40))
																										{	/* Ieee/output.scm 761 */
																											bgl_display_char((
																													(unsigned char) ':'),
																												BgL_portz00_41);
																											{	/* Ieee/output.scm 866 */
																												obj_t
																													BgL_arg1640z00_3056;
																												BgL_arg1640z00_3056 =
																													KEYWORD_TO_STRING
																													(BgL_objz00_40);
																												return
																													bgl_display_string
																													(BgL_arg1640z00_3056,
																													BgL_portz00_41);
																											}
																										}
																									else
																										{	/* Ieee/output.scm 761 */
																											if (BGl_classzf3zf3zz__objectz00(BgL_objz00_40))
																												{	/* Ieee/output.scm 761 */
																													bgl_display_string
																														(BGl_string2289z00zz__r4_output_6_10_3z00,
																														BgL_portz00_41);
																													{	/* Ieee/output.scm 936 */
																														obj_t
																															BgL_arg1650z00_3060;
																														BgL_arg1650z00_3060
																															=
																															BGl_classzd2namezd2zz__objectz00
																															(BgL_objz00_40);
																														{	/* Ieee/output.scm 773 */
																															obj_t
																																BgL_arg1598z00_3065;
																															BgL_arg1598z00_3065
																																=
																																SYMBOL_TO_STRING
																																(BgL_arg1650z00_3060);
																															bgl_display_string
																																(BgL_arg1598z00_3065,
																																BgL_portz00_41);
																														}
																													}
																													return
																														bgl_display_string
																														(BGl_string2290z00zz__r4_output_6_10_3z00,
																														BgL_portz00_41);
																												}
																											else
																												{	/* Ieee/output.scm 761 */
																													if (VECTORP
																														(BgL_objz00_40))
																														{	/* Ieee/output.scm 761 */
																															return
																																BGl_writezf2displayzd2vectorz20zz__r4_output_6_10_3z00
																																(BgL_objz00_40,
																																BgL_portz00_41,
																																BGl_displayzd22zd2envz00zz__r4_output_6_10_3z00);
																														}
																													else
																														{	/* Ieee/output.scm 761 */
																															if (LLONGP
																																(BgL_objz00_40))
																																{	/* Ieee/output.scm 761 */
																																	return
																																		bgl_display_llong
																																		(BLLONG_TO_LLONG
																																		(BgL_objz00_40),
																																		BgL_portz00_41);
																																}
																															else
																																{	/* Ieee/output.scm 761 */
																																	if (UCS2_STRINGP(BgL_objz00_40))
																																		{	/* Ieee/output.scm 761 */
																																			return
																																				bgl_display_ucs2string
																																				(BgL_objz00_40,
																																				BgL_portz00_41);
																																		}
																																	else
																																		{	/* Ieee/output.scm 761 */
																																			if (STRUCTP(BgL_objz00_40))
																																				{	/* Ieee/output.scm 761 */
																																					return
																																						BGl_writezf2displayzd2structurez20zz__r4_output_6_10_3z00
																																						(BgL_objz00_40,
																																						BgL_portz00_41,
																																						BGl_displayzd22zd2envz00zz__r4_output_6_10_3z00);
																																				}
																																			else
																																				{	/* Ieee/output.scm 761 */
																																					if (BGL_OBJECTP(BgL_objz00_40))
																																						{	/* Ieee/output.scm 761 */
																																							obj_t
																																								BgL_list1473z00_1883;
																																							BgL_list1473z00_1883
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_portz00_41,
																																								BNIL);
																																							return
																																								BGl_objectzd2displayzd2zz__objectz00
																																								(
																																								((BgL_objectz00_bglt) BgL_objz00_40), BgL_list1473z00_1883);
																																						}
																																					else
																																						{	/* Ieee/output.scm 761 */
																																							if (BGL_DATEP(BgL_objz00_40))
																																								{	/* Ieee/output.scm 833 */
																																									obj_t
																																										BgL_tmpz00_4749;
																																									BgL_tmpz00_4749
																																										=
																																										BGl_datezd2ze3stringz31zz__datez00
																																										(BgL_objz00_40);
																																									return
																																										bgl_display_string
																																										(BgL_tmpz00_4749,
																																										BgL_portz00_41);
																																								}
																																							else
																																								{	/* Ieee/output.scm 761 */
																																									if (BGL_MUTEXP(BgL_objz00_40))
																																										{	/* Ieee/output.scm 761 */
																																											bgl_display_string
																																												(BGl_string2291z00zz__r4_output_6_10_3z00,
																																												BgL_portz00_41);
																																											{	/* Ieee/output.scm 918 */
																																												obj_t
																																													BgL_arg1646z00_3076;
																																												{	/* Ieee/output.scm 918 */
																																													obj_t
																																														BgL_tmpz00_4755;
																																													BgL_tmpz00_4755
																																														=
																																														(
																																														(obj_t)
																																														BgL_objz00_40);
																																													BgL_arg1646z00_3076
																																														=
																																														BGL_MUTEX_NAME
																																														(BgL_tmpz00_4755);
																																												}
																																												bgl_display_obj
																																													(BgL_arg1646z00_3076,
																																													BgL_portz00_41);
																																											}
																																											bgl_display_string
																																												(BGl_string2292z00zz__r4_output_6_10_3z00,
																																												BgL_portz00_41);
																																											{	/* Ieee/output.scm 920 */
																																												obj_t
																																													BgL_arg1648z00_3077;
																																												BgL_arg1648z00_3077
																																													=
																																													BGL_MUTEX_BACKEND
																																													(BgL_objz00_40);
																																												bgl_display_obj
																																													(BgL_arg1648z00_3077,
																																													BgL_portz00_41);
																																											}
																																											return
																																												bgl_display_string
																																												(BGl_string2290z00zz__r4_output_6_10_3z00,
																																												BgL_portz00_41);
																																										}
																																									else
																																										{	/* Ieee/output.scm 761 */
																																											if (BGL_CONDVARP(BgL_objz00_40))
																																												{	/* Ieee/output.scm 761 */
																																													bgl_display_string
																																														(BGl_string2293z00zz__r4_output_6_10_3z00,
																																														BgL_portz00_41);
																																													{	/* Ieee/output.scm 928 */
																																														obj_t
																																															BgL_arg1649z00_3085;
																																														{	/* Ieee/output.scm 928 */
																																															obj_t
																																																BgL_tmpz00_4766;
																																															BgL_tmpz00_4766
																																																=
																																																(
																																																(obj_t)
																																																BgL_objz00_40);
																																															BgL_arg1649z00_3085
																																																=
																																																BGL_CONDVAR_NAME
																																																(BgL_tmpz00_4766);
																																														}
																																														bgl_display_obj
																																															(BgL_arg1649z00_3085,
																																															BgL_portz00_41);
																																													}
																																													return
																																														bgl_display_string
																																														(BGl_string2290z00zz__r4_output_6_10_3z00,
																																														BgL_portz00_41);
																																												}
																																											else
																																												{	/* Ieee/output.scm 761 */
																																													if (UCS2P(BgL_objz00_40))
																																														{	/* Ieee/output.scm 761 */
																																															return
																																																bgl_display_ucs2
																																																(BgL_objz00_40,
																																																BgL_portz00_41);
																																														}
																																													else
																																														{	/* Ieee/output.scm 761 */
																																															if (CELLP(BgL_objz00_40))
																																																{	/* Ieee/output.scm 761 */
																																																	bgl_display_string
																																																		(BGl_string2294z00zz__r4_output_6_10_3z00,
																																																		BgL_portz00_41);
																																																	bgl_display_obj
																																																		(CELL_REF
																																																		(BgL_objz00_40),
																																																		BgL_portz00_41);
																																																	return
																																																		bgl_display_string
																																																		(BGl_string2290z00zz__r4_output_6_10_3z00,
																																																		BgL_portz00_41);
																																																}
																																															else
																																																{	/* Ieee/output.scm 761 */
																																																	if (EOF_OBJECTP(BgL_objz00_40))
																																																		{	/* Ieee/output.scm 761 */
																																																			return
																																																				bgl_display_string
																																																				(BGl_string2295z00zz__r4_output_6_10_3z00,
																																																				BgL_portz00_41);
																																																		}
																																																	else
																																																		{	/* Ieee/output.scm 761 */
																																																			if ((BgL_objz00_40 == (BOPTIONAL)))
																																																				{	/* Ieee/output.scm 761 */
																																																					return
																																																						bgl_display_string
																																																						(BGl_string2296z00zz__r4_output_6_10_3z00,
																																																						BgL_portz00_41);
																																																				}
																																																			else
																																																				{	/* Ieee/output.scm 761 */
																																																					if ((BgL_objz00_40 == (BREST)))
																																																						{	/* Ieee/output.scm 761 */
																																																							return
																																																								bgl_display_string
																																																								(BGl_string2297z00zz__r4_output_6_10_3z00,
																																																								BgL_portz00_41);
																																																						}
																																																					else
																																																						{	/* Ieee/output.scm 761 */
																																																							if ((BgL_objz00_40 == (BKEY)))
																																																								{	/* Ieee/output.scm 761 */
																																																									return
																																																										bgl_display_string
																																																										(BGl_string2298z00zz__r4_output_6_10_3z00,
																																																										BgL_portz00_41);
																																																								}
																																																							else
																																																								{	/* Ieee/output.scm 761 */
																																																									if (PROCEDUREP(BgL_objz00_40))
																																																										{	/* Ieee/output.scm 761 */
																																																											return
																																																												bgl_write_procedure
																																																												(BgL_objz00_40,
																																																												BgL_portz00_41);
																																																										}
																																																									else
																																																										{	/* Ieee/output.scm 761 */
																																																											if (OUTPUT_PORTP(BgL_objz00_40))
																																																												{	/* Ieee/output.scm 761 */
																																																													if (BGL_OUTPUT_STRING_PORTP(BgL_objz00_40))
																																																														{	/* Ieee/output.scm 761 */
																																																															return
																																																																bgl_display_string
																																																																(BGl_string2299z00zz__r4_output_6_10_3z00,
																																																																BgL_portz00_41);
																																																														}
																																																													else
																																																														{	/* Ieee/output.scm 761 */
																																																															if (BGL_OUTPUT_PROCEDURE_PORTP(BgL_objz00_40))
																																																																{	/* Ieee/output.scm 761 */
																																																																	return
																																																																		bgl_display_string
																																																																		(BGl_string2300z00zz__r4_output_6_10_3z00,
																																																																		BgL_portz00_41);
																																																																}
																																																															else
																																																																{	/* Ieee/output.scm 761 */
																																																																	return
																																																																		bgl_write_output_port
																																																																		(BgL_objz00_40,
																																																																		BgL_portz00_41);
																																																																}
																																																														}
																																																												}
																																																											else
																																																												{	/* Ieee/output.scm 761 */
																																																													if (INPUT_PORTP(BgL_objz00_40))
																																																														{	/* Ieee/output.scm 761 */
																																																															return
																																																																bgl_write_input_port
																																																																(BgL_objz00_40,
																																																																BgL_portz00_41);
																																																														}
																																																													else
																																																														{	/* Ieee/output.scm 761 */
																																																															if (BIGNUMP(BgL_objz00_40))
																																																																{	/* Ieee/output.scm 761 */
																																																																	return
																																																																		bgl_display_bignum
																																																																		(BgL_objz00_40,
																																																																		BgL_portz00_41);
																																																																}
																																																															else
																																																																{	/* Ieee/output.scm 761 */
																																																																	if (BGL_HVECTORP(BgL_objz00_40))
																																																																		{	/* Ieee/output.scm 761 */
																																																																			return
																																																																				BGl_writezf2displayzd2hvectorz20zz__r4_output_6_10_3z00
																																																																				(BgL_objz00_40,
																																																																				BgL_portz00_41,
																																																																				BGl_displayzd22zd2envz00zz__r4_output_6_10_3z00);
																																																																		}
																																																																	else
																																																																		{	/* Ieee/output.scm 761 */
																																																																			if (TVECTORP(BgL_objz00_40))
																																																																				{	/* Ieee/output.scm 761 */
																																																																					return
																																																																						BGl_writezf2displayzd2tvectorz20zz__r4_output_6_10_3z00
																																																																						(BgL_objz00_40,
																																																																						BgL_portz00_41,
																																																																						BGl_displayzd22zd2envz00zz__r4_output_6_10_3z00);
																																																																				}
																																																																			else
																																																																				{	/* Ieee/output.scm 761 */
																																																																					if (BGL_WEAKPTRP(BgL_objz00_40))
																																																																						{	/* Ieee/output.scm 1052 */
																																																																							obj_t
																																																																								BgL_dataz00_3109;
																																																																							BgL_dataz00_3109
																																																																								=
																																																																								bgl_weakptr_data
																																																																								(BgL_objz00_40);
																																																																							bgl_display_string
																																																																								(BGl_string2301z00zz__r4_output_6_10_3z00,
																																																																								BgL_portz00_41);
																																																																							bgl_display_obj
																																																																								(BgL_dataz00_3109,
																																																																								BgL_portz00_41);
																																																																							return
																																																																								bgl_display_char
																																																																								(
																																																																								((unsigned char) '>'), BgL_portz00_41);
																																																																						}
																																																																					else
																																																																						{	/* Ieee/output.scm 761 */
																																																																							if (FOREIGNP(BgL_objz00_40))
																																																																								{	/* Ieee/output.scm 761 */
																																																																									return
																																																																										bgl_write_foreign
																																																																										(BgL_objz00_40,
																																																																										BgL_portz00_41);
																																																																								}
																																																																							else
																																																																								{	/* Ieee/output.scm 761 */
																																																																									if (PROCESSP(BgL_objz00_40))
																																																																										{	/* Ieee/output.scm 761 */
																																																																											return
																																																																												bgl_write_process
																																																																												(BgL_objz00_40,
																																																																												BgL_portz00_41);
																																																																										}
																																																																									else
																																																																										{	/* Ieee/output.scm 761 */
																																																																											if (SOCKETP(BgL_objz00_40))
																																																																												{	/* Ieee/output.scm 761 */
																																																																													return
																																																																														bgl_write_socket
																																																																														(BgL_objz00_40,
																																																																														BgL_portz00_41);
																																																																												}
																																																																											else
																																																																												{	/* Ieee/output.scm 761 */
																																																																													if (BGL_DATAGRAM_SOCKETP(BgL_objz00_40))
																																																																														{	/* Ieee/output.scm 761 */
																																																																															return
																																																																																bgl_write_datagram_socket
																																																																																(BgL_objz00_40,
																																																																																BgL_portz00_41);
																																																																														}
																																																																													else
																																																																														{	/* Ieee/output.scm 761 */
																																																																															if (BGL_REGEXPP(BgL_objz00_40))
																																																																																{	/* Ieee/output.scm 761 */
																																																																																	return
																																																																																		bgl_write_regexp
																																																																																		(BgL_objz00_40,
																																																																																		BgL_portz00_41);
																																																																																}
																																																																															else
																																																																																{	/* Ieee/output.scm 761 */
																																																																																	if (BGL_MMAPP(BgL_objz00_40))
																																																																																		{	/* Ieee/output.scm 761 */
																																																																																			return
																																																																																				bgl_write_mmap
																																																																																				(BgL_objz00_40,
																																																																																				BgL_portz00_41);
																																																																																		}
																																																																																	else
																																																																																		{	/* Ieee/output.scm 761 */
																																																																																			if (BGL_SEMAPHOREP(BgL_objz00_40))
																																																																																				{	/* Ieee/output.scm 761 */
																																																																																					return
																																																																																						bgl_write_semaphore
																																																																																						(BgL_objz00_40,
																																																																																						BgL_portz00_41);
																																																																																				}
																																																																																			else
																																																																																				{	/* Ieee/output.scm 761 */
																																																																																					if (OPAQUEP(BgL_objz00_40))
																																																																																						{	/* Ieee/output.scm 761 */
																																																																																							BGL_TAIL
																																																																																								return
																																																																																								bgl_write_opaque
																																																																																								(BgL_objz00_40,
																																																																																								BgL_portz00_41);
																																																																																						}
																																																																																					else
																																																																																						{	/* Ieee/output.scm 761 */
																																																																																							if (CUSTOMP(BgL_objz00_40))
																																																																																								{	/* Ieee/output.scm 761 */
																																																																																									BGL_TAIL
																																																																																										return
																																																																																										bgl_write_custom
																																																																																										(BgL_objz00_40,
																																																																																										BgL_portz00_41);
																																																																																								}
																																																																																							else
																																																																																								{	/* Ieee/output.scm 761 */
																																																																																									if (BINARY_PORTP(BgL_objz00_40))
																																																																																										{	/* Ieee/output.scm 761 */
																																																																																											return
																																																																																												bgl_write_binary_port
																																																																																												(BgL_objz00_40,
																																																																																												BgL_portz00_41);
																																																																																										}
																																																																																									else
																																																																																										{	/* Ieee/output.scm 761 */
																																																																																											if (BGL_DYNAMIC_ENVP(BgL_objz00_40))
																																																																																												{	/* Ieee/output.scm 761 */
																																																																																													return
																																																																																														bgl_write_dynamic_env
																																																																																														(BgL_objz00_40,
																																																																																														BgL_portz00_41);
																																																																																												}
																																																																																											else
																																																																																												{	/* Ieee/output.scm 761 */
																																																																																													if (BGL_INT8P(BgL_objz00_40))
																																																																																														{	/* Ieee/output.scm 761 */
																																																																																															long
																																																																																																BgL_arg1501z00_1911;
																																																																																															{	/* Ieee/output.scm 761 */
																																																																																																int8_t
																																																																																																	BgL_xz00_3111;
																																																																																																BgL_xz00_3111
																																																																																																	=
																																																																																																	BGL_BINT8_TO_INT8
																																																																																																	(BgL_objz00_40);
																																																																																																{	/* Ieee/output.scm 761 */
																																																																																																	long
																																																																																																		BgL_arg1976z00_3112;
																																																																																																	BgL_arg1976z00_3112
																																																																																																		=
																																																																																																		(long)
																																																																																																		(BgL_xz00_3111);
																																																																																																	BgL_arg1501z00_1911
																																																																																																		=
																																																																																																		(long)
																																																																																																		(BgL_arg1976z00_3112);
																																																																																															}}
																																																																																															{
																																																																																																obj_t
																																																																																																	BgL_objz00_4860;
																																																																																																BgL_objz00_4860
																																																																																																	=
																																																																																																	BINT
																																																																																																	(BgL_arg1501z00_1911);
																																																																																																BgL_objz00_40
																																																																																																	=
																																																																																																	BgL_objz00_4860;
																																																																																																goto
																																																																																																	bgl_display_obj;
																																																																																															}
																																																																																														}
																																																																																													else
																																																																																														{	/* Ieee/output.scm 761 */
																																																																																															if (BGL_UINT8P(BgL_objz00_40))
																																																																																																{	/* Ieee/output.scm 761 */
																																																																																																	long
																																																																																																		BgL_arg1503z00_1913;
																																																																																																	{	/* Ieee/output.scm 761 */
																																																																																																		uint8_t
																																																																																																			BgL_xz00_3114;
																																																																																																		BgL_xz00_3114
																																																																																																			=
																																																																																																			BGL_BUINT8_TO_UINT8
																																																																																																			(BgL_objz00_40);
																																																																																																		{	/* Ieee/output.scm 761 */
																																																																																																			long
																																																																																																				BgL_arg1975z00_3115;
																																																																																																			BgL_arg1975z00_3115
																																																																																																				=
																																																																																																				(long)
																																																																																																				(BgL_xz00_3114);
																																																																																																			BgL_arg1503z00_1913
																																																																																																				=
																																																																																																				(long)
																																																																																																				(BgL_arg1975z00_3115);
																																																																																																	}}
																																																																																																	{
																																																																																																		obj_t
																																																																																																			BgL_objz00_4867;
																																																																																																		BgL_objz00_4867
																																																																																																			=
																																																																																																			BINT
																																																																																																			(BgL_arg1503z00_1913);
																																																																																																		BgL_objz00_40
																																																																																																			=
																																																																																																			BgL_objz00_4867;
																																																																																																		goto
																																																																																																			bgl_display_obj;
																																																																																																	}
																																																																																																}
																																																																																															else
																																																																																																{	/* Ieee/output.scm 761 */
																																																																																																	if (BGL_INT16P(BgL_objz00_40))
																																																																																																		{	/* Ieee/output.scm 761 */
																																																																																																			long
																																																																																																				BgL_arg1505z00_1915;
																																																																																																			{	/* Ieee/output.scm 761 */
																																																																																																				int16_t
																																																																																																					BgL_xz00_3117;
																																																																																																				BgL_xz00_3117
																																																																																																					=
																																																																																																					BGL_BINT16_TO_INT16
																																																																																																					(BgL_objz00_40);
																																																																																																				{	/* Ieee/output.scm 761 */
																																																																																																					long
																																																																																																						BgL_arg1974z00_3118;
																																																																																																					BgL_arg1974z00_3118
																																																																																																						=
																																																																																																						(long)
																																																																																																						(BgL_xz00_3117);
																																																																																																					BgL_arg1505z00_1915
																																																																																																						=
																																																																																																						(long)
																																																																																																						(BgL_arg1974z00_3118);
																																																																																																			}}
																																																																																																			{
																																																																																																				obj_t
																																																																																																					BgL_objz00_4874;
																																																																																																				BgL_objz00_4874
																																																																																																					=
																																																																																																					BINT
																																																																																																					(BgL_arg1505z00_1915);
																																																																																																				BgL_objz00_40
																																																																																																					=
																																																																																																					BgL_objz00_4874;
																																																																																																				goto
																																																																																																					bgl_display_obj;
																																																																																																			}
																																																																																																		}
																																																																																																	else
																																																																																																		{	/* Ieee/output.scm 761 */
																																																																																																			if (BGL_UINT16P(BgL_objz00_40))
																																																																																																				{	/* Ieee/output.scm 761 */
																																																																																																					long
																																																																																																						BgL_arg1507z00_1917;
																																																																																																					{	/* Ieee/output.scm 761 */
																																																																																																						uint16_t
																																																																																																							BgL_xz00_3120;
																																																																																																						BgL_xz00_3120
																																																																																																							=
																																																																																																							BGL_BUINT16_TO_UINT16
																																																																																																							(BgL_objz00_40);
																																																																																																						{	/* Ieee/output.scm 761 */
																																																																																																							long
																																																																																																								BgL_arg1973z00_3121;
																																																																																																							BgL_arg1973z00_3121
																																																																																																								=
																																																																																																								(long)
																																																																																																								(BgL_xz00_3120);
																																																																																																							BgL_arg1507z00_1917
																																																																																																								=
																																																																																																								(long)
																																																																																																								(BgL_arg1973z00_3121);
																																																																																																					}}
																																																																																																					{
																																																																																																						obj_t
																																																																																																							BgL_objz00_4881;
																																																																																																						BgL_objz00_4881
																																																																																																							=
																																																																																																							BINT
																																																																																																							(BgL_arg1507z00_1917);
																																																																																																						BgL_objz00_40
																																																																																																							=
																																																																																																							BgL_objz00_4881;
																																																																																																						goto
																																																																																																							bgl_display_obj;
																																																																																																					}
																																																																																																				}
																																																																																																			else
																																																																																																				{	/* Ieee/output.scm 761 */
																																																																																																					if (BGL_INT32P(BgL_objz00_40))
																																																																																																						{	/* Ieee/output.scm 761 */
																																																																																																							long
																																																																																																								BgL_arg1509z00_1919;
																																																																																																							{	/* Ieee/output.scm 761 */
																																																																																																								int32_t
																																																																																																									BgL_nz00_3123;
																																																																																																								BgL_nz00_3123
																																																																																																									=
																																																																																																									BGL_BINT32_TO_INT32
																																																																																																									(BgL_objz00_40);
																																																																																																								BgL_arg1509z00_1919
																																																																																																									=
																																																																																																									(long)
																																																																																																									(BgL_nz00_3123);
																																																																																																							}
																																																																																																							{
																																																																																																								obj_t
																																																																																																									BgL_objz00_4887;
																																																																																																								BgL_objz00_4887
																																																																																																									=
																																																																																																									make_belong
																																																																																																									(BgL_arg1509z00_1919);
																																																																																																								BgL_objz00_40
																																																																																																									=
																																																																																																									BgL_objz00_4887;
																																																																																																								goto
																																																																																																									bgl_display_obj;
																																																																																																							}
																																																																																																						}
																																																																																																					else
																																																																																																						{	/* Ieee/output.scm 761 */
																																																																																																							if (BGL_UINT32P(BgL_objz00_40))
																																																																																																								{	/* Ieee/output.scm 761 */
																																																																																																									BGL_LONGLONG_T
																																																																																																										BgL_arg1511z00_1921;
																																																																																																									{	/* Ieee/output.scm 761 */
																																																																																																										uint32_t
																																																																																																											BgL_nz00_3124;
																																																																																																										BgL_nz00_3124
																																																																																																											=
																																																																																																											BGL_BUINT32_TO_UINT32
																																																																																																											(BgL_objz00_40);
																																																																																																										BgL_arg1511z00_1921
																																																																																																											=
																																																																																																											(BGL_LONGLONG_T)
																																																																																																											(BgL_nz00_3124);
																																																																																																									}
																																																																																																									{
																																																																																																										obj_t
																																																																																																											BgL_objz00_4893;
																																																																																																										BgL_objz00_4893
																																																																																																											=
																																																																																																											make_bllong
																																																																																																											(BgL_arg1511z00_1921);
																																																																																																										BgL_objz00_40
																																																																																																											=
																																																																																																											BgL_objz00_4893;
																																																																																																										goto
																																																																																																											bgl_display_obj;
																																																																																																									}
																																																																																																								}
																																																																																																							else
																																																																																																								{	/* Ieee/output.scm 761 */
																																																																																																									if (BGL_INT64P(BgL_objz00_40))
																																																																																																										{	/* Ieee/output.scm 761 */
																																																																																																											BGL_LONGLONG_T
																																																																																																												BgL_arg1513z00_1923;
																																																																																																											{	/* Ieee/output.scm 761 */
																																																																																																												int64_t
																																																																																																													BgL_nz00_3125;
																																																																																																												BgL_nz00_3125
																																																																																																													=
																																																																																																													BGL_BINT64_TO_INT64
																																																																																																													(BgL_objz00_40);
																																																																																																												BgL_arg1513z00_1923
																																																																																																													=
																																																																																																													(BGL_LONGLONG_T)
																																																																																																													(BgL_nz00_3125);
																																																																																																											}
																																																																																																											{
																																																																																																												obj_t
																																																																																																													BgL_objz00_4899;
																																																																																																												BgL_objz00_4899
																																																																																																													=
																																																																																																													make_bllong
																																																																																																													(BgL_arg1513z00_1923);
																																																																																																												BgL_objz00_40
																																																																																																													=
																																																																																																													BgL_objz00_4899;
																																																																																																												goto
																																																																																																													bgl_display_obj;
																																																																																																											}
																																																																																																										}
																																																																																																									else
																																																																																																										{	/* Ieee/output.scm 761 */
																																																																																																											if (BGL_UINT64P(BgL_objz00_40))
																																																																																																												{	/* Ieee/output.scm 761 */
																																																																																																													{	/* Ieee/output.scm 761 */
																																																																																																														uint64_t
																																																																																																															BgL_vz00_1925;
																																																																																																														{	/* Ieee/output.scm 761 */
																																																																																																															uint64_t
																																																																																																																BgL_arg1517z00_1928;
																																																																																																															BgL_arg1517z00_1928
																																																																																																																=
																																																																																																																(uint64_t)
																																																																																																																(10L);
																																																																																																															{	/* Ieee/output.scm 761 */
																																																																																																																uint64_t
																																																																																																																	BgL_za71za7_3126;
																																																																																																																BgL_za71za7_3126
																																																																																																																	=
																																																																																																																	BGL_BINT64_TO_INT64
																																																																																																																	(BgL_objz00_40);
																																																																																																																BgL_vz00_1925
																																																																																																																	=
																																																																																																																	(BgL_za71za7_3126
																																																																																																																	/
																																																																																																																	BgL_arg1517z00_1928);
																																																																																																															}
																																																																																																														}
																																																																																																														if ((BgL_vz00_1925 > (uint64_t) (0)))
																																																																																																															{	/* Ieee/output.scm 761 */
																																																																																																																BGL_LONGLONG_T
																																																																																																																	BgL_arg1516z00_1927;
																																																																																																																BgL_arg1516z00_1927
																																																																																																																	=
																																																																																																																	(BGL_LONGLONG_T)
																																																																																																																	(BgL_vz00_1925);
																																																																																																																bgl_display_obj
																																																																																																																	(make_bllong
																																																																																																																	(BgL_arg1516z00_1927),
																																																																																																																	BgL_portz00_41);
																																																																																																															}
																																																																																																														else
																																																																																																															{	/* Ieee/output.scm 761 */
																																																																																																																BFALSE;
																																																																																																															}
																																																																																																													}
																																																																																																													{	/* Ieee/output.scm 761 */
																																																																																																														long
																																																																																																															BgL_arg1521z00_1929;
																																																																																																														{	/* Ieee/output.scm 761 */
																																																																																																															uint64_t
																																																																																																																BgL_arg1522z00_1930;
																																																																																																															{	/* Ieee/output.scm 761 */
																																																																																																																uint64_t
																																																																																																																	BgL_arg1523z00_1931;
																																																																																																																BgL_arg1523z00_1931
																																																																																																																	=
																																																																																																																	(uint64_t)
																																																																																																																	(10L);
																																																																																																																{	/* Ieee/output.scm 761 */
																																																																																																																	uint64_t
																																																																																																																		BgL_n1z00_3131;
																																																																																																																	BgL_n1z00_3131
																																																																																																																		=
																																																																																																																		BGL_BINT64_TO_INT64
																																																																																																																		(BgL_objz00_40);
																																																																																																																	{	/* Ieee/output.scm 761 */
																																																																																																																		int64_t
																																																																																																																			BgL_tmpz00_4913;
																																																																																																																		BgL_tmpz00_4913
																																																																																																																			=
																																																																																																																			(int64_t)
																																																																																																																			(BgL_arg1523z00_1931);
																																																																																																																		BgL_arg1522z00_1930
																																																																																																																			=
																																																																																																																			(BgL_n1z00_3131
																																																																																																																			%
																																																																																																																			BgL_tmpz00_4913);
																																																																																																															}}}
																																																																																																															{	/* Ieee/output.scm 761 */
																																																																																																																long
																																																																																																																	BgL_arg1970z00_3134;
																																																																																																																BgL_arg1970z00_3134
																																																																																																																	=
																																																																																																																	(long)
																																																																																																																	(BgL_arg1522z00_1930);
																																																																																																																BgL_arg1521z00_1929
																																																																																																																	=
																																																																																																																	(long)
																																																																																																																	(BgL_arg1970z00_3134);
																																																																																																														}}
																																																																																																														{
																																																																																																															obj_t
																																																																																																																BgL_objz00_4918;
																																																																																																															BgL_objz00_4918
																																																																																																																=
																																																																																																																BINT
																																																																																																																(BgL_arg1521z00_1929);
																																																																																																															BgL_objz00_40
																																																																																																																=
																																																																																																																BgL_objz00_4918;
																																																																																																															goto
																																																																																																																bgl_display_obj;
																																																																																																														}
																																																																																																													}
																																																																																																												}
																																																																																																											else
																																																																																																												{	/* Ieee/output.scm 761 */
																																																																																																													if (CNSTP(BgL_objz00_40))
																																																																																																														{	/* Ieee/output.scm 761 */
																																																																																																															BGL_TAIL
																																																																																																																return
																																																																																																																bgl_write_cnst
																																																																																																																(BgL_objz00_40,
																																																																																																																BgL_portz00_41);
																																																																																																														}
																																																																																																													else
																																																																																																														{	/* Ieee/output.scm 761 */
																																																																																																															BGL_TAIL
																																																																																																																return
																																																																																																																bgl_write_unknown
																																																																																																																(BgL_objz00_40,
																																																																																																																BgL_portz00_41);
																																																																																																														}
																																																																																																												}
																																																																																																										}
																																																																																																								}
																																																																																																						}
																																																																																																				}
																																																																																																		}
																																																																																																}
																																																																																														}
																																																																																												}
																																																																																										}
																																																																																								}
																																																																																						}
																																																																																				}
																																																																																		}
																																																																																}
																																																																														}
																																																																												}
																																																																										}
																																																																								}
																																																																						}
																																																																				}
																																																																		}
																																																																}
																																																														}
																																																												}
																																																										}
																																																								}
																																																						}
																																																				}
																																																		}
																																																}
																																														}
																																												}
																																										}
																																								}
																																						}
																																				}
																																		}
																																}
																														}
																												}
																										}
																								}
																						}
																				}
																		}
																}
														}
												}
										}
								}
						}
				}
		}

	}



/* &display-2 */
	obj_t BGl_z62displayzd22zb0zz__r4_output_6_10_3z00(obj_t BgL_envz00_3884,
		obj_t BgL_objz00_3885, obj_t BgL_portz00_3886)
	{
		{	/* Ieee/output.scm 760 */
			return bgl_display_obj(BgL_objz00_3885, BgL_portz00_3886);
		}

	}



/* write-2 */
	BGL_EXPORTED_DEF obj_t bgl_write_obj(obj_t BgL_objz00_42,
		obj_t BgL_portz00_43)
	{
		{	/* Ieee/output.scm 766 */
			if (STRINGP(BgL_objz00_42))
				{	/* Ieee/output.scm 767 */
					if (BGl_bigloozd2strictzd2r5rszd2stringszd2zz__paramz00())
						{	/* Ieee/output.scm 854 */
							obj_t BgL_strz00_3139;

							BgL_strz00_3139 = string_for_read(BgL_objz00_42);
							{	/* Ieee/output.scm 855 */
								obj_t BgL_escz00_3140;

								{	/* Ieee/output.scm 856 */
									obj_t BgL_tmpz00_3143;

									{	/* Ieee/output.scm 856 */
										int BgL_tmpz00_4930;

										BgL_tmpz00_4930 = (int) (1L);
										BgL_tmpz00_3143 = BGL_MVALUES_VAL(BgL_tmpz00_4930);
									}
									{	/* Ieee/output.scm 856 */
										int BgL_tmpz00_4933;

										BgL_tmpz00_4933 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_4933, BUNSPEC);
									}
									BgL_escz00_3140 = BgL_tmpz00_3143;
								}
								return
									bgl_write_string(BgL_strz00_3139,
									CBOOL(BgL_escz00_3140), BgL_portz00_43);
							}
						}
					else
						{	/* Ieee/output.scm 851 */
							return
								bgl_write_string(string_for_read(BgL_objz00_42), ((bool_t) 0),
								BgL_portz00_43);
						}
				}
			else
				{	/* Ieee/output.scm 767 */
					if (SYMBOLP(BgL_objz00_42))
						{	/* Ieee/output.scm 767 */
							return
								BGl_writezd2symbolzd2zz__r4_output_6_10_3z00(BgL_objz00_42,
								BgL_portz00_43);
						}
					else
						{	/* Ieee/output.scm 767 */
							if (INTEGERP(BgL_objz00_42))
								{	/* Ieee/output.scm 767 */
									return bgl_display_fixnum(BgL_objz00_42, BgL_portz00_43);
								}
							else
								{	/* Ieee/output.scm 767 */
									if (CHARP(BgL_objz00_42))
										{	/* Ieee/output.scm 767 */
											return bgl_write_char(BgL_objz00_42, BgL_portz00_43);
										}
									else
										{	/* Ieee/output.scm 767 */
											if (PAIRP(BgL_objz00_42))
												{	/* Ieee/output.scm 767 */
													return
														BGl_writezd2pairzd2zz__r4_output_6_10_3z00
														(BgL_objz00_42, BgL_portz00_43);
												}
											else
												{	/* Ieee/output.scm 767 */
													if (NULLP(BgL_objz00_42))
														{	/* Ieee/output.scm 767 */
															return
																bgl_display_string
																(BGl_string2285z00zz__r4_output_6_10_3z00,
																BgL_portz00_43);
														}
													else
														{	/* Ieee/output.scm 767 */
															if ((BgL_objz00_42 == BFALSE))
																{	/* Ieee/output.scm 767 */
																	return
																		bgl_display_string
																		(BGl_string2286z00zz__r4_output_6_10_3z00,
																		BgL_portz00_43);
																}
															else
																{	/* Ieee/output.scm 767 */
																	if ((BgL_objz00_42 == BTRUE))
																		{	/* Ieee/output.scm 767 */
																			return
																				bgl_display_string
																				(BGl_string2287z00zz__r4_output_6_10_3z00,
																				BgL_portz00_43);
																		}
																	else
																		{	/* Ieee/output.scm 767 */
																			if ((BgL_objz00_42 == BUNSPEC))
																				{	/* Ieee/output.scm 767 */
																					return
																						bgl_display_string
																						(BGl_string2288z00zz__r4_output_6_10_3z00,
																						BgL_portz00_43);
																				}
																			else
																				{	/* Ieee/output.scm 767 */
																					if (ELONGP(BgL_objz00_42))
																						{	/* Ieee/output.scm 767 */
																							return
																								bgl_write_elong(BELONG_TO_LONG
																								(BgL_objz00_42),
																								BgL_portz00_43);
																						}
																					else
																						{	/* Ieee/output.scm 767 */
																							if (REALP(BgL_objz00_42))
																								{	/* Ieee/output.scm 767 */
																									obj_t BgL_objz00_3153;

																									BgL_objz00_3153 =
																										DOUBLE_TO_REAL
																										(REAL_TO_DOUBLE
																										(BgL_objz00_42));
																									{	/* Ieee/output.scm 884 */
																										obj_t BgL_arg1641z00_3155;

																										BgL_arg1641z00_3155 =
																											bgl_real_to_string
																											(REAL_TO_DOUBLE
																											(BgL_objz00_3153));
																										return
																											bgl_display_string
																											(BgL_arg1641z00_3155,
																											BgL_portz00_43);
																									}
																								}
																							else
																								{	/* Ieee/output.scm 767 */
																									if (KEYWORDP(BgL_objz00_42))
																										{	/* Ieee/output.scm 767 */
																											bgl_display_char((
																													(unsigned char) ':'),
																												BgL_portz00_43);
																											{	/* Ieee/output.scm 866 */
																												obj_t
																													BgL_arg1640z00_3158;
																												BgL_arg1640z00_3158 =
																													KEYWORD_TO_STRING
																													(BgL_objz00_42);
																												return
																													bgl_display_string
																													(BgL_arg1640z00_3158,
																													BgL_portz00_43);
																											}
																										}
																									else
																										{	/* Ieee/output.scm 767 */
																											if (BGl_classzf3zf3zz__objectz00(BgL_objz00_42))
																												{	/* Ieee/output.scm 767 */
																													bgl_display_string
																														(BGl_string2289z00zz__r4_output_6_10_3z00,
																														BgL_portz00_43);
																													{	/* Ieee/output.scm 936 */
																														obj_t
																															BgL_arg1650z00_3162;
																														BgL_arg1650z00_3162
																															=
																															BGl_classzd2namezd2zz__objectz00
																															(BgL_objz00_42);
																														{	/* Ieee/output.scm 773 */
																															obj_t
																																BgL_arg1598z00_3167;
																															BgL_arg1598z00_3167
																																=
																																SYMBOL_TO_STRING
																																(BgL_arg1650z00_3162);
																															bgl_display_string
																																(BgL_arg1598z00_3167,
																																BgL_portz00_43);
																														}
																													}
																													return
																														bgl_display_string
																														(BGl_string2290z00zz__r4_output_6_10_3z00,
																														BgL_portz00_43);
																												}
																											else
																												{	/* Ieee/output.scm 767 */
																													if (VECTORP
																														(BgL_objz00_42))
																														{	/* Ieee/output.scm 767 */
																															return
																																BGl_writezf2displayzd2vectorz20zz__r4_output_6_10_3z00
																																(BgL_objz00_42,
																																BgL_portz00_43,
																																BGl_writezd22zd2envz00zz__r4_output_6_10_3z00);
																														}
																													else
																														{	/* Ieee/output.scm 767 */
																															if (LLONGP
																																(BgL_objz00_42))
																																{	/* Ieee/output.scm 767 */
																																	return
																																		bgl_write_llong
																																		(BLLONG_TO_LLONG
																																		(BgL_objz00_42),
																																		BgL_portz00_43);
																																}
																															else
																																{	/* Ieee/output.scm 767 */
																																	if (UCS2_STRINGP(BgL_objz00_42))
																																		{	/* Ieee/output.scm 767 */
																																			return
																																				bgl_write_utf8string
																																				(string_for_read
																																				(ucs2_string_to_utf8_string
																																					(BgL_objz00_42)),
																																				BgL_portz00_43);
																																		}
																																	else
																																		{	/* Ieee/output.scm 767 */
																																			if (STRUCTP(BgL_objz00_42))
																																				{	/* Ieee/output.scm 767 */
																																					return
																																						BGl_writezf2displayzd2structurez20zz__r4_output_6_10_3z00
																																						(BgL_objz00_42,
																																						BgL_portz00_43,
																																						BGl_writezd22zd2envz00zz__r4_output_6_10_3z00);
																																				}
																																			else
																																				{	/* Ieee/output.scm 767 */
																																					if (BGL_OBJECTP(BgL_objz00_42))
																																						{	/* Ieee/output.scm 767 */
																																							obj_t
																																								BgL_list1540z00_1948;
																																							BgL_list1540z00_1948
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_portz00_43,
																																								BNIL);
																																							return
																																								BGl_objectzd2writezd2zz__objectz00
																																								(
																																								((BgL_objectz00_bglt) BgL_objz00_42), BgL_list1540z00_1948);
																																						}
																																					else
																																						{	/* Ieee/output.scm 767 */
																																							if (BGL_DATEP(BgL_objz00_42))
																																								{	/* Ieee/output.scm 767 */
																																									bgl_display_string
																																										(BGl_string2302z00zz__r4_output_6_10_3z00,
																																										BgL_portz00_43);
																																									{	/* Ieee/output.scm 833 */
																																										obj_t
																																											BgL_tmpz00_5010;
																																										BgL_tmpz00_5010
																																											=
																																											BGl_datezd2ze3stringz31zz__datez00
																																											(BgL_objz00_42);
																																										bgl_display_string
																																											(BgL_tmpz00_5010,
																																											BgL_portz00_43);
																																									}
																																									return
																																										bgl_display_string
																																										(BGl_string2290z00zz__r4_output_6_10_3z00,
																																										BgL_portz00_43);
																																								}
																																							else
																																								{	/* Ieee/output.scm 767 */
																																									if (BGL_MUTEXP(BgL_objz00_42))
																																										{	/* Ieee/output.scm 767 */
																																											bgl_display_string
																																												(BGl_string2291z00zz__r4_output_6_10_3z00,
																																												BgL_portz00_43);
																																											{	/* Ieee/output.scm 918 */
																																												obj_t
																																													BgL_arg1646z00_3186;
																																												{	/* Ieee/output.scm 918 */
																																													obj_t
																																														BgL_tmpz00_5017;
																																													BgL_tmpz00_5017
																																														=
																																														(
																																														(obj_t)
																																														BgL_objz00_42);
																																													BgL_arg1646z00_3186
																																														=
																																														BGL_MUTEX_NAME
																																														(BgL_tmpz00_5017);
																																												}
																																												bgl_display_obj
																																													(BgL_arg1646z00_3186,
																																													BgL_portz00_43);
																																											}
																																											bgl_display_string
																																												(BGl_string2292z00zz__r4_output_6_10_3z00,
																																												BgL_portz00_43);
																																											{	/* Ieee/output.scm 920 */
																																												obj_t
																																													BgL_arg1648z00_3187;
																																												BgL_arg1648z00_3187
																																													=
																																													BGL_MUTEX_BACKEND
																																													(BgL_objz00_42);
																																												bgl_display_obj
																																													(BgL_arg1648z00_3187,
																																													BgL_portz00_43);
																																											}
																																											return
																																												bgl_display_string
																																												(BGl_string2290z00zz__r4_output_6_10_3z00,
																																												BgL_portz00_43);
																																										}
																																									else
																																										{	/* Ieee/output.scm 767 */
																																											if (BGL_CONDVARP(BgL_objz00_42))
																																												{	/* Ieee/output.scm 767 */
																																													bgl_display_string
																																														(BGl_string2293z00zz__r4_output_6_10_3z00,
																																														BgL_portz00_43);
																																													{	/* Ieee/output.scm 928 */
																																														obj_t
																																															BgL_arg1649z00_3195;
																																														{	/* Ieee/output.scm 928 */
																																															obj_t
																																																BgL_tmpz00_5028;
																																															BgL_tmpz00_5028
																																																=
																																																(
																																																(obj_t)
																																																BgL_objz00_42);
																																															BgL_arg1649z00_3195
																																																=
																																																BGL_CONDVAR_NAME
																																																(BgL_tmpz00_5028);
																																														}
																																														bgl_display_obj
																																															(BgL_arg1649z00_3195,
																																															BgL_portz00_43);
																																													}
																																													return
																																														bgl_display_string
																																														(BGl_string2290z00zz__r4_output_6_10_3z00,
																																														BgL_portz00_43);
																																												}
																																											else
																																												{	/* Ieee/output.scm 767 */
																																													if (UCS2P(BgL_objz00_42))
																																														{	/* Ieee/output.scm 767 */
																																															return
																																																bgl_write_ucs2
																																																(BgL_objz00_42,
																																																BgL_portz00_43);
																																														}
																																													else
																																														{	/* Ieee/output.scm 767 */
																																															if (CELLP(BgL_objz00_42))
																																																{	/* Ieee/output.scm 767 */
																																																	bgl_display_string
																																																		(BGl_string2294z00zz__r4_output_6_10_3z00,
																																																		BgL_portz00_43);
																																																	bgl_write_obj
																																																		(CELL_REF
																																																		(BgL_objz00_42),
																																																		BgL_portz00_43);
																																																	return
																																																		bgl_display_string
																																																		(BGl_string2290z00zz__r4_output_6_10_3z00,
																																																		BgL_portz00_43);
																																																}
																																															else
																																																{	/* Ieee/output.scm 767 */
																																																	if (EOF_OBJECTP(BgL_objz00_42))
																																																		{	/* Ieee/output.scm 767 */
																																																			return
																																																				bgl_display_string
																																																				(BGl_string2295z00zz__r4_output_6_10_3z00,
																																																				BgL_portz00_43);
																																																		}
																																																	else
																																																		{	/* Ieee/output.scm 767 */
																																																			if ((BgL_objz00_42 == (BOPTIONAL)))
																																																				{	/* Ieee/output.scm 767 */
																																																					return
																																																						bgl_display_string
																																																						(BGl_string2296z00zz__r4_output_6_10_3z00,
																																																						BgL_portz00_43);
																																																				}
																																																			else
																																																				{	/* Ieee/output.scm 767 */
																																																					if ((BgL_objz00_42 == (BREST)))
																																																						{	/* Ieee/output.scm 767 */
																																																							return
																																																								bgl_display_string
																																																								(BGl_string2297z00zz__r4_output_6_10_3z00,
																																																								BgL_portz00_43);
																																																						}
																																																					else
																																																						{	/* Ieee/output.scm 767 */
																																																							if ((BgL_objz00_42 == (BKEY)))
																																																								{	/* Ieee/output.scm 767 */
																																																									return
																																																										bgl_display_string
																																																										(BGl_string2298z00zz__r4_output_6_10_3z00,
																																																										BgL_portz00_43);
																																																								}
																																																							else
																																																								{	/* Ieee/output.scm 767 */
																																																									if (PROCEDUREP(BgL_objz00_42))
																																																										{	/* Ieee/output.scm 767 */
																																																											return
																																																												bgl_write_procedure
																																																												(BgL_objz00_42,
																																																												BgL_portz00_43);
																																																										}
																																																									else
																																																										{	/* Ieee/output.scm 767 */
																																																											if (OUTPUT_PORTP(BgL_objz00_42))
																																																												{	/* Ieee/output.scm 767 */
																																																													if (BGL_OUTPUT_STRING_PORTP(BgL_objz00_42))
																																																														{	/* Ieee/output.scm 767 */
																																																															return
																																																																bgl_display_string
																																																																(BGl_string2299z00zz__r4_output_6_10_3z00,
																																																																BgL_portz00_43);
																																																														}
																																																													else
																																																														{	/* Ieee/output.scm 767 */
																																																															if (BGL_OUTPUT_PROCEDURE_PORTP(BgL_objz00_42))
																																																																{	/* Ieee/output.scm 767 */
																																																																	return
																																																																		bgl_display_string
																																																																		(BGl_string2300z00zz__r4_output_6_10_3z00,
																																																																		BgL_portz00_43);
																																																																}
																																																															else
																																																																{	/* Ieee/output.scm 767 */
																																																																	return
																																																																		bgl_write_output_port
																																																																		(BgL_objz00_42,
																																																																		BgL_portz00_43);
																																																																}
																																																														}
																																																												}
																																																											else
																																																												{	/* Ieee/output.scm 767 */
																																																													if (INPUT_PORTP(BgL_objz00_42))
																																																														{	/* Ieee/output.scm 767 */
																																																															return
																																																																bgl_write_input_port
																																																																(BgL_objz00_42,
																																																																BgL_portz00_43);
																																																														}
																																																													else
																																																														{	/* Ieee/output.scm 767 */
																																																															if (BIGNUMP(BgL_objz00_42))
																																																																{	/* Ieee/output.scm 767 */
																																																																	return
																																																																		bgl_write_bignum
																																																																		(BgL_objz00_42,
																																																																		BgL_portz00_43);
																																																																}
																																																															else
																																																																{	/* Ieee/output.scm 767 */
																																																																	if (BGL_HVECTORP(BgL_objz00_42))
																																																																		{	/* Ieee/output.scm 767 */
																																																																			return
																																																																				BGl_writezf2displayzd2hvectorz20zz__r4_output_6_10_3z00
																																																																				(BgL_objz00_42,
																																																																				BgL_portz00_43,
																																																																				BGl_writezd22zd2envz00zz__r4_output_6_10_3z00);
																																																																		}
																																																																	else
																																																																		{	/* Ieee/output.scm 767 */
																																																																			if (TVECTORP(BgL_objz00_42))
																																																																				{	/* Ieee/output.scm 767 */
																																																																					return
																																																																						BGl_writezf2displayzd2tvectorz20zz__r4_output_6_10_3z00
																																																																						(BgL_objz00_42,
																																																																						BgL_portz00_43,
																																																																						BGl_writezd22zd2envz00zz__r4_output_6_10_3z00);
																																																																				}
																																																																			else
																																																																				{	/* Ieee/output.scm 767 */
																																																																					if (BGL_WEAKPTRP(BgL_objz00_42))
																																																																						{	/* Ieee/output.scm 1052 */
																																																																							obj_t
																																																																								BgL_dataz00_3219;
																																																																							BgL_dataz00_3219
																																																																								=
																																																																								bgl_weakptr_data
																																																																								(BgL_objz00_42);
																																																																							bgl_display_string
																																																																								(BGl_string2301z00zz__r4_output_6_10_3z00,
																																																																								BgL_portz00_43);
																																																																							bgl_write_obj
																																																																								(BgL_dataz00_3219,
																																																																								BgL_portz00_43);
																																																																							return
																																																																								bgl_display_char
																																																																								(
																																																																								((unsigned char) '>'), BgL_portz00_43);
																																																																						}
																																																																					else
																																																																						{	/* Ieee/output.scm 767 */
																																																																							if (FOREIGNP(BgL_objz00_42))
																																																																								{	/* Ieee/output.scm 767 */
																																																																									return
																																																																										bgl_write_foreign
																																																																										(BgL_objz00_42,
																																																																										BgL_portz00_43);
																																																																								}
																																																																							else
																																																																								{	/* Ieee/output.scm 767 */
																																																																									if (PROCESSP(BgL_objz00_42))
																																																																										{	/* Ieee/output.scm 767 */
																																																																											return
																																																																												bgl_write_process
																																																																												(BgL_objz00_42,
																																																																												BgL_portz00_43);
																																																																										}
																																																																									else
																																																																										{	/* Ieee/output.scm 767 */
																																																																											if (SOCKETP(BgL_objz00_42))
																																																																												{	/* Ieee/output.scm 767 */
																																																																													return
																																																																														bgl_write_socket
																																																																														(BgL_objz00_42,
																																																																														BgL_portz00_43);
																																																																												}
																																																																											else
																																																																												{	/* Ieee/output.scm 767 */
																																																																													if (BGL_DATAGRAM_SOCKETP(BgL_objz00_42))
																																																																														{	/* Ieee/output.scm 767 */
																																																																															return
																																																																																bgl_write_datagram_socket
																																																																																(BgL_objz00_42,
																																																																																BgL_portz00_43);
																																																																														}
																																																																													else
																																																																														{	/* Ieee/output.scm 767 */
																																																																															if (BGL_REGEXPP(BgL_objz00_42))
																																																																																{	/* Ieee/output.scm 767 */
																																																																																	return
																																																																																		bgl_write_regexp
																																																																																		(BgL_objz00_42,
																																																																																		BgL_portz00_43);
																																																																																}
																																																																															else
																																																																																{	/* Ieee/output.scm 767 */
																																																																																	if (BGL_MMAPP(BgL_objz00_42))
																																																																																		{	/* Ieee/output.scm 767 */
																																																																																			return
																																																																																				bgl_write_mmap
																																																																																				(BgL_objz00_42,
																																																																																				BgL_portz00_43);
																																																																																		}
																																																																																	else
																																																																																		{	/* Ieee/output.scm 767 */
																																																																																			if (BGL_SEMAPHOREP(BgL_objz00_42))
																																																																																				{	/* Ieee/output.scm 767 */
																																																																																					return
																																																																																						bgl_write_semaphore
																																																																																						(BgL_objz00_42,
																																																																																						BgL_portz00_43);
																																																																																				}
																																																																																			else
																																																																																				{	/* Ieee/output.scm 767 */
																																																																																					if (OPAQUEP(BgL_objz00_42))
																																																																																						{	/* Ieee/output.scm 767 */
																																																																																							BGL_TAIL
																																																																																								return
																																																																																								bgl_write_opaque
																																																																																								(BgL_objz00_42,
																																																																																								BgL_portz00_43);
																																																																																						}
																																																																																					else
																																																																																						{	/* Ieee/output.scm 767 */
																																																																																							if (CUSTOMP(BgL_objz00_42))
																																																																																								{	/* Ieee/output.scm 767 */
																																																																																									BGL_TAIL
																																																																																										return
																																																																																										bgl_write_custom
																																																																																										(BgL_objz00_42,
																																																																																										BgL_portz00_43);
																																																																																								}
																																																																																							else
																																																																																								{	/* Ieee/output.scm 767 */
																																																																																									if (BINARY_PORTP(BgL_objz00_42))
																																																																																										{	/* Ieee/output.scm 767 */
																																																																																											return
																																																																																												bgl_write_binary_port
																																																																																												(BgL_objz00_42,
																																																																																												BgL_portz00_43);
																																																																																										}
																																																																																									else
																																																																																										{	/* Ieee/output.scm 767 */
																																																																																											if (BGL_DYNAMIC_ENVP(BgL_objz00_42))
																																																																																												{	/* Ieee/output.scm 767 */
																																																																																													return
																																																																																														bgl_write_dynamic_env
																																																																																														(BgL_objz00_42,
																																																																																														BgL_portz00_43);
																																																																																												}
																																																																																											else
																																																																																												{	/* Ieee/output.scm 767 */
																																																																																													if (BGL_INT8P(BgL_objz00_42))
																																																																																														{	/* Ieee/output.scm 767 */
																																																																																															bgl_display_string
																																																																																																(BGl_string2303z00zz__r4_output_6_10_3z00,
																																																																																																BgL_portz00_43);
																																																																																															{	/* Ieee/output.scm 767 */
																																																																																																long
																																																																																																	BgL_arg1571z00_1976;
																																																																																																{	/* Ieee/output.scm 767 */
																																																																																																	int8_t
																																																																																																		BgL_xz00_3223;
																																																																																																	BgL_xz00_3223
																																																																																																		=
																																																																																																		BGL_BINT8_TO_INT8
																																																																																																		(BgL_objz00_42);
																																																																																																	{	/* Ieee/output.scm 767 */
																																																																																																		long
																																																																																																			BgL_arg1976z00_3224;
																																																																																																		BgL_arg1976z00_3224
																																																																																																			=
																																																																																																			(long)
																																																																																																			(BgL_xz00_3223);
																																																																																																		BgL_arg1571z00_1976
																																																																																																			=
																																																																																																			(long)
																																																																																																			(BgL_arg1976z00_3224);
																																																																																																}}
																																																																																																BGL_TAIL
																																																																																																	return
																																																																																																	bgl_display_obj
																																																																																																	(BINT
																																																																																																	(BgL_arg1571z00_1976),
																																																																																																	BgL_portz00_43);
																																																																																															}
																																																																																														}
																																																																																													else
																																																																																														{	/* Ieee/output.scm 767 */
																																																																																															if (BGL_UINT8P(BgL_objz00_42))
																																																																																																{	/* Ieee/output.scm 767 */
																																																																																																	bgl_display_string
																																																																																																		(BGl_string2304z00zz__r4_output_6_10_3z00,
																																																																																																		BgL_portz00_43);
																																																																																																	{	/* Ieee/output.scm 767 */
																																																																																																		long
																																																																																																			BgL_arg1573z00_1978;
																																																																																																		{	/* Ieee/output.scm 767 */
																																																																																																			uint8_t
																																																																																																				BgL_xz00_3228;
																																																																																																			BgL_xz00_3228
																																																																																																				=
																																																																																																				BGL_BUINT8_TO_UINT8
																																																																																																				(BgL_objz00_42);
																																																																																																			{	/* Ieee/output.scm 767 */
																																																																																																				long
																																																																																																					BgL_arg1975z00_3229;
																																																																																																				BgL_arg1975z00_3229
																																																																																																					=
																																																																																																					(long)
																																																																																																					(BgL_xz00_3228);
																																																																																																				BgL_arg1573z00_1978
																																																																																																					=
																																																																																																					(long)
																																																																																																					(BgL_arg1975z00_3229);
																																																																																																		}}
																																																																																																		BGL_TAIL
																																																																																																			return
																																																																																																			bgl_display_obj
																																																																																																			(BINT
																																																																																																			(BgL_arg1573z00_1978),
																																																																																																			BgL_portz00_43);
																																																																																																	}
																																																																																																}
																																																																																															else
																																																																																																{	/* Ieee/output.scm 767 */
																																																																																																	if (BGL_INT16P(BgL_objz00_42))
																																																																																																		{	/* Ieee/output.scm 767 */
																																																																																																			bgl_display_string
																																																																																																				(BGl_string2305z00zz__r4_output_6_10_3z00,
																																																																																																				BgL_portz00_43);
																																																																																																			{	/* Ieee/output.scm 767 */
																																																																																																				long
																																																																																																					BgL_arg1575z00_1980;
																																																																																																				{	/* Ieee/output.scm 767 */
																																																																																																					int16_t
																																																																																																						BgL_xz00_3233;
																																																																																																					BgL_xz00_3233
																																																																																																						=
																																																																																																						BGL_BINT16_TO_INT16
																																																																																																						(BgL_objz00_42);
																																																																																																					{	/* Ieee/output.scm 767 */
																																																																																																						long
																																																																																																							BgL_arg1974z00_3234;
																																																																																																						BgL_arg1974z00_3234
																																																																																																							=
																																																																																																							(long)
																																																																																																							(BgL_xz00_3233);
																																																																																																						BgL_arg1575z00_1980
																																																																																																							=
																																																																																																							(long)
																																																																																																							(BgL_arg1974z00_3234);
																																																																																																				}}
																																																																																																				BGL_TAIL
																																																																																																					return
																																																																																																					bgl_display_obj
																																																																																																					(BINT
																																																																																																					(BgL_arg1575z00_1980),
																																																																																																					BgL_portz00_43);
																																																																																																			}
																																																																																																		}
																																																																																																	else
																																																																																																		{	/* Ieee/output.scm 767 */
																																																																																																			if (BGL_UINT16P(BgL_objz00_42))
																																																																																																				{	/* Ieee/output.scm 767 */
																																																																																																					bgl_display_string
																																																																																																						(BGl_string2306z00zz__r4_output_6_10_3z00,
																																																																																																						BgL_portz00_43);
																																																																																																					{	/* Ieee/output.scm 767 */
																																																																																																						long
																																																																																																							BgL_arg1578z00_1982;
																																																																																																						{	/* Ieee/output.scm 767 */
																																																																																																							uint16_t
																																																																																																								BgL_xz00_3238;
																																																																																																							BgL_xz00_3238
																																																																																																								=
																																																																																																								BGL_BUINT16_TO_UINT16
																																																																																																								(BgL_objz00_42);
																																																																																																							{	/* Ieee/output.scm 767 */
																																																																																																								long
																																																																																																									BgL_arg1973z00_3239;
																																																																																																								BgL_arg1973z00_3239
																																																																																																									=
																																																																																																									(long)
																																																																																																									(BgL_xz00_3238);
																																																																																																								BgL_arg1578z00_1982
																																																																																																									=
																																																																																																									(long)
																																																																																																									(BgL_arg1973z00_3239);
																																																																																																						}}
																																																																																																						BGL_TAIL
																																																																																																							return
																																																																																																							bgl_display_obj
																																																																																																							(BINT
																																																																																																							(BgL_arg1578z00_1982),
																																																																																																							BgL_portz00_43);
																																																																																																					}
																																																																																																				}
																																																																																																			else
																																																																																																				{	/* Ieee/output.scm 767 */
																																																																																																					if (BGL_INT32P(BgL_objz00_42))
																																																																																																						{	/* Ieee/output.scm 767 */
																																																																																																							bgl_display_string
																																																																																																								(BGl_string2307z00zz__r4_output_6_10_3z00,
																																																																																																								BgL_portz00_43);
																																																																																																							{	/* Ieee/output.scm 767 */
																																																																																																								long
																																																																																																									BgL_arg1580z00_1984;
																																																																																																								{	/* Ieee/output.scm 767 */
																																																																																																									int32_t
																																																																																																										BgL_nz00_3243;
																																																																																																									BgL_nz00_3243
																																																																																																										=
																																																																																																										BGL_BINT32_TO_INT32
																																																																																																										(BgL_objz00_42);
																																																																																																									BgL_arg1580z00_1984
																																																																																																										=
																																																																																																										(long)
																																																																																																										(BgL_nz00_3243);
																																																																																																								}
																																																																																																								BGL_TAIL
																																																																																																									return
																																																																																																									bgl_display_obj
																																																																																																									(make_belong
																																																																																																									(BgL_arg1580z00_1984),
																																																																																																									BgL_portz00_43);
																																																																																																							}
																																																																																																						}
																																																																																																					else
																																																																																																						{	/* Ieee/output.scm 767 */
																																																																																																							if (BGL_UINT32P(BgL_objz00_42))
																																																																																																								{	/* Ieee/output.scm 767 */
																																																																																																									bgl_display_string
																																																																																																										(BGl_string2308z00zz__r4_output_6_10_3z00,
																																																																																																										BgL_portz00_43);
																																																																																																									{	/* Ieee/output.scm 767 */
																																																																																																										BGL_LONGLONG_T
																																																																																																											BgL_arg1582z00_1986;
																																																																																																										{	/* Ieee/output.scm 767 */
																																																																																																											uint32_t
																																																																																																												BgL_nz00_3246;
																																																																																																											BgL_nz00_3246
																																																																																																												=
																																																																																																												BGL_BUINT32_TO_UINT32
																																																																																																												(BgL_objz00_42);
																																																																																																											BgL_arg1582z00_1986
																																																																																																												=
																																																																																																												(BGL_LONGLONG_T)
																																																																																																												(BgL_nz00_3246);
																																																																																																										}
																																																																																																										BGL_TAIL
																																																																																																											return
																																																																																																											bgl_display_obj
																																																																																																											(make_bllong
																																																																																																											(BgL_arg1582z00_1986),
																																																																																																											BgL_portz00_43);
																																																																																																									}
																																																																																																								}
																																																																																																							else
																																																																																																								{	/* Ieee/output.scm 767 */
																																																																																																									if (BGL_INT64P(BgL_objz00_42))
																																																																																																										{	/* Ieee/output.scm 767 */
																																																																																																											bgl_display_string
																																																																																																												(BGl_string2309z00zz__r4_output_6_10_3z00,
																																																																																																												BgL_portz00_43);
																																																																																																											{	/* Ieee/output.scm 767 */
																																																																																																												BGL_LONGLONG_T
																																																																																																													BgL_arg1584z00_1988;
																																																																																																												{	/* Ieee/output.scm 767 */
																																																																																																													int64_t
																																																																																																														BgL_nz00_3249;
																																																																																																													BgL_nz00_3249
																																																																																																														=
																																																																																																														BGL_BINT64_TO_INT64
																																																																																																														(BgL_objz00_42);
																																																																																																													BgL_arg1584z00_1988
																																																																																																														=
																																																																																																														(BGL_LONGLONG_T)
																																																																																																														(BgL_nz00_3249);
																																																																																																												}
																																																																																																												BGL_TAIL
																																																																																																													return
																																																																																																													bgl_display_obj
																																																																																																													(make_bllong
																																																																																																													(BgL_arg1584z00_1988),
																																																																																																													BgL_portz00_43);
																																																																																																											}
																																																																																																										}
																																																																																																									else
																																																																																																										{	/* Ieee/output.scm 767 */
																																																																																																											if (BGL_UINT64P(BgL_objz00_42))
																																																																																																												{	/* Ieee/output.scm 767 */
																																																																																																													bgl_display_string
																																																																																																														(BGl_string2310z00zz__r4_output_6_10_3z00,
																																																																																																														BgL_portz00_43);
																																																																																																													{	/* Ieee/output.scm 767 */
																																																																																																														uint64_t
																																																																																																															BgL_vz00_1990;
																																																																																																														{	/* Ieee/output.scm 767 */
																																																																																																															uint64_t
																																																																																																																BgL_arg1589z00_1993;
																																																																																																															BgL_arg1589z00_1993
																																																																																																																=
																																																																																																																(uint64_t)
																																																																																																																(10L);
																																																																																																															{	/* Ieee/output.scm 767 */
																																																																																																																uint64_t
																																																																																																																	BgL_za71za7_3252;
																																																																																																																BgL_za71za7_3252
																																																																																																																	=
																																																																																																																	BGL_BINT64_TO_INT64
																																																																																																																	(BgL_objz00_42);
																																																																																																																BgL_vz00_1990
																																																																																																																	=
																																																																																																																	(BgL_za71za7_3252
																																																																																																																	/
																																																																																																																	BgL_arg1589z00_1993);
																																																																																																															}
																																																																																																														}
																																																																																																														if ((BgL_vz00_1990 > (uint64_t) (0)))
																																																																																																															{	/* Ieee/output.scm 767 */
																																																																																																																BGL_LONGLONG_T
																																																																																																																	BgL_arg1587z00_1992;
																																																																																																																BgL_arg1587z00_1992
																																																																																																																	=
																																																																																																																	(BGL_LONGLONG_T)
																																																																																																																	(BgL_vz00_1990);
																																																																																																																bgl_display_obj
																																																																																																																	(make_bllong
																																																																																																																	(BgL_arg1587z00_1992),
																																																																																																																	BgL_portz00_43);
																																																																																																															}
																																																																																																														else
																																																																																																															{	/* Ieee/output.scm 767 */
																																																																																																																BFALSE;
																																																																																																															}
																																																																																																													}
																																																																																																													{	/* Ieee/output.scm 767 */
																																																																																																														long
																																																																																																															BgL_arg1591z00_1994;
																																																																																																														{	/* Ieee/output.scm 767 */
																																																																																																															uint64_t
																																																																																																																BgL_arg1593z00_1995;
																																																																																																															{	/* Ieee/output.scm 767 */
																																																																																																																uint64_t
																																																																																																																	BgL_arg1594z00_1996;
																																																																																																																BgL_arg1594z00_1996
																																																																																																																	=
																																																																																																																	(uint64_t)
																																																																																																																	(10L);
																																																																																																																{	/* Ieee/output.scm 767 */
																																																																																																																	uint64_t
																																																																																																																		BgL_n1z00_3257;
																																																																																																																	BgL_n1z00_3257
																																																																																																																		=
																																																																																																																		BGL_BINT64_TO_INT64
																																																																																																																		(BgL_objz00_42);
																																																																																																																	{	/* Ieee/output.scm 767 */
																																																																																																																		int64_t
																																																																																																																			BgL_tmpz00_5183;
																																																																																																																		BgL_tmpz00_5183
																																																																																																																			=
																																																																																																																			(int64_t)
																																																																																																																			(BgL_arg1594z00_1996);
																																																																																																																		BgL_arg1593z00_1995
																																																																																																																			=
																																																																																																																			(BgL_n1z00_3257
																																																																																																																			%
																																																																																																																			BgL_tmpz00_5183);
																																																																																																															}}}
																																																																																																															{	/* Ieee/output.scm 767 */
																																																																																																																long
																																																																																																																	BgL_arg1970z00_3260;
																																																																																																																BgL_arg1970z00_3260
																																																																																																																	=
																																																																																																																	(long)
																																																																																																																	(BgL_arg1593z00_1995);
																																																																																																																BgL_arg1591z00_1994
																																																																																																																	=
																																																																																																																	(long)
																																																																																																																	(BgL_arg1970z00_3260);
																																																																																																														}}
																																																																																																														BGL_TAIL
																																																																																																															return
																																																																																																															bgl_display_obj
																																																																																																															(BINT
																																																																																																															(BgL_arg1591z00_1994),
																																																																																																															BgL_portz00_43);
																																																																																																													}
																																																																																																												}
																																																																																																											else
																																																																																																												{	/* Ieee/output.scm 767 */
																																																																																																													if (CNSTP(BgL_objz00_42))
																																																																																																														{	/* Ieee/output.scm 767 */
																																																																																																															BGL_TAIL
																																																																																																																return
																																																																																																																bgl_write_cnst
																																																																																																																(BgL_objz00_42,
																																																																																																																BgL_portz00_43);
																																																																																																														}
																																																																																																													else
																																																																																																														{	/* Ieee/output.scm 767 */
																																																																																																															BGL_TAIL
																																																																																																																return
																																																																																																																bgl_write_unknown
																																																																																																																(BgL_objz00_42,
																																																																																																																BgL_portz00_43);
																																																																																																														}
																																																																																																												}
																																																																																																										}
																																																																																																								}
																																																																																																						}
																																																																																																				}
																																																																																																		}
																																																																																																}
																																																																																														}
																																																																																												}
																																																																																										}
																																																																																								}
																																																																																						}
																																																																																				}
																																																																																		}
																																																																																}
																																																																														}
																																																																												}
																																																																										}
																																																																								}
																																																																						}
																																																																				}
																																																																		}
																																																																}
																																																														}
																																																												}
																																																										}
																																																								}
																																																						}
																																																				}
																																																		}
																																																}
																																														}
																																												}
																																										}
																																								}
																																						}
																																				}
																																		}
																																}
																														}
																												}
																										}
																								}
																						}
																				}
																		}
																}
														}
												}
										}
								}
						}
				}
		}

	}



/* &write-2 */
	obj_t BGl_z62writezd22zb0zz__r4_output_6_10_3z00(obj_t BgL_envz00_3887,
		obj_t BgL_objz00_3888, obj_t BgL_portz00_3889)
	{
		{	/* Ieee/output.scm 766 */
			return bgl_write_obj(BgL_objz00_3888, BgL_portz00_3889);
		}

	}



/* display-symbol */
	BGL_EXPORTED_DEF obj_t BGl_displayzd2symbolzd2zz__r4_output_6_10_3z00(obj_t
		BgL_objz00_44, obj_t BgL_portz00_45)
	{
		{	/* Ieee/output.scm 772 */
			{	/* Ieee/output.scm 773 */
				obj_t BgL_arg1598z00_3262;

				BgL_arg1598z00_3262 = SYMBOL_TO_STRING(BgL_objz00_44);
				return bgl_display_string(BgL_arg1598z00_3262, BgL_portz00_45);
			}
		}

	}



/* &display-symbol */
	obj_t BGl_z62displayzd2symbolzb0zz__r4_output_6_10_3z00(obj_t BgL_envz00_3890,
		obj_t BgL_objz00_3891, obj_t BgL_portz00_3892)
	{
		{	/* Ieee/output.scm 772 */
			{	/* Ieee/output.scm 773 */
				obj_t BgL_auxz00_5204;
				obj_t BgL_auxz00_5197;

				if (OUTPUT_PORTP(BgL_portz00_3892))
					{	/* Ieee/output.scm 773 */
						BgL_auxz00_5204 = BgL_portz00_3892;
					}
				else
					{
						obj_t BgL_auxz00_5207;

						BgL_auxz00_5207 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2247z00zz__r4_output_6_10_3z00, BINT(27404L),
							BGl_string2311z00zz__r4_output_6_10_3z00,
							BGl_string2249z00zz__r4_output_6_10_3z00, BgL_portz00_3892);
						FAILURE(BgL_auxz00_5207, BFALSE, BFALSE);
					}
				if (SYMBOLP(BgL_objz00_3891))
					{	/* Ieee/output.scm 773 */
						BgL_auxz00_5197 = BgL_objz00_3891;
					}
				else
					{
						obj_t BgL_auxz00_5200;

						BgL_auxz00_5200 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2247z00zz__r4_output_6_10_3z00, BINT(27404L),
							BGl_string2311z00zz__r4_output_6_10_3z00,
							BGl_string2312z00zz__r4_output_6_10_3z00, BgL_objz00_3891);
						FAILURE(BgL_auxz00_5200, BFALSE, BFALSE);
					}
				return
					BGl_displayzd2symbolzd2zz__r4_output_6_10_3z00(BgL_auxz00_5197,
					BgL_auxz00_5204);
			}
		}

	}



/* write-symbol */
	BGL_EXPORTED_DEF obj_t BGl_writezd2symbolzd2zz__r4_output_6_10_3z00(obj_t
		BgL_objz00_46, obj_t BgL_portz00_47)
	{
		{	/* Ieee/output.scm 778 */
			{	/* Ieee/output.scm 785 */
				obj_t BgL_strz00_2000;

				BgL_strz00_2000 = SYMBOL_TO_STRING(BgL_objz00_46);
				{	/* Ieee/output.scm 785 */
					long BgL_lenz00_2001;

					BgL_lenz00_2001 = STRING_LENGTH(BgL_strz00_2000);
					{	/* Ieee/output.scm 786 */
						long BgL_lenzd21zd2_2002;

						BgL_lenzd21zd2_2002 = (BgL_lenz00_2001 - 1L);
						{	/* Ieee/output.scm 787 */

							{
								long BgL_iz00_2004;
								bool_t BgL_az00_2005;

								BgL_iz00_2004 = 0L;
								BgL_az00_2005 = ((bool_t) 0);
							BgL_zc3z04anonymousza31599ze3z87_2006:
								if ((BgL_iz00_2004 == BgL_lenz00_2001))
									{	/* Ieee/output.scm 790 */
										if (BgL_az00_2005)
											{	/* Ieee/output.scm 792 */
												return
													bgl_display_string(BgL_strz00_2000, BgL_portz00_47);
											}
										else
											{	/* Ieee/output.scm 793 */
												bool_t BgL_test2644z00_5219;

												if (
													(BgL_objz00_46 ==
														BGl_symbol2313z00zz__r4_output_6_10_3z00))
													{	/* Ieee/output.scm 793 */
														BgL_test2644z00_5219 = ((bool_t) 1);
													}
												else
													{	/* Ieee/output.scm 793 */
														BgL_test2644z00_5219 =
															(BgL_objz00_46 ==
															BGl_symbol2315z00zz__r4_output_6_10_3z00);
													}
												if (BgL_test2644z00_5219)
													{	/* Ieee/output.scm 793 */
														return
															bgl_display_string(BgL_strz00_2000,
															BgL_portz00_47);
													}
												else
													{	/* Ieee/output.scm 793 */
														bgl_display_string
															(BGl_string2317z00zz__r4_output_6_10_3z00,
															BgL_portz00_47);
														{	/* Ieee/output.scm 833 */
															obj_t BgL_tmpz00_5225;

															BgL_tmpz00_5225 =
																symbol_for_read(BgL_strz00_2000);
															bgl_display_string(BgL_tmpz00_5225,
																BgL_portz00_47);
														}
														return
															bgl_display_string
															(BGl_string2317z00zz__r4_output_6_10_3z00,
															BgL_portz00_47);
													}
											}
									}
								else
									{	/* Ieee/output.scm 795 */
										unsigned char BgL_cz00_2009;

										BgL_cz00_2009 = STRING_REF(BgL_strz00_2000, BgL_iz00_2004);
										{

											switch (BgL_cz00_2009)
												{
												case ((unsigned char) 10):
												case ((unsigned char) 9):
												case ((unsigned char) 13):
												case ((unsigned char) '`'):
												case ((unsigned char) '\''):
												case ((unsigned char) '"'):
												case ((unsigned char) '#'):
												case ((unsigned char) '\\'):
												case ((unsigned char) ';'):
												case ((unsigned char) '('):
												case ((unsigned char) ')'):
												case ((unsigned char) '['):
												case ((unsigned char) ']'):
												case ((unsigned char) '{'):
												case ((unsigned char) '}'):
												case ((unsigned char) ','):
												case ((unsigned char) '|'):

													bgl_display_string
														(BGl_string2317z00zz__r4_output_6_10_3z00,
														BgL_portz00_47);
													{	/* Ieee/output.scm 833 */
														obj_t BgL_tmpz00_5231;

														BgL_tmpz00_5231 = symbol_for_read(BgL_strz00_2000);
														bgl_display_string(BgL_tmpz00_5231, BgL_portz00_47);
													}
													return
														bgl_display_string
														(BGl_string2317z00zz__r4_output_6_10_3z00,
														BgL_portz00_47);
													break;
												case ((unsigned char) ':'):

													if ((BgL_iz00_2004 == 0L))
														{	/* Ieee/output.scm 803 */
															bool_t BgL_test2647z00_5237;

															if ((BgL_lenzd21zd2_2002 > 2L))
																{	/* Ieee/output.scm 803 */
																	BgL_test2647z00_5237 =
																		(STRING_REF(BgL_strz00_2000,
																			(BgL_iz00_2004 + 1L)) ==
																		((unsigned char) ':'));
																}
															else
																{	/* Ieee/output.scm 803 */
																	BgL_test2647z00_5237 = ((bool_t) 0);
																}
															if (BgL_test2647z00_5237)
																{
																	long BgL_iz00_5243;

																	BgL_iz00_5243 = (BgL_iz00_2004 + 2L);
																	BgL_iz00_2004 = BgL_iz00_5243;
																	goto BgL_zc3z04anonymousza31599ze3z87_2006;
																}
															else
																{	/* Ieee/output.scm 803 */
																	bgl_display_string
																		(BGl_string2317z00zz__r4_output_6_10_3z00,
																		BgL_portz00_47);
																	{	/* Ieee/output.scm 833 */
																		obj_t BgL_tmpz00_5246;

																		BgL_tmpz00_5246 =
																			symbol_for_read(BgL_strz00_2000);
																		bgl_display_string(BgL_tmpz00_5246,
																			BgL_portz00_47);
																	}
																	return
																		bgl_display_string
																		(BGl_string2317z00zz__r4_output_6_10_3z00,
																		BgL_portz00_47);
																}
														}
													else
														{	/* Ieee/output.scm 802 */
															if ((BgL_iz00_2004 == BgL_lenzd21zd2_2002))
																{	/* Ieee/output.scm 807 */
																	bgl_display_string
																		(BGl_string2317z00zz__r4_output_6_10_3z00,
																		BgL_portz00_47);
																	{	/* Ieee/output.scm 833 */
																		obj_t BgL_tmpz00_5253;

																		BgL_tmpz00_5253 =
																			symbol_for_read(BgL_strz00_2000);
																		bgl_display_string(BgL_tmpz00_5253,
																			BgL_portz00_47);
																	}
																	return
																		bgl_display_string
																		(BGl_string2317z00zz__r4_output_6_10_3z00,
																		BgL_portz00_47);
																}
															else
																{
																	long BgL_iz00_5257;

																	BgL_iz00_5257 = (BgL_iz00_2004 + 1L);
																	BgL_iz00_2004 = BgL_iz00_5257;
																	goto BgL_zc3z04anonymousza31599ze3z87_2006;
																}
														}
													break;
												case ((unsigned char) '.'):

													if ((BgL_lenz00_2001 == 1L))
														{	/* Ieee/output.scm 812 */
															bgl_display_string
																(BGl_string2317z00zz__r4_output_6_10_3z00,
																BgL_portz00_47);
															{	/* Ieee/output.scm 833 */
																obj_t BgL_tmpz00_5262;

																BgL_tmpz00_5262 =
																	symbol_for_read(BgL_strz00_2000);
																bgl_display_string(BgL_tmpz00_5262,
																	BgL_portz00_47);
															}
															return
																bgl_display_string
																(BGl_string2317z00zz__r4_output_6_10_3z00,
																BgL_portz00_47);
														}
													else
														{
															long BgL_iz00_5266;

															BgL_iz00_5266 = (BgL_iz00_2004 + 1L);
															BgL_iz00_2004 = BgL_iz00_5266;
															goto BgL_zc3z04anonymousza31599ze3z87_2006;
														}
													break;
												default:
													{	/* Ieee/output.scm 816 */
														bool_t BgL_test2651z00_5268;

														if ((BgL_cz00_2009 <= ((unsigned char) ' ')))
															{	/* Ieee/output.scm 816 */
																BgL_test2651z00_5268 = ((bool_t) 1);
															}
														else
															{	/* Ieee/output.scm 816 */
																BgL_test2651z00_5268 =
																	(BgL_cz00_2009 >= ((unsigned char) ''));
															}
														if (BgL_test2651z00_5268)
															{	/* Ieee/output.scm 816 */
																bgl_display_string
																	(BGl_string2317z00zz__r4_output_6_10_3z00,
																	BgL_portz00_47);
																{	/* Ieee/output.scm 833 */
																	obj_t BgL_tmpz00_5273;

																	BgL_tmpz00_5273 =
																		symbol_for_read(BgL_strz00_2000);
																	bgl_display_string(BgL_tmpz00_5273,
																		BgL_portz00_47);
																}
																return
																	bgl_display_string
																	(BGl_string2317z00zz__r4_output_6_10_3z00,
																	BgL_portz00_47);
															}
														else
															{	/* Ieee/output.scm 818 */
																long BgL_arg1618z00_2028;
																bool_t BgL_arg1619z00_2029;

																BgL_arg1618z00_2028 = (BgL_iz00_2004 + 1L);
																if (BgL_az00_2005)
																	{	/* Ieee/output.scm 819 */
																		BgL_arg1619z00_2029 = BgL_az00_2005;
																	}
																else
																	{	/* Ieee/output.scm 820 */
																		bool_t BgL__ortest_1053z00_2031;

																		if (isdigit(BgL_cz00_2009))
																			{	/* Ieee/output.scm 820 */
																				BgL__ortest_1053z00_2031 = ((bool_t) 0);
																			}
																		else
																			{	/* Ieee/output.scm 820 */
																				if (
																					(BgL_cz00_2009 ==
																						((unsigned char) 'e')))
																					{	/* Ieee/output.scm 821 */
																						BgL__ortest_1053z00_2031 =
																							((bool_t) 0);
																					}
																				else
																					{	/* Ieee/output.scm 821 */
																						if (
																							(BgL_cz00_2009 ==
																								((unsigned char) 'E')))
																							{	/* Ieee/output.scm 822 */
																								BgL__ortest_1053z00_2031 =
																									((bool_t) 0);
																							}
																						else
																							{	/* Ieee/output.scm 822 */
																								if (
																									(BgL_cz00_2009 ==
																										((unsigned char) '-')))
																									{	/* Ieee/output.scm 823 */
																										BgL__ortest_1053z00_2031 =
																											((bool_t) 0);
																									}
																								else
																									{	/* Ieee/output.scm 823 */
																										if (
																											(BgL_cz00_2009 ==
																												((unsigned char) '+')))
																											{	/* Ieee/output.scm 824 */
																												BgL__ortest_1053z00_2031
																													= ((bool_t) 0);
																											}
																										else
																											{	/* Ieee/output.scm 824 */
																												BgL__ortest_1053z00_2031
																													= ((bool_t) 1);
																											}
																									}
																							}
																					}
																			}
																		if (BgL__ortest_1053z00_2031)
																			{	/* Ieee/output.scm 820 */
																				BgL_arg1619z00_2029 =
																					BgL__ortest_1053z00_2031;
																			}
																		else
																			{	/* Ieee/output.scm 820 */
																				if ((BgL_iz00_2004 == 0L))
																					{	/* Ieee/output.scm 826 */
																						bool_t BgL__ortest_1059z00_2033;

																						BgL__ortest_1059z00_2033 =
																							(BgL_cz00_2009 ==
																							((unsigned char) 'e'));
																						if (BgL__ortest_1059z00_2033)
																							{	/* Ieee/output.scm 826 */
																								BgL_arg1619z00_2029 =
																									BgL__ortest_1059z00_2033;
																							}
																						else
																							{	/* Ieee/output.scm 826 */
																								BgL_arg1619z00_2029 =
																									(BgL_cz00_2009 ==
																									((unsigned char) 'E'));
																					}}
																				else
																					{	/* Ieee/output.scm 825 */
																						BgL_arg1619z00_2029 = ((bool_t) 0);
																					}
																			}
																	}
																{
																	bool_t BgL_az00_5296;
																	long BgL_iz00_5295;

																	BgL_iz00_5295 = BgL_arg1618z00_2028;
																	BgL_az00_5296 = BgL_arg1619z00_2029;
																	BgL_az00_2005 = BgL_az00_5296;
																	BgL_iz00_2004 = BgL_iz00_5295;
																	goto BgL_zc3z04anonymousza31599ze3z87_2006;
																}
															}
													}
												}
										}
									}
							}
						}
					}
				}
			}
		}

	}



/* &write-symbol */
	obj_t BGl_z62writezd2symbolzb0zz__r4_output_6_10_3z00(obj_t BgL_envz00_3893,
		obj_t BgL_objz00_3894, obj_t BgL_portz00_3895)
	{
		{	/* Ieee/output.scm 778 */
			{	/* Ieee/output.scm 785 */
				obj_t BgL_auxz00_5305;
				obj_t BgL_auxz00_5298;

				if (OUTPUT_PORTP(BgL_portz00_3895))
					{	/* Ieee/output.scm 785 */
						BgL_auxz00_5305 = BgL_portz00_3895;
					}
				else
					{
						obj_t BgL_auxz00_5308;

						BgL_auxz00_5308 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2247z00zz__r4_output_6_10_3z00, BINT(27836L),
							BGl_string2318z00zz__r4_output_6_10_3z00,
							BGl_string2249z00zz__r4_output_6_10_3z00, BgL_portz00_3895);
						FAILURE(BgL_auxz00_5308, BFALSE, BFALSE);
					}
				if (SYMBOLP(BgL_objz00_3894))
					{	/* Ieee/output.scm 785 */
						BgL_auxz00_5298 = BgL_objz00_3894;
					}
				else
					{
						obj_t BgL_auxz00_5301;

						BgL_auxz00_5301 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2247z00zz__r4_output_6_10_3z00, BINT(27836L),
							BGl_string2318z00zz__r4_output_6_10_3z00,
							BGl_string2312z00zz__r4_output_6_10_3z00, BgL_objz00_3894);
						FAILURE(BgL_auxz00_5301, BFALSE, BFALSE);
					}
				return
					BGl_writezd2symbolzd2zz__r4_output_6_10_3z00(BgL_auxz00_5298,
					BgL_auxz00_5305);
			}
		}

	}



/* display-string */
	BGL_EXPORTED_DEF obj_t BGl_displayzd2stringzd2zz__r4_output_6_10_3z00(obj_t
		BgL_objz00_48, obj_t BgL_portz00_49)
	{
		{	/* Ieee/output.scm 832 */
			return bgl_display_string(BgL_objz00_48, BgL_portz00_49);
		}

	}



/* &display-string */
	obj_t BGl_z62displayzd2stringzb0zz__r4_output_6_10_3z00(obj_t BgL_envz00_3896,
		obj_t BgL_objz00_3897, obj_t BgL_portz00_3898)
	{
		{	/* Ieee/output.scm 832 */
			{	/* Ieee/output.scm 833 */
				obj_t BgL_auxz00_5321;
				obj_t BgL_auxz00_5314;

				if (OUTPUT_PORTP(BgL_portz00_3898))
					{	/* Ieee/output.scm 833 */
						BgL_auxz00_5321 = BgL_portz00_3898;
					}
				else
					{
						obj_t BgL_auxz00_5324;

						BgL_auxz00_5324 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2247z00zz__r4_output_6_10_3z00, BINT(29178L),
							BGl_string2319z00zz__r4_output_6_10_3z00,
							BGl_string2249z00zz__r4_output_6_10_3z00, BgL_portz00_3898);
						FAILURE(BgL_auxz00_5324, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_objz00_3897))
					{	/* Ieee/output.scm 833 */
						BgL_auxz00_5314 = BgL_objz00_3897;
					}
				else
					{
						obj_t BgL_auxz00_5317;

						BgL_auxz00_5317 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2247z00zz__r4_output_6_10_3z00, BINT(29178L),
							BGl_string2319z00zz__r4_output_6_10_3z00,
							BGl_string2278z00zz__r4_output_6_10_3z00, BgL_objz00_3897);
						FAILURE(BgL_auxz00_5317, BFALSE, BFALSE);
					}
				return
					BGl_displayzd2stringzd2zz__r4_output_6_10_3z00(BgL_auxz00_5314,
					BgL_auxz00_5321);
			}
		}

	}



/* display-substring */
	BGL_EXPORTED_DEF obj_t BGl_displayzd2substringzd2zz__r4_output_6_10_3z00(obj_t
		BgL_objz00_50, long BgL_startz00_51, long BgL_endz00_52,
		obj_t BgL_portz00_53)
	{
		{	/* Ieee/output.scm 838 */
			{	/* Ieee/output.scm 839 */
				bool_t BgL_test2666z00_5329;

				if ((BgL_endz00_52 >= BgL_startz00_51))
					{	/* Ieee/output.scm 840 */
						bool_t BgL_test2668z00_5332;

						{	/* Ieee/output.scm 840 */
							long BgL_tmpz00_5333;

							BgL_tmpz00_5333 = (STRING_LENGTH(BgL_objz00_50) + 1L);
							BgL_test2668z00_5332 =
								BOUND_CHECK(BgL_endz00_52, BgL_tmpz00_5333);
						}
						if (BgL_test2668z00_5332)
							{	/* Ieee/output.scm 840 */
								BgL_test2666z00_5329 = (BgL_startz00_51 >= 0L);
							}
						else
							{	/* Ieee/output.scm 840 */
								BgL_test2666z00_5329 = ((bool_t) 0);
							}
					}
				else
					{	/* Ieee/output.scm 839 */
						BgL_test2666z00_5329 = ((bool_t) 0);
					}
				if (BgL_test2666z00_5329)
					{	/* Ieee/output.scm 839 */
						return
							bgl_display_substring(BgL_objz00_50, BgL_startz00_51,
							BgL_endz00_52, BgL_portz00_53);
					}
				else
					{	/* Ieee/output.scm 844 */
						obj_t BgL_arg1630z00_4002;

						{	/* Ieee/output.scm 844 */
							obj_t BgL_list1631z00_4003;

							{	/* Ieee/output.scm 844 */
								obj_t BgL_arg1634z00_4004;

								BgL_arg1634z00_4004 =
									MAKE_YOUNG_PAIR(BINT(BgL_endz00_52), BNIL);
								BgL_list1631z00_4003 =
									MAKE_YOUNG_PAIR(BINT(BgL_startz00_51), BgL_arg1634z00_4004);
							}
							BgL_arg1630z00_4002 =
								BGl_formatz00zz__r4_output_6_10_3z00
								(BGl_string2320z00zz__r4_output_6_10_3z00,
								BgL_list1631z00_4003);
						}
						return
							BGl_errorz00zz__errorz00(BGl_symbol2321z00zz__r4_output_6_10_3z00,
							BgL_arg1630z00_4002, BgL_objz00_50);
					}
			}
		}

	}



/* &display-substring */
	obj_t BGl_z62displayzd2substringzb0zz__r4_output_6_10_3z00(obj_t
		BgL_envz00_3899, obj_t BgL_objz00_3900, obj_t BgL_startz00_3901,
		obj_t BgL_endz00_3902, obj_t BgL_portz00_3903)
	{
		{	/* Ieee/output.scm 838 */
			{	/* Ieee/output.scm 839 */
				obj_t BgL_auxz00_5370;
				long BgL_auxz00_5361;
				long BgL_auxz00_5352;
				obj_t BgL_auxz00_5345;

				if (OUTPUT_PORTP(BgL_portz00_3903))
					{	/* Ieee/output.scm 839 */
						BgL_auxz00_5370 = BgL_portz00_3903;
					}
				else
					{
						obj_t BgL_auxz00_5373;

						BgL_auxz00_5373 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2247z00zz__r4_output_6_10_3z00, BINT(29486L),
							BGl_string2323z00zz__r4_output_6_10_3z00,
							BGl_string2249z00zz__r4_output_6_10_3z00, BgL_portz00_3903);
						FAILURE(BgL_auxz00_5373, BFALSE, BFALSE);
					}
				{	/* Ieee/output.scm 839 */
					obj_t BgL_tmpz00_5362;

					if (INTEGERP(BgL_endz00_3902))
						{	/* Ieee/output.scm 839 */
							BgL_tmpz00_5362 = BgL_endz00_3902;
						}
					else
						{
							obj_t BgL_auxz00_5365;

							BgL_auxz00_5365 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2247z00zz__r4_output_6_10_3z00, BINT(29486L),
								BGl_string2323z00zz__r4_output_6_10_3z00,
								BGl_string2256z00zz__r4_output_6_10_3z00, BgL_endz00_3902);
							FAILURE(BgL_auxz00_5365, BFALSE, BFALSE);
						}
					BgL_auxz00_5361 = (long) CINT(BgL_tmpz00_5362);
				}
				{	/* Ieee/output.scm 839 */
					obj_t BgL_tmpz00_5353;

					if (INTEGERP(BgL_startz00_3901))
						{	/* Ieee/output.scm 839 */
							BgL_tmpz00_5353 = BgL_startz00_3901;
						}
					else
						{
							obj_t BgL_auxz00_5356;

							BgL_auxz00_5356 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2247z00zz__r4_output_6_10_3z00, BINT(29486L),
								BGl_string2323z00zz__r4_output_6_10_3z00,
								BGl_string2256z00zz__r4_output_6_10_3z00, BgL_startz00_3901);
							FAILURE(BgL_auxz00_5356, BFALSE, BFALSE);
						}
					BgL_auxz00_5352 = (long) CINT(BgL_tmpz00_5353);
				}
				if (STRINGP(BgL_objz00_3900))
					{	/* Ieee/output.scm 839 */
						BgL_auxz00_5345 = BgL_objz00_3900;
					}
				else
					{
						obj_t BgL_auxz00_5348;

						BgL_auxz00_5348 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2247z00zz__r4_output_6_10_3z00, BINT(29486L),
							BGl_string2323z00zz__r4_output_6_10_3z00,
							BGl_string2278z00zz__r4_output_6_10_3z00, BgL_objz00_3900);
						FAILURE(BgL_auxz00_5348, BFALSE, BFALSE);
					}
				return
					BGl_displayzd2substringzd2zz__r4_output_6_10_3z00(BgL_auxz00_5345,
					BgL_auxz00_5352, BgL_auxz00_5361, BgL_auxz00_5370);
			}
		}

	}



/* write-string */
	BGL_EXPORTED_DEF obj_t BGl_writezd2stringzd2zz__r4_output_6_10_3z00(obj_t
		BgL_objz00_54, obj_t BgL_portz00_55)
	{
		{	/* Ieee/output.scm 850 */
			if (BGl_bigloozd2strictzd2r5rszd2stringszd2zz__paramz00())
				{	/* Ieee/output.scm 854 */
					obj_t BgL_strz00_3372;

					BgL_strz00_3372 = string_for_read(BgL_objz00_54);
					{	/* Ieee/output.scm 855 */
						obj_t BgL_escz00_3373;

						{	/* Ieee/output.scm 856 */
							obj_t BgL_tmpz00_3376;

							{	/* Ieee/output.scm 856 */
								int BgL_tmpz00_5381;

								BgL_tmpz00_5381 = (int) (1L);
								BgL_tmpz00_3376 = BGL_MVALUES_VAL(BgL_tmpz00_5381);
							}
							{	/* Ieee/output.scm 856 */
								int BgL_tmpz00_5384;

								BgL_tmpz00_5384 = (int) (1L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_5384, BUNSPEC);
							}
							BgL_escz00_3373 = BgL_tmpz00_3376;
						}
						return
							bgl_write_string(BgL_strz00_3372,
							CBOOL(BgL_escz00_3373), BgL_portz00_55);
					}
				}
			else
				{	/* Ieee/output.scm 851 */
					return
						bgl_write_string(string_for_read(BgL_objz00_54), ((bool_t) 0),
						BgL_portz00_55);
				}
		}

	}



/* &write-string */
	obj_t BGl_z62writezd2stringzb0zz__r4_output_6_10_3z00(obj_t BgL_envz00_3904,
		obj_t BgL_objz00_3905, obj_t BgL_portz00_3906)
	{
		{	/* Ieee/output.scm 850 */
			{	/* Ieee/output.scm 851 */
				obj_t BgL_auxz00_5398;
				obj_t BgL_auxz00_5391;

				if (OUTPUT_PORTP(BgL_portz00_3906))
					{	/* Ieee/output.scm 851 */
						BgL_auxz00_5398 = BgL_portz00_3906;
					}
				else
					{
						obj_t BgL_auxz00_5401;

						BgL_auxz00_5401 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2247z00zz__r4_output_6_10_3z00, BINT(30004L),
							BGl_string2324z00zz__r4_output_6_10_3z00,
							BGl_string2249z00zz__r4_output_6_10_3z00, BgL_portz00_3906);
						FAILURE(BgL_auxz00_5401, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_objz00_3905))
					{	/* Ieee/output.scm 851 */
						BgL_auxz00_5391 = BgL_objz00_3905;
					}
				else
					{
						obj_t BgL_auxz00_5394;

						BgL_auxz00_5394 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2247z00zz__r4_output_6_10_3z00, BINT(30004L),
							BGl_string2324z00zz__r4_output_6_10_3z00,
							BGl_string2278z00zz__r4_output_6_10_3z00, BgL_objz00_3905);
						FAILURE(BgL_auxz00_5394, BFALSE, BFALSE);
					}
				return
					BGl_writezd2stringzd2zz__r4_output_6_10_3z00(BgL_auxz00_5391,
					BgL_auxz00_5398);
			}
		}

	}



/* display-fixnum */
	BGL_EXPORTED_DEF obj_t BGl_displayzd2fixnumzd2zz__r4_output_6_10_3z00(obj_t
		BgL_objz00_58, obj_t BgL_portz00_59)
	{
		{	/* Ieee/output.scm 871 */
			BGL_TAIL return bgl_display_fixnum(BgL_objz00_58, BgL_portz00_59);
		}

	}



/* &display-fixnum */
	obj_t BGl_z62displayzd2fixnumzb0zz__r4_output_6_10_3z00(obj_t BgL_envz00_3907,
		obj_t BgL_objz00_3908, obj_t BgL_portz00_3909)
	{
		{	/* Ieee/output.scm 871 */
			{	/* Ieee/output.scm 872 */
				obj_t BgL_auxz00_5414;
				obj_t BgL_auxz00_5407;

				if (OUTPUT_PORTP(BgL_portz00_3909))
					{	/* Ieee/output.scm 872 */
						BgL_auxz00_5414 = BgL_portz00_3909;
					}
				else
					{
						obj_t BgL_auxz00_5417;

						BgL_auxz00_5417 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2247z00zz__r4_output_6_10_3z00, BINT(30890L),
							BGl_string2325z00zz__r4_output_6_10_3z00,
							BGl_string2249z00zz__r4_output_6_10_3z00, BgL_portz00_3909);
						FAILURE(BgL_auxz00_5417, BFALSE, BFALSE);
					}
				if (INTEGERP(BgL_objz00_3908))
					{	/* Ieee/output.scm 872 */
						BgL_auxz00_5407 = BgL_objz00_3908;
					}
				else
					{
						obj_t BgL_auxz00_5410;

						BgL_auxz00_5410 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2247z00zz__r4_output_6_10_3z00, BINT(30890L),
							BGl_string2325z00zz__r4_output_6_10_3z00,
							BGl_string2256z00zz__r4_output_6_10_3z00, BgL_objz00_3908);
						FAILURE(BgL_auxz00_5410, BFALSE, BFALSE);
					}
				return
					BGl_displayzd2fixnumzd2zz__r4_output_6_10_3z00(BgL_auxz00_5407,
					BgL_auxz00_5414);
			}
		}

	}



/* display-elong */
	BGL_EXPORTED_DEF obj_t BGl_displayzd2elongzd2zz__r4_output_6_10_3z00(long
		BgL_objz00_60, obj_t BgL_portz00_61)
	{
		{	/* Ieee/output.scm 877 */
			BGL_TAIL return bgl_display_elong(BgL_objz00_60, BgL_portz00_61);
		}

	}



/* &display-elong */
	obj_t BGl_z62displayzd2elongzb0zz__r4_output_6_10_3z00(obj_t BgL_envz00_3910,
		obj_t BgL_objz00_3911, obj_t BgL_portz00_3912)
	{
		{	/* Ieee/output.scm 877 */
			{	/* Ieee/output.scm 878 */
				obj_t BgL_auxz00_5432;
				long BgL_auxz00_5423;

				if (OUTPUT_PORTP(BgL_portz00_3912))
					{	/* Ieee/output.scm 878 */
						BgL_auxz00_5432 = BgL_portz00_3912;
					}
				else
					{
						obj_t BgL_auxz00_5435;

						BgL_auxz00_5435 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2247z00zz__r4_output_6_10_3z00, BINT(31184L),
							BGl_string2326z00zz__r4_output_6_10_3z00,
							BGl_string2249z00zz__r4_output_6_10_3z00, BgL_portz00_3912);
						FAILURE(BgL_auxz00_5435, BFALSE, BFALSE);
					}
				{	/* Ieee/output.scm 878 */
					obj_t BgL_tmpz00_5424;

					if (ELONGP(BgL_objz00_3911))
						{	/* Ieee/output.scm 878 */
							BgL_tmpz00_5424 = BgL_objz00_3911;
						}
					else
						{
							obj_t BgL_auxz00_5427;

							BgL_auxz00_5427 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2247z00zz__r4_output_6_10_3z00, BINT(31184L),
								BGl_string2326z00zz__r4_output_6_10_3z00,
								BGl_string2327z00zz__r4_output_6_10_3z00, BgL_objz00_3911);
							FAILURE(BgL_auxz00_5427, BFALSE, BFALSE);
						}
					BgL_auxz00_5423 = BELONG_TO_LONG(BgL_tmpz00_5424);
				}
				return
					BGl_displayzd2elongzd2zz__r4_output_6_10_3z00(BgL_auxz00_5423,
					BgL_auxz00_5432);
			}
		}

	}



/* display-flonum */
	BGL_EXPORTED_DEF obj_t BGl_displayzd2flonumzd2zz__r4_output_6_10_3z00(obj_t
		BgL_objz00_62, obj_t BgL_portz00_63)
	{
		{	/* Ieee/output.scm 883 */
			{	/* Ieee/output.scm 884 */
				obj_t BgL_arg1641z00_3382;

				BgL_arg1641z00_3382 = bgl_real_to_string(REAL_TO_DOUBLE(BgL_objz00_62));
				return bgl_display_string(BgL_arg1641z00_3382, BgL_portz00_63);
			}
		}

	}



/* &display-flonum */
	obj_t BGl_z62displayzd2flonumzb0zz__r4_output_6_10_3z00(obj_t BgL_envz00_3913,
		obj_t BgL_objz00_3914, obj_t BgL_portz00_3915)
	{
		{	/* Ieee/output.scm 883 */
			{	/* Ieee/output.scm 884 */
				obj_t BgL_auxz00_5450;
				obj_t BgL_auxz00_5443;

				if (OUTPUT_PORTP(BgL_portz00_3915))
					{	/* Ieee/output.scm 884 */
						BgL_auxz00_5450 = BgL_portz00_3915;
					}
				else
					{
						obj_t BgL_auxz00_5453;

						BgL_auxz00_5453 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2247z00zz__r4_output_6_10_3z00, BINT(31487L),
							BGl_string2328z00zz__r4_output_6_10_3z00,
							BGl_string2249z00zz__r4_output_6_10_3z00, BgL_portz00_3915);
						FAILURE(BgL_auxz00_5453, BFALSE, BFALSE);
					}
				if (REALP(BgL_objz00_3914))
					{	/* Ieee/output.scm 884 */
						BgL_auxz00_5443 = BgL_objz00_3914;
					}
				else
					{
						obj_t BgL_auxz00_5446;

						BgL_auxz00_5446 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2247z00zz__r4_output_6_10_3z00, BINT(31487L),
							BGl_string2328z00zz__r4_output_6_10_3z00,
							BGl_string2329z00zz__r4_output_6_10_3z00, BgL_objz00_3914);
						FAILURE(BgL_auxz00_5446, BFALSE, BFALSE);
					}
				return
					BGl_displayzd2flonumzd2zz__r4_output_6_10_3z00(BgL_auxz00_5443,
					BgL_auxz00_5450);
			}
		}

	}



/* display-ucs2string */
	BGL_EXPORTED_DEF obj_t
		BGl_displayzd2ucs2stringzd2zz__r4_output_6_10_3z00(obj_t BgL_objz00_64,
		obj_t BgL_portz00_65)
	{
		{	/* Ieee/output.scm 889 */
			BGL_TAIL return bgl_display_ucs2string(BgL_objz00_64, BgL_portz00_65);
		}

	}



/* &display-ucs2string */
	obj_t BGl_z62displayzd2ucs2stringzb0zz__r4_output_6_10_3z00(obj_t
		BgL_envz00_3916, obj_t BgL_objz00_3917, obj_t BgL_portz00_3918)
	{
		{	/* Ieee/output.scm 889 */
			{	/* Ieee/output.scm 890 */
				obj_t BgL_auxz00_5466;
				obj_t BgL_auxz00_5459;

				if (OUTPUT_PORTP(BgL_portz00_3918))
					{	/* Ieee/output.scm 890 */
						BgL_auxz00_5466 = BgL_portz00_3918;
					}
				else
					{
						obj_t BgL_auxz00_5469;

						BgL_auxz00_5469 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2247z00zz__r4_output_6_10_3z00, BINT(31777L),
							BGl_string2330z00zz__r4_output_6_10_3z00,
							BGl_string2249z00zz__r4_output_6_10_3z00, BgL_portz00_3918);
						FAILURE(BgL_auxz00_5469, BFALSE, BFALSE);
					}
				if (UCS2_STRINGP(BgL_objz00_3917))
					{	/* Ieee/output.scm 890 */
						BgL_auxz00_5459 = BgL_objz00_3917;
					}
				else
					{
						obj_t BgL_auxz00_5462;

						BgL_auxz00_5462 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2247z00zz__r4_output_6_10_3z00, BINT(31777L),
							BGl_string2330z00zz__r4_output_6_10_3z00,
							BGl_string2331z00zz__r4_output_6_10_3z00, BgL_objz00_3917);
						FAILURE(BgL_auxz00_5462, BFALSE, BFALSE);
					}
				return
					BGl_displayzd2ucs2stringzd2zz__r4_output_6_10_3z00(BgL_auxz00_5459,
					BgL_auxz00_5466);
			}
		}

	}



/* write-ucs2string */
	BGL_EXPORTED_DEF obj_t BGl_writezd2ucs2stringzd2zz__r4_output_6_10_3z00(obj_t
		BgL_objz00_66, obj_t BgL_portz00_67)
	{
		{	/* Ieee/output.scm 895 */
			return
				bgl_write_utf8string(string_for_read(ucs2_string_to_utf8_string
					(BgL_objz00_66)), BgL_portz00_67);
		}

	}



/* &write-ucs2string */
	obj_t BGl_z62writezd2ucs2stringzb0zz__r4_output_6_10_3z00(obj_t
		BgL_envz00_3919, obj_t BgL_objz00_3920, obj_t BgL_portz00_3921)
	{
		{	/* Ieee/output.scm 895 */
			{	/* Ieee/output.scm 896 */
				obj_t BgL_auxz00_5484;
				obj_t BgL_auxz00_5477;

				if (OUTPUT_PORTP(BgL_portz00_3921))
					{	/* Ieee/output.scm 896 */
						BgL_auxz00_5484 = BgL_portz00_3921;
					}
				else
					{
						obj_t BgL_auxz00_5487;

						BgL_auxz00_5487 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2247z00zz__r4_output_6_10_3z00, BINT(32107L),
							BGl_string2332z00zz__r4_output_6_10_3z00,
							BGl_string2249z00zz__r4_output_6_10_3z00, BgL_portz00_3921);
						FAILURE(BgL_auxz00_5487, BFALSE, BFALSE);
					}
				if (UCS2_STRINGP(BgL_objz00_3920))
					{	/* Ieee/output.scm 896 */
						BgL_auxz00_5477 = BgL_objz00_3920;
					}
				else
					{
						obj_t BgL_auxz00_5480;

						BgL_auxz00_5480 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2247z00zz__r4_output_6_10_3z00, BINT(32107L),
							BGl_string2332z00zz__r4_output_6_10_3z00,
							BGl_string2331z00zz__r4_output_6_10_3z00, BgL_objz00_3920);
						FAILURE(BgL_auxz00_5480, BFALSE, BFALSE);
					}
				return
					BGl_writezd2ucs2stringzd2zz__r4_output_6_10_3z00(BgL_auxz00_5477,
					BgL_auxz00_5484);
			}
		}

	}



/* write/display-structure */
	obj_t BGl_writezf2displayzd2structurez20zz__r4_output_6_10_3z00(obj_t
		BgL_objz00_81, obj_t BgL_portz00_82, obj_t BgL_dispz00_83)
	{
		{	/* Ieee/output.scm 950 */
			bgl_display_char(((unsigned char) '#'), BgL_portz00_82);
			bgl_display_char(((unsigned char) '{'), BgL_portz00_82);
			{	/* Ieee/output.scm 953 */
				obj_t BgL_arg1652z00_2074;

				BgL_arg1652z00_2074 = STRUCT_KEY(BgL_objz00_81);
				BGL_PROCEDURE_CALL2(BgL_dispz00_83, BgL_arg1652z00_2074,
					BgL_portz00_82);
			}
			if ((0L == (long) (STRUCT_LENGTH(BgL_objz00_81))))
				{	/* Ieee/output.scm 954 */
					return bgl_display_char(((unsigned char) '}'), BgL_portz00_82);
				}
			else
				{	/* Ieee/output.scm 956 */
					long BgL_lenz00_2077;

					BgL_lenz00_2077 = ((long) (STRUCT_LENGTH(BgL_objz00_81)) - 1L);
					bgl_display_char(((unsigned char) ' '), BgL_portz00_82);
					{
						long BgL_iz00_2079;

						BgL_iz00_2079 = 0L;
					BgL_zc3z04anonymousza31655ze3z87_2080:
						if ((BgL_iz00_2079 == BgL_lenz00_2077))
							{	/* Ieee/output.scm 960 */
								{	/* Ieee/output.scm 961 */
									obj_t BgL_arg1657z00_2082;

									BgL_arg1657z00_2082 =
										STRUCT_REF(BgL_objz00_81, (int) (BgL_iz00_2079));
									BGL_PROCEDURE_CALL2(BgL_dispz00_83, BgL_arg1657z00_2082,
										BgL_portz00_82);
								}
								return bgl_display_char(((unsigned char) '}'), BgL_portz00_82);
							}
						else
							{	/* Ieee/output.scm 960 */
								{	/* Ieee/output.scm 964 */
									obj_t BgL_arg1658z00_2083;

									BgL_arg1658z00_2083 =
										STRUCT_REF(BgL_objz00_81, (int) (BgL_iz00_2079));
									BGL_PROCEDURE_CALL2(BgL_dispz00_83, BgL_arg1658z00_2083,
										BgL_portz00_82);
								}
								bgl_display_char(((unsigned char) ' '), BgL_portz00_82);
								{
									long BgL_iz00_5527;

									BgL_iz00_5527 = (1L + BgL_iz00_2079);
									BgL_iz00_2079 = BgL_iz00_5527;
									goto BgL_zc3z04anonymousza31655ze3z87_2080;
								}
							}
					}
				}
		}

	}



/* write/display-vector */
	obj_t BGl_writezf2displayzd2vectorz20zz__r4_output_6_10_3z00(obj_t
		BgL_objz00_84, obj_t BgL_portz00_85, obj_t BgL_dispz00_86)
	{
		{	/* Ieee/output.scm 971 */
			bgl_display_char(((unsigned char) '#'), BgL_portz00_85);
			{	/* Ieee/output.scm 973 */
				int BgL_tagz00_2088;

				BgL_tagz00_2088 = VECTOR_TAG(BgL_objz00_84);
				if (((long) (BgL_tagz00_2088) > 0L))
					{	/* Ieee/output.scm 974 */
						if (((long) (BgL_tagz00_2088) >= 100L))
							{	/* Ieee/output.scm 976 */
								BGL_PROCEDURE_CALL2(BgL_dispz00_86,
									BINT(BgL_tagz00_2088), BgL_portz00_85);
							}
						else
							{	/* Ieee/output.scm 976 */
								bgl_display_char(((unsigned char) '0'), BgL_portz00_85);
								if (((long) (BgL_tagz00_2088) >= 10L))
									{	/* Ieee/output.scm 980 */
										BGL_PROCEDURE_CALL2(BgL_dispz00_86,
											BINT(BgL_tagz00_2088), BgL_portz00_85);
									}
								else
									{	/* Ieee/output.scm 980 */
										bgl_display_char(((unsigned char) '0'), BgL_portz00_85);
										BGL_PROCEDURE_CALL2(BgL_dispz00_86,
											BINT(BgL_tagz00_2088), BgL_portz00_85);
					}}}
				else
					{	/* Ieee/output.scm 974 */
						BFALSE;
					}
			}
			bgl_display_char(((unsigned char) '('), BgL_portz00_85);
			if ((0L == VECTOR_LENGTH(BgL_objz00_84)))
				{	/* Ieee/output.scm 986 */
					return bgl_display_char(((unsigned char) ')'), BgL_portz00_85);
				}
			else
				{	/* Ieee/output.scm 988 */
					long BgL_lenz00_2094;

					BgL_lenz00_2094 = (VECTOR_LENGTH(BgL_objz00_84) - 1L);
					{
						long BgL_iz00_2096;

						BgL_iz00_2096 = 0L;
					BgL_zc3z04anonymousza31670ze3z87_2097:
						if ((BgL_iz00_2096 == BgL_lenz00_2094))
							{	/* Ieee/output.scm 991 */
								{	/* Ieee/output.scm 992 */
									obj_t BgL_arg1675z00_2099;

									BgL_arg1675z00_2099 =
										VECTOR_REF(BgL_objz00_84, BgL_iz00_2096);
									BGL_PROCEDURE_CALL2(BgL_dispz00_86, BgL_arg1675z00_2099,
										BgL_portz00_85);
								}
								return bgl_display_char(((unsigned char) ')'), BgL_portz00_85);
							}
						else
							{	/* Ieee/output.scm 991 */
								{	/* Ieee/output.scm 995 */
									obj_t BgL_arg1676z00_2100;

									BgL_arg1676z00_2100 =
										VECTOR_REF(BgL_objz00_84, BgL_iz00_2096);
									BGL_PROCEDURE_CALL2(BgL_dispz00_86, BgL_arg1676z00_2100,
										BgL_portz00_85);
								}
								bgl_display_char(((unsigned char) ' '), BgL_portz00_85);
								{
									long BgL_iz00_5583;

									BgL_iz00_5583 = (1L + BgL_iz00_2096);
									BgL_iz00_2096 = BgL_iz00_5583;
									goto BgL_zc3z04anonymousza31670ze3z87_2097;
								}
							}
					}
				}
		}

	}



/* write/display-tvector */
	obj_t BGl_writezf2displayzd2tvectorz20zz__r4_output_6_10_3z00(obj_t
		BgL_tvecz00_87, obj_t BgL_portz00_88, obj_t BgL_dispz00_89)
	{
		{	/* Ieee/output.scm 1002 */
			{	/* Ieee/output.scm 1003 */
				obj_t BgL_tvectorzd2refzd2_2105;
				obj_t BgL_idz00_2106;

				BgL_tvectorzd2refzd2_2105 =
					BGl_tvectorzd2refzd2zz__tvectorz00(BgL_tvecz00_87);
				BgL_idz00_2106 = BGl_tvectorzd2idzd2zz__tvectorz00(BgL_tvecz00_87);
				bgl_display_char(((unsigned char) '#'), BgL_portz00_88);
				BGL_PROCEDURE_CALL2(BgL_dispz00_89, BgL_idz00_2106, BgL_portz00_88);
				bgl_display_char(((unsigned char) '('), BgL_portz00_88);
				if (CBOOL(BgL_tvectorzd2refzd2_2105))
					{	/* Ieee/output.scm 1008 */
						if ((0L == (long) (TVECTOR_LENGTH(BgL_tvecz00_87))))
							{	/* Ieee/output.scm 1013 */
								return bgl_display_char(((unsigned char) ')'), BgL_portz00_88);
							}
						else
							{	/* Ieee/output.scm 1015 */
								long BgL_lenz00_2109;

								BgL_lenz00_2109 =
									((long) (TVECTOR_LENGTH(BgL_tvecz00_87)) - 1L);
								{
									long BgL_iz00_2111;

									BgL_iz00_2111 = 0L;
								BgL_zc3z04anonymousza31687ze3z87_2112:
									if ((BgL_iz00_2111 == BgL_lenz00_2109))
										{	/* Ieee/output.scm 1018 */
											{	/* Ieee/output.scm 1019 */
												obj_t BgL_arg1689z00_2114;

												BgL_arg1689z00_2114 =
													BGL_PROCEDURE_CALL2(BgL_tvectorzd2refzd2_2105,
													BgL_tvecz00_87, BINT(BgL_iz00_2111));
												BGL_PROCEDURE_CALL2(BgL_dispz00_89, BgL_arg1689z00_2114,
													BgL_portz00_88);
											}
											return
												bgl_display_char(((unsigned char) ')'), BgL_portz00_88);
										}
									else
										{	/* Ieee/output.scm 1018 */
											{	/* Ieee/output.scm 1022 */
												obj_t BgL_arg1691z00_2115;

												BgL_arg1691z00_2115 =
													BGL_PROCEDURE_CALL2(BgL_tvectorzd2refzd2_2105,
													BgL_tvecz00_87, BINT(BgL_iz00_2111));
												BGL_PROCEDURE_CALL2(BgL_dispz00_89, BgL_arg1691z00_2115,
													BgL_portz00_88);
											}
											bgl_display_char(((unsigned char) ' '), BgL_portz00_88);
											{
												long BgL_iz00_5630;

												BgL_iz00_5630 = (1L + BgL_iz00_2111);
												BgL_iz00_2111 = BgL_iz00_5630;
												goto BgL_zc3z04anonymousza31687ze3z87_2112;
											}
										}
								}
							}
					}
				else
					{	/* Ieee/output.scm 1008 */
						bgl_display_string(BGl_string2333z00zz__r4_output_6_10_3z00,
							BgL_portz00_88);
						return BgL_tvecz00_87;
					}
			}
		}

	}



/* write/display-hvector */
	obj_t BGl_writezf2displayzd2hvectorz20zz__r4_output_6_10_3z00(obj_t
		BgL_svecz00_90, obj_t BgL_portz00_91, obj_t BgL_dispz00_92)
	{
		{	/* Ieee/output.scm 1029 */
			{	/* Ieee/output.scm 1030 */
				obj_t BgL_idz00_2120;

				BgL_idz00_2120 =
					BGl_homogeneouszd2vectorzd2infoz00zz__srfi4z00(BgL_svecz00_90);
				{	/* Ieee/output.scm 1031 */
					obj_t BgL__z00_2121;
					obj_t BgL_vrefz00_2122;
					obj_t BgL__z00_2123;
					obj_t BgL__z00_2124;

					{	/* Ieee/output.scm 1032 */
						obj_t BgL_tmpz00_3468;

						{	/* Ieee/output.scm 1032 */
							int BgL_tmpz00_5634;

							BgL_tmpz00_5634 = (int) (1L);
							BgL_tmpz00_3468 = BGL_MVALUES_VAL(BgL_tmpz00_5634);
						}
						{	/* Ieee/output.scm 1032 */
							int BgL_tmpz00_5637;

							BgL_tmpz00_5637 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_5637, BUNSPEC);
						}
						BgL__z00_2121 = BgL_tmpz00_3468;
					}
					{	/* Ieee/output.scm 1032 */
						obj_t BgL_tmpz00_3469;

						{	/* Ieee/output.scm 1032 */
							int BgL_tmpz00_5640;

							BgL_tmpz00_5640 = (int) (2L);
							BgL_tmpz00_3469 = BGL_MVALUES_VAL(BgL_tmpz00_5640);
						}
						{	/* Ieee/output.scm 1032 */
							int BgL_tmpz00_5643;

							BgL_tmpz00_5643 = (int) (2L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_5643, BUNSPEC);
						}
						BgL_vrefz00_2122 = BgL_tmpz00_3469;
					}
					{	/* Ieee/output.scm 1032 */
						obj_t BgL_tmpz00_3470;

						{	/* Ieee/output.scm 1032 */
							int BgL_tmpz00_5646;

							BgL_tmpz00_5646 = (int) (3L);
							BgL_tmpz00_3470 = BGL_MVALUES_VAL(BgL_tmpz00_5646);
						}
						{	/* Ieee/output.scm 1032 */
							int BgL_tmpz00_5649;

							BgL_tmpz00_5649 = (int) (3L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_5649, BUNSPEC);
						}
						BgL__z00_2123 = BgL_tmpz00_3470;
					}
					{	/* Ieee/output.scm 1032 */
						obj_t BgL_tmpz00_3471;

						{	/* Ieee/output.scm 1032 */
							int BgL_tmpz00_5652;

							BgL_tmpz00_5652 = (int) (4L);
							BgL_tmpz00_3471 = BGL_MVALUES_VAL(BgL_tmpz00_5652);
						}
						{	/* Ieee/output.scm 1032 */
							int BgL_tmpz00_5655;

							BgL_tmpz00_5655 = (int) (4L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_5655, BUNSPEC);
						}
						BgL__z00_2124 = BgL_tmpz00_3471;
					}
					bgl_display_char(((unsigned char) '#'), BgL_portz00_91);
					{	/* Ieee/output.scm 773 */
						obj_t BgL_arg1598z00_3474;

						BgL_arg1598z00_3474 = SYMBOL_TO_STRING(((obj_t) BgL_idz00_2120));
						bgl_display_string(BgL_arg1598z00_3474, BgL_portz00_91);
					}
					bgl_display_char(((unsigned char) '('), BgL_portz00_91);
					{	/* Ieee/output.scm 1035 */
						bool_t BgL_test2696z00_5663;

						{	/* Ieee/output.scm 1035 */
							long BgL_arg1709z00_2137;

							BgL_arg1709z00_2137 = BGL_HVECTOR_LENGTH(BgL_svecz00_90);
							BgL_test2696z00_5663 = (0L == BgL_arg1709z00_2137);
						}
						if (BgL_test2696z00_5663)
							{	/* Ieee/output.scm 1035 */
								return bgl_display_char(((unsigned char) ')'), BgL_portz00_91);
							}
						else
							{	/* Ieee/output.scm 1037 */
								long BgL_lenz00_2127;

								{	/* Ieee/output.scm 1037 */
									long BgL_arg1708z00_2136;

									BgL_arg1708z00_2136 = BGL_HVECTOR_LENGTH(BgL_svecz00_90);
									BgL_lenz00_2127 = (BgL_arg1708z00_2136 - 1L);
								}
								{
									long BgL_iz00_2129;

									BgL_iz00_2129 = 0L;
								BgL_zc3z04anonymousza31703ze3z87_2130:
									if ((BgL_iz00_2129 == BgL_lenz00_2127))
										{	/* Ieee/output.scm 1040 */
											{	/* Ieee/output.scm 1041 */
												obj_t BgL_arg1705z00_2132;

												BgL_arg1705z00_2132 =
													BGL_PROCEDURE_CALL2(BgL_vrefz00_2122, BgL_svecz00_90,
													BINT(BgL_iz00_2129));
												BGL_PROCEDURE_CALL2(BgL_dispz00_92, BgL_arg1705z00_2132,
													BgL_portz00_91);
											}
											return
												bgl_display_char(((unsigned char) ')'), BgL_portz00_91);
										}
									else
										{	/* Ieee/output.scm 1040 */
											{	/* Ieee/output.scm 1044 */
												obj_t BgL_arg1706z00_2133;

												BgL_arg1706z00_2133 =
													BGL_PROCEDURE_CALL2(BgL_vrefz00_2122, BgL_svecz00_90,
													BINT(BgL_iz00_2129));
												BGL_PROCEDURE_CALL2(BgL_dispz00_92, BgL_arg1706z00_2133,
													BgL_portz00_91);
											}
											bgl_display_char(((unsigned char) ' '), BgL_portz00_91);
											{
												long BgL_iz00_5695;

												BgL_iz00_5695 = (1L + BgL_iz00_2129);
												BgL_iz00_2129 = BgL_iz00_5695;
												goto BgL_zc3z04anonymousza31703ze3z87_2130;
											}
										}
								}
							}
					}
				}
			}
		}

	}



/* display-pair */
	obj_t BGl_displayzd2pairzd2zz__r4_output_6_10_3z00(obj_t BgL_objz00_96,
		obj_t BgL_portz00_97)
	{
		{	/* Ieee/output.scm 1083 */
			bgl_display_char(((unsigned char) '('), BgL_portz00_97);
			{
				obj_t BgL_lz00_2140;

				BgL_lz00_2140 = BgL_objz00_96;
			BgL_zc3z04anonymousza31710ze3z87_2141:
				if (NULLP(CDR(((obj_t) BgL_lz00_2140))))
					{	/* Ieee/output.scm 1084 */
						{	/* Ieee/output.scm 1084 */
							obj_t BgL_arg1714z00_2144;

							BgL_arg1714z00_2144 = CAR(((obj_t) BgL_lz00_2140));
							bgl_display_obj(BgL_arg1714z00_2144, BgL_portz00_97);
						}
						return bgl_display_char(((unsigned char) ')'), BgL_portz00_97);
					}
				else
					{	/* Ieee/output.scm 1084 */
						bool_t BgL_test2699z00_5706;

						{	/* Ieee/output.scm 1084 */
							obj_t BgL_tmpz00_5707;

							BgL_tmpz00_5707 = CDR(((obj_t) BgL_lz00_2140));
							BgL_test2699z00_5706 = PAIRP(BgL_tmpz00_5707);
						}
						if (BgL_test2699z00_5706)
							{	/* Ieee/output.scm 1084 */
								{	/* Ieee/output.scm 1084 */
									obj_t BgL_arg1717z00_2147;

									BgL_arg1717z00_2147 = CAR(((obj_t) BgL_lz00_2140));
									bgl_display_obj(BgL_arg1717z00_2147, BgL_portz00_97);
								}
								bgl_display_char(((unsigned char) ' '), BgL_portz00_97);
								{	/* Ieee/output.scm 1084 */
									obj_t BgL_arg1718z00_2148;

									BgL_arg1718z00_2148 = CDR(((obj_t) BgL_lz00_2140));
									{
										obj_t BgL_lz00_5717;

										BgL_lz00_5717 = BgL_arg1718z00_2148;
										BgL_lz00_2140 = BgL_lz00_5717;
										goto BgL_zc3z04anonymousza31710ze3z87_2141;
									}
								}
							}
						else
							{	/* Ieee/output.scm 1084 */
								{	/* Ieee/output.scm 1084 */
									obj_t BgL_arg1720z00_2149;

									BgL_arg1720z00_2149 = CAR(((obj_t) BgL_lz00_2140));
									bgl_display_obj(BgL_arg1720z00_2149, BgL_portz00_97);
								}
								bgl_display_char(((unsigned char) ' '), BgL_portz00_97);
								bgl_display_char(((unsigned char) '.'), BgL_portz00_97);
								bgl_display_char(((unsigned char) ' '), BgL_portz00_97);
								{	/* Ieee/output.scm 1084 */
									obj_t BgL_arg1722z00_2150;

									BgL_arg1722z00_2150 = CDR(((obj_t) BgL_lz00_2140));
									bgl_display_obj(BgL_arg1722z00_2150, BgL_portz00_97);
								}
								return bgl_display_char(((unsigned char) ')'), BgL_portz00_97);
		}}}}

	}



/* write-pair */
	obj_t BGl_writezd2pairzd2zz__r4_output_6_10_3z00(obj_t BgL_objz00_98,
		obj_t BgL_portz00_99)
	{
		{	/* Ieee/output.scm 1089 */
			bgl_display_char(((unsigned char) '('), BgL_portz00_99);
			{
				obj_t BgL_lz00_2155;

				BgL_lz00_2155 = BgL_objz00_98;
			BgL_zc3z04anonymousza31725ze3z87_2156:
				if (NULLP(CDR(((obj_t) BgL_lz00_2155))))
					{	/* Ieee/output.scm 1090 */
						{	/* Ieee/output.scm 1090 */
							obj_t BgL_arg1728z00_2159;

							BgL_arg1728z00_2159 = CAR(((obj_t) BgL_lz00_2155));
							bgl_write_obj(BgL_arg1728z00_2159, BgL_portz00_99);
						}
						return bgl_display_char(((unsigned char) ')'), BgL_portz00_99);
					}
				else
					{	/* Ieee/output.scm 1090 */
						bool_t BgL_test2701z00_5737;

						{	/* Ieee/output.scm 1090 */
							obj_t BgL_tmpz00_5738;

							BgL_tmpz00_5738 = CDR(((obj_t) BgL_lz00_2155));
							BgL_test2701z00_5737 = PAIRP(BgL_tmpz00_5738);
						}
						if (BgL_test2701z00_5737)
							{	/* Ieee/output.scm 1090 */
								{	/* Ieee/output.scm 1090 */
									obj_t BgL_arg1731z00_2162;

									BgL_arg1731z00_2162 = CAR(((obj_t) BgL_lz00_2155));
									bgl_write_obj(BgL_arg1731z00_2162, BgL_portz00_99);
								}
								bgl_display_char(((unsigned char) ' '), BgL_portz00_99);
								{	/* Ieee/output.scm 1090 */
									obj_t BgL_arg1733z00_2163;

									BgL_arg1733z00_2163 = CDR(((obj_t) BgL_lz00_2155));
									{
										obj_t BgL_lz00_5748;

										BgL_lz00_5748 = BgL_arg1733z00_2163;
										BgL_lz00_2155 = BgL_lz00_5748;
										goto BgL_zc3z04anonymousza31725ze3z87_2156;
									}
								}
							}
						else
							{	/* Ieee/output.scm 1090 */
								{	/* Ieee/output.scm 1090 */
									obj_t BgL_arg1734z00_2164;

									BgL_arg1734z00_2164 = CAR(((obj_t) BgL_lz00_2155));
									bgl_write_obj(BgL_arg1734z00_2164, BgL_portz00_99);
								}
								bgl_display_char(((unsigned char) ' '), BgL_portz00_99);
								bgl_display_char(((unsigned char) '.'), BgL_portz00_99);
								bgl_display_char(((unsigned char) ' '), BgL_portz00_99);
								{	/* Ieee/output.scm 1090 */
									obj_t BgL_arg1735z00_2165;

									BgL_arg1735z00_2165 = CDR(((obj_t) BgL_lz00_2155));
									bgl_write_obj(BgL_arg1735z00_2165, BgL_portz00_99);
								}
								return bgl_display_char(((unsigned char) ')'), BgL_portz00_99);
		}}}}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__r4_output_6_10_3z00(void)
	{
		{	/* Ieee/output.scm 24 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__r4_output_6_10_3z00(void)
	{
		{	/* Ieee/output.scm 24 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__r4_output_6_10_3z00(void)
	{
		{	/* Ieee/output.scm 24 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__r4_output_6_10_3z00(void)
	{
		{	/* Ieee/output.scm 24 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string2334z00zz__r4_output_6_10_3z00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string2334z00zz__r4_output_6_10_3z00));
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L,
				BSTRING_TO_STRING(BGl_string2334z00zz__r4_output_6_10_3z00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00
				(268155843L,
				BSTRING_TO_STRING(BGl_string2334z00zz__r4_output_6_10_3z00));
			BGl_modulezd2initializa7ationz75zz__biglooz00(536217185L,
				BSTRING_TO_STRING(BGl_string2334z00zz__r4_output_6_10_3z00));
			BGl_modulezd2initializa7ationz75zz__srfi4z00(467924964L,
				BSTRING_TO_STRING(BGl_string2334z00zz__r4_output_6_10_3z00));
			return BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string2334z00zz__r4_output_6_10_3z00));
		}

	}

#ifdef __cplusplus
}
#endif
