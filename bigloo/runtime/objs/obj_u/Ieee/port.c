/*===========================================================================*/
/*   (Ieee/port.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Ieee/port.scm -indent -o objs/obj_u/Ieee/port.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___R4_PORTS_6_10_1_TYPE_DEFINITIONS
#define BGL___R4_PORTS_6_10_1_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_z62httpzd2redirectionzb0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_portz00;
		obj_t BgL_urlz00;
	}                               *BgL_z62httpzd2redirectionzb0_bglt;


#endif													// BGL___R4_PORTS_6_10_1_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62outputzd2portzd2truncatez62zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62directoryzd2ze3pathzd2listz81zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	extern obj_t bgl_open_input_descriptor(int, obj_t);
	static obj_t BGl__selectz00zz__r4_ports_6_10_1z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_appendzd2outputzd2filez00zz__r4_ports_6_10_1z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_outputzd2portzd2closezd2hookzd2zz__r4_ports_6_10_1z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_callzd2withzd2inputzd2stringzd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_openzd2inputzd2gza7ipzd2portz75zz__r4_ports_6_10_1z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_inputzd2portzd2namez00zz__r4_ports_6_10_1z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_outputzd2portzd2namez00zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl__openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	static obj_t BGl_z62inputzd2portzd2namez62zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	static obj_t
		BGl_z62outputzd2portzd2flushzd2bufferzd2setz12z70zz__r4_ports_6_10_1z00
		(obj_t, obj_t, obj_t);
	static obj_t BGl_z62truncatezd2filezb0zz__r4_ports_6_10_1z00(obj_t, obj_t,
		obj_t);
	extern bool_t reset_eof(obj_t);
	extern obj_t bgl_directory_to_list(char *);
	BGL_EXPORTED_DECL obj_t
		BGl_inputzd2portzd2clonez12z12zz__r4_ports_6_10_1z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_closedzd2inputzd2portzf3zf3zz__r4_ports_6_10_1z00(obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_makezd2directorieszd2zz__r4_ports_6_10_1z00(obj_t);
	static obj_t
		BGl_za2inputzd2portzd2protocolszd2mutexza2zd2zz__r4_ports_6_10_1z00 =
		BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_inputzd2portzd2namezd2setz12zc0zz__r4_ports_6_10_1z00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__r4_ports_6_10_1z00 =
		BUNSPEC;
	BGL_EXPORTED_DECL bool_t
		BGl_outputzd2portzd2truncatez00zz__r4_ports_6_10_1z00(obj_t, long);
	BGL_EXPORTED_DECL obj_t
		BGl_directoryzd2ze3pathzd2listze3zz__r4_ports_6_10_1z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_withzd2outputzd2tozd2filezd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
	static obj_t
		BGl_z62withzd2errorzd2tozd2procedurezb0zz__r4_ports_6_10_1z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_outputzd2stringzd2portzf3zf3zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_symbol2583z00zz__r4_ports_6_10_1z00 = BUNSPEC;
	BGL_EXPORTED_DECL bool_t BGl_filezd2existszf3z21zz__r4_ports_6_10_1z00(char
		*);
	static obj_t BGl__lockfz00zz__r4_ports_6_10_1z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_currentzd2outputzd2portz00zz__r4_ports_6_10_1z00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_resetzd2outputzd2portz00zz__r4_ports_6_10_1z00(obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t bgl_input_port_reopen(obj_t);
	extern obj_t bgl_system_failure(int, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_setzd2outputzd2portzd2positionz12zc0zz__r4_ports_6_10_1z00(obj_t, long);
	extern obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_symbol2591z00zz__r4_ports_6_10_1z00 = BUNSPEC;
	static obj_t BGl_symbol2593z00zz__r4_ports_6_10_1z00 = BUNSPEC;
	static obj_t BGl_symbol2595z00zz__r4_ports_6_10_1z00 = BUNSPEC;
	extern obj_t BGl_z62httpzd2redirectionzb0zz__httpz00;
	extern int bgl_input_fill_string(obj_t, obj_t);
	static obj_t BGl_symbol2597z00zz__r4_ports_6_10_1z00 = BUNSPEC;
	static obj_t BGl_z62inputzd2portzd2bufferz62zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	static obj_t BGl__openzd2outputzd2stringz00zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	extern obj_t BGl_assocz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_renamezd2filezd2zz__r4_ports_6_10_1z00(char *,
		char *);
	extern obj_t bgl_open_input_resource(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_closedzd2outputzd2portzf3zf3zz__r4_ports_6_10_1z00(obj_t);
	extern long bgl_file_size(char *);
	static obj_t BGl_toplevelzd2initzd2zz__r4_ports_6_10_1z00(void);
	extern obj_t bgl_output_port_seek(obj_t, long);
	BGL_EXPORTED_DECL obj_t
		BGl_withzd2errorzd2tozd2filezd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_deletezd2directoryzd2zz__r4_ports_6_10_1z00(char
		*);
	static obj_t BGl__openzd2outputzd2filez00zz__r4_ports_6_10_1z00(obj_t, obj_t);
	static obj_t BGl_z62outputzd2portzd2timeoutz62zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_setzd2inputzd2portzd2positionz12zc0zz__r4_ports_6_10_1z00(obj_t, long);
	BGL_EXPORTED_DECL obj_t
		BGl_outputzd2portzd2closezd2hookzd2setz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_directoryzd2lengthzd2zz__r4_ports_6_10_1z00(char
		*);
	BGL_EXPORTED_DECL obj_t
		BGl_closezd2outputzd2portz00zz__r4_ports_6_10_1z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_openzd2inputzd2mmapz00zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
	static obj_t BGl_search1309ze70ze7zz__r4_ports_6_10_1z00(long, obj_t, obj_t,
		long);
	static obj_t BGl_z62currentzd2errorzd2portz62zz__r4_ports_6_10_1z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_openzd2inputzd2descriptorz00zz__r4_ports_6_10_1z00(int, obj_t);
	static obj_t
		BGl_z62z52openzd2inputzd2httpzd2socketze2zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zz__r4_ports_6_10_1z00(void);
	BGL_EXPORTED_DECL long
		BGl_filezd2modificationzd2timez00zz__r4_ports_6_10_1z00(char *);
	static obj_t BGl_z62makezd2directoryzb0zz__r4_ports_6_10_1z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_outputzd2portzd2namezd2setz12zc0zz__r4_ports_6_10_1z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_outputzd2portzd2isattyzf3zf3zz__r4_ports_6_10_1z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_callzd2withzd2appendzd2filezd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
	static obj_t
		BGl_z62outputzd2portzd2timeoutzd2setz12za2zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62outputzd2portzd2namez62zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	static obj_t BGl_objectzd2initzd2zz__r4_ports_6_10_1z00(void);
	extern obj_t BGl_gunza7ipzd2parsezd2headerza7zz__gunza7ipza7(obj_t);
	static obj_t BGl_z62inputzd2portzd2reopenz12z70zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_outputzd2portzf3z21zz__r4_ports_6_10_1z00(obj_t);
	extern obj_t bgl_open_pipes(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_closezd2inputzd2portz00zz__r4_ports_6_10_1z00(obj_t);
	extern obj_t bgl_open_input_mmap(obj_t, obj_t, long, long);
	static obj_t BGl_z62makezd2directorieszb0zz__r4_ports_6_10_1z00(obj_t, obj_t);
	extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern long bgl_file_mode(char *);
	BGL_EXPORTED_DECL long BGl_filezd2accesszd2timez00zz__r4_ports_6_10_1z00(char
		*);
	static obj_t BGl_z62withzd2errorzd2tozd2portzb0zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	extern obj_t string_append_3(obj_t, obj_t, obj_t);
	extern obj_t BGl_dirnamez00zz__osz00(obj_t);
	static obj_t
		BGl_z62withzd2outputzd2tozd2procedurezb0zz__r4_ports_6_10_1z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62inputzd2portzd2positionz62zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	extern obj_t BGl_openzd2inputzd2binaryzd2filezd2zz__binaryz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_callzd2withzd2outputzd2filezd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_inputzd2portzd2timeoutzd2setz12zc0zz__r4_ports_6_10_1z00(obj_t, long);
	static obj_t BGl__openzd2inputzd2stringz00zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_directoryzd2ze3vectorz31zz__r4_ports_6_10_1z00(char *);
	static obj_t BGl_z62filezd2uidzb0zz__r4_ports_6_10_1z00(obj_t, obj_t);
	extern int bgl_output_string(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_inputzd2mmapzd2portzf3zf3zz__r4_ports_6_10_1z00(obj_t);
	static obj_t
		BGl_z62outputzd2procedurezd2portzf3z91zz__r4_ports_6_10_1z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_inputzd2portzd2fillzd2barrierzd2zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_z62withzd2errorzd2tozd2stringzb0zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL long BGl_filezd2changezd2timez00zz__r4_ports_6_10_1z00(char
		*);
	extern obj_t BGl_openzd2outputzd2binaryzd2filezd2zz__binaryz00(obj_t);
	static obj_t
		BGl_z62directoryzd2ze3pathzd2vectorz81zz__r4_ports_6_10_1z00(obj_t, obj_t);
	BGL_EXPORTED_DECL long
		BGl_inputzd2portzd2timeoutz00zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_z62currentzd2inputzd2portz62zz__r4_ports_6_10_1z00(obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_getzd2outputzd2stringz00zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_z62z52openzd2inputzd2filez30zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62outputzd2portzd2closezd2hookzb0zz__r4_ports_6_10_1z00(obj_t, obj_t);
	static obj_t
		BGl_z62callzd2withzd2inputzd2stringzb0zz__r4_ports_6_10_1z00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62outputzd2portzd2namezd2setz12za2zz__r4_ports_6_10_1z00(obj_t, obj_t,
		obj_t);
	extern bool_t bgl_input_port_timeout_set(obj_t, long);
	BGL_EXPORTED_DECL obj_t
		BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62deletezd2directoryzb0zz__r4_ports_6_10_1z00(obj_t, obj_t);
	static obj_t
		BGl_z62callzd2withzd2appendzd2filezb0zz__r4_ports_6_10_1z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_inputzd2portzf3z21zz__r4_ports_6_10_1z00(obj_t);
	static obj_t
		BGl_z62inputzd2portzd2protocolzd2setz12za2zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	extern void bgl_input_port_seek(obj_t, long);
	static obj_t BGl_methodzd2initzd2zz__r4_ports_6_10_1z00(void);
	static obj_t BGl_z62withzd2outputzd2tozd2portzb0zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	extern obj_t bgl_input_port_clone(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_withzd2errorzd2tozd2stringzd2zz__r4_ports_6_10_1z00(obj_t);
	extern long bgl_directory_length(char *);
	static obj_t BGl_z62directoryzd2lengthzb0zz__r4_ports_6_10_1z00(obj_t, obj_t);
	static obj_t BGl_z62close2049z62zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31413ze3ze5zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62outputzd2portzd2bufferz62zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	extern obj_t get_output_string(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_directoryzf3zf3zz__r4_ports_6_10_1z00(char *);
	static obj_t
		BGl_z62inputzd2portzd2lastzd2tokenzd2positionz62zz__r4_ports_6_10_1z00
		(obj_t, obj_t);
	static obj_t BGl__openzd2inputzd2filez00zz__r4_ports_6_10_1z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_makezd2directoryzd2zz__r4_ports_6_10_1z00(char
		*);
	extern obj_t bgl_open_input_substring(obj_t, long, long);
	BGL_EXPORTED_DECL obj_t
		BGl_inputzd2portzd2fillzd2barrierzd2setz12z12zz__r4_ports_6_10_1z00(obj_t,
		long);
	static obj_t BGl_z62close2051z62zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_z62close2052z62zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_z62close2053z62zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_z62filezd2gidzb0zz__r4_ports_6_10_1z00(obj_t, obj_t);
	static obj_t
		BGl_z62outputzd2portzd2bufferzd2setz12za2zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	extern int bgl_symlink(char *, char *);
	static obj_t BGl_z62renamezd2filezb0zz__r4_ports_6_10_1z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62withzd2inputzd2fromzd2portzb0zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_outputzd2portzd2flushzd2bufferzd2setz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_truncatezd2filezd2zz__r4_ports_6_10_1z00(char *,
		long);
	static obj_t BGl_z62z52openzd2inputzd2pipez30zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62callzd2withzd2outputzd2filezb0zz__r4_ports_6_10_1z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL long
		BGl_outputzd2portzd2timeoutz00zz__r4_ports_6_10_1z00(obj_t);
	extern obj_t BGl_urlzd2sanszd2protocolzd2parsezd2zz__urlz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_openzd2outputzd2filez00zz__r4_ports_6_10_1z00(obj_t, obj_t);
	extern obj_t bgl_reopen_input_c_string(obj_t, char *);
	static obj_t BGl_z62withzd2appendzd2tozd2filezb0zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62outputzd2portzd2closezd2hookzd2setz12z70zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	extern obj_t make_string(long, unsigned char);
	extern obj_t bgl_open_input_c_string(char *);
	static obj_t BGl_z62inputzd2portzd2clonez12z70zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	extern obj_t bgl_close_input_port(obj_t);
	static obj_t BGl_z62closezd2inputzd2portz62zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_flushzd2outputzd2portz00zz__r4_ports_6_10_1z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_openzd2outputzd2stringz00zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_z62directoryzf3z91zz__r4_ports_6_10_1z00(obj_t, obj_t);
	extern obj_t close_binary_port(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_deletezd2filezd2zz__r4_ports_6_10_1z00(char *);
	static obj_t BGl_z62filezd2accesszd2timez62zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	static obj_t BGl__openzd2outputzd2procedurez00zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	extern obj_t bgl_append_output_file(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_withzd2inputzd2fromzd2portzd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
	static obj_t BGl_z62resetzd2outputzd2portz62zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	static obj_t BGl_z62inputzd2mmapzd2portzf3z91zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	extern bool_t bigloo_strncmp(obj_t, obj_t, long);
	BGL_EXPORTED_DECL obj_t
		BGl_callzd2withzd2outputzd2stringzd2zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_z62flushz62zz__r4_ports_6_10_1z00(obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t bgl_close_output_port(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31441ze3ze5zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_z62copyzd2filezb0zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62filezd2changezd2timez62zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	static obj_t BGl_z62filezd2typezb0zz__r4_ports_6_10_1z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_inputzd2portzd2seekz00zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_z62inputzd2portzd2seekz62zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	extern obj_t BGl_substringz00zz__r4_strings_6_7z00(obj_t, long, long);
	extern bool_t BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	BGL_EXPORTED_DECL long
		BGl_inputzd2portzd2positionz00zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl__openzd2inputzd2procedurez00zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	static obj_t BGl_z62filezd2modificationzd2timez62zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	static obj_t BGl_list2451z00zz__r4_ports_6_10_1z00 = BUNSPEC;
	static obj_t BGl_list2452z00zz__r4_ports_6_10_1z00 = BUNSPEC;
	BGL_EXPORTED_DECL int BGl_filezd2timeszd2setz12z12zz__r4_ports_6_10_1z00(char
		*, long, long);
	static obj_t BGl_list2456z00zz__r4_ports_6_10_1z00 = BUNSPEC;
	static obj_t BGl_z62filezd2timeszd2setz12z70zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_outputzd2portzd2timeoutzd2setz12zc0zz__r4_ports_6_10_1z00(obj_t, long);
	BGL_EXPORTED_DECL obj_t BGl_makezd2symlinkzd2zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	static obj_t BGl_z62filezd2existszf3z43zz__r4_ports_6_10_1z00(obj_t, obj_t);
	extern obj_t BGl_portzd2ze3gza7ipzd2portz44zz__gunza7ipza7(obj_t, obj_t);
	static obj_t BGl_z62inputzd2stringzd2portzf3z91zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	static obj_t BGl_z62closezd2outputzd2portz62zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	extern obj_t bstring_to_keyword(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31451ze3ze5zz__r4_ports_6_10_1z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL long
		BGl_inputzd2portzd2lastzd2tokenzd2positionz00zz__r4_ports_6_10_1z00(obj_t);
	static obj_t
		BGl_z62inputzd2portzd2seekzd2setz12za2zz__r4_ports_6_10_1z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_withzd2errorzd2tozd2procedurezd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_withzd2inputzd2fromzd2stringzd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_withzd2outputzd2tozd2procedurezd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
	extern obj_t bgl_open_input_procedure(obj_t, obj_t);
	static obj_t BGl_z62outputzd2portzd2positionz62zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL int BGl_filezd2uidzd2zz__r4_ports_6_10_1z00(char *);
	BGL_EXPORTED_DECL long
		BGl_inputzd2portzd2lengthz00zz__r4_ports_6_10_1z00(obj_t);
	extern obj_t bgl_directory_to_vector(char *);
	BGL_EXPORTED_DECL obj_t
		BGl_reopenzd2inputzd2czd2stringzd2zz__r4_ports_6_10_1z00(obj_t, char *);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__ftpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__httpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__urlz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__gunza7ipza7(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_EXPORTED_DECL obj_t
		BGl_withzd2outputzd2tozd2portzd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
	extern obj_t bgl_open_output_file(obj_t, obj_t);
	extern obj_t bgl_open_input_substring_bang(obj_t, long, long);
	static obj_t BGl_z62inputzd2portzf3z43zz__r4_ports_6_10_1z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_inputzd2gza7ipzd2portzf3z54zz__r4_ports_6_10_1z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_copyzd2filezd2zz__r4_ports_6_10_1z00(char *,
		char *);
	static obj_t BGl_z62openzd2inputzd2czd2stringzb0zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_currentzd2errorzd2portz00zz__r4_ports_6_10_1z00(void);
	static obj_t BGl_z62closedzd2inputzd2portzf3z91zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_filezd2typezd2zz__r4_ports_6_10_1z00(char *);
	static obj_t BGl_z62zc3z04anonymousza31453ze3ze5zz__r4_ports_6_10_1z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_inputzd2portzd2seekzd2setz12zc0zz__r4_ports_6_10_1z00(obj_t, obj_t);
	extern long bgl_last_access_time(char *);
	static obj_t BGl_z62zc3z04anonymousza31437ze3ze5zz__r4_ports_6_10_1z00(obj_t);
	extern bool_t fexists(char *);
	BGL_EXPORTED_DECL long
		BGl_outputzd2portzd2positionz00zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl__openzd2inputzd2gza7ipzd2portz75zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	static obj_t BGl_list2574z00zz__r4_ports_6_10_1z00 = BUNSPEC;
	static obj_t BGl_z62flush2050z62zz__r4_ports_6_10_1z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_outputzd2portzd2flushzd2hookzd2zz__r4_ports_6_10_1z00(obj_t);
	extern obj_t BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(long,
		long);
	static obj_t BGl_z62zc3z04anonymousza31462ze3ze5zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_z62flush2054z62zz__r4_ports_6_10_1z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_inputzd2portzd2reopenz12z12zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_z62directoryzd2ze3vectorz53zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_inputzd2portzd2protocolzd2setz12zc0zz__r4_ports_6_10_1z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_withzd2appendzd2tozd2filezd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
	static obj_t
		BGl_z62outputzd2portzd2flushzd2bufferzb0zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	extern obj_t bgl_open_input_file(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_inputzd2portzd2bufferzd2setz12zc0zz__r4_ports_6_10_1z00(obj_t, obj_t);
	extern long bgl_last_change_time(char *);
	BGL_EXPORTED_DECL obj_t BGl_filezd2gza7ipzf3z86zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_z62outputzd2portzf3z43zz__r4_ports_6_10_1z00(obj_t, obj_t);
	extern obj_t bgl_open_output_procedure(obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_outputzd2portzd2flushzd2hookzd2setz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	static obj_t BGl_z62z52openzd2inputzd2resourcez30zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31455ze3ze5zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31447ze3ze5zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31439ze3ze5zz__r4_ports_6_10_1z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_withzd2errorzd2tozd2portzd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__r4_ports_6_10_1z00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(obj_t, obj_t, int);
	static obj_t BGl_z62getzd2portzd2bufferz62zz__r4_ports_6_10_1z00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL int BGl_filezd2gidzd2zz__r4_ports_6_10_1z00(char *);
	static obj_t BGl__appendzd2outputzd2filez00zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_portzf3zf3zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_z62deletezd2filezb0zz__r4_ports_6_10_1z00(obj_t, obj_t);
	static obj_t BGl_z62getzd2outputzd2stringz62zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__r4_ports_6_10_1z00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_z62filezd2siza7ez17zz__r4_ports_6_10_1z00(obj_t, obj_t);
	static obj_t BGl_gczd2rootszd2initz00zz__r4_ports_6_10_1z00(void);
	static obj_t BGl_z62zc3z04anonymousza31464ze3ze5zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31456ze3ze5zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31448ze3ze5zz__r4_ports_6_10_1z00(obj_t);
	extern bool_t bgl_output_port_timeout_set(obj_t, long);
	static obj_t BGl_z62withzd2errorzd2tozd2filezb0zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62closez62zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl__openzd2pipeszd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
	static obj_t
		BGl_z62reopenzd2inputzd2czd2stringzb0zz__r4_ports_6_10_1z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62inputzd2portzd2protocolz62zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_inputzd2portzd2bufferz00zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_z62filezd2gza7ipzf3ze4zz__r4_ports_6_10_1z00(obj_t, obj_t);
	extern obj_t bgl_open_input_pipe(obj_t, obj_t);
	extern obj_t socket_close(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_currentzd2inputzd2portz00zz__r4_ports_6_10_1z00(void);
	static obj_t BGl_z62zc3z04anonymousza31465ze3ze5zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31457ze3ze5zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31449ze3ze5zz__r4_ports_6_10_1z00(obj_t);
	extern long bgl_input_port_timeout(obj_t);
	extern obj_t bgl_directory_to_path_list(char *, int, unsigned char);
	static obj_t BGl_keyword2453z00zz__r4_ports_6_10_1z00 = BUNSPEC;
	extern long bgl_file_uid(char *);
	static obj_t BGl_z62zc3z04anonymousza31466ze3ze5zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31458ze3ze5zz__r4_ports_6_10_1z00(obj_t);
	static obj_t
		BGl_z62inputzd2portzd2timeoutzd2setz12za2zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_keyword2457z00zz__r4_ports_6_10_1z00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_inputzd2procedurezd2portzf3zf3zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl__openzd2inputzd2descriptorz00zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_openzd2pipeszd2zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_z62loopz62zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_vector2561z00zz__r4_ports_6_10_1z00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_outputzd2portzd2bufferz00zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_z62inputzd2gza7ipzd2portzf3z36zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	static obj_t
		BGl_z62inputzd2portzd2fillzd2barrierzb0zz__r4_ports_6_10_1z00(obj_t, obj_t);
	static obj_t BGl_zc3z04exitza31579ze3ze70z60zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	static obj_t BGl_zc3z04exitza31498ze3ze70z60zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	extern bool_t bgl_lockf(obj_t, int, long);
	static obj_t BGl_z62parserz62zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62callzd2withzd2inputzd2filezb0zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62withzd2inputzd2fromzd2procedurezb0zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	extern bool_t bgl_directoryp(char *);
	static obj_t BGl_z62flushzd2outputzd2portz62zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	static obj_t BGl_z62filezd2modezb0zz__r4_ports_6_10_1z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_inputzd2portzd2closezd2hookzd2setz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	static obj_t BGl_z62withzd2outputzd2tozd2filezb0zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_inputzd2portzd2closezd2hookzd2zz__r4_ports_6_10_1z00(obj_t);
	extern obj_t BGl_httpzd2parsezd2responsez00zz__httpz00(obj_t, obj_t, obj_t);
	extern obj_t c_substring(obj_t, long, long);
	BGL_EXPORTED_DECL long BGl_filezd2siza7ez75zz__r4_ports_6_10_1z00(char *);
	static obj_t
		BGl_z62inputzd2portzd2fillzd2barrierzd2setz12z70zz__r4_ports_6_10_1z00
		(obj_t, obj_t, obj_t);
	static obj_t BGl_z62portzf3z91zz__r4_ports_6_10_1z00(obj_t, obj_t);
	static obj_t BGl_z62outputzd2stringzd2portzf3z91zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	static obj_t BGl_keyword2472z00zz__r4_ports_6_10_1z00 = BUNSPEC;
	static obj_t
		BGl_z62withzd2outputzd2tozd2stringzb0zz__r4_ports_6_10_1z00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31484ze3ze5zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	static obj_t BGl_z62currentzd2outputzd2portz62zz__r4_ports_6_10_1z00(obj_t);
	extern long default_io_bufsiz;
	BGL_EXPORTED_DECL obj_t
		BGl_openzd2inputzd2czd2stringzd2zz__r4_ports_6_10_1z00(char *);
	static obj_t
		BGl_z62setzd2inputzd2portzd2positionz12za2zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_symbol2435z00zz__r4_ports_6_10_1z00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_openzd2outputzd2procedurez00zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t,
		obj_t);
	extern int bgl_utime(char *, long, long);
	static obj_t BGl_za2inputzd2portzd2protocolsza2z00zz__r4_ports_6_10_1z00 =
		BUNSPEC;
	static obj_t BGl_z62withzd2inputzd2fromzd2filezb0zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_callzd2withzd2inputzd2filezd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
	static obj_t BGl_z62resetzd2eofzb0zz__r4_ports_6_10_1z00(obj_t, obj_t);
	extern long bgl_last_modification_time(char *);
	extern obj_t bgl_open_output_string(obj_t);
	static obj_t BGl_z62inputzd2portzd2timeoutz62zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	static obj_t
		BGl_z62inputzd2portzd2closezd2hookzd2setz12z70zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	extern obj_t bgl_select(long, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_outputzd2portzd2bufferzd2setz12zc0zz__r4_ports_6_10_1z00(obj_t, obj_t);
	static obj_t
		BGl_z62withzd2inputzd2fromzd2stringzb0zz__r4_ports_6_10_1z00(obj_t, obj_t,
		obj_t);
	extern long bgl_file_gid(char *);
	BGL_EXPORTED_DECL obj_t
		BGl_openzd2inputzd2procedurez00zz__r4_ports_6_10_1z00(obj_t, obj_t);
	static obj_t BGl_z62closedzd2outputzd2portzf3z91zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31486ze3ze5zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_keyword2575z00zz__r4_ports_6_10_1z00 = BUNSPEC;
	static obj_t BGl_keyword2577z00zz__r4_ports_6_10_1z00 = BUNSPEC;
	static obj_t BGl_keyword2579z00zz__r4_ports_6_10_1z00 = BUNSPEC;
	static obj_t
		BGl_z62inputzd2procedurezd2portzf3z91zz__r4_ports_6_10_1z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_withzd2inputzd2fromzd2filezd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
	static obj_t BGl_symbol2459z00zz__r4_ports_6_10_1z00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_withzd2inputzd2fromzd2procedurezd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
	static obj_t
		BGl_z62outputzd2portzd2flushzd2hookzd2setz12z70zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	extern obj_t bgl_directory_to_path_vector(char *, int, unsigned char);
	BGL_EXPORTED_DECL int BGl_filezd2modezd2zz__r4_ports_6_10_1z00(char *);
	extern obj_t bgl_file_type(char *);
	static obj_t BGl_keyword2581z00zz__r4_ports_6_10_1z00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31576ze3ze5zz__r4_ports_6_10_1z00(obj_t);
	extern obj_t BGl_httpz00zz__httpz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_selectz00zz__r4_ports_6_10_1z00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_symbol2461z00zz__r4_ports_6_10_1z00 = BUNSPEC;
	static obj_t BGl_symbol2463z00zz__r4_ports_6_10_1z00 = BUNSPEC;
	static obj_t BGl__openzd2inputzd2mmapz00zz__r4_ports_6_10_1z00(obj_t, obj_t);
	static obj_t
		BGl_z62inputzd2portzd2closezd2hookzb0zz__r4_ports_6_10_1z00(obj_t, obj_t);
	extern obj_t make_string_sans_fill(long);
	static obj_t BGl_z62outputzd2portzd2isattyzf3z91zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	static obj_t
		BGl_z62outputzd2portzd2flushzd2hookzb0zz__r4_ports_6_10_1z00(obj_t, obj_t);
	static obj_t BGl_z62inputzd2portzd2lengthz62zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_outputzd2procedurezd2portzf3zf3zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_z62makezd2symlinkzb0zz__r4_ports_6_10_1z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_outputzd2portzd2flushzd2bufferzd2zz__r4_ports_6_10_1z00(obj_t);
	extern long bgl_output_port_timeout(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_directoryzd2ze3listz31zz__r4_ports_6_10_1z00(char
		*);
	BGL_EXPORTED_DECL obj_t
		BGl_inputzd2portzd2protocolz00zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_z62directoryzd2ze3listz53zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_inputzd2stringzd2portzf3zf3zz__r4_ports_6_10_1z00(obj_t);
	static obj_t
		BGl_z62setzd2outputzd2portzd2positionz12za2zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62z52openzd2inputzd2descriptorz30zz__r4_ports_6_10_1z00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62inputzd2portzd2bufferzd2setz12za2zz__r4_ports_6_10_1z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_resetzd2eofzd2zz__r4_ports_6_10_1z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_directoryzd2ze3pathzd2vectorze3zz__r4_ports_6_10_1z00(obj_t);
	static obj_t
		BGl_z62callzd2withzd2outputzd2stringzb0zz__r4_ports_6_10_1z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_lockfz00zz__r4_ports_6_10_1z00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62inputzd2portzd2namezd2setz12za2zz__r4_ports_6_10_1z00(obj_t, obj_t,
		obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_openzd2inputzd2mmapzd2envzd2zz__r4_ports_6_10_1z00,
		BgL_bgl__openza7d2inputza7d22602z00, opt_generic_entry,
		BGl__openzd2inputzd2mmapz00zz__r4_ports_6_10_1z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_closedzd2inputzd2portzf3zd2envz21zz__r4_ports_6_10_1z00,
		BgL_bgl_za762closedza7d2inpu2603z00,
		BGl_z62closedzd2inputzd2portzf3z91zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2directorieszd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762makeza7d2direct2604z00,
		BGl_z62makezd2directorieszb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_outputzd2portzd2bufferzd2envzd2zz__r4_ports_6_10_1z00,
		BgL_bgl_za762outputza7d2port2605z00,
		BGl_z62outputzd2portzd2bufferz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_inputzd2portzd2timeoutzd2envzd2zz__r4_ports_6_10_1z00,
		BgL_bgl_za762inputza7d2portza72606za7,
		BGl_z62inputzd2portzd2timeoutz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_filezd2modificationzd2timezd2envzd2zz__r4_ports_6_10_1z00,
		BgL_bgl_za762fileza7d2modifi2607z00,
		BGl_z62filezd2modificationzd2timez62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_z52openzd2inputzd2pipezd2envz80zz__r4_ports_6_10_1z00,
		BgL_bgl_za762za752openza7d2inp2608za7,
		BGl_z62z52openzd2inputzd2pipez30zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_callzd2withzd2appendzd2filezd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762callza7d2withza7d2609za7,
		BGl_z62callzd2withzd2appendzd2filezb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_outputzd2portzd2flushzd2bufferzd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762outputza7d2port2610z00,
		BGl_z62outputzd2portzd2flushzd2bufferzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_withzd2outputzd2tozd2portzd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762withza7d2output2611z00,
		BGl_z62withzd2outputzd2tozd2portzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_openzd2inputzd2filezd2envzd2zz__r4_ports_6_10_1z00,
		BgL_bgl__openza7d2inputza7d22612z00, opt_generic_entry,
		BGl__openzd2inputzd2filez00zz__r4_ports_6_10_1z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_inputzd2portzd2protocolzd2envzd2zz__r4_ports_6_10_1z00,
		BgL_bgl_za762inputza7d2portza72613za7,
		BGl_z62inputzd2portzd2protocolz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_currentzd2errorzd2portzd2envzd2zz__r4_ports_6_10_1z00,
		BgL_bgl_za762currentza7d2err2614z00,
		BGl_z62currentzd2errorzd2portz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_withzd2errorzd2tozd2filezd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762withza7d2errorza72615za7,
		BGl_z62withzd2errorzd2tozd2filezb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_outputzd2portzd2namezd2envzd2zz__r4_ports_6_10_1z00,
		BgL_bgl_za762outputza7d2port2616z00,
		BGl_z62outputzd2portzd2namez62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_inputzd2portzd2clonez12zd2envzc0zz__r4_ports_6_10_1z00,
		BgL_bgl_za762inputza7d2portza72617za7,
		BGl_z62inputzd2portzd2clonez12z70zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2400z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2400za700za7za7_2618za7, "call-with-input-file", 20);
	      DEFINE_STRING(BGl_string2401z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2401za700za7za7_2619za7, "can't open file", 15);
	      DEFINE_STRING(BGl_string2402z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2402za700za7za7_2620za7, "/tmp/bigloo/runtime/Ieee/port.scm",
		33);
	      DEFINE_STRING(BGl_string2403z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2403za700za7za7_2621za7, "&call-with-input-file", 21);
	      DEFINE_STRING(BGl_string2404z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2404za700za7za7_2622za7, "bstring", 7);
	      DEFINE_STRING(BGl_string2405z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2405za700za7za7_2623za7, "procedure", 9);
	      DEFINE_STRING(BGl_string2406z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2406za700za7za7_2624za7, "&call-with-input-string", 23);
	      DEFINE_STRING(BGl_string2407z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2407za700za7za7_2625za7, "open-output-file", 16);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_filezd2changezd2timezd2envzd2zz__r4_ports_6_10_1z00,
		BgL_bgl_za762fileza7d2change2626z00,
		BGl_z62filezd2changezd2timez62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2408z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2408za700za7za7_2627za7, "call-with-output-file", 21);
	      DEFINE_STRING(BGl_string2409z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2409za700za7za7_2628za7, "&call-with-output-file", 22);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_filezd2modezd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762fileza7d2modeza7b2629za7,
		BGl_z62filezd2modezb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_inputzd2portzd2closezd2hookzd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762inputza7d2portza72630za7,
		BGl_z62inputzd2portzd2closezd2hookzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC,
		1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_portzf3zd2envz21zz__r4_ports_6_10_1z00,
		BgL_bgl_za762portza7f3za791za7za7_2631za7,
		BGl_z62portzf3z91zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2410z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2410za700za7za7_2632za7, "append-output-file", 18);
	      DEFINE_STRING(BGl_string2411z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2411za700za7za7_2633za7, "call-with-append-file", 21);
	      DEFINE_STRING(BGl_string2412z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2412za700za7za7_2634za7, "&call-with-append-file", 22);
	      DEFINE_STRING(BGl_string2413z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2413za700za7za7_2635za7, "&call-with-output-string", 24);
	      DEFINE_STRING(BGl_string2414z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2414za700za7za7_2636za7, "input-port-reopen!", 18);
	      DEFINE_STRING(BGl_string2415z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2415za700za7za7_2637za7, "Cannot reopen port", 18);
	      DEFINE_STRING(BGl_string2416z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2416za700za7za7_2638za7, "&input-port-reopen!", 19);
	      DEFINE_STRING(BGl_string2417z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2417za700za7za7_2639za7, "input-port", 10);
	      DEFINE_STRING(BGl_string2418z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2418za700za7za7_2640za7, "&input-port-clone!", 18);
	      DEFINE_STRING(BGl_string2419z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2419za700za7za7_2641za7, "with-input-from-file", 20);
	extern obj_t BGl_openzd2inputzd2za7libzd2filezd2envza7zz__gunza7ipza7;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_openzd2inputzd2czd2stringzd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762openza7d2inputza72642za7,
		BGl_z62openzd2inputzd2czd2stringzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_withzd2errorzd2tozd2stringzd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762withza7d2errorza72643za7,
		BGl_z62withzd2errorzd2tozd2stringzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2420z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2420za700za7za7_2644za7, "&with-input-from-file", 21);
	      DEFINE_STRING(BGl_string2502z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2502za700za7za7_2645za7, "open-output-procedure", 21);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_resetzd2eofzd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762resetza7d2eofza7b2646za7,
		BGl_z62resetzd2eofzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2421z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2421za700za7za7_2647za7, "&with-input-from-string", 23);
	      DEFINE_STRING(BGl_string2503z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2503za700za7za7_2648za7, "Illegal flush procedure", 23);
	      DEFINE_STRING(BGl_string2422z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2422za700za7za7_2649za7, "&with-input-from-port", 21);
	      DEFINE_STRING(BGl_string2504z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2504za700za7za7_2650za7, "Illegal close procedure", 23);
	      DEFINE_STRING(BGl_string2423z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2423za700za7za7_2651za7, "open-input-procedure", 20);
	      DEFINE_STRING(BGl_string2505z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2505za700za7za7_2652za7, "Illegal write procedure", 23);
	      DEFINE_STRING(BGl_string2424z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2424za700za7za7_2653za7, "&with-input-from-procedure", 26);
	      DEFINE_STRING(BGl_string2506z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2506za700za7za7_2654za7, "&output-port-timeout-set!", 25);
	      DEFINE_STRING(BGl_string2425z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2425za700za7za7_2655za7, "with-output-to-file", 19);
	      DEFINE_STRING(BGl_string2507z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2507za700za7za7_2656za7, "&closed-input-port?", 19);
	      DEFINE_STRING(BGl_string2426z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2426za700za7za7_2657za7, "&with-output-to-file", 20);
	      DEFINE_STRING(BGl_string2508z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2508za700za7za7_2658za7, "&close-input-port", 17);
	      DEFINE_STRING(BGl_string2427z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2427za700za7za7_2659za7, "&with-append-to-file", 20);
	      DEFINE_STRING(BGl_string2509z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2509za700za7za7_2660za7, "&get-output-string", 18);
	      DEFINE_STRING(BGl_string2428z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2428za700za7za7_2661za7, "&with-output-to-port", 20);
	      DEFINE_STRING(BGl_string2429z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2429za700za7za7_2662za7, "output-port", 11);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_z52openzd2inputzd2filezd2envz80zz__r4_ports_6_10_1z00,
		BgL_bgl_za762za752openza7d2inp2663za7,
		BGl_z62z52openzd2inputzd2filez30zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2500z00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762close2052za762za72664za7,
		BGl_z62close2052z62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_outputzd2portzd2positionzd2envzd2zz__r4_ports_6_10_1z00,
		BgL_bgl_za762outputza7d2port2665z00,
		BGl_z62outputzd2portzd2positionz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2501z00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762close2051za762za72666za7,
		BGl_z62close2051z62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2510z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2510za700za7za7_2667za7, "&close-output-port", 18);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_closedzd2outputzd2portzf3zd2envz21zz__r4_ports_6_10_1z00,
		BgL_bgl_za762closedza7d2outp2668z00,
		BGl_z62closedzd2outputzd2portzf3z91zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2511z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2511za700za7za7_2669za7, "&flush-output-port", 18);
	      DEFINE_STRING(BGl_string2430z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2430za700za7za7_2670za7, "&with-output-to-string", 22);
	      DEFINE_STRING(BGl_string2512z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2512za700za7za7_2671za7, "&reset-output-port", 18);
	      DEFINE_STRING(BGl_string2513z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2513za700za7za7_2672za7, "&reset-eof", 10);
	      DEFINE_STRING(BGl_string2514z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2514za700za7za7_2673za7, "&set-input-port-position!", 25);
	      DEFINE_STRING(BGl_string2433z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2433za700za7za7_2674za7, "&with-output-to-procedure", 25);
	      DEFINE_STRING(BGl_string2515z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2515za700za7za7_2675za7, "&input-port-position", 20);
	      DEFINE_STRING(BGl_string2434z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2434za700za7za7_2676za7, "&with-error-to-string", 21);
	      DEFINE_STRING(BGl_string2516z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2516za700za7za7_2677za7, "&input-port-fill-barrier", 24);
	      DEFINE_STRING(BGl_string2517z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2517za700za7za7_2678za7, "&input-port-fill-barrier-set!", 29);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_callzd2withzd2outputzd2filezd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762callza7d2withza7d2679za7,
		BGl_z62callzd2withzd2outputzd2filezb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_inputzd2portzd2timeoutzd2setz12zd2envz12zz__r4_ports_6_10_1z00,
		BgL_bgl_za762inputza7d2portza72680za7,
		BGl_z62inputzd2portzd2timeoutzd2setz12za2zz__r4_ports_6_10_1z00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2436z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2436za700za7za7_2681za7, "with-error-to-file", 18);
	      DEFINE_STRING(BGl_string2518z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2518za700za7za7_2682za7, "&input-port-last-token-position",
		31);
	      DEFINE_STRING(BGl_string2437z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2437za700za7za7_2683za7, "&with-error-to-file", 19);
	      DEFINE_STRING(BGl_string2519z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2519za700za7za7_2684za7, "&output-port-name", 17);
	      DEFINE_STRING(BGl_string2438z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2438za700za7za7_2685za7, "&with-error-to-port", 19);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_outputzd2portzd2timeoutzd2envzd2zz__r4_ports_6_10_1z00,
		BgL_bgl_za762outputza7d2port2686z00,
		BGl_z62outputzd2portzd2timeoutz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2431z00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762flushza762za7za7__r2687z00,
		BGl_z62flushz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2600z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2600za700za7za7_2688za7, "Bad command", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2432z00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762closeza762za7za7__r2689z00,
		BGl_z62closez62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2601z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2601za700za7za7_2690za7, "__r4_ports_6_10_1", 17);
	      DEFINE_STRING(BGl_string2520z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2520za700za7za7_2691za7, "&output-port-name-set!", 22);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_inputzd2portzd2namezd2setz12zd2envz12zz__r4_ports_6_10_1z00,
		BgL_bgl_za762inputza7d2portza72692za7,
		BGl_z62inputzd2portzd2namezd2setz12za2zz__r4_ports_6_10_1z00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2521z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2521za700za7za7_2693za7, "set-output-port-position!", 25);
	      DEFINE_STRING(BGl_string2522z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2522za700za7za7_2694za7, "Cannot seek port", 16);
	      DEFINE_STRING(BGl_string2441z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2441za700za7za7_2695za7, "&with-error-to-procedure", 24);
	      DEFINE_STRING(BGl_string2523z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2523za700za7za7_2696za7, "&set-output-port-position!", 26);
	      DEFINE_STRING(BGl_string2442z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2442za700za7za7_2697za7, "input-port-protocol-set!", 24);
	      DEFINE_STRING(BGl_string2524z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2524za700za7za7_2698za7, "&output-port-position", 21);
	      DEFINE_STRING(BGl_string2443z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2443za700za7za7_2699za7,
		"Illegal open procedure for protocol", 35);
	      DEFINE_STRING(BGl_string2525z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2525za700za7za7_2700za7, "&output-port-isatty?", 20);
	      DEFINE_STRING(BGl_string2444z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2444za700za7za7_2701za7, "Illegal buffer", 14);
	      DEFINE_STRING(BGl_string2526z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2526za700za7za7_2702za7, "&input-port-name", 16);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2439z00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762flush2050za762za72703za7,
		BGl_z62flush2050z62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2445z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2445za700za7za7_2704za7, "&get-port-buffer", 16);
	      DEFINE_STRING(BGl_string2527z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2527za700za7za7_2705za7, "&input-port-name-set!", 21);
	      DEFINE_STRING(BGl_string2446z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2446za700za7za7_2706za7, "bint", 4);
	      DEFINE_STRING(BGl_string2528z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2528za700za7za7_2707za7, "&input-port-length", 18);
	      DEFINE_STRING(BGl_string2447z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2447za700za7za7_2708za7, "open-input-pipe", 15);
	      DEFINE_STRING(BGl_string2529z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2529za700za7za7_2709za7, "&output-port-close-hook", 23);
	      DEFINE_STRING(BGl_string2448z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2448za700za7za7_2710za7, "open-input-file", 15);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_withzd2errorzd2tozd2portzd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762withza7d2errorza72711za7,
		BGl_z62withzd2errorzd2tozd2portzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_withzd2inputzd2fromzd2stringzd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762withza7d2inputza72712za7,
		BGl_z62withzd2inputzd2fromzd2stringzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2440z00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762close2049za762za72713za7,
		BGl_z62close2049z62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2530z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2530za700za7za7_2714za7, "&closed-output-port?", 20);
	      DEFINE_STRING(BGl_string2531z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2531za700za7za7_2715za7, "output-port-close-hook-set!", 27);
	      DEFINE_STRING(BGl_string2450z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2450za700za7za7_2716za7, "http", 4);
	      DEFINE_STRING(BGl_string2532z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2532za700za7za7_2717za7, "&output-port-close-hook-set!", 28);
	      DEFINE_STRING(BGl_string2533z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2533za700za7za7_2718za7, "&output-port-flush-hook", 23);
	      DEFINE_STRING(BGl_string2534z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2534za700za7za7_2719za7, "output-port-flush-hook-set!", 27);
	      DEFINE_STRING(BGl_string2535z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2535za700za7za7_2720za7, "&output-port-flush-hook-set!", 28);
	      DEFINE_STRING(BGl_string2454z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2454za700za7za7_2721za7, "user-agent", 10);
	      DEFINE_STRING(BGl_string2536z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2536za700za7za7_2722za7, "&output-port-flush-buffer", 25);
	      DEFINE_STRING(BGl_string2455z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2455za700za7za7_2723za7, "Mozilla/5.0", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2449z00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762parserza762za7za7__2724z00,
		BGl_z62parserz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 5);
	      DEFINE_STRING(BGl_string2537z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2537za700za7za7_2725za7, "&output-port-flush-buffer-set!",
		30);
	      DEFINE_STRING(BGl_string2538z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2538za700za7za7_2726za7, "&input-port-close-hook", 22);
	      DEFINE_STRING(BGl_string2539z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2539za700za7za7_2727za7, "&input-port-close-hook-set!", 27);
	      DEFINE_STRING(BGl_string2458z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2458za700za7za7_2728za7, "Connection", 10);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_callzd2withzd2outputzd2stringzd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762callza7d2withza7d2729za7,
		BGl_z62callzd2withzd2outputzd2stringzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string2540z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2540za700za7za7_2730za7, "&input-port-seek", 16);
	      DEFINE_STRING(BGl_string2541z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2541za700za7za7_2731za7, "&input-port-seek-set!", 21);
	      DEFINE_STRING(BGl_string2460z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2460za700za7za7_2732za7, "close", 5);
	      DEFINE_STRING(BGl_string2542z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2542za700za7za7_2733za7, "&input-port-buffer", 18);
	      DEFINE_STRING(BGl_string2543z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2543za700za7za7_2734za7, "&input-port-buffer-set!", 23);
	      DEFINE_STRING(BGl_string2462z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2462za700za7za7_2735za7, "get", 3);
	      DEFINE_STRING(BGl_string2544z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2544za700za7za7_2736za7, "&output-port-buffer", 19);
	      DEFINE_STRING(BGl_string2545z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2545za700za7za7_2737za7, "&output-port-buffer-set!", 24);
	      DEFINE_STRING(BGl_string2464z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2464za700za7za7_2738za7, "HTTP/1.1", 8);
	      DEFINE_STRING(BGl_string2546z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2546za700za7za7_2739za7, "&file-exists?", 13);
	      DEFINE_STRING(BGl_string2465z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2465za700za7za7_2740za7, "input-port-close-hook-set!", 26);
	      DEFINE_STRING(BGl_string2466z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2466za700za7za7_2741za7, "Illegal hook", 12);
	      DEFINE_STRING(BGl_string2548z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2548za700za7za7_2742za7, "&delete-file", 12);
	      DEFINE_STRING(BGl_string2467z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2467za700za7za7_2743za7, "input-port-seek-set!", 20);
	      DEFINE_STRING(BGl_string2549z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2549za700za7za7_2744za7, "&make-directory", 15);
	      DEFINE_STRING(BGl_string2468z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2468za700za7za7_2745za7, "Illegal seek procedure", 22);
	      DEFINE_STRING(BGl_string2387z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2387za700za7za7_2746za7, "input-port-protocols", 20);
	      DEFINE_STRING(BGl_string2469z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2469za700za7za7_2747za7, "", 0);
	      DEFINE_STRING(BGl_string2388z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2388za700za7za7_2748za7, "file:", 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_inputzd2portzd2seekzd2envzd2zz__r4_ports_6_10_1z00,
		BgL_bgl_za762inputza7d2portza72749za7,
		BGl_z62inputzd2portzd2seekz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2389z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2389za700za7za7_2750za7, "string:", 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2directoryzd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762makeza7d2direct2751z00,
		BGl_z62makezd2directoryzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_appendzd2outputzd2filezd2envzd2zz__r4_ports_6_10_1z00,
		BgL_bgl__appendza7d2output2752za7, opt_generic_entry,
		BGl__appendzd2outputzd2filez00zz__r4_ports_6_10_1z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_outputzd2portzd2bufferzd2setz12zd2envz12zz__r4_ports_6_10_1z00,
		BgL_bgl_za762outputza7d2port2753z00,
		BGl_z62outputzd2portzd2bufferzd2setz12za2zz__r4_ports_6_10_1z00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2550z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2550za700za7za7_2754za7, "&make-directories", 17);
	      DEFINE_STRING(BGl_string2551z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2551za700za7za7_2755za7, "&delete-directory", 17);
	      DEFINE_STRING(BGl_string2470z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2470za700za7za7_2756za7, "bytes=", 6);
	      DEFINE_STRING(BGl_string2552z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2552za700za7za7_2757za7, "&rename-file", 12);
	      DEFINE_STRING(BGl_string2471z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2471za700za7za7_2758za7, "-", 1);
	      DEFINE_STRING(BGl_string2553z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2553za700za7za7_2759za7, "&truncate-file", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2547z00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762za7c3za704anonymo2760za7,
		BGl_z62zc3z04anonymousza31576ze3ze5zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2391z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2391za700za7za7_2761za7, "| ", 2);
	      DEFINE_STRING(BGl_string2554z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2554za700za7za7_2762za7, "&output-port-truncate", 21);
	      DEFINE_STRING(BGl_string2473z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2473za700za7za7_2763za7, "range", 5);
	      DEFINE_STRING(BGl_string2392z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2392za700za7za7_2764za7, "pipe:", 5);
	      DEFINE_STRING(BGl_string2555z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2555za700za7za7_2765za7, "&copy-file", 10);
	      DEFINE_STRING(BGl_string2474z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2474za700za7za7_2766za7, "_open-input-file", 16);
	      DEFINE_STRING(BGl_string2393z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2393za700za7za7_2767za7, "http://", 7);
	      DEFINE_STRING(BGl_string2556z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2556za700za7za7_2768za7, "&directory?", 11);
	      DEFINE_STRING(BGl_string2475z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2475za700za7za7_2769za7, "_open-input-descriptor", 22);
	      DEFINE_STRING(BGl_string2394z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2394za700za7za7_2770za7, "gzip:", 5);
	      DEFINE_STRING(BGl_string2557z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2557za700za7za7_2771za7, "&directory-length", 17);
	      DEFINE_STRING(BGl_string2476z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2476za700za7za7_2772za7, "_open-input-string", 18);
	      DEFINE_STRING(BGl_string2395z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2395za700za7za7_2773za7, "zlib:", 5);
	      DEFINE_STRING(BGl_string2558z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2558za700za7za7_2774za7, "&directory->list", 16);
	      DEFINE_STRING(BGl_string2477z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2477za700za7za7_2775za7, "open-input-string", 17);
	      DEFINE_STRING(BGl_string2396z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2396za700za7za7_2776za7, "inflate:", 8);
	      DEFINE_STRING(BGl_string2559z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2559za700za7za7_2777za7, "&directory->path-list", 21);
	      DEFINE_STRING(BGl_string2478z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2478za700za7za7_2778za7, "Illegal start offset", 20);
	      DEFINE_STRING(BGl_string2397z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2397za700za7za7_2779za7, "/resource/", 10);
	      DEFINE_STRING(BGl_string2479z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2479za700za7za7_2780za7, "Start offset out of bounds", 26);
	      DEFINE_STRING(BGl_string2398z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2398za700za7za7_2781za7, "ftp://", 6);
	      DEFINE_STRING(BGl_string2399z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2399za700za7za7_2782za7, "fd:", 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_inputzd2mmapzd2portzf3zd2envz21zz__r4_ports_6_10_1z00,
		BgL_bgl_za762inputza7d2mmapza72783za7,
		BGl_z62inputzd2mmapzd2portzf3z91zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_withzd2appendzd2tozd2filezd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762withza7d2append2784z00,
		BGl_z62withzd2appendzd2tozd2filezb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_filezd2gza7ipzf3zd2envz54zz__r4_ports_6_10_1z00,
		BgL_bgl_za762fileza7d2gza7a7ip2785za7,
		BGl_z62filezd2gza7ipzf3ze4zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2390z00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762za7c3za704anonymo2786za7,
		BGl_z62zc3z04anonymousza31413ze3ze5zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2560z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2560za700za7za7_2787za7, "&directory->vector", 18);
	      DEFINE_STRING(BGl_string2480z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2480za700za7za7_2788za7, "Start offset greater than end", 29);
	      DEFINE_STRING(BGl_string2562z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2562za700za7za7_2789za7, "&directory->path-vector", 23);
	      DEFINE_STRING(BGl_string2481z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2481za700za7za7_2790za7, "End offset out of bounds", 24);
	      DEFINE_STRING(BGl_string2563z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2563za700za7za7_2791za7, "&file-modification-time", 23);
	      DEFINE_STRING(BGl_string2482z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2482za700za7za7_2792za7, "_open-input-string!", 19);
	      DEFINE_STRING(BGl_string2564z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2564za700za7za7_2793za7, "&file-change-time", 17);
	      DEFINE_STRING(BGl_string2483z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2483za700za7za7_2794za7, "open-input-string!", 18);
	      DEFINE_STRING(BGl_string2565z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2565za700za7za7_2795za7, "&file-access-time", 17);
	      DEFINE_STRING(BGl_string2484z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2484za700za7za7_2796za7, "_open-input-mmap", 16);
	      DEFINE_STRING(BGl_string2566z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2566za700za7za7_2797za7, "&file-times-set!", 16);
	      DEFINE_STRING(BGl_string2485z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2485za700za7za7_2798za7, "mmap", 4);
	      DEFINE_STRING(BGl_string2567z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2567za700za7za7_2799za7, "belong", 6);
	      DEFINE_STRING(BGl_string2486z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2486za700za7za7_2800za7, "open-input-mmap", 15);
	      DEFINE_STRING(BGl_string2568z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2568za700za7za7_2801za7, "&file-size", 10);
	      DEFINE_STRING(BGl_string2487z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2487za700za7za7_2802za7, "_open-input-procedure", 21);
	      DEFINE_STRING(BGl_string2569z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2569za700za7za7_2803za7, "&file-uid", 9);
	      DEFINE_STRING(BGl_string2488z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2488za700za7za7_2804za7, "_open-input-gzip-port", 21);
	      DEFINE_STRING(BGl_string2489z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2489za700za7za7_2805za7, "open-input-gzip-port", 20);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_inputzd2portzd2closezd2hookzd2setz12zd2envzc0zz__r4_ports_6_10_1z00,
		BgL_bgl_za762inputza7d2portza72806za7,
		BGl_z62inputzd2portzd2closezd2hookzd2setz12z70zz__r4_ports_6_10_1z00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_deletezd2filezd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762deleteza7d2file2807z00,
		BGl_z62deletezd2filezb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_withzd2outputzd2tozd2stringzd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762withza7d2output2808z00,
		BGl_z62withzd2outputzd2tozd2stringzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string2570z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2570za700za7za7_2809za7, "&file-gid", 9);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_filezd2uidzd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762fileza7d2uidza7b02810za7,
		BGl_z62filezd2uidzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2571z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2571za700za7za7_2811za7, "&file-mode", 10);
	      DEFINE_STRING(BGl_string2490z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2490za700za7za7_2812za7, "&open-input-c-string", 20);
	      DEFINE_STRING(BGl_string2572z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2572za700za7za7_2813za7, "&file-type", 10);
	      DEFINE_STRING(BGl_string2491z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2491za700za7za7_2814za7, "&reopen-input-c-string", 22);
	      DEFINE_STRING(BGl_string2573z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2573za700za7za7_2815za7, "&make-symlink", 13);
	      DEFINE_STRING(BGl_string2492z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2492za700za7za7_2816za7, "&input-port-timeout", 19);
	      DEFINE_STRING(BGl_string2493z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2493za700za7za7_2817za7, "&input-port-timeout-set!", 24);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_currentzd2outputzd2portzd2envzd2zz__r4_ports_6_10_1z00,
		BgL_bgl_za762currentza7d2out2818z00,
		BGl_z62currentzd2outputzd2portz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_resetzd2outputzd2portzd2envzd2zz__r4_ports_6_10_1z00,
		BgL_bgl_za762resetza7d2outpu2819z00,
		BGl_z62resetzd2outputzd2portz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2494z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2494za700za7za7_2820za7, "&output-port-timeout", 20);
	      DEFINE_STRING(BGl_string2576z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2576za700za7za7_2821za7, "except", 6);
	      DEFINE_STRING(BGl_string2495z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2495za700za7za7_2822za7, "_open-output-file", 17);
	      DEFINE_STRING(BGl_string2496z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2496za700za7za7_2823za7, "_append-output-file", 19);
	      DEFINE_STRING(BGl_string2578z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2578za700za7za7_2824za7, "read", 4);
	      DEFINE_STRING(BGl_string2499z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2499za700za7za7_2825za7, "_open-output-procedure", 22);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_directoryzd2ze3listzd2envze3zz__r4_ports_6_10_1z00,
		BgL_bgl_za762directoryza7d2za72826za7,
		BGl_z62directoryzd2ze3listz53zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_inputzd2portzf3zd2envzf3zz__r4_ports_6_10_1z00,
		BgL_bgl_za762inputza7d2portza72827za7,
		BGl_z62inputzd2portzf3z43zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_outputzd2portzf3zd2envzf3zz__r4_ports_6_10_1z00,
		BgL_bgl_za762outputza7d2port2828z00,
		BGl_z62outputzd2portzf3z43zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_filezd2typezd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762fileza7d2typeza7b2829za7,
		BGl_z62filezd2typezb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2580z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2580za700za7za7_2830za7, "timeout", 7);
	      DEFINE_STRING(BGl_string2582z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2582za700za7za7_2831za7, "write", 5);
	      DEFINE_STRING(BGl_string2584z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2584za700za7za7_2832za7, "select", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2497z00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762flush2054za762za72833za7,
		BGl_z62flush2054z62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2585z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2585za700za7za7_2834za7, "Illegal keyword argument", 24);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2498z00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762close2053za762za72835za7,
		BGl_z62close2053z62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2586z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2586za700za7za7_2836za7, "_select", 7);
	      DEFINE_STRING(BGl_string2587z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2587za700za7za7_2837za7, "pair-nil", 8);
	      DEFINE_STRING(BGl_string2588z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2588za700za7za7_2838za7,
		"wrong number of arguments: [0..4] expected, provided", 52);
	      DEFINE_STRING(BGl_string2589z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2589za700za7za7_2839za7, "_lockf", 6);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_outputzd2portzd2closezd2hookzd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762outputza7d2port2840z00,
		BGl_z62outputzd2portzd2closezd2hookzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_openzd2inputzd2procedurezd2envzd2zz__r4_ports_6_10_1z00,
		BgL_bgl__openza7d2inputza7d22841z00, opt_generic_entry,
		BGl__openzd2inputzd2procedurez00zz__r4_ports_6_10_1z00, BFALSE, -1);
	      DEFINE_STRING(BGl_string2590z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2590za700za7za7_2842za7, "symbol", 6);
	      DEFINE_STRING(BGl_string2592z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2592za700za7za7_2843za7, "lock", 4);
	      DEFINE_STRING(BGl_string2594z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2594za700za7za7_2844za7, "tlock", 5);
	      DEFINE_STRING(BGl_string2596z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2596za700za7za7_2845za7, "ulock", 5);
	      DEFINE_STRING(BGl_string2598z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2598za700za7za7_2846za7, "test", 4);
	      DEFINE_STRING(BGl_string2599z00zz__r4_ports_6_10_1z00,
		BgL_bgl_string2599za700za7za7_2847za7, "lockf", 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_inputzd2portzd2fillzd2barrierzd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762inputza7d2portza72848za7,
		BGl_z62inputzd2portzd2fillzd2barrierzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC,
		1);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_z52openzd2inputzd2httpzd2socketzd2envz52zz__r4_ports_6_10_1z00,
		BgL_bgl_za762za752openza7d2inp2849za7,
		BGl_z62z52openzd2inputzd2httpzd2socketze2zz__r4_ports_6_10_1z00, 0L,
		BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_openzd2inputzd2descriptorzd2envzd2zz__r4_ports_6_10_1z00,
		BgL_bgl__openza7d2inputza7d22850z00, opt_generic_entry,
		BGl__openzd2inputzd2descriptorz00zz__r4_ports_6_10_1z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_withzd2outputzd2tozd2procedurezd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762withza7d2output2851z00,
		BGl_z62withzd2outputzd2tozd2procedurezb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2outputzd2stringzd2envzd2zz__r4_ports_6_10_1z00,
		BgL_bgl_za762getza7d2outputza72852za7,
		BGl_z62getzd2outputzd2stringz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_outputzd2portzd2namezd2setz12zd2envz12zz__r4_ports_6_10_1z00,
		BgL_bgl_za762outputza7d2port2853z00,
		BGl_z62outputzd2portzd2namezd2setz12za2zz__r4_ports_6_10_1z00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_filezd2existszf3zd2envzf3zz__r4_ports_6_10_1z00,
		BgL_bgl_za762fileza7d2exists2854z00,
		BGl_z62filezd2existszf3z43zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_inputzd2stringzd2portzf3zd2envz21zz__r4_ports_6_10_1z00,
		BgL_bgl_za762inputza7d2strin2855z00,
		BGl_z62inputzd2stringzd2portzf3z91zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_openzd2outputzd2procedurezd2envzd2zz__r4_ports_6_10_1z00,
		BgL_bgl__openza7d2outputza7d2856z00, opt_generic_entry,
		BGl__openzd2outputzd2procedurez00zz__r4_ports_6_10_1z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_inputzd2gza7ipzd2portzf3zd2envz86zz__r4_ports_6_10_1z00,
		BgL_bgl_za762inputza7d2gza7a7i2857za7,
		BGl_z62inputzd2gza7ipzd2portzf3z36zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_copyzd2filezd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762copyza7d2fileza7b2858za7,
		BGl_z62copyzd2filezb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_directoryzd2ze3pathzd2vectorzd2envz31zz__r4_ports_6_10_1z00,
		BgL_bgl_za762directoryza7d2za72859za7,
		BGl_z62directoryzd2ze3pathzd2vectorz81zz__r4_ports_6_10_1z00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_filezd2accesszd2timezd2envzd2zz__r4_ports_6_10_1z00,
		BgL_bgl_za762fileza7d2access2860z00,
		BGl_z62filezd2accesszd2timez62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_filezd2timeszd2setz12zd2envzc0zz__r4_ports_6_10_1z00,
		BgL_bgl_za762fileza7d2timesza72861za7,
		BGl_z62filezd2timeszd2setz12z70zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 3);
	extern obj_t BGl_openzd2inputzd2inflatezd2filezd2envz00zz__gunza7ipza7;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_outputzd2portzd2flushzd2hookzd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762outputza7d2port2862z00,
		BGl_z62outputzd2portzd2flushzd2hookzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_outputzd2portzd2timeoutzd2setz12zd2envz12zz__r4_ports_6_10_1z00,
		BgL_bgl_za762outputza7d2port2863z00,
		BGl_z62outputzd2portzd2timeoutzd2setz12za2zz__r4_ports_6_10_1z00, 0L,
		BUNSPEC, 2);
	extern obj_t BGl_openzd2inputzd2ftpzd2filezd2envz00zz__ftpz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_directoryzd2ze3vectorzd2envze3zz__r4_ports_6_10_1z00,
		BgL_bgl_za762directoryza7d2za72864za7,
		BGl_z62directoryzd2ze3vectorz53zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_inputzd2procedurezd2portzf3zd2envz21zz__r4_ports_6_10_1z00,
		BgL_bgl_za762inputza7d2proce2865z00,
		BGl_z62inputzd2procedurezd2portzf3z91zz__r4_ports_6_10_1z00, 0L, BUNSPEC,
		1);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_z52openzd2inputzd2descriptorzd2envz80zz__r4_ports_6_10_1z00,
		BgL_bgl_za762za752openza7d2inp2866za7,
		BGl_z62z52openzd2inputzd2descriptorz30zz__r4_ports_6_10_1z00, 0L, BUNSPEC,
		3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_directoryzd2lengthzd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762directoryza7d2l2867z00,
		BGl_z62directoryzd2lengthzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_flushzd2outputzd2portzd2envzd2zz__r4_ports_6_10_1z00,
		BgL_bgl_za762flushza7d2outpu2868z00,
		BGl_z62flushzd2outputzd2portz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_withzd2errorzd2tozd2procedurezd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762withza7d2errorza72869za7,
		BGl_z62withzd2errorzd2tozd2procedurezb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_inputzd2portzd2lengthzd2envzd2zz__r4_ports_6_10_1z00,
		BgL_bgl_za762inputza7d2portza72870za7,
		BGl_z62inputzd2portzd2lengthz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_callzd2withzd2inputzd2filezd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762callza7d2withza7d2871za7,
		BGl_z62callzd2withzd2inputzd2filezb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_inputzd2portzd2seekzd2setz12zd2envz12zz__r4_ports_6_10_1z00,
		BgL_bgl_za762inputza7d2portza72872za7,
		BGl_z62inputzd2portzd2seekzd2setz12za2zz__r4_ports_6_10_1z00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_renamezd2filezd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762renameza7d2file2873z00,
		BGl_z62renamezd2filezb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_callzd2withzd2inputzd2stringzd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762callza7d2withza7d2874za7,
		BGl_z62callzd2withzd2inputzd2stringzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2symlinkzd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762makeza7d2symlin2875z00,
		BGl_z62makezd2symlinkzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_outputzd2portzd2flushzd2bufferzd2setz12zd2envzc0zz__r4_ports_6_10_1z00,
		BgL_bgl_za762outputza7d2port2876z00,
		BGl_z62outputzd2portzd2flushzd2bufferzd2setz12z70zz__r4_ports_6_10_1z00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_truncatezd2filezd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762truncateza7d2fi2877z00,
		BGl_z62truncatezd2filezb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_deletezd2directoryzd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762deleteza7d2dire2878z00,
		BGl_z62deletezd2directoryzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_inputzd2portzd2reopenz12zd2envzc0zz__r4_ports_6_10_1z00,
		BgL_bgl_za762inputza7d2portza72879za7,
		BGl_z62inputzd2portzd2reopenz12z70zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_openzd2pipeszd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl__openza7d2pipesza7d22880z00, opt_generic_entry,
		BGl__openzd2pipeszd2zz__r4_ports_6_10_1z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_withzd2inputzd2fromzd2filezd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762withza7d2inputza72881za7,
		BGl_z62withzd2inputzd2fromzd2filezb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_outputzd2portzd2truncatezd2envzd2zz__r4_ports_6_10_1z00,
		BgL_bgl_za762outputza7d2port2882z00,
		BGl_z62outputzd2portzd2truncatez62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_directoryzd2ze3pathzd2listzd2envz31zz__r4_ports_6_10_1z00,
		BgL_bgl_za762directoryza7d2za72883za7,
		BGl_z62directoryzd2ze3pathzd2listz81zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2portzd2bufferzd2envzd2zz__r4_ports_6_10_1z00,
		BgL_bgl_za762getza7d2portza7d22884za7,
		BGl_z62getzd2portzd2bufferz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_filezd2gidzd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762fileza7d2gidza7b02885za7,
		BGl_z62filezd2gidzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_outputzd2stringzd2portzf3zd2envz21zz__r4_ports_6_10_1z00,
		BgL_bgl_za762outputza7d2stri2886z00,
		BGl_z62outputzd2stringzd2portzf3z91zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_filezd2siza7ezd2envza7zz__r4_ports_6_10_1z00,
		BgL_bgl_za762fileza7d2siza7a7e2887za7,
		BGl_z62filezd2siza7ez17zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_outputzd2portzd2isattyzf3zd2envz21zz__r4_ports_6_10_1z00,
		BgL_bgl_za762outputza7d2port2888z00,
		BGl_z62outputzd2portzd2isattyzf3z91zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_openzd2inputzd2stringz12zd2envzc0zz__r4_ports_6_10_1z00,
		BgL_bgl__openza7d2inputza7d22889z00, opt_generic_entry,
		BGl__openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reopenzd2inputzd2czd2stringzd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762reopenza7d2inpu2890z00,
		BGl_z62reopenzd2inputzd2czd2stringzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC,
		2);
	extern obj_t BGl_openzd2inputzd2gza7ipzd2filezd2envza7zz__gunza7ipza7;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_closezd2inputzd2portzd2envzd2zz__r4_ports_6_10_1z00,
		BgL_bgl_za762closeza7d2input2891z00,
		BGl_z62closezd2inputzd2portz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_z52openzd2inputzd2resourcezd2envz80zz__r4_ports_6_10_1z00,
		BgL_bgl_za762za752openza7d2inp2892za7,
		BGl_z62z52openzd2inputzd2resourcez30zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_inputzd2portzd2bufferzd2envzd2zz__r4_ports_6_10_1z00,
		BgL_bgl_za762inputza7d2portza72893za7,
		BGl_z62inputzd2portzd2bufferz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_lockfzd2envzd2zz__r4_ports_6_10_1z00,
		BgL_bgl__lockfza700za7za7__r4_2894za7, opt_generic_entry,
		BGl__lockfz00zz__r4_ports_6_10_1z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_directoryzf3zd2envz21zz__r4_ports_6_10_1z00,
		BgL_bgl_za762directoryza7f3za72895za7,
		BGl_z62directoryzf3z91zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_inputzd2portzd2fillzd2barrierzd2setz12zd2envzc0zz__r4_ports_6_10_1z00,
		BgL_bgl_za762inputza7d2portza72896za7,
		BGl_z62inputzd2portzd2fillzd2barrierzd2setz12z70zz__r4_ports_6_10_1z00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setzd2inputzd2portzd2positionz12zd2envz12zz__r4_ports_6_10_1z00,
		BgL_bgl_za762setza7d2inputza7d2897za7,
		BGl_z62setzd2inputzd2portzd2positionz12za2zz__r4_ports_6_10_1z00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_outputzd2portzd2closezd2hookzd2setz12zd2envzc0zz__r4_ports_6_10_1z00,
		BgL_bgl_za762outputza7d2port2898z00,
		BGl_z62outputzd2portzd2closezd2hookzd2setz12z70zz__r4_ports_6_10_1z00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_closezd2outputzd2portzd2envzd2zz__r4_ports_6_10_1z00,
		BgL_bgl_za762closeza7d2outpu2899z00,
		BGl_z62closezd2outputzd2portz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_outputzd2portzd2flushzd2hookzd2setz12zd2envzc0zz__r4_ports_6_10_1z00,
		BgL_bgl_za762outputza7d2port2900z00,
		BGl_z62outputzd2portzd2flushzd2hookzd2setz12z70zz__r4_ports_6_10_1z00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_withzd2inputzd2fromzd2procedurezd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762withza7d2inputza72901za7,
		BGl_z62withzd2inputzd2fromzd2procedurezb0zz__r4_ports_6_10_1z00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_openzd2inputzd2stringzd2envzd2zz__r4_ports_6_10_1z00,
		BgL_bgl__openza7d2inputza7d22902z00, opt_generic_entry,
		BGl__openzd2inputzd2stringz00zz__r4_ports_6_10_1z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_inputzd2portzd2lastzd2tokenzd2positionzd2envzd2zz__r4_ports_6_10_1z00,
		BgL_bgl_za762inputza7d2portza72903za7,
		BGl_z62inputzd2portzd2lastzd2tokenzd2positionz62zz__r4_ports_6_10_1z00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_openzd2outputzd2stringzd2envzd2zz__r4_ports_6_10_1z00,
		BgL_bgl__openza7d2outputza7d2904z00, opt_generic_entry,
		BGl__openzd2outputzd2stringz00zz__r4_ports_6_10_1z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_withzd2outputzd2tozd2filezd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762withza7d2output2905z00,
		BGl_z62withzd2outputzd2tozd2filezb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_selectzd2envzd2zz__r4_ports_6_10_1z00,
		BgL_bgl__selectza700za7za7__r42906za7, opt_generic_entry,
		BGl__selectz00zz__r4_ports_6_10_1z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_withzd2inputzd2fromzd2portzd2envz00zz__r4_ports_6_10_1z00,
		BgL_bgl_za762withza7d2inputza72907za7,
		BGl_z62withzd2inputzd2fromzd2portzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_outputzd2procedurezd2portzf3zd2envz21zz__r4_ports_6_10_1z00,
		BgL_bgl_za762outputza7d2proc2908z00,
		BGl_z62outputzd2procedurezd2portzf3z91zz__r4_ports_6_10_1z00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setzd2outputzd2portzd2positionz12zd2envz12zz__r4_ports_6_10_1z00,
		BgL_bgl_za762setza7d2outputza72909za7,
		BGl_z62setzd2outputzd2portzd2positionz12za2zz__r4_ports_6_10_1z00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_currentzd2inputzd2portzd2envzd2zz__r4_ports_6_10_1z00,
		BgL_bgl_za762currentza7d2inp2910z00,
		BGl_z62currentzd2inputzd2portz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_inputzd2portzd2positionzd2envzd2zz__r4_ports_6_10_1z00,
		BgL_bgl_za762inputza7d2portza72911za7,
		BGl_z62inputzd2portzd2positionz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_openzd2inputzd2gza7ipzd2portzd2envza7zz__r4_ports_6_10_1z00,
		BgL_bgl__openza7d2inputza7d22912z00, opt_generic_entry,
		BGl__openzd2inputzd2gza7ipzd2portz75zz__r4_ports_6_10_1z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_inputzd2portzd2namezd2envzd2zz__r4_ports_6_10_1z00,
		BgL_bgl_za762inputza7d2portza72913za7,
		BGl_z62inputzd2portzd2namez62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_inputzd2portzd2protocolzd2setz12zd2envz12zz__r4_ports_6_10_1z00,
		BgL_bgl_za762inputza7d2portza72914za7,
		BGl_z62inputzd2portzd2protocolzd2setz12za2zz__r4_ports_6_10_1z00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_openzd2outputzd2filezd2envzd2zz__r4_ports_6_10_1z00,
		BgL_bgl__openza7d2outputza7d2915z00, opt_generic_entry,
		BGl__openzd2outputzd2filez00zz__r4_ports_6_10_1z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_inputzd2portzd2bufferzd2setz12zd2envz12zz__r4_ports_6_10_1z00,
		BgL_bgl_za762inputza7d2portza72916za7,
		BGl_z62inputzd2portzd2bufferzd2setz12za2zz__r4_ports_6_10_1z00, 0L, BUNSPEC,
		2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*)
			(&BGl_za2inputzd2portzd2protocolszd2mutexza2zd2zz__r4_ports_6_10_1z00));
		     ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__r4_ports_6_10_1z00));
		     ADD_ROOT((void *) (&BGl_symbol2583z00zz__r4_ports_6_10_1z00));
		     ADD_ROOT((void *) (&BGl_symbol2591z00zz__r4_ports_6_10_1z00));
		     ADD_ROOT((void *) (&BGl_symbol2593z00zz__r4_ports_6_10_1z00));
		     ADD_ROOT((void *) (&BGl_symbol2595z00zz__r4_ports_6_10_1z00));
		     ADD_ROOT((void *) (&BGl_symbol2597z00zz__r4_ports_6_10_1z00));
		     ADD_ROOT((void *) (&BGl_list2451z00zz__r4_ports_6_10_1z00));
		     ADD_ROOT((void *) (&BGl_list2452z00zz__r4_ports_6_10_1z00));
		     ADD_ROOT((void *) (&BGl_list2456z00zz__r4_ports_6_10_1z00));
		     ADD_ROOT((void *) (&BGl_list2574z00zz__r4_ports_6_10_1z00));
		     ADD_ROOT((void *) (&BGl_keyword2453z00zz__r4_ports_6_10_1z00));
		     ADD_ROOT((void *) (&BGl_keyword2457z00zz__r4_ports_6_10_1z00));
		     ADD_ROOT((void *) (&BGl_vector2561z00zz__r4_ports_6_10_1z00));
		     ADD_ROOT((void *) (&BGl_keyword2472z00zz__r4_ports_6_10_1z00));
		     ADD_ROOT((void *) (&BGl_symbol2435z00zz__r4_ports_6_10_1z00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2inputzd2portzd2protocolsza2z00zz__r4_ports_6_10_1z00));
		     ADD_ROOT((void *) (&BGl_keyword2575z00zz__r4_ports_6_10_1z00));
		     ADD_ROOT((void *) (&BGl_keyword2577z00zz__r4_ports_6_10_1z00));
		     ADD_ROOT((void *) (&BGl_keyword2579z00zz__r4_ports_6_10_1z00));
		     ADD_ROOT((void *) (&BGl_symbol2459z00zz__r4_ports_6_10_1z00));
		     ADD_ROOT((void *) (&BGl_keyword2581z00zz__r4_ports_6_10_1z00));
		     ADD_ROOT((void *) (&BGl_symbol2461z00zz__r4_ports_6_10_1z00));
		     ADD_ROOT((void *) (&BGl_symbol2463z00zz__r4_ports_6_10_1z00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long
		BgL_checksumz00_4225, char *BgL_fromz00_4226)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__r4_ports_6_10_1z00))
				{
					BGl_requirezd2initializa7ationz75zz__r4_ports_6_10_1z00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__r4_ports_6_10_1z00();
					BGl_cnstzd2initzd2zz__r4_ports_6_10_1z00();
					BGl_importedzd2moduleszd2initz00zz__r4_ports_6_10_1z00();
					return BGl_toplevelzd2initzd2zz__r4_ports_6_10_1z00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__r4_ports_6_10_1z00(void)
	{
		{	/* Ieee/port.scm 18 */
			BGl_symbol2435z00zz__r4_ports_6_10_1z00 =
				bstring_to_symbol(BGl_string2436z00zz__r4_ports_6_10_1z00);
			BGl_keyword2453z00zz__r4_ports_6_10_1z00 =
				bstring_to_keyword(BGl_string2454z00zz__r4_ports_6_10_1z00);
			BGl_list2452z00zz__r4_ports_6_10_1z00 =
				MAKE_YOUNG_PAIR(BGl_keyword2453z00zz__r4_ports_6_10_1z00,
				MAKE_YOUNG_PAIR(BGl_string2455z00zz__r4_ports_6_10_1z00, BNIL));
			BGl_keyword2457z00zz__r4_ports_6_10_1z00 =
				bstring_to_keyword(BGl_string2458z00zz__r4_ports_6_10_1z00);
			BGl_symbol2459z00zz__r4_ports_6_10_1z00 =
				bstring_to_symbol(BGl_string2460z00zz__r4_ports_6_10_1z00);
			BGl_list2456z00zz__r4_ports_6_10_1z00 =
				MAKE_YOUNG_PAIR(BGl_keyword2457z00zz__r4_ports_6_10_1z00,
				MAKE_YOUNG_PAIR(BGl_symbol2459z00zz__r4_ports_6_10_1z00, BNIL));
			BGl_list2451z00zz__r4_ports_6_10_1z00 =
				MAKE_YOUNG_PAIR(BGl_list2452z00zz__r4_ports_6_10_1z00,
				MAKE_YOUNG_PAIR(BGl_list2456z00zz__r4_ports_6_10_1z00, BNIL));
			BGl_symbol2461z00zz__r4_ports_6_10_1z00 =
				bstring_to_symbol(BGl_string2462z00zz__r4_ports_6_10_1z00);
			BGl_symbol2463z00zz__r4_ports_6_10_1z00 =
				bstring_to_symbol(BGl_string2450z00zz__r4_ports_6_10_1z00);
			BGl_keyword2472z00zz__r4_ports_6_10_1z00 =
				bstring_to_keyword(BGl_string2473z00zz__r4_ports_6_10_1z00);
			BGl_vector2561z00zz__r4_ports_6_10_1z00 =
				BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(BNIL);
			BGl_keyword2575z00zz__r4_ports_6_10_1z00 =
				bstring_to_keyword(BGl_string2576z00zz__r4_ports_6_10_1z00);
			BGl_keyword2577z00zz__r4_ports_6_10_1z00 =
				bstring_to_keyword(BGl_string2578z00zz__r4_ports_6_10_1z00);
			BGl_keyword2579z00zz__r4_ports_6_10_1z00 =
				bstring_to_keyword(BGl_string2580z00zz__r4_ports_6_10_1z00);
			BGl_keyword2581z00zz__r4_ports_6_10_1z00 =
				bstring_to_keyword(BGl_string2582z00zz__r4_ports_6_10_1z00);
			BGl_list2574z00zz__r4_ports_6_10_1z00 =
				MAKE_YOUNG_PAIR(BGl_keyword2575z00zz__r4_ports_6_10_1z00,
				MAKE_YOUNG_PAIR(BGl_keyword2577z00zz__r4_ports_6_10_1z00,
					MAKE_YOUNG_PAIR(BGl_keyword2579z00zz__r4_ports_6_10_1z00,
						MAKE_YOUNG_PAIR(BGl_keyword2581z00zz__r4_ports_6_10_1z00, BNIL))));
			BGl_symbol2583z00zz__r4_ports_6_10_1z00 =
				bstring_to_symbol(BGl_string2584z00zz__r4_ports_6_10_1z00);
			BGl_symbol2591z00zz__r4_ports_6_10_1z00 =
				bstring_to_symbol(BGl_string2592z00zz__r4_ports_6_10_1z00);
			BGl_symbol2593z00zz__r4_ports_6_10_1z00 =
				bstring_to_symbol(BGl_string2594z00zz__r4_ports_6_10_1z00);
			BGl_symbol2595z00zz__r4_ports_6_10_1z00 =
				bstring_to_symbol(BGl_string2596z00zz__r4_ports_6_10_1z00);
			return (BGl_symbol2597z00zz__r4_ports_6_10_1z00 =
				bstring_to_symbol(BGl_string2598z00zz__r4_ports_6_10_1z00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__r4_ports_6_10_1z00(void)
	{
		{	/* Ieee/port.scm 18 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__r4_ports_6_10_1z00(void)
	{
		{	/* Ieee/port.scm 18 */
			BGl_za2inputzd2portzd2protocolszd2mutexza2zd2zz__r4_ports_6_10_1z00 =
				bgl_make_mutex(BGl_string2387z00zz__r4_ports_6_10_1z00);
			{	/* Ieee/port.scm 924 */
				obj_t BgL_arg1407z00_1402;
				obj_t BgL_arg1408z00_1403;

				BgL_arg1407z00_1402 =
					MAKE_YOUNG_PAIR(BGl_string2388z00zz__r4_ports_6_10_1z00,
					BGl_z52openzd2inputzd2filezd2envz80zz__r4_ports_6_10_1z00);
				{	/* Ieee/port.scm 925 */
					obj_t BgL_arg1410z00_1404;
					obj_t BgL_arg1411z00_1405;

					BgL_arg1410z00_1404 =
						MAKE_YOUNG_PAIR(BGl_string2389z00zz__r4_ports_6_10_1z00,
						BGl_proc2390z00zz__r4_ports_6_10_1z00);
					{	/* Ieee/port.scm 926 */
						obj_t BgL_arg1414z00_1415;
						obj_t BgL_arg1415z00_1416;

						BgL_arg1414z00_1415 =
							MAKE_YOUNG_PAIR(BGl_string2391z00zz__r4_ports_6_10_1z00,
							BGl_z52openzd2inputzd2pipezd2envz80zz__r4_ports_6_10_1z00);
						{	/* Ieee/port.scm 927 */
							obj_t BgL_arg1416z00_1417;
							obj_t BgL_arg1417z00_1418;

							BgL_arg1416z00_1417 =
								MAKE_YOUNG_PAIR(BGl_string2392z00zz__r4_ports_6_10_1z00,
								BGl_z52openzd2inputzd2pipezd2envz80zz__r4_ports_6_10_1z00);
							{	/* Ieee/port.scm 928 */
								obj_t BgL_arg1418z00_1419;
								obj_t BgL_arg1419z00_1420;

								BgL_arg1418z00_1419 =
									MAKE_YOUNG_PAIR(BGl_string2393z00zz__r4_ports_6_10_1z00,
									BGl_z52openzd2inputzd2httpzd2socketzd2envz52zz__r4_ports_6_10_1z00);
								{	/* Ieee/port.scm 929 */
									obj_t BgL_arg1420z00_1421;
									obj_t BgL_arg1421z00_1422;

									BgL_arg1420z00_1421 =
										MAKE_YOUNG_PAIR(BGl_string2394z00zz__r4_ports_6_10_1z00,
										BGl_openzd2inputzd2gza7ipzd2filezd2envza7zz__gunza7ipza7);
									{	/* Ieee/port.scm 930 */
										obj_t BgL_arg1422z00_1423;
										obj_t BgL_arg1423z00_1424;

										BgL_arg1422z00_1423 =
											MAKE_YOUNG_PAIR(BGl_string2395z00zz__r4_ports_6_10_1z00,
											BGl_openzd2inputzd2za7libzd2filezd2envza7zz__gunza7ipza7);
										{	/* Ieee/port.scm 931 */
											obj_t BgL_arg1424z00_1425;
											obj_t BgL_arg1425z00_1426;

											BgL_arg1424z00_1425 =
												MAKE_YOUNG_PAIR(BGl_string2396z00zz__r4_ports_6_10_1z00,
												BGl_openzd2inputzd2inflatezd2filezd2envz00zz__gunza7ipza7);
											{	/* Ieee/port.scm 932 */
												obj_t BgL_arg1426z00_1427;
												obj_t BgL_arg1427z00_1428;

												BgL_arg1426z00_1427 =
													MAKE_YOUNG_PAIR
													(BGl_string2397z00zz__r4_ports_6_10_1z00,
													BGl_z52openzd2inputzd2resourcezd2envz80zz__r4_ports_6_10_1z00);
												{	/* Ieee/port.scm 933 */
													obj_t BgL_arg1428z00_1429;
													obj_t BgL_arg1429z00_1430;

													BgL_arg1428z00_1429 =
														MAKE_YOUNG_PAIR
														(BGl_string2398z00zz__r4_ports_6_10_1z00,
														BGl_openzd2inputzd2ftpzd2filezd2envz00zz__ftpz00);
													{	/* Ieee/port.scm 934 */
														obj_t BgL_arg1430z00_1431;

														BgL_arg1430z00_1431 =
															MAKE_YOUNG_PAIR
															(BGl_string2399z00zz__r4_ports_6_10_1z00,
															BGl_z52openzd2inputzd2descriptorzd2envz80zz__r4_ports_6_10_1z00);
														BgL_arg1429z00_1430 =
															MAKE_YOUNG_PAIR(BgL_arg1430z00_1431, BNIL);
													}
													BgL_arg1427z00_1428 =
														MAKE_YOUNG_PAIR(BgL_arg1428z00_1429,
														BgL_arg1429z00_1430);
												}
												BgL_arg1425z00_1426 =
													MAKE_YOUNG_PAIR(BgL_arg1426z00_1427,
													BgL_arg1427z00_1428);
											}
											BgL_arg1423z00_1424 =
												MAKE_YOUNG_PAIR(BgL_arg1424z00_1425,
												BgL_arg1425z00_1426);
										}
										BgL_arg1421z00_1422 =
											MAKE_YOUNG_PAIR(BgL_arg1422z00_1423, BgL_arg1423z00_1424);
									}
									BgL_arg1419z00_1420 =
										MAKE_YOUNG_PAIR(BgL_arg1420z00_1421, BgL_arg1421z00_1422);
								}
								BgL_arg1417z00_1418 =
									MAKE_YOUNG_PAIR(BgL_arg1418z00_1419, BgL_arg1419z00_1420);
							}
							BgL_arg1415z00_1416 =
								MAKE_YOUNG_PAIR(BgL_arg1416z00_1417, BgL_arg1417z00_1418);
						}
						BgL_arg1411z00_1405 =
							MAKE_YOUNG_PAIR(BgL_arg1414z00_1415, BgL_arg1415z00_1416);
					}
					BgL_arg1408z00_1403 =
						MAKE_YOUNG_PAIR(BgL_arg1410z00_1404, BgL_arg1411z00_1405);
				}
				return (BGl_za2inputzd2portzd2protocolsza2z00zz__r4_ports_6_10_1z00 =
					MAKE_YOUNG_PAIR(BgL_arg1407z00_1402, BgL_arg1408z00_1403), BUNSPEC);
			}
		}

	}



/* &<@anonymous:1413> */
	obj_t BGl_z62zc3z04anonymousza31413ze3ze5zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3381, obj_t BgL_sz00_3382, obj_t BgL_pz00_3383,
		obj_t BgL_tmtz00_3384)
	{
		{	/* Ieee/port.scm 925 */
			{	/* Ieee/port.scm 469 */
				long BgL_endz00_4162;

				BgL_endz00_4162 = STRING_LENGTH(((obj_t) BgL_sz00_3382));
				{	/* Ieee/port.scm 469 */

					return
						BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00(BgL_sz00_3382,
						BINT(0L), BINT(BgL_endz00_4162));
				}
			}
		}

	}



/* call-with-input-file */
	BGL_EXPORTED_DEF obj_t
		BGl_callzd2withzd2inputzd2filezd2zz__r4_ports_6_10_1z00(obj_t
		BgL_stringz00_3, obj_t BgL_procz00_4)
	{
		{	/* Ieee/port.scm 569 */
			{	/* Ieee/port.scm 570 */
				obj_t BgL_portz00_1440;

				{	/* Ieee/port.scm 466 */

					BgL_portz00_1440 =
						BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(BgL_stringz00_3,
						BTRUE, BINT(5000000L));
				}
				if (INPUT_PORTP(BgL_portz00_1440))
					{	/* Ieee/port.scm 572 */
						obj_t BgL_exitd1047z00_1442;

						BgL_exitd1047z00_1442 = BGL_EXITD_TOP_AS_OBJ();
						{	/* Ieee/port.scm 574 */
							obj_t BgL_zc3z04anonymousza31437ze3z87_3421;

							BgL_zc3z04anonymousza31437ze3z87_3421 =
								MAKE_FX_PROCEDURE
								(BGl_z62zc3z04anonymousza31437ze3ze5zz__r4_ports_6_10_1z00,
								(int) (0L), (int) (1L));
							PROCEDURE_SET(BgL_zc3z04anonymousza31437ze3z87_3421, (int) (0L),
								BgL_portz00_1440);
							{	/* Ieee/port.scm 572 */
								obj_t BgL_arg2014z00_2509;

								{	/* Ieee/port.scm 572 */
									obj_t BgL_arg2015z00_2510;

									BgL_arg2015z00_2510 =
										BGL_EXITD_PROTECT(BgL_exitd1047z00_1442);
									BgL_arg2014z00_2509 =
										MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31437ze3z87_3421,
										BgL_arg2015z00_2510);
								}
								BGL_EXITD_PROTECT_SET(BgL_exitd1047z00_1442,
									BgL_arg2014z00_2509);
								BUNSPEC;
							}
							{	/* Ieee/port.scm 573 */
								obj_t BgL_tmp1049z00_1444;

								BgL_tmp1049z00_1444 =
									BGL_PROCEDURE_CALL1(BgL_procz00_4, BgL_portz00_1440);
								{	/* Ieee/port.scm 572 */
									bool_t BgL_test2919z00_4307;

									{	/* Ieee/port.scm 572 */
										obj_t BgL_arg2013z00_2512;

										BgL_arg2013z00_2512 =
											BGL_EXITD_PROTECT(BgL_exitd1047z00_1442);
										BgL_test2919z00_4307 = PAIRP(BgL_arg2013z00_2512);
									}
									if (BgL_test2919z00_4307)
										{	/* Ieee/port.scm 572 */
											obj_t BgL_arg2011z00_2513;

											{	/* Ieee/port.scm 572 */
												obj_t BgL_arg2012z00_2514;

												BgL_arg2012z00_2514 =
													BGL_EXITD_PROTECT(BgL_exitd1047z00_1442);
												BgL_arg2011z00_2513 =
													CDR(((obj_t) BgL_arg2012z00_2514));
											}
											BGL_EXITD_PROTECT_SET(BgL_exitd1047z00_1442,
												BgL_arg2011z00_2513);
											BUNSPEC;
										}
									else
										{	/* Ieee/port.scm 572 */
											BFALSE;
										}
								}
								bgl_close_input_port(BgL_portz00_1440);
								return BgL_tmp1049z00_1444;
							}
						}
					}
				else
					{	/* Ieee/port.scm 571 */
						return
							bgl_system_failure(BGL_IO_PORT_ERROR,
							BGl_string2400z00zz__r4_ports_6_10_1z00,
							BGl_string2401z00zz__r4_ports_6_10_1z00, BgL_stringz00_3);
					}
			}
		}

	}



/* &call-with-input-file */
	obj_t BGl_z62callzd2withzd2inputzd2filezb0zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3422, obj_t BgL_stringz00_3423, obj_t BgL_procz00_3424)
	{
		{	/* Ieee/port.scm 569 */
			{	/* Ieee/port.scm 570 */
				obj_t BgL_auxz00_4323;
				obj_t BgL_auxz00_4316;

				if (PROCEDUREP(BgL_procz00_3424))
					{	/* Ieee/port.scm 570 */
						BgL_auxz00_4323 = BgL_procz00_3424;
					}
				else
					{
						obj_t BgL_auxz00_4326;

						BgL_auxz00_4326 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(25846L),
							BGl_string2403z00zz__r4_ports_6_10_1z00,
							BGl_string2405z00zz__r4_ports_6_10_1z00, BgL_procz00_3424);
						FAILURE(BgL_auxz00_4326, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_stringz00_3423))
					{	/* Ieee/port.scm 570 */
						BgL_auxz00_4316 = BgL_stringz00_3423;
					}
				else
					{
						obj_t BgL_auxz00_4319;

						BgL_auxz00_4319 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(25846L),
							BGl_string2403z00zz__r4_ports_6_10_1z00,
							BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_stringz00_3423);
						FAILURE(BgL_auxz00_4319, BFALSE, BFALSE);
					}
				return
					BGl_callzd2withzd2inputzd2filezd2zz__r4_ports_6_10_1z00
					(BgL_auxz00_4316, BgL_auxz00_4323);
			}
		}

	}



/* &<@anonymous:1437> */
	obj_t BGl_z62zc3z04anonymousza31437ze3ze5zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3425)
	{
		{	/* Ieee/port.scm 572 */
			{	/* Ieee/port.scm 574 */
				obj_t BgL_portz00_3426;

				BgL_portz00_3426 = PROCEDURE_REF(BgL_envz00_3425, (int) (0L));
				return bgl_close_input_port(((obj_t) BgL_portz00_3426));
			}
		}

	}



/* call-with-input-string */
	BGL_EXPORTED_DEF obj_t
		BGl_callzd2withzd2inputzd2stringzd2zz__r4_ports_6_10_1z00(obj_t
		BgL_stringz00_5, obj_t BgL_procz00_6)
	{
		{	/* Ieee/port.scm 581 */
			{	/* Ieee/port.scm 582 */
				obj_t BgL_portz00_2517;

				{	/* Ieee/port.scm 469 */
					long BgL_endz00_2520;

					BgL_endz00_2520 = STRING_LENGTH(BgL_stringz00_5);
					{	/* Ieee/port.scm 469 */

						BgL_portz00_2517 =
							BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00
							(BgL_stringz00_5, BINT(0L), BINT(BgL_endz00_2520));
				}}
				{	/* Ieee/port.scm 584 */
					obj_t BgL_resz00_2521;

					BgL_resz00_2521 =
						BGL_PROCEDURE_CALL1(BgL_procz00_6, BgL_portz00_2517);
					bgl_close_input_port(BgL_portz00_2517);
					return BgL_resz00_2521;
				}
			}
		}

	}



/* &call-with-input-string */
	obj_t BGl_z62callzd2withzd2inputzd2stringzb0zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3427, obj_t BgL_stringz00_3428, obj_t BgL_procz00_3429)
	{
		{	/* Ieee/port.scm 581 */
			{	/* Ieee/port.scm 582 */
				obj_t BgL_auxz00_4351;
				obj_t BgL_auxz00_4344;

				if (PROCEDUREP(BgL_procz00_3429))
					{	/* Ieee/port.scm 582 */
						BgL_auxz00_4351 = BgL_procz00_3429;
					}
				else
					{
						obj_t BgL_auxz00_4354;

						BgL_auxz00_4354 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(26348L),
							BGl_string2406z00zz__r4_ports_6_10_1z00,
							BGl_string2405z00zz__r4_ports_6_10_1z00, BgL_procz00_3429);
						FAILURE(BgL_auxz00_4354, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_stringz00_3428))
					{	/* Ieee/port.scm 582 */
						BgL_auxz00_4344 = BgL_stringz00_3428;
					}
				else
					{
						obj_t BgL_auxz00_4347;

						BgL_auxz00_4347 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(26348L),
							BGl_string2406z00zz__r4_ports_6_10_1z00,
							BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_stringz00_3428);
						FAILURE(BgL_auxz00_4347, BFALSE, BFALSE);
					}
				return
					BGl_callzd2withzd2inputzd2stringzd2zz__r4_ports_6_10_1z00
					(BgL_auxz00_4344, BgL_auxz00_4351);
			}
		}

	}



/* call-with-output-file */
	BGL_EXPORTED_DEF obj_t
		BGl_callzd2withzd2outputzd2filezd2zz__r4_ports_6_10_1z00(obj_t
		BgL_stringz00_7, obj_t BgL_procz00_8)
	{
		{	/* Ieee/port.scm 591 */
			{	/* Ieee/port.scm 592 */
				obj_t BgL_portz00_1455;

				{	/* Ieee/port.scm 483 */

					BgL_portz00_1455 =
						bgl_open_output_file(BgL_stringz00_7,
						BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00
						(BGl_string2407z00zz__r4_ports_6_10_1z00, BTRUE,
							(int) (default_io_bufsiz)));
				}
				if (OUTPUT_PORTP(BgL_portz00_1455))
					{	/* Ieee/port.scm 594 */
						obj_t BgL_exitd1051z00_1457;

						BgL_exitd1051z00_1457 = BGL_EXITD_TOP_AS_OBJ();
						{	/* Ieee/port.scm 596 */
							obj_t BgL_zc3z04anonymousza31439ze3z87_3430;

							BgL_zc3z04anonymousza31439ze3z87_3430 =
								MAKE_FX_PROCEDURE
								(BGl_z62zc3z04anonymousza31439ze3ze5zz__r4_ports_6_10_1z00,
								(int) (0L), (int) (1L));
							PROCEDURE_SET(BgL_zc3z04anonymousza31439ze3z87_3430, (int) (0L),
								BgL_portz00_1455);
							{	/* Ieee/port.scm 594 */
								obj_t BgL_arg2014z00_2527;

								{	/* Ieee/port.scm 594 */
									obj_t BgL_arg2015z00_2528;

									BgL_arg2015z00_2528 =
										BGL_EXITD_PROTECT(BgL_exitd1051z00_1457);
									BgL_arg2014z00_2527 =
										MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31439ze3z87_3430,
										BgL_arg2015z00_2528);
								}
								BGL_EXITD_PROTECT_SET(BgL_exitd1051z00_1457,
									BgL_arg2014z00_2527);
								BUNSPEC;
							}
							{	/* Ieee/port.scm 595 */
								obj_t BgL_tmp1053z00_1459;

								BgL_tmp1053z00_1459 =
									BGL_PROCEDURE_CALL1(BgL_procz00_8, BgL_portz00_1455);
								{	/* Ieee/port.scm 594 */
									bool_t BgL_test2925z00_4377;

									{	/* Ieee/port.scm 594 */
										obj_t BgL_arg2013z00_2530;

										BgL_arg2013z00_2530 =
											BGL_EXITD_PROTECT(BgL_exitd1051z00_1457);
										BgL_test2925z00_4377 = PAIRP(BgL_arg2013z00_2530);
									}
									if (BgL_test2925z00_4377)
										{	/* Ieee/port.scm 594 */
											obj_t BgL_arg2011z00_2531;

											{	/* Ieee/port.scm 594 */
												obj_t BgL_arg2012z00_2532;

												BgL_arg2012z00_2532 =
													BGL_EXITD_PROTECT(BgL_exitd1051z00_1457);
												BgL_arg2011z00_2531 =
													CDR(((obj_t) BgL_arg2012z00_2532));
											}
											BGL_EXITD_PROTECT_SET(BgL_exitd1051z00_1457,
												BgL_arg2011z00_2531);
											BUNSPEC;
										}
									else
										{	/* Ieee/port.scm 594 */
											BFALSE;
										}
								}
								bgl_close_output_port(BgL_portz00_1455);
								return BgL_tmp1053z00_1459;
							}
						}
					}
				else
					{	/* Ieee/port.scm 593 */
						return
							bgl_system_failure(BGL_IO_PORT_ERROR,
							BGl_string2408z00zz__r4_ports_6_10_1z00,
							BGl_string2401z00zz__r4_ports_6_10_1z00, BgL_stringz00_7);
					}
			}
		}

	}



/* &call-with-output-file */
	obj_t BGl_z62callzd2withzd2outputzd2filezb0zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3431, obj_t BgL_stringz00_3432, obj_t BgL_procz00_3433)
	{
		{	/* Ieee/port.scm 591 */
			{	/* Ieee/port.scm 592 */
				obj_t BgL_auxz00_4393;
				obj_t BgL_auxz00_4386;

				if (PROCEDUREP(BgL_procz00_3433))
					{	/* Ieee/port.scm 592 */
						BgL_auxz00_4393 = BgL_procz00_3433;
					}
				else
					{
						obj_t BgL_auxz00_4396;

						BgL_auxz00_4396 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(26798L),
							BGl_string2409z00zz__r4_ports_6_10_1z00,
							BGl_string2405z00zz__r4_ports_6_10_1z00, BgL_procz00_3433);
						FAILURE(BgL_auxz00_4396, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_stringz00_3432))
					{	/* Ieee/port.scm 592 */
						BgL_auxz00_4386 = BgL_stringz00_3432;
					}
				else
					{
						obj_t BgL_auxz00_4389;

						BgL_auxz00_4389 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(26798L),
							BGl_string2409z00zz__r4_ports_6_10_1z00,
							BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_stringz00_3432);
						FAILURE(BgL_auxz00_4389, BFALSE, BFALSE);
					}
				return
					BGl_callzd2withzd2outputzd2filezd2zz__r4_ports_6_10_1z00
					(BgL_auxz00_4386, BgL_auxz00_4393);
			}
		}

	}



/* &<@anonymous:1439> */
	obj_t BGl_z62zc3z04anonymousza31439ze3ze5zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3434)
	{
		{	/* Ieee/port.scm 594 */
			{	/* Ieee/port.scm 596 */
				obj_t BgL_portz00_3435;

				BgL_portz00_3435 = PROCEDURE_REF(BgL_envz00_3434, (int) (0L));
				return bgl_close_output_port(((obj_t) BgL_portz00_3435));
			}
		}

	}



/* call-with-append-file */
	BGL_EXPORTED_DEF obj_t
		BGl_callzd2withzd2appendzd2filezd2zz__r4_ports_6_10_1z00(obj_t
		BgL_stringz00_9, obj_t BgL_procz00_10)
	{
		{	/* Ieee/port.scm 603 */
			{	/* Ieee/port.scm 604 */
				obj_t BgL_portz00_1464;

				{	/* Ieee/port.scm 484 */

					BgL_portz00_1464 =
						bgl_append_output_file(BgL_stringz00_9,
						BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00
						(BGl_string2410z00zz__r4_ports_6_10_1z00, BTRUE,
							(int) (default_io_bufsiz)));
				}
				if (OUTPUT_PORTP(BgL_portz00_1464))
					{	/* Ieee/port.scm 606 */
						obj_t BgL_exitd1055z00_1466;

						BgL_exitd1055z00_1466 = BGL_EXITD_TOP_AS_OBJ();
						{	/* Ieee/port.scm 608 */
							obj_t BgL_zc3z04anonymousza31441ze3z87_3436;

							BgL_zc3z04anonymousza31441ze3z87_3436 =
								MAKE_FX_PROCEDURE
								(BGl_z62zc3z04anonymousza31441ze3ze5zz__r4_ports_6_10_1z00,
								(int) (0L), (int) (1L));
							PROCEDURE_SET(BgL_zc3z04anonymousza31441ze3z87_3436, (int) (0L),
								BgL_portz00_1464);
							{	/* Ieee/port.scm 606 */
								obj_t BgL_arg2014z00_2538;

								{	/* Ieee/port.scm 606 */
									obj_t BgL_arg2015z00_2539;

									BgL_arg2015z00_2539 =
										BGL_EXITD_PROTECT(BgL_exitd1055z00_1466);
									BgL_arg2014z00_2538 =
										MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31441ze3z87_3436,
										BgL_arg2015z00_2539);
								}
								BGL_EXITD_PROTECT_SET(BgL_exitd1055z00_1466,
									BgL_arg2014z00_2538);
								BUNSPEC;
							}
							{	/* Ieee/port.scm 607 */
								obj_t BgL_tmp1057z00_1468;

								BgL_tmp1057z00_1468 =
									BGL_PROCEDURE_CALL1(BgL_procz00_10, BgL_portz00_1464);
								{	/* Ieee/port.scm 606 */
									bool_t BgL_test2929z00_4423;

									{	/* Ieee/port.scm 606 */
										obj_t BgL_arg2013z00_2541;

										BgL_arg2013z00_2541 =
											BGL_EXITD_PROTECT(BgL_exitd1055z00_1466);
										BgL_test2929z00_4423 = PAIRP(BgL_arg2013z00_2541);
									}
									if (BgL_test2929z00_4423)
										{	/* Ieee/port.scm 606 */
											obj_t BgL_arg2011z00_2542;

											{	/* Ieee/port.scm 606 */
												obj_t BgL_arg2012z00_2543;

												BgL_arg2012z00_2543 =
													BGL_EXITD_PROTECT(BgL_exitd1055z00_1466);
												BgL_arg2011z00_2542 =
													CDR(((obj_t) BgL_arg2012z00_2543));
											}
											BGL_EXITD_PROTECT_SET(BgL_exitd1055z00_1466,
												BgL_arg2011z00_2542);
											BUNSPEC;
										}
									else
										{	/* Ieee/port.scm 606 */
											BFALSE;
										}
								}
								bgl_close_output_port(BgL_portz00_1464);
								return BgL_tmp1057z00_1468;
							}
						}
					}
				else
					{	/* Ieee/port.scm 605 */
						return
							bgl_system_failure(BGL_IO_PORT_ERROR,
							BGl_string2411z00zz__r4_ports_6_10_1z00,
							BGl_string2401z00zz__r4_ports_6_10_1z00, BgL_stringz00_9);
					}
			}
		}

	}



/* &call-with-append-file */
	obj_t BGl_z62callzd2withzd2appendzd2filezb0zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3437, obj_t BgL_stringz00_3438, obj_t BgL_procz00_3439)
	{
		{	/* Ieee/port.scm 603 */
			{	/* Ieee/port.scm 604 */
				obj_t BgL_auxz00_4439;
				obj_t BgL_auxz00_4432;

				if (PROCEDUREP(BgL_procz00_3439))
					{	/* Ieee/port.scm 604 */
						BgL_auxz00_4439 = BgL_procz00_3439;
					}
				else
					{
						obj_t BgL_auxz00_4442;

						BgL_auxz00_4442 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(27304L),
							BGl_string2412z00zz__r4_ports_6_10_1z00,
							BGl_string2405z00zz__r4_ports_6_10_1z00, BgL_procz00_3439);
						FAILURE(BgL_auxz00_4442, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_stringz00_3438))
					{	/* Ieee/port.scm 604 */
						BgL_auxz00_4432 = BgL_stringz00_3438;
					}
				else
					{
						obj_t BgL_auxz00_4435;

						BgL_auxz00_4435 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(27304L),
							BGl_string2412z00zz__r4_ports_6_10_1z00,
							BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_stringz00_3438);
						FAILURE(BgL_auxz00_4435, BFALSE, BFALSE);
					}
				return
					BGl_callzd2withzd2appendzd2filezd2zz__r4_ports_6_10_1z00
					(BgL_auxz00_4432, BgL_auxz00_4439);
			}
		}

	}



/* &<@anonymous:1441> */
	obj_t BGl_z62zc3z04anonymousza31441ze3ze5zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3440)
	{
		{	/* Ieee/port.scm 606 */
			{	/* Ieee/port.scm 608 */
				obj_t BgL_portz00_3441;

				BgL_portz00_3441 = PROCEDURE_REF(BgL_envz00_3440, (int) (0L));
				return bgl_close_output_port(((obj_t) BgL_portz00_3441));
			}
		}

	}



/* call-with-output-string */
	BGL_EXPORTED_DEF obj_t
		BGl_callzd2withzd2outputzd2stringzd2zz__r4_ports_6_10_1z00(obj_t
		BgL_procz00_11)
	{
		{	/* Ieee/port.scm 615 */
			{	/* Ieee/port.scm 616 */
				obj_t BgL_portz00_2546;

				{	/* Ieee/port.scm 616 */

					{	/* Ieee/port.scm 485 */

						BgL_portz00_2546 =
							bgl_open_output_string
							(BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00
							(BGl_string2407z00zz__r4_ports_6_10_1z00, BTRUE, (int) (128L)));
				}}
				BGL_PROCEDURE_CALL1(BgL_procz00_11, BgL_portz00_2546);
				return bgl_close_output_port(BgL_portz00_2546);
			}
		}

	}



/* &call-with-output-string */
	obj_t BGl_z62callzd2withzd2outputzd2stringzb0zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3442, obj_t BgL_procz00_3443)
	{
		{	/* Ieee/port.scm 615 */
			{	/* Ieee/port.scm 616 */
				obj_t BgL_auxz00_4459;

				if (PROCEDUREP(BgL_procz00_3443))
					{	/* Ieee/port.scm 616 */
						BgL_auxz00_4459 = BgL_procz00_3443;
					}
				else
					{
						obj_t BgL_auxz00_4462;

						BgL_auxz00_4462 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(27807L),
							BGl_string2413z00zz__r4_ports_6_10_1z00,
							BGl_string2405z00zz__r4_ports_6_10_1z00, BgL_procz00_3443);
						FAILURE(BgL_auxz00_4462, BFALSE, BFALSE);
					}
				return
					BGl_callzd2withzd2outputzd2stringzd2zz__r4_ports_6_10_1z00
					(BgL_auxz00_4459);
			}
		}

	}



/* input-port? */
	BGL_EXPORTED_DEF obj_t BGl_inputzd2portzf3z21zz__r4_ports_6_10_1z00(obj_t
		BgL_objz00_12)
	{
		{	/* Ieee/port.scm 623 */
			return BBOOL(INPUT_PORTP(BgL_objz00_12));
		}

	}



/* &input-port? */
	obj_t BGl_z62inputzd2portzf3z43zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3444,
		obj_t BgL_objz00_3445)
	{
		{	/* Ieee/port.scm 623 */
			return BGl_inputzd2portzf3z21zz__r4_ports_6_10_1z00(BgL_objz00_3445);
		}

	}



/* input-string-port? */
	BGL_EXPORTED_DEF obj_t
		BGl_inputzd2stringzd2portzf3zf3zz__r4_ports_6_10_1z00(obj_t BgL_objz00_13)
	{
		{	/* Ieee/port.scm 629 */
			return BBOOL(INPUT_STRING_PORTP(BgL_objz00_13));
		}

	}



/* &input-string-port? */
	obj_t BGl_z62inputzd2stringzd2portzf3z91zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3446, obj_t BgL_objz00_3447)
	{
		{	/* Ieee/port.scm 629 */
			return
				BGl_inputzd2stringzd2portzf3zf3zz__r4_ports_6_10_1z00(BgL_objz00_3447);
		}

	}



/* input-procedure-port? */
	BGL_EXPORTED_DEF obj_t
		BGl_inputzd2procedurezd2portzf3zf3zz__r4_ports_6_10_1z00(obj_t
		BgL_objz00_14)
	{
		{	/* Ieee/port.scm 635 */
			return BBOOL(INPUT_PROCEDURE_PORTP(BgL_objz00_14));
		}

	}



/* &input-procedure-port? */
	obj_t BGl_z62inputzd2procedurezd2portzf3z91zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3448, obj_t BgL_objz00_3449)
	{
		{	/* Ieee/port.scm 635 */
			return
				BGl_inputzd2procedurezd2portzf3zf3zz__r4_ports_6_10_1z00
				(BgL_objz00_3449);
		}

	}



/* input-gzip-port? */
	BGL_EXPORTED_DEF obj_t
		BGl_inputzd2gza7ipzd2portzf3z54zz__r4_ports_6_10_1z00(obj_t BgL_objz00_15)
	{
		{	/* Ieee/port.scm 641 */
			return BBOOL(INPUT_GZIP_PORTP(BgL_objz00_15));
		}

	}



/* &input-gzip-port? */
	obj_t BGl_z62inputzd2gza7ipzd2portzf3z36zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3450, obj_t BgL_objz00_3451)
	{
		{	/* Ieee/port.scm 641 */
			return
				BGl_inputzd2gza7ipzd2portzf3z54zz__r4_ports_6_10_1z00(BgL_objz00_3451);
		}

	}



/* input-mmap-port? */
	BGL_EXPORTED_DEF obj_t
		BGl_inputzd2mmapzd2portzf3zf3zz__r4_ports_6_10_1z00(obj_t BgL_objz00_16)
	{
		{	/* Ieee/port.scm 647 */
			return BBOOL(INPUT_MMAP_PORTP(BgL_objz00_16));
		}

	}



/* &input-mmap-port? */
	obj_t BGl_z62inputzd2mmapzd2portzf3z91zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3452, obj_t BgL_objz00_3453)
	{
		{	/* Ieee/port.scm 647 */
			return
				BGl_inputzd2mmapzd2portzf3zf3zz__r4_ports_6_10_1z00(BgL_objz00_3453);
		}

	}



/* output-port? */
	BGL_EXPORTED_DEF obj_t BGl_outputzd2portzf3z21zz__r4_ports_6_10_1z00(obj_t
		BgL_objz00_17)
	{
		{	/* Ieee/port.scm 653 */
			return BBOOL(OUTPUT_PORTP(BgL_objz00_17));
		}

	}



/* &output-port? */
	obj_t BGl_z62outputzd2portzf3z43zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3454,
		obj_t BgL_objz00_3455)
	{
		{	/* Ieee/port.scm 653 */
			return BGl_outputzd2portzf3z21zz__r4_ports_6_10_1z00(BgL_objz00_3455);
		}

	}



/* output-string-port? */
	BGL_EXPORTED_DEF obj_t
		BGl_outputzd2stringzd2portzf3zf3zz__r4_ports_6_10_1z00(obj_t BgL_objz00_18)
	{
		{	/* Ieee/port.scm 659 */
			return BBOOL(BGL_OUTPUT_STRING_PORTP(BgL_objz00_18));
		}

	}



/* &output-string-port? */
	obj_t BGl_z62outputzd2stringzd2portzf3z91zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3456, obj_t BgL_objz00_3457)
	{
		{	/* Ieee/port.scm 659 */
			return
				BGl_outputzd2stringzd2portzf3zf3zz__r4_ports_6_10_1z00(BgL_objz00_3457);
		}

	}



/* output-procedure-port? */
	BGL_EXPORTED_DEF obj_t
		BGl_outputzd2procedurezd2portzf3zf3zz__r4_ports_6_10_1z00(obj_t
		BgL_objz00_19)
	{
		{	/* Ieee/port.scm 665 */
			return BBOOL(BGL_OUTPUT_PROCEDURE_PORTP(BgL_objz00_19));
		}

	}



/* &output-procedure-port? */
	obj_t BGl_z62outputzd2procedurezd2portzf3z91zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3458, obj_t BgL_objz00_3459)
	{
		{	/* Ieee/port.scm 665 */
			return
				BGl_outputzd2procedurezd2portzf3zf3zz__r4_ports_6_10_1z00
				(BgL_objz00_3459);
		}

	}



/* current-input-port */
	BGL_EXPORTED_DEF obj_t
		BGl_currentzd2inputzd2portz00zz__r4_ports_6_10_1z00(void)
	{
		{	/* Ieee/port.scm 671 */
			{	/* Ieee/port.scm 672 */
				obj_t BgL_tmpz00_4491;

				BgL_tmpz00_4491 = BGL_CURRENT_DYNAMIC_ENV();
				return BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_4491);
			}
		}

	}



/* &current-input-port */
	obj_t BGl_z62currentzd2inputzd2portz62zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3460)
	{
		{	/* Ieee/port.scm 671 */
			return BGl_currentzd2inputzd2portz00zz__r4_ports_6_10_1z00();
		}

	}



/* current-output-port */
	BGL_EXPORTED_DEF obj_t
		BGl_currentzd2outputzd2portz00zz__r4_ports_6_10_1z00(void)
	{
		{	/* Ieee/port.scm 677 */
			{	/* Ieee/port.scm 678 */
				obj_t BgL_tmpz00_4495;

				BgL_tmpz00_4495 = BGL_CURRENT_DYNAMIC_ENV();
				return BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4495);
			}
		}

	}



/* &current-output-port */
	obj_t BGl_z62currentzd2outputzd2portz62zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3461)
	{
		{	/* Ieee/port.scm 677 */
			return BGl_currentzd2outputzd2portz00zz__r4_ports_6_10_1z00();
		}

	}



/* current-error-port */
	BGL_EXPORTED_DEF obj_t
		BGl_currentzd2errorzd2portz00zz__r4_ports_6_10_1z00(void)
	{
		{	/* Ieee/port.scm 683 */
			{	/* Ieee/port.scm 684 */
				obj_t BgL_tmpz00_4499;

				BgL_tmpz00_4499 = BGL_CURRENT_DYNAMIC_ENV();
				return BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_4499);
			}
		}

	}



/* &current-error-port */
	obj_t BGl_z62currentzd2errorzd2portz62zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3462)
	{
		{	/* Ieee/port.scm 683 */
			return BGl_currentzd2errorzd2portz00zz__r4_ports_6_10_1z00();
		}

	}



/* input-port-reopen! */
	BGL_EXPORTED_DEF obj_t
		BGl_inputzd2portzd2reopenz12z12zz__r4_ports_6_10_1z00(obj_t BgL_portz00_20)
	{
		{	/* Ieee/port.scm 689 */
			if (CBOOL(bgl_input_port_reopen(BgL_portz00_20)))
				{	/* Ieee/port.scm 690 */
					return BFALSE;
				}
			else
				{	/* Ieee/port.scm 690 */
					return
						bgl_system_failure(BGL_IO_PORT_ERROR,
						BGl_string2414z00zz__r4_ports_6_10_1z00,
						BGl_string2415z00zz__r4_ports_6_10_1z00, BgL_portz00_20);
				}
		}

	}



/* &input-port-reopen! */
	obj_t BGl_z62inputzd2portzd2reopenz12z70zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3463, obj_t BgL_portz00_3464)
	{
		{	/* Ieee/port.scm 689 */
			{	/* Ieee/port.scm 690 */
				obj_t BgL_auxz00_4507;

				if (INPUT_PORTP(BgL_portz00_3464))
					{	/* Ieee/port.scm 690 */
						BgL_auxz00_4507 = BgL_portz00_3464;
					}
				else
					{
						obj_t BgL_auxz00_4510;

						BgL_auxz00_4510 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(31434L),
							BGl_string2416z00zz__r4_ports_6_10_1z00,
							BGl_string2417z00zz__r4_ports_6_10_1z00, BgL_portz00_3464);
						FAILURE(BgL_auxz00_4510, BFALSE, BFALSE);
					}
				return
					BGl_inputzd2portzd2reopenz12z12zz__r4_ports_6_10_1z00
					(BgL_auxz00_4507);
			}
		}

	}



/* input-port-clone! */
	BGL_EXPORTED_DEF obj_t
		BGl_inputzd2portzd2clonez12z12zz__r4_ports_6_10_1z00(obj_t BgL_dstz00_21,
		obj_t BgL_srcz00_22)
	{
		{	/* Ieee/port.scm 697 */
			BGL_TAIL return bgl_input_port_clone(BgL_dstz00_21, BgL_srcz00_22);
		}

	}



/* &input-port-clone! */
	obj_t BGl_z62inputzd2portzd2clonez12z70zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3465, obj_t BgL_dstz00_3466, obj_t BgL_srcz00_3467)
	{
		{	/* Ieee/port.scm 697 */
			{	/* Ieee/port.scm 698 */
				obj_t BgL_auxz00_4523;
				obj_t BgL_auxz00_4516;

				if (INPUT_PORTP(BgL_srcz00_3467))
					{	/* Ieee/port.scm 698 */
						BgL_auxz00_4523 = BgL_srcz00_3467;
					}
				else
					{
						obj_t BgL_auxz00_4526;

						BgL_auxz00_4526 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(31854L),
							BGl_string2418z00zz__r4_ports_6_10_1z00,
							BGl_string2417z00zz__r4_ports_6_10_1z00, BgL_srcz00_3467);
						FAILURE(BgL_auxz00_4526, BFALSE, BFALSE);
					}
				if (INPUT_PORTP(BgL_dstz00_3466))
					{	/* Ieee/port.scm 698 */
						BgL_auxz00_4516 = BgL_dstz00_3466;
					}
				else
					{
						obj_t BgL_auxz00_4519;

						BgL_auxz00_4519 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(31854L),
							BGl_string2418z00zz__r4_ports_6_10_1z00,
							BGl_string2417z00zz__r4_ports_6_10_1z00, BgL_dstz00_3466);
						FAILURE(BgL_auxz00_4519, BFALSE, BFALSE);
					}
				return
					BGl_inputzd2portzd2clonez12z12zz__r4_ports_6_10_1z00(BgL_auxz00_4516,
					BgL_auxz00_4523);
			}
		}

	}



/* with-input-from-file */
	BGL_EXPORTED_DEF obj_t
		BGl_withzd2inputzd2fromzd2filezd2zz__r4_ports_6_10_1z00(obj_t
		BgL_stringz00_23, obj_t BgL_thunkz00_24)
	{
		{	/* Ieee/port.scm 703 */
			{	/* Ieee/port.scm 704 */
				obj_t BgL_portz00_1479;

				{	/* Ieee/port.scm 466 */

					BgL_portz00_1479 =
						BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(BgL_stringz00_23,
						BTRUE, BINT(5000000L));
				}
				if (INPUT_PORTP(BgL_portz00_1479))
					{	/* Ieee/port.scm 706 */
						obj_t BgL_denvz00_1481;

						BgL_denvz00_1481 = BGL_CURRENT_DYNAMIC_ENV();
						{	/* Ieee/port.scm 706 */
							obj_t BgL_oldzd2inputzd2portz00_1482;

							BgL_oldzd2inputzd2portz00_1482 =
								BGL_ENV_CURRENT_INPUT_PORT(BgL_denvz00_1481);
							{	/* Ieee/port.scm 707 */

								{	/* Ieee/port.scm 708 */
									obj_t BgL_exitd1059z00_1483;

									BgL_exitd1059z00_1483 = BGL_EXITD_TOP_AS_OBJ();
									{	/* Ieee/port.scm 713 */
										obj_t BgL_zc3z04anonymousza31447ze3z87_3468;

										BgL_zc3z04anonymousza31447ze3z87_3468 =
											MAKE_FX_PROCEDURE
											(BGl_z62zc3z04anonymousza31447ze3ze5zz__r4_ports_6_10_1z00,
											(int) (0L), (int) (3L));
										PROCEDURE_SET(BgL_zc3z04anonymousza31447ze3z87_3468,
											(int) (0L), BgL_denvz00_1481);
										PROCEDURE_SET(BgL_zc3z04anonymousza31447ze3z87_3468,
											(int) (1L), BgL_oldzd2inputzd2portz00_1482);
										PROCEDURE_SET(BgL_zc3z04anonymousza31447ze3z87_3468,
											(int) (2L), BgL_portz00_1479);
										{	/* Ieee/port.scm 708 */
											obj_t BgL_arg2014z00_2555;

											{	/* Ieee/port.scm 708 */
												obj_t BgL_arg2015z00_2556;

												BgL_arg2015z00_2556 =
													BGL_EXITD_PROTECT(BgL_exitd1059z00_1483);
												BgL_arg2014z00_2555 =
													MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31447ze3z87_3468,
													BgL_arg2015z00_2556);
											}
											BGL_EXITD_PROTECT_SET(BgL_exitd1059z00_1483,
												BgL_arg2014z00_2555);
											BUNSPEC;
										}
										{	/* Ieee/port.scm 710 */
											obj_t BgL_tmp1061z00_1485;

											BGL_ENV_CURRENT_INPUT_PORT_SET(BgL_denvz00_1481,
												BgL_portz00_1479);
											BUNSPEC;
											BgL_tmp1061z00_1485 =
												BGL_PROCEDURE_CALL0(BgL_thunkz00_24);
											{	/* Ieee/port.scm 708 */
												bool_t BgL_test2938z00_4554;

												{	/* Ieee/port.scm 708 */
													obj_t BgL_arg2013z00_2558;

													BgL_arg2013z00_2558 =
														BGL_EXITD_PROTECT(BgL_exitd1059z00_1483);
													BgL_test2938z00_4554 = PAIRP(BgL_arg2013z00_2558);
												}
												if (BgL_test2938z00_4554)
													{	/* Ieee/port.scm 708 */
														obj_t BgL_arg2011z00_2559;

														{	/* Ieee/port.scm 708 */
															obj_t BgL_arg2012z00_2560;

															BgL_arg2012z00_2560 =
																BGL_EXITD_PROTECT(BgL_exitd1059z00_1483);
															BgL_arg2011z00_2559 =
																CDR(((obj_t) BgL_arg2012z00_2560));
														}
														BGL_EXITD_PROTECT_SET(BgL_exitd1059z00_1483,
															BgL_arg2011z00_2559);
														BUNSPEC;
													}
												else
													{	/* Ieee/port.scm 708 */
														BFALSE;
													}
											}
											BGL_ENV_CURRENT_INPUT_PORT_SET(BgL_denvz00_1481,
												BgL_oldzd2inputzd2portz00_1482);
											BUNSPEC;
											bgl_close_input_port(BgL_portz00_1479);
											return BgL_tmp1061z00_1485;
										}
									}
								}
							}
						}
					}
				else
					{	/* Ieee/port.scm 705 */
						return
							bgl_system_failure(BGL_IO_PORT_ERROR,
							BGl_string2419z00zz__r4_ports_6_10_1z00,
							BGl_string2401z00zz__r4_ports_6_10_1z00, BgL_stringz00_23);
					}
			}
		}

	}



/* &with-input-from-file */
	obj_t BGl_z62withzd2inputzd2fromzd2filezb0zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3469, obj_t BgL_stringz00_3470, obj_t BgL_thunkz00_3471)
	{
		{	/* Ieee/port.scm 703 */
			{	/* Ieee/port.scm 704 */
				obj_t BgL_auxz00_4571;
				obj_t BgL_auxz00_4564;

				if (PROCEDUREP(BgL_thunkz00_3471))
					{	/* Ieee/port.scm 704 */
						BgL_auxz00_4571 = BgL_thunkz00_3471;
					}
				else
					{
						obj_t BgL_auxz00_4574;

						BgL_auxz00_4574 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(32154L),
							BGl_string2420z00zz__r4_ports_6_10_1z00,
							BGl_string2405z00zz__r4_ports_6_10_1z00, BgL_thunkz00_3471);
						FAILURE(BgL_auxz00_4574, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_stringz00_3470))
					{	/* Ieee/port.scm 704 */
						BgL_auxz00_4564 = BgL_stringz00_3470;
					}
				else
					{
						obj_t BgL_auxz00_4567;

						BgL_auxz00_4567 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(32154L),
							BGl_string2420z00zz__r4_ports_6_10_1z00,
							BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_stringz00_3470);
						FAILURE(BgL_auxz00_4567, BFALSE, BFALSE);
					}
				return
					BGl_withzd2inputzd2fromzd2filezd2zz__r4_ports_6_10_1z00
					(BgL_auxz00_4564, BgL_auxz00_4571);
			}
		}

	}



/* &<@anonymous:1447> */
	obj_t BGl_z62zc3z04anonymousza31447ze3ze5zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3472)
	{
		{	/* Ieee/port.scm 708 */
			{	/* Ieee/port.scm 713 */
				obj_t BgL_denvz00_3473;
				obj_t BgL_oldzd2inputzd2portz00_3474;
				obj_t BgL_portz00_3475;

				BgL_denvz00_3473 = ((obj_t) PROCEDURE_REF(BgL_envz00_3472, (int) (0L)));
				BgL_oldzd2inputzd2portz00_3474 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3472, (int) (1L)));
				BgL_portz00_3475 = PROCEDURE_REF(BgL_envz00_3472, (int) (2L));
				BGL_ENV_CURRENT_INPUT_PORT_SET(BgL_denvz00_3473,
					BgL_oldzd2inputzd2portz00_3474);
				BUNSPEC;
				return bgl_close_input_port(((obj_t) BgL_portz00_3475));
			}
		}

	}



/* with-input-from-string */
	BGL_EXPORTED_DEF obj_t
		BGl_withzd2inputzd2fromzd2stringzd2zz__r4_ports_6_10_1z00(obj_t
		BgL_stringz00_25, obj_t BgL_thunkz00_26)
	{
		{	/* Ieee/port.scm 721 */
			{	/* Ieee/port.scm 722 */
				obj_t BgL_portz00_1491;

				{	/* Ieee/port.scm 469 */
					long BgL_endz00_1501;

					BgL_endz00_1501 = STRING_LENGTH(BgL_stringz00_25);
					{	/* Ieee/port.scm 469 */

						BgL_portz00_1491 =
							BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00
							(BgL_stringz00_25, BINT(0L), BINT(BgL_endz00_1501));
				}}
				{	/* Ieee/port.scm 722 */
					obj_t BgL_denvz00_1492;

					BgL_denvz00_1492 = BGL_CURRENT_DYNAMIC_ENV();
					{	/* Ieee/port.scm 723 */
						obj_t BgL_oldzd2inputzd2portz00_1493;

						BgL_oldzd2inputzd2portz00_1493 =
							BGL_ENV_CURRENT_INPUT_PORT(BgL_denvz00_1492);
						{	/* Ieee/port.scm 724 */

							{	/* Ieee/port.scm 725 */
								obj_t BgL_exitd1063z00_1494;

								BgL_exitd1063z00_1494 = BGL_EXITD_TOP_AS_OBJ();
								{	/* Ieee/port.scm 730 */
									obj_t BgL_zc3z04anonymousza31448ze3z87_3476;

									BgL_zc3z04anonymousza31448ze3z87_3476 =
										MAKE_FX_PROCEDURE
										(BGl_z62zc3z04anonymousza31448ze3ze5zz__r4_ports_6_10_1z00,
										(int) (0L), (int) (3L));
									PROCEDURE_SET(BgL_zc3z04anonymousza31448ze3z87_3476,
										(int) (0L), BgL_denvz00_1492);
									PROCEDURE_SET(BgL_zc3z04anonymousza31448ze3z87_3476,
										(int) (1L), BgL_oldzd2inputzd2portz00_1493);
									PROCEDURE_SET(BgL_zc3z04anonymousza31448ze3z87_3476,
										(int) (2L), BgL_portz00_1491);
									{	/* Ieee/port.scm 725 */
										obj_t BgL_arg2014z00_2565;

										{	/* Ieee/port.scm 725 */
											obj_t BgL_arg2015z00_2566;

											BgL_arg2015z00_2566 =
												BGL_EXITD_PROTECT(BgL_exitd1063z00_1494);
											BgL_arg2014z00_2565 =
												MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31448ze3z87_3476,
												BgL_arg2015z00_2566);
										}
										BGL_EXITD_PROTECT_SET(BgL_exitd1063z00_1494,
											BgL_arg2014z00_2565);
										BUNSPEC;
									}
									{	/* Ieee/port.scm 727 */
										obj_t BgL_tmp1065z00_1496;

										BGL_ENV_CURRENT_INPUT_PORT_SET(BgL_denvz00_1492,
											BgL_portz00_1491);
										BUNSPEC;
										BgL_tmp1065z00_1496 = BGL_PROCEDURE_CALL0(BgL_thunkz00_26);
										{	/* Ieee/port.scm 725 */
											bool_t BgL_test2941z00_4613;

											{	/* Ieee/port.scm 725 */
												obj_t BgL_arg2013z00_2568;

												BgL_arg2013z00_2568 =
													BGL_EXITD_PROTECT(BgL_exitd1063z00_1494);
												BgL_test2941z00_4613 = PAIRP(BgL_arg2013z00_2568);
											}
											if (BgL_test2941z00_4613)
												{	/* Ieee/port.scm 725 */
													obj_t BgL_arg2011z00_2569;

													{	/* Ieee/port.scm 725 */
														obj_t BgL_arg2012z00_2570;

														BgL_arg2012z00_2570 =
															BGL_EXITD_PROTECT(BgL_exitd1063z00_1494);
														BgL_arg2011z00_2569 =
															CDR(((obj_t) BgL_arg2012z00_2570));
													}
													BGL_EXITD_PROTECT_SET(BgL_exitd1063z00_1494,
														BgL_arg2011z00_2569);
													BUNSPEC;
												}
											else
												{	/* Ieee/port.scm 725 */
													BFALSE;
												}
										}
										BGL_ENV_CURRENT_INPUT_PORT_SET(BgL_denvz00_1492,
											BgL_oldzd2inputzd2portz00_1493);
										BUNSPEC;
										bgl_close_input_port(BgL_portz00_1491);
										return BgL_tmp1065z00_1496;
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &with-input-from-string */
	obj_t BGl_z62withzd2inputzd2fromzd2stringzb0zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3477, obj_t BgL_stringz00_3478, obj_t BgL_thunkz00_3479)
	{
		{	/* Ieee/port.scm 721 */
			{	/* Ieee/port.scm 722 */
				obj_t BgL_auxz00_4629;
				obj_t BgL_auxz00_4622;

				if (PROCEDUREP(BgL_thunkz00_3479))
					{	/* Ieee/port.scm 722 */
						BgL_auxz00_4629 = BgL_thunkz00_3479;
					}
				else
					{
						obj_t BgL_auxz00_4632;

						BgL_auxz00_4632 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(32860L),
							BGl_string2421z00zz__r4_ports_6_10_1z00,
							BGl_string2405z00zz__r4_ports_6_10_1z00, BgL_thunkz00_3479);
						FAILURE(BgL_auxz00_4632, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_stringz00_3478))
					{	/* Ieee/port.scm 722 */
						BgL_auxz00_4622 = BgL_stringz00_3478;
					}
				else
					{
						obj_t BgL_auxz00_4625;

						BgL_auxz00_4625 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(32860L),
							BGl_string2421z00zz__r4_ports_6_10_1z00,
							BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_stringz00_3478);
						FAILURE(BgL_auxz00_4625, BFALSE, BFALSE);
					}
				return
					BGl_withzd2inputzd2fromzd2stringzd2zz__r4_ports_6_10_1z00
					(BgL_auxz00_4622, BgL_auxz00_4629);
			}
		}

	}



/* &<@anonymous:1448> */
	obj_t BGl_z62zc3z04anonymousza31448ze3ze5zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3480)
	{
		{	/* Ieee/port.scm 725 */
			{	/* Ieee/port.scm 730 */
				obj_t BgL_denvz00_3481;
				obj_t BgL_oldzd2inputzd2portz00_3482;
				obj_t BgL_portz00_3483;

				BgL_denvz00_3481 = ((obj_t) PROCEDURE_REF(BgL_envz00_3480, (int) (0L)));
				BgL_oldzd2inputzd2portz00_3482 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3480, (int) (1L)));
				BgL_portz00_3483 = ((obj_t) PROCEDURE_REF(BgL_envz00_3480, (int) (2L)));
				BGL_ENV_CURRENT_INPUT_PORT_SET(BgL_denvz00_3481,
					BgL_oldzd2inputzd2portz00_3482);
				BUNSPEC;
				return bgl_close_input_port(BgL_portz00_3483);
			}
		}

	}



/* with-input-from-port */
	BGL_EXPORTED_DEF obj_t
		BGl_withzd2inputzd2fromzd2portzd2zz__r4_ports_6_10_1z00(obj_t
		BgL_portz00_27, obj_t BgL_thunkz00_28)
	{
		{	/* Ieee/port.scm 736 */
			{	/* Ieee/port.scm 737 */
				obj_t BgL_denvz00_1502;

				BgL_denvz00_1502 = BGL_CURRENT_DYNAMIC_ENV();
				{	/* Ieee/port.scm 737 */
					obj_t BgL_oldzd2inputzd2portz00_1503;

					BgL_oldzd2inputzd2portz00_1503 =
						BGL_ENV_CURRENT_INPUT_PORT(BgL_denvz00_1502);
					{	/* Ieee/port.scm 738 */

						{	/* Ieee/port.scm 739 */
							obj_t BgL_exitd1067z00_1504;

							BgL_exitd1067z00_1504 = BGL_EXITD_TOP_AS_OBJ();
							{
								obj_t BgL_zc3z04anonymousza31449ze3z87_3484;

								BgL_zc3z04anonymousza31449ze3z87_3484 =
									MAKE_FX_PROCEDURE
									(BGl_z62zc3z04anonymousza31449ze3ze5zz__r4_ports_6_10_1z00,
									(int) (0L), (int) (2L));
								PROCEDURE_SET(BgL_zc3z04anonymousza31449ze3z87_3484, (int) (0L),
									BgL_denvz00_1502);
								PROCEDURE_SET(BgL_zc3z04anonymousza31449ze3z87_3484, (int) (1L),
									BgL_oldzd2inputzd2portz00_1503);
								{	/* Ieee/port.scm 739 */
									obj_t BgL_arg2014z00_2573;

									{	/* Ieee/port.scm 739 */
										obj_t BgL_arg2015z00_2574;

										BgL_arg2015z00_2574 =
											BGL_EXITD_PROTECT(BgL_exitd1067z00_1504);
										BgL_arg2014z00_2573 =
											MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31449ze3z87_3484,
											BgL_arg2015z00_2574);
									}
									BGL_EXITD_PROTECT_SET(BgL_exitd1067z00_1504,
										BgL_arg2014z00_2573);
									BUNSPEC;
								}
								{	/* Ieee/port.scm 741 */
									obj_t BgL_tmp1069z00_1506;

									BGL_ENV_CURRENT_INPUT_PORT_SET(BgL_denvz00_1502,
										BgL_portz00_27);
									BUNSPEC;
									BgL_tmp1069z00_1506 = BGL_PROCEDURE_CALL0(BgL_thunkz00_28);
									{	/* Ieee/port.scm 739 */
										bool_t BgL_test2944z00_4665;

										{	/* Ieee/port.scm 739 */
											obj_t BgL_arg2013z00_2576;

											BgL_arg2013z00_2576 =
												BGL_EXITD_PROTECT(BgL_exitd1067z00_1504);
											BgL_test2944z00_4665 = PAIRP(BgL_arg2013z00_2576);
										}
										if (BgL_test2944z00_4665)
											{	/* Ieee/port.scm 739 */
												obj_t BgL_arg2011z00_2577;

												{	/* Ieee/port.scm 739 */
													obj_t BgL_arg2012z00_2578;

													BgL_arg2012z00_2578 =
														BGL_EXITD_PROTECT(BgL_exitd1067z00_1504);
													BgL_arg2011z00_2577 =
														CDR(((obj_t) BgL_arg2012z00_2578));
												}
												BGL_EXITD_PROTECT_SET(BgL_exitd1067z00_1504,
													BgL_arg2011z00_2577);
												BUNSPEC;
											}
										else
											{	/* Ieee/port.scm 739 */
												BFALSE;
											}
									}
									BGL_ENV_CURRENT_INPUT_PORT_SET(BgL_denvz00_1502,
										BgL_oldzd2inputzd2portz00_1503);
									BUNSPEC;
									return BgL_tmp1069z00_1506;
								}
							}
						}
					}
				}
			}
		}

	}



/* &with-input-from-port */
	obj_t BGl_z62withzd2inputzd2fromzd2portzb0zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3485, obj_t BgL_portz00_3486, obj_t BgL_thunkz00_3487)
	{
		{	/* Ieee/port.scm 736 */
			{	/* Ieee/port.scm 737 */
				obj_t BgL_auxz00_4680;
				obj_t BgL_auxz00_4673;

				if (PROCEDUREP(BgL_thunkz00_3487))
					{	/* Ieee/port.scm 737 */
						BgL_auxz00_4680 = BgL_thunkz00_3487;
					}
				else
					{
						obj_t BgL_auxz00_4683;

						BgL_auxz00_4683 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(33434L),
							BGl_string2422z00zz__r4_ports_6_10_1z00,
							BGl_string2405z00zz__r4_ports_6_10_1z00, BgL_thunkz00_3487);
						FAILURE(BgL_auxz00_4683, BFALSE, BFALSE);
					}
				if (INPUT_PORTP(BgL_portz00_3486))
					{	/* Ieee/port.scm 737 */
						BgL_auxz00_4673 = BgL_portz00_3486;
					}
				else
					{
						obj_t BgL_auxz00_4676;

						BgL_auxz00_4676 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(33434L),
							BGl_string2422z00zz__r4_ports_6_10_1z00,
							BGl_string2417z00zz__r4_ports_6_10_1z00, BgL_portz00_3486);
						FAILURE(BgL_auxz00_4676, BFALSE, BFALSE);
					}
				return
					BGl_withzd2inputzd2fromzd2portzd2zz__r4_ports_6_10_1z00
					(BgL_auxz00_4673, BgL_auxz00_4680);
			}
		}

	}



/* &<@anonymous:1449> */
	obj_t BGl_z62zc3z04anonymousza31449ze3ze5zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3488)
	{
		{	/* Ieee/port.scm 739 */
			{
				obj_t BgL_denvz00_3489;
				obj_t BgL_oldzd2inputzd2portz00_3490;

				BgL_denvz00_3489 = ((obj_t) PROCEDURE_REF(BgL_envz00_3488, (int) (0L)));
				BgL_oldzd2inputzd2portz00_3490 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3488, (int) (1L)));
				BGL_ENV_CURRENT_INPUT_PORT_SET(BgL_denvz00_3489,
					BgL_oldzd2inputzd2portz00_3490);
				return BUNSPEC;
			}
		}

	}



/* with-input-from-procedure */
	BGL_EXPORTED_DEF obj_t
		BGl_withzd2inputzd2fromzd2procedurezd2zz__r4_ports_6_10_1z00(obj_t
		BgL_procz00_29, obj_t BgL_thunkz00_30)
	{
		{	/* Ieee/port.scm 748 */
			{	/* Ieee/port.scm 749 */
				obj_t BgL_portz00_1509;

				{	/* Ieee/port.scm 474 */

					BgL_portz00_1509 =
						bgl_open_input_procedure(BgL_procz00_29,
						BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00
						(BGl_string2423z00zz__r4_ports_6_10_1z00, BTRUE, (int) (1024L)));
				}
				{	/* Ieee/port.scm 751 */
					obj_t BgL_denvz00_1511;

					BgL_denvz00_1511 = BGL_CURRENT_DYNAMIC_ENV();
					{	/* Ieee/port.scm 751 */
						obj_t BgL_oldzd2inputzd2portz00_1512;

						BgL_oldzd2inputzd2portz00_1512 =
							BGL_ENV_CURRENT_INPUT_PORT(BgL_denvz00_1511);
						{	/* Ieee/port.scm 752 */

							{	/* Ieee/port.scm 753 */
								obj_t BgL_exitd1071z00_1513;

								BgL_exitd1071z00_1513 = BGL_EXITD_TOP_AS_OBJ();
								{	/* Ieee/port.scm 758 */
									obj_t BgL_zc3z04anonymousza31451ze3z87_3491;

									BgL_zc3z04anonymousza31451ze3z87_3491 =
										MAKE_FX_PROCEDURE
										(BGl_z62zc3z04anonymousza31451ze3ze5zz__r4_ports_6_10_1z00,
										(int) (0L), (int) (3L));
									PROCEDURE_SET(BgL_zc3z04anonymousza31451ze3z87_3491,
										(int) (0L), BgL_denvz00_1511);
									PROCEDURE_SET(BgL_zc3z04anonymousza31451ze3z87_3491,
										(int) (1L), BgL_oldzd2inputzd2portz00_1512);
									PROCEDURE_SET(BgL_zc3z04anonymousza31451ze3z87_3491,
										(int) (2L), BgL_portz00_1509);
									{	/* Ieee/port.scm 753 */
										obj_t BgL_arg2014z00_2583;

										{	/* Ieee/port.scm 753 */
											obj_t BgL_arg2015z00_2584;

											BgL_arg2015z00_2584 =
												BGL_EXITD_PROTECT(BgL_exitd1071z00_1513);
											BgL_arg2014z00_2583 =
												MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31451ze3z87_3491,
												BgL_arg2015z00_2584);
										}
										BGL_EXITD_PROTECT_SET(BgL_exitd1071z00_1513,
											BgL_arg2014z00_2583);
										BUNSPEC;
									}
									{	/* Ieee/port.scm 755 */
										obj_t BgL_tmp1073z00_1515;

										BGL_ENV_CURRENT_INPUT_PORT_SET(BgL_denvz00_1511,
											BgL_portz00_1509);
										BUNSPEC;
										BgL_tmp1073z00_1515 = BGL_PROCEDURE_CALL0(BgL_thunkz00_30);
										{	/* Ieee/port.scm 753 */
											bool_t BgL_test2947z00_4717;

											{	/* Ieee/port.scm 753 */
												obj_t BgL_arg2013z00_2586;

												BgL_arg2013z00_2586 =
													BGL_EXITD_PROTECT(BgL_exitd1071z00_1513);
												BgL_test2947z00_4717 = PAIRP(BgL_arg2013z00_2586);
											}
											if (BgL_test2947z00_4717)
												{	/* Ieee/port.scm 753 */
													obj_t BgL_arg2011z00_2587;

													{	/* Ieee/port.scm 753 */
														obj_t BgL_arg2012z00_2588;

														BgL_arg2012z00_2588 =
															BGL_EXITD_PROTECT(BgL_exitd1071z00_1513);
														BgL_arg2011z00_2587 =
															CDR(((obj_t) BgL_arg2012z00_2588));
													}
													BGL_EXITD_PROTECT_SET(BgL_exitd1071z00_1513,
														BgL_arg2011z00_2587);
													BUNSPEC;
												}
											else
												{	/* Ieee/port.scm 753 */
													BFALSE;
												}
										}
										BGL_ENV_CURRENT_INPUT_PORT_SET(BgL_denvz00_1511,
											BgL_oldzd2inputzd2portz00_1512);
										BUNSPEC;
										bgl_close_input_port(BgL_portz00_1509);
										return BgL_tmp1073z00_1515;
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &with-input-from-procedure */
	obj_t BGl_z62withzd2inputzd2fromzd2procedurezb0zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3492, obj_t BgL_procz00_3493, obj_t BgL_thunkz00_3494)
	{
		{	/* Ieee/port.scm 748 */
			{	/* Ieee/port.scm 749 */
				obj_t BgL_auxz00_4733;
				obj_t BgL_auxz00_4726;

				if (PROCEDUREP(BgL_thunkz00_3494))
					{	/* Ieee/port.scm 749 */
						BgL_auxz00_4733 = BgL_thunkz00_3494;
					}
				else
					{
						obj_t BgL_auxz00_4736;

						BgL_auxz00_4736 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(33934L),
							BGl_string2424z00zz__r4_ports_6_10_1z00,
							BGl_string2405z00zz__r4_ports_6_10_1z00, BgL_thunkz00_3494);
						FAILURE(BgL_auxz00_4736, BFALSE, BFALSE);
					}
				if (PROCEDUREP(BgL_procz00_3493))
					{	/* Ieee/port.scm 749 */
						BgL_auxz00_4726 = BgL_procz00_3493;
					}
				else
					{
						obj_t BgL_auxz00_4729;

						BgL_auxz00_4729 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(33934L),
							BGl_string2424z00zz__r4_ports_6_10_1z00,
							BGl_string2405z00zz__r4_ports_6_10_1z00, BgL_procz00_3493);
						FAILURE(BgL_auxz00_4729, BFALSE, BFALSE);
					}
				return
					BGl_withzd2inputzd2fromzd2procedurezd2zz__r4_ports_6_10_1z00
					(BgL_auxz00_4726, BgL_auxz00_4733);
			}
		}

	}



/* &<@anonymous:1451> */
	obj_t BGl_z62zc3z04anonymousza31451ze3ze5zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3495)
	{
		{	/* Ieee/port.scm 753 */
			{	/* Ieee/port.scm 758 */
				obj_t BgL_denvz00_3496;
				obj_t BgL_oldzd2inputzd2portz00_3497;
				obj_t BgL_portz00_3498;

				BgL_denvz00_3496 = ((obj_t) PROCEDURE_REF(BgL_envz00_3495, (int) (0L)));
				BgL_oldzd2inputzd2portz00_3497 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3495, (int) (1L)));
				BgL_portz00_3498 = ((obj_t) PROCEDURE_REF(BgL_envz00_3495, (int) (2L)));
				BGL_ENV_CURRENT_INPUT_PORT_SET(BgL_denvz00_3496,
					BgL_oldzd2inputzd2portz00_3497);
				BUNSPEC;
				return bgl_close_input_port(BgL_portz00_3498);
			}
		}

	}



/* with-output-to-file */
	BGL_EXPORTED_DEF obj_t
		BGl_withzd2outputzd2tozd2filezd2zz__r4_ports_6_10_1z00(obj_t
		BgL_stringz00_31, obj_t BgL_thunkz00_32)
	{
		{	/* Ieee/port.scm 765 */
			{	/* Ieee/port.scm 766 */
				obj_t BgL_portz00_1520;

				{	/* Ieee/port.scm 483 */

					BgL_portz00_1520 =
						bgl_open_output_file(BgL_stringz00_31,
						BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00
						(BGl_string2407z00zz__r4_ports_6_10_1z00, BTRUE,
							(int) (default_io_bufsiz)));
				}
				if (OUTPUT_PORTP(BgL_portz00_1520))
					{	/* Ieee/port.scm 768 */
						obj_t BgL_denvz00_1522;

						BgL_denvz00_1522 = BGL_CURRENT_DYNAMIC_ENV();
						{	/* Ieee/port.scm 768 */
							obj_t BgL_oldzd2outputzd2portz00_1523;

							BgL_oldzd2outputzd2portz00_1523 =
								BGL_ENV_CURRENT_OUTPUT_PORT(BgL_denvz00_1522);
							{	/* Ieee/port.scm 769 */

								{	/* Ieee/port.scm 770 */
									obj_t BgL_exitd1075z00_1524;

									BgL_exitd1075z00_1524 = BGL_EXITD_TOP_AS_OBJ();
									{	/* Ieee/port.scm 775 */
										obj_t BgL_zc3z04anonymousza31453ze3z87_3499;

										BgL_zc3z04anonymousza31453ze3z87_3499 =
											MAKE_FX_PROCEDURE
											(BGl_z62zc3z04anonymousza31453ze3ze5zz__r4_ports_6_10_1z00,
											(int) (0L), (int) (3L));
										PROCEDURE_SET(BgL_zc3z04anonymousza31453ze3z87_3499,
											(int) (0L), BgL_denvz00_1522);
										PROCEDURE_SET(BgL_zc3z04anonymousza31453ze3z87_3499,
											(int) (1L), BgL_oldzd2outputzd2portz00_1523);
										PROCEDURE_SET(BgL_zc3z04anonymousza31453ze3z87_3499,
											(int) (2L), BgL_portz00_1520);
										{	/* Ieee/port.scm 770 */
											obj_t BgL_arg2014z00_2594;

											{	/* Ieee/port.scm 770 */
												obj_t BgL_arg2015z00_2595;

												BgL_arg2015z00_2595 =
													BGL_EXITD_PROTECT(BgL_exitd1075z00_1524);
												BgL_arg2014z00_2594 =
													MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31453ze3z87_3499,
													BgL_arg2015z00_2595);
											}
											BGL_EXITD_PROTECT_SET(BgL_exitd1075z00_1524,
												BgL_arg2014z00_2594);
											BUNSPEC;
										}
										{	/* Ieee/port.scm 772 */
											obj_t BgL_tmp1077z00_1526;

											BGL_ENV_CURRENT_OUTPUT_PORT_SET(BgL_denvz00_1522,
												BgL_portz00_1520);
											BUNSPEC;
											BgL_tmp1077z00_1526 =
												BGL_PROCEDURE_CALL0(BgL_thunkz00_32);
											{	/* Ieee/port.scm 770 */
												bool_t BgL_test2951z00_4776;

												{	/* Ieee/port.scm 770 */
													obj_t BgL_arg2013z00_2597;

													BgL_arg2013z00_2597 =
														BGL_EXITD_PROTECT(BgL_exitd1075z00_1524);
													BgL_test2951z00_4776 = PAIRP(BgL_arg2013z00_2597);
												}
												if (BgL_test2951z00_4776)
													{	/* Ieee/port.scm 770 */
														obj_t BgL_arg2011z00_2598;

														{	/* Ieee/port.scm 770 */
															obj_t BgL_arg2012z00_2599;

															BgL_arg2012z00_2599 =
																BGL_EXITD_PROTECT(BgL_exitd1075z00_1524);
															BgL_arg2011z00_2598 =
																CDR(((obj_t) BgL_arg2012z00_2599));
														}
														BGL_EXITD_PROTECT_SET(BgL_exitd1075z00_1524,
															BgL_arg2011z00_2598);
														BUNSPEC;
													}
												else
													{	/* Ieee/port.scm 770 */
														BFALSE;
													}
											}
											BGL_ENV_CURRENT_OUTPUT_PORT_SET(BgL_denvz00_1522,
												BgL_oldzd2outputzd2portz00_1523);
											BUNSPEC;
											bgl_close_output_port(BgL_portz00_1520);
											return BgL_tmp1077z00_1526;
										}
									}
								}
							}
						}
					}
				else
					{	/* Ieee/port.scm 767 */
						return
							bgl_system_failure(BGL_IO_PORT_ERROR,
							BGl_string2425z00zz__r4_ports_6_10_1z00,
							BGl_string2401z00zz__r4_ports_6_10_1z00, BgL_stringz00_31);
					}
			}
		}

	}



/* &with-output-to-file */
	obj_t BGl_z62withzd2outputzd2tozd2filezb0zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3500, obj_t BgL_stringz00_3501, obj_t BgL_thunkz00_3502)
	{
		{	/* Ieee/port.scm 765 */
			{	/* Ieee/port.scm 766 */
				obj_t BgL_auxz00_4793;
				obj_t BgL_auxz00_4786;

				if (PROCEDUREP(BgL_thunkz00_3502))
					{	/* Ieee/port.scm 766 */
						BgL_auxz00_4793 = BgL_thunkz00_3502;
					}
				else
					{
						obj_t BgL_auxz00_4796;

						BgL_auxz00_4796 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(34615L),
							BGl_string2426z00zz__r4_ports_6_10_1z00,
							BGl_string2405z00zz__r4_ports_6_10_1z00, BgL_thunkz00_3502);
						FAILURE(BgL_auxz00_4796, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_stringz00_3501))
					{	/* Ieee/port.scm 766 */
						BgL_auxz00_4786 = BgL_stringz00_3501;
					}
				else
					{
						obj_t BgL_auxz00_4789;

						BgL_auxz00_4789 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(34615L),
							BGl_string2426z00zz__r4_ports_6_10_1z00,
							BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_stringz00_3501);
						FAILURE(BgL_auxz00_4789, BFALSE, BFALSE);
					}
				return
					BGl_withzd2outputzd2tozd2filezd2zz__r4_ports_6_10_1z00
					(BgL_auxz00_4786, BgL_auxz00_4793);
			}
		}

	}



/* &<@anonymous:1453> */
	obj_t BGl_z62zc3z04anonymousza31453ze3ze5zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3503)
	{
		{	/* Ieee/port.scm 770 */
			{	/* Ieee/port.scm 775 */
				obj_t BgL_denvz00_3504;
				obj_t BgL_oldzd2outputzd2portz00_3505;
				obj_t BgL_portz00_3506;

				BgL_denvz00_3504 = ((obj_t) PROCEDURE_REF(BgL_envz00_3503, (int) (0L)));
				BgL_oldzd2outputzd2portz00_3505 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3503, (int) (1L)));
				BgL_portz00_3506 = PROCEDURE_REF(BgL_envz00_3503, (int) (2L));
				BGL_ENV_CURRENT_OUTPUT_PORT_SET(BgL_denvz00_3504,
					BgL_oldzd2outputzd2portz00_3505);
				BUNSPEC;
				return bgl_close_output_port(((obj_t) BgL_portz00_3506));
			}
		}

	}



/* with-append-to-file */
	BGL_EXPORTED_DEF obj_t
		BGl_withzd2appendzd2tozd2filezd2zz__r4_ports_6_10_1z00(obj_t
		BgL_stringz00_33, obj_t BgL_thunkz00_34)
	{
		{	/* Ieee/port.scm 783 */
			{	/* Ieee/port.scm 784 */
				obj_t BgL_portz00_1531;

				{	/* Ieee/port.scm 484 */

					BgL_portz00_1531 =
						bgl_append_output_file(BgL_stringz00_33,
						BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00
						(BGl_string2410z00zz__r4_ports_6_10_1z00, BTRUE,
							(int) (default_io_bufsiz)));
				}
				if (OUTPUT_PORTP(BgL_portz00_1531))
					{	/* Ieee/port.scm 786 */
						obj_t BgL_denvz00_1533;

						BgL_denvz00_1533 = BGL_CURRENT_DYNAMIC_ENV();
						{	/* Ieee/port.scm 786 */
							obj_t BgL_oldzd2outputzd2portz00_1534;

							BgL_oldzd2outputzd2portz00_1534 =
								BGL_ENV_CURRENT_OUTPUT_PORT(BgL_denvz00_1533);
							{	/* Ieee/port.scm 787 */

								{	/* Ieee/port.scm 788 */
									obj_t BgL_exitd1079z00_1535;

									BgL_exitd1079z00_1535 = BGL_EXITD_TOP_AS_OBJ();
									{	/* Ieee/port.scm 793 */
										obj_t BgL_zc3z04anonymousza31455ze3z87_3507;

										BgL_zc3z04anonymousza31455ze3z87_3507 =
											MAKE_FX_PROCEDURE
											(BGl_z62zc3z04anonymousza31455ze3ze5zz__r4_ports_6_10_1z00,
											(int) (0L), (int) (3L));
										PROCEDURE_SET(BgL_zc3z04anonymousza31455ze3z87_3507,
											(int) (0L), BgL_denvz00_1533);
										PROCEDURE_SET(BgL_zc3z04anonymousza31455ze3z87_3507,
											(int) (1L), BgL_oldzd2outputzd2portz00_1534);
										PROCEDURE_SET(BgL_zc3z04anonymousza31455ze3z87_3507,
											(int) (2L), BgL_portz00_1531);
										{	/* Ieee/port.scm 788 */
											obj_t BgL_arg2014z00_2605;

											{	/* Ieee/port.scm 788 */
												obj_t BgL_arg2015z00_2606;

												BgL_arg2015z00_2606 =
													BGL_EXITD_PROTECT(BgL_exitd1079z00_1535);
												BgL_arg2014z00_2605 =
													MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31455ze3z87_3507,
													BgL_arg2015z00_2606);
											}
											BGL_EXITD_PROTECT_SET(BgL_exitd1079z00_1535,
												BgL_arg2014z00_2605);
											BUNSPEC;
										}
										{	/* Ieee/port.scm 790 */
											obj_t BgL_tmp1081z00_1537;

											BGL_ENV_CURRENT_OUTPUT_PORT_SET(BgL_denvz00_1533,
												BgL_portz00_1531);
											BUNSPEC;
											BgL_tmp1081z00_1537 =
												BGL_PROCEDURE_CALL0(BgL_thunkz00_34);
											{	/* Ieee/port.scm 788 */
												bool_t BgL_test2955z00_4836;

												{	/* Ieee/port.scm 788 */
													obj_t BgL_arg2013z00_2608;

													BgL_arg2013z00_2608 =
														BGL_EXITD_PROTECT(BgL_exitd1079z00_1535);
													BgL_test2955z00_4836 = PAIRP(BgL_arg2013z00_2608);
												}
												if (BgL_test2955z00_4836)
													{	/* Ieee/port.scm 788 */
														obj_t BgL_arg2011z00_2609;

														{	/* Ieee/port.scm 788 */
															obj_t BgL_arg2012z00_2610;

															BgL_arg2012z00_2610 =
																BGL_EXITD_PROTECT(BgL_exitd1079z00_1535);
															BgL_arg2011z00_2609 =
																CDR(((obj_t) BgL_arg2012z00_2610));
														}
														BGL_EXITD_PROTECT_SET(BgL_exitd1079z00_1535,
															BgL_arg2011z00_2609);
														BUNSPEC;
													}
												else
													{	/* Ieee/port.scm 788 */
														BFALSE;
													}
											}
											BGL_ENV_CURRENT_OUTPUT_PORT_SET(BgL_denvz00_1533,
												BgL_oldzd2outputzd2portz00_1534);
											BUNSPEC;
											bgl_close_output_port(BgL_portz00_1531);
											return BgL_tmp1081z00_1537;
										}
									}
								}
							}
						}
					}
				else
					{	/* Ieee/port.scm 785 */
						return
							bgl_system_failure(BGL_IO_PORT_ERROR,
							BGl_string2425z00zz__r4_ports_6_10_1z00,
							BGl_string2401z00zz__r4_ports_6_10_1z00, BgL_stringz00_33);
					}
			}
		}

	}



/* &with-append-to-file */
	obj_t BGl_z62withzd2appendzd2tozd2filezb0zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3508, obj_t BgL_stringz00_3509, obj_t BgL_thunkz00_3510)
	{
		{	/* Ieee/port.scm 783 */
			{	/* Ieee/port.scm 784 */
				obj_t BgL_auxz00_4853;
				obj_t BgL_auxz00_4846;

				if (PROCEDUREP(BgL_thunkz00_3510))
					{	/* Ieee/port.scm 784 */
						BgL_auxz00_4853 = BgL_thunkz00_3510;
					}
				else
					{
						obj_t BgL_auxz00_4856;

						BgL_auxz00_4856 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(35325L),
							BGl_string2427z00zz__r4_ports_6_10_1z00,
							BGl_string2405z00zz__r4_ports_6_10_1z00, BgL_thunkz00_3510);
						FAILURE(BgL_auxz00_4856, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_stringz00_3509))
					{	/* Ieee/port.scm 784 */
						BgL_auxz00_4846 = BgL_stringz00_3509;
					}
				else
					{
						obj_t BgL_auxz00_4849;

						BgL_auxz00_4849 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(35325L),
							BGl_string2427z00zz__r4_ports_6_10_1z00,
							BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_stringz00_3509);
						FAILURE(BgL_auxz00_4849, BFALSE, BFALSE);
					}
				return
					BGl_withzd2appendzd2tozd2filezd2zz__r4_ports_6_10_1z00
					(BgL_auxz00_4846, BgL_auxz00_4853);
			}
		}

	}



/* &<@anonymous:1455> */
	obj_t BGl_z62zc3z04anonymousza31455ze3ze5zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3511)
	{
		{	/* Ieee/port.scm 788 */
			{	/* Ieee/port.scm 793 */
				obj_t BgL_denvz00_3512;
				obj_t BgL_oldzd2outputzd2portz00_3513;
				obj_t BgL_portz00_3514;

				BgL_denvz00_3512 = ((obj_t) PROCEDURE_REF(BgL_envz00_3511, (int) (0L)));
				BgL_oldzd2outputzd2portz00_3513 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3511, (int) (1L)));
				BgL_portz00_3514 = PROCEDURE_REF(BgL_envz00_3511, (int) (2L));
				BGL_ENV_CURRENT_OUTPUT_PORT_SET(BgL_denvz00_3512,
					BgL_oldzd2outputzd2portz00_3513);
				BUNSPEC;
				return bgl_close_output_port(((obj_t) BgL_portz00_3514));
			}
		}

	}



/* with-output-to-port */
	BGL_EXPORTED_DEF obj_t
		BGl_withzd2outputzd2tozd2portzd2zz__r4_ports_6_10_1z00(obj_t BgL_portz00_35,
		obj_t BgL_thunkz00_36)
	{
		{	/* Ieee/port.scm 801 */
			{	/* Ieee/port.scm 802 */
				obj_t BgL_denvz00_1542;

				BgL_denvz00_1542 = BGL_CURRENT_DYNAMIC_ENV();
				{	/* Ieee/port.scm 802 */
					obj_t BgL_oldzd2outputzd2portz00_1543;

					BgL_oldzd2outputzd2portz00_1543 =
						BGL_ENV_CURRENT_OUTPUT_PORT(BgL_denvz00_1542);
					{	/* Ieee/port.scm 803 */

						{	/* Ieee/port.scm 804 */
							obj_t BgL_exitd1083z00_1544;

							BgL_exitd1083z00_1544 = BGL_EXITD_TOP_AS_OBJ();
							{
								obj_t BgL_zc3z04anonymousza31456ze3z87_3515;

								BgL_zc3z04anonymousza31456ze3z87_3515 =
									MAKE_FX_PROCEDURE
									(BGl_z62zc3z04anonymousza31456ze3ze5zz__r4_ports_6_10_1z00,
									(int) (0L), (int) (2L));
								PROCEDURE_SET(BgL_zc3z04anonymousza31456ze3z87_3515, (int) (0L),
									BgL_denvz00_1542);
								PROCEDURE_SET(BgL_zc3z04anonymousza31456ze3z87_3515, (int) (1L),
									BgL_oldzd2outputzd2portz00_1543);
								{	/* Ieee/port.scm 804 */
									obj_t BgL_arg2014z00_2613;

									{	/* Ieee/port.scm 804 */
										obj_t BgL_arg2015z00_2614;

										BgL_arg2015z00_2614 =
											BGL_EXITD_PROTECT(BgL_exitd1083z00_1544);
										BgL_arg2014z00_2613 =
											MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31456ze3z87_3515,
											BgL_arg2015z00_2614);
									}
									BGL_EXITD_PROTECT_SET(BgL_exitd1083z00_1544,
										BgL_arg2014z00_2613);
									BUNSPEC;
								}
								{	/* Ieee/port.scm 806 */
									obj_t BgL_tmp1085z00_1546;

									BGL_ENV_CURRENT_OUTPUT_PORT_SET(BgL_denvz00_1542,
										BgL_portz00_35);
									BUNSPEC;
									BgL_tmp1085z00_1546 = BGL_PROCEDURE_CALL0(BgL_thunkz00_36);
									{	/* Ieee/port.scm 804 */
										bool_t BgL_test2958z00_4889;

										{	/* Ieee/port.scm 804 */
											obj_t BgL_arg2013z00_2616;

											BgL_arg2013z00_2616 =
												BGL_EXITD_PROTECT(BgL_exitd1083z00_1544);
											BgL_test2958z00_4889 = PAIRP(BgL_arg2013z00_2616);
										}
										if (BgL_test2958z00_4889)
											{	/* Ieee/port.scm 804 */
												obj_t BgL_arg2011z00_2617;

												{	/* Ieee/port.scm 804 */
													obj_t BgL_arg2012z00_2618;

													BgL_arg2012z00_2618 =
														BGL_EXITD_PROTECT(BgL_exitd1083z00_1544);
													BgL_arg2011z00_2617 =
														CDR(((obj_t) BgL_arg2012z00_2618));
												}
												BGL_EXITD_PROTECT_SET(BgL_exitd1083z00_1544,
													BgL_arg2011z00_2617);
												BUNSPEC;
											}
										else
											{	/* Ieee/port.scm 804 */
												BFALSE;
											}
									}
									BGL_ENV_CURRENT_OUTPUT_PORT_SET(BgL_denvz00_1542,
										BgL_oldzd2outputzd2portz00_1543);
									BUNSPEC;
									return BgL_tmp1085z00_1546;
								}
							}
						}
					}
				}
			}
		}

	}



/* &with-output-to-port */
	obj_t BGl_z62withzd2outputzd2tozd2portzb0zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3516, obj_t BgL_portz00_3517, obj_t BgL_thunkz00_3518)
	{
		{	/* Ieee/port.scm 801 */
			{	/* Ieee/port.scm 802 */
				obj_t BgL_auxz00_4904;
				obj_t BgL_auxz00_4897;

				if (PROCEDUREP(BgL_thunkz00_3518))
					{	/* Ieee/port.scm 802 */
						BgL_auxz00_4904 = BgL_thunkz00_3518;
					}
				else
					{
						obj_t BgL_auxz00_4907;

						BgL_auxz00_4907 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(36035L),
							BGl_string2428z00zz__r4_ports_6_10_1z00,
							BGl_string2405z00zz__r4_ports_6_10_1z00, BgL_thunkz00_3518);
						FAILURE(BgL_auxz00_4907, BFALSE, BFALSE);
					}
				if (OUTPUT_PORTP(BgL_portz00_3517))
					{	/* Ieee/port.scm 802 */
						BgL_auxz00_4897 = BgL_portz00_3517;
					}
				else
					{
						obj_t BgL_auxz00_4900;

						BgL_auxz00_4900 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(36035L),
							BGl_string2428z00zz__r4_ports_6_10_1z00,
							BGl_string2429z00zz__r4_ports_6_10_1z00, BgL_portz00_3517);
						FAILURE(BgL_auxz00_4900, BFALSE, BFALSE);
					}
				return
					BGl_withzd2outputzd2tozd2portzd2zz__r4_ports_6_10_1z00
					(BgL_auxz00_4897, BgL_auxz00_4904);
			}
		}

	}



/* &<@anonymous:1456> */
	obj_t BGl_z62zc3z04anonymousza31456ze3ze5zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3519)
	{
		{	/* Ieee/port.scm 804 */
			{
				obj_t BgL_denvz00_3520;
				obj_t BgL_oldzd2outputzd2portz00_3521;

				BgL_denvz00_3520 = ((obj_t) PROCEDURE_REF(BgL_envz00_3519, (int) (0L)));
				BgL_oldzd2outputzd2portz00_3521 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3519, (int) (1L)));
				BGL_ENV_CURRENT_OUTPUT_PORT_SET(BgL_denvz00_3520,
					BgL_oldzd2outputzd2portz00_3521);
				return BUNSPEC;
			}
		}

	}



/* with-output-to-string */
	BGL_EXPORTED_DEF obj_t
		BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00(obj_t
		BgL_thunkz00_37)
	{
		{	/* Ieee/port.scm 813 */
			{	/* Ieee/port.scm 814 */
				obj_t BgL_portz00_1549;

				{	/* Ieee/port.scm 814 */

					{	/* Ieee/port.scm 485 */

						BgL_portz00_1549 =
							bgl_open_output_string
							(BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00
							(BGl_string2407z00zz__r4_ports_6_10_1z00, BTRUE, (int) (128L)));
				}}
				{	/* Ieee/port.scm 814 */
					obj_t BgL_denvz00_1550;

					BgL_denvz00_1550 = BGL_CURRENT_DYNAMIC_ENV();
					{	/* Ieee/port.scm 815 */
						obj_t BgL_oldzd2outputzd2portz00_1551;

						BgL_oldzd2outputzd2portz00_1551 =
							BGL_ENV_CURRENT_OUTPUT_PORT(BgL_denvz00_1550);
						{	/* Ieee/port.scm 816 */
							obj_t BgL_resz00_3531;

							BgL_resz00_3531 = MAKE_CELL(BUNSPEC);
							{	/* Ieee/port.scm 817 */

								{	/* Ieee/port.scm 818 */
									obj_t BgL_exitd1087z00_1553;

									BgL_exitd1087z00_1553 = BGL_EXITD_TOP_AS_OBJ();
									{	/* Ieee/port.scm 823 */
										obj_t BgL_zc3z04anonymousza31457ze3z87_3522;

										BgL_zc3z04anonymousza31457ze3z87_3522 =
											MAKE_FX_PROCEDURE
											(BGl_z62zc3z04anonymousza31457ze3ze5zz__r4_ports_6_10_1z00,
											(int) (0L), (int) (4L));
										PROCEDURE_SET(BgL_zc3z04anonymousza31457ze3z87_3522,
											(int) (0L), BgL_denvz00_1550);
										PROCEDURE_SET(BgL_zc3z04anonymousza31457ze3z87_3522,
											(int) (1L), BgL_oldzd2outputzd2portz00_1551);
										PROCEDURE_SET(BgL_zc3z04anonymousza31457ze3z87_3522,
											(int) (2L), BgL_portz00_1549);
										PROCEDURE_SET(BgL_zc3z04anonymousza31457ze3z87_3522,
											(int) (3L), ((obj_t) BgL_resz00_3531));
										{	/* Ieee/port.scm 818 */
											obj_t BgL_arg2014z00_2622;

											{	/* Ieee/port.scm 818 */
												obj_t BgL_arg2015z00_2623;

												BgL_arg2015z00_2623 =
													BGL_EXITD_PROTECT(BgL_exitd1087z00_1553);
												BgL_arg2014z00_2622 =
													MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31457ze3z87_3522,
													BgL_arg2015z00_2623);
											}
											BGL_EXITD_PROTECT_SET(BgL_exitd1087z00_1553,
												BgL_arg2014z00_2622);
											BUNSPEC;
										}
										{	/* Ieee/port.scm 820 */
											obj_t BgL_tmp1089z00_1555;

											BGL_ENV_CURRENT_OUTPUT_PORT_SET(BgL_denvz00_1550,
												BgL_portz00_1549);
											BUNSPEC;
											BgL_tmp1089z00_1555 =
												BGL_PROCEDURE_CALL0(BgL_thunkz00_37);
											{	/* Ieee/port.scm 818 */
												bool_t BgL_test2961z00_4944;

												{	/* Ieee/port.scm 818 */
													obj_t BgL_arg2013z00_2625;

													BgL_arg2013z00_2625 =
														BGL_EXITD_PROTECT(BgL_exitd1087z00_1553);
													BgL_test2961z00_4944 = PAIRP(BgL_arg2013z00_2625);
												}
												if (BgL_test2961z00_4944)
													{	/* Ieee/port.scm 818 */
														obj_t BgL_arg2011z00_2626;

														{	/* Ieee/port.scm 818 */
															obj_t BgL_arg2012z00_2627;

															BgL_arg2012z00_2627 =
																BGL_EXITD_PROTECT(BgL_exitd1087z00_1553);
															BgL_arg2011z00_2626 =
																CDR(((obj_t) BgL_arg2012z00_2627));
														}
														BGL_EXITD_PROTECT_SET(BgL_exitd1087z00_1553,
															BgL_arg2011z00_2626);
														BUNSPEC;
													}
												else
													{	/* Ieee/port.scm 818 */
														BFALSE;
													}
											}
											BGl_z62zc3z04anonymousza31457ze3ze5zz__r4_ports_6_10_1z00
												(BgL_zc3z04anonymousza31457ze3z87_3522);
											BgL_tmp1089z00_1555;
										}
									}
								}
								return CELL_REF(BgL_resz00_3531);
							}
						}
					}
				}
			}
		}

	}



/* &with-output-to-string */
	obj_t BGl_z62withzd2outputzd2tozd2stringzb0zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3523, obj_t BgL_thunkz00_3524)
	{
		{	/* Ieee/port.scm 813 */
			{	/* Ieee/port.scm 814 */
				obj_t BgL_auxz00_4952;

				if (PROCEDUREP(BgL_thunkz00_3524))
					{	/* Ieee/port.scm 814 */
						BgL_auxz00_4952 = BgL_thunkz00_3524;
					}
				else
					{
						obj_t BgL_auxz00_4955;

						BgL_auxz00_4955 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(36531L),
							BGl_string2430z00zz__r4_ports_6_10_1z00,
							BGl_string2405z00zz__r4_ports_6_10_1z00, BgL_thunkz00_3524);
						FAILURE(BgL_auxz00_4955, BFALSE, BFALSE);
					}
				return
					BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00
					(BgL_auxz00_4952);
			}
		}

	}



/* &<@anonymous:1457> */
	obj_t BGl_z62zc3z04anonymousza31457ze3ze5zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3525)
	{
		{	/* Ieee/port.scm 818 */
			{	/* Ieee/port.scm 823 */
				obj_t BgL_denvz00_3526;
				obj_t BgL_oldzd2outputzd2portz00_3527;
				obj_t BgL_portz00_3528;
				obj_t BgL_resz00_3529;

				BgL_denvz00_3526 = ((obj_t) PROCEDURE_REF(BgL_envz00_3525, (int) (0L)));
				BgL_oldzd2outputzd2portz00_3527 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3525, (int) (1L)));
				BgL_portz00_3528 = ((obj_t) PROCEDURE_REF(BgL_envz00_3525, (int) (2L)));
				BgL_resz00_3529 = PROCEDURE_REF(BgL_envz00_3525, (int) (3L));
				BGL_ENV_CURRENT_OUTPUT_PORT_SET(BgL_denvz00_3526,
					BgL_oldzd2outputzd2portz00_3527);
				BUNSPEC;
				{	/* Ieee/port.scm 824 */
					obj_t BgL_auxz00_4163;

					BgL_auxz00_4163 = bgl_close_output_port(BgL_portz00_3528);
					return CELL_SET(BgL_resz00_3529, BgL_auxz00_4163);
				}
			}
		}

	}



/* with-output-to-procedure */
	BGL_EXPORTED_DEF obj_t
		BGl_withzd2outputzd2tozd2procedurezd2zz__r4_ports_6_10_1z00(obj_t
		BgL_procz00_38, obj_t BgL_thunkz00_39)
	{
		{	/* Ieee/port.scm 830 */
			{	/* Ieee/port.scm 831 */
				obj_t BgL_portz00_1559;

				{	/* Ieee/port.scm 486 */

					BgL_portz00_1559 =
						BGl_openzd2outputzd2procedurez00zz__r4_ports_6_10_1z00
						(BgL_procz00_38, BGl_proc2431z00zz__r4_ports_6_10_1z00, BTRUE,
						BGl_proc2432z00zz__r4_ports_6_10_1z00);
				}
				{	/* Ieee/port.scm 831 */
					obj_t BgL_denvz00_1560;

					BgL_denvz00_1560 = BGL_CURRENT_DYNAMIC_ENV();
					{	/* Ieee/port.scm 832 */
						obj_t BgL_oldzd2outputzd2portz00_1561;

						BgL_oldzd2outputzd2portz00_1561 =
							BGL_ENV_CURRENT_OUTPUT_PORT(BgL_denvz00_1560);
						{	/* Ieee/port.scm 833 */
							obj_t BgL_resz00_3547;

							BgL_resz00_3547 = MAKE_CELL(BUNSPEC);
							{	/* Ieee/port.scm 834 */

								{	/* Ieee/port.scm 835 */
									obj_t BgL_exitd1091z00_1563;

									BgL_exitd1091z00_1563 = BGL_EXITD_TOP_AS_OBJ();
									{	/* Ieee/port.scm 840 */
										obj_t BgL_zc3z04anonymousza31458ze3z87_3533;

										BgL_zc3z04anonymousza31458ze3z87_3533 =
											MAKE_FX_PROCEDURE
											(BGl_z62zc3z04anonymousza31458ze3ze5zz__r4_ports_6_10_1z00,
											(int) (0L), (int) (4L));
										PROCEDURE_SET(BgL_zc3z04anonymousza31458ze3z87_3533,
											(int) (0L), BgL_denvz00_1560);
										PROCEDURE_SET(BgL_zc3z04anonymousza31458ze3z87_3533,
											(int) (1L), BgL_oldzd2outputzd2portz00_1561);
										PROCEDURE_SET(BgL_zc3z04anonymousza31458ze3z87_3533,
											(int) (2L), BgL_portz00_1559);
										PROCEDURE_SET(BgL_zc3z04anonymousza31458ze3z87_3533,
											(int) (3L), ((obj_t) BgL_resz00_3547));
										{	/* Ieee/port.scm 835 */
											obj_t BgL_arg2014z00_2630;

											{	/* Ieee/port.scm 835 */
												obj_t BgL_arg2015z00_2631;

												BgL_arg2015z00_2631 =
													BGL_EXITD_PROTECT(BgL_exitd1091z00_1563);
												BgL_arg2014z00_2630 =
													MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31458ze3z87_3533,
													BgL_arg2015z00_2631);
											}
											BGL_EXITD_PROTECT_SET(BgL_exitd1091z00_1563,
												BgL_arg2014z00_2630);
											BUNSPEC;
										}
										{	/* Ieee/port.scm 837 */
											obj_t BgL_tmp1093z00_1565;

											BGL_ENV_CURRENT_OUTPUT_PORT_SET(BgL_denvz00_1560,
												BgL_portz00_1559);
											BUNSPEC;
											BgL_tmp1093z00_1565 =
												BGL_PROCEDURE_CALL0(BgL_thunkz00_39);
											{	/* Ieee/port.scm 835 */
												bool_t BgL_test2963z00_4996;

												{	/* Ieee/port.scm 835 */
													obj_t BgL_arg2013z00_2633;

													BgL_arg2013z00_2633 =
														BGL_EXITD_PROTECT(BgL_exitd1091z00_1563);
													BgL_test2963z00_4996 = PAIRP(BgL_arg2013z00_2633);
												}
												if (BgL_test2963z00_4996)
													{	/* Ieee/port.scm 835 */
														obj_t BgL_arg2011z00_2634;

														{	/* Ieee/port.scm 835 */
															obj_t BgL_arg2012z00_2635;

															BgL_arg2012z00_2635 =
																BGL_EXITD_PROTECT(BgL_exitd1091z00_1563);
															BgL_arg2011z00_2634 =
																CDR(((obj_t) BgL_arg2012z00_2635));
														}
														BGL_EXITD_PROTECT_SET(BgL_exitd1091z00_1563,
															BgL_arg2011z00_2634);
														BUNSPEC;
													}
												else
													{	/* Ieee/port.scm 835 */
														BFALSE;
													}
											}
											BGl_z62zc3z04anonymousza31458ze3ze5zz__r4_ports_6_10_1z00
												(BgL_zc3z04anonymousza31458ze3z87_3533);
											BgL_tmp1093z00_1565;
										}
									}
								}
								return CELL_REF(BgL_resz00_3547);
							}
						}
					}
				}
			}
		}

	}



/* &with-output-to-procedure */
	obj_t BGl_z62withzd2outputzd2tozd2procedurezb0zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3536, obj_t BgL_procz00_3537, obj_t BgL_thunkz00_3538)
	{
		{	/* Ieee/port.scm 830 */
			{	/* Ieee/port.scm 831 */
				obj_t BgL_auxz00_5011;
				obj_t BgL_auxz00_5004;

				if (PROCEDUREP(BgL_thunkz00_3538))
					{	/* Ieee/port.scm 831 */
						BgL_auxz00_5011 = BgL_thunkz00_3538;
					}
				else
					{
						obj_t BgL_auxz00_5014;

						BgL_auxz00_5014 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(37152L),
							BGl_string2433z00zz__r4_ports_6_10_1z00,
							BGl_string2405z00zz__r4_ports_6_10_1z00, BgL_thunkz00_3538);
						FAILURE(BgL_auxz00_5014, BFALSE, BFALSE);
					}
				if (PROCEDUREP(BgL_procz00_3537))
					{	/* Ieee/port.scm 831 */
						BgL_auxz00_5004 = BgL_procz00_3537;
					}
				else
					{
						obj_t BgL_auxz00_5007;

						BgL_auxz00_5007 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(37152L),
							BGl_string2433z00zz__r4_ports_6_10_1z00,
							BGl_string2405z00zz__r4_ports_6_10_1z00, BgL_procz00_3537);
						FAILURE(BgL_auxz00_5007, BFALSE, BFALSE);
					}
				return
					BGl_withzd2outputzd2tozd2procedurezd2zz__r4_ports_6_10_1z00
					(BgL_auxz00_5004, BgL_auxz00_5011);
			}
		}

	}



/* &<@anonymous:1458> */
	obj_t BGl_z62zc3z04anonymousza31458ze3ze5zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3539)
	{
		{	/* Ieee/port.scm 835 */
			{	/* Ieee/port.scm 840 */
				obj_t BgL_denvz00_3540;
				obj_t BgL_oldzd2outputzd2portz00_3541;
				obj_t BgL_portz00_3542;
				obj_t BgL_resz00_3543;

				BgL_denvz00_3540 = ((obj_t) PROCEDURE_REF(BgL_envz00_3539, (int) (0L)));
				BgL_oldzd2outputzd2portz00_3541 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3539, (int) (1L)));
				BgL_portz00_3542 = PROCEDURE_REF(BgL_envz00_3539, (int) (2L));
				BgL_resz00_3543 = PROCEDURE_REF(BgL_envz00_3539, (int) (3L));
				BGL_ENV_CURRENT_OUTPUT_PORT_SET(BgL_denvz00_3540,
					BgL_oldzd2outputzd2portz00_3541);
				BUNSPEC;
				{	/* Ieee/port.scm 841 */
					obj_t BgL_auxz00_4164;

					BgL_auxz00_4164 = bgl_close_output_port(((obj_t) BgL_portz00_3542));
					return CELL_SET(BgL_resz00_3543, BgL_auxz00_4164);
				}
			}
		}

	}



/* &close */
	obj_t BGl_z62closez62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3545)
	{
		{	/* Ieee/port.scm 486 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &flush */
	obj_t BGl_z62flushz62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3546)
	{
		{	/* Ieee/port.scm 486 */
			return BBOOL(((bool_t) 0));
		}

	}



/* with-error-to-string */
	BGL_EXPORTED_DEF obj_t
		BGl_withzd2errorzd2tozd2stringzd2zz__r4_ports_6_10_1z00(obj_t
		BgL_thunkz00_40)
	{
		{	/* Ieee/port.scm 847 */
			{	/* Ieee/port.scm 848 */
				obj_t BgL_portz00_1576;

				{	/* Ieee/port.scm 848 */

					{	/* Ieee/port.scm 485 */

						BgL_portz00_1576 =
							bgl_open_output_string
							(BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00
							(BGl_string2407z00zz__r4_ports_6_10_1z00, BTRUE, (int) (128L)));
				}}
				{	/* Ieee/port.scm 850 */
					obj_t BgL_denvz00_1578;

					BgL_denvz00_1578 = BGL_CURRENT_DYNAMIC_ENV();
					{	/* Ieee/port.scm 850 */
						obj_t BgL_oldzd2errorzd2portz00_1579;

						BgL_oldzd2errorzd2portz00_1579 =
							BGL_ENV_CURRENT_ERROR_PORT(BgL_denvz00_1578);
						{	/* Ieee/port.scm 851 */
							obj_t BgL_resz00_3558;

							BgL_resz00_3558 = MAKE_CELL(BUNSPEC);
							{	/* Ieee/port.scm 852 */

								{	/* Ieee/port.scm 853 */
									obj_t BgL_exitd1095z00_1581;

									BgL_exitd1095z00_1581 = BGL_EXITD_TOP_AS_OBJ();
									{	/* Ieee/port.scm 858 */
										obj_t BgL_zc3z04anonymousza31462ze3z87_3549;

										BgL_zc3z04anonymousza31462ze3z87_3549 =
											MAKE_FX_PROCEDURE
											(BGl_z62zc3z04anonymousza31462ze3ze5zz__r4_ports_6_10_1z00,
											(int) (0L), (int) (4L));
										PROCEDURE_SET(BgL_zc3z04anonymousza31462ze3z87_3549,
											(int) (0L), BgL_denvz00_1578);
										PROCEDURE_SET(BgL_zc3z04anonymousza31462ze3z87_3549,
											(int) (1L), BgL_oldzd2errorzd2portz00_1579);
										PROCEDURE_SET(BgL_zc3z04anonymousza31462ze3z87_3549,
											(int) (2L), BgL_portz00_1576);
										PROCEDURE_SET(BgL_zc3z04anonymousza31462ze3z87_3549,
											(int) (3L), ((obj_t) BgL_resz00_3558));
										{	/* Ieee/port.scm 853 */
											obj_t BgL_arg2014z00_2639;

											{	/* Ieee/port.scm 853 */
												obj_t BgL_arg2015z00_2640;

												BgL_arg2015z00_2640 =
													BGL_EXITD_PROTECT(BgL_exitd1095z00_1581);
												BgL_arg2014z00_2639 =
													MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31462ze3z87_3549,
													BgL_arg2015z00_2640);
											}
											BGL_EXITD_PROTECT_SET(BgL_exitd1095z00_1581,
												BgL_arg2014z00_2639);
											BUNSPEC;
										}
										{	/* Ieee/port.scm 855 */
											obj_t BgL_tmp1097z00_1583;

											BGL_ENV_CURRENT_ERROR_PORT_SET(BgL_denvz00_1578,
												BgL_portz00_1576);
											BUNSPEC;
											BgL_tmp1097z00_1583 =
												BGL_PROCEDURE_CALL0(BgL_thunkz00_40);
											{	/* Ieee/port.scm 853 */
												bool_t BgL_test2966z00_5059;

												{	/* Ieee/port.scm 853 */
													obj_t BgL_arg2013z00_2642;

													BgL_arg2013z00_2642 =
														BGL_EXITD_PROTECT(BgL_exitd1095z00_1581);
													BgL_test2966z00_5059 = PAIRP(BgL_arg2013z00_2642);
												}
												if (BgL_test2966z00_5059)
													{	/* Ieee/port.scm 853 */
														obj_t BgL_arg2011z00_2643;

														{	/* Ieee/port.scm 853 */
															obj_t BgL_arg2012z00_2644;

															BgL_arg2012z00_2644 =
																BGL_EXITD_PROTECT(BgL_exitd1095z00_1581);
															BgL_arg2011z00_2643 =
																CDR(((obj_t) BgL_arg2012z00_2644));
														}
														BGL_EXITD_PROTECT_SET(BgL_exitd1095z00_1581,
															BgL_arg2011z00_2643);
														BUNSPEC;
													}
												else
													{	/* Ieee/port.scm 853 */
														BFALSE;
													}
											}
											BGl_z62zc3z04anonymousza31462ze3ze5zz__r4_ports_6_10_1z00
												(BgL_zc3z04anonymousza31462ze3z87_3549);
											BgL_tmp1097z00_1583;
										}
									}
								}
								return CELL_REF(BgL_resz00_3558);
							}
						}
					}
				}
			}
		}

	}



/* &with-error-to-string */
	obj_t BGl_z62withzd2errorzd2tozd2stringzb0zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3550, obj_t BgL_thunkz00_3551)
	{
		{	/* Ieee/port.scm 847 */
			{	/* Ieee/port.scm 848 */
				obj_t BgL_auxz00_5067;

				if (PROCEDUREP(BgL_thunkz00_3551))
					{	/* Ieee/port.scm 848 */
						BgL_auxz00_5067 = BgL_thunkz00_3551;
					}
				else
					{
						obj_t BgL_auxz00_5070;

						BgL_auxz00_5070 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(37772L),
							BGl_string2434z00zz__r4_ports_6_10_1z00,
							BGl_string2405z00zz__r4_ports_6_10_1z00, BgL_thunkz00_3551);
						FAILURE(BgL_auxz00_5070, BFALSE, BFALSE);
					}
				return
					BGl_withzd2errorzd2tozd2stringzd2zz__r4_ports_6_10_1z00
					(BgL_auxz00_5067);
			}
		}

	}



/* &<@anonymous:1462> */
	obj_t BGl_z62zc3z04anonymousza31462ze3ze5zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3552)
	{
		{	/* Ieee/port.scm 853 */
			{	/* Ieee/port.scm 858 */
				obj_t BgL_denvz00_3553;
				obj_t BgL_oldzd2errorzd2portz00_3554;
				obj_t BgL_portz00_3555;
				obj_t BgL_resz00_3556;

				BgL_denvz00_3553 = ((obj_t) PROCEDURE_REF(BgL_envz00_3552, (int) (0L)));
				BgL_oldzd2errorzd2portz00_3554 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3552, (int) (1L)));
				BgL_portz00_3555 = ((obj_t) PROCEDURE_REF(BgL_envz00_3552, (int) (2L)));
				BgL_resz00_3556 = PROCEDURE_REF(BgL_envz00_3552, (int) (3L));
				BGL_ENV_CURRENT_ERROR_PORT_SET(BgL_denvz00_3553,
					BgL_oldzd2errorzd2portz00_3554);
				BUNSPEC;
				{	/* Ieee/port.scm 859 */
					obj_t BgL_auxz00_4165;

					BgL_auxz00_4165 = bgl_close_output_port(BgL_portz00_3555);
					return CELL_SET(BgL_resz00_3556, BgL_auxz00_4165);
				}
			}
		}

	}



/* with-error-to-file */
	BGL_EXPORTED_DEF obj_t
		BGl_withzd2errorzd2tozd2filezd2zz__r4_ports_6_10_1z00(obj_t
		BgL_stringz00_41, obj_t BgL_thunkz00_42)
	{
		{	/* Ieee/port.scm 869 */
			{	/* Ieee/port.scm 870 */
				obj_t BgL_portz00_1587;

				{	/* Ieee/port.scm 483 */

					BgL_portz00_1587 =
						bgl_open_output_file(BgL_stringz00_41,
						BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00
						(BGl_string2407z00zz__r4_ports_6_10_1z00, BTRUE,
							(int) (default_io_bufsiz)));
				}
				if (OUTPUT_PORTP(BgL_portz00_1587))
					{	/* Ieee/port.scm 872 */
						obj_t BgL_denvz00_1589;

						BgL_denvz00_1589 = BGL_CURRENT_DYNAMIC_ENV();
						{	/* Ieee/port.scm 872 */
							obj_t BgL_oldzd2outputzd2portz00_1590;

							BgL_oldzd2outputzd2portz00_1590 =
								BGL_ENV_CURRENT_ERROR_PORT(BgL_denvz00_1589);
							{	/* Ieee/port.scm 873 */

								{	/* Ieee/port.scm 874 */
									obj_t BgL_exitd1099z00_1591;

									BgL_exitd1099z00_1591 = BGL_EXITD_TOP_AS_OBJ();
									{	/* Ieee/port.scm 879 */
										obj_t BgL_zc3z04anonymousza31464ze3z87_3560;

										BgL_zc3z04anonymousza31464ze3z87_3560 =
											MAKE_FX_PROCEDURE
											(BGl_z62zc3z04anonymousza31464ze3ze5zz__r4_ports_6_10_1z00,
											(int) (0L), (int) (3L));
										PROCEDURE_SET(BgL_zc3z04anonymousza31464ze3z87_3560,
											(int) (0L), BgL_denvz00_1589);
										PROCEDURE_SET(BgL_zc3z04anonymousza31464ze3z87_3560,
											(int) (1L), BgL_oldzd2outputzd2portz00_1590);
										PROCEDURE_SET(BgL_zc3z04anonymousza31464ze3z87_3560,
											(int) (2L), BgL_portz00_1587);
										{	/* Ieee/port.scm 874 */
											obj_t BgL_arg2014z00_2649;

											{	/* Ieee/port.scm 874 */
												obj_t BgL_arg2015z00_2650;

												BgL_arg2015z00_2650 =
													BGL_EXITD_PROTECT(BgL_exitd1099z00_1591);
												BgL_arg2014z00_2649 =
													MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31464ze3z87_3560,
													BgL_arg2015z00_2650);
											}
											BGL_EXITD_PROTECT_SET(BgL_exitd1099z00_1591,
												BgL_arg2014z00_2649);
											BUNSPEC;
										}
										{	/* Ieee/port.scm 876 */
											obj_t BgL_tmp1101z00_1593;

											BGL_ENV_CURRENT_ERROR_PORT_SET(BgL_denvz00_1589,
												BgL_portz00_1587);
											BUNSPEC;
											BgL_tmp1101z00_1593 =
												BGL_PROCEDURE_CALL0(BgL_thunkz00_42);
											{	/* Ieee/port.scm 874 */
												bool_t BgL_test2969z00_5112;

												{	/* Ieee/port.scm 874 */
													obj_t BgL_arg2013z00_2652;

													BgL_arg2013z00_2652 =
														BGL_EXITD_PROTECT(BgL_exitd1099z00_1591);
													BgL_test2969z00_5112 = PAIRP(BgL_arg2013z00_2652);
												}
												if (BgL_test2969z00_5112)
													{	/* Ieee/port.scm 874 */
														obj_t BgL_arg2011z00_2653;

														{	/* Ieee/port.scm 874 */
															obj_t BgL_arg2012z00_2654;

															BgL_arg2012z00_2654 =
																BGL_EXITD_PROTECT(BgL_exitd1099z00_1591);
															BgL_arg2011z00_2653 =
																CDR(((obj_t) BgL_arg2012z00_2654));
														}
														BGL_EXITD_PROTECT_SET(BgL_exitd1099z00_1591,
															BgL_arg2011z00_2653);
														BUNSPEC;
													}
												else
													{	/* Ieee/port.scm 874 */
														BFALSE;
													}
											}
											BGL_ENV_CURRENT_ERROR_PORT_SET(BgL_denvz00_1589,
												BgL_oldzd2outputzd2portz00_1590);
											BUNSPEC;
											bgl_close_output_port(BgL_portz00_1587);
											return BgL_tmp1101z00_1593;
										}
									}
								}
							}
						}
					}
				else
					{	/* Ieee/port.scm 871 */
						return
							bgl_system_failure(BGL_IO_PORT_ERROR,
							BGl_symbol2435z00zz__r4_ports_6_10_1z00,
							BGl_string2401z00zz__r4_ports_6_10_1z00, BgL_stringz00_41);
					}
			}
		}

	}



/* &with-error-to-file */
	obj_t BGl_z62withzd2errorzd2tozd2filezb0zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3561, obj_t BgL_stringz00_3562, obj_t BgL_thunkz00_3563)
	{
		{	/* Ieee/port.scm 869 */
			{	/* Ieee/port.scm 870 */
				obj_t BgL_auxz00_5129;
				obj_t BgL_auxz00_5122;

				if (PROCEDUREP(BgL_thunkz00_3563))
					{	/* Ieee/port.scm 870 */
						BgL_auxz00_5129 = BgL_thunkz00_3563;
					}
				else
					{
						obj_t BgL_auxz00_5132;

						BgL_auxz00_5132 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(38543L),
							BGl_string2437z00zz__r4_ports_6_10_1z00,
							BGl_string2405z00zz__r4_ports_6_10_1z00, BgL_thunkz00_3563);
						FAILURE(BgL_auxz00_5132, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_stringz00_3562))
					{	/* Ieee/port.scm 870 */
						BgL_auxz00_5122 = BgL_stringz00_3562;
					}
				else
					{
						obj_t BgL_auxz00_5125;

						BgL_auxz00_5125 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(38543L),
							BGl_string2437z00zz__r4_ports_6_10_1z00,
							BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_stringz00_3562);
						FAILURE(BgL_auxz00_5125, BFALSE, BFALSE);
					}
				return
					BGl_withzd2errorzd2tozd2filezd2zz__r4_ports_6_10_1z00(BgL_auxz00_5122,
					BgL_auxz00_5129);
			}
		}

	}



/* &<@anonymous:1464> */
	obj_t BGl_z62zc3z04anonymousza31464ze3ze5zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3564)
	{
		{	/* Ieee/port.scm 874 */
			{	/* Ieee/port.scm 879 */
				obj_t BgL_denvz00_3565;
				obj_t BgL_oldzd2outputzd2portz00_3566;
				obj_t BgL_portz00_3567;

				BgL_denvz00_3565 = ((obj_t) PROCEDURE_REF(BgL_envz00_3564, (int) (0L)));
				BgL_oldzd2outputzd2portz00_3566 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3564, (int) (1L)));
				BgL_portz00_3567 = PROCEDURE_REF(BgL_envz00_3564, (int) (2L));
				BGL_ENV_CURRENT_ERROR_PORT_SET(BgL_denvz00_3565,
					BgL_oldzd2outputzd2portz00_3566);
				BUNSPEC;
				return bgl_close_output_port(((obj_t) BgL_portz00_3567));
			}
		}

	}



/* with-error-to-port */
	BGL_EXPORTED_DEF obj_t
		BGl_withzd2errorzd2tozd2portzd2zz__r4_ports_6_10_1z00(obj_t BgL_portz00_43,
		obj_t BgL_thunkz00_44)
	{
		{	/* Ieee/port.scm 889 */
			{	/* Ieee/port.scm 890 */
				obj_t BgL_denvz00_1598;

				BgL_denvz00_1598 = BGL_CURRENT_DYNAMIC_ENV();
				{	/* Ieee/port.scm 890 */
					obj_t BgL_oldzd2outputzd2portz00_1599;

					BgL_oldzd2outputzd2portz00_1599 =
						BGL_ENV_CURRENT_ERROR_PORT(BgL_denvz00_1598);
					{	/* Ieee/port.scm 891 */

						{	/* Ieee/port.scm 892 */
							obj_t BgL_exitd1103z00_1600;

							BgL_exitd1103z00_1600 = BGL_EXITD_TOP_AS_OBJ();
							{
								obj_t BgL_zc3z04anonymousza31465ze3z87_3568;

								BgL_zc3z04anonymousza31465ze3z87_3568 =
									MAKE_FX_PROCEDURE
									(BGl_z62zc3z04anonymousza31465ze3ze5zz__r4_ports_6_10_1z00,
									(int) (0L), (int) (2L));
								PROCEDURE_SET(BgL_zc3z04anonymousza31465ze3z87_3568, (int) (0L),
									BgL_denvz00_1598);
								PROCEDURE_SET(BgL_zc3z04anonymousza31465ze3z87_3568, (int) (1L),
									BgL_oldzd2outputzd2portz00_1599);
								{	/* Ieee/port.scm 892 */
									obj_t BgL_arg2014z00_2657;

									{	/* Ieee/port.scm 892 */
										obj_t BgL_arg2015z00_2658;

										BgL_arg2015z00_2658 =
											BGL_EXITD_PROTECT(BgL_exitd1103z00_1600);
										BgL_arg2014z00_2657 =
											MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31465ze3z87_3568,
											BgL_arg2015z00_2658);
									}
									BGL_EXITD_PROTECT_SET(BgL_exitd1103z00_1600,
										BgL_arg2014z00_2657);
									BUNSPEC;
								}
								{	/* Ieee/port.scm 894 */
									obj_t BgL_tmp1105z00_1602;

									BGL_ENV_CURRENT_ERROR_PORT_SET(BgL_denvz00_1598,
										BgL_portz00_43);
									BUNSPEC;
									BgL_tmp1105z00_1602 = BGL_PROCEDURE_CALL0(BgL_thunkz00_44);
									{	/* Ieee/port.scm 892 */
										bool_t BgL_test2972z00_5165;

										{	/* Ieee/port.scm 892 */
											obj_t BgL_arg2013z00_2660;

											BgL_arg2013z00_2660 =
												BGL_EXITD_PROTECT(BgL_exitd1103z00_1600);
											BgL_test2972z00_5165 = PAIRP(BgL_arg2013z00_2660);
										}
										if (BgL_test2972z00_5165)
											{	/* Ieee/port.scm 892 */
												obj_t BgL_arg2011z00_2661;

												{	/* Ieee/port.scm 892 */
													obj_t BgL_arg2012z00_2662;

													BgL_arg2012z00_2662 =
														BGL_EXITD_PROTECT(BgL_exitd1103z00_1600);
													BgL_arg2011z00_2661 =
														CDR(((obj_t) BgL_arg2012z00_2662));
												}
												BGL_EXITD_PROTECT_SET(BgL_exitd1103z00_1600,
													BgL_arg2011z00_2661);
												BUNSPEC;
											}
										else
											{	/* Ieee/port.scm 892 */
												BFALSE;
											}
									}
									BGL_ENV_CURRENT_ERROR_PORT_SET(BgL_denvz00_1598,
										BgL_oldzd2outputzd2portz00_1599);
									BUNSPEC;
									return BgL_tmp1105z00_1602;
								}
							}
						}
					}
				}
			}
		}

	}



/* &with-error-to-port */
	obj_t BGl_z62withzd2errorzd2tozd2portzb0zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3569, obj_t BgL_portz00_3570, obj_t BgL_thunkz00_3571)
	{
		{	/* Ieee/port.scm 889 */
			{	/* Ieee/port.scm 890 */
				obj_t BgL_auxz00_5180;
				obj_t BgL_auxz00_5173;

				if (PROCEDUREP(BgL_thunkz00_3571))
					{	/* Ieee/port.scm 890 */
						BgL_auxz00_5180 = BgL_thunkz00_3571;
					}
				else
					{
						obj_t BgL_auxz00_5183;

						BgL_auxz00_5183 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(39266L),
							BGl_string2438z00zz__r4_ports_6_10_1z00,
							BGl_string2405z00zz__r4_ports_6_10_1z00, BgL_thunkz00_3571);
						FAILURE(BgL_auxz00_5183, BFALSE, BFALSE);
					}
				if (OUTPUT_PORTP(BgL_portz00_3570))
					{	/* Ieee/port.scm 890 */
						BgL_auxz00_5173 = BgL_portz00_3570;
					}
				else
					{
						obj_t BgL_auxz00_5176;

						BgL_auxz00_5176 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(39266L),
							BGl_string2438z00zz__r4_ports_6_10_1z00,
							BGl_string2429z00zz__r4_ports_6_10_1z00, BgL_portz00_3570);
						FAILURE(BgL_auxz00_5176, BFALSE, BFALSE);
					}
				return
					BGl_withzd2errorzd2tozd2portzd2zz__r4_ports_6_10_1z00(BgL_auxz00_5173,
					BgL_auxz00_5180);
			}
		}

	}



/* &<@anonymous:1465> */
	obj_t BGl_z62zc3z04anonymousza31465ze3ze5zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3572)
	{
		{	/* Ieee/port.scm 892 */
			{
				obj_t BgL_denvz00_3573;
				obj_t BgL_oldzd2outputzd2portz00_3574;

				BgL_denvz00_3573 = ((obj_t) PROCEDURE_REF(BgL_envz00_3572, (int) (0L)));
				BgL_oldzd2outputzd2portz00_3574 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3572, (int) (1L)));
				BGL_ENV_CURRENT_ERROR_PORT_SET(BgL_denvz00_3573,
					BgL_oldzd2outputzd2portz00_3574);
				return BUNSPEC;
			}
		}

	}



/* with-error-to-procedure */
	BGL_EXPORTED_DEF obj_t
		BGl_withzd2errorzd2tozd2procedurezd2zz__r4_ports_6_10_1z00(obj_t
		BgL_procz00_45, obj_t BgL_thunkz00_46)
	{
		{	/* Ieee/port.scm 901 */
			{	/* Ieee/port.scm 902 */
				obj_t BgL_portz00_1605;

				{	/* Ieee/port.scm 486 */

					BgL_portz00_1605 =
						BGl_openzd2outputzd2procedurez00zz__r4_ports_6_10_1z00
						(BgL_procz00_45, BGl_proc2439z00zz__r4_ports_6_10_1z00, BTRUE,
						BGl_proc2440z00zz__r4_ports_6_10_1z00);
				}
				{	/* Ieee/port.scm 902 */
					obj_t BgL_denvz00_1606;

					BgL_denvz00_1606 = BGL_CURRENT_DYNAMIC_ENV();
					{	/* Ieee/port.scm 903 */
						obj_t BgL_oldzd2errorzd2portz00_1607;

						BgL_oldzd2errorzd2portz00_1607 =
							BGL_ENV_CURRENT_ERROR_PORT(BgL_denvz00_1606);
						{	/* Ieee/port.scm 904 */
							obj_t BgL_resz00_3589;

							BgL_resz00_3589 = MAKE_CELL(BUNSPEC);
							{	/* Ieee/port.scm 905 */

								{	/* Ieee/port.scm 906 */
									obj_t BgL_exitd1107z00_1609;

									BgL_exitd1107z00_1609 = BGL_EXITD_TOP_AS_OBJ();
									{	/* Ieee/port.scm 911 */
										obj_t BgL_zc3z04anonymousza31466ze3z87_3575;

										BgL_zc3z04anonymousza31466ze3z87_3575 =
											MAKE_FX_PROCEDURE
											(BGl_z62zc3z04anonymousza31466ze3ze5zz__r4_ports_6_10_1z00,
											(int) (0L), (int) (4L));
										PROCEDURE_SET(BgL_zc3z04anonymousza31466ze3z87_3575,
											(int) (0L), BgL_denvz00_1606);
										PROCEDURE_SET(BgL_zc3z04anonymousza31466ze3z87_3575,
											(int) (1L), BgL_oldzd2errorzd2portz00_1607);
										PROCEDURE_SET(BgL_zc3z04anonymousza31466ze3z87_3575,
											(int) (2L), BgL_portz00_1605);
										PROCEDURE_SET(BgL_zc3z04anonymousza31466ze3z87_3575,
											(int) (3L), ((obj_t) BgL_resz00_3589));
										{	/* Ieee/port.scm 906 */
											obj_t BgL_arg2014z00_2665;

											{	/* Ieee/port.scm 906 */
												obj_t BgL_arg2015z00_2666;

												BgL_arg2015z00_2666 =
													BGL_EXITD_PROTECT(BgL_exitd1107z00_1609);
												BgL_arg2014z00_2665 =
													MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31466ze3z87_3575,
													BgL_arg2015z00_2666);
											}
											BGL_EXITD_PROTECT_SET(BgL_exitd1107z00_1609,
												BgL_arg2014z00_2665);
											BUNSPEC;
										}
										{	/* Ieee/port.scm 908 */
											obj_t BgL_tmp1109z00_1611;

											BGL_ENV_CURRENT_ERROR_PORT_SET(BgL_denvz00_1606,
												BgL_portz00_1605);
											BUNSPEC;
											BgL_tmp1109z00_1611 =
												BGL_PROCEDURE_CALL0(BgL_thunkz00_46);
											{	/* Ieee/port.scm 906 */
												bool_t BgL_test2975z00_5218;

												{	/* Ieee/port.scm 906 */
													obj_t BgL_arg2013z00_2668;

													BgL_arg2013z00_2668 =
														BGL_EXITD_PROTECT(BgL_exitd1107z00_1609);
													BgL_test2975z00_5218 = PAIRP(BgL_arg2013z00_2668);
												}
												if (BgL_test2975z00_5218)
													{	/* Ieee/port.scm 906 */
														obj_t BgL_arg2011z00_2669;

														{	/* Ieee/port.scm 906 */
															obj_t BgL_arg2012z00_2670;

															BgL_arg2012z00_2670 =
																BGL_EXITD_PROTECT(BgL_exitd1107z00_1609);
															BgL_arg2011z00_2669 =
																CDR(((obj_t) BgL_arg2012z00_2670));
														}
														BGL_EXITD_PROTECT_SET(BgL_exitd1107z00_1609,
															BgL_arg2011z00_2669);
														BUNSPEC;
													}
												else
													{	/* Ieee/port.scm 906 */
														BFALSE;
													}
											}
											BGl_z62zc3z04anonymousza31466ze3ze5zz__r4_ports_6_10_1z00
												(BgL_zc3z04anonymousza31466ze3z87_3575);
											BgL_tmp1109z00_1611;
										}
									}
								}
								return CELL_REF(BgL_resz00_3589);
							}
						}
					}
				}
			}
		}

	}



/* &with-error-to-procedure */
	obj_t BGl_z62withzd2errorzd2tozd2procedurezb0zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3578, obj_t BgL_procz00_3579, obj_t BgL_thunkz00_3580)
	{
		{	/* Ieee/port.scm 901 */
			{	/* Ieee/port.scm 902 */
				obj_t BgL_auxz00_5233;
				obj_t BgL_auxz00_5226;

				if (PROCEDUREP(BgL_thunkz00_3580))
					{	/* Ieee/port.scm 902 */
						BgL_auxz00_5233 = BgL_thunkz00_3580;
					}
				else
					{
						obj_t BgL_auxz00_5236;

						BgL_auxz00_5236 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(39766L),
							BGl_string2441z00zz__r4_ports_6_10_1z00,
							BGl_string2405z00zz__r4_ports_6_10_1z00, BgL_thunkz00_3580);
						FAILURE(BgL_auxz00_5236, BFALSE, BFALSE);
					}
				if (PROCEDUREP(BgL_procz00_3579))
					{	/* Ieee/port.scm 902 */
						BgL_auxz00_5226 = BgL_procz00_3579;
					}
				else
					{
						obj_t BgL_auxz00_5229;

						BgL_auxz00_5229 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(39766L),
							BGl_string2441z00zz__r4_ports_6_10_1z00,
							BGl_string2405z00zz__r4_ports_6_10_1z00, BgL_procz00_3579);
						FAILURE(BgL_auxz00_5229, BFALSE, BFALSE);
					}
				return
					BGl_withzd2errorzd2tozd2procedurezd2zz__r4_ports_6_10_1z00
					(BgL_auxz00_5226, BgL_auxz00_5233);
			}
		}

	}



/* &<@anonymous:1466> */
	obj_t BGl_z62zc3z04anonymousza31466ze3ze5zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3581)
	{
		{	/* Ieee/port.scm 906 */
			{	/* Ieee/port.scm 911 */
				obj_t BgL_denvz00_3582;
				obj_t BgL_oldzd2errorzd2portz00_3583;
				obj_t BgL_portz00_3584;
				obj_t BgL_resz00_3585;

				BgL_denvz00_3582 = ((obj_t) PROCEDURE_REF(BgL_envz00_3581, (int) (0L)));
				BgL_oldzd2errorzd2portz00_3583 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3581, (int) (1L)));
				BgL_portz00_3584 = PROCEDURE_REF(BgL_envz00_3581, (int) (2L));
				BgL_resz00_3585 = PROCEDURE_REF(BgL_envz00_3581, (int) (3L));
				BGL_ENV_CURRENT_ERROR_PORT_SET(BgL_denvz00_3582,
					BgL_oldzd2errorzd2portz00_3583);
				BUNSPEC;
				{	/* Ieee/port.scm 912 */
					obj_t BgL_auxz00_4166;

					BgL_auxz00_4166 = bgl_close_output_port(((obj_t) BgL_portz00_3584));
					return CELL_SET(BgL_resz00_3585, BgL_auxz00_4166);
				}
			}
		}

	}



/* &close2049 */
	obj_t BGl_z62close2049z62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3587)
	{
		{	/* Ieee/port.scm 486 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &flush2050 */
	obj_t BGl_z62flush2050z62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3588)
	{
		{	/* Ieee/port.scm 486 */
			return BBOOL(((bool_t) 0));
		}

	}



/* input-port-protocol */
	BGL_EXPORTED_DEF obj_t
		BGl_inputzd2portzd2protocolz00zz__r4_ports_6_10_1z00(obj_t
		BgL_prototcolz00_47)
	{
		{	/* Ieee/port.scm 946 */
			{	/* Ieee/port.scm 947 */
				obj_t BgL_cellz00_2672;

				{	/* Ieee/port.scm 947 */
					obj_t BgL_top2979z00_5257;

					BgL_top2979z00_5257 = BGL_EXITD_TOP_AS_OBJ();
					BGL_MUTEX_LOCK
						(BGl_za2inputzd2portzd2protocolszd2mutexza2zd2zz__r4_ports_6_10_1z00);
					BGL_EXITD_PUSH_PROTECT(BgL_top2979z00_5257,
						BGl_za2inputzd2portzd2protocolszd2mutexza2zd2zz__r4_ports_6_10_1z00);
					BUNSPEC;
					{	/* Ieee/port.scm 947 */
						obj_t BgL_tmp2978z00_5256;

						BgL_tmp2978z00_5256 =
							BGl_assocz00zz__r4_pairs_and_lists_6_3z00(BgL_prototcolz00_47,
							BGl_za2inputzd2portzd2protocolsza2z00zz__r4_ports_6_10_1z00);
						BGL_EXITD_POP_PROTECT(BgL_top2979z00_5257);
						BUNSPEC;
						BGL_MUTEX_UNLOCK
							(BGl_za2inputzd2portzd2protocolszd2mutexza2zd2zz__r4_ports_6_10_1z00);
						BgL_cellz00_2672 = BgL_tmp2978z00_5256;
					}
				}
				if (PAIRP(BgL_cellz00_2672))
					{	/* Ieee/port.scm 949 */
						return CDR(BgL_cellz00_2672);
					}
				else
					{	/* Ieee/port.scm 949 */
						return BFALSE;
					}
			}
		}

	}



/* &input-port-protocol */
	obj_t BGl_z62inputzd2portzd2protocolz62zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3591, obj_t BgL_prototcolz00_3592)
	{
		{	/* Ieee/port.scm 946 */
			return
				BGl_inputzd2portzd2protocolz00zz__r4_ports_6_10_1z00
				(BgL_prototcolz00_3592);
		}

	}



/* input-port-protocol-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_inputzd2portzd2protocolzd2setz12zc0zz__r4_ports_6_10_1z00(obj_t
		BgL_protocolz00_48, obj_t BgL_openz00_49)
	{
		{	/* Ieee/port.scm 956 */
			{	/* Ieee/port.scm 957 */
				obj_t BgL_top2982z00_5269;

				BgL_top2982z00_5269 = BGL_EXITD_TOP_AS_OBJ();
				BGL_MUTEX_LOCK
					(BGl_za2inputzd2portzd2protocolszd2mutexza2zd2zz__r4_ports_6_10_1z00);
				BGL_EXITD_PUSH_PROTECT(BgL_top2982z00_5269,
					BGl_za2inputzd2portzd2protocolszd2mutexza2zd2zz__r4_ports_6_10_1z00);
				BUNSPEC;
				{	/* Ieee/port.scm 957 */
					obj_t BgL_tmp2981z00_5268;

					{	/* Ieee/port.scm 958 */
						bool_t BgL_test2983z00_5273;

						if (PROCEDUREP(BgL_openz00_49))
							{	/* Ieee/port.scm 958 */
								BgL_test2983z00_5273 =
									PROCEDURE_CORRECT_ARITYP(BgL_openz00_49, (int) (3L));
							}
						else
							{	/* Ieee/port.scm 958 */
								BgL_test2983z00_5273 = ((bool_t) 0);
							}
						if (BgL_test2983z00_5273)
							{	/* Ieee/port.scm 958 */
								BFALSE;
							}
						else
							{	/* Ieee/port.scm 958 */
								BGl_errorz00zz__errorz00
									(BGl_string2442z00zz__r4_ports_6_10_1z00,
									BGl_string2443z00zz__r4_ports_6_10_1z00, BgL_protocolz00_48);
							}
					}
					{	/* Ieee/port.scm 962 */
						obj_t BgL_cz00_1628;

						BgL_cz00_1628 =
							BGl_assocz00zz__r4_pairs_and_lists_6_3z00(BgL_protocolz00_48,
							BGl_za2inputzd2portzd2protocolsza2z00zz__r4_ports_6_10_1z00);
						if (PAIRP(BgL_cz00_1628))
							{	/* Ieee/port.scm 963 */
								BgL_tmp2981z00_5268 = SET_CDR(BgL_cz00_1628, BgL_openz00_49);
							}
						else
							{	/* Ieee/port.scm 966 */
								obj_t BgL_arg1474z00_1630;

								BgL_arg1474z00_1630 =
									MAKE_YOUNG_PAIR(BgL_protocolz00_48, BgL_openz00_49);
								BgL_tmp2981z00_5268 =
									(BGl_za2inputzd2portzd2protocolsza2z00zz__r4_ports_6_10_1z00 =
									MAKE_YOUNG_PAIR(BgL_arg1474z00_1630,
										BGl_za2inputzd2portzd2protocolsza2z00zz__r4_ports_6_10_1z00),
									BUNSPEC);
							}
					}
					BGL_EXITD_POP_PROTECT(BgL_top2982z00_5269);
					BUNSPEC;
					BGL_MUTEX_UNLOCK
						(BGl_za2inputzd2portzd2protocolszd2mutexza2zd2zz__r4_ports_6_10_1z00);
					BgL_tmp2981z00_5268;
				}
			}
			return BgL_openz00_49;
		}

	}



/* &input-port-protocol-set! */
	obj_t BGl_z62inputzd2portzd2protocolzd2setz12za2zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3593, obj_t BgL_protocolz00_3594, obj_t BgL_openz00_3595)
	{
		{	/* Ieee/port.scm 956 */
			return
				BGl_inputzd2portzd2protocolzd2setz12zc0zz__r4_ports_6_10_1z00
				(BgL_protocolz00_3594, BgL_openz00_3595);
		}

	}



/* get-port-buffer */
	BGL_EXPORTED_DEF obj_t BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(obj_t
		BgL_whoz00_50, obj_t BgL_bufinfoz00_51, int BgL_defsiza7za7_52)
	{
		{	/* Ieee/port.scm 972 */
			if ((BgL_bufinfoz00_51 == BTRUE))
				{	/* Ieee/port.scm 974 */
					return make_string_sans_fill((long) (BgL_defsiza7za7_52));
				}
			else
				{	/* Ieee/port.scm 974 */
					if ((BgL_bufinfoz00_51 == BFALSE))
						{	/* Ieee/port.scm 976 */
							return make_string_sans_fill(2L);
						}
					else
						{	/* Ieee/port.scm 976 */
							if (STRINGP(BgL_bufinfoz00_51))
								{	/* Ieee/port.scm 978 */
									return BgL_bufinfoz00_51;
								}
							else
								{	/* Ieee/port.scm 978 */
									if (INTEGERP(BgL_bufinfoz00_51))
										{	/* Ieee/port.scm 980 */
											if (((long) CINT(BgL_bufinfoz00_51) >= 2L))
												{	/* Ieee/port.scm 981 */
													return
														make_string_sans_fill(
														(long) CINT(BgL_bufinfoz00_51));
												}
											else
												{	/* Ieee/port.scm 981 */
													return make_string_sans_fill(2L);
												}
										}
									else
										{	/* Ieee/port.scm 980 */
											return
												BGl_errorz00zz__errorz00(BgL_whoz00_50,
												BGl_string2444z00zz__r4_ports_6_10_1z00,
												BgL_bufinfoz00_51);
										}
								}
						}
				}
		}

	}



/* &get-port-buffer */
	obj_t BGl_z62getzd2portzd2bufferz62zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3596, obj_t BgL_whoz00_3597, obj_t BgL_bufinfoz00_3598,
		obj_t BgL_defsiza7za7_3599)
	{
		{	/* Ieee/port.scm 972 */
			{	/* Ieee/port.scm 974 */
				int BgL_auxz00_5306;

				{	/* Ieee/port.scm 974 */
					obj_t BgL_tmpz00_5307;

					if (INTEGERP(BgL_defsiza7za7_3599))
						{	/* Ieee/port.scm 974 */
							BgL_tmpz00_5307 = BgL_defsiza7za7_3599;
						}
					else
						{
							obj_t BgL_auxz00_5310;

							BgL_auxz00_5310 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(42843L),
								BGl_string2445z00zz__r4_ports_6_10_1z00,
								BGl_string2446z00zz__r4_ports_6_10_1z00, BgL_defsiza7za7_3599);
							FAILURE(BgL_auxz00_5310, BFALSE, BFALSE);
						}
					BgL_auxz00_5306 = CINT(BgL_tmpz00_5307);
				}
				return
					BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BgL_whoz00_3597,
					BgL_bufinfoz00_3598, BgL_auxz00_5306);
			}
		}

	}



/* &%open-input-file */
	obj_t BGl_z62z52openzd2inputzd2filez30zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3385, obj_t BgL_stringz00_3386, obj_t BgL_bufz00_3387,
		obj_t BgL_tmtz00_3388)
	{
		{	/* Ieee/port.scm 990 */
			return bgl_open_input_file(BgL_stringz00_3386, BgL_bufz00_3387);
		}

	}



/* &%open-input-descriptor */
	obj_t BGl_z62z52openzd2inputzd2descriptorz30zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3417, obj_t BgL_stringz00_3418, obj_t BgL_bufz00_3419,
		obj_t BgL_tmtz00_3420)
	{
		{	/* Ieee/port.scm 996 */
			{	/* Ieee/port.scm 997 */
				long BgL_fdz00_4167;

				{	/* Ieee/port.scm 997 */
					obj_t BgL_arg1478z00_4168;

					{	/* Ieee/port.scm 997 */
						long BgL_endz00_4169;

						BgL_endz00_4169 = STRING_LENGTH(((obj_t) BgL_stringz00_3418));
						{	/* Ieee/port.scm 997 */

							BgL_arg1478z00_4168 =
								BGl_substringz00zz__r4_strings_6_7z00(BgL_stringz00_3418, 3L,
								BgL_endz00_4169);
					}}
					{	/* Ieee/port.scm 997 */
						char *BgL_tmpz00_5320;

						BgL_tmpz00_5320 = BSTRING_TO_STRING(BgL_arg1478z00_4168);
						BgL_fdz00_4167 = BGL_STRTOL(BgL_tmpz00_5320, 0L, 10L);
				}}
				return
					bgl_open_input_descriptor((int) (BgL_fdz00_4167), BgL_bufz00_3419);
		}}

	}



/* &%open-input-pipe */
	obj_t BGl_z62z52openzd2inputzd2pipez30zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3389, obj_t BgL_stringz00_3390, obj_t BgL_bufinfoz00_3391,
		obj_t BgL_tmtz00_3392)
	{
		{	/* Ieee/port.scm 1003 */
			return
				bgl_open_input_pipe(BgL_stringz00_3390,
				BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00
				(BGl_string2447z00zz__r4_ports_6_10_1z00, BgL_bufinfoz00_3391,
					(int) (1024L)));
		}

	}



/* &%open-input-resource */
	obj_t BGl_z62z52openzd2inputzd2resourcez30zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3409, obj_t BgL_filez00_3410, obj_t BgL_bufinfoz00_3411,
		obj_t BgL_tmtz00_3412)
	{
		{	/* Ieee/port.scm 1010 */
			{	/* Ieee/port.scm 1011 */
				obj_t BgL_bufz00_4170;

				{	/* Ieee/port.scm 1011 */
					obj_t BgL_res2016z00_4171;

					{	/* Ieee/port.scm 1011 */
						int BgL_defsiza7za7_4172;

						BgL_defsiza7za7_4172 = (int) (default_io_bufsiz);
						if ((BgL_bufinfoz00_3411 == BTRUE))
							{	/* Ieee/port.scm 974 */
								BgL_res2016z00_4171 =
									make_string_sans_fill((long) (BgL_defsiza7za7_4172));
							}
						else
							{	/* Ieee/port.scm 974 */
								if ((BgL_bufinfoz00_3411 == BFALSE))
									{	/* Ieee/port.scm 976 */
										BgL_res2016z00_4171 = make_string_sans_fill(2L);
									}
								else
									{	/* Ieee/port.scm 976 */
										if (STRINGP(BgL_bufinfoz00_3411))
											{	/* Ieee/port.scm 978 */
												BgL_res2016z00_4171 = BgL_bufinfoz00_3411;
											}
										else
											{	/* Ieee/port.scm 978 */
												if (INTEGERP(BgL_bufinfoz00_3411))
													{	/* Ieee/port.scm 980 */
														if (((long) CINT(BgL_bufinfoz00_3411) >= 2L))
															{	/* Ieee/port.scm 981 */
																BgL_res2016z00_4171 =
																	make_string_sans_fill(
																	(long) CINT(BgL_bufinfoz00_3411));
															}
														else
															{	/* Ieee/port.scm 981 */
																BgL_res2016z00_4171 = make_string_sans_fill(2L);
															}
													}
												else
													{	/* Ieee/port.scm 980 */
														BgL_res2016z00_4171 =
															BGl_errorz00zz__errorz00
															(BGl_string2448z00zz__r4_ports_6_10_1z00,
															BGl_string2444z00zz__r4_ports_6_10_1z00,
															BgL_bufinfoz00_3411);
													}
											}
									}
							}
					}
					BgL_bufz00_4170 = BgL_res2016z00_4171;
				}
				return bgl_open_input_resource(BgL_filez00_3410, BgL_bufz00_4170);
			}
		}

	}



/* &%open-input-http-socket */
	obj_t BGl_z62z52openzd2inputzd2httpzd2socketze2zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3393, obj_t BgL_stringz00_3394, obj_t BgL_bufinfoz00_3395,
		obj_t BgL_timeoutz00_3396)
	{
		{	/* Ieee/port.scm 1017 */
			{	/* Ieee/port.scm 1031 */
				obj_t BgL_protocolz00_4174;

				BgL_protocolz00_4174 =
					BGl_urlzd2sanszd2protocolzd2parsezd2zz__urlz00(BgL_stringz00_3394,
					BGl_string2450z00zz__r4_ports_6_10_1z00);
				{	/* Ieee/port.scm 1032 */
					obj_t BgL_loginz00_4175;
					obj_t BgL_hostz00_4176;
					obj_t BgL_portz00_4177;
					obj_t BgL_abspathz00_4178;

					{	/* Ieee/port.scm 1033 */
						obj_t BgL_tmpz00_4179;

						{	/* Ieee/port.scm 1033 */
							int BgL_tmpz00_5349;

							BgL_tmpz00_5349 = (int) (1L);
							BgL_tmpz00_4179 = BGL_MVALUES_VAL(BgL_tmpz00_5349);
						}
						{	/* Ieee/port.scm 1033 */
							int BgL_tmpz00_5352;

							BgL_tmpz00_5352 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_5352, BUNSPEC);
						}
						BgL_loginz00_4175 = BgL_tmpz00_4179;
					}
					{	/* Ieee/port.scm 1033 */
						obj_t BgL_tmpz00_4180;

						{	/* Ieee/port.scm 1033 */
							int BgL_tmpz00_5355;

							BgL_tmpz00_5355 = (int) (2L);
							BgL_tmpz00_4180 = BGL_MVALUES_VAL(BgL_tmpz00_5355);
						}
						{	/* Ieee/port.scm 1033 */
							int BgL_tmpz00_5358;

							BgL_tmpz00_5358 = (int) (2L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_5358, BUNSPEC);
						}
						BgL_hostz00_4176 = BgL_tmpz00_4180;
					}
					{	/* Ieee/port.scm 1033 */
						obj_t BgL_tmpz00_4181;

						{	/* Ieee/port.scm 1033 */
							int BgL_tmpz00_5361;

							BgL_tmpz00_5361 = (int) (3L);
							BgL_tmpz00_4181 = BGL_MVALUES_VAL(BgL_tmpz00_5361);
						}
						{	/* Ieee/port.scm 1033 */
							int BgL_tmpz00_5364;

							BgL_tmpz00_5364 = (int) (3L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_5364, BUNSPEC);
						}
						BgL_portz00_4177 = BgL_tmpz00_4181;
					}
					{	/* Ieee/port.scm 1033 */
						obj_t BgL_tmpz00_4182;

						{	/* Ieee/port.scm 1033 */
							int BgL_tmpz00_5367;

							BgL_tmpz00_5367 = (int) (4L);
							BgL_tmpz00_4182 = BGL_MVALUES_VAL(BgL_tmpz00_5367);
						}
						{	/* Ieee/port.scm 1033 */
							int BgL_tmpz00_5370;

							BgL_tmpz00_5370 = (int) (4L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_5370, BUNSPEC);
						}
						BgL_abspathz00_4178 = BgL_tmpz00_4182;
					}
					return
						BGl_z62loopz62zz__r4_ports_6_10_1z00
						(BGl_proc2449z00zz__r4_ports_6_10_1z00, BgL_bufinfoz00_3395,
						BgL_timeoutz00_3396, BgL_portz00_4177, BgL_abspathz00_4178,
						BgL_loginz00_4175, BgL_hostz00_4176, BFALSE,
						BGl_list2451z00zz__r4_ports_6_10_1z00);
				}
			}
		}

	}



/* &loop */
	obj_t BGl_z62loopz62zz__r4_ports_6_10_1z00(obj_t BgL_parserz00_3610,
		obj_t BgL_bufinfoz00_3609, obj_t BgL_timeoutz00_3608,
		obj_t BgL_portz00_3607, obj_t BgL_abspathz00_3606, obj_t BgL_loginz00_3605,
		obj_t BgL_hostz00_3604, obj_t BgL_ipz00_1649, obj_t BgL_headerz00_1650)
	{
		{	/* Ieee/port.scm 1033 */
			{	/* Ieee/port.scm 1035 */
				struct bgl_cell BgL_box2997_3810z00;
				obj_t BgL_ipz00_3810;

				BgL_ipz00_3810 = MAKE_CELL_STACK(BgL_ipz00_1649, BgL_box2997_3810z00);
				{	/* Ieee/port.scm 1035 */
					obj_t BgL_sockz00_1652;

					{	/* Ieee/port.scm 1035 */
						obj_t BgL_methodz00_1709;

						BgL_methodz00_1709 = BGl_symbol2461z00zz__r4_ports_6_10_1z00;
						BgL_sockz00_1652 =
							BGl_httpz00zz__httpz00(BNIL, BFALSE, BFALSE, BUNSPEC, BFALSE,
							BgL_headerz00_1650, BgL_hostz00_3604,
							BGl_string2464z00zz__r4_ports_6_10_1z00, BFALSE,
							BgL_loginz00_3605, BgL_methodz00_1709, BFALSE, BFALSE,
							BgL_abspathz00_3606, BgL_portz00_3607,
							BGl_symbol2463z00zz__r4_ports_6_10_1z00, BFALSE, BFALSE,
							BgL_timeoutz00_3608, BFALSE);
					}
					{	/* Ieee/port.scm 1035 */
						obj_t BgL_opz00_1653;

						{	/* Ieee/port.scm 1041 */
							obj_t BgL_tmpz00_5375;

							BgL_tmpz00_5375 = ((obj_t) BgL_sockz00_1652);
							BgL_opz00_1653 = SOCKET_OUTPUT(BgL_tmpz00_5375);
						}
						{	/* Ieee/port.scm 1041 */

							{	/* Ieee/port.scm 1042 */
								bool_t BgL_test2998z00_5378;

								{	/* Ieee/port.scm 1042 */
									obj_t BgL_objz00_2713;

									BgL_objz00_2713 = CELL_REF(BgL_ipz00_3810);
									BgL_test2998z00_5378 = INPUT_PORTP(BgL_objz00_2713);
								}
								if (BgL_test2998z00_5378)
									{	/* Ieee/port.scm 1045 */
										obj_t BgL_arg1482z00_1655;

										{	/* Ieee/port.scm 1045 */
											obj_t BgL_tmpz00_5380;

											BgL_tmpz00_5380 = ((obj_t) BgL_sockz00_1652);
											BgL_arg1482z00_1655 = SOCKET_INPUT(BgL_tmpz00_5380);
										}
										{	/* Ieee/port.scm 1045 */
											obj_t BgL_dstz00_2715;

											BgL_dstz00_2715 = CELL_REF(BgL_ipz00_3810);
											bgl_input_port_clone(BgL_dstz00_2715,
												BgL_arg1482z00_1655);
										}
									}
								else
									{	/* Ieee/port.scm 1046 */
										obj_t BgL_auxz00_3811;

										{	/* Ieee/port.scm 1046 */
											obj_t BgL_tmpz00_5384;

											BgL_tmpz00_5384 = ((obj_t) BgL_sockz00_1652);
											BgL_auxz00_3811 = SOCKET_INPUT(BgL_tmpz00_5384);
										}
										CELL_SET(BgL_ipz00_3810, BgL_auxz00_3811);
									}
							}
							{	/* Ieee/port.scm 1049 */
								obj_t BgL_zc3z04anonymousza31484ze3z87_3600;

								BgL_zc3z04anonymousza31484ze3z87_3600 =
									MAKE_FX_PROCEDURE
									(BGl_z62zc3z04anonymousza31484ze3ze5zz__r4_ports_6_10_1z00,
									(int) (1L), (int) (2L));
								PROCEDURE_SET(BgL_zc3z04anonymousza31484ze3z87_3600, (int) (0L),
									BgL_opz00_1653);
								PROCEDURE_SET(BgL_zc3z04anonymousza31484ze3z87_3600, (int) (1L),
									BgL_sockz00_1652);
								{	/* Ieee/port.scm 1047 */
									obj_t BgL_portz00_2720;

									BgL_portz00_2720 = CELL_REF(BgL_ipz00_3810);
									if (PROCEDURE_CORRECT_ARITYP
										(BgL_zc3z04anonymousza31484ze3z87_3600, (int) (1L)))
										{	/* Ieee/port.scm 1458 */
											PORT_CHOOK_SET(BgL_portz00_2720,
												BgL_zc3z04anonymousza31484ze3z87_3600);
											BUNSPEC;
											BgL_zc3z04anonymousza31484ze3z87_3600;
										}
									else
										{	/* Ieee/port.scm 1458 */
											bgl_system_failure(BGL_IO_PORT_ERROR,
												BGl_string2465z00zz__r4_ports_6_10_1z00,
												BGl_string2466z00zz__r4_ports_6_10_1z00,
												BgL_zc3z04anonymousza31484ze3z87_3600);
										}
								}
							}
							{	/* Ieee/port.scm 1053 */
								obj_t BgL_zc3z04anonymousza31486ze3z87_3601;

								BgL_zc3z04anonymousza31486ze3z87_3601 =
									MAKE_FX_PROCEDURE
									(BGl_z62zc3z04anonymousza31486ze3ze5zz__r4_ports_6_10_1z00,
									(int) (2L), (int) (8L));
								PROCEDURE_SET(BgL_zc3z04anonymousza31486ze3z87_3601, (int) (0L),
									BgL_hostz00_3604);
								PROCEDURE_SET(BgL_zc3z04anonymousza31486ze3z87_3601, (int) (1L),
									BgL_loginz00_3605);
								PROCEDURE_SET(BgL_zc3z04anonymousza31486ze3z87_3601, (int) (2L),
									BgL_abspathz00_3606);
								PROCEDURE_SET(BgL_zc3z04anonymousza31486ze3z87_3601, (int) (3L),
									BgL_portz00_3607);
								PROCEDURE_SET(BgL_zc3z04anonymousza31486ze3z87_3601, (int) (4L),
									BgL_timeoutz00_3608);
								PROCEDURE_SET(BgL_zc3z04anonymousza31486ze3z87_3601, (int) (5L),
									BgL_bufinfoz00_3609);
								PROCEDURE_SET(BgL_zc3z04anonymousza31486ze3z87_3601, (int) (6L),
									BgL_parserz00_3610);
								PROCEDURE_SET(BgL_zc3z04anonymousza31486ze3z87_3601, (int) (7L),
									BgL_sockz00_1652);
								{	/* Ieee/port.scm 1051 */
									obj_t BgL_portz00_2724;

									BgL_portz00_2724 = CELL_REF(BgL_ipz00_3810);
									if (PROCEDURE_CORRECT_ARITYP
										(BgL_zc3z04anonymousza31486ze3z87_3601, (int) (2L)))
										{	/* Ieee/port.scm 1475 */
											BGL_INPUT_PORT_USEEK_SET(BgL_portz00_2724,
												BgL_zc3z04anonymousza31486ze3z87_3601);
											BUNSPEC;
											BgL_zc3z04anonymousza31486ze3z87_3601;
										}
									else
										{	/* Ieee/port.scm 1475 */
											bgl_system_failure(BGL_IO_PORT_ERROR,
												BGl_string2467z00zz__r4_ports_6_10_1z00,
												BGl_string2468z00zz__r4_ports_6_10_1z00,
												BgL_zc3z04anonymousza31486ze3z87_3601);
										}
								}
							}
							{	/* Ieee/port.scm 1056 */
								obj_t BgL_env1120z00_1676;

								BgL_env1120z00_1676 = BGL_CURRENT_DYNAMIC_ENV();
								{
									obj_t BgL_ez00_1690;

									{	/* Ieee/port.scm 1056 */
										obj_t BgL_cell1115z00_1677;

										BgL_cell1115z00_1677 = MAKE_STACK_CELL(BUNSPEC);
										{	/* Ieee/port.scm 1056 */
											obj_t BgL_val1119z00_1678;

											BgL_val1119z00_1678 =
												BGl_zc3z04exitza31498ze3ze70z60zz__r4_ports_6_10_1z00
												(BgL_parserz00_3610, BgL_opz00_1653, BgL_ipz00_3810,
												BgL_cell1115z00_1677, BgL_env1120z00_1676);
											if ((BgL_val1119z00_1678 == BgL_cell1115z00_1677))
												{	/* Ieee/port.scm 1056 */
													{	/* Ieee/port.scm 1056 */
														int BgL_tmpz00_5428;

														BgL_tmpz00_5428 = (int) (0L);
														BGL_SIGSETMASK(BgL_tmpz00_5428);
													}
													{	/* Ieee/port.scm 1056 */
														obj_t BgL_arg1497z00_1679;

														BgL_arg1497z00_1679 =
															CELL_REF(((obj_t) BgL_val1119z00_1678));
														BgL_ez00_1690 = BgL_arg1497z00_1679;
														socket_close(((obj_t) BgL_sockz00_1652));
														{	/* Ieee/port.scm 1059 */
															bool_t BgL_test3002z00_5435;

															{	/* Ieee/port.scm 1059 */
																obj_t BgL_classz00_2728;

																BgL_classz00_2728 =
																	BGl_z62httpzd2redirectionzb0zz__httpz00;
																if (BGL_OBJECTP(BgL_ez00_1690))
																	{	/* Ieee/port.scm 1059 */
																		BgL_objectz00_bglt BgL_arg1994z00_2730;

																		BgL_arg1994z00_2730 =
																			(BgL_objectz00_bglt) (BgL_ez00_1690);
																		if (BGL_CONDEXPAND_ISA_ARCH64())
																			{	/* Ieee/port.scm 1059 */
																				long BgL_idxz00_2736;

																				BgL_idxz00_2736 =
																					BGL_OBJECT_INHERITANCE_NUM
																					(BgL_arg1994z00_2730);
																				BgL_test3002z00_5435 =
																					(VECTOR_REF
																					(BGl_za2inheritancesza2z00zz__objectz00,
																						(BgL_idxz00_2736 + 3L)) ==
																					BgL_classz00_2728);
																			}
																		else
																			{	/* Ieee/port.scm 1059 */
																				bool_t BgL_res2017z00_2761;

																				{	/* Ieee/port.scm 1059 */
																					obj_t BgL_oclassz00_2744;

																					{	/* Ieee/port.scm 1059 */
																						obj_t BgL_arg2001z00_2752;
																						long BgL_arg2002z00_2753;

																						BgL_arg2001z00_2752 =
																							(BGl_za2classesza2z00zz__objectz00);
																						{	/* Ieee/port.scm 1059 */
																							long BgL_arg2003z00_2754;

																							BgL_arg2003z00_2754 =
																								BGL_OBJECT_CLASS_NUM
																								(BgL_arg1994z00_2730);
																							BgL_arg2002z00_2753 =
																								(BgL_arg2003z00_2754 -
																								OBJECT_TYPE);
																						}
																						BgL_oclassz00_2744 =
																							VECTOR_REF(BgL_arg2001z00_2752,
																							BgL_arg2002z00_2753);
																					}
																					{	/* Ieee/port.scm 1059 */
																						bool_t BgL__ortest_1211z00_2745;

																						BgL__ortest_1211z00_2745 =
																							(BgL_classz00_2728 ==
																							BgL_oclassz00_2744);
																						if (BgL__ortest_1211z00_2745)
																							{	/* Ieee/port.scm 1059 */
																								BgL_res2017z00_2761 =
																									BgL__ortest_1211z00_2745;
																							}
																						else
																							{	/* Ieee/port.scm 1059 */
																								long BgL_odepthz00_2746;

																								{	/* Ieee/port.scm 1059 */
																									obj_t BgL_arg1991z00_2747;

																									BgL_arg1991z00_2747 =
																										(BgL_oclassz00_2744);
																									BgL_odepthz00_2746 =
																										BGL_CLASS_DEPTH
																										(BgL_arg1991z00_2747);
																								}
																								if ((3L < BgL_odepthz00_2746))
																									{	/* Ieee/port.scm 1059 */
																										obj_t BgL_arg1989z00_2749;

																										{	/* Ieee/port.scm 1059 */
																											obj_t BgL_arg1990z00_2750;

																											BgL_arg1990z00_2750 =
																												(BgL_oclassz00_2744);
																											BgL_arg1989z00_2749 =
																												BGL_CLASS_ANCESTORS_REF
																												(BgL_arg1990z00_2750,
																												3L);
																										}
																										BgL_res2017z00_2761 =
																											(BgL_arg1989z00_2749 ==
																											BgL_classz00_2728);
																									}
																								else
																									{	/* Ieee/port.scm 1059 */
																										BgL_res2017z00_2761 =
																											((bool_t) 0);
																									}
																							}
																					}
																				}
																				BgL_test3002z00_5435 =
																					BgL_res2017z00_2761;
																			}
																	}
																else
																	{	/* Ieee/port.scm 1059 */
																		BgL_test3002z00_5435 = ((bool_t) 0);
																	}
															}
															if (BgL_test3002z00_5435)
																{	/* Ieee/port.scm 1061 */
																	obj_t BgL_arg1502z00_1694;

																	BgL_arg1502z00_1694 =
																		(((BgL_z62httpzd2redirectionzb0_bglt)
																			COBJECT((
																					(BgL_z62httpzd2redirectionzb0_bglt)
																					BgL_ez00_1690)))->BgL_urlz00);
																	{	/* Ieee/port.scm 466 */

																		return
																			BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00
																			(BgL_arg1502z00_1694, BgL_bufinfoz00_3609,
																			BINT(5000000L));
																	}
																}
															else
																{	/* Ieee/port.scm 1059 */
																	return BFALSE;
																}
														}
													}
												}
											else
												{	/* Ieee/port.scm 1056 */
													return BgL_val1119z00_1678;
												}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* <@exit:1498>~0 */
	obj_t BGl_zc3z04exitza31498ze3ze70z60zz__r4_ports_6_10_1z00(obj_t
		BgL_parserz00_3809, obj_t BgL_opz00_3808, obj_t BgL_ipz00_3807,
		obj_t BgL_cell1115z00_3806, obj_t BgL_env1120z00_3805)
	{
		{	/* Ieee/port.scm 1056 */
			jmp_buf_t jmpbuf;
			void *BgL_an_exit1125z00_1681;

			if (SET_EXIT(BgL_an_exit1125z00_1681))
				{
					return BGL_EXIT_VALUE();
				}
			else
				{
#if( SIGSETJMP_SAVESIGS == 0 )
				 // MS: CARE 5 jan 2021: see runtime/Clib/csystem.c
				 // bgl_restore_signal_handlers();
#endif

					BgL_an_exit1125z00_1681 = (void *) jmpbuf;
					PUSH_ENV_EXIT(BgL_env1120z00_3805, BgL_an_exit1125z00_1681, 1L);
					{	/* Ieee/port.scm 1056 */
						obj_t BgL_escape1116z00_1682;

						BgL_escape1116z00_1682 =
							BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1120z00_3805);
						{	/* Ieee/port.scm 1056 */
							obj_t BgL_res1128z00_1683;

							{	/* Ieee/port.scm 1056 */
								obj_t BgL_ohs1112z00_1684;

								BgL_ohs1112z00_1684 =
									BGL_ENV_ERROR_HANDLER_GET(BgL_env1120z00_3805);
								{	/* Ieee/port.scm 1056 */
									obj_t BgL_hds1113z00_1685;

									BgL_hds1113z00_1685 =
										MAKE_STACK_PAIR(BgL_escape1116z00_1682,
										BgL_cell1115z00_3806);
									BGL_ENV_ERROR_HANDLER_SET(BgL_env1120z00_3805,
										BgL_hds1113z00_1685);
									BUNSPEC;
									{	/* Ieee/port.scm 1056 */
										obj_t BgL_exitd1122z00_1686;

										BgL_exitd1122z00_1686 =
											BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1120z00_3805);
										{	/* Ieee/port.scm 1056 */
											obj_t BgL_tmp1124z00_1687;

											{	/* Ieee/port.scm 1056 */
												obj_t BgL_arg1499z00_1689;

												BgL_arg1499z00_1689 =
													BGL_EXITD_PROTECT(BgL_exitd1122z00_1686);
												BgL_tmp1124z00_1687 =
													MAKE_YOUNG_PAIR(BgL_ohs1112z00_1684,
													BgL_arg1499z00_1689);
											}
											{	/* Ieee/port.scm 1056 */

												BGL_EXITD_PROTECT_SET(BgL_exitd1122z00_1686,
													BgL_tmp1124z00_1687);
												BUNSPEC;
												{	/* Ieee/port.scm 1062 */
													obj_t BgL_tmp1123z00_1688;

													BgL_tmp1123z00_1688 =
														BGl_httpzd2parsezd2responsez00zz__httpz00(CELL_REF
														(BgL_ipz00_3807), BgL_opz00_3808,
														BgL_parserz00_3809);
													{	/* Ieee/port.scm 1056 */
														bool_t BgL_test3007z00_5474;

														{	/* Ieee/port.scm 1056 */
															obj_t BgL_arg2013z00_2763;

															BgL_arg2013z00_2763 =
																BGL_EXITD_PROTECT(BgL_exitd1122z00_1686);
															BgL_test3007z00_5474 = PAIRP(BgL_arg2013z00_2763);
														}
														if (BgL_test3007z00_5474)
															{	/* Ieee/port.scm 1056 */
																obj_t BgL_arg2011z00_2764;

																{	/* Ieee/port.scm 1056 */
																	obj_t BgL_arg2012z00_2765;

																	BgL_arg2012z00_2765 =
																		BGL_EXITD_PROTECT(BgL_exitd1122z00_1686);
																	BgL_arg2011z00_2764 =
																		CDR(((obj_t) BgL_arg2012z00_2765));
																}
																BGL_EXITD_PROTECT_SET(BgL_exitd1122z00_1686,
																	BgL_arg2011z00_2764);
																BUNSPEC;
															}
														else
															{	/* Ieee/port.scm 1056 */
																BFALSE;
															}
													}
													BGL_ENV_ERROR_HANDLER_SET(BgL_env1120z00_3805,
														BgL_ohs1112z00_1684);
													BUNSPEC;
													BgL_res1128z00_1683 = BgL_tmp1123z00_1688;
												}
											}
										}
									}
								}
							}
							POP_ENV_EXIT(BgL_env1120z00_3805);
							return BgL_res1128z00_1683;
						}
					}
				}
		}

	}



/* &parser */
	obj_t BGl_z62parserz62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3611,
		obj_t BgL_ipz00_3612, obj_t BgL_statuszd2codezd2_3613,
		obj_t BgL_headerz00_3614, obj_t BgL_clenz00_3615, obj_t BgL_tencz00_3616)
	{
		{	/* Ieee/port.scm 1029 */
			{	/* Ieee/port.scm 1020 */
				bool_t BgL_test3008z00_5483;

				if (((long) CINT(BgL_statuszd2codezd2_3613) >= 200L))
					{	/* Ieee/port.scm 1020 */
						BgL_test3008z00_5483 =
							((long) CINT(BgL_statuszd2codezd2_3613) <= 299L);
					}
				else
					{	/* Ieee/port.scm 1020 */
						BgL_test3008z00_5483 = ((bool_t) 0);
					}
				if (BgL_test3008z00_5483)
					{	/* Ieee/port.scm 1020 */
						if (INPUT_PORTP(BgL_ipz00_3612))
							{	/* Ieee/port.scm 1024 */
								bool_t BgL_test3011z00_5491;

								if (ELONGP(BgL_clenz00_3615))
									{	/* Ieee/port.scm 1024 */
										long BgL_n1z00_4184;

										BgL_n1z00_4184 = BELONG_TO_LONG(BgL_clenz00_3615);
										BgL_test3011z00_5491 = (BgL_n1z00_4184 > (long) (0L));
									}
								else
									{	/* Ieee/port.scm 1024 */
										BgL_test3011z00_5491 = ((bool_t) 0);
									}
								if (BgL_test3011z00_5491)
									{	/* Ieee/port.scm 1024 */
										{	/* Ieee/port.scm 1025 */
											long BgL_arg1509z00_4185;

											{	/* Ieee/port.scm 1025 */
												long BgL_xz00_4186;

												BgL_xz00_4186 = BELONG_TO_LONG(BgL_clenz00_3615);
												BgL_arg1509z00_4185 = (long) (BgL_xz00_4186);
											}
											INPUT_PORT_FILLBARRIER_SET(BgL_ipz00_3612,
												BgL_arg1509z00_4185);
											BUNSPEC;
											BgL_arg1509z00_4185;
										}
										{	/* Ieee/port.scm 1026 */
											long BgL_tmpz00_5500;

											BgL_tmpz00_5500 = BELONG_TO_LONG(BgL_clenz00_3615);
											BGL_INPUT_PORT_LENGTH_SET(BgL_ipz00_3612,
												BgL_tmpz00_5500);
										} BUNSPEC;
										return BgL_ipz00_3612;
									}
								else
									{	/* Ieee/port.scm 1024 */
										return BgL_ipz00_3612;
									}
							}
						else
							{	/* Ieee/port.scm 469 */

								return
									BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00
									(BGl_string2469z00zz__r4_ports_6_10_1z00, BINT(0L), BINT(0L));
							}
					}
				else
					{	/* Ieee/port.scm 1020 */
						return BFALSE;
					}
			}
		}

	}



/* &<@anonymous:1486> */
	obj_t BGl_z62zc3z04anonymousza31486ze3ze5zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3617, obj_t BgL_ipz00_3626, obj_t BgL_offsetz00_3627)
	{
		{	/* Ieee/port.scm 1052 */
			{	/* Ieee/port.scm 1053 */
				obj_t BgL_hostz00_3618;
				obj_t BgL_loginz00_3619;
				obj_t BgL_abspathz00_3620;
				obj_t BgL_portz00_3621;
				obj_t BgL_timeoutz00_3622;
				obj_t BgL_bufinfoz00_3623;
				obj_t BgL_parserz00_3624;
				obj_t BgL_sockz00_3625;

				BgL_hostz00_3618 = PROCEDURE_REF(BgL_envz00_3617, (int) (0L));
				BgL_loginz00_3619 = PROCEDURE_REF(BgL_envz00_3617, (int) (1L));
				BgL_abspathz00_3620 = PROCEDURE_REF(BgL_envz00_3617, (int) (2L));
				BgL_portz00_3621 = PROCEDURE_REF(BgL_envz00_3617, (int) (3L));
				BgL_timeoutz00_3622 = PROCEDURE_REF(BgL_envz00_3617, (int) (4L));
				BgL_bufinfoz00_3623 = PROCEDURE_REF(BgL_envz00_3617, (int) (5L));
				BgL_parserz00_3624 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3617, (int) (6L)));
				BgL_sockz00_3625 = PROCEDURE_REF(BgL_envz00_3617, (int) (7L));
				socket_close(((obj_t) BgL_sockz00_3625));
				{	/* Ieee/port.scm 1054 */
					obj_t BgL_rz00_4188;

					{	/* Ieee/port.scm 1054 */
						obj_t BgL_arg1495z00_4189;

						{	/* Ieee/port.scm 1053 */

							BgL_arg1495z00_4189 =
								BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
								(long) CINT(BgL_offsetz00_3627), 10L);
						}
						BgL_rz00_4188 =
							string_append_3(BGl_string2470z00zz__r4_ports_6_10_1z00,
							BgL_arg1495z00_4189, BGl_string2471z00zz__r4_ports_6_10_1z00);
					}
					{	/* Ieee/port.scm 1055 */
						obj_t BgL_arg1487z00_4190;

						{	/* Ieee/port.scm 1055 */
							obj_t BgL_arg1488z00_4191;
							obj_t BgL_arg1489z00_4192;

							{	/* Ieee/port.scm 1055 */
								obj_t BgL_arg1490z00_4193;

								BgL_arg1490z00_4193 = MAKE_YOUNG_PAIR(BgL_rz00_4188, BNIL);
								BgL_arg1488z00_4191 =
									MAKE_YOUNG_PAIR(BGl_keyword2472z00zz__r4_ports_6_10_1z00,
									BgL_arg1490z00_4193);
							}
							{	/* Ieee/port.scm 1055 */
								obj_t BgL_arg1492z00_4194;

								{	/* Ieee/port.scm 1055 */
									obj_t BgL_arg1494z00_4195;

									BgL_arg1494z00_4195 =
										MAKE_YOUNG_PAIR(BGl_string2455z00zz__r4_ports_6_10_1z00,
										BNIL);
									BgL_arg1492z00_4194 =
										MAKE_YOUNG_PAIR(BGl_keyword2453z00zz__r4_ports_6_10_1z00,
										BgL_arg1494z00_4195);
								}
								BgL_arg1489z00_4192 =
									MAKE_YOUNG_PAIR(BgL_arg1492z00_4194, BNIL);
							}
							BgL_arg1487z00_4190 =
								MAKE_YOUNG_PAIR(BgL_arg1488z00_4191, BgL_arg1489z00_4192);
						}
						return
							BGl_z62loopz62zz__r4_ports_6_10_1z00(BgL_parserz00_3624,
							BgL_bufinfoz00_3623, BgL_timeoutz00_3622, BgL_portz00_3621,
							BgL_abspathz00_3620, BgL_loginz00_3619, BgL_hostz00_3618,
							BgL_ipz00_3626, BgL_arg1487z00_4190);
					}
				}
			}
		}

	}



/* &<@anonymous:1484> */
	obj_t BGl_z62zc3z04anonymousza31484ze3ze5zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3628, obj_t BgL_ipz00_3631)
	{
		{	/* Ieee/port.scm 1048 */
			{	/* Ieee/port.scm 1049 */
				obj_t BgL_opz00_3629;
				obj_t BgL_sockz00_3630;

				BgL_opz00_3629 = ((obj_t) PROCEDURE_REF(BgL_envz00_3628, (int) (0L)));
				BgL_sockz00_3630 = PROCEDURE_REF(BgL_envz00_3628, (int) (1L));
				bgl_close_output_port(BgL_opz00_3629);
				return socket_close(((obj_t) BgL_sockz00_3630));
			}
		}

	}



/* _open-input-file */
	obj_t BGl__openzd2inputzd2filez00zz__r4_ports_6_10_1z00(obj_t
		BgL_env1256z00_72, obj_t BgL_opt1255z00_71)
	{
		{	/* Ieee/port.scm 1084 */
			{	/* Ieee/port.scm 1084 */
				obj_t BgL_g1257z00_1738;

				BgL_g1257z00_1738 = VECTOR_REF(BgL_opt1255z00_71, 0L);
				switch (VECTOR_LENGTH(BgL_opt1255z00_71))
					{
					case 1L:

						{	/* Ieee/port.scm 1084 */

							{	/* Ieee/port.scm 1084 */
								obj_t BgL_auxz00_5544;

								if (STRINGP(BgL_g1257z00_1738))
									{	/* Ieee/port.scm 1084 */
										BgL_auxz00_5544 = BgL_g1257z00_1738;
									}
								else
									{
										obj_t BgL_auxz00_5547;

										BgL_auxz00_5547 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(47820L),
											BGl_string2474z00zz__r4_ports_6_10_1z00,
											BGl_string2404z00zz__r4_ports_6_10_1z00,
											BgL_g1257z00_1738);
										FAILURE(BgL_auxz00_5547, BFALSE, BFALSE);
									}
								return
									BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00
									(BgL_auxz00_5544, BTRUE, BINT(5000000L));
							}
						}
						break;
					case 2L:

						{	/* Ieee/port.scm 1084 */
							obj_t BgL_bufinfoz00_1743;

							BgL_bufinfoz00_1743 = VECTOR_REF(BgL_opt1255z00_71, 1L);
							{	/* Ieee/port.scm 1084 */

								{	/* Ieee/port.scm 1084 */
									obj_t BgL_auxz00_5554;

									if (STRINGP(BgL_g1257z00_1738))
										{	/* Ieee/port.scm 1084 */
											BgL_auxz00_5554 = BgL_g1257z00_1738;
										}
									else
										{
											obj_t BgL_auxz00_5557;

											BgL_auxz00_5557 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(47820L),
												BGl_string2474z00zz__r4_ports_6_10_1z00,
												BGl_string2404z00zz__r4_ports_6_10_1z00,
												BgL_g1257z00_1738);
											FAILURE(BgL_auxz00_5557, BFALSE, BFALSE);
										}
									return
										BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00
										(BgL_auxz00_5554, BgL_bufinfoz00_1743, BINT(5000000L));
								}
							}
						}
						break;
					case 3L:

						{	/* Ieee/port.scm 1084 */
							obj_t BgL_bufinfoz00_1745;

							BgL_bufinfoz00_1745 = VECTOR_REF(BgL_opt1255z00_71, 1L);
							{	/* Ieee/port.scm 1084 */
								obj_t BgL_timeoutz00_1746;

								BgL_timeoutz00_1746 = VECTOR_REF(BgL_opt1255z00_71, 2L);
								{	/* Ieee/port.scm 1084 */

									{	/* Ieee/port.scm 1084 */
										obj_t BgL_auxz00_5565;

										if (STRINGP(BgL_g1257z00_1738))
											{	/* Ieee/port.scm 1084 */
												BgL_auxz00_5565 = BgL_g1257z00_1738;
											}
										else
											{
												obj_t BgL_auxz00_5568;

												BgL_auxz00_5568 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2402z00zz__r4_ports_6_10_1z00,
													BINT(47820L), BGl_string2474z00zz__r4_ports_6_10_1z00,
													BGl_string2404z00zz__r4_ports_6_10_1z00,
													BgL_g1257z00_1738);
												FAILURE(BgL_auxz00_5568, BFALSE, BFALSE);
											}
										return
											BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00
											(BgL_auxz00_5565, BgL_bufinfoz00_1745,
											BgL_timeoutz00_1746);
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* open-input-file */
	BGL_EXPORTED_DEF obj_t BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(obj_t
		BgL_stringz00_68, obj_t BgL_bufinfoz00_69, obj_t BgL_timeoutz00_70)
	{
		{	/* Ieee/port.scm 1084 */
			{	/* Ieee/port.scm 1085 */
				obj_t BgL_bufferz00_1747;

				{	/* Ieee/port.scm 1085 */
					obj_t BgL_res2018z00_2773;

					{	/* Ieee/port.scm 1085 */
						int BgL_defsiza7za7_2768;

						BgL_defsiza7za7_2768 = (int) (default_io_bufsiz);
						if ((BgL_bufinfoz00_69 == BTRUE))
							{	/* Ieee/port.scm 974 */
								BgL_res2018z00_2773 =
									make_string_sans_fill((long) (BgL_defsiza7za7_2768));
							}
						else
							{	/* Ieee/port.scm 974 */
								if ((BgL_bufinfoz00_69 == BFALSE))
									{	/* Ieee/port.scm 976 */
										BgL_res2018z00_2773 = make_string_sans_fill(2L);
									}
								else
									{	/* Ieee/port.scm 976 */
										if (STRINGP(BgL_bufinfoz00_69))
											{	/* Ieee/port.scm 978 */
												BgL_res2018z00_2773 = BgL_bufinfoz00_69;
											}
										else
											{	/* Ieee/port.scm 978 */
												if (INTEGERP(BgL_bufinfoz00_69))
													{	/* Ieee/port.scm 980 */
														if (((long) CINT(BgL_bufinfoz00_69) >= 2L))
															{	/* Ieee/port.scm 981 */
																BgL_res2018z00_2773 =
																	make_string_sans_fill(
																	(long) CINT(BgL_bufinfoz00_69));
															}
														else
															{	/* Ieee/port.scm 981 */
																BgL_res2018z00_2773 = make_string_sans_fill(2L);
															}
													}
												else
													{	/* Ieee/port.scm 980 */
														BgL_res2018z00_2773 =
															BGl_errorz00zz__errorz00
															(BGl_string2448z00zz__r4_ports_6_10_1z00,
															BGl_string2444z00zz__r4_ports_6_10_1z00,
															BgL_bufinfoz00_69);
													}
											}
									}
							}
					}
					BgL_bufferz00_1747 = BgL_res2018z00_2773;
				}
				{
					obj_t BgL_protosz00_1749;

					BgL_protosz00_1749 =
						BGl_za2inputzd2portzd2protocolsza2z00zz__r4_ports_6_10_1z00;
				BgL_zc3z04anonymousza31510ze3z87_1750:
					if (NULLP(BgL_protosz00_1749))
						{	/* Ieee/port.scm 1087 */
							return bgl_open_input_file(BgL_stringz00_68, BgL_bufferz00_1747);
						}
					else
						{	/* Ieee/port.scm 1090 */
							obj_t BgL_cellz00_1752;

							BgL_cellz00_1752 = CAR(((obj_t) BgL_protosz00_1749));
							{	/* Ieee/port.scm 1090 */
								obj_t BgL_identz00_1753;

								BgL_identz00_1753 = CAR(((obj_t) BgL_cellz00_1752));
								{	/* Ieee/port.scm 1091 */
									long BgL_lz00_1754;

									BgL_lz00_1754 = STRING_LENGTH(((obj_t) BgL_identz00_1753));
									{	/* Ieee/port.scm 1092 */
										obj_t BgL_openz00_1755;

										BgL_openz00_1755 = CDR(((obj_t) BgL_cellz00_1752));
										{	/* Ieee/port.scm 1093 */

											if (bigloo_strncmp(BgL_stringz00_68,
													((obj_t) BgL_identz00_1753), BgL_lz00_1754))
												{	/* Ieee/port.scm 1096 */
													obj_t BgL_namez00_1757;

													BgL_namez00_1757 =
														c_substring(BgL_stringz00_68, BgL_lz00_1754,
														STRING_LENGTH(BgL_stringz00_68));
													return
														BGL_PROCEDURE_CALL3(BgL_openz00_1755,
														BgL_namez00_1757, BgL_bufferz00_1747,
														BgL_timeoutz00_70);
												}
											else
												{	/* Ieee/port.scm 1098 */
													obj_t BgL_arg1514z00_1759;

													BgL_arg1514z00_1759 =
														CDR(((obj_t) BgL_protosz00_1749));
													{
														obj_t BgL_protosz00_5618;

														BgL_protosz00_5618 = BgL_arg1514z00_1759;
														BgL_protosz00_1749 = BgL_protosz00_5618;
														goto BgL_zc3z04anonymousza31510ze3z87_1750;
													}
												}
										}
									}
								}
							}
						}
				}
			}
		}

	}



/* _open-input-descriptor */
	obj_t BGl__openzd2inputzd2descriptorz00zz__r4_ports_6_10_1z00(obj_t
		BgL_env1261z00_76, obj_t BgL_opt1260z00_75)
	{
		{	/* Ieee/port.scm 1103 */
			{	/* Ieee/port.scm 1103 */
				obj_t BgL_g1262z00_1761;

				BgL_g1262z00_1761 = VECTOR_REF(BgL_opt1260z00_75, 0L);
				switch (VECTOR_LENGTH(BgL_opt1260z00_75))
					{
					case 1L:

						{	/* Ieee/port.scm 1103 */

							{	/* Ieee/port.scm 1103 */
								int BgL_fdz00_2786;

								{	/* Ieee/port.scm 1103 */
									obj_t BgL_tmpz00_5620;

									if (INTEGERP(BgL_g1262z00_1761))
										{	/* Ieee/port.scm 1103 */
											BgL_tmpz00_5620 = BgL_g1262z00_1761;
										}
									else
										{
											obj_t BgL_auxz00_5623;

											BgL_auxz00_5623 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(48665L),
												BGl_string2475z00zz__r4_ports_6_10_1z00,
												BGl_string2446z00zz__r4_ports_6_10_1z00,
												BgL_g1262z00_1761);
											FAILURE(BgL_auxz00_5623, BFALSE, BFALSE);
										}
									BgL_fdz00_2786 = CINT(BgL_tmpz00_5620);
								}
								return
									bgl_open_input_descriptor(BgL_fdz00_2786,
									BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00
									(BGl_string2448z00zz__r4_ports_6_10_1z00, BTRUE,
										(int) (default_io_bufsiz)));
						}} break;
					case 2L:

						{	/* Ieee/port.scm 1103 */
							obj_t BgL_bufinfoz00_1765;

							BgL_bufinfoz00_1765 = VECTOR_REF(BgL_opt1260z00_75, 1L);
							{	/* Ieee/port.scm 1103 */

								{	/* Ieee/port.scm 1103 */
									int BgL_fdz00_2788;

									{	/* Ieee/port.scm 1103 */
										obj_t BgL_tmpz00_5632;

										if (INTEGERP(BgL_g1262z00_1761))
											{	/* Ieee/port.scm 1103 */
												BgL_tmpz00_5632 = BgL_g1262z00_1761;
											}
										else
											{
												obj_t BgL_auxz00_5635;

												BgL_auxz00_5635 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2402z00zz__r4_ports_6_10_1z00,
													BINT(48665L), BGl_string2475z00zz__r4_ports_6_10_1z00,
													BGl_string2446z00zz__r4_ports_6_10_1z00,
													BgL_g1262z00_1761);
												FAILURE(BgL_auxz00_5635, BFALSE, BFALSE);
											}
										BgL_fdz00_2788 = CINT(BgL_tmpz00_5632);
									}
									return
										bgl_open_input_descriptor(BgL_fdz00_2788,
										BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00
										(BGl_string2448z00zz__r4_ports_6_10_1z00,
											BgL_bufinfoz00_1765, (int) (default_io_bufsiz)));
						}}} break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* open-input-descriptor */
	BGL_EXPORTED_DEF obj_t
		BGl_openzd2inputzd2descriptorz00zz__r4_ports_6_10_1z00(int BgL_fdz00_73,
		obj_t BgL_bufinfoz00_74)
	{
		{	/* Ieee/port.scm 1103 */
			{	/* Ieee/port.scm 1104 */
				obj_t BgL_bufferz00_2790;

				{	/* Ieee/port.scm 1104 */
					obj_t BgL_res2019z00_2796;

					{	/* Ieee/port.scm 1104 */
						int BgL_defsiza7za7_2791;

						BgL_defsiza7za7_2791 = (int) (default_io_bufsiz);
						if ((BgL_bufinfoz00_74 == BTRUE))
							{	/* Ieee/port.scm 974 */
								BgL_res2019z00_2796 =
									make_string_sans_fill((long) (BgL_defsiza7za7_2791));
							}
						else
							{	/* Ieee/port.scm 974 */
								if ((BgL_bufinfoz00_74 == BFALSE))
									{	/* Ieee/port.scm 976 */
										BgL_res2019z00_2796 = make_string_sans_fill(2L);
									}
								else
									{	/* Ieee/port.scm 976 */
										if (STRINGP(BgL_bufinfoz00_74))
											{	/* Ieee/port.scm 978 */
												BgL_res2019z00_2796 = BgL_bufinfoz00_74;
											}
										else
											{	/* Ieee/port.scm 978 */
												if (INTEGERP(BgL_bufinfoz00_74))
													{	/* Ieee/port.scm 980 */
														if (((long) CINT(BgL_bufinfoz00_74) >= 2L))
															{	/* Ieee/port.scm 981 */
																BgL_res2019z00_2796 =
																	make_string_sans_fill(
																	(long) CINT(BgL_bufinfoz00_74));
															}
														else
															{	/* Ieee/port.scm 981 */
																BgL_res2019z00_2796 = make_string_sans_fill(2L);
															}
													}
												else
													{	/* Ieee/port.scm 980 */
														BgL_res2019z00_2796 =
															BGl_errorz00zz__errorz00
															(BGl_string2448z00zz__r4_ports_6_10_1z00,
															BGl_string2444z00zz__r4_ports_6_10_1z00,
															BgL_bufinfoz00_74);
													}
											}
									}
							}
					}
					BgL_bufferz00_2790 = BgL_res2019z00_2796;
				}
				return bgl_open_input_descriptor(BgL_fdz00_73, BgL_bufferz00_2790);
			}
		}

	}



/* _open-input-string */
	obj_t BGl__openzd2inputzd2stringz00zz__r4_ports_6_10_1z00(obj_t
		BgL_env1266z00_81, obj_t BgL_opt1265z00_80)
	{
		{	/* Ieee/port.scm 1110 */
			{	/* Ieee/port.scm 1110 */
				obj_t BgL_stringz00_1767;

				BgL_stringz00_1767 = VECTOR_REF(BgL_opt1265z00_80, 0L);
				switch (VECTOR_LENGTH(BgL_opt1265z00_80))
					{
					case 1L:

						{	/* Ieee/port.scm 1111 */
							long BgL_endz00_1771;

							{	/* Ieee/port.scm 1111 */
								obj_t BgL_stringz00_2797;

								if (STRINGP(BgL_stringz00_1767))
									{	/* Ieee/port.scm 1111 */
										BgL_stringz00_2797 = BgL_stringz00_1767;
									}
								else
									{
										obj_t BgL_auxz00_5668;

										BgL_auxz00_5668 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(49140L),
											BGl_string2476z00zz__r4_ports_6_10_1z00,
											BGl_string2404z00zz__r4_ports_6_10_1z00,
											BgL_stringz00_1767);
										FAILURE(BgL_auxz00_5668, BFALSE, BFALSE);
									}
								BgL_endz00_1771 = STRING_LENGTH(BgL_stringz00_2797);
							}
							{	/* Ieee/port.scm 1110 */

								{	/* Ieee/port.scm 1110 */
									obj_t BgL_auxz00_5673;

									if (STRINGP(BgL_stringz00_1767))
										{	/* Ieee/port.scm 1110 */
											BgL_auxz00_5673 = BgL_stringz00_1767;
										}
									else
										{
											obj_t BgL_auxz00_5676;

											BgL_auxz00_5676 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(49076L),
												BGl_string2476z00zz__r4_ports_6_10_1z00,
												BGl_string2404z00zz__r4_ports_6_10_1z00,
												BgL_stringz00_1767);
											FAILURE(BgL_auxz00_5676, BFALSE, BFALSE);
										}
									return
										BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00
										(BgL_auxz00_5673, BINT(0L), BINT(BgL_endz00_1771));
								}
							}
						}
						break;
					case 2L:

						{	/* Ieee/port.scm 1110 */
							obj_t BgL_startz00_1772;

							BgL_startz00_1772 = VECTOR_REF(BgL_opt1265z00_80, 1L);
							{	/* Ieee/port.scm 1111 */
								long BgL_endz00_1773;

								{	/* Ieee/port.scm 1111 */
									obj_t BgL_stringz00_2798;

									if (STRINGP(BgL_stringz00_1767))
										{	/* Ieee/port.scm 1111 */
											BgL_stringz00_2798 = BgL_stringz00_1767;
										}
									else
										{
											obj_t BgL_auxz00_5686;

											BgL_auxz00_5686 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(49140L),
												BGl_string2476z00zz__r4_ports_6_10_1z00,
												BGl_string2404z00zz__r4_ports_6_10_1z00,
												BgL_stringz00_1767);
											FAILURE(BgL_auxz00_5686, BFALSE, BFALSE);
										}
									BgL_endz00_1773 = STRING_LENGTH(BgL_stringz00_2798);
								}
								{	/* Ieee/port.scm 1110 */

									{	/* Ieee/port.scm 1110 */
										obj_t BgL_auxz00_5691;

										if (STRINGP(BgL_stringz00_1767))
											{	/* Ieee/port.scm 1110 */
												BgL_auxz00_5691 = BgL_stringz00_1767;
											}
										else
											{
												obj_t BgL_auxz00_5694;

												BgL_auxz00_5694 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2402z00zz__r4_ports_6_10_1z00,
													BINT(49076L), BGl_string2476z00zz__r4_ports_6_10_1z00,
													BGl_string2404z00zz__r4_ports_6_10_1z00,
													BgL_stringz00_1767);
												FAILURE(BgL_auxz00_5694, BFALSE, BFALSE);
											}
										return
											BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00
											(BgL_auxz00_5691, BgL_startz00_1772,
											BINT(BgL_endz00_1773));
									}
								}
							}
						}
						break;
					case 3L:

						{	/* Ieee/port.scm 1110 */
							obj_t BgL_startz00_1774;

							BgL_startz00_1774 = VECTOR_REF(BgL_opt1265z00_80, 1L);
							{	/* Ieee/port.scm 1110 */
								obj_t BgL_endz00_1775;

								BgL_endz00_1775 = VECTOR_REF(BgL_opt1265z00_80, 2L);
								{	/* Ieee/port.scm 1110 */

									{	/* Ieee/port.scm 1110 */
										obj_t BgL_auxz00_5702;

										if (STRINGP(BgL_stringz00_1767))
											{	/* Ieee/port.scm 1110 */
												BgL_auxz00_5702 = BgL_stringz00_1767;
											}
										else
											{
												obj_t BgL_auxz00_5705;

												BgL_auxz00_5705 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2402z00zz__r4_ports_6_10_1z00,
													BINT(49076L), BGl_string2476z00zz__r4_ports_6_10_1z00,
													BGl_string2404z00zz__r4_ports_6_10_1z00,
													BgL_stringz00_1767);
												FAILURE(BgL_auxz00_5705, BFALSE, BFALSE);
											}
										return
											BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00
											(BgL_auxz00_5702, BgL_startz00_1774, BgL_endz00_1775);
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* open-input-string */
	BGL_EXPORTED_DEF obj_t
		BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00(obj_t BgL_stringz00_77,
		obj_t BgL_startz00_78, obj_t BgL_endz00_79)
	{
		{	/* Ieee/port.scm 1110 */
			if (((long) CINT(BgL_startz00_78) < 0L))
				{	/* Ieee/port.scm 1113 */
					return
						BGl_errorz00zz__errorz00(BGl_string2477z00zz__r4_ports_6_10_1z00,
						BGl_string2478z00zz__r4_ports_6_10_1z00, BgL_startz00_78);
				}
			else
				{	/* Ieee/port.scm 1113 */
					if (((long) CINT(BgL_startz00_78) > STRING_LENGTH(BgL_stringz00_77)))
						{	/* Ieee/port.scm 1115 */
							return
								BGl_errorz00zz__errorz00
								(BGl_string2477z00zz__r4_ports_6_10_1z00,
								BGl_string2479z00zz__r4_ports_6_10_1z00, BgL_startz00_78);
						}
					else
						{	/* Ieee/port.scm 1115 */
							if (((long) CINT(BgL_startz00_78) > (long) CINT(BgL_endz00_79)))
								{	/* Ieee/port.scm 1117 */
									return
										BGl_errorz00zz__errorz00
										(BGl_string2477z00zz__r4_ports_6_10_1z00,
										BGl_string2480z00zz__r4_ports_6_10_1z00, BgL_startz00_78);
								}
							else
								{	/* Ieee/port.scm 1117 */
									if (
										((long) CINT(BgL_endz00_79) >
											STRING_LENGTH(BgL_stringz00_77)))
										{	/* Ieee/port.scm 1119 */
											return
												BGl_errorz00zz__errorz00
												(BGl_string2477z00zz__r4_ports_6_10_1z00,
												BGl_string2481z00zz__r4_ports_6_10_1z00, BgL_endz00_79);
										}
									else
										{	/* Ieee/port.scm 1119 */
											return
												bgl_open_input_substring(BgL_stringz00_77,
												(long) CINT(BgL_startz00_78),
												(long) CINT(BgL_endz00_79));
		}}}}}

	}



/* _open-input-string! */
	obj_t BGl__openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t
		BgL_env1270z00_86, obj_t BgL_opt1269z00_85)
	{
		{	/* Ieee/port.scm 1127 */
			{	/* Ieee/port.scm 1127 */
				obj_t BgL_stringz00_1784;

				BgL_stringz00_1784 = VECTOR_REF(BgL_opt1269z00_85, 0L);
				switch (VECTOR_LENGTH(BgL_opt1269z00_85))
					{
					case 1L:

						{	/* Ieee/port.scm 1128 */
							long BgL_endz00_1788;

							{	/* Ieee/port.scm 1128 */
								obj_t BgL_stringz00_2808;

								if (STRINGP(BgL_stringz00_1784))
									{	/* Ieee/port.scm 1128 */
										BgL_stringz00_2808 = BgL_stringz00_1784;
									}
								else
									{
										obj_t BgL_auxz00_5737;

										BgL_auxz00_5737 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(49928L),
											BGl_string2482z00zz__r4_ports_6_10_1z00,
											BGl_string2404z00zz__r4_ports_6_10_1z00,
											BgL_stringz00_1784);
										FAILURE(BgL_auxz00_5737, BFALSE, BFALSE);
									}
								BgL_endz00_1788 = STRING_LENGTH(BgL_stringz00_2808);
							}
							{	/* Ieee/port.scm 1127 */

								{	/* Ieee/port.scm 1127 */
									obj_t BgL_auxz00_5742;

									if (STRINGP(BgL_stringz00_1784))
										{	/* Ieee/port.scm 1127 */
											BgL_auxz00_5742 = BgL_stringz00_1784;
										}
									else
										{
											obj_t BgL_auxz00_5745;

											BgL_auxz00_5745 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(49863L),
												BGl_string2482z00zz__r4_ports_6_10_1z00,
												BGl_string2404z00zz__r4_ports_6_10_1z00,
												BgL_stringz00_1784);
											FAILURE(BgL_auxz00_5745, BFALSE, BFALSE);
										}
									return
										BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
										(BgL_auxz00_5742, BINT(0L), BINT(BgL_endz00_1788));
								}
							}
						}
						break;
					case 2L:

						{	/* Ieee/port.scm 1127 */
							obj_t BgL_startz00_1789;

							BgL_startz00_1789 = VECTOR_REF(BgL_opt1269z00_85, 1L);
							{	/* Ieee/port.scm 1128 */
								long BgL_endz00_1790;

								{	/* Ieee/port.scm 1128 */
									obj_t BgL_stringz00_2809;

									if (STRINGP(BgL_stringz00_1784))
										{	/* Ieee/port.scm 1128 */
											BgL_stringz00_2809 = BgL_stringz00_1784;
										}
									else
										{
											obj_t BgL_auxz00_5755;

											BgL_auxz00_5755 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(49928L),
												BGl_string2482z00zz__r4_ports_6_10_1z00,
												BGl_string2404z00zz__r4_ports_6_10_1z00,
												BgL_stringz00_1784);
											FAILURE(BgL_auxz00_5755, BFALSE, BFALSE);
										}
									BgL_endz00_1790 = STRING_LENGTH(BgL_stringz00_2809);
								}
								{	/* Ieee/port.scm 1127 */

									{	/* Ieee/port.scm 1127 */
										obj_t BgL_auxz00_5760;

										if (STRINGP(BgL_stringz00_1784))
											{	/* Ieee/port.scm 1127 */
												BgL_auxz00_5760 = BgL_stringz00_1784;
											}
										else
											{
												obj_t BgL_auxz00_5763;

												BgL_auxz00_5763 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2402z00zz__r4_ports_6_10_1z00,
													BINT(49863L), BGl_string2482z00zz__r4_ports_6_10_1z00,
													BGl_string2404z00zz__r4_ports_6_10_1z00,
													BgL_stringz00_1784);
												FAILURE(BgL_auxz00_5763, BFALSE, BFALSE);
											}
										return
											BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
											(BgL_auxz00_5760, BgL_startz00_1789,
											BINT(BgL_endz00_1790));
									}
								}
							}
						}
						break;
					case 3L:

						{	/* Ieee/port.scm 1127 */
							obj_t BgL_startz00_1791;

							BgL_startz00_1791 = VECTOR_REF(BgL_opt1269z00_85, 1L);
							{	/* Ieee/port.scm 1127 */
								obj_t BgL_endz00_1792;

								BgL_endz00_1792 = VECTOR_REF(BgL_opt1269z00_85, 2L);
								{	/* Ieee/port.scm 1127 */

									{	/* Ieee/port.scm 1127 */
										obj_t BgL_auxz00_5771;

										if (STRINGP(BgL_stringz00_1784))
											{	/* Ieee/port.scm 1127 */
												BgL_auxz00_5771 = BgL_stringz00_1784;
											}
										else
											{
												obj_t BgL_auxz00_5774;

												BgL_auxz00_5774 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2402z00zz__r4_ports_6_10_1z00,
													BINT(49863L), BGl_string2482z00zz__r4_ports_6_10_1z00,
													BGl_string2404z00zz__r4_ports_6_10_1z00,
													BgL_stringz00_1784);
												FAILURE(BgL_auxz00_5774, BFALSE, BFALSE);
											}
										return
											BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
											(BgL_auxz00_5771, BgL_startz00_1791, BgL_endz00_1792);
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* open-input-string! */
	BGL_EXPORTED_DEF obj_t
		BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t
		BgL_stringz00_82, obj_t BgL_startz00_83, obj_t BgL_endz00_84)
	{
		{	/* Ieee/port.scm 1127 */
			if (((long) CINT(BgL_startz00_83) < 0L))
				{	/* Ieee/port.scm 1130 */
					return
						BGl_errorz00zz__errorz00(BGl_string2483z00zz__r4_ports_6_10_1z00,
						BGl_string2478z00zz__r4_ports_6_10_1z00, BgL_startz00_83);
				}
			else
				{	/* Ieee/port.scm 1130 */
					if (((long) CINT(BgL_startz00_83) > STRING_LENGTH(BgL_stringz00_82)))
						{	/* Ieee/port.scm 1132 */
							return
								BGl_errorz00zz__errorz00
								(BGl_string2483z00zz__r4_ports_6_10_1z00,
								BGl_string2479z00zz__r4_ports_6_10_1z00, BgL_startz00_83);
						}
					else
						{	/* Ieee/port.scm 1132 */
							if (((long) CINT(BgL_startz00_83) > (long) CINT(BgL_endz00_84)))
								{	/* Ieee/port.scm 1134 */
									return
										BGl_errorz00zz__errorz00
										(BGl_string2483z00zz__r4_ports_6_10_1z00,
										BGl_string2480z00zz__r4_ports_6_10_1z00, BgL_startz00_83);
								}
							else
								{	/* Ieee/port.scm 1134 */
									if (
										((long) CINT(BgL_endz00_84) >
											STRING_LENGTH(BgL_stringz00_82)))
										{	/* Ieee/port.scm 1136 */
											return
												BGl_errorz00zz__errorz00
												(BGl_string2483z00zz__r4_ports_6_10_1z00,
												BGl_string2481z00zz__r4_ports_6_10_1z00, BgL_endz00_84);
										}
									else
										{	/* Ieee/port.scm 1136 */
											return
												bgl_open_input_substring_bang(BgL_stringz00_82,
												(long) CINT(BgL_startz00_83),
												(long) CINT(BgL_endz00_84));
		}}}}}

	}



/* _open-input-mmap */
	obj_t BGl__openzd2inputzd2mmapz00zz__r4_ports_6_10_1z00(obj_t
		BgL_env1274z00_91, obj_t BgL_opt1273z00_90)
	{
		{	/* Ieee/port.scm 1144 */
			{	/* Ieee/port.scm 1144 */
				obj_t BgL_mmapz00_1801;

				BgL_mmapz00_1801 = VECTOR_REF(BgL_opt1273z00_90, 0L);
				switch (VECTOR_LENGTH(BgL_opt1273z00_90))
					{
					case 1L:

						{	/* Ieee/port.scm 1145 */
							long BgL_endz00_1805;

							{	/* Ieee/port.scm 1145 */
								long BgL_arg1536z00_1806;

								{	/* Ieee/port.scm 1145 */
									obj_t BgL_objz00_2819;

									if (BGL_MMAPP(BgL_mmapz00_1801))
										{	/* Ieee/port.scm 1145 */
											BgL_objz00_2819 = BgL_mmapz00_1801;
										}
									else
										{
											obj_t BgL_auxz00_5806;

											BgL_auxz00_5806 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(50750L),
												BGl_string2484z00zz__r4_ports_6_10_1z00,
												BGl_string2485z00zz__r4_ports_6_10_1z00,
												BgL_mmapz00_1801);
											FAILURE(BgL_auxz00_5806, BFALSE, BFALSE);
										}
									BgL_arg1536z00_1806 = BGL_MMAP_LENGTH(BgL_objz00_2819);
								}
								BgL_endz00_1805 = (long) (BgL_arg1536z00_1806);
							}
							{	/* Ieee/port.scm 1144 */

								{	/* Ieee/port.scm 1144 */
									obj_t BgL_auxz00_5812;

									if (BGL_MMAPP(BgL_mmapz00_1801))
										{	/* Ieee/port.scm 1144 */
											BgL_auxz00_5812 = BgL_mmapz00_1801;
										}
									else
										{
											obj_t BgL_auxz00_5815;

											BgL_auxz00_5815 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(50656L),
												BGl_string2484z00zz__r4_ports_6_10_1z00,
												BGl_string2485z00zz__r4_ports_6_10_1z00,
												BgL_mmapz00_1801);
											FAILURE(BgL_auxz00_5815, BFALSE, BFALSE);
										}
									return
										BGl_openzd2inputzd2mmapz00zz__r4_ports_6_10_1z00
										(BgL_auxz00_5812, BINT(0L), BINT(BgL_endz00_1805));
								}
							}
						}
						break;
					case 2L:

						{	/* Ieee/port.scm 1144 */
							obj_t BgL_startz00_1807;

							BgL_startz00_1807 = VECTOR_REF(BgL_opt1273z00_90, 1L);
							{	/* Ieee/port.scm 1145 */
								long BgL_endz00_1808;

								{	/* Ieee/port.scm 1145 */
									long BgL_arg1537z00_1809;

									{	/* Ieee/port.scm 1145 */
										obj_t BgL_objz00_2821;

										if (BGL_MMAPP(BgL_mmapz00_1801))
											{	/* Ieee/port.scm 1145 */
												BgL_objz00_2821 = BgL_mmapz00_1801;
											}
										else
											{
												obj_t BgL_auxz00_5825;

												BgL_auxz00_5825 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2402z00zz__r4_ports_6_10_1z00,
													BINT(50750L), BGl_string2484z00zz__r4_ports_6_10_1z00,
													BGl_string2485z00zz__r4_ports_6_10_1z00,
													BgL_mmapz00_1801);
												FAILURE(BgL_auxz00_5825, BFALSE, BFALSE);
											}
										BgL_arg1537z00_1809 = BGL_MMAP_LENGTH(BgL_objz00_2821);
									}
									BgL_endz00_1808 = (long) (BgL_arg1537z00_1809);
								}
								{	/* Ieee/port.scm 1144 */

									{	/* Ieee/port.scm 1144 */
										obj_t BgL_auxz00_5831;

										if (BGL_MMAPP(BgL_mmapz00_1801))
											{	/* Ieee/port.scm 1144 */
												BgL_auxz00_5831 = BgL_mmapz00_1801;
											}
										else
											{
												obj_t BgL_auxz00_5834;

												BgL_auxz00_5834 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2402z00zz__r4_ports_6_10_1z00,
													BINT(50656L), BGl_string2484z00zz__r4_ports_6_10_1z00,
													BGl_string2485z00zz__r4_ports_6_10_1z00,
													BgL_mmapz00_1801);
												FAILURE(BgL_auxz00_5834, BFALSE, BFALSE);
											}
										return
											BGl_openzd2inputzd2mmapz00zz__r4_ports_6_10_1z00
											(BgL_auxz00_5831, BgL_startz00_1807,
											BINT(BgL_endz00_1808));
									}
								}
							}
						}
						break;
					case 3L:

						{	/* Ieee/port.scm 1144 */
							obj_t BgL_startz00_1810;

							BgL_startz00_1810 = VECTOR_REF(BgL_opt1273z00_90, 1L);
							{	/* Ieee/port.scm 1144 */
								obj_t BgL_endz00_1811;

								BgL_endz00_1811 = VECTOR_REF(BgL_opt1273z00_90, 2L);
								{	/* Ieee/port.scm 1144 */

									{	/* Ieee/port.scm 1144 */
										obj_t BgL_auxz00_5842;

										if (BGL_MMAPP(BgL_mmapz00_1801))
											{	/* Ieee/port.scm 1144 */
												BgL_auxz00_5842 = BgL_mmapz00_1801;
											}
										else
											{
												obj_t BgL_auxz00_5845;

												BgL_auxz00_5845 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2402z00zz__r4_ports_6_10_1z00,
													BINT(50656L), BGl_string2484z00zz__r4_ports_6_10_1z00,
													BGl_string2485z00zz__r4_ports_6_10_1z00,
													BgL_mmapz00_1801);
												FAILURE(BgL_auxz00_5845, BFALSE, BFALSE);
											}
										return
											BGl_openzd2inputzd2mmapz00zz__r4_ports_6_10_1z00
											(BgL_auxz00_5842, BgL_startz00_1810, BgL_endz00_1811);
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* open-input-mmap */
	BGL_EXPORTED_DEF obj_t BGl_openzd2inputzd2mmapz00zz__r4_ports_6_10_1z00(obj_t
		BgL_mmapz00_87, obj_t BgL_startz00_88, obj_t BgL_endz00_89)
	{
		{	/* Ieee/port.scm 1144 */
			if (((long) CINT(BgL_startz00_88) < 0L))
				{	/* Ieee/port.scm 1147 */
					return
						BGl_errorz00zz__errorz00(BGl_string2486z00zz__r4_ports_6_10_1z00,
						BGl_string2478z00zz__r4_ports_6_10_1z00, BgL_startz00_88);
				}
			else
				{	/* Ieee/port.scm 1149 */
					bool_t BgL_test3054z00_5856;

					{	/* Ieee/port.scm 1149 */
						long BgL_arg1554z00_1827;

						{	/* Ieee/port.scm 1149 */
							long BgL_arg1555z00_1828;

							BgL_arg1555z00_1828 = BGL_MMAP_LENGTH(BgL_mmapz00_87);
							BgL_arg1554z00_1827 = (long) (BgL_arg1555z00_1828);
						}
						BgL_test3054z00_5856 =
							((long) CINT(BgL_startz00_88) > BgL_arg1554z00_1827);
					}
					if (BgL_test3054z00_5856)
						{	/* Ieee/port.scm 1149 */
							return
								BGl_errorz00zz__errorz00
								(BGl_string2486z00zz__r4_ports_6_10_1z00,
								BGl_string2479z00zz__r4_ports_6_10_1z00, BgL_startz00_88);
						}
					else
						{	/* Ieee/port.scm 1149 */
							if (((long) CINT(BgL_startz00_88) > (long) CINT(BgL_endz00_89)))
								{	/* Ieee/port.scm 1151 */
									return
										BGl_errorz00zz__errorz00
										(BGl_string2486z00zz__r4_ports_6_10_1z00,
										BGl_string2480z00zz__r4_ports_6_10_1z00, BgL_startz00_88);
								}
							else
								{	/* Ieee/port.scm 1153 */
									bool_t BgL_test3056z00_5867;

									{	/* Ieee/port.scm 1153 */
										long BgL_arg1552z00_1825;

										{	/* Ieee/port.scm 1153 */
											long BgL_arg1553z00_1826;

											BgL_arg1553z00_1826 = BGL_MMAP_LENGTH(BgL_mmapz00_87);
											BgL_arg1552z00_1825 = (long) (BgL_arg1553z00_1826);
										}
										BgL_test3056z00_5867 =
											((long) CINT(BgL_endz00_89) > BgL_arg1552z00_1825);
									}
									if (BgL_test3056z00_5867)
										{	/* Ieee/port.scm 1153 */
											return
												BGl_errorz00zz__errorz00
												(BGl_string2486z00zz__r4_ports_6_10_1z00,
												BGl_string2481z00zz__r4_ports_6_10_1z00, BgL_endz00_89);
										}
									else
										{	/* Ieee/port.scm 1153 */
											return
												bgl_open_input_mmap(BgL_mmapz00_87,
												make_string_sans_fill(2L),
												(long) CINT(BgL_startz00_88),
												(long) CINT(BgL_endz00_89));
		}}}}}

	}



/* _open-input-procedure */
	obj_t BGl__openzd2inputzd2procedurez00zz__r4_ports_6_10_1z00(obj_t
		BgL_env1278z00_95, obj_t BgL_opt1277z00_94)
	{
		{	/* Ieee/port.scm 1165 */
			{	/* Ieee/port.scm 1165 */
				obj_t BgL_g1279z00_1829;

				BgL_g1279z00_1829 = VECTOR_REF(BgL_opt1277z00_94, 0L);
				switch (VECTOR_LENGTH(BgL_opt1277z00_94))
					{
					case 1L:

						{	/* Ieee/port.scm 1165 */

							{	/* Ieee/port.scm 1165 */
								obj_t BgL_procz00_2846;

								if (PROCEDUREP(BgL_g1279z00_1829))
									{	/* Ieee/port.scm 1165 */
										BgL_procz00_2846 = BgL_g1279z00_1829;
									}
								else
									{
										obj_t BgL_auxz00_5880;

										BgL_auxz00_5880 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(51639L),
											BGl_string2487z00zz__r4_ports_6_10_1z00,
											BGl_string2405z00zz__r4_ports_6_10_1z00,
											BgL_g1279z00_1829);
										FAILURE(BgL_auxz00_5880, BFALSE, BFALSE);
									}
								return
									bgl_open_input_procedure(BgL_procz00_2846,
									BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00
									(BGl_string2423z00zz__r4_ports_6_10_1z00, BTRUE,
										(int) (1024L)));
						}} break;
					case 2L:

						{	/* Ieee/port.scm 1165 */
							obj_t BgL_bufinfoz00_1833;

							BgL_bufinfoz00_1833 = VECTOR_REF(BgL_opt1277z00_94, 1L);
							{	/* Ieee/port.scm 1165 */

								{	/* Ieee/port.scm 1165 */
									obj_t BgL_procz00_2848;

									if (PROCEDUREP(BgL_g1279z00_1829))
										{	/* Ieee/port.scm 1165 */
											BgL_procz00_2848 = BgL_g1279z00_1829;
										}
									else
										{
											obj_t BgL_auxz00_5890;

											BgL_auxz00_5890 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(51639L),
												BGl_string2487z00zz__r4_ports_6_10_1z00,
												BGl_string2405z00zz__r4_ports_6_10_1z00,
												BgL_g1279z00_1829);
											FAILURE(BgL_auxz00_5890, BFALSE, BFALSE);
										}
									return
										bgl_open_input_procedure(BgL_procz00_2848,
										BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00
										(BGl_string2423z00zz__r4_ports_6_10_1z00,
											BgL_bufinfoz00_1833, (int) (1024L)));
						}}} break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* open-input-procedure */
	BGL_EXPORTED_DEF obj_t
		BGl_openzd2inputzd2procedurez00zz__r4_ports_6_10_1z00(obj_t BgL_procz00_92,
		obj_t BgL_bufinfoz00_93)
	{
		{	/* Ieee/port.scm 1165 */
			{	/* Ieee/port.scm 1166 */
				obj_t BgL_bufz00_2850;

				{	/* Ieee/port.scm 1166 */
					obj_t BgL_res2021z00_2855;

					if ((BgL_bufinfoz00_93 == BTRUE))
						{	/* Ieee/port.scm 974 */
							BgL_res2021z00_2855 = make_string_sans_fill(1024L);
						}
					else
						{	/* Ieee/port.scm 974 */
							if ((BgL_bufinfoz00_93 == BFALSE))
								{	/* Ieee/port.scm 976 */
									BgL_res2021z00_2855 = make_string_sans_fill(2L);
								}
							else
								{	/* Ieee/port.scm 976 */
									if (STRINGP(BgL_bufinfoz00_93))
										{	/* Ieee/port.scm 978 */
											BgL_res2021z00_2855 = BgL_bufinfoz00_93;
										}
									else
										{	/* Ieee/port.scm 978 */
											if (INTEGERP(BgL_bufinfoz00_93))
												{	/* Ieee/port.scm 980 */
													if (((long) CINT(BgL_bufinfoz00_93) >= 2L))
														{	/* Ieee/port.scm 981 */
															BgL_res2021z00_2855 =
																make_string_sans_fill(
																(long) CINT(BgL_bufinfoz00_93));
														}
													else
														{	/* Ieee/port.scm 981 */
															BgL_res2021z00_2855 = make_string_sans_fill(2L);
														}
												}
											else
												{	/* Ieee/port.scm 980 */
													BgL_res2021z00_2855 =
														BGl_errorz00zz__errorz00
														(BGl_string2423z00zz__r4_ports_6_10_1z00,
														BGl_string2444z00zz__r4_ports_6_10_1z00,
														BgL_bufinfoz00_93);
												}
										}
								}
						}
					BgL_bufz00_2850 = BgL_res2021z00_2855;
				}
				return bgl_open_input_procedure(BgL_procz00_92, BgL_bufz00_2850);
			}
		}

	}



/* _open-input-gzip-port */
	obj_t BGl__openzd2inputzd2gza7ipzd2portz75zz__r4_ports_6_10_1z00(obj_t
		BgL_env1283z00_99, obj_t BgL_opt1282z00_98)
	{
		{	/* Ieee/port.scm 1172 */
			{	/* Ieee/port.scm 1172 */
				obj_t BgL_g1284z00_1835;

				BgL_g1284z00_1835 = VECTOR_REF(BgL_opt1282z00_98, 0L);
				switch (VECTOR_LENGTH(BgL_opt1282z00_98))
					{
					case 1L:

						{	/* Ieee/port.scm 1172 */

							{	/* Ieee/port.scm 1172 */
								obj_t BgL_inz00_2856;

								if (INPUT_PORTP(BgL_g1284z00_1835))
									{	/* Ieee/port.scm 1172 */
										BgL_inz00_2856 = BgL_g1284z00_1835;
									}
								else
									{
										obj_t BgL_auxz00_5920;

										BgL_auxz00_5920 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(52033L),
											BGl_string2488z00zz__r4_ports_6_10_1z00,
											BGl_string2417z00zz__r4_ports_6_10_1z00,
											BgL_g1284z00_1835);
										FAILURE(BgL_auxz00_5920, BFALSE, BFALSE);
									}
								return
									BGl_portzd2ze3gza7ipzd2portz44zz__gunza7ipza7(BgL_inz00_2856,
									BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00
									(BGl_string2489z00zz__r4_ports_6_10_1z00, BTRUE,
										(int) (default_io_bufsiz)));
						}} break;
					case 2L:

						{	/* Ieee/port.scm 1172 */
							obj_t BgL_bufinfoz00_1839;

							BgL_bufinfoz00_1839 = VECTOR_REF(BgL_opt1282z00_98, 1L);
							{	/* Ieee/port.scm 1172 */

								{	/* Ieee/port.scm 1172 */
									obj_t BgL_inz00_2858;

									if (INPUT_PORTP(BgL_g1284z00_1835))
										{	/* Ieee/port.scm 1172 */
											BgL_inz00_2858 = BgL_g1284z00_1835;
										}
									else
										{
											obj_t BgL_auxz00_5930;

											BgL_auxz00_5930 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(52033L),
												BGl_string2488z00zz__r4_ports_6_10_1z00,
												BGl_string2417z00zz__r4_ports_6_10_1z00,
												BgL_g1284z00_1835);
											FAILURE(BgL_auxz00_5930, BFALSE, BFALSE);
										}
									return
										BGl_portzd2ze3gza7ipzd2portz44zz__gunza7ipza7
										(BgL_inz00_2858,
										BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00
										(BGl_string2489z00zz__r4_ports_6_10_1z00,
											BgL_bufinfoz00_1839, (int) (default_io_bufsiz)));
						}}} break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* open-input-gzip-port */
	BGL_EXPORTED_DEF obj_t
		BGl_openzd2inputzd2gza7ipzd2portz75zz__r4_ports_6_10_1z00(obj_t
		BgL_inz00_96, obj_t BgL_bufinfoz00_97)
	{
		{	/* Ieee/port.scm 1172 */
			{	/* Ieee/port.scm 1173 */
				obj_t BgL_bufz00_2860;

				{	/* Ieee/port.scm 1173 */
					obj_t BgL_res2022z00_2866;

					{	/* Ieee/port.scm 1173 */
						int BgL_defsiza7za7_2861;

						BgL_defsiza7za7_2861 = (int) (default_io_bufsiz);
						if ((BgL_bufinfoz00_97 == BTRUE))
							{	/* Ieee/port.scm 974 */
								BgL_res2022z00_2866 =
									make_string_sans_fill((long) (BgL_defsiza7za7_2861));
							}
						else
							{	/* Ieee/port.scm 974 */
								if ((BgL_bufinfoz00_97 == BFALSE))
									{	/* Ieee/port.scm 976 */
										BgL_res2022z00_2866 = make_string_sans_fill(2L);
									}
								else
									{	/* Ieee/port.scm 976 */
										if (STRINGP(BgL_bufinfoz00_97))
											{	/* Ieee/port.scm 978 */
												BgL_res2022z00_2866 = BgL_bufinfoz00_97;
											}
										else
											{	/* Ieee/port.scm 978 */
												if (INTEGERP(BgL_bufinfoz00_97))
													{	/* Ieee/port.scm 980 */
														if (((long) CINT(BgL_bufinfoz00_97) >= 2L))
															{	/* Ieee/port.scm 981 */
																BgL_res2022z00_2866 =
																	make_string_sans_fill(
																	(long) CINT(BgL_bufinfoz00_97));
															}
														else
															{	/* Ieee/port.scm 981 */
																BgL_res2022z00_2866 = make_string_sans_fill(2L);
															}
													}
												else
													{	/* Ieee/port.scm 980 */
														BgL_res2022z00_2866 =
															BGl_errorz00zz__errorz00
															(BGl_string2489z00zz__r4_ports_6_10_1z00,
															BGl_string2444z00zz__r4_ports_6_10_1z00,
															BgL_bufinfoz00_97);
													}
											}
									}
							}
					}
					BgL_bufz00_2860 = BgL_res2022z00_2866;
				}
				BGL_TAIL return
					BGl_portzd2ze3gza7ipzd2portz44zz__gunza7ipza7(BgL_inz00_96,
					BgL_bufz00_2860);
			}
		}

	}



/* open-input-c-string */
	BGL_EXPORTED_DEF obj_t
		BGl_openzd2inputzd2czd2stringzd2zz__r4_ports_6_10_1z00(char
		*BgL_stringz00_100)
	{
		{	/* Ieee/port.scm 1179 */
			BGL_TAIL return bgl_open_input_c_string(BgL_stringz00_100);
		}

	}



/* &open-input-c-string */
	obj_t BGl_z62openzd2inputzd2czd2stringzb0zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3632, obj_t BgL_stringz00_3633)
	{
		{	/* Ieee/port.scm 1179 */
			{	/* Ieee/port.scm 1180 */
				char *BgL_auxz00_5960;

				{	/* Ieee/port.scm 1180 */
					obj_t BgL_tmpz00_5961;

					if (STRINGP(BgL_stringz00_3633))
						{	/* Ieee/port.scm 1180 */
							BgL_tmpz00_5961 = BgL_stringz00_3633;
						}
					else
						{
							obj_t BgL_auxz00_5964;

							BgL_auxz00_5964 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(52491L),
								BGl_string2490z00zz__r4_ports_6_10_1z00,
								BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_stringz00_3633);
							FAILURE(BgL_auxz00_5964, BFALSE, BFALSE);
						}
					BgL_auxz00_5960 = BSTRING_TO_STRING(BgL_tmpz00_5961);
				}
				return
					BGl_openzd2inputzd2czd2stringzd2zz__r4_ports_6_10_1z00
					(BgL_auxz00_5960);
			}
		}

	}



/* reopen-input-c-string */
	BGL_EXPORTED_DEF obj_t
		BGl_reopenzd2inputzd2czd2stringzd2zz__r4_ports_6_10_1z00(obj_t
		BgL_portz00_101, char *BgL_stringz00_102)
	{
		{	/* Ieee/port.scm 1185 */
			BGL_TAIL return
				bgl_reopen_input_c_string(BgL_portz00_101, BgL_stringz00_102);
		}

	}



/* &reopen-input-c-string */
	obj_t BGl_z62reopenzd2inputzd2czd2stringzb0zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3634, obj_t BgL_portz00_3635, obj_t BgL_stringz00_3636)
	{
		{	/* Ieee/port.scm 1185 */
			{	/* Ieee/port.scm 1186 */
				char *BgL_auxz00_5978;
				obj_t BgL_auxz00_5971;

				{	/* Ieee/port.scm 1186 */
					obj_t BgL_tmpz00_5979;

					if (STRINGP(BgL_stringz00_3636))
						{	/* Ieee/port.scm 1186 */
							BgL_tmpz00_5979 = BgL_stringz00_3636;
						}
					else
						{
							obj_t BgL_auxz00_5982;

							BgL_auxz00_5982 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(52811L),
								BGl_string2491z00zz__r4_ports_6_10_1z00,
								BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_stringz00_3636);
							FAILURE(BgL_auxz00_5982, BFALSE, BFALSE);
						}
					BgL_auxz00_5978 = BSTRING_TO_STRING(BgL_tmpz00_5979);
				}
				if (INPUT_PORTP(BgL_portz00_3635))
					{	/* Ieee/port.scm 1186 */
						BgL_auxz00_5971 = BgL_portz00_3635;
					}
				else
					{
						obj_t BgL_auxz00_5974;

						BgL_auxz00_5974 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(52811L),
							BGl_string2491z00zz__r4_ports_6_10_1z00,
							BGl_string2417z00zz__r4_ports_6_10_1z00, BgL_portz00_3635);
						FAILURE(BgL_auxz00_5974, BFALSE, BFALSE);
					}
				return
					BGl_reopenzd2inputzd2czd2stringzd2zz__r4_ports_6_10_1z00
					(BgL_auxz00_5971, BgL_auxz00_5978);
			}
		}

	}



/* input-port-timeout */
	BGL_EXPORTED_DEF long
		BGl_inputzd2portzd2timeoutz00zz__r4_ports_6_10_1z00(obj_t BgL_portz00_103)
	{
		{	/* Ieee/port.scm 1191 */
			BGL_TAIL return bgl_input_port_timeout(BgL_portz00_103);
		}

	}



/* &input-port-timeout */
	obj_t BGl_z62inputzd2portzd2timeoutz62zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3637, obj_t BgL_portz00_3638)
	{
		{	/* Ieee/port.scm 1191 */
			{	/* Ieee/port.scm 1192 */
				long BgL_tmpz00_5989;

				{	/* Ieee/port.scm 1192 */
					obj_t BgL_auxz00_5990;

					if (INPUT_PORTP(BgL_portz00_3638))
						{	/* Ieee/port.scm 1192 */
							BgL_auxz00_5990 = BgL_portz00_3638;
						}
					else
						{
							obj_t BgL_auxz00_5993;

							BgL_auxz00_5993 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(53134L),
								BGl_string2492z00zz__r4_ports_6_10_1z00,
								BGl_string2417z00zz__r4_ports_6_10_1z00, BgL_portz00_3638);
							FAILURE(BgL_auxz00_5993, BFALSE, BFALSE);
						}
					BgL_tmpz00_5989 =
						BGl_inputzd2portzd2timeoutz00zz__r4_ports_6_10_1z00
						(BgL_auxz00_5990);
				}
				return BINT(BgL_tmpz00_5989);
			}
		}

	}



/* input-port-timeout-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_inputzd2portzd2timeoutzd2setz12zc0zz__r4_ports_6_10_1z00(obj_t
		BgL_portz00_104, long BgL_timeoutz00_105)
	{
		{	/* Ieee/port.scm 1197 */
			return
				BBOOL(bgl_input_port_timeout_set(BgL_portz00_104, BgL_timeoutz00_105));
		}

	}



/* &input-port-timeout-set! */
	obj_t BGl_z62inputzd2portzd2timeoutzd2setz12za2zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3639, obj_t BgL_portz00_3640, obj_t BgL_timeoutz00_3641)
	{
		{	/* Ieee/port.scm 1197 */
			{	/* Ieee/port.scm 1198 */
				long BgL_auxz00_6008;
				obj_t BgL_auxz00_6001;

				{	/* Ieee/port.scm 1198 */
					obj_t BgL_tmpz00_6009;

					if (INTEGERP(BgL_timeoutz00_3641))
						{	/* Ieee/port.scm 1198 */
							BgL_tmpz00_6009 = BgL_timeoutz00_3641;
						}
					else
						{
							obj_t BgL_auxz00_6012;

							BgL_auxz00_6012 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(53460L),
								BGl_string2493z00zz__r4_ports_6_10_1z00,
								BGl_string2446z00zz__r4_ports_6_10_1z00, BgL_timeoutz00_3641);
							FAILURE(BgL_auxz00_6012, BFALSE, BFALSE);
						}
					BgL_auxz00_6008 = (long) CINT(BgL_tmpz00_6009);
				}
				if (INPUT_PORTP(BgL_portz00_3640))
					{	/* Ieee/port.scm 1198 */
						BgL_auxz00_6001 = BgL_portz00_3640;
					}
				else
					{
						obj_t BgL_auxz00_6004;

						BgL_auxz00_6004 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(53460L),
							BGl_string2493z00zz__r4_ports_6_10_1z00,
							BGl_string2417z00zz__r4_ports_6_10_1z00, BgL_portz00_3640);
						FAILURE(BgL_auxz00_6004, BFALSE, BFALSE);
					}
				return
					BGl_inputzd2portzd2timeoutzd2setz12zc0zz__r4_ports_6_10_1z00
					(BgL_auxz00_6001, BgL_auxz00_6008);
			}
		}

	}



/* output-port-timeout */
	BGL_EXPORTED_DEF long
		BGl_outputzd2portzd2timeoutz00zz__r4_ports_6_10_1z00(obj_t BgL_portz00_106)
	{
		{	/* Ieee/port.scm 1203 */
			BGL_TAIL return bgl_output_port_timeout(BgL_portz00_106);
		}

	}



/* &output-port-timeout */
	obj_t BGl_z62outputzd2portzd2timeoutz62zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3642, obj_t BgL_portz00_3643)
	{
		{	/* Ieee/port.scm 1203 */
			{	/* Ieee/port.scm 1204 */
				long BgL_tmpz00_6019;

				{	/* Ieee/port.scm 1204 */
					obj_t BgL_auxz00_6020;

					if (OUTPUT_PORTP(BgL_portz00_3643))
						{	/* Ieee/port.scm 1204 */
							BgL_auxz00_6020 = BgL_portz00_3643;
						}
					else
						{
							obj_t BgL_auxz00_6023;

							BgL_auxz00_6023 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(53788L),
								BGl_string2494z00zz__r4_ports_6_10_1z00,
								BGl_string2429z00zz__r4_ports_6_10_1z00, BgL_portz00_3643);
							FAILURE(BgL_auxz00_6023, BFALSE, BFALSE);
						}
					BgL_tmpz00_6019 =
						BGl_outputzd2portzd2timeoutz00zz__r4_ports_6_10_1z00
						(BgL_auxz00_6020);
				}
				return BINT(BgL_tmpz00_6019);
			}
		}

	}



/* _open-output-file */
	obj_t BGl__openzd2outputzd2filez00zz__r4_ports_6_10_1z00(obj_t
		BgL_env1288z00_110, obj_t BgL_opt1287z00_109)
	{
		{	/* Ieee/port.scm 1209 */
			{	/* Ieee/port.scm 1209 */
				obj_t BgL_g1289z00_1841;

				BgL_g1289z00_1841 = VECTOR_REF(BgL_opt1287z00_109, 0L);
				switch (VECTOR_LENGTH(BgL_opt1287z00_109))
					{
					case 1L:

						{	/* Ieee/port.scm 1209 */

							{	/* Ieee/port.scm 1209 */
								obj_t BgL_stringz00_2867;

								if (STRINGP(BgL_g1289z00_1841))
									{	/* Ieee/port.scm 1209 */
										BgL_stringz00_2867 = BgL_g1289z00_1841;
									}
								else
									{
										obj_t BgL_auxz00_6032;

										BgL_auxz00_6032 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(54040L),
											BGl_string2495z00zz__r4_ports_6_10_1z00,
											BGl_string2404z00zz__r4_ports_6_10_1z00,
											BgL_g1289z00_1841);
										FAILURE(BgL_auxz00_6032, BFALSE, BFALSE);
									}
								return
									bgl_open_output_file(BgL_stringz00_2867,
									BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00
									(BGl_string2407z00zz__r4_ports_6_10_1z00, BTRUE,
										(int) (default_io_bufsiz)));
						}} break;
					case 2L:

						{	/* Ieee/port.scm 1209 */
							obj_t BgL_bufinfoz00_1845;

							BgL_bufinfoz00_1845 = VECTOR_REF(BgL_opt1287z00_109, 1L);
							{	/* Ieee/port.scm 1209 */

								{	/* Ieee/port.scm 1209 */
									obj_t BgL_stringz00_2869;

									if (STRINGP(BgL_g1289z00_1841))
										{	/* Ieee/port.scm 1209 */
											BgL_stringz00_2869 = BgL_g1289z00_1841;
										}
									else
										{
											obj_t BgL_auxz00_6042;

											BgL_auxz00_6042 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(54040L),
												BGl_string2495z00zz__r4_ports_6_10_1z00,
												BGl_string2404z00zz__r4_ports_6_10_1z00,
												BgL_g1289z00_1841);
											FAILURE(BgL_auxz00_6042, BFALSE, BFALSE);
										}
									return
										bgl_open_output_file(BgL_stringz00_2869,
										BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00
										(BGl_string2407z00zz__r4_ports_6_10_1z00,
											BgL_bufinfoz00_1845, (int) (default_io_bufsiz)));
						}}} break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* open-output-file */
	BGL_EXPORTED_DEF obj_t BGl_openzd2outputzd2filez00zz__r4_ports_6_10_1z00(obj_t
		BgL_stringz00_107, obj_t BgL_bufinfoz00_108)
	{
		{	/* Ieee/port.scm 1209 */
			{	/* Ieee/port.scm 1210 */
				obj_t BgL_bufz00_2871;

				{	/* Ieee/port.scm 1210 */
					obj_t BgL_res2023z00_2877;

					{	/* Ieee/port.scm 1210 */
						int BgL_defsiza7za7_2872;

						BgL_defsiza7za7_2872 = (int) (default_io_bufsiz);
						if ((BgL_bufinfoz00_108 == BTRUE))
							{	/* Ieee/port.scm 974 */
								BgL_res2023z00_2877 =
									make_string_sans_fill((long) (BgL_defsiza7za7_2872));
							}
						else
							{	/* Ieee/port.scm 974 */
								if ((BgL_bufinfoz00_108 == BFALSE))
									{	/* Ieee/port.scm 976 */
										BgL_res2023z00_2877 = make_string_sans_fill(2L);
									}
								else
									{	/* Ieee/port.scm 976 */
										if (STRINGP(BgL_bufinfoz00_108))
											{	/* Ieee/port.scm 978 */
												BgL_res2023z00_2877 = BgL_bufinfoz00_108;
											}
										else
											{	/* Ieee/port.scm 978 */
												if (INTEGERP(BgL_bufinfoz00_108))
													{	/* Ieee/port.scm 980 */
														if (((long) CINT(BgL_bufinfoz00_108) >= 2L))
															{	/* Ieee/port.scm 981 */
																BgL_res2023z00_2877 =
																	make_string_sans_fill(
																	(long) CINT(BgL_bufinfoz00_108));
															}
														else
															{	/* Ieee/port.scm 981 */
																BgL_res2023z00_2877 = make_string_sans_fill(2L);
															}
													}
												else
													{	/* Ieee/port.scm 980 */
														BgL_res2023z00_2877 =
															BGl_errorz00zz__errorz00
															(BGl_string2407z00zz__r4_ports_6_10_1z00,
															BGl_string2444z00zz__r4_ports_6_10_1z00,
															BgL_bufinfoz00_108);
													}
											}
									}
							}
					}
					BgL_bufz00_2871 = BgL_res2023z00_2877;
				}
				return bgl_open_output_file(BgL_stringz00_107, BgL_bufz00_2871);
			}
		}

	}



/* _append-output-file */
	obj_t BGl__appendzd2outputzd2filez00zz__r4_ports_6_10_1z00(obj_t
		BgL_env1293z00_114, obj_t BgL_opt1292z00_113)
	{
		{	/* Ieee/port.scm 1216 */
			{	/* Ieee/port.scm 1216 */
				obj_t BgL_g1294z00_1847;

				BgL_g1294z00_1847 = VECTOR_REF(BgL_opt1292z00_113, 0L);
				switch (VECTOR_LENGTH(BgL_opt1292z00_113))
					{
					case 1L:

						{	/* Ieee/port.scm 1216 */

							{	/* Ieee/port.scm 1216 */
								obj_t BgL_stringz00_2878;

								if (STRINGP(BgL_g1294z00_1847))
									{	/* Ieee/port.scm 1216 */
										BgL_stringz00_2878 = BgL_g1294z00_1847;
									}
								else
									{
										obj_t BgL_auxz00_6074;

										BgL_auxz00_6074 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(54441L),
											BGl_string2496z00zz__r4_ports_6_10_1z00,
											BGl_string2404z00zz__r4_ports_6_10_1z00,
											BgL_g1294z00_1847);
										FAILURE(BgL_auxz00_6074, BFALSE, BFALSE);
									}
								return
									bgl_append_output_file(BgL_stringz00_2878,
									BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00
									(BGl_string2410z00zz__r4_ports_6_10_1z00, BTRUE,
										(int) (default_io_bufsiz)));
						}} break;
					case 2L:

						{	/* Ieee/port.scm 1216 */
							obj_t BgL_bufinfoz00_1851;

							BgL_bufinfoz00_1851 = VECTOR_REF(BgL_opt1292z00_113, 1L);
							{	/* Ieee/port.scm 1216 */

								{	/* Ieee/port.scm 1216 */
									obj_t BgL_stringz00_2880;

									if (STRINGP(BgL_g1294z00_1847))
										{	/* Ieee/port.scm 1216 */
											BgL_stringz00_2880 = BgL_g1294z00_1847;
										}
									else
										{
											obj_t BgL_auxz00_6084;

											BgL_auxz00_6084 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(54441L),
												BGl_string2496z00zz__r4_ports_6_10_1z00,
												BGl_string2404z00zz__r4_ports_6_10_1z00,
												BgL_g1294z00_1847);
											FAILURE(BgL_auxz00_6084, BFALSE, BFALSE);
										}
									return
										bgl_append_output_file(BgL_stringz00_2880,
										BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00
										(BGl_string2410z00zz__r4_ports_6_10_1z00,
											BgL_bufinfoz00_1851, (int) (default_io_bufsiz)));
						}}} break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* append-output-file */
	BGL_EXPORTED_DEF obj_t
		BGl_appendzd2outputzd2filez00zz__r4_ports_6_10_1z00(obj_t BgL_stringz00_111,
		obj_t BgL_bufinfoz00_112)
	{
		{	/* Ieee/port.scm 1216 */
			{	/* Ieee/port.scm 1217 */
				obj_t BgL_bufz00_2882;

				{	/* Ieee/port.scm 1217 */
					obj_t BgL_res2024z00_2888;

					{	/* Ieee/port.scm 1217 */
						int BgL_defsiza7za7_2883;

						BgL_defsiza7za7_2883 = (int) (default_io_bufsiz);
						if ((BgL_bufinfoz00_112 == BTRUE))
							{	/* Ieee/port.scm 974 */
								BgL_res2024z00_2888 =
									make_string_sans_fill((long) (BgL_defsiza7za7_2883));
							}
						else
							{	/* Ieee/port.scm 974 */
								if ((BgL_bufinfoz00_112 == BFALSE))
									{	/* Ieee/port.scm 976 */
										BgL_res2024z00_2888 = make_string_sans_fill(2L);
									}
								else
									{	/* Ieee/port.scm 976 */
										if (STRINGP(BgL_bufinfoz00_112))
											{	/* Ieee/port.scm 978 */
												BgL_res2024z00_2888 = BgL_bufinfoz00_112;
											}
										else
											{	/* Ieee/port.scm 978 */
												if (INTEGERP(BgL_bufinfoz00_112))
													{	/* Ieee/port.scm 980 */
														if (((long) CINT(BgL_bufinfoz00_112) >= 2L))
															{	/* Ieee/port.scm 981 */
																BgL_res2024z00_2888 =
																	make_string_sans_fill(
																	(long) CINT(BgL_bufinfoz00_112));
															}
														else
															{	/* Ieee/port.scm 981 */
																BgL_res2024z00_2888 = make_string_sans_fill(2L);
															}
													}
												else
													{	/* Ieee/port.scm 980 */
														BgL_res2024z00_2888 =
															BGl_errorz00zz__errorz00
															(BGl_string2410z00zz__r4_ports_6_10_1z00,
															BGl_string2444z00zz__r4_ports_6_10_1z00,
															BgL_bufinfoz00_112);
													}
											}
									}
							}
					}
					BgL_bufz00_2882 = BgL_res2024z00_2888;
				}
				return bgl_append_output_file(BgL_stringz00_111, BgL_bufz00_2882);
			}
		}

	}



/* _open-output-string */
	obj_t BGl__openzd2outputzd2stringz00zz__r4_ports_6_10_1z00(obj_t
		BgL_env1298z00_117, obj_t BgL_opt1297z00_116)
	{
		{	/* Ieee/port.scm 1223 */
			{	/* Ieee/port.scm 1223 */

				switch (VECTOR_LENGTH(BgL_opt1297z00_116))
					{
					case 0L:

						{	/* Ieee/port.scm 1223 */

							return
								bgl_open_output_string
								(BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00
								(BGl_string2407z00zz__r4_ports_6_10_1z00, BTRUE, (int) (128L)));
						} break;
					case 1L:

						{	/* Ieee/port.scm 1223 */
							obj_t BgL_bufinfoz00_1856;

							BgL_bufinfoz00_1856 = VECTOR_REF(BgL_opt1297z00_116, 0L);
							{	/* Ieee/port.scm 1223 */

								return
									bgl_open_output_string
									(BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00
									(BGl_string2407z00zz__r4_ports_6_10_1z00, BgL_bufinfoz00_1856,
										(int) (128L)));
						}} break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* open-output-string */
	BGL_EXPORTED_DEF obj_t
		BGl_openzd2outputzd2stringz00zz__r4_ports_6_10_1z00(obj_t
		BgL_bufinfoz00_115)
	{
		{	/* Ieee/port.scm 1223 */
			{	/* Ieee/port.scm 1224 */
				obj_t BgL_bufz00_2891;

				{	/* Ieee/port.scm 1224 */
					obj_t BgL_res2025z00_2896;

					if ((BgL_bufinfoz00_115 == BTRUE))
						{	/* Ieee/port.scm 974 */
							BgL_res2025z00_2896 = make_string_sans_fill(128L);
						}
					else
						{	/* Ieee/port.scm 974 */
							if ((BgL_bufinfoz00_115 == BFALSE))
								{	/* Ieee/port.scm 976 */
									BgL_res2025z00_2896 = make_string_sans_fill(2L);
								}
							else
								{	/* Ieee/port.scm 976 */
									if (STRINGP(BgL_bufinfoz00_115))
										{	/* Ieee/port.scm 978 */
											BgL_res2025z00_2896 = BgL_bufinfoz00_115;
										}
									else
										{	/* Ieee/port.scm 978 */
											if (INTEGERP(BgL_bufinfoz00_115))
												{	/* Ieee/port.scm 980 */
													if (((long) CINT(BgL_bufinfoz00_115) >= 2L))
														{	/* Ieee/port.scm 981 */
															BgL_res2025z00_2896 =
																make_string_sans_fill(
																(long) CINT(BgL_bufinfoz00_115));
														}
													else
														{	/* Ieee/port.scm 981 */
															BgL_res2025z00_2896 = make_string_sans_fill(2L);
														}
												}
											else
												{	/* Ieee/port.scm 980 */
													BgL_res2025z00_2896 =
														BGl_errorz00zz__errorz00
														(BGl_string2407z00zz__r4_ports_6_10_1z00,
														BGl_string2444z00zz__r4_ports_6_10_1z00,
														BgL_bufinfoz00_115);
												}
										}
								}
						}
					BgL_bufz00_2891 = BgL_res2025z00_2896;
				}
				return bgl_open_output_string(BgL_bufz00_2891);
			}
		}

	}



/* _open-output-procedure */
	obj_t BGl__openzd2outputzd2procedurez00zz__r4_ports_6_10_1z00(obj_t
		BgL_env1302z00_123, obj_t BgL_opt1301z00_122)
	{
		{	/* Ieee/port.scm 1230 */
			{	/* Ieee/port.scm 1230 */
				obj_t BgL_g1303z00_1858;

				BgL_g1303z00_1858 = VECTOR_REF(BgL_opt1301z00_122, 0L);
				switch (VECTOR_LENGTH(BgL_opt1301z00_122))
					{
					case 1L:

						{	/* Ieee/port.scm 1230 */

							{	/* Ieee/port.scm 1230 */
								obj_t BgL_auxz00_6141;

								if (PROCEDUREP(BgL_g1303z00_1858))
									{	/* Ieee/port.scm 1230 */
										BgL_auxz00_6141 = BgL_g1303z00_1858;
									}
								else
									{
										obj_t BgL_auxz00_6144;

										BgL_auxz00_6144 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(55223L),
											BGl_string2499z00zz__r4_ports_6_10_1z00,
											BGl_string2405z00zz__r4_ports_6_10_1z00,
											BgL_g1303z00_1858);
										FAILURE(BgL_auxz00_6144, BFALSE, BFALSE);
									}
								return
									BGl_openzd2outputzd2procedurez00zz__r4_ports_6_10_1z00
									(BgL_auxz00_6141, BGl_proc2497z00zz__r4_ports_6_10_1z00,
									BTRUE, BGl_proc2498z00zz__r4_ports_6_10_1z00);
							}
						}
						break;
					case 2L:

						{	/* Ieee/port.scm 1230 */
							obj_t BgL_flushz00_1868;

							BgL_flushz00_1868 = VECTOR_REF(BgL_opt1301z00_122, 1L);
							{	/* Ieee/port.scm 1230 */

								{	/* Ieee/port.scm 1230 */
									obj_t BgL_auxz00_6157;
									obj_t BgL_auxz00_6150;

									if (PROCEDUREP(BgL_flushz00_1868))
										{	/* Ieee/port.scm 1230 */
											BgL_auxz00_6157 = BgL_flushz00_1868;
										}
									else
										{
											obj_t BgL_auxz00_6160;

											BgL_auxz00_6160 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(55223L),
												BGl_string2499z00zz__r4_ports_6_10_1z00,
												BGl_string2405z00zz__r4_ports_6_10_1z00,
												BgL_flushz00_1868);
											FAILURE(BgL_auxz00_6160, BFALSE, BFALSE);
										}
									if (PROCEDUREP(BgL_g1303z00_1858))
										{	/* Ieee/port.scm 1230 */
											BgL_auxz00_6150 = BgL_g1303z00_1858;
										}
									else
										{
											obj_t BgL_auxz00_6153;

											BgL_auxz00_6153 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(55223L),
												BGl_string2499z00zz__r4_ports_6_10_1z00,
												BGl_string2405z00zz__r4_ports_6_10_1z00,
												BgL_g1303z00_1858);
											FAILURE(BgL_auxz00_6153, BFALSE, BFALSE);
										}
									return
										BGl_openzd2outputzd2procedurez00zz__r4_ports_6_10_1z00
										(BgL_auxz00_6150, BgL_auxz00_6157, BTRUE,
										BGl_proc2500z00zz__r4_ports_6_10_1z00);
								}
							}
						}
						break;
					case 3L:

						{	/* Ieee/port.scm 1230 */
							obj_t BgL_flushz00_1873;

							BgL_flushz00_1873 = VECTOR_REF(BgL_opt1301z00_122, 1L);
							{	/* Ieee/port.scm 1230 */
								obj_t BgL_bufinfoz00_1874;

								BgL_bufinfoz00_1874 = VECTOR_REF(BgL_opt1301z00_122, 2L);
								{	/* Ieee/port.scm 1230 */

									{	/* Ieee/port.scm 1230 */
										obj_t BgL_auxz00_6174;
										obj_t BgL_auxz00_6167;

										if (PROCEDUREP(BgL_flushz00_1873))
											{	/* Ieee/port.scm 1230 */
												BgL_auxz00_6174 = BgL_flushz00_1873;
											}
										else
											{
												obj_t BgL_auxz00_6177;

												BgL_auxz00_6177 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2402z00zz__r4_ports_6_10_1z00,
													BINT(55223L), BGl_string2499z00zz__r4_ports_6_10_1z00,
													BGl_string2405z00zz__r4_ports_6_10_1z00,
													BgL_flushz00_1873);
												FAILURE(BgL_auxz00_6177, BFALSE, BFALSE);
											}
										if (PROCEDUREP(BgL_g1303z00_1858))
											{	/* Ieee/port.scm 1230 */
												BgL_auxz00_6167 = BgL_g1303z00_1858;
											}
										else
											{
												obj_t BgL_auxz00_6170;

												BgL_auxz00_6170 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2402z00zz__r4_ports_6_10_1z00,
													BINT(55223L), BGl_string2499z00zz__r4_ports_6_10_1z00,
													BGl_string2405z00zz__r4_ports_6_10_1z00,
													BgL_g1303z00_1858);
												FAILURE(BgL_auxz00_6170, BFALSE, BFALSE);
											}
										return
											BGl_openzd2outputzd2procedurez00zz__r4_ports_6_10_1z00
											(BgL_auxz00_6167, BgL_auxz00_6174, BgL_bufinfoz00_1874,
											BGl_proc2501z00zz__r4_ports_6_10_1z00);
									}
								}
							}
						}
						break;
					case 4L:

						{	/* Ieee/port.scm 1230 */
							obj_t BgL_flushz00_1878;

							BgL_flushz00_1878 = VECTOR_REF(BgL_opt1301z00_122, 1L);
							{	/* Ieee/port.scm 1230 */
								obj_t BgL_bufinfoz00_1879;

								BgL_bufinfoz00_1879 = VECTOR_REF(BgL_opt1301z00_122, 2L);
								{	/* Ieee/port.scm 1230 */
									obj_t BgL_closez00_1880;

									BgL_closez00_1880 = VECTOR_REF(BgL_opt1301z00_122, 3L);
									{	/* Ieee/port.scm 1230 */

										{	/* Ieee/port.scm 1230 */
											obj_t BgL_auxz00_6199;
											obj_t BgL_auxz00_6192;
											obj_t BgL_auxz00_6185;

											if (PROCEDUREP(BgL_closez00_1880))
												{	/* Ieee/port.scm 1230 */
													BgL_auxz00_6199 = BgL_closez00_1880;
												}
											else
												{
													obj_t BgL_auxz00_6202;

													BgL_auxz00_6202 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2402z00zz__r4_ports_6_10_1z00,
														BINT(55223L),
														BGl_string2499z00zz__r4_ports_6_10_1z00,
														BGl_string2405z00zz__r4_ports_6_10_1z00,
														BgL_closez00_1880);
													FAILURE(BgL_auxz00_6202, BFALSE, BFALSE);
												}
											if (PROCEDUREP(BgL_flushz00_1878))
												{	/* Ieee/port.scm 1230 */
													BgL_auxz00_6192 = BgL_flushz00_1878;
												}
											else
												{
													obj_t BgL_auxz00_6195;

													BgL_auxz00_6195 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2402z00zz__r4_ports_6_10_1z00,
														BINT(55223L),
														BGl_string2499z00zz__r4_ports_6_10_1z00,
														BGl_string2405z00zz__r4_ports_6_10_1z00,
														BgL_flushz00_1878);
													FAILURE(BgL_auxz00_6195, BFALSE, BFALSE);
												}
											if (PROCEDUREP(BgL_g1303z00_1858))
												{	/* Ieee/port.scm 1230 */
													BgL_auxz00_6185 = BgL_g1303z00_1858;
												}
											else
												{
													obj_t BgL_auxz00_6188;

													BgL_auxz00_6188 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2402z00zz__r4_ports_6_10_1z00,
														BINT(55223L),
														BGl_string2499z00zz__r4_ports_6_10_1z00,
														BGl_string2405z00zz__r4_ports_6_10_1z00,
														BgL_g1303z00_1858);
													FAILURE(BgL_auxz00_6188, BFALSE, BFALSE);
												}
											return
												BGl_openzd2outputzd2procedurez00zz__r4_ports_6_10_1z00
												(BgL_auxz00_6185, BgL_auxz00_6192, BgL_bufinfoz00_1879,
												BgL_auxz00_6199);
										}
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* &close2051 */
	obj_t BGl_z62close2051z62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3648)
	{
		{	/* Ieee/port.scm 1234 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &close2052 */
	obj_t BGl_z62close2052z62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3649)
	{
		{	/* Ieee/port.scm 1234 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &close2053 */
	obj_t BGl_z62close2053z62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3650)
	{
		{	/* Ieee/port.scm 1234 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &flush2054 */
	obj_t BGl_z62flush2054z62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3651)
	{
		{	/* Ieee/port.scm 1232 */
			return BBOOL(((bool_t) 0));
		}

	}



/* open-output-procedure */
	BGL_EXPORTED_DEF obj_t
		BGl_openzd2outputzd2procedurez00zz__r4_ports_6_10_1z00(obj_t
		BgL_procz00_118, obj_t BgL_flushz00_119, obj_t BgL_bufinfoz00_120,
		obj_t BgL_closez00_121)
	{
		{	/* Ieee/port.scm 1230 */
			if (PROCEDURE_CORRECT_ARITYP(BgL_procz00_118, (int) (1L)))
				{	/* Ieee/port.scm 1241 */
					bool_t BgL_test3106z00_6216;

					if (PROCEDURE_CORRECT_ARITYP(BgL_flushz00_119, (int) (0L)))
						{	/* Ieee/port.scm 1242 */
							BgL_test3106z00_6216 = ((bool_t) 0);
						}
					else
						{	/* Ieee/port.scm 1242 */
							BgL_test3106z00_6216 = ((bool_t) 1);
						}
					if (BgL_test3106z00_6216)
						{	/* Ieee/port.scm 1241 */
							return
								bgl_system_failure(BGL_IO_PORT_ERROR,
								BGl_string2502z00zz__r4_ports_6_10_1z00,
								BGl_string2503z00zz__r4_ports_6_10_1z00, BgL_flushz00_119);
						}
					else
						{	/* Ieee/port.scm 1247 */
							bool_t BgL_test3108z00_6221;

							if (PROCEDURE_CORRECT_ARITYP(BgL_closez00_121, (int) (0L)))
								{	/* Ieee/port.scm 1248 */
									BgL_test3108z00_6221 = ((bool_t) 0);
								}
							else
								{	/* Ieee/port.scm 1248 */
									BgL_test3108z00_6221 = ((bool_t) 1);
								}
							if (BgL_test3108z00_6221)
								{	/* Ieee/port.scm 1247 */
									return
										bgl_system_failure(BGL_IO_PORT_ERROR,
										BGl_string2502z00zz__r4_ports_6_10_1z00,
										BGl_string2504z00zz__r4_ports_6_10_1z00, BgL_flushz00_119);
								}
							else
								{	/* Ieee/port.scm 1254 */
									obj_t BgL_bufz00_1885;

									{	/* Ieee/port.scm 1254 */
										obj_t BgL_res2026z00_2901;

										if ((BgL_bufinfoz00_120 == BTRUE))
											{	/* Ieee/port.scm 974 */
												BgL_res2026z00_2901 = make_string_sans_fill(128L);
											}
										else
											{	/* Ieee/port.scm 974 */
												if ((BgL_bufinfoz00_120 == BFALSE))
													{	/* Ieee/port.scm 976 */
														BgL_res2026z00_2901 = make_string_sans_fill(2L);
													}
												else
													{	/* Ieee/port.scm 976 */
														if (STRINGP(BgL_bufinfoz00_120))
															{	/* Ieee/port.scm 978 */
																BgL_res2026z00_2901 = BgL_bufinfoz00_120;
															}
														else
															{	/* Ieee/port.scm 978 */
																if (INTEGERP(BgL_bufinfoz00_120))
																	{	/* Ieee/port.scm 980 */
																		if (((long) CINT(BgL_bufinfoz00_120) >= 2L))
																			{	/* Ieee/port.scm 981 */
																				BgL_res2026z00_2901 =
																					make_string_sans_fill(
																					(long) CINT(BgL_bufinfoz00_120));
																			}
																		else
																			{	/* Ieee/port.scm 981 */
																				BgL_res2026z00_2901 =
																					make_string_sans_fill(2L);
																			}
																	}
																else
																	{	/* Ieee/port.scm 980 */
																		BgL_res2026z00_2901 =
																			BGl_errorz00zz__errorz00
																			(BGl_string2502z00zz__r4_ports_6_10_1z00,
																			BGl_string2444z00zz__r4_ports_6_10_1z00,
																			BgL_bufinfoz00_120);
																	}
															}
													}
											}
										BgL_bufz00_1885 = BgL_res2026z00_2901;
									}
									return
										bgl_open_output_procedure(BgL_procz00_118, BgL_flushz00_119,
										BgL_closez00_121, BgL_bufz00_1885);
								}
						}
				}
			else
				{	/* Ieee/port.scm 1236 */
					return
						bgl_system_failure(BGL_IO_PORT_ERROR,
						BGl_string2502z00zz__r4_ports_6_10_1z00,
						BGl_string2505z00zz__r4_ports_6_10_1z00, BgL_procz00_118);
				}
		}

	}



/* output-port-timeout-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_outputzd2portzd2timeoutzd2setz12zc0zz__r4_ports_6_10_1z00(obj_t
		BgL_portz00_124, long BgL_timeoutz00_125)
	{
		{	/* Ieee/port.scm 1260 */
			return
				BBOOL(bgl_output_port_timeout_set(BgL_portz00_124, BgL_timeoutz00_125));
		}

	}



/* &output-port-timeout-set! */
	obj_t BGl_z62outputzd2portzd2timeoutzd2setz12za2zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3652, obj_t BgL_portz00_3653, obj_t BgL_timeoutz00_3654)
	{
		{	/* Ieee/port.scm 1260 */
			{	/* Ieee/port.scm 1261 */
				long BgL_auxz00_6254;
				obj_t BgL_auxz00_6247;

				{	/* Ieee/port.scm 1261 */
					obj_t BgL_tmpz00_6255;

					if (INTEGERP(BgL_timeoutz00_3654))
						{	/* Ieee/port.scm 1261 */
							BgL_tmpz00_6255 = BgL_timeoutz00_3654;
						}
					else
						{
							obj_t BgL_auxz00_6258;

							BgL_auxz00_6258 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(56320L),
								BGl_string2506z00zz__r4_ports_6_10_1z00,
								BGl_string2446z00zz__r4_ports_6_10_1z00, BgL_timeoutz00_3654);
							FAILURE(BgL_auxz00_6258, BFALSE, BFALSE);
						}
					BgL_auxz00_6254 = (long) CINT(BgL_tmpz00_6255);
				}
				if (OUTPUT_PORTP(BgL_portz00_3653))
					{	/* Ieee/port.scm 1261 */
						BgL_auxz00_6247 = BgL_portz00_3653;
					}
				else
					{
						obj_t BgL_auxz00_6250;

						BgL_auxz00_6250 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(56320L),
							BGl_string2506z00zz__r4_ports_6_10_1z00,
							BGl_string2429z00zz__r4_ports_6_10_1z00, BgL_portz00_3653);
						FAILURE(BgL_auxz00_6250, BFALSE, BFALSE);
					}
				return
					BGl_outputzd2portzd2timeoutzd2setz12zc0zz__r4_ports_6_10_1z00
					(BgL_auxz00_6247, BgL_auxz00_6254);
			}
		}

	}



/* closed-input-port? */
	BGL_EXPORTED_DEF bool_t
		BGl_closedzd2inputzd2portzf3zf3zz__r4_ports_6_10_1z00(obj_t BgL_portz00_126)
	{
		{	/* Ieee/port.scm 1266 */
			return INPUT_PORT_CLOSEP(BgL_portz00_126);
		}

	}



/* &closed-input-port? */
	obj_t BGl_z62closedzd2inputzd2portzf3z91zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3655, obj_t BgL_portz00_3656)
	{
		{	/* Ieee/port.scm 1266 */
			{	/* Ieee/port.scm 1267 */
				bool_t BgL_tmpz00_6265;

				{	/* Ieee/port.scm 1267 */
					obj_t BgL_auxz00_6266;

					if (INPUT_PORTP(BgL_portz00_3656))
						{	/* Ieee/port.scm 1267 */
							BgL_auxz00_6266 = BgL_portz00_3656;
						}
					else
						{
							obj_t BgL_auxz00_6269;

							BgL_auxz00_6269 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(56629L),
								BGl_string2507z00zz__r4_ports_6_10_1z00,
								BGl_string2417z00zz__r4_ports_6_10_1z00, BgL_portz00_3656);
							FAILURE(BgL_auxz00_6269, BFALSE, BFALSE);
						}
					BgL_tmpz00_6265 =
						BGl_closedzd2inputzd2portzf3zf3zz__r4_ports_6_10_1z00
						(BgL_auxz00_6266);
				}
				return BBOOL(BgL_tmpz00_6265);
			}
		}

	}



/* close-input-port */
	BGL_EXPORTED_DEF obj_t BGl_closezd2inputzd2portz00zz__r4_ports_6_10_1z00(obj_t
		BgL_portz00_127)
	{
		{	/* Ieee/port.scm 1272 */
			return bgl_close_input_port(BgL_portz00_127);
		}

	}



/* &close-input-port */
	obj_t BGl_z62closezd2inputzd2portz62zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3657, obj_t BgL_portz00_3658)
	{
		{	/* Ieee/port.scm 1272 */
			{	/* Ieee/port.scm 1273 */
				obj_t BgL_auxz00_6276;

				if (INPUT_PORTP(BgL_portz00_3658))
					{	/* Ieee/port.scm 1273 */
						BgL_auxz00_6276 = BgL_portz00_3658;
					}
				else
					{
						obj_t BgL_auxz00_6279;

						BgL_auxz00_6279 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(56923L),
							BGl_string2508z00zz__r4_ports_6_10_1z00,
							BGl_string2417z00zz__r4_ports_6_10_1z00, BgL_portz00_3658);
						FAILURE(BgL_auxz00_6279, BFALSE, BFALSE);
					}
				return
					BGl_closezd2inputzd2portz00zz__r4_ports_6_10_1z00(BgL_auxz00_6276);
			}
		}

	}



/* get-output-string */
	BGL_EXPORTED_DEF obj_t
		BGl_getzd2outputzd2stringz00zz__r4_ports_6_10_1z00(obj_t BgL_portz00_128)
	{
		{	/* Ieee/port.scm 1278 */
			BGL_TAIL return get_output_string(BgL_portz00_128);
		}

	}



/* &get-output-string */
	obj_t BGl_z62getzd2outputzd2stringz62zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3659, obj_t BgL_portz00_3660)
	{
		{	/* Ieee/port.scm 1278 */
			{	/* Ieee/port.scm 1279 */
				obj_t BgL_auxz00_6285;

				if (OUTPUT_PORTP(BgL_portz00_3660))
					{	/* Ieee/port.scm 1279 */
						BgL_auxz00_6285 = BgL_portz00_3660;
					}
				else
					{
						obj_t BgL_auxz00_6288;

						BgL_auxz00_6288 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(57215L),
							BGl_string2509z00zz__r4_ports_6_10_1z00,
							BGl_string2429z00zz__r4_ports_6_10_1z00, BgL_portz00_3660);
						FAILURE(BgL_auxz00_6288, BFALSE, BFALSE);
					}
				return
					BGl_getzd2outputzd2stringz00zz__r4_ports_6_10_1z00(BgL_auxz00_6285);
			}
		}

	}



/* close-output-port */
	BGL_EXPORTED_DEF obj_t
		BGl_closezd2outputzd2portz00zz__r4_ports_6_10_1z00(obj_t BgL_portz00_129)
	{
		{	/* Ieee/port.scm 1284 */
			BGL_TAIL return bgl_close_output_port(BgL_portz00_129);
		}

	}



/* &close-output-port */
	obj_t BGl_z62closezd2outputzd2portz62zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3661, obj_t BgL_portz00_3662)
	{
		{	/* Ieee/port.scm 1284 */
			{	/* Ieee/port.scm 1285 */
				obj_t BgL_auxz00_6294;

				if (OUTPUT_PORTP(BgL_portz00_3662))
					{	/* Ieee/port.scm 1285 */
						BgL_auxz00_6294 = BgL_portz00_3662;
					}
				else
					{
						obj_t BgL_auxz00_6297;

						BgL_auxz00_6297 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(57509L),
							BGl_string2510z00zz__r4_ports_6_10_1z00,
							BGl_string2429z00zz__r4_ports_6_10_1z00, BgL_portz00_3662);
						FAILURE(BgL_auxz00_6297, BFALSE, BFALSE);
					}
				return
					BGl_closezd2outputzd2portz00zz__r4_ports_6_10_1z00(BgL_auxz00_6294);
			}
		}

	}



/* flush-output-port */
	BGL_EXPORTED_DEF obj_t
		BGl_flushzd2outputzd2portz00zz__r4_ports_6_10_1z00(obj_t BgL_portz00_130)
	{
		{	/* Ieee/port.scm 1290 */
			return bgl_flush_output_port(BgL_portz00_130);
		}

	}



/* &flush-output-port */
	obj_t BGl_z62flushzd2outputzd2portz62zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3663, obj_t BgL_portz00_3664)
	{
		{	/* Ieee/port.scm 1290 */
			{	/* Ieee/port.scm 1291 */
				obj_t BgL_auxz00_6303;

				if (OUTPUT_PORTP(BgL_portz00_3664))
					{	/* Ieee/port.scm 1291 */
						BgL_auxz00_6303 = BgL_portz00_3664;
					}
				else
					{
						obj_t BgL_auxz00_6306;

						BgL_auxz00_6306 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(57802L),
							BGl_string2511z00zz__r4_ports_6_10_1z00,
							BGl_string2429z00zz__r4_ports_6_10_1z00, BgL_portz00_3664);
						FAILURE(BgL_auxz00_6306, BFALSE, BFALSE);
					}
				return
					BGl_flushzd2outputzd2portz00zz__r4_ports_6_10_1z00(BgL_auxz00_6303);
			}
		}

	}



/* reset-output-port */
	BGL_EXPORTED_DEF obj_t
		BGl_resetzd2outputzd2portz00zz__r4_ports_6_10_1z00(obj_t BgL_portz00_131)
	{
		{	/* Ieee/port.scm 1296 */
			bgl_reset_output_port_error(BgL_portz00_131);
			if (BGL_OUTPUT_STRING_PORTP(BgL_portz00_131))
				{	/* Ieee/port.scm 1298 */
					return bgl_reset_output_string_port(BgL_portz00_131);
				}
			else
				{	/* Ieee/port.scm 1298 */
					return bgl_flush_output_port(BgL_portz00_131);
				}
		}

	}



/* &reset-output-port */
	obj_t BGl_z62resetzd2outputzd2portz62zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3665, obj_t BgL_portz00_3666)
	{
		{	/* Ieee/port.scm 1296 */
			{	/* Ieee/port.scm 1297 */
				obj_t BgL_auxz00_6316;

				if (OUTPUT_PORTP(BgL_portz00_3666))
					{	/* Ieee/port.scm 1297 */
						BgL_auxz00_6316 = BgL_portz00_3666;
					}
				else
					{
						obj_t BgL_auxz00_6319;

						BgL_auxz00_6319 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(58126L),
							BGl_string2512z00zz__r4_ports_6_10_1z00,
							BGl_string2429z00zz__r4_ports_6_10_1z00, BgL_portz00_3666);
						FAILURE(BgL_auxz00_6319, BFALSE, BFALSE);
					}
				return
					BGl_resetzd2outputzd2portz00zz__r4_ports_6_10_1z00(BgL_auxz00_6316);
			}
		}

	}



/* reset-eof */
	BGL_EXPORTED_DEF bool_t BGl_resetzd2eofzd2zz__r4_ports_6_10_1z00(obj_t
		BgL_portz00_132)
	{
		{	/* Ieee/port.scm 1305 */
			return reset_eof(BgL_portz00_132);
		}

	}



/* &reset-eof */
	obj_t BGl_z62resetzd2eofzb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3667,
		obj_t BgL_portz00_3668)
	{
		{	/* Ieee/port.scm 1305 */
			{	/* Ieee/port.scm 1306 */
				bool_t BgL_tmpz00_6325;

				{	/* Ieee/port.scm 1306 */
					obj_t BgL_auxz00_6326;

					if (INPUT_PORTP(BgL_portz00_3668))
						{	/* Ieee/port.scm 1306 */
							BgL_auxz00_6326 = BgL_portz00_3668;
						}
					else
						{
							obj_t BgL_auxz00_6329;

							BgL_auxz00_6329 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(58496L),
								BGl_string2513z00zz__r4_ports_6_10_1z00,
								BGl_string2417z00zz__r4_ports_6_10_1z00, BgL_portz00_3668);
							FAILURE(BgL_auxz00_6329, BFALSE, BFALSE);
						}
					BgL_tmpz00_6325 =
						BGl_resetzd2eofzd2zz__r4_ports_6_10_1z00(BgL_auxz00_6326);
				}
				return BBOOL(BgL_tmpz00_6325);
			}
		}

	}



/* set-input-port-position! */
	BGL_EXPORTED_DEF obj_t
		BGl_setzd2inputzd2portzd2positionz12zc0zz__r4_ports_6_10_1z00(obj_t
		BgL_portz00_133, long BgL_posz00_134)
	{
		{	/* Ieee/port.scm 1311 */
			{	/* Ieee/port.scm 1312 */
				obj_t BgL_useekz00_2904;

				BgL_useekz00_2904 = BGL_INPUT_PORT_USEEK(BgL_portz00_133);
				if (PROCEDUREP(BgL_useekz00_2904))
					{	/* Ieee/port.scm 1313 */
						BGL_PROCEDURE_CALL2(BgL_useekz00_2904, BgL_portz00_133,
							BINT(BgL_posz00_134));
					}
				else
					{	/* Ieee/port.scm 1313 */
						bgl_input_port_seek(BgL_portz00_133, BgL_posz00_134);
						BUNSPEC;
					}
			}
			return BUNSPEC;
		}

	}



/* &set-input-port-position! */
	obj_t BGl_z62setzd2inputzd2portzd2positionz12za2zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3669, obj_t BgL_portz00_3670, obj_t BgL_posz00_3671)
	{
		{	/* Ieee/port.scm 1311 */
			{	/* Ieee/port.scm 1315 */
				long BgL_auxz00_6352;
				obj_t BgL_auxz00_6345;

				{	/* Ieee/port.scm 1315 */
					obj_t BgL_tmpz00_6353;

					if (INTEGERP(BgL_posz00_3671))
						{	/* Ieee/port.scm 1315 */
							BgL_tmpz00_6353 = BgL_posz00_3671;
						}
					else
						{
							obj_t BgL_auxz00_6356;

							BgL_auxz00_6356 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(58933L),
								BGl_string2514z00zz__r4_ports_6_10_1z00,
								BGl_string2446z00zz__r4_ports_6_10_1z00, BgL_posz00_3671);
							FAILURE(BgL_auxz00_6356, BFALSE, BFALSE);
						}
					BgL_auxz00_6352 = (long) CINT(BgL_tmpz00_6353);
				}
				if (INPUT_PORTP(BgL_portz00_3670))
					{	/* Ieee/port.scm 1315 */
						BgL_auxz00_6345 = BgL_portz00_3670;
					}
				else
					{
						obj_t BgL_auxz00_6348;

						BgL_auxz00_6348 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(58933L),
							BGl_string2514z00zz__r4_ports_6_10_1z00,
							BGl_string2417z00zz__r4_ports_6_10_1z00, BgL_portz00_3670);
						FAILURE(BgL_auxz00_6348, BFALSE, BFALSE);
					}
				return
					BGl_setzd2inputzd2portzd2positionz12zc0zz__r4_ports_6_10_1z00
					(BgL_auxz00_6345, BgL_auxz00_6352);
			}
		}

	}



/* input-port-position */
	BGL_EXPORTED_DEF long
		BGl_inputzd2portzd2positionz00zz__r4_ports_6_10_1z00(obj_t BgL_portz00_135)
	{
		{	/* Ieee/port.scm 1321 */
			return INPUT_PORT_FILEPOS(BgL_portz00_135);
		}

	}



/* &input-port-position */
	obj_t BGl_z62inputzd2portzd2positionz62zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3672, obj_t BgL_portz00_3673)
	{
		{	/* Ieee/port.scm 1321 */
			{	/* Ieee/port.scm 1322 */
				long BgL_tmpz00_6363;

				{	/* Ieee/port.scm 1322 */
					obj_t BgL_auxz00_6364;

					if (INPUT_PORTP(BgL_portz00_3673))
						{	/* Ieee/port.scm 1322 */
							BgL_auxz00_6364 = BgL_portz00_3673;
						}
					else
						{
							obj_t BgL_auxz00_6367;

							BgL_auxz00_6367 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(59223L),
								BGl_string2515z00zz__r4_ports_6_10_1z00,
								BGl_string2417z00zz__r4_ports_6_10_1z00, BgL_portz00_3673);
							FAILURE(BgL_auxz00_6367, BFALSE, BFALSE);
						}
					BgL_tmpz00_6363 =
						BGl_inputzd2portzd2positionz00zz__r4_ports_6_10_1z00
						(BgL_auxz00_6364);
				}
				return BINT(BgL_tmpz00_6363);
			}
		}

	}



/* input-port-fill-barrier */
	BGL_EXPORTED_DEF obj_t
		BGl_inputzd2portzd2fillzd2barrierzd2zz__r4_ports_6_10_1z00(obj_t
		BgL_portz00_136)
	{
		{	/* Ieee/port.scm 1327 */
			return BINT(INPUT_PORT_FILLBARRIER(BgL_portz00_136));
		}

	}



/* &input-port-fill-barrier */
	obj_t BGl_z62inputzd2portzd2fillzd2barrierzb0zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3674, obj_t BgL_portz00_3675)
	{
		{	/* Ieee/port.scm 1327 */
			{	/* Ieee/port.scm 1328 */
				obj_t BgL_auxz00_6375;

				if (INPUT_PORTP(BgL_portz00_3675))
					{	/* Ieee/port.scm 1328 */
						BgL_auxz00_6375 = BgL_portz00_3675;
					}
				else
					{
						obj_t BgL_auxz00_6378;

						BgL_auxz00_6378 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(59537L),
							BGl_string2516z00zz__r4_ports_6_10_1z00,
							BGl_string2417z00zz__r4_ports_6_10_1z00, BgL_portz00_3675);
						FAILURE(BgL_auxz00_6378, BFALSE, BFALSE);
					}
				return
					BGl_inputzd2portzd2fillzd2barrierzd2zz__r4_ports_6_10_1z00
					(BgL_auxz00_6375);
			}
		}

	}



/* input-port-fill-barrier-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_inputzd2portzd2fillzd2barrierzd2setz12z12zz__r4_ports_6_10_1z00(obj_t
		BgL_portz00_137, long BgL_posz00_138)
	{
		{	/* Ieee/port.scm 1333 */
			INPUT_PORT_FILLBARRIER_SET(BgL_portz00_137, BgL_posz00_138);
			BUNSPEC;
			return BINT(BgL_posz00_138);
		}

	}



/* &input-port-fill-barrier-set! */
	obj_t
		BGl_z62inputzd2portzd2fillzd2barrierzd2setz12z70zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3676, obj_t BgL_portz00_3677, obj_t BgL_posz00_3678)
	{
		{	/* Ieee/port.scm 1333 */
			{	/* Ieee/port.scm 1334 */
				long BgL_auxz00_6392;
				obj_t BgL_auxz00_6385;

				{	/* Ieee/port.scm 1334 */
					obj_t BgL_tmpz00_6393;

					if (INTEGERP(BgL_posz00_3678))
						{	/* Ieee/port.scm 1334 */
							BgL_tmpz00_6393 = BgL_posz00_3678;
						}
					else
						{
							obj_t BgL_auxz00_6396;

							BgL_auxz00_6396 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(59911L),
								BGl_string2517z00zz__r4_ports_6_10_1z00,
								BGl_string2446z00zz__r4_ports_6_10_1z00, BgL_posz00_3678);
							FAILURE(BgL_auxz00_6396, BFALSE, BFALSE);
						}
					BgL_auxz00_6392 = (long) CINT(BgL_tmpz00_6393);
				}
				if (INPUT_PORTP(BgL_portz00_3677))
					{	/* Ieee/port.scm 1334 */
						BgL_auxz00_6385 = BgL_portz00_3677;
					}
				else
					{
						obj_t BgL_auxz00_6388;

						BgL_auxz00_6388 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(59911L),
							BGl_string2517z00zz__r4_ports_6_10_1z00,
							BGl_string2417z00zz__r4_ports_6_10_1z00, BgL_portz00_3677);
						FAILURE(BgL_auxz00_6388, BFALSE, BFALSE);
					}
				return
					BGl_inputzd2portzd2fillzd2barrierzd2setz12z12zz__r4_ports_6_10_1z00
					(BgL_auxz00_6385, BgL_auxz00_6392);
			}
		}

	}



/* input-port-last-token-position */
	BGL_EXPORTED_DEF long
		BGl_inputzd2portzd2lastzd2tokenzd2positionz00zz__r4_ports_6_10_1z00(obj_t
		BgL_portz00_139)
	{
		{	/* Ieee/port.scm 1340 */
			{	/* Ieee/port.scm 1341 */
				long BgL_tmpz00_6402;

				BgL_tmpz00_6402 = INPUT_PORT_TOKENPOS(BgL_portz00_139);
				return (long) (BgL_tmpz00_6402);
		}}

	}



/* &input-port-last-token-position */
	obj_t
		BGl_z62inputzd2portzd2lastzd2tokenzd2positionz62zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3679, obj_t BgL_portz00_3680)
	{
		{	/* Ieee/port.scm 1340 */
			{	/* Ieee/port.scm 1341 */
				long BgL_tmpz00_6405;

				{	/* Ieee/port.scm 1341 */
					obj_t BgL_auxz00_6406;

					if (INPUT_PORTP(BgL_portz00_3680))
						{	/* Ieee/port.scm 1341 */
							BgL_auxz00_6406 = BgL_portz00_3680;
						}
					else
						{
							obj_t BgL_auxz00_6409;

							BgL_auxz00_6409 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(60203L),
								BGl_string2518z00zz__r4_ports_6_10_1z00,
								BGl_string2417z00zz__r4_ports_6_10_1z00, BgL_portz00_3680);
							FAILURE(BgL_auxz00_6409, BFALSE, BFALSE);
						}
					BgL_tmpz00_6405 =
						BGl_inputzd2portzd2lastzd2tokenzd2positionz00zz__r4_ports_6_10_1z00
						(BgL_auxz00_6406);
				}
				return BINT(BgL_tmpz00_6405);
			}
		}

	}



/* output-port-name */
	BGL_EXPORTED_DEF obj_t BGl_outputzd2portzd2namez00zz__r4_ports_6_10_1z00(obj_t
		BgL_portz00_140)
	{
		{	/* Ieee/port.scm 1346 */
			return OUTPUT_PORT_NAME(BgL_portz00_140);
		}

	}



/* &output-port-name */
	obj_t BGl_z62outputzd2portzd2namez62zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3681, obj_t BgL_portz00_3682)
	{
		{	/* Ieee/port.scm 1346 */
			{	/* Ieee/port.scm 1347 */
				obj_t BgL_auxz00_6416;

				if (OUTPUT_PORTP(BgL_portz00_3682))
					{	/* Ieee/port.scm 1347 */
						BgL_auxz00_6416 = BgL_portz00_3682;
					}
				else
					{
						obj_t BgL_auxz00_6419;

						BgL_auxz00_6419 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(60509L),
							BGl_string2519z00zz__r4_ports_6_10_1z00,
							BGl_string2429z00zz__r4_ports_6_10_1z00, BgL_portz00_3682);
						FAILURE(BgL_auxz00_6419, BFALSE, BFALSE);
					}
				return
					BGl_outputzd2portzd2namez00zz__r4_ports_6_10_1z00(BgL_auxz00_6416);
			}
		}

	}



/* output-port-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_outputzd2portzd2namezd2setz12zc0zz__r4_ports_6_10_1z00(obj_t
		BgL_portz00_141, obj_t BgL_namez00_142)
	{
		{	/* Ieee/port.scm 1352 */
			OUTPUT_PORT_NAME_SET(BgL_portz00_141, BgL_namez00_142);
			return BUNSPEC;
		}

	}



/* &output-port-name-set! */
	obj_t BGl_z62outputzd2portzd2namezd2setz12za2zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3683, obj_t BgL_portz00_3684, obj_t BgL_namez00_3685)
	{
		{	/* Ieee/port.scm 1352 */
			{	/* Ieee/port.scm 1352 */
				obj_t BgL_auxz00_6432;
				obj_t BgL_auxz00_6425;

				if (STRINGP(BgL_namez00_3685))
					{	/* Ieee/port.scm 1352 */
						BgL_auxz00_6432 = BgL_namez00_3685;
					}
				else
					{
						obj_t BgL_auxz00_6435;

						BgL_auxz00_6435 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(60758L),
							BGl_string2520z00zz__r4_ports_6_10_1z00,
							BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_namez00_3685);
						FAILURE(BgL_auxz00_6435, BFALSE, BFALSE);
					}
				if (OUTPUT_PORTP(BgL_portz00_3684))
					{	/* Ieee/port.scm 1352 */
						BgL_auxz00_6425 = BgL_portz00_3684;
					}
				else
					{
						obj_t BgL_auxz00_6428;

						BgL_auxz00_6428 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(60758L),
							BGl_string2520z00zz__r4_ports_6_10_1z00,
							BGl_string2429z00zz__r4_ports_6_10_1z00, BgL_portz00_3684);
						FAILURE(BgL_auxz00_6428, BFALSE, BFALSE);
					}
				return
					BGl_outputzd2portzd2namezd2setz12zc0zz__r4_ports_6_10_1z00
					(BgL_auxz00_6425, BgL_auxz00_6432);
			}
		}

	}



/* set-output-port-position! */
	BGL_EXPORTED_DEF obj_t
		BGl_setzd2outputzd2portzd2positionz12zc0zz__r4_ports_6_10_1z00(obj_t
		BgL_portz00_143, long BgL_posz00_144)
	{
		{	/* Ieee/port.scm 1358 */
			if (CBOOL(bgl_output_port_seek(BgL_portz00_143, BgL_posz00_144)))
				{	/* Ieee/port.scm 1359 */
					return BFALSE;
				}
			else
				{	/* Ieee/port.scm 1359 */
					return
						bgl_system_failure(BGL_IO_PORT_ERROR,
						BGl_string2521z00zz__r4_ports_6_10_1z00,
						BGl_string2522z00zz__r4_ports_6_10_1z00, BgL_portz00_143);
				}
		}

	}



/* &set-output-port-position! */
	obj_t BGl_z62setzd2outputzd2portzd2positionz12za2zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3686, obj_t BgL_portz00_3687, obj_t BgL_posz00_3688)
	{
		{	/* Ieee/port.scm 1358 */
			{	/* Ieee/port.scm 1359 */
				long BgL_auxz00_6451;
				obj_t BgL_auxz00_6444;

				{	/* Ieee/port.scm 1359 */
					obj_t BgL_tmpz00_6452;

					if (INTEGERP(BgL_posz00_3688))
						{	/* Ieee/port.scm 1359 */
							BgL_tmpz00_6452 = BgL_posz00_3688;
						}
					else
						{
							obj_t BgL_auxz00_6455;

							BgL_auxz00_6455 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(61143L),
								BGl_string2523z00zz__r4_ports_6_10_1z00,
								BGl_string2446z00zz__r4_ports_6_10_1z00, BgL_posz00_3688);
							FAILURE(BgL_auxz00_6455, BFALSE, BFALSE);
						}
					BgL_auxz00_6451 = (long) CINT(BgL_tmpz00_6452);
				}
				if (OUTPUT_PORTP(BgL_portz00_3687))
					{	/* Ieee/port.scm 1359 */
						BgL_auxz00_6444 = BgL_portz00_3687;
					}
				else
					{
						obj_t BgL_auxz00_6447;

						BgL_auxz00_6447 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(61143L),
							BGl_string2523z00zz__r4_ports_6_10_1z00,
							BGl_string2429z00zz__r4_ports_6_10_1z00, BgL_portz00_3687);
						FAILURE(BgL_auxz00_6447, BFALSE, BFALSE);
					}
				return
					BGl_setzd2outputzd2portzd2positionz12zc0zz__r4_ports_6_10_1z00
					(BgL_auxz00_6444, BgL_auxz00_6451);
			}
		}

	}



/* output-port-position */
	BGL_EXPORTED_DEF long
		BGl_outputzd2portzd2positionz00zz__r4_ports_6_10_1z00(obj_t BgL_portz00_145)
	{
		{	/* Ieee/port.scm 1366 */
			return BGL_OUTPUT_PORT_FILEPOS(BgL_portz00_145);
		}

	}



/* &output-port-position */
	obj_t BGl_z62outputzd2portzd2positionz62zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3689, obj_t BgL_portz00_3690)
	{
		{	/* Ieee/port.scm 1366 */
			{	/* Ieee/port.scm 1367 */
				long BgL_tmpz00_6462;

				{	/* Ieee/port.scm 1367 */
					obj_t BgL_auxz00_6463;

					if (OUTPUT_PORTP(BgL_portz00_3690))
						{	/* Ieee/port.scm 1367 */
							BgL_auxz00_6463 = BgL_portz00_3690;
						}
					else
						{
							obj_t BgL_auxz00_6466;

							BgL_auxz00_6466 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(61559L),
								BGl_string2524z00zz__r4_ports_6_10_1z00,
								BGl_string2429z00zz__r4_ports_6_10_1z00, BgL_portz00_3690);
							FAILURE(BgL_auxz00_6466, BFALSE, BFALSE);
						}
					BgL_tmpz00_6462 =
						BGl_outputzd2portzd2positionz00zz__r4_ports_6_10_1z00
						(BgL_auxz00_6463);
				}
				return BINT(BgL_tmpz00_6462);
			}
		}

	}



/* output-port-isatty? */
	BGL_EXPORTED_DEF bool_t
		BGl_outputzd2portzd2isattyzf3zf3zz__r4_ports_6_10_1z00(obj_t
		BgL_portz00_146)
	{
		{	/* Ieee/port.scm 1372 */
			return bgl_port_isatty(BgL_portz00_146);
		}

	}



/* &output-port-isatty? */
	obj_t BGl_z62outputzd2portzd2isattyzf3z91zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3691, obj_t BgL_portz00_3692)
	{
		{	/* Ieee/port.scm 1372 */
			{	/* Ieee/port.scm 1374 */
				bool_t BgL_tmpz00_6473;

				{	/* Ieee/port.scm 1374 */
					obj_t BgL_auxz00_6474;

					if (OUTPUT_PORTP(BgL_portz00_3692))
						{	/* Ieee/port.scm 1374 */
							BgL_auxz00_6474 = BgL_portz00_3692;
						}
					else
						{
							obj_t BgL_auxz00_6477;

							BgL_auxz00_6477 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(61887L),
								BGl_string2525z00zz__r4_ports_6_10_1z00,
								BGl_string2429z00zz__r4_ports_6_10_1z00, BgL_portz00_3692);
							FAILURE(BgL_auxz00_6477, BFALSE, BFALSE);
						}
					BgL_tmpz00_6473 =
						BGl_outputzd2portzd2isattyzf3zf3zz__r4_ports_6_10_1z00
						(BgL_auxz00_6474);
				}
				return BBOOL(BgL_tmpz00_6473);
			}
		}

	}



/* input-port-name */
	BGL_EXPORTED_DEF obj_t BGl_inputzd2portzd2namez00zz__r4_ports_6_10_1z00(obj_t
		BgL_portz00_147)
	{
		{	/* Ieee/port.scm 1380 */
			return INPUT_PORT_NAME(BgL_portz00_147);
		}

	}



/* &input-port-name */
	obj_t BGl_z62inputzd2portzd2namez62zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3693, obj_t BgL_portz00_3694)
	{
		{	/* Ieee/port.scm 1380 */
			{	/* Ieee/port.scm 1381 */
				obj_t BgL_auxz00_6484;

				if (INPUT_PORTP(BgL_portz00_3694))
					{	/* Ieee/port.scm 1381 */
						BgL_auxz00_6484 = BgL_portz00_3694;
					}
				else
					{
						obj_t BgL_auxz00_6487;

						BgL_auxz00_6487 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(62194L),
							BGl_string2526z00zz__r4_ports_6_10_1z00,
							BGl_string2417z00zz__r4_ports_6_10_1z00, BgL_portz00_3694);
						FAILURE(BgL_auxz00_6487, BFALSE, BFALSE);
					}
				return
					BGl_inputzd2portzd2namez00zz__r4_ports_6_10_1z00(BgL_auxz00_6484);
			}
		}

	}



/* input-port-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_inputzd2portzd2namezd2setz12zc0zz__r4_ports_6_10_1z00(obj_t
		BgL_portz00_148, obj_t BgL_namez00_149)
	{
		{	/* Ieee/port.scm 1386 */
			INPUT_PORT_NAME_SET(BgL_portz00_148, BgL_namez00_149);
			return BUNSPEC;
		}

	}



/* &input-port-name-set! */
	obj_t BGl_z62inputzd2portzd2namezd2setz12za2zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3695, obj_t BgL_portz00_3696, obj_t BgL_namez00_3697)
	{
		{	/* Ieee/port.scm 1386 */
			{	/* Ieee/port.scm 1386 */
				obj_t BgL_auxz00_6500;
				obj_t BgL_auxz00_6493;

				if (STRINGP(BgL_namez00_3697))
					{	/* Ieee/port.scm 1386 */
						BgL_auxz00_6500 = BgL_namez00_3697;
					}
				else
					{
						obj_t BgL_auxz00_6503;

						BgL_auxz00_6503 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(62442L),
							BGl_string2527z00zz__r4_ports_6_10_1z00,
							BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_namez00_3697);
						FAILURE(BgL_auxz00_6503, BFALSE, BFALSE);
					}
				if (INPUT_PORTP(BgL_portz00_3696))
					{	/* Ieee/port.scm 1386 */
						BgL_auxz00_6493 = BgL_portz00_3696;
					}
				else
					{
						obj_t BgL_auxz00_6496;

						BgL_auxz00_6496 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(62442L),
							BGl_string2527z00zz__r4_ports_6_10_1z00,
							BGl_string2417z00zz__r4_ports_6_10_1z00, BgL_portz00_3696);
						FAILURE(BgL_auxz00_6496, BFALSE, BFALSE);
					}
				return
					BGl_inputzd2portzd2namezd2setz12zc0zz__r4_ports_6_10_1z00
					(BgL_auxz00_6493, BgL_auxz00_6500);
			}
		}

	}



/* input-port-length */
	BGL_EXPORTED_DEF long BGl_inputzd2portzd2lengthz00zz__r4_ports_6_10_1z00(obj_t
		BgL_portz00_150)
	{
		{	/* Ieee/port.scm 1392 */
			return BGL_INPUT_PORT_LENGTH(BgL_portz00_150);
		}

	}



/* &input-port-length */
	obj_t BGl_z62inputzd2portzd2lengthz62zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3698, obj_t BgL_portz00_3699)
	{
		{	/* Ieee/port.scm 1392 */
			{	/* Ieee/port.scm 1393 */
				long BgL_tmpz00_6509;

				{	/* Ieee/port.scm 1393 */
					obj_t BgL_auxz00_6510;

					if (INPUT_PORTP(BgL_portz00_3699))
						{	/* Ieee/port.scm 1393 */
							BgL_auxz00_6510 = BgL_portz00_3699;
						}
					else
						{
							obj_t BgL_auxz00_6513;

							BgL_auxz00_6513 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(62794L),
								BGl_string2528z00zz__r4_ports_6_10_1z00,
								BGl_string2417z00zz__r4_ports_6_10_1z00, BgL_portz00_3699);
							FAILURE(BgL_auxz00_6513, BFALSE, BFALSE);
						}
					BgL_tmpz00_6509 =
						BGl_inputzd2portzd2lengthz00zz__r4_ports_6_10_1z00(BgL_auxz00_6510);
				}
				return make_belong(BgL_tmpz00_6509);
			}
		}

	}



/* output-port-close-hook */
	BGL_EXPORTED_DEF obj_t
		BGl_outputzd2portzd2closezd2hookzd2zz__r4_ports_6_10_1z00(obj_t
		BgL_portz00_151)
	{
		{	/* Ieee/port.scm 1398 */
			return PORT_CHOOK(BgL_portz00_151);
		}

	}



/* &output-port-close-hook */
	obj_t BGl_z62outputzd2portzd2closezd2hookzb0zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3700, obj_t BgL_portz00_3701)
	{
		{	/* Ieee/port.scm 1398 */
			{	/* Ieee/port.scm 1399 */
				obj_t BgL_auxz00_6520;

				if (OUTPUT_PORTP(BgL_portz00_3701))
					{	/* Ieee/port.scm 1399 */
						BgL_auxz00_6520 = BgL_portz00_3701;
					}
				else
					{
						obj_t BgL_auxz00_6523;

						BgL_auxz00_6523 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(63092L),
							BGl_string2529z00zz__r4_ports_6_10_1z00,
							BGl_string2429z00zz__r4_ports_6_10_1z00, BgL_portz00_3701);
						FAILURE(BgL_auxz00_6523, BFALSE, BFALSE);
					}
				return
					BGl_outputzd2portzd2closezd2hookzd2zz__r4_ports_6_10_1z00
					(BgL_auxz00_6520);
			}
		}

	}



/* closed-output-port? */
	BGL_EXPORTED_DEF bool_t
		BGl_closedzd2outputzd2portzf3zf3zz__r4_ports_6_10_1z00(obj_t
		BgL_portz00_152)
	{
		{	/* Ieee/port.scm 1404 */
			return OUTPUT_PORT_CLOSEP(BgL_portz00_152);
		}

	}



/* &closed-output-port? */
	obj_t BGl_z62closedzd2outputzd2portzf3z91zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3702, obj_t BgL_portz00_3703)
	{
		{	/* Ieee/port.scm 1404 */
			{	/* Ieee/port.scm 1405 */
				bool_t BgL_tmpz00_6529;

				{	/* Ieee/port.scm 1405 */
					obj_t BgL_auxz00_6530;

					if (OUTPUT_PORTP(BgL_portz00_3703))
						{	/* Ieee/port.scm 1405 */
							BgL_auxz00_6530 = BgL_portz00_3703;
						}
					else
						{
							obj_t BgL_auxz00_6533;

							BgL_auxz00_6533 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(63388L),
								BGl_string2530z00zz__r4_ports_6_10_1z00,
								BGl_string2429z00zz__r4_ports_6_10_1z00, BgL_portz00_3703);
							FAILURE(BgL_auxz00_6533, BFALSE, BFALSE);
						}
					BgL_tmpz00_6529 =
						BGl_closedzd2outputzd2portzf3zf3zz__r4_ports_6_10_1z00
						(BgL_auxz00_6530);
				}
				return BBOOL(BgL_tmpz00_6529);
			}
		}

	}



/* output-port-close-hook-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_outputzd2portzd2closezd2hookzd2setz12z12zz__r4_ports_6_10_1z00(obj_t
		BgL_portz00_153, obj_t BgL_procz00_154)
	{
		{	/* Ieee/port.scm 1410 */
			if (PROCEDURE_CORRECT_ARITYP(BgL_procz00_154, (int) (1L)))
				{	/* Ieee/port.scm 1411 */
					PORT_CHOOK_SET(BgL_portz00_153, BgL_procz00_154);
					BUNSPEC;
					return BgL_procz00_154;
				}
			else
				{	/* Ieee/port.scm 1411 */
					return
						bgl_system_failure(BGL_IO_PORT_ERROR,
						BGl_string2531z00zz__r4_ports_6_10_1z00,
						BGl_string2466z00zz__r4_ports_6_10_1z00, BgL_procz00_154);
				}
		}

	}



/* &output-port-close-hook-set! */
	obj_t
		BGl_z62outputzd2portzd2closezd2hookzd2setz12z70zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3704, obj_t BgL_portz00_3705, obj_t BgL_procz00_3706)
	{
		{	/* Ieee/port.scm 1410 */
			{	/* Ieee/port.scm 1411 */
				obj_t BgL_auxz00_6551;
				obj_t BgL_auxz00_6544;

				if (PROCEDUREP(BgL_procz00_3706))
					{	/* Ieee/port.scm 1411 */
						BgL_auxz00_6551 = BgL_procz00_3706;
					}
				else
					{
						obj_t BgL_auxz00_6554;

						BgL_auxz00_6554 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(63691L),
							BGl_string2532z00zz__r4_ports_6_10_1z00,
							BGl_string2405z00zz__r4_ports_6_10_1z00, BgL_procz00_3706);
						FAILURE(BgL_auxz00_6554, BFALSE, BFALSE);
					}
				if (OUTPUT_PORTP(BgL_portz00_3705))
					{	/* Ieee/port.scm 1411 */
						BgL_auxz00_6544 = BgL_portz00_3705;
					}
				else
					{
						obj_t BgL_auxz00_6547;

						BgL_auxz00_6547 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(63691L),
							BGl_string2532z00zz__r4_ports_6_10_1z00,
							BGl_string2429z00zz__r4_ports_6_10_1z00, BgL_portz00_3705);
						FAILURE(BgL_auxz00_6547, BFALSE, BFALSE);
					}
				return
					BGl_outputzd2portzd2closezd2hookzd2setz12z12zz__r4_ports_6_10_1z00
					(BgL_auxz00_6544, BgL_auxz00_6551);
			}
		}

	}



/* output-port-flush-hook */
	BGL_EXPORTED_DEF obj_t
		BGl_outputzd2portzd2flushzd2hookzd2zz__r4_ports_6_10_1z00(obj_t
		BgL_portz00_155)
	{
		{	/* Ieee/port.scm 1421 */
			return BGL_OUTPUT_PORT_FHOOK(BgL_portz00_155);
		}

	}



/* &output-port-flush-hook */
	obj_t BGl_z62outputzd2portzd2flushzd2hookzb0zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3707, obj_t BgL_portz00_3708)
	{
		{	/* Ieee/port.scm 1421 */
			{	/* Ieee/port.scm 1422 */
				obj_t BgL_auxz00_6560;

				if (OUTPUT_PORTP(BgL_portz00_3708))
					{	/* Ieee/port.scm 1422 */
						BgL_auxz00_6560 = BgL_portz00_3708;
					}
				else
					{
						obj_t BgL_auxz00_6563;

						BgL_auxz00_6563 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(64180L),
							BGl_string2533z00zz__r4_ports_6_10_1z00,
							BGl_string2429z00zz__r4_ports_6_10_1z00, BgL_portz00_3708);
						FAILURE(BgL_auxz00_6563, BFALSE, BFALSE);
					}
				return
					BGl_outputzd2portzd2flushzd2hookzd2zz__r4_ports_6_10_1z00
					(BgL_auxz00_6560);
			}
		}

	}



/* output-port-flush-hook-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_outputzd2portzd2flushzd2hookzd2setz12z12zz__r4_ports_6_10_1z00(obj_t
		BgL_portz00_156, obj_t BgL_procz00_157)
	{
		{	/* Ieee/port.scm 1427 */
			{	/* Ieee/port.scm 1428 */
				bool_t BgL_test3151z00_6568;

				if (PROCEDUREP(BgL_procz00_157))
					{	/* Ieee/port.scm 1428 */
						if (PROCEDURE_CORRECT_ARITYP(BgL_procz00_157, (int) (2L)))
							{	/* Ieee/port.scm 1428 */
								BgL_test3151z00_6568 = ((bool_t) 0);
							}
						else
							{	/* Ieee/port.scm 1428 */
								BgL_test3151z00_6568 = ((bool_t) 1);
							}
					}
				else
					{	/* Ieee/port.scm 1428 */
						BgL_test3151z00_6568 = ((bool_t) 0);
					}
				if (BgL_test3151z00_6568)
					{	/* Ieee/port.scm 1428 */
						return
							bgl_system_failure(BGL_IO_PORT_ERROR,
							BGl_string2534z00zz__r4_ports_6_10_1z00,
							BGl_string2466z00zz__r4_ports_6_10_1z00, BgL_procz00_157);
					}
				else
					{	/* Ieee/port.scm 1428 */
						BGL_OUTPUT_PORT_FHOOK_SET(BgL_portz00_156, BgL_procz00_157);
						BUNSPEC;
						return BgL_procz00_157;
					}
			}
		}

	}



/* &output-port-flush-hook-set! */
	obj_t
		BGl_z62outputzd2portzd2flushzd2hookzd2setz12z70zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3709, obj_t BgL_portz00_3710, obj_t BgL_procz00_3711)
	{
		{	/* Ieee/port.scm 1427 */
			{	/* Ieee/port.scm 1428 */
				obj_t BgL_auxz00_6576;

				if (OUTPUT_PORTP(BgL_portz00_3710))
					{	/* Ieee/port.scm 1428 */
						BgL_auxz00_6576 = BgL_portz00_3710;
					}
				else
					{
						obj_t BgL_auxz00_6579;

						BgL_auxz00_6579 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(64481L),
							BGl_string2535z00zz__r4_ports_6_10_1z00,
							BGl_string2429z00zz__r4_ports_6_10_1z00, BgL_portz00_3710);
						FAILURE(BgL_auxz00_6579, BFALSE, BFALSE);
					}
				return
					BGl_outputzd2portzd2flushzd2hookzd2setz12z12zz__r4_ports_6_10_1z00
					(BgL_auxz00_6576, BgL_procz00_3711);
			}
		}

	}



/* output-port-flush-buffer */
	BGL_EXPORTED_DEF obj_t
		BGl_outputzd2portzd2flushzd2bufferzd2zz__r4_ports_6_10_1z00(obj_t
		BgL_portz00_158)
	{
		{	/* Ieee/port.scm 1438 */
			return BGL_OUTPUT_PORT_FLUSHBUF(BgL_portz00_158);
		}

	}



/* &output-port-flush-buffer */
	obj_t BGl_z62outputzd2portzd2flushzd2bufferzb0zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3712, obj_t BgL_portz00_3713)
	{
		{	/* Ieee/port.scm 1438 */
			{	/* Ieee/port.scm 1439 */
				obj_t BgL_auxz00_6585;

				if (OUTPUT_PORTP(BgL_portz00_3713))
					{	/* Ieee/port.scm 1439 */
						BgL_auxz00_6585 = BgL_portz00_3713;
					}
				else
					{
						obj_t BgL_auxz00_6588;

						BgL_auxz00_6588 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(64971L),
							BGl_string2536z00zz__r4_ports_6_10_1z00,
							BGl_string2429z00zz__r4_ports_6_10_1z00, BgL_portz00_3713);
						FAILURE(BgL_auxz00_6588, BFALSE, BFALSE);
					}
				return
					BGl_outputzd2portzd2flushzd2bufferzd2zz__r4_ports_6_10_1z00
					(BgL_auxz00_6585);
			}
		}

	}



/* output-port-flush-buffer-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_outputzd2portzd2flushzd2bufferzd2setz12z12zz__r4_ports_6_10_1z00(obj_t
		BgL_portz00_159, obj_t BgL_bufz00_160)
	{
		{	/* Ieee/port.scm 1444 */
			BGL_OUTPUT_PORT_FLUSHBUF_SET(BgL_portz00_159, BgL_bufz00_160);
			BUNSPEC;
			return BgL_bufz00_160;
		}

	}



/* &output-port-flush-buffer-set! */
	obj_t
		BGl_z62outputzd2portzd2flushzd2bufferzd2setz12z70zz__r4_ports_6_10_1z00
		(obj_t BgL_envz00_3714, obj_t BgL_portz00_3715, obj_t BgL_bufz00_3716)
	{
		{	/* Ieee/port.scm 1444 */
			{	/* Ieee/port.scm 1445 */
				obj_t BgL_auxz00_6594;

				if (OUTPUT_PORTP(BgL_portz00_3715))
					{	/* Ieee/port.scm 1445 */
						BgL_auxz00_6594 = BgL_portz00_3715;
					}
				else
					{
						obj_t BgL_auxz00_6597;

						BgL_auxz00_6597 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(65319L),
							BGl_string2537z00zz__r4_ports_6_10_1z00,
							BGl_string2429z00zz__r4_ports_6_10_1z00, BgL_portz00_3715);
						FAILURE(BgL_auxz00_6597, BFALSE, BFALSE);
					}
				return
					BGl_outputzd2portzd2flushzd2bufferzd2setz12z12zz__r4_ports_6_10_1z00
					(BgL_auxz00_6594, BgL_bufz00_3716);
			}
		}

	}



/* input-port-close-hook */
	BGL_EXPORTED_DEF obj_t
		BGl_inputzd2portzd2closezd2hookzd2zz__r4_ports_6_10_1z00(obj_t
		BgL_portz00_161)
	{
		{	/* Ieee/port.scm 1451 */
			return PORT_CHOOK(BgL_portz00_161);
		}

	}



/* &input-port-close-hook */
	obj_t BGl_z62inputzd2portzd2closezd2hookzb0zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3717, obj_t BgL_portz00_3718)
	{
		{	/* Ieee/port.scm 1451 */
			{	/* Ieee/port.scm 1452 */
				obj_t BgL_auxz00_6603;

				if (INPUT_PORTP(BgL_portz00_3718))
					{	/* Ieee/port.scm 1452 */
						BgL_auxz00_6603 = BgL_portz00_3718;
					}
				else
					{
						obj_t BgL_auxz00_6606;

						BgL_auxz00_6606 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(65599L),
							BGl_string2538z00zz__r4_ports_6_10_1z00,
							BGl_string2417z00zz__r4_ports_6_10_1z00, BgL_portz00_3718);
						FAILURE(BgL_auxz00_6606, BFALSE, BFALSE);
					}
				return
					BGl_inputzd2portzd2closezd2hookzd2zz__r4_ports_6_10_1z00
					(BgL_auxz00_6603);
			}
		}

	}



/* input-port-close-hook-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_inputzd2portzd2closezd2hookzd2setz12z12zz__r4_ports_6_10_1z00(obj_t
		BgL_portz00_162, obj_t BgL_procz00_163)
	{
		{	/* Ieee/port.scm 1457 */
			if (PROCEDURE_CORRECT_ARITYP(BgL_procz00_163, (int) (1L)))
				{	/* Ieee/port.scm 1458 */
					PORT_CHOOK_SET(BgL_portz00_162, BgL_procz00_163);
					BUNSPEC;
					return BgL_procz00_163;
				}
			else
				{	/* Ieee/port.scm 1458 */
					return
						bgl_system_failure(BGL_IO_PORT_ERROR,
						BGl_string2465z00zz__r4_ports_6_10_1z00,
						BGl_string2466z00zz__r4_ports_6_10_1z00, BgL_procz00_163);
				}
		}

	}



/* &input-port-close-hook-set! */
	obj_t
		BGl_z62inputzd2portzd2closezd2hookzd2setz12z70zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3719, obj_t BgL_portz00_3720, obj_t BgL_procz00_3721)
	{
		{	/* Ieee/port.scm 1457 */
			{	/* Ieee/port.scm 1458 */
				obj_t BgL_auxz00_6623;
				obj_t BgL_auxz00_6616;

				if (PROCEDUREP(BgL_procz00_3721))
					{	/* Ieee/port.scm 1458 */
						BgL_auxz00_6623 = BgL_procz00_3721;
					}
				else
					{
						obj_t BgL_auxz00_6626;

						BgL_auxz00_6626 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(65898L),
							BGl_string2539z00zz__r4_ports_6_10_1z00,
							BGl_string2405z00zz__r4_ports_6_10_1z00, BgL_procz00_3721);
						FAILURE(BgL_auxz00_6626, BFALSE, BFALSE);
					}
				if (INPUT_PORTP(BgL_portz00_3720))
					{	/* Ieee/port.scm 1458 */
						BgL_auxz00_6616 = BgL_portz00_3720;
					}
				else
					{
						obj_t BgL_auxz00_6619;

						BgL_auxz00_6619 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(65898L),
							BGl_string2539z00zz__r4_ports_6_10_1z00,
							BGl_string2417z00zz__r4_ports_6_10_1z00, BgL_portz00_3720);
						FAILURE(BgL_auxz00_6619, BFALSE, BFALSE);
					}
				return
					BGl_inputzd2portzd2closezd2hookzd2setz12z12zz__r4_ports_6_10_1z00
					(BgL_auxz00_6616, BgL_auxz00_6623);
			}
		}

	}



/* input-port-seek */
	BGL_EXPORTED_DEF obj_t BGl_inputzd2portzd2seekz00zz__r4_ports_6_10_1z00(obj_t
		BgL_portz00_164)
	{
		{	/* Ieee/port.scm 1468 */
			return BGL_INPUT_PORT_USEEK(BgL_portz00_164);
		}

	}



/* &input-port-seek */
	obj_t BGl_z62inputzd2portzd2seekz62zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3722, obj_t BgL_portz00_3723)
	{
		{	/* Ieee/port.scm 1468 */
			{	/* Ieee/port.scm 1469 */
				obj_t BgL_auxz00_6632;

				if (INPUT_PORTP(BgL_portz00_3723))
					{	/* Ieee/port.scm 1469 */
						BgL_auxz00_6632 = BgL_portz00_3723;
					}
				else
					{
						obj_t BgL_auxz00_6635;

						BgL_auxz00_6635 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(66377L),
							BGl_string2540z00zz__r4_ports_6_10_1z00,
							BGl_string2417z00zz__r4_ports_6_10_1z00, BgL_portz00_3723);
						FAILURE(BgL_auxz00_6635, BFALSE, BFALSE);
					}
				return
					BGl_inputzd2portzd2seekz00zz__r4_ports_6_10_1z00(BgL_auxz00_6632);
			}
		}

	}



/* input-port-seek-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_inputzd2portzd2seekzd2setz12zc0zz__r4_ports_6_10_1z00(obj_t
		BgL_portz00_165, obj_t BgL_procz00_166)
	{
		{	/* Ieee/port.scm 1474 */
			if (PROCEDURE_CORRECT_ARITYP(BgL_procz00_166, (int) (2L)))
				{	/* Ieee/port.scm 1475 */
					BGL_INPUT_PORT_USEEK_SET(BgL_portz00_165, BgL_procz00_166);
					BUNSPEC;
					return BgL_procz00_166;
				}
			else
				{	/* Ieee/port.scm 1475 */
					return
						bgl_system_failure(BGL_IO_PORT_ERROR,
						BGl_string2467z00zz__r4_ports_6_10_1z00,
						BGl_string2468z00zz__r4_ports_6_10_1z00, BgL_procz00_166);
				}
		}

	}



/* &input-port-seek-set! */
	obj_t BGl_z62inputzd2portzd2seekzd2setz12za2zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3724, obj_t BgL_portz00_3725, obj_t BgL_procz00_3726)
	{
		{	/* Ieee/port.scm 1474 */
			{	/* Ieee/port.scm 1475 */
				obj_t BgL_auxz00_6652;
				obj_t BgL_auxz00_6645;

				if (PROCEDUREP(BgL_procz00_3726))
					{	/* Ieee/port.scm 1475 */
						BgL_auxz00_6652 = BgL_procz00_3726;
					}
				else
					{
						obj_t BgL_auxz00_6655;

						BgL_auxz00_6655 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(66670L),
							BGl_string2541z00zz__r4_ports_6_10_1z00,
							BGl_string2405z00zz__r4_ports_6_10_1z00, BgL_procz00_3726);
						FAILURE(BgL_auxz00_6655, BFALSE, BFALSE);
					}
				if (INPUT_PORTP(BgL_portz00_3725))
					{	/* Ieee/port.scm 1475 */
						BgL_auxz00_6645 = BgL_portz00_3725;
					}
				else
					{
						obj_t BgL_auxz00_6648;

						BgL_auxz00_6648 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(66670L),
							BGl_string2541z00zz__r4_ports_6_10_1z00,
							BGl_string2417z00zz__r4_ports_6_10_1z00, BgL_portz00_3725);
						FAILURE(BgL_auxz00_6648, BFALSE, BFALSE);
					}
				return
					BGl_inputzd2portzd2seekzd2setz12zc0zz__r4_ports_6_10_1z00
					(BgL_auxz00_6645, BgL_auxz00_6652);
			}
		}

	}



/* input-port-buffer */
	BGL_EXPORTED_DEF obj_t
		BGl_inputzd2portzd2bufferz00zz__r4_ports_6_10_1z00(obj_t BgL_portz00_167)
	{
		{	/* Ieee/port.scm 1485 */
			return BGL_INPUT_PORT_BUFFER(BgL_portz00_167);
		}

	}



/* &input-port-buffer */
	obj_t BGl_z62inputzd2portzd2bufferz62zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3727, obj_t BgL_portz00_3728)
	{
		{	/* Ieee/port.scm 1485 */
			{	/* Ieee/port.scm 1486 */
				obj_t BgL_auxz00_6661;

				if (INPUT_PORTP(BgL_portz00_3728))
					{	/* Ieee/port.scm 1486 */
						BgL_auxz00_6661 = BgL_portz00_3728;
					}
				else
					{
						obj_t BgL_auxz00_6664;

						BgL_auxz00_6664 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(67155L),
							BGl_string2542z00zz__r4_ports_6_10_1z00,
							BGl_string2417z00zz__r4_ports_6_10_1z00, BgL_portz00_3728);
						FAILURE(BgL_auxz00_6664, BFALSE, BFALSE);
					}
				return
					BGl_inputzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BgL_auxz00_6661);
			}
		}

	}



/* input-port-buffer-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_inputzd2portzd2bufferzd2setz12zc0zz__r4_ports_6_10_1z00(obj_t
		BgL_portz00_168, obj_t BgL_bufferz00_169)
	{
		{	/* Ieee/port.scm 1491 */
			bgl_input_port_buffer_set(BgL_portz00_168, BgL_bufferz00_169);
			BUNSPEC;
			return BgL_portz00_168;
		}

	}



/* &input-port-buffer-set! */
	obj_t BGl_z62inputzd2portzd2bufferzd2setz12za2zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3729, obj_t BgL_portz00_3730, obj_t BgL_bufferz00_3731)
	{
		{	/* Ieee/port.scm 1491 */
			{	/* Ieee/port.scm 1492 */
				obj_t BgL_auxz00_6677;
				obj_t BgL_auxz00_6670;

				if (STRINGP(BgL_bufferz00_3731))
					{	/* Ieee/port.scm 1492 */
						BgL_auxz00_6677 = BgL_bufferz00_3731;
					}
				else
					{
						obj_t BgL_auxz00_6680;

						BgL_auxz00_6680 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(67503L),
							BGl_string2543z00zz__r4_ports_6_10_1z00,
							BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_bufferz00_3731);
						FAILURE(BgL_auxz00_6680, BFALSE, BFALSE);
					}
				if (INPUT_PORTP(BgL_portz00_3730))
					{	/* Ieee/port.scm 1492 */
						BgL_auxz00_6670 = BgL_portz00_3730;
					}
				else
					{
						obj_t BgL_auxz00_6673;

						BgL_auxz00_6673 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(67503L),
							BGl_string2543z00zz__r4_ports_6_10_1z00,
							BGl_string2417z00zz__r4_ports_6_10_1z00, BgL_portz00_3730);
						FAILURE(BgL_auxz00_6673, BFALSE, BFALSE);
					}
				return
					BGl_inputzd2portzd2bufferzd2setz12zc0zz__r4_ports_6_10_1z00
					(BgL_auxz00_6670, BgL_auxz00_6677);
			}
		}

	}



/* output-port-buffer */
	BGL_EXPORTED_DEF obj_t
		BGl_outputzd2portzd2bufferz00zz__r4_ports_6_10_1z00(obj_t BgL_portz00_170)
	{
		{	/* Ieee/port.scm 1497 */
			return BGL_OUTPUT_PORT_BUFFER(BgL_portz00_170);
		}

	}



/* &output-port-buffer */
	obj_t BGl_z62outputzd2portzd2bufferz62zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3732, obj_t BgL_portz00_3733)
	{
		{	/* Ieee/port.scm 1497 */
			{	/* Ieee/port.scm 1498 */
				obj_t BgL_auxz00_6686;

				if (OUTPUT_PORTP(BgL_portz00_3733))
					{	/* Ieee/port.scm 1498 */
						BgL_auxz00_6686 = BgL_portz00_3733;
					}
				else
					{
						obj_t BgL_auxz00_6689;

						BgL_auxz00_6689 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(67779L),
							BGl_string2544z00zz__r4_ports_6_10_1z00,
							BGl_string2429z00zz__r4_ports_6_10_1z00, BgL_portz00_3733);
						FAILURE(BgL_auxz00_6689, BFALSE, BFALSE);
					}
				return
					BGl_outputzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BgL_auxz00_6686);
			}
		}

	}



/* output-port-buffer-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_outputzd2portzd2bufferzd2setz12zc0zz__r4_ports_6_10_1z00(obj_t
		BgL_portz00_171, obj_t BgL_bufferz00_172)
	{
		{	/* Ieee/port.scm 1503 */
			bgl_output_port_buffer_set(BgL_portz00_171, BgL_bufferz00_172);
			BUNSPEC;
			return BgL_portz00_171;
		}

	}



/* &output-port-buffer-set! */
	obj_t BGl_z62outputzd2portzd2bufferzd2setz12za2zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3734, obj_t BgL_portz00_3735, obj_t BgL_bufferz00_3736)
	{
		{	/* Ieee/port.scm 1503 */
			{	/* Ieee/port.scm 1504 */
				obj_t BgL_auxz00_6702;
				obj_t BgL_auxz00_6695;

				if (STRINGP(BgL_bufferz00_3736))
					{	/* Ieee/port.scm 1504 */
						BgL_auxz00_6702 = BgL_bufferz00_3736;
					}
				else
					{
						obj_t BgL_auxz00_6705;

						BgL_auxz00_6705 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(68130L),
							BGl_string2545z00zz__r4_ports_6_10_1z00,
							BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_bufferz00_3736);
						FAILURE(BgL_auxz00_6705, BFALSE, BFALSE);
					}
				if (OUTPUT_PORTP(BgL_portz00_3735))
					{	/* Ieee/port.scm 1504 */
						BgL_auxz00_6695 = BgL_portz00_3735;
					}
				else
					{
						obj_t BgL_auxz00_6698;

						BgL_auxz00_6698 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(68130L),
							BGl_string2545z00zz__r4_ports_6_10_1z00,
							BGl_string2429z00zz__r4_ports_6_10_1z00, BgL_portz00_3735);
						FAILURE(BgL_auxz00_6698, BFALSE, BFALSE);
					}
				return
					BGl_outputzd2portzd2bufferzd2setz12zc0zz__r4_ports_6_10_1z00
					(BgL_auxz00_6695, BgL_auxz00_6702);
			}
		}

	}



/* file-exists? */
	BGL_EXPORTED_DEF bool_t BGl_filezd2existszf3z21zz__r4_ports_6_10_1z00(char
		*BgL_namez00_173)
	{
		{	/* Ieee/port.scm 1509 */
			BGL_TAIL return fexists(BgL_namez00_173);
		}

	}



/* &file-exists? */
	obj_t BGl_z62filezd2existszf3z43zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3737,
		obj_t BgL_namez00_3738)
	{
		{	/* Ieee/port.scm 1509 */
			{	/* Ieee/port.scm 1510 */
				bool_t BgL_tmpz00_6711;

				{	/* Ieee/port.scm 1510 */
					char *BgL_auxz00_6712;

					{	/* Ieee/port.scm 1510 */
						obj_t BgL_tmpz00_6713;

						if (STRINGP(BgL_namez00_3738))
							{	/* Ieee/port.scm 1510 */
								BgL_tmpz00_6713 = BgL_namez00_3738;
							}
						else
							{
								obj_t BgL_auxz00_6716;

								BgL_auxz00_6716 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(68400L),
									BGl_string2546z00zz__r4_ports_6_10_1z00,
									BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_namez00_3738);
								FAILURE(BgL_auxz00_6716, BFALSE, BFALSE);
							}
						BgL_auxz00_6712 = BSTRING_TO_STRING(BgL_tmpz00_6713);
					}
					BgL_tmpz00_6711 =
						BGl_filezd2existszf3z21zz__r4_ports_6_10_1z00(BgL_auxz00_6712);
				}
				return BBOOL(BgL_tmpz00_6711);
			}
		}

	}



/* file-gzip? */
	BGL_EXPORTED_DEF obj_t BGl_filezd2gza7ipzf3z86zz__r4_ports_6_10_1z00(obj_t
		BgL_namez00_174)
	{
		{	/* Ieee/port.scm 1515 */
			if (fexists(BSTRING_TO_STRING(BgL_namez00_174)))
				{	/* Ieee/port.scm 1516 */
					return
						BGl_withzd2inputzd2fromzd2filezd2zz__r4_ports_6_10_1z00
						(BgL_namez00_174, BGl_proc2547z00zz__r4_ports_6_10_1z00);
				}
			else
				{	/* Ieee/port.scm 1516 */
					return BFALSE;
				}
		}

	}



/* &file-gzip? */
	obj_t BGl_z62filezd2gza7ipzf3ze4zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3740,
		obj_t BgL_namez00_3741)
	{
		{	/* Ieee/port.scm 1515 */
			return BGl_filezd2gza7ipzf3z86zz__r4_ports_6_10_1z00(BgL_namez00_3741);
		}

	}



/* &<@anonymous:1576> */
	obj_t BGl_z62zc3z04anonymousza31576ze3ze5zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3742)
	{
		{	/* Ieee/port.scm 1518 */
			{	/* Ieee/port.scm 1519 */
				obj_t BgL_env1150z00_4196;

				BgL_env1150z00_4196 = BGL_CURRENT_DYNAMIC_ENV();
				{	/* Ieee/port.scm 1519 */
					obj_t BgL_cell1145z00_4197;

					BgL_cell1145z00_4197 = MAKE_STACK_CELL(BUNSPEC);
					{	/* Ieee/port.scm 1519 */
						obj_t BgL_val1149z00_4198;

						BgL_val1149z00_4198 =
							BGl_zc3z04exitza31579ze3ze70z60zz__r4_ports_6_10_1z00
							(BgL_cell1145z00_4197, BgL_env1150z00_4196);
						if ((BgL_val1149z00_4198 == BgL_cell1145z00_4197))
							{	/* Ieee/port.scm 1519 */
								{	/* Ieee/port.scm 1519 */
									int BgL_tmpz00_6733;

									BgL_tmpz00_6733 = (int) (0L);
									BGL_SIGSETMASK(BgL_tmpz00_6733);
								}
								{	/* Ieee/port.scm 1519 */
									obj_t BgL_arg1578z00_4199;

									BgL_arg1578z00_4199 = CELL_REF(((obj_t) BgL_val1149z00_4198));
									return BFALSE;
								}
							}
						else
							{	/* Ieee/port.scm 1519 */
								return BgL_val1149z00_4198;
							}
					}
				}
			}
		}

	}



/* <@exit:1579>~0 */
	obj_t BGl_zc3z04exitza31579ze3ze70z60zz__r4_ports_6_10_1z00(obj_t
		BgL_cell1145z00_3804, obj_t BgL_env1150z00_3803)
	{
		{	/* Ieee/port.scm 1519 */
			jmp_buf_t jmpbuf;
			void *BgL_an_exit1154z00_1913;

			if (SET_EXIT(BgL_an_exit1154z00_1913))
				{
					return BGL_EXIT_VALUE();
				}
			else
				{
#if( SIGSETJMP_SAVESIGS == 0 )
				 // MS: CARE 5 jan 2021: see runtime/Clib/csystem.c
				 // bgl_restore_signal_handlers();
#endif

					BgL_an_exit1154z00_1913 = (void *) jmpbuf;
					PUSH_ENV_EXIT(BgL_env1150z00_3803, BgL_an_exit1154z00_1913, 1L);
					{	/* Ieee/port.scm 1519 */
						obj_t BgL_escape1146z00_1914;

						BgL_escape1146z00_1914 =
							BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1150z00_3803);
						{	/* Ieee/port.scm 1519 */
							obj_t BgL_res1157z00_1915;

							{	/* Ieee/port.scm 1519 */
								obj_t BgL_ohs1142z00_1916;

								BgL_ohs1142z00_1916 =
									BGL_ENV_ERROR_HANDLER_GET(BgL_env1150z00_3803);
								{	/* Ieee/port.scm 1519 */
									obj_t BgL_hds1143z00_1917;

									BgL_hds1143z00_1917 =
										MAKE_STACK_PAIR(BgL_escape1146z00_1914,
										BgL_cell1145z00_3804);
									BGL_ENV_ERROR_HANDLER_SET(BgL_env1150z00_3803,
										BgL_hds1143z00_1917);
									BUNSPEC;
									{	/* Ieee/port.scm 1519 */
										obj_t BgL_exitd1151z00_1918;

										BgL_exitd1151z00_1918 =
											BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1150z00_3803);
										{	/* Ieee/port.scm 1519 */
											obj_t BgL_tmp1153z00_1919;

											{	/* Ieee/port.scm 1519 */
												obj_t BgL_arg1582z00_1922;

												BgL_arg1582z00_1922 =
													BGL_EXITD_PROTECT(BgL_exitd1151z00_1918);
												BgL_tmp1153z00_1919 =
													MAKE_YOUNG_PAIR(BgL_ohs1142z00_1916,
													BgL_arg1582z00_1922);
											}
											{	/* Ieee/port.scm 1519 */

												BGL_EXITD_PROTECT_SET(BgL_exitd1151z00_1918,
													BgL_tmp1153z00_1919);
												BUNSPEC;
												{	/* Ieee/port.scm 1522 */
													obj_t BgL_tmp1152z00_1920;

													{	/* Ieee/port.scm 1522 */
														obj_t BgL_arg1580z00_1921;

														{	/* Ieee/port.scm 672 */
															obj_t BgL_tmpz00_6749;

															BgL_tmpz00_6749 = BGL_CURRENT_DYNAMIC_ENV();
															BgL_arg1580z00_1921 =
																BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_6749);
														}
														BgL_tmp1152z00_1920 =
															BGl_gunza7ipzd2parsezd2headerza7zz__gunza7ipza7
															(BgL_arg1580z00_1921);
													}
													{	/* Ieee/port.scm 1519 */
														bool_t BgL_test3174z00_6753;

														{	/* Ieee/port.scm 1519 */
															obj_t BgL_arg2013z00_2918;

															BgL_arg2013z00_2918 =
																BGL_EXITD_PROTECT(BgL_exitd1151z00_1918);
															BgL_test3174z00_6753 = PAIRP(BgL_arg2013z00_2918);
														}
														if (BgL_test3174z00_6753)
															{	/* Ieee/port.scm 1519 */
																obj_t BgL_arg2011z00_2919;

																{	/* Ieee/port.scm 1519 */
																	obj_t BgL_arg2012z00_2920;

																	BgL_arg2012z00_2920 =
																		BGL_EXITD_PROTECT(BgL_exitd1151z00_1918);
																	BgL_arg2011z00_2919 =
																		CDR(((obj_t) BgL_arg2012z00_2920));
																}
																BGL_EXITD_PROTECT_SET(BgL_exitd1151z00_1918,
																	BgL_arg2011z00_2919);
																BUNSPEC;
															}
														else
															{	/* Ieee/port.scm 1519 */
																BFALSE;
															}
													}
													BGL_ENV_ERROR_HANDLER_SET(BgL_env1150z00_3803,
														BgL_ohs1142z00_1916);
													BUNSPEC;
													BgL_res1157z00_1915 = BgL_tmp1152z00_1920;
												}
											}
										}
									}
								}
							}
							POP_ENV_EXIT(BgL_env1150z00_3803);
							return BgL_res1157z00_1915;
						}
					}
				}
		}

	}



/* delete-file */
	BGL_EXPORTED_DEF obj_t BGl_deletezd2filezd2zz__r4_ports_6_10_1z00(char
		*BgL_stringz00_175)
	{
		{	/* Ieee/port.scm 1527 */
			if (unlink(BgL_stringz00_175))
				{	/* Ieee/port.scm 1528 */
					return BFALSE;
				}
			else
				{	/* Ieee/port.scm 1528 */
					return BTRUE;
				}
		}

	}



/* &delete-file */
	obj_t BGl_z62deletezd2filezb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3743,
		obj_t BgL_stringz00_3744)
	{
		{	/* Ieee/port.scm 1527 */
			{	/* Ieee/port.scm 1528 */
				char *BgL_auxz00_6764;

				{	/* Ieee/port.scm 1528 */
					obj_t BgL_tmpz00_6765;

					if (STRINGP(BgL_stringz00_3744))
						{	/* Ieee/port.scm 1528 */
							BgL_tmpz00_6765 = BgL_stringz00_3744;
						}
					else
						{
							obj_t BgL_auxz00_6768;

							BgL_auxz00_6768 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(69104L),
								BGl_string2548z00zz__r4_ports_6_10_1z00,
								BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_stringz00_3744);
							FAILURE(BgL_auxz00_6768, BFALSE, BFALSE);
						}
					BgL_auxz00_6764 = BSTRING_TO_STRING(BgL_tmpz00_6765);
				}
				return BGl_deletezd2filezd2zz__r4_ports_6_10_1z00(BgL_auxz00_6764);
			}
		}

	}



/* make-directory */
	BGL_EXPORTED_DEF bool_t BGl_makezd2directoryzd2zz__r4_ports_6_10_1z00(char
		*BgL_stringz00_176)
	{
		{	/* Ieee/port.scm 1533 */
			return !BGL_MKDIR(BgL_stringz00_176, 511L);
		}

	}



/* &make-directory */
	obj_t BGl_z62makezd2directoryzb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3745,
		obj_t BgL_stringz00_3746)
	{
		{	/* Ieee/port.scm 1533 */
			{	/* Ieee/port.scm 1534 */
				bool_t BgL_tmpz00_6775;

				{	/* Ieee/port.scm 1534 */
					char *BgL_auxz00_6776;

					{	/* Ieee/port.scm 1534 */
						obj_t BgL_tmpz00_6777;

						if (STRINGP(BgL_stringz00_3746))
							{	/* Ieee/port.scm 1534 */
								BgL_tmpz00_6777 = BgL_stringz00_3746;
							}
						else
							{
								obj_t BgL_auxz00_6780;

								BgL_auxz00_6780 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(69399L),
									BGl_string2549z00zz__r4_ports_6_10_1z00,
									BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_stringz00_3746);
								FAILURE(BgL_auxz00_6780, BFALSE, BFALSE);
							}
						BgL_auxz00_6776 = BSTRING_TO_STRING(BgL_tmpz00_6777);
					}
					BgL_tmpz00_6775 =
						BGl_makezd2directoryzd2zz__r4_ports_6_10_1z00(BgL_auxz00_6776);
				}
				return BBOOL(BgL_tmpz00_6775);
			}
		}

	}



/* make-directories */
	BGL_EXPORTED_DEF bool_t BGl_makezd2directorieszd2zz__r4_ports_6_10_1z00(obj_t
		BgL_stringz00_177)
	{
		{	/* Ieee/port.scm 1539 */
			{	/* Ieee/port.scm 1540 */
				bool_t BgL__ortest_1159z00_1928;

				BgL__ortest_1159z00_1928 =
					bgl_directoryp(BSTRING_TO_STRING(BgL_stringz00_177));
				if (BgL__ortest_1159z00_1928)
					{	/* Ieee/port.scm 1540 */
						return BgL__ortest_1159z00_1928;
					}
				else
					{	/* Ieee/port.scm 1541 */
						bool_t BgL__ortest_1160z00_1929;

						{	/* Ieee/port.scm 1534 */
							char *BgL_tmpz00_6790;

							BgL_tmpz00_6790 = BSTRING_TO_STRING(BgL_stringz00_177);
							BgL__ortest_1160z00_1929 = !BGL_MKDIR(BgL_tmpz00_6790, 511L);
						}
						if (BgL__ortest_1160z00_1929)
							{	/* Ieee/port.scm 1541 */
								return BgL__ortest_1160z00_1929;
							}
						else
							{	/* Ieee/port.scm 1542 */
								obj_t BgL_dnamez00_1930;

								BgL_dnamez00_1930 = BGl_dirnamez00zz__osz00(BgL_stringz00_177);
								{	/* Ieee/port.scm 1543 */
									bool_t BgL_test3180z00_6795;

									if ((STRING_LENGTH(BgL_dnamez00_1930) == 0L))
										{	/* Ieee/port.scm 1543 */
											BgL_test3180z00_6795 = ((bool_t) 1);
										}
									else
										{	/* Ieee/port.scm 1543 */
											BgL_test3180z00_6795 =
												fexists(BSTRING_TO_STRING(BgL_dnamez00_1930));
										}
									if (BgL_test3180z00_6795)
										{	/* Ieee/port.scm 1543 */
											return ((bool_t) 0);
										}
									else
										{	/* Ieee/port.scm 1545 */
											bool_t BgL_auxz00_1935;

											BgL_auxz00_1935 =
												BGl_makezd2directorieszd2zz__r4_ports_6_10_1z00
												(BgL_dnamez00_1930);
											if ((STRING_REF(BgL_stringz00_177,
														(STRING_LENGTH(BgL_stringz00_177) - 1L)) ==
													(unsigned char) (FILE_SEPARATOR)))
												{	/* Ieee/port.scm 1546 */
													return BgL_auxz00_1935;
												}
											else
												{	/* Ieee/port.scm 1534 */
													char *BgL_tmpz00_6808;

													BgL_tmpz00_6808 =
														BSTRING_TO_STRING(BgL_stringz00_177);
													return !BGL_MKDIR(BgL_tmpz00_6808, 511L);
												}
										}
								}
							}
					}
			}
		}

	}



/* &make-directories */
	obj_t BGl_z62makezd2directorieszb0zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3747, obj_t BgL_stringz00_3748)
	{
		{	/* Ieee/port.scm 1539 */
			{	/* Ieee/port.scm 1540 */
				bool_t BgL_tmpz00_6811;

				{	/* Ieee/port.scm 1540 */
					obj_t BgL_auxz00_6812;

					if (STRINGP(BgL_stringz00_3748))
						{	/* Ieee/port.scm 1540 */
							BgL_auxz00_6812 = BgL_stringz00_3748;
						}
					else
						{
							obj_t BgL_auxz00_6815;

							BgL_auxz00_6815 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(69687L),
								BGl_string2550z00zz__r4_ports_6_10_1z00,
								BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_stringz00_3748);
							FAILURE(BgL_auxz00_6815, BFALSE, BFALSE);
						}
					BgL_tmpz00_6811 =
						BGl_makezd2directorieszd2zz__r4_ports_6_10_1z00(BgL_auxz00_6812);
				}
				return BBOOL(BgL_tmpz00_6811);
			}
		}

	}



/* delete-directory */
	BGL_EXPORTED_DEF obj_t BGl_deletezd2directoryzd2zz__r4_ports_6_10_1z00(char
		*BgL_stringz00_178)
	{
		{	/* Ieee/port.scm 1554 */
			if (rmdir(BgL_stringz00_178))
				{	/* Ieee/port.scm 1555 */
					return BFALSE;
				}
			else
				{	/* Ieee/port.scm 1555 */
					return BTRUE;
				}
		}

	}



/* &delete-directory */
	obj_t BGl_z62deletezd2directoryzb0zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3749, obj_t BgL_stringz00_3750)
	{
		{	/* Ieee/port.scm 1554 */
			{	/* Ieee/port.scm 1555 */
				char *BgL_auxz00_6823;

				{	/* Ieee/port.scm 1555 */
					obj_t BgL_tmpz00_6824;

					if (STRINGP(BgL_stringz00_3750))
						{	/* Ieee/port.scm 1555 */
							BgL_tmpz00_6824 = BgL_stringz00_3750;
						}
					else
						{
							obj_t BgL_auxz00_6827;

							BgL_auxz00_6827 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(70308L),
								BGl_string2551z00zz__r4_ports_6_10_1z00,
								BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_stringz00_3750);
							FAILURE(BgL_auxz00_6827, BFALSE, BFALSE);
						}
					BgL_auxz00_6823 = BSTRING_TO_STRING(BgL_tmpz00_6824);
				}
				return BGl_deletezd2directoryzd2zz__r4_ports_6_10_1z00(BgL_auxz00_6823);
			}
		}

	}



/* rename-file */
	BGL_EXPORTED_DEF bool_t BGl_renamezd2filezd2zz__r4_ports_6_10_1z00(char
		*BgL_string1z00_179, char *BgL_string2z00_180)
	{
		{	/* Ieee/port.scm 1560 */
			{	/* Ieee/port.scm 1561 */
				int BgL_arg1609z00_4200;

				BgL_arg1609z00_4200 = rename(BgL_string1z00_179, BgL_string2z00_180);
				return (BINT(BgL_arg1609z00_4200) == BINT(0L));
			}
		}

	}



/* &rename-file */
	obj_t BGl_z62renamezd2filezb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3751,
		obj_t BgL_string1z00_3752, obj_t BgL_string2z00_3753)
	{
		{	/* Ieee/port.scm 1560 */
			{	/* Ieee/port.scm 1561 */
				bool_t BgL_tmpz00_6837;

				{	/* Ieee/port.scm 1561 */
					char *BgL_auxz00_6847;
					char *BgL_auxz00_6838;

					{	/* Ieee/port.scm 1561 */
						obj_t BgL_tmpz00_6848;

						if (STRINGP(BgL_string2z00_3753))
							{	/* Ieee/port.scm 1561 */
								BgL_tmpz00_6848 = BgL_string2z00_3753;
							}
						else
							{
								obj_t BgL_auxz00_6851;

								BgL_auxz00_6851 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(70623L),
									BGl_string2552z00zz__r4_ports_6_10_1z00,
									BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_string2z00_3753);
								FAILURE(BgL_auxz00_6851, BFALSE, BFALSE);
							}
						BgL_auxz00_6847 = BSTRING_TO_STRING(BgL_tmpz00_6848);
					}
					{	/* Ieee/port.scm 1561 */
						obj_t BgL_tmpz00_6839;

						if (STRINGP(BgL_string1z00_3752))
							{	/* Ieee/port.scm 1561 */
								BgL_tmpz00_6839 = BgL_string1z00_3752;
							}
						else
							{
								obj_t BgL_auxz00_6842;

								BgL_auxz00_6842 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(70623L),
									BGl_string2552z00zz__r4_ports_6_10_1z00,
									BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_string1z00_3752);
								FAILURE(BgL_auxz00_6842, BFALSE, BFALSE);
							}
						BgL_auxz00_6838 = BSTRING_TO_STRING(BgL_tmpz00_6839);
					}
					BgL_tmpz00_6837 =
						BGl_renamezd2filezd2zz__r4_ports_6_10_1z00(BgL_auxz00_6838,
						BgL_auxz00_6847);
				}
				return BBOOL(BgL_tmpz00_6837);
			}
		}

	}



/* truncate-file */
	BGL_EXPORTED_DEF bool_t BGl_truncatezd2filezd2zz__r4_ports_6_10_1z00(char
		*BgL_pathz00_181, long BgL_siza7eza7_182)
	{
		{	/* Ieee/port.scm 1566 */
			{	/* Ieee/port.scm 1567 */
				int BgL_xz00_4201;

				BgL_xz00_4201 = truncate(BgL_pathz00_181, BgL_siza7eza7_182);
				return ((bool_t) 1);
			}
		}

	}



/* &truncate-file */
	obj_t BGl_z62truncatezd2filezb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3754,
		obj_t BgL_pathz00_3755, obj_t BgL_siza7eza7_3756)
	{
		{	/* Ieee/port.scm 1566 */
			{	/* Ieee/port.scm 1567 */
				bool_t BgL_tmpz00_6859;

				{	/* Ieee/port.scm 1567 */
					long BgL_auxz00_6869;
					char *BgL_auxz00_6860;

					{	/* Ieee/port.scm 1567 */
						obj_t BgL_tmpz00_6870;

						if (INTEGERP(BgL_siza7eza7_3756))
							{	/* Ieee/port.scm 1567 */
								BgL_tmpz00_6870 = BgL_siza7eza7_3756;
							}
						else
							{
								obj_t BgL_auxz00_6873;

								BgL_auxz00_6873 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(70933L),
									BGl_string2553z00zz__r4_ports_6_10_1z00,
									BGl_string2446z00zz__r4_ports_6_10_1z00, BgL_siza7eza7_3756);
								FAILURE(BgL_auxz00_6873, BFALSE, BFALSE);
							}
						BgL_auxz00_6869 = (long) CINT(BgL_tmpz00_6870);
					}
					{	/* Ieee/port.scm 1567 */
						obj_t BgL_tmpz00_6861;

						if (STRINGP(BgL_pathz00_3755))
							{	/* Ieee/port.scm 1567 */
								BgL_tmpz00_6861 = BgL_pathz00_3755;
							}
						else
							{
								obj_t BgL_auxz00_6864;

								BgL_auxz00_6864 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(70933L),
									BGl_string2553z00zz__r4_ports_6_10_1z00,
									BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_pathz00_3755);
								FAILURE(BgL_auxz00_6864, BFALSE, BFALSE);
							}
						BgL_auxz00_6860 = BSTRING_TO_STRING(BgL_tmpz00_6861);
					}
					BgL_tmpz00_6859 =
						BGl_truncatezd2filezd2zz__r4_ports_6_10_1z00(BgL_auxz00_6860,
						BgL_auxz00_6869);
				}
				return BBOOL(BgL_tmpz00_6859);
			}
		}

	}



/* output-port-truncate */
	BGL_EXPORTED_DEF bool_t
		BGl_outputzd2portzd2truncatez00zz__r4_ports_6_10_1z00(obj_t
		BgL_oportz00_183, long BgL_siza7eza7_184)
	{
		{	/* Ieee/port.scm 1572 */
			{	/* Ieee/port.scm 1573 */
				int BgL_xz00_4202;

				BgL_xz00_4202 =
					bgl_output_port_truncate(BgL_oportz00_183, BgL_siza7eza7_184);
				return ((bool_t) 1);
			}
		}

	}



/* &output-port-truncate */
	obj_t BGl_z62outputzd2portzd2truncatez62zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3757, obj_t BgL_oportz00_3758, obj_t BgL_siza7eza7_3759)
	{
		{	/* Ieee/port.scm 1572 */
			{	/* Ieee/port.scm 1573 */
				bool_t BgL_tmpz00_6881;

				{	/* Ieee/port.scm 1573 */
					long BgL_auxz00_6889;
					obj_t BgL_auxz00_6882;

					{	/* Ieee/port.scm 1573 */
						obj_t BgL_tmpz00_6890;

						if (INTEGERP(BgL_siza7eza7_3759))
							{	/* Ieee/port.scm 1573 */
								BgL_tmpz00_6890 = BgL_siza7eza7_3759;
							}
						else
							{
								obj_t BgL_auxz00_6893;

								BgL_auxz00_6893 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(71236L),
									BGl_string2554z00zz__r4_ports_6_10_1z00,
									BGl_string2446z00zz__r4_ports_6_10_1z00, BgL_siza7eza7_3759);
								FAILURE(BgL_auxz00_6893, BFALSE, BFALSE);
							}
						BgL_auxz00_6889 = (long) CINT(BgL_tmpz00_6890);
					}
					if (OUTPUT_PORTP(BgL_oportz00_3758))
						{	/* Ieee/port.scm 1573 */
							BgL_auxz00_6882 = BgL_oportz00_3758;
						}
					else
						{
							obj_t BgL_auxz00_6885;

							BgL_auxz00_6885 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(71236L),
								BGl_string2554z00zz__r4_ports_6_10_1z00,
								BGl_string2429z00zz__r4_ports_6_10_1z00, BgL_oportz00_3758);
							FAILURE(BgL_auxz00_6885, BFALSE, BFALSE);
						}
					BgL_tmpz00_6881 =
						BGl_outputzd2portzd2truncatez00zz__r4_ports_6_10_1z00
						(BgL_auxz00_6882, BgL_auxz00_6889);
				}
				return BBOOL(BgL_tmpz00_6881);
			}
		}

	}



/* copy-file */
	BGL_EXPORTED_DEF obj_t BGl_copyzd2filezd2zz__r4_ports_6_10_1z00(char
		*BgL_string1z00_185, char *BgL_string2z00_186)
	{
		{	/* Ieee/port.scm 1578 */
			{	/* Ieee/port.scm 1579 */
				obj_t BgL_piz00_1951;

				BgL_piz00_1951 =
					BGl_openzd2inputzd2binaryzd2filezd2zz__binaryz00(string_to_bstring
					(BgL_string1z00_185));
				if (BINARY_PORTP(BgL_piz00_1951))
					{	/* Ieee/port.scm 1581 */
						obj_t BgL_poz00_1953;

						BgL_poz00_1953 =
							BGl_openzd2outputzd2binaryzd2filezd2zz__binaryz00
							(string_to_bstring(BgL_string2z00_186));
						if (BINARY_PORTP(BgL_poz00_1953))
							{	/* Ieee/port.scm 1586 */
								obj_t BgL_sz00_1955;

								{	/* Ieee/string.scm 172 */

									BgL_sz00_1955 = make_string(1024L, ((unsigned char) ' '));
								}
								{	/* Ieee/port.scm 1587 */
									int BgL_g1161z00_1956;

									BgL_g1161z00_1956 =
										bgl_input_fill_string(BgL_piz00_1951, BgL_sz00_1955);
									{
										int BgL_lz00_1958;

										{	/* Ieee/port.scm 1587 */
											bool_t BgL_tmpz00_6910;

											BgL_lz00_1958 = BgL_g1161z00_1956;
										BgL_zc3z04anonymousza31612ze3z87_1959:
											if (((long) (BgL_lz00_1958) == 1024L))
												{	/* Ieee/port.scm 1588 */
													bgl_output_string(
														((obj_t) BgL_poz00_1953), BgL_sz00_1955);
													{	/* Ieee/port.scm 1591 */
														int BgL_arg1615z00_1961;

														BgL_arg1615z00_1961 =
															bgl_input_fill_string(
															((obj_t) BgL_piz00_1951), BgL_sz00_1955);
														{
															int BgL_lz00_6918;

															BgL_lz00_6918 = BgL_arg1615z00_1961;
															BgL_lz00_1958 = BgL_lz00_6918;
															goto BgL_zc3z04anonymousza31612ze3z87_1959;
														}
													}
												}
											else
												{	/* Ieee/port.scm 1588 */
													{	/* Ieee/port.scm 1593 */
														obj_t BgL_arg1616z00_1962;

														{	/* Ieee/port.scm 1593 */
															long BgL_tmpz00_6919;

															BgL_tmpz00_6919 = (long) (BgL_lz00_1958);
															BgL_arg1616z00_1962 =
																bgl_string_shrink(BgL_sz00_1955,
																BgL_tmpz00_6919);
														}
														bgl_output_string(
															((obj_t) BgL_poz00_1953), BgL_arg1616z00_1962);
													}
													close_binary_port(((obj_t) BgL_piz00_1951));
													close_binary_port(((obj_t) BgL_poz00_1953));
													BgL_tmpz00_6910 = ((bool_t) 1);
												}
											return BBOOL(BgL_tmpz00_6910);
										}
									}
								}
							}
						else
							{	/* Ieee/port.scm 1582 */
								close_binary_port(BgL_piz00_1951);
								return BFALSE;
							}
					}
				else
					{	/* Ieee/port.scm 1580 */
						return BFALSE;
					}
			}
		}

	}



/* &copy-file */
	obj_t BGl_z62copyzd2filezb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3760,
		obj_t BgL_string1z00_3761, obj_t BgL_string2z00_3762)
	{
		{	/* Ieee/port.scm 1578 */
			{	/* Ieee/port.scm 1579 */
				char *BgL_auxz00_6939;
				char *BgL_auxz00_6930;

				{	/* Ieee/port.scm 1579 */
					obj_t BgL_tmpz00_6940;

					if (STRINGP(BgL_string2z00_3762))
						{	/* Ieee/port.scm 1579 */
							BgL_tmpz00_6940 = BgL_string2z00_3762;
						}
					else
						{
							obj_t BgL_auxz00_6943;

							BgL_auxz00_6943 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(71523L),
								BGl_string2555z00zz__r4_ports_6_10_1z00,
								BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_string2z00_3762);
							FAILURE(BgL_auxz00_6943, BFALSE, BFALSE);
						}
					BgL_auxz00_6939 = BSTRING_TO_STRING(BgL_tmpz00_6940);
				}
				{	/* Ieee/port.scm 1579 */
					obj_t BgL_tmpz00_6931;

					if (STRINGP(BgL_string1z00_3761))
						{	/* Ieee/port.scm 1579 */
							BgL_tmpz00_6931 = BgL_string1z00_3761;
						}
					else
						{
							obj_t BgL_auxz00_6934;

							BgL_auxz00_6934 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(71523L),
								BGl_string2555z00zz__r4_ports_6_10_1z00,
								BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_string1z00_3761);
							FAILURE(BgL_auxz00_6934, BFALSE, BFALSE);
						}
					BgL_auxz00_6930 = BSTRING_TO_STRING(BgL_tmpz00_6931);
				}
				return
					BGl_copyzd2filezd2zz__r4_ports_6_10_1z00(BgL_auxz00_6930,
					BgL_auxz00_6939);
			}
		}

	}



/* port? */
	BGL_EXPORTED_DEF bool_t BGl_portzf3zf3zz__r4_ports_6_10_1z00(obj_t
		BgL_objz00_187)
	{
		{	/* Ieee/port.scm 1601 */
			{	/* Ieee/port.scm 1602 */
				bool_t BgL__ortest_1162z00_4203;

				BgL__ortest_1162z00_4203 = OUTPUT_PORTP(BgL_objz00_187);
				if (BgL__ortest_1162z00_4203)
					{	/* Ieee/port.scm 1602 */
						return BgL__ortest_1162z00_4203;
					}
				else
					{	/* Ieee/port.scm 1602 */
						return INPUT_PORTP(BgL_objz00_187);
					}
			}
		}

	}



/* &port? */
	obj_t BGl_z62portzf3z91zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3763,
		obj_t BgL_objz00_3764)
	{
		{	/* Ieee/port.scm 1601 */
			return BBOOL(BGl_portzf3zf3zz__r4_ports_6_10_1z00(BgL_objz00_3764));
		}

	}



/* directory? */
	BGL_EXPORTED_DEF bool_t BGl_directoryzf3zf3zz__r4_ports_6_10_1z00(char
		*BgL_stringz00_188)
	{
		{	/* Ieee/port.scm 1607 */
			BGL_TAIL return bgl_directoryp(BgL_stringz00_188);
		}

	}



/* &directory? */
	obj_t BGl_z62directoryzf3z91zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3765,
		obj_t BgL_stringz00_3766)
	{
		{	/* Ieee/port.scm 1607 */
			{	/* Ieee/port.scm 1608 */
				bool_t BgL_tmpz00_6955;

				{	/* Ieee/port.scm 1608 */
					char *BgL_auxz00_6956;

					{	/* Ieee/port.scm 1608 */
						obj_t BgL_tmpz00_6957;

						if (STRINGP(BgL_stringz00_3766))
							{	/* Ieee/port.scm 1608 */
								BgL_tmpz00_6957 = BgL_stringz00_3766;
							}
						else
							{
								obj_t BgL_auxz00_6960;

								BgL_auxz00_6960 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(72611L),
									BGl_string2556z00zz__r4_ports_6_10_1z00,
									BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_stringz00_3766);
								FAILURE(BgL_auxz00_6960, BFALSE, BFALSE);
							}
						BgL_auxz00_6956 = BSTRING_TO_STRING(BgL_tmpz00_6957);
					}
					BgL_tmpz00_6955 =
						BGl_directoryzf3zf3zz__r4_ports_6_10_1z00(BgL_auxz00_6956);
				}
				return BBOOL(BgL_tmpz00_6955);
			}
		}

	}



/* directory-length */
	BGL_EXPORTED_DEF obj_t BGl_directoryzd2lengthzd2zz__r4_ports_6_10_1z00(char
		*BgL_stringz00_189)
	{
		{	/* Ieee/port.scm 1613 */
			return BINT(bgl_directory_length(BgL_stringz00_189));
		}

	}



/* &directory-length */
	obj_t BGl_z62directoryzd2lengthzb0zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3767, obj_t BgL_stringz00_3768)
	{
		{	/* Ieee/port.scm 1613 */
			{	/* Ieee/port.scm 1614 */
				char *BgL_auxz00_6969;

				{	/* Ieee/port.scm 1614 */
					obj_t BgL_tmpz00_6970;

					if (STRINGP(BgL_stringz00_3768))
						{	/* Ieee/port.scm 1614 */
							BgL_tmpz00_6970 = BgL_stringz00_3768;
						}
					else
						{
							obj_t BgL_auxz00_6973;

							BgL_auxz00_6973 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(72900L),
								BGl_string2557z00zz__r4_ports_6_10_1z00,
								BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_stringz00_3768);
							FAILURE(BgL_auxz00_6973, BFALSE, BFALSE);
						}
					BgL_auxz00_6969 = BSTRING_TO_STRING(BgL_tmpz00_6970);
				}
				return BGl_directoryzd2lengthzd2zz__r4_ports_6_10_1z00(BgL_auxz00_6969);
			}
		}

	}



/* directory->list */
	BGL_EXPORTED_DEF obj_t BGl_directoryzd2ze3listz31zz__r4_ports_6_10_1z00(char
		*BgL_stringz00_190)
	{
		{	/* Ieee/port.scm 1619 */
			BGL_TAIL return bgl_directory_to_list(BgL_stringz00_190);
		}

	}



/* &directory->list */
	obj_t BGl_z62directoryzd2ze3listz53zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3769, obj_t BgL_stringz00_3770)
	{
		{	/* Ieee/port.scm 1619 */
			{	/* Ieee/port.scm 1620 */
				char *BgL_auxz00_6980;

				{	/* Ieee/port.scm 1620 */
					obj_t BgL_tmpz00_6981;

					if (STRINGP(BgL_stringz00_3770))
						{	/* Ieee/port.scm 1620 */
							BgL_tmpz00_6981 = BgL_stringz00_3770;
						}
					else
						{
							obj_t BgL_auxz00_6984;

							BgL_auxz00_6984 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(73194L),
								BGl_string2558z00zz__r4_ports_6_10_1z00,
								BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_stringz00_3770);
							FAILURE(BgL_auxz00_6984, BFALSE, BFALSE);
						}
					BgL_auxz00_6980 = BSTRING_TO_STRING(BgL_tmpz00_6981);
				}
				return
					BGl_directoryzd2ze3listz31zz__r4_ports_6_10_1z00(BgL_auxz00_6980);
			}
		}

	}



/* directory->path-list */
	BGL_EXPORTED_DEF obj_t
		BGl_directoryzd2ze3pathzd2listze3zz__r4_ports_6_10_1z00(obj_t
		BgL_dirz00_191)
	{
		{	/* Ieee/port.scm 1625 */
			{	/* Ieee/port.scm 1626 */
				long BgL_lz00_1967;

				BgL_lz00_1967 = STRING_LENGTH(BgL_dirz00_191);
				if ((BgL_lz00_1967 == 0L))
					{	/* Ieee/port.scm 1628 */
						return BNIL;
					}
				else
					{	/* Ieee/port.scm 1628 */
						if (
							(STRING_REF(BgL_dirz00_191,
									(BgL_lz00_1967 - 1L)) == (unsigned char) (FILE_SEPARATOR)))
							{	/* Ieee/port.scm 1633 */
								long BgL_arg1622z00_1973;

								BgL_arg1622z00_1973 = (BgL_lz00_1967 - 1L);
								return
									bgl_directory_to_path_list(BSTRING_TO_STRING(BgL_dirz00_191),
									(int) (BgL_arg1622z00_1973), FILE_SEPARATOR);
							}
						else
							{	/* Ieee/port.scm 1630 */
								return
									bgl_directory_to_path_list(BSTRING_TO_STRING(BgL_dirz00_191),
									(int) (BgL_lz00_1967), FILE_SEPARATOR);
		}}}}

	}



/* &directory->path-list */
	obj_t BGl_z62directoryzd2ze3pathzd2listz81zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3771, obj_t BgL_dirz00_3772)
	{
		{	/* Ieee/port.scm 1625 */
			{	/* Ieee/port.scm 1626 */
				obj_t BgL_auxz00_7005;

				if (STRINGP(BgL_dirz00_3772))
					{	/* Ieee/port.scm 1626 */
						BgL_auxz00_7005 = BgL_dirz00_3772;
					}
				else
					{
						obj_t BgL_auxz00_7008;

						BgL_auxz00_7008 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(73482L),
							BGl_string2559z00zz__r4_ports_6_10_1z00,
							BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_dirz00_3772);
						FAILURE(BgL_auxz00_7008, BFALSE, BFALSE);
					}
				return
					BGl_directoryzd2ze3pathzd2listze3zz__r4_ports_6_10_1z00
					(BgL_auxz00_7005);
			}
		}

	}



/* directory->vector */
	BGL_EXPORTED_DEF obj_t BGl_directoryzd2ze3vectorz31zz__r4_ports_6_10_1z00(char
		*BgL_stringz00_192)
	{
		{	/* Ieee/port.scm 1648 */
			BGL_TAIL return bgl_directory_to_vector(BgL_stringz00_192);
		}

	}



/* &directory->vector */
	obj_t BGl_z62directoryzd2ze3vectorz53zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3773, obj_t BgL_stringz00_3774)
	{
		{	/* Ieee/port.scm 1648 */
			{	/* Ieee/port.scm 1649 */
				char *BgL_auxz00_7014;

				{	/* Ieee/port.scm 1649 */
					obj_t BgL_tmpz00_7015;

					if (STRINGP(BgL_stringz00_3774))
						{	/* Ieee/port.scm 1649 */
							BgL_tmpz00_7015 = BgL_stringz00_3774;
						}
					else
						{
							obj_t BgL_auxz00_7018;

							BgL_auxz00_7018 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(74246L),
								BGl_string2560z00zz__r4_ports_6_10_1z00,
								BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_stringz00_3774);
							FAILURE(BgL_auxz00_7018, BFALSE, BFALSE);
						}
					BgL_auxz00_7014 = BSTRING_TO_STRING(BgL_tmpz00_7015);
				}
				return
					BGl_directoryzd2ze3vectorz31zz__r4_ports_6_10_1z00(BgL_auxz00_7014);
			}
		}

	}



/* directory->path-vector */
	BGL_EXPORTED_DEF obj_t
		BGl_directoryzd2ze3pathzd2vectorze3zz__r4_ports_6_10_1z00(obj_t
		BgL_dirz00_193)
	{
		{	/* Ieee/port.scm 1654 */
			{	/* Ieee/port.scm 1655 */
				long BgL_lz00_1979;

				BgL_lz00_1979 = STRING_LENGTH(BgL_dirz00_193);
				if ((BgL_lz00_1979 == 0L))
					{	/* Ieee/port.scm 1657 */
						return BGl_vector2561z00zz__r4_ports_6_10_1z00;
					}
				else
					{	/* Ieee/port.scm 1657 */
						if (
							(STRING_REF(BgL_dirz00_193,
									(BgL_lz00_1979 - 1L)) == (unsigned char) (FILE_SEPARATOR)))
							{	/* Ieee/port.scm 1662 */
								long BgL_arg1634z00_1985;

								BgL_arg1634z00_1985 = (BgL_lz00_1979 - 1L);
								return
									bgl_directory_to_path_vector(BSTRING_TO_STRING
									(BgL_dirz00_193), (int) (BgL_arg1634z00_1985),
									FILE_SEPARATOR);
							}
						else
							{	/* Ieee/port.scm 1659 */
								return
									bgl_directory_to_path_vector(BSTRING_TO_STRING
									(BgL_dirz00_193), (int) (BgL_lz00_1979), FILE_SEPARATOR);
		}}}}

	}



/* &directory->path-vector */
	obj_t BGl_z62directoryzd2ze3pathzd2vectorz81zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3775, obj_t BgL_dirz00_3776)
	{
		{	/* Ieee/port.scm 1654 */
			{	/* Ieee/port.scm 1655 */
				obj_t BgL_auxz00_7039;

				if (STRINGP(BgL_dirz00_3776))
					{	/* Ieee/port.scm 1655 */
						BgL_auxz00_7039 = BgL_dirz00_3776;
					}
				else
					{
						obj_t BgL_auxz00_7042;

						BgL_auxz00_7042 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(74538L),
							BGl_string2562z00zz__r4_ports_6_10_1z00,
							BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_dirz00_3776);
						FAILURE(BgL_auxz00_7042, BFALSE, BFALSE);
					}
				return
					BGl_directoryzd2ze3pathzd2vectorze3zz__r4_ports_6_10_1z00
					(BgL_auxz00_7039);
			}
		}

	}



/* file-modification-time */
	BGL_EXPORTED_DEF long
		BGl_filezd2modificationzd2timez00zz__r4_ports_6_10_1z00(char
		*BgL_filez00_194)
	{
		{	/* Ieee/port.scm 1677 */
			BGL_TAIL return bgl_last_modification_time(BgL_filez00_194);
		}

	}



/* &file-modification-time */
	obj_t BGl_z62filezd2modificationzd2timez62zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3777, obj_t BgL_filez00_3778)
	{
		{	/* Ieee/port.scm 1677 */
			{	/* Ieee/port.scm 1678 */
				long BgL_tmpz00_7048;

				{	/* Ieee/port.scm 1678 */
					char *BgL_auxz00_7049;

					{	/* Ieee/port.scm 1678 */
						obj_t BgL_tmpz00_7050;

						if (STRINGP(BgL_filez00_3778))
							{	/* Ieee/port.scm 1678 */
								BgL_tmpz00_7050 = BgL_filez00_3778;
							}
						else
							{
								obj_t BgL_auxz00_7053;

								BgL_auxz00_7053 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(75310L),
									BGl_string2563z00zz__r4_ports_6_10_1z00,
									BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_filez00_3778);
								FAILURE(BgL_auxz00_7053, BFALSE, BFALSE);
							}
						BgL_auxz00_7049 = BSTRING_TO_STRING(BgL_tmpz00_7050);
					}
					BgL_tmpz00_7048 =
						BGl_filezd2modificationzd2timez00zz__r4_ports_6_10_1z00
						(BgL_auxz00_7049);
				}
				return make_belong(BgL_tmpz00_7048);
			}
		}

	}



/* file-change-time */
	BGL_EXPORTED_DEF long BGl_filezd2changezd2timez00zz__r4_ports_6_10_1z00(char
		*BgL_filez00_195)
	{
		{	/* Ieee/port.scm 1683 */
			BGL_TAIL return bgl_last_change_time(BgL_filez00_195);
		}

	}



/* &file-change-time */
	obj_t BGl_z62filezd2changezd2timez62zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3779, obj_t BgL_filez00_3780)
	{
		{	/* Ieee/port.scm 1683 */
			{	/* Ieee/port.scm 1684 */
				long BgL_tmpz00_7061;

				{	/* Ieee/port.scm 1684 */
					char *BgL_auxz00_7062;

					{	/* Ieee/port.scm 1684 */
						obj_t BgL_tmpz00_7063;

						if (STRINGP(BgL_filez00_3780))
							{	/* Ieee/port.scm 1684 */
								BgL_tmpz00_7063 = BgL_filez00_3780;
							}
						else
							{
								obj_t BgL_auxz00_7066;

								BgL_auxz00_7066 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(75602L),
									BGl_string2564z00zz__r4_ports_6_10_1z00,
									BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_filez00_3780);
								FAILURE(BgL_auxz00_7066, BFALSE, BFALSE);
							}
						BgL_auxz00_7062 = BSTRING_TO_STRING(BgL_tmpz00_7063);
					}
					BgL_tmpz00_7061 =
						BGl_filezd2changezd2timez00zz__r4_ports_6_10_1z00(BgL_auxz00_7062);
				}
				return make_belong(BgL_tmpz00_7061);
			}
		}

	}



/* file-access-time */
	BGL_EXPORTED_DEF long BGl_filezd2accesszd2timez00zz__r4_ports_6_10_1z00(char
		*BgL_filez00_196)
	{
		{	/* Ieee/port.scm 1689 */
			BGL_TAIL return bgl_last_access_time(BgL_filez00_196);
		}

	}



/* &file-access-time */
	obj_t BGl_z62filezd2accesszd2timez62zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3781, obj_t BgL_filez00_3782)
	{
		{	/* Ieee/port.scm 1689 */
			{	/* Ieee/port.scm 1690 */
				long BgL_tmpz00_7074;

				{	/* Ieee/port.scm 1690 */
					char *BgL_auxz00_7075;

					{	/* Ieee/port.scm 1690 */
						obj_t BgL_tmpz00_7076;

						if (STRINGP(BgL_filez00_3782))
							{	/* Ieee/port.scm 1690 */
								BgL_tmpz00_7076 = BgL_filez00_3782;
							}
						else
							{
								obj_t BgL_auxz00_7079;

								BgL_auxz00_7079 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(75888L),
									BGl_string2565z00zz__r4_ports_6_10_1z00,
									BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_filez00_3782);
								FAILURE(BgL_auxz00_7079, BFALSE, BFALSE);
							}
						BgL_auxz00_7075 = BSTRING_TO_STRING(BgL_tmpz00_7076);
					}
					BgL_tmpz00_7074 =
						BGl_filezd2accesszd2timez00zz__r4_ports_6_10_1z00(BgL_auxz00_7075);
				}
				return make_belong(BgL_tmpz00_7074);
			}
		}

	}



/* file-times-set! */
	BGL_EXPORTED_DEF int BGl_filezd2timeszd2setz12z12zz__r4_ports_6_10_1z00(char
		*BgL_filez00_197, long BgL_atimez00_198, long BgL_mtimez00_199)
	{
		{	/* Ieee/port.scm 1695 */
			BGL_TAIL return
				bgl_utime(BgL_filez00_197, BgL_atimez00_198, BgL_mtimez00_199);
		}

	}



/* &file-times-set! */
	obj_t BGl_z62filezd2timeszd2setz12z70zz__r4_ports_6_10_1z00(obj_t
		BgL_envz00_3783, obj_t BgL_filez00_3784, obj_t BgL_atimez00_3785,
		obj_t BgL_mtimez00_3786)
	{
		{	/* Ieee/port.scm 1695 */
			{	/* Ieee/port.scm 1696 */
				int BgL_tmpz00_7087;

				{	/* Ieee/port.scm 1696 */
					long BgL_auxz00_7106;
					long BgL_auxz00_7097;
					char *BgL_auxz00_7088;

					{	/* Ieee/port.scm 1696 */
						obj_t BgL_tmpz00_7107;

						if (ELONGP(BgL_mtimez00_3786))
							{	/* Ieee/port.scm 1696 */
								BgL_tmpz00_7107 = BgL_mtimez00_3786;
							}
						else
							{
								obj_t BgL_auxz00_7110;

								BgL_auxz00_7110 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(76185L),
									BGl_string2566z00zz__r4_ports_6_10_1z00,
									BGl_string2567z00zz__r4_ports_6_10_1z00, BgL_mtimez00_3786);
								FAILURE(BgL_auxz00_7110, BFALSE, BFALSE);
							}
						BgL_auxz00_7106 = BELONG_TO_LONG(BgL_tmpz00_7107);
					}
					{	/* Ieee/port.scm 1696 */
						obj_t BgL_tmpz00_7098;

						if (ELONGP(BgL_atimez00_3785))
							{	/* Ieee/port.scm 1696 */
								BgL_tmpz00_7098 = BgL_atimez00_3785;
							}
						else
							{
								obj_t BgL_auxz00_7101;

								BgL_auxz00_7101 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(76185L),
									BGl_string2566z00zz__r4_ports_6_10_1z00,
									BGl_string2567z00zz__r4_ports_6_10_1z00, BgL_atimez00_3785);
								FAILURE(BgL_auxz00_7101, BFALSE, BFALSE);
							}
						BgL_auxz00_7097 = BELONG_TO_LONG(BgL_tmpz00_7098);
					}
					{	/* Ieee/port.scm 1696 */
						obj_t BgL_tmpz00_7089;

						if (STRINGP(BgL_filez00_3784))
							{	/* Ieee/port.scm 1696 */
								BgL_tmpz00_7089 = BgL_filez00_3784;
							}
						else
							{
								obj_t BgL_auxz00_7092;

								BgL_auxz00_7092 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(76185L),
									BGl_string2566z00zz__r4_ports_6_10_1z00,
									BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_filez00_3784);
								FAILURE(BgL_auxz00_7092, BFALSE, BFALSE);
							}
						BgL_auxz00_7088 = BSTRING_TO_STRING(BgL_tmpz00_7089);
					}
					BgL_tmpz00_7087 =
						BGl_filezd2timeszd2setz12z12zz__r4_ports_6_10_1z00(BgL_auxz00_7088,
						BgL_auxz00_7097, BgL_auxz00_7106);
				}
				return BINT(BgL_tmpz00_7087);
			}
		}

	}



/* file-size */
	BGL_EXPORTED_DEF long BGl_filezd2siza7ez75zz__r4_ports_6_10_1z00(char
		*BgL_filez00_200)
	{
		{	/* Ieee/port.scm 1701 */
			BGL_TAIL return bgl_file_size(BgL_filez00_200);
		}

	}



/* &file-size */
	obj_t BGl_z62filezd2siza7ez17zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3787,
		obj_t BgL_filez00_3788)
	{
		{	/* Ieee/port.scm 1701 */
			{	/* Ieee/port.scm 1702 */
				long BgL_tmpz00_7118;

				{	/* Ieee/port.scm 1702 */
					char *BgL_auxz00_7119;

					{	/* Ieee/port.scm 1702 */
						obj_t BgL_tmpz00_7120;

						if (STRINGP(BgL_filez00_3788))
							{	/* Ieee/port.scm 1702 */
								BgL_tmpz00_7120 = BgL_filez00_3788;
							}
						else
							{
								obj_t BgL_auxz00_7123;

								BgL_auxz00_7123 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(76470L),
									BGl_string2568z00zz__r4_ports_6_10_1z00,
									BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_filez00_3788);
								FAILURE(BgL_auxz00_7123, BFALSE, BFALSE);
							}
						BgL_auxz00_7119 = BSTRING_TO_STRING(BgL_tmpz00_7120);
					}
					BgL_tmpz00_7118 =
						BGl_filezd2siza7ez75zz__r4_ports_6_10_1z00(BgL_auxz00_7119);
				}
				return make_belong(BgL_tmpz00_7118);
			}
		}

	}



/* file-uid */
	BGL_EXPORTED_DEF int BGl_filezd2uidzd2zz__r4_ports_6_10_1z00(char
		*BgL_filez00_201)
	{
		{	/* Ieee/port.scm 1707 */
			return (int) (bgl_file_uid(BgL_filez00_201));
		}

	}



/* &file-uid */
	obj_t BGl_z62filezd2uidzb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3789,
		obj_t BgL_filez00_3790)
	{
		{	/* Ieee/port.scm 1707 */
			{	/* Ieee/port.scm 1708 */
				int BgL_tmpz00_7132;

				{	/* Ieee/port.scm 1708 */
					char *BgL_auxz00_7133;

					{	/* Ieee/port.scm 1708 */
						obj_t BgL_tmpz00_7134;

						if (STRINGP(BgL_filez00_3790))
							{	/* Ieee/port.scm 1708 */
								BgL_tmpz00_7134 = BgL_filez00_3790;
							}
						else
							{
								obj_t BgL_auxz00_7137;

								BgL_auxz00_7137 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(76746L),
									BGl_string2569z00zz__r4_ports_6_10_1z00,
									BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_filez00_3790);
								FAILURE(BgL_auxz00_7137, BFALSE, BFALSE);
							}
						BgL_auxz00_7133 = BSTRING_TO_STRING(BgL_tmpz00_7134);
					}
					BgL_tmpz00_7132 =
						BGl_filezd2uidzd2zz__r4_ports_6_10_1z00(BgL_auxz00_7133);
				}
				return BINT(BgL_tmpz00_7132);
			}
		}

	}



/* file-gid */
	BGL_EXPORTED_DEF int BGl_filezd2gidzd2zz__r4_ports_6_10_1z00(char
		*BgL_filez00_202)
	{
		{	/* Ieee/port.scm 1713 */
			return (int) (bgl_file_gid(BgL_filez00_202));
		}

	}



/* &file-gid */
	obj_t BGl_z62filezd2gidzb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3791,
		obj_t BgL_filez00_3792)
	{
		{	/* Ieee/port.scm 1713 */
			{	/* Ieee/port.scm 1714 */
				int BgL_tmpz00_7146;

				{	/* Ieee/port.scm 1714 */
					char *BgL_auxz00_7147;

					{	/* Ieee/port.scm 1714 */
						obj_t BgL_tmpz00_7148;

						if (STRINGP(BgL_filez00_3792))
							{	/* Ieee/port.scm 1714 */
								BgL_tmpz00_7148 = BgL_filez00_3792;
							}
						else
							{
								obj_t BgL_auxz00_7151;

								BgL_auxz00_7151 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(77021L),
									BGl_string2570z00zz__r4_ports_6_10_1z00,
									BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_filez00_3792);
								FAILURE(BgL_auxz00_7151, BFALSE, BFALSE);
							}
						BgL_auxz00_7147 = BSTRING_TO_STRING(BgL_tmpz00_7148);
					}
					BgL_tmpz00_7146 =
						BGl_filezd2gidzd2zz__r4_ports_6_10_1z00(BgL_auxz00_7147);
				}
				return BINT(BgL_tmpz00_7146);
			}
		}

	}



/* file-mode */
	BGL_EXPORTED_DEF int BGl_filezd2modezd2zz__r4_ports_6_10_1z00(char
		*BgL_filez00_203)
	{
		{	/* Ieee/port.scm 1719 */
			return (int) (bgl_file_mode(BgL_filez00_203));
		}

	}



/* &file-mode */
	obj_t BGl_z62filezd2modezb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3793,
		obj_t BgL_filez00_3794)
	{
		{	/* Ieee/port.scm 1719 */
			{	/* Ieee/port.scm 1720 */
				int BgL_tmpz00_7160;

				{	/* Ieee/port.scm 1720 */
					char *BgL_auxz00_7161;

					{	/* Ieee/port.scm 1720 */
						obj_t BgL_tmpz00_7162;

						if (STRINGP(BgL_filez00_3794))
							{	/* Ieee/port.scm 1720 */
								BgL_tmpz00_7162 = BgL_filez00_3794;
							}
						else
							{
								obj_t BgL_auxz00_7165;

								BgL_auxz00_7165 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(77297L),
									BGl_string2571z00zz__r4_ports_6_10_1z00,
									BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_filez00_3794);
								FAILURE(BgL_auxz00_7165, BFALSE, BFALSE);
							}
						BgL_auxz00_7161 = BSTRING_TO_STRING(BgL_tmpz00_7162);
					}
					BgL_tmpz00_7160 =
						BGl_filezd2modezd2zz__r4_ports_6_10_1z00(BgL_auxz00_7161);
				}
				return BINT(BgL_tmpz00_7160);
			}
		}

	}



/* file-type */
	BGL_EXPORTED_DEF obj_t BGl_filezd2typezd2zz__r4_ports_6_10_1z00(char
		*BgL_filez00_204)
	{
		{	/* Ieee/port.scm 1725 */
			BGL_TAIL return bgl_file_type(BgL_filez00_204);
		}

	}



/* &file-type */
	obj_t BGl_z62filezd2typezb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3795,
		obj_t BgL_filez00_3796)
	{
		{	/* Ieee/port.scm 1725 */
			{	/* Ieee/port.scm 1726 */
				char *BgL_auxz00_7173;

				{	/* Ieee/port.scm 1726 */
					obj_t BgL_tmpz00_7174;

					if (STRINGP(BgL_filez00_3796))
						{	/* Ieee/port.scm 1726 */
							BgL_tmpz00_7174 = BgL_filez00_3796;
						}
					else
						{
							obj_t BgL_auxz00_7177;

							BgL_auxz00_7177 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(77574L),
								BGl_string2572z00zz__r4_ports_6_10_1z00,
								BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_filez00_3796);
							FAILURE(BgL_auxz00_7177, BFALSE, BFALSE);
						}
					BgL_auxz00_7173 = BSTRING_TO_STRING(BgL_tmpz00_7174);
				}
				return BGl_filezd2typezd2zz__r4_ports_6_10_1z00(BgL_auxz00_7173);
			}
		}

	}



/* make-symlink */
	BGL_EXPORTED_DEF obj_t BGl_makezd2symlinkzd2zz__r4_ports_6_10_1z00(obj_t
		BgL_path1z00_205, obj_t BgL_path2z00_206)
	{
		{	/* Ieee/port.scm 1731 */
			return
				BINT(bgl_symlink(BSTRING_TO_STRING(BgL_path1z00_205),
					BSTRING_TO_STRING(BgL_path2z00_206)));
		}

	}



/* &make-symlink */
	obj_t BGl_z62makezd2symlinkzb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3797,
		obj_t BgL_path1z00_3798, obj_t BgL_path2z00_3799)
	{
		{	/* Ieee/port.scm 1731 */
			{	/* Ieee/port.scm 1732 */
				obj_t BgL_auxz00_7194;
				obj_t BgL_auxz00_7187;

				if (STRINGP(BgL_path2z00_3799))
					{	/* Ieee/port.scm 1732 */
						BgL_auxz00_7194 = BgL_path2z00_3799;
					}
				else
					{
						obj_t BgL_auxz00_7197;

						BgL_auxz00_7197 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(77861L),
							BGl_string2573z00zz__r4_ports_6_10_1z00,
							BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_path2z00_3799);
						FAILURE(BgL_auxz00_7197, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_path1z00_3798))
					{	/* Ieee/port.scm 1732 */
						BgL_auxz00_7187 = BgL_path1z00_3798;
					}
				else
					{
						obj_t BgL_auxz00_7190;

						BgL_auxz00_7190 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(77861L),
							BGl_string2573z00zz__r4_ports_6_10_1z00,
							BGl_string2404z00zz__r4_ports_6_10_1z00, BgL_path1z00_3798);
						FAILURE(BgL_auxz00_7190, BFALSE, BFALSE);
					}
				return
					BGl_makezd2symlinkzd2zz__r4_ports_6_10_1z00(BgL_auxz00_7187,
					BgL_auxz00_7194);
			}
		}

	}



/* _select */
	obj_t BGl__selectz00zz__r4_ports_6_10_1z00(obj_t BgL_env1307z00_212,
		obj_t BgL_opt1306z00_211)
	{
		{	/* Ieee/port.scm 1737 */
			{	/* Ieee/port.scm 1737 */

				{	/* Ieee/port.scm 1737 */
					obj_t BgL_exceptz00_4204;

					BgL_exceptz00_4204 = BNIL;
					{	/* Ieee/port.scm 1737 */
						obj_t BgL_readz00_4205;

						BgL_readz00_4205 = BNIL;
						{	/* Ieee/port.scm 1737 */
							obj_t BgL_timeoutz00_4206;

							BgL_timeoutz00_4206 = BINT(0L);
							{	/* Ieee/port.scm 1737 */
								obj_t BgL_writez00_4207;

								BgL_writez00_4207 = BNIL;
								{	/* Ieee/port.scm 1737 */

									{
										long BgL_iz00_4209;

										BgL_iz00_4209 = 0L;
									BgL_check1310z00_4208:
										if ((BgL_iz00_4209 == VECTOR_LENGTH(BgL_opt1306z00_211)))
											{	/* Ieee/port.scm 1737 */
												BNIL;
											}
										else
											{	/* Ieee/port.scm 1737 */
												bool_t BgL_test3222z00_7206;

												{	/* Ieee/port.scm 1737 */
													obj_t BgL_arg1646z00_4210;

													BgL_arg1646z00_4210 =
														VECTOR_REF(BgL_opt1306z00_211, BgL_iz00_4209);
													BgL_test3222z00_7206 =
														CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
														(BgL_arg1646z00_4210,
															BGl_list2574z00zz__r4_ports_6_10_1z00));
												}
												if (BgL_test3222z00_7206)
													{
														long BgL_iz00_7210;

														BgL_iz00_7210 = (BgL_iz00_4209 + 2L);
														BgL_iz00_4209 = BgL_iz00_7210;
														goto BgL_check1310z00_4208;
													}
												else
													{	/* Ieee/port.scm 1737 */
														obj_t BgL_arg1645z00_4211;

														BgL_arg1645z00_4211 =
															VECTOR_REF(BgL_opt1306z00_211, BgL_iz00_4209);
														BGl_errorz00zz__errorz00
															(BGl_symbol2583z00zz__r4_ports_6_10_1z00,
															BGl_string2585z00zz__r4_ports_6_10_1z00,
															BgL_arg1645z00_4211);
													}
											}
									}
									{	/* Ieee/port.scm 1737 */
										obj_t BgL_index1312z00_4212;

										BgL_index1312z00_4212 =
											BGl_search1309ze70ze7zz__r4_ports_6_10_1z00(VECTOR_LENGTH
											(BgL_opt1306z00_211), BgL_opt1306z00_211,
											BGl_keyword2575z00zz__r4_ports_6_10_1z00, 0L);
										{	/* Ieee/port.scm 1737 */
											bool_t BgL_test3223z00_7216;

											{	/* Ieee/port.scm 1737 */
												long BgL_n1z00_4213;

												{	/* Ieee/port.scm 1737 */
													obj_t BgL_tmpz00_7217;

													if (INTEGERP(BgL_index1312z00_4212))
														{	/* Ieee/port.scm 1737 */
															BgL_tmpz00_7217 = BgL_index1312z00_4212;
														}
													else
														{
															obj_t BgL_auxz00_7220;

															BgL_auxz00_7220 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2402z00zz__r4_ports_6_10_1z00,
																BINT(78115L),
																BGl_string2586z00zz__r4_ports_6_10_1z00,
																BGl_string2446z00zz__r4_ports_6_10_1z00,
																BgL_index1312z00_4212);
															FAILURE(BgL_auxz00_7220, BFALSE, BFALSE);
														}
													BgL_n1z00_4213 = (long) CINT(BgL_tmpz00_7217);
												}
												BgL_test3223z00_7216 = (BgL_n1z00_4213 >= 0L);
											}
											if (BgL_test3223z00_7216)
												{
													long BgL_auxz00_7226;

													{	/* Ieee/port.scm 1737 */
														obj_t BgL_tmpz00_7227;

														if (INTEGERP(BgL_index1312z00_4212))
															{	/* Ieee/port.scm 1737 */
																BgL_tmpz00_7227 = BgL_index1312z00_4212;
															}
														else
															{
																obj_t BgL_auxz00_7230;

																BgL_auxz00_7230 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string2402z00zz__r4_ports_6_10_1z00,
																	BINT(78115L),
																	BGl_string2586z00zz__r4_ports_6_10_1z00,
																	BGl_string2446z00zz__r4_ports_6_10_1z00,
																	BgL_index1312z00_4212);
																FAILURE(BgL_auxz00_7230, BFALSE, BFALSE);
															}
														BgL_auxz00_7226 = (long) CINT(BgL_tmpz00_7227);
													}
													BgL_exceptz00_4204 =
														VECTOR_REF(BgL_opt1306z00_211, BgL_auxz00_7226);
												}
											else
												{	/* Ieee/port.scm 1737 */
													BFALSE;
												}
										}
									}
									{	/* Ieee/port.scm 1737 */
										obj_t BgL_index1313z00_4214;

										BgL_index1313z00_4214 =
											BGl_search1309ze70ze7zz__r4_ports_6_10_1z00(VECTOR_LENGTH
											(BgL_opt1306z00_211), BgL_opt1306z00_211,
											BGl_keyword2577z00zz__r4_ports_6_10_1z00, 0L);
										{	/* Ieee/port.scm 1737 */
											bool_t BgL_test3226z00_7238;

											{	/* Ieee/port.scm 1737 */
												long BgL_n1z00_4215;

												{	/* Ieee/port.scm 1737 */
													obj_t BgL_tmpz00_7239;

													if (INTEGERP(BgL_index1313z00_4214))
														{	/* Ieee/port.scm 1737 */
															BgL_tmpz00_7239 = BgL_index1313z00_4214;
														}
													else
														{
															obj_t BgL_auxz00_7242;

															BgL_auxz00_7242 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2402z00zz__r4_ports_6_10_1z00,
																BINT(78115L),
																BGl_string2586z00zz__r4_ports_6_10_1z00,
																BGl_string2446z00zz__r4_ports_6_10_1z00,
																BgL_index1313z00_4214);
															FAILURE(BgL_auxz00_7242, BFALSE, BFALSE);
														}
													BgL_n1z00_4215 = (long) CINT(BgL_tmpz00_7239);
												}
												BgL_test3226z00_7238 = (BgL_n1z00_4215 >= 0L);
											}
											if (BgL_test3226z00_7238)
												{
													long BgL_auxz00_7248;

													{	/* Ieee/port.scm 1737 */
														obj_t BgL_tmpz00_7249;

														if (INTEGERP(BgL_index1313z00_4214))
															{	/* Ieee/port.scm 1737 */
																BgL_tmpz00_7249 = BgL_index1313z00_4214;
															}
														else
															{
																obj_t BgL_auxz00_7252;

																BgL_auxz00_7252 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string2402z00zz__r4_ports_6_10_1z00,
																	BINT(78115L),
																	BGl_string2586z00zz__r4_ports_6_10_1z00,
																	BGl_string2446z00zz__r4_ports_6_10_1z00,
																	BgL_index1313z00_4214);
																FAILURE(BgL_auxz00_7252, BFALSE, BFALSE);
															}
														BgL_auxz00_7248 = (long) CINT(BgL_tmpz00_7249);
													}
													BgL_readz00_4205 =
														VECTOR_REF(BgL_opt1306z00_211, BgL_auxz00_7248);
												}
											else
												{	/* Ieee/port.scm 1737 */
													BFALSE;
												}
										}
									}
									{	/* Ieee/port.scm 1737 */
										obj_t BgL_index1314z00_4216;

										BgL_index1314z00_4216 =
											BGl_search1309ze70ze7zz__r4_ports_6_10_1z00(VECTOR_LENGTH
											(BgL_opt1306z00_211), BgL_opt1306z00_211,
											BGl_keyword2579z00zz__r4_ports_6_10_1z00, 0L);
										{	/* Ieee/port.scm 1737 */
											bool_t BgL_test3229z00_7260;

											{	/* Ieee/port.scm 1737 */
												long BgL_n1z00_4217;

												{	/* Ieee/port.scm 1737 */
													obj_t BgL_tmpz00_7261;

													if (INTEGERP(BgL_index1314z00_4216))
														{	/* Ieee/port.scm 1737 */
															BgL_tmpz00_7261 = BgL_index1314z00_4216;
														}
													else
														{
															obj_t BgL_auxz00_7264;

															BgL_auxz00_7264 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2402z00zz__r4_ports_6_10_1z00,
																BINT(78115L),
																BGl_string2586z00zz__r4_ports_6_10_1z00,
																BGl_string2446z00zz__r4_ports_6_10_1z00,
																BgL_index1314z00_4216);
															FAILURE(BgL_auxz00_7264, BFALSE, BFALSE);
														}
													BgL_n1z00_4217 = (long) CINT(BgL_tmpz00_7261);
												}
												BgL_test3229z00_7260 = (BgL_n1z00_4217 >= 0L);
											}
											if (BgL_test3229z00_7260)
												{
													long BgL_auxz00_7270;

													{	/* Ieee/port.scm 1737 */
														obj_t BgL_tmpz00_7271;

														if (INTEGERP(BgL_index1314z00_4216))
															{	/* Ieee/port.scm 1737 */
																BgL_tmpz00_7271 = BgL_index1314z00_4216;
															}
														else
															{
																obj_t BgL_auxz00_7274;

																BgL_auxz00_7274 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string2402z00zz__r4_ports_6_10_1z00,
																	BINT(78115L),
																	BGl_string2586z00zz__r4_ports_6_10_1z00,
																	BGl_string2446z00zz__r4_ports_6_10_1z00,
																	BgL_index1314z00_4216);
																FAILURE(BgL_auxz00_7274, BFALSE, BFALSE);
															}
														BgL_auxz00_7270 = (long) CINT(BgL_tmpz00_7271);
													}
													BgL_timeoutz00_4206 =
														VECTOR_REF(BgL_opt1306z00_211, BgL_auxz00_7270);
												}
											else
												{	/* Ieee/port.scm 1737 */
													BFALSE;
												}
										}
									}
									{	/* Ieee/port.scm 1737 */
										obj_t BgL_index1315z00_4218;

										BgL_index1315z00_4218 =
											BGl_search1309ze70ze7zz__r4_ports_6_10_1z00(VECTOR_LENGTH
											(BgL_opt1306z00_211), BgL_opt1306z00_211,
											BGl_keyword2581z00zz__r4_ports_6_10_1z00, 0L);
										{	/* Ieee/port.scm 1737 */
											bool_t BgL_test3232z00_7282;

											{	/* Ieee/port.scm 1737 */
												long BgL_n1z00_4219;

												{	/* Ieee/port.scm 1737 */
													obj_t BgL_tmpz00_7283;

													if (INTEGERP(BgL_index1315z00_4218))
														{	/* Ieee/port.scm 1737 */
															BgL_tmpz00_7283 = BgL_index1315z00_4218;
														}
													else
														{
															obj_t BgL_auxz00_7286;

															BgL_auxz00_7286 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2402z00zz__r4_ports_6_10_1z00,
																BINT(78115L),
																BGl_string2586z00zz__r4_ports_6_10_1z00,
																BGl_string2446z00zz__r4_ports_6_10_1z00,
																BgL_index1315z00_4218);
															FAILURE(BgL_auxz00_7286, BFALSE, BFALSE);
														}
													BgL_n1z00_4219 = (long) CINT(BgL_tmpz00_7283);
												}
												BgL_test3232z00_7282 = (BgL_n1z00_4219 >= 0L);
											}
											if (BgL_test3232z00_7282)
												{
													long BgL_auxz00_7292;

													{	/* Ieee/port.scm 1737 */
														obj_t BgL_tmpz00_7293;

														if (INTEGERP(BgL_index1315z00_4218))
															{	/* Ieee/port.scm 1737 */
																BgL_tmpz00_7293 = BgL_index1315z00_4218;
															}
														else
															{
																obj_t BgL_auxz00_7296;

																BgL_auxz00_7296 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string2402z00zz__r4_ports_6_10_1z00,
																	BINT(78115L),
																	BGl_string2586z00zz__r4_ports_6_10_1z00,
																	BGl_string2446z00zz__r4_ports_6_10_1z00,
																	BgL_index1315z00_4218);
																FAILURE(BgL_auxz00_7296, BFALSE, BFALSE);
															}
														BgL_auxz00_7292 = (long) CINT(BgL_tmpz00_7293);
													}
													BgL_writez00_4207 =
														VECTOR_REF(BgL_opt1306z00_211, BgL_auxz00_7292);
												}
											else
												{	/* Ieee/port.scm 1737 */
													BFALSE;
												}
										}
									}
									{	/* Ieee/port.scm 1737 */
										obj_t BgL_exceptz00_4220;

										BgL_exceptz00_4220 = BgL_exceptz00_4204;
										{	/* Ieee/port.scm 1737 */
											obj_t BgL_readz00_4221;

											BgL_readz00_4221 = BgL_readz00_4205;
											{	/* Ieee/port.scm 1737 */
												obj_t BgL_timeoutz00_4222;

												BgL_timeoutz00_4222 = BgL_timeoutz00_4206;
												{	/* Ieee/port.scm 1737 */
													obj_t BgL_writez00_4223;

													BgL_writez00_4223 = BgL_writez00_4207;
													{	/* Ieee/port.scm 1738 */
														obj_t BgL_auxz00_7325;
														obj_t BgL_auxz00_7318;
														obj_t BgL_auxz00_7311;
														long BgL_tmpz00_7302;

														if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_exceptz00_4220))
															{	/* Ieee/port.scm 1738 */
																BgL_auxz00_7325 = BgL_exceptz00_4220;
															}
														else
															{
																obj_t BgL_auxz00_7328;

																BgL_auxz00_7328 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string2402z00zz__r4_ports_6_10_1z00,
																	BINT(78224L),
																	BGl_string2586z00zz__r4_ports_6_10_1z00,
																	BGl_string2587z00zz__r4_ports_6_10_1z00,
																	BgL_exceptz00_4220);
																FAILURE(BgL_auxz00_7328, BFALSE, BFALSE);
															}
														if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_writez00_4223))
															{	/* Ieee/port.scm 1738 */
																BgL_auxz00_7318 = BgL_writez00_4223;
															}
														else
															{
																obj_t BgL_auxz00_7321;

																BgL_auxz00_7321 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string2402z00zz__r4_ports_6_10_1z00,
																	BINT(78218L),
																	BGl_string2586z00zz__r4_ports_6_10_1z00,
																	BGl_string2587z00zz__r4_ports_6_10_1z00,
																	BgL_writez00_4223);
																FAILURE(BgL_auxz00_7321, BFALSE, BFALSE);
															}
														if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_readz00_4221))
															{	/* Ieee/port.scm 1738 */
																BgL_auxz00_7311 = BgL_readz00_4221;
															}
														else
															{
																obj_t BgL_auxz00_7314;

																BgL_auxz00_7314 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string2402z00zz__r4_ports_6_10_1z00,
																	BINT(78213L),
																	BGl_string2586z00zz__r4_ports_6_10_1z00,
																	BGl_string2587z00zz__r4_ports_6_10_1z00,
																	BgL_readz00_4221);
																FAILURE(BgL_auxz00_7314, BFALSE, BFALSE);
															}
														{	/* Ieee/port.scm 1738 */
															obj_t BgL_tmpz00_7303;

															if (INTEGERP(BgL_timeoutz00_4222))
																{	/* Ieee/port.scm 1738 */
																	BgL_tmpz00_7303 = BgL_timeoutz00_4222;
																}
															else
																{
																	obj_t BgL_auxz00_7306;

																	BgL_auxz00_7306 =
																		BGl_typezd2errorzd2zz__errorz00
																		(BGl_string2402z00zz__r4_ports_6_10_1z00,
																		BINT(78205L),
																		BGl_string2586z00zz__r4_ports_6_10_1z00,
																		BGl_string2446z00zz__r4_ports_6_10_1z00,
																		BgL_timeoutz00_4222);
																	FAILURE(BgL_auxz00_7306, BFALSE, BFALSE);
																}
															BgL_tmpz00_7302 = (long) CINT(BgL_tmpz00_7303);
														}
														return
															bgl_select(BgL_tmpz00_7302, BgL_auxz00_7311,
															BgL_auxz00_7318, BgL_auxz00_7325);
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* search1309~0 */
	obj_t BGl_search1309ze70ze7zz__r4_ports_6_10_1z00(long BgL_l1308z00_3802,
		obj_t BgL_opt1306z00_3801, obj_t BgL_k1z00_2973, long BgL_iz00_2974)
	{
		{	/* Ieee/port.scm 1737 */
		BGl_search1309ze70ze7zz__r4_ports_6_10_1z00:
			if ((BgL_iz00_2974 == BgL_l1308z00_3802))
				{	/* Ieee/port.scm 1737 */
					return BINT(-1L);
				}
			else
				{	/* Ieee/port.scm 1737 */
					if ((BgL_iz00_2974 == (BgL_l1308z00_3802 - 1L)))
						{	/* Ieee/port.scm 1737 */
							return
								BGl_errorz00zz__errorz00
								(BGl_symbol2583z00zz__r4_ports_6_10_1z00,
								BGl_string2588z00zz__r4_ports_6_10_1z00,
								BINT(VECTOR_LENGTH(BgL_opt1306z00_3801)));
						}
					else
						{	/* Ieee/port.scm 1737 */
							obj_t BgL_vz00_2979;

							BgL_vz00_2979 = VECTOR_REF(BgL_opt1306z00_3801, BgL_iz00_2974);
							if ((BgL_vz00_2979 == BgL_k1z00_2973))
								{	/* Ieee/port.scm 1737 */
									return BINT((BgL_iz00_2974 + 1L));
								}
							else
								{
									long BgL_iz00_7347;

									BgL_iz00_7347 = (BgL_iz00_2974 + 2L);
									BgL_iz00_2974 = BgL_iz00_7347;
									goto BGl_search1309ze70ze7zz__r4_ports_6_10_1z00;
								}
						}
				}
		}

	}



/* select */
	BGL_EXPORTED_DEF obj_t BGl_selectz00zz__r4_ports_6_10_1z00(obj_t
		BgL_exceptz00_207, obj_t BgL_readz00_208, obj_t BgL_timeoutz00_209,
		obj_t BgL_writez00_210)
	{
		{	/* Ieee/port.scm 1737 */
			return
				bgl_select(
				(long) CINT(BgL_timeoutz00_209), BgL_readz00_208, BgL_writez00_210,
				BgL_exceptz00_207);
		}

	}



/* _open-pipes */
	obj_t BGl__openzd2pipeszd2zz__r4_ports_6_10_1z00(obj_t BgL_env1317z00_215,
		obj_t BgL_opt1316z00_214)
	{
		{	/* Ieee/port.scm 1743 */
			{	/* Ieee/port.scm 1743 */

				switch (VECTOR_LENGTH(BgL_opt1316z00_214))
					{
					case 0L:

						{	/* Ieee/port.scm 1743 */

							return bgl_open_pipes(BFALSE);
						}
						break;
					case 1L:

						{	/* Ieee/port.scm 1743 */
							obj_t BgL_namez00_4224;

							BgL_namez00_4224 = VECTOR_REF(BgL_opt1316z00_214, 0L);
							{	/* Ieee/port.scm 1743 */

								return bgl_open_pipes(BgL_namez00_4224);
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* open-pipes */
	BGL_EXPORTED_DEF obj_t BGl_openzd2pipeszd2zz__r4_ports_6_10_1z00(obj_t
		BgL_namez00_213)
	{
		{	/* Ieee/port.scm 1743 */
			BGL_TAIL return bgl_open_pipes(BgL_namez00_213);
		}

	}



/* _lockf */
	obj_t BGl__lockfz00zz__r4_ports_6_10_1z00(obj_t BgL_env1321z00_220,
		obj_t BgL_opt1320z00_219)
	{
		{	/* Ieee/port.scm 1749 */
			{	/* Ieee/port.scm 1749 */
				obj_t BgL_g1322z00_2030;
				obj_t BgL_g1323z00_2031;

				BgL_g1322z00_2030 = VECTOR_REF(BgL_opt1320z00_219, 0L);
				BgL_g1323z00_2031 = VECTOR_REF(BgL_opt1320z00_219, 1L);
				switch (VECTOR_LENGTH(BgL_opt1320z00_219))
					{
					case 2L:

						{	/* Ieee/port.scm 1749 */

							{	/* Ieee/port.scm 1749 */
								bool_t BgL_tmpz00_7359;

								{	/* Ieee/port.scm 1749 */
									obj_t BgL_auxz00_7367;
									obj_t BgL_auxz00_7360;

									if (SYMBOLP(BgL_g1323z00_2031))
										{	/* Ieee/port.scm 1749 */
											BgL_auxz00_7367 = BgL_g1323z00_2031;
										}
									else
										{
											obj_t BgL_auxz00_7370;

											BgL_auxz00_7370 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(78746L),
												BGl_string2589z00zz__r4_ports_6_10_1z00,
												BGl_string2590z00zz__r4_ports_6_10_1z00,
												BgL_g1323z00_2031);
											FAILURE(BgL_auxz00_7370, BFALSE, BFALSE);
										}
									if (OUTPUT_PORTP(BgL_g1322z00_2030))
										{	/* Ieee/port.scm 1749 */
											BgL_auxz00_7360 = BgL_g1322z00_2030;
										}
									else
										{
											obj_t BgL_auxz00_7363;

											BgL_auxz00_7363 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2402z00zz__r4_ports_6_10_1z00, BINT(78746L),
												BGl_string2589z00zz__r4_ports_6_10_1z00,
												BGl_string2429z00zz__r4_ports_6_10_1z00,
												BgL_g1322z00_2030);
											FAILURE(BgL_auxz00_7363, BFALSE, BFALSE);
										}
									BgL_tmpz00_7359 =
										BGl_lockfz00zz__r4_ports_6_10_1z00(BgL_auxz00_7360,
										BgL_auxz00_7367, BINT(0L));
								}
								return BBOOL(BgL_tmpz00_7359);
							}
						}
						break;
					case 3L:

						{	/* Ieee/port.scm 1749 */
							obj_t BgL_lenz00_2035;

							BgL_lenz00_2035 = VECTOR_REF(BgL_opt1320z00_219, 2L);
							{	/* Ieee/port.scm 1749 */

								{	/* Ieee/port.scm 1749 */
									bool_t BgL_tmpz00_7378;

									{	/* Ieee/port.scm 1749 */
										obj_t BgL_auxz00_7386;
										obj_t BgL_auxz00_7379;

										if (SYMBOLP(BgL_g1323z00_2031))
											{	/* Ieee/port.scm 1749 */
												BgL_auxz00_7386 = BgL_g1323z00_2031;
											}
										else
											{
												obj_t BgL_auxz00_7389;

												BgL_auxz00_7389 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2402z00zz__r4_ports_6_10_1z00,
													BINT(78746L), BGl_string2589z00zz__r4_ports_6_10_1z00,
													BGl_string2590z00zz__r4_ports_6_10_1z00,
													BgL_g1323z00_2031);
												FAILURE(BgL_auxz00_7389, BFALSE, BFALSE);
											}
										if (OUTPUT_PORTP(BgL_g1322z00_2030))
											{	/* Ieee/port.scm 1749 */
												BgL_auxz00_7379 = BgL_g1322z00_2030;
											}
										else
											{
												obj_t BgL_auxz00_7382;

												BgL_auxz00_7382 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2402z00zz__r4_ports_6_10_1z00,
													BINT(78746L), BGl_string2589z00zz__r4_ports_6_10_1z00,
													BGl_string2429z00zz__r4_ports_6_10_1z00,
													BgL_g1322z00_2030);
												FAILURE(BgL_auxz00_7382, BFALSE, BFALSE);
											}
										BgL_tmpz00_7378 =
											BGl_lockfz00zz__r4_ports_6_10_1z00(BgL_auxz00_7379,
											BgL_auxz00_7386, BgL_lenz00_2035);
									}
									return BBOOL(BgL_tmpz00_7378);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* lockf */
	BGL_EXPORTED_DEF bool_t BGl_lockfz00zz__r4_ports_6_10_1z00(obj_t
		BgL_portz00_216, obj_t BgL_cmdz00_217, obj_t BgL_lenz00_218)
	{
		{	/* Ieee/port.scm 1749 */
			if ((BgL_cmdz00_217 == BGl_symbol2591z00zz__r4_ports_6_10_1z00))
				{	/* Ieee/port.scm 1752 */
					return
						bgl_lockf(BgL_portz00_216, F_LOCK, (long) CINT(BgL_lenz00_218));
				}
			else
				{	/* Ieee/port.scm 1752 */
					if ((BgL_cmdz00_217 == BGl_symbol2593z00zz__r4_ports_6_10_1z00))
						{	/* Ieee/port.scm 1752 */
							return
								bgl_lockf(BgL_portz00_216, F_TLOCK,
								(long) CINT(BgL_lenz00_218));
						}
					else
						{	/* Ieee/port.scm 1752 */
							if ((BgL_cmdz00_217 == BGl_symbol2595z00zz__r4_ports_6_10_1z00))
								{	/* Ieee/port.scm 1752 */
									return
										bgl_lockf(BgL_portz00_216, F_ULOCK,
										(long) CINT(BgL_lenz00_218));
								}
							else
								{	/* Ieee/port.scm 1752 */
									if (
										(BgL_cmdz00_217 == BGl_symbol2597z00zz__r4_ports_6_10_1z00))
										{	/* Ieee/port.scm 1752 */
											return
												bgl_lockf(BgL_portz00_216, F_TEST,
												(long) CINT(BgL_lenz00_218));
										}
									else
										{	/* Ieee/port.scm 1752 */
											return
												CBOOL(BGl_errorz00zz__errorz00
												(BGl_string2599z00zz__r4_ports_6_10_1z00,
													BGl_string2600z00zz__r4_ports_6_10_1z00,
													BgL_cmdz00_217));
										}
								}
						}
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__r4_ports_6_10_1z00(void)
	{
		{	/* Ieee/port.scm 18 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__r4_ports_6_10_1z00(void)
	{
		{	/* Ieee/port.scm 18 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__r4_ports_6_10_1z00(void)
	{
		{	/* Ieee/port.scm 18 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__r4_ports_6_10_1z00(void)
	{
		{	/* Ieee/port.scm 18 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string2601z00zz__r4_ports_6_10_1z00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string2601z00zz__r4_ports_6_10_1z00));
			BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(323064722L,
				BSTRING_TO_STRING(BGl_string2601z00zz__r4_ports_6_10_1z00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string2601z00zz__r4_ports_6_10_1z00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string2601z00zz__r4_ports_6_10_1z00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string2601z00zz__r4_ports_6_10_1z00));
			BGl_modulezd2initializa7ationz75zz__gunza7ipza7(224363699L,
				BSTRING_TO_STRING(BGl_string2601z00zz__r4_ports_6_10_1z00));
			BGl_modulezd2initializa7ationz75zz__urlz00(337061926L,
				BSTRING_TO_STRING(BGl_string2601z00zz__r4_ports_6_10_1z00));
			BGl_modulezd2initializa7ationz75zz__httpz00(354980671L,
				BSTRING_TO_STRING(BGl_string2601z00zz__r4_ports_6_10_1z00));
			return
				BGl_modulezd2initializa7ationz75zz__ftpz00(102328204L,
				BSTRING_TO_STRING(BGl_string2601z00zz__r4_ports_6_10_1z00));
		}

	}

#ifdef __cplusplus
}
#endif
