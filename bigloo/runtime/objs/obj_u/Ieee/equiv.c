/*===========================================================================*/
/*   (Ieee/equiv.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Ieee/equiv.scm -indent -o objs/obj_u/Ieee/equiv.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___R4_EQUIVALENCE_6_2_TYPE_DEFINITIONS
#define BGL___R4_EQUIVALENCE_6_2_TYPE_DEFINITIONS

/* object type definitions */

#endif													// BGL___R4_EQUIVALENCE_6_2_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zz__r4_equivalence_6_2z00 =
		BUNSPEC;
	BGL_EXPORTED_DECL bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t,
		obj_t);
	static obj_t BGl_z62eqzf3z91zz__r4_equivalence_6_2z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern bool_t BGl_numberzf3zf3zz__r4_numbers_6_5z00(obj_t);
	extern bool_t ucs2_strcmp(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zz__r4_equivalence_6_2z00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zz__r4_equivalence_6_2z00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__r4_equivalence_6_2z00(void);
	static obj_t BGl_objectzd2initzd2zz__r4_equivalence_6_2z00(void);
	extern obj_t BGl_homogeneouszd2vectorzd2infoz00zz__srfi4z00(obj_t);
	extern bool_t BGl_2zd3zd3zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern bool_t BGl_objectzd2equalzf3z21zz__objectz00(BgL_objectz00_bglt,
		BgL_objectz00_bglt);
	extern obj_t bgl_weakptr_data(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_eqzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zz__r4_equivalence_6_2z00(void);
	extern long bgl_date_to_seconds(obj_t);
	static obj_t BGl_z62equalzf3z91zz__r4_equivalence_6_2z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62eqvzf3z91zz__r4_equivalence_6_2z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_eqvzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string1640z00zz__r4_equivalence_6_2z00,
		BgL_bgl_string1640za700za7za7_1641za7, "__r4_equivalence_6_2", 20);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_eqvzf3zd2envz21zz__r4_equivalence_6_2z00,
		BgL_bgl_za762eqvza7f3za791za7za7__1642za7,
		BGl_z62eqvzf3z91zz__r4_equivalence_6_2z00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_eqzf3zd2envz21zz__r4_equivalence_6_2z00,
		BgL_bgl_za762eqza7f3za791za7za7__r1643za7,
		BGl_z62eqzf3z91zz__r4_equivalence_6_2z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_equalzf3zd2envz21zz__r4_equivalence_6_2z00,
		BgL_bgl_za762equalza7f3za791za7za71644za7,
		BGl_z62equalzf3z91zz__r4_equivalence_6_2z00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__r4_equivalence_6_2z00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long
		BgL_checksumz00_1950, char *BgL_fromz00_1951)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__r4_equivalence_6_2z00))
				{
					BGl_requirezd2initializa7ationz75zz__r4_equivalence_6_2z00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__r4_equivalence_6_2z00();
					BGl_importedzd2moduleszd2initz00zz__r4_equivalence_6_2z00();
					return BGl_methodzd2initzd2zz__r4_equivalence_6_2z00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__r4_equivalence_6_2z00(void)
	{
		{	/* Ieee/equiv.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* eq? */
	BGL_EXPORTED_DEF bool_t BGl_eqzf3zf3zz__r4_equivalence_6_2z00(obj_t
		BgL_obj1z00_3, obj_t BgL_obj2z00_4)
	{
		{	/* Ieee/equiv.scm 67 */
			return (BgL_obj1z00_3 == BgL_obj2z00_4);
		}

	}



/* &eq? */
	obj_t BGl_z62eqzf3z91zz__r4_equivalence_6_2z00(obj_t BgL_envz00_1941,
		obj_t BgL_obj1z00_1942, obj_t BgL_obj2z00_1943)
	{
		{	/* Ieee/equiv.scm 67 */
			return
				BBOOL(BGl_eqzf3zf3zz__r4_equivalence_6_2z00(BgL_obj1z00_1942,
					BgL_obj2z00_1943));
		}

	}



/* eqv? */
	BGL_EXPORTED_DEF bool_t BGl_eqvzf3zf3zz__r4_equivalence_6_2z00(obj_t
		BgL_obj1z00_5, obj_t BgL_obj2z00_6)
	{
		{	/* Ieee/equiv.scm 73 */
		BGl_eqvzf3zf3zz__r4_equivalence_6_2z00:
			if ((BgL_obj1z00_5 == BgL_obj2z00_6))
				{	/* Ieee/equiv.scm 75 */
					return ((bool_t) 1);
				}
			else
				{	/* Ieee/equiv.scm 77 */
					bool_t BgL_test1647z00_1964;

					{	/* Ieee/equiv.scm 77 */
						bool_t BgL__ortest_1076z00_1706;

						BgL__ortest_1076z00_1706 = INTEGERP(BgL_obj1z00_5);
						if (BgL__ortest_1076z00_1706)
							{	/* Ieee/equiv.scm 77 */
								BgL_test1647z00_1964 = BgL__ortest_1076z00_1706;
							}
						else
							{	/* Ieee/equiv.scm 77 */
								bool_t BgL__ortest_1077z00_1707;

								BgL__ortest_1077z00_1707 = ELONGP(BgL_obj1z00_5);
								if (BgL__ortest_1077z00_1707)
									{	/* Ieee/equiv.scm 77 */
										BgL_test1647z00_1964 = BgL__ortest_1077z00_1707;
									}
								else
									{	/* Ieee/equiv.scm 77 */
										bool_t BgL__ortest_1078z00_1708;

										BgL__ortest_1078z00_1708 = LLONGP(BgL_obj1z00_5);
										if (BgL__ortest_1078z00_1708)
											{	/* Ieee/equiv.scm 77 */
												BgL_test1647z00_1964 = BgL__ortest_1078z00_1708;
											}
										else
											{	/* Ieee/equiv.scm 77 */
												bool_t BgL__ortest_1079z00_1709;

												BgL__ortest_1079z00_1709 = BGL_INT8P(BgL_obj1z00_5);
												if (BgL__ortest_1079z00_1709)
													{	/* Ieee/equiv.scm 77 */
														BgL_test1647z00_1964 = BgL__ortest_1079z00_1709;
													}
												else
													{	/* Ieee/equiv.scm 77 */
														bool_t BgL__ortest_1080z00_1710;

														BgL__ortest_1080z00_1710 =
															BGL_UINT8P(BgL_obj1z00_5);
														if (BgL__ortest_1080z00_1710)
															{	/* Ieee/equiv.scm 77 */
																BgL_test1647z00_1964 = BgL__ortest_1080z00_1710;
															}
														else
															{	/* Ieee/equiv.scm 77 */
																bool_t BgL__ortest_1081z00_1711;

																BgL__ortest_1081z00_1711 =
																	BGL_INT16P(BgL_obj1z00_5);
																if (BgL__ortest_1081z00_1711)
																	{	/* Ieee/equiv.scm 77 */
																		BgL_test1647z00_1964 =
																			BgL__ortest_1081z00_1711;
																	}
																else
																	{	/* Ieee/equiv.scm 77 */
																		bool_t BgL__ortest_1082z00_1712;

																		BgL__ortest_1082z00_1712 =
																			BGL_UINT16P(BgL_obj1z00_5);
																		if (BgL__ortest_1082z00_1712)
																			{	/* Ieee/equiv.scm 77 */
																				BgL_test1647z00_1964 =
																					BgL__ortest_1082z00_1712;
																			}
																		else
																			{	/* Ieee/equiv.scm 77 */
																				bool_t BgL__ortest_1083z00_1713;

																				BgL__ortest_1083z00_1713 =
																					BGL_INT32P(BgL_obj1z00_5);
																				if (BgL__ortest_1083z00_1713)
																					{	/* Ieee/equiv.scm 77 */
																						BgL_test1647z00_1964 =
																							BgL__ortest_1083z00_1713;
																					}
																				else
																					{	/* Ieee/equiv.scm 77 */
																						bool_t BgL__ortest_1084z00_1714;

																						BgL__ortest_1084z00_1714 =
																							BGL_UINT32P(BgL_obj1z00_5);
																						if (BgL__ortest_1084z00_1714)
																							{	/* Ieee/equiv.scm 77 */
																								BgL_test1647z00_1964 =
																									BgL__ortest_1084z00_1714;
																							}
																						else
																							{	/* Ieee/equiv.scm 77 */
																								bool_t BgL__ortest_1085z00_1715;

																								BgL__ortest_1085z00_1715 =
																									BGL_INT64P(BgL_obj1z00_5);
																								if (BgL__ortest_1085z00_1715)
																									{	/* Ieee/equiv.scm 77 */
																										BgL_test1647z00_1964 =
																											BgL__ortest_1085z00_1715;
																									}
																								else
																									{	/* Ieee/equiv.scm 77 */
																										bool_t
																											BgL__ortest_1086z00_1716;
																										BgL__ortest_1086z00_1716 =
																											BGL_UINT64P
																											(BgL_obj1z00_5);
																										if (BgL__ortest_1086z00_1716)
																											{	/* Ieee/equiv.scm 77 */
																												BgL_test1647z00_1964 =
																													BgL__ortest_1086z00_1716;
																											}
																										else
																											{	/* Ieee/equiv.scm 77 */
																												BgL_test1647z00_1964 =
																													BIGNUMP
																													(BgL_obj1z00_5);
																											}
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
									}
							}
					}
					if (BgL_test1647z00_1964)
						{	/* Ieee/equiv.scm 78 */
							bool_t BgL_test1659z00_1988;

							{	/* Ieee/equiv.scm 78 */
								bool_t BgL__ortest_1076z00_1718;

								BgL__ortest_1076z00_1718 = INTEGERP(BgL_obj2z00_6);
								if (BgL__ortest_1076z00_1718)
									{	/* Ieee/equiv.scm 78 */
										BgL_test1659z00_1988 = BgL__ortest_1076z00_1718;
									}
								else
									{	/* Ieee/equiv.scm 78 */
										bool_t BgL__ortest_1077z00_1719;

										BgL__ortest_1077z00_1719 = ELONGP(BgL_obj2z00_6);
										if (BgL__ortest_1077z00_1719)
											{	/* Ieee/equiv.scm 78 */
												BgL_test1659z00_1988 = BgL__ortest_1077z00_1719;
											}
										else
											{	/* Ieee/equiv.scm 78 */
												bool_t BgL__ortest_1078z00_1720;

												BgL__ortest_1078z00_1720 = LLONGP(BgL_obj2z00_6);
												if (BgL__ortest_1078z00_1720)
													{	/* Ieee/equiv.scm 78 */
														BgL_test1659z00_1988 = BgL__ortest_1078z00_1720;
													}
												else
													{	/* Ieee/equiv.scm 78 */
														bool_t BgL__ortest_1079z00_1721;

														BgL__ortest_1079z00_1721 = BGL_INT8P(BgL_obj2z00_6);
														if (BgL__ortest_1079z00_1721)
															{	/* Ieee/equiv.scm 78 */
																BgL_test1659z00_1988 = BgL__ortest_1079z00_1721;
															}
														else
															{	/* Ieee/equiv.scm 78 */
																bool_t BgL__ortest_1080z00_1722;

																BgL__ortest_1080z00_1722 =
																	BGL_UINT8P(BgL_obj2z00_6);
																if (BgL__ortest_1080z00_1722)
																	{	/* Ieee/equiv.scm 78 */
																		BgL_test1659z00_1988 =
																			BgL__ortest_1080z00_1722;
																	}
																else
																	{	/* Ieee/equiv.scm 78 */
																		bool_t BgL__ortest_1081z00_1723;

																		BgL__ortest_1081z00_1723 =
																			BGL_INT16P(BgL_obj2z00_6);
																		if (BgL__ortest_1081z00_1723)
																			{	/* Ieee/equiv.scm 78 */
																				BgL_test1659z00_1988 =
																					BgL__ortest_1081z00_1723;
																			}
																		else
																			{	/* Ieee/equiv.scm 78 */
																				bool_t BgL__ortest_1082z00_1724;

																				BgL__ortest_1082z00_1724 =
																					BGL_UINT16P(BgL_obj2z00_6);
																				if (BgL__ortest_1082z00_1724)
																					{	/* Ieee/equiv.scm 78 */
																						BgL_test1659z00_1988 =
																							BgL__ortest_1082z00_1724;
																					}
																				else
																					{	/* Ieee/equiv.scm 78 */
																						bool_t BgL__ortest_1083z00_1725;

																						BgL__ortest_1083z00_1725 =
																							BGL_INT32P(BgL_obj2z00_6);
																						if (BgL__ortest_1083z00_1725)
																							{	/* Ieee/equiv.scm 78 */
																								BgL_test1659z00_1988 =
																									BgL__ortest_1083z00_1725;
																							}
																						else
																							{	/* Ieee/equiv.scm 78 */
																								bool_t BgL__ortest_1084z00_1726;

																								BgL__ortest_1084z00_1726 =
																									BGL_UINT32P(BgL_obj2z00_6);
																								if (BgL__ortest_1084z00_1726)
																									{	/* Ieee/equiv.scm 78 */
																										BgL_test1659z00_1988 =
																											BgL__ortest_1084z00_1726;
																									}
																								else
																									{	/* Ieee/equiv.scm 78 */
																										bool_t
																											BgL__ortest_1085z00_1727;
																										BgL__ortest_1085z00_1727 =
																											BGL_INT64P(BgL_obj2z00_6);
																										if (BgL__ortest_1085z00_1727)
																											{	/* Ieee/equiv.scm 78 */
																												BgL_test1659z00_1988 =
																													BgL__ortest_1085z00_1727;
																											}
																										else
																											{	/* Ieee/equiv.scm 78 */
																												bool_t
																													BgL__ortest_1086z00_1728;
																												BgL__ortest_1086z00_1728
																													=
																													BGL_UINT64P
																													(BgL_obj2z00_6);
																												if (BgL__ortest_1086z00_1728)
																													{	/* Ieee/equiv.scm 78 */
																														BgL_test1659z00_1988
																															=
																															BgL__ortest_1086z00_1728;
																													}
																												else
																													{	/* Ieee/equiv.scm 78 */
																														BgL_test1659z00_1988
																															=
																															BIGNUMP
																															(BgL_obj2z00_6);
																													}
																											}
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
									}
							}
							if (BgL_test1659z00_1988)
								{	/* Ieee/equiv.scm 78 */
									bool_t BgL_test1671z00_2012;

									if (INTEGERP(BgL_obj1z00_5))
										{	/* Ieee/equiv.scm 78 */
											BgL_test1671z00_2012 = INTEGERP(BgL_obj2z00_6);
										}
									else
										{	/* Ieee/equiv.scm 78 */
											BgL_test1671z00_2012 = ((bool_t) 0);
										}
									if (BgL_test1671z00_2012)
										{	/* Ieee/equiv.scm 78 */
											return
												(
												(long) CINT(BgL_obj1z00_5) ==
												(long) CINT(BgL_obj2z00_6));
										}
									else
										{	/* Ieee/equiv.scm 78 */
											BGL_TAIL return
												BGl_2zd3zd3zz__r4_numbers_6_5z00(BgL_obj1z00_5,
												BgL_obj2z00_6);
										}
								}
							else
								{	/* Ieee/equiv.scm 78 */
									return ((bool_t) 0);
								}
						}
					else
						{	/* Ieee/equiv.scm 77 */
							if (REALP(BgL_obj1z00_5))
								{	/* Ieee/equiv.scm 79 */
									if (REALP(BgL_obj2z00_6))
										{	/* Ieee/equiv.scm 80 */
											BGL_TAIL return
												BGl_2zd3zd3zz__r4_numbers_6_5z00(BgL_obj1z00_5,
												BgL_obj2z00_6);
										}
									else
										{	/* Ieee/equiv.scm 80 */
											return ((bool_t) 0);
										}
								}
							else
								{	/* Ieee/equiv.scm 79 */
									if (SYMBOLP(BgL_obj1z00_5))
										{	/* Ieee/equiv.scm 81 */
											if (SYMBOLP(BgL_obj2z00_6))
												{	/* Ieee/equiv.scm 83 */
													obj_t BgL_arg1148z00_1206;
													obj_t BgL_arg1149z00_1207;

													BgL_arg1148z00_1206 = SYMBOL_TO_STRING(BgL_obj1z00_5);
													BgL_arg1149z00_1207 = SYMBOL_TO_STRING(BgL_obj2z00_6);
													{	/* Ieee/equiv.scm 83 */
														long BgL_l1z00_1738;

														BgL_l1z00_1738 = STRING_LENGTH(BgL_arg1148z00_1206);
														if (
															(BgL_l1z00_1738 ==
																STRING_LENGTH(BgL_arg1149z00_1207)))
															{	/* Ieee/equiv.scm 83 */
																int BgL_arg1236z00_1741;

																{	/* Ieee/equiv.scm 83 */
																	char *BgL_auxz00_2037;
																	char *BgL_tmpz00_2035;

																	BgL_auxz00_2037 =
																		BSTRING_TO_STRING(BgL_arg1149z00_1207);
																	BgL_tmpz00_2035 =
																		BSTRING_TO_STRING(BgL_arg1148z00_1206);
																	BgL_arg1236z00_1741 =
																		memcmp(BgL_tmpz00_2035, BgL_auxz00_2037,
																		BgL_l1z00_1738);
																}
																return ((long) (BgL_arg1236z00_1741) == 0L);
															}
														else
															{	/* Ieee/equiv.scm 83 */
																return ((bool_t) 0);
															}
													}
												}
											else
												{	/* Ieee/equiv.scm 82 */
													return ((bool_t) 0);
												}
										}
									else
										{	/* Ieee/equiv.scm 81 */
											if (FOREIGNP(BgL_obj1z00_5))
												{	/* Ieee/equiv.scm 84 */
													if (FOREIGNP(BgL_obj2z00_6))
														{	/* Ieee/equiv.scm 85 */
															return FOREIGN_EQP(BgL_obj1z00_5, BgL_obj2z00_6);
														}
													else
														{	/* Ieee/equiv.scm 85 */
															return ((bool_t) 0);
														}
												}
											else
												{	/* Ieee/equiv.scm 84 */
													if (BGL_WEAKPTRP(BgL_obj1z00_5))
														{	/* Ieee/equiv.scm 86 */
															if (BGL_WEAKPTRP(BgL_obj2z00_6))
																{
																	obj_t BgL_obj2z00_2053;
																	obj_t BgL_obj1z00_2051;

																	BgL_obj1z00_2051 =
																		bgl_weakptr_data(BgL_obj1z00_5);
																	BgL_obj2z00_2053 =
																		bgl_weakptr_data(BgL_obj2z00_6);
																	BgL_obj2z00_6 = BgL_obj2z00_2053;
																	BgL_obj1z00_5 = BgL_obj1z00_2051;
																	goto BGl_eqvzf3zf3zz__r4_equivalence_6_2z00;
																}
															else
																{	/* Ieee/equiv.scm 87 */
																	return ((bool_t) 0);
																}
														}
													else
														{	/* Ieee/equiv.scm 86 */
															return ((bool_t) 0);
														}
												}
										}
								}
						}
				}
		}

	}



/* &eqv? */
	obj_t BGl_z62eqvzf3z91zz__r4_equivalence_6_2z00(obj_t BgL_envz00_1944,
		obj_t BgL_obj1z00_1945, obj_t BgL_obj2z00_1946)
	{
		{	/* Ieee/equiv.scm 73 */
			return
				BBOOL(BGl_eqvzf3zf3zz__r4_equivalence_6_2z00(BgL_obj1z00_1945,
					BgL_obj2z00_1946));
		}

	}



/* equal? */
	BGL_EXPORTED_DEF bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t
		BgL_obj1z00_7, obj_t BgL_obj2z00_8)
	{
		{	/* Ieee/equiv.scm 94 */
		BGl_equalzf3zf3zz__r4_equivalence_6_2z00:
			if ((BgL_obj1z00_7 == BgL_obj2z00_8))
				{	/* Ieee/equiv.scm 96 */
					return ((bool_t) 1);
				}
			else
				{	/* Ieee/equiv.scm 96 */
					if (STRINGP(BgL_obj1z00_7))
						{	/* Ieee/equiv.scm 98 */
							if (STRINGP(BgL_obj2z00_8))
								{	/* Ieee/equiv.scm 99 */
									long BgL_l1z00_1751;

									BgL_l1z00_1751 = STRING_LENGTH(BgL_obj1z00_7);
									if ((BgL_l1z00_1751 == STRING_LENGTH(BgL_obj2z00_8)))
										{	/* Ieee/equiv.scm 99 */
											int BgL_arg1236z00_1754;

											{	/* Ieee/equiv.scm 99 */
												char *BgL_auxz00_2069;
												char *BgL_tmpz00_2067;

												BgL_auxz00_2069 = BSTRING_TO_STRING(BgL_obj2z00_8);
												BgL_tmpz00_2067 = BSTRING_TO_STRING(BgL_obj1z00_7);
												BgL_arg1236z00_1754 =
													memcmp(BgL_tmpz00_2067, BgL_auxz00_2069,
													BgL_l1z00_1751);
											}
											return ((long) (BgL_arg1236z00_1754) == 0L);
										}
									else
										{	/* Ieee/equiv.scm 99 */
											return ((bool_t) 0);
										}
								}
							else
								{	/* Ieee/equiv.scm 99 */
									return ((bool_t) 0);
								}
						}
					else
						{	/* Ieee/equiv.scm 98 */
							if (SYMBOLP(BgL_obj1z00_7))
								{	/* Ieee/equiv.scm 100 */
									return ((bool_t) 0);
								}
							else
								{	/* Ieee/equiv.scm 100 */
									if (PAIRP(BgL_obj1z00_7))
										{	/* Ieee/equiv.scm 102 */
											if (PAIRP(BgL_obj2z00_8))
												{	/* Ieee/equiv.scm 103 */
													if (BGl_equalzf3zf3zz__r4_equivalence_6_2z00(CAR
															(BgL_obj1z00_7), CAR(BgL_obj2z00_8)))
														{
															obj_t BgL_obj2z00_2086;
															obj_t BgL_obj1z00_2084;

															BgL_obj1z00_2084 = CDR(BgL_obj1z00_7);
															BgL_obj2z00_2086 = CDR(BgL_obj2z00_8);
															BgL_obj2z00_8 = BgL_obj2z00_2086;
															BgL_obj1z00_7 = BgL_obj1z00_2084;
															goto BGl_equalzf3zf3zz__r4_equivalence_6_2z00;
														}
													else
														{	/* Ieee/equiv.scm 104 */
															return ((bool_t) 0);
														}
												}
											else
												{	/* Ieee/equiv.scm 103 */
													return ((bool_t) 0);
												}
										}
									else
										{	/* Ieee/equiv.scm 102 */
											if (VECTORP(BgL_obj1z00_7))
												{	/* Ieee/equiv.scm 106 */
													if (VECTORP(BgL_obj2z00_8))
														{	/* Ieee/equiv.scm 108 */
															if (
																(VECTOR_LENGTH(BgL_obj2z00_8) ==
																	VECTOR_LENGTH(BgL_obj1z00_7)))
																{	/* Ieee/equiv.scm 110 */
																	bool_t BgL_test1693z00_2096;

																	{	/* Ieee/equiv.scm 110 */
																		int BgL_arg1183z00_1238;
																		int BgL_arg1187z00_1239;

																		BgL_arg1183z00_1238 =
																			VECTOR_TAG(BgL_obj1z00_7);
																		BgL_arg1187z00_1239 =
																			VECTOR_TAG(BgL_obj2z00_8);
																		BgL_test1693z00_2096 =
																			(
																			(long) (BgL_arg1183z00_1238) ==
																			(long) (BgL_arg1187z00_1239));
																	}
																	if (BgL_test1693z00_2096)
																		{
																			long BgL_iz00_1780;

																			BgL_iz00_1780 = 0L;
																		BgL_testz00_1779:
																			{	/* Ieee/equiv.scm 112 */
																				bool_t BgL__ortest_1047z00_1781;

																				BgL__ortest_1047z00_1781 =
																					(BgL_iz00_1780 ==
																					VECTOR_LENGTH(BgL_obj1z00_7));
																				if (BgL__ortest_1047z00_1781)
																					{	/* Ieee/equiv.scm 112 */
																						return BgL__ortest_1047z00_1781;
																					}
																				else
																					{	/* Ieee/equiv.scm 113 */
																						bool_t BgL_test1695z00_2105;

																						{	/* Ieee/equiv.scm 113 */
																							obj_t BgL_arg1172z00_1785;
																							obj_t BgL_arg1182z00_1786;

																							BgL_arg1172z00_1785 =
																								VECTOR_REF(
																								((obj_t) BgL_obj1z00_7),
																								BgL_iz00_1780);
																							BgL_arg1182z00_1786 =
																								VECTOR_REF(((obj_t)
																									BgL_obj2z00_8),
																								BgL_iz00_1780);
																							BgL_test1695z00_2105 =
																								BGl_equalzf3zf3zz__r4_equivalence_6_2z00
																								(BgL_arg1172z00_1785,
																								BgL_arg1182z00_1786);
																						}
																						if (BgL_test1695z00_2105)
																							{
																								long BgL_iz00_2111;

																								BgL_iz00_2111 =
																									(BgL_iz00_1780 + 1L);
																								BgL_iz00_1780 = BgL_iz00_2111;
																								goto BgL_testz00_1779;
																							}
																						else
																							{	/* Ieee/equiv.scm 113 */
																								return ((bool_t) 0);
																							}
																					}
																			}
																		}
																	else
																		{	/* Ieee/equiv.scm 110 */
																			return ((bool_t) 0);
																		}
																}
															else
																{	/* Ieee/equiv.scm 109 */
																	return ((bool_t) 0);
																}
														}
													else
														{	/* Ieee/equiv.scm 108 */
															return ((bool_t) 0);
														}
												}
											else
												{	/* Ieee/equiv.scm 106 */
													if (BGl_eqvzf3zf3zz__r4_equivalence_6_2z00
														(BgL_obj1z00_7, BgL_obj2z00_8))
														{	/* Ieee/equiv.scm 116 */
															return ((bool_t) 1);
														}
													else
														{	/* Ieee/equiv.scm 116 */
															if (INTEGERP(BgL_obj1z00_7))
																{	/* Ieee/equiv.scm 118 */
																	return ((bool_t) 0);
																}
															else
																{	/* Ieee/equiv.scm 118 */
																	if (BGL_HVECTORP(BgL_obj1z00_7))
																		{	/* Ieee/equiv.scm 121 */
																			long BgL_lobj1z00_1244;

																			BgL_lobj1z00_1244 =
																				BGL_HVECTOR_LENGTH(BgL_obj1z00_7);
																			if (BGL_HVECTORP(BgL_obj2z00_8))
																				{	/* Ieee/equiv.scm 123 */
																					bool_t BgL_test1700z00_2122;

																					{	/* Ieee/equiv.scm 123 */
																						long BgL_arg1197z00_1267;

																						BgL_arg1197z00_1267 =
																							BGL_HVECTOR_LENGTH(BgL_obj2z00_8);
																						BgL_test1700z00_2122 =
																							(BgL_arg1197z00_1267 ==
																							BgL_lobj1z00_1244);
																					}
																					if (BgL_test1700z00_2122)
																						{	/* Ieee/equiv.scm 124 */
																							obj_t BgL_tag1z00_1247;

																							BgL_tag1z00_1247 =
																								BGl_homogeneouszd2vectorzd2infoz00zz__srfi4z00
																								(BgL_obj1z00_7);
																							{	/* Ieee/equiv.scm 125 */
																								obj_t BgL__z00_1248;
																								obj_t BgL_getz00_1249;
																								obj_t BgL__z00_1250;
																								obj_t BgL_cmpz00_1251;

																								{	/* Ieee/equiv.scm 126 */
																									obj_t BgL_tmpz00_1795;

																									{	/* Ieee/equiv.scm 126 */
																										int BgL_tmpz00_2126;

																										BgL_tmpz00_2126 =
																											(int) (1L);
																										BgL_tmpz00_1795 =
																											BGL_MVALUES_VAL
																											(BgL_tmpz00_2126);
																									}
																									{	/* Ieee/equiv.scm 126 */
																										int BgL_tmpz00_2129;

																										BgL_tmpz00_2129 =
																											(int) (1L);
																										BGL_MVALUES_VAL_SET
																											(BgL_tmpz00_2129,
																											BUNSPEC);
																									}
																									BgL__z00_1248 =
																										BgL_tmpz00_1795;
																								}
																								{	/* Ieee/equiv.scm 126 */
																									obj_t BgL_tmpz00_1796;

																									{	/* Ieee/equiv.scm 126 */
																										int BgL_tmpz00_2132;

																										BgL_tmpz00_2132 =
																											(int) (2L);
																										BgL_tmpz00_1796 =
																											BGL_MVALUES_VAL
																											(BgL_tmpz00_2132);
																									}
																									{	/* Ieee/equiv.scm 126 */
																										int BgL_tmpz00_2135;

																										BgL_tmpz00_2135 =
																											(int) (2L);
																										BGL_MVALUES_VAL_SET
																											(BgL_tmpz00_2135,
																											BUNSPEC);
																									}
																									BgL_getz00_1249 =
																										BgL_tmpz00_1796;
																								}
																								{	/* Ieee/equiv.scm 126 */
																									obj_t BgL_tmpz00_1797;

																									{	/* Ieee/equiv.scm 126 */
																										int BgL_tmpz00_2138;

																										BgL_tmpz00_2138 =
																											(int) (3L);
																										BgL_tmpz00_1797 =
																											BGL_MVALUES_VAL
																											(BgL_tmpz00_2138);
																									}
																									{	/* Ieee/equiv.scm 126 */
																										int BgL_tmpz00_2141;

																										BgL_tmpz00_2141 =
																											(int) (3L);
																										BGL_MVALUES_VAL_SET
																											(BgL_tmpz00_2141,
																											BUNSPEC);
																									}
																									BgL__z00_1250 =
																										BgL_tmpz00_1797;
																								}
																								{	/* Ieee/equiv.scm 126 */
																									obj_t BgL_tmpz00_1798;

																									{	/* Ieee/equiv.scm 126 */
																										int BgL_tmpz00_2144;

																										BgL_tmpz00_2144 =
																											(int) (4L);
																										BgL_tmpz00_1798 =
																											BGL_MVALUES_VAL
																											(BgL_tmpz00_2144);
																									}
																									{	/* Ieee/equiv.scm 126 */
																										int BgL_tmpz00_2147;

																										BgL_tmpz00_2147 =
																											(int) (4L);
																										BGL_MVALUES_VAL_SET
																											(BgL_tmpz00_2147,
																											BUNSPEC);
																									}
																									BgL_cmpz00_1251 =
																										BgL_tmpz00_1798;
																								}
																								{	/* Ieee/equiv.scm 126 */
																									obj_t BgL_tag2z00_1252;

																									BgL_tag2z00_1252 =
																										BGl_homogeneouszd2vectorzd2infoz00zz__srfi4z00
																										(BgL_obj2z00_8);
																									{	/* Ieee/equiv.scm 127 */
																										obj_t BgL__z00_1253;
																										obj_t BgL__z00_1254;
																										obj_t BgL__z00_1255;
																										obj_t BgL__z00_1256;

																										{	/* Ieee/equiv.scm 128 */
																											obj_t BgL_tmpz00_1799;

																											{	/* Ieee/equiv.scm 128 */
																												int BgL_tmpz00_2151;

																												BgL_tmpz00_2151 =
																													(int) (1L);
																												BgL_tmpz00_1799 =
																													BGL_MVALUES_VAL
																													(BgL_tmpz00_2151);
																											}
																											{	/* Ieee/equiv.scm 128 */
																												int BgL_tmpz00_2154;

																												BgL_tmpz00_2154 =
																													(int) (1L);
																												BGL_MVALUES_VAL_SET
																													(BgL_tmpz00_2154,
																													BUNSPEC);
																											}
																											BgL__z00_1253 =
																												BgL_tmpz00_1799;
																										}
																										{	/* Ieee/equiv.scm 128 */
																											obj_t BgL_tmpz00_1800;

																											{	/* Ieee/equiv.scm 128 */
																												int BgL_tmpz00_2157;

																												BgL_tmpz00_2157 =
																													(int) (2L);
																												BgL_tmpz00_1800 =
																													BGL_MVALUES_VAL
																													(BgL_tmpz00_2157);
																											}
																											{	/* Ieee/equiv.scm 128 */
																												int BgL_tmpz00_2160;

																												BgL_tmpz00_2160 =
																													(int) (2L);
																												BGL_MVALUES_VAL_SET
																													(BgL_tmpz00_2160,
																													BUNSPEC);
																											}
																											BgL__z00_1254 =
																												BgL_tmpz00_1800;
																										}
																										{	/* Ieee/equiv.scm 128 */
																											obj_t BgL_tmpz00_1801;

																											{	/* Ieee/equiv.scm 128 */
																												int BgL_tmpz00_2163;

																												BgL_tmpz00_2163 =
																													(int) (3L);
																												BgL_tmpz00_1801 =
																													BGL_MVALUES_VAL
																													(BgL_tmpz00_2163);
																											}
																											{	/* Ieee/equiv.scm 128 */
																												int BgL_tmpz00_2166;

																												BgL_tmpz00_2166 =
																													(int) (3L);
																												BGL_MVALUES_VAL_SET
																													(BgL_tmpz00_2166,
																													BUNSPEC);
																											}
																											BgL__z00_1255 =
																												BgL_tmpz00_1801;
																										}
																										{	/* Ieee/equiv.scm 128 */
																											obj_t BgL_tmpz00_1802;

																											{	/* Ieee/equiv.scm 128 */
																												int BgL_tmpz00_2169;

																												BgL_tmpz00_2169 =
																													(int) (4L);
																												BgL_tmpz00_1802 =
																													BGL_MVALUES_VAL
																													(BgL_tmpz00_2169);
																											}
																											{	/* Ieee/equiv.scm 128 */
																												int BgL_tmpz00_2172;

																												BgL_tmpz00_2172 =
																													(int) (4L);
																												BGL_MVALUES_VAL_SET
																													(BgL_tmpz00_2172,
																													BUNSPEC);
																											}
																											BgL__z00_1256 =
																												BgL_tmpz00_1802;
																										}
																										if (
																											(BgL_tag1z00_1247 ==
																												BgL_tag2z00_1252))
																											{
																												long BgL_iz00_1259;

																												BgL_iz00_1259 = 0L;
																											BgL_zc3z04anonymousza31192ze3z87_1260:
																												{	/* Ieee/equiv.scm 130 */
																													bool_t
																														BgL__ortest_1052z00_1261;
																													BgL__ortest_1052z00_1261
																														=
																														(BgL_iz00_1259 ==
																														BgL_lobj1z00_1244);
																													if (BgL__ortest_1052z00_1261)
																														{	/* Ieee/equiv.scm 130 */
																															return
																																BgL__ortest_1052z00_1261;
																														}
																													else
																														{	/* Ieee/equiv.scm 131 */
																															obj_t
																																BgL__andtest_1053z00_1262;
																															{	/* Ieee/equiv.scm 131 */
																																obj_t
																																	BgL_arg1194z00_1264;
																																obj_t
																																	BgL_arg1196z00_1265;
																																BgL_arg1194z00_1264
																																	=
																																	BGL_PROCEDURE_CALL2
																																	(BgL_getz00_1249,
																																	BgL_obj1z00_7,
																																	BINT
																																	(BgL_iz00_1259));
																																BgL_arg1196z00_1265
																																	=
																																	BGL_PROCEDURE_CALL2
																																	(BgL_getz00_1249,
																																	BgL_obj2z00_8,
																																	BINT
																																	(BgL_iz00_1259));
																																BgL__andtest_1053z00_1262
																																	=
																																	BGL_PROCEDURE_CALL2
																																	(BgL_cmpz00_1251,
																																	BgL_arg1194z00_1264,
																																	BgL_arg1196z00_1265);
																															}
																															if (CBOOL
																																(BgL__andtest_1053z00_1262))
																																{
																																	long
																																		BgL_iz00_2198;
																																	BgL_iz00_2198
																																		=
																																		(BgL_iz00_1259
																																		+ 1L);
																																	BgL_iz00_1259
																																		=
																																		BgL_iz00_2198;
																																	goto
																																		BgL_zc3z04anonymousza31192ze3z87_1260;
																																}
																															else
																																{	/* Ieee/equiv.scm 131 */
																																	return (
																																		(bool_t) 0);
																																}
																														}
																												}
																											}
																										else
																											{	/* Ieee/equiv.scm 128 */
																												return ((bool_t) 0);
																											}
																									}
																								}
																							}
																						}
																					else
																						{	/* Ieee/equiv.scm 123 */
																							return ((bool_t) 0);
																						}
																				}
																			else
																				{	/* Ieee/equiv.scm 122 */
																					return ((bool_t) 0);
																				}
																		}
																	else
																		{	/* Ieee/equiv.scm 120 */
																			if (REALP(BgL_obj1z00_7))
																				{	/* Ieee/equiv.scm 133 */
																					return ((bool_t) 0);
																				}
																			else
																				{	/* Ieee/equiv.scm 133 */
																					if (STRUCTP(BgL_obj1z00_7))
																						{	/* Ieee/equiv.scm 136 */
																							int BgL_lobj1z00_1270;

																							BgL_lobj1z00_1270 =
																								STRUCT_LENGTH(BgL_obj1z00_7);
																							if (STRUCTP(BgL_obj2z00_8))
																								{	/* Ieee/equiv.scm 137 */
																									if (
																										((long) (STRUCT_LENGTH
																												(BgL_obj2z00_8)) ==
																											(long)
																											(BgL_lobj1z00_1270)))
																										{
																											long BgL_iz00_1818;

																											BgL_iz00_1818 = 0L;
																										BgL_testz00_1817:
																											{	/* Ieee/equiv.scm 140 */
																												bool_t
																													BgL__ortest_1056z00_1819;
																												BgL__ortest_1056z00_1819
																													=
																													(BgL_iz00_1818 ==
																													(long)
																													(BgL_lobj1z00_1270));
																												if (BgL__ortest_1056z00_1819)
																													{	/* Ieee/equiv.scm 140 */
																														return
																															BgL__ortest_1056z00_1819;
																													}
																												else
																													{	/* Ieee/equiv.scm 141 */
																														bool_t
																															BgL_test1709z00_2215;
																														{	/* Ieee/equiv.scm 141 */
																															obj_t
																																BgL_arg1202z00_1823;
																															obj_t
																																BgL_arg1203z00_1824;
																															BgL_arg1202z00_1823
																																=
																																STRUCT_REF((
																																	(obj_t)
																																	BgL_obj1z00_7),
																																(int)
																																(BgL_iz00_1818));
																															BgL_arg1203z00_1824
																																=
																																STRUCT_REF((
																																	(obj_t)
																																	BgL_obj2z00_8),
																																(int)
																																(BgL_iz00_1818));
																															BgL_test1709z00_2215
																																=
																																BGl_equalzf3zf3zz__r4_equivalence_6_2z00
																																(BgL_arg1202z00_1823,
																																BgL_arg1203z00_1824);
																														}
																														if (BgL_test1709z00_2215)
																															{
																																long
																																	BgL_iz00_2223;
																																BgL_iz00_2223 =
																																	(BgL_iz00_1818
																																	+ 1L);
																																BgL_iz00_1818 =
																																	BgL_iz00_2223;
																																goto
																																	BgL_testz00_1817;
																															}
																														else
																															{	/* Ieee/equiv.scm 141 */
																																return ((bool_t)
																																	0);
																															}
																													}
																											}
																										}
																									else
																										{	/* Ieee/equiv.scm 138 */
																											return ((bool_t) 0);
																										}
																								}
																							else
																								{	/* Ieee/equiv.scm 137 */
																									return ((bool_t) 0);
																								}
																						}
																					else
																						{	/* Ieee/equiv.scm 135 */
																							if (CELLP(BgL_obj1z00_7))
																								{	/* Ieee/equiv.scm 143 */
																									if (CELLP(BgL_obj2z00_8))
																										{
																											obj_t BgL_obj2z00_2231;
																											obj_t BgL_obj1z00_2229;

																											BgL_obj1z00_2229 =
																												CELL_REF(BgL_obj1z00_7);
																											BgL_obj2z00_2231 =
																												CELL_REF(BgL_obj2z00_8);
																											BgL_obj2z00_8 =
																												BgL_obj2z00_2231;
																											BgL_obj1z00_7 =
																												BgL_obj1z00_2229;
																											goto
																												BGl_equalzf3zf3zz__r4_equivalence_6_2z00;
																										}
																									else
																										{	/* Ieee/equiv.scm 144 */
																											return ((bool_t) 0);
																										}
																								}
																							else
																								{	/* Ieee/equiv.scm 143 */
																									if (BGL_OBJECTP
																										(BgL_obj1z00_7))
																										{	/* Ieee/equiv.scm 145 */
																											if (BGL_OBJECTP
																												(BgL_obj2z00_8))
																												{	/* Ieee/equiv.scm 146 */
																													return
																														BGl_objectzd2equalzf3z21zz__objectz00
																														(((BgL_objectz00_bglt) BgL_obj1z00_7), ((BgL_objectz00_bglt) BgL_obj2z00_8));
																												}
																											else
																												{	/* Ieee/equiv.scm 146 */
																													return ((bool_t) 0);
																												}
																										}
																									else
																										{	/* Ieee/equiv.scm 145 */
																											if (UCS2_STRINGP
																												(BgL_obj1z00_7))
																												{	/* Ieee/equiv.scm 147 */
																													if (UCS2_STRINGP
																														(BgL_obj2z00_8))
																														{	/* Ieee/equiv.scm 148 */
																															return
																																ucs2_strcmp
																																(BgL_obj1z00_7,
																																BgL_obj2z00_8);
																														}
																													else
																														{	/* Ieee/equiv.scm 148 */
																															return ((bool_t)
																																0);
																														}
																												}
																											else
																												{	/* Ieee/equiv.scm 147 */
																													if (CUSTOMP
																														(BgL_obj1z00_7))
																														{	/* Ieee/equiv.scm 149 */
																															if (CUSTOMP
																																(BgL_obj2z00_8))
																																{	/* Ieee/equiv.scm 150 */
																																	return
																																		CUSTOM_CMP(
																																		((obj_t)
																																			BgL_obj1z00_7),
																																		((obj_t)
																																			BgL_obj2z00_8));
																																}
																															else
																																{	/* Ieee/equiv.scm 150 */
																																	return (
																																		(bool_t) 0);
																																}
																														}
																													else
																														{	/* Ieee/equiv.scm 149 */
																															if (UCS2P
																																(BgL_obj1z00_7))
																																{	/* Ieee/equiv.scm 151 */
																																	if (UCS2P
																																		(BgL_obj2z00_8))
																																		{	/* Ieee/equiv.scm 152 */
																																			return
																																				(CUCS2
																																				(BgL_obj1z00_7)
																																				==
																																				CUCS2
																																				(BgL_obj2z00_8));
																																		}
																																	else
																																		{	/* Ieee/equiv.scm 152 */
																																			return (
																																				(bool_t)
																																				0);
																																		}
																																}
																															else
																																{	/* Ieee/equiv.scm 151 */
																																	if (BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_obj1z00_7))
																																		{	/* Ieee/equiv.scm 153 */
																																			return (
																																				(bool_t)
																																				0);
																																		}
																																	else
																																		{	/* Ieee/equiv.scm 153 */
																																			if (BGL_DATEP(BgL_obj1z00_7))
																																				{	/* Ieee/equiv.scm 155 */
																																					if (BGL_DATEP(BgL_obj2z00_8))
																																						{	/* Ieee/equiv.scm 156 */
																																							return
																																								(bgl_date_to_seconds
																																								(BgL_obj1z00_7)
																																								==
																																								bgl_date_to_seconds
																																								(BgL_obj2z00_8));
																																						}
																																					else
																																						{	/* Ieee/equiv.scm 156 */
																																							return
																																								(
																																								(bool_t)
																																								0);
																																						}
																																				}
																																			else
																																				{	/* Ieee/equiv.scm 155 */
																																					if (FOREIGNP(BgL_obj1z00_7))
																																						{	/* Ieee/equiv.scm 157 */
																																							if (FOREIGNP(BgL_obj2z00_8))
																																								{	/* Ieee/equiv.scm 158 */
																																									return
																																										FOREIGN_EQP
																																										(BgL_obj1z00_7,
																																										BgL_obj2z00_8);
																																								}
																																							else
																																								{	/* Ieee/equiv.scm 158 */
																																									return
																																										(
																																										(bool_t)
																																										0);
																																								}
																																						}
																																					else
																																						{	/* Ieee/equiv.scm 157 */
																																							if (BGL_WEAKPTRP(BgL_obj1z00_7))
																																								{	/* Ieee/equiv.scm 159 */
																																									if (BGL_WEAKPTRP(BgL_obj2z00_8))
																																										{
																																											obj_t
																																												BgL_obj2z00_2279;
																																											obj_t
																																												BgL_obj1z00_2277;
																																											BgL_obj1z00_2277
																																												=
																																												bgl_weakptr_data
																																												(BgL_obj1z00_7);
																																											BgL_obj2z00_2279
																																												=
																																												bgl_weakptr_data
																																												(BgL_obj2z00_8);
																																											BgL_obj2z00_8
																																												=
																																												BgL_obj2z00_2279;
																																											BgL_obj1z00_7
																																												=
																																												BgL_obj1z00_2277;
																																											goto
																																												BGl_equalzf3zf3zz__r4_equivalence_6_2z00;
																																										}
																																									else
																																										{	/* Ieee/equiv.scm 160 */
																																											return
																																												(
																																												(bool_t)
																																												0);
																																										}
																																								}
																																							else
																																								{	/* Ieee/equiv.scm 159 */
																																									return
																																										(
																																										(bool_t)
																																										0);
																																								}
																																						}
																																				}
																																		}
																																}
																														}
																												}
																										}
																								}
																						}
																				}
																		}
																}
														}
												}
										}
								}
						}
				}
		}

	}



/* &equal? */
	obj_t BGl_z62equalzf3z91zz__r4_equivalence_6_2z00(obj_t BgL_envz00_1947,
		obj_t BgL_obj1z00_1948, obj_t BgL_obj2z00_1949)
	{
		{	/* Ieee/equiv.scm 94 */
			return
				BBOOL(BGl_equalzf3zf3zz__r4_equivalence_6_2z00(BgL_obj1z00_1948,
					BgL_obj2z00_1949));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__r4_equivalence_6_2z00(void)
	{
		{	/* Ieee/equiv.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__r4_equivalence_6_2z00(void)
	{
		{	/* Ieee/equiv.scm 14 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__r4_equivalence_6_2z00(void)
	{
		{	/* Ieee/equiv.scm 14 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__r4_equivalence_6_2z00(void)
	{
		{	/* Ieee/equiv.scm 14 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1640z00zz__r4_equivalence_6_2z00));
			return
				BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1640z00zz__r4_equivalence_6_2z00));
		}

	}

#ifdef __cplusplus
}
#endif
