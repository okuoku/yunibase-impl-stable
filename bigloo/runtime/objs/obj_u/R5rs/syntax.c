/*===========================================================================*/
/*   (R5rs/syntax.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c R5rs/syntax.scm -indent -o objs/obj_u/R5rs/syntax.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___R5_MACRO_4_3_SYNTAX_TYPE_DEFINITIONS
#define BGL___R5_MACRO_4_3_SYNTAX_TYPE_DEFINITIONS
#endif													// BGL___R5_MACRO_4_3_SYNTAX_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2letreczd2syntaxz00zz__r5_macro_4_3_syntaxz00(obj_t, obj_t);
	extern bool_t BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_hygienezd2prefixzd2zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2450z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2484z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2451z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2370z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2533z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2486z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2454z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2488z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2455z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2375z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2538z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2376z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2539z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2458z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2377z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2459z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2378z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2379z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	extern obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_syntaxzd2getzd2framesz00zz__r5_macro_4_3_syntaxz00(obj_t,
		obj_t, obj_t);
	static bool_t BGl_ellipsiszf3zf3zz__r5_macro_4_3_syntaxz00(obj_t);
	static obj_t BGl_list2540z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2460z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2461z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2380z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2576z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2543z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2462z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2381z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2544z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2496z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2463z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2382z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2545z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2464z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2383z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2546z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2465z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2384z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	extern obj_t bstring_to_keyword(obj_t);
	static obj_t BGl_list2547z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2385z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2548z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2386z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2549z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2387z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2388z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_requirezd2initializa7ationz75zz__r5_macro_4_3_syntaxz00 =
		BUNSPEC;
	static obj_t BGl_list2389z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_hygienezd2markzd2zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t
		BGl_syntaxzd2matcheszd2patternzf3zf3zz__r5_macro_4_3_syntaxz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2letzd2syntaxz00zz__r5_macro_4_3_syntaxz00(obj_t, obj_t);
	static obj_t BGl_list2550z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2551z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2470z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2552z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2390z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2553z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2391z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static bool_t BGl_hygienezd2eqzf3z21zz__r5_macro_4_3_syntaxz00(obj_t, obj_t);
	static obj_t BGl_list2554z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2555z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2475z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2394z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2476z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2395z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2477z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2396z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2478z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2397z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2479z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2398z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	extern bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__r5_macro_4_3_syntaxz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_installzd2expanderzd2zz__macroz00(obj_t, obj_t);
	static obj_t BGl_list2480z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2481z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2482z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_hygieniza7eza7zz__r5_macro_4_3_syntaxz00(obj_t, obj_t);
	static obj_t BGl_list2483z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static long BGl_hygienezd2prefixzd2lenz00zz__r5_macro_4_3_syntaxz00 = 0L;
	static obj_t BGl_genvarsz00zz__r5_macro_4_3_syntaxz00(obj_t);
	static obj_t
		BGl_initzd2syntaxzd2expandersz12z12zz__r5_macro_4_3_syntaxz00(void);
	static obj_t
		BGl_z62syntaxzd2ruleszd2ze3expanderz81zz__r5_macro_4_3_syntaxz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_list2490z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zz__r5_macro_4_3_syntaxz00(void);
	static obj_t BGl_list2492z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2493z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2494z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_unhygieniza7eza7zz__r5_macro_4_3_syntaxz00(obj_t);
	static obj_t BGl_list2495z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t
		BGl_z62zc3z04anonymousza31373ze3ze5zz__r5_macro_4_3_syntaxz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_list2498z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2499z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_syntaxesz00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_installzd2syntaxzd2expanderz00zz__r5_macro_4_3_syntaxz00(obj_t, obj_t);
	static obj_t
		BGl_z62installzd2syntaxzd2expanderz62zz__r5_macro_4_3_syntaxz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__r5_macro_4_3_syntaxz00(void);
	extern bool_t BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	extern obj_t bgl_reverse(obj_t);
	static obj_t BGl_genericzd2initzd2zz__r5_macro_4_3_syntaxz00(void);
	extern obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	extern long bgl_list_length(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__r5_macro_4_3_syntaxz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__r5_macro_4_3_syntaxz00(void);
	static obj_t
		BGl_z62expandzd2definezd2syntaxz62zz__r5_macro_4_3_syntaxz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_objectzd2initzd2zz__r5_macro_4_3_syntaxz00(void);
	static obj_t BGl_hygienezd2valueza2z70zz__r5_macro_4_3_syntaxz00(obj_t);
	extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern bool_t bigloo_strcmp_at(obj_t, obj_t, long);
	static obj_t BGl_symbol2401z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2320z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2322z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2406z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2329z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2definezd2syntaxz00zz__r5_macro_4_3_syntaxz00(obj_t, obj_t);
	static obj_t BGl_syntaxzd2expanderszd2mutexz00zz__r5_macro_4_3_syntaxz00 =
		BUNSPEC;
	static obj_t BGl_symbol2331z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2415z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2334z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2336z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2419z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_gennameze70ze7zz__r5_macro_4_3_syntaxz00(obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_symbol2342z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_syntaxzd2ruleszd2ze3expanderze3zz__r5_macro_4_3_syntaxz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol2505z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2424z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2344z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	extern obj_t c_substring(obj_t, long, long);
	static obj_t BGl_symbol2349z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	extern obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_appendzd221011zd2zz__r5_macro_4_3_syntaxz00(obj_t, obj_t);
	static obj_t BGl_symbol2510z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_syntaxzd2expandzd2patternz00zz__r5_macro_4_3_syntaxz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_symbol2512z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2433z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_methodzd2initzd2zz__r5_macro_4_3_syntaxz00(void);
	static obj_t BGl_symbol2435z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2354z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2403z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2518z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2437z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2404z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2356z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2405z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2439z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2359z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2408z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2409z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t
		BGl_z62zc3z04anonymousza31308ze3ze5zz__r5_macro_4_3_syntaxz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_getzd2ellipsiszd2framesz00zz__r5_macro_4_3_syntaxz00(obj_t,
		obj_t);
	static obj_t BGl_symbol2520z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2443z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2410z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2411z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2363z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2412z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2527z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2413z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2414z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2366z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2333z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2529z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2448z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_loopze70ze7zz__r5_macro_4_3_syntaxz00(obj_t, obj_t);
	static obj_t BGl_loopze71ze7zz__r5_macro_4_3_syntaxz00(obj_t, obj_t);
	static obj_t BGl_list2417z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_loopze72ze7zz__r5_macro_4_3_syntaxz00(obj_t, obj_t);
	static obj_t BGl_list2418z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_loopze73ze7zz__r5_macro_4_3_syntaxz00(obj_t, obj_t);
	static obj_t BGl_list2338z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2339z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_keyword2574z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2531z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2500z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2452z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2371z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2534z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2501z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2502z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2421z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2373z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2340z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2536z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2503z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2422z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2341z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2504z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2456z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2423z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_syntaxzd2mutexzd2zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2507z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2426z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2508z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2427z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2509z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2428z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2429z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2348z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t
		BGl_z62zc3z04anonymousza31318ze3ze5zz__r5_macro_4_3_syntaxz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_hygienezd2valuezd2zz__r5_macro_4_3_syntaxz00(obj_t);
	static obj_t BGl_subze70ze7zz__r5_macro_4_3_syntaxz00(obj_t, obj_t);
	extern obj_t string_append(obj_t, obj_t);
	static obj_t BGl_symbol2541z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2430z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2431z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t
		BGl_z62expandzd2letreczd2syntaxz62zz__r5_macro_4_3_syntaxz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_list2432z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2351z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2514z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2466z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2352z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2515z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2353z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2516z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2468z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2517z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_z62expandzd2letzd2syntaxz62zz__r5_macro_4_3_syntaxz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_list2358z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t
		BGl_z62zc3z04anonymousza31416ze3ze5zz__r5_macro_4_3_syntaxz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol2471z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2473z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2392z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2522z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2441z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2523z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2442z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2361z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2524z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2362z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2558z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2525z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_list2526z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2445z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_syntaxzd2expanderzd2zz__r5_macro_4_3_syntaxz00(obj_t, obj_t);
	static obj_t BGl_list2446z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2365z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2447z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_symbol2399z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_flattenz00zz__r5_macro_4_3_syntaxz00(obj_t);
	static obj_t BGl_list2368z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t BGl_list2369z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
	static obj_t
		BGl_z62zc3z04anonymousza31409ze3ze5zz__r5_macro_4_3_syntaxz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31328ze3ze5zz__r5_macro_4_3_syntaxz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62syntaxzd2expanderzb0zz__r5_macro_4_3_syntaxz00(obj_t,
		obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_installzd2syntaxzd2expanderzd2envzd2zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_za762installza7d2syn2579z00,
		BGl_z62installzd2syntaxzd2expanderz62zz__r5_macro_4_3_syntaxz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2400z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2400za700za7za7_2580za7, "key", 3);
	      DEFINE_STRING(BGl_string2402z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2402za700za7za7_2581za7, "clauses", 7);
	      DEFINE_STRING(BGl_string2321z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2321za700za7za7_2582za7, "hygiene.r5rs.mark", 17);
	      DEFINE_STRING(BGl_string2323z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2323za700za7za7_2583za7, "mutex", 5);
	      DEFINE_STRING(BGl_string2324z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2324za700za7za7_2584za7,
		"/tmp/bigloo/runtime/R5rs/syntax.scm", 35);
	      DEFINE_STRING(BGl_string2325z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2325za700za7za7_2585za7, "&install-syntax-expander", 24);
	      DEFINE_STRING(BGl_string2407z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2407za700za7za7_2586za7, "atom-key", 8);
	      DEFINE_STRING(BGl_string2326z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2326za700za7za7_2587za7, "symbol", 6);
	      DEFINE_STRING(BGl_string2327z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2327za700za7za7_2588za7, "procedure", 9);
	      DEFINE_STRING(BGl_string2330z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2330za700za7za7_2589za7, "quote", 5);
	      DEFINE_STRING(BGl_string2332z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2332za700za7za7_2590za7, "cond", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2328z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_za762za7c3za704anonymo2591za7,
		BGl_z62zc3z04anonymousza31308ze3ze5zz__r5_macro_4_3_syntaxz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2416z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2416za700za7za7_2592za7, "atoms", 5);
	      DEFINE_STRING(BGl_string2335z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2335za700za7za7_2593za7, "else", 4);
	      DEFINE_STRING(BGl_string2337z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2337za700za7za7_2594za7, "=>", 2);
	      DEFINE_STRING(BGl_string2420z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2420za700za7za7_2595za7, "memv", 4);
	      DEFINE_STRING(BGl_string2343z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2343za700za7za7_2596za7, "result1", 7);
	      DEFINE_STRING(BGl_string2506z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2506za700za7za7_2597za7, "set!", 4);
	      DEFINE_STRING(BGl_string2425z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2425za700za7za7_2598za7, "clause", 6);
	      DEFINE_STRING(BGl_string2345z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2345za700za7za7_2599za7, "result2", 7);
	      DEFINE_STRING(BGl_string2347z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2347za700za7za7_2600za7, "...", 3);
	      DEFINE_STRING(BGl_string2511z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2511za700za7za7_2601za7, "x", 1);
	      DEFINE_STRING(BGl_string2350z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2350za700za7za7_2602za7, "begin", 5);
	      DEFINE_STRING(BGl_string2513z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2513za700za7za7_2603za7, "y", 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2definezd2syntaxzd2envzd2zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_za762expandza7d2defi2604z00,
		BGl_z62expandzd2definezd2syntaxz62zz__r5_macro_4_3_syntaxz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2434z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2434za700za7za7_2605za7, "name", 4);
	      DEFINE_STRING(BGl_string2436z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2436za700za7za7_2606za7, "val", 3);
	      DEFINE_STRING(BGl_string2355z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2355za700za7za7_2607za7, "test", 4);
	      DEFINE_STRING(BGl_string2519z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2519za700za7za7_2608za7, "newtemp", 7);
	      DEFINE_STRING(BGl_string2438z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2438za700za7za7_2609za7, "body1", 5);
	      DEFINE_STRING(BGl_string2357z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2357za700za7za7_2610za7, "result", 6);
	      DEFINE_STRING(BGl_string2521z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2521za700za7za7_2611za7, "do", 2);
	      DEFINE_STRING(BGl_string2440z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2440za700za7za7_2612za7, "body2", 5);
	      DEFINE_STRING(BGl_string2360z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2360za700za7za7_2613za7, "let", 3);
	      DEFINE_STRING(BGl_string2444z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2444za700za7za7_2614za7, "lambda", 6);
	      DEFINE_STRING(BGl_string2364z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2364za700za7za7_2615za7, "temp", 4);
	      DEFINE_STRING(BGl_string2528z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2528za700za7za7_2616za7, "var", 3);
	      DEFINE_STRING(BGl_string2367z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2367za700za7za7_2617za7, "if", 2);
	      DEFINE_STRING(BGl_string2449z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2449za700za7za7_2618za7, "tag", 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2letzd2syntaxzd2envzd2zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_za762expandza7d2letza72619za7,
		BGl_z62expandzd2letzd2syntaxz62zz__r5_macro_4_3_syntaxz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2530z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2530za700za7za7_2620za7, "init", 4);
	      DEFINE_STRING(BGl_string2532z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2532za700za7za7_2621za7, "step", 4);
	      DEFINE_STRING(BGl_string2453z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2453za700za7za7_2622za7, "letrec", 6);
	      DEFINE_STRING(BGl_string2372z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2372za700za7za7_2623za7, "clause1", 7);
	      DEFINE_STRING(BGl_string2535z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2535za700za7za7_2624za7, "expr", 4);
	      DEFINE_STRING(BGl_string2374z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2374za700za7za7_2625za7, "clause2", 7);
	      DEFINE_STRING(BGl_string2537z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2537za700za7za7_2626za7, "command", 7);
	      DEFINE_STRING(BGl_string2457z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2457za700za7za7_2627za7, "let*", 4);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_syntaxzd2expanderzd2envz00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_za762syntaxza7d2expa2628z00,
		BGl_z62syntaxzd2expanderzb0zz__r5_macro_4_3_syntaxz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2542z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2542za700za7za7_2629za7, "loop", 4);
	      DEFINE_STRING(BGl_string2467z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2467za700za7za7_2630za7, "name1", 5);
	      DEFINE_STRING(BGl_string2469z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2469za700za7za7_2631za7, "val1", 4);
	      DEFINE_STRING(BGl_string2472z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2472za700za7za7_2632za7, "name2", 5);
	      DEFINE_STRING(BGl_string2474z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2474za700za7za7_2633za7, "val2", 4);
	      DEFINE_STRING(BGl_string2393z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2393za700za7za7_2634za7, "case", 4);
	      DEFINE_STRING(BGl_string2559z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2559za700za7za7_2635za7, "syntax-rules", 12);
	      DEFINE_STRING(BGl_string2560z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2560za700za7za7_2636za7, "define-syntax", 13);
	      DEFINE_STRING(BGl_string2561z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2561za700za7za7_2637za7, "Illegal form", 12);
	      DEFINE_STRING(BGl_string2562z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2562za700za7za7_2638za7, "&expand-define-syntax", 21);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2556z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_za762za7c3za704anonymo2639za7,
		BGl_z62zc3z04anonymousza31318ze3ze5zz__r5_macro_4_3_syntaxz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2563z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2563za700za7za7_2640za7, "letrec-syntax", 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2557z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_za762za7c3za704anonymo2641za7,
		BGl_z62zc3z04anonymousza31328ze3ze5zz__r5_macro_4_3_syntaxz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2564z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2564za700za7za7_2642za7, "let-syntax", 10);
	      DEFINE_STRING(BGl_string2565z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2565za700za7za7_2643za7, "Illegal bindings", 16);
	      DEFINE_STRING(BGl_string2566z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2566za700za7za7_2644za7, "&expand-letrec-syntax", 21);
	      DEFINE_STRING(BGl_string2485z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2485za700za7za7_2645za7, "var1", 4);
	      DEFINE_STRING(BGl_string2567z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2567za700za7za7_2646za7, "&expand-let-syntax", 18);
	      DEFINE_STRING(BGl_string2568z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2568za700za7za7_2647za7, "Illegal declaration", 19);
	      DEFINE_STRING(BGl_string2487z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2487za700za7za7_2648za7, "init1", 5);
	      DEFINE_STRING(BGl_string2569z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2569za700za7za7_2649za7, "&syntax-rules->expander", 23);
	      DEFINE_STRING(BGl_string2489z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2489za700za7za7_2650za7, "body", 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_syntaxzd2ruleszd2ze3expanderzd2envz31zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_za762syntaxza7d2rule2651z00,
		BGl_z62syntaxzd2ruleszd2ze3expanderz81zz__r5_macro_4_3_syntaxz00, 0L,
		BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2570z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2570za700za7za7_2652za7, "pair-nil", 8);
	      DEFINE_STRING(BGl_string2571z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2571za700za7za7_2653za7, "No matching clause", 18);
	      DEFINE_STRING(BGl_string2572z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2572za700za7za7_2654za7, "Illegal clause", 14);
	      DEFINE_STRING(BGl_string2491z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2491za700za7za7_2655za7, "generate temp names", 19);
	      DEFINE_STRING(BGl_string2573z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2573za700za7za7_2656za7, "Illegal ellipsis", 16);
	      DEFINE_STRING(BGl_string2575z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2575za700za7za7_2657za7, "ellipsis", 8);
	      DEFINE_STRING(BGl_string2577z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2577za700za7za7_2658za7, "bind-exit", 9);
	      DEFINE_STRING(BGl_string2578z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2578za700za7za7_2659za7, "__r5_macro_4_3_syntax", 21);
	      DEFINE_STRING(BGl_string2497z00zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_string2497za700za7za7_2660za7, "temp1", 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2letreczd2syntaxzd2envzd2zz__r5_macro_4_3_syntaxz00,
		BgL_bgl_za762expandza7d2letr2661z00,
		BGl_z62expandzd2letreczd2syntaxz62zz__r5_macro_4_3_syntaxz00, 0L, BUNSPEC,
		2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_hygienezd2prefixzd2zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2450z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2484z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2451z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2370z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2533z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2486z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2454z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2488z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2455z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2375z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2538z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2376z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2539z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2458z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2377z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2459z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2378z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2379z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2540z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2460z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2461z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2380z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2576z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2543z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2462z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2381z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2544z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2496z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2463z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2382z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2545z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2464z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2383z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2546z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2465z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2384z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2547z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2385z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2548z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2386z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2549z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2387z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2388z00zz__r5_macro_4_3_syntaxz00));
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2389z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_hygienezd2markzd2zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2550z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2551z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2470z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2552z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2390z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2553z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2391z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2554z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2555z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2475z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2394z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2476z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2395z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2477z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2396z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2478z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2397z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2479z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2398z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2480z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2481z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2482z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2483z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2490z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2492z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2493z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2494z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2495z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2498z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2499z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_syntaxesz00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2401z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2320z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2322z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2406z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2329z00zz__r5_macro_4_3_syntaxz00));
		   
			 ADD_ROOT((void
				*) (&BGl_syntaxzd2expanderszd2mutexz00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2331z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2415z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2334z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2336z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2419z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2342z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2505z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2424z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2344z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2349z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2510z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2512z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2433z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2435z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2354z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2403z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2518z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2437z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2404z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2356z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2405z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2439z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2359z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2408z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2409z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2520z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2443z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2410z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2411z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2363z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2412z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2527z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2413z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2414z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2366z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2333z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2529z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2448z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2417z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2418z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2338z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2339z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_keyword2574z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2531z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2500z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2452z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2371z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2534z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2501z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2502z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2421z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2373z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2340z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2536z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2503z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2422z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2341z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2504z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2456z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2423z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_syntaxzd2mutexzd2zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2507z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2426z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2508z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2427z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2509z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2428z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2429z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2348z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2541z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2430z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2431z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2432z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2351z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2514z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2466z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2352z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2515z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2353z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2516z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2468z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2517z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2358z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2471z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2473z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2392z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2522z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2441z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2523z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2442z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2361z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2524z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2362z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2558z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2525z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2526z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2445z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2446z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2365z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2447z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_symbol2399z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2368z00zz__r5_macro_4_3_syntaxz00));
		     ADD_ROOT((void *) (&BGl_list2369z00zz__r5_macro_4_3_syntaxz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__r5_macro_4_3_syntaxz00(long
		BgL_checksumz00_3825, char *BgL_fromz00_3826)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__r5_macro_4_3_syntaxz00))
				{
					BGl_requirezd2initializa7ationz75zz__r5_macro_4_3_syntaxz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__r5_macro_4_3_syntaxz00();
					BGl_cnstzd2initzd2zz__r5_macro_4_3_syntaxz00();
					BGl_importedzd2moduleszd2initz00zz__r5_macro_4_3_syntaxz00();
					return BGl_toplevelzd2initzd2zz__r5_macro_4_3_syntaxz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__r5_macro_4_3_syntaxz00(void)
	{
		{	/* R5rs/syntax.scm 17 */
			BGl_symbol2320z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2321z00zz__r5_macro_4_3_syntaxz00);
			BGl_symbol2322z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2323z00zz__r5_macro_4_3_syntaxz00);
			BGl_symbol2329z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2330z00zz__r5_macro_4_3_syntaxz00);
			BGl_symbol2331z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2332z00zz__r5_macro_4_3_syntaxz00);
			BGl_symbol2334z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2335z00zz__r5_macro_4_3_syntaxz00);
			BGl_symbol2336z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2337z00zz__r5_macro_4_3_syntaxz00);
			BGl_list2333z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2334z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2336z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_symbol2342z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2343z00zz__r5_macro_4_3_syntaxz00);
			BGl_symbol2344z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2345z00zz__r5_macro_4_3_syntaxz00);
			BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2347z00zz__r5_macro_4_3_syntaxz00);
			BGl_list2341z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2334z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2342z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_symbol2344z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00,
							BNIL))));
			BGl_list2340z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2331z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2341z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_symbol2349z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2350z00zz__r5_macro_4_3_syntaxz00);
			BGl_list2348z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2349z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2342z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_symbol2344z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00,
							BNIL))));
			BGl_list2339z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2340z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2348z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_symbol2354z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2355z00zz__r5_macro_4_3_syntaxz00);
			BGl_symbol2356z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2357z00zz__r5_macro_4_3_syntaxz00);
			BGl_list2353z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2354z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2336z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_symbol2356z00zz__r5_macro_4_3_syntaxz00, BNIL)));
			BGl_list2352z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2331z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2353z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_symbol2359z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2360z00zz__r5_macro_4_3_syntaxz00);
			BGl_symbol2363z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2364z00zz__r5_macro_4_3_syntaxz00);
			BGl_list2362z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2363z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2354z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_list2361z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2362z00zz__r5_macro_4_3_syntaxz00, BNIL);
			BGl_symbol2366z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2367z00zz__r5_macro_4_3_syntaxz00);
			BGl_list2368z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2356z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2363z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_list2365z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2366z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2363z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_list2368z00zz__r5_macro_4_3_syntaxz00, BNIL)));
			BGl_list2358z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2359z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2361z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_list2365z00zz__r5_macro_4_3_syntaxz00, BNIL)));
			BGl_list2351z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2352z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2358z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_symbol2371z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2372z00zz__r5_macro_4_3_syntaxz00);
			BGl_symbol2373z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2374z00zz__r5_macro_4_3_syntaxz00);
			BGl_list2370z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2331z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2353z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_symbol2371z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BGl_symbol2373z00zz__r5_macro_4_3_syntaxz00,
							MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00,
								BNIL)))));
			BGl_list2377z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2331z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2371z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_symbol2373z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00,
							BNIL))));
			BGl_list2376z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2366z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2363z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_list2368z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BGl_list2377z00zz__r5_macro_4_3_syntaxz00, BNIL))));
			BGl_list2375z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2359z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2361z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_list2376z00zz__r5_macro_4_3_syntaxz00, BNIL)));
			BGl_list2369z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2370z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2375z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_list2380z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2354z00zz__r5_macro_4_3_syntaxz00, BNIL);
			BGl_list2379z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2331z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2380z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_list2378z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2379z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2354z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_list2382z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2331z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2380z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_symbol2371z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BGl_symbol2373z00zz__r5_macro_4_3_syntaxz00,
							MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00,
								BNIL)))));
			BGl_list2384z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2366z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2363z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_symbol2363z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BGl_list2377z00zz__r5_macro_4_3_syntaxz00, BNIL))));
			BGl_list2383z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2359z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2361z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_list2384z00zz__r5_macro_4_3_syntaxz00, BNIL)));
			BGl_list2381z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2382z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2383z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_list2387z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2354z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2342z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_symbol2344z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00,
							BNIL))));
			BGl_list2386z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2331z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2387z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_list2388z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2366z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2354z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_list2348z00zz__r5_macro_4_3_syntaxz00, BNIL)));
			BGl_list2385z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2386z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2388z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_list2390z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2331z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2387z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_symbol2371z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BGl_symbol2373z00zz__r5_macro_4_3_syntaxz00,
							MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00,
								BNIL)))));
			BGl_list2391z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2366z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2354z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_list2348z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BGl_list2377z00zz__r5_macro_4_3_syntaxz00, BNIL))));
			BGl_list2389z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2390z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2391z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_list2338z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2339z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2351z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_list2369z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BGl_list2378z00zz__r5_macro_4_3_syntaxz00,
							MAKE_YOUNG_PAIR(BGl_list2381z00zz__r5_macro_4_3_syntaxz00,
								MAKE_YOUNG_PAIR(BGl_list2385z00zz__r5_macro_4_3_syntaxz00,
									MAKE_YOUNG_PAIR(BGl_list2389z00zz__r5_macro_4_3_syntaxz00,
										BNIL)))))));
			BGl_symbol2392z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2393z00zz__r5_macro_4_3_syntaxz00);
			BGl_list2394z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2334z00zz__r5_macro_4_3_syntaxz00, BNIL);
			BGl_symbol2399z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2400z00zz__r5_macro_4_3_syntaxz00);
			BGl_list2398z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2399z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_symbol2401z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2402z00zz__r5_macro_4_3_syntaxz00);
			BGl_list2397z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2392z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2398z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_symbol2401z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00,
							BNIL))));
			BGl_symbol2406z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2407z00zz__r5_macro_4_3_syntaxz00);
			BGl_list2405z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2406z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2398z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_list2404z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2405z00zz__r5_macro_4_3_syntaxz00, BNIL);
			BGl_list2408z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2392z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2406z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_symbol2401z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00,
							BNIL))));
			BGl_list2403z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2359z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2404z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_list2408z00zz__r5_macro_4_3_syntaxz00, BNIL)));
			BGl_list2396z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2397z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2403z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_list2410z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2392z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2399z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_list2341z00zz__r5_macro_4_3_syntaxz00, BNIL)));
			BGl_list2409z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2410z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2348z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_symbol2415z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2416z00zz__r5_macro_4_3_syntaxz00);
			BGl_list2414z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2415z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_list2413z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2414z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2342z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_symbol2344z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00,
							BNIL))));
			BGl_list2412z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2392z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2399z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_list2413z00zz__r5_macro_4_3_syntaxz00, BNIL)));
			BGl_symbol2419z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2420z00zz__r5_macro_4_3_syntaxz00);
			BGl_list2421z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2329z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2414z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_list2418z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2419z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2399z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_list2421z00zz__r5_macro_4_3_syntaxz00, BNIL)));
			BGl_list2417z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2366z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2418z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_list2348z00zz__r5_macro_4_3_syntaxz00, BNIL)));
			BGl_list2411z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2412z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2417z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_symbol2424z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2425z00zz__r5_macro_4_3_syntaxz00);
			BGl_list2423z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2392z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2399z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_list2413z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BGl_symbol2424z00zz__r5_macro_4_3_syntaxz00,
							MAKE_YOUNG_PAIR(BGl_symbol2401z00zz__r5_macro_4_3_syntaxz00,
								MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00,
									BNIL))))));
			BGl_list2427z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2392z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2399z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_symbol2424z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BGl_symbol2401z00zz__r5_macro_4_3_syntaxz00,
							MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00,
								BNIL)))));
			BGl_list2426z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2366z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2418z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_list2348z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BGl_list2427z00zz__r5_macro_4_3_syntaxz00, BNIL))));
			BGl_list2422z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2423z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2426z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_list2395z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2396z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2409z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_list2411z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BGl_list2422z00zz__r5_macro_4_3_syntaxz00, BNIL))));
			BGl_symbol2433z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2434z00zz__r5_macro_4_3_syntaxz00);
			BGl_symbol2435z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2436z00zz__r5_macro_4_3_syntaxz00);
			BGl_list2432z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2433z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2435z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_list2431z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2432z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_symbol2437z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2438z00zz__r5_macro_4_3_syntaxz00);
			BGl_symbol2439z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2440z00zz__r5_macro_4_3_syntaxz00);
			BGl_list2430z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2359z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2431z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_symbol2437z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BGl_symbol2439z00zz__r5_macro_4_3_syntaxz00,
							MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00,
								BNIL)))));
			BGl_symbol2443z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2444z00zz__r5_macro_4_3_syntaxz00);
			BGl_list2445z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2433z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_list2442z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2443z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2445z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_symbol2437z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BGl_symbol2439z00zz__r5_macro_4_3_syntaxz00,
							MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00,
								BNIL)))));
			BGl_list2441z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2442z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2435z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00, BNIL)));
			BGl_list2429z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2430z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2441z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_symbol2448z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2449z00zz__r5_macro_4_3_syntaxz00);
			BGl_list2447z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2359z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2448z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_list2431z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BGl_symbol2437z00zz__r5_macro_4_3_syntaxz00,
							MAKE_YOUNG_PAIR(BGl_symbol2439z00zz__r5_macro_4_3_syntaxz00,
								MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00,
									BNIL))))));
			BGl_symbol2452z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2453z00zz__r5_macro_4_3_syntaxz00);
			BGl_list2455z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2448z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2442z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_list2454z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2455z00zz__r5_macro_4_3_syntaxz00, BNIL);
			BGl_list2451z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2452z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2454z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_symbol2448z00zz__r5_macro_4_3_syntaxz00, BNIL)));
			BGl_list2450z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2451z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2435z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00, BNIL)));
			BGl_list2446z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2447z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2450z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_list2428z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2429z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2446z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_symbol2456z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2457z00zz__r5_macro_4_3_syntaxz00);
			BGl_list2460z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2456z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BNIL,
					MAKE_YOUNG_PAIR(BGl_symbol2437z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BGl_symbol2439z00zz__r5_macro_4_3_syntaxz00,
							MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00,
								BNIL)))));
			BGl_list2461z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2359z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BNIL,
					MAKE_YOUNG_PAIR(BGl_symbol2437z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BGl_symbol2439z00zz__r5_macro_4_3_syntaxz00,
							MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00,
								BNIL)))));
			BGl_list2459z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2460z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2461z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_symbol2466z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2467z00zz__r5_macro_4_3_syntaxz00);
			BGl_symbol2468z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2469z00zz__r5_macro_4_3_syntaxz00);
			BGl_list2465z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2466z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2468z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_symbol2471z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2472z00zz__r5_macro_4_3_syntaxz00);
			BGl_symbol2473z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2474z00zz__r5_macro_4_3_syntaxz00);
			BGl_list2470z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2471z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2473z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_list2464z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2465z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2470z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00, BNIL)));
			BGl_list2463z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2456z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2464z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_symbol2437z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BGl_symbol2439z00zz__r5_macro_4_3_syntaxz00,
							MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00,
								BNIL)))));
			BGl_list2476z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2465z00zz__r5_macro_4_3_syntaxz00, BNIL);
			BGl_list2478z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2470z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_list2477z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2456z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2478z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_symbol2437z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BGl_symbol2439z00zz__r5_macro_4_3_syntaxz00,
							MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00,
								BNIL)))));
			BGl_list2475z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2359z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2476z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_list2477z00zz__r5_macro_4_3_syntaxz00, BNIL)));
			BGl_list2462z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2463z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2475z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_list2458z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2459z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2462z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_symbol2484z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2485z00zz__r5_macro_4_3_syntaxz00);
			BGl_symbol2486z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2487z00zz__r5_macro_4_3_syntaxz00);
			BGl_list2483z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2484z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2486z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_list2482z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2483z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_symbol2488z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2489z00zz__r5_macro_4_3_syntaxz00);
			BGl_list2481z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2452z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2482z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_symbol2488z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00,
							BNIL))));
			BGl_list2492z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2484z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_list2490z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2452z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_string2491z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_list2492z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BNIL,
							MAKE_YOUNG_PAIR(BGl_list2482z00zz__r5_macro_4_3_syntaxz00,
								MAKE_YOUNG_PAIR(BGl_symbol2488z00zz__r5_macro_4_3_syntaxz00,
									MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00,
										BNIL)))))));
			BGl_list2480z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2481z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2490z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_symbol2496z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2497z00zz__r5_macro_4_3_syntaxz00);
			BGl_list2495z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2496z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_list2494z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2452z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_string2491z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BNIL,
						MAKE_YOUNG_PAIR(BGl_list2495z00zz__r5_macro_4_3_syntaxz00,
							MAKE_YOUNG_PAIR(BGl_list2482z00zz__r5_macro_4_3_syntaxz00,
								MAKE_YOUNG_PAIR(BGl_symbol2488z00zz__r5_macro_4_3_syntaxz00,
									MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00,
										BNIL)))))));
			BGl_list2500z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2484z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BUNSPEC, BNIL));
			BGl_list2499z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2500z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_list2503z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2496z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2486z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_list2502z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2503z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_symbol2505z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2506z00zz__r5_macro_4_3_syntaxz00);
			BGl_list2504z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2505z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2484z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_symbol2496z00zz__r5_macro_4_3_syntaxz00, BNIL)));
			BGl_list2501z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2359z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2502z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_list2504z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00,
							MAKE_YOUNG_PAIR(BGl_symbol2488z00zz__r5_macro_4_3_syntaxz00,
								MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00,
									BNIL))))));
			BGl_list2498z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2359z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2499z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_list2501z00zz__r5_macro_4_3_syntaxz00, BNIL)));
			BGl_list2493z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2494z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2498z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_symbol2510z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2511z00zz__r5_macro_4_3_syntaxz00);
			BGl_symbol2512z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2513z00zz__r5_macro_4_3_syntaxz00);
			BGl_list2509z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2510z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2512z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00, BNIL)));
			BGl_list2514z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2363z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_list2508z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2452z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_string2491z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_list2509z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BGl_list2514z00zz__r5_macro_4_3_syntaxz00,
							MAKE_YOUNG_PAIR(BGl_list2482z00zz__r5_macro_4_3_syntaxz00,
								MAKE_YOUNG_PAIR(BGl_symbol2488z00zz__r5_macro_4_3_syntaxz00,
									MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00,
										BNIL)))))));
			BGl_list2516z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2512z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_symbol2518z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2519z00zz__r5_macro_4_3_syntaxz00);
			BGl_list2517z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2518z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2363z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00, BNIL)));
			BGl_list2515z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2452z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_string2491z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_list2516z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BGl_list2517z00zz__r5_macro_4_3_syntaxz00,
							MAKE_YOUNG_PAIR(BGl_list2482z00zz__r5_macro_4_3_syntaxz00,
								MAKE_YOUNG_PAIR(BGl_symbol2488z00zz__r5_macro_4_3_syntaxz00,
									MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00,
										BNIL)))))));
			BGl_list2507z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2508z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2515z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_list2479z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2480z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2493z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_list2507z00zz__r5_macro_4_3_syntaxz00, BNIL)));
			BGl_symbol2520z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2521z00zz__r5_macro_4_3_syntaxz00);
			BGl_symbol2527z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2528z00zz__r5_macro_4_3_syntaxz00);
			BGl_symbol2529z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2530z00zz__r5_macro_4_3_syntaxz00);
			BGl_symbol2531z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2532z00zz__r5_macro_4_3_syntaxz00);
			BGl_list2526z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2527z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2529z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_symbol2531z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00,
							BNIL))));
			BGl_list2525z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2526z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_symbol2534z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2535z00zz__r5_macro_4_3_syntaxz00);
			BGl_list2533z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2354z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2534z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00, BNIL)));
			BGl_symbol2536z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2537z00zz__r5_macro_4_3_syntaxz00);
			BGl_list2524z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2520z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2525z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_list2533z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BGl_symbol2536z00zz__r5_macro_4_3_syntaxz00,
							MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00,
								BNIL)))));
			BGl_symbol2541z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2542z00zz__r5_macro_4_3_syntaxz00);
			BGl_list2544z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2527z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_list2547z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2366z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BBOOL(((bool_t) 0)),
					MAKE_YOUNG_PAIR(BBOOL(((bool_t) 0)), BNIL)));
			BGl_list2546z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2349z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2547z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_symbol2534z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00,
							BNIL))));
			BGl_list2550z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2520z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_string2532z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_symbol2527z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BGl_symbol2531z00zz__r5_macro_4_3_syntaxz00,
							MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00,
								BNIL)))));
			BGl_list2549z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2541z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2550z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00, BNIL)));
			BGl_list2548z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2349z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2536z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BGl_list2549z00zz__r5_macro_4_3_syntaxz00, BNIL))));
			BGl_list2545z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2366z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2354z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_list2546z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BGl_list2548z00zz__r5_macro_4_3_syntaxz00, BNIL))));
			BGl_list2543z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2443z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2544z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_list2545z00zz__r5_macro_4_3_syntaxz00, BNIL)));
			BGl_list2540z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2541z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2543z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_list2539z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2540z00zz__r5_macro_4_3_syntaxz00, BNIL);
			BGl_list2551z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2541z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2529z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00, BNIL)));
			BGl_list2538z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2452z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2539z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_list2551z00zz__r5_macro_4_3_syntaxz00, BNIL)));
			BGl_list2523z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2524z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2538z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_list2553z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2520z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_string2532z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_symbol2510z00zz__r5_macro_4_3_syntaxz00, BNIL)));
			BGl_list2552z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2553z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2510z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_list2555z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2520z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_string2532z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_symbol2510z00zz__r5_macro_4_3_syntaxz00,
						MAKE_YOUNG_PAIR(BGl_symbol2512z00zz__r5_macro_4_3_syntaxz00,
							BNIL))));
			BGl_list2554z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2555z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_symbol2512z00zz__r5_macro_4_3_syntaxz00, BNIL));
			BGl_list2522z00zz__r5_macro_4_3_syntaxz00 =
				MAKE_YOUNG_PAIR(BGl_list2523z00zz__r5_macro_4_3_syntaxz00,
				MAKE_YOUNG_PAIR(BGl_list2552z00zz__r5_macro_4_3_syntaxz00,
					MAKE_YOUNG_PAIR(BGl_list2554z00zz__r5_macro_4_3_syntaxz00, BNIL)));
			BGl_symbol2558z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2559z00zz__r5_macro_4_3_syntaxz00);
			BGl_keyword2574z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_keyword(BGl_string2575z00zz__r5_macro_4_3_syntaxz00);
			return (BGl_symbol2576z00zz__r5_macro_4_3_syntaxz00 =
				bstring_to_symbol(BGl_string2577z00zz__r5_macro_4_3_syntaxz00),
				BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__r5_macro_4_3_syntaxz00(void)
	{
		{	/* R5rs/syntax.scm 17 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__r5_macro_4_3_syntaxz00(void)
	{
		{	/* R5rs/syntax.scm 17 */
			BGl_hygienezd2markzd2zz__r5_macro_4_3_syntaxz00 =
				BGl_gensymz00zz__r4_symbols_6_4z00
				(BGl_symbol2320z00zz__r5_macro_4_3_syntaxz00);
			{	/* R5rs/syntax.scm 60 */
				obj_t BgL_symbolz00_2555;

				BgL_symbolz00_2555 = BGl_hygienezd2markzd2zz__r5_macro_4_3_syntaxz00;
				{	/* R5rs/syntax.scm 60 */
					obj_t BgL_arg2119z00_2556;

					BgL_arg2119z00_2556 = SYMBOL_TO_STRING(BgL_symbolz00_2555);
					BGl_hygienezd2prefixzd2zz__r5_macro_4_3_syntaxz00 =
						BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg2119z00_2556);
			}}
			BGl_hygienezd2prefixzd2lenz00zz__r5_macro_4_3_syntaxz00 =
				STRING_LENGTH(BGl_hygienezd2prefixzd2zz__r5_macro_4_3_syntaxz00);
			BGl_syntaxesz00zz__r5_macro_4_3_syntaxz00 = BFALSE;
			{	/* R5rs/syntax.scm 64 */

				{	/* Llib/thread.scm 218 */
					obj_t BgL_namez00_1109;

					BgL_namez00_1109 =
						BGl_gensymz00zz__r4_symbols_6_4z00
						(BGl_symbol2322z00zz__r5_macro_4_3_syntaxz00);
					{	/* Llib/thread.scm 218 */

						BGl_syntaxzd2mutexzd2zz__r5_macro_4_3_syntaxz00 =
							bgl_make_mutex(BgL_namez00_1109);
			}}}
			{	/* R5rs/syntax.scm 65 */

				{	/* Llib/thread.scm 218 */
					obj_t BgL_namez00_1110;

					BgL_namez00_1110 =
						BGl_gensymz00zz__r4_symbols_6_4z00
						(BGl_symbol2322z00zz__r5_macro_4_3_syntaxz00);
					{	/* Llib/thread.scm 218 */

						return (BGl_syntaxzd2expanderszd2mutexz00zz__r5_macro_4_3_syntaxz00
							= bgl_make_mutex(BgL_namez00_1110), BUNSPEC);
					}
				}
			}
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zz__r5_macro_4_3_syntaxz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1111;

				BgL_headz00_1111 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_2573;
					obj_t BgL_tailz00_2574;

					BgL_prevz00_2573 = BgL_headz00_1111;
					BgL_tailz00_2574 = BgL_l1z00_1;
				BgL_loopz00_2572:
					if (PAIRP(BgL_tailz00_2574))
						{
							obj_t BgL_newzd2prevzd2_2580;

							BgL_newzd2prevzd2_2580 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_2574), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_2573, BgL_newzd2prevzd2_2580);
							{
								obj_t BgL_tailz00_4293;
								obj_t BgL_prevz00_4292;

								BgL_prevz00_4292 = BgL_newzd2prevzd2_2580;
								BgL_tailz00_4293 = CDR(BgL_tailz00_2574);
								BgL_tailz00_2574 = BgL_tailz00_4293;
								BgL_prevz00_2573 = BgL_prevz00_4292;
								goto BgL_loopz00_2572;
							}
						}
					else
						{
							BNIL;
						}
				}
				return CDR(BgL_headz00_1111);
			}
		}

	}



/* install-syntax-expander */
	BGL_EXPORTED_DEF obj_t
		BGl_installzd2syntaxzd2expanderz00zz__r5_macro_4_3_syntaxz00(obj_t
		BgL_keywordz00_4, obj_t BgL_expanderz00_5)
	{
		{	/* R5rs/syntax.scm 79 */
			BGL_MUTEX_LOCK(BGl_syntaxzd2mutexzd2zz__r5_macro_4_3_syntaxz00);
			{	/* R5rs/syntax.scm 80 */
				obj_t BgL_tmp2664z00_4296;

				{	/* R5rs/syntax.scm 81 */
					obj_t BgL_arg1306z00_2591;

					BgL_arg1306z00_2591 =
						MAKE_YOUNG_PAIR(BgL_keywordz00_4, BgL_expanderz00_5);
					BgL_tmp2664z00_4296 = (BGl_syntaxesz00zz__r5_macro_4_3_syntaxz00 =
						MAKE_YOUNG_PAIR(BgL_arg1306z00_2591,
							BGl_syntaxesz00zz__r5_macro_4_3_syntaxz00), BUNSPEC);
				}
				BGL_MUTEX_UNLOCK(BGl_syntaxzd2mutexzd2zz__r5_macro_4_3_syntaxz00);
				return BgL_tmp2664z00_4296;
			}
		}

	}



/* &install-syntax-expander */
	obj_t BGl_z62installzd2syntaxzd2expanderz62zz__r5_macro_4_3_syntaxz00(obj_t
		BgL_envz00_3685, obj_t BgL_keywordz00_3686, obj_t BgL_expanderz00_3687)
	{
		{	/* R5rs/syntax.scm 79 */
			{	/* R5rs/syntax.scm 80 */
				obj_t BgL_auxz00_4308;
				obj_t BgL_auxz00_4301;

				if (PROCEDUREP(BgL_expanderz00_3687))
					{	/* R5rs/syntax.scm 80 */
						BgL_auxz00_4308 = BgL_expanderz00_3687;
					}
				else
					{
						obj_t BgL_auxz00_4311;

						BgL_auxz00_4311 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2324z00zz__r5_macro_4_3_syntaxz00, BINT(3110L),
							BGl_string2325z00zz__r5_macro_4_3_syntaxz00,
							BGl_string2327z00zz__r5_macro_4_3_syntaxz00,
							BgL_expanderz00_3687);
						FAILURE(BgL_auxz00_4311, BFALSE, BFALSE);
					}
				if (SYMBOLP(BgL_keywordz00_3686))
					{	/* R5rs/syntax.scm 80 */
						BgL_auxz00_4301 = BgL_keywordz00_3686;
					}
				else
					{
						obj_t BgL_auxz00_4304;

						BgL_auxz00_4304 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2324z00zz__r5_macro_4_3_syntaxz00, BINT(3110L),
							BGl_string2325z00zz__r5_macro_4_3_syntaxz00,
							BGl_string2326z00zz__r5_macro_4_3_syntaxz00, BgL_keywordz00_3686);
						FAILURE(BgL_auxz00_4304, BFALSE, BFALSE);
					}
				return
					BGl_installzd2syntaxzd2expanderz00zz__r5_macro_4_3_syntaxz00
					(BgL_auxz00_4301, BgL_auxz00_4308);
			}
		}

	}



/* init-syntax-expanders! */
	obj_t BGl_initzd2syntaxzd2expandersz12z12zz__r5_macro_4_3_syntaxz00(void)
	{
		{	/* R5rs/syntax.scm 86 */
			{	/* R5rs/syntax.scm 91 */
				obj_t BgL_top2668z00_4317;

				BgL_top2668z00_4317 = BGL_EXITD_TOP_AS_OBJ();
				BGL_MUTEX_LOCK
					(BGl_syntaxzd2expanderszd2mutexz00zz__r5_macro_4_3_syntaxz00);
				BGL_EXITD_PUSH_PROTECT(BgL_top2668z00_4317,
					BGl_syntaxzd2expanderszd2mutexz00zz__r5_macro_4_3_syntaxz00);
				BUNSPEC;
				{	/* R5rs/syntax.scm 91 */
					obj_t BgL_tmp2667z00_4316;

					if (CBOOL(BGl_syntaxesz00zz__r5_macro_4_3_syntaxz00))
						{	/* R5rs/syntax.scm 92 */
							BgL_tmp2667z00_4316 = BFALSE;
						}
					else
						{	/* R5rs/syntax.scm 92 */
							BGl_syntaxesz00zz__r5_macro_4_3_syntaxz00 = BNIL;
							{	/* R5rs/syntax.scm 95 */
								obj_t BgL_keywordz00_2596;

								BgL_keywordz00_2596 =
									BGl_symbol2329z00zz__r5_macro_4_3_syntaxz00;
								BGL_MUTEX_LOCK(BGl_syntaxzd2mutexzd2zz__r5_macro_4_3_syntaxz00);
								{	/* R5rs/syntax.scm 80 */
									obj_t BgL_tmp2670z00_4323;

									{	/* R5rs/syntax.scm 81 */
										obj_t BgL_arg1306z00_2597;

										BgL_arg1306z00_2597 =
											MAKE_YOUNG_PAIR(BgL_keywordz00_2596,
											BGl_proc2328z00zz__r5_macro_4_3_syntaxz00);
										BgL_tmp2670z00_4323 =
											(BGl_syntaxesz00zz__r5_macro_4_3_syntaxz00 =
											MAKE_YOUNG_PAIR(BgL_arg1306z00_2597,
												BGl_syntaxesz00zz__r5_macro_4_3_syntaxz00), BUNSPEC);
									}
									BGL_MUTEX_UNLOCK
										(BGl_syntaxzd2mutexzd2zz__r5_macro_4_3_syntaxz00);
									BgL_tmp2670z00_4323;
								}
							}
							{	/* R5rs/syntax.scm 97 */
								obj_t BgL_idz00_2598;
								obj_t BgL_literalsz00_2599;
								obj_t BgL_rulesz00_2600;

								BgL_idz00_2598 = BGl_symbol2331z00zz__r5_macro_4_3_syntaxz00;
								BgL_literalsz00_2599 =
									BGl_list2333z00zz__r5_macro_4_3_syntaxz00;
								BgL_rulesz00_2600 = BGl_list2338z00zz__r5_macro_4_3_syntaxz00;
								{	/* R5rs/syntax.scm 89 */
									obj_t BgL_arg1310z00_2601;

									BgL_arg1310z00_2601 =
										BGl_syntaxzd2ruleszd2ze3expanderze3zz__r5_macro_4_3_syntaxz00
										(BgL_idz00_2598, BgL_literalsz00_2599, BgL_rulesz00_2600);
									BGL_MUTEX_LOCK
										(BGl_syntaxzd2mutexzd2zz__r5_macro_4_3_syntaxz00);
									{	/* R5rs/syntax.scm 80 */
										obj_t BgL_tmp2671z00_4329;

										{	/* R5rs/syntax.scm 81 */
											obj_t BgL_arg1306z00_2604;

											BgL_arg1306z00_2604 =
												MAKE_YOUNG_PAIR(BgL_idz00_2598,
												((obj_t) BgL_arg1310z00_2601));
											BgL_tmp2671z00_4329 =
												(BGl_syntaxesz00zz__r5_macro_4_3_syntaxz00 =
												MAKE_YOUNG_PAIR(BgL_arg1306z00_2604,
													BGl_syntaxesz00zz__r5_macro_4_3_syntaxz00), BUNSPEC);
										}
										BGL_MUTEX_UNLOCK
											(BGl_syntaxzd2mutexzd2zz__r5_macro_4_3_syntaxz00);
										BgL_tmp2671z00_4329;
									}
								}
							}
							{	/* R5rs/syntax.scm 123 */
								obj_t BgL_idz00_2605;
								obj_t BgL_literalsz00_2606;
								obj_t BgL_rulesz00_2607;

								BgL_idz00_2605 = BGl_symbol2392z00zz__r5_macro_4_3_syntaxz00;
								BgL_literalsz00_2606 =
									BGl_list2394z00zz__r5_macro_4_3_syntaxz00;
								BgL_rulesz00_2607 = BGl_list2395z00zz__r5_macro_4_3_syntaxz00;
								{	/* R5rs/syntax.scm 89 */
									obj_t BgL_arg1310z00_2608;

									BgL_arg1310z00_2608 =
										BGl_syntaxzd2ruleszd2ze3expanderze3zz__r5_macro_4_3_syntaxz00
										(BgL_idz00_2605, BgL_literalsz00_2606, BgL_rulesz00_2607);
									BGL_MUTEX_LOCK
										(BGl_syntaxzd2mutexzd2zz__r5_macro_4_3_syntaxz00);
									{	/* R5rs/syntax.scm 80 */
										obj_t BgL_tmp2672z00_4336;

										{	/* R5rs/syntax.scm 81 */
											obj_t BgL_arg1306z00_2611;

											BgL_arg1306z00_2611 =
												MAKE_YOUNG_PAIR(BgL_idz00_2605,
												((obj_t) BgL_arg1310z00_2608));
											BgL_tmp2672z00_4336 =
												(BGl_syntaxesz00zz__r5_macro_4_3_syntaxz00 =
												MAKE_YOUNG_PAIR(BgL_arg1306z00_2611,
													BGl_syntaxesz00zz__r5_macro_4_3_syntaxz00), BUNSPEC);
										}
										BGL_MUTEX_UNLOCK
											(BGl_syntaxzd2mutexzd2zz__r5_macro_4_3_syntaxz00);
										BgL_tmp2672z00_4336;
									}
								}
							}
							{	/* R5rs/syntax.scm 143 */
								obj_t BgL_idz00_2612;
								obj_t BgL_rulesz00_2613;

								BgL_idz00_2612 = BGl_symbol2359z00zz__r5_macro_4_3_syntaxz00;
								BgL_rulesz00_2613 = BGl_list2428z00zz__r5_macro_4_3_syntaxz00;
								{	/* R5rs/syntax.scm 89 */
									obj_t BgL_arg1310z00_2614;

									BgL_arg1310z00_2614 =
										BGl_syntaxzd2ruleszd2ze3expanderze3zz__r5_macro_4_3_syntaxz00
										(BgL_idz00_2612, BNIL, BgL_rulesz00_2613);
									BGL_MUTEX_LOCK
										(BGl_syntaxzd2mutexzd2zz__r5_macro_4_3_syntaxz00);
									{	/* R5rs/syntax.scm 80 */
										obj_t BgL_tmp2673z00_4343;

										{	/* R5rs/syntax.scm 81 */
											obj_t BgL_arg1306z00_2617;

											BgL_arg1306z00_2617 =
												MAKE_YOUNG_PAIR(BgL_idz00_2612,
												((obj_t) BgL_arg1310z00_2614));
											BgL_tmp2673z00_4343 =
												(BGl_syntaxesz00zz__r5_macro_4_3_syntaxz00 =
												MAKE_YOUNG_PAIR(BgL_arg1306z00_2617,
													BGl_syntaxesz00zz__r5_macro_4_3_syntaxz00), BUNSPEC);
										}
										BGL_MUTEX_UNLOCK
											(BGl_syntaxzd2mutexzd2zz__r5_macro_4_3_syntaxz00);
										BgL_tmp2673z00_4343;
									}
								}
							}
							{	/* R5rs/syntax.scm 154 */
								obj_t BgL_idz00_2618;
								obj_t BgL_rulesz00_2619;

								BgL_idz00_2618 = BGl_symbol2456z00zz__r5_macro_4_3_syntaxz00;
								BgL_rulesz00_2619 = BGl_list2458z00zz__r5_macro_4_3_syntaxz00;
								{	/* R5rs/syntax.scm 89 */
									obj_t BgL_arg1310z00_2620;

									BgL_arg1310z00_2620 =
										BGl_syntaxzd2ruleszd2ze3expanderze3zz__r5_macro_4_3_syntaxz00
										(BgL_idz00_2618, BNIL, BgL_rulesz00_2619);
									BGL_MUTEX_LOCK
										(BGl_syntaxzd2mutexzd2zz__r5_macro_4_3_syntaxz00);
									{	/* R5rs/syntax.scm 80 */
										obj_t BgL_tmp2674z00_4350;

										{	/* R5rs/syntax.scm 81 */
											obj_t BgL_arg1306z00_2623;

											BgL_arg1306z00_2623 =
												MAKE_YOUNG_PAIR(BgL_idz00_2618,
												((obj_t) BgL_arg1310z00_2620));
											BgL_tmp2674z00_4350 =
												(BGl_syntaxesz00zz__r5_macro_4_3_syntaxz00 =
												MAKE_YOUNG_PAIR(BgL_arg1306z00_2623,
													BGl_syntaxesz00zz__r5_macro_4_3_syntaxz00), BUNSPEC);
										}
										BGL_MUTEX_UNLOCK
											(BGl_syntaxzd2mutexzd2zz__r5_macro_4_3_syntaxz00);
										BgL_tmp2674z00_4350;
									}
								}
							}
							{	/* R5rs/syntax.scm 164 */
								obj_t BgL_idz00_2624;
								obj_t BgL_rulesz00_2625;

								BgL_idz00_2624 = BGl_symbol2452z00zz__r5_macro_4_3_syntaxz00;
								BgL_rulesz00_2625 = BGl_list2479z00zz__r5_macro_4_3_syntaxz00;
								{	/* R5rs/syntax.scm 89 */
									obj_t BgL_arg1310z00_2626;

									BgL_arg1310z00_2626 =
										BGl_syntaxzd2ruleszd2ze3expanderze3zz__r5_macro_4_3_syntaxz00
										(BgL_idz00_2624, BNIL, BgL_rulesz00_2625);
									BGL_MUTEX_LOCK
										(BGl_syntaxzd2mutexzd2zz__r5_macro_4_3_syntaxz00);
									{	/* R5rs/syntax.scm 80 */
										obj_t BgL_tmp2675z00_4357;

										{	/* R5rs/syntax.scm 81 */
											obj_t BgL_arg1306z00_2629;

											BgL_arg1306z00_2629 =
												MAKE_YOUNG_PAIR(BgL_idz00_2624,
												((obj_t) BgL_arg1310z00_2626));
											BgL_tmp2675z00_4357 =
												(BGl_syntaxesz00zz__r5_macro_4_3_syntaxz00 =
												MAKE_YOUNG_PAIR(BgL_arg1306z00_2629,
													BGl_syntaxesz00zz__r5_macro_4_3_syntaxz00), BUNSPEC);
										}
										BGL_MUTEX_UNLOCK
											(BGl_syntaxzd2mutexzd2zz__r5_macro_4_3_syntaxz00);
										BgL_tmp2675z00_4357;
									}
								}
							}
							{	/* R5rs/syntax.scm 193 */
								obj_t BgL_idz00_2630;
								obj_t BgL_rulesz00_2631;

								BgL_idz00_2630 = BGl_symbol2520z00zz__r5_macro_4_3_syntaxz00;
								BgL_rulesz00_2631 = BGl_list2522z00zz__r5_macro_4_3_syntaxz00;
								{	/* R5rs/syntax.scm 89 */
									obj_t BgL_arg1310z00_2632;

									BgL_arg1310z00_2632 =
										BGl_syntaxzd2ruleszd2ze3expanderze3zz__r5_macro_4_3_syntaxz00
										(BgL_idz00_2630, BNIL, BgL_rulesz00_2631);
									BGL_MUTEX_LOCK
										(BGl_syntaxzd2mutexzd2zz__r5_macro_4_3_syntaxz00);
									{	/* R5rs/syntax.scm 80 */
										obj_t BgL_tmp2676z00_4364;

										{	/* R5rs/syntax.scm 81 */
											obj_t BgL_arg1306z00_2635;

											BgL_arg1306z00_2635 =
												MAKE_YOUNG_PAIR(BgL_idz00_2630,
												((obj_t) BgL_arg1310z00_2632));
											BgL_tmp2676z00_4364 =
												(BGl_syntaxesz00zz__r5_macro_4_3_syntaxz00 =
												MAKE_YOUNG_PAIR(BgL_arg1306z00_2635,
													BGl_syntaxesz00zz__r5_macro_4_3_syntaxz00), BUNSPEC);
										}
										BGL_MUTEX_UNLOCK
											(BGl_syntaxzd2mutexzd2zz__r5_macro_4_3_syntaxz00);
										BgL_tmp2667z00_4316 = BgL_tmp2676z00_4364;
									}
								}
							}
						}
					BGL_EXITD_POP_PROTECT(BgL_top2668z00_4317);
					BUNSPEC;
					BGL_MUTEX_UNLOCK
						(BGl_syntaxzd2expanderszd2mutexz00zz__r5_macro_4_3_syntaxz00);
					return BgL_tmp2667z00_4316;
				}
			}
		}

	}



/* &<@anonymous:1308> */
	obj_t BGl_z62zc3z04anonymousza31308ze3ze5zz__r5_macro_4_3_syntaxz00(obj_t
		BgL_envz00_3689, obj_t BgL_xz00_3690, obj_t BgL_ez00_3691)
	{
		{	/* R5rs/syntax.scm 95 */
			return BgL_xz00_3690;
		}

	}



/* syntax-expander */
	obj_t BGl_syntaxzd2expanderzd2zz__r5_macro_4_3_syntaxz00(obj_t BgL_xz00_7,
		obj_t BgL_ez00_8)
	{
		{	/* R5rs/syntax.scm 225 */
			{	/* R5rs/syntax.scm 226 */
				obj_t BgL_e1z00_1135;

				if (PAIRP(BgL_xz00_7))
					{	/* R5rs/syntax.scm 229 */
						obj_t BgL_g1039z00_1146;

						{	/* R5rs/syntax.scm 229 */
							obj_t BgL_arg1327z00_1162;

							BgL_arg1327z00_1162 = CAR(BgL_xz00_7);
							{	/* R5rs/syntax.scm 71 */
								obj_t BgL_idz00_2637;

								BgL_idz00_2637 =
									BGl_hygienezd2valueza2z70zz__r5_macro_4_3_syntaxz00
									(BgL_arg1327z00_1162);
								{	/* R5rs/syntax.scm 71 */
									obj_t BgL_cz00_2638;

									{	/* R5rs/syntax.scm 72 */
										obj_t BgL_top2679z00_4377;

										BgL_top2679z00_4377 = BGL_EXITD_TOP_AS_OBJ();
										BGL_MUTEX_LOCK
											(BGl_syntaxzd2mutexzd2zz__r5_macro_4_3_syntaxz00);
										BGL_EXITD_PUSH_PROTECT(BgL_top2679z00_4377,
											BGl_syntaxzd2mutexzd2zz__r5_macro_4_3_syntaxz00);
										BUNSPEC;
										{	/* R5rs/syntax.scm 72 */
											obj_t BgL_tmp2678z00_4376;

											BgL_tmp2678z00_4376 =
												BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_idz00_2637,
												BGl_syntaxesz00zz__r5_macro_4_3_syntaxz00);
											BGL_EXITD_POP_PROTECT(BgL_top2679z00_4377);
											BUNSPEC;
											BGL_MUTEX_UNLOCK
												(BGl_syntaxzd2mutexzd2zz__r5_macro_4_3_syntaxz00);
											BgL_cz00_2638 = BgL_tmp2678z00_4376;
										}
									}
									{	/* R5rs/syntax.scm 72 */

										if (PAIRP(BgL_cz00_2638))
											{	/* R5rs/syntax.scm 73 */
												BgL_g1039z00_1146 = CDR(BgL_cz00_2638);
											}
										else
											{	/* R5rs/syntax.scm 73 */
												BgL_g1039z00_1146 = BFALSE;
											}
									}
								}
							}
						}
						if (CBOOL(BgL_g1039z00_1146))
							{	/* R5rs/syntax.scm 229 */
								BgL_e1z00_1135 = BgL_g1039z00_1146;
							}
						else
							{	/* R5rs/syntax.scm 229 */
								BgL_e1z00_1135 = BGl_proc2556z00zz__r5_macro_4_3_syntaxz00;
							}
					}
				else
					{	/* R5rs/syntax.scm 227 */
						BgL_e1z00_1135 = BGl_proc2557z00zz__r5_macro_4_3_syntaxz00;
					}
				{	/* R5rs/syntax.scm 242 */
					obj_t BgL_newz00_1136;

					BgL_newz00_1136 =
						BGL_PROCEDURE_CALL2(BgL_e1z00_1135, BgL_xz00_7, BgL_ez00_8);
					{	/* R5rs/syntax.scm 243 */
						bool_t BgL_test2682z00_4394;

						if (PAIRP(BgL_newz00_1136))
							{	/* R5rs/syntax.scm 243 */
								if (EPAIRP(BgL_newz00_1136))
									{	/* R5rs/syntax.scm 243 */
										BgL_test2682z00_4394 = ((bool_t) 0);
									}
								else
									{	/* R5rs/syntax.scm 243 */
										BgL_test2682z00_4394 = EPAIRP(BgL_xz00_7);
									}
							}
						else
							{	/* R5rs/syntax.scm 243 */
								BgL_test2682z00_4394 = ((bool_t) 0);
							}
						if (BgL_test2682z00_4394)
							{	/* R5rs/syntax.scm 244 */
								obj_t BgL_arg1314z00_1140;
								obj_t BgL_arg1315z00_1141;
								obj_t BgL_arg1316z00_1142;

								BgL_arg1314z00_1140 = CAR(BgL_newz00_1136);
								BgL_arg1315z00_1141 = CDR(BgL_newz00_1136);
								BgL_arg1316z00_1142 = CER(((obj_t) BgL_xz00_7));
								{	/* R5rs/syntax.scm 244 */
									obj_t BgL_res2295z00_2646;

									BgL_res2295z00_2646 =
										MAKE_YOUNG_EPAIR(BgL_arg1314z00_1140, BgL_arg1315z00_1141,
										BgL_arg1316z00_1142);
									return BgL_res2295z00_2646;
								}
							}
						else
							{	/* R5rs/syntax.scm 243 */
								return BgL_newz00_1136;
							}
					}
				}
			}
		}

	}



/* &syntax-expander */
	obj_t BGl_z62syntaxzd2expanderzb0zz__r5_macro_4_3_syntaxz00(obj_t
		BgL_envz00_3694, obj_t BgL_xz00_3695, obj_t BgL_ez00_3696)
	{
		{	/* R5rs/syntax.scm 225 */
			return
				BGl_syntaxzd2expanderzd2zz__r5_macro_4_3_syntaxz00(BgL_xz00_3695,
				BgL_ez00_3696);
		}

	}



/* &<@anonymous:1318> */
	obj_t BGl_z62zc3z04anonymousza31318ze3ze5zz__r5_macro_4_3_syntaxz00(obj_t
		BgL_envz00_3697, obj_t BgL_xz00_3698, obj_t BgL_ez00_3699)
	{
		{	/* R5rs/syntax.scm 233 */
			return
				BGl_loopze73ze7zz__r5_macro_4_3_syntaxz00(BgL_ez00_3699, BgL_xz00_3698);
		}

	}



/* loop~3 */
	obj_t BGl_loopze73ze7zz__r5_macro_4_3_syntaxz00(obj_t BgL_ez00_3742,
		obj_t BgL_xz00_1153)
	{
		{	/* R5rs/syntax.scm 234 */
			if (PAIRP(BgL_xz00_1153))
				{	/* R5rs/syntax.scm 237 */
					obj_t BgL_arg1321z00_1156;
					obj_t BgL_arg1322z00_1157;

					{	/* R5rs/syntax.scm 237 */
						obj_t BgL_arg1323z00_1158;

						BgL_arg1323z00_1158 = CAR(BgL_xz00_1153);
						BgL_arg1321z00_1156 =
							BGL_PROCEDURE_CALL2(BgL_ez00_3742, BgL_arg1323z00_1158,
							BgL_ez00_3742);
					}
					BgL_arg1322z00_1157 =
						BGl_loopze73ze7zz__r5_macro_4_3_syntaxz00(BgL_ez00_3742,
						CDR(BgL_xz00_1153));
					return MAKE_YOUNG_PAIR(BgL_arg1321z00_1156, BgL_arg1322z00_1157);
				}
			else
				{	/* R5rs/syntax.scm 236 */
					if (NULLP(BgL_xz00_1153))
						{	/* R5rs/syntax.scm 238 */
							return BNIL;
						}
					else
						{	/* R5rs/syntax.scm 238 */
							return
								BGL_PROCEDURE_CALL2(BgL_ez00_3742, BgL_xz00_1153,
								BgL_ez00_3742);
						}
				}
		}

	}



/* &<@anonymous:1328> */
	obj_t BGl_z62zc3z04anonymousza31328ze3ze5zz__r5_macro_4_3_syntaxz00(obj_t
		BgL_envz00_3700, obj_t BgL_xz00_3701, obj_t BgL_ez00_3702)
	{
		{	/* R5rs/syntax.scm 228 */
			return BgL_xz00_3701;
		}

	}



/* expand-define-syntax */
	BGL_EXPORTED_DEF obj_t
		BGl_expandzd2definezd2syntaxz00zz__r5_macro_4_3_syntaxz00(obj_t BgL_xz00_9,
		obj_t BgL_ez00_10)
	{
		{	/* R5rs/syntax.scm 250 */
			if (PAIRP(BgL_xz00_9))
				{	/* R5rs/syntax.scm 251 */
					obj_t BgL_cdrzd2111zd2_1173;

					BgL_cdrzd2111zd2_1173 = CDR(((obj_t) BgL_xz00_9));
					if (PAIRP(BgL_cdrzd2111zd2_1173))
						{	/* R5rs/syntax.scm 251 */
							obj_t BgL_carzd2115zd2_1175;
							obj_t BgL_cdrzd2116zd2_1176;

							BgL_carzd2115zd2_1175 = CAR(BgL_cdrzd2111zd2_1173);
							BgL_cdrzd2116zd2_1176 = CDR(BgL_cdrzd2111zd2_1173);
							if (SYMBOLP(BgL_carzd2115zd2_1175))
								{	/* R5rs/syntax.scm 251 */
									if (PAIRP(BgL_cdrzd2116zd2_1176))
										{	/* R5rs/syntax.scm 251 */
											obj_t BgL_carzd2122zd2_1179;

											BgL_carzd2122zd2_1179 = CAR(BgL_cdrzd2116zd2_1176);
											if (PAIRP(BgL_carzd2122zd2_1179))
												{	/* R5rs/syntax.scm 251 */
													obj_t BgL_cdrzd2127zd2_1181;

													BgL_cdrzd2127zd2_1181 = CDR(BgL_carzd2122zd2_1179);
													if (
														(CAR(BgL_carzd2122zd2_1179) ==
															BGl_symbol2558z00zz__r5_macro_4_3_syntaxz00))
														{	/* R5rs/syntax.scm 251 */
															if (PAIRP(BgL_cdrzd2127zd2_1181))
																{	/* R5rs/syntax.scm 251 */
																	if (NULLP(CDR(BgL_cdrzd2116zd2_1176)))
																		{	/* R5rs/syntax.scm 251 */
																			obj_t BgL_arg1339z00_1187;
																			obj_t BgL_arg1340z00_1188;

																			BgL_arg1339z00_1187 =
																				CAR(BgL_cdrzd2127zd2_1181);
																			BgL_arg1340z00_1188 =
																				CDR(BgL_cdrzd2127zd2_1181);
																			{	/* R5rs/syntax.scm 253 */
																				obj_t BgL_exz00_2660;

																				BGl_initzd2syntaxzd2expandersz12z12zz__r5_macro_4_3_syntaxz00
																					();
																				BgL_exz00_2660 =
																					BGl_syntaxzd2ruleszd2ze3expanderze3zz__r5_macro_4_3_syntaxz00
																					(BgL_carzd2115zd2_1175,
																					BgL_arg1339z00_1187,
																					BgL_arg1340z00_1188);
																				BGL_MUTEX_LOCK
																					(BGl_syntaxzd2mutexzd2zz__r5_macro_4_3_syntaxz00);
																				{	/* R5rs/syntax.scm 80 */
																					obj_t BgL_tmp2695z00_4453;

																					{	/* R5rs/syntax.scm 81 */
																						obj_t BgL_arg1306z00_2663;

																						BgL_arg1306z00_2663 =
																							MAKE_YOUNG_PAIR
																							(BgL_carzd2115zd2_1175,
																							((obj_t) BgL_exz00_2660));
																						BgL_tmp2695z00_4453 =
																							(BGl_syntaxesz00zz__r5_macro_4_3_syntaxz00
																							=
																							MAKE_YOUNG_PAIR
																							(BgL_arg1306z00_2663,
																								BGl_syntaxesz00zz__r5_macro_4_3_syntaxz00),
																							BUNSPEC);
																					}
																					BGL_MUTEX_UNLOCK
																						(BGl_syntaxzd2mutexzd2zz__r5_macro_4_3_syntaxz00);
																					BgL_tmp2695z00_4453;
																				}
																				BGl_installzd2expanderzd2zz__macroz00
																					(BgL_carzd2115zd2_1175,
																					BgL_exz00_2660);
																				return BUNSPEC;
																			}
																		}
																	else
																		{	/* R5rs/syntax.scm 251 */
																			return
																				BGl_errorz00zz__errorz00
																				(BGl_string2560z00zz__r5_macro_4_3_syntaxz00,
																				BGl_string2561z00zz__r5_macro_4_3_syntaxz00,
																				BgL_xz00_9);
																		}
																}
															else
																{	/* R5rs/syntax.scm 251 */
																	return
																		BGl_errorz00zz__errorz00
																		(BGl_string2560z00zz__r5_macro_4_3_syntaxz00,
																		BGl_string2561z00zz__r5_macro_4_3_syntaxz00,
																		BgL_xz00_9);
																}
														}
													else
														{	/* R5rs/syntax.scm 251 */
															return
																BGl_errorz00zz__errorz00
																(BGl_string2560z00zz__r5_macro_4_3_syntaxz00,
																BGl_string2561z00zz__r5_macro_4_3_syntaxz00,
																BgL_xz00_9);
														}
												}
											else
												{	/* R5rs/syntax.scm 251 */
													return
														BGl_errorz00zz__errorz00
														(BGl_string2560z00zz__r5_macro_4_3_syntaxz00,
														BGl_string2561z00zz__r5_macro_4_3_syntaxz00,
														BgL_xz00_9);
												}
										}
									else
										{	/* R5rs/syntax.scm 251 */
											return
												BGl_errorz00zz__errorz00
												(BGl_string2560z00zz__r5_macro_4_3_syntaxz00,
												BGl_string2561z00zz__r5_macro_4_3_syntaxz00,
												BgL_xz00_9);
										}
								}
							else
								{	/* R5rs/syntax.scm 251 */
									return
										BGl_errorz00zz__errorz00
										(BGl_string2560z00zz__r5_macro_4_3_syntaxz00,
										BGl_string2561z00zz__r5_macro_4_3_syntaxz00, BgL_xz00_9);
								}
						}
					else
						{	/* R5rs/syntax.scm 251 */
							return
								BGl_errorz00zz__errorz00
								(BGl_string2560z00zz__r5_macro_4_3_syntaxz00,
								BGl_string2561z00zz__r5_macro_4_3_syntaxz00, BgL_xz00_9);
						}
				}
			else
				{	/* R5rs/syntax.scm 251 */
					return
						BGl_errorz00zz__errorz00
						(BGl_string2560z00zz__r5_macro_4_3_syntaxz00,
						BGl_string2561z00zz__r5_macro_4_3_syntaxz00, BgL_xz00_9);
				}
		}

	}



/* &expand-define-syntax */
	obj_t BGl_z62expandzd2definezd2syntaxz62zz__r5_macro_4_3_syntaxz00(obj_t
		BgL_envz00_3703, obj_t BgL_xz00_3704, obj_t BgL_ez00_3705)
	{
		{	/* R5rs/syntax.scm 250 */
			{	/* R5rs/syntax.scm 251 */
				obj_t BgL_auxz00_4468;

				if (PROCEDUREP(BgL_ez00_3705))
					{	/* R5rs/syntax.scm 251 */
						BgL_auxz00_4468 = BgL_ez00_3705;
					}
				else
					{
						obj_t BgL_auxz00_4471;

						BgL_auxz00_4471 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2324z00zz__r5_macro_4_3_syntaxz00, BINT(7966L),
							BGl_string2562z00zz__r5_macro_4_3_syntaxz00,
							BGl_string2327z00zz__r5_macro_4_3_syntaxz00, BgL_ez00_3705);
						FAILURE(BgL_auxz00_4471, BFALSE, BFALSE);
					}
				return
					BGl_expandzd2definezd2syntaxz00zz__r5_macro_4_3_syntaxz00
					(BgL_xz00_3704, BgL_auxz00_4468);
			}
		}

	}



/* expand-letrec-syntax */
	BGL_EXPORTED_DEF obj_t
		BGl_expandzd2letreczd2syntaxz00zz__r5_macro_4_3_syntaxz00(obj_t BgL_xz00_11,
		obj_t BgL_ez00_12)
	{
		{	/* R5rs/syntax.scm 263 */
			{
				obj_t BgL_bsz00_1192;
				obj_t BgL_bodyz00_1193;

				if (PAIRP(BgL_xz00_11))
					{	/* R5rs/syntax.scm 264 */
						obj_t BgL_cdrzd2142zd2_1198;

						BgL_cdrzd2142zd2_1198 = CDR(((obj_t) BgL_xz00_11));
						if (PAIRP(BgL_cdrzd2142zd2_1198))
							{	/* R5rs/syntax.scm 264 */
								BgL_bsz00_1192 = CAR(BgL_cdrzd2142zd2_1198);
								BgL_bodyz00_1193 = CDR(BgL_cdrzd2142zd2_1198);
								{	/* R5rs/syntax.scm 266 */
									obj_t BgL_e1z00_1202;

									BgL_e1z00_1202 =
										BGl_loopze72ze7zz__r5_macro_4_3_syntaxz00(BgL_ez00_12,
										BgL_bsz00_1192);
									{	/* R5rs/syntax.scm 280 */
										obj_t BgL_arg1348z00_1203;

										{	/* R5rs/syntax.scm 280 */
											obj_t BgL_arg1349z00_1204;

											if (NULLP(BgL_bodyz00_1193))
												{	/* R5rs/syntax.scm 280 */
													BgL_arg1349z00_1204 = BNIL;
												}
											else
												{	/* R5rs/syntax.scm 280 */
													obj_t BgL_head1095z00_1207;

													BgL_head1095z00_1207 = MAKE_YOUNG_PAIR(BNIL, BNIL);
													{
														obj_t BgL_l1093z00_1209;
														obj_t BgL_tail1096z00_1210;

														BgL_l1093z00_1209 = BgL_bodyz00_1193;
														BgL_tail1096z00_1210 = BgL_head1095z00_1207;
													BgL_zc3z04anonymousza31351ze3z87_1211:
														if (NULLP(BgL_l1093z00_1209))
															{	/* R5rs/syntax.scm 280 */
																BgL_arg1349z00_1204 = CDR(BgL_head1095z00_1207);
															}
														else
															{	/* R5rs/syntax.scm 280 */
																obj_t BgL_newtail1097z00_1213;

																{	/* R5rs/syntax.scm 280 */
																	obj_t BgL_arg1356z00_1215;

																	{	/* R5rs/syntax.scm 280 */
																		obj_t BgL_xz00_1216;

																		BgL_xz00_1216 =
																			CAR(((obj_t) BgL_l1093z00_1209));
																		{	/* R5rs/syntax.scm 280 */
																			obj_t BgL_arg1357z00_1217;

																			BgL_arg1357z00_1217 =
																				BGl_hygieniza7eza7zz__r5_macro_4_3_syntaxz00
																				(BgL_xz00_1216, BNIL);
																			BgL_arg1356z00_1215 =
																				BGL_PROCEDURE_CALL2(BgL_e1z00_1202,
																				BgL_arg1357z00_1217, BgL_e1z00_1202);
																		}
																	}
																	BgL_newtail1097z00_1213 =
																		MAKE_YOUNG_PAIR(BgL_arg1356z00_1215, BNIL);
																}
																SET_CDR(BgL_tail1096z00_1210,
																	BgL_newtail1097z00_1213);
																{	/* R5rs/syntax.scm 280 */
																	obj_t BgL_arg1354z00_1214;

																	BgL_arg1354z00_1214 =
																		CDR(((obj_t) BgL_l1093z00_1209));
																	{
																		obj_t BgL_tail1096z00_4502;
																		obj_t BgL_l1093z00_4501;

																		BgL_l1093z00_4501 = BgL_arg1354z00_1214;
																		BgL_tail1096z00_4502 =
																			BgL_newtail1097z00_1213;
																		BgL_tail1096z00_1210 = BgL_tail1096z00_4502;
																		BgL_l1093z00_1209 = BgL_l1093z00_4501;
																		goto BgL_zc3z04anonymousza31351ze3z87_1211;
																	}
																}
															}
													}
												}
											BgL_arg1348z00_1203 =
												BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
												(BgL_arg1349z00_1204, BNIL);
										}
										return
											MAKE_YOUNG_PAIR
											(BGl_symbol2349z00zz__r5_macro_4_3_syntaxz00,
											BgL_arg1348z00_1203);
									}
								}
							}
						else
							{	/* R5rs/syntax.scm 264 */
								return
									BGl_errorz00zz__errorz00
									(BGl_string2563z00zz__r5_macro_4_3_syntaxz00,
									BGl_string2561z00zz__r5_macro_4_3_syntaxz00, BgL_xz00_11);
							}
					}
				else
					{	/* R5rs/syntax.scm 264 */
						return
							BGl_errorz00zz__errorz00
							(BGl_string2563z00zz__r5_macro_4_3_syntaxz00,
							BGl_string2561z00zz__r5_macro_4_3_syntaxz00, BgL_xz00_11);
					}
			}
		}

	}



/* loop~2 */
	obj_t BGl_loopze72ze7zz__r5_macro_4_3_syntaxz00(obj_t BgL_ez00_3741,
		obj_t BgL_bsz00_1220)
	{
		{	/* R5rs/syntax.scm 266 */
			if (NULLP(BgL_bsz00_1220))
				{	/* R5rs/syntax.scm 267 */
					return BgL_ez00_3741;
				}
			else
				{
					obj_t BgL_mz00_1223;
					obj_t BgL_lsz00_1224;
					obj_t BgL_rulesz00_1225;

					{	/* R5rs/syntax.scm 269 */
						obj_t BgL_ezd2151zd2_1228;

						BgL_ezd2151zd2_1228 = CAR(((obj_t) BgL_bsz00_1220));
						if (PAIRP(BgL_ezd2151zd2_1228))
							{	/* R5rs/syntax.scm 269 */
								obj_t BgL_carzd2158zd2_1230;
								obj_t BgL_cdrzd2159zd2_1231;

								BgL_carzd2158zd2_1230 = CAR(BgL_ezd2151zd2_1228);
								BgL_cdrzd2159zd2_1231 = CDR(BgL_ezd2151zd2_1228);
								if (SYMBOLP(BgL_carzd2158zd2_1230))
									{	/* R5rs/syntax.scm 269 */
										if (PAIRP(BgL_cdrzd2159zd2_1231))
											{	/* R5rs/syntax.scm 269 */
												obj_t BgL_carzd2165zd2_1234;

												BgL_carzd2165zd2_1234 = CAR(BgL_cdrzd2159zd2_1231);
												if (PAIRP(BgL_carzd2165zd2_1234))
													{	/* R5rs/syntax.scm 269 */
														obj_t BgL_cdrzd2170zd2_1236;

														BgL_cdrzd2170zd2_1236 = CDR(BgL_carzd2165zd2_1234);
														if (
															(CAR(BgL_carzd2165zd2_1234) ==
																BGl_symbol2558z00zz__r5_macro_4_3_syntaxz00))
															{	/* R5rs/syntax.scm 269 */
																if (PAIRP(BgL_cdrzd2170zd2_1236))
																	{	/* R5rs/syntax.scm 269 */
																		if (NULLP(CDR(BgL_cdrzd2159zd2_1231)))
																			{	/* R5rs/syntax.scm 269 */
																				BgL_mz00_1223 = BgL_carzd2158zd2_1230;
																				BgL_lsz00_1224 =
																					CAR(BgL_cdrzd2170zd2_1236);
																				BgL_rulesz00_1225 =
																					CDR(BgL_cdrzd2170zd2_1236);
																				{	/* R5rs/syntax.scm 271 */
																					obj_t BgL_e3z00_1246;
																					obj_t BgL_e4z00_1247;

																					BGl_initzd2syntaxzd2expandersz12z12zz__r5_macro_4_3_syntaxz00
																						();
																					BgL_e3z00_1246 =
																						BGl_syntaxzd2ruleszd2ze3expanderze3zz__r5_macro_4_3_syntaxz00
																						(BgL_mz00_1223, BgL_lsz00_1224,
																						BgL_rulesz00_1225);
																					{	/* R5rs/syntax.scm 272 */
																						obj_t BgL_arg1378z00_1256;

																						BgL_arg1378z00_1256 =
																							CDR(((obj_t) BgL_bsz00_1220));
																						BgL_e4z00_1247 =
																							BGl_loopze72ze7zz__r5_macro_4_3_syntaxz00
																							(BgL_ez00_3741,
																							BgL_arg1378z00_1256);
																					}
																					{	/* R5rs/syntax.scm 273 */
																						obj_t
																							BgL_zc3z04anonymousza31373ze3z87_3706;
																						BgL_zc3z04anonymousza31373ze3z87_3706
																							=
																							MAKE_FX_PROCEDURE
																							(BGl_z62zc3z04anonymousza31373ze3ze5zz__r5_macro_4_3_syntaxz00,
																							(int) (2L), (int) (3L));
																						PROCEDURE_SET
																							(BgL_zc3z04anonymousza31373ze3z87_3706,
																							(int) (0L), BgL_e4z00_1247);
																						PROCEDURE_SET
																							(BgL_zc3z04anonymousza31373ze3z87_3706,
																							(int) (1L), BgL_e3z00_1246);
																						PROCEDURE_SET
																							(BgL_zc3z04anonymousza31373ze3z87_3706,
																							(int) (2L), BgL_mz00_1223);
																						return
																							BgL_zc3z04anonymousza31373ze3z87_3706;
																					}
																				}
																			}
																		else
																			{	/* R5rs/syntax.scm 269 */
																				return
																					BGl_errorz00zz__errorz00
																					(BGl_string2564z00zz__r5_macro_4_3_syntaxz00,
																					BGl_string2565z00zz__r5_macro_4_3_syntaxz00,
																					BgL_bsz00_1220);
																			}
																	}
																else
																	{	/* R5rs/syntax.scm 269 */
																		return
																			BGl_errorz00zz__errorz00
																			(BGl_string2564z00zz__r5_macro_4_3_syntaxz00,
																			BGl_string2565z00zz__r5_macro_4_3_syntaxz00,
																			BgL_bsz00_1220);
																	}
															}
														else
															{	/* R5rs/syntax.scm 269 */
																return
																	BGl_errorz00zz__errorz00
																	(BGl_string2564z00zz__r5_macro_4_3_syntaxz00,
																	BGl_string2565z00zz__r5_macro_4_3_syntaxz00,
																	BgL_bsz00_1220);
															}
													}
												else
													{	/* R5rs/syntax.scm 269 */
														return
															BGl_errorz00zz__errorz00
															(BGl_string2564z00zz__r5_macro_4_3_syntaxz00,
															BGl_string2565z00zz__r5_macro_4_3_syntaxz00,
															BgL_bsz00_1220);
													}
											}
										else
											{	/* R5rs/syntax.scm 269 */
												return
													BGl_errorz00zz__errorz00
													(BGl_string2564z00zz__r5_macro_4_3_syntaxz00,
													BGl_string2565z00zz__r5_macro_4_3_syntaxz00,
													BgL_bsz00_1220);
											}
									}
								else
									{	/* R5rs/syntax.scm 269 */
										return
											BGl_errorz00zz__errorz00
											(BGl_string2564z00zz__r5_macro_4_3_syntaxz00,
											BGl_string2565z00zz__r5_macro_4_3_syntaxz00,
											BgL_bsz00_1220);
									}
							}
						else
							{	/* R5rs/syntax.scm 269 */
								return
									BGl_errorz00zz__errorz00
									(BGl_string2564z00zz__r5_macro_4_3_syntaxz00,
									BGl_string2565z00zz__r5_macro_4_3_syntaxz00, BgL_bsz00_1220);
							}
					}
				}
		}

	}



/* &expand-letrec-syntax */
	obj_t BGl_z62expandzd2letreczd2syntaxz62zz__r5_macro_4_3_syntaxz00(obj_t
		BgL_envz00_3707, obj_t BgL_xz00_3708, obj_t BgL_ez00_3709)
	{
		{	/* R5rs/syntax.scm 263 */
			{	/* R5rs/syntax.scm 264 */
				obj_t BgL_auxz00_4556;

				if (PROCEDUREP(BgL_ez00_3709))
					{	/* R5rs/syntax.scm 264 */
						BgL_auxz00_4556 = BgL_ez00_3709;
					}
				else
					{
						obj_t BgL_auxz00_4559;

						BgL_auxz00_4559 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2324z00zz__r5_macro_4_3_syntaxz00, BINT(8548L),
							BGl_string2566z00zz__r5_macro_4_3_syntaxz00,
							BGl_string2327z00zz__r5_macro_4_3_syntaxz00, BgL_ez00_3709);
						FAILURE(BgL_auxz00_4559, BFALSE, BFALSE);
					}
				return
					BGl_expandzd2letreczd2syntaxz00zz__r5_macro_4_3_syntaxz00
					(BgL_xz00_3708, BgL_auxz00_4556);
			}
		}

	}



/* &<@anonymous:1373> */
	obj_t BGl_z62zc3z04anonymousza31373ze3ze5zz__r5_macro_4_3_syntaxz00(obj_t
		BgL_envz00_3710, obj_t BgL_xz00_3714, obj_t BgL_e2z00_3715)
	{
		{	/* R5rs/syntax.scm 273 */
			{	/* R5rs/syntax.scm 274 */
				obj_t BgL_e4z00_3711;
				obj_t BgL_e3z00_3712;
				obj_t BgL_mz00_3713;

				BgL_e4z00_3711 = PROCEDURE_REF(BgL_envz00_3710, (int) (0L));
				BgL_e3z00_3712 = PROCEDURE_REF(BgL_envz00_3710, (int) (1L));
				BgL_mz00_3713 = ((obj_t) PROCEDURE_REF(BgL_envz00_3710, (int) (2L)));
				{	/* R5rs/syntax.scm 274 */
					bool_t BgL_test2710z00_4571;

					if (PAIRP(BgL_xz00_3714))
						{	/* R5rs/syntax.scm 274 */
							BgL_test2710z00_4571 =
								BGl_hygienezd2eqzf3z21zz__r5_macro_4_3_syntaxz00(BgL_mz00_3713,
								CAR(BgL_xz00_3714));
						}
					else
						{	/* R5rs/syntax.scm 274 */
							BgL_test2710z00_4571 = ((bool_t) 0);
						}
					if (BgL_test2710z00_4571)
						{	/* R5rs/syntax.scm 274 */
							return
								BGL_PROCEDURE_CALL2(BgL_e3z00_3712, BgL_xz00_3714,
								BgL_e2z00_3715);
						}
					else
						{	/* R5rs/syntax.scm 274 */
							return
								BGL_PROCEDURE_CALL2(BgL_e4z00_3711, BgL_xz00_3714,
								BgL_e2z00_3715);
						}
				}
			}
		}

	}



/* expand-let-syntax */
	BGL_EXPORTED_DEF obj_t
		BGl_expandzd2letzd2syntaxz00zz__r5_macro_4_3_syntaxz00(obj_t BgL_xz00_13,
		obj_t BgL_ez00_14)
	{
		{	/* R5rs/syntax.scm 287 */
			{
				obj_t BgL_bsz00_1258;
				obj_t BgL_bodyz00_1259;

				if (PAIRP(BgL_xz00_13))
					{	/* R5rs/syntax.scm 288 */
						obj_t BgL_cdrzd2185zd2_1264;

						BgL_cdrzd2185zd2_1264 = CDR(((obj_t) BgL_xz00_13));
						if (PAIRP(BgL_cdrzd2185zd2_1264))
							{	/* R5rs/syntax.scm 288 */
								BgL_bsz00_1258 = CAR(BgL_cdrzd2185zd2_1264);
								BgL_bodyz00_1259 = CDR(BgL_cdrzd2185zd2_1264);
								{	/* R5rs/syntax.scm 290 */
									obj_t BgL_e1z00_1268;

									BgL_e1z00_1268 =
										BGl_loopze71ze7zz__r5_macro_4_3_syntaxz00(BgL_ez00_14,
										BgL_bsz00_1258);
									{	/* R5rs/syntax.scm 304 */
										obj_t BgL_arg1384z00_1269;

										{	/* R5rs/syntax.scm 304 */
											obj_t BgL_arg1387z00_1270;

											if (NULLP(BgL_bodyz00_1259))
												{	/* R5rs/syntax.scm 304 */
													BgL_arg1387z00_1270 = BNIL;
												}
											else
												{	/* R5rs/syntax.scm 304 */
													obj_t BgL_head1100z00_1273;

													BgL_head1100z00_1273 = MAKE_YOUNG_PAIR(BNIL, BNIL);
													{
														obj_t BgL_l1098z00_1275;
														obj_t BgL_tail1101z00_1276;

														BgL_l1098z00_1275 = BgL_bodyz00_1259;
														BgL_tail1101z00_1276 = BgL_head1100z00_1273;
													BgL_zc3z04anonymousza31389ze3z87_1277:
														if (NULLP(BgL_l1098z00_1275))
															{	/* R5rs/syntax.scm 304 */
																BgL_arg1387z00_1270 = CDR(BgL_head1100z00_1273);
															}
														else
															{	/* R5rs/syntax.scm 304 */
																obj_t BgL_newtail1102z00_1279;

																{	/* R5rs/syntax.scm 304 */
																	obj_t BgL_arg1392z00_1281;

																	{	/* R5rs/syntax.scm 304 */
																		obj_t BgL_xz00_1282;

																		BgL_xz00_1282 =
																			CAR(((obj_t) BgL_l1098z00_1275));
																		{	/* R5rs/syntax.scm 304 */
																			obj_t BgL_arg1393z00_1283;

																			BgL_arg1393z00_1283 =
																				BGl_hygieniza7eza7zz__r5_macro_4_3_syntaxz00
																				(BgL_xz00_1282, BNIL);
																			BgL_arg1392z00_1281 =
																				BGL_PROCEDURE_CALL2(BgL_e1z00_1268,
																				BgL_arg1393z00_1283, BgL_e1z00_1268);
																		}
																	}
																	BgL_newtail1102z00_1279 =
																		MAKE_YOUNG_PAIR(BgL_arg1392z00_1281, BNIL);
																}
																SET_CDR(BgL_tail1101z00_1276,
																	BgL_newtail1102z00_1279);
																{	/* R5rs/syntax.scm 304 */
																	obj_t BgL_arg1391z00_1280;

																	BgL_arg1391z00_1280 =
																		CDR(((obj_t) BgL_l1098z00_1275));
																	{
																		obj_t BgL_tail1101z00_4612;
																		obj_t BgL_l1098z00_4611;

																		BgL_l1098z00_4611 = BgL_arg1391z00_1280;
																		BgL_tail1101z00_4612 =
																			BgL_newtail1102z00_1279;
																		BgL_tail1101z00_1276 = BgL_tail1101z00_4612;
																		BgL_l1098z00_1275 = BgL_l1098z00_4611;
																		goto BgL_zc3z04anonymousza31389ze3z87_1277;
																	}
																}
															}
													}
												}
											BgL_arg1384z00_1269 =
												BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
												(BgL_arg1387z00_1270, BNIL);
										}
										return
											MAKE_YOUNG_PAIR
											(BGl_symbol2349z00zz__r5_macro_4_3_syntaxz00,
											BgL_arg1384z00_1269);
									}
								}
							}
						else
							{	/* R5rs/syntax.scm 288 */
								return
									BGl_errorz00zz__errorz00
									(BGl_string2564z00zz__r5_macro_4_3_syntaxz00,
									BGl_string2561z00zz__r5_macro_4_3_syntaxz00, BgL_xz00_13);
							}
					}
				else
					{	/* R5rs/syntax.scm 288 */
						return
							BGl_errorz00zz__errorz00
							(BGl_string2564z00zz__r5_macro_4_3_syntaxz00,
							BGl_string2561z00zz__r5_macro_4_3_syntaxz00, BgL_xz00_13);
					}
			}
		}

	}



/* loop~1 */
	obj_t BGl_loopze71ze7zz__r5_macro_4_3_syntaxz00(obj_t BgL_ez00_3740,
		obj_t BgL_bsz00_1286)
	{
		{	/* R5rs/syntax.scm 290 */
			if (NULLP(BgL_bsz00_1286))
				{	/* R5rs/syntax.scm 291 */
					return BgL_ez00_3740;
				}
			else
				{
					obj_t BgL_mz00_1289;
					obj_t BgL_lsz00_1290;
					obj_t BgL_rulesz00_1291;

					{	/* R5rs/syntax.scm 293 */
						obj_t BgL_ezd2194zd2_1294;

						BgL_ezd2194zd2_1294 = CAR(((obj_t) BgL_bsz00_1286));
						if (PAIRP(BgL_ezd2194zd2_1294))
							{	/* R5rs/syntax.scm 293 */
								obj_t BgL_carzd2201zd2_1296;
								obj_t BgL_cdrzd2202zd2_1297;

								BgL_carzd2201zd2_1296 = CAR(BgL_ezd2194zd2_1294);
								BgL_cdrzd2202zd2_1297 = CDR(BgL_ezd2194zd2_1294);
								if (SYMBOLP(BgL_carzd2201zd2_1296))
									{	/* R5rs/syntax.scm 293 */
										if (PAIRP(BgL_cdrzd2202zd2_1297))
											{	/* R5rs/syntax.scm 293 */
												obj_t BgL_carzd2208zd2_1300;

												BgL_carzd2208zd2_1300 = CAR(BgL_cdrzd2202zd2_1297);
												if (PAIRP(BgL_carzd2208zd2_1300))
													{	/* R5rs/syntax.scm 293 */
														obj_t BgL_cdrzd2213zd2_1302;

														BgL_cdrzd2213zd2_1302 = CDR(BgL_carzd2208zd2_1300);
														if (
															(CAR(BgL_carzd2208zd2_1300) ==
																BGl_symbol2558z00zz__r5_macro_4_3_syntaxz00))
															{	/* R5rs/syntax.scm 293 */
																if (PAIRP(BgL_cdrzd2213zd2_1302))
																	{	/* R5rs/syntax.scm 293 */
																		if (NULLP(CDR(BgL_cdrzd2202zd2_1297)))
																			{	/* R5rs/syntax.scm 293 */
																				BgL_mz00_1289 = BgL_carzd2201zd2_1296;
																				BgL_lsz00_1290 =
																					CAR(BgL_cdrzd2213zd2_1302);
																				BgL_rulesz00_1291 =
																					CDR(BgL_cdrzd2213zd2_1302);
																				{	/* R5rs/syntax.scm 295 */
																					obj_t BgL_e3z00_1312;
																					obj_t BgL_e4z00_1313;

																					BGl_initzd2syntaxzd2expandersz12z12zz__r5_macro_4_3_syntaxz00
																						();
																					BgL_e3z00_1312 =
																						BGl_syntaxzd2ruleszd2ze3expanderze3zz__r5_macro_4_3_syntaxz00
																						(BgL_mz00_1289, BgL_lsz00_1290,
																						BgL_rulesz00_1291);
																					{	/* R5rs/syntax.scm 296 */
																						obj_t BgL_arg1414z00_1322;

																						BgL_arg1414z00_1322 =
																							CDR(((obj_t) BgL_bsz00_1286));
																						BgL_e4z00_1313 =
																							BGl_loopze71ze7zz__r5_macro_4_3_syntaxz00
																							(BgL_ez00_3740,
																							BgL_arg1414z00_1322);
																					}
																					{	/* R5rs/syntax.scm 297 */
																						obj_t
																							BgL_zc3z04anonymousza31409ze3z87_3716;
																						BgL_zc3z04anonymousza31409ze3z87_3716
																							=
																							MAKE_FX_PROCEDURE
																							(BGl_z62zc3z04anonymousza31409ze3ze5zz__r5_macro_4_3_syntaxz00,
																							(int) (2L), (int) (4L));
																						PROCEDURE_SET
																							(BgL_zc3z04anonymousza31409ze3z87_3716,
																							(int) (0L), BgL_e4z00_1313);
																						PROCEDURE_SET
																							(BgL_zc3z04anonymousza31409ze3z87_3716,
																							(int) (1L), BgL_e3z00_1312);
																						PROCEDURE_SET
																							(BgL_zc3z04anonymousza31409ze3z87_3716,
																							(int) (2L), BgL_ez00_3740);
																						PROCEDURE_SET
																							(BgL_zc3z04anonymousza31409ze3z87_3716,
																							(int) (3L), BgL_mz00_1289);
																						return
																							BgL_zc3z04anonymousza31409ze3z87_3716;
																					}
																				}
																			}
																		else
																			{	/* R5rs/syntax.scm 293 */
																				return
																					BGl_errorz00zz__errorz00
																					(BGl_string2564z00zz__r5_macro_4_3_syntaxz00,
																					BGl_string2565z00zz__r5_macro_4_3_syntaxz00,
																					BgL_bsz00_1286);
																			}
																	}
																else
																	{	/* R5rs/syntax.scm 293 */
																		return
																			BGl_errorz00zz__errorz00
																			(BGl_string2564z00zz__r5_macro_4_3_syntaxz00,
																			BGl_string2565z00zz__r5_macro_4_3_syntaxz00,
																			BgL_bsz00_1286);
																	}
															}
														else
															{	/* R5rs/syntax.scm 293 */
																return
																	BGl_errorz00zz__errorz00
																	(BGl_string2564z00zz__r5_macro_4_3_syntaxz00,
																	BGl_string2565z00zz__r5_macro_4_3_syntaxz00,
																	BgL_bsz00_1286);
															}
													}
												else
													{	/* R5rs/syntax.scm 293 */
														return
															BGl_errorz00zz__errorz00
															(BGl_string2564z00zz__r5_macro_4_3_syntaxz00,
															BGl_string2565z00zz__r5_macro_4_3_syntaxz00,
															BgL_bsz00_1286);
													}
											}
										else
											{	/* R5rs/syntax.scm 293 */
												return
													BGl_errorz00zz__errorz00
													(BGl_string2564z00zz__r5_macro_4_3_syntaxz00,
													BGl_string2565z00zz__r5_macro_4_3_syntaxz00,
													BgL_bsz00_1286);
											}
									}
								else
									{	/* R5rs/syntax.scm 293 */
										return
											BGl_errorz00zz__errorz00
											(BGl_string2564z00zz__r5_macro_4_3_syntaxz00,
											BGl_string2565z00zz__r5_macro_4_3_syntaxz00,
											BgL_bsz00_1286);
									}
							}
						else
							{	/* R5rs/syntax.scm 293 */
								return
									BGl_errorz00zz__errorz00
									(BGl_string2564z00zz__r5_macro_4_3_syntaxz00,
									BGl_string2565z00zz__r5_macro_4_3_syntaxz00, BgL_bsz00_1286);
							}
					}
				}
		}

	}



/* &expand-let-syntax */
	obj_t BGl_z62expandzd2letzd2syntaxz62zz__r5_macro_4_3_syntaxz00(obj_t
		BgL_envz00_3717, obj_t BgL_xz00_3718, obj_t BgL_ez00_3719)
	{
		{	/* R5rs/syntax.scm 287 */
			{	/* R5rs/syntax.scm 288 */
				obj_t BgL_auxz00_4668;

				if (PROCEDUREP(BgL_ez00_3719))
					{	/* R5rs/syntax.scm 288 */
						BgL_auxz00_4668 = BgL_ez00_3719;
					}
				else
					{
						obj_t BgL_auxz00_4671;

						BgL_auxz00_4671 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2324z00zz__r5_macro_4_3_syntaxz00, BINT(9399L),
							BGl_string2567z00zz__r5_macro_4_3_syntaxz00,
							BGl_string2327z00zz__r5_macro_4_3_syntaxz00, BgL_ez00_3719);
						FAILURE(BgL_auxz00_4671, BFALSE, BFALSE);
					}
				return
					BGl_expandzd2letzd2syntaxz00zz__r5_macro_4_3_syntaxz00(BgL_xz00_3718,
					BgL_auxz00_4668);
			}
		}

	}



/* &<@anonymous:1409> */
	obj_t BGl_z62zc3z04anonymousza31409ze3ze5zz__r5_macro_4_3_syntaxz00(obj_t
		BgL_envz00_3720, obj_t BgL_xz00_3725, obj_t BgL_e2z00_3726)
	{
		{	/* R5rs/syntax.scm 297 */
			{	/* R5rs/syntax.scm 298 */
				obj_t BgL_e4z00_3721;
				obj_t BgL_e3z00_3722;
				obj_t BgL_ez00_3723;
				obj_t BgL_mz00_3724;

				BgL_e4z00_3721 = PROCEDURE_REF(BgL_envz00_3720, (int) (0L));
				BgL_e3z00_3722 = PROCEDURE_REF(BgL_envz00_3720, (int) (1L));
				BgL_ez00_3723 = ((obj_t) PROCEDURE_REF(BgL_envz00_3720, (int) (2L)));
				BgL_mz00_3724 = ((obj_t) PROCEDURE_REF(BgL_envz00_3720, (int) (3L)));
				{	/* R5rs/syntax.scm 298 */
					bool_t BgL_test2725z00_4686;

					if (PAIRP(BgL_xz00_3725))
						{	/* R5rs/syntax.scm 298 */
							BgL_test2725z00_4686 =
								BGl_hygienezd2eqzf3z21zz__r5_macro_4_3_syntaxz00(BgL_mz00_3724,
								CAR(BgL_xz00_3725));
						}
					else
						{	/* R5rs/syntax.scm 298 */
							BgL_test2725z00_4686 = ((bool_t) 0);
						}
					if (BgL_test2725z00_4686)
						{	/* R5rs/syntax.scm 298 */
							return
								BGL_PROCEDURE_CALL2(BgL_e3z00_3722, BgL_xz00_3725,
								BgL_ez00_3723);
						}
					else
						{	/* R5rs/syntax.scm 298 */
							return
								BGL_PROCEDURE_CALL2(BgL_e4z00_3721, BgL_xz00_3725,
								BgL_e2z00_3726);
						}
				}
			}
		}

	}



/* syntax-rules->expander */
	BGL_EXPORTED_DEF obj_t
		BGl_syntaxzd2ruleszd2ze3expanderze3zz__r5_macro_4_3_syntaxz00(obj_t
		BgL_keywordz00_18, obj_t BgL_literalsz00_19, obj_t BgL_rulesz00_20)
	{
		{	/* R5rs/syntax.scm 318 */
			{	/* R5rs/syntax.scm 319 */
				obj_t BgL_kz00_1324;

				BgL_kz00_1324 = MAKE_YOUNG_PAIR(BgL_keywordz00_18, BgL_literalsz00_19);
				if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_rulesz00_20))
					{	/* R5rs/syntax.scm 321 */
						obj_t BgL_zc3z04anonymousza31416ze3z87_3727;

						BgL_zc3z04anonymousza31416ze3z87_3727 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31416ze3ze5zz__r5_macro_4_3_syntaxz00,
							(int) (2L), (int) (3L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31416ze3z87_3727, (int) (0L),
							BgL_kz00_1324);
						PROCEDURE_SET(BgL_zc3z04anonymousza31416ze3z87_3727, (int) (1L),
							BgL_keywordz00_18);
						PROCEDURE_SET(BgL_zc3z04anonymousza31416ze3z87_3727, (int) (2L),
							BgL_rulesz00_20);
						return BgL_zc3z04anonymousza31416ze3z87_3727;
					}
				else
					{	/* R5rs/syntax.scm 320 */
						return
							BGl_errorz00zz__errorz00(BgL_keywordz00_18,
							BGl_string2568z00zz__r5_macro_4_3_syntaxz00, BgL_rulesz00_20);
					}
			}
		}

	}



/* &syntax-rules->expander */
	obj_t BGl_z62syntaxzd2ruleszd2ze3expanderz81zz__r5_macro_4_3_syntaxz00(obj_t
		BgL_envz00_3728, obj_t BgL_keywordz00_3729, obj_t BgL_literalsz00_3730,
		obj_t BgL_rulesz00_3731)
	{
		{	/* R5rs/syntax.scm 318 */
			{	/* R5rs/syntax.scm 319 */
				obj_t BgL_auxz00_4728;
				obj_t BgL_auxz00_4721;
				obj_t BgL_auxz00_4714;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_rulesz00_3731))
					{	/* R5rs/syntax.scm 319 */
						BgL_auxz00_4728 = BgL_rulesz00_3731;
					}
				else
					{
						obj_t BgL_auxz00_4731;

						BgL_auxz00_4731 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2324z00zz__r5_macro_4_3_syntaxz00, BINT(10634L),
							BGl_string2569z00zz__r5_macro_4_3_syntaxz00,
							BGl_string2570z00zz__r5_macro_4_3_syntaxz00, BgL_rulesz00_3731);
						FAILURE(BgL_auxz00_4731, BFALSE, BFALSE);
					}
				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_literalsz00_3730))
					{	/* R5rs/syntax.scm 319 */
						BgL_auxz00_4721 = BgL_literalsz00_3730;
					}
				else
					{
						obj_t BgL_auxz00_4724;

						BgL_auxz00_4724 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2324z00zz__r5_macro_4_3_syntaxz00, BINT(10634L),
							BGl_string2569z00zz__r5_macro_4_3_syntaxz00,
							BGl_string2570z00zz__r5_macro_4_3_syntaxz00,
							BgL_literalsz00_3730);
						FAILURE(BgL_auxz00_4724, BFALSE, BFALSE);
					}
				if (SYMBOLP(BgL_keywordz00_3729))
					{	/* R5rs/syntax.scm 319 */
						BgL_auxz00_4714 = BgL_keywordz00_3729;
					}
				else
					{
						obj_t BgL_auxz00_4717;

						BgL_auxz00_4717 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2324z00zz__r5_macro_4_3_syntaxz00, BINT(10634L),
							BGl_string2569z00zz__r5_macro_4_3_syntaxz00,
							BGl_string2326z00zz__r5_macro_4_3_syntaxz00, BgL_keywordz00_3729);
						FAILURE(BgL_auxz00_4717, BFALSE, BFALSE);
					}
				return
					BGl_syntaxzd2ruleszd2ze3expanderze3zz__r5_macro_4_3_syntaxz00
					(BgL_auxz00_4714, BgL_auxz00_4721, BgL_auxz00_4728);
			}
		}

	}



/* &<@anonymous:1416> */
	obj_t BGl_z62zc3z04anonymousza31416ze3ze5zz__r5_macro_4_3_syntaxz00(obj_t
		BgL_envz00_3732, obj_t BgL_xz00_3736, obj_t BgL_ez00_3737)
	{
		{	/* R5rs/syntax.scm 321 */
			{	/* R5rs/syntax.scm 323 */
				obj_t BgL_kz00_3733;
				obj_t BgL_keywordz00_3734;
				obj_t BgL_rulesz00_3735;

				BgL_kz00_3733 = ((obj_t) PROCEDURE_REF(BgL_envz00_3732, (int) (0L)));
				BgL_keywordz00_3734 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3732, (int) (1L)));
				BgL_rulesz00_3735 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3732, (int) (2L)));
				{
					obj_t BgL_rulesz00_3814;

					BgL_rulesz00_3814 = BgL_rulesz00_3735;
				BgL_loopz00_3813:
					if (NULLP(BgL_rulesz00_3814))
						{	/* R5rs/syntax.scm 323 */
							return
								BGl_errorz00zz__errorz00(BgL_keywordz00_3734,
								BGl_string2571z00zz__r5_macro_4_3_syntaxz00, BgL_xz00_3736);
						}
					else
						{
							obj_t BgL_patternz00_3816;
							obj_t BgL_templatez00_3817;

							{	/* R5rs/syntax.scm 325 */
								obj_t BgL_ezd2222zd2_3823;

								BgL_ezd2222zd2_3823 = CAR(((obj_t) BgL_rulesz00_3814));
								if (PAIRP(BgL_ezd2222zd2_3823))
									{	/* R5rs/syntax.scm 325 */
										obj_t BgL_cdrzd2228zd2_3824;

										BgL_cdrzd2228zd2_3824 = CDR(BgL_ezd2222zd2_3823);
										if (PAIRP(BgL_cdrzd2228zd2_3824))
											{	/* R5rs/syntax.scm 325 */
												if (NULLP(CDR(BgL_cdrzd2228zd2_3824)))
													{	/* R5rs/syntax.scm 325 */
														BgL_patternz00_3816 = CAR(BgL_ezd2222zd2_3823);
														BgL_templatez00_3817 = CAR(BgL_cdrzd2228zd2_3824);
														if (CBOOL
															(BGl_syntaxzd2matcheszd2patternzf3zf3zz__r5_macro_4_3_syntaxz00
																(BgL_keywordz00_3734, BgL_patternz00_3816,
																	BgL_xz00_3736, BgL_kz00_3733)))
															{	/* R5rs/syntax.scm 329 */
																obj_t BgL_fsz00_3818;

																BgL_fsz00_3818 =
																	BGl_syntaxzd2getzd2framesz00zz__r5_macro_4_3_syntaxz00
																	(BgL_patternz00_3816, BgL_xz00_3736,
																	BgL_kz00_3733);
																{	/* R5rs/syntax.scm 329 */
																	obj_t BgL_tz00_3819;

																	BgL_tz00_3819 =
																		BGl_syntaxzd2expandzd2patternz00zz__r5_macro_4_3_syntaxz00
																		(BgL_templatez00_3817, BgL_fsz00_3818,
																		BgL_kz00_3733);
																	{	/* R5rs/syntax.scm 330 */
																		obj_t BgL_tez00_3820;

																		BgL_tez00_3820 =
																			BGl_syntaxzd2expanderzd2zz__r5_macro_4_3_syntaxz00
																			(BgL_tz00_3819,
																			BGl_syntaxzd2expanderzd2envz00zz__r5_macro_4_3_syntaxz00);
																		{	/* R5rs/syntax.scm 331 */

																			{	/* R5rs/syntax.scm 332 */
																				obj_t BgL_arg1427z00_3821;

																				BgL_arg1427z00_3821 =
																					BGl_hygieniza7eza7zz__r5_macro_4_3_syntaxz00
																					(BgL_tez00_3820, BNIL);
																				return
																					BGL_PROCEDURE_CALL2(BgL_ez00_3737,
																					BgL_arg1427z00_3821, BgL_ez00_3737);
																			}
																		}
																	}
																}
															}
														else
															{	/* R5rs/syntax.scm 333 */
																obj_t BgL_arg1428z00_3822;

																BgL_arg1428z00_3822 =
																	CDR(((obj_t) BgL_rulesz00_3814));
																{
																	obj_t BgL_rulesz00_4772;

																	BgL_rulesz00_4772 = BgL_arg1428z00_3822;
																	BgL_rulesz00_3814 = BgL_rulesz00_4772;
																	goto BgL_loopz00_3813;
																}
															}
													}
												else
													{	/* R5rs/syntax.scm 325 */
														return
															BGl_errorz00zz__errorz00(BgL_keywordz00_3734,
															BGl_string2572z00zz__r5_macro_4_3_syntaxz00,
															BgL_ezd2222zd2_3823);
													}
											}
										else
											{	/* R5rs/syntax.scm 325 */
												return
													BGl_errorz00zz__errorz00(BgL_keywordz00_3734,
													BGl_string2572z00zz__r5_macro_4_3_syntaxz00,
													BgL_ezd2222zd2_3823);
											}
									}
								else
									{	/* R5rs/syntax.scm 325 */
										return
											BGl_errorz00zz__errorz00(BgL_keywordz00_3734,
											BGl_string2572z00zz__r5_macro_4_3_syntaxz00,
											BgL_ezd2222zd2_3823);
									}
							}
						}
				}
			}
		}

	}



/* syntax-matches-pattern? */
	obj_t BGl_syntaxzd2matcheszd2patternzf3zf3zz__r5_macro_4_3_syntaxz00(obj_t
		BgL_keywordz00_21, obj_t BgL_pz00_22, obj_t BgL_ez00_23, obj_t BgL_kz00_24)
	{
		{	/* R5rs/syntax.scm 341 */
		BGl_syntaxzd2matcheszd2patternzf3zf3zz__r5_macro_4_3_syntaxz00:
			if (BGl_ellipsiszf3zf3zz__r5_macro_4_3_syntaxz00(BgL_pz00_22))
				{	/* R5rs/syntax.scm 343 */
					if ((bgl_list_length(BgL_pz00_22) == 2L))
						{	/* R5rs/syntax.scm 344 */
							if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_ez00_23))
								{	/* R5rs/syntax.scm 347 */
									obj_t BgL_p0z00_1358;

									BgL_p0z00_1358 = CAR(((obj_t) BgL_pz00_22));
									{
										obj_t BgL_l1103z00_1360;

										{	/* R5rs/syntax.scm 348 */
											bool_t BgL_tmpz00_4787;

											BgL_l1103z00_1360 = BgL_ez00_23;
										BgL_zc3z04anonymousza31433ze3z87_1361:
											if (NULLP(BgL_l1103z00_1360))
												{	/* R5rs/syntax.scm 348 */
													BgL_tmpz00_4787 = ((bool_t) 1);
												}
											else
												{	/* R5rs/syntax.scm 349 */
													obj_t BgL_nvz00_1363;

													{	/* R5rs/syntax.scm 349 */
														obj_t BgL_eiz00_1366;

														BgL_eiz00_1366 = CAR(((obj_t) BgL_l1103z00_1360));
														BgL_nvz00_1363 =
															BGl_syntaxzd2matcheszd2patternzf3zf3zz__r5_macro_4_3_syntaxz00
															(BgL_keywordz00_21, BgL_p0z00_1358,
															BgL_eiz00_1366, BgL_kz00_24);
													}
													if (CBOOL(BgL_nvz00_1363))
														{	/* R5rs/syntax.scm 348 */
															obj_t BgL_arg1435z00_1365;

															BgL_arg1435z00_1365 =
																CDR(((obj_t) BgL_l1103z00_1360));
															{
																obj_t BgL_l1103z00_4797;

																BgL_l1103z00_4797 = BgL_arg1435z00_1365;
																BgL_l1103z00_1360 = BgL_l1103z00_4797;
																goto BgL_zc3z04anonymousza31433ze3z87_1361;
															}
														}
													else
														{	/* R5rs/syntax.scm 348 */
															BgL_tmpz00_4787 = ((bool_t) 0);
														}
												}
											return BBOOL(BgL_tmpz00_4787);
										}
									}
								}
							else
								{	/* R5rs/syntax.scm 346 */
									return BFALSE;
								}
						}
					else
						{	/* R5rs/syntax.scm 344 */
							return
								BGl_errorz00zz__errorz00(BgL_keywordz00_21,
								BGl_string2573z00zz__r5_macro_4_3_syntaxz00, BgL_pz00_22);
						}
				}
			else
				{	/* R5rs/syntax.scm 343 */
					if (PAIRP(BgL_pz00_22))
						{	/* R5rs/syntax.scm 351 */
							if (PAIRP(BgL_ez00_23))
								{	/* R5rs/syntax.scm 353 */
									obj_t BgL__andtest_1043z00_1371;

									BgL__andtest_1043z00_1371 =
										BGl_syntaxzd2matcheszd2patternzf3zf3zz__r5_macro_4_3_syntaxz00
										(BgL_keywordz00_21, CAR(BgL_pz00_22), CAR(BgL_ez00_23),
										BgL_kz00_24);
									if (CBOOL(BgL__andtest_1043z00_1371))
										{
											obj_t BgL_ez00_4811;
											obj_t BgL_pz00_4809;

											BgL_pz00_4809 = CDR(BgL_pz00_22);
											BgL_ez00_4811 = CDR(BgL_ez00_23);
											BgL_ez00_23 = BgL_ez00_4811;
											BgL_pz00_22 = BgL_pz00_4809;
											goto
												BGl_syntaxzd2matcheszd2patternzf3zf3zz__r5_macro_4_3_syntaxz00;
										}
									else
										{	/* R5rs/syntax.scm 353 */
											return BFALSE;
										}
								}
							else
								{	/* R5rs/syntax.scm 352 */
									return BFALSE;
								}
						}
					else
						{	/* R5rs/syntax.scm 351 */
							if (SYMBOLP(BgL_pz00_22))
								{	/* R5rs/syntax.scm 355 */
									if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
											(BgL_pz00_22, BgL_kz00_24)))
										{	/* R5rs/syntax.scm 356 */
											return
												BBOOL(BGl_hygienezd2eqzf3z21zz__r5_macro_4_3_syntaxz00
												(BgL_ez00_23, BgL_pz00_22));
										}
									else
										{	/* R5rs/syntax.scm 356 */
											return BTRUE;
										}
								}
							else
								{	/* R5rs/syntax.scm 355 */
									return
										BBOOL(BGl_equalzf3zf3zz__r4_equivalence_6_2z00(BgL_pz00_22,
											BgL_ez00_23));
								}
						}
				}
		}

	}



/* syntax-get-frames */
	obj_t BGl_syntaxzd2getzd2framesz00zz__r5_macro_4_3_syntaxz00(obj_t
		BgL_pz00_25, obj_t BgL_ez00_26, obj_t BgL_kz00_27)
	{
		{	/* R5rs/syntax.scm 363 */
			if (BGl_ellipsiszf3zf3zz__r5_macro_4_3_syntaxz00(BgL_pz00_25))
				{	/* R5rs/syntax.scm 366 */
					obj_t BgL_p0z00_1379;

					BgL_p0z00_1379 = CAR(((obj_t) BgL_pz00_25));
					{	/* R5rs/syntax.scm 368 */
						obj_t BgL_arg1445z00_1380;

						{	/* R5rs/syntax.scm 368 */
							obj_t BgL_arg1447z00_1382;

							if (NULLP(BgL_ez00_26))
								{	/* R5rs/syntax.scm 368 */
									BgL_arg1447z00_1382 = BNIL;
								}
							else
								{	/* R5rs/syntax.scm 368 */
									obj_t BgL_head1108z00_1385;

									BgL_head1108z00_1385 = MAKE_YOUNG_PAIR(BNIL, BNIL);
									{
										obj_t BgL_l1106z00_2748;
										obj_t BgL_tail1109z00_2749;

										BgL_l1106z00_2748 = BgL_ez00_26;
										BgL_tail1109z00_2749 = BgL_head1108z00_1385;
									BgL_zc3z04anonymousza31449ze3z87_2747:
										if (NULLP(BgL_l1106z00_2748))
											{	/* R5rs/syntax.scm 368 */
												BgL_arg1447z00_1382 = CDR(BgL_head1108z00_1385);
											}
										else
											{	/* R5rs/syntax.scm 368 */
												obj_t BgL_newtail1110z00_2756;

												{	/* R5rs/syntax.scm 368 */
													obj_t BgL_arg1452z00_2757;

													{	/* R5rs/syntax.scm 368 */
														obj_t BgL_eiz00_2758;

														BgL_eiz00_2758 = CAR(((obj_t) BgL_l1106z00_2748));
														BgL_arg1452z00_2757 =
															BGl_syntaxzd2getzd2framesz00zz__r5_macro_4_3_syntaxz00
															(BgL_p0z00_1379, BgL_eiz00_2758, BgL_kz00_27);
													}
													BgL_newtail1110z00_2756 =
														MAKE_YOUNG_PAIR(BgL_arg1452z00_2757, BNIL);
												}
												SET_CDR(BgL_tail1109z00_2749, BgL_newtail1110z00_2756);
												{	/* R5rs/syntax.scm 368 */
													obj_t BgL_arg1451z00_2759;

													BgL_arg1451z00_2759 =
														CDR(((obj_t) BgL_l1106z00_2748));
													{
														obj_t BgL_tail1109z00_4840;
														obj_t BgL_l1106z00_4839;

														BgL_l1106z00_4839 = BgL_arg1451z00_2759;
														BgL_tail1109z00_4840 = BgL_newtail1110z00_2756;
														BgL_tail1109z00_2749 = BgL_tail1109z00_4840;
														BgL_l1106z00_2748 = BgL_l1106z00_4839;
														goto BgL_zc3z04anonymousza31449ze3z87_2747;
													}
												}
											}
									}
								}
							BgL_arg1445z00_1380 =
								MAKE_YOUNG_PAIR(BGl_keyword2574z00zz__r5_macro_4_3_syntaxz00,
								BgL_arg1447z00_1382);
						}
						{	/* R5rs/syntax.scm 367 */
							obj_t BgL_list1446z00_1381;

							BgL_list1446z00_1381 = MAKE_YOUNG_PAIR(BgL_arg1445z00_1380, BNIL);
							return BgL_list1446z00_1381;
						}
					}
				}
			else
				{	/* R5rs/syntax.scm 365 */
					if (PAIRP(BgL_pz00_25))
						{	/* R5rs/syntax.scm 372 */
							obj_t BgL_arg1454z00_1397;
							obj_t BgL_arg1455z00_1398;

							{	/* R5rs/syntax.scm 372 */
								obj_t BgL_arg1456z00_1399;
								obj_t BgL_arg1457z00_1400;

								BgL_arg1456z00_1399 = CAR(BgL_pz00_25);
								BgL_arg1457z00_1400 = CAR(((obj_t) BgL_ez00_26));
								BgL_arg1454z00_1397 =
									BGl_syntaxzd2getzd2framesz00zz__r5_macro_4_3_syntaxz00
									(BgL_arg1456z00_1399, BgL_arg1457z00_1400, BgL_kz00_27);
							}
							{	/* R5rs/syntax.scm 373 */
								obj_t BgL_arg1458z00_1401;
								obj_t BgL_arg1459z00_1402;

								BgL_arg1458z00_1401 = CDR(BgL_pz00_25);
								BgL_arg1459z00_1402 = CDR(((obj_t) BgL_ez00_26));
								BgL_arg1455z00_1398 =
									BGl_syntaxzd2getzd2framesz00zz__r5_macro_4_3_syntaxz00
									(BgL_arg1458z00_1401, BgL_arg1459z00_1402, BgL_kz00_27);
							}
							return
								BGl_appendzd221011zd2zz__r5_macro_4_3_syntaxz00
								(BgL_arg1454z00_1397, BgL_arg1455z00_1398);
						}
					else
						{	/* R5rs/syntax.scm 371 */
							if (SYMBOLP(BgL_pz00_25))
								{	/* R5rs/syntax.scm 374 */
									if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
											(BgL_pz00_25, BgL_kz00_27)))
										{	/* R5rs/syntax.scm 375 */
											return BNIL;
										}
									else
										{	/* R5rs/syntax.scm 375 */
											obj_t BgL_arg1462z00_1405;

											BgL_arg1462z00_1405 =
												MAKE_YOUNG_PAIR(BgL_pz00_25,
												BGl_unhygieniza7eza7zz__r5_macro_4_3_syntaxz00
												(BgL_ez00_26));
											{	/* R5rs/syntax.scm 375 */
												obj_t BgL_list1463z00_1406;

												BgL_list1463z00_1406 =
													MAKE_YOUNG_PAIR(BgL_arg1462z00_1405, BNIL);
												return BgL_list1463z00_1406;
											}
										}
								}
							else
								{	/* R5rs/syntax.scm 374 */
									return BNIL;
								}
						}
				}
		}

	}



/* syntax-expand-pattern */
	obj_t BGl_syntaxzd2expandzd2patternz00zz__r5_macro_4_3_syntaxz00(obj_t
		BgL_pz00_28, obj_t BgL_envz00_29, obj_t BgL_kz00_30)
	{
		{	/* R5rs/syntax.scm 382 */
			{
				obj_t BgL_p0z00_1423;
				obj_t BgL_envz00_1424;
				obj_t BgL_kz00_1425;

				if (BGl_ellipsiszf3zf3zz__r5_macro_4_3_syntaxz00(BgL_pz00_28))
					{	/* R5rs/syntax.scm 395 */
						obj_t BgL_arg1466z00_1410;
						obj_t BgL_arg1467z00_1411;

						{	/* R5rs/syntax.scm 395 */
							obj_t BgL_arg1468z00_1412;

							BgL_arg1468z00_1412 = CAR(((obj_t) BgL_pz00_28));
							BgL_p0z00_1423 = BgL_arg1468z00_1412;
							BgL_envz00_1424 = BgL_envz00_29;
							BgL_kz00_1425 = BgL_kz00_30;
							{	/* R5rs/syntax.scm 385 */
								obj_t BgL_varsz00_1427;

								BgL_varsz00_1427 =
									BGl_subze70ze7zz__r5_macro_4_3_syntaxz00(BgL_kz00_1425,
									BgL_p0z00_1423);
								{	/* R5rs/syntax.scm 385 */
									obj_t BgL_framesz00_1428;

									BgL_framesz00_1428 =
										BGl_getzd2ellipsiszd2framesz00zz__r5_macro_4_3_syntaxz00
										(BgL_varsz00_1427, BgL_envz00_1424);
									{	/* R5rs/syntax.scm 386 */

										if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00
											(BgL_framesz00_1428))
											{	/* R5rs/syntax.scm 387 */
												if (NULLP(BgL_framesz00_1428))
													{	/* R5rs/syntax.scm 389 */
														BgL_arg1466z00_1410 = BNIL;
													}
												else
													{	/* R5rs/syntax.scm 389 */
														obj_t BgL_head1113z00_1432;

														BgL_head1113z00_1432 = MAKE_YOUNG_PAIR(BNIL, BNIL);
														{
															obj_t BgL_l1111z00_1434;
															obj_t BgL_tail1114z00_1435;

															BgL_l1111z00_1434 = BgL_framesz00_1428;
															BgL_tail1114z00_1435 = BgL_head1113z00_1432;
														BgL_zc3z04anonymousza31483ze3z87_1436:
															if (NULLP(BgL_l1111z00_1434))
																{	/* R5rs/syntax.scm 389 */
																	BgL_arg1466z00_1410 =
																		CDR(BgL_head1113z00_1432);
																}
															else
																{	/* R5rs/syntax.scm 389 */
																	obj_t BgL_newtail1115z00_1438;

																	{	/* R5rs/syntax.scm 389 */
																		obj_t BgL_arg1486z00_1440;

																		{	/* R5rs/syntax.scm 389 */
																			obj_t BgL_fz00_1441;

																			BgL_fz00_1441 =
																				CAR(((obj_t) BgL_l1111z00_1434));
																			BgL_arg1486z00_1440 =
																				BGl_syntaxzd2expandzd2patternz00zz__r5_macro_4_3_syntaxz00
																				(BgL_p0z00_1423,
																				BGl_appendzd221011zd2zz__r5_macro_4_3_syntaxz00
																				(BgL_fz00_1441, BgL_envz00_1424),
																				BgL_kz00_1425);
																		}
																		BgL_newtail1115z00_1438 =
																			MAKE_YOUNG_PAIR(BgL_arg1486z00_1440,
																			BNIL);
																	}
																	SET_CDR(BgL_tail1114z00_1435,
																		BgL_newtail1115z00_1438);
																	{	/* R5rs/syntax.scm 389 */
																		obj_t BgL_arg1485z00_1439;

																		BgL_arg1485z00_1439 =
																			CDR(((obj_t) BgL_l1111z00_1434));
																		{
																			obj_t BgL_tail1114z00_4885;
																			obj_t BgL_l1111z00_4884;

																			BgL_l1111z00_4884 = BgL_arg1485z00_1439;
																			BgL_tail1114z00_4885 =
																				BgL_newtail1115z00_1438;
																			BgL_tail1114z00_1435 =
																				BgL_tail1114z00_4885;
																			BgL_l1111z00_1434 = BgL_l1111z00_4884;
																			goto
																				BgL_zc3z04anonymousza31483ze3z87_1436;
																		}
																	}
																}
														}
													}
											}
										else
											{	/* R5rs/syntax.scm 387 */
												BgL_arg1466z00_1410 = BNIL;
											}
									}
								}
							}
						}
						{	/* R5rs/syntax.scm 396 */
							obj_t BgL_arg1469z00_1413;

							{	/* R5rs/syntax.scm 396 */
								obj_t BgL_pairz00_2778;

								BgL_pairz00_2778 = CDR(((obj_t) BgL_pz00_28));
								BgL_arg1469z00_1413 = CDR(BgL_pairz00_2778);
							}
							BgL_arg1467z00_1411 =
								BGl_syntaxzd2expandzd2patternz00zz__r5_macro_4_3_syntaxz00
								(BgL_arg1469z00_1413, BgL_envz00_29, BgL_kz00_30);
						}
						return
							BGl_appendzd221011zd2zz__r5_macro_4_3_syntaxz00
							(BgL_arg1466z00_1410, BgL_arg1467z00_1411);
					}
				else
					{	/* R5rs/syntax.scm 394 */
						if (PAIRP(BgL_pz00_28))
							{	/* R5rs/syntax.scm 397 */
								return
									MAKE_YOUNG_PAIR
									(BGl_syntaxzd2expandzd2patternz00zz__r5_macro_4_3_syntaxz00
									(CAR(BgL_pz00_28), BgL_envz00_29, BgL_kz00_30),
									BGl_syntaxzd2expandzd2patternz00zz__r5_macro_4_3_syntaxz00(CDR
										(BgL_pz00_28), BgL_envz00_29, BgL_kz00_30));
							}
						else
							{	/* R5rs/syntax.scm 397 */
								if (SYMBOLP(BgL_pz00_28))
									{	/* R5rs/syntax.scm 400 */
										if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
												(BgL_pz00_28, BgL_kz00_30)))
											{	/* R5rs/syntax.scm 401 */
												return BgL_pz00_28;
											}
										else
											{	/* R5rs/syntax.scm 403 */
												obj_t BgL_xz00_1421;

												BgL_xz00_1421 =
													BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_pz00_28,
													BgL_envz00_29);
												if (PAIRP(BgL_xz00_1421))
													{	/* R5rs/syntax.scm 404 */
														return CDR(BgL_xz00_1421);
													}
												else
													{	/* R5rs/syntax.scm 404 */
														return BgL_pz00_28;
													}
											}
									}
								else
									{	/* R5rs/syntax.scm 400 */
										return BgL_pz00_28;
									}
							}
					}
			}
		}

	}



/* get-ellipsis-frames */
	obj_t BGl_getzd2ellipsiszd2framesz00zz__r5_macro_4_3_syntaxz00(obj_t
		BgL_varsz00_31, obj_t BgL_framesz00_32)
	{
		{	/* R5rs/syntax.scm 413 */
			{
				obj_t BgL_varsz00_1447;
				obj_t BgL_resz00_1448;

				BgL_varsz00_1447 = BgL_varsz00_31;
				BgL_resz00_1448 = BNIL;
			BgL_zc3z04anonymousza31488ze3z87_1449:
				if (NULLP(BgL_varsz00_1447))
					{	/* R5rs/syntax.scm 416 */
						return BgL_resz00_1448;
					}
				else
					{	/* R5rs/syntax.scm 418 */
						obj_t BgL_vz00_1451;

						BgL_vz00_1451 = CAR(((obj_t) BgL_varsz00_1447));
						{	/* R5rs/syntax.scm 419 */
							obj_t BgL_fz00_1452;

							{
								obj_t BgL_l1122z00_1512;

								BgL_l1122z00_1512 = BgL_framesz00_32;
							BgL_zc3z04anonymousza31525ze3z87_1513:
								if (NULLP(BgL_l1122z00_1512))
									{	/* R5rs/syntax.scm 419 */
										BgL_fz00_1452 = BFALSE;
									}
								else
									{	/* R5rs/syntax.scm 420 */
										obj_t BgL__ortest_1124z00_1515;

										{	/* R5rs/syntax.scm 420 */
											obj_t BgL_fz00_1517;

											BgL_fz00_1517 = CAR(((obj_t) BgL_l1122z00_1512));
											if (
												(CAR(
														((obj_t) BgL_fz00_1517)) ==
													BGl_keyword2574z00zz__r5_macro_4_3_syntaxz00))
												{	/* R5rs/syntax.scm 421 */
													obj_t BgL_ez00_1520;

													{	/* R5rs/syntax.scm 421 */
														obj_t BgL_hook1120z00_1522;

														BgL_hook1120z00_1522 =
															MAKE_YOUNG_PAIR(BFALSE, BNIL);
														{	/* R5rs/syntax.scm 421 */
															obj_t BgL_g1121z00_1523;

															BgL_g1121z00_1523 = CDR(((obj_t) BgL_fz00_1517));
															{
																obj_t BgL_l1117z00_1525;
																obj_t BgL_h1118z00_1526;

																BgL_l1117z00_1525 = BgL_g1121z00_1523;
																BgL_h1118z00_1526 = BgL_hook1120z00_1522;
															BgL_zc3z04anonymousza31531ze3z87_1527:
																if (NULLP(BgL_l1117z00_1525))
																	{	/* R5rs/syntax.scm 421 */
																		BgL_ez00_1520 = CDR(BgL_hook1120z00_1522);
																	}
																else
																	{	/* R5rs/syntax.scm 421 */
																		if (CBOOL
																			(BGl_assqz00zz__r4_pairs_and_lists_6_3z00
																				(BgL_vz00_1451,
																					CAR(((obj_t) BgL_l1117z00_1525)))))
																			{	/* R5rs/syntax.scm 421 */
																				obj_t BgL_nh1119z00_1531;

																				{	/* R5rs/syntax.scm 421 */
																					obj_t BgL_arg1536z00_1533;

																					BgL_arg1536z00_1533 =
																						CAR(((obj_t) BgL_l1117z00_1525));
																					BgL_nh1119z00_1531 =
																						MAKE_YOUNG_PAIR(BgL_arg1536z00_1533,
																						BNIL);
																				}
																				SET_CDR(BgL_h1118z00_1526,
																					BgL_nh1119z00_1531);
																				{	/* R5rs/syntax.scm 421 */
																					obj_t BgL_arg1535z00_1532;

																					BgL_arg1535z00_1532 =
																						CDR(((obj_t) BgL_l1117z00_1525));
																					{
																						obj_t BgL_h1118z00_4937;
																						obj_t BgL_l1117z00_4936;

																						BgL_l1117z00_4936 =
																							BgL_arg1535z00_1532;
																						BgL_h1118z00_4937 =
																							BgL_nh1119z00_1531;
																						BgL_h1118z00_1526 =
																							BgL_h1118z00_4937;
																						BgL_l1117z00_1525 =
																							BgL_l1117z00_4936;
																						goto
																							BgL_zc3z04anonymousza31531ze3z87_1527;
																					}
																				}
																			}
																		else
																			{	/* R5rs/syntax.scm 421 */
																				obj_t BgL_arg1537z00_1534;

																				BgL_arg1537z00_1534 =
																					CDR(((obj_t) BgL_l1117z00_1525));
																				{
																					obj_t BgL_l1117z00_4940;

																					BgL_l1117z00_4940 =
																						BgL_arg1537z00_1534;
																					BgL_l1117z00_1525 = BgL_l1117z00_4940;
																					goto
																						BgL_zc3z04anonymousza31531ze3z87_1527;
																				}
																			}
																	}
															}
														}
													}
													if (PAIRP(BgL_ez00_1520))
														{	/* R5rs/syntax.scm 422 */
															BgL__ortest_1124z00_1515 = BgL_ez00_1520;
														}
													else
														{	/* R5rs/syntax.scm 422 */
															BgL__ortest_1124z00_1515 = BFALSE;
														}
												}
											else
												{	/* R5rs/syntax.scm 420 */
													BgL__ortest_1124z00_1515 = BFALSE;
												}
										}
										if (CBOOL(BgL__ortest_1124z00_1515))
											{	/* R5rs/syntax.scm 419 */
												BgL_fz00_1452 = BgL__ortest_1124z00_1515;
											}
										else
											{	/* R5rs/syntax.scm 419 */
												obj_t BgL_arg1527z00_1516;

												BgL_arg1527z00_1516 = CDR(((obj_t) BgL_l1122z00_1512));
												{
													obj_t BgL_l1122z00_4947;

													BgL_l1122z00_4947 = BgL_arg1527z00_1516;
													BgL_l1122z00_1512 = BgL_l1122z00_4947;
													goto BgL_zc3z04anonymousza31525ze3z87_1513;
												}
											}
									}
							}
							if (CBOOL(BgL_fz00_1452))
								{	/* R5rs/syntax.scm 425 */
									obj_t BgL_g1045z00_1453;

									BgL_g1045z00_1453 = CDR(((obj_t) BgL_varsz00_1447));
									{
										obj_t BgL_ovarsz00_1456;
										obj_t BgL_nvarsz00_1457;

										BgL_ovarsz00_1456 = BgL_g1045z00_1453;
										BgL_nvarsz00_1457 = BNIL;
									BgL_zc3z04anonymousza31490ze3z87_1458:
										if (NULLP(BgL_ovarsz00_1456))
											{	/* R5rs/syntax.scm 428 */
												if (NULLP(BgL_resz00_1448))
													{
														obj_t BgL_resz00_4957;
														obj_t BgL_varsz00_4956;

														BgL_varsz00_4956 = BgL_nvarsz00_1457;
														BgL_resz00_4957 = BgL_fz00_1452;
														BgL_resz00_1448 = BgL_resz00_4957;
														BgL_varsz00_1447 = BgL_varsz00_4956;
														goto BgL_zc3z04anonymousza31488ze3z87_1449;
													}
												else
													{	/* R5rs/syntax.scm 431 */
														obj_t BgL_arg1494z00_1461;

														if (NULLP(BgL_fz00_1452))
															{	/* R5rs/syntax.scm 431 */
																BgL_arg1494z00_1461 = BNIL;
															}
														else
															{	/* R5rs/syntax.scm 431 */
																obj_t BgL_head1127z00_1465;

																{	/* R5rs/syntax.scm 431 */
																	obj_t BgL_arg1503z00_1481;

																	{	/* R5rs/syntax.scm 431 */
																		obj_t BgL_arg1504z00_1482;
																		obj_t BgL_arg1505z00_1483;

																		BgL_arg1504z00_1482 =
																			CAR(((obj_t) BgL_fz00_1452));
																		BgL_arg1505z00_1483 =
																			CAR(((obj_t) BgL_resz00_1448));
																		BgL_arg1503z00_1481 =
																			BGl_appendzd221011zd2zz__r5_macro_4_3_syntaxz00
																			(BgL_arg1504z00_1482,
																			BgL_arg1505z00_1483);
																	}
																	BgL_head1127z00_1465 =
																		MAKE_YOUNG_PAIR(BgL_arg1503z00_1481, BNIL);
																}
																{	/* R5rs/syntax.scm 431 */
																	obj_t BgL_g1131z00_1466;
																	obj_t BgL_g1132z00_1467;

																	BgL_g1131z00_1466 =
																		CDR(((obj_t) BgL_fz00_1452));
																	BgL_g1132z00_1467 =
																		CDR(((obj_t) BgL_resz00_1448));
																	{
																		obj_t BgL_ll1125z00_2823;
																		obj_t BgL_ll1126z00_2824;
																		obj_t BgL_tail1128z00_2825;

																		BgL_ll1125z00_2823 = BgL_g1131z00_1466;
																		BgL_ll1126z00_2824 = BgL_g1132z00_1467;
																		BgL_tail1128z00_2825 = BgL_head1127z00_1465;
																	BgL_zc3z04anonymousza31496ze3z87_2822:
																		if (NULLP(BgL_ll1125z00_2823))
																			{	/* R5rs/syntax.scm 431 */
																				BgL_arg1494z00_1461 =
																					BgL_head1127z00_1465;
																			}
																		else
																			{	/* R5rs/syntax.scm 431 */
																				obj_t BgL_newtail1129z00_2834;

																				{	/* R5rs/syntax.scm 431 */
																					obj_t BgL_arg1500z00_2835;

																					{	/* R5rs/syntax.scm 431 */
																						obj_t BgL_arg1501z00_2836;
																						obj_t BgL_arg1502z00_2837;

																						BgL_arg1501z00_2836 =
																							CAR(((obj_t) BgL_ll1125z00_2823));
																						BgL_arg1502z00_2837 =
																							CAR(((obj_t) BgL_ll1126z00_2824));
																						BgL_arg1500z00_2835 =
																							BGl_appendzd221011zd2zz__r5_macro_4_3_syntaxz00
																							(BgL_arg1501z00_2836,
																							BgL_arg1502z00_2837);
																					}
																					BgL_newtail1129z00_2834 =
																						MAKE_YOUNG_PAIR(BgL_arg1500z00_2835,
																						BNIL);
																				}
																				SET_CDR(BgL_tail1128z00_2825,
																					BgL_newtail1129z00_2834);
																				{	/* R5rs/syntax.scm 431 */
																					obj_t BgL_arg1498z00_2838;
																					obj_t BgL_arg1499z00_2839;

																					BgL_arg1498z00_2838 =
																						CDR(((obj_t) BgL_ll1125z00_2823));
																					BgL_arg1499z00_2839 =
																						CDR(((obj_t) BgL_ll1126z00_2824));
																					{
																						obj_t BgL_tail1128z00_4985;
																						obj_t BgL_ll1126z00_4984;
																						obj_t BgL_ll1125z00_4983;

																						BgL_ll1125z00_4983 =
																							BgL_arg1498z00_2838;
																						BgL_ll1126z00_4984 =
																							BgL_arg1499z00_2839;
																						BgL_tail1128z00_4985 =
																							BgL_newtail1129z00_2834;
																						BgL_tail1128z00_2825 =
																							BgL_tail1128z00_4985;
																						BgL_ll1126z00_2824 =
																							BgL_ll1126z00_4984;
																						BgL_ll1125z00_2823 =
																							BgL_ll1125z00_4983;
																						goto
																							BgL_zc3z04anonymousza31496ze3z87_2822;
																					}
																				}
																			}
																	}
																}
															}
														{
															obj_t BgL_resz00_4987;
															obj_t BgL_varsz00_4986;

															BgL_varsz00_4986 = BgL_nvarsz00_1457;
															BgL_resz00_4987 = BgL_arg1494z00_1461;
															BgL_resz00_1448 = BgL_resz00_4987;
															BgL_varsz00_1447 = BgL_varsz00_4986;
															goto BgL_zc3z04anonymousza31488ze3z87_1449;
														}
													}
											}
										else
											{	/* R5rs/syntax.scm 432 */
												bool_t BgL_test2772z00_4988;

												{
													obj_t BgL_l1133z00_1500;

													BgL_l1133z00_1500 = BgL_fz00_1452;
												BgL_zc3z04anonymousza31518ze3z87_1501:
													if (NULLP(BgL_l1133z00_1500))
														{	/* R5rs/syntax.scm 432 */
															BgL_test2772z00_4988 = ((bool_t) 0);
														}
													else
														{	/* R5rs/syntax.scm 432 */
															bool_t BgL__ortest_1135z00_1503;

															{	/* R5rs/syntax.scm 432 */
																obj_t BgL_tmpz00_4991;

																BgL_tmpz00_4991 =
																	BGl_assqz00zz__r4_pairs_and_lists_6_3z00(CAR(
																		((obj_t) BgL_ovarsz00_1456)),
																	CAR(((obj_t) BgL_l1133z00_1500)));
																BgL__ortest_1135z00_1503 =
																	PAIRP(BgL_tmpz00_4991);
															}
															if (BgL__ortest_1135z00_1503)
																{	/* R5rs/syntax.scm 432 */
																	BgL_test2772z00_4988 =
																		BgL__ortest_1135z00_1503;
																}
															else
																{
																	obj_t BgL_l1133z00_4999;

																	BgL_l1133z00_4999 =
																		CDR(((obj_t) BgL_l1133z00_1500));
																	BgL_l1133z00_1500 = BgL_l1133z00_4999;
																	goto BgL_zc3z04anonymousza31518ze3z87_1501;
																}
														}
												}
												if (BgL_test2772z00_4988)
													{	/* R5rs/syntax.scm 433 */
														obj_t BgL_arg1513z00_1495;

														BgL_arg1513z00_1495 =
															CDR(((obj_t) BgL_ovarsz00_1456));
														{
															obj_t BgL_ovarsz00_5004;

															BgL_ovarsz00_5004 = BgL_arg1513z00_1495;
															BgL_ovarsz00_1456 = BgL_ovarsz00_5004;
															goto BgL_zc3z04anonymousza31490ze3z87_1458;
														}
													}
												else
													{	/* R5rs/syntax.scm 435 */
														obj_t BgL_arg1514z00_1496;
														obj_t BgL_arg1516z00_1497;

														BgL_arg1514z00_1496 =
															CDR(((obj_t) BgL_ovarsz00_1456));
														{	/* R5rs/syntax.scm 435 */
															obj_t BgL_arg1517z00_1498;

															BgL_arg1517z00_1498 =
																CAR(((obj_t) BgL_ovarsz00_1456));
															BgL_arg1516z00_1497 =
																MAKE_YOUNG_PAIR(BgL_arg1517z00_1498,
																BgL_nvarsz00_1457);
														}
														{
															obj_t BgL_nvarsz00_5011;
															obj_t BgL_ovarsz00_5010;

															BgL_ovarsz00_5010 = BgL_arg1514z00_1496;
															BgL_nvarsz00_5011 = BgL_arg1516z00_1497;
															BgL_nvarsz00_1457 = BgL_nvarsz00_5011;
															BgL_ovarsz00_1456 = BgL_ovarsz00_5010;
															goto BgL_zc3z04anonymousza31490ze3z87_1458;
														}
													}
											}
									}
								}
							else
								{	/* R5rs/syntax.scm 436 */
									obj_t BgL_arg1524z00_1510;

									BgL_arg1524z00_1510 = CDR(((obj_t) BgL_varsz00_1447));
									{
										obj_t BgL_varsz00_5014;

										BgL_varsz00_5014 = BgL_arg1524z00_1510;
										BgL_varsz00_1447 = BgL_varsz00_5014;
										goto BgL_zc3z04anonymousza31488ze3z87_1449;
									}
								}
						}
					}
			}
		}

	}



/* sub~0 */
	obj_t BGl_subze70ze7zz__r5_macro_4_3_syntaxz00(obj_t BgL_kz00_3739,
		obj_t BgL_pz00_1541)
	{
		{	/* R5rs/syntax.scm 442 */
			if (BGl_ellipsiszf3zf3zz__r5_macro_4_3_syntaxz00(BgL_pz00_1541))
				{	/* R5rs/syntax.scm 445 */
					obj_t BgL_arg1543z00_1544;
					obj_t BgL_arg1544z00_1545;

					{	/* R5rs/syntax.scm 445 */
						obj_t BgL_arg1546z00_1546;

						BgL_arg1546z00_1546 = CAR(((obj_t) BgL_pz00_1541));
						BgL_arg1543z00_1544 =
							BGl_subze70ze7zz__r5_macro_4_3_syntaxz00(BgL_kz00_3739,
							BgL_arg1546z00_1546);
					}
					{	/* R5rs/syntax.scm 445 */
						obj_t BgL_arg1547z00_1547;

						{	/* R5rs/syntax.scm 445 */
							obj_t BgL_pairz00_2856;

							BgL_pairz00_2856 = CDR(((obj_t) BgL_pz00_1541));
							BgL_arg1547z00_1547 = CDR(BgL_pairz00_2856);
						}
						BgL_arg1544z00_1545 =
							BGl_subze70ze7zz__r5_macro_4_3_syntaxz00(BgL_kz00_3739,
							BgL_arg1547z00_1547);
					}
					return MAKE_YOUNG_PAIR(BgL_arg1543z00_1544, BgL_arg1544z00_1545);
				}
			else
				{	/* R5rs/syntax.scm 444 */
					if (PAIRP(BgL_pz00_1541))
						{	/* R5rs/syntax.scm 446 */
							return
								BGl_appendzd221011zd2zz__r5_macro_4_3_syntaxz00
								(BGl_subze70ze7zz__r5_macro_4_3_syntaxz00(BgL_kz00_3739,
									CAR(BgL_pz00_1541)),
								BGl_subze70ze7zz__r5_macro_4_3_syntaxz00(BgL_kz00_3739,
									CDR(BgL_pz00_1541)));
						}
					else
						{	/* R5rs/syntax.scm 446 */
							if (SYMBOLP(BgL_pz00_1541))
								{	/* R5rs/syntax.scm 448 */
									if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
											(BgL_pz00_1541, BgL_kz00_3739)))
										{	/* R5rs/syntax.scm 449 */
											return BNIL;
										}
									else
										{	/* R5rs/syntax.scm 449 */
											obj_t BgL_list1557z00_1555;

											BgL_list1557z00_1555 =
												MAKE_YOUNG_PAIR(BgL_pz00_1541, BNIL);
											return BgL_list1557z00_1555;
										}
								}
							else
								{	/* R5rs/syntax.scm 448 */
									return BNIL;
								}
						}
				}
		}

	}



/* ellipsis? */
	bool_t BGl_ellipsiszf3zf3zz__r5_macro_4_3_syntaxz00(obj_t BgL_xz00_35)
	{
		{	/* R5rs/syntax.scm 456 */
			if (PAIRP(BgL_xz00_35))
				{	/* R5rs/syntax.scm 458 */
					bool_t BgL_test2780z00_5040;

					{	/* R5rs/syntax.scm 458 */
						obj_t BgL_tmpz00_5041;

						BgL_tmpz00_5041 = CDR(BgL_xz00_35);
						BgL_test2780z00_5040 = PAIRP(BgL_tmpz00_5041);
					}
					if (BgL_test2780z00_5040)
						{	/* R5rs/syntax.scm 458 */
							return
								(CAR(CDR(BgL_xz00_35)) ==
								BGl_symbol2346z00zz__r5_macro_4_3_syntaxz00);
						}
					else
						{	/* R5rs/syntax.scm 458 */
							return ((bool_t) 0);
						}
				}
			else
				{	/* R5rs/syntax.scm 457 */
					return ((bool_t) 0);
				}
		}

	}



/* hygiene-value* */
	obj_t BGl_hygienezd2valueza2z70zz__r5_macro_4_3_syntaxz00(obj_t BgL_xz00_38)
	{
		{	/* R5rs/syntax.scm 477 */
			if (SYMBOLP(BgL_xz00_38))
				{	/* R5rs/syntax.scm 482 */
					obj_t BgL_sz00_1566;

					{	/* R5rs/syntax.scm 482 */
						obj_t BgL_arg2119z00_2877;

						BgL_arg2119z00_2877 = SYMBOL_TO_STRING(BgL_xz00_38);
						BgL_sz00_1566 =
							BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg2119z00_2877);
					}
					{
						obj_t BgL_sz00_1568;

						BgL_sz00_1568 = BgL_sz00_1566;
					BgL_zc3z04anonymousza31566ze3z87_1569:
						if (bigloo_strcmp_at(BgL_sz00_1568,
								BGl_hygienezd2prefixzd2zz__r5_macro_4_3_syntaxz00, 0L))
							{	/* R5rs/syntax.scm 485 */
								obj_t BgL_arg1571z00_1571;

								{	/* R5rs/syntax.scm 485 */
									long BgL_arg1573z00_1572;

									BgL_arg1573z00_1572 = STRING_LENGTH(BgL_sz00_1568);
									BgL_arg1571z00_1571 =
										c_substring(BgL_sz00_1568,
										BGl_hygienezd2prefixzd2lenz00zz__r5_macro_4_3_syntaxz00,
										BgL_arg1573z00_1572);
								}
								{
									obj_t BgL_sz00_5055;

									BgL_sz00_5055 = BgL_arg1571z00_1571;
									BgL_sz00_1568 = BgL_sz00_5055;
									goto BgL_zc3z04anonymousza31566ze3z87_1569;
								}
							}
						else
							{	/* R5rs/syntax.scm 484 */
								return bstring_to_symbol(BgL_sz00_1568);
							}
					}
				}
			else
				{	/* R5rs/syntax.scm 480 */
					return BgL_xz00_38;
				}
		}

	}



/* hygiene-value */
	obj_t BGl_hygienezd2valuezd2zz__r5_macro_4_3_syntaxz00(obj_t BgL_xz00_39)
	{
		{	/* R5rs/syntax.scm 491 */
			if (SYMBOLP(BgL_xz00_39))
				{	/* R5rs/syntax.scm 494 */
					obj_t BgL_sz00_1575;

					{	/* R5rs/syntax.scm 494 */
						obj_t BgL_arg2119z00_2884;

						BgL_arg2119z00_2884 = SYMBOL_TO_STRING(BgL_xz00_39);
						BgL_sz00_1575 =
							BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg2119z00_2884);
					}
					if (bigloo_strcmp_at(BgL_sz00_1575,
							BGl_hygienezd2prefixzd2zz__r5_macro_4_3_syntaxz00, 0L))
						{	/* R5rs/syntax.scm 497 */
							obj_t BgL_arg1576z00_1577;

							{	/* R5rs/syntax.scm 497 */
								long BgL_arg1578z00_1578;

								BgL_arg1578z00_1578 = STRING_LENGTH(BgL_sz00_1575);
								BgL_arg1576z00_1577 =
									c_substring(BgL_sz00_1575,
									BGl_hygienezd2prefixzd2lenz00zz__r5_macro_4_3_syntaxz00,
									BgL_arg1578z00_1578);
							}
							return bstring_to_symbol(BgL_arg1576z00_1577);
						}
					else
						{	/* R5rs/syntax.scm 495 */
							return BgL_xz00_39;
						}
				}
			else
				{	/* R5rs/syntax.scm 492 */
					return BgL_xz00_39;
				}
		}

	}



/* hygiene-eq? */
	bool_t BGl_hygienezd2eqzf3z21zz__r5_macro_4_3_syntaxz00(obj_t BgL_xz00_40,
		obj_t BgL_idz00_41)
	{
		{	/* R5rs/syntax.scm 503 */
		BGl_hygienezd2eqzf3z21zz__r5_macro_4_3_syntaxz00:
			{	/* R5rs/syntax.scm 504 */
				bool_t BgL_test2785z00_5066;

				if (SYMBOLP(BgL_idz00_41))
					{	/* R5rs/syntax.scm 504 */
						BgL_test2785z00_5066 = SYMBOLP(BgL_xz00_40);
					}
				else
					{	/* R5rs/syntax.scm 504 */
						BgL_test2785z00_5066 = ((bool_t) 0);
					}
				if (BgL_test2785z00_5066)
					{	/* R5rs/syntax.scm 505 */
						bool_t BgL__ortest_1049z00_1581;

						BgL__ortest_1049z00_1581 = (BgL_xz00_40 == BgL_idz00_41);
						if (BgL__ortest_1049z00_1581)
							{	/* R5rs/syntax.scm 505 */
								return BgL__ortest_1049z00_1581;
							}
						else
							{	/* R5rs/syntax.scm 506 */
								bool_t BgL_test2788z00_5072;

								{	/* R5rs/syntax.scm 471 */
									obj_t BgL_sz00_2890;

									{	/* R5rs/syntax.scm 471 */
										obj_t BgL_arg2119z00_2892;

										BgL_arg2119z00_2892 = SYMBOL_TO_STRING(BgL_xz00_40);
										BgL_sz00_2890 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg2119z00_2892);
									}
									BgL_test2788z00_5072 =
										bigloo_strcmp_at(BgL_sz00_2890,
										BGl_hygienezd2prefixzd2zz__r5_macro_4_3_syntaxz00, 0L);
								}
								if (BgL_test2788z00_5072)
									{
										obj_t BgL_xz00_5076;

										BgL_xz00_5076 =
											BGl_hygienezd2valuezd2zz__r5_macro_4_3_syntaxz00
											(BgL_xz00_40);
										BgL_xz00_40 = BgL_xz00_5076;
										goto BGl_hygienezd2eqzf3z21zz__r5_macro_4_3_syntaxz00;
									}
								else
									{	/* R5rs/syntax.scm 506 */
										return ((bool_t) 0);
									}
							}
					}
				else
					{	/* R5rs/syntax.scm 504 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* unhygienize */
	obj_t BGl_unhygieniza7eza7zz__r5_macro_4_3_syntaxz00(obj_t BgL_xz00_42)
	{
		{	/* R5rs/syntax.scm 511 */
			if (SYMBOLP(BgL_xz00_42))
				{	/* R5rs/syntax.scm 465 */
					obj_t BgL_arg1561z00_2893;

					{	/* R5rs/syntax.scm 465 */
						obj_t BgL_arg1562z00_2894;
						obj_t BgL_arg1564z00_2895;

						{	/* R5rs/syntax.scm 465 */
							obj_t BgL_symbolz00_2896;

							BgL_symbolz00_2896 =
								BGl_hygienezd2markzd2zz__r5_macro_4_3_syntaxz00;
							{	/* R5rs/syntax.scm 465 */
								obj_t BgL_arg2119z00_2897;

								BgL_arg2119z00_2897 = SYMBOL_TO_STRING(BgL_symbolz00_2896);
								BgL_arg1562z00_2894 =
									BGl_stringzd2copyzd2zz__r4_strings_6_7z00
									(BgL_arg2119z00_2897);
							}
						}
						{	/* R5rs/syntax.scm 465 */
							obj_t BgL_arg2119z00_2899;

							BgL_arg2119z00_2899 = SYMBOL_TO_STRING(BgL_xz00_42);
							BgL_arg1564z00_2895 =
								BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg2119z00_2899);
						}
						BgL_arg1561z00_2893 =
							string_append(BgL_arg1562z00_2894, BgL_arg1564z00_2895);
					}
					return bstring_to_symbol(BgL_arg1561z00_2893);
				}
			else
				{	/* R5rs/syntax.scm 513 */
					if (PAIRP(BgL_xz00_42))
						{	/* R5rs/syntax.scm 515 */
							return
								MAKE_YOUNG_PAIR(BGl_unhygieniza7eza7zz__r5_macro_4_3_syntaxz00
								(CAR(BgL_xz00_42)),
								BGl_unhygieniza7eza7zz__r5_macro_4_3_syntaxz00(CDR
									(BgL_xz00_42)));
						}
					else
						{	/* R5rs/syntax.scm 515 */
							return BgL_xz00_42;
						}
				}
		}

	}



/* hygienize */
	obj_t BGl_hygieniza7eza7zz__r5_macro_4_3_syntaxz00(obj_t BgL_xz00_43,
		obj_t BgL_envz00_44)
	{
		{	/* R5rs/syntax.scm 523 */
			{
				obj_t BgL_bindingsz00_1608;
				obj_t BgL_bodyz00_1609;
				obj_t BgL_bindingsz00_1605;
				obj_t BgL_bodyz00_1606;
				obj_t BgL_bindingsz00_1602;
				obj_t BgL_bodyz00_1603;
				obj_t BgL_loopz00_1598;
				obj_t BgL_bindingsz00_1599;
				obj_t BgL_bodyz00_1600;
				obj_t BgL_varsz00_1595;
				obj_t BgL_bodyz00_1596;
				obj_t BgL_varz00_1591;

				if (SYMBOLP(BgL_xz00_43))
					{	/* R5rs/syntax.scm 524 */
						BgL_varz00_1591 = BgL_xz00_43;
						{	/* R5rs/syntax.scm 526 */
							bool_t BgL_test2792z00_5095;

							{	/* R5rs/syntax.scm 471 */
								obj_t BgL_sz00_2903;

								{	/* R5rs/syntax.scm 471 */
									obj_t BgL_arg2119z00_2905;

									BgL_arg2119z00_2905 = SYMBOL_TO_STRING(((obj_t) BgL_xz00_43));
									BgL_sz00_2903 =
										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
										(BgL_arg2119z00_2905);
								}
								BgL_test2792z00_5095 =
									bigloo_strcmp_at(BgL_sz00_2903,
									BGl_hygienezd2prefixzd2zz__r5_macro_4_3_syntaxz00, 0L);
							}
							if (BgL_test2792z00_5095)
								{	/* R5rs/syntax.scm 526 */
									return
										BGl_hygienezd2valuezd2zz__r5_macro_4_3_syntaxz00
										(BgL_xz00_43);
								}
							else
								{	/* R5rs/syntax.scm 528 */
									obj_t BgL_oz00_1864;

									BgL_oz00_1864 =
										BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_varz00_1591,
										BgL_envz00_44);
									if (PAIRP(BgL_oz00_1864))
										{	/* R5rs/syntax.scm 529 */
											return CDR(BgL_oz00_1864);
										}
									else
										{	/* R5rs/syntax.scm 529 */
											return BgL_varz00_1591;
										}
								}
						}
					}
				else
					{	/* R5rs/syntax.scm 524 */
						if (PAIRP(BgL_xz00_43))
							{	/* R5rs/syntax.scm 524 */
								if (
									(CAR(
											((obj_t) BgL_xz00_43)) ==
										BGl_symbol2329z00zz__r5_macro_4_3_syntaxz00))
									{	/* R5rs/syntax.scm 524 */
										obj_t BgL_arg1595z00_1621;

										BgL_arg1595z00_1621 = CDR(((obj_t) BgL_xz00_43));
										{	/* R5rs/syntax.scm 533 */
											obj_t BgL_arg1833z00_3298;

											BgL_arg1833z00_3298 =
												BGl_loopze70ze7zz__r5_macro_4_3_syntaxz00(BgL_envz00_44,
												BgL_arg1595z00_1621);
											return
												MAKE_YOUNG_PAIR
												(BGl_symbol2329z00zz__r5_macro_4_3_syntaxz00,
												BgL_arg1833z00_3298);
										}
									}
								else
									{	/* R5rs/syntax.scm 524 */
										obj_t BgL_cdrzd2271zd2_1622;

										BgL_cdrzd2271zd2_1622 = CDR(((obj_t) BgL_xz00_43));
										if (
											(CAR(
													((obj_t) BgL_xz00_43)) ==
												BGl_symbol2443z00zz__r5_macro_4_3_syntaxz00))
											{	/* R5rs/syntax.scm 524 */
												if (PAIRP(BgL_cdrzd2271zd2_1622))
													{	/* R5rs/syntax.scm 524 */
														BgL_varsz00_1595 = CAR(BgL_cdrzd2271zd2_1622);
														BgL_bodyz00_1596 = CDR(BgL_cdrzd2271zd2_1622);
														{	/* R5rs/syntax.scm 535 */
															obj_t BgL_nvarsz00_1867;

															BgL_nvarsz00_1867 =
																BGl_genvarsz00zz__r5_macro_4_3_syntaxz00
																(BgL_varsz00_1595);
															{	/* R5rs/syntax.scm 535 */
																obj_t BgL_nenvz00_1868;

																{	/* R5rs/syntax.scm 536 */
																	obj_t BgL_arg1837z00_1872;

																	{	/* R5rs/syntax.scm 536 */
																		obj_t BgL_ll1136z00_1873;
																		obj_t BgL_ll1137z00_1874;

																		BgL_ll1136z00_1873 =
																			BGl_flattenz00zz__r5_macro_4_3_syntaxz00
																			(BgL_varsz00_1595);
																		BgL_ll1137z00_1874 =
																			BGl_flattenz00zz__r5_macro_4_3_syntaxz00
																			(BgL_nvarsz00_1867);
																		if (NULLP(BgL_ll1136z00_1873))
																			{	/* R5rs/syntax.scm 536 */
																				BgL_arg1837z00_1872 = BNIL;
																			}
																		else
																			{	/* R5rs/syntax.scm 536 */
																				obj_t BgL_head1138z00_1876;

																				{	/* R5rs/syntax.scm 536 */
																					obj_t BgL_arg1847z00_1892;

																					{	/* R5rs/syntax.scm 536 */
																						obj_t BgL_arg1848z00_1893;
																						obj_t BgL_arg1849z00_1894;

																						BgL_arg1848z00_1893 =
																							CAR(BgL_ll1136z00_1873);
																						BgL_arg1849z00_1894 =
																							CAR(((obj_t) BgL_ll1137z00_1874));
																						BgL_arg1847z00_1892 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1848z00_1893,
																							BgL_arg1849z00_1894);
																					}
																					BgL_head1138z00_1876 =
																						MAKE_YOUNG_PAIR(BgL_arg1847z00_1892,
																						BNIL);
																				}
																				{	/* R5rs/syntax.scm 536 */
																					obj_t BgL_g1142z00_1877;
																					obj_t BgL_g1143z00_1878;

																					BgL_g1142z00_1877 =
																						CDR(BgL_ll1136z00_1873);
																					BgL_g1143z00_1878 =
																						CDR(((obj_t) BgL_ll1137z00_1874));
																					{
																						obj_t BgL_ll1136z00_2937;
																						obj_t BgL_ll1137z00_2938;
																						obj_t BgL_tail1139z00_2939;

																						BgL_ll1136z00_2937 =
																							BgL_g1142z00_1877;
																						BgL_ll1137z00_2938 =
																							BgL_g1143z00_1878;
																						BgL_tail1139z00_2939 =
																							BgL_head1138z00_1876;
																					BgL_zc3z04anonymousza31839ze3z87_2936:
																						if (NULLP
																							(BgL_ll1136z00_2937))
																							{	/* R5rs/syntax.scm 536 */
																								BgL_arg1837z00_1872 =
																									BgL_head1138z00_1876;
																							}
																						else
																							{	/* R5rs/syntax.scm 536 */
																								obj_t BgL_newtail1140z00_2948;

																								{	/* R5rs/syntax.scm 536 */
																									obj_t BgL_arg1844z00_2949;

																									{	/* R5rs/syntax.scm 536 */
																										obj_t BgL_arg1845z00_2950;
																										obj_t BgL_arg1846z00_2951;

																										BgL_arg1845z00_2950 =
																											CAR(
																											((obj_t)
																												BgL_ll1136z00_2937));
																										BgL_arg1846z00_2951 =
																											CAR(((obj_t)
																												BgL_ll1137z00_2938));
																										BgL_arg1844z00_2949 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1845z00_2950,
																											BgL_arg1846z00_2951);
																									}
																									BgL_newtail1140z00_2948 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1844z00_2949, BNIL);
																								}
																								SET_CDR(BgL_tail1139z00_2939,
																									BgL_newtail1140z00_2948);
																								{	/* R5rs/syntax.scm 536 */
																									obj_t BgL_arg1842z00_2952;
																									obj_t BgL_arg1843z00_2953;

																									BgL_arg1842z00_2952 =
																										CDR(
																										((obj_t)
																											BgL_ll1136z00_2937));
																									BgL_arg1843z00_2953 =
																										CDR(((obj_t)
																											BgL_ll1137z00_2938));
																									{
																										obj_t BgL_tail1139z00_5151;
																										obj_t BgL_ll1137z00_5150;
																										obj_t BgL_ll1136z00_5149;

																										BgL_ll1136z00_5149 =
																											BgL_arg1842z00_2952;
																										BgL_ll1137z00_5150 =
																											BgL_arg1843z00_2953;
																										BgL_tail1139z00_5151 =
																											BgL_newtail1140z00_2948;
																										BgL_tail1139z00_2939 =
																											BgL_tail1139z00_5151;
																										BgL_ll1137z00_2938 =
																											BgL_ll1137z00_5150;
																										BgL_ll1136z00_2937 =
																											BgL_ll1136z00_5149;
																										goto
																											BgL_zc3z04anonymousza31839ze3z87_2936;
																									}
																								}
																							}
																					}
																				}
																			}
																	}
																	BgL_nenvz00_1868 =
																		BGl_appendzd221011zd2zz__r5_macro_4_3_syntaxz00
																		(BgL_arg1837z00_1872, BgL_envz00_44);
																}
																{	/* R5rs/syntax.scm 536 */

																	{	/* R5rs/syntax.scm 538 */
																		obj_t BgL_arg1834z00_1869;

																		BgL_arg1834z00_1869 =
																			MAKE_YOUNG_PAIR(BgL_nvarsz00_1867,
																			BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																			(BGl_loopze70ze7zz__r5_macro_4_3_syntaxz00
																				(BgL_nenvz00_1868, BgL_bodyz00_1596),
																				BNIL));
																		return
																			MAKE_YOUNG_PAIR
																			(BGl_symbol2443z00zz__r5_macro_4_3_syntaxz00,
																			BgL_arg1834z00_1869);
																	}
																}
															}
														}
													}
												else
													{	/* R5rs/syntax.scm 524 */
														BGL_TAIL return
															BGl_loopze70ze7zz__r5_macro_4_3_syntaxz00
															(BgL_envz00_44, BgL_xz00_43);
													}
											}
										else
											{	/* R5rs/syntax.scm 524 */
												if (
													(CAR(
															((obj_t) BgL_xz00_43)) ==
														BGl_symbol2359z00zz__r5_macro_4_3_syntaxz00))
													{	/* R5rs/syntax.scm 524 */
														if (PAIRP(BgL_cdrzd2271zd2_1622))
															{	/* R5rs/syntax.scm 524 */
																obj_t BgL_carzd2316zd2_1632;
																obj_t BgL_cdrzd2317zd2_1633;

																BgL_carzd2316zd2_1632 =
																	CAR(BgL_cdrzd2271zd2_1622);
																BgL_cdrzd2317zd2_1633 =
																	CDR(BgL_cdrzd2271zd2_1622);
																if (SYMBOLP(BgL_carzd2316zd2_1632))
																	{	/* R5rs/syntax.scm 524 */
																		if (PAIRP(BgL_cdrzd2317zd2_1633))
																			{	/* R5rs/syntax.scm 524 */
																				BgL_loopz00_1598 =
																					BgL_carzd2316zd2_1632;
																				BgL_bindingsz00_1599 =
																					CAR(BgL_cdrzd2317zd2_1633);
																				BgL_bodyz00_1600 =
																					CDR(BgL_cdrzd2317zd2_1633);
																				{	/* R5rs/syntax.scm 540 */
																					obj_t BgL_nloopz00_1895;

																					BgL_nloopz00_1895 =
																						BGl_genvarsz00zz__r5_macro_4_3_syntaxz00
																						(BgL_loopz00_1598);
																					{	/* R5rs/syntax.scm 540 */
																						obj_t BgL_nvarsz00_1896;

																						{	/* R5rs/syntax.scm 541 */
																							obj_t BgL_arg1877z00_1945;

																							if (NULLP(BgL_bindingsz00_1599))
																								{	/* R5rs/syntax.scm 541 */
																									BgL_arg1877z00_1945 = BNIL;
																								}
																							else
																								{	/* R5rs/syntax.scm 541 */
																									obj_t BgL_head1146z00_1948;

																									{	/* R5rs/syntax.scm 541 */
																										obj_t BgL_arg1885z00_1960;

																										{	/* R5rs/syntax.scm 541 */
																											obj_t BgL_pairz00_2960;

																											BgL_pairz00_2960 =
																												CAR(
																												((obj_t)
																													BgL_bindingsz00_1599));
																											BgL_arg1885z00_1960 =
																												CAR(BgL_pairz00_2960);
																										}
																										BgL_head1146z00_1948 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1885z00_1960,
																											BNIL);
																									}
																									{	/* R5rs/syntax.scm 541 */
																										obj_t BgL_g1149z00_1949;

																										BgL_g1149z00_1949 =
																											CDR(
																											((obj_t)
																												BgL_bindingsz00_1599));
																										{
																											obj_t BgL_l1144z00_2981;
																											obj_t
																												BgL_tail1147z00_2982;
																											BgL_l1144z00_2981 =
																												BgL_g1149z00_1949;
																											BgL_tail1147z00_2982 =
																												BgL_head1146z00_1948;
																										BgL_zc3z04anonymousza31879ze3z87_2980:
																											if (NULLP
																												(BgL_l1144z00_2981))
																												{	/* R5rs/syntax.scm 541 */
																													BgL_arg1877z00_1945 =
																														BgL_head1146z00_1948;
																												}
																											else
																												{	/* R5rs/syntax.scm 541 */
																													obj_t
																														BgL_newtail1148z00_2989;
																													{	/* R5rs/syntax.scm 541 */
																														obj_t
																															BgL_arg1883z00_2990;
																														{	/* R5rs/syntax.scm 541 */
																															obj_t
																																BgL_pairz00_2994;
																															BgL_pairz00_2994 =
																																CAR(((obj_t)
																																	BgL_l1144z00_2981));
																															BgL_arg1883z00_2990
																																=
																																CAR
																																(BgL_pairz00_2994);
																														}
																														BgL_newtail1148z00_2989
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1883z00_2990,
																															BNIL);
																													}
																													SET_CDR
																														(BgL_tail1147z00_2982,
																														BgL_newtail1148z00_2989);
																													{	/* R5rs/syntax.scm 541 */
																														obj_t
																															BgL_arg1882z00_2992;
																														BgL_arg1882z00_2992
																															=
																															CDR(((obj_t)
																																BgL_l1144z00_2981));
																														{
																															obj_t
																																BgL_tail1147z00_5191;
																															obj_t
																																BgL_l1144z00_5190;
																															BgL_l1144z00_5190
																																=
																																BgL_arg1882z00_2992;
																															BgL_tail1147z00_5191
																																=
																																BgL_newtail1148z00_2989;
																															BgL_tail1147z00_2982
																																=
																																BgL_tail1147z00_5191;
																															BgL_l1144z00_2981
																																=
																																BgL_l1144z00_5190;
																															goto
																																BgL_zc3z04anonymousza31879ze3z87_2980;
																														}
																													}
																												}
																										}
																									}
																								}
																							BgL_nvarsz00_1896 =
																								BGl_genvarsz00zz__r5_macro_4_3_syntaxz00
																								(BgL_arg1877z00_1945);
																						}
																						{	/* R5rs/syntax.scm 541 */
																							obj_t BgL_nenvz00_1897;

																							{	/* R5rs/syntax.scm 542 */
																								obj_t BgL_arg1866z00_1924;
																								obj_t BgL_arg1868z00_1925;

																								BgL_arg1866z00_1924 =
																									MAKE_YOUNG_PAIR
																									(BgL_loopz00_1598,
																									BgL_nloopz00_1895);
																								{	/* R5rs/syntax.scm 543 */
																									obj_t BgL_arg1869z00_1926;

																									if (NULLP
																										(BgL_bindingsz00_1599))
																										{	/* R5rs/syntax.scm 543 */
																											BgL_arg1869z00_1926 =
																												BNIL;
																										}
																									else
																										{	/* R5rs/syntax.scm 543 */
																											obj_t
																												BgL_head1152z00_1930;
																											BgL_head1152z00_1930 =
																												MAKE_YOUNG_PAIR(BNIL,
																												BNIL);
																											{
																												obj_t
																													BgL_ll1150z00_3028;
																												obj_t
																													BgL_ll1151z00_3029;
																												obj_t
																													BgL_tail1153z00_3030;
																												BgL_ll1150z00_3028 =
																													BgL_bindingsz00_1599;
																												BgL_ll1151z00_3029 =
																													BgL_nvarsz00_1896;
																												BgL_tail1153z00_3030 =
																													BgL_head1152z00_1930;
																											BgL_zc3z04anonymousza31871ze3z87_3027:
																												if (NULLP
																													(BgL_ll1150z00_3028))
																													{	/* R5rs/syntax.scm 543 */
																														BgL_arg1869z00_1926
																															=
																															CDR
																															(BgL_head1152z00_1930);
																													}
																												else
																													{	/* R5rs/syntax.scm 543 */
																														obj_t
																															BgL_newtail1154z00_3040;
																														{	/* R5rs/syntax.scm 543 */
																															obj_t
																																BgL_arg1875z00_3041;
																															{	/* R5rs/syntax.scm 543 */
																																obj_t
																																	BgL_bz00_3042;
																																obj_t
																																	BgL_vz00_3043;
																																BgL_bz00_3042 =
																																	CAR(((obj_t)
																																		BgL_ll1150z00_3028));
																																BgL_vz00_3043 =
																																	CAR(((obj_t)
																																		BgL_ll1151z00_3029));
																																{	/* R5rs/syntax.scm 544 */
																																	obj_t
																																		BgL_arg1876z00_3044;
																																	BgL_arg1876z00_3044
																																		=
																																		CAR(((obj_t)
																																			BgL_bz00_3042));
																																	BgL_arg1875z00_3041
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1876z00_3044,
																																		BgL_vz00_3043);
																																}
																															}
																															BgL_newtail1154z00_3040
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1875z00_3041,
																																BNIL);
																														}
																														SET_CDR
																															(BgL_tail1153z00_3030,
																															BgL_newtail1154z00_3040);
																														{	/* R5rs/syntax.scm 543 */
																															obj_t
																																BgL_arg1873z00_3045;
																															obj_t
																																BgL_arg1874z00_3046;
																															BgL_arg1873z00_3045
																																=
																																CDR(((obj_t)
																																	BgL_ll1150z00_3028));
																															BgL_arg1874z00_3046
																																=
																																CDR(((obj_t)
																																	BgL_ll1151z00_3029));
																															{
																																obj_t
																																	BgL_tail1153z00_5215;
																																obj_t
																																	BgL_ll1151z00_5214;
																																obj_t
																																	BgL_ll1150z00_5213;
																																BgL_ll1150z00_5213
																																	=
																																	BgL_arg1873z00_3045;
																																BgL_ll1151z00_5214
																																	=
																																	BgL_arg1874z00_3046;
																																BgL_tail1153z00_5215
																																	=
																																	BgL_newtail1154z00_3040;
																																BgL_tail1153z00_3030
																																	=
																																	BgL_tail1153z00_5215;
																																BgL_ll1151z00_3029
																																	=
																																	BgL_ll1151z00_5214;
																																BgL_ll1150z00_3028
																																	=
																																	BgL_ll1150z00_5213;
																																goto
																																	BgL_zc3z04anonymousza31871ze3z87_3027;
																															}
																														}
																													}
																											}
																										}
																									BgL_arg1868z00_1925 =
																										BGl_appendzd221011zd2zz__r5_macro_4_3_syntaxz00
																										(BgL_arg1869z00_1926,
																										BgL_envz00_44);
																								}
																								BgL_nenvz00_1897 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1866z00_1924,
																									BgL_arg1868z00_1925);
																							}
																							{	/* R5rs/syntax.scm 542 */

																								{	/* R5rs/syntax.scm 547 */
																									obj_t BgL_arg1850z00_1898;

																									{	/* R5rs/syntax.scm 547 */
																										obj_t BgL_arg1851z00_1899;

																										{	/* R5rs/syntax.scm 547 */
																											obj_t BgL_arg1852z00_1900;
																											obj_t BgL_arg1853z00_1901;

																											if (NULLP
																												(BgL_bindingsz00_1599))
																												{	/* R5rs/syntax.scm 547 */
																													BgL_arg1852z00_1900 =
																														BNIL;
																												}
																											else
																												{	/* R5rs/syntax.scm 547 */
																													obj_t
																														BgL_head1158z00_1905;
																													BgL_head1158z00_1905 =
																														MAKE_YOUNG_PAIR
																														(BNIL, BNIL);
																													{
																														obj_t
																															BgL_ll1156z00_1907;
																														obj_t
																															BgL_ll1157z00_1908;
																														obj_t
																															BgL_tail1159z00_1909;
																														BgL_ll1156z00_1907 =
																															BgL_bindingsz00_1599;
																														BgL_ll1157z00_1908 =
																															BgL_nvarsz00_1896;
																														BgL_tail1159z00_1909
																															=
																															BgL_head1158z00_1905;
																													BgL_zc3z04anonymousza31855ze3z87_1910:
																														if (NULLP
																															(BgL_ll1156z00_1907))
																															{	/* R5rs/syntax.scm 547 */
																																BgL_arg1852z00_1900
																																	=
																																	CDR
																																	(BgL_head1158z00_1905);
																															}
																														else
																															{	/* R5rs/syntax.scm 547 */
																																obj_t
																																	BgL_newtail1160z00_1912;
																																{	/* R5rs/syntax.scm 547 */
																																	obj_t
																																		BgL_arg1859z00_1915;
																																	{	/* R5rs/syntax.scm 547 */
																																		obj_t
																																			BgL_bz00_1916;
																																		obj_t
																																			BgL_vz00_1917;
																																		BgL_bz00_1916
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_ll1156z00_1907));
																																		BgL_vz00_1917
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_ll1157z00_1908));
																																		{	/* R5rs/syntax.scm 548 */
																																			obj_t
																																				BgL_arg1860z00_1918;
																																			{	/* R5rs/syntax.scm 548 */
																																				obj_t
																																					BgL_arg1863z00_1921;
																																				{	/* R5rs/syntax.scm 548 */
																																					obj_t
																																						BgL_pairz00_3060;
																																					BgL_pairz00_3060
																																						=
																																						CDR(
																																						((obj_t) BgL_bz00_1916));
																																					BgL_arg1863z00_1921
																																						=
																																						CAR
																																						(BgL_pairz00_3060);
																																				}
																																				BgL_arg1860z00_1918
																																					=
																																					BGl_hygieniza7eza7zz__r5_macro_4_3_syntaxz00
																																					(BgL_arg1863z00_1921,
																																					BgL_envz00_44);
																																			}
																																			{	/* R5rs/syntax.scm 548 */
																																				obj_t
																																					BgL_list1861z00_1919;
																																				{	/* R5rs/syntax.scm 548 */
																																					obj_t
																																						BgL_arg1862z00_1920;
																																					BgL_arg1862z00_1920
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg1860z00_1918,
																																						BNIL);
																																					BgL_list1861z00_1919
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_vz00_1917,
																																						BgL_arg1862z00_1920);
																																				}
																																				BgL_arg1859z00_1915
																																					=
																																					BgL_list1861z00_1919;
																																			}
																																		}
																																	}
																																	BgL_newtail1160z00_1912
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1859z00_1915,
																																		BNIL);
																																}
																																SET_CDR
																																	(BgL_tail1159z00_1909,
																																	BgL_newtail1160z00_1912);
																																{	/* R5rs/syntax.scm 547 */
																																	obj_t
																																		BgL_arg1857z00_1913;
																																	obj_t
																																		BgL_arg1858z00_1914;
																																	BgL_arg1857z00_1913
																																		=
																																		CDR(((obj_t)
																																			BgL_ll1156z00_1907));
																																	BgL_arg1858z00_1914
																																		=
																																		CDR(((obj_t)
																																			BgL_ll1157z00_1908));
																																	{
																																		obj_t
																																			BgL_tail1159z00_5242;
																																		obj_t
																																			BgL_ll1157z00_5241;
																																		obj_t
																																			BgL_ll1156z00_5240;
																																		BgL_ll1156z00_5240
																																			=
																																			BgL_arg1857z00_1913;
																																		BgL_ll1157z00_5241
																																			=
																																			BgL_arg1858z00_1914;
																																		BgL_tail1159z00_5242
																																			=
																																			BgL_newtail1160z00_1912;
																																		BgL_tail1159z00_1909
																																			=
																																			BgL_tail1159z00_5242;
																																		BgL_ll1157z00_1908
																																			=
																																			BgL_ll1157z00_5241;
																																		BgL_ll1156z00_1907
																																			=
																																			BgL_ll1156z00_5240;
																																		goto
																																			BgL_zc3z04anonymousza31855ze3z87_1910;
																																	}
																																}
																															}
																													}
																												}
																											BgL_arg1853z00_1901 =
																												BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																												(BGl_loopze70ze7zz__r5_macro_4_3_syntaxz00
																												(BgL_nenvz00_1897,
																													BgL_bodyz00_1600),
																												BNIL);
																											BgL_arg1851z00_1899 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1852z00_1900,
																												BgL_arg1853z00_1901);
																										}
																										BgL_arg1850z00_1898 =
																											MAKE_YOUNG_PAIR
																											(BgL_nloopz00_1895,
																											BgL_arg1851z00_1899);
																									}
																									return
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2359z00zz__r5_macro_4_3_syntaxz00,
																										BgL_arg1850z00_1898);
																								}
																							}
																						}
																					}
																				}
																			}
																		else
																			{	/* R5rs/syntax.scm 524 */
																				obj_t BgL_cdrzd2333zd2_1638;

																				BgL_cdrzd2333zd2_1638 =
																					CDR(((obj_t) BgL_xz00_43));
																				{	/* R5rs/syntax.scm 524 */
																					obj_t BgL_arg1610z00_1639;
																					obj_t BgL_arg1611z00_1640;

																					BgL_arg1610z00_1639 =
																						CAR(
																						((obj_t) BgL_cdrzd2333zd2_1638));
																					BgL_arg1611z00_1640 =
																						CDR(
																						((obj_t) BgL_cdrzd2333zd2_1638));
																					BgL_bindingsz00_1602 =
																						BgL_arg1610z00_1639;
																					BgL_bodyz00_1603 =
																						BgL_arg1611z00_1640;
																				BgL_tagzd2238zd2_1604:
																					{	/* R5rs/syntax.scm 552 */
																						obj_t BgL_nvarsz00_1962;

																						{	/* R5rs/syntax.scm 552 */
																							obj_t BgL_arg1914z00_2008;

																							if (NULLP(BgL_bindingsz00_1602))
																								{	/* R5rs/syntax.scm 552 */
																									BgL_arg1914z00_2008 = BNIL;
																								}
																							else
																								{	/* R5rs/syntax.scm 552 */
																									obj_t BgL_head1164z00_2011;

																									{	/* R5rs/syntax.scm 552 */
																										obj_t BgL_arg1923z00_2023;

																										{	/* R5rs/syntax.scm 552 */
																											obj_t BgL_pairz00_3066;

																											BgL_pairz00_3066 =
																												CAR(
																												((obj_t)
																													BgL_bindingsz00_1602));
																											BgL_arg1923z00_2023 =
																												CAR(BgL_pairz00_3066);
																										}
																										BgL_head1164z00_2011 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1923z00_2023,
																											BNIL);
																									}
																									{	/* R5rs/syntax.scm 552 */
																										obj_t BgL_g1167z00_2012;

																										BgL_g1167z00_2012 =
																											CDR(
																											((obj_t)
																												BgL_bindingsz00_1602));
																										{
																											obj_t BgL_l1162z00_3087;
																											obj_t
																												BgL_tail1165z00_3088;
																											BgL_l1162z00_3087 =
																												BgL_g1167z00_2012;
																											BgL_tail1165z00_3088 =
																												BgL_head1164z00_2011;
																										BgL_zc3z04anonymousza31916ze3z87_3086:
																											if (NULLP
																												(BgL_l1162z00_3087))
																												{	/* R5rs/syntax.scm 552 */
																													BgL_arg1914z00_2008 =
																														BgL_head1164z00_2011;
																												}
																											else
																												{	/* R5rs/syntax.scm 552 */
																													obj_t
																														BgL_newtail1166z00_3095;
																													{	/* R5rs/syntax.scm 552 */
																														obj_t
																															BgL_arg1919z00_3096;
																														{	/* R5rs/syntax.scm 552 */
																															obj_t
																																BgL_pairz00_3100;
																															BgL_pairz00_3100 =
																																CAR(((obj_t)
																																	BgL_l1162z00_3087));
																															BgL_arg1919z00_3096
																																=
																																CAR
																																(BgL_pairz00_3100);
																														}
																														BgL_newtail1166z00_3095
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1919z00_3096,
																															BNIL);
																													}
																													SET_CDR
																														(BgL_tail1165z00_3088,
																														BgL_newtail1166z00_3095);
																													{	/* R5rs/syntax.scm 552 */
																														obj_t
																															BgL_arg1918z00_3098;
																														BgL_arg1918z00_3098
																															=
																															CDR(((obj_t)
																																BgL_l1162z00_3087));
																														{
																															obj_t
																																BgL_tail1165z00_5274;
																															obj_t
																																BgL_l1162z00_5273;
																															BgL_l1162z00_5273
																																=
																																BgL_arg1918z00_3098;
																															BgL_tail1165z00_5274
																																=
																																BgL_newtail1166z00_3095;
																															BgL_tail1165z00_3088
																																=
																																BgL_tail1165z00_5274;
																															BgL_l1162z00_3087
																																=
																																BgL_l1162z00_5273;
																															goto
																																BgL_zc3z04anonymousza31916ze3z87_3086;
																														}
																													}
																												}
																										}
																									}
																								}
																							BgL_nvarsz00_1962 =
																								BGl_genvarsz00zz__r5_macro_4_3_syntaxz00
																								(BgL_arg1914z00_2008);
																						}
																						{	/* R5rs/syntax.scm 552 */
																							obj_t BgL_nenvz00_1963;

																							{	/* R5rs/syntax.scm 553 */
																								obj_t BgL_arg1904z00_1989;

																								if (NULLP(BgL_bindingsz00_1602))
																									{	/* R5rs/syntax.scm 553 */
																										BgL_arg1904z00_1989 = BNIL;
																									}
																								else
																									{	/* R5rs/syntax.scm 553 */
																										obj_t BgL_head1170z00_1993;

																										BgL_head1170z00_1993 =
																											MAKE_YOUNG_PAIR(BNIL,
																											BNIL);
																										{
																											obj_t BgL_ll1168z00_3134;
																											obj_t BgL_ll1169z00_3135;
																											obj_t
																												BgL_tail1171z00_3136;
																											BgL_ll1168z00_3134 =
																												BgL_bindingsz00_1602;
																											BgL_ll1169z00_3135 =
																												BgL_nvarsz00_1962;
																											BgL_tail1171z00_3136 =
																												BgL_head1170z00_1993;
																										BgL_zc3z04anonymousza31906ze3z87_3133:
																											if (NULLP
																												(BgL_ll1168z00_3134))
																												{	/* R5rs/syntax.scm 553 */
																													BgL_arg1904z00_1989 =
																														CDR
																														(BgL_head1170z00_1993);
																												}
																											else
																												{	/* R5rs/syntax.scm 553 */
																													obj_t
																														BgL_newtail1172z00_3146;
																													{	/* R5rs/syntax.scm 553 */
																														obj_t
																															BgL_arg1912z00_3147;
																														{	/* R5rs/syntax.scm 553 */
																															obj_t
																																BgL_bz00_3148;
																															obj_t
																																BgL_vz00_3149;
																															BgL_bz00_3148 =
																																CAR(((obj_t)
																																	BgL_ll1168z00_3134));
																															BgL_vz00_3149 =
																																CAR(((obj_t)
																																	BgL_ll1169z00_3135));
																															{	/* R5rs/syntax.scm 554 */
																																obj_t
																																	BgL_arg1913z00_3150;
																																BgL_arg1913z00_3150
																																	=
																																	CAR(((obj_t)
																																		BgL_bz00_3148));
																																BgL_arg1912z00_3147
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1913z00_3150,
																																	BgL_vz00_3149);
																															}
																														}
																														BgL_newtail1172z00_3146
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1912z00_3147,
																															BNIL);
																													}
																													SET_CDR
																														(BgL_tail1171z00_3136,
																														BgL_newtail1172z00_3146);
																													{	/* R5rs/syntax.scm 553 */
																														obj_t
																															BgL_arg1910z00_3151;
																														obj_t
																															BgL_arg1911z00_3152;
																														BgL_arg1910z00_3151
																															=
																															CDR(((obj_t)
																																BgL_ll1168z00_3134));
																														BgL_arg1911z00_3152
																															=
																															CDR(((obj_t)
																																BgL_ll1169z00_3135));
																														{
																															obj_t
																																BgL_tail1171z00_5297;
																															obj_t
																																BgL_ll1169z00_5296;
																															obj_t
																																BgL_ll1168z00_5295;
																															BgL_ll1168z00_5295
																																=
																																BgL_arg1910z00_3151;
																															BgL_ll1169z00_5296
																																=
																																BgL_arg1911z00_3152;
																															BgL_tail1171z00_5297
																																=
																																BgL_newtail1172z00_3146;
																															BgL_tail1171z00_3136
																																=
																																BgL_tail1171z00_5297;
																															BgL_ll1169z00_3135
																																=
																																BgL_ll1169z00_5296;
																															BgL_ll1168z00_3134
																																=
																																BgL_ll1168z00_5295;
																															goto
																																BgL_zc3z04anonymousza31906ze3z87_3133;
																														}
																													}
																												}
																										}
																									}
																								BgL_nenvz00_1963 =
																									BGl_appendzd221011zd2zz__r5_macro_4_3_syntaxz00
																									(BgL_arg1904z00_1989,
																									BgL_envz00_44);
																							}
																							{	/* R5rs/syntax.scm 553 */

																								{	/* R5rs/syntax.scm 557 */
																									obj_t BgL_arg1888z00_1964;

																									{	/* R5rs/syntax.scm 557 */
																										obj_t BgL_arg1889z00_1965;
																										obj_t BgL_arg1890z00_1966;

																										if (NULLP
																											(BgL_bindingsz00_1602))
																											{	/* R5rs/syntax.scm 557 */
																												BgL_arg1889z00_1965 =
																													BNIL;
																											}
																										else
																											{	/* R5rs/syntax.scm 557 */
																												obj_t
																													BgL_head1176z00_1970;
																												BgL_head1176z00_1970 =
																													MAKE_YOUNG_PAIR(BNIL,
																													BNIL);
																												{
																													obj_t
																														BgL_ll1174z00_1972;
																													obj_t
																														BgL_ll1175z00_1973;
																													obj_t
																														BgL_tail1177z00_1974;
																													BgL_ll1174z00_1972 =
																														BgL_bindingsz00_1602;
																													BgL_ll1175z00_1973 =
																														BgL_nvarsz00_1962;
																													BgL_tail1177z00_1974 =
																														BgL_head1176z00_1970;
																												BgL_zc3z04anonymousza31892ze3z87_1975:
																													if (NULLP
																														(BgL_ll1174z00_1972))
																														{	/* R5rs/syntax.scm 557 */
																															BgL_arg1889z00_1965
																																=
																																CDR
																																(BgL_head1176z00_1970);
																														}
																													else
																														{	/* R5rs/syntax.scm 557 */
																															obj_t
																																BgL_newtail1178z00_1977;
																															{	/* R5rs/syntax.scm 557 */
																																obj_t
																																	BgL_arg1897z00_1980;
																																{	/* R5rs/syntax.scm 557 */
																																	obj_t
																																		BgL_bz00_1981;
																																	obj_t
																																		BgL_vz00_1982;
																																	BgL_bz00_1981
																																		=
																																		CAR(((obj_t)
																																			BgL_ll1174z00_1972));
																																	BgL_vz00_1982
																																		=
																																		CAR(((obj_t)
																																			BgL_ll1175z00_1973));
																																	{	/* R5rs/syntax.scm 558 */
																																		obj_t
																																			BgL_arg1898z00_1983;
																																		{	/* R5rs/syntax.scm 558 */
																																			obj_t
																																				BgL_arg1902z00_1986;
																																			{	/* R5rs/syntax.scm 558 */
																																				obj_t
																																					BgL_pairz00_3166;
																																				BgL_pairz00_3166
																																					=
																																					CDR((
																																						(obj_t)
																																						BgL_bz00_1981));
																																				BgL_arg1902z00_1986
																																					=
																																					CAR
																																					(BgL_pairz00_3166);
																																			}
																																			BgL_arg1898z00_1983
																																				=
																																				BGl_hygieniza7eza7zz__r5_macro_4_3_syntaxz00
																																				(BgL_arg1902z00_1986,
																																				BgL_envz00_44);
																																		}
																																		{	/* R5rs/syntax.scm 558 */
																																			obj_t
																																				BgL_list1899z00_1984;
																																			{	/* R5rs/syntax.scm 558 */
																																				obj_t
																																					BgL_arg1901z00_1985;
																																				BgL_arg1901z00_1985
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1898z00_1983,
																																					BNIL);
																																				BgL_list1899z00_1984
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_vz00_1982,
																																					BgL_arg1901z00_1985);
																																			}
																																			BgL_arg1897z00_1980
																																				=
																																				BgL_list1899z00_1984;
																																		}
																																	}
																																}
																																BgL_newtail1178z00_1977
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1897z00_1980,
																																	BNIL);
																															}
																															SET_CDR
																																(BgL_tail1177z00_1974,
																																BgL_newtail1178z00_1977);
																															{	/* R5rs/syntax.scm 557 */
																																obj_t
																																	BgL_arg1894z00_1978;
																																obj_t
																																	BgL_arg1896z00_1979;
																																BgL_arg1894z00_1978
																																	=
																																	CDR(((obj_t)
																																		BgL_ll1174z00_1972));
																																BgL_arg1896z00_1979
																																	=
																																	CDR(((obj_t)
																																		BgL_ll1175z00_1973));
																																{
																																	obj_t
																																		BgL_tail1177z00_5323;
																																	obj_t
																																		BgL_ll1175z00_5322;
																																	obj_t
																																		BgL_ll1174z00_5321;
																																	BgL_ll1174z00_5321
																																		=
																																		BgL_arg1894z00_1978;
																																	BgL_ll1175z00_5322
																																		=
																																		BgL_arg1896z00_1979;
																																	BgL_tail1177z00_5323
																																		=
																																		BgL_newtail1178z00_1977;
																																	BgL_tail1177z00_1974
																																		=
																																		BgL_tail1177z00_5323;
																																	BgL_ll1175z00_1973
																																		=
																																		BgL_ll1175z00_5322;
																																	BgL_ll1174z00_1972
																																		=
																																		BgL_ll1174z00_5321;
																																	goto
																																		BgL_zc3z04anonymousza31892ze3z87_1975;
																																}
																															}
																														}
																												}
																											}
																										BgL_arg1890z00_1966 =
																											BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																											(BGl_loopze70ze7zz__r5_macro_4_3_syntaxz00
																											(BgL_nenvz00_1963,
																												BgL_bodyz00_1603),
																											BNIL);
																										BgL_arg1888z00_1964 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1889z00_1965,
																											BgL_arg1890z00_1966);
																									}
																									return
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2359z00zz__r5_macro_4_3_syntaxz00,
																										BgL_arg1888z00_1964);
																								}
																							}
																						}
																					}
																				}
																			}
																	}
																else
																	{	/* R5rs/syntax.scm 524 */
																		obj_t BgL_cdrzd2349zd2_1641;

																		BgL_cdrzd2349zd2_1641 =
																			CDR(((obj_t) BgL_xz00_43));
																		{	/* R5rs/syntax.scm 524 */
																			obj_t BgL_arg1612z00_1642;
																			obj_t BgL_arg1613z00_1643;

																			BgL_arg1612z00_1642 =
																				CAR(((obj_t) BgL_cdrzd2349zd2_1641));
																			BgL_arg1613z00_1643 =
																				CDR(((obj_t) BgL_cdrzd2349zd2_1641));
																			{
																				obj_t BgL_bodyz00_5335;
																				obj_t BgL_bindingsz00_5334;

																				BgL_bindingsz00_5334 =
																					BgL_arg1612z00_1642;
																				BgL_bodyz00_5335 = BgL_arg1613z00_1643;
																				BgL_bodyz00_1603 = BgL_bodyz00_5335;
																				BgL_bindingsz00_1602 =
																					BgL_bindingsz00_5334;
																				goto BgL_tagzd2238zd2_1604;
																			}
																		}
																	}
															}
														else
															{	/* R5rs/syntax.scm 524 */
																BGL_TAIL return
																	BGl_loopze70ze7zz__r5_macro_4_3_syntaxz00
																	(BgL_envz00_44, BgL_xz00_43);
															}
													}
												else
													{	/* R5rs/syntax.scm 524 */
														obj_t BgL_cdrzd2388zd2_1644;

														BgL_cdrzd2388zd2_1644 = CDR(((obj_t) BgL_xz00_43));
														if (
															(CAR(
																	((obj_t) BgL_xz00_43)) ==
																BGl_symbol2456z00zz__r5_macro_4_3_syntaxz00))
															{	/* R5rs/syntax.scm 524 */
																if (PAIRP(BgL_cdrzd2388zd2_1644))
																	{	/* R5rs/syntax.scm 524 */
																		BgL_bindingsz00_1605 =
																			CAR(BgL_cdrzd2388zd2_1644);
																		BgL_bodyz00_1606 =
																			CDR(BgL_cdrzd2388zd2_1644);
																		{
																			obj_t BgL_bindingsz00_2027;
																			obj_t BgL_nbindingsz00_2028;
																			obj_t BgL_nenvz00_2029;

																			BgL_bindingsz00_2027 =
																				BgL_bindingsz00_1605;
																			BgL_nbindingsz00_2028 = BNIL;
																			BgL_nenvz00_2029 = BgL_envz00_44;
																		BgL_zc3z04anonymousza31925ze3z87_2030:
																			if (NULLP(BgL_bindingsz00_2027))
																				{	/* R5rs/syntax.scm 566 */
																					obj_t BgL_arg1927z00_2032;

																					BgL_arg1927z00_2032 =
																						MAKE_YOUNG_PAIR(bgl_reverse
																						(BgL_nbindingsz00_2028),
																						BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																						(BGl_loopze70ze7zz__r5_macro_4_3_syntaxz00
																							(BgL_nenvz00_2029,
																								BgL_bodyz00_1606), BNIL));
																					return
																						MAKE_YOUNG_PAIR
																						(BGl_symbol2456z00zz__r5_macro_4_3_syntaxz00,
																						BgL_arg1927z00_2032);
																				}
																			else
																				{	/* R5rs/syntax.scm 567 */
																					obj_t BgL_varz00_2036;

																					{	/* R5rs/syntax.scm 567 */
																						obj_t BgL_pairz00_3174;

																						BgL_pairz00_3174 =
																							CAR(
																							((obj_t) BgL_bindingsz00_2027));
																						BgL_varz00_2036 =
																							CAR(BgL_pairz00_3174);
																					}
																					{	/* R5rs/syntax.scm 568 */
																						obj_t BgL_nvarz00_2038;

																						BgL_nvarz00_2038 =
																							BGl_genvarsz00zz__r5_macro_4_3_syntaxz00
																							(BgL_varz00_2036);
																						{	/* R5rs/syntax.scm 569 */
																							obj_t BgL_nenvz00_2039;

																							{	/* R5rs/syntax.scm 570 */
																								obj_t BgL_arg1937z00_2046;

																								BgL_arg1937z00_2046 =
																									MAKE_YOUNG_PAIR
																									(BgL_varz00_2036,
																									BgL_nvarz00_2038);
																								BgL_nenvz00_2039 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1937z00_2046,
																									BgL_envz00_44);
																							}
																							{	/* R5rs/syntax.scm 570 */

																								{	/* R5rs/syntax.scm 571 */
																									obj_t BgL_arg1931z00_2040;
																									obj_t BgL_arg1932z00_2041;

																									BgL_arg1931z00_2040 =
																										CDR(
																										((obj_t)
																											BgL_bindingsz00_2027));
																									{	/* R5rs/syntax.scm 572 */
																										obj_t BgL_arg1933z00_2042;

																										{	/* R5rs/syntax.scm 572 */
																											obj_t BgL_arg1934z00_2043;

																											BgL_arg1934z00_2043 =
																												BGl_hygieniza7eza7zz__r5_macro_4_3_syntaxz00
																												(BgL_varz00_2036,
																												BgL_envz00_44);
																											{	/* R5rs/syntax.scm 572 */
																												obj_t
																													BgL_list1935z00_2044;
																												{	/* R5rs/syntax.scm 572 */
																													obj_t
																														BgL_arg1936z00_2045;
																													BgL_arg1936z00_2045 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1934z00_2043,
																														BNIL);
																													BgL_list1935z00_2044 =
																														MAKE_YOUNG_PAIR
																														(BgL_varz00_2036,
																														BgL_arg1936z00_2045);
																												}
																												BgL_arg1933z00_2042 =
																													BgL_list1935z00_2044;
																											}
																										}
																										BgL_arg1932z00_2041 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1933z00_2042,
																											BgL_nbindingsz00_2028);
																									}
																									{
																										obj_t BgL_nenvz00_5366;
																										obj_t BgL_nbindingsz00_5365;
																										obj_t BgL_bindingsz00_5364;

																										BgL_bindingsz00_5364 =
																											BgL_arg1931z00_2040;
																										BgL_nbindingsz00_5365 =
																											BgL_arg1932z00_2041;
																										BgL_nenvz00_5366 =
																											BgL_nenvz00_2039;
																										BgL_nenvz00_2029 =
																											BgL_nenvz00_5366;
																										BgL_nbindingsz00_2028 =
																											BgL_nbindingsz00_5365;
																										BgL_bindingsz00_2027 =
																											BgL_bindingsz00_5364;
																										goto
																											BgL_zc3z04anonymousza31925ze3z87_2030;
																									}
																								}
																							}
																						}
																					}
																				}
																		}
																	}
																else
																	{	/* R5rs/syntax.scm 524 */
																		BGL_TAIL return
																			BGl_loopze70ze7zz__r5_macro_4_3_syntaxz00
																			(BgL_envz00_44, BgL_xz00_43);
																	}
															}
														else
															{	/* R5rs/syntax.scm 524 */
																if (
																	(CAR(
																			((obj_t) BgL_xz00_43)) ==
																		BGl_symbol2452z00zz__r5_macro_4_3_syntaxz00))
																	{	/* R5rs/syntax.scm 524 */
																		if (PAIRP(BgL_cdrzd2388zd2_1644))
																			{	/* R5rs/syntax.scm 524 */
																				BgL_bindingsz00_1608 =
																					CAR(BgL_cdrzd2388zd2_1644);
																				BgL_bodyz00_1609 =
																					CDR(BgL_cdrzd2388zd2_1644);
																				{	/* R5rs/syntax.scm 575 */
																					obj_t BgL_nvarsz00_2048;

																					{	/* R5rs/syntax.scm 575 */
																						obj_t BgL_arg1960z00_2094;

																						if (NULLP(BgL_bindingsz00_1608))
																							{	/* R5rs/syntax.scm 575 */
																								BgL_arg1960z00_2094 = BNIL;
																							}
																						else
																							{	/* R5rs/syntax.scm 575 */
																								obj_t BgL_head1182z00_2097;

																								{	/* R5rs/syntax.scm 575 */
																									obj_t BgL_arg1967z00_2109;

																									{	/* R5rs/syntax.scm 575 */
																										obj_t BgL_pairz00_3184;

																										BgL_pairz00_3184 =
																											CAR(
																											((obj_t)
																												BgL_bindingsz00_1608));
																										BgL_arg1967z00_2109 =
																											CAR(BgL_pairz00_3184);
																									}
																									BgL_head1182z00_2097 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1967z00_2109, BNIL);
																								}
																								{	/* R5rs/syntax.scm 575 */
																									obj_t BgL_g1185z00_2098;

																									BgL_g1185z00_2098 =
																										CDR(
																										((obj_t)
																											BgL_bindingsz00_1608));
																									{
																										obj_t BgL_l1180z00_3205;
																										obj_t BgL_tail1183z00_3206;

																										BgL_l1180z00_3205 =
																											BgL_g1185z00_2098;
																										BgL_tail1183z00_3206 =
																											BgL_head1182z00_2097;
																									BgL_zc3z04anonymousza31962ze3z87_3204:
																										if (NULLP
																											(BgL_l1180z00_3205))
																											{	/* R5rs/syntax.scm 575 */
																												BgL_arg1960z00_2094 =
																													BgL_head1182z00_2097;
																											}
																										else
																											{	/* R5rs/syntax.scm 575 */
																												obj_t
																													BgL_newtail1184z00_3213;
																												{	/* R5rs/syntax.scm 575 */
																													obj_t
																														BgL_arg1965z00_3214;
																													{	/* R5rs/syntax.scm 575 */
																														obj_t
																															BgL_pairz00_3218;
																														BgL_pairz00_3218 =
																															CAR(((obj_t)
																																BgL_l1180z00_3205));
																														BgL_arg1965z00_3214
																															=
																															CAR
																															(BgL_pairz00_3218);
																													}
																													BgL_newtail1184z00_3213
																														=
																														MAKE_YOUNG_PAIR
																														(BgL_arg1965z00_3214,
																														BNIL);
																												}
																												SET_CDR
																													(BgL_tail1183z00_3206,
																													BgL_newtail1184z00_3213);
																												{	/* R5rs/syntax.scm 575 */
																													obj_t
																														BgL_arg1964z00_3216;
																													BgL_arg1964z00_3216 =
																														CDR(((obj_t)
																															BgL_l1180z00_3205));
																													{
																														obj_t
																															BgL_tail1183z00_5394;
																														obj_t
																															BgL_l1180z00_5393;
																														BgL_l1180z00_5393 =
																															BgL_arg1964z00_3216;
																														BgL_tail1183z00_5394
																															=
																															BgL_newtail1184z00_3213;
																														BgL_tail1183z00_3206
																															=
																															BgL_tail1183z00_5394;
																														BgL_l1180z00_3205 =
																															BgL_l1180z00_5393;
																														goto
																															BgL_zc3z04anonymousza31962ze3z87_3204;
																													}
																												}
																											}
																									}
																								}
																							}
																						BgL_nvarsz00_2048 =
																							BGl_genvarsz00zz__r5_macro_4_3_syntaxz00
																							(BgL_arg1960z00_2094);
																					}
																					{	/* R5rs/syntax.scm 575 */
																						obj_t BgL_nenvz00_2049;

																						{	/* R5rs/syntax.scm 576 */
																							obj_t BgL_arg1952z00_2075;

																							if (NULLP(BgL_bindingsz00_1608))
																								{	/* R5rs/syntax.scm 576 */
																									BgL_arg1952z00_2075 = BNIL;
																								}
																							else
																								{	/* R5rs/syntax.scm 576 */
																									obj_t BgL_head1188z00_2079;

																									BgL_head1188z00_2079 =
																										MAKE_YOUNG_PAIR(BNIL, BNIL);
																									{
																										obj_t BgL_ll1186z00_3252;
																										obj_t BgL_ll1187z00_3253;
																										obj_t BgL_tail1189z00_3254;

																										BgL_ll1186z00_3252 =
																											BgL_bindingsz00_1608;
																										BgL_ll1187z00_3253 =
																											BgL_nvarsz00_2048;
																										BgL_tail1189z00_3254 =
																											BgL_head1188z00_2079;
																									BgL_zc3z04anonymousza31954ze3z87_3251:
																										if (NULLP
																											(BgL_ll1186z00_3252))
																											{	/* R5rs/syntax.scm 576 */
																												BgL_arg1952z00_2075 =
																													CDR
																													(BgL_head1188z00_2079);
																											}
																										else
																											{	/* R5rs/syntax.scm 576 */
																												obj_t
																													BgL_newtail1190z00_3264;
																												{	/* R5rs/syntax.scm 576 */
																													obj_t
																														BgL_arg1958z00_3265;
																													{	/* R5rs/syntax.scm 576 */
																														obj_t BgL_bz00_3266;
																														obj_t BgL_vz00_3267;

																														BgL_bz00_3266 =
																															CAR(
																															((obj_t)
																																BgL_ll1186z00_3252));
																														BgL_vz00_3267 =
																															CAR(((obj_t)
																																BgL_ll1187z00_3253));
																														{	/* R5rs/syntax.scm 577 */
																															obj_t
																																BgL_arg1959z00_3268;
																															BgL_arg1959z00_3268
																																=
																																CAR(((obj_t)
																																	BgL_bz00_3266));
																															BgL_arg1958z00_3265
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1959z00_3268,
																																BgL_vz00_3267);
																														}
																													}
																													BgL_newtail1190z00_3264
																														=
																														MAKE_YOUNG_PAIR
																														(BgL_arg1958z00_3265,
																														BNIL);
																												}
																												SET_CDR
																													(BgL_tail1189z00_3254,
																													BgL_newtail1190z00_3264);
																												{	/* R5rs/syntax.scm 576 */
																													obj_t
																														BgL_arg1956z00_3269;
																													obj_t
																														BgL_arg1957z00_3270;
																													BgL_arg1956z00_3269 =
																														CDR(((obj_t)
																															BgL_ll1186z00_3252));
																													BgL_arg1957z00_3270 =
																														CDR(((obj_t)
																															BgL_ll1187z00_3253));
																													{
																														obj_t
																															BgL_tail1189z00_5417;
																														obj_t
																															BgL_ll1187z00_5416;
																														obj_t
																															BgL_ll1186z00_5415;
																														BgL_ll1186z00_5415 =
																															BgL_arg1956z00_3269;
																														BgL_ll1187z00_5416 =
																															BgL_arg1957z00_3270;
																														BgL_tail1189z00_5417
																															=
																															BgL_newtail1190z00_3264;
																														BgL_tail1189z00_3254
																															=
																															BgL_tail1189z00_5417;
																														BgL_ll1187z00_3253 =
																															BgL_ll1187z00_5416;
																														BgL_ll1186z00_3252 =
																															BgL_ll1186z00_5415;
																														goto
																															BgL_zc3z04anonymousza31954ze3z87_3251;
																													}
																												}
																											}
																									}
																								}
																							BgL_nenvz00_2049 =
																								BGl_appendzd221011zd2zz__r5_macro_4_3_syntaxz00
																								(BgL_arg1952z00_2075,
																								BgL_envz00_44);
																						}
																						{	/* R5rs/syntax.scm 576 */

																							{	/* R5rs/syntax.scm 580 */
																								obj_t BgL_arg1938z00_2050;

																								{	/* R5rs/syntax.scm 580 */
																									obj_t BgL_arg1939z00_2051;
																									obj_t BgL_arg1940z00_2052;

																									if (NULLP
																										(BgL_bindingsz00_1608))
																										{	/* R5rs/syntax.scm 580 */
																											BgL_arg1939z00_2051 =
																												BNIL;
																										}
																									else
																										{	/* R5rs/syntax.scm 580 */
																											obj_t
																												BgL_head1194z00_2056;
																											BgL_head1194z00_2056 =
																												MAKE_YOUNG_PAIR(BNIL,
																												BNIL);
																											{
																												obj_t
																													BgL_ll1192z00_2058;
																												obj_t
																													BgL_ll1193z00_2059;
																												obj_t
																													BgL_tail1195z00_2060;
																												BgL_ll1192z00_2058 =
																													BgL_bindingsz00_1608;
																												BgL_ll1193z00_2059 =
																													BgL_nvarsz00_2048;
																												BgL_tail1195z00_2060 =
																													BgL_head1194z00_2056;
																											BgL_zc3z04anonymousza31942ze3z87_2061:
																												if (NULLP
																													(BgL_ll1192z00_2058))
																													{	/* R5rs/syntax.scm 580 */
																														BgL_arg1939z00_2051
																															=
																															CDR
																															(BgL_head1194z00_2056);
																													}
																												else
																													{	/* R5rs/syntax.scm 580 */
																														obj_t
																															BgL_newtail1196z00_2063;
																														{	/* R5rs/syntax.scm 580 */
																															obj_t
																																BgL_arg1946z00_2066;
																															{	/* R5rs/syntax.scm 580 */
																																obj_t
																																	BgL_bz00_2067;
																																obj_t
																																	BgL_vz00_2068;
																																BgL_bz00_2067 =
																																	CAR(((obj_t)
																																		BgL_ll1192z00_2058));
																																BgL_vz00_2068 =
																																	CAR(((obj_t)
																																		BgL_ll1193z00_2059));
																																{	/* R5rs/syntax.scm 581 */
																																	obj_t
																																		BgL_arg1947z00_2069;
																																	{	/* R5rs/syntax.scm 581 */
																																		obj_t
																																			BgL_arg1950z00_2072;
																																		{	/* R5rs/syntax.scm 581 */
																																			obj_t
																																				BgL_pairz00_3284;
																																			BgL_pairz00_3284
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_bz00_2067));
																																			BgL_arg1950z00_2072
																																				=
																																				CAR
																																				(BgL_pairz00_3284);
																																		}
																																		BgL_arg1947z00_2069
																																			=
																																			BGl_hygieniza7eza7zz__r5_macro_4_3_syntaxz00
																																			(BgL_arg1950z00_2072,
																																			BgL_nenvz00_2049);
																																	}
																																	{	/* R5rs/syntax.scm 581 */
																																		obj_t
																																			BgL_list1948z00_2070;
																																		{	/* R5rs/syntax.scm 581 */
																																			obj_t
																																				BgL_arg1949z00_2071;
																																			BgL_arg1949z00_2071
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1947z00_2069,
																																				BNIL);
																																			BgL_list1948z00_2070
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_vz00_2068,
																																				BgL_arg1949z00_2071);
																																		}
																																		BgL_arg1946z00_2066
																																			=
																																			BgL_list1948z00_2070;
																																	}
																																}
																															}
																															BgL_newtail1196z00_2063
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1946z00_2066,
																																BNIL);
																														}
																														SET_CDR
																															(BgL_tail1195z00_2060,
																															BgL_newtail1196z00_2063);
																														{	/* R5rs/syntax.scm 580 */
																															obj_t
																																BgL_arg1944z00_2064;
																															obj_t
																																BgL_arg1945z00_2065;
																															BgL_arg1944z00_2064
																																=
																																CDR(((obj_t)
																																	BgL_ll1192z00_2058));
																															BgL_arg1945z00_2065
																																=
																																CDR(((obj_t)
																																	BgL_ll1193z00_2059));
																															{
																																obj_t
																																	BgL_tail1195z00_5443;
																																obj_t
																																	BgL_ll1193z00_5442;
																																obj_t
																																	BgL_ll1192z00_5441;
																																BgL_ll1192z00_5441
																																	=
																																	BgL_arg1944z00_2064;
																																BgL_ll1193z00_5442
																																	=
																																	BgL_arg1945z00_2065;
																																BgL_tail1195z00_5443
																																	=
																																	BgL_newtail1196z00_2063;
																																BgL_tail1195z00_2060
																																	=
																																	BgL_tail1195z00_5443;
																																BgL_ll1193z00_2059
																																	=
																																	BgL_ll1193z00_5442;
																																BgL_ll1192z00_2058
																																	=
																																	BgL_ll1192z00_5441;
																																goto
																																	BgL_zc3z04anonymousza31942ze3z87_2061;
																															}
																														}
																													}
																											}
																										}
																									BgL_arg1940z00_2052 =
																										BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																										(BGl_loopze70ze7zz__r5_macro_4_3_syntaxz00
																										(BgL_nenvz00_2049,
																											BgL_bodyz00_1609), BNIL);
																									BgL_arg1938z00_2050 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1939z00_2051,
																										BgL_arg1940z00_2052);
																								}
																								return
																									MAKE_YOUNG_PAIR
																									(BGl_symbol2452z00zz__r5_macro_4_3_syntaxz00,
																									BgL_arg1938z00_2050);
																							}
																						}
																					}
																				}
																			}
																		else
																			{	/* R5rs/syntax.scm 524 */
																				BGL_TAIL return
																					BGl_loopze70ze7zz__r5_macro_4_3_syntaxz00
																					(BgL_envz00_44, BgL_xz00_43);
																			}
																	}
																else
																	{	/* R5rs/syntax.scm 524 */
																		obj_t BgL_cdrzd2426zd2_1656;

																		BgL_cdrzd2426zd2_1656 =
																			CDR(((obj_t) BgL_xz00_43));
																		if (
																			(CAR(
																					((obj_t) BgL_xz00_43)) ==
																				BGl_symbol2576z00zz__r5_macro_4_3_syntaxz00))
																			{	/* R5rs/syntax.scm 524 */
																				if (PAIRP(BgL_cdrzd2426zd2_1656))
																					{	/* R5rs/syntax.scm 524 */
																						obj_t BgL_carzd2429zd2_1660;

																						BgL_carzd2429zd2_1660 =
																							CAR(BgL_cdrzd2426zd2_1656);
																						if (PAIRP(BgL_carzd2429zd2_1660))
																							{	/* R5rs/syntax.scm 524 */
																								if (NULLP(CDR
																										(BgL_carzd2429zd2_1660)))
																									{	/* R5rs/syntax.scm 524 */
																										obj_t BgL_arg1631z00_1664;
																										obj_t BgL_arg1634z00_1665;

																										BgL_arg1631z00_1664 =
																											CAR
																											(BgL_carzd2429zd2_1660);
																										BgL_arg1634z00_1665 =
																											CDR
																											(BgL_cdrzd2426zd2_1656);
																										{	/* R5rs/syntax.scm 585 */
																											obj_t BgL_nvarz00_3329;

																											BgL_nvarz00_3329 =
																												BGl_genvarsz00zz__r5_macro_4_3_syntaxz00
																												(BgL_arg1631z00_1664);
																											{	/* R5rs/syntax.scm 585 */
																												obj_t BgL_nenvz00_3330;

																												{	/* R5rs/syntax.scm 586 */
																													obj_t
																														BgL_arg1973z00_3331;
																													BgL_arg1973z00_3331 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1631z00_1664,
																														BgL_nvarz00_3329);
																													BgL_nenvz00_3330 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1973z00_3331,
																														BgL_envz00_44);
																												}
																												{	/* R5rs/syntax.scm 586 */

																													{	/* R5rs/syntax.scm 587 */
																														obj_t
																															BgL_arg1969z00_3332;
																														{	/* R5rs/syntax.scm 587 */
																															obj_t
																																BgL_arg1970z00_3333;
																															obj_t
																																BgL_arg1971z00_3334;
																															BgL_arg1970z00_3333
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_nvarz00_3329,
																																BNIL);
																															BgL_arg1971z00_3334
																																=
																																BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																(BGl_loopze70ze7zz__r5_macro_4_3_syntaxz00
																																(BgL_nenvz00_3330,
																																	BgL_arg1634z00_1665),
																																BNIL);
																															BgL_arg1969z00_3332
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1970z00_3333,
																																BgL_arg1971z00_3334);
																														}
																														return
																															MAKE_YOUNG_PAIR
																															(BGl_symbol2576z00zz__r5_macro_4_3_syntaxz00,
																															BgL_arg1969z00_3332);
																													}
																												}
																											}
																										}
																									}
																								else
																									{	/* R5rs/syntax.scm 524 */
																										BGL_TAIL return
																											BGl_loopze70ze7zz__r5_macro_4_3_syntaxz00
																											(BgL_envz00_44,
																											BgL_xz00_43);
																									}
																							}
																						else
																							{	/* R5rs/syntax.scm 524 */
																								BGL_TAIL return
																									BGl_loopze70ze7zz__r5_macro_4_3_syntaxz00
																									(BgL_envz00_44, BgL_xz00_43);
																							}
																					}
																				else
																					{	/* R5rs/syntax.scm 524 */
																						BGL_TAIL return
																							BGl_loopze70ze7zz__r5_macro_4_3_syntaxz00
																							(BgL_envz00_44, BgL_xz00_43);
																					}
																			}
																		else
																			{	/* R5rs/syntax.scm 524 */
																				BGL_TAIL return
																					BGl_loopze70ze7zz__r5_macro_4_3_syntaxz00
																					(BgL_envz00_44, BgL_xz00_43);
																			}
																	}
															}
													}
											}
									}
							}
						else
							{	/* R5rs/syntax.scm 524 */
								return BgL_xz00_43;
							}
					}
			}
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zz__r5_macro_4_3_syntaxz00(obj_t BgL_envz00_3738,
		obj_t BgL_xz00_2119)
	{
		{	/* R5rs/syntax.scm 597 */
			if (PAIRP(BgL_xz00_2119))
				{	/* R5rs/syntax.scm 599 */
					return
						MAKE_YOUNG_PAIR(BGl_hygieniza7eza7zz__r5_macro_4_3_syntaxz00(CAR
							(BgL_xz00_2119), BgL_envz00_3738),
						BGl_loopze70ze7zz__r5_macro_4_3_syntaxz00(BgL_envz00_3738,
							CDR(BgL_xz00_2119)));
				}
			else
				{	/* R5rs/syntax.scm 599 */
					if (NULLP(BgL_xz00_2119))
						{	/* R5rs/syntax.scm 601 */
							return BNIL;
						}
					else
						{	/* R5rs/syntax.scm 601 */
							BGL_TAIL return
								BGl_hygieniza7eza7zz__r5_macro_4_3_syntaxz00(BgL_xz00_2119,
								BgL_envz00_3738);
						}
				}
		}

	}



/* flatten */
	obj_t BGl_flattenz00zz__r5_macro_4_3_syntaxz00(obj_t BgL_lz00_47)
	{
		{	/* R5rs/syntax.scm 609 */
			if (PAIRP(BgL_lz00_47))
				{	/* R5rs/syntax.scm 611 */
					return
						MAKE_YOUNG_PAIR(CAR(BgL_lz00_47),
						BGl_flattenz00zz__r5_macro_4_3_syntaxz00(CDR(BgL_lz00_47)));
				}
			else
				{	/* R5rs/syntax.scm 611 */
					if (NULLP(BgL_lz00_47))
						{	/* R5rs/syntax.scm 612 */
							return BgL_lz00_47;
						}
					else
						{	/* R5rs/syntax.scm 613 */
							obj_t BgL_list1986z00_2133;

							BgL_list1986z00_2133 = MAKE_YOUNG_PAIR(BgL_lz00_47, BNIL);
							return BgL_list1986z00_2133;
						}
				}
		}

	}



/* genvars */
	obj_t BGl_genvarsz00zz__r5_macro_4_3_syntaxz00(obj_t BgL_lz00_48)
	{
		{	/* R5rs/syntax.scm 618 */
			if (PAIRP(BgL_lz00_48))
				{	/* R5rs/syntax.scm 628 */
					return
						MAKE_YOUNG_PAIR(BGl_gennameze70ze7zz__r5_macro_4_3_syntaxz00(CAR
							(BgL_lz00_48)),
						BGl_genvarsz00zz__r5_macro_4_3_syntaxz00(CDR(BgL_lz00_48)));
				}
			else
				{	/* R5rs/syntax.scm 628 */
					if (NULLP(BgL_lz00_48))
						{	/* R5rs/syntax.scm 629 */
							return BgL_lz00_48;
						}
					else
						{	/* R5rs/syntax.scm 629 */
							BGL_TAIL return
								BGl_gennameze70ze7zz__r5_macro_4_3_syntaxz00(BgL_lz00_48);
						}
				}
		}

	}



/* genname~0 */
	obj_t BGl_gennameze70ze7zz__r5_macro_4_3_syntaxz00(obj_t BgL_lz00_2141)
	{
		{	/* R5rs/syntax.scm 626 */
			{

				if (SYMBOLP(BgL_lz00_2141))
					{	/* R5rs/syntax.scm 626 */
						{	/* R5rs/syntax.scm 622 */
							bool_t BgL_test2838z00_5510;

							{	/* R5rs/syntax.scm 471 */
								obj_t BgL_sz00_3487;

								{	/* R5rs/syntax.scm 471 */
									obj_t BgL_arg2119z00_3489;

									BgL_arg2119z00_3489 =
										SYMBOL_TO_STRING(((obj_t) BgL_lz00_2141));
									BgL_sz00_3487 =
										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
										(BgL_arg2119z00_3489);
								}
								BgL_test2838z00_5510 =
									bigloo_strcmp_at(BgL_sz00_3487,
									BGl_hygienezd2prefixzd2zz__r5_macro_4_3_syntaxz00, 0L);
							}
							if (BgL_test2838z00_5510)
								{	/* R5rs/syntax.scm 622 */
									BGL_TAIL return
										BGl_hygienezd2valuezd2zz__r5_macro_4_3_syntaxz00
										(BgL_lz00_2141);
								}
							else
								{	/* R5rs/syntax.scm 622 */
									BGL_TAIL return
										BGl_gensymz00zz__r4_symbols_6_4z00(BgL_lz00_2141);
								}
						}
					}
				else
					{	/* R5rs/syntax.scm 626 */

						{	/* R5rs/syntax.scm 626 */

							BGL_TAIL return BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
						}
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__r5_macro_4_3_syntaxz00(void)
	{
		{	/* R5rs/syntax.scm 17 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__r5_macro_4_3_syntaxz00(void)
	{
		{	/* R5rs/syntax.scm 17 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__r5_macro_4_3_syntaxz00(void)
	{
		{	/* R5rs/syntax.scm 17 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__r5_macro_4_3_syntaxz00(void)
	{
		{	/* R5rs/syntax.scm 17 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string2578z00zz__r5_macro_4_3_syntaxz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string2578z00zz__r5_macro_4_3_syntaxz00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string2578z00zz__r5_macro_4_3_syntaxz00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string2578z00zz__r5_macro_4_3_syntaxz00));
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L,
				BSTRING_TO_STRING(BGl_string2578z00zz__r5_macro_4_3_syntaxz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(481261960L,
				BSTRING_TO_STRING(BGl_string2578z00zz__r5_macro_4_3_syntaxz00));
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L,
				BSTRING_TO_STRING(BGl_string2578z00zz__r5_macro_4_3_syntaxz00));
			return
				BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00
				(268155843L,
				BSTRING_TO_STRING(BGl_string2578z00zz__r5_macro_4_3_syntaxz00));
		}

	}

#ifdef __cplusplus
}
#endif
