/*===========================================================================*/
/*   (Llib/custom.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Llib/custom.scm -indent -o objs/obj_u/Llib/custom.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___CUSTOM_TYPE_DEFINITIONS
#define BGL___CUSTOM_TYPE_DEFINITIONS
#endif													// BGL___CUSTOM_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL bool_t BGl_customzf3zf3zz__customz00(obj_t);
	static obj_t BGl_z62customzf3z91zz__customz00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__customz00 = BUNSPEC;
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__customz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_EXPORTED_DECL obj_t
		BGl_customzd2identifierzd2setz12z12zz__customz00(obj_t, char *);
	BGL_EXPORTED_DECL obj_t BGl_customzd2nilzd2zz__customz00(void);
	static obj_t BGl_z62customzd2hashzb0zz__customz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zz__customz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zz__customz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__customz00(void);
	static obj_t BGl_objectzd2initzd2zz__customz00(void);
	extern obj_t bgl_custom_nil(void);
	static obj_t BGl_z62customzd2identifierzb0zz__customz00(obj_t, obj_t);
	static obj_t BGl_z62customzd2nilzb0zz__customz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_customzd2equalzf3z21zz__customz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zz__customz00(void);
	BGL_EXPORTED_DECL char *BGl_customzd2identifierzd2zz__customz00(obj_t);
	static obj_t BGl_z62customzd2identifierzd2setz12z70zz__customz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_customzd2hashzd2zz__customz00(obj_t, int);
	static obj_t BGl_z62customzd2equalzf3z43zz__customz00(obj_t, obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_customzd2nilzd2envz00zz__customz00,
		BgL_bgl_za762customza7d2nilza71584za7, BGl_z62customzd2nilzb0zz__customz00,
		0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_customzd2equalzf3zd2envzf3zz__customz00,
		BgL_bgl_za762customza7d2equa1585z00,
		BGl_z62customzd2equalzf3z43zz__customz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_customzd2identifierzd2setz12zd2envzc0zz__customz00,
		BgL_bgl_za762customza7d2iden1586z00,
		BGl_z62customzd2identifierzd2setz12z70zz__customz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_customzd2identifierzd2envz00zz__customz00,
		BgL_bgl_za762customza7d2iden1587z00,
		BGl_z62customzd2identifierzb0zz__customz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_customzf3zd2envz21zz__customz00,
		BgL_bgl_za762customza7f3za791za71588z00, BGl_z62customzf3z91zz__customz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1575z00zz__customz00,
		BgL_bgl_string1575za700za7za7_1589za7,
		"/tmp/bigloo/runtime/Llib/custom.scm", 35);
	      DEFINE_STRING(BGl_string1576z00zz__customz00,
		BgL_bgl_string1576za700za7za7_1590za7, "&custom-equal?", 14);
	      DEFINE_STRING(BGl_string1577z00zz__customz00,
		BgL_bgl_string1577za700za7za7_1591za7, "custom", 6);
	      DEFINE_STRING(BGl_string1578z00zz__customz00,
		BgL_bgl_string1578za700za7za7_1592za7, "&custom-identifier", 18);
	      DEFINE_STRING(BGl_string1579z00zz__customz00,
		BgL_bgl_string1579za700za7za7_1593za7, "&custom-identifier-set!", 23);
	      DEFINE_STRING(BGl_string1580z00zz__customz00,
		BgL_bgl_string1580za700za7za7_1594za7, "bstring", 7);
	      DEFINE_STRING(BGl_string1581z00zz__customz00,
		BgL_bgl_string1581za700za7za7_1595za7, "&custom-hash", 12);
	      DEFINE_STRING(BGl_string1582z00zz__customz00,
		BgL_bgl_string1582za700za7za7_1596za7, "bint", 4);
	      DEFINE_STRING(BGl_string1583z00zz__customz00,
		BgL_bgl_string1583za700za7za7_1597za7, "__custom", 8);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_customzd2hashzd2envz00zz__customz00,
		BgL_bgl_za762customza7d2hash1598z00, BGl_z62customzd2hashzb0zz__customz00,
		0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__customz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__customz00(long
		BgL_checksumz00_1735, char *BgL_fromz00_1736)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__customz00))
				{
					BGl_requirezd2initializa7ationz75zz__customz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__customz00();
					BGl_importedzd2moduleszd2initz00zz__customz00();
					return BGl_methodzd2initzd2zz__customz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__customz00(void)
	{
		{	/* Llib/custom.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* custom? */
	BGL_EXPORTED_DEF bool_t BGl_customzf3zf3zz__customz00(obj_t BgL_objz00_3)
	{
		{	/* Llib/custom.scm 84 */
			return CUSTOMP(BgL_objz00_3);
		}

	}



/* &custom? */
	obj_t BGl_z62customzf3z91zz__customz00(obj_t BgL_envz00_1708,
		obj_t BgL_objz00_1709)
	{
		{	/* Llib/custom.scm 84 */
			return BBOOL(BGl_customzf3zf3zz__customz00(BgL_objz00_1709));
		}

	}



/* custom-nil */
	BGL_EXPORTED_DEF obj_t BGl_customzd2nilzd2zz__customz00(void)
	{
		{	/* Llib/custom.scm 90 */
			BGL_TAIL return bgl_custom_nil();
		}

	}



/* &custom-nil */
	obj_t BGl_z62customzd2nilzb0zz__customz00(obj_t BgL_envz00_1710)
	{
		{	/* Llib/custom.scm 90 */
			return BGl_customzd2nilzd2zz__customz00();
		}

	}



/* custom-equal? */
	BGL_EXPORTED_DEF bool_t BGl_customzd2equalzf3z21zz__customz00(obj_t
		BgL_obj1z00_4, obj_t BgL_obj2z00_5)
	{
		{	/* Llib/custom.scm 96 */
			return CUSTOM_CMP(BgL_obj1z00_4, BgL_obj2z00_5);
		}

	}



/* &custom-equal? */
	obj_t BGl_z62customzd2equalzf3z43zz__customz00(obj_t BgL_envz00_1711,
		obj_t BgL_obj1z00_1712, obj_t BgL_obj2z00_1713)
	{
		{	/* Llib/custom.scm 96 */
			{	/* Llib/custom.scm 97 */
				bool_t BgL_tmpz00_1750;

				{	/* Llib/custom.scm 97 */
					obj_t BgL_auxz00_1758;
					obj_t BgL_auxz00_1751;

					if (CUSTOMP(BgL_obj2z00_1713))
						{	/* Llib/custom.scm 97 */
							BgL_auxz00_1758 = BgL_obj2z00_1713;
						}
					else
						{
							obj_t BgL_auxz00_1761;

							BgL_auxz00_1761 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1575z00zz__customz00,
								BINT(3593L), BGl_string1576z00zz__customz00,
								BGl_string1577z00zz__customz00, BgL_obj2z00_1713);
							FAILURE(BgL_auxz00_1761, BFALSE, BFALSE);
						}
					if (CUSTOMP(BgL_obj1z00_1712))
						{	/* Llib/custom.scm 97 */
							BgL_auxz00_1751 = BgL_obj1z00_1712;
						}
					else
						{
							obj_t BgL_auxz00_1754;

							BgL_auxz00_1754 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1575z00zz__customz00,
								BINT(3593L), BGl_string1576z00zz__customz00,
								BGl_string1577z00zz__customz00, BgL_obj1z00_1712);
							FAILURE(BgL_auxz00_1754, BFALSE, BFALSE);
						}
					BgL_tmpz00_1750 =
						BGl_customzd2equalzf3z21zz__customz00(BgL_auxz00_1751,
						BgL_auxz00_1758);
				}
				return BBOOL(BgL_tmpz00_1750);
			}
		}

	}



/* custom-identifier */
	BGL_EXPORTED_DEF char *BGl_customzd2identifierzd2zz__customz00(obj_t
		BgL_customz00_6)
	{
		{	/* Llib/custom.scm 102 */
			return CUSTOM_IDENTIFIER(BgL_customz00_6);
		}

	}



/* &custom-identifier */
	obj_t BGl_z62customzd2identifierzb0zz__customz00(obj_t BgL_envz00_1714,
		obj_t BgL_customz00_1715)
	{
		{	/* Llib/custom.scm 102 */
			{	/* Llib/custom.scm 103 */
				char *BgL_tmpz00_1768;

				{	/* Llib/custom.scm 103 */
					obj_t BgL_auxz00_1769;

					if (CUSTOMP(BgL_customz00_1715))
						{	/* Llib/custom.scm 103 */
							BgL_auxz00_1769 = BgL_customz00_1715;
						}
					else
						{
							obj_t BgL_auxz00_1772;

							BgL_auxz00_1772 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1575z00zz__customz00,
								BINT(3906L), BGl_string1578z00zz__customz00,
								BGl_string1577z00zz__customz00, BgL_customz00_1715);
							FAILURE(BgL_auxz00_1772, BFALSE, BFALSE);
						}
					BgL_tmpz00_1768 =
						BGl_customzd2identifierzd2zz__customz00(BgL_auxz00_1769);
				}
				return string_to_bstring(BgL_tmpz00_1768);
			}
		}

	}



/* custom-identifier-set! */
	BGL_EXPORTED_DEF obj_t BGl_customzd2identifierzd2setz12z12zz__customz00(obj_t
		BgL_customz00_7, char *BgL_strz00_8)
	{
		{	/* Llib/custom.scm 108 */
			return CUSTOM_IDENTIFIER_SET(BgL_customz00_7, BgL_strz00_8);
		}

	}



/* &custom-identifier-set! */
	obj_t BGl_z62customzd2identifierzd2setz12z70zz__customz00(obj_t
		BgL_envz00_1716, obj_t BgL_customz00_1717, obj_t BgL_strz00_1718)
	{
		{	/* Llib/custom.scm 108 */
			{	/* Llib/custom.scm 109 */
				char *BgL_auxz00_1786;
				obj_t BgL_auxz00_1779;

				{	/* Llib/custom.scm 109 */
					obj_t BgL_tmpz00_1787;

					if (STRINGP(BgL_strz00_1718))
						{	/* Llib/custom.scm 109 */
							BgL_tmpz00_1787 = BgL_strz00_1718;
						}
					else
						{
							obj_t BgL_auxz00_1790;

							BgL_auxz00_1790 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1575z00zz__customz00,
								BINT(4229L), BGl_string1579z00zz__customz00,
								BGl_string1580z00zz__customz00, BgL_strz00_1718);
							FAILURE(BgL_auxz00_1790, BFALSE, BFALSE);
						}
					BgL_auxz00_1786 = BSTRING_TO_STRING(BgL_tmpz00_1787);
				}
				if (CUSTOMP(BgL_customz00_1717))
					{	/* Llib/custom.scm 109 */
						BgL_auxz00_1779 = BgL_customz00_1717;
					}
				else
					{
						obj_t BgL_auxz00_1782;

						BgL_auxz00_1782 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1575z00zz__customz00,
							BINT(4229L), BGl_string1579z00zz__customz00,
							BGl_string1577z00zz__customz00, BgL_customz00_1717);
						FAILURE(BgL_auxz00_1782, BFALSE, BFALSE);
					}
				return
					BGl_customzd2identifierzd2setz12z12zz__customz00(BgL_auxz00_1779,
					BgL_auxz00_1786);
			}
		}

	}



/* custom-hash */
	BGL_EXPORTED_DEF obj_t BGl_customzd2hashzd2zz__customz00(obj_t
		BgL_customz00_9, int BgL_modz00_10)
	{
		{	/* Llib/custom.scm 114 */
			{	/* Llib/custom.scm 115 */
				int BgL_numz00_1505;

				BgL_numz00_1505 = CUSTOM_HASH_NUMBER(BgL_customz00_9);
				{	/* Llib/custom.scm 116 */
					long BgL_n1z00_1506;
					long BgL_n2z00_1507;

					BgL_n1z00_1506 = (long) (BgL_numz00_1505);
					BgL_n2z00_1507 = (long) (BgL_modz00_10);
					{	/* Llib/custom.scm 116 */
						bool_t BgL_test1605z00_1799;

						{	/* Llib/custom.scm 116 */
							long BgL_arg1384z00_1509;

							BgL_arg1384z00_1509 =
								(((BgL_n1z00_1506) | (BgL_n2z00_1507)) & -2147483648);
							BgL_test1605z00_1799 = (BgL_arg1384z00_1509 == 0L);
						}
						if (BgL_test1605z00_1799)
							{	/* Llib/custom.scm 116 */
								int32_t BgL_arg1380z00_1510;

								{	/* Llib/custom.scm 116 */
									int32_t BgL_arg1382z00_1511;
									int32_t BgL_arg1383z00_1512;

									BgL_arg1382z00_1511 = (int32_t) (BgL_n1z00_1506);
									BgL_arg1383z00_1512 = (int32_t) (BgL_n2z00_1507);
									BgL_arg1380z00_1510 =
										(BgL_arg1382z00_1511 % BgL_arg1383z00_1512);
								}
								{	/* Llib/custom.scm 116 */
									long BgL_arg1495z00_1517;

									BgL_arg1495z00_1517 = (long) (BgL_arg1380z00_1510);
									return BINT((long) (BgL_arg1495z00_1517));
							}}
						else
							{	/* Llib/custom.scm 116 */
								return BINT((BgL_n1z00_1506 % BgL_n2z00_1507));
							}
					}
				}
			}
		}

	}



/* &custom-hash */
	obj_t BGl_z62customzd2hashzb0zz__customz00(obj_t BgL_envz00_1719,
		obj_t BgL_customz00_1720, obj_t BgL_modz00_1721)
	{
		{	/* Llib/custom.scm 114 */
			{	/* Llib/custom.scm 115 */
				int BgL_auxz00_1817;
				obj_t BgL_auxz00_1810;

				{	/* Llib/custom.scm 115 */
					obj_t BgL_tmpz00_1818;

					if (INTEGERP(BgL_modz00_1721))
						{	/* Llib/custom.scm 115 */
							BgL_tmpz00_1818 = BgL_modz00_1721;
						}
					else
						{
							obj_t BgL_auxz00_1821;

							BgL_auxz00_1821 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1575z00zz__customz00,
								BINT(4540L), BGl_string1581z00zz__customz00,
								BGl_string1582z00zz__customz00, BgL_modz00_1721);
							FAILURE(BgL_auxz00_1821, BFALSE, BFALSE);
						}
					BgL_auxz00_1817 = CINT(BgL_tmpz00_1818);
				}
				if (CUSTOMP(BgL_customz00_1720))
					{	/* Llib/custom.scm 115 */
						BgL_auxz00_1810 = BgL_customz00_1720;
					}
				else
					{
						obj_t BgL_auxz00_1813;

						BgL_auxz00_1813 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1575z00zz__customz00,
							BINT(4540L), BGl_string1581z00zz__customz00,
							BGl_string1577z00zz__customz00, BgL_customz00_1720);
						FAILURE(BgL_auxz00_1813, BFALSE, BFALSE);
					}
				return
					BGl_customzd2hashzd2zz__customz00(BgL_auxz00_1810, BgL_auxz00_1817);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__customz00(void)
	{
		{	/* Llib/custom.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__customz00(void)
	{
		{	/* Llib/custom.scm 14 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__customz00(void)
	{
		{	/* Llib/custom.scm 14 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__customz00(void)
	{
		{	/* Llib/custom.scm 14 */
			return
				BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1583z00zz__customz00));
		}

	}

#ifdef __cplusplus
}
#endif
