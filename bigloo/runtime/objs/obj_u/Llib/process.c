/*===========================================================================*/
/*   (Llib/process.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Llib/process.scm -indent -o objs/obj_u/Llib/process.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___PROCESS_TYPE_DEFINITIONS
#define BGL___PROCESS_TYPE_DEFINITIONS
#endif													// BGL___PROCESS_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t c_process_xstatus(obj_t);
	extern obj_t c_process_list(void);
	extern obj_t bstring_to_keyword(obj_t);
	static obj_t BGl_list1657z00zz__processz00 = BUNSPEC;
	static obj_t BGl_requirezd2initializa7ationz75zz__processz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_processzd2errorzd2portz00zz__processz00(obj_t);
	static obj_t BGl_z62processzd2pidzb0zz__processz00(obj_t, obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__processz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_z62processzd2killzb0zz__processz00(obj_t, obj_t);
	static obj_t BGl_z62runzd2processzb0zz__processz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62processzd2sendzd2signalz62zz__processz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62processzd2waitzb0zz__processz00(obj_t, obj_t);
	static obj_t BGl_z62processzd2errorzd2portz62zz__processz00(obj_t, obj_t);
	static obj_t BGl_z62processzd2stopzb0zz__processz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_processzd2killzd2zz__processz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_processzf3zf3zz__processz00(obj_t);
	static obj_t BGl_z62processzd2outputzd2portz62zz__processz00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__processz00(void);
	static obj_t BGl_genericzd2initzd2zz__processz00(void);
	BGL_EXPORTED_DECL bool_t BGl_processzd2waitzd2zz__processz00(obj_t);
	extern obj_t c_process_kill(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__processz00(void);
	static obj_t BGl_z62closezd2processzd2portsz62zz__processz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_processzd2inputzd2portz00zz__processz00(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zz__processz00(void);
	BGL_EXPORTED_DECL obj_t BGl_processzd2nilzd2zz__processz00(void);
	BGL_EXPORTED_DECL obj_t BGl_processzd2stopzd2zz__processz00(obj_t);
	static obj_t BGl_objectzd2initzd2zz__processz00(void);
	static obj_t BGl_z62processzd2exitzd2statusz62zz__processz00(obj_t, obj_t);
	extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t c_process_wait(obj_t);
	extern obj_t bgl_reverse_bang(obj_t);
	extern obj_t bgl_process_nil(void);
	static obj_t BGl_z62processzd2alivezf3z43zz__processz00(obj_t, obj_t);
	extern obj_t c_process_stop(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_processzd2continuezd2zz__processz00(obj_t);
	static obj_t BGl_z62processzd2inputzd2portz62zz__processz00(obj_t, obj_t);
	static obj_t BGl_keyword1658z00zz__processz00 = BUNSPEC;
	extern obj_t c_process_continue(obj_t);
	BGL_EXPORTED_DECL int BGl_processzd2pidzd2zz__processz00(obj_t);
	static obj_t BGl_keyword1660z00zz__processz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_unregisterzd2processzd2zz__processz00(obj_t);
	static obj_t BGl_keyword1664z00zz__processz00 = BUNSPEC;
	static obj_t BGl_keyword1666z00zz__processz00 = BUNSPEC;
	static obj_t BGl_keyword1668z00zz__processz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_runzd2processzd2zz__processz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zz__processz00(void);
	static obj_t BGl_z62processzf3z91zz__processz00(obj_t, obj_t);
	static obj_t BGl_keyword1670z00zz__processz00 = BUNSPEC;
	static obj_t BGl_keyword1672z00zz__processz00 = BUNSPEC;
	static obj_t BGl_keyword1674z00zz__processz00 = BUNSPEC;
	static obj_t BGl_keyword1676z00zz__processz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_processzd2sendzd2signalz00zz__processz00(obj_t,
		int);
	extern obj_t c_unregister_process(obj_t);
	extern obj_t c_run_process(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62unregisterzd2processzb0zz__processz00(obj_t, obj_t);
	extern obj_t bgl_close_input_port(obj_t);
	static obj_t BGl_z62processzd2listzb0zz__processz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_processzd2outputzd2portz00zz__processz00(obj_t);
	extern obj_t c_process_send_signal(obj_t, int);
	BGL_EXPORTED_DECL bool_t BGl_processzd2alivezf3z21zz__processz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_closezd2processzd2portsz00zz__processz00(obj_t);
	static obj_t BGl_z62processzd2nilzb0zz__processz00(obj_t);
	static obj_t BGl_z62processzd2continuezb0zz__processz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_processzd2exitzd2statusz00zz__processz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_processzd2listzd2zz__processz00(void);
	extern obj_t bgl_close_output_port(obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern bool_t c_process_alivep(obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_processzd2errorzd2portzd2envzd2zz__processz00,
		BgL_bgl_za762processza7d2err1683z00,
		BGl_z62processzd2errorzd2portz62zz__processz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_processzd2nilzd2envz00zz__processz00,
		BgL_bgl_za762processza7d2nil1684z00, BGl_z62processzd2nilzb0zz__processz00,
		0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_processzd2killzd2envz00zz__processz00,
		BgL_bgl_za762processza7d2kil1685z00, BGl_z62processzd2killzb0zz__processz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_processzd2outputzd2portzd2envzd2zz__processz00,
		BgL_bgl_za762processza7d2out1686z00,
		BGl_z62processzd2outputzd2portz62zz__processz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_processzd2waitzd2envz00zz__processz00,
		BgL_bgl_za762processza7d2wai1687z00, BGl_z62processzd2waitzb0zz__processz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_closezd2processzd2portszd2envzd2zz__processz00,
		BgL_bgl_za762closeza7d2proce1688z00,
		BGl_z62closezd2processzd2portsz62zz__processz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1643z00zz__processz00,
		BgL_bgl_string1643za700za7za7_1689za7,
		"/tmp/bigloo/runtime/Llib/process.scm", 36);
	      DEFINE_STRING(BGl_string1644z00zz__processz00,
		BgL_bgl_string1644za700za7za7_1690za7, "&process-pid", 12);
	      DEFINE_STRING(BGl_string1645z00zz__processz00,
		BgL_bgl_string1645za700za7za7_1691za7, "process", 7);
	      DEFINE_STRING(BGl_string1646z00zz__processz00,
		BgL_bgl_string1646za700za7za7_1692za7, "&process-output-port", 20);
	      DEFINE_STRING(BGl_string1647z00zz__processz00,
		BgL_bgl_string1647za700za7za7_1693za7, "&process-input-port", 19);
	      DEFINE_STRING(BGl_string1648z00zz__processz00,
		BgL_bgl_string1648za700za7za7_1694za7, "&process-error-port", 19);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_unregisterzd2processzd2envz00zz__processz00,
		BgL_bgl_za762unregisterza7d21695z00,
		BGl_z62unregisterzd2processzb0zz__processz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1649z00zz__processz00,
		BgL_bgl_string1649za700za7za7_1696za7, "&process-alive?", 15);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_processzd2exitzd2statuszd2envzd2zz__processz00,
		BgL_bgl_za762processza7d2exi1697z00,
		BGl_z62processzd2exitzd2statusz62zz__processz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1650z00zz__processz00,
		BgL_bgl_string1650za700za7za7_1698za7, "&process-wait", 13);
	      DEFINE_STRING(BGl_string1651z00zz__processz00,
		BgL_bgl_string1651za700za7za7_1699za7, "&process-exit-status", 20);
	      DEFINE_STRING(BGl_string1652z00zz__processz00,
		BgL_bgl_string1652za700za7za7_1700za7, "&process-send-signal", 20);
	      DEFINE_STRING(BGl_string1653z00zz__processz00,
		BgL_bgl_string1653za700za7za7_1701za7, "bint", 4);
	      DEFINE_STRING(BGl_string1654z00zz__processz00,
		BgL_bgl_string1654za700za7za7_1702za7, "&process-kill", 13);
	      DEFINE_STRING(BGl_string1655z00zz__processz00,
		BgL_bgl_string1655za700za7za7_1703za7, "&process-stop", 13);
	      DEFINE_STRING(BGl_string1656z00zz__processz00,
		BgL_bgl_string1656za700za7za7_1704za7, "&process-continue", 17);
	      DEFINE_STRING(BGl_string1659z00zz__processz00,
		BgL_bgl_string1659za700za7za7_1705za7, "pipe", 4);
	      DEFINE_STRING(BGl_string1661z00zz__processz00,
		BgL_bgl_string1661za700za7za7_1706za7, "wait", 4);
	      DEFINE_STRING(BGl_string1662z00zz__processz00,
		BgL_bgl_string1662za700za7za7_1707za7, "run-process", 11);
	      DEFINE_STRING(BGl_string1663z00zz__processz00,
		BgL_bgl_string1663za700za7za7_1708za7, "Illegal argument", 16);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_processzd2continuezd2envz00zz__processz00,
		BgL_bgl_za762processza7d2con1709z00,
		BGl_z62processzd2continuezb0zz__processz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1665z00zz__processz00,
		BgL_bgl_string1665za700za7za7_1710za7, "fork", 4);
	      DEFINE_STRING(BGl_string1667z00zz__processz00,
		BgL_bgl_string1667za700za7za7_1711za7, "input", 5);
	      DEFINE_STRING(BGl_string1669z00zz__processz00,
		BgL_bgl_string1669za700za7za7_1712za7, "output", 6);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_processzf3zd2envz21zz__processz00,
		BgL_bgl_za762processza7f3za7911713za7, BGl_z62processzf3z91zz__processz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1671z00zz__processz00,
		BgL_bgl_string1671za700za7za7_1714za7, "null", 4);
	      DEFINE_STRING(BGl_string1673z00zz__processz00,
		BgL_bgl_string1673za700za7za7_1715za7, "error", 5);
	      DEFINE_STRING(BGl_string1675z00zz__processz00,
		BgL_bgl_string1675za700za7za7_1716za7, "host", 4);
	      DEFINE_STRING(BGl_string1677z00zz__processz00,
		BgL_bgl_string1677za700za7za7_1717za7, "env", 3);
	      DEFINE_STRING(BGl_string1678z00zz__processz00,
		BgL_bgl_string1678za700za7za7_1718za7, "&run-process", 12);
	      DEFINE_STRING(BGl_string1679z00zz__processz00,
		BgL_bgl_string1679za700za7za7_1719za7, "bstring", 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_processzd2alivezf3zd2envzf3zz__processz00,
		BgL_bgl_za762processza7d2ali1720z00,
		BGl_z62processzd2alivezf3z43zz__processz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_processzd2pidzd2envz00zz__processz00,
		BgL_bgl_za762processza7d2pid1721z00, BGl_z62processzd2pidzb0zz__processz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_processzd2inputzd2portzd2envzd2zz__processz00,
		BgL_bgl_za762processza7d2inp1722z00,
		BGl_z62processzd2inputzd2portz62zz__processz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1680z00zz__processz00,
		BgL_bgl_string1680za700za7za7_1723za7, "&close-process-ports", 20);
	      DEFINE_STRING(BGl_string1681z00zz__processz00,
		BgL_bgl_string1681za700za7za7_1724za7, "&unregister-process", 19);
	      DEFINE_STRING(BGl_string1682z00zz__processz00,
		BgL_bgl_string1682za700za7za7_1725za7, "__process", 9);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_processzd2stopzd2envz00zz__processz00,
		BgL_bgl_za762processza7d2sto1726z00, BGl_z62processzd2stopzb0zz__processz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_runzd2processzd2envz00zz__processz00,
		BgL_bgl_za762runza7d2process1727z00, va_generic_entry,
		BGl_z62runzd2processzb0zz__processz00, BUNSPEC, -2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_processzd2listzd2envz00zz__processz00,
		BgL_bgl_za762processza7d2lis1728z00, BGl_z62processzd2listzb0zz__processz00,
		0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_processzd2sendzd2signalzd2envzd2zz__processz00,
		BgL_bgl_za762processza7d2sen1729z00,
		BGl_z62processzd2sendzd2signalz62zz__processz00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_list1657z00zz__processz00));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__processz00));
		     ADD_ROOT((void *) (&BGl_keyword1658z00zz__processz00));
		     ADD_ROOT((void *) (&BGl_keyword1660z00zz__processz00));
		     ADD_ROOT((void *) (&BGl_keyword1664z00zz__processz00));
		     ADD_ROOT((void *) (&BGl_keyword1666z00zz__processz00));
		     ADD_ROOT((void *) (&BGl_keyword1668z00zz__processz00));
		     ADD_ROOT((void *) (&BGl_keyword1670z00zz__processz00));
		     ADD_ROOT((void *) (&BGl_keyword1672z00zz__processz00));
		     ADD_ROOT((void *) (&BGl_keyword1674z00zz__processz00));
		     ADD_ROOT((void *) (&BGl_keyword1676z00zz__processz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__processz00(long
		BgL_checksumz00_1867, char *BgL_fromz00_1868)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__processz00))
				{
					BGl_requirezd2initializa7ationz75zz__processz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__processz00();
					BGl_cnstzd2initzd2zz__processz00();
					BGl_importedzd2moduleszd2initz00zz__processz00();
					return BGl_methodzd2initzd2zz__processz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__processz00(void)
	{
		{	/* Llib/process.scm 15 */
			BGl_keyword1658z00zz__processz00 =
				bstring_to_keyword(BGl_string1659z00zz__processz00);
			BGl_list1657z00zz__processz00 =
				MAKE_YOUNG_PAIR(BGl_keyword1658z00zz__processz00, BNIL);
			BGl_keyword1660z00zz__processz00 =
				bstring_to_keyword(BGl_string1661z00zz__processz00);
			BGl_keyword1664z00zz__processz00 =
				bstring_to_keyword(BGl_string1665z00zz__processz00);
			BGl_keyword1666z00zz__processz00 =
				bstring_to_keyword(BGl_string1667z00zz__processz00);
			BGl_keyword1668z00zz__processz00 =
				bstring_to_keyword(BGl_string1669z00zz__processz00);
			BGl_keyword1670z00zz__processz00 =
				bstring_to_keyword(BGl_string1671z00zz__processz00);
			BGl_keyword1672z00zz__processz00 =
				bstring_to_keyword(BGl_string1673z00zz__processz00);
			BGl_keyword1674z00zz__processz00 =
				bstring_to_keyword(BGl_string1675z00zz__processz00);
			return (BGl_keyword1676z00zz__processz00 =
				bstring_to_keyword(BGl_string1677z00zz__processz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__processz00(void)
	{
		{	/* Llib/process.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* process? */
	BGL_EXPORTED_DEF bool_t BGl_processzf3zf3zz__processz00(obj_t BgL_objz00_3)
	{
		{	/* Llib/process.scm 123 */
			return PROCESSP(BgL_objz00_3);
		}

	}



/* &process? */
	obj_t BGl_z62processzf3z91zz__processz00(obj_t BgL_envz00_1795,
		obj_t BgL_objz00_1796)
	{
		{	/* Llib/process.scm 123 */
			return BBOOL(BGl_processzf3zf3zz__processz00(BgL_objz00_1796));
		}

	}



/* process-nil */
	BGL_EXPORTED_DEF obj_t BGl_processzd2nilzd2zz__processz00(void)
	{
		{	/* Llib/process.scm 129 */
			BGL_TAIL return bgl_process_nil();
		}

	}



/* &process-nil */
	obj_t BGl_z62processzd2nilzb0zz__processz00(obj_t BgL_envz00_1797)
	{
		{	/* Llib/process.scm 129 */
			return BGl_processzd2nilzd2zz__processz00();
		}

	}



/* process-pid */
	BGL_EXPORTED_DEF int BGl_processzd2pidzd2zz__processz00(obj_t BgL_procz00_4)
	{
		{	/* Llib/process.scm 135 */
			return PROCESS_PID(BgL_procz00_4);
		}

	}



/* &process-pid */
	obj_t BGl_z62processzd2pidzb0zz__processz00(obj_t BgL_envz00_1798,
		obj_t BgL_procz00_1799)
	{
		{	/* Llib/process.scm 135 */
			{	/* Llib/process.scm 136 */
				int BgL_tmpz00_1893;

				{	/* Llib/process.scm 136 */
					obj_t BgL_auxz00_1894;

					if (PROCESSP(BgL_procz00_1799))
						{	/* Llib/process.scm 136 */
							BgL_auxz00_1894 = BgL_procz00_1799;
						}
					else
						{
							obj_t BgL_auxz00_1897;

							BgL_auxz00_1897 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1643z00zz__processz00,
								BINT(5714L), BGl_string1644z00zz__processz00,
								BGl_string1645z00zz__processz00, BgL_procz00_1799);
							FAILURE(BgL_auxz00_1897, BFALSE, BFALSE);
						}
					BgL_tmpz00_1893 = BGl_processzd2pidzd2zz__processz00(BgL_auxz00_1894);
				}
				return BINT(BgL_tmpz00_1893);
			}
		}

	}



/* process-output-port */
	BGL_EXPORTED_DEF obj_t BGl_processzd2outputzd2portz00zz__processz00(obj_t
		BgL_procz00_5)
	{
		{	/* Llib/process.scm 141 */
			return PROCESS_OUTPUT_PORT(BgL_procz00_5);
		}

	}



/* &process-output-port */
	obj_t BGl_z62processzd2outputzd2portz62zz__processz00(obj_t BgL_envz00_1800,
		obj_t BgL_procz00_1801)
	{
		{	/* Llib/process.scm 141 */
			{	/* Llib/process.scm 142 */
				obj_t BgL_auxz00_1904;

				if (PROCESSP(BgL_procz00_1801))
					{	/* Llib/process.scm 142 */
						BgL_auxz00_1904 = BgL_procz00_1801;
					}
				else
					{
						obj_t BgL_auxz00_1907;

						BgL_auxz00_1907 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1643z00zz__processz00,
							BINT(6004L), BGl_string1646z00zz__processz00,
							BGl_string1645z00zz__processz00, BgL_procz00_1801);
						FAILURE(BgL_auxz00_1907, BFALSE, BFALSE);
					}
				return BGl_processzd2outputzd2portz00zz__processz00(BgL_auxz00_1904);
			}
		}

	}



/* process-input-port */
	BGL_EXPORTED_DEF obj_t BGl_processzd2inputzd2portz00zz__processz00(obj_t
		BgL_procz00_6)
	{
		{	/* Llib/process.scm 147 */
			return PROCESS_INPUT_PORT(BgL_procz00_6);
		}

	}



/* &process-input-port */
	obj_t BGl_z62processzd2inputzd2portz62zz__processz00(obj_t BgL_envz00_1802,
		obj_t BgL_procz00_1803)
	{
		{	/* Llib/process.scm 147 */
			{	/* Llib/process.scm 148 */
				obj_t BgL_auxz00_1913;

				if (PROCESSP(BgL_procz00_1803))
					{	/* Llib/process.scm 148 */
						BgL_auxz00_1913 = BgL_procz00_1803;
					}
				else
					{
						obj_t BgL_auxz00_1916;

						BgL_auxz00_1916 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1643z00zz__processz00,
							BINT(6301L), BGl_string1647z00zz__processz00,
							BGl_string1645z00zz__processz00, BgL_procz00_1803);
						FAILURE(BgL_auxz00_1916, BFALSE, BFALSE);
					}
				return BGl_processzd2inputzd2portz00zz__processz00(BgL_auxz00_1913);
			}
		}

	}



/* process-error-port */
	BGL_EXPORTED_DEF obj_t BGl_processzd2errorzd2portz00zz__processz00(obj_t
		BgL_procz00_7)
	{
		{	/* Llib/process.scm 153 */
			return PROCESS_ERROR_PORT(BgL_procz00_7);
		}

	}



/* &process-error-port */
	obj_t BGl_z62processzd2errorzd2portz62zz__processz00(obj_t BgL_envz00_1804,
		obj_t BgL_procz00_1805)
	{
		{	/* Llib/process.scm 153 */
			{	/* Llib/process.scm 154 */
				obj_t BgL_auxz00_1922;

				if (PROCESSP(BgL_procz00_1805))
					{	/* Llib/process.scm 154 */
						BgL_auxz00_1922 = BgL_procz00_1805;
					}
				else
					{
						obj_t BgL_auxz00_1925;

						BgL_auxz00_1925 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1643z00zz__processz00,
							BINT(6597L), BGl_string1648z00zz__processz00,
							BGl_string1645z00zz__processz00, BgL_procz00_1805);
						FAILURE(BgL_auxz00_1925, BFALSE, BFALSE);
					}
				return BGl_processzd2errorzd2portz00zz__processz00(BgL_auxz00_1922);
			}
		}

	}



/* process-alive? */
	BGL_EXPORTED_DEF bool_t BGl_processzd2alivezf3z21zz__processz00(obj_t
		BgL_procz00_8)
	{
		{	/* Llib/process.scm 159 */
			BGL_TAIL return c_process_alivep(BgL_procz00_8);
		}

	}



/* &process-alive? */
	obj_t BGl_z62processzd2alivezf3z43zz__processz00(obj_t BgL_envz00_1806,
		obj_t BgL_procz00_1807)
	{
		{	/* Llib/process.scm 159 */
			{	/* Llib/process.scm 160 */
				bool_t BgL_tmpz00_1931;

				{	/* Llib/process.scm 160 */
					obj_t BgL_auxz00_1932;

					if (PROCESSP(BgL_procz00_1807))
						{	/* Llib/process.scm 160 */
							BgL_auxz00_1932 = BgL_procz00_1807;
						}
					else
						{
							obj_t BgL_auxz00_1935;

							BgL_auxz00_1935 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1643z00zz__processz00,
								BINT(6889L), BGl_string1649z00zz__processz00,
								BGl_string1645z00zz__processz00, BgL_procz00_1807);
							FAILURE(BgL_auxz00_1935, BFALSE, BFALSE);
						}
					BgL_tmpz00_1931 =
						BGl_processzd2alivezf3z21zz__processz00(BgL_auxz00_1932);
				}
				return BBOOL(BgL_tmpz00_1931);
			}
		}

	}



/* process-wait */
	BGL_EXPORTED_DEF bool_t BGl_processzd2waitzd2zz__processz00(obj_t
		BgL_procz00_9)
	{
		{	/* Llib/process.scm 165 */
			if (c_process_alivep(BgL_procz00_9))
				{	/* Llib/process.scm 166 */
					return CBOOL(c_process_wait(BgL_procz00_9));
				}
			else
				{	/* Llib/process.scm 166 */
					return ((bool_t) 0);
				}
		}

	}



/* &process-wait */
	obj_t BGl_z62processzd2waitzb0zz__processz00(obj_t BgL_envz00_1808,
		obj_t BgL_procz00_1809)
	{
		{	/* Llib/process.scm 165 */
			{	/* Llib/process.scm 166 */
				bool_t BgL_tmpz00_1945;

				{	/* Llib/process.scm 166 */
					obj_t BgL_auxz00_1946;

					if (PROCESSP(BgL_procz00_1809))
						{	/* Llib/process.scm 166 */
							BgL_auxz00_1946 = BgL_procz00_1809;
						}
					else
						{
							obj_t BgL_auxz00_1949;

							BgL_auxz00_1949 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1643z00zz__processz00,
								BINT(7175L), BGl_string1650z00zz__processz00,
								BGl_string1645z00zz__processz00, BgL_procz00_1809);
							FAILURE(BgL_auxz00_1949, BFALSE, BFALSE);
						}
					BgL_tmpz00_1945 =
						BGl_processzd2waitzd2zz__processz00(BgL_auxz00_1946);
				}
				return BBOOL(BgL_tmpz00_1945);
			}
		}

	}



/* process-exit-status */
	BGL_EXPORTED_DEF obj_t BGl_processzd2exitzd2statusz00zz__processz00(obj_t
		BgL_procz00_10)
	{
		{	/* Llib/process.scm 172 */
			BGL_TAIL return c_process_xstatus(BgL_procz00_10);
		}

	}



/* &process-exit-status */
	obj_t BGl_z62processzd2exitzd2statusz62zz__processz00(obj_t BgL_envz00_1810,
		obj_t BgL_procz00_1811)
	{
		{	/* Llib/process.scm 172 */
			{	/* Llib/process.scm 173 */
				obj_t BgL_auxz00_1956;

				if (PROCESSP(BgL_procz00_1811))
					{	/* Llib/process.scm 173 */
						BgL_auxz00_1956 = BgL_procz00_1811;
					}
				else
					{
						obj_t BgL_auxz00_1959;

						BgL_auxz00_1959 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1643z00zz__processz00,
							BINT(7500L), BGl_string1651z00zz__processz00,
							BGl_string1645z00zz__processz00, BgL_procz00_1811);
						FAILURE(BgL_auxz00_1959, BFALSE, BFALSE);
					}
				return BGl_processzd2exitzd2statusz00zz__processz00(BgL_auxz00_1956);
			}
		}

	}



/* process-send-signal */
	BGL_EXPORTED_DEF obj_t BGl_processzd2sendzd2signalz00zz__processz00(obj_t
		BgL_procz00_11, int BgL_signalz00_12)
	{
		{	/* Llib/process.scm 178 */
			BGL_TAIL return c_process_send_signal(BgL_procz00_11, BgL_signalz00_12);
		}

	}



/* &process-send-signal */
	obj_t BGl_z62processzd2sendzd2signalz62zz__processz00(obj_t BgL_envz00_1812,
		obj_t BgL_procz00_1813, obj_t BgL_signalz00_1814)
	{
		{	/* Llib/process.scm 178 */
			{	/* Llib/process.scm 179 */
				int BgL_auxz00_1972;
				obj_t BgL_auxz00_1965;

				{	/* Llib/process.scm 179 */
					obj_t BgL_tmpz00_1973;

					if (INTEGERP(BgL_signalz00_1814))
						{	/* Llib/process.scm 179 */
							BgL_tmpz00_1973 = BgL_signalz00_1814;
						}
					else
						{
							obj_t BgL_auxz00_1976;

							BgL_auxz00_1976 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1643z00zz__processz00,
								BINT(7805L), BGl_string1652z00zz__processz00,
								BGl_string1653z00zz__processz00, BgL_signalz00_1814);
							FAILURE(BgL_auxz00_1976, BFALSE, BFALSE);
						}
					BgL_auxz00_1972 = CINT(BgL_tmpz00_1973);
				}
				if (PROCESSP(BgL_procz00_1813))
					{	/* Llib/process.scm 179 */
						BgL_auxz00_1965 = BgL_procz00_1813;
					}
				else
					{
						obj_t BgL_auxz00_1968;

						BgL_auxz00_1968 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1643z00zz__processz00,
							BINT(7805L), BGl_string1652z00zz__processz00,
							BGl_string1645z00zz__processz00, BgL_procz00_1813);
						FAILURE(BgL_auxz00_1968, BFALSE, BFALSE);
					}
				return
					BGl_processzd2sendzd2signalz00zz__processz00(BgL_auxz00_1965,
					BgL_auxz00_1972);
			}
		}

	}



/* process-kill */
	BGL_EXPORTED_DEF obj_t BGl_processzd2killzd2zz__processz00(obj_t
		BgL_procz00_13)
	{
		{	/* Llib/process.scm 184 */
			c_process_kill(BgL_procz00_13);
			BGL_TAIL return
				BGl_closezd2processzd2portsz00zz__processz00(BgL_procz00_13);
		}

	}



/* &process-kill */
	obj_t BGl_z62processzd2killzb0zz__processz00(obj_t BgL_envz00_1815,
		obj_t BgL_procz00_1816)
	{
		{	/* Llib/process.scm 184 */
			{	/* Llib/process.scm 185 */
				obj_t BgL_auxz00_1984;

				if (PROCESSP(BgL_procz00_1816))
					{	/* Llib/process.scm 185 */
						BgL_auxz00_1984 = BgL_procz00_1816;
					}
				else
					{
						obj_t BgL_auxz00_1987;

						BgL_auxz00_1987 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1643z00zz__processz00,
							BINT(8123L), BGl_string1654z00zz__processz00,
							BGl_string1645z00zz__processz00, BgL_procz00_1816);
						FAILURE(BgL_auxz00_1987, BFALSE, BFALSE);
					}
				return BGl_processzd2killzd2zz__processz00(BgL_auxz00_1984);
			}
		}

	}



/* process-stop */
	BGL_EXPORTED_DEF obj_t BGl_processzd2stopzd2zz__processz00(obj_t
		BgL_procz00_14)
	{
		{	/* Llib/process.scm 191 */
			BGL_TAIL return c_process_stop(BgL_procz00_14);
		}

	}



/* &process-stop */
	obj_t BGl_z62processzd2stopzb0zz__processz00(obj_t BgL_envz00_1817,
		obj_t BgL_procz00_1818)
	{
		{	/* Llib/process.scm 191 */
			{	/* Llib/process.scm 192 */
				obj_t BgL_auxz00_1993;

				if (PROCESSP(BgL_procz00_1818))
					{	/* Llib/process.scm 192 */
						BgL_auxz00_1993 = BgL_procz00_1818;
					}
				else
					{
						obj_t BgL_auxz00_1996;

						BgL_auxz00_1996 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1643z00zz__processz00,
							BINT(8417L), BGl_string1655z00zz__processz00,
							BGl_string1645z00zz__processz00, BgL_procz00_1818);
						FAILURE(BgL_auxz00_1996, BFALSE, BFALSE);
					}
				return BGl_processzd2stopzd2zz__processz00(BgL_auxz00_1993);
			}
		}

	}



/* process-continue */
	BGL_EXPORTED_DEF obj_t BGl_processzd2continuezd2zz__processz00(obj_t
		BgL_procz00_15)
	{
		{	/* Llib/process.scm 197 */
			BGL_TAIL return c_process_continue(BgL_procz00_15);
		}

	}



/* &process-continue */
	obj_t BGl_z62processzd2continuezb0zz__processz00(obj_t BgL_envz00_1819,
		obj_t BgL_procz00_1820)
	{
		{	/* Llib/process.scm 197 */
			{	/* Llib/process.scm 198 */
				obj_t BgL_auxz00_2002;

				if (PROCESSP(BgL_procz00_1820))
					{	/* Llib/process.scm 198 */
						BgL_auxz00_2002 = BgL_procz00_1820;
					}
				else
					{
						obj_t BgL_auxz00_2005;

						BgL_auxz00_2005 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1643z00zz__processz00,
							BINT(8705L), BGl_string1656z00zz__processz00,
							BGl_string1645z00zz__processz00, BgL_procz00_1820);
						FAILURE(BgL_auxz00_2005, BFALSE, BFALSE);
					}
				return BGl_processzd2continuezd2zz__processz00(BgL_auxz00_2002);
			}
		}

	}



/* process-list */
	BGL_EXPORTED_DEF obj_t BGl_processzd2listzd2zz__processz00(void)
	{
		{	/* Llib/process.scm 203 */
			BGL_TAIL return c_process_list();
		}

	}



/* &process-list */
	obj_t BGl_z62processzd2listzb0zz__processz00(obj_t BgL_envz00_1821)
	{
		{	/* Llib/process.scm 203 */
			return BGl_processzd2listzd2zz__processz00();
		}

	}



/* run-process */
	BGL_EXPORTED_DEF obj_t BGl_runzd2processzd2zz__processz00(obj_t
		BgL_commandz00_16, obj_t BgL_restz00_17)
	{
		{	/* Llib/process.scm 212 */
			{	/* Llib/process.scm 213 */
				bool_t BgL_forkz00_1114;
				bool_t BgL_waitz00_1115;
				obj_t BgL_inputz00_1116;
				obj_t BgL_outputz00_1117;
				obj_t BgL_errorz00_1118;
				obj_t BgL_hostz00_1119;
				obj_t BgL_pipesz00_1120;
				obj_t BgL_argsz00_1121;
				obj_t BgL_envz00_1122;

				BgL_forkz00_1114 = ((bool_t) 1);
				BgL_waitz00_1115 = ((bool_t) 0);
				BgL_inputz00_1116 = BUNSPEC;
				BgL_outputz00_1117 = BUNSPEC;
				BgL_errorz00_1118 = BUNSPEC;
				BgL_hostz00_1119 = BUNSPEC;
				BgL_pipesz00_1120 = BGl_list1657z00zz__processz00;
				BgL_argsz00_1121 = BNIL;
				BgL_envz00_1122 = BNIL;
				{
					obj_t BgL_restz00_1125;

					BgL_restz00_1125 = BgL_restz00_17;
				BgL_zc3z04anonymousza31154ze3z87_1126:
					if (NULLP(BgL_restz00_1125))
						{	/* Llib/process.scm 229 */
							obj_t BgL_arg1157z00_1128;

							BgL_arg1157z00_1128 = bgl_reverse_bang(BgL_argsz00_1121);
							return
								c_run_process(BgL_hostz00_1119,
								BBOOL(BgL_forkz00_1114),
								BBOOL(BgL_waitz00_1115), BgL_inputz00_1116, BgL_outputz00_1117,
								BgL_errorz00_1118, BgL_commandz00_16, BgL_arg1157z00_1128,
								BgL_envz00_1122);
						}
					else
						{	/* Llib/process.scm 230 */
							bool_t BgL_test1745z00_2018;

							{	/* Llib/process.scm 230 */
								bool_t BgL_test1746z00_2019;

								{	/* Llib/process.scm 230 */
									obj_t BgL_tmpz00_2020;

									BgL_tmpz00_2020 = CAR(((obj_t) BgL_restz00_1125));
									BgL_test1746z00_2019 = KEYWORDP(BgL_tmpz00_2020);
								}
								if (BgL_test1746z00_2019)
									{	/* Llib/process.scm 230 */
										obj_t BgL_tmpz00_2024;

										BgL_tmpz00_2024 = CDR(((obj_t) BgL_restz00_1125));
										BgL_test1745z00_2018 = PAIRP(BgL_tmpz00_2024);
									}
								else
									{	/* Llib/process.scm 230 */
										BgL_test1745z00_2018 = ((bool_t) 0);
									}
							}
							if (BgL_test1745z00_2018)
								{	/* Llib/process.scm 231 */
									obj_t BgL_valz00_1134;

									{	/* Llib/process.scm 231 */
										obj_t BgL_pairz00_1593;

										BgL_pairz00_1593 = CDR(((obj_t) BgL_restz00_1125));
										BgL_valz00_1134 = CAR(BgL_pairz00_1593);
									}
									{	/* Llib/process.scm 232 */
										obj_t BgL_casezd2valuezd2_1135;

										BgL_casezd2valuezd2_1135 = CAR(((obj_t) BgL_restz00_1125));
										if (
											(BgL_casezd2valuezd2_1135 ==
												BGl_keyword1660z00zz__processz00))
											{	/* Llib/process.scm 232 */
												if (BOOLEANP(BgL_valz00_1134))
													{	/* Llib/process.scm 234 */
														BgL_waitz00_1115 = CBOOL(BgL_valz00_1134);
													}
												else
													{	/* Llib/process.scm 234 */
														BGl_errorz00zz__errorz00
															(BGl_string1662z00zz__processz00,
															BGl_string1663z00zz__processz00,
															BgL_restz00_1125);
													}
											}
										else
											{	/* Llib/process.scm 232 */
												if (
													(BgL_casezd2valuezd2_1135 ==
														BGl_keyword1664z00zz__processz00))
													{	/* Llib/process.scm 232 */
														if (BOOLEANP(BgL_valz00_1134))
															{	/* Llib/process.scm 238 */
																BgL_forkz00_1114 = CBOOL(BgL_valz00_1134);
															}
														else
															{	/* Llib/process.scm 238 */
																BGl_errorz00zz__errorz00
																	(BGl_string1662z00zz__processz00,
																	BGl_string1663z00zz__processz00,
																	BgL_restz00_1125);
															}
													}
												else
													{	/* Llib/process.scm 232 */
														if (
															(BgL_casezd2valuezd2_1135 ==
																BGl_keyword1666z00zz__processz00))
															{	/* Llib/process.scm 242 */
																bool_t BgL_test1752z00_2047;

																if (STRINGP(BgL_valz00_1134))
																	{	/* Llib/process.scm 242 */
																		BgL_test1752z00_2047 = ((bool_t) 1);
																	}
																else
																	{	/* Llib/process.scm 242 */
																		BgL_test1752z00_2047 =
																			CBOOL
																			(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																			(BgL_valz00_1134, BgL_pipesz00_1120));
																	}
																if (BgL_test1752z00_2047)
																	{	/* Llib/process.scm 242 */
																		BgL_inputz00_1116 = BgL_valz00_1134;
																	}
																else
																	{	/* Llib/process.scm 242 */
																		BGl_errorz00zz__errorz00
																			(BGl_string1662z00zz__processz00,
																			BGl_string1663z00zz__processz00,
																			BgL_restz00_1125);
																	}
															}
														else
															{	/* Llib/process.scm 232 */
																if (
																	(BgL_casezd2valuezd2_1135 ==
																		BGl_keyword1668z00zz__processz00))
																	{	/* Llib/process.scm 246 */
																		bool_t BgL_test1755z00_2055;

																		if (STRINGP(BgL_valz00_1134))
																			{	/* Llib/process.scm 246 */
																				BgL_test1755z00_2055 = ((bool_t) 1);
																			}
																		else
																			{	/* Llib/process.scm 246 */
																				if (CBOOL
																					(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																						(BgL_valz00_1134,
																							BgL_pipesz00_1120)))
																					{	/* Llib/process.scm 246 */
																						BgL_test1755z00_2055 = ((bool_t) 1);
																					}
																				else
																					{	/* Llib/process.scm 246 */
																						BgL_test1755z00_2055 =
																							(BgL_valz00_1134 ==
																							BGl_keyword1670z00zz__processz00);
																					}
																			}
																		if (BgL_test1755z00_2055)
																			{	/* Llib/process.scm 246 */
																				BgL_outputz00_1117 = BgL_valz00_1134;
																			}
																		else
																			{	/* Llib/process.scm 246 */
																				BGl_errorz00zz__errorz00
																					(BGl_string1662z00zz__processz00,
																					BGl_string1663z00zz__processz00,
																					BgL_restz00_1125);
																			}
																	}
																else
																	{	/* Llib/process.scm 232 */
																		if (
																			(BgL_casezd2valuezd2_1135 ==
																				BGl_keyword1672z00zz__processz00))
																			{	/* Llib/process.scm 250 */
																				bool_t BgL_test1759z00_2065;

																				if (STRINGP(BgL_valz00_1134))
																					{	/* Llib/process.scm 250 */
																						BgL_test1759z00_2065 = ((bool_t) 1);
																					}
																				else
																					{	/* Llib/process.scm 250 */
																						if (CBOOL
																							(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																								(BgL_valz00_1134,
																									BgL_pipesz00_1120)))
																							{	/* Llib/process.scm 250 */
																								BgL_test1759z00_2065 =
																									((bool_t) 1);
																							}
																						else
																							{	/* Llib/process.scm 250 */
																								BgL_test1759z00_2065 =
																									(BgL_valz00_1134 ==
																									BGl_keyword1670z00zz__processz00);
																							}
																					}
																				if (BgL_test1759z00_2065)
																					{	/* Llib/process.scm 250 */
																						BgL_errorz00_1118 = BgL_valz00_1134;
																					}
																				else
																					{	/* Llib/process.scm 250 */
																						BGl_errorz00zz__errorz00
																							(BGl_string1662z00zz__processz00,
																							BGl_string1663z00zz__processz00,
																							BgL_restz00_1125);
																					}
																			}
																		else
																			{	/* Llib/process.scm 232 */
																				if (
																					(BgL_casezd2valuezd2_1135 ==
																						BGl_keyword1674z00zz__processz00))
																					{	/* Llib/process.scm 232 */
																						if (STRINGP(BgL_valz00_1134))
																							{	/* Llib/process.scm 254 */
																								BgL_hostz00_1119 =
																									BgL_valz00_1134;
																							}
																						else
																							{	/* Llib/process.scm 254 */
																								BGl_errorz00zz__errorz00
																									(BGl_string1662z00zz__processz00,
																									BGl_string1663z00zz__processz00,
																									BgL_restz00_1125);
																							}
																					}
																				else
																					{	/* Llib/process.scm 232 */
																						if (
																							(BgL_casezd2valuezd2_1135 ==
																								BGl_keyword1676z00zz__processz00))
																							{	/* Llib/process.scm 232 */
																								if (STRINGP(BgL_valz00_1134))
																									{	/* Llib/process.scm 258 */
																										BgL_envz00_1122 =
																											MAKE_YOUNG_PAIR
																											(BgL_valz00_1134,
																											BgL_envz00_1122);
																									}
																								else
																									{	/* Llib/process.scm 258 */
																										BGl_errorz00zz__errorz00
																											(BGl_string1662z00zz__processz00,
																											BGl_string1663z00zz__processz00,
																											BgL_restz00_1125);
																									}
																							}
																						else
																							{	/* Llib/process.scm 232 */
																								BGl_errorz00zz__errorz00
																									(BGl_string1662z00zz__processz00,
																									BGl_string1663z00zz__processz00,
																									BgL_restz00_1125);
																							}
																					}
																			}
																	}
															}
													}
											}
									}
									{	/* Llib/process.scm 263 */
										obj_t BgL_arg1187z00_1160;

										{	/* Llib/process.scm 263 */
											obj_t BgL_pairz00_1603;

											BgL_pairz00_1603 = CDR(((obj_t) BgL_restz00_1125));
											BgL_arg1187z00_1160 = CDR(BgL_pairz00_1603);
										}
										{
											obj_t BgL_restz00_2088;

											BgL_restz00_2088 = BgL_arg1187z00_1160;
											BgL_restz00_1125 = BgL_restz00_2088;
											goto BgL_zc3z04anonymousza31154ze3z87_1126;
										}
									}
								}
							else
								{	/* Llib/process.scm 264 */
									bool_t BgL_test1766z00_2089;

									{	/* Llib/process.scm 264 */
										obj_t BgL_tmpz00_2090;

										BgL_tmpz00_2090 = CAR(((obj_t) BgL_restz00_1125));
										BgL_test1766z00_2089 = STRINGP(BgL_tmpz00_2090);
									}
									if (BgL_test1766z00_2089)
										{	/* Llib/process.scm 264 */
											{	/* Llib/process.scm 265 */
												obj_t BgL_arg1191z00_1164;

												BgL_arg1191z00_1164 = CAR(((obj_t) BgL_restz00_1125));
												BgL_argsz00_1121 =
													MAKE_YOUNG_PAIR(BgL_arg1191z00_1164,
													BgL_argsz00_1121);
											}
											{	/* Llib/process.scm 266 */
												obj_t BgL_arg1193z00_1165;

												BgL_arg1193z00_1165 = CDR(((obj_t) BgL_restz00_1125));
												{
													obj_t BgL_restz00_2099;

													BgL_restz00_2099 = BgL_arg1193z00_1165;
													BgL_restz00_1125 = BgL_restz00_2099;
													goto BgL_zc3z04anonymousza31154ze3z87_1126;
												}
											}
										}
									else
										{	/* Llib/process.scm 264 */
											return
												BGl_errorz00zz__errorz00
												(BGl_string1662z00zz__processz00,
												BGl_string1663z00zz__processz00, BgL_restz00_1125);
										}
								}
						}
				}
			}
		}

	}



/* &run-process */
	obj_t BGl_z62runzd2processzb0zz__processz00(obj_t BgL_envz00_1822,
		obj_t BgL_commandz00_1823, obj_t BgL_restz00_1824)
	{
		{	/* Llib/process.scm 212 */
			{	/* Llib/process.scm 213 */
				obj_t BgL_auxz00_2101;

				if (STRINGP(BgL_commandz00_1823))
					{	/* Llib/process.scm 213 */
						BgL_auxz00_2101 = BgL_commandz00_1823;
					}
				else
					{
						obj_t BgL_auxz00_2104;

						BgL_auxz00_2104 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1643z00zz__processz00,
							BINT(9491L), BGl_string1678z00zz__processz00,
							BGl_string1679z00zz__processz00, BgL_commandz00_1823);
						FAILURE(BgL_auxz00_2104, BFALSE, BFALSE);
					}
				return
					BGl_runzd2processzd2zz__processz00(BgL_auxz00_2101, BgL_restz00_1824);
			}
		}

	}



/* close-process-ports */
	BGL_EXPORTED_DEF obj_t BGl_closezd2processzd2portsz00zz__processz00(obj_t
		BgL_procz00_18)
	{
		{	/* Llib/process.scm 273 */
			{	/* Llib/process.scm 274 */
				bool_t BgL_test1768z00_2109;

				{	/* Llib/process.scm 274 */
					obj_t BgL_tmpz00_2110;

					BgL_tmpz00_2110 = PROCESS_INPUT_PORT(BgL_procz00_18);
					BgL_test1768z00_2109 = OUTPUT_PORTP(BgL_tmpz00_2110);
				}
				if (BgL_test1768z00_2109)
					{	/* Llib/process.scm 275 */
						obj_t BgL_arg1201z00_1176;

						BgL_arg1201z00_1176 = PROCESS_INPUT_PORT(BgL_procz00_18);
						bgl_close_output_port(((obj_t) BgL_arg1201z00_1176));
					}
				else
					{	/* Llib/process.scm 274 */
						BFALSE;
					}
			}
			{	/* Llib/process.scm 276 */
				bool_t BgL_test1769z00_2116;

				{	/* Llib/process.scm 276 */
					obj_t BgL_tmpz00_2117;

					BgL_tmpz00_2117 = PROCESS_ERROR_PORT(BgL_procz00_18);
					BgL_test1769z00_2116 = INPUT_PORTP(BgL_tmpz00_2117);
				}
				if (BgL_test1769z00_2116)
					{	/* Llib/process.scm 277 */
						obj_t BgL_arg1206z00_1180;

						BgL_arg1206z00_1180 = PROCESS_ERROR_PORT(BgL_procz00_18);
						bgl_close_input_port(((obj_t) BgL_arg1206z00_1180));
					}
				else
					{	/* Llib/process.scm 276 */
						BFALSE;
					}
			}
			{	/* Llib/process.scm 278 */
				bool_t BgL_test1770z00_2123;

				{	/* Llib/process.scm 278 */
					obj_t BgL_tmpz00_2124;

					BgL_tmpz00_2124 = PROCESS_OUTPUT_PORT(BgL_procz00_18);
					BgL_test1770z00_2123 = INPUT_PORTP(BgL_tmpz00_2124);
				}
				if (BgL_test1770z00_2123)
					{	/* Llib/process.scm 279 */
						obj_t BgL_arg1212z00_1184;

						BgL_arg1212z00_1184 = PROCESS_OUTPUT_PORT(BgL_procz00_18);
						return bgl_close_input_port(((obj_t) BgL_arg1212z00_1184));
					}
				else
					{	/* Llib/process.scm 278 */
						return BFALSE;
					}
			}
		}

	}



/* &close-process-ports */
	obj_t BGl_z62closezd2processzd2portsz62zz__processz00(obj_t BgL_envz00_1825,
		obj_t BgL_procz00_1826)
	{
		{	/* Llib/process.scm 273 */
			{	/* Llib/process.scm 275 */
				obj_t BgL_auxz00_2130;

				if (PROCESSP(BgL_procz00_1826))
					{	/* Llib/process.scm 275 */
						BgL_auxz00_2130 = BgL_procz00_1826;
					}
				else
					{
						obj_t BgL_auxz00_2133;

						BgL_auxz00_2133 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1643z00zz__processz00,
							BINT(11238L), BGl_string1680z00zz__processz00,
							BGl_string1645z00zz__processz00, BgL_procz00_1826);
						FAILURE(BgL_auxz00_2133, BFALSE, BFALSE);
					}
				return BGl_closezd2processzd2portsz00zz__processz00(BgL_auxz00_2130);
			}
		}

	}



/* unregister-process */
	BGL_EXPORTED_DEF obj_t BGl_unregisterzd2processzd2zz__processz00(obj_t
		BgL_procz00_19)
	{
		{	/* Llib/process.scm 284 */
			BGL_TAIL return c_unregister_process(BgL_procz00_19);
		}

	}



/* &unregister-process */
	obj_t BGl_z62unregisterzd2processzb0zz__processz00(obj_t BgL_envz00_1827,
		obj_t BgL_procz00_1828)
	{
		{	/* Llib/process.scm 284 */
			{	/* Llib/process.scm 285 */
				obj_t BgL_auxz00_2139;

				if (PROCESSP(BgL_procz00_1828))
					{	/* Llib/process.scm 285 */
						BgL_auxz00_2139 = BgL_procz00_1828;
					}
				else
					{
						obj_t BgL_auxz00_2142;

						BgL_auxz00_2142 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1643z00zz__processz00,
							BINT(11710L), BGl_string1681z00zz__processz00,
							BGl_string1645z00zz__processz00, BgL_procz00_1828);
						FAILURE(BgL_auxz00_2142, BFALSE, BFALSE);
					}
				return BGl_unregisterzd2processzd2zz__processz00(BgL_auxz00_2139);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__processz00(void)
	{
		{	/* Llib/process.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__processz00(void)
	{
		{	/* Llib/process.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__processz00(void)
	{
		{	/* Llib/process.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__processz00(void)
	{
		{	/* Llib/process.scm 15 */
			return
				BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1682z00zz__processz00));
		}

	}

#ifdef __cplusplus
}
#endif
