/*===========================================================================*/
/*   (Llib/module.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Llib/module.scm -indent -o objs/obj_u/Llib/module.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___MODULE_TYPE_DEFINITIONS
#define BGL___MODULE_TYPE_DEFINITIONS
#endif													// BGL___MODULE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_symbol1756z00zz__modulez00 = BUNSPEC;
	static obj_t BGl_symbol1758z00zz__modulez00 = BUNSPEC;
	static obj_t BGl_z62modulezd2addzd2accessz12z70zz__modulez00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_symbol1760z00zz__modulez00 = BUNSPEC;
	static obj_t BGl_z62bigloozd2modulezd2resolverzd2setz12za2zz__modulez00(obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__modulez00 = BUNSPEC;
	static obj_t BGl_resolvezd2abasezf2bucketz20zz__modulez00(obj_t, obj_t);
	static obj_t BGl_za2afileszd2tableza2zd2zz__modulez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_bigloozd2modulezd2resolverz00zz__modulez00(void);
	extern bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
	extern obj_t BGl_makezd2hashtablezd2zz__hashz00(obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__modulez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__hashz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__configurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_list1755z00zz__modulez00 = BUNSPEC;
	extern bool_t fexists(char *);
	extern obj_t BGl_warningz00zz__errorz00(obj_t);
	extern obj_t BGl_assocz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t
		BGl_z62zc3z04z52bigloozd2modulezd2res1186ze3z14zz__modulez00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zz__modulez00(void);
	static obj_t BGl_z62modulezd2abasezd2setz12z70zz__modulez00(obj_t, obj_t);
	extern obj_t BGl_filezd2namezd2canonicaliza7eza7zz__osz00(obj_t);
	extern obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__modulez00(void);
	extern bool_t BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_genericzd2initzd2zz__modulez00(void);
	extern obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__modulez00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__modulez00(void);
	static obj_t BGl_z52bigloozd2modulezd2resolverz52zz__modulez00 = BUNSPEC;
	static obj_t BGl_objectzd2initzd2zz__modulez00(void);
	extern obj_t BGl_dirnamez00zz__osz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2abasezd2setz12z12zz__modulez00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bigloozd2modulezd2resolverzd2setz12zc0zz__modulez00(obj_t);
	extern bool_t bgl_directoryp(char *);
	extern obj_t BGl_makezd2filezd2namez00zz__osz00(obj_t, obj_t);
	extern obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_modulezd2addzd2accesszd2innerz12zc0zz__modulez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_hashtablezd2putz12zc0zz__hashz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62modulezd2abasezb0zz__modulez00(obj_t);
	static obj_t BGl_modulezd2readzd2accesszd2filezd2zz__modulez00(obj_t);
	static obj_t BGl_methodzd2initzd2zz__modulez00(void);
	extern obj_t BGl_callzd2withzd2inputzd2filezd2zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2abasezd2zz__modulez00(void);
	static obj_t BGl_z62bigloozd2modulezd2resolverz62zz__modulez00(obj_t);
	static obj_t BGl_z62modulezd2defaultzd2resolverz62zz__modulez00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2loadzd2accesszd2filezd2zz__modulez00(obj_t);
	static obj_t BGl_z62modulezd2loadzd2accesszd2filezb0zz__modulez00(obj_t,
		obj_t);
	static obj_t BGl_resolvezd2abaseza2z70zz__modulez00(obj_t);
	extern obj_t string_append(obj_t, obj_t);
	static obj_t BGl_symbol1738z00zz__modulez00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31351ze3ze5zz__modulez00(obj_t, obj_t);
	static obj_t BGl_afilezd2tablezd2zz__modulez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2addzd2accessz12z12zz__modulez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_moduleszd2mutexzd2zz__modulez00 = BUNSPEC;
	extern obj_t BGl_hashtablezd2getzd2zz__hashz00(obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_modulezd2abasezd2envz00zz__modulez00,
		BgL_bgl_za762moduleza7d2abas1771z00, BGl_z62modulezd2abasezb0zz__modulez00,
		0L, BUNSPEC, 0);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_modulezd2defaultzd2resolverzd2envzd2zz__modulez00,
		BgL_bgl_za762moduleza7d2defa1772z00,
		BGl_z62modulezd2defaultzd2resolverz62zz__modulez00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_modulezd2loadzd2accesszd2filezd2envz00zz__modulez00,
		BgL_bgl_za762moduleza7d2load1773z00,
		BGl_z62modulezd2loadzd2accesszd2filezb0zz__modulez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2modulezd2resolverzd2setz12zd2envz12zz__modulez00,
		BgL_bgl_za762biglooza7d2modu1774z00,
		BGl_z62bigloozd2modulezd2resolverzd2setz12za2zz__modulez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2modulezd2resolverzd2envzd2zz__modulez00,
		BgL_bgl_za762biglooza7d2modu1775z00,
		BGl_z62bigloozd2modulezd2resolverz62zz__modulez00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1737z00zz__modulez00,
		BgL_bgl_string1737za700za7za7_1776za7, "modules", 7);
	      DEFINE_STRING(BGl_string1739z00zz__modulez00,
		BgL_bgl_string1739za700za7za7_1777za7, "bigloo-module-resolver-set!", 27);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_modulezd2abasezd2setz12zd2envzc0zz__modulez00,
		BgL_bgl_za762moduleza7d2abas1778z00,
		BGl_z62modulezd2abasezd2setz12z70zz__modulez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1740z00zz__modulez00,
		BgL_bgl_string1740za700za7za7_1779za7, "Illegal resolver", 16);
	      DEFINE_STRING(BGl_string1741z00zz__modulez00,
		BgL_bgl_string1741za700za7za7_1780za7,
		"/tmp/bigloo/runtime/Llib/module.scm", 35);
	      DEFINE_STRING(BGl_string1742z00zz__modulez00,
		BgL_bgl_string1742za700za7za7_1781za7, "&bigloo-module-resolver-set!", 28);
	      DEFINE_STRING(BGl_string1743z00zz__modulez00,
		BgL_bgl_string1743za700za7za7_1782za7, "procedure", 9);
	      DEFINE_STRING(BGl_string1744z00zz__modulez00,
		BgL_bgl_string1744za700za7za7_1783za7, ".", 1);
	      DEFINE_STRING(BGl_string1745z00zz__modulez00,
		BgL_bgl_string1745za700za7za7_1784za7, "&module-default-resolver", 24);
	      DEFINE_STRING(BGl_string1746z00zz__modulez00,
		BgL_bgl_string1746za700za7za7_1785za7, "symbol", 6);
	      DEFINE_STRING(BGl_string1747z00zz__modulez00,
		BgL_bgl_string1747za700za7za7_1786za7, "pair-nil", 8);
	      DEFINE_STRING(BGl_string1748z00zz__modulez00,
		BgL_bgl_string1748za700za7za7_1787za7, ".scm", 4);
	      DEFINE_STRING(BGl_string1749z00zz__modulez00,
		BgL_bgl_string1749za700za7za7_1788za7, "\"", 1);
	      DEFINE_STRING(BGl_string1750z00zz__modulez00,
		BgL_bgl_string1750za700za7za7_1789za7, "] in directory \"", 16);
	      DEFINE_STRING(BGl_string1751z00zz__modulez00,
		BgL_bgl_string1751za700za7za7_1790za7, " ", 1);
	      DEFINE_STRING(BGl_string1752z00zz__modulez00,
		BgL_bgl_string1752za700za7za7_1791za7, " [", 2);
	      DEFINE_STRING(BGl_string1753z00zz__modulez00,
		BgL_bgl_string1753za700za7za7_1792za7, "access redefinition -- ", 23);
	      DEFINE_STRING(BGl_string1754z00zz__modulez00,
		BgL_bgl_string1754za700za7za7_1793za7, "add-access!", 11);
	      DEFINE_STRING(BGl_string1757z00zz__modulez00,
		BgL_bgl_string1757za700za7za7_1794za7, "set-cdr!", 8);
	      DEFINE_STRING(BGl_string1759z00zz__modulez00,
		BgL_bgl_string1759za700za7za7_1795za7, "cell", 4);
	      DEFINE_STRING(BGl_string1761z00zz__modulez00,
		BgL_bgl_string1761za700za7za7_1796za7, "files", 5);
	      DEFINE_STRING(BGl_string1762z00zz__modulez00,
		BgL_bgl_string1762za700za7za7_1797za7, "&module-add-access!", 19);
	      DEFINE_STRING(BGl_string1763z00zz__modulez00,
		BgL_bgl_string1763za700za7za7_1798za7, "pair", 4);
	      DEFINE_STRING(BGl_string1764z00zz__modulez00,
		BgL_bgl_string1764za700za7za7_1799za7, "bstring", 7);
	      DEFINE_STRING(BGl_string1765z00zz__modulez00,
		BgL_bgl_string1765za700za7za7_1800za7, "Illegal entry -- ", 17);
	      DEFINE_STRING(BGl_string1766z00zz__modulez00,
		BgL_bgl_string1766za700za7za7_1801za7, "module-read-access-file", 23);
	      DEFINE_STRING(BGl_string1767z00zz__modulez00,
		BgL_bgl_string1767za700za7za7_1802za7, ".afile", 6);
	      DEFINE_STRING(BGl_string1768z00zz__modulez00,
		BgL_bgl_string1768za700za7za7_1803za7, "&module-load-access-file", 24);
	      DEFINE_STRING(BGl_string1769z00zz__modulez00,
		BgL_bgl_string1769za700za7za7_1804za7, "", 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_modulezd2addzd2accessz12zd2envzc0zz__modulez00,
		BgL_bgl_za762moduleza7d2addza71805za7,
		BGl_z62modulezd2addzd2accessz12z70zz__modulez00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string1770z00zz__modulez00,
		BgL_bgl_string1770za700za7za7_1806za7, "__module", 8);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_symbol1756z00zz__modulez00));
		     ADD_ROOT((void *) (&BGl_symbol1758z00zz__modulez00));
		     ADD_ROOT((void *) (&BGl_symbol1760z00zz__modulez00));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__modulez00));
		     ADD_ROOT((void *) (&BGl_za2afileszd2tableza2zd2zz__modulez00));
		     ADD_ROOT((void *) (&BGl_list1755z00zz__modulez00));
		   
			 ADD_ROOT((void *) (&BGl_z52bigloozd2modulezd2resolverz52zz__modulez00));
		     ADD_ROOT((void *) (&BGl_symbol1738z00zz__modulez00));
		     ADD_ROOT((void *) (&BGl_afilezd2tablezd2zz__modulez00));
		     ADD_ROOT((void *) (&BGl_moduleszd2mutexzd2zz__modulez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__modulez00(long
		BgL_checksumz00_2140, char *BgL_fromz00_2141)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__modulez00))
				{
					BGl_requirezd2initializa7ationz75zz__modulez00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__modulez00();
					BGl_cnstzd2initzd2zz__modulez00();
					BGl_importedzd2moduleszd2initz00zz__modulez00();
					return BGl_toplevelzd2initzd2zz__modulez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__modulez00(void)
	{
		{	/* Llib/module.scm 17 */
			BGl_symbol1738z00zz__modulez00 =
				bstring_to_symbol(BGl_string1739z00zz__modulez00);
			BGl_symbol1756z00zz__modulez00 =
				bstring_to_symbol(BGl_string1757z00zz__modulez00);
			BGl_symbol1758z00zz__modulez00 =
				bstring_to_symbol(BGl_string1759z00zz__modulez00);
			BGl_symbol1760z00zz__modulez00 =
				bstring_to_symbol(BGl_string1761z00zz__modulez00);
			return (BGl_list1755z00zz__modulez00 =
				MAKE_YOUNG_PAIR(BGl_symbol1756z00zz__modulez00,
					MAKE_YOUNG_PAIR(BGl_symbol1758z00zz__modulez00,
						MAKE_YOUNG_PAIR(BGl_symbol1760z00zz__modulez00, BNIL))), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__modulez00(void)
	{
		{	/* Llib/module.scm 17 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__modulez00(void)
	{
		{	/* Llib/module.scm 17 */
			BGl_moduleszd2mutexzd2zz__modulez00 =
				bgl_make_mutex(BGl_string1737z00zz__modulez00);
			BGl_afilezd2tablezd2zz__modulez00 = BNIL;
			BGl_z52bigloozd2modulezd2resolverz52zz__modulez00 =
				BGl_modulezd2defaultzd2resolverzd2envzd2zz__modulez00;
			{	/* Llib/module.scm 218 */
				obj_t BgL_list1175z00_1120;

				BgL_list1175z00_1120 = MAKE_YOUNG_PAIR(BINT(256L), BNIL);
				return (BGl_za2afileszd2tableza2zd2zz__modulez00 =
					BGl_makezd2hashtablezd2zz__hashz00(BgL_list1175z00_1120), BUNSPEC);
			}
		}

	}



/* module-abase */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2abasezd2zz__modulez00(void)
	{
		{	/* Llib/module.scm 75 */
			return BGL_ABASE();
		}

	}



/* &module-abase */
	obj_t BGl_z62modulezd2abasezb0zz__modulez00(obj_t BgL_envz00_2068)
	{
		{	/* Llib/module.scm 75 */
			return BGl_modulezd2abasezd2zz__modulez00();
		}

	}



/* module-abase-set! */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2abasezd2setz12z12zz__modulez00(obj_t
		BgL_valz00_3)
	{
		{	/* Llib/module.scm 78 */
			return BGL_ABASE_SET(BgL_valz00_3);
		}

	}



/* &module-abase-set! */
	obj_t BGl_z62modulezd2abasezd2setz12z70zz__modulez00(obj_t BgL_envz00_2069,
		obj_t BgL_valz00_2070)
	{
		{	/* Llib/module.scm 78 */
			return BGl_modulezd2abasezd2setz12z12zz__modulez00(BgL_valz00_2070);
		}

	}



/* bigloo-module-resolver */
	BGL_EXPORTED_DEF obj_t BGl_bigloozd2modulezd2resolverz00zz__modulez00(void)
	{
		{	/* Llib/module.scm 91 */
			return BGl_z52bigloozd2modulezd2resolverz52zz__modulez00;
		}

	}



/* &bigloo-module-resolver */
	obj_t BGl_z62bigloozd2modulezd2resolverz62zz__modulez00(obj_t BgL_envz00_2071)
	{
		{	/* Llib/module.scm 91 */
			return BGl_bigloozd2modulezd2resolverz00zz__modulez00();
		}

	}



/* bigloo-module-resolver-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bigloozd2modulezd2resolverzd2setz12zc0zz__modulez00(obj_t
		BgL_resolvez00_4)
	{
		{	/* Llib/module.scm 94 */
			{	/* Llib/module.scm 95 */
				obj_t BgL_top1809z00_2167;

				BgL_top1809z00_2167 = BGL_EXITD_TOP_AS_OBJ();
				BGL_MUTEX_LOCK(BGl_moduleszd2mutexzd2zz__modulez00);
				BGL_EXITD_PUSH_PROTECT(BgL_top1809z00_2167,
					BGl_moduleszd2mutexzd2zz__modulez00);
				BUNSPEC;
				{	/* Llib/module.scm 95 */
					obj_t BgL_tmp1808z00_2166;

					if (PROCEDURE_CORRECT_ARITYP(BgL_resolvez00_4, (int) (2L)))
						{	/* Llib/module.scm 100 */
							obj_t BgL_zc3z04z52bigloozd2modulezd2res1186ze3z76_2072;

							BgL_zc3z04z52bigloozd2modulezd2res1186ze3z76_2072 =
								MAKE_FX_PROCEDURE
								(BGl_z62zc3z04z52bigloozd2modulezd2res1186ze3z14zz__modulez00,
								(int) (3L), (int) (1L));
							PROCEDURE_SET(BgL_zc3z04z52bigloozd2modulezd2res1186ze3z76_2072,
								(int) (0L), BgL_resolvez00_4);
							BgL_tmp1808z00_2166 =
								(BGl_z52bigloozd2modulezd2resolverz52zz__modulez00 =
								BgL_zc3z04z52bigloozd2modulezd2res1186ze3z76_2072, BUNSPEC);
						}
					else
						{	/* Llib/module.scm 97 */
							if (PROCEDURE_CORRECT_ARITYP(BgL_resolvez00_4, (int) (3L)))
								{	/* Llib/module.scm 101 */
									BgL_tmp1808z00_2166 =
										(BGl_z52bigloozd2modulezd2resolverz52zz__modulez00 =
										BgL_resolvez00_4, BUNSPEC);
								}
							else
								{	/* Llib/module.scm 101 */
									BgL_tmp1808z00_2166 =
										BGl_errorz00zz__errorz00(BGl_symbol1738z00zz__modulez00,
										BGl_string1740z00zz__modulez00, BgL_resolvez00_4);
								}
						}
					BGL_EXITD_POP_PROTECT(BgL_top1809z00_2167);
					BUNSPEC;
					BGL_MUTEX_UNLOCK(BGl_moduleszd2mutexzd2zz__modulez00);
					return BgL_tmp1808z00_2166;
				}
			}
		}

	}



/* &bigloo-module-resolver-set! */
	obj_t BGl_z62bigloozd2modulezd2resolverzd2setz12za2zz__modulez00(obj_t
		BgL_envz00_2073, obj_t BgL_resolvez00_2074)
	{
		{	/* Llib/module.scm 94 */
			{	/* Llib/module.scm 95 */
				obj_t BgL_auxz00_2185;

				if (PROCEDUREP(BgL_resolvez00_2074))
					{	/* Llib/module.scm 95 */
						BgL_auxz00_2185 = BgL_resolvez00_2074;
					}
				else
					{
						obj_t BgL_auxz00_2188;

						BgL_auxz00_2188 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1741z00zz__modulez00,
							BINT(3527L), BGl_string1742z00zz__modulez00,
							BGl_string1743z00zz__modulez00, BgL_resolvez00_2074);
						FAILURE(BgL_auxz00_2188, BFALSE, BFALSE);
					}
				return
					BGl_bigloozd2modulezd2resolverzd2setz12zc0zz__modulez00
					(BgL_auxz00_2185);
			}
		}

	}



/* &<@%bigloo-module-res1186> */
	obj_t BGl_z62zc3z04z52bigloozd2modulezd2res1186ze3z14zz__modulez00(obj_t
		BgL_envz00_2075, obj_t BgL_modulez00_2077, obj_t BgL_filesz00_2078,
		obj_t BgL_abasez00_2079)
	{
		{	/* Llib/module.scm 100 */
			{	/* Llib/module.scm 100 */
				obj_t BgL_resolvez00_2076;

				BgL_resolvez00_2076 =
					((obj_t) PROCEDURE_REF(BgL_envz00_2075, (int) (0L)));
				return
					BGL_PROCEDURE_CALL2(BgL_resolvez00_2076, BgL_modulez00_2077,
					BgL_abasez00_2079);
			}
		}

	}



/* &module-default-resolver */
	obj_t BGl_z62modulezd2defaultzd2resolverz62zz__modulez00(obj_t
		BgL_envz00_2064, obj_t BgL_modz00_2065, obj_t BgL_filesz00_2066,
		obj_t BgL_abasez00_2067)
	{
		{	/* Llib/module.scm 113 */
			{	/* Llib/module.scm 114 */
				obj_t BgL_modz00_2112;
				obj_t BgL_filesz00_2113;

				if (SYMBOLP(BgL_modz00_2065))
					{	/* Llib/module.scm 114 */
						BgL_modz00_2112 = BgL_modz00_2065;
					}
				else
					{
						obj_t BgL_auxz00_2203;

						BgL_auxz00_2203 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1741z00zz__modulez00,
							BINT(4515L), BGl_string1745z00zz__modulez00,
							BGl_string1746z00zz__modulez00, BgL_modz00_2065);
						FAILURE(BgL_auxz00_2203, BFALSE, BFALSE);
					}
				{	/* Llib/module.scm 114 */
					bool_t BgL_test1814z00_2207;

					if (PAIRP(BgL_filesz00_2066))
						{	/* Llib/module.scm 114 */
							BgL_test1814z00_2207 = ((bool_t) 1);
						}
					else
						{	/* Llib/module.scm 114 */
							BgL_test1814z00_2207 = NULLP(BgL_filesz00_2066);
						}
					if (BgL_test1814z00_2207)
						{	/* Llib/module.scm 114 */
							BgL_filesz00_2113 = BgL_filesz00_2066;
						}
					else
						{
							obj_t BgL_auxz00_2211;

							BgL_auxz00_2211 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1741z00zz__modulez00,
								BINT(4515L), BGl_string1745z00zz__modulez00,
								BGl_string1747z00zz__modulez00, BgL_filesz00_2066);
							FAILURE(BgL_auxz00_2211, BFALSE, BFALSE);
						}
				}
				{	/* Llib/module.scm 114 */
					obj_t BgL_top1817z00_2216;

					BgL_top1817z00_2216 = BGL_EXITD_TOP_AS_OBJ();
					BGL_MUTEX_LOCK(BGl_moduleszd2mutexzd2zz__modulez00);
					BGL_EXITD_PUSH_PROTECT(BgL_top1817z00_2216,
						BGl_moduleszd2mutexzd2zz__modulez00);
					BUNSPEC;
					{	/* Llib/module.scm 114 */
						obj_t BgL_tmp1816z00_2215;

						if (NULLP(BgL_filesz00_2113))
							{	/* Llib/module.scm 116 */
								if (NULLP(BgL_abasez00_2067))
									{	/* Llib/module.scm 150 */
										obj_t BgL_basez00_2114;

										BgL_basez00_2114 =
											BGl_assocz00zz__r4_pairs_and_lists_6_3z00
											(BGl_string1744z00zz__modulez00,
											BGl_afilezd2tablezd2zz__modulez00);
										if (PAIRP(BgL_basez00_2114))
											{	/* Llib/module.scm 151 */
												BgL_tmp1816z00_2215 =
													BGl_resolvezd2abasezf2bucketz20zz__modulez00
													(BgL_modz00_2112, BgL_basez00_2114);
											}
										else
											{	/* Llib/module.scm 151 */
												BgL_tmp1816z00_2215 = BNIL;
											}
									}
								else
									{	/* Llib/module.scm 119 */
										if (STRINGP(BgL_abasez00_2067))
											{	/* Llib/module.scm 150 */
												obj_t BgL_basez00_2115;

												BgL_basez00_2115 =
													BGl_assocz00zz__r4_pairs_and_lists_6_3z00
													(BgL_abasez00_2067,
													BGl_afilezd2tablezd2zz__modulez00);
												if (PAIRP(BgL_basez00_2115))
													{	/* Llib/module.scm 151 */
														BgL_tmp1816z00_2215 =
															BGl_resolvezd2abasezf2bucketz20zz__modulez00
															(BgL_modz00_2112, BgL_basez00_2115);
													}
												else
													{	/* Llib/module.scm 151 */
														BgL_tmp1816z00_2215 = BNIL;
													}
											}
										else
											{	/* Llib/module.scm 121 */
												if (PAIRP(BgL_abasez00_2067))
													{
														obj_t BgL_abasez00_2117;

														BgL_abasez00_2117 = BgL_abasez00_2067;
													BgL_loopz00_2116:
														if (PAIRP(BgL_abasez00_2117))
															{	/* Llib/module.scm 126 */
																obj_t BgL_resolvez00_2118;

																{	/* Llib/module.scm 126 */
																	obj_t BgL_arg1197z00_2119;

																	BgL_arg1197z00_2119 = CAR(BgL_abasez00_2117);
																	{	/* Llib/module.scm 150 */
																		obj_t BgL_basez00_2120;

																		BgL_basez00_2120 =
																			BGl_assocz00zz__r4_pairs_and_lists_6_3z00
																			(BgL_arg1197z00_2119,
																			BGl_afilezd2tablezd2zz__modulez00);
																		if (PAIRP(BgL_basez00_2120))
																			{	/* Llib/module.scm 151 */
																				BgL_resolvez00_2118 =
																					BGl_resolvezd2abasezf2bucketz20zz__modulez00
																					(BgL_modz00_2112, BgL_basez00_2120);
																			}
																		else
																			{	/* Llib/module.scm 151 */
																				BgL_resolvez00_2118 = BNIL;
																			}
																	}
																}
																if (PAIRP(BgL_resolvez00_2118))
																	{	/* Llib/module.scm 127 */
																		BgL_tmp1816z00_2215 = BgL_resolvez00_2118;
																	}
																else
																	{
																		obj_t BgL_abasez00_2245;

																		BgL_abasez00_2245 = CDR(BgL_abasez00_2117);
																		BgL_abasez00_2117 = BgL_abasez00_2245;
																		goto BgL_loopz00_2116;
																	}
															}
														else
															{	/* Llib/module.scm 125 */
																BgL_tmp1816z00_2215 = BNIL;
															}
													}
												else
													{	/* Llib/module.scm 123 */
														BgL_tmp1816z00_2215 =
															BGl_resolvezd2abaseza2z70zz__modulez00
															(BgL_modz00_2112);
													}
											}
									}
							}
						else
							{	/* Llib/module.scm 116 */
								BGl_modulezd2addzd2accessz12z12zz__modulez00(BgL_modz00_2112,
									BgL_filesz00_2113, BgL_abasez00_2067);
								BgL_tmp1816z00_2215 = BgL_filesz00_2113;
							}
						BGL_EXITD_POP_PROTECT(BgL_top1817z00_2216);
						BUNSPEC;
						BGL_MUTEX_UNLOCK(BGl_moduleszd2mutexzd2zz__modulez00);
						return BgL_tmp1816z00_2215;
					}
				}
			}
		}

	}



/* resolve-abase* */
	obj_t BGl_resolvezd2abaseza2z70zz__modulez00(obj_t BgL_modz00_8)
	{
		{	/* Llib/module.scm 137 */
			{
				obj_t BgL_afilez00_1153;

				BgL_afilez00_1153 = BGl_afilezd2tablezd2zz__modulez00;
			BgL_zc3z04anonymousza31198ze3z87_1154:
				if (NULLP(BgL_afilez00_1153))
					{	/* Llib/module.scm 139 */
						return BNIL;
					}
				else
					{	/* Llib/module.scm 141 */
						obj_t BgL_fz00_1156;

						{	/* Llib/module.scm 141 */
							obj_t BgL_arg1202z00_1159;

							BgL_arg1202z00_1159 = CAR(((obj_t) BgL_afilez00_1153));
							BgL_fz00_1156 =
								BGl_resolvezd2abasezf2bucketz20zz__modulez00(BgL_modz00_8,
								BgL_arg1202z00_1159);
						}
						if (PAIRP(BgL_fz00_1156))
							{	/* Llib/module.scm 142 */
								return BgL_fz00_1156;
							}
						else
							{	/* Llib/module.scm 144 */
								obj_t BgL_arg1201z00_1158;

								BgL_arg1201z00_1158 = CDR(((obj_t) BgL_afilez00_1153));
								{
									obj_t BgL_afilez00_2260;

									BgL_afilez00_2260 = BgL_arg1201z00_1158;
									BgL_afilez00_1153 = BgL_afilez00_2260;
									goto BgL_zc3z04anonymousza31198ze3z87_1154;
								}
							}
					}
			}
		}

	}



/* resolve-abase/bucket */
	obj_t BGl_resolvezd2abasezf2bucketz20zz__modulez00(obj_t BgL_modz00_11,
		obj_t BgL_basez00_12)
	{
		{	/* Llib/module.scm 158 */
			{	/* Llib/module.scm 159 */
				obj_t BgL_cellz00_1163;

				BgL_cellz00_1163 =
					BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_modz00_11,
					CDR(((obj_t) BgL_basez00_12)));
				if (PAIRP(BgL_cellz00_1163))
					{	/* Llib/module.scm 161 */
						obj_t BgL_hook1075z00_1165;

						BgL_hook1075z00_1165 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
						{	/* Llib/module.scm 161 */
							obj_t BgL_g1077z00_1166;

							BgL_g1077z00_1166 = CDR(BgL_cellz00_1163);
							{
								obj_t BgL_l1072z00_1168;
								obj_t BgL_h1073z00_1169;

								BgL_l1072z00_1168 = BgL_g1077z00_1166;
								BgL_h1073z00_1169 = BgL_hook1075z00_1165;
							BgL_zc3z04anonymousza31205ze3z87_1170:
								if (NULLP(BgL_l1072z00_1168))
									{	/* Llib/module.scm 161 */
										return CDR(BgL_hook1075z00_1165);
									}
								else
									{	/* Llib/module.scm 161 */
										bool_t BgL_test1831z00_2271;

										{	/* Llib/module.scm 161 */
											obj_t BgL_tmpz00_2272;

											BgL_tmpz00_2272 = CAR(((obj_t) BgL_l1072z00_1168));
											BgL_test1831z00_2271 = STRINGP(BgL_tmpz00_2272);
										}
										if (BgL_test1831z00_2271)
											{	/* Llib/module.scm 161 */
												obj_t BgL_nh1074z00_1174;

												{	/* Llib/module.scm 161 */
													obj_t BgL_arg1212z00_1176;

													BgL_arg1212z00_1176 =
														CAR(((obj_t) BgL_l1072z00_1168));
													BgL_nh1074z00_1174 =
														MAKE_YOUNG_PAIR(BgL_arg1212z00_1176, BNIL);
												}
												SET_CDR(BgL_h1073z00_1169, BgL_nh1074z00_1174);
												{	/* Llib/module.scm 161 */
													obj_t BgL_arg1210z00_1175;

													BgL_arg1210z00_1175 =
														CDR(((obj_t) BgL_l1072z00_1168));
													{
														obj_t BgL_h1073z00_2283;
														obj_t BgL_l1072z00_2282;

														BgL_l1072z00_2282 = BgL_arg1210z00_1175;
														BgL_h1073z00_2283 = BgL_nh1074z00_1174;
														BgL_h1073z00_1169 = BgL_h1073z00_2283;
														BgL_l1072z00_1168 = BgL_l1072z00_2282;
														goto BgL_zc3z04anonymousza31205ze3z87_1170;
													}
												}
											}
										else
											{	/* Llib/module.scm 161 */
												obj_t BgL_arg1215z00_1177;

												BgL_arg1215z00_1177 = CDR(((obj_t) BgL_l1072z00_1168));
												{
													obj_t BgL_l1072z00_2286;

													BgL_l1072z00_2286 = BgL_arg1215z00_1177;
													BgL_l1072z00_1168 = BgL_l1072z00_2286;
													goto BgL_zc3z04anonymousza31205ze3z87_1170;
												}
											}
									}
							}
						}
					}
				else
					{	/* Llib/module.scm 162 */
						obj_t BgL_fz00_1180;

						{	/* Llib/module.scm 162 */
							obj_t BgL_arg1219z00_1183;

							{	/* Llib/module.scm 162 */
								obj_t BgL_arg1419z00_1759;

								BgL_arg1419z00_1759 = SYMBOL_TO_STRING(BgL_modz00_11);
								BgL_arg1219z00_1183 =
									BGl_stringzd2copyzd2zz__r4_strings_6_7z00
									(BgL_arg1419z00_1759);
							}
							BgL_fz00_1180 =
								string_append(BgL_arg1219z00_1183,
								BGl_string1748z00zz__modulez00);
						}
						if (fexists(BSTRING_TO_STRING(BgL_fz00_1180)))
							{	/* Llib/module.scm 164 */
								obj_t BgL_list1218z00_1182;

								BgL_list1218z00_1182 = MAKE_YOUNG_PAIR(BgL_fz00_1180, BNIL);
								return BgL_list1218z00_1182;
							}
						else
							{	/* Llib/module.scm 163 */
								return BNIL;
							}
					}
			}
		}

	}



/* module-add-access-inner! */
	obj_t BGl_modulezd2addzd2accesszd2innerz12zc0zz__modulez00(obj_t
		BgL_modulez00_13, obj_t BgL_filesz00_14, obj_t BgL_abasez00_15)
	{
		{	/* Llib/module.scm 170 */
			{	/* Llib/module.scm 171 */
				obj_t BgL_basez00_1185;

				BgL_basez00_1185 =
					BGl_assocz00zz__r4_pairs_and_lists_6_3z00(BgL_abasez00_15,
					BGl_afilezd2tablezd2zz__modulez00);
				if (CBOOL(BgL_basez00_1185))
					{	/* Llib/module.scm 175 */
						obj_t BgL_cellz00_1186;

						BgL_cellz00_1186 =
							BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_modulez00_13,
							CDR(((obj_t) BgL_basez00_1185)));
						if (CBOOL(BgL_cellz00_1186))
							{	/* Llib/module.scm 176 */
								if (BGl_equalzf3zf3zz__r4_equivalence_6_2z00(CDR(
											((obj_t) BgL_cellz00_1186)), BgL_filesz00_14))
									{	/* Llib/module.scm 178 */
										return BFALSE;
									}
								else
									{	/* Llib/module.scm 178 */
										{	/* Llib/module.scm 181 */
											obj_t BgL_arg1223z00_1189;

											BgL_arg1223z00_1189 = CDR(((obj_t) BgL_cellz00_1186));
											{	/* Llib/module.scm 179 */
												obj_t BgL_list1224z00_1190;

												{	/* Llib/module.scm 179 */
													obj_t BgL_arg1225z00_1191;

													{	/* Llib/module.scm 179 */
														obj_t BgL_arg1226z00_1192;

														{	/* Llib/module.scm 179 */
															obj_t BgL_arg1227z00_1193;

															{	/* Llib/module.scm 179 */
																obj_t BgL_arg1228z00_1194;

																{	/* Llib/module.scm 179 */
																	obj_t BgL_arg1229z00_1195;

																	{	/* Llib/module.scm 179 */
																		obj_t BgL_arg1230z00_1196;

																		{	/* Llib/module.scm 179 */
																			obj_t BgL_arg1231z00_1197;

																			{	/* Llib/module.scm 179 */
																				obj_t BgL_arg1232z00_1198;

																				{	/* Llib/module.scm 179 */
																					obj_t BgL_arg1233z00_1199;

																					BgL_arg1233z00_1199 =
																						MAKE_YOUNG_PAIR
																						(BGl_string1749z00zz__modulez00,
																						BNIL);
																					BgL_arg1232z00_1198 =
																						MAKE_YOUNG_PAIR(BgL_abasez00_15,
																						BgL_arg1233z00_1199);
																				}
																				BgL_arg1231z00_1197 =
																					MAKE_YOUNG_PAIR
																					(BGl_string1750z00zz__modulez00,
																					BgL_arg1232z00_1198);
																			}
																			BgL_arg1230z00_1196 =
																				MAKE_YOUNG_PAIR(BgL_filesz00_14,
																				BgL_arg1231z00_1197);
																		}
																		BgL_arg1229z00_1195 =
																			MAKE_YOUNG_PAIR
																			(BGl_string1751z00zz__modulez00,
																			BgL_arg1230z00_1196);
																	}
																	BgL_arg1228z00_1194 =
																		MAKE_YOUNG_PAIR(BgL_arg1223z00_1189,
																		BgL_arg1229z00_1195);
																}
																BgL_arg1227z00_1193 =
																	MAKE_YOUNG_PAIR
																	(BGl_string1752z00zz__modulez00,
																	BgL_arg1228z00_1194);
															}
															BgL_arg1226z00_1192 =
																MAKE_YOUNG_PAIR(BgL_modulez00_13,
																BgL_arg1227z00_1193);
														}
														BgL_arg1225z00_1191 =
															MAKE_YOUNG_PAIR(BGl_string1753z00zz__modulez00,
															BgL_arg1226z00_1192);
													}
													BgL_list1224z00_1190 =
														MAKE_YOUNG_PAIR(BGl_string1754z00zz__modulez00,
														BgL_arg1225z00_1191);
												}
												BGl_warningz00zz__errorz00(BgL_list1224z00_1190);
											}
										}
										return BGl_list1755z00zz__modulez00;
									}
							}
						else
							{	/* Llib/module.scm 177 */
								obj_t BgL_arg1236z00_1201;

								{	/* Llib/module.scm 177 */
									obj_t BgL_arg1238z00_1202;
									obj_t BgL_arg1239z00_1203;

									BgL_arg1238z00_1202 =
										MAKE_YOUNG_PAIR(BgL_modulez00_13, BgL_filesz00_14);
									BgL_arg1239z00_1203 = CDR(((obj_t) BgL_basez00_1185));
									BgL_arg1236z00_1201 =
										MAKE_YOUNG_PAIR(BgL_arg1238z00_1202, BgL_arg1239z00_1203);
								}
								{	/* Llib/module.scm 177 */
									obj_t BgL_tmpz00_2323;

									BgL_tmpz00_2323 = ((obj_t) BgL_basez00_1185);
									return SET_CDR(BgL_tmpz00_2323, BgL_arg1236z00_1201);
								}
							}
					}
				else
					{	/* Llib/module.scm 174 */
						obj_t BgL_arg1244z00_1205;

						{	/* Llib/module.scm 174 */
							obj_t BgL_arg1248z00_1206;

							{	/* Llib/module.scm 174 */
								obj_t BgL_arg1249z00_1207;

								BgL_arg1249z00_1207 =
									MAKE_YOUNG_PAIR(BgL_modulez00_13, BgL_filesz00_14);
								{	/* Llib/module.scm 174 */
									obj_t BgL_list1250z00_1208;

									BgL_list1250z00_1208 =
										MAKE_YOUNG_PAIR(BgL_arg1249z00_1207, BNIL);
									BgL_arg1248z00_1206 = BgL_list1250z00_1208;
								}
							}
							BgL_arg1244z00_1205 =
								MAKE_YOUNG_PAIR(BgL_abasez00_15, BgL_arg1248z00_1206);
						}
						return (BGl_afilezd2tablezd2zz__modulez00 =
							MAKE_YOUNG_PAIR(BgL_arg1244z00_1205,
								BGl_afilezd2tablezd2zz__modulez00), BUNSPEC);
					}
			}
		}

	}



/* module-add-access! */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2addzd2accessz12z12zz__modulez00(obj_t
		BgL_modulez00_16, obj_t BgL_filesz00_17, obj_t BgL_abasez00_18)
	{
		{	/* Llib/module.scm 189 */
			{
				obj_t BgL_fz00_1224;
				obj_t BgL_basez00_1225;

				{	/* Llib/module.scm 197 */
					obj_t BgL_top1837z00_2331;

					BgL_top1837z00_2331 = BGL_EXITD_TOP_AS_OBJ();
					BGL_MUTEX_LOCK(BGl_moduleszd2mutexzd2zz__modulez00);
					BGL_EXITD_PUSH_PROTECT(BgL_top1837z00_2331,
						BGl_moduleszd2mutexzd2zz__modulez00);
					BUNSPEC;
					{	/* Llib/module.scm 197 */
						obj_t BgL_tmp1836z00_2330;

						{	/* Llib/module.scm 199 */
							obj_t BgL_arg1252z00_1210;

							{	/* Llib/module.scm 199 */
								obj_t BgL_head1080z00_1213;

								BgL_head1080z00_1213 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								{
									obj_t BgL_l1078z00_1807;
									obj_t BgL_tail1081z00_1808;

									BgL_l1078z00_1807 = BgL_filesz00_17;
									BgL_tail1081z00_1808 = BgL_head1080z00_1213;
								BgL_zc3z04anonymousza31254ze3z87_1806:
									if (NULLP(BgL_l1078z00_1807))
										{	/* Llib/module.scm 199 */
											BgL_arg1252z00_1210 = CDR(BgL_head1080z00_1213);
										}
									else
										{	/* Llib/module.scm 199 */
											obj_t BgL_newtail1082z00_1815;

											{	/* Llib/module.scm 199 */
												obj_t BgL_arg1272z00_1816;

												{	/* Llib/module.scm 199 */
													obj_t BgL_fz00_1817;

													BgL_fz00_1817 = CAR(((obj_t) BgL_l1078z00_1807));
													BgL_fz00_1224 = BgL_fz00_1817;
													BgL_basez00_1225 = BgL_abasez00_18;
													{	/* Llib/module.scm 192 */
														bool_t BgL_test1839z00_2341;

														if ((STRING_LENGTH(((obj_t) BgL_fz00_1224)) > 0L))
															{	/* Llib/module.scm 192 */
																BgL_test1839z00_2341 =
																	(STRING_REF(
																		((obj_t) BgL_fz00_1224), 0L) ==
																	(unsigned char) (FILE_SEPARATOR));
															}
														else
															{	/* Llib/module.scm 192 */
																BgL_test1839z00_2341 = ((bool_t) 0);
															}
														if (BgL_test1839z00_2341)
															{	/* Llib/module.scm 192 */
																BgL_arg1272z00_1816 = BgL_fz00_1224;
															}
														else
															{	/* Llib/module.scm 192 */
																BgL_arg1272z00_1816 =
																	BGl_filezd2namezd2canonicaliza7eza7zz__osz00
																	(BGl_makezd2filezd2namez00zz__osz00
																	(BgL_basez00_1225, BgL_fz00_1224));
															}
													}
												}
												BgL_newtail1082z00_1815 =
													MAKE_YOUNG_PAIR(BgL_arg1272z00_1816, BNIL);
											}
											SET_CDR(BgL_tail1081z00_1808, BgL_newtail1082z00_1815);
											{	/* Llib/module.scm 199 */
												obj_t BgL_arg1268z00_1818;

												BgL_arg1268z00_1818 = CDR(((obj_t) BgL_l1078z00_1807));
												{
													obj_t BgL_tail1081z00_2357;
													obj_t BgL_l1078z00_2356;

													BgL_l1078z00_2356 = BgL_arg1268z00_1818;
													BgL_tail1081z00_2357 = BgL_newtail1082z00_1815;
													BgL_tail1081z00_1808 = BgL_tail1081z00_2357;
													BgL_l1078z00_1807 = BgL_l1078z00_2356;
													goto BgL_zc3z04anonymousza31254ze3z87_1806;
												}
											}
										}
								}
							}
							BgL_tmp1836z00_2330 =
								BGl_modulezd2addzd2accesszd2innerz12zc0zz__modulez00
								(BgL_modulez00_16, BgL_arg1252z00_1210, BgL_abasez00_18);
						}
						BGL_EXITD_POP_PROTECT(BgL_top1837z00_2331);
						BUNSPEC;
						BGL_MUTEX_UNLOCK(BGl_moduleszd2mutexzd2zz__modulez00);
						return BgL_tmp1836z00_2330;
					}
				}
			}
		}

	}



/* &module-add-access! */
	obj_t BGl_z62modulezd2addzd2accessz12z70zz__modulez00(obj_t BgL_envz00_2080,
		obj_t BgL_modulez00_2081, obj_t BgL_filesz00_2082, obj_t BgL_abasez00_2083)
	{
		{	/* Llib/module.scm 189 */
			{	/* Llib/module.scm 192 */
				obj_t BgL_auxz00_2375;
				obj_t BgL_auxz00_2368;
				obj_t BgL_auxz00_2361;

				if (STRINGP(BgL_abasez00_2083))
					{	/* Llib/module.scm 192 */
						BgL_auxz00_2375 = BgL_abasez00_2083;
					}
				else
					{
						obj_t BgL_auxz00_2378;

						BgL_auxz00_2378 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1741z00zz__modulez00,
							BINT(7320L), BGl_string1762z00zz__modulez00,
							BGl_string1764z00zz__modulez00, BgL_abasez00_2083);
						FAILURE(BgL_auxz00_2378, BFALSE, BFALSE);
					}
				if (PAIRP(BgL_filesz00_2082))
					{	/* Llib/module.scm 192 */
						BgL_auxz00_2368 = BgL_filesz00_2082;
					}
				else
					{
						obj_t BgL_auxz00_2371;

						BgL_auxz00_2371 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1741z00zz__modulez00,
							BINT(7320L), BGl_string1762z00zz__modulez00,
							BGl_string1763z00zz__modulez00, BgL_filesz00_2082);
						FAILURE(BgL_auxz00_2371, BFALSE, BFALSE);
					}
				if (SYMBOLP(BgL_modulez00_2081))
					{	/* Llib/module.scm 192 */
						BgL_auxz00_2361 = BgL_modulez00_2081;
					}
				else
					{
						obj_t BgL_auxz00_2364;

						BgL_auxz00_2364 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1741z00zz__modulez00,
							BINT(7320L), BGl_string1762z00zz__modulez00,
							BGl_string1746z00zz__modulez00, BgL_modulez00_2081);
						FAILURE(BgL_auxz00_2364, BFALSE, BFALSE);
					}
				return
					BGl_modulezd2addzd2accessz12z12zz__modulez00(BgL_auxz00_2361,
					BgL_auxz00_2368, BgL_auxz00_2375);
			}
		}

	}



/* module-read-access-file */
	obj_t BGl_modulezd2readzd2accesszd2filezd2zz__modulez00(obj_t BgL_portz00_19)
	{
		{	/* Llib/module.scm 205 */
			{	/* Llib/module.scm 206 */
				obj_t BgL_hook1087z00_1237;

				BgL_hook1087z00_1237 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
				{	/* Llib/module.scm 212 */
					obj_t BgL_g1089z00_1238;

					{	/* Llib/module.scm 212 */

						{	/* Llib/module.scm 212 */

							BgL_g1089z00_1238 =
								BGl_readz00zz__readerz00(((obj_t) BgL_portz00_19), BFALSE);
						}
					}
					{
						obj_t BgL_l1084z00_1240;
						obj_t BgL_h1085z00_1241;

						BgL_l1084z00_1240 = BgL_g1089z00_1238;
						BgL_h1085z00_1241 = BgL_hook1087z00_1237;
					BgL_zc3z04anonymousza31310ze3z87_1242:
						if (NULLP(BgL_l1084z00_1240))
							{	/* Llib/module.scm 212 */
								return CDR(BgL_hook1087z00_1237);
							}
						else
							{	/* Llib/module.scm 212 */
								bool_t BgL_test1845z00_2389;

								{	/* Llib/module.scm 207 */
									obj_t BgL_xz00_1263;

									BgL_xz00_1263 = CAR(((obj_t) BgL_l1084z00_1240));
									{	/* Llib/module.scm 207 */
										bool_t BgL_test1846z00_2392;

										if (PAIRP(BgL_xz00_1263))
											{	/* Llib/module.scm 207 */
												bool_t BgL_test1848z00_2395;

												{	/* Llib/module.scm 207 */
													obj_t BgL_tmpz00_2396;

													BgL_tmpz00_2396 = CAR(BgL_xz00_1263);
													BgL_test1848z00_2395 = SYMBOLP(BgL_tmpz00_2396);
												}
												if (BgL_test1848z00_2395)
													{	/* Llib/module.scm 207 */
														BgL_test1846z00_2392 =
															BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(CDR
															(BgL_xz00_1263));
													}
												else
													{	/* Llib/module.scm 207 */
														BgL_test1846z00_2392 = ((bool_t) 0);
													}
											}
										else
											{	/* Llib/module.scm 207 */
												BgL_test1846z00_2392 = ((bool_t) 0);
											}
										if (BgL_test1846z00_2392)
											{	/* Llib/module.scm 207 */
												BgL_test1845z00_2389 = ((bool_t) 1);
											}
										else
											{	/* Llib/module.scm 207 */
												{	/* Llib/module.scm 210 */
													obj_t BgL_list1328z00_1265;

													{	/* Llib/module.scm 210 */
														obj_t BgL_arg1329z00_1266;

														{	/* Llib/module.scm 210 */
															obj_t BgL_arg1331z00_1267;

															BgL_arg1331z00_1267 =
																MAKE_YOUNG_PAIR(BgL_xz00_1263, BNIL);
															BgL_arg1329z00_1266 =
																MAKE_YOUNG_PAIR(BGl_string1765z00zz__modulez00,
																BgL_arg1331z00_1267);
														}
														BgL_list1328z00_1265 =
															MAKE_YOUNG_PAIR(BGl_string1766z00zz__modulez00,
															BgL_arg1329z00_1266);
													}
													BGl_warningz00zz__errorz00(BgL_list1328z00_1265);
												}
												BgL_test1845z00_2389 = ((bool_t) 0);
											}
									}
								}
								if (BgL_test1845z00_2389)
									{	/* Llib/module.scm 212 */
										obj_t BgL_nh1086z00_1259;

										{	/* Llib/module.scm 212 */
											obj_t BgL_arg1326z00_1261;

											BgL_arg1326z00_1261 = CAR(((obj_t) BgL_l1084z00_1240));
											BgL_nh1086z00_1259 =
												MAKE_YOUNG_PAIR(BgL_arg1326z00_1261, BNIL);
										}
										SET_CDR(BgL_h1085z00_1241, BgL_nh1086z00_1259);
										{	/* Llib/module.scm 212 */
											obj_t BgL_arg1325z00_1260;

											BgL_arg1325z00_1260 = CDR(((obj_t) BgL_l1084z00_1240));
											{
												obj_t BgL_h1085z00_2412;
												obj_t BgL_l1084z00_2411;

												BgL_l1084z00_2411 = BgL_arg1325z00_1260;
												BgL_h1085z00_2412 = BgL_nh1086z00_1259;
												BgL_h1085z00_1241 = BgL_h1085z00_2412;
												BgL_l1084z00_1240 = BgL_l1084z00_2411;
												goto BgL_zc3z04anonymousza31310ze3z87_1242;
											}
										}
									}
								else
									{	/* Llib/module.scm 212 */
										obj_t BgL_arg1327z00_1262;

										BgL_arg1327z00_1262 = CDR(((obj_t) BgL_l1084z00_1240));
										{
											obj_t BgL_l1084z00_2415;

											BgL_l1084z00_2415 = BgL_arg1327z00_1262;
											BgL_l1084z00_1240 = BgL_l1084z00_2415;
											goto BgL_zc3z04anonymousza31310ze3z87_1242;
										}
									}
							}
					}
				}
			}
		}

	}



/* module-load-access-file */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2loadzd2accesszd2filezd2zz__modulez00(obj_t
		BgL_pathz00_20)
	{
		{	/* Llib/module.scm 223 */
			{
				obj_t BgL_filez00_1302;
				obj_t BgL_dirz00_1303;
				obj_t BgL_abasez00_1304;

				{	/* Llib/module.scm 246 */
					obj_t BgL_pathz00_1277;

					BgL_pathz00_1277 =
						BGl_filezd2namezd2canonicaliza7eza7zz__osz00(BgL_pathz00_20);
					{	/* Llib/module.scm 247 */
						obj_t BgL_top1850z00_2418;

						BgL_top1850z00_2418 = BGL_EXITD_TOP_AS_OBJ();
						BGL_MUTEX_LOCK(BGl_moduleszd2mutexzd2zz__modulez00);
						BGL_EXITD_PUSH_PROTECT(BgL_top1850z00_2418,
							BGl_moduleszd2mutexzd2zz__modulez00);
						BUNSPEC;
						{	/* Llib/module.scm 247 */
							obj_t BgL_tmp1849z00_2417;

							{	/* Llib/module.scm 248 */
								obj_t BgL__ortest_1039z00_1278;

								BgL__ortest_1039z00_1278 =
									BGl_hashtablezd2getzd2zz__hashz00
									(BGl_za2afileszd2tableza2zd2zz__modulez00, BgL_pathz00_1277);
								if (CBOOL(BgL__ortest_1039z00_1278))
									{	/* Llib/module.scm 248 */
										BgL_tmp1849z00_2417 = BgL__ortest_1039z00_1278;
									}
								else
									{	/* Llib/module.scm 248 */
										if (bgl_directoryp(BSTRING_TO_STRING(BgL_pathz00_1277)))
											{
												obj_t BgL_dz00_1281;

												BgL_dz00_1281 = BgL_pathz00_1277;
											BgL_zc3z04anonymousza31335ze3z87_1282:
												{	/* Llib/module.scm 252 */
													obj_t BgL_filez00_1283;

													BgL_filez00_1283 =
														BGl_makezd2filezd2namez00zz__osz00(BgL_dz00_1281,
														BGl_string1767z00zz__modulez00);
													if (fexists(BSTRING_TO_STRING(BgL_filez00_1283)))
														{	/* Llib/module.scm 253 */
															BgL_filez00_1302 = BgL_filez00_1283;
															BgL_dirz00_1303 = BgL_dz00_1281;
															BgL_abasez00_1304 = BgL_pathz00_1277;
														BgL_zc3z04anonymousza31349ze3z87_1305:
															{	/* Llib/module.scm 234 */
																obj_t BgL_zc3z04anonymousza31351ze3z87_2084;

																BgL_zc3z04anonymousza31351ze3z87_2084 =
																	MAKE_FX_PROCEDURE
																	(BGl_z62zc3z04anonymousza31351ze3ze5zz__modulez00,
																	(int) (1L), (int) (4L));
																PROCEDURE_SET
																	(BgL_zc3z04anonymousza31351ze3z87_2084,
																	(int) (0L), BgL_pathz00_20);
																PROCEDURE_SET
																	(BgL_zc3z04anonymousza31351ze3z87_2084,
																	(int) (1L), BgL_filez00_1302);
																PROCEDURE_SET
																	(BgL_zc3z04anonymousza31351ze3z87_2084,
																	(int) (2L), BgL_dirz00_1303);
																PROCEDURE_SET
																	(BgL_zc3z04anonymousza31351ze3z87_2084,
																	(int) (3L), BgL_abasez00_1304);
																BGl_callzd2withzd2inputzd2filezd2zz__r4_ports_6_10_1z00
																	(BgL_filez00_1302,
																	BgL_zc3z04anonymousza31351ze3z87_2084);
															}
															BgL_tmp1849z00_2417 = BgL_filez00_1302;
														}
													else
														{	/* Llib/module.scm 255 */
															obj_t BgL_parentz00_1285;

															BgL_parentz00_1285 =
																BGl_dirnamez00zz__osz00(BgL_dz00_1281);
															{	/* Llib/module.scm 256 */
																bool_t BgL_test1854z00_2445;

																{	/* Llib/module.scm 256 */
																	long BgL_l1z00_1868;

																	BgL_l1z00_1868 =
																		STRING_LENGTH(BgL_parentz00_1285);
																	if (
																		(BgL_l1z00_1868 ==
																			STRING_LENGTH(BgL_dz00_1281)))
																		{	/* Llib/module.scm 256 */
																			int BgL_arg1413z00_1871;

																			{	/* Llib/module.scm 256 */
																				char *BgL_auxz00_2452;
																				char *BgL_tmpz00_2450;

																				BgL_auxz00_2452 =
																					BSTRING_TO_STRING(BgL_dz00_1281);
																				BgL_tmpz00_2450 =
																					BSTRING_TO_STRING(BgL_parentz00_1285);
																				BgL_arg1413z00_1871 =
																					memcmp(BgL_tmpz00_2450,
																					BgL_auxz00_2452, BgL_l1z00_1868);
																			}
																			BgL_test1854z00_2445 =
																				((long) (BgL_arg1413z00_1871) == 0L);
																		}
																	else
																		{	/* Llib/module.scm 256 */
																			BgL_test1854z00_2445 = ((bool_t) 0);
																		}
																}
																if (BgL_test1854z00_2445)
																	{	/* Llib/module.scm 256 */
																		BgL_tmp1849z00_2417 = BFALSE;
																	}
																else
																	{
																		obj_t BgL_dz00_2457;

																		BgL_dz00_2457 = BgL_parentz00_1285;
																		BgL_dz00_1281 = BgL_dz00_2457;
																		goto BgL_zc3z04anonymousza31335ze3z87_1282;
																	}
															}
														}
												}
											}
										else
											{	/* Llib/module.scm 250 */
												if (fexists(BSTRING_TO_STRING(BgL_pathz00_1277)))
													{	/* Llib/module.scm 259 */
														obj_t BgL_dirz00_1289;

														BgL_dirz00_1289 =
															BGl_dirnamez00zz__osz00(BgL_pathz00_1277);
														{
															obj_t BgL_abasez00_2464;
															obj_t BgL_dirz00_2463;
															obj_t BgL_filez00_2462;

															BgL_filez00_2462 = BgL_pathz00_1277;
															BgL_dirz00_2463 = BgL_dirz00_1289;
															BgL_abasez00_2464 = BgL_dirz00_1289;
															BgL_abasez00_1304 = BgL_abasez00_2464;
															BgL_dirz00_1303 = BgL_dirz00_2463;
															BgL_filez00_1302 = BgL_filez00_2462;
															goto BgL_zc3z04anonymousza31349ze3z87_1305;
														}
													}
												else
													{	/* Llib/module.scm 258 */
														BgL_tmp1849z00_2417 = BFALSE;
													}
											}
									}
							}
							BGL_EXITD_POP_PROTECT(BgL_top1850z00_2418);
							BUNSPEC;
							BGL_MUTEX_UNLOCK(BGl_moduleszd2mutexzd2zz__modulez00);
							return BgL_tmp1849z00_2417;
						}
					}
				}
			}
		}

	}



/* &module-load-access-file */
	obj_t BGl_z62modulezd2loadzd2accesszd2filezb0zz__modulez00(obj_t
		BgL_envz00_2085, obj_t BgL_pathz00_2086)
	{
		{	/* Llib/module.scm 223 */
			{	/* Llib/module.scm 227 */
				obj_t BgL_auxz00_2467;

				if (STRINGP(BgL_pathz00_2086))
					{	/* Llib/module.scm 227 */
						BgL_auxz00_2467 = BgL_pathz00_2086;
					}
				else
					{
						obj_t BgL_auxz00_2470;

						BgL_auxz00_2470 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1741z00zz__modulez00,
							BINT(8648L), BGl_string1768z00zz__modulez00,
							BGl_string1764z00zz__modulez00, BgL_pathz00_2086);
						FAILURE(BgL_auxz00_2470, BFALSE, BFALSE);
					}
				return
					BGl_modulezd2loadzd2accesszd2filezd2zz__modulez00(BgL_auxz00_2467);
			}
		}

	}



/* &<@anonymous:1351> */
	obj_t BGl_z62zc3z04anonymousza31351ze3ze5zz__modulez00(obj_t BgL_envz00_2087,
		obj_t BgL_portz00_2092)
	{
		{	/* Llib/module.scm 233 */
			{	/* Llib/module.scm 234 */
				obj_t BgL_pathz00_2088;
				obj_t BgL_filez00_2089;
				obj_t BgL_dirz00_2090;
				obj_t BgL_abasez00_2091;

				BgL_pathz00_2088 = ((obj_t) PROCEDURE_REF(BgL_envz00_2087, (int) (0L)));
				BgL_filez00_2089 = ((obj_t) PROCEDURE_REF(BgL_envz00_2087, (int) (1L)));
				BgL_dirz00_2090 = ((obj_t) PROCEDURE_REF(BgL_envz00_2087, (int) (2L)));
				BgL_abasez00_2091 =
					((obj_t) PROCEDURE_REF(BgL_envz00_2087, (int) (3L)));
				{	/* Llib/module.scm 234 */
					bool_t BgL_tmpz00_2487;

					{
						obj_t BgL_fz00_2122;
						obj_t BgL_abasez00_2123;

						BGl_hashtablezd2putz12zc0zz__hashz00
							(BGl_za2afileszd2tableza2zd2zz__modulez00, BgL_pathz00_2088,
							BgL_filez00_2089);
						{	/* Llib/module.scm 235 */
							obj_t BgL_g1095z00_2126;

							BgL_g1095z00_2126 =
								BGl_modulezd2readzd2accesszd2filezd2zz__modulez00
								(BgL_portz00_2092);
							{
								obj_t BgL_l1093z00_2128;

								BgL_l1093z00_2128 = BgL_g1095z00_2126;
							BgL_zc3z04anonymousza31352ze3z87_2127:
								if (PAIRP(BgL_l1093z00_2128))
									{	/* Llib/module.scm 243 */
										{	/* Llib/module.scm 236 */
											obj_t BgL_accessz00_2129;

											BgL_accessz00_2129 = CAR(BgL_l1093z00_2128);
											{	/* Llib/module.scm 236 */
												obj_t BgL_infoz00_2130;

												{	/* Llib/module.scm 236 */
													bool_t BgL_test1859z00_2493;

													{	/* Llib/module.scm 236 */
														long BgL_l1z00_2131;

														BgL_l1z00_2131 = STRING_LENGTH(BgL_dirz00_2090);
														if ((BgL_l1z00_2131 == 1L))
															{	/* Llib/module.scm 236 */
																int BgL_arg1413z00_2132;

																{	/* Llib/module.scm 236 */
																	char *BgL_auxz00_2499;
																	char *BgL_tmpz00_2497;

																	BgL_auxz00_2499 =
																		BSTRING_TO_STRING
																		(BGl_string1744z00zz__modulez00);
																	BgL_tmpz00_2497 =
																		BSTRING_TO_STRING(BgL_dirz00_2090);
																	BgL_arg1413z00_2132 =
																		memcmp(BgL_tmpz00_2497, BgL_auxz00_2499,
																		BgL_l1z00_2131);
																}
																BgL_test1859z00_2493 =
																	((long) (BgL_arg1413z00_2132) == 0L);
															}
														else
															{	/* Llib/module.scm 236 */
																BgL_test1859z00_2493 = ((bool_t) 0);
															}
													}
													if (BgL_test1859z00_2493)
														{	/* Llib/module.scm 236 */
															BgL_infoz00_2130 =
																CDR(((obj_t) BgL_accessz00_2129));
														}
													else
														{	/* Llib/module.scm 238 */
															obj_t BgL_l01092z00_2133;

															BgL_l01092z00_2133 =
																CDR(((obj_t) BgL_accessz00_2129));
															{
																obj_t BgL_l1091z00_2135;

																BgL_l1091z00_2135 = BgL_l01092z00_2133;
															BgL_zc3z04anonymousza31356ze3z87_2134:
																if (NULLP(BgL_l1091z00_2135))
																	{	/* Llib/module.scm 240 */
																		BgL_infoz00_2130 = BgL_l01092z00_2133;
																	}
																else
																	{	/* Llib/module.scm 240 */
																		{	/* Llib/module.scm 239 */
																			obj_t BgL_arg1358z00_2136;

																			{	/* Llib/module.scm 239 */
																				obj_t BgL_fz00_2137;

																				BgL_fz00_2137 =
																					CAR(((obj_t) BgL_l1091z00_2135));
																				BgL_fz00_2122 = BgL_fz00_2137;
																				BgL_abasez00_2123 = BgL_dirz00_2090;
																				if (STRINGP(BgL_fz00_2122))
																					{	/* Llib/module.scm 228 */
																						bool_t BgL_test1863z00_2514;

																						{	/* Llib/module.scm 228 */
																							bool_t BgL_test1864z00_2515;

																							{	/* Llib/module.scm 228 */
																								long BgL_l1z00_2124;

																								BgL_l1z00_2124 =
																									STRING_LENGTH(BgL_fz00_2122);
																								if ((BgL_l1z00_2124 == 0L))
																									{	/* Llib/module.scm 228 */
																										int BgL_arg1413z00_2125;

																										{	/* Llib/module.scm 228 */
																											char *BgL_auxz00_2521;
																											char *BgL_tmpz00_2519;

																											BgL_auxz00_2521 =
																												BSTRING_TO_STRING
																												(BGl_string1769z00zz__modulez00);
																											BgL_tmpz00_2519 =
																												BSTRING_TO_STRING
																												(BgL_fz00_2122);
																											BgL_arg1413z00_2125 =
																												memcmp(BgL_tmpz00_2519,
																												BgL_auxz00_2521,
																												BgL_l1z00_2124);
																										}
																										BgL_test1864z00_2515 =
																											(
																											(long)
																											(BgL_arg1413z00_2125) ==
																											0L);
																									}
																								else
																									{	/* Llib/module.scm 228 */
																										BgL_test1864z00_2515 =
																											((bool_t) 0);
																									}
																							}
																							if (BgL_test1864z00_2515)
																								{	/* Llib/module.scm 228 */
																									BgL_test1863z00_2514 =
																										((bool_t) 1);
																								}
																							else
																								{	/* Llib/module.scm 228 */
																									BgL_test1863z00_2514 =
																										(STRING_REF(BgL_fz00_2122,
																											0L) ==
																										(unsigned
																											char) (FILE_SEPARATOR));
																						}}
																						if (BgL_test1863z00_2514)
																							{	/* Llib/module.scm 228 */
																								BgL_arg1358z00_2136 =
																									BgL_fz00_2122;
																							}
																						else
																							{	/* Llib/module.scm 228 */
																								BgL_arg1358z00_2136 =
																									BGl_filezd2namezd2canonicaliza7eza7zz__osz00
																									(BGl_makezd2filezd2namez00zz__osz00
																									(BgL_abasez00_2123,
																										BgL_fz00_2122));
																							}
																					}
																				else
																					{	/* Llib/module.scm 227 */
																						BgL_arg1358z00_2136 = BgL_fz00_2122;
																					}
																			}
																			{	/* Llib/module.scm 240 */
																				obj_t BgL_tmpz00_2531;

																				BgL_tmpz00_2531 =
																					((obj_t) BgL_l1091z00_2135);
																				SET_CAR(BgL_tmpz00_2531,
																					BgL_arg1358z00_2136);
																			}
																		}
																		{	/* Llib/module.scm 240 */
																			obj_t BgL_arg1359z00_2138;

																			BgL_arg1359z00_2138 =
																				CDR(((obj_t) BgL_l1091z00_2135));
																			{
																				obj_t BgL_l1091z00_2536;

																				BgL_l1091z00_2536 = BgL_arg1359z00_2138;
																				BgL_l1091z00_2135 = BgL_l1091z00_2536;
																				goto
																					BgL_zc3z04anonymousza31356ze3z87_2134;
																			}
																		}
																	}
															}
														}
												}
												{	/* Llib/module.scm 242 */
													obj_t BgL_arg1354z00_2139;

													BgL_arg1354z00_2139 =
														CAR(((obj_t) BgL_accessz00_2129));
													BGl_modulezd2addzd2accesszd2innerz12zc0zz__modulez00
														(BgL_arg1354z00_2139, BgL_infoz00_2130,
														BgL_abasez00_2091);
												}
											}
										}
										{
											obj_t BgL_l1093z00_2540;

											BgL_l1093z00_2540 = CDR(BgL_l1093z00_2128);
											BgL_l1093z00_2128 = BgL_l1093z00_2540;
											goto BgL_zc3z04anonymousza31352ze3z87_2127;
										}
									}
								else
									{	/* Llib/module.scm 243 */
										BgL_tmpz00_2487 = ((bool_t) 1);
									}
							}
						}
					}
					return BBOOL(BgL_tmpz00_2487);
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__modulez00(void)
	{
		{	/* Llib/module.scm 17 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__modulez00(void)
	{
		{	/* Llib/module.scm 17 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__modulez00(void)
	{
		{	/* Llib/module.scm 17 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__modulez00(void)
	{
		{	/* Llib/module.scm 17 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1770z00zz__modulez00));
			BGl_modulezd2initializa7ationz75zz__configurez00(35034923L,
				BSTRING_TO_STRING(BGl_string1770z00zz__modulez00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1770z00zz__modulez00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string1770z00zz__modulez00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string1770z00zz__modulez00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string1770z00zz__modulez00));
			BGl_modulezd2initializa7ationz75zz__readerz00(220648206L,
				BSTRING_TO_STRING(BGl_string1770z00zz__modulez00));
			return
				BGl_modulezd2initializa7ationz75zz__hashz00(482391669L,
				BSTRING_TO_STRING(BGl_string1770z00zz__modulez00));
		}

	}

#ifdef __cplusplus
}
#endif
