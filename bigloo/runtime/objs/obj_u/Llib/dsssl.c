/*===========================================================================*/
/*   (Llib/dsssl.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Llib/dsssl.scm -indent -o objs/obj_u/Llib/dsssl.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___DSSSL_TYPE_DEFINITIONS
#define BGL___DSSSL_TYPE_DEFINITIONS
#endif													// BGL___DSSSL_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern bool_t BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__dssslz00 = BUNSPEC;
	static obj_t BGl_symbol2100z00zz__dssslz00 = BUNSPEC;
	static obj_t BGl_z62dssslzd2getzd2keyzd2argzb0zz__dssslz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_symbol2102z00zz__dssslz00 = BUNSPEC;
	static obj_t BGl_symbol2105z00zz__dssslz00 = BUNSPEC;
	static obj_t BGl_symbol2107z00zz__dssslz00 = BUNSPEC;
	static obj_t BGl_symbol2109z00zz__dssslz00 = BUNSPEC;
	static obj_t
		BGl_z62dssslzd2formalszd2ze3schemezd2typedzd2formalsz81zz__dssslz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62restzd2statezb0zz__dssslz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__dssslz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_symbol2112z00zz__dssslz00 = BUNSPEC;
	static obj_t BGl_symbol2114z00zz__dssslz00 = BUNSPEC;
	static obj_t BGl_symbol2116z00zz__dssslz00 = BUNSPEC;
	static obj_t BGl_symbol2118z00zz__dssslz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_dssslzd2checkzd2keyzd2argsz12zc0zz__dssslz00(obj_t, obj_t);
	static obj_t BGl_z62dssslzd2namedzd2constantzf3z91zz__dssslz00(obj_t, obj_t);
	static obj_t BGl_z62optionalzd2statezb0zz__dssslz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2122z00zz__dssslz00 = BUNSPEC;
	static obj_t BGl_symbol2124z00zz__dssslz00 = BUNSPEC;
	static obj_t BGl_symbol2126z00zz__dssslz00 = BUNSPEC;
	static obj_t BGl_symbol2128z00zz__dssslz00 = BUNSPEC;
	extern obj_t BGl_symbolzd2ze3keywordz31zz__r4_symbols_6_4z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_dssslzd2formalszd2ze3schemezd2formalsz31zz__dssslz00(obj_t, obj_t);
	static obj_t BGl_symbol2130z00zz__dssslz00 = BUNSPEC;
	static obj_t BGl_symbol2132z00zz__dssslz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2dssslzd2functionzd2preludezd2zz__dssslz00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_dssslzd2getzd2keyzd2argzd2zz__dssslz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__dssslz00(void);
	static obj_t BGl_genericzd2initzd2zz__dssslz00(void);
	extern obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__dssslz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__dssslz00(void);
	static obj_t BGl_objectzd2initzd2zz__dssslz00(void);
	static obj_t BGl_z62dssslzd2getzd2keyzd2restzd2argz62zz__dssslz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62dssslzd2checkzd2keyzd2argsz12za2zz__dssslz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t bgl_reverse_bang(obj_t);
	static obj_t BGl_symbol2085z00zz__dssslz00 = BUNSPEC;
	static obj_t BGl_symbol2087z00zz__dssslz00 = BUNSPEC;
	static obj_t
		BGl_z62dssslzd2formalszd2ze3schemezd2formalsz53zz__dssslz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62nozd2restzd2keyzd2statezb0zz__dssslz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol2094z00zz__dssslz00 = BUNSPEC;
	static obj_t BGl_symbol2096z00zz__dssslz00 = BUNSPEC;
	static obj_t BGl_list2146z00zz__dssslz00 = BUNSPEC;
	static obj_t BGl_symbol2098z00zz__dssslz00 = BUNSPEC;
	extern obj_t bstring_to_symbol(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_dssslzd2formalszd2ze3schemezd2typedzd2formalsze3zz__dssslz00(obj_t,
		obj_t, bool_t);
	extern obj_t c_substring(obj_t, long, long);
	extern obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_methodzd2initzd2zz__dssslz00(void);
	static obj_t BGl_list2083z00zz__dssslz00 = BUNSPEC;
	static obj_t BGl_optionalzd2argsze70z35zz__dssslz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_loopze70ze7zz__dssslz00(bool_t, obj_t, obj_t, obj_t, bool_t);
	static obj_t BGl_loopze71ze7zz__dssslz00(obj_t, obj_t);
	static obj_t BGl_idzd2sanszd2typez00zz__dssslz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_dssslzd2getzd2keyzd2restzd2argz00zz__dssslz00(obj_t, obj_t);
	static obj_t BGl_z62makezd2dssslzd2functionzd2preludezb0zz__dssslz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62keyzd2statezb0zz__dssslz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, bool_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00(obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string2120z00zz__dssslz00,
		BgL_bgl_string2120za700za7za7_2154za7, "Illegal DSSSL formal list (#!rest)",
		34);
	      DEFINE_STRING(BGl_string2121z00zz__dssslz00,
		BgL_bgl_string2121za700za7za7_2155za7, "Illegal DSSSL formal list (#!key)",
		33);
	      DEFINE_STRING(BGl_string2123z00zz__dssslz00,
		BgL_bgl_string2123za700za7za7_2156za7, "tmp", 3);
	      DEFINE_STRING(BGl_string2125z00zz__dssslz00,
		BgL_bgl_string2125za700za7za7_2157za7, "car", 3);
	      DEFINE_STRING(BGl_string2127z00zz__dssslz00,
		BgL_bgl_string2127za700za7za7_2158za7, "memq", 4);
	      DEFINE_STRING(BGl_string2129z00zz__dssslz00,
		BgL_bgl_string2129za700za7za7_2159za7, "cdr", 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_dssslzd2getzd2keyzd2argzd2envz00zz__dssslz00,
		BgL_bgl_za762dssslza7d2getza7d2160za7,
		BGl_z62dssslzd2getzd2keyzd2argzb0zz__dssslz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_dssslzd2formalszd2ze3schemezd2typedzd2formalszd2envz31zz__dssslz00,
		BgL_bgl_za762dssslza7d2forma2161z00,
		BGl_z62dssslzd2formalszd2ze3schemezd2typedzd2formalsz81zz__dssslz00, 0L,
		BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2131z00zz__dssslz00,
		BgL_bgl_string2131za700za7za7_2162za7, "set!", 4);
	      DEFINE_STRING(BGl_string2133z00zz__dssslz00,
		BgL_bgl_string2133za700za7za7_2163za7, "begin", 5);
	      DEFINE_STRING(BGl_string2134z00zz__dssslz00,
		BgL_bgl_string2134za700za7za7_2164za7,
		"Illegal DSSSL formal list (#!optional)", 38);
	      DEFINE_STRING(BGl_string2135z00zz__dssslz00,
		BgL_bgl_string2135za700za7za7_2165za7, "Illegal #!keys parameters", 25);
	      DEFINE_STRING(BGl_string2136z00zz__dssslz00,
		BgL_bgl_string2136za700za7za7_2166za7, "dsssl formal parsing", 20);
	      DEFINE_STRING(BGl_string2137z00zz__dssslz00,
		BgL_bgl_string2137za700za7za7_2167za7, "Unexpected #!keys parameters", 28);
	      DEFINE_STRING(BGl_string2138z00zz__dssslz00,
		BgL_bgl_string2138za700za7za7_2168za7, "Keyword argument misses value", 29);
	      DEFINE_STRING(BGl_string2139z00zz__dssslz00,
		BgL_bgl_string2139za700za7za7_2169za7, "Illegal keyword actual value", 28);
	      DEFINE_STRING(BGl_string2140z00zz__dssslz00,
		BgL_bgl_string2140za700za7za7_2170za7, "Illegal DSSSL arguments", 23);
	      DEFINE_STRING(BGl_string2141z00zz__dssslz00,
		BgL_bgl_string2141za700za7za7_2171za7, "&dsssl-get-key-arg", 18);
	      DEFINE_STRING(BGl_string2142z00zz__dssslz00,
		BgL_bgl_string2142za700za7za7_2172za7, "keyword", 7);
	      DEFINE_STRING(BGl_string2143z00zz__dssslz00,
		BgL_bgl_string2143za700za7za7_2173za7, "&dsssl-get-key-rest-arg", 23);
	      DEFINE_STRING(BGl_string2144z00zz__dssslz00,
		BgL_bgl_string2144za700za7za7_2174za7, "pair-nil", 8);
	      DEFINE_STRING(BGl_string2145z00zz__dssslz00,
		BgL_bgl_string2145za700za7za7_2175za7, "&dsssl-formals->scheme-formals",
		30);
	      DEFINE_STRING(BGl_string2147z00zz__dssslz00,
		BgL_bgl_string2147za700za7za7_2176za7, "Illegal formal parameter", 24);
	      DEFINE_STRING(BGl_string2148z00zz__dssslz00,
		BgL_bgl_string2148za700za7za7_2177za7, "symbol or named constant expected",
		33);
	      DEFINE_STRING(BGl_string2149z00zz__dssslz00,
		BgL_bgl_string2149za700za7za7_2178za7, "symbol expected", 15);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_dssslzd2namedzd2constantzf3zd2envz21zz__dssslz00,
		BgL_bgl_za762dssslza7d2named2179z00,
		BGl_z62dssslzd2namedzd2constantzf3z91zz__dssslz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2150z00zz__dssslz00,
		BgL_bgl_string2150za700za7za7_2180za7,
		"Can't use both DSSSL named constant", 35);
	      DEFINE_STRING(BGl_string2151z00zz__dssslz00,
		BgL_bgl_string2151za700za7za7_2181za7, "and `.' notation", 16);
	      DEFINE_STRING(BGl_string2152z00zz__dssslz00,
		BgL_bgl_string2152za700za7za7_2182za7,
		"&dsssl-formals->scheme-typed-formals", 36);
	      DEFINE_STRING(BGl_string2153z00zz__dssslz00,
		BgL_bgl_string2153za700za7za7_2183za7, "__dsssl", 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_dssslzd2getzd2keyzd2restzd2argzd2envzd2zz__dssslz00,
		BgL_bgl_za762dssslza7d2getza7d2184za7,
		BGl_z62dssslzd2getzd2keyzd2restzd2argz62zz__dssslz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2084z00zz__dssslz00,
		BgL_bgl_string2084za700za7za7_2185za7, "Illegal formal list.1", 21);
	      DEFINE_STRING(BGl_string2086z00zz__dssslz00,
		BgL_bgl_string2086za700za7za7_2186za7, "dsssl", 5);
	      DEFINE_STRING(BGl_string2088z00zz__dssslz00,
		BgL_bgl_string2088za700za7za7_2187za7, "let", 3);
	      DEFINE_STRING(BGl_string2089z00zz__dssslz00,
		BgL_bgl_string2089za700za7za7_2188za7, "Illegal formal list.3", 21);
	      DEFINE_STRING(BGl_string2090z00zz__dssslz00,
		BgL_bgl_string2090za700za7za7_2189za7, "Illegal formal list.2", 21);
	      DEFINE_STRING(BGl_string2091z00zz__dssslz00,
		BgL_bgl_string2091za700za7za7_2190za7, "/tmp/bigloo/runtime/Llib/dsssl.scm",
		34);
	      DEFINE_STRING(BGl_string2092z00zz__dssslz00,
		BgL_bgl_string2092za700za7za7_2191za7, "&make-dsssl-function-prelude", 28);
	      DEFINE_STRING(BGl_string2093z00zz__dssslz00,
		BgL_bgl_string2093za700za7za7_2192za7, "procedure", 9);
	      DEFINE_STRING(BGl_string2095z00zz__dssslz00,
		BgL_bgl_string2095za700za7za7_2193za7, "quote", 5);
	      DEFINE_STRING(BGl_string2097z00zz__dssslz00,
		BgL_bgl_string2097za700za7za7_2194za7, "dsssl-get-key-rest-arg", 22);
	      DEFINE_STRING(BGl_string2099z00zz__dssslz00,
		BgL_bgl_string2099za700za7za7_2195za7, "dsssl-get-key-arg", 17);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_dssslzd2checkzd2keyzd2argsz12zd2envz12zz__dssslz00,
		BgL_bgl_za762dssslza7d2check2196z00,
		BGl_z62dssslzd2checkzd2keyzd2argsz12za2zz__dssslz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2dssslzd2functionzd2preludezd2envz00zz__dssslz00,
		BgL_bgl_za762makeza7d2dssslza72197za7,
		BGl_z62makezd2dssslzd2functionzd2preludezb0zz__dssslz00, 0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string2101z00zz__dssslz00,
		BgL_bgl_string2101za700za7za7_2198za7, "null?", 5);
	      DEFINE_STRING(BGl_string2103z00zz__dssslz00,
		BgL_bgl_string2103za700za7za7_2199za7, "v", 1);
	      DEFINE_STRING(BGl_string2104z00zz__dssslz00,
		BgL_bgl_string2104za700za7za7_2200za7, "~a ", 3);
	      DEFINE_STRING(BGl_string2106z00zz__dssslz00,
		BgL_bgl_string2106za700za7za7_2201za7, "format", 6);
	      DEFINE_STRING(BGl_string2108z00zz__dssslz00,
		BgL_bgl_string2108za700za7za7_2202za7, "lambda", 6);
	      DEFINE_STRING(BGl_string2110z00zz__dssslz00,
		BgL_bgl_string2110za700za7za7_2203za7, "map", 3);
	      DEFINE_STRING(BGl_string2111z00zz__dssslz00,
		BgL_bgl_string2111za700za7za7_2204za7, "Illegal extra key arguments: ", 29);
	      DEFINE_STRING(BGl_string2113z00zz__dssslz00,
		BgL_bgl_string2113za700za7za7_2205za7, "string-append", 13);
	      DEFINE_STRING(BGl_string2115z00zz__dssslz00,
		BgL_bgl_string2115za700za7za7_2206za7, "apply", 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_dssslzd2formalszd2ze3schemezd2formalszd2envze3zz__dssslz00,
		BgL_bgl_za762dssslza7d2forma2207z00,
		BGl_z62dssslzd2formalszd2ze3schemezd2formalsz53zz__dssslz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2117z00zz__dssslz00,
		BgL_bgl_string2117za700za7za7_2208za7, "error", 5);
	      DEFINE_STRING(BGl_string2119z00zz__dssslz00,
		BgL_bgl_string2119za700za7za7_2209za7, "if", 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__dssslz00));
		     ADD_ROOT((void *) (&BGl_symbol2100z00zz__dssslz00));
		     ADD_ROOT((void *) (&BGl_symbol2102z00zz__dssslz00));
		     ADD_ROOT((void *) (&BGl_symbol2105z00zz__dssslz00));
		     ADD_ROOT((void *) (&BGl_symbol2107z00zz__dssslz00));
		     ADD_ROOT((void *) (&BGl_symbol2109z00zz__dssslz00));
		     ADD_ROOT((void *) (&BGl_symbol2112z00zz__dssslz00));
		     ADD_ROOT((void *) (&BGl_symbol2114z00zz__dssslz00));
		     ADD_ROOT((void *) (&BGl_symbol2116z00zz__dssslz00));
		     ADD_ROOT((void *) (&BGl_symbol2118z00zz__dssslz00));
		     ADD_ROOT((void *) (&BGl_symbol2122z00zz__dssslz00));
		     ADD_ROOT((void *) (&BGl_symbol2124z00zz__dssslz00));
		     ADD_ROOT((void *) (&BGl_symbol2126z00zz__dssslz00));
		     ADD_ROOT((void *) (&BGl_symbol2128z00zz__dssslz00));
		     ADD_ROOT((void *) (&BGl_symbol2130z00zz__dssslz00));
		     ADD_ROOT((void *) (&BGl_symbol2132z00zz__dssslz00));
		     ADD_ROOT((void *) (&BGl_symbol2085z00zz__dssslz00));
		     ADD_ROOT((void *) (&BGl_symbol2087z00zz__dssslz00));
		     ADD_ROOT((void *) (&BGl_symbol2094z00zz__dssslz00));
		     ADD_ROOT((void *) (&BGl_symbol2096z00zz__dssslz00));
		     ADD_ROOT((void *) (&BGl_list2146z00zz__dssslz00));
		     ADD_ROOT((void *) (&BGl_symbol2098z00zz__dssslz00));
		     ADD_ROOT((void *) (&BGl_list2083z00zz__dssslz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__dssslz00(long
		BgL_checksumz00_2655, char *BgL_fromz00_2656)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__dssslz00))
				{
					BGl_requirezd2initializa7ationz75zz__dssslz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__dssslz00();
					BGl_cnstzd2initzd2zz__dssslz00();
					BGl_importedzd2moduleszd2initz00zz__dssslz00();
					return BGl_methodzd2initzd2zz__dssslz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__dssslz00(void)
	{
		{	/* Llib/dsssl.scm 14 */
			BGl_list2083z00zz__dssslz00 =
				MAKE_YOUNG_PAIR((BREST),
				MAKE_YOUNG_PAIR((BOPTIONAL), MAKE_YOUNG_PAIR((BKEY), BNIL)));
			BGl_symbol2085z00zz__dssslz00 =
				bstring_to_symbol(BGl_string2086z00zz__dssslz00);
			BGl_symbol2087z00zz__dssslz00 =
				bstring_to_symbol(BGl_string2088z00zz__dssslz00);
			BGl_symbol2094z00zz__dssslz00 =
				bstring_to_symbol(BGl_string2095z00zz__dssslz00);
			BGl_symbol2096z00zz__dssslz00 =
				bstring_to_symbol(BGl_string2097z00zz__dssslz00);
			BGl_symbol2098z00zz__dssslz00 =
				bstring_to_symbol(BGl_string2099z00zz__dssslz00);
			BGl_symbol2100z00zz__dssslz00 =
				bstring_to_symbol(BGl_string2101z00zz__dssslz00);
			BGl_symbol2102z00zz__dssslz00 =
				bstring_to_symbol(BGl_string2103z00zz__dssslz00);
			BGl_symbol2105z00zz__dssslz00 =
				bstring_to_symbol(BGl_string2106z00zz__dssslz00);
			BGl_symbol2107z00zz__dssslz00 =
				bstring_to_symbol(BGl_string2108z00zz__dssslz00);
			BGl_symbol2109z00zz__dssslz00 =
				bstring_to_symbol(BGl_string2110z00zz__dssslz00);
			BGl_symbol2112z00zz__dssslz00 =
				bstring_to_symbol(BGl_string2113z00zz__dssslz00);
			BGl_symbol2114z00zz__dssslz00 =
				bstring_to_symbol(BGl_string2115z00zz__dssslz00);
			BGl_symbol2116z00zz__dssslz00 =
				bstring_to_symbol(BGl_string2117z00zz__dssslz00);
			BGl_symbol2118z00zz__dssslz00 =
				bstring_to_symbol(BGl_string2119z00zz__dssslz00);
			BGl_symbol2122z00zz__dssslz00 =
				bstring_to_symbol(BGl_string2123z00zz__dssslz00);
			BGl_symbol2124z00zz__dssslz00 =
				bstring_to_symbol(BGl_string2125z00zz__dssslz00);
			BGl_symbol2126z00zz__dssslz00 =
				bstring_to_symbol(BGl_string2127z00zz__dssslz00);
			BGl_symbol2128z00zz__dssslz00 =
				bstring_to_symbol(BGl_string2129z00zz__dssslz00);
			BGl_symbol2130z00zz__dssslz00 =
				bstring_to_symbol(BGl_string2131z00zz__dssslz00);
			BGl_symbol2132z00zz__dssslz00 =
				bstring_to_symbol(BGl_string2133z00zz__dssslz00);
			return (BGl_list2146z00zz__dssslz00 =
				MAKE_YOUNG_PAIR((BOPTIONAL),
					MAKE_YOUNG_PAIR((BREST), MAKE_YOUNG_PAIR((BKEY), BNIL))), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__dssslz00(void)
	{
		{	/* Llib/dsssl.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* dsssl-named-constant? */
	BGL_EXPORTED_DEF bool_t BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00(obj_t
		BgL_objz00_3)
	{
		{	/* Llib/dsssl.scm 59 */
			if (CNSTP(BgL_objz00_3))
				{	/* Llib/dsssl.scm 60 */
					return
						CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_objz00_3,
							BGl_list2083z00zz__dssslz00));
				}
			else
				{	/* Llib/dsssl.scm 60 */
					return ((bool_t) 0);
				}
		}

	}



/* &dsssl-named-constant? */
	obj_t BGl_z62dssslzd2namedzd2constantzf3z91zz__dssslz00(obj_t BgL_envz00_2502,
		obj_t BgL_objz00_2503)
	{
		{	/* Llib/dsssl.scm 59 */
			return
				BBOOL(BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00(BgL_objz00_2503));
		}

	}



/* make-dsssl-function-prelude */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2dssslzd2functionzd2preludezd2zz__dssslz00(obj_t BgL_wherez00_4,
		obj_t BgL_formalsz00_5, obj_t BgL_bodyz00_6, obj_t BgL_errz00_7)
	{
		{	/* Llib/dsssl.scm 71 */
			{	/* Llib/dsssl.scm 75 */
				obj_t BgL_nozd2restzd2keyzd2statezd2_2505;
				obj_t BgL_restzd2statezd2_2506;
				obj_t BgL_optionalzd2statezd2_2507;

				{
					int BgL_tmpz00_2697;

					BgL_tmpz00_2697 = (int) (4L);
					BgL_nozd2restzd2keyzd2statezd2_2505 =
						MAKE_L_PROCEDURE(BGl_z62nozd2restzd2keyzd2statezb0zz__dssslz00,
						BgL_tmpz00_2697);
				}
				{
					int BgL_tmpz00_2700;

					BgL_tmpz00_2700 = (int) (4L);
					BgL_restzd2statezd2_2506 =
						MAKE_L_PROCEDURE(BGl_z62restzd2statezb0zz__dssslz00,
						BgL_tmpz00_2700);
				}
				{
					int BgL_tmpz00_2703;

					BgL_tmpz00_2703 = (int) (6L);
					BgL_optionalzd2statezd2_2507 =
						MAKE_L_PROCEDURE(BGl_z62optionalzd2statezb0zz__dssslz00,
						BgL_tmpz00_2703);
				}
				PROCEDURE_L_SET(BgL_nozd2restzd2keyzd2statezd2_2505,
					(int) (0L), BgL_bodyz00_6);
				PROCEDURE_L_SET(BgL_nozd2restzd2keyzd2statezd2_2505,
					(int) (1L), BgL_errz00_7);
				PROCEDURE_L_SET(BgL_nozd2restzd2keyzd2statezd2_2505,
					(int) (2L), BgL_wherez00_4);
				PROCEDURE_L_SET(BgL_nozd2restzd2keyzd2statezd2_2505,
					(int) (3L), BgL_formalsz00_5);
				PROCEDURE_L_SET(BgL_restzd2statezd2_2506, (int) (0L), BgL_errz00_7);
				PROCEDURE_L_SET(BgL_restzd2statezd2_2506, (int) (1L), BgL_wherez00_4);
				PROCEDURE_L_SET(BgL_restzd2statezd2_2506, (int) (2L), BgL_formalsz00_5);
				PROCEDURE_L_SET(BgL_restzd2statezd2_2506, (int) (3L), BgL_bodyz00_6);
				PROCEDURE_L_SET(BgL_optionalzd2statezd2_2507, (int) (0L), BgL_errz00_7);
				PROCEDURE_L_SET(BgL_optionalzd2statezd2_2507,
					(int) (1L), BgL_wherez00_4);
				PROCEDURE_L_SET(BgL_optionalzd2statezd2_2507,
					(int) (2L), BgL_formalsz00_5);
				PROCEDURE_L_SET(BgL_optionalzd2statezd2_2507,
					(int) (3L), BgL_nozd2restzd2keyzd2statezd2_2505);
				PROCEDURE_L_SET(BgL_optionalzd2statezd2_2507,
					(int) (4L), BgL_restzd2statezd2_2506);
				PROCEDURE_L_SET(BgL_optionalzd2statezd2_2507,
					(int) (5L), BgL_bodyz00_6);
				{
					obj_t BgL_argsz00_1136;
					obj_t BgL_nextzd2statezd2_1137;
					obj_t BgL_argsz00_1107;

					BgL_argsz00_1107 = BgL_formalsz00_5;
				BgL_zc3z04anonymousza31165ze3z87_1108:
					if (PAIRP(BgL_argsz00_1107))
						{	/* Llib/dsssl.scm 77 */
							bool_t BgL_test2213z00_2736;

							{	/* Llib/dsssl.scm 77 */
								bool_t BgL_test2214z00_2737;

								{	/* Llib/dsssl.scm 77 */
									obj_t BgL_tmpz00_2738;

									BgL_tmpz00_2738 = CAR(BgL_argsz00_1107);
									BgL_test2214z00_2737 = SYMBOLP(BgL_tmpz00_2738);
								}
								if (BgL_test2214z00_2737)
									{	/* Llib/dsssl.scm 77 */
										BgL_test2213z00_2736 = ((bool_t) 0);
									}
								else
									{	/* Llib/dsssl.scm 77 */
										bool_t BgL_test2215z00_2741;

										{	/* Llib/dsssl.scm 77 */
											obj_t BgL_tmpz00_2742;

											BgL_tmpz00_2742 = CAR(BgL_argsz00_1107);
											BgL_test2215z00_2741 = PAIRP(BgL_tmpz00_2742);
										}
										if (BgL_test2215z00_2741)
											{	/* Llib/dsssl.scm 77 */
												BgL_test2213z00_2736 = ((bool_t) 0);
											}
										else
											{	/* Llib/dsssl.scm 77 */
												BgL_test2213z00_2736 = ((bool_t) 1);
											}
									}
							}
							if (BgL_test2213z00_2736)
								{	/* Llib/dsssl.scm 77 */
									if ((CAR(BgL_argsz00_1107) == (BOPTIONAL)))
										{	/* Llib/dsssl.scm 81 */
											BgL_argsz00_1136 = CDR(BgL_argsz00_1107);
											BgL_nextzd2statezd2_1137 = BgL_optionalzd2statezd2_2507;
										BgL_zc3z04anonymousza31211ze3z87_1138:
											{
												obj_t BgL_asz00_1140;

												BgL_asz00_1140 = BgL_argsz00_1136;
												if (PAIRP(BgL_asz00_1140))
													{

														{	/* Llib/dsssl.scm 99 */
															obj_t BgL_ezd2104zd2_1146;

															BgL_ezd2104zd2_1146 = CAR(BgL_asz00_1140);
															if (SYMBOLP(BgL_ezd2104zd2_1146))
																{	/* Llib/dsssl.scm 99 */
																	{	/* Llib/dsssl.scm 101 */
																		obj_t BgL_dssslzd2argzd2_1157;

																		BgL_dssslzd2argzd2_1157 =
																			BGl_gensymz00zz__r4_symbols_6_4z00
																			(BGl_symbol2085z00zz__dssslz00);
																		{	/* Llib/dsssl.scm 102 */
																			obj_t BgL_arg1225z00_1158;

																			{	/* Llib/dsssl.scm 102 */
																				obj_t BgL_arg1226z00_1159;
																				obj_t BgL_arg1227z00_1160;

																				{	/* Llib/dsssl.scm 102 */
																					obj_t BgL_arg1228z00_1161;

																					{	/* Llib/dsssl.scm 102 */
																						obj_t BgL_arg1229z00_1162;

																						{	/* Llib/dsssl.scm 102 */
																							obj_t BgL_arg1230z00_1163;

																							BgL_arg1230z00_1163 =
																								CAR(((obj_t) BgL_asz00_1140));
																							BgL_arg1229z00_1162 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1230z00_1163, BNIL);
																						}
																						BgL_arg1228z00_1161 =
																							MAKE_YOUNG_PAIR
																							(BgL_dssslzd2argzd2_1157,
																							BgL_arg1229z00_1162);
																					}
																					BgL_arg1226z00_1159 =
																						MAKE_YOUNG_PAIR(BgL_arg1228z00_1161,
																						BNIL);
																				}
																				{	/* Llib/dsssl.scm 103 */
																					obj_t BgL_arg1231z00_1164;

																					BgL_arg1231z00_1164 =
																						((obj_t(*)(obj_t, obj_t,
																								obj_t))
																						PROCEDURE_L_ENTRY
																						(BgL_nextzd2statezd2_1137))
																						(BgL_nextzd2statezd2_1137,
																						BgL_argsz00_1136,
																						BgL_dssslzd2argzd2_1157);
																					BgL_arg1227z00_1160 =
																						MAKE_YOUNG_PAIR(BgL_arg1231z00_1164,
																						BNIL);
																				}
																				BgL_arg1225z00_1158 =
																					MAKE_YOUNG_PAIR(BgL_arg1226z00_1159,
																					BgL_arg1227z00_1160);
																			}
																			return
																				MAKE_YOUNG_PAIR
																				(BGl_symbol2087z00zz__dssslz00,
																				BgL_arg1225z00_1158);
																		}
																	}
																}
															else
																{	/* Llib/dsssl.scm 99 */
																	if (PAIRP(BgL_ezd2104zd2_1146))
																		{	/* Llib/dsssl.scm 99 */
																			obj_t BgL_cdrzd2106zd2_1149;

																			BgL_cdrzd2106zd2_1149 =
																				CDR(BgL_ezd2104zd2_1146);
																			{	/* Llib/dsssl.scm 99 */
																				bool_t BgL_test2220z00_2770;

																				{	/* Llib/dsssl.scm 99 */
																					obj_t BgL_tmpz00_2771;

																					BgL_tmpz00_2771 =
																						CAR(BgL_ezd2104zd2_1146);
																					BgL_test2220z00_2770 =
																						SYMBOLP(BgL_tmpz00_2771);
																				}
																				if (BgL_test2220z00_2770)
																					{	/* Llib/dsssl.scm 99 */
																						if (PAIRP(BgL_cdrzd2106zd2_1149))
																							{	/* Llib/dsssl.scm 99 */
																								if (NULLP(CDR
																										(BgL_cdrzd2106zd2_1149)))
																									{	/* Llib/dsssl.scm 99 */
																										{	/* Llib/dsssl.scm 105 */
																											obj_t
																												BgL_dssslzd2argzd2_1165;
																											BgL_dssslzd2argzd2_1165 =
																												BGl_gensymz00zz__r4_symbols_6_4z00
																												(BGl_symbol2085z00zz__dssslz00);
																											{	/* Llib/dsssl.scm 106 */
																												obj_t
																													BgL_arg1232z00_1166;
																												{	/* Llib/dsssl.scm 106 */
																													obj_t
																														BgL_arg1233z00_1167;
																													obj_t
																														BgL_arg1234z00_1168;
																													{	/* Llib/dsssl.scm 106 */
																														obj_t
																															BgL_arg1236z00_1169;
																														{	/* Llib/dsssl.scm 106 */
																															obj_t
																																BgL_arg1238z00_1170;
																															{	/* Llib/dsssl.scm 106 */
																																obj_t
																																	BgL_arg1239z00_1171;
																																{	/* Llib/dsssl.scm 106 */
																																	obj_t
																																		BgL_pairz00_2137;
																																	BgL_pairz00_2137
																																		=
																																		CAR(((obj_t)
																																			BgL_asz00_1140));
																																	BgL_arg1239z00_1171
																																		=
																																		CAR
																																		(BgL_pairz00_2137);
																																}
																																BgL_arg1238z00_1170
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1239z00_1171,
																																	BNIL);
																															}
																															BgL_arg1236z00_1169
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_dssslzd2argzd2_1165,
																																BgL_arg1238z00_1170);
																														}
																														BgL_arg1233z00_1167
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1236z00_1169,
																															BNIL);
																													}
																													{	/* Llib/dsssl.scm 107 */
																														obj_t
																															BgL_arg1244z00_1173;
																														BgL_arg1244z00_1173
																															=
																															((obj_t(*)(obj_t,
																																	obj_t,
																																	obj_t))
																															PROCEDURE_L_ENTRY
																															(BgL_nextzd2statezd2_1137))
																															(BgL_nextzd2statezd2_1137,
																															BgL_argsz00_1136,
																															BgL_dssslzd2argzd2_1165);
																														BgL_arg1234z00_1168
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1244z00_1173,
																															BNIL);
																													}
																													BgL_arg1232z00_1166 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1233z00_1167,
																														BgL_arg1234z00_1168);
																												}
																												return
																													MAKE_YOUNG_PAIR
																													(BGl_symbol2087z00zz__dssslz00,
																													BgL_arg1232z00_1166);
																											}
																										}
																									}
																								else
																									{	/* Llib/dsssl.scm 99 */
																									BgL_tagzd2103zd2_1145:
																										{	/* Llib/dsssl.scm 109 */
																											obj_t BgL_arg1248z00_1174;

																											{	/* Llib/dsssl.scm 109 */
																												obj_t
																													BgL_arg1249z00_1175;
																												BgL_arg1249z00_1175 =
																													CAR(((obj_t)
																														BgL_asz00_1140));
																												BgL_arg1248z00_1174 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1249z00_1175,
																													BgL_formalsz00_5);
																											}
																											return
																												BGL_PROCEDURE_CALL3
																												(BgL_errz00_7,
																												BgL_wherez00_4,
																												BGl_string2089z00zz__dssslz00,
																												BgL_arg1248z00_1174);
																										}
																									}
																							}
																						else
																							{	/* Llib/dsssl.scm 99 */
																								goto BgL_tagzd2103zd2_1145;
																							}
																					}
																				else
																					{	/* Llib/dsssl.scm 99 */
																						goto BgL_tagzd2103zd2_1145;
																					}
																			}
																		}
																	else
																		{	/* Llib/dsssl.scm 99 */
																			goto BgL_tagzd2103zd2_1145;
																		}
																}
														}
													}
												else
													{	/* Llib/dsssl.scm 97 */
														obj_t BgL_arg1252z00_1176;

														BgL_arg1252z00_1176 =
															MAKE_YOUNG_PAIR(BgL_asz00_1140, BgL_formalsz00_5);
														return
															BGL_PROCEDURE_CALL3(BgL_errz00_7, BgL_wherez00_4,
															BGl_string2090z00zz__dssslz00,
															BgL_arg1252z00_1176);
													}
											}
										}
									else
										{	/* Llib/dsssl.scm 81 */
											if ((CAR(BgL_argsz00_1107) == (BREST)))
												{
													obj_t BgL_nextzd2statezd2_2816;
													obj_t BgL_argsz00_2814;

													BgL_argsz00_2814 = CDR(BgL_argsz00_1107);
													BgL_nextzd2statezd2_2816 = BgL_restzd2statezd2_2506;
													BgL_nextzd2statezd2_1137 = BgL_nextzd2statezd2_2816;
													BgL_argsz00_1136 = BgL_argsz00_2814;
													goto BgL_zc3z04anonymousza31211ze3z87_1138;
												}
											else
												{	/* Llib/dsssl.scm 83 */
													if ((CAR(BgL_argsz00_1107) == (BKEY)))
														{
															obj_t BgL_nextzd2statezd2_2822;
															obj_t BgL_argsz00_2820;

															BgL_argsz00_2820 = CDR(BgL_argsz00_1107);
															BgL_nextzd2statezd2_2822 =
																BgL_nozd2restzd2keyzd2statezd2_2505;
															BgL_nextzd2statezd2_1137 =
																BgL_nextzd2statezd2_2822;
															BgL_argsz00_1136 = BgL_argsz00_2820;
															goto BgL_zc3z04anonymousza31211ze3z87_1138;
														}
													else
														{	/* Llib/dsssl.scm 88 */
															obj_t BgL_arg1200z00_1126;

															BgL_arg1200z00_1126 =
																MAKE_YOUNG_PAIR(CAR(BgL_argsz00_1107),
																BgL_formalsz00_5);
															return BGL_PROCEDURE_CALL3(BgL_errz00_7,
																BgL_wherez00_4, BGl_string2084z00zz__dssslz00,
																BgL_arg1200z00_1126);
														}
												}
										}
								}
							else
								{
									obj_t BgL_argsz00_2831;

									BgL_argsz00_2831 = CDR(BgL_argsz00_1107);
									BgL_argsz00_1107 = BgL_argsz00_2831;
									goto BgL_zc3z04anonymousza31165ze3z87_1108;
								}
						}
					else
						{	/* Llib/dsssl.scm 75 */
							return BgL_bodyz00_6;
						}
				}
			}
		}

	}



/* &make-dsssl-function-prelude */
	obj_t BGl_z62makezd2dssslzd2functionzd2preludezb0zz__dssslz00(obj_t
		BgL_envz00_2508, obj_t BgL_wherez00_2509, obj_t BgL_formalsz00_2510,
		obj_t BgL_bodyz00_2511, obj_t BgL_errz00_2512)
	{
		{	/* Llib/dsssl.scm 71 */
			{	/* Llib/dsssl.scm 75 */
				obj_t BgL_auxz00_2833;

				if (PROCEDUREP(BgL_errz00_2512))
					{	/* Llib/dsssl.scm 75 */
						BgL_auxz00_2833 = BgL_errz00_2512;
					}
				else
					{
						obj_t BgL_auxz00_2836;

						BgL_auxz00_2836 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2091z00zz__dssslz00,
							BINT(3130L), BGl_string2092z00zz__dssslz00,
							BGl_string2093z00zz__dssslz00, BgL_errz00_2512);
						FAILURE(BgL_auxz00_2836, BFALSE, BFALSE);
					}
				return
					BGl_makezd2dssslzd2functionzd2preludezd2zz__dssslz00
					(BgL_wherez00_2509, BgL_formalsz00_2510, BgL_bodyz00_2511,
					BgL_auxz00_2833);
			}
		}

	}



/* &key-state */
	obj_t BGl_z62keyzd2statezb0zz__dssslz00(obj_t BgL_bodyz00_2516,
		obj_t BgL_formalsz00_2515, obj_t BgL_wherez00_2514, obj_t BgL_errz00_2513,
		obj_t BgL_argsz00_1423, obj_t BgL_dssslzd2argzd2_1424,
		obj_t BgL_collectedzd2keyszd2_1425, bool_t BgL_allowzd2restpzd2_1426)
	{
		{	/* Llib/dsssl.scm 264 */
			{
				obj_t BgL_argz00_1513;
				obj_t BgL_initializa7erza7_1514;
				obj_t BgL_collectedzd2keyszd2_1515;
				obj_t BgL_argz00_1531;
				obj_t BgL_bodyz00_1532;

				if (NULLP(BgL_argsz00_1423))
					{	/* Llib/dsssl.scm 271 */
						if (BgL_allowzd2restpzd2_1426)
							{	/* Llib/dsssl.scm 274 */
								obj_t BgL_arg1480z00_1431;

								{	/* Llib/dsssl.scm 274 */
									obj_t BgL_arg1481z00_1432;
									obj_t BgL_arg1482z00_1433;

									{	/* Llib/dsssl.scm 274 */
										obj_t BgL_arg1483z00_1434;

										{	/* Llib/dsssl.scm 274 */
											obj_t BgL_arg1484z00_1435;

											{	/* Llib/dsssl.scm 274 */
												obj_t BgL_arg1485z00_1436;

												{	/* Llib/dsssl.scm 274 */
													obj_t BgL_arg1486z00_1437;

													{	/* Llib/dsssl.scm 274 */
														obj_t BgL_arg1487z00_1438;

														{	/* Llib/dsssl.scm 274 */
															obj_t BgL_arg1488z00_1439;

															BgL_arg1488z00_1439 =
																MAKE_YOUNG_PAIR(BgL_collectedzd2keyszd2_1425,
																BNIL);
															BgL_arg1487z00_1438 =
																MAKE_YOUNG_PAIR(BGl_symbol2094z00zz__dssslz00,
																BgL_arg1488z00_1439);
														}
														BgL_arg1486z00_1437 =
															MAKE_YOUNG_PAIR(BgL_arg1487z00_1438, BNIL);
													}
													BgL_arg1485z00_1436 =
														MAKE_YOUNG_PAIR(BgL_dssslzd2argzd2_1424,
														BgL_arg1486z00_1437);
												}
												BgL_arg1484z00_1435 =
													MAKE_YOUNG_PAIR(BGl_symbol2096z00zz__dssslz00,
													BgL_arg1485z00_1436);
											}
											BgL_arg1483z00_1434 =
												MAKE_YOUNG_PAIR(BgL_arg1484z00_1435, BNIL);
										}
										BgL_arg1481z00_1432 =
											MAKE_YOUNG_PAIR(BGl_symbol2100z00zz__dssslz00,
											BgL_arg1483z00_1434);
									}
									{	/* Llib/dsssl.scm 278 */
										obj_t BgL_arg1489z00_1440;

										{	/* Llib/dsssl.scm 278 */
											obj_t BgL_arg1490z00_1441;

											{	/* Llib/dsssl.scm 278 */
												obj_t BgL_arg1492z00_1442;

												{	/* Llib/dsssl.scm 278 */
													obj_t BgL_arg1494z00_1443;

													{	/* Llib/dsssl.scm 278 */
														obj_t BgL_arg1495z00_1444;
														obj_t BgL_arg1497z00_1445;

														{	/* Llib/dsssl.scm 278 */
															obj_t BgL_arg1498z00_1446;

															{	/* Llib/dsssl.scm 278 */
																obj_t BgL_arg1499z00_1447;

																{	/* Llib/dsssl.scm 278 */
																	obj_t BgL_arg1500z00_1448;

																	{	/* Llib/dsssl.scm 278 */
																		obj_t BgL_arg1501z00_1449;

																		{	/* Llib/dsssl.scm 278 */
																			obj_t BgL_arg1502z00_1450;

																			{	/* Llib/dsssl.scm 278 */
																				obj_t BgL_arg1503z00_1451;
																				obj_t BgL_arg1504z00_1452;

																				{	/* Llib/dsssl.scm 278 */
																					obj_t BgL_arg1505z00_1453;

																					{	/* Llib/dsssl.scm 278 */
																						obj_t BgL_arg1506z00_1454;
																						obj_t BgL_arg1507z00_1455;

																						BgL_arg1506z00_1454 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol2102z00zz__dssslz00,
																							BNIL);
																						{	/* Llib/dsssl.scm 279 */
																							obj_t BgL_arg1508z00_1456;

																							{	/* Llib/dsssl.scm 279 */
																								obj_t BgL_arg1509z00_1457;

																								{	/* Llib/dsssl.scm 279 */
																									obj_t BgL_arg1510z00_1458;

																									BgL_arg1510z00_1458 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2102z00zz__dssslz00,
																										BNIL);
																									BgL_arg1509z00_1457 =
																										MAKE_YOUNG_PAIR
																										(BGl_string2104z00zz__dssslz00,
																										BgL_arg1510z00_1458);
																								}
																								BgL_arg1508z00_1456 =
																									MAKE_YOUNG_PAIR
																									(BGl_symbol2105z00zz__dssslz00,
																									BgL_arg1509z00_1457);
																							}
																							BgL_arg1507z00_1455 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1508z00_1456, BNIL);
																						}
																						BgL_arg1505z00_1453 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1506z00_1454,
																							BgL_arg1507z00_1455);
																					}
																					BgL_arg1503z00_1451 =
																						MAKE_YOUNG_PAIR
																						(BGl_symbol2107z00zz__dssslz00,
																						BgL_arg1505z00_1453);
																				}
																				{	/* Llib/dsssl.scm 280 */
																					obj_t BgL_arg1511z00_1459;

																					{	/* Llib/dsssl.scm 280 */
																						obj_t BgL_arg1513z00_1460;

																						{	/* Llib/dsssl.scm 280 */
																							obj_t BgL_arg1514z00_1461;

																							{	/* Llib/dsssl.scm 280 */
																								obj_t BgL_arg1516z00_1462;

																								{	/* Llib/dsssl.scm 280 */
																									obj_t BgL_arg1517z00_1463;

																									BgL_arg1517z00_1463 =
																										MAKE_YOUNG_PAIR
																										(BgL_collectedzd2keyszd2_1425,
																										BNIL);
																									BgL_arg1516z00_1462 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2094z00zz__dssslz00,
																										BgL_arg1517z00_1463);
																								}
																								BgL_arg1514z00_1461 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1516z00_1462, BNIL);
																							}
																							BgL_arg1513z00_1460 =
																								MAKE_YOUNG_PAIR
																								(BgL_dssslzd2argzd2_1424,
																								BgL_arg1514z00_1461);
																						}
																						BgL_arg1511z00_1459 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol2096z00zz__dssslz00,
																							BgL_arg1513z00_1460);
																					}
																					BgL_arg1504z00_1452 =
																						MAKE_YOUNG_PAIR(BgL_arg1511z00_1459,
																						BNIL);
																				}
																				BgL_arg1502z00_1450 =
																					MAKE_YOUNG_PAIR(BgL_arg1503z00_1451,
																					BgL_arg1504z00_1452);
																			}
																			BgL_arg1501z00_1449 =
																				MAKE_YOUNG_PAIR
																				(BGl_symbol2109z00zz__dssslz00,
																				BgL_arg1502z00_1450);
																		}
																		BgL_arg1500z00_1448 =
																			MAKE_YOUNG_PAIR(BgL_arg1501z00_1449,
																			BNIL);
																	}
																	BgL_arg1499z00_1447 =
																		MAKE_YOUNG_PAIR
																		(BGl_string2111z00zz__dssslz00,
																		BgL_arg1500z00_1448);
																}
																BgL_arg1498z00_1446 =
																	MAKE_YOUNG_PAIR(BGl_symbol2112z00zz__dssslz00,
																	BgL_arg1499z00_1447);
															}
															BgL_arg1495z00_1444 =
																MAKE_YOUNG_PAIR(BGl_symbol2114z00zz__dssslz00,
																BgL_arg1498z00_1446);
														}
														BgL_arg1497z00_1445 =
															MAKE_YOUNG_PAIR(BgL_dssslzd2argzd2_1424, BNIL);
														BgL_arg1494z00_1443 =
															MAKE_YOUNG_PAIR(BgL_arg1495z00_1444,
															BgL_arg1497z00_1445);
													}
													BgL_arg1492z00_1442 =
														MAKE_YOUNG_PAIR(BGl_string2099z00zz__dssslz00,
														BgL_arg1494z00_1443);
												}
												BgL_arg1490z00_1441 =
													MAKE_YOUNG_PAIR(BGl_symbol2116z00zz__dssslz00,
													BgL_arg1492z00_1442);
											}
											BgL_arg1489z00_1440 =
												MAKE_YOUNG_PAIR(BgL_arg1490z00_1441, BNIL);
										}
										BgL_arg1482z00_1433 =
											MAKE_YOUNG_PAIR(BgL_bodyz00_2516, BgL_arg1489z00_1440);
									}
									BgL_arg1480z00_1431 =
										MAKE_YOUNG_PAIR(BgL_arg1481z00_1432, BgL_arg1482z00_1433);
								}
								return
									MAKE_YOUNG_PAIR(BGl_symbol2118z00zz__dssslz00,
									BgL_arg1480z00_1431);
							}
						else
							{	/* Llib/dsssl.scm 272 */
								return BgL_bodyz00_2516;
							}
					}
				else
					{	/* Llib/dsssl.scm 271 */
						if ((CAR(((obj_t) BgL_argsz00_1423)) == (BREST)))
							{	/* Llib/dsssl.scm 285 */
								bool_t BgL_test2229z00_2882;

								if (BgL_allowzd2restpzd2_1426)
									{	/* Llib/dsssl.scm 285 */
										if (NULLP(CDR(((obj_t) BgL_argsz00_1423))))
											{	/* Llib/dsssl.scm 286 */
												BgL_test2229z00_2882 = ((bool_t) 1);
											}
										else
											{	/* Llib/dsssl.scm 287 */
												bool_t BgL_test2232z00_2888;

												{	/* Llib/dsssl.scm 287 */
													obj_t BgL_tmpz00_2889;

													{	/* Llib/dsssl.scm 287 */
														obj_t BgL_pairz00_2221;

														BgL_pairz00_2221 = CDR(((obj_t) BgL_argsz00_1423));
														BgL_tmpz00_2889 = CAR(BgL_pairz00_2221);
													}
													BgL_test2232z00_2888 = SYMBOLP(BgL_tmpz00_2889);
												}
												if (BgL_test2232z00_2888)
													{	/* Llib/dsssl.scm 288 */
														obj_t BgL_tmpz00_2894;

														{	/* Llib/dsssl.scm 288 */
															obj_t BgL_pairz00_2225;

															BgL_pairz00_2225 =
																CDR(((obj_t) BgL_argsz00_1423));
															BgL_tmpz00_2894 = CDR(BgL_pairz00_2225);
														}
														BgL_test2229z00_2882 = PAIRP(BgL_tmpz00_2894);
													}
												else
													{	/* Llib/dsssl.scm 287 */
														BgL_test2229z00_2882 = ((bool_t) 1);
													}
											}
									}
								else
									{	/* Llib/dsssl.scm 285 */
										BgL_test2229z00_2882 = ((bool_t) 1);
									}
								if (BgL_test2229z00_2882)
									{	/* Llib/dsssl.scm 285 */
										return
											BGL_PROCEDURE_CALL3(BgL_errz00_2513, BgL_wherez00_2514,
											BGl_string2120z00zz__dssslz00, BgL_formalsz00_2515);
									}
								else
									{	/* Llib/dsssl.scm 290 */
										obj_t BgL_arg1531z00_1474;

										{	/* Llib/dsssl.scm 290 */
											obj_t BgL_pairz00_2229;

											BgL_pairz00_2229 = CDR(((obj_t) BgL_argsz00_1423));
											BgL_arg1531z00_1474 = CAR(BgL_pairz00_2229);
										}
										BgL_argz00_1531 = BgL_arg1531z00_1474;
										BgL_bodyz00_1532 = BgL_bodyz00_2516;
										{	/* Llib/dsssl.scm 267 */
											obj_t BgL_arg1589z00_1534;

											{	/* Llib/dsssl.scm 267 */
												obj_t BgL_arg1591z00_1535;
												obj_t BgL_arg1593z00_1536;

												{	/* Llib/dsssl.scm 267 */
													obj_t BgL_arg1594z00_1537;

													{	/* Llib/dsssl.scm 267 */
														obj_t BgL_arg1595z00_1538;

														{	/* Llib/dsssl.scm 267 */
															obj_t BgL_arg1598z00_1539;

															{	/* Llib/dsssl.scm 267 */
																obj_t BgL_arg1601z00_1540;

																{	/* Llib/dsssl.scm 267 */
																	obj_t BgL_arg1602z00_1541;

																	{	/* Llib/dsssl.scm 267 */
																		obj_t BgL_arg1603z00_1542;

																		{	/* Llib/dsssl.scm 267 */
																			obj_t BgL_arg1605z00_1543;

																			BgL_arg1605z00_1543 =
																				MAKE_YOUNG_PAIR
																				(BgL_collectedzd2keyszd2_1425, BNIL);
																			BgL_arg1603z00_1542 =
																				MAKE_YOUNG_PAIR
																				(BGl_symbol2094z00zz__dssslz00,
																				BgL_arg1605z00_1543);
																		}
																		BgL_arg1602z00_1541 =
																			MAKE_YOUNG_PAIR(BgL_arg1603z00_1542,
																			BNIL);
																	}
																	BgL_arg1601z00_1540 =
																		MAKE_YOUNG_PAIR(BgL_dssslzd2argzd2_1424,
																		BgL_arg1602z00_1541);
																}
																BgL_arg1598z00_1539 =
																	MAKE_YOUNG_PAIR(BGl_symbol2096z00zz__dssslz00,
																	BgL_arg1601z00_1540);
															}
															BgL_arg1595z00_1538 =
																MAKE_YOUNG_PAIR(BgL_arg1598z00_1539, BNIL);
														}
														BgL_arg1594z00_1537 =
															MAKE_YOUNG_PAIR(BgL_argz00_1531,
															BgL_arg1595z00_1538);
													}
													BgL_arg1591z00_1535 =
														MAKE_YOUNG_PAIR(BgL_arg1594z00_1537, BNIL);
												}
												BgL_arg1593z00_1536 =
													MAKE_YOUNG_PAIR(BgL_bodyz00_1532, BNIL);
												BgL_arg1589z00_1534 =
													MAKE_YOUNG_PAIR(BgL_arg1591z00_1535,
													BgL_arg1593z00_1536);
											}
											return
												MAKE_YOUNG_PAIR(BGl_symbol2087z00zz__dssslz00,
												BgL_arg1589z00_1534);
										}
									}
							}
						else
							{	/* Llib/dsssl.scm 284 */
								if (PAIRP(BgL_argsz00_1423))
									{	/* Llib/dsssl.scm 293 */
										bool_t BgL_test2234z00_2921;

										{	/* Llib/dsssl.scm 293 */
											bool_t BgL_test2235z00_2922;

											{	/* Llib/dsssl.scm 293 */
												obj_t BgL_tmpz00_2923;

												BgL_tmpz00_2923 = CAR(BgL_argsz00_1423);
												BgL_test2235z00_2922 = SYMBOLP(BgL_tmpz00_2923);
											}
											if (BgL_test2235z00_2922)
												{	/* Llib/dsssl.scm 293 */
													BgL_test2234z00_2921 = ((bool_t) 0);
												}
											else
												{	/* Llib/dsssl.scm 293 */
													bool_t BgL_test2236z00_2926;

													{	/* Llib/dsssl.scm 293 */
														obj_t BgL_tmpz00_2927;

														BgL_tmpz00_2927 = CAR(BgL_argsz00_1423);
														BgL_test2236z00_2926 = PAIRP(BgL_tmpz00_2927);
													}
													if (BgL_test2236z00_2926)
														{	/* Llib/dsssl.scm 293 */
															BgL_test2234z00_2921 = ((bool_t) 0);
														}
													else
														{	/* Llib/dsssl.scm 293 */
															BgL_test2234z00_2921 = ((bool_t) 1);
														}
												}
										}
										if (BgL_test2234z00_2921)
											{	/* Llib/dsssl.scm 293 */
												return
													BGL_PROCEDURE_CALL3(BgL_errz00_2513,
													BgL_wherez00_2514, BGl_string2121z00zz__dssslz00,
													BgL_formalsz00_2515);
											}
										else
											{	/* Llib/dsssl.scm 297 */
												obj_t BgL_ezd2162zd2_1494;

												BgL_ezd2162zd2_1494 = CAR(BgL_argsz00_1423);
												if (PAIRP(BgL_ezd2162zd2_1494))
													{	/* Llib/dsssl.scm 297 */
														obj_t BgL_carzd2167zd2_1496;
														obj_t BgL_cdrzd2168zd2_1497;

														BgL_carzd2167zd2_1496 = CAR(BgL_ezd2162zd2_1494);
														BgL_cdrzd2168zd2_1497 = CDR(BgL_ezd2162zd2_1494);
														if (SYMBOLP(BgL_carzd2167zd2_1496))
															{	/* Llib/dsssl.scm 297 */
																if (PAIRP(BgL_cdrzd2168zd2_1497))
																	{	/* Llib/dsssl.scm 297 */
																		if (NULLP(CDR(BgL_cdrzd2168zd2_1497)))
																			{	/* Llib/dsssl.scm 297 */
																				BgL_argz00_1513 = BgL_carzd2167zd2_1496;
																				BgL_initializa7erza7_1514 =
																					CAR(BgL_cdrzd2168zd2_1497);
																				BgL_collectedzd2keyszd2_1515 =
																					BgL_collectedzd2keyszd2_1425;
																			BgL_zc3z04anonymousza31565ze3z87_1516:
																				{	/* Llib/dsssl.scm 260 */
																					obj_t BgL_arg1567z00_1517;

																					{	/* Llib/dsssl.scm 260 */
																						obj_t BgL_arg1571z00_1518;
																						obj_t BgL_arg1573z00_1519;

																						{	/* Llib/dsssl.scm 260 */
																							obj_t BgL_arg1575z00_1520;

																							{	/* Llib/dsssl.scm 260 */
																								obj_t BgL_arg1576z00_1521;

																								{	/* Llib/dsssl.scm 260 */
																									obj_t BgL_arg1578z00_1522;

																									{	/* Llib/dsssl.scm 260 */
																										obj_t BgL_arg1579z00_1523;

																										{	/* Llib/dsssl.scm 260 */
																											obj_t BgL_arg1580z00_1524;

																											{	/* Llib/dsssl.scm 260 */
																												obj_t
																													BgL_arg1582z00_1525;
																												obj_t
																													BgL_arg1583z00_1526;
																												BgL_arg1582z00_1525 =
																													BGl_symbolzd2ze3keywordz31zz__r4_symbols_6_4z00
																													(BgL_argz00_1513);
																												BgL_arg1583z00_1526 =
																													MAKE_YOUNG_PAIR
																													(BgL_initializa7erza7_1514,
																													BNIL);
																												BgL_arg1580z00_1524 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1582z00_1525,
																													BgL_arg1583z00_1526);
																											}
																											BgL_arg1579z00_1523 =
																												MAKE_YOUNG_PAIR
																												(BgL_dssslzd2argzd2_1424,
																												BgL_arg1580z00_1524);
																										}
																										BgL_arg1578z00_1522 =
																											MAKE_YOUNG_PAIR
																											(BGl_symbol2098z00zz__dssslz00,
																											BgL_arg1579z00_1523);
																									}
																									BgL_arg1576z00_1521 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1578z00_1522, BNIL);
																								}
																								BgL_arg1575z00_1520 =
																									MAKE_YOUNG_PAIR
																									(BgL_argz00_1513,
																									BgL_arg1576z00_1521);
																							}
																							BgL_arg1571z00_1518 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1575z00_1520, BNIL);
																						}
																						{	/* Llib/dsssl.scm 261 */
																							obj_t BgL_arg1584z00_1527;

																							{	/* Llib/dsssl.scm 261 */
																								obj_t BgL_arg1585z00_1528;
																								obj_t BgL_arg1586z00_1529;

																								BgL_arg1585z00_1528 =
																									CDR(
																									((obj_t) BgL_argsz00_1423));
																								BgL_arg1586z00_1529 =
																									MAKE_YOUNG_PAIR
																									(BGl_symbolzd2ze3keywordz31zz__r4_symbols_6_4z00
																									(BgL_argz00_1513),
																									BgL_collectedzd2keyszd2_1515);
																								BgL_arg1584z00_1527 =
																									BGl_z62keyzd2statezb0zz__dssslz00
																									(BgL_bodyz00_2516,
																									BgL_formalsz00_2515,
																									BgL_wherez00_2514,
																									BgL_errz00_2513,
																									BgL_arg1585z00_1528,
																									BgL_dssslzd2argzd2_1424,
																									BgL_arg1586z00_1529,
																									BgL_allowzd2restpzd2_1426);
																							}
																							BgL_arg1573z00_1519 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1584z00_1527, BNIL);
																						}
																						BgL_arg1567z00_1517 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1571z00_1518,
																							BgL_arg1573z00_1519);
																					}
																					return
																						MAKE_YOUNG_PAIR
																						(BGl_symbol2087z00zz__dssslz00,
																						BgL_arg1567z00_1517);
																				}
																			}
																		else
																			{	/* Llib/dsssl.scm 297 */
																				return
																					BGL_PROCEDURE_CALL3(BgL_errz00_2513,
																					BgL_wherez00_2514,
																					BGl_string2121z00zz__dssslz00,
																					BgL_formalsz00_2515);
																			}
																	}
																else
																	{	/* Llib/dsssl.scm 297 */
																		return
																			BGL_PROCEDURE_CALL3(BgL_errz00_2513,
																			BgL_wherez00_2514,
																			BGl_string2121z00zz__dssslz00,
																			BgL_formalsz00_2515);
																	}
															}
														else
															{	/* Llib/dsssl.scm 297 */
																return
																	BGL_PROCEDURE_CALL3(BgL_errz00_2513,
																	BgL_wherez00_2514,
																	BGl_string2121z00zz__dssslz00,
																	BgL_formalsz00_2515);
															}
													}
												else
													{	/* Llib/dsssl.scm 297 */
														if (SYMBOLP(BgL_ezd2162zd2_1494))
															{
																obj_t BgL_collectedzd2keyszd2_2987;
																obj_t BgL_initializa7erza7_2986;
																obj_t BgL_argz00_2985;

																BgL_argz00_2985 = BgL_ezd2162zd2_1494;
																BgL_initializa7erza7_2986 = BFALSE;
																BgL_collectedzd2keyszd2_2987 =
																	BgL_collectedzd2keyszd2_1425;
																BgL_collectedzd2keyszd2_1515 =
																	BgL_collectedzd2keyszd2_2987;
																BgL_initializa7erza7_1514 =
																	BgL_initializa7erza7_2986;
																BgL_argz00_1513 = BgL_argz00_2985;
																goto BgL_zc3z04anonymousza31565ze3z87_1516;
															}
														else
															{	/* Llib/dsssl.scm 297 */
																return
																	BGL_PROCEDURE_CALL3(BgL_errz00_2513,
																	BgL_wherez00_2514,
																	BGl_string2121z00zz__dssslz00,
																	BgL_formalsz00_2515);
															}
													}
											}
									}
								else
									{	/* Llib/dsssl.scm 291 */
										return
											BGL_PROCEDURE_CALL3(BgL_errz00_2513, BgL_wherez00_2514,
											BGl_string2121z00zz__dssslz00, BgL_formalsz00_2515);
									}
							}
					}
			}
		}

	}



/* &optional-state */
	obj_t BGl_z62optionalzd2statezb0zz__dssslz00(obj_t BgL_envz00_2517,
		obj_t BgL_argsz00_2524, obj_t BgL_dssslzd2argzd2_2525)
	{
		{	/* Llib/dsssl.scm 135 */
			{	/* Llib/dsssl.scm 116 */
				obj_t BgL_errz00_2518;
				obj_t BgL_wherez00_2519;
				obj_t BgL_formalsz00_2520;
				obj_t BgL_nozd2restzd2keyzd2statezd2_2521;
				obj_t BgL_restzd2statezd2_2522;
				obj_t BgL_bodyz00_2523;

				BgL_errz00_2518 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_2517, (int) (0L)));
				BgL_wherez00_2519 = PROCEDURE_L_REF(BgL_envz00_2517, (int) (1L));
				BgL_formalsz00_2520 = PROCEDURE_L_REF(BgL_envz00_2517, (int) (2L));
				BgL_nozd2restzd2keyzd2statezd2_2521 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_2517, (int) (3L)));
				BgL_restzd2statezd2_2522 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_2517, (int) (4L)));
				BgL_bodyz00_2523 = PROCEDURE_L_REF(BgL_envz00_2517, (int) (5L));
				{
					obj_t BgL_argsz00_2601;

					{	/* Llib/dsssl.scm 183 */
						obj_t BgL_auxz00_3015;

						BgL_argsz00_2601 = BgL_argsz00_2524;
						{
							obj_t BgL_argsz00_2603;

							BgL_argsz00_2603 = BgL_argsz00_2601;
						BgL_loopz00_2602:
							if (PAIRP(BgL_argsz00_2603))
								{	/* Llib/dsssl.scm 116 */
									if ((CAR(BgL_argsz00_2603) == (BKEY)))
										{	/* Llib/dsssl.scm 119 */
											obj_t BgL_g1040z00_2604;

											BgL_g1040z00_2604 = CDR(BgL_argsz00_2603);
											{
												obj_t BgL_argsz00_2606;
												obj_t BgL_resz00_2607;

												BgL_argsz00_2606 = BgL_g1040z00_2604;
												BgL_resz00_2607 = BNIL;
											BgL_loopz00_2605:
												{	/* Llib/dsssl.scm 122 */
													bool_t BgL_test2244z00_3022;

													if (PAIRP(BgL_argsz00_2606))
														{	/* Llib/dsssl.scm 123 */
															bool_t BgL_test2246z00_3025;

															{	/* Llib/dsssl.scm 123 */
																bool_t BgL_test2247z00_3026;

																{	/* Llib/dsssl.scm 123 */
																	obj_t BgL_tmpz00_3027;

																	BgL_tmpz00_3027 = CAR(BgL_argsz00_2606);
																	BgL_test2247z00_3026 = PAIRP(BgL_tmpz00_3027);
																}
																if (BgL_test2247z00_3026)
																	{	/* Llib/dsssl.scm 123 */
																		BgL_test2246z00_3025 = ((bool_t) 1);
																	}
																else
																	{	/* Llib/dsssl.scm 124 */
																		obj_t BgL_tmpz00_3030;

																		BgL_tmpz00_3030 = CAR(BgL_argsz00_2606);
																		BgL_test2246z00_3025 =
																			SYMBOLP(BgL_tmpz00_3030);
																	}
															}
															if (BgL_test2246z00_3025)
																{	/* Llib/dsssl.scm 123 */
																	if ((CAR(BgL_argsz00_2606) == (BOPTIONAL)))
																		{	/* Llib/dsssl.scm 125 */
																			BgL_test2244z00_3022 = ((bool_t) 1);
																		}
																	else
																		{	/* Llib/dsssl.scm 125 */
																			BgL_test2244z00_3022 =
																				(CAR(BgL_argsz00_2606) == (BREST));
																		}
																}
															else
																{	/* Llib/dsssl.scm 123 */
																	BgL_test2244z00_3022 = ((bool_t) 1);
																}
														}
													else
														{	/* Llib/dsssl.scm 122 */
															BgL_test2244z00_3022 = ((bool_t) 1);
														}
													if (BgL_test2244z00_3022)
														{	/* Llib/dsssl.scm 122 */
															BgL_auxz00_3015 = BgL_resz00_2607;
														}
													else
														{	/* Llib/dsssl.scm 128 */
															bool_t BgL_test2249z00_3038;

															{	/* Llib/dsssl.scm 128 */
																obj_t BgL_tmpz00_3039;

																BgL_tmpz00_3039 =
																	CAR(((obj_t) BgL_argsz00_2606));
																BgL_test2249z00_3038 = SYMBOLP(BgL_tmpz00_3039);
															}
															if (BgL_test2249z00_3038)
																{	/* Llib/dsssl.scm 129 */
																	obj_t BgL_arg1401z00_2608;
																	obj_t BgL_arg1402z00_2609;

																	BgL_arg1401z00_2608 =
																		CDR(((obj_t) BgL_argsz00_2606));
																	{	/* Llib/dsssl.scm 130 */
																		obj_t BgL_arg1403z00_2610;

																		{	/* Llib/dsssl.scm 130 */
																			obj_t BgL_arg1404z00_2611;

																			BgL_arg1404z00_2611 =
																				CAR(((obj_t) BgL_argsz00_2606));
																			BgL_arg1403z00_2610 =
																				BGl_symbolzd2ze3keywordz31zz__r4_symbols_6_4z00
																				(BgL_arg1404z00_2611);
																		}
																		BgL_arg1402z00_2609 =
																			MAKE_YOUNG_PAIR(BgL_arg1403z00_2610,
																			BgL_resz00_2607);
																	}
																	{
																		obj_t BgL_resz00_3050;
																		obj_t BgL_argsz00_3049;

																		BgL_argsz00_3049 = BgL_arg1401z00_2608;
																		BgL_resz00_3050 = BgL_arg1402z00_2609;
																		BgL_resz00_2607 = BgL_resz00_3050;
																		BgL_argsz00_2606 = BgL_argsz00_3049;
																		goto BgL_loopz00_2605;
																	}
																}
															else
																{	/* Llib/dsssl.scm 132 */
																	obj_t BgL_arg1405z00_2612;
																	obj_t BgL_arg1406z00_2613;

																	BgL_arg1405z00_2612 =
																		CDR(((obj_t) BgL_argsz00_2606));
																	{	/* Llib/dsssl.scm 133 */
																		obj_t BgL_arg1407z00_2614;

																		{	/* Llib/dsssl.scm 133 */
																			obj_t BgL_arg1408z00_2615;

																			{	/* Llib/dsssl.scm 133 */
																				obj_t BgL_pairz00_2616;

																				BgL_pairz00_2616 =
																					CAR(((obj_t) BgL_argsz00_2606));
																				BgL_arg1408z00_2615 =
																					CAR(BgL_pairz00_2616);
																			}
																			BgL_arg1407z00_2614 =
																				BGl_symbolzd2ze3keywordz31zz__r4_symbols_6_4z00
																				(BgL_arg1408z00_2615);
																		}
																		BgL_arg1406z00_2613 =
																			MAKE_YOUNG_PAIR(BgL_arg1407z00_2614,
																			BgL_resz00_2607);
																	}
																	{
																		obj_t BgL_resz00_3059;
																		obj_t BgL_argsz00_3058;

																		BgL_argsz00_3058 = BgL_arg1405z00_2612;
																		BgL_resz00_3059 = BgL_arg1406z00_2613;
																		BgL_resz00_2607 = BgL_resz00_3059;
																		BgL_argsz00_2606 = BgL_argsz00_3058;
																		goto BgL_loopz00_2605;
																	}
																}
														}
												}
											}
										}
									else
										{
											obj_t BgL_argsz00_3060;

											BgL_argsz00_3060 = CDR(BgL_argsz00_2603);
											BgL_argsz00_2603 = BgL_argsz00_3060;
											goto BgL_loopz00_2602;
										}
								}
							else
								{	/* Llib/dsssl.scm 116 */
									BgL_auxz00_3015 = BNIL;
								}
						}
						return
							BGl_optionalzd2argsze70z35zz__dssslz00(BgL_bodyz00_2523,
							BgL_restzd2statezd2_2522, BgL_nozd2restzd2keyzd2statezd2_2521,
							BgL_formalsz00_2520, BgL_wherez00_2519, BgL_errz00_2518,
							BgL_auxz00_3015, BgL_dssslzd2argzd2_2525, BgL_argsz00_2524);
					}
				}
			}
		}

	}



/* optional-args~0 */
	obj_t BGl_optionalzd2argsze70z35zz__dssslz00(obj_t BgL_bodyz00_2568,
		obj_t BgL_restzd2statezd2_2567, obj_t BgL_nozd2restzd2keyzd2statezd2_2566,
		obj_t BgL_formalsz00_2565, obj_t BgL_wherez00_2564, obj_t BgL_errz00_2563,
		obj_t BgL_keywordzd2argumentszd2_2562, obj_t BgL_dssslzd2argzd2_2561,
		obj_t BgL_argsz00_1229)
	{
		{	/* Llib/dsssl.scm 181 */
			{
				obj_t BgL_argz00_1185;
				obj_t BgL_initializa7erza7_1186;
				obj_t BgL_restz00_1187;

				if (NULLP(BgL_argsz00_1229))
					{	/* Llib/dsssl.scm 159 */
						return BgL_bodyz00_2568;
					}
				else
					{	/* Llib/dsssl.scm 159 */
						if (PAIRP(BgL_argsz00_1229))
							{	/* Llib/dsssl.scm 163 */
								bool_t BgL_test2252z00_3067;

								{	/* Llib/dsssl.scm 163 */
									bool_t BgL_test2253z00_3068;

									{	/* Llib/dsssl.scm 163 */
										obj_t BgL_tmpz00_3069;

										BgL_tmpz00_3069 = CAR(BgL_argsz00_1229);
										BgL_test2253z00_3068 = SYMBOLP(BgL_tmpz00_3069);
									}
									if (BgL_test2253z00_3068)
										{	/* Llib/dsssl.scm 163 */
											BgL_test2252z00_3067 = ((bool_t) 0);
										}
									else
										{	/* Llib/dsssl.scm 163 */
											bool_t BgL_test2254z00_3072;

											{	/* Llib/dsssl.scm 163 */
												obj_t BgL_tmpz00_3073;

												BgL_tmpz00_3073 = CAR(BgL_argsz00_1229);
												BgL_test2254z00_3072 = PAIRP(BgL_tmpz00_3073);
											}
											if (BgL_test2254z00_3072)
												{	/* Llib/dsssl.scm 163 */
													BgL_test2252z00_3067 = ((bool_t) 0);
												}
											else
												{	/* Llib/dsssl.scm 163 */
													BgL_test2252z00_3067 = ((bool_t) 1);
												}
										}
								}
								if (BgL_test2252z00_3067)
									{	/* Llib/dsssl.scm 163 */
										if ((CAR(BgL_argsz00_1229) == (BREST)))
											{	/* Llib/dsssl.scm 167 */
												return
													BGl_z62restzd2statezb0zz__dssslz00
													(BgL_restzd2statezd2_2567, CDR(BgL_argsz00_1229),
													BgL_dssslzd2argzd2_2561);
											}
										else
											{	/* Llib/dsssl.scm 167 */
												if ((CAR(BgL_argsz00_1229) == (BKEY)))
													{	/* Llib/dsssl.scm 169 */
														return
															BGl_z62nozd2restzd2keyzd2statezb0zz__dssslz00
															(BgL_nozd2restzd2keyzd2statezd2_2566,
															CDR(BgL_argsz00_1229), BgL_dssslzd2argzd2_2561);
													}
												else
													{	/* Llib/dsssl.scm 169 */
														return
															BGL_PROCEDURE_CALL3(BgL_errz00_2563,
															BgL_wherez00_2564, BGl_string2134z00zz__dssslz00,
															BgL_formalsz00_2565);
													}
											}
									}
								else
									{	/* Llib/dsssl.scm 175 */
										obj_t BgL_ezd2112zd2_1254;

										BgL_ezd2112zd2_1254 = CAR(BgL_argsz00_1229);
										if (PAIRP(BgL_ezd2112zd2_1254))
											{	/* Llib/dsssl.scm 175 */
												obj_t BgL_carzd2117zd2_1256;
												obj_t BgL_cdrzd2118zd2_1257;

												BgL_carzd2117zd2_1256 = CAR(BgL_ezd2112zd2_1254);
												BgL_cdrzd2118zd2_1257 = CDR(BgL_ezd2112zd2_1254);
												if (SYMBOLP(BgL_carzd2117zd2_1256))
													{	/* Llib/dsssl.scm 175 */
														if (PAIRP(BgL_cdrzd2118zd2_1257))
															{	/* Llib/dsssl.scm 175 */
																if (NULLP(CDR(BgL_cdrzd2118zd2_1257)))
																	{	/* Llib/dsssl.scm 175 */
																		BgL_argz00_1185 = BgL_carzd2117zd2_1256;
																		BgL_initializa7erza7_1186 =
																			CAR(BgL_cdrzd2118zd2_1257);
																		BgL_restz00_1187 = CDR(BgL_argsz00_1229);
																	BgL_zc3z04anonymousza31254ze3z87_1188:
																		{	/* Llib/dsssl.scm 140 */
																			obj_t BgL_tmpz00_1189;

																			BgL_tmpz00_1189 =
																				BGl_gensymz00zz__r4_symbols_6_4z00
																				(BGl_symbol2122z00zz__dssslz00);
																			{	/* Llib/dsssl.scm 141 */
																				obj_t BgL_arg1268z00_1190;

																				{	/* Llib/dsssl.scm 141 */
																					obj_t BgL_arg1272z00_1191;
																					obj_t BgL_arg1284z00_1192;

																					{	/* Llib/dsssl.scm 141 */
																						obj_t BgL_arg1304z00_1193;

																						{	/* Llib/dsssl.scm 141 */
																							obj_t BgL_arg1305z00_1194;

																							{	/* Llib/dsssl.scm 141 */
																								obj_t BgL_arg1306z00_1195;

																								{	/* Llib/dsssl.scm 141 */
																									obj_t BgL_arg1307z00_1196;

																									{	/* Llib/dsssl.scm 141 */
																										obj_t BgL_arg1308z00_1197;
																										obj_t BgL_arg1309z00_1198;

																										{	/* Llib/dsssl.scm 141 */
																											obj_t BgL_arg1310z00_1199;

																											{	/* Llib/dsssl.scm 141 */
																												obj_t
																													BgL_arg1311z00_1200;
																												obj_t
																													BgL_arg1312z00_1201;
																												{	/* Llib/dsssl.scm 141 */
																													obj_t
																														BgL_arg1314z00_1202;
																													BgL_arg1314z00_1202 =
																														MAKE_YOUNG_PAIR
																														(BgL_dssslzd2argzd2_2561,
																														BNIL);
																													BgL_arg1311z00_1200 =
																														MAKE_YOUNG_PAIR
																														(BGl_symbol2100z00zz__dssslz00,
																														BgL_arg1314z00_1202);
																												}
																												{	/* Llib/dsssl.scm 143 */
																													obj_t
																														BgL_arg1315z00_1203;
																													{	/* Llib/dsssl.scm 143 */
																														obj_t
																															BgL_arg1316z00_1204;
																														{	/* Llib/dsssl.scm 143 */
																															obj_t
																																BgL_arg1317z00_1205;
																															{	/* Llib/dsssl.scm 143 */
																																obj_t
																																	BgL_arg1318z00_1206;
																																obj_t
																																	BgL_arg1319z00_1207;
																																{	/* Llib/dsssl.scm 143 */
																																	obj_t
																																		BgL_arg1320z00_1208;
																																	BgL_arg1320z00_1208
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_dssslzd2argzd2_2561,
																																		BNIL);
																																	BgL_arg1318z00_1206
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_symbol2124z00zz__dssslz00,
																																		BgL_arg1320z00_1208);
																																}
																																{	/* Llib/dsssl.scm 143 */
																																	obj_t
																																		BgL_arg1321z00_1209;
																																	{	/* Llib/dsssl.scm 143 */
																																		obj_t
																																			BgL_arg1322z00_1210;
																																		BgL_arg1322z00_1210
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_keywordzd2argumentszd2_2562,
																																			BNIL);
																																		BgL_arg1321z00_1209
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BGl_symbol2094z00zz__dssslz00,
																																			BgL_arg1322z00_1210);
																																	}
																																	BgL_arg1319z00_1207
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1321z00_1209,
																																		BNIL);
																																}
																																BgL_arg1317z00_1205
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1318z00_1206,
																																	BgL_arg1319z00_1207);
																															}
																															BgL_arg1316z00_1204
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol2126z00zz__dssslz00,
																																BgL_arg1317z00_1205);
																														}
																														BgL_arg1315z00_1203
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1316z00_1204,
																															BNIL);
																													}
																													BgL_arg1312z00_1201 =
																														MAKE_YOUNG_PAIR
																														(BTRUE,
																														BgL_arg1315z00_1203);
																												}
																												BgL_arg1310z00_1199 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1311z00_1200,
																													BgL_arg1312z00_1201);
																											}
																											BgL_arg1308z00_1197 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol2118z00zz__dssslz00,
																												BgL_arg1310z00_1199);
																										}
																										{	/* Llib/dsssl.scm 145 */
																											obj_t BgL_arg1323z00_1211;

																											{	/* Llib/dsssl.scm 145 */
																												obj_t
																													BgL_arg1325z00_1212;
																												{	/* Llib/dsssl.scm 145 */
																													obj_t
																														BgL_arg1326z00_1213;
																													{	/* Llib/dsssl.scm 145 */
																														obj_t
																															BgL_arg1327z00_1214;
																														obj_t
																															BgL_arg1328z00_1215;
																														{	/* Llib/dsssl.scm 145 */
																															obj_t
																																BgL_arg1329z00_1216;
																															{	/* Llib/dsssl.scm 145 */
																																obj_t
																																	BgL_arg1331z00_1217;
																																{	/* Llib/dsssl.scm 145 */
																																	obj_t
																																		BgL_arg1332z00_1218;
																																	{	/* Llib/dsssl.scm 145 */
																																		obj_t
																																			BgL_arg1333z00_1219;
																																		BgL_arg1333z00_1219
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_dssslzd2argzd2_2561,
																																			BNIL);
																																		BgL_arg1332z00_1218
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BGl_symbol2124z00zz__dssslz00,
																																			BgL_arg1333z00_1219);
																																	}
																																	BgL_arg1331z00_1217
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1332z00_1218,
																																		BNIL);
																																}
																																BgL_arg1329z00_1216
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_tmpz00_1189,
																																	BgL_arg1331z00_1217);
																															}
																															BgL_arg1327z00_1214
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1329z00_1216,
																																BNIL);
																														}
																														{	/* Llib/dsssl.scm 153 */
																															obj_t
																																BgL_arg1334z00_1220;
																															{	/* Llib/dsssl.scm 153 */
																																obj_t
																																	BgL_arg1335z00_1221;
																																{	/* Llib/dsssl.scm 153 */
																																	obj_t
																																		BgL_arg1336z00_1222;
																																	obj_t
																																		BgL_arg1337z00_1223;
																																	{	/* Llib/dsssl.scm 153 */
																																		obj_t
																																			BgL_arg1338z00_1224;
																																		{	/* Llib/dsssl.scm 153 */
																																			obj_t
																																				BgL_arg1339z00_1225;
																																			{	/* Llib/dsssl.scm 153 */
																																				obj_t
																																					BgL_arg1340z00_1226;
																																				{	/* Llib/dsssl.scm 153 */
																																					obj_t
																																						BgL_arg1341z00_1227;
																																					BgL_arg1341z00_1227
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_dssslzd2argzd2_2561,
																																						BNIL);
																																					BgL_arg1340z00_1226
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_symbol2128z00zz__dssslz00,
																																						BgL_arg1341z00_1227);
																																				}
																																				BgL_arg1339z00_1225
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1340z00_1226,
																																					BNIL);
																																			}
																																			BgL_arg1338z00_1224
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_dssslzd2argzd2_2561,
																																				BgL_arg1339z00_1225);
																																		}
																																		BgL_arg1336z00_1222
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BGl_symbol2130z00zz__dssslz00,
																																			BgL_arg1338z00_1224);
																																	}
																																	BgL_arg1337z00_1223
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_tmpz00_1189,
																																		BNIL);
																																	BgL_arg1335z00_1221
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1336z00_1222,
																																		BgL_arg1337z00_1223);
																																}
																																BgL_arg1334z00_1220
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BGl_symbol2132z00zz__dssslz00,
																																	BgL_arg1335z00_1221);
																															}
																															BgL_arg1328z00_1215
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1334z00_1220,
																																BNIL);
																														}
																														BgL_arg1326z00_1213
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1327z00_1214,
																															BgL_arg1328z00_1215);
																													}
																													BgL_arg1325z00_1212 =
																														MAKE_YOUNG_PAIR
																														(BGl_symbol2087z00zz__dssslz00,
																														BgL_arg1326z00_1213);
																												}
																												BgL_arg1323z00_1211 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1325z00_1212,
																													BNIL);
																											}
																											BgL_arg1309z00_1198 =
																												MAKE_YOUNG_PAIR
																												(BgL_initializa7erza7_1186,
																												BgL_arg1323z00_1211);
																										}
																										BgL_arg1307z00_1196 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1308z00_1197,
																											BgL_arg1309z00_1198);
																									}
																									BgL_arg1306z00_1195 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2118z00zz__dssslz00,
																										BgL_arg1307z00_1196);
																								}
																								BgL_arg1305z00_1194 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1306z00_1195, BNIL);
																							}
																							BgL_arg1304z00_1193 =
																								MAKE_YOUNG_PAIR(BgL_argz00_1185,
																								BgL_arg1305z00_1194);
																						}
																						BgL_arg1272z00_1191 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1304z00_1193, BNIL);
																					}
																					BgL_arg1284z00_1192 =
																						MAKE_YOUNG_PAIR
																						(BGl_optionalzd2argsze70z35zz__dssslz00
																						(BgL_bodyz00_2568,
																							BgL_restzd2statezd2_2567,
																							BgL_nozd2restzd2keyzd2statezd2_2566,
																							BgL_formalsz00_2565,
																							BgL_wherez00_2564,
																							BgL_errz00_2563,
																							BgL_keywordzd2argumentszd2_2562,
																							BgL_dssslzd2argzd2_2561,
																							BgL_restz00_1187), BNIL);
																					BgL_arg1268z00_1190 =
																						MAKE_YOUNG_PAIR(BgL_arg1272z00_1191,
																						BgL_arg1284z00_1192);
																				}
																				return
																					MAKE_YOUNG_PAIR
																					(BGl_symbol2087z00zz__dssslz00,
																					BgL_arg1268z00_1190);
																			}
																		}
																	}
																else
																	{	/* Llib/dsssl.scm 175 */
																		return
																			BGL_PROCEDURE_CALL3(BgL_errz00_2563,
																			BgL_wherez00_2564,
																			BGl_string2134z00zz__dssslz00,
																			BgL_formalsz00_2565);
																	}
															}
														else
															{	/* Llib/dsssl.scm 175 */
																return
																	BGL_PROCEDURE_CALL3(BgL_errz00_2563,
																	BgL_wherez00_2564,
																	BGl_string2134z00zz__dssslz00,
																	BgL_formalsz00_2565);
															}
													}
												else
													{	/* Llib/dsssl.scm 175 */
														return
															BGL_PROCEDURE_CALL3(BgL_errz00_2563,
															BgL_wherez00_2564, BGl_string2134z00zz__dssslz00,
															BgL_formalsz00_2565);
													}
											}
										else
											{	/* Llib/dsssl.scm 175 */
												if (SYMBOLP(BgL_ezd2112zd2_1254))
													{
														obj_t BgL_restz00_3169;
														obj_t BgL_initializa7erza7_3168;
														obj_t BgL_argz00_3167;

														BgL_argz00_3167 = BgL_ezd2112zd2_1254;
														BgL_initializa7erza7_3168 = BFALSE;
														BgL_restz00_3169 = CDR(BgL_argsz00_1229);
														BgL_restz00_1187 = BgL_restz00_3169;
														BgL_initializa7erza7_1186 =
															BgL_initializa7erza7_3168;
														BgL_argz00_1185 = BgL_argz00_3167;
														goto BgL_zc3z04anonymousza31254ze3z87_1188;
													}
												else
													{	/* Llib/dsssl.scm 175 */
														return
															BGL_PROCEDURE_CALL3(BgL_errz00_2563,
															BgL_wherez00_2564, BGl_string2134z00zz__dssslz00,
															BgL_formalsz00_2565);
													}
											}
									}
							}
						else
							{	/* Llib/dsssl.scm 161 */
								return
									BGL_PROCEDURE_CALL3(BgL_errz00_2563, BgL_wherez00_2564,
									BGl_string2134z00zz__dssslz00, BgL_formalsz00_2565);
							}
					}
			}
		}

	}



/* &rest-state */
	obj_t BGl_z62restzd2statezb0zz__dssslz00(obj_t BgL_envz00_2526,
		obj_t BgL_argsz00_2531, obj_t BgL_dssslzd2argzd2_2532)
	{
		{	/* Llib/dsssl.scm 193 */
			{	/* Llib/dsssl.scm 187 */
				obj_t BgL_errz00_2527;
				obj_t BgL_wherez00_2528;
				obj_t BgL_formalsz00_2529;
				obj_t BgL_bodyz00_2530;

				BgL_errz00_2527 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_2526, (int) (0L)));
				BgL_wherez00_2528 = PROCEDURE_L_REF(BgL_envz00_2526, (int) (1L));
				BgL_formalsz00_2529 = PROCEDURE_L_REF(BgL_envz00_2526, (int) (2L));
				BgL_bodyz00_2530 = PROCEDURE_L_REF(BgL_envz00_2526, (int) (3L));
				{
					obj_t BgL_argsz00_2633;
					obj_t BgL_dssslzd2argzd2_2634;
					obj_t BgL_argsz00_2620;
					obj_t BgL_dssslzd2argzd2_2621;

					if (PAIRP(BgL_argsz00_2531))
						{	/* Llib/dsssl.scm 189 */
							bool_t BgL_test2263z00_3194;

							{	/* Llib/dsssl.scm 189 */
								obj_t BgL_tmpz00_3195;

								BgL_tmpz00_3195 = CAR(BgL_argsz00_2531);
								BgL_test2263z00_3194 = SYMBOLP(BgL_tmpz00_3195);
							}
							if (BgL_test2263z00_3194)
								{	/* Llib/dsssl.scm 192 */
									obj_t BgL_arg1421z00_2635;

									{	/* Llib/dsssl.scm 192 */
										obj_t BgL_arg1422z00_2636;
										obj_t BgL_arg1423z00_2637;

										{	/* Llib/dsssl.scm 192 */
											obj_t BgL_arg1424z00_2638;

											{	/* Llib/dsssl.scm 192 */
												obj_t BgL_arg1425z00_2639;
												obj_t BgL_arg1426z00_2640;

												BgL_arg1425z00_2639 = CAR(BgL_argsz00_2531);
												BgL_arg1426z00_2640 =
													MAKE_YOUNG_PAIR(BgL_dssslzd2argzd2_2532, BNIL);
												BgL_arg1424z00_2638 =
													MAKE_YOUNG_PAIR(BgL_arg1425z00_2639,
													BgL_arg1426z00_2640);
											}
											BgL_arg1422z00_2636 =
												MAKE_YOUNG_PAIR(BgL_arg1424z00_2638, BNIL);
										}
										{	/* Llib/dsssl.scm 192 */
											obj_t BgL_tmpz00_3202;

											BgL_argsz00_2633 = CDR(BgL_argsz00_2531);
											BgL_dssslzd2argzd2_2634 = BgL_dssslzd2argzd2_2532;
											if (NULLP(BgL_argsz00_2633))
												{	/* Llib/dsssl.scm 197 */
													BgL_tmpz00_3202 = BgL_bodyz00_2530;
												}
											else
												{	/* Llib/dsssl.scm 197 */
													if (PAIRP(BgL_argsz00_2633))
														{	/* Llib/dsssl.scm 199 */
															if ((CAR(BgL_argsz00_2633) == (BKEY)))
																{	/* Llib/dsssl.scm 201 */
																	BgL_argsz00_2620 = CDR(BgL_argsz00_2633);
																	BgL_dssslzd2argzd2_2621 =
																		BgL_dssslzd2argzd2_2634;
																	{
																		obj_t BgL_argsz00_2623;

																		if (NULLP(BgL_argsz00_2620))
																			{	/* Llib/dsssl.scm 220 */
																				BgL_tmpz00_3202 =
																					BGL_PROCEDURE_CALL3(BgL_errz00_2527,
																					BgL_wherez00_2528,
																					BGl_string2121z00zz__dssslz00,
																					BgL_formalsz00_2529);
																			}
																		else
																			{	/* Llib/dsssl.scm 223 */
																				obj_t BgL_keysz00_2632;

																				BgL_argsz00_2623 = BgL_argsz00_2620;
																				if (NULLP(BgL_argsz00_2623))
																					{	/* Llib/dsssl.scm 209 */
																						BgL_keysz00_2632 = BNIL;
																					}
																				else
																					{	/* Llib/dsssl.scm 209 */
																						obj_t BgL_head1078z00_2624;

																						BgL_head1078z00_2624 =
																							MAKE_YOUNG_PAIR(BNIL, BNIL);
																						{
																							obj_t BgL_l1076z00_2626;
																							obj_t BgL_tail1079z00_2627;

																							BgL_l1076z00_2626 =
																								BgL_argsz00_2623;
																							BgL_tail1079z00_2627 =
																								BgL_head1078z00_2624;
																						BgL_zc3z04anonymousza31443ze3z87_2625:
																							if (NULLP
																								(BgL_l1076z00_2626))
																								{	/* Llib/dsssl.scm 209 */
																									BgL_keysz00_2632 =
																										CDR(BgL_head1078z00_2624);
																								}
																							else
																								{	/* Llib/dsssl.scm 209 */
																									obj_t BgL_newtail1080z00_2628;

																									{	/* Llib/dsssl.scm 209 */
																										obj_t BgL_arg1446z00_2629;

																										{	/* Llib/dsssl.scm 209 */
																											obj_t BgL_xz00_2630;

																											BgL_xz00_2630 =
																												CAR(
																												((obj_t)
																													BgL_l1076z00_2626));
																											{	/* Llib/dsssl.scm 211 */
																												bool_t
																													BgL_test2270z00_3226;
																												if (PAIRP
																													(BgL_xz00_2630))
																													{	/* Llib/dsssl.scm 211 */
																														obj_t
																															BgL_tmpz00_3229;
																														BgL_tmpz00_3229 =
																															CAR
																															(BgL_xz00_2630);
																														BgL_test2270z00_3226
																															=
																															SYMBOLP
																															(BgL_tmpz00_3229);
																													}
																												else
																													{	/* Llib/dsssl.scm 211 */
																														BgL_test2270z00_3226
																															= ((bool_t) 0);
																													}
																												if (BgL_test2270z00_3226)
																													{	/* Llib/dsssl.scm 211 */
																														BgL_arg1446z00_2629
																															=
																															BGl_symbolzd2ze3keywordz31zz__r4_symbols_6_4z00
																															(CAR
																															(BgL_xz00_2630));
																													}
																												else
																													{	/* Llib/dsssl.scm 211 */
																														if (SYMBOLP
																															(BgL_xz00_2630))
																															{	/* Llib/dsssl.scm 213 */
																																BgL_arg1446z00_2629
																																	=
																																	BGl_symbolzd2ze3keywordz31zz__r4_symbols_6_4z00
																																	(BgL_xz00_2630);
																															}
																														else
																															{	/* Llib/dsssl.scm 213 */
																																BgL_arg1446z00_2629
																																	=
																																	BGL_PROCEDURE_CALL3
																																	(BgL_errz00_2527,
																																	BgL_wherez00_2528,
																																	BGl_string2135z00zz__dssslz00,
																																	BgL_formalsz00_2529);
																															}
																													}
																											}
																										}
																										BgL_newtail1080z00_2628 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1446z00_2629,
																											BNIL);
																									}
																									SET_CDR(BgL_tail1079z00_2627,
																										BgL_newtail1080z00_2628);
																									{	/* Llib/dsssl.scm 209 */
																										obj_t BgL_arg1445z00_2631;

																										BgL_arg1445z00_2631 =
																											CDR(
																											((obj_t)
																												BgL_l1076z00_2626));
																										{
																											obj_t
																												BgL_tail1079z00_3248;
																											obj_t BgL_l1076z00_3247;

																											BgL_l1076z00_3247 =
																												BgL_arg1445z00_2631;
																											BgL_tail1079z00_3248 =
																												BgL_newtail1080z00_2628;
																											BgL_tail1079z00_2627 =
																												BgL_tail1079z00_3248;
																											BgL_l1076z00_2626 =
																												BgL_l1076z00_3247;
																											goto
																												BgL_zc3z04anonymousza31443ze3z87_2625;
																										}
																									}
																								}
																						}
																					}
																				if (NULLP(BgL_keysz00_2632))
																					{	/* Llib/dsssl.scm 224 */
																						BgL_tmpz00_3202 =
																							BGL_PROCEDURE_CALL3
																							(BgL_errz00_2527,
																							BgL_wherez00_2528,
																							BGl_string2121z00zz__dssslz00,
																							BgL_formalsz00_2529);
																					}
																				else
																					{	/* Llib/dsssl.scm 224 */
																						BgL_tmpz00_3202 =
																							BGl_z62keyzd2statezb0zz__dssslz00
																							(BgL_bodyz00_2530,
																							BgL_formalsz00_2529,
																							BgL_wherez00_2528,
																							BgL_errz00_2527, BgL_argsz00_2620,
																							BgL_dssslzd2argzd2_2621, BNIL,
																							((bool_t) 0));
																					}
																			}
																	}
																}
															else
																{	/* Llib/dsssl.scm 201 */
																	BgL_tmpz00_3202 =
																		BGL_PROCEDURE_CALL3(BgL_errz00_2527,
																		BgL_wherez00_2528,
																		BGl_string2120z00zz__dssslz00,
																		BgL_formalsz00_2529);
																}
														}
													else
														{	/* Llib/dsssl.scm 199 */
															BgL_tmpz00_3202 =
																BGL_PROCEDURE_CALL3(BgL_errz00_2527,
																BgL_wherez00_2528,
																BGl_string2120z00zz__dssslz00,
																BgL_formalsz00_2529);
														}
												}
											BgL_arg1423z00_2637 =
												MAKE_YOUNG_PAIR(BgL_tmpz00_3202, BNIL);
										}
										BgL_arg1421z00_2635 =
											MAKE_YOUNG_PAIR(BgL_arg1422z00_2636, BgL_arg1423z00_2637);
									}
									return
										MAKE_YOUNG_PAIR(BGl_symbol2087z00zz__dssslz00,
										BgL_arg1421z00_2635);
								}
							else
								{	/* Llib/dsssl.scm 189 */
									return
										BGL_PROCEDURE_CALL3(BgL_errz00_2527, BgL_wherez00_2528,
										BGl_string2120z00zz__dssslz00, BgL_formalsz00_2529);
								}
						}
					else
						{	/* Llib/dsssl.scm 187 */
							return
								BGL_PROCEDURE_CALL3(BgL_errz00_2527, BgL_wherez00_2528,
								BGl_string2120z00zz__dssslz00, BgL_formalsz00_2529);
						}
				}
			}
		}

	}



/* &no-rest-key-state */
	obj_t BGl_z62nozd2restzd2keyzd2statezb0zz__dssslz00(obj_t BgL_envz00_2533,
		obj_t BgL_argsz00_2538, obj_t BgL_dssslzd2argzd2_2539)
	{
		{	/* Llib/dsssl.scm 245 */
			{	/* Llib/dsssl.scm 231 */
				obj_t BgL_bodyz00_2534;
				obj_t BgL_errz00_2535;
				obj_t BgL_wherez00_2536;
				obj_t BgL_formalsz00_2537;

				BgL_bodyz00_2534 = PROCEDURE_L_REF(BgL_envz00_2533, (int) (0L));
				BgL_errz00_2535 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_2533, (int) (1L)));
				BgL_wherez00_2536 = PROCEDURE_L_REF(BgL_envz00_2533, (int) (2L));
				BgL_formalsz00_2537 = PROCEDURE_L_REF(BgL_envz00_2533, (int) (3L));
				{
					obj_t BgL_argsz00_2643;

					if (NULLP(BgL_argsz00_2538))
						{	/* Llib/dsssl.scm 248 */
							return
								BGL_PROCEDURE_CALL3(BgL_errz00_2535, BgL_wherez00_2536,
								BGl_string2121z00zz__dssslz00, BgL_formalsz00_2537);
						}
					else
						{	/* Llib/dsssl.scm 251 */
							obj_t BgL_keysz00_2654;

							BgL_argsz00_2643 = BgL_argsz00_2538;
							{
								obj_t BgL_argsz00_2645;
								obj_t BgL_auxz00_2646;

								BgL_argsz00_2645 = BgL_argsz00_2643;
								BgL_auxz00_2646 = BNIL;
							BgL_loopz00_2644:
								if (NULLP(BgL_argsz00_2645))
									{	/* Llib/dsssl.scm 234 */
										BgL_keysz00_2654 = bgl_reverse_bang(BgL_auxz00_2646);
									}
								else
									{	/* Llib/dsssl.scm 234 */
										if ((CAR(((obj_t) BgL_argsz00_2645)) == (BREST)))
											{	/* Llib/dsssl.scm 236 */
												BgL_keysz00_2654 = bgl_reverse_bang(BgL_auxz00_2646);
											}
										else
											{	/* Llib/dsssl.scm 239 */
												obj_t BgL_ezd2145zd2_2647;

												BgL_ezd2145zd2_2647 = CAR(((obj_t) BgL_argsz00_2645));
												if (SYMBOLP(BgL_ezd2145zd2_2647))
													{	/* Llib/dsssl.scm 241 */
														obj_t BgL_arg1468z00_2648;
														obj_t BgL_arg1469z00_2649;

														BgL_arg1468z00_2648 =
															CDR(((obj_t) BgL_argsz00_2645));
														BgL_arg1469z00_2649 =
															MAKE_YOUNG_PAIR
															(BGl_symbolzd2ze3keywordz31zz__r4_symbols_6_4z00
															(BgL_ezd2145zd2_2647), BgL_auxz00_2646);
														{
															obj_t BgL_auxz00_3321;
															obj_t BgL_argsz00_3320;

															BgL_argsz00_3320 = BgL_arg1468z00_2648;
															BgL_auxz00_3321 = BgL_arg1469z00_2649;
															BgL_auxz00_2646 = BgL_auxz00_3321;
															BgL_argsz00_2645 = BgL_argsz00_3320;
															goto BgL_loopz00_2644;
														}
													}
												else
													{	/* Llib/dsssl.scm 239 */
														if (PAIRP(BgL_ezd2145zd2_2647))
															{	/* Llib/dsssl.scm 239 */
																obj_t BgL_carzd2152zd2_2650;
																obj_t BgL_cdrzd2153zd2_2651;

																BgL_carzd2152zd2_2650 =
																	CAR(BgL_ezd2145zd2_2647);
																BgL_cdrzd2153zd2_2651 =
																	CDR(BgL_ezd2145zd2_2647);
																if (SYMBOLP(BgL_carzd2152zd2_2650))
																	{	/* Llib/dsssl.scm 239 */
																		if (PAIRP(BgL_cdrzd2153zd2_2651))
																			{	/* Llib/dsssl.scm 239 */
																				if (NULLP(CDR(BgL_cdrzd2153zd2_2651)))
																					{	/* Llib/dsssl.scm 243 */
																						obj_t BgL_arg1473z00_2652;
																						obj_t BgL_arg1474z00_2653;

																						BgL_arg1473z00_2652 =
																							CDR(((obj_t) BgL_argsz00_2645));
																						BgL_arg1474z00_2653 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbolzd2ze3keywordz31zz__r4_symbols_6_4z00
																							(BgL_carzd2152zd2_2650),
																							BgL_auxz00_2646);
																						{
																							obj_t BgL_auxz00_3338;
																							obj_t BgL_argsz00_3337;

																							BgL_argsz00_3337 =
																								BgL_arg1473z00_2652;
																							BgL_auxz00_3338 =
																								BgL_arg1474z00_2653;
																							BgL_auxz00_2646 = BgL_auxz00_3338;
																							BgL_argsz00_2645 =
																								BgL_argsz00_3337;
																							goto BgL_loopz00_2644;
																						}
																					}
																				else
																					{	/* Llib/dsssl.scm 239 */
																						BgL_keysz00_2654 =
																							BGL_PROCEDURE_CALL3
																							(BgL_errz00_2535,
																							BgL_wherez00_2536,
																							BGl_string2121z00zz__dssslz00,
																							BgL_formalsz00_2537);
																					}
																			}
																		else
																			{	/* Llib/dsssl.scm 239 */
																				BgL_keysz00_2654 =
																					BGL_PROCEDURE_CALL3(BgL_errz00_2535,
																					BgL_wherez00_2536,
																					BGl_string2121z00zz__dssslz00,
																					BgL_formalsz00_2537);
																			}
																	}
																else
																	{	/* Llib/dsssl.scm 239 */
																		BgL_keysz00_2654 =
																			BGL_PROCEDURE_CALL3(BgL_errz00_2535,
																			BgL_wherez00_2536,
																			BGl_string2121z00zz__dssslz00,
																			BgL_formalsz00_2537);
																	}
															}
														else
															{	/* Llib/dsssl.scm 239 */
																BgL_keysz00_2654 =
																	BGL_PROCEDURE_CALL3(BgL_errz00_2535,
																	BgL_wherez00_2536,
																	BGl_string2121z00zz__dssslz00,
																	BgL_formalsz00_2537);
															}
													}
											}
									}
							}
							if (NULLP(BgL_keysz00_2654))
								{	/* Llib/dsssl.scm 252 */
									return
										BGL_PROCEDURE_CALL3(BgL_errz00_2535, BgL_wherez00_2536,
										BGl_string2121z00zz__dssslz00, BgL_formalsz00_2537);
								}
							else
								{	/* Llib/dsssl.scm 252 */
									return
										BGl_z62keyzd2statezb0zz__dssslz00(BgL_bodyz00_2534,
										BgL_formalsz00_2537, BgL_wherez00_2536, BgL_errz00_2535,
										BgL_argsz00_2538, BgL_dssslzd2argzd2_2539, BNIL,
										((bool_t) 1));
								}
						}
				}
			}
		}

	}



/* dsssl-check-key-args! */
	BGL_EXPORTED_DEF obj_t BGl_dssslzd2checkzd2keyzd2argsz12zc0zz__dssslz00(obj_t
		BgL_dssslzd2argszd2_8, obj_t BgL_keyzd2listzd2_9)
	{
		{	/* Llib/dsssl.scm 316 */
			if (NULLP(BgL_keyzd2listzd2_9))
				{
					obj_t BgL_argsz00_1556;

					BgL_argsz00_1556 = BgL_dssslzd2argszd2_8;
				BgL_zc3z04anonymousza31607ze3z87_1557:
					if (NULLP(BgL_argsz00_1556))
						{	/* Llib/dsssl.scm 320 */
							return BgL_dssslzd2argszd2_8;
						}
					else
						{	/* Llib/dsssl.scm 322 */
							bool_t BgL_test2285z00_3376;

							if (PAIRP(BgL_argsz00_1556))
								{	/* Llib/dsssl.scm 322 */
									if (NULLP(CDR(BgL_argsz00_1556)))
										{	/* Llib/dsssl.scm 323 */
											BgL_test2285z00_3376 = ((bool_t) 1);
										}
									else
										{	/* Llib/dsssl.scm 324 */
											bool_t BgL_test2288z00_3382;

											{	/* Llib/dsssl.scm 324 */
												obj_t BgL_tmpz00_3383;

												BgL_tmpz00_3383 = CAR(BgL_argsz00_1556);
												BgL_test2288z00_3382 = KEYWORDP(BgL_tmpz00_3383);
											}
											if (BgL_test2288z00_3382)
												{	/* Llib/dsssl.scm 324 */
													BgL_test2285z00_3376 = ((bool_t) 0);
												}
											else
												{	/* Llib/dsssl.scm 324 */
													BgL_test2285z00_3376 = ((bool_t) 1);
												}
										}
								}
							else
								{	/* Llib/dsssl.scm 322 */
									BgL_test2285z00_3376 = ((bool_t) 1);
								}
							if (BgL_test2285z00_3376)
								{	/* Llib/dsssl.scm 322 */
									return
										BGl_errorz00zz__errorz00(BGl_string2136z00zz__dssslz00,
										BGl_string2137z00zz__dssslz00, BgL_argsz00_1556);
								}
							else
								{	/* Llib/dsssl.scm 329 */
									obj_t BgL_arg1618z00_1567;

									{	/* Llib/dsssl.scm 329 */
										obj_t BgL_pairz00_2242;

										BgL_pairz00_2242 = CDR(((obj_t) BgL_argsz00_1556));
										BgL_arg1618z00_1567 = CDR(BgL_pairz00_2242);
									}
									{
										obj_t BgL_argsz00_3390;

										BgL_argsz00_3390 = BgL_arg1618z00_1567;
										BgL_argsz00_1556 = BgL_argsz00_3390;
										goto BgL_zc3z04anonymousza31607ze3z87_1557;
									}
								}
						}
				}
			else
				{
					obj_t BgL_argsz00_1576;
					bool_t BgL_armedz00_1577;
					obj_t BgL_optsz00_1578;

					BgL_argsz00_1576 = BgL_dssslzd2argszd2_8;
					BgL_armedz00_1577 = ((bool_t) 0);
					BgL_optsz00_1578 = BNIL;
				BgL_zc3z04anonymousza31621ze3z87_1579:
					if (NULLP(BgL_argsz00_1576))
						{	/* Llib/dsssl.scm 334 */
							return bgl_reverse_bang(BgL_optsz00_1578);
						}
					else
						{	/* Llib/dsssl.scm 336 */
							bool_t BgL_test2290z00_3394;

							if (PAIRP(BgL_argsz00_1576))
								{	/* Llib/dsssl.scm 336 */
									if (NULLP(CDR(BgL_argsz00_1576)))
										{	/* Llib/dsssl.scm 337 */
											BgL_test2290z00_3394 = ((bool_t) 1);
										}
									else
										{	/* Llib/dsssl.scm 338 */
											bool_t BgL_test2293z00_3400;

											{	/* Llib/dsssl.scm 338 */
												obj_t BgL_tmpz00_3401;

												BgL_tmpz00_3401 = CAR(BgL_argsz00_1576);
												BgL_test2293z00_3400 = KEYWORDP(BgL_tmpz00_3401);
											}
											if (BgL_test2293z00_3400)
												{	/* Llib/dsssl.scm 338 */
													if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CAR
																(BgL_argsz00_1576), BgL_keyzd2listzd2_9)))
														{	/* Llib/dsssl.scm 339 */
															BgL_test2290z00_3394 = ((bool_t) 0);
														}
													else
														{	/* Llib/dsssl.scm 339 */
															BgL_test2290z00_3394 = ((bool_t) 1);
														}
												}
											else
												{	/* Llib/dsssl.scm 338 */
													BgL_test2290z00_3394 = ((bool_t) 1);
												}
										}
								}
							else
								{	/* Llib/dsssl.scm 336 */
									BgL_test2290z00_3394 = ((bool_t) 1);
								}
							if (BgL_test2290z00_3394)
								{	/* Llib/dsssl.scm 336 */
									if (BgL_armedz00_1577)
										{	/* Llib/dsssl.scm 342 */
											obj_t BgL_arg1636z00_1592;
											obj_t BgL_arg1637z00_1593;

											BgL_arg1636z00_1592 = CDR(((obj_t) BgL_argsz00_1576));
											{	/* Llib/dsssl.scm 344 */
												obj_t BgL_arg1638z00_1594;

												BgL_arg1638z00_1594 = CAR(((obj_t) BgL_argsz00_1576));
												BgL_arg1637z00_1593 =
													MAKE_YOUNG_PAIR(BgL_arg1638z00_1594,
													BgL_optsz00_1578);
											}
											{
												obj_t BgL_optsz00_3416;
												bool_t BgL_armedz00_3415;
												obj_t BgL_argsz00_3414;

												BgL_argsz00_3414 = BgL_arg1636z00_1592;
												BgL_armedz00_3415 = ((bool_t) 0);
												BgL_optsz00_3416 = BgL_arg1637z00_1593;
												BgL_optsz00_1578 = BgL_optsz00_3416;
												BgL_armedz00_1577 = BgL_armedz00_3415;
												BgL_argsz00_1576 = BgL_argsz00_3414;
												goto BgL_zc3z04anonymousza31621ze3z87_1579;
											}
										}
									else
										{	/* Llib/dsssl.scm 341 */
											obj_t BgL_arg1639z00_1595;

											BgL_arg1639z00_1595 = CDR(((obj_t) BgL_argsz00_1576));
											{
												obj_t BgL_argsz00_3419;

												BgL_argsz00_3419 = BgL_arg1639z00_1595;
												BgL_argsz00_1576 = BgL_argsz00_3419;
												goto BgL_zc3z04anonymousza31621ze3z87_1579;
											}
										}
								}
							else
								{	/* Llib/dsssl.scm 346 */
									obj_t BgL_arg1640z00_1596;

									{	/* Llib/dsssl.scm 346 */
										obj_t BgL_pairz00_2252;

										BgL_pairz00_2252 = CDR(((obj_t) BgL_argsz00_1576));
										BgL_arg1640z00_1596 = CDR(BgL_pairz00_2252);
									}
									{
										bool_t BgL_armedz00_3424;
										obj_t BgL_argsz00_3423;

										BgL_argsz00_3423 = BgL_arg1640z00_1596;
										BgL_armedz00_3424 = ((bool_t) 1);
										BgL_armedz00_1577 = BgL_armedz00_3424;
										BgL_argsz00_1576 = BgL_argsz00_3423;
										goto BgL_zc3z04anonymousza31621ze3z87_1579;
									}
								}
						}
				}
		}

	}



/* &dsssl-check-key-args! */
	obj_t BGl_z62dssslzd2checkzd2keyzd2argsz12za2zz__dssslz00(obj_t
		BgL_envz00_2540, obj_t BgL_dssslzd2argszd2_2541,
		obj_t BgL_keyzd2listzd2_2542)
	{
		{	/* Llib/dsssl.scm 316 */
			return
				BGl_dssslzd2checkzd2keyzd2argsz12zc0zz__dssslz00
				(BgL_dssslzd2argszd2_2541, BgL_keyzd2listzd2_2542);
		}

	}



/* dsssl-get-key-arg */
	BGL_EXPORTED_DEF obj_t BGl_dssslzd2getzd2keyzd2argzd2zz__dssslz00(obj_t
		BgL_dssslzd2argszd2_10, obj_t BgL_keywordz00_11,
		obj_t BgL_initializa7erza7_12)
	{
		{	/* Llib/dsssl.scm 354 */
			{
				obj_t BgL_argsz00_1606;

				BgL_argsz00_1606 = BgL_dssslzd2argszd2_10;
			BgL_zc3z04anonymousza31644ze3z87_1607:
				if (PAIRP(BgL_argsz00_1606))
					{	/* Llib/dsssl.scm 362 */
						bool_t BgL_test2297z00_3428;

						{	/* Llib/dsssl.scm 362 */
							obj_t BgL_tmpz00_3429;

							BgL_tmpz00_3429 = CAR(BgL_argsz00_1606);
							BgL_test2297z00_3428 = KEYWORDP(BgL_tmpz00_3429);
						}
						if (BgL_test2297z00_3428)
							{	/* Llib/dsssl.scm 362 */
								if ((CAR(BgL_argsz00_1606) == BgL_keywordz00_11))
									{	/* Llib/dsssl.scm 365 */
										bool_t BgL_test2299z00_3435;

										{	/* Llib/dsssl.scm 365 */
											obj_t BgL_tmpz00_3436;

											BgL_tmpz00_3436 = CDR(BgL_argsz00_1606);
											BgL_test2299z00_3435 = PAIRP(BgL_tmpz00_3436);
										}
										if (BgL_test2299z00_3435)
											{	/* Llib/dsssl.scm 365 */
												return CAR(CDR(BgL_argsz00_1606));
											}
										else
											{	/* Llib/dsssl.scm 365 */
												return
													BGl_errorz00zz__errorz00
													(BGl_string2099z00zz__dssslz00,
													BGl_string2138z00zz__dssslz00, CAR(BgL_argsz00_1606));
											}
									}
								else
									{	/* Llib/dsssl.scm 370 */
										bool_t BgL_test2300z00_3443;

										{	/* Llib/dsssl.scm 370 */
											obj_t BgL_tmpz00_3444;

											BgL_tmpz00_3444 = CAR(BgL_argsz00_1606);
											BgL_test2300z00_3443 = KEYWORDP(BgL_tmpz00_3444);
										}
										if (BgL_test2300z00_3443)
											{	/* Llib/dsssl.scm 375 */
												bool_t BgL_test2301z00_3447;

												{	/* Llib/dsssl.scm 375 */
													obj_t BgL_tmpz00_3448;

													BgL_tmpz00_3448 = CDR(BgL_argsz00_1606);
													BgL_test2301z00_3447 = PAIRP(BgL_tmpz00_3448);
												}
												if (BgL_test2301z00_3447)
													{
														obj_t BgL_argsz00_3451;

														BgL_argsz00_3451 = CDR(CDR(BgL_argsz00_1606));
														BgL_argsz00_1606 = BgL_argsz00_3451;
														goto BgL_zc3z04anonymousza31644ze3z87_1607;
													}
												else
													{	/* Llib/dsssl.scm 375 */
														return
															BGl_errorz00zz__errorz00
															(BGl_string2099z00zz__dssslz00,
															BGl_string2138z00zz__dssslz00,
															CAR(BgL_argsz00_1606));
													}
											}
										else
											{	/* Llib/dsssl.scm 370 */
												return
													BGl_errorz00zz__errorz00
													(BGl_string2099z00zz__dssslz00,
													BGl_string2139z00zz__dssslz00, CAR(BgL_argsz00_1606));
											}
									}
							}
						else
							{
								obj_t BgL_argsz00_3458;

								BgL_argsz00_3458 = CDR(BgL_argsz00_1606);
								BgL_argsz00_1606 = BgL_argsz00_3458;
								goto BgL_zc3z04anonymousza31644ze3z87_1607;
							}
					}
				else
					{	/* Llib/dsssl.scm 357 */
						if (NULLP(BgL_argsz00_1606))
							{	/* Llib/dsssl.scm 358 */
								return BgL_initializa7erza7_12;
							}
						else
							{	/* Llib/dsssl.scm 358 */
								return
									BGl_errorz00zz__errorz00(BGl_string2099z00zz__dssslz00,
									BGl_string2140z00zz__dssslz00, BgL_dssslzd2argszd2_10);
							}
					}
			}
		}

	}



/* &dsssl-get-key-arg */
	obj_t BGl_z62dssslzd2getzd2keyzd2argzb0zz__dssslz00(obj_t BgL_envz00_2543,
		obj_t BgL_dssslzd2argszd2_2544, obj_t BgL_keywordz00_2545,
		obj_t BgL_initializa7erza7_2546)
	{
		{	/* Llib/dsssl.scm 354 */
			{	/* Llib/dsssl.scm 357 */
				obj_t BgL_auxz00_3463;

				if (KEYWORDP(BgL_keywordz00_2545))
					{	/* Llib/dsssl.scm 357 */
						BgL_auxz00_3463 = BgL_keywordz00_2545;
					}
				else
					{
						obj_t BgL_auxz00_3466;

						BgL_auxz00_3466 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2091z00zz__dssslz00,
							BINT(12351L), BGl_string2141z00zz__dssslz00,
							BGl_string2142z00zz__dssslz00, BgL_keywordz00_2545);
						FAILURE(BgL_auxz00_3466, BFALSE, BFALSE);
					}
				return
					BGl_dssslzd2getzd2keyzd2argzd2zz__dssslz00(BgL_dssslzd2argszd2_2544,
					BgL_auxz00_3463, BgL_initializa7erza7_2546);
			}
		}

	}



/* dsssl-get-key-rest-arg */
	BGL_EXPORTED_DEF obj_t BGl_dssslzd2getzd2keyzd2restzd2argz00zz__dssslz00(obj_t
		BgL_dssslzd2argszd2_13, obj_t BgL_keysz00_14)
	{
		{	/* Llib/dsssl.scm 384 */
			return
				BGl_loopze71ze7zz__dssslz00(BgL_keysz00_14, BgL_dssslzd2argszd2_13);
		}

	}



/* loop~1 */
	obj_t BGl_loopze71ze7zz__dssslz00(obj_t BgL_keysz00_2560,
		obj_t BgL_argsz00_1632)
	{
		{	/* Llib/dsssl.scm 385 */
		BGl_loopze71ze7zz__dssslz00:
			if (NULLP(BgL_argsz00_1632))
				{	/* Llib/dsssl.scm 387 */
					return BNIL;
				}
			else
				{	/* Llib/dsssl.scm 389 */
					bool_t BgL_test2305z00_3474;

					{	/* Llib/dsssl.scm 389 */
						bool_t BgL_test2306z00_3475;

						{	/* Llib/dsssl.scm 389 */
							obj_t BgL_tmpz00_3476;

							BgL_tmpz00_3476 = CAR(((obj_t) BgL_argsz00_1632));
							BgL_test2306z00_3475 = KEYWORDP(BgL_tmpz00_3476);
						}
						if (BgL_test2306z00_3475)
							{	/* Llib/dsssl.scm 389 */
								if (NULLP(CDR(((obj_t) BgL_argsz00_1632))))
									{	/* Llib/dsssl.scm 390 */
										BgL_test2305z00_3474 = ((bool_t) 1);
									}
								else
									{	/* Llib/dsssl.scm 390 */
										if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CAR(
														((obj_t) BgL_argsz00_1632)), BgL_keysz00_2560)))
											{	/* Llib/dsssl.scm 391 */
												BgL_test2305z00_3474 = ((bool_t) 0);
											}
										else
											{	/* Llib/dsssl.scm 391 */
												BgL_test2305z00_3474 = ((bool_t) 1);
											}
									}
							}
						else
							{	/* Llib/dsssl.scm 389 */
								BgL_test2305z00_3474 = ((bool_t) 1);
							}
					}
					if (BgL_test2305z00_3474)
						{	/* Llib/dsssl.scm 392 */
							obj_t BgL_arg1691z00_1645;
							obj_t BgL_arg1692z00_1646;

							BgL_arg1691z00_1645 = CAR(((obj_t) BgL_argsz00_1632));
							{	/* Llib/dsssl.scm 392 */
								obj_t BgL_arg1699z00_1647;

								BgL_arg1699z00_1647 = CDR(((obj_t) BgL_argsz00_1632));
								BgL_arg1692z00_1646 =
									BGl_loopze71ze7zz__dssslz00(BgL_keysz00_2560,
									BgL_arg1699z00_1647);
							}
							return MAKE_YOUNG_PAIR(BgL_arg1691z00_1645, BgL_arg1692z00_1646);
						}
					else
						{	/* Llib/dsssl.scm 394 */
							obj_t BgL_arg1700z00_1648;

							{	/* Llib/dsssl.scm 394 */
								obj_t BgL_pairz00_2278;

								BgL_pairz00_2278 = CDR(((obj_t) BgL_argsz00_1632));
								BgL_arg1700z00_1648 = CDR(BgL_pairz00_2278);
							}
							{
								obj_t BgL_argsz00_3498;

								BgL_argsz00_3498 = BgL_arg1700z00_1648;
								BgL_argsz00_1632 = BgL_argsz00_3498;
								goto BGl_loopze71ze7zz__dssslz00;
							}
						}
				}
		}

	}



/* &dsssl-get-key-rest-arg */
	obj_t BGl_z62dssslzd2getzd2keyzd2restzd2argz62zz__dssslz00(obj_t
		BgL_envz00_2547, obj_t BgL_dssslzd2argszd2_2548, obj_t BgL_keysz00_2549)
	{
		{	/* Llib/dsssl.scm 384 */
			{	/* Llib/dsssl.scm 387 */
				obj_t BgL_auxz00_3499;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_keysz00_2549))
					{	/* Llib/dsssl.scm 387 */
						BgL_auxz00_3499 = BgL_keysz00_2549;
					}
				else
					{
						obj_t BgL_auxz00_3502;

						BgL_auxz00_3502 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2091z00zz__dssslz00,
							BINT(13315L), BGl_string2143z00zz__dssslz00,
							BGl_string2144z00zz__dssslz00, BgL_keysz00_2549);
						FAILURE(BgL_auxz00_3502, BFALSE, BFALSE);
					}
				return
					BGl_dssslzd2getzd2keyzd2restzd2argz00zz__dssslz00
					(BgL_dssslzd2argszd2_2548, BgL_auxz00_3499);
			}
		}

	}



/* id-sans-type */
	obj_t BGl_idzd2sanszd2typez00zz__dssslz00(obj_t BgL_idz00_15)
	{
		{	/* Llib/dsssl.scm 402 */
			{	/* Llib/dsssl.scm 403 */
				obj_t BgL_stringz00_1656;

				{	/* Llib/dsssl.scm 403 */
					obj_t BgL_arg1758z00_2280;

					BgL_arg1758z00_2280 = SYMBOL_TO_STRING(BgL_idz00_15);
					BgL_stringz00_1656 =
						BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1758z00_2280);
				}
				{	/* Llib/dsssl.scm 403 */
					long BgL_lenz00_1657;

					BgL_lenz00_1657 = STRING_LENGTH(BgL_stringz00_1656);
					{	/* Llib/dsssl.scm 404 */

						{
							long BgL_walkerz00_1659;

							BgL_walkerz00_1659 = 0L;
						BgL_zc3z04anonymousza31704ze3z87_1660:
							if ((BgL_walkerz00_1659 == BgL_lenz00_1657))
								{	/* Llib/dsssl.scm 407 */
									return BgL_idz00_15;
								}
							else
								{	/* Llib/dsssl.scm 409 */
									bool_t BgL_test2311z00_3512;

									if (
										(STRING_REF(BgL_stringz00_1656,
												BgL_walkerz00_1659) == ((unsigned char) ':')))
										{	/* Llib/dsssl.scm 409 */
											if ((BgL_walkerz00_1659 < (BgL_lenz00_1657 - 1L)))
												{	/* Llib/dsssl.scm 410 */
													BgL_test2311z00_3512 =
														(STRING_REF(BgL_stringz00_1656,
															(BgL_walkerz00_1659 + 1L)) ==
														((unsigned char) ':'));
												}
											else
												{	/* Llib/dsssl.scm 410 */
													BgL_test2311z00_3512 = ((bool_t) 0);
												}
										}
									else
										{	/* Llib/dsssl.scm 409 */
											BgL_test2311z00_3512 = ((bool_t) 0);
										}
									if (BgL_test2311z00_3512)
										{	/* Llib/dsssl.scm 409 */
											return
												bstring_to_symbol(c_substring(BgL_stringz00_1656, 0L,
													BgL_walkerz00_1659));
										}
									else
										{
											long BgL_walkerz00_3524;

											BgL_walkerz00_3524 = (BgL_walkerz00_1659 + 1L);
											BgL_walkerz00_1659 = BgL_walkerz00_3524;
											goto BgL_zc3z04anonymousza31704ze3z87_1660;
										}
								}
						}
					}
				}
			}
		}

	}



/* dsssl-formals->scheme-formals */
	BGL_EXPORTED_DEF obj_t
		BGl_dssslzd2formalszd2ze3schemezd2formalsz31zz__dssslz00(obj_t
		BgL_formalsz00_16, obj_t BgL_errz00_17)
	{
		{	/* Llib/dsssl.scm 419 */
			return
				BGl_dssslzd2formalszd2ze3schemezd2typedzd2formalsze3zz__dssslz00
				(BgL_formalsz00_16, BgL_errz00_17, ((bool_t) 0));
		}

	}



/* &dsssl-formals->scheme-formals */
	obj_t BGl_z62dssslzd2formalszd2ze3schemezd2formalsz53zz__dssslz00(obj_t
		BgL_envz00_2550, obj_t BgL_formalsz00_2551, obj_t BgL_errz00_2552)
	{
		{	/* Llib/dsssl.scm 419 */
			{	/* Llib/dsssl.scm 420 */
				obj_t BgL_auxz00_3527;

				if (PROCEDUREP(BgL_errz00_2552))
					{	/* Llib/dsssl.scm 420 */
						BgL_auxz00_3527 = BgL_errz00_2552;
					}
				else
					{
						obj_t BgL_auxz00_3530;

						BgL_auxz00_3530 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2091z00zz__dssslz00,
							BINT(14652L), BGl_string2145z00zz__dssslz00,
							BGl_string2093z00zz__dssslz00, BgL_errz00_2552);
						FAILURE(BgL_auxz00_3530, BFALSE, BFALSE);
					}
				return
					BGl_dssslzd2formalszd2ze3schemezd2formalsz31zz__dssslz00
					(BgL_formalsz00_2551, BgL_auxz00_3527);
			}
		}

	}



/* dsssl-formals->scheme-typed-formals */
	BGL_EXPORTED_DEF obj_t
		BGl_dssslzd2formalszd2ze3schemezd2typedzd2formalsze3zz__dssslz00(obj_t
		BgL_formalsz00_18, obj_t BgL_errz00_19, bool_t BgL_typedz00_20)
	{
		{	/* Llib/dsssl.scm 436 */
			return
				BGl_loopze70ze7zz__dssslz00(BgL_typedz00_20, BgL_formalsz00_18,
				BgL_errz00_19, BgL_formalsz00_18, ((bool_t) 0));
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zz__dssslz00(bool_t BgL_typedz00_2559,
		obj_t BgL_formalsz00_2558, obj_t BgL_errz00_2557, obj_t BgL_argsz00_1684,
		bool_t BgL_dssslz00_1685)
	{
		{	/* Llib/dsssl.scm 449 */
		BGl_loopze70ze7zz__dssslz00:
			{
				obj_t BgL_objz00_1710;

				if (NULLP(BgL_argsz00_1684))
					{	/* Llib/dsssl.scm 452 */
						return BNIL;
					}
				else
					{	/* Llib/dsssl.scm 452 */
						if (PAIRP(BgL_argsz00_1684))
							{	/* Llib/dsssl.scm 464 */
								bool_t BgL_test2317z00_3540;

								{	/* Llib/dsssl.scm 464 */
									obj_t BgL_tmpz00_3541;

									BgL_tmpz00_3541 = CAR(BgL_argsz00_1684);
									BgL_test2317z00_3540 = SYMBOLP(BgL_tmpz00_3541);
								}
								if (BgL_test2317z00_3540)
									{	/* Llib/dsssl.scm 464 */
										if (BgL_dssslz00_1685)
											{	/* Llib/dsssl.scm 476 */
												return
													BGl_idzd2sanszd2typez00zz__dssslz00(CAR
													(BgL_argsz00_1684));
											}
										else
											{	/* Llib/dsssl.scm 479 */
												obj_t BgL_arg1731z00_1692;
												obj_t BgL_arg1733z00_1693;

												if (BgL_typedz00_2559)
													{	/* Llib/dsssl.scm 479 */
														BgL_arg1731z00_1692 = CAR(BgL_argsz00_1684);
													}
												else
													{	/* Llib/dsssl.scm 479 */
														BgL_arg1731z00_1692 =
															BGl_idzd2sanszd2typez00zz__dssslz00(CAR
															(BgL_argsz00_1684));
													}
												BgL_arg1733z00_1693 =
													BGl_loopze70ze7zz__dssslz00(BgL_typedz00_2559,
													BgL_formalsz00_2558, BgL_errz00_2557,
													CDR(BgL_argsz00_1684), ((bool_t) 0));
												return MAKE_YOUNG_PAIR(BgL_arg1731z00_1692,
													BgL_arg1733z00_1693);
											}
									}
								else
									{	/* Llib/dsssl.scm 464 */
										if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CAR
													(BgL_argsz00_1684), BGl_list2146z00zz__dssslz00)))
											{
												bool_t BgL_dssslz00_3560;
												obj_t BgL_argsz00_3558;

												BgL_argsz00_3558 = CDR(BgL_argsz00_1684);
												BgL_dssslz00_3560 = ((bool_t) 1);
												BgL_dssslz00_1685 = BgL_dssslz00_3560;
												BgL_argsz00_1684 = BgL_argsz00_3558;
												goto BGl_loopze70ze7zz__dssslz00;
											}
										else
											{	/* Llib/dsssl.scm 466 */
												if (BgL_dssslz00_1685)
													{	/* Llib/dsssl.scm 470 */
														bool_t BgL_test2322z00_3562;

														BgL_objz00_1710 = CAR(BgL_argsz00_1684);
														if (PAIRP(BgL_objz00_1710))
															{	/* Llib/dsssl.scm 443 */
																bool_t BgL_test2324z00_3565;

																{	/* Llib/dsssl.scm 443 */
																	obj_t BgL_tmpz00_3566;

																	BgL_tmpz00_3566 = CDR(BgL_objz00_1710);
																	BgL_test2324z00_3565 = PAIRP(BgL_tmpz00_3566);
																}
																if (BgL_test2324z00_3565)
																	{	/* Llib/dsssl.scm 443 */
																		BgL_test2322z00_3562 =
																			NULLP(CDR(CDR(BgL_objz00_1710)));
																	}
																else
																	{	/* Llib/dsssl.scm 443 */
																		BgL_test2322z00_3562 = ((bool_t) 0);
																	}
															}
														else
															{	/* Llib/dsssl.scm 442 */
																BgL_test2322z00_3562 = ((bool_t) 0);
															}
														if (BgL_test2322z00_3562)
															{	/* Llib/dsssl.scm 470 */
																return
																	BGl_idzd2sanszd2typez00zz__dssslz00(CAR(CAR
																		(BgL_argsz00_1684)));
															}
														else
															{	/* Llib/dsssl.scm 470 */
																return
																	BGL_PROCEDURE_CALL3(BgL_errz00_2557,
																	BGl_string2147z00zz__dssslz00,
																	BGl_string2148z00zz__dssslz00,
																	BgL_formalsz00_2558);
															}
													}
												else
													{	/* Llib/dsssl.scm 468 */
														return
															BGL_PROCEDURE_CALL3(BgL_errz00_2557,
															BGl_string2147z00zz__dssslz00,
															BGl_string2149z00zz__dssslz00,
															BgL_formalsz00_2558);
													}
											}
									}
							}
						else
							{	/* Llib/dsssl.scm 454 */
								if (BgL_dssslz00_1685)
									{	/* Llib/dsssl.scm 456 */
										return
											BGL_PROCEDURE_CALL3(BgL_errz00_2557,
											BGl_string2150z00zz__dssslz00,
											BGl_string2151z00zz__dssslz00, BgL_formalsz00_2558);
									}
								else
									{	/* Llib/dsssl.scm 456 */
										if (SYMBOLP(BgL_argsz00_1684))
											{	/* Llib/dsssl.scm 460 */
												return
													BGl_idzd2sanszd2typez00zz__dssslz00(BgL_argsz00_1684);
											}
										else
											{	/* Llib/dsssl.scm 460 */
												return
													BGL_PROCEDURE_CALL3(BgL_errz00_2557,
													BGl_string2147z00zz__dssslz00,
													BGl_string2149z00zz__dssslz00, BgL_formalsz00_2558);
											}
									}
							}
					}
			}
		}

	}



/* &dsssl-formals->scheme-typed-formals */
	obj_t
		BGl_z62dssslzd2formalszd2ze3schemezd2typedzd2formalsz81zz__dssslz00(obj_t
		BgL_envz00_2553, obj_t BgL_formalsz00_2554, obj_t BgL_errz00_2555,
		obj_t BgL_typedz00_2556)
	{
		{	/* Llib/dsssl.scm 436 */
			{	/* Llib/dsssl.scm 439 */
				obj_t BgL_auxz00_3604;

				if (PROCEDUREP(BgL_errz00_2555))
					{	/* Llib/dsssl.scm 439 */
						BgL_auxz00_3604 = BgL_errz00_2555;
					}
				else
					{
						obj_t BgL_auxz00_3607;

						BgL_auxz00_3607 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2091z00zz__dssslz00,
							BINT(15856L), BGl_string2152z00zz__dssslz00,
							BGl_string2093z00zz__dssslz00, BgL_errz00_2555);
						FAILURE(BgL_auxz00_3607, BFALSE, BFALSE);
					}
				return
					BGl_dssslzd2formalszd2ze3schemezd2typedzd2formalsze3zz__dssslz00
					(BgL_formalsz00_2554, BgL_auxz00_3604, CBOOL(BgL_typedz00_2556));
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__dssslz00(void)
	{
		{	/* Llib/dsssl.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__dssslz00(void)
	{
		{	/* Llib/dsssl.scm 14 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__dssslz00(void)
	{
		{	/* Llib/dsssl.scm 14 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__dssslz00(void)
	{
		{	/* Llib/dsssl.scm 14 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string2153z00zz__dssslz00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string2153z00zz__dssslz00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string2153z00zz__dssslz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string2153z00zz__dssslz00));
			return
				BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string2153z00zz__dssslz00));
		}

	}

#ifdef __cplusplus
}
#endif
