/*===========================================================================*/
/*   (Llib/srfi4.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Llib/srfi4.scm -indent -o objs/obj_u/Llib/srfi4.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___SRFI4_TYPE_DEFINITIONS
#define BGL___SRFI4_TYPE_DEFINITIONS
#endif													// BGL___SRFI4_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t BGl_u8vectorzd2setz12zc0zz__srfi4z00(obj_t, long,
		uint8_t);
	static obj_t BGl_z62s64vectorzd2refzb0zz__srfi4z00(obj_t, obj_t, obj_t);
	extern bool_t BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl__s8vectorzd2copyz12zc0zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_u8vectorzd2ze3listz31zz__srfi4z00(obj_t);
	static obj_t BGl_z62f64vectorzf3z91zz__srfi4z00(obj_t, obj_t);
	static obj_t BGl_z62f64vectorz62zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_u8vectorzd2copyz12zc0zz__srfi4z00(obj_t, long,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_listzd2ze3f64vectorz31zz__srfi4z00(obj_t);
	static obj_t BGl_z62listzd2ze3s8vectorz53zz__srfi4z00(obj_t, obj_t);
	static obj_t BGl_z62s8vectorzd2ze3listz53zz__srfi4z00(obj_t, obj_t);
	static obj_t BGl__f64vectorzd2copyz12zc0zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_makezd2u8vectorzd2zz__srfi4z00(long, uint8_t);
	BGL_EXPORTED_DECL obj_t BGl_makezd2f64vectorzd2zz__srfi4z00(long, double);
	static obj_t BGl_z62listzd2ze3f64vectorz53zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_u16vectorzd2ze3listz31zz__srfi4z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_u16vectorzd2copyz12zc0zz__srfi4z00(obj_t, long,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL uint16_t BGl_u16vectorzd2refzd2zz__srfi4z00(obj_t, long);
	static obj_t BGl_z62u16vectorzd2ze3listz53zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL long BGl_u16vectorzd2lengthzd2zz__srfi4z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_s8vectorz00zz__srfi4z00(obj_t);
	static obj_t BGl_z62s32vectorzd2lengthzb0zz__srfi4z00(obj_t, obj_t);
	static obj_t BGl__f32vectorzd2copyz12zc0zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_homogeneouszd2vectorzf3z21zz__srfi4z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_u16vectorzd2setz12zc0zz__srfi4z00(obj_t, long,
		uint16_t);
	static obj_t BGl_z62s16vectorzd2setz12za2zz__srfi4z00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL long BGl_u8vectorzd2lengthzd2zz__srfi4z00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__srfi4z00 = BUNSPEC;
	static obj_t BGl_z62u64vectorzd2setz12za2zz__srfi4z00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62u32vectorzf3z91zz__srfi4z00(obj_t, obj_t);
	static obj_t BGl_z62s16vectorz62zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_listzd2ze3s16vectorz31zz__srfi4z00(obj_t);
	static obj_t BGl_z62u16vectorz62zz__srfi4z00(obj_t, obj_t);
	static obj_t BGl_z62u8vectorzd2lengthzb0zz__srfi4z00(obj_t, obj_t);
	static obj_t BGl__makezd2s32vectorzd2zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_listzd2ze3u16vectorz31zz__srfi4z00(obj_t);
	static obj_t BGl__makezd2u32vectorzd2zz__srfi4z00(obj_t, obj_t);
	static obj_t BGl_z62f64vectorzd2lengthzb0zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_makezd2s16vectorzd2zz__srfi4z00(long, int16_t);
	static obj_t BGl_z62listzd2ze3s16vectorz53zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_makezd2u16vectorzd2zz__srfi4z00(long, uint16_t);
	static obj_t BGl_z62listzd2ze3u16vectorz53zz__srfi4z00(obj_t, obj_t);
	static obj_t BGl_z62s8vectorzf3z91zz__srfi4z00(obj_t, obj_t);
	static obj_t BGl_z62u8vectorzf3z91zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_makezd2s8vectorzd2zz__srfi4z00(long, int8_t);
	BGL_EXPORTED_DECL obj_t BGl_hvectorzd2rangezd2errorz00zz__srfi4z00(obj_t,
		obj_t, long);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl__u64vectorzd2copyz12zc0zz__srfi4z00(obj_t, obj_t);
	static obj_t BGl__makezd2s64vectorzd2zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__srfi4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__hashz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl__makezd2u64vectorzd2zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL long BGl_s16vectorzd2lengthzd2zz__srfi4z00(obj_t);
	static obj_t BGl_z62u32vectorzd2refzb0zz__srfi4z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_f32vectorz00zz__srfi4z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_s32vectorzd2setz12zc0zz__srfi4z00(obj_t, long,
		int32_t);
	static obj_t BGl_z62u64vectorzd2lengthzb0zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_s64vectorzf3zf3zz__srfi4z00(obj_t);
	static obj_t BGl_z62f64vectorzd2refzb0zz__srfi4z00(obj_t, obj_t, obj_t);
	static obj_t BGl__u32vectorzd2copyz12zc0zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL uint64_t BGl_u64vectorzd2refzd2zz__srfi4z00(obj_t, long);
	BGL_EXPORTED_DECL obj_t BGl_f64vectorzd2setz12zc0zz__srfi4z00(obj_t, long,
		double);
	static obj_t BGl_z62f32vectorzd2setz12za2zz__srfi4z00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62u8vectorzd2setz12za2zz__srfi4z00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_f64vectorz00zz__srfi4z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_listzd2ze3u8vectorz31zz__srfi4z00(obj_t);
	extern obj_t BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(long,
		long);
	BGL_EXPORTED_DECL obj_t BGl_s16vectorzd2ze3listz31zz__srfi4z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_s16vectorzd2copyz12zc0zz__srfi4z00(obj_t, long,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62s16vectorzd2ze3listz53zz__srfi4z00(obj_t, obj_t);
	static obj_t BGl_z62u16vectorzd2refzb0zz__srfi4z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62s64vectorzd2lengthzb0zz__srfi4z00(obj_t, obj_t);
	static obj_t BGl_z62u8vectorz62zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL long BGl_s8vectorzd2lengthzd2zz__srfi4z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_s16vectorz00zz__srfi4z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_u16vectorz00zz__srfi4z00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_u64vectorzf3zf3zz__srfi4z00(obj_t);
	static obj_t BGl_z62s8vectorzd2lengthzb0zz__srfi4z00(obj_t, obj_t);
	static obj_t BGl_z62s64vectorzf3z91zz__srfi4z00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__srfi4z00(void);
	BGL_EXPORTED_DECL bool_t BGl_s8vectorzf3zf3zz__srfi4z00(obj_t);
	static obj_t BGl_genericzd2initzd2zz__srfi4z00(void);
	BGL_EXPORTED_DECL bool_t BGl_u8vectorzf3zf3zz__srfi4z00(obj_t);
	extern long bgl_list_length(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_s8vectorzd2ze3listz31zz__srfi4z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_listzd2ze3s8vectorz31zz__srfi4z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_s8vectorzd2copyz12zc0zz__srfi4z00(obj_t, long,
		obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__srfi4z00(void);
	static obj_t BGl_z62s8vectorzd2refzb0zz__srfi4z00(obj_t, obj_t, obj_t);
	static obj_t BGl_gczd2rootszd2initz00zz__srfi4z00(void);
	BGL_EXPORTED_DECL int32_t BGl_s32vectorzd2refzd2zz__srfi4z00(obj_t, long);
	static obj_t BGl_objectzd2initzd2zz__srfi4z00(void);
	static obj_t BGl__s64vectorzd2copyz12zc0zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_homogeneouszd2vectorzd2infoz00zz__srfi4z00(obj_t);
	static obj_t BGl_z62s8vectorz62zz__srfi4z00(obj_t, obj_t);
	static obj_t BGl_z62u16vectorzd2lengthzb0zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_s16vectorzd2setz12zc0zz__srfi4z00(obj_t, long,
		int16_t);
	static obj_t BGl_z62homogeneouszd2vectorzd2infoz62zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_f64vectorzd2ze3listz31zz__srfi4z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_f64vectorzd2copyz12zc0zz__srfi4z00(obj_t, long,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_u64vectorzd2setz12zc0zz__srfi4z00(obj_t, long,
		uint64_t);
	static obj_t BGl_z62u32vectorzd2setz12za2zz__srfi4z00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL long BGl_f32vectorzd2lengthzd2zz__srfi4z00(obj_t);
	static obj_t BGl_z62f64vectorzd2ze3listz53zz__srfi4z00(obj_t, obj_t);
	static obj_t BGl_z62s64vectorzd2setz12za2zz__srfi4z00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62s32vectorz62zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_listzd2ze3s32vectorz31zz__srfi4z00(obj_t);
	static obj_t BGl_z62u32vectorz62zz__srfi4z00(obj_t, obj_t);
	static obj_t BGl__s32vectorzd2copyz12zc0zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_listzd2ze3u32vectorz31zz__srfi4z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_makezd2s32vectorzd2zz__srfi4z00(long, int32_t);
	static obj_t BGl_z62u64vectorzf3z91zz__srfi4z00(obj_t, obj_t);
	static obj_t BGl_z62listzd2ze3s32vectorz53zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_s8vectorzd2setz12zc0zz__srfi4z00(obj_t, long,
		int8_t);
	BGL_EXPORTED_DECL obj_t BGl_makezd2u32vectorzd2zz__srfi4z00(long, uint32_t);
	static obj_t BGl_z62listzd2ze3u32vectorz53zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_f32vectorzf3zf3zz__srfi4z00(obj_t);
	extern obj_t string_append_3(obj_t, obj_t, obj_t);
	static obj_t BGl_z62u64vectorzd2refzb0zz__srfi4z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_f32vectorzd2ze3listz31zz__srfi4z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_f32vectorzd2copyz12zc0zz__srfi4z00(obj_t, long,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL int8_t BGl_s8vectorzd2refzd2zz__srfi4z00(obj_t, long);
	static obj_t BGl_z62f32vectorzd2ze3listz53zz__srfi4z00(obj_t, obj_t);
	static obj_t BGl_z62s64vectorz62zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL int16_t BGl_s16vectorzd2refzd2zz__srfi4z00(obj_t, long);
	BGL_EXPORTED_DECL obj_t BGl_listzd2ze3s64vectorz31zz__srfi4z00(obj_t);
	static obj_t BGl_z62u64vectorz62zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_listzd2ze3u64vectorz31zz__srfi4z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_makezd2s64vectorzd2zz__srfi4z00(long, int64_t);
	BGL_EXPORTED_DECL long BGl_u32vectorzd2lengthzd2zz__srfi4z00(obj_t);
	static obj_t BGl_z62listzd2ze3s64vectorz53zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_makezd2u64vectorzd2zz__srfi4z00(long, uint64_t);
	static obj_t BGl_z62listzd2ze3u64vectorz53zz__srfi4z00(obj_t, obj_t);
	static obj_t BGl_z62s16vectorzd2lengthzb0zz__srfi4z00(obj_t, obj_t);
	static obj_t BGl__makezd2f32vectorzd2zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_s16vectorzf3zf3zz__srfi4z00(obj_t);
	static obj_t BGl_z62homogeneouszd2vectorzf3z43zz__srfi4z00(obj_t, obj_t);
	static obj_t BGl__u16vectorzd2copyz12zc0zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_f32vectorzd2setz12zc0zz__srfi4z00(obj_t, long,
		float);
	BGL_EXPORTED_DECL obj_t BGl_u64vectorzd2ze3listz31zz__srfi4z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_u64vectorzd2copyz12zc0zz__srfi4z00(obj_t, long,
		obj_t, obj_t, obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_z62u64vectorzd2ze3listz53zz__srfi4z00(obj_t, obj_t);
	static obj_t BGl_z62f32vectorzf3z91zz__srfi4z00(obj_t, obj_t);
	static obj_t BGl_symbol2345z00zz__srfi4z00 = BUNSPEC;
	static obj_t BGl_symbol2347z00zz__srfi4z00 = BUNSPEC;
	static obj_t BGl_symbol2349z00zz__srfi4z00 = BUNSPEC;
	static obj_t BGl__u8vectorzd2copyz12zc0zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL float BGl_f32vectorzd2refzd2zz__srfi4z00(obj_t, long);
	static obj_t BGl_z62u8vectorzd2ze3listz53zz__srfi4z00(obj_t, obj_t);
	static obj_t BGl__makezd2f64vectorzd2zz__srfi4z00(obj_t, obj_t);
	static obj_t BGl_z62hvectorzd2rangezd2errorz62zz__srfi4z00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_u32vectorzd2ze3listz31zz__srfi4z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_u32vectorzd2copyz12zc0zz__srfi4z00(obj_t, long,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62u32vectorzd2ze3listz53zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL long BGl_s32vectorzd2lengthzd2zz__srfi4z00(obj_t);
	static obj_t BGl_z62s32vectorzd2refzb0zz__srfi4z00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2351z00zz__srfi4z00 = BUNSPEC;
	static obj_t BGl_methodzd2initzd2zz__srfi4z00(void);
	extern obj_t BGl_bigloozd2typezd2errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2353z00zz__srfi4z00 = BUNSPEC;
	static obj_t BGl_symbol2355z00zz__srfi4z00 = BUNSPEC;
	static obj_t BGl_symbol2357z00zz__srfi4z00 = BUNSPEC;
	static obj_t BGl_symbol2359z00zz__srfi4z00 = BUNSPEC;
	static obj_t BGl__makezd2u8vectorzd2zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_s32vectorz00zz__srfi4z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_u32vectorz00zz__srfi4z00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_u16vectorzf3zf3zz__srfi4z00(obj_t);
	static obj_t BGl_z62u16vectorzd2setz12za2zz__srfi4z00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62s16vectorzf3z91zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL long BGl_f64vectorzd2lengthzd2zz__srfi4z00(obj_t);
	static obj_t BGl__makezd2s16vectorzd2zz__srfi4z00(obj_t, obj_t);
	static obj_t BGl__makezd2u16vectorzd2zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL int64_t BGl_s64vectorzd2refzd2zz__srfi4z00(obj_t, long);
	static obj_t BGl_z62s8vectorzd2setz12za2zz__srfi4z00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol2361z00zz__srfi4z00 = BUNSPEC;
	static obj_t BGl_symbol2363z00zz__srfi4z00 = BUNSPEC;
	static obj_t BGl_z62u8vectorzd2refzb0zz__srfi4z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_s64vectorz00zz__srfi4z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_u64vectorz00zz__srfi4z00(obj_t);
	static obj_t BGl_z62s16vectorzd2refzb0zz__srfi4z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL long BGl_u64vectorzd2lengthzd2zz__srfi4z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_u32vectorzd2setz12zc0zz__srfi4z00(obj_t, long,
		uint32_t);
	static obj_t BGl__makezd2s8vectorzd2zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_s64vectorzd2setz12zc0zz__srfi4z00(obj_t, long,
		int64_t);
	BGL_EXPORTED_DECL bool_t BGl_s32vectorzf3zf3zz__srfi4z00(obj_t);
	static obj_t BGl_z62s32vectorzd2setz12za2zz__srfi4z00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62f32vectorzd2lengthzb0zz__srfi4z00(obj_t, obj_t);
	static obj_t BGl_z62u16vectorzf3z91zz__srfi4z00(obj_t, obj_t);
	static obj_t BGl__s16vectorzd2copyz12zc0zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_s64vectorzd2ze3listz31zz__srfi4z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_s64vectorzd2copyz12zc0zz__srfi4z00(obj_t, long,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62s64vectorzd2ze3listz53zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_f64vectorzf3zf3zz__srfi4z00(obj_t);
	static obj_t BGl_z62f64vectorzd2setz12za2zz__srfi4z00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL uint8_t BGl_u8vectorzd2refzd2zz__srfi4z00(obj_t, long);
	static obj_t BGl_z62listzd2ze3u8vectorz53zz__srfi4z00(obj_t, obj_t);
	static obj_t BGl_z62f32vectorzd2refzb0zz__srfi4z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_s32vectorzd2ze3listz31zz__srfi4z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_s32vectorzd2copyz12zc0zz__srfi4z00(obj_t, long,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL uint32_t BGl_u32vectorzd2refzd2zz__srfi4z00(obj_t, long);
	static obj_t BGl_z62s32vectorzd2ze3listz53zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL long BGl_s64vectorzd2lengthzd2zz__srfi4z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_u8vectorz00zz__srfi4z00(obj_t);
	static obj_t BGl_z62u32vectorzd2lengthzb0zz__srfi4z00(obj_t, obj_t);
	static obj_t BGl_z62f32vectorz62zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_listzd2ze3f32vectorz31zz__srfi4z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_makezd2f32vectorzd2zz__srfi4z00(long, float);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62listzd2ze3f32vectorz53zz__srfi4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_u32vectorzf3zf3zz__srfi4z00(obj_t);
	BGL_EXPORTED_DECL double BGl_f64vectorzd2refzd2zz__srfi4z00(obj_t, long);
	static obj_t BGl_z62s32vectorzf3z91zz__srfi4z00(obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_homogeneouszd2vectorzf3zd2envzf3zz__srfi4z00,
		BgL_bgl_za762homogeneousza7d2465z00,
		BGl_z62homogeneouszd2vectorzf3z43zz__srfi4z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_u16vectorzd2envzd2zz__srfi4z00,
		BgL_bgl_za762u16vectorza762za72466za7, va_generic_entry,
		BGl_z62u16vectorz62zz__srfi4z00, BUNSPEC, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_listzd2ze3s16vectorzd2envze3zz__srfi4z00,
		BgL_bgl_za762listza7d2za7e3s162467za7,
		BGl_z62listzd2ze3s16vectorz53zz__srfi4z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2s16vectorzd2envz00zz__srfi4z00,
		BgL_bgl__makeza7d2s16vecto2468za7, opt_generic_entry,
		BGl__makezd2s16vectorzd2zz__srfi4z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_s64vectorzd2copyz12zd2envz12zz__srfi4z00,
		BgL_bgl__s64vectorza7d2cop2469za7, opt_generic_entry,
		BGl__s64vectorzd2copyz12zc0zz__srfi4z00, BFALSE, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_u8vectorzf3zd2envz21zz__srfi4z00,
		BgL_bgl_za762u8vectorza7f3za792470za7, BGl_z62u8vectorzf3z91zz__srfi4z00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_s8vectorzd2copyz12zd2envz12zz__srfi4z00,
		BgL_bgl__s8vectorza7d2copy2471za7, opt_generic_entry,
		BGl__s8vectorzd2copyz12zc0zz__srfi4z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_hvectorzd2rangezd2errorzd2envzd2zz__srfi4z00,
		BgL_bgl_za762hvectorza7d2ran2472z00,
		BGl_z62hvectorzd2rangezd2errorz62zz__srfi4z00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_u32vectorzd2copyz12zd2envz12zz__srfi4z00,
		BgL_bgl__u32vectorza7d2cop2473za7, opt_generic_entry,
		BGl__u32vectorzd2copyz12zc0zz__srfi4z00, BFALSE, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_s32vectorzd2lengthzd2envz00zz__srfi4z00,
		BgL_bgl_za762s32vectorza7d2l2474z00,
		BGl_z62s32vectorzd2lengthzb0zz__srfi4z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_s16vectorzd2lengthzd2envz00zz__srfi4z00,
		BgL_bgl_za762s16vectorza7d2l2475z00,
		BGl_z62s16vectorzd2lengthzb0zz__srfi4z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_listzd2ze3f32vectorzd2envze3zz__srfi4z00,
		BgL_bgl_za762listza7d2za7e3f322476za7,
		BGl_z62listzd2ze3f32vectorz53zz__srfi4z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_s16vectorzd2setz12zd2envz12zz__srfi4z00,
		BgL_bgl_za762s16vectorza7d2s2477z00,
		BGl_z62s16vectorzd2setz12za2zz__srfi4z00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2f32vectorzd2envz00zz__srfi4z00,
		BgL_bgl__makeza7d2f32vecto2478za7, opt_generic_entry,
		BGl__makezd2f32vectorzd2zz__srfi4z00, BFALSE, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_f64vectorzd2refzd2envz00zz__srfi4z00,
		BgL_bgl_za762f64vectorza7d2r2479z00, BGl_z62f64vectorzd2refzb0zz__srfi4z00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_u16vectorzf3zd2envz21zz__srfi4z00,
		BgL_bgl_za762u16vectorza7f3za72480za7, BGl_z62u16vectorzf3z91zz__srfi4z00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_f64vectorzd2lengthzd2envz00zz__srfi4z00,
		BgL_bgl_za762f64vectorza7d2l2481z00,
		BGl_z62f64vectorzd2lengthzb0zz__srfi4z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_s64vectorzd2refzd2envz00zz__srfi4z00,
		BgL_bgl_za762s64vectorza7d2r2482z00, BGl_z62s64vectorzd2refzb0zz__srfi4z00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_u64vectorzd2refzd2envz00zz__srfi4z00,
		BgL_bgl_za762u64vectorza7d2r2483z00, BGl_z62u64vectorzd2refzb0zz__srfi4z00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_f32vectorzd2copyz12zd2envz12zz__srfi4z00,
		BgL_bgl__f32vectorza7d2cop2484za7, opt_generic_entry,
		BGl__f32vectorzd2copyz12zc0zz__srfi4z00, BFALSE, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_u8vectorzd2copyz12zd2envz12zz__srfi4z00,
		BgL_bgl__u8vectorza7d2copy2485za7, opt_generic_entry,
		BGl__u8vectorzd2copyz12zc0zz__srfi4z00, BFALSE, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_u64vectorzd2envzd2zz__srfi4z00,
		BgL_bgl_za762u64vectorza762za72486za7, va_generic_entry,
		BGl_z62u64vectorz62zz__srfi4z00, BUNSPEC, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_s16vectorzd2ze3listzd2envze3zz__srfi4z00,
		BgL_bgl_za762s16vectorza7d2za72487za7,
		BGl_z62s16vectorzd2ze3listz53zz__srfi4z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_listzd2ze3s64vectorzd2envze3zz__srfi4z00,
		BgL_bgl_za762listza7d2za7e3s642488za7,
		BGl_z62listzd2ze3s64vectorz53zz__srfi4z00, 0L, BUNSPEC, 1);
	extern obj_t BGl_zd3u32zd2envz01zz__r4_numbers_6_5_fixnumz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_s16vectorzd2refzd2envz00zz__srfi4z00,
		BgL_bgl_za762s16vectorza7d2r2489z00, BGl_z62s16vectorzd2refzb0zz__srfi4z00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_u16vectorzd2refzd2envz00zz__srfi4z00,
		BgL_bgl_za762u16vectorza7d2r2490z00, BGl_z62u16vectorzd2refzb0zz__srfi4z00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2s64vectorzd2envz00zz__srfi4z00,
		BgL_bgl__makeza7d2s64vecto2491za7, opt_generic_entry,
		BGl__makezd2s64vectorzd2zz__srfi4z00, BFALSE, -1);
	      DEFINE_STRING(BGl_string2400z00zz__srfi4z00,
		BgL_bgl_string2400za700za7za7_2492za7, "_make-u32vector", 15);
	      DEFINE_STRING(BGl_string2401z00zz__srfi4z00,
		BgL_bgl_string2401za700za7za7_2493za7, "buint32", 7);
	      DEFINE_STRING(BGl_string2402z00zz__srfi4z00,
		BgL_bgl_string2402za700za7za7_2494za7, "_make-s64vector", 15);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_u16vectorzd2setz12zd2envz12zz__srfi4z00,
		BgL_bgl_za762u16vectorza7d2s2495z00,
		BGl_z62u16vectorzd2setz12za2zz__srfi4z00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2403z00zz__srfi4z00,
		BgL_bgl_string2403za700za7za7_2496za7, "bint64", 6);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_s64vectorzd2setz12zd2envz12zz__srfi4z00,
		BgL_bgl_za762s64vectorza7d2s2497z00,
		BGl_z62s64vectorzd2setz12za2zz__srfi4z00, 0L, BUNSPEC, 3);
	extern obj_t BGl_zd3u8zd2envz01zz__r4_numbers_6_5_fixnumz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_s32vectorzf3zd2envz21zz__srfi4z00,
		BgL_bgl_za762s32vectorza7f3za72498za7, BGl_z62s32vectorzf3z91zz__srfi4z00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2404z00zz__srfi4z00,
		BgL_bgl_string2404za700za7za7_2499za7, "_make-u64vector", 15);
	      DEFINE_STRING(BGl_string2405z00zz__srfi4z00,
		BgL_bgl_string2405za700za7za7_2500za7, "buint64", 7);
	      DEFINE_STRING(BGl_string2406z00zz__srfi4z00,
		BgL_bgl_string2406za700za7za7_2501za7, "_make-f32vector", 15);
	      DEFINE_STRING(BGl_string2407z00zz__srfi4z00,
		BgL_bgl_string2407za700za7za7_2502za7, "real", 4);
	      DEFINE_STRING(BGl_string2408z00zz__srfi4z00,
		BgL_bgl_string2408za700za7za7_2503za7, "_make-f64vector", 15);
	      DEFINE_STRING(BGl_string2409z00zz__srfi4z00,
		BgL_bgl_string2409za700za7za7_2504za7, "index out of range [0..", 23);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_u64vectorzd2copyz12zd2envz12zz__srfi4z00,
		BgL_bgl__u64vectorza7d2cop2505za7, opt_generic_entry,
		BGl__u64vectorzd2copyz12zc0zz__srfi4z00, BFALSE, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_f64vectorzf3zd2envz21zz__srfi4z00,
		BgL_bgl_za762f64vectorza7f3za72506za7, BGl_z62f64vectorzf3z91zz__srfi4z00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2410z00zz__srfi4z00,
		BgL_bgl_string2410za700za7za7_2507za7, "]", 1);
	      DEFINE_STRING(BGl_string2411z00zz__srfi4z00,
		BgL_bgl_string2411za700za7za7_2508za7, "&hvector-range-error", 20);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2s8vectorzd2envz00zz__srfi4z00,
		BgL_bgl__makeza7d2s8vector2509za7, opt_generic_entry,
		BGl__makezd2s8vectorzd2zz__srfi4z00, BFALSE, -1);
	      DEFINE_STRING(BGl_string2412z00zz__srfi4z00,
		BgL_bgl_string2412za700za7za7_2510za7, "bstring", 7);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_u8vectorzd2refzd2envz00zz__srfi4z00,
		BgL_bgl_za762u8vectorza7d2re2511z00, BGl_z62u8vectorzd2refzb0zz__srfi4z00,
		0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2413z00zz__srfi4z00,
		BgL_bgl_string2413za700za7za7_2512za7, "&s8vector-ref", 13);
	      DEFINE_STRING(BGl_string2414z00zz__srfi4z00,
		BgL_bgl_string2414za700za7za7_2513za7, "&u8vector-ref", 13);
	      DEFINE_STRING(BGl_string2415z00zz__srfi4z00,
		BgL_bgl_string2415za700za7za7_2514za7, "&s16vector-ref", 14);
	      DEFINE_STRING(BGl_string2416z00zz__srfi4z00,
		BgL_bgl_string2416za700za7za7_2515za7, "&u16vector-ref", 14);
	      DEFINE_STRING(BGl_string2417z00zz__srfi4z00,
		BgL_bgl_string2417za700za7za7_2516za7, "&s32vector-ref", 14);
	      DEFINE_STRING(BGl_string2418z00zz__srfi4z00,
		BgL_bgl_string2418za700za7za7_2517za7, "&u32vector-ref", 14);
	      DEFINE_STRING(BGl_string2419z00zz__srfi4z00,
		BgL_bgl_string2419za700za7za7_2518za7, "&s64vector-ref", 14);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_s32vectorzd2ze3listzd2envze3zz__srfi4z00,
		BgL_bgl_za762s32vectorza7d2za72519za7,
		BGl_z62s32vectorzd2ze3listz53zz__srfi4z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_s32vectorzd2setz12zd2envz12zz__srfi4z00,
		BgL_bgl_za762s32vectorza7d2s2520z00,
		BGl_z62s32vectorzd2setz12za2zz__srfi4z00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2420z00zz__srfi4z00,
		BgL_bgl_string2420za700za7za7_2521za7, "&u64vector-ref", 14);
	      DEFINE_STRING(BGl_string2421z00zz__srfi4z00,
		BgL_bgl_string2421za700za7za7_2522za7, "&f32vector-ref", 14);
	      DEFINE_STRING(BGl_string2422z00zz__srfi4z00,
		BgL_bgl_string2422za700za7za7_2523za7, "&f64vector-ref", 14);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_s32vectorzd2envzd2zz__srfi4z00,
		BgL_bgl_za762s32vectorza762za72524za7, va_generic_entry,
		BGl_z62s32vectorz62zz__srfi4z00, BUNSPEC, -1);
	      DEFINE_STRING(BGl_string2423z00zz__srfi4z00,
		BgL_bgl_string2423za700za7za7_2525za7, "&s8vector-set!", 14);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_f64vectorzd2copyz12zd2envz12zz__srfi4z00,
		BgL_bgl__f64vectorza7d2cop2526za7, opt_generic_entry,
		BGl__f64vectorzd2copyz12zc0zz__srfi4z00, BFALSE, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_u64vectorzd2setz12zd2envz12zz__srfi4z00,
		BgL_bgl_za762u64vectorza7d2s2527z00,
		BGl_z62u64vectorzd2setz12za2zz__srfi4z00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2424z00zz__srfi4z00,
		BgL_bgl_string2424za700za7za7_2528za7, "&u8vector-set!", 14);
	      DEFINE_STRING(BGl_string2425z00zz__srfi4z00,
		BgL_bgl_string2425za700za7za7_2529za7, "&s16vector-set!", 15);
	      DEFINE_STRING(BGl_string2426z00zz__srfi4z00,
		BgL_bgl_string2426za700za7za7_2530za7, "&u16vector-set!", 15);
	      DEFINE_STRING(BGl_string2427z00zz__srfi4z00,
		BgL_bgl_string2427za700za7za7_2531za7, "&s32vector-set!", 15);
	      DEFINE_STRING(BGl_string2346z00zz__srfi4z00,
		BgL_bgl_string2346za700za7za7_2532za7, "s8", 2);
	      DEFINE_STRING(BGl_string2428z00zz__srfi4z00,
		BgL_bgl_string2428za700za7za7_2533za7, "&u32vector-set!", 15);
	      DEFINE_STRING(BGl_string2429z00zz__srfi4z00,
		BgL_bgl_string2429za700za7za7_2534za7, "&s64vector-set!", 15);
	      DEFINE_STRING(BGl_string2348z00zz__srfi4z00,
		BgL_bgl_string2348za700za7za7_2535za7, "u8", 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_listzd2ze3u32vectorzd2envze3zz__srfi4z00,
		BgL_bgl_za762listza7d2za7e3u322536za7,
		BGl_z62listzd2ze3u32vectorz53zz__srfi4z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2u32vectorzd2envz00zz__srfi4z00,
		BgL_bgl__makeza7d2u32vecto2537za7, opt_generic_entry,
		BGl__makezd2u32vectorzd2zz__srfi4z00, BFALSE, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_s8vectorzd2setz12zd2envz12zz__srfi4z00,
		BgL_bgl_za762s8vectorza7d2se2538z00,
		BGl_z62s8vectorzd2setz12za2zz__srfi4z00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_u8vectorzd2setz12zd2envz12zz__srfi4z00,
		BgL_bgl_za762u8vectorza7d2se2539z00,
		BGl_z62u8vectorzd2setz12za2zz__srfi4z00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_f64vectorzd2setz12zd2envz12zz__srfi4z00,
		BgL_bgl_za762f64vectorza7d2s2540z00,
		BGl_z62f64vectorzd2setz12za2zz__srfi4z00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_s8vectorzd2refzd2envz00zz__srfi4z00,
		BgL_bgl_za762s8vectorza7d2re2541z00, BGl_z62s8vectorzd2refzb0zz__srfi4z00,
		0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2430z00zz__srfi4z00,
		BgL_bgl_string2430za700za7za7_2542za7, "&u64vector-set!", 15);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_listzd2ze3f64vectorzd2envze3zz__srfi4z00,
		BgL_bgl_za762listza7d2za7e3f642543za7,
		BGl_z62listzd2ze3f64vectorz53zz__srfi4z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2431z00zz__srfi4z00,
		BgL_bgl_string2431za700za7za7_2544za7, "&f32vector-set!", 15);
	      DEFINE_STRING(BGl_string2350z00zz__srfi4z00,
		BgL_bgl_string2350za700za7za7_2545za7, "s16", 3);
	      DEFINE_STRING(BGl_string2432z00zz__srfi4z00,
		BgL_bgl_string2432za700za7za7_2546za7, "&f64vector-set!", 15);
	      DEFINE_STRING(BGl_string2433z00zz__srfi4z00,
		BgL_bgl_string2433za700za7za7_2547za7, "&s8vector->list", 15);
	      DEFINE_STRING(BGl_string2352z00zz__srfi4z00,
		BgL_bgl_string2352za700za7za7_2548za7, "u16", 3);
	      DEFINE_STRING(BGl_string2434z00zz__srfi4z00,
		BgL_bgl_string2434za700za7za7_2549za7, "&u8vector->list", 15);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2f64vectorzd2envz00zz__srfi4z00,
		BgL_bgl__makeza7d2f64vecto2550za7, opt_generic_entry,
		BGl__makezd2f64vectorzd2zz__srfi4z00, BFALSE, -1);
	      DEFINE_STRING(BGl_string2435z00zz__srfi4z00,
		BgL_bgl_string2435za700za7za7_2551za7, "&s16vector->list", 16);
	      DEFINE_STRING(BGl_string2354z00zz__srfi4z00,
		BgL_bgl_string2354za700za7za7_2552za7, "s32", 3);
	      DEFINE_STRING(BGl_string2436z00zz__srfi4z00,
		BgL_bgl_string2436za700za7za7_2553za7, "&u16vector->list", 16);
	      DEFINE_STRING(BGl_string2437z00zz__srfi4z00,
		BgL_bgl_string2437za700za7za7_2554za7, "&s32vector->list", 16);
	      DEFINE_STRING(BGl_string2356z00zz__srfi4z00,
		BgL_bgl_string2356za700za7za7_2555za7, "u32", 3);
	      DEFINE_STRING(BGl_string2438z00zz__srfi4z00,
		BgL_bgl_string2438za700za7za7_2556za7, "&u32vector->list", 16);
	      DEFINE_STRING(BGl_string2439z00zz__srfi4z00,
		BgL_bgl_string2439za700za7za7_2557za7, "&s64vector->list", 16);
	      DEFINE_STRING(BGl_string2358z00zz__srfi4z00,
		BgL_bgl_string2358za700za7za7_2558za7, "s64", 3);
	extern obj_t BGl_zd3s32zd2envz01zz__r4_numbers_6_5_fixnumz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_u16vectorzd2ze3listzd2envze3zz__srfi4z00,
		BgL_bgl_za762u16vectorza7d2za72559za7,
		BGl_z62u16vectorzd2ze3listz53zz__srfi4z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_u32vectorzd2lengthzd2envz00zz__srfi4z00,
		BgL_bgl_za762u32vectorza7d2l2560z00,
		BGl_z62u32vectorzd2lengthzb0zz__srfi4z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_s8vectorzd2envzd2zz__srfi4z00,
		BgL_bgl_za762s8vectorza762za7za72561z00, va_generic_entry,
		BGl_z62s8vectorz62zz__srfi4z00, BUNSPEC, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_u16vectorzd2lengthzd2envz00zz__srfi4z00,
		BgL_bgl_za762u16vectorza7d2l2562z00,
		BGl_z62u16vectorzd2lengthzb0zz__srfi4z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_u32vectorzd2setz12zd2envz12zz__srfi4z00,
		BgL_bgl_za762u32vectorza7d2s2563z00,
		BGl_z62u32vectorzd2setz12za2zz__srfi4z00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2440z00zz__srfi4z00,
		BgL_bgl_string2440za700za7za7_2564za7, "&u64vector->list", 16);
	      DEFINE_STRING(BGl_string2441z00zz__srfi4z00,
		BgL_bgl_string2441za700za7za7_2565za7, "&f32vector->list", 16);
	      DEFINE_STRING(BGl_string2360z00zz__srfi4z00,
		BgL_bgl_string2360za700za7za7_2566za7, "u64", 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_s16vectorzf3zd2envz21zz__srfi4z00,
		BgL_bgl_za762s16vectorza7f3za72567za7, BGl_z62s16vectorzf3z91zz__srfi4z00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2442z00zz__srfi4z00,
		BgL_bgl_string2442za700za7za7_2568za7, "&f64vector->list", 16);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_s16vectorzd2envzd2zz__srfi4z00,
		BgL_bgl_za762s16vectorza762za72569za7, va_generic_entry,
		BGl_z62s16vectorz62zz__srfi4z00, BUNSPEC, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_s8vectorzd2lengthzd2envz00zz__srfi4z00,
		BgL_bgl_za762s8vectorza7d2le2570z00,
		BGl_z62s8vectorzd2lengthzb0zz__srfi4z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2443z00zz__srfi4z00,
		BgL_bgl_string2443za700za7za7_2571za7, "&list->s8vector", 15);
	      DEFINE_STRING(BGl_string2362z00zz__srfi4z00,
		BgL_bgl_string2362za700za7za7_2572za7, "f32", 3);
	      DEFINE_STRING(BGl_string2444z00zz__srfi4z00,
		BgL_bgl_string2444za700za7za7_2573za7, "pair-nil", 8);
	      DEFINE_STRING(BGl_string2445z00zz__srfi4z00,
		BgL_bgl_string2445za700za7za7_2574za7, "&list->u8vector", 15);
	      DEFINE_STRING(BGl_string2364z00zz__srfi4z00,
		BgL_bgl_string2364za700za7za7_2575za7, "f64", 3);
	      DEFINE_STRING(BGl_string2446z00zz__srfi4z00,
		BgL_bgl_string2446za700za7za7_2576za7, "&list->s16vector", 16);
	      DEFINE_STRING(BGl_string2365z00zz__srfi4z00,
		BgL_bgl_string2365za700za7za7_2577za7, "homogeneous-vector-info", 23);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_u64vectorzf3zd2envz21zz__srfi4z00,
		BgL_bgl_za762u64vectorza7f3za72578za7, BGl_z62u64vectorzf3z91zz__srfi4z00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2447z00zz__srfi4z00,
		BgL_bgl_string2447za700za7za7_2579za7, "&list->u16vector", 16);
	      DEFINE_STRING(BGl_string2366z00zz__srfi4z00,
		BgL_bgl_string2366za700za7za7_2580za7, "Illegal hvector ident", 21);
	      DEFINE_STRING(BGl_string2448z00zz__srfi4z00,
		BgL_bgl_string2448za700za7za7_2581za7, "&list->s32vector", 16);
	      DEFINE_STRING(BGl_string2367z00zz__srfi4z00,
		BgL_bgl_string2367za700za7za7_2582za7, "hvector", 7);
	      DEFINE_STRING(BGl_string2449z00zz__srfi4z00,
		BgL_bgl_string2449za700za7za7_2583za7, "&list->u32vector", 16);
	      DEFINE_STRING(BGl_string2368z00zz__srfi4z00,
		BgL_bgl_string2368za700za7za7_2584za7, "/tmp/bigloo/runtime/Llib/srfi4.scm",
		34);
	      DEFINE_STRING(BGl_string2369z00zz__srfi4z00,
		BgL_bgl_string2369za700za7za7_2585za7, "&s8vector-length", 16);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_listzd2ze3u16vectorzd2envze3zz__srfi4z00,
		BgL_bgl_za762listza7d2za7e3u162586za7,
		BGl_z62listzd2ze3u16vectorz53zz__srfi4z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_s64vectorzd2ze3listzd2envze3zz__srfi4z00,
		BgL_bgl_za762s64vectorza7d2za72587za7,
		BGl_z62s64vectorzd2ze3listz53zz__srfi4z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_f32vectorzd2setz12zd2envz12zz__srfi4z00,
		BgL_bgl_za762f32vectorza7d2s2588z00,
		BGl_z62f32vectorzd2setz12za2zz__srfi4z00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2u16vectorzd2envz00zz__srfi4z00,
		BgL_bgl__makeza7d2u16vecto2589za7, opt_generic_entry,
		BGl__makezd2u16vectorzd2zz__srfi4z00, BFALSE, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_s8vectorzf3zd2envz21zz__srfi4z00,
		BgL_bgl_za762s8vectorza7f3za792590za7, BGl_z62s8vectorzf3z91zz__srfi4z00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_listzd2ze3s8vectorzd2envze3zz__srfi4z00,
		BgL_bgl_za762listza7d2za7e3s8v2591za7,
		BGl_z62listzd2ze3s8vectorz53zz__srfi4z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_s8vectorzd2ze3listzd2envze3zz__srfi4z00,
		BgL_bgl_za762s8vectorza7d2za7e2592za7,
		BGl_z62s8vectorzd2ze3listz53zz__srfi4z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_f32vectorzd2refzd2envz00zz__srfi4z00,
		BgL_bgl_za762f32vectorza7d2r2593z00, BGl_z62f32vectorzd2refzb0zz__srfi4z00,
		0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2450z00zz__srfi4z00,
		BgL_bgl_string2450za700za7za7_2594za7, "&list->s64vector", 16);
	      DEFINE_STRING(BGl_string2451z00zz__srfi4z00,
		BgL_bgl_string2451za700za7za7_2595za7, "&list->u64vector", 16);
	      DEFINE_STRING(BGl_string2370z00zz__srfi4z00,
		BgL_bgl_string2370za700za7za7_2596za7, "s8vector", 8);
	      DEFINE_STRING(BGl_string2452z00zz__srfi4z00,
		BgL_bgl_string2452za700za7za7_2597za7, "&list->f32vector", 16);
	      DEFINE_STRING(BGl_string2371z00zz__srfi4z00,
		BgL_bgl_string2371za700za7za7_2598za7, "&u8vector-length", 16);
	      DEFINE_STRING(BGl_string2453z00zz__srfi4z00,
		BgL_bgl_string2453za700za7za7_2599za7, "&list->f64vector", 16);
	      DEFINE_STRING(BGl_string2372z00zz__srfi4z00,
		BgL_bgl_string2372za700za7za7_2600za7, "u8vector", 8);
	      DEFINE_STRING(BGl_string2454z00zz__srfi4z00,
		BgL_bgl_string2454za700za7za7_2601za7, "_s8vector-copy!", 15);
	      DEFINE_STRING(BGl_string2373z00zz__srfi4z00,
		BgL_bgl_string2373za700za7za7_2602za7, "&s16vector-length", 17);
	      DEFINE_STRING(BGl_string2455z00zz__srfi4z00,
		BgL_bgl_string2455za700za7za7_2603za7, "_u8vector-copy!", 15);
	      DEFINE_STRING(BGl_string2374z00zz__srfi4z00,
		BgL_bgl_string2374za700za7za7_2604za7, "s16vector", 9);
	      DEFINE_STRING(BGl_string2456z00zz__srfi4z00,
		BgL_bgl_string2456za700za7za7_2605za7, "_s16vector-copy!", 16);
	      DEFINE_STRING(BGl_string2375z00zz__srfi4z00,
		BgL_bgl_string2375za700za7za7_2606za7, "&u16vector-length", 17);
	      DEFINE_STRING(BGl_string2457z00zz__srfi4z00,
		BgL_bgl_string2457za700za7za7_2607za7, "_u16vector-copy!", 16);
	      DEFINE_STRING(BGl_string2376z00zz__srfi4z00,
		BgL_bgl_string2376za700za7za7_2608za7, "u16vector", 9);
	      DEFINE_STRING(BGl_string2458z00zz__srfi4z00,
		BgL_bgl_string2458za700za7za7_2609za7, "_s32vector-copy!", 16);
	      DEFINE_STRING(BGl_string2377z00zz__srfi4z00,
		BgL_bgl_string2377za700za7za7_2610za7, "&s32vector-length", 17);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_u32vectorzd2ze3listzd2envze3zz__srfi4z00,
		BgL_bgl_za762u32vectorza7d2za72611za7,
		BGl_z62u32vectorzd2ze3listz53zz__srfi4z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2459z00zz__srfi4z00,
		BgL_bgl_string2459za700za7za7_2612za7, "_u32vector-copy!", 16);
	      DEFINE_STRING(BGl_string2378z00zz__srfi4z00,
		BgL_bgl_string2378za700za7za7_2613za7, "s32vector", 9);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_s32vectorzd2refzd2envz00zz__srfi4z00,
		BgL_bgl_za762s32vectorza7d2r2614z00, BGl_z62s32vectorzd2refzb0zz__srfi4z00,
		0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2379z00zz__srfi4z00,
		BgL_bgl_string2379za700za7za7_2615za7, "&u32vector-length", 17);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_u32vectorzd2refzd2envz00zz__srfi4z00,
		BgL_bgl_za762u32vectorza7d2r2616z00, BGl_z62u32vectorzd2refzb0zz__srfi4z00,
		0L, BUNSPEC, 2);
	extern obj_t BGl_zd3u16zd2envz01zz__r4_numbers_6_5_fixnumz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_f32vectorzd2envzd2zz__srfi4z00,
		BgL_bgl_za762f32vectorza762za72617za7, va_generic_entry,
		BGl_z62f32vectorz62zz__srfi4z00, BUNSPEC, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_s64vectorzd2lengthzd2envz00zz__srfi4z00,
		BgL_bgl_za762s64vectorza7d2l2618z00,
		BGl_z62s64vectorzd2lengthzb0zz__srfi4z00, 0L, BUNSPEC, 1);
	extern obj_t BGl_zd3s8zd2envz01zz__r4_numbers_6_5_fixnumz00;
	   
		 
		DEFINE_STRING(BGl_string2460z00zz__srfi4z00,
		BgL_bgl_string2460za700za7za7_2619za7, "_s64vector-copy!", 16);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_f32vectorzd2lengthzd2envz00zz__srfi4z00,
		BgL_bgl_za762f32vectorza7d2l2620z00,
		BGl_z62f32vectorzd2lengthzb0zz__srfi4z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2461z00zz__srfi4z00,
		BgL_bgl_string2461za700za7za7_2621za7, "_u64vector-copy!", 16);
	      DEFINE_STRING(BGl_string2380z00zz__srfi4z00,
		BgL_bgl_string2380za700za7za7_2622za7, "u32vector", 9);
	      DEFINE_STRING(BGl_string2462z00zz__srfi4z00,
		BgL_bgl_string2462za700za7za7_2623za7, "_f32vector-copy!", 16);
	      DEFINE_STRING(BGl_string2381z00zz__srfi4z00,
		BgL_bgl_string2381za700za7za7_2624za7, "&s64vector-length", 17);
	extern obj_t BGl_zd3flzd2envz01zz__r4_numbers_6_5_flonumz00;
	   
		 
		DEFINE_STRING(BGl_string2463z00zz__srfi4z00,
		BgL_bgl_string2463za700za7za7_2625za7, "_f64vector-copy!", 16);
	      DEFINE_STRING(BGl_string2382z00zz__srfi4z00,
		BgL_bgl_string2382za700za7za7_2626za7, "s64vector", 9);
	      DEFINE_STRING(BGl_string2464z00zz__srfi4z00,
		BgL_bgl_string2464za700za7za7_2627za7, "__srfi4", 7);
	      DEFINE_STRING(BGl_string2383z00zz__srfi4z00,
		BgL_bgl_string2383za700za7za7_2628za7, "&u64vector-length", 17);
	      DEFINE_STRING(BGl_string2384z00zz__srfi4z00,
		BgL_bgl_string2384za700za7za7_2629za7, "u64vector", 9);
	      DEFINE_STRING(BGl_string2385z00zz__srfi4z00,
		BgL_bgl_string2385za700za7za7_2630za7, "&f32vector-length", 17);
	      DEFINE_STRING(BGl_string2386z00zz__srfi4z00,
		BgL_bgl_string2386za700za7za7_2631za7, "f32vector", 9);
	      DEFINE_STRING(BGl_string2387z00zz__srfi4z00,
		BgL_bgl_string2387za700za7za7_2632za7, "&f64vector-length", 17);
	      DEFINE_STRING(BGl_string2388z00zz__srfi4z00,
		BgL_bgl_string2388za700za7za7_2633za7, "f64vector", 9);
	      DEFINE_STRING(BGl_string2389z00zz__srfi4z00,
		BgL_bgl_string2389za700za7za7_2634za7, "_make-s8vector", 14);
	extern obj_t BGl_zd3u64zd2envz01zz__r4_numbers_6_5_fixnumz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_f32vectorzf3zd2envz21zz__srfi4z00,
		BgL_bgl_za762f32vectorza7f3za72635za7, BGl_z62f32vectorzf3z91zz__srfi4z00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_f32vectorzd2ze3listzd2envze3zz__srfi4z00,
		BgL_bgl_za762f32vectorza7d2za72636za7,
		BGl_z62f32vectorzd2ze3listz53zz__srfi4z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_u8vectorzd2ze3listzd2envze3zz__srfi4z00,
		BgL_bgl_za762u8vectorza7d2za7e2637za7,
		BGl_z62u8vectorzd2ze3listz53zz__srfi4z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2u8vectorzd2envz00zz__srfi4z00,
		BgL_bgl__makeza7d2u8vector2638za7, opt_generic_entry,
		BGl__makezd2u8vectorzd2zz__srfi4z00, BFALSE, -1);
	      DEFINE_STRING(BGl_string2390z00zz__srfi4z00,
		BgL_bgl_string2390za700za7za7_2639za7, "bint", 4);
	      DEFINE_STRING(BGl_string2391z00zz__srfi4z00,
		BgL_bgl_string2391za700za7za7_2640za7, "bint8", 5);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_s64vectorzd2envzd2zz__srfi4z00,
		BgL_bgl_za762s64vectorza762za72641za7, va_generic_entry,
		BGl_z62s64vectorz62zz__srfi4z00, BUNSPEC, -1);
	      DEFINE_STRING(BGl_string2392z00zz__srfi4z00,
		BgL_bgl_string2392za700za7za7_2642za7, "_make-u8vector", 14);
	      DEFINE_STRING(BGl_string2393z00zz__srfi4z00,
		BgL_bgl_string2393za700za7za7_2643za7, "buint8", 6);
	      DEFINE_STRING(BGl_string2394z00zz__srfi4z00,
		BgL_bgl_string2394za700za7za7_2644za7, "_make-s16vector", 15);
	      DEFINE_STRING(BGl_string2395z00zz__srfi4z00,
		BgL_bgl_string2395za700za7za7_2645za7, "bint16", 6);
	      DEFINE_STRING(BGl_string2396z00zz__srfi4z00,
		BgL_bgl_string2396za700za7za7_2646za7, "_make-u16vector", 15);
	      DEFINE_STRING(BGl_string2397z00zz__srfi4z00,
		BgL_bgl_string2397za700za7za7_2647za7, "buint16", 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_s16vectorzd2copyz12zd2envz12zz__srfi4z00,
		BgL_bgl__s16vectorza7d2cop2648za7, opt_generic_entry,
		BGl__s16vectorzd2copyz12zc0zz__srfi4z00, BFALSE, -1);
	      DEFINE_STRING(BGl_string2398z00zz__srfi4z00,
		BgL_bgl_string2398za700za7za7_2649za7, "_make-s32vector", 15);
	      DEFINE_STRING(BGl_string2399z00zz__srfi4z00,
		BgL_bgl_string2399za700za7za7_2650za7, "bint32", 6);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_listzd2ze3u64vectorzd2envze3zz__srfi4z00,
		BgL_bgl_za762listza7d2za7e3u642651za7,
		BGl_z62listzd2ze3u64vectorz53zz__srfi4z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2u64vectorzd2envz00zz__srfi4z00,
		BgL_bgl__makeza7d2u64vecto2652za7, opt_generic_entry,
		BGl__makezd2u64vectorzd2zz__srfi4z00, BFALSE, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_u8vectorzd2lengthzd2envz00zz__srfi4z00,
		BgL_bgl_za762u8vectorza7d2le2653z00,
		BGl_z62u8vectorzd2lengthzb0zz__srfi4z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_u64vectorzd2ze3listzd2envze3zz__srfi4z00,
		BgL_bgl_za762u64vectorza7d2za72654za7,
		BGl_z62u64vectorzd2ze3listz53zz__srfi4z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_s32vectorzd2copyz12zd2envz12zz__srfi4z00,
		BgL_bgl__s32vectorza7d2cop2655za7, opt_generic_entry,
		BGl__s32vectorzd2copyz12zc0zz__srfi4z00, BFALSE, -1);
	extern obj_t BGl_zd3s16zd2envz01zz__r4_numbers_6_5_fixnumz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_u8vectorzd2envzd2zz__srfi4z00,
		BgL_bgl_za762u8vectorza762za7za72656z00, va_generic_entry,
		BGl_z62u8vectorz62zz__srfi4z00, BUNSPEC, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_homogeneouszd2vectorzd2infozd2envzd2zz__srfi4z00,
		BgL_bgl_za762homogeneousza7d2657z00,
		BGl_z62homogeneouszd2vectorzd2infoz62zz__srfi4z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_f64vectorzd2ze3listzd2envze3zz__srfi4z00,
		BgL_bgl_za762f64vectorza7d2za72658za7,
		BGl_z62f64vectorzd2ze3listz53zz__srfi4z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_u32vectorzf3zd2envz21zz__srfi4z00,
		BgL_bgl_za762u32vectorza7f3za72659za7, BGl_z62u32vectorzf3z91zz__srfi4z00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_u32vectorzd2envzd2zz__srfi4z00,
		BgL_bgl_za762u32vectorza762za72660za7, va_generic_entry,
		BGl_z62u32vectorz62zz__srfi4z00, BUNSPEC, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_s64vectorzf3zd2envz21zz__srfi4z00,
		BgL_bgl_za762s64vectorza7f3za72661za7, BGl_z62s64vectorzf3z91zz__srfi4z00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_listzd2ze3s32vectorzd2envze3zz__srfi4z00,
		BgL_bgl_za762listza7d2za7e3s322662za7,
		BGl_z62listzd2ze3s32vectorz53zz__srfi4z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2s32vectorzd2envz00zz__srfi4z00,
		BgL_bgl__makeza7d2s32vecto2663za7, opt_generic_entry,
		BGl__makezd2s32vectorzd2zz__srfi4z00, BFALSE, -1);
	extern obj_t BGl_zd3s64zd2envz01zz__r4_numbers_6_5_fixnumz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_f64vectorzd2envzd2zz__srfi4z00,
		BgL_bgl_za762f64vectorza762za72664za7, va_generic_entry,
		BGl_z62f64vectorz62zz__srfi4z00, BUNSPEC, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_listzd2ze3u8vectorzd2envze3zz__srfi4z00,
		BgL_bgl_za762listza7d2za7e3u8v2665za7,
		BGl_z62listzd2ze3u8vectorz53zz__srfi4z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_u16vectorzd2copyz12zd2envz12zz__srfi4z00,
		BgL_bgl__u16vectorza7d2cop2666za7, opt_generic_entry,
		BGl__u16vectorzd2copyz12zc0zz__srfi4z00, BFALSE, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_u64vectorzd2lengthzd2envz00zz__srfi4z00,
		BgL_bgl_za762u64vectorza7d2l2667z00,
		BGl_z62u64vectorzd2lengthzb0zz__srfi4z00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__srfi4z00));
		     ADD_ROOT((void *) (&BGl_symbol2345z00zz__srfi4z00));
		     ADD_ROOT((void *) (&BGl_symbol2347z00zz__srfi4z00));
		     ADD_ROOT((void *) (&BGl_symbol2349z00zz__srfi4z00));
		     ADD_ROOT((void *) (&BGl_symbol2351z00zz__srfi4z00));
		     ADD_ROOT((void *) (&BGl_symbol2353z00zz__srfi4z00));
		     ADD_ROOT((void *) (&BGl_symbol2355z00zz__srfi4z00));
		     ADD_ROOT((void *) (&BGl_symbol2357z00zz__srfi4z00));
		     ADD_ROOT((void *) (&BGl_symbol2359z00zz__srfi4z00));
		     ADD_ROOT((void *) (&BGl_symbol2361z00zz__srfi4z00));
		     ADD_ROOT((void *) (&BGl_symbol2363z00zz__srfi4z00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__srfi4z00(long
		BgL_checksumz00_3881, char *BgL_fromz00_3882)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__srfi4z00))
				{
					BGl_requirezd2initializa7ationz75zz__srfi4z00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__srfi4z00();
					BGl_cnstzd2initzd2zz__srfi4z00();
					BGl_importedzd2moduleszd2initz00zz__srfi4z00();
					return BGl_methodzd2initzd2zz__srfi4z00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__srfi4z00(void)
	{
		{	/* Llib/srfi4.scm 15 */
			BGl_symbol2345z00zz__srfi4z00 =
				bstring_to_symbol(BGl_string2346z00zz__srfi4z00);
			BGl_symbol2347z00zz__srfi4z00 =
				bstring_to_symbol(BGl_string2348z00zz__srfi4z00);
			BGl_symbol2349z00zz__srfi4z00 =
				bstring_to_symbol(BGl_string2350z00zz__srfi4z00);
			BGl_symbol2351z00zz__srfi4z00 =
				bstring_to_symbol(BGl_string2352z00zz__srfi4z00);
			BGl_symbol2353z00zz__srfi4z00 =
				bstring_to_symbol(BGl_string2354z00zz__srfi4z00);
			BGl_symbol2355z00zz__srfi4z00 =
				bstring_to_symbol(BGl_string2356z00zz__srfi4z00);
			BGl_symbol2357z00zz__srfi4z00 =
				bstring_to_symbol(BGl_string2358z00zz__srfi4z00);
			BGl_symbol2359z00zz__srfi4z00 =
				bstring_to_symbol(BGl_string2360z00zz__srfi4z00);
			BGl_symbol2361z00zz__srfi4z00 =
				bstring_to_symbol(BGl_string2362z00zz__srfi4z00);
			return (BGl_symbol2363z00zz__srfi4z00 =
				bstring_to_symbol(BGl_string2364z00zz__srfi4z00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__srfi4z00(void)
	{
		{	/* Llib/srfi4.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* homogeneous-vector? */
	BGL_EXPORTED_DEF obj_t BGl_homogeneouszd2vectorzf3z21zz__srfi4z00(obj_t
		BgL_xz00_3)
	{
		{	/* Llib/srfi4.scm 569 */
			return BBOOL(BGL_HVECTORP(BgL_xz00_3));
		}

	}



/* &homogeneous-vector? */
	obj_t BGl_z62homogeneouszd2vectorzf3z43zz__srfi4z00(obj_t BgL_envz00_3213,
		obj_t BgL_xz00_3214)
	{
		{	/* Llib/srfi4.scm 569 */
			return BGl_homogeneouszd2vectorzf3z21zz__srfi4z00(BgL_xz00_3214);
		}

	}



/* s8vector? */
	BGL_EXPORTED_DEF bool_t BGl_s8vectorzf3zf3zz__srfi4z00(obj_t BgL_xz00_4)
	{
		{	/* Llib/srfi4.scm 570 */
			return BGL_S8VECTORP(BgL_xz00_4);
		}

	}



/* &s8vector? */
	obj_t BGl_z62s8vectorzf3z91zz__srfi4z00(obj_t BgL_envz00_3215,
		obj_t BgL_xz00_3216)
	{
		{	/* Llib/srfi4.scm 570 */
			return BBOOL(BGl_s8vectorzf3zf3zz__srfi4z00(BgL_xz00_3216));
		}

	}



/* u8vector? */
	BGL_EXPORTED_DEF bool_t BGl_u8vectorzf3zf3zz__srfi4z00(obj_t BgL_xz00_5)
	{
		{	/* Llib/srfi4.scm 571 */
			return BGL_U8VECTORP(BgL_xz00_5);
		}

	}



/* &u8vector? */
	obj_t BGl_z62u8vectorzf3z91zz__srfi4z00(obj_t BgL_envz00_3217,
		obj_t BgL_xz00_3218)
	{
		{	/* Llib/srfi4.scm 571 */
			return BBOOL(BGl_u8vectorzf3zf3zz__srfi4z00(BgL_xz00_3218));
		}

	}



/* s16vector? */
	BGL_EXPORTED_DEF bool_t BGl_s16vectorzf3zf3zz__srfi4z00(obj_t BgL_xz00_6)
	{
		{	/* Llib/srfi4.scm 572 */
			return BGL_S16VECTORP(BgL_xz00_6);
		}

	}



/* &s16vector? */
	obj_t BGl_z62s16vectorzf3z91zz__srfi4z00(obj_t BgL_envz00_3219,
		obj_t BgL_xz00_3220)
	{
		{	/* Llib/srfi4.scm 572 */
			return BBOOL(BGl_s16vectorzf3zf3zz__srfi4z00(BgL_xz00_3220));
		}

	}



/* u16vector? */
	BGL_EXPORTED_DEF bool_t BGl_u16vectorzf3zf3zz__srfi4z00(obj_t BgL_xz00_7)
	{
		{	/* Llib/srfi4.scm 573 */
			return BGL_U16VECTORP(BgL_xz00_7);
		}

	}



/* &u16vector? */
	obj_t BGl_z62u16vectorzf3z91zz__srfi4z00(obj_t BgL_envz00_3221,
		obj_t BgL_xz00_3222)
	{
		{	/* Llib/srfi4.scm 573 */
			return BBOOL(BGl_u16vectorzf3zf3zz__srfi4z00(BgL_xz00_3222));
		}

	}



/* s32vector? */
	BGL_EXPORTED_DEF bool_t BGl_s32vectorzf3zf3zz__srfi4z00(obj_t BgL_xz00_8)
	{
		{	/* Llib/srfi4.scm 574 */
			return BGL_S32VECTORP(BgL_xz00_8);
		}

	}



/* &s32vector? */
	obj_t BGl_z62s32vectorzf3z91zz__srfi4z00(obj_t BgL_envz00_3223,
		obj_t BgL_xz00_3224)
	{
		{	/* Llib/srfi4.scm 574 */
			return BBOOL(BGl_s32vectorzf3zf3zz__srfi4z00(BgL_xz00_3224));
		}

	}



/* u32vector? */
	BGL_EXPORTED_DEF bool_t BGl_u32vectorzf3zf3zz__srfi4z00(obj_t BgL_xz00_9)
	{
		{	/* Llib/srfi4.scm 575 */
			return BGL_U32VECTORP(BgL_xz00_9);
		}

	}



/* &u32vector? */
	obj_t BGl_z62u32vectorzf3z91zz__srfi4z00(obj_t BgL_envz00_3225,
		obj_t BgL_xz00_3226)
	{
		{	/* Llib/srfi4.scm 575 */
			return BBOOL(BGl_u32vectorzf3zf3zz__srfi4z00(BgL_xz00_3226));
		}

	}



/* s64vector? */
	BGL_EXPORTED_DEF bool_t BGl_s64vectorzf3zf3zz__srfi4z00(obj_t BgL_xz00_10)
	{
		{	/* Llib/srfi4.scm 576 */
			return BGL_S64VECTORP(BgL_xz00_10);
		}

	}



/* &s64vector? */
	obj_t BGl_z62s64vectorzf3z91zz__srfi4z00(obj_t BgL_envz00_3227,
		obj_t BgL_xz00_3228)
	{
		{	/* Llib/srfi4.scm 576 */
			return BBOOL(BGl_s64vectorzf3zf3zz__srfi4z00(BgL_xz00_3228));
		}

	}



/* u64vector? */
	BGL_EXPORTED_DEF bool_t BGl_u64vectorzf3zf3zz__srfi4z00(obj_t BgL_xz00_11)
	{
		{	/* Llib/srfi4.scm 577 */
			return BGL_U64VECTORP(BgL_xz00_11);
		}

	}



/* &u64vector? */
	obj_t BGl_z62u64vectorzf3z91zz__srfi4z00(obj_t BgL_envz00_3229,
		obj_t BgL_xz00_3230)
	{
		{	/* Llib/srfi4.scm 577 */
			return BBOOL(BGl_u64vectorzf3zf3zz__srfi4z00(BgL_xz00_3230));
		}

	}



/* f32vector? */
	BGL_EXPORTED_DEF bool_t BGl_f32vectorzf3zf3zz__srfi4z00(obj_t BgL_xz00_12)
	{
		{	/* Llib/srfi4.scm 578 */
			return BGL_F32VECTORP(BgL_xz00_12);
		}

	}



/* &f32vector? */
	obj_t BGl_z62f32vectorzf3z91zz__srfi4z00(obj_t BgL_envz00_3231,
		obj_t BgL_xz00_3232)
	{
		{	/* Llib/srfi4.scm 578 */
			return BBOOL(BGl_f32vectorzf3zf3zz__srfi4z00(BgL_xz00_3232));
		}

	}



/* f64vector? */
	BGL_EXPORTED_DEF bool_t BGl_f64vectorzf3zf3zz__srfi4z00(obj_t BgL_xz00_13)
	{
		{	/* Llib/srfi4.scm 579 */
			return BGL_F64VECTORP(BgL_xz00_13);
		}

	}



/* &f64vector? */
	obj_t BGl_z62f64vectorzf3z91zz__srfi4z00(obj_t BgL_envz00_3233,
		obj_t BgL_xz00_3234)
	{
		{	/* Llib/srfi4.scm 579 */
			return BBOOL(BGl_f64vectorzf3zf3zz__srfi4z00(BgL_xz00_3234));
		}

	}



/* homogeneous-vector-info */
	BGL_EXPORTED_DEF obj_t BGl_homogeneouszd2vectorzd2infoz00zz__srfi4z00(obj_t
		BgL_oz00_14)
	{
		{	/* Llib/srfi4.scm 584 */
			if (BGL_HVECTORP(BgL_oz00_14))
				{	/* Llib/srfi4.scm 586 */
					int BgL_aux1043z00_1343;

					BgL_aux1043z00_1343 = BGL_HVECTOR_IDENT(BgL_oz00_14);
					switch ((long) (BgL_aux1043z00_1343))
						{
						case 0L:

							{	/* Llib/srfi4.scm 590 */
								obj_t BgL_val0_1145z00_1344;

								BgL_val0_1145z00_1344 = BGl_symbol2345z00zz__srfi4z00;
								{	/* Llib/srfi4.scm 590 */
									int BgL_tmpz00_3937;

									BgL_tmpz00_3937 = (int) (5L);
									BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3937);
								}
								{	/* Llib/srfi4.scm 590 */
									obj_t BgL_auxz00_3942;
									int BgL_tmpz00_3940;

									BgL_auxz00_3942 = BINT(1L);
									BgL_tmpz00_3940 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3940, BgL_auxz00_3942);
								}
								{	/* Llib/srfi4.scm 590 */
									int BgL_tmpz00_3945;

									BgL_tmpz00_3945 = (int) (2L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3945,
										BGl_s8vectorzd2refzd2envz00zz__srfi4z00);
								}
								{	/* Llib/srfi4.scm 590 */
									int BgL_tmpz00_3948;

									BgL_tmpz00_3948 = (int) (3L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3948,
										BGl_s8vectorzd2setz12zd2envz12zz__srfi4z00);
								}
								{	/* Llib/srfi4.scm 590 */
									int BgL_tmpz00_3951;

									BgL_tmpz00_3951 = (int) (4L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3951,
										BGl_zd3s8zd2envz01zz__r4_numbers_6_5_fixnumz00);
								}
								return BgL_val0_1145z00_1344;
							}
							break;
						case 1L:

							{	/* Llib/srfi4.scm 592 */
								obj_t BgL_val0_1150z00_1349;

								BgL_val0_1150z00_1349 = BGl_symbol2347z00zz__srfi4z00;
								{	/* Llib/srfi4.scm 592 */
									int BgL_tmpz00_3954;

									BgL_tmpz00_3954 = (int) (5L);
									BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3954);
								}
								{	/* Llib/srfi4.scm 592 */
									obj_t BgL_auxz00_3959;
									int BgL_tmpz00_3957;

									BgL_auxz00_3959 = BINT(1L);
									BgL_tmpz00_3957 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3957, BgL_auxz00_3959);
								}
								{	/* Llib/srfi4.scm 592 */
									int BgL_tmpz00_3962;

									BgL_tmpz00_3962 = (int) (2L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3962,
										BGl_u8vectorzd2refzd2envz00zz__srfi4z00);
								}
								{	/* Llib/srfi4.scm 592 */
									int BgL_tmpz00_3965;

									BgL_tmpz00_3965 = (int) (3L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3965,
										BGl_u8vectorzd2setz12zd2envz12zz__srfi4z00);
								}
								{	/* Llib/srfi4.scm 592 */
									int BgL_tmpz00_3968;

									BgL_tmpz00_3968 = (int) (4L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3968,
										BGl_zd3u8zd2envz01zz__r4_numbers_6_5_fixnumz00);
								}
								return BgL_val0_1150z00_1349;
							}
							break;
						case 2L:

							{	/* Llib/srfi4.scm 594 */
								obj_t BgL_val0_1155z00_1354;

								BgL_val0_1155z00_1354 = BGl_symbol2349z00zz__srfi4z00;
								{	/* Llib/srfi4.scm 594 */
									int BgL_tmpz00_3971;

									BgL_tmpz00_3971 = (int) (5L);
									BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3971);
								}
								{	/* Llib/srfi4.scm 594 */
									obj_t BgL_auxz00_3976;
									int BgL_tmpz00_3974;

									BgL_auxz00_3976 = BINT(2L);
									BgL_tmpz00_3974 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3974, BgL_auxz00_3976);
								}
								{	/* Llib/srfi4.scm 594 */
									int BgL_tmpz00_3979;

									BgL_tmpz00_3979 = (int) (2L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3979,
										BGl_s16vectorzd2refzd2envz00zz__srfi4z00);
								}
								{	/* Llib/srfi4.scm 594 */
									int BgL_tmpz00_3982;

									BgL_tmpz00_3982 = (int) (3L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3982,
										BGl_s16vectorzd2setz12zd2envz12zz__srfi4z00);
								}
								{	/* Llib/srfi4.scm 594 */
									int BgL_tmpz00_3985;

									BgL_tmpz00_3985 = (int) (4L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3985,
										BGl_zd3s16zd2envz01zz__r4_numbers_6_5_fixnumz00);
								}
								return BgL_val0_1155z00_1354;
							}
							break;
						case 3L:

							{	/* Llib/srfi4.scm 596 */
								obj_t BgL_val0_1160z00_1359;

								BgL_val0_1160z00_1359 = BGl_symbol2351z00zz__srfi4z00;
								{	/* Llib/srfi4.scm 596 */
									int BgL_tmpz00_3988;

									BgL_tmpz00_3988 = (int) (5L);
									BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3988);
								}
								{	/* Llib/srfi4.scm 596 */
									obj_t BgL_auxz00_3993;
									int BgL_tmpz00_3991;

									BgL_auxz00_3993 = BINT(2L);
									BgL_tmpz00_3991 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3991, BgL_auxz00_3993);
								}
								{	/* Llib/srfi4.scm 596 */
									int BgL_tmpz00_3996;

									BgL_tmpz00_3996 = (int) (2L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3996,
										BGl_u16vectorzd2refzd2envz00zz__srfi4z00);
								}
								{	/* Llib/srfi4.scm 596 */
									int BgL_tmpz00_3999;

									BgL_tmpz00_3999 = (int) (3L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3999,
										BGl_u16vectorzd2setz12zd2envz12zz__srfi4z00);
								}
								{	/* Llib/srfi4.scm 596 */
									int BgL_tmpz00_4002;

									BgL_tmpz00_4002 = (int) (4L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_4002,
										BGl_zd3u16zd2envz01zz__r4_numbers_6_5_fixnumz00);
								}
								return BgL_val0_1160z00_1359;
							}
							break;
						case 4L:

							{	/* Llib/srfi4.scm 598 */
								obj_t BgL_val0_1165z00_1364;

								BgL_val0_1165z00_1364 = BGl_symbol2353z00zz__srfi4z00;
								{	/* Llib/srfi4.scm 598 */
									int BgL_tmpz00_4005;

									BgL_tmpz00_4005 = (int) (5L);
									BGL_MVALUES_NUMBER_SET(BgL_tmpz00_4005);
								}
								{	/* Llib/srfi4.scm 598 */
									obj_t BgL_auxz00_4010;
									int BgL_tmpz00_4008;

									BgL_auxz00_4010 = BINT(4L);
									BgL_tmpz00_4008 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_4008, BgL_auxz00_4010);
								}
								{	/* Llib/srfi4.scm 598 */
									int BgL_tmpz00_4013;

									BgL_tmpz00_4013 = (int) (2L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_4013,
										BGl_s32vectorzd2refzd2envz00zz__srfi4z00);
								}
								{	/* Llib/srfi4.scm 598 */
									int BgL_tmpz00_4016;

									BgL_tmpz00_4016 = (int) (3L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_4016,
										BGl_s32vectorzd2setz12zd2envz12zz__srfi4z00);
								}
								{	/* Llib/srfi4.scm 598 */
									int BgL_tmpz00_4019;

									BgL_tmpz00_4019 = (int) (4L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_4019,
										BGl_zd3s32zd2envz01zz__r4_numbers_6_5_fixnumz00);
								}
								return BgL_val0_1165z00_1364;
							}
							break;
						case 5L:

							{	/* Llib/srfi4.scm 600 */
								obj_t BgL_val0_1170z00_1369;

								BgL_val0_1170z00_1369 = BGl_symbol2355z00zz__srfi4z00;
								{	/* Llib/srfi4.scm 600 */
									int BgL_tmpz00_4022;

									BgL_tmpz00_4022 = (int) (5L);
									BGL_MVALUES_NUMBER_SET(BgL_tmpz00_4022);
								}
								{	/* Llib/srfi4.scm 600 */
									obj_t BgL_auxz00_4027;
									int BgL_tmpz00_4025;

									BgL_auxz00_4027 = BINT(4L);
									BgL_tmpz00_4025 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_4025, BgL_auxz00_4027);
								}
								{	/* Llib/srfi4.scm 600 */
									int BgL_tmpz00_4030;

									BgL_tmpz00_4030 = (int) (2L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_4030,
										BGl_u32vectorzd2refzd2envz00zz__srfi4z00);
								}
								{	/* Llib/srfi4.scm 600 */
									int BgL_tmpz00_4033;

									BgL_tmpz00_4033 = (int) (3L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_4033,
										BGl_u32vectorzd2setz12zd2envz12zz__srfi4z00);
								}
								{	/* Llib/srfi4.scm 600 */
									int BgL_tmpz00_4036;

									BgL_tmpz00_4036 = (int) (4L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_4036,
										BGl_zd3u32zd2envz01zz__r4_numbers_6_5_fixnumz00);
								}
								return BgL_val0_1170z00_1369;
							}
							break;
						case 6L:

							{	/* Llib/srfi4.scm 602 */
								obj_t BgL_val0_1175z00_1374;

								BgL_val0_1175z00_1374 = BGl_symbol2357z00zz__srfi4z00;
								{	/* Llib/srfi4.scm 602 */
									int BgL_tmpz00_4039;

									BgL_tmpz00_4039 = (int) (5L);
									BGL_MVALUES_NUMBER_SET(BgL_tmpz00_4039);
								}
								{	/* Llib/srfi4.scm 602 */
									obj_t BgL_auxz00_4044;
									int BgL_tmpz00_4042;

									BgL_auxz00_4044 = BINT(8L);
									BgL_tmpz00_4042 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_4042, BgL_auxz00_4044);
								}
								{	/* Llib/srfi4.scm 602 */
									int BgL_tmpz00_4047;

									BgL_tmpz00_4047 = (int) (2L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_4047,
										BGl_s64vectorzd2refzd2envz00zz__srfi4z00);
								}
								{	/* Llib/srfi4.scm 602 */
									int BgL_tmpz00_4050;

									BgL_tmpz00_4050 = (int) (3L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_4050,
										BGl_s64vectorzd2setz12zd2envz12zz__srfi4z00);
								}
								{	/* Llib/srfi4.scm 602 */
									int BgL_tmpz00_4053;

									BgL_tmpz00_4053 = (int) (4L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_4053,
										BGl_zd3s64zd2envz01zz__r4_numbers_6_5_fixnumz00);
								}
								return BgL_val0_1175z00_1374;
							}
							break;
						case 7L:

							{	/* Llib/srfi4.scm 604 */
								obj_t BgL_val0_1180z00_1379;

								BgL_val0_1180z00_1379 = BGl_symbol2359z00zz__srfi4z00;
								{	/* Llib/srfi4.scm 604 */
									int BgL_tmpz00_4056;

									BgL_tmpz00_4056 = (int) (5L);
									BGL_MVALUES_NUMBER_SET(BgL_tmpz00_4056);
								}
								{	/* Llib/srfi4.scm 604 */
									obj_t BgL_auxz00_4061;
									int BgL_tmpz00_4059;

									BgL_auxz00_4061 = BINT(8L);
									BgL_tmpz00_4059 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_4059, BgL_auxz00_4061);
								}
								{	/* Llib/srfi4.scm 604 */
									int BgL_tmpz00_4064;

									BgL_tmpz00_4064 = (int) (2L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_4064,
										BGl_u64vectorzd2refzd2envz00zz__srfi4z00);
								}
								{	/* Llib/srfi4.scm 604 */
									int BgL_tmpz00_4067;

									BgL_tmpz00_4067 = (int) (3L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_4067,
										BGl_u64vectorzd2setz12zd2envz12zz__srfi4z00);
								}
								{	/* Llib/srfi4.scm 604 */
									int BgL_tmpz00_4070;

									BgL_tmpz00_4070 = (int) (4L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_4070,
										BGl_zd3u64zd2envz01zz__r4_numbers_6_5_fixnumz00);
								}
								return BgL_val0_1180z00_1379;
							}
							break;
						case 8L:

							{	/* Llib/srfi4.scm 606 */
								obj_t BgL_val0_1185z00_1384;

								BgL_val0_1185z00_1384 = BGl_symbol2361z00zz__srfi4z00;
								{	/* Llib/srfi4.scm 606 */
									int BgL_tmpz00_4073;

									BgL_tmpz00_4073 = (int) (5L);
									BGL_MVALUES_NUMBER_SET(BgL_tmpz00_4073);
								}
								{	/* Llib/srfi4.scm 606 */
									obj_t BgL_auxz00_4078;
									int BgL_tmpz00_4076;

									BgL_auxz00_4078 = BINT(4L);
									BgL_tmpz00_4076 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_4076, BgL_auxz00_4078);
								}
								{	/* Llib/srfi4.scm 606 */
									int BgL_tmpz00_4081;

									BgL_tmpz00_4081 = (int) (2L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_4081,
										BGl_f32vectorzd2refzd2envz00zz__srfi4z00);
								}
								{	/* Llib/srfi4.scm 606 */
									int BgL_tmpz00_4084;

									BgL_tmpz00_4084 = (int) (3L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_4084,
										BGl_f32vectorzd2setz12zd2envz12zz__srfi4z00);
								}
								{	/* Llib/srfi4.scm 606 */
									int BgL_tmpz00_4087;

									BgL_tmpz00_4087 = (int) (4L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_4087,
										BGl_zd3flzd2envz01zz__r4_numbers_6_5_flonumz00);
								}
								return BgL_val0_1185z00_1384;
							}
							break;
						case 9L:

							{	/* Llib/srfi4.scm 608 */
								obj_t BgL_val0_1190z00_1389;

								BgL_val0_1190z00_1389 = BGl_symbol2363z00zz__srfi4z00;
								{	/* Llib/srfi4.scm 608 */
									int BgL_tmpz00_4090;

									BgL_tmpz00_4090 = (int) (5L);
									BGL_MVALUES_NUMBER_SET(BgL_tmpz00_4090);
								}
								{	/* Llib/srfi4.scm 608 */
									obj_t BgL_auxz00_4095;
									int BgL_tmpz00_4093;

									BgL_auxz00_4095 = BINT(8L);
									BgL_tmpz00_4093 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_4093, BgL_auxz00_4095);
								}
								{	/* Llib/srfi4.scm 608 */
									int BgL_tmpz00_4098;

									BgL_tmpz00_4098 = (int) (2L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_4098,
										BGl_f64vectorzd2refzd2envz00zz__srfi4z00);
								}
								{	/* Llib/srfi4.scm 608 */
									int BgL_tmpz00_4101;

									BgL_tmpz00_4101 = (int) (3L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_4101,
										BGl_f64vectorzd2setz12zd2envz12zz__srfi4z00);
								}
								{	/* Llib/srfi4.scm 608 */
									int BgL_tmpz00_4104;

									BgL_tmpz00_4104 = (int) (4L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_4104,
										BGl_zd3flzd2envz01zz__r4_numbers_6_5_flonumz00);
								}
								return BgL_val0_1190z00_1389;
							}
							break;
						default:
							{	/* Llib/srfi4.scm 611 */
								int BgL_arg1390z00_2308;

								BgL_arg1390z00_2308 = BGL_HVECTOR_IDENT(BgL_oz00_14);
								return
									BGl_errorz00zz__errorz00(BGl_string2365z00zz__srfi4z00,
									BGl_string2366z00zz__srfi4z00, BINT(BgL_arg1390z00_2308));
							}
						}
				}
			else
				{	/* Llib/srfi4.scm 585 */
					return
						BGl_bigloozd2typezd2errorz00zz__errorz00
						(BGl_string2365z00zz__srfi4z00, BGl_string2367z00zz__srfi4z00,
						BgL_oz00_14);
				}
		}

	}



/* &homogeneous-vector-info */
	obj_t BGl_z62homogeneouszd2vectorzd2infoz62zz__srfi4z00(obj_t BgL_envz00_3235,
		obj_t BgL_oz00_3236)
	{
		{	/* Llib/srfi4.scm 584 */
			return BGl_homogeneouszd2vectorzd2infoz00zz__srfi4z00(BgL_oz00_3236);
		}

	}



/* s8vector-length */
	BGL_EXPORTED_DEF long BGl_s8vectorzd2lengthzd2zz__srfi4z00(obj_t BgL_xz00_15)
	{
		{	/* Llib/srfi4.scm 617 */
			return BGL_HVECTOR_LENGTH(BgL_xz00_15);
		}

	}



/* &s8vector-length */
	obj_t BGl_z62s8vectorzd2lengthzb0zz__srfi4z00(obj_t BgL_envz00_3334,
		obj_t BgL_xz00_3335)
	{
		{	/* Llib/srfi4.scm 617 */
			{	/* Llib/srfi4.scm 617 */
				long BgL_tmpz00_4115;

				{	/* Llib/srfi4.scm 617 */
					obj_t BgL_auxz00_4116;

					if (BGL_S8VECTORP(BgL_xz00_3335))
						{	/* Llib/srfi4.scm 617 */
							BgL_auxz00_4116 = BgL_xz00_3335;
						}
					else
						{
							obj_t BgL_auxz00_4119;

							BgL_auxz00_4119 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(25371L), BGl_string2369z00zz__srfi4z00,
								BGl_string2370z00zz__srfi4z00, BgL_xz00_3335);
							FAILURE(BgL_auxz00_4119, BFALSE, BFALSE);
						}
					BgL_tmpz00_4115 =
						BGl_s8vectorzd2lengthzd2zz__srfi4z00(BgL_auxz00_4116);
				}
				return BINT(BgL_tmpz00_4115);
			}
		}

	}



/* u8vector-length */
	BGL_EXPORTED_DEF long BGl_u8vectorzd2lengthzd2zz__srfi4z00(obj_t BgL_xz00_16)
	{
		{	/* Llib/srfi4.scm 618 */
			return BGL_HVECTOR_LENGTH(BgL_xz00_16);
		}

	}



/* &u8vector-length */
	obj_t BGl_z62u8vectorzd2lengthzb0zz__srfi4z00(obj_t BgL_envz00_3336,
		obj_t BgL_xz00_3337)
	{
		{	/* Llib/srfi4.scm 618 */
			{	/* Llib/srfi4.scm 618 */
				long BgL_tmpz00_4126;

				{	/* Llib/srfi4.scm 618 */
					obj_t BgL_auxz00_4127;

					if (BGL_U8VECTORP(BgL_xz00_3337))
						{	/* Llib/srfi4.scm 618 */
							BgL_auxz00_4127 = BgL_xz00_3337;
						}
					else
						{
							obj_t BgL_auxz00_4130;

							BgL_auxz00_4130 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(25427L), BGl_string2371z00zz__srfi4z00,
								BGl_string2372z00zz__srfi4z00, BgL_xz00_3337);
							FAILURE(BgL_auxz00_4130, BFALSE, BFALSE);
						}
					BgL_tmpz00_4126 =
						BGl_u8vectorzd2lengthzd2zz__srfi4z00(BgL_auxz00_4127);
				}
				return BINT(BgL_tmpz00_4126);
			}
		}

	}



/* s16vector-length */
	BGL_EXPORTED_DEF long BGl_s16vectorzd2lengthzd2zz__srfi4z00(obj_t BgL_xz00_17)
	{
		{	/* Llib/srfi4.scm 619 */
			return BGL_HVECTOR_LENGTH(BgL_xz00_17);
		}

	}



/* &s16vector-length */
	obj_t BGl_z62s16vectorzd2lengthzb0zz__srfi4z00(obj_t BgL_envz00_3338,
		obj_t BgL_xz00_3339)
	{
		{	/* Llib/srfi4.scm 619 */
			{	/* Llib/srfi4.scm 619 */
				long BgL_tmpz00_4137;

				{	/* Llib/srfi4.scm 619 */
					obj_t BgL_auxz00_4138;

					if (BGL_S16VECTORP(BgL_xz00_3339))
						{	/* Llib/srfi4.scm 619 */
							BgL_auxz00_4138 = BgL_xz00_3339;
						}
					else
						{
							obj_t BgL_auxz00_4141;

							BgL_auxz00_4141 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(25484L), BGl_string2373z00zz__srfi4z00,
								BGl_string2374z00zz__srfi4z00, BgL_xz00_3339);
							FAILURE(BgL_auxz00_4141, BFALSE, BFALSE);
						}
					BgL_tmpz00_4137 =
						BGl_s16vectorzd2lengthzd2zz__srfi4z00(BgL_auxz00_4138);
				}
				return BINT(BgL_tmpz00_4137);
			}
		}

	}



/* u16vector-length */
	BGL_EXPORTED_DEF long BGl_u16vectorzd2lengthzd2zz__srfi4z00(obj_t BgL_xz00_18)
	{
		{	/* Llib/srfi4.scm 620 */
			return BGL_HVECTOR_LENGTH(BgL_xz00_18);
		}

	}



/* &u16vector-length */
	obj_t BGl_z62u16vectorzd2lengthzb0zz__srfi4z00(obj_t BgL_envz00_3340,
		obj_t BgL_xz00_3341)
	{
		{	/* Llib/srfi4.scm 620 */
			{	/* Llib/srfi4.scm 620 */
				long BgL_tmpz00_4148;

				{	/* Llib/srfi4.scm 620 */
					obj_t BgL_auxz00_4149;

					if (BGL_U16VECTORP(BgL_xz00_3341))
						{	/* Llib/srfi4.scm 620 */
							BgL_auxz00_4149 = BgL_xz00_3341;
						}
					else
						{
							obj_t BgL_auxz00_4152;

							BgL_auxz00_4152 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(25541L), BGl_string2375z00zz__srfi4z00,
								BGl_string2376z00zz__srfi4z00, BgL_xz00_3341);
							FAILURE(BgL_auxz00_4152, BFALSE, BFALSE);
						}
					BgL_tmpz00_4148 =
						BGl_u16vectorzd2lengthzd2zz__srfi4z00(BgL_auxz00_4149);
				}
				return BINT(BgL_tmpz00_4148);
			}
		}

	}



/* s32vector-length */
	BGL_EXPORTED_DEF long BGl_s32vectorzd2lengthzd2zz__srfi4z00(obj_t BgL_xz00_19)
	{
		{	/* Llib/srfi4.scm 621 */
			return BGL_HVECTOR_LENGTH(BgL_xz00_19);
		}

	}



/* &s32vector-length */
	obj_t BGl_z62s32vectorzd2lengthzb0zz__srfi4z00(obj_t BgL_envz00_3342,
		obj_t BgL_xz00_3343)
	{
		{	/* Llib/srfi4.scm 621 */
			{	/* Llib/srfi4.scm 621 */
				long BgL_tmpz00_4159;

				{	/* Llib/srfi4.scm 621 */
					obj_t BgL_auxz00_4160;

					if (BGL_S32VECTORP(BgL_xz00_3343))
						{	/* Llib/srfi4.scm 621 */
							BgL_auxz00_4160 = BgL_xz00_3343;
						}
					else
						{
							obj_t BgL_auxz00_4163;

							BgL_auxz00_4163 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(25598L), BGl_string2377z00zz__srfi4z00,
								BGl_string2378z00zz__srfi4z00, BgL_xz00_3343);
							FAILURE(BgL_auxz00_4163, BFALSE, BFALSE);
						}
					BgL_tmpz00_4159 =
						BGl_s32vectorzd2lengthzd2zz__srfi4z00(BgL_auxz00_4160);
				}
				return BINT(BgL_tmpz00_4159);
			}
		}

	}



/* u32vector-length */
	BGL_EXPORTED_DEF long BGl_u32vectorzd2lengthzd2zz__srfi4z00(obj_t BgL_xz00_20)
	{
		{	/* Llib/srfi4.scm 622 */
			return BGL_HVECTOR_LENGTH(BgL_xz00_20);
		}

	}



/* &u32vector-length */
	obj_t BGl_z62u32vectorzd2lengthzb0zz__srfi4z00(obj_t BgL_envz00_3344,
		obj_t BgL_xz00_3345)
	{
		{	/* Llib/srfi4.scm 622 */
			{	/* Llib/srfi4.scm 622 */
				long BgL_tmpz00_4170;

				{	/* Llib/srfi4.scm 622 */
					obj_t BgL_auxz00_4171;

					if (BGL_U32VECTORP(BgL_xz00_3345))
						{	/* Llib/srfi4.scm 622 */
							BgL_auxz00_4171 = BgL_xz00_3345;
						}
					else
						{
							obj_t BgL_auxz00_4174;

							BgL_auxz00_4174 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(25655L), BGl_string2379z00zz__srfi4z00,
								BGl_string2380z00zz__srfi4z00, BgL_xz00_3345);
							FAILURE(BgL_auxz00_4174, BFALSE, BFALSE);
						}
					BgL_tmpz00_4170 =
						BGl_u32vectorzd2lengthzd2zz__srfi4z00(BgL_auxz00_4171);
				}
				return BINT(BgL_tmpz00_4170);
			}
		}

	}



/* s64vector-length */
	BGL_EXPORTED_DEF long BGl_s64vectorzd2lengthzd2zz__srfi4z00(obj_t BgL_xz00_21)
	{
		{	/* Llib/srfi4.scm 623 */
			return BGL_HVECTOR_LENGTH(BgL_xz00_21);
		}

	}



/* &s64vector-length */
	obj_t BGl_z62s64vectorzd2lengthzb0zz__srfi4z00(obj_t BgL_envz00_3346,
		obj_t BgL_xz00_3347)
	{
		{	/* Llib/srfi4.scm 623 */
			{	/* Llib/srfi4.scm 623 */
				long BgL_tmpz00_4181;

				{	/* Llib/srfi4.scm 623 */
					obj_t BgL_auxz00_4182;

					if (BGL_S64VECTORP(BgL_xz00_3347))
						{	/* Llib/srfi4.scm 623 */
							BgL_auxz00_4182 = BgL_xz00_3347;
						}
					else
						{
							obj_t BgL_auxz00_4185;

							BgL_auxz00_4185 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(25712L), BGl_string2381z00zz__srfi4z00,
								BGl_string2382z00zz__srfi4z00, BgL_xz00_3347);
							FAILURE(BgL_auxz00_4185, BFALSE, BFALSE);
						}
					BgL_tmpz00_4181 =
						BGl_s64vectorzd2lengthzd2zz__srfi4z00(BgL_auxz00_4182);
				}
				return BINT(BgL_tmpz00_4181);
			}
		}

	}



/* u64vector-length */
	BGL_EXPORTED_DEF long BGl_u64vectorzd2lengthzd2zz__srfi4z00(obj_t BgL_xz00_22)
	{
		{	/* Llib/srfi4.scm 624 */
			return BGL_HVECTOR_LENGTH(BgL_xz00_22);
		}

	}



/* &u64vector-length */
	obj_t BGl_z62u64vectorzd2lengthzb0zz__srfi4z00(obj_t BgL_envz00_3348,
		obj_t BgL_xz00_3349)
	{
		{	/* Llib/srfi4.scm 624 */
			{	/* Llib/srfi4.scm 624 */
				long BgL_tmpz00_4192;

				{	/* Llib/srfi4.scm 624 */
					obj_t BgL_auxz00_4193;

					if (BGL_U64VECTORP(BgL_xz00_3349))
						{	/* Llib/srfi4.scm 624 */
							BgL_auxz00_4193 = BgL_xz00_3349;
						}
					else
						{
							obj_t BgL_auxz00_4196;

							BgL_auxz00_4196 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(25769L), BGl_string2383z00zz__srfi4z00,
								BGl_string2384z00zz__srfi4z00, BgL_xz00_3349);
							FAILURE(BgL_auxz00_4196, BFALSE, BFALSE);
						}
					BgL_tmpz00_4192 =
						BGl_u64vectorzd2lengthzd2zz__srfi4z00(BgL_auxz00_4193);
				}
				return BINT(BgL_tmpz00_4192);
			}
		}

	}



/* f32vector-length */
	BGL_EXPORTED_DEF long BGl_f32vectorzd2lengthzd2zz__srfi4z00(obj_t BgL_xz00_23)
	{
		{	/* Llib/srfi4.scm 625 */
			return BGL_HVECTOR_LENGTH(BgL_xz00_23);
		}

	}



/* &f32vector-length */
	obj_t BGl_z62f32vectorzd2lengthzb0zz__srfi4z00(obj_t BgL_envz00_3350,
		obj_t BgL_xz00_3351)
	{
		{	/* Llib/srfi4.scm 625 */
			{	/* Llib/srfi4.scm 625 */
				long BgL_tmpz00_4203;

				{	/* Llib/srfi4.scm 625 */
					obj_t BgL_auxz00_4204;

					if (BGL_F32VECTORP(BgL_xz00_3351))
						{	/* Llib/srfi4.scm 625 */
							BgL_auxz00_4204 = BgL_xz00_3351;
						}
					else
						{
							obj_t BgL_auxz00_4207;

							BgL_auxz00_4207 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(25826L), BGl_string2385z00zz__srfi4z00,
								BGl_string2386z00zz__srfi4z00, BgL_xz00_3351);
							FAILURE(BgL_auxz00_4207, BFALSE, BFALSE);
						}
					BgL_tmpz00_4203 =
						BGl_f32vectorzd2lengthzd2zz__srfi4z00(BgL_auxz00_4204);
				}
				return BINT(BgL_tmpz00_4203);
			}
		}

	}



/* f64vector-length */
	BGL_EXPORTED_DEF long BGl_f64vectorzd2lengthzd2zz__srfi4z00(obj_t BgL_xz00_24)
	{
		{	/* Llib/srfi4.scm 626 */
			return BGL_HVECTOR_LENGTH(BgL_xz00_24);
		}

	}



/* &f64vector-length */
	obj_t BGl_z62f64vectorzd2lengthzb0zz__srfi4z00(obj_t BgL_envz00_3352,
		obj_t BgL_xz00_3353)
	{
		{	/* Llib/srfi4.scm 626 */
			{	/* Llib/srfi4.scm 626 */
				long BgL_tmpz00_4214;

				{	/* Llib/srfi4.scm 626 */
					obj_t BgL_auxz00_4215;

					if (BGL_F64VECTORP(BgL_xz00_3353))
						{	/* Llib/srfi4.scm 626 */
							BgL_auxz00_4215 = BgL_xz00_3353;
						}
					else
						{
							obj_t BgL_auxz00_4218;

							BgL_auxz00_4218 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(25883L), BGl_string2387z00zz__srfi4z00,
								BGl_string2388z00zz__srfi4z00, BgL_xz00_3353);
							FAILURE(BgL_auxz00_4218, BFALSE, BFALSE);
						}
					BgL_tmpz00_4214 =
						BGl_f64vectorzd2lengthzd2zz__srfi4z00(BgL_auxz00_4215);
				}
				return BINT(BgL_tmpz00_4214);
			}
		}

	}



/* s8vector */
	BGL_EXPORTED_DEF obj_t BGl_s8vectorz00zz__srfi4z00(obj_t BgL_xz00_25)
	{
		{	/* Llib/srfi4.scm 637 */
			return BGl_listzd2ze3s8vectorz31zz__srfi4z00(BgL_xz00_25);
		}

	}



/* &s8vector */
	obj_t BGl_z62s8vectorz62zz__srfi4z00(obj_t BgL_envz00_3354,
		obj_t BgL_xz00_3355)
	{
		{	/* Llib/srfi4.scm 637 */
			return BGl_s8vectorz00zz__srfi4z00(BgL_xz00_3355);
		}

	}



/* u8vector */
	BGL_EXPORTED_DEF obj_t BGl_u8vectorz00zz__srfi4z00(obj_t BgL_xz00_26)
	{
		{	/* Llib/srfi4.scm 637 */
			return BGl_listzd2ze3u8vectorz31zz__srfi4z00(BgL_xz00_26);
		}

	}



/* &u8vector */
	obj_t BGl_z62u8vectorz62zz__srfi4z00(obj_t BgL_envz00_3356,
		obj_t BgL_xz00_3357)
	{
		{	/* Llib/srfi4.scm 637 */
			return BGl_u8vectorz00zz__srfi4z00(BgL_xz00_3357);
		}

	}



/* s16vector */
	BGL_EXPORTED_DEF obj_t BGl_s16vectorz00zz__srfi4z00(obj_t BgL_xz00_27)
	{
		{	/* Llib/srfi4.scm 637 */
			return BGl_listzd2ze3s16vectorz31zz__srfi4z00(BgL_xz00_27);
		}

	}



/* &s16vector */
	obj_t BGl_z62s16vectorz62zz__srfi4z00(obj_t BgL_envz00_3358,
		obj_t BgL_xz00_3359)
	{
		{	/* Llib/srfi4.scm 637 */
			return BGl_s16vectorz00zz__srfi4z00(BgL_xz00_3359);
		}

	}



/* u16vector */
	BGL_EXPORTED_DEF obj_t BGl_u16vectorz00zz__srfi4z00(obj_t BgL_xz00_28)
	{
		{	/* Llib/srfi4.scm 637 */
			return BGl_listzd2ze3u16vectorz31zz__srfi4z00(BgL_xz00_28);
		}

	}



/* &u16vector */
	obj_t BGl_z62u16vectorz62zz__srfi4z00(obj_t BgL_envz00_3360,
		obj_t BgL_xz00_3361)
	{
		{	/* Llib/srfi4.scm 637 */
			return BGl_u16vectorz00zz__srfi4z00(BgL_xz00_3361);
		}

	}



/* s32vector */
	BGL_EXPORTED_DEF obj_t BGl_s32vectorz00zz__srfi4z00(obj_t BgL_xz00_29)
	{
		{	/* Llib/srfi4.scm 637 */
			return BGl_listzd2ze3s32vectorz31zz__srfi4z00(BgL_xz00_29);
		}

	}



/* &s32vector */
	obj_t BGl_z62s32vectorz62zz__srfi4z00(obj_t BgL_envz00_3362,
		obj_t BgL_xz00_3363)
	{
		{	/* Llib/srfi4.scm 637 */
			return BGl_s32vectorz00zz__srfi4z00(BgL_xz00_3363);
		}

	}



/* u32vector */
	BGL_EXPORTED_DEF obj_t BGl_u32vectorz00zz__srfi4z00(obj_t BgL_xz00_30)
	{
		{	/* Llib/srfi4.scm 637 */
			return BGl_listzd2ze3u32vectorz31zz__srfi4z00(BgL_xz00_30);
		}

	}



/* &u32vector */
	obj_t BGl_z62u32vectorz62zz__srfi4z00(obj_t BgL_envz00_3364,
		obj_t BgL_xz00_3365)
	{
		{	/* Llib/srfi4.scm 637 */
			return BGl_u32vectorz00zz__srfi4z00(BgL_xz00_3365);
		}

	}



/* s64vector */
	BGL_EXPORTED_DEF obj_t BGl_s64vectorz00zz__srfi4z00(obj_t BgL_xz00_31)
	{
		{	/* Llib/srfi4.scm 637 */
			return BGl_listzd2ze3s64vectorz31zz__srfi4z00(BgL_xz00_31);
		}

	}



/* &s64vector */
	obj_t BGl_z62s64vectorz62zz__srfi4z00(obj_t BgL_envz00_3366,
		obj_t BgL_xz00_3367)
	{
		{	/* Llib/srfi4.scm 637 */
			return BGl_s64vectorz00zz__srfi4z00(BgL_xz00_3367);
		}

	}



/* u64vector */
	BGL_EXPORTED_DEF obj_t BGl_u64vectorz00zz__srfi4z00(obj_t BgL_xz00_32)
	{
		{	/* Llib/srfi4.scm 637 */
			return BGl_listzd2ze3u64vectorz31zz__srfi4z00(BgL_xz00_32);
		}

	}



/* &u64vector */
	obj_t BGl_z62u64vectorz62zz__srfi4z00(obj_t BgL_envz00_3368,
		obj_t BgL_xz00_3369)
	{
		{	/* Llib/srfi4.scm 637 */
			return BGl_u64vectorz00zz__srfi4z00(BgL_xz00_3369);
		}

	}



/* f32vector */
	BGL_EXPORTED_DEF obj_t BGl_f32vectorz00zz__srfi4z00(obj_t BgL_xz00_33)
	{
		{	/* Llib/srfi4.scm 637 */
			return BGl_listzd2ze3f32vectorz31zz__srfi4z00(BgL_xz00_33);
		}

	}



/* &f32vector */
	obj_t BGl_z62f32vectorz62zz__srfi4z00(obj_t BgL_envz00_3370,
		obj_t BgL_xz00_3371)
	{
		{	/* Llib/srfi4.scm 637 */
			return BGl_f32vectorz00zz__srfi4z00(BgL_xz00_3371);
		}

	}



/* f64vector */
	BGL_EXPORTED_DEF obj_t BGl_f64vectorz00zz__srfi4z00(obj_t BgL_xz00_34)
	{
		{	/* Llib/srfi4.scm 637 */
			return BGl_listzd2ze3f64vectorz31zz__srfi4z00(BgL_xz00_34);
		}

	}



/* &f64vector */
	obj_t BGl_z62f64vectorz62zz__srfi4z00(obj_t BgL_envz00_3372,
		obj_t BgL_xz00_3373)
	{
		{	/* Llib/srfi4.scm 637 */
			return BGl_f64vectorz00zz__srfi4z00(BgL_xz00_3373);
		}

	}



/* _make-s8vector */
	obj_t BGl__makezd2s8vectorzd2zz__srfi4z00(obj_t BgL_env1229z00_38,
		obj_t BgL_opt1228z00_37)
	{
		{	/* Llib/srfi4.scm 661 */
			{	/* Llib/srfi4.scm 661 */
				obj_t BgL_g1230z00_1395;

				BgL_g1230z00_1395 = VECTOR_REF(BgL_opt1228z00_37, 0L);
				switch (VECTOR_LENGTH(BgL_opt1228z00_37))
					{
					case 1L:

						{	/* Llib/srfi4.scm 661 */

							{	/* Llib/srfi4.scm 661 */
								long BgL_auxz00_4245;

								{	/* Llib/srfi4.scm 661 */
									obj_t BgL_tmpz00_4246;

									if (INTEGERP(BgL_g1230z00_1395))
										{	/* Llib/srfi4.scm 661 */
											BgL_tmpz00_4246 = BgL_g1230z00_1395;
										}
									else
										{
											obj_t BgL_auxz00_4249;

											BgL_auxz00_4249 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(27216L),
												BGl_string2389z00zz__srfi4z00,
												BGl_string2390z00zz__srfi4z00, BgL_g1230z00_1395);
											FAILURE(BgL_auxz00_4249, BFALSE, BFALSE);
										}
									BgL_auxz00_4245 = (long) CINT(BgL_tmpz00_4246);
								}
								return
									BGl_makezd2s8vectorzd2zz__srfi4z00(BgL_auxz00_4245,
									(int8_t) (0));
							}
						}
						break;
					case 2L:

						{	/* Llib/srfi4.scm 661 */
							obj_t BgL_initz00_1399;

							BgL_initz00_1399 = VECTOR_REF(BgL_opt1228z00_37, 1L);
							{	/* Llib/srfi4.scm 661 */

								{	/* Llib/srfi4.scm 661 */
									int8_t BgL_auxz00_4265;
									long BgL_auxz00_4256;

									{	/* Llib/srfi4.scm 661 */
										obj_t BgL_tmpz00_4266;

										if (BGL_INT8P(BgL_initz00_1399))
											{	/* Llib/srfi4.scm 661 */
												BgL_tmpz00_4266 = BgL_initz00_1399;
											}
										else
											{
												obj_t BgL_auxz00_4269;

												BgL_auxz00_4269 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(27216L),
													BGl_string2389z00zz__srfi4z00,
													BGl_string2391z00zz__srfi4z00, BgL_initz00_1399);
												FAILURE(BgL_auxz00_4269, BFALSE, BFALSE);
											}
										BgL_auxz00_4265 = BGL_BINT8_TO_INT8(BgL_tmpz00_4266);
									}
									{	/* Llib/srfi4.scm 661 */
										obj_t BgL_tmpz00_4257;

										if (INTEGERP(BgL_g1230z00_1395))
											{	/* Llib/srfi4.scm 661 */
												BgL_tmpz00_4257 = BgL_g1230z00_1395;
											}
										else
											{
												obj_t BgL_auxz00_4260;

												BgL_auxz00_4260 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(27216L),
													BGl_string2389z00zz__srfi4z00,
													BGl_string2390z00zz__srfi4z00, BgL_g1230z00_1395);
												FAILURE(BgL_auxz00_4260, BFALSE, BFALSE);
											}
										BgL_auxz00_4256 = (long) CINT(BgL_tmpz00_4257);
									}
									return
										BGl_makezd2s8vectorzd2zz__srfi4z00(BgL_auxz00_4256,
										BgL_auxz00_4265);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* make-s8vector */
	BGL_EXPORTED_DEF obj_t BGl_makezd2s8vectorzd2zz__srfi4z00(long BgL_lenz00_35,
		int8_t BgL_initz00_36)
	{
		{	/* Llib/srfi4.scm 661 */
			{	/* Llib/srfi4.scm 661 */
				obj_t BgL_vz00_1400;

				{	/* Llib/srfi4.scm 661 */
					int32_t BgL_tmpz00_4277;

					BgL_tmpz00_4277 = (int32_t) (BgL_lenz00_35);
					BgL_vz00_1400 = BGL_ALLOC_S8VECTOR(BgL_tmpz00_4277);
				}
				{
					long BgL_iz00_2314;

					BgL_iz00_2314 = 0L;
				BgL_loopz00_2313:
					if ((BgL_iz00_2314 < BgL_lenz00_35))
						{	/* Llib/srfi4.scm 661 */
							BGL_S8VSET(BgL_vz00_1400, BgL_iz00_2314, BgL_initz00_36);
							BUNSPEC;
							{
								long BgL_iz00_4283;

								BgL_iz00_4283 = (BgL_iz00_2314 + 1L);
								BgL_iz00_2314 = BgL_iz00_4283;
								goto BgL_loopz00_2313;
							}
						}
					else
						{	/* Llib/srfi4.scm 661 */
							((bool_t) 0);
						}
				}
				return BgL_vz00_1400;
			}
		}

	}



/* _make-u8vector */
	obj_t BGl__makezd2u8vectorzd2zz__srfi4z00(obj_t BgL_env1234z00_42,
		obj_t BgL_opt1233z00_41)
	{
		{	/* Llib/srfi4.scm 662 */
			{	/* Llib/srfi4.scm 662 */
				obj_t BgL_g1235z00_1407;

				BgL_g1235z00_1407 = VECTOR_REF(BgL_opt1233z00_41, 0L);
				switch (VECTOR_LENGTH(BgL_opt1233z00_41))
					{
					case 1L:

						{	/* Llib/srfi4.scm 662 */

							{	/* Llib/srfi4.scm 662 */
								long BgL_auxz00_4286;

								{	/* Llib/srfi4.scm 662 */
									obj_t BgL_tmpz00_4287;

									if (INTEGERP(BgL_g1235z00_1407))
										{	/* Llib/srfi4.scm 662 */
											BgL_tmpz00_4287 = BgL_g1235z00_1407;
										}
									else
										{
											obj_t BgL_auxz00_4290;

											BgL_auxz00_4290 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(27248L),
												BGl_string2392z00zz__srfi4z00,
												BGl_string2390z00zz__srfi4z00, BgL_g1235z00_1407);
											FAILURE(BgL_auxz00_4290, BFALSE, BFALSE);
										}
									BgL_auxz00_4286 = (long) CINT(BgL_tmpz00_4287);
								}
								return
									BGl_makezd2u8vectorzd2zz__srfi4z00(BgL_auxz00_4286,
									(uint8_t) (0));
							}
						}
						break;
					case 2L:

						{	/* Llib/srfi4.scm 662 */
							obj_t BgL_initz00_1411;

							BgL_initz00_1411 = VECTOR_REF(BgL_opt1233z00_41, 1L);
							{	/* Llib/srfi4.scm 662 */

								{	/* Llib/srfi4.scm 662 */
									uint8_t BgL_auxz00_4306;
									long BgL_auxz00_4297;

									{	/* Llib/srfi4.scm 662 */
										obj_t BgL_tmpz00_4307;

										if (BGL_UINT8P(BgL_initz00_1411))
											{	/* Llib/srfi4.scm 662 */
												BgL_tmpz00_4307 = BgL_initz00_1411;
											}
										else
											{
												obj_t BgL_auxz00_4310;

												BgL_auxz00_4310 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(27248L),
													BGl_string2392z00zz__srfi4z00,
													BGl_string2393z00zz__srfi4z00, BgL_initz00_1411);
												FAILURE(BgL_auxz00_4310, BFALSE, BFALSE);
											}
										BgL_auxz00_4306 = BGL_BUINT8_TO_UINT8(BgL_tmpz00_4307);
									}
									{	/* Llib/srfi4.scm 662 */
										obj_t BgL_tmpz00_4298;

										if (INTEGERP(BgL_g1235z00_1407))
											{	/* Llib/srfi4.scm 662 */
												BgL_tmpz00_4298 = BgL_g1235z00_1407;
											}
										else
											{
												obj_t BgL_auxz00_4301;

												BgL_auxz00_4301 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(27248L),
													BGl_string2392z00zz__srfi4z00,
													BGl_string2390z00zz__srfi4z00, BgL_g1235z00_1407);
												FAILURE(BgL_auxz00_4301, BFALSE, BFALSE);
											}
										BgL_auxz00_4297 = (long) CINT(BgL_tmpz00_4298);
									}
									return
										BGl_makezd2u8vectorzd2zz__srfi4z00(BgL_auxz00_4297,
										BgL_auxz00_4306);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* make-u8vector */
	BGL_EXPORTED_DEF obj_t BGl_makezd2u8vectorzd2zz__srfi4z00(long BgL_lenz00_39,
		uint8_t BgL_initz00_40)
	{
		{	/* Llib/srfi4.scm 662 */
			{	/* Llib/srfi4.scm 662 */
				obj_t BgL_vz00_1412;

				{	/* Llib/srfi4.scm 662 */
					int32_t BgL_tmpz00_4318;

					BgL_tmpz00_4318 = (int32_t) (BgL_lenz00_39);
					BgL_vz00_1412 = BGL_ALLOC_U8VECTOR(BgL_tmpz00_4318);
				}
				{
					long BgL_iz00_2324;

					BgL_iz00_2324 = 0L;
				BgL_loopz00_2323:
					if ((BgL_iz00_2324 < BgL_lenz00_39))
						{	/* Llib/srfi4.scm 662 */
							BGL_U8VSET(BgL_vz00_1412, BgL_iz00_2324, BgL_initz00_40);
							BUNSPEC;
							{
								long BgL_iz00_4324;

								BgL_iz00_4324 = (BgL_iz00_2324 + 1L);
								BgL_iz00_2324 = BgL_iz00_4324;
								goto BgL_loopz00_2323;
							}
						}
					else
						{	/* Llib/srfi4.scm 662 */
							((bool_t) 0);
						}
				}
				return BgL_vz00_1412;
			}
		}

	}



/* _make-s16vector */
	obj_t BGl__makezd2s16vectorzd2zz__srfi4z00(obj_t BgL_env1239z00_46,
		obj_t BgL_opt1238z00_45)
	{
		{	/* Llib/srfi4.scm 663 */
			{	/* Llib/srfi4.scm 663 */
				obj_t BgL_g1240z00_1419;

				BgL_g1240z00_1419 = VECTOR_REF(BgL_opt1238z00_45, 0L);
				switch (VECTOR_LENGTH(BgL_opt1238z00_45))
					{
					case 1L:

						{	/* Llib/srfi4.scm 663 */

							{	/* Llib/srfi4.scm 663 */
								long BgL_auxz00_4327;

								{	/* Llib/srfi4.scm 663 */
									obj_t BgL_tmpz00_4328;

									if (INTEGERP(BgL_g1240z00_1419))
										{	/* Llib/srfi4.scm 663 */
											BgL_tmpz00_4328 = BgL_g1240z00_1419;
										}
									else
										{
											obj_t BgL_auxz00_4331;

											BgL_auxz00_4331 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(27280L),
												BGl_string2394z00zz__srfi4z00,
												BGl_string2390z00zz__srfi4z00, BgL_g1240z00_1419);
											FAILURE(BgL_auxz00_4331, BFALSE, BFALSE);
										}
									BgL_auxz00_4327 = (long) CINT(BgL_tmpz00_4328);
								}
								return
									BGl_makezd2s16vectorzd2zz__srfi4z00(BgL_auxz00_4327,
									(int16_t) (0));
							}
						}
						break;
					case 2L:

						{	/* Llib/srfi4.scm 663 */
							obj_t BgL_initz00_1423;

							BgL_initz00_1423 = VECTOR_REF(BgL_opt1238z00_45, 1L);
							{	/* Llib/srfi4.scm 663 */

								{	/* Llib/srfi4.scm 663 */
									int16_t BgL_auxz00_4347;
									long BgL_auxz00_4338;

									{	/* Llib/srfi4.scm 663 */
										obj_t BgL_tmpz00_4348;

										if (BGL_INT16P(BgL_initz00_1423))
											{	/* Llib/srfi4.scm 663 */
												BgL_tmpz00_4348 = BgL_initz00_1423;
											}
										else
											{
												obj_t BgL_auxz00_4351;

												BgL_auxz00_4351 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(27280L),
													BGl_string2394z00zz__srfi4z00,
													BGl_string2395z00zz__srfi4z00, BgL_initz00_1423);
												FAILURE(BgL_auxz00_4351, BFALSE, BFALSE);
											}
										BgL_auxz00_4347 = BGL_BINT16_TO_INT16(BgL_tmpz00_4348);
									}
									{	/* Llib/srfi4.scm 663 */
										obj_t BgL_tmpz00_4339;

										if (INTEGERP(BgL_g1240z00_1419))
											{	/* Llib/srfi4.scm 663 */
												BgL_tmpz00_4339 = BgL_g1240z00_1419;
											}
										else
											{
												obj_t BgL_auxz00_4342;

												BgL_auxz00_4342 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(27280L),
													BGl_string2394z00zz__srfi4z00,
													BGl_string2390z00zz__srfi4z00, BgL_g1240z00_1419);
												FAILURE(BgL_auxz00_4342, BFALSE, BFALSE);
											}
										BgL_auxz00_4338 = (long) CINT(BgL_tmpz00_4339);
									}
									return
										BGl_makezd2s16vectorzd2zz__srfi4z00(BgL_auxz00_4338,
										BgL_auxz00_4347);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* make-s16vector */
	BGL_EXPORTED_DEF obj_t BGl_makezd2s16vectorzd2zz__srfi4z00(long BgL_lenz00_43,
		int16_t BgL_initz00_44)
	{
		{	/* Llib/srfi4.scm 663 */
			{	/* Llib/srfi4.scm 663 */
				obj_t BgL_vz00_1424;

				{	/* Llib/srfi4.scm 663 */
					int32_t BgL_tmpz00_4359;

					BgL_tmpz00_4359 = (int32_t) (BgL_lenz00_43);
					BgL_vz00_1424 = BGL_ALLOC_S16VECTOR(BgL_tmpz00_4359);
				}
				{
					long BgL_iz00_2334;

					BgL_iz00_2334 = 0L;
				BgL_loopz00_2333:
					if ((BgL_iz00_2334 < BgL_lenz00_43))
						{	/* Llib/srfi4.scm 663 */
							BGL_S16VSET(BgL_vz00_1424, BgL_iz00_2334, BgL_initz00_44);
							BUNSPEC;
							{
								long BgL_iz00_4365;

								BgL_iz00_4365 = (BgL_iz00_2334 + 1L);
								BgL_iz00_2334 = BgL_iz00_4365;
								goto BgL_loopz00_2333;
							}
						}
					else
						{	/* Llib/srfi4.scm 663 */
							((bool_t) 0);
						}
				}
				return BgL_vz00_1424;
			}
		}

	}



/* _make-u16vector */
	obj_t BGl__makezd2u16vectorzd2zz__srfi4z00(obj_t BgL_env1244z00_50,
		obj_t BgL_opt1243z00_49)
	{
		{	/* Llib/srfi4.scm 664 */
			{	/* Llib/srfi4.scm 664 */
				obj_t BgL_g1245z00_1431;

				BgL_g1245z00_1431 = VECTOR_REF(BgL_opt1243z00_49, 0L);
				switch (VECTOR_LENGTH(BgL_opt1243z00_49))
					{
					case 1L:

						{	/* Llib/srfi4.scm 664 */

							{	/* Llib/srfi4.scm 664 */
								long BgL_auxz00_4368;

								{	/* Llib/srfi4.scm 664 */
									obj_t BgL_tmpz00_4369;

									if (INTEGERP(BgL_g1245z00_1431))
										{	/* Llib/srfi4.scm 664 */
											BgL_tmpz00_4369 = BgL_g1245z00_1431;
										}
									else
										{
											obj_t BgL_auxz00_4372;

											BgL_auxz00_4372 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(27314L),
												BGl_string2396z00zz__srfi4z00,
												BGl_string2390z00zz__srfi4z00, BgL_g1245z00_1431);
											FAILURE(BgL_auxz00_4372, BFALSE, BFALSE);
										}
									BgL_auxz00_4368 = (long) CINT(BgL_tmpz00_4369);
								}
								return
									BGl_makezd2u16vectorzd2zz__srfi4z00(BgL_auxz00_4368,
									(uint16_t) (0));
							}
						}
						break;
					case 2L:

						{	/* Llib/srfi4.scm 664 */
							obj_t BgL_initz00_1435;

							BgL_initz00_1435 = VECTOR_REF(BgL_opt1243z00_49, 1L);
							{	/* Llib/srfi4.scm 664 */

								{	/* Llib/srfi4.scm 664 */
									uint16_t BgL_auxz00_4388;
									long BgL_auxz00_4379;

									{	/* Llib/srfi4.scm 664 */
										obj_t BgL_tmpz00_4389;

										if (BGL_UINT16P(BgL_initz00_1435))
											{	/* Llib/srfi4.scm 664 */
												BgL_tmpz00_4389 = BgL_initz00_1435;
											}
										else
											{
												obj_t BgL_auxz00_4392;

												BgL_auxz00_4392 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(27314L),
													BGl_string2396z00zz__srfi4z00,
													BGl_string2397z00zz__srfi4z00, BgL_initz00_1435);
												FAILURE(BgL_auxz00_4392, BFALSE, BFALSE);
											}
										BgL_auxz00_4388 = BGL_BUINT16_TO_UINT16(BgL_tmpz00_4389);
									}
									{	/* Llib/srfi4.scm 664 */
										obj_t BgL_tmpz00_4380;

										if (INTEGERP(BgL_g1245z00_1431))
											{	/* Llib/srfi4.scm 664 */
												BgL_tmpz00_4380 = BgL_g1245z00_1431;
											}
										else
											{
												obj_t BgL_auxz00_4383;

												BgL_auxz00_4383 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(27314L),
													BGl_string2396z00zz__srfi4z00,
													BGl_string2390z00zz__srfi4z00, BgL_g1245z00_1431);
												FAILURE(BgL_auxz00_4383, BFALSE, BFALSE);
											}
										BgL_auxz00_4379 = (long) CINT(BgL_tmpz00_4380);
									}
									return
										BGl_makezd2u16vectorzd2zz__srfi4z00(BgL_auxz00_4379,
										BgL_auxz00_4388);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* make-u16vector */
	BGL_EXPORTED_DEF obj_t BGl_makezd2u16vectorzd2zz__srfi4z00(long BgL_lenz00_47,
		uint16_t BgL_initz00_48)
	{
		{	/* Llib/srfi4.scm 664 */
			{	/* Llib/srfi4.scm 664 */
				obj_t BgL_vz00_1436;

				{	/* Llib/srfi4.scm 664 */
					int32_t BgL_tmpz00_4400;

					BgL_tmpz00_4400 = (int32_t) (BgL_lenz00_47);
					BgL_vz00_1436 = BGL_ALLOC_U16VECTOR(BgL_tmpz00_4400);
				}
				{
					long BgL_iz00_2344;

					BgL_iz00_2344 = 0L;
				BgL_loopz00_2343:
					if ((BgL_iz00_2344 < BgL_lenz00_47))
						{	/* Llib/srfi4.scm 664 */
							BGL_U16VSET(BgL_vz00_1436, BgL_iz00_2344, BgL_initz00_48);
							BUNSPEC;
							{
								long BgL_iz00_4406;

								BgL_iz00_4406 = (BgL_iz00_2344 + 1L);
								BgL_iz00_2344 = BgL_iz00_4406;
								goto BgL_loopz00_2343;
							}
						}
					else
						{	/* Llib/srfi4.scm 664 */
							((bool_t) 0);
						}
				}
				return BgL_vz00_1436;
			}
		}

	}



/* _make-s32vector */
	obj_t BGl__makezd2s32vectorzd2zz__srfi4z00(obj_t BgL_env1249z00_54,
		obj_t BgL_opt1248z00_53)
	{
		{	/* Llib/srfi4.scm 665 */
			{	/* Llib/srfi4.scm 665 */
				obj_t BgL_g1250z00_1443;

				BgL_g1250z00_1443 = VECTOR_REF(BgL_opt1248z00_53, 0L);
				switch (VECTOR_LENGTH(BgL_opt1248z00_53))
					{
					case 1L:

						{	/* Llib/srfi4.scm 665 */

							{	/* Llib/srfi4.scm 665 */
								long BgL_auxz00_4409;

								{	/* Llib/srfi4.scm 665 */
									obj_t BgL_tmpz00_4410;

									if (INTEGERP(BgL_g1250z00_1443))
										{	/* Llib/srfi4.scm 665 */
											BgL_tmpz00_4410 = BgL_g1250z00_1443;
										}
									else
										{
											obj_t BgL_auxz00_4413;

											BgL_auxz00_4413 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(27348L),
												BGl_string2398z00zz__srfi4z00,
												BGl_string2390z00zz__srfi4z00, BgL_g1250z00_1443);
											FAILURE(BgL_auxz00_4413, BFALSE, BFALSE);
										}
									BgL_auxz00_4409 = (long) CINT(BgL_tmpz00_4410);
								}
								return
									BGl_makezd2s32vectorzd2zz__srfi4z00(BgL_auxz00_4409,
									(int32_t) (0));
							}
						}
						break;
					case 2L:

						{	/* Llib/srfi4.scm 665 */
							obj_t BgL_initz00_1447;

							BgL_initz00_1447 = VECTOR_REF(BgL_opt1248z00_53, 1L);
							{	/* Llib/srfi4.scm 665 */

								{	/* Llib/srfi4.scm 665 */
									int32_t BgL_auxz00_4429;
									long BgL_auxz00_4420;

									{	/* Llib/srfi4.scm 665 */
										obj_t BgL_tmpz00_4430;

										if (BGL_INT32P(BgL_initz00_1447))
											{	/* Llib/srfi4.scm 665 */
												BgL_tmpz00_4430 = BgL_initz00_1447;
											}
										else
											{
												obj_t BgL_auxz00_4433;

												BgL_auxz00_4433 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(27348L),
													BGl_string2398z00zz__srfi4z00,
													BGl_string2399z00zz__srfi4z00, BgL_initz00_1447);
												FAILURE(BgL_auxz00_4433, BFALSE, BFALSE);
											}
										BgL_auxz00_4429 = BGL_BINT32_TO_INT32(BgL_tmpz00_4430);
									}
									{	/* Llib/srfi4.scm 665 */
										obj_t BgL_tmpz00_4421;

										if (INTEGERP(BgL_g1250z00_1443))
											{	/* Llib/srfi4.scm 665 */
												BgL_tmpz00_4421 = BgL_g1250z00_1443;
											}
										else
											{
												obj_t BgL_auxz00_4424;

												BgL_auxz00_4424 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(27348L),
													BGl_string2398z00zz__srfi4z00,
													BGl_string2390z00zz__srfi4z00, BgL_g1250z00_1443);
												FAILURE(BgL_auxz00_4424, BFALSE, BFALSE);
											}
										BgL_auxz00_4420 = (long) CINT(BgL_tmpz00_4421);
									}
									return
										BGl_makezd2s32vectorzd2zz__srfi4z00(BgL_auxz00_4420,
										BgL_auxz00_4429);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* make-s32vector */
	BGL_EXPORTED_DEF obj_t BGl_makezd2s32vectorzd2zz__srfi4z00(long BgL_lenz00_51,
		int32_t BgL_initz00_52)
	{
		{	/* Llib/srfi4.scm 665 */
			{	/* Llib/srfi4.scm 665 */
				obj_t BgL_vz00_1448;

				{	/* Llib/srfi4.scm 665 */
					int32_t BgL_tmpz00_4441;

					BgL_tmpz00_4441 = (int32_t) (BgL_lenz00_51);
					BgL_vz00_1448 = BGL_ALLOC_S32VECTOR(BgL_tmpz00_4441);
				}
				{
					long BgL_iz00_2354;

					BgL_iz00_2354 = 0L;
				BgL_loopz00_2353:
					if ((BgL_iz00_2354 < BgL_lenz00_51))
						{	/* Llib/srfi4.scm 665 */
							BGL_S32VSET(BgL_vz00_1448, BgL_iz00_2354, BgL_initz00_52);
							BUNSPEC;
							{
								long BgL_iz00_4447;

								BgL_iz00_4447 = (BgL_iz00_2354 + 1L);
								BgL_iz00_2354 = BgL_iz00_4447;
								goto BgL_loopz00_2353;
							}
						}
					else
						{	/* Llib/srfi4.scm 665 */
							((bool_t) 0);
						}
				}
				return BgL_vz00_1448;
			}
		}

	}



/* _make-u32vector */
	obj_t BGl__makezd2u32vectorzd2zz__srfi4z00(obj_t BgL_env1254z00_58,
		obj_t BgL_opt1253z00_57)
	{
		{	/* Llib/srfi4.scm 666 */
			{	/* Llib/srfi4.scm 666 */
				obj_t BgL_g1255z00_1455;

				BgL_g1255z00_1455 = VECTOR_REF(BgL_opt1253z00_57, 0L);
				switch (VECTOR_LENGTH(BgL_opt1253z00_57))
					{
					case 1L:

						{	/* Llib/srfi4.scm 666 */

							{	/* Llib/srfi4.scm 666 */
								long BgL_auxz00_4450;

								{	/* Llib/srfi4.scm 666 */
									obj_t BgL_tmpz00_4451;

									if (INTEGERP(BgL_g1255z00_1455))
										{	/* Llib/srfi4.scm 666 */
											BgL_tmpz00_4451 = BgL_g1255z00_1455;
										}
									else
										{
											obj_t BgL_auxz00_4454;

											BgL_auxz00_4454 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(27382L),
												BGl_string2400z00zz__srfi4z00,
												BGl_string2390z00zz__srfi4z00, BgL_g1255z00_1455);
											FAILURE(BgL_auxz00_4454, BFALSE, BFALSE);
										}
									BgL_auxz00_4450 = (long) CINT(BgL_tmpz00_4451);
								}
								return
									BGl_makezd2u32vectorzd2zz__srfi4z00(BgL_auxz00_4450,
									(uint32_t) (0));
							}
						}
						break;
					case 2L:

						{	/* Llib/srfi4.scm 666 */
							obj_t BgL_initz00_1459;

							BgL_initz00_1459 = VECTOR_REF(BgL_opt1253z00_57, 1L);
							{	/* Llib/srfi4.scm 666 */

								{	/* Llib/srfi4.scm 666 */
									uint32_t BgL_auxz00_4470;
									long BgL_auxz00_4461;

									{	/* Llib/srfi4.scm 666 */
										obj_t BgL_tmpz00_4471;

										if (BGL_UINT32P(BgL_initz00_1459))
											{	/* Llib/srfi4.scm 666 */
												BgL_tmpz00_4471 = BgL_initz00_1459;
											}
										else
											{
												obj_t BgL_auxz00_4474;

												BgL_auxz00_4474 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(27382L),
													BGl_string2400z00zz__srfi4z00,
													BGl_string2401z00zz__srfi4z00, BgL_initz00_1459);
												FAILURE(BgL_auxz00_4474, BFALSE, BFALSE);
											}
										BgL_auxz00_4470 = BGL_BUINT32_TO_UINT32(BgL_tmpz00_4471);
									}
									{	/* Llib/srfi4.scm 666 */
										obj_t BgL_tmpz00_4462;

										if (INTEGERP(BgL_g1255z00_1455))
											{	/* Llib/srfi4.scm 666 */
												BgL_tmpz00_4462 = BgL_g1255z00_1455;
											}
										else
											{
												obj_t BgL_auxz00_4465;

												BgL_auxz00_4465 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(27382L),
													BGl_string2400z00zz__srfi4z00,
													BGl_string2390z00zz__srfi4z00, BgL_g1255z00_1455);
												FAILURE(BgL_auxz00_4465, BFALSE, BFALSE);
											}
										BgL_auxz00_4461 = (long) CINT(BgL_tmpz00_4462);
									}
									return
										BGl_makezd2u32vectorzd2zz__srfi4z00(BgL_auxz00_4461,
										BgL_auxz00_4470);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* make-u32vector */
	BGL_EXPORTED_DEF obj_t BGl_makezd2u32vectorzd2zz__srfi4z00(long BgL_lenz00_55,
		uint32_t BgL_initz00_56)
	{
		{	/* Llib/srfi4.scm 666 */
			{	/* Llib/srfi4.scm 666 */
				obj_t BgL_vz00_1460;

				{	/* Llib/srfi4.scm 666 */
					int32_t BgL_tmpz00_4482;

					BgL_tmpz00_4482 = (int32_t) (BgL_lenz00_55);
					BgL_vz00_1460 = BGL_ALLOC_U32VECTOR(BgL_tmpz00_4482);
				}
				{
					long BgL_iz00_2364;

					BgL_iz00_2364 = 0L;
				BgL_loopz00_2363:
					if ((BgL_iz00_2364 < BgL_lenz00_55))
						{	/* Llib/srfi4.scm 666 */
							BGL_U32VSET(BgL_vz00_1460, BgL_iz00_2364, BgL_initz00_56);
							BUNSPEC;
							{
								long BgL_iz00_4488;

								BgL_iz00_4488 = (BgL_iz00_2364 + 1L);
								BgL_iz00_2364 = BgL_iz00_4488;
								goto BgL_loopz00_2363;
							}
						}
					else
						{	/* Llib/srfi4.scm 666 */
							((bool_t) 0);
						}
				}
				return BgL_vz00_1460;
			}
		}

	}



/* _make-s64vector */
	obj_t BGl__makezd2s64vectorzd2zz__srfi4z00(obj_t BgL_env1259z00_62,
		obj_t BgL_opt1258z00_61)
	{
		{	/* Llib/srfi4.scm 667 */
			{	/* Llib/srfi4.scm 667 */
				obj_t BgL_g1260z00_1467;

				BgL_g1260z00_1467 = VECTOR_REF(BgL_opt1258z00_61, 0L);
				switch (VECTOR_LENGTH(BgL_opt1258z00_61))
					{
					case 1L:

						{	/* Llib/srfi4.scm 667 */

							{	/* Llib/srfi4.scm 667 */
								long BgL_auxz00_4491;

								{	/* Llib/srfi4.scm 667 */
									obj_t BgL_tmpz00_4492;

									if (INTEGERP(BgL_g1260z00_1467))
										{	/* Llib/srfi4.scm 667 */
											BgL_tmpz00_4492 = BgL_g1260z00_1467;
										}
									else
										{
											obj_t BgL_auxz00_4495;

											BgL_auxz00_4495 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(27416L),
												BGl_string2402z00zz__srfi4z00,
												BGl_string2390z00zz__srfi4z00, BgL_g1260z00_1467);
											FAILURE(BgL_auxz00_4495, BFALSE, BFALSE);
										}
									BgL_auxz00_4491 = (long) CINT(BgL_tmpz00_4492);
								}
								return
									BGl_makezd2s64vectorzd2zz__srfi4z00(BgL_auxz00_4491,
									(int64_t) (0));
							}
						}
						break;
					case 2L:

						{	/* Llib/srfi4.scm 667 */
							obj_t BgL_initz00_1471;

							BgL_initz00_1471 = VECTOR_REF(BgL_opt1258z00_61, 1L);
							{	/* Llib/srfi4.scm 667 */

								{	/* Llib/srfi4.scm 667 */
									int64_t BgL_auxz00_4511;
									long BgL_auxz00_4502;

									{	/* Llib/srfi4.scm 667 */
										obj_t BgL_tmpz00_4512;

										if (BGL_INT64P(BgL_initz00_1471))
											{	/* Llib/srfi4.scm 667 */
												BgL_tmpz00_4512 = BgL_initz00_1471;
											}
										else
											{
												obj_t BgL_auxz00_4515;

												BgL_auxz00_4515 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(27416L),
													BGl_string2402z00zz__srfi4z00,
													BGl_string2403z00zz__srfi4z00, BgL_initz00_1471);
												FAILURE(BgL_auxz00_4515, BFALSE, BFALSE);
											}
										BgL_auxz00_4511 = BGL_BINT64_TO_INT64(BgL_tmpz00_4512);
									}
									{	/* Llib/srfi4.scm 667 */
										obj_t BgL_tmpz00_4503;

										if (INTEGERP(BgL_g1260z00_1467))
											{	/* Llib/srfi4.scm 667 */
												BgL_tmpz00_4503 = BgL_g1260z00_1467;
											}
										else
											{
												obj_t BgL_auxz00_4506;

												BgL_auxz00_4506 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(27416L),
													BGl_string2402z00zz__srfi4z00,
													BGl_string2390z00zz__srfi4z00, BgL_g1260z00_1467);
												FAILURE(BgL_auxz00_4506, BFALSE, BFALSE);
											}
										BgL_auxz00_4502 = (long) CINT(BgL_tmpz00_4503);
									}
									return
										BGl_makezd2s64vectorzd2zz__srfi4z00(BgL_auxz00_4502,
										BgL_auxz00_4511);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* make-s64vector */
	BGL_EXPORTED_DEF obj_t BGl_makezd2s64vectorzd2zz__srfi4z00(long BgL_lenz00_59,
		int64_t BgL_initz00_60)
	{
		{	/* Llib/srfi4.scm 667 */
			{	/* Llib/srfi4.scm 667 */
				obj_t BgL_vz00_1472;

				{	/* Llib/srfi4.scm 667 */
					int32_t BgL_tmpz00_4523;

					BgL_tmpz00_4523 = (int32_t) (BgL_lenz00_59);
					BgL_vz00_1472 = BGL_ALLOC_S64VECTOR(BgL_tmpz00_4523);
				}
				{
					long BgL_iz00_2374;

					BgL_iz00_2374 = 0L;
				BgL_loopz00_2373:
					if ((BgL_iz00_2374 < BgL_lenz00_59))
						{	/* Llib/srfi4.scm 667 */
							BGL_S64VSET(BgL_vz00_1472, BgL_iz00_2374, BgL_initz00_60);
							BUNSPEC;
							{
								long BgL_iz00_4529;

								BgL_iz00_4529 = (BgL_iz00_2374 + 1L);
								BgL_iz00_2374 = BgL_iz00_4529;
								goto BgL_loopz00_2373;
							}
						}
					else
						{	/* Llib/srfi4.scm 667 */
							((bool_t) 0);
						}
				}
				return BgL_vz00_1472;
			}
		}

	}



/* _make-u64vector */
	obj_t BGl__makezd2u64vectorzd2zz__srfi4z00(obj_t BgL_env1264z00_66,
		obj_t BgL_opt1263z00_65)
	{
		{	/* Llib/srfi4.scm 668 */
			{	/* Llib/srfi4.scm 668 */
				obj_t BgL_g1265z00_1479;

				BgL_g1265z00_1479 = VECTOR_REF(BgL_opt1263z00_65, 0L);
				switch (VECTOR_LENGTH(BgL_opt1263z00_65))
					{
					case 1L:

						{	/* Llib/srfi4.scm 668 */

							{	/* Llib/srfi4.scm 668 */
								long BgL_auxz00_4532;

								{	/* Llib/srfi4.scm 668 */
									obj_t BgL_tmpz00_4533;

									if (INTEGERP(BgL_g1265z00_1479))
										{	/* Llib/srfi4.scm 668 */
											BgL_tmpz00_4533 = BgL_g1265z00_1479;
										}
									else
										{
											obj_t BgL_auxz00_4536;

											BgL_auxz00_4536 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(27450L),
												BGl_string2404z00zz__srfi4z00,
												BGl_string2390z00zz__srfi4z00, BgL_g1265z00_1479);
											FAILURE(BgL_auxz00_4536, BFALSE, BFALSE);
										}
									BgL_auxz00_4532 = (long) CINT(BgL_tmpz00_4533);
								}
								return
									BGl_makezd2u64vectorzd2zz__srfi4z00(BgL_auxz00_4532,
									(uint64_t) (0));
							}
						}
						break;
					case 2L:

						{	/* Llib/srfi4.scm 668 */
							obj_t BgL_initz00_1483;

							BgL_initz00_1483 = VECTOR_REF(BgL_opt1263z00_65, 1L);
							{	/* Llib/srfi4.scm 668 */

								{	/* Llib/srfi4.scm 668 */
									uint64_t BgL_auxz00_4552;
									long BgL_auxz00_4543;

									{	/* Llib/srfi4.scm 668 */
										obj_t BgL_tmpz00_4553;

										if (BGL_UINT64P(BgL_initz00_1483))
											{	/* Llib/srfi4.scm 668 */
												BgL_tmpz00_4553 = BgL_initz00_1483;
											}
										else
											{
												obj_t BgL_auxz00_4556;

												BgL_auxz00_4556 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(27450L),
													BGl_string2404z00zz__srfi4z00,
													BGl_string2405z00zz__srfi4z00, BgL_initz00_1483);
												FAILURE(BgL_auxz00_4556, BFALSE, BFALSE);
											}
										BgL_auxz00_4552 = BGL_BINT64_TO_INT64(BgL_tmpz00_4553);
									}
									{	/* Llib/srfi4.scm 668 */
										obj_t BgL_tmpz00_4544;

										if (INTEGERP(BgL_g1265z00_1479))
											{	/* Llib/srfi4.scm 668 */
												BgL_tmpz00_4544 = BgL_g1265z00_1479;
											}
										else
											{
												obj_t BgL_auxz00_4547;

												BgL_auxz00_4547 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(27450L),
													BGl_string2404z00zz__srfi4z00,
													BGl_string2390z00zz__srfi4z00, BgL_g1265z00_1479);
												FAILURE(BgL_auxz00_4547, BFALSE, BFALSE);
											}
										BgL_auxz00_4543 = (long) CINT(BgL_tmpz00_4544);
									}
									return
										BGl_makezd2u64vectorzd2zz__srfi4z00(BgL_auxz00_4543,
										BgL_auxz00_4552);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* make-u64vector */
	BGL_EXPORTED_DEF obj_t BGl_makezd2u64vectorzd2zz__srfi4z00(long BgL_lenz00_63,
		uint64_t BgL_initz00_64)
	{
		{	/* Llib/srfi4.scm 668 */
			{	/* Llib/srfi4.scm 668 */
				obj_t BgL_vz00_1484;

				{	/* Llib/srfi4.scm 668 */
					int32_t BgL_tmpz00_4564;

					BgL_tmpz00_4564 = (int32_t) (BgL_lenz00_63);
					BgL_vz00_1484 = BGL_ALLOC_U64VECTOR(BgL_tmpz00_4564);
				}
				{
					long BgL_iz00_2384;

					BgL_iz00_2384 = 0L;
				BgL_loopz00_2383:
					if ((BgL_iz00_2384 < BgL_lenz00_63))
						{	/* Llib/srfi4.scm 668 */
							BGL_U64VSET(BgL_vz00_1484, BgL_iz00_2384, BgL_initz00_64);
							BUNSPEC;
							{
								long BgL_iz00_4570;

								BgL_iz00_4570 = (BgL_iz00_2384 + 1L);
								BgL_iz00_2384 = BgL_iz00_4570;
								goto BgL_loopz00_2383;
							}
						}
					else
						{	/* Llib/srfi4.scm 668 */
							((bool_t) 0);
						}
				}
				return BgL_vz00_1484;
			}
		}

	}



/* _make-f32vector */
	obj_t BGl__makezd2f32vectorzd2zz__srfi4z00(obj_t BgL_env1269z00_70,
		obj_t BgL_opt1268z00_69)
	{
		{	/* Llib/srfi4.scm 669 */
			{	/* Llib/srfi4.scm 669 */
				obj_t BgL_g1270z00_1491;

				BgL_g1270z00_1491 = VECTOR_REF(BgL_opt1268z00_69, 0L);
				switch (VECTOR_LENGTH(BgL_opt1268z00_69))
					{
					case 1L:

						{	/* Llib/srfi4.scm 669 */

							{	/* Llib/srfi4.scm 669 */
								long BgL_auxz00_4573;

								{	/* Llib/srfi4.scm 669 */
									obj_t BgL_tmpz00_4574;

									if (INTEGERP(BgL_g1270z00_1491))
										{	/* Llib/srfi4.scm 669 */
											BgL_tmpz00_4574 = BgL_g1270z00_1491;
										}
									else
										{
											obj_t BgL_auxz00_4577;

											BgL_auxz00_4577 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(27484L),
												BGl_string2406z00zz__srfi4z00,
												BGl_string2390z00zz__srfi4z00, BgL_g1270z00_1491);
											FAILURE(BgL_auxz00_4577, BFALSE, BFALSE);
										}
									BgL_auxz00_4573 = (long) CINT(BgL_tmpz00_4574);
								}
								return
									BGl_makezd2f32vectorzd2zz__srfi4z00(BgL_auxz00_4573,
									(float) (((double) 0.0)));
						}} break;
					case 2L:

						{	/* Llib/srfi4.scm 669 */
							obj_t BgL_initz00_1495;

							BgL_initz00_1495 = VECTOR_REF(BgL_opt1268z00_69, 1L);
							{	/* Llib/srfi4.scm 669 */

								{	/* Llib/srfi4.scm 669 */
									float BgL_auxz00_4594;
									long BgL_auxz00_4585;

									{	/* Llib/srfi4.scm 669 */
										obj_t BgL_tmpz00_4595;

										if (REALP(BgL_initz00_1495))
											{	/* Llib/srfi4.scm 669 */
												BgL_tmpz00_4595 = BgL_initz00_1495;
											}
										else
											{
												obj_t BgL_auxz00_4598;

												BgL_auxz00_4598 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(27484L),
													BGl_string2406z00zz__srfi4z00,
													BGl_string2407z00zz__srfi4z00, BgL_initz00_1495);
												FAILURE(BgL_auxz00_4598, BFALSE, BFALSE);
											}
										BgL_auxz00_4594 = REAL_TO_FLOAT(BgL_tmpz00_4595);
									}
									{	/* Llib/srfi4.scm 669 */
										obj_t BgL_tmpz00_4586;

										if (INTEGERP(BgL_g1270z00_1491))
											{	/* Llib/srfi4.scm 669 */
												BgL_tmpz00_4586 = BgL_g1270z00_1491;
											}
										else
											{
												obj_t BgL_auxz00_4589;

												BgL_auxz00_4589 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(27484L),
													BGl_string2406z00zz__srfi4z00,
													BGl_string2390z00zz__srfi4z00, BgL_g1270z00_1491);
												FAILURE(BgL_auxz00_4589, BFALSE, BFALSE);
											}
										BgL_auxz00_4585 = (long) CINT(BgL_tmpz00_4586);
									}
									return
										BGl_makezd2f32vectorzd2zz__srfi4z00(BgL_auxz00_4585,
										BgL_auxz00_4594);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* make-f32vector */
	BGL_EXPORTED_DEF obj_t BGl_makezd2f32vectorzd2zz__srfi4z00(long BgL_lenz00_67,
		float BgL_initz00_68)
	{
		{	/* Llib/srfi4.scm 669 */
			{	/* Llib/srfi4.scm 669 */
				obj_t BgL_vz00_1496;

				{	/* Llib/srfi4.scm 669 */
					int32_t BgL_tmpz00_4606;

					BgL_tmpz00_4606 = (int32_t) (BgL_lenz00_67);
					BgL_vz00_1496 = BGL_ALLOC_F32VECTOR(BgL_tmpz00_4606);
				}
				{
					long BgL_iz00_2394;

					BgL_iz00_2394 = 0L;
				BgL_loopz00_2393:
					if ((BgL_iz00_2394 < BgL_lenz00_67))
						{	/* Llib/srfi4.scm 669 */
							BGL_F32VSET(BgL_vz00_1496, BgL_iz00_2394, BgL_initz00_68);
							BUNSPEC;
							{
								long BgL_iz00_4612;

								BgL_iz00_4612 = (BgL_iz00_2394 + 1L);
								BgL_iz00_2394 = BgL_iz00_4612;
								goto BgL_loopz00_2393;
							}
						}
					else
						{	/* Llib/srfi4.scm 669 */
							((bool_t) 0);
						}
				}
				return BgL_vz00_1496;
			}
		}

	}



/* _make-f64vector */
	obj_t BGl__makezd2f64vectorzd2zz__srfi4z00(obj_t BgL_env1274z00_74,
		obj_t BgL_opt1273z00_73)
	{
		{	/* Llib/srfi4.scm 670 */
			{	/* Llib/srfi4.scm 670 */
				obj_t BgL_g1275z00_1503;

				BgL_g1275z00_1503 = VECTOR_REF(BgL_opt1273z00_73, 0L);
				switch (VECTOR_LENGTH(BgL_opt1273z00_73))
					{
					case 1L:

						{	/* Llib/srfi4.scm 670 */

							{	/* Llib/srfi4.scm 670 */
								long BgL_auxz00_4615;

								{	/* Llib/srfi4.scm 670 */
									obj_t BgL_tmpz00_4616;

									if (INTEGERP(BgL_g1275z00_1503))
										{	/* Llib/srfi4.scm 670 */
											BgL_tmpz00_4616 = BgL_g1275z00_1503;
										}
									else
										{
											obj_t BgL_auxz00_4619;

											BgL_auxz00_4619 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(27515L),
												BGl_string2408z00zz__srfi4z00,
												BGl_string2390z00zz__srfi4z00, BgL_g1275z00_1503);
											FAILURE(BgL_auxz00_4619, BFALSE, BFALSE);
										}
									BgL_auxz00_4615 = (long) CINT(BgL_tmpz00_4616);
								}
								return
									BGl_makezd2f64vectorzd2zz__srfi4z00(BgL_auxz00_4615,
									((double) 0.0));
						}} break;
					case 2L:

						{	/* Llib/srfi4.scm 670 */
							obj_t BgL_initz00_1507;

							BgL_initz00_1507 = VECTOR_REF(BgL_opt1273z00_73, 1L);
							{	/* Llib/srfi4.scm 670 */

								{	/* Llib/srfi4.scm 670 */
									double BgL_auxz00_4635;
									long BgL_auxz00_4626;

									{	/* Llib/srfi4.scm 670 */
										obj_t BgL_tmpz00_4636;

										if (REALP(BgL_initz00_1507))
											{	/* Llib/srfi4.scm 670 */
												BgL_tmpz00_4636 = BgL_initz00_1507;
											}
										else
											{
												obj_t BgL_auxz00_4639;

												BgL_auxz00_4639 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(27515L),
													BGl_string2408z00zz__srfi4z00,
													BGl_string2407z00zz__srfi4z00, BgL_initz00_1507);
												FAILURE(BgL_auxz00_4639, BFALSE, BFALSE);
											}
										BgL_auxz00_4635 = REAL_TO_DOUBLE(BgL_tmpz00_4636);
									}
									{	/* Llib/srfi4.scm 670 */
										obj_t BgL_tmpz00_4627;

										if (INTEGERP(BgL_g1275z00_1503))
											{	/* Llib/srfi4.scm 670 */
												BgL_tmpz00_4627 = BgL_g1275z00_1503;
											}
										else
											{
												obj_t BgL_auxz00_4630;

												BgL_auxz00_4630 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(27515L),
													BGl_string2408z00zz__srfi4z00,
													BGl_string2390z00zz__srfi4z00, BgL_g1275z00_1503);
												FAILURE(BgL_auxz00_4630, BFALSE, BFALSE);
											}
										BgL_auxz00_4626 = (long) CINT(BgL_tmpz00_4627);
									}
									return
										BGl_makezd2f64vectorzd2zz__srfi4z00(BgL_auxz00_4626,
										BgL_auxz00_4635);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* make-f64vector */
	BGL_EXPORTED_DEF obj_t BGl_makezd2f64vectorzd2zz__srfi4z00(long BgL_lenz00_71,
		double BgL_initz00_72)
	{
		{	/* Llib/srfi4.scm 670 */
			{	/* Llib/srfi4.scm 670 */
				obj_t BgL_vz00_1508;

				{	/* Llib/srfi4.scm 670 */
					int32_t BgL_tmpz00_4647;

					BgL_tmpz00_4647 = (int32_t) (BgL_lenz00_71);
					BgL_vz00_1508 = BGL_ALLOC_F64VECTOR(BgL_tmpz00_4647);
				}
				{
					long BgL_iz00_2404;

					BgL_iz00_2404 = 0L;
				BgL_loopz00_2403:
					if ((BgL_iz00_2404 < BgL_lenz00_71))
						{	/* Llib/srfi4.scm 670 */
							BGL_F64VSET(BgL_vz00_1508, BgL_iz00_2404, BgL_initz00_72);
							BUNSPEC;
							{
								long BgL_iz00_4653;

								BgL_iz00_4653 = (BgL_iz00_2404 + 1L);
								BgL_iz00_2404 = BgL_iz00_4653;
								goto BgL_loopz00_2403;
							}
						}
					else
						{	/* Llib/srfi4.scm 670 */
							((bool_t) 0);
						}
				}
				return BgL_vz00_1508;
			}
		}

	}



/* hvector-range-error */
	BGL_EXPORTED_DEF obj_t BGl_hvectorzd2rangezd2errorz00zz__srfi4z00(obj_t
		BgL_funz00_75, obj_t BgL_vz00_76, long BgL_kz00_77)
	{
		{	/* Llib/srfi4.scm 675 */
			{	/* Llib/srfi4.scm 678 */
				obj_t BgL_arg1421z00_2410;

				{	/* Llib/srfi4.scm 678 */
					obj_t BgL_arg1422z00_2411;

					{	/* Llib/srfi4.scm 678 */
						long BgL_arg1423z00_2412;

						{	/* Llib/srfi4.scm 678 */
							long BgL_arg1425z00_2413;

							BgL_arg1425z00_2413 = BGL_HVECTOR_LENGTH(BgL_vz00_76);
							BgL_arg1423z00_2412 = (BgL_arg1425z00_2413 - 1L);
						}
						{	/* Llib/srfi4.scm 678 */

							BgL_arg1422z00_2411 =
								BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
								(BgL_arg1423z00_2412, 10L);
					}}
					BgL_arg1421z00_2410 =
						string_append_3(BGl_string2409z00zz__srfi4z00, BgL_arg1422z00_2411,
						BGl_string2410z00zz__srfi4z00);
				}
				return
					BGl_errorz00zz__errorz00(BgL_funz00_75, BgL_arg1421z00_2410,
					BINT(BgL_kz00_77));
			}
		}

	}



/* &hvector-range-error */
	obj_t BGl_z62hvectorzd2rangezd2errorz62zz__srfi4z00(obj_t BgL_envz00_3374,
		obj_t BgL_funz00_3375, obj_t BgL_vz00_3376, obj_t BgL_kz00_3377)
	{
		{	/* Llib/srfi4.scm 675 */
			{	/* Llib/srfi4.scm 678 */
				long BgL_auxz00_4668;
				obj_t BgL_auxz00_4661;

				{	/* Llib/srfi4.scm 678 */
					obj_t BgL_tmpz00_4669;

					if (INTEGERP(BgL_kz00_3377))
						{	/* Llib/srfi4.scm 678 */
							BgL_tmpz00_4669 = BgL_kz00_3377;
						}
					else
						{
							obj_t BgL_auxz00_4672;

							BgL_auxz00_4672 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(27892L), BGl_string2411z00zz__srfi4z00,
								BGl_string2390z00zz__srfi4z00, BgL_kz00_3377);
							FAILURE(BgL_auxz00_4672, BFALSE, BFALSE);
						}
					BgL_auxz00_4668 = (long) CINT(BgL_tmpz00_4669);
				}
				if (STRINGP(BgL_funz00_3375))
					{	/* Llib/srfi4.scm 678 */
						BgL_auxz00_4661 = BgL_funz00_3375;
					}
				else
					{
						obj_t BgL_auxz00_4664;

						BgL_auxz00_4664 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
							BINT(27892L), BGl_string2411z00zz__srfi4z00,
							BGl_string2412z00zz__srfi4z00, BgL_funz00_3375);
						FAILURE(BgL_auxz00_4664, BFALSE, BFALSE);
					}
				return
					BGl_hvectorzd2rangezd2errorz00zz__srfi4z00(BgL_auxz00_4661,
					BgL_vz00_3376, BgL_auxz00_4668);
			}
		}

	}



/* s8vector-ref */
	BGL_EXPORTED_DEF int8_t BGl_s8vectorzd2refzd2zz__srfi4z00(obj_t BgL_vz00_78,
		long BgL_kz00_79)
	{
		{	/* Llib/srfi4.scm 685 */
			return BGL_S8VREF(BgL_vz00_78, BgL_kz00_79);
		}

	}



/* &s8vector-ref */
	obj_t BGl_z62s8vectorzd2refzb0zz__srfi4z00(obj_t BgL_envz00_3237,
		obj_t BgL_vz00_3238, obj_t BgL_kz00_3239)
	{
		{	/* Llib/srfi4.scm 685 */
			{	/* Llib/srfi4.scm 686 */
				int8_t BgL_tmpz00_4679;

				{	/* Llib/srfi4.scm 686 */
					obj_t BgL_vz00_3831;
					long BgL_kz00_3832;

					if (BGL_S8VECTORP(BgL_vz00_3238))
						{	/* Llib/srfi4.scm 686 */
							BgL_vz00_3831 = BgL_vz00_3238;
						}
					else
						{
							obj_t BgL_auxz00_4682;

							BgL_auxz00_4682 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(28196L), BGl_string2413z00zz__srfi4z00,
								BGl_string2370z00zz__srfi4z00, BgL_vz00_3238);
							FAILURE(BgL_auxz00_4682, BFALSE, BFALSE);
						}
					{	/* Llib/srfi4.scm 686 */
						obj_t BgL_tmpz00_4686;

						if (INTEGERP(BgL_kz00_3239))
							{	/* Llib/srfi4.scm 686 */
								BgL_tmpz00_4686 = BgL_kz00_3239;
							}
						else
							{
								obj_t BgL_auxz00_4689;

								BgL_auxz00_4689 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
									BINT(28196L), BGl_string2413z00zz__srfi4z00,
									BGl_string2390z00zz__srfi4z00, BgL_kz00_3239);
								FAILURE(BgL_auxz00_4689, BFALSE, BFALSE);
							}
						BgL_kz00_3832 = (long) CINT(BgL_tmpz00_4686);
					}
					BgL_tmpz00_4679 = BGL_S8VREF(BgL_vz00_3831, BgL_kz00_3832);
				}
				return BGL_INT8_TO_BINT8(BgL_tmpz00_4679);
			}
		}

	}



/* u8vector-ref */
	BGL_EXPORTED_DEF uint8_t BGl_u8vectorzd2refzd2zz__srfi4z00(obj_t BgL_vz00_80,
		long BgL_kz00_81)
	{
		{	/* Llib/srfi4.scm 688 */
			return BGL_U8VREF(BgL_vz00_80, BgL_kz00_81);
		}

	}



/* &u8vector-ref */
	obj_t BGl_z62u8vectorzd2refzb0zz__srfi4z00(obj_t BgL_envz00_3247,
		obj_t BgL_vz00_3248, obj_t BgL_kz00_3249)
	{
		{	/* Llib/srfi4.scm 688 */
			{	/* Llib/srfi4.scm 689 */
				uint8_t BgL_tmpz00_4697;

				{	/* Llib/srfi4.scm 689 */
					obj_t BgL_vz00_3833;
					long BgL_kz00_3834;

					if (BGL_U8VECTORP(BgL_vz00_3248))
						{	/* Llib/srfi4.scm 689 */
							BgL_vz00_3833 = BgL_vz00_3248;
						}
					else
						{
							obj_t BgL_auxz00_4700;

							BgL_auxz00_4700 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(28255L), BGl_string2414z00zz__srfi4z00,
								BGl_string2372z00zz__srfi4z00, BgL_vz00_3248);
							FAILURE(BgL_auxz00_4700, BFALSE, BFALSE);
						}
					{	/* Llib/srfi4.scm 689 */
						obj_t BgL_tmpz00_4704;

						if (INTEGERP(BgL_kz00_3249))
							{	/* Llib/srfi4.scm 689 */
								BgL_tmpz00_4704 = BgL_kz00_3249;
							}
						else
							{
								obj_t BgL_auxz00_4707;

								BgL_auxz00_4707 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
									BINT(28255L), BGl_string2414z00zz__srfi4z00,
									BGl_string2390z00zz__srfi4z00, BgL_kz00_3249);
								FAILURE(BgL_auxz00_4707, BFALSE, BFALSE);
							}
						BgL_kz00_3834 = (long) CINT(BgL_tmpz00_4704);
					}
					BgL_tmpz00_4697 = BGL_U8VREF(BgL_vz00_3833, BgL_kz00_3834);
				}
				return BGL_UINT8_TO_BUINT8(BgL_tmpz00_4697);
			}
		}

	}



/* s16vector-ref */
	BGL_EXPORTED_DEF int16_t BGl_s16vectorzd2refzd2zz__srfi4z00(obj_t BgL_vz00_82,
		long BgL_kz00_83)
	{
		{	/* Llib/srfi4.scm 691 */
			return BGL_S16VREF(BgL_vz00_82, BgL_kz00_83);
		}

	}



/* &s16vector-ref */
	obj_t BGl_z62s16vectorzd2refzb0zz__srfi4z00(obj_t BgL_envz00_3257,
		obj_t BgL_vz00_3258, obj_t BgL_kz00_3259)
	{
		{	/* Llib/srfi4.scm 691 */
			{	/* Llib/srfi4.scm 692 */
				int16_t BgL_tmpz00_4715;

				{	/* Llib/srfi4.scm 692 */
					obj_t BgL_vz00_3835;
					long BgL_kz00_3836;

					if (BGL_S16VECTORP(BgL_vz00_3258))
						{	/* Llib/srfi4.scm 692 */
							BgL_vz00_3835 = BgL_vz00_3258;
						}
					else
						{
							obj_t BgL_auxz00_4718;

							BgL_auxz00_4718 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(28315L), BGl_string2415z00zz__srfi4z00,
								BGl_string2374z00zz__srfi4z00, BgL_vz00_3258);
							FAILURE(BgL_auxz00_4718, BFALSE, BFALSE);
						}
					{	/* Llib/srfi4.scm 692 */
						obj_t BgL_tmpz00_4722;

						if (INTEGERP(BgL_kz00_3259))
							{	/* Llib/srfi4.scm 692 */
								BgL_tmpz00_4722 = BgL_kz00_3259;
							}
						else
							{
								obj_t BgL_auxz00_4725;

								BgL_auxz00_4725 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
									BINT(28315L), BGl_string2415z00zz__srfi4z00,
									BGl_string2390z00zz__srfi4z00, BgL_kz00_3259);
								FAILURE(BgL_auxz00_4725, BFALSE, BFALSE);
							}
						BgL_kz00_3836 = (long) CINT(BgL_tmpz00_4722);
					}
					BgL_tmpz00_4715 = BGL_S16VREF(BgL_vz00_3835, BgL_kz00_3836);
				}
				return BGL_INT16_TO_BINT16(BgL_tmpz00_4715);
			}
		}

	}



/* u16vector-ref */
	BGL_EXPORTED_DEF uint16_t BGl_u16vectorzd2refzd2zz__srfi4z00(obj_t
		BgL_vz00_84, long BgL_kz00_85)
	{
		{	/* Llib/srfi4.scm 694 */
			return BGL_U16VREF(BgL_vz00_84, BgL_kz00_85);
		}

	}



/* &u16vector-ref */
	obj_t BGl_z62u16vectorzd2refzb0zz__srfi4z00(obj_t BgL_envz00_3267,
		obj_t BgL_vz00_3268, obj_t BgL_kz00_3269)
	{
		{	/* Llib/srfi4.scm 694 */
			{	/* Llib/srfi4.scm 695 */
				uint16_t BgL_tmpz00_4733;

				{	/* Llib/srfi4.scm 695 */
					obj_t BgL_vz00_3837;
					long BgL_kz00_3838;

					if (BGL_U16VECTORP(BgL_vz00_3268))
						{	/* Llib/srfi4.scm 695 */
							BgL_vz00_3837 = BgL_vz00_3268;
						}
					else
						{
							obj_t BgL_auxz00_4736;

							BgL_auxz00_4736 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(28376L), BGl_string2416z00zz__srfi4z00,
								BGl_string2376z00zz__srfi4z00, BgL_vz00_3268);
							FAILURE(BgL_auxz00_4736, BFALSE, BFALSE);
						}
					{	/* Llib/srfi4.scm 695 */
						obj_t BgL_tmpz00_4740;

						if (INTEGERP(BgL_kz00_3269))
							{	/* Llib/srfi4.scm 695 */
								BgL_tmpz00_4740 = BgL_kz00_3269;
							}
						else
							{
								obj_t BgL_auxz00_4743;

								BgL_auxz00_4743 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
									BINT(28376L), BGl_string2416z00zz__srfi4z00,
									BGl_string2390z00zz__srfi4z00, BgL_kz00_3269);
								FAILURE(BgL_auxz00_4743, BFALSE, BFALSE);
							}
						BgL_kz00_3838 = (long) CINT(BgL_tmpz00_4740);
					}
					BgL_tmpz00_4733 = BGL_U16VREF(BgL_vz00_3837, BgL_kz00_3838);
				}
				return BGL_UINT16_TO_BUINT16(BgL_tmpz00_4733);
			}
		}

	}



/* s32vector-ref */
	BGL_EXPORTED_DEF int32_t BGl_s32vectorzd2refzd2zz__srfi4z00(obj_t BgL_vz00_86,
		long BgL_kz00_87)
	{
		{	/* Llib/srfi4.scm 697 */
			return BGL_S32VREF(BgL_vz00_86, BgL_kz00_87);
		}

	}



/* &s32vector-ref */
	obj_t BGl_z62s32vectorzd2refzb0zz__srfi4z00(obj_t BgL_envz00_3277,
		obj_t BgL_vz00_3278, obj_t BgL_kz00_3279)
	{
		{	/* Llib/srfi4.scm 697 */
			{	/* Llib/srfi4.scm 698 */
				int32_t BgL_tmpz00_4751;

				{	/* Llib/srfi4.scm 698 */
					obj_t BgL_vz00_3839;
					long BgL_kz00_3840;

					if (BGL_S32VECTORP(BgL_vz00_3278))
						{	/* Llib/srfi4.scm 698 */
							BgL_vz00_3839 = BgL_vz00_3278;
						}
					else
						{
							obj_t BgL_auxz00_4754;

							BgL_auxz00_4754 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(28437L), BGl_string2417z00zz__srfi4z00,
								BGl_string2378z00zz__srfi4z00, BgL_vz00_3278);
							FAILURE(BgL_auxz00_4754, BFALSE, BFALSE);
						}
					{	/* Llib/srfi4.scm 698 */
						obj_t BgL_tmpz00_4758;

						if (INTEGERP(BgL_kz00_3279))
							{	/* Llib/srfi4.scm 698 */
								BgL_tmpz00_4758 = BgL_kz00_3279;
							}
						else
							{
								obj_t BgL_auxz00_4761;

								BgL_auxz00_4761 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
									BINT(28437L), BGl_string2417z00zz__srfi4z00,
									BGl_string2390z00zz__srfi4z00, BgL_kz00_3279);
								FAILURE(BgL_auxz00_4761, BFALSE, BFALSE);
							}
						BgL_kz00_3840 = (long) CINT(BgL_tmpz00_4758);
					}
					BgL_tmpz00_4751 = BGL_S32VREF(BgL_vz00_3839, BgL_kz00_3840);
				}
				return BGL_INT32_TO_BINT32(BgL_tmpz00_4751);
			}
		}

	}



/* u32vector-ref */
	BGL_EXPORTED_DEF uint32_t BGl_u32vectorzd2refzd2zz__srfi4z00(obj_t
		BgL_vz00_88, long BgL_kz00_89)
	{
		{	/* Llib/srfi4.scm 700 */
			return BGL_U32VREF(BgL_vz00_88, BgL_kz00_89);
		}

	}



/* &u32vector-ref */
	obj_t BGl_z62u32vectorzd2refzb0zz__srfi4z00(obj_t BgL_envz00_3287,
		obj_t BgL_vz00_3288, obj_t BgL_kz00_3289)
	{
		{	/* Llib/srfi4.scm 700 */
			{	/* Llib/srfi4.scm 701 */
				uint32_t BgL_tmpz00_4769;

				{	/* Llib/srfi4.scm 701 */
					obj_t BgL_vz00_3841;
					long BgL_kz00_3842;

					if (BGL_U32VECTORP(BgL_vz00_3288))
						{	/* Llib/srfi4.scm 701 */
							BgL_vz00_3841 = BgL_vz00_3288;
						}
					else
						{
							obj_t BgL_auxz00_4772;

							BgL_auxz00_4772 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(28498L), BGl_string2418z00zz__srfi4z00,
								BGl_string2380z00zz__srfi4z00, BgL_vz00_3288);
							FAILURE(BgL_auxz00_4772, BFALSE, BFALSE);
						}
					{	/* Llib/srfi4.scm 701 */
						obj_t BgL_tmpz00_4776;

						if (INTEGERP(BgL_kz00_3289))
							{	/* Llib/srfi4.scm 701 */
								BgL_tmpz00_4776 = BgL_kz00_3289;
							}
						else
							{
								obj_t BgL_auxz00_4779;

								BgL_auxz00_4779 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
									BINT(28498L), BGl_string2418z00zz__srfi4z00,
									BGl_string2390z00zz__srfi4z00, BgL_kz00_3289);
								FAILURE(BgL_auxz00_4779, BFALSE, BFALSE);
							}
						BgL_kz00_3842 = (long) CINT(BgL_tmpz00_4776);
					}
					BgL_tmpz00_4769 = BGL_U32VREF(BgL_vz00_3841, BgL_kz00_3842);
				}
				return BGL_UINT32_TO_BUINT32(BgL_tmpz00_4769);
			}
		}

	}



/* s64vector-ref */
	BGL_EXPORTED_DEF int64_t BGl_s64vectorzd2refzd2zz__srfi4z00(obj_t BgL_vz00_90,
		long BgL_kz00_91)
	{
		{	/* Llib/srfi4.scm 703 */
			return BGL_S64VREF(BgL_vz00_90, BgL_kz00_91);
		}

	}



/* &s64vector-ref */
	obj_t BGl_z62s64vectorzd2refzb0zz__srfi4z00(obj_t BgL_envz00_3297,
		obj_t BgL_vz00_3298, obj_t BgL_kz00_3299)
	{
		{	/* Llib/srfi4.scm 703 */
			{	/* Llib/srfi4.scm 704 */
				int64_t BgL_tmpz00_4787;

				{	/* Llib/srfi4.scm 704 */
					obj_t BgL_vz00_3843;
					long BgL_kz00_3844;

					if (BGL_S64VECTORP(BgL_vz00_3298))
						{	/* Llib/srfi4.scm 704 */
							BgL_vz00_3843 = BgL_vz00_3298;
						}
					else
						{
							obj_t BgL_auxz00_4790;

							BgL_auxz00_4790 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(28559L), BGl_string2419z00zz__srfi4z00,
								BGl_string2382z00zz__srfi4z00, BgL_vz00_3298);
							FAILURE(BgL_auxz00_4790, BFALSE, BFALSE);
						}
					{	/* Llib/srfi4.scm 704 */
						obj_t BgL_tmpz00_4794;

						if (INTEGERP(BgL_kz00_3299))
							{	/* Llib/srfi4.scm 704 */
								BgL_tmpz00_4794 = BgL_kz00_3299;
							}
						else
							{
								obj_t BgL_auxz00_4797;

								BgL_auxz00_4797 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
									BINT(28559L), BGl_string2419z00zz__srfi4z00,
									BGl_string2390z00zz__srfi4z00, BgL_kz00_3299);
								FAILURE(BgL_auxz00_4797, BFALSE, BFALSE);
							}
						BgL_kz00_3844 = (long) CINT(BgL_tmpz00_4794);
					}
					BgL_tmpz00_4787 = BGL_S64VREF(BgL_vz00_3843, BgL_kz00_3844);
				}
				return BGL_INT64_TO_BINT64(BgL_tmpz00_4787);
			}
		}

	}



/* u64vector-ref */
	BGL_EXPORTED_DEF uint64_t BGl_u64vectorzd2refzd2zz__srfi4z00(obj_t
		BgL_vz00_92, long BgL_kz00_93)
	{
		{	/* Llib/srfi4.scm 706 */
			return BGL_U64VREF(BgL_vz00_92, BgL_kz00_93);
		}

	}



/* &u64vector-ref */
	obj_t BGl_z62u64vectorzd2refzb0zz__srfi4z00(obj_t BgL_envz00_3307,
		obj_t BgL_vz00_3308, obj_t BgL_kz00_3309)
	{
		{	/* Llib/srfi4.scm 706 */
			{	/* Llib/srfi4.scm 707 */
				uint64_t BgL_tmpz00_4805;

				{	/* Llib/srfi4.scm 707 */
					obj_t BgL_vz00_3845;
					long BgL_kz00_3846;

					if (BGL_U64VECTORP(BgL_vz00_3308))
						{	/* Llib/srfi4.scm 707 */
							BgL_vz00_3845 = BgL_vz00_3308;
						}
					else
						{
							obj_t BgL_auxz00_4808;

							BgL_auxz00_4808 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(28620L), BGl_string2420z00zz__srfi4z00,
								BGl_string2384z00zz__srfi4z00, BgL_vz00_3308);
							FAILURE(BgL_auxz00_4808, BFALSE, BFALSE);
						}
					{	/* Llib/srfi4.scm 707 */
						obj_t BgL_tmpz00_4812;

						if (INTEGERP(BgL_kz00_3309))
							{	/* Llib/srfi4.scm 707 */
								BgL_tmpz00_4812 = BgL_kz00_3309;
							}
						else
							{
								obj_t BgL_auxz00_4815;

								BgL_auxz00_4815 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
									BINT(28620L), BGl_string2420z00zz__srfi4z00,
									BGl_string2390z00zz__srfi4z00, BgL_kz00_3309);
								FAILURE(BgL_auxz00_4815, BFALSE, BFALSE);
							}
						BgL_kz00_3846 = (long) CINT(BgL_tmpz00_4812);
					}
					BgL_tmpz00_4805 = BGL_U64VREF(BgL_vz00_3845, BgL_kz00_3846);
				}
				return BGL_UINT64_TO_BUINT64(BgL_tmpz00_4805);
			}
		}

	}



/* f32vector-ref */
	BGL_EXPORTED_DEF float BGl_f32vectorzd2refzd2zz__srfi4z00(obj_t BgL_vz00_94,
		long BgL_kz00_95)
	{
		{	/* Llib/srfi4.scm 709 */
			return BGL_F32VREF(BgL_vz00_94, BgL_kz00_95);
		}

	}



/* &f32vector-ref */
	obj_t BGl_z62f32vectorzd2refzb0zz__srfi4z00(obj_t BgL_envz00_3317,
		obj_t BgL_vz00_3318, obj_t BgL_kz00_3319)
	{
		{	/* Llib/srfi4.scm 709 */
			{	/* Llib/srfi4.scm 710 */
				float BgL_tmpz00_4823;

				{	/* Llib/srfi4.scm 710 */
					obj_t BgL_vz00_3847;
					long BgL_kz00_3848;

					if (BGL_F32VECTORP(BgL_vz00_3318))
						{	/* Llib/srfi4.scm 710 */
							BgL_vz00_3847 = BgL_vz00_3318;
						}
					else
						{
							obj_t BgL_auxz00_4826;

							BgL_auxz00_4826 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(28681L), BGl_string2421z00zz__srfi4z00,
								BGl_string2386z00zz__srfi4z00, BgL_vz00_3318);
							FAILURE(BgL_auxz00_4826, BFALSE, BFALSE);
						}
					{	/* Llib/srfi4.scm 710 */
						obj_t BgL_tmpz00_4830;

						if (INTEGERP(BgL_kz00_3319))
							{	/* Llib/srfi4.scm 710 */
								BgL_tmpz00_4830 = BgL_kz00_3319;
							}
						else
							{
								obj_t BgL_auxz00_4833;

								BgL_auxz00_4833 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
									BINT(28681L), BGl_string2421z00zz__srfi4z00,
									BGl_string2390z00zz__srfi4z00, BgL_kz00_3319);
								FAILURE(BgL_auxz00_4833, BFALSE, BFALSE);
							}
						BgL_kz00_3848 = (long) CINT(BgL_tmpz00_4830);
					}
					BgL_tmpz00_4823 = BGL_F32VREF(BgL_vz00_3847, BgL_kz00_3848);
				}
				return FLOAT_TO_REAL(BgL_tmpz00_4823);
			}
		}

	}



/* f64vector-ref */
	BGL_EXPORTED_DEF double BGl_f64vectorzd2refzd2zz__srfi4z00(obj_t BgL_vz00_96,
		long BgL_kz00_97)
	{
		{	/* Llib/srfi4.scm 712 */
			return BGL_F64VREF(BgL_vz00_96, BgL_kz00_97);
		}

	}



/* &f64vector-ref */
	obj_t BGl_z62f64vectorzd2refzb0zz__srfi4z00(obj_t BgL_envz00_3327,
		obj_t BgL_vz00_3328, obj_t BgL_kz00_3329)
	{
		{	/* Llib/srfi4.scm 712 */
			{	/* Llib/srfi4.scm 713 */
				double BgL_tmpz00_4841;

				{	/* Llib/srfi4.scm 713 */
					obj_t BgL_vz00_3849;
					long BgL_kz00_3850;

					if (BGL_F64VECTORP(BgL_vz00_3328))
						{	/* Llib/srfi4.scm 713 */
							BgL_vz00_3849 = BgL_vz00_3328;
						}
					else
						{
							obj_t BgL_auxz00_4844;

							BgL_auxz00_4844 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(28742L), BGl_string2422z00zz__srfi4z00,
								BGl_string2388z00zz__srfi4z00, BgL_vz00_3328);
							FAILURE(BgL_auxz00_4844, BFALSE, BFALSE);
						}
					{	/* Llib/srfi4.scm 713 */
						obj_t BgL_tmpz00_4848;

						if (INTEGERP(BgL_kz00_3329))
							{	/* Llib/srfi4.scm 713 */
								BgL_tmpz00_4848 = BgL_kz00_3329;
							}
						else
							{
								obj_t BgL_auxz00_4851;

								BgL_auxz00_4851 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
									BINT(28742L), BGl_string2422z00zz__srfi4z00,
									BGl_string2390z00zz__srfi4z00, BgL_kz00_3329);
								FAILURE(BgL_auxz00_4851, BFALSE, BFALSE);
							}
						BgL_kz00_3850 = (long) CINT(BgL_tmpz00_4848);
					}
					BgL_tmpz00_4841 = BGL_F64VREF(BgL_vz00_3849, BgL_kz00_3850);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_4841);
			}
		}

	}



/* s8vector-set! */
	BGL_EXPORTED_DEF obj_t BGl_s8vectorzd2setz12zc0zz__srfi4z00(obj_t
		BgL_vz00_100, long BgL_kz00_101, int8_t BgL_valz00_102)
	{
		{	/* Llib/srfi4.scm 721 */
			BGL_S8VSET(BgL_vz00_100, BgL_kz00_101, BgL_valz00_102);
			return BUNSPEC;
		}

	}



/* &s8vector-set! */
	obj_t BGl_z62s8vectorzd2setz12za2zz__srfi4z00(obj_t BgL_envz00_3240,
		obj_t BgL_vz00_3241, obj_t BgL_kz00_3242, obj_t BgL_valz00_3243)
	{
		{	/* Llib/srfi4.scm 721 */
			{	/* Llib/srfi4.scm 721 */
				obj_t BgL_vz00_3851;
				long BgL_kz00_3852;
				int8_t BgL_valz00_3853;

				if (BGL_S8VECTORP(BgL_vz00_3241))
					{	/* Llib/srfi4.scm 721 */
						BgL_vz00_3851 = BgL_vz00_3241;
					}
				else
					{
						obj_t BgL_auxz00_4861;

						BgL_auxz00_4861 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
							BINT(29057L), BGl_string2423z00zz__srfi4z00,
							BGl_string2370z00zz__srfi4z00, BgL_vz00_3241);
						FAILURE(BgL_auxz00_4861, BFALSE, BFALSE);
					}
				{	/* Llib/srfi4.scm 721 */
					obj_t BgL_tmpz00_4865;

					if (INTEGERP(BgL_kz00_3242))
						{	/* Llib/srfi4.scm 721 */
							BgL_tmpz00_4865 = BgL_kz00_3242;
						}
					else
						{
							obj_t BgL_auxz00_4868;

							BgL_auxz00_4868 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(29057L), BGl_string2423z00zz__srfi4z00,
								BGl_string2390z00zz__srfi4z00, BgL_kz00_3242);
							FAILURE(BgL_auxz00_4868, BFALSE, BFALSE);
						}
					BgL_kz00_3852 = (long) CINT(BgL_tmpz00_4865);
				}
				{	/* Llib/srfi4.scm 721 */
					obj_t BgL_tmpz00_4873;

					if (BGL_INT8P(BgL_valz00_3243))
						{	/* Llib/srfi4.scm 721 */
							BgL_tmpz00_4873 = BgL_valz00_3243;
						}
					else
						{
							obj_t BgL_auxz00_4876;

							BgL_auxz00_4876 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(29057L), BGl_string2423z00zz__srfi4z00,
								BGl_string2391z00zz__srfi4z00, BgL_valz00_3243);
							FAILURE(BgL_auxz00_4876, BFALSE, BFALSE);
						}
					BgL_valz00_3853 = BGL_BINT8_TO_INT8(BgL_tmpz00_4873);
				}
				BGL_S8VSET(BgL_vz00_3851, BgL_kz00_3852, BgL_valz00_3853);
				return BUNSPEC;
			}
		}

	}



/* u8vector-set! */
	BGL_EXPORTED_DEF obj_t BGl_u8vectorzd2setz12zc0zz__srfi4z00(obj_t
		BgL_vz00_103, long BgL_kz00_104, uint8_t BgL_valz00_105)
	{
		{	/* Llib/srfi4.scm 724 */
			BGL_U8VSET(BgL_vz00_103, BgL_kz00_104, BgL_valz00_105);
			return BUNSPEC;
		}

	}



/* &u8vector-set! */
	obj_t BGl_z62u8vectorzd2setz12za2zz__srfi4z00(obj_t BgL_envz00_3250,
		obj_t BgL_vz00_3251, obj_t BgL_kz00_3252, obj_t BgL_valz00_3253)
	{
		{	/* Llib/srfi4.scm 724 */
			{	/* Llib/srfi4.scm 724 */
				obj_t BgL_vz00_3854;
				long BgL_kz00_3855;
				uint8_t BgL_valz00_3856;

				if (BGL_U8VECTORP(BgL_vz00_3251))
					{	/* Llib/srfi4.scm 724 */
						BgL_vz00_3854 = BgL_vz00_3251;
					}
				else
					{
						obj_t BgL_auxz00_4885;

						BgL_auxz00_4885 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
							BINT(29126L), BGl_string2424z00zz__srfi4z00,
							BGl_string2372z00zz__srfi4z00, BgL_vz00_3251);
						FAILURE(BgL_auxz00_4885, BFALSE, BFALSE);
					}
				{	/* Llib/srfi4.scm 724 */
					obj_t BgL_tmpz00_4889;

					if (INTEGERP(BgL_kz00_3252))
						{	/* Llib/srfi4.scm 724 */
							BgL_tmpz00_4889 = BgL_kz00_3252;
						}
					else
						{
							obj_t BgL_auxz00_4892;

							BgL_auxz00_4892 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(29126L), BGl_string2424z00zz__srfi4z00,
								BGl_string2390z00zz__srfi4z00, BgL_kz00_3252);
							FAILURE(BgL_auxz00_4892, BFALSE, BFALSE);
						}
					BgL_kz00_3855 = (long) CINT(BgL_tmpz00_4889);
				}
				{	/* Llib/srfi4.scm 724 */
					obj_t BgL_tmpz00_4897;

					if (BGL_UINT8P(BgL_valz00_3253))
						{	/* Llib/srfi4.scm 724 */
							BgL_tmpz00_4897 = BgL_valz00_3253;
						}
					else
						{
							obj_t BgL_auxz00_4900;

							BgL_auxz00_4900 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(29126L), BGl_string2424z00zz__srfi4z00,
								BGl_string2393z00zz__srfi4z00, BgL_valz00_3253);
							FAILURE(BgL_auxz00_4900, BFALSE, BFALSE);
						}
					BgL_valz00_3856 = BGL_BUINT8_TO_UINT8(BgL_tmpz00_4897);
				}
				BGL_U8VSET(BgL_vz00_3854, BgL_kz00_3855, BgL_valz00_3856);
				return BUNSPEC;
			}
		}

	}



/* s16vector-set! */
	BGL_EXPORTED_DEF obj_t BGl_s16vectorzd2setz12zc0zz__srfi4z00(obj_t
		BgL_vz00_106, long BgL_kz00_107, int16_t BgL_valz00_108)
	{
		{	/* Llib/srfi4.scm 727 */
			BGL_S16VSET(BgL_vz00_106, BgL_kz00_107, BgL_valz00_108);
			return BUNSPEC;
		}

	}



/* &s16vector-set! */
	obj_t BGl_z62s16vectorzd2setz12za2zz__srfi4z00(obj_t BgL_envz00_3260,
		obj_t BgL_vz00_3261, obj_t BgL_kz00_3262, obj_t BgL_valz00_3263)
	{
		{	/* Llib/srfi4.scm 727 */
			{	/* Llib/srfi4.scm 727 */
				obj_t BgL_vz00_3857;
				long BgL_kz00_3858;
				int16_t BgL_valz00_3859;

				if (BGL_S16VECTORP(BgL_vz00_3261))
					{	/* Llib/srfi4.scm 727 */
						BgL_vz00_3857 = BgL_vz00_3261;
					}
				else
					{
						obj_t BgL_auxz00_4909;

						BgL_auxz00_4909 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
							BINT(29195L), BGl_string2425z00zz__srfi4z00,
							BGl_string2374z00zz__srfi4z00, BgL_vz00_3261);
						FAILURE(BgL_auxz00_4909, BFALSE, BFALSE);
					}
				{	/* Llib/srfi4.scm 727 */
					obj_t BgL_tmpz00_4913;

					if (INTEGERP(BgL_kz00_3262))
						{	/* Llib/srfi4.scm 727 */
							BgL_tmpz00_4913 = BgL_kz00_3262;
						}
					else
						{
							obj_t BgL_auxz00_4916;

							BgL_auxz00_4916 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(29195L), BGl_string2425z00zz__srfi4z00,
								BGl_string2390z00zz__srfi4z00, BgL_kz00_3262);
							FAILURE(BgL_auxz00_4916, BFALSE, BFALSE);
						}
					BgL_kz00_3858 = (long) CINT(BgL_tmpz00_4913);
				}
				{	/* Llib/srfi4.scm 727 */
					obj_t BgL_tmpz00_4921;

					if (BGL_INT16P(BgL_valz00_3263))
						{	/* Llib/srfi4.scm 727 */
							BgL_tmpz00_4921 = BgL_valz00_3263;
						}
					else
						{
							obj_t BgL_auxz00_4924;

							BgL_auxz00_4924 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(29195L), BGl_string2425z00zz__srfi4z00,
								BGl_string2395z00zz__srfi4z00, BgL_valz00_3263);
							FAILURE(BgL_auxz00_4924, BFALSE, BFALSE);
						}
					BgL_valz00_3859 = BGL_BINT16_TO_INT16(BgL_tmpz00_4921);
				}
				BGL_S16VSET(BgL_vz00_3857, BgL_kz00_3858, BgL_valz00_3859);
				return BUNSPEC;
			}
		}

	}



/* u16vector-set! */
	BGL_EXPORTED_DEF obj_t BGl_u16vectorzd2setz12zc0zz__srfi4z00(obj_t
		BgL_vz00_109, long BgL_kz00_110, uint16_t BgL_valz00_111)
	{
		{	/* Llib/srfi4.scm 730 */
			BGL_U16VSET(BgL_vz00_109, BgL_kz00_110, BgL_valz00_111);
			return BUNSPEC;
		}

	}



/* &u16vector-set! */
	obj_t BGl_z62u16vectorzd2setz12za2zz__srfi4z00(obj_t BgL_envz00_3270,
		obj_t BgL_vz00_3271, obj_t BgL_kz00_3272, obj_t BgL_valz00_3273)
	{
		{	/* Llib/srfi4.scm 730 */
			{	/* Llib/srfi4.scm 730 */
				obj_t BgL_vz00_3860;
				long BgL_kz00_3861;
				uint16_t BgL_valz00_3862;

				if (BGL_U16VECTORP(BgL_vz00_3271))
					{	/* Llib/srfi4.scm 730 */
						BgL_vz00_3860 = BgL_vz00_3271;
					}
				else
					{
						obj_t BgL_auxz00_4933;

						BgL_auxz00_4933 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
							BINT(29266L), BGl_string2426z00zz__srfi4z00,
							BGl_string2376z00zz__srfi4z00, BgL_vz00_3271);
						FAILURE(BgL_auxz00_4933, BFALSE, BFALSE);
					}
				{	/* Llib/srfi4.scm 730 */
					obj_t BgL_tmpz00_4937;

					if (INTEGERP(BgL_kz00_3272))
						{	/* Llib/srfi4.scm 730 */
							BgL_tmpz00_4937 = BgL_kz00_3272;
						}
					else
						{
							obj_t BgL_auxz00_4940;

							BgL_auxz00_4940 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(29266L), BGl_string2426z00zz__srfi4z00,
								BGl_string2390z00zz__srfi4z00, BgL_kz00_3272);
							FAILURE(BgL_auxz00_4940, BFALSE, BFALSE);
						}
					BgL_kz00_3861 = (long) CINT(BgL_tmpz00_4937);
				}
				{	/* Llib/srfi4.scm 730 */
					obj_t BgL_tmpz00_4945;

					if (BGL_UINT16P(BgL_valz00_3273))
						{	/* Llib/srfi4.scm 730 */
							BgL_tmpz00_4945 = BgL_valz00_3273;
						}
					else
						{
							obj_t BgL_auxz00_4948;

							BgL_auxz00_4948 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(29266L), BGl_string2426z00zz__srfi4z00,
								BGl_string2397z00zz__srfi4z00, BgL_valz00_3273);
							FAILURE(BgL_auxz00_4948, BFALSE, BFALSE);
						}
					BgL_valz00_3862 = BGL_BUINT16_TO_UINT16(BgL_tmpz00_4945);
				}
				BGL_U16VSET(BgL_vz00_3860, BgL_kz00_3861, BgL_valz00_3862);
				return BUNSPEC;
			}
		}

	}



/* s32vector-set! */
	BGL_EXPORTED_DEF obj_t BGl_s32vectorzd2setz12zc0zz__srfi4z00(obj_t
		BgL_vz00_112, long BgL_kz00_113, int32_t BgL_valz00_114)
	{
		{	/* Llib/srfi4.scm 733 */
			BGL_S32VSET(BgL_vz00_112, BgL_kz00_113, BgL_valz00_114);
			return BUNSPEC;
		}

	}



/* &s32vector-set! */
	obj_t BGl_z62s32vectorzd2setz12za2zz__srfi4z00(obj_t BgL_envz00_3280,
		obj_t BgL_vz00_3281, obj_t BgL_kz00_3282, obj_t BgL_valz00_3283)
	{
		{	/* Llib/srfi4.scm 733 */
			{	/* Llib/srfi4.scm 733 */
				obj_t BgL_vz00_3863;
				long BgL_kz00_3864;
				int32_t BgL_valz00_3865;

				if (BGL_S32VECTORP(BgL_vz00_3281))
					{	/* Llib/srfi4.scm 733 */
						BgL_vz00_3863 = BgL_vz00_3281;
					}
				else
					{
						obj_t BgL_auxz00_4957;

						BgL_auxz00_4957 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
							BINT(29337L), BGl_string2427z00zz__srfi4z00,
							BGl_string2378z00zz__srfi4z00, BgL_vz00_3281);
						FAILURE(BgL_auxz00_4957, BFALSE, BFALSE);
					}
				{	/* Llib/srfi4.scm 733 */
					obj_t BgL_tmpz00_4961;

					if (INTEGERP(BgL_kz00_3282))
						{	/* Llib/srfi4.scm 733 */
							BgL_tmpz00_4961 = BgL_kz00_3282;
						}
					else
						{
							obj_t BgL_auxz00_4964;

							BgL_auxz00_4964 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(29337L), BGl_string2427z00zz__srfi4z00,
								BGl_string2390z00zz__srfi4z00, BgL_kz00_3282);
							FAILURE(BgL_auxz00_4964, BFALSE, BFALSE);
						}
					BgL_kz00_3864 = (long) CINT(BgL_tmpz00_4961);
				}
				{	/* Llib/srfi4.scm 733 */
					obj_t BgL_tmpz00_4969;

					if (BGL_INT32P(BgL_valz00_3283))
						{	/* Llib/srfi4.scm 733 */
							BgL_tmpz00_4969 = BgL_valz00_3283;
						}
					else
						{
							obj_t BgL_auxz00_4972;

							BgL_auxz00_4972 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(29337L), BGl_string2427z00zz__srfi4z00,
								BGl_string2399z00zz__srfi4z00, BgL_valz00_3283);
							FAILURE(BgL_auxz00_4972, BFALSE, BFALSE);
						}
					BgL_valz00_3865 = BGL_BINT32_TO_INT32(BgL_tmpz00_4969);
				}
				BGL_S32VSET(BgL_vz00_3863, BgL_kz00_3864, BgL_valz00_3865);
				return BUNSPEC;
			}
		}

	}



/* u32vector-set! */
	BGL_EXPORTED_DEF obj_t BGl_u32vectorzd2setz12zc0zz__srfi4z00(obj_t
		BgL_vz00_115, long BgL_kz00_116, uint32_t BgL_valz00_117)
	{
		{	/* Llib/srfi4.scm 736 */
			BGL_U32VSET(BgL_vz00_115, BgL_kz00_116, BgL_valz00_117);
			return BUNSPEC;
		}

	}



/* &u32vector-set! */
	obj_t BGl_z62u32vectorzd2setz12za2zz__srfi4z00(obj_t BgL_envz00_3290,
		obj_t BgL_vz00_3291, obj_t BgL_kz00_3292, obj_t BgL_valz00_3293)
	{
		{	/* Llib/srfi4.scm 736 */
			{	/* Llib/srfi4.scm 736 */
				obj_t BgL_vz00_3866;
				long BgL_kz00_3867;
				uint32_t BgL_valz00_3868;

				if (BGL_U32VECTORP(BgL_vz00_3291))
					{	/* Llib/srfi4.scm 736 */
						BgL_vz00_3866 = BgL_vz00_3291;
					}
				else
					{
						obj_t BgL_auxz00_4981;

						BgL_auxz00_4981 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
							BINT(29408L), BGl_string2428z00zz__srfi4z00,
							BGl_string2380z00zz__srfi4z00, BgL_vz00_3291);
						FAILURE(BgL_auxz00_4981, BFALSE, BFALSE);
					}
				{	/* Llib/srfi4.scm 736 */
					obj_t BgL_tmpz00_4985;

					if (INTEGERP(BgL_kz00_3292))
						{	/* Llib/srfi4.scm 736 */
							BgL_tmpz00_4985 = BgL_kz00_3292;
						}
					else
						{
							obj_t BgL_auxz00_4988;

							BgL_auxz00_4988 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(29408L), BGl_string2428z00zz__srfi4z00,
								BGl_string2390z00zz__srfi4z00, BgL_kz00_3292);
							FAILURE(BgL_auxz00_4988, BFALSE, BFALSE);
						}
					BgL_kz00_3867 = (long) CINT(BgL_tmpz00_4985);
				}
				{	/* Llib/srfi4.scm 736 */
					obj_t BgL_tmpz00_4993;

					if (BGL_UINT32P(BgL_valz00_3293))
						{	/* Llib/srfi4.scm 736 */
							BgL_tmpz00_4993 = BgL_valz00_3293;
						}
					else
						{
							obj_t BgL_auxz00_4996;

							BgL_auxz00_4996 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(29408L), BGl_string2428z00zz__srfi4z00,
								BGl_string2401z00zz__srfi4z00, BgL_valz00_3293);
							FAILURE(BgL_auxz00_4996, BFALSE, BFALSE);
						}
					BgL_valz00_3868 = BGL_BUINT32_TO_UINT32(BgL_tmpz00_4993);
				}
				BGL_U32VSET(BgL_vz00_3866, BgL_kz00_3867, BgL_valz00_3868);
				return BUNSPEC;
			}
		}

	}



/* s64vector-set! */
	BGL_EXPORTED_DEF obj_t BGl_s64vectorzd2setz12zc0zz__srfi4z00(obj_t
		BgL_vz00_118, long BgL_kz00_119, int64_t BgL_valz00_120)
	{
		{	/* Llib/srfi4.scm 739 */
			BGL_S64VSET(BgL_vz00_118, BgL_kz00_119, BgL_valz00_120);
			return BUNSPEC;
		}

	}



/* &s64vector-set! */
	obj_t BGl_z62s64vectorzd2setz12za2zz__srfi4z00(obj_t BgL_envz00_3300,
		obj_t BgL_vz00_3301, obj_t BgL_kz00_3302, obj_t BgL_valz00_3303)
	{
		{	/* Llib/srfi4.scm 739 */
			{	/* Llib/srfi4.scm 739 */
				obj_t BgL_vz00_3869;
				long BgL_kz00_3870;
				int64_t BgL_valz00_3871;

				if (BGL_S64VECTORP(BgL_vz00_3301))
					{	/* Llib/srfi4.scm 739 */
						BgL_vz00_3869 = BgL_vz00_3301;
					}
				else
					{
						obj_t BgL_auxz00_5005;

						BgL_auxz00_5005 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
							BINT(29479L), BGl_string2429z00zz__srfi4z00,
							BGl_string2382z00zz__srfi4z00, BgL_vz00_3301);
						FAILURE(BgL_auxz00_5005, BFALSE, BFALSE);
					}
				{	/* Llib/srfi4.scm 739 */
					obj_t BgL_tmpz00_5009;

					if (INTEGERP(BgL_kz00_3302))
						{	/* Llib/srfi4.scm 739 */
							BgL_tmpz00_5009 = BgL_kz00_3302;
						}
					else
						{
							obj_t BgL_auxz00_5012;

							BgL_auxz00_5012 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(29479L), BGl_string2429z00zz__srfi4z00,
								BGl_string2390z00zz__srfi4z00, BgL_kz00_3302);
							FAILURE(BgL_auxz00_5012, BFALSE, BFALSE);
						}
					BgL_kz00_3870 = (long) CINT(BgL_tmpz00_5009);
				}
				{	/* Llib/srfi4.scm 739 */
					obj_t BgL_tmpz00_5017;

					if (BGL_INT64P(BgL_valz00_3303))
						{	/* Llib/srfi4.scm 739 */
							BgL_tmpz00_5017 = BgL_valz00_3303;
						}
					else
						{
							obj_t BgL_auxz00_5020;

							BgL_auxz00_5020 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(29479L), BGl_string2429z00zz__srfi4z00,
								BGl_string2403z00zz__srfi4z00, BgL_valz00_3303);
							FAILURE(BgL_auxz00_5020, BFALSE, BFALSE);
						}
					BgL_valz00_3871 = BGL_BINT64_TO_INT64(BgL_tmpz00_5017);
				}
				BGL_S64VSET(BgL_vz00_3869, BgL_kz00_3870, BgL_valz00_3871);
				return BUNSPEC;
			}
		}

	}



/* u64vector-set! */
	BGL_EXPORTED_DEF obj_t BGl_u64vectorzd2setz12zc0zz__srfi4z00(obj_t
		BgL_vz00_121, long BgL_kz00_122, uint64_t BgL_valz00_123)
	{
		{	/* Llib/srfi4.scm 742 */
			BGL_U64VSET(BgL_vz00_121, BgL_kz00_122, BgL_valz00_123);
			return BUNSPEC;
		}

	}



/* &u64vector-set! */
	obj_t BGl_z62u64vectorzd2setz12za2zz__srfi4z00(obj_t BgL_envz00_3310,
		obj_t BgL_vz00_3311, obj_t BgL_kz00_3312, obj_t BgL_valz00_3313)
	{
		{	/* Llib/srfi4.scm 742 */
			{	/* Llib/srfi4.scm 742 */
				obj_t BgL_vz00_3872;
				long BgL_kz00_3873;
				uint64_t BgL_valz00_3874;

				if (BGL_U64VECTORP(BgL_vz00_3311))
					{	/* Llib/srfi4.scm 742 */
						BgL_vz00_3872 = BgL_vz00_3311;
					}
				else
					{
						obj_t BgL_auxz00_5029;

						BgL_auxz00_5029 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
							BINT(29550L), BGl_string2430z00zz__srfi4z00,
							BGl_string2384z00zz__srfi4z00, BgL_vz00_3311);
						FAILURE(BgL_auxz00_5029, BFALSE, BFALSE);
					}
				{	/* Llib/srfi4.scm 742 */
					obj_t BgL_tmpz00_5033;

					if (INTEGERP(BgL_kz00_3312))
						{	/* Llib/srfi4.scm 742 */
							BgL_tmpz00_5033 = BgL_kz00_3312;
						}
					else
						{
							obj_t BgL_auxz00_5036;

							BgL_auxz00_5036 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(29550L), BGl_string2430z00zz__srfi4z00,
								BGl_string2390z00zz__srfi4z00, BgL_kz00_3312);
							FAILURE(BgL_auxz00_5036, BFALSE, BFALSE);
						}
					BgL_kz00_3873 = (long) CINT(BgL_tmpz00_5033);
				}
				{	/* Llib/srfi4.scm 742 */
					obj_t BgL_tmpz00_5041;

					if (BGL_UINT64P(BgL_valz00_3313))
						{	/* Llib/srfi4.scm 742 */
							BgL_tmpz00_5041 = BgL_valz00_3313;
						}
					else
						{
							obj_t BgL_auxz00_5044;

							BgL_auxz00_5044 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(29550L), BGl_string2430z00zz__srfi4z00,
								BGl_string2405z00zz__srfi4z00, BgL_valz00_3313);
							FAILURE(BgL_auxz00_5044, BFALSE, BFALSE);
						}
					BgL_valz00_3874 = BGL_BINT64_TO_INT64(BgL_tmpz00_5041);
				}
				BGL_U64VSET(BgL_vz00_3872, BgL_kz00_3873, BgL_valz00_3874);
				return BUNSPEC;
			}
		}

	}



/* f32vector-set! */
	BGL_EXPORTED_DEF obj_t BGl_f32vectorzd2setz12zc0zz__srfi4z00(obj_t
		BgL_vz00_124, long BgL_kz00_125, float BgL_valz00_126)
	{
		{	/* Llib/srfi4.scm 745 */
			BGL_F32VSET(BgL_vz00_124, BgL_kz00_125, BgL_valz00_126);
			return BUNSPEC;
		}

	}



/* &f32vector-set! */
	obj_t BGl_z62f32vectorzd2setz12za2zz__srfi4z00(obj_t BgL_envz00_3320,
		obj_t BgL_vz00_3321, obj_t BgL_kz00_3322, obj_t BgL_valz00_3323)
	{
		{	/* Llib/srfi4.scm 745 */
			{	/* Llib/srfi4.scm 745 */
				obj_t BgL_vz00_3875;
				long BgL_kz00_3876;
				float BgL_valz00_3877;

				if (BGL_F32VECTORP(BgL_vz00_3321))
					{	/* Llib/srfi4.scm 745 */
						BgL_vz00_3875 = BgL_vz00_3321;
					}
				else
					{
						obj_t BgL_auxz00_5053;

						BgL_auxz00_5053 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
							BINT(29621L), BGl_string2431z00zz__srfi4z00,
							BGl_string2386z00zz__srfi4z00, BgL_vz00_3321);
						FAILURE(BgL_auxz00_5053, BFALSE, BFALSE);
					}
				{	/* Llib/srfi4.scm 745 */
					obj_t BgL_tmpz00_5057;

					if (INTEGERP(BgL_kz00_3322))
						{	/* Llib/srfi4.scm 745 */
							BgL_tmpz00_5057 = BgL_kz00_3322;
						}
					else
						{
							obj_t BgL_auxz00_5060;

							BgL_auxz00_5060 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(29621L), BGl_string2431z00zz__srfi4z00,
								BGl_string2390z00zz__srfi4z00, BgL_kz00_3322);
							FAILURE(BgL_auxz00_5060, BFALSE, BFALSE);
						}
					BgL_kz00_3876 = (long) CINT(BgL_tmpz00_5057);
				}
				{	/* Llib/srfi4.scm 745 */
					obj_t BgL_tmpz00_5065;

					if (REALP(BgL_valz00_3323))
						{	/* Llib/srfi4.scm 745 */
							BgL_tmpz00_5065 = BgL_valz00_3323;
						}
					else
						{
							obj_t BgL_auxz00_5068;

							BgL_auxz00_5068 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(29621L), BGl_string2431z00zz__srfi4z00,
								BGl_string2407z00zz__srfi4z00, BgL_valz00_3323);
							FAILURE(BgL_auxz00_5068, BFALSE, BFALSE);
						}
					BgL_valz00_3877 = REAL_TO_FLOAT(BgL_tmpz00_5065);
				}
				BGL_F32VSET(BgL_vz00_3875, BgL_kz00_3876, BgL_valz00_3877);
				return BUNSPEC;
			}
		}

	}



/* f64vector-set! */
	BGL_EXPORTED_DEF obj_t BGl_f64vectorzd2setz12zc0zz__srfi4z00(obj_t
		BgL_vz00_127, long BgL_kz00_128, double BgL_valz00_129)
	{
		{	/* Llib/srfi4.scm 748 */
			BGL_F64VSET(BgL_vz00_127, BgL_kz00_128, BgL_valz00_129);
			return BUNSPEC;
		}

	}



/* &f64vector-set! */
	obj_t BGl_z62f64vectorzd2setz12za2zz__srfi4z00(obj_t BgL_envz00_3330,
		obj_t BgL_vz00_3331, obj_t BgL_kz00_3332, obj_t BgL_valz00_3333)
	{
		{	/* Llib/srfi4.scm 748 */
			{	/* Llib/srfi4.scm 748 */
				obj_t BgL_vz00_3878;
				long BgL_kz00_3879;
				double BgL_valz00_3880;

				if (BGL_F64VECTORP(BgL_vz00_3331))
					{	/* Llib/srfi4.scm 748 */
						BgL_vz00_3878 = BgL_vz00_3331;
					}
				else
					{
						obj_t BgL_auxz00_5077;

						BgL_auxz00_5077 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
							BINT(29692L), BGl_string2432z00zz__srfi4z00,
							BGl_string2388z00zz__srfi4z00, BgL_vz00_3331);
						FAILURE(BgL_auxz00_5077, BFALSE, BFALSE);
					}
				{	/* Llib/srfi4.scm 748 */
					obj_t BgL_tmpz00_5081;

					if (INTEGERP(BgL_kz00_3332))
						{	/* Llib/srfi4.scm 748 */
							BgL_tmpz00_5081 = BgL_kz00_3332;
						}
					else
						{
							obj_t BgL_auxz00_5084;

							BgL_auxz00_5084 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(29692L), BGl_string2432z00zz__srfi4z00,
								BGl_string2390z00zz__srfi4z00, BgL_kz00_3332);
							FAILURE(BgL_auxz00_5084, BFALSE, BFALSE);
						}
					BgL_kz00_3879 = (long) CINT(BgL_tmpz00_5081);
				}
				{	/* Llib/srfi4.scm 748 */
					obj_t BgL_tmpz00_5089;

					if (REALP(BgL_valz00_3333))
						{	/* Llib/srfi4.scm 748 */
							BgL_tmpz00_5089 = BgL_valz00_3333;
						}
					else
						{
							obj_t BgL_auxz00_5092;

							BgL_auxz00_5092 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
								BINT(29692L), BGl_string2432z00zz__srfi4z00,
								BGl_string2407z00zz__srfi4z00, BgL_valz00_3333);
							FAILURE(BgL_auxz00_5092, BFALSE, BFALSE);
						}
					BgL_valz00_3880 = REAL_TO_DOUBLE(BgL_tmpz00_5089);
				}
				BGL_F64VSET(BgL_vz00_3878, BgL_kz00_3879, BgL_valz00_3880);
				return BUNSPEC;
			}
		}

	}



/* s8vector->list */
	BGL_EXPORTED_DEF obj_t BGl_s8vectorzd2ze3listz31zz__srfi4z00(obj_t
		BgL_xz00_133)
	{
		{	/* Llib/srfi4.scm 771 */
			{	/* Llib/srfi4.scm 771 */
				long BgL_g1063z00_1521;

				BgL_g1063z00_1521 = BGL_HVECTOR_LENGTH(BgL_xz00_133);
				{
					long BgL_iz00_2430;
					obj_t BgL_resz00_2431;

					BgL_iz00_2430 = BgL_g1063z00_1521;
					BgL_resz00_2431 = BNIL;
				BgL_loopz00_2429:
					if ((BgL_iz00_2430 == 0L))
						{	/* Llib/srfi4.scm 771 */
							return BgL_resz00_2431;
						}
					else
						{	/* Llib/srfi4.scm 771 */
							long BgL_niz00_2437;

							BgL_niz00_2437 = (BgL_iz00_2430 - 1L);
							{	/* Llib/srfi4.scm 771 */
								obj_t BgL_arg1428z00_2438;

								{	/* Llib/srfi4.scm 771 */
									int8_t BgL_arg1429z00_2439;

									BgL_arg1429z00_2439 =
										BGL_S8VREF(BgL_xz00_133, BgL_niz00_2437);
									BgL_arg1428z00_2438 =
										MAKE_YOUNG_PAIR(BGL_INT8_TO_BINT8(BgL_arg1429z00_2439),
										BgL_resz00_2431);
								}
								{
									obj_t BgL_resz00_5106;
									long BgL_iz00_5105;

									BgL_iz00_5105 = BgL_niz00_2437;
									BgL_resz00_5106 = BgL_arg1428z00_2438;
									BgL_resz00_2431 = BgL_resz00_5106;
									BgL_iz00_2430 = BgL_iz00_5105;
									goto BgL_loopz00_2429;
								}
							}
						}
				}
			}
		}

	}



/* &s8vector->list */
	obj_t BGl_z62s8vectorzd2ze3listz53zz__srfi4z00(obj_t BgL_envz00_3378,
		obj_t BgL_xz00_3379)
	{
		{	/* Llib/srfi4.scm 771 */
			{	/* Llib/srfi4.scm 771 */
				obj_t BgL_auxz00_5107;

				if (BGL_S8VECTORP(BgL_xz00_3379))
					{	/* Llib/srfi4.scm 771 */
						BgL_auxz00_5107 = BgL_xz00_3379;
					}
				else
					{
						obj_t BgL_auxz00_5110;

						BgL_auxz00_5110 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
							BINT(30505L), BGl_string2433z00zz__srfi4z00,
							BGl_string2370z00zz__srfi4z00, BgL_xz00_3379);
						FAILURE(BgL_auxz00_5110, BFALSE, BFALSE);
					}
				return BGl_s8vectorzd2ze3listz31zz__srfi4z00(BgL_auxz00_5107);
			}
		}

	}



/* u8vector->list */
	BGL_EXPORTED_DEF obj_t BGl_u8vectorzd2ze3listz31zz__srfi4z00(obj_t
		BgL_xz00_134)
	{
		{	/* Llib/srfi4.scm 771 */
			{	/* Llib/srfi4.scm 771 */
				long BgL_g1065z00_1532;

				BgL_g1065z00_1532 = BGL_HVECTOR_LENGTH(BgL_xz00_134);
				{
					long BgL_iz00_2455;
					obj_t BgL_resz00_2456;

					BgL_iz00_2455 = BgL_g1065z00_1532;
					BgL_resz00_2456 = BNIL;
				BgL_loopz00_2454:
					if ((BgL_iz00_2455 == 0L))
						{	/* Llib/srfi4.scm 771 */
							return BgL_resz00_2456;
						}
					else
						{	/* Llib/srfi4.scm 771 */
							long BgL_niz00_2462;

							BgL_niz00_2462 = (BgL_iz00_2455 - 1L);
							{	/* Llib/srfi4.scm 771 */
								obj_t BgL_arg1434z00_2463;

								{	/* Llib/srfi4.scm 771 */
									uint8_t BgL_arg1435z00_2464;

									BgL_arg1435z00_2464 =
										BGL_U8VREF(BgL_xz00_134, BgL_niz00_2462);
									BgL_arg1434z00_2463 =
										MAKE_YOUNG_PAIR(BGL_UINT8_TO_BUINT8(BgL_arg1435z00_2464),
										BgL_resz00_2456);
								}
								{
									obj_t BgL_resz00_5123;
									long BgL_iz00_5122;

									BgL_iz00_5122 = BgL_niz00_2462;
									BgL_resz00_5123 = BgL_arg1434z00_2463;
									BgL_resz00_2456 = BgL_resz00_5123;
									BgL_iz00_2455 = BgL_iz00_5122;
									goto BgL_loopz00_2454;
								}
							}
						}
				}
			}
		}

	}



/* &u8vector->list */
	obj_t BGl_z62u8vectorzd2ze3listz53zz__srfi4z00(obj_t BgL_envz00_3380,
		obj_t BgL_xz00_3381)
	{
		{	/* Llib/srfi4.scm 771 */
			{	/* Llib/srfi4.scm 771 */
				obj_t BgL_auxz00_5124;

				if (BGL_U8VECTORP(BgL_xz00_3381))
					{	/* Llib/srfi4.scm 771 */
						BgL_auxz00_5124 = BgL_xz00_3381;
					}
				else
					{
						obj_t BgL_auxz00_5127;

						BgL_auxz00_5127 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
							BINT(30505L), BGl_string2434z00zz__srfi4z00,
							BGl_string2372z00zz__srfi4z00, BgL_xz00_3381);
						FAILURE(BgL_auxz00_5127, BFALSE, BFALSE);
					}
				return BGl_u8vectorzd2ze3listz31zz__srfi4z00(BgL_auxz00_5124);
			}
		}

	}



/* s16vector->list */
	BGL_EXPORTED_DEF obj_t BGl_s16vectorzd2ze3listz31zz__srfi4z00(obj_t
		BgL_xz00_135)
	{
		{	/* Llib/srfi4.scm 771 */
			{	/* Llib/srfi4.scm 771 */
				long BgL_g1067z00_1543;

				BgL_g1067z00_1543 = BGL_HVECTOR_LENGTH(BgL_xz00_135);
				{
					long BgL_iz00_2480;
					obj_t BgL_resz00_2481;

					BgL_iz00_2480 = BgL_g1067z00_1543;
					BgL_resz00_2481 = BNIL;
				BgL_loopz00_2479:
					if ((BgL_iz00_2480 == 0L))
						{	/* Llib/srfi4.scm 771 */
							return BgL_resz00_2481;
						}
					else
						{	/* Llib/srfi4.scm 771 */
							long BgL_niz00_2487;

							BgL_niz00_2487 = (BgL_iz00_2480 - 1L);
							{	/* Llib/srfi4.scm 771 */
								obj_t BgL_arg1438z00_2488;

								{	/* Llib/srfi4.scm 771 */
									int16_t BgL_arg1439z00_2489;

									BgL_arg1439z00_2489 =
										BGL_S16VREF(BgL_xz00_135, BgL_niz00_2487);
									BgL_arg1438z00_2488 =
										MAKE_YOUNG_PAIR(BGL_INT16_TO_BINT16(BgL_arg1439z00_2489),
										BgL_resz00_2481);
								}
								{
									obj_t BgL_resz00_5140;
									long BgL_iz00_5139;

									BgL_iz00_5139 = BgL_niz00_2487;
									BgL_resz00_5140 = BgL_arg1438z00_2488;
									BgL_resz00_2481 = BgL_resz00_5140;
									BgL_iz00_2480 = BgL_iz00_5139;
									goto BgL_loopz00_2479;
								}
							}
						}
				}
			}
		}

	}



/* &s16vector->list */
	obj_t BGl_z62s16vectorzd2ze3listz53zz__srfi4z00(obj_t BgL_envz00_3382,
		obj_t BgL_xz00_3383)
	{
		{	/* Llib/srfi4.scm 771 */
			{	/* Llib/srfi4.scm 771 */
				obj_t BgL_auxz00_5141;

				if (BGL_S16VECTORP(BgL_xz00_3383))
					{	/* Llib/srfi4.scm 771 */
						BgL_auxz00_5141 = BgL_xz00_3383;
					}
				else
					{
						obj_t BgL_auxz00_5144;

						BgL_auxz00_5144 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
							BINT(30505L), BGl_string2435z00zz__srfi4z00,
							BGl_string2374z00zz__srfi4z00, BgL_xz00_3383);
						FAILURE(BgL_auxz00_5144, BFALSE, BFALSE);
					}
				return BGl_s16vectorzd2ze3listz31zz__srfi4z00(BgL_auxz00_5141);
			}
		}

	}



/* u16vector->list */
	BGL_EXPORTED_DEF obj_t BGl_u16vectorzd2ze3listz31zz__srfi4z00(obj_t
		BgL_xz00_136)
	{
		{	/* Llib/srfi4.scm 771 */
			{	/* Llib/srfi4.scm 771 */
				long BgL_g1069z00_1554;

				BgL_g1069z00_1554 = BGL_HVECTOR_LENGTH(BgL_xz00_136);
				{
					long BgL_iz00_2505;
					obj_t BgL_resz00_2506;

					BgL_iz00_2505 = BgL_g1069z00_1554;
					BgL_resz00_2506 = BNIL;
				BgL_loopz00_2504:
					if ((BgL_iz00_2505 == 0L))
						{	/* Llib/srfi4.scm 771 */
							return BgL_resz00_2506;
						}
					else
						{	/* Llib/srfi4.scm 771 */
							long BgL_niz00_2512;

							BgL_niz00_2512 = (BgL_iz00_2505 - 1L);
							{	/* Llib/srfi4.scm 771 */
								obj_t BgL_arg1442z00_2513;

								{	/* Llib/srfi4.scm 771 */
									uint16_t BgL_arg1443z00_2514;

									BgL_arg1443z00_2514 =
										BGL_U16VREF(BgL_xz00_136, BgL_niz00_2512);
									BgL_arg1442z00_2513 =
										MAKE_YOUNG_PAIR(BGL_UINT16_TO_BUINT16(BgL_arg1443z00_2514),
										BgL_resz00_2506);
								}
								{
									obj_t BgL_resz00_5157;
									long BgL_iz00_5156;

									BgL_iz00_5156 = BgL_niz00_2512;
									BgL_resz00_5157 = BgL_arg1442z00_2513;
									BgL_resz00_2506 = BgL_resz00_5157;
									BgL_iz00_2505 = BgL_iz00_5156;
									goto BgL_loopz00_2504;
								}
							}
						}
				}
			}
		}

	}



/* &u16vector->list */
	obj_t BGl_z62u16vectorzd2ze3listz53zz__srfi4z00(obj_t BgL_envz00_3384,
		obj_t BgL_xz00_3385)
	{
		{	/* Llib/srfi4.scm 771 */
			{	/* Llib/srfi4.scm 771 */
				obj_t BgL_auxz00_5158;

				if (BGL_U16VECTORP(BgL_xz00_3385))
					{	/* Llib/srfi4.scm 771 */
						BgL_auxz00_5158 = BgL_xz00_3385;
					}
				else
					{
						obj_t BgL_auxz00_5161;

						BgL_auxz00_5161 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
							BINT(30505L), BGl_string2436z00zz__srfi4z00,
							BGl_string2376z00zz__srfi4z00, BgL_xz00_3385);
						FAILURE(BgL_auxz00_5161, BFALSE, BFALSE);
					}
				return BGl_u16vectorzd2ze3listz31zz__srfi4z00(BgL_auxz00_5158);
			}
		}

	}



/* s32vector->list */
	BGL_EXPORTED_DEF obj_t BGl_s32vectorzd2ze3listz31zz__srfi4z00(obj_t
		BgL_xz00_137)
	{
		{	/* Llib/srfi4.scm 771 */
			{	/* Llib/srfi4.scm 771 */
				long BgL_g1071z00_1565;

				BgL_g1071z00_1565 = BGL_HVECTOR_LENGTH(BgL_xz00_137);
				{
					long BgL_iz00_2530;
					obj_t BgL_resz00_2531;

					BgL_iz00_2530 = BgL_g1071z00_1565;
					BgL_resz00_2531 = BNIL;
				BgL_loopz00_2529:
					if ((BgL_iz00_2530 == 0L))
						{	/* Llib/srfi4.scm 771 */
							return BgL_resz00_2531;
						}
					else
						{	/* Llib/srfi4.scm 771 */
							long BgL_niz00_2537;

							BgL_niz00_2537 = (BgL_iz00_2530 - 1L);
							{	/* Llib/srfi4.scm 771 */
								obj_t BgL_arg1446z00_2538;

								{	/* Llib/srfi4.scm 771 */
									int32_t BgL_arg1447z00_2539;

									BgL_arg1447z00_2539 =
										BGL_S32VREF(BgL_xz00_137, BgL_niz00_2537);
									BgL_arg1446z00_2538 =
										MAKE_YOUNG_PAIR(BGL_INT32_TO_BINT32(BgL_arg1447z00_2539),
										BgL_resz00_2531);
								}
								{
									obj_t BgL_resz00_5174;
									long BgL_iz00_5173;

									BgL_iz00_5173 = BgL_niz00_2537;
									BgL_resz00_5174 = BgL_arg1446z00_2538;
									BgL_resz00_2531 = BgL_resz00_5174;
									BgL_iz00_2530 = BgL_iz00_5173;
									goto BgL_loopz00_2529;
								}
							}
						}
				}
			}
		}

	}



/* &s32vector->list */
	obj_t BGl_z62s32vectorzd2ze3listz53zz__srfi4z00(obj_t BgL_envz00_3386,
		obj_t BgL_xz00_3387)
	{
		{	/* Llib/srfi4.scm 771 */
			{	/* Llib/srfi4.scm 771 */
				obj_t BgL_auxz00_5175;

				if (BGL_S32VECTORP(BgL_xz00_3387))
					{	/* Llib/srfi4.scm 771 */
						BgL_auxz00_5175 = BgL_xz00_3387;
					}
				else
					{
						obj_t BgL_auxz00_5178;

						BgL_auxz00_5178 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
							BINT(30505L), BGl_string2437z00zz__srfi4z00,
							BGl_string2378z00zz__srfi4z00, BgL_xz00_3387);
						FAILURE(BgL_auxz00_5178, BFALSE, BFALSE);
					}
				return BGl_s32vectorzd2ze3listz31zz__srfi4z00(BgL_auxz00_5175);
			}
		}

	}



/* u32vector->list */
	BGL_EXPORTED_DEF obj_t BGl_u32vectorzd2ze3listz31zz__srfi4z00(obj_t
		BgL_xz00_138)
	{
		{	/* Llib/srfi4.scm 771 */
			{	/* Llib/srfi4.scm 771 */
				long BgL_g1073z00_1576;

				BgL_g1073z00_1576 = BGL_HVECTOR_LENGTH(BgL_xz00_138);
				{
					long BgL_iz00_2555;
					obj_t BgL_resz00_2556;

					BgL_iz00_2555 = BgL_g1073z00_1576;
					BgL_resz00_2556 = BNIL;
				BgL_loopz00_2554:
					if ((BgL_iz00_2555 == 0L))
						{	/* Llib/srfi4.scm 771 */
							return BgL_resz00_2556;
						}
					else
						{	/* Llib/srfi4.scm 771 */
							long BgL_niz00_2562;

							BgL_niz00_2562 = (BgL_iz00_2555 - 1L);
							{	/* Llib/srfi4.scm 771 */
								obj_t BgL_arg1450z00_2563;

								{	/* Llib/srfi4.scm 771 */
									uint32_t BgL_arg1451z00_2564;

									BgL_arg1451z00_2564 =
										BGL_U32VREF(BgL_xz00_138, BgL_niz00_2562);
									BgL_arg1450z00_2563 =
										MAKE_YOUNG_PAIR(BGL_UINT32_TO_BUINT32(BgL_arg1451z00_2564),
										BgL_resz00_2556);
								}
								{
									obj_t BgL_resz00_5191;
									long BgL_iz00_5190;

									BgL_iz00_5190 = BgL_niz00_2562;
									BgL_resz00_5191 = BgL_arg1450z00_2563;
									BgL_resz00_2556 = BgL_resz00_5191;
									BgL_iz00_2555 = BgL_iz00_5190;
									goto BgL_loopz00_2554;
								}
							}
						}
				}
			}
		}

	}



/* &u32vector->list */
	obj_t BGl_z62u32vectorzd2ze3listz53zz__srfi4z00(obj_t BgL_envz00_3388,
		obj_t BgL_xz00_3389)
	{
		{	/* Llib/srfi4.scm 771 */
			{	/* Llib/srfi4.scm 771 */
				obj_t BgL_auxz00_5192;

				if (BGL_U32VECTORP(BgL_xz00_3389))
					{	/* Llib/srfi4.scm 771 */
						BgL_auxz00_5192 = BgL_xz00_3389;
					}
				else
					{
						obj_t BgL_auxz00_5195;

						BgL_auxz00_5195 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
							BINT(30505L), BGl_string2438z00zz__srfi4z00,
							BGl_string2380z00zz__srfi4z00, BgL_xz00_3389);
						FAILURE(BgL_auxz00_5195, BFALSE, BFALSE);
					}
				return BGl_u32vectorzd2ze3listz31zz__srfi4z00(BgL_auxz00_5192);
			}
		}

	}



/* s64vector->list */
	BGL_EXPORTED_DEF obj_t BGl_s64vectorzd2ze3listz31zz__srfi4z00(obj_t
		BgL_xz00_139)
	{
		{	/* Llib/srfi4.scm 771 */
			{	/* Llib/srfi4.scm 771 */
				long BgL_g1075z00_1587;

				BgL_g1075z00_1587 = BGL_HVECTOR_LENGTH(BgL_xz00_139);
				{
					long BgL_iz00_2580;
					obj_t BgL_resz00_2581;

					BgL_iz00_2580 = BgL_g1075z00_1587;
					BgL_resz00_2581 = BNIL;
				BgL_loopz00_2579:
					if ((BgL_iz00_2580 == 0L))
						{	/* Llib/srfi4.scm 771 */
							return BgL_resz00_2581;
						}
					else
						{	/* Llib/srfi4.scm 771 */
							long BgL_niz00_2587;

							BgL_niz00_2587 = (BgL_iz00_2580 - 1L);
							{	/* Llib/srfi4.scm 771 */
								obj_t BgL_arg1454z00_2588;

								{	/* Llib/srfi4.scm 771 */
									int64_t BgL_arg1455z00_2589;

									BgL_arg1455z00_2589 =
										BGL_S64VREF(BgL_xz00_139, BgL_niz00_2587);
									BgL_arg1454z00_2588 =
										MAKE_YOUNG_PAIR(BGL_INT64_TO_BINT64(BgL_arg1455z00_2589),
										BgL_resz00_2581);
								}
								{
									obj_t BgL_resz00_5208;
									long BgL_iz00_5207;

									BgL_iz00_5207 = BgL_niz00_2587;
									BgL_resz00_5208 = BgL_arg1454z00_2588;
									BgL_resz00_2581 = BgL_resz00_5208;
									BgL_iz00_2580 = BgL_iz00_5207;
									goto BgL_loopz00_2579;
								}
							}
						}
				}
			}
		}

	}



/* &s64vector->list */
	obj_t BGl_z62s64vectorzd2ze3listz53zz__srfi4z00(obj_t BgL_envz00_3390,
		obj_t BgL_xz00_3391)
	{
		{	/* Llib/srfi4.scm 771 */
			{	/* Llib/srfi4.scm 771 */
				obj_t BgL_auxz00_5209;

				if (BGL_S64VECTORP(BgL_xz00_3391))
					{	/* Llib/srfi4.scm 771 */
						BgL_auxz00_5209 = BgL_xz00_3391;
					}
				else
					{
						obj_t BgL_auxz00_5212;

						BgL_auxz00_5212 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
							BINT(30505L), BGl_string2439z00zz__srfi4z00,
							BGl_string2382z00zz__srfi4z00, BgL_xz00_3391);
						FAILURE(BgL_auxz00_5212, BFALSE, BFALSE);
					}
				return BGl_s64vectorzd2ze3listz31zz__srfi4z00(BgL_auxz00_5209);
			}
		}

	}



/* u64vector->list */
	BGL_EXPORTED_DEF obj_t BGl_u64vectorzd2ze3listz31zz__srfi4z00(obj_t
		BgL_xz00_140)
	{
		{	/* Llib/srfi4.scm 771 */
			{	/* Llib/srfi4.scm 771 */
				long BgL_g1078z00_1598;

				BgL_g1078z00_1598 = BGL_HVECTOR_LENGTH(BgL_xz00_140);
				{
					long BgL_iz00_2605;
					obj_t BgL_resz00_2606;

					BgL_iz00_2605 = BgL_g1078z00_1598;
					BgL_resz00_2606 = BNIL;
				BgL_loopz00_2604:
					if ((BgL_iz00_2605 == 0L))
						{	/* Llib/srfi4.scm 771 */
							return BgL_resz00_2606;
						}
					else
						{	/* Llib/srfi4.scm 771 */
							long BgL_niz00_2612;

							BgL_niz00_2612 = (BgL_iz00_2605 - 1L);
							{	/* Llib/srfi4.scm 771 */
								obj_t BgL_arg1458z00_2613;

								{	/* Llib/srfi4.scm 771 */
									uint64_t BgL_arg1459z00_2614;

									BgL_arg1459z00_2614 =
										BGL_U64VREF(BgL_xz00_140, BgL_niz00_2612);
									BgL_arg1458z00_2613 =
										MAKE_YOUNG_PAIR(BGL_UINT64_TO_BUINT64(BgL_arg1459z00_2614),
										BgL_resz00_2606);
								}
								{
									obj_t BgL_resz00_5225;
									long BgL_iz00_5224;

									BgL_iz00_5224 = BgL_niz00_2612;
									BgL_resz00_5225 = BgL_arg1458z00_2613;
									BgL_resz00_2606 = BgL_resz00_5225;
									BgL_iz00_2605 = BgL_iz00_5224;
									goto BgL_loopz00_2604;
								}
							}
						}
				}
			}
		}

	}



/* &u64vector->list */
	obj_t BGl_z62u64vectorzd2ze3listz53zz__srfi4z00(obj_t BgL_envz00_3392,
		obj_t BgL_xz00_3393)
	{
		{	/* Llib/srfi4.scm 771 */
			{	/* Llib/srfi4.scm 771 */
				obj_t BgL_auxz00_5226;

				if (BGL_U64VECTORP(BgL_xz00_3393))
					{	/* Llib/srfi4.scm 771 */
						BgL_auxz00_5226 = BgL_xz00_3393;
					}
				else
					{
						obj_t BgL_auxz00_5229;

						BgL_auxz00_5229 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
							BINT(30505L), BGl_string2440z00zz__srfi4z00,
							BGl_string2384z00zz__srfi4z00, BgL_xz00_3393);
						FAILURE(BgL_auxz00_5229, BFALSE, BFALSE);
					}
				return BGl_u64vectorzd2ze3listz31zz__srfi4z00(BgL_auxz00_5226);
			}
		}

	}



/* f32vector->list */
	BGL_EXPORTED_DEF obj_t BGl_f32vectorzd2ze3listz31zz__srfi4z00(obj_t
		BgL_xz00_141)
	{
		{	/* Llib/srfi4.scm 771 */
			{	/* Llib/srfi4.scm 771 */
				long BgL_g1080z00_1609;

				BgL_g1080z00_1609 = BGL_HVECTOR_LENGTH(BgL_xz00_141);
				{
					long BgL_iz00_2630;
					obj_t BgL_resz00_2631;

					BgL_iz00_2630 = BgL_g1080z00_1609;
					BgL_resz00_2631 = BNIL;
				BgL_loopz00_2629:
					if ((BgL_iz00_2630 == 0L))
						{	/* Llib/srfi4.scm 771 */
							return BgL_resz00_2631;
						}
					else
						{	/* Llib/srfi4.scm 771 */
							long BgL_niz00_2637;

							BgL_niz00_2637 = (BgL_iz00_2630 - 1L);
							{	/* Llib/srfi4.scm 771 */
								obj_t BgL_arg1462z00_2638;

								{	/* Llib/srfi4.scm 771 */
									float BgL_arg1463z00_2639;

									BgL_arg1463z00_2639 =
										BGL_F32VREF(BgL_xz00_141, BgL_niz00_2637);
									BgL_arg1462z00_2638 =
										MAKE_YOUNG_PAIR(FLOAT_TO_REAL(BgL_arg1463z00_2639),
										BgL_resz00_2631);
								}
								{
									obj_t BgL_resz00_5242;
									long BgL_iz00_5241;

									BgL_iz00_5241 = BgL_niz00_2637;
									BgL_resz00_5242 = BgL_arg1462z00_2638;
									BgL_resz00_2631 = BgL_resz00_5242;
									BgL_iz00_2630 = BgL_iz00_5241;
									goto BgL_loopz00_2629;
								}
							}
						}
				}
			}
		}

	}



/* &f32vector->list */
	obj_t BGl_z62f32vectorzd2ze3listz53zz__srfi4z00(obj_t BgL_envz00_3394,
		obj_t BgL_xz00_3395)
	{
		{	/* Llib/srfi4.scm 771 */
			{	/* Llib/srfi4.scm 771 */
				obj_t BgL_auxz00_5243;

				if (BGL_F32VECTORP(BgL_xz00_3395))
					{	/* Llib/srfi4.scm 771 */
						BgL_auxz00_5243 = BgL_xz00_3395;
					}
				else
					{
						obj_t BgL_auxz00_5246;

						BgL_auxz00_5246 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
							BINT(30505L), BGl_string2441z00zz__srfi4z00,
							BGl_string2386z00zz__srfi4z00, BgL_xz00_3395);
						FAILURE(BgL_auxz00_5246, BFALSE, BFALSE);
					}
				return BGl_f32vectorzd2ze3listz31zz__srfi4z00(BgL_auxz00_5243);
			}
		}

	}



/* f64vector->list */
	BGL_EXPORTED_DEF obj_t BGl_f64vectorzd2ze3listz31zz__srfi4z00(obj_t
		BgL_xz00_142)
	{
		{	/* Llib/srfi4.scm 771 */
			{	/* Llib/srfi4.scm 771 */
				long BgL_g1082z00_1620;

				BgL_g1082z00_1620 = BGL_HVECTOR_LENGTH(BgL_xz00_142);
				{
					long BgL_iz00_2655;
					obj_t BgL_resz00_2656;

					BgL_iz00_2655 = BgL_g1082z00_1620;
					BgL_resz00_2656 = BNIL;
				BgL_loopz00_2654:
					if ((BgL_iz00_2655 == 0L))
						{	/* Llib/srfi4.scm 771 */
							return BgL_resz00_2656;
						}
					else
						{	/* Llib/srfi4.scm 771 */
							long BgL_niz00_2662;

							BgL_niz00_2662 = (BgL_iz00_2655 - 1L);
							{	/* Llib/srfi4.scm 771 */
								obj_t BgL_arg1466z00_2663;

								{	/* Llib/srfi4.scm 771 */
									double BgL_arg1467z00_2664;

									BgL_arg1467z00_2664 =
										BGL_F64VREF(BgL_xz00_142, BgL_niz00_2662);
									BgL_arg1466z00_2663 =
										MAKE_YOUNG_PAIR(DOUBLE_TO_REAL(BgL_arg1467z00_2664),
										BgL_resz00_2656);
								}
								{
									obj_t BgL_resz00_5259;
									long BgL_iz00_5258;

									BgL_iz00_5258 = BgL_niz00_2662;
									BgL_resz00_5259 = BgL_arg1466z00_2663;
									BgL_resz00_2656 = BgL_resz00_5259;
									BgL_iz00_2655 = BgL_iz00_5258;
									goto BgL_loopz00_2654;
								}
							}
						}
				}
			}
		}

	}



/* &f64vector->list */
	obj_t BGl_z62f64vectorzd2ze3listz53zz__srfi4z00(obj_t BgL_envz00_3396,
		obj_t BgL_xz00_3397)
	{
		{	/* Llib/srfi4.scm 771 */
			{	/* Llib/srfi4.scm 771 */
				obj_t BgL_auxz00_5260;

				if (BGL_F64VECTORP(BgL_xz00_3397))
					{	/* Llib/srfi4.scm 771 */
						BgL_auxz00_5260 = BgL_xz00_3397;
					}
				else
					{
						obj_t BgL_auxz00_5263;

						BgL_auxz00_5263 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
							BINT(30505L), BGl_string2442z00zz__srfi4z00,
							BGl_string2388z00zz__srfi4z00, BgL_xz00_3397);
						FAILURE(BgL_auxz00_5263, BFALSE, BFALSE);
					}
				return BGl_f64vectorzd2ze3listz31zz__srfi4z00(BgL_auxz00_5260);
			}
		}

	}



/* list->s8vector */
	BGL_EXPORTED_DEF obj_t BGl_listzd2ze3s8vectorz31zz__srfi4z00(obj_t
		BgL_xz00_143)
	{
		{	/* Llib/srfi4.scm 796 */
			{	/* Llib/srfi4.scm 796 */
				long BgL_lenz00_1631;

				BgL_lenz00_1631 = bgl_list_length(BgL_xz00_143);
				{	/* Llib/srfi4.scm 796 */
					obj_t BgL_vecz00_1632;

					{	/* Llib/srfi4.scm 796 */
						int32_t BgL_tmpz00_5269;

						BgL_tmpz00_5269 = (int32_t) (BgL_lenz00_1631);
						BgL_vecz00_1632 = BGL_ALLOC_S8VECTOR(BgL_tmpz00_5269);
					}
					{	/* Llib/srfi4.scm 796 */

						{
							long BgL_iz00_2676;
							obj_t BgL_lz00_2677;

							BgL_iz00_2676 = 0L;
							BgL_lz00_2677 = BgL_xz00_143;
						BgL_loopz00_2675:
							if ((BgL_iz00_2676 == BgL_lenz00_1631))
								{	/* Llib/srfi4.scm 796 */
									return BgL_vecz00_1632;
								}
							else
								{	/* Llib/srfi4.scm 796 */
									{	/* Llib/srfi4.scm 796 */
										obj_t BgL_arg1472z00_2681;

										if (INTEGERP(CAR(((obj_t) BgL_lz00_2677))))
											{	/* Llib/srfi4.scm 796 */
												obj_t BgL_arg1476z00_2685;

												BgL_arg1476z00_2685 = CAR(((obj_t) BgL_lz00_2677));
												{	/* Llib/srfi4.scm 796 */
													long BgL_xz00_2687;

													BgL_xz00_2687 = (long) CINT(BgL_arg1476z00_2685);
													{	/* Llib/srfi4.scm 796 */
														int8_t BgL_tmpz00_5281;

														BgL_tmpz00_5281 = (int8_t) (BgL_xz00_2687);
														BgL_arg1472z00_2681 =
															BGL_INT8_TO_BINT8(BgL_tmpz00_5281);
											}}}
										else
											{	/* Llib/srfi4.scm 796 */
												BgL_arg1472z00_2681 = CAR(((obj_t) BgL_lz00_2677));
											}
										{	/* Llib/srfi4.scm 796 */
											int8_t BgL_tmpz00_5286;

											BgL_tmpz00_5286 = BGL_BINT8_TO_INT8(BgL_arg1472z00_2681);
											BGL_S8VSET(BgL_vecz00_1632, BgL_iz00_2676,
												BgL_tmpz00_5286);
										}
										BUNSPEC;
									}
									{	/* Llib/srfi4.scm 796 */
										long BgL_arg1478z00_2689;
										obj_t BgL_arg1479z00_2690;

										BgL_arg1478z00_2689 = (BgL_iz00_2676 + 1L);
										BgL_arg1479z00_2690 = CDR(((obj_t) BgL_lz00_2677));
										{
											obj_t BgL_lz00_5293;
											long BgL_iz00_5292;

											BgL_iz00_5292 = BgL_arg1478z00_2689;
											BgL_lz00_5293 = BgL_arg1479z00_2690;
											BgL_lz00_2677 = BgL_lz00_5293;
											BgL_iz00_2676 = BgL_iz00_5292;
											goto BgL_loopz00_2675;
										}
									}
								}
						}
					}
				}
			}
		}

	}



/* &list->s8vector */
	obj_t BGl_z62listzd2ze3s8vectorz53zz__srfi4z00(obj_t BgL_envz00_3398,
		obj_t BgL_xz00_3399)
	{
		{	/* Llib/srfi4.scm 796 */
			{	/* Llib/srfi4.scm 796 */
				obj_t BgL_auxz00_5294;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_xz00_3399))
					{	/* Llib/srfi4.scm 796 */
						BgL_auxz00_5294 = BgL_xz00_3399;
					}
				else
					{
						obj_t BgL_auxz00_5297;

						BgL_auxz00_5297 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
							BINT(31419L), BGl_string2443z00zz__srfi4z00,
							BGl_string2444z00zz__srfi4z00, BgL_xz00_3399);
						FAILURE(BgL_auxz00_5297, BFALSE, BFALSE);
					}
				return BGl_listzd2ze3s8vectorz31zz__srfi4z00(BgL_auxz00_5294);
			}
		}

	}



/* list->u8vector */
	BGL_EXPORTED_DEF obj_t BGl_listzd2ze3u8vectorz31zz__srfi4z00(obj_t
		BgL_xz00_144)
	{
		{	/* Llib/srfi4.scm 796 */
			{	/* Llib/srfi4.scm 796 */
				long BgL_lenz00_1646;

				BgL_lenz00_1646 = bgl_list_length(BgL_xz00_144);
				{	/* Llib/srfi4.scm 796 */
					obj_t BgL_vecz00_1647;

					{	/* Llib/srfi4.scm 796 */
						int32_t BgL_tmpz00_5303;

						BgL_tmpz00_5303 = (int32_t) (BgL_lenz00_1646);
						BgL_vecz00_1647 = BGL_ALLOC_U8VECTOR(BgL_tmpz00_5303);
					}
					{	/* Llib/srfi4.scm 796 */

						{
							long BgL_iz00_2702;
							obj_t BgL_lz00_2703;

							BgL_iz00_2702 = 0L;
							BgL_lz00_2703 = BgL_xz00_144;
						BgL_loopz00_2701:
							if ((BgL_iz00_2702 == BgL_lenz00_1646))
								{	/* Llib/srfi4.scm 796 */
									return BgL_vecz00_1647;
								}
							else
								{	/* Llib/srfi4.scm 796 */
									{	/* Llib/srfi4.scm 796 */
										obj_t BgL_arg1482z00_2707;

										if (INTEGERP(CAR(((obj_t) BgL_lz00_2703))))
											{	/* Llib/srfi4.scm 796 */
												obj_t BgL_arg1485z00_2711;

												BgL_arg1485z00_2711 = CAR(((obj_t) BgL_lz00_2703));
												{	/* Llib/srfi4.scm 796 */
													long BgL_xz00_2713;

													BgL_xz00_2713 = (long) CINT(BgL_arg1485z00_2711);
													{	/* Llib/srfi4.scm 796 */
														uint8_t BgL_tmpz00_5315;

														BgL_tmpz00_5315 = (uint8_t) (BgL_xz00_2713);
														BgL_arg1482z00_2707 =
															BGL_UINT8_TO_BUINT8(BgL_tmpz00_5315);
											}}}
										else
											{	/* Llib/srfi4.scm 796 */
												BgL_arg1482z00_2707 = CAR(((obj_t) BgL_lz00_2703));
											}
										{	/* Llib/srfi4.scm 796 */
											uint8_t BgL_tmpz00_5320;

											BgL_tmpz00_5320 =
												BGL_BUINT8_TO_UINT8(BgL_arg1482z00_2707);
											BGL_U8VSET(BgL_vecz00_1647, BgL_iz00_2702,
												BgL_tmpz00_5320);
										}
										BUNSPEC;
									}
									{	/* Llib/srfi4.scm 796 */
										long BgL_arg1487z00_2715;
										obj_t BgL_arg1488z00_2716;

										BgL_arg1487z00_2715 = (BgL_iz00_2702 + 1L);
										BgL_arg1488z00_2716 = CDR(((obj_t) BgL_lz00_2703));
										{
											obj_t BgL_lz00_5327;
											long BgL_iz00_5326;

											BgL_iz00_5326 = BgL_arg1487z00_2715;
											BgL_lz00_5327 = BgL_arg1488z00_2716;
											BgL_lz00_2703 = BgL_lz00_5327;
											BgL_iz00_2702 = BgL_iz00_5326;
											goto BgL_loopz00_2701;
										}
									}
								}
						}
					}
				}
			}
		}

	}



/* &list->u8vector */
	obj_t BGl_z62listzd2ze3u8vectorz53zz__srfi4z00(obj_t BgL_envz00_3400,
		obj_t BgL_xz00_3401)
	{
		{	/* Llib/srfi4.scm 796 */
			{	/* Llib/srfi4.scm 796 */
				obj_t BgL_auxz00_5328;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_xz00_3401))
					{	/* Llib/srfi4.scm 796 */
						BgL_auxz00_5328 = BgL_xz00_3401;
					}
				else
					{
						obj_t BgL_auxz00_5331;

						BgL_auxz00_5331 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
							BINT(31419L), BGl_string2445z00zz__srfi4z00,
							BGl_string2444z00zz__srfi4z00, BgL_xz00_3401);
						FAILURE(BgL_auxz00_5331, BFALSE, BFALSE);
					}
				return BGl_listzd2ze3u8vectorz31zz__srfi4z00(BgL_auxz00_5328);
			}
		}

	}



/* list->s16vector */
	BGL_EXPORTED_DEF obj_t BGl_listzd2ze3s16vectorz31zz__srfi4z00(obj_t
		BgL_xz00_145)
	{
		{	/* Llib/srfi4.scm 796 */
			{	/* Llib/srfi4.scm 796 */
				long BgL_lenz00_1661;

				BgL_lenz00_1661 = bgl_list_length(BgL_xz00_145);
				{	/* Llib/srfi4.scm 796 */
					obj_t BgL_vecz00_1662;

					{	/* Llib/srfi4.scm 796 */
						int32_t BgL_tmpz00_5337;

						BgL_tmpz00_5337 = (int32_t) (BgL_lenz00_1661);
						BgL_vecz00_1662 = BGL_ALLOC_S16VECTOR(BgL_tmpz00_5337);
					}
					{	/* Llib/srfi4.scm 796 */

						{
							long BgL_iz00_2728;
							obj_t BgL_lz00_2729;

							BgL_iz00_2728 = 0L;
							BgL_lz00_2729 = BgL_xz00_145;
						BgL_loopz00_2727:
							if ((BgL_iz00_2728 == BgL_lenz00_1661))
								{	/* Llib/srfi4.scm 796 */
									return BgL_vecz00_1662;
								}
							else
								{	/* Llib/srfi4.scm 796 */
									{	/* Llib/srfi4.scm 796 */
										obj_t BgL_arg1492z00_2733;

										if (INTEGERP(CAR(((obj_t) BgL_lz00_2729))))
											{	/* Llib/srfi4.scm 796 */
												obj_t BgL_arg1497z00_2737;

												BgL_arg1497z00_2737 = CAR(((obj_t) BgL_lz00_2729));
												{	/* Llib/srfi4.scm 796 */
													long BgL_xz00_2739;

													BgL_xz00_2739 = (long) CINT(BgL_arg1497z00_2737);
													{	/* Llib/srfi4.scm 796 */
														int16_t BgL_tmpz00_5349;

														BgL_tmpz00_5349 = (int16_t) (BgL_xz00_2739);
														BgL_arg1492z00_2733 =
															BGL_INT16_TO_BINT16(BgL_tmpz00_5349);
											}}}
										else
											{	/* Llib/srfi4.scm 796 */
												BgL_arg1492z00_2733 = CAR(((obj_t) BgL_lz00_2729));
											}
										{	/* Llib/srfi4.scm 796 */
											int16_t BgL_tmpz00_5354;

											BgL_tmpz00_5354 =
												BGL_BINT16_TO_INT16(BgL_arg1492z00_2733);
											BGL_S16VSET(BgL_vecz00_1662, BgL_iz00_2728,
												BgL_tmpz00_5354);
										}
										BUNSPEC;
									}
									{	/* Llib/srfi4.scm 796 */
										long BgL_arg1499z00_2741;
										obj_t BgL_arg1500z00_2742;

										BgL_arg1499z00_2741 = (BgL_iz00_2728 + 1L);
										BgL_arg1500z00_2742 = CDR(((obj_t) BgL_lz00_2729));
										{
											obj_t BgL_lz00_5361;
											long BgL_iz00_5360;

											BgL_iz00_5360 = BgL_arg1499z00_2741;
											BgL_lz00_5361 = BgL_arg1500z00_2742;
											BgL_lz00_2729 = BgL_lz00_5361;
											BgL_iz00_2728 = BgL_iz00_5360;
											goto BgL_loopz00_2727;
										}
									}
								}
						}
					}
				}
			}
		}

	}



/* &list->s16vector */
	obj_t BGl_z62listzd2ze3s16vectorz53zz__srfi4z00(obj_t BgL_envz00_3402,
		obj_t BgL_xz00_3403)
	{
		{	/* Llib/srfi4.scm 796 */
			{	/* Llib/srfi4.scm 796 */
				obj_t BgL_auxz00_5362;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_xz00_3403))
					{	/* Llib/srfi4.scm 796 */
						BgL_auxz00_5362 = BgL_xz00_3403;
					}
				else
					{
						obj_t BgL_auxz00_5365;

						BgL_auxz00_5365 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
							BINT(31419L), BGl_string2446z00zz__srfi4z00,
							BGl_string2444z00zz__srfi4z00, BgL_xz00_3403);
						FAILURE(BgL_auxz00_5365, BFALSE, BFALSE);
					}
				return BGl_listzd2ze3s16vectorz31zz__srfi4z00(BgL_auxz00_5362);
			}
		}

	}



/* list->u16vector */
	BGL_EXPORTED_DEF obj_t BGl_listzd2ze3u16vectorz31zz__srfi4z00(obj_t
		BgL_xz00_146)
	{
		{	/* Llib/srfi4.scm 796 */
			{	/* Llib/srfi4.scm 796 */
				long BgL_lenz00_1676;

				BgL_lenz00_1676 = bgl_list_length(BgL_xz00_146);
				{	/* Llib/srfi4.scm 796 */
					obj_t BgL_vecz00_1677;

					{	/* Llib/srfi4.scm 796 */
						int32_t BgL_tmpz00_5371;

						BgL_tmpz00_5371 = (int32_t) (BgL_lenz00_1676);
						BgL_vecz00_1677 = BGL_ALLOC_U16VECTOR(BgL_tmpz00_5371);
					}
					{	/* Llib/srfi4.scm 796 */

						{
							long BgL_iz00_2754;
							obj_t BgL_lz00_2755;

							BgL_iz00_2754 = 0L;
							BgL_lz00_2755 = BgL_xz00_146;
						BgL_loopz00_2753:
							if ((BgL_iz00_2754 == BgL_lenz00_1676))
								{	/* Llib/srfi4.scm 796 */
									return BgL_vecz00_1677;
								}
							else
								{	/* Llib/srfi4.scm 796 */
									{	/* Llib/srfi4.scm 796 */
										obj_t BgL_arg1503z00_2759;

										if (INTEGERP(CAR(((obj_t) BgL_lz00_2755))))
											{	/* Llib/srfi4.scm 796 */
												obj_t BgL_arg1506z00_2763;

												BgL_arg1506z00_2763 = CAR(((obj_t) BgL_lz00_2755));
												{	/* Llib/srfi4.scm 796 */
													long BgL_xz00_2765;

													BgL_xz00_2765 = (long) CINT(BgL_arg1506z00_2763);
													{	/* Llib/srfi4.scm 796 */
														uint16_t BgL_tmpz00_5383;

														BgL_tmpz00_5383 = (uint16_t) (BgL_xz00_2765);
														BgL_arg1503z00_2759 =
															BGL_UINT16_TO_BUINT16(BgL_tmpz00_5383);
											}}}
										else
											{	/* Llib/srfi4.scm 796 */
												BgL_arg1503z00_2759 = CAR(((obj_t) BgL_lz00_2755));
											}
										{	/* Llib/srfi4.scm 796 */
											uint16_t BgL_tmpz00_5388;

											BgL_tmpz00_5388 =
												BGL_BUINT16_TO_UINT16(BgL_arg1503z00_2759);
											BGL_U16VSET(BgL_vecz00_1677, BgL_iz00_2754,
												BgL_tmpz00_5388);
										}
										BUNSPEC;
									}
									{	/* Llib/srfi4.scm 796 */
										long BgL_arg1508z00_2767;
										obj_t BgL_arg1509z00_2768;

										BgL_arg1508z00_2767 = (BgL_iz00_2754 + 1L);
										BgL_arg1509z00_2768 = CDR(((obj_t) BgL_lz00_2755));
										{
											obj_t BgL_lz00_5395;
											long BgL_iz00_5394;

											BgL_iz00_5394 = BgL_arg1508z00_2767;
											BgL_lz00_5395 = BgL_arg1509z00_2768;
											BgL_lz00_2755 = BgL_lz00_5395;
											BgL_iz00_2754 = BgL_iz00_5394;
											goto BgL_loopz00_2753;
										}
									}
								}
						}
					}
				}
			}
		}

	}



/* &list->u16vector */
	obj_t BGl_z62listzd2ze3u16vectorz53zz__srfi4z00(obj_t BgL_envz00_3404,
		obj_t BgL_xz00_3405)
	{
		{	/* Llib/srfi4.scm 796 */
			{	/* Llib/srfi4.scm 796 */
				obj_t BgL_auxz00_5396;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_xz00_3405))
					{	/* Llib/srfi4.scm 796 */
						BgL_auxz00_5396 = BgL_xz00_3405;
					}
				else
					{
						obj_t BgL_auxz00_5399;

						BgL_auxz00_5399 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
							BINT(31419L), BGl_string2447z00zz__srfi4z00,
							BGl_string2444z00zz__srfi4z00, BgL_xz00_3405);
						FAILURE(BgL_auxz00_5399, BFALSE, BFALSE);
					}
				return BGl_listzd2ze3u16vectorz31zz__srfi4z00(BgL_auxz00_5396);
			}
		}

	}



/* list->s32vector */
	BGL_EXPORTED_DEF obj_t BGl_listzd2ze3s32vectorz31zz__srfi4z00(obj_t
		BgL_xz00_147)
	{
		{	/* Llib/srfi4.scm 796 */
			{	/* Llib/srfi4.scm 796 */
				long BgL_lenz00_1691;

				BgL_lenz00_1691 = bgl_list_length(BgL_xz00_147);
				{	/* Llib/srfi4.scm 796 */
					obj_t BgL_vecz00_1692;

					{	/* Llib/srfi4.scm 796 */
						int32_t BgL_tmpz00_5405;

						BgL_tmpz00_5405 = (int32_t) (BgL_lenz00_1691);
						BgL_vecz00_1692 = BGL_ALLOC_S32VECTOR(BgL_tmpz00_5405);
					}
					{	/* Llib/srfi4.scm 796 */

						{
							long BgL_iz00_2780;
							obj_t BgL_lz00_2781;

							BgL_iz00_2780 = 0L;
							BgL_lz00_2781 = BgL_xz00_147;
						BgL_loopz00_2779:
							if ((BgL_iz00_2780 == BgL_lenz00_1691))
								{	/* Llib/srfi4.scm 796 */
									return BgL_vecz00_1692;
								}
							else
								{	/* Llib/srfi4.scm 796 */
									{	/* Llib/srfi4.scm 796 */
										obj_t BgL_arg1513z00_2785;

										if (INTEGERP(CAR(((obj_t) BgL_lz00_2781))))
											{	/* Llib/srfi4.scm 796 */
												obj_t BgL_arg1516z00_2789;

												BgL_arg1516z00_2789 = CAR(((obj_t) BgL_lz00_2781));
												{	/* Llib/srfi4.scm 796 */
													long BgL_xz00_2791;

													BgL_xz00_2791 = (long) CINT(BgL_arg1516z00_2789);
													{	/* Llib/srfi4.scm 796 */
														int32_t BgL_tmpz00_5417;

														BgL_tmpz00_5417 = (int32_t) (BgL_xz00_2791);
														BgL_arg1513z00_2785 =
															BGL_INT32_TO_BINT32(BgL_tmpz00_5417);
											}}}
										else
											{	/* Llib/srfi4.scm 796 */
												BgL_arg1513z00_2785 = CAR(((obj_t) BgL_lz00_2781));
											}
										{	/* Llib/srfi4.scm 796 */
											int32_t BgL_tmpz00_5422;

											BgL_tmpz00_5422 =
												BGL_BINT32_TO_INT32(BgL_arg1513z00_2785);
											BGL_S32VSET(BgL_vecz00_1692, BgL_iz00_2780,
												BgL_tmpz00_5422);
										}
										BUNSPEC;
									}
									{	/* Llib/srfi4.scm 796 */
										long BgL_arg1521z00_2793;
										obj_t BgL_arg1522z00_2794;

										BgL_arg1521z00_2793 = (BgL_iz00_2780 + 1L);
										BgL_arg1522z00_2794 = CDR(((obj_t) BgL_lz00_2781));
										{
											obj_t BgL_lz00_5429;
											long BgL_iz00_5428;

											BgL_iz00_5428 = BgL_arg1521z00_2793;
											BgL_lz00_5429 = BgL_arg1522z00_2794;
											BgL_lz00_2781 = BgL_lz00_5429;
											BgL_iz00_2780 = BgL_iz00_5428;
											goto BgL_loopz00_2779;
										}
									}
								}
						}
					}
				}
			}
		}

	}



/* &list->s32vector */
	obj_t BGl_z62listzd2ze3s32vectorz53zz__srfi4z00(obj_t BgL_envz00_3406,
		obj_t BgL_xz00_3407)
	{
		{	/* Llib/srfi4.scm 796 */
			{	/* Llib/srfi4.scm 796 */
				obj_t BgL_auxz00_5430;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_xz00_3407))
					{	/* Llib/srfi4.scm 796 */
						BgL_auxz00_5430 = BgL_xz00_3407;
					}
				else
					{
						obj_t BgL_auxz00_5433;

						BgL_auxz00_5433 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
							BINT(31419L), BGl_string2448z00zz__srfi4z00,
							BGl_string2444z00zz__srfi4z00, BgL_xz00_3407);
						FAILURE(BgL_auxz00_5433, BFALSE, BFALSE);
					}
				return BGl_listzd2ze3s32vectorz31zz__srfi4z00(BgL_auxz00_5430);
			}
		}

	}



/* list->u32vector */
	BGL_EXPORTED_DEF obj_t BGl_listzd2ze3u32vectorz31zz__srfi4z00(obj_t
		BgL_xz00_148)
	{
		{	/* Llib/srfi4.scm 796 */
			{	/* Llib/srfi4.scm 796 */
				long BgL_lenz00_1706;

				BgL_lenz00_1706 = bgl_list_length(BgL_xz00_148);
				{	/* Llib/srfi4.scm 796 */
					obj_t BgL_vecz00_1707;

					{	/* Llib/srfi4.scm 796 */
						int32_t BgL_tmpz00_5439;

						BgL_tmpz00_5439 = (int32_t) (BgL_lenz00_1706);
						BgL_vecz00_1707 = BGL_ALLOC_U32VECTOR(BgL_tmpz00_5439);
					}
					{	/* Llib/srfi4.scm 796 */

						{
							long BgL_iz00_2806;
							obj_t BgL_lz00_2807;

							BgL_iz00_2806 = 0L;
							BgL_lz00_2807 = BgL_xz00_148;
						BgL_loopz00_2805:
							if ((BgL_iz00_2806 == BgL_lenz00_1706))
								{	/* Llib/srfi4.scm 796 */
									return BgL_vecz00_1707;
								}
							else
								{	/* Llib/srfi4.scm 796 */
									{	/* Llib/srfi4.scm 796 */
										obj_t BgL_arg1525z00_2811;

										if (INTEGERP(CAR(((obj_t) BgL_lz00_2807))))
											{	/* Llib/srfi4.scm 796 */
												obj_t BgL_arg1528z00_2815;

												BgL_arg1528z00_2815 = CAR(((obj_t) BgL_lz00_2807));
												{	/* Llib/srfi4.scm 796 */
													long BgL_xz00_2817;

													BgL_xz00_2817 = (long) CINT(BgL_arg1528z00_2815);
													{	/* Llib/srfi4.scm 796 */
														uint32_t BgL_tmpz00_5451;

														BgL_tmpz00_5451 = (uint32_t) (BgL_xz00_2817);
														BgL_arg1525z00_2811 =
															BGL_UINT32_TO_BUINT32(BgL_tmpz00_5451);
											}}}
										else
											{	/* Llib/srfi4.scm 796 */
												BgL_arg1525z00_2811 = CAR(((obj_t) BgL_lz00_2807));
											}
										{	/* Llib/srfi4.scm 796 */
											uint32_t BgL_tmpz00_5456;

											BgL_tmpz00_5456 =
												BGL_BUINT32_TO_UINT32(BgL_arg1525z00_2811);
											BGL_U32VSET(BgL_vecz00_1707, BgL_iz00_2806,
												BgL_tmpz00_5456);
										}
										BUNSPEC;
									}
									{	/* Llib/srfi4.scm 796 */
										long BgL_arg1530z00_2819;
										obj_t BgL_arg1531z00_2820;

										BgL_arg1530z00_2819 = (BgL_iz00_2806 + 1L);
										BgL_arg1531z00_2820 = CDR(((obj_t) BgL_lz00_2807));
										{
											obj_t BgL_lz00_5463;
											long BgL_iz00_5462;

											BgL_iz00_5462 = BgL_arg1530z00_2819;
											BgL_lz00_5463 = BgL_arg1531z00_2820;
											BgL_lz00_2807 = BgL_lz00_5463;
											BgL_iz00_2806 = BgL_iz00_5462;
											goto BgL_loopz00_2805;
										}
									}
								}
						}
					}
				}
			}
		}

	}



/* &list->u32vector */
	obj_t BGl_z62listzd2ze3u32vectorz53zz__srfi4z00(obj_t BgL_envz00_3408,
		obj_t BgL_xz00_3409)
	{
		{	/* Llib/srfi4.scm 796 */
			{	/* Llib/srfi4.scm 796 */
				obj_t BgL_auxz00_5464;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_xz00_3409))
					{	/* Llib/srfi4.scm 796 */
						BgL_auxz00_5464 = BgL_xz00_3409;
					}
				else
					{
						obj_t BgL_auxz00_5467;

						BgL_auxz00_5467 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
							BINT(31419L), BGl_string2449z00zz__srfi4z00,
							BGl_string2444z00zz__srfi4z00, BgL_xz00_3409);
						FAILURE(BgL_auxz00_5467, BFALSE, BFALSE);
					}
				return BGl_listzd2ze3u32vectorz31zz__srfi4z00(BgL_auxz00_5464);
			}
		}

	}



/* list->s64vector */
	BGL_EXPORTED_DEF obj_t BGl_listzd2ze3s64vectorz31zz__srfi4z00(obj_t
		BgL_xz00_149)
	{
		{	/* Llib/srfi4.scm 796 */
			{	/* Llib/srfi4.scm 796 */
				long BgL_lenz00_1721;

				BgL_lenz00_1721 = bgl_list_length(BgL_xz00_149);
				{	/* Llib/srfi4.scm 796 */
					obj_t BgL_vecz00_1722;

					{	/* Llib/srfi4.scm 796 */
						int32_t BgL_tmpz00_5473;

						BgL_tmpz00_5473 = (int32_t) (BgL_lenz00_1721);
						BgL_vecz00_1722 = BGL_ALLOC_S64VECTOR(BgL_tmpz00_5473);
					}
					{	/* Llib/srfi4.scm 796 */

						{
							long BgL_iz00_2832;
							obj_t BgL_lz00_2833;

							BgL_iz00_2832 = 0L;
							BgL_lz00_2833 = BgL_xz00_149;
						BgL_loopz00_2831:
							if ((BgL_iz00_2832 == BgL_lenz00_1721))
								{	/* Llib/srfi4.scm 796 */
									return BgL_vecz00_1722;
								}
							else
								{	/* Llib/srfi4.scm 796 */
									{	/* Llib/srfi4.scm 796 */
										obj_t BgL_arg1535z00_2837;

										if (INTEGERP(CAR(((obj_t) BgL_lz00_2833))))
											{	/* Llib/srfi4.scm 796 */
												obj_t BgL_arg1539z00_2841;

												BgL_arg1539z00_2841 = CAR(((obj_t) BgL_lz00_2833));
												{	/* Llib/srfi4.scm 796 */
													long BgL_xz00_2843;

													BgL_xz00_2843 = (long) CINT(BgL_arg1539z00_2841);
													{	/* Llib/srfi4.scm 796 */
														int64_t BgL_tmpz00_5485;

														BgL_tmpz00_5485 = (int64_t) (BgL_xz00_2843);
														BgL_arg1535z00_2837 =
															BGL_INT64_TO_BINT64(BgL_tmpz00_5485);
											}}}
										else
											{	/* Llib/srfi4.scm 796 */
												BgL_arg1535z00_2837 = CAR(((obj_t) BgL_lz00_2833));
											}
										{	/* Llib/srfi4.scm 796 */
											int64_t BgL_tmpz00_5490;

											BgL_tmpz00_5490 =
												BGL_BINT64_TO_INT64(BgL_arg1535z00_2837);
											BGL_S64VSET(BgL_vecz00_1722, BgL_iz00_2832,
												BgL_tmpz00_5490);
										}
										BUNSPEC;
									}
									{	/* Llib/srfi4.scm 796 */
										long BgL_arg1543z00_2845;
										obj_t BgL_arg1544z00_2846;

										BgL_arg1543z00_2845 = (BgL_iz00_2832 + 1L);
										BgL_arg1544z00_2846 = CDR(((obj_t) BgL_lz00_2833));
										{
											obj_t BgL_lz00_5497;
											long BgL_iz00_5496;

											BgL_iz00_5496 = BgL_arg1543z00_2845;
											BgL_lz00_5497 = BgL_arg1544z00_2846;
											BgL_lz00_2833 = BgL_lz00_5497;
											BgL_iz00_2832 = BgL_iz00_5496;
											goto BgL_loopz00_2831;
										}
									}
								}
						}
					}
				}
			}
		}

	}



/* &list->s64vector */
	obj_t BGl_z62listzd2ze3s64vectorz53zz__srfi4z00(obj_t BgL_envz00_3410,
		obj_t BgL_xz00_3411)
	{
		{	/* Llib/srfi4.scm 796 */
			{	/* Llib/srfi4.scm 796 */
				obj_t BgL_auxz00_5498;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_xz00_3411))
					{	/* Llib/srfi4.scm 796 */
						BgL_auxz00_5498 = BgL_xz00_3411;
					}
				else
					{
						obj_t BgL_auxz00_5501;

						BgL_auxz00_5501 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
							BINT(31419L), BGl_string2450z00zz__srfi4z00,
							BGl_string2444z00zz__srfi4z00, BgL_xz00_3411);
						FAILURE(BgL_auxz00_5501, BFALSE, BFALSE);
					}
				return BGl_listzd2ze3s64vectorz31zz__srfi4z00(BgL_auxz00_5498);
			}
		}

	}



/* list->u64vector */
	BGL_EXPORTED_DEF obj_t BGl_listzd2ze3u64vectorz31zz__srfi4z00(obj_t
		BgL_xz00_150)
	{
		{	/* Llib/srfi4.scm 796 */
			{	/* Llib/srfi4.scm 796 */
				long BgL_lenz00_1736;

				BgL_lenz00_1736 = bgl_list_length(BgL_xz00_150);
				{	/* Llib/srfi4.scm 796 */
					obj_t BgL_vecz00_1737;

					{	/* Llib/srfi4.scm 796 */
						int32_t BgL_tmpz00_5507;

						BgL_tmpz00_5507 = (int32_t) (BgL_lenz00_1736);
						BgL_vecz00_1737 = BGL_ALLOC_U64VECTOR(BgL_tmpz00_5507);
					}
					{	/* Llib/srfi4.scm 796 */

						{
							long BgL_iz00_2858;
							obj_t BgL_lz00_2859;

							BgL_iz00_2858 = 0L;
							BgL_lz00_2859 = BgL_xz00_150;
						BgL_loopz00_2857:
							if ((BgL_iz00_2858 == BgL_lenz00_1736))
								{	/* Llib/srfi4.scm 796 */
									return BgL_vecz00_1737;
								}
							else
								{	/* Llib/srfi4.scm 796 */
									{	/* Llib/srfi4.scm 796 */
										obj_t BgL_arg1547z00_2863;

										if (INTEGERP(CAR(((obj_t) BgL_lz00_2859))))
											{	/* Llib/srfi4.scm 796 */
												obj_t BgL_arg1552z00_2867;

												BgL_arg1552z00_2867 = CAR(((obj_t) BgL_lz00_2859));
												{	/* Llib/srfi4.scm 796 */
													long BgL_xz00_2869;

													BgL_xz00_2869 = (long) CINT(BgL_arg1552z00_2867);
													{	/* Llib/srfi4.scm 796 */
														uint64_t BgL_tmpz00_5519;

														BgL_tmpz00_5519 = (uint64_t) (BgL_xz00_2869);
														BgL_arg1547z00_2863 =
															BGL_UINT64_TO_BUINT64(BgL_tmpz00_5519);
											}}}
										else
											{	/* Llib/srfi4.scm 796 */
												BgL_arg1547z00_2863 = CAR(((obj_t) BgL_lz00_2859));
											}
										{	/* Llib/srfi4.scm 796 */
											uint64_t BgL_tmpz00_5524;

											BgL_tmpz00_5524 =
												BGL_BINT64_TO_INT64(BgL_arg1547z00_2863);
											BGL_U64VSET(BgL_vecz00_1737, BgL_iz00_2858,
												BgL_tmpz00_5524);
										}
										BUNSPEC;
									}
									{	/* Llib/srfi4.scm 796 */
										long BgL_arg1554z00_2871;
										obj_t BgL_arg1555z00_2872;

										BgL_arg1554z00_2871 = (BgL_iz00_2858 + 1L);
										BgL_arg1555z00_2872 = CDR(((obj_t) BgL_lz00_2859));
										{
											obj_t BgL_lz00_5531;
											long BgL_iz00_5530;

											BgL_iz00_5530 = BgL_arg1554z00_2871;
											BgL_lz00_5531 = BgL_arg1555z00_2872;
											BgL_lz00_2859 = BgL_lz00_5531;
											BgL_iz00_2858 = BgL_iz00_5530;
											goto BgL_loopz00_2857;
										}
									}
								}
						}
					}
				}
			}
		}

	}



/* &list->u64vector */
	obj_t BGl_z62listzd2ze3u64vectorz53zz__srfi4z00(obj_t BgL_envz00_3412,
		obj_t BgL_xz00_3413)
	{
		{	/* Llib/srfi4.scm 796 */
			{	/* Llib/srfi4.scm 796 */
				obj_t BgL_auxz00_5532;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_xz00_3413))
					{	/* Llib/srfi4.scm 796 */
						BgL_auxz00_5532 = BgL_xz00_3413;
					}
				else
					{
						obj_t BgL_auxz00_5535;

						BgL_auxz00_5535 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
							BINT(31419L), BGl_string2451z00zz__srfi4z00,
							BGl_string2444z00zz__srfi4z00, BgL_xz00_3413);
						FAILURE(BgL_auxz00_5535, BFALSE, BFALSE);
					}
				return BGl_listzd2ze3u64vectorz31zz__srfi4z00(BgL_auxz00_5532);
			}
		}

	}



/* list->f32vector */
	BGL_EXPORTED_DEF obj_t BGl_listzd2ze3f32vectorz31zz__srfi4z00(obj_t
		BgL_xz00_151)
	{
		{	/* Llib/srfi4.scm 796 */
			{	/* Llib/srfi4.scm 796 */
				long BgL_lenz00_1751;

				BgL_lenz00_1751 = bgl_list_length(BgL_xz00_151);
				{	/* Llib/srfi4.scm 796 */
					obj_t BgL_vecz00_1752;

					{	/* Llib/srfi4.scm 796 */
						int32_t BgL_tmpz00_5541;

						BgL_tmpz00_5541 = (int32_t) (BgL_lenz00_1751);
						BgL_vecz00_1752 = BGL_ALLOC_F32VECTOR(BgL_tmpz00_5541);
					}
					{	/* Llib/srfi4.scm 796 */

						{
							long BgL_iz00_2884;
							obj_t BgL_lz00_2885;

							BgL_iz00_2884 = 0L;
							BgL_lz00_2885 = BgL_xz00_151;
						BgL_loopz00_2883:
							if ((BgL_iz00_2884 == BgL_lenz00_1751))
								{	/* Llib/srfi4.scm 796 */
									return BgL_vecz00_1752;
								}
							else
								{	/* Llib/srfi4.scm 796 */
									{	/* Llib/srfi4.scm 796 */
										obj_t BgL_arg1558z00_2889;

										if (INTEGERP(CAR(((obj_t) BgL_lz00_2885))))
											{	/* Llib/srfi4.scm 796 */
												obj_t BgL_arg1561z00_2893;

												BgL_arg1561z00_2893 = CAR(((obj_t) BgL_lz00_2885));
												{	/* Llib/srfi4.scm 796 */
													long BgL_xz00_2895;

													BgL_xz00_2895 = (long) CINT(BgL_arg1561z00_2893);
													{	/* Llib/srfi4.scm 796 */
														uint32_t BgL_tmpz00_5553;

														BgL_tmpz00_5553 = (uint32_t) (BgL_xz00_2895);
														BgL_arg1558z00_2889 =
															BGL_UINT32_TO_BUINT32(BgL_tmpz00_5553);
											}}}
										else
											{	/* Llib/srfi4.scm 796 */
												BgL_arg1558z00_2889 = CAR(((obj_t) BgL_lz00_2885));
											}
										{	/* Llib/srfi4.scm 796 */
											float BgL_tmpz00_5558;

											BgL_tmpz00_5558 = REAL_TO_FLOAT(BgL_arg1558z00_2889);
											BGL_F32VSET(BgL_vecz00_1752, BgL_iz00_2884,
												BgL_tmpz00_5558);
										} BUNSPEC;
									}
									{	/* Llib/srfi4.scm 796 */
										long BgL_arg1564z00_2897;
										obj_t BgL_arg1565z00_2898;

										BgL_arg1564z00_2897 = (BgL_iz00_2884 + 1L);
										BgL_arg1565z00_2898 = CDR(((obj_t) BgL_lz00_2885));
										{
											obj_t BgL_lz00_5565;
											long BgL_iz00_5564;

											BgL_iz00_5564 = BgL_arg1564z00_2897;
											BgL_lz00_5565 = BgL_arg1565z00_2898;
											BgL_lz00_2885 = BgL_lz00_5565;
											BgL_iz00_2884 = BgL_iz00_5564;
											goto BgL_loopz00_2883;
										}
									}
								}
						}
					}
				}
			}
		}

	}



/* &list->f32vector */
	obj_t BGl_z62listzd2ze3f32vectorz53zz__srfi4z00(obj_t BgL_envz00_3414,
		obj_t BgL_xz00_3415)
	{
		{	/* Llib/srfi4.scm 796 */
			{	/* Llib/srfi4.scm 796 */
				obj_t BgL_auxz00_5566;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_xz00_3415))
					{	/* Llib/srfi4.scm 796 */
						BgL_auxz00_5566 = BgL_xz00_3415;
					}
				else
					{
						obj_t BgL_auxz00_5569;

						BgL_auxz00_5569 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
							BINT(31419L), BGl_string2452z00zz__srfi4z00,
							BGl_string2444z00zz__srfi4z00, BgL_xz00_3415);
						FAILURE(BgL_auxz00_5569, BFALSE, BFALSE);
					}
				return BGl_listzd2ze3f32vectorz31zz__srfi4z00(BgL_auxz00_5566);
			}
		}

	}



/* list->f64vector */
	BGL_EXPORTED_DEF obj_t BGl_listzd2ze3f64vectorz31zz__srfi4z00(obj_t
		BgL_xz00_152)
	{
		{	/* Llib/srfi4.scm 796 */
			{	/* Llib/srfi4.scm 796 */
				long BgL_lenz00_1766;

				BgL_lenz00_1766 = bgl_list_length(BgL_xz00_152);
				{	/* Llib/srfi4.scm 796 */
					obj_t BgL_vecz00_1767;

					{	/* Llib/srfi4.scm 796 */
						int32_t BgL_tmpz00_5575;

						BgL_tmpz00_5575 = (int32_t) (BgL_lenz00_1766);
						BgL_vecz00_1767 = BGL_ALLOC_F64VECTOR(BgL_tmpz00_5575);
					}
					{	/* Llib/srfi4.scm 796 */

						{
							long BgL_iz00_2910;
							obj_t BgL_lz00_2911;

							BgL_iz00_2910 = 0L;
							BgL_lz00_2911 = BgL_xz00_152;
						BgL_loopz00_2909:
							if ((BgL_iz00_2910 == BgL_lenz00_1766))
								{	/* Llib/srfi4.scm 796 */
									return BgL_vecz00_1767;
								}
							else
								{	/* Llib/srfi4.scm 796 */
									{	/* Llib/srfi4.scm 796 */
										obj_t BgL_arg1571z00_2915;

										if (INTEGERP(CAR(((obj_t) BgL_lz00_2911))))
											{	/* Llib/srfi4.scm 796 */
												obj_t BgL_arg1575z00_2919;

												BgL_arg1575z00_2919 = CAR(((obj_t) BgL_lz00_2911));
												{	/* Llib/srfi4.scm 796 */
													long BgL_xz00_2921;

													BgL_xz00_2921 = (long) CINT(BgL_arg1575z00_2919);
													{	/* Llib/srfi4.scm 796 */
														uint64_t BgL_tmpz00_5587;

														BgL_tmpz00_5587 = (uint64_t) (BgL_xz00_2921);
														BgL_arg1571z00_2915 =
															BGL_UINT64_TO_BUINT64(BgL_tmpz00_5587);
											}}}
										else
											{	/* Llib/srfi4.scm 796 */
												BgL_arg1571z00_2915 = CAR(((obj_t) BgL_lz00_2911));
											}
										{	/* Llib/srfi4.scm 796 */
											double BgL_tmpz00_5592;

											BgL_tmpz00_5592 = REAL_TO_DOUBLE(BgL_arg1571z00_2915);
											BGL_F64VSET(BgL_vecz00_1767, BgL_iz00_2910,
												BgL_tmpz00_5592);
										} BUNSPEC;
									}
									{	/* Llib/srfi4.scm 796 */
										long BgL_arg1578z00_2923;
										obj_t BgL_arg1579z00_2924;

										BgL_arg1578z00_2923 = (BgL_iz00_2910 + 1L);
										BgL_arg1579z00_2924 = CDR(((obj_t) BgL_lz00_2911));
										{
											obj_t BgL_lz00_5599;
											long BgL_iz00_5598;

											BgL_iz00_5598 = BgL_arg1578z00_2923;
											BgL_lz00_5599 = BgL_arg1579z00_2924;
											BgL_lz00_2911 = BgL_lz00_5599;
											BgL_iz00_2910 = BgL_iz00_5598;
											goto BgL_loopz00_2909;
										}
									}
								}
						}
					}
				}
			}
		}

	}



/* &list->f64vector */
	obj_t BGl_z62listzd2ze3f64vectorz53zz__srfi4z00(obj_t BgL_envz00_3416,
		obj_t BgL_xz00_3417)
	{
		{	/* Llib/srfi4.scm 796 */
			{	/* Llib/srfi4.scm 796 */
				obj_t BgL_auxz00_5600;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_xz00_3417))
					{	/* Llib/srfi4.scm 796 */
						BgL_auxz00_5600 = BgL_xz00_3417;
					}
				else
					{
						obj_t BgL_auxz00_5603;

						BgL_auxz00_5603 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2368z00zz__srfi4z00,
							BINT(31419L), BGl_string2453z00zz__srfi4z00,
							BGl_string2444z00zz__srfi4z00, BgL_xz00_3417);
						FAILURE(BgL_auxz00_5603, BFALSE, BFALSE);
					}
				return BGl_listzd2ze3f64vectorz31zz__srfi4z00(BgL_auxz00_5600);
			}
		}

	}



/* _s8vector-copy! */
	obj_t BGl__s8vectorzd2copyz12zc0zz__srfi4z00(obj_t BgL_env1279z00_159,
		obj_t BgL_opt1278z00_158)
	{
		{	/* Llib/srfi4.scm 838 */
			{	/* Llib/srfi4.scm 838 */
				obj_t BgL_targetz00_1781;
				obj_t BgL_tstartz00_1782;
				obj_t BgL_sourcez00_1783;

				BgL_targetz00_1781 = VECTOR_REF(BgL_opt1278z00_158, 0L);
				BgL_tstartz00_1782 = VECTOR_REF(BgL_opt1278z00_158, 1L);
				BgL_sourcez00_1783 = VECTOR_REF(BgL_opt1278z00_158, 2L);
				switch (VECTOR_LENGTH(BgL_opt1278z00_158))
					{
					case 3L:

						{	/* Llib/srfi4.scm 838 */
							long BgL_sendz00_1787;

							{	/* Llib/srfi4.scm 838 */
								obj_t BgL_xz00_2927;

								if (BGL_S8VECTORP(BgL_sourcez00_1783))
									{	/* Llib/srfi4.scm 838 */
										BgL_xz00_2927 = BgL_sourcez00_1783;
									}
								else
									{
										obj_t BgL_auxz00_5613;

										BgL_auxz00_5613 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2368z00zz__srfi4z00, BINT(33208L),
											BGl_string2454z00zz__srfi4z00,
											BGl_string2370z00zz__srfi4z00, BgL_sourcez00_1783);
										FAILURE(BgL_auxz00_5613, BFALSE, BFALSE);
									}
								BgL_sendz00_1787 = BGL_HVECTOR_LENGTH(BgL_xz00_2927);
							}
							{	/* Llib/srfi4.scm 838 */

								{	/* Llib/srfi4.scm 838 */
									obj_t BgL_targetz00_2928;
									long BgL_tstartz00_2929;
									obj_t BgL_sourcez00_2930;

									if (BGL_S8VECTORP(BgL_targetz00_1781))
										{	/* Llib/srfi4.scm 838 */
											BgL_targetz00_2928 = BgL_targetz00_1781;
										}
									else
										{
											obj_t BgL_auxz00_5620;

											BgL_auxz00_5620 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(33208L),
												BGl_string2454z00zz__srfi4z00,
												BGl_string2370z00zz__srfi4z00, BgL_targetz00_1781);
											FAILURE(BgL_auxz00_5620, BFALSE, BFALSE);
										}
									{	/* Llib/srfi4.scm 838 */
										obj_t BgL_tmpz00_5624;

										if (INTEGERP(BgL_tstartz00_1782))
											{	/* Llib/srfi4.scm 838 */
												BgL_tmpz00_5624 = BgL_tstartz00_1782;
											}
										else
											{
												obj_t BgL_auxz00_5627;

												BgL_auxz00_5627 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2454z00zz__srfi4z00,
													BGl_string2390z00zz__srfi4z00, BgL_tstartz00_1782);
												FAILURE(BgL_auxz00_5627, BFALSE, BFALSE);
											}
										BgL_tstartz00_2929 = (long) CINT(BgL_tmpz00_5624);
									}
									if (BGL_S8VECTORP(BgL_sourcez00_1783))
										{	/* Llib/srfi4.scm 838 */
											BgL_sourcez00_2930 = BgL_sourcez00_1783;
										}
									else
										{
											obj_t BgL_auxz00_5634;

											BgL_auxz00_5634 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(33208L),
												BGl_string2454z00zz__srfi4z00,
												BGl_string2370z00zz__srfi4z00, BgL_sourcez00_1783);
											FAILURE(BgL_auxz00_5634, BFALSE, BFALSE);
										}
									BGL_SU8VECTOR_COPY(BgL_targetz00_2928, BgL_tstartz00_2929,
										BgL_sourcez00_2930, 0L, BgL_sendz00_1787);
									return BUNSPEC;
								}
							}
						}
						break;
					case 4L:

						{	/* Llib/srfi4.scm 838 */
							obj_t BgL_sstartz00_1788;

							BgL_sstartz00_1788 = VECTOR_REF(BgL_opt1278z00_158, 3L);
							{	/* Llib/srfi4.scm 838 */
								long BgL_sendz00_1789;

								{	/* Llib/srfi4.scm 838 */
									obj_t BgL_xz00_2931;

									if (BGL_S8VECTORP(BgL_sourcez00_1783))
										{	/* Llib/srfi4.scm 838 */
											BgL_xz00_2931 = BgL_sourcez00_1783;
										}
									else
										{
											obj_t BgL_auxz00_5642;

											BgL_auxz00_5642 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(33208L),
												BGl_string2454z00zz__srfi4z00,
												BGl_string2370z00zz__srfi4z00, BgL_sourcez00_1783);
											FAILURE(BgL_auxz00_5642, BFALSE, BFALSE);
										}
									BgL_sendz00_1789 = BGL_HVECTOR_LENGTH(BgL_xz00_2931);
								}
								{	/* Llib/srfi4.scm 838 */

									{	/* Llib/srfi4.scm 838 */
										obj_t BgL_targetz00_2932;
										long BgL_tstartz00_2933;
										obj_t BgL_sourcez00_2934;

										if (BGL_S8VECTORP(BgL_targetz00_1781))
											{	/* Llib/srfi4.scm 838 */
												BgL_targetz00_2932 = BgL_targetz00_1781;
											}
										else
											{
												obj_t BgL_auxz00_5649;

												BgL_auxz00_5649 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2454z00zz__srfi4z00,
													BGl_string2370z00zz__srfi4z00, BgL_targetz00_1781);
												FAILURE(BgL_auxz00_5649, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											obj_t BgL_tmpz00_5653;

											if (INTEGERP(BgL_tstartz00_1782))
												{	/* Llib/srfi4.scm 838 */
													BgL_tmpz00_5653 = BgL_tstartz00_1782;
												}
											else
												{
													obj_t BgL_auxz00_5656;

													BgL_auxz00_5656 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2368z00zz__srfi4z00, BINT(33208L),
														BGl_string2454z00zz__srfi4z00,
														BGl_string2390z00zz__srfi4z00, BgL_tstartz00_1782);
													FAILURE(BgL_auxz00_5656, BFALSE, BFALSE);
												}
											BgL_tstartz00_2933 = (long) CINT(BgL_tmpz00_5653);
										}
										if (BGL_S8VECTORP(BgL_sourcez00_1783))
											{	/* Llib/srfi4.scm 838 */
												BgL_sourcez00_2934 = BgL_sourcez00_1783;
											}
										else
											{
												obj_t BgL_auxz00_5663;

												BgL_auxz00_5663 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2454z00zz__srfi4z00,
													BGl_string2370z00zz__srfi4z00, BgL_sourcez00_1783);
												FAILURE(BgL_auxz00_5663, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											long BgL_tmpz00_5667;

											{	/* Llib/srfi4.scm 838 */
												obj_t BgL_tmpz00_5668;

												if (INTEGERP(BgL_sstartz00_1788))
													{	/* Llib/srfi4.scm 838 */
														BgL_tmpz00_5668 = BgL_sstartz00_1788;
													}
												else
													{
														obj_t BgL_auxz00_5671;

														BgL_auxz00_5671 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2368z00zz__srfi4z00, BINT(33208L),
															BGl_string2454z00zz__srfi4z00,
															BGl_string2390z00zz__srfi4z00,
															BgL_sstartz00_1788);
														FAILURE(BgL_auxz00_5671, BFALSE, BFALSE);
													}
												BgL_tmpz00_5667 = (long) CINT(BgL_tmpz00_5668);
											}
											BGL_SU8VECTOR_COPY(BgL_targetz00_2932, BgL_tstartz00_2933,
												BgL_sourcez00_2934, BgL_tmpz00_5667, BgL_sendz00_1789);
										}
										return BUNSPEC;
									}
								}
							}
						}
						break;
					case 5L:

						{	/* Llib/srfi4.scm 838 */
							obj_t BgL_sstartz00_1790;

							BgL_sstartz00_1790 = VECTOR_REF(BgL_opt1278z00_158, 3L);
							{	/* Llib/srfi4.scm 838 */
								obj_t BgL_sendz00_1791;

								BgL_sendz00_1791 = VECTOR_REF(BgL_opt1278z00_158, 4L);
								{	/* Llib/srfi4.scm 838 */

									{	/* Llib/srfi4.scm 838 */
										obj_t BgL_targetz00_2935;
										long BgL_tstartz00_2936;
										obj_t BgL_sourcez00_2937;

										if (BGL_S8VECTORP(BgL_targetz00_1781))
											{	/* Llib/srfi4.scm 838 */
												BgL_targetz00_2935 = BgL_targetz00_1781;
											}
										else
											{
												obj_t BgL_auxz00_5681;

												BgL_auxz00_5681 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2454z00zz__srfi4z00,
													BGl_string2370z00zz__srfi4z00, BgL_targetz00_1781);
												FAILURE(BgL_auxz00_5681, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											obj_t BgL_tmpz00_5685;

											if (INTEGERP(BgL_tstartz00_1782))
												{	/* Llib/srfi4.scm 838 */
													BgL_tmpz00_5685 = BgL_tstartz00_1782;
												}
											else
												{
													obj_t BgL_auxz00_5688;

													BgL_auxz00_5688 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2368z00zz__srfi4z00, BINT(33208L),
														BGl_string2454z00zz__srfi4z00,
														BGl_string2390z00zz__srfi4z00, BgL_tstartz00_1782);
													FAILURE(BgL_auxz00_5688, BFALSE, BFALSE);
												}
											BgL_tstartz00_2936 = (long) CINT(BgL_tmpz00_5685);
										}
										if (BGL_S8VECTORP(BgL_sourcez00_1783))
											{	/* Llib/srfi4.scm 838 */
												BgL_sourcez00_2937 = BgL_sourcez00_1783;
											}
										else
											{
												obj_t BgL_auxz00_5695;

												BgL_auxz00_5695 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2454z00zz__srfi4z00,
													BGl_string2370z00zz__srfi4z00, BgL_sourcez00_1783);
												FAILURE(BgL_auxz00_5695, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											long BgL_auxz00_5708;
											long BgL_tmpz00_5699;

											{	/* Llib/srfi4.scm 838 */
												obj_t BgL_tmpz00_5709;

												if (INTEGERP(BgL_sendz00_1791))
													{	/* Llib/srfi4.scm 838 */
														BgL_tmpz00_5709 = BgL_sendz00_1791;
													}
												else
													{
														obj_t BgL_auxz00_5712;

														BgL_auxz00_5712 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2368z00zz__srfi4z00, BINT(33208L),
															BGl_string2454z00zz__srfi4z00,
															BGl_string2390z00zz__srfi4z00, BgL_sendz00_1791);
														FAILURE(BgL_auxz00_5712, BFALSE, BFALSE);
													}
												BgL_auxz00_5708 = (long) CINT(BgL_tmpz00_5709);
											}
											{	/* Llib/srfi4.scm 838 */
												obj_t BgL_tmpz00_5700;

												if (INTEGERP(BgL_sstartz00_1790))
													{	/* Llib/srfi4.scm 838 */
														BgL_tmpz00_5700 = BgL_sstartz00_1790;
													}
												else
													{
														obj_t BgL_auxz00_5703;

														BgL_auxz00_5703 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2368z00zz__srfi4z00, BINT(33208L),
															BGl_string2454z00zz__srfi4z00,
															BGl_string2390z00zz__srfi4z00,
															BgL_sstartz00_1790);
														FAILURE(BgL_auxz00_5703, BFALSE, BFALSE);
													}
												BgL_tmpz00_5699 = (long) CINT(BgL_tmpz00_5700);
											}
											BGL_SU8VECTOR_COPY(BgL_targetz00_2935, BgL_tstartz00_2936,
												BgL_sourcez00_2937, BgL_tmpz00_5699, BgL_auxz00_5708);
										}
										return BUNSPEC;
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* s8vector-copy! */
	BGL_EXPORTED_DEF obj_t BGl_s8vectorzd2copyz12zc0zz__srfi4z00(obj_t
		BgL_targetz00_153, long BgL_tstartz00_154, obj_t BgL_sourcez00_155,
		obj_t BgL_sstartz00_156, obj_t BgL_sendz00_157)
	{
		{	/* Llib/srfi4.scm 838 */
			{	/* Llib/srfi4.scm 838 */
				long BgL_auxz00_5722;
				long BgL_tmpz00_5720;

				BgL_auxz00_5722 = (long) CINT(BgL_sendz00_157);
				BgL_tmpz00_5720 = (long) CINT(BgL_sstartz00_156);
				BGL_SU8VECTOR_COPY(BgL_targetz00_153, BgL_tstartz00_154,
					BgL_sourcez00_155, BgL_tmpz00_5720, BgL_auxz00_5722);
			}
			return BUNSPEC;
		}

	}



/* _u8vector-copy! */
	obj_t BGl__u8vectorzd2copyz12zc0zz__srfi4z00(obj_t BgL_env1283z00_166,
		obj_t BgL_opt1282z00_165)
	{
		{	/* Llib/srfi4.scm 838 */
			{	/* Llib/srfi4.scm 838 */
				obj_t BgL_targetz00_1792;
				obj_t BgL_tstartz00_1793;
				obj_t BgL_sourcez00_1794;

				BgL_targetz00_1792 = VECTOR_REF(BgL_opt1282z00_165, 0L);
				BgL_tstartz00_1793 = VECTOR_REF(BgL_opt1282z00_165, 1L);
				BgL_sourcez00_1794 = VECTOR_REF(BgL_opt1282z00_165, 2L);
				switch (VECTOR_LENGTH(BgL_opt1282z00_165))
					{
					case 3L:

						{	/* Llib/srfi4.scm 838 */
							long BgL_sendz00_1798;

							{	/* Llib/srfi4.scm 838 */
								obj_t BgL_xz00_2938;

								if (BGL_U8VECTORP(BgL_sourcez00_1794))
									{	/* Llib/srfi4.scm 838 */
										BgL_xz00_2938 = BgL_sourcez00_1794;
									}
								else
									{
										obj_t BgL_auxz00_5730;

										BgL_auxz00_5730 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2368z00zz__srfi4z00, BINT(33208L),
											BGl_string2455z00zz__srfi4z00,
											BGl_string2372z00zz__srfi4z00, BgL_sourcez00_1794);
										FAILURE(BgL_auxz00_5730, BFALSE, BFALSE);
									}
								BgL_sendz00_1798 = BGL_HVECTOR_LENGTH(BgL_xz00_2938);
							}
							{	/* Llib/srfi4.scm 838 */

								{	/* Llib/srfi4.scm 838 */
									obj_t BgL_targetz00_2939;
									long BgL_tstartz00_2940;
									obj_t BgL_sourcez00_2941;

									if (BGL_U8VECTORP(BgL_targetz00_1792))
										{	/* Llib/srfi4.scm 838 */
											BgL_targetz00_2939 = BgL_targetz00_1792;
										}
									else
										{
											obj_t BgL_auxz00_5737;

											BgL_auxz00_5737 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(33208L),
												BGl_string2455z00zz__srfi4z00,
												BGl_string2372z00zz__srfi4z00, BgL_targetz00_1792);
											FAILURE(BgL_auxz00_5737, BFALSE, BFALSE);
										}
									{	/* Llib/srfi4.scm 838 */
										obj_t BgL_tmpz00_5741;

										if (INTEGERP(BgL_tstartz00_1793))
											{	/* Llib/srfi4.scm 838 */
												BgL_tmpz00_5741 = BgL_tstartz00_1793;
											}
										else
											{
												obj_t BgL_auxz00_5744;

												BgL_auxz00_5744 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2455z00zz__srfi4z00,
													BGl_string2390z00zz__srfi4z00, BgL_tstartz00_1793);
												FAILURE(BgL_auxz00_5744, BFALSE, BFALSE);
											}
										BgL_tstartz00_2940 = (long) CINT(BgL_tmpz00_5741);
									}
									if (BGL_U8VECTORP(BgL_sourcez00_1794))
										{	/* Llib/srfi4.scm 838 */
											BgL_sourcez00_2941 = BgL_sourcez00_1794;
										}
									else
										{
											obj_t BgL_auxz00_5751;

											BgL_auxz00_5751 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(33208L),
												BGl_string2455z00zz__srfi4z00,
												BGl_string2372z00zz__srfi4z00, BgL_sourcez00_1794);
											FAILURE(BgL_auxz00_5751, BFALSE, BFALSE);
										}
									BGL_SU8VECTOR_COPY(BgL_targetz00_2939, BgL_tstartz00_2940,
										BgL_sourcez00_2941, 0L, BgL_sendz00_1798);
									return BUNSPEC;
								}
							}
						}
						break;
					case 4L:

						{	/* Llib/srfi4.scm 838 */
							obj_t BgL_sstartz00_1799;

							BgL_sstartz00_1799 = VECTOR_REF(BgL_opt1282z00_165, 3L);
							{	/* Llib/srfi4.scm 838 */
								long BgL_sendz00_1800;

								{	/* Llib/srfi4.scm 838 */
									obj_t BgL_xz00_2942;

									if (BGL_U8VECTORP(BgL_sourcez00_1794))
										{	/* Llib/srfi4.scm 838 */
											BgL_xz00_2942 = BgL_sourcez00_1794;
										}
									else
										{
											obj_t BgL_auxz00_5759;

											BgL_auxz00_5759 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(33208L),
												BGl_string2455z00zz__srfi4z00,
												BGl_string2372z00zz__srfi4z00, BgL_sourcez00_1794);
											FAILURE(BgL_auxz00_5759, BFALSE, BFALSE);
										}
									BgL_sendz00_1800 = BGL_HVECTOR_LENGTH(BgL_xz00_2942);
								}
								{	/* Llib/srfi4.scm 838 */

									{	/* Llib/srfi4.scm 838 */
										obj_t BgL_targetz00_2943;
										long BgL_tstartz00_2944;
										obj_t BgL_sourcez00_2945;

										if (BGL_U8VECTORP(BgL_targetz00_1792))
											{	/* Llib/srfi4.scm 838 */
												BgL_targetz00_2943 = BgL_targetz00_1792;
											}
										else
											{
												obj_t BgL_auxz00_5766;

												BgL_auxz00_5766 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2455z00zz__srfi4z00,
													BGl_string2372z00zz__srfi4z00, BgL_targetz00_1792);
												FAILURE(BgL_auxz00_5766, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											obj_t BgL_tmpz00_5770;

											if (INTEGERP(BgL_tstartz00_1793))
												{	/* Llib/srfi4.scm 838 */
													BgL_tmpz00_5770 = BgL_tstartz00_1793;
												}
											else
												{
													obj_t BgL_auxz00_5773;

													BgL_auxz00_5773 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2368z00zz__srfi4z00, BINT(33208L),
														BGl_string2455z00zz__srfi4z00,
														BGl_string2390z00zz__srfi4z00, BgL_tstartz00_1793);
													FAILURE(BgL_auxz00_5773, BFALSE, BFALSE);
												}
											BgL_tstartz00_2944 = (long) CINT(BgL_tmpz00_5770);
										}
										if (BGL_U8VECTORP(BgL_sourcez00_1794))
											{	/* Llib/srfi4.scm 838 */
												BgL_sourcez00_2945 = BgL_sourcez00_1794;
											}
										else
											{
												obj_t BgL_auxz00_5780;

												BgL_auxz00_5780 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2455z00zz__srfi4z00,
													BGl_string2372z00zz__srfi4z00, BgL_sourcez00_1794);
												FAILURE(BgL_auxz00_5780, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											long BgL_tmpz00_5784;

											{	/* Llib/srfi4.scm 838 */
												obj_t BgL_tmpz00_5785;

												if (INTEGERP(BgL_sstartz00_1799))
													{	/* Llib/srfi4.scm 838 */
														BgL_tmpz00_5785 = BgL_sstartz00_1799;
													}
												else
													{
														obj_t BgL_auxz00_5788;

														BgL_auxz00_5788 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2368z00zz__srfi4z00, BINT(33208L),
															BGl_string2455z00zz__srfi4z00,
															BGl_string2390z00zz__srfi4z00,
															BgL_sstartz00_1799);
														FAILURE(BgL_auxz00_5788, BFALSE, BFALSE);
													}
												BgL_tmpz00_5784 = (long) CINT(BgL_tmpz00_5785);
											}
											BGL_SU8VECTOR_COPY(BgL_targetz00_2943, BgL_tstartz00_2944,
												BgL_sourcez00_2945, BgL_tmpz00_5784, BgL_sendz00_1800);
										}
										return BUNSPEC;
									}
								}
							}
						}
						break;
					case 5L:

						{	/* Llib/srfi4.scm 838 */
							obj_t BgL_sstartz00_1801;

							BgL_sstartz00_1801 = VECTOR_REF(BgL_opt1282z00_165, 3L);
							{	/* Llib/srfi4.scm 838 */
								obj_t BgL_sendz00_1802;

								BgL_sendz00_1802 = VECTOR_REF(BgL_opt1282z00_165, 4L);
								{	/* Llib/srfi4.scm 838 */

									{	/* Llib/srfi4.scm 838 */
										obj_t BgL_targetz00_2946;
										long BgL_tstartz00_2947;
										obj_t BgL_sourcez00_2948;

										if (BGL_U8VECTORP(BgL_targetz00_1792))
											{	/* Llib/srfi4.scm 838 */
												BgL_targetz00_2946 = BgL_targetz00_1792;
											}
										else
											{
												obj_t BgL_auxz00_5798;

												BgL_auxz00_5798 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2455z00zz__srfi4z00,
													BGl_string2372z00zz__srfi4z00, BgL_targetz00_1792);
												FAILURE(BgL_auxz00_5798, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											obj_t BgL_tmpz00_5802;

											if (INTEGERP(BgL_tstartz00_1793))
												{	/* Llib/srfi4.scm 838 */
													BgL_tmpz00_5802 = BgL_tstartz00_1793;
												}
											else
												{
													obj_t BgL_auxz00_5805;

													BgL_auxz00_5805 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2368z00zz__srfi4z00, BINT(33208L),
														BGl_string2455z00zz__srfi4z00,
														BGl_string2390z00zz__srfi4z00, BgL_tstartz00_1793);
													FAILURE(BgL_auxz00_5805, BFALSE, BFALSE);
												}
											BgL_tstartz00_2947 = (long) CINT(BgL_tmpz00_5802);
										}
										if (BGL_U8VECTORP(BgL_sourcez00_1794))
											{	/* Llib/srfi4.scm 838 */
												BgL_sourcez00_2948 = BgL_sourcez00_1794;
											}
										else
											{
												obj_t BgL_auxz00_5812;

												BgL_auxz00_5812 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2455z00zz__srfi4z00,
													BGl_string2372z00zz__srfi4z00, BgL_sourcez00_1794);
												FAILURE(BgL_auxz00_5812, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											long BgL_auxz00_5825;
											long BgL_tmpz00_5816;

											{	/* Llib/srfi4.scm 838 */
												obj_t BgL_tmpz00_5826;

												if (INTEGERP(BgL_sendz00_1802))
													{	/* Llib/srfi4.scm 838 */
														BgL_tmpz00_5826 = BgL_sendz00_1802;
													}
												else
													{
														obj_t BgL_auxz00_5829;

														BgL_auxz00_5829 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2368z00zz__srfi4z00, BINT(33208L),
															BGl_string2455z00zz__srfi4z00,
															BGl_string2390z00zz__srfi4z00, BgL_sendz00_1802);
														FAILURE(BgL_auxz00_5829, BFALSE, BFALSE);
													}
												BgL_auxz00_5825 = (long) CINT(BgL_tmpz00_5826);
											}
											{	/* Llib/srfi4.scm 838 */
												obj_t BgL_tmpz00_5817;

												if (INTEGERP(BgL_sstartz00_1801))
													{	/* Llib/srfi4.scm 838 */
														BgL_tmpz00_5817 = BgL_sstartz00_1801;
													}
												else
													{
														obj_t BgL_auxz00_5820;

														BgL_auxz00_5820 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2368z00zz__srfi4z00, BINT(33208L),
															BGl_string2455z00zz__srfi4z00,
															BGl_string2390z00zz__srfi4z00,
															BgL_sstartz00_1801);
														FAILURE(BgL_auxz00_5820, BFALSE, BFALSE);
													}
												BgL_tmpz00_5816 = (long) CINT(BgL_tmpz00_5817);
											}
											BGL_SU8VECTOR_COPY(BgL_targetz00_2946, BgL_tstartz00_2947,
												BgL_sourcez00_2948, BgL_tmpz00_5816, BgL_auxz00_5825);
										}
										return BUNSPEC;
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* u8vector-copy! */
	BGL_EXPORTED_DEF obj_t BGl_u8vectorzd2copyz12zc0zz__srfi4z00(obj_t
		BgL_targetz00_160, long BgL_tstartz00_161, obj_t BgL_sourcez00_162,
		obj_t BgL_sstartz00_163, obj_t BgL_sendz00_164)
	{
		{	/* Llib/srfi4.scm 838 */
			{	/* Llib/srfi4.scm 838 */
				long BgL_auxz00_5839;
				long BgL_tmpz00_5837;

				BgL_auxz00_5839 = (long) CINT(BgL_sendz00_164);
				BgL_tmpz00_5837 = (long) CINT(BgL_sstartz00_163);
				BGL_SU8VECTOR_COPY(BgL_targetz00_160, BgL_tstartz00_161,
					BgL_sourcez00_162, BgL_tmpz00_5837, BgL_auxz00_5839);
			}
			return BUNSPEC;
		}

	}



/* _s16vector-copy! */
	obj_t BGl__s16vectorzd2copyz12zc0zz__srfi4z00(obj_t BgL_env1287z00_173,
		obj_t BgL_opt1286z00_172)
	{
		{	/* Llib/srfi4.scm 838 */
			{	/* Llib/srfi4.scm 838 */
				obj_t BgL_targetz00_1803;
				obj_t BgL_tstartz00_1804;
				obj_t BgL_sourcez00_1805;

				BgL_targetz00_1803 = VECTOR_REF(BgL_opt1286z00_172, 0L);
				BgL_tstartz00_1804 = VECTOR_REF(BgL_opt1286z00_172, 1L);
				BgL_sourcez00_1805 = VECTOR_REF(BgL_opt1286z00_172, 2L);
				switch (VECTOR_LENGTH(BgL_opt1286z00_172))
					{
					case 3L:

						{	/* Llib/srfi4.scm 838 */
							long BgL_sendz00_1809;

							{	/* Llib/srfi4.scm 838 */
								obj_t BgL_xz00_2949;

								if (BGL_S16VECTORP(BgL_sourcez00_1805))
									{	/* Llib/srfi4.scm 838 */
										BgL_xz00_2949 = BgL_sourcez00_1805;
									}
								else
									{
										obj_t BgL_auxz00_5847;

										BgL_auxz00_5847 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2368z00zz__srfi4z00, BINT(33208L),
											BGl_string2456z00zz__srfi4z00,
											BGl_string2374z00zz__srfi4z00, BgL_sourcez00_1805);
										FAILURE(BgL_auxz00_5847, BFALSE, BFALSE);
									}
								BgL_sendz00_1809 = BGL_HVECTOR_LENGTH(BgL_xz00_2949);
							}
							{	/* Llib/srfi4.scm 838 */

								{	/* Llib/srfi4.scm 838 */
									obj_t BgL_targetz00_2950;
									long BgL_tstartz00_2951;
									obj_t BgL_sourcez00_2952;

									if (BGL_S16VECTORP(BgL_targetz00_1803))
										{	/* Llib/srfi4.scm 838 */
											BgL_targetz00_2950 = BgL_targetz00_1803;
										}
									else
										{
											obj_t BgL_auxz00_5854;

											BgL_auxz00_5854 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(33208L),
												BGl_string2456z00zz__srfi4z00,
												BGl_string2374z00zz__srfi4z00, BgL_targetz00_1803);
											FAILURE(BgL_auxz00_5854, BFALSE, BFALSE);
										}
									{	/* Llib/srfi4.scm 838 */
										obj_t BgL_tmpz00_5858;

										if (INTEGERP(BgL_tstartz00_1804))
											{	/* Llib/srfi4.scm 838 */
												BgL_tmpz00_5858 = BgL_tstartz00_1804;
											}
										else
											{
												obj_t BgL_auxz00_5861;

												BgL_auxz00_5861 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2456z00zz__srfi4z00,
													BGl_string2390z00zz__srfi4z00, BgL_tstartz00_1804);
												FAILURE(BgL_auxz00_5861, BFALSE, BFALSE);
											}
										BgL_tstartz00_2951 = (long) CINT(BgL_tmpz00_5858);
									}
									if (BGL_S16VECTORP(BgL_sourcez00_1805))
										{	/* Llib/srfi4.scm 838 */
											BgL_sourcez00_2952 = BgL_sourcez00_1805;
										}
									else
										{
											obj_t BgL_auxz00_5868;

											BgL_auxz00_5868 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(33208L),
												BGl_string2456z00zz__srfi4z00,
												BGl_string2374z00zz__srfi4z00, BgL_sourcez00_1805);
											FAILURE(BgL_auxz00_5868, BFALSE, BFALSE);
										}
									BGL_SU16VECTOR_COPY(BgL_targetz00_2950, BgL_tstartz00_2951,
										BgL_sourcez00_2952, 0L, BgL_sendz00_1809);
									return BUNSPEC;
								}
							}
						}
						break;
					case 4L:

						{	/* Llib/srfi4.scm 838 */
							obj_t BgL_sstartz00_1810;

							BgL_sstartz00_1810 = VECTOR_REF(BgL_opt1286z00_172, 3L);
							{	/* Llib/srfi4.scm 838 */
								long BgL_sendz00_1811;

								{	/* Llib/srfi4.scm 838 */
									obj_t BgL_xz00_2953;

									if (BGL_S16VECTORP(BgL_sourcez00_1805))
										{	/* Llib/srfi4.scm 838 */
											BgL_xz00_2953 = BgL_sourcez00_1805;
										}
									else
										{
											obj_t BgL_auxz00_5876;

											BgL_auxz00_5876 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(33208L),
												BGl_string2456z00zz__srfi4z00,
												BGl_string2374z00zz__srfi4z00, BgL_sourcez00_1805);
											FAILURE(BgL_auxz00_5876, BFALSE, BFALSE);
										}
									BgL_sendz00_1811 = BGL_HVECTOR_LENGTH(BgL_xz00_2953);
								}
								{	/* Llib/srfi4.scm 838 */

									{	/* Llib/srfi4.scm 838 */
										obj_t BgL_targetz00_2954;
										long BgL_tstartz00_2955;
										obj_t BgL_sourcez00_2956;

										if (BGL_S16VECTORP(BgL_targetz00_1803))
											{	/* Llib/srfi4.scm 838 */
												BgL_targetz00_2954 = BgL_targetz00_1803;
											}
										else
											{
												obj_t BgL_auxz00_5883;

												BgL_auxz00_5883 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2456z00zz__srfi4z00,
													BGl_string2374z00zz__srfi4z00, BgL_targetz00_1803);
												FAILURE(BgL_auxz00_5883, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											obj_t BgL_tmpz00_5887;

											if (INTEGERP(BgL_tstartz00_1804))
												{	/* Llib/srfi4.scm 838 */
													BgL_tmpz00_5887 = BgL_tstartz00_1804;
												}
											else
												{
													obj_t BgL_auxz00_5890;

													BgL_auxz00_5890 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2368z00zz__srfi4z00, BINT(33208L),
														BGl_string2456z00zz__srfi4z00,
														BGl_string2390z00zz__srfi4z00, BgL_tstartz00_1804);
													FAILURE(BgL_auxz00_5890, BFALSE, BFALSE);
												}
											BgL_tstartz00_2955 = (long) CINT(BgL_tmpz00_5887);
										}
										if (BGL_S16VECTORP(BgL_sourcez00_1805))
											{	/* Llib/srfi4.scm 838 */
												BgL_sourcez00_2956 = BgL_sourcez00_1805;
											}
										else
											{
												obj_t BgL_auxz00_5897;

												BgL_auxz00_5897 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2456z00zz__srfi4z00,
													BGl_string2374z00zz__srfi4z00, BgL_sourcez00_1805);
												FAILURE(BgL_auxz00_5897, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											long BgL_tmpz00_5901;

											{	/* Llib/srfi4.scm 838 */
												obj_t BgL_tmpz00_5902;

												if (INTEGERP(BgL_sstartz00_1810))
													{	/* Llib/srfi4.scm 838 */
														BgL_tmpz00_5902 = BgL_sstartz00_1810;
													}
												else
													{
														obj_t BgL_auxz00_5905;

														BgL_auxz00_5905 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2368z00zz__srfi4z00, BINT(33208L),
															BGl_string2456z00zz__srfi4z00,
															BGl_string2390z00zz__srfi4z00,
															BgL_sstartz00_1810);
														FAILURE(BgL_auxz00_5905, BFALSE, BFALSE);
													}
												BgL_tmpz00_5901 = (long) CINT(BgL_tmpz00_5902);
											}
											BGL_SU16VECTOR_COPY(BgL_targetz00_2954,
												BgL_tstartz00_2955, BgL_sourcez00_2956, BgL_tmpz00_5901,
												BgL_sendz00_1811);
										}
										return BUNSPEC;
									}
								}
							}
						}
						break;
					case 5L:

						{	/* Llib/srfi4.scm 838 */
							obj_t BgL_sstartz00_1812;

							BgL_sstartz00_1812 = VECTOR_REF(BgL_opt1286z00_172, 3L);
							{	/* Llib/srfi4.scm 838 */
								obj_t BgL_sendz00_1813;

								BgL_sendz00_1813 = VECTOR_REF(BgL_opt1286z00_172, 4L);
								{	/* Llib/srfi4.scm 838 */

									{	/* Llib/srfi4.scm 838 */
										obj_t BgL_targetz00_2957;
										long BgL_tstartz00_2958;
										obj_t BgL_sourcez00_2959;

										if (BGL_S16VECTORP(BgL_targetz00_1803))
											{	/* Llib/srfi4.scm 838 */
												BgL_targetz00_2957 = BgL_targetz00_1803;
											}
										else
											{
												obj_t BgL_auxz00_5915;

												BgL_auxz00_5915 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2456z00zz__srfi4z00,
													BGl_string2374z00zz__srfi4z00, BgL_targetz00_1803);
												FAILURE(BgL_auxz00_5915, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											obj_t BgL_tmpz00_5919;

											if (INTEGERP(BgL_tstartz00_1804))
												{	/* Llib/srfi4.scm 838 */
													BgL_tmpz00_5919 = BgL_tstartz00_1804;
												}
											else
												{
													obj_t BgL_auxz00_5922;

													BgL_auxz00_5922 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2368z00zz__srfi4z00, BINT(33208L),
														BGl_string2456z00zz__srfi4z00,
														BGl_string2390z00zz__srfi4z00, BgL_tstartz00_1804);
													FAILURE(BgL_auxz00_5922, BFALSE, BFALSE);
												}
											BgL_tstartz00_2958 = (long) CINT(BgL_tmpz00_5919);
										}
										if (BGL_S16VECTORP(BgL_sourcez00_1805))
											{	/* Llib/srfi4.scm 838 */
												BgL_sourcez00_2959 = BgL_sourcez00_1805;
											}
										else
											{
												obj_t BgL_auxz00_5929;

												BgL_auxz00_5929 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2456z00zz__srfi4z00,
													BGl_string2374z00zz__srfi4z00, BgL_sourcez00_1805);
												FAILURE(BgL_auxz00_5929, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											long BgL_auxz00_5942;
											long BgL_tmpz00_5933;

											{	/* Llib/srfi4.scm 838 */
												obj_t BgL_tmpz00_5943;

												if (INTEGERP(BgL_sendz00_1813))
													{	/* Llib/srfi4.scm 838 */
														BgL_tmpz00_5943 = BgL_sendz00_1813;
													}
												else
													{
														obj_t BgL_auxz00_5946;

														BgL_auxz00_5946 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2368z00zz__srfi4z00, BINT(33208L),
															BGl_string2456z00zz__srfi4z00,
															BGl_string2390z00zz__srfi4z00, BgL_sendz00_1813);
														FAILURE(BgL_auxz00_5946, BFALSE, BFALSE);
													}
												BgL_auxz00_5942 = (long) CINT(BgL_tmpz00_5943);
											}
											{	/* Llib/srfi4.scm 838 */
												obj_t BgL_tmpz00_5934;

												if (INTEGERP(BgL_sstartz00_1812))
													{	/* Llib/srfi4.scm 838 */
														BgL_tmpz00_5934 = BgL_sstartz00_1812;
													}
												else
													{
														obj_t BgL_auxz00_5937;

														BgL_auxz00_5937 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2368z00zz__srfi4z00, BINT(33208L),
															BGl_string2456z00zz__srfi4z00,
															BGl_string2390z00zz__srfi4z00,
															BgL_sstartz00_1812);
														FAILURE(BgL_auxz00_5937, BFALSE, BFALSE);
													}
												BgL_tmpz00_5933 = (long) CINT(BgL_tmpz00_5934);
											}
											BGL_SU16VECTOR_COPY(BgL_targetz00_2957,
												BgL_tstartz00_2958, BgL_sourcez00_2959, BgL_tmpz00_5933,
												BgL_auxz00_5942);
										}
										return BUNSPEC;
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* s16vector-copy! */
	BGL_EXPORTED_DEF obj_t BGl_s16vectorzd2copyz12zc0zz__srfi4z00(obj_t
		BgL_targetz00_167, long BgL_tstartz00_168, obj_t BgL_sourcez00_169,
		obj_t BgL_sstartz00_170, obj_t BgL_sendz00_171)
	{
		{	/* Llib/srfi4.scm 838 */
			{	/* Llib/srfi4.scm 838 */
				long BgL_auxz00_5956;
				long BgL_tmpz00_5954;

				BgL_auxz00_5956 = (long) CINT(BgL_sendz00_171);
				BgL_tmpz00_5954 = (long) CINT(BgL_sstartz00_170);
				BGL_SU16VECTOR_COPY(BgL_targetz00_167, BgL_tstartz00_168,
					BgL_sourcez00_169, BgL_tmpz00_5954, BgL_auxz00_5956);
			}
			return BUNSPEC;
		}

	}



/* _u16vector-copy! */
	obj_t BGl__u16vectorzd2copyz12zc0zz__srfi4z00(obj_t BgL_env1291z00_180,
		obj_t BgL_opt1290z00_179)
	{
		{	/* Llib/srfi4.scm 838 */
			{	/* Llib/srfi4.scm 838 */
				obj_t BgL_targetz00_1814;
				obj_t BgL_tstartz00_1815;
				obj_t BgL_sourcez00_1816;

				BgL_targetz00_1814 = VECTOR_REF(BgL_opt1290z00_179, 0L);
				BgL_tstartz00_1815 = VECTOR_REF(BgL_opt1290z00_179, 1L);
				BgL_sourcez00_1816 = VECTOR_REF(BgL_opt1290z00_179, 2L);
				switch (VECTOR_LENGTH(BgL_opt1290z00_179))
					{
					case 3L:

						{	/* Llib/srfi4.scm 838 */
							long BgL_sendz00_1820;

							{	/* Llib/srfi4.scm 838 */
								obj_t BgL_xz00_2960;

								if (BGL_U16VECTORP(BgL_sourcez00_1816))
									{	/* Llib/srfi4.scm 838 */
										BgL_xz00_2960 = BgL_sourcez00_1816;
									}
								else
									{
										obj_t BgL_auxz00_5964;

										BgL_auxz00_5964 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2368z00zz__srfi4z00, BINT(33208L),
											BGl_string2457z00zz__srfi4z00,
											BGl_string2376z00zz__srfi4z00, BgL_sourcez00_1816);
										FAILURE(BgL_auxz00_5964, BFALSE, BFALSE);
									}
								BgL_sendz00_1820 = BGL_HVECTOR_LENGTH(BgL_xz00_2960);
							}
							{	/* Llib/srfi4.scm 838 */

								{	/* Llib/srfi4.scm 838 */
									obj_t BgL_targetz00_2961;
									long BgL_tstartz00_2962;
									obj_t BgL_sourcez00_2963;

									if (BGL_U16VECTORP(BgL_targetz00_1814))
										{	/* Llib/srfi4.scm 838 */
											BgL_targetz00_2961 = BgL_targetz00_1814;
										}
									else
										{
											obj_t BgL_auxz00_5971;

											BgL_auxz00_5971 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(33208L),
												BGl_string2457z00zz__srfi4z00,
												BGl_string2376z00zz__srfi4z00, BgL_targetz00_1814);
											FAILURE(BgL_auxz00_5971, BFALSE, BFALSE);
										}
									{	/* Llib/srfi4.scm 838 */
										obj_t BgL_tmpz00_5975;

										if (INTEGERP(BgL_tstartz00_1815))
											{	/* Llib/srfi4.scm 838 */
												BgL_tmpz00_5975 = BgL_tstartz00_1815;
											}
										else
											{
												obj_t BgL_auxz00_5978;

												BgL_auxz00_5978 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2457z00zz__srfi4z00,
													BGl_string2390z00zz__srfi4z00, BgL_tstartz00_1815);
												FAILURE(BgL_auxz00_5978, BFALSE, BFALSE);
											}
										BgL_tstartz00_2962 = (long) CINT(BgL_tmpz00_5975);
									}
									if (BGL_U16VECTORP(BgL_sourcez00_1816))
										{	/* Llib/srfi4.scm 838 */
											BgL_sourcez00_2963 = BgL_sourcez00_1816;
										}
									else
										{
											obj_t BgL_auxz00_5985;

											BgL_auxz00_5985 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(33208L),
												BGl_string2457z00zz__srfi4z00,
												BGl_string2376z00zz__srfi4z00, BgL_sourcez00_1816);
											FAILURE(BgL_auxz00_5985, BFALSE, BFALSE);
										}
									BGL_SU16VECTOR_COPY(BgL_targetz00_2961, BgL_tstartz00_2962,
										BgL_sourcez00_2963, 0L, BgL_sendz00_1820);
									return BUNSPEC;
								}
							}
						}
						break;
					case 4L:

						{	/* Llib/srfi4.scm 838 */
							obj_t BgL_sstartz00_1821;

							BgL_sstartz00_1821 = VECTOR_REF(BgL_opt1290z00_179, 3L);
							{	/* Llib/srfi4.scm 838 */
								long BgL_sendz00_1822;

								{	/* Llib/srfi4.scm 838 */
									obj_t BgL_xz00_2964;

									if (BGL_U16VECTORP(BgL_sourcez00_1816))
										{	/* Llib/srfi4.scm 838 */
											BgL_xz00_2964 = BgL_sourcez00_1816;
										}
									else
										{
											obj_t BgL_auxz00_5993;

											BgL_auxz00_5993 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(33208L),
												BGl_string2457z00zz__srfi4z00,
												BGl_string2376z00zz__srfi4z00, BgL_sourcez00_1816);
											FAILURE(BgL_auxz00_5993, BFALSE, BFALSE);
										}
									BgL_sendz00_1822 = BGL_HVECTOR_LENGTH(BgL_xz00_2964);
								}
								{	/* Llib/srfi4.scm 838 */

									{	/* Llib/srfi4.scm 838 */
										obj_t BgL_targetz00_2965;
										long BgL_tstartz00_2966;
										obj_t BgL_sourcez00_2967;

										if (BGL_U16VECTORP(BgL_targetz00_1814))
											{	/* Llib/srfi4.scm 838 */
												BgL_targetz00_2965 = BgL_targetz00_1814;
											}
										else
											{
												obj_t BgL_auxz00_6000;

												BgL_auxz00_6000 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2457z00zz__srfi4z00,
													BGl_string2376z00zz__srfi4z00, BgL_targetz00_1814);
												FAILURE(BgL_auxz00_6000, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											obj_t BgL_tmpz00_6004;

											if (INTEGERP(BgL_tstartz00_1815))
												{	/* Llib/srfi4.scm 838 */
													BgL_tmpz00_6004 = BgL_tstartz00_1815;
												}
											else
												{
													obj_t BgL_auxz00_6007;

													BgL_auxz00_6007 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2368z00zz__srfi4z00, BINT(33208L),
														BGl_string2457z00zz__srfi4z00,
														BGl_string2390z00zz__srfi4z00, BgL_tstartz00_1815);
													FAILURE(BgL_auxz00_6007, BFALSE, BFALSE);
												}
											BgL_tstartz00_2966 = (long) CINT(BgL_tmpz00_6004);
										}
										if (BGL_U16VECTORP(BgL_sourcez00_1816))
											{	/* Llib/srfi4.scm 838 */
												BgL_sourcez00_2967 = BgL_sourcez00_1816;
											}
										else
											{
												obj_t BgL_auxz00_6014;

												BgL_auxz00_6014 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2457z00zz__srfi4z00,
													BGl_string2376z00zz__srfi4z00, BgL_sourcez00_1816);
												FAILURE(BgL_auxz00_6014, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											long BgL_tmpz00_6018;

											{	/* Llib/srfi4.scm 838 */
												obj_t BgL_tmpz00_6019;

												if (INTEGERP(BgL_sstartz00_1821))
													{	/* Llib/srfi4.scm 838 */
														BgL_tmpz00_6019 = BgL_sstartz00_1821;
													}
												else
													{
														obj_t BgL_auxz00_6022;

														BgL_auxz00_6022 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2368z00zz__srfi4z00, BINT(33208L),
															BGl_string2457z00zz__srfi4z00,
															BGl_string2390z00zz__srfi4z00,
															BgL_sstartz00_1821);
														FAILURE(BgL_auxz00_6022, BFALSE, BFALSE);
													}
												BgL_tmpz00_6018 = (long) CINT(BgL_tmpz00_6019);
											}
											BGL_SU16VECTOR_COPY(BgL_targetz00_2965,
												BgL_tstartz00_2966, BgL_sourcez00_2967, BgL_tmpz00_6018,
												BgL_sendz00_1822);
										}
										return BUNSPEC;
									}
								}
							}
						}
						break;
					case 5L:

						{	/* Llib/srfi4.scm 838 */
							obj_t BgL_sstartz00_1823;

							BgL_sstartz00_1823 = VECTOR_REF(BgL_opt1290z00_179, 3L);
							{	/* Llib/srfi4.scm 838 */
								obj_t BgL_sendz00_1824;

								BgL_sendz00_1824 = VECTOR_REF(BgL_opt1290z00_179, 4L);
								{	/* Llib/srfi4.scm 838 */

									{	/* Llib/srfi4.scm 838 */
										obj_t BgL_targetz00_2968;
										long BgL_tstartz00_2969;
										obj_t BgL_sourcez00_2970;

										if (BGL_U16VECTORP(BgL_targetz00_1814))
											{	/* Llib/srfi4.scm 838 */
												BgL_targetz00_2968 = BgL_targetz00_1814;
											}
										else
											{
												obj_t BgL_auxz00_6032;

												BgL_auxz00_6032 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2457z00zz__srfi4z00,
													BGl_string2376z00zz__srfi4z00, BgL_targetz00_1814);
												FAILURE(BgL_auxz00_6032, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											obj_t BgL_tmpz00_6036;

											if (INTEGERP(BgL_tstartz00_1815))
												{	/* Llib/srfi4.scm 838 */
													BgL_tmpz00_6036 = BgL_tstartz00_1815;
												}
											else
												{
													obj_t BgL_auxz00_6039;

													BgL_auxz00_6039 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2368z00zz__srfi4z00, BINT(33208L),
														BGl_string2457z00zz__srfi4z00,
														BGl_string2390z00zz__srfi4z00, BgL_tstartz00_1815);
													FAILURE(BgL_auxz00_6039, BFALSE, BFALSE);
												}
											BgL_tstartz00_2969 = (long) CINT(BgL_tmpz00_6036);
										}
										if (BGL_U16VECTORP(BgL_sourcez00_1816))
											{	/* Llib/srfi4.scm 838 */
												BgL_sourcez00_2970 = BgL_sourcez00_1816;
											}
										else
											{
												obj_t BgL_auxz00_6046;

												BgL_auxz00_6046 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2457z00zz__srfi4z00,
													BGl_string2376z00zz__srfi4z00, BgL_sourcez00_1816);
												FAILURE(BgL_auxz00_6046, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											long BgL_auxz00_6059;
											long BgL_tmpz00_6050;

											{	/* Llib/srfi4.scm 838 */
												obj_t BgL_tmpz00_6060;

												if (INTEGERP(BgL_sendz00_1824))
													{	/* Llib/srfi4.scm 838 */
														BgL_tmpz00_6060 = BgL_sendz00_1824;
													}
												else
													{
														obj_t BgL_auxz00_6063;

														BgL_auxz00_6063 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2368z00zz__srfi4z00, BINT(33208L),
															BGl_string2457z00zz__srfi4z00,
															BGl_string2390z00zz__srfi4z00, BgL_sendz00_1824);
														FAILURE(BgL_auxz00_6063, BFALSE, BFALSE);
													}
												BgL_auxz00_6059 = (long) CINT(BgL_tmpz00_6060);
											}
											{	/* Llib/srfi4.scm 838 */
												obj_t BgL_tmpz00_6051;

												if (INTEGERP(BgL_sstartz00_1823))
													{	/* Llib/srfi4.scm 838 */
														BgL_tmpz00_6051 = BgL_sstartz00_1823;
													}
												else
													{
														obj_t BgL_auxz00_6054;

														BgL_auxz00_6054 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2368z00zz__srfi4z00, BINT(33208L),
															BGl_string2457z00zz__srfi4z00,
															BGl_string2390z00zz__srfi4z00,
															BgL_sstartz00_1823);
														FAILURE(BgL_auxz00_6054, BFALSE, BFALSE);
													}
												BgL_tmpz00_6050 = (long) CINT(BgL_tmpz00_6051);
											}
											BGL_SU16VECTOR_COPY(BgL_targetz00_2968,
												BgL_tstartz00_2969, BgL_sourcez00_2970, BgL_tmpz00_6050,
												BgL_auxz00_6059);
										}
										return BUNSPEC;
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* u16vector-copy! */
	BGL_EXPORTED_DEF obj_t BGl_u16vectorzd2copyz12zc0zz__srfi4z00(obj_t
		BgL_targetz00_174, long BgL_tstartz00_175, obj_t BgL_sourcez00_176,
		obj_t BgL_sstartz00_177, obj_t BgL_sendz00_178)
	{
		{	/* Llib/srfi4.scm 838 */
			{	/* Llib/srfi4.scm 838 */
				long BgL_auxz00_6073;
				long BgL_tmpz00_6071;

				BgL_auxz00_6073 = (long) CINT(BgL_sendz00_178);
				BgL_tmpz00_6071 = (long) CINT(BgL_sstartz00_177);
				BGL_SU16VECTOR_COPY(BgL_targetz00_174, BgL_tstartz00_175,
					BgL_sourcez00_176, BgL_tmpz00_6071, BgL_auxz00_6073);
			}
			return BUNSPEC;
		}

	}



/* _s32vector-copy! */
	obj_t BGl__s32vectorzd2copyz12zc0zz__srfi4z00(obj_t BgL_env1295z00_187,
		obj_t BgL_opt1294z00_186)
	{
		{	/* Llib/srfi4.scm 838 */
			{	/* Llib/srfi4.scm 838 */
				obj_t BgL_targetz00_1825;
				obj_t BgL_tstartz00_1826;
				obj_t BgL_sourcez00_1827;

				BgL_targetz00_1825 = VECTOR_REF(BgL_opt1294z00_186, 0L);
				BgL_tstartz00_1826 = VECTOR_REF(BgL_opt1294z00_186, 1L);
				BgL_sourcez00_1827 = VECTOR_REF(BgL_opt1294z00_186, 2L);
				switch (VECTOR_LENGTH(BgL_opt1294z00_186))
					{
					case 3L:

						{	/* Llib/srfi4.scm 838 */
							long BgL_sendz00_1831;

							{	/* Llib/srfi4.scm 838 */
								obj_t BgL_xz00_2971;

								if (BGL_S32VECTORP(BgL_sourcez00_1827))
									{	/* Llib/srfi4.scm 838 */
										BgL_xz00_2971 = BgL_sourcez00_1827;
									}
								else
									{
										obj_t BgL_auxz00_6081;

										BgL_auxz00_6081 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2368z00zz__srfi4z00, BINT(33208L),
											BGl_string2458z00zz__srfi4z00,
											BGl_string2378z00zz__srfi4z00, BgL_sourcez00_1827);
										FAILURE(BgL_auxz00_6081, BFALSE, BFALSE);
									}
								BgL_sendz00_1831 = BGL_HVECTOR_LENGTH(BgL_xz00_2971);
							}
							{	/* Llib/srfi4.scm 838 */

								{	/* Llib/srfi4.scm 838 */
									obj_t BgL_targetz00_2972;
									long BgL_tstartz00_2973;
									obj_t BgL_sourcez00_2974;

									if (BGL_S32VECTORP(BgL_targetz00_1825))
										{	/* Llib/srfi4.scm 838 */
											BgL_targetz00_2972 = BgL_targetz00_1825;
										}
									else
										{
											obj_t BgL_auxz00_6088;

											BgL_auxz00_6088 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(33208L),
												BGl_string2458z00zz__srfi4z00,
												BGl_string2378z00zz__srfi4z00, BgL_targetz00_1825);
											FAILURE(BgL_auxz00_6088, BFALSE, BFALSE);
										}
									{	/* Llib/srfi4.scm 838 */
										obj_t BgL_tmpz00_6092;

										if (INTEGERP(BgL_tstartz00_1826))
											{	/* Llib/srfi4.scm 838 */
												BgL_tmpz00_6092 = BgL_tstartz00_1826;
											}
										else
											{
												obj_t BgL_auxz00_6095;

												BgL_auxz00_6095 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2458z00zz__srfi4z00,
													BGl_string2390z00zz__srfi4z00, BgL_tstartz00_1826);
												FAILURE(BgL_auxz00_6095, BFALSE, BFALSE);
											}
										BgL_tstartz00_2973 = (long) CINT(BgL_tmpz00_6092);
									}
									if (BGL_S32VECTORP(BgL_sourcez00_1827))
										{	/* Llib/srfi4.scm 838 */
											BgL_sourcez00_2974 = BgL_sourcez00_1827;
										}
									else
										{
											obj_t BgL_auxz00_6102;

											BgL_auxz00_6102 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(33208L),
												BGl_string2458z00zz__srfi4z00,
												BGl_string2378z00zz__srfi4z00, BgL_sourcez00_1827);
											FAILURE(BgL_auxz00_6102, BFALSE, BFALSE);
										}
									BGL_SU32VECTOR_COPY(BgL_targetz00_2972, BgL_tstartz00_2973,
										BgL_sourcez00_2974, 0L, BgL_sendz00_1831);
									return BUNSPEC;
								}
							}
						}
						break;
					case 4L:

						{	/* Llib/srfi4.scm 838 */
							obj_t BgL_sstartz00_1832;

							BgL_sstartz00_1832 = VECTOR_REF(BgL_opt1294z00_186, 3L);
							{	/* Llib/srfi4.scm 838 */
								long BgL_sendz00_1833;

								{	/* Llib/srfi4.scm 838 */
									obj_t BgL_xz00_2975;

									if (BGL_S32VECTORP(BgL_sourcez00_1827))
										{	/* Llib/srfi4.scm 838 */
											BgL_xz00_2975 = BgL_sourcez00_1827;
										}
									else
										{
											obj_t BgL_auxz00_6110;

											BgL_auxz00_6110 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(33208L),
												BGl_string2458z00zz__srfi4z00,
												BGl_string2378z00zz__srfi4z00, BgL_sourcez00_1827);
											FAILURE(BgL_auxz00_6110, BFALSE, BFALSE);
										}
									BgL_sendz00_1833 = BGL_HVECTOR_LENGTH(BgL_xz00_2975);
								}
								{	/* Llib/srfi4.scm 838 */

									{	/* Llib/srfi4.scm 838 */
										obj_t BgL_targetz00_2976;
										long BgL_tstartz00_2977;
										obj_t BgL_sourcez00_2978;

										if (BGL_S32VECTORP(BgL_targetz00_1825))
											{	/* Llib/srfi4.scm 838 */
												BgL_targetz00_2976 = BgL_targetz00_1825;
											}
										else
											{
												obj_t BgL_auxz00_6117;

												BgL_auxz00_6117 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2458z00zz__srfi4z00,
													BGl_string2378z00zz__srfi4z00, BgL_targetz00_1825);
												FAILURE(BgL_auxz00_6117, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											obj_t BgL_tmpz00_6121;

											if (INTEGERP(BgL_tstartz00_1826))
												{	/* Llib/srfi4.scm 838 */
													BgL_tmpz00_6121 = BgL_tstartz00_1826;
												}
											else
												{
													obj_t BgL_auxz00_6124;

													BgL_auxz00_6124 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2368z00zz__srfi4z00, BINT(33208L),
														BGl_string2458z00zz__srfi4z00,
														BGl_string2390z00zz__srfi4z00, BgL_tstartz00_1826);
													FAILURE(BgL_auxz00_6124, BFALSE, BFALSE);
												}
											BgL_tstartz00_2977 = (long) CINT(BgL_tmpz00_6121);
										}
										if (BGL_S32VECTORP(BgL_sourcez00_1827))
											{	/* Llib/srfi4.scm 838 */
												BgL_sourcez00_2978 = BgL_sourcez00_1827;
											}
										else
											{
												obj_t BgL_auxz00_6131;

												BgL_auxz00_6131 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2458z00zz__srfi4z00,
													BGl_string2378z00zz__srfi4z00, BgL_sourcez00_1827);
												FAILURE(BgL_auxz00_6131, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											long BgL_tmpz00_6135;

											{	/* Llib/srfi4.scm 838 */
												obj_t BgL_tmpz00_6136;

												if (INTEGERP(BgL_sstartz00_1832))
													{	/* Llib/srfi4.scm 838 */
														BgL_tmpz00_6136 = BgL_sstartz00_1832;
													}
												else
													{
														obj_t BgL_auxz00_6139;

														BgL_auxz00_6139 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2368z00zz__srfi4z00, BINT(33208L),
															BGl_string2458z00zz__srfi4z00,
															BGl_string2390z00zz__srfi4z00,
															BgL_sstartz00_1832);
														FAILURE(BgL_auxz00_6139, BFALSE, BFALSE);
													}
												BgL_tmpz00_6135 = (long) CINT(BgL_tmpz00_6136);
											}
											BGL_SU32VECTOR_COPY(BgL_targetz00_2976,
												BgL_tstartz00_2977, BgL_sourcez00_2978, BgL_tmpz00_6135,
												BgL_sendz00_1833);
										}
										return BUNSPEC;
									}
								}
							}
						}
						break;
					case 5L:

						{	/* Llib/srfi4.scm 838 */
							obj_t BgL_sstartz00_1834;

							BgL_sstartz00_1834 = VECTOR_REF(BgL_opt1294z00_186, 3L);
							{	/* Llib/srfi4.scm 838 */
								obj_t BgL_sendz00_1835;

								BgL_sendz00_1835 = VECTOR_REF(BgL_opt1294z00_186, 4L);
								{	/* Llib/srfi4.scm 838 */

									{	/* Llib/srfi4.scm 838 */
										obj_t BgL_targetz00_2979;
										long BgL_tstartz00_2980;
										obj_t BgL_sourcez00_2981;

										if (BGL_S32VECTORP(BgL_targetz00_1825))
											{	/* Llib/srfi4.scm 838 */
												BgL_targetz00_2979 = BgL_targetz00_1825;
											}
										else
											{
												obj_t BgL_auxz00_6149;

												BgL_auxz00_6149 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2458z00zz__srfi4z00,
													BGl_string2378z00zz__srfi4z00, BgL_targetz00_1825);
												FAILURE(BgL_auxz00_6149, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											obj_t BgL_tmpz00_6153;

											if (INTEGERP(BgL_tstartz00_1826))
												{	/* Llib/srfi4.scm 838 */
													BgL_tmpz00_6153 = BgL_tstartz00_1826;
												}
											else
												{
													obj_t BgL_auxz00_6156;

													BgL_auxz00_6156 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2368z00zz__srfi4z00, BINT(33208L),
														BGl_string2458z00zz__srfi4z00,
														BGl_string2390z00zz__srfi4z00, BgL_tstartz00_1826);
													FAILURE(BgL_auxz00_6156, BFALSE, BFALSE);
												}
											BgL_tstartz00_2980 = (long) CINT(BgL_tmpz00_6153);
										}
										if (BGL_S32VECTORP(BgL_sourcez00_1827))
											{	/* Llib/srfi4.scm 838 */
												BgL_sourcez00_2981 = BgL_sourcez00_1827;
											}
										else
											{
												obj_t BgL_auxz00_6163;

												BgL_auxz00_6163 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2458z00zz__srfi4z00,
													BGl_string2378z00zz__srfi4z00, BgL_sourcez00_1827);
												FAILURE(BgL_auxz00_6163, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											long BgL_auxz00_6176;
											long BgL_tmpz00_6167;

											{	/* Llib/srfi4.scm 838 */
												obj_t BgL_tmpz00_6177;

												if (INTEGERP(BgL_sendz00_1835))
													{	/* Llib/srfi4.scm 838 */
														BgL_tmpz00_6177 = BgL_sendz00_1835;
													}
												else
													{
														obj_t BgL_auxz00_6180;

														BgL_auxz00_6180 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2368z00zz__srfi4z00, BINT(33208L),
															BGl_string2458z00zz__srfi4z00,
															BGl_string2390z00zz__srfi4z00, BgL_sendz00_1835);
														FAILURE(BgL_auxz00_6180, BFALSE, BFALSE);
													}
												BgL_auxz00_6176 = (long) CINT(BgL_tmpz00_6177);
											}
											{	/* Llib/srfi4.scm 838 */
												obj_t BgL_tmpz00_6168;

												if (INTEGERP(BgL_sstartz00_1834))
													{	/* Llib/srfi4.scm 838 */
														BgL_tmpz00_6168 = BgL_sstartz00_1834;
													}
												else
													{
														obj_t BgL_auxz00_6171;

														BgL_auxz00_6171 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2368z00zz__srfi4z00, BINT(33208L),
															BGl_string2458z00zz__srfi4z00,
															BGl_string2390z00zz__srfi4z00,
															BgL_sstartz00_1834);
														FAILURE(BgL_auxz00_6171, BFALSE, BFALSE);
													}
												BgL_tmpz00_6167 = (long) CINT(BgL_tmpz00_6168);
											}
											BGL_SU32VECTOR_COPY(BgL_targetz00_2979,
												BgL_tstartz00_2980, BgL_sourcez00_2981, BgL_tmpz00_6167,
												BgL_auxz00_6176);
										}
										return BUNSPEC;
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* s32vector-copy! */
	BGL_EXPORTED_DEF obj_t BGl_s32vectorzd2copyz12zc0zz__srfi4z00(obj_t
		BgL_targetz00_181, long BgL_tstartz00_182, obj_t BgL_sourcez00_183,
		obj_t BgL_sstartz00_184, obj_t BgL_sendz00_185)
	{
		{	/* Llib/srfi4.scm 838 */
			{	/* Llib/srfi4.scm 838 */
				long BgL_auxz00_6190;
				long BgL_tmpz00_6188;

				BgL_auxz00_6190 = (long) CINT(BgL_sendz00_185);
				BgL_tmpz00_6188 = (long) CINT(BgL_sstartz00_184);
				BGL_SU32VECTOR_COPY(BgL_targetz00_181, BgL_tstartz00_182,
					BgL_sourcez00_183, BgL_tmpz00_6188, BgL_auxz00_6190);
			}
			return BUNSPEC;
		}

	}



/* _u32vector-copy! */
	obj_t BGl__u32vectorzd2copyz12zc0zz__srfi4z00(obj_t BgL_env1299z00_194,
		obj_t BgL_opt1298z00_193)
	{
		{	/* Llib/srfi4.scm 838 */
			{	/* Llib/srfi4.scm 838 */
				obj_t BgL_targetz00_1836;
				obj_t BgL_tstartz00_1837;
				obj_t BgL_sourcez00_1838;

				BgL_targetz00_1836 = VECTOR_REF(BgL_opt1298z00_193, 0L);
				BgL_tstartz00_1837 = VECTOR_REF(BgL_opt1298z00_193, 1L);
				BgL_sourcez00_1838 = VECTOR_REF(BgL_opt1298z00_193, 2L);
				switch (VECTOR_LENGTH(BgL_opt1298z00_193))
					{
					case 3L:

						{	/* Llib/srfi4.scm 838 */
							long BgL_sendz00_1842;

							{	/* Llib/srfi4.scm 838 */
								obj_t BgL_xz00_2982;

								if (BGL_U32VECTORP(BgL_sourcez00_1838))
									{	/* Llib/srfi4.scm 838 */
										BgL_xz00_2982 = BgL_sourcez00_1838;
									}
								else
									{
										obj_t BgL_auxz00_6198;

										BgL_auxz00_6198 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2368z00zz__srfi4z00, BINT(33208L),
											BGl_string2459z00zz__srfi4z00,
											BGl_string2380z00zz__srfi4z00, BgL_sourcez00_1838);
										FAILURE(BgL_auxz00_6198, BFALSE, BFALSE);
									}
								BgL_sendz00_1842 = BGL_HVECTOR_LENGTH(BgL_xz00_2982);
							}
							{	/* Llib/srfi4.scm 838 */

								{	/* Llib/srfi4.scm 838 */
									obj_t BgL_targetz00_2983;
									long BgL_tstartz00_2984;
									obj_t BgL_sourcez00_2985;

									if (BGL_U32VECTORP(BgL_targetz00_1836))
										{	/* Llib/srfi4.scm 838 */
											BgL_targetz00_2983 = BgL_targetz00_1836;
										}
									else
										{
											obj_t BgL_auxz00_6205;

											BgL_auxz00_6205 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(33208L),
												BGl_string2459z00zz__srfi4z00,
												BGl_string2380z00zz__srfi4z00, BgL_targetz00_1836);
											FAILURE(BgL_auxz00_6205, BFALSE, BFALSE);
										}
									{	/* Llib/srfi4.scm 838 */
										obj_t BgL_tmpz00_6209;

										if (INTEGERP(BgL_tstartz00_1837))
											{	/* Llib/srfi4.scm 838 */
												BgL_tmpz00_6209 = BgL_tstartz00_1837;
											}
										else
											{
												obj_t BgL_auxz00_6212;

												BgL_auxz00_6212 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2459z00zz__srfi4z00,
													BGl_string2390z00zz__srfi4z00, BgL_tstartz00_1837);
												FAILURE(BgL_auxz00_6212, BFALSE, BFALSE);
											}
										BgL_tstartz00_2984 = (long) CINT(BgL_tmpz00_6209);
									}
									if (BGL_U32VECTORP(BgL_sourcez00_1838))
										{	/* Llib/srfi4.scm 838 */
											BgL_sourcez00_2985 = BgL_sourcez00_1838;
										}
									else
										{
											obj_t BgL_auxz00_6219;

											BgL_auxz00_6219 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(33208L),
												BGl_string2459z00zz__srfi4z00,
												BGl_string2380z00zz__srfi4z00, BgL_sourcez00_1838);
											FAILURE(BgL_auxz00_6219, BFALSE, BFALSE);
										}
									BGL_SU32VECTOR_COPY(BgL_targetz00_2983, BgL_tstartz00_2984,
										BgL_sourcez00_2985, 0L, BgL_sendz00_1842);
									return BUNSPEC;
								}
							}
						}
						break;
					case 4L:

						{	/* Llib/srfi4.scm 838 */
							obj_t BgL_sstartz00_1843;

							BgL_sstartz00_1843 = VECTOR_REF(BgL_opt1298z00_193, 3L);
							{	/* Llib/srfi4.scm 838 */
								long BgL_sendz00_1844;

								{	/* Llib/srfi4.scm 838 */
									obj_t BgL_xz00_2986;

									if (BGL_U32VECTORP(BgL_sourcez00_1838))
										{	/* Llib/srfi4.scm 838 */
											BgL_xz00_2986 = BgL_sourcez00_1838;
										}
									else
										{
											obj_t BgL_auxz00_6227;

											BgL_auxz00_6227 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(33208L),
												BGl_string2459z00zz__srfi4z00,
												BGl_string2380z00zz__srfi4z00, BgL_sourcez00_1838);
											FAILURE(BgL_auxz00_6227, BFALSE, BFALSE);
										}
									BgL_sendz00_1844 = BGL_HVECTOR_LENGTH(BgL_xz00_2986);
								}
								{	/* Llib/srfi4.scm 838 */

									{	/* Llib/srfi4.scm 838 */
										obj_t BgL_targetz00_2987;
										long BgL_tstartz00_2988;
										obj_t BgL_sourcez00_2989;

										if (BGL_U32VECTORP(BgL_targetz00_1836))
											{	/* Llib/srfi4.scm 838 */
												BgL_targetz00_2987 = BgL_targetz00_1836;
											}
										else
											{
												obj_t BgL_auxz00_6234;

												BgL_auxz00_6234 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2459z00zz__srfi4z00,
													BGl_string2380z00zz__srfi4z00, BgL_targetz00_1836);
												FAILURE(BgL_auxz00_6234, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											obj_t BgL_tmpz00_6238;

											if (INTEGERP(BgL_tstartz00_1837))
												{	/* Llib/srfi4.scm 838 */
													BgL_tmpz00_6238 = BgL_tstartz00_1837;
												}
											else
												{
													obj_t BgL_auxz00_6241;

													BgL_auxz00_6241 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2368z00zz__srfi4z00, BINT(33208L),
														BGl_string2459z00zz__srfi4z00,
														BGl_string2390z00zz__srfi4z00, BgL_tstartz00_1837);
													FAILURE(BgL_auxz00_6241, BFALSE, BFALSE);
												}
											BgL_tstartz00_2988 = (long) CINT(BgL_tmpz00_6238);
										}
										if (BGL_U32VECTORP(BgL_sourcez00_1838))
											{	/* Llib/srfi4.scm 838 */
												BgL_sourcez00_2989 = BgL_sourcez00_1838;
											}
										else
											{
												obj_t BgL_auxz00_6248;

												BgL_auxz00_6248 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2459z00zz__srfi4z00,
													BGl_string2380z00zz__srfi4z00, BgL_sourcez00_1838);
												FAILURE(BgL_auxz00_6248, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											long BgL_tmpz00_6252;

											{	/* Llib/srfi4.scm 838 */
												obj_t BgL_tmpz00_6253;

												if (INTEGERP(BgL_sstartz00_1843))
													{	/* Llib/srfi4.scm 838 */
														BgL_tmpz00_6253 = BgL_sstartz00_1843;
													}
												else
													{
														obj_t BgL_auxz00_6256;

														BgL_auxz00_6256 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2368z00zz__srfi4z00, BINT(33208L),
															BGl_string2459z00zz__srfi4z00,
															BGl_string2390z00zz__srfi4z00,
															BgL_sstartz00_1843);
														FAILURE(BgL_auxz00_6256, BFALSE, BFALSE);
													}
												BgL_tmpz00_6252 = (long) CINT(BgL_tmpz00_6253);
											}
											BGL_SU32VECTOR_COPY(BgL_targetz00_2987,
												BgL_tstartz00_2988, BgL_sourcez00_2989, BgL_tmpz00_6252,
												BgL_sendz00_1844);
										}
										return BUNSPEC;
									}
								}
							}
						}
						break;
					case 5L:

						{	/* Llib/srfi4.scm 838 */
							obj_t BgL_sstartz00_1845;

							BgL_sstartz00_1845 = VECTOR_REF(BgL_opt1298z00_193, 3L);
							{	/* Llib/srfi4.scm 838 */
								obj_t BgL_sendz00_1846;

								BgL_sendz00_1846 = VECTOR_REF(BgL_opt1298z00_193, 4L);
								{	/* Llib/srfi4.scm 838 */

									{	/* Llib/srfi4.scm 838 */
										obj_t BgL_targetz00_2990;
										long BgL_tstartz00_2991;
										obj_t BgL_sourcez00_2992;

										if (BGL_U32VECTORP(BgL_targetz00_1836))
											{	/* Llib/srfi4.scm 838 */
												BgL_targetz00_2990 = BgL_targetz00_1836;
											}
										else
											{
												obj_t BgL_auxz00_6266;

												BgL_auxz00_6266 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2459z00zz__srfi4z00,
													BGl_string2380z00zz__srfi4z00, BgL_targetz00_1836);
												FAILURE(BgL_auxz00_6266, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											obj_t BgL_tmpz00_6270;

											if (INTEGERP(BgL_tstartz00_1837))
												{	/* Llib/srfi4.scm 838 */
													BgL_tmpz00_6270 = BgL_tstartz00_1837;
												}
											else
												{
													obj_t BgL_auxz00_6273;

													BgL_auxz00_6273 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2368z00zz__srfi4z00, BINT(33208L),
														BGl_string2459z00zz__srfi4z00,
														BGl_string2390z00zz__srfi4z00, BgL_tstartz00_1837);
													FAILURE(BgL_auxz00_6273, BFALSE, BFALSE);
												}
											BgL_tstartz00_2991 = (long) CINT(BgL_tmpz00_6270);
										}
										if (BGL_U32VECTORP(BgL_sourcez00_1838))
											{	/* Llib/srfi4.scm 838 */
												BgL_sourcez00_2992 = BgL_sourcez00_1838;
											}
										else
											{
												obj_t BgL_auxz00_6280;

												BgL_auxz00_6280 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2459z00zz__srfi4z00,
													BGl_string2380z00zz__srfi4z00, BgL_sourcez00_1838);
												FAILURE(BgL_auxz00_6280, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											long BgL_auxz00_6293;
											long BgL_tmpz00_6284;

											{	/* Llib/srfi4.scm 838 */
												obj_t BgL_tmpz00_6294;

												if (INTEGERP(BgL_sendz00_1846))
													{	/* Llib/srfi4.scm 838 */
														BgL_tmpz00_6294 = BgL_sendz00_1846;
													}
												else
													{
														obj_t BgL_auxz00_6297;

														BgL_auxz00_6297 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2368z00zz__srfi4z00, BINT(33208L),
															BGl_string2459z00zz__srfi4z00,
															BGl_string2390z00zz__srfi4z00, BgL_sendz00_1846);
														FAILURE(BgL_auxz00_6297, BFALSE, BFALSE);
													}
												BgL_auxz00_6293 = (long) CINT(BgL_tmpz00_6294);
											}
											{	/* Llib/srfi4.scm 838 */
												obj_t BgL_tmpz00_6285;

												if (INTEGERP(BgL_sstartz00_1845))
													{	/* Llib/srfi4.scm 838 */
														BgL_tmpz00_6285 = BgL_sstartz00_1845;
													}
												else
													{
														obj_t BgL_auxz00_6288;

														BgL_auxz00_6288 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2368z00zz__srfi4z00, BINT(33208L),
															BGl_string2459z00zz__srfi4z00,
															BGl_string2390z00zz__srfi4z00,
															BgL_sstartz00_1845);
														FAILURE(BgL_auxz00_6288, BFALSE, BFALSE);
													}
												BgL_tmpz00_6284 = (long) CINT(BgL_tmpz00_6285);
											}
											BGL_SU32VECTOR_COPY(BgL_targetz00_2990,
												BgL_tstartz00_2991, BgL_sourcez00_2992, BgL_tmpz00_6284,
												BgL_auxz00_6293);
										}
										return BUNSPEC;
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* u32vector-copy! */
	BGL_EXPORTED_DEF obj_t BGl_u32vectorzd2copyz12zc0zz__srfi4z00(obj_t
		BgL_targetz00_188, long BgL_tstartz00_189, obj_t BgL_sourcez00_190,
		obj_t BgL_sstartz00_191, obj_t BgL_sendz00_192)
	{
		{	/* Llib/srfi4.scm 838 */
			{	/* Llib/srfi4.scm 838 */
				long BgL_auxz00_6307;
				long BgL_tmpz00_6305;

				BgL_auxz00_6307 = (long) CINT(BgL_sendz00_192);
				BgL_tmpz00_6305 = (long) CINT(BgL_sstartz00_191);
				BGL_SU32VECTOR_COPY(BgL_targetz00_188, BgL_tstartz00_189,
					BgL_sourcez00_190, BgL_tmpz00_6305, BgL_auxz00_6307);
			}
			return BUNSPEC;
		}

	}



/* _s64vector-copy! */
	obj_t BGl__s64vectorzd2copyz12zc0zz__srfi4z00(obj_t BgL_env1303z00_201,
		obj_t BgL_opt1302z00_200)
	{
		{	/* Llib/srfi4.scm 838 */
			{	/* Llib/srfi4.scm 838 */
				obj_t BgL_targetz00_1847;
				obj_t BgL_tstartz00_1848;
				obj_t BgL_sourcez00_1849;

				BgL_targetz00_1847 = VECTOR_REF(BgL_opt1302z00_200, 0L);
				BgL_tstartz00_1848 = VECTOR_REF(BgL_opt1302z00_200, 1L);
				BgL_sourcez00_1849 = VECTOR_REF(BgL_opt1302z00_200, 2L);
				switch (VECTOR_LENGTH(BgL_opt1302z00_200))
					{
					case 3L:

						{	/* Llib/srfi4.scm 838 */
							long BgL_sendz00_1853;

							{	/* Llib/srfi4.scm 838 */
								obj_t BgL_xz00_2993;

								if (BGL_S64VECTORP(BgL_sourcez00_1849))
									{	/* Llib/srfi4.scm 838 */
										BgL_xz00_2993 = BgL_sourcez00_1849;
									}
								else
									{
										obj_t BgL_auxz00_6315;

										BgL_auxz00_6315 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2368z00zz__srfi4z00, BINT(33208L),
											BGl_string2460z00zz__srfi4z00,
											BGl_string2382z00zz__srfi4z00, BgL_sourcez00_1849);
										FAILURE(BgL_auxz00_6315, BFALSE, BFALSE);
									}
								BgL_sendz00_1853 = BGL_HVECTOR_LENGTH(BgL_xz00_2993);
							}
							{	/* Llib/srfi4.scm 838 */

								{	/* Llib/srfi4.scm 838 */
									obj_t BgL_targetz00_2994;
									long BgL_tstartz00_2995;
									obj_t BgL_sourcez00_2996;

									if (BGL_S64VECTORP(BgL_targetz00_1847))
										{	/* Llib/srfi4.scm 838 */
											BgL_targetz00_2994 = BgL_targetz00_1847;
										}
									else
										{
											obj_t BgL_auxz00_6322;

											BgL_auxz00_6322 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(33208L),
												BGl_string2460z00zz__srfi4z00,
												BGl_string2382z00zz__srfi4z00, BgL_targetz00_1847);
											FAILURE(BgL_auxz00_6322, BFALSE, BFALSE);
										}
									{	/* Llib/srfi4.scm 838 */
										obj_t BgL_tmpz00_6326;

										if (INTEGERP(BgL_tstartz00_1848))
											{	/* Llib/srfi4.scm 838 */
												BgL_tmpz00_6326 = BgL_tstartz00_1848;
											}
										else
											{
												obj_t BgL_auxz00_6329;

												BgL_auxz00_6329 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2460z00zz__srfi4z00,
													BGl_string2390z00zz__srfi4z00, BgL_tstartz00_1848);
												FAILURE(BgL_auxz00_6329, BFALSE, BFALSE);
											}
										BgL_tstartz00_2995 = (long) CINT(BgL_tmpz00_6326);
									}
									if (BGL_S64VECTORP(BgL_sourcez00_1849))
										{	/* Llib/srfi4.scm 838 */
											BgL_sourcez00_2996 = BgL_sourcez00_1849;
										}
									else
										{
											obj_t BgL_auxz00_6336;

											BgL_auxz00_6336 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(33208L),
												BGl_string2460z00zz__srfi4z00,
												BGl_string2382z00zz__srfi4z00, BgL_sourcez00_1849);
											FAILURE(BgL_auxz00_6336, BFALSE, BFALSE);
										}
									BGL_SU64VECTOR_COPY(BgL_targetz00_2994, BgL_tstartz00_2995,
										BgL_sourcez00_2996, 0L, BgL_sendz00_1853);
									return BUNSPEC;
								}
							}
						}
						break;
					case 4L:

						{	/* Llib/srfi4.scm 838 */
							obj_t BgL_sstartz00_1854;

							BgL_sstartz00_1854 = VECTOR_REF(BgL_opt1302z00_200, 3L);
							{	/* Llib/srfi4.scm 838 */
								long BgL_sendz00_1855;

								{	/* Llib/srfi4.scm 838 */
									obj_t BgL_xz00_2997;

									if (BGL_S64VECTORP(BgL_sourcez00_1849))
										{	/* Llib/srfi4.scm 838 */
											BgL_xz00_2997 = BgL_sourcez00_1849;
										}
									else
										{
											obj_t BgL_auxz00_6344;

											BgL_auxz00_6344 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(33208L),
												BGl_string2460z00zz__srfi4z00,
												BGl_string2382z00zz__srfi4z00, BgL_sourcez00_1849);
											FAILURE(BgL_auxz00_6344, BFALSE, BFALSE);
										}
									BgL_sendz00_1855 = BGL_HVECTOR_LENGTH(BgL_xz00_2997);
								}
								{	/* Llib/srfi4.scm 838 */

									{	/* Llib/srfi4.scm 838 */
										obj_t BgL_targetz00_2998;
										long BgL_tstartz00_2999;
										obj_t BgL_sourcez00_3000;

										if (BGL_S64VECTORP(BgL_targetz00_1847))
											{	/* Llib/srfi4.scm 838 */
												BgL_targetz00_2998 = BgL_targetz00_1847;
											}
										else
											{
												obj_t BgL_auxz00_6351;

												BgL_auxz00_6351 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2460z00zz__srfi4z00,
													BGl_string2382z00zz__srfi4z00, BgL_targetz00_1847);
												FAILURE(BgL_auxz00_6351, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											obj_t BgL_tmpz00_6355;

											if (INTEGERP(BgL_tstartz00_1848))
												{	/* Llib/srfi4.scm 838 */
													BgL_tmpz00_6355 = BgL_tstartz00_1848;
												}
											else
												{
													obj_t BgL_auxz00_6358;

													BgL_auxz00_6358 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2368z00zz__srfi4z00, BINT(33208L),
														BGl_string2460z00zz__srfi4z00,
														BGl_string2390z00zz__srfi4z00, BgL_tstartz00_1848);
													FAILURE(BgL_auxz00_6358, BFALSE, BFALSE);
												}
											BgL_tstartz00_2999 = (long) CINT(BgL_tmpz00_6355);
										}
										if (BGL_S64VECTORP(BgL_sourcez00_1849))
											{	/* Llib/srfi4.scm 838 */
												BgL_sourcez00_3000 = BgL_sourcez00_1849;
											}
										else
											{
												obj_t BgL_auxz00_6365;

												BgL_auxz00_6365 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2460z00zz__srfi4z00,
													BGl_string2382z00zz__srfi4z00, BgL_sourcez00_1849);
												FAILURE(BgL_auxz00_6365, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											long BgL_tmpz00_6369;

											{	/* Llib/srfi4.scm 838 */
												obj_t BgL_tmpz00_6370;

												if (INTEGERP(BgL_sstartz00_1854))
													{	/* Llib/srfi4.scm 838 */
														BgL_tmpz00_6370 = BgL_sstartz00_1854;
													}
												else
													{
														obj_t BgL_auxz00_6373;

														BgL_auxz00_6373 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2368z00zz__srfi4z00, BINT(33208L),
															BGl_string2460z00zz__srfi4z00,
															BGl_string2390z00zz__srfi4z00,
															BgL_sstartz00_1854);
														FAILURE(BgL_auxz00_6373, BFALSE, BFALSE);
													}
												BgL_tmpz00_6369 = (long) CINT(BgL_tmpz00_6370);
											}
											BGL_SU64VECTOR_COPY(BgL_targetz00_2998,
												BgL_tstartz00_2999, BgL_sourcez00_3000, BgL_tmpz00_6369,
												BgL_sendz00_1855);
										}
										return BUNSPEC;
									}
								}
							}
						}
						break;
					case 5L:

						{	/* Llib/srfi4.scm 838 */
							obj_t BgL_sstartz00_1856;

							BgL_sstartz00_1856 = VECTOR_REF(BgL_opt1302z00_200, 3L);
							{	/* Llib/srfi4.scm 838 */
								obj_t BgL_sendz00_1857;

								BgL_sendz00_1857 = VECTOR_REF(BgL_opt1302z00_200, 4L);
								{	/* Llib/srfi4.scm 838 */

									{	/* Llib/srfi4.scm 838 */
										obj_t BgL_targetz00_3001;
										long BgL_tstartz00_3002;
										obj_t BgL_sourcez00_3003;

										if (BGL_S64VECTORP(BgL_targetz00_1847))
											{	/* Llib/srfi4.scm 838 */
												BgL_targetz00_3001 = BgL_targetz00_1847;
											}
										else
											{
												obj_t BgL_auxz00_6383;

												BgL_auxz00_6383 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2460z00zz__srfi4z00,
													BGl_string2382z00zz__srfi4z00, BgL_targetz00_1847);
												FAILURE(BgL_auxz00_6383, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											obj_t BgL_tmpz00_6387;

											if (INTEGERP(BgL_tstartz00_1848))
												{	/* Llib/srfi4.scm 838 */
													BgL_tmpz00_6387 = BgL_tstartz00_1848;
												}
											else
												{
													obj_t BgL_auxz00_6390;

													BgL_auxz00_6390 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2368z00zz__srfi4z00, BINT(33208L),
														BGl_string2460z00zz__srfi4z00,
														BGl_string2390z00zz__srfi4z00, BgL_tstartz00_1848);
													FAILURE(BgL_auxz00_6390, BFALSE, BFALSE);
												}
											BgL_tstartz00_3002 = (long) CINT(BgL_tmpz00_6387);
										}
										if (BGL_S64VECTORP(BgL_sourcez00_1849))
											{	/* Llib/srfi4.scm 838 */
												BgL_sourcez00_3003 = BgL_sourcez00_1849;
											}
										else
											{
												obj_t BgL_auxz00_6397;

												BgL_auxz00_6397 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2460z00zz__srfi4z00,
													BGl_string2382z00zz__srfi4z00, BgL_sourcez00_1849);
												FAILURE(BgL_auxz00_6397, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											long BgL_auxz00_6410;
											long BgL_tmpz00_6401;

											{	/* Llib/srfi4.scm 838 */
												obj_t BgL_tmpz00_6411;

												if (INTEGERP(BgL_sendz00_1857))
													{	/* Llib/srfi4.scm 838 */
														BgL_tmpz00_6411 = BgL_sendz00_1857;
													}
												else
													{
														obj_t BgL_auxz00_6414;

														BgL_auxz00_6414 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2368z00zz__srfi4z00, BINT(33208L),
															BGl_string2460z00zz__srfi4z00,
															BGl_string2390z00zz__srfi4z00, BgL_sendz00_1857);
														FAILURE(BgL_auxz00_6414, BFALSE, BFALSE);
													}
												BgL_auxz00_6410 = (long) CINT(BgL_tmpz00_6411);
											}
											{	/* Llib/srfi4.scm 838 */
												obj_t BgL_tmpz00_6402;

												if (INTEGERP(BgL_sstartz00_1856))
													{	/* Llib/srfi4.scm 838 */
														BgL_tmpz00_6402 = BgL_sstartz00_1856;
													}
												else
													{
														obj_t BgL_auxz00_6405;

														BgL_auxz00_6405 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2368z00zz__srfi4z00, BINT(33208L),
															BGl_string2460z00zz__srfi4z00,
															BGl_string2390z00zz__srfi4z00,
															BgL_sstartz00_1856);
														FAILURE(BgL_auxz00_6405, BFALSE, BFALSE);
													}
												BgL_tmpz00_6401 = (long) CINT(BgL_tmpz00_6402);
											}
											BGL_SU64VECTOR_COPY(BgL_targetz00_3001,
												BgL_tstartz00_3002, BgL_sourcez00_3003, BgL_tmpz00_6401,
												BgL_auxz00_6410);
										}
										return BUNSPEC;
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* s64vector-copy! */
	BGL_EXPORTED_DEF obj_t BGl_s64vectorzd2copyz12zc0zz__srfi4z00(obj_t
		BgL_targetz00_195, long BgL_tstartz00_196, obj_t BgL_sourcez00_197,
		obj_t BgL_sstartz00_198, obj_t BgL_sendz00_199)
	{
		{	/* Llib/srfi4.scm 838 */
			{	/* Llib/srfi4.scm 838 */
				long BgL_auxz00_6424;
				long BgL_tmpz00_6422;

				BgL_auxz00_6424 = (long) CINT(BgL_sendz00_199);
				BgL_tmpz00_6422 = (long) CINT(BgL_sstartz00_198);
				BGL_SU64VECTOR_COPY(BgL_targetz00_195, BgL_tstartz00_196,
					BgL_sourcez00_197, BgL_tmpz00_6422, BgL_auxz00_6424);
			}
			return BUNSPEC;
		}

	}



/* _u64vector-copy! */
	obj_t BGl__u64vectorzd2copyz12zc0zz__srfi4z00(obj_t BgL_env1307z00_208,
		obj_t BgL_opt1306z00_207)
	{
		{	/* Llib/srfi4.scm 838 */
			{	/* Llib/srfi4.scm 838 */
				obj_t BgL_targetz00_1858;
				obj_t BgL_tstartz00_1859;
				obj_t BgL_sourcez00_1860;

				BgL_targetz00_1858 = VECTOR_REF(BgL_opt1306z00_207, 0L);
				BgL_tstartz00_1859 = VECTOR_REF(BgL_opt1306z00_207, 1L);
				BgL_sourcez00_1860 = VECTOR_REF(BgL_opt1306z00_207, 2L);
				switch (VECTOR_LENGTH(BgL_opt1306z00_207))
					{
					case 3L:

						{	/* Llib/srfi4.scm 838 */
							long BgL_sendz00_1864;

							{	/* Llib/srfi4.scm 838 */
								obj_t BgL_xz00_3004;

								if (BGL_U64VECTORP(BgL_sourcez00_1860))
									{	/* Llib/srfi4.scm 838 */
										BgL_xz00_3004 = BgL_sourcez00_1860;
									}
								else
									{
										obj_t BgL_auxz00_6432;

										BgL_auxz00_6432 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2368z00zz__srfi4z00, BINT(33208L),
											BGl_string2461z00zz__srfi4z00,
											BGl_string2384z00zz__srfi4z00, BgL_sourcez00_1860);
										FAILURE(BgL_auxz00_6432, BFALSE, BFALSE);
									}
								BgL_sendz00_1864 = BGL_HVECTOR_LENGTH(BgL_xz00_3004);
							}
							{	/* Llib/srfi4.scm 838 */

								{	/* Llib/srfi4.scm 838 */
									obj_t BgL_targetz00_3005;
									long BgL_tstartz00_3006;
									obj_t BgL_sourcez00_3007;

									if (BGL_U64VECTORP(BgL_targetz00_1858))
										{	/* Llib/srfi4.scm 838 */
											BgL_targetz00_3005 = BgL_targetz00_1858;
										}
									else
										{
											obj_t BgL_auxz00_6439;

											BgL_auxz00_6439 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(33208L),
												BGl_string2461z00zz__srfi4z00,
												BGl_string2384z00zz__srfi4z00, BgL_targetz00_1858);
											FAILURE(BgL_auxz00_6439, BFALSE, BFALSE);
										}
									{	/* Llib/srfi4.scm 838 */
										obj_t BgL_tmpz00_6443;

										if (INTEGERP(BgL_tstartz00_1859))
											{	/* Llib/srfi4.scm 838 */
												BgL_tmpz00_6443 = BgL_tstartz00_1859;
											}
										else
											{
												obj_t BgL_auxz00_6446;

												BgL_auxz00_6446 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2461z00zz__srfi4z00,
													BGl_string2390z00zz__srfi4z00, BgL_tstartz00_1859);
												FAILURE(BgL_auxz00_6446, BFALSE, BFALSE);
											}
										BgL_tstartz00_3006 = (long) CINT(BgL_tmpz00_6443);
									}
									if (BGL_U64VECTORP(BgL_sourcez00_1860))
										{	/* Llib/srfi4.scm 838 */
											BgL_sourcez00_3007 = BgL_sourcez00_1860;
										}
									else
										{
											obj_t BgL_auxz00_6453;

											BgL_auxz00_6453 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(33208L),
												BGl_string2461z00zz__srfi4z00,
												BGl_string2384z00zz__srfi4z00, BgL_sourcez00_1860);
											FAILURE(BgL_auxz00_6453, BFALSE, BFALSE);
										}
									BGL_SU64VECTOR_COPY(BgL_targetz00_3005, BgL_tstartz00_3006,
										BgL_sourcez00_3007, 0L, BgL_sendz00_1864);
									return BUNSPEC;
								}
							}
						}
						break;
					case 4L:

						{	/* Llib/srfi4.scm 838 */
							obj_t BgL_sstartz00_1865;

							BgL_sstartz00_1865 = VECTOR_REF(BgL_opt1306z00_207, 3L);
							{	/* Llib/srfi4.scm 838 */
								long BgL_sendz00_1866;

								{	/* Llib/srfi4.scm 838 */
									obj_t BgL_xz00_3008;

									if (BGL_U64VECTORP(BgL_sourcez00_1860))
										{	/* Llib/srfi4.scm 838 */
											BgL_xz00_3008 = BgL_sourcez00_1860;
										}
									else
										{
											obj_t BgL_auxz00_6461;

											BgL_auxz00_6461 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(33208L),
												BGl_string2461z00zz__srfi4z00,
												BGl_string2384z00zz__srfi4z00, BgL_sourcez00_1860);
											FAILURE(BgL_auxz00_6461, BFALSE, BFALSE);
										}
									BgL_sendz00_1866 = BGL_HVECTOR_LENGTH(BgL_xz00_3008);
								}
								{	/* Llib/srfi4.scm 838 */

									{	/* Llib/srfi4.scm 838 */
										obj_t BgL_targetz00_3009;
										long BgL_tstartz00_3010;
										obj_t BgL_sourcez00_3011;

										if (BGL_U64VECTORP(BgL_targetz00_1858))
											{	/* Llib/srfi4.scm 838 */
												BgL_targetz00_3009 = BgL_targetz00_1858;
											}
										else
											{
												obj_t BgL_auxz00_6468;

												BgL_auxz00_6468 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2461z00zz__srfi4z00,
													BGl_string2384z00zz__srfi4z00, BgL_targetz00_1858);
												FAILURE(BgL_auxz00_6468, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											obj_t BgL_tmpz00_6472;

											if (INTEGERP(BgL_tstartz00_1859))
												{	/* Llib/srfi4.scm 838 */
													BgL_tmpz00_6472 = BgL_tstartz00_1859;
												}
											else
												{
													obj_t BgL_auxz00_6475;

													BgL_auxz00_6475 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2368z00zz__srfi4z00, BINT(33208L),
														BGl_string2461z00zz__srfi4z00,
														BGl_string2390z00zz__srfi4z00, BgL_tstartz00_1859);
													FAILURE(BgL_auxz00_6475, BFALSE, BFALSE);
												}
											BgL_tstartz00_3010 = (long) CINT(BgL_tmpz00_6472);
										}
										if (BGL_U64VECTORP(BgL_sourcez00_1860))
											{	/* Llib/srfi4.scm 838 */
												BgL_sourcez00_3011 = BgL_sourcez00_1860;
											}
										else
											{
												obj_t BgL_auxz00_6482;

												BgL_auxz00_6482 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2461z00zz__srfi4z00,
													BGl_string2384z00zz__srfi4z00, BgL_sourcez00_1860);
												FAILURE(BgL_auxz00_6482, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											long BgL_tmpz00_6486;

											{	/* Llib/srfi4.scm 838 */
												obj_t BgL_tmpz00_6487;

												if (INTEGERP(BgL_sstartz00_1865))
													{	/* Llib/srfi4.scm 838 */
														BgL_tmpz00_6487 = BgL_sstartz00_1865;
													}
												else
													{
														obj_t BgL_auxz00_6490;

														BgL_auxz00_6490 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2368z00zz__srfi4z00, BINT(33208L),
															BGl_string2461z00zz__srfi4z00,
															BGl_string2390z00zz__srfi4z00,
															BgL_sstartz00_1865);
														FAILURE(BgL_auxz00_6490, BFALSE, BFALSE);
													}
												BgL_tmpz00_6486 = (long) CINT(BgL_tmpz00_6487);
											}
											BGL_SU64VECTOR_COPY(BgL_targetz00_3009,
												BgL_tstartz00_3010, BgL_sourcez00_3011, BgL_tmpz00_6486,
												BgL_sendz00_1866);
										}
										return BUNSPEC;
									}
								}
							}
						}
						break;
					case 5L:

						{	/* Llib/srfi4.scm 838 */
							obj_t BgL_sstartz00_1867;

							BgL_sstartz00_1867 = VECTOR_REF(BgL_opt1306z00_207, 3L);
							{	/* Llib/srfi4.scm 838 */
								obj_t BgL_sendz00_1868;

								BgL_sendz00_1868 = VECTOR_REF(BgL_opt1306z00_207, 4L);
								{	/* Llib/srfi4.scm 838 */

									{	/* Llib/srfi4.scm 838 */
										obj_t BgL_targetz00_3012;
										long BgL_tstartz00_3013;
										obj_t BgL_sourcez00_3014;

										if (BGL_U64VECTORP(BgL_targetz00_1858))
											{	/* Llib/srfi4.scm 838 */
												BgL_targetz00_3012 = BgL_targetz00_1858;
											}
										else
											{
												obj_t BgL_auxz00_6500;

												BgL_auxz00_6500 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2461z00zz__srfi4z00,
													BGl_string2384z00zz__srfi4z00, BgL_targetz00_1858);
												FAILURE(BgL_auxz00_6500, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											obj_t BgL_tmpz00_6504;

											if (INTEGERP(BgL_tstartz00_1859))
												{	/* Llib/srfi4.scm 838 */
													BgL_tmpz00_6504 = BgL_tstartz00_1859;
												}
											else
												{
													obj_t BgL_auxz00_6507;

													BgL_auxz00_6507 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2368z00zz__srfi4z00, BINT(33208L),
														BGl_string2461z00zz__srfi4z00,
														BGl_string2390z00zz__srfi4z00, BgL_tstartz00_1859);
													FAILURE(BgL_auxz00_6507, BFALSE, BFALSE);
												}
											BgL_tstartz00_3013 = (long) CINT(BgL_tmpz00_6504);
										}
										if (BGL_U64VECTORP(BgL_sourcez00_1860))
											{	/* Llib/srfi4.scm 838 */
												BgL_sourcez00_3014 = BgL_sourcez00_1860;
											}
										else
											{
												obj_t BgL_auxz00_6514;

												BgL_auxz00_6514 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2461z00zz__srfi4z00,
													BGl_string2384z00zz__srfi4z00, BgL_sourcez00_1860);
												FAILURE(BgL_auxz00_6514, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											long BgL_auxz00_6527;
											long BgL_tmpz00_6518;

											{	/* Llib/srfi4.scm 838 */
												obj_t BgL_tmpz00_6528;

												if (INTEGERP(BgL_sendz00_1868))
													{	/* Llib/srfi4.scm 838 */
														BgL_tmpz00_6528 = BgL_sendz00_1868;
													}
												else
													{
														obj_t BgL_auxz00_6531;

														BgL_auxz00_6531 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2368z00zz__srfi4z00, BINT(33208L),
															BGl_string2461z00zz__srfi4z00,
															BGl_string2390z00zz__srfi4z00, BgL_sendz00_1868);
														FAILURE(BgL_auxz00_6531, BFALSE, BFALSE);
													}
												BgL_auxz00_6527 = (long) CINT(BgL_tmpz00_6528);
											}
											{	/* Llib/srfi4.scm 838 */
												obj_t BgL_tmpz00_6519;

												if (INTEGERP(BgL_sstartz00_1867))
													{	/* Llib/srfi4.scm 838 */
														BgL_tmpz00_6519 = BgL_sstartz00_1867;
													}
												else
													{
														obj_t BgL_auxz00_6522;

														BgL_auxz00_6522 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2368z00zz__srfi4z00, BINT(33208L),
															BGl_string2461z00zz__srfi4z00,
															BGl_string2390z00zz__srfi4z00,
															BgL_sstartz00_1867);
														FAILURE(BgL_auxz00_6522, BFALSE, BFALSE);
													}
												BgL_tmpz00_6518 = (long) CINT(BgL_tmpz00_6519);
											}
											BGL_SU64VECTOR_COPY(BgL_targetz00_3012,
												BgL_tstartz00_3013, BgL_sourcez00_3014, BgL_tmpz00_6518,
												BgL_auxz00_6527);
										}
										return BUNSPEC;
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* u64vector-copy! */
	BGL_EXPORTED_DEF obj_t BGl_u64vectorzd2copyz12zc0zz__srfi4z00(obj_t
		BgL_targetz00_202, long BgL_tstartz00_203, obj_t BgL_sourcez00_204,
		obj_t BgL_sstartz00_205, obj_t BgL_sendz00_206)
	{
		{	/* Llib/srfi4.scm 838 */
			{	/* Llib/srfi4.scm 838 */
				long BgL_auxz00_6541;
				long BgL_tmpz00_6539;

				BgL_auxz00_6541 = (long) CINT(BgL_sendz00_206);
				BgL_tmpz00_6539 = (long) CINT(BgL_sstartz00_205);
				BGL_SU64VECTOR_COPY(BgL_targetz00_202, BgL_tstartz00_203,
					BgL_sourcez00_204, BgL_tmpz00_6539, BgL_auxz00_6541);
			}
			return BUNSPEC;
		}

	}



/* _f32vector-copy! */
	obj_t BGl__f32vectorzd2copyz12zc0zz__srfi4z00(obj_t BgL_env1311z00_215,
		obj_t BgL_opt1310z00_214)
	{
		{	/* Llib/srfi4.scm 838 */
			{	/* Llib/srfi4.scm 838 */
				obj_t BgL_targetz00_1869;
				obj_t BgL_tstartz00_1870;
				obj_t BgL_sourcez00_1871;

				BgL_targetz00_1869 = VECTOR_REF(BgL_opt1310z00_214, 0L);
				BgL_tstartz00_1870 = VECTOR_REF(BgL_opt1310z00_214, 1L);
				BgL_sourcez00_1871 = VECTOR_REF(BgL_opt1310z00_214, 2L);
				switch (VECTOR_LENGTH(BgL_opt1310z00_214))
					{
					case 3L:

						{	/* Llib/srfi4.scm 838 */
							long BgL_sendz00_1875;

							{	/* Llib/srfi4.scm 838 */
								obj_t BgL_xz00_3015;

								if (BGL_F32VECTORP(BgL_sourcez00_1871))
									{	/* Llib/srfi4.scm 838 */
										BgL_xz00_3015 = BgL_sourcez00_1871;
									}
								else
									{
										obj_t BgL_auxz00_6549;

										BgL_auxz00_6549 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2368z00zz__srfi4z00, BINT(33208L),
											BGl_string2462z00zz__srfi4z00,
											BGl_string2386z00zz__srfi4z00, BgL_sourcez00_1871);
										FAILURE(BgL_auxz00_6549, BFALSE, BFALSE);
									}
								BgL_sendz00_1875 = BGL_HVECTOR_LENGTH(BgL_xz00_3015);
							}
							{	/* Llib/srfi4.scm 838 */

								{	/* Llib/srfi4.scm 838 */
									obj_t BgL_targetz00_3016;
									long BgL_tstartz00_3017;
									obj_t BgL_sourcez00_3018;

									if (BGL_F32VECTORP(BgL_targetz00_1869))
										{	/* Llib/srfi4.scm 838 */
											BgL_targetz00_3016 = BgL_targetz00_1869;
										}
									else
										{
											obj_t BgL_auxz00_6556;

											BgL_auxz00_6556 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(33208L),
												BGl_string2462z00zz__srfi4z00,
												BGl_string2386z00zz__srfi4z00, BgL_targetz00_1869);
											FAILURE(BgL_auxz00_6556, BFALSE, BFALSE);
										}
									{	/* Llib/srfi4.scm 838 */
										obj_t BgL_tmpz00_6560;

										if (INTEGERP(BgL_tstartz00_1870))
											{	/* Llib/srfi4.scm 838 */
												BgL_tmpz00_6560 = BgL_tstartz00_1870;
											}
										else
											{
												obj_t BgL_auxz00_6563;

												BgL_auxz00_6563 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2462z00zz__srfi4z00,
													BGl_string2390z00zz__srfi4z00, BgL_tstartz00_1870);
												FAILURE(BgL_auxz00_6563, BFALSE, BFALSE);
											}
										BgL_tstartz00_3017 = (long) CINT(BgL_tmpz00_6560);
									}
									if (BGL_F32VECTORP(BgL_sourcez00_1871))
										{	/* Llib/srfi4.scm 838 */
											BgL_sourcez00_3018 = BgL_sourcez00_1871;
										}
									else
										{
											obj_t BgL_auxz00_6570;

											BgL_auxz00_6570 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(33208L),
												BGl_string2462z00zz__srfi4z00,
												BGl_string2386z00zz__srfi4z00, BgL_sourcez00_1871);
											FAILURE(BgL_auxz00_6570, BFALSE, BFALSE);
										}
									BGL_F32VECTOR_COPY(BgL_targetz00_3016, BgL_tstartz00_3017,
										BgL_sourcez00_3018, 0L, BgL_sendz00_1875);
									return BUNSPEC;
								}
							}
						}
						break;
					case 4L:

						{	/* Llib/srfi4.scm 838 */
							obj_t BgL_sstartz00_1876;

							BgL_sstartz00_1876 = VECTOR_REF(BgL_opt1310z00_214, 3L);
							{	/* Llib/srfi4.scm 838 */
								long BgL_sendz00_1877;

								{	/* Llib/srfi4.scm 838 */
									obj_t BgL_xz00_3019;

									if (BGL_F32VECTORP(BgL_sourcez00_1871))
										{	/* Llib/srfi4.scm 838 */
											BgL_xz00_3019 = BgL_sourcez00_1871;
										}
									else
										{
											obj_t BgL_auxz00_6578;

											BgL_auxz00_6578 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(33208L),
												BGl_string2462z00zz__srfi4z00,
												BGl_string2386z00zz__srfi4z00, BgL_sourcez00_1871);
											FAILURE(BgL_auxz00_6578, BFALSE, BFALSE);
										}
									BgL_sendz00_1877 = BGL_HVECTOR_LENGTH(BgL_xz00_3019);
								}
								{	/* Llib/srfi4.scm 838 */

									{	/* Llib/srfi4.scm 838 */
										obj_t BgL_targetz00_3020;
										long BgL_tstartz00_3021;
										obj_t BgL_sourcez00_3022;

										if (BGL_F32VECTORP(BgL_targetz00_1869))
											{	/* Llib/srfi4.scm 838 */
												BgL_targetz00_3020 = BgL_targetz00_1869;
											}
										else
											{
												obj_t BgL_auxz00_6585;

												BgL_auxz00_6585 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2462z00zz__srfi4z00,
													BGl_string2386z00zz__srfi4z00, BgL_targetz00_1869);
												FAILURE(BgL_auxz00_6585, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											obj_t BgL_tmpz00_6589;

											if (INTEGERP(BgL_tstartz00_1870))
												{	/* Llib/srfi4.scm 838 */
													BgL_tmpz00_6589 = BgL_tstartz00_1870;
												}
											else
												{
													obj_t BgL_auxz00_6592;

													BgL_auxz00_6592 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2368z00zz__srfi4z00, BINT(33208L),
														BGl_string2462z00zz__srfi4z00,
														BGl_string2390z00zz__srfi4z00, BgL_tstartz00_1870);
													FAILURE(BgL_auxz00_6592, BFALSE, BFALSE);
												}
											BgL_tstartz00_3021 = (long) CINT(BgL_tmpz00_6589);
										}
										if (BGL_F32VECTORP(BgL_sourcez00_1871))
											{	/* Llib/srfi4.scm 838 */
												BgL_sourcez00_3022 = BgL_sourcez00_1871;
											}
										else
											{
												obj_t BgL_auxz00_6599;

												BgL_auxz00_6599 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2462z00zz__srfi4z00,
													BGl_string2386z00zz__srfi4z00, BgL_sourcez00_1871);
												FAILURE(BgL_auxz00_6599, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											long BgL_tmpz00_6603;

											{	/* Llib/srfi4.scm 838 */
												obj_t BgL_tmpz00_6604;

												if (INTEGERP(BgL_sstartz00_1876))
													{	/* Llib/srfi4.scm 838 */
														BgL_tmpz00_6604 = BgL_sstartz00_1876;
													}
												else
													{
														obj_t BgL_auxz00_6607;

														BgL_auxz00_6607 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2368z00zz__srfi4z00, BINT(33208L),
															BGl_string2462z00zz__srfi4z00,
															BGl_string2390z00zz__srfi4z00,
															BgL_sstartz00_1876);
														FAILURE(BgL_auxz00_6607, BFALSE, BFALSE);
													}
												BgL_tmpz00_6603 = (long) CINT(BgL_tmpz00_6604);
											}
											BGL_F32VECTOR_COPY(BgL_targetz00_3020, BgL_tstartz00_3021,
												BgL_sourcez00_3022, BgL_tmpz00_6603, BgL_sendz00_1877);
										}
										return BUNSPEC;
									}
								}
							}
						}
						break;
					case 5L:

						{	/* Llib/srfi4.scm 838 */
							obj_t BgL_sstartz00_1878;

							BgL_sstartz00_1878 = VECTOR_REF(BgL_opt1310z00_214, 3L);
							{	/* Llib/srfi4.scm 838 */
								obj_t BgL_sendz00_1879;

								BgL_sendz00_1879 = VECTOR_REF(BgL_opt1310z00_214, 4L);
								{	/* Llib/srfi4.scm 838 */

									{	/* Llib/srfi4.scm 838 */
										obj_t BgL_targetz00_3023;
										long BgL_tstartz00_3024;
										obj_t BgL_sourcez00_3025;

										if (BGL_F32VECTORP(BgL_targetz00_1869))
											{	/* Llib/srfi4.scm 838 */
												BgL_targetz00_3023 = BgL_targetz00_1869;
											}
										else
											{
												obj_t BgL_auxz00_6617;

												BgL_auxz00_6617 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2462z00zz__srfi4z00,
													BGl_string2386z00zz__srfi4z00, BgL_targetz00_1869);
												FAILURE(BgL_auxz00_6617, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											obj_t BgL_tmpz00_6621;

											if (INTEGERP(BgL_tstartz00_1870))
												{	/* Llib/srfi4.scm 838 */
													BgL_tmpz00_6621 = BgL_tstartz00_1870;
												}
											else
												{
													obj_t BgL_auxz00_6624;

													BgL_auxz00_6624 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2368z00zz__srfi4z00, BINT(33208L),
														BGl_string2462z00zz__srfi4z00,
														BGl_string2390z00zz__srfi4z00, BgL_tstartz00_1870);
													FAILURE(BgL_auxz00_6624, BFALSE, BFALSE);
												}
											BgL_tstartz00_3024 = (long) CINT(BgL_tmpz00_6621);
										}
										if (BGL_F32VECTORP(BgL_sourcez00_1871))
											{	/* Llib/srfi4.scm 838 */
												BgL_sourcez00_3025 = BgL_sourcez00_1871;
											}
										else
											{
												obj_t BgL_auxz00_6631;

												BgL_auxz00_6631 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2462z00zz__srfi4z00,
													BGl_string2386z00zz__srfi4z00, BgL_sourcez00_1871);
												FAILURE(BgL_auxz00_6631, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											long BgL_auxz00_6644;
											long BgL_tmpz00_6635;

											{	/* Llib/srfi4.scm 838 */
												obj_t BgL_tmpz00_6645;

												if (INTEGERP(BgL_sendz00_1879))
													{	/* Llib/srfi4.scm 838 */
														BgL_tmpz00_6645 = BgL_sendz00_1879;
													}
												else
													{
														obj_t BgL_auxz00_6648;

														BgL_auxz00_6648 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2368z00zz__srfi4z00, BINT(33208L),
															BGl_string2462z00zz__srfi4z00,
															BGl_string2390z00zz__srfi4z00, BgL_sendz00_1879);
														FAILURE(BgL_auxz00_6648, BFALSE, BFALSE);
													}
												BgL_auxz00_6644 = (long) CINT(BgL_tmpz00_6645);
											}
											{	/* Llib/srfi4.scm 838 */
												obj_t BgL_tmpz00_6636;

												if (INTEGERP(BgL_sstartz00_1878))
													{	/* Llib/srfi4.scm 838 */
														BgL_tmpz00_6636 = BgL_sstartz00_1878;
													}
												else
													{
														obj_t BgL_auxz00_6639;

														BgL_auxz00_6639 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2368z00zz__srfi4z00, BINT(33208L),
															BGl_string2462z00zz__srfi4z00,
															BGl_string2390z00zz__srfi4z00,
															BgL_sstartz00_1878);
														FAILURE(BgL_auxz00_6639, BFALSE, BFALSE);
													}
												BgL_tmpz00_6635 = (long) CINT(BgL_tmpz00_6636);
											}
											BGL_F32VECTOR_COPY(BgL_targetz00_3023, BgL_tstartz00_3024,
												BgL_sourcez00_3025, BgL_tmpz00_6635, BgL_auxz00_6644);
										}
										return BUNSPEC;
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* f32vector-copy! */
	BGL_EXPORTED_DEF obj_t BGl_f32vectorzd2copyz12zc0zz__srfi4z00(obj_t
		BgL_targetz00_209, long BgL_tstartz00_210, obj_t BgL_sourcez00_211,
		obj_t BgL_sstartz00_212, obj_t BgL_sendz00_213)
	{
		{	/* Llib/srfi4.scm 838 */
			{	/* Llib/srfi4.scm 838 */
				long BgL_auxz00_6658;
				long BgL_tmpz00_6656;

				BgL_auxz00_6658 = (long) CINT(BgL_sendz00_213);
				BgL_tmpz00_6656 = (long) CINT(BgL_sstartz00_212);
				BGL_F32VECTOR_COPY(BgL_targetz00_209, BgL_tstartz00_210,
					BgL_sourcez00_211, BgL_tmpz00_6656, BgL_auxz00_6658);
			}
			return BUNSPEC;
		}

	}



/* _f64vector-copy! */
	obj_t BGl__f64vectorzd2copyz12zc0zz__srfi4z00(obj_t BgL_env1315z00_222,
		obj_t BgL_opt1314z00_221)
	{
		{	/* Llib/srfi4.scm 838 */
			{	/* Llib/srfi4.scm 838 */
				obj_t BgL_targetz00_1880;
				obj_t BgL_tstartz00_1881;
				obj_t BgL_sourcez00_1882;

				BgL_targetz00_1880 = VECTOR_REF(BgL_opt1314z00_221, 0L);
				BgL_tstartz00_1881 = VECTOR_REF(BgL_opt1314z00_221, 1L);
				BgL_sourcez00_1882 = VECTOR_REF(BgL_opt1314z00_221, 2L);
				switch (VECTOR_LENGTH(BgL_opt1314z00_221))
					{
					case 3L:

						{	/* Llib/srfi4.scm 838 */
							long BgL_sendz00_1886;

							{	/* Llib/srfi4.scm 838 */
								obj_t BgL_xz00_3026;

								if (BGL_F64VECTORP(BgL_sourcez00_1882))
									{	/* Llib/srfi4.scm 838 */
										BgL_xz00_3026 = BgL_sourcez00_1882;
									}
								else
									{
										obj_t BgL_auxz00_6666;

										BgL_auxz00_6666 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2368z00zz__srfi4z00, BINT(33208L),
											BGl_string2463z00zz__srfi4z00,
											BGl_string2388z00zz__srfi4z00, BgL_sourcez00_1882);
										FAILURE(BgL_auxz00_6666, BFALSE, BFALSE);
									}
								BgL_sendz00_1886 = BGL_HVECTOR_LENGTH(BgL_xz00_3026);
							}
							{	/* Llib/srfi4.scm 838 */

								{	/* Llib/srfi4.scm 838 */
									obj_t BgL_targetz00_3027;
									long BgL_tstartz00_3028;
									obj_t BgL_sourcez00_3029;

									if (BGL_F64VECTORP(BgL_targetz00_1880))
										{	/* Llib/srfi4.scm 838 */
											BgL_targetz00_3027 = BgL_targetz00_1880;
										}
									else
										{
											obj_t BgL_auxz00_6673;

											BgL_auxz00_6673 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(33208L),
												BGl_string2463z00zz__srfi4z00,
												BGl_string2388z00zz__srfi4z00, BgL_targetz00_1880);
											FAILURE(BgL_auxz00_6673, BFALSE, BFALSE);
										}
									{	/* Llib/srfi4.scm 838 */
										obj_t BgL_tmpz00_6677;

										if (INTEGERP(BgL_tstartz00_1881))
											{	/* Llib/srfi4.scm 838 */
												BgL_tmpz00_6677 = BgL_tstartz00_1881;
											}
										else
											{
												obj_t BgL_auxz00_6680;

												BgL_auxz00_6680 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2463z00zz__srfi4z00,
													BGl_string2390z00zz__srfi4z00, BgL_tstartz00_1881);
												FAILURE(BgL_auxz00_6680, BFALSE, BFALSE);
											}
										BgL_tstartz00_3028 = (long) CINT(BgL_tmpz00_6677);
									}
									if (BGL_F64VECTORP(BgL_sourcez00_1882))
										{	/* Llib/srfi4.scm 838 */
											BgL_sourcez00_3029 = BgL_sourcez00_1882;
										}
									else
										{
											obj_t BgL_auxz00_6687;

											BgL_auxz00_6687 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(33208L),
												BGl_string2463z00zz__srfi4z00,
												BGl_string2388z00zz__srfi4z00, BgL_sourcez00_1882);
											FAILURE(BgL_auxz00_6687, BFALSE, BFALSE);
										}
									BGL_F64VECTOR_COPY(BgL_targetz00_3027, BgL_tstartz00_3028,
										BgL_sourcez00_3029, 0L, BgL_sendz00_1886);
									return BUNSPEC;
								}
							}
						}
						break;
					case 4L:

						{	/* Llib/srfi4.scm 838 */
							obj_t BgL_sstartz00_1887;

							BgL_sstartz00_1887 = VECTOR_REF(BgL_opt1314z00_221, 3L);
							{	/* Llib/srfi4.scm 838 */
								long BgL_sendz00_1888;

								{	/* Llib/srfi4.scm 838 */
									obj_t BgL_xz00_3030;

									if (BGL_F64VECTORP(BgL_sourcez00_1882))
										{	/* Llib/srfi4.scm 838 */
											BgL_xz00_3030 = BgL_sourcez00_1882;
										}
									else
										{
											obj_t BgL_auxz00_6695;

											BgL_auxz00_6695 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2368z00zz__srfi4z00, BINT(33208L),
												BGl_string2463z00zz__srfi4z00,
												BGl_string2388z00zz__srfi4z00, BgL_sourcez00_1882);
											FAILURE(BgL_auxz00_6695, BFALSE, BFALSE);
										}
									BgL_sendz00_1888 = BGL_HVECTOR_LENGTH(BgL_xz00_3030);
								}
								{	/* Llib/srfi4.scm 838 */

									{	/* Llib/srfi4.scm 838 */
										obj_t BgL_targetz00_3031;
										long BgL_tstartz00_3032;
										obj_t BgL_sourcez00_3033;

										if (BGL_F64VECTORP(BgL_targetz00_1880))
											{	/* Llib/srfi4.scm 838 */
												BgL_targetz00_3031 = BgL_targetz00_1880;
											}
										else
											{
												obj_t BgL_auxz00_6702;

												BgL_auxz00_6702 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2463z00zz__srfi4z00,
													BGl_string2388z00zz__srfi4z00, BgL_targetz00_1880);
												FAILURE(BgL_auxz00_6702, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											obj_t BgL_tmpz00_6706;

											if (INTEGERP(BgL_tstartz00_1881))
												{	/* Llib/srfi4.scm 838 */
													BgL_tmpz00_6706 = BgL_tstartz00_1881;
												}
											else
												{
													obj_t BgL_auxz00_6709;

													BgL_auxz00_6709 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2368z00zz__srfi4z00, BINT(33208L),
														BGl_string2463z00zz__srfi4z00,
														BGl_string2390z00zz__srfi4z00, BgL_tstartz00_1881);
													FAILURE(BgL_auxz00_6709, BFALSE, BFALSE);
												}
											BgL_tstartz00_3032 = (long) CINT(BgL_tmpz00_6706);
										}
										if (BGL_F64VECTORP(BgL_sourcez00_1882))
											{	/* Llib/srfi4.scm 838 */
												BgL_sourcez00_3033 = BgL_sourcez00_1882;
											}
										else
											{
												obj_t BgL_auxz00_6716;

												BgL_auxz00_6716 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2463z00zz__srfi4z00,
													BGl_string2388z00zz__srfi4z00, BgL_sourcez00_1882);
												FAILURE(BgL_auxz00_6716, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											long BgL_tmpz00_6720;

											{	/* Llib/srfi4.scm 838 */
												obj_t BgL_tmpz00_6721;

												if (INTEGERP(BgL_sstartz00_1887))
													{	/* Llib/srfi4.scm 838 */
														BgL_tmpz00_6721 = BgL_sstartz00_1887;
													}
												else
													{
														obj_t BgL_auxz00_6724;

														BgL_auxz00_6724 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2368z00zz__srfi4z00, BINT(33208L),
															BGl_string2463z00zz__srfi4z00,
															BGl_string2390z00zz__srfi4z00,
															BgL_sstartz00_1887);
														FAILURE(BgL_auxz00_6724, BFALSE, BFALSE);
													}
												BgL_tmpz00_6720 = (long) CINT(BgL_tmpz00_6721);
											}
											BGL_F64VECTOR_COPY(BgL_targetz00_3031, BgL_tstartz00_3032,
												BgL_sourcez00_3033, BgL_tmpz00_6720, BgL_sendz00_1888);
										}
										return BUNSPEC;
									}
								}
							}
						}
						break;
					case 5L:

						{	/* Llib/srfi4.scm 838 */
							obj_t BgL_sstartz00_1889;

							BgL_sstartz00_1889 = VECTOR_REF(BgL_opt1314z00_221, 3L);
							{	/* Llib/srfi4.scm 838 */
								obj_t BgL_sendz00_1890;

								BgL_sendz00_1890 = VECTOR_REF(BgL_opt1314z00_221, 4L);
								{	/* Llib/srfi4.scm 838 */

									{	/* Llib/srfi4.scm 838 */
										obj_t BgL_targetz00_3034;
										long BgL_tstartz00_3035;
										obj_t BgL_sourcez00_3036;

										if (BGL_F64VECTORP(BgL_targetz00_1880))
											{	/* Llib/srfi4.scm 838 */
												BgL_targetz00_3034 = BgL_targetz00_1880;
											}
										else
											{
												obj_t BgL_auxz00_6734;

												BgL_auxz00_6734 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2463z00zz__srfi4z00,
													BGl_string2388z00zz__srfi4z00, BgL_targetz00_1880);
												FAILURE(BgL_auxz00_6734, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											obj_t BgL_tmpz00_6738;

											if (INTEGERP(BgL_tstartz00_1881))
												{	/* Llib/srfi4.scm 838 */
													BgL_tmpz00_6738 = BgL_tstartz00_1881;
												}
											else
												{
													obj_t BgL_auxz00_6741;

													BgL_auxz00_6741 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2368z00zz__srfi4z00, BINT(33208L),
														BGl_string2463z00zz__srfi4z00,
														BGl_string2390z00zz__srfi4z00, BgL_tstartz00_1881);
													FAILURE(BgL_auxz00_6741, BFALSE, BFALSE);
												}
											BgL_tstartz00_3035 = (long) CINT(BgL_tmpz00_6738);
										}
										if (BGL_F64VECTORP(BgL_sourcez00_1882))
											{	/* Llib/srfi4.scm 838 */
												BgL_sourcez00_3036 = BgL_sourcez00_1882;
											}
										else
											{
												obj_t BgL_auxz00_6748;

												BgL_auxz00_6748 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2368z00zz__srfi4z00, BINT(33208L),
													BGl_string2463z00zz__srfi4z00,
													BGl_string2388z00zz__srfi4z00, BgL_sourcez00_1882);
												FAILURE(BgL_auxz00_6748, BFALSE, BFALSE);
											}
										{	/* Llib/srfi4.scm 838 */
											long BgL_auxz00_6761;
											long BgL_tmpz00_6752;

											{	/* Llib/srfi4.scm 838 */
												obj_t BgL_tmpz00_6762;

												if (INTEGERP(BgL_sendz00_1890))
													{	/* Llib/srfi4.scm 838 */
														BgL_tmpz00_6762 = BgL_sendz00_1890;
													}
												else
													{
														obj_t BgL_auxz00_6765;

														BgL_auxz00_6765 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2368z00zz__srfi4z00, BINT(33208L),
															BGl_string2463z00zz__srfi4z00,
															BGl_string2390z00zz__srfi4z00, BgL_sendz00_1890);
														FAILURE(BgL_auxz00_6765, BFALSE, BFALSE);
													}
												BgL_auxz00_6761 = (long) CINT(BgL_tmpz00_6762);
											}
											{	/* Llib/srfi4.scm 838 */
												obj_t BgL_tmpz00_6753;

												if (INTEGERP(BgL_sstartz00_1889))
													{	/* Llib/srfi4.scm 838 */
														BgL_tmpz00_6753 = BgL_sstartz00_1889;
													}
												else
													{
														obj_t BgL_auxz00_6756;

														BgL_auxz00_6756 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2368z00zz__srfi4z00, BINT(33208L),
															BGl_string2463z00zz__srfi4z00,
															BGl_string2390z00zz__srfi4z00,
															BgL_sstartz00_1889);
														FAILURE(BgL_auxz00_6756, BFALSE, BFALSE);
													}
												BgL_tmpz00_6752 = (long) CINT(BgL_tmpz00_6753);
											}
											BGL_F64VECTOR_COPY(BgL_targetz00_3034, BgL_tstartz00_3035,
												BgL_sourcez00_3036, BgL_tmpz00_6752, BgL_auxz00_6761);
										}
										return BUNSPEC;
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* f64vector-copy! */
	BGL_EXPORTED_DEF obj_t BGl_f64vectorzd2copyz12zc0zz__srfi4z00(obj_t
		BgL_targetz00_216, long BgL_tstartz00_217, obj_t BgL_sourcez00_218,
		obj_t BgL_sstartz00_219, obj_t BgL_sendz00_220)
	{
		{	/* Llib/srfi4.scm 838 */
			{	/* Llib/srfi4.scm 838 */
				long BgL_auxz00_6775;
				long BgL_tmpz00_6773;

				BgL_auxz00_6775 = (long) CINT(BgL_sendz00_220);
				BgL_tmpz00_6773 = (long) CINT(BgL_sstartz00_219);
				BGL_F64VECTOR_COPY(BgL_targetz00_216, BgL_tstartz00_217,
					BgL_sourcez00_218, BgL_tmpz00_6773, BgL_auxz00_6775);
			}
			return BUNSPEC;
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__srfi4z00(void)
	{
		{	/* Llib/srfi4.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__srfi4z00(void)
	{
		{	/* Llib/srfi4.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__srfi4z00(void)
	{
		{	/* Llib/srfi4.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__srfi4z00(void)
	{
		{	/* Llib/srfi4.scm 15 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string2464z00zz__srfi4z00));
			BGl_modulezd2initializa7ationz75zz__hashz00(482391669L,
				BSTRING_TO_STRING(BGl_string2464z00zz__srfi4z00));
			BGl_modulezd2initializa7ationz75zz__readerz00(220648206L,
				BSTRING_TO_STRING(BGl_string2464z00zz__srfi4z00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string2464z00zz__srfi4z00));
			return
				BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L,
				BSTRING_TO_STRING(BGl_string2464z00zz__srfi4z00));
		}

	}

#ifdef __cplusplus
}
#endif
