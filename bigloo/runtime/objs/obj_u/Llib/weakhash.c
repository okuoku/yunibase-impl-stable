/*===========================================================================*/
/*   (Llib/weakhash.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Llib/weakhash.scm -indent -o objs/obj_u/Llib/weakhash.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___WEAKHASH_TYPE_DEFINITIONS
#define BGL___WEAKHASH_TYPE_DEFINITIONS
#endif													// BGL___WEAKHASH_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t BGl_weakzd2hashtablezd2putz12z12zz__weakhashz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_weakzd2hashtablezd2getz00zz__weakhashz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31426ze3ze5zz__weakhashz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31418ze3ze5zz__weakhashz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_weakzd2hashtablezd2forzd2eachzd2zz__weakhashz00(obj_t, obj_t);
	static obj_t BGl_z62weakzd2hashtablezd2forzd2eachzb0zz__weakhashz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_weakzd2hashtablezd2keyzd2listzd2zz__weakhashz00(obj_t);
	static obj_t BGl_z62weakzd2hashtablezd2keyzd2listzb0zz__weakhashz00(obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__weakhashz00 = BUNSPEC;
	static obj_t BGl_traversezd2bucketszd2zz__weakhashz00(obj_t, obj_t, long,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_weakzd2hashtablezd2updatez12z12zz__weakhashz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62weakzd2hashtablezd2getz62zz__weakhashz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__weakhashz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__hashz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_removez00zz__weakhashz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31453ze3ze5zz__weakhashz00(obj_t, obj_t,
		obj_t);
	extern long BGl_hashtablezd2siza7ez75zz__hashz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31679ze3ze5zz__weakhashz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62weakzd2hashtablezd2clearz12z70zz__weakhashz00(obj_t,
		obj_t);
	static obj_t BGl_weakzd2oldzd2hashtablezd2addz12zc0zz__weakhashz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static bool_t BGl_weakzd2oldzd2hashtablezd2removez12zc0zz__weakhashz00(obj_t,
		obj_t);
	extern bool_t BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(obj_t);
	static obj_t BGl_toplevelzd2initzd2zz__weakhashz00(void);
	static obj_t BGl_z62zc3z04anonymousza31640ze3ze5zz__weakhashz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31527ze3ze5zz__weakhashz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_weakzd2oldzd2hashtablezd2putz12zc0zz__weakhashz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_keepgoingz00zz__weakhashz00 = BUNSPEC;
	extern void bgl_weakptr_ref_set(obj_t, obj_t);
	extern obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31455ze3ze5zz__weakhashz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31447ze3ze5zz__weakhashz00(obj_t, obj_t,
		obj_t, obj_t);
	static bool_t
		BGl_weakzd2keyszd2hashtablezd2containszf3z21zz__weakhashz00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__weakhashz00(void);
	static obj_t BGl_weakzd2oldzd2hashtablezd2expandz12zc0zz__weakhashz00(obj_t);
	static obj_t BGl_genericzd2initzd2zz__weakhashz00(void);
	static bool_t BGl_weakzd2oldzd2hashtablezd2filterz12zc0zz__weakhashz00(obj_t,
		obj_t);
	static obj_t BGl_z62weakzd2hashtablezd2removez12z70zz__weakhashz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62weakzd2hashtablezd2ze3vectorz81zz__weakhashz00(obj_t,
		obj_t);
	static bool_t BGl_weakzd2keyszd2hashtablezd2removez12zc0zz__weakhashz00(obj_t,
		obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__weakhashz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__weakhashz00(void);
	static obj_t BGl_weakzd2keyszd2hashtablezd2getzd2zz__weakhashz00(obj_t,
		obj_t);
	static obj_t BGl_objectzd2initzd2zz__weakhashz00(void);
	extern obj_t BGl_copyzd2vectorzd2zz__r4_vectors_6_8z00(obj_t, long);
	extern obj_t BGl_2za2za2zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62weakzd2hashtablezd2expandz12z70zz__weakhashz00(obj_t,
		obj_t);
	static obj_t BGl_weakzd2keyszd2hashtablezd2expandz12zc0zz__weakhashz00(obj_t);
	static obj_t BGl_z62weakzd2hashtablezd2filterz12z70zz__weakhashz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62weakzd2hashtablezd2addz12z70zz__weakhashz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static bool_t BGl_weakzd2keyszd2hashtablezd2filterz12zc0zz__weakhashz00(obj_t,
		obj_t);
	static bool_t BGl_keyszd2traversezd2hashz00zz__weakhashz00(obj_t, obj_t);
	extern obj_t bgl_weakptr_data(obj_t);
	extern obj_t bgl_weakptr_ref(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_weakzd2hashtablezd2mapz00zz__weakhashz00(obj_t,
		obj_t);
	static obj_t BGl_z62weakzd2hashtablezd2putz12z70zz__weakhashz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62weakzd2hashtablezd2ze3listz81zz__weakhashz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31482ze3ze5zz__weakhashz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31466ze3ze5zz__weakhashz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_weakzd2oldzd2hashtablezd2getzd2zz__weakhashz00(obj_t, obj_t);
	static obj_t BGl_symbol2173z00zz__weakhashz00 = BUNSPEC;
	extern obj_t BGl_filterz12z12zz__r4_control_features_6_9z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_weakzd2hashtablezd2containszf3zf3zz__weakhashz00(obj_t, obj_t);
	static bool_t
		BGl_weakzd2oldzd2hashtablezd2containszf3z21zz__weakhashz00(obj_t, obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	extern obj_t make_vector(long, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_weakzd2hashtablezd2ze3listze3zz__weakhashz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31420ze3ze5zz__weakhashz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62weakzd2hashtablezd2mapz62zz__weakhashz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_weakzd2oldzd2hashtablezd2updatez12zc0zz__weakhashz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zz__weakhashz00(void);
	extern obj_t bgl_make_weakptr(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31590ze3ze5zz__weakhashz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_weakzd2hashtablezd2removez12z12zz__weakhashz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_weakzd2hashtablezd2ze3vectorze3zz__weakhashz00(obj_t);
	static obj_t BGl_z62weakzd2hashtablezd2updatez12z70zz__weakhashz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_weakzd2keyszd2hashtablezd2updatez12zc0zz__weakhashz00(obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_weakzd2hashtablezd2expandz12z12zz__weakhashz00(obj_t);
	static obj_t BGl_removestopz00zz__weakhashz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_weakzd2hashtablezd2filterz12z12zz__weakhashz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31343ze3ze5zz__weakhashz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31416ze3ze5zz__weakhashz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31408ze3ze5zz__weakhashz00(obj_t, obj_t,
		obj_t);
	extern long BGl_getzd2hashnumberzd2persistentz00zz__hashz00(obj_t);
	extern long BGl_getzd2hashnumberzd2zz__hashz00(obj_t);
	static obj_t BGl_weakzd2keyszd2hashtablezd2addz12zc0zz__weakhashz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	extern bool_t BGl_hashtablezd2weakzd2datazf3zf3zz__hashz00(obj_t);
	static bool_t BGl_oldzd2traversezd2hashz00zz__weakhashz00(obj_t, obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_gcz00zz__biglooz00(obj_t);
	static obj_t BGl_weakzd2keyszd2hashtablezd2putz12zc0zz__weakhashz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62weakzd2hashtablezd2containszf3z91zz__weakhashz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_weakzd2hashtablezd2clearz12z12zz__weakhashz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_weakzd2hashtablezd2addz12z12zz__weakhashz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_weakzd2hashtablezd2filterz12zd2envzc0zz__weakhashz00,
		BgL_bgl_za762weakza7d2hashta2185z00,
		BGl_z62weakzd2hashtablezd2filterz12z70zz__weakhashz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2161z00zz__weakhashz00,
		BgL_bgl_string2161za700za7za7_2186za7,
		"/tmp/bigloo/runtime/Llib/weakhash.scm", 37);
	      DEFINE_STRING(BGl_string2162z00zz__weakhashz00,
		BgL_bgl_string2162za700za7za7_2187za7, "&weak-hashtable->vector", 23);
	      DEFINE_STRING(BGl_string2163z00zz__weakhashz00,
		BgL_bgl_string2163za700za7za7_2188za7, "struct", 6);
	      DEFINE_STRING(BGl_string2164z00zz__weakhashz00,
		BgL_bgl_string2164za700za7za7_2189za7, "&weak-hashtable->list", 21);
	      DEFINE_STRING(BGl_string2165z00zz__weakhashz00,
		BgL_bgl_string2165za700za7za7_2190za7, "&weak-hashtable-key-list", 24);
	      DEFINE_STRING(BGl_string2166z00zz__weakhashz00,
		BgL_bgl_string2166za700za7za7_2191za7, "&weak-hashtable-map", 19);
	      DEFINE_STRING(BGl_string2167z00zz__weakhashz00,
		BgL_bgl_string2167za700za7za7_2192za7, "procedure", 9);
	      DEFINE_STRING(BGl_string2168z00zz__weakhashz00,
		BgL_bgl_string2168za700za7za7_2193za7, "&weak-hashtable-for-each", 24);
	      DEFINE_STRING(BGl_string2169z00zz__weakhashz00,
		BgL_bgl_string2169za700za7za7_2194za7, "&weak-hashtable-filter!", 23);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2160z00zz__weakhashz00,
		BgL_bgl_za762za7c3za704anonymo2195za7,
		BGl_z62zc3z04anonymousza31343ze3ze5zz__weakhashz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2172z00zz__weakhashz00,
		BgL_bgl_string2172za700za7za7_2196za7, "&weak-hashtable-clear!", 22);
	      DEFINE_STRING(BGl_string2174z00zz__weakhashz00,
		BgL_bgl_string2174za700za7za7_2197za7, "persistent", 10);
	      DEFINE_STRING(BGl_string2175z00zz__weakhashz00,
		BgL_bgl_string2175za700za7za7_2198za7, "&weak-hashtable-contains?", 25);
	      DEFINE_STRING(BGl_string2176z00zz__weakhashz00,
		BgL_bgl_string2176za700za7za7_2199za7, "&weak-hashtable-get", 19);
	      DEFINE_STRING(BGl_string2177z00zz__weakhashz00,
		BgL_bgl_string2177za700za7za7_2200za7, "&weak-hashtable-put!", 20);
	      DEFINE_STRING(BGl_string2178z00zz__weakhashz00,
		BgL_bgl_string2178za700za7za7_2201za7, "&weak-hashtable-update!", 23);
	      DEFINE_STRING(BGl_string2179z00zz__weakhashz00,
		BgL_bgl_string2179za700za7za7_2202za7, "&weak-hashtable-add!", 20);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2170z00zz__weakhashz00,
		BgL_bgl_za762za7c3za704anonymo2203za7,
		BGl_z62zc3z04anonymousza31453ze3ze5zz__weakhashz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2171z00zz__weakhashz00,
		BgL_bgl_za762za7c3za704anonymo2204za7,
		BGl_z62zc3z04anonymousza31455ze3ze5zz__weakhashz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2180z00zz__weakhashz00,
		BgL_bgl_string2180za700za7za7_2205za7, "&weak-hashtable-remove!", 23);
	      DEFINE_STRING(BGl_string2181z00zz__weakhashz00,
		BgL_bgl_string2181za700za7za7_2206za7,
		"Hashtable too large (new-len=~a/~a, size=~a)", 44);
	      DEFINE_STRING(BGl_string2182z00zz__weakhashz00,
		BgL_bgl_string2182za700za7za7_2207za7, "hashtable-put!", 14);
	      DEFINE_STRING(BGl_string2183z00zz__weakhashz00,
		BgL_bgl_string2183za700za7za7_2208za7, "&weak-hashtable-expand!", 23);
	      DEFINE_STRING(BGl_string2184z00zz__weakhashz00,
		BgL_bgl_string2184za700za7za7_2209za7, "__weakhash", 10);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_weakzd2hashtablezd2clearz12zd2envzc0zz__weakhashz00,
		BgL_bgl_za762weakza7d2hashta2210z00,
		BGl_z62weakzd2hashtablezd2clearz12z70zz__weakhashz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_weakzd2hashtablezd2ze3vectorzd2envz31zz__weakhashz00,
		BgL_bgl_za762weakza7d2hashta2211z00,
		BGl_z62weakzd2hashtablezd2ze3vectorz81zz__weakhashz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_weakzd2hashtablezd2mapzd2envzd2zz__weakhashz00,
		BgL_bgl_za762weakza7d2hashta2212z00,
		BGl_z62weakzd2hashtablezd2mapz62zz__weakhashz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_weakzd2hashtablezd2getzd2envzd2zz__weakhashz00,
		BgL_bgl_za762weakza7d2hashta2213z00,
		BGl_z62weakzd2hashtablezd2getz62zz__weakhashz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_weakzd2hashtablezd2containszf3zd2envz21zz__weakhashz00,
		BgL_bgl_za762weakza7d2hashta2214z00,
		BGl_z62weakzd2hashtablezd2containszf3z91zz__weakhashz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_weakzd2hashtablezd2expandz12zd2envzc0zz__weakhashz00,
		BgL_bgl_za762weakza7d2hashta2215z00,
		BGl_z62weakzd2hashtablezd2expandz12z70zz__weakhashz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_weakzd2hashtablezd2ze3listzd2envz31zz__weakhashz00,
		BgL_bgl_za762weakza7d2hashta2216z00,
		BGl_z62weakzd2hashtablezd2ze3listz81zz__weakhashz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_weakzd2hashtablezd2updatez12zd2envzc0zz__weakhashz00,
		BgL_bgl_za762weakza7d2hashta2217z00,
		BGl_z62weakzd2hashtablezd2updatez12z70zz__weakhashz00, 0L, BUNSPEC, 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_weakzd2hashtablezd2forzd2eachzd2envz00zz__weakhashz00,
		BgL_bgl_za762weakza7d2hashta2218z00,
		BGl_z62weakzd2hashtablezd2forzd2eachzb0zz__weakhashz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_weakzd2hashtablezd2addz12zd2envzc0zz__weakhashz00,
		BgL_bgl_za762weakza7d2hashta2219z00,
		BGl_z62weakzd2hashtablezd2addz12z70zz__weakhashz00, 0L, BUNSPEC, 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_weakzd2hashtablezd2removez12zd2envzc0zz__weakhashz00,
		BgL_bgl_za762weakza7d2hashta2220z00,
		BGl_z62weakzd2hashtablezd2removez12z70zz__weakhashz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_weakzd2hashtablezd2putz12zd2envzc0zz__weakhashz00,
		BgL_bgl_za762weakza7d2hashta2221z00,
		BGl_z62weakzd2hashtablezd2putz12z70zz__weakhashz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_weakzd2hashtablezd2keyzd2listzd2envz00zz__weakhashz00,
		BgL_bgl_za762weakza7d2hashta2222z00,
		BGl_z62weakzd2hashtablezd2keyzd2listzb0zz__weakhashz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__weakhashz00));
		     ADD_ROOT((void *) (&BGl_removez00zz__weakhashz00));
		     ADD_ROOT((void *) (&BGl_keepgoingz00zz__weakhashz00));
		     ADD_ROOT((void *) (&BGl_symbol2173z00zz__weakhashz00));
		     ADD_ROOT((void *) (&BGl_removestopz00zz__weakhashz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__weakhashz00(long
		BgL_checksumz00_4299, char *BgL_fromz00_4300)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__weakhashz00))
				{
					BGl_requirezd2initializa7ationz75zz__weakhashz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__weakhashz00();
					BGl_cnstzd2initzd2zz__weakhashz00();
					BGl_importedzd2moduleszd2initz00zz__weakhashz00();
					return BGl_toplevelzd2initzd2zz__weakhashz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__weakhashz00(void)
	{
		{	/* Llib/weakhash.scm 18 */
			return (BGl_symbol2173z00zz__weakhashz00 =
				bstring_to_symbol(BGl_string2174z00zz__weakhashz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__weakhashz00(void)
	{
		{	/* Llib/weakhash.scm 18 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__weakhashz00(void)
	{
		{	/* Llib/weakhash.scm 18 */
			BGl_keepgoingz00zz__weakhashz00 = MAKE_YOUNG_PAIR(BUNSPEC, BUNSPEC);
			BGl_removez00zz__weakhashz00 = MAKE_YOUNG_PAIR(BUNSPEC, BUNSPEC);
			return (BGl_removestopz00zz__weakhashz00 =
				MAKE_YOUNG_PAIR(BUNSPEC, BUNSPEC), BUNSPEC);
		}

	}



/* traverse-buckets */
	obj_t BGl_traversezd2bucketszd2zz__weakhashz00(obj_t BgL_tablez00_47,
		obj_t BgL_bucketsz00_48, long BgL_iz00_49, obj_t BgL_funz00_50)
	{
		{	/* Llib/weakhash.scm 144 */
			if ((1L == (long) CINT(STRUCT_REF(BgL_tablez00_47, (int) (5L)))))
				{	/* Llib/weakhash.scm 145 */
					obj_t BgL_g1060z00_1292;

					BgL_g1060z00_1292 =
						VECTOR_REF(((obj_t) BgL_bucketsz00_48), BgL_iz00_49);
					{
						obj_t BgL_bucketz00_1294;
						obj_t BgL_lastzd2bucketzd2_1295;

						BgL_bucketz00_1294 = BgL_g1060z00_1292;
						BgL_lastzd2bucketzd2_1295 = BFALSE;
					BgL_zc3z04anonymousza31308ze3z87_1296:
						if (NULLP(BgL_bucketz00_1294))
							{	/* Llib/weakhash.scm 145 */
								return BGl_keepgoingz00zz__weakhashz00;
							}
						else
							{	/* Llib/weakhash.scm 145 */
								obj_t BgL_retz00_1298;

								{	/* Llib/weakhash.scm 153 */
									obj_t BgL_keyz00_1301;

									{	/* Llib/weakhash.scm 153 */
										obj_t BgL_arg1314z00_1303;

										{	/* Llib/weakhash.scm 153 */
											obj_t BgL_pairz00_2564;

											BgL_pairz00_2564 = CAR(((obj_t) BgL_bucketz00_1294));
											BgL_arg1314z00_1303 = CAR(BgL_pairz00_2564);
										}
										BgL_keyz00_1301 =
											bgl_weakptr_data(((obj_t) BgL_arg1314z00_1303));
									}
									if ((BgL_keyz00_1301 == BUNSPEC))
										{	/* Llib/weakhash.scm 154 */
											BgL_retz00_1298 = BGl_removez00zz__weakhashz00;
										}
									else
										{	/* Llib/weakhash.scm 156 */
											obj_t BgL_arg1312z00_1302;

											{	/* Llib/weakhash.scm 156 */
												obj_t BgL_pairz00_2569;

												BgL_pairz00_2569 = CAR(((obj_t) BgL_bucketz00_1294));
												BgL_arg1312z00_1302 = CDR(BgL_pairz00_2569);
											}
											BgL_retz00_1298 =
												((obj_t(*)(obj_t, obj_t, obj_t,
														obj_t))
												PROCEDURE_L_ENTRY(BgL_funz00_50)) (BgL_funz00_50,
												BgL_keyz00_1301, BgL_arg1312z00_1302,
												BgL_bucketz00_1294);
										}
								}
								if ((BgL_retz00_1298 == BGl_keepgoingz00zz__weakhashz00))
									{	/* Llib/weakhash.scm 145 */
										obj_t BgL_arg1310z00_1299;

										BgL_arg1310z00_1299 = CDR(((obj_t) BgL_bucketz00_1294));
										{
											obj_t BgL_lastzd2bucketzd2_4343;
											obj_t BgL_bucketz00_4342;

											BgL_bucketz00_4342 = BgL_arg1310z00_1299;
											BgL_lastzd2bucketzd2_4343 = BgL_bucketz00_1294;
											BgL_lastzd2bucketzd2_1295 = BgL_lastzd2bucketzd2_4343;
											BgL_bucketz00_1294 = BgL_bucketz00_4342;
											goto BgL_zc3z04anonymousza31308ze3z87_1296;
										}
									}
								else
									{	/* Llib/weakhash.scm 145 */
										if ((BgL_retz00_1298 == BGl_removez00zz__weakhashz00))
											{	/* Llib/weakhash.scm 145 */
												{	/* Llib/weakhash.scm 134 */
													long BgL_arg1272z00_2571;

													BgL_arg1272z00_2571 =
														(
														(long) CINT(STRUCT_REF(BgL_tablez00_47,
																(int) (0L))) - 1L);
													{	/* Llib/weakhash.scm 134 */
														obj_t BgL_auxz00_4352;
														int BgL_tmpz00_4350;

														BgL_auxz00_4352 = BINT(BgL_arg1272z00_2571);
														BgL_tmpz00_4350 = (int) (0L);
														STRUCT_SET(BgL_tablez00_47, BgL_tmpz00_4350,
															BgL_auxz00_4352);
												}}
												if (CBOOL(BgL_lastzd2bucketzd2_1295))
													{	/* Llib/weakhash.scm 136 */
														obj_t BgL_arg1304z00_2573;

														BgL_arg1304z00_2573 =
															CDR(((obj_t) BgL_bucketz00_1294));
														{	/* Llib/weakhash.scm 136 */
															obj_t BgL_tmpz00_4359;

															BgL_tmpz00_4359 =
																((obj_t) BgL_lastzd2bucketzd2_1295);
															SET_CDR(BgL_tmpz00_4359, BgL_arg1304z00_2573);
														}
													}
												else
													{	/* Llib/weakhash.scm 137 */
														obj_t BgL_arg1305z00_2574;

														BgL_arg1305z00_2574 =
															CDR(((obj_t) BgL_bucketz00_1294));
														VECTOR_SET(
															((obj_t) BgL_bucketsz00_48), BgL_iz00_49,
															BgL_arg1305z00_2574);
													}
												BGl_keepgoingz00zz__weakhashz00;
												{	/* Llib/weakhash.scm 145 */
													obj_t BgL_arg1311z00_1300;

													BgL_arg1311z00_1300 =
														CDR(((obj_t) BgL_bucketz00_1294));
													{
														obj_t BgL_bucketz00_4368;

														BgL_bucketz00_4368 = BgL_arg1311z00_1300;
														BgL_bucketz00_1294 = BgL_bucketz00_4368;
														goto BgL_zc3z04anonymousza31308ze3z87_1296;
													}
												}
											}
										else
											{	/* Llib/weakhash.scm 145 */
												if (
													(BgL_retz00_1298 == BGl_removestopz00zz__weakhashz00))
													{	/* Llib/weakhash.scm 145 */
														{	/* Llib/weakhash.scm 134 */
															long BgL_arg1272z00_2584;

															BgL_arg1272z00_2584 =
																(
																(long) CINT(STRUCT_REF(BgL_tablez00_47,
																		(int) (0L))) - 1L);
															{	/* Llib/weakhash.scm 134 */
																obj_t BgL_auxz00_4377;
																int BgL_tmpz00_4375;

																BgL_auxz00_4377 = BINT(BgL_arg1272z00_2584);
																BgL_tmpz00_4375 = (int) (0L);
																STRUCT_SET(BgL_tablez00_47, BgL_tmpz00_4375,
																	BgL_auxz00_4377);
														}}
														if (CBOOL(BgL_lastzd2bucketzd2_1295))
															{	/* Llib/weakhash.scm 136 */
																obj_t BgL_arg1304z00_2586;

																BgL_arg1304z00_2586 =
																	CDR(((obj_t) BgL_bucketz00_1294));
																{	/* Llib/weakhash.scm 136 */
																	obj_t BgL_tmpz00_4384;

																	BgL_tmpz00_4384 =
																		((obj_t) BgL_lastzd2bucketzd2_1295);
																	SET_CDR(BgL_tmpz00_4384, BgL_arg1304z00_2586);
																}
															}
														else
															{	/* Llib/weakhash.scm 137 */
																obj_t BgL_arg1305z00_2587;

																BgL_arg1305z00_2587 =
																	CDR(((obj_t) BgL_bucketz00_1294));
																VECTOR_SET(
																	((obj_t) BgL_bucketsz00_48), BgL_iz00_49,
																	BgL_arg1305z00_2587);
															}
														return BGl_keepgoingz00zz__weakhashz00;
													}
												else
													{	/* Llib/weakhash.scm 145 */
														return BgL_retz00_1298;
													}
											}
									}
							}
					}
				}
			else
				{	/* Llib/weakhash.scm 145 */
					if ((2L == (long) CINT(STRUCT_REF(BgL_tablez00_47, (int) (5L)))))
						{	/* Llib/weakhash.scm 145 */
							obj_t BgL_g1061z00_1307;

							BgL_g1061z00_1307 =
								VECTOR_REF(((obj_t) BgL_bucketsz00_48), BgL_iz00_49);
							{
								obj_t BgL_bucketz00_1309;
								obj_t BgL_lastzd2bucketzd2_1310;

								BgL_bucketz00_1309 = BgL_g1061z00_1307;
								BgL_lastzd2bucketzd2_1310 = BFALSE;
							BgL_zc3z04anonymousza31317ze3z87_1311:
								if (NULLP(BgL_bucketz00_1309))
									{	/* Llib/weakhash.scm 145 */
										return BGl_keepgoingz00zz__weakhashz00;
									}
								else
									{	/* Llib/weakhash.scm 145 */
										obj_t BgL_retz00_1313;

										{	/* Llib/weakhash.scm 159 */
											obj_t BgL_dataz00_1316;

											{	/* Llib/weakhash.scm 159 */
												obj_t BgL_arg1322z00_1318;

												{	/* Llib/weakhash.scm 159 */
													obj_t BgL_pairz00_2603;

													BgL_pairz00_2603 = CAR(((obj_t) BgL_bucketz00_1309));
													BgL_arg1322z00_1318 = CDR(BgL_pairz00_2603);
												}
												BgL_dataz00_1316 =
													bgl_weakptr_data(((obj_t) BgL_arg1322z00_1318));
											}
											if ((BgL_dataz00_1316 == BUNSPEC))
												{	/* Llib/weakhash.scm 160 */
													BgL_retz00_1313 = BGl_removez00zz__weakhashz00;
												}
											else
												{	/* Llib/weakhash.scm 162 */
													obj_t BgL_arg1321z00_1317;

													{	/* Llib/weakhash.scm 162 */
														obj_t BgL_pairz00_2608;

														BgL_pairz00_2608 =
															CAR(((obj_t) BgL_bucketz00_1309));
														BgL_arg1321z00_1317 = CAR(BgL_pairz00_2608);
													}
													BgL_retz00_1313 =
														((obj_t(*)(obj_t, obj_t, obj_t,
																obj_t))
														PROCEDURE_L_ENTRY(BgL_funz00_50)) (BgL_funz00_50,
														BgL_arg1321z00_1317, BgL_dataz00_1316,
														BgL_bucketz00_1309);
												}
										}
										if ((BgL_retz00_1313 == BGl_keepgoingz00zz__weakhashz00))
											{	/* Llib/weakhash.scm 145 */
												obj_t BgL_arg1319z00_1314;

												BgL_arg1319z00_1314 = CDR(((obj_t) BgL_bucketz00_1309));
												{
													obj_t BgL_lastzd2bucketzd2_4421;
													obj_t BgL_bucketz00_4420;

													BgL_bucketz00_4420 = BgL_arg1319z00_1314;
													BgL_lastzd2bucketzd2_4421 = BgL_bucketz00_1309;
													BgL_lastzd2bucketzd2_1310 = BgL_lastzd2bucketzd2_4421;
													BgL_bucketz00_1309 = BgL_bucketz00_4420;
													goto BgL_zc3z04anonymousza31317ze3z87_1311;
												}
											}
										else
											{	/* Llib/weakhash.scm 145 */
												if ((BgL_retz00_1313 == BGl_removez00zz__weakhashz00))
													{	/* Llib/weakhash.scm 145 */
														{	/* Llib/weakhash.scm 134 */
															long BgL_arg1272z00_2610;

															BgL_arg1272z00_2610 =
																(
																(long) CINT(STRUCT_REF(BgL_tablez00_47,
																		(int) (0L))) - 1L);
															{	/* Llib/weakhash.scm 134 */
																obj_t BgL_auxz00_4430;
																int BgL_tmpz00_4428;

																BgL_auxz00_4430 = BINT(BgL_arg1272z00_2610);
																BgL_tmpz00_4428 = (int) (0L);
																STRUCT_SET(BgL_tablez00_47, BgL_tmpz00_4428,
																	BgL_auxz00_4430);
														}}
														if (CBOOL(BgL_lastzd2bucketzd2_1310))
															{	/* Llib/weakhash.scm 136 */
																obj_t BgL_arg1304z00_2612;

																BgL_arg1304z00_2612 =
																	CDR(((obj_t) BgL_bucketz00_1309));
																{	/* Llib/weakhash.scm 136 */
																	obj_t BgL_tmpz00_4437;

																	BgL_tmpz00_4437 =
																		((obj_t) BgL_lastzd2bucketzd2_1310);
																	SET_CDR(BgL_tmpz00_4437, BgL_arg1304z00_2612);
																}
															}
														else
															{	/* Llib/weakhash.scm 137 */
																obj_t BgL_arg1305z00_2613;

																BgL_arg1305z00_2613 =
																	CDR(((obj_t) BgL_bucketz00_1309));
																VECTOR_SET(
																	((obj_t) BgL_bucketsz00_48), BgL_iz00_49,
																	BgL_arg1305z00_2613);
															}
														BGl_keepgoingz00zz__weakhashz00;
														{	/* Llib/weakhash.scm 145 */
															obj_t BgL_arg1320z00_1315;

															BgL_arg1320z00_1315 =
																CDR(((obj_t) BgL_bucketz00_1309));
															{
																obj_t BgL_bucketz00_4446;

																BgL_bucketz00_4446 = BgL_arg1320z00_1315;
																BgL_bucketz00_1309 = BgL_bucketz00_4446;
																goto BgL_zc3z04anonymousza31317ze3z87_1311;
															}
														}
													}
												else
													{	/* Llib/weakhash.scm 145 */
														if (
															(BgL_retz00_1313 ==
																BGl_removestopz00zz__weakhashz00))
															{	/* Llib/weakhash.scm 145 */
																{	/* Llib/weakhash.scm 134 */
																	long BgL_arg1272z00_2623;

																	BgL_arg1272z00_2623 =
																		(
																		(long) CINT(STRUCT_REF(BgL_tablez00_47,
																				(int) (0L))) - 1L);
																	{	/* Llib/weakhash.scm 134 */
																		obj_t BgL_auxz00_4455;
																		int BgL_tmpz00_4453;

																		BgL_auxz00_4455 = BINT(BgL_arg1272z00_2623);
																		BgL_tmpz00_4453 = (int) (0L);
																		STRUCT_SET(BgL_tablez00_47, BgL_tmpz00_4453,
																			BgL_auxz00_4455);
																}}
																if (CBOOL(BgL_lastzd2bucketzd2_1310))
																	{	/* Llib/weakhash.scm 136 */
																		obj_t BgL_arg1304z00_2625;

																		BgL_arg1304z00_2625 =
																			CDR(((obj_t) BgL_bucketz00_1309));
																		{	/* Llib/weakhash.scm 136 */
																			obj_t BgL_tmpz00_4462;

																			BgL_tmpz00_4462 =
																				((obj_t) BgL_lastzd2bucketzd2_1310);
																			SET_CDR(BgL_tmpz00_4462,
																				BgL_arg1304z00_2625);
																		}
																	}
																else
																	{	/* Llib/weakhash.scm 137 */
																		obj_t BgL_arg1305z00_2626;

																		BgL_arg1305z00_2626 =
																			CDR(((obj_t) BgL_bucketz00_1309));
																		VECTOR_SET(
																			((obj_t) BgL_bucketsz00_48), BgL_iz00_49,
																			BgL_arg1305z00_2626);
																	}
																return BGl_keepgoingz00zz__weakhashz00;
															}
														else
															{	/* Llib/weakhash.scm 145 */
																return BgL_retz00_1313;
															}
													}
											}
									}
							}
						}
					else
						{	/* Llib/weakhash.scm 145 */
							if ((3L == (long) CINT(STRUCT_REF(BgL_tablez00_47, (int) (5L)))))
								{	/* Llib/weakhash.scm 145 */
									obj_t BgL_g1062z00_1322;

									BgL_g1062z00_1322 =
										VECTOR_REF(((obj_t) BgL_bucketsz00_48), BgL_iz00_49);
									{
										obj_t BgL_bucketz00_1324;
										obj_t BgL_lastzd2bucketzd2_1325;

										BgL_bucketz00_1324 = BgL_g1062z00_1322;
										BgL_lastzd2bucketzd2_1325 = BFALSE;
									BgL_zc3z04anonymousza31325ze3z87_1326:
										if (NULLP(BgL_bucketz00_1324))
											{	/* Llib/weakhash.scm 145 */
												return BGl_keepgoingz00zz__weakhashz00;
											}
										else
											{	/* Llib/weakhash.scm 145 */
												obj_t BgL_retz00_1328;

												{	/* Llib/weakhash.scm 165 */
													obj_t BgL_keyz00_1331;
													obj_t BgL_dataz00_1332;

													{	/* Llib/weakhash.scm 165 */
														obj_t BgL_arg1331z00_1334;

														{	/* Llib/weakhash.scm 165 */
															obj_t BgL_pairz00_2642;

															BgL_pairz00_2642 =
																CAR(((obj_t) BgL_bucketz00_1324));
															BgL_arg1331z00_1334 = CAR(BgL_pairz00_2642);
														}
														BgL_keyz00_1331 =
															bgl_weakptr_data(((obj_t) BgL_arg1331z00_1334));
													}
													{	/* Llib/weakhash.scm 166 */
														obj_t BgL_arg1332z00_1335;

														{	/* Llib/weakhash.scm 166 */
															obj_t BgL_pairz00_2647;

															BgL_pairz00_2647 =
																CAR(((obj_t) BgL_bucketz00_1324));
															BgL_arg1332z00_1335 = CDR(BgL_pairz00_2647);
														}
														BgL_dataz00_1332 =
															bgl_weakptr_data(((obj_t) BgL_arg1332z00_1335));
													}
													{	/* Llib/weakhash.scm 167 */
														bool_t BgL_test2242z00_4488;

														if ((BgL_keyz00_1331 == BUNSPEC))
															{	/* Llib/weakhash.scm 167 */
																BgL_test2242z00_4488 = ((bool_t) 1);
															}
														else
															{	/* Llib/weakhash.scm 167 */
																BgL_test2242z00_4488 =
																	(BgL_dataz00_1332 == BUNSPEC);
															}
														if (BgL_test2242z00_4488)
															{	/* Llib/weakhash.scm 167 */
																BgL_retz00_1328 = BGl_removez00zz__weakhashz00;
															}
														else
															{	/* Llib/weakhash.scm 167 */
																BgL_retz00_1328 =
																	((obj_t(*)(obj_t, obj_t, obj_t,
																			obj_t))
																	PROCEDURE_L_ENTRY(BgL_funz00_50))
																	(BgL_funz00_50, BgL_keyz00_1331,
																	BgL_dataz00_1332, BgL_bucketz00_1324);
															}
													}
												}
												if (
													(BgL_retz00_1328 == BGl_keepgoingz00zz__weakhashz00))
													{	/* Llib/weakhash.scm 145 */
														obj_t BgL_arg1327z00_1329;

														BgL_arg1327z00_1329 =
															CDR(((obj_t) BgL_bucketz00_1324));
														{
															obj_t BgL_lastzd2bucketzd2_4503;
															obj_t BgL_bucketz00_4502;

															BgL_bucketz00_4502 = BgL_arg1327z00_1329;
															BgL_lastzd2bucketzd2_4503 = BgL_bucketz00_1324;
															BgL_lastzd2bucketzd2_1325 =
																BgL_lastzd2bucketzd2_4503;
															BgL_bucketz00_1324 = BgL_bucketz00_4502;
															goto BgL_zc3z04anonymousza31325ze3z87_1326;
														}
													}
												else
													{	/* Llib/weakhash.scm 145 */
														if (
															(BgL_retz00_1328 == BGl_removez00zz__weakhashz00))
															{	/* Llib/weakhash.scm 145 */
																{	/* Llib/weakhash.scm 134 */
																	long BgL_arg1272z00_2650;

																	BgL_arg1272z00_2650 =
																		(
																		(long) CINT(STRUCT_REF(BgL_tablez00_47,
																				(int) (0L))) - 1L);
																	{	/* Llib/weakhash.scm 134 */
																		obj_t BgL_auxz00_4512;
																		int BgL_tmpz00_4510;

																		BgL_auxz00_4512 = BINT(BgL_arg1272z00_2650);
																		BgL_tmpz00_4510 = (int) (0L);
																		STRUCT_SET(BgL_tablez00_47, BgL_tmpz00_4510,
																			BgL_auxz00_4512);
																}}
																if (CBOOL(BgL_lastzd2bucketzd2_1325))
																	{	/* Llib/weakhash.scm 136 */
																		obj_t BgL_arg1304z00_2652;

																		BgL_arg1304z00_2652 =
																			CDR(((obj_t) BgL_bucketz00_1324));
																		{	/* Llib/weakhash.scm 136 */
																			obj_t BgL_tmpz00_4519;

																			BgL_tmpz00_4519 =
																				((obj_t) BgL_lastzd2bucketzd2_1325);
																			SET_CDR(BgL_tmpz00_4519,
																				BgL_arg1304z00_2652);
																		}
																	}
																else
																	{	/* Llib/weakhash.scm 137 */
																		obj_t BgL_arg1305z00_2653;

																		BgL_arg1305z00_2653 =
																			CDR(((obj_t) BgL_bucketz00_1324));
																		VECTOR_SET(
																			((obj_t) BgL_bucketsz00_48), BgL_iz00_49,
																			BgL_arg1305z00_2653);
																	}
																BGl_keepgoingz00zz__weakhashz00;
																{	/* Llib/weakhash.scm 145 */
																	obj_t BgL_arg1328z00_1330;

																	BgL_arg1328z00_1330 =
																		CDR(((obj_t) BgL_bucketz00_1324));
																	{
																		obj_t BgL_bucketz00_4528;

																		BgL_bucketz00_4528 = BgL_arg1328z00_1330;
																		BgL_bucketz00_1324 = BgL_bucketz00_4528;
																		goto BgL_zc3z04anonymousza31325ze3z87_1326;
																	}
																}
															}
														else
															{	/* Llib/weakhash.scm 145 */
																if (
																	(BgL_retz00_1328 ==
																		BGl_removestopz00zz__weakhashz00))
																	{	/* Llib/weakhash.scm 145 */
																		{	/* Llib/weakhash.scm 134 */
																			long BgL_arg1272z00_2663;

																			BgL_arg1272z00_2663 =
																				(
																				(long) CINT(STRUCT_REF(BgL_tablez00_47,
																						(int) (0L))) - 1L);
																			{	/* Llib/weakhash.scm 134 */
																				obj_t BgL_auxz00_4537;
																				int BgL_tmpz00_4535;

																				BgL_auxz00_4537 =
																					BINT(BgL_arg1272z00_2663);
																				BgL_tmpz00_4535 = (int) (0L);
																				STRUCT_SET(BgL_tablez00_47,
																					BgL_tmpz00_4535, BgL_auxz00_4537);
																		}}
																		if (CBOOL(BgL_lastzd2bucketzd2_1325))
																			{	/* Llib/weakhash.scm 136 */
																				obj_t BgL_arg1304z00_2665;

																				BgL_arg1304z00_2665 =
																					CDR(((obj_t) BgL_bucketz00_1324));
																				{	/* Llib/weakhash.scm 136 */
																					obj_t BgL_tmpz00_4544;

																					BgL_tmpz00_4544 =
																						((obj_t) BgL_lastzd2bucketzd2_1325);
																					SET_CDR(BgL_tmpz00_4544,
																						BgL_arg1304z00_2665);
																				}
																			}
																		else
																			{	/* Llib/weakhash.scm 137 */
																				obj_t BgL_arg1305z00_2666;

																				BgL_arg1305z00_2666 =
																					CDR(((obj_t) BgL_bucketz00_1324));
																				VECTOR_SET(
																					((obj_t) BgL_bucketsz00_48),
																					BgL_iz00_49, BgL_arg1305z00_2666);
																			}
																		return BGl_keepgoingz00zz__weakhashz00;
																	}
																else
																	{	/* Llib/weakhash.scm 145 */
																		return BgL_retz00_1328;
																	}
															}
													}
											}
									}
								}
							else
								{	/* Llib/weakhash.scm 145 */
									obj_t BgL_g1063z00_1337;

									BgL_g1063z00_1337 =
										VECTOR_REF(((obj_t) BgL_bucketsz00_48), BgL_iz00_49);
									{
										obj_t BgL_bucketz00_1339;
										obj_t BgL_lastzd2bucketzd2_1340;

										BgL_bucketz00_1339 = BgL_g1063z00_1337;
										BgL_lastzd2bucketzd2_1340 = BFALSE;
									BgL_zc3z04anonymousza31333ze3z87_1341:
										if (NULLP(BgL_bucketz00_1339))
											{	/* Llib/weakhash.scm 145 */
												return BGl_keepgoingz00zz__weakhashz00;
											}
										else
											{	/* Llib/weakhash.scm 145 */
												obj_t BgL_retz00_1343;

												{	/* Llib/weakhash.scm 173 */
													obj_t BgL_arg1337z00_1346;
													obj_t BgL_arg1338z00_1347;

													{	/* Llib/weakhash.scm 173 */
														obj_t BgL_pairz00_2680;

														BgL_pairz00_2680 =
															CAR(((obj_t) BgL_bucketz00_1339));
														BgL_arg1337z00_1346 = CAR(BgL_pairz00_2680);
													}
													{	/* Llib/weakhash.scm 173 */
														obj_t BgL_pairz00_2684;

														BgL_pairz00_2684 =
															CAR(((obj_t) BgL_bucketz00_1339));
														BgL_arg1338z00_1347 = CDR(BgL_pairz00_2684);
													}
													BgL_retz00_1343 =
														((obj_t(*)(obj_t, obj_t, obj_t,
																obj_t))
														PROCEDURE_L_ENTRY(BgL_funz00_50)) (BgL_funz00_50,
														BgL_arg1337z00_1346, BgL_arg1338z00_1347,
														BgL_bucketz00_1339);
												}
												if (
													(BgL_retz00_1343 == BGl_keepgoingz00zz__weakhashz00))
													{	/* Llib/weakhash.scm 145 */
														obj_t BgL_arg1335z00_1344;

														BgL_arg1335z00_1344 =
															CDR(((obj_t) BgL_bucketz00_1339));
														{
															obj_t BgL_lastzd2bucketzd2_4572;
															obj_t BgL_bucketz00_4571;

															BgL_bucketz00_4571 = BgL_arg1335z00_1344;
															BgL_lastzd2bucketzd2_4572 = BgL_bucketz00_1339;
															BgL_lastzd2bucketzd2_1340 =
																BgL_lastzd2bucketzd2_4572;
															BgL_bucketz00_1339 = BgL_bucketz00_4571;
															goto BgL_zc3z04anonymousza31333ze3z87_1341;
														}
													}
												else
													{	/* Llib/weakhash.scm 145 */
														if (
															(BgL_retz00_1343 == BGl_removez00zz__weakhashz00))
															{	/* Llib/weakhash.scm 145 */
																{	/* Llib/weakhash.scm 134 */
																	long BgL_arg1272z00_2686;

																	BgL_arg1272z00_2686 =
																		(
																		(long) CINT(STRUCT_REF(BgL_tablez00_47,
																				(int) (0L))) - 1L);
																	{	/* Llib/weakhash.scm 134 */
																		obj_t BgL_auxz00_4581;
																		int BgL_tmpz00_4579;

																		BgL_auxz00_4581 = BINT(BgL_arg1272z00_2686);
																		BgL_tmpz00_4579 = (int) (0L);
																		STRUCT_SET(BgL_tablez00_47, BgL_tmpz00_4579,
																			BgL_auxz00_4581);
																}}
																if (CBOOL(BgL_lastzd2bucketzd2_1340))
																	{	/* Llib/weakhash.scm 136 */
																		obj_t BgL_arg1304z00_2688;

																		BgL_arg1304z00_2688 =
																			CDR(((obj_t) BgL_bucketz00_1339));
																		{	/* Llib/weakhash.scm 136 */
																			obj_t BgL_tmpz00_4588;

																			BgL_tmpz00_4588 =
																				((obj_t) BgL_lastzd2bucketzd2_1340);
																			SET_CDR(BgL_tmpz00_4588,
																				BgL_arg1304z00_2688);
																		}
																	}
																else
																	{	/* Llib/weakhash.scm 137 */
																		obj_t BgL_arg1305z00_2689;

																		BgL_arg1305z00_2689 =
																			CDR(((obj_t) BgL_bucketz00_1339));
																		VECTOR_SET(
																			((obj_t) BgL_bucketsz00_48), BgL_iz00_49,
																			BgL_arg1305z00_2689);
																	}
																BGl_keepgoingz00zz__weakhashz00;
																{	/* Llib/weakhash.scm 145 */
																	obj_t BgL_arg1336z00_1345;

																	BgL_arg1336z00_1345 =
																		CDR(((obj_t) BgL_bucketz00_1339));
																	{
																		obj_t BgL_bucketz00_4597;

																		BgL_bucketz00_4597 = BgL_arg1336z00_1345;
																		BgL_bucketz00_1339 = BgL_bucketz00_4597;
																		goto BgL_zc3z04anonymousza31333ze3z87_1341;
																	}
																}
															}
														else
															{	/* Llib/weakhash.scm 145 */
																if (
																	(BgL_retz00_1343 ==
																		BGl_removestopz00zz__weakhashz00))
																	{	/* Llib/weakhash.scm 145 */
																		{	/* Llib/weakhash.scm 134 */
																			long BgL_arg1272z00_2699;

																			BgL_arg1272z00_2699 =
																				(
																				(long) CINT(STRUCT_REF(BgL_tablez00_47,
																						(int) (0L))) - 1L);
																			{	/* Llib/weakhash.scm 134 */
																				obj_t BgL_auxz00_4606;
																				int BgL_tmpz00_4604;

																				BgL_auxz00_4606 =
																					BINT(BgL_arg1272z00_2699);
																				BgL_tmpz00_4604 = (int) (0L);
																				STRUCT_SET(BgL_tablez00_47,
																					BgL_tmpz00_4604, BgL_auxz00_4606);
																		}}
																		if (CBOOL(BgL_lastzd2bucketzd2_1340))
																			{	/* Llib/weakhash.scm 136 */
																				obj_t BgL_arg1304z00_2701;

																				BgL_arg1304z00_2701 =
																					CDR(((obj_t) BgL_bucketz00_1339));
																				{	/* Llib/weakhash.scm 136 */
																					obj_t BgL_tmpz00_4613;

																					BgL_tmpz00_4613 =
																						((obj_t) BgL_lastzd2bucketzd2_1340);
																					SET_CDR(BgL_tmpz00_4613,
																						BgL_arg1304z00_2701);
																				}
																			}
																		else
																			{	/* Llib/weakhash.scm 137 */
																				obj_t BgL_arg1305z00_2702;

																				BgL_arg1305z00_2702 =
																					CDR(((obj_t) BgL_bucketz00_1339));
																				VECTOR_SET(
																					((obj_t) BgL_bucketsz00_48),
																					BgL_iz00_49, BgL_arg1305z00_2702);
																			}
																		return BGl_keepgoingz00zz__weakhashz00;
																	}
																else
																	{	/* Llib/weakhash.scm 145 */
																		return BgL_retz00_1343;
																	}
															}
													}
											}
									}
								}
						}
				}
		}

	}



/* keys-traverse-hash */
	bool_t BGl_keyszd2traversezd2hashz00zz__weakhashz00(obj_t BgL_tablez00_51,
		obj_t BgL_funz00_52)
	{
		{	/* Llib/weakhash.scm 187 */
			BGl_weakzd2keyszd2hashtablezd2filterz12zc0zz__weakhashz00(BgL_tablez00_51,
				BGl_proc2160z00zz__weakhashz00);
			{	/* Llib/weakhash.scm 191 */
				obj_t BgL_bucketsz00_1357;

				BgL_bucketsz00_1357 = STRUCT_REF(BgL_tablez00_51, (int) (2L));
				{	/* Llib/weakhash.scm 192 */

					{
						long BgL_iz00_1360;

						BgL_iz00_1360 = 0L;
						if ((BgL_iz00_1360 == VECTOR_LENGTH(((obj_t) BgL_bucketsz00_1357))))
							{	/* Llib/weakhash.scm 194 */
								return ((bool_t) 0);
							}
						else
							{	/* Llib/weakhash.scm 195 */
								obj_t BgL_bucketz00_1363;

								BgL_bucketz00_1363 =
									VECTOR_REF(((obj_t) BgL_bucketsz00_1357), BgL_iz00_1360);
								{
									obj_t BgL_l1113z00_1365;

									BgL_l1113z00_1365 = BgL_bucketz00_1363;
								BgL_zc3z04anonymousza31346ze3z87_1366:
									if (PAIRP(BgL_l1113z00_1365))
										{	/* Llib/weakhash.scm 196 */
											{	/* Llib/weakhash.scm 197 */
												obj_t BgL_pz00_1368;

												BgL_pz00_1368 = CAR(BgL_l1113z00_1365);
												{	/* Llib/weakhash.scm 197 */
													bool_t BgL_test2257z00_4632;

													{	/* Llib/weakhash.scm 197 */
														obj_t BgL_arg1352z00_1373;

														BgL_arg1352z00_1373 =
															bgl_weakptr_data(((obj_t) BgL_pz00_1368));
														BgL_test2257z00_4632 =
															(BgL_arg1352z00_1373 == BUNSPEC);
													}
													if (BgL_test2257z00_4632)
														{	/* Llib/weakhash.scm 197 */
															BFALSE;
														}
													else
														{	/* Llib/weakhash.scm 198 */
															obj_t BgL_arg1350z00_1371;
															obj_t BgL_arg1351z00_1372;

															BgL_arg1350z00_1371 =
																bgl_weakptr_data(((obj_t) BgL_pz00_1368));
															BgL_arg1351z00_1372 =
																bgl_weakptr_ref(((obj_t) BgL_pz00_1368));
															BGL_PROCEDURE_CALL2(BgL_funz00_52,
																BgL_arg1350z00_1371, BgL_arg1351z00_1372);
														}
												}
											}
											{
												obj_t BgL_l1113z00_4645;

												BgL_l1113z00_4645 = CDR(BgL_l1113z00_1365);
												BgL_l1113z00_1365 = BgL_l1113z00_4645;
												goto BgL_zc3z04anonymousza31346ze3z87_1366;
											}
										}
									else
										{	/* Llib/weakhash.scm 196 */
											return ((bool_t) 1);
										}
								}
							}
					}
				}
			}
		}

	}



/* &<@anonymous:1343> */
	obj_t BGl_z62zc3z04anonymousza31343ze3ze5zz__weakhashz00(obj_t
		BgL_envz00_4055, obj_t BgL_kz00_4056, obj_t BgL_vz00_4057)
	{
		{	/* Llib/weakhash.scm 189 */
			return BBOOL(((bool_t) 1));
		}

	}



/* old-traverse-hash */
	bool_t BGl_oldzd2traversezd2hashz00zz__weakhashz00(obj_t BgL_tablez00_53,
		obj_t BgL_funz00_54)
	{
		{	/* Llib/weakhash.scm 204 */
			if ((1L == (long) CINT(STRUCT_REF(BgL_tablez00_53, (int) (5L)))))
				{	/* Llib/weakhash.scm 205 */
					obj_t BgL_bucketsz00_1379;

					BgL_bucketsz00_1379 = STRUCT_REF(BgL_tablez00_53, (int) (2L));
					{	/* Llib/weakhash.scm 205 */

						{
							long BgL_iz00_1382;

							BgL_iz00_1382 = 0L;
						BgL_zc3z04anonymousza31358ze3z87_1383:
							if (
								(BgL_iz00_1382 == VECTOR_LENGTH(((obj_t) BgL_bucketsz00_1379))))
								{	/* Llib/weakhash.scm 205 */
									return ((bool_t) 0);
								}
							else
								{	/* Llib/weakhash.scm 205 */
									{	/* Llib/weakhash.scm 205 */
										obj_t BgL_g1064z00_1385;

										BgL_g1064z00_1385 =
											VECTOR_REF(((obj_t) BgL_bucketsz00_1379), BgL_iz00_1382);
										{
											obj_t BgL_bucketz00_1387;
											obj_t BgL_lastzd2bucketzd2_1388;

											BgL_bucketz00_1387 = BgL_g1064z00_1385;
											BgL_lastzd2bucketzd2_1388 = BFALSE;
										BgL_zc3z04anonymousza31360ze3z87_1389:
											if (NULLP(BgL_bucketz00_1387))
												{	/* Llib/weakhash.scm 205 */
													((bool_t) 0);
												}
											else
												{	/* Llib/weakhash.scm 216 */
													obj_t BgL_retz00_1391;

													{	/* Llib/weakhash.scm 216 */
														obj_t BgL_keyz00_1394;

														{	/* Llib/weakhash.scm 216 */
															obj_t BgL_arg1365z00_1396;

															{	/* Llib/weakhash.scm 216 */
																obj_t BgL_pairz00_2733;

																BgL_pairz00_2733 =
																	CAR(((obj_t) BgL_bucketz00_1387));
																BgL_arg1365z00_1396 = CAR(BgL_pairz00_2733);
															}
															BgL_keyz00_1394 =
																bgl_weakptr_data(((obj_t) BgL_arg1365z00_1396));
														}
														if ((BgL_keyz00_1394 == BUNSPEC))
															{	/* Llib/weakhash.scm 217 */
																BgL_retz00_1391 = BGl_removez00zz__weakhashz00;
															}
														else
															{	/* Llib/weakhash.scm 219 */
																obj_t BgL_arg1364z00_1395;

																{	/* Llib/weakhash.scm 219 */
																	obj_t BgL_pairz00_2738;

																	BgL_pairz00_2738 =
																		CAR(((obj_t) BgL_bucketz00_1387));
																	BgL_arg1364z00_1395 = CDR(BgL_pairz00_2738);
																}
																BgL_retz00_1391 =
																	BGL_PROCEDURE_CALL2(BgL_funz00_54,
																	BgL_keyz00_1394, BgL_arg1364z00_1395);
															}
													}
													if ((BgL_retz00_1391 == BGl_removez00zz__weakhashz00))
														{	/* Llib/weakhash.scm 205 */
															{	/* Llib/weakhash.scm 134 */
																long BgL_arg1272z00_2739;

																BgL_arg1272z00_2739 =
																	(
																	(long) CINT(STRUCT_REF(BgL_tablez00_53,
																			(int) (0L))) - 1L);
																{	/* Llib/weakhash.scm 134 */
																	obj_t BgL_auxz00_4686;
																	int BgL_tmpz00_4684;

																	BgL_auxz00_4686 = BINT(BgL_arg1272z00_2739);
																	BgL_tmpz00_4684 = (int) (0L);
																	STRUCT_SET(BgL_tablez00_53, BgL_tmpz00_4684,
																		BgL_auxz00_4686);
															}}
															if (CBOOL(BgL_lastzd2bucketzd2_1388))
																{	/* Llib/weakhash.scm 136 */
																	obj_t BgL_arg1304z00_2741;

																	BgL_arg1304z00_2741 =
																		CDR(((obj_t) BgL_bucketz00_1387));
																	{	/* Llib/weakhash.scm 136 */
																		obj_t BgL_tmpz00_4693;

																		BgL_tmpz00_4693 =
																			((obj_t) BgL_lastzd2bucketzd2_1388);
																		SET_CDR(BgL_tmpz00_4693,
																			BgL_arg1304z00_2741);
																	}
																}
															else
																{	/* Llib/weakhash.scm 137 */
																	obj_t BgL_arg1305z00_2742;

																	BgL_arg1305z00_2742 =
																		CDR(((obj_t) BgL_bucketz00_1387));
																	VECTOR_SET(
																		((obj_t) BgL_bucketsz00_1379),
																		BgL_iz00_1382, BgL_arg1305z00_2742);
																}
															BGl_keepgoingz00zz__weakhashz00;
															{	/* Llib/weakhash.scm 205 */
																obj_t BgL_arg1362z00_1392;

																BgL_arg1362z00_1392 =
																	CDR(((obj_t) BgL_bucketz00_1387));
																{
																	obj_t BgL_bucketz00_4702;

																	BgL_bucketz00_4702 = BgL_arg1362z00_1392;
																	BgL_bucketz00_1387 = BgL_bucketz00_4702;
																	goto BgL_zc3z04anonymousza31360ze3z87_1389;
																}
															}
														}
													else
														{	/* Llib/weakhash.scm 205 */
															obj_t BgL_arg1363z00_1393;

															BgL_arg1363z00_1393 =
																CDR(((obj_t) BgL_bucketz00_1387));
															{
																obj_t BgL_lastzd2bucketzd2_4706;
																obj_t BgL_bucketz00_4705;

																BgL_bucketz00_4705 = BgL_arg1363z00_1393;
																BgL_lastzd2bucketzd2_4706 = BgL_bucketz00_1387;
																BgL_lastzd2bucketzd2_1388 =
																	BgL_lastzd2bucketzd2_4706;
																BgL_bucketz00_1387 = BgL_bucketz00_4705;
																goto BgL_zc3z04anonymousza31360ze3z87_1389;
															}
														}
												}
										}
									}
									{
										long BgL_iz00_4707;

										BgL_iz00_4707 = (BgL_iz00_1382 + 1L);
										BgL_iz00_1382 = BgL_iz00_4707;
										goto BgL_zc3z04anonymousza31358ze3z87_1383;
									}
								}
						}
					}
				}
			else
				{	/* Llib/weakhash.scm 205 */
					if ((2L == (long) CINT(STRUCT_REF(BgL_tablez00_53, (int) (5L)))))
						{	/* Llib/weakhash.scm 205 */
							obj_t BgL_bucketsz00_1402;

							BgL_bucketsz00_1402 = STRUCT_REF(BgL_tablez00_53, (int) (2L));
							{	/* Llib/weakhash.scm 205 */

								{
									long BgL_iz00_1405;

									BgL_iz00_1405 = 0L;
								BgL_zc3z04anonymousza31369ze3z87_1406:
									if (
										(BgL_iz00_1405 ==
											VECTOR_LENGTH(((obj_t) BgL_bucketsz00_1402))))
										{	/* Llib/weakhash.scm 205 */
											return ((bool_t) 0);
										}
									else
										{	/* Llib/weakhash.scm 205 */
											{	/* Llib/weakhash.scm 205 */
												obj_t BgL_g1065z00_1408;

												BgL_g1065z00_1408 =
													VECTOR_REF(
													((obj_t) BgL_bucketsz00_1402), BgL_iz00_1405);
												{
													obj_t BgL_bucketz00_1410;
													obj_t BgL_lastzd2bucketzd2_1411;

													BgL_bucketz00_1410 = BgL_g1065z00_1408;
													BgL_lastzd2bucketzd2_1411 = BFALSE;
												BgL_zc3z04anonymousza31371ze3z87_1412:
													if (NULLP(BgL_bucketz00_1410))
														{	/* Llib/weakhash.scm 205 */
															((bool_t) 0);
														}
													else
														{	/* Llib/weakhash.scm 222 */
															obj_t BgL_retz00_1414;

															{	/* Llib/weakhash.scm 222 */
																obj_t BgL_dataz00_1417;

																{	/* Llib/weakhash.scm 222 */
																	obj_t BgL_arg1377z00_1419;

																	{	/* Llib/weakhash.scm 222 */
																		obj_t BgL_pairz00_2765;

																		BgL_pairz00_2765 =
																			CAR(((obj_t) BgL_bucketz00_1410));
																		BgL_arg1377z00_1419 = CDR(BgL_pairz00_2765);
																	}
																	BgL_dataz00_1417 =
																		bgl_weakptr_data(
																		((obj_t) BgL_arg1377z00_1419));
																}
																if ((BgL_dataz00_1417 == BUNSPEC))
																	{	/* Llib/weakhash.scm 223 */
																		BgL_retz00_1414 =
																			BGl_removez00zz__weakhashz00;
																	}
																else
																	{	/* Llib/weakhash.scm 225 */
																		obj_t BgL_arg1376z00_1418;

																		{	/* Llib/weakhash.scm 225 */
																			obj_t BgL_pairz00_2770;

																			BgL_pairz00_2770 =
																				CAR(((obj_t) BgL_bucketz00_1410));
																			BgL_arg1376z00_1418 =
																				CAR(BgL_pairz00_2770);
																		}
																		BgL_retz00_1414 =
																			BGL_PROCEDURE_CALL2(BgL_funz00_54,
																			BgL_arg1376z00_1418, BgL_dataz00_1417);
																	}
															}
															if (
																(BgL_retz00_1414 ==
																	BGl_removez00zz__weakhashz00))
																{	/* Llib/weakhash.scm 205 */
																	{	/* Llib/weakhash.scm 134 */
																		long BgL_arg1272z00_2771;

																		BgL_arg1272z00_2771 =
																			(
																			(long) CINT(STRUCT_REF(BgL_tablez00_53,
																					(int) (0L))) - 1L);
																		{	/* Llib/weakhash.scm 134 */
																			obj_t BgL_auxz00_4747;
																			int BgL_tmpz00_4745;

																			BgL_auxz00_4747 =
																				BINT(BgL_arg1272z00_2771);
																			BgL_tmpz00_4745 = (int) (0L);
																			STRUCT_SET(BgL_tablez00_53,
																				BgL_tmpz00_4745, BgL_auxz00_4747);
																	}}
																	if (CBOOL(BgL_lastzd2bucketzd2_1411))
																		{	/* Llib/weakhash.scm 136 */
																			obj_t BgL_arg1304z00_2773;

																			BgL_arg1304z00_2773 =
																				CDR(((obj_t) BgL_bucketz00_1410));
																			{	/* Llib/weakhash.scm 136 */
																				obj_t BgL_tmpz00_4754;

																				BgL_tmpz00_4754 =
																					((obj_t) BgL_lastzd2bucketzd2_1411);
																				SET_CDR(BgL_tmpz00_4754,
																					BgL_arg1304z00_2773);
																			}
																		}
																	else
																		{	/* Llib/weakhash.scm 137 */
																			obj_t BgL_arg1305z00_2774;

																			BgL_arg1305z00_2774 =
																				CDR(((obj_t) BgL_bucketz00_1410));
																			VECTOR_SET(
																				((obj_t) BgL_bucketsz00_1402),
																				BgL_iz00_1405, BgL_arg1305z00_2774);
																		}
																	BGl_keepgoingz00zz__weakhashz00;
																	{	/* Llib/weakhash.scm 205 */
																		obj_t BgL_arg1373z00_1415;

																		BgL_arg1373z00_1415 =
																			CDR(((obj_t) BgL_bucketz00_1410));
																		{
																			obj_t BgL_bucketz00_4763;

																			BgL_bucketz00_4763 = BgL_arg1373z00_1415;
																			BgL_bucketz00_1410 = BgL_bucketz00_4763;
																			goto
																				BgL_zc3z04anonymousza31371ze3z87_1412;
																		}
																	}
																}
															else
																{	/* Llib/weakhash.scm 205 */
																	obj_t BgL_arg1375z00_1416;

																	BgL_arg1375z00_1416 =
																		CDR(((obj_t) BgL_bucketz00_1410));
																	{
																		obj_t BgL_lastzd2bucketzd2_4767;
																		obj_t BgL_bucketz00_4766;

																		BgL_bucketz00_4766 = BgL_arg1375z00_1416;
																		BgL_lastzd2bucketzd2_4767 =
																			BgL_bucketz00_1410;
																		BgL_lastzd2bucketzd2_1411 =
																			BgL_lastzd2bucketzd2_4767;
																		BgL_bucketz00_1410 = BgL_bucketz00_4766;
																		goto BgL_zc3z04anonymousza31371ze3z87_1412;
																	}
																}
														}
												}
											}
											{
												long BgL_iz00_4768;

												BgL_iz00_4768 = (BgL_iz00_1405 + 1L);
												BgL_iz00_1405 = BgL_iz00_4768;
												goto BgL_zc3z04anonymousza31369ze3z87_1406;
											}
										}
								}
							}
						}
					else
						{	/* Llib/weakhash.scm 205 */
							if ((3L == (long) CINT(STRUCT_REF(BgL_tablez00_53, (int) (5L)))))
								{	/* Llib/weakhash.scm 205 */
									obj_t BgL_bucketsz00_1425;

									BgL_bucketsz00_1425 = STRUCT_REF(BgL_tablez00_53, (int) (2L));
									{	/* Llib/weakhash.scm 205 */

										{
											long BgL_iz00_1428;

											BgL_iz00_1428 = 0L;
										BgL_zc3z04anonymousza31381ze3z87_1429:
											if (
												(BgL_iz00_1428 ==
													VECTOR_LENGTH(((obj_t) BgL_bucketsz00_1425))))
												{	/* Llib/weakhash.scm 205 */
													return ((bool_t) 0);
												}
											else
												{	/* Llib/weakhash.scm 205 */
													{	/* Llib/weakhash.scm 205 */
														obj_t BgL_g1066z00_1431;

														BgL_g1066z00_1431 =
															VECTOR_REF(
															((obj_t) BgL_bucketsz00_1425), BgL_iz00_1428);
														{
															obj_t BgL_bucketz00_1433;
															obj_t BgL_lastzd2bucketzd2_1434;

															BgL_bucketz00_1433 = BgL_g1066z00_1431;
															BgL_lastzd2bucketzd2_1434 = BFALSE;
														BgL_zc3z04anonymousza31383ze3z87_1435:
															if (NULLP(BgL_bucketz00_1433))
																{	/* Llib/weakhash.scm 205 */
																	((bool_t) 0);
																}
															else
																{	/* Llib/weakhash.scm 228 */
																	obj_t BgL_retz00_1437;

																	{	/* Llib/weakhash.scm 228 */
																		obj_t BgL_keyz00_1440;
																		obj_t BgL_dataz00_1441;

																		{	/* Llib/weakhash.scm 228 */
																			obj_t BgL_arg1390z00_1443;

																			{	/* Llib/weakhash.scm 228 */
																				obj_t BgL_pairz00_2797;

																				BgL_pairz00_2797 =
																					CAR(((obj_t) BgL_bucketz00_1433));
																				BgL_arg1390z00_1443 =
																					CAR(BgL_pairz00_2797);
																			}
																			BgL_keyz00_1440 =
																				bgl_weakptr_data(
																				((obj_t) BgL_arg1390z00_1443));
																		}
																		{	/* Llib/weakhash.scm 229 */
																			obj_t BgL_arg1391z00_1444;

																			{	/* Llib/weakhash.scm 229 */
																				obj_t BgL_pairz00_2802;

																				BgL_pairz00_2802 =
																					CAR(((obj_t) BgL_bucketz00_1433));
																				BgL_arg1391z00_1444 =
																					CDR(BgL_pairz00_2802);
																			}
																			BgL_dataz00_1441 =
																				bgl_weakptr_data(
																				((obj_t) BgL_arg1391z00_1444));
																		}
																		{	/* Llib/weakhash.scm 230 */
																			bool_t BgL_test2273z00_4795;

																			if ((BgL_keyz00_1440 == BUNSPEC))
																				{	/* Llib/weakhash.scm 230 */
																					BgL_test2273z00_4795 = ((bool_t) 1);
																				}
																			else
																				{	/* Llib/weakhash.scm 230 */
																					BgL_test2273z00_4795 =
																						(BgL_dataz00_1441 == BUNSPEC);
																				}
																			if (BgL_test2273z00_4795)
																				{	/* Llib/weakhash.scm 230 */
																					BgL_retz00_1437 =
																						BGl_removez00zz__weakhashz00;
																				}
																			else
																				{	/* Llib/weakhash.scm 230 */
																					BgL_retz00_1437 =
																						BGL_PROCEDURE_CALL2(BgL_funz00_54,
																						BgL_keyz00_1440, BgL_dataz00_1441);
																				}
																		}
																	}
																	if (
																		(BgL_retz00_1437 ==
																			BGl_removez00zz__weakhashz00))
																		{	/* Llib/weakhash.scm 205 */
																			{	/* Llib/weakhash.scm 134 */
																				long BgL_arg1272z00_2804;

																				BgL_arg1272z00_2804 =
																					(
																					(long)
																					CINT(STRUCT_REF(BgL_tablez00_53,
																							(int) (0L))) - 1L);
																				{	/* Llib/weakhash.scm 134 */
																					obj_t BgL_auxz00_4812;
																					int BgL_tmpz00_4810;

																					BgL_auxz00_4812 =
																						BINT(BgL_arg1272z00_2804);
																					BgL_tmpz00_4810 = (int) (0L);
																					STRUCT_SET(BgL_tablez00_53,
																						BgL_tmpz00_4810, BgL_auxz00_4812);
																			}}
																			if (CBOOL(BgL_lastzd2bucketzd2_1434))
																				{	/* Llib/weakhash.scm 136 */
																					obj_t BgL_arg1304z00_2806;

																					BgL_arg1304z00_2806 =
																						CDR(((obj_t) BgL_bucketz00_1433));
																					{	/* Llib/weakhash.scm 136 */
																						obj_t BgL_tmpz00_4819;

																						BgL_tmpz00_4819 =
																							((obj_t)
																							BgL_lastzd2bucketzd2_1434);
																						SET_CDR(BgL_tmpz00_4819,
																							BgL_arg1304z00_2806);
																					}
																				}
																			else
																				{	/* Llib/weakhash.scm 137 */
																					obj_t BgL_arg1305z00_2807;

																					BgL_arg1305z00_2807 =
																						CDR(((obj_t) BgL_bucketz00_1433));
																					VECTOR_SET(
																						((obj_t) BgL_bucketsz00_1425),
																						BgL_iz00_1428, BgL_arg1305z00_2807);
																				}
																			BGl_keepgoingz00zz__weakhashz00;
																			{	/* Llib/weakhash.scm 205 */
																				obj_t BgL_arg1387z00_1438;

																				BgL_arg1387z00_1438 =
																					CDR(((obj_t) BgL_bucketz00_1433));
																				{
																					obj_t BgL_bucketz00_4828;

																					BgL_bucketz00_4828 =
																						BgL_arg1387z00_1438;
																					BgL_bucketz00_1433 =
																						BgL_bucketz00_4828;
																					goto
																						BgL_zc3z04anonymousza31383ze3z87_1435;
																				}
																			}
																		}
																	else
																		{	/* Llib/weakhash.scm 205 */
																			obj_t BgL_arg1388z00_1439;

																			BgL_arg1388z00_1439 =
																				CDR(((obj_t) BgL_bucketz00_1433));
																			{
																				obj_t BgL_lastzd2bucketzd2_4832;
																				obj_t BgL_bucketz00_4831;

																				BgL_bucketz00_4831 =
																					BgL_arg1388z00_1439;
																				BgL_lastzd2bucketzd2_4832 =
																					BgL_bucketz00_1433;
																				BgL_lastzd2bucketzd2_1434 =
																					BgL_lastzd2bucketzd2_4832;
																				BgL_bucketz00_1433 = BgL_bucketz00_4831;
																				goto
																					BgL_zc3z04anonymousza31383ze3z87_1435;
																			}
																		}
																}
														}
													}
													{
														long BgL_iz00_4833;

														BgL_iz00_4833 = (BgL_iz00_1428 + 1L);
														BgL_iz00_1428 = BgL_iz00_4833;
														goto BgL_zc3z04anonymousza31381ze3z87_1429;
													}
												}
										}
									}
								}
							else
								{	/* Llib/weakhash.scm 205 */
									obj_t BgL_bucketsz00_1448;

									BgL_bucketsz00_1448 = STRUCT_REF(BgL_tablez00_53, (int) (2L));
									{	/* Llib/weakhash.scm 205 */

										{
											long BgL_iz00_1451;

											BgL_iz00_1451 = 0L;
										BgL_zc3z04anonymousza31393ze3z87_1452:
											if (
												(BgL_iz00_1451 ==
													VECTOR_LENGTH(((obj_t) BgL_bucketsz00_1448))))
												{	/* Llib/weakhash.scm 205 */
													return ((bool_t) 0);
												}
											else
												{	/* Llib/weakhash.scm 205 */
													{	/* Llib/weakhash.scm 205 */
														obj_t BgL_g1067z00_1454;

														BgL_g1067z00_1454 =
															VECTOR_REF(
															((obj_t) BgL_bucketsz00_1448), BgL_iz00_1451);
														{
															obj_t BgL_bucketz00_1456;
															obj_t BgL_lastzd2bucketzd2_1457;

															BgL_bucketz00_1456 = BgL_g1067z00_1454;
															BgL_lastzd2bucketzd2_1457 = BFALSE;
														BgL_zc3z04anonymousza31395ze3z87_1458:
															if (NULLP(BgL_bucketz00_1456))
																{	/* Llib/weakhash.scm 205 */
																	((bool_t) 0);
																}
															else
																{	/* Llib/weakhash.scm 236 */
																	obj_t BgL_retz00_1460;

																	{	/* Llib/weakhash.scm 236 */
																		obj_t BgL_arg1400z00_1463;
																		obj_t BgL_arg1401z00_1464;

																		{	/* Llib/weakhash.scm 236 */
																			obj_t BgL_pairz00_2828;

																			BgL_pairz00_2828 =
																				CAR(((obj_t) BgL_bucketz00_1456));
																			BgL_arg1400z00_1463 =
																				CAR(BgL_pairz00_2828);
																		}
																		{	/* Llib/weakhash.scm 236 */
																			obj_t BgL_pairz00_2832;

																			BgL_pairz00_2832 =
																				CAR(((obj_t) BgL_bucketz00_1456));
																			BgL_arg1401z00_1464 =
																				CDR(BgL_pairz00_2832);
																		}
																		BgL_retz00_1460 =
																			BGL_PROCEDURE_CALL2(BgL_funz00_54,
																			BgL_arg1400z00_1463, BgL_arg1401z00_1464);
																	}
																	if (
																		(BgL_retz00_1460 ==
																			BGl_removez00zz__weakhashz00))
																		{	/* Llib/weakhash.scm 205 */
																			{	/* Llib/weakhash.scm 134 */
																				long BgL_arg1272z00_2833;

																				BgL_arg1272z00_2833 =
																					(
																					(long)
																					CINT(STRUCT_REF(BgL_tablez00_53,
																							(int) (0L))) - 1L);
																				{	/* Llib/weakhash.scm 134 */
																					obj_t BgL_auxz00_4864;
																					int BgL_tmpz00_4862;

																					BgL_auxz00_4864 =
																						BINT(BgL_arg1272z00_2833);
																					BgL_tmpz00_4862 = (int) (0L);
																					STRUCT_SET(BgL_tablez00_53,
																						BgL_tmpz00_4862, BgL_auxz00_4864);
																			}}
																			if (CBOOL(BgL_lastzd2bucketzd2_1457))
																				{	/* Llib/weakhash.scm 136 */
																					obj_t BgL_arg1304z00_2835;

																					BgL_arg1304z00_2835 =
																						CDR(((obj_t) BgL_bucketz00_1456));
																					{	/* Llib/weakhash.scm 136 */
																						obj_t BgL_tmpz00_4871;

																						BgL_tmpz00_4871 =
																							((obj_t)
																							BgL_lastzd2bucketzd2_1457);
																						SET_CDR(BgL_tmpz00_4871,
																							BgL_arg1304z00_2835);
																					}
																				}
																			else
																				{	/* Llib/weakhash.scm 137 */
																					obj_t BgL_arg1305z00_2836;

																					BgL_arg1305z00_2836 =
																						CDR(((obj_t) BgL_bucketz00_1456));
																					VECTOR_SET(
																						((obj_t) BgL_bucketsz00_1448),
																						BgL_iz00_1451, BgL_arg1305z00_2836);
																				}
																			BGl_keepgoingz00zz__weakhashz00;
																			{	/* Llib/weakhash.scm 205 */
																				obj_t BgL_arg1397z00_1461;

																				BgL_arg1397z00_1461 =
																					CDR(((obj_t) BgL_bucketz00_1456));
																				{
																					obj_t BgL_bucketz00_4880;

																					BgL_bucketz00_4880 =
																						BgL_arg1397z00_1461;
																					BgL_bucketz00_1456 =
																						BgL_bucketz00_4880;
																					goto
																						BgL_zc3z04anonymousza31395ze3z87_1458;
																				}
																			}
																		}
																	else
																		{	/* Llib/weakhash.scm 205 */
																			obj_t BgL_arg1399z00_1462;

																			BgL_arg1399z00_1462 =
																				CDR(((obj_t) BgL_bucketz00_1456));
																			{
																				obj_t BgL_lastzd2bucketzd2_4884;
																				obj_t BgL_bucketz00_4883;

																				BgL_bucketz00_4883 =
																					BgL_arg1399z00_1462;
																				BgL_lastzd2bucketzd2_4884 =
																					BgL_bucketz00_1456;
																				BgL_lastzd2bucketzd2_1457 =
																					BgL_lastzd2bucketzd2_4884;
																				BgL_bucketz00_1456 = BgL_bucketz00_4883;
																				goto
																					BgL_zc3z04anonymousza31395ze3z87_1458;
																			}
																		}
																}
														}
													}
													{
														long BgL_iz00_4885;

														BgL_iz00_4885 = (BgL_iz00_1451 + 1L);
														BgL_iz00_1451 = BgL_iz00_4885;
														goto BgL_zc3z04anonymousza31393ze3z87_1452;
													}
												}
										}
									}
								}
						}
				}
		}

	}



/* weak-hashtable->vector */
	BGL_EXPORTED_DEF obj_t
		BGl_weakzd2hashtablezd2ze3vectorze3zz__weakhashz00(obj_t BgL_tablez00_57)
	{
		{	/* Llib/weakhash.scm 256 */
			{	/* Llib/weakhash.scm 257 */
				obj_t BgL_vecz00_1472;
				obj_t BgL_wz00_4067;

				BgL_vecz00_1472 =
					make_vector(BGl_hashtablezd2siza7ez75zz__hashz00(BgL_tablez00_57),
					BUNSPEC);
				BgL_wz00_4067 = MAKE_CELL(BINT(0L));
				{	/* Llib/weakhash.scm 261 */
					obj_t BgL_zc3z04anonymousza31408ze3z87_4058;

					BgL_zc3z04anonymousza31408ze3z87_4058 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza31408ze3ze5zz__weakhashz00, (int) (2L),
						(int) (2L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31408ze3z87_4058, (int) (0L),
						BgL_vecz00_1472);
					PROCEDURE_SET(BgL_zc3z04anonymousza31408ze3z87_4058, (int) (1L),
						((obj_t) BgL_wz00_4067));
					if (BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(BgL_tablez00_57))
						{	/* Llib/weakhash.scm 249 */
							BGl_keyszd2traversezd2hashz00zz__weakhashz00(BgL_tablez00_57,
								BgL_zc3z04anonymousza31408ze3z87_4058);
						}
					else
						{	/* Llib/weakhash.scm 249 */
							BGl_oldzd2traversezd2hashz00zz__weakhashz00(BgL_tablez00_57,
								BgL_zc3z04anonymousza31408ze3z87_4058);
						}
				}
				{	/* Llib/weakhash.scm 263 */
					bool_t BgL_test2282z00_4902;

					{	/* Llib/weakhash.scm 263 */
						long BgL_arg1412z00_1481;

						BgL_arg1412z00_1481 =
							BGl_hashtablezd2siza7ez75zz__hashz00(BgL_tablez00_57);
						BgL_test2282z00_4902 =
							((long) CINT(CELL_REF(BgL_wz00_4067)) < BgL_arg1412z00_1481);
					}
					if (BgL_test2282z00_4902)
						{	/* Llib/weakhash.scm 263 */
							return
								BGl_copyzd2vectorzd2zz__r4_vectors_6_8z00(BgL_vecz00_1472,
								(long) CINT(CELL_REF(BgL_wz00_4067)));
						}
					else
						{	/* Llib/weakhash.scm 263 */
							return BgL_vecz00_1472;
						}
				}
			}
		}

	}



/* &weak-hashtable->vector */
	obj_t BGl_z62weakzd2hashtablezd2ze3vectorz81zz__weakhashz00(obj_t
		BgL_envz00_4059, obj_t BgL_tablez00_4060)
	{
		{	/* Llib/weakhash.scm 256 */
			{	/* Llib/weakhash.scm 257 */
				obj_t BgL_auxz00_4908;

				if (STRUCTP(BgL_tablez00_4060))
					{	/* Llib/weakhash.scm 257 */
						BgL_auxz00_4908 = BgL_tablez00_4060;
					}
				else
					{
						obj_t BgL_auxz00_4911;

						BgL_auxz00_4911 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2161z00zz__weakhashz00,
							BINT(9857L), BGl_string2162z00zz__weakhashz00,
							BGl_string2163z00zz__weakhashz00, BgL_tablez00_4060);
						FAILURE(BgL_auxz00_4911, BFALSE, BFALSE);
					}
				return
					BGl_weakzd2hashtablezd2ze3vectorze3zz__weakhashz00(BgL_auxz00_4908);
			}
		}

	}



/* &<@anonymous:1408> */
	obj_t BGl_z62zc3z04anonymousza31408ze3ze5zz__weakhashz00(obj_t
		BgL_envz00_4061, obj_t BgL_keyz00_4064, obj_t BgL_valz00_4065)
	{
		{	/* Llib/weakhash.scm 260 */
			{	/* Llib/weakhash.scm 261 */
				obj_t BgL_vecz00_4062;
				obj_t BgL_wz00_4063;

				BgL_vecz00_4062 = ((obj_t) PROCEDURE_REF(BgL_envz00_4061, (int) (0L)));
				BgL_wz00_4063 = PROCEDURE_REF(BgL_envz00_4061, (int) (1L));
				{	/* Llib/weakhash.scm 261 */
					long BgL_kz00_4260;

					BgL_kz00_4260 = (long) CINT(CELL_REF(BgL_wz00_4063));
					VECTOR_SET(((obj_t) BgL_vecz00_4062), BgL_kz00_4260, BgL_valz00_4065);
				}
				{	/* Llib/weakhash.scm 262 */
					obj_t BgL_auxz00_4261;

					BgL_auxz00_4261 = ADDFX(CELL_REF(BgL_wz00_4063), BINT(1L));
					return CELL_SET(BgL_wz00_4063, BgL_auxz00_4261);
				}
			}
		}

	}



/* weak-hashtable->list */
	BGL_EXPORTED_DEF obj_t BGl_weakzd2hashtablezd2ze3listze3zz__weakhashz00(obj_t
		BgL_tablez00_58)
	{
		{	/* Llib/weakhash.scm 270 */
			{	/* Llib/weakhash.scm 271 */
				obj_t BgL_resz00_4077;

				BgL_resz00_4077 = MAKE_CELL(BNIL);
				{	/* Llib/weakhash.scm 274 */
					obj_t BgL_zc3z04anonymousza31416ze3z87_4069;

					BgL_zc3z04anonymousza31416ze3z87_4069 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza31416ze3ze5zz__weakhashz00, (int) (2L),
						(int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31416ze3z87_4069, (int) (0L),
						((obj_t) BgL_resz00_4077));
					if (BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(BgL_tablez00_58))
						{	/* Llib/weakhash.scm 249 */
							BGl_keyszd2traversezd2hashz00zz__weakhashz00(BgL_tablez00_58,
								BgL_zc3z04anonymousza31416ze3z87_4069);
						}
					else
						{	/* Llib/weakhash.scm 249 */
							BGl_oldzd2traversezd2hashz00zz__weakhashz00(BgL_tablez00_58,
								BgL_zc3z04anonymousza31416ze3z87_4069);
						}
				}
				return CELL_REF(BgL_resz00_4077);
			}
		}

	}



/* &weak-hashtable->list */
	obj_t BGl_z62weakzd2hashtablezd2ze3listz81zz__weakhashz00(obj_t
		BgL_envz00_4070, obj_t BgL_tablez00_4071)
	{
		{	/* Llib/weakhash.scm 270 */
			{	/* Llib/weakhash.scm 271 */
				obj_t BgL_auxz00_4936;

				if (STRUCTP(BgL_tablez00_4071))
					{	/* Llib/weakhash.scm 271 */
						BgL_auxz00_4936 = BgL_tablez00_4071;
					}
				else
					{
						obj_t BgL_auxz00_4939;

						BgL_auxz00_4939 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2161z00zz__weakhashz00,
							BINT(10362L), BGl_string2164z00zz__weakhashz00,
							BGl_string2163z00zz__weakhashz00, BgL_tablez00_4071);
						FAILURE(BgL_auxz00_4939, BFALSE, BFALSE);
					}
				return
					BGl_weakzd2hashtablezd2ze3listze3zz__weakhashz00(BgL_auxz00_4936);
			}
		}

	}



/* &<@anonymous:1416> */
	obj_t BGl_z62zc3z04anonymousza31416ze3ze5zz__weakhashz00(obj_t
		BgL_envz00_4072, obj_t BgL_keyz00_4074, obj_t BgL_valz00_4075)
	{
		{	/* Llib/weakhash.scm 273 */
			{	/* Llib/weakhash.scm 274 */
				obj_t BgL_resz00_4073;

				BgL_resz00_4073 = PROCEDURE_REF(BgL_envz00_4072, (int) (0L));
				{	/* Llib/weakhash.scm 274 */
					obj_t BgL_auxz00_4262;

					BgL_auxz00_4262 =
						MAKE_YOUNG_PAIR(BgL_valz00_4075, CELL_REF(BgL_resz00_4073));
					return CELL_SET(BgL_resz00_4073, BgL_auxz00_4262);
				}
			}
		}

	}



/* weak-hashtable-key-list */
	BGL_EXPORTED_DEF obj_t
		BGl_weakzd2hashtablezd2keyzd2listzd2zz__weakhashz00(obj_t BgL_tablez00_59)
	{
		{	/* Llib/weakhash.scm 280 */
			{	/* Llib/weakhash.scm 281 */
				obj_t BgL_resz00_4087;

				BgL_resz00_4087 = MAKE_CELL(BNIL);
				{	/* Llib/weakhash.scm 284 */
					obj_t BgL_zc3z04anonymousza31418ze3z87_4079;

					BgL_zc3z04anonymousza31418ze3z87_4079 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza31418ze3ze5zz__weakhashz00, (int) (2L),
						(int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31418ze3z87_4079, (int) (0L),
						((obj_t) BgL_resz00_4087));
					if (BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(BgL_tablez00_59))
						{	/* Llib/weakhash.scm 249 */
							BGl_keyszd2traversezd2hashz00zz__weakhashz00(BgL_tablez00_59,
								BgL_zc3z04anonymousza31418ze3z87_4079);
						}
					else
						{	/* Llib/weakhash.scm 249 */
							BGl_oldzd2traversezd2hashz00zz__weakhashz00(BgL_tablez00_59,
								BgL_zc3z04anonymousza31418ze3z87_4079);
						}
				}
				return CELL_REF(BgL_resz00_4087);
			}
		}

	}



/* &weak-hashtable-key-list */
	obj_t BGl_z62weakzd2hashtablezd2keyzd2listzb0zz__weakhashz00(obj_t
		BgL_envz00_4080, obj_t BgL_tablez00_4081)
	{
		{	/* Llib/weakhash.scm 280 */
			{	/* Llib/weakhash.scm 281 */
				obj_t BgL_auxz00_4957;

				if (STRUCTP(BgL_tablez00_4081))
					{	/* Llib/weakhash.scm 281 */
						BgL_auxz00_4957 = BgL_tablez00_4081;
					}
				else
					{
						obj_t BgL_auxz00_4960;

						BgL_auxz00_4960 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2161z00zz__weakhashz00,
							BINT(10745L), BGl_string2165z00zz__weakhashz00,
							BGl_string2163z00zz__weakhashz00, BgL_tablez00_4081);
						FAILURE(BgL_auxz00_4960, BFALSE, BFALSE);
					}
				return
					BGl_weakzd2hashtablezd2keyzd2listzd2zz__weakhashz00(BgL_auxz00_4957);
			}
		}

	}



/* &<@anonymous:1418> */
	obj_t BGl_z62zc3z04anonymousza31418ze3ze5zz__weakhashz00(obj_t
		BgL_envz00_4082, obj_t BgL_keyz00_4084, obj_t BgL_valz00_4085)
	{
		{	/* Llib/weakhash.scm 283 */
			{	/* Llib/weakhash.scm 284 */
				obj_t BgL_resz00_4083;

				BgL_resz00_4083 = PROCEDURE_REF(BgL_envz00_4082, (int) (0L));
				{	/* Llib/weakhash.scm 284 */
					obj_t BgL_auxz00_4263;

					BgL_auxz00_4263 =
						MAKE_YOUNG_PAIR(BgL_keyz00_4084, CELL_REF(BgL_resz00_4083));
					return CELL_SET(BgL_resz00_4083, BgL_auxz00_4263);
				}
			}
		}

	}



/* weak-hashtable-map */
	BGL_EXPORTED_DEF obj_t BGl_weakzd2hashtablezd2mapz00zz__weakhashz00(obj_t
		BgL_tablez00_60, obj_t BgL_funz00_61)
	{
		{	/* Llib/weakhash.scm 290 */
			{	/* Llib/weakhash.scm 291 */
				obj_t BgL_resz00_4099;

				BgL_resz00_4099 = MAKE_CELL(BNIL);
				{	/* Llib/weakhash.scm 294 */
					obj_t BgL_zc3z04anonymousza31420ze3z87_4089;

					BgL_zc3z04anonymousza31420ze3z87_4089 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza31420ze3ze5zz__weakhashz00, (int) (2L),
						(int) (2L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31420ze3z87_4089, (int) (0L),
						BgL_funz00_61);
					PROCEDURE_SET(BgL_zc3z04anonymousza31420ze3z87_4089, (int) (1L),
						((obj_t) BgL_resz00_4099));
					if (BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(BgL_tablez00_60))
						{	/* Llib/weakhash.scm 249 */
							BGl_keyszd2traversezd2hashz00zz__weakhashz00(BgL_tablez00_60,
								BgL_zc3z04anonymousza31420ze3z87_4089);
						}
					else
						{	/* Llib/weakhash.scm 249 */
							BGl_oldzd2traversezd2hashz00zz__weakhashz00(BgL_tablez00_60,
								BgL_zc3z04anonymousza31420ze3z87_4089);
						}
				}
				return CELL_REF(BgL_resz00_4099);
			}
		}

	}



/* &weak-hashtable-map */
	obj_t BGl_z62weakzd2hashtablezd2mapz62zz__weakhashz00(obj_t BgL_envz00_4090,
		obj_t BgL_tablez00_4091, obj_t BgL_funz00_4092)
	{
		{	/* Llib/weakhash.scm 290 */
			{	/* Llib/weakhash.scm 291 */
				obj_t BgL_auxz00_4987;
				obj_t BgL_auxz00_4980;

				if (PROCEDUREP(BgL_funz00_4092))
					{	/* Llib/weakhash.scm 291 */
						BgL_auxz00_4987 = BgL_funz00_4092;
					}
				else
					{
						obj_t BgL_auxz00_4990;

						BgL_auxz00_4990 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2161z00zz__weakhashz00,
							BINT(11138L), BGl_string2166z00zz__weakhashz00,
							BGl_string2167z00zz__weakhashz00, BgL_funz00_4092);
						FAILURE(BgL_auxz00_4990, BFALSE, BFALSE);
					}
				if (STRUCTP(BgL_tablez00_4091))
					{	/* Llib/weakhash.scm 291 */
						BgL_auxz00_4980 = BgL_tablez00_4091;
					}
				else
					{
						obj_t BgL_auxz00_4983;

						BgL_auxz00_4983 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2161z00zz__weakhashz00,
							BINT(11138L), BGl_string2166z00zz__weakhashz00,
							BGl_string2163z00zz__weakhashz00, BgL_tablez00_4091);
						FAILURE(BgL_auxz00_4983, BFALSE, BFALSE);
					}
				return
					BGl_weakzd2hashtablezd2mapz00zz__weakhashz00(BgL_auxz00_4980,
					BgL_auxz00_4987);
			}
		}

	}



/* &<@anonymous:1420> */
	obj_t BGl_z62zc3z04anonymousza31420ze3ze5zz__weakhashz00(obj_t
		BgL_envz00_4093, obj_t BgL_keyz00_4096, obj_t BgL_valz00_4097)
	{
		{	/* Llib/weakhash.scm 293 */
			{	/* Llib/weakhash.scm 294 */
				obj_t BgL_funz00_4094;
				obj_t BgL_resz00_4095;

				BgL_funz00_4094 = ((obj_t) PROCEDURE_REF(BgL_envz00_4093, (int) (0L)));
				BgL_resz00_4095 = PROCEDURE_REF(BgL_envz00_4093, (int) (1L));
				{	/* Llib/weakhash.scm 294 */
					obj_t BgL_auxz00_4264;

					{	/* Llib/weakhash.scm 294 */
						obj_t BgL_arg1421z00_4265;

						BgL_arg1421z00_4265 =
							BGL_PROCEDURE_CALL2(BgL_funz00_4094, BgL_keyz00_4096,
							BgL_valz00_4097);
						BgL_auxz00_4264 =
							MAKE_YOUNG_PAIR(BgL_arg1421z00_4265, CELL_REF(BgL_resz00_4095));
					}
					return CELL_SET(BgL_resz00_4095, BgL_auxz00_4264);
				}
			}
		}

	}



/* weak-hashtable-for-each */
	BGL_EXPORTED_DEF obj_t
		BGl_weakzd2hashtablezd2forzd2eachzd2zz__weakhashz00(obj_t BgL_tablez00_62,
		obj_t BgL_funz00_63)
	{
		{	/* Llib/weakhash.scm 300 */
			if (BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(BgL_tablez00_62))
				{	/* Llib/weakhash.scm 249 */
					return
						BBOOL(BGl_keyszd2traversezd2hashz00zz__weakhashz00(BgL_tablez00_62,
							BgL_funz00_63));
				}
			else
				{	/* Llib/weakhash.scm 249 */
					return
						BBOOL(BGl_oldzd2traversezd2hashz00zz__weakhashz00(BgL_tablez00_62,
							BgL_funz00_63));
				}
		}

	}



/* &weak-hashtable-for-each */
	obj_t BGl_z62weakzd2hashtablezd2forzd2eachzb0zz__weakhashz00(obj_t
		BgL_envz00_4101, obj_t BgL_tablez00_4102, obj_t BgL_funz00_4103)
	{
		{	/* Llib/weakhash.scm 300 */
			{	/* Llib/weakhash.scm 249 */
				obj_t BgL_auxz00_5019;
				obj_t BgL_auxz00_5012;

				if (PROCEDUREP(BgL_funz00_4103))
					{	/* Llib/weakhash.scm 249 */
						BgL_auxz00_5019 = BgL_funz00_4103;
					}
				else
					{
						obj_t BgL_auxz00_5022;

						BgL_auxz00_5022 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2161z00zz__weakhashz00,
							BINT(9474L), BGl_string2168z00zz__weakhashz00,
							BGl_string2167z00zz__weakhashz00, BgL_funz00_4103);
						FAILURE(BgL_auxz00_5022, BFALSE, BFALSE);
					}
				if (STRUCTP(BgL_tablez00_4102))
					{	/* Llib/weakhash.scm 249 */
						BgL_auxz00_5012 = BgL_tablez00_4102;
					}
				else
					{
						obj_t BgL_auxz00_5015;

						BgL_auxz00_5015 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2161z00zz__weakhashz00,
							BINT(9474L), BGl_string2168z00zz__weakhashz00,
							BGl_string2163z00zz__weakhashz00, BgL_tablez00_4102);
						FAILURE(BgL_auxz00_5015, BFALSE, BFALSE);
					}
				return
					BGl_weakzd2hashtablezd2forzd2eachzd2zz__weakhashz00(BgL_auxz00_5012,
					BgL_auxz00_5019);
			}
		}

	}



/* weak-keys-hashtable-filter! */
	bool_t BGl_weakzd2keyszd2hashtablezd2filterz12zc0zz__weakhashz00(obj_t
		BgL_tablez00_64, obj_t BgL_funz00_65)
	{
		{	/* Llib/weakhash.scm 306 */
			{	/* Llib/weakhash.scm 307 */
				obj_t BgL_bucketsz00_1503;

				BgL_bucketsz00_1503 = STRUCT_REF(BgL_tablez00_64, (int) (2L));
				{	/* Llib/weakhash.scm 308 */

					{
						long BgL_iz00_1506;

						BgL_iz00_1506 = 0L;
					BgL_zc3z04anonymousza31422ze3z87_1507:
						if ((BgL_iz00_1506 < VECTOR_LENGTH(((obj_t) BgL_bucketsz00_1503))))
							{	/* Llib/weakhash.scm 311 */
								obj_t BgL_bucketz00_1509;
								obj_t BgL_countz00_4110;

								BgL_bucketz00_1509 =
									VECTOR_REF(((obj_t) BgL_bucketsz00_1503), BgL_iz00_1506);
								BgL_countz00_4110 = MAKE_CELL(BINT(0L));
								{	/* Llib/weakhash.scm 315 */
									obj_t BgL_arg1424z00_1511;

									{	/* Llib/weakhash.scm 315 */
										obj_t BgL_zc3z04anonymousza31426ze3z87_4104;

										BgL_zc3z04anonymousza31426ze3z87_4104 =
											MAKE_FX_PROCEDURE
											(BGl_z62zc3z04anonymousza31426ze3ze5zz__weakhashz00,
											(int) (1L), (int) (2L));
										PROCEDURE_SET(BgL_zc3z04anonymousza31426ze3z87_4104,
											(int) (0L), ((obj_t) BgL_countz00_4110));
										PROCEDURE_SET(BgL_zc3z04anonymousza31426ze3z87_4104,
											(int) (1L), ((obj_t) BgL_funz00_65));
										BgL_arg1424z00_1511 =
											BGl_filterz12z12zz__r4_control_features_6_9z00
											(BgL_zc3z04anonymousza31426ze3z87_4104,
											BgL_bucketz00_1509);
									}
									VECTOR_SET(
										((obj_t) BgL_bucketsz00_1503), BgL_iz00_1506,
										BgL_arg1424z00_1511);
								}
								{	/* Llib/weakhash.scm 322 */
									long BgL_arg1441z00_1530;

									BgL_arg1441z00_1530 =
										(
										(long) CINT(STRUCT_REF(BgL_tablez00_64,
												(int) (0L))) -
										(long) CINT(CELL_REF(BgL_countz00_4110)));
									{	/* Llib/weakhash.scm 322 */
										obj_t BgL_auxz00_5055;
										int BgL_tmpz00_5053;

										BgL_auxz00_5055 = BINT(BgL_arg1441z00_1530);
										BgL_tmpz00_5053 = (int) (0L);
										STRUCT_SET(BgL_tablez00_64, BgL_tmpz00_5053,
											BgL_auxz00_5055);
								}}
								{
									long BgL_iz00_5058;

									BgL_iz00_5058 = (BgL_iz00_1506 + 1L);
									BgL_iz00_1506 = BgL_iz00_5058;
									goto BgL_zc3z04anonymousza31422ze3z87_1507;
								}
							}
						else
							{	/* Llib/weakhash.scm 310 */
								return ((bool_t) 0);
							}
					}
				}
			}
		}

	}



/* &<@anonymous:1426> */
	obj_t BGl_z62zc3z04anonymousza31426ze3ze5zz__weakhashz00(obj_t
		BgL_envz00_4105, obj_t BgL_vz00_4108)
	{
		{	/* Llib/weakhash.scm 314 */
			{	/* Llib/weakhash.scm 315 */
				obj_t BgL_countz00_4106;
				obj_t BgL_funz00_4107;

				BgL_countz00_4106 = PROCEDURE_REF(BgL_envz00_4105, (int) (0L));
				BgL_funz00_4107 = PROCEDURE_REF(BgL_envz00_4105, (int) (1L));
				{	/* Llib/weakhash.scm 315 */
					bool_t BgL_tmpz00_5064;

					{	/* Llib/weakhash.scm 315 */
						bool_t BgL_test2295z00_5065;

						{	/* Llib/weakhash.scm 315 */
							bool_t BgL_test2296z00_5066;

							{	/* Llib/weakhash.scm 315 */
								obj_t BgL_arg1440z00_4266;

								BgL_arg1440z00_4266 = bgl_weakptr_data(((obj_t) BgL_vz00_4108));
								BgL_test2296z00_5066 = (BgL_arg1440z00_4266 == BUNSPEC);
							}
							if (BgL_test2296z00_5066)
								{	/* Llib/weakhash.scm 315 */
									BgL_test2295z00_5065 = ((bool_t) 1);
								}
							else
								{	/* Llib/weakhash.scm 316 */
									bool_t BgL_test2297z00_5070;

									{	/* Llib/weakhash.scm 316 */
										obj_t BgL_arg1438z00_4267;
										obj_t BgL_arg1439z00_4268;

										BgL_arg1438z00_4267 =
											bgl_weakptr_data(((obj_t) BgL_vz00_4108));
										BgL_arg1439z00_4268 =
											bgl_weakptr_ref(((obj_t) BgL_vz00_4108));
										BgL_test2297z00_5070 =
											CBOOL(BGL_PROCEDURE_CALL2(BgL_funz00_4107,
												BgL_arg1438z00_4267, BgL_arg1439z00_4268));
									}
									if (BgL_test2297z00_5070)
										{	/* Llib/weakhash.scm 316 */
											BgL_test2295z00_5065 = ((bool_t) 0);
										}
									else
										{	/* Llib/weakhash.scm 316 */
											BgL_test2295z00_5065 = ((bool_t) 1);
										}
								}
						}
						if (BgL_test2295z00_5065)
							{	/* Llib/weakhash.scm 315 */
								{	/* Llib/weakhash.scm 318 */
									obj_t BgL_auxz00_4269;

									BgL_auxz00_4269 =
										ADDFX(CELL_REF(BgL_countz00_4106), BINT(1L));
									CELL_SET(BgL_countz00_4106, BgL_auxz00_4269);
								}
								BgL_tmpz00_5064 = ((bool_t) 0);
							}
						else
							{	/* Llib/weakhash.scm 315 */
								BgL_tmpz00_5064 = ((bool_t) 1);
							}
					}
					return BBOOL(BgL_tmpz00_5064);
				}
			}
		}

	}



/* weak-old-hashtable-filter! */
	bool_t BGl_weakzd2oldzd2hashtablezd2filterz12zc0zz__weakhashz00(obj_t
		BgL_tablez00_66, obj_t BgL_funz00_67)
	{
		{	/* Llib/weakhash.scm 328 */
			{	/* Llib/weakhash.scm 329 */
				obj_t BgL_bucketsz00_1534;

				BgL_bucketsz00_1534 = STRUCT_REF(BgL_tablez00_66, (int) (2L));
				{	/* Llib/weakhash.scm 330 */

					{
						long BgL_iz00_1537;

						BgL_iz00_1537 = 0L;
					BgL_zc3z04anonymousza31444ze3z87_1538:
						if ((BgL_iz00_1537 < VECTOR_LENGTH(((obj_t) BgL_bucketsz00_1534))))
							{	/* Llib/weakhash.scm 332 */
								{	/* Llib/weakhash.scm 336 */
									obj_t BgL_zc3z04anonymousza31447ze3z87_4112;

									{
										int BgL_tmpz00_5090;

										BgL_tmpz00_5090 = (int) (1L);
										BgL_zc3z04anonymousza31447ze3z87_4112 =
											MAKE_L_PROCEDURE
											(BGl_z62zc3z04anonymousza31447ze3ze5zz__weakhashz00,
											BgL_tmpz00_5090);
									}
									PROCEDURE_L_SET(BgL_zc3z04anonymousza31447ze3z87_4112,
										(int) (0L), ((obj_t) BgL_funz00_67));
									BGl_traversezd2bucketszd2zz__weakhashz00(BgL_tablez00_66,
										BgL_bucketsz00_1534, BgL_iz00_1537,
										BgL_zc3z04anonymousza31447ze3z87_4112);
								}
								{
									long BgL_iz00_5097;

									BgL_iz00_5097 = (BgL_iz00_1537 + 1L);
									BgL_iz00_1537 = BgL_iz00_5097;
									goto BgL_zc3z04anonymousza31444ze3z87_1538;
								}
							}
						else
							{	/* Llib/weakhash.scm 332 */
								return ((bool_t) 0);
							}
					}
				}
			}
		}

	}



/* &<@anonymous:1447> */
	obj_t BGl_z62zc3z04anonymousza31447ze3ze5zz__weakhashz00(obj_t
		BgL_envz00_4113, obj_t BgL_keyz00_4115, obj_t BgL_valz00_4116,
		obj_t BgL_bucketz00_4117)
	{
		{	/* Llib/weakhash.scm 335 */
			{	/* Llib/weakhash.scm 336 */
				obj_t BgL_funz00_4114;

				BgL_funz00_4114 = PROCEDURE_L_REF(BgL_envz00_4113, (int) (0L));
				if (CBOOL(BGL_PROCEDURE_CALL2(BgL_funz00_4114, BgL_keyz00_4115,
							BgL_valz00_4116)))
					{	/* Llib/weakhash.scm 336 */
						return BGl_keepgoingz00zz__weakhashz00;
					}
				else
					{	/* Llib/weakhash.scm 336 */
						return BGl_removez00zz__weakhashz00;
					}
			}
		}

	}



/* weak-hashtable-filter! */
	BGL_EXPORTED_DEF obj_t
		BGl_weakzd2hashtablezd2filterz12z12zz__weakhashz00(obj_t BgL_tablez00_68,
		obj_t BgL_funz00_69)
	{
		{	/* Llib/weakhash.scm 344 */
			if (BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(BgL_tablez00_68))
				{	/* Llib/weakhash.scm 345 */
					return
						BBOOL(BGl_weakzd2keyszd2hashtablezd2filterz12zc0zz__weakhashz00
						(BgL_tablez00_68, BgL_funz00_69));
				}
			else
				{	/* Llib/weakhash.scm 345 */
					return
						BBOOL(BGl_weakzd2oldzd2hashtablezd2filterz12zc0zz__weakhashz00
						(BgL_tablez00_68, BgL_funz00_69));
				}
		}

	}



/* &weak-hashtable-filter! */
	obj_t BGl_z62weakzd2hashtablezd2filterz12z70zz__weakhashz00(obj_t
		BgL_envz00_4118, obj_t BgL_tablez00_4119, obj_t BgL_funz00_4120)
	{
		{	/* Llib/weakhash.scm 344 */
			{	/* Llib/weakhash.scm 345 */
				obj_t BgL_auxz00_5121;
				obj_t BgL_auxz00_5114;

				if (PROCEDUREP(BgL_funz00_4120))
					{	/* Llib/weakhash.scm 345 */
						BgL_auxz00_5121 = BgL_funz00_4120;
					}
				else
					{
						obj_t BgL_auxz00_5124;

						BgL_auxz00_5124 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2161z00zz__weakhashz00,
							BINT(13294L), BGl_string2169z00zz__weakhashz00,
							BGl_string2167z00zz__weakhashz00, BgL_funz00_4120);
						FAILURE(BgL_auxz00_5124, BFALSE, BFALSE);
					}
				if (STRUCTP(BgL_tablez00_4119))
					{	/* Llib/weakhash.scm 345 */
						BgL_auxz00_5114 = BgL_tablez00_4119;
					}
				else
					{
						obj_t BgL_auxz00_5117;

						BgL_auxz00_5117 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2161z00zz__weakhashz00,
							BINT(13294L), BGl_string2169z00zz__weakhashz00,
							BGl_string2163z00zz__weakhashz00, BgL_tablez00_4119);
						FAILURE(BgL_auxz00_5117, BFALSE, BFALSE);
					}
				return
					BGl_weakzd2hashtablezd2filterz12z12zz__weakhashz00(BgL_auxz00_5114,
					BgL_auxz00_5121);
			}
		}

	}



/* weak-hashtable-clear! */
	BGL_EXPORTED_DEF obj_t BGl_weakzd2hashtablezd2clearz12z12zz__weakhashz00(obj_t
		BgL_tablez00_70)
	{
		{	/* Llib/weakhash.scm 352 */
			if (BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(BgL_tablez00_70))
				{	/* Llib/weakhash.scm 353 */
					return
						BBOOL(BGl_weakzd2keyszd2hashtablezd2filterz12zc0zz__weakhashz00
						(BgL_tablez00_70, BGl_proc2170z00zz__weakhashz00));
				}
			else
				{	/* Llib/weakhash.scm 353 */
					return
						BBOOL(BGl_weakzd2oldzd2hashtablezd2filterz12zc0zz__weakhashz00
						(BgL_tablez00_70, BGl_proc2171z00zz__weakhashz00));
				}
		}

	}



/* &weak-hashtable-clear! */
	obj_t BGl_z62weakzd2hashtablezd2clearz12z70zz__weakhashz00(obj_t
		BgL_envz00_4123, obj_t BgL_tablez00_4124)
	{
		{	/* Llib/weakhash.scm 352 */
			{	/* Llib/weakhash.scm 353 */
				obj_t BgL_auxz00_5135;

				if (STRUCTP(BgL_tablez00_4124))
					{	/* Llib/weakhash.scm 353 */
						BgL_auxz00_5135 = BgL_tablez00_4124;
					}
				else
					{
						obj_t BgL_auxz00_5138;

						BgL_auxz00_5138 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2161z00zz__weakhashz00,
							BINT(13701L), BGl_string2172z00zz__weakhashz00,
							BGl_string2163z00zz__weakhashz00, BgL_tablez00_4124);
						FAILURE(BgL_auxz00_5138, BFALSE, BFALSE);
					}
				return
					BGl_weakzd2hashtablezd2clearz12z12zz__weakhashz00(BgL_auxz00_5135);
			}
		}

	}



/* &<@anonymous:1453> */
	obj_t BGl_z62zc3z04anonymousza31453ze3ze5zz__weakhashz00(obj_t
		BgL_envz00_4125, obj_t BgL_kz00_4126, obj_t BgL_vz00_4127)
	{
		{	/* Llib/weakhash.scm 354 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &<@anonymous:1455> */
	obj_t BGl_z62zc3z04anonymousza31455ze3ze5zz__weakhashz00(obj_t
		BgL_envz00_4128, obj_t BgL_kz00_4129, obj_t BgL_vz00_4130)
	{
		{	/* Llib/weakhash.scm 355 */
			return BBOOL(((bool_t) 0));
		}

	}



/* weak-keys-hashtable-contains? */
	bool_t BGl_weakzd2keyszd2hashtablezd2containszf3z21zz__weakhashz00(obj_t
		BgL_tablez00_71, obj_t BgL_keyz00_72)
	{
		{	/* Llib/weakhash.scm 360 */
			{	/* Llib/weakhash.scm 361 */
				obj_t BgL_bucketsz00_1561;

				BgL_bucketsz00_1561 = STRUCT_REF(BgL_tablez00_71, (int) (2L));
				{	/* Llib/weakhash.scm 362 */
					long BgL_bucketzd2numzd2_1563;

					{	/* Llib/weakhash.scm 363 */
						long BgL_arg1464z00_1576;

						{	/* Llib/weakhash.scm 363 */
							obj_t BgL_hashnz00_2905;

							BgL_hashnz00_2905 = STRUCT_REF(BgL_tablez00_71, (int) (4L));
							if (PROCEDUREP(BgL_hashnz00_2905))
								{	/* Llib/weakhash.scm 363 */
									obj_t BgL_arg1268z00_2907;

									BgL_arg1268z00_2907 =
										BGL_PROCEDURE_CALL1(BgL_hashnz00_2905, BgL_keyz00_72);
									{	/* Llib/weakhash.scm 363 */
										long BgL_nz00_2909;

										BgL_nz00_2909 = (long) CINT(BgL_arg1268z00_2907);
										if ((BgL_nz00_2909 < 0L))
											{	/* Llib/weakhash.scm 363 */
												BgL_arg1464z00_1576 = NEG(BgL_nz00_2909);
											}
										else
											{	/* Llib/weakhash.scm 363 */
												BgL_arg1464z00_1576 = BgL_nz00_2909;
											}
									}
								}
							else
								{	/* Llib/weakhash.scm 363 */
									if ((BgL_hashnz00_2905 == BGl_symbol2173z00zz__weakhashz00))
										{	/* Llib/weakhash.scm 363 */
											BgL_arg1464z00_1576 =
												BGl_getzd2hashnumberzd2persistentz00zz__hashz00
												(BgL_keyz00_72);
										}
									else
										{	/* Llib/weakhash.scm 363 */
											BgL_arg1464z00_1576 =
												BGl_getzd2hashnumberzd2zz__hashz00(BgL_keyz00_72);
										}
								}
						}
						{	/* Llib/weakhash.scm 363 */
							long BgL_n1z00_2913;
							long BgL_n2z00_2914;

							BgL_n1z00_2913 = BgL_arg1464z00_1576;
							BgL_n2z00_2914 = VECTOR_LENGTH(((obj_t) BgL_bucketsz00_1561));
							{	/* Llib/weakhash.scm 363 */
								bool_t BgL_test2308z00_5165;

								{	/* Llib/weakhash.scm 363 */
									long BgL_arg1961z00_2916;

									BgL_arg1961z00_2916 =
										(((BgL_n1z00_2913) | (BgL_n2z00_2914)) & -2147483648);
									BgL_test2308z00_5165 = (BgL_arg1961z00_2916 == 0L);
								}
								if (BgL_test2308z00_5165)
									{	/* Llib/weakhash.scm 363 */
										int32_t BgL_arg1958z00_2917;

										{	/* Llib/weakhash.scm 363 */
											int32_t BgL_arg1959z00_2918;
											int32_t BgL_arg1960z00_2919;

											BgL_arg1959z00_2918 = (int32_t) (BgL_n1z00_2913);
											BgL_arg1960z00_2919 = (int32_t) (BgL_n2z00_2914);
											BgL_arg1958z00_2917 =
												(BgL_arg1959z00_2918 % BgL_arg1960z00_2919);
										}
										{	/* Llib/weakhash.scm 363 */
											long BgL_arg2069z00_2924;

											BgL_arg2069z00_2924 = (long) (BgL_arg1958z00_2917);
											BgL_bucketzd2numzd2_1563 = (long) (BgL_arg2069z00_2924);
									}}
								else
									{	/* Llib/weakhash.scm 363 */
										BgL_bucketzd2numzd2_1563 =
											(BgL_n1z00_2913 % BgL_n2z00_2914);
									}
							}
						}
					}
					{	/* Llib/weakhash.scm 363 */
						obj_t BgL_bucketz00_1564;

						BgL_bucketz00_1564 =
							VECTOR_REF(
							((obj_t) BgL_bucketsz00_1561), BgL_bucketzd2numzd2_1563);
						{	/* Llib/weakhash.scm 364 */

							{
								obj_t BgL_bucketz00_1566;

								BgL_bucketz00_1566 = BgL_bucketz00_1564;
							BgL_zc3z04anonymousza31456ze3z87_1567:
								if (NULLP(BgL_bucketz00_1566))
									{	/* Llib/weakhash.scm 367 */
										return ((bool_t) 0);
									}
								else
									{	/* Llib/weakhash.scm 369 */
										bool_t BgL_test2310z00_5178;

										{	/* Llib/weakhash.scm 369 */
											obj_t BgL_arg1462z00_1573;

											{	/* Llib/weakhash.scm 369 */
												obj_t BgL_arg1463z00_1574;

												BgL_arg1463z00_1574 = CAR(((obj_t) BgL_bucketz00_1566));
												BgL_arg1462z00_1573 =
													bgl_weakptr_data(((obj_t) BgL_arg1463z00_1574));
											}
											{	/* Llib/weakhash.scm 369 */
												obj_t BgL_eqtz00_2930;

												BgL_eqtz00_2930 =
													STRUCT_REF(BgL_tablez00_71, (int) (3L));
												if (PROCEDUREP(BgL_eqtz00_2930))
													{	/* Llib/weakhash.scm 369 */
														BgL_test2310z00_5178 =
															CBOOL(BGL_PROCEDURE_CALL2(BgL_eqtz00_2930,
																BgL_arg1462z00_1573, BgL_keyz00_72));
													}
												else
													{	/* Llib/weakhash.scm 369 */
														if ((BgL_arg1462z00_1573 == BgL_keyz00_72))
															{	/* Llib/weakhash.scm 369 */
																BgL_test2310z00_5178 = ((bool_t) 1);
															}
														else
															{	/* Llib/weakhash.scm 369 */
																if (STRINGP(BgL_arg1462z00_1573))
																	{	/* Llib/weakhash.scm 369 */
																		if (STRINGP(BgL_keyz00_72))
																			{	/* Llib/weakhash.scm 369 */
																				long BgL_l1z00_2937;

																				BgL_l1z00_2937 =
																					STRING_LENGTH(BgL_arg1462z00_1573);
																				if (
																					(BgL_l1z00_2937 ==
																						STRING_LENGTH(BgL_keyz00_72)))
																					{	/* Llib/weakhash.scm 369 */
																						int BgL_arg1844z00_2940;

																						{	/* Llib/weakhash.scm 369 */
																							char *BgL_auxz00_5205;
																							char *BgL_tmpz00_5203;

																							BgL_auxz00_5205 =
																								BSTRING_TO_STRING
																								(BgL_keyz00_72);
																							BgL_tmpz00_5203 =
																								BSTRING_TO_STRING
																								(BgL_arg1462z00_1573);
																							BgL_arg1844z00_2940 =
																								memcmp(BgL_tmpz00_5203,
																								BgL_auxz00_5205,
																								BgL_l1z00_2937);
																						}
																						BgL_test2310z00_5178 =
																							(
																							(long) (BgL_arg1844z00_2940) ==
																							0L);
																					}
																				else
																					{	/* Llib/weakhash.scm 369 */
																						BgL_test2310z00_5178 = ((bool_t) 0);
																					}
																			}
																		else
																			{	/* Llib/weakhash.scm 369 */
																				BgL_test2310z00_5178 = ((bool_t) 0);
																			}
																	}
																else
																	{	/* Llib/weakhash.scm 369 */
																		BgL_test2310z00_5178 = ((bool_t) 0);
																	}
															}
													}
											}
										}
										if (BgL_test2310z00_5178)
											{	/* Llib/weakhash.scm 369 */
												return ((bool_t) 1);
											}
										else
											{	/* Llib/weakhash.scm 372 */
												obj_t BgL_arg1461z00_1572;

												BgL_arg1461z00_1572 = CDR(((obj_t) BgL_bucketz00_1566));
												{
													obj_t BgL_bucketz00_5212;

													BgL_bucketz00_5212 = BgL_arg1461z00_1572;
													BgL_bucketz00_1566 = BgL_bucketz00_5212;
													goto BgL_zc3z04anonymousza31456ze3z87_1567;
												}
											}
									}
							}
						}
					}
				}
			}
		}

	}



/* weak-old-hashtable-contains? */
	bool_t BGl_weakzd2oldzd2hashtablezd2containszf3z21zz__weakhashz00(obj_t
		BgL_tablez00_73, obj_t BgL_keyz00_74)
	{
		{	/* Llib/weakhash.scm 377 */
			{	/* Llib/weakhash.scm 378 */
				obj_t BgL_bucketsz00_1577;

				BgL_bucketsz00_1577 = STRUCT_REF(BgL_tablez00_73, (int) (2L));
				{	/* Llib/weakhash.scm 379 */
					long BgL_bucketzd2numzd2_1579;

					{	/* Llib/weakhash.scm 380 */
						long BgL_arg1468z00_1588;

						{	/* Llib/weakhash.scm 380 */
							obj_t BgL_hashnz00_2949;

							BgL_hashnz00_2949 = STRUCT_REF(BgL_tablez00_73, (int) (4L));
							if (PROCEDUREP(BgL_hashnz00_2949))
								{	/* Llib/weakhash.scm 380 */
									obj_t BgL_arg1268z00_2951;

									BgL_arg1268z00_2951 =
										BGL_PROCEDURE_CALL1(BgL_hashnz00_2949, BgL_keyz00_74);
									{	/* Llib/weakhash.scm 380 */
										long BgL_nz00_2953;

										BgL_nz00_2953 = (long) CINT(BgL_arg1268z00_2951);
										if ((BgL_nz00_2953 < 0L))
											{	/* Llib/weakhash.scm 380 */
												BgL_arg1468z00_1588 = NEG(BgL_nz00_2953);
											}
										else
											{	/* Llib/weakhash.scm 380 */
												BgL_arg1468z00_1588 = BgL_nz00_2953;
											}
									}
								}
							else
								{	/* Llib/weakhash.scm 380 */
									if ((BgL_hashnz00_2949 == BGl_symbol2173z00zz__weakhashz00))
										{	/* Llib/weakhash.scm 380 */
											BgL_arg1468z00_1588 =
												BGl_getzd2hashnumberzd2persistentz00zz__hashz00
												(BgL_keyz00_74);
										}
									else
										{	/* Llib/weakhash.scm 380 */
											BgL_arg1468z00_1588 =
												BGl_getzd2hashnumberzd2zz__hashz00(BgL_keyz00_74);
										}
								}
						}
						{	/* Llib/weakhash.scm 380 */
							long BgL_n1z00_2957;
							long BgL_n2z00_2958;

							BgL_n1z00_2957 = BgL_arg1468z00_1588;
							BgL_n2z00_2958 = VECTOR_LENGTH(((obj_t) BgL_bucketsz00_1577));
							{	/* Llib/weakhash.scm 380 */
								bool_t BgL_test2319z00_5233;

								{	/* Llib/weakhash.scm 380 */
									long BgL_arg1961z00_2960;

									BgL_arg1961z00_2960 =
										(((BgL_n1z00_2957) | (BgL_n2z00_2958)) & -2147483648);
									BgL_test2319z00_5233 = (BgL_arg1961z00_2960 == 0L);
								}
								if (BgL_test2319z00_5233)
									{	/* Llib/weakhash.scm 380 */
										int32_t BgL_arg1958z00_2961;

										{	/* Llib/weakhash.scm 380 */
											int32_t BgL_arg1959z00_2962;
											int32_t BgL_arg1960z00_2963;

											BgL_arg1959z00_2962 = (int32_t) (BgL_n1z00_2957);
											BgL_arg1960z00_2963 = (int32_t) (BgL_n2z00_2958);
											BgL_arg1958z00_2961 =
												(BgL_arg1959z00_2962 % BgL_arg1960z00_2963);
										}
										{	/* Llib/weakhash.scm 380 */
											long BgL_arg2069z00_2968;

											BgL_arg2069z00_2968 = (long) (BgL_arg1958z00_2961);
											BgL_bucketzd2numzd2_1579 = (long) (BgL_arg2069z00_2968);
									}}
								else
									{	/* Llib/weakhash.scm 380 */
										BgL_bucketzd2numzd2_1579 =
											(BgL_n1z00_2957 % BgL_n2z00_2958);
									}
							}
						}
					}
					{	/* Llib/weakhash.scm 380 */
						obj_t BgL_retz00_1580;

						{	/* Llib/weakhash.scm 383 */
							obj_t BgL_zc3z04anonymousza31466ze3z87_4131;

							{
								int BgL_tmpz00_5242;

								BgL_tmpz00_5242 = (int) (2L);
								BgL_zc3z04anonymousza31466ze3z87_4131 =
									MAKE_L_PROCEDURE
									(BGl_z62zc3z04anonymousza31466ze3ze5zz__weakhashz00,
									BgL_tmpz00_5242);
							}
							PROCEDURE_L_SET(BgL_zc3z04anonymousza31466ze3z87_4131,
								(int) (0L), BgL_tablez00_73);
							PROCEDURE_L_SET(BgL_zc3z04anonymousza31466ze3z87_4131,
								(int) (1L), BgL_keyz00_74);
							BgL_retz00_1580 =
								BGl_traversezd2bucketszd2zz__weakhashz00(BgL_tablez00_73,
								BgL_bucketsz00_1577, BgL_bucketzd2numzd2_1579,
								BgL_zc3z04anonymousza31466ze3z87_4131);
						}
						{	/* Llib/weakhash.scm 381 */

							if ((BgL_retz00_1580 == BGl_keepgoingz00zz__weakhashz00))
								{	/* Llib/weakhash.scm 386 */
									return ((bool_t) 0);
								}
							else
								{	/* Llib/weakhash.scm 386 */
									return ((bool_t) 1);
								}
						}
					}
				}
			}
		}

	}



/* &<@anonymous:1466> */
	obj_t BGl_z62zc3z04anonymousza31466ze3ze5zz__weakhashz00(obj_t
		BgL_envz00_4132, obj_t BgL_bkeyz00_4135, obj_t BgL_valz00_4136,
		obj_t BgL_bucketz00_4137)
	{
		{	/* Llib/weakhash.scm 382 */
			{	/* Llib/weakhash.scm 383 */
				obj_t BgL_tablez00_4133;
				obj_t BgL_keyz00_4134;

				BgL_tablez00_4133 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_4132, (int) (0L)));
				BgL_keyz00_4134 = PROCEDURE_L_REF(BgL_envz00_4132, (int) (1L));
				{	/* Llib/weakhash.scm 383 */
					bool_t BgL_test2321z00_5257;

					{	/* Llib/weakhash.scm 383 */
						obj_t BgL_eqtz00_4270;

						BgL_eqtz00_4270 = STRUCT_REF(BgL_tablez00_4133, (int) (3L));
						if (PROCEDUREP(BgL_eqtz00_4270))
							{	/* Llib/weakhash.scm 383 */
								BgL_test2321z00_5257 =
									CBOOL(BGL_PROCEDURE_CALL2(BgL_eqtz00_4270, BgL_keyz00_4134,
										BgL_bkeyz00_4135));
							}
						else
							{	/* Llib/weakhash.scm 383 */
								if ((BgL_keyz00_4134 == BgL_bkeyz00_4135))
									{	/* Llib/weakhash.scm 383 */
										BgL_test2321z00_5257 = ((bool_t) 1);
									}
								else
									{	/* Llib/weakhash.scm 383 */
										if (STRINGP(BgL_keyz00_4134))
											{	/* Llib/weakhash.scm 383 */
												if (STRINGP(BgL_bkeyz00_4135))
													{	/* Llib/weakhash.scm 383 */
														long BgL_l1z00_4271;

														BgL_l1z00_4271 = STRING_LENGTH(BgL_keyz00_4134);
														if (
															(BgL_l1z00_4271 ==
																STRING_LENGTH(BgL_bkeyz00_4135)))
															{	/* Llib/weakhash.scm 383 */
																int BgL_arg1844z00_4272;

																{	/* Llib/weakhash.scm 383 */
																	char *BgL_auxz00_5280;
																	char *BgL_tmpz00_5278;

																	BgL_auxz00_5280 =
																		BSTRING_TO_STRING(BgL_bkeyz00_4135);
																	BgL_tmpz00_5278 =
																		BSTRING_TO_STRING(BgL_keyz00_4134);
																	BgL_arg1844z00_4272 =
																		memcmp(BgL_tmpz00_5278, BgL_auxz00_5280,
																		BgL_l1z00_4271);
																}
																BgL_test2321z00_5257 =
																	((long) (BgL_arg1844z00_4272) == 0L);
															}
														else
															{	/* Llib/weakhash.scm 383 */
																BgL_test2321z00_5257 = ((bool_t) 0);
															}
													}
												else
													{	/* Llib/weakhash.scm 383 */
														BgL_test2321z00_5257 = ((bool_t) 0);
													}
											}
										else
											{	/* Llib/weakhash.scm 383 */
												BgL_test2321z00_5257 = ((bool_t) 0);
											}
									}
							}
					}
					if (BgL_test2321z00_5257)
						{	/* Llib/weakhash.scm 383 */
							return BTRUE;
						}
					else
						{	/* Llib/weakhash.scm 383 */
							return BGl_keepgoingz00zz__weakhashz00;
						}
				}
			}
		}

	}



/* weak-hashtable-contains? */
	BGL_EXPORTED_DEF bool_t
		BGl_weakzd2hashtablezd2containszf3zf3zz__weakhashz00(obj_t BgL_tablez00_75,
		obj_t BgL_keyz00_76)
	{
		{	/* Llib/weakhash.scm 391 */
			if (BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(BgL_tablez00_75))
				{	/* Llib/weakhash.scm 392 */
					BGL_TAIL return
						BGl_weakzd2keyszd2hashtablezd2containszf3z21zz__weakhashz00
						(BgL_tablez00_75, BgL_keyz00_76);
				}
			else
				{	/* Llib/weakhash.scm 392 */
					BGL_TAIL return
						BGl_weakzd2oldzd2hashtablezd2containszf3z21zz__weakhashz00
						(BgL_tablez00_75, BgL_keyz00_76);
				}
		}

	}



/* &weak-hashtable-contains? */
	obj_t BGl_z62weakzd2hashtablezd2containszf3z91zz__weakhashz00(obj_t
		BgL_envz00_4138, obj_t BgL_tablez00_4139, obj_t BgL_keyz00_4140)
	{
		{	/* Llib/weakhash.scm 391 */
			{	/* Llib/weakhash.scm 392 */
				bool_t BgL_tmpz00_5289;

				{	/* Llib/weakhash.scm 392 */
					obj_t BgL_auxz00_5290;

					if (STRUCTP(BgL_tablez00_4139))
						{	/* Llib/weakhash.scm 392 */
							BgL_auxz00_5290 = BgL_tablez00_4139;
						}
					else
						{
							obj_t BgL_auxz00_5293;

							BgL_auxz00_5293 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2161z00zz__weakhashz00, BINT(15453L),
								BGl_string2175z00zz__weakhashz00,
								BGl_string2163z00zz__weakhashz00, BgL_tablez00_4139);
							FAILURE(BgL_auxz00_5293, BFALSE, BFALSE);
						}
					BgL_tmpz00_5289 =
						BGl_weakzd2hashtablezd2containszf3zf3zz__weakhashz00
						(BgL_auxz00_5290, BgL_keyz00_4140);
				}
				return BBOOL(BgL_tmpz00_5289);
			}
		}

	}



/* weak-keys-hashtable-get */
	obj_t BGl_weakzd2keyszd2hashtablezd2getzd2zz__weakhashz00(obj_t
		BgL_tablez00_77, obj_t BgL_keyz00_78)
	{
		{	/* Llib/weakhash.scm 399 */
			{	/* Llib/weakhash.scm 400 */
				obj_t BgL_bucketsz00_1590;

				BgL_bucketsz00_1590 = STRUCT_REF(BgL_tablez00_77, (int) (2L));
				{	/* Llib/weakhash.scm 401 */
					long BgL_bucketzd2numzd2_1592;

					{	/* Llib/weakhash.scm 402 */
						long BgL_arg1480z00_1606;

						{	/* Llib/weakhash.scm 402 */
							obj_t BgL_hashnz00_2990;

							BgL_hashnz00_2990 = STRUCT_REF(BgL_tablez00_77, (int) (4L));
							if (PROCEDUREP(BgL_hashnz00_2990))
								{	/* Llib/weakhash.scm 402 */
									obj_t BgL_arg1268z00_2992;

									BgL_arg1268z00_2992 =
										BGL_PROCEDURE_CALL1(BgL_hashnz00_2990, BgL_keyz00_78);
									{	/* Llib/weakhash.scm 402 */
										long BgL_nz00_2994;

										BgL_nz00_2994 = (long) CINT(BgL_arg1268z00_2992);
										if ((BgL_nz00_2994 < 0L))
											{	/* Llib/weakhash.scm 402 */
												BgL_arg1480z00_1606 = NEG(BgL_nz00_2994);
											}
										else
											{	/* Llib/weakhash.scm 402 */
												BgL_arg1480z00_1606 = BgL_nz00_2994;
											}
									}
								}
							else
								{	/* Llib/weakhash.scm 402 */
									if ((BgL_hashnz00_2990 == BGl_symbol2173z00zz__weakhashz00))
										{	/* Llib/weakhash.scm 402 */
											BgL_arg1480z00_1606 =
												BGl_getzd2hashnumberzd2persistentz00zz__hashz00
												(BgL_keyz00_78);
										}
									else
										{	/* Llib/weakhash.scm 402 */
											BgL_arg1480z00_1606 =
												BGl_getzd2hashnumberzd2zz__hashz00(BgL_keyz00_78);
										}
								}
						}
						{	/* Llib/weakhash.scm 402 */
							long BgL_n1z00_2998;
							long BgL_n2z00_2999;

							BgL_n1z00_2998 = BgL_arg1480z00_1606;
							BgL_n2z00_2999 = VECTOR_LENGTH(((obj_t) BgL_bucketsz00_1590));
							{	/* Llib/weakhash.scm 402 */
								bool_t BgL_test2332z00_5319;

								{	/* Llib/weakhash.scm 402 */
									long BgL_arg1961z00_3001;

									BgL_arg1961z00_3001 =
										(((BgL_n1z00_2998) | (BgL_n2z00_2999)) & -2147483648);
									BgL_test2332z00_5319 = (BgL_arg1961z00_3001 == 0L);
								}
								if (BgL_test2332z00_5319)
									{	/* Llib/weakhash.scm 402 */
										int32_t BgL_arg1958z00_3002;

										{	/* Llib/weakhash.scm 402 */
											int32_t BgL_arg1959z00_3003;
											int32_t BgL_arg1960z00_3004;

											BgL_arg1959z00_3003 = (int32_t) (BgL_n1z00_2998);
											BgL_arg1960z00_3004 = (int32_t) (BgL_n2z00_2999);
											BgL_arg1958z00_3002 =
												(BgL_arg1959z00_3003 % BgL_arg1960z00_3004);
										}
										{	/* Llib/weakhash.scm 402 */
											long BgL_arg2069z00_3009;

											BgL_arg2069z00_3009 = (long) (BgL_arg1958z00_3002);
											BgL_bucketzd2numzd2_1592 = (long) (BgL_arg2069z00_3009);
									}}
								else
									{	/* Llib/weakhash.scm 402 */
										BgL_bucketzd2numzd2_1592 =
											(BgL_n1z00_2998 % BgL_n2z00_2999);
									}
							}
						}
					}
					{	/* Llib/weakhash.scm 402 */
						obj_t BgL_bucketz00_1593;

						BgL_bucketz00_1593 =
							VECTOR_REF(
							((obj_t) BgL_bucketsz00_1590), BgL_bucketzd2numzd2_1592);
						{	/* Llib/weakhash.scm 403 */

							{
								obj_t BgL_bucketz00_1595;

								BgL_bucketz00_1595 = BgL_bucketz00_1593;
							BgL_zc3z04anonymousza31470ze3z87_1596:
								if (NULLP(BgL_bucketz00_1595))
									{	/* Llib/weakhash.scm 406 */
										return BFALSE;
									}
								else
									{	/* Llib/weakhash.scm 408 */
										bool_t BgL_test2334z00_5332;

										{	/* Llib/weakhash.scm 408 */
											obj_t BgL_arg1478z00_1603;

											{	/* Llib/weakhash.scm 408 */
												obj_t BgL_arg1479z00_1604;

												BgL_arg1479z00_1604 = CAR(((obj_t) BgL_bucketz00_1595));
												BgL_arg1478z00_1603 =
													bgl_weakptr_data(((obj_t) BgL_arg1479z00_1604));
											}
											{	/* Llib/weakhash.scm 408 */
												obj_t BgL_eqtz00_3015;

												BgL_eqtz00_3015 =
													STRUCT_REF(BgL_tablez00_77, (int) (3L));
												if (PROCEDUREP(BgL_eqtz00_3015))
													{	/* Llib/weakhash.scm 408 */
														BgL_test2334z00_5332 =
															CBOOL(BGL_PROCEDURE_CALL2(BgL_eqtz00_3015,
																BgL_arg1478z00_1603, BgL_keyz00_78));
													}
												else
													{	/* Llib/weakhash.scm 408 */
														if ((BgL_arg1478z00_1603 == BgL_keyz00_78))
															{	/* Llib/weakhash.scm 408 */
																BgL_test2334z00_5332 = ((bool_t) 1);
															}
														else
															{	/* Llib/weakhash.scm 408 */
																if (STRINGP(BgL_arg1478z00_1603))
																	{	/* Llib/weakhash.scm 408 */
																		if (STRINGP(BgL_keyz00_78))
																			{	/* Llib/weakhash.scm 408 */
																				long BgL_l1z00_3022;

																				BgL_l1z00_3022 =
																					STRING_LENGTH(BgL_arg1478z00_1603);
																				if (
																					(BgL_l1z00_3022 ==
																						STRING_LENGTH(BgL_keyz00_78)))
																					{	/* Llib/weakhash.scm 408 */
																						int BgL_arg1844z00_3025;

																						{	/* Llib/weakhash.scm 408 */
																							char *BgL_auxz00_5359;
																							char *BgL_tmpz00_5357;

																							BgL_auxz00_5359 =
																								BSTRING_TO_STRING
																								(BgL_keyz00_78);
																							BgL_tmpz00_5357 =
																								BSTRING_TO_STRING
																								(BgL_arg1478z00_1603);
																							BgL_arg1844z00_3025 =
																								memcmp(BgL_tmpz00_5357,
																								BgL_auxz00_5359,
																								BgL_l1z00_3022);
																						}
																						BgL_test2334z00_5332 =
																							(
																							(long) (BgL_arg1844z00_3025) ==
																							0L);
																					}
																				else
																					{	/* Llib/weakhash.scm 408 */
																						BgL_test2334z00_5332 = ((bool_t) 0);
																					}
																			}
																		else
																			{	/* Llib/weakhash.scm 408 */
																				BgL_test2334z00_5332 = ((bool_t) 0);
																			}
																	}
																else
																	{	/* Llib/weakhash.scm 408 */
																		BgL_test2334z00_5332 = ((bool_t) 0);
																	}
															}
													}
											}
										}
										if (BgL_test2334z00_5332)
											{	/* Llib/weakhash.scm 409 */
												obj_t BgL_arg1476z00_1601;

												BgL_arg1476z00_1601 = CAR(((obj_t) BgL_bucketz00_1595));
												return bgl_weakptr_ref(((obj_t) BgL_arg1476z00_1601));
											}
										else
											{	/* Llib/weakhash.scm 411 */
												obj_t BgL_arg1477z00_1602;

												BgL_arg1477z00_1602 = CDR(((obj_t) BgL_bucketz00_1595));
												{
													obj_t BgL_bucketz00_5370;

													BgL_bucketz00_5370 = BgL_arg1477z00_1602;
													BgL_bucketz00_1595 = BgL_bucketz00_5370;
													goto BgL_zc3z04anonymousza31470ze3z87_1596;
												}
											}
									}
							}
						}
					}
				}
			}
		}

	}



/* weak-old-hashtable-get */
	obj_t BGl_weakzd2oldzd2hashtablezd2getzd2zz__weakhashz00(obj_t
		BgL_tablez00_79, obj_t BgL_keyz00_80)
	{
		{	/* Llib/weakhash.scm 416 */
			{	/* Llib/weakhash.scm 417 */
				obj_t BgL_bucketsz00_1607;

				BgL_bucketsz00_1607 = STRUCT_REF(BgL_tablez00_79, (int) (2L));
				{	/* Llib/weakhash.scm 418 */
					long BgL_bucketzd2numzd2_1609;

					{	/* Llib/weakhash.scm 419 */
						long BgL_arg1484z00_1618;

						{	/* Llib/weakhash.scm 419 */
							obj_t BgL_hashnz00_3036;

							BgL_hashnz00_3036 = STRUCT_REF(BgL_tablez00_79, (int) (4L));
							if (PROCEDUREP(BgL_hashnz00_3036))
								{	/* Llib/weakhash.scm 419 */
									obj_t BgL_arg1268z00_3038;

									BgL_arg1268z00_3038 =
										BGL_PROCEDURE_CALL1(BgL_hashnz00_3036, BgL_keyz00_80);
									{	/* Llib/weakhash.scm 419 */
										long BgL_nz00_3040;

										BgL_nz00_3040 = (long) CINT(BgL_arg1268z00_3038);
										if ((BgL_nz00_3040 < 0L))
											{	/* Llib/weakhash.scm 419 */
												BgL_arg1484z00_1618 = NEG(BgL_nz00_3040);
											}
										else
											{	/* Llib/weakhash.scm 419 */
												BgL_arg1484z00_1618 = BgL_nz00_3040;
											}
									}
								}
							else
								{	/* Llib/weakhash.scm 419 */
									if ((BgL_hashnz00_3036 == BGl_symbol2173z00zz__weakhashz00))
										{	/* Llib/weakhash.scm 419 */
											BgL_arg1484z00_1618 =
												BGl_getzd2hashnumberzd2persistentz00zz__hashz00
												(BgL_keyz00_80);
										}
									else
										{	/* Llib/weakhash.scm 419 */
											BgL_arg1484z00_1618 =
												BGl_getzd2hashnumberzd2zz__hashz00(BgL_keyz00_80);
										}
								}
						}
						{	/* Llib/weakhash.scm 419 */
							long BgL_n1z00_3044;
							long BgL_n2z00_3045;

							BgL_n1z00_3044 = BgL_arg1484z00_1618;
							BgL_n2z00_3045 = VECTOR_LENGTH(((obj_t) BgL_bucketsz00_1607));
							{	/* Llib/weakhash.scm 419 */
								bool_t BgL_test2343z00_5391;

								{	/* Llib/weakhash.scm 419 */
									long BgL_arg1961z00_3047;

									BgL_arg1961z00_3047 =
										(((BgL_n1z00_3044) | (BgL_n2z00_3045)) & -2147483648);
									BgL_test2343z00_5391 = (BgL_arg1961z00_3047 == 0L);
								}
								if (BgL_test2343z00_5391)
									{	/* Llib/weakhash.scm 419 */
										int32_t BgL_arg1958z00_3048;

										{	/* Llib/weakhash.scm 419 */
											int32_t BgL_arg1959z00_3049;
											int32_t BgL_arg1960z00_3050;

											BgL_arg1959z00_3049 = (int32_t) (BgL_n1z00_3044);
											BgL_arg1960z00_3050 = (int32_t) (BgL_n2z00_3045);
											BgL_arg1958z00_3048 =
												(BgL_arg1959z00_3049 % BgL_arg1960z00_3050);
										}
										{	/* Llib/weakhash.scm 419 */
											long BgL_arg2069z00_3055;

											BgL_arg2069z00_3055 = (long) (BgL_arg1958z00_3048);
											BgL_bucketzd2numzd2_1609 = (long) (BgL_arg2069z00_3055);
									}}
								else
									{	/* Llib/weakhash.scm 419 */
										BgL_bucketzd2numzd2_1609 =
											(BgL_n1z00_3044 % BgL_n2z00_3045);
									}
							}
						}
					}
					{	/* Llib/weakhash.scm 419 */
						obj_t BgL_retz00_1610;

						{	/* Llib/weakhash.scm 422 */
							obj_t BgL_zc3z04anonymousza31482ze3z87_4141;

							{
								int BgL_tmpz00_5400;

								BgL_tmpz00_5400 = (int) (2L);
								BgL_zc3z04anonymousza31482ze3z87_4141 =
									MAKE_L_PROCEDURE
									(BGl_z62zc3z04anonymousza31482ze3ze5zz__weakhashz00,
									BgL_tmpz00_5400);
							}
							PROCEDURE_L_SET(BgL_zc3z04anonymousza31482ze3z87_4141,
								(int) (0L), BgL_tablez00_79);
							PROCEDURE_L_SET(BgL_zc3z04anonymousza31482ze3z87_4141,
								(int) (1L), BgL_keyz00_80);
							BgL_retz00_1610 =
								BGl_traversezd2bucketszd2zz__weakhashz00(BgL_tablez00_79,
								BgL_bucketsz00_1607, BgL_bucketzd2numzd2_1609,
								BgL_zc3z04anonymousza31482ze3z87_4141);
						}
						{	/* Llib/weakhash.scm 420 */

							if ((BgL_retz00_1610 == BGl_keepgoingz00zz__weakhashz00))
								{	/* Llib/weakhash.scm 425 */
									return BFALSE;
								}
							else
								{	/* Llib/weakhash.scm 425 */
									return BgL_retz00_1610;
								}
						}
					}
				}
			}
		}

	}



/* &<@anonymous:1482> */
	obj_t BGl_z62zc3z04anonymousza31482ze3ze5zz__weakhashz00(obj_t
		BgL_envz00_4142, obj_t BgL_bkeyz00_4145, obj_t BgL_valz00_4146,
		obj_t BgL_bucketz00_4147)
	{
		{	/* Llib/weakhash.scm 421 */
			{	/* Llib/weakhash.scm 422 */
				obj_t BgL_tablez00_4143;
				obj_t BgL_keyz00_4144;

				BgL_tablez00_4143 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_4142, (int) (0L)));
				BgL_keyz00_4144 = PROCEDURE_L_REF(BgL_envz00_4142, (int) (1L));
				{	/* Llib/weakhash.scm 422 */
					bool_t BgL_test2345z00_5415;

					{	/* Llib/weakhash.scm 422 */
						obj_t BgL_eqtz00_4273;

						BgL_eqtz00_4273 = STRUCT_REF(BgL_tablez00_4143, (int) (3L));
						if (PROCEDUREP(BgL_eqtz00_4273))
							{	/* Llib/weakhash.scm 422 */
								BgL_test2345z00_5415 =
									CBOOL(BGL_PROCEDURE_CALL2(BgL_eqtz00_4273, BgL_keyz00_4144,
										BgL_bkeyz00_4145));
							}
						else
							{	/* Llib/weakhash.scm 422 */
								if ((BgL_keyz00_4144 == BgL_bkeyz00_4145))
									{	/* Llib/weakhash.scm 422 */
										BgL_test2345z00_5415 = ((bool_t) 1);
									}
								else
									{	/* Llib/weakhash.scm 422 */
										if (STRINGP(BgL_keyz00_4144))
											{	/* Llib/weakhash.scm 422 */
												if (STRINGP(BgL_bkeyz00_4145))
													{	/* Llib/weakhash.scm 422 */
														long BgL_l1z00_4274;

														BgL_l1z00_4274 = STRING_LENGTH(BgL_keyz00_4144);
														if (
															(BgL_l1z00_4274 ==
																STRING_LENGTH(BgL_bkeyz00_4145)))
															{	/* Llib/weakhash.scm 422 */
																int BgL_arg1844z00_4275;

																{	/* Llib/weakhash.scm 422 */
																	char *BgL_auxz00_5438;
																	char *BgL_tmpz00_5436;

																	BgL_auxz00_5438 =
																		BSTRING_TO_STRING(BgL_bkeyz00_4145);
																	BgL_tmpz00_5436 =
																		BSTRING_TO_STRING(BgL_keyz00_4144);
																	BgL_arg1844z00_4275 =
																		memcmp(BgL_tmpz00_5436, BgL_auxz00_5438,
																		BgL_l1z00_4274);
																}
																BgL_test2345z00_5415 =
																	((long) (BgL_arg1844z00_4275) == 0L);
															}
														else
															{	/* Llib/weakhash.scm 422 */
																BgL_test2345z00_5415 = ((bool_t) 0);
															}
													}
												else
													{	/* Llib/weakhash.scm 422 */
														BgL_test2345z00_5415 = ((bool_t) 0);
													}
											}
										else
											{	/* Llib/weakhash.scm 422 */
												BgL_test2345z00_5415 = ((bool_t) 0);
											}
									}
							}
					}
					if (BgL_test2345z00_5415)
						{	/* Llib/weakhash.scm 422 */
							return BgL_valz00_4146;
						}
					else
						{	/* Llib/weakhash.scm 422 */
							return BGl_keepgoingz00zz__weakhashz00;
						}
				}
			}
		}

	}



/* weak-hashtable-get */
	BGL_EXPORTED_DEF obj_t BGl_weakzd2hashtablezd2getz00zz__weakhashz00(obj_t
		BgL_tablez00_81, obj_t BgL_keyz00_82)
	{
		{	/* Llib/weakhash.scm 432 */
			if (BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(BgL_tablez00_81))
				{	/* Llib/weakhash.scm 433 */
					BGL_TAIL return
						BGl_weakzd2keyszd2hashtablezd2getzd2zz__weakhashz00(BgL_tablez00_81,
						BgL_keyz00_82);
				}
			else
				{	/* Llib/weakhash.scm 433 */
					BGL_TAIL return
						BGl_weakzd2oldzd2hashtablezd2getzd2zz__weakhashz00(BgL_tablez00_81,
						BgL_keyz00_82);
				}
		}

	}



/* &weak-hashtable-get */
	obj_t BGl_z62weakzd2hashtablezd2getz62zz__weakhashz00(obj_t BgL_envz00_4148,
		obj_t BgL_tablez00_4149, obj_t BgL_keyz00_4150)
	{
		{	/* Llib/weakhash.scm 432 */
			{	/* Llib/weakhash.scm 433 */
				obj_t BgL_auxz00_5447;

				if (STRUCTP(BgL_tablez00_4149))
					{	/* Llib/weakhash.scm 433 */
						BgL_auxz00_5447 = BgL_tablez00_4149;
					}
				else
					{
						obj_t BgL_auxz00_5450;

						BgL_auxz00_5450 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2161z00zz__weakhashz00,
							BINT(17200L), BGl_string2176z00zz__weakhashz00,
							BGl_string2163z00zz__weakhashz00, BgL_tablez00_4149);
						FAILURE(BgL_auxz00_5450, BFALSE, BFALSE);
					}
				return
					BGl_weakzd2hashtablezd2getz00zz__weakhashz00(BgL_auxz00_5447,
					BgL_keyz00_4150);
			}
		}

	}



/* weak-keys-hashtable-put! */
	obj_t BGl_weakzd2keyszd2hashtablezd2putz12zc0zz__weakhashz00(obj_t
		BgL_tablez00_83, obj_t BgL_keyz00_84, obj_t BgL_objz00_85)
	{
		{	/* Llib/weakhash.scm 440 */
			{	/* Llib/weakhash.scm 441 */
				obj_t BgL_bucketsz00_1620;

				BgL_bucketsz00_1620 = STRUCT_REF(BgL_tablez00_83, (int) (2L));
				{	/* Llib/weakhash.scm 442 */
					long BgL_bucketzd2numzd2_1622;

					{	/* Llib/weakhash.scm 443 */
						long BgL_arg1509z00_1652;

						{	/* Llib/weakhash.scm 443 */
							obj_t BgL_hashnz00_3077;

							BgL_hashnz00_3077 = STRUCT_REF(BgL_tablez00_83, (int) (4L));
							if (PROCEDUREP(BgL_hashnz00_3077))
								{	/* Llib/weakhash.scm 443 */
									obj_t BgL_arg1268z00_3079;

									BgL_arg1268z00_3079 =
										BGL_PROCEDURE_CALL1(BgL_hashnz00_3077, BgL_keyz00_84);
									{	/* Llib/weakhash.scm 443 */
										long BgL_nz00_3081;

										BgL_nz00_3081 = (long) CINT(BgL_arg1268z00_3079);
										if ((BgL_nz00_3081 < 0L))
											{	/* Llib/weakhash.scm 443 */
												BgL_arg1509z00_1652 = NEG(BgL_nz00_3081);
											}
										else
											{	/* Llib/weakhash.scm 443 */
												BgL_arg1509z00_1652 = BgL_nz00_3081;
											}
									}
								}
							else
								{	/* Llib/weakhash.scm 443 */
									if ((BgL_hashnz00_3077 == BGl_symbol2173z00zz__weakhashz00))
										{	/* Llib/weakhash.scm 443 */
											BgL_arg1509z00_1652 =
												BGl_getzd2hashnumberzd2persistentz00zz__hashz00
												(BgL_keyz00_84);
										}
									else
										{	/* Llib/weakhash.scm 443 */
											BgL_arg1509z00_1652 =
												BGl_getzd2hashnumberzd2zz__hashz00(BgL_keyz00_84);
										}
								}
						}
						{	/* Llib/weakhash.scm 443 */
							long BgL_n1z00_3085;
							long BgL_n2z00_3086;

							BgL_n1z00_3085 = BgL_arg1509z00_1652;
							BgL_n2z00_3086 = VECTOR_LENGTH(((obj_t) BgL_bucketsz00_1620));
							{	/* Llib/weakhash.scm 443 */
								bool_t BgL_test2356z00_5475;

								{	/* Llib/weakhash.scm 443 */
									long BgL_arg1961z00_3088;

									BgL_arg1961z00_3088 =
										(((BgL_n1z00_3085) | (BgL_n2z00_3086)) & -2147483648);
									BgL_test2356z00_5475 = (BgL_arg1961z00_3088 == 0L);
								}
								if (BgL_test2356z00_5475)
									{	/* Llib/weakhash.scm 443 */
										int32_t BgL_arg1958z00_3089;

										{	/* Llib/weakhash.scm 443 */
											int32_t BgL_arg1959z00_3090;
											int32_t BgL_arg1960z00_3091;

											BgL_arg1959z00_3090 = (int32_t) (BgL_n1z00_3085);
											BgL_arg1960z00_3091 = (int32_t) (BgL_n2z00_3086);
											BgL_arg1958z00_3089 =
												(BgL_arg1959z00_3090 % BgL_arg1960z00_3091);
										}
										{	/* Llib/weakhash.scm 443 */
											long BgL_arg2069z00_3096;

											BgL_arg2069z00_3096 = (long) (BgL_arg1958z00_3089);
											BgL_bucketzd2numzd2_1622 = (long) (BgL_arg2069z00_3096);
									}}
								else
									{	/* Llib/weakhash.scm 443 */
										BgL_bucketzd2numzd2_1622 =
											(BgL_n1z00_3085 % BgL_n2z00_3086);
									}
							}
						}
					}
					{	/* Llib/weakhash.scm 443 */
						obj_t BgL_bucketz00_1623;

						BgL_bucketz00_1623 =
							VECTOR_REF(
							((obj_t) BgL_bucketsz00_1620), BgL_bucketzd2numzd2_1622);
						{	/* Llib/weakhash.scm 444 */
							obj_t BgL_maxzd2bucketzd2lenz00_1624;

							BgL_maxzd2bucketzd2lenz00_1624 =
								STRUCT_REF(BgL_tablez00_83, (int) (1L));
							{	/* Llib/weakhash.scm 445 */

								if (NULLP(BgL_bucketz00_1623))
									{	/* Llib/weakhash.scm 446 */
										{	/* Llib/weakhash.scm 448 */
											long BgL_arg1487z00_1626;

											BgL_arg1487z00_1626 =
												(
												(long) CINT(STRUCT_REF(BgL_tablez00_83,
														(int) (0L))) + 1L);
											{	/* Llib/weakhash.scm 448 */
												obj_t BgL_auxz00_5496;
												int BgL_tmpz00_5494;

												BgL_auxz00_5496 = BINT(BgL_arg1487z00_1626);
												BgL_tmpz00_5494 = (int) (0L);
												STRUCT_SET(BgL_tablez00_83, BgL_tmpz00_5494,
													BgL_auxz00_5496);
										}}
										{	/* Llib/weakhash.scm 449 */
											obj_t BgL_arg1489z00_1628;

											{	/* Llib/weakhash.scm 449 */
												obj_t BgL_arg1490z00_1629;

												BgL_arg1490z00_1629 =
													bgl_make_weakptr(BgL_keyz00_84, BgL_objz00_85);
												{	/* Llib/weakhash.scm 449 */
													obj_t BgL_list1491z00_1630;

													BgL_list1491z00_1630 =
														MAKE_YOUNG_PAIR(BgL_arg1490z00_1629, BNIL);
													BgL_arg1489z00_1628 = BgL_list1491z00_1630;
											}}
											VECTOR_SET(
												((obj_t) BgL_bucketsz00_1620), BgL_bucketzd2numzd2_1622,
												BgL_arg1489z00_1628);
										}
										return BgL_objz00_85;
									}
								else
									{
										obj_t BgL_buckz00_1632;
										long BgL_countz00_1633;

										BgL_buckz00_1632 = BgL_bucketz00_1623;
										BgL_countz00_1633 = 0L;
									BgL_zc3z04anonymousza31492ze3z87_1634:
										if (NULLP(BgL_buckz00_1632))
											{	/* Llib/weakhash.scm 454 */
												{	/* Llib/weakhash.scm 455 */
													long BgL_arg1494z00_1636;

													BgL_arg1494z00_1636 =
														(
														(long) CINT(STRUCT_REF(BgL_tablez00_83,
																(int) (0L))) + 1L);
													{	/* Llib/weakhash.scm 455 */
														obj_t BgL_auxz00_5511;
														int BgL_tmpz00_5509;

														BgL_auxz00_5511 = BINT(BgL_arg1494z00_1636);
														BgL_tmpz00_5509 = (int) (0L);
														STRUCT_SET(BgL_tablez00_83, BgL_tmpz00_5509,
															BgL_auxz00_5511);
												}}
												{	/* Llib/weakhash.scm 457 */
													obj_t BgL_arg1497z00_1638;

													BgL_arg1497z00_1638 =
														MAKE_YOUNG_PAIR(bgl_make_weakptr(BgL_keyz00_84,
															BgL_objz00_85), BgL_bucketz00_1623);
													VECTOR_SET(((obj_t) BgL_bucketsz00_1620),
														BgL_bucketzd2numzd2_1622, BgL_arg1497z00_1638);
												}
												if (
													(BgL_countz00_1633 >
														(long) CINT(BgL_maxzd2bucketzd2lenz00_1624)))
													{	/* Llib/weakhash.scm 458 */
														BGl_weakzd2keyszd2hashtablezd2expandz12zc0zz__weakhashz00
															(BgL_tablez00_83);
													}
												else
													{	/* Llib/weakhash.scm 458 */
														BFALSE;
													}
												return BgL_objz00_85;
											}
										else
											{	/* Llib/weakhash.scm 461 */
												bool_t BgL_test2360z00_5522;

												{	/* Llib/weakhash.scm 461 */
													obj_t BgL_arg1507z00_1649;

													{	/* Llib/weakhash.scm 461 */
														obj_t BgL_arg1508z00_1650;

														BgL_arg1508z00_1650 =
															CAR(((obj_t) BgL_buckz00_1632));
														BgL_arg1507z00_1649 =
															bgl_weakptr_data(((obj_t) BgL_arg1508z00_1650));
													}
													{	/* Llib/weakhash.scm 461 */
														obj_t BgL_eqtz00_3116;

														BgL_eqtz00_3116 =
															STRUCT_REF(BgL_tablez00_83, (int) (3L));
														if (PROCEDUREP(BgL_eqtz00_3116))
															{	/* Llib/weakhash.scm 461 */
																BgL_test2360z00_5522 =
																	CBOOL(BGL_PROCEDURE_CALL2(BgL_eqtz00_3116,
																		BgL_arg1507z00_1649, BgL_keyz00_84));
															}
														else
															{	/* Llib/weakhash.scm 461 */
																if ((BgL_arg1507z00_1649 == BgL_keyz00_84))
																	{	/* Llib/weakhash.scm 461 */
																		BgL_test2360z00_5522 = ((bool_t) 1);
																	}
																else
																	{	/* Llib/weakhash.scm 461 */
																		if (STRINGP(BgL_arg1507z00_1649))
																			{	/* Llib/weakhash.scm 461 */
																				if (STRINGP(BgL_keyz00_84))
																					{	/* Llib/weakhash.scm 461 */
																						long BgL_l1z00_3123;

																						BgL_l1z00_3123 =
																							STRING_LENGTH
																							(BgL_arg1507z00_1649);
																						if ((BgL_l1z00_3123 ==
																								STRING_LENGTH(BgL_keyz00_84)))
																							{	/* Llib/weakhash.scm 461 */
																								int BgL_arg1844z00_3126;

																								{	/* Llib/weakhash.scm 461 */
																									char *BgL_auxz00_5549;
																									char *BgL_tmpz00_5547;

																									BgL_auxz00_5549 =
																										BSTRING_TO_STRING
																										(BgL_keyz00_84);
																									BgL_tmpz00_5547 =
																										BSTRING_TO_STRING
																										(BgL_arg1507z00_1649);
																									BgL_arg1844z00_3126 =
																										memcmp(BgL_tmpz00_5547,
																										BgL_auxz00_5549,
																										BgL_l1z00_3123);
																								}
																								BgL_test2360z00_5522 =
																									(
																									(long) (BgL_arg1844z00_3126)
																									== 0L);
																							}
																						else
																							{	/* Llib/weakhash.scm 461 */
																								BgL_test2360z00_5522 =
																									((bool_t) 0);
																							}
																					}
																				else
																					{	/* Llib/weakhash.scm 461 */
																						BgL_test2360z00_5522 = ((bool_t) 0);
																					}
																			}
																		else
																			{	/* Llib/weakhash.scm 461 */
																				BgL_test2360z00_5522 = ((bool_t) 0);
																			}
																	}
															}
													}
												}
												if (BgL_test2360z00_5522)
													{	/* Llib/weakhash.scm 462 */
														obj_t BgL_oldzd2objzd2_1644;

														{	/* Llib/weakhash.scm 462 */
															obj_t BgL_arg1504z00_1646;

															BgL_arg1504z00_1646 =
																CAR(((obj_t) BgL_buckz00_1632));
															BgL_oldzd2objzd2_1644 =
																bgl_weakptr_ref(((obj_t) BgL_arg1504z00_1646));
														}
														{	/* Llib/weakhash.scm 463 */
															obj_t BgL_arg1503z00_1645;

															BgL_arg1503z00_1645 =
																CAR(((obj_t) BgL_buckz00_1632));
															bgl_weakptr_ref_set(
																((obj_t) BgL_arg1503z00_1645), BgL_objz00_85);
															BUNSPEC;
															BUNSPEC;
														}
														return BgL_oldzd2objzd2_1644;
													}
												else
													{	/* Llib/weakhash.scm 466 */
														obj_t BgL_arg1505z00_1647;
														long BgL_arg1506z00_1648;

														BgL_arg1505z00_1647 =
															CDR(((obj_t) BgL_buckz00_1632));
														BgL_arg1506z00_1648 = (BgL_countz00_1633 + 1L);
														{
															long BgL_countz00_5566;
															obj_t BgL_buckz00_5565;

															BgL_buckz00_5565 = BgL_arg1505z00_1647;
															BgL_countz00_5566 = BgL_arg1506z00_1648;
															BgL_countz00_1633 = BgL_countz00_5566;
															BgL_buckz00_1632 = BgL_buckz00_5565;
															goto BgL_zc3z04anonymousza31492ze3z87_1634;
														}
													}
											}
									}
							}
						}
					}
				}
			}
		}

	}



/* weak-old-hashtable-put! */
	obj_t BGl_weakzd2oldzd2hashtablezd2putz12zc0zz__weakhashz00(obj_t
		BgL_tablez00_86, obj_t BgL_keyz00_87, obj_t BgL_objz00_88)
	{
		{	/* Llib/weakhash.scm 471 */
			{	/* Llib/weakhash.scm 472 */
				obj_t BgL_bucketsz00_1653;

				BgL_bucketsz00_1653 = STRUCT_REF(BgL_tablez00_86, (int) (2L));
				{	/* Llib/weakhash.scm 473 */
					long BgL_bucketzd2numzd2_1655;

					{	/* Llib/weakhash.scm 474 */
						long BgL_arg1535z00_1687;

						{	/* Llib/weakhash.scm 474 */
							obj_t BgL_hashnz00_3140;

							BgL_hashnz00_3140 = STRUCT_REF(BgL_tablez00_86, (int) (4L));
							if (PROCEDUREP(BgL_hashnz00_3140))
								{	/* Llib/weakhash.scm 474 */
									obj_t BgL_arg1268z00_3142;

									BgL_arg1268z00_3142 =
										BGL_PROCEDURE_CALL1(BgL_hashnz00_3140, BgL_keyz00_87);
									{	/* Llib/weakhash.scm 474 */
										long BgL_nz00_3144;

										BgL_nz00_3144 = (long) CINT(BgL_arg1268z00_3142);
										if ((BgL_nz00_3144 < 0L))
											{	/* Llib/weakhash.scm 474 */
												BgL_arg1535z00_1687 = NEG(BgL_nz00_3144);
											}
										else
											{	/* Llib/weakhash.scm 474 */
												BgL_arg1535z00_1687 = BgL_nz00_3144;
											}
									}
								}
							else
								{	/* Llib/weakhash.scm 474 */
									if ((BgL_hashnz00_3140 == BGl_symbol2173z00zz__weakhashz00))
										{	/* Llib/weakhash.scm 474 */
											BgL_arg1535z00_1687 =
												BGl_getzd2hashnumberzd2persistentz00zz__hashz00
												(BgL_keyz00_87);
										}
									else
										{	/* Llib/weakhash.scm 474 */
											BgL_arg1535z00_1687 =
												BGl_getzd2hashnumberzd2zz__hashz00(BgL_keyz00_87);
										}
								}
						}
						{	/* Llib/weakhash.scm 474 */
							long BgL_n1z00_3148;
							long BgL_n2z00_3149;

							BgL_n1z00_3148 = BgL_arg1535z00_1687;
							BgL_n2z00_3149 = VECTOR_LENGTH(((obj_t) BgL_bucketsz00_1653));
							{	/* Llib/weakhash.scm 474 */
								bool_t BgL_test2369z00_5587;

								{	/* Llib/weakhash.scm 474 */
									long BgL_arg1961z00_3151;

									BgL_arg1961z00_3151 =
										(((BgL_n1z00_3148) | (BgL_n2z00_3149)) & -2147483648);
									BgL_test2369z00_5587 = (BgL_arg1961z00_3151 == 0L);
								}
								if (BgL_test2369z00_5587)
									{	/* Llib/weakhash.scm 474 */
										int32_t BgL_arg1958z00_3152;

										{	/* Llib/weakhash.scm 474 */
											int32_t BgL_arg1959z00_3153;
											int32_t BgL_arg1960z00_3154;

											BgL_arg1959z00_3153 = (int32_t) (BgL_n1z00_3148);
											BgL_arg1960z00_3154 = (int32_t) (BgL_n2z00_3149);
											BgL_arg1958z00_3152 =
												(BgL_arg1959z00_3153 % BgL_arg1960z00_3154);
										}
										{	/* Llib/weakhash.scm 474 */
											long BgL_arg2069z00_3159;

											BgL_arg2069z00_3159 = (long) (BgL_arg1958z00_3152);
											BgL_bucketzd2numzd2_1655 = (long) (BgL_arg2069z00_3159);
									}}
								else
									{	/* Llib/weakhash.scm 474 */
										BgL_bucketzd2numzd2_1655 =
											(BgL_n1z00_3148 % BgL_n2z00_3149);
									}
							}
						}
					}
					{	/* Llib/weakhash.scm 474 */
						obj_t BgL_bucketz00_1656;

						BgL_bucketz00_1656 =
							VECTOR_REF(
							((obj_t) BgL_bucketsz00_1653), BgL_bucketzd2numzd2_1655);
						{	/* Llib/weakhash.scm 475 */
							obj_t BgL_maxzd2bucketzd2lenz00_1657;

							BgL_maxzd2bucketzd2lenz00_1657 =
								STRUCT_REF(BgL_tablez00_86, (int) (1L));
							{	/* Llib/weakhash.scm 476 */
								obj_t BgL_countz00_4161;

								BgL_countz00_4161 = MAKE_CELL(BINT(0L));
								{	/* Llib/weakhash.scm 477 */
									obj_t BgL_foundz00_1659;

									{	/* Llib/weakhash.scm 482 */
										obj_t BgL_zc3z04anonymousza31527ze3z87_4151;

										{
											int BgL_tmpz00_5601;

											BgL_tmpz00_5601 = (int) (4L);
											BgL_zc3z04anonymousza31527ze3z87_4151 =
												MAKE_L_PROCEDURE
												(BGl_z62zc3z04anonymousza31527ze3ze5zz__weakhashz00,
												BgL_tmpz00_5601);
										}
										PROCEDURE_L_SET(BgL_zc3z04anonymousza31527ze3z87_4151,
											(int) (0L), ((obj_t) BgL_countz00_4161));
										PROCEDURE_L_SET(BgL_zc3z04anonymousza31527ze3z87_4151,
											(int) (1L), BgL_objz00_88);
										PROCEDURE_L_SET(BgL_zc3z04anonymousza31527ze3z87_4151,
											(int) (2L), BgL_tablez00_86);
										PROCEDURE_L_SET(BgL_zc3z04anonymousza31527ze3z87_4151,
											(int) (3L), BgL_keyz00_87);
										BgL_foundz00_1659 =
											BGl_traversezd2bucketszd2zz__weakhashz00(BgL_tablez00_86,
											BgL_bucketsz00_1653, BgL_bucketzd2numzd2_1655,
											BgL_zc3z04anonymousza31527ze3z87_4151);
									}
									{	/* Llib/weakhash.scm 479 */

										if ((BgL_foundz00_1659 == BGl_keepgoingz00zz__weakhashz00))
											{	/* Llib/weakhash.scm 493 */
												{	/* Llib/weakhash.scm 496 */
													long BgL_arg1510z00_1660;

													BgL_arg1510z00_1660 =
														(
														(long) CINT(STRUCT_REF(BgL_tablez00_86,
																(int) (0L))) + 1L);
													{	/* Llib/weakhash.scm 496 */
														obj_t BgL_auxz00_5622;
														int BgL_tmpz00_5620;

														BgL_auxz00_5622 = BINT(BgL_arg1510z00_1660);
														BgL_tmpz00_5620 = (int) (0L);
														STRUCT_SET(BgL_tablez00_86, BgL_tmpz00_5620,
															BgL_auxz00_5622);
												}}
												{	/* Llib/weakhash.scm 498 */
													obj_t BgL_arg1513z00_1662;

													{	/* Llib/weakhash.scm 498 */
														obj_t BgL_arg1514z00_1663;
														obj_t BgL_arg1516z00_1664;

														{	/* Llib/weakhash.scm 498 */
															obj_t BgL_arg1517z00_1665;
															obj_t BgL_arg1521z00_1666;

															if (BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00
																(BgL_tablez00_86))
																{	/* Llib/weakhash.scm 499 */

																	BgL_arg1517z00_1665 =
																		bgl_make_weakptr(BgL_keyz00_87, BFALSE);
																}
															else
																{	/* Llib/weakhash.scm 498 */
																	BgL_arg1517z00_1665 = BgL_keyz00_87;
																}
															if (BGl_hashtablezd2weakzd2datazf3zf3zz__hashz00
																(BgL_tablez00_86))
																{	/* Llib/weakhash.scm 502 */

																	BgL_arg1521z00_1666 =
																		bgl_make_weakptr(BgL_objz00_88, BFALSE);
																}
															else
																{	/* Llib/weakhash.scm 501 */
																	BgL_arg1521z00_1666 = BgL_objz00_88;
																}
															BgL_arg1514z00_1663 =
																MAKE_YOUNG_PAIR(BgL_arg1517z00_1665,
																BgL_arg1521z00_1666);
														}
														{	/* Llib/weakhash.scm 506 */
															obj_t BgL_arg1524z00_1673;

															BgL_arg1524z00_1673 =
																STRUCT_REF(BgL_tablez00_86, (int) (2L));
															BgL_arg1516z00_1664 =
																VECTOR_REF(
																((obj_t) BgL_arg1524z00_1673),
																BgL_bucketzd2numzd2_1655);
														}
														BgL_arg1513z00_1662 =
															MAKE_YOUNG_PAIR(BgL_arg1514z00_1663,
															BgL_arg1516z00_1664);
													}
													VECTOR_SET(
														((obj_t) BgL_bucketsz00_1653),
														BgL_bucketzd2numzd2_1655, BgL_arg1513z00_1662);
												}
												if (
													((long) CINT(CELL_REF(BgL_countz00_4161)) >
														(long) CINT(BgL_maxzd2bucketzd2lenz00_1657)))
													{	/* Llib/weakhash.scm 508 */
														if (BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00
															(BgL_tablez00_86))
															{	/* Llib/weakhash.scm 877 */
																BGl_weakzd2keyszd2hashtablezd2expandz12zc0zz__weakhashz00
																	(BgL_tablez00_86);
															}
														else
															{	/* Llib/weakhash.scm 877 */
																BGl_weakzd2oldzd2hashtablezd2expandz12zc0zz__weakhashz00
																	(BgL_tablez00_86);
															}
													}
												else
													{	/* Llib/weakhash.scm 508 */
														BFALSE;
													}
												return BgL_objz00_88;
											}
										else
											{	/* Llib/weakhash.scm 493 */
												return BgL_foundz00_1659;
											}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &<@anonymous:1527> */
	obj_t BGl_z62zc3z04anonymousza31527ze3ze5zz__weakhashz00(obj_t
		BgL_envz00_4152, obj_t BgL_bkeyz00_4157, obj_t BgL_valz00_4158,
		obj_t BgL_bucketz00_4159)
	{
		{	/* Llib/weakhash.scm 481 */
			{	/* Llib/weakhash.scm 482 */
				obj_t BgL_countz00_4153;
				obj_t BgL_objz00_4154;
				obj_t BgL_tablez00_4155;
				obj_t BgL_keyz00_4156;

				BgL_countz00_4153 = PROCEDURE_L_REF(BgL_envz00_4152, (int) (0L));
				BgL_objz00_4154 = PROCEDURE_L_REF(BgL_envz00_4152, (int) (1L));
				BgL_tablez00_4155 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_4152, (int) (2L)));
				BgL_keyz00_4156 = PROCEDURE_L_REF(BgL_envz00_4152, (int) (3L));
				{	/* Llib/weakhash.scm 482 */
					obj_t BgL_auxz00_4276;

					BgL_auxz00_4276 = ADDFX(CELL_REF(BgL_countz00_4153), BINT(1L));
					CELL_SET(BgL_countz00_4153, BgL_auxz00_4276);
				}
				{	/* Llib/weakhash.scm 483 */
					bool_t BgL_test2375z00_5658;

					{	/* Llib/weakhash.scm 483 */
						obj_t BgL_eqtz00_4277;

						BgL_eqtz00_4277 = STRUCT_REF(BgL_tablez00_4155, (int) (3L));
						if (PROCEDUREP(BgL_eqtz00_4277))
							{	/* Llib/weakhash.scm 483 */
								BgL_test2375z00_5658 =
									CBOOL(BGL_PROCEDURE_CALL2(BgL_eqtz00_4277, BgL_bkeyz00_4157,
										BgL_keyz00_4156));
							}
						else
							{	/* Llib/weakhash.scm 483 */
								if ((BgL_bkeyz00_4157 == BgL_keyz00_4156))
									{	/* Llib/weakhash.scm 483 */
										BgL_test2375z00_5658 = ((bool_t) 1);
									}
								else
									{	/* Llib/weakhash.scm 483 */
										if (STRINGP(BgL_bkeyz00_4157))
											{	/* Llib/weakhash.scm 483 */
												if (STRINGP(BgL_keyz00_4156))
													{	/* Llib/weakhash.scm 483 */
														long BgL_l1z00_4278;

														BgL_l1z00_4278 = STRING_LENGTH(BgL_bkeyz00_4157);
														if (
															(BgL_l1z00_4278 ==
																STRING_LENGTH(BgL_keyz00_4156)))
															{	/* Llib/weakhash.scm 483 */
																int BgL_arg1844z00_4279;

																{	/* Llib/weakhash.scm 483 */
																	char *BgL_auxz00_5681;
																	char *BgL_tmpz00_5679;

																	BgL_auxz00_5681 =
																		BSTRING_TO_STRING(BgL_keyz00_4156);
																	BgL_tmpz00_5679 =
																		BSTRING_TO_STRING(BgL_bkeyz00_4157);
																	BgL_arg1844z00_4279 =
																		memcmp(BgL_tmpz00_5679, BgL_auxz00_5681,
																		BgL_l1z00_4278);
																}
																BgL_test2375z00_5658 =
																	((long) (BgL_arg1844z00_4279) == 0L);
															}
														else
															{	/* Llib/weakhash.scm 483 */
																BgL_test2375z00_5658 = ((bool_t) 0);
															}
													}
												else
													{	/* Llib/weakhash.scm 483 */
														BgL_test2375z00_5658 = ((bool_t) 0);
													}
											}
										else
											{	/* Llib/weakhash.scm 483 */
												BgL_test2375z00_5658 = ((bool_t) 0);
											}
									}
							}
					}
					if (BgL_test2375z00_5658)
						{	/* Llib/weakhash.scm 483 */
							{	/* Llib/weakhash.scm 485 */
								obj_t BgL_arg1529z00_4280;
								obj_t BgL_arg1530z00_4281;

								BgL_arg1529z00_4280 = CAR(((obj_t) BgL_bucketz00_4159));
								if (BGl_hashtablezd2weakzd2datazf3zf3zz__hashz00
									(BgL_tablez00_4155))
									{	/* Llib/weakhash.scm 487 */

										BgL_arg1530z00_4281 =
											bgl_make_weakptr(BgL_objz00_4154, BFALSE);
									}
								else
									{	/* Llib/weakhash.scm 486 */
										BgL_arg1530z00_4281 = BgL_objz00_4154;
									}
								{	/* Llib/weakhash.scm 485 */
									obj_t BgL_tmpz00_5691;

									BgL_tmpz00_5691 = ((obj_t) BgL_arg1529z00_4280);
									SET_CDR(BgL_tmpz00_5691, BgL_arg1530z00_4281);
								}
							}
							return BgL_valz00_4158;
						}
					else
						{	/* Llib/weakhash.scm 483 */
							return BGl_keepgoingz00zz__weakhashz00;
						}
				}
			}
		}

	}



/* weak-hashtable-put! */
	BGL_EXPORTED_DEF obj_t BGl_weakzd2hashtablezd2putz12z12zz__weakhashz00(obj_t
		BgL_tablez00_89, obj_t BgL_keyz00_90, obj_t BgL_objz00_91)
	{
		{	/* Llib/weakhash.scm 515 */
			if (BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(BgL_tablez00_89))
				{	/* Llib/weakhash.scm 516 */
					BGL_TAIL return
						BGl_weakzd2keyszd2hashtablezd2putz12zc0zz__weakhashz00
						(BgL_tablez00_89, BgL_keyz00_90, BgL_objz00_91);
				}
			else
				{	/* Llib/weakhash.scm 516 */
					BGL_TAIL return
						BGl_weakzd2oldzd2hashtablezd2putz12zc0zz__weakhashz00
						(BgL_tablez00_89, BgL_keyz00_90, BgL_objz00_91);
				}
		}

	}



/* &weak-hashtable-put! */
	obj_t BGl_z62weakzd2hashtablezd2putz12z70zz__weakhashz00(obj_t
		BgL_envz00_4163, obj_t BgL_tablez00_4164, obj_t BgL_keyz00_4165,
		obj_t BgL_objz00_4166)
	{
		{	/* Llib/weakhash.scm 515 */
			{	/* Llib/weakhash.scm 516 */
				obj_t BgL_auxz00_5698;

				if (STRUCTP(BgL_tablez00_4164))
					{	/* Llib/weakhash.scm 516 */
						BgL_auxz00_5698 = BgL_tablez00_4164;
					}
				else
					{
						obj_t BgL_auxz00_5701;

						BgL_auxz00_5701 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2161z00zz__weakhashz00,
							BINT(20423L), BGl_string2177z00zz__weakhashz00,
							BGl_string2163z00zz__weakhashz00, BgL_tablez00_4164);
						FAILURE(BgL_auxz00_5701, BFALSE, BFALSE);
					}
				return
					BGl_weakzd2hashtablezd2putz12z12zz__weakhashz00(BgL_auxz00_5698,
					BgL_keyz00_4165, BgL_objz00_4166);
			}
		}

	}



/* weak-keys-hashtable-update! */
	obj_t BGl_weakzd2keyszd2hashtablezd2updatez12zc0zz__weakhashz00(obj_t
		BgL_tablez00_92, obj_t BgL_keyz00_93, obj_t BgL_procz00_94,
		obj_t BgL_objz00_95)
	{
		{	/* Llib/weakhash.scm 523 */
			{	/* Llib/weakhash.scm 524 */
				obj_t BgL_bucketsz00_1689;

				BgL_bucketsz00_1689 = STRUCT_REF(BgL_tablez00_92, (int) (2L));
				{	/* Llib/weakhash.scm 525 */
					long BgL_bucketzd2numzd2_1691;

					{	/* Llib/weakhash.scm 526 */
						long BgL_arg1573z00_1722;

						{	/* Llib/weakhash.scm 526 */
							obj_t BgL_hashnz00_3204;

							BgL_hashnz00_3204 = STRUCT_REF(BgL_tablez00_92, (int) (4L));
							if (PROCEDUREP(BgL_hashnz00_3204))
								{	/* Llib/weakhash.scm 526 */
									obj_t BgL_arg1268z00_3206;

									BgL_arg1268z00_3206 =
										BGL_PROCEDURE_CALL1(BgL_hashnz00_3204, BgL_keyz00_93);
									{	/* Llib/weakhash.scm 526 */
										long BgL_nz00_3208;

										BgL_nz00_3208 = (long) CINT(BgL_arg1268z00_3206);
										if ((BgL_nz00_3208 < 0L))
											{	/* Llib/weakhash.scm 526 */
												BgL_arg1573z00_1722 = NEG(BgL_nz00_3208);
											}
										else
											{	/* Llib/weakhash.scm 526 */
												BgL_arg1573z00_1722 = BgL_nz00_3208;
											}
									}
								}
							else
								{	/* Llib/weakhash.scm 526 */
									if ((BgL_hashnz00_3204 == BGl_symbol2173z00zz__weakhashz00))
										{	/* Llib/weakhash.scm 526 */
											BgL_arg1573z00_1722 =
												BGl_getzd2hashnumberzd2persistentz00zz__hashz00
												(BgL_keyz00_93);
										}
									else
										{	/* Llib/weakhash.scm 526 */
											BgL_arg1573z00_1722 =
												BGl_getzd2hashnumberzd2zz__hashz00(BgL_keyz00_93);
										}
								}
						}
						{	/* Llib/weakhash.scm 526 */
							long BgL_n1z00_3212;
							long BgL_n2z00_3213;

							BgL_n1z00_3212 = BgL_arg1573z00_1722;
							BgL_n2z00_3213 = VECTOR_LENGTH(((obj_t) BgL_bucketsz00_1689));
							{	/* Llib/weakhash.scm 526 */
								bool_t BgL_test2387z00_5726;

								{	/* Llib/weakhash.scm 526 */
									long BgL_arg1961z00_3215;

									BgL_arg1961z00_3215 =
										(((BgL_n1z00_3212) | (BgL_n2z00_3213)) & -2147483648);
									BgL_test2387z00_5726 = (BgL_arg1961z00_3215 == 0L);
								}
								if (BgL_test2387z00_5726)
									{	/* Llib/weakhash.scm 526 */
										int32_t BgL_arg1958z00_3216;

										{	/* Llib/weakhash.scm 526 */
											int32_t BgL_arg1959z00_3217;
											int32_t BgL_arg1960z00_3218;

											BgL_arg1959z00_3217 = (int32_t) (BgL_n1z00_3212);
											BgL_arg1960z00_3218 = (int32_t) (BgL_n2z00_3213);
											BgL_arg1958z00_3216 =
												(BgL_arg1959z00_3217 % BgL_arg1960z00_3218);
										}
										{	/* Llib/weakhash.scm 526 */
											long BgL_arg2069z00_3223;

											BgL_arg2069z00_3223 = (long) (BgL_arg1958z00_3216);
											BgL_bucketzd2numzd2_1691 = (long) (BgL_arg2069z00_3223);
									}}
								else
									{	/* Llib/weakhash.scm 526 */
										BgL_bucketzd2numzd2_1691 =
											(BgL_n1z00_3212 % BgL_n2z00_3213);
									}
							}
						}
					}
					{	/* Llib/weakhash.scm 526 */
						obj_t BgL_bucketz00_1692;

						BgL_bucketz00_1692 =
							VECTOR_REF(
							((obj_t) BgL_bucketsz00_1689), BgL_bucketzd2numzd2_1691);
						{	/* Llib/weakhash.scm 527 */
							obj_t BgL_maxzd2bucketzd2lenz00_1693;

							BgL_maxzd2bucketzd2lenz00_1693 =
								STRUCT_REF(BgL_tablez00_92, (int) (1L));
							{	/* Llib/weakhash.scm 528 */

								if (NULLP(BgL_bucketz00_1692))
									{	/* Llib/weakhash.scm 529 */
										{	/* Llib/weakhash.scm 531 */
											long BgL_arg1539z00_1695;

											BgL_arg1539z00_1695 =
												(
												(long) CINT(STRUCT_REF(BgL_tablez00_92,
														(int) (0L))) + 1L);
											{	/* Llib/weakhash.scm 531 */
												obj_t BgL_auxz00_5747;
												int BgL_tmpz00_5745;

												BgL_auxz00_5747 = BINT(BgL_arg1539z00_1695);
												BgL_tmpz00_5745 = (int) (0L);
												STRUCT_SET(BgL_tablez00_92, BgL_tmpz00_5745,
													BgL_auxz00_5747);
										}}
										{	/* Llib/weakhash.scm 532 */
											obj_t BgL_arg1543z00_1697;

											{	/* Llib/weakhash.scm 532 */
												obj_t BgL_arg1544z00_1698;

												BgL_arg1544z00_1698 =
													MAKE_YOUNG_PAIR(BgL_keyz00_93, BgL_objz00_95);
												{	/* Llib/weakhash.scm 532 */
													obj_t BgL_list1545z00_1699;

													BgL_list1545z00_1699 =
														MAKE_YOUNG_PAIR(BgL_arg1544z00_1698, BNIL);
													BgL_arg1543z00_1697 = BgL_list1545z00_1699;
											}}
											VECTOR_SET(
												((obj_t) BgL_bucketsz00_1689), BgL_bucketzd2numzd2_1691,
												BgL_arg1543z00_1697);
										}
										return BgL_objz00_95;
									}
								else
									{
										obj_t BgL_buckz00_1701;
										long BgL_countz00_1702;

										BgL_buckz00_1701 = BgL_bucketz00_1692;
										BgL_countz00_1702 = 0L;
									BgL_zc3z04anonymousza31546ze3z87_1703:
										if (NULLP(BgL_buckz00_1701))
											{	/* Llib/weakhash.scm 537 */
												{	/* Llib/weakhash.scm 538 */
													long BgL_arg1549z00_1705;

													BgL_arg1549z00_1705 =
														(
														(long) CINT(STRUCT_REF(BgL_tablez00_92,
																(int) (0L))) + 1L);
													{	/* Llib/weakhash.scm 538 */
														obj_t BgL_auxz00_5762;
														int BgL_tmpz00_5760;

														BgL_auxz00_5762 = BINT(BgL_arg1549z00_1705);
														BgL_tmpz00_5760 = (int) (0L);
														STRUCT_SET(BgL_tablez00_92, BgL_tmpz00_5760,
															BgL_auxz00_5762);
												}}
												{	/* Llib/weakhash.scm 540 */
													obj_t BgL_arg1553z00_1707;

													BgL_arg1553z00_1707 =
														MAKE_YOUNG_PAIR(bgl_make_weakptr(BgL_keyz00_93,
															BgL_objz00_95), BgL_bucketz00_1692);
													VECTOR_SET(((obj_t) BgL_bucketsz00_1689),
														BgL_bucketzd2numzd2_1691, BgL_arg1553z00_1707);
												}
												if (
													(BgL_countz00_1702 >
														(long) CINT(BgL_maxzd2bucketzd2lenz00_1693)))
													{	/* Llib/weakhash.scm 541 */
														BGl_weakzd2keyszd2hashtablezd2expandz12zc0zz__weakhashz00
															(BgL_tablez00_92);
													}
												else
													{	/* Llib/weakhash.scm 541 */
														BFALSE;
													}
												return BgL_objz00_95;
											}
										else
											{	/* Llib/weakhash.scm 544 */
												bool_t BgL_test2391z00_5773;

												{	/* Llib/weakhash.scm 544 */
													obj_t BgL_arg1567z00_1719;

													{	/* Llib/weakhash.scm 544 */
														obj_t BgL_arg1571z00_1720;

														BgL_arg1571z00_1720 =
															CAR(((obj_t) BgL_buckz00_1701));
														BgL_arg1567z00_1719 =
															bgl_weakptr_data(((obj_t) BgL_arg1571z00_1720));
													}
													{	/* Llib/weakhash.scm 544 */
														obj_t BgL_eqtz00_3243;

														BgL_eqtz00_3243 =
															STRUCT_REF(BgL_tablez00_92, (int) (3L));
														if (PROCEDUREP(BgL_eqtz00_3243))
															{	/* Llib/weakhash.scm 544 */
																BgL_test2391z00_5773 =
																	CBOOL(BGL_PROCEDURE_CALL2(BgL_eqtz00_3243,
																		BgL_arg1567z00_1719, BgL_keyz00_93));
															}
														else
															{	/* Llib/weakhash.scm 544 */
																if ((BgL_arg1567z00_1719 == BgL_keyz00_93))
																	{	/* Llib/weakhash.scm 544 */
																		BgL_test2391z00_5773 = ((bool_t) 1);
																	}
																else
																	{	/* Llib/weakhash.scm 544 */
																		if (STRINGP(BgL_arg1567z00_1719))
																			{	/* Llib/weakhash.scm 544 */
																				if (STRINGP(BgL_keyz00_93))
																					{	/* Llib/weakhash.scm 544 */
																						long BgL_l1z00_3250;

																						BgL_l1z00_3250 =
																							STRING_LENGTH
																							(BgL_arg1567z00_1719);
																						if ((BgL_l1z00_3250 ==
																								STRING_LENGTH(BgL_keyz00_93)))
																							{	/* Llib/weakhash.scm 544 */
																								int BgL_arg1844z00_3253;

																								{	/* Llib/weakhash.scm 544 */
																									char *BgL_auxz00_5800;
																									char *BgL_tmpz00_5798;

																									BgL_auxz00_5800 =
																										BSTRING_TO_STRING
																										(BgL_keyz00_93);
																									BgL_tmpz00_5798 =
																										BSTRING_TO_STRING
																										(BgL_arg1567z00_1719);
																									BgL_arg1844z00_3253 =
																										memcmp(BgL_tmpz00_5798,
																										BgL_auxz00_5800,
																										BgL_l1z00_3250);
																								}
																								BgL_test2391z00_5773 =
																									(
																									(long) (BgL_arg1844z00_3253)
																									== 0L);
																							}
																						else
																							{	/* Llib/weakhash.scm 544 */
																								BgL_test2391z00_5773 =
																									((bool_t) 0);
																							}
																					}
																				else
																					{	/* Llib/weakhash.scm 544 */
																						BgL_test2391z00_5773 = ((bool_t) 0);
																					}
																			}
																		else
																			{	/* Llib/weakhash.scm 544 */
																				BgL_test2391z00_5773 = ((bool_t) 0);
																			}
																	}
															}
													}
												}
												if (BgL_test2391z00_5773)
													{	/* Llib/weakhash.scm 545 */
														obj_t BgL_resz00_1713;

														{	/* Llib/weakhash.scm 545 */
															obj_t BgL_arg1561z00_1715;

															{	/* Llib/weakhash.scm 545 */
																obj_t BgL_arg1562z00_1716;

																BgL_arg1562z00_1716 =
																	CAR(((obj_t) BgL_buckz00_1701));
																BgL_arg1561z00_1715 =
																	bgl_weakptr_ref(
																	((obj_t) BgL_arg1562z00_1716));
															}
															BgL_resz00_1713 =
																BGL_PROCEDURE_CALL1(BgL_procz00_94,
																BgL_arg1561z00_1715);
														}
														{	/* Llib/weakhash.scm 546 */
															obj_t BgL_arg1559z00_1714;

															BgL_arg1559z00_1714 =
																CAR(((obj_t) BgL_buckz00_1701));
															bgl_weakptr_ref_set(
																((obj_t) BgL_arg1559z00_1714), BgL_resz00_1713);
															BUNSPEC;
															BUNSPEC;
														}
														return BgL_resz00_1713;
													}
												else
													{	/* Llib/weakhash.scm 549 */
														obj_t BgL_arg1564z00_1717;
														long BgL_arg1565z00_1718;

														BgL_arg1564z00_1717 =
															CDR(((obj_t) BgL_buckz00_1701));
														BgL_arg1565z00_1718 = (BgL_countz00_1702 + 1L);
														{
															long BgL_countz00_5821;
															obj_t BgL_buckz00_5820;

															BgL_buckz00_5820 = BgL_arg1564z00_1717;
															BgL_countz00_5821 = BgL_arg1565z00_1718;
															BgL_countz00_1702 = BgL_countz00_5821;
															BgL_buckz00_1701 = BgL_buckz00_5820;
															goto BgL_zc3z04anonymousza31546ze3z87_1703;
														}
													}
											}
									}
							}
						}
					}
				}
			}
		}

	}



/* weak-old-hashtable-update! */
	obj_t BGl_weakzd2oldzd2hashtablezd2updatez12zc0zz__weakhashz00(obj_t
		BgL_tablez00_96, obj_t BgL_keyz00_97, obj_t BgL_procz00_98,
		obj_t BgL_objz00_99)
	{
		{	/* Llib/weakhash.scm 554 */
			{	/* Llib/weakhash.scm 555 */
				obj_t BgL_bucketsz00_1723;

				BgL_bucketsz00_1723 = STRUCT_REF(BgL_tablez00_96, (int) (2L));
				{	/* Llib/weakhash.scm 556 */
					long BgL_bucketzd2numzd2_1725;

					{	/* Llib/weakhash.scm 557 */
						long BgL_arg1598z00_1758;

						{	/* Llib/weakhash.scm 557 */
							obj_t BgL_hashnz00_3267;

							BgL_hashnz00_3267 = STRUCT_REF(BgL_tablez00_96, (int) (4L));
							if (PROCEDUREP(BgL_hashnz00_3267))
								{	/* Llib/weakhash.scm 557 */
									obj_t BgL_arg1268z00_3269;

									BgL_arg1268z00_3269 =
										BGL_PROCEDURE_CALL1(BgL_hashnz00_3267, BgL_keyz00_97);
									{	/* Llib/weakhash.scm 557 */
										long BgL_nz00_3271;

										BgL_nz00_3271 = (long) CINT(BgL_arg1268z00_3269);
										if ((BgL_nz00_3271 < 0L))
											{	/* Llib/weakhash.scm 557 */
												BgL_arg1598z00_1758 = NEG(BgL_nz00_3271);
											}
										else
											{	/* Llib/weakhash.scm 557 */
												BgL_arg1598z00_1758 = BgL_nz00_3271;
											}
									}
								}
							else
								{	/* Llib/weakhash.scm 557 */
									if ((BgL_hashnz00_3267 == BGl_symbol2173z00zz__weakhashz00))
										{	/* Llib/weakhash.scm 557 */
											BgL_arg1598z00_1758 =
												BGl_getzd2hashnumberzd2persistentz00zz__hashz00
												(BgL_keyz00_97);
										}
									else
										{	/* Llib/weakhash.scm 557 */
											BgL_arg1598z00_1758 =
												BGl_getzd2hashnumberzd2zz__hashz00(BgL_keyz00_97);
										}
								}
						}
						{	/* Llib/weakhash.scm 557 */
							long BgL_n1z00_3275;
							long BgL_n2z00_3276;

							BgL_n1z00_3275 = BgL_arg1598z00_1758;
							BgL_n2z00_3276 = VECTOR_LENGTH(((obj_t) BgL_bucketsz00_1723));
							{	/* Llib/weakhash.scm 557 */
								bool_t BgL_test2400z00_5842;

								{	/* Llib/weakhash.scm 557 */
									long BgL_arg1961z00_3278;

									BgL_arg1961z00_3278 =
										(((BgL_n1z00_3275) | (BgL_n2z00_3276)) & -2147483648);
									BgL_test2400z00_5842 = (BgL_arg1961z00_3278 == 0L);
								}
								if (BgL_test2400z00_5842)
									{	/* Llib/weakhash.scm 557 */
										int32_t BgL_arg1958z00_3279;

										{	/* Llib/weakhash.scm 557 */
											int32_t BgL_arg1959z00_3280;
											int32_t BgL_arg1960z00_3281;

											BgL_arg1959z00_3280 = (int32_t) (BgL_n1z00_3275);
											BgL_arg1960z00_3281 = (int32_t) (BgL_n2z00_3276);
											BgL_arg1958z00_3279 =
												(BgL_arg1959z00_3280 % BgL_arg1960z00_3281);
										}
										{	/* Llib/weakhash.scm 557 */
											long BgL_arg2069z00_3286;

											BgL_arg2069z00_3286 = (long) (BgL_arg1958z00_3279);
											BgL_bucketzd2numzd2_1725 = (long) (BgL_arg2069z00_3286);
									}}
								else
									{	/* Llib/weakhash.scm 557 */
										BgL_bucketzd2numzd2_1725 =
											(BgL_n1z00_3275 % BgL_n2z00_3276);
									}
							}
						}
					}
					{	/* Llib/weakhash.scm 557 */
						obj_t BgL_bucketz00_1726;

						BgL_bucketz00_1726 =
							VECTOR_REF(
							((obj_t) BgL_bucketsz00_1723), BgL_bucketzd2numzd2_1725);
						{	/* Llib/weakhash.scm 558 */
							obj_t BgL_maxzd2bucketzd2lenz00_1727;

							BgL_maxzd2bucketzd2lenz00_1727 =
								STRUCT_REF(BgL_tablez00_96, (int) (1L));
							{	/* Llib/weakhash.scm 559 */
								obj_t BgL_countz00_4177;

								BgL_countz00_4177 = MAKE_CELL(BINT(0L));
								{	/* Llib/weakhash.scm 560 */
									obj_t BgL_foundz00_1729;

									{	/* Llib/weakhash.scm 565 */
										obj_t BgL_zc3z04anonymousza31590ze3z87_4167;

										{
											int BgL_tmpz00_5856;

											BgL_tmpz00_5856 = (int) (4L);
											BgL_zc3z04anonymousza31590ze3z87_4167 =
												MAKE_L_PROCEDURE
												(BGl_z62zc3z04anonymousza31590ze3ze5zz__weakhashz00,
												BgL_tmpz00_5856);
										}
										PROCEDURE_L_SET(BgL_zc3z04anonymousza31590ze3z87_4167,
											(int) (0L), ((obj_t) BgL_countz00_4177));
										PROCEDURE_L_SET(BgL_zc3z04anonymousza31590ze3z87_4167,
											(int) (1L), BgL_procz00_98);
										PROCEDURE_L_SET(BgL_zc3z04anonymousza31590ze3z87_4167,
											(int) (2L), BgL_tablez00_96);
										PROCEDURE_L_SET(BgL_zc3z04anonymousza31590ze3z87_4167,
											(int) (3L), BgL_keyz00_97);
										BgL_foundz00_1729 =
											BGl_traversezd2bucketszd2zz__weakhashz00(BgL_tablez00_96,
											BgL_bucketsz00_1723, BgL_bucketzd2numzd2_1725,
											BgL_zc3z04anonymousza31590ze3z87_4167);
									}
									{	/* Llib/weakhash.scm 562 */

										if ((BgL_foundz00_1729 == BGl_keepgoingz00zz__weakhashz00))
											{	/* Llib/weakhash.scm 576 */
												{	/* Llib/weakhash.scm 579 */
													long BgL_arg1575z00_1730;

													BgL_arg1575z00_1730 =
														(
														(long) CINT(STRUCT_REF(BgL_tablez00_96,
																(int) (0L))) + 1L);
													{	/* Llib/weakhash.scm 579 */
														obj_t BgL_auxz00_5877;
														int BgL_tmpz00_5875;

														BgL_auxz00_5877 = BINT(BgL_arg1575z00_1730);
														BgL_tmpz00_5875 = (int) (0L);
														STRUCT_SET(BgL_tablez00_96, BgL_tmpz00_5875,
															BgL_auxz00_5877);
												}}
												{	/* Llib/weakhash.scm 581 */
													obj_t BgL_arg1578z00_1732;

													{	/* Llib/weakhash.scm 581 */
														obj_t BgL_arg1579z00_1733;
														obj_t BgL_arg1580z00_1734;

														{	/* Llib/weakhash.scm 581 */
															obj_t BgL_arg1582z00_1735;
															obj_t BgL_arg1583z00_1736;

															if (BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00
																(BgL_tablez00_96))
																{	/* Llib/weakhash.scm 582 */

																	BgL_arg1582z00_1735 =
																		bgl_make_weakptr(BgL_keyz00_97, BFALSE);
																}
															else
																{	/* Llib/weakhash.scm 581 */
																	BgL_arg1582z00_1735 = BgL_keyz00_97;
																}
															if (BGl_hashtablezd2weakzd2datazf3zf3zz__hashz00
																(BgL_tablez00_96))
																{	/* Llib/weakhash.scm 585 */

																	BgL_arg1583z00_1736 =
																		bgl_make_weakptr(BgL_objz00_99, BFALSE);
																}
															else
																{	/* Llib/weakhash.scm 584 */
																	BgL_arg1583z00_1736 = BgL_objz00_99;
																}
															BgL_arg1579z00_1733 =
																MAKE_YOUNG_PAIR(BgL_arg1582z00_1735,
																BgL_arg1583z00_1736);
														}
														{	/* Llib/weakhash.scm 589 */
															obj_t BgL_arg1586z00_1743;

															BgL_arg1586z00_1743 =
																STRUCT_REF(BgL_tablez00_96, (int) (2L));
															BgL_arg1580z00_1734 =
																VECTOR_REF(
																((obj_t) BgL_arg1586z00_1743),
																BgL_bucketzd2numzd2_1725);
														}
														BgL_arg1578z00_1732 =
															MAKE_YOUNG_PAIR(BgL_arg1579z00_1733,
															BgL_arg1580z00_1734);
													}
													VECTOR_SET(
														((obj_t) BgL_bucketsz00_1723),
														BgL_bucketzd2numzd2_1725, BgL_arg1578z00_1732);
												}
												if (
													((long) CINT(CELL_REF(BgL_countz00_4177)) >
														(long) CINT(BgL_maxzd2bucketzd2lenz00_1727)))
													{	/* Llib/weakhash.scm 591 */
														if (BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00
															(BgL_tablez00_96))
															{	/* Llib/weakhash.scm 877 */
																BGl_weakzd2keyszd2hashtablezd2expandz12zc0zz__weakhashz00
																	(BgL_tablez00_96);
															}
														else
															{	/* Llib/weakhash.scm 877 */
																BGl_weakzd2oldzd2hashtablezd2expandz12zc0zz__weakhashz00
																	(BgL_tablez00_96);
															}
													}
												else
													{	/* Llib/weakhash.scm 591 */
														BFALSE;
													}
												return BgL_objz00_99;
											}
										else
											{	/* Llib/weakhash.scm 576 */
												return BgL_foundz00_1729;
											}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &<@anonymous:1590> */
	obj_t BGl_z62zc3z04anonymousza31590ze3ze5zz__weakhashz00(obj_t
		BgL_envz00_4168, obj_t BgL_bkeyz00_4173, obj_t BgL_valz00_4174,
		obj_t BgL_bucketz00_4175)
	{
		{	/* Llib/weakhash.scm 564 */
			{	/* Llib/weakhash.scm 565 */
				obj_t BgL_countz00_4169;
				obj_t BgL_procz00_4170;
				obj_t BgL_tablez00_4171;
				obj_t BgL_keyz00_4172;

				BgL_countz00_4169 = PROCEDURE_L_REF(BgL_envz00_4168, (int) (0L));
				BgL_procz00_4170 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_4168, (int) (1L)));
				BgL_tablez00_4171 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_4168, (int) (2L)));
				BgL_keyz00_4172 = PROCEDURE_L_REF(BgL_envz00_4168, (int) (3L));
				{	/* Llib/weakhash.scm 565 */
					obj_t BgL_auxz00_4282;

					BgL_auxz00_4282 = ADDFX(CELL_REF(BgL_countz00_4169), BINT(1L));
					CELL_SET(BgL_countz00_4169, BgL_auxz00_4282);
				}
				{	/* Llib/weakhash.scm 566 */
					bool_t BgL_test2406z00_5914;

					{	/* Llib/weakhash.scm 566 */
						obj_t BgL_eqtz00_4283;

						BgL_eqtz00_4283 = STRUCT_REF(BgL_tablez00_4171, (int) (3L));
						if (PROCEDUREP(BgL_eqtz00_4283))
							{	/* Llib/weakhash.scm 566 */
								BgL_test2406z00_5914 =
									CBOOL(BGL_PROCEDURE_CALL2(BgL_eqtz00_4283, BgL_bkeyz00_4173,
										BgL_keyz00_4172));
							}
						else
							{	/* Llib/weakhash.scm 566 */
								if ((BgL_bkeyz00_4173 == BgL_keyz00_4172))
									{	/* Llib/weakhash.scm 566 */
										BgL_test2406z00_5914 = ((bool_t) 1);
									}
								else
									{	/* Llib/weakhash.scm 566 */
										if (STRINGP(BgL_bkeyz00_4173))
											{	/* Llib/weakhash.scm 566 */
												if (STRINGP(BgL_keyz00_4172))
													{	/* Llib/weakhash.scm 566 */
														long BgL_l1z00_4284;

														BgL_l1z00_4284 = STRING_LENGTH(BgL_bkeyz00_4173);
														if (
															(BgL_l1z00_4284 ==
																STRING_LENGTH(BgL_keyz00_4172)))
															{	/* Llib/weakhash.scm 566 */
																int BgL_arg1844z00_4285;

																{	/* Llib/weakhash.scm 566 */
																	char *BgL_auxz00_5937;
																	char *BgL_tmpz00_5935;

																	BgL_auxz00_5937 =
																		BSTRING_TO_STRING(BgL_keyz00_4172);
																	BgL_tmpz00_5935 =
																		BSTRING_TO_STRING(BgL_bkeyz00_4173);
																	BgL_arg1844z00_4285 =
																		memcmp(BgL_tmpz00_5935, BgL_auxz00_5937,
																		BgL_l1z00_4284);
																}
																BgL_test2406z00_5914 =
																	((long) (BgL_arg1844z00_4285) == 0L);
															}
														else
															{	/* Llib/weakhash.scm 566 */
																BgL_test2406z00_5914 = ((bool_t) 0);
															}
													}
												else
													{	/* Llib/weakhash.scm 566 */
														BgL_test2406z00_5914 = ((bool_t) 0);
													}
											}
										else
											{	/* Llib/weakhash.scm 566 */
												BgL_test2406z00_5914 = ((bool_t) 0);
											}
									}
							}
					}
					if (BgL_test2406z00_5914)
						{	/* Llib/weakhash.scm 567 */
							obj_t BgL_newvalz00_4286;

							BgL_newvalz00_4286 =
								BGL_PROCEDURE_CALL1(BgL_procz00_4170, BgL_valz00_4174);
							{	/* Llib/weakhash.scm 568 */
								obj_t BgL_arg1593z00_4287;
								obj_t BgL_arg1594z00_4288;

								BgL_arg1593z00_4287 = CAR(((obj_t) BgL_bucketz00_4175));
								if (BGl_hashtablezd2weakzd2datazf3zf3zz__hashz00
									(BgL_tablez00_4171))
									{	/* Llib/weakhash.scm 570 */

										BgL_arg1594z00_4288 =
											bgl_make_weakptr(BgL_newvalz00_4286, BFALSE);
									}
								else
									{	/* Llib/weakhash.scm 569 */
										BgL_arg1594z00_4288 = BgL_newvalz00_4286;
									}
								{	/* Llib/weakhash.scm 568 */
									obj_t BgL_tmpz00_5951;

									BgL_tmpz00_5951 = ((obj_t) BgL_arg1593z00_4287);
									SET_CDR(BgL_tmpz00_5951, BgL_arg1594z00_4288);
								}
							}
							return BgL_newvalz00_4286;
						}
					else
						{	/* Llib/weakhash.scm 566 */
							return BGl_keepgoingz00zz__weakhashz00;
						}
				}
			}
		}

	}



/* weak-hashtable-update! */
	BGL_EXPORTED_DEF obj_t
		BGl_weakzd2hashtablezd2updatez12z12zz__weakhashz00(obj_t BgL_tablez00_100,
		obj_t BgL_keyz00_101, obj_t BgL_procz00_102, obj_t BgL_objz00_103)
	{
		{	/* Llib/weakhash.scm 598 */
			if (BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(BgL_tablez00_100))
				{	/* Llib/weakhash.scm 599 */
					BGL_TAIL return
						BGl_weakzd2keyszd2hashtablezd2updatez12zc0zz__weakhashz00
						(BgL_tablez00_100, BgL_keyz00_101, BgL_procz00_102, BgL_objz00_103);
				}
			else
				{	/* Llib/weakhash.scm 599 */
					BGL_TAIL return
						BGl_weakzd2oldzd2hashtablezd2updatez12zc0zz__weakhashz00
						(BgL_tablez00_100, BgL_keyz00_101, BgL_procz00_102, BgL_objz00_103);
				}
		}

	}



/* &weak-hashtable-update! */
	obj_t BGl_z62weakzd2hashtablezd2updatez12z70zz__weakhashz00(obj_t
		BgL_envz00_4179, obj_t BgL_tablez00_4180, obj_t BgL_keyz00_4181,
		obj_t BgL_procz00_4182, obj_t BgL_objz00_4183)
	{
		{	/* Llib/weakhash.scm 598 */
			{	/* Llib/weakhash.scm 599 */
				obj_t BgL_auxz00_5965;
				obj_t BgL_auxz00_5958;

				if (PROCEDUREP(BgL_procz00_4182))
					{	/* Llib/weakhash.scm 599 */
						BgL_auxz00_5965 = BgL_procz00_4182;
					}
				else
					{
						obj_t BgL_auxz00_5968;

						BgL_auxz00_5968 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2161z00zz__weakhashz00,
							BINT(23717L), BGl_string2178z00zz__weakhashz00,
							BGl_string2167z00zz__weakhashz00, BgL_procz00_4182);
						FAILURE(BgL_auxz00_5968, BFALSE, BFALSE);
					}
				if (STRUCTP(BgL_tablez00_4180))
					{	/* Llib/weakhash.scm 599 */
						BgL_auxz00_5958 = BgL_tablez00_4180;
					}
				else
					{
						obj_t BgL_auxz00_5961;

						BgL_auxz00_5961 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2161z00zz__weakhashz00,
							BINT(23717L), BGl_string2178z00zz__weakhashz00,
							BGl_string2163z00zz__weakhashz00, BgL_tablez00_4180);
						FAILURE(BgL_auxz00_5961, BFALSE, BFALSE);
					}
				return
					BGl_weakzd2hashtablezd2updatez12z12zz__weakhashz00(BgL_auxz00_5958,
					BgL_keyz00_4181, BgL_auxz00_5965, BgL_objz00_4183);
			}
		}

	}



/* weak-keys-hashtable-add! */
	obj_t BGl_weakzd2keyszd2hashtablezd2addz12zc0zz__weakhashz00(obj_t
		BgL_tablez00_104, obj_t BgL_keyz00_105, obj_t BgL_procz00_106,
		obj_t BgL_objz00_107, obj_t BgL_initz00_108)
	{
		{	/* Llib/weakhash.scm 606 */
			{	/* Llib/weakhash.scm 607 */
				obj_t BgL_bucketsz00_1760;

				BgL_bucketsz00_1760 = STRUCT_REF(BgL_tablez00_104, (int) (2L));
				{	/* Llib/weakhash.scm 608 */
					long BgL_bucketzd2numzd2_1762;

					{	/* Llib/weakhash.scm 609 */
						long BgL_arg1625z00_1795;

						{	/* Llib/weakhash.scm 609 */
							obj_t BgL_hashnz00_3332;

							BgL_hashnz00_3332 = STRUCT_REF(BgL_tablez00_104, (int) (4L));
							if (PROCEDUREP(BgL_hashnz00_3332))
								{	/* Llib/weakhash.scm 609 */
									obj_t BgL_arg1268z00_3334;

									BgL_arg1268z00_3334 =
										BGL_PROCEDURE_CALL1(BgL_hashnz00_3332, BgL_keyz00_105);
									{	/* Llib/weakhash.scm 609 */
										long BgL_nz00_3336;

										BgL_nz00_3336 = (long) CINT(BgL_arg1268z00_3334);
										if ((BgL_nz00_3336 < 0L))
											{	/* Llib/weakhash.scm 609 */
												BgL_arg1625z00_1795 = NEG(BgL_nz00_3336);
											}
										else
											{	/* Llib/weakhash.scm 609 */
												BgL_arg1625z00_1795 = BgL_nz00_3336;
											}
									}
								}
							else
								{	/* Llib/weakhash.scm 609 */
									if ((BgL_hashnz00_3332 == BGl_symbol2173z00zz__weakhashz00))
										{	/* Llib/weakhash.scm 609 */
											BgL_arg1625z00_1795 =
												BGl_getzd2hashnumberzd2persistentz00zz__hashz00
												(BgL_keyz00_105);
										}
									else
										{	/* Llib/weakhash.scm 609 */
											BgL_arg1625z00_1795 =
												BGl_getzd2hashnumberzd2zz__hashz00(BgL_keyz00_105);
										}
								}
						}
						{	/* Llib/weakhash.scm 609 */
							long BgL_n1z00_3340;
							long BgL_n2z00_3341;

							BgL_n1z00_3340 = BgL_arg1625z00_1795;
							BgL_n2z00_3341 = VECTOR_LENGTH(((obj_t) BgL_bucketsz00_1760));
							{	/* Llib/weakhash.scm 609 */
								bool_t BgL_test2419z00_5993;

								{	/* Llib/weakhash.scm 609 */
									long BgL_arg1961z00_3343;

									BgL_arg1961z00_3343 =
										(((BgL_n1z00_3340) | (BgL_n2z00_3341)) & -2147483648);
									BgL_test2419z00_5993 = (BgL_arg1961z00_3343 == 0L);
								}
								if (BgL_test2419z00_5993)
									{	/* Llib/weakhash.scm 609 */
										int32_t BgL_arg1958z00_3344;

										{	/* Llib/weakhash.scm 609 */
											int32_t BgL_arg1959z00_3345;
											int32_t BgL_arg1960z00_3346;

											BgL_arg1959z00_3345 = (int32_t) (BgL_n1z00_3340);
											BgL_arg1960z00_3346 = (int32_t) (BgL_n2z00_3341);
											BgL_arg1958z00_3344 =
												(BgL_arg1959z00_3345 % BgL_arg1960z00_3346);
										}
										{	/* Llib/weakhash.scm 609 */
											long BgL_arg2069z00_3351;

											BgL_arg2069z00_3351 = (long) (BgL_arg1958z00_3344);
											BgL_bucketzd2numzd2_1762 = (long) (BgL_arg2069z00_3351);
									}}
								else
									{	/* Llib/weakhash.scm 609 */
										BgL_bucketzd2numzd2_1762 =
											(BgL_n1z00_3340 % BgL_n2z00_3341);
									}
							}
						}
					}
					{	/* Llib/weakhash.scm 609 */
						obj_t BgL_bucketz00_1763;

						BgL_bucketz00_1763 =
							VECTOR_REF(
							((obj_t) BgL_bucketsz00_1760), BgL_bucketzd2numzd2_1762);
						{	/* Llib/weakhash.scm 610 */
							obj_t BgL_maxzd2bucketzd2lenz00_1764;

							BgL_maxzd2bucketzd2lenz00_1764 =
								STRUCT_REF(BgL_tablez00_104, (int) (1L));
							{	/* Llib/weakhash.scm 611 */

								if (NULLP(BgL_bucketz00_1763))
									{	/* Llib/weakhash.scm 613 */
										obj_t BgL_vz00_1766;

										BgL_vz00_1766 =
											BGL_PROCEDURE_CALL2(BgL_procz00_106, BgL_objz00_107,
											BgL_initz00_108);
										{	/* Llib/weakhash.scm 614 */
											long BgL_arg1601z00_1767;

											BgL_arg1601z00_1767 =
												(
												(long) CINT(STRUCT_REF(BgL_tablez00_104,
														(int) (0L))) + 1L);
											{	/* Llib/weakhash.scm 614 */
												obj_t BgL_auxz00_6019;
												int BgL_tmpz00_6017;

												BgL_auxz00_6019 = BINT(BgL_arg1601z00_1767);
												BgL_tmpz00_6017 = (int) (0L);
												STRUCT_SET(BgL_tablez00_104, BgL_tmpz00_6017,
													BgL_auxz00_6019);
										}}
										{	/* Llib/weakhash.scm 615 */
											obj_t BgL_arg1603z00_1769;

											{	/* Llib/weakhash.scm 615 */
												obj_t BgL_arg1605z00_1770;

												BgL_arg1605z00_1770 =
													MAKE_YOUNG_PAIR(BgL_keyz00_105, BgL_vz00_1766);
												{	/* Llib/weakhash.scm 615 */
													obj_t BgL_list1606z00_1771;

													BgL_list1606z00_1771 =
														MAKE_YOUNG_PAIR(BgL_arg1605z00_1770, BNIL);
													BgL_arg1603z00_1769 = BgL_list1606z00_1771;
											}}
											VECTOR_SET(
												((obj_t) BgL_bucketsz00_1760), BgL_bucketzd2numzd2_1762,
												BgL_arg1603z00_1769);
										}
										return BgL_vz00_1766;
									}
								else
									{
										obj_t BgL_buckz00_1773;
										long BgL_countz00_1774;

										BgL_buckz00_1773 = BgL_bucketz00_1763;
										BgL_countz00_1774 = 0L;
									BgL_zc3z04anonymousza31607ze3z87_1775:
										if (NULLP(BgL_buckz00_1773))
											{	/* Llib/weakhash.scm 621 */
												obj_t BgL_vz00_1777;

												BgL_vz00_1777 =
													BGL_PROCEDURE_CALL2(BgL_procz00_106, BgL_objz00_107,
													BgL_initz00_108);
												{	/* Llib/weakhash.scm 622 */
													long BgL_arg1609z00_1778;

													BgL_arg1609z00_1778 =
														(
														(long) CINT(STRUCT_REF(BgL_tablez00_104,
																(int) (0L))) + 1L);
													{	/* Llib/weakhash.scm 622 */
														obj_t BgL_auxz00_6039;
														int BgL_tmpz00_6037;

														BgL_auxz00_6039 = BINT(BgL_arg1609z00_1778);
														BgL_tmpz00_6037 = (int) (0L);
														STRUCT_SET(BgL_tablez00_104, BgL_tmpz00_6037,
															BgL_auxz00_6039);
												}}
												{	/* Llib/weakhash.scm 624 */
													obj_t BgL_arg1611z00_1780;

													BgL_arg1611z00_1780 =
														MAKE_YOUNG_PAIR(bgl_make_weakptr(BgL_keyz00_105,
															BgL_vz00_1777), BgL_bucketz00_1763);
													VECTOR_SET(((obj_t) BgL_bucketsz00_1760),
														BgL_bucketzd2numzd2_1762, BgL_arg1611z00_1780);
												}
												if (
													(BgL_countz00_1774 >
														(long) CINT(BgL_maxzd2bucketzd2lenz00_1764)))
													{	/* Llib/weakhash.scm 625 */
														BGl_weakzd2keyszd2hashtablezd2expandz12zc0zz__weakhashz00
															(BgL_tablez00_104);
													}
												else
													{	/* Llib/weakhash.scm 625 */
														BFALSE;
													}
												return BgL_vz00_1777;
											}
										else
											{	/* Llib/weakhash.scm 628 */
												bool_t BgL_test2423z00_6050;

												{	/* Llib/weakhash.scm 628 */
													obj_t BgL_arg1623z00_1792;

													{	/* Llib/weakhash.scm 628 */
														obj_t BgL_arg1624z00_1793;

														BgL_arg1624z00_1793 =
															CAR(((obj_t) BgL_buckz00_1773));
														BgL_arg1623z00_1792 =
															bgl_weakptr_data(((obj_t) BgL_arg1624z00_1793));
													}
													{	/* Llib/weakhash.scm 628 */
														obj_t BgL_eqtz00_3371;

														BgL_eqtz00_3371 =
															STRUCT_REF(BgL_tablez00_104, (int) (3L));
														if (PROCEDUREP(BgL_eqtz00_3371))
															{	/* Llib/weakhash.scm 628 */
																BgL_test2423z00_6050 =
																	CBOOL(BGL_PROCEDURE_CALL2(BgL_eqtz00_3371,
																		BgL_arg1623z00_1792, BgL_keyz00_105));
															}
														else
															{	/* Llib/weakhash.scm 628 */
																if ((BgL_arg1623z00_1792 == BgL_keyz00_105))
																	{	/* Llib/weakhash.scm 628 */
																		BgL_test2423z00_6050 = ((bool_t) 1);
																	}
																else
																	{	/* Llib/weakhash.scm 628 */
																		if (STRINGP(BgL_arg1623z00_1792))
																			{	/* Llib/weakhash.scm 628 */
																				if (STRINGP(BgL_keyz00_105))
																					{	/* Llib/weakhash.scm 628 */
																						long BgL_l1z00_3378;

																						BgL_l1z00_3378 =
																							STRING_LENGTH
																							(BgL_arg1623z00_1792);
																						if ((BgL_l1z00_3378 ==
																								STRING_LENGTH(BgL_keyz00_105)))
																							{	/* Llib/weakhash.scm 628 */
																								int BgL_arg1844z00_3381;

																								{	/* Llib/weakhash.scm 628 */
																									char *BgL_auxz00_6077;
																									char *BgL_tmpz00_6075;

																									BgL_auxz00_6077 =
																										BSTRING_TO_STRING
																										(BgL_keyz00_105);
																									BgL_tmpz00_6075 =
																										BSTRING_TO_STRING
																										(BgL_arg1623z00_1792);
																									BgL_arg1844z00_3381 =
																										memcmp(BgL_tmpz00_6075,
																										BgL_auxz00_6077,
																										BgL_l1z00_3378);
																								}
																								BgL_test2423z00_6050 =
																									(
																									(long) (BgL_arg1844z00_3381)
																									== 0L);
																							}
																						else
																							{	/* Llib/weakhash.scm 628 */
																								BgL_test2423z00_6050 =
																									((bool_t) 0);
																							}
																					}
																				else
																					{	/* Llib/weakhash.scm 628 */
																						BgL_test2423z00_6050 = ((bool_t) 0);
																					}
																			}
																		else
																			{	/* Llib/weakhash.scm 628 */
																				BgL_test2423z00_6050 = ((bool_t) 0);
																			}
																	}
															}
													}
												}
												if (BgL_test2423z00_6050)
													{	/* Llib/weakhash.scm 629 */
														obj_t BgL_resz00_1786;

														{	/* Llib/weakhash.scm 629 */
															obj_t BgL_arg1619z00_1788;

															{	/* Llib/weakhash.scm 629 */
																obj_t BgL_arg1620z00_1789;

																BgL_arg1620z00_1789 =
																	CAR(((obj_t) BgL_buckz00_1773));
																BgL_arg1619z00_1788 =
																	bgl_weakptr_ref(
																	((obj_t) BgL_arg1620z00_1789));
															}
															BgL_resz00_1786 =
																BGL_PROCEDURE_CALL2(BgL_procz00_106,
																BgL_objz00_107, BgL_arg1619z00_1788);
														}
														{	/* Llib/weakhash.scm 630 */
															obj_t BgL_arg1618z00_1787;

															BgL_arg1618z00_1787 =
																CAR(((obj_t) BgL_buckz00_1773));
															bgl_weakptr_ref_set(
																((obj_t) BgL_arg1618z00_1787), BgL_resz00_1786);
															BUNSPEC;
															BUNSPEC;
														}
														return BgL_resz00_1786;
													}
												else
													{	/* Llib/weakhash.scm 633 */
														obj_t BgL_arg1621z00_1790;
														long BgL_arg1622z00_1791;

														BgL_arg1621z00_1790 =
															CDR(((obj_t) BgL_buckz00_1773));
														BgL_arg1622z00_1791 = (BgL_countz00_1774 + 1L);
														{
															long BgL_countz00_6099;
															obj_t BgL_buckz00_6098;

															BgL_buckz00_6098 = BgL_arg1621z00_1790;
															BgL_countz00_6099 = BgL_arg1622z00_1791;
															BgL_countz00_1774 = BgL_countz00_6099;
															BgL_buckz00_1773 = BgL_buckz00_6098;
															goto BgL_zc3z04anonymousza31607ze3z87_1775;
														}
													}
											}
									}
							}
						}
					}
				}
			}
		}

	}



/* weak-old-hashtable-add! */
	obj_t BGl_weakzd2oldzd2hashtablezd2addz12zc0zz__weakhashz00(obj_t
		BgL_tablez00_109, obj_t BgL_keyz00_110, obj_t BgL_procz00_111,
		obj_t BgL_objz00_112, obj_t BgL_initz00_113)
	{
		{	/* Llib/weakhash.scm 638 */
			{	/* Llib/weakhash.scm 639 */
				obj_t BgL_bucketsz00_1796;

				BgL_bucketsz00_1796 = STRUCT_REF(BgL_tablez00_109, (int) (2L));
				{	/* Llib/weakhash.scm 640 */
					long BgL_bucketzd2numzd2_1798;

					{	/* Llib/weakhash.scm 641 */
						long BgL_arg1645z00_1835;

						{	/* Llib/weakhash.scm 641 */
							obj_t BgL_hashnz00_3395;

							BgL_hashnz00_3395 = STRUCT_REF(BgL_tablez00_109, (int) (4L));
							if (PROCEDUREP(BgL_hashnz00_3395))
								{	/* Llib/weakhash.scm 641 */
									obj_t BgL_arg1268z00_3397;

									BgL_arg1268z00_3397 =
										BGL_PROCEDURE_CALL1(BgL_hashnz00_3395, BgL_keyz00_110);
									{	/* Llib/weakhash.scm 641 */
										long BgL_nz00_3399;

										BgL_nz00_3399 = (long) CINT(BgL_arg1268z00_3397);
										if ((BgL_nz00_3399 < 0L))
											{	/* Llib/weakhash.scm 641 */
												BgL_arg1645z00_1835 = NEG(BgL_nz00_3399);
											}
										else
											{	/* Llib/weakhash.scm 641 */
												BgL_arg1645z00_1835 = BgL_nz00_3399;
											}
									}
								}
							else
								{	/* Llib/weakhash.scm 641 */
									if ((BgL_hashnz00_3395 == BGl_symbol2173z00zz__weakhashz00))
										{	/* Llib/weakhash.scm 641 */
											BgL_arg1645z00_1835 =
												BGl_getzd2hashnumberzd2persistentz00zz__hashz00
												(BgL_keyz00_110);
										}
									else
										{	/* Llib/weakhash.scm 641 */
											BgL_arg1645z00_1835 =
												BGl_getzd2hashnumberzd2zz__hashz00(BgL_keyz00_110);
										}
								}
						}
						{	/* Llib/weakhash.scm 641 */
							long BgL_n1z00_3403;
							long BgL_n2z00_3404;

							BgL_n1z00_3403 = BgL_arg1645z00_1835;
							BgL_n2z00_3404 = VECTOR_LENGTH(((obj_t) BgL_bucketsz00_1796));
							{	/* Llib/weakhash.scm 641 */
								bool_t BgL_test2432z00_6120;

								{	/* Llib/weakhash.scm 641 */
									long BgL_arg1961z00_3406;

									BgL_arg1961z00_3406 =
										(((BgL_n1z00_3403) | (BgL_n2z00_3404)) & -2147483648);
									BgL_test2432z00_6120 = (BgL_arg1961z00_3406 == 0L);
								}
								if (BgL_test2432z00_6120)
									{	/* Llib/weakhash.scm 641 */
										int32_t BgL_arg1958z00_3407;

										{	/* Llib/weakhash.scm 641 */
											int32_t BgL_arg1959z00_3408;
											int32_t BgL_arg1960z00_3409;

											BgL_arg1959z00_3408 = (int32_t) (BgL_n1z00_3403);
											BgL_arg1960z00_3409 = (int32_t) (BgL_n2z00_3404);
											BgL_arg1958z00_3407 =
												(BgL_arg1959z00_3408 % BgL_arg1960z00_3409);
										}
										{	/* Llib/weakhash.scm 641 */
											long BgL_arg2069z00_3414;

											BgL_arg2069z00_3414 = (long) (BgL_arg1958z00_3407);
											BgL_bucketzd2numzd2_1798 = (long) (BgL_arg2069z00_3414);
									}}
								else
									{	/* Llib/weakhash.scm 641 */
										BgL_bucketzd2numzd2_1798 =
											(BgL_n1z00_3403 % BgL_n2z00_3404);
									}
							}
						}
					}
					{	/* Llib/weakhash.scm 641 */
						obj_t BgL_bucketz00_1799;

						BgL_bucketz00_1799 =
							VECTOR_REF(
							((obj_t) BgL_bucketsz00_1796), BgL_bucketzd2numzd2_1798);
						{	/* Llib/weakhash.scm 642 */
							obj_t BgL_maxzd2bucketzd2lenz00_1800;

							BgL_maxzd2bucketzd2lenz00_1800 =
								STRUCT_REF(BgL_tablez00_109, (int) (1L));
							{	/* Llib/weakhash.scm 643 */
								obj_t BgL_countz00_4194;

								BgL_countz00_4194 = MAKE_CELL(BINT(0L));
								{	/* Llib/weakhash.scm 644 */
									obj_t BgL_foundz00_1802;

									{	/* Llib/weakhash.scm 649 */
										obj_t BgL_zc3z04anonymousza31640ze3z87_4184;

										{
											int BgL_tmpz00_6134;

											BgL_tmpz00_6134 = (int) (4L);
											BgL_zc3z04anonymousza31640ze3z87_4184 =
												MAKE_L_PROCEDURE
												(BGl_z62zc3z04anonymousza31640ze3ze5zz__weakhashz00,
												BgL_tmpz00_6134);
										}
										PROCEDURE_L_SET(BgL_zc3z04anonymousza31640ze3z87_4184,
											(int) (0L), ((obj_t) BgL_countz00_4194));
										PROCEDURE_L_SET(BgL_zc3z04anonymousza31640ze3z87_4184,
											(int) (1L), BgL_procz00_111);
										PROCEDURE_L_SET(BgL_zc3z04anonymousza31640ze3z87_4184,
											(int) (2L), BgL_tablez00_109);
										PROCEDURE_L_SET(BgL_zc3z04anonymousza31640ze3z87_4184,
											(int) (3L), BgL_keyz00_110);
										BgL_foundz00_1802 =
											BGl_traversezd2bucketszd2zz__weakhashz00(BgL_tablez00_109,
											BgL_bucketsz00_1796, BgL_bucketzd2numzd2_1798,
											BgL_zc3z04anonymousza31640ze3z87_4184);
									}
									{	/* Llib/weakhash.scm 646 */

										if ((BgL_foundz00_1802 == BGl_keepgoingz00zz__weakhashz00))
											{	/* Llib/weakhash.scm 662 */
												obj_t BgL_vz00_1803;

												if (BGl_hashtablezd2weakzd2datazf3zf3zz__hashz00
													(BgL_tablez00_109))
													{	/* Llib/weakhash.scm 663 */
														obj_t BgL_arg1637z00_1816;
														obj_t BgL_arg1638z00_1817;

														{	/* Llib/weakhash.scm 663 */

															BgL_arg1637z00_1816 =
																bgl_make_weakptr(BgL_objz00_112, BFALSE);
														}
														{	/* Llib/weakhash.scm 663 */

															BgL_arg1638z00_1817 =
																bgl_make_weakptr(BgL_initz00_113, BFALSE);
														}
														BgL_vz00_1803 =
															BGL_PROCEDURE_CALL2(BgL_procz00_111,
															BgL_arg1637z00_1816, BgL_arg1638z00_1817);
													}
												else
													{	/* Llib/weakhash.scm 662 */
														BgL_vz00_1803 =
															BGL_PROCEDURE_CALL2(BgL_procz00_111,
															BgL_objz00_112, BgL_initz00_113);
													}
												{	/* Llib/weakhash.scm 665 */
													long BgL_arg1626z00_1804;

													BgL_arg1626z00_1804 =
														(
														(long) CINT(STRUCT_REF(BgL_tablez00_109,
																(int) (0L))) + 1L);
													{	/* Llib/weakhash.scm 665 */
														obj_t BgL_auxz00_6169;
														int BgL_tmpz00_6167;

														BgL_auxz00_6169 = BINT(BgL_arg1626z00_1804);
														BgL_tmpz00_6167 = (int) (0L);
														STRUCT_SET(BgL_tablez00_109, BgL_tmpz00_6167,
															BgL_auxz00_6169);
												}}
												{	/* Llib/weakhash.scm 667 */
													obj_t BgL_arg1628z00_1806;

													{	/* Llib/weakhash.scm 667 */
														obj_t BgL_arg1629z00_1807;
														obj_t BgL_arg1630z00_1808;

														{	/* Llib/weakhash.scm 667 */
															obj_t BgL_arg1631z00_1809;

															if (BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00
																(BgL_tablez00_109))
																{	/* Llib/weakhash.scm 668 */

																	BgL_arg1631z00_1809 =
																		bgl_make_weakptr(BgL_keyz00_110, BFALSE);
																}
															else
																{	/* Llib/weakhash.scm 667 */
																	BgL_arg1631z00_1809 = BgL_keyz00_110;
																}
															BgL_arg1629z00_1807 =
																MAKE_YOUNG_PAIR(BgL_arg1631z00_1809,
																BgL_vz00_1803);
														}
														{	/* Llib/weakhash.scm 673 */
															obj_t BgL_arg1634z00_1813;

															BgL_arg1634z00_1813 =
																STRUCT_REF(BgL_tablez00_109, (int) (2L));
															BgL_arg1630z00_1808 =
																VECTOR_REF(
																((obj_t) BgL_arg1634z00_1813),
																BgL_bucketzd2numzd2_1798);
														}
														BgL_arg1628z00_1806 =
															MAKE_YOUNG_PAIR(BgL_arg1629z00_1807,
															BgL_arg1630z00_1808);
													}
													VECTOR_SET(
														((obj_t) BgL_bucketsz00_1796),
														BgL_bucketzd2numzd2_1798, BgL_arg1628z00_1806);
												}
												if (
													((long) CINT(CELL_REF(BgL_countz00_4194)) >
														(long) CINT(BgL_maxzd2bucketzd2lenz00_1800)))
													{	/* Llib/weakhash.scm 675 */
														if (BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00
															(BgL_tablez00_109))
															{	/* Llib/weakhash.scm 877 */
																BGl_weakzd2keyszd2hashtablezd2expandz12zc0zz__weakhashz00
																	(BgL_tablez00_109);
															}
														else
															{	/* Llib/weakhash.scm 877 */
																BGl_weakzd2oldzd2hashtablezd2expandz12zc0zz__weakhashz00
																	(BgL_tablez00_109);
															}
													}
												else
													{	/* Llib/weakhash.scm 675 */
														BFALSE;
													}
												return BgL_vz00_1803;
											}
										else
											{	/* Llib/weakhash.scm 660 */
												return BgL_foundz00_1802;
											}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &<@anonymous:1640> */
	obj_t BGl_z62zc3z04anonymousza31640ze3ze5zz__weakhashz00(obj_t
		BgL_envz00_4185, obj_t BgL_bkeyz00_4190, obj_t BgL_valz00_4191,
		obj_t BgL_bucketz00_4192)
	{
		{	/* Llib/weakhash.scm 648 */
			{	/* Llib/weakhash.scm 649 */
				obj_t BgL_countz00_4186;
				obj_t BgL_procz00_4187;
				obj_t BgL_tablez00_4188;
				obj_t BgL_keyz00_4189;

				BgL_countz00_4186 = PROCEDURE_L_REF(BgL_envz00_4185, (int) (0L));
				BgL_procz00_4187 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_4185, (int) (1L)));
				BgL_tablez00_4188 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_4185, (int) (2L)));
				BgL_keyz00_4189 = PROCEDURE_L_REF(BgL_envz00_4185, (int) (3L));
				{	/* Llib/weakhash.scm 649 */
					obj_t BgL_auxz00_4289;

					BgL_auxz00_4289 = ADDFX(CELL_REF(BgL_countz00_4186), BINT(1L));
					CELL_SET(BgL_countz00_4186, BgL_auxz00_4289);
				}
				{	/* Llib/weakhash.scm 650 */
					bool_t BgL_test2438z00_6203;

					{	/* Llib/weakhash.scm 650 */
						obj_t BgL_eqtz00_4290;

						BgL_eqtz00_4290 = STRUCT_REF(BgL_tablez00_4188, (int) (3L));
						if (PROCEDUREP(BgL_eqtz00_4290))
							{	/* Llib/weakhash.scm 650 */
								BgL_test2438z00_6203 =
									CBOOL(BGL_PROCEDURE_CALL2(BgL_eqtz00_4290, BgL_bkeyz00_4190,
										BgL_keyz00_4189));
							}
						else
							{	/* Llib/weakhash.scm 650 */
								if ((BgL_bkeyz00_4190 == BgL_keyz00_4189))
									{	/* Llib/weakhash.scm 650 */
										BgL_test2438z00_6203 = ((bool_t) 1);
									}
								else
									{	/* Llib/weakhash.scm 650 */
										if (STRINGP(BgL_bkeyz00_4190))
											{	/* Llib/weakhash.scm 650 */
												if (STRINGP(BgL_keyz00_4189))
													{	/* Llib/weakhash.scm 650 */
														long BgL_l1z00_4291;

														BgL_l1z00_4291 = STRING_LENGTH(BgL_bkeyz00_4190);
														if (
															(BgL_l1z00_4291 ==
																STRING_LENGTH(BgL_keyz00_4189)))
															{	/* Llib/weakhash.scm 650 */
																int BgL_arg1844z00_4292;

																{	/* Llib/weakhash.scm 650 */
																	char *BgL_auxz00_6226;
																	char *BgL_tmpz00_6224;

																	BgL_auxz00_6226 =
																		BSTRING_TO_STRING(BgL_keyz00_4189);
																	BgL_tmpz00_6224 =
																		BSTRING_TO_STRING(BgL_bkeyz00_4190);
																	BgL_arg1844z00_4292 =
																		memcmp(BgL_tmpz00_6224, BgL_auxz00_6226,
																		BgL_l1z00_4291);
																}
																BgL_test2438z00_6203 =
																	((long) (BgL_arg1844z00_4292) == 0L);
															}
														else
															{	/* Llib/weakhash.scm 650 */
																BgL_test2438z00_6203 = ((bool_t) 0);
															}
													}
												else
													{	/* Llib/weakhash.scm 650 */
														BgL_test2438z00_6203 = ((bool_t) 0);
													}
											}
										else
											{	/* Llib/weakhash.scm 650 */
												BgL_test2438z00_6203 = ((bool_t) 0);
											}
									}
							}
					}
					if (BgL_test2438z00_6203)
						{	/* Llib/weakhash.scm 651 */
							obj_t BgL_newvalz00_4293;

							BgL_newvalz00_4293 =
								BGL_PROCEDURE_CALL1(BgL_procz00_4187, BgL_valz00_4191);
							{	/* Llib/weakhash.scm 652 */
								obj_t BgL_arg1642z00_4294;
								obj_t BgL_arg1643z00_4295;

								BgL_arg1642z00_4294 = CAR(((obj_t) BgL_bucketz00_4192));
								if (BGl_hashtablezd2weakzd2datazf3zf3zz__hashz00
									(BgL_tablez00_4188))
									{	/* Llib/weakhash.scm 654 */

										BgL_arg1643z00_4295 =
											bgl_make_weakptr(BgL_newvalz00_4293, BFALSE);
									}
								else
									{	/* Llib/weakhash.scm 653 */
										BgL_arg1643z00_4295 = BgL_newvalz00_4293;
									}
								{	/* Llib/weakhash.scm 652 */
									obj_t BgL_tmpz00_6240;

									BgL_tmpz00_6240 = ((obj_t) BgL_arg1642z00_4294);
									SET_CDR(BgL_tmpz00_6240, BgL_arg1643z00_4295);
								}
							}
							return BgL_newvalz00_4293;
						}
					else
						{	/* Llib/weakhash.scm 650 */
							return BGl_keepgoingz00zz__weakhashz00;
						}
				}
			}
		}

	}



/* weak-hashtable-add! */
	BGL_EXPORTED_DEF obj_t BGl_weakzd2hashtablezd2addz12z12zz__weakhashz00(obj_t
		BgL_tablez00_114, obj_t BgL_keyz00_115, obj_t BgL_procz00_116,
		obj_t BgL_objz00_117, obj_t BgL_initz00_118)
	{
		{	/* Llib/weakhash.scm 682 */
			if (BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(BgL_tablez00_114))
				{	/* Llib/weakhash.scm 683 */
					BGL_TAIL return
						BGl_weakzd2keyszd2hashtablezd2addz12zc0zz__weakhashz00
						(BgL_tablez00_114, BgL_keyz00_115, BgL_procz00_116, BgL_objz00_117,
						BgL_initz00_118);
				}
			else
				{	/* Llib/weakhash.scm 683 */
					BGL_TAIL return
						BGl_weakzd2oldzd2hashtablezd2addz12zc0zz__weakhashz00
						(BgL_tablez00_114, BgL_keyz00_115, BgL_procz00_116, BgL_objz00_117,
						BgL_initz00_118);
				}
		}

	}



/* &weak-hashtable-add! */
	obj_t BGl_z62weakzd2hashtablezd2addz12z70zz__weakhashz00(obj_t
		BgL_envz00_4196, obj_t BgL_tablez00_4197, obj_t BgL_keyz00_4198,
		obj_t BgL_procz00_4199, obj_t BgL_objz00_4200, obj_t BgL_initz00_4201)
	{
		{	/* Llib/weakhash.scm 682 */
			{	/* Llib/weakhash.scm 683 */
				obj_t BgL_auxz00_6254;
				obj_t BgL_auxz00_6247;

				if (PROCEDUREP(BgL_procz00_4199))
					{	/* Llib/weakhash.scm 683 */
						BgL_auxz00_6254 = BgL_procz00_4199;
					}
				else
					{
						obj_t BgL_auxz00_6257;

						BgL_auxz00_6257 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2161z00zz__weakhashz00,
							BINT(27143L), BGl_string2179z00zz__weakhashz00,
							BGl_string2167z00zz__weakhashz00, BgL_procz00_4199);
						FAILURE(BgL_auxz00_6257, BFALSE, BFALSE);
					}
				if (STRUCTP(BgL_tablez00_4197))
					{	/* Llib/weakhash.scm 683 */
						BgL_auxz00_6247 = BgL_tablez00_4197;
					}
				else
					{
						obj_t BgL_auxz00_6250;

						BgL_auxz00_6250 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2161z00zz__weakhashz00,
							BINT(27143L), BGl_string2179z00zz__weakhashz00,
							BGl_string2163z00zz__weakhashz00, BgL_tablez00_4197);
						FAILURE(BgL_auxz00_6250, BFALSE, BFALSE);
					}
				return
					BGl_weakzd2hashtablezd2addz12z12zz__weakhashz00(BgL_auxz00_6247,
					BgL_keyz00_4198, BgL_auxz00_6254, BgL_objz00_4200, BgL_initz00_4201);
			}
		}

	}



/* weak-keys-hashtable-remove! */
	bool_t BGl_weakzd2keyszd2hashtablezd2removez12zc0zz__weakhashz00(obj_t
		BgL_tablez00_119, obj_t BgL_keyz00_120)
	{
		{	/* Llib/weakhash.scm 690 */
			{	/* Llib/weakhash.scm 691 */
				obj_t BgL_bucketsz00_1837;

				BgL_bucketsz00_1837 = STRUCT_REF(BgL_tablez00_119, (int) (2L));
				{	/* Llib/weakhash.scm 692 */
					long BgL_bucketzd2numzd2_1839;

					{	/* Llib/weakhash.scm 693 */
						long BgL_arg1676z00_1866;

						{	/* Llib/weakhash.scm 693 */
							obj_t BgL_hashnz00_3460;

							BgL_hashnz00_3460 = STRUCT_REF(BgL_tablez00_119, (int) (4L));
							if (PROCEDUREP(BgL_hashnz00_3460))
								{	/* Llib/weakhash.scm 693 */
									obj_t BgL_arg1268z00_3462;

									BgL_arg1268z00_3462 =
										BGL_PROCEDURE_CALL1(BgL_hashnz00_3460, BgL_keyz00_120);
									{	/* Llib/weakhash.scm 693 */
										long BgL_nz00_3464;

										BgL_nz00_3464 = (long) CINT(BgL_arg1268z00_3462);
										if ((BgL_nz00_3464 < 0L))
											{	/* Llib/weakhash.scm 693 */
												BgL_arg1676z00_1866 = NEG(BgL_nz00_3464);
											}
										else
											{	/* Llib/weakhash.scm 693 */
												BgL_arg1676z00_1866 = BgL_nz00_3464;
											}
									}
								}
							else
								{	/* Llib/weakhash.scm 693 */
									if ((BgL_hashnz00_3460 == BGl_symbol2173z00zz__weakhashz00))
										{	/* Llib/weakhash.scm 693 */
											BgL_arg1676z00_1866 =
												BGl_getzd2hashnumberzd2persistentz00zz__hashz00
												(BgL_keyz00_120);
										}
									else
										{	/* Llib/weakhash.scm 693 */
											BgL_arg1676z00_1866 =
												BGl_getzd2hashnumberzd2zz__hashz00(BgL_keyz00_120);
										}
								}
						}
						{	/* Llib/weakhash.scm 693 */
							long BgL_n1z00_3468;
							long BgL_n2z00_3469;

							BgL_n1z00_3468 = BgL_arg1676z00_1866;
							BgL_n2z00_3469 = VECTOR_LENGTH(((obj_t) BgL_bucketsz00_1837));
							{	/* Llib/weakhash.scm 693 */
								bool_t BgL_test2451z00_6282;

								{	/* Llib/weakhash.scm 693 */
									long BgL_arg1961z00_3471;

									BgL_arg1961z00_3471 =
										(((BgL_n1z00_3468) | (BgL_n2z00_3469)) & -2147483648);
									BgL_test2451z00_6282 = (BgL_arg1961z00_3471 == 0L);
								}
								if (BgL_test2451z00_6282)
									{	/* Llib/weakhash.scm 693 */
										int32_t BgL_arg1958z00_3472;

										{	/* Llib/weakhash.scm 693 */
											int32_t BgL_arg1959z00_3473;
											int32_t BgL_arg1960z00_3474;

											BgL_arg1959z00_3473 = (int32_t) (BgL_n1z00_3468);
											BgL_arg1960z00_3474 = (int32_t) (BgL_n2z00_3469);
											BgL_arg1958z00_3472 =
												(BgL_arg1959z00_3473 % BgL_arg1960z00_3474);
										}
										{	/* Llib/weakhash.scm 693 */
											long BgL_arg2069z00_3479;

											BgL_arg2069z00_3479 = (long) (BgL_arg1958z00_3472);
											BgL_bucketzd2numzd2_1839 = (long) (BgL_arg2069z00_3479);
									}}
								else
									{	/* Llib/weakhash.scm 693 */
										BgL_bucketzd2numzd2_1839 =
											(BgL_n1z00_3468 % BgL_n2z00_3469);
									}
							}
						}
					}
					{	/* Llib/weakhash.scm 693 */
						obj_t BgL_bucketz00_1840;

						BgL_bucketz00_1840 =
							VECTOR_REF(
							((obj_t) BgL_bucketsz00_1837), BgL_bucketzd2numzd2_1839);
						{	/* Llib/weakhash.scm 694 */

							if (NULLP(BgL_bucketz00_1840))
								{	/* Llib/weakhash.scm 696 */
									return ((bool_t) 0);
								}
							else
								{	/* Llib/weakhash.scm 698 */
									bool_t BgL_test2453z00_6295;

									{	/* Llib/weakhash.scm 698 */
										obj_t BgL_arg1670z00_1864;

										{	/* Llib/weakhash.scm 698 */
											obj_t BgL_arg1675z00_1865;

											BgL_arg1675z00_1865 = CAR(((obj_t) BgL_bucketz00_1840));
											BgL_arg1670z00_1864 =
												bgl_weakptr_data(((obj_t) BgL_arg1675z00_1865));
										}
										{	/* Llib/weakhash.scm 698 */
											obj_t BgL_eqtz00_3485;

											BgL_eqtz00_3485 =
												STRUCT_REF(BgL_tablez00_119, (int) (3L));
											if (PROCEDUREP(BgL_eqtz00_3485))
												{	/* Llib/weakhash.scm 698 */
													BgL_test2453z00_6295 =
														CBOOL(BGL_PROCEDURE_CALL2(BgL_eqtz00_3485,
															BgL_arg1670z00_1864, BgL_keyz00_120));
												}
											else
												{	/* Llib/weakhash.scm 698 */
													if ((BgL_arg1670z00_1864 == BgL_keyz00_120))
														{	/* Llib/weakhash.scm 698 */
															BgL_test2453z00_6295 = ((bool_t) 1);
														}
													else
														{	/* Llib/weakhash.scm 698 */
															if (STRINGP(BgL_arg1670z00_1864))
																{	/* Llib/weakhash.scm 698 */
																	if (STRINGP(BgL_keyz00_120))
																		{	/* Llib/weakhash.scm 698 */
																			long BgL_l1z00_3492;

																			BgL_l1z00_3492 =
																				STRING_LENGTH(BgL_arg1670z00_1864);
																			if (
																				(BgL_l1z00_3492 ==
																					STRING_LENGTH(BgL_keyz00_120)))
																				{	/* Llib/weakhash.scm 698 */
																					int BgL_arg1844z00_3495;

																					{	/* Llib/weakhash.scm 698 */
																						char *BgL_auxz00_6322;
																						char *BgL_tmpz00_6320;

																						BgL_auxz00_6322 =
																							BSTRING_TO_STRING(BgL_keyz00_120);
																						BgL_tmpz00_6320 =
																							BSTRING_TO_STRING
																							(BgL_arg1670z00_1864);
																						BgL_arg1844z00_3495 =
																							memcmp(BgL_tmpz00_6320,
																							BgL_auxz00_6322, BgL_l1z00_3492);
																					}
																					BgL_test2453z00_6295 =
																						(
																						(long) (BgL_arg1844z00_3495) == 0L);
																				}
																			else
																				{	/* Llib/weakhash.scm 698 */
																					BgL_test2453z00_6295 = ((bool_t) 0);
																				}
																		}
																	else
																		{	/* Llib/weakhash.scm 698 */
																			BgL_test2453z00_6295 = ((bool_t) 0);
																		}
																}
															else
																{	/* Llib/weakhash.scm 698 */
																	BgL_test2453z00_6295 = ((bool_t) 0);
																}
														}
												}
										}
									}
									if (BgL_test2453z00_6295)
										{	/* Llib/weakhash.scm 698 */
											{	/* Llib/weakhash.scm 699 */
												obj_t BgL_arg1651z00_1845;

												BgL_arg1651z00_1845 = CDR(((obj_t) BgL_bucketz00_1840));
												VECTOR_SET(
													((obj_t) BgL_bucketsz00_1837),
													BgL_bucketzd2numzd2_1839, BgL_arg1651z00_1845);
											}
											{	/* Llib/weakhash.scm 700 */
												long BgL_arg1652z00_1846;

												BgL_arg1652z00_1846 =
													(
													(long) CINT(STRUCT_REF(BgL_tablez00_119,
															(int) (0L))) - 1L);
												{	/* Llib/weakhash.scm 700 */
													obj_t BgL_auxz00_6337;
													int BgL_tmpz00_6335;

													BgL_auxz00_6337 = BINT(BgL_arg1652z00_1846);
													BgL_tmpz00_6335 = (int) (0L);
													STRUCT_SET(BgL_tablez00_119, BgL_tmpz00_6335,
														BgL_auxz00_6337);
											}}
											return ((bool_t) 1);
										}
									else
										{	/* Llib/weakhash.scm 703 */
											obj_t BgL_g1068z00_1848;

											BgL_g1068z00_1848 = CDR(((obj_t) BgL_bucketz00_1840));
											{
												obj_t BgL_bucketz00_1850;
												obj_t BgL_prevz00_1851;

												BgL_bucketz00_1850 = BgL_g1068z00_1848;
												BgL_prevz00_1851 = BgL_bucketz00_1840;
											BgL_zc3z04anonymousza31654ze3z87_1852:
												if (PAIRP(BgL_bucketz00_1850))
													{	/* Llib/weakhash.scm 706 */
														bool_t BgL_test2460z00_6344;

														{	/* Llib/weakhash.scm 706 */
															obj_t BgL_arg1668z00_1861;

															{	/* Llib/weakhash.scm 706 */
																obj_t BgL_arg1669z00_1862;

																BgL_arg1669z00_1862 = CAR(BgL_bucketz00_1850);
																BgL_arg1668z00_1861 =
																	bgl_weakptr_data(
																	((obj_t) BgL_arg1669z00_1862));
															}
															{	/* Llib/weakhash.scm 706 */
																obj_t BgL_eqtz00_3510;

																BgL_eqtz00_3510 =
																	STRUCT_REF(BgL_tablez00_119, (int) (3L));
																if (PROCEDUREP(BgL_eqtz00_3510))
																	{	/* Llib/weakhash.scm 706 */
																		BgL_test2460z00_6344 =
																			CBOOL(BGL_PROCEDURE_CALL2(BgL_eqtz00_3510,
																				BgL_arg1668z00_1861, BgL_keyz00_120));
																	}
																else
																	{	/* Llib/weakhash.scm 706 */
																		if ((BgL_arg1668z00_1861 == BgL_keyz00_120))
																			{	/* Llib/weakhash.scm 706 */
																				BgL_test2460z00_6344 = ((bool_t) 1);
																			}
																		else
																			{	/* Llib/weakhash.scm 706 */
																				if (STRINGP(BgL_arg1668z00_1861))
																					{	/* Llib/weakhash.scm 706 */
																						if (STRINGP(BgL_keyz00_120))
																							{	/* Llib/weakhash.scm 706 */
																								long BgL_l1z00_3517;

																								BgL_l1z00_3517 =
																									STRING_LENGTH
																									(BgL_arg1668z00_1861);
																								if ((BgL_l1z00_3517 ==
																										STRING_LENGTH
																										(BgL_keyz00_120)))
																									{	/* Llib/weakhash.scm 706 */
																										int BgL_arg1844z00_3520;

																										{	/* Llib/weakhash.scm 706 */
																											char *BgL_auxz00_6370;
																											char *BgL_tmpz00_6368;

																											BgL_auxz00_6370 =
																												BSTRING_TO_STRING
																												(BgL_keyz00_120);
																											BgL_tmpz00_6368 =
																												BSTRING_TO_STRING
																												(BgL_arg1668z00_1861);
																											BgL_arg1844z00_3520 =
																												memcmp(BgL_tmpz00_6368,
																												BgL_auxz00_6370,
																												BgL_l1z00_3517);
																										}
																										BgL_test2460z00_6344 =
																											(
																											(long)
																											(BgL_arg1844z00_3520) ==
																											0L);
																									}
																								else
																									{	/* Llib/weakhash.scm 706 */
																										BgL_test2460z00_6344 =
																											((bool_t) 0);
																									}
																							}
																						else
																							{	/* Llib/weakhash.scm 706 */
																								BgL_test2460z00_6344 =
																									((bool_t) 0);
																							}
																					}
																				else
																					{	/* Llib/weakhash.scm 706 */
																						BgL_test2460z00_6344 = ((bool_t) 0);
																					}
																			}
																	}
															}
														}
														if (BgL_test2460z00_6344)
															{	/* Llib/weakhash.scm 706 */
																{	/* Llib/weakhash.scm 708 */
																	obj_t BgL_arg1661z00_1857;

																	BgL_arg1661z00_1857 = CDR(BgL_bucketz00_1850);
																	{	/* Llib/weakhash.scm 708 */
																		obj_t BgL_tmpz00_6376;

																		BgL_tmpz00_6376 =
																			((obj_t) BgL_prevz00_1851);
																		SET_CDR(BgL_tmpz00_6376,
																			BgL_arg1661z00_1857);
																	}
																}
																{	/* Llib/weakhash.scm 710 */
																	long BgL_arg1663z00_1858;

																	BgL_arg1663z00_1858 =
																		(
																		(long) CINT(STRUCT_REF(BgL_tablez00_119,
																				(int) (0L))) - 1L);
																	{	/* Llib/weakhash.scm 709 */
																		obj_t BgL_auxz00_6385;
																		int BgL_tmpz00_6383;

																		BgL_auxz00_6385 = BINT(BgL_arg1663z00_1858);
																		BgL_tmpz00_6383 = (int) (0L);
																		STRUCT_SET(BgL_tablez00_119,
																			BgL_tmpz00_6383, BgL_auxz00_6385);
																}}
																return ((bool_t) 1);
															}
														else
															{
																obj_t BgL_prevz00_6390;
																obj_t BgL_bucketz00_6388;

																BgL_bucketz00_6388 = CDR(BgL_bucketz00_1850);
																BgL_prevz00_6390 = BgL_bucketz00_1850;
																BgL_prevz00_1851 = BgL_prevz00_6390;
																BgL_bucketz00_1850 = BgL_bucketz00_6388;
																goto BgL_zc3z04anonymousza31654ze3z87_1852;
															}
													}
												else
													{	/* Llib/weakhash.scm 705 */
														return ((bool_t) 0);
													}
											}
										}
								}
						}
					}
				}
			}
		}

	}



/* weak-old-hashtable-remove! */
	bool_t BGl_weakzd2oldzd2hashtablezd2removez12zc0zz__weakhashz00(obj_t
		BgL_tablez00_121, obj_t BgL_keyz00_122)
	{
		{	/* Llib/weakhash.scm 718 */
			{	/* Llib/weakhash.scm 719 */
				obj_t BgL_bucketsz00_1867;

				BgL_bucketsz00_1867 = STRUCT_REF(BgL_tablez00_121, (int) (2L));
				{	/* Llib/weakhash.scm 720 */
					long BgL_bucketzd2numzd2_1869;

					{	/* Llib/weakhash.scm 721 */
						long BgL_arg1681z00_1879;

						{	/* Llib/weakhash.scm 721 */
							obj_t BgL_hashnz00_3534;

							BgL_hashnz00_3534 = STRUCT_REF(BgL_tablez00_121, (int) (4L));
							if (PROCEDUREP(BgL_hashnz00_3534))
								{	/* Llib/weakhash.scm 721 */
									obj_t BgL_arg1268z00_3536;

									BgL_arg1268z00_3536 =
										BGL_PROCEDURE_CALL1(BgL_hashnz00_3534, BgL_keyz00_122);
									{	/* Llib/weakhash.scm 721 */
										long BgL_nz00_3538;

										BgL_nz00_3538 = (long) CINT(BgL_arg1268z00_3536);
										if ((BgL_nz00_3538 < 0L))
											{	/* Llib/weakhash.scm 721 */
												BgL_arg1681z00_1879 = NEG(BgL_nz00_3538);
											}
										else
											{	/* Llib/weakhash.scm 721 */
												BgL_arg1681z00_1879 = BgL_nz00_3538;
											}
									}
								}
							else
								{	/* Llib/weakhash.scm 721 */
									if ((BgL_hashnz00_3534 == BGl_symbol2173z00zz__weakhashz00))
										{	/* Llib/weakhash.scm 721 */
											BgL_arg1681z00_1879 =
												BGl_getzd2hashnumberzd2persistentz00zz__hashz00
												(BgL_keyz00_122);
										}
									else
										{	/* Llib/weakhash.scm 721 */
											BgL_arg1681z00_1879 =
												BGl_getzd2hashnumberzd2zz__hashz00(BgL_keyz00_122);
										}
								}
						}
						{	/* Llib/weakhash.scm 721 */
							long BgL_n1z00_3542;
							long BgL_n2z00_3543;

							BgL_n1z00_3542 = BgL_arg1681z00_1879;
							BgL_n2z00_3543 = VECTOR_LENGTH(((obj_t) BgL_bucketsz00_1867));
							{	/* Llib/weakhash.scm 721 */
								bool_t BgL_test2469z00_6411;

								{	/* Llib/weakhash.scm 721 */
									long BgL_arg1961z00_3545;

									BgL_arg1961z00_3545 =
										(((BgL_n1z00_3542) | (BgL_n2z00_3543)) & -2147483648);
									BgL_test2469z00_6411 = (BgL_arg1961z00_3545 == 0L);
								}
								if (BgL_test2469z00_6411)
									{	/* Llib/weakhash.scm 721 */
										int32_t BgL_arg1958z00_3546;

										{	/* Llib/weakhash.scm 721 */
											int32_t BgL_arg1959z00_3547;
											int32_t BgL_arg1960z00_3548;

											BgL_arg1959z00_3547 = (int32_t) (BgL_n1z00_3542);
											BgL_arg1960z00_3548 = (int32_t) (BgL_n2z00_3543);
											BgL_arg1958z00_3546 =
												(BgL_arg1959z00_3547 % BgL_arg1960z00_3548);
										}
										{	/* Llib/weakhash.scm 721 */
											long BgL_arg2069z00_3553;

											BgL_arg2069z00_3553 = (long) (BgL_arg1958z00_3546);
											BgL_bucketzd2numzd2_1869 = (long) (BgL_arg2069z00_3553);
									}}
								else
									{	/* Llib/weakhash.scm 721 */
										BgL_bucketzd2numzd2_1869 =
											(BgL_n1z00_3542 % BgL_n2z00_3543);
									}
							}
						}
					}
					{	/* Llib/weakhash.scm 721 */
						obj_t BgL_bucketz00_1870;

						BgL_bucketz00_1870 =
							VECTOR_REF(
							((obj_t) BgL_bucketsz00_1867), BgL_bucketzd2numzd2_1869);
						{	/* Llib/weakhash.scm 722 */
							obj_t BgL_foundz00_1871;

							{	/* Llib/weakhash.scm 726 */
								obj_t BgL_zc3z04anonymousza31679ze3z87_4202;

								{
									int BgL_tmpz00_6422;

									BgL_tmpz00_6422 = (int) (2L);
									BgL_zc3z04anonymousza31679ze3z87_4202 =
										MAKE_L_PROCEDURE
										(BGl_z62zc3z04anonymousza31679ze3ze5zz__weakhashz00,
										BgL_tmpz00_6422);
								}
								PROCEDURE_L_SET(BgL_zc3z04anonymousza31679ze3z87_4202,
									(int) (0L), BgL_tablez00_121);
								PROCEDURE_L_SET(BgL_zc3z04anonymousza31679ze3z87_4202,
									(int) (1L), BgL_keyz00_122);
								BgL_foundz00_1871 =
									BGl_traversezd2bucketszd2zz__weakhashz00(BgL_tablez00_121,
									BgL_bucketsz00_1867, BgL_bucketzd2numzd2_1869,
									BgL_zc3z04anonymousza31679ze3z87_4202);
							}
							{	/* Llib/weakhash.scm 723 */

								if ((BgL_foundz00_1871 == BGl_keepgoingz00zz__weakhashz00))
									{	/* Llib/weakhash.scm 729 */
										return ((bool_t) 0);
									}
								else
									{	/* Llib/weakhash.scm 729 */
										return ((bool_t) 1);
									}
							}
						}
					}
				}
			}
		}

	}



/* &<@anonymous:1679> */
	obj_t BGl_z62zc3z04anonymousza31679ze3ze5zz__weakhashz00(obj_t
		BgL_envz00_4203, obj_t BgL_bkeyz00_4206, obj_t BgL_valz00_4207,
		obj_t BgL_bucketz00_4208)
	{
		{	/* Llib/weakhash.scm 725 */
			{	/* Llib/weakhash.scm 726 */
				obj_t BgL_tablez00_4204;
				obj_t BgL_keyz00_4205;

				BgL_tablez00_4204 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_4203, (int) (0L)));
				BgL_keyz00_4205 = PROCEDURE_L_REF(BgL_envz00_4203, (int) (1L));
				{	/* Llib/weakhash.scm 726 */
					bool_t BgL_test2471z00_6437;

					{	/* Llib/weakhash.scm 726 */
						obj_t BgL_eqtz00_4296;

						BgL_eqtz00_4296 = STRUCT_REF(BgL_tablez00_4204, (int) (3L));
						if (PROCEDUREP(BgL_eqtz00_4296))
							{	/* Llib/weakhash.scm 726 */
								BgL_test2471z00_6437 =
									CBOOL(BGL_PROCEDURE_CALL2(BgL_eqtz00_4296, BgL_keyz00_4205,
										BgL_bkeyz00_4206));
							}
						else
							{	/* Llib/weakhash.scm 726 */
								if ((BgL_keyz00_4205 == BgL_bkeyz00_4206))
									{	/* Llib/weakhash.scm 726 */
										BgL_test2471z00_6437 = ((bool_t) 1);
									}
								else
									{	/* Llib/weakhash.scm 726 */
										if (STRINGP(BgL_keyz00_4205))
											{	/* Llib/weakhash.scm 726 */
												if (STRINGP(BgL_bkeyz00_4206))
													{	/* Llib/weakhash.scm 726 */
														long BgL_l1z00_4297;

														BgL_l1z00_4297 = STRING_LENGTH(BgL_keyz00_4205);
														if (
															(BgL_l1z00_4297 ==
																STRING_LENGTH(BgL_bkeyz00_4206)))
															{	/* Llib/weakhash.scm 726 */
																int BgL_arg1844z00_4298;

																{	/* Llib/weakhash.scm 726 */
																	char *BgL_auxz00_6460;
																	char *BgL_tmpz00_6458;

																	BgL_auxz00_6460 =
																		BSTRING_TO_STRING(BgL_bkeyz00_4206);
																	BgL_tmpz00_6458 =
																		BSTRING_TO_STRING(BgL_keyz00_4205);
																	BgL_arg1844z00_4298 =
																		memcmp(BgL_tmpz00_6458, BgL_auxz00_6460,
																		BgL_l1z00_4297);
																}
																BgL_test2471z00_6437 =
																	((long) (BgL_arg1844z00_4298) == 0L);
															}
														else
															{	/* Llib/weakhash.scm 726 */
																BgL_test2471z00_6437 = ((bool_t) 0);
															}
													}
												else
													{	/* Llib/weakhash.scm 726 */
														BgL_test2471z00_6437 = ((bool_t) 0);
													}
											}
										else
											{	/* Llib/weakhash.scm 726 */
												BgL_test2471z00_6437 = ((bool_t) 0);
											}
									}
							}
					}
					if (BgL_test2471z00_6437)
						{	/* Llib/weakhash.scm 726 */
							return BGl_removestopz00zz__weakhashz00;
						}
					else
						{	/* Llib/weakhash.scm 726 */
							return BGl_keepgoingz00zz__weakhashz00;
						}
				}
			}
		}

	}



/* weak-hashtable-remove! */
	BGL_EXPORTED_DEF obj_t
		BGl_weakzd2hashtablezd2removez12z12zz__weakhashz00(obj_t BgL_tablez00_123,
		obj_t BgL_keyz00_124)
	{
		{	/* Llib/weakhash.scm 734 */
			if (BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(BgL_tablez00_123))
				{	/* Llib/weakhash.scm 735 */
					return
						BBOOL(BGl_weakzd2keyszd2hashtablezd2removez12zc0zz__weakhashz00
						(BgL_tablez00_123, BgL_keyz00_124));
				}
			else
				{	/* Llib/weakhash.scm 735 */
					return
						BBOOL(BGl_weakzd2oldzd2hashtablezd2removez12zc0zz__weakhashz00
						(BgL_tablez00_123, BgL_keyz00_124));
				}
		}

	}



/* &weak-hashtable-remove! */
	obj_t BGl_z62weakzd2hashtablezd2removez12z70zz__weakhashz00(obj_t
		BgL_envz00_4209, obj_t BgL_tablez00_4210, obj_t BgL_keyz00_4211)
	{
		{	/* Llib/weakhash.scm 734 */
			{	/* Llib/weakhash.scm 735 */
				obj_t BgL_auxz00_6471;

				if (STRUCTP(BgL_tablez00_4210))
					{	/* Llib/weakhash.scm 735 */
						BgL_auxz00_6471 = BgL_tablez00_4210;
					}
				else
					{
						obj_t BgL_auxz00_6474;

						BgL_auxz00_6474 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2161z00zz__weakhashz00,
							BINT(29319L), BGl_string2180z00zz__weakhashz00,
							BGl_string2163z00zz__weakhashz00, BgL_tablez00_4210);
						FAILURE(BgL_auxz00_6474, BFALSE, BFALSE);
					}
				return
					BGl_weakzd2hashtablezd2removez12z12zz__weakhashz00(BgL_auxz00_6471,
					BgL_keyz00_4211);
			}
		}

	}



/* weak-keys-hashtable-expand! */
	obj_t BGl_weakzd2keyszd2hashtablezd2expandz12zc0zz__weakhashz00(obj_t
		BgL_tablez00_125)
	{
		{	/* Llib/weakhash.scm 742 */
			BGl_gcz00zz__biglooz00(BTRUE);
			{	/* Llib/weakhash.scm 746 */
				obj_t BgL_oldzd2buckszd2_1882;

				BgL_oldzd2buckszd2_1882 = STRUCT_REF(BgL_tablez00_125, (int) (2L));
				{	/* Llib/weakhash.scm 747 */
					obj_t BgL_maxzd2bucketzd2lenz00_1884;

					BgL_maxzd2bucketzd2lenz00_1884 =
						STRUCT_REF(BgL_tablez00_125, (int) (1L));
					{	/* Llib/weakhash.scm 748 */

						{
							long BgL_iz00_1886;
							long BgL_siza7eza7_1887;
							bool_t BgL_expandz00_1888;

							BgL_iz00_1886 = 0L;
							BgL_siza7eza7_1887 = 0L;
							BgL_expandz00_1888 = ((bool_t) 0);
						BgL_zc3z04anonymousza31683ze3z87_1889:
							if (
								(BgL_iz00_1886 <
									VECTOR_LENGTH(((obj_t) BgL_oldzd2buckszd2_1882))))
								{	/* Llib/weakhash.scm 754 */
									long BgL_countz00_1891;

									BgL_countz00_1891 = 0L;
									{	/* Llib/weakhash.scm 756 */
										obj_t BgL_arg1685z00_1892;

										{	/* Llib/weakhash.scm 756 */
											obj_t BgL_hook1119z00_1893;

											BgL_hook1119z00_1893 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
											{	/* Llib/weakhash.scm 761 */
												obj_t BgL_g1120z00_1894;

												BgL_g1120z00_1894 =
													VECTOR_REF(
													((obj_t) BgL_oldzd2buckszd2_1882), BgL_iz00_1886);
												{
													obj_t BgL_l1116z00_1896;
													obj_t BgL_h1117z00_1897;

													BgL_l1116z00_1896 = BgL_g1120z00_1894;
													BgL_h1117z00_1897 = BgL_hook1119z00_1893;
												BgL_zc3z04anonymousza31686ze3z87_1898:
													if (NULLP(BgL_l1116z00_1896))
														{	/* Llib/weakhash.scm 761 */
															BgL_arg1685z00_1892 = CDR(BgL_hook1119z00_1893);
														}
													else
														{	/* Llib/weakhash.scm 761 */
															bool_t BgL_test2481z00_6494;

															{	/* Llib/weakhash.scm 757 */
																obj_t BgL_cellz00_1907;

																BgL_cellz00_1907 =
																	CAR(((obj_t) BgL_l1116z00_1896));
																{	/* Llib/weakhash.scm 757 */
																	obj_t BgL_keyz00_1908;

																	BgL_keyz00_1908 =
																		bgl_weakptr_data(
																		((obj_t) BgL_cellz00_1907));
																	if ((BgL_keyz00_1908 == BUNSPEC))
																		{	/* Llib/weakhash.scm 758 */
																			BgL_test2481z00_6494 = ((bool_t) 0);
																		}
																	else
																		{	/* Llib/weakhash.scm 758 */
																			BgL_countz00_1891 =
																				(BgL_countz00_1891 + 1L);
																			BgL_test2481z00_6494 =
																				CBOOL(BgL_keyz00_1908);
																		}
																}
															}
															if (BgL_test2481z00_6494)
																{	/* Llib/weakhash.scm 761 */
																	obj_t BgL_nh1118z00_1903;

																	{	/* Llib/weakhash.scm 761 */
																		obj_t BgL_arg1691z00_1905;

																		BgL_arg1691z00_1905 =
																			CAR(((obj_t) BgL_l1116z00_1896));
																		BgL_nh1118z00_1903 =
																			MAKE_YOUNG_PAIR(BgL_arg1691z00_1905,
																			BNIL);
																	}
																	SET_CDR(BgL_h1117z00_1897,
																		BgL_nh1118z00_1903);
																	{	/* Llib/weakhash.scm 761 */
																		obj_t BgL_arg1689z00_1904;

																		BgL_arg1689z00_1904 =
																			CDR(((obj_t) BgL_l1116z00_1896));
																		{
																			obj_t BgL_h1117z00_6510;
																			obj_t BgL_l1116z00_6509;

																			BgL_l1116z00_6509 = BgL_arg1689z00_1904;
																			BgL_h1117z00_6510 = BgL_nh1118z00_1903;
																			BgL_h1117z00_1897 = BgL_h1117z00_6510;
																			BgL_l1116z00_1896 = BgL_l1116z00_6509;
																			goto
																				BgL_zc3z04anonymousza31686ze3z87_1898;
																		}
																	}
																}
															else
																{	/* Llib/weakhash.scm 761 */
																	obj_t BgL_arg1692z00_1906;

																	BgL_arg1692z00_1906 =
																		CDR(((obj_t) BgL_l1116z00_1896));
																	{
																		obj_t BgL_l1116z00_6513;

																		BgL_l1116z00_6513 = BgL_arg1692z00_1906;
																		BgL_l1116z00_1896 = BgL_l1116z00_6513;
																		goto BgL_zc3z04anonymousza31686ze3z87_1898;
																	}
																}
														}
												}
											}
										}
										VECTOR_SET(
											((obj_t) BgL_oldzd2buckszd2_1882), BgL_iz00_1886,
											BgL_arg1685z00_1892);
									}
									{	/* Llib/weakhash.scm 762 */
										long BgL_arg1699z00_1910;
										long BgL_arg1700z00_1911;
										bool_t BgL_arg1701z00_1912;

										BgL_arg1699z00_1910 = (BgL_iz00_1886 + 1L);
										BgL_arg1700z00_1911 =
											(BgL_siza7eza7_1887 + BgL_countz00_1891);
										if (BgL_expandz00_1888)
											{	/* Llib/weakhash.scm 763 */
												BgL_arg1701z00_1912 = BgL_expandz00_1888;
											}
										else
											{	/* Llib/weakhash.scm 763 */
												BgL_arg1701z00_1912 =
													(BgL_countz00_1891 >
													(long) CINT(BgL_maxzd2bucketzd2lenz00_1884));
											}
										{
											bool_t BgL_expandz00_6523;
											long BgL_siza7eza7_6522;
											long BgL_iz00_6521;

											BgL_iz00_6521 = BgL_arg1699z00_1910;
											BgL_siza7eza7_6522 = BgL_arg1700z00_1911;
											BgL_expandz00_6523 = BgL_arg1701z00_1912;
											BgL_expandz00_1888 = BgL_expandz00_6523;
											BgL_siza7eza7_1887 = BgL_siza7eza7_6522;
											BgL_iz00_1886 = BgL_iz00_6521;
											goto BgL_zc3z04anonymousza31683ze3z87_1889;
										}
									}
								}
							else
								{	/* Llib/weakhash.scm 753 */
									if (BgL_expandz00_1888)
										{	/* Llib/weakhash.scm 765 */
											long BgL_newzd2lenzd2_1914;

											BgL_newzd2lenzd2_1914 =
												(2L * VECTOR_LENGTH(((obj_t) BgL_oldzd2buckszd2_1882)));
											{	/* Llib/weakhash.scm 765 */
												obj_t BgL_maxzd2lenzd2_1915;

												BgL_maxzd2lenzd2_1915 =
													STRUCT_REF(BgL_tablez00_125, (int) (6L));
												{	/* Llib/weakhash.scm 766 */

													{	/* Llib/weakhash.scm 768 */
														obj_t BgL_nmaxz00_1916;

														{	/* Llib/weakhash.scm 768 */
															obj_t BgL_a1121z00_1919;

															BgL_a1121z00_1919 =
																STRUCT_REF(BgL_tablez00_125, (int) (1L));
															{	/* Llib/weakhash.scm 769 */
																obj_t BgL_b1122z00_1920;

																BgL_b1122z00_1920 =
																	STRUCT_REF(BgL_tablez00_125, (int) (7L));
																{	/* Llib/weakhash.scm 768 */

																	{	/* Llib/weakhash.scm 768 */
																		bool_t BgL_test2485z00_6534;

																		if (INTEGERP(BgL_a1121z00_1919))
																			{	/* Llib/weakhash.scm 768 */
																				BgL_test2485z00_6534 =
																					INTEGERP(BgL_b1122z00_1920);
																			}
																		else
																			{	/* Llib/weakhash.scm 768 */
																				BgL_test2485z00_6534 = ((bool_t) 0);
																			}
																		if (BgL_test2485z00_6534)
																			{	/* Llib/weakhash.scm 768 */
																				BgL_nmaxz00_1916 =
																					BINT(
																					((long) CINT(BgL_a1121z00_1919) *
																						(long) CINT(BgL_b1122z00_1920)));
																			}
																		else
																			{	/* Llib/weakhash.scm 768 */
																				BgL_nmaxz00_1916 =
																					BGl_2za2za2zz__r4_numbers_6_5z00
																					(BgL_a1121z00_1919,
																					BgL_b1122z00_1920);
																			}
																	}
																}
															}
														}
														{	/* Llib/weakhash.scm 771 */
															obj_t BgL_arg1702z00_1917;

															if (REALP(BgL_nmaxz00_1916))
																{	/* Llib/weakhash.scm 771 */
																	BgL_arg1702z00_1917 =
																		BINT(
																		(long) (REAL_TO_DOUBLE(BgL_nmaxz00_1916)));
																}
															else
																{	/* Llib/weakhash.scm 771 */
																	BgL_arg1702z00_1917 = BgL_nmaxz00_1916;
																}
															{	/* Llib/weakhash.scm 770 */
																int BgL_tmpz00_6548;

																BgL_tmpz00_6548 = (int) (1L);
																STRUCT_SET(BgL_tablez00_125, BgL_tmpz00_6548,
																	BgL_arg1702z00_1917);
													}}}
													{	/* Llib/weakhash.scm 773 */
														bool_t BgL_test2488z00_6551;

														if (((long) CINT(BgL_maxzd2lenzd2_1915) < 0L))
															{	/* Llib/weakhash.scm 773 */
																BgL_test2488z00_6551 = ((bool_t) 1);
															}
														else
															{	/* Llib/weakhash.scm 773 */
																BgL_test2488z00_6551 =
																	(BgL_newzd2lenzd2_1914 <=
																	(long) CINT(BgL_maxzd2lenzd2_1915));
															}
														if (BgL_test2488z00_6551)
															{	/* Llib/weakhash.scm 774 */
																obj_t BgL_newzd2buckszd2_1924;

																BgL_newzd2buckszd2_1924 =
																	make_vector(BgL_newzd2lenzd2_1914, BNIL);
																{	/* Llib/weakhash.scm 775 */
																	int BgL_tmpz00_6558;

																	BgL_tmpz00_6558 = (int) (2L);
																	STRUCT_SET(BgL_tablez00_125, BgL_tmpz00_6558,
																		BgL_newzd2buckszd2_1924);
																}
																{
																	long BgL_iz00_1926;

																	BgL_iz00_1926 = 0L;
																BgL_zc3z04anonymousza31707ze3z87_1927:
																	if (
																		(BgL_iz00_1926 <
																			VECTOR_LENGTH(
																				((obj_t) BgL_oldzd2buckszd2_1882))))
																		{	/* Llib/weakhash.scm 778 */
																			{	/* Llib/weakhash.scm 779 */
																				obj_t BgL_g1125z00_1929;

																				BgL_g1125z00_1929 =
																					VECTOR_REF(
																					((obj_t) BgL_oldzd2buckszd2_1882),
																					BgL_iz00_1926);
																				{
																					obj_t BgL_l1123z00_1931;

																					BgL_l1123z00_1931 = BgL_g1125z00_1929;
																				BgL_zc3z04anonymousza31709ze3z87_1932:
																					if (PAIRP(BgL_l1123z00_1931))
																						{	/* Llib/weakhash.scm 786 */
																							{	/* Llib/weakhash.scm 780 */
																								obj_t BgL_cellz00_1934;

																								BgL_cellz00_1934 =
																									CAR(BgL_l1123z00_1931);
																								{	/* Llib/weakhash.scm 780 */
																									obj_t BgL_keyz00_1935;

																									BgL_keyz00_1935 =
																										bgl_weakptr_data(
																										((obj_t) BgL_cellz00_1934));
																									{	/* Llib/weakhash.scm 781 */
																										long BgL_nz00_1936;

																										{	/* Llib/weakhash.scm 781 */
																											obj_t BgL_hashnz00_3615;

																											BgL_hashnz00_3615 =
																												STRUCT_REF
																												(BgL_tablez00_125,
																												(int) (4L));
																											if (PROCEDUREP
																												(BgL_hashnz00_3615))
																												{	/* Llib/weakhash.scm 781 */
																													obj_t
																														BgL_arg1268z00_3617;
																													BgL_arg1268z00_3617 =
																														BGL_PROCEDURE_CALL1
																														(BgL_hashnz00_3615,
																														BgL_keyz00_1935);
																													{	/* Llib/weakhash.scm 781 */
																														long BgL_nz00_3619;

																														BgL_nz00_3619 =
																															(long)
																															CINT
																															(BgL_arg1268z00_3617);
																														if ((BgL_nz00_3619 <
																																0L))
																															{	/* Llib/weakhash.scm 781 */
																																BgL_nz00_1936 =
																																	NEG
																																	(BgL_nz00_3619);
																															}
																														else
																															{	/* Llib/weakhash.scm 781 */
																																BgL_nz00_1936 =
																																	BgL_nz00_3619;
																															}
																													}
																												}
																											else
																												{	/* Llib/weakhash.scm 781 */
																													if (
																														(BgL_hashnz00_3615
																															==
																															BGl_symbol2173z00zz__weakhashz00))
																														{	/* Llib/weakhash.scm 781 */
																															BgL_nz00_1936 =
																																BGl_getzd2hashnumberzd2persistentz00zz__hashz00
																																(BgL_keyz00_1935);
																														}
																													else
																														{	/* Llib/weakhash.scm 781 */
																															BgL_nz00_1936 =
																																BGl_getzd2hashnumberzd2zz__hashz00
																																(BgL_keyz00_1935);
																														}
																												}
																										}
																										{	/* Llib/weakhash.scm 781 */
																											long BgL_hz00_1937;

																											{	/* Llib/weakhash.scm 783 */
																												long BgL_n1z00_3623;
																												long BgL_n2z00_3624;

																												BgL_n1z00_3623 =
																													BgL_nz00_1936;
																												BgL_n2z00_3624 =
																													BgL_newzd2lenzd2_1914;
																												{	/* Llib/weakhash.scm 783 */
																													bool_t
																														BgL_test2495z00_6588;
																													{	/* Llib/weakhash.scm 783 */
																														long
																															BgL_arg1961z00_3626;
																														BgL_arg1961z00_3626
																															=
																															(((BgL_n1z00_3623)
																																|
																																(BgL_n2z00_3624))
																															& -2147483648);
																														BgL_test2495z00_6588
																															=
																															(BgL_arg1961z00_3626
																															== 0L);
																													}
																													if (BgL_test2495z00_6588)
																														{	/* Llib/weakhash.scm 783 */
																															int32_t
																																BgL_arg1958z00_3627;
																															{	/* Llib/weakhash.scm 783 */
																																int32_t
																																	BgL_arg1959z00_3628;
																																int32_t
																																	BgL_arg1960z00_3629;
																																BgL_arg1959z00_3628
																																	=
																																	(int32_t)
																																	(BgL_n1z00_3623);
																																BgL_arg1960z00_3629
																																	=
																																	(int32_t)
																																	(BgL_n2z00_3624);
																																BgL_arg1958z00_3627
																																	=
																																	(BgL_arg1959z00_3628
																																	%
																																	BgL_arg1960z00_3629);
																															}
																															{	/* Llib/weakhash.scm 783 */
																																long
																																	BgL_arg2069z00_3634;
																																BgL_arg2069z00_3634
																																	=
																																	(long)
																																	(BgL_arg1958z00_3627);
																																BgL_hz00_1937 =
																																	(long)
																																	(BgL_arg2069z00_3634);
																														}}
																													else
																														{	/* Llib/weakhash.scm 783 */
																															BgL_hz00_1937 =
																																(BgL_n1z00_3623
																																%
																																BgL_n2z00_3624);
																														}
																												}
																											}
																											{	/* Llib/weakhash.scm 783 */

																												{	/* Llib/weakhash.scm 785 */
																													obj_t
																														BgL_arg1711z00_1938;
																													BgL_arg1711z00_1938 =
																														MAKE_YOUNG_PAIR
																														(BgL_cellz00_1934,
																														VECTOR_REF
																														(BgL_newzd2buckszd2_1924,
																															BgL_hz00_1937));
																													VECTOR_SET
																														(BgL_newzd2buckszd2_1924,
																														BgL_hz00_1937,
																														BgL_arg1711z00_1938);
																												}
																											}
																										}
																									}
																								}
																							}
																							{
																								obj_t BgL_l1123z00_6600;

																								BgL_l1123z00_6600 =
																									CDR(BgL_l1123z00_1931);
																								BgL_l1123z00_1931 =
																									BgL_l1123z00_6600;
																								goto
																									BgL_zc3z04anonymousza31709ze3z87_1932;
																							}
																						}
																					else
																						{	/* Llib/weakhash.scm 786 */
																							((bool_t) 1);
																						}
																				}
																			}
																			{
																				long BgL_iz00_6602;

																				BgL_iz00_6602 = (BgL_iz00_1926 + 1L);
																				BgL_iz00_1926 = BgL_iz00_6602;
																				goto
																					BgL_zc3z04anonymousza31707ze3z87_1927;
																			}
																		}
																	else
																		{	/* Llib/weakhash.scm 789 */
																			obj_t BgL_xz00_4252;

																			{	/* Llib/weakhash.scm 789 */
																				obj_t BgL_auxz00_6606;
																				int BgL_tmpz00_6604;

																				BgL_auxz00_6606 =
																					BINT(BgL_siza7eza7_1887);
																				BgL_tmpz00_6604 = (int) (0L);
																				BgL_xz00_4252 =
																					STRUCT_SET(BgL_tablez00_125,
																					BgL_tmpz00_6604, BgL_auxz00_6606);
																			}
																			return BUNSPEC;
																		}
																}
															}
														else
															{	/* Llib/weakhash.scm 793 */
																obj_t BgL_arg1718z00_1944;

																{	/* Llib/weakhash.scm 793 */
																	long BgL_arg1720z00_1945;

																	BgL_arg1720z00_1945 =
																		BGl_hashtablezd2siza7ez75zz__hashz00
																		(BgL_tablez00_125);
																	{	/* Llib/weakhash.scm 791 */
																		obj_t BgL_list1721z00_1946;

																		{	/* Llib/weakhash.scm 791 */
																			obj_t BgL_arg1722z00_1947;

																			{	/* Llib/weakhash.scm 791 */
																				obj_t BgL_arg1723z00_1948;

																				BgL_arg1723z00_1948 =
																					MAKE_YOUNG_PAIR(BINT
																					(BgL_arg1720z00_1945), BNIL);
																				BgL_arg1722z00_1947 =
																					MAKE_YOUNG_PAIR(BgL_maxzd2lenzd2_1915,
																					BgL_arg1723z00_1948);
																			}
																			BgL_list1721z00_1946 =
																				MAKE_YOUNG_PAIR(BINT
																				(BgL_newzd2lenzd2_1914),
																				BgL_arg1722z00_1947);
																		}
																		BgL_arg1718z00_1944 =
																			BGl_formatz00zz__r4_output_6_10_3z00
																			(BGl_string2181z00zz__weakhashz00,
																			BgL_list1721z00_1946);
																}}
																return
																	BGl_errorz00zz__errorz00
																	(BGl_string2182z00zz__weakhashz00,
																	BgL_arg1718z00_1944, BgL_tablez00_125);
															}
													}
												}
											}
										}
									else
										{	/* Llib/weakhash.scm 764 */
											return BFALSE;
										}
								}
						}
					}
				}
			}
		}

	}



/* weak-old-hashtable-expand! */
	obj_t BGl_weakzd2oldzd2hashtablezd2expandz12zc0zz__weakhashz00(obj_t
		BgL_tablez00_126)
	{
		{	/* Llib/weakhash.scm 799 */
			if ((1L == (long) CINT(STRUCT_REF(BgL_tablez00_126, (int) (5L)))))
				{	/* Llib/weakhash.scm 800 */
					obj_t BgL_oldzd2buckszd2_1953;

					BgL_oldzd2buckszd2_1953 = STRUCT_REF(BgL_tablez00_126, (int) (2L));
					{	/* Llib/weakhash.scm 800 */
						long BgL_newzd2buckszd2lenz00_1955;

						BgL_newzd2buckszd2lenz00_1955 =
							(2L * VECTOR_LENGTH(((obj_t) BgL_oldzd2buckszd2_1953)));
						{	/* Llib/weakhash.scm 800 */
							obj_t BgL_newzd2buckszd2_1956;

							BgL_newzd2buckszd2_1956 =
								make_vector(BgL_newzd2buckszd2lenz00_1955, BNIL);
							{	/* Llib/weakhash.scm 800 */
								obj_t BgL_countz00_1957;

								BgL_countz00_1957 = STRUCT_REF(BgL_tablez00_126, (int) (0L));
								{	/* Llib/weakhash.scm 800 */

									{	/* Llib/weakhash.scm 800 */
										obj_t BgL_nmaxz00_1958;

										{	/* Llib/weakhash.scm 800 */
											obj_t BgL_a1126z00_1961;

											BgL_a1126z00_1961 =
												STRUCT_REF(BgL_tablez00_126, (int) (1L));
											{	/* Llib/weakhash.scm 800 */
												obj_t BgL_b1127z00_1962;

												BgL_b1127z00_1962 =
													STRUCT_REF(BgL_tablez00_126, (int) (7L));
												{	/* Llib/weakhash.scm 800 */

													{	/* Llib/weakhash.scm 800 */
														bool_t BgL_test2497z00_6634;

														if (INTEGERP(BgL_a1126z00_1961))
															{	/* Llib/weakhash.scm 800 */
																BgL_test2497z00_6634 =
																	INTEGERP(BgL_b1127z00_1962);
															}
														else
															{	/* Llib/weakhash.scm 800 */
																BgL_test2497z00_6634 = ((bool_t) 0);
															}
														if (BgL_test2497z00_6634)
															{	/* Llib/weakhash.scm 800 */
																BgL_nmaxz00_1958 =
																	BINT(
																	((long) CINT(BgL_a1126z00_1961) *
																		(long) CINT(BgL_b1127z00_1962)));
															}
														else
															{	/* Llib/weakhash.scm 800 */
																BgL_nmaxz00_1958 =
																	BGl_2za2za2zz__r4_numbers_6_5z00
																	(BgL_a1126z00_1961, BgL_b1127z00_1962);
															}
													}
												}
											}
										}
										{	/* Llib/weakhash.scm 800 */
											obj_t BgL_arg1726z00_1959;

											if (REALP(BgL_nmaxz00_1958))
												{	/* Llib/weakhash.scm 800 */
													BgL_arg1726z00_1959 =
														BINT((long) (REAL_TO_DOUBLE(BgL_nmaxz00_1958)));
												}
											else
												{	/* Llib/weakhash.scm 800 */
													BgL_arg1726z00_1959 = BgL_nmaxz00_1958;
												}
											{	/* Llib/weakhash.scm 800 */
												int BgL_tmpz00_6648;

												BgL_tmpz00_6648 = (int) (1L);
												STRUCT_SET(BgL_tablez00_126, BgL_tmpz00_6648,
													BgL_arg1726z00_1959);
									}}}
									{	/* Llib/weakhash.scm 800 */
										int BgL_tmpz00_6651;

										BgL_tmpz00_6651 = (int) (2L);
										STRUCT_SET(BgL_tablez00_126, BgL_tmpz00_6651,
											BgL_newzd2buckszd2_1956);
									}
									{
										long BgL_iz00_1965;

										BgL_iz00_1965 = 0L;
									BgL_zc3z04anonymousza31729ze3z87_1966:
										if (
											(BgL_iz00_1965 <
												VECTOR_LENGTH(((obj_t) BgL_oldzd2buckszd2_1953))))
											{	/* Llib/weakhash.scm 800 */
												{	/* Llib/weakhash.scm 800 */
													obj_t BgL_g1130z00_1968;

													BgL_g1130z00_1968 =
														VECTOR_REF(
														((obj_t) BgL_oldzd2buckszd2_1953), BgL_iz00_1965);
													{
														obj_t BgL_l1128z00_1970;

														BgL_l1128z00_1970 = BgL_g1130z00_1968;
													BgL_zc3z04anonymousza31731ze3z87_1971:
														if (PAIRP(BgL_l1128z00_1970))
															{	/* Llib/weakhash.scm 800 */
																{	/* Llib/weakhash.scm 818 */
																	obj_t BgL_cellz00_1973;

																	BgL_cellz00_1973 = CAR(BgL_l1128z00_1970);
																	{	/* Llib/weakhash.scm 818 */
																		obj_t BgL_keyz00_1974;

																		{	/* Llib/weakhash.scm 818 */
																			obj_t BgL_arg1736z00_1979;

																			BgL_arg1736z00_1979 =
																				CAR(((obj_t) BgL_cellz00_1973));
																			BgL_keyz00_1974 =
																				bgl_weakptr_data(
																				((obj_t) BgL_arg1736z00_1979));
																		}
																		if ((BgL_keyz00_1974 == BUNSPEC))
																			{	/* Llib/weakhash.scm 819 */
																				BgL_countz00_1957 =
																					SUBFX(BgL_countz00_1957, BINT(1L));
																			}
																		else
																			{	/* Llib/weakhash.scm 821 */
																				long BgL_hz00_1975;

																				{	/* Llib/weakhash.scm 822 */
																					long BgL_arg1735z00_1978;

																					{	/* Llib/weakhash.scm 822 */
																						obj_t BgL_hashnz00_3664;

																						BgL_hashnz00_3664 =
																							STRUCT_REF(BgL_tablez00_126,
																							(int) (4L));
																						if (PROCEDUREP(BgL_hashnz00_3664))
																							{	/* Llib/weakhash.scm 822 */
																								obj_t BgL_arg1268z00_3666;

																								BgL_arg1268z00_3666 =
																									BGL_PROCEDURE_CALL1
																									(BgL_hashnz00_3664,
																									BgL_keyz00_1974);
																								{	/* Llib/weakhash.scm 822 */
																									long BgL_nz00_3668;

																									BgL_nz00_3668 =
																										(long)
																										CINT(BgL_arg1268z00_3666);
																									if ((BgL_nz00_3668 < 0L))
																										{	/* Llib/weakhash.scm 822 */
																											BgL_arg1735z00_1978 =
																												NEG(BgL_nz00_3668);
																										}
																									else
																										{	/* Llib/weakhash.scm 822 */
																											BgL_arg1735z00_1978 =
																												BgL_nz00_3668;
																										}
																								}
																							}
																						else
																							{	/* Llib/weakhash.scm 822 */
																								if (
																									(BgL_hashnz00_3664 ==
																										BGl_symbol2173z00zz__weakhashz00))
																									{	/* Llib/weakhash.scm 822 */
																										BgL_arg1735z00_1978 =
																											BGl_getzd2hashnumberzd2persistentz00zz__hashz00
																											(BgL_keyz00_1974);
																									}
																								else
																									{	/* Llib/weakhash.scm 822 */
																										BgL_arg1735z00_1978 =
																											BGl_getzd2hashnumberzd2zz__hashz00
																											(BgL_keyz00_1974);
																									}
																							}
																					}
																					{	/* Llib/weakhash.scm 821 */
																						long BgL_n1z00_3672;
																						long BgL_n2z00_3673;

																						BgL_n1z00_3672 =
																							BgL_arg1735z00_1978;
																						BgL_n2z00_3673 =
																							BgL_newzd2buckszd2lenz00_1955;
																						{	/* Llib/weakhash.scm 821 */
																							bool_t BgL_test2506z00_6687;

																							{	/* Llib/weakhash.scm 821 */
																								long BgL_arg1961z00_3675;

																								BgL_arg1961z00_3675 =
																									(((BgL_n1z00_3672) |
																										(BgL_n2z00_3673)) &
																									-2147483648);
																								BgL_test2506z00_6687 =
																									(BgL_arg1961z00_3675 == 0L);
																							}
																							if (BgL_test2506z00_6687)
																								{	/* Llib/weakhash.scm 821 */
																									int32_t BgL_arg1958z00_3676;

																									{	/* Llib/weakhash.scm 821 */
																										int32_t BgL_arg1959z00_3677;
																										int32_t BgL_arg1960z00_3678;

																										BgL_arg1959z00_3677 =
																											(int32_t)
																											(BgL_n1z00_3672);
																										BgL_arg1960z00_3678 =
																											(int32_t)
																											(BgL_n2z00_3673);
																										BgL_arg1958z00_3676 =
																											(BgL_arg1959z00_3677 %
																											BgL_arg1960z00_3678);
																									}
																									{	/* Llib/weakhash.scm 821 */
																										long BgL_arg2069z00_3683;

																										BgL_arg2069z00_3683 =
																											(long)
																											(BgL_arg1958z00_3676);
																										BgL_hz00_1975 =
																											(long)
																											(BgL_arg2069z00_3683);
																								}}
																							else
																								{	/* Llib/weakhash.scm 821 */
																									BgL_hz00_1975 =
																										(BgL_n1z00_3672 %
																										BgL_n2z00_3673);
																								}
																						}
																					}
																				}
																				{	/* Llib/weakhash.scm 827 */
																					obj_t BgL_arg1733z00_1976;

																					BgL_arg1733z00_1976 =
																						MAKE_YOUNG_PAIR(BgL_cellz00_1973,
																						VECTOR_REF(BgL_newzd2buckszd2_1956,
																							BgL_hz00_1975));
																					VECTOR_SET(BgL_newzd2buckszd2_1956,
																						BgL_hz00_1975, BgL_arg1733z00_1976);
																				}
																			}
																	}
																}
																{
																	obj_t BgL_l1128z00_6699;

																	BgL_l1128z00_6699 = CDR(BgL_l1128z00_1970);
																	BgL_l1128z00_1970 = BgL_l1128z00_6699;
																	goto BgL_zc3z04anonymousza31731ze3z87_1971;
																}
															}
														else
															{	/* Llib/weakhash.scm 800 */
																((bool_t) 1);
															}
													}
												}
												{
													long BgL_iz00_6701;

													BgL_iz00_6701 = (BgL_iz00_1965 + 1L);
													BgL_iz00_1965 = BgL_iz00_6701;
													goto BgL_zc3z04anonymousza31729ze3z87_1966;
												}
											}
										else
											{	/* Llib/weakhash.scm 800 */
												((bool_t) 0);
											}
									}
									{	/* Llib/weakhash.scm 800 */
										obj_t BgL_vz00_3691;

										BgL_vz00_3691 = BgL_countz00_1957;
										{	/* Llib/weakhash.scm 800 */
											obj_t BgL_xz00_4253;

											{	/* Llib/weakhash.scm 800 */
												int BgL_tmpz00_6703;

												BgL_tmpz00_6703 = (int) (0L);
												BgL_xz00_4253 =
													STRUCT_SET(BgL_tablez00_126, BgL_tmpz00_6703,
													BgL_vz00_3691);
											}
											return BUNSPEC;
										}
									}
								}
							}
						}
					}
				}
			else
				{	/* Llib/weakhash.scm 800 */
					if ((2L == (long) CINT(STRUCT_REF(BgL_tablez00_126, (int) (5L)))))
						{	/* Llib/weakhash.scm 800 */
							obj_t BgL_oldzd2buckszd2_1986;

							BgL_oldzd2buckszd2_1986 =
								STRUCT_REF(BgL_tablez00_126, (int) (2L));
							{	/* Llib/weakhash.scm 800 */
								long BgL_newzd2buckszd2lenz00_1988;

								BgL_newzd2buckszd2lenz00_1988 =
									(2L * VECTOR_LENGTH(((obj_t) BgL_oldzd2buckszd2_1986)));
								{	/* Llib/weakhash.scm 800 */
									obj_t BgL_newzd2buckszd2_1989;

									BgL_newzd2buckszd2_1989 =
										make_vector(BgL_newzd2buckszd2lenz00_1988, BNIL);
									{	/* Llib/weakhash.scm 800 */
										obj_t BgL_countz00_1990;

										BgL_countz00_1990 =
											STRUCT_REF(BgL_tablez00_126, (int) (0L));
										{	/* Llib/weakhash.scm 800 */

											{	/* Llib/weakhash.scm 800 */
												obj_t BgL_nmaxz00_1991;

												{	/* Llib/weakhash.scm 800 */
													obj_t BgL_a1131z00_1994;

													BgL_a1131z00_1994 =
														STRUCT_REF(BgL_tablez00_126, (int) (1L));
													{	/* Llib/weakhash.scm 800 */
														obj_t BgL_b1132z00_1995;

														BgL_b1132z00_1995 =
															STRUCT_REF(BgL_tablez00_126, (int) (7L));
														{	/* Llib/weakhash.scm 800 */

															{	/* Llib/weakhash.scm 800 */
																bool_t BgL_test2508z00_6723;

																if (INTEGERP(BgL_a1131z00_1994))
																	{	/* Llib/weakhash.scm 800 */
																		BgL_test2508z00_6723 =
																			INTEGERP(BgL_b1132z00_1995);
																	}
																else
																	{	/* Llib/weakhash.scm 800 */
																		BgL_test2508z00_6723 = ((bool_t) 0);
																	}
																if (BgL_test2508z00_6723)
																	{	/* Llib/weakhash.scm 800 */
																		BgL_nmaxz00_1991 =
																			BINT(
																			((long) CINT(BgL_a1131z00_1994) *
																				(long) CINT(BgL_b1132z00_1995)));
																	}
																else
																	{	/* Llib/weakhash.scm 800 */
																		BgL_nmaxz00_1991 =
																			BGl_2za2za2zz__r4_numbers_6_5z00
																			(BgL_a1131z00_1994, BgL_b1132z00_1995);
																	}
															}
														}
													}
												}
												{	/* Llib/weakhash.scm 800 */
													obj_t BgL_arg1741z00_1992;

													if (REALP(BgL_nmaxz00_1991))
														{	/* Llib/weakhash.scm 800 */
															BgL_arg1741z00_1992 =
																BINT((long) (REAL_TO_DOUBLE(BgL_nmaxz00_1991)));
														}
													else
														{	/* Llib/weakhash.scm 800 */
															BgL_arg1741z00_1992 = BgL_nmaxz00_1991;
														}
													{	/* Llib/weakhash.scm 800 */
														int BgL_tmpz00_6737;

														BgL_tmpz00_6737 = (int) (1L);
														STRUCT_SET(BgL_tablez00_126, BgL_tmpz00_6737,
															BgL_arg1741z00_1992);
											}}}
											{	/* Llib/weakhash.scm 800 */
												int BgL_tmpz00_6740;

												BgL_tmpz00_6740 = (int) (2L);
												STRUCT_SET(BgL_tablez00_126, BgL_tmpz00_6740,
													BgL_newzd2buckszd2_1989);
											}
											{
												long BgL_iz00_1998;

												BgL_iz00_1998 = 0L;
											BgL_zc3z04anonymousza31744ze3z87_1999:
												if (
													(BgL_iz00_1998 <
														VECTOR_LENGTH(((obj_t) BgL_oldzd2buckszd2_1986))))
													{	/* Llib/weakhash.scm 800 */
														{	/* Llib/weakhash.scm 800 */
															obj_t BgL_g1135z00_2001;

															BgL_g1135z00_2001 =
																VECTOR_REF(
																((obj_t) BgL_oldzd2buckszd2_1986),
																BgL_iz00_1998);
															{
																obj_t BgL_l1133z00_2003;

																BgL_l1133z00_2003 = BgL_g1135z00_2001;
															BgL_zc3z04anonymousza31746ze3z87_2004:
																if (PAIRP(BgL_l1133z00_2003))
																	{	/* Llib/weakhash.scm 800 */
																		{	/* Llib/weakhash.scm 831 */
																			obj_t BgL_cellz00_2006;

																			BgL_cellz00_2006 = CAR(BgL_l1133z00_2003);
																			{	/* Llib/weakhash.scm 831 */
																				obj_t BgL_dataz00_2007;

																				{	/* Llib/weakhash.scm 831 */
																					obj_t BgL_arg1752z00_2013;

																					BgL_arg1752z00_2013 =
																						CDR(((obj_t) BgL_cellz00_2006));
																					BgL_dataz00_2007 =
																						bgl_weakptr_data(
																						((obj_t) BgL_arg1752z00_2013));
																				}
																				if ((BgL_dataz00_2007 == BUNSPEC))
																					{	/* Llib/weakhash.scm 832 */
																						BgL_countz00_1990 =
																							SUBFX(BgL_countz00_1990,
																							BINT(1L));
																					}
																				else
																					{	/* Llib/weakhash.scm 834 */
																						long BgL_hz00_2008;

																						{	/* Llib/weakhash.scm 836 */
																							long BgL_arg1750z00_2011;

																							{	/* Llib/weakhash.scm 836 */
																								obj_t BgL_arg1751z00_2012;

																								BgL_arg1751z00_2012 =
																									CAR(
																									((obj_t) BgL_cellz00_2006));
																								{	/* Llib/weakhash.scm 835 */
																									obj_t BgL_hashnz00_3715;

																									BgL_hashnz00_3715 =
																										STRUCT_REF(BgL_tablez00_126,
																										(int) (4L));
																									if (PROCEDUREP
																										(BgL_hashnz00_3715))
																										{	/* Llib/weakhash.scm 835 */
																											obj_t BgL_arg1268z00_3717;

																											BgL_arg1268z00_3717 =
																												BGL_PROCEDURE_CALL1
																												(BgL_hashnz00_3715,
																												BgL_arg1751z00_2012);
																											{	/* Llib/weakhash.scm 835 */
																												long BgL_nz00_3719;

																												BgL_nz00_3719 =
																													(long)
																													CINT
																													(BgL_arg1268z00_3717);
																												if ((BgL_nz00_3719 <
																														0L))
																													{	/* Llib/weakhash.scm 835 */
																														BgL_arg1750z00_2011
																															=
																															NEG
																															(BgL_nz00_3719);
																													}
																												else
																													{	/* Llib/weakhash.scm 835 */
																														BgL_arg1750z00_2011
																															= BgL_nz00_3719;
																													}
																											}
																										}
																									else
																										{	/* Llib/weakhash.scm 835 */
																											if (
																												(BgL_hashnz00_3715 ==
																													BGl_symbol2173z00zz__weakhashz00))
																												{	/* Llib/weakhash.scm 835 */
																													BgL_arg1750z00_2011 =
																														BGl_getzd2hashnumberzd2persistentz00zz__hashz00
																														(BgL_arg1751z00_2012);
																												}
																											else
																												{	/* Llib/weakhash.scm 835 */
																													BgL_arg1750z00_2011 =
																														BGl_getzd2hashnumberzd2zz__hashz00
																														(BgL_arg1751z00_2012);
																												}
																										}
																								}
																							}
																							{	/* Llib/weakhash.scm 834 */
																								long BgL_n1z00_3723;
																								long BgL_n2z00_3724;

																								BgL_n1z00_3723 =
																									BgL_arg1750z00_2011;
																								BgL_n2z00_3724 =
																									BgL_newzd2buckszd2lenz00_1988;
																								{	/* Llib/weakhash.scm 834 */
																									bool_t BgL_test2517z00_6778;

																									{	/* Llib/weakhash.scm 834 */
																										long BgL_arg1961z00_3726;

																										BgL_arg1961z00_3726 =
																											(((BgL_n1z00_3723) |
																												(BgL_n2z00_3724)) &
																											-2147483648);
																										BgL_test2517z00_6778 =
																											(BgL_arg1961z00_3726 ==
																											0L);
																									}
																									if (BgL_test2517z00_6778)
																										{	/* Llib/weakhash.scm 834 */
																											int32_t
																												BgL_arg1958z00_3727;
																											{	/* Llib/weakhash.scm 834 */
																												int32_t
																													BgL_arg1959z00_3728;
																												int32_t
																													BgL_arg1960z00_3729;
																												BgL_arg1959z00_3728 =
																													(int32_t)
																													(BgL_n1z00_3723);
																												BgL_arg1960z00_3729 =
																													(int32_t)
																													(BgL_n2z00_3724);
																												BgL_arg1958z00_3727 =
																													(BgL_arg1959z00_3728 %
																													BgL_arg1960z00_3729);
																											}
																											{	/* Llib/weakhash.scm 834 */
																												long
																													BgL_arg2069z00_3734;
																												BgL_arg2069z00_3734 =
																													(long)
																													(BgL_arg1958z00_3727);
																												BgL_hz00_2008 =
																													(long)
																													(BgL_arg2069z00_3734);
																										}}
																									else
																										{	/* Llib/weakhash.scm 834 */
																											BgL_hz00_2008 =
																												(BgL_n1z00_3723 %
																												BgL_n2z00_3724);
																										}
																								}
																							}
																						}
																						{	/* Llib/weakhash.scm 841 */
																							obj_t BgL_arg1748z00_2009;

																							BgL_arg1748z00_2009 =
																								MAKE_YOUNG_PAIR
																								(BgL_cellz00_2006,
																								VECTOR_REF
																								(BgL_newzd2buckszd2_1989,
																									BgL_hz00_2008));
																							VECTOR_SET
																								(BgL_newzd2buckszd2_1989,
																								BgL_hz00_2008,
																								BgL_arg1748z00_2009);
																						}
																					}
																			}
																		}
																		{
																			obj_t BgL_l1133z00_6790;

																			BgL_l1133z00_6790 =
																				CDR(BgL_l1133z00_2003);
																			BgL_l1133z00_2003 = BgL_l1133z00_6790;
																			goto
																				BgL_zc3z04anonymousza31746ze3z87_2004;
																		}
																	}
																else
																	{	/* Llib/weakhash.scm 800 */
																		((bool_t) 1);
																	}
															}
														}
														{
															long BgL_iz00_6792;

															BgL_iz00_6792 = (BgL_iz00_1998 + 1L);
															BgL_iz00_1998 = BgL_iz00_6792;
															goto BgL_zc3z04anonymousza31744ze3z87_1999;
														}
													}
												else
													{	/* Llib/weakhash.scm 800 */
														((bool_t) 0);
													}
											}
											{	/* Llib/weakhash.scm 800 */
												obj_t BgL_vz00_3742;

												BgL_vz00_3742 = BgL_countz00_1990;
												{	/* Llib/weakhash.scm 800 */
													obj_t BgL_xz00_4254;

													{	/* Llib/weakhash.scm 800 */
														int BgL_tmpz00_6794;

														BgL_tmpz00_6794 = (int) (0L);
														BgL_xz00_4254 =
															STRUCT_SET(BgL_tablez00_126, BgL_tmpz00_6794,
															BgL_vz00_3742);
													}
													return BUNSPEC;
												}
											}
										}
									}
								}
							}
						}
					else
						{	/* Llib/weakhash.scm 800 */
							if ((3L == (long) CINT(STRUCT_REF(BgL_tablez00_126, (int) (5L)))))
								{	/* Llib/weakhash.scm 800 */
									obj_t BgL_oldzd2buckszd2_2020;

									BgL_oldzd2buckszd2_2020 =
										STRUCT_REF(BgL_tablez00_126, (int) (2L));
									{	/* Llib/weakhash.scm 800 */
										long BgL_newzd2buckszd2lenz00_2022;

										BgL_newzd2buckszd2lenz00_2022 =
											(2L * VECTOR_LENGTH(((obj_t) BgL_oldzd2buckszd2_2020)));
										{	/* Llib/weakhash.scm 800 */
											obj_t BgL_newzd2buckszd2_2023;

											BgL_newzd2buckszd2_2023 =
												make_vector(BgL_newzd2buckszd2lenz00_2022, BNIL);
											{	/* Llib/weakhash.scm 800 */
												obj_t BgL_countz00_2024;

												BgL_countz00_2024 =
													STRUCT_REF(BgL_tablez00_126, (int) (0L));
												{	/* Llib/weakhash.scm 800 */

													{	/* Llib/weakhash.scm 800 */
														obj_t BgL_nmaxz00_2025;

														{	/* Llib/weakhash.scm 800 */
															obj_t BgL_a1136z00_2028;

															BgL_a1136z00_2028 =
																STRUCT_REF(BgL_tablez00_126, (int) (1L));
															{	/* Llib/weakhash.scm 800 */
																obj_t BgL_b1137z00_2029;

																BgL_b1137z00_2029 =
																	STRUCT_REF(BgL_tablez00_126, (int) (7L));
																{	/* Llib/weakhash.scm 800 */

																	{	/* Llib/weakhash.scm 800 */
																		bool_t BgL_test2519z00_6814;

																		if (INTEGERP(BgL_a1136z00_2028))
																			{	/* Llib/weakhash.scm 800 */
																				BgL_test2519z00_6814 =
																					INTEGERP(BgL_b1137z00_2029);
																			}
																		else
																			{	/* Llib/weakhash.scm 800 */
																				BgL_test2519z00_6814 = ((bool_t) 0);
																			}
																		if (BgL_test2519z00_6814)
																			{	/* Llib/weakhash.scm 800 */
																				BgL_nmaxz00_2025 =
																					BINT(
																					((long) CINT(BgL_a1136z00_2028) *
																						(long) CINT(BgL_b1137z00_2029)));
																			}
																		else
																			{	/* Llib/weakhash.scm 800 */
																				BgL_nmaxz00_2025 =
																					BGl_2za2za2zz__r4_numbers_6_5z00
																					(BgL_a1136z00_2028,
																					BgL_b1137z00_2029);
																			}
																	}
																}
															}
														}
														{	/* Llib/weakhash.scm 800 */
															obj_t BgL_arg1757z00_2026;

															if (REALP(BgL_nmaxz00_2025))
																{	/* Llib/weakhash.scm 800 */
																	BgL_arg1757z00_2026 =
																		BINT(
																		(long) (REAL_TO_DOUBLE(BgL_nmaxz00_2025)));
																}
															else
																{	/* Llib/weakhash.scm 800 */
																	BgL_arg1757z00_2026 = BgL_nmaxz00_2025;
																}
															{	/* Llib/weakhash.scm 800 */
																int BgL_tmpz00_6828;

																BgL_tmpz00_6828 = (int) (1L);
																STRUCT_SET(BgL_tablez00_126, BgL_tmpz00_6828,
																	BgL_arg1757z00_2026);
													}}}
													{	/* Llib/weakhash.scm 800 */
														int BgL_tmpz00_6831;

														BgL_tmpz00_6831 = (int) (2L);
														STRUCT_SET(BgL_tablez00_126, BgL_tmpz00_6831,
															BgL_newzd2buckszd2_2023);
													}
													{
														long BgL_iz00_2032;

														BgL_iz00_2032 = 0L;
													BgL_zc3z04anonymousza31760ze3z87_2033:
														if (
															(BgL_iz00_2032 <
																VECTOR_LENGTH(
																	((obj_t) BgL_oldzd2buckszd2_2020))))
															{	/* Llib/weakhash.scm 800 */
																{	/* Llib/weakhash.scm 800 */
																	obj_t BgL_g1140z00_2035;

																	BgL_g1140z00_2035 =
																		VECTOR_REF(
																		((obj_t) BgL_oldzd2buckszd2_2020),
																		BgL_iz00_2032);
																	{
																		obj_t BgL_l1138z00_2037;

																		BgL_l1138z00_2037 = BgL_g1140z00_2035;
																	BgL_zc3z04anonymousza31762ze3z87_2038:
																		if (PAIRP(BgL_l1138z00_2037))
																			{	/* Llib/weakhash.scm 800 */
																				{	/* Llib/weakhash.scm 845 */
																					obj_t BgL_cellz00_2040;

																					BgL_cellz00_2040 =
																						CAR(BgL_l1138z00_2037);
																					{	/* Llib/weakhash.scm 845 */
																						obj_t BgL_keyz00_2041;
																						obj_t BgL_dataz00_2042;

																						{	/* Llib/weakhash.scm 845 */
																							obj_t BgL_arg1768z00_2048;

																							BgL_arg1768z00_2048 =
																								CAR(((obj_t) BgL_cellz00_2040));
																							BgL_keyz00_2041 =
																								bgl_weakptr_data(
																								((obj_t) BgL_arg1768z00_2048));
																						}
																						{	/* Llib/weakhash.scm 846 */
																							obj_t BgL_arg1769z00_2049;

																							BgL_arg1769z00_2049 =
																								CDR(((obj_t) BgL_cellz00_2040));
																							BgL_dataz00_2042 =
																								bgl_weakptr_data(
																								((obj_t) BgL_arg1769z00_2049));
																						}
																						{	/* Llib/weakhash.scm 847 */
																							bool_t BgL_test2524z00_6851;

																							if ((BgL_keyz00_2041 == BUNSPEC))
																								{	/* Llib/weakhash.scm 847 */
																									BgL_test2524z00_6851 =
																										((bool_t) 1);
																								}
																							else
																								{	/* Llib/weakhash.scm 847 */
																									BgL_test2524z00_6851 =
																										(BgL_dataz00_2042 ==
																										BUNSPEC);
																								}
																							if (BgL_test2524z00_6851)
																								{	/* Llib/weakhash.scm 847 */
																									BgL_countz00_2024 =
																										SUBFX(BgL_countz00_2024,
																										BINT(1L));
																								}
																							else
																								{	/* Llib/weakhash.scm 850 */
																									long BgL_hz00_2044;

																									{	/* Llib/weakhash.scm 851 */
																										long BgL_arg1767z00_2047;

																										{	/* Llib/weakhash.scm 851 */
																											obj_t BgL_hashnz00_3767;

																											BgL_hashnz00_3767 =
																												STRUCT_REF
																												(BgL_tablez00_126,
																												(int) (4L));
																											if (PROCEDUREP
																												(BgL_hashnz00_3767))
																												{	/* Llib/weakhash.scm 851 */
																													obj_t
																														BgL_arg1268z00_3769;
																													BgL_arg1268z00_3769 =
																														BGL_PROCEDURE_CALL1
																														(BgL_hashnz00_3767,
																														BgL_keyz00_2041);
																													{	/* Llib/weakhash.scm 851 */
																														long BgL_nz00_3771;

																														BgL_nz00_3771 =
																															(long)
																															CINT
																															(BgL_arg1268z00_3769);
																														if ((BgL_nz00_3771 <
																																0L))
																															{	/* Llib/weakhash.scm 851 */
																																BgL_arg1767z00_2047
																																	=
																																	NEG
																																	(BgL_nz00_3771);
																															}
																														else
																															{	/* Llib/weakhash.scm 851 */
																																BgL_arg1767z00_2047
																																	=
																																	BgL_nz00_3771;
																															}
																													}
																												}
																											else
																												{	/* Llib/weakhash.scm 851 */
																													if (
																														(BgL_hashnz00_3767
																															==
																															BGl_symbol2173z00zz__weakhashz00))
																														{	/* Llib/weakhash.scm 851 */
																															BgL_arg1767z00_2047
																																=
																																BGl_getzd2hashnumberzd2persistentz00zz__hashz00
																																(BgL_keyz00_2041);
																														}
																													else
																														{	/* Llib/weakhash.scm 851 */
																															BgL_arg1767z00_2047
																																=
																																BGl_getzd2hashnumberzd2zz__hashz00
																																(BgL_keyz00_2041);
																														}
																												}
																										}
																										{	/* Llib/weakhash.scm 850 */
																											long BgL_n1z00_3775;
																											long BgL_n2z00_3776;

																											BgL_n1z00_3775 =
																												BgL_arg1767z00_2047;
																											BgL_n2z00_3776 =
																												BgL_newzd2buckszd2lenz00_2022;
																											{	/* Llib/weakhash.scm 850 */
																												bool_t
																													BgL_test2529z00_6873;
																												{	/* Llib/weakhash.scm 850 */
																													long
																														BgL_arg1961z00_3778;
																													BgL_arg1961z00_3778 =
																														(((BgL_n1z00_3775) |
																															(BgL_n2z00_3776))
																														& -2147483648);
																													BgL_test2529z00_6873 =
																														(BgL_arg1961z00_3778
																														== 0L);
																												}
																												if (BgL_test2529z00_6873)
																													{	/* Llib/weakhash.scm 850 */
																														int32_t
																															BgL_arg1958z00_3779;
																														{	/* Llib/weakhash.scm 850 */
																															int32_t
																																BgL_arg1959z00_3780;
																															int32_t
																																BgL_arg1960z00_3781;
																															BgL_arg1959z00_3780
																																=
																																(int32_t)
																																(BgL_n1z00_3775);
																															BgL_arg1960z00_3781
																																=
																																(int32_t)
																																(BgL_n2z00_3776);
																															BgL_arg1958z00_3779
																																=
																																(BgL_arg1959z00_3780
																																%
																																BgL_arg1960z00_3781);
																														}
																														{	/* Llib/weakhash.scm 850 */
																															long
																																BgL_arg2069z00_3786;
																															BgL_arg2069z00_3786
																																=
																																(long)
																																(BgL_arg1958z00_3779);
																															BgL_hz00_2044 =
																																(long)
																																(BgL_arg2069z00_3786);
																													}}
																												else
																													{	/* Llib/weakhash.scm 850 */
																														BgL_hz00_2044 =
																															(BgL_n1z00_3775 %
																															BgL_n2z00_3776);
																													}
																											}
																										}
																									}
																									{	/* Llib/weakhash.scm 856 */
																										obj_t BgL_arg1765z00_2045;

																										BgL_arg1765z00_2045 =
																											MAKE_YOUNG_PAIR
																											(BgL_cellz00_2040,
																											VECTOR_REF
																											(BgL_newzd2buckszd2_2023,
																												BgL_hz00_2044));
																										VECTOR_SET
																											(BgL_newzd2buckszd2_2023,
																											BgL_hz00_2044,
																											BgL_arg1765z00_2045);
																									}
																								}
																						}
																					}
																				}
																				{
																					obj_t BgL_l1138z00_6885;

																					BgL_l1138z00_6885 =
																						CDR(BgL_l1138z00_2037);
																					BgL_l1138z00_2037 = BgL_l1138z00_6885;
																					goto
																						BgL_zc3z04anonymousza31762ze3z87_2038;
																				}
																			}
																		else
																			{	/* Llib/weakhash.scm 800 */
																				((bool_t) 1);
																			}
																	}
																}
																{
																	long BgL_iz00_6887;

																	BgL_iz00_6887 = (BgL_iz00_2032 + 1L);
																	BgL_iz00_2032 = BgL_iz00_6887;
																	goto BgL_zc3z04anonymousza31760ze3z87_2033;
																}
															}
														else
															{	/* Llib/weakhash.scm 800 */
																((bool_t) 0);
															}
													}
													{	/* Llib/weakhash.scm 800 */
														obj_t BgL_vz00_3794;

														BgL_vz00_3794 = BgL_countz00_2024;
														{	/* Llib/weakhash.scm 800 */
															obj_t BgL_xz00_4255;

															{	/* Llib/weakhash.scm 800 */
																int BgL_tmpz00_6889;

																BgL_tmpz00_6889 = (int) (0L);
																BgL_xz00_4255 =
																	STRUCT_SET(BgL_tablez00_126, BgL_tmpz00_6889,
																	BgL_vz00_3794);
															}
															return BUNSPEC;
														}
													}
												}
											}
										}
									}
								}
							else
								{	/* Llib/weakhash.scm 800 */
									obj_t BgL_oldzd2buckszd2_2054;

									BgL_oldzd2buckszd2_2054 =
										STRUCT_REF(BgL_tablez00_126, (int) (2L));
									{	/* Llib/weakhash.scm 800 */
										long BgL_newzd2buckszd2lenz00_2056;

										BgL_newzd2buckszd2lenz00_2056 =
											(2L * VECTOR_LENGTH(((obj_t) BgL_oldzd2buckszd2_2054)));
										{	/* Llib/weakhash.scm 800 */
											obj_t BgL_newzd2buckszd2_2057;

											BgL_newzd2buckszd2_2057 =
												make_vector(BgL_newzd2buckszd2lenz00_2056, BNIL);
											{	/* Llib/weakhash.scm 800 */
												obj_t BgL_countz00_2058;

												BgL_countz00_2058 =
													STRUCT_REF(BgL_tablez00_126, (int) (0L));
												{	/* Llib/weakhash.scm 800 */

													{	/* Llib/weakhash.scm 800 */
														obj_t BgL_nmaxz00_2059;

														{	/* Llib/weakhash.scm 800 */
															obj_t BgL_a1141z00_2062;

															BgL_a1141z00_2062 =
																STRUCT_REF(BgL_tablez00_126, (int) (1L));
															{	/* Llib/weakhash.scm 800 */
																obj_t BgL_b1142z00_2063;

																BgL_b1142z00_2063 =
																	STRUCT_REF(BgL_tablez00_126, (int) (7L));
																{	/* Llib/weakhash.scm 800 */

																	{	/* Llib/weakhash.scm 800 */
																		bool_t BgL_test2530z00_6904;

																		if (INTEGERP(BgL_a1141z00_2062))
																			{	/* Llib/weakhash.scm 800 */
																				BgL_test2530z00_6904 =
																					INTEGERP(BgL_b1142z00_2063);
																			}
																		else
																			{	/* Llib/weakhash.scm 800 */
																				BgL_test2530z00_6904 = ((bool_t) 0);
																			}
																		if (BgL_test2530z00_6904)
																			{	/* Llib/weakhash.scm 800 */
																				BgL_nmaxz00_2059 =
																					BINT(
																					((long) CINT(BgL_a1141z00_2062) *
																						(long) CINT(BgL_b1142z00_2063)));
																			}
																		else
																			{	/* Llib/weakhash.scm 800 */
																				BgL_nmaxz00_2059 =
																					BGl_2za2za2zz__r4_numbers_6_5z00
																					(BgL_a1141z00_2062,
																					BgL_b1142z00_2063);
																			}
																	}
																}
															}
														}
														{	/* Llib/weakhash.scm 800 */
															obj_t BgL_arg1772z00_2060;

															if (REALP(BgL_nmaxz00_2059))
																{	/* Llib/weakhash.scm 800 */
																	BgL_arg1772z00_2060 =
																		BINT(
																		(long) (REAL_TO_DOUBLE(BgL_nmaxz00_2059)));
																}
															else
																{	/* Llib/weakhash.scm 800 */
																	BgL_arg1772z00_2060 = BgL_nmaxz00_2059;
																}
															{	/* Llib/weakhash.scm 800 */
																int BgL_tmpz00_6918;

																BgL_tmpz00_6918 = (int) (1L);
																STRUCT_SET(BgL_tablez00_126, BgL_tmpz00_6918,
																	BgL_arg1772z00_2060);
													}}}
													{	/* Llib/weakhash.scm 800 */
														int BgL_tmpz00_6921;

														BgL_tmpz00_6921 = (int) (2L);
														STRUCT_SET(BgL_tablez00_126, BgL_tmpz00_6921,
															BgL_newzd2buckszd2_2057);
													}
													{
														long BgL_iz00_2066;

														BgL_iz00_2066 = 0L;
													BgL_zc3z04anonymousza31775ze3z87_2067:
														if (
															(BgL_iz00_2066 <
																VECTOR_LENGTH(
																	((obj_t) BgL_oldzd2buckszd2_2054))))
															{	/* Llib/weakhash.scm 800 */
																{	/* Llib/weakhash.scm 800 */
																	obj_t BgL_g1145z00_2069;

																	BgL_g1145z00_2069 =
																		VECTOR_REF(
																		((obj_t) BgL_oldzd2buckszd2_2054),
																		BgL_iz00_2066);
																	{
																		obj_t BgL_l1143z00_2071;

																		BgL_l1143z00_2071 = BgL_g1145z00_2069;
																	BgL_zc3z04anonymousza31777ze3z87_2072:
																		if (PAIRP(BgL_l1143z00_2071))
																			{	/* Llib/weakhash.scm 800 */
																				{	/* Llib/weakhash.scm 860 */
																					obj_t BgL_cellz00_2074;

																					BgL_cellz00_2074 =
																						CAR(BgL_l1143z00_2071);
																					{	/* Llib/weakhash.scm 860 */
																						long BgL_hz00_2075;

																						{	/* Llib/weakhash.scm 862 */
																							long BgL_arg1782z00_2078;

																							{	/* Llib/weakhash.scm 862 */
																								obj_t BgL_arg1783z00_2079;

																								BgL_arg1783z00_2079 =
																									CAR(
																									((obj_t) BgL_cellz00_2074));
																								{	/* Llib/weakhash.scm 861 */
																									obj_t BgL_hashnz00_3813;

																									BgL_hashnz00_3813 =
																										STRUCT_REF(BgL_tablez00_126,
																										(int) (4L));
																									if (PROCEDUREP
																										(BgL_hashnz00_3813))
																										{	/* Llib/weakhash.scm 861 */
																											obj_t BgL_arg1268z00_3815;

																											BgL_arg1268z00_3815 =
																												BGL_PROCEDURE_CALL1
																												(BgL_hashnz00_3813,
																												BgL_arg1783z00_2079);
																											{	/* Llib/weakhash.scm 861 */
																												long BgL_nz00_3817;

																												BgL_nz00_3817 =
																													(long)
																													CINT
																													(BgL_arg1268z00_3815);
																												if ((BgL_nz00_3817 <
																														0L))
																													{	/* Llib/weakhash.scm 861 */
																														BgL_arg1782z00_2078
																															=
																															NEG
																															(BgL_nz00_3817);
																													}
																												else
																													{	/* Llib/weakhash.scm 861 */
																														BgL_arg1782z00_2078
																															= BgL_nz00_3817;
																													}
																											}
																										}
																									else
																										{	/* Llib/weakhash.scm 861 */
																											if (
																												(BgL_hashnz00_3813 ==
																													BGl_symbol2173z00zz__weakhashz00))
																												{	/* Llib/weakhash.scm 861 */
																													BgL_arg1782z00_2078 =
																														BGl_getzd2hashnumberzd2persistentz00zz__hashz00
																														(BgL_arg1783z00_2079);
																												}
																											else
																												{	/* Llib/weakhash.scm 861 */
																													BgL_arg1782z00_2078 =
																														BGl_getzd2hashnumberzd2zz__hashz00
																														(BgL_arg1783z00_2079);
																												}
																										}
																								}
																							}
																							{	/* Llib/weakhash.scm 860 */
																								long BgL_n1z00_3821;
																								long BgL_n2z00_3822;

																								BgL_n1z00_3821 =
																									BgL_arg1782z00_2078;
																								BgL_n2z00_3822 =
																									BgL_newzd2buckszd2lenz00_2056;
																								{	/* Llib/weakhash.scm 860 */
																									bool_t BgL_test2538z00_6951;

																									{	/* Llib/weakhash.scm 860 */
																										long BgL_arg1961z00_3824;

																										BgL_arg1961z00_3824 =
																											(((BgL_n1z00_3821) |
																												(BgL_n2z00_3822)) &
																											-2147483648);
																										BgL_test2538z00_6951 =
																											(BgL_arg1961z00_3824 ==
																											0L);
																									}
																									if (BgL_test2538z00_6951)
																										{	/* Llib/weakhash.scm 860 */
																											int32_t
																												BgL_arg1958z00_3825;
																											{	/* Llib/weakhash.scm 860 */
																												int32_t
																													BgL_arg1959z00_3826;
																												int32_t
																													BgL_arg1960z00_3827;
																												BgL_arg1959z00_3826 =
																													(int32_t)
																													(BgL_n1z00_3821);
																												BgL_arg1960z00_3827 =
																													(int32_t)
																													(BgL_n2z00_3822);
																												BgL_arg1958z00_3825 =
																													(BgL_arg1959z00_3826 %
																													BgL_arg1960z00_3827);
																											}
																											{	/* Llib/weakhash.scm 860 */
																												long
																													BgL_arg2069z00_3832;
																												BgL_arg2069z00_3832 =
																													(long)
																													(BgL_arg1958z00_3825);
																												BgL_hz00_2075 =
																													(long)
																													(BgL_arg2069z00_3832);
																										}}
																									else
																										{	/* Llib/weakhash.scm 860 */
																											BgL_hz00_2075 =
																												(BgL_n1z00_3821 %
																												BgL_n2z00_3822);
																										}
																								}
																							}
																						}
																						{	/* Llib/weakhash.scm 868 */
																							obj_t BgL_arg1779z00_2076;

																							BgL_arg1779z00_2076 =
																								MAKE_YOUNG_PAIR
																								(BgL_cellz00_2074,
																								VECTOR_REF
																								(BgL_newzd2buckszd2_2057,
																									BgL_hz00_2075));
																							VECTOR_SET
																								(BgL_newzd2buckszd2_2057,
																								BgL_hz00_2075,
																								BgL_arg1779z00_2076);
																						}
																					}
																				}
																				{
																					obj_t BgL_l1143z00_6963;

																					BgL_l1143z00_6963 =
																						CDR(BgL_l1143z00_2071);
																					BgL_l1143z00_2071 = BgL_l1143z00_6963;
																					goto
																						BgL_zc3z04anonymousza31777ze3z87_2072;
																				}
																			}
																		else
																			{	/* Llib/weakhash.scm 800 */
																				((bool_t) 1);
																			}
																	}
																}
																{
																	long BgL_iz00_6965;

																	BgL_iz00_6965 = (BgL_iz00_2066 + 1L);
																	BgL_iz00_2066 = BgL_iz00_6965;
																	goto BgL_zc3z04anonymousza31775ze3z87_2067;
																}
															}
														else
															{	/* Llib/weakhash.scm 800 */
																((bool_t) 0);
															}
													}
													{	/* Llib/weakhash.scm 800 */
														obj_t BgL_xz00_4256;

														{	/* Llib/weakhash.scm 800 */
															int BgL_tmpz00_6967;

															BgL_tmpz00_6967 = (int) (0L);
															BgL_xz00_4256 =
																STRUCT_SET(BgL_tablez00_126, BgL_tmpz00_6967,
																BgL_countz00_2058);
														}
														return BUNSPEC;
													}
												}
											}
										}
									}
								}
						}
				}
		}

	}



/* weak-hashtable-expand! */
	BGL_EXPORTED_DEF obj_t
		BGl_weakzd2hashtablezd2expandz12z12zz__weakhashz00(obj_t BgL_tablez00_127)
	{
		{	/* Llib/weakhash.scm 876 */
			if (BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(BgL_tablez00_127))
				{	/* Llib/weakhash.scm 877 */
					BGL_TAIL return
						BGl_weakzd2keyszd2hashtablezd2expandz12zc0zz__weakhashz00
						(BgL_tablez00_127);
				}
			else
				{	/* Llib/weakhash.scm 877 */
					BGL_TAIL return
						BGl_weakzd2oldzd2hashtablezd2expandz12zc0zz__weakhashz00
						(BgL_tablez00_127);
				}
		}

	}



/* &weak-hashtable-expand! */
	obj_t BGl_z62weakzd2hashtablezd2expandz12z70zz__weakhashz00(obj_t
		BgL_envz00_4212, obj_t BgL_tablez00_4213)
	{
		{	/* Llib/weakhash.scm 876 */
			{	/* Llib/weakhash.scm 877 */
				obj_t BgL_auxz00_6974;

				if (STRUCTP(BgL_tablez00_4213))
					{	/* Llib/weakhash.scm 877 */
						BgL_auxz00_6974 = BgL_tablez00_4213;
					}
				else
					{
						obj_t BgL_auxz00_6977;

						BgL_auxz00_6977 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2161z00zz__weakhashz00,
							BINT(34257L), BGl_string2183z00zz__weakhashz00,
							BGl_string2163z00zz__weakhashz00, BgL_tablez00_4213);
						FAILURE(BgL_auxz00_6977, BFALSE, BFALSE);
					}
				return
					BGl_weakzd2hashtablezd2expandz12z12zz__weakhashz00(BgL_auxz00_6974);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__weakhashz00(void)
	{
		{	/* Llib/weakhash.scm 18 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__weakhashz00(void)
	{
		{	/* Llib/weakhash.scm 18 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__weakhashz00(void)
	{
		{	/* Llib/weakhash.scm 18 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__weakhashz00(void)
	{
		{	/* Llib/weakhash.scm 18 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string2184z00zz__weakhashz00));
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L,
				BSTRING_TO_STRING(BGl_string2184z00zz__weakhashz00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string2184z00zz__weakhashz00));
			BGl_modulezd2initializa7ationz75zz__hashz00(482391669L,
				BSTRING_TO_STRING(BGl_string2184z00zz__weakhashz00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string2184z00zz__weakhashz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string2184z00zz__weakhashz00));
			return
				BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string2184z00zz__weakhashz00));
		}

	}

#ifdef __cplusplus
}
#endif
