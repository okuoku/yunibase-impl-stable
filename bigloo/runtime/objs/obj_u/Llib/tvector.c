/*===========================================================================*/
/*   (Llib/tvector.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Llib/tvector.scm -indent -o objs/obj_u/Llib/tvector.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___TVECTOR_TYPE_DEFINITIONS
#define BGL___TVECTOR_TYPE_DEFINITIONS
#endif													// BGL___TVECTOR_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern bool_t BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	BGL_EXPORTED_DECL int BGl_tvectorzd2lengthzd2zz__tvectorz00(obj_t);
	static obj_t BGl_z62tvectorzd2refzb0zz__tvectorz00(obj_t, obj_t);
	static obj_t BGl_z62tvectorzf3z91zz__tvectorz00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__tvectorz00 = BUNSPEC;
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__tvectorz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bignumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__hashz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_EXPORTED_DECL obj_t BGl_vectorzd2ze3tvectorz31zz__tvectorz00(obj_t,
		obj_t);
	static obj_t BGl_z62vectorzd2ze3tvectorz53zz__tvectorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zz__tvectorz00(void);
	static obj_t BGl_z62getzd2tvectorzd2descriptorz62zz__tvectorz00(obj_t, obj_t);
	extern obj_t create_struct(obj_t, int);
	extern obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__tvectorz00(void);
	static obj_t BGl_genericzd2initzd2zz__tvectorz00(void);
	extern long bgl_list_length(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_tvectorzd2ze3vectorz31zz__tvectorz00(obj_t);
	static obj_t BGl_z62tvectorzd2ze3vectorz53zz__tvectorz00(obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__tvectorz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__tvectorz00(void);
	static obj_t BGl_objectzd2initzd2zz__tvectorz00(void);
	BGL_EXPORTED_DECL obj_t BGl_declarezd2tvectorz12zc0zz__tvectorz00(char *,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_tvectorzd2refzd2zz__tvectorz00(obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_z62tvectorzd2lengthzb0zz__tvectorz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t get_tvector_descriptor(obj_t);
	static obj_t BGl_methodzd2initzd2zz__tvectorz00(void);
	BGL_EXPORTED_DECL bool_t BGl_tvectorzf3zf3zz__tvectorz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_listzd2ze3tvectorz31zz__tvectorz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_tvectorzd2idzd2zz__tvectorz00(obj_t);
	static obj_t BGl_symbol1645z00zz__tvectorz00 = BUNSPEC;
	static obj_t BGl_symbol1647z00zz__tvectorz00 = BUNSPEC;
	static obj_t BGl_symbol1649z00zz__tvectorz00 = BUNSPEC;
	static obj_t BGl_z62declarezd2tvectorz12za2zz__tvectorz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_bigloozd2casezd2sensitivityz00zz__readerz00(void);
	extern obj_t BGl_stringzd2upcasezd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_z62tvectorzd2idzb0zz__tvectorz00(obj_t, obj_t);
	extern obj_t BGl_stringzd2downcasezd2zz__r4_strings_6_7z00(obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_za2tvectorzd2tableza2zd2zz__tvectorz00 = BUNSPEC;
	static obj_t BGl_z62listzd2ze3tvectorz53zz__tvectorz00(obj_t, obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_tvectorzf3zd2envz21zz__tvectorz00,
		BgL_bgl_za762tvectorza7f3za7911666za7, BGl_z62tvectorzf3z91zz__tvectorz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_listzd2ze3tvectorzd2envze3zz__tvectorz00,
		BgL_bgl_za762listza7d2za7e3tve1667za7,
		BGl_z62listzd2ze3tvectorz53zz__tvectorz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_vectorzd2ze3tvectorzd2envze3zz__tvectorz00,
		BgL_bgl_za762vectorza7d2za7e3t1668za7,
		BGl_z62vectorzd2ze3tvectorz53zz__tvectorz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_tvectorzd2lengthzd2envz00zz__tvectorz00,
		BgL_bgl_za762tvectorza7d2len1669z00,
		BGl_z62tvectorzd2lengthzb0zz__tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1639z00zz__tvectorz00,
		BgL_bgl_string1639za700za7za7_1670za7,
		"/tmp/bigloo/runtime/Llib/tvector.scm", 36);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_tvectorzd2ze3vectorzd2envze3zz__tvectorz00,
		BgL_bgl_za762tvectorza7d2za7e31671za7,
		BGl_z62tvectorzd2ze3vectorz53zz__tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1640z00zz__tvectorz00,
		BgL_bgl_string1640za700za7za7_1672za7, "&tvector-length", 15);
	      DEFINE_STRING(BGl_string1641z00zz__tvectorz00,
		BgL_bgl_string1641za700za7za7_1673za7, "tvector", 7);
	      DEFINE_STRING(BGl_string1642z00zz__tvectorz00,
		BgL_bgl_string1642za700za7za7_1674za7, "&tvector-id", 11);
	      DEFINE_STRING(BGl_string1643z00zz__tvectorz00,
		BgL_bgl_string1643za700za7za7_1675za7, "&get-tvector-descriptor", 23);
	      DEFINE_STRING(BGl_string1644z00zz__tvectorz00,
		BgL_bgl_string1644za700za7za7_1676za7, "symbol", 6);
	      DEFINE_STRING(BGl_string1646z00zz__tvectorz00,
		BgL_bgl_string1646za700za7za7_1677za7, "upcase", 6);
	      DEFINE_STRING(BGl_string1648z00zz__tvectorz00,
		BgL_bgl_string1648za700za7za7_1678za7, "downcase", 8);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2tvectorzd2descriptorzd2envzd2zz__tvectorz00,
		BgL_bgl_za762getza7d2tvector1679z00,
		BGl_z62getzd2tvectorzd2descriptorz62zz__tvectorz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_declarezd2tvectorz12zd2envz12zz__tvectorz00,
		BgL_bgl_za762declareza7d2tve1680z00,
		BGl_z62declarezd2tvectorz12za2zz__tvectorz00, 0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string1650z00zz__tvectorz00,
		BgL_bgl_string1650za700za7za7_1681za7, "tvect-descr", 11);
	      DEFINE_STRING(BGl_string1651z00zz__tvectorz00,
		BgL_bgl_string1651za700za7za7_1682za7, "&declare-tvector!", 17);
	      DEFINE_STRING(BGl_string1652z00zz__tvectorz00,
		BgL_bgl_string1652za700za7za7_1683za7, "bstring", 7);
	      DEFINE_STRING(BGl_string1653z00zz__tvectorz00,
		BgL_bgl_string1653za700za7za7_1684za7, "procedure", 9);
	      DEFINE_STRING(BGl_string1654z00zz__tvectorz00,
		BgL_bgl_string1654za700za7za7_1685za7, "&tvector-ref", 12);
	      DEFINE_STRING(BGl_string1655z00zz__tvectorz00,
		BgL_bgl_string1655za700za7za7_1686za7, "list->tvector", 13);
	      DEFINE_STRING(BGl_string1656z00zz__tvectorz00,
		BgL_bgl_string1656za700za7za7_1687za7, "Unable to convert to such tvector",
		33);
	      DEFINE_STRING(BGl_string1657z00zz__tvectorz00,
		BgL_bgl_string1657za700za7za7_1688za7, "Undeclared tvector", 18);
	      DEFINE_STRING(BGl_string1658z00zz__tvectorz00,
		BgL_bgl_string1658za700za7za7_1689za7, "&list->tvector", 14);
	      DEFINE_STRING(BGl_string1659z00zz__tvectorz00,
		BgL_bgl_string1659za700za7za7_1690za7, "pair-nil", 8);
	      DEFINE_STRING(BGl_string1660z00zz__tvectorz00,
		BgL_bgl_string1660za700za7za7_1691za7, "vector->tvector", 15);
	      DEFINE_STRING(BGl_string1661z00zz__tvectorz00,
		BgL_bgl_string1661za700za7za7_1692za7, "&vector->tvector", 16);
	      DEFINE_STRING(BGl_string1662z00zz__tvectorz00,
		BgL_bgl_string1662za700za7za7_1693za7, "vector", 6);
	      DEFINE_STRING(BGl_string1663z00zz__tvectorz00,
		BgL_bgl_string1663za700za7za7_1694za7, "tvector->vector", 15);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_tvectorzd2refzd2envz00zz__tvectorz00,
		BgL_bgl_za762tvectorza7d2ref1695z00, BGl_z62tvectorzd2refzb0zz__tvectorz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1664z00zz__tvectorz00,
		BgL_bgl_string1664za700za7za7_1696za7, "&tvector->vector", 16);
	      DEFINE_STRING(BGl_string1665z00zz__tvectorz00,
		BgL_bgl_string1665za700za7za7_1697za7, "__tvector", 9);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_tvectorzd2idzd2envz00zz__tvectorz00,
		BgL_bgl_za762tvectorza7d2idza71698za7, BGl_z62tvectorzd2idzb0zz__tvectorz00,
		0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__tvectorz00));
		     ADD_ROOT((void *) (&BGl_symbol1645z00zz__tvectorz00));
		     ADD_ROOT((void *) (&BGl_symbol1647z00zz__tvectorz00));
		     ADD_ROOT((void *) (&BGl_symbol1649z00zz__tvectorz00));
		     ADD_ROOT((void *) (&BGl_za2tvectorzd2tableza2zd2zz__tvectorz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__tvectorz00(long
		BgL_checksumz00_1935, char *BgL_fromz00_1936)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__tvectorz00))
				{
					BGl_requirezd2initializa7ationz75zz__tvectorz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__tvectorz00();
					BGl_cnstzd2initzd2zz__tvectorz00();
					BGl_importedzd2moduleszd2initz00zz__tvectorz00();
					return BGl_toplevelzd2initzd2zz__tvectorz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__tvectorz00(void)
	{
		{	/* Llib/tvector.scm 14 */
			BGl_symbol1645z00zz__tvectorz00 =
				bstring_to_symbol(BGl_string1646z00zz__tvectorz00);
			BGl_symbol1647z00zz__tvectorz00 =
				bstring_to_symbol(BGl_string1648z00zz__tvectorz00);
			return (BGl_symbol1649z00zz__tvectorz00 =
				bstring_to_symbol(BGl_string1650z00zz__tvectorz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__tvectorz00(void)
	{
		{	/* Llib/tvector.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__tvectorz00(void)
	{
		{	/* Llib/tvector.scm 14 */
			return (BGl_za2tvectorzd2tableza2zd2zz__tvectorz00 = BNIL, BUNSPEC);
		}

	}



/* tvector? */
	BGL_EXPORTED_DEF bool_t BGl_tvectorzf3zf3zz__tvectorz00(obj_t BgL_objz00_3)
	{
		{	/* Llib/tvector.scm 92 */
			return TVECTORP(BgL_objz00_3);
		}

	}



/* &tvector? */
	obj_t BGl_z62tvectorzf3z91zz__tvectorz00(obj_t BgL_envz00_1886,
		obj_t BgL_objz00_1887)
	{
		{	/* Llib/tvector.scm 92 */
			return BBOOL(BGl_tvectorzf3zf3zz__tvectorz00(BgL_objz00_1887));
		}

	}



/* tvector-length */
	BGL_EXPORTED_DEF int BGl_tvectorzd2lengthzd2zz__tvectorz00(obj_t BgL_objz00_4)
	{
		{	/* Llib/tvector.scm 98 */
			return TVECTOR_LENGTH(BgL_objz00_4);
		}

	}



/* &tvector-length */
	obj_t BGl_z62tvectorzd2lengthzb0zz__tvectorz00(obj_t BgL_envz00_1888,
		obj_t BgL_objz00_1889)
	{
		{	/* Llib/tvector.scm 98 */
			{	/* Llib/tvector.scm 99 */
				int BgL_tmpz00_1952;

				{	/* Llib/tvector.scm 99 */
					obj_t BgL_auxz00_1953;

					if (TVECTORP(BgL_objz00_1889))
						{	/* Llib/tvector.scm 99 */
							BgL_auxz00_1953 = BgL_objz00_1889;
						}
					else
						{
							obj_t BgL_auxz00_1956;

							BgL_auxz00_1956 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1639z00zz__tvectorz00,
								BINT(3558L), BGl_string1640z00zz__tvectorz00,
								BGl_string1641z00zz__tvectorz00, BgL_objz00_1889);
							FAILURE(BgL_auxz00_1956, BFALSE, BFALSE);
						}
					BgL_tmpz00_1952 =
						BGl_tvectorzd2lengthzd2zz__tvectorz00(BgL_auxz00_1953);
				}
				return BINT(BgL_tmpz00_1952);
			}
		}

	}



/* tvector-id */
	BGL_EXPORTED_DEF obj_t BGl_tvectorzd2idzd2zz__tvectorz00(obj_t BgL_tvectz00_5)
	{
		{	/* Llib/tvector.scm 104 */
			return STRUCT_REF(TVECTOR_DESCR(BgL_tvectz00_5), (int) (0L));
		}

	}



/* &tvector-id */
	obj_t BGl_z62tvectorzd2idzb0zz__tvectorz00(obj_t BgL_envz00_1890,
		obj_t BgL_tvectz00_1891)
	{
		{	/* Llib/tvector.scm 104 */
			{	/* Llib/tvector.scm 105 */
				obj_t BgL_auxz00_1965;

				if (TVECTORP(BgL_tvectz00_1891))
					{	/* Llib/tvector.scm 105 */
						BgL_auxz00_1965 = BgL_tvectz00_1891;
					}
				else
					{
						obj_t BgL_auxz00_1968;

						BgL_auxz00_1968 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1639z00zz__tvectorz00,
							BINT(3850L), BGl_string1642z00zz__tvectorz00,
							BGl_string1641z00zz__tvectorz00, BgL_tvectz00_1891);
						FAILURE(BgL_auxz00_1968, BFALSE, BFALSE);
					}
				return BGl_tvectorzd2idzd2zz__tvectorz00(BgL_auxz00_1965);
			}
		}

	}



/* get-tvector-descriptor */
	BGL_EXPORTED_DEF obj_t get_tvector_descriptor(obj_t BgL_idz00_24)
	{
		{	/* Llib/tvector.scm 127 */
			if (NULLP(BGl_za2tvectorzd2tableza2zd2zz__tvectorz00))
				{	/* Llib/tvector.scm 128 */
					return BFALSE;
				}
			else
				{	/* Llib/tvector.scm 129 */
					obj_t BgL_cellz00_1625;

					BgL_cellz00_1625 =
						BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_idz00_24,
						BGl_za2tvectorzd2tableza2zd2zz__tvectorz00);
					if (PAIRP(BgL_cellz00_1625))
						{	/* Llib/tvector.scm 130 */
							return CDR(BgL_cellz00_1625);
						}
					else
						{	/* Llib/tvector.scm 130 */
							return BFALSE;
						}
				}
		}

	}



/* &get-tvector-descriptor */
	obj_t BGl_z62getzd2tvectorzd2descriptorz62zz__tvectorz00(obj_t
		BgL_envz00_1892, obj_t BgL_idz00_1893)
	{
		{	/* Llib/tvector.scm 127 */
			{	/* Llib/tvector.scm 128 */
				obj_t BgL_auxz00_1979;

				if (SYMBOLP(BgL_idz00_1893))
					{	/* Llib/tvector.scm 128 */
						BgL_auxz00_1979 = BgL_idz00_1893;
					}
				else
					{
						obj_t BgL_auxz00_1982;

						BgL_auxz00_1982 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1639z00zz__tvectorz00,
							BINT(5101L), BGl_string1643z00zz__tvectorz00,
							BGl_string1644z00zz__tvectorz00, BgL_idz00_1893);
						FAILURE(BgL_auxz00_1982, BFALSE, BFALSE);
					}
				return get_tvector_descriptor(BgL_auxz00_1979);
			}
		}

	}



/* declare-tvector! */
	BGL_EXPORTED_DEF obj_t BGl_declarezd2tvectorz12zc0zz__tvectorz00(char
		*BgL_idz00_25, obj_t BgL_allocatez00_26, obj_t BgL_refz00_27,
		obj_t BgL_setz00_28)
	{
		{	/* Llib/tvector.scm 141 */
			{	/* Llib/tvector.scm 142 */
				obj_t BgL_idz00_1144;

				{	/* Llib/tvector.scm 142 */
					obj_t BgL_arg1188z00_1149;

					{	/* Llib/tvector.scm 142 */
						obj_t BgL_casezd2valuezd2_1150;

						BgL_casezd2valuezd2_1150 =
							BGl_bigloozd2casezd2sensitivityz00zz__readerz00();
						if ((BgL_casezd2valuezd2_1150 == BGl_symbol1645z00zz__tvectorz00))
							{	/* Llib/tvector.scm 142 */
								BgL_arg1188z00_1149 =
									BGl_stringzd2upcasezd2zz__r4_strings_6_7z00(string_to_bstring
									(BgL_idz00_25));
							}
						else
							{	/* Llib/tvector.scm 142 */
								if (
									(BgL_casezd2valuezd2_1150 == BGl_symbol1647z00zz__tvectorz00))
									{	/* Llib/tvector.scm 142 */
										BgL_arg1188z00_1149 =
											BGl_stringzd2downcasezd2zz__r4_strings_6_7z00
											(string_to_bstring(BgL_idz00_25));
									}
								else
									{	/* Llib/tvector.scm 142 */
										BgL_arg1188z00_1149 = string_to_bstring(BgL_idz00_25);
									}
							}
					}
					BgL_idz00_1144 = bstring_to_symbol(BgL_arg1188z00_1149);
				}
				{	/* Llib/tvector.scm 142 */
					obj_t BgL_oldz00_1145;

					if (NULLP(BGl_za2tvectorzd2tableza2zd2zz__tvectorz00))
						{	/* Llib/tvector.scm 128 */
							BgL_oldz00_1145 = BFALSE;
						}
					else
						{	/* Llib/tvector.scm 129 */
							obj_t BgL_cellz00_1634;

							BgL_cellz00_1634 =
								BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_idz00_1144,
								BGl_za2tvectorzd2tableza2zd2zz__tvectorz00);
							if (PAIRP(BgL_cellz00_1634))
								{	/* Llib/tvector.scm 130 */
									BgL_oldz00_1145 = CDR(BgL_cellz00_1634);
								}
							else
								{	/* Llib/tvector.scm 130 */
									BgL_oldz00_1145 = BFALSE;
								}
						}
					{	/* Llib/tvector.scm 149 */

						{	/* Llib/tvector.scm 150 */
							bool_t BgL_test1709z00_2004;

							if (STRUCTP(BgL_oldz00_1145))
								{	/* Llib/tvector.scm 113 */
									BgL_test1709z00_2004 =
										(STRUCT_KEY(BgL_oldz00_1145) ==
										BGl_symbol1649z00zz__tvectorz00);
								}
							else
								{	/* Llib/tvector.scm 113 */
									BgL_test1709z00_2004 = ((bool_t) 0);
								}
							if (BgL_test1709z00_2004)
								{	/* Llib/tvector.scm 150 */
									return BgL_oldz00_1145;
								}
							else
								{	/* Llib/tvector.scm 151 */
									obj_t BgL_newz00_1147;

									{	/* Llib/tvector.scm 113 */
										obj_t BgL_newz00_1642;

										BgL_newz00_1642 =
											create_struct(BGl_symbol1649z00zz__tvectorz00,
											(int) (4L));
										{	/* Llib/tvector.scm 113 */
											int BgL_tmpz00_2011;

											BgL_tmpz00_2011 = (int) (3L);
											STRUCT_SET(BgL_newz00_1642, BgL_tmpz00_2011,
												BgL_setz00_28);
										}
										{	/* Llib/tvector.scm 113 */
											int BgL_tmpz00_2014;

											BgL_tmpz00_2014 = (int) (2L);
											STRUCT_SET(BgL_newz00_1642, BgL_tmpz00_2014,
												BgL_refz00_27);
										}
										{	/* Llib/tvector.scm 113 */
											int BgL_tmpz00_2017;

											BgL_tmpz00_2017 = (int) (1L);
											STRUCT_SET(BgL_newz00_1642, BgL_tmpz00_2017,
												BgL_allocatez00_26);
										}
										{	/* Llib/tvector.scm 113 */
											int BgL_tmpz00_2020;

											BgL_tmpz00_2020 = (int) (0L);
											STRUCT_SET(BgL_newz00_1642, BgL_tmpz00_2020,
												BgL_idz00_1144);
										}
										BgL_newz00_1147 = BgL_newz00_1642;
									}
									{	/* Llib/tvector.scm 152 */
										obj_t BgL_arg1187z00_1148;

										BgL_arg1187z00_1148 =
											MAKE_YOUNG_PAIR(BgL_idz00_1144, BgL_newz00_1147);
										BGl_za2tvectorzd2tableza2zd2zz__tvectorz00 =
											MAKE_YOUNG_PAIR(BgL_arg1187z00_1148,
											BGl_za2tvectorzd2tableza2zd2zz__tvectorz00);
									}
									return BgL_newz00_1147;
								}
						}
					}
				}
			}
		}

	}



/* &declare-tvector! */
	obj_t BGl_z62declarezd2tvectorz12za2zz__tvectorz00(obj_t BgL_envz00_1894,
		obj_t BgL_idz00_1895, obj_t BgL_allocatez00_1896, obj_t BgL_refz00_1897,
		obj_t BgL_setz00_1898)
	{
		{	/* Llib/tvector.scm 141 */
			{	/* Llib/tvector.scm 142 */
				obj_t BgL_auxz00_2034;
				char *BgL_auxz00_2025;

				if (PROCEDUREP(BgL_allocatez00_1896))
					{	/* Llib/tvector.scm 142 */
						BgL_auxz00_2034 = BgL_allocatez00_1896;
					}
				else
					{
						obj_t BgL_auxz00_2037;

						BgL_auxz00_2037 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1639z00zz__tvectorz00,
							BINT(5790L), BGl_string1651z00zz__tvectorz00,
							BGl_string1653z00zz__tvectorz00, BgL_allocatez00_1896);
						FAILURE(BgL_auxz00_2037, BFALSE, BFALSE);
					}
				{	/* Llib/tvector.scm 142 */
					obj_t BgL_tmpz00_2026;

					if (STRINGP(BgL_idz00_1895))
						{	/* Llib/tvector.scm 142 */
							BgL_tmpz00_2026 = BgL_idz00_1895;
						}
					else
						{
							obj_t BgL_auxz00_2029;

							BgL_auxz00_2029 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1639z00zz__tvectorz00,
								BINT(5790L), BGl_string1651z00zz__tvectorz00,
								BGl_string1652z00zz__tvectorz00, BgL_idz00_1895);
							FAILURE(BgL_auxz00_2029, BFALSE, BFALSE);
						}
					BgL_auxz00_2025 = BSTRING_TO_STRING(BgL_tmpz00_2026);
				}
				return
					BGl_declarezd2tvectorz12zc0zz__tvectorz00(BgL_auxz00_2025,
					BgL_auxz00_2034, BgL_refz00_1897, BgL_setz00_1898);
			}
		}

	}



/* tvector-ref */
	BGL_EXPORTED_DEF obj_t BGl_tvectorzd2refzd2zz__tvectorz00(obj_t
		BgL_tvectorz00_29)
	{
		{	/* Llib/tvector.scm 159 */
			return STRUCT_REF(TVECTOR_DESCR(BgL_tvectorz00_29), (int) (2L));
		}

	}



/* &tvector-ref */
	obj_t BGl_z62tvectorzd2refzb0zz__tvectorz00(obj_t BgL_envz00_1899,
		obj_t BgL_tvectorz00_1900)
	{
		{	/* Llib/tvector.scm 159 */
			{	/* Llib/tvector.scm 160 */
				obj_t BgL_auxz00_2045;

				if (TVECTORP(BgL_tvectorz00_1900))
					{	/* Llib/tvector.scm 160 */
						BgL_auxz00_2045 = BgL_tvectorz00_1900;
					}
				else
					{
						obj_t BgL_auxz00_2048;

						BgL_auxz00_2048 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1639z00zz__tvectorz00,
							BINT(6511L), BGl_string1654z00zz__tvectorz00,
							BGl_string1641z00zz__tvectorz00, BgL_tvectorz00_1900);
						FAILURE(BgL_auxz00_2048, BFALSE, BFALSE);
					}
				return BGl_tvectorzd2refzd2zz__tvectorz00(BgL_auxz00_2045);
			}
		}

	}



/* list->tvector */
	BGL_EXPORTED_DEF obj_t BGl_listzd2ze3tvectorz31zz__tvectorz00(obj_t
		BgL_idz00_30, obj_t BgL_lz00_31)
	{
		{	/* Llib/tvector.scm 165 */
			{	/* Llib/tvector.scm 166 */
				obj_t BgL_descrz00_1156;

				if (NULLP(BGl_za2tvectorzd2tableza2zd2zz__tvectorz00))
					{	/* Llib/tvector.scm 128 */
						BgL_descrz00_1156 = BFALSE;
					}
				else
					{	/* Llib/tvector.scm 129 */
						obj_t BgL_cellz00_1651;

						BgL_cellz00_1651 =
							BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_idz00_30,
							BGl_za2tvectorzd2tableza2zd2zz__tvectorz00);
						if (PAIRP(BgL_cellz00_1651))
							{	/* Llib/tvector.scm 130 */
								BgL_descrz00_1156 = CDR(BgL_cellz00_1651);
							}
						else
							{	/* Llib/tvector.scm 130 */
								BgL_descrz00_1156 = BFALSE;
							}
					}
				if (CBOOL(BgL_descrz00_1156))
					{	/* Llib/tvector.scm 169 */
						obj_t BgL_allocatez00_1157;
						obj_t BgL_setz00_1158;

						BgL_allocatez00_1157 =
							STRUCT_REF(((obj_t) BgL_descrz00_1156), (int) (1L));
						BgL_setz00_1158 =
							STRUCT_REF(((obj_t) BgL_descrz00_1156), (int) (3L));
						if (PROCEDUREP(BgL_setz00_1158))
							{	/* Llib/tvector.scm 175 */
								long BgL_lenz00_1160;

								BgL_lenz00_1160 = bgl_list_length(BgL_lz00_31);
								{	/* Llib/tvector.scm 175 */
									obj_t BgL_tvecz00_1161;

									BgL_tvecz00_1161 =
										BGL_PROCEDURE_CALL1(BgL_allocatez00_1157,
										BINT(BgL_lenz00_1160));
									{	/* Llib/tvector.scm 176 */

										{
											obj_t BgL_lz00_1672;
											long BgL_iz00_1673;

											BgL_lz00_1672 = BgL_lz00_31;
											BgL_iz00_1673 = 0L;
										BgL_loopz00_1671:
											if (NULLP(BgL_lz00_1672))
												{	/* Llib/tvector.scm 179 */
													return BgL_tvecz00_1161;
												}
											else
												{	/* Llib/tvector.scm 179 */
													{	/* Llib/tvector.scm 182 */
														obj_t BgL_arg1198z00_1679;

														BgL_arg1198z00_1679 = CAR(((obj_t) BgL_lz00_1672));
														BGL_PROCEDURE_CALL3(BgL_setz00_1158,
															BgL_tvecz00_1161, BINT(BgL_iz00_1673),
															BgL_arg1198z00_1679);
													}
													{	/* Llib/tvector.scm 183 */
														obj_t BgL_arg1199z00_1680;
														long BgL_arg1200z00_1681;

														BgL_arg1199z00_1680 = CDR(((obj_t) BgL_lz00_1672));
														BgL_arg1200z00_1681 = (BgL_iz00_1673 + 1L);
														{
															long BgL_iz00_2090;
															obj_t BgL_lz00_2089;

															BgL_lz00_2089 = BgL_arg1199z00_1680;
															BgL_iz00_2090 = BgL_arg1200z00_1681;
															BgL_iz00_1673 = BgL_iz00_2090;
															BgL_lz00_1672 = BgL_lz00_2089;
															goto BgL_loopz00_1671;
														}
													}
												}
										}
									}
								}
							}
						else
							{	/* Llib/tvector.scm 171 */
								return
									BGl_errorz00zz__errorz00(BGl_string1655z00zz__tvectorz00,
									BGl_string1656z00zz__tvectorz00, BgL_idz00_30);
							}
					}
				else
					{	/* Llib/tvector.scm 167 */
						return
							BGl_errorz00zz__errorz00(BGl_string1655z00zz__tvectorz00,
							BGl_string1657z00zz__tvectorz00, BgL_idz00_30);
					}
			}
		}

	}



/* &list->tvector */
	obj_t BGl_z62listzd2ze3tvectorz53zz__tvectorz00(obj_t BgL_envz00_1901,
		obj_t BgL_idz00_1902, obj_t BgL_lz00_1903)
	{
		{	/* Llib/tvector.scm 165 */
			{	/* Llib/tvector.scm 166 */
				obj_t BgL_auxz00_2100;
				obj_t BgL_auxz00_2093;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_lz00_1903))
					{	/* Llib/tvector.scm 166 */
						BgL_auxz00_2100 = BgL_lz00_1903;
					}
				else
					{
						obj_t BgL_auxz00_2103;

						BgL_auxz00_2103 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1639z00zz__tvectorz00,
							BINT(6812L), BGl_string1658z00zz__tvectorz00,
							BGl_string1659z00zz__tvectorz00, BgL_lz00_1903);
						FAILURE(BgL_auxz00_2103, BFALSE, BFALSE);
					}
				if (SYMBOLP(BgL_idz00_1902))
					{	/* Llib/tvector.scm 166 */
						BgL_auxz00_2093 = BgL_idz00_1902;
					}
				else
					{
						obj_t BgL_auxz00_2096;

						BgL_auxz00_2096 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1639z00zz__tvectorz00,
							BINT(6812L), BGl_string1658z00zz__tvectorz00,
							BGl_string1644z00zz__tvectorz00, BgL_idz00_1902);
						FAILURE(BgL_auxz00_2096, BFALSE, BFALSE);
					}
				return
					BGl_listzd2ze3tvectorz31zz__tvectorz00(BgL_auxz00_2093,
					BgL_auxz00_2100);
			}
		}

	}



/* vector->tvector */
	BGL_EXPORTED_DEF obj_t BGl_vectorzd2ze3tvectorz31zz__tvectorz00(obj_t
		BgL_idz00_32, obj_t BgL_vz00_33)
	{
		{	/* Llib/tvector.scm 188 */
			{	/* Llib/tvector.scm 189 */
				obj_t BgL_descrz00_1171;

				if (NULLP(BGl_za2tvectorzd2tableza2zd2zz__tvectorz00))
					{	/* Llib/tvector.scm 128 */
						BgL_descrz00_1171 = BFALSE;
					}
				else
					{	/* Llib/tvector.scm 129 */
						obj_t BgL_cellz00_1687;

						BgL_cellz00_1687 =
							BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_idz00_32,
							BGl_za2tvectorzd2tableza2zd2zz__tvectorz00);
						if (PAIRP(BgL_cellz00_1687))
							{	/* Llib/tvector.scm 130 */
								BgL_descrz00_1171 = CDR(BgL_cellz00_1687);
							}
						else
							{	/* Llib/tvector.scm 130 */
								BgL_descrz00_1171 = BFALSE;
							}
					}
				if (CBOOL(BgL_descrz00_1171))
					{	/* Llib/tvector.scm 192 */
						obj_t BgL_allocatez00_1172;
						obj_t BgL_setz00_1173;

						BgL_allocatez00_1172 =
							STRUCT_REF(((obj_t) BgL_descrz00_1171), (int) (1L));
						BgL_setz00_1173 =
							STRUCT_REF(((obj_t) BgL_descrz00_1171), (int) (3L));
						if (PROCEDUREP(BgL_setz00_1173))
							{	/* Llib/tvector.scm 198 */
								obj_t BgL_tvecz00_1176;

								BgL_tvecz00_1176 =
									BGL_PROCEDURE_CALL1(BgL_allocatez00_1172,
									BINT(VECTOR_LENGTH(BgL_vz00_33)));
								{	/* Llib/tvector.scm 199 */

									{	/* Llib/tvector.scm 200 */
										long BgL_g1040z00_1177;

										BgL_g1040z00_1177 = (VECTOR_LENGTH(BgL_vz00_33) - 1L);
										{
											long BgL_iz00_1179;

											BgL_iz00_1179 = BgL_g1040z00_1177;
										BgL_zc3z04anonymousza31202ze3z87_1180:
											if ((BgL_iz00_1179 == -1L))
												{	/* Llib/tvector.scm 201 */
													return BgL_tvecz00_1176;
												}
											else
												{	/* Llib/tvector.scm 201 */
													{	/* Llib/tvector.scm 204 */
														obj_t BgL_arg1206z00_1182;

														BgL_arg1206z00_1182 =
															VECTOR_REF(BgL_vz00_33, BgL_iz00_1179);
														BGL_PROCEDURE_CALL3(BgL_setz00_1173,
															BgL_tvecz00_1176, BINT(BgL_iz00_1179),
															BgL_arg1206z00_1182);
													}
													{
														long BgL_iz00_2142;

														BgL_iz00_2142 = (BgL_iz00_1179 - 1L);
														BgL_iz00_1179 = BgL_iz00_2142;
														goto BgL_zc3z04anonymousza31202ze3z87_1180;
													}
												}
										}
									}
								}
							}
						else
							{	/* Llib/tvector.scm 194 */
								return
									BGl_errorz00zz__errorz00(BGl_string1660z00zz__tvectorz00,
									BGl_string1656z00zz__tvectorz00, BgL_idz00_32);
							}
					}
				else
					{	/* Llib/tvector.scm 190 */
						return
							BGl_errorz00zz__errorz00(BGl_string1660z00zz__tvectorz00,
							BGl_string1657z00zz__tvectorz00, BgL_idz00_32);
					}
			}
		}

	}



/* &vector->tvector */
	obj_t BGl_z62vectorzd2ze3tvectorz53zz__tvectorz00(obj_t BgL_envz00_1904,
		obj_t BgL_idz00_1905, obj_t BgL_vz00_1906)
	{
		{	/* Llib/tvector.scm 188 */
			{	/* Llib/tvector.scm 189 */
				obj_t BgL_auxz00_2153;
				obj_t BgL_auxz00_2146;

				if (VECTORP(BgL_vz00_1906))
					{	/* Llib/tvector.scm 189 */
						BgL_auxz00_2153 = BgL_vz00_1906;
					}
				else
					{
						obj_t BgL_auxz00_2156;

						BgL_auxz00_2156 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1639z00zz__tvectorz00,
							BINT(7607L), BGl_string1661z00zz__tvectorz00,
							BGl_string1662z00zz__tvectorz00, BgL_vz00_1906);
						FAILURE(BgL_auxz00_2156, BFALSE, BFALSE);
					}
				if (SYMBOLP(BgL_idz00_1905))
					{	/* Llib/tvector.scm 189 */
						BgL_auxz00_2146 = BgL_idz00_1905;
					}
				else
					{
						obj_t BgL_auxz00_2149;

						BgL_auxz00_2149 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1639z00zz__tvectorz00,
							BINT(7607L), BGl_string1661z00zz__tvectorz00,
							BGl_string1644z00zz__tvectorz00, BgL_idz00_1905);
						FAILURE(BgL_auxz00_2149, BFALSE, BFALSE);
					}
				return
					BGl_vectorzd2ze3tvectorz31zz__tvectorz00(BgL_auxz00_2146,
					BgL_auxz00_2153);
			}
		}

	}



/* tvector->vector */
	BGL_EXPORTED_DEF obj_t BGl_tvectorzd2ze3vectorz31zz__tvectorz00(obj_t
		BgL_tvz00_34)
	{
		{	/* Llib/tvector.scm 210 */
			{	/* Llib/tvector.scm 211 */
				obj_t BgL_descrz00_1185;

				BgL_descrz00_1185 = TVECTOR_DESCR(BgL_tvz00_34);
				{	/* Llib/tvector.scm 212 */
					obj_t BgL_refz00_1187;

					BgL_refz00_1187 = STRUCT_REF(((obj_t) BgL_descrz00_1185), (int) (2L));
					if (PROCEDUREP(BgL_refz00_1187))
						{	/* Llib/tvector.scm 218 */
							int BgL_lenz00_1189;

							BgL_lenz00_1189 = TVECTOR_LENGTH(BgL_tvz00_34);
							{	/* Llib/tvector.scm 218 */
								obj_t BgL_vecz00_1190;

								BgL_vecz00_1190 = create_vector((long) (BgL_lenz00_1189));
								{	/* Llib/tvector.scm 219 */

									{	/* Llib/tvector.scm 220 */
										long BgL_g1041z00_1191;

										BgL_g1041z00_1191 = ((long) (BgL_lenz00_1189) - 1L);
										{
											long BgL_iz00_1193;

											BgL_iz00_1193 = BgL_g1041z00_1191;
										BgL_zc3z04anonymousza31210ze3z87_1194:
											if ((BgL_iz00_1193 == -1L))
												{	/* Llib/tvector.scm 221 */
													return BgL_vecz00_1190;
												}
											else
												{	/* Llib/tvector.scm 221 */
													{	/* Llib/tvector.scm 224 */
														obj_t BgL_arg1212z00_1196;

														BgL_arg1212z00_1196 =
															BGL_PROCEDURE_CALL2(BgL_refz00_1187, BgL_tvz00_34,
															BINT(BgL_iz00_1193));
														VECTOR_SET(BgL_vecz00_1190, BgL_iz00_1193,
															BgL_arg1212z00_1196);
													}
													{
														long BgL_iz00_2181;

														BgL_iz00_2181 = (BgL_iz00_1193 - 1L);
														BgL_iz00_1193 = BgL_iz00_2181;
														goto BgL_zc3z04anonymousza31210ze3z87_1194;
													}
												}
										}
									}
								}
							}
						}
					else
						{	/* Llib/tvector.scm 217 */
							obj_t BgL_arg1216z00_1199;

							BgL_arg1216z00_1199 =
								STRUCT_REF(((obj_t) BgL_descrz00_1185), (int) (0L));
							return
								BGl_errorz00zz__errorz00(BGl_string1663z00zz__tvectorz00,
								BGl_string1656z00zz__tvectorz00, BgL_arg1216z00_1199);
						}
				}
			}
		}

	}



/* &tvector->vector */
	obj_t BGl_z62tvectorzd2ze3vectorz53zz__tvectorz00(obj_t BgL_envz00_1907,
		obj_t BgL_tvz00_1908)
	{
		{	/* Llib/tvector.scm 210 */
			{	/* Llib/tvector.scm 211 */
				obj_t BgL_auxz00_2187;

				if (TVECTORP(BgL_tvz00_1908))
					{	/* Llib/tvector.scm 211 */
						BgL_auxz00_2187 = BgL_tvz00_1908;
					}
				else
					{
						obj_t BgL_auxz00_2190;

						BgL_auxz00_2190 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1639z00zz__tvectorz00,
							BINT(8400L), BGl_string1664z00zz__tvectorz00,
							BGl_string1641z00zz__tvectorz00, BgL_tvz00_1908);
						FAILURE(BgL_auxz00_2190, BFALSE, BFALSE);
					}
				return BGl_tvectorzd2ze3vectorz31zz__tvectorz00(BgL_auxz00_2187);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__tvectorz00(void)
	{
		{	/* Llib/tvector.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__tvectorz00(void)
	{
		{	/* Llib/tvector.scm 14 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__tvectorz00(void)
	{
		{	/* Llib/tvector.scm 14 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__tvectorz00(void)
	{
		{	/* Llib/tvector.scm 14 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1665z00zz__tvectorz00));
			BGl_modulezd2initializa7ationz75zz__hashz00(482391669L,
				BSTRING_TO_STRING(BGl_string1665z00zz__tvectorz00));
			BGl_modulezd2initializa7ationz75zz__readerz00(220648206L,
				BSTRING_TO_STRING(BGl_string1665z00zz__tvectorz00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1665z00zz__tvectorz00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string1665z00zz__tvectorz00));
			BGl_modulezd2initializa7ationz75zz__bignumz00(6519932L,
				BSTRING_TO_STRING(BGl_string1665z00zz__tvectorz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string1665z00zz__tvectorz00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string1665z00zz__tvectorz00));
			return
				BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L,
				BSTRING_TO_STRING(BGl_string1665z00zz__tvectorz00));
		}

	}

#ifdef __cplusplus
}
#endif
