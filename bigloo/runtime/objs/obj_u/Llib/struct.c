/*===========================================================================*/
/*   (Llib/struct.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Llib/struct.scm -indent -o objs/obj_u/Llib/struct.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___STRUCTURE_TYPE_DEFINITIONS
#define BGL___STRUCTURE_TYPE_DEFINITIONS
#endif													// BGL___STRUCTURE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t BGl_structzd2refzd2zz__structurez00(obj_t, int);
	static obj_t BGl_z62makezd2structzb0zz__structurez00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__structurez00 = BUNSPEC;
	static obj_t BGl_z62listzd2ze3structz53zz__structurez00(obj_t, obj_t);
	BGL_EXPORTED_DECL int BGl_structzd2lengthzd2zz__structurez00(obj_t);
	static obj_t BGl_z62structzd2refzb0zz__structurez00(obj_t, obj_t, obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_EXPORTED_DECL obj_t BGl_structzd2keyzd2zz__structurez00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_listzd2ze3structz31zz__structurez00(obj_t);
	static obj_t BGl_z62structzd2keyzb0zz__structurez00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__structurez00(void);
	extern long bgl_list_length(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__structurez00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__structurez00(void);
	static obj_t BGl_z62structzd2lengthzb0zz__structurez00(obj_t, obj_t);
	static obj_t BGl_z62structzd2updatez12za2zz__structurez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_recordzf3zf3zz__structurez00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_structzd2setz12zc0zz__structurez00(obj_t, int,
		obj_t);
	static obj_t BGl_symbol1505z00zz__structurez00 = BUNSPEC;
	static obj_t BGl_z62recordzf3z91zz__structurez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_makezd2structzd2zz__structurez00(obj_t, int,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_structzf3zf3zz__structurez00(obj_t);
	static obj_t BGl_z62structzf3z91zz__structurez00(obj_t, obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_z62structzd2ze3listz53zz__structurez00(obj_t, obj_t);
	extern obj_t make_struct(obj_t, int, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_structzd2keyzd2setz12z12zz__structurez00(obj_t,
		obj_t);
	static obj_t BGl_z62structzd2keyzd2setz12z70zz__structurez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_structzd2ze3listz31zz__structurez00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_structzd2updatez12zc0zz__structurez00(obj_t,
		obj_t);
	static obj_t BGl_z62structzd2setz12za2zz__structurez00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_structzd2lengthzd2envz00zz__structurez00,
		BgL_bgl_za762structza7d2leng1511z00,
		BGl_z62structzd2lengthzb0zz__structurez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_structzd2setz12zd2envz12zz__structurez00,
		BgL_bgl_za762structza7d2setza71512za7,
		BGl_z62structzd2setz12za2zz__structurez00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string1500z00zz__structurez00,
		BgL_bgl_string1500za700za7za7_1513za7, "&struct-set!", 12);
	      DEFINE_STRING(BGl_string1501z00zz__structurez00,
		BgL_bgl_string1501za700za7za7_1514za7, "struct-update!", 14);
	      DEFINE_STRING(BGl_string1502z00zz__structurez00,
		BgL_bgl_string1502za700za7za7_1515za7, "Incompatible structures", 23);
	      DEFINE_STRING(BGl_string1503z00zz__structurez00,
		BgL_bgl_string1503za700za7za7_1516za7, "&struct-update!", 15);
	      DEFINE_STRING(BGl_string1504z00zz__structurez00,
		BgL_bgl_string1504za700za7za7_1517za7, "&struct->list", 13);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_listzd2ze3structzd2envze3zz__structurez00,
		BgL_bgl_za762listza7d2za7e3str1518za7,
		BGl_z62listzd2ze3structz53zz__structurez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1506z00zz__structurez00,
		BgL_bgl_string1506za700za7za7_1519za7, "list->struct", 12);
	      DEFINE_STRING(BGl_string1507z00zz__structurez00,
		BgL_bgl_string1507za700za7za7_1520za7, "Illegal struct key", 18);
	      DEFINE_STRING(BGl_string1508z00zz__structurez00,
		BgL_bgl_string1508za700za7za7_1521za7, "&list->struct", 13);
	      DEFINE_STRING(BGl_string1509z00zz__structurez00,
		BgL_bgl_string1509za700za7za7_1522za7, "pair", 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_structzd2updatez12zd2envz12zz__structurez00,
		BgL_bgl_za762structza7d2upda1523z00,
		BGl_z62structzd2updatez12za2zz__structurez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1510z00zz__structurez00,
		BgL_bgl_string1510za700za7za7_1524za7, "__structure", 11);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2structzd2envz00zz__structurez00,
		BgL_bgl_za762makeza7d2struct1525z00,
		BGl_z62makezd2structzb0zz__structurez00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_structzf3zd2envz21zz__structurez00,
		BgL_bgl_za762structza7f3za791za71526z00,
		BGl_z62structzf3z91zz__structurez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1491z00zz__structurez00,
		BgL_bgl_string1491za700za7za7_1527za7,
		"/tmp/bigloo/runtime/Llib/struct.scm", 35);
	      DEFINE_STRING(BGl_string1492z00zz__structurez00,
		BgL_bgl_string1492za700za7za7_1528za7, "&make-struct", 12);
	      DEFINE_STRING(BGl_string1493z00zz__structurez00,
		BgL_bgl_string1493za700za7za7_1529za7, "symbol", 6);
	      DEFINE_STRING(BGl_string1494z00zz__structurez00,
		BgL_bgl_string1494za700za7za7_1530za7, "bint", 4);
	      DEFINE_STRING(BGl_string1495z00zz__structurez00,
		BgL_bgl_string1495za700za7za7_1531za7, "&struct-key", 11);
	      DEFINE_STRING(BGl_string1496z00zz__structurez00,
		BgL_bgl_string1496za700za7za7_1532za7, "struct", 6);
	      DEFINE_STRING(BGl_string1497z00zz__structurez00,
		BgL_bgl_string1497za700za7za7_1533za7, "&struct-key-set!", 16);
	      DEFINE_STRING(BGl_string1498z00zz__structurez00,
		BgL_bgl_string1498za700za7za7_1534za7, "&struct-length", 14);
	      DEFINE_STRING(BGl_string1499z00zz__structurez00,
		BgL_bgl_string1499za700za7za7_1535za7, "&struct-ref", 11);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_structzd2ze3listzd2envze3zz__structurez00,
		BgL_bgl_za762structza7d2za7e3l1536za7,
		BGl_z62structzd2ze3listz53zz__structurez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_structzd2refzd2envz00zz__structurez00,
		BgL_bgl_za762structza7d2refza71537za7,
		BGl_z62structzd2refzb0zz__structurez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_structzd2keyzd2setz12zd2envzc0zz__structurez00,
		BgL_bgl_za762structza7d2keyza71538za7,
		BGl_z62structzd2keyzd2setz12z70zz__structurez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_structzd2keyzd2envz00zz__structurez00,
		BgL_bgl_za762structza7d2keyza71539za7,
		BGl_z62structzd2keyzb0zz__structurez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_recordzf3zd2envz21zz__structurez00,
		BgL_bgl_za762recordza7f3za791za71540z00,
		BGl_z62recordzf3z91zz__structurez00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__structurez00));
		     ADD_ROOT((void *) (&BGl_symbol1505z00zz__structurez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long
		BgL_checksumz00_1278, char *BgL_fromz00_1279)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__structurez00))
				{
					BGl_requirezd2initializa7ationz75zz__structurez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__structurez00();
					BGl_cnstzd2initzd2zz__structurez00();
					return BGl_importedzd2moduleszd2initz00zz__structurez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__structurez00(void)
	{
		{	/* Llib/struct.scm 18 */
			return (BGl_symbol1505z00zz__structurez00 =
				bstring_to_symbol(BGl_string1506z00zz__structurez00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__structurez00(void)
	{
		{	/* Llib/struct.scm 18 */
			return bgl_gc_roots_register();
		}

	}



/* make-struct */
	BGL_EXPORTED_DEF obj_t BGl_makezd2structzd2zz__structurez00(obj_t
		BgL_keyz00_3, int BgL_lenz00_4, obj_t BgL_initz00_5)
	{
		{	/* Llib/struct.scm 119 */
			BGL_TAIL return make_struct(BgL_keyz00_3, BgL_lenz00_4, BgL_initz00_5);
		}

	}



/* &make-struct */
	obj_t BGl_z62makezd2structzb0zz__structurez00(obj_t BgL_envz00_1223,
		obj_t BgL_keyz00_1224, obj_t BgL_lenz00_1225, obj_t BgL_initz00_1226)
	{
		{	/* Llib/struct.scm 119 */
			{	/* Llib/struct.scm 120 */
				int BgL_auxz00_1296;
				obj_t BgL_auxz00_1289;

				{	/* Llib/struct.scm 120 */
					obj_t BgL_tmpz00_1297;

					if (INTEGERP(BgL_lenz00_1225))
						{	/* Llib/struct.scm 120 */
							BgL_tmpz00_1297 = BgL_lenz00_1225;
						}
					else
						{
							obj_t BgL_auxz00_1300;

							BgL_auxz00_1300 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string1491z00zz__structurez00, BINT(5074L),
								BGl_string1492z00zz__structurez00,
								BGl_string1494z00zz__structurez00, BgL_lenz00_1225);
							FAILURE(BgL_auxz00_1300, BFALSE, BFALSE);
						}
					BgL_auxz00_1296 = CINT(BgL_tmpz00_1297);
				}
				if (SYMBOLP(BgL_keyz00_1224))
					{	/* Llib/struct.scm 120 */
						BgL_auxz00_1289 = BgL_keyz00_1224;
					}
				else
					{
						obj_t BgL_auxz00_1292;

						BgL_auxz00_1292 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1491z00zz__structurez00,
							BINT(5074L), BGl_string1492z00zz__structurez00,
							BGl_string1493z00zz__structurez00, BgL_keyz00_1224);
						FAILURE(BgL_auxz00_1292, BFALSE, BFALSE);
					}
				return
					BGl_makezd2structzd2zz__structurez00(BgL_auxz00_1289, BgL_auxz00_1296,
					BgL_initz00_1226);
			}
		}

	}



/* struct? */
	BGL_EXPORTED_DEF bool_t BGl_structzf3zf3zz__structurez00(obj_t BgL_oz00_6)
	{
		{	/* Llib/struct.scm 125 */
			return STRUCTP(BgL_oz00_6);
		}

	}



/* &struct? */
	obj_t BGl_z62structzf3z91zz__structurez00(obj_t BgL_envz00_1227,
		obj_t BgL_oz00_1228)
	{
		{	/* Llib/struct.scm 125 */
			return BBOOL(BGl_structzf3zf3zz__structurez00(BgL_oz00_1228));
		}

	}



/* record? */
	BGL_EXPORTED_DEF bool_t BGl_recordzf3zf3zz__structurez00(obj_t BgL_oz00_7)
	{
		{	/* Llib/struct.scm 131 */
			return STRUCTP(BgL_oz00_7);
		}

	}



/* &record? */
	obj_t BGl_z62recordzf3z91zz__structurez00(obj_t BgL_envz00_1229,
		obj_t BgL_oz00_1230)
	{
		{	/* Llib/struct.scm 131 */
			return BBOOL(BGl_recordzf3zf3zz__structurez00(BgL_oz00_1230));
		}

	}



/* struct-key */
	BGL_EXPORTED_DEF obj_t BGl_structzd2keyzd2zz__structurez00(obj_t BgL_sz00_8)
	{
		{	/* Llib/struct.scm 137 */
			return STRUCT_KEY(BgL_sz00_8);
		}

	}



/* &struct-key */
	obj_t BGl_z62structzd2keyzb0zz__structurez00(obj_t BgL_envz00_1231,
		obj_t BgL_sz00_1232)
	{
		{	/* Llib/struct.scm 137 */
			{	/* Llib/struct.scm 138 */
				obj_t BgL_auxz00_1313;

				if (STRUCTP(BgL_sz00_1232))
					{	/* Llib/struct.scm 138 */
						BgL_auxz00_1313 = BgL_sz00_1232;
					}
				else
					{
						obj_t BgL_auxz00_1316;

						BgL_auxz00_1316 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1491z00zz__structurez00,
							BINT(5893L), BGl_string1495z00zz__structurez00,
							BGl_string1496z00zz__structurez00, BgL_sz00_1232);
						FAILURE(BgL_auxz00_1316, BFALSE, BFALSE);
					}
				return BGl_structzd2keyzd2zz__structurez00(BgL_auxz00_1313);
			}
		}

	}



/* struct-key-set! */
	BGL_EXPORTED_DEF obj_t BGl_structzd2keyzd2setz12z12zz__structurez00(obj_t
		BgL_sz00_9, obj_t BgL_kz00_10)
	{
		{	/* Llib/struct.scm 143 */
			return STRUCT_KEY_SET(BgL_sz00_9, BgL_kz00_10);
		}

	}



/* &struct-key-set! */
	obj_t BGl_z62structzd2keyzd2setz12z70zz__structurez00(obj_t BgL_envz00_1233,
		obj_t BgL_sz00_1234, obj_t BgL_kz00_1235)
	{
		{	/* Llib/struct.scm 143 */
			{	/* Llib/struct.scm 144 */
				obj_t BgL_auxz00_1329;
				obj_t BgL_auxz00_1322;

				if (SYMBOLP(BgL_kz00_1235))
					{	/* Llib/struct.scm 144 */
						BgL_auxz00_1329 = BgL_kz00_1235;
					}
				else
					{
						obj_t BgL_auxz00_1332;

						BgL_auxz00_1332 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1491z00zz__structurez00,
							BINT(6173L), BGl_string1497z00zz__structurez00,
							BGl_string1493z00zz__structurez00, BgL_kz00_1235);
						FAILURE(BgL_auxz00_1332, BFALSE, BFALSE);
					}
				if (STRUCTP(BgL_sz00_1234))
					{	/* Llib/struct.scm 144 */
						BgL_auxz00_1322 = BgL_sz00_1234;
					}
				else
					{
						obj_t BgL_auxz00_1325;

						BgL_auxz00_1325 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1491z00zz__structurez00,
							BINT(6173L), BGl_string1497z00zz__structurez00,
							BGl_string1496z00zz__structurez00, BgL_sz00_1234);
						FAILURE(BgL_auxz00_1325, BFALSE, BFALSE);
					}
				return
					BGl_structzd2keyzd2setz12z12zz__structurez00(BgL_auxz00_1322,
					BgL_auxz00_1329);
			}
		}

	}



/* struct-length */
	BGL_EXPORTED_DEF int BGl_structzd2lengthzd2zz__structurez00(obj_t BgL_sz00_11)
	{
		{	/* Llib/struct.scm 149 */
			return STRUCT_LENGTH(BgL_sz00_11);
		}

	}



/* &struct-length */
	obj_t BGl_z62structzd2lengthzb0zz__structurez00(obj_t BgL_envz00_1236,
		obj_t BgL_sz00_1237)
	{
		{	/* Llib/struct.scm 149 */
			{	/* Llib/struct.scm 150 */
				int BgL_tmpz00_1338;

				{	/* Llib/struct.scm 150 */
					obj_t BgL_auxz00_1339;

					if (STRUCTP(BgL_sz00_1237))
						{	/* Llib/struct.scm 150 */
							BgL_auxz00_1339 = BgL_sz00_1237;
						}
					else
						{
							obj_t BgL_auxz00_1342;

							BgL_auxz00_1342 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string1491z00zz__structurez00, BINT(6457L),
								BGl_string1498z00zz__structurez00,
								BGl_string1496z00zz__structurez00, BgL_sz00_1237);
							FAILURE(BgL_auxz00_1342, BFALSE, BFALSE);
						}
					BgL_tmpz00_1338 =
						BGl_structzd2lengthzd2zz__structurez00(BgL_auxz00_1339);
				}
				return BINT(BgL_tmpz00_1338);
			}
		}

	}



/* struct-ref */
	BGL_EXPORTED_DEF obj_t BGl_structzd2refzd2zz__structurez00(obj_t BgL_sz00_12,
		int BgL_kz00_13)
	{
		{	/* Llib/struct.scm 155 */
			return STRUCT_REF(BgL_sz00_12, BgL_kz00_13);
		}

	}



/* &struct-ref */
	obj_t BGl_z62structzd2refzb0zz__structurez00(obj_t BgL_envz00_1238,
		obj_t BgL_sz00_1239, obj_t BgL_kz00_1240)
	{
		{	/* Llib/struct.scm 155 */
			{	/* Llib/struct.scm 156 */
				int BgL_auxz00_1356;
				obj_t BgL_auxz00_1349;

				{	/* Llib/struct.scm 156 */
					obj_t BgL_tmpz00_1357;

					if (INTEGERP(BgL_kz00_1240))
						{	/* Llib/struct.scm 156 */
							BgL_tmpz00_1357 = BgL_kz00_1240;
						}
					else
						{
							obj_t BgL_auxz00_1360;

							BgL_auxz00_1360 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string1491z00zz__structurez00, BINT(6739L),
								BGl_string1499z00zz__structurez00,
								BGl_string1494z00zz__structurez00, BgL_kz00_1240);
							FAILURE(BgL_auxz00_1360, BFALSE, BFALSE);
						}
					BgL_auxz00_1356 = CINT(BgL_tmpz00_1357);
				}
				if (STRUCTP(BgL_sz00_1239))
					{	/* Llib/struct.scm 156 */
						BgL_auxz00_1349 = BgL_sz00_1239;
					}
				else
					{
						obj_t BgL_auxz00_1352;

						BgL_auxz00_1352 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1491z00zz__structurez00,
							BINT(6739L), BGl_string1499z00zz__structurez00,
							BGl_string1496z00zz__structurez00, BgL_sz00_1239);
						FAILURE(BgL_auxz00_1352, BFALSE, BFALSE);
					}
				return
					BGl_structzd2refzd2zz__structurez00(BgL_auxz00_1349, BgL_auxz00_1356);
			}
		}

	}



/* struct-set! */
	BGL_EXPORTED_DEF obj_t BGl_structzd2setz12zc0zz__structurez00(obj_t
		BgL_sz00_14, int BgL_kz00_15, obj_t BgL_oz00_16)
	{
		{	/* Llib/struct.scm 161 */
			return STRUCT_SET(BgL_sz00_14, BgL_kz00_15, BgL_oz00_16);
		}

	}



/* &struct-set! */
	obj_t BGl_z62structzd2setz12za2zz__structurez00(obj_t BgL_envz00_1241,
		obj_t BgL_sz00_1242, obj_t BgL_kz00_1243, obj_t BgL_oz00_1244)
	{
		{	/* Llib/struct.scm 161 */
			{	/* Llib/struct.scm 162 */
				int BgL_auxz00_1374;
				obj_t BgL_auxz00_1367;

				{	/* Llib/struct.scm 162 */
					obj_t BgL_tmpz00_1375;

					if (INTEGERP(BgL_kz00_1243))
						{	/* Llib/struct.scm 162 */
							BgL_tmpz00_1375 = BgL_kz00_1243;
						}
					else
						{
							obj_t BgL_auxz00_1378;

							BgL_auxz00_1378 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string1491z00zz__structurez00, BINT(7020L),
								BGl_string1500z00zz__structurez00,
								BGl_string1494z00zz__structurez00, BgL_kz00_1243);
							FAILURE(BgL_auxz00_1378, BFALSE, BFALSE);
						}
					BgL_auxz00_1374 = CINT(BgL_tmpz00_1375);
				}
				if (STRUCTP(BgL_sz00_1242))
					{	/* Llib/struct.scm 162 */
						BgL_auxz00_1367 = BgL_sz00_1242;
					}
				else
					{
						obj_t BgL_auxz00_1370;

						BgL_auxz00_1370 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1491z00zz__structurez00,
							BINT(7020L), BGl_string1500z00zz__structurez00,
							BGl_string1496z00zz__structurez00, BgL_sz00_1242);
						FAILURE(BgL_auxz00_1370, BFALSE, BFALSE);
					}
				return
					BGl_structzd2setz12zc0zz__structurez00(BgL_auxz00_1367,
					BgL_auxz00_1374, BgL_oz00_1244);
			}
		}

	}



/* struct-update! */
	BGL_EXPORTED_DEF obj_t BGl_structzd2updatez12zc0zz__structurez00(obj_t
		BgL_dstz00_17, obj_t BgL_srcz00_18)
	{
		{	/* Llib/struct.scm 167 */
			{	/* Llib/struct.scm 168 */
				bool_t BgL_test1552z00_1384;

				if ((STRUCT_KEY(BgL_dstz00_17) == STRUCT_KEY(BgL_srcz00_18)))
					{	/* Llib/struct.scm 168 */
						BgL_test1552z00_1384 =
							(
							(long) (STRUCT_LENGTH(BgL_dstz00_17)) ==
							(long) (STRUCT_LENGTH(BgL_srcz00_18)));
					}
				else
					{	/* Llib/struct.scm 168 */
						BgL_test1552z00_1384 = ((bool_t) 0);
					}
				if (BgL_test1552z00_1384)
					{	/* Llib/struct.scm 170 */
						long BgL_g1012z00_759;

						BgL_g1012z00_759 = ((long) (STRUCT_LENGTH(BgL_dstz00_17)) - 1L);
						{
							long BgL_iz00_761;

							BgL_iz00_761 = BgL_g1012z00_759;
						BgL_zc3z04anonymousza31068ze3z87_762:
							if ((BgL_iz00_761 == -1L))
								{	/* Llib/struct.scm 171 */
									return BgL_dstz00_17;
								}
							else
								{	/* Llib/struct.scm 171 */
									{	/* Llib/struct.scm 162 */
										obj_t BgL_auxz00_1401;
										int BgL_tmpz00_1399;

										BgL_auxz00_1401 =
											STRUCT_REF(BgL_srcz00_18, (int) (BgL_iz00_761));
										BgL_tmpz00_1399 = (int) (BgL_iz00_761);
										STRUCT_SET(BgL_dstz00_17, BgL_tmpz00_1399, BgL_auxz00_1401);
									}
									{
										long BgL_iz00_1405;

										BgL_iz00_1405 = (BgL_iz00_761 - 1L);
										BgL_iz00_761 = BgL_iz00_1405;
										goto BgL_zc3z04anonymousza31068ze3z87_762;
									}
								}
						}
					}
				else
					{	/* Llib/struct.scm 176 */
						obj_t BgL_arg1078z00_768;

						{	/* Llib/struct.scm 176 */
							obj_t BgL_list1079z00_769;

							{	/* Llib/struct.scm 176 */
								obj_t BgL_arg1080z00_770;

								BgL_arg1080z00_770 = MAKE_YOUNG_PAIR(BgL_srcz00_18, BNIL);
								BgL_list1079z00_769 =
									MAKE_YOUNG_PAIR(BgL_dstz00_17, BgL_arg1080z00_770);
							}
							BgL_arg1078z00_768 = BgL_list1079z00_769;
						}
						return
							BGl_errorz00zz__errorz00(BGl_string1501z00zz__structurez00,
							BGl_string1502z00zz__structurez00, BgL_arg1078z00_768);
					}
			}
		}

	}



/* &struct-update! */
	obj_t BGl_z62structzd2updatez12za2zz__structurez00(obj_t BgL_envz00_1245,
		obj_t BgL_dstz00_1246, obj_t BgL_srcz00_1247)
	{
		{	/* Llib/struct.scm 167 */
			{	/* Llib/struct.scm 168 */
				obj_t BgL_auxz00_1417;
				obj_t BgL_auxz00_1410;

				if (STRUCTP(BgL_srcz00_1247))
					{	/* Llib/struct.scm 168 */
						BgL_auxz00_1417 = BgL_srcz00_1247;
					}
				else
					{
						obj_t BgL_auxz00_1420;

						BgL_auxz00_1420 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1491z00zz__structurez00,
							BINT(7301L), BGl_string1503z00zz__structurez00,
							BGl_string1496z00zz__structurez00, BgL_srcz00_1247);
						FAILURE(BgL_auxz00_1420, BFALSE, BFALSE);
					}
				if (STRUCTP(BgL_dstz00_1246))
					{	/* Llib/struct.scm 168 */
						BgL_auxz00_1410 = BgL_dstz00_1246;
					}
				else
					{
						obj_t BgL_auxz00_1413;

						BgL_auxz00_1413 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1491z00zz__structurez00,
							BINT(7301L), BGl_string1503z00zz__structurez00,
							BGl_string1496z00zz__structurez00, BgL_dstz00_1246);
						FAILURE(BgL_auxz00_1413, BFALSE, BFALSE);
					}
				return
					BGl_structzd2updatez12zc0zz__structurez00(BgL_auxz00_1410,
					BgL_auxz00_1417);
			}
		}

	}



/* struct->list */
	BGL_EXPORTED_DEF obj_t BGl_structzd2ze3listz31zz__structurez00(obj_t
		BgL_structz00_19)
	{
		{	/* Llib/struct.scm 181 */
			{	/* Llib/struct.scm 182 */
				long BgL_g1013z00_776;

				BgL_g1013z00_776 = ((long) (STRUCT_LENGTH(BgL_structz00_19)) - 1L);
				{
					long BgL_iz00_1111;
					obj_t BgL_rz00_1112;

					BgL_iz00_1111 = BgL_g1013z00_776;
					BgL_rz00_1112 = BNIL;
				BgL_loopz00_1110:
					if ((BgL_iz00_1111 == -1L))
						{	/* Llib/struct.scm 184 */
							return
								MAKE_YOUNG_PAIR(STRUCT_KEY(BgL_structz00_19), BgL_rz00_1112);
						}
					else
						{	/* Llib/struct.scm 186 */
							long BgL_arg1090z00_1120;
							obj_t BgL_arg1092z00_1121;

							BgL_arg1090z00_1120 = (BgL_iz00_1111 - 1L);
							BgL_arg1092z00_1121 =
								MAKE_YOUNG_PAIR(STRUCT_REF(BgL_structz00_19,
									(int) (BgL_iz00_1111)), BgL_rz00_1112);
							{
								obj_t BgL_rz00_1437;
								long BgL_iz00_1436;

								BgL_iz00_1436 = BgL_arg1090z00_1120;
								BgL_rz00_1437 = BgL_arg1092z00_1121;
								BgL_rz00_1112 = BgL_rz00_1437;
								BgL_iz00_1111 = BgL_iz00_1436;
								goto BgL_loopz00_1110;
							}
						}
				}
			}
		}

	}



/* &struct->list */
	obj_t BGl_z62structzd2ze3listz53zz__structurez00(obj_t BgL_envz00_1248,
		obj_t BgL_structz00_1249)
	{
		{	/* Llib/struct.scm 181 */
			{	/* Llib/struct.scm 182 */
				obj_t BgL_auxz00_1438;

				if (STRUCTP(BgL_structz00_1249))
					{	/* Llib/struct.scm 182 */
						BgL_auxz00_1438 = BgL_structz00_1249;
					}
				else
					{
						obj_t BgL_auxz00_1441;

						BgL_auxz00_1441 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1491z00zz__structurez00,
							BINT(7892L), BGl_string1504z00zz__structurez00,
							BGl_string1496z00zz__structurez00, BgL_structz00_1249);
						FAILURE(BgL_auxz00_1441, BFALSE, BFALSE);
					}
				return BGl_structzd2ze3listz31zz__structurez00(BgL_auxz00_1438);
			}
		}

	}



/* list->struct */
	BGL_EXPORTED_DEF obj_t BGl_listzd2ze3structz31zz__structurez00(obj_t
		BgL_lstz00_20)
	{
		{	/* Llib/struct.scm 191 */
			{	/* Llib/struct.scm 195 */
				bool_t BgL_test1559z00_1446;

				{	/* Llib/struct.scm 195 */
					obj_t BgL_tmpz00_1447;

					BgL_tmpz00_1447 = CAR(BgL_lstz00_20);
					BgL_test1559z00_1446 = SYMBOLP(BgL_tmpz00_1447);
				}
				if (BgL_test1559z00_1446)
					{	/* Llib/struct.scm 198 */
						long BgL_lenz00_792;

						BgL_lenz00_792 = bgl_list_length(CDR(BgL_lstz00_20));
						{	/* Llib/struct.scm 198 */
							obj_t BgL_structz00_793;

							{	/* Llib/struct.scm 199 */
								obj_t BgL_arg1123z00_804;

								BgL_arg1123z00_804 = CAR(BgL_lstz00_20);
								{	/* Llib/struct.scm 199 */
									int BgL_lenz00_1133;

									BgL_lenz00_1133 = (int) (BgL_lenz00_792);
									BgL_structz00_793 =
										make_struct(
										((obj_t) BgL_arg1123z00_804), BgL_lenz00_1133, BUNSPEC);
							}}
							{	/* Llib/struct.scm 199 */

								{	/* Llib/struct.scm 200 */
									obj_t BgL_g1015z00_794;

									BgL_g1015z00_794 = CDR(BgL_lstz00_20);
									{
										long BgL_iz00_1154;
										obj_t BgL_lz00_1155;

										BgL_iz00_1154 = 0L;
										BgL_lz00_1155 = BgL_g1015z00_794;
									BgL_loopz00_1153:
										if (NULLP(BgL_lz00_1155))
											{	/* Llib/struct.scm 202 */
												return BgL_structz00_793;
											}
										else
											{	/* Llib/struct.scm 202 */
												{	/* Llib/struct.scm 205 */
													obj_t BgL_arg1114z00_1161;

													BgL_arg1114z00_1161 = CAR(((obj_t) BgL_lz00_1155));
													{	/* Llib/struct.scm 162 */
														int BgL_tmpz00_1461;

														BgL_tmpz00_1461 = (int) (BgL_iz00_1154);
														STRUCT_SET(BgL_structz00_793, BgL_tmpz00_1461,
															BgL_arg1114z00_1161);
												}}
												{	/* Llib/struct.scm 206 */
													long BgL_arg1115z00_1162;
													obj_t BgL_arg1122z00_1163;

													BgL_arg1115z00_1162 = (BgL_iz00_1154 + 1L);
													BgL_arg1122z00_1163 = CDR(((obj_t) BgL_lz00_1155));
													{
														obj_t BgL_lz00_1468;
														long BgL_iz00_1467;

														BgL_iz00_1467 = BgL_arg1115z00_1162;
														BgL_lz00_1468 = BgL_arg1122z00_1163;
														BgL_lz00_1155 = BgL_lz00_1468;
														BgL_iz00_1154 = BgL_iz00_1467;
														goto BgL_loopz00_1153;
													}
												}
											}
									}
								}
							}
						}
					}
				else
					{	/* Llib/struct.scm 196 */
						obj_t BgL_arg1126z00_806;

						BgL_arg1126z00_806 = CAR(BgL_lstz00_20);
						return
							BGl_errorz00zz__errorz00(BGl_symbol1505z00zz__structurez00,
							BGl_string1507z00zz__structurez00, BgL_arg1126z00_806);
					}
			}
		}

	}



/* &list->struct */
	obj_t BGl_z62listzd2ze3structz53zz__structurez00(obj_t BgL_envz00_1250,
		obj_t BgL_lstz00_1251)
	{
		{	/* Llib/struct.scm 191 */
			{	/* Llib/struct.scm 195 */
				obj_t BgL_auxz00_1471;

				if (PAIRP(BgL_lstz00_1251))
					{	/* Llib/struct.scm 195 */
						BgL_auxz00_1471 = BgL_lstz00_1251;
					}
				else
					{
						obj_t BgL_auxz00_1474;

						BgL_auxz00_1474 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1491z00zz__structurez00,
							BINT(8400L), BGl_string1508z00zz__structurez00,
							BGl_string1509z00zz__structurez00, BgL_lstz00_1251);
						FAILURE(BgL_auxz00_1474, BFALSE, BFALSE);
					}
				return BGl_listzd2ze3structz31zz__structurez00(BgL_auxz00_1471);
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__structurez00(void)
	{
		{	/* Llib/struct.scm 18 */
			return
				BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1510z00zz__structurez00));
		}

	}

#ifdef __cplusplus
}
#endif
