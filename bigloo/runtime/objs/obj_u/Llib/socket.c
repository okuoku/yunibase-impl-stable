/*===========================================================================*/
/*   (Llib/socket.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Llib/socket.scm -indent -o objs/obj_u/Llib/socket.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___SOCKET_TYPE_DEFINITIONS
#define BGL___SOCKET_TYPE_DEFINITIONS
#endif													// BGL___SOCKET_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_keyword1957z00zz__socketz00 = BUNSPEC;
	static obj_t BGl_symbol2005z00zz__socketz00 = BUNSPEC;
	extern obj_t bgl_gethostname_by_address(obj_t);
	static obj_t BGl_symbol2007z00zz__socketz00 = BUNSPEC;
	extern obj_t bgl_socket_host_addr(obj_t);
	static obj_t BGl_symbol2009z00zz__socketz00 = BUNSPEC;
	static obj_t BGl_z62socketzd2localzf3z43zz__socketz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_socketzd2outputzd2zz__socketz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_socketzd2portzd2numberz00zz__socketz00(obj_t);
	extern obj_t bgl_datagram_socket_send(obj_t, obj_t, obj_t, int);
	extern obj_t bgl_datagram_socket_close(obj_t);
	static obj_t BGl_z62hostz62zz__socketz00(obj_t, obj_t);
	extern obj_t bgl_make_client_socket(obj_t, int, int, obj_t, obj_t, obj_t);
	static obj_t BGl_z62socketzd2hostzd2addresszd3zf3z42zz__socketz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_socketzd2closezd2zz__socketz00(obj_t);
	extern long bgl_socket_accept_many(obj_t, bool_t, obj_t, obj_t, obj_t);
	extern obj_t bstring_to_keyword(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__socketz00 = BUNSPEC;
	extern obj_t bgl_make_server_socket(obj_t, int, int, obj_t);
	static obj_t BGl_keyword1977z00zz__socketz00 = BUNSPEC;
	static obj_t BGl_keyword1979z00zz__socketz00 = BUNSPEC;
	static obj_t BGl_z62datagramzd2socketzd2portzd2numberzb0zz__socketz00(obj_t,
		obj_t);
	extern obj_t BGl_applyz00zz__r4_control_features_6_9z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_socketzd2acceptzd2zz__socketz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62socketzd2inputzb0zz__socketz00(obj_t, obj_t);
	static obj_t BGl_z62socketzd2portzd2numberz62zz__socketz00(obj_t, obj_t);
	extern obj_t bgl_setsockopt(obj_t, obj_t, obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62socketzd2localzd2addressz62zz__socketz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__socketz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_EXPORTED_DECL obj_t BGl_hostz00zz__socketz00(obj_t);
	static obj_t BGl_keyword1988z00zz__socketz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_makezd2clientzd2socketz00zz__socketz00(obj_t, int,
		obj_t, obj_t, obj_t, obj_t);
	extern obj_t bgl_gethostname(void);
	extern obj_t bgl_gethostinterfaces(void);
	BGL_EXPORTED_DECL obj_t BGl_socketzd2acceptzd2manyz00zz__socketz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_socketzd2optionzd2setz12z12zz__socketz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_symbol1948z00zz__socketz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_datagramzd2socketzd2outputz00zz__socketz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_makezd2serverzd2socketz00zz__socketz00(obj_t);
	static obj_t BGl_keyword1995z00zz__socketz00 = BUNSPEC;
	static obj_t BGl_keyword1997z00zz__socketz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_socketzd2inputzd2zz__socketz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_socketzd2hostzd2addressz00zz__socketz00(obj_t);
	static obj_t BGl_toplevelzd2initzd2zz__socketz00(void);
	static obj_t BGl_z62socketzd2outputzb0zz__socketz00(obj_t, obj_t);
	static obj_t BGl_symbol1959z00zz__socketz00 = BUNSPEC;
	extern obj_t bgl_make_datagram_unbound_socket(obj_t);
	extern void socket_cleanup(void);
	BGL_EXPORTED_DECL obj_t BGl_z52socketzd2initz12z92zz__socketz00(void);
	static obj_t BGl__socketzd2shutdownzd2zz__socketz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_socketzd2localzd2addressz00zz__socketz00(obj_t);
	static obj_t BGl_z62getzd2interfaceszb0zz__socketz00(obj_t);
	static obj_t BGl_z62datagramzd2socketzd2optionzd2setz12za2zz__socketz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl__hostnamez00zz__socketz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_hostnamez00zz__socketz00(obj_t);
	static obj_t BGl_z62datagramzd2socketzd2optionz62zz__socketz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_datagramzd2socketzd2hostnamez00zz__socketz00(obj_t);
	extern obj_t bgl_getsockopt(obj_t, obj_t);
	extern obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_symbol1966z00zz__socketz00 = BUNSPEC;
	extern bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	static obj_t BGl_symbol1968z00zz__socketz00 = BUNSPEC;
	static obj_t BGl_z62socketzd2optionzd2setz12z70zz__socketz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_dssslzd2getzd2keyzd2argzd2zz__dssslz00(obj_t, obj_t, obj_t);
	extern obj_t bgl_getprotobyname(char *);
	BGL_EXPORTED_DECL obj_t BGl_socketzd2hostnamezd2zz__socketz00(obj_t);
	static obj_t BGl_cnstzd2initzd2zz__socketz00(void);
	extern obj_t BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(obj_t, obj_t,
		int);
	static obj_t BGl_z62socketzd2hostnamezb0zz__socketz00(obj_t, obj_t);
	static obj_t BGl_z62makezd2serverzd2socketz62zz__socketz00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zz__socketz00(void);
	static obj_t BGl_z62resolvz62zz__socketz00(obj_t, obj_t, obj_t);
	extern bool_t bgl_socket_localp(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_datagramzd2socketzd2receivez00zz__socketz00(obj_t,
		int);
	static obj_t BGl_symbol1971z00zz__socketz00 = BUNSPEC;
	static obj_t BGl_symbol1973z00zz__socketz00 = BUNSPEC;
	static obj_t BGl_importedzd2moduleszd2initz00zz__socketz00(void);
	static obj_t BGl_z62datagramzd2socketzd2receivez62zz__socketz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl__makezd2datagramzd2clientzd2socketzd2zz__socketz00(obj_t,
		obj_t);
	static obj_t BGl_gczd2rootszd2initz00zz__socketz00(void);
	BGL_EXPORTED_DECL obj_t BGl_getzd2protocolszd2zz__socketz00(void);
	static obj_t BGl_z62socketzd2downzf3z43zz__socketz00(obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zz__socketz00(void);
	static obj_t BGl_z62socketzd2clientzf3z43zz__socketz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_datagramzd2socketzd2closez00zz__socketz00(obj_t);
	static obj_t BGl__makezd2datagramzd2serverzd2socketzd2zz__socketz00(obj_t,
		obj_t);
	static obj_t BGl_list1950z00zz__socketz00 = BUNSPEC;
	extern obj_t bgl_make_server_unix_socket(obj_t, int);
	BGL_EXPORTED_DECL obj_t BGl_socketzd2optionzd2zz__socketz00(obj_t, obj_t);
	extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t socket_close(obj_t);
	static obj_t BGl_z62socketzd2serverzf3z43zz__socketz00(obj_t, obj_t);
	static bool_t BGl_za2socketzd2initializa7edza2z75zz__socketz00;
	static obj_t BGl_symbol1990z00zz__socketz00 = BUNSPEC;
	extern obj_t bgl_datagram_socket_receive(obj_t, long);
	BGL_EXPORTED_DECL bool_t BGl_socketzd2downzf3z21zz__socketz00(obj_t);
	static obj_t BGl_symbol1999z00zz__socketz00 = BUNSPEC;
	static obj_t BGl_z62datagramzd2socketzd2sendz62zz__socketz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62z52socketzd2initz12zf0zz__socketz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_datagramzd2socketzf3z21zz__socketz00(obj_t);
	static obj_t BGl_za2socketzd2mutexza2zd2zz__socketz00 = BUNSPEC;
	static obj_t BGl__makezd2clientzd2socketz00zz__socketz00(obj_t, obj_t);
	extern obj_t bgl_host(obj_t);
	static obj_t BGl__socketzd2acceptzd2manyz00zz__socketz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_socketzf3zf3zz__socketz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_hostinfoz00zz__socketz00(obj_t);
	extern obj_t bgl_getsockopt(obj_t, obj_t);
	static obj_t BGl_z62socketzf3z91zz__socketz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2datagramzd2clientzd2socketzd2zz__socketz00(obj_t, int, obj_t,
		obj_t);
	static obj_t BGl_list1976z00zz__socketz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_datagramzd2socketzd2inputz00zz__socketz00(obj_t);
	static obj_t BGl_z62datagramzd2socketzd2hostzd2addresszb0zz__socketz00(obj_t,
		obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_datagramzd2socketzd2portzd2numberzd2zz__socketz00(obj_t);
	extern obj_t bgl_res_query(obj_t, obj_t);
	extern obj_t make_vector(long, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2datagramzd2serverzd2socketzd2zz__socketz00(obj_t, obj_t);
	static obj_t BGl__makezd2datagramzd2unboundzd2socketzd2zz__socketz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_datagramzd2socketzd2sendz00zz__socketz00(obj_t,
		obj_t, obj_t, int);
	BGL_EXPORTED_DECL obj_t BGl_getzd2interfaceszd2zz__socketz00(void);
	extern obj_t bgl_setsockopt(obj_t, obj_t, obj_t);
	static obj_t BGl_list1987z00zz__socketz00 = BUNSPEC;
	static obj_t BGl_z62getzd2protocolszb0zz__socketz00(obj_t);
	extern bool_t bgl_socket_host_addr_cmp(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_datagramzd2socketzd2optionz00zz__socketz00(obj_t,
		obj_t);
	static obj_t BGl_z62datagramzd2socketzd2closez62zz__socketz00(obj_t, obj_t);
	extern obj_t BGl_registerzd2exitzd2functionz12z12zz__biglooz00(obj_t);
	static obj_t BGl_methodzd2initzd2zz__socketz00(void);
	static obj_t BGl_z62datagramzd2socketzd2hostnamez62zz__socketz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31324ze3ze5zz__socketz00(obj_t, obj_t);
	BGL_EXPORTED_DECL int BGl_socketzd2shutdownzd2zz__socketz00(obj_t, obj_t);
	extern obj_t bgl_make_datagram_client_socket(obj_t, int, bool_t, obj_t);
	static obj_t BGl_list1994z00zz__socketz00 = BUNSPEC;
	static obj_t BGl_z62socketzd2optionzb0zz__socketz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_datagramzd2socketzd2hostzd2addresszd2zz__socketz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_resolvz00zz__socketz00(obj_t, obj_t);
	extern obj_t BGl_mapz00zz__r4_control_features_6_9z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_socketzd2localzf3z21zz__socketz00(obj_t);
	extern obj_t bgl_make_datagram_server_socket(int, obj_t);
	extern obj_t bgl_socket_accept(obj_t, bool_t, obj_t, obj_t);
	static obj_t BGl_z62socketzd2hostzd2addressz62zz__socketz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_socketzd2clientzf3z21zz__socketz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_datagramzd2socketzd2optionzd2setz12zc0zz__socketz00(obj_t, obj_t,
		obj_t);
	extern void socket_startup(void);
	extern obj_t BGl_dssslzd2getzd2keyzd2restzd2argz00zz__dssslz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_socketzd2hostzd2addresszd3zf3z20zz__socketz00(obj_t, obj_t);
	static obj_t BGl_z62getzd2protocolzb0zz__socketz00(obj_t, obj_t);
	static obj_t BGl__socketzd2acceptzd2zz__socketz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_socketzd2serverzf3z21zz__socketz00(obj_t);
	extern obj_t bgl_socket_local_addr(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31245ze3ze5zz__socketz00(obj_t, obj_t);
	extern obj_t bgl_getprotoents(void);
	extern int socket_shutdown(obj_t, int);
	static obj_t BGl_z62datagramzd2socketzd2inputz62zz__socketz00(obj_t, obj_t);
	static obj_t BGl_z62datagramzd2socketzd2outputz62zz__socketz00(obj_t, obj_t);
	extern obj_t bgl_getprotobynumber(int);
	BGL_EXPORTED_DECL bool_t
		BGl_datagramzd2socketzd2clientzf3zf3zz__socketz00(obj_t);
	static obj_t BGl_z62datagramzd2socketzd2clientzf3z91zz__socketz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2datagramzd2unboundzd2socketzd2zz__socketz00(obj_t);
	extern obj_t bgl_make_client_unix_socket(obj_t, int, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_datagramzd2socketzd2serverzf3zf3zz__socketz00(obj_t);
	static obj_t BGl_z62datagramzd2socketzf3z43zz__socketz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_getzd2protocolzd2zz__socketz00(obj_t);
	static obj_t BGl_z62datagramzd2socketzd2serverzf3z91zz__socketz00(obj_t,
		obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t bgl_hostinfo(obj_t);
	static obj_t BGl_z62hostinfoz62zz__socketz00(obj_t, obj_t);
	static obj_t BGl_z62socketzd2closezb0zz__socketz00(obj_t, obj_t);
	static obj_t BGl_keyword1951z00zz__socketz00 = BUNSPEC;
	static obj_t BGl_keyword1953z00zz__socketz00 = BUNSPEC;
	static obj_t BGl_keyword1955z00zz__socketz00 = BUNSPEC;
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string2040z00zz__socketz00,
		BgL_bgl_string2040za700za7za7_2043za7, "&datagram-socket-option", 23);
	      DEFINE_STRING(BGl_string2041z00zz__socketz00,
		BgL_bgl_string2041za700za7za7_2044za7, "&datagram-socket-option-set!", 28);
	      DEFINE_STRING(BGl_string2042z00zz__socketz00,
		BgL_bgl_string2042za700za7za7_2045za7, "__socket", 8);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_hostinfozd2envzd2zz__socketz00,
		BgL_bgl_za762hostinfoza762za7za72046z00, BGl_z62hostinfoz62zz__socketz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_socketzd2closezd2envz00zz__socketz00,
		BgL_bgl_za762socketza7d2clos2047z00, BGl_z62socketzd2closezb0zz__socketz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2datagramzd2clientzd2socketzd2envz00zz__socketz00,
		BgL_bgl__makeza7d2datagram2048za7, opt_generic_entry,
		BGl__makezd2datagramzd2clientzd2socketzd2zz__socketz00, BFALSE, -1);
	      DEFINE_STRING(BGl_string1952z00zz__socketz00,
		BgL_bgl_string1952za700za7za7_2049za7, "domain", 6);
	      DEFINE_STRING(BGl_string1954z00zz__socketz00,
		BgL_bgl_string1954za700za7za7_2050za7, "inbuf", 5);
	      DEFINE_STRING(BGl_string1956z00zz__socketz00,
		BgL_bgl_string1956za700za7za7_2051za7, "outbuf", 6);
	      DEFINE_STRING(BGl_string1958z00zz__socketz00,
		BgL_bgl_string1958za700za7za7_2052za7, "timeout", 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2datagramzd2serverzd2socketzd2envz00zz__socketz00,
		BgL_bgl__makeza7d2datagram2053za7, opt_generic_entry,
		BGl__makezd2datagramzd2serverzd2socketzd2zz__socketz00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_datagramzd2socketzd2receivezd2envzd2zz__socketz00,
		BgL_bgl_za762datagramza7d2so2054z00,
		BGl_z62datagramzd2socketzd2receivez62zz__socketz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_datagramzd2socketzd2sendzd2envzd2zz__socketz00,
		BgL_bgl_za762datagramza7d2so2055z00,
		BGl_z62datagramzd2socketzd2sendz62zz__socketz00, 0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string1960z00zz__socketz00,
		BgL_bgl_string1960za700za7za7_2056za7, "make-client-socket::socket", 26);
	      DEFINE_STRING(BGl_string1961z00zz__socketz00,
		BgL_bgl_string1961za700za7za7_2057za7, "Illegal keyword argument", 24);
	      DEFINE_STRING(BGl_string1962z00zz__socketz00,
		BgL_bgl_string1962za700za7za7_2058za7,
		"wrong number of arguments: [2..6] expected, provided", 52);
	      DEFINE_STRING(BGl_string1963z00zz__socketz00,
		BgL_bgl_string1963za700za7za7_2059za7, "_make-client-socket", 19);
	      DEFINE_STRING(BGl_string1964z00zz__socketz00,
		BgL_bgl_string1964za700za7za7_2060za7, "bint", 4);
	      DEFINE_STRING(BGl_string1965z00zz__socketz00,
		BgL_bgl_string1965za700za7za7_2061za7, "make-client-socket", 18);
	      DEFINE_STRING(BGl_string1967z00zz__socketz00,
		BgL_bgl_string1967za700za7za7_2062za7, "inet6", 5);
	      DEFINE_STRING(BGl_string1969z00zz__socketz00,
		BgL_bgl_string1969za700za7za7_2063za7, "unspec", 6);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2clientzd2socketzd2envzd2zz__socketz00,
		BgL_bgl__makeza7d2clientza7d2064z00, opt_generic_entry,
		BGl__makezd2clientzd2socketz00zz__socketz00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_socketzd2acceptzd2manyzd2envzd2zz__socketz00,
		BgL_bgl__socketza7d2accept2065za7, opt_generic_entry,
		BGl__socketzd2acceptzd2manyz00zz__socketz00, BFALSE, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_socketzd2optionzd2envz00zz__socketz00,
		BgL_bgl_za762socketza7d2opti2066z00, BGl_z62socketzd2optionzb0zz__socketz00,
		0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1970z00zz__socketz00,
		BgL_bgl_string1970za700za7za7_2067za7, "symbol", 6);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_socketzd2shutdownzd2envz00zz__socketz00,
		BgL_bgl__socketza7d2shutdo2068za7, opt_generic_entry,
		BGl__socketzd2shutdownzd2zz__socketz00, BFALSE, -1);
	      DEFINE_STRING(BGl_string1972z00zz__socketz00,
		BgL_bgl_string1972za700za7za7_2069za7, "unix", 4);
	      DEFINE_STRING(BGl_string1974z00zz__socketz00,
		BgL_bgl_string1974za700za7za7_2070za7, "local", 5);
	      DEFINE_STRING(BGl_string1975z00zz__socketz00,
		BgL_bgl_string1975za700za7za7_2071za7, "Unknown socket domain", 21);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2serverzd2socketzd2envzd2zz__socketz00,
		BgL_bgl_za762makeza7d2server2072z00, va_generic_entry,
		BGl_z62makezd2serverzd2socketz62zz__socketz00, BUNSPEC, -1);
	      DEFINE_STRING(BGl_string1978z00zz__socketz00,
		BgL_bgl_string1978za700za7za7_2073za7, "backlog", 7);
	extern obj_t BGl_stringzd2appendzd2envz00zz__r4_strings_6_7z00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_z52socketzd2initz12zd2envz40zz__socketz00,
		BgL_bgl_za762za752socketza7d2i2074za7,
		BGl_z62z52socketzd2initz12zf0zz__socketz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_socketzd2portzd2numberzd2envzd2zz__socketz00,
		BgL_bgl_za762socketza7d2port2075z00,
		BGl_z62socketzd2portzd2numberz62zz__socketz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1980z00zz__socketz00,
		BgL_bgl_string1980za700za7za7_2076za7, "name", 4);
	      DEFINE_STRING(BGl_string1981z00zz__socketz00,
		BgL_bgl_string1981za700za7za7_2077za7, "make-server-socket", 18);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_socketzd2localzd2addresszd2envzd2zz__socketz00,
		BgL_bgl_za762socketza7d2loca2078z00,
		BGl_z62socketzd2localzd2addressz62zz__socketz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1982z00zz__socketz00,
		BgL_bgl_string1982za700za7za7_2079za7, "Unsupported domain", 18);
	      DEFINE_STRING(BGl_string1984z00zz__socketz00,
		BgL_bgl_string1984za700za7za7_2080za7, "Illegal extra key arguments: ", 29);
	      DEFINE_STRING(BGl_string1985z00zz__socketz00,
		BgL_bgl_string1985za700za7za7_2081za7, "dsssl-get-key-arg", 17);
	      DEFINE_STRING(BGl_string1986z00zz__socketz00,
		BgL_bgl_string1986za700za7za7_2082za7, "~a ", 3);
	      DEFINE_STRING(BGl_string1989z00zz__socketz00,
		BgL_bgl_string1989za700za7za7_2083za7, "errp", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1983z00zz__socketz00,
		BgL_bgl_za762za7c3za704anonymo2084za7,
		BGl_z62zc3z04anonymousza31324ze3ze5zz__socketz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1991z00zz__socketz00,
		BgL_bgl_string1991za700za7za7_2085za7, "socket-accept", 13);
	      DEFINE_STRING(BGl_string1992z00zz__socketz00,
		BgL_bgl_string1992za700za7za7_2086za7,
		"wrong number of arguments: [1..4] expected, provided", 52);
	      DEFINE_STRING(BGl_string1993z00zz__socketz00,
		BgL_bgl_string1993za700za7za7_2087za7, "_socket-accept", 14);
	      DEFINE_STRING(BGl_string1996z00zz__socketz00,
		BgL_bgl_string1996za700za7za7_2088za7, "inbufs", 6);
	      DEFINE_STRING(BGl_string1998z00zz__socketz00,
		BgL_bgl_string1998za700za7za7_2089za7, "outbufs", 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_datagramzd2socketzd2portzd2numberzd2envz00zz__socketz00,
		BgL_bgl_za762datagramza7d2so2090z00,
		BGl_z62datagramzd2socketzd2portzd2numberzb0zz__socketz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_datagramzd2socketzd2clientzf3zd2envz21zz__socketz00,
		BgL_bgl_za762datagramza7d2so2091z00,
		BGl_z62datagramzd2socketzd2clientzf3z91zz__socketz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2datagramzd2unboundzd2socketzd2envz00zz__socketz00,
		BgL_bgl__makeza7d2datagram2092za7, opt_generic_entry,
		BGl__makezd2datagramzd2unboundzd2socketzd2zz__socketz00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_datagramzd2socketzd2serverzf3zd2envz21zz__socketz00,
		BgL_bgl_za762datagramza7d2so2093z00,
		BGl_z62datagramzd2socketzd2serverzf3z91zz__socketz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_socketzd2optionzd2setz12zd2envzc0zz__socketz00,
		BgL_bgl_za762socketza7d2opti2094z00,
		BGl_z62socketzd2optionzd2setz12z70zz__socketz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_datagramzd2socketzd2outputzd2envzd2zz__socketz00,
		BgL_bgl_za762datagramza7d2so2095z00,
		BGl_z62datagramzd2socketzd2outputz62zz__socketz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_resolvzd2envzd2zz__socketz00,
		BgL_bgl_za762resolvza762za7za7__2096z00, BGl_z62resolvz62zz__socketz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_datagramzd2socketzd2optionzd2setz12zd2envz12zz__socketz00,
		BgL_bgl_za762datagramza7d2so2097z00,
		BGl_z62datagramzd2socketzd2optionzd2setz12za2zz__socketz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_socketzd2hostzd2addresszd3zf3zd2envzf2zz__socketz00,
		BgL_bgl_za762socketza7d2host2098z00,
		BGl_z62socketzd2hostzd2addresszd3zf3z42zz__socketz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_socketzf3zd2envz21zz__socketz00,
		BgL_bgl_za762socketza7f3za791za72099z00, BGl_z62socketzf3z91zz__socketz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_datagramzd2socketzd2inputzd2envzd2zz__socketz00,
		BgL_bgl_za762datagramza7d2so2100z00,
		BGl_z62datagramzd2socketzd2inputz62zz__socketz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_socketzd2hostnamezd2envz00zz__socketz00,
		BgL_bgl_za762socketza7d2host2101z00,
		BGl_z62socketzd2hostnamezb0zz__socketz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_socketzd2acceptzd2envz00zz__socketz00,
		BgL_bgl__socketza7d2accept2102za7, opt_generic_entry,
		BGl__socketzd2acceptzd2zz__socketz00, BFALSE, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_hostzd2envzd2zz__socketz00,
		BgL_bgl_za762hostza762za7za7__so2103z00, BGl_z62hostz62zz__socketz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_getzd2protocolzd2envz00zz__socketz00,
		BgL_bgl_za762getza7d2protoco2104z00, BGl_z62getzd2protocolzb0zz__socketz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_socketzd2localzf3zd2envzf3zz__socketz00,
		BgL_bgl_za762socketza7d2loca2105z00,
		BGl_z62socketzd2localzf3z43zz__socketz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_socketzd2inputzd2envz00zz__socketz00,
		BgL_bgl_za762socketza7d2inpu2106z00, BGl_z62socketzd2inputzb0zz__socketz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_socketzd2outputzd2envz00zz__socketz00,
		BgL_bgl_za762socketza7d2outp2107z00, BGl_z62socketzd2outputzb0zz__socketz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_socketzd2downzf3zd2envzf3zz__socketz00,
		BgL_bgl_za762socketza7d2down2108z00,
		BGl_z62socketzd2downzf3z43zz__socketz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_socketzd2clientzf3zd2envzf3zz__socketz00,
		BgL_bgl_za762socketza7d2clie2109z00,
		BGl_z62socketzd2clientzf3z43zz__socketz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2000z00zz__socketz00,
		BgL_bgl_string2000za700za7za7_2110za7, "socket-accept-many", 18);
	      DEFINE_STRING(BGl_string2001z00zz__socketz00,
		BgL_bgl_string2001za700za7za7_2111za7,
		"wrong number of arguments: [2..5] expected, provided", 52);
	      DEFINE_STRING(BGl_string2002z00zz__socketz00,
		BgL_bgl_string2002za700za7za7_2112za7, "_socket-accept-many", 19);
	      DEFINE_STRING(BGl_string2003z00zz__socketz00,
		BgL_bgl_string2003za700za7za7_2113za7, "vector", 6);
	      DEFINE_STRING(BGl_string2004z00zz__socketz00,
		BgL_bgl_string2004za700za7za7_2114za7, "_socket-shutdown", 16);
	      DEFINE_STRING(BGl_string2006z00zz__socketz00,
		BgL_bgl_string2006za700za7za7_2115za7, "RDWR", 4);
	      DEFINE_STRING(BGl_string2008z00zz__socketz00,
		BgL_bgl_string2008za700za7za7_2116za7, "WR", 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_socketzd2serverzf3zd2envzf3zz__socketz00,
		BgL_bgl_za762socketza7d2serv2117z00,
		BGl_z62socketzd2serverzf3z43zz__socketz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2010z00zz__socketz00,
		BgL_bgl_string2010za700za7za7_2118za7, "RD", 2);
	      DEFINE_STRING(BGl_string2011z00zz__socketz00,
		BgL_bgl_string2011za700za7za7_2119za7, "socket-shutdown", 15);
	      DEFINE_STRING(BGl_string2012z00zz__socketz00,
		BgL_bgl_string2012za700za7za7_2120za7, "wrong optional argument", 23);
	      DEFINE_STRING(BGl_string2013z00zz__socketz00,
		BgL_bgl_string2013za700za7za7_2121za7, "&socket-close", 13);
	      DEFINE_STRING(BGl_string2014z00zz__socketz00,
		BgL_bgl_string2014za700za7za7_2122za7, "&host", 5);
	      DEFINE_STRING(BGl_string2015z00zz__socketz00,
		BgL_bgl_string2015za700za7za7_2123za7, "&hostinfo", 9);
	      DEFINE_STRING(BGl_string2016z00zz__socketz00,
		BgL_bgl_string2016za700za7za7_2124za7, "_hostname", 9);
	      DEFINE_STRING(BGl_string2017z00zz__socketz00,
		BgL_bgl_string2017za700za7za7_2125za7, "&resolv", 7);
	      DEFINE_STRING(BGl_string2018z00zz__socketz00,
		BgL_bgl_string2018za700za7za7_2126za7, "&socket-option", 14);
	      DEFINE_STRING(BGl_string2019z00zz__socketz00,
		BgL_bgl_string2019za700za7za7_2127za7, "keyword", 7);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_getzd2interfaceszd2envz00zz__socketz00,
		BgL_bgl_za762getza7d2interfa2128z00,
		BGl_z62getzd2interfaceszb0zz__socketz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_getzd2protocolszd2envz00zz__socketz00,
		BgL_bgl_za762getza7d2protoco2129z00, BGl_z62getzd2protocolszb0zz__socketz00,
		0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_datagramzd2socketzd2closezd2envzd2zz__socketz00,
		BgL_bgl_za762datagramza7d2so2130z00,
		BGl_z62datagramzd2socketzd2closez62zz__socketz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_datagramzd2socketzd2optionzd2envzd2zz__socketz00,
		BgL_bgl_za762datagramza7d2so2131z00,
		BGl_z62datagramzd2socketzd2optionz62zz__socketz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2020z00zz__socketz00,
		BgL_bgl_string2020za700za7za7_2132za7, "&socket-option-set!", 19);
	      DEFINE_STRING(BGl_string2021z00zz__socketz00,
		BgL_bgl_string2021za700za7za7_2133za7, "&datagram-socket-hostname", 25);
	      DEFINE_STRING(BGl_string2022z00zz__socketz00,
		BgL_bgl_string2022za700za7za7_2134za7, "datagram-socket", 15);
	      DEFINE_STRING(BGl_string2023z00zz__socketz00,
		BgL_bgl_string2023za700za7za7_2135za7, "&datagram-socket-host-address", 29);
	      DEFINE_STRING(BGl_string2024z00zz__socketz00,
		BgL_bgl_string2024za700za7za7_2136za7, "&datagram-socket-port-number", 28);
	      DEFINE_STRING(BGl_string2025z00zz__socketz00,
		BgL_bgl_string2025za700za7za7_2137za7, "datagram-socket-output", 22);
	      DEFINE_STRING(BGl_string2026z00zz__socketz00,
		BgL_bgl_string2026za700za7za7_2138za7, "Datagram-socket has no output port",
		34);
	      DEFINE_STRING(BGl_string2027z00zz__socketz00,
		BgL_bgl_string2027za700za7za7_2139za7, "&datagram-socket-output", 23);
	      DEFINE_STRING(BGl_string2028z00zz__socketz00,
		BgL_bgl_string2028za700za7za7_2140za7, "datagram-socket-input", 21);
	      DEFINE_STRING(BGl_string2029z00zz__socketz00,
		BgL_bgl_string2029za700za7za7_2141za7, "Datagram-socket has no input port",
		33);
	      DEFINE_STRING(BGl_string1935z00zz__socketz00,
		BgL_bgl_string1935za700za7za7_2142za7, "socket", 6);
	      DEFINE_STRING(BGl_string1937z00zz__socketz00,
		BgL_bgl_string1937za700za7za7_2143za7,
		"/tmp/bigloo/runtime/Llib/socket.scm", 35);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_datagramzd2socketzd2hostzd2addresszd2envz00zz__socketz00,
		BgL_bgl_za762datagramza7d2so2144z00,
		BGl_z62datagramzd2socketzd2hostzd2addresszb0zz__socketz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1938z00zz__socketz00,
		BgL_bgl_string1938za700za7za7_2145za7, "&socket-hostname", 16);
	      DEFINE_STRING(BGl_string1939z00zz__socketz00,
		BgL_bgl_string1939za700za7za7_2146za7, "&socket-host-address", 20);
	      DEFINE_STRING(BGl_string2030z00zz__socketz00,
		BgL_bgl_string2030za700za7za7_2147za7, "&datagram-socket-input", 22);
	      DEFINE_STRING(BGl_string2031z00zz__socketz00,
		BgL_bgl_string2031za700za7za7_2148za7, "make-datagram-server-socket", 27);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_socketzd2hostzd2addresszd2envzd2zz__socketz00,
		BgL_bgl_za762socketza7d2host2149z00,
		BGl_z62socketzd2hostzd2addressz62zz__socketz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2032z00zz__socketz00,
		BgL_bgl_string2032za700za7za7_2150za7, "_make-datagram-server-socket", 28);
	      DEFINE_STRING(BGl_string2033z00zz__socketz00,
		BgL_bgl_string2033za700za7za7_2151za7, "make-datagram-unbound-socket", 28);
	      DEFINE_STRING(BGl_string2034z00zz__socketz00,
		BgL_bgl_string2034za700za7za7_2152za7, "_make-datagram-unbound-socket", 29);
	      DEFINE_STRING(BGl_string2035z00zz__socketz00,
		BgL_bgl_string2035za700za7za7_2153za7, "_make-datagram-client-socket", 28);
	      DEFINE_STRING(BGl_string2036z00zz__socketz00,
		BgL_bgl_string2036za700za7za7_2154za7, "make-datagram-client-socket", 27);
	      DEFINE_STRING(BGl_string2037z00zz__socketz00,
		BgL_bgl_string2037za700za7za7_2155za7, "&datagram-socket-close", 22);
	      DEFINE_STRING(BGl_string2038z00zz__socketz00,
		BgL_bgl_string2038za700za7za7_2156za7, "&datagram-socket-receive", 24);
	      DEFINE_STRING(BGl_string2039z00zz__socketz00,
		BgL_bgl_string2039za700za7za7_2157za7, "&datagram-socket-send", 21);
	      DEFINE_STRING(BGl_string1940z00zz__socketz00,
		BgL_bgl_string1940za700za7za7_2158za7, "&socket-local-address", 21);
	      DEFINE_STRING(BGl_string1941z00zz__socketz00,
		BgL_bgl_string1941za700za7za7_2159za7, "&socket-local?", 14);
	      DEFINE_STRING(BGl_string1942z00zz__socketz00,
		BgL_bgl_string1942za700za7za7_2160za7, "&socket-host-address=?", 22);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1936z00zz__socketz00,
		BgL_bgl_za762za7c3za704anonymo2161za7,
		BGl_z62zc3z04anonymousza31245ze3ze5zz__socketz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1943z00zz__socketz00,
		BgL_bgl_string1943za700za7za7_2162za7, "bstring", 7);
	      DEFINE_STRING(BGl_string1944z00zz__socketz00,
		BgL_bgl_string1944za700za7za7_2163za7, "&socket-down?", 13);
	      DEFINE_STRING(BGl_string1945z00zz__socketz00,
		BgL_bgl_string1945za700za7za7_2164za7, "&socket-port-number", 19);
	      DEFINE_STRING(BGl_string1946z00zz__socketz00,
		BgL_bgl_string1946za700za7za7_2165za7, "&socket-input", 13);
	      DEFINE_STRING(BGl_string1947z00zz__socketz00,
		BgL_bgl_string1947za700za7za7_2166za7, "&socket-output", 14);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_datagramzd2socketzf3zd2envzf3zz__socketz00,
		BgL_bgl_za762datagramza7d2so2167z00,
		BGl_z62datagramzd2socketzf3z43zz__socketz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1949z00zz__socketz00,
		BgL_bgl_string1949za700za7za7_2168za7, "inet", 4);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_hostnamezd2envzd2zz__socketz00,
		BgL_bgl__hostnameza700za7za7__2169za7, opt_generic_entry,
		BGl__hostnamez00zz__socketz00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_datagramzd2socketzd2hostnamezd2envzd2zz__socketz00,
		BgL_bgl_za762datagramza7d2so2170z00,
		BGl_z62datagramzd2socketzd2hostnamez62zz__socketz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_keyword1957z00zz__socketz00));
		     ADD_ROOT((void *) (&BGl_symbol2005z00zz__socketz00));
		     ADD_ROOT((void *) (&BGl_symbol2007z00zz__socketz00));
		     ADD_ROOT((void *) (&BGl_symbol2009z00zz__socketz00));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__socketz00));
		     ADD_ROOT((void *) (&BGl_keyword1977z00zz__socketz00));
		     ADD_ROOT((void *) (&BGl_keyword1979z00zz__socketz00));
		     ADD_ROOT((void *) (&BGl_keyword1988z00zz__socketz00));
		     ADD_ROOT((void *) (&BGl_symbol1948z00zz__socketz00));
		     ADD_ROOT((void *) (&BGl_keyword1995z00zz__socketz00));
		     ADD_ROOT((void *) (&BGl_keyword1997z00zz__socketz00));
		     ADD_ROOT((void *) (&BGl_symbol1959z00zz__socketz00));
		     ADD_ROOT((void *) (&BGl_symbol1966z00zz__socketz00));
		     ADD_ROOT((void *) (&BGl_symbol1968z00zz__socketz00));
		     ADD_ROOT((void *) (&BGl_symbol1971z00zz__socketz00));
		     ADD_ROOT((void *) (&BGl_symbol1973z00zz__socketz00));
		     ADD_ROOT((void *) (&BGl_list1950z00zz__socketz00));
		     ADD_ROOT((void *) (&BGl_symbol1990z00zz__socketz00));
		     ADD_ROOT((void *) (&BGl_symbol1999z00zz__socketz00));
		     ADD_ROOT((void *) (&BGl_za2socketzd2mutexza2zd2zz__socketz00));
		     ADD_ROOT((void *) (&BGl_list1976z00zz__socketz00));
		     ADD_ROOT((void *) (&BGl_list1987z00zz__socketz00));
		     ADD_ROOT((void *) (&BGl_list1994z00zz__socketz00));
		     ADD_ROOT((void *) (&BGl_keyword1951z00zz__socketz00));
		     ADD_ROOT((void *) (&BGl_keyword1953z00zz__socketz00));
		     ADD_ROOT((void *) (&BGl_keyword1955z00zz__socketz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__socketz00(long
		BgL_checksumz00_2695, char *BgL_fromz00_2696)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__socketz00))
				{
					BGl_requirezd2initializa7ationz75zz__socketz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__socketz00();
					BGl_cnstzd2initzd2zz__socketz00();
					BGl_importedzd2moduleszd2initz00zz__socketz00();
					return BGl_toplevelzd2initzd2zz__socketz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__socketz00(void)
	{
		{	/* Llib/socket.scm 14 */
			BGl_symbol1948z00zz__socketz00 =
				bstring_to_symbol(BGl_string1949z00zz__socketz00);
			BGl_keyword1951z00zz__socketz00 =
				bstring_to_keyword(BGl_string1952z00zz__socketz00);
			BGl_keyword1953z00zz__socketz00 =
				bstring_to_keyword(BGl_string1954z00zz__socketz00);
			BGl_keyword1955z00zz__socketz00 =
				bstring_to_keyword(BGl_string1956z00zz__socketz00);
			BGl_keyword1957z00zz__socketz00 =
				bstring_to_keyword(BGl_string1958z00zz__socketz00);
			BGl_list1950z00zz__socketz00 =
				MAKE_YOUNG_PAIR(BGl_keyword1951z00zz__socketz00,
				MAKE_YOUNG_PAIR(BGl_keyword1953z00zz__socketz00,
					MAKE_YOUNG_PAIR(BGl_keyword1955z00zz__socketz00,
						MAKE_YOUNG_PAIR(BGl_keyword1957z00zz__socketz00, BNIL))));
			BGl_symbol1959z00zz__socketz00 =
				bstring_to_symbol(BGl_string1960z00zz__socketz00);
			BGl_symbol1966z00zz__socketz00 =
				bstring_to_symbol(BGl_string1967z00zz__socketz00);
			BGl_symbol1968z00zz__socketz00 =
				bstring_to_symbol(BGl_string1969z00zz__socketz00);
			BGl_symbol1971z00zz__socketz00 =
				bstring_to_symbol(BGl_string1972z00zz__socketz00);
			BGl_symbol1973z00zz__socketz00 =
				bstring_to_symbol(BGl_string1974z00zz__socketz00);
			BGl_keyword1977z00zz__socketz00 =
				bstring_to_keyword(BGl_string1978z00zz__socketz00);
			BGl_keyword1979z00zz__socketz00 =
				bstring_to_keyword(BGl_string1980z00zz__socketz00);
			BGl_list1976z00zz__socketz00 =
				MAKE_YOUNG_PAIR(BGl_keyword1951z00zz__socketz00,
				MAKE_YOUNG_PAIR(BGl_keyword1977z00zz__socketz00,
					MAKE_YOUNG_PAIR(BGl_keyword1979z00zz__socketz00, BNIL)));
			BGl_keyword1988z00zz__socketz00 =
				bstring_to_keyword(BGl_string1989z00zz__socketz00);
			BGl_list1987z00zz__socketz00 =
				MAKE_YOUNG_PAIR(BGl_keyword1988z00zz__socketz00,
				MAKE_YOUNG_PAIR(BGl_keyword1953z00zz__socketz00,
					MAKE_YOUNG_PAIR(BGl_keyword1955z00zz__socketz00, BNIL)));
			BGl_symbol1990z00zz__socketz00 =
				bstring_to_symbol(BGl_string1991z00zz__socketz00);
			BGl_keyword1995z00zz__socketz00 =
				bstring_to_keyword(BGl_string1996z00zz__socketz00);
			BGl_keyword1997z00zz__socketz00 =
				bstring_to_keyword(BGl_string1998z00zz__socketz00);
			BGl_list1994z00zz__socketz00 =
				MAKE_YOUNG_PAIR(BGl_keyword1988z00zz__socketz00,
				MAKE_YOUNG_PAIR(BGl_keyword1995z00zz__socketz00,
					MAKE_YOUNG_PAIR(BGl_keyword1997z00zz__socketz00, BNIL)));
			BGl_symbol1999z00zz__socketz00 =
				bstring_to_symbol(BGl_string2000z00zz__socketz00);
			BGl_symbol2005z00zz__socketz00 =
				bstring_to_symbol(BGl_string2006z00zz__socketz00);
			BGl_symbol2007z00zz__socketz00 =
				bstring_to_symbol(BGl_string2008z00zz__socketz00);
			return (BGl_symbol2009z00zz__socketz00 =
				bstring_to_symbol(BGl_string2010z00zz__socketz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__socketz00(void)
	{
		{	/* Llib/socket.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__socketz00(void)
	{
		{	/* Llib/socket.scm 14 */
			BGl_za2socketzd2initializa7edza2z75zz__socketz00 = ((bool_t) 0);
			return (BGl_za2socketzd2mutexza2zd2zz__socketz00 =
				bgl_make_mutex(BGl_string1935z00zz__socketz00), BUNSPEC);
		}

	}



/* %socket-init! */
	BGL_EXPORTED_DEF obj_t BGl_z52socketzd2initz12z92zz__socketz00(void)
	{
		{	/* Llib/socket.scm 286 */
			{	/* Llib/socket.scm 287 */
				obj_t BgL_top2173z00_2740;

				BgL_top2173z00_2740 = BGL_EXITD_TOP_AS_OBJ();
				BGL_MUTEX_LOCK(BGl_za2socketzd2mutexza2zd2zz__socketz00);
				BGL_EXITD_PUSH_PROTECT(BgL_top2173z00_2740,
					BGl_za2socketzd2mutexza2zd2zz__socketz00);
				BUNSPEC;
				{	/* Llib/socket.scm 287 */
					obj_t BgL_tmp2172z00_2739;

					if (BGl_za2socketzd2initializa7edza2z75zz__socketz00)
						{	/* Llib/socket.scm 288 */
							BgL_tmp2172z00_2739 = BFALSE;
						}
					else
						{	/* Llib/socket.scm 288 */
							BGl_za2socketzd2initializa7edza2z75zz__socketz00 = ((bool_t) 1);
							socket_startup();
							BUNSPEC;
							BGl_registerzd2exitzd2functionz12z12zz__biglooz00
								(BGl_proc1936z00zz__socketz00);
							BgL_tmp2172z00_2739 = BUNSPEC;
						}
					BGL_EXITD_POP_PROTECT(BgL_top2173z00_2740);
					BUNSPEC;
					BGL_MUTEX_UNLOCK(BGl_za2socketzd2mutexza2zd2zz__socketz00);
					return BgL_tmp2172z00_2739;
				}
			}
		}

	}



/* &%socket-init! */
	obj_t BGl_z62z52socketzd2initz12zf0zz__socketz00(obj_t BgL_envz00_2386)
	{
		{	/* Llib/socket.scm 286 */
			return BGl_z52socketzd2initz12z92zz__socketz00();
		}

	}



/* &<@anonymous:1245> */
	obj_t BGl_z62zc3z04anonymousza31245ze3ze5zz__socketz00(obj_t BgL_envz00_2387,
		obj_t BgL_xz00_2388)
	{
		{	/* Llib/socket.scm 292 */
			socket_cleanup();
			BUNSPEC;
			return BgL_xz00_2388;
		}

	}



/* socket? */
	BGL_EXPORTED_DEF bool_t BGl_socketzf3zf3zz__socketz00(obj_t BgL_objz00_3)
	{
		{	/* Llib/socket.scm 302 */
			return SOCKETP(BgL_objz00_3);
		}

	}



/* &socket? */
	obj_t BGl_z62socketzf3z91zz__socketz00(obj_t BgL_envz00_2389,
		obj_t BgL_objz00_2390)
	{
		{	/* Llib/socket.scm 302 */
			return BBOOL(BGl_socketzf3zf3zz__socketz00(BgL_objz00_2390));
		}

	}



/* socket-server? */
	BGL_EXPORTED_DEF bool_t BGl_socketzd2serverzf3z21zz__socketz00(obj_t
		BgL_objz00_4)
	{
		{	/* Llib/socket.scm 308 */
			return BGL_SOCKET_SERVERP(BgL_objz00_4);
		}

	}



/* &socket-server? */
	obj_t BGl_z62socketzd2serverzf3z43zz__socketz00(obj_t BgL_envz00_2391,
		obj_t BgL_objz00_2392)
	{
		{	/* Llib/socket.scm 308 */
			return BBOOL(BGl_socketzd2serverzf3z21zz__socketz00(BgL_objz00_2392));
		}

	}



/* socket-client? */
	BGL_EXPORTED_DEF bool_t BGl_socketzd2clientzf3z21zz__socketz00(obj_t
		BgL_objz00_5)
	{
		{	/* Llib/socket.scm 314 */
			return BGL_SOCKET_CLIENTP(BgL_objz00_5);
		}

	}



/* &socket-client? */
	obj_t BGl_z62socketzd2clientzf3z43zz__socketz00(obj_t BgL_envz00_2393,
		obj_t BgL_objz00_2394)
	{
		{	/* Llib/socket.scm 314 */
			return BBOOL(BGl_socketzd2clientzf3z21zz__socketz00(BgL_objz00_2394));
		}

	}



/* socket-hostname */
	BGL_EXPORTED_DEF obj_t BGl_socketzd2hostnamezd2zz__socketz00(obj_t
		BgL_socketz00_6)
	{
		{	/* Llib/socket.scm 320 */
			return SOCKET_HOSTNAME(BgL_socketz00_6);
		}

	}



/* &socket-hostname */
	obj_t BGl_z62socketzd2hostnamezb0zz__socketz00(obj_t BgL_envz00_2395,
		obj_t BgL_socketz00_2396)
	{
		{	/* Llib/socket.scm 320 */
			{	/* Llib/socket.scm 321 */
				obj_t BgL_auxz00_2761;

				if (SOCKETP(BgL_socketz00_2396))
					{	/* Llib/socket.scm 321 */
						BgL_auxz00_2761 = BgL_socketz00_2396;
					}
				else
					{
						obj_t BgL_auxz00_2764;

						BgL_auxz00_2764 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1937z00zz__socketz00,
							BINT(14228L), BGl_string1938z00zz__socketz00,
							BGl_string1935z00zz__socketz00, BgL_socketz00_2396);
						FAILURE(BgL_auxz00_2764, BFALSE, BFALSE);
					}
				return BGl_socketzd2hostnamezd2zz__socketz00(BgL_auxz00_2761);
			}
		}

	}



/* socket-host-address */
	BGL_EXPORTED_DEF obj_t BGl_socketzd2hostzd2addressz00zz__socketz00(obj_t
		BgL_socketz00_7)
	{
		{	/* Llib/socket.scm 326 */
			BGL_TAIL return bgl_socket_host_addr(BgL_socketz00_7);
		}

	}



/* &socket-host-address */
	obj_t BGl_z62socketzd2hostzd2addressz62zz__socketz00(obj_t BgL_envz00_2397,
		obj_t BgL_socketz00_2398)
	{
		{	/* Llib/socket.scm 326 */
			{	/* Llib/socket.scm 327 */
				obj_t BgL_auxz00_2770;

				if (SOCKETP(BgL_socketz00_2398))
					{	/* Llib/socket.scm 327 */
						BgL_auxz00_2770 = BgL_socketz00_2398;
					}
				else
					{
						obj_t BgL_auxz00_2773;

						BgL_auxz00_2773 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1937z00zz__socketz00,
							BINT(14526L), BGl_string1939z00zz__socketz00,
							BGl_string1935z00zz__socketz00, BgL_socketz00_2398);
						FAILURE(BgL_auxz00_2773, BFALSE, BFALSE);
					}
				return BGl_socketzd2hostzd2addressz00zz__socketz00(BgL_auxz00_2770);
			}
		}

	}



/* socket-local-address */
	BGL_EXPORTED_DEF obj_t BGl_socketzd2localzd2addressz00zz__socketz00(obj_t
		BgL_socketz00_8)
	{
		{	/* Llib/socket.scm 332 */
			BGL_TAIL return bgl_socket_local_addr(BgL_socketz00_8);
		}

	}



/* &socket-local-address */
	obj_t BGl_z62socketzd2localzd2addressz62zz__socketz00(obj_t BgL_envz00_2399,
		obj_t BgL_socketz00_2400)
	{
		{	/* Llib/socket.scm 332 */
			{	/* Llib/socket.scm 333 */
				obj_t BgL_auxz00_2779;

				if (SOCKETP(BgL_socketz00_2400))
					{	/* Llib/socket.scm 333 */
						BgL_auxz00_2779 = BgL_socketz00_2400;
					}
				else
					{
						obj_t BgL_auxz00_2782;

						BgL_auxz00_2782 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1937z00zz__socketz00,
							BINT(14833L), BGl_string1940z00zz__socketz00,
							BGl_string1935z00zz__socketz00, BgL_socketz00_2400);
						FAILURE(BgL_auxz00_2782, BFALSE, BFALSE);
					}
				return BGl_socketzd2localzd2addressz00zz__socketz00(BgL_auxz00_2779);
			}
		}

	}



/* socket-local? */
	BGL_EXPORTED_DEF bool_t BGl_socketzd2localzf3z21zz__socketz00(obj_t
		BgL_socketz00_9)
	{
		{	/* Llib/socket.scm 338 */
			BGL_TAIL return bgl_socket_localp(BgL_socketz00_9);
		}

	}



/* &socket-local? */
	obj_t BGl_z62socketzd2localzf3z43zz__socketz00(obj_t BgL_envz00_2401,
		obj_t BgL_socketz00_2402)
	{
		{	/* Llib/socket.scm 338 */
			{	/* Llib/socket.scm 341 */
				bool_t BgL_tmpz00_2788;

				{	/* Llib/socket.scm 341 */
					obj_t BgL_auxz00_2789;

					if (SOCKETP(BgL_socketz00_2402))
						{	/* Llib/socket.scm 341 */
							BgL_auxz00_2789 = BgL_socketz00_2402;
						}
					else
						{
							obj_t BgL_auxz00_2792;

							BgL_auxz00_2792 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1937z00zz__socketz00,
								BINT(15170L), BGl_string1941z00zz__socketz00,
								BGl_string1935z00zz__socketz00, BgL_socketz00_2402);
							FAILURE(BgL_auxz00_2792, BFALSE, BFALSE);
						}
					BgL_tmpz00_2788 =
						BGl_socketzd2localzf3z21zz__socketz00(BgL_auxz00_2789);
				}
				return BBOOL(BgL_tmpz00_2788);
			}
		}

	}



/* socket-host-address=? */
	BGL_EXPORTED_DEF bool_t
		BGl_socketzd2hostzd2addresszd3zf3z20zz__socketz00(obj_t BgL_socketz00_10,
		obj_t BgL_addrz00_11)
	{
		{	/* Llib/socket.scm 348 */
			BGL_TAIL return
				bgl_socket_host_addr_cmp(BgL_socketz00_10, BgL_addrz00_11);
		}

	}



/* &socket-host-address=? */
	obj_t BGl_z62socketzd2hostzd2addresszd3zf3z42zz__socketz00(obj_t
		BgL_envz00_2403, obj_t BgL_socketz00_2404, obj_t BgL_addrz00_2405)
	{
		{	/* Llib/socket.scm 348 */
			{	/* Llib/socket.scm 351 */
				bool_t BgL_tmpz00_2799;

				{	/* Llib/socket.scm 351 */
					obj_t BgL_auxz00_2807;
					obj_t BgL_auxz00_2800;

					if (STRINGP(BgL_addrz00_2405))
						{	/* Llib/socket.scm 351 */
							BgL_auxz00_2807 = BgL_addrz00_2405;
						}
					else
						{
							obj_t BgL_auxz00_2810;

							BgL_auxz00_2810 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1937z00zz__socketz00,
								BINT(15624L), BGl_string1942z00zz__socketz00,
								BGl_string1943z00zz__socketz00, BgL_addrz00_2405);
							FAILURE(BgL_auxz00_2810, BFALSE, BFALSE);
						}
					if (SOCKETP(BgL_socketz00_2404))
						{	/* Llib/socket.scm 351 */
							BgL_auxz00_2800 = BgL_socketz00_2404;
						}
					else
						{
							obj_t BgL_auxz00_2803;

							BgL_auxz00_2803 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1937z00zz__socketz00,
								BINT(15624L), BGl_string1942z00zz__socketz00,
								BGl_string1935z00zz__socketz00, BgL_socketz00_2404);
							FAILURE(BgL_auxz00_2803, BFALSE, BFALSE);
						}
					BgL_tmpz00_2799 =
						BGl_socketzd2hostzd2addresszd3zf3z20zz__socketz00(BgL_auxz00_2800,
						BgL_auxz00_2807);
				}
				return BBOOL(BgL_tmpz00_2799);
			}
		}

	}



/* socket-down? */
	BGL_EXPORTED_DEF bool_t BGl_socketzd2downzf3z21zz__socketz00(obj_t
		BgL_socketz00_12)
	{
		{	/* Llib/socket.scm 358 */
			return SOCKET_DOWNP(BgL_socketz00_12);
		}

	}



/* &socket-down? */
	obj_t BGl_z62socketzd2downzf3z43zz__socketz00(obj_t BgL_envz00_2406,
		obj_t BgL_socketz00_2407)
	{
		{	/* Llib/socket.scm 358 */
			{	/* Llib/socket.scm 359 */
				bool_t BgL_tmpz00_2817;

				{	/* Llib/socket.scm 359 */
					obj_t BgL_auxz00_2818;

					if (SOCKETP(BgL_socketz00_2407))
						{	/* Llib/socket.scm 359 */
							BgL_auxz00_2818 = BgL_socketz00_2407;
						}
					else
						{
							obj_t BgL_auxz00_2821;

							BgL_auxz00_2821 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1937z00zz__socketz00,
								BINT(16009L), BGl_string1944z00zz__socketz00,
								BGl_string1935z00zz__socketz00, BgL_socketz00_2407);
							FAILURE(BgL_auxz00_2821, BFALSE, BFALSE);
						}
					BgL_tmpz00_2817 =
						BGl_socketzd2downzf3z21zz__socketz00(BgL_auxz00_2818);
				}
				return BBOOL(BgL_tmpz00_2817);
			}
		}

	}



/* socket-port-number */
	BGL_EXPORTED_DEF obj_t BGl_socketzd2portzd2numberz00zz__socketz00(obj_t
		BgL_socketz00_13)
	{
		{	/* Llib/socket.scm 364 */
			return BINT(SOCKET_PORT(BgL_socketz00_13));
		}

	}



/* &socket-port-number */
	obj_t BGl_z62socketzd2portzd2numberz62zz__socketz00(obj_t BgL_envz00_2408,
		obj_t BgL_socketz00_2409)
	{
		{	/* Llib/socket.scm 364 */
			{	/* Llib/socket.scm 365 */
				obj_t BgL_auxz00_2829;

				if (SOCKETP(BgL_socketz00_2409))
					{	/* Llib/socket.scm 365 */
						BgL_auxz00_2829 = BgL_socketz00_2409;
					}
				else
					{
						obj_t BgL_auxz00_2832;

						BgL_auxz00_2832 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1937z00zz__socketz00,
							BINT(16317L), BGl_string1945z00zz__socketz00,
							BGl_string1935z00zz__socketz00, BgL_socketz00_2409);
						FAILURE(BgL_auxz00_2832, BFALSE, BFALSE);
					}
				return BGl_socketzd2portzd2numberz00zz__socketz00(BgL_auxz00_2829);
			}
		}

	}



/* socket-input */
	BGL_EXPORTED_DEF obj_t BGl_socketzd2inputzd2zz__socketz00(obj_t
		BgL_socketz00_14)
	{
		{	/* Llib/socket.scm 370 */
			return SOCKET_INPUT(BgL_socketz00_14);
		}

	}



/* &socket-input */
	obj_t BGl_z62socketzd2inputzb0zz__socketz00(obj_t BgL_envz00_2410,
		obj_t BgL_socketz00_2411)
	{
		{	/* Llib/socket.scm 370 */
			{	/* Llib/socket.scm 371 */
				obj_t BgL_auxz00_2838;

				if (SOCKETP(BgL_socketz00_2411))
					{	/* Llib/socket.scm 371 */
						BgL_auxz00_2838 = BgL_socketz00_2411;
					}
				else
					{
						obj_t BgL_auxz00_2841;

						BgL_auxz00_2841 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1937z00zz__socketz00,
							BINT(16619L), BGl_string1946z00zz__socketz00,
							BGl_string1935z00zz__socketz00, BgL_socketz00_2411);
						FAILURE(BgL_auxz00_2841, BFALSE, BFALSE);
					}
				return BGl_socketzd2inputzd2zz__socketz00(BgL_auxz00_2838);
			}
		}

	}



/* socket-output */
	BGL_EXPORTED_DEF obj_t BGl_socketzd2outputzd2zz__socketz00(obj_t
		BgL_socketz00_15)
	{
		{	/* Llib/socket.scm 376 */
			return SOCKET_OUTPUT(BgL_socketz00_15);
		}

	}



/* &socket-output */
	obj_t BGl_z62socketzd2outputzb0zz__socketz00(obj_t BgL_envz00_2412,
		obj_t BgL_socketz00_2413)
	{
		{	/* Llib/socket.scm 376 */
			{	/* Llib/socket.scm 377 */
				obj_t BgL_auxz00_2847;

				if (SOCKETP(BgL_socketz00_2413))
					{	/* Llib/socket.scm 377 */
						BgL_auxz00_2847 = BgL_socketz00_2413;
					}
				else
					{
						obj_t BgL_auxz00_2850;

						BgL_auxz00_2850 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1937z00zz__socketz00,
							BINT(16916L), BGl_string1947z00zz__socketz00,
							BGl_string1935z00zz__socketz00, BgL_socketz00_2413);
						FAILURE(BgL_auxz00_2850, BFALSE, BFALSE);
					}
				return BGl_socketzd2outputzd2zz__socketz00(BgL_auxz00_2847);
			}
		}

	}



/* _make-client-socket */
	obj_t BGl__makezd2clientzd2socketz00zz__socketz00(obj_t BgL_env1115z00_23,
		obj_t BgL_opt1114z00_22)
	{
		{	/* Llib/socket.scm 382 */
			{	/* Llib/socket.scm 382 */
				obj_t BgL_g1124z00_1205;
				obj_t BgL_g1125z00_1206;

				BgL_g1124z00_1205 = VECTOR_REF(BgL_opt1114z00_22, 0L);
				BgL_g1125z00_1206 = VECTOR_REF(BgL_opt1114z00_22, 1L);
				{	/* Llib/socket.scm 382 */
					obj_t BgL_domainz00_1207;

					BgL_domainz00_1207 = BGl_symbol1948z00zz__socketz00;
					{	/* Llib/socket.scm 382 */
						obj_t BgL_inbufz00_1208;

						BgL_inbufz00_1208 = BTRUE;
						{	/* Llib/socket.scm 382 */
							obj_t BgL_outbufz00_1209;

							BgL_outbufz00_1209 = BTRUE;
							{	/* Llib/socket.scm 382 */
								obj_t BgL_timeoutz00_1210;

								BgL_timeoutz00_1210 = BINT(0L);
								{	/* Llib/socket.scm 382 */

									{
										long BgL_iz00_1211;

										BgL_iz00_1211 = 2L;
									BgL_check1118z00_1212:
										if ((BgL_iz00_1211 == VECTOR_LENGTH(BgL_opt1114z00_22)))
											{	/* Llib/socket.scm 382 */
												BNIL;
											}
										else
											{	/* Llib/socket.scm 382 */
												bool_t BgL_test2186z00_2861;

												{	/* Llib/socket.scm 382 */
													obj_t BgL_arg1272z00_1218;

													BgL_arg1272z00_1218 =
														VECTOR_REF(BgL_opt1114z00_22, BgL_iz00_1211);
													BgL_test2186z00_2861 =
														CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
														(BgL_arg1272z00_1218,
															BGl_list1950z00zz__socketz00));
												}
												if (BgL_test2186z00_2861)
													{
														long BgL_iz00_2865;

														BgL_iz00_2865 = (BgL_iz00_1211 + 2L);
														BgL_iz00_1211 = BgL_iz00_2865;
														goto BgL_check1118z00_1212;
													}
												else
													{	/* Llib/socket.scm 382 */
														obj_t BgL_arg1268z00_1217;

														BgL_arg1268z00_1217 =
															VECTOR_REF(BgL_opt1114z00_22, BgL_iz00_1211);
														BGl_errorz00zz__errorz00
															(BGl_symbol1959z00zz__socketz00,
															BGl_string1961z00zz__socketz00,
															BgL_arg1268z00_1217);
													}
											}
									}
									{	/* Llib/socket.scm 382 */
										obj_t BgL_index1120z00_1219;

										{
											long BgL_iz00_1862;

											BgL_iz00_1862 = 2L;
										BgL_search1117z00_1861:
											if ((BgL_iz00_1862 == VECTOR_LENGTH(BgL_opt1114z00_22)))
												{	/* Llib/socket.scm 382 */
													BgL_index1120z00_1219 = BINT(-1L);
												}
											else
												{	/* Llib/socket.scm 382 */
													if (
														(BgL_iz00_1862 ==
															(VECTOR_LENGTH(BgL_opt1114z00_22) - 1L)))
														{	/* Llib/socket.scm 382 */
															BgL_index1120z00_1219 =
																BGl_errorz00zz__errorz00
																(BGl_symbol1959z00zz__socketz00,
																BGl_string1962z00zz__socketz00,
																BINT(VECTOR_LENGTH(BgL_opt1114z00_22)));
														}
													else
														{	/* Llib/socket.scm 382 */
															obj_t BgL_vz00_1872;

															BgL_vz00_1872 =
																VECTOR_REF(BgL_opt1114z00_22, BgL_iz00_1862);
															if (
																(BgL_vz00_1872 ==
																	BGl_keyword1951z00zz__socketz00))
																{	/* Llib/socket.scm 382 */
																	BgL_index1120z00_1219 =
																		BINT((BgL_iz00_1862 + 1L));
																}
															else
																{
																	long BgL_iz00_2885;

																	BgL_iz00_2885 = (BgL_iz00_1862 + 2L);
																	BgL_iz00_1862 = BgL_iz00_2885;
																	goto BgL_search1117z00_1861;
																}
														}
												}
										}
										{	/* Llib/socket.scm 382 */
											bool_t BgL_test2190z00_2887;

											{	/* Llib/socket.scm 382 */
												long BgL_n1z00_1876;

												{	/* Llib/socket.scm 382 */
													obj_t BgL_tmpz00_2888;

													if (INTEGERP(BgL_index1120z00_1219))
														{	/* Llib/socket.scm 382 */
															BgL_tmpz00_2888 = BgL_index1120z00_1219;
														}
													else
														{
															obj_t BgL_auxz00_2891;

															BgL_auxz00_2891 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string1937z00zz__socketz00, BINT(17165L),
																BGl_string1963z00zz__socketz00,
																BGl_string1964z00zz__socketz00,
																BgL_index1120z00_1219);
															FAILURE(BgL_auxz00_2891, BFALSE, BFALSE);
														}
													BgL_n1z00_1876 = (long) CINT(BgL_tmpz00_2888);
												}
												BgL_test2190z00_2887 = (BgL_n1z00_1876 >= 0L);
											}
											if (BgL_test2190z00_2887)
												{
													long BgL_auxz00_2897;

													{	/* Llib/socket.scm 382 */
														obj_t BgL_tmpz00_2898;

														if (INTEGERP(BgL_index1120z00_1219))
															{	/* Llib/socket.scm 382 */
																BgL_tmpz00_2898 = BgL_index1120z00_1219;
															}
														else
															{
																obj_t BgL_auxz00_2901;

																BgL_auxz00_2901 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string1937z00zz__socketz00, BINT(17165L),
																	BGl_string1963z00zz__socketz00,
																	BGl_string1964z00zz__socketz00,
																	BgL_index1120z00_1219);
																FAILURE(BgL_auxz00_2901, BFALSE, BFALSE);
															}
														BgL_auxz00_2897 = (long) CINT(BgL_tmpz00_2898);
													}
													BgL_domainz00_1207 =
														VECTOR_REF(BgL_opt1114z00_22, BgL_auxz00_2897);
												}
											else
												{	/* Llib/socket.scm 382 */
													BFALSE;
												}
										}
									}
									{	/* Llib/socket.scm 382 */
										obj_t BgL_index1121z00_1221;

										{
											long BgL_iz00_1878;

											BgL_iz00_1878 = 2L;
										BgL_search1117z00_1877:
											if ((BgL_iz00_1878 == VECTOR_LENGTH(BgL_opt1114z00_22)))
												{	/* Llib/socket.scm 382 */
													BgL_index1121z00_1221 = BINT(-1L);
												}
											else
												{	/* Llib/socket.scm 382 */
													if (
														(BgL_iz00_1878 ==
															(VECTOR_LENGTH(BgL_opt1114z00_22) - 1L)))
														{	/* Llib/socket.scm 382 */
															BgL_index1121z00_1221 =
																BGl_errorz00zz__errorz00
																(BGl_symbol1959z00zz__socketz00,
																BGl_string1962z00zz__socketz00,
																BINT(VECTOR_LENGTH(BgL_opt1114z00_22)));
														}
													else
														{	/* Llib/socket.scm 382 */
															obj_t BgL_vz00_1888;

															BgL_vz00_1888 =
																VECTOR_REF(BgL_opt1114z00_22, BgL_iz00_1878);
															if (
																(BgL_vz00_1888 ==
																	BGl_keyword1953z00zz__socketz00))
																{	/* Llib/socket.scm 382 */
																	BgL_index1121z00_1221 =
																		BINT((BgL_iz00_1878 + 1L));
																}
															else
																{
																	long BgL_iz00_2923;

																	BgL_iz00_2923 = (BgL_iz00_1878 + 2L);
																	BgL_iz00_1878 = BgL_iz00_2923;
																	goto BgL_search1117z00_1877;
																}
														}
												}
										}
										{	/* Llib/socket.scm 382 */
											bool_t BgL_test2196z00_2925;

											{	/* Llib/socket.scm 382 */
												long BgL_n1z00_1892;

												{	/* Llib/socket.scm 382 */
													obj_t BgL_tmpz00_2926;

													if (INTEGERP(BgL_index1121z00_1221))
														{	/* Llib/socket.scm 382 */
															BgL_tmpz00_2926 = BgL_index1121z00_1221;
														}
													else
														{
															obj_t BgL_auxz00_2929;

															BgL_auxz00_2929 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string1937z00zz__socketz00, BINT(17165L),
																BGl_string1963z00zz__socketz00,
																BGl_string1964z00zz__socketz00,
																BgL_index1121z00_1221);
															FAILURE(BgL_auxz00_2929, BFALSE, BFALSE);
														}
													BgL_n1z00_1892 = (long) CINT(BgL_tmpz00_2926);
												}
												BgL_test2196z00_2925 = (BgL_n1z00_1892 >= 0L);
											}
											if (BgL_test2196z00_2925)
												{
													long BgL_auxz00_2935;

													{	/* Llib/socket.scm 382 */
														obj_t BgL_tmpz00_2936;

														if (INTEGERP(BgL_index1121z00_1221))
															{	/* Llib/socket.scm 382 */
																BgL_tmpz00_2936 = BgL_index1121z00_1221;
															}
														else
															{
																obj_t BgL_auxz00_2939;

																BgL_auxz00_2939 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string1937z00zz__socketz00, BINT(17165L),
																	BGl_string1963z00zz__socketz00,
																	BGl_string1964z00zz__socketz00,
																	BgL_index1121z00_1221);
																FAILURE(BgL_auxz00_2939, BFALSE, BFALSE);
															}
														BgL_auxz00_2935 = (long) CINT(BgL_tmpz00_2936);
													}
													BgL_inbufz00_1208 =
														VECTOR_REF(BgL_opt1114z00_22, BgL_auxz00_2935);
												}
											else
												{	/* Llib/socket.scm 382 */
													BFALSE;
												}
										}
									}
									{	/* Llib/socket.scm 382 */
										obj_t BgL_index1122z00_1223;

										{
											long BgL_iz00_1894;

											BgL_iz00_1894 = 2L;
										BgL_search1117z00_1893:
											if ((BgL_iz00_1894 == VECTOR_LENGTH(BgL_opt1114z00_22)))
												{	/* Llib/socket.scm 382 */
													BgL_index1122z00_1223 = BINT(-1L);
												}
											else
												{	/* Llib/socket.scm 382 */
													if (
														(BgL_iz00_1894 ==
															(VECTOR_LENGTH(BgL_opt1114z00_22) - 1L)))
														{	/* Llib/socket.scm 382 */
															BgL_index1122z00_1223 =
																BGl_errorz00zz__errorz00
																(BGl_symbol1959z00zz__socketz00,
																BGl_string1962z00zz__socketz00,
																BINT(VECTOR_LENGTH(BgL_opt1114z00_22)));
														}
													else
														{	/* Llib/socket.scm 382 */
															obj_t BgL_vz00_1904;

															BgL_vz00_1904 =
																VECTOR_REF(BgL_opt1114z00_22, BgL_iz00_1894);
															if (
																(BgL_vz00_1904 ==
																	BGl_keyword1955z00zz__socketz00))
																{	/* Llib/socket.scm 382 */
																	BgL_index1122z00_1223 =
																		BINT((BgL_iz00_1894 + 1L));
																}
															else
																{
																	long BgL_iz00_2961;

																	BgL_iz00_2961 = (BgL_iz00_1894 + 2L);
																	BgL_iz00_1894 = BgL_iz00_2961;
																	goto BgL_search1117z00_1893;
																}
														}
												}
										}
										{	/* Llib/socket.scm 382 */
											bool_t BgL_test2202z00_2963;

											{	/* Llib/socket.scm 382 */
												long BgL_n1z00_1908;

												{	/* Llib/socket.scm 382 */
													obj_t BgL_tmpz00_2964;

													if (INTEGERP(BgL_index1122z00_1223))
														{	/* Llib/socket.scm 382 */
															BgL_tmpz00_2964 = BgL_index1122z00_1223;
														}
													else
														{
															obj_t BgL_auxz00_2967;

															BgL_auxz00_2967 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string1937z00zz__socketz00, BINT(17165L),
																BGl_string1963z00zz__socketz00,
																BGl_string1964z00zz__socketz00,
																BgL_index1122z00_1223);
															FAILURE(BgL_auxz00_2967, BFALSE, BFALSE);
														}
													BgL_n1z00_1908 = (long) CINT(BgL_tmpz00_2964);
												}
												BgL_test2202z00_2963 = (BgL_n1z00_1908 >= 0L);
											}
											if (BgL_test2202z00_2963)
												{
													long BgL_auxz00_2973;

													{	/* Llib/socket.scm 382 */
														obj_t BgL_tmpz00_2974;

														if (INTEGERP(BgL_index1122z00_1223))
															{	/* Llib/socket.scm 382 */
																BgL_tmpz00_2974 = BgL_index1122z00_1223;
															}
														else
															{
																obj_t BgL_auxz00_2977;

																BgL_auxz00_2977 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string1937z00zz__socketz00, BINT(17165L),
																	BGl_string1963z00zz__socketz00,
																	BGl_string1964z00zz__socketz00,
																	BgL_index1122z00_1223);
																FAILURE(BgL_auxz00_2977, BFALSE, BFALSE);
															}
														BgL_auxz00_2973 = (long) CINT(BgL_tmpz00_2974);
													}
													BgL_outbufz00_1209 =
														VECTOR_REF(BgL_opt1114z00_22, BgL_auxz00_2973);
												}
											else
												{	/* Llib/socket.scm 382 */
													BFALSE;
												}
										}
									}
									{	/* Llib/socket.scm 382 */
										obj_t BgL_index1123z00_1225;

										{
											long BgL_iz00_1910;

											BgL_iz00_1910 = 2L;
										BgL_search1117z00_1909:
											if ((BgL_iz00_1910 == VECTOR_LENGTH(BgL_opt1114z00_22)))
												{	/* Llib/socket.scm 382 */
													BgL_index1123z00_1225 = BINT(-1L);
												}
											else
												{	/* Llib/socket.scm 382 */
													if (
														(BgL_iz00_1910 ==
															(VECTOR_LENGTH(BgL_opt1114z00_22) - 1L)))
														{	/* Llib/socket.scm 382 */
															BgL_index1123z00_1225 =
																BGl_errorz00zz__errorz00
																(BGl_symbol1959z00zz__socketz00,
																BGl_string1962z00zz__socketz00,
																BINT(VECTOR_LENGTH(BgL_opt1114z00_22)));
														}
													else
														{	/* Llib/socket.scm 382 */
															obj_t BgL_vz00_1920;

															BgL_vz00_1920 =
																VECTOR_REF(BgL_opt1114z00_22, BgL_iz00_1910);
															if (
																(BgL_vz00_1920 ==
																	BGl_keyword1957z00zz__socketz00))
																{	/* Llib/socket.scm 382 */
																	BgL_index1123z00_1225 =
																		BINT((BgL_iz00_1910 + 1L));
																}
															else
																{
																	long BgL_iz00_2999;

																	BgL_iz00_2999 = (BgL_iz00_1910 + 2L);
																	BgL_iz00_1910 = BgL_iz00_2999;
																	goto BgL_search1117z00_1909;
																}
														}
												}
										}
										{	/* Llib/socket.scm 382 */
											bool_t BgL_test2208z00_3001;

											{	/* Llib/socket.scm 382 */
												long BgL_n1z00_1924;

												{	/* Llib/socket.scm 382 */
													obj_t BgL_tmpz00_3002;

													if (INTEGERP(BgL_index1123z00_1225))
														{	/* Llib/socket.scm 382 */
															BgL_tmpz00_3002 = BgL_index1123z00_1225;
														}
													else
														{
															obj_t BgL_auxz00_3005;

															BgL_auxz00_3005 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string1937z00zz__socketz00, BINT(17165L),
																BGl_string1963z00zz__socketz00,
																BGl_string1964z00zz__socketz00,
																BgL_index1123z00_1225);
															FAILURE(BgL_auxz00_3005, BFALSE, BFALSE);
														}
													BgL_n1z00_1924 = (long) CINT(BgL_tmpz00_3002);
												}
												BgL_test2208z00_3001 = (BgL_n1z00_1924 >= 0L);
											}
											if (BgL_test2208z00_3001)
												{
													long BgL_auxz00_3011;

													{	/* Llib/socket.scm 382 */
														obj_t BgL_tmpz00_3012;

														if (INTEGERP(BgL_index1123z00_1225))
															{	/* Llib/socket.scm 382 */
																BgL_tmpz00_3012 = BgL_index1123z00_1225;
															}
														else
															{
																obj_t BgL_auxz00_3015;

																BgL_auxz00_3015 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string1937z00zz__socketz00, BINT(17165L),
																	BGl_string1963z00zz__socketz00,
																	BGl_string1964z00zz__socketz00,
																	BgL_index1123z00_1225);
																FAILURE(BgL_auxz00_3015, BFALSE, BFALSE);
															}
														BgL_auxz00_3011 = (long) CINT(BgL_tmpz00_3012);
													}
													BgL_timeoutz00_1210 =
														VECTOR_REF(BgL_opt1114z00_22, BgL_auxz00_3011);
												}
											else
												{	/* Llib/socket.scm 382 */
													BFALSE;
												}
										}
									}
									{	/* Llib/socket.scm 382 */
										obj_t BgL_arg1284z00_1227;
										obj_t BgL_arg1304z00_1228;

										BgL_arg1284z00_1227 = VECTOR_REF(BgL_opt1114z00_22, 0L);
										BgL_arg1304z00_1228 = VECTOR_REF(BgL_opt1114z00_22, 1L);
										{	/* Llib/socket.scm 382 */
											obj_t BgL_domainz00_1229;

											BgL_domainz00_1229 = BgL_domainz00_1207;
											{	/* Llib/socket.scm 382 */
												obj_t BgL_inbufz00_1230;

												BgL_inbufz00_1230 = BgL_inbufz00_1208;
												{	/* Llib/socket.scm 382 */
													obj_t BgL_outbufz00_1231;

													BgL_outbufz00_1231 = BgL_outbufz00_1209;
													{	/* Llib/socket.scm 382 */
														obj_t BgL_timeoutz00_1232;

														BgL_timeoutz00_1232 = BgL_timeoutz00_1210;
														{	/* Llib/socket.scm 382 */
															obj_t BgL_res1780z00_1935;

															{	/* Llib/socket.scm 382 */
																obj_t BgL_hostz00_1925;
																int BgL_portz00_1926;

																if (STRINGP(BgL_arg1284z00_1227))
																	{	/* Llib/socket.scm 382 */
																		BgL_hostz00_1925 = BgL_arg1284z00_1227;
																	}
																else
																	{
																		obj_t BgL_auxz00_3025;

																		BgL_auxz00_3025 =
																			BGl_typezd2errorzd2zz__errorz00
																			(BGl_string1937z00zz__socketz00,
																			BINT(17165L),
																			BGl_string1963z00zz__socketz00,
																			BGl_string1943z00zz__socketz00,
																			BgL_arg1284z00_1227);
																		FAILURE(BgL_auxz00_3025, BFALSE, BFALSE);
																	}
																{	/* Llib/socket.scm 382 */
																	obj_t BgL_tmpz00_3029;

																	if (INTEGERP(BgL_arg1304z00_1228))
																		{	/* Llib/socket.scm 382 */
																			BgL_tmpz00_3029 = BgL_arg1304z00_1228;
																		}
																	else
																		{
																			obj_t BgL_auxz00_3032;

																			BgL_auxz00_3032 =
																				BGl_typezd2errorzd2zz__errorz00
																				(BGl_string1937z00zz__socketz00,
																				BINT(17165L),
																				BGl_string1963z00zz__socketz00,
																				BGl_string1964z00zz__socketz00,
																				BgL_arg1304z00_1228);
																			FAILURE(BgL_auxz00_3032, BFALSE, BFALSE);
																		}
																	BgL_portz00_1926 = CINT(BgL_tmpz00_3029);
																}
																BGl_z52socketzd2initz12z92zz__socketz00();
																{	/* Llib/socket.scm 384 */
																	obj_t BgL_inbufz00_1927;
																	obj_t BgL_outbufz00_1928;

																	BgL_inbufz00_1927 =
																		BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00
																		(BGl_string1965z00zz__socketz00,
																		BgL_inbufz00_1230, (int) (512L));
																	BgL_outbufz00_1928 =
																		BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00
																		(BGl_string1965z00zz__socketz00,
																		BgL_outbufz00_1231, (int) (1024L));
																	{	/* Llib/socket.scm 386 */
																		bool_t BgL_test2213z00_3042;

																		{	/* Llib/socket.scm 386 */
																			bool_t BgL__ortest_1040z00_1931;

																			BgL__ortest_1040z00_1931 =
																				(BgL_domainz00_1229 ==
																				BGl_symbol1948z00zz__socketz00);
																			if (BgL__ortest_1040z00_1931)
																				{	/* Llib/socket.scm 386 */
																					BgL_test2213z00_3042 =
																						BgL__ortest_1040z00_1931;
																				}
																			else
																				{	/* Llib/socket.scm 386 */
																					bool_t BgL__ortest_1041z00_1932;

																					BgL__ortest_1041z00_1932 =
																						(BgL_domainz00_1229 ==
																						BGl_symbol1966z00zz__socketz00);
																					if (BgL__ortest_1041z00_1932)
																						{	/* Llib/socket.scm 386 */
																							BgL_test2213z00_3042 =
																								BgL__ortest_1041z00_1932;
																						}
																					else
																						{	/* Llib/socket.scm 386 */
																							BgL_test2213z00_3042 =
																								(BgL_domainz00_1229 ==
																								BGl_symbol1968z00zz__socketz00);
																						}
																				}
																		}
																		if (BgL_test2213z00_3042)
																			{	/* Llib/socket.scm 388 */
																				obj_t BgL_auxz00_3057;
																				int BgL_tmpz00_3048;

																				if (SYMBOLP(BgL_domainz00_1229))
																					{	/* Llib/socket.scm 388 */
																						BgL_auxz00_3057 =
																							BgL_domainz00_1229;
																					}
																				else
																					{
																						obj_t BgL_auxz00_3060;

																						BgL_auxz00_3060 =
																							BGl_typezd2errorzd2zz__errorz00
																							(BGl_string1937z00zz__socketz00,
																							BINT(17513L),
																							BGl_string1963z00zz__socketz00,
																							BGl_string1970z00zz__socketz00,
																							BgL_domainz00_1229);
																						FAILURE(BgL_auxz00_3060, BFALSE,
																							BFALSE);
																					}
																				{	/* Llib/socket.scm 388 */
																					obj_t BgL_tmpz00_3049;

																					if (INTEGERP(BgL_timeoutz00_1232))
																						{	/* Llib/socket.scm 388 */
																							BgL_tmpz00_3049 =
																								BgL_timeoutz00_1232;
																						}
																					else
																						{
																							obj_t BgL_auxz00_3052;

																							BgL_auxz00_3052 =
																								BGl_typezd2errorzd2zz__errorz00
																								(BGl_string1937z00zz__socketz00,
																								BINT(17492L),
																								BGl_string1963z00zz__socketz00,
																								BGl_string1964z00zz__socketz00,
																								BgL_timeoutz00_1232);
																							FAILURE(BgL_auxz00_3052, BFALSE,
																								BFALSE);
																						}
																					BgL_tmpz00_3048 =
																						CINT(BgL_tmpz00_3049);
																				}
																				BgL_res1780z00_1935 =
																					bgl_make_client_socket
																					(BgL_hostz00_1925, BgL_portz00_1926,
																					BgL_tmpz00_3048, BgL_inbufz00_1927,
																					BgL_outbufz00_1928, BgL_auxz00_3057);
																			}
																		else
																			{	/* Llib/socket.scm 386 */
																				bool_t BgL_test2218z00_3065;

																				{	/* Llib/socket.scm 386 */
																					bool_t BgL__ortest_1042z00_1934;

																					BgL__ortest_1042z00_1934 =
																						(BgL_domainz00_1229 ==
																						BGl_symbol1971z00zz__socketz00);
																					if (BgL__ortest_1042z00_1934)
																						{	/* Llib/socket.scm 386 */
																							BgL_test2218z00_3065 =
																								BgL__ortest_1042z00_1934;
																						}
																					else
																						{	/* Llib/socket.scm 386 */
																							BgL_test2218z00_3065 =
																								(BgL_domainz00_1229 ==
																								BGl_symbol1973z00zz__socketz00);
																						}
																				}
																				if (BgL_test2218z00_3065)
																					{	/* Llib/socket.scm 391 */
																						int BgL_tmpz00_3069;

																						{	/* Llib/socket.scm 391 */
																							obj_t BgL_tmpz00_3070;

																							if (INTEGERP(BgL_timeoutz00_1232))
																								{	/* Llib/socket.scm 391 */
																									BgL_tmpz00_3070 =
																										BgL_timeoutz00_1232;
																								}
																							else
																								{
																									obj_t BgL_auxz00_3073;

																									BgL_auxz00_3073 =
																										BGl_typezd2errorzd2zz__errorz00
																										(BGl_string1937z00zz__socketz00,
																										BINT(17601L),
																										BGl_string1963z00zz__socketz00,
																										BGl_string1964z00zz__socketz00,
																										BgL_timeoutz00_1232);
																									FAILURE(BgL_auxz00_3073,
																										BFALSE, BFALSE);
																								}
																							BgL_tmpz00_3069 =
																								CINT(BgL_tmpz00_3070);
																						}
																						BgL_res1780z00_1935 =
																							bgl_make_client_unix_socket
																							(BgL_hostz00_1925,
																							BgL_tmpz00_3069,
																							BgL_inbufz00_1927,
																							BgL_outbufz00_1928);
																					}
																				else
																					{	/* Llib/socket.scm 394 */
																						obj_t BgL_aux1827z00_2509;

																						BgL_aux1827z00_2509 =
																							BGl_errorz00zz__errorz00
																							(BGl_string1965z00zz__socketz00,
																							BGl_string1975z00zz__socketz00,
																							BgL_domainz00_1229);
																						if (SOCKETP(BgL_aux1827z00_2509))
																							{	/* Llib/socket.scm 394 */
																								BgL_res1780z00_1935 =
																									BgL_aux1827z00_2509;
																							}
																						else
																							{
																								obj_t BgL_auxz00_3082;

																								BgL_auxz00_3082 =
																									BGl_typezd2errorzd2zz__errorz00
																									(BGl_string1937z00zz__socketz00,
																									BINT(17707L),
																									BGl_string1963z00zz__socketz00,
																									BGl_string1935z00zz__socketz00,
																									BgL_aux1827z00_2509);
																								FAILURE(BgL_auxz00_3082, BFALSE,
																									BFALSE);
																							}
																					}
																			}
																	}
																}
															}
															return BgL_res1780z00_1935;
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* make-client-socket */
	BGL_EXPORTED_DEF obj_t BGl_makezd2clientzd2socketz00zz__socketz00(obj_t
		BgL_hostz00_16, int BgL_portz00_17, obj_t BgL_domainz00_18,
		obj_t BgL_inbufz00_19, obj_t BgL_outbufz00_20, obj_t BgL_timeoutz00_21)
	{
		{	/* Llib/socket.scm 382 */
			BGl_z52socketzd2initz12z92zz__socketz00();
			{	/* Llib/socket.scm 384 */
				obj_t BgL_inbufz00_1936;
				obj_t BgL_outbufz00_1937;

				BgL_inbufz00_1936 =
					BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00
					(BGl_string1965z00zz__socketz00, BgL_inbufz00_19, (int) (512L));
				BgL_outbufz00_1937 =
					BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00
					(BGl_string1965z00zz__socketz00, BgL_outbufz00_20, (int) (1024L));
				{	/* Llib/socket.scm 386 */
					bool_t BgL_test2222z00_3091;

					{	/* Llib/socket.scm 386 */
						bool_t BgL__ortest_1040z00_1940;

						BgL__ortest_1040z00_1940 =
							(BgL_domainz00_18 == BGl_symbol1948z00zz__socketz00);
						if (BgL__ortest_1040z00_1940)
							{	/* Llib/socket.scm 386 */
								BgL_test2222z00_3091 = BgL__ortest_1040z00_1940;
							}
						else
							{	/* Llib/socket.scm 386 */
								bool_t BgL__ortest_1041z00_1941;

								BgL__ortest_1041z00_1941 =
									(BgL_domainz00_18 == BGl_symbol1966z00zz__socketz00);
								if (BgL__ortest_1041z00_1941)
									{	/* Llib/socket.scm 386 */
										BgL_test2222z00_3091 = BgL__ortest_1041z00_1941;
									}
								else
									{	/* Llib/socket.scm 386 */
										BgL_test2222z00_3091 =
											(BgL_domainz00_18 == BGl_symbol1968z00zz__socketz00);
									}
							}
					}
					if (BgL_test2222z00_3091)
						{	/* Llib/socket.scm 386 */
							return
								bgl_make_client_socket(BgL_hostz00_16, BgL_portz00_17,
								CINT(BgL_timeoutz00_21), BgL_inbufz00_1936, BgL_outbufz00_1937,
								BgL_domainz00_18);
						}
					else
						{	/* Llib/socket.scm 386 */
							bool_t BgL_test2225z00_3099;

							{	/* Llib/socket.scm 386 */
								bool_t BgL__ortest_1042z00_1943;

								BgL__ortest_1042z00_1943 =
									(BgL_domainz00_18 == BGl_symbol1971z00zz__socketz00);
								if (BgL__ortest_1042z00_1943)
									{	/* Llib/socket.scm 386 */
										BgL_test2225z00_3099 = BgL__ortest_1042z00_1943;
									}
								else
									{	/* Llib/socket.scm 386 */
										BgL_test2225z00_3099 =
											(BgL_domainz00_18 == BGl_symbol1973z00zz__socketz00);
									}
							}
							if (BgL_test2225z00_3099)
								{	/* Llib/socket.scm 386 */
									return
										bgl_make_client_unix_socket(BgL_hostz00_16,
										CINT(BgL_timeoutz00_21), BgL_inbufz00_1936,
										BgL_outbufz00_1937);
								}
							else
								{	/* Llib/socket.scm 386 */
									return
										BGl_errorz00zz__errorz00(BGl_string1965z00zz__socketz00,
										BGl_string1975z00zz__socketz00, BgL_domainz00_18);
								}
						}
				}
			}
		}

	}



/* make-server-socket */
	BGL_EXPORTED_DEF obj_t BGl_makezd2serverzd2socketz00zz__socketz00(obj_t
		BgL_portz00_24)
	{
		{	/* Llib/socket.scm 399 */
			{	/* Llib/socket.scm 228 */
				obj_t BgL_dsssl1126z00_1251;

				BgL_dsssl1126z00_1251 = BgL_portz00_24;
				{	/* Llib/socket.scm 228 */
					obj_t BgL_portz00_1252;

					{	/* Llib/socket.scm 228 */
						bool_t BgL_test2227z00_3106;

						if (NULLP(BgL_dsssl1126z00_1251))
							{	/* Llib/socket.scm 228 */
								BgL_test2227z00_3106 = ((bool_t) 1);
							}
						else
							{	/* Llib/socket.scm 228 */
								BgL_test2227z00_3106 =
									CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CAR
										(BgL_dsssl1126z00_1251), BGl_list1976z00zz__socketz00));
							}
						if (BgL_test2227z00_3106)
							{	/* Llib/socket.scm 228 */
								BgL_portz00_1252 = BINT(0L);
							}
						else
							{	/* Llib/socket.scm 228 */
								obj_t BgL_tmp1127z00_1279;

								BgL_tmp1127z00_1279 = CAR(BgL_dsssl1126z00_1251);
								BgL_dsssl1126z00_1251 = CDR(BgL_dsssl1126z00_1251);
								BgL_portz00_1252 = BgL_tmp1127z00_1279;
							}
					}
					{	/* Llib/socket.scm 228 */
						obj_t BgL_namez00_1253;

						BgL_namez00_1253 =
							BGl_dssslzd2getzd2keyzd2argzd2zz__dssslz00(BgL_dsssl1126z00_1251,
							BGl_keyword1979z00zz__socketz00, BFALSE);
						{	/* Llib/socket.scm 228 */
							obj_t BgL_backlogz00_1254;

							BgL_backlogz00_1254 =
								BGl_dssslzd2getzd2keyzd2argzd2zz__dssslz00
								(BgL_dsssl1126z00_1251, BGl_keyword1977z00zz__socketz00,
								BINT(5L));
							{	/* Llib/socket.scm 228 */
								obj_t BgL_domainz00_1255;

								BgL_domainz00_1255 =
									BGl_dssslzd2getzd2keyzd2argzd2zz__dssslz00
									(BgL_dsssl1126z00_1251, BGl_keyword1951z00zz__socketz00,
									BGl_symbol1948z00zz__socketz00);
								if (NULLP(BGl_dssslzd2getzd2keyzd2restzd2argz00zz__dssslz00
										(BgL_dsssl1126z00_1251, BGl_list1976z00zz__socketz00)))
									{	/* Llib/socket.scm 228 */
										BGl_z52socketzd2initz12z92zz__socketz00();
										{	/* Llib/socket.scm 401 */
											bool_t BgL_test2230z00_3123;

											{	/* Llib/socket.scm 401 */
												bool_t BgL__ortest_1045z00_1264;

												BgL__ortest_1045z00_1264 =
													(BgL_domainz00_1255 ==
													BGl_symbol1948z00zz__socketz00);
												if (BgL__ortest_1045z00_1264)
													{	/* Llib/socket.scm 401 */
														BgL_test2230z00_3123 = BgL__ortest_1045z00_1264;
													}
												else
													{	/* Llib/socket.scm 401 */
														BgL_test2230z00_3123 =
															(BgL_domainz00_1255 ==
															BGl_symbol1966z00zz__socketz00);
													}
											}
											if (BgL_test2230z00_3123)
												{	/* Llib/socket.scm 401 */
													return
														bgl_make_server_socket(BgL_namez00_1253,
														CINT(BgL_portz00_1252),
														CINT(BgL_backlogz00_1254), BgL_domainz00_1255);
												}
											else
												{	/* Llib/socket.scm 401 */
													bool_t BgL_test2232z00_3130;

													{	/* Llib/socket.scm 401 */
														bool_t BgL__ortest_1046z00_1263;

														BgL__ortest_1046z00_1263 =
															(BgL_domainz00_1255 ==
															BGl_symbol1971z00zz__socketz00);
														if (BgL__ortest_1046z00_1263)
															{	/* Llib/socket.scm 401 */
																BgL_test2232z00_3130 = BgL__ortest_1046z00_1263;
															}
														else
															{	/* Llib/socket.scm 401 */
																BgL_test2232z00_3130 =
																	(BgL_domainz00_1255 ==
																	BGl_symbol1973z00zz__socketz00);
															}
													}
													if (BgL_test2232z00_3130)
														{	/* Llib/socket.scm 401 */
															return
																bgl_make_server_unix_socket(BgL_namez00_1253,
																CINT(BgL_backlogz00_1254));
														}
													else
														{	/* Llib/socket.scm 401 */
															return
																BGl_errorz00zz__errorz00
																(BGl_string1981z00zz__socketz00,
																BGl_string1982z00zz__socketz00,
																BgL_domainz00_1255);
														}
												}
										}
									}
								else
									{	/* Llib/socket.scm 228 */
										obj_t BgL_arg1318z00_1265;

										{	/* Llib/socket.scm 228 */
											obj_t BgL_arg1319z00_1266;

											{	/* Llib/socket.scm 228 */
												obj_t BgL_arg1322z00_1269;

												BgL_arg1322z00_1269 =
													BGl_dssslzd2getzd2keyzd2restzd2argz00zz__dssslz00
													(BgL_dsssl1126z00_1251, BGl_list1976z00zz__socketz00);
												{	/* Llib/socket.scm 228 */
													obj_t BgL_list1323z00_1270;

													BgL_list1323z00_1270 =
														MAKE_YOUNG_PAIR(BgL_arg1322z00_1269, BNIL);
													BgL_arg1319z00_1266 =
														BGl_mapz00zz__r4_control_features_6_9z00
														(BGl_proc1983z00zz__socketz00,
														BgL_list1323z00_1270);
												}
											}
											{	/* Llib/socket.scm 228 */
												obj_t BgL_list1320z00_1267;

												BgL_list1320z00_1267 =
													MAKE_YOUNG_PAIR(BgL_arg1319z00_1266, BNIL);
												BgL_arg1318z00_1265 =
													BGl_applyz00zz__r4_control_features_6_9z00
													(BGl_stringzd2appendzd2envz00zz__r4_strings_6_7z00,
													BGl_string1984z00zz__socketz00, BgL_list1320z00_1267);
											}
										}
										return
											BGl_errorz00zz__errorz00(BGl_string1985z00zz__socketz00,
											BgL_arg1318z00_1265, BgL_dsssl1126z00_1251);
									}
							}
						}
					}
				}
			}
		}

	}



/* &make-server-socket */
	obj_t BGl_z62makezd2serverzd2socketz62zz__socketz00(obj_t BgL_envz00_2415,
		obj_t BgL_portz00_2416)
	{
		{	/* Llib/socket.scm 399 */
			return BGl_makezd2serverzd2socketz00zz__socketz00(BgL_portz00_2416);
		}

	}



/* &<@anonymous:1324> */
	obj_t BGl_z62zc3z04anonymousza31324ze3ze5zz__socketz00(obj_t BgL_envz00_2417,
		obj_t BgL_vz00_2418)
	{
		{	/* Llib/socket.scm 228 */
			{	/* Llib/socket.scm 228 */
				obj_t BgL_list1325z00_2639;

				BgL_list1325z00_2639 = MAKE_YOUNG_PAIR(BgL_vz00_2418, BNIL);
				return
					BGl_formatz00zz__r4_output_6_10_3z00(BGl_string1986z00zz__socketz00,
					BgL_list1325z00_2639);
			}
		}

	}



/* _socket-accept */
	obj_t BGl__socketzd2acceptzd2zz__socketz00(obj_t BgL_env1129z00_30,
		obj_t BgL_opt1128z00_29)
	{
		{	/* Llib/socket.scm 415 */
			{	/* Llib/socket.scm 415 */
				obj_t BgL_g1137z00_1286;

				BgL_g1137z00_1286 = VECTOR_REF(BgL_opt1128z00_29, 0L);
				{	/* Llib/socket.scm 415 */
					obj_t BgL_errpz00_1287;

					BgL_errpz00_1287 = BTRUE;
					{	/* Llib/socket.scm 415 */
						obj_t BgL_inbufz00_1288;

						BgL_inbufz00_1288 = BTRUE;
						{	/* Llib/socket.scm 415 */
							obj_t BgL_outbufz00_1289;

							BgL_outbufz00_1289 = BTRUE;
							{	/* Llib/socket.scm 415 */

								{
									long BgL_iz00_1290;

									BgL_iz00_1290 = 1L;
								BgL_check1132z00_1291:
									if ((BgL_iz00_1290 == VECTOR_LENGTH(BgL_opt1128z00_29)))
										{	/* Llib/socket.scm 415 */
											BNIL;
										}
									else
										{	/* Llib/socket.scm 415 */
											bool_t BgL_test2235z00_3150;

											{	/* Llib/socket.scm 415 */
												obj_t BgL_arg1337z00_1297;

												BgL_arg1337z00_1297 =
													VECTOR_REF(BgL_opt1128z00_29, BgL_iz00_1290);
												BgL_test2235z00_3150 =
													CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
													(BgL_arg1337z00_1297, BGl_list1987z00zz__socketz00));
											}
											if (BgL_test2235z00_3150)
												{
													long BgL_iz00_3154;

													BgL_iz00_3154 = (BgL_iz00_1290 + 2L);
													BgL_iz00_1290 = BgL_iz00_3154;
													goto BgL_check1132z00_1291;
												}
											else
												{	/* Llib/socket.scm 415 */
													obj_t BgL_arg1336z00_1296;

													BgL_arg1336z00_1296 =
														VECTOR_REF(BgL_opt1128z00_29, BgL_iz00_1290);
													BGl_errorz00zz__errorz00
														(BGl_symbol1990z00zz__socketz00,
														BGl_string1961z00zz__socketz00,
														BgL_arg1336z00_1296);
												}
										}
								}
								{	/* Llib/socket.scm 415 */
									obj_t BgL_index1134z00_1298;

									{
										long BgL_iz00_1965;

										BgL_iz00_1965 = 1L;
									BgL_search1131z00_1964:
										if ((BgL_iz00_1965 == VECTOR_LENGTH(BgL_opt1128z00_29)))
											{	/* Llib/socket.scm 415 */
												BgL_index1134z00_1298 = BINT(-1L);
											}
										else
											{	/* Llib/socket.scm 415 */
												if (
													(BgL_iz00_1965 ==
														(VECTOR_LENGTH(BgL_opt1128z00_29) - 1L)))
													{	/* Llib/socket.scm 415 */
														BgL_index1134z00_1298 =
															BGl_errorz00zz__errorz00
															(BGl_symbol1990z00zz__socketz00,
															BGl_string1992z00zz__socketz00,
															BINT(VECTOR_LENGTH(BgL_opt1128z00_29)));
													}
												else
													{	/* Llib/socket.scm 415 */
														obj_t BgL_vz00_1975;

														BgL_vz00_1975 =
															VECTOR_REF(BgL_opt1128z00_29, BgL_iz00_1965);
														if (
															(BgL_vz00_1975 ==
																BGl_keyword1988z00zz__socketz00))
															{	/* Llib/socket.scm 415 */
																BgL_index1134z00_1298 =
																	BINT((BgL_iz00_1965 + 1L));
															}
														else
															{
																long BgL_iz00_3174;

																BgL_iz00_3174 = (BgL_iz00_1965 + 2L);
																BgL_iz00_1965 = BgL_iz00_3174;
																goto BgL_search1131z00_1964;
															}
													}
											}
									}
									{	/* Llib/socket.scm 415 */
										bool_t BgL_test2239z00_3176;

										{	/* Llib/socket.scm 415 */
											long BgL_n1z00_1979;

											{	/* Llib/socket.scm 415 */
												obj_t BgL_tmpz00_3177;

												if (INTEGERP(BgL_index1134z00_1298))
													{	/* Llib/socket.scm 415 */
														BgL_tmpz00_3177 = BgL_index1134z00_1298;
													}
												else
													{
														obj_t BgL_auxz00_3180;

														BgL_auxz00_3180 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string1937z00zz__socketz00, BINT(18694L),
															BGl_string1993z00zz__socketz00,
															BGl_string1964z00zz__socketz00,
															BgL_index1134z00_1298);
														FAILURE(BgL_auxz00_3180, BFALSE, BFALSE);
													}
												BgL_n1z00_1979 = (long) CINT(BgL_tmpz00_3177);
											}
											BgL_test2239z00_3176 = (BgL_n1z00_1979 >= 0L);
										}
										if (BgL_test2239z00_3176)
											{
												long BgL_auxz00_3186;

												{	/* Llib/socket.scm 415 */
													obj_t BgL_tmpz00_3187;

													if (INTEGERP(BgL_index1134z00_1298))
														{	/* Llib/socket.scm 415 */
															BgL_tmpz00_3187 = BgL_index1134z00_1298;
														}
													else
														{
															obj_t BgL_auxz00_3190;

															BgL_auxz00_3190 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string1937z00zz__socketz00, BINT(18694L),
																BGl_string1993z00zz__socketz00,
																BGl_string1964z00zz__socketz00,
																BgL_index1134z00_1298);
															FAILURE(BgL_auxz00_3190, BFALSE, BFALSE);
														}
													BgL_auxz00_3186 = (long) CINT(BgL_tmpz00_3187);
												}
												BgL_errpz00_1287 =
													VECTOR_REF(BgL_opt1128z00_29, BgL_auxz00_3186);
											}
										else
											{	/* Llib/socket.scm 415 */
												BFALSE;
											}
									}
								}
								{	/* Llib/socket.scm 415 */
									obj_t BgL_index1135z00_1300;

									{
										long BgL_iz00_1981;

										BgL_iz00_1981 = 1L;
									BgL_search1131z00_1980:
										if ((BgL_iz00_1981 == VECTOR_LENGTH(BgL_opt1128z00_29)))
											{	/* Llib/socket.scm 415 */
												BgL_index1135z00_1300 = BINT(-1L);
											}
										else
											{	/* Llib/socket.scm 415 */
												if (
													(BgL_iz00_1981 ==
														(VECTOR_LENGTH(BgL_opt1128z00_29) - 1L)))
													{	/* Llib/socket.scm 415 */
														BgL_index1135z00_1300 =
															BGl_errorz00zz__errorz00
															(BGl_symbol1990z00zz__socketz00,
															BGl_string1992z00zz__socketz00,
															BINT(VECTOR_LENGTH(BgL_opt1128z00_29)));
													}
												else
													{	/* Llib/socket.scm 415 */
														obj_t BgL_vz00_1991;

														BgL_vz00_1991 =
															VECTOR_REF(BgL_opt1128z00_29, BgL_iz00_1981);
														if (
															(BgL_vz00_1991 ==
																BGl_keyword1953z00zz__socketz00))
															{	/* Llib/socket.scm 415 */
																BgL_index1135z00_1300 =
																	BINT((BgL_iz00_1981 + 1L));
															}
														else
															{
																long BgL_iz00_3212;

																BgL_iz00_3212 = (BgL_iz00_1981 + 2L);
																BgL_iz00_1981 = BgL_iz00_3212;
																goto BgL_search1131z00_1980;
															}
													}
											}
									}
									{	/* Llib/socket.scm 415 */
										bool_t BgL_test2245z00_3214;

										{	/* Llib/socket.scm 415 */
											long BgL_n1z00_1995;

											{	/* Llib/socket.scm 415 */
												obj_t BgL_tmpz00_3215;

												if (INTEGERP(BgL_index1135z00_1300))
													{	/* Llib/socket.scm 415 */
														BgL_tmpz00_3215 = BgL_index1135z00_1300;
													}
												else
													{
														obj_t BgL_auxz00_3218;

														BgL_auxz00_3218 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string1937z00zz__socketz00, BINT(18694L),
															BGl_string1993z00zz__socketz00,
															BGl_string1964z00zz__socketz00,
															BgL_index1135z00_1300);
														FAILURE(BgL_auxz00_3218, BFALSE, BFALSE);
													}
												BgL_n1z00_1995 = (long) CINT(BgL_tmpz00_3215);
											}
											BgL_test2245z00_3214 = (BgL_n1z00_1995 >= 0L);
										}
										if (BgL_test2245z00_3214)
											{
												long BgL_auxz00_3224;

												{	/* Llib/socket.scm 415 */
													obj_t BgL_tmpz00_3225;

													if (INTEGERP(BgL_index1135z00_1300))
														{	/* Llib/socket.scm 415 */
															BgL_tmpz00_3225 = BgL_index1135z00_1300;
														}
													else
														{
															obj_t BgL_auxz00_3228;

															BgL_auxz00_3228 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string1937z00zz__socketz00, BINT(18694L),
																BGl_string1993z00zz__socketz00,
																BGl_string1964z00zz__socketz00,
																BgL_index1135z00_1300);
															FAILURE(BgL_auxz00_3228, BFALSE, BFALSE);
														}
													BgL_auxz00_3224 = (long) CINT(BgL_tmpz00_3225);
												}
												BgL_inbufz00_1288 =
													VECTOR_REF(BgL_opt1128z00_29, BgL_auxz00_3224);
											}
										else
											{	/* Llib/socket.scm 415 */
												BFALSE;
											}
									}
								}
								{	/* Llib/socket.scm 415 */
									obj_t BgL_index1136z00_1302;

									{
										long BgL_iz00_1997;

										BgL_iz00_1997 = 1L;
									BgL_search1131z00_1996:
										if ((BgL_iz00_1997 == VECTOR_LENGTH(BgL_opt1128z00_29)))
											{	/* Llib/socket.scm 415 */
												BgL_index1136z00_1302 = BINT(-1L);
											}
										else
											{	/* Llib/socket.scm 415 */
												if (
													(BgL_iz00_1997 ==
														(VECTOR_LENGTH(BgL_opt1128z00_29) - 1L)))
													{	/* Llib/socket.scm 415 */
														BgL_index1136z00_1302 =
															BGl_errorz00zz__errorz00
															(BGl_symbol1990z00zz__socketz00,
															BGl_string1992z00zz__socketz00,
															BINT(VECTOR_LENGTH(BgL_opt1128z00_29)));
													}
												else
													{	/* Llib/socket.scm 415 */
														obj_t BgL_vz00_2007;

														BgL_vz00_2007 =
															VECTOR_REF(BgL_opt1128z00_29, BgL_iz00_1997);
														if (
															(BgL_vz00_2007 ==
																BGl_keyword1955z00zz__socketz00))
															{	/* Llib/socket.scm 415 */
																BgL_index1136z00_1302 =
																	BINT((BgL_iz00_1997 + 1L));
															}
														else
															{
																long BgL_iz00_3250;

																BgL_iz00_3250 = (BgL_iz00_1997 + 2L);
																BgL_iz00_1997 = BgL_iz00_3250;
																goto BgL_search1131z00_1996;
															}
													}
											}
									}
									{	/* Llib/socket.scm 415 */
										bool_t BgL_test2251z00_3252;

										{	/* Llib/socket.scm 415 */
											long BgL_n1z00_2011;

											{	/* Llib/socket.scm 415 */
												obj_t BgL_tmpz00_3253;

												if (INTEGERP(BgL_index1136z00_1302))
													{	/* Llib/socket.scm 415 */
														BgL_tmpz00_3253 = BgL_index1136z00_1302;
													}
												else
													{
														obj_t BgL_auxz00_3256;

														BgL_auxz00_3256 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string1937z00zz__socketz00, BINT(18694L),
															BGl_string1993z00zz__socketz00,
															BGl_string1964z00zz__socketz00,
															BgL_index1136z00_1302);
														FAILURE(BgL_auxz00_3256, BFALSE, BFALSE);
													}
												BgL_n1z00_2011 = (long) CINT(BgL_tmpz00_3253);
											}
											BgL_test2251z00_3252 = (BgL_n1z00_2011 >= 0L);
										}
										if (BgL_test2251z00_3252)
											{
												long BgL_auxz00_3262;

												{	/* Llib/socket.scm 415 */
													obj_t BgL_tmpz00_3263;

													if (INTEGERP(BgL_index1136z00_1302))
														{	/* Llib/socket.scm 415 */
															BgL_tmpz00_3263 = BgL_index1136z00_1302;
														}
													else
														{
															obj_t BgL_auxz00_3266;

															BgL_auxz00_3266 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string1937z00zz__socketz00, BINT(18694L),
																BGl_string1993z00zz__socketz00,
																BGl_string1964z00zz__socketz00,
																BgL_index1136z00_1302);
															FAILURE(BgL_auxz00_3266, BFALSE, BFALSE);
														}
													BgL_auxz00_3262 = (long) CINT(BgL_tmpz00_3263);
												}
												BgL_outbufz00_1289 =
													VECTOR_REF(BgL_opt1128z00_29, BgL_auxz00_3262);
											}
										else
											{	/* Llib/socket.scm 415 */
												BFALSE;
											}
									}
								}
								{	/* Llib/socket.scm 415 */
									obj_t BgL_arg1341z00_1304;

									BgL_arg1341z00_1304 = VECTOR_REF(BgL_opt1128z00_29, 0L);
									{	/* Llib/socket.scm 415 */
										obj_t BgL_errpz00_1305;

										BgL_errpz00_1305 = BgL_errpz00_1287;
										{	/* Llib/socket.scm 415 */
											obj_t BgL_inbufz00_1306;

											BgL_inbufz00_1306 = BgL_inbufz00_1288;
											{	/* Llib/socket.scm 415 */
												obj_t BgL_outbufz00_1307;

												BgL_outbufz00_1307 = BgL_outbufz00_1289;
												{	/* Llib/socket.scm 415 */
													obj_t BgL_socketz00_2012;

													if (SOCKETP(BgL_arg1341z00_1304))
														{	/* Llib/socket.scm 415 */
															BgL_socketz00_2012 = BgL_arg1341z00_1304;
														}
													else
														{
															obj_t BgL_auxz00_3275;

															BgL_auxz00_3275 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string1937z00zz__socketz00, BINT(18694L),
																BGl_string1993z00zz__socketz00,
																BGl_string1935z00zz__socketz00,
																BgL_arg1341z00_1304);
															FAILURE(BgL_auxz00_3275, BFALSE, BFALSE);
														}
													return
														bgl_socket_accept(BgL_socketz00_2012,
														CBOOL(BgL_errpz00_1305),
														BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00
														(BGl_string1991z00zz__socketz00, BgL_inbufz00_1306,
															(int) (512L)),
														BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00
														(BGl_string1991z00zz__socketz00, BgL_outbufz00_1307,
															(int) (1024L)));
		}}}}}}}}}}}

	}



/* socket-accept */
	BGL_EXPORTED_DEF obj_t BGl_socketzd2acceptzd2zz__socketz00(obj_t
		BgL_socketz00_25, obj_t BgL_errpz00_26, obj_t BgL_inbufz00_27,
		obj_t BgL_outbufz00_28)
	{
		{	/* Llib/socket.scm 415 */
			return
				bgl_socket_accept(BgL_socketz00_25,
				CBOOL(BgL_errpz00_26),
				BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00
				(BGl_string1991z00zz__socketz00, BgL_inbufz00_27, (int) (512L)),
				BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00
				(BGl_string1991z00zz__socketz00, BgL_outbufz00_28, (int) (1024L)));
		}

	}



/* _socket-accept-many */
	obj_t BGl__socketzd2acceptzd2manyz00zz__socketz00(obj_t BgL_env1139z00_37,
		obj_t BgL_opt1138z00_36)
	{
		{	/* Llib/socket.scm 426 */
			{	/* Llib/socket.scm 426 */
				obj_t BgL_g1147z00_1321;
				obj_t BgL_g1148z00_1322;

				BgL_g1147z00_1321 = VECTOR_REF(BgL_opt1138z00_36, 0L);
				BgL_g1148z00_1322 = VECTOR_REF(BgL_opt1138z00_36, 1L);
				{	/* Llib/socket.scm 426 */
					obj_t BgL_errpz00_1323;

					BgL_errpz00_1323 = BTRUE;
					{	/* Llib/socket.scm 426 */
						obj_t BgL_inbufsz00_1324;

						BgL_inbufsz00_1324 = BTRUE;
						{	/* Llib/socket.scm 426 */
							obj_t BgL_outbufsz00_1325;

							BgL_outbufsz00_1325 = BTRUE;
							{	/* Llib/socket.scm 426 */

								{
									long BgL_iz00_1326;

									BgL_iz00_1326 = 2L;
								BgL_check1142z00_1327:
									if ((BgL_iz00_1326 == VECTOR_LENGTH(BgL_opt1138z00_36)))
										{	/* Llib/socket.scm 426 */
											BNIL;
										}
									else
										{	/* Llib/socket.scm 426 */
											bool_t BgL_test2256z00_3296;

											{	/* Llib/socket.scm 426 */
												obj_t BgL_arg1356z00_1333;

												BgL_arg1356z00_1333 =
													VECTOR_REF(BgL_opt1138z00_36, BgL_iz00_1326);
												BgL_test2256z00_3296 =
													CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
													(BgL_arg1356z00_1333, BGl_list1994z00zz__socketz00));
											}
											if (BgL_test2256z00_3296)
												{
													long BgL_iz00_3300;

													BgL_iz00_3300 = (BgL_iz00_1326 + 2L);
													BgL_iz00_1326 = BgL_iz00_3300;
													goto BgL_check1142z00_1327;
												}
											else
												{	/* Llib/socket.scm 426 */
													obj_t BgL_arg1354z00_1332;

													BgL_arg1354z00_1332 =
														VECTOR_REF(BgL_opt1138z00_36, BgL_iz00_1326);
													BGl_errorz00zz__errorz00
														(BGl_symbol1999z00zz__socketz00,
														BGl_string1961z00zz__socketz00,
														BgL_arg1354z00_1332);
												}
										}
								}
								{	/* Llib/socket.scm 426 */
									obj_t BgL_index1144z00_1334;

									{
										long BgL_iz00_2033;

										BgL_iz00_2033 = 2L;
									BgL_search1141z00_2032:
										if ((BgL_iz00_2033 == VECTOR_LENGTH(BgL_opt1138z00_36)))
											{	/* Llib/socket.scm 426 */
												BgL_index1144z00_1334 = BINT(-1L);
											}
										else
											{	/* Llib/socket.scm 426 */
												if (
													(BgL_iz00_2033 ==
														(VECTOR_LENGTH(BgL_opt1138z00_36) - 1L)))
													{	/* Llib/socket.scm 426 */
														BgL_index1144z00_1334 =
															BGl_errorz00zz__errorz00
															(BGl_symbol1999z00zz__socketz00,
															BGl_string2001z00zz__socketz00,
															BINT(VECTOR_LENGTH(BgL_opt1138z00_36)));
													}
												else
													{	/* Llib/socket.scm 426 */
														obj_t BgL_vz00_2043;

														BgL_vz00_2043 =
															VECTOR_REF(BgL_opt1138z00_36, BgL_iz00_2033);
														if (
															(BgL_vz00_2043 ==
																BGl_keyword1988z00zz__socketz00))
															{	/* Llib/socket.scm 426 */
																BgL_index1144z00_1334 =
																	BINT((BgL_iz00_2033 + 1L));
															}
														else
															{
																long BgL_iz00_3320;

																BgL_iz00_3320 = (BgL_iz00_2033 + 2L);
																BgL_iz00_2033 = BgL_iz00_3320;
																goto BgL_search1141z00_2032;
															}
													}
											}
									}
									{	/* Llib/socket.scm 426 */
										bool_t BgL_test2260z00_3322;

										{	/* Llib/socket.scm 426 */
											long BgL_n1z00_2047;

											{	/* Llib/socket.scm 426 */
												obj_t BgL_tmpz00_3323;

												if (INTEGERP(BgL_index1144z00_1334))
													{	/* Llib/socket.scm 426 */
														BgL_tmpz00_3323 = BgL_index1144z00_1334;
													}
												else
													{
														obj_t BgL_auxz00_3326;

														BgL_auxz00_3326 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string1937z00zz__socketz00, BINT(19386L),
															BGl_string2002z00zz__socketz00,
															BGl_string1964z00zz__socketz00,
															BgL_index1144z00_1334);
														FAILURE(BgL_auxz00_3326, BFALSE, BFALSE);
													}
												BgL_n1z00_2047 = (long) CINT(BgL_tmpz00_3323);
											}
											BgL_test2260z00_3322 = (BgL_n1z00_2047 >= 0L);
										}
										if (BgL_test2260z00_3322)
											{
												long BgL_auxz00_3332;

												{	/* Llib/socket.scm 426 */
													obj_t BgL_tmpz00_3333;

													if (INTEGERP(BgL_index1144z00_1334))
														{	/* Llib/socket.scm 426 */
															BgL_tmpz00_3333 = BgL_index1144z00_1334;
														}
													else
														{
															obj_t BgL_auxz00_3336;

															BgL_auxz00_3336 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string1937z00zz__socketz00, BINT(19386L),
																BGl_string2002z00zz__socketz00,
																BGl_string1964z00zz__socketz00,
																BgL_index1144z00_1334);
															FAILURE(BgL_auxz00_3336, BFALSE, BFALSE);
														}
													BgL_auxz00_3332 = (long) CINT(BgL_tmpz00_3333);
												}
												BgL_errpz00_1323 =
													VECTOR_REF(BgL_opt1138z00_36, BgL_auxz00_3332);
											}
										else
											{	/* Llib/socket.scm 426 */
												BFALSE;
											}
									}
								}
								{	/* Llib/socket.scm 426 */
									obj_t BgL_index1145z00_1336;

									{
										long BgL_iz00_2049;

										BgL_iz00_2049 = 2L;
									BgL_search1141z00_2048:
										if ((BgL_iz00_2049 == VECTOR_LENGTH(BgL_opt1138z00_36)))
											{	/* Llib/socket.scm 426 */
												BgL_index1145z00_1336 = BINT(-1L);
											}
										else
											{	/* Llib/socket.scm 426 */
												if (
													(BgL_iz00_2049 ==
														(VECTOR_LENGTH(BgL_opt1138z00_36) - 1L)))
													{	/* Llib/socket.scm 426 */
														BgL_index1145z00_1336 =
															BGl_errorz00zz__errorz00
															(BGl_symbol1999z00zz__socketz00,
															BGl_string2001z00zz__socketz00,
															BINT(VECTOR_LENGTH(BgL_opt1138z00_36)));
													}
												else
													{	/* Llib/socket.scm 426 */
														obj_t BgL_vz00_2059;

														BgL_vz00_2059 =
															VECTOR_REF(BgL_opt1138z00_36, BgL_iz00_2049);
														if (
															(BgL_vz00_2059 ==
																BGl_keyword1995z00zz__socketz00))
															{	/* Llib/socket.scm 426 */
																BgL_index1145z00_1336 =
																	BINT((BgL_iz00_2049 + 1L));
															}
														else
															{
																long BgL_iz00_3358;

																BgL_iz00_3358 = (BgL_iz00_2049 + 2L);
																BgL_iz00_2049 = BgL_iz00_3358;
																goto BgL_search1141z00_2048;
															}
													}
											}
									}
									{	/* Llib/socket.scm 426 */
										bool_t BgL_test2266z00_3360;

										{	/* Llib/socket.scm 426 */
											long BgL_n1z00_2063;

											{	/* Llib/socket.scm 426 */
												obj_t BgL_tmpz00_3361;

												if (INTEGERP(BgL_index1145z00_1336))
													{	/* Llib/socket.scm 426 */
														BgL_tmpz00_3361 = BgL_index1145z00_1336;
													}
												else
													{
														obj_t BgL_auxz00_3364;

														BgL_auxz00_3364 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string1937z00zz__socketz00, BINT(19386L),
															BGl_string2002z00zz__socketz00,
															BGl_string1964z00zz__socketz00,
															BgL_index1145z00_1336);
														FAILURE(BgL_auxz00_3364, BFALSE, BFALSE);
													}
												BgL_n1z00_2063 = (long) CINT(BgL_tmpz00_3361);
											}
											BgL_test2266z00_3360 = (BgL_n1z00_2063 >= 0L);
										}
										if (BgL_test2266z00_3360)
											{
												long BgL_auxz00_3370;

												{	/* Llib/socket.scm 426 */
													obj_t BgL_tmpz00_3371;

													if (INTEGERP(BgL_index1145z00_1336))
														{	/* Llib/socket.scm 426 */
															BgL_tmpz00_3371 = BgL_index1145z00_1336;
														}
													else
														{
															obj_t BgL_auxz00_3374;

															BgL_auxz00_3374 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string1937z00zz__socketz00, BINT(19386L),
																BGl_string2002z00zz__socketz00,
																BGl_string1964z00zz__socketz00,
																BgL_index1145z00_1336);
															FAILURE(BgL_auxz00_3374, BFALSE, BFALSE);
														}
													BgL_auxz00_3370 = (long) CINT(BgL_tmpz00_3371);
												}
												BgL_inbufsz00_1324 =
													VECTOR_REF(BgL_opt1138z00_36, BgL_auxz00_3370);
											}
										else
											{	/* Llib/socket.scm 426 */
												BFALSE;
											}
									}
								}
								{	/* Llib/socket.scm 426 */
									obj_t BgL_index1146z00_1338;

									{
										long BgL_iz00_2065;

										BgL_iz00_2065 = 2L;
									BgL_search1141z00_2064:
										if ((BgL_iz00_2065 == VECTOR_LENGTH(BgL_opt1138z00_36)))
											{	/* Llib/socket.scm 426 */
												BgL_index1146z00_1338 = BINT(-1L);
											}
										else
											{	/* Llib/socket.scm 426 */
												if (
													(BgL_iz00_2065 ==
														(VECTOR_LENGTH(BgL_opt1138z00_36) - 1L)))
													{	/* Llib/socket.scm 426 */
														BgL_index1146z00_1338 =
															BGl_errorz00zz__errorz00
															(BGl_symbol1999z00zz__socketz00,
															BGl_string2001z00zz__socketz00,
															BINT(VECTOR_LENGTH(BgL_opt1138z00_36)));
													}
												else
													{	/* Llib/socket.scm 426 */
														obj_t BgL_vz00_2075;

														BgL_vz00_2075 =
															VECTOR_REF(BgL_opt1138z00_36, BgL_iz00_2065);
														if (
															(BgL_vz00_2075 ==
																BGl_keyword1997z00zz__socketz00))
															{	/* Llib/socket.scm 426 */
																BgL_index1146z00_1338 =
																	BINT((BgL_iz00_2065 + 1L));
															}
														else
															{
																long BgL_iz00_3396;

																BgL_iz00_3396 = (BgL_iz00_2065 + 2L);
																BgL_iz00_2065 = BgL_iz00_3396;
																goto BgL_search1141z00_2064;
															}
													}
											}
									}
									{	/* Llib/socket.scm 426 */
										bool_t BgL_test2272z00_3398;

										{	/* Llib/socket.scm 426 */
											long BgL_n1z00_2079;

											{	/* Llib/socket.scm 426 */
												obj_t BgL_tmpz00_3399;

												if (INTEGERP(BgL_index1146z00_1338))
													{	/* Llib/socket.scm 426 */
														BgL_tmpz00_3399 = BgL_index1146z00_1338;
													}
												else
													{
														obj_t BgL_auxz00_3402;

														BgL_auxz00_3402 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string1937z00zz__socketz00, BINT(19386L),
															BGl_string2002z00zz__socketz00,
															BGl_string1964z00zz__socketz00,
															BgL_index1146z00_1338);
														FAILURE(BgL_auxz00_3402, BFALSE, BFALSE);
													}
												BgL_n1z00_2079 = (long) CINT(BgL_tmpz00_3399);
											}
											BgL_test2272z00_3398 = (BgL_n1z00_2079 >= 0L);
										}
										if (BgL_test2272z00_3398)
											{
												long BgL_auxz00_3408;

												{	/* Llib/socket.scm 426 */
													obj_t BgL_tmpz00_3409;

													if (INTEGERP(BgL_index1146z00_1338))
														{	/* Llib/socket.scm 426 */
															BgL_tmpz00_3409 = BgL_index1146z00_1338;
														}
													else
														{
															obj_t BgL_auxz00_3412;

															BgL_auxz00_3412 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string1937z00zz__socketz00, BINT(19386L),
																BGl_string2002z00zz__socketz00,
																BGl_string1964z00zz__socketz00,
																BgL_index1146z00_1338);
															FAILURE(BgL_auxz00_3412, BFALSE, BFALSE);
														}
													BgL_auxz00_3408 = (long) CINT(BgL_tmpz00_3409);
												}
												BgL_outbufsz00_1325 =
													VECTOR_REF(BgL_opt1138z00_36, BgL_auxz00_3408);
											}
										else
											{	/* Llib/socket.scm 426 */
												BFALSE;
											}
									}
								}
								{	/* Llib/socket.scm 426 */
									obj_t BgL_arg1360z00_1340;
									obj_t BgL_arg1361z00_1341;

									BgL_arg1360z00_1340 = VECTOR_REF(BgL_opt1138z00_36, 0L);
									BgL_arg1361z00_1341 = VECTOR_REF(BgL_opt1138z00_36, 1L);
									{	/* Llib/socket.scm 426 */
										obj_t BgL_errpz00_1342;

										BgL_errpz00_1342 = BgL_errpz00_1323;
										{	/* Llib/socket.scm 426 */
											obj_t BgL_inbufsz00_1343;

											BgL_inbufsz00_1343 = BgL_inbufsz00_1324;
											{	/* Llib/socket.scm 426 */
												obj_t BgL_outbufsz00_1344;

												BgL_outbufsz00_1344 = BgL_outbufsz00_1325;
												{	/* Llib/socket.scm 426 */
													obj_t BgL_auxz00_3427;
													obj_t BgL_auxz00_3420;

													if (VECTORP(BgL_arg1361z00_1341))
														{	/* Llib/socket.scm 426 */
															BgL_auxz00_3427 = BgL_arg1361z00_1341;
														}
													else
														{
															obj_t BgL_auxz00_3430;

															BgL_auxz00_3430 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string1937z00zz__socketz00, BINT(19386L),
																BGl_string2002z00zz__socketz00,
																BGl_string2003z00zz__socketz00,
																BgL_arg1361z00_1341);
															FAILURE(BgL_auxz00_3430, BFALSE, BFALSE);
														}
													if (SOCKETP(BgL_arg1360z00_1340))
														{	/* Llib/socket.scm 426 */
															BgL_auxz00_3420 = BgL_arg1360z00_1340;
														}
													else
														{
															obj_t BgL_auxz00_3423;

															BgL_auxz00_3423 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string1937z00zz__socketz00, BINT(19386L),
																BGl_string2002z00zz__socketz00,
																BGl_string1935z00zz__socketz00,
																BgL_arg1360z00_1340);
															FAILURE(BgL_auxz00_3423, BFALSE, BFALSE);
														}
													return
														BGl_socketzd2acceptzd2manyz00zz__socketz00
														(BgL_auxz00_3420, BgL_auxz00_3427, BgL_errpz00_1342,
														BgL_inbufsz00_1343, BgL_outbufsz00_1344);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* socket-accept-many */
	BGL_EXPORTED_DEF obj_t BGl_socketzd2acceptzd2manyz00zz__socketz00(obj_t
		BgL_socketz00_31, obj_t BgL_resultz00_32, obj_t BgL_errpz00_33,
		obj_t BgL_inbufsz00_34, obj_t BgL_outbufsz00_35)
	{
		{	/* Llib/socket.scm 426 */
			if (VECTORP(BgL_inbufsz00_34))
				{	/* Llib/socket.scm 427 */
					((bool_t) 0);
				}
			else
				{	/* Llib/socket.scm 427 */
					BgL_inbufsz00_34 =
						make_vector(VECTOR_LENGTH(BgL_resultz00_32), BUNSPEC);
					{
						long BgL_iz00_2090;

						BgL_iz00_2090 = 0L;
					BgL_loopz00_2089:
						if ((BgL_iz00_2090 < VECTOR_LENGTH(BgL_resultz00_32)))
							{	/* Llib/socket.scm 431 */
								obj_t BgL_bufz00_2096;

								BgL_bufz00_2096 =
									BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00
									(BGl_string2000z00zz__socketz00, BTRUE, (int) (512L));
								VECTOR_SET(BgL_inbufsz00_34, BgL_iz00_2090, BgL_bufz00_2096);
								{
									long BgL_iz00_3445;

									BgL_iz00_3445 = (BgL_iz00_2090 + 1L);
									BgL_iz00_2090 = BgL_iz00_3445;
									goto BgL_loopz00_2089;
								}
							}
						else
							{	/* Llib/socket.scm 430 */
								((bool_t) 0);
							}
					}
				}
			if (VECTORP(BgL_outbufsz00_35))
				{	/* Llib/socket.scm 434 */
					((bool_t) 0);
				}
			else
				{	/* Llib/socket.scm 434 */
					BgL_outbufsz00_35 =
						make_vector(VECTOR_LENGTH(BgL_resultz00_32), BUNSPEC);
					{
						long BgL_iz00_2111;

						BgL_iz00_2111 = 0L;
					BgL_loopz00_2110:
						if ((BgL_iz00_2111 < VECTOR_LENGTH(BgL_resultz00_32)))
							{	/* Llib/socket.scm 438 */
								obj_t BgL_bufz00_2117;

								BgL_bufz00_2117 =
									BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00
									(BGl_string2000z00zz__socketz00, BTRUE, (int) (512L));
								VECTOR_SET(BgL_outbufsz00_35, BgL_iz00_2111, BgL_bufz00_2117);
								{
									long BgL_iz00_3457;

									BgL_iz00_3457 = (BgL_iz00_2111 + 1L);
									BgL_iz00_2111 = BgL_iz00_3457;
									goto BgL_loopz00_2110;
								}
							}
						else
							{	/* Llib/socket.scm 437 */
								((bool_t) 0);
							}
					}
				}
			return
				BINT(bgl_socket_accept_many(BgL_socketz00_31,
					CBOOL(BgL_errpz00_33), BgL_inbufsz00_34, BgL_outbufsz00_35,
					BgL_resultz00_32));
		}

	}



/* _socket-shutdown */
	obj_t BGl__socketzd2shutdownzd2zz__socketz00(obj_t BgL_env1150z00_41,
		obj_t BgL_opt1149z00_40)
	{
		{	/* Llib/socket.scm 454 */
			{	/* Llib/socket.scm 454 */
				obj_t BgL_g1151z00_1376;

				BgL_g1151z00_1376 = VECTOR_REF(BgL_opt1149z00_40, 0L);
				switch (VECTOR_LENGTH(BgL_opt1149z00_40))
					{
					case 1L:

						{	/* Llib/socket.scm 454 */

							{	/* Llib/socket.scm 454 */
								int BgL_tmpz00_3463;

								{	/* Llib/socket.scm 454 */
									obj_t BgL_auxz00_3464;

									if (SOCKETP(BgL_g1151z00_1376))
										{	/* Llib/socket.scm 454 */
											BgL_auxz00_3464 = BgL_g1151z00_1376;
										}
									else
										{
											obj_t BgL_auxz00_3467;

											BgL_auxz00_3467 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string1937z00zz__socketz00, BINT(20524L),
												BGl_string2004z00zz__socketz00,
												BGl_string1935z00zz__socketz00, BgL_g1151z00_1376);
											FAILURE(BgL_auxz00_3467, BFALSE, BFALSE);
										}
									BgL_tmpz00_3463 =
										BGl_socketzd2shutdownzd2zz__socketz00(BgL_auxz00_3464,
										BTRUE);
								}
								return BINT(BgL_tmpz00_3463);
							}
						}
						break;
					case 2L:

						{	/* Llib/socket.scm 454 */
							obj_t BgL_howz00_1380;

							BgL_howz00_1380 = VECTOR_REF(BgL_opt1149z00_40, 1L);
							{	/* Llib/socket.scm 454 */

								{	/* Llib/socket.scm 454 */
									int BgL_tmpz00_3474;

									{	/* Llib/socket.scm 454 */
										obj_t BgL_auxz00_3475;

										if (SOCKETP(BgL_g1151z00_1376))
											{	/* Llib/socket.scm 454 */
												BgL_auxz00_3475 = BgL_g1151z00_1376;
											}
										else
											{
												obj_t BgL_auxz00_3478;

												BgL_auxz00_3478 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string1937z00zz__socketz00, BINT(20524L),
													BGl_string2004z00zz__socketz00,
													BGl_string1935z00zz__socketz00, BgL_g1151z00_1376);
												FAILURE(BgL_auxz00_3478, BFALSE, BFALSE);
											}
										BgL_tmpz00_3474 =
											BGl_socketzd2shutdownzd2zz__socketz00(BgL_auxz00_3475,
											BgL_howz00_1380);
									}
									return BINT(BgL_tmpz00_3474);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* socket-shutdown */
	BGL_EXPORTED_DEF int BGl_socketzd2shutdownzd2zz__socketz00(obj_t
		BgL_socketz00_38, obj_t BgL_howz00_39)
	{
		{	/* Llib/socket.scm 454 */
			if ((BgL_howz00_39 == BTRUE))
				{	/* Llib/socket.scm 457 */
					int BgL_rz00_1381;

					BgL_rz00_1381 = socket_shutdown(BgL_socketz00_38, (int) (2L));
					socket_close(BgL_socketz00_38);
					return BgL_rz00_1381;
				}
			else
				{	/* Llib/socket.scm 460 */
					bool_t BgL_test2284z00_3491;

					if ((BgL_howz00_39 == BFALSE))
						{	/* Llib/socket.scm 460 */
							BgL_test2284z00_3491 = ((bool_t) 1);
						}
					else
						{	/* Llib/socket.scm 460 */
							BgL_test2284z00_3491 =
								(BgL_howz00_39 == BGl_symbol2005z00zz__socketz00);
						}
					if (BgL_test2284z00_3491)
						{	/* Llib/socket.scm 460 */
							return socket_shutdown(BgL_socketz00_38, (int) (2L));
						}
					else
						{	/* Llib/socket.scm 460 */
							if ((BgL_howz00_39 == BGl_symbol2007z00zz__socketz00))
								{	/* Llib/socket.scm 462 */
									return socket_shutdown(BgL_socketz00_38, (int) (1L));
								}
							else
								{	/* Llib/socket.scm 462 */
									if ((BgL_howz00_39 == BGl_symbol2009z00zz__socketz00))
										{	/* Llib/socket.scm 464 */
											return socket_shutdown(BgL_socketz00_38, (int) (0L));
										}
									else
										{	/* Llib/socket.scm 464 */
											return
												CINT(BGl_errorz00zz__errorz00
												(BGl_string2011z00zz__socketz00,
													BGl_string2012z00zz__socketz00, BgL_howz00_39));
										}
								}
						}
				}
		}

	}



/* socket-close */
	BGL_EXPORTED_DEF obj_t BGl_socketzd2closezd2zz__socketz00(obj_t
		BgL_socketz00_42)
	{
		{	/* Llib/socket.scm 472 */
			BGL_TAIL return socket_close(BgL_socketz00_42);
		}

	}



/* &socket-close */
	obj_t BGl_z62socketzd2closezb0zz__socketz00(obj_t BgL_envz00_2421,
		obj_t BgL_socketz00_2422)
	{
		{	/* Llib/socket.scm 472 */
			{	/* Llib/socket.scm 473 */
				obj_t BgL_auxz00_3508;

				if (SOCKETP(BgL_socketz00_2422))
					{	/* Llib/socket.scm 473 */
						BgL_auxz00_3508 = BgL_socketz00_2422;
					}
				else
					{
						obj_t BgL_auxz00_3511;

						BgL_auxz00_3511 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1937z00zz__socketz00,
							BINT(21232L), BGl_string2013z00zz__socketz00,
							BGl_string1935z00zz__socketz00, BgL_socketz00_2422);
						FAILURE(BgL_auxz00_3511, BFALSE, BFALSE);
					}
				return BGl_socketzd2closezd2zz__socketz00(BgL_auxz00_3508);
			}
		}

	}



/* host */
	BGL_EXPORTED_DEF obj_t BGl_hostz00zz__socketz00(obj_t BgL_hostnamez00_43)
	{
		{	/* Llib/socket.scm 478 */
			BGl_z52socketzd2initz12z92zz__socketz00();
			BGL_TAIL return bgl_host(BgL_hostnamez00_43);
		}

	}



/* &host */
	obj_t BGl_z62hostz62zz__socketz00(obj_t BgL_envz00_2423,
		obj_t BgL_hostnamez00_2424)
	{
		{	/* Llib/socket.scm 478 */
			{	/* Llib/socket.scm 480 */
				obj_t BgL_auxz00_3518;

				if (STRINGP(BgL_hostnamez00_2424))
					{	/* Llib/socket.scm 480 */
						BgL_auxz00_3518 = BgL_hostnamez00_2424;
					}
				else
					{
						obj_t BgL_auxz00_3521;

						BgL_auxz00_3521 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1937z00zz__socketz00,
							BINT(21540L), BGl_string2014z00zz__socketz00,
							BGl_string1943z00zz__socketz00, BgL_hostnamez00_2424);
						FAILURE(BgL_auxz00_3521, BFALSE, BFALSE);
					}
				return BGl_hostz00zz__socketz00(BgL_auxz00_3518);
			}
		}

	}



/* hostinfo */
	BGL_EXPORTED_DEF obj_t BGl_hostinfoz00zz__socketz00(obj_t BgL_hostnamez00_44)
	{
		{	/* Llib/socket.scm 486 */
			BGl_z52socketzd2initz12z92zz__socketz00();
			BGL_TAIL return bgl_hostinfo(BgL_hostnamez00_44);
		}

	}



/* &hostinfo */
	obj_t BGl_z62hostinfoz62zz__socketz00(obj_t BgL_envz00_2425,
		obj_t BgL_hostnamez00_2426)
	{
		{	/* Llib/socket.scm 486 */
			{	/* Llib/socket.scm 488 */
				obj_t BgL_auxz00_3528;

				if (STRINGP(BgL_hostnamez00_2426))
					{	/* Llib/socket.scm 488 */
						BgL_auxz00_3528 = BgL_hostnamez00_2426;
					}
				else
					{
						obj_t BgL_auxz00_3531;

						BgL_auxz00_3531 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1937z00zz__socketz00,
							BINT(21855L), BGl_string2015z00zz__socketz00,
							BGl_string1943z00zz__socketz00, BgL_hostnamez00_2426);
						FAILURE(BgL_auxz00_3531, BFALSE, BFALSE);
					}
				return BGl_hostinfoz00zz__socketz00(BgL_auxz00_3528);
			}
		}

	}



/* _hostname */
	obj_t BGl__hostnamez00zz__socketz00(obj_t BgL_env1155z00_47,
		obj_t BgL_opt1154z00_46)
	{
		{	/* Llib/socket.scm 494 */
			{	/* Llib/socket.scm 494 */

				switch (VECTOR_LENGTH(BgL_opt1154z00_46))
					{
					case 0L:

						{	/* Llib/socket.scm 494 */

							BGl_z52socketzd2initz12z92zz__socketz00();
							return bgl_gethostname();
						}
						break;
					case 1L:

						{	/* Llib/socket.scm 494 */
							obj_t BgL_hostipz00_1386;

							BgL_hostipz00_1386 = VECTOR_REF(BgL_opt1154z00_46, 0L);
							{	/* Llib/socket.scm 494 */

								BGl_z52socketzd2initz12z92zz__socketz00();
								if (CBOOL(BgL_hostipz00_1386))
									{	/* Llib/socket.scm 497 */
										obj_t BgL_tmpz00_3542;

										if (STRINGP(BgL_hostipz00_1386))
											{	/* Llib/socket.scm 497 */
												BgL_tmpz00_3542 = BgL_hostipz00_1386;
											}
										else
											{
												obj_t BgL_auxz00_3545;

												BgL_auxz00_3545 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string1937z00zz__socketz00, BINT(22211L),
													BGl_string2016z00zz__socketz00,
													BGl_string1943z00zz__socketz00, BgL_hostipz00_1386);
												FAILURE(BgL_auxz00_3545, BFALSE, BFALSE);
											}
										return bgl_gethostname_by_address(BgL_tmpz00_3542);
									}
								else
									{	/* Llib/socket.scm 496 */
										return bgl_gethostname();
									}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* hostname */
	BGL_EXPORTED_DEF obj_t BGl_hostnamez00zz__socketz00(obj_t BgL_hostipz00_45)
	{
		{	/* Llib/socket.scm 494 */
			BGl_z52socketzd2initz12z92zz__socketz00();
			if (CBOOL(BgL_hostipz00_45))
				{	/* Llib/socket.scm 496 */
					return bgl_gethostname_by_address(BgL_hostipz00_45);
				}
			else
				{	/* Llib/socket.scm 496 */
					return bgl_gethostname();
				}
		}

	}



/* resolv */
	BGL_EXPORTED_DEF obj_t BGl_resolvz00zz__socketz00(obj_t BgL_hostnamez00_48,
		obj_t BgL_typez00_49)
	{
		{	/* Llib/socket.scm 503 */
			{	/* Llib/socket.scm 506 */
				obj_t BgL_arg1389z00_2123;

				BgL_arg1389z00_2123 = SYMBOL_TO_STRING(BgL_typez00_49);
				return bgl_res_query(BgL_hostnamez00_48, BgL_arg1389z00_2123);
			}
		}

	}



/* &resolv */
	obj_t BGl_z62resolvz62zz__socketz00(obj_t BgL_envz00_2427,
		obj_t BgL_hostnamez00_2428, obj_t BgL_typez00_2429)
	{
		{	/* Llib/socket.scm 503 */
			{	/* Llib/socket.scm 506 */
				obj_t BgL_auxz00_3567;
				obj_t BgL_auxz00_3560;

				if (SYMBOLP(BgL_typez00_2429))
					{	/* Llib/socket.scm 506 */
						BgL_auxz00_3567 = BgL_typez00_2429;
					}
				else
					{
						obj_t BgL_auxz00_3570;

						BgL_auxz00_3570 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1937z00zz__socketz00,
							BINT(22554L), BGl_string2017z00zz__socketz00,
							BGl_string1970z00zz__socketz00, BgL_typez00_2429);
						FAILURE(BgL_auxz00_3570, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_hostnamez00_2428))
					{	/* Llib/socket.scm 506 */
						BgL_auxz00_3560 = BgL_hostnamez00_2428;
					}
				else
					{
						obj_t BgL_auxz00_3563;

						BgL_auxz00_3563 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1937z00zz__socketz00,
							BINT(22554L), BGl_string2017z00zz__socketz00,
							BGl_string1943z00zz__socketz00, BgL_hostnamez00_2428);
						FAILURE(BgL_auxz00_3563, BFALSE, BFALSE);
					}
				return BGl_resolvz00zz__socketz00(BgL_auxz00_3560, BgL_auxz00_3567);
			}
		}

	}



/* get-interfaces */
	BGL_EXPORTED_DEF obj_t BGl_getzd2interfaceszd2zz__socketz00(void)
	{
		{	/* Llib/socket.scm 513 */
			BGl_z52socketzd2initz12z92zz__socketz00();
			BGL_TAIL return bgl_gethostinterfaces();
		}

	}



/* &get-interfaces */
	obj_t BGl_z62getzd2interfaceszb0zz__socketz00(obj_t BgL_envz00_2430)
	{
		{	/* Llib/socket.scm 513 */
			return BGl_getzd2interfaceszd2zz__socketz00();
		}

	}



/* get-protocols */
	BGL_EXPORTED_DEF obj_t BGl_getzd2protocolszd2zz__socketz00(void)
	{
		{	/* Llib/socket.scm 521 */
			BGL_TAIL return bgl_getprotoents();
		}

	}



/* &get-protocols */
	obj_t BGl_z62getzd2protocolszb0zz__socketz00(obj_t BgL_envz00_2431)
	{
		{	/* Llib/socket.scm 521 */
			return BGl_getzd2protocolszd2zz__socketz00();
		}

	}



/* get-protocol */
	BGL_EXPORTED_DEF obj_t BGl_getzd2protocolzd2zz__socketz00(obj_t
		BgL_protocolz00_50)
	{
		{	/* Llib/socket.scm 527 */
			if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BgL_protocolz00_50))
				{	/* Llib/socket.scm 529 */
					return bgl_getprotobynumber(CINT(BgL_protocolz00_50));
				}
			else
				{	/* Llib/socket.scm 529 */
					if (STRINGP(BgL_protocolz00_50))
						{	/* Llib/socket.scm 531 */
							return bgl_getprotobyname(BSTRING_TO_STRING(BgL_protocolz00_50));
						}
					else
						{	/* Llib/socket.scm 531 */
							return BFALSE;
						}
				}
		}

	}



/* &get-protocol */
	obj_t BGl_z62getzd2protocolzb0zz__socketz00(obj_t BgL_envz00_2432,
		obj_t BgL_protocolz00_2433)
	{
		{	/* Llib/socket.scm 527 */
			return BGl_getzd2protocolzd2zz__socketz00(BgL_protocolz00_2433);
		}

	}



/* socket-option */
	BGL_EXPORTED_DEF obj_t BGl_socketzd2optionzd2zz__socketz00(obj_t
		BgL_socketz00_51, obj_t BgL_optionz00_52)
	{
		{	/* Llib/socket.scm 539 */
			BGL_TAIL return bgl_getsockopt(BgL_socketz00_51, BgL_optionz00_52);
		}

	}



/* &socket-option */
	obj_t BGl_z62socketzd2optionzb0zz__socketz00(obj_t BgL_envz00_2434,
		obj_t BgL_socketz00_2435, obj_t BgL_optionz00_2436)
	{
		{	/* Llib/socket.scm 539 */
			{	/* Llib/socket.scm 540 */
				obj_t BgL_auxz00_3597;
				obj_t BgL_auxz00_3590;

				if (KEYWORDP(BgL_optionz00_2436))
					{	/* Llib/socket.scm 540 */
						BgL_auxz00_3597 = BgL_optionz00_2436;
					}
				else
					{
						obj_t BgL_auxz00_3600;

						BgL_auxz00_3600 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1937z00zz__socketz00,
							BINT(23881L), BGl_string2018z00zz__socketz00,
							BGl_string2019z00zz__socketz00, BgL_optionz00_2436);
						FAILURE(BgL_auxz00_3600, BFALSE, BFALSE);
					}
				if (SOCKETP(BgL_socketz00_2435))
					{	/* Llib/socket.scm 540 */
						BgL_auxz00_3590 = BgL_socketz00_2435;
					}
				else
					{
						obj_t BgL_auxz00_3593;

						BgL_auxz00_3593 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1937z00zz__socketz00,
							BINT(23881L), BGl_string2018z00zz__socketz00,
							BGl_string1935z00zz__socketz00, BgL_socketz00_2435);
						FAILURE(BgL_auxz00_3593, BFALSE, BFALSE);
					}
				return
					BGl_socketzd2optionzd2zz__socketz00(BgL_auxz00_3590, BgL_auxz00_3597);
			}
		}

	}



/* socket-option-set! */
	BGL_EXPORTED_DEF obj_t BGl_socketzd2optionzd2setz12z12zz__socketz00(obj_t
		BgL_socketz00_53, obj_t BgL_optionz00_54, obj_t BgL_valz00_55)
	{
		{	/* Llib/socket.scm 545 */
			BGL_TAIL return
				bgl_setsockopt(BgL_socketz00_53, BgL_optionz00_54, BgL_valz00_55);
		}

	}



/* &socket-option-set! */
	obj_t BGl_z62socketzd2optionzd2setz12z70zz__socketz00(obj_t BgL_envz00_2437,
		obj_t BgL_socketz00_2438, obj_t BgL_optionz00_2439, obj_t BgL_valz00_2440)
	{
		{	/* Llib/socket.scm 545 */
			{	/* Llib/socket.scm 546 */
				obj_t BgL_auxz00_3613;
				obj_t BgL_auxz00_3606;

				if (KEYWORDP(BgL_optionz00_2439))
					{	/* Llib/socket.scm 546 */
						BgL_auxz00_3613 = BgL_optionz00_2439;
					}
				else
					{
						obj_t BgL_auxz00_3616;

						BgL_auxz00_3616 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1937z00zz__socketz00,
							BINT(24190L), BGl_string2020z00zz__socketz00,
							BGl_string2019z00zz__socketz00, BgL_optionz00_2439);
						FAILURE(BgL_auxz00_3616, BFALSE, BFALSE);
					}
				if (SOCKETP(BgL_socketz00_2438))
					{	/* Llib/socket.scm 546 */
						BgL_auxz00_3606 = BgL_socketz00_2438;
					}
				else
					{
						obj_t BgL_auxz00_3609;

						BgL_auxz00_3609 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1937z00zz__socketz00,
							BINT(24190L), BGl_string2020z00zz__socketz00,
							BGl_string1935z00zz__socketz00, BgL_socketz00_2438);
						FAILURE(BgL_auxz00_3609, BFALSE, BFALSE);
					}
				return
					BGl_socketzd2optionzd2setz12z12zz__socketz00(BgL_auxz00_3606,
					BgL_auxz00_3613, BgL_valz00_2440);
			}
		}

	}



/* datagram-socket? */
	BGL_EXPORTED_DEF bool_t BGl_datagramzd2socketzf3z21zz__socketz00(obj_t
		BgL_objz00_56)
	{
		{	/* Llib/socket.scm 551 */
			return BGL_DATAGRAM_SOCKETP(BgL_objz00_56);
		}

	}



/* &datagram-socket? */
	obj_t BGl_z62datagramzd2socketzf3z43zz__socketz00(obj_t BgL_envz00_2441,
		obj_t BgL_objz00_2442)
	{
		{	/* Llib/socket.scm 551 */
			return BBOOL(BGl_datagramzd2socketzf3z21zz__socketz00(BgL_objz00_2442));
		}

	}



/* datagram-socket-server? */
	BGL_EXPORTED_DEF bool_t
		BGl_datagramzd2socketzd2serverzf3zf3zz__socketz00(obj_t BgL_objz00_57)
	{
		{	/* Llib/socket.scm 557 */
			return BGL_DATAGRAM_SOCKET_SERVERP(BgL_objz00_57);
		}

	}



/* &datagram-socket-server? */
	obj_t BGl_z62datagramzd2socketzd2serverzf3z91zz__socketz00(obj_t
		BgL_envz00_2443, obj_t BgL_objz00_2444)
	{
		{	/* Llib/socket.scm 557 */
			return
				BBOOL(BGl_datagramzd2socketzd2serverzf3zf3zz__socketz00
				(BgL_objz00_2444));
		}

	}



/* datagram-socket-client? */
	BGL_EXPORTED_DEF bool_t
		BGl_datagramzd2socketzd2clientzf3zf3zz__socketz00(obj_t BgL_objz00_58)
	{
		{	/* Llib/socket.scm 563 */
			return BGL_DATAGRAM_SOCKET_CLIENTP(BgL_objz00_58);
		}

	}



/* &datagram-socket-client? */
	obj_t BGl_z62datagramzd2socketzd2clientzf3z91zz__socketz00(obj_t
		BgL_envz00_2445, obj_t BgL_objz00_2446)
	{
		{	/* Llib/socket.scm 563 */
			return
				BBOOL(BGl_datagramzd2socketzd2clientzf3zf3zz__socketz00
				(BgL_objz00_2446));
		}

	}



/* datagram-socket-hostname */
	BGL_EXPORTED_DEF obj_t BGl_datagramzd2socketzd2hostnamez00zz__socketz00(obj_t
		BgL_socketz00_59)
	{
		{	/* Llib/socket.scm 569 */
			return BGL_DATAGRAM_SOCKET_HOSTNAME(BgL_socketz00_59);
		}

	}



/* &datagram-socket-hostname */
	obj_t BGl_z62datagramzd2socketzd2hostnamez62zz__socketz00(obj_t
		BgL_envz00_2447, obj_t BgL_socketz00_2448)
	{
		{	/* Llib/socket.scm 569 */
			{	/* Llib/socket.scm 570 */
				obj_t BgL_auxz00_3631;

				if (BGL_DATAGRAM_SOCKETP(BgL_socketz00_2448))
					{	/* Llib/socket.scm 570 */
						BgL_auxz00_3631 = BgL_socketz00_2448;
					}
				else
					{
						obj_t BgL_auxz00_3634;

						BgL_auxz00_3634 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1937z00zz__socketz00,
							BINT(25394L), BGl_string2021z00zz__socketz00,
							BGl_string2022z00zz__socketz00, BgL_socketz00_2448);
						FAILURE(BgL_auxz00_3634, BFALSE, BFALSE);
					}
				return
					BGl_datagramzd2socketzd2hostnamez00zz__socketz00(BgL_auxz00_3631);
			}
		}

	}



/* datagram-socket-host-address */
	BGL_EXPORTED_DEF obj_t
		BGl_datagramzd2socketzd2hostzd2addresszd2zz__socketz00(obj_t
		BgL_socketz00_60)
	{
		{	/* Llib/socket.scm 575 */
			return BGL_DATAGRAM_SOCKET_HOSTIP(BgL_socketz00_60);
		}

	}



/* &datagram-socket-host-address */
	obj_t BGl_z62datagramzd2socketzd2hostzd2addresszb0zz__socketz00(obj_t
		BgL_envz00_2449, obj_t BgL_socketz00_2450)
	{
		{	/* Llib/socket.scm 575 */
			{	/* Llib/socket.scm 576 */
				obj_t BgL_auxz00_3640;

				if (BGL_DATAGRAM_SOCKETP(BgL_socketz00_2450))
					{	/* Llib/socket.scm 576 */
						BgL_auxz00_3640 = BgL_socketz00_2450;
					}
				else
					{
						obj_t BgL_auxz00_3643;

						BgL_auxz00_3643 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1937z00zz__socketz00,
							BINT(25709L), BGl_string2023z00zz__socketz00,
							BGl_string2022z00zz__socketz00, BgL_socketz00_2450);
						FAILURE(BgL_auxz00_3643, BFALSE, BFALSE);
					}
				return
					BGl_datagramzd2socketzd2hostzd2addresszd2zz__socketz00
					(BgL_auxz00_3640);
			}
		}

	}



/* datagram-socket-port-number */
	BGL_EXPORTED_DEF obj_t
		BGl_datagramzd2socketzd2portzd2numberzd2zz__socketz00(obj_t
		BgL_socketz00_61)
	{
		{	/* Llib/socket.scm 581 */
			return BINT(BGL_DATAGRAM_SOCKET_PORTNUM(BgL_socketz00_61));
		}

	}



/* &datagram-socket-port-number */
	obj_t BGl_z62datagramzd2socketzd2portzd2numberzb0zz__socketz00(obj_t
		BgL_envz00_2451, obj_t BgL_socketz00_2452)
	{
		{	/* Llib/socket.scm 581 */
			{	/* Llib/socket.scm 582 */
				obj_t BgL_auxz00_3650;

				if (BGL_DATAGRAM_SOCKETP(BgL_socketz00_2452))
					{	/* Llib/socket.scm 582 */
						BgL_auxz00_3650 = BgL_socketz00_2452;
					}
				else
					{
						obj_t BgL_auxz00_3653;

						BgL_auxz00_3653 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1937z00zz__socketz00,
							BINT(26021L), BGl_string2024z00zz__socketz00,
							BGl_string2022z00zz__socketz00, BgL_socketz00_2452);
						FAILURE(BgL_auxz00_3653, BFALSE, BFALSE);
					}
				return
					BGl_datagramzd2socketzd2portzd2numberzd2zz__socketz00
					(BgL_auxz00_3650);
			}
		}

	}



/* datagram-socket-output */
	BGL_EXPORTED_DEF obj_t BGl_datagramzd2socketzd2outputz00zz__socketz00(obj_t
		BgL_socketz00_62)
	{
		{	/* Llib/socket.scm 587 */
			{	/* Llib/socket.scm 588 */
				bool_t BgL_test2305z00_3658;

				{	/* Llib/socket.scm 588 */
					obj_t BgL_arg1394z00_2640;

					BgL_arg1394z00_2640 = BGL_DATAGRAM_SOCKET_PORT(BgL_socketz00_62);
					BgL_test2305z00_3658 = OUTPUT_PORTP(BgL_arg1394z00_2640);
				}
				if (BgL_test2305z00_3658)
					{	/* Llib/socket.scm 588 */
						return BGL_DATAGRAM_SOCKET_PORT(BgL_socketz00_62);
					}
				else
					{	/* Llib/socket.scm 588 */
						return
							BGl_errorz00zz__errorz00(BGl_string2025z00zz__socketz00,
							BGl_string2026z00zz__socketz00, BgL_socketz00_62);
					}
			}
		}

	}



/* &datagram-socket-output */
	obj_t BGl_z62datagramzd2socketzd2outputz62zz__socketz00(obj_t BgL_envz00_2453,
		obj_t BgL_socketz00_2454)
	{
		{	/* Llib/socket.scm 587 */
			{	/* Llib/socket.scm 588 */
				obj_t BgL_auxz00_3663;

				if (BGL_DATAGRAM_SOCKETP(BgL_socketz00_2454))
					{	/* Llib/socket.scm 588 */
						BgL_auxz00_3663 = BgL_socketz00_2454;
					}
				else
					{
						obj_t BgL_auxz00_3666;

						BgL_auxz00_3666 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1937z00zz__socketz00,
							BINT(26333L), BGl_string2027z00zz__socketz00,
							BGl_string2022z00zz__socketz00, BgL_socketz00_2454);
						FAILURE(BgL_auxz00_3666, BFALSE, BFALSE);
					}
				return BGl_datagramzd2socketzd2outputz00zz__socketz00(BgL_auxz00_3663);
			}
		}

	}



/* datagram-socket-input */
	BGL_EXPORTED_DEF obj_t BGl_datagramzd2socketzd2inputz00zz__socketz00(obj_t
		BgL_socketz00_63)
	{
		{	/* Llib/socket.scm 596 */
			{	/* Llib/socket.scm 597 */
				bool_t BgL_test2307z00_3671;

				{	/* Llib/socket.scm 597 */
					obj_t BgL_arg1397z00_2641;

					BgL_arg1397z00_2641 = BGL_DATAGRAM_SOCKET_PORT(BgL_socketz00_63);
					BgL_test2307z00_3671 = INPUT_PORTP(BgL_arg1397z00_2641);
				}
				if (BgL_test2307z00_3671)
					{	/* Llib/socket.scm 597 */
						return BGL_DATAGRAM_SOCKET_PORT(BgL_socketz00_63);
					}
				else
					{	/* Llib/socket.scm 597 */
						return
							BGl_errorz00zz__errorz00(BGl_string2028z00zz__socketz00,
							BGl_string2029z00zz__socketz00, BgL_socketz00_63);
					}
			}
		}

	}



/* &datagram-socket-input */
	obj_t BGl_z62datagramzd2socketzd2inputz62zz__socketz00(obj_t BgL_envz00_2455,
		obj_t BgL_socketz00_2456)
	{
		{	/* Llib/socket.scm 596 */
			{	/* Llib/socket.scm 597 */
				obj_t BgL_auxz00_3676;

				if (BGL_DATAGRAM_SOCKETP(BgL_socketz00_2456))
					{	/* Llib/socket.scm 597 */
						BgL_auxz00_3676 = BgL_socketz00_2456;
					}
				else
					{
						obj_t BgL_auxz00_3679;

						BgL_auxz00_3679 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1937z00zz__socketz00,
							BINT(26782L), BGl_string2030z00zz__socketz00,
							BGl_string2022z00zz__socketz00, BgL_socketz00_2456);
						FAILURE(BgL_auxz00_3679, BFALSE, BFALSE);
					}
				return BGl_datagramzd2socketzd2inputz00zz__socketz00(BgL_auxz00_3676);
			}
		}

	}



/* _make-datagram-server-socket */
	obj_t BGl__makezd2datagramzd2serverzd2socketzd2zz__socketz00(obj_t
		BgL_env1159z00_67, obj_t BgL_opt1158z00_66)
	{
		{	/* Llib/socket.scm 605 */
			{	/* Llib/socket.scm 605 */

				switch (VECTOR_LENGTH(BgL_opt1158z00_66))
					{
					case 0L:

						{	/* Llib/socket.scm 605 */
							obj_t BgL_domainz00_2642;

							BgL_domainz00_2642 = BGl_symbol1948z00zz__socketz00;
							{	/* Llib/socket.scm 605 */

								{	/* Llib/socket.scm 605 */
									obj_t BgL_res1783z00_2643;

									BGl_z52socketzd2initz12z92zz__socketz00();
									{	/* Llib/socket.scm 607 */
										bool_t BgL_test2309z00_3685;

										{	/* Llib/socket.scm 607 */
											bool_t BgL__ortest_1053z00_2644;

											BgL__ortest_1053z00_2644 =
												(BgL_domainz00_2642 == BGl_symbol1948z00zz__socketz00);
											if (BgL__ortest_1053z00_2644)
												{	/* Llib/socket.scm 607 */
													BgL_test2309z00_3685 = BgL__ortest_1053z00_2644;
												}
											else
												{	/* Llib/socket.scm 607 */
													BgL_test2309z00_3685 =
														(BgL_domainz00_2642 ==
														BGl_symbol1966z00zz__socketz00);
												}
										}
										if (BgL_test2309z00_3685)
											{	/* Llib/socket.scm 607 */
												BgL_res1783z00_2643 =
													bgl_make_datagram_server_socket(
													(int) (0L), BgL_domainz00_2642);
											}
										else
											{	/* Llib/socket.scm 611 */
												obj_t BgL_aux1882z00_2645;

												BgL_aux1882z00_2645 =
													BGl_errorz00zz__errorz00
													(BGl_string2031z00zz__socketz00,
													BGl_string1982z00zz__socketz00, BgL_domainz00_2642);
												if (BGL_DATAGRAM_SOCKETP(BgL_aux1882z00_2645))
													{	/* Llib/socket.scm 611 */
														BgL_res1783z00_2643 = BgL_aux1882z00_2645;
													}
												else
													{
														obj_t BgL_auxz00_3694;

														BgL_auxz00_3694 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string1937z00zz__socketz00, BINT(27384L),
															BGl_string2032z00zz__socketz00,
															BGl_string2022z00zz__socketz00,
															BgL_aux1882z00_2645);
														FAILURE(BgL_auxz00_3694, BFALSE, BFALSE);
													}
											}
									}
									return BgL_res1783z00_2643;
								}
							}
						}
						break;
					case 1L:

						{	/* Llib/socket.scm 605 */
							obj_t BgL_portz00_2646;

							BgL_portz00_2646 = VECTOR_REF(BgL_opt1158z00_66, 0L);
							{	/* Llib/socket.scm 605 */
								obj_t BgL_domainz00_2647;

								BgL_domainz00_2647 = BGl_symbol1948z00zz__socketz00;
								{	/* Llib/socket.scm 605 */

									{	/* Llib/socket.scm 605 */
										obj_t BgL_res1784z00_2648;

										BGl_z52socketzd2initz12z92zz__socketz00();
										{	/* Llib/socket.scm 607 */
											bool_t BgL_test2312z00_3700;

											{	/* Llib/socket.scm 607 */
												bool_t BgL__ortest_1053z00_2649;

												BgL__ortest_1053z00_2649 =
													(BgL_domainz00_2647 ==
													BGl_symbol1948z00zz__socketz00);
												if (BgL__ortest_1053z00_2649)
													{	/* Llib/socket.scm 607 */
														BgL_test2312z00_3700 = BgL__ortest_1053z00_2649;
													}
												else
													{	/* Llib/socket.scm 607 */
														BgL_test2312z00_3700 =
															(BgL_domainz00_2647 ==
															BGl_symbol1966z00zz__socketz00);
													}
											}
											if (BgL_test2312z00_3700)
												{	/* Llib/socket.scm 609 */
													int BgL_tmpz00_3704;

													{	/* Llib/socket.scm 609 */
														obj_t BgL_tmpz00_3705;

														if (INTEGERP(BgL_portz00_2646))
															{	/* Llib/socket.scm 609 */
																BgL_tmpz00_3705 = BgL_portz00_2646;
															}
														else
															{
																obj_t BgL_auxz00_3708;

																BgL_auxz00_3708 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string1937z00zz__socketz00, BINT(27351L),
																	BGl_string2032z00zz__socketz00,
																	BGl_string1964z00zz__socketz00,
																	BgL_portz00_2646);
																FAILURE(BgL_auxz00_3708, BFALSE, BFALSE);
															}
														BgL_tmpz00_3704 = CINT(BgL_tmpz00_3705);
													}
													BgL_res1784z00_2648 =
														bgl_make_datagram_server_socket(BgL_tmpz00_3704,
														BgL_domainz00_2647);
												}
											else
												{	/* Llib/socket.scm 611 */
													obj_t BgL_aux1885z00_2650;

													BgL_aux1885z00_2650 =
														BGl_errorz00zz__errorz00
														(BGl_string2031z00zz__socketz00,
														BGl_string1982z00zz__socketz00, BgL_domainz00_2647);
													if (BGL_DATAGRAM_SOCKETP(BgL_aux1885z00_2650))
														{	/* Llib/socket.scm 611 */
															BgL_res1784z00_2648 = BgL_aux1885z00_2650;
														}
													else
														{
															obj_t BgL_auxz00_3717;

															BgL_auxz00_3717 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string1937z00zz__socketz00, BINT(27384L),
																BGl_string2032z00zz__socketz00,
																BGl_string2022z00zz__socketz00,
																BgL_aux1885z00_2650);
															FAILURE(BgL_auxz00_3717, BFALSE, BFALSE);
														}
												}
										}
										return BgL_res1784z00_2648;
									}
								}
							}
						}
						break;
					case 2L:

						{	/* Llib/socket.scm 605 */
							obj_t BgL_portz00_2651;

							BgL_portz00_2651 = VECTOR_REF(BgL_opt1158z00_66, 0L);
							{	/* Llib/socket.scm 605 */
								obj_t BgL_domainz00_2652;

								BgL_domainz00_2652 = VECTOR_REF(BgL_opt1158z00_66, 1L);
								{	/* Llib/socket.scm 605 */

									{	/* Llib/socket.scm 605 */
										obj_t BgL_res1785z00_2653;

										BGl_z52socketzd2initz12z92zz__socketz00();
										{	/* Llib/socket.scm 607 */
											bool_t BgL_test2316z00_3724;

											{	/* Llib/socket.scm 607 */
												bool_t BgL__ortest_1053z00_2654;

												BgL__ortest_1053z00_2654 =
													(BgL_domainz00_2652 ==
													BGl_symbol1948z00zz__socketz00);
												if (BgL__ortest_1053z00_2654)
													{	/* Llib/socket.scm 607 */
														BgL_test2316z00_3724 = BgL__ortest_1053z00_2654;
													}
												else
													{	/* Llib/socket.scm 607 */
														BgL_test2316z00_3724 =
															(BgL_domainz00_2652 ==
															BGl_symbol1966z00zz__socketz00);
													}
											}
											if (BgL_test2316z00_3724)
												{	/* Llib/socket.scm 609 */
													obj_t BgL_auxz00_3737;
													int BgL_tmpz00_3728;

													if (SYMBOLP(BgL_domainz00_2652))
														{	/* Llib/socket.scm 609 */
															BgL_auxz00_3737 = BgL_domainz00_2652;
														}
													else
														{
															obj_t BgL_auxz00_3740;

															BgL_auxz00_3740 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string1937z00zz__socketz00, BINT(27356L),
																BGl_string2032z00zz__socketz00,
																BGl_string1970z00zz__socketz00,
																BgL_domainz00_2652);
															FAILURE(BgL_auxz00_3740, BFALSE, BFALSE);
														}
													{	/* Llib/socket.scm 609 */
														obj_t BgL_tmpz00_3729;

														if (INTEGERP(BgL_portz00_2651))
															{	/* Llib/socket.scm 609 */
																BgL_tmpz00_3729 = BgL_portz00_2651;
															}
														else
															{
																obj_t BgL_auxz00_3732;

																BgL_auxz00_3732 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string1937z00zz__socketz00, BINT(27351L),
																	BGl_string2032z00zz__socketz00,
																	BGl_string1964z00zz__socketz00,
																	BgL_portz00_2651);
																FAILURE(BgL_auxz00_3732, BFALSE, BFALSE);
															}
														BgL_tmpz00_3728 = CINT(BgL_tmpz00_3729);
													}
													BgL_res1785z00_2653 =
														bgl_make_datagram_server_socket(BgL_tmpz00_3728,
														BgL_auxz00_3737);
												}
											else
												{	/* Llib/socket.scm 611 */
													obj_t BgL_aux1890z00_2655;

													BgL_aux1890z00_2655 =
														BGl_errorz00zz__errorz00
														(BGl_string2031z00zz__socketz00,
														BGl_string1982z00zz__socketz00, BgL_domainz00_2652);
													if (BGL_DATAGRAM_SOCKETP(BgL_aux1890z00_2655))
														{	/* Llib/socket.scm 611 */
															BgL_res1785z00_2653 = BgL_aux1890z00_2655;
														}
													else
														{
															obj_t BgL_auxz00_3748;

															BgL_auxz00_3748 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string1937z00zz__socketz00, BINT(27384L),
																BGl_string2032z00zz__socketz00,
																BGl_string2022z00zz__socketz00,
																BgL_aux1890z00_2655);
															FAILURE(BgL_auxz00_3748, BFALSE, BFALSE);
														}
												}
										}
										return BgL_res1785z00_2653;
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* make-datagram-server-socket */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2datagramzd2serverzd2socketzd2zz__socketz00(obj_t BgL_portz00_64,
		obj_t BgL_domainz00_65)
	{
		{	/* Llib/socket.scm 605 */
			BGl_z52socketzd2initz12z92zz__socketz00();
			{	/* Llib/socket.scm 607 */
				bool_t BgL_test2321z00_3755;

				{	/* Llib/socket.scm 607 */
					bool_t BgL__ortest_1053z00_2656;

					BgL__ortest_1053z00_2656 =
						(BgL_domainz00_65 == BGl_symbol1948z00zz__socketz00);
					if (BgL__ortest_1053z00_2656)
						{	/* Llib/socket.scm 607 */
							BgL_test2321z00_3755 = BgL__ortest_1053z00_2656;
						}
					else
						{	/* Llib/socket.scm 607 */
							BgL_test2321z00_3755 =
								(BgL_domainz00_65 == BGl_symbol1966z00zz__socketz00);
						}
				}
				if (BgL_test2321z00_3755)
					{	/* Llib/socket.scm 607 */
						return
							bgl_make_datagram_server_socket(CINT(BgL_portz00_64),
							BgL_domainz00_65);
					}
				else
					{	/* Llib/socket.scm 607 */
						return
							BGl_errorz00zz__errorz00(BGl_string2031z00zz__socketz00,
							BGl_string1982z00zz__socketz00, BgL_domainz00_65);
					}
			}
		}

	}



/* _make-datagram-unbound-socket */
	obj_t BGl__makezd2datagramzd2unboundzd2socketzd2zz__socketz00(obj_t
		BgL_env1163z00_70, obj_t BgL_opt1162z00_69)
	{
		{	/* Llib/socket.scm 616 */
			{	/* Llib/socket.scm 616 */

				switch (VECTOR_LENGTH(BgL_opt1162z00_69))
					{
					case 0L:

						{	/* Llib/socket.scm 616 */
							obj_t BgL_domainz00_2657;

							BgL_domainz00_2657 = BGl_symbol1948z00zz__socketz00;
							{	/* Llib/socket.scm 616 */

								{	/* Llib/socket.scm 616 */
									obj_t BgL_res1786z00_2658;

									BGl_z52socketzd2initz12z92zz__socketz00();
									{	/* Llib/socket.scm 618 */
										bool_t BgL_test2323z00_3763;

										{	/* Llib/socket.scm 618 */
											bool_t BgL__ortest_1056z00_2659;

											BgL__ortest_1056z00_2659 =
												(BgL_domainz00_2657 == BGl_symbol1948z00zz__socketz00);
											if (BgL__ortest_1056z00_2659)
												{	/* Llib/socket.scm 618 */
													BgL_test2323z00_3763 = BgL__ortest_1056z00_2659;
												}
											else
												{	/* Llib/socket.scm 618 */
													BgL_test2323z00_3763 =
														(BgL_domainz00_2657 ==
														BGl_symbol1966z00zz__socketz00);
												}
										}
										if (BgL_test2323z00_3763)
											{	/* Llib/socket.scm 618 */
												BgL_res1786z00_2658 =
													bgl_make_datagram_unbound_socket(BgL_domainz00_2657);
											}
										else
											{	/* Llib/socket.scm 622 */
												obj_t BgL_aux1892z00_2660;

												BgL_aux1892z00_2660 =
													BGl_errorz00zz__errorz00
													(BGl_string2033z00zz__socketz00,
													BGl_string1982z00zz__socketz00, BgL_domainz00_2657);
												if (BGL_DATAGRAM_SOCKETP(BgL_aux1892z00_2660))
													{	/* Llib/socket.scm 622 */
														BgL_res1786z00_2658 = BgL_aux1892z00_2660;
													}
												else
													{
														obj_t BgL_auxz00_3771;

														BgL_auxz00_3771 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string1937z00zz__socketz00, BINT(27877L),
															BGl_string2034z00zz__socketz00,
															BGl_string2022z00zz__socketz00,
															BgL_aux1892z00_2660);
														FAILURE(BgL_auxz00_3771, BFALSE, BFALSE);
													}
											}
									}
									return BgL_res1786z00_2658;
								}
							}
						}
						break;
					case 1L:

						{	/* Llib/socket.scm 616 */
							obj_t BgL_domainz00_2661;

							BgL_domainz00_2661 = VECTOR_REF(BgL_opt1162z00_69, 0L);
							{	/* Llib/socket.scm 616 */

								{	/* Llib/socket.scm 616 */
									obj_t BgL_res1787z00_2662;

									{	/* Llib/socket.scm 616 */
										obj_t BgL_domainz00_2663;

										if (SYMBOLP(BgL_domainz00_2661))
											{	/* Llib/socket.scm 616 */
												BgL_domainz00_2663 = BgL_domainz00_2661;
											}
										else
											{
												obj_t BgL_auxz00_3778;

												BgL_auxz00_3778 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string1937z00zz__socketz00, BINT(27676L),
													BGl_string2034z00zz__socketz00,
													BGl_string1970z00zz__socketz00, BgL_domainz00_2661);
												FAILURE(BgL_auxz00_3778, BFALSE, BFALSE);
											}
										BGl_z52socketzd2initz12z92zz__socketz00();
										{	/* Llib/socket.scm 618 */
											bool_t BgL_test2327z00_3783;

											{	/* Llib/socket.scm 618 */
												bool_t BgL__ortest_1056z00_2664;

												BgL__ortest_1056z00_2664 =
													(BgL_domainz00_2663 ==
													BGl_symbol1948z00zz__socketz00);
												if (BgL__ortest_1056z00_2664)
													{	/* Llib/socket.scm 618 */
														BgL_test2327z00_3783 = BgL__ortest_1056z00_2664;
													}
												else
													{	/* Llib/socket.scm 618 */
														BgL_test2327z00_3783 =
															(BgL_domainz00_2663 ==
															BGl_symbol1966z00zz__socketz00);
													}
											}
											if (BgL_test2327z00_3783)
												{	/* Llib/socket.scm 618 */
													BgL_res1787z00_2662 =
														bgl_make_datagram_unbound_socket
														(BgL_domainz00_2663);
												}
											else
												{	/* Llib/socket.scm 622 */
													obj_t BgL_aux1896z00_2665;

													BgL_aux1896z00_2665 =
														BGl_errorz00zz__errorz00
														(BGl_string2033z00zz__socketz00,
														BGl_string1982z00zz__socketz00, BgL_domainz00_2663);
													if (BGL_DATAGRAM_SOCKETP(BgL_aux1896z00_2665))
														{	/* Llib/socket.scm 622 */
															BgL_res1787z00_2662 = BgL_aux1896z00_2665;
														}
													else
														{
															obj_t BgL_auxz00_3791;

															BgL_auxz00_3791 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string1937z00zz__socketz00, BINT(27877L),
																BGl_string2034z00zz__socketz00,
																BGl_string2022z00zz__socketz00,
																BgL_aux1896z00_2665);
															FAILURE(BgL_auxz00_3791, BFALSE, BFALSE);
														}
												}
										}
									}
									return BgL_res1787z00_2662;
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* make-datagram-unbound-socket */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2datagramzd2unboundzd2socketzd2zz__socketz00(obj_t
		BgL_domainz00_68)
	{
		{	/* Llib/socket.scm 616 */
			BGl_z52socketzd2initz12z92zz__socketz00();
			{	/* Llib/socket.scm 618 */
				bool_t BgL_test2330z00_3798;

				{	/* Llib/socket.scm 618 */
					bool_t BgL__ortest_1056z00_2666;

					BgL__ortest_1056z00_2666 =
						(BgL_domainz00_68 == BGl_symbol1948z00zz__socketz00);
					if (BgL__ortest_1056z00_2666)
						{	/* Llib/socket.scm 618 */
							BgL_test2330z00_3798 = BgL__ortest_1056z00_2666;
						}
					else
						{	/* Llib/socket.scm 618 */
							BgL_test2330z00_3798 =
								(BgL_domainz00_68 == BGl_symbol1966z00zz__socketz00);
						}
				}
				if (BgL_test2330z00_3798)
					{	/* Llib/socket.scm 618 */
						BGL_TAIL return bgl_make_datagram_unbound_socket(BgL_domainz00_68);
					}
				else
					{	/* Llib/socket.scm 618 */
						return
							BGl_errorz00zz__errorz00(BGl_string2033z00zz__socketz00,
							BGl_string1982z00zz__socketz00, BgL_domainz00_68);
					}
			}
		}

	}



/* _make-datagram-client-socket */
	obj_t BGl__makezd2datagramzd2clientzd2socketzd2zz__socketz00(obj_t
		BgL_env1167z00_76, obj_t BgL_opt1166z00_75)
	{
		{	/* Llib/socket.scm 627 */
			{	/* Llib/socket.scm 627 */
				obj_t BgL_g1168z00_2667;
				obj_t BgL_g1169z00_2668;

				BgL_g1168z00_2667 = VECTOR_REF(BgL_opt1166z00_75, 0L);
				BgL_g1169z00_2668 = VECTOR_REF(BgL_opt1166z00_75, 1L);
				switch (VECTOR_LENGTH(BgL_opt1166z00_75))
					{
					case 2L:

						{	/* Llib/socket.scm 627 */
							obj_t BgL_domainz00_2669;

							BgL_domainz00_2669 = BGl_symbol1948z00zz__socketz00;
							{	/* Llib/socket.scm 627 */

								{	/* Llib/socket.scm 627 */
									obj_t BgL_res1788z00_2670;

									{	/* Llib/socket.scm 627 */
										obj_t BgL_hostnamez00_2671;
										int BgL_portz00_2672;

										if (STRINGP(BgL_g1168z00_2667))
											{	/* Llib/socket.scm 627 */
												BgL_hostnamez00_2671 = BgL_g1168z00_2667;
											}
										else
											{
												obj_t BgL_auxz00_3808;

												BgL_auxz00_3808 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string1937z00zz__socketz00, BINT(28170L),
													BGl_string2035z00zz__socketz00,
													BGl_string1943z00zz__socketz00, BgL_g1168z00_2667);
												FAILURE(BgL_auxz00_3808, BFALSE, BFALSE);
											}
										{	/* Llib/socket.scm 627 */
											obj_t BgL_tmpz00_3812;

											if (INTEGERP(BgL_g1169z00_2668))
												{	/* Llib/socket.scm 627 */
													BgL_tmpz00_3812 = BgL_g1169z00_2668;
												}
											else
												{
													obj_t BgL_auxz00_3815;

													BgL_auxz00_3815 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string1937z00zz__socketz00, BINT(28170L),
														BGl_string2035z00zz__socketz00,
														BGl_string1964z00zz__socketz00, BgL_g1169z00_2668);
													FAILURE(BgL_auxz00_3815, BFALSE, BFALSE);
												}
											BgL_portz00_2672 = CINT(BgL_tmpz00_3812);
										}
										BGl_z52socketzd2initz12z92zz__socketz00();
										{	/* Llib/socket.scm 629 */
											bool_t BgL_test2334z00_3821;

											{	/* Llib/socket.scm 629 */
												bool_t BgL__ortest_1059z00_2673;

												BgL__ortest_1059z00_2673 =
													(BgL_domainz00_2669 ==
													BGl_symbol1948z00zz__socketz00);
												if (BgL__ortest_1059z00_2673)
													{	/* Llib/socket.scm 629 */
														BgL_test2334z00_3821 = BgL__ortest_1059z00_2673;
													}
												else
													{	/* Llib/socket.scm 629 */
														bool_t BgL__ortest_1060z00_2674;

														BgL__ortest_1060z00_2674 =
															(BgL_domainz00_2669 ==
															BGl_symbol1966z00zz__socketz00);
														if (BgL__ortest_1060z00_2674)
															{	/* Llib/socket.scm 629 */
																BgL_test2334z00_3821 = BgL__ortest_1060z00_2674;
															}
														else
															{	/* Llib/socket.scm 629 */
																BgL_test2334z00_3821 =
																	(BgL_domainz00_2669 ==
																	BGl_symbol1968z00zz__socketz00);
															}
													}
											}
											if (BgL_test2334z00_3821)
												{	/* Llib/socket.scm 629 */
													BgL_res1788z00_2670 =
														bgl_make_datagram_client_socket
														(BgL_hostnamez00_2671, BgL_portz00_2672,
														((bool_t) 0), BgL_domainz00_2669);
												}
											else
												{	/* Llib/socket.scm 633 */
													obj_t BgL_aux1901z00_2675;

													BgL_aux1901z00_2675 =
														BGl_errorz00zz__errorz00
														(BGl_string2036z00zz__socketz00,
														BGl_string1982z00zz__socketz00, BgL_domainz00_2669);
													if (BGL_DATAGRAM_SOCKETP(BgL_aux1901z00_2675))
														{	/* Llib/socket.scm 633 */
															BgL_res1788z00_2670 = BgL_aux1901z00_2675;
														}
													else
														{
															obj_t BgL_auxz00_3831;

															BgL_auxz00_3831 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string1937z00zz__socketz00, BINT(28424L),
																BGl_string2035z00zz__socketz00,
																BGl_string2022z00zz__socketz00,
																BgL_aux1901z00_2675);
															FAILURE(BgL_auxz00_3831, BFALSE, BFALSE);
														}
												}
										}
									}
									return BgL_res1788z00_2670;
								}
							}
						}
						break;
					case 3L:

						{	/* Llib/socket.scm 627 */
							obj_t BgL_broadcastz00_2676;

							BgL_broadcastz00_2676 = VECTOR_REF(BgL_opt1166z00_75, 2L);
							{	/* Llib/socket.scm 627 */
								obj_t BgL_domainz00_2677;

								BgL_domainz00_2677 = BGl_symbol1948z00zz__socketz00;
								{	/* Llib/socket.scm 627 */

									{	/* Llib/socket.scm 627 */
										obj_t BgL_res1789z00_2678;

										{	/* Llib/socket.scm 627 */
											obj_t BgL_hostnamez00_2679;
											int BgL_portz00_2680;

											if (STRINGP(BgL_g1168z00_2667))
												{	/* Llib/socket.scm 627 */
													BgL_hostnamez00_2679 = BgL_g1168z00_2667;
												}
											else
												{
													obj_t BgL_auxz00_3838;

													BgL_auxz00_3838 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string1937z00zz__socketz00, BINT(28170L),
														BGl_string2035z00zz__socketz00,
														BGl_string1943z00zz__socketz00, BgL_g1168z00_2667);
													FAILURE(BgL_auxz00_3838, BFALSE, BFALSE);
												}
											{	/* Llib/socket.scm 627 */
												obj_t BgL_tmpz00_3842;

												if (INTEGERP(BgL_g1169z00_2668))
													{	/* Llib/socket.scm 627 */
														BgL_tmpz00_3842 = BgL_g1169z00_2668;
													}
												else
													{
														obj_t BgL_auxz00_3845;

														BgL_auxz00_3845 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string1937z00zz__socketz00, BINT(28170L),
															BGl_string2035z00zz__socketz00,
															BGl_string1964z00zz__socketz00,
															BgL_g1169z00_2668);
														FAILURE(BgL_auxz00_3845, BFALSE, BFALSE);
													}
												BgL_portz00_2680 = CINT(BgL_tmpz00_3842);
											}
											BGl_z52socketzd2initz12z92zz__socketz00();
											{	/* Llib/socket.scm 629 */
												bool_t BgL_test2340z00_3851;

												{	/* Llib/socket.scm 629 */
													bool_t BgL__ortest_1059z00_2681;

													BgL__ortest_1059z00_2681 =
														(BgL_domainz00_2677 ==
														BGl_symbol1948z00zz__socketz00);
													if (BgL__ortest_1059z00_2681)
														{	/* Llib/socket.scm 629 */
															BgL_test2340z00_3851 = BgL__ortest_1059z00_2681;
														}
													else
														{	/* Llib/socket.scm 629 */
															bool_t BgL__ortest_1060z00_2682;

															BgL__ortest_1060z00_2682 =
																(BgL_domainz00_2677 ==
																BGl_symbol1966z00zz__socketz00);
															if (BgL__ortest_1060z00_2682)
																{	/* Llib/socket.scm 629 */
																	BgL_test2340z00_3851 =
																		BgL__ortest_1060z00_2682;
																}
															else
																{	/* Llib/socket.scm 629 */
																	BgL_test2340z00_3851 =
																		(BgL_domainz00_2677 ==
																		BGl_symbol1968z00zz__socketz00);
																}
														}
												}
												if (BgL_test2340z00_3851)
													{	/* Llib/socket.scm 629 */
														BgL_res1789z00_2678 =
															bgl_make_datagram_client_socket
															(BgL_hostnamez00_2679, BgL_portz00_2680,
															CBOOL(BgL_broadcastz00_2676), BgL_domainz00_2677);
													}
												else
													{	/* Llib/socket.scm 633 */
														obj_t BgL_aux1906z00_2683;

														BgL_aux1906z00_2683 =
															BGl_errorz00zz__errorz00
															(BGl_string2036z00zz__socketz00,
															BGl_string1982z00zz__socketz00,
															BgL_domainz00_2677);
														if (BGL_DATAGRAM_SOCKETP(BgL_aux1906z00_2683))
															{	/* Llib/socket.scm 633 */
																BgL_res1789z00_2678 = BgL_aux1906z00_2683;
															}
														else
															{
																obj_t BgL_auxz00_3862;

																BgL_auxz00_3862 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string1937z00zz__socketz00, BINT(28424L),
																	BGl_string2035z00zz__socketz00,
																	BGl_string2022z00zz__socketz00,
																	BgL_aux1906z00_2683);
																FAILURE(BgL_auxz00_3862, BFALSE, BFALSE);
															}
													}
											}
										}
										return BgL_res1789z00_2678;
									}
								}
							}
						}
						break;
					case 4L:

						{	/* Llib/socket.scm 627 */
							obj_t BgL_broadcastz00_2684;

							BgL_broadcastz00_2684 = VECTOR_REF(BgL_opt1166z00_75, 2L);
							{	/* Llib/socket.scm 627 */
								obj_t BgL_domainz00_2685;

								BgL_domainz00_2685 = VECTOR_REF(BgL_opt1166z00_75, 3L);
								{	/* Llib/socket.scm 627 */

									{	/* Llib/socket.scm 627 */
										obj_t BgL_res1790z00_2686;

										{	/* Llib/socket.scm 627 */
											obj_t BgL_hostnamez00_2687;
											int BgL_portz00_2688;
											obj_t BgL_domainz00_2689;

											if (STRINGP(BgL_g1168z00_2667))
												{	/* Llib/socket.scm 627 */
													BgL_hostnamez00_2687 = BgL_g1168z00_2667;
												}
											else
												{
													obj_t BgL_auxz00_3870;

													BgL_auxz00_3870 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string1937z00zz__socketz00, BINT(28170L),
														BGl_string2035z00zz__socketz00,
														BGl_string1943z00zz__socketz00, BgL_g1168z00_2667);
													FAILURE(BgL_auxz00_3870, BFALSE, BFALSE);
												}
											{	/* Llib/socket.scm 627 */
												obj_t BgL_tmpz00_3874;

												if (INTEGERP(BgL_g1169z00_2668))
													{	/* Llib/socket.scm 627 */
														BgL_tmpz00_3874 = BgL_g1169z00_2668;
													}
												else
													{
														obj_t BgL_auxz00_3877;

														BgL_auxz00_3877 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string1937z00zz__socketz00, BINT(28170L),
															BGl_string2035z00zz__socketz00,
															BGl_string1964z00zz__socketz00,
															BgL_g1169z00_2668);
														FAILURE(BgL_auxz00_3877, BFALSE, BFALSE);
													}
												BgL_portz00_2688 = CINT(BgL_tmpz00_3874);
											}
											if (SYMBOLP(BgL_domainz00_2685))
												{	/* Llib/socket.scm 627 */
													BgL_domainz00_2689 = BgL_domainz00_2685;
												}
											else
												{
													obj_t BgL_auxz00_3884;

													BgL_auxz00_3884 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string1937z00zz__socketz00, BINT(28170L),
														BGl_string2035z00zz__socketz00,
														BGl_string1970z00zz__socketz00, BgL_domainz00_2685);
													FAILURE(BgL_auxz00_3884, BFALSE, BFALSE);
												}
											BGl_z52socketzd2initz12z92zz__socketz00();
											{	/* Llib/socket.scm 629 */
												bool_t BgL_test2347z00_3889;

												{	/* Llib/socket.scm 629 */
													bool_t BgL__ortest_1059z00_2690;

													BgL__ortest_1059z00_2690 =
														(BgL_domainz00_2689 ==
														BGl_symbol1948z00zz__socketz00);
													if (BgL__ortest_1059z00_2690)
														{	/* Llib/socket.scm 629 */
															BgL_test2347z00_3889 = BgL__ortest_1059z00_2690;
														}
													else
														{	/* Llib/socket.scm 629 */
															bool_t BgL__ortest_1060z00_2691;

															BgL__ortest_1060z00_2691 =
																(BgL_domainz00_2689 ==
																BGl_symbol1966z00zz__socketz00);
															if (BgL__ortest_1060z00_2691)
																{	/* Llib/socket.scm 629 */
																	BgL_test2347z00_3889 =
																		BgL__ortest_1060z00_2691;
																}
															else
																{	/* Llib/socket.scm 629 */
																	BgL_test2347z00_3889 =
																		(BgL_domainz00_2689 ==
																		BGl_symbol1968z00zz__socketz00);
																}
														}
												}
												if (BgL_test2347z00_3889)
													{	/* Llib/socket.scm 629 */
														BgL_res1790z00_2686 =
															bgl_make_datagram_client_socket
															(BgL_hostnamez00_2687, BgL_portz00_2688,
															CBOOL(BgL_broadcastz00_2684), BgL_domainz00_2689);
													}
												else
													{	/* Llib/socket.scm 633 */
														obj_t BgL_aux1913z00_2692;

														BgL_aux1913z00_2692 =
															BGl_errorz00zz__errorz00
															(BGl_string2036z00zz__socketz00,
															BGl_string1982z00zz__socketz00,
															BgL_domainz00_2689);
														if (BGL_DATAGRAM_SOCKETP(BgL_aux1913z00_2692))
															{	/* Llib/socket.scm 633 */
																BgL_res1790z00_2686 = BgL_aux1913z00_2692;
															}
														else
															{
																obj_t BgL_auxz00_3900;

																BgL_auxz00_3900 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string1937z00zz__socketz00, BINT(28424L),
																	BGl_string2035z00zz__socketz00,
																	BGl_string2022z00zz__socketz00,
																	BgL_aux1913z00_2692);
																FAILURE(BgL_auxz00_3900, BFALSE, BFALSE);
															}
													}
											}
										}
										return BgL_res1790z00_2686;
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* make-datagram-client-socket */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2datagramzd2clientzd2socketzd2zz__socketz00(obj_t
		BgL_hostnamez00_71, int BgL_portz00_72, obj_t BgL_broadcastz00_73,
		obj_t BgL_domainz00_74)
	{
		{	/* Llib/socket.scm 627 */
			BGl_z52socketzd2initz12z92zz__socketz00();
			{	/* Llib/socket.scm 629 */
				bool_t BgL_test2351z00_3907;

				{	/* Llib/socket.scm 629 */
					bool_t BgL__ortest_1059z00_2693;

					BgL__ortest_1059z00_2693 =
						(BgL_domainz00_74 == BGl_symbol1948z00zz__socketz00);
					if (BgL__ortest_1059z00_2693)
						{	/* Llib/socket.scm 629 */
							BgL_test2351z00_3907 = BgL__ortest_1059z00_2693;
						}
					else
						{	/* Llib/socket.scm 629 */
							bool_t BgL__ortest_1060z00_2694;

							BgL__ortest_1060z00_2694 =
								(BgL_domainz00_74 == BGl_symbol1966z00zz__socketz00);
							if (BgL__ortest_1060z00_2694)
								{	/* Llib/socket.scm 629 */
									BgL_test2351z00_3907 = BgL__ortest_1060z00_2694;
								}
							else
								{	/* Llib/socket.scm 629 */
									BgL_test2351z00_3907 =
										(BgL_domainz00_74 == BGl_symbol1968z00zz__socketz00);
								}
						}
				}
				if (BgL_test2351z00_3907)
					{	/* Llib/socket.scm 629 */
						return
							bgl_make_datagram_client_socket(BgL_hostnamez00_71,
							BgL_portz00_72, CBOOL(BgL_broadcastz00_73), BgL_domainz00_74);
					}
				else
					{	/* Llib/socket.scm 629 */
						return
							BGl_errorz00zz__errorz00(BGl_string2036z00zz__socketz00,
							BGl_string1982z00zz__socketz00, BgL_domainz00_74);
					}
			}
		}

	}



/* datagram-socket-close */
	BGL_EXPORTED_DEF obj_t BGl_datagramzd2socketzd2closez00zz__socketz00(obj_t
		BgL_socketz00_77)
	{
		{	/* Llib/socket.scm 638 */
			BGL_TAIL return bgl_datagram_socket_close(BgL_socketz00_77);
		}

	}



/* &datagram-socket-close */
	obj_t BGl_z62datagramzd2socketzd2closez62zz__socketz00(obj_t BgL_envz00_2457,
		obj_t BgL_socketz00_2458)
	{
		{	/* Llib/socket.scm 638 */
			{	/* Llib/socket.scm 639 */
				obj_t BgL_auxz00_3917;

				if (BGL_DATAGRAM_SOCKETP(BgL_socketz00_2458))
					{	/* Llib/socket.scm 639 */
						BgL_auxz00_3917 = BgL_socketz00_2458;
					}
				else
					{
						obj_t BgL_auxz00_3920;

						BgL_auxz00_3920 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1937z00zz__socketz00,
							BINT(28765L), BGl_string2037z00zz__socketz00,
							BGl_string2022z00zz__socketz00, BgL_socketz00_2458);
						FAILURE(BgL_auxz00_3920, BFALSE, BFALSE);
					}
				return BGl_datagramzd2socketzd2closez00zz__socketz00(BgL_auxz00_3917);
			}
		}

	}



/* datagram-socket-receive */
	BGL_EXPORTED_DEF obj_t BGl_datagramzd2socketzd2receivez00zz__socketz00(obj_t
		BgL_socketz00_78, int BgL_lengthz00_79)
	{
		{	/* Llib/socket.scm 644 */
			return
				bgl_datagram_socket_receive(BgL_socketz00_78,
				(long) (BgL_lengthz00_79));
		}

	}



/* &datagram-socket-receive */
	obj_t BGl_z62datagramzd2socketzd2receivez62zz__socketz00(obj_t
		BgL_envz00_2459, obj_t BgL_socketz00_2460, obj_t BgL_lengthz00_2461)
	{
		{	/* Llib/socket.scm 644 */
			{	/* Llib/socket.scm 645 */
				int BgL_auxz00_3934;
				obj_t BgL_auxz00_3927;

				{	/* Llib/socket.scm 645 */
					obj_t BgL_tmpz00_3935;

					if (INTEGERP(BgL_lengthz00_2461))
						{	/* Llib/socket.scm 645 */
							BgL_tmpz00_3935 = BgL_lengthz00_2461;
						}
					else
						{
							obj_t BgL_auxz00_3938;

							BgL_auxz00_3938 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1937z00zz__socketz00,
								BINT(29082L), BGl_string2038z00zz__socketz00,
								BGl_string1964z00zz__socketz00, BgL_lengthz00_2461);
							FAILURE(BgL_auxz00_3938, BFALSE, BFALSE);
						}
					BgL_auxz00_3934 = CINT(BgL_tmpz00_3935);
				}
				if (BGL_DATAGRAM_SOCKETP(BgL_socketz00_2460))
					{	/* Llib/socket.scm 645 */
						BgL_auxz00_3927 = BgL_socketz00_2460;
					}
				else
					{
						obj_t BgL_auxz00_3930;

						BgL_auxz00_3930 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1937z00zz__socketz00,
							BINT(29082L), BGl_string2038z00zz__socketz00,
							BGl_string2022z00zz__socketz00, BgL_socketz00_2460);
						FAILURE(BgL_auxz00_3930, BFALSE, BFALSE);
					}
				return
					BGl_datagramzd2socketzd2receivez00zz__socketz00(BgL_auxz00_3927,
					BgL_auxz00_3934);
			}
		}

	}



/* datagram-socket-send */
	BGL_EXPORTED_DEF obj_t BGl_datagramzd2socketzd2sendz00zz__socketz00(obj_t
		BgL_socketz00_80, obj_t BgL_stringz00_81, obj_t BgL_hostz00_82,
		int BgL_portz00_83)
	{
		{	/* Llib/socket.scm 650 */
			BGL_TAIL return
				bgl_datagram_socket_send(BgL_socketz00_80, BgL_stringz00_81,
				BgL_hostz00_82, BgL_portz00_83);
		}

	}



/* &datagram-socket-send */
	obj_t BGl_z62datagramzd2socketzd2sendz62zz__socketz00(obj_t BgL_envz00_2462,
		obj_t BgL_socketz00_2463, obj_t BgL_stringz00_2464, obj_t BgL_hostz00_2465,
		obj_t BgL_portz00_2466)
	{
		{	/* Llib/socket.scm 650 */
			{	/* Llib/socket.scm 651 */
				int BgL_auxz00_3966;
				obj_t BgL_auxz00_3959;
				obj_t BgL_auxz00_3952;
				obj_t BgL_auxz00_3945;

				{	/* Llib/socket.scm 651 */
					obj_t BgL_tmpz00_3967;

					if (INTEGERP(BgL_portz00_2466))
						{	/* Llib/socket.scm 651 */
							BgL_tmpz00_3967 = BgL_portz00_2466;
						}
					else
						{
							obj_t BgL_auxz00_3970;

							BgL_auxz00_3970 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1937z00zz__socketz00,
								BINT(29412L), BGl_string2039z00zz__socketz00,
								BGl_string1964z00zz__socketz00, BgL_portz00_2466);
							FAILURE(BgL_auxz00_3970, BFALSE, BFALSE);
						}
					BgL_auxz00_3966 = CINT(BgL_tmpz00_3967);
				}
				if (STRINGP(BgL_hostz00_2465))
					{	/* Llib/socket.scm 651 */
						BgL_auxz00_3959 = BgL_hostz00_2465;
					}
				else
					{
						obj_t BgL_auxz00_3962;

						BgL_auxz00_3962 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1937z00zz__socketz00,
							BINT(29412L), BGl_string2039z00zz__socketz00,
							BGl_string1943z00zz__socketz00, BgL_hostz00_2465);
						FAILURE(BgL_auxz00_3962, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_stringz00_2464))
					{	/* Llib/socket.scm 651 */
						BgL_auxz00_3952 = BgL_stringz00_2464;
					}
				else
					{
						obj_t BgL_auxz00_3955;

						BgL_auxz00_3955 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1937z00zz__socketz00,
							BINT(29412L), BGl_string2039z00zz__socketz00,
							BGl_string1943z00zz__socketz00, BgL_stringz00_2464);
						FAILURE(BgL_auxz00_3955, BFALSE, BFALSE);
					}
				if (BGL_DATAGRAM_SOCKETP(BgL_socketz00_2463))
					{	/* Llib/socket.scm 651 */
						BgL_auxz00_3945 = BgL_socketz00_2463;
					}
				else
					{
						obj_t BgL_auxz00_3948;

						BgL_auxz00_3948 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1937z00zz__socketz00,
							BINT(29412L), BGl_string2039z00zz__socketz00,
							BGl_string2022z00zz__socketz00, BgL_socketz00_2463);
						FAILURE(BgL_auxz00_3948, BFALSE, BFALSE);
					}
				return
					BGl_datagramzd2socketzd2sendz00zz__socketz00(BgL_auxz00_3945,
					BgL_auxz00_3952, BgL_auxz00_3959, BgL_auxz00_3966);
			}
		}

	}



/* datagram-socket-option */
	BGL_EXPORTED_DEF obj_t BGl_datagramzd2socketzd2optionz00zz__socketz00(obj_t
		BgL_socketz00_84, obj_t BgL_optionz00_85)
	{
		{	/* Llib/socket.scm 656 */
			BGL_TAIL return bgl_getsockopt(BgL_socketz00_84, BgL_optionz00_85);
		}

	}



/* &datagram-socket-option */
	obj_t BGl_z62datagramzd2socketzd2optionz62zz__socketz00(obj_t BgL_envz00_2467,
		obj_t BgL_socketz00_2468, obj_t BgL_optionz00_2469)
	{
		{	/* Llib/socket.scm 656 */
			{	/* Llib/socket.scm 657 */
				obj_t BgL_auxz00_3984;
				obj_t BgL_auxz00_3977;

				if (KEYWORDP(BgL_optionz00_2469))
					{	/* Llib/socket.scm 657 */
						BgL_auxz00_3984 = BgL_optionz00_2469;
					}
				else
					{
						obj_t BgL_auxz00_3987;

						BgL_auxz00_3987 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1937z00zz__socketz00,
							BINT(29741L), BGl_string2040z00zz__socketz00,
							BGl_string2019z00zz__socketz00, BgL_optionz00_2469);
						FAILURE(BgL_auxz00_3987, BFALSE, BFALSE);
					}
				if (BGL_DATAGRAM_SOCKETP(BgL_socketz00_2468))
					{	/* Llib/socket.scm 657 */
						BgL_auxz00_3977 = BgL_socketz00_2468;
					}
				else
					{
						obj_t BgL_auxz00_3980;

						BgL_auxz00_3980 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1937z00zz__socketz00,
							BINT(29741L), BGl_string2040z00zz__socketz00,
							BGl_string2022z00zz__socketz00, BgL_socketz00_2468);
						FAILURE(BgL_auxz00_3980, BFALSE, BFALSE);
					}
				return
					BGl_datagramzd2socketzd2optionz00zz__socketz00(BgL_auxz00_3977,
					BgL_auxz00_3984);
			}
		}

	}



/* datagram-socket-option-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_datagramzd2socketzd2optionzd2setz12zc0zz__socketz00(obj_t
		BgL_socketz00_86, obj_t BgL_optionz00_87, obj_t BgL_valz00_88)
	{
		{	/* Llib/socket.scm 662 */
			BGL_TAIL return
				bgl_setsockopt(BgL_socketz00_86, BgL_optionz00_87, BgL_valz00_88);
		}

	}



/* &datagram-socket-option-set! */
	obj_t BGl_z62datagramzd2socketzd2optionzd2setz12za2zz__socketz00(obj_t
		BgL_envz00_2470, obj_t BgL_socketz00_2471, obj_t BgL_optionz00_2472,
		obj_t BgL_valz00_2473)
	{
		{	/* Llib/socket.scm 662 */
			{	/* Llib/socket.scm 663 */
				obj_t BgL_auxz00_4000;
				obj_t BgL_auxz00_3993;

				if (KEYWORDP(BgL_optionz00_2472))
					{	/* Llib/socket.scm 663 */
						BgL_auxz00_4000 = BgL_optionz00_2472;
					}
				else
					{
						obj_t BgL_auxz00_4003;

						BgL_auxz00_4003 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1937z00zz__socketz00,
							BINT(30060L), BGl_string2041z00zz__socketz00,
							BGl_string2019z00zz__socketz00, BgL_optionz00_2472);
						FAILURE(BgL_auxz00_4003, BFALSE, BFALSE);
					}
				if (BGL_DATAGRAM_SOCKETP(BgL_socketz00_2471))
					{	/* Llib/socket.scm 663 */
						BgL_auxz00_3993 = BgL_socketz00_2471;
					}
				else
					{
						obj_t BgL_auxz00_3996;

						BgL_auxz00_3996 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1937z00zz__socketz00,
							BINT(30060L), BGl_string2041z00zz__socketz00,
							BGl_string2022z00zz__socketz00, BgL_socketz00_2471);
						FAILURE(BgL_auxz00_3996, BFALSE, BFALSE);
					}
				return
					BGl_datagramzd2socketzd2optionzd2setz12zc0zz__socketz00
					(BgL_auxz00_3993, BgL_auxz00_4000, BgL_valz00_2473);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__socketz00(void)
	{
		{	/* Llib/socket.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__socketz00(void)
	{
		{	/* Llib/socket.scm 14 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__socketz00(void)
	{
		{	/* Llib/socket.scm 14 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__socketz00(void)
	{
		{	/* Llib/socket.scm 14 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string2042z00zz__socketz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string2042z00zz__socketz00));
			return
				BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string2042z00zz__socketz00));
		}

	}

#ifdef __cplusplus
}
#endif
