/*===========================================================================*/
/*   (Llib/unicode.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Llib/unicode.scm -indent -o objs/obj_u/Llib/unicode.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___UNICODE_TYPE_DEFINITIONS
#define BGL___UNICODE_TYPE_DEFINITIONS
#endif													// BGL___UNICODE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t BGl_ucs2zd2stringzd2downcasez00zz__unicodez00(obj_t);
	static obj_t BGl__utf8zd2stringzf3z21zz__unicodez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_ucs2zd2stringzd2upcasez12z12zz__unicodez00(obj_t);
	BGL_EXPORTED_DECL int BGl_ucs2zd2stringzd2lengthz00zz__unicodez00(obj_t);
	static obj_t BGl_z62asciizd2stringzf3z43zz__unicodez00(obj_t, obj_t);
	extern obj_t c_ucs2_string_copy(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_subucs2zd2stringzd2urz00zz__unicodez00(obj_t, int,
		int);
	static obj_t BGl_z62utf8zd2stringzd2appendza2zc0zz__unicodez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_isozd2latinzd2ze3utf8ze3zz__unicodez00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_utf8zd2stringzd2encodez00zz__unicodez00(obj_t,
		bool_t, long, long);
	BGL_EXPORTED_DECL obj_t BGl_ucs2zd2stringzd2appendz00zz__unicodez00(obj_t);
	static obj_t BGl_symbol3626z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl_symbol3708z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl_z62isozd2latinzd2ze3utf8z81zz__unicodez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_listzd2ze3ucs2zd2stringze3zz__unicodez00(obj_t);
	static obj_t BGl_symbol3629z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl__makezd2ucs2zd2stringz00zz__unicodez00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__unicodez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_utf8zd2stringzd2minimalzd2charsetzd2zz__unicodez00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_ucs2zd2stringzd2ze3utf8zd2stringz31zz__unicodez00(obj_t);
	static obj_t BGl_z62ucs2zd2stringzd2cizc3zd3zf3z81zz__unicodez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL long BGl_utf8zd2stringzd2lengthz00zz__unicodez00(obj_t);
	static obj_t BGl_z62ucs2zd2stringzd2cizc3zf3z52zz__unicodez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_8bitszd2invzd2latinzd215zd2zz__unicodez00 = BUNSPEC;
	BGL_EXPORTED_DECL bool_t
		BGl_utf8zd2stringzd2leftzd2replacementzf3z21zz__unicodez00(obj_t, long,
		long);
	BGL_EXPORTED_DECL ucs2_t BGl_ucs2zd2stringzd2refzd2urzd2zz__unicodez00(obj_t,
		int);
	static obj_t BGl_makezd2tablezd2entryze70ze7zz__unicodez00(obj_t, long);
	static obj_t BGl_z62ucs2zd2stringze3zd3zf3z73zz__unicodez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol3712z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl_symbol3714z00zz__unicodez00 = BUNSPEC;
	static long BGl_8bitszd2ze3utf8zd2lengthze3zz__unicodez00(obj_t, long, obj_t);
	static long BGl_utf8zd2ze38bitszd2lengthze3zz__unicodez00(obj_t, long);
	BGL_EXPORTED_DECL obj_t BGl_utf8zd2stringzd2appendz00zz__unicodez00(obj_t,
		obj_t);
	static obj_t BGl_z62ucs2zd2stringzd2lengthz62zz__unicodez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_ucs2zd2stringzd2fillz12z12zz__unicodez00(obj_t,
		ucs2_t);
	extern obj_t ucs2_string_append(obj_t, obj_t);
	extern bool_t ucs2_string_cige(obj_t, obj_t);
	extern obj_t string_for_read(obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__unicodez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_z62ucs2zd2stringzd2cizd3zf3z42zz__unicodez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_ucs2zd2stringzc3zf3ze2zz__unicodez00(obj_t,
		obj_t);
	static obj_t BGl_z62utf8zd2charzd2siza7ezc5zz__unicodez00(obj_t, obj_t);
	static obj_t BGl_z62ucs2zd2stringzd2setz12z70zz__unicodez00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t ucs2_string_to_utf8_string(obj_t);
	extern bool_t ucs2_string_cigt(obj_t, obj_t);
	static obj_t BGl_utf8zd2collapsez12zc0zz__unicodez00(obj_t, long, obj_t,
		obj_t);
	static obj_t BGl_z62utf8zd2ze3isozd2latinz12z93zz__unicodez00(obj_t, obj_t);
	static obj_t BGl_z62ucs2zd2stringzd2appendz62zz__unicodez00(obj_t, obj_t);
	static obj_t BGl_z62subucs2zd2stringzb0zz__unicodez00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_utf8zd2stringzd2ze3ucs2zd2stringz31zz__unicodez00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_ucs2zd2stringzc3zd3zf3z31zz__unicodez00(obj_t,
		obj_t);
	extern obj_t BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(obj_t);
	static obj_t BGl_z62ucs2zd2stringzd2cize3zf3z72zz__unicodez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_ucs2zd2stringzd3zf3zf2zz__unicodez00(obj_t,
		obj_t);
	static obj_t BGl_z62utf8zd2stringzd2lengthz62zz__unicodez00(obj_t, obj_t);
	static obj_t BGl_z628bitszd2ze3utf8z53zz__unicodez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62utf8zd2ze38bitsz53zz__unicodez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62ucs2zd2stringzd2refzd2urzb0zz__unicodez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_inversezd2utf8zd2tablez00zz__unicodez00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_utf8zd2stringzf3z21zz__unicodez00(obj_t, bool_t);
	static obj_t BGl_toplevelzd2initzd2zz__unicodez00(void);
	extern obj_t BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(long,
		long);
	BGL_EXPORTED_DECL obj_t BGl_cp1252zd2ze3utf8z12z23zz__unicodez00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_utf8zd2ze3cp1252z12z23zz__unicodez00(obj_t);
	static obj_t BGl_list3549z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl_z62utf8zd2stringzd2appendz62zz__unicodez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_utf8zd2ze3isozd2latinzd215z12z23zz__unicodez00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_ucs2zd2stringze3zf3zc2zz__unicodez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_utf8zd2ze3isozd2latinze3zz__unicodez00(obj_t);
	static obj_t BGl_symbol3662z00zz__unicodez00 = BUNSPEC;
	extern obj_t utf8_string_to_ucs2_string(obj_t);
	static obj_t BGl_z62utf8zd2ze3isozd2latinz81zz__unicodez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_utf8zd2normaliza7ezd2utf16za7zz__unicodez00(obj_t,
		bool_t, long, long);
	static obj_t BGl_list3550z00zz__unicodez00 = BUNSPEC;
	extern obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_list3551z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl_list3552z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl_addzd2tablezd2entryze70ze7zz__unicodez00(obj_t, obj_t, long);
	BGL_EXPORTED_DECL obj_t BGl_ucs2zd2substringzd2zz__unicodez00(obj_t, int,
		int);
	static obj_t BGl_list3553z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl_list3554z00zz__unicodez00 = BUNSPEC;
	extern bool_t ucs2_strcmp(obj_t, obj_t);
	static obj_t BGl_list3555z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl_list3556z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl_z62ucs2zd2stringzd2copyz62zz__unicodez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_ucs2zd2stringzd2setzd2urz12zc0zz__unicodez00(obj_t, int, ucs2_t);
	static obj_t BGl_list3557z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl_list3558z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl_list3559z00zz__unicodez00 = BUNSPEC;
	BGL_EXPORTED_DECL bool_t
		BGl_ucs2zd2stringzd2cizc3zd3zf3ze3zz__unicodez00(obj_t, obj_t);
	extern obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__unicodez00(void);
	static obj_t BGl_z62ucs2zd2stringzd2cize3zd3zf3za1zz__unicodez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_genericzd2initzd2zz__unicodez00(void);
	extern obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	extern long bgl_list_length(obj_t);
	static obj_t
		BGl_z62utf8zd2stringzd2leftzd2replacementzf3z43zz__unicodez00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_symbol3671z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl_list3560z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl_list3561z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl_z62inversezd2utf8zd2tablez62zz__unicodez00(obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__unicodez00(void);
	static obj_t BGl_list3562z00zz__unicodez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_ucs2zd2stringzd2minimalzd2charsetzd2zz__unicodez00(obj_t);
	static obj_t BGl_list3563z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl_list3564z00zz__unicodez00 = BUNSPEC;
	extern ucs2_t ucs2_tolower(ucs2_t);
	static obj_t BGl_list3565z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl_gczd2rootszd2initz00zz__unicodez00(void);
	static obj_t BGl_list3566z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl_list3567z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl_list3568z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl_list3569z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl_objectzd2initzd2zz__unicodez00(void);
	BGL_EXPORTED_DECL obj_t BGl_ucs2zd2stringzd2zz__unicodez00(obj_t);
	static obj_t BGl_z62ucs2zd2stringzc3zf3z80zz__unicodez00(obj_t, obj_t, obj_t);
	static obj_t BGl__utf8zd2stringzd2encodez00zz__unicodez00(obj_t, obj_t);
	static obj_t BGl_list3570z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl_list3571z00zz__unicodez00 = BUNSPEC;
	extern bool_t ucs2_string_ge(obj_t, obj_t);
	static obj_t BGl_list3572z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl_list3573z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl_list3574z00zz__unicodez00 = BUNSPEC;
	extern obj_t c_subucs2_string(obj_t, int, int);
	extern bool_t ucs2_strcicmp(obj_t, obj_t);
	static obj_t BGl_list3575z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl_list3576z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl_list3577z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl_list3578z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl_list3579z00zz__unicodez00 = BUNSPEC;
	BGL_EXPORTED_DECL long
		BGl_stringzd2indexzd2ze3utf8zd2stringzd2indexze3zz__unicodez00(obj_t, long);
	BGL_EXPORTED_DECL bool_t BGl_ucs2zd2stringze3zd3zf3z11zz__unicodez00(obj_t,
		obj_t);
	extern bool_t ucs2_string_gt(obj_t, obj_t);
	static obj_t
		BGl_z62stringzd2indexzd2ze3utf8zd2stringzd2indexz81zz__unicodez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62ucs2zd2stringzd2ze3utf8zd2stringz53zz__unicodez00(obj_t,
		obj_t);
	extern obj_t bgl_reverse_bang(obj_t);
	extern bool_t ucs2_string_cile(obj_t, obj_t);
	extern obj_t string_append_3(obj_t, obj_t, obj_t);
	static obj_t BGl_z62ucs2zd2stringzd3zf3z90zz__unicodez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_ucs2zd2stringzd2downcasez12z12zz__unicodez00(obj_t);
	static obj_t BGl__utf8zd2stringzd2appendzd2fillz12zc0zz__unicodez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL long
		BGl_utf8zd2stringzd2appendzd2fillz12zc0zz__unicodez00(obj_t, long, obj_t,
		obj_t);
	static obj_t BGl_list3580z00zz__unicodez00 = BUNSPEC;
	extern bool_t ucs2_string_cilt(obj_t, obj_t);
	static obj_t BGl_list3581z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl_list3582z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl_list3583z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl_list3584z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl_list3585z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl_list3586z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl_list3587z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl__utf8zd2substringzd2zz__unicodez00(obj_t, obj_t);
	static obj_t BGl_list3589z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl_z62cp1252zd2ze3utf8z12z41zz__unicodez00(obj_t, obj_t);
	static obj_t BGl_z62utf8zd2ze3cp1252z12z41zz__unicodez00(obj_t, obj_t);
	static obj_t BGl_z62ucs2zd2stringzd2upcasez12z70zz__unicodez00(obj_t, obj_t);
	static obj_t BGl_cp1252zd2invzd2zz__unicodez00 = BUNSPEC;
	static obj_t
		BGl_z62utf8zd2stringzd2rightzd2replacementzf3z43zz__unicodez00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_utf8zd2stringzd2appendza2za2zz__unicodez00(obj_t);
	BGL_EXPORTED_DECL long BGl_utf8zd2charzd2siza7eza7zz__unicodez00(unsigned
		char);
	BGL_EXPORTED_DECL obj_t BGl_isozd2latinzd2ze3utf8z12zf1zz__unicodez00(obj_t);
	static obj_t BGl_z62ucs2zd2stringze3zf3za0zz__unicodez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62ucs2zd2substringzd2urz62zz__unicodez00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl__utf8zd2normaliza7ezd2utf16za7zz__unicodez00(obj_t, obj_t);
	static obj_t BGl_z62stringzd2minimalzd2charsetz62zz__unicodez00(obj_t, obj_t);
	static obj_t BGl_tablezd2ze38bitsze70zd6zz__unicodez00(obj_t, obj_t, obj_t,
		int, obj_t, obj_t, long, long);
	BGL_EXPORTED_DECL obj_t BGl_subucs2zd2stringzd2zz__unicodez00(obj_t, int,
		int);
	BGL_EXPORTED_DECL obj_t BGl_makezd2ucs2zd2stringz00zz__unicodez00(int,
		ucs2_t);
	static obj_t BGl_list3674z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl_8bitszd2ze3utf8zd2fillz12zf1zz__unicodez00(obj_t, obj_t,
		long, obj_t);
	static obj_t BGl_utf8zd2ze38bitszd2fillz12zf1zz__unicodez00(obj_t, obj_t, int,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_ucs2zd2stringzd2cize3zd3zf3zc3zz__unicodez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_utf8zd2ze3isozd2latinzd215z31zz__unicodez00(obj_t);
	extern ucs2_t ucs2_toupper(ucs2_t);
	static obj_t BGl_z62utf8zd2stringzd2minimalzd2charsetzb0zz__unicodez00(obj_t,
		obj_t);
	static obj_t BGl_z62utf8zd2stringzd2ze3ucs2zd2stringz53zz__unicodez00(obj_t,
		obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	extern obj_t c_substring(obj_t, long, long);
	BGL_EXPORTED_DECL ucs2_t BGl_ucs2zd2stringzd2refz00zz__unicodez00(obj_t, int);
	static obj_t BGl_z62ucs2zd2stringzd2ze3listz81zz__unicodez00(obj_t, obj_t);
	static obj_t BGl_z62ucs2zd2stringzd2refz62zz__unicodez00(obj_t, obj_t, obj_t);
	static obj_t BGl_errorzd2toozd2shortze70ze7zz__unicodez00(obj_t, int, obj_t);
	extern obj_t make_ucs2_string(int, ucs2_t);
	static obj_t BGl_methodzd2initzd2zz__unicodez00(void);
	static obj_t BGl_8bitszd2invzd2zz__unicodez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_ucs2zd2stringzd2upcasez00zz__unicodez00(obj_t);
	extern obj_t blit_string(obj_t, long, obj_t, long, long);
	BGL_EXPORTED_DECL bool_t BGl_ucs2zd2stringzd2cizc3zf3z30zz__unicodez00(obj_t,
		obj_t);
	static obj_t BGl_z62utf8zd2ze3isozd2latinzd215z53zz__unicodez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_8bitszd2ze3utf8z31zz__unicodez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_utf8zd2ze38bitsz31zz__unicodez00(obj_t, obj_t);
	static obj_t BGl_z628bitszd2ze3utf8z12z41zz__unicodez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62utf8zd2ze38bitsz12z41zz__unicodez00(obj_t, obj_t, obj_t);
	static obj_t BGl_loopze70ze7zz__unicodez00(long, bool_t, long, obj_t, obj_t,
		long, long);
	static obj_t BGl_loopze71ze7zz__unicodez00(long, long, obj_t, long);
	static obj_t BGl_z62ucs2zd2stringzd2downcasez12z70zz__unicodez00(obj_t,
		obj_t);
	static obj_t BGl_loopze72ze7zz__unicodez00(obj_t, obj_t);
	static obj_t BGl_loopze73ze7zz__unicodez00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_asciizd2stringzf3z21zz__unicodez00(obj_t);
	extern bool_t ucs2_string_le(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_ucs2zd2stringzd2cizd3zf3z20zz__unicodez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL long
		BGl_utf8zd2stringzd2indexzd2ze3stringzd2indexze3zz__unicodez00(obj_t, long);
	BGL_EXPORTED_DECL obj_t BGl_ucs2zd2stringzd2setz12z12zz__unicodez00(obj_t,
		int, ucs2_t);
	static obj_t BGl_z62utf8zd2ze3isozd2latinzd215z12z41zz__unicodez00(obj_t,
		obj_t);
	static obj_t
		BGl_z62utf8zd2stringzd2indexzd2ze3stringzd2indexz81zz__unicodez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_ucs2zd2substringzd2urz00zz__unicodez00(obj_t, int,
		int);
	extern bool_t ucs2_string_lt(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_utf8zd2ze3isozd2latinz12zf1zz__unicodez00(obj_t);
	static obj_t BGl_z62subucs2zd2stringzd2urz62zz__unicodez00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62ucs2zd2stringzf3z43zz__unicodez00(obj_t, obj_t);
	extern obj_t make_string(long, unsigned char);
	static obj_t BGl_z62listzd2ze3ucs2zd2stringz81zz__unicodez00(obj_t, obj_t);
	static obj_t BGl_z62cp1252zd2ze3utf8z53zz__unicodez00(obj_t, obj_t);
	static obj_t BGl_z62utf8zd2ze3cp1252z53zz__unicodez00(obj_t, obj_t);
	static obj_t BGl_z62ucs2zd2stringzd2upcasez62zz__unicodez00(obj_t, obj_t);
	static obj_t BGl_z62ucs2zd2stringzd2setzd2urz12za2zz__unicodez00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_utf8zd2stringzd2refz00zz__unicodez00(obj_t, long);
	BGL_EXPORTED_DECL bool_t BGl_ucs2zd2stringzd2cize3zf3z10zz__unicodez00(obj_t,
		obj_t);
	static obj_t BGl_z62utf8zd2stringzd2refz62zz__unicodez00(obj_t, obj_t, obj_t);
	static obj_t BGl_cp1252z00zz__unicodez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_8bitszd2ze3utf8z12z23zz__unicodez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_utf8zd2ze38bitsz12z23zz__unicodez00(obj_t, obj_t);
	static obj_t BGl_z62ucs2zd2stringzc3zd3zf3z53zz__unicodez00(obj_t, obj_t,
		obj_t);
	extern obj_t string_append(obj_t, obj_t);
	static obj_t BGl_z62ucs2zd2stringzd2downcasez62zz__unicodez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_ucs2zd2stringzd2ze3listze3zz__unicodez00(obj_t);
	static obj_t BGl_vector3588z00zz__unicodez00 = BUNSPEC;
	extern obj_t make_string_sans_fill(long);
	static obj_t BGl_z62ucs2zd2stringzd2minimalzd2charsetzb0zz__unicodez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_utf8zd2stringzd2rightzd2replacementzf3z21zz__unicodez00(obj_t, long,
		long);
	BGL_EXPORTED_DECL obj_t BGl_utf8zd2substringzd2zz__unicodez00(obj_t, long,
		long);
	static obj_t BGl_z62ucs2zd2stringzd2fillz12z70zz__unicodez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_ucs2zd2stringzf3z21zz__unicodez00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_stringzd2minimalzd2charsetz00zz__unicodez00(obj_t);
	static obj_t BGl_vector3673z00zz__unicodez00 = BUNSPEC;
	static obj_t BGl_z62ucs2zd2stringzb0zz__unicodez00(obj_t, obj_t);
	static obj_t BGl_z62isozd2latinzd2ze3utf8z12z93zz__unicodez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_ucs2zd2stringzd2copyz00zz__unicodez00(obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cp1252zd2ze3utf8z31zz__unicodez00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_utf8zd2ze3cp1252z31zz__unicodez00(obj_t);
	static obj_t BGl_z62ucs2zd2substringzb0zz__unicodez00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_listzd2ze3ucs2zd2stringzd2envz31zz__unicodez00,
		BgL_bgl_za762listza7d2za7e3ucs3718za7,
		BGl_z62listzd2ze3ucs2zd2stringz81zz__unicodez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ucs2zd2substringzd2envz00zz__unicodez00,
		BgL_bgl_za762ucs2za7d2substr3719z00,
		BGl_z62ucs2zd2substringzb0zz__unicodez00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string3650z00zz__unicodez00,
		BgL_bgl_string3650za700za7za7_3720za7, "&ucs2-string->list", 18);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_ucs2zd2stringzd2setzd2urz12zd2envz12zz__unicodez00,
		BgL_bgl_za762ucs2za7d2string3721z00,
		BGl_z62ucs2zd2stringzd2setzd2urz12za2zz__unicodez00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string3651z00zz__unicodez00,
		BgL_bgl_string3651za700za7za7_3722za7, "&ucs2-string-copy", 17);
	      DEFINE_STRING(BGl_string3652z00zz__unicodez00,
		BgL_bgl_string3652za700za7za7_3723za7, "&ucs2-string-fill!", 18);
	      DEFINE_STRING(BGl_string3653z00zz__unicodez00,
		BgL_bgl_string3653za700za7za7_3724za7, "&ucs2-string-upcase", 19);
	      DEFINE_STRING(BGl_string3654z00zz__unicodez00,
		BgL_bgl_string3654za700za7za7_3725za7, "&ucs2-string-downcase", 21);
	      DEFINE_STRING(BGl_string3655z00zz__unicodez00,
		BgL_bgl_string3655za700za7za7_3726za7, "&ucs2-string-upcase!", 20);
	      DEFINE_STRING(BGl_string3656z00zz__unicodez00,
		BgL_bgl_string3656za700za7za7_3727za7, "&ucs2-string-downcase!", 22);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_utf8zd2stringzd2refzd2envzd2zz__unicodez00,
		BgL_bgl_za762utf8za7d2string3728z00,
		BGl_z62utf8zd2stringzd2refz62zz__unicodez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string3657z00zz__unicodez00,
		BgL_bgl_string3657za700za7za7_3729za7, "&ucs2-string->utf8-string", 25);
	      DEFINE_STRING(BGl_string3658z00zz__unicodez00,
		BgL_bgl_string3658za700za7za7_3730za7, "&inverse-utf8-table", 19);
	      DEFINE_STRING(BGl_string3659z00zz__unicodez00,
		BgL_bgl_string3659za700za7za7_3731za7, "vector", 6);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_8bitszd2ze3utf8z12zd2envzf1zz__unicodez00,
		BgL_bgl_za7628bitsza7d2za7e3ut3732za7,
		BGl_z628bitszd2ze3utf8z12z41zz__unicodez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_utf8zd2ze38bitsz12zd2envzf1zz__unicodez00,
		BgL_bgl_za762utf8za7d2za7e38bi3733za7,
		BGl_z62utf8zd2ze38bitsz12z41zz__unicodez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string3660z00zz__unicodez00,
		BgL_bgl_string3660za700za7za7_3734za7, "&utf8-string->ucs2-string", 25);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_utf8zd2stringzd2appendzd2envzd2zz__unicodez00,
		BgL_bgl_za762utf8za7d2string3735z00,
		BGl_z62utf8zd2stringzd2appendz62zz__unicodez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string3661z00zz__unicodez00,
		BgL_bgl_string3661za700za7za7_3736za7, "bstring", 7);
	      DEFINE_STRING(BGl_string3663z00zz__unicodez00,
		BgL_bgl_string3663za700za7za7_3737za7, "ascii", 5);
	      DEFINE_STRING(BGl_string3664z00zz__unicodez00,
		BgL_bgl_string3664za700za7za7_3738za7, "&ascii-string?", 14);
	      DEFINE_STRING(BGl_string3665z00zz__unicodez00,
		BgL_bgl_string3665za700za7za7_3739za7, "_utf8-string?", 13);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_utf8zd2substringzd2envz00zz__unicodez00,
		BgL_bgl__utf8za7d2substrin3740za7, opt_generic_entry,
		BGl__utf8zd2substringzd2zz__unicodez00, BFALSE, -1);
	      DEFINE_STRING(BGl_string3666z00zz__unicodez00,
		BgL_bgl_string3666za700za7za7_3741za7, "_utf8-string-encode", 19);
	      DEFINE_STRING(BGl_string3667z00zz__unicodez00,
		BgL_bgl_string3667za700za7za7_3742za7, "utf8-string-encode", 18);
	      DEFINE_STRING(BGl_string3668z00zz__unicodez00,
		BgL_bgl_string3668za700za7za7_3743za7, "Illegal indexes", 15);
	      DEFINE_STRING(BGl_string3669z00zz__unicodez00,
		BgL_bgl_string3669za700za7za7_3744za7, "_utf8-normalize-utf16", 21);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_ucs2zd2stringzc3zf3zd2envz30zz__unicodez00,
		BgL_bgl_za762ucs2za7d2string3745z00,
		BGl_z62ucs2zd2stringzc3zf3z80zz__unicodez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_ucs2zd2stringzd2copyzd2envzd2zz__unicodez00,
		BgL_bgl_za762ucs2za7d2string3746z00,
		BGl_z62ucs2zd2stringzd2copyz62zz__unicodez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3670z00zz__unicodez00,
		BgL_bgl_string3670za700za7za7_3747za7, "utf8-normalize-utf16", 20);
	      DEFINE_STRING(BGl_string3590z00zz__unicodez00,
		BgL_bgl_string3590za700za7za7_3748za7, "€", 3);
	      DEFINE_STRING(BGl_string3672z00zz__unicodez00,
		BgL_bgl_string3672za700za7za7_3749za7, "utf8", 4);
	      DEFINE_STRING(BGl_string3591z00zz__unicodez00,
		BgL_bgl_string3591za700za7za7_3750za7, "", 0);
	      DEFINE_STRING(BGl_string3592z00zz__unicodez00,
		BgL_bgl_string3592za700za7za7_3751za7, "‚", 3);
	      DEFINE_STRING(BGl_string3593z00zz__unicodez00,
		BgL_bgl_string3593za700za7za7_3752za7, "ƒ", 2);
	      DEFINE_STRING(BGl_string3675z00zz__unicodez00,
		BgL_bgl_string3675za700za7za7_3753za7, "&utf8-char-size", 15);
	      DEFINE_STRING(BGl_string3594z00zz__unicodez00,
		BgL_bgl_string3594za700za7za7_3754za7, "„", 3);
	      DEFINE_STRING(BGl_string3676z00zz__unicodez00,
		BgL_bgl_string3676za700za7za7_3755za7, "bchar", 5);
	      DEFINE_STRING(BGl_string3595z00zz__unicodez00,
		BgL_bgl_string3595za700za7za7_3756za7, "…", 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2indexzd2ze3utf8zd2stringzd2indexzd2envz31zz__unicodez00,
		BgL_bgl_za762stringza7d2inde3757z00,
		BGl_z62stringzd2indexzd2ze3utf8zd2stringzd2indexz81zz__unicodez00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string3677z00zz__unicodez00,
		BgL_bgl_string3677za700za7za7_3758za7, "&utf8-string-length", 19);
	      DEFINE_STRING(BGl_string3596z00zz__unicodez00,
		BgL_bgl_string3596za700za7za7_3759za7, "†", 3);
	      DEFINE_STRING(BGl_string3678z00zz__unicodez00,
		BgL_bgl_string3678za700za7za7_3760za7, "&utf8-string-ref", 16);
	      DEFINE_STRING(BGl_string3597z00zz__unicodez00,
		BgL_bgl_string3597za700za7za7_3761za7, "‡", 3);
	      DEFINE_STRING(BGl_string3679z00zz__unicodez00,
		BgL_bgl_string3679za700za7za7_3762za7, "&utf8-string-index->string-index",
		32);
	      DEFINE_STRING(BGl_string3598z00zz__unicodez00,
		BgL_bgl_string3598za700za7za7_3763za7, "ˆ", 2);
	      DEFINE_STRING(BGl_string3599z00zz__unicodez00,
		BgL_bgl_string3599za700za7za7_3764za7, "‰", 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_ucs2zd2stringze3zd3zf3zd2envzc3zz__unicodez00,
		BgL_bgl_za762ucs2za7d2string3765z00,
		BGl_z62ucs2zd2stringze3zd3zf3z73zz__unicodez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_ucs2zd2stringzd2downcasez12zd2envzc0zz__unicodez00,
		BgL_bgl_za762ucs2za7d2string3766z00,
		BGl_z62ucs2zd2stringzd2downcasez12z70zz__unicodez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cp1252zd2ze3utf8z12zd2envzf1zz__unicodez00,
		BgL_bgl_za762cp1252za7d2za7e3u3767za7,
		BGl_z62cp1252zd2ze3utf8z12z41zz__unicodez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_utf8zd2ze3cp1252z12zd2envzf1zz__unicodez00,
		BgL_bgl_za762utf8za7d2za7e3cp13768za7,
		BGl_z62utf8zd2ze3cp1252z12z41zz__unicodez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3680z00zz__unicodez00,
		BgL_bgl_string3680za700za7za7_3769za7, "&string-index->utf8-string-index",
		32);
	      DEFINE_STRING(BGl_string3681z00zz__unicodez00,
		BgL_bgl_string3681za700za7za7_3770za7, "&utf8-string-left-replacement?",
		30);
	      DEFINE_STRING(BGl_string3682z00zz__unicodez00,
		BgL_bgl_string3682za700za7za7_3771za7, "&utf8-string-right-replacement?",
		31);
	      DEFINE_STRING(BGl_string3683z00zz__unicodez00,
		BgL_bgl_string3683za700za7za7_3772za7, "_utf8-string-append-fill!", 25);
	      DEFINE_STRING(BGl_string3684z00zz__unicodez00,
		BgL_bgl_string3684za700za7za7_3773za7, "&utf8-string-append", 19);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_ucs2zd2stringzd2cizd3zf3zd2envzf2zz__unicodez00,
		BgL_bgl_za762ucs2za7d2string3774z00,
		BGl_z62ucs2zd2stringzd2cizd3zf3z42zz__unicodez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string3685z00zz__unicodez00,
		BgL_bgl_string3685za700za7za7_3775za7, "_utf8-substring", 15);
	      DEFINE_STRING(BGl_string3686z00zz__unicodez00,
		BgL_bgl_string3686za700za7za7_3776za7, "utf8-substring", 14);
	      DEFINE_STRING(BGl_string3687z00zz__unicodez00,
		BgL_bgl_string3687za700za7za7_3777za7, "Illegal start index \"", 21);
	      DEFINE_STRING(BGl_string3688z00zz__unicodez00,
		BgL_bgl_string3688za700za7za7_3778za7, "\"", 1);
	      DEFINE_STRING(BGl_string3689z00zz__unicodez00,
		BgL_bgl_string3689za700za7za7_3779za7, "Illegal end index \"", 19);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isozd2latinzd2ze3utf8z12zd2envz23zz__unicodez00,
		BgL_bgl_za762isoza7d2latinza7d3780za7,
		BGl_z62isozd2latinzd2ze3utf8z12z93zz__unicodez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_utf8zd2normaliza7ezd2utf16zd2envz75zz__unicodez00,
		BgL_bgl__utf8za7d2normaliza73781z00, opt_generic_entry,
		BGl__utf8zd2normaliza7ezd2utf16za7zz__unicodez00, BFALSE, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_subucs2zd2stringzd2envz00zz__unicodez00,
		BgL_bgl_za762subucs2za7d2str3782z00,
		BGl_z62subucs2zd2stringzb0zz__unicodez00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string3690z00zz__unicodez00,
		BgL_bgl_string3690za700za7za7_3783za7,
		"Illegal character \"~x\" at index ~a", 34);
	      DEFINE_STRING(BGl_string3691z00zz__unicodez00,
		BgL_bgl_string3691za700za7za7_3784za7, "utf8->8bits", 11);
	      DEFINE_STRING(BGl_string3692z00zz__unicodez00,
		BgL_bgl_string3692za700za7za7_3785za7, "String too short", 16);
	      DEFINE_STRING(BGl_string3693z00zz__unicodez00,
		BgL_bgl_string3693za700za7za7_3786za7, "Cannot encode at index ", 23);
	      DEFINE_STRING(BGl_string3694z00zz__unicodez00,
		BgL_bgl_string3694za700za7za7_3787za7, "&utf8->8bits", 12);
	      DEFINE_STRING(BGl_string3695z00zz__unicodez00,
		BgL_bgl_string3695za700za7za7_3788za7, "&utf8->8bits!", 13);
	      DEFINE_STRING(BGl_string3696z00zz__unicodez00,
		BgL_bgl_string3696za700za7za7_3789za7, "&utf8->iso-latin", 16);
	      DEFINE_STRING(BGl_string3697z00zz__unicodez00,
		BgL_bgl_string3697za700za7za7_3790za7, "&utf8->iso-latin!", 17);
	      DEFINE_STRING(BGl_string3698z00zz__unicodez00,
		BgL_bgl_string3698za700za7za7_3791za7, "&utf8->iso-latin-15", 19);
	      DEFINE_STRING(BGl_string3699z00zz__unicodez00,
		BgL_bgl_string3699za700za7za7_3792za7, "&utf8->iso-latin-15!", 20);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_ucs2zd2stringzd2refzd2envzd2zz__unicodez00,
		BgL_bgl_za762ucs2za7d2string3793z00,
		BGl_z62ucs2zd2stringzd2refz62zz__unicodez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_ucs2zd2stringzd2minimalzd2charsetzd2envz00zz__unicodez00,
		BgL_bgl_za762ucs2za7d2string3794z00,
		BGl_z62ucs2zd2stringzd2minimalzd2charsetzb0zz__unicodez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_utf8zd2stringzd2rightzd2replacementzf3zd2envzf3zz__unicodez00,
		BgL_bgl_za762utf8za7d2string3795z00,
		BGl_z62utf8zd2stringzd2rightzd2replacementzf3z43zz__unicodez00, 0L, BUNSPEC,
		3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2minimalzd2charsetzd2envzd2zz__unicodez00,
		BgL_bgl_za762stringza7d2mini3796z00,
		BGl_z62stringzd2minimalzd2charsetz62zz__unicodez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_ucs2zd2stringzd2upcasezd2envzd2zz__unicodez00,
		BgL_bgl_za762ucs2za7d2string3797z00,
		BGl_z62ucs2zd2stringzd2upcasez62zz__unicodez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_ucs2zd2stringzc3zd3zf3zd2envze3zz__unicodez00,
		BgL_bgl_za762ucs2za7d2string3798z00,
		BGl_z62ucs2zd2stringzc3zd3zf3z53zz__unicodez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_ucs2zd2stringzd3zf3zd2envz20zz__unicodez00,
		BgL_bgl_za762ucs2za7d2string3799z00,
		BGl_z62ucs2zd2stringzd3zf3z90zz__unicodez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_ucs2zd2stringzd2downcasezd2envzd2zz__unicodez00,
		BgL_bgl_za762ucs2za7d2string3800z00,
		BGl_z62ucs2zd2stringzd2downcasez62zz__unicodez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_inversezd2utf8zd2tablezd2envzd2zz__unicodez00,
		BgL_bgl_za762inverseza7d2utf3801z00,
		BGl_z62inversezd2utf8zd2tablez62zz__unicodez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_utf8zd2stringzd2indexzd2ze3stringzd2indexzd2envz31zz__unicodez00,
		BgL_bgl_za762utf8za7d2string3802z00,
		BGl_z62utf8zd2stringzd2indexzd2ze3stringzd2indexz81zz__unicodez00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_ucs2zd2stringzd2setz12zd2envzc0zz__unicodez00,
		BgL_bgl_za762ucs2za7d2string3803z00,
		BGl_z62ucs2zd2stringzd2setz12z70zz__unicodez00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_ucs2zd2substringzd2urzd2envzd2zz__unicodez00,
		BgL_bgl_za762ucs2za7d2substr3804z00,
		BGl_z62ucs2zd2substringzd2urz62zz__unicodez00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isozd2latinzd2ze3utf8zd2envz31zz__unicodez00,
		BgL_bgl_za762isoza7d2latinza7d3805za7,
		BGl_z62isozd2latinzd2ze3utf8z81zz__unicodez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_utf8zd2ze3isozd2latinz12zd2envz23zz__unicodez00,
		BgL_bgl_za762utf8za7d2za7e3iso3806za7,
		BGl_z62utf8zd2ze3isozd2latinz12z93zz__unicodez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_utf8zd2stringzd2encodezd2envzd2zz__unicodez00,
		BgL_bgl__utf8za7d2stringza7d3807z00, opt_generic_entry,
		BGl__utf8zd2stringzd2encodez00zz__unicodez00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_ucs2zd2stringzd2appendzd2envzd2zz__unicodez00,
		BgL_bgl_za762ucs2za7d2string3808z00, va_generic_entry,
		BGl_z62ucs2zd2stringzd2appendz62zz__unicodez00, BUNSPEC, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2ucs2zd2stringzd2envzd2zz__unicodez00,
		BgL_bgl__makeza7d2ucs2za7d2s3809z00, opt_generic_entry,
		BGl__makezd2ucs2zd2stringz00zz__unicodez00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_ucs2zd2stringzd2cize3zd3zf3zd2envz11zz__unicodez00,
		BgL_bgl_za762ucs2za7d2string3810z00,
		BGl_z62ucs2zd2stringzd2cize3zd3zf3za1zz__unicodez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_utf8zd2stringzd2minimalzd2charsetzd2envz00zz__unicodez00,
		BgL_bgl_za762utf8za7d2string3811z00,
		BGl_z62utf8zd2stringzd2minimalzd2charsetzb0zz__unicodez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_ucs2zd2stringzd2cize3zf3zd2envzc2zz__unicodez00,
		BgL_bgl_za762ucs2za7d2string3812z00,
		BGl_z62ucs2zd2stringzd2cize3zf3z72zz__unicodez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_utf8zd2stringzd2lengthzd2envzd2zz__unicodez00,
		BgL_bgl_za762utf8za7d2string3813z00,
		BGl_z62utf8zd2stringzd2lengthz62zz__unicodez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_ucs2zd2stringzd2fillz12zd2envzc0zz__unicodez00,
		BgL_bgl_za762ucs2za7d2string3814z00,
		BGl_z62ucs2zd2stringzd2fillz12z70zz__unicodez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ucs2zd2stringzd2envz00zz__unicodez00,
		BgL_bgl_za762ucs2za7d2string3815z00, va_generic_entry,
		BGl_z62ucs2zd2stringzb0zz__unicodez00, BUNSPEC, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cp1252zd2ze3utf8zd2envze3zz__unicodez00,
		BgL_bgl_za762cp1252za7d2za7e3u3816za7,
		BGl_z62cp1252zd2ze3utf8z53zz__unicodez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_utf8zd2ze3cp1252zd2envze3zz__unicodez00,
		BgL_bgl_za762utf8za7d2za7e3cp13817za7,
		BGl_z62utf8zd2ze3cp1252z53zz__unicodez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_8bitszd2ze3utf8zd2envze3zz__unicodez00,
		BgL_bgl_za7628bitsza7d2za7e3ut3818za7,
		BGl_z628bitszd2ze3utf8z53zz__unicodez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_utf8zd2ze38bitszd2envze3zz__unicodez00,
		BgL_bgl_za762utf8za7d2za7e38bi3819za7,
		BGl_z62utf8zd2ze38bitsz53zz__unicodez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_utf8zd2stringzd2appendzd2fillz12zd2envz12zz__unicodez00,
		BgL_bgl__utf8za7d2stringza7d3820z00, opt_generic_entry,
		BGl__utf8zd2stringzd2appendzd2fillz12zc0zz__unicodez00, BFALSE, -1);
	      DEFINE_STRING(BGl_string3600z00zz__unicodez00,
		BgL_bgl_string3600za700za7za7_3821za7, "Š", 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_utf8zd2stringzf3zd2envzf3zz__unicodez00,
		BgL_bgl__utf8za7d2stringza7f3822z00, opt_generic_entry,
		BGl__utf8zd2stringzf3z21zz__unicodez00, BFALSE, -1);
	      DEFINE_STRING(BGl_string3601z00zz__unicodez00,
		BgL_bgl_string3601za700za7za7_3823za7, "‹", 3);
	      DEFINE_STRING(BGl_string3602z00zz__unicodez00,
		BgL_bgl_string3602za700za7za7_3824za7, "Œ", 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_asciizd2stringzf3zd2envzf3zz__unicodez00,
		BgL_bgl_za762asciiza7d2strin3825z00,
		BGl_z62asciizd2stringzf3z43zz__unicodez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3603z00zz__unicodez00,
		BgL_bgl_string3603za700za7za7_3826za7, "Ž", 2);
	      DEFINE_STRING(BGl_string3604z00zz__unicodez00,
		BgL_bgl_string3604za700za7za7_3827za7, "‘", 3);
	      DEFINE_STRING(BGl_string3605z00zz__unicodez00,
		BgL_bgl_string3605za700za7za7_3828za7, "’", 3);
	      DEFINE_STRING(BGl_string3606z00zz__unicodez00,
		BgL_bgl_string3606za700za7za7_3829za7, "“", 3);
	      DEFINE_STRING(BGl_string3607z00zz__unicodez00,
		BgL_bgl_string3607za700za7za7_3830za7, "”", 3);
	      DEFINE_STRING(BGl_string3608z00zz__unicodez00,
		BgL_bgl_string3608za700za7za7_3831za7, "•", 3);
	      DEFINE_STRING(BGl_string3609z00zz__unicodez00,
		BgL_bgl_string3609za700za7za7_3832za7, "–", 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_utf8zd2ze3isozd2latinzd215z12zd2envzf1zz__unicodez00,
		BgL_bgl_za762utf8za7d2za7e3iso3833za7,
		BGl_z62utf8zd2ze3isozd2latinzd215z12z41zz__unicodez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_ucs2zd2stringze3zf3zd2envz10zz__unicodez00,
		BgL_bgl_za762ucs2za7d2string3834z00,
		BGl_z62ucs2zd2stringze3zf3za0zz__unicodez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_subucs2zd2stringzd2urzd2envzd2zz__unicodez00,
		BgL_bgl_za762subucs2za7d2str3835z00,
		BGl_z62subucs2zd2stringzd2urz62zz__unicodez00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_utf8zd2ze3isozd2latinzd2envz31zz__unicodez00,
		BgL_bgl_za762utf8za7d2za7e3iso3836za7,
		BGl_z62utf8zd2ze3isozd2latinz81zz__unicodez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3610z00zz__unicodez00,
		BgL_bgl_string3610za700za7za7_3837za7, "—", 3);
	      DEFINE_STRING(BGl_string3611z00zz__unicodez00,
		BgL_bgl_string3611za700za7za7_3838za7, "˜", 2);
	      DEFINE_STRING(BGl_string3612z00zz__unicodez00,
		BgL_bgl_string3612za700za7za7_3839za7, "™", 3);
	      DEFINE_STRING(BGl_string3613z00zz__unicodez00,
		BgL_bgl_string3613za700za7za7_3840za7, "š", 2);
	      DEFINE_STRING(BGl_string3614z00zz__unicodez00,
		BgL_bgl_string3614za700za7za7_3841za7, "›", 3);
	      DEFINE_STRING(BGl_string3615z00zz__unicodez00,
		BgL_bgl_string3615za700za7za7_3842za7, "œ", 2);
	      DEFINE_STRING(BGl_string3616z00zz__unicodez00,
		BgL_bgl_string3616za700za7za7_3843za7, "ž", 2);
	      DEFINE_STRING(BGl_string3617z00zz__unicodez00,
		BgL_bgl_string3617za700za7za7_3844za7, "Ÿ", 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_ucs2zd2stringzd2cizc3zd3zf3zd2envz31zz__unicodez00,
		BgL_bgl_za762ucs2za7d2string3845z00,
		BGl_z62ucs2zd2stringzd2cizc3zd3zf3z81zz__unicodez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string3618z00zz__unicodez00,
		BgL_bgl_string3618za700za7za7_3846za7,
		"/tmp/bigloo/runtime/Llib/unicode.scm", 36);
	      DEFINE_STRING(BGl_string3619z00zz__unicodez00,
		BgL_bgl_string3619za700za7za7_3847za7, "_make-ucs2-string", 17);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_utf8zd2ze3isozd2latinzd215zd2envze3zz__unicodez00,
		BgL_bgl_za762utf8za7d2za7e3iso3848za7,
		BGl_z62utf8zd2ze3isozd2latinzd215z53zz__unicodez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_ucs2zd2stringzd2ze3utf8zd2stringzd2envze3zz__unicodez00,
		BgL_bgl_za762ucs2za7d2string3849z00,
		BGl_z62ucs2zd2stringzd2ze3utf8zd2stringz53zz__unicodez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_utf8zd2stringzd2leftzd2replacementzf3zd2envzf3zz__unicodez00,
		BgL_bgl_za762utf8za7d2string3850z00,
		BGl_z62utf8zd2stringzd2leftzd2replacementzf3z43zz__unicodez00, 0L, BUNSPEC,
		3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_ucs2zd2stringzd2refzd2urzd2envz00zz__unicodez00,
		BgL_bgl_za762ucs2za7d2string3851z00,
		BGl_z62ucs2zd2stringzd2refzd2urzb0zz__unicodez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_ucs2zd2stringzd2ze3listzd2envz31zz__unicodez00,
		BgL_bgl_za762ucs2za7d2string3852z00,
		BGl_z62ucs2zd2stringzd2ze3listz81zz__unicodez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3700z00zz__unicodez00,
		BgL_bgl_string3700za700za7za7_3853za7, "&utf8->cp1252", 13);
	      DEFINE_STRING(BGl_string3701z00zz__unicodez00,
		BgL_bgl_string3701za700za7za7_3854za7, "&utf8->cp1252!", 14);
	      DEFINE_STRING(BGl_string3620z00zz__unicodez00,
		BgL_bgl_string3620za700za7za7_3855za7, "bint", 4);
	      DEFINE_STRING(BGl_string3702z00zz__unicodez00,
		BgL_bgl_string3702za700za7za7_3856za7, "&8bits->utf8", 12);
	      DEFINE_STRING(BGl_string3621z00zz__unicodez00,
		BgL_bgl_string3621za700za7za7_3857za7, "bucs2", 5);
	      DEFINE_STRING(BGl_string3703z00zz__unicodez00,
		BgL_bgl_string3703za700za7za7_3858za7, "&8bits->utf8!", 13);
	      DEFINE_STRING(BGl_string3622z00zz__unicodez00,
		BgL_bgl_string3622za700za7za7_3859za7, "&ucs2-string-length", 19);
	      DEFINE_STRING(BGl_string3704z00zz__unicodez00,
		BgL_bgl_string3704za700za7za7_3860za7, "&iso-latin->utf8", 16);
	      DEFINE_STRING(BGl_string3623z00zz__unicodez00,
		BgL_bgl_string3623za700za7za7_3861za7, "ucs2string", 10);
	      DEFINE_STRING(BGl_string3705z00zz__unicodez00,
		BgL_bgl_string3705za700za7za7_3862za7, "&iso-latin->utf8!", 17);
	      DEFINE_STRING(BGl_string3624z00zz__unicodez00,
		BgL_bgl_string3624za700za7za7_3863za7, "index out of range [0..", 23);
	      DEFINE_STRING(BGl_string3706z00zz__unicodez00,
		BgL_bgl_string3706za700za7za7_3864za7, "&cp1252->utf8", 13);
	      DEFINE_STRING(BGl_string3625z00zz__unicodez00,
		BgL_bgl_string3625za700za7za7_3865za7, "]", 1);
	      DEFINE_STRING(BGl_string3707z00zz__unicodez00,
		BgL_bgl_string3707za700za7za7_3866za7, "&cp1252->utf8!", 14);
	      DEFINE_STRING(BGl_string3627z00zz__unicodez00,
		BgL_bgl_string3627za700za7za7_3867za7, "ucs2-string-ref", 15);
	      DEFINE_STRING(BGl_string3709z00zz__unicodez00,
		BgL_bgl_string3709za700za7za7_3868za7, "latin1", 6);
	      DEFINE_STRING(BGl_string3628z00zz__unicodez00,
		BgL_bgl_string3628za700za7za7_3869za7, "&ucs2-string-ref", 16);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ucs2zd2stringzf3zd2envzf3zz__unicodez00,
		BgL_bgl_za762ucs2za7d2string3870z00,
		BGl_z62ucs2zd2stringzf3z43zz__unicodez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3710z00zz__unicodez00,
		BgL_bgl_string3710za700za7za7_3871za7, "&string-minimal-charset", 23);
	      DEFINE_STRING(BGl_string3711z00zz__unicodez00,
		BgL_bgl_string3711za700za7za7_3872za7, "&utf8-string-minimal-charset", 28);
	      DEFINE_STRING(BGl_string3630z00zz__unicodez00,
		BgL_bgl_string3630za700za7za7_3873za7, "ucs2-string-set!", 16);
	      DEFINE_STRING(BGl_string3631z00zz__unicodez00,
		BgL_bgl_string3631za700za7za7_3874za7, "&ucs2-string-set!", 17);
	      DEFINE_STRING(BGl_string3713z00zz__unicodez00,
		BgL_bgl_string3713za700za7za7_3875za7, "ucs2", 4);
	      DEFINE_STRING(BGl_string3632z00zz__unicodez00,
		BgL_bgl_string3632za700za7za7_3876za7, "&ucs2-string-ref-ur", 19);
	      DEFINE_STRING(BGl_string3633z00zz__unicodez00,
		BgL_bgl_string3633za700za7za7_3877za7, "&ucs2-string-set-ur!", 20);
	      DEFINE_STRING(BGl_string3715z00zz__unicodez00,
		BgL_bgl_string3715za700za7za7_3878za7, "utf16", 5);
	      DEFINE_STRING(BGl_string3634z00zz__unicodez00,
		BgL_bgl_string3634za700za7za7_3879za7, "&ucs2-string=?", 14);
	      DEFINE_STRING(BGl_string3716z00zz__unicodez00,
		BgL_bgl_string3716za700za7za7_3880za7, "&ucs2-string-minimal-charset", 28);
	      DEFINE_STRING(BGl_string3635z00zz__unicodez00,
		BgL_bgl_string3635za700za7za7_3881za7, "&ucs2-string-ci=?", 17);
	      DEFINE_STRING(BGl_string3717z00zz__unicodez00,
		BgL_bgl_string3717za700za7za7_3882za7, "__unicode", 9);
	      DEFINE_STRING(BGl_string3636z00zz__unicodez00,
		BgL_bgl_string3636za700za7za7_3883za7, "&ucs2-string<?", 14);
	      DEFINE_STRING(BGl_string3637z00zz__unicodez00,
		BgL_bgl_string3637za700za7za7_3884za7, "&ucs2-string>?", 14);
	      DEFINE_STRING(BGl_string3638z00zz__unicodez00,
		BgL_bgl_string3638za700za7za7_3885za7, "&ucs2-string<=?", 15);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_ucs2zd2stringzd2cizc3zf3zd2envze2zz__unicodez00,
		BgL_bgl_za762ucs2za7d2string3886z00,
		BGl_z62ucs2zd2stringzd2cizc3zf3z52zz__unicodez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string3639z00zz__unicodez00,
		BgL_bgl_string3639za700za7za7_3887za7, "&ucs2-string>=?", 15);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_utf8zd2stringzd2ze3ucs2zd2stringzd2envze3zz__unicodez00,
		BgL_bgl_za762utf8za7d2string3888z00,
		BGl_z62utf8zd2stringzd2ze3ucs2zd2stringz53zz__unicodez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3640z00zz__unicodez00,
		BgL_bgl_string3640za700za7za7_3889za7, "&ucs2-string-ci<?", 17);
	      DEFINE_STRING(BGl_string3641z00zz__unicodez00,
		BgL_bgl_string3641za700za7za7_3890za7, "&ucs2-string-ci>?", 17);
	      DEFINE_STRING(BGl_string3642z00zz__unicodez00,
		BgL_bgl_string3642za700za7za7_3891za7, "&ucs2-string-ci<=?", 18);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_ucs2zd2stringzd2upcasez12zd2envzc0zz__unicodez00,
		BgL_bgl_za762ucs2za7d2string3892z00,
		BGl_z62ucs2zd2stringzd2upcasez12z70zz__unicodez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_ucs2zd2stringzd2lengthzd2envzd2zz__unicodez00,
		BgL_bgl_za762ucs2za7d2string3893z00,
		BGl_z62ucs2zd2stringzd2lengthz62zz__unicodez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3643z00zz__unicodez00,
		BgL_bgl_string3643za700za7za7_3894za7, "&ucs2-string-ci>=?", 18);
	      DEFINE_STRING(BGl_string3644z00zz__unicodez00,
		BgL_bgl_string3644za700za7za7_3895za7, "subucs2-string", 14);
	      DEFINE_STRING(BGl_string3645z00zz__unicodez00,
		BgL_bgl_string3645za700za7za7_3896za7, "Illegal index", 13);
	      DEFINE_STRING(BGl_string3646z00zz__unicodez00,
		BgL_bgl_string3646za700za7za7_3897za7, "&subucs2-string", 15);
	      DEFINE_STRING(BGl_string3647z00zz__unicodez00,
		BgL_bgl_string3647za700za7za7_3898za7, "&subucs2-string-ur", 18);
	      DEFINE_STRING(BGl_string3648z00zz__unicodez00,
		BgL_bgl_string3648za700za7za7_3899za7, "&ucs2-substring", 15);
	      DEFINE_STRING(BGl_string3649z00zz__unicodez00,
		BgL_bgl_string3649za700za7za7_3900za7, "&ucs2-substring-ur", 18);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_utf8zd2stringzd2appendza2zd2envz70zz__unicodez00,
		BgL_bgl_za762utf8za7d2string3901z00, va_generic_entry,
		BGl_z62utf8zd2stringzd2appendza2zc0zz__unicodez00, BUNSPEC, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_utf8zd2charzd2siza7ezd2envz75zz__unicodez00,
		BgL_bgl_za762utf8za7d2charza7d3902za7,
		BGl_z62utf8zd2charzd2siza7ezc5zz__unicodez00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_symbol3626z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_symbol3708z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_symbol3629z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_8bitszd2invzd2latinzd215zd2zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_symbol3712z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_symbol3714z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3549z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_symbol3662z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3550z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3551z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3552z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3553z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3554z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3555z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3556z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3557z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3558z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3559z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_symbol3671z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3560z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3561z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3562z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3563z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3564z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3565z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3566z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3567z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3568z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3569z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3570z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3571z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3572z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3573z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3574z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3575z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3576z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3577z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3578z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3579z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3580z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3581z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3582z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3583z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3584z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3585z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3586z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3587z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3589z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_cp1252zd2invzd2zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_list3674z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_8bitszd2invzd2zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_cp1252z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_vector3588z00zz__unicodez00));
		     ADD_ROOT((void *) (&BGl_vector3673z00zz__unicodez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__unicodez00(long
		BgL_checksumz00_7269, char *BgL_fromz00_7270)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__unicodez00))
				{
					BGl_requirezd2initializa7ationz75zz__unicodez00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__unicodez00();
					BGl_cnstzd2initzd2zz__unicodez00();
					BGl_importedzd2moduleszd2initz00zz__unicodez00();
					return BGl_toplevelzd2initzd2zz__unicodez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__unicodez00(void)
	{
		{	/* Llib/unicode.scm 14 */
			BGl_list3552z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(144L), BCHAR(((unsigned char) '-')));
			BGl_list3553z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(145L), BCHAR(((unsigned char) '-')));
			BGl_list3554z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(146L), BCHAR(((unsigned char) '-')));
			BGl_list3555z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(147L), BCHAR(((unsigned char) '-')));
			BGl_list3556z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(148L), BCHAR(((unsigned char) '-')));
			BGl_list3557z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(149L), BCHAR(((unsigned char) '-')));
			BGl_list3558z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(152L), BCHAR(((unsigned char) '`')));
			BGl_list3559z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(153L), BCHAR(((unsigned char) '\'')));
			BGl_list3560z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(154L), BCHAR(((unsigned char) ',')));
			BGl_list3561z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(155L), BCHAR(((unsigned char) '`')));
			BGl_list3562z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(164L), BCHAR(((unsigned char) '.')));
			BGl_list3563z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(167L), BCHAR(((unsigned char) '.')));
			BGl_list3564z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(178L), BCHAR(((unsigned char) '\'')));
			BGl_list3565z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(179L), BCHAR(((unsigned char) '"')));
			BGl_list3566z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(181L), BCHAR(((unsigned char) '`')));
			BGl_list3567z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(187L), BCHAR(((unsigned char) '"')));
			BGl_list3568z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(184L), BCHAR(((unsigned char) '^')));
			BGl_list3569z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(185L), BCHAR(((unsigned char) '<')));
			BGl_list3570z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(186L), BCHAR(((unsigned char) '>')));
			BGl_list3551z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(128L),
				MAKE_YOUNG_PAIR(BGl_list3552z00zz__unicodez00,
					MAKE_YOUNG_PAIR(BGl_list3553z00zz__unicodez00,
						MAKE_YOUNG_PAIR(BGl_list3554z00zz__unicodez00,
							MAKE_YOUNG_PAIR(BGl_list3555z00zz__unicodez00,
								MAKE_YOUNG_PAIR(BGl_list3556z00zz__unicodez00,
									MAKE_YOUNG_PAIR(BGl_list3557z00zz__unicodez00,
										MAKE_YOUNG_PAIR(BGl_list3558z00zz__unicodez00,
											MAKE_YOUNG_PAIR(BGl_list3559z00zz__unicodez00,
												MAKE_YOUNG_PAIR(BGl_list3560z00zz__unicodez00,
													MAKE_YOUNG_PAIR(BGl_list3561z00zz__unicodez00,
														MAKE_YOUNG_PAIR(BGl_list3562z00zz__unicodez00,
															MAKE_YOUNG_PAIR(BGl_list3563z00zz__unicodez00,
																MAKE_YOUNG_PAIR(BGl_list3564z00zz__unicodez00,
																	MAKE_YOUNG_PAIR(BGl_list3565z00zz__unicodez00,
																		MAKE_YOUNG_PAIR
																		(BGl_list3566z00zz__unicodez00,
																			MAKE_YOUNG_PAIR
																			(BGl_list3567z00zz__unicodez00,
																				MAKE_YOUNG_PAIR
																				(BGl_list3568z00zz__unicodez00,
																					MAKE_YOUNG_PAIR
																					(BGl_list3569z00zz__unicodez00,
																						MAKE_YOUNG_PAIR
																						(BGl_list3570z00zz__unicodez00,
																							BNIL))))))))))))))))))));
			BGl_list3572z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(131L), BCHAR(((unsigned char) '*')));
			BGl_list3571z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(129L),
				MAKE_YOUNG_PAIR(BGl_list3572z00zz__unicodez00, BNIL));
			BGl_list3574z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(141L), BCHAR(((unsigned char) '(')));
			BGl_list3575z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(142L), BCHAR(((unsigned char) ')')));
			BGl_list3573z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(130L),
				MAKE_YOUNG_PAIR(BGl_list3574z00zz__unicodez00,
					MAKE_YOUNG_PAIR(BGl_list3575z00zz__unicodez00, BNIL)));
			BGl_list3550z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(226L),
				MAKE_YOUNG_PAIR(BGl_list3551z00zz__unicodez00,
					MAKE_YOUNG_PAIR(BGl_list3571z00zz__unicodez00,
						MAKE_YOUNG_PAIR(BGl_list3573z00zz__unicodez00, BNIL))));
			BGl_list3549z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BGl_list3550z00zz__unicodez00, BNIL);
			BGl_list3579z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(172L), BCHAR(((unsigned char) 164)));
			BGl_list3578z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(130L),
				MAKE_YOUNG_PAIR(BGl_list3574z00zz__unicodez00,
					MAKE_YOUNG_PAIR(BGl_list3575z00zz__unicodez00,
						MAKE_YOUNG_PAIR(BGl_list3579z00zz__unicodez00, BNIL))));
			BGl_list3577z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(226L),
				MAKE_YOUNG_PAIR(BGl_list3551z00zz__unicodez00,
					MAKE_YOUNG_PAIR(BGl_list3571z00zz__unicodez00,
						MAKE_YOUNG_PAIR(BGl_list3578z00zz__unicodez00, BNIL))));
			BGl_list3581z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(147L), BCHAR(((unsigned char) 189)));
			BGl_list3582z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(146L), BCHAR(((unsigned char) 188)));
			BGl_list3583z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(184L), BCHAR(((unsigned char) 190)));
			BGl_list3584z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(161L), BCHAR(((unsigned char) 168)));
			BGl_list3585z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(160L), BCHAR(((unsigned char) 166)));
			BGl_list3586z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(190L), BCHAR(((unsigned char) 184)));
			BGl_list3587z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(189L), BCHAR(((unsigned char) 180)));
			BGl_list3580z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(197L),
				MAKE_YOUNG_PAIR(BGl_list3581z00zz__unicodez00,
					MAKE_YOUNG_PAIR(BGl_list3582z00zz__unicodez00,
						MAKE_YOUNG_PAIR(BGl_list3583z00zz__unicodez00,
							MAKE_YOUNG_PAIR(BGl_list3584z00zz__unicodez00,
								MAKE_YOUNG_PAIR(BGl_list3585z00zz__unicodez00,
									MAKE_YOUNG_PAIR(BGl_list3586z00zz__unicodez00,
										MAKE_YOUNG_PAIR(BGl_list3587z00zz__unicodez00, BNIL))))))));
			BGl_list3576z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BGl_list3577z00zz__unicodez00,
				MAKE_YOUNG_PAIR(BGl_list3580z00zz__unicodez00, BNIL));
			BGl_list3589z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BGl_string3590z00zz__unicodez00,
				MAKE_YOUNG_PAIR(BGl_string3591z00zz__unicodez00,
					MAKE_YOUNG_PAIR(BGl_string3592z00zz__unicodez00,
						MAKE_YOUNG_PAIR(BGl_string3593z00zz__unicodez00,
							MAKE_YOUNG_PAIR(BGl_string3594z00zz__unicodez00,
								MAKE_YOUNG_PAIR(BGl_string3595z00zz__unicodez00,
									MAKE_YOUNG_PAIR(BGl_string3596z00zz__unicodez00,
										MAKE_YOUNG_PAIR(BGl_string3597z00zz__unicodez00,
											MAKE_YOUNG_PAIR(BGl_string3598z00zz__unicodez00,
												MAKE_YOUNG_PAIR(BGl_string3599z00zz__unicodez00,
													MAKE_YOUNG_PAIR(BGl_string3600z00zz__unicodez00,
														MAKE_YOUNG_PAIR(BGl_string3601z00zz__unicodez00,
															MAKE_YOUNG_PAIR(BGl_string3602z00zz__unicodez00,
																MAKE_YOUNG_PAIR(BGl_string3591z00zz__unicodez00,
																	MAKE_YOUNG_PAIR
																	(BGl_string3603z00zz__unicodez00,
																		MAKE_YOUNG_PAIR
																		(BGl_string3591z00zz__unicodez00,
																			MAKE_YOUNG_PAIR
																			(BGl_string3591z00zz__unicodez00,
																				MAKE_YOUNG_PAIR
																				(BGl_string3604z00zz__unicodez00,
																					MAKE_YOUNG_PAIR
																					(BGl_string3605z00zz__unicodez00,
																						MAKE_YOUNG_PAIR
																						(BGl_string3606z00zz__unicodez00,
																							MAKE_YOUNG_PAIR
																							(BGl_string3607z00zz__unicodez00,
																								MAKE_YOUNG_PAIR
																								(BGl_string3608z00zz__unicodez00,
																									MAKE_YOUNG_PAIR
																									(BGl_string3609z00zz__unicodez00,
																										MAKE_YOUNG_PAIR
																										(BGl_string3610z00zz__unicodez00,
																											MAKE_YOUNG_PAIR
																											(BGl_string3611z00zz__unicodez00,
																												MAKE_YOUNG_PAIR
																												(BGl_string3612z00zz__unicodez00,
																													MAKE_YOUNG_PAIR
																													(BGl_string3613z00zz__unicodez00,
																														MAKE_YOUNG_PAIR
																														(BGl_string3614z00zz__unicodez00,
																															MAKE_YOUNG_PAIR
																															(BGl_string3615z00zz__unicodez00,
																																MAKE_YOUNG_PAIR
																																(BGl_string3591z00zz__unicodez00,
																																	MAKE_YOUNG_PAIR
																																	(BGl_string3616z00zz__unicodez00,
																																		MAKE_YOUNG_PAIR
																																		(BGl_string3617z00zz__unicodez00,
																																			BNIL))))))))))))))))))))))))))))))));
			BGl_vector3588z00zz__unicodez00 =
				BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00
				(BGl_list3589z00zz__unicodez00);
			BGl_symbol3626z00zz__unicodez00 =
				bstring_to_symbol(BGl_string3627z00zz__unicodez00);
			BGl_symbol3629z00zz__unicodez00 =
				bstring_to_symbol(BGl_string3630z00zz__unicodez00);
			BGl_symbol3662z00zz__unicodez00 =
				bstring_to_symbol(BGl_string3663z00zz__unicodez00);
			BGl_symbol3671z00zz__unicodez00 =
				bstring_to_symbol(BGl_string3672z00zz__unicodez00);
			BGl_list3674z00zz__unicodez00 =
				MAKE_YOUNG_PAIR(BINT(1L), MAKE_YOUNG_PAIR(BINT(1L),
					MAKE_YOUNG_PAIR(BINT(1L), MAKE_YOUNG_PAIR(BINT(1L),
							MAKE_YOUNG_PAIR(BINT(1L), MAKE_YOUNG_PAIR(BINT(1L),
									MAKE_YOUNG_PAIR(BINT(1L), MAKE_YOUNG_PAIR(BINT(1L),
											MAKE_YOUNG_PAIR(BINT(2L), MAKE_YOUNG_PAIR(BINT(2L),
													MAKE_YOUNG_PAIR(BINT(2L), MAKE_YOUNG_PAIR(BINT(2L),
															MAKE_YOUNG_PAIR(BINT(2L),
																MAKE_YOUNG_PAIR(BINT(2L),
																	MAKE_YOUNG_PAIR(BINT(3L),
																		MAKE_YOUNG_PAIR(BINT(4L),
																			BNIL))))))))))))))));
			BGl_vector3673z00zz__unicodez00 =
				BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00
				(BGl_list3674z00zz__unicodez00);
			BGl_symbol3708z00zz__unicodez00 =
				bstring_to_symbol(BGl_string3709z00zz__unicodez00);
			BGl_symbol3712z00zz__unicodez00 =
				bstring_to_symbol(BGl_string3713z00zz__unicodez00);
			return (BGl_symbol3714z00zz__unicodez00 =
				bstring_to_symbol(BGl_string3715z00zz__unicodez00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__unicodez00(void)
	{
		{	/* Llib/unicode.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__unicodez00(void)
	{
		{	/* Llib/unicode.scm 14 */
			BGl_8bitszd2invzd2zz__unicodez00 = BGl_list3549z00zz__unicodez00;
			BGl_8bitszd2invzd2latinzd215zd2zz__unicodez00 =
				BGl_list3576z00zz__unicodez00;
			BGl_cp1252z00zz__unicodez00 = BGl_vector3588z00zz__unicodez00;
			return (BGl_cp1252zd2invzd2zz__unicodez00 = BFALSE, BUNSPEC);
		}

	}



/* ucs2-string? */
	BGL_EXPORTED_DEF bool_t BGl_ucs2zd2stringzf3z21zz__unicodez00(obj_t
		BgL_objz00_3)
	{
		{	/* Llib/unicode.scm 225 */
			return UCS2_STRINGP(BgL_objz00_3);
		}

	}



/* &ucs2-string? */
	obj_t BGl_z62ucs2zd2stringzf3z43zz__unicodez00(obj_t BgL_envz00_6857,
		obj_t BgL_objz00_6858)
	{
		{	/* Llib/unicode.scm 225 */
			return BBOOL(BGl_ucs2zd2stringzf3z21zz__unicodez00(BgL_objz00_6858));
		}

	}



/* _make-ucs2-string */
	obj_t BGl__makezd2ucs2zd2stringz00zz__unicodez00(obj_t BgL_env1124z00_7,
		obj_t BgL_opt1123z00_6)
	{
		{	/* Llib/unicode.scm 231 */
			{	/* Llib/unicode.scm 231 */
				obj_t BgL_g1125z00_7255;

				BgL_g1125z00_7255 = VECTOR_REF(BgL_opt1123z00_6, 0L);
				switch (VECTOR_LENGTH(BgL_opt1123z00_6))
					{
					case 1L:

						{	/* Llib/unicode.scm 232 */
							ucs2_t BgL_fillerz00_7256;

							{	/* Llib/unicode.scm 232 */
								int BgL_tmpz00_7501;

								BgL_tmpz00_7501 =
									(int) (((unsigned char) ((char) (((unsigned char) ' ')))));
								BgL_fillerz00_7256 = BGL_INT_TO_UCS2(BgL_tmpz00_7501);
							}
							{	/* Llib/unicode.scm 231 */

								{	/* Llib/unicode.scm 231 */
									int BgL_kz00_7257;

									{	/* Llib/unicode.scm 231 */
										obj_t BgL_tmpz00_7507;

										if (INTEGERP(BgL_g1125z00_7255))
											{	/* Llib/unicode.scm 231 */
												BgL_tmpz00_7507 = BgL_g1125z00_7255;
											}
										else
											{
												obj_t BgL_auxz00_7510;

												BgL_auxz00_7510 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3618z00zz__unicodez00, BINT(10346L),
													BGl_string3619z00zz__unicodez00,
													BGl_string3620z00zz__unicodez00, BgL_g1125z00_7255);
												FAILURE(BgL_auxz00_7510, BFALSE, BFALSE);
											}
										BgL_kz00_7257 = CINT(BgL_tmpz00_7507);
									}
									return make_ucs2_string(BgL_kz00_7257, BgL_fillerz00_7256);
								}
							}
						}
						break;
					case 2L:

						{	/* Llib/unicode.scm 231 */
							obj_t BgL_fillerz00_7258;

							BgL_fillerz00_7258 = VECTOR_REF(BgL_opt1123z00_6, 1L);
							{	/* Llib/unicode.scm 231 */

								{	/* Llib/unicode.scm 231 */
									int BgL_kz00_7259;
									ucs2_t BgL_fillerz00_7260;

									{	/* Llib/unicode.scm 231 */
										obj_t BgL_tmpz00_7517;

										if (INTEGERP(BgL_g1125z00_7255))
											{	/* Llib/unicode.scm 231 */
												BgL_tmpz00_7517 = BgL_g1125z00_7255;
											}
										else
											{
												obj_t BgL_auxz00_7520;

												BgL_auxz00_7520 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3618z00zz__unicodez00, BINT(10346L),
													BGl_string3619z00zz__unicodez00,
													BGl_string3620z00zz__unicodez00, BgL_g1125z00_7255);
												FAILURE(BgL_auxz00_7520, BFALSE, BFALSE);
											}
										BgL_kz00_7259 = CINT(BgL_tmpz00_7517);
									}
									{	/* Llib/unicode.scm 231 */
										obj_t BgL_tmpz00_7525;

										if (UCS2P(BgL_fillerz00_7258))
											{	/* Llib/unicode.scm 231 */
												BgL_tmpz00_7525 = BgL_fillerz00_7258;
											}
										else
											{
												obj_t BgL_auxz00_7528;

												BgL_auxz00_7528 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3618z00zz__unicodez00, BINT(10346L),
													BGl_string3619z00zz__unicodez00,
													BGl_string3621z00zz__unicodez00, BgL_fillerz00_7258);
												FAILURE(BgL_auxz00_7528, BFALSE, BFALSE);
											}
										BgL_fillerz00_7260 = CUCS2(BgL_tmpz00_7525);
									}
									return make_ucs2_string(BgL_kz00_7259, BgL_fillerz00_7260);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* make-ucs2-string */
	BGL_EXPORTED_DEF obj_t BGl_makezd2ucs2zd2stringz00zz__unicodez00(int
		BgL_kz00_4, ucs2_t BgL_fillerz00_5)
	{
		{	/* Llib/unicode.scm 231 */
			BGL_TAIL return make_ucs2_string(BgL_kz00_4, BgL_fillerz00_5);
		}

	}



/* ucs2-string */
	BGL_EXPORTED_DEF obj_t BGl_ucs2zd2stringzd2zz__unicodez00(obj_t
		BgL_ucs2sz00_8)
	{
		{	/* Llib/unicode.scm 238 */
			BGL_TAIL return
				BGl_listzd2ze3ucs2zd2stringze3zz__unicodez00(BgL_ucs2sz00_8);
		}

	}



/* &ucs2-string */
	obj_t BGl_z62ucs2zd2stringzb0zz__unicodez00(obj_t BgL_envz00_6859,
		obj_t BgL_ucs2sz00_6860)
	{
		{	/* Llib/unicode.scm 238 */
			return BGl_ucs2zd2stringzd2zz__unicodez00(BgL_ucs2sz00_6860);
		}

	}



/* ucs2-string-length */
	BGL_EXPORTED_DEF int BGl_ucs2zd2stringzd2lengthz00zz__unicodez00(obj_t
		BgL_ucs2zd2stringzd2_9)
	{
		{	/* Llib/unicode.scm 244 */
			return UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_9);
		}

	}



/* &ucs2-string-length */
	obj_t BGl_z62ucs2zd2stringzd2lengthz62zz__unicodez00(obj_t BgL_envz00_6861,
		obj_t BgL_ucs2zd2stringzd2_6862)
	{
		{	/* Llib/unicode.scm 244 */
			{	/* Llib/unicode.scm 245 */
				int BgL_tmpz00_7540;

				{	/* Llib/unicode.scm 245 */
					obj_t BgL_auxz00_7541;

					if (UCS2_STRINGP(BgL_ucs2zd2stringzd2_6862))
						{	/* Llib/unicode.scm 245 */
							BgL_auxz00_7541 = BgL_ucs2zd2stringzd2_6862;
						}
					else
						{
							obj_t BgL_auxz00_7544;

							BgL_auxz00_7544 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(11033L), BGl_string3622z00zz__unicodez00,
								BGl_string3623z00zz__unicodez00, BgL_ucs2zd2stringzd2_6862);
							FAILURE(BgL_auxz00_7544, BFALSE, BFALSE);
						}
					BgL_tmpz00_7540 =
						BGl_ucs2zd2stringzd2lengthz00zz__unicodez00(BgL_auxz00_7541);
				}
				return BINT(BgL_tmpz00_7540);
			}
		}

	}



/* ucs2-string-ref */
	BGL_EXPORTED_DEF ucs2_t BGl_ucs2zd2stringzd2refz00zz__unicodez00(obj_t
		BgL_ucs2zd2stringzd2_10, int BgL_kz00_11)
	{
		{	/* Llib/unicode.scm 250 */
			{	/* Llib/unicode.scm 251 */
				bool_t BgL_test3908z00_7550;

				{	/* Llib/unicode.scm 251 */
					long BgL_auxz00_7553;
					long BgL_tmpz00_7551;

					BgL_auxz00_7553 =
						(long) (UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_10));
					BgL_tmpz00_7551 = (long) (BgL_kz00_11);
					BgL_test3908z00_7550 = BOUND_CHECK(BgL_tmpz00_7551, BgL_auxz00_7553);
				}
				if (BgL_test3908z00_7550)
					{	/* Llib/unicode.scm 251 */
						return UCS2_STRING_REF(BgL_ucs2zd2stringzd2_10, BgL_kz00_11);
					}
				else
					{	/* Llib/unicode.scm 256 */
						obj_t BgL_arg1225z00_7261;

						{	/* Llib/unicode.scm 256 */
							obj_t BgL_arg1226z00_7262;

							{	/* Llib/unicode.scm 256 */

								BgL_arg1226z00_7262 =
									BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
									((long) (UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_10)) - 1L),
									10L);
							}
							BgL_arg1225z00_7261 =
								string_append_3(BGl_string3624z00zz__unicodez00,
								BgL_arg1226z00_7262, BGl_string3625z00zz__unicodez00);
						}
						return
							CUCS2(BGl_errorz00zz__errorz00(BGl_symbol3626z00zz__unicodez00,
								BgL_arg1225z00_7261, BINT(BgL_kz00_11)));
					}
			}
		}

	}



/* &ucs2-string-ref */
	obj_t BGl_z62ucs2zd2stringzd2refz62zz__unicodez00(obj_t BgL_envz00_6863,
		obj_t BgL_ucs2zd2stringzd2_6864, obj_t BgL_kz00_6865)
	{
		{	/* Llib/unicode.scm 250 */
			{	/* Llib/unicode.scm 251 */
				ucs2_t BgL_tmpz00_7566;

				{	/* Llib/unicode.scm 251 */
					int BgL_auxz00_7574;
					obj_t BgL_auxz00_7567;

					{	/* Llib/unicode.scm 251 */
						obj_t BgL_tmpz00_7575;

						if (INTEGERP(BgL_kz00_6865))
							{	/* Llib/unicode.scm 251 */
								BgL_tmpz00_7575 = BgL_kz00_6865;
							}
						else
							{
								obj_t BgL_auxz00_7578;

								BgL_auxz00_7578 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string3618z00zz__unicodez00, BINT(11342L),
									BGl_string3628z00zz__unicodez00,
									BGl_string3620z00zz__unicodez00, BgL_kz00_6865);
								FAILURE(BgL_auxz00_7578, BFALSE, BFALSE);
							}
						BgL_auxz00_7574 = CINT(BgL_tmpz00_7575);
					}
					if (UCS2_STRINGP(BgL_ucs2zd2stringzd2_6864))
						{	/* Llib/unicode.scm 251 */
							BgL_auxz00_7567 = BgL_ucs2zd2stringzd2_6864;
						}
					else
						{
							obj_t BgL_auxz00_7570;

							BgL_auxz00_7570 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(11342L), BGl_string3628z00zz__unicodez00,
								BGl_string3623z00zz__unicodez00, BgL_ucs2zd2stringzd2_6864);
							FAILURE(BgL_auxz00_7570, BFALSE, BFALSE);
						}
					BgL_tmpz00_7566 =
						BGl_ucs2zd2stringzd2refz00zz__unicodez00(BgL_auxz00_7567,
						BgL_auxz00_7574);
				}
				return BUCS2(BgL_tmpz00_7566);
			}
		}

	}



/* ucs2-string-set! */
	BGL_EXPORTED_DEF obj_t BGl_ucs2zd2stringzd2setz12z12zz__unicodez00(obj_t
		BgL_ucs2zd2stringzd2_12, int BgL_kz00_13, ucs2_t BgL_ucs2z00_14)
	{
		{	/* Llib/unicode.scm 263 */
			{	/* Llib/unicode.scm 264 */
				bool_t BgL_test3911z00_7585;

				{	/* Llib/unicode.scm 264 */
					long BgL_auxz00_7588;
					long BgL_tmpz00_7586;

					BgL_auxz00_7588 =
						(long) (UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_12));
					BgL_tmpz00_7586 = (long) (BgL_kz00_13);
					BgL_test3911z00_7585 = BOUND_CHECK(BgL_tmpz00_7586, BgL_auxz00_7588);
				}
				if (BgL_test3911z00_7585)
					{	/* Llib/unicode.scm 264 */
						return
							UCS2_STRING_SET(BgL_ucs2zd2stringzd2_12, BgL_kz00_13,
							BgL_ucs2z00_14);
					}
				else
					{	/* Llib/unicode.scm 269 */
						obj_t BgL_arg1233z00_7263;

						{	/* Llib/unicode.scm 269 */
							obj_t BgL_arg1234z00_7264;

							{	/* Llib/unicode.scm 269 */

								BgL_arg1234z00_7264 =
									BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
									((long) (UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_12)) - 1L),
									10L);
							}
							BgL_arg1233z00_7263 =
								string_append_3(BGl_string3624z00zz__unicodez00,
								BgL_arg1234z00_7264, BGl_string3625z00zz__unicodez00);
						}
						return
							BGl_errorz00zz__errorz00(BGl_symbol3629z00zz__unicodez00,
							BgL_arg1233z00_7263, BINT(BgL_kz00_13));
					}
			}
		}

	}



/* &ucs2-string-set! */
	obj_t BGl_z62ucs2zd2stringzd2setz12z70zz__unicodez00(obj_t BgL_envz00_6866,
		obj_t BgL_ucs2zd2stringzd2_6867, obj_t BgL_kz00_6868,
		obj_t BgL_ucs2z00_6869)
	{
		{	/* Llib/unicode.scm 263 */
			{	/* Llib/unicode.scm 264 */
				ucs2_t BgL_auxz00_7616;
				int BgL_auxz00_7607;
				obj_t BgL_auxz00_7600;

				{	/* Llib/unicode.scm 264 */
					obj_t BgL_tmpz00_7617;

					if (UCS2P(BgL_ucs2z00_6869))
						{	/* Llib/unicode.scm 264 */
							BgL_tmpz00_7617 = BgL_ucs2z00_6869;
						}
					else
						{
							obj_t BgL_auxz00_7620;

							BgL_auxz00_7620 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(11905L), BGl_string3631z00zz__unicodez00,
								BGl_string3621z00zz__unicodez00, BgL_ucs2z00_6869);
							FAILURE(BgL_auxz00_7620, BFALSE, BFALSE);
						}
					BgL_auxz00_7616 = CUCS2(BgL_tmpz00_7617);
				}
				{	/* Llib/unicode.scm 264 */
					obj_t BgL_tmpz00_7608;

					if (INTEGERP(BgL_kz00_6868))
						{	/* Llib/unicode.scm 264 */
							BgL_tmpz00_7608 = BgL_kz00_6868;
						}
					else
						{
							obj_t BgL_auxz00_7611;

							BgL_auxz00_7611 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(11905L), BGl_string3631z00zz__unicodez00,
								BGl_string3620z00zz__unicodez00, BgL_kz00_6868);
							FAILURE(BgL_auxz00_7611, BFALSE, BFALSE);
						}
					BgL_auxz00_7607 = CINT(BgL_tmpz00_7608);
				}
				if (UCS2_STRINGP(BgL_ucs2zd2stringzd2_6867))
					{	/* Llib/unicode.scm 264 */
						BgL_auxz00_7600 = BgL_ucs2zd2stringzd2_6867;
					}
				else
					{
						obj_t BgL_auxz00_7603;

						BgL_auxz00_7603 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
							BINT(11905L), BGl_string3631z00zz__unicodez00,
							BGl_string3623z00zz__unicodez00, BgL_ucs2zd2stringzd2_6867);
						FAILURE(BgL_auxz00_7603, BFALSE, BFALSE);
					}
				return
					BGl_ucs2zd2stringzd2setz12z12zz__unicodez00(BgL_auxz00_7600,
					BgL_auxz00_7607, BgL_auxz00_7616);
			}
		}

	}



/* ucs2-string-ref-ur */
	BGL_EXPORTED_DEF ucs2_t BGl_ucs2zd2stringzd2refzd2urzd2zz__unicodez00(obj_t
		BgL_ucs2zd2stringzd2_15, int BgL_kz00_16)
	{
		{	/* Llib/unicode.scm 276 */
			return UCS2_STRING_REF(BgL_ucs2zd2stringzd2_15, BgL_kz00_16);
		}

	}



/* &ucs2-string-ref-ur */
	obj_t BGl_z62ucs2zd2stringzd2refzd2urzb0zz__unicodez00(obj_t BgL_envz00_6870,
		obj_t BgL_ucs2zd2stringzd2_6871, obj_t BgL_kz00_6872)
	{
		{	/* Llib/unicode.scm 276 */
			{	/* Llib/unicode.scm 277 */
				ucs2_t BgL_tmpz00_7627;

				{	/* Llib/unicode.scm 277 */
					int BgL_auxz00_7635;
					obj_t BgL_auxz00_7628;

					{	/* Llib/unicode.scm 277 */
						obj_t BgL_tmpz00_7636;

						if (INTEGERP(BgL_kz00_6872))
							{	/* Llib/unicode.scm 277 */
								BgL_tmpz00_7636 = BgL_kz00_6872;
							}
						else
							{
								obj_t BgL_auxz00_7639;

								BgL_auxz00_7639 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string3618z00zz__unicodez00, BINT(12471L),
									BGl_string3632z00zz__unicodez00,
									BGl_string3620z00zz__unicodez00, BgL_kz00_6872);
								FAILURE(BgL_auxz00_7639, BFALSE, BFALSE);
							}
						BgL_auxz00_7635 = CINT(BgL_tmpz00_7636);
					}
					if (UCS2_STRINGP(BgL_ucs2zd2stringzd2_6871))
						{	/* Llib/unicode.scm 277 */
							BgL_auxz00_7628 = BgL_ucs2zd2stringzd2_6871;
						}
					else
						{
							obj_t BgL_auxz00_7631;

							BgL_auxz00_7631 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(12471L), BGl_string3632z00zz__unicodez00,
								BGl_string3623z00zz__unicodez00, BgL_ucs2zd2stringzd2_6871);
							FAILURE(BgL_auxz00_7631, BFALSE, BFALSE);
						}
					BgL_tmpz00_7627 =
						BGl_ucs2zd2stringzd2refzd2urzd2zz__unicodez00(BgL_auxz00_7628,
						BgL_auxz00_7635);
				}
				return BUCS2(BgL_tmpz00_7627);
			}
		}

	}



/* ucs2-string-set-ur! */
	BGL_EXPORTED_DEF obj_t BGl_ucs2zd2stringzd2setzd2urz12zc0zz__unicodez00(obj_t
		BgL_ucs2zd2stringzd2_17, int BgL_kz00_18, ucs2_t BgL_ucs2z00_19)
	{
		{	/* Llib/unicode.scm 282 */
			return
				UCS2_STRING_SET(BgL_ucs2zd2stringzd2_17, BgL_kz00_18, BgL_ucs2z00_19);
		}

	}



/* &ucs2-string-set-ur! */
	obj_t BGl_z62ucs2zd2stringzd2setzd2urz12za2zz__unicodez00(obj_t
		BgL_envz00_6873, obj_t BgL_ucs2zd2stringzd2_6874, obj_t BgL_kz00_6875,
		obj_t BgL_ucs2z00_6876)
	{
		{	/* Llib/unicode.scm 282 */
			{	/* Llib/unicode.scm 283 */
				ucs2_t BgL_auxz00_7663;
				int BgL_auxz00_7654;
				obj_t BgL_auxz00_7647;

				{	/* Llib/unicode.scm 283 */
					obj_t BgL_tmpz00_7664;

					if (UCS2P(BgL_ucs2z00_6876))
						{	/* Llib/unicode.scm 283 */
							BgL_tmpz00_7664 = BgL_ucs2z00_6876;
						}
					else
						{
							obj_t BgL_auxz00_7667;

							BgL_auxz00_7667 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(12789L), BGl_string3633z00zz__unicodez00,
								BGl_string3621z00zz__unicodez00, BgL_ucs2z00_6876);
							FAILURE(BgL_auxz00_7667, BFALSE, BFALSE);
						}
					BgL_auxz00_7663 = CUCS2(BgL_tmpz00_7664);
				}
				{	/* Llib/unicode.scm 283 */
					obj_t BgL_tmpz00_7655;

					if (INTEGERP(BgL_kz00_6875))
						{	/* Llib/unicode.scm 283 */
							BgL_tmpz00_7655 = BgL_kz00_6875;
						}
					else
						{
							obj_t BgL_auxz00_7658;

							BgL_auxz00_7658 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(12789L), BGl_string3633z00zz__unicodez00,
								BGl_string3620z00zz__unicodez00, BgL_kz00_6875);
							FAILURE(BgL_auxz00_7658, BFALSE, BFALSE);
						}
					BgL_auxz00_7654 = CINT(BgL_tmpz00_7655);
				}
				if (UCS2_STRINGP(BgL_ucs2zd2stringzd2_6874))
					{	/* Llib/unicode.scm 283 */
						BgL_auxz00_7647 = BgL_ucs2zd2stringzd2_6874;
					}
				else
					{
						obj_t BgL_auxz00_7650;

						BgL_auxz00_7650 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
							BINT(12789L), BGl_string3633z00zz__unicodez00,
							BGl_string3623z00zz__unicodez00, BgL_ucs2zd2stringzd2_6874);
						FAILURE(BgL_auxz00_7650, BFALSE, BFALSE);
					}
				return
					BGl_ucs2zd2stringzd2setzd2urz12zc0zz__unicodez00(BgL_auxz00_7647,
					BgL_auxz00_7654, BgL_auxz00_7663);
			}
		}

	}



/* ucs2-string=? */
	BGL_EXPORTED_DEF bool_t BGl_ucs2zd2stringzd3zf3zf2zz__unicodez00(obj_t
		BgL_ucs2zd2string1zd2_20, obj_t BgL_ucs2zd2string2zd2_21)
	{
		{	/* Llib/unicode.scm 288 */
			BGL_TAIL return
				ucs2_strcmp(BgL_ucs2zd2string1zd2_20, BgL_ucs2zd2string2zd2_21);
		}

	}



/* &ucs2-string=? */
	obj_t BGl_z62ucs2zd2stringzd3zf3z90zz__unicodez00(obj_t BgL_envz00_6877,
		obj_t BgL_ucs2zd2string1zd2_6878, obj_t BgL_ucs2zd2string2zd2_6879)
	{
		{	/* Llib/unicode.scm 288 */
			{	/* Llib/unicode.scm 289 */
				bool_t BgL_tmpz00_7674;

				{	/* Llib/unicode.scm 289 */
					obj_t BgL_auxz00_7682;
					obj_t BgL_auxz00_7675;

					if (UCS2_STRINGP(BgL_ucs2zd2string2zd2_6879))
						{	/* Llib/unicode.scm 289 */
							BgL_auxz00_7682 = BgL_ucs2zd2string2zd2_6879;
						}
					else
						{
							obj_t BgL_auxz00_7685;

							BgL_auxz00_7685 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(13113L), BGl_string3634z00zz__unicodez00,
								BGl_string3623z00zz__unicodez00, BgL_ucs2zd2string2zd2_6879);
							FAILURE(BgL_auxz00_7685, BFALSE, BFALSE);
						}
					if (UCS2_STRINGP(BgL_ucs2zd2string1zd2_6878))
						{	/* Llib/unicode.scm 289 */
							BgL_auxz00_7675 = BgL_ucs2zd2string1zd2_6878;
						}
					else
						{
							obj_t BgL_auxz00_7678;

							BgL_auxz00_7678 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(13113L), BGl_string3634z00zz__unicodez00,
								BGl_string3623z00zz__unicodez00, BgL_ucs2zd2string1zd2_6878);
							FAILURE(BgL_auxz00_7678, BFALSE, BFALSE);
						}
					BgL_tmpz00_7674 =
						BGl_ucs2zd2stringzd3zf3zf2zz__unicodez00(BgL_auxz00_7675,
						BgL_auxz00_7682);
				}
				return BBOOL(BgL_tmpz00_7674);
			}
		}

	}



/* ucs2-string-ci=? */
	BGL_EXPORTED_DEF bool_t BGl_ucs2zd2stringzd2cizd3zf3z20zz__unicodez00(obj_t
		BgL_ucs2zd2string1zd2_22, obj_t BgL_ucs2zd2string2zd2_23)
	{
		{	/* Llib/unicode.scm 294 */
			BGL_TAIL return
				ucs2_strcicmp(BgL_ucs2zd2string1zd2_22, BgL_ucs2zd2string2zd2_23);
		}

	}



/* &ucs2-string-ci=? */
	obj_t BGl_z62ucs2zd2stringzd2cizd3zf3z42zz__unicodez00(obj_t BgL_envz00_6880,
		obj_t BgL_ucs2zd2string1zd2_6881, obj_t BgL_ucs2zd2string2zd2_6882)
	{
		{	/* Llib/unicode.scm 294 */
			{	/* Llib/unicode.scm 295 */
				bool_t BgL_tmpz00_7692;

				{	/* Llib/unicode.scm 295 */
					obj_t BgL_auxz00_7700;
					obj_t BgL_auxz00_7693;

					if (UCS2_STRINGP(BgL_ucs2zd2string2zd2_6882))
						{	/* Llib/unicode.scm 295 */
							BgL_auxz00_7700 = BgL_ucs2zd2string2zd2_6882;
						}
					else
						{
							obj_t BgL_auxz00_7703;

							BgL_auxz00_7703 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(13444L), BGl_string3635z00zz__unicodez00,
								BGl_string3623z00zz__unicodez00, BgL_ucs2zd2string2zd2_6882);
							FAILURE(BgL_auxz00_7703, BFALSE, BFALSE);
						}
					if (UCS2_STRINGP(BgL_ucs2zd2string1zd2_6881))
						{	/* Llib/unicode.scm 295 */
							BgL_auxz00_7693 = BgL_ucs2zd2string1zd2_6881;
						}
					else
						{
							obj_t BgL_auxz00_7696;

							BgL_auxz00_7696 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(13444L), BGl_string3635z00zz__unicodez00,
								BGl_string3623z00zz__unicodez00, BgL_ucs2zd2string1zd2_6881);
							FAILURE(BgL_auxz00_7696, BFALSE, BFALSE);
						}
					BgL_tmpz00_7692 =
						BGl_ucs2zd2stringzd2cizd3zf3z20zz__unicodez00(BgL_auxz00_7693,
						BgL_auxz00_7700);
				}
				return BBOOL(BgL_tmpz00_7692);
			}
		}

	}



/* ucs2-string<? */
	BGL_EXPORTED_DEF bool_t BGl_ucs2zd2stringzc3zf3ze2zz__unicodez00(obj_t
		BgL_ucs2zd2string1zd2_24, obj_t BgL_ucs2zd2string2zd2_25)
	{
		{	/* Llib/unicode.scm 300 */
			BGL_TAIL return
				ucs2_string_lt(BgL_ucs2zd2string1zd2_24, BgL_ucs2zd2string2zd2_25);
		}

	}



/* &ucs2-string<? */
	obj_t BGl_z62ucs2zd2stringzc3zf3z80zz__unicodez00(obj_t BgL_envz00_6883,
		obj_t BgL_ucs2zd2string1zd2_6884, obj_t BgL_ucs2zd2string2zd2_6885)
	{
		{	/* Llib/unicode.scm 300 */
			{	/* Llib/unicode.scm 301 */
				bool_t BgL_tmpz00_7710;

				{	/* Llib/unicode.scm 301 */
					obj_t BgL_auxz00_7718;
					obj_t BgL_auxz00_7711;

					if (UCS2_STRINGP(BgL_ucs2zd2string2zd2_6885))
						{	/* Llib/unicode.scm 301 */
							BgL_auxz00_7718 = BgL_ucs2zd2string2zd2_6885;
						}
					else
						{
							obj_t BgL_auxz00_7721;

							BgL_auxz00_7721 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(13770L), BGl_string3636z00zz__unicodez00,
								BGl_string3623z00zz__unicodez00, BgL_ucs2zd2string2zd2_6885);
							FAILURE(BgL_auxz00_7721, BFALSE, BFALSE);
						}
					if (UCS2_STRINGP(BgL_ucs2zd2string1zd2_6884))
						{	/* Llib/unicode.scm 301 */
							BgL_auxz00_7711 = BgL_ucs2zd2string1zd2_6884;
						}
					else
						{
							obj_t BgL_auxz00_7714;

							BgL_auxz00_7714 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(13770L), BGl_string3636z00zz__unicodez00,
								BGl_string3623z00zz__unicodez00, BgL_ucs2zd2string1zd2_6884);
							FAILURE(BgL_auxz00_7714, BFALSE, BFALSE);
						}
					BgL_tmpz00_7710 =
						BGl_ucs2zd2stringzc3zf3ze2zz__unicodez00(BgL_auxz00_7711,
						BgL_auxz00_7718);
				}
				return BBOOL(BgL_tmpz00_7710);
			}
		}

	}



/* ucs2-string>? */
	BGL_EXPORTED_DEF bool_t BGl_ucs2zd2stringze3zf3zc2zz__unicodez00(obj_t
		BgL_ucs2zd2string1zd2_26, obj_t BgL_ucs2zd2string2zd2_27)
	{
		{	/* Llib/unicode.scm 306 */
			BGL_TAIL return
				ucs2_string_gt(BgL_ucs2zd2string1zd2_26, BgL_ucs2zd2string2zd2_27);
		}

	}



/* &ucs2-string>? */
	obj_t BGl_z62ucs2zd2stringze3zf3za0zz__unicodez00(obj_t BgL_envz00_6886,
		obj_t BgL_ucs2zd2string1zd2_6887, obj_t BgL_ucs2zd2string2zd2_6888)
	{
		{	/* Llib/unicode.scm 306 */
			{	/* Llib/unicode.scm 307 */
				bool_t BgL_tmpz00_7728;

				{	/* Llib/unicode.scm 307 */
					obj_t BgL_auxz00_7736;
					obj_t BgL_auxz00_7729;

					if (UCS2_STRINGP(BgL_ucs2zd2string2zd2_6888))
						{	/* Llib/unicode.scm 307 */
							BgL_auxz00_7736 = BgL_ucs2zd2string2zd2_6888;
						}
					else
						{
							obj_t BgL_auxz00_7739;

							BgL_auxz00_7739 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(14097L), BGl_string3637z00zz__unicodez00,
								BGl_string3623z00zz__unicodez00, BgL_ucs2zd2string2zd2_6888);
							FAILURE(BgL_auxz00_7739, BFALSE, BFALSE);
						}
					if (UCS2_STRINGP(BgL_ucs2zd2string1zd2_6887))
						{	/* Llib/unicode.scm 307 */
							BgL_auxz00_7729 = BgL_ucs2zd2string1zd2_6887;
						}
					else
						{
							obj_t BgL_auxz00_7732;

							BgL_auxz00_7732 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(14097L), BGl_string3637z00zz__unicodez00,
								BGl_string3623z00zz__unicodez00, BgL_ucs2zd2string1zd2_6887);
							FAILURE(BgL_auxz00_7732, BFALSE, BFALSE);
						}
					BgL_tmpz00_7728 =
						BGl_ucs2zd2stringze3zf3zc2zz__unicodez00(BgL_auxz00_7729,
						BgL_auxz00_7736);
				}
				return BBOOL(BgL_tmpz00_7728);
			}
		}

	}



/* ucs2-string<=? */
	BGL_EXPORTED_DEF bool_t BGl_ucs2zd2stringzc3zd3zf3z31zz__unicodez00(obj_t
		BgL_ucs2zd2string1zd2_28, obj_t BgL_ucs2zd2string2zd2_29)
	{
		{	/* Llib/unicode.scm 312 */
			BGL_TAIL return
				ucs2_string_le(BgL_ucs2zd2string1zd2_28, BgL_ucs2zd2string2zd2_29);
		}

	}



/* &ucs2-string<=? */
	obj_t BGl_z62ucs2zd2stringzc3zd3zf3z53zz__unicodez00(obj_t BgL_envz00_6889,
		obj_t BgL_ucs2zd2string1zd2_6890, obj_t BgL_ucs2zd2string2zd2_6891)
	{
		{	/* Llib/unicode.scm 312 */
			{	/* Llib/unicode.scm 313 */
				bool_t BgL_tmpz00_7746;

				{	/* Llib/unicode.scm 313 */
					obj_t BgL_auxz00_7754;
					obj_t BgL_auxz00_7747;

					if (UCS2_STRINGP(BgL_ucs2zd2string2zd2_6891))
						{	/* Llib/unicode.scm 313 */
							BgL_auxz00_7754 = BgL_ucs2zd2string2zd2_6891;
						}
					else
						{
							obj_t BgL_auxz00_7757;

							BgL_auxz00_7757 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(14425L), BGl_string3638z00zz__unicodez00,
								BGl_string3623z00zz__unicodez00, BgL_ucs2zd2string2zd2_6891);
							FAILURE(BgL_auxz00_7757, BFALSE, BFALSE);
						}
					if (UCS2_STRINGP(BgL_ucs2zd2string1zd2_6890))
						{	/* Llib/unicode.scm 313 */
							BgL_auxz00_7747 = BgL_ucs2zd2string1zd2_6890;
						}
					else
						{
							obj_t BgL_auxz00_7750;

							BgL_auxz00_7750 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(14425L), BGl_string3638z00zz__unicodez00,
								BGl_string3623z00zz__unicodez00, BgL_ucs2zd2string1zd2_6890);
							FAILURE(BgL_auxz00_7750, BFALSE, BFALSE);
						}
					BgL_tmpz00_7746 =
						BGl_ucs2zd2stringzc3zd3zf3z31zz__unicodez00(BgL_auxz00_7747,
						BgL_auxz00_7754);
				}
				return BBOOL(BgL_tmpz00_7746);
			}
		}

	}



/* ucs2-string>=? */
	BGL_EXPORTED_DEF bool_t BGl_ucs2zd2stringze3zd3zf3z11zz__unicodez00(obj_t
		BgL_ucs2zd2string1zd2_30, obj_t BgL_ucs2zd2string2zd2_31)
	{
		{	/* Llib/unicode.scm 318 */
			BGL_TAIL return
				ucs2_string_ge(BgL_ucs2zd2string1zd2_30, BgL_ucs2zd2string2zd2_31);
		}

	}



/* &ucs2-string>=? */
	obj_t BGl_z62ucs2zd2stringze3zd3zf3z73zz__unicodez00(obj_t BgL_envz00_6892,
		obj_t BgL_ucs2zd2string1zd2_6893, obj_t BgL_ucs2zd2string2zd2_6894)
	{
		{	/* Llib/unicode.scm 318 */
			{	/* Llib/unicode.scm 319 */
				bool_t BgL_tmpz00_7764;

				{	/* Llib/unicode.scm 319 */
					obj_t BgL_auxz00_7772;
					obj_t BgL_auxz00_7765;

					if (UCS2_STRINGP(BgL_ucs2zd2string2zd2_6894))
						{	/* Llib/unicode.scm 319 */
							BgL_auxz00_7772 = BgL_ucs2zd2string2zd2_6894;
						}
					else
						{
							obj_t BgL_auxz00_7775;

							BgL_auxz00_7775 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(14753L), BGl_string3639z00zz__unicodez00,
								BGl_string3623z00zz__unicodez00, BgL_ucs2zd2string2zd2_6894);
							FAILURE(BgL_auxz00_7775, BFALSE, BFALSE);
						}
					if (UCS2_STRINGP(BgL_ucs2zd2string1zd2_6893))
						{	/* Llib/unicode.scm 319 */
							BgL_auxz00_7765 = BgL_ucs2zd2string1zd2_6893;
						}
					else
						{
							obj_t BgL_auxz00_7768;

							BgL_auxz00_7768 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(14753L), BGl_string3639z00zz__unicodez00,
								BGl_string3623z00zz__unicodez00, BgL_ucs2zd2string1zd2_6893);
							FAILURE(BgL_auxz00_7768, BFALSE, BFALSE);
						}
					BgL_tmpz00_7764 =
						BGl_ucs2zd2stringze3zd3zf3z11zz__unicodez00(BgL_auxz00_7765,
						BgL_auxz00_7772);
				}
				return BBOOL(BgL_tmpz00_7764);
			}
		}

	}



/* ucs2-string-ci<? */
	BGL_EXPORTED_DEF bool_t BGl_ucs2zd2stringzd2cizc3zf3z30zz__unicodez00(obj_t
		BgL_ucs2zd2string1zd2_32, obj_t BgL_ucs2zd2string2zd2_33)
	{
		{	/* Llib/unicode.scm 324 */
			BGL_TAIL return
				ucs2_string_cilt(BgL_ucs2zd2string1zd2_32, BgL_ucs2zd2string2zd2_33);
		}

	}



/* &ucs2-string-ci<? */
	obj_t BGl_z62ucs2zd2stringzd2cizc3zf3z52zz__unicodez00(obj_t BgL_envz00_6895,
		obj_t BgL_ucs2zd2string1zd2_6896, obj_t BgL_ucs2zd2string2zd2_6897)
	{
		{	/* Llib/unicode.scm 324 */
			{	/* Llib/unicode.scm 325 */
				bool_t BgL_tmpz00_7782;

				{	/* Llib/unicode.scm 325 */
					obj_t BgL_auxz00_7790;
					obj_t BgL_auxz00_7783;

					if (UCS2_STRINGP(BgL_ucs2zd2string2zd2_6897))
						{	/* Llib/unicode.scm 325 */
							BgL_auxz00_7790 = BgL_ucs2zd2string2zd2_6897;
						}
					else
						{
							obj_t BgL_auxz00_7793;

							BgL_auxz00_7793 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(15083L), BGl_string3640z00zz__unicodez00,
								BGl_string3623z00zz__unicodez00, BgL_ucs2zd2string2zd2_6897);
							FAILURE(BgL_auxz00_7793, BFALSE, BFALSE);
						}
					if (UCS2_STRINGP(BgL_ucs2zd2string1zd2_6896))
						{	/* Llib/unicode.scm 325 */
							BgL_auxz00_7783 = BgL_ucs2zd2string1zd2_6896;
						}
					else
						{
							obj_t BgL_auxz00_7786;

							BgL_auxz00_7786 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(15083L), BGl_string3640z00zz__unicodez00,
								BGl_string3623z00zz__unicodez00, BgL_ucs2zd2string1zd2_6896);
							FAILURE(BgL_auxz00_7786, BFALSE, BFALSE);
						}
					BgL_tmpz00_7782 =
						BGl_ucs2zd2stringzd2cizc3zf3z30zz__unicodez00(BgL_auxz00_7783,
						BgL_auxz00_7790);
				}
				return BBOOL(BgL_tmpz00_7782);
			}
		}

	}



/* ucs2-string-ci>? */
	BGL_EXPORTED_DEF bool_t BGl_ucs2zd2stringzd2cize3zf3z10zz__unicodez00(obj_t
		BgL_ucs2zd2string1zd2_34, obj_t BgL_ucs2zd2string2zd2_35)
	{
		{	/* Llib/unicode.scm 330 */
			BGL_TAIL return
				ucs2_string_cigt(BgL_ucs2zd2string1zd2_34, BgL_ucs2zd2string2zd2_35);
		}

	}



/* &ucs2-string-ci>? */
	obj_t BGl_z62ucs2zd2stringzd2cize3zf3z72zz__unicodez00(obj_t BgL_envz00_6898,
		obj_t BgL_ucs2zd2string1zd2_6899, obj_t BgL_ucs2zd2string2zd2_6900)
	{
		{	/* Llib/unicode.scm 330 */
			{	/* Llib/unicode.scm 331 */
				bool_t BgL_tmpz00_7800;

				{	/* Llib/unicode.scm 331 */
					obj_t BgL_auxz00_7808;
					obj_t BgL_auxz00_7801;

					if (UCS2_STRINGP(BgL_ucs2zd2string2zd2_6900))
						{	/* Llib/unicode.scm 331 */
							BgL_auxz00_7808 = BgL_ucs2zd2string2zd2_6900;
						}
					else
						{
							obj_t BgL_auxz00_7811;

							BgL_auxz00_7811 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(15415L), BGl_string3641z00zz__unicodez00,
								BGl_string3623z00zz__unicodez00, BgL_ucs2zd2string2zd2_6900);
							FAILURE(BgL_auxz00_7811, BFALSE, BFALSE);
						}
					if (UCS2_STRINGP(BgL_ucs2zd2string1zd2_6899))
						{	/* Llib/unicode.scm 331 */
							BgL_auxz00_7801 = BgL_ucs2zd2string1zd2_6899;
						}
					else
						{
							obj_t BgL_auxz00_7804;

							BgL_auxz00_7804 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(15415L), BGl_string3641z00zz__unicodez00,
								BGl_string3623z00zz__unicodez00, BgL_ucs2zd2string1zd2_6899);
							FAILURE(BgL_auxz00_7804, BFALSE, BFALSE);
						}
					BgL_tmpz00_7800 =
						BGl_ucs2zd2stringzd2cize3zf3z10zz__unicodez00(BgL_auxz00_7801,
						BgL_auxz00_7808);
				}
				return BBOOL(BgL_tmpz00_7800);
			}
		}

	}



/* ucs2-string-ci<=? */
	BGL_EXPORTED_DEF bool_t BGl_ucs2zd2stringzd2cizc3zd3zf3ze3zz__unicodez00(obj_t
		BgL_ucs2zd2string1zd2_36, obj_t BgL_ucs2zd2string2zd2_37)
	{
		{	/* Llib/unicode.scm 336 */
			BGL_TAIL return
				ucs2_string_cile(BgL_ucs2zd2string1zd2_36, BgL_ucs2zd2string2zd2_37);
		}

	}



/* &ucs2-string-ci<=? */
	obj_t BGl_z62ucs2zd2stringzd2cizc3zd3zf3z81zz__unicodez00(obj_t
		BgL_envz00_6901, obj_t BgL_ucs2zd2string1zd2_6902,
		obj_t BgL_ucs2zd2string2zd2_6903)
	{
		{	/* Llib/unicode.scm 336 */
			{	/* Llib/unicode.scm 337 */
				bool_t BgL_tmpz00_7818;

				{	/* Llib/unicode.scm 337 */
					obj_t BgL_auxz00_7826;
					obj_t BgL_auxz00_7819;

					if (UCS2_STRINGP(BgL_ucs2zd2string2zd2_6903))
						{	/* Llib/unicode.scm 337 */
							BgL_auxz00_7826 = BgL_ucs2zd2string2zd2_6903;
						}
					else
						{
							obj_t BgL_auxz00_7829;

							BgL_auxz00_7829 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(15748L), BGl_string3642z00zz__unicodez00,
								BGl_string3623z00zz__unicodez00, BgL_ucs2zd2string2zd2_6903);
							FAILURE(BgL_auxz00_7829, BFALSE, BFALSE);
						}
					if (UCS2_STRINGP(BgL_ucs2zd2string1zd2_6902))
						{	/* Llib/unicode.scm 337 */
							BgL_auxz00_7819 = BgL_ucs2zd2string1zd2_6902;
						}
					else
						{
							obj_t BgL_auxz00_7822;

							BgL_auxz00_7822 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(15748L), BGl_string3642z00zz__unicodez00,
								BGl_string3623z00zz__unicodez00, BgL_ucs2zd2string1zd2_6902);
							FAILURE(BgL_auxz00_7822, BFALSE, BFALSE);
						}
					BgL_tmpz00_7818 =
						BGl_ucs2zd2stringzd2cizc3zd3zf3ze3zz__unicodez00(BgL_auxz00_7819,
						BgL_auxz00_7826);
				}
				return BBOOL(BgL_tmpz00_7818);
			}
		}

	}



/* ucs2-string-ci>=? */
	BGL_EXPORTED_DEF bool_t BGl_ucs2zd2stringzd2cize3zd3zf3zc3zz__unicodez00(obj_t
		BgL_ucs2zd2string1zd2_38, obj_t BgL_ucs2zd2string2zd2_39)
	{
		{	/* Llib/unicode.scm 342 */
			BGL_TAIL return
				ucs2_string_cige(BgL_ucs2zd2string1zd2_38, BgL_ucs2zd2string2zd2_39);
		}

	}



/* &ucs2-string-ci>=? */
	obj_t BGl_z62ucs2zd2stringzd2cize3zd3zf3za1zz__unicodez00(obj_t
		BgL_envz00_6904, obj_t BgL_ucs2zd2string1zd2_6905,
		obj_t BgL_ucs2zd2string2zd2_6906)
	{
		{	/* Llib/unicode.scm 342 */
			{	/* Llib/unicode.scm 343 */
				bool_t BgL_tmpz00_7836;

				{	/* Llib/unicode.scm 343 */
					obj_t BgL_auxz00_7844;
					obj_t BgL_auxz00_7837;

					if (UCS2_STRINGP(BgL_ucs2zd2string2zd2_6906))
						{	/* Llib/unicode.scm 343 */
							BgL_auxz00_7844 = BgL_ucs2zd2string2zd2_6906;
						}
					else
						{
							obj_t BgL_auxz00_7847;

							BgL_auxz00_7847 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(16081L), BGl_string3643z00zz__unicodez00,
								BGl_string3623z00zz__unicodez00, BgL_ucs2zd2string2zd2_6906);
							FAILURE(BgL_auxz00_7847, BFALSE, BFALSE);
						}
					if (UCS2_STRINGP(BgL_ucs2zd2string1zd2_6905))
						{	/* Llib/unicode.scm 343 */
							BgL_auxz00_7837 = BgL_ucs2zd2string1zd2_6905;
						}
					else
						{
							obj_t BgL_auxz00_7840;

							BgL_auxz00_7840 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(16081L), BGl_string3643z00zz__unicodez00,
								BGl_string3623z00zz__unicodez00, BgL_ucs2zd2string1zd2_6905);
							FAILURE(BgL_auxz00_7840, BFALSE, BFALSE);
						}
					BgL_tmpz00_7836 =
						BGl_ucs2zd2stringzd2cize3zd3zf3zc3zz__unicodez00(BgL_auxz00_7837,
						BgL_auxz00_7844);
				}
				return BBOOL(BgL_tmpz00_7836);
			}
		}

	}



/* subucs2-string */
	BGL_EXPORTED_DEF obj_t BGl_subucs2zd2stringzd2zz__unicodez00(obj_t
		BgL_ucs2zd2stringzd2_40, int BgL_startz00_41, int BgL_endz00_42)
	{
		{	/* Llib/unicode.scm 348 */
			{	/* Llib/unicode.scm 350 */
				bool_t BgL_test3940z00_7853;

				if (((long) (BgL_endz00_42) >= (long) (BgL_startz00_41)))
					{	/* Llib/unicode.scm 351 */
						bool_t BgL_test3942z00_7858;

						{	/* Llib/unicode.scm 351 */
							long BgL_auxz00_7861;
							long BgL_tmpz00_7859;

							BgL_auxz00_7861 =
								((long) (UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_40)) + 1L);
							BgL_tmpz00_7859 = (long) (BgL_startz00_41);
							BgL_test3942z00_7858 =
								BOUND_CHECK(BgL_tmpz00_7859, BgL_auxz00_7861);
						}
						if (BgL_test3942z00_7858)
							{	/* Llib/unicode.scm 353 */
								long BgL_auxz00_7868;
								long BgL_tmpz00_7866;

								BgL_auxz00_7868 =
									((long) (UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_40)) + 1L);
								BgL_tmpz00_7866 = (long) (BgL_endz00_42);
								BgL_test3940z00_7853 =
									BOUND_CHECK(BgL_tmpz00_7866, BgL_auxz00_7868);
							}
						else
							{	/* Llib/unicode.scm 351 */
								BgL_test3940z00_7853 = ((bool_t) 0);
							}
					}
				else
					{	/* Llib/unicode.scm 350 */
						BgL_test3940z00_7853 = ((bool_t) 0);
					}
				if (BgL_test3940z00_7853)
					{	/* Llib/unicode.scm 350 */
						BGL_TAIL return
							c_subucs2_string(BgL_ucs2zd2stringzd2_40, BgL_startz00_41,
							BgL_endz00_42);
					}
				else
					{	/* Llib/unicode.scm 358 */
						obj_t BgL_arg1272z00_7265;

						BgL_arg1272z00_7265 =
							MAKE_YOUNG_PAIR(BINT(BgL_startz00_41), BINT(BgL_endz00_42));
						return
							BGl_errorz00zz__errorz00(BGl_string3644z00zz__unicodez00,
							BGl_string3645z00zz__unicodez00, BgL_arg1272z00_7265);
					}
			}
		}

	}



/* &subucs2-string */
	obj_t BGl_z62subucs2zd2stringzb0zz__unicodez00(obj_t BgL_envz00_6907,
		obj_t BgL_ucs2zd2stringzd2_6908, obj_t BgL_startz00_6909,
		obj_t BgL_endz00_6910)
	{
		{	/* Llib/unicode.scm 348 */
			{	/* Llib/unicode.scm 350 */
				int BgL_auxz00_7894;
				int BgL_auxz00_7885;
				obj_t BgL_auxz00_7878;

				{	/* Llib/unicode.scm 350 */
					obj_t BgL_tmpz00_7895;

					if (INTEGERP(BgL_endz00_6910))
						{	/* Llib/unicode.scm 350 */
							BgL_tmpz00_7895 = BgL_endz00_6910;
						}
					else
						{
							obj_t BgL_auxz00_7898;

							BgL_auxz00_7898 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(16454L), BGl_string3646z00zz__unicodez00,
								BGl_string3620z00zz__unicodez00, BgL_endz00_6910);
							FAILURE(BgL_auxz00_7898, BFALSE, BFALSE);
						}
					BgL_auxz00_7894 = CINT(BgL_tmpz00_7895);
				}
				{	/* Llib/unicode.scm 350 */
					obj_t BgL_tmpz00_7886;

					if (INTEGERP(BgL_startz00_6909))
						{	/* Llib/unicode.scm 350 */
							BgL_tmpz00_7886 = BgL_startz00_6909;
						}
					else
						{
							obj_t BgL_auxz00_7889;

							BgL_auxz00_7889 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(16454L), BGl_string3646z00zz__unicodez00,
								BGl_string3620z00zz__unicodez00, BgL_startz00_6909);
							FAILURE(BgL_auxz00_7889, BFALSE, BFALSE);
						}
					BgL_auxz00_7885 = CINT(BgL_tmpz00_7886);
				}
				if (UCS2_STRINGP(BgL_ucs2zd2stringzd2_6908))
					{	/* Llib/unicode.scm 350 */
						BgL_auxz00_7878 = BgL_ucs2zd2stringzd2_6908;
					}
				else
					{
						obj_t BgL_auxz00_7881;

						BgL_auxz00_7881 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
							BINT(16454L), BGl_string3646z00zz__unicodez00,
							BGl_string3623z00zz__unicodez00, BgL_ucs2zd2stringzd2_6908);
						FAILURE(BgL_auxz00_7881, BFALSE, BFALSE);
					}
				return
					BGl_subucs2zd2stringzd2zz__unicodez00(BgL_auxz00_7878,
					BgL_auxz00_7885, BgL_auxz00_7894);
			}
		}

	}



/* subucs2-string-ur */
	BGL_EXPORTED_DEF obj_t BGl_subucs2zd2stringzd2urz00zz__unicodez00(obj_t
		BgL_ucs2zd2stringzd2_43, int BgL_startz00_44, int BgL_endz00_45)
	{
		{	/* Llib/unicode.scm 363 */
			BGL_TAIL return
				c_subucs2_string(BgL_ucs2zd2stringzd2_43, BgL_startz00_44,
				BgL_endz00_45);
		}

	}



/* &subucs2-string-ur */
	obj_t BGl_z62subucs2zd2stringzd2urz62zz__unicodez00(obj_t BgL_envz00_6911,
		obj_t BgL_ucs2zd2stringzd2_6912, obj_t BgL_startz00_6913,
		obj_t BgL_endz00_6914)
	{
		{	/* Llib/unicode.scm 363 */
			{	/* Llib/unicode.scm 364 */
				int BgL_auxz00_7921;
				int BgL_auxz00_7912;
				obj_t BgL_auxz00_7905;

				{	/* Llib/unicode.scm 364 */
					obj_t BgL_tmpz00_7922;

					if (INTEGERP(BgL_endz00_6914))
						{	/* Llib/unicode.scm 364 */
							BgL_tmpz00_7922 = BgL_endz00_6914;
						}
					else
						{
							obj_t BgL_auxz00_7925;

							BgL_auxz00_7925 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(17058L), BGl_string3647z00zz__unicodez00,
								BGl_string3620z00zz__unicodez00, BgL_endz00_6914);
							FAILURE(BgL_auxz00_7925, BFALSE, BFALSE);
						}
					BgL_auxz00_7921 = CINT(BgL_tmpz00_7922);
				}
				{	/* Llib/unicode.scm 364 */
					obj_t BgL_tmpz00_7913;

					if (INTEGERP(BgL_startz00_6913))
						{	/* Llib/unicode.scm 364 */
							BgL_tmpz00_7913 = BgL_startz00_6913;
						}
					else
						{
							obj_t BgL_auxz00_7916;

							BgL_auxz00_7916 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(17058L), BGl_string3647z00zz__unicodez00,
								BGl_string3620z00zz__unicodez00, BgL_startz00_6913);
							FAILURE(BgL_auxz00_7916, BFALSE, BFALSE);
						}
					BgL_auxz00_7912 = CINT(BgL_tmpz00_7913);
				}
				if (UCS2_STRINGP(BgL_ucs2zd2stringzd2_6912))
					{	/* Llib/unicode.scm 364 */
						BgL_auxz00_7905 = BgL_ucs2zd2stringzd2_6912;
					}
				else
					{
						obj_t BgL_auxz00_7908;

						BgL_auxz00_7908 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
							BINT(17058L), BGl_string3647z00zz__unicodez00,
							BGl_string3623z00zz__unicodez00, BgL_ucs2zd2stringzd2_6912);
						FAILURE(BgL_auxz00_7908, BFALSE, BFALSE);
					}
				return
					BGl_subucs2zd2stringzd2urz00zz__unicodez00(BgL_auxz00_7905,
					BgL_auxz00_7912, BgL_auxz00_7921);
			}
		}

	}



/* ucs2-substring */
	BGL_EXPORTED_DEF obj_t BGl_ucs2zd2substringzd2zz__unicodez00(obj_t
		BgL_ucs2zd2stringzd2_46, int BgL_startz00_47, int BgL_endz00_48)
	{
		{	/* Llib/unicode.scm 369 */
			{	/* Llib/unicode.scm 371 */
				bool_t BgL_test3949z00_7931;

				if (((long) (BgL_endz00_48) >= (long) (BgL_startz00_47)))
					{	/* Llib/unicode.scm 372 */
						bool_t BgL_test3951z00_7936;

						{	/* Llib/unicode.scm 372 */
							long BgL_auxz00_7939;
							long BgL_tmpz00_7937;

							BgL_auxz00_7939 =
								((long) (UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_46)) + 1L);
							BgL_tmpz00_7937 = (long) (BgL_startz00_47);
							BgL_test3951z00_7936 =
								BOUND_CHECK(BgL_tmpz00_7937, BgL_auxz00_7939);
						}
						if (BgL_test3951z00_7936)
							{	/* Llib/unicode.scm 374 */
								long BgL_auxz00_7946;
								long BgL_tmpz00_7944;

								BgL_auxz00_7946 =
									((long) (UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_46)) + 1L);
								BgL_tmpz00_7944 = (long) (BgL_endz00_48);
								BgL_test3949z00_7931 =
									BOUND_CHECK(BgL_tmpz00_7944, BgL_auxz00_7946);
							}
						else
							{	/* Llib/unicode.scm 372 */
								BgL_test3949z00_7931 = ((bool_t) 0);
							}
					}
				else
					{	/* Llib/unicode.scm 371 */
						BgL_test3949z00_7931 = ((bool_t) 0);
					}
				if (BgL_test3949z00_7931)
					{	/* Llib/unicode.scm 371 */
						BGL_TAIL return
							c_subucs2_string(BgL_ucs2zd2stringzd2_46, BgL_startz00_47,
							BgL_endz00_48);
					}
				else
					{	/* Llib/unicode.scm 379 */
						obj_t BgL_arg1317z00_7266;

						BgL_arg1317z00_7266 =
							MAKE_YOUNG_PAIR(BINT(BgL_startz00_47), BINT(BgL_endz00_48));
						return
							BGl_errorz00zz__errorz00(BGl_string3644z00zz__unicodez00,
							BGl_string3645z00zz__unicodez00, BgL_arg1317z00_7266);
					}
			}
		}

	}



/* &ucs2-substring */
	obj_t BGl_z62ucs2zd2substringzb0zz__unicodez00(obj_t BgL_envz00_6915,
		obj_t BgL_ucs2zd2stringzd2_6916, obj_t BgL_startz00_6917,
		obj_t BgL_endz00_6918)
	{
		{	/* Llib/unicode.scm 369 */
			{	/* Llib/unicode.scm 371 */
				int BgL_auxz00_7972;
				int BgL_auxz00_7963;
				obj_t BgL_auxz00_7956;

				{	/* Llib/unicode.scm 371 */
					obj_t BgL_tmpz00_7973;

					if (INTEGERP(BgL_endz00_6918))
						{	/* Llib/unicode.scm 371 */
							BgL_tmpz00_7973 = BgL_endz00_6918;
						}
					else
						{
							obj_t BgL_auxz00_7976;

							BgL_auxz00_7976 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(17426L), BGl_string3648z00zz__unicodez00,
								BGl_string3620z00zz__unicodez00, BgL_endz00_6918);
							FAILURE(BgL_auxz00_7976, BFALSE, BFALSE);
						}
					BgL_auxz00_7972 = CINT(BgL_tmpz00_7973);
				}
				{	/* Llib/unicode.scm 371 */
					obj_t BgL_tmpz00_7964;

					if (INTEGERP(BgL_startz00_6917))
						{	/* Llib/unicode.scm 371 */
							BgL_tmpz00_7964 = BgL_startz00_6917;
						}
					else
						{
							obj_t BgL_auxz00_7967;

							BgL_auxz00_7967 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(17426L), BGl_string3648z00zz__unicodez00,
								BGl_string3620z00zz__unicodez00, BgL_startz00_6917);
							FAILURE(BgL_auxz00_7967, BFALSE, BFALSE);
						}
					BgL_auxz00_7963 = CINT(BgL_tmpz00_7964);
				}
				if (UCS2_STRINGP(BgL_ucs2zd2stringzd2_6916))
					{	/* Llib/unicode.scm 371 */
						BgL_auxz00_7956 = BgL_ucs2zd2stringzd2_6916;
					}
				else
					{
						obj_t BgL_auxz00_7959;

						BgL_auxz00_7959 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
							BINT(17426L), BGl_string3648z00zz__unicodez00,
							BGl_string3623z00zz__unicodez00, BgL_ucs2zd2stringzd2_6916);
						FAILURE(BgL_auxz00_7959, BFALSE, BFALSE);
					}
				return
					BGl_ucs2zd2substringzd2zz__unicodez00(BgL_auxz00_7956,
					BgL_auxz00_7963, BgL_auxz00_7972);
			}
		}

	}



/* ucs2-substring-ur */
	BGL_EXPORTED_DEF obj_t BGl_ucs2zd2substringzd2urz00zz__unicodez00(obj_t
		BgL_ucs2zd2stringzd2_49, int BgL_startz00_50, int BgL_endz00_51)
	{
		{	/* Llib/unicode.scm 384 */
			BGL_TAIL return
				c_subucs2_string(BgL_ucs2zd2stringzd2_49, BgL_startz00_50,
				BgL_endz00_51);
		}

	}



/* &ucs2-substring-ur */
	obj_t BGl_z62ucs2zd2substringzd2urz62zz__unicodez00(obj_t BgL_envz00_6919,
		obj_t BgL_ucs2zd2stringzd2_6920, obj_t BgL_startz00_6921,
		obj_t BgL_endz00_6922)
	{
		{	/* Llib/unicode.scm 384 */
			{	/* Llib/unicode.scm 385 */
				int BgL_auxz00_7999;
				int BgL_auxz00_7990;
				obj_t BgL_auxz00_7983;

				{	/* Llib/unicode.scm 385 */
					obj_t BgL_tmpz00_8000;

					if (INTEGERP(BgL_endz00_6922))
						{	/* Llib/unicode.scm 385 */
							BgL_tmpz00_8000 = BgL_endz00_6922;
						}
					else
						{
							obj_t BgL_auxz00_8003;

							BgL_auxz00_8003 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(18030L), BGl_string3649z00zz__unicodez00,
								BGl_string3620z00zz__unicodez00, BgL_endz00_6922);
							FAILURE(BgL_auxz00_8003, BFALSE, BFALSE);
						}
					BgL_auxz00_7999 = CINT(BgL_tmpz00_8000);
				}
				{	/* Llib/unicode.scm 385 */
					obj_t BgL_tmpz00_7991;

					if (INTEGERP(BgL_startz00_6921))
						{	/* Llib/unicode.scm 385 */
							BgL_tmpz00_7991 = BgL_startz00_6921;
						}
					else
						{
							obj_t BgL_auxz00_7994;

							BgL_auxz00_7994 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(18030L), BGl_string3649z00zz__unicodez00,
								BGl_string3620z00zz__unicodez00, BgL_startz00_6921);
							FAILURE(BgL_auxz00_7994, BFALSE, BFALSE);
						}
					BgL_auxz00_7990 = CINT(BgL_tmpz00_7991);
				}
				if (UCS2_STRINGP(BgL_ucs2zd2stringzd2_6920))
					{	/* Llib/unicode.scm 385 */
						BgL_auxz00_7983 = BgL_ucs2zd2stringzd2_6920;
					}
				else
					{
						obj_t BgL_auxz00_7986;

						BgL_auxz00_7986 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
							BINT(18030L), BGl_string3649z00zz__unicodez00,
							BGl_string3623z00zz__unicodez00, BgL_ucs2zd2stringzd2_6920);
						FAILURE(BgL_auxz00_7986, BFALSE, BFALSE);
					}
				return
					BGl_ucs2zd2substringzd2urz00zz__unicodez00(BgL_auxz00_7983,
					BgL_auxz00_7990, BgL_auxz00_7999);
			}
		}

	}



/* ucs2-string-append */
	BGL_EXPORTED_DEF obj_t BGl_ucs2zd2stringzd2appendz00zz__unicodez00(obj_t
		BgL_listz00_52)
	{
		{	/* Llib/unicode.scm 390 */
			if (NULLP(BgL_listz00_52))
				{	/* Llib/unicode.scm 137 */
					ucs2_t BgL_fillerz00_1315;

					{	/* Llib/unicode.scm 137 */
						int BgL_tmpz00_8011;

						BgL_tmpz00_8011 =
							(int) (((unsigned char) ((char) (((unsigned char) ' ')))));
						BgL_fillerz00_1315 = BGL_INT_TO_UCS2(BgL_tmpz00_8011);
					}
					{	/* Llib/unicode.scm 137 */

						return make_ucs2_string((int) (0L), BgL_fillerz00_1315);
				}}
			else
				{	/* Llib/unicode.scm 391 */
					BGL_TAIL return BGl_loopze73ze7zz__unicodez00(BgL_listz00_52);
				}
		}

	}



/* loop~3 */
	obj_t BGl_loopze73ze7zz__unicodez00(obj_t BgL_listz00_1317)
	{
		{	/* Llib/unicode.scm 393 */
			if (NULLP(CDR(((obj_t) BgL_listz00_1317))))
				{	/* Llib/unicode.scm 394 */
					return CAR(((obj_t) BgL_listz00_1317));
				}
			else
				{	/* Llib/unicode.scm 396 */
					obj_t BgL_arg1327z00_1321;
					obj_t BgL_arg1328z00_1322;

					BgL_arg1327z00_1321 = CAR(((obj_t) BgL_listz00_1317));
					{	/* Llib/unicode.scm 396 */
						obj_t BgL_arg1329z00_1323;

						BgL_arg1329z00_1323 = CDR(((obj_t) BgL_listz00_1317));
						BgL_arg1328z00_1322 =
							BGl_loopze73ze7zz__unicodez00(BgL_arg1329z00_1323);
					}
					return ucs2_string_append(BgL_arg1327z00_1321, BgL_arg1328z00_1322);
				}
		}

	}



/* &ucs2-string-append */
	obj_t BGl_z62ucs2zd2stringzd2appendz62zz__unicodez00(obj_t BgL_envz00_6923,
		obj_t BgL_listz00_6924)
	{
		{	/* Llib/unicode.scm 390 */
			return BGl_ucs2zd2stringzd2appendz00zz__unicodez00(BgL_listz00_6924);
		}

	}



/* list->ucs2-string */
	BGL_EXPORTED_DEF obj_t BGl_listzd2ze3ucs2zd2stringze3zz__unicodez00(obj_t
		BgL_listz00_53)
	{
		{	/* Llib/unicode.scm 401 */
			{	/* Llib/unicode.scm 402 */
				long BgL_lenz00_1326;

				BgL_lenz00_1326 = bgl_list_length(BgL_listz00_53);
				{	/* Llib/unicode.scm 402 */
					obj_t BgL_ucs2zd2stringzd2_1327;

					{	/* Llib/unicode.scm 137 */
						ucs2_t BgL_fillerz00_1338;

						{	/* Llib/unicode.scm 137 */
							int BgL_tmpz00_8034;

							BgL_tmpz00_8034 =
								(int) (((unsigned char) ((char) (((unsigned char) ' ')))));
							BgL_fillerz00_1338 = BGL_INT_TO_UCS2(BgL_tmpz00_8034);
						}
						{	/* Llib/unicode.scm 137 */

							BgL_ucs2zd2stringzd2_1327 =
								make_ucs2_string((int) (BgL_lenz00_1326), BgL_fillerz00_1338);
					}}
					{	/* Llib/unicode.scm 403 */

						{
							long BgL_iz00_3621;
							obj_t BgL_lz00_3622;

							BgL_iz00_3621 = 0L;
							BgL_lz00_3622 = BgL_listz00_53;
						BgL_loopz00_3620:
							if ((BgL_iz00_3621 == BgL_lenz00_1326))
								{	/* Llib/unicode.scm 406 */
									return BgL_ucs2zd2stringzd2_1327;
								}
							else
								{	/* Llib/unicode.scm 406 */
									{	/* Llib/unicode.scm 409 */
										obj_t BgL_arg1334z00_3628;

										BgL_arg1334z00_3628 = CAR(((obj_t) BgL_lz00_3622));
										{	/* Llib/unicode.scm 409 */
											int BgL_kz00_3635;
											ucs2_t BgL_ucs2z00_3636;

											BgL_kz00_3635 = (int) (BgL_iz00_3621);
											BgL_ucs2z00_3636 = CUCS2(BgL_arg1334z00_3628);
											{	/* Llib/unicode.scm 264 */
												bool_t BgL_test3961z00_8048;

												{	/* Llib/unicode.scm 264 */
													long BgL_auxz00_8051;
													long BgL_tmpz00_8049;

													BgL_auxz00_8051 =
														(long) (UCS2_STRING_LENGTH
														(BgL_ucs2zd2stringzd2_1327));
													BgL_tmpz00_8049 = (long) (BgL_kz00_3635);
													BgL_test3961z00_8048 =
														BOUND_CHECK(BgL_tmpz00_8049, BgL_auxz00_8051);
												}
												if (BgL_test3961z00_8048)
													{	/* Llib/unicode.scm 264 */
														UCS2_STRING_SET(BgL_ucs2zd2stringzd2_1327,
															BgL_kz00_3635, BgL_ucs2z00_3636);
													}
												else
													{	/* Llib/unicode.scm 269 */
														obj_t BgL_arg1233z00_3639;

														{	/* Llib/unicode.scm 269 */
															obj_t BgL_arg1234z00_3640;

															{	/* Llib/unicode.scm 269 */

																BgL_arg1234z00_3640 =
																	BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
																	(((long) (UCS2_STRING_LENGTH
																			(BgL_ucs2zd2stringzd2_1327)) - 1L), 10L);
															}
															BgL_arg1233z00_3639 =
																string_append_3(BGl_string3624z00zz__unicodez00,
																BgL_arg1234z00_3640,
																BGl_string3625z00zz__unicodez00);
														}
														BGl_errorz00zz__errorz00
															(BGl_symbol3629z00zz__unicodez00,
															BgL_arg1233z00_3639, BINT(BgL_kz00_3635));
									}}}}
									{	/* Llib/unicode.scm 410 */
										long BgL_arg1335z00_3629;
										obj_t BgL_arg1336z00_3630;

										BgL_arg1335z00_3629 = (BgL_iz00_3621 + 1L);
										BgL_arg1336z00_3630 = CDR(((obj_t) BgL_lz00_3622));
										{
											obj_t BgL_lz00_8067;
											long BgL_iz00_8066;

											BgL_iz00_8066 = BgL_arg1335z00_3629;
											BgL_lz00_8067 = BgL_arg1336z00_3630;
											BgL_lz00_3622 = BgL_lz00_8067;
											BgL_iz00_3621 = BgL_iz00_8066;
											goto BgL_loopz00_3620;
										}
									}
								}
						}
					}
				}
			}
		}

	}



/* &list->ucs2-string */
	obj_t BGl_z62listzd2ze3ucs2zd2stringz81zz__unicodez00(obj_t BgL_envz00_6925,
		obj_t BgL_listz00_6926)
	{
		{	/* Llib/unicode.scm 401 */
			return BGl_listzd2ze3ucs2zd2stringze3zz__unicodez00(BgL_listz00_6926);
		}

	}



/* ucs2-string->list */
	BGL_EXPORTED_DEF obj_t BGl_ucs2zd2stringzd2ze3listze3zz__unicodez00(obj_t
		BgL_ucs2zd2stringzd2_54)
	{
		{	/* Llib/unicode.scm 415 */
			{	/* Llib/unicode.scm 416 */
				int BgL_lenz00_1339;

				BgL_lenz00_1339 = UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_54);
				{
					long BgL_iz00_3694;
					obj_t BgL_accz00_3695;

					BgL_iz00_3694 = 0L;
					BgL_accz00_3695 = BNIL;
				BgL_loopz00_3693:
					if ((BgL_iz00_3694 == (long) (BgL_lenz00_1339)))
						{	/* Llib/unicode.scm 419 */
							return bgl_reverse_bang(BgL_accz00_3695);
						}
					else
						{	/* Llib/unicode.scm 421 */
							long BgL_arg1339z00_3701;
							obj_t BgL_arg1340z00_3702;

							BgL_arg1339z00_3701 = (BgL_iz00_3694 + 1L);
							{	/* Llib/unicode.scm 422 */
								ucs2_t BgL_arg1341z00_3703;

								{	/* Llib/unicode.scm 422 */
									ucs2_t BgL_res3308z00_3720;

									{	/* Llib/unicode.scm 422 */
										int BgL_kz00_3708;

										BgL_kz00_3708 = (int) (BgL_iz00_3694);
										{	/* Llib/unicode.scm 251 */
											bool_t BgL_test3963z00_8076;

											{	/* Llib/unicode.scm 251 */
												long BgL_auxz00_8079;
												long BgL_tmpz00_8077;

												BgL_auxz00_8079 =
													(long) (UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_54));
												BgL_tmpz00_8077 = (long) (BgL_kz00_3708);
												BgL_test3963z00_8076 =
													BOUND_CHECK(BgL_tmpz00_8077, BgL_auxz00_8079);
											}
											if (BgL_test3963z00_8076)
												{	/* Llib/unicode.scm 251 */
													BgL_res3308z00_3720 =
														UCS2_STRING_REF(BgL_ucs2zd2stringzd2_54,
														BgL_kz00_3708);
												}
											else
												{	/* Llib/unicode.scm 256 */
													obj_t BgL_arg1225z00_3711;

													{	/* Llib/unicode.scm 256 */
														obj_t BgL_arg1226z00_3712;

														{	/* Llib/unicode.scm 256 */

															BgL_arg1226z00_3712 =
																BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
																(((long) (UCS2_STRING_LENGTH
																		(BgL_ucs2zd2stringzd2_54)) - 1L), 10L);
														}
														BgL_arg1225z00_3711 =
															string_append_3(BGl_string3624z00zz__unicodez00,
															BgL_arg1226z00_3712,
															BGl_string3625z00zz__unicodez00);
													}
													BgL_res3308z00_3720 =
														CUCS2(BGl_errorz00zz__errorz00
														(BGl_symbol3626z00zz__unicodez00,
															BgL_arg1225z00_3711, BINT(BgL_kz00_3708)));
									}}}
									BgL_arg1341z00_3703 = BgL_res3308z00_3720;
								}
								BgL_arg1340z00_3702 =
									MAKE_YOUNG_PAIR(BUCS2(BgL_arg1341z00_3703), BgL_accz00_3695);
							}
							{
								obj_t BgL_accz00_8095;
								long BgL_iz00_8094;

								BgL_iz00_8094 = BgL_arg1339z00_3701;
								BgL_accz00_8095 = BgL_arg1340z00_3702;
								BgL_accz00_3695 = BgL_accz00_8095;
								BgL_iz00_3694 = BgL_iz00_8094;
								goto BgL_loopz00_3693;
							}
						}
				}
			}
		}

	}



/* &ucs2-string->list */
	obj_t BGl_z62ucs2zd2stringzd2ze3listz81zz__unicodez00(obj_t BgL_envz00_6927,
		obj_t BgL_ucs2zd2stringzd2_6928)
	{
		{	/* Llib/unicode.scm 415 */
			{	/* Llib/unicode.scm 416 */
				obj_t BgL_auxz00_8096;

				if (UCS2_STRINGP(BgL_ucs2zd2stringzd2_6928))
					{	/* Llib/unicode.scm 416 */
						BgL_auxz00_8096 = BgL_ucs2zd2stringzd2_6928;
					}
				else
					{
						obj_t BgL_auxz00_8099;

						BgL_auxz00_8099 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
							BINT(19272L), BGl_string3650z00zz__unicodez00,
							BGl_string3623z00zz__unicodez00, BgL_ucs2zd2stringzd2_6928);
						FAILURE(BgL_auxz00_8099, BFALSE, BFALSE);
					}
				return BGl_ucs2zd2stringzd2ze3listze3zz__unicodez00(BgL_auxz00_8096);
			}
		}

	}



/* ucs2-string-copy */
	BGL_EXPORTED_DEF obj_t BGl_ucs2zd2stringzd2copyz00zz__unicodez00(obj_t
		BgL_ucs2zd2stringzd2_55)
	{
		{	/* Llib/unicode.scm 428 */
			BGL_TAIL return c_ucs2_string_copy(BgL_ucs2zd2stringzd2_55);
		}

	}



/* &ucs2-string-copy */
	obj_t BGl_z62ucs2zd2stringzd2copyz62zz__unicodez00(obj_t BgL_envz00_6929,
		obj_t BgL_ucs2zd2stringzd2_6930)
	{
		{	/* Llib/unicode.scm 428 */
			{	/* Llib/unicode.scm 429 */
				obj_t BgL_auxz00_8105;

				if (UCS2_STRINGP(BgL_ucs2zd2stringzd2_6930))
					{	/* Llib/unicode.scm 429 */
						BgL_auxz00_8105 = BgL_ucs2zd2stringzd2_6930;
					}
				else
					{
						obj_t BgL_auxz00_8108;

						BgL_auxz00_8108 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
							BINT(19747L), BGl_string3651z00zz__unicodez00,
							BGl_string3623z00zz__unicodez00, BgL_ucs2zd2stringzd2_6930);
						FAILURE(BgL_auxz00_8108, BFALSE, BFALSE);
					}
				return BGl_ucs2zd2stringzd2copyz00zz__unicodez00(BgL_auxz00_8105);
			}
		}

	}



/* ucs2-string-fill! */
	BGL_EXPORTED_DEF obj_t BGl_ucs2zd2stringzd2fillz12z12zz__unicodez00(obj_t
		BgL_ucs2zd2stringzd2_56, ucs2_t BgL_ucs2z00_57)
	{
		{	/* Llib/unicode.scm 434 */
			{	/* Llib/unicode.scm 435 */
				int BgL_lenz00_3721;

				BgL_lenz00_3721 = UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_56);
				{
					long BgL_iz00_3766;

					BgL_iz00_3766 = 0L;
				BgL_loopz00_3765:
					if ((BgL_iz00_3766 == (long) (BgL_lenz00_3721)))
						{	/* Llib/unicode.scm 437 */
							return BgL_ucs2zd2stringzd2_56;
						}
					else
						{	/* Llib/unicode.scm 437 */
							{	/* Llib/unicode.scm 440 */
								int BgL_kz00_3774;

								BgL_kz00_3774 = (int) (BgL_iz00_3766);
								{	/* Llib/unicode.scm 264 */
									bool_t BgL_test3967z00_8118;

									{	/* Llib/unicode.scm 264 */
										long BgL_auxz00_8121;
										long BgL_tmpz00_8119;

										BgL_auxz00_8121 =
											(long) (UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_56));
										BgL_tmpz00_8119 = (long) (BgL_kz00_3774);
										BgL_test3967z00_8118 =
											BOUND_CHECK(BgL_tmpz00_8119, BgL_auxz00_8121);
									}
									if (BgL_test3967z00_8118)
										{	/* Llib/unicode.scm 264 */
											UCS2_STRING_SET(BgL_ucs2zd2stringzd2_56, BgL_kz00_3774,
												BgL_ucs2z00_57);
										}
									else
										{	/* Llib/unicode.scm 269 */
											obj_t BgL_arg1233z00_3778;

											{	/* Llib/unicode.scm 269 */
												obj_t BgL_arg1234z00_3779;

												{	/* Llib/unicode.scm 269 */

													BgL_arg1234z00_3779 =
														BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
														(((long) (UCS2_STRING_LENGTH
																(BgL_ucs2zd2stringzd2_56)) - 1L), 10L);
												}
												BgL_arg1233z00_3778 =
													string_append_3(BGl_string3624z00zz__unicodez00,
													BgL_arg1234z00_3779, BGl_string3625z00zz__unicodez00);
											}
											BGl_errorz00zz__errorz00(BGl_symbol3629z00zz__unicodez00,
												BgL_arg1233z00_3778, BINT(BgL_kz00_3774));
							}}}
							{
								long BgL_iz00_8133;

								BgL_iz00_8133 = (BgL_iz00_3766 + 1L);
								BgL_iz00_3766 = BgL_iz00_8133;
								goto BgL_loopz00_3765;
							}
						}
				}
			}
		}

	}



/* &ucs2-string-fill! */
	obj_t BGl_z62ucs2zd2stringzd2fillz12z70zz__unicodez00(obj_t BgL_envz00_6931,
		obj_t BgL_ucs2zd2stringzd2_6932, obj_t BgL_ucs2z00_6933)
	{
		{	/* Llib/unicode.scm 434 */
			{	/* Llib/unicode.scm 435 */
				ucs2_t BgL_auxz00_8142;
				obj_t BgL_auxz00_8135;

				{	/* Llib/unicode.scm 435 */
					obj_t BgL_tmpz00_8143;

					if (UCS2P(BgL_ucs2z00_6933))
						{	/* Llib/unicode.scm 435 */
							BgL_tmpz00_8143 = BgL_ucs2z00_6933;
						}
					else
						{
							obj_t BgL_auxz00_8146;

							BgL_auxz00_8146 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(20052L), BGl_string3652z00zz__unicodez00,
								BGl_string3621z00zz__unicodez00, BgL_ucs2z00_6933);
							FAILURE(BgL_auxz00_8146, BFALSE, BFALSE);
						}
					BgL_auxz00_8142 = CUCS2(BgL_tmpz00_8143);
				}
				if (UCS2_STRINGP(BgL_ucs2zd2stringzd2_6932))
					{	/* Llib/unicode.scm 435 */
						BgL_auxz00_8135 = BgL_ucs2zd2stringzd2_6932;
					}
				else
					{
						obj_t BgL_auxz00_8138;

						BgL_auxz00_8138 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
							BINT(20052L), BGl_string3652z00zz__unicodez00,
							BGl_string3623z00zz__unicodez00, BgL_ucs2zd2stringzd2_6932);
						FAILURE(BgL_auxz00_8138, BFALSE, BFALSE);
					}
				return
					BGl_ucs2zd2stringzd2fillz12z12zz__unicodez00(BgL_auxz00_8135,
					BgL_auxz00_8142);
			}
		}

	}



/* ucs2-string-upcase */
	BGL_EXPORTED_DEF obj_t BGl_ucs2zd2stringzd2upcasez00zz__unicodez00(obj_t
		BgL_ucs2zd2stringzd2_58)
	{
		{	/* Llib/unicode.scm 446 */
			{	/* Llib/unicode.scm 447 */
				int BgL_lenz00_1357;

				BgL_lenz00_1357 = UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_58);
				{	/* Llib/unicode.scm 447 */
					obj_t BgL_resz00_1358;

					{	/* Llib/unicode.scm 137 */
						ucs2_t BgL_fillerz00_1368;

						{	/* Llib/unicode.scm 137 */
							int BgL_tmpz00_8153;

							BgL_tmpz00_8153 =
								(int) (((unsigned char) ((char) (((unsigned char) ' ')))));
							BgL_fillerz00_1368 = BGL_INT_TO_UCS2(BgL_tmpz00_8153);
						}
						{	/* Llib/unicode.scm 137 */

							BgL_resz00_1358 =
								make_ucs2_string(BgL_lenz00_1357, BgL_fillerz00_1368);
					}}
					{	/* Llib/unicode.scm 448 */

						{
							long BgL_iz00_3828;

							BgL_iz00_3828 = 0L;
						BgL_loopz00_3827:
							if ((BgL_iz00_3828 == (long) (BgL_lenz00_1357)))
								{	/* Llib/unicode.scm 450 */
									return BgL_resz00_1358;
								}
							else
								{	/* Llib/unicode.scm 450 */
									{	/* Llib/unicode.scm 454 */
										ucs2_t BgL_arg1347z00_3832;

										{	/* Llib/unicode.scm 454 */
											ucs2_t BgL_arg1348z00_3833;

											{	/* Llib/unicode.scm 454 */
												ucs2_t BgL_res3309z00_3834;

												{	/* Llib/unicode.scm 454 */
													int BgL_kz00_3836;

													BgL_kz00_3836 = (int) (BgL_iz00_3828);
													{	/* Llib/unicode.scm 251 */
														bool_t BgL_test3971z00_8164;

														{	/* Llib/unicode.scm 251 */
															long BgL_auxz00_8167;
															long BgL_tmpz00_8165;

															BgL_auxz00_8167 =
																(long) (UCS2_STRING_LENGTH
																(BgL_ucs2zd2stringzd2_58));
															BgL_tmpz00_8165 = (long) (BgL_kz00_3836);
															BgL_test3971z00_8164 =
																BOUND_CHECK(BgL_tmpz00_8165, BgL_auxz00_8167);
														}
														if (BgL_test3971z00_8164)
															{	/* Llib/unicode.scm 251 */
																BgL_res3309z00_3834 =
																	UCS2_STRING_REF(BgL_ucs2zd2stringzd2_58,
																	BgL_kz00_3836);
															}
														else
															{	/* Llib/unicode.scm 256 */
																obj_t BgL_arg1225z00_3840;

																{	/* Llib/unicode.scm 256 */
																	obj_t BgL_arg1226z00_3841;

																	{	/* Llib/unicode.scm 256 */

																		BgL_arg1226z00_3841 =
																			BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
																			(((long) (UCS2_STRING_LENGTH
																					(BgL_ucs2zd2stringzd2_58)) - 1L),
																			10L);
																	}
																	BgL_arg1225z00_3840 =
																		string_append_3
																		(BGl_string3624z00zz__unicodez00,
																		BgL_arg1226z00_3841,
																		BGl_string3625z00zz__unicodez00);
																}
																BgL_res3309z00_3834 =
																	CUCS2(BGl_errorz00zz__errorz00
																	(BGl_symbol3626z00zz__unicodez00,
																		BgL_arg1225z00_3840, BINT(BgL_kz00_3836)));
												}}}
												BgL_arg1348z00_3833 = BgL_res3309z00_3834;
											}
											BgL_arg1347z00_3832 = ucs2_toupper(BgL_arg1348z00_3833);
										}
										{	/* Llib/unicode.scm 453 */
											int BgL_kz00_3850;

											BgL_kz00_3850 = (int) (BgL_iz00_3828);
											{	/* Llib/unicode.scm 264 */
												bool_t BgL_test3972z00_8182;

												{	/* Llib/unicode.scm 264 */
													long BgL_auxz00_8185;
													long BgL_tmpz00_8183;

													BgL_auxz00_8185 =
														(long) (UCS2_STRING_LENGTH(BgL_resz00_1358));
													BgL_tmpz00_8183 = (long) (BgL_kz00_3850);
													BgL_test3972z00_8182 =
														BOUND_CHECK(BgL_tmpz00_8183, BgL_auxz00_8185);
												}
												if (BgL_test3972z00_8182)
													{	/* Llib/unicode.scm 264 */
														UCS2_STRING_SET(BgL_resz00_1358, BgL_kz00_3850,
															BgL_arg1347z00_3832);
													}
												else
													{	/* Llib/unicode.scm 269 */
														obj_t BgL_arg1233z00_3855;

														{	/* Llib/unicode.scm 269 */
															obj_t BgL_arg1234z00_3856;

															{	/* Llib/unicode.scm 269 */

																BgL_arg1234z00_3856 =
																	BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
																	(((long) (UCS2_STRING_LENGTH(BgL_resz00_1358))
																		- 1L), 10L);
															}
															BgL_arg1233z00_3855 =
																string_append_3(BGl_string3624z00zz__unicodez00,
																BgL_arg1234z00_3856,
																BGl_string3625z00zz__unicodez00);
														}
														BGl_errorz00zz__errorz00
															(BGl_symbol3629z00zz__unicodez00,
															BgL_arg1233z00_3855, BINT(BgL_kz00_3850));
									}}}}
									{
										long BgL_iz00_8197;

										BgL_iz00_8197 = (BgL_iz00_3828 + 1L);
										BgL_iz00_3828 = BgL_iz00_8197;
										goto BgL_loopz00_3827;
									}
								}
						}
					}
				}
			}
		}

	}



/* &ucs2-string-upcase */
	obj_t BGl_z62ucs2zd2stringzd2upcasez62zz__unicodez00(obj_t BgL_envz00_6934,
		obj_t BgL_ucs2zd2stringzd2_6935)
	{
		{	/* Llib/unicode.scm 446 */
			{	/* Llib/unicode.scm 447 */
				obj_t BgL_auxz00_8199;

				if (UCS2_STRINGP(BgL_ucs2zd2stringzd2_6935))
					{	/* Llib/unicode.scm 447 */
						BgL_auxz00_8199 = BgL_ucs2zd2stringzd2_6935;
					}
				else
					{
						obj_t BgL_auxz00_8202;

						BgL_auxz00_8202 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
							BINT(20502L), BGl_string3653z00zz__unicodez00,
							BGl_string3623z00zz__unicodez00, BgL_ucs2zd2stringzd2_6935);
						FAILURE(BgL_auxz00_8202, BFALSE, BFALSE);
					}
				return BGl_ucs2zd2stringzd2upcasez00zz__unicodez00(BgL_auxz00_8199);
			}
		}

	}



/* ucs2-string-downcase */
	BGL_EXPORTED_DEF obj_t BGl_ucs2zd2stringzd2downcasez00zz__unicodez00(obj_t
		BgL_ucs2zd2stringzd2_59)
	{
		{	/* Llib/unicode.scm 460 */
			{	/* Llib/unicode.scm 461 */
				int BgL_lenz00_1369;

				BgL_lenz00_1369 = UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_59);
				{	/* Llib/unicode.scm 461 */
					obj_t BgL_resz00_1370;

					{	/* Llib/unicode.scm 137 */
						ucs2_t BgL_fillerz00_1380;

						{	/* Llib/unicode.scm 137 */
							int BgL_tmpz00_8208;

							BgL_tmpz00_8208 =
								(int) (((unsigned char) ((char) (((unsigned char) ' ')))));
							BgL_fillerz00_1380 = BGL_INT_TO_UCS2(BgL_tmpz00_8208);
						}
						{	/* Llib/unicode.scm 137 */

							BgL_resz00_1370 =
								make_ucs2_string(BgL_lenz00_1369, BgL_fillerz00_1380);
					}}
					{	/* Llib/unicode.scm 462 */

						{
							long BgL_iz00_3905;

							BgL_iz00_3905 = 0L;
						BgL_loopz00_3904:
							if ((BgL_iz00_3905 == (long) (BgL_lenz00_1369)))
								{	/* Llib/unicode.scm 464 */
									return BgL_resz00_1370;
								}
							else
								{	/* Llib/unicode.scm 464 */
									{	/* Llib/unicode.scm 468 */
										ucs2_t BgL_arg1352z00_3909;

										{	/* Llib/unicode.scm 468 */
											ucs2_t BgL_arg1354z00_3910;

											{	/* Llib/unicode.scm 468 */
												ucs2_t BgL_res3310z00_3911;

												{	/* Llib/unicode.scm 468 */
													int BgL_kz00_3913;

													BgL_kz00_3913 = (int) (BgL_iz00_3905);
													{	/* Llib/unicode.scm 251 */
														bool_t BgL_test3975z00_8219;

														{	/* Llib/unicode.scm 251 */
															long BgL_auxz00_8222;
															long BgL_tmpz00_8220;

															BgL_auxz00_8222 =
																(long) (UCS2_STRING_LENGTH
																(BgL_ucs2zd2stringzd2_59));
															BgL_tmpz00_8220 = (long) (BgL_kz00_3913);
															BgL_test3975z00_8219 =
																BOUND_CHECK(BgL_tmpz00_8220, BgL_auxz00_8222);
														}
														if (BgL_test3975z00_8219)
															{	/* Llib/unicode.scm 251 */
																BgL_res3310z00_3911 =
																	UCS2_STRING_REF(BgL_ucs2zd2stringzd2_59,
																	BgL_kz00_3913);
															}
														else
															{	/* Llib/unicode.scm 256 */
																obj_t BgL_arg1225z00_3917;

																{	/* Llib/unicode.scm 256 */
																	obj_t BgL_arg1226z00_3918;

																	{	/* Llib/unicode.scm 256 */

																		BgL_arg1226z00_3918 =
																			BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
																			(((long) (UCS2_STRING_LENGTH
																					(BgL_ucs2zd2stringzd2_59)) - 1L),
																			10L);
																	}
																	BgL_arg1225z00_3917 =
																		string_append_3
																		(BGl_string3624z00zz__unicodez00,
																		BgL_arg1226z00_3918,
																		BGl_string3625z00zz__unicodez00);
																}
																BgL_res3310z00_3911 =
																	CUCS2(BGl_errorz00zz__errorz00
																	(BGl_symbol3626z00zz__unicodez00,
																		BgL_arg1225z00_3917, BINT(BgL_kz00_3913)));
												}}}
												BgL_arg1354z00_3910 = BgL_res3310z00_3911;
											}
											BgL_arg1352z00_3909 = ucs2_tolower(BgL_arg1354z00_3910);
										}
										{	/* Llib/unicode.scm 467 */
											int BgL_kz00_3927;

											BgL_kz00_3927 = (int) (BgL_iz00_3905);
											{	/* Llib/unicode.scm 264 */
												bool_t BgL_test3976z00_8237;

												{	/* Llib/unicode.scm 264 */
													long BgL_auxz00_8240;
													long BgL_tmpz00_8238;

													BgL_auxz00_8240 =
														(long) (UCS2_STRING_LENGTH(BgL_resz00_1370));
													BgL_tmpz00_8238 = (long) (BgL_kz00_3927);
													BgL_test3976z00_8237 =
														BOUND_CHECK(BgL_tmpz00_8238, BgL_auxz00_8240);
												}
												if (BgL_test3976z00_8237)
													{	/* Llib/unicode.scm 264 */
														UCS2_STRING_SET(BgL_resz00_1370, BgL_kz00_3927,
															BgL_arg1352z00_3909);
													}
												else
													{	/* Llib/unicode.scm 269 */
														obj_t BgL_arg1233z00_3932;

														{	/* Llib/unicode.scm 269 */
															obj_t BgL_arg1234z00_3933;

															{	/* Llib/unicode.scm 269 */

																BgL_arg1234z00_3933 =
																	BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
																	(((long) (UCS2_STRING_LENGTH(BgL_resz00_1370))
																		- 1L), 10L);
															}
															BgL_arg1233z00_3932 =
																string_append_3(BGl_string3624z00zz__unicodez00,
																BgL_arg1234z00_3933,
																BGl_string3625z00zz__unicodez00);
														}
														BGl_errorz00zz__errorz00
															(BGl_symbol3629z00zz__unicodez00,
															BgL_arg1233z00_3932, BINT(BgL_kz00_3927));
									}}}}
									{
										long BgL_iz00_8252;

										BgL_iz00_8252 = (BgL_iz00_3905 + 1L);
										BgL_iz00_3905 = BgL_iz00_8252;
										goto BgL_loopz00_3904;
									}
								}
						}
					}
				}
			}
		}

	}



/* &ucs2-string-downcase */
	obj_t BGl_z62ucs2zd2stringzd2downcasez62zz__unicodez00(obj_t BgL_envz00_6936,
		obj_t BgL_ucs2zd2stringzd2_6937)
	{
		{	/* Llib/unicode.scm 460 */
			{	/* Llib/unicode.scm 461 */
				obj_t BgL_auxz00_8254;

				if (UCS2_STRINGP(BgL_ucs2zd2stringzd2_6937))
					{	/* Llib/unicode.scm 461 */
						BgL_auxz00_8254 = BgL_ucs2zd2stringzd2_6937;
					}
				else
					{
						obj_t BgL_auxz00_8257;

						BgL_auxz00_8257 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
							BINT(21017L), BGl_string3654z00zz__unicodez00,
							BGl_string3623z00zz__unicodez00, BgL_ucs2zd2stringzd2_6937);
						FAILURE(BgL_auxz00_8257, BFALSE, BFALSE);
					}
				return BGl_ucs2zd2stringzd2downcasez00zz__unicodez00(BgL_auxz00_8254);
			}
		}

	}



/* ucs2-string-upcase! */
	BGL_EXPORTED_DEF obj_t BGl_ucs2zd2stringzd2upcasez12z12zz__unicodez00(obj_t
		BgL_ucs2zd2stringzd2_60)
	{
		{	/* Llib/unicode.scm 474 */
			{	/* Llib/unicode.scm 475 */
				int BgL_lenz00_1381;

				BgL_lenz00_1381 = UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_60);
				{	/* Llib/unicode.scm 476 */

					{
						long BgL_iz00_3976;

						BgL_iz00_3976 = 0L;
					BgL_loopz00_3975:
						if ((BgL_iz00_3976 == (long) (BgL_lenz00_1381)))
							{	/* Llib/unicode.scm 478 */
								return BgL_ucs2zd2stringzd2_60;
							}
						else
							{	/* Llib/unicode.scm 478 */
								{	/* Llib/unicode.scm 482 */
									ucs2_t BgL_arg1359z00_3980;

									{	/* Llib/unicode.scm 482 */
										ucs2_t BgL_arg1360z00_3981;

										{	/* Llib/unicode.scm 482 */
											ucs2_t BgL_res3311z00_3982;

											{	/* Llib/unicode.scm 482 */
												int BgL_kz00_3984;

												BgL_kz00_3984 = (int) (BgL_iz00_3976);
												{	/* Llib/unicode.scm 251 */
													bool_t BgL_test3979z00_8267;

													{	/* Llib/unicode.scm 251 */
														long BgL_auxz00_8270;
														long BgL_tmpz00_8268;

														BgL_auxz00_8270 =
															(long) (UCS2_STRING_LENGTH
															(BgL_ucs2zd2stringzd2_60));
														BgL_tmpz00_8268 = (long) (BgL_kz00_3984);
														BgL_test3979z00_8267 =
															BOUND_CHECK(BgL_tmpz00_8268, BgL_auxz00_8270);
													}
													if (BgL_test3979z00_8267)
														{	/* Llib/unicode.scm 251 */
															BgL_res3311z00_3982 =
																UCS2_STRING_REF(BgL_ucs2zd2stringzd2_60,
																BgL_kz00_3984);
														}
													else
														{	/* Llib/unicode.scm 256 */
															obj_t BgL_arg1225z00_3988;

															{	/* Llib/unicode.scm 256 */
																obj_t BgL_arg1226z00_3989;

																{	/* Llib/unicode.scm 256 */

																	BgL_arg1226z00_3989 =
																		BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
																		(((long) (UCS2_STRING_LENGTH
																				(BgL_ucs2zd2stringzd2_60)) - 1L), 10L);
																}
																BgL_arg1225z00_3988 =
																	string_append_3
																	(BGl_string3624z00zz__unicodez00,
																	BgL_arg1226z00_3989,
																	BGl_string3625z00zz__unicodez00);
															}
															BgL_res3311z00_3982 =
																CUCS2(BGl_errorz00zz__errorz00
																(BGl_symbol3626z00zz__unicodez00,
																	BgL_arg1225z00_3988, BINT(BgL_kz00_3984)));
											}}}
											BgL_arg1360z00_3981 = BgL_res3311z00_3982;
										}
										BgL_arg1359z00_3980 = ucs2_toupper(BgL_arg1360z00_3981);
									}
									{	/* Llib/unicode.scm 481 */
										int BgL_kz00_3998;

										BgL_kz00_3998 = (int) (BgL_iz00_3976);
										{	/* Llib/unicode.scm 264 */
											bool_t BgL_test3980z00_8285;

											{	/* Llib/unicode.scm 264 */
												long BgL_auxz00_8288;
												long BgL_tmpz00_8286;

												BgL_auxz00_8288 =
													(long) (UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_60));
												BgL_tmpz00_8286 = (long) (BgL_kz00_3998);
												BgL_test3980z00_8285 =
													BOUND_CHECK(BgL_tmpz00_8286, BgL_auxz00_8288);
											}
											if (BgL_test3980z00_8285)
												{	/* Llib/unicode.scm 264 */
													UCS2_STRING_SET(BgL_ucs2zd2stringzd2_60,
														BgL_kz00_3998, BgL_arg1359z00_3980);
												}
											else
												{	/* Llib/unicode.scm 269 */
													obj_t BgL_arg1233z00_4003;

													{	/* Llib/unicode.scm 269 */
														obj_t BgL_arg1234z00_4004;

														{	/* Llib/unicode.scm 269 */

															BgL_arg1234z00_4004 =
																BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
																(((long) (UCS2_STRING_LENGTH
																		(BgL_ucs2zd2stringzd2_60)) - 1L), 10L);
														}
														BgL_arg1233z00_4003 =
															string_append_3(BGl_string3624z00zz__unicodez00,
															BgL_arg1234z00_4004,
															BGl_string3625z00zz__unicodez00);
													}
													BGl_errorz00zz__errorz00
														(BGl_symbol3629z00zz__unicodez00,
														BgL_arg1233z00_4003, BINT(BgL_kz00_3998));
								}}}}
								{
									long BgL_iz00_8300;

									BgL_iz00_8300 = (BgL_iz00_3976 + 1L);
									BgL_iz00_3976 = BgL_iz00_8300;
									goto BgL_loopz00_3975;
								}
							}
					}
				}
			}
		}

	}



/* &ucs2-string-upcase! */
	obj_t BGl_z62ucs2zd2stringzd2upcasez12z70zz__unicodez00(obj_t BgL_envz00_6938,
		obj_t BgL_ucs2zd2stringzd2_6939)
	{
		{	/* Llib/unicode.scm 474 */
			{	/* Llib/unicode.scm 475 */
				obj_t BgL_auxz00_8302;

				if (UCS2_STRINGP(BgL_ucs2zd2stringzd2_6939))
					{	/* Llib/unicode.scm 475 */
						BgL_auxz00_8302 = BgL_ucs2zd2stringzd2_6939;
					}
				else
					{
						obj_t BgL_auxz00_8305;

						BgL_auxz00_8305 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
							BINT(21533L), BGl_string3655z00zz__unicodez00,
							BGl_string3623z00zz__unicodez00, BgL_ucs2zd2stringzd2_6939);
						FAILURE(BgL_auxz00_8305, BFALSE, BFALSE);
					}
				return BGl_ucs2zd2stringzd2upcasez12z12zz__unicodez00(BgL_auxz00_8302);
			}
		}

	}



/* ucs2-string-downcase! */
	BGL_EXPORTED_DEF obj_t BGl_ucs2zd2stringzd2downcasez12z12zz__unicodez00(obj_t
		BgL_ucs2zd2stringzd2_61)
	{
		{	/* Llib/unicode.scm 488 */
			{	/* Llib/unicode.scm 489 */
				int BgL_lenz00_1391;

				BgL_lenz00_1391 = UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_61);
				{	/* Llib/unicode.scm 490 */

					{
						long BgL_iz00_4047;

						BgL_iz00_4047 = 0L;
					BgL_loopz00_4046:
						if ((BgL_iz00_4047 == (long) (BgL_lenz00_1391)))
							{	/* Llib/unicode.scm 492 */
								return BgL_ucs2zd2stringzd2_61;
							}
						else
							{	/* Llib/unicode.scm 492 */
								{	/* Llib/unicode.scm 496 */
									ucs2_t BgL_arg1364z00_4051;

									{	/* Llib/unicode.scm 496 */
										ucs2_t BgL_arg1365z00_4052;

										{	/* Llib/unicode.scm 496 */
											ucs2_t BgL_res3312z00_4053;

											{	/* Llib/unicode.scm 496 */
												int BgL_kz00_4055;

												BgL_kz00_4055 = (int) (BgL_iz00_4047);
												{	/* Llib/unicode.scm 251 */
													bool_t BgL_test3983z00_8315;

													{	/* Llib/unicode.scm 251 */
														long BgL_auxz00_8318;
														long BgL_tmpz00_8316;

														BgL_auxz00_8318 =
															(long) (UCS2_STRING_LENGTH
															(BgL_ucs2zd2stringzd2_61));
														BgL_tmpz00_8316 = (long) (BgL_kz00_4055);
														BgL_test3983z00_8315 =
															BOUND_CHECK(BgL_tmpz00_8316, BgL_auxz00_8318);
													}
													if (BgL_test3983z00_8315)
														{	/* Llib/unicode.scm 251 */
															BgL_res3312z00_4053 =
																UCS2_STRING_REF(BgL_ucs2zd2stringzd2_61,
																BgL_kz00_4055);
														}
													else
														{	/* Llib/unicode.scm 256 */
															obj_t BgL_arg1225z00_4059;

															{	/* Llib/unicode.scm 256 */
																obj_t BgL_arg1226z00_4060;

																{	/* Llib/unicode.scm 256 */

																	BgL_arg1226z00_4060 =
																		BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
																		(((long) (UCS2_STRING_LENGTH
																				(BgL_ucs2zd2stringzd2_61)) - 1L), 10L);
																}
																BgL_arg1225z00_4059 =
																	string_append_3
																	(BGl_string3624z00zz__unicodez00,
																	BgL_arg1226z00_4060,
																	BGl_string3625z00zz__unicodez00);
															}
															BgL_res3312z00_4053 =
																CUCS2(BGl_errorz00zz__errorz00
																(BGl_symbol3626z00zz__unicodez00,
																	BgL_arg1225z00_4059, BINT(BgL_kz00_4055)));
											}}}
											BgL_arg1365z00_4052 = BgL_res3312z00_4053;
										}
										BgL_arg1364z00_4051 = ucs2_tolower(BgL_arg1365z00_4052);
									}
									{	/* Llib/unicode.scm 495 */
										int BgL_kz00_4069;

										BgL_kz00_4069 = (int) (BgL_iz00_4047);
										{	/* Llib/unicode.scm 264 */
											bool_t BgL_test3984z00_8333;

											{	/* Llib/unicode.scm 264 */
												long BgL_auxz00_8336;
												long BgL_tmpz00_8334;

												BgL_auxz00_8336 =
													(long) (UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_61));
												BgL_tmpz00_8334 = (long) (BgL_kz00_4069);
												BgL_test3984z00_8333 =
													BOUND_CHECK(BgL_tmpz00_8334, BgL_auxz00_8336);
											}
											if (BgL_test3984z00_8333)
												{	/* Llib/unicode.scm 264 */
													UCS2_STRING_SET(BgL_ucs2zd2stringzd2_61,
														BgL_kz00_4069, BgL_arg1364z00_4051);
												}
											else
												{	/* Llib/unicode.scm 269 */
													obj_t BgL_arg1233z00_4074;

													{	/* Llib/unicode.scm 269 */
														obj_t BgL_arg1234z00_4075;

														{	/* Llib/unicode.scm 269 */

															BgL_arg1234z00_4075 =
																BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
																(((long) (UCS2_STRING_LENGTH
																		(BgL_ucs2zd2stringzd2_61)) - 1L), 10L);
														}
														BgL_arg1233z00_4074 =
															string_append_3(BGl_string3624z00zz__unicodez00,
															BgL_arg1234z00_4075,
															BGl_string3625z00zz__unicodez00);
													}
													BGl_errorz00zz__errorz00
														(BGl_symbol3629z00zz__unicodez00,
														BgL_arg1233z00_4074, BINT(BgL_kz00_4069));
								}}}}
								{
									long BgL_iz00_8348;

									BgL_iz00_8348 = (BgL_iz00_4047 + 1L);
									BgL_iz00_4047 = BgL_iz00_8348;
									goto BgL_loopz00_4046;
								}
							}
					}
				}
			}
		}

	}



/* &ucs2-string-downcase! */
	obj_t BGl_z62ucs2zd2stringzd2downcasez12z70zz__unicodez00(obj_t
		BgL_envz00_6940, obj_t BgL_ucs2zd2stringzd2_6941)
	{
		{	/* Llib/unicode.scm 488 */
			{	/* Llib/unicode.scm 489 */
				obj_t BgL_auxz00_8350;

				if (UCS2_STRINGP(BgL_ucs2zd2stringzd2_6941))
					{	/* Llib/unicode.scm 489 */
						BgL_auxz00_8350 = BgL_ucs2zd2stringzd2_6941;
					}
				else
					{
						obj_t BgL_auxz00_8353;

						BgL_auxz00_8353 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
							BINT(22038L), BGl_string3656z00zz__unicodez00,
							BGl_string3623z00zz__unicodez00, BgL_ucs2zd2stringzd2_6941);
						FAILURE(BgL_auxz00_8353, BFALSE, BFALSE);
					}
				return
					BGl_ucs2zd2stringzd2downcasez12z12zz__unicodez00(BgL_auxz00_8350);
			}
		}

	}



/* ucs2-string->utf8-string */
	BGL_EXPORTED_DEF obj_t
		BGl_ucs2zd2stringzd2ze3utf8zd2stringz31zz__unicodez00(obj_t BgL_ucs2z00_62)
	{
		{	/* Llib/unicode.scm 502 */
			BGL_TAIL return ucs2_string_to_utf8_string(BgL_ucs2z00_62);
		}

	}



/* &ucs2-string->utf8-string */
	obj_t BGl_z62ucs2zd2stringzd2ze3utf8zd2stringz53zz__unicodez00(obj_t
		BgL_envz00_6942, obj_t BgL_ucs2z00_6943)
	{
		{	/* Llib/unicode.scm 502 */
			{	/* Llib/unicode.scm 503 */
				obj_t BgL_auxz00_8359;

				if (UCS2_STRINGP(BgL_ucs2z00_6943))
					{	/* Llib/unicode.scm 503 */
						BgL_auxz00_8359 = BgL_ucs2z00_6943;
					}
				else
					{
						obj_t BgL_auxz00_8362;

						BgL_auxz00_8362 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
							BINT(22554L), BGl_string3657z00zz__unicodez00,
							BGl_string3623z00zz__unicodez00, BgL_ucs2z00_6943);
						FAILURE(BgL_auxz00_8362, BFALSE, BFALSE);
					}
				return
					BGl_ucs2zd2stringzd2ze3utf8zd2stringz31zz__unicodez00
					(BgL_auxz00_8359);
			}
		}

	}



/* inverse-utf8-table */
	BGL_EXPORTED_DEF obj_t BGl_inversezd2utf8zd2tablez00zz__unicodez00(obj_t
		BgL_tablez00_63)
	{
		{	/* Llib/unicode.scm 611 */
			{
				long BgL_iz00_1406;
				obj_t BgL_resz00_1407;

				BgL_iz00_1406 = 0L;
				BgL_resz00_1407 = BNIL;
			BgL_zc3z04anonymousza31367ze3z87_1408:
				if ((BgL_iz00_1406 == VECTOR_LENGTH(BgL_tablez00_63)))
					{	/* Llib/unicode.scm 636 */
						return BgL_resz00_1407;
					}
				else
					{	/* Llib/unicode.scm 638 */
						obj_t BgL_sz00_1410;

						BgL_sz00_1410 = VECTOR_REF(BgL_tablez00_63, BgL_iz00_1406);
						if ((STRING_LENGTH(((obj_t) BgL_sz00_1410)) > 0L))
							{
								obj_t BgL_resz00_8377;
								long BgL_iz00_8375;

								BgL_iz00_8375 = (BgL_iz00_1406 + 1L);
								BgL_resz00_8377 =
									BGl_addzd2tablezd2entryze70ze7zz__unicodez00(BgL_resz00_1407,
									BgL_sz00_1410, (128L + BgL_iz00_1406));
								BgL_resz00_1407 = BgL_resz00_8377;
								BgL_iz00_1406 = BgL_iz00_8375;
								goto BgL_zc3z04anonymousza31367ze3z87_1408;
							}
						else
							{
								long BgL_iz00_8380;

								BgL_iz00_8380 = (BgL_iz00_1406 + 1L);
								BgL_iz00_1406 = BgL_iz00_8380;
								goto BgL_zc3z04anonymousza31367ze3z87_1408;
							}
					}
			}
		}

	}



/* loop~1 */
	obj_t BGl_loopze71ze7zz__unicodez00(long BgL_lenz00_7027,
		long BgL_charz00_7026, obj_t BgL_sz00_7025, long BgL_iz00_1424)
	{
		{	/* Llib/unicode.scm 615 */
			if ((BgL_lenz00_7027 == BgL_iz00_1424))
				{	/* Llib/unicode.scm 616 */
					return BCHAR((BgL_charz00_7026));
				}
			else
				{	/* Llib/unicode.scm 618 */
					obj_t BgL_arg1380z00_1427;

					{	/* Llib/unicode.scm 618 */
						long BgL_arg1382z00_1429;
						obj_t BgL_arg1383z00_1430;

						BgL_arg1382z00_1429 =
							(STRING_REF(((obj_t) BgL_sz00_7025), BgL_iz00_1424));
						BgL_arg1383z00_1430 =
							BGl_loopze71ze7zz__unicodez00(BgL_lenz00_7027, BgL_charz00_7026,
							BgL_sz00_7025, (BgL_iz00_1424 + 1L));
						BgL_arg1380z00_1427 =
							MAKE_YOUNG_PAIR(BINT(BgL_arg1382z00_1429), BgL_arg1383z00_1430);
					}
					{	/* Llib/unicode.scm 618 */
						obj_t BgL_list1381z00_1428;

						BgL_list1381z00_1428 = MAKE_YOUNG_PAIR(BgL_arg1380z00_1427, BNIL);
						return BgL_list1381z00_1428;
					}
				}
		}

	}



/* make-table-entry~0 */
	obj_t BGl_makezd2tablezd2entryze70ze7zz__unicodez00(obj_t BgL_sz00_1419,
		long BgL_charz00_1420)
	{
		{	/* Llib/unicode.scm 618 */
			{	/* Llib/unicode.scm 614 */
				long BgL_lenz00_1422;

				BgL_lenz00_1422 = STRING_LENGTH(((obj_t) BgL_sz00_1419));
				return
					BGl_loopze71ze7zz__unicodez00(BgL_lenz00_1422, BgL_charz00_1420,
					BgL_sz00_1419, 0L);
			}
		}

	}



/* loop~2 */
	obj_t BGl_loopze72ze7zz__unicodez00(obj_t BgL_ez00_1440,
		obj_t BgL_tablez00_1441)
	{
		{	/* Llib/unicode.scm 621 */
			if (NULLP(BgL_ez00_1440))
				{	/* Llib/unicode.scm 623 */
					return BgL_tablez00_1441;
				}
			else
				{	/* Llib/unicode.scm 625 */
					obj_t BgL_nz00_1444;

					BgL_nz00_1444 = CAR(((obj_t) BgL_ez00_1440));
					{	/* Llib/unicode.scm 625 */
						obj_t BgL_oz00_1445;

						BgL_oz00_1445 =
							BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_nz00_1444,
							BgL_tablez00_1441);
						{	/* Llib/unicode.scm 626 */

							if (CBOOL(BgL_oz00_1445))
								{	/* Llib/unicode.scm 628 */
									obj_t BgL_stz00_1446;

									BgL_stz00_1446 = CDR(((obj_t) BgL_oz00_1445));
									{	/* Llib/unicode.scm 629 */
										obj_t BgL_arg1391z00_1447;

										{	/* Llib/unicode.scm 629 */
											obj_t BgL_arg1392z00_1448;

											{	/* Llib/unicode.scm 629 */
												obj_t BgL_pairz00_4099;

												BgL_pairz00_4099 = CDR(((obj_t) BgL_ez00_1440));
												BgL_arg1392z00_1448 = CAR(BgL_pairz00_4099);
											}
											BgL_arg1391z00_1447 =
												BGl_loopze72ze7zz__unicodez00(BgL_arg1392z00_1448,
												BgL_stz00_1446);
										}
										{	/* Llib/unicode.scm 629 */
											obj_t BgL_tmpz00_8410;

											BgL_tmpz00_8410 = ((obj_t) BgL_oz00_1445);
											SET_CDR(BgL_tmpz00_8410, BgL_arg1391z00_1447);
										}
									}
									return BgL_tablez00_1441;
								}
							else
								{	/* Llib/unicode.scm 627 */
									return MAKE_YOUNG_PAIR(BgL_ez00_1440, BgL_tablez00_1441);
								}
						}
					}
				}
		}

	}



/* add-table-entry~0 */
	obj_t BGl_addzd2tablezd2entryze70ze7zz__unicodez00(obj_t BgL_tablez00_1434,
		obj_t BgL_sz00_1435, long BgL_charz00_1436)
	{
		{	/* Llib/unicode.scm 631 */
			{	/* Llib/unicode.scm 621 */
				obj_t BgL_g1042z00_1438;

				{	/* Llib/unicode.scm 621 */
					obj_t BgL_arg1393z00_1450;

					BgL_arg1393z00_1450 =
						BGl_makezd2tablezd2entryze70ze7zz__unicodez00(BgL_sz00_1435,
						BgL_charz00_1436);
					BgL_g1042z00_1438 = CAR(((obj_t) BgL_arg1393z00_1450));
				}
				return
					BGl_loopze72ze7zz__unicodez00(BgL_g1042z00_1438, BgL_tablez00_1434);
			}
		}

	}



/* &inverse-utf8-table */
	obj_t BGl_z62inversezd2utf8zd2tablez62zz__unicodez00(obj_t BgL_envz00_6944,
		obj_t BgL_tablez00_6945)
	{
		{	/* Llib/unicode.scm 611 */
			{	/* Llib/unicode.scm 614 */
				obj_t BgL_auxz00_8418;

				if (VECTORP(BgL_tablez00_6945))
					{	/* Llib/unicode.scm 614 */
						BgL_auxz00_8418 = BgL_tablez00_6945;
					}
				else
					{
						obj_t BgL_auxz00_8421;

						BgL_auxz00_8421 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
							BINT(25484L), BGl_string3658z00zz__unicodez00,
							BGl_string3659z00zz__unicodez00, BgL_tablez00_6945);
						FAILURE(BgL_auxz00_8421, BFALSE, BFALSE);
					}
				return BGl_inversezd2utf8zd2tablez00zz__unicodez00(BgL_auxz00_8418);
			}
		}

	}



/* utf8-string->ucs2-string */
	BGL_EXPORTED_DEF obj_t
		BGl_utf8zd2stringzd2ze3ucs2zd2stringz31zz__unicodez00(obj_t BgL_utf8z00_64)
	{
		{	/* Llib/unicode.scm 646 */
			BGL_TAIL return utf8_string_to_ucs2_string(BgL_utf8z00_64);
		}

	}



/* &utf8-string->ucs2-string */
	obj_t BGl_z62utf8zd2stringzd2ze3ucs2zd2stringz53zz__unicodez00(obj_t
		BgL_envz00_6946, obj_t BgL_utf8z00_6947)
	{
		{	/* Llib/unicode.scm 646 */
			{	/* Llib/unicode.scm 647 */
				obj_t BgL_auxz00_8427;

				if (STRINGP(BgL_utf8z00_6947))
					{	/* Llib/unicode.scm 647 */
						BgL_auxz00_8427 = BgL_utf8z00_6947;
					}
				else
					{
						obj_t BgL_auxz00_8430;

						BgL_auxz00_8430 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
							BINT(26505L), BGl_string3660z00zz__unicodez00,
							BGl_string3661z00zz__unicodez00, BgL_utf8z00_6947);
						FAILURE(BgL_auxz00_8430, BFALSE, BFALSE);
					}
				return
					BGl_utf8zd2stringzd2ze3ucs2zd2stringz31zz__unicodez00
					(BgL_auxz00_8427);
			}
		}

	}



/* utf8->8bits-length */
	long BGl_utf8zd2ze38bitszd2lengthze3zz__unicodez00(obj_t BgL_strz00_65,
		long BgL_lenz00_66)
	{
		{	/* Llib/unicode.scm 652 */
			{
				long BgL_siza7eza7_1454;
				long BgL_iz00_1455;

				BgL_siza7eza7_1454 = 0L;
				BgL_iz00_1455 = 0L;
			BgL_zc3z04anonymousza31394ze3z87_1456:
				if ((BgL_iz00_1455 >= BgL_lenz00_66))
					{	/* Llib/unicode.scm 655 */
						return BgL_siza7eza7_1454;
					}
				else
					{	/* Llib/unicode.scm 657 */
						long BgL_cz00_1458;

						BgL_cz00_1458 = (STRING_REF(BgL_strz00_65, BgL_iz00_1455));
						if ((BgL_cz00_1458 < 194L))
							{
								long BgL_iz00_8443;
								long BgL_siza7eza7_8441;

								BgL_siza7eza7_8441 = (BgL_siza7eza7_1454 + 1L);
								BgL_iz00_8443 = (BgL_iz00_1455 + 1L);
								BgL_iz00_1455 = BgL_iz00_8443;
								BgL_siza7eza7_1454 = BgL_siza7eza7_8441;
								goto BgL_zc3z04anonymousza31394ze3z87_1456;
							}
						else
							{	/* Llib/unicode.scm 659 */
								if ((BgL_cz00_1458 <= 223L))
									{
										long BgL_iz00_8449;
										long BgL_siza7eza7_8447;

										BgL_siza7eza7_8447 = (BgL_siza7eza7_1454 + 1L);
										BgL_iz00_8449 = (BgL_iz00_1455 + 2L);
										BgL_iz00_1455 = BgL_iz00_8449;
										BgL_siza7eza7_1454 = BgL_siza7eza7_8447;
										goto BgL_zc3z04anonymousza31394ze3z87_1456;
									}
								else
									{	/* Llib/unicode.scm 661 */
										if ((BgL_cz00_1458 <= 239L))
											{
												long BgL_iz00_8455;
												long BgL_siza7eza7_8453;

												BgL_siza7eza7_8453 = (BgL_siza7eza7_1454 + 1L);
												BgL_iz00_8455 = (BgL_iz00_1455 + 3L);
												BgL_iz00_1455 = BgL_iz00_8455;
												BgL_siza7eza7_1454 = BgL_siza7eza7_8453;
												goto BgL_zc3z04anonymousza31394ze3z87_1456;
											}
										else
											{	/* Llib/unicode.scm 663 */
												if ((BgL_cz00_1458 <= 247L))
													{
														long BgL_iz00_8461;
														long BgL_siza7eza7_8459;

														BgL_siza7eza7_8459 = (BgL_siza7eza7_1454 + 1L);
														BgL_iz00_8461 = (BgL_iz00_1455 + 4L);
														BgL_iz00_1455 = BgL_iz00_8461;
														BgL_siza7eza7_1454 = BgL_siza7eza7_8459;
														goto BgL_zc3z04anonymousza31394ze3z87_1456;
													}
												else
													{	/* Llib/unicode.scm 665 */
														if ((BgL_cz00_1458 <= 251L))
															{
																long BgL_iz00_8467;
																long BgL_siza7eza7_8465;

																BgL_siza7eza7_8465 = (BgL_siza7eza7_1454 + 1L);
																BgL_iz00_8467 = (BgL_iz00_1455 + 5L);
																BgL_iz00_1455 = BgL_iz00_8467;
																BgL_siza7eza7_1454 = BgL_siza7eza7_8465;
																goto BgL_zc3z04anonymousza31394ze3z87_1456;
															}
														else
															{
																long BgL_iz00_8471;
																long BgL_siza7eza7_8469;

																BgL_siza7eza7_8469 = (BgL_siza7eza7_1454 + 1L);
																BgL_iz00_8471 = (BgL_iz00_1455 + 6L);
																BgL_iz00_1455 = BgL_iz00_8471;
																BgL_siza7eza7_1454 = BgL_siza7eza7_8469;
																goto BgL_zc3z04anonymousza31394ze3z87_1456;
															}
													}
											}
									}
							}
					}
			}
		}

	}



/* ascii-string? */
	BGL_EXPORTED_DEF bool_t BGl_asciizd2stringzf3z21zz__unicodez00(obj_t
		BgL_strz00_67)
	{
		{	/* Llib/unicode.scm 675 */
			return
				(BGl_stringzd2minimalzd2charsetz00zz__unicodez00(BgL_strz00_67) ==
				BGl_symbol3662z00zz__unicodez00);
		}

	}



/* &ascii-string? */
	obj_t BGl_z62asciizd2stringzf3z43zz__unicodez00(obj_t BgL_envz00_6948,
		obj_t BgL_strz00_6949)
	{
		{	/* Llib/unicode.scm 675 */
			{	/* Llib/unicode.scm 676 */
				bool_t BgL_tmpz00_8475;

				{	/* Llib/unicode.scm 676 */
					obj_t BgL_auxz00_8476;

					if (STRINGP(BgL_strz00_6949))
						{	/* Llib/unicode.scm 676 */
							BgL_auxz00_8476 = BgL_strz00_6949;
						}
					else
						{
							obj_t BgL_auxz00_8479;

							BgL_auxz00_8479 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(27489L), BGl_string3664z00zz__unicodez00,
								BGl_string3661z00zz__unicodez00, BgL_strz00_6949);
							FAILURE(BgL_auxz00_8479, BFALSE, BFALSE);
						}
					BgL_tmpz00_8475 =
						BGl_asciizd2stringzf3z21zz__unicodez00(BgL_auxz00_8476);
				}
				return BBOOL(BgL_tmpz00_8475);
			}
		}

	}



/* _utf8-string? */
	obj_t BGl__utf8zd2stringzf3z21zz__unicodez00(obj_t BgL_env1129z00_71,
		obj_t BgL_opt1128z00_70)
	{
		{	/* Llib/unicode.scm 681 */
			{	/* Llib/unicode.scm 681 */
				obj_t BgL_g1130z00_1479;

				BgL_g1130z00_1479 = VECTOR_REF(BgL_opt1128z00_70, 0L);
				switch (VECTOR_LENGTH(BgL_opt1128z00_70))
					{
					case 1L:

						{	/* Llib/unicode.scm 681 */

							{	/* Llib/unicode.scm 681 */
								bool_t BgL_tmpz00_8486;

								{	/* Llib/unicode.scm 681 */
									obj_t BgL_auxz00_8487;

									if (STRINGP(BgL_g1130z00_1479))
										{	/* Llib/unicode.scm 681 */
											BgL_auxz00_8487 = BgL_g1130z00_1479;
										}
									else
										{
											obj_t BgL_auxz00_8490;

											BgL_auxz00_8490 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3618z00zz__unicodez00, BINT(27750L),
												BGl_string3665z00zz__unicodez00,
												BGl_string3661z00zz__unicodez00, BgL_g1130z00_1479);
											FAILURE(BgL_auxz00_8490, BFALSE, BFALSE);
										}
									BgL_tmpz00_8486 =
										BGl_utf8zd2stringzf3z21zz__unicodez00(BgL_auxz00_8487,
										((bool_t) 0));
								}
								return BBOOL(BgL_tmpz00_8486);
							}
						}
						break;
					case 2L:

						{	/* Llib/unicode.scm 681 */
							obj_t BgL_strictz00_1483;

							BgL_strictz00_1483 = VECTOR_REF(BgL_opt1128z00_70, 1L);
							{	/* Llib/unicode.scm 681 */

								{	/* Llib/unicode.scm 681 */
									bool_t BgL_tmpz00_8497;

									{	/* Llib/unicode.scm 681 */
										obj_t BgL_auxz00_8498;

										if (STRINGP(BgL_g1130z00_1479))
											{	/* Llib/unicode.scm 681 */
												BgL_auxz00_8498 = BgL_g1130z00_1479;
											}
										else
											{
												obj_t BgL_auxz00_8501;

												BgL_auxz00_8501 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3618z00zz__unicodez00, BINT(27750L),
													BGl_string3665z00zz__unicodez00,
													BGl_string3661z00zz__unicodez00, BgL_g1130z00_1479);
												FAILURE(BgL_auxz00_8501, BFALSE, BFALSE);
											}
										BgL_tmpz00_8497 =
											BGl_utf8zd2stringzf3z21zz__unicodez00(BgL_auxz00_8498,
											CBOOL(BgL_strictz00_1483));
									}
									return BBOOL(BgL_tmpz00_8497);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* utf8-string? */
	BGL_EXPORTED_DEF bool_t BGl_utf8zd2stringzf3z21zz__unicodez00(obj_t
		BgL_strz00_68, bool_t BgL_strictz00_69)
	{
		{	/* Llib/unicode.scm 681 */
			{	/* Llib/unicode.scm 687 */
				long BgL_lenz00_1485;

				BgL_lenz00_1485 = STRING_LENGTH(BgL_strz00_68);
				{
					long BgL_rz00_1487;

					BgL_rz00_1487 = 0L;
				BgL_zc3z04anonymousza31416ze3z87_1488:
					if ((BgL_rz00_1487 == BgL_lenz00_1485))
						{	/* Llib/unicode.scm 689 */
							return ((bool_t) 1);
						}
					else
						{	/* Llib/unicode.scm 691 */
							long BgL_nz00_1491;

							BgL_nz00_1491 = (STRING_REF(BgL_strz00_68, BgL_rz00_1487));
							{	/* Llib/unicode.scm 692 */

								if ((BgL_nz00_1491 <= 127L))
									{
										long BgL_rz00_8517;

										BgL_rz00_8517 = (BgL_rz00_1487 + 1L);
										BgL_rz00_1487 = BgL_rz00_8517;
										goto BgL_zc3z04anonymousza31416ze3z87_1488;
									}
								else
									{	/* Llib/unicode.scm 694 */
										if ((BgL_nz00_1491 < 194L))
											{	/* Llib/unicode.scm 697 */
												return ((bool_t) 0);
											}
										else
											{	/* Llib/unicode.scm 697 */
												if ((BgL_nz00_1491 < 223L))
													{	/* Llib/unicode.scm 702 */
														bool_t BgL_test4007z00_8523;

														if (((1L + BgL_rz00_1487) < BgL_lenz00_1485))
															{	/* Llib/unicode.scm 684 */
																long BgL_nz00_4157;

																BgL_nz00_4157 =
																	(STRING_REF(BgL_strz00_68,
																		(BgL_rz00_1487 + 1L)));
																if ((BgL_nz00_4157 >= 128L))
																	{	/* Llib/unicode.scm 685 */
																		BgL_test4007z00_8523 =
																			(BgL_nz00_4157 <= 191L);
																	}
																else
																	{	/* Llib/unicode.scm 685 */
																		BgL_test4007z00_8523 = ((bool_t) 0);
																	}
															}
														else
															{	/* Llib/unicode.scm 702 */
																BgL_test4007z00_8523 = ((bool_t) 0);
															}
														if (BgL_test4007z00_8523)
															{
																long BgL_rz00_8533;

																BgL_rz00_8533 = (BgL_rz00_1487 + 2L);
																BgL_rz00_1487 = BgL_rz00_8533;
																goto BgL_zc3z04anonymousza31416ze3z87_1488;
															}
														else
															{	/* Llib/unicode.scm 702 */
																return ((bool_t) 0);
															}
													}
												else
													{	/* Llib/unicode.scm 705 */
														bool_t BgL_test4010z00_8535;

														if ((BgL_nz00_1491 >= 216L))
															{	/* Llib/unicode.scm 705 */
																BgL_test4010z00_8535 = (BgL_nz00_1491 <= 219L);
															}
														else
															{	/* Llib/unicode.scm 705 */
																BgL_test4010z00_8535 = ((bool_t) 0);
															}
														if (BgL_test4010z00_8535)
															{	/* Llib/unicode.scm 707 */
																bool_t BgL_test4012z00_8539;

																if ((BgL_rz00_1487 < (BgL_lenz00_1485 - 3L)))
																	{	/* Llib/unicode.scm 708 */
																		bool_t BgL_test4014z00_8543;

																		{	/* Llib/unicode.scm 684 */
																			long BgL_nz00_4171;

																			BgL_nz00_4171 =
																				(STRING_REF(BgL_strz00_68,
																					(BgL_rz00_1487 + 1L)));
																			if ((BgL_nz00_4171 >= 220L))
																				{	/* Llib/unicode.scm 685 */
																					BgL_test4014z00_8543 =
																						(BgL_nz00_4171 <= 223L);
																				}
																			else
																				{	/* Llib/unicode.scm 685 */
																					BgL_test4014z00_8543 = ((bool_t) 0);
																				}
																		}
																		if (BgL_test4014z00_8543)
																			{	/* Llib/unicode.scm 709 */
																				bool_t BgL_test4016z00_8550;

																				{	/* Llib/unicode.scm 684 */
																					long BgL_nz00_4179;

																					BgL_nz00_4179 =
																						(STRING_REF(BgL_strz00_68,
																							(BgL_rz00_1487 + 2L)));
																					if ((BgL_nz00_4179 >= 220L))
																						{	/* Llib/unicode.scm 685 */
																							BgL_test4016z00_8550 =
																								(BgL_nz00_4179 <= 223L);
																						}
																					else
																						{	/* Llib/unicode.scm 685 */
																							BgL_test4016z00_8550 =
																								((bool_t) 0);
																						}
																				}
																				if (BgL_test4016z00_8550)
																					{	/* Llib/unicode.scm 684 */
																						long BgL_nz00_4187;

																						BgL_nz00_4187 =
																							(STRING_REF(BgL_strz00_68,
																								(BgL_rz00_1487 + 3L)));
																						if ((BgL_nz00_4187 >= 220L))
																							{	/* Llib/unicode.scm 685 */
																								BgL_test4012z00_8539 =
																									(BgL_nz00_4187 <= 223L);
																							}
																						else
																							{	/* Llib/unicode.scm 685 */
																								BgL_test4012z00_8539 =
																									((bool_t) 0);
																							}
																					}
																				else
																					{	/* Llib/unicode.scm 709 */
																						BgL_test4012z00_8539 = ((bool_t) 0);
																					}
																			}
																		else
																			{	/* Llib/unicode.scm 708 */
																				BgL_test4012z00_8539 = ((bool_t) 0);
																			}
																	}
																else
																	{	/* Llib/unicode.scm 707 */
																		BgL_test4012z00_8539 = ((bool_t) 0);
																	}
																if (BgL_test4012z00_8539)
																	{
																		long BgL_rz00_8563;

																		BgL_rz00_8563 = (BgL_rz00_1487 + 4L);
																		BgL_rz00_1487 = BgL_rz00_8563;
																		goto BgL_zc3z04anonymousza31416ze3z87_1488;
																	}
																else
																	{	/* Llib/unicode.scm 707 */
																		return ((bool_t) 0);
																	}
															}
														else
															{	/* Llib/unicode.scm 705 */
																if ((BgL_nz00_1491 <= 223L))
																	{	/* Llib/unicode.scm 714 */
																		bool_t BgL_test4020z00_8567;

																		if (
																			((1L + BgL_rz00_1487) < BgL_lenz00_1485))
																			{	/* Llib/unicode.scm 684 */
																				long BgL_nz00_4200;

																				BgL_nz00_4200 =
																					(STRING_REF(BgL_strz00_68,
																						(BgL_rz00_1487 + 1L)));
																				if ((BgL_nz00_4200 >= 128L))
																					{	/* Llib/unicode.scm 685 */
																						BgL_test4020z00_8567 =
																							(BgL_nz00_4200 <= 191L);
																					}
																				else
																					{	/* Llib/unicode.scm 685 */
																						BgL_test4020z00_8567 = ((bool_t) 0);
																					}
																			}
																		else
																			{	/* Llib/unicode.scm 714 */
																				BgL_test4020z00_8567 = ((bool_t) 0);
																			}
																		if (BgL_test4020z00_8567)
																			{
																				long BgL_rz00_8577;

																				BgL_rz00_8577 = (BgL_rz00_1487 + 2L);
																				BgL_rz00_1487 = BgL_rz00_8577;
																				goto
																					BgL_zc3z04anonymousza31416ze3z87_1488;
																			}
																		else
																			{	/* Llib/unicode.scm 714 */
																				return ((bool_t) 0);
																			}
																	}
																else
																	{	/* Llib/unicode.scm 712 */
																		if ((BgL_nz00_1491 <= 239L))
																			{	/* Llib/unicode.scm 719 */
																				bool_t BgL_test4024z00_8581;

																				if (
																					(BgL_rz00_1487 <
																						(BgL_lenz00_1485 - 2L)))
																					{	/* Llib/unicode.scm 720 */
																						bool_t BgL_test4026z00_8585;

																						{	/* Llib/unicode.scm 684 */
																							long BgL_nz00_4213;

																							BgL_nz00_4213 =
																								(STRING_REF(BgL_strz00_68,
																									(BgL_rz00_1487 + 1L)));
																							if ((BgL_nz00_4213 >= 128L))
																								{	/* Llib/unicode.scm 685 */
																									BgL_test4026z00_8585 =
																										(BgL_nz00_4213 <= 191L);
																								}
																							else
																								{	/* Llib/unicode.scm 685 */
																									BgL_test4026z00_8585 =
																										((bool_t) 0);
																								}
																						}
																						if (BgL_test4026z00_8585)
																							{	/* Llib/unicode.scm 684 */
																								long BgL_nz00_4221;

																								BgL_nz00_4221 =
																									(STRING_REF(BgL_strz00_68,
																										(BgL_rz00_1487 + 2L)));
																								if ((BgL_nz00_4221 >= 128L))
																									{	/* Llib/unicode.scm 685 */
																										BgL_test4024z00_8581 =
																											(BgL_nz00_4221 <= 191L);
																									}
																								else
																									{	/* Llib/unicode.scm 685 */
																										BgL_test4024z00_8581 =
																											((bool_t) 0);
																									}
																							}
																						else
																							{	/* Llib/unicode.scm 720 */
																								BgL_test4024z00_8581 =
																									((bool_t) 0);
																							}
																					}
																				else
																					{	/* Llib/unicode.scm 719 */
																						BgL_test4024z00_8581 = ((bool_t) 0);
																					}
																				if (BgL_test4024z00_8581)
																					{
																						long BgL_rz00_8598;

																						BgL_rz00_8598 =
																							(BgL_rz00_1487 + 3L);
																						BgL_rz00_1487 = BgL_rz00_8598;
																						goto
																							BgL_zc3z04anonymousza31416ze3z87_1488;
																					}
																				else
																					{	/* Llib/unicode.scm 719 */
																						return ((bool_t) 0);
																					}
																			}
																		else
																			{	/* Llib/unicode.scm 717 */
																				if ((BgL_nz00_1491 == 240L))
																					{	/* Llib/unicode.scm 725 */
																						bool_t BgL_test4030z00_8602;

																						if (
																							(BgL_rz00_1487 <
																								(BgL_lenz00_1485 - 3L)))
																							{	/* Llib/unicode.scm 726 */
																								bool_t BgL_test4032z00_8606;

																								{	/* Llib/unicode.scm 684 */
																									long BgL_nz00_4234;

																									BgL_nz00_4234 =
																										(STRING_REF(BgL_strz00_68,
																											(BgL_rz00_1487 + 1L)));
																									if ((BgL_nz00_4234 >= 144L))
																										{	/* Llib/unicode.scm 685 */
																											BgL_test4032z00_8606 =
																												(BgL_nz00_4234 <= 191L);
																										}
																									else
																										{	/* Llib/unicode.scm 685 */
																											BgL_test4032z00_8606 =
																												((bool_t) 0);
																										}
																								}
																								if (BgL_test4032z00_8606)
																									{	/* Llib/unicode.scm 727 */
																										bool_t BgL_test4034z00_8613;

																										{	/* Llib/unicode.scm 684 */
																											long BgL_nz00_4242;

																											BgL_nz00_4242 =
																												(STRING_REF
																												(BgL_strz00_68,
																													(BgL_rz00_1487 +
																														2L)));
																											if ((BgL_nz00_4242 >=
																													128L))
																												{	/* Llib/unicode.scm 685 */
																													BgL_test4034z00_8613 =
																														(BgL_nz00_4242 <=
																														191L);
																												}
																											else
																												{	/* Llib/unicode.scm 685 */
																													BgL_test4034z00_8613 =
																														((bool_t) 0);
																												}
																										}
																										if (BgL_test4034z00_8613)
																											{	/* Llib/unicode.scm 684 */
																												long BgL_nz00_4250;

																												BgL_nz00_4250 =
																													(STRING_REF
																													(BgL_strz00_68,
																														(BgL_rz00_1487 +
																															3L)));
																												if ((BgL_nz00_4250 >=
																														128L))
																													{	/* Llib/unicode.scm 685 */
																														BgL_test4030z00_8602
																															=
																															(BgL_nz00_4250 <=
																															191L);
																													}
																												else
																													{	/* Llib/unicode.scm 685 */
																														BgL_test4030z00_8602
																															= ((bool_t) 0);
																													}
																											}
																										else
																											{	/* Llib/unicode.scm 727 */
																												BgL_test4030z00_8602 =
																													((bool_t) 0);
																											}
																									}
																								else
																									{	/* Llib/unicode.scm 726 */
																										BgL_test4030z00_8602 =
																											((bool_t) 0);
																									}
																							}
																						else
																							{	/* Llib/unicode.scm 725 */
																								BgL_test4030z00_8602 =
																									((bool_t) 0);
																							}
																						if (BgL_test4030z00_8602)
																							{
																								long BgL_rz00_8626;

																								BgL_rz00_8626 =
																									(BgL_rz00_1487 + 4L);
																								BgL_rz00_1487 = BgL_rz00_8626;
																								goto
																									BgL_zc3z04anonymousza31416ze3z87_1488;
																							}
																						else
																							{	/* Llib/unicode.scm 725 */
																								return ((bool_t) 0);
																							}
																					}
																				else
																					{	/* Llib/unicode.scm 730 */
																						bool_t BgL_test4037z00_8628;

																						if ((BgL_nz00_1491 == 244L))
																							{	/* Llib/unicode.scm 730 */
																								BgL_test4037z00_8628 =
																									((bool_t) 1);
																							}
																						else
																							{	/* Llib/unicode.scm 731 */
																								bool_t BgL_test4039z00_8631;

																								{	/* Llib/unicode.scm 731 */
																									bool_t
																										BgL__ortest_1047z00_1737;
																									BgL__ortest_1047z00_1737 =
																										(BgL_nz00_1491 == 248L);
																									if (BgL__ortest_1047z00_1737)
																										{	/* Llib/unicode.scm 731 */
																											BgL_test4039z00_8631 =
																												BgL__ortest_1047z00_1737;
																										}
																									else
																										{	/* Llib/unicode.scm 731 */
																											BgL_test4039z00_8631 =
																												(BgL_nz00_1491 == 252L);
																										}
																								}
																								if (BgL_test4039z00_8631)
																									{	/* Llib/unicode.scm 731 */
																										if (BgL_strictz00_69)
																											{	/* Llib/unicode.scm 731 */
																												BgL_test4037z00_8628 =
																													((bool_t) 0);
																											}
																										else
																											{	/* Llib/unicode.scm 731 */
																												BgL_test4037z00_8628 =
																													((bool_t) 1);
																											}
																									}
																								else
																									{	/* Llib/unicode.scm 731 */
																										BgL_test4037z00_8628 =
																											((bool_t) 0);
																									}
																							}
																						if (BgL_test4037z00_8628)
																							{	/* Llib/unicode.scm 733 */
																								bool_t BgL_test4042z00_8636;

																								if (
																									(BgL_rz00_1487 <
																										(BgL_lenz00_1485 - 3L)))
																									{	/* Llib/unicode.scm 734 */
																										bool_t BgL_test4044z00_8640;

																										{	/* Llib/unicode.scm 684 */
																											long BgL_nz00_4265;

																											BgL_nz00_4265 =
																												(STRING_REF
																												(BgL_strz00_68,
																													(BgL_rz00_1487 +
																														1L)));
																											if ((BgL_nz00_4265 >=
																													128L))
																												{	/* Llib/unicode.scm 685 */
																													BgL_test4044z00_8640 =
																														(BgL_nz00_4265 <=
																														191L);
																												}
																											else
																												{	/* Llib/unicode.scm 685 */
																													BgL_test4044z00_8640 =
																														((bool_t) 0);
																												}
																										}
																										if (BgL_test4044z00_8640)
																											{	/* Llib/unicode.scm 735 */
																												bool_t
																													BgL_test4046z00_8647;
																												{	/* Llib/unicode.scm 684 */
																													long BgL_nz00_4273;

																													BgL_nz00_4273 =
																														(STRING_REF
																														(BgL_strz00_68,
																															(BgL_rz00_1487 +
																																2L)));
																													if ((BgL_nz00_4273 >=
																															128L))
																														{	/* Llib/unicode.scm 685 */
																															BgL_test4046z00_8647
																																=
																																(BgL_nz00_4273
																																<= 191L);
																														}
																													else
																														{	/* Llib/unicode.scm 685 */
																															BgL_test4046z00_8647
																																= ((bool_t) 0);
																														}
																												}
																												if (BgL_test4046z00_8647)
																													{	/* Llib/unicode.scm 684 */
																														long BgL_nz00_4281;

																														BgL_nz00_4281 =
																															(STRING_REF
																															(BgL_strz00_68,
																																(BgL_rz00_1487 +
																																	3L)));
																														if ((BgL_nz00_4281
																																>= 128L))
																															{	/* Llib/unicode.scm 685 */
																																BgL_test4042z00_8636
																																	=
																																	(BgL_nz00_4281
																																	<= 191L);
																															}
																														else
																															{	/* Llib/unicode.scm 685 */
																																BgL_test4042z00_8636
																																	=
																																	((bool_t) 0);
																															}
																													}
																												else
																													{	/* Llib/unicode.scm 735 */
																														BgL_test4042z00_8636
																															= ((bool_t) 0);
																													}
																											}
																										else
																											{	/* Llib/unicode.scm 734 */
																												BgL_test4042z00_8636 =
																													((bool_t) 0);
																											}
																									}
																								else
																									{	/* Llib/unicode.scm 733 */
																										BgL_test4042z00_8636 =
																											((bool_t) 0);
																									}
																								if (BgL_test4042z00_8636)
																									{
																										long BgL_rz00_8660;

																										BgL_rz00_8660 =
																											(BgL_rz00_1487 + 4L);
																										BgL_rz00_1487 =
																											BgL_rz00_8660;
																										goto
																											BgL_zc3z04anonymousza31416ze3z87_1488;
																									}
																								else
																									{	/* Llib/unicode.scm 733 */
																										return ((bool_t) 0);
																									}
																							}
																						else
																							{	/* Llib/unicode.scm 730 */
																								if ((BgL_nz00_1491 <= 247L))
																									{	/* Llib/unicode.scm 740 */
																										bool_t BgL_test4050z00_8664;

																										if (
																											(BgL_rz00_1487 <
																												(BgL_lenz00_1485 - 3L)))
																											{	/* Llib/unicode.scm 741 */
																												bool_t
																													BgL_test4052z00_8668;
																												{	/* Llib/unicode.scm 684 */
																													long BgL_nz00_4294;

																													BgL_nz00_4294 =
																														(STRING_REF
																														(BgL_strz00_68,
																															(BgL_rz00_1487 +
																																1L)));
																													if ((BgL_nz00_4294 >=
																															128L))
																														{	/* Llib/unicode.scm 685 */
																															BgL_test4052z00_8668
																																=
																																(BgL_nz00_4294
																																<= 191L);
																														}
																													else
																														{	/* Llib/unicode.scm 685 */
																															BgL_test4052z00_8668
																																= ((bool_t) 0);
																														}
																												}
																												if (BgL_test4052z00_8668)
																													{	/* Llib/unicode.scm 742 */
																														bool_t
																															BgL_test4054z00_8675;
																														{	/* Llib/unicode.scm 684 */
																															long
																																BgL_nz00_4302;
																															BgL_nz00_4302 =
																																(STRING_REF
																																(BgL_strz00_68,
																																	(BgL_rz00_1487
																																		+ 2L)));
																															if ((BgL_nz00_4302
																																	>= 128L))
																																{	/* Llib/unicode.scm 685 */
																																	BgL_test4054z00_8675
																																		=
																																		(BgL_nz00_4302
																																		<= 191L);
																																}
																															else
																																{	/* Llib/unicode.scm 685 */
																																	BgL_test4054z00_8675
																																		=
																																		((bool_t)
																																		0);
																																}
																														}
																														if (BgL_test4054z00_8675)
																															{	/* Llib/unicode.scm 684 */
																																long
																																	BgL_nz00_4310;
																																BgL_nz00_4310 =
																																	(STRING_REF
																																	(BgL_strz00_68,
																																		(BgL_rz00_1487
																																			+ 3L)));
																																if (
																																	(BgL_nz00_4310
																																		>= 128L))
																																	{	/* Llib/unicode.scm 685 */
																																		BgL_test4050z00_8664
																																			=
																																			(BgL_nz00_4310
																																			<= 191L);
																																	}
																																else
																																	{	/* Llib/unicode.scm 685 */
																																		BgL_test4050z00_8664
																																			=
																																			((bool_t)
																																			0);
																																	}
																															}
																														else
																															{	/* Llib/unicode.scm 742 */
																																BgL_test4050z00_8664
																																	=
																																	((bool_t) 0);
																															}
																													}
																												else
																													{	/* Llib/unicode.scm 741 */
																														BgL_test4050z00_8664
																															= ((bool_t) 0);
																													}
																											}
																										else
																											{	/* Llib/unicode.scm 740 */
																												BgL_test4050z00_8664 =
																													((bool_t) 0);
																											}
																										if (BgL_test4050z00_8664)
																											{
																												long BgL_rz00_8688;

																												BgL_rz00_8688 =
																													(BgL_rz00_1487 + 4L);
																												BgL_rz00_1487 =
																													BgL_rz00_8688;
																												goto
																													BgL_zc3z04anonymousza31416ze3z87_1488;
																											}
																										else
																											{	/* Llib/unicode.scm 740 */
																												return ((bool_t) 0);
																											}
																									}
																								else
																									{	/* Llib/unicode.scm 738 */
																										if ((BgL_nz00_1491 <= 251L))
																											{	/* Llib/unicode.scm 746 */
																												bool_t
																													BgL_test4058z00_8692;
																												if ((BgL_rz00_1487 <
																														(BgL_lenz00_1485 -
																															4L)))
																													{	/* Llib/unicode.scm 747 */
																														bool_t
																															BgL_test4060z00_8696;
																														{	/* Llib/unicode.scm 684 */
																															long
																																BgL_nz00_4323;
																															BgL_nz00_4323 =
																																(STRING_REF
																																(BgL_strz00_68,
																																	(BgL_rz00_1487
																																		+ 1L)));
																															if ((BgL_nz00_4323
																																	>= 128L))
																																{	/* Llib/unicode.scm 685 */
																																	BgL_test4060z00_8696
																																		=
																																		(BgL_nz00_4323
																																		<= 191L);
																																}
																															else
																																{	/* Llib/unicode.scm 685 */
																																	BgL_test4060z00_8696
																																		=
																																		((bool_t)
																																		0);
																																}
																														}
																														if (BgL_test4060z00_8696)
																															{	/* Llib/unicode.scm 748 */
																																bool_t
																																	BgL_test4062z00_8703;
																																{	/* Llib/unicode.scm 684 */
																																	long
																																		BgL_nz00_4331;
																																	BgL_nz00_4331
																																		=
																																		(STRING_REF
																																		(BgL_strz00_68,
																																			(BgL_rz00_1487
																																				+ 2L)));
																																	if (
																																		(BgL_nz00_4331
																																			>= 128L))
																																		{	/* Llib/unicode.scm 685 */
																																			BgL_test4062z00_8703
																																				=
																																				(BgL_nz00_4331
																																				<=
																																				191L);
																																		}
																																	else
																																		{	/* Llib/unicode.scm 685 */
																																			BgL_test4062z00_8703
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																}
																																if (BgL_test4062z00_8703)
																																	{	/* Llib/unicode.scm 749 */
																																		bool_t
																																			BgL_test4064z00_8710;
																																		{	/* Llib/unicode.scm 684 */
																																			long
																																				BgL_nz00_4339;
																																			BgL_nz00_4339
																																				=
																																				(STRING_REF
																																				(BgL_strz00_68,
																																					(BgL_rz00_1487
																																						+
																																						3L)));
																																			if (
																																				(BgL_nz00_4339
																																					>=
																																					128L))
																																				{	/* Llib/unicode.scm 685 */
																																					BgL_test4064z00_8710
																																						=
																																						(BgL_nz00_4339
																																						<=
																																						191L);
																																				}
																																			else
																																				{	/* Llib/unicode.scm 685 */
																																					BgL_test4064z00_8710
																																						=
																																						(
																																						(bool_t)
																																						0);
																																				}
																																		}
																																		if (BgL_test4064z00_8710)
																																			{	/* Llib/unicode.scm 684 */
																																				long
																																					BgL_nz00_4347;
																																				BgL_nz00_4347
																																					=
																																					(STRING_REF
																																					(BgL_strz00_68,
																																						(BgL_rz00_1487
																																							+
																																							4L)));
																																				if (
																																					(BgL_nz00_4347
																																						>=
																																						128L))
																																					{	/* Llib/unicode.scm 685 */
																																						BgL_test4058z00_8692
																																							=
																																							(BgL_nz00_4347
																																							<=
																																							191L);
																																					}
																																				else
																																					{	/* Llib/unicode.scm 685 */
																																						BgL_test4058z00_8692
																																							=
																																							(
																																							(bool_t)
																																							0);
																																					}
																																			}
																																		else
																																			{	/* Llib/unicode.scm 749 */
																																				BgL_test4058z00_8692
																																					=
																																					(
																																					(bool_t)
																																					0);
																																			}
																																	}
																																else
																																	{	/* Llib/unicode.scm 748 */
																																		BgL_test4058z00_8692
																																			=
																																			((bool_t)
																																			0);
																																	}
																															}
																														else
																															{	/* Llib/unicode.scm 747 */
																																BgL_test4058z00_8692
																																	=
																																	((bool_t) 0);
																															}
																													}
																												else
																													{	/* Llib/unicode.scm 746 */
																														BgL_test4058z00_8692
																															= ((bool_t) 0);
																													}
																												if (BgL_test4058z00_8692)
																													{
																														long BgL_rz00_8723;

																														BgL_rz00_8723 =
																															(BgL_rz00_1487 +
																															5L);
																														BgL_rz00_1487 =
																															BgL_rz00_8723;
																														goto
																															BgL_zc3z04anonymousza31416ze3z87_1488;
																													}
																												else
																													{	/* Llib/unicode.scm 746 */
																														return ((bool_t) 0);
																													}
																											}
																										else
																											{	/* Llib/unicode.scm 745 */
																												if (
																													(BgL_nz00_1491 <=
																														253L))
																													{	/* Llib/unicode.scm 753 */
																														bool_t
																															BgL_test4068z00_8727;
																														if ((BgL_rz00_1487 <
																																(BgL_lenz00_1485
																																	- 5L)))
																															{	/* Llib/unicode.scm 754 */
																																bool_t
																																	BgL_test4070z00_8731;
																																{	/* Llib/unicode.scm 684 */
																																	long
																																		BgL_nz00_4360;
																																	BgL_nz00_4360
																																		=
																																		(STRING_REF
																																		(BgL_strz00_68,
																																			(BgL_rz00_1487
																																				+ 1L)));
																																	if (
																																		(BgL_nz00_4360
																																			>= 128L))
																																		{	/* Llib/unicode.scm 685 */
																																			BgL_test4070z00_8731
																																				=
																																				(BgL_nz00_4360
																																				<=
																																				191L);
																																		}
																																	else
																																		{	/* Llib/unicode.scm 685 */
																																			BgL_test4070z00_8731
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																}
																																if (BgL_test4070z00_8731)
																																	{	/* Llib/unicode.scm 755 */
																																		bool_t
																																			BgL_test4072z00_8738;
																																		{	/* Llib/unicode.scm 684 */
																																			long
																																				BgL_nz00_4368;
																																			BgL_nz00_4368
																																				=
																																				(STRING_REF
																																				(BgL_strz00_68,
																																					(BgL_rz00_1487
																																						+
																																						2L)));
																																			if (
																																				(BgL_nz00_4368
																																					>=
																																					128L))
																																				{	/* Llib/unicode.scm 685 */
																																					BgL_test4072z00_8738
																																						=
																																						(BgL_nz00_4368
																																						<=
																																						191L);
																																				}
																																			else
																																				{	/* Llib/unicode.scm 685 */
																																					BgL_test4072z00_8738
																																						=
																																						(
																																						(bool_t)
																																						0);
																																				}
																																		}
																																		if (BgL_test4072z00_8738)
																																			{	/* Llib/unicode.scm 756 */
																																				bool_t
																																					BgL_test4074z00_8745;
																																				{	/* Llib/unicode.scm 684 */
																																					long
																																						BgL_nz00_4376;
																																					BgL_nz00_4376
																																						=
																																						(STRING_REF
																																						(BgL_strz00_68,
																																							(BgL_rz00_1487
																																								+
																																								3L)));
																																					if (
																																						(BgL_nz00_4376
																																							>=
																																							128L))
																																						{	/* Llib/unicode.scm 685 */
																																							BgL_test4074z00_8745
																																								=
																																								(BgL_nz00_4376
																																								<=
																																								191L);
																																						}
																																					else
																																						{	/* Llib/unicode.scm 685 */
																																							BgL_test4074z00_8745
																																								=
																																								(
																																								(bool_t)
																																								0);
																																						}
																																				}
																																				if (BgL_test4074z00_8745)
																																					{	/* Llib/unicode.scm 757 */
																																						bool_t
																																							BgL_test4076z00_8752;
																																						{	/* Llib/unicode.scm 684 */
																																							long
																																								BgL_nz00_4384;
																																							BgL_nz00_4384
																																								=
																																								(STRING_REF
																																								(BgL_strz00_68,
																																									(BgL_rz00_1487
																																										+
																																										4L)));
																																							if ((BgL_nz00_4384 >= 128L))
																																								{	/* Llib/unicode.scm 685 */
																																									BgL_test4076z00_8752
																																										=
																																										(BgL_nz00_4384
																																										<=
																																										191L);
																																								}
																																							else
																																								{	/* Llib/unicode.scm 685 */
																																									BgL_test4076z00_8752
																																										=
																																										(
																																										(bool_t)
																																										0);
																																								}
																																						}
																																						if (BgL_test4076z00_8752)
																																							{	/* Llib/unicode.scm 684 */
																																								long
																																									BgL_nz00_4392;
																																								BgL_nz00_4392
																																									=
																																									(STRING_REF
																																									(BgL_strz00_68,
																																										(BgL_rz00_1487
																																											+
																																											5L)));
																																								if ((BgL_nz00_4392 >= 128L))
																																									{	/* Llib/unicode.scm 685 */
																																										BgL_test4068z00_8727
																																											=
																																											(BgL_nz00_4392
																																											<=
																																											191L);
																																									}
																																								else
																																									{	/* Llib/unicode.scm 685 */
																																										BgL_test4068z00_8727
																																											=
																																											(
																																											(bool_t)
																																											0);
																																									}
																																							}
																																						else
																																							{	/* Llib/unicode.scm 757 */
																																								BgL_test4068z00_8727
																																									=
																																									(
																																									(bool_t)
																																									0);
																																							}
																																					}
																																				else
																																					{	/* Llib/unicode.scm 756 */
																																						BgL_test4068z00_8727
																																							=
																																							(
																																							(bool_t)
																																							0);
																																					}
																																			}
																																		else
																																			{	/* Llib/unicode.scm 755 */
																																				BgL_test4068z00_8727
																																					=
																																					(
																																					(bool_t)
																																					0);
																																			}
																																	}
																																else
																																	{	/* Llib/unicode.scm 754 */
																																		BgL_test4068z00_8727
																																			=
																																			((bool_t)
																																			0);
																																	}
																															}
																														else
																															{	/* Llib/unicode.scm 753 */
																																BgL_test4068z00_8727
																																	=
																																	((bool_t) 0);
																															}
																														if (BgL_test4068z00_8727)
																															{
																																long
																																	BgL_rz00_8765;
																																BgL_rz00_8765 =
																																	(BgL_rz00_1487
																																	+ 6L);
																																BgL_rz00_1487 =
																																	BgL_rz00_8765;
																																goto
																																	BgL_zc3z04anonymousza31416ze3z87_1488;
																															}
																														else
																															{	/* Llib/unicode.scm 753 */
																																return ((bool_t)
																																	0);
																															}
																													}
																												else
																													{	/* Llib/unicode.scm 752 */
																														return ((bool_t) 0);
																													}
																											}
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
									}
							}
						}
				}
			}
		}

	}



/* _utf8-string-encode */
	obj_t BGl__utf8zd2stringzd2encodez00zz__unicodez00(obj_t BgL_env1134z00_77,
		obj_t BgL_opt1133z00_76)
	{
		{	/* Llib/unicode.scm 774 */
			{	/* Llib/unicode.scm 774 */
				obj_t BgL_strz00_1747;

				BgL_strz00_1747 = VECTOR_REF(BgL_opt1133z00_76, 0L);
				switch (VECTOR_LENGTH(BgL_opt1133z00_76))
					{
					case 1L:

						{	/* Llib/unicode.scm 774 */
							long BgL_endz00_1752;

							{	/* Llib/unicode.scm 774 */
								obj_t BgL_stringz00_4398;

								if (STRINGP(BgL_strz00_1747))
									{	/* Llib/unicode.scm 774 */
										BgL_stringz00_4398 = BgL_strz00_1747;
									}
								else
									{
										obj_t BgL_auxz00_8770;

										BgL_auxz00_8770 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string3618z00zz__unicodez00, BINT(31642L),
											BGl_string3666z00zz__unicodez00,
											BGl_string3661z00zz__unicodez00, BgL_strz00_1747);
										FAILURE(BgL_auxz00_8770, BFALSE, BFALSE);
									}
								BgL_endz00_1752 = STRING_LENGTH(BgL_stringz00_4398);
							}
							{	/* Llib/unicode.scm 774 */

								{	/* Llib/unicode.scm 774 */
									obj_t BgL_auxz00_8775;

									if (STRINGP(BgL_strz00_1747))
										{	/* Llib/unicode.scm 774 */
											BgL_auxz00_8775 = BgL_strz00_1747;
										}
									else
										{
											obj_t BgL_auxz00_8778;

											BgL_auxz00_8778 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3618z00zz__unicodez00, BINT(31550L),
												BGl_string3666z00zz__unicodez00,
												BGl_string3661z00zz__unicodez00, BgL_strz00_1747);
											FAILURE(BgL_auxz00_8778, BFALSE, BFALSE);
										}
									return
										BGl_utf8zd2stringzd2encodez00zz__unicodez00(BgL_auxz00_8775,
										((bool_t) 0), 0L, BgL_endz00_1752);
								}
							}
						}
						break;
					case 2L:

						{	/* Llib/unicode.scm 774 */
							obj_t BgL_strictz00_1753;

							BgL_strictz00_1753 = VECTOR_REF(BgL_opt1133z00_76, 1L);
							{	/* Llib/unicode.scm 774 */
								long BgL_endz00_1755;

								{	/* Llib/unicode.scm 774 */
									obj_t BgL_stringz00_4399;

									if (STRINGP(BgL_strz00_1747))
										{	/* Llib/unicode.scm 774 */
											BgL_stringz00_4399 = BgL_strz00_1747;
										}
									else
										{
											obj_t BgL_auxz00_8786;

											BgL_auxz00_8786 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3618z00zz__unicodez00, BINT(31642L),
												BGl_string3666z00zz__unicodez00,
												BGl_string3661z00zz__unicodez00, BgL_strz00_1747);
											FAILURE(BgL_auxz00_8786, BFALSE, BFALSE);
										}
									BgL_endz00_1755 = STRING_LENGTH(BgL_stringz00_4399);
								}
								{	/* Llib/unicode.scm 774 */

									{	/* Llib/unicode.scm 774 */
										obj_t BgL_auxz00_8791;

										if (STRINGP(BgL_strz00_1747))
											{	/* Llib/unicode.scm 774 */
												BgL_auxz00_8791 = BgL_strz00_1747;
											}
										else
											{
												obj_t BgL_auxz00_8794;

												BgL_auxz00_8794 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3618z00zz__unicodez00, BINT(31550L),
													BGl_string3666z00zz__unicodez00,
													BGl_string3661z00zz__unicodez00, BgL_strz00_1747);
												FAILURE(BgL_auxz00_8794, BFALSE, BFALSE);
											}
										return
											BGl_utf8zd2stringzd2encodez00zz__unicodez00
											(BgL_auxz00_8791, CBOOL(BgL_strictz00_1753), 0L,
											BgL_endz00_1755);
									}
								}
							}
						}
						break;
					case 3L:

						{	/* Llib/unicode.scm 774 */
							obj_t BgL_strictz00_1756;

							BgL_strictz00_1756 = VECTOR_REF(BgL_opt1133z00_76, 1L);
							{	/* Llib/unicode.scm 774 */
								obj_t BgL_startz00_1757;

								BgL_startz00_1757 = VECTOR_REF(BgL_opt1133z00_76, 2L);
								{	/* Llib/unicode.scm 774 */
									long BgL_endz00_1758;

									{	/* Llib/unicode.scm 774 */
										obj_t BgL_stringz00_4400;

										if (STRINGP(BgL_strz00_1747))
											{	/* Llib/unicode.scm 774 */
												BgL_stringz00_4400 = BgL_strz00_1747;
											}
										else
											{
												obj_t BgL_auxz00_8804;

												BgL_auxz00_8804 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3618z00zz__unicodez00, BINT(31642L),
													BGl_string3666z00zz__unicodez00,
													BGl_string3661z00zz__unicodez00, BgL_strz00_1747);
												FAILURE(BgL_auxz00_8804, BFALSE, BFALSE);
											}
										BgL_endz00_1758 = STRING_LENGTH(BgL_stringz00_4400);
									}
									{	/* Llib/unicode.scm 774 */

										{	/* Llib/unicode.scm 774 */
											long BgL_auxz00_8816;
											obj_t BgL_auxz00_8809;

											{	/* Llib/unicode.scm 774 */
												obj_t BgL_tmpz00_8818;

												if (INTEGERP(BgL_startz00_1757))
													{	/* Llib/unicode.scm 774 */
														BgL_tmpz00_8818 = BgL_startz00_1757;
													}
												else
													{
														obj_t BgL_auxz00_8821;

														BgL_auxz00_8821 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string3618z00zz__unicodez00, BINT(31550L),
															BGl_string3666z00zz__unicodez00,
															BGl_string3620z00zz__unicodez00,
															BgL_startz00_1757);
														FAILURE(BgL_auxz00_8821, BFALSE, BFALSE);
													}
												BgL_auxz00_8816 = (long) CINT(BgL_tmpz00_8818);
											}
											if (STRINGP(BgL_strz00_1747))
												{	/* Llib/unicode.scm 774 */
													BgL_auxz00_8809 = BgL_strz00_1747;
												}
											else
												{
													obj_t BgL_auxz00_8812;

													BgL_auxz00_8812 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string3618z00zz__unicodez00, BINT(31550L),
														BGl_string3666z00zz__unicodez00,
														BGl_string3661z00zz__unicodez00, BgL_strz00_1747);
													FAILURE(BgL_auxz00_8812, BFALSE, BFALSE);
												}
											return
												BGl_utf8zd2stringzd2encodez00zz__unicodez00
												(BgL_auxz00_8809, CBOOL(BgL_strictz00_1756),
												BgL_auxz00_8816, BgL_endz00_1758);
										}
									}
								}
							}
						}
						break;
					case 4L:

						{	/* Llib/unicode.scm 774 */
							obj_t BgL_strictz00_1759;

							BgL_strictz00_1759 = VECTOR_REF(BgL_opt1133z00_76, 1L);
							{	/* Llib/unicode.scm 774 */
								obj_t BgL_startz00_1760;

								BgL_startz00_1760 = VECTOR_REF(BgL_opt1133z00_76, 2L);
								{	/* Llib/unicode.scm 774 */
									obj_t BgL_endz00_1761;

									BgL_endz00_1761 = VECTOR_REF(BgL_opt1133z00_76, 3L);
									{	/* Llib/unicode.scm 774 */

										{	/* Llib/unicode.scm 774 */
											long BgL_auxz00_8847;
											long BgL_auxz00_8837;
											obj_t BgL_auxz00_8830;

											{	/* Llib/unicode.scm 774 */
												obj_t BgL_tmpz00_8848;

												if (INTEGERP(BgL_endz00_1761))
													{	/* Llib/unicode.scm 774 */
														BgL_tmpz00_8848 = BgL_endz00_1761;
													}
												else
													{
														obj_t BgL_auxz00_8851;

														BgL_auxz00_8851 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string3618z00zz__unicodez00, BINT(31550L),
															BGl_string3666z00zz__unicodez00,
															BGl_string3620z00zz__unicodez00, BgL_endz00_1761);
														FAILURE(BgL_auxz00_8851, BFALSE, BFALSE);
													}
												BgL_auxz00_8847 = (long) CINT(BgL_tmpz00_8848);
											}
											{	/* Llib/unicode.scm 774 */
												obj_t BgL_tmpz00_8839;

												if (INTEGERP(BgL_startz00_1760))
													{	/* Llib/unicode.scm 774 */
														BgL_tmpz00_8839 = BgL_startz00_1760;
													}
												else
													{
														obj_t BgL_auxz00_8842;

														BgL_auxz00_8842 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string3618z00zz__unicodez00, BINT(31550L),
															BGl_string3666z00zz__unicodez00,
															BGl_string3620z00zz__unicodez00,
															BgL_startz00_1760);
														FAILURE(BgL_auxz00_8842, BFALSE, BFALSE);
													}
												BgL_auxz00_8837 = (long) CINT(BgL_tmpz00_8839);
											}
											if (STRINGP(BgL_strz00_1747))
												{	/* Llib/unicode.scm 774 */
													BgL_auxz00_8830 = BgL_strz00_1747;
												}
											else
												{
													obj_t BgL_auxz00_8833;

													BgL_auxz00_8833 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string3618z00zz__unicodez00, BINT(31550L),
														BGl_string3666z00zz__unicodez00,
														BGl_string3661z00zz__unicodez00, BgL_strz00_1747);
													FAILURE(BgL_auxz00_8833, BFALSE, BFALSE);
												}
											return
												BGl_utf8zd2stringzd2encodez00zz__unicodez00
												(BgL_auxz00_8830, CBOOL(BgL_strictz00_1759),
												BgL_auxz00_8837, BgL_auxz00_8847);
										}
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* utf8-string-encode */
	BGL_EXPORTED_DEF obj_t BGl_utf8zd2stringzd2encodez00zz__unicodez00(obj_t
		BgL_strz00_72, bool_t BgL_strictz00_73, long BgL_startz00_74,
		long BgL_endz00_75)
	{
		{	/* Llib/unicode.scm 774 */
			{	/* Llib/unicode.scm 786 */
				bool_t BgL_test4089z00_8859;

				if ((BgL_endz00_75 > STRING_LENGTH(BgL_strz00_72)))
					{	/* Llib/unicode.scm 786 */
						BgL_test4089z00_8859 = ((bool_t) 1);
					}
				else
					{	/* Llib/unicode.scm 786 */
						if ((BgL_startz00_74 < 0L))
							{	/* Llib/unicode.scm 787 */
								BgL_test4089z00_8859 = ((bool_t) 1);
							}
						else
							{	/* Llib/unicode.scm 787 */
								BgL_test4089z00_8859 = (BgL_startz00_74 > BgL_endz00_75);
							}
					}
				if (BgL_test4089z00_8859)
					{	/* Llib/unicode.scm 789 */
						obj_t BgL_arg1699z00_1769;

						BgL_arg1699z00_1769 =
							MAKE_YOUNG_PAIR(BINT(BgL_startz00_74), BINT(BgL_endz00_75));
						return
							BGl_errorz00zz__errorz00(BGl_string3667z00zz__unicodez00,
							BGl_string3668z00zz__unicodez00, BgL_arg1699z00_1769);
					}
				else
					{	/* Llib/unicode.scm 790 */
						long BgL_lenz00_1770;

						BgL_lenz00_1770 = (BgL_endz00_75 - BgL_startz00_74);
						{	/* Llib/unicode.scm 790 */
							obj_t BgL_resz00_1771;

							{	/* Llib/unicode.scm 791 */
								long BgL_arg2058z00_2132;

								BgL_arg2058z00_2132 = (3L * BgL_lenz00_1770);
								{	/* Ieee/string.scm 172 */

									BgL_resz00_1771 =
										make_string(BgL_arg2058z00_2132, ((unsigned char) ' '));
							}}
							{	/* Llib/unicode.scm 791 */

								return
									BGl_loopze70ze7zz__unicodez00(BgL_endz00_75, BgL_strictz00_73,
									BgL_lenz00_1770, BgL_resz00_1771, BgL_strz00_72,
									BgL_startz00_74, 0L);
							}
						}
					}
			}
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zz__unicodez00(long BgL_endz00_7024,
		bool_t BgL_strictz00_7023, long BgL_lenz00_7022, obj_t BgL_resz00_7021,
		obj_t BgL_strz00_7020, long BgL_rz00_1773, long BgL_wz00_1774)
	{
		{	/* Llib/unicode.scm 792 */
		BGl_loopze70ze7zz__unicodez00:
			if ((BgL_rz00_1773 == BgL_endz00_7024))
				{	/* Llib/unicode.scm 794 */
					return bgl_string_shrink(BgL_resz00_7021, BgL_wz00_1774);
				}
			else
				{	/* Llib/unicode.scm 796 */
					unsigned char BgL_cz00_1777;

					BgL_cz00_1777 = STRING_REF(BgL_strz00_7020, BgL_rz00_1773);
					{	/* Llib/unicode.scm 796 */
						long BgL_nz00_1778;

						BgL_nz00_1778 = (BgL_cz00_1777);
						{	/* Llib/unicode.scm 797 */

							if ((BgL_nz00_1778 <= 127L))
								{	/* Llib/unicode.scm 799 */
									STRING_SET(BgL_resz00_7021, BgL_wz00_1774, BgL_cz00_1777);
									{
										long BgL_wz00_8884;
										long BgL_rz00_8882;

										BgL_rz00_8882 = (BgL_rz00_1773 + 1L);
										BgL_wz00_8884 = (BgL_wz00_1774 + 1L);
										BgL_wz00_1774 = BgL_wz00_8884;
										BgL_rz00_1773 = BgL_rz00_8882;
										goto BGl_loopze70ze7zz__unicodez00;
									}
								}
							else
								{	/* Llib/unicode.scm 799 */
									if ((BgL_nz00_1778 < 194L))
										{	/* Llib/unicode.scm 803 */
											STRING_SET(BgL_resz00_7021, BgL_wz00_1774,
												((unsigned char) 239));
											{	/* Llib/unicode.scm 783 */
												long BgL_tmpz00_8889;

												BgL_tmpz00_8889 = (BgL_wz00_1774 + 1L);
												STRING_SET(BgL_resz00_7021, BgL_tmpz00_8889,
													((unsigned char) 191));
											}
											{	/* Llib/unicode.scm 784 */
												long BgL_tmpz00_8892;

												BgL_tmpz00_8892 = (BgL_wz00_1774 + 2L);
												STRING_SET(BgL_resz00_7021, BgL_tmpz00_8892,
													((unsigned char) 189));
											}
											{
												long BgL_wz00_8897;
												long BgL_rz00_8895;

												BgL_rz00_8895 = (BgL_rz00_1773 + 1L);
												BgL_wz00_8897 = (BgL_wz00_1774 + 3L);
												BgL_wz00_1774 = BgL_wz00_8897;
												BgL_rz00_1773 = BgL_rz00_8895;
												goto BGl_loopze70ze7zz__unicodez00;
											}
										}
									else
										{	/* Llib/unicode.scm 803 */
											if ((BgL_nz00_1778 < 223L))
												{	/* Llib/unicode.scm 809 */
													bool_t BgL_test4096z00_8901;

													if (((1L + BgL_rz00_1773) < BgL_lenz00_7022))
														{	/* Llib/unicode.scm 777 */
															long BgL_nz00_4467;

															BgL_nz00_4467 =
																(STRING_REF(BgL_strz00_7020,
																	(BgL_rz00_1773 + 1L)));
															if ((BgL_nz00_4467 >= 128L))
																{	/* Llib/unicode.scm 778 */
																	BgL_test4096z00_8901 =
																		(BgL_nz00_4467 <= 191L);
																}
															else
																{	/* Llib/unicode.scm 778 */
																	BgL_test4096z00_8901 = ((bool_t) 0);
																}
														}
													else
														{	/* Llib/unicode.scm 809 */
															BgL_test4096z00_8901 = ((bool_t) 0);
														}
													if (BgL_test4096z00_8901)
														{	/* Llib/unicode.scm 809 */
															STRING_SET(BgL_resz00_7021, BgL_wz00_1774,
																BgL_cz00_1777);
															{	/* Llib/unicode.scm 813 */
																unsigned char BgL_auxz00_8914;
																long BgL_tmpz00_8912;

																BgL_auxz00_8914 =
																	STRING_REF(BgL_strz00_7020,
																	(BgL_rz00_1773 + 1L));
																BgL_tmpz00_8912 = (BgL_wz00_1774 + 1L);
																STRING_SET(BgL_resz00_7021, BgL_tmpz00_8912,
																	BgL_auxz00_8914);
															}
															{
																long BgL_wz00_8920;
																long BgL_rz00_8918;

																BgL_rz00_8918 = (BgL_rz00_1773 + 2L);
																BgL_wz00_8920 = (BgL_wz00_1774 + 2L);
																BgL_wz00_1774 = BgL_wz00_8920;
																BgL_rz00_1773 = BgL_rz00_8918;
																goto BGl_loopze70ze7zz__unicodez00;
															}
														}
													else
														{	/* Llib/unicode.scm 809 */
															STRING_SET(BgL_resz00_7021, BgL_wz00_1774,
																((unsigned char) 239));
															{	/* Llib/unicode.scm 783 */
																long BgL_tmpz00_8923;

																BgL_tmpz00_8923 = (BgL_wz00_1774 + 1L);
																STRING_SET(BgL_resz00_7021, BgL_tmpz00_8923,
																	((unsigned char) 191));
															}
															{	/* Llib/unicode.scm 784 */
																long BgL_tmpz00_8926;

																BgL_tmpz00_8926 = (BgL_wz00_1774 + 2L);
																STRING_SET(BgL_resz00_7021, BgL_tmpz00_8926,
																	((unsigned char) 189));
															}
															{
																long BgL_wz00_8931;
																long BgL_rz00_8929;

																BgL_rz00_8929 = (BgL_rz00_1773 + 1L);
																BgL_wz00_8931 = (BgL_wz00_1774 + 3L);
																BgL_wz00_1774 = BgL_wz00_8931;
																BgL_rz00_1773 = BgL_rz00_8929;
																goto BGl_loopze70ze7zz__unicodez00;
															}
														}
												}
											else
												{	/* Llib/unicode.scm 818 */
													bool_t BgL_test4099z00_8933;

													if ((BgL_nz00_1778 >= 216L))
														{	/* Llib/unicode.scm 818 */
															BgL_test4099z00_8933 = (BgL_nz00_1778 <= 219L);
														}
													else
														{	/* Llib/unicode.scm 818 */
															BgL_test4099z00_8933 = ((bool_t) 0);
														}
													if (BgL_test4099z00_8933)
														{	/* Llib/unicode.scm 820 */
															bool_t BgL_test4101z00_8937;

															if ((BgL_rz00_1773 < (BgL_lenz00_7022 - 3L)))
																{	/* Llib/unicode.scm 821 */
																	bool_t BgL_test4103z00_8941;

																	{	/* Llib/unicode.scm 777 */
																		long BgL_nz00_4507;

																		BgL_nz00_4507 =
																			(STRING_REF(BgL_strz00_7020,
																				(BgL_rz00_1773 + 1L)));
																		if ((BgL_nz00_4507 >= 220L))
																			{	/* Llib/unicode.scm 778 */
																				BgL_test4103z00_8941 =
																					(BgL_nz00_4507 <= 223L);
																			}
																		else
																			{	/* Llib/unicode.scm 778 */
																				BgL_test4103z00_8941 = ((bool_t) 0);
																			}
																	}
																	if (BgL_test4103z00_8941)
																		{	/* Llib/unicode.scm 822 */
																			bool_t BgL_test4105z00_8948;

																			{	/* Llib/unicode.scm 777 */
																				long BgL_nz00_4515;

																				BgL_nz00_4515 =
																					(STRING_REF(BgL_strz00_7020,
																						(BgL_rz00_1773 + 2L)));
																				if ((BgL_nz00_4515 >= 220L))
																					{	/* Llib/unicode.scm 778 */
																						BgL_test4105z00_8948 =
																							(BgL_nz00_4515 <= 223L);
																					}
																				else
																					{	/* Llib/unicode.scm 778 */
																						BgL_test4105z00_8948 = ((bool_t) 0);
																					}
																			}
																			if (BgL_test4105z00_8948)
																				{	/* Llib/unicode.scm 777 */
																					long BgL_nz00_4523;

																					BgL_nz00_4523 =
																						(STRING_REF(BgL_strz00_7020,
																							(BgL_rz00_1773 + 3L)));
																					if ((BgL_nz00_4523 >= 220L))
																						{	/* Llib/unicode.scm 778 */
																							BgL_test4101z00_8937 =
																								(BgL_nz00_4523 <= 223L);
																						}
																					else
																						{	/* Llib/unicode.scm 778 */
																							BgL_test4101z00_8937 =
																								((bool_t) 0);
																						}
																				}
																			else
																				{	/* Llib/unicode.scm 822 */
																					BgL_test4101z00_8937 = ((bool_t) 0);
																				}
																		}
																	else
																		{	/* Llib/unicode.scm 821 */
																			BgL_test4101z00_8937 = ((bool_t) 0);
																		}
																}
															else
																{	/* Llib/unicode.scm 820 */
																	BgL_test4101z00_8937 = ((bool_t) 0);
																}
															if (BgL_test4101z00_8937)
																{	/* Llib/unicode.scm 820 */
																	STRING_SET(BgL_resz00_7021, BgL_wz00_1774,
																		BgL_cz00_1777);
																	{	/* Llib/unicode.scm 826 */
																		unsigned char BgL_auxz00_8964;
																		long BgL_tmpz00_8962;

																		BgL_auxz00_8964 =
																			STRING_REF(BgL_strz00_7020,
																			(BgL_rz00_1773 + 1L));
																		BgL_tmpz00_8962 = (BgL_wz00_1774 + 1L);
																		STRING_SET(BgL_resz00_7021, BgL_tmpz00_8962,
																			BgL_auxz00_8964);
																	}
																	{	/* Llib/unicode.scm 827 */
																		unsigned char BgL_auxz00_8970;
																		long BgL_tmpz00_8968;

																		BgL_auxz00_8970 =
																			STRING_REF(BgL_strz00_7020,
																			(BgL_rz00_1773 + 2L));
																		BgL_tmpz00_8968 = (BgL_wz00_1774 + 2L);
																		STRING_SET(BgL_resz00_7021, BgL_tmpz00_8968,
																			BgL_auxz00_8970);
																	}
																	{	/* Llib/unicode.scm 828 */
																		unsigned char BgL_auxz00_8976;
																		long BgL_tmpz00_8974;

																		BgL_auxz00_8976 =
																			STRING_REF(BgL_strz00_7020,
																			(BgL_rz00_1773 + 3L));
																		BgL_tmpz00_8974 = (BgL_wz00_1774 + 3L);
																		STRING_SET(BgL_resz00_7021, BgL_tmpz00_8974,
																			BgL_auxz00_8976);
																	}
																	BGl_loopze70ze7zz__unicodez00(BgL_endz00_7024,
																		BgL_strictz00_7023, BgL_lenz00_7022,
																		BgL_resz00_7021, BgL_strz00_7020,
																		(BgL_rz00_1773 + 4L), (BgL_wz00_1774 + 4L));
																	STRING_SET(BgL_resz00_7021, BgL_wz00_1774,
																		((unsigned char) 239));
																	{	/* Llib/unicode.scm 783 */
																		long BgL_tmpz00_8984;

																		BgL_tmpz00_8984 = (BgL_wz00_1774 + 1L);
																		STRING_SET(BgL_resz00_7021, BgL_tmpz00_8984,
																			((unsigned char) 191));
																	}
																	{	/* Llib/unicode.scm 784 */
																		long BgL_tmpz00_8987;

																		BgL_tmpz00_8987 = (BgL_wz00_1774 + 2L);
																		STRING_SET(BgL_resz00_7021, BgL_tmpz00_8987,
																			((unsigned char) 189));
																	}
																	{
																		long BgL_wz00_8992;
																		long BgL_rz00_8990;

																		BgL_rz00_8990 = (BgL_rz00_1773 + 1L);
																		BgL_wz00_8992 = (BgL_wz00_1774 + 3L);
																		BgL_wz00_1774 = BgL_wz00_8992;
																		BgL_rz00_1773 = BgL_rz00_8990;
																		goto BGl_loopze70ze7zz__unicodez00;
																	}
																}
															else
																{	/* Llib/unicode.scm 820 */
																	return BFALSE;
																}
														}
													else
														{	/* Llib/unicode.scm 818 */
															if ((BgL_nz00_1778 <= 223L))
																{	/* Llib/unicode.scm 835 */
																	bool_t BgL_test4109z00_8996;

																	if (((1L + BgL_rz00_1773) < BgL_lenz00_7022))
																		{	/* Llib/unicode.scm 777 */
																			long BgL_nz00_4576;

																			BgL_nz00_4576 =
																				(STRING_REF(BgL_strz00_7020,
																					(BgL_rz00_1773 + 1L)));
																			if ((BgL_nz00_4576 >= 128L))
																				{	/* Llib/unicode.scm 778 */
																					BgL_test4109z00_8996 =
																						(BgL_nz00_4576 <= 191L);
																				}
																			else
																				{	/* Llib/unicode.scm 778 */
																					BgL_test4109z00_8996 = ((bool_t) 0);
																				}
																		}
																	else
																		{	/* Llib/unicode.scm 835 */
																			BgL_test4109z00_8996 = ((bool_t) 0);
																		}
																	if (BgL_test4109z00_8996)
																		{	/* Llib/unicode.scm 835 */
																			STRING_SET(BgL_resz00_7021, BgL_wz00_1774,
																				BgL_cz00_1777);
																			{	/* Llib/unicode.scm 839 */
																				unsigned char BgL_auxz00_9009;
																				long BgL_tmpz00_9007;

																				BgL_auxz00_9009 =
																					STRING_REF(BgL_strz00_7020,
																					(BgL_rz00_1773 + 1L));
																				BgL_tmpz00_9007 = (BgL_wz00_1774 + 1L);
																				STRING_SET(BgL_resz00_7021,
																					BgL_tmpz00_9007, BgL_auxz00_9009);
																			}
																			{
																				long BgL_wz00_9015;
																				long BgL_rz00_9013;

																				BgL_rz00_9013 = (BgL_rz00_1773 + 2L);
																				BgL_wz00_9015 = (BgL_wz00_1774 + 2L);
																				BgL_wz00_1774 = BgL_wz00_9015;
																				BgL_rz00_1773 = BgL_rz00_9013;
																				goto BGl_loopze70ze7zz__unicodez00;
																			}
																		}
																	else
																		{	/* Llib/unicode.scm 835 */
																			STRING_SET(BgL_resz00_7021, BgL_wz00_1774,
																				((unsigned char) 239));
																			{	/* Llib/unicode.scm 783 */
																				long BgL_tmpz00_9018;

																				BgL_tmpz00_9018 = (BgL_wz00_1774 + 1L);
																				STRING_SET(BgL_resz00_7021,
																					BgL_tmpz00_9018,
																					((unsigned char) 191));
																			}
																			{	/* Llib/unicode.scm 784 */
																				long BgL_tmpz00_9021;

																				BgL_tmpz00_9021 = (BgL_wz00_1774 + 2L);
																				STRING_SET(BgL_resz00_7021,
																					BgL_tmpz00_9021,
																					((unsigned char) 189));
																			}
																			{
																				long BgL_wz00_9026;
																				long BgL_rz00_9024;

																				BgL_rz00_9024 = (BgL_rz00_1773 + 1L);
																				BgL_wz00_9026 = (BgL_wz00_1774 + 3L);
																				BgL_wz00_1774 = BgL_wz00_9026;
																				BgL_rz00_1773 = BgL_rz00_9024;
																				goto BGl_loopze70ze7zz__unicodez00;
																			}
																		}
																}
															else
																{	/* Llib/unicode.scm 833 */
																	if ((BgL_nz00_1778 <= 239L))
																		{	/* Llib/unicode.scm 846 */
																			bool_t BgL_test4113z00_9030;

																			if (
																				(BgL_rz00_1773 <
																					(BgL_lenz00_7022 - 2L)))
																				{	/* Llib/unicode.scm 847 */
																					bool_t BgL_test4115z00_9034;

																					{	/* Llib/unicode.scm 777 */
																						long BgL_nz00_4615;

																						BgL_nz00_4615 =
																							(STRING_REF(BgL_strz00_7020,
																								(BgL_rz00_1773 + 1L)));
																						if ((BgL_nz00_4615 >= 128L))
																							{	/* Llib/unicode.scm 778 */
																								BgL_test4115z00_9034 =
																									(BgL_nz00_4615 <= 191L);
																							}
																						else
																							{	/* Llib/unicode.scm 778 */
																								BgL_test4115z00_9034 =
																									((bool_t) 0);
																							}
																					}
																					if (BgL_test4115z00_9034)
																						{	/* Llib/unicode.scm 777 */
																							long BgL_nz00_4623;

																							BgL_nz00_4623 =
																								(STRING_REF(BgL_strz00_7020,
																									(BgL_rz00_1773 + 2L)));
																							if ((BgL_nz00_4623 >= 128L))
																								{	/* Llib/unicode.scm 778 */
																									BgL_test4113z00_9030 =
																										(BgL_nz00_4623 <= 191L);
																								}
																							else
																								{	/* Llib/unicode.scm 778 */
																									BgL_test4113z00_9030 =
																										((bool_t) 0);
																								}
																						}
																					else
																						{	/* Llib/unicode.scm 847 */
																							BgL_test4113z00_9030 =
																								((bool_t) 0);
																						}
																				}
																			else
																				{	/* Llib/unicode.scm 846 */
																					BgL_test4113z00_9030 = ((bool_t) 0);
																				}
																			if (BgL_test4113z00_9030)
																				{	/* Llib/unicode.scm 846 */
																					STRING_SET(BgL_resz00_7021,
																						BgL_wz00_1774, BgL_cz00_1777);
																					{	/* Llib/unicode.scm 851 */
																						unsigned char BgL_auxz00_9050;
																						long BgL_tmpz00_9048;

																						BgL_auxz00_9050 =
																							STRING_REF(BgL_strz00_7020,
																							(BgL_rz00_1773 + 1L));
																						BgL_tmpz00_9048 =
																							(BgL_wz00_1774 + 1L);
																						STRING_SET(BgL_resz00_7021,
																							BgL_tmpz00_9048, BgL_auxz00_9050);
																					}
																					{	/* Llib/unicode.scm 852 */
																						unsigned char BgL_auxz00_9056;
																						long BgL_tmpz00_9054;

																						BgL_auxz00_9056 =
																							STRING_REF(BgL_strz00_7020,
																							(BgL_rz00_1773 + 2L));
																						BgL_tmpz00_9054 =
																							(BgL_wz00_1774 + 2L);
																						STRING_SET(BgL_resz00_7021,
																							BgL_tmpz00_9054, BgL_auxz00_9056);
																					}
																					{
																						long BgL_wz00_9062;
																						long BgL_rz00_9060;

																						BgL_rz00_9060 =
																							(BgL_rz00_1773 + 3L);
																						BgL_wz00_9062 =
																							(BgL_wz00_1774 + 3L);
																						BgL_wz00_1774 = BgL_wz00_9062;
																						BgL_rz00_1773 = BgL_rz00_9060;
																						goto BGl_loopze70ze7zz__unicodez00;
																					}
																				}
																			else
																				{	/* Llib/unicode.scm 846 */
																					STRING_SET(BgL_resz00_7021,
																						BgL_wz00_1774,
																						((unsigned char) 239));
																					{	/* Llib/unicode.scm 783 */
																						long BgL_tmpz00_9065;

																						BgL_tmpz00_9065 =
																							(BgL_wz00_1774 + 1L);
																						STRING_SET(BgL_resz00_7021,
																							BgL_tmpz00_9065,
																							((unsigned char) 191));
																					}
																					{	/* Llib/unicode.scm 784 */
																						long BgL_tmpz00_9068;

																						BgL_tmpz00_9068 =
																							(BgL_wz00_1774 + 2L);
																						STRING_SET(BgL_resz00_7021,
																							BgL_tmpz00_9068,
																							((unsigned char) 189));
																					}
																					{
																						long BgL_wz00_9073;
																						long BgL_rz00_9071;

																						BgL_rz00_9071 =
																							(BgL_rz00_1773 + 1L);
																						BgL_wz00_9073 =
																							(BgL_wz00_1774 + 3L);
																						BgL_wz00_1774 = BgL_wz00_9073;
																						BgL_rz00_1773 = BgL_rz00_9071;
																						goto BGl_loopze70ze7zz__unicodez00;
																					}
																				}
																		}
																	else
																		{	/* Llib/unicode.scm 844 */
																			if ((BgL_nz00_1778 == 240L))
																				{	/* Llib/unicode.scm 859 */
																					bool_t BgL_test4119z00_9077;

																					if (
																						(BgL_rz00_1773 <
																							(BgL_lenz00_7022 - 3L)))
																						{	/* Llib/unicode.scm 860 */
																							bool_t BgL_test4121z00_9081;

																							{	/* Llib/unicode.scm 777 */
																								long BgL_nz00_4669;

																								BgL_nz00_4669 =
																									(STRING_REF(BgL_strz00_7020,
																										(BgL_rz00_1773 + 1L)));
																								if ((BgL_nz00_4669 >= 144L))
																									{	/* Llib/unicode.scm 778 */
																										BgL_test4121z00_9081 =
																											(BgL_nz00_4669 <= 191L);
																									}
																								else
																									{	/* Llib/unicode.scm 778 */
																										BgL_test4121z00_9081 =
																											((bool_t) 0);
																									}
																							}
																							if (BgL_test4121z00_9081)
																								{	/* Llib/unicode.scm 861 */
																									bool_t BgL_test4123z00_9088;

																									{	/* Llib/unicode.scm 777 */
																										long BgL_nz00_4677;

																										BgL_nz00_4677 =
																											(STRING_REF
																											(BgL_strz00_7020,
																												(BgL_rz00_1773 + 2L)));
																										if ((BgL_nz00_4677 >= 128L))
																											{	/* Llib/unicode.scm 778 */
																												BgL_test4123z00_9088 =
																													(BgL_nz00_4677 <=
																													191L);
																											}
																										else
																											{	/* Llib/unicode.scm 778 */
																												BgL_test4123z00_9088 =
																													((bool_t) 0);
																											}
																									}
																									if (BgL_test4123z00_9088)
																										{	/* Llib/unicode.scm 777 */
																											long BgL_nz00_4685;

																											BgL_nz00_4685 =
																												(STRING_REF
																												(BgL_strz00_7020,
																													(BgL_rz00_1773 +
																														3L)));
																											if ((BgL_nz00_4685 >=
																													128L))
																												{	/* Llib/unicode.scm 778 */
																													BgL_test4119z00_9077 =
																														(BgL_nz00_4685 <=
																														191L);
																												}
																											else
																												{	/* Llib/unicode.scm 778 */
																													BgL_test4119z00_9077 =
																														((bool_t) 0);
																												}
																										}
																									else
																										{	/* Llib/unicode.scm 861 */
																											BgL_test4119z00_9077 =
																												((bool_t) 0);
																										}
																								}
																							else
																								{	/* Llib/unicode.scm 860 */
																									BgL_test4119z00_9077 =
																										((bool_t) 0);
																								}
																						}
																					else
																						{	/* Llib/unicode.scm 859 */
																							BgL_test4119z00_9077 =
																								((bool_t) 0);
																						}
																					if (BgL_test4119z00_9077)
																						{	/* Llib/unicode.scm 859 */
																							STRING_SET(BgL_resz00_7021,
																								BgL_wz00_1774, BgL_cz00_1777);
																							{	/* Llib/unicode.scm 865 */
																								unsigned char BgL_auxz00_9104;
																								long BgL_tmpz00_9102;

																								BgL_auxz00_9104 =
																									STRING_REF(BgL_strz00_7020,
																									(BgL_rz00_1773 + 1L));
																								BgL_tmpz00_9102 =
																									(BgL_wz00_1774 + 1L);
																								STRING_SET(BgL_resz00_7021,
																									BgL_tmpz00_9102,
																									BgL_auxz00_9104);
																							}
																							{	/* Llib/unicode.scm 866 */
																								unsigned char BgL_auxz00_9110;
																								long BgL_tmpz00_9108;

																								BgL_auxz00_9110 =
																									STRING_REF(BgL_strz00_7020,
																									(BgL_rz00_1773 + 2L));
																								BgL_tmpz00_9108 =
																									(BgL_wz00_1774 + 2L);
																								STRING_SET(BgL_resz00_7021,
																									BgL_tmpz00_9108,
																									BgL_auxz00_9110);
																							}
																							{	/* Llib/unicode.scm 867 */
																								unsigned char BgL_auxz00_9116;
																								long BgL_tmpz00_9114;

																								BgL_auxz00_9116 =
																									STRING_REF(BgL_strz00_7020,
																									(BgL_rz00_1773 + 3L));
																								BgL_tmpz00_9114 =
																									(BgL_wz00_1774 + 3L);
																								STRING_SET(BgL_resz00_7021,
																									BgL_tmpz00_9114,
																									BgL_auxz00_9116);
																							}
																							{
																								long BgL_wz00_9122;
																								long BgL_rz00_9120;

																								BgL_rz00_9120 =
																									(BgL_rz00_1773 + 4L);
																								BgL_wz00_9122 =
																									(BgL_wz00_1774 + 4L);
																								BgL_wz00_1774 = BgL_wz00_9122;
																								BgL_rz00_1773 = BgL_rz00_9120;
																								goto
																									BGl_loopze70ze7zz__unicodez00;
																							}
																						}
																					else
																						{	/* Llib/unicode.scm 859 */
																							STRING_SET(BgL_resz00_7021,
																								BgL_wz00_1774,
																								((unsigned char) 239));
																							{	/* Llib/unicode.scm 783 */
																								long BgL_tmpz00_9125;

																								BgL_tmpz00_9125 =
																									(BgL_wz00_1774 + 1L);
																								STRING_SET(BgL_resz00_7021,
																									BgL_tmpz00_9125,
																									((unsigned char) 191));
																							}
																							{	/* Llib/unicode.scm 784 */
																								long BgL_tmpz00_9128;

																								BgL_tmpz00_9128 =
																									(BgL_wz00_1774 + 2L);
																								STRING_SET(BgL_resz00_7021,
																									BgL_tmpz00_9128,
																									((unsigned char) 189));
																							}
																							{
																								long BgL_wz00_9133;
																								long BgL_rz00_9131;

																								BgL_rz00_9131 =
																									(BgL_rz00_1773 + 1L);
																								BgL_wz00_9133 =
																									(BgL_wz00_1774 + 3L);
																								BgL_wz00_1774 = BgL_wz00_9133;
																								BgL_rz00_1773 = BgL_rz00_9131;
																								goto
																									BGl_loopze70ze7zz__unicodez00;
																							}
																						}
																				}
																			else
																				{	/* Llib/unicode.scm 872 */
																					bool_t BgL_test4126z00_9135;

																					if ((BgL_nz00_1778 == 244L))
																						{	/* Llib/unicode.scm 872 */
																							BgL_test4126z00_9135 =
																								((bool_t) 1);
																						}
																					else
																						{	/* Llib/unicode.scm 873 */
																							bool_t BgL_test4128z00_9138;

																							{	/* Llib/unicode.scm 873 */
																								bool_t BgL__ortest_1051z00_2129;

																								BgL__ortest_1051z00_2129 =
																									(BgL_nz00_1778 == 248L);
																								if (BgL__ortest_1051z00_2129)
																									{	/* Llib/unicode.scm 873 */
																										BgL_test4128z00_9138 =
																											BgL__ortest_1051z00_2129;
																									}
																								else
																									{	/* Llib/unicode.scm 873 */
																										BgL_test4128z00_9138 =
																											(BgL_nz00_1778 == 252L);
																									}
																							}
																							if (BgL_test4128z00_9138)
																								{	/* Llib/unicode.scm 873 */
																									if (BgL_strictz00_7023)
																										{	/* Llib/unicode.scm 873 */
																											BgL_test4126z00_9135 =
																												((bool_t) 0);
																										}
																									else
																										{	/* Llib/unicode.scm 873 */
																											BgL_test4126z00_9135 =
																												((bool_t) 1);
																										}
																								}
																							else
																								{	/* Llib/unicode.scm 873 */
																									BgL_test4126z00_9135 =
																										((bool_t) 0);
																								}
																						}
																					if (BgL_test4126z00_9135)
																						{	/* Llib/unicode.scm 875 */
																							bool_t BgL_test4131z00_9143;

																							if (
																								(BgL_rz00_1773 <
																									(BgL_lenz00_7022 - 3L)))
																								{	/* Llib/unicode.scm 876 */
																									bool_t BgL_test4133z00_9147;

																									{	/* Llib/unicode.scm 777 */
																										long BgL_nz00_4740;

																										BgL_nz00_4740 =
																											(STRING_REF
																											(BgL_strz00_7020,
																												(BgL_rz00_1773 + 1L)));
																										if ((BgL_nz00_4740 >= 128L))
																											{	/* Llib/unicode.scm 778 */
																												BgL_test4133z00_9147 =
																													(BgL_nz00_4740 <=
																													191L);
																											}
																										else
																											{	/* Llib/unicode.scm 778 */
																												BgL_test4133z00_9147 =
																													((bool_t) 0);
																											}
																									}
																									if (BgL_test4133z00_9147)
																										{	/* Llib/unicode.scm 877 */
																											bool_t
																												BgL_test4135z00_9154;
																											{	/* Llib/unicode.scm 777 */
																												long BgL_nz00_4748;

																												BgL_nz00_4748 =
																													(STRING_REF
																													(BgL_strz00_7020,
																														(BgL_rz00_1773 +
																															2L)));
																												if ((BgL_nz00_4748 >=
																														128L))
																													{	/* Llib/unicode.scm 778 */
																														BgL_test4135z00_9154
																															=
																															(BgL_nz00_4748 <=
																															191L);
																													}
																												else
																													{	/* Llib/unicode.scm 778 */
																														BgL_test4135z00_9154
																															= ((bool_t) 0);
																													}
																											}
																											if (BgL_test4135z00_9154)
																												{	/* Llib/unicode.scm 777 */
																													long BgL_nz00_4756;

																													BgL_nz00_4756 =
																														(STRING_REF
																														(BgL_strz00_7020,
																															(BgL_rz00_1773 +
																																3L)));
																													if ((BgL_nz00_4756 >=
																															128L))
																														{	/* Llib/unicode.scm 778 */
																															BgL_test4131z00_9143
																																=
																																(BgL_nz00_4756
																																<= 191L);
																														}
																													else
																														{	/* Llib/unicode.scm 778 */
																															BgL_test4131z00_9143
																																= ((bool_t) 0);
																														}
																												}
																											else
																												{	/* Llib/unicode.scm 877 */
																													BgL_test4131z00_9143 =
																														((bool_t) 0);
																												}
																										}
																									else
																										{	/* Llib/unicode.scm 876 */
																											BgL_test4131z00_9143 =
																												((bool_t) 0);
																										}
																								}
																							else
																								{	/* Llib/unicode.scm 875 */
																									BgL_test4131z00_9143 =
																										((bool_t) 0);
																								}
																							if (BgL_test4131z00_9143)
																								{	/* Llib/unicode.scm 875 */
																									STRING_SET(BgL_resz00_7021,
																										BgL_wz00_1774,
																										BgL_cz00_1777);
																									{	/* Llib/unicode.scm 881 */
																										unsigned char
																											BgL_auxz00_9170;
																										long BgL_tmpz00_9168;

																										BgL_auxz00_9170 =
																											STRING_REF
																											(BgL_strz00_7020,
																											(BgL_rz00_1773 + 1L));
																										BgL_tmpz00_9168 =
																											(BgL_wz00_1774 + 1L);
																										STRING_SET(BgL_resz00_7021,
																											BgL_tmpz00_9168,
																											BgL_auxz00_9170);
																									}
																									{	/* Llib/unicode.scm 882 */
																										unsigned char
																											BgL_auxz00_9176;
																										long BgL_tmpz00_9174;

																										BgL_auxz00_9176 =
																											STRING_REF
																											(BgL_strz00_7020,
																											(BgL_rz00_1773 + 2L));
																										BgL_tmpz00_9174 =
																											(BgL_wz00_1774 + 2L);
																										STRING_SET(BgL_resz00_7021,
																											BgL_tmpz00_9174,
																											BgL_auxz00_9176);
																									}
																									{	/* Llib/unicode.scm 883 */
																										unsigned char
																											BgL_auxz00_9182;
																										long BgL_tmpz00_9180;

																										BgL_auxz00_9182 =
																											STRING_REF
																											(BgL_strz00_7020,
																											(BgL_rz00_1773 + 3L));
																										BgL_tmpz00_9180 =
																											(BgL_wz00_1774 + 3L);
																										STRING_SET(BgL_resz00_7021,
																											BgL_tmpz00_9180,
																											BgL_auxz00_9182);
																									}
																									{
																										long BgL_wz00_9188;
																										long BgL_rz00_9186;

																										BgL_rz00_9186 =
																											(BgL_rz00_1773 + 4L);
																										BgL_wz00_9188 =
																											(BgL_wz00_1774 + 4L);
																										BgL_wz00_1774 =
																											BgL_wz00_9188;
																										BgL_rz00_1773 =
																											BgL_rz00_9186;
																										goto
																											BGl_loopze70ze7zz__unicodez00;
																									}
																								}
																							else
																								{	/* Llib/unicode.scm 875 */
																									STRING_SET(BgL_resz00_7021,
																										BgL_wz00_1774,
																										((unsigned char) 239));
																									{	/* Llib/unicode.scm 783 */
																										long BgL_tmpz00_9191;

																										BgL_tmpz00_9191 =
																											(BgL_wz00_1774 + 1L);
																										STRING_SET(BgL_resz00_7021,
																											BgL_tmpz00_9191,
																											((unsigned char) 191));
																									}
																									{	/* Llib/unicode.scm 784 */
																										long BgL_tmpz00_9194;

																										BgL_tmpz00_9194 =
																											(BgL_wz00_1774 + 2L);
																										STRING_SET(BgL_resz00_7021,
																											BgL_tmpz00_9194,
																											((unsigned char) 189));
																									}
																									{
																										long BgL_wz00_9199;
																										long BgL_rz00_9197;

																										BgL_rz00_9197 =
																											(BgL_rz00_1773 + 1L);
																										BgL_wz00_9199 =
																											(BgL_wz00_1774 + 3L);
																										BgL_wz00_1774 =
																											BgL_wz00_9199;
																										BgL_rz00_1773 =
																											BgL_rz00_9197;
																										goto
																											BGl_loopze70ze7zz__unicodez00;
																									}
																								}
																						}
																					else
																						{	/* Llib/unicode.scm 872 */
																							if ((BgL_nz00_1778 <= 247L))
																								{	/* Llib/unicode.scm 890 */
																									bool_t BgL_test4139z00_9203;

																									if (
																										(BgL_rz00_1773 <
																											(BgL_lenz00_7022 - 3L)))
																										{	/* Llib/unicode.scm 891 */
																											bool_t
																												BgL_test4141z00_9207;
																											{	/* Llib/unicode.scm 777 */
																												long BgL_nz00_4809;

																												BgL_nz00_4809 =
																													(STRING_REF
																													(BgL_strz00_7020,
																														(BgL_rz00_1773 +
																															1L)));
																												if ((BgL_nz00_4809 >=
																														128L))
																													{	/* Llib/unicode.scm 778 */
																														BgL_test4141z00_9207
																															=
																															(BgL_nz00_4809 <=
																															191L);
																													}
																												else
																													{	/* Llib/unicode.scm 778 */
																														BgL_test4141z00_9207
																															= ((bool_t) 0);
																													}
																											}
																											if (BgL_test4141z00_9207)
																												{	/* Llib/unicode.scm 892 */
																													bool_t
																														BgL_test4143z00_9214;
																													{	/* Llib/unicode.scm 777 */
																														long BgL_nz00_4817;

																														BgL_nz00_4817 =
																															(STRING_REF
																															(BgL_strz00_7020,
																																(BgL_rz00_1773 +
																																	2L)));
																														if ((BgL_nz00_4817
																																>= 128L))
																															{	/* Llib/unicode.scm 778 */
																																BgL_test4143z00_9214
																																	=
																																	(BgL_nz00_4817
																																	<= 191L);
																															}
																														else
																															{	/* Llib/unicode.scm 778 */
																																BgL_test4143z00_9214
																																	=
																																	((bool_t) 0);
																															}
																													}
																													if (BgL_test4143z00_9214)
																														{	/* Llib/unicode.scm 777 */
																															long
																																BgL_nz00_4825;
																															BgL_nz00_4825 =
																																(STRING_REF
																																(BgL_strz00_7020,
																																	(BgL_rz00_1773
																																		+ 3L)));
																															if ((BgL_nz00_4825
																																	>= 128L))
																																{	/* Llib/unicode.scm 778 */
																																	BgL_test4139z00_9203
																																		=
																																		(BgL_nz00_4825
																																		<= 191L);
																																}
																															else
																																{	/* Llib/unicode.scm 778 */
																																	BgL_test4139z00_9203
																																		=
																																		((bool_t)
																																		0);
																																}
																														}
																													else
																														{	/* Llib/unicode.scm 892 */
																															BgL_test4139z00_9203
																																= ((bool_t) 0);
																														}
																												}
																											else
																												{	/* Llib/unicode.scm 891 */
																													BgL_test4139z00_9203 =
																														((bool_t) 0);
																												}
																										}
																									else
																										{	/* Llib/unicode.scm 890 */
																											BgL_test4139z00_9203 =
																												((bool_t) 0);
																										}
																									if (BgL_test4139z00_9203)
																										{	/* Llib/unicode.scm 890 */
																											STRING_SET
																												(BgL_resz00_7021,
																												BgL_wz00_1774,
																												BgL_cz00_1777);
																											{	/* Llib/unicode.scm 896 */
																												unsigned char
																													BgL_auxz00_9230;
																												long BgL_tmpz00_9228;

																												BgL_auxz00_9230 =
																													STRING_REF
																													(BgL_strz00_7020,
																													(BgL_rz00_1773 + 1L));
																												BgL_tmpz00_9228 =
																													(BgL_wz00_1774 + 1L);
																												STRING_SET
																													(BgL_resz00_7021,
																													BgL_tmpz00_9228,
																													BgL_auxz00_9230);
																											}
																											{	/* Llib/unicode.scm 897 */
																												unsigned char
																													BgL_auxz00_9236;
																												long BgL_tmpz00_9234;

																												BgL_auxz00_9236 =
																													STRING_REF
																													(BgL_strz00_7020,
																													(BgL_rz00_1773 + 2L));
																												BgL_tmpz00_9234 =
																													(BgL_wz00_1774 + 2L);
																												STRING_SET
																													(BgL_resz00_7021,
																													BgL_tmpz00_9234,
																													BgL_auxz00_9236);
																											}
																											{	/* Llib/unicode.scm 898 */
																												unsigned char
																													BgL_auxz00_9242;
																												long BgL_tmpz00_9240;

																												BgL_auxz00_9242 =
																													STRING_REF
																													(BgL_strz00_7020,
																													(BgL_rz00_1773 + 3L));
																												BgL_tmpz00_9240 =
																													(BgL_wz00_1774 + 3L);
																												STRING_SET
																													(BgL_resz00_7021,
																													BgL_tmpz00_9240,
																													BgL_auxz00_9242);
																											}
																											{
																												long BgL_wz00_9248;
																												long BgL_rz00_9246;

																												BgL_rz00_9246 =
																													(BgL_rz00_1773 + 4L);
																												BgL_wz00_9248 =
																													(BgL_wz00_1774 + 4L);
																												BgL_wz00_1774 =
																													BgL_wz00_9248;
																												BgL_rz00_1773 =
																													BgL_rz00_9246;
																												goto
																													BGl_loopze70ze7zz__unicodez00;
																											}
																										}
																									else
																										{	/* Llib/unicode.scm 890 */
																											STRING_SET
																												(BgL_resz00_7021,
																												BgL_wz00_1774,
																												((unsigned char) 239));
																											{	/* Llib/unicode.scm 783 */
																												long BgL_tmpz00_9251;

																												BgL_tmpz00_9251 =
																													(BgL_wz00_1774 + 1L);
																												STRING_SET
																													(BgL_resz00_7021,
																													BgL_tmpz00_9251,
																													((unsigned char)
																														191));
																											}
																											{	/* Llib/unicode.scm 784 */
																												long BgL_tmpz00_9254;

																												BgL_tmpz00_9254 =
																													(BgL_wz00_1774 + 2L);
																												STRING_SET
																													(BgL_resz00_7021,
																													BgL_tmpz00_9254,
																													((unsigned char)
																														189));
																											}
																											{
																												long BgL_wz00_9259;
																												long BgL_rz00_9257;

																												BgL_rz00_9257 =
																													(BgL_rz00_1773 + 1L);
																												BgL_wz00_9259 =
																													(BgL_wz00_1774 + 3L);
																												BgL_wz00_1774 =
																													BgL_wz00_9259;
																												BgL_rz00_1773 =
																													BgL_rz00_9257;
																												goto
																													BGl_loopze70ze7zz__unicodez00;
																											}
																										}
																								}
																							else
																								{	/* Llib/unicode.scm 888 */
																									if ((BgL_nz00_1778 <= 251L))
																										{	/* Llib/unicode.scm 904 */
																											bool_t
																												BgL_test4147z00_9263;
																											if ((BgL_rz00_1773 <
																													(BgL_lenz00_7022 -
																														4L)))
																												{	/* Llib/unicode.scm 905 */
																													bool_t
																														BgL_test4149z00_9267;
																													{	/* Llib/unicode.scm 777 */
																														long BgL_nz00_4878;

																														BgL_nz00_4878 =
																															(STRING_REF
																															(BgL_strz00_7020,
																																(BgL_rz00_1773 +
																																	1L)));
																														if ((BgL_nz00_4878
																																>= 128L))
																															{	/* Llib/unicode.scm 778 */
																																BgL_test4149z00_9267
																																	=
																																	(BgL_nz00_4878
																																	<= 191L);
																															}
																														else
																															{	/* Llib/unicode.scm 778 */
																																BgL_test4149z00_9267
																																	=
																																	((bool_t) 0);
																															}
																													}
																													if (BgL_test4149z00_9267)
																														{	/* Llib/unicode.scm 906 */
																															bool_t
																																BgL_test4151z00_9274;
																															{	/* Llib/unicode.scm 777 */
																																long
																																	BgL_nz00_4886;
																																BgL_nz00_4886 =
																																	(STRING_REF
																																	(BgL_strz00_7020,
																																		(BgL_rz00_1773
																																			+ 2L)));
																																if (
																																	(BgL_nz00_4886
																																		>= 128L))
																																	{	/* Llib/unicode.scm 778 */
																																		BgL_test4151z00_9274
																																			=
																																			(BgL_nz00_4886
																																			<= 191L);
																																	}
																																else
																																	{	/* Llib/unicode.scm 778 */
																																		BgL_test4151z00_9274
																																			=
																																			((bool_t)
																																			0);
																																	}
																															}
																															if (BgL_test4151z00_9274)
																																{	/* Llib/unicode.scm 907 */
																																	bool_t
																																		BgL_test4153z00_9281;
																																	{	/* Llib/unicode.scm 777 */
																																		long
																																			BgL_nz00_4894;
																																		BgL_nz00_4894
																																			=
																																			(STRING_REF
																																			(BgL_strz00_7020,
																																				(BgL_rz00_1773
																																					+
																																					3L)));
																																		if (
																																			(BgL_nz00_4894
																																				>=
																																				128L))
																																			{	/* Llib/unicode.scm 778 */
																																				BgL_test4153z00_9281
																																					=
																																					(BgL_nz00_4894
																																					<=
																																					191L);
																																			}
																																		else
																																			{	/* Llib/unicode.scm 778 */
																																				BgL_test4153z00_9281
																																					=
																																					(
																																					(bool_t)
																																					0);
																																			}
																																	}
																																	if (BgL_test4153z00_9281)
																																		{	/* Llib/unicode.scm 777 */
																																			long
																																				BgL_nz00_4902;
																																			BgL_nz00_4902
																																				=
																																				(STRING_REF
																																				(BgL_strz00_7020,
																																					(BgL_rz00_1773
																																						+
																																						4L)));
																																			if (
																																				(BgL_nz00_4902
																																					>=
																																					128L))
																																				{	/* Llib/unicode.scm 778 */
																																					BgL_test4147z00_9263
																																						=
																																						(BgL_nz00_4902
																																						<=
																																						191L);
																																				}
																																			else
																																				{	/* Llib/unicode.scm 778 */
																																					BgL_test4147z00_9263
																																						=
																																						(
																																						(bool_t)
																																						0);
																																				}
																																		}
																																	else
																																		{	/* Llib/unicode.scm 907 */
																																			BgL_test4147z00_9263
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																}
																															else
																																{	/* Llib/unicode.scm 906 */
																																	BgL_test4147z00_9263
																																		=
																																		((bool_t)
																																		0);
																																}
																														}
																													else
																														{	/* Llib/unicode.scm 905 */
																															BgL_test4147z00_9263
																																= ((bool_t) 0);
																														}
																												}
																											else
																												{	/* Llib/unicode.scm 904 */
																													BgL_test4147z00_9263 =
																														((bool_t) 0);
																												}
																											if (BgL_test4147z00_9263)
																												{	/* Llib/unicode.scm 904 */
																													STRING_SET
																														(BgL_resz00_7021,
																														BgL_wz00_1774,
																														BgL_cz00_1777);
																													{	/* Llib/unicode.scm 911 */
																														unsigned char
																															BgL_auxz00_9297;
																														long
																															BgL_tmpz00_9295;
																														BgL_auxz00_9297 =
																															STRING_REF
																															(BgL_strz00_7020,
																															(BgL_rz00_1773 +
																																1L));
																														BgL_tmpz00_9295 =
																															(BgL_wz00_1774 +
																															1L);
																														STRING_SET
																															(BgL_resz00_7021,
																															BgL_tmpz00_9295,
																															BgL_auxz00_9297);
																													}
																													{	/* Llib/unicode.scm 912 */
																														unsigned char
																															BgL_auxz00_9303;
																														long
																															BgL_tmpz00_9301;
																														BgL_auxz00_9303 =
																															STRING_REF
																															(BgL_strz00_7020,
																															(BgL_rz00_1773 +
																																2L));
																														BgL_tmpz00_9301 =
																															(BgL_wz00_1774 +
																															2L);
																														STRING_SET
																															(BgL_resz00_7021,
																															BgL_tmpz00_9301,
																															BgL_auxz00_9303);
																													}
																													{	/* Llib/unicode.scm 913 */
																														unsigned char
																															BgL_auxz00_9309;
																														long
																															BgL_tmpz00_9307;
																														BgL_auxz00_9309 =
																															STRING_REF
																															(BgL_strz00_7020,
																															(BgL_rz00_1773 +
																																3L));
																														BgL_tmpz00_9307 =
																															(BgL_wz00_1774 +
																															3L);
																														STRING_SET
																															(BgL_resz00_7021,
																															BgL_tmpz00_9307,
																															BgL_auxz00_9309);
																													}
																													{	/* Llib/unicode.scm 914 */
																														unsigned char
																															BgL_auxz00_9315;
																														long
																															BgL_tmpz00_9313;
																														BgL_auxz00_9315 =
																															STRING_REF
																															(BgL_strz00_7020,
																															(BgL_rz00_1773 +
																																4L));
																														BgL_tmpz00_9313 =
																															(BgL_wz00_1774 +
																															4L);
																														STRING_SET
																															(BgL_resz00_7021,
																															BgL_tmpz00_9313,
																															BgL_auxz00_9315);
																													}
																													{
																														long BgL_wz00_9321;
																														long BgL_rz00_9319;

																														BgL_rz00_9319 =
																															(BgL_rz00_1773 +
																															5L);
																														BgL_wz00_9321 =
																															(BgL_wz00_1774 +
																															5L);
																														BgL_wz00_1774 =
																															BgL_wz00_9321;
																														BgL_rz00_1773 =
																															BgL_rz00_9319;
																														goto
																															BGl_loopze70ze7zz__unicodez00;
																													}
																												}
																											else
																												{	/* Llib/unicode.scm 904 */
																													STRING_SET
																														(BgL_resz00_7021,
																														BgL_wz00_1774,
																														((unsigned char)
																															239));
																													{	/* Llib/unicode.scm 783 */
																														long
																															BgL_tmpz00_9324;
																														BgL_tmpz00_9324 =
																															(BgL_wz00_1774 +
																															1L);
																														STRING_SET
																															(BgL_resz00_7021,
																															BgL_tmpz00_9324,
																															((unsigned char)
																																191));
																													}
																													{	/* Llib/unicode.scm 784 */
																														long
																															BgL_tmpz00_9327;
																														BgL_tmpz00_9327 =
																															(BgL_wz00_1774 +
																															2L);
																														STRING_SET
																															(BgL_resz00_7021,
																															BgL_tmpz00_9327,
																															((unsigned char)
																																189));
																													}
																													{
																														long BgL_wz00_9332;
																														long BgL_rz00_9330;

																														BgL_rz00_9330 =
																															(BgL_rz00_1773 +
																															1L);
																														BgL_wz00_9332 =
																															(BgL_wz00_1774 +
																															3L);
																														BgL_wz00_1774 =
																															BgL_wz00_9332;
																														BgL_rz00_1773 =
																															BgL_rz00_9330;
																														goto
																															BGl_loopze70ze7zz__unicodez00;
																													}
																												}
																										}
																									else
																										{	/* Llib/unicode.scm 903 */
																											if (
																												(BgL_nz00_1778 <= 253L))
																												{	/* Llib/unicode.scm 920 */
																													bool_t
																														BgL_test4157z00_9336;
																													if ((BgL_rz00_1773 <
																															(BgL_lenz00_7022 -
																																5L)))
																														{	/* Llib/unicode.scm 921 */
																															bool_t
																																BgL_test4159z00_9340;
																															{	/* Llib/unicode.scm 777 */
																																long
																																	BgL_nz00_4962;
																																BgL_nz00_4962 =
																																	(STRING_REF
																																	(BgL_strz00_7020,
																																		(BgL_rz00_1773
																																			+ 1L)));
																																if (
																																	(BgL_nz00_4962
																																		>= 128L))
																																	{	/* Llib/unicode.scm 778 */
																																		BgL_test4159z00_9340
																																			=
																																			(BgL_nz00_4962
																																			<= 191L);
																																	}
																																else
																																	{	/* Llib/unicode.scm 778 */
																																		BgL_test4159z00_9340
																																			=
																																			((bool_t)
																																			0);
																																	}
																															}
																															if (BgL_test4159z00_9340)
																																{	/* Llib/unicode.scm 922 */
																																	bool_t
																																		BgL_test4161z00_9347;
																																	{	/* Llib/unicode.scm 777 */
																																		long
																																			BgL_nz00_4970;
																																		BgL_nz00_4970
																																			=
																																			(STRING_REF
																																			(BgL_strz00_7020,
																																				(BgL_rz00_1773
																																					+
																																					2L)));
																																		if (
																																			(BgL_nz00_4970
																																				>=
																																				128L))
																																			{	/* Llib/unicode.scm 778 */
																																				BgL_test4161z00_9347
																																					=
																																					(BgL_nz00_4970
																																					<=
																																					191L);
																																			}
																																		else
																																			{	/* Llib/unicode.scm 778 */
																																				BgL_test4161z00_9347
																																					=
																																					(
																																					(bool_t)
																																					0);
																																			}
																																	}
																																	if (BgL_test4161z00_9347)
																																		{	/* Llib/unicode.scm 923 */
																																			bool_t
																																				BgL_test4163z00_9354;
																																			{	/* Llib/unicode.scm 777 */
																																				long
																																					BgL_nz00_4978;
																																				BgL_nz00_4978
																																					=
																																					(STRING_REF
																																					(BgL_strz00_7020,
																																						(BgL_rz00_1773
																																							+
																																							3L)));
																																				if (
																																					(BgL_nz00_4978
																																						>=
																																						128L))
																																					{	/* Llib/unicode.scm 778 */
																																						BgL_test4163z00_9354
																																							=
																																							(BgL_nz00_4978
																																							<=
																																							191L);
																																					}
																																				else
																																					{	/* Llib/unicode.scm 778 */
																																						BgL_test4163z00_9354
																																							=
																																							(
																																							(bool_t)
																																							0);
																																					}
																																			}
																																			if (BgL_test4163z00_9354)
																																				{	/* Llib/unicode.scm 924 */
																																					bool_t
																																						BgL_test4165z00_9361;
																																					{	/* Llib/unicode.scm 777 */
																																						long
																																							BgL_nz00_4986;
																																						BgL_nz00_4986
																																							=
																																							(STRING_REF
																																							(BgL_strz00_7020,
																																								(BgL_rz00_1773
																																									+
																																									4L)));
																																						if (
																																							(BgL_nz00_4986
																																								>=
																																								128L))
																																							{	/* Llib/unicode.scm 778 */
																																								BgL_test4165z00_9361
																																									=
																																									(BgL_nz00_4986
																																									<=
																																									191L);
																																							}
																																						else
																																							{	/* Llib/unicode.scm 778 */
																																								BgL_test4165z00_9361
																																									=
																																									(
																																									(bool_t)
																																									0);
																																							}
																																					}
																																					if (BgL_test4165z00_9361)
																																						{	/* Llib/unicode.scm 777 */
																																							long
																																								BgL_nz00_4994;
																																							BgL_nz00_4994
																																								=
																																								(STRING_REF
																																								(BgL_strz00_7020,
																																									(BgL_rz00_1773
																																										+
																																										5L)));
																																							if ((BgL_nz00_4994 >= 128L))
																																								{	/* Llib/unicode.scm 778 */
																																									BgL_test4157z00_9336
																																										=
																																										(BgL_nz00_4994
																																										<=
																																										191L);
																																								}
																																							else
																																								{	/* Llib/unicode.scm 778 */
																																									BgL_test4157z00_9336
																																										=
																																										(
																																										(bool_t)
																																										0);
																																								}
																																						}
																																					else
																																						{	/* Llib/unicode.scm 924 */
																																							BgL_test4157z00_9336
																																								=
																																								(
																																								(bool_t)
																																								0);
																																						}
																																				}
																																			else
																																				{	/* Llib/unicode.scm 923 */
																																					BgL_test4157z00_9336
																																						=
																																						(
																																						(bool_t)
																																						0);
																																				}
																																		}
																																	else
																																		{	/* Llib/unicode.scm 922 */
																																			BgL_test4157z00_9336
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																}
																															else
																																{	/* Llib/unicode.scm 921 */
																																	BgL_test4157z00_9336
																																		=
																																		((bool_t)
																																		0);
																																}
																														}
																													else
																														{	/* Llib/unicode.scm 920 */
																															BgL_test4157z00_9336
																																= ((bool_t) 0);
																														}
																													if (BgL_test4157z00_9336)
																														{	/* Llib/unicode.scm 920 */
																															STRING_SET
																																(BgL_resz00_7021,
																																BgL_wz00_1774,
																																BgL_cz00_1777);
																															{	/* Llib/unicode.scm 928 */
																																unsigned char
																																	BgL_auxz00_9377;
																																long
																																	BgL_tmpz00_9375;
																																BgL_auxz00_9377
																																	=
																																	STRING_REF
																																	(BgL_strz00_7020,
																																	(BgL_rz00_1773
																																		+ 1L));
																																BgL_tmpz00_9375
																																	=
																																	(BgL_wz00_1774
																																	+ 1L);
																																STRING_SET
																																	(BgL_resz00_7021,
																																	BgL_tmpz00_9375,
																																	BgL_auxz00_9377);
																															}
																															{	/* Llib/unicode.scm 929 */
																																unsigned char
																																	BgL_auxz00_9383;
																																long
																																	BgL_tmpz00_9381;
																																BgL_auxz00_9383
																																	=
																																	STRING_REF
																																	(BgL_strz00_7020,
																																	(BgL_rz00_1773
																																		+ 2L));
																																BgL_tmpz00_9381
																																	=
																																	(BgL_wz00_1774
																																	+ 2L);
																																STRING_SET
																																	(BgL_resz00_7021,
																																	BgL_tmpz00_9381,
																																	BgL_auxz00_9383);
																															}
																															{	/* Llib/unicode.scm 930 */
																																unsigned char
																																	BgL_auxz00_9389;
																																long
																																	BgL_tmpz00_9387;
																																BgL_auxz00_9389
																																	=
																																	STRING_REF
																																	(BgL_strz00_7020,
																																	(BgL_rz00_1773
																																		+ 3L));
																																BgL_tmpz00_9387
																																	=
																																	(BgL_wz00_1774
																																	+ 3L);
																																STRING_SET
																																	(BgL_resz00_7021,
																																	BgL_tmpz00_9387,
																																	BgL_auxz00_9389);
																															}
																															{	/* Llib/unicode.scm 931 */
																																unsigned char
																																	BgL_auxz00_9395;
																																long
																																	BgL_tmpz00_9393;
																																BgL_auxz00_9395
																																	=
																																	STRING_REF
																																	(BgL_strz00_7020,
																																	(BgL_rz00_1773
																																		+ 4L));
																																BgL_tmpz00_9393
																																	=
																																	(BgL_wz00_1774
																																	+ 4L);
																																STRING_SET
																																	(BgL_resz00_7021,
																																	BgL_tmpz00_9393,
																																	BgL_auxz00_9395);
																															}
																															{	/* Llib/unicode.scm 932 */
																																unsigned char
																																	BgL_auxz00_9401;
																																long
																																	BgL_tmpz00_9399;
																																BgL_auxz00_9401
																																	=
																																	STRING_REF
																																	(BgL_strz00_7020,
																																	(BgL_rz00_1773
																																		+ 5L));
																																BgL_tmpz00_9399
																																	=
																																	(BgL_wz00_1774
																																	+ 5L);
																																STRING_SET
																																	(BgL_resz00_7021,
																																	BgL_tmpz00_9399,
																																	BgL_auxz00_9401);
																															}
																															{
																																long
																																	BgL_wz00_9407;
																																long
																																	BgL_rz00_9405;
																																BgL_rz00_9405 =
																																	(BgL_rz00_1773
																																	+ 6L);
																																BgL_wz00_9407 =
																																	(BgL_wz00_1774
																																	+ 6L);
																																BgL_wz00_1774 =
																																	BgL_wz00_9407;
																																BgL_rz00_1773 =
																																	BgL_rz00_9405;
																																goto
																																	BGl_loopze70ze7zz__unicodez00;
																															}
																														}
																													else
																														{	/* Llib/unicode.scm 920 */
																															STRING_SET
																																(BgL_resz00_7021,
																																BgL_wz00_1774,
																																((unsigned char)
																																	239));
																															{	/* Llib/unicode.scm 783 */
																																long
																																	BgL_tmpz00_9410;
																																BgL_tmpz00_9410
																																	=
																																	(BgL_wz00_1774
																																	+ 1L);
																																STRING_SET
																																	(BgL_resz00_7021,
																																	BgL_tmpz00_9410,
																																	((unsigned
																																			char)
																																		191));
																															}
																															{	/* Llib/unicode.scm 784 */
																																long
																																	BgL_tmpz00_9413;
																																BgL_tmpz00_9413
																																	=
																																	(BgL_wz00_1774
																																	+ 2L);
																																STRING_SET
																																	(BgL_resz00_7021,
																																	BgL_tmpz00_9413,
																																	((unsigned
																																			char)
																																		189));
																															}
																															{
																																long
																																	BgL_wz00_9418;
																																long
																																	BgL_rz00_9416;
																																BgL_rz00_9416 =
																																	(BgL_rz00_1773
																																	+ 1L);
																																BgL_wz00_9418 =
																																	(BgL_wz00_1774
																																	+ 3L);
																																BgL_wz00_1774 =
																																	BgL_wz00_9418;
																																BgL_rz00_1773 =
																																	BgL_rz00_9416;
																																goto
																																	BGl_loopze70ze7zz__unicodez00;
																															}
																														}
																												}
																											else
																												{	/* Llib/unicode.scm 919 */
																													return BFALSE;
																												}
																										}
																								}
																						}
																				}
																		}
																}
														}
												}
										}
								}
						}
					}
				}
		}

	}



/* _utf8-normalize-utf16 */
	obj_t BGl__utf8zd2normaliza7ezd2utf16za7zz__unicodez00(obj_t
		BgL_env1138z00_83, obj_t BgL_opt1137z00_82)
	{
		{	/* Llib/unicode.scm 947 */
			{	/* Llib/unicode.scm 947 */
				obj_t BgL_strz00_2151;

				BgL_strz00_2151 = VECTOR_REF(BgL_opt1137z00_82, 0L);
				switch (VECTOR_LENGTH(BgL_opt1137z00_82))
					{
					case 1L:

						{	/* Llib/unicode.scm 947 */
							long BgL_endz00_2156;

							{	/* Llib/unicode.scm 947 */
								obj_t BgL_stringz00_5054;

								if (STRINGP(BgL_strz00_2151))
									{	/* Llib/unicode.scm 947 */
										BgL_stringz00_5054 = BgL_strz00_2151;
									}
								else
									{
										obj_t BgL_auxz00_9423;

										BgL_auxz00_9423 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string3618z00zz__unicodez00, BINT(38527L),
											BGl_string3669z00zz__unicodez00,
											BGl_string3661z00zz__unicodez00, BgL_strz00_2151);
										FAILURE(BgL_auxz00_9423, BFALSE, BFALSE);
									}
								BgL_endz00_2156 = STRING_LENGTH(BgL_stringz00_5054);
							}
							{	/* Llib/unicode.scm 947 */

								{	/* Llib/unicode.scm 947 */
									obj_t BgL_auxz00_9428;

									if (STRINGP(BgL_strz00_2151))
										{	/* Llib/unicode.scm 947 */
											BgL_auxz00_9428 = BgL_strz00_2151;
										}
									else
										{
											obj_t BgL_auxz00_9431;

											BgL_auxz00_9431 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3618z00zz__unicodez00, BINT(38433L),
												BGl_string3669z00zz__unicodez00,
												BGl_string3661z00zz__unicodez00, BgL_strz00_2151);
											FAILURE(BgL_auxz00_9431, BFALSE, BFALSE);
										}
									return
										BGl_utf8zd2normaliza7ezd2utf16za7zz__unicodez00
										(BgL_auxz00_9428, ((bool_t) 0), 0L, BgL_endz00_2156);
								}
							}
						}
						break;
					case 2L:

						{	/* Llib/unicode.scm 947 */
							obj_t BgL_strictz00_2157;

							BgL_strictz00_2157 = VECTOR_REF(BgL_opt1137z00_82, 1L);
							{	/* Llib/unicode.scm 947 */
								long BgL_endz00_2159;

								{	/* Llib/unicode.scm 947 */
									obj_t BgL_stringz00_5055;

									if (STRINGP(BgL_strz00_2151))
										{	/* Llib/unicode.scm 947 */
											BgL_stringz00_5055 = BgL_strz00_2151;
										}
									else
										{
											obj_t BgL_auxz00_9439;

											BgL_auxz00_9439 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3618z00zz__unicodez00, BINT(38527L),
												BGl_string3669z00zz__unicodez00,
												BGl_string3661z00zz__unicodez00, BgL_strz00_2151);
											FAILURE(BgL_auxz00_9439, BFALSE, BFALSE);
										}
									BgL_endz00_2159 = STRING_LENGTH(BgL_stringz00_5055);
								}
								{	/* Llib/unicode.scm 947 */

									{	/* Llib/unicode.scm 947 */
										obj_t BgL_auxz00_9444;

										if (STRINGP(BgL_strz00_2151))
											{	/* Llib/unicode.scm 947 */
												BgL_auxz00_9444 = BgL_strz00_2151;
											}
										else
											{
												obj_t BgL_auxz00_9447;

												BgL_auxz00_9447 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3618z00zz__unicodez00, BINT(38433L),
													BGl_string3669z00zz__unicodez00,
													BGl_string3661z00zz__unicodez00, BgL_strz00_2151);
												FAILURE(BgL_auxz00_9447, BFALSE, BFALSE);
											}
										return
											BGl_utf8zd2normaliza7ezd2utf16za7zz__unicodez00
											(BgL_auxz00_9444, CBOOL(BgL_strictz00_2157), 0L,
											BgL_endz00_2159);
									}
								}
							}
						}
						break;
					case 3L:

						{	/* Llib/unicode.scm 947 */
							obj_t BgL_strictz00_2160;

							BgL_strictz00_2160 = VECTOR_REF(BgL_opt1137z00_82, 1L);
							{	/* Llib/unicode.scm 947 */
								obj_t BgL_startz00_2161;

								BgL_startz00_2161 = VECTOR_REF(BgL_opt1137z00_82, 2L);
								{	/* Llib/unicode.scm 947 */
									long BgL_endz00_2162;

									{	/* Llib/unicode.scm 947 */
										obj_t BgL_stringz00_5056;

										if (STRINGP(BgL_strz00_2151))
											{	/* Llib/unicode.scm 947 */
												BgL_stringz00_5056 = BgL_strz00_2151;
											}
										else
											{
												obj_t BgL_auxz00_9457;

												BgL_auxz00_9457 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3618z00zz__unicodez00, BINT(38527L),
													BGl_string3669z00zz__unicodez00,
													BGl_string3661z00zz__unicodez00, BgL_strz00_2151);
												FAILURE(BgL_auxz00_9457, BFALSE, BFALSE);
											}
										BgL_endz00_2162 = STRING_LENGTH(BgL_stringz00_5056);
									}
									{	/* Llib/unicode.scm 947 */

										{	/* Llib/unicode.scm 947 */
											long BgL_auxz00_9469;
											obj_t BgL_auxz00_9462;

											{	/* Llib/unicode.scm 947 */
												obj_t BgL_tmpz00_9471;

												if (INTEGERP(BgL_startz00_2161))
													{	/* Llib/unicode.scm 947 */
														BgL_tmpz00_9471 = BgL_startz00_2161;
													}
												else
													{
														obj_t BgL_auxz00_9474;

														BgL_auxz00_9474 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string3618z00zz__unicodez00, BINT(38433L),
															BGl_string3669z00zz__unicodez00,
															BGl_string3620z00zz__unicodez00,
															BgL_startz00_2161);
														FAILURE(BgL_auxz00_9474, BFALSE, BFALSE);
													}
												BgL_auxz00_9469 = (long) CINT(BgL_tmpz00_9471);
											}
											if (STRINGP(BgL_strz00_2151))
												{	/* Llib/unicode.scm 947 */
													BgL_auxz00_9462 = BgL_strz00_2151;
												}
											else
												{
													obj_t BgL_auxz00_9465;

													BgL_auxz00_9465 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string3618z00zz__unicodez00, BINT(38433L),
														BGl_string3669z00zz__unicodez00,
														BGl_string3661z00zz__unicodez00, BgL_strz00_2151);
													FAILURE(BgL_auxz00_9465, BFALSE, BFALSE);
												}
											return
												BGl_utf8zd2normaliza7ezd2utf16za7zz__unicodez00
												(BgL_auxz00_9462, CBOOL(BgL_strictz00_2160),
												BgL_auxz00_9469, BgL_endz00_2162);
										}
									}
								}
							}
						}
						break;
					case 4L:

						{	/* Llib/unicode.scm 947 */
							obj_t BgL_strictz00_2163;

							BgL_strictz00_2163 = VECTOR_REF(BgL_opt1137z00_82, 1L);
							{	/* Llib/unicode.scm 947 */
								obj_t BgL_startz00_2164;

								BgL_startz00_2164 = VECTOR_REF(BgL_opt1137z00_82, 2L);
								{	/* Llib/unicode.scm 947 */
									obj_t BgL_endz00_2165;

									BgL_endz00_2165 = VECTOR_REF(BgL_opt1137z00_82, 3L);
									{	/* Llib/unicode.scm 947 */

										{	/* Llib/unicode.scm 947 */
											long BgL_auxz00_9500;
											long BgL_auxz00_9490;
											obj_t BgL_auxz00_9483;

											{	/* Llib/unicode.scm 947 */
												obj_t BgL_tmpz00_9501;

												if (INTEGERP(BgL_endz00_2165))
													{	/* Llib/unicode.scm 947 */
														BgL_tmpz00_9501 = BgL_endz00_2165;
													}
												else
													{
														obj_t BgL_auxz00_9504;

														BgL_auxz00_9504 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string3618z00zz__unicodez00, BINT(38433L),
															BGl_string3669z00zz__unicodez00,
															BGl_string3620z00zz__unicodez00, BgL_endz00_2165);
														FAILURE(BgL_auxz00_9504, BFALSE, BFALSE);
													}
												BgL_auxz00_9500 = (long) CINT(BgL_tmpz00_9501);
											}
											{	/* Llib/unicode.scm 947 */
												obj_t BgL_tmpz00_9492;

												if (INTEGERP(BgL_startz00_2164))
													{	/* Llib/unicode.scm 947 */
														BgL_tmpz00_9492 = BgL_startz00_2164;
													}
												else
													{
														obj_t BgL_auxz00_9495;

														BgL_auxz00_9495 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string3618z00zz__unicodez00, BINT(38433L),
															BGl_string3669z00zz__unicodez00,
															BGl_string3620z00zz__unicodez00,
															BgL_startz00_2164);
														FAILURE(BgL_auxz00_9495, BFALSE, BFALSE);
													}
												BgL_auxz00_9490 = (long) CINT(BgL_tmpz00_9492);
											}
											if (STRINGP(BgL_strz00_2151))
												{	/* Llib/unicode.scm 947 */
													BgL_auxz00_9483 = BgL_strz00_2151;
												}
											else
												{
													obj_t BgL_auxz00_9486;

													BgL_auxz00_9486 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string3618z00zz__unicodez00, BINT(38433L),
														BGl_string3669z00zz__unicodez00,
														BGl_string3661z00zz__unicodez00, BgL_strz00_2151);
													FAILURE(BgL_auxz00_9486, BFALSE, BFALSE);
												}
											return
												BGl_utf8zd2normaliza7ezd2utf16za7zz__unicodez00
												(BgL_auxz00_9483, CBOOL(BgL_strictz00_2163),
												BgL_auxz00_9490, BgL_auxz00_9500);
										}
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* utf8-normalize-utf16 */
	BGL_EXPORTED_DEF obj_t BGl_utf8zd2normaliza7ezd2utf16za7zz__unicodez00(obj_t
		BgL_strz00_78, bool_t BgL_strictz00_79, long BgL_startz00_80,
		long BgL_endz00_81)
	{
		{	/* Llib/unicode.scm 947 */
			{	/* Llib/unicode.scm 960 */
				bool_t BgL_test4178z00_9512;

				if ((BgL_endz00_81 > STRING_LENGTH(BgL_strz00_78)))
					{	/* Llib/unicode.scm 960 */
						BgL_test4178z00_9512 = ((bool_t) 1);
					}
				else
					{	/* Llib/unicode.scm 960 */
						if ((BgL_startz00_80 < 0L))
							{	/* Llib/unicode.scm 961 */
								BgL_test4178z00_9512 = ((bool_t) 1);
							}
						else
							{	/* Llib/unicode.scm 961 */
								BgL_test4178z00_9512 = (BgL_startz00_80 > BgL_endz00_81);
							}
					}
				if (BgL_test4178z00_9512)
					{	/* Llib/unicode.scm 963 */
						obj_t BgL_arg2069z00_2173;

						BgL_arg2069z00_2173 =
							MAKE_YOUNG_PAIR(BINT(BgL_startz00_80), BINT(BgL_endz00_81));
						return
							BGl_errorz00zz__errorz00(BGl_string3670z00zz__unicodez00,
							BGl_string3668z00zz__unicodez00, BgL_arg2069z00_2173);
					}
				else
					{	/* Llib/unicode.scm 965 */
						long BgL_lenz00_2174;

						BgL_lenz00_2174 = (BgL_endz00_81 - BgL_startz00_80);
						{	/* Llib/unicode.scm 965 */
							obj_t BgL_resz00_2175;

							{	/* Llib/unicode.scm 966 */
								long BgL_arg2526z00_2642;

								BgL_arg2526z00_2642 = (3L * BgL_lenz00_2174);
								{	/* Ieee/string.scm 172 */

									BgL_resz00_2175 =
										make_string(BgL_arg2526z00_2642, ((unsigned char) ' '));
							}}
							{	/* Llib/unicode.scm 966 */

								{
									long BgL_rz00_2177;
									long BgL_wz00_2178;
									bool_t BgL_asciiz00_2179;

									BgL_rz00_2177 = BgL_startz00_80;
									BgL_wz00_2178 = 0L;
									BgL_asciiz00_2179 = ((bool_t) 1);
								BgL_zc3z04anonymousza32070ze3z87_2180:
									if ((BgL_rz00_2177 == BgL_endz00_81))
										{	/* Llib/unicode.scm 971 */
											obj_t BgL_val0_1102z00_2182;
											obj_t BgL_val1_1103z00_2183;

											BgL_val0_1102z00_2182 =
												bgl_string_shrink(BgL_resz00_2175, BgL_wz00_2178);
											if (BgL_asciiz00_2179)
												{	/* Llib/unicode.scm 971 */
													BgL_val1_1103z00_2183 =
														BGl_symbol3662z00zz__unicodez00;
												}
											else
												{	/* Llib/unicode.scm 971 */
													BgL_val1_1103z00_2183 =
														BGl_symbol3671z00zz__unicodez00;
												}
											{	/* Llib/unicode.scm 971 */
												int BgL_tmpz00_9530;

												BgL_tmpz00_9530 = (int) (2L);
												BGL_MVALUES_NUMBER_SET(BgL_tmpz00_9530);
											}
											{	/* Llib/unicode.scm 971 */
												int BgL_tmpz00_9533;

												BgL_tmpz00_9533 = (int) (1L);
												BGL_MVALUES_VAL_SET(BgL_tmpz00_9533,
													BgL_val1_1103z00_2183);
											}
											return BgL_val0_1102z00_2182;
										}
									else
										{	/* Llib/unicode.scm 972 */
											unsigned char BgL_cz00_2184;

											BgL_cz00_2184 = STRING_REF(BgL_strz00_78, BgL_rz00_2177);
											{	/* Llib/unicode.scm 972 */
												long BgL_nz00_2185;

												BgL_nz00_2185 = (BgL_cz00_2184);
												{	/* Llib/unicode.scm 973 */

													if ((BgL_nz00_2185 <= 127L))
														{	/* Llib/unicode.scm 975 */
															STRING_SET(BgL_resz00_2175, BgL_wz00_2178,
																BgL_cz00_2184);
															{
																long BgL_wz00_9543;
																long BgL_rz00_9541;

																BgL_rz00_9541 = (BgL_rz00_2177 + 1L);
																BgL_wz00_9543 = (BgL_wz00_2178 + 1L);
																BgL_wz00_2178 = BgL_wz00_9543;
																BgL_rz00_2177 = BgL_rz00_9541;
																goto BgL_zc3z04anonymousza32070ze3z87_2180;
															}
														}
													else
														{	/* Llib/unicode.scm 975 */
															if ((BgL_nz00_2185 < 194L))
																{	/* Llib/unicode.scm 979 */
																	STRING_SET(BgL_resz00_2175, BgL_wz00_2178,
																		((unsigned char) 239));
																	{	/* Llib/unicode.scm 956 */
																		long BgL_tmpz00_9548;

																		BgL_tmpz00_9548 = (BgL_wz00_2178 + 1L);
																		STRING_SET(BgL_resz00_2175, BgL_tmpz00_9548,
																			((unsigned char) 191));
																	}
																	{	/* Llib/unicode.scm 957 */
																		long BgL_tmpz00_9551;

																		BgL_tmpz00_9551 = (BgL_wz00_2178 + 2L);
																		STRING_SET(BgL_resz00_2175, BgL_tmpz00_9551,
																			((unsigned char) 189));
																	}
																	{
																		bool_t BgL_asciiz00_9558;
																		long BgL_wz00_9556;
																		long BgL_rz00_9554;

																		BgL_rz00_9554 = (BgL_rz00_2177 + 1L);
																		BgL_wz00_9556 = (BgL_wz00_2178 + 3L);
																		BgL_asciiz00_9558 = ((bool_t) 0);
																		BgL_asciiz00_2179 = BgL_asciiz00_9558;
																		BgL_wz00_2178 = BgL_wz00_9556;
																		BgL_rz00_2177 = BgL_rz00_9554;
																		goto BgL_zc3z04anonymousza32070ze3z87_2180;
																	}
																}
															else
																{	/* Llib/unicode.scm 979 */
																	if ((BgL_nz00_2185 < 216L))
																		{	/* Llib/unicode.scm 985 */
																			bool_t BgL_test4186z00_9561;

																			if (
																				((1L + BgL_rz00_2177) < BgL_endz00_81))
																				{	/* Llib/unicode.scm 950 */
																					long BgL_nz00_5123;

																					BgL_nz00_5123 =
																						(STRING_REF(BgL_strz00_78,
																							(BgL_rz00_2177 + 1L)));
																					if ((BgL_nz00_5123 >= 128L))
																						{	/* Llib/unicode.scm 951 */
																							BgL_test4186z00_9561 =
																								(BgL_nz00_5123 <= 191L);
																						}
																					else
																						{	/* Llib/unicode.scm 951 */
																							BgL_test4186z00_9561 =
																								((bool_t) 0);
																						}
																				}
																			else
																				{	/* Llib/unicode.scm 985 */
																					BgL_test4186z00_9561 = ((bool_t) 0);
																				}
																			if (BgL_test4186z00_9561)
																				{	/* Llib/unicode.scm 985 */
																					STRING_SET(BgL_resz00_2175,
																						BgL_wz00_2178, BgL_cz00_2184);
																					{	/* Llib/unicode.scm 989 */
																						unsigned char BgL_auxz00_9574;
																						long BgL_tmpz00_9572;

																						BgL_auxz00_9574 =
																							STRING_REF(BgL_strz00_78,
																							(BgL_rz00_2177 + 1L));
																						BgL_tmpz00_9572 =
																							(BgL_wz00_2178 + 1L);
																						STRING_SET(BgL_resz00_2175,
																							BgL_tmpz00_9572, BgL_auxz00_9574);
																					}
																					{
																						bool_t BgL_asciiz00_9582;
																						long BgL_wz00_9580;
																						long BgL_rz00_9578;

																						BgL_rz00_9578 =
																							(BgL_rz00_2177 + 2L);
																						BgL_wz00_9580 =
																							(BgL_wz00_2178 + 2L);
																						BgL_asciiz00_9582 = ((bool_t) 0);
																						BgL_asciiz00_2179 =
																							BgL_asciiz00_9582;
																						BgL_wz00_2178 = BgL_wz00_9580;
																						BgL_rz00_2177 = BgL_rz00_9578;
																						goto
																							BgL_zc3z04anonymousza32070ze3z87_2180;
																					}
																				}
																			else
																				{	/* Llib/unicode.scm 985 */
																					STRING_SET(BgL_resz00_2175,
																						BgL_wz00_2178,
																						((unsigned char) 239));
																					{	/* Llib/unicode.scm 956 */
																						long BgL_tmpz00_9584;

																						BgL_tmpz00_9584 =
																							(BgL_wz00_2178 + 1L);
																						STRING_SET(BgL_resz00_2175,
																							BgL_tmpz00_9584,
																							((unsigned char) 191));
																					}
																					{	/* Llib/unicode.scm 957 */
																						long BgL_tmpz00_9587;

																						BgL_tmpz00_9587 =
																							(BgL_wz00_2178 + 2L);
																						STRING_SET(BgL_resz00_2175,
																							BgL_tmpz00_9587,
																							((unsigned char) 189));
																					}
																					{
																						bool_t BgL_asciiz00_9594;
																						long BgL_wz00_9592;
																						long BgL_rz00_9590;

																						BgL_rz00_9590 =
																							(BgL_rz00_2177 + 1L);
																						BgL_wz00_9592 =
																							(BgL_wz00_2178 + 3L);
																						BgL_asciiz00_9594 = ((bool_t) 0);
																						BgL_asciiz00_2179 =
																							BgL_asciiz00_9594;
																						BgL_wz00_2178 = BgL_wz00_9592;
																						BgL_rz00_2177 = BgL_rz00_9590;
																						goto
																							BgL_zc3z04anonymousza32070ze3z87_2180;
																					}
																				}
																		}
																	else
																		{	/* Llib/unicode.scm 983 */
																			if ((BgL_nz00_2185 <= 223L))
																				{	/* Llib/unicode.scm 995 */
																					bool_t BgL_test4190z00_9597;

																					if (
																						((1L + BgL_rz00_2177) <
																							BgL_endz00_81))
																						{	/* Llib/unicode.scm 950 */
																							long BgL_nz00_5162;

																							BgL_nz00_5162 =
																								(STRING_REF(BgL_strz00_78,
																									(BgL_rz00_2177 + 1L)));
																							if ((BgL_nz00_5162 >= 128L))
																								{	/* Llib/unicode.scm 951 */
																									BgL_test4190z00_9597 =
																										(BgL_nz00_5162 <= 191L);
																								}
																							else
																								{	/* Llib/unicode.scm 951 */
																									BgL_test4190z00_9597 =
																										((bool_t) 0);
																								}
																						}
																					else
																						{	/* Llib/unicode.scm 995 */
																							BgL_test4190z00_9597 =
																								((bool_t) 0);
																						}
																					if (BgL_test4190z00_9597)
																						{	/* Llib/unicode.scm 995 */
																							STRING_SET(BgL_resz00_2175,
																								BgL_wz00_2178, BgL_cz00_2184);
																							{	/* Llib/unicode.scm 999 */
																								unsigned char BgL_auxz00_9610;
																								long BgL_tmpz00_9608;

																								BgL_auxz00_9610 =
																									STRING_REF(BgL_strz00_78,
																									(BgL_rz00_2177 + 1L));
																								BgL_tmpz00_9608 =
																									(BgL_wz00_2178 + 1L);
																								STRING_SET(BgL_resz00_2175,
																									BgL_tmpz00_9608,
																									BgL_auxz00_9610);
																							}
																							{
																								bool_t BgL_asciiz00_9618;
																								long BgL_wz00_9616;
																								long BgL_rz00_9614;

																								BgL_rz00_9614 =
																									(BgL_rz00_2177 + 2L);
																								BgL_wz00_9616 =
																									(BgL_wz00_2178 + 2L);
																								BgL_asciiz00_9618 =
																									((bool_t) 0);
																								BgL_asciiz00_2179 =
																									BgL_asciiz00_9618;
																								BgL_wz00_2178 = BgL_wz00_9616;
																								BgL_rz00_2177 = BgL_rz00_9614;
																								goto
																									BgL_zc3z04anonymousza32070ze3z87_2180;
																							}
																						}
																					else
																						{	/* Llib/unicode.scm 995 */
																							STRING_SET(BgL_resz00_2175,
																								BgL_wz00_2178,
																								((unsigned char) 239));
																							{	/* Llib/unicode.scm 956 */
																								long BgL_tmpz00_9620;

																								BgL_tmpz00_9620 =
																									(BgL_wz00_2178 + 1L);
																								STRING_SET(BgL_resz00_2175,
																									BgL_tmpz00_9620,
																									((unsigned char) 191));
																							}
																							{	/* Llib/unicode.scm 957 */
																								long BgL_tmpz00_9623;

																								BgL_tmpz00_9623 =
																									(BgL_wz00_2178 + 2L);
																								STRING_SET(BgL_resz00_2175,
																									BgL_tmpz00_9623,
																									((unsigned char) 189));
																							}
																							{
																								bool_t BgL_asciiz00_9630;
																								long BgL_wz00_9628;
																								long BgL_rz00_9626;

																								BgL_rz00_9626 =
																									(BgL_rz00_2177 + 1L);
																								BgL_wz00_9628 =
																									(BgL_wz00_2178 + 3L);
																								BgL_asciiz00_9630 =
																									((bool_t) 0);
																								BgL_asciiz00_2179 =
																									BgL_asciiz00_9630;
																								BgL_wz00_2178 = BgL_wz00_9628;
																								BgL_rz00_2177 = BgL_rz00_9626;
																								goto
																									BgL_zc3z04anonymousza32070ze3z87_2180;
																							}
																						}
																				}
																			else
																				{	/* Llib/unicode.scm 994 */
																					if ((BgL_nz00_2185 == 237L))
																						{	/* Llib/unicode.scm 1007 */
																							bool_t BgL_test4194z00_9633;

																							if (
																								(BgL_rz00_2177 <
																									(BgL_endz00_81 - 2L)))
																								{	/* Llib/unicode.scm 1008 */
																									bool_t BgL_test4196z00_9637;

																									{	/* Llib/unicode.scm 950 */
																										long BgL_nz00_5201;

																										BgL_nz00_5201 =
																											(STRING_REF(BgL_strz00_78,
																												(BgL_rz00_2177 + 1L)));
																										if ((BgL_nz00_5201 >= 128L))
																											{	/* Llib/unicode.scm 951 */
																												BgL_test4196z00_9637 =
																													(BgL_nz00_5201 <=
																													191L);
																											}
																										else
																											{	/* Llib/unicode.scm 951 */
																												BgL_test4196z00_9637 =
																													((bool_t) 0);
																											}
																									}
																									if (BgL_test4196z00_9637)
																										{	/* Llib/unicode.scm 950 */
																											long BgL_nz00_5209;

																											BgL_nz00_5209 =
																												(STRING_REF
																												(BgL_strz00_78,
																													(BgL_rz00_2177 +
																														2L)));
																											if ((BgL_nz00_5209 >=
																													128L))
																												{	/* Llib/unicode.scm 951 */
																													BgL_test4194z00_9633 =
																														(BgL_nz00_5209 <=
																														191L);
																												}
																											else
																												{	/* Llib/unicode.scm 951 */
																													BgL_test4194z00_9633 =
																														((bool_t) 0);
																												}
																										}
																									else
																										{	/* Llib/unicode.scm 1008 */
																											BgL_test4194z00_9633 =
																												((bool_t) 0);
																										}
																								}
																							else
																								{	/* Llib/unicode.scm 1007 */
																									BgL_test4194z00_9633 =
																										((bool_t) 0);
																								}
																							if (BgL_test4194z00_9633)
																								{	/* Llib/unicode.scm 1015 */
																									long BgL_n1z00_2240;

																									BgL_n1z00_2240 =
																										(STRING_REF(BgL_strz00_78,
																											(BgL_rz00_2177 + 1L)));
																									{	/* Llib/unicode.scm 1015 */
																										long BgL_n2z00_2241;

																										BgL_n2z00_2241 =
																											(STRING_REF(BgL_strz00_78,
																												(BgL_rz00_2177 + 2L)));
																										{	/* Llib/unicode.scm 1016 */
																											long BgL_ucs2z00_2242;

																											BgL_ucs2z00_2242 =
																												(
																												((BgL_nz00_2185 & 15L)
																													<< (int) (12L)) +
																												(((BgL_n1z00_2240 & 63L)
																														<< (int) (6L)) +
																													(BgL_n2z00_2241 &
																														63L)));
																											{	/* Llib/unicode.scm 1017 */

																												if (
																													(BgL_ucs2z00_2242 >
																														57343L))
																													{	/* Llib/unicode.scm 1021 */
																														STRING_SET
																															(BgL_resz00_2175,
																															BgL_wz00_2178,
																															((unsigned char)
																																239));
																														{	/* Llib/unicode.scm 956 */
																															long
																																BgL_tmpz00_9668;
																															BgL_tmpz00_9668 =
																																(BgL_wz00_2178 +
																																1L);
																															STRING_SET
																																(BgL_resz00_2175,
																																BgL_tmpz00_9668,
																																((unsigned char)
																																	191));
																														}
																														{	/* Llib/unicode.scm 957 */
																															long
																																BgL_tmpz00_9671;
																															BgL_tmpz00_9671 =
																																(BgL_wz00_2178 +
																																2L);
																															STRING_SET
																																(BgL_resz00_2175,
																																BgL_tmpz00_9671,
																																((unsigned char)
																																	189));
																														}
																														{
																															bool_t
																																BgL_asciiz00_9678;
																															long
																																BgL_wz00_9676;
																															long
																																BgL_rz00_9674;
																															BgL_rz00_9674 =
																																(BgL_rz00_2177 +
																																1L);
																															BgL_wz00_9676 =
																																(BgL_wz00_2178 +
																																3L);
																															BgL_asciiz00_9678
																																= ((bool_t) 0);
																															BgL_asciiz00_2179
																																=
																																BgL_asciiz00_9678;
																															BgL_wz00_2178 =
																																BgL_wz00_9676;
																															BgL_rz00_2177 =
																																BgL_rz00_9674;
																															goto
																																BgL_zc3z04anonymousza32070ze3z87_2180;
																														}
																													}
																												else
																													{	/* Llib/unicode.scm 1024 */
																														bool_t
																															BgL_test4200z00_9679;
																														if ((BgL_rz00_2177
																																<=
																																(BgL_endz00_81 -
																																	4L)))
																															{	/* Llib/unicode.scm 1024 */
																																BgL_test4200z00_9679
																																	=
																																	((STRING_REF
																																		(BgL_strz00_78,
																																			(BgL_rz00_2177
																																				+
																																				3L))) ==
																																	237L);
																															}
																														else
																															{	/* Llib/unicode.scm 1024 */
																																BgL_test4200z00_9679
																																	=
																																	((bool_t) 0);
																															}
																														if (BgL_test4200z00_9679)
																															{	/* Llib/unicode.scm 1027 */
																																long
																																	BgL_m1z00_2254;
																																BgL_m1z00_2254 =
																																	(STRING_REF
																																	(BgL_strz00_78,
																																		(BgL_rz00_2177
																																			+ 4L)));
																																{	/* Llib/unicode.scm 1028 */
																																	long
																																		BgL_m2z00_2255;
																																	BgL_m2z00_2255
																																		=
																																		(STRING_REF
																																		(BgL_strz00_78,
																																			(BgL_rz00_2177
																																				+ 5L)));
																																	{	/* Llib/unicode.scm 1029 */
																																		long
																																			BgL_nuz00_2256;
																																		BgL_nuz00_2256
																																			=
																																			(((237L &
																																					15L)
																																				<<
																																				(int)
																																				(12L)) +
																																			(((BgL_m1z00_2254 & 63L) << (int) (6L)) + (BgL_m2z00_2255 & 63L)));
																																		{	/* Llib/unicode.scm 1030 */

																																			if (
																																				(BgL_nuz00_2256
																																					>=
																																					56320L))
																																				{	/* Llib/unicode.scm 1034 */
																																					long
																																						BgL_za7za7za7za7za7za7z00_2258;
																																					BgL_za7za7za7za7za7za7z00_2258
																																						=
																																						(BgL_nuz00_2256
																																						&
																																						63L);
																																					{	/* Llib/unicode.scm 1034 */
																																						long
																																							BgL_yyyyz00_2259;
																																						BgL_yyyyz00_2259
																																							=
																																							(
																																							(BgL_nuz00_2256
																																								&
																																								1023L)
																																							>>
																																							(int)
																																							(6L));
																																						{	/* Llib/unicode.scm 1035 */
																																							long
																																								BgL_xxz00_2260;
																																							BgL_xxz00_2260
																																								=
																																								(BgL_ucs2z00_2242
																																								&
																																								3L);
																																							{	/* Llib/unicode.scm 1036 */
																																								long
																																									BgL_wwwwz00_2261;
																																								BgL_wwwwz00_2261
																																									=
																																									(
																																									(BgL_ucs2z00_2242
																																										>>
																																										(int)
																																										(2L))
																																									&
																																									15L);
																																								{	/* Llib/unicode.scm 1037 */
																																									long
																																										BgL_vvvvz00_2262;
																																									BgL_vvvvz00_2262
																																										=
																																										(
																																										(BgL_ucs2z00_2242
																																											>>
																																											(int)
																																											(6L))
																																										&
																																										15L);
																																									{	/* Llib/unicode.scm 1038 */
																																										long
																																											BgL_uuuuuz00_2263;
																																										BgL_uuuuuz00_2263
																																											=
																																											(BgL_vvvvz00_2262
																																											+
																																											1L);
																																										{	/* Llib/unicode.scm 1039 */

																																											{	/* Llib/unicode.scm 1040 */
																																												unsigned
																																													char
																																													BgL_auxz00_9718;
																																												long
																																													BgL_tmpz00_9716;
																																												BgL_auxz00_9718
																																													=
																																													(
																																													(128L
																																														+
																																														BgL_za7za7za7za7za7za7z00_2258));
																																												BgL_tmpz00_9716
																																													=
																																													(BgL_wz00_2178
																																													+
																																													3L);
																																												STRING_SET
																																													(BgL_resz00_2175,
																																													BgL_tmpz00_9716,
																																													BgL_auxz00_9718);
																																											}
																																											{	/* Llib/unicode.scm 1043 */
																																												unsigned
																																													char
																																													BgL_auxz00_9724;
																																												long
																																													BgL_tmpz00_9722;
																																												BgL_auxz00_9724
																																													=
																																													(
																																													(128L
																																														+
																																														((BgL_xxz00_2260 << (int) (4L)) | BgL_yyyyz00_2259)));
																																												BgL_tmpz00_9722
																																													=
																																													(BgL_wz00_2178
																																													+
																																													2L);
																																												STRING_SET
																																													(BgL_resz00_2175,
																																													BgL_tmpz00_9722,
																																													BgL_auxz00_9724);
																																											}
																																											{	/* Llib/unicode.scm 1047 */
																																												unsigned
																																													char
																																													BgL_auxz00_9733;
																																												long
																																													BgL_tmpz00_9731;
																																												BgL_auxz00_9733
																																													=
																																													(
																																													(128L
																																														+
																																														(BgL_wwwwz00_2261
																																															|
																																															((BgL_uuuuuz00_2263 & 3L) << (int) (4L)))));
																																												BgL_tmpz00_9731
																																													=
																																													(BgL_wz00_2178
																																													+
																																													1L);
																																												STRING_SET
																																													(BgL_resz00_2175,
																																													BgL_tmpz00_9731,
																																													BgL_auxz00_9733);
																																											}
																																											{	/* Llib/unicode.scm 1052 */
																																												unsigned
																																													char
																																													BgL_tmpz00_9741;
																																												BgL_tmpz00_9741
																																													=
																																													(
																																													(240L
																																														|
																																														(BgL_uuuuuz00_2263
																																															>>
																																															(int)
																																															(2L))));
																																												STRING_SET
																																													(BgL_resz00_2175,
																																													BgL_wz00_2178,
																																													BgL_tmpz00_9741);
																																											}
																																											{
																																												bool_t
																																													BgL_asciiz00_9751;
																																												long
																																													BgL_wz00_9749;
																																												long
																																													BgL_rz00_9747;
																																												BgL_rz00_9747
																																													=
																																													(BgL_rz00_2177
																																													+
																																													6L);
																																												BgL_wz00_9749
																																													=
																																													(BgL_wz00_2178
																																													+
																																													4L);
																																												BgL_asciiz00_9751
																																													=
																																													(
																																													(bool_t)
																																													0);
																																												BgL_asciiz00_2179
																																													=
																																													BgL_asciiz00_9751;
																																												BgL_wz00_2178
																																													=
																																													BgL_wz00_9749;
																																												BgL_rz00_2177
																																													=
																																													BgL_rz00_9747;
																																												goto
																																													BgL_zc3z04anonymousza32070ze3z87_2180;
																																											}
																																										}
																																									}
																																								}
																																							}
																																						}
																																					}
																																				}
																																			else
																																				{	/* Llib/unicode.scm 1033 */
																																					STRING_SET
																																						(BgL_resz00_2175,
																																						BgL_wz00_2178,
																																						((unsigned char) 239));
																																					{	/* Llib/unicode.scm 956 */
																																						long
																																							BgL_tmpz00_9753;
																																						BgL_tmpz00_9753
																																							=
																																							(BgL_wz00_2178
																																							+
																																							1L);
																																						STRING_SET
																																							(BgL_resz00_2175,
																																							BgL_tmpz00_9753,
																																							((unsigned char) 191));
																																					}
																																					{	/* Llib/unicode.scm 957 */
																																						long
																																							BgL_tmpz00_9756;
																																						BgL_tmpz00_9756
																																							=
																																							(BgL_wz00_2178
																																							+
																																							2L);
																																						STRING_SET
																																							(BgL_resz00_2175,
																																							BgL_tmpz00_9756,
																																							((unsigned char) 189));
																																					}
																																					{
																																						bool_t
																																							BgL_asciiz00_9763;
																																						long
																																							BgL_wz00_9761;
																																						long
																																							BgL_rz00_9759;
																																						BgL_rz00_9759
																																							=
																																							(BgL_rz00_2177
																																							+
																																							1L);
																																						BgL_wz00_9761
																																							=
																																							(BgL_wz00_2178
																																							+
																																							3L);
																																						BgL_asciiz00_9763
																																							=
																																							(
																																							(bool_t)
																																							0);
																																						BgL_asciiz00_2179
																																							=
																																							BgL_asciiz00_9763;
																																						BgL_wz00_2178
																																							=
																																							BgL_wz00_9761;
																																						BgL_rz00_2177
																																							=
																																							BgL_rz00_9759;
																																						goto
																																							BgL_zc3z04anonymousza32070ze3z87_2180;
																																					}
																																				}
																																		}
																																	}
																																}
																															}
																														else
																															{	/* Llib/unicode.scm 1024 */
																																if (
																																	(BgL_ucs2z00_2242
																																		<= 56319L))
																																	{	/* Llib/unicode.scm 1060 */
																																		long
																																			BgL_xxz00_2299;
																																		BgL_xxz00_2299
																																			=
																																			(BgL_ucs2z00_2242
																																			& 3L);
																																		{	/* Llib/unicode.scm 1060 */
																																			long
																																				BgL_wwwwz00_2300;
																																			BgL_wwwwz00_2300
																																				=
																																				(
																																				(BgL_ucs2z00_2242
																																					>>
																																					(int)
																																					(2L))
																																				& 15L);
																																			{	/* Llib/unicode.scm 1061 */
																																				long
																																					BgL_vvvvz00_2301;
																																				BgL_vvvvz00_2301
																																					=
																																					(
																																					(BgL_ucs2z00_2242
																																						>>
																																						(int)
																																						(6L))
																																					&
																																					15L);
																																				{	/* Llib/unicode.scm 1062 */
																																					long
																																						BgL_uuuuuz00_2302;
																																					BgL_uuuuuz00_2302
																																						=
																																						(BgL_vvvvz00_2301
																																						+
																																						1L);
																																					{	/* Llib/unicode.scm 1063 */

																																						{	/* Llib/unicode.scm 1064 */
																																							unsigned
																																								char
																																								BgL_auxz00_9776;
																																							long
																																								BgL_tmpz00_9774;
																																							BgL_auxz00_9776
																																								=
																																								(
																																								(128L
																																									|
																																									(BgL_uuuuuz00_2302
																																										>>
																																										(int)
																																										(2L))));
																																							BgL_tmpz00_9774
																																								=
																																								(BgL_wz00_2178
																																								+
																																								3L);
																																							STRING_SET
																																								(BgL_resz00_2175,
																																								BgL_tmpz00_9774,
																																								BgL_auxz00_9776);
																																						}
																																						{	/* Llib/unicode.scm 1066 */
																																							unsigned
																																								char
																																								BgL_auxz00_9784;
																																							long
																																								BgL_tmpz00_9782;
																																							BgL_auxz00_9784
																																								=
																																								(
																																								((BgL_xxz00_2299 << (int) (4L)) + 128L));
																																							BgL_tmpz00_9782
																																								=
																																								(BgL_wz00_2178
																																								+
																																								2L);
																																							STRING_SET
																																								(BgL_resz00_2175,
																																								BgL_tmpz00_9782,
																																								BgL_auxz00_9784);
																																						}
																																						{	/* Llib/unicode.scm 1068 */
																																							unsigned
																																								char
																																								BgL_auxz00_9792;
																																							long
																																								BgL_tmpz00_9790;
																																							BgL_auxz00_9792
																																								=
																																								(
																																								(128L
																																									+
																																									(BgL_wwwwz00_2300
																																										|
																																										((BgL_uuuuuz00_2302 & 3L) << (int) (4L)))));
																																							BgL_tmpz00_9790
																																								=
																																								(BgL_wz00_2178
																																								+
																																								1L);
																																							STRING_SET
																																								(BgL_resz00_2175,
																																								BgL_tmpz00_9790,
																																								BgL_auxz00_9792);
																																						}
																																						STRING_SET
																																							(BgL_resz00_2175,
																																							BgL_wz00_2178,
																																							((unsigned char) 248));
																																						{
																																							bool_t
																																								BgL_asciiz00_9805;
																																							long
																																								BgL_wz00_9803;
																																							long
																																								BgL_rz00_9801;
																																							BgL_rz00_9801
																																								=
																																								(BgL_rz00_2177
																																								+
																																								3L);
																																							BgL_wz00_9803
																																								=
																																								(BgL_wz00_2178
																																								+
																																								4L);
																																							BgL_asciiz00_9805
																																								=
																																								(
																																								(bool_t)
																																								0);
																																							BgL_asciiz00_2179
																																								=
																																								BgL_asciiz00_9805;
																																							BgL_wz00_2178
																																								=
																																								BgL_wz00_9803;
																																							BgL_rz00_2177
																																								=
																																								BgL_rz00_9801;
																																							goto
																																								BgL_zc3z04anonymousza32070ze3z87_2180;
																																						}
																																					}
																																				}
																																			}
																																		}
																																	}
																																else
																																	{	/* Llib/unicode.scm 1076 */
																																		long
																																			BgL_za7za7za7za7za7za7z00_2321;
																																		long
																																			BgL_yyyyz00_2322;
																																		BgL_za7za7za7za7za7za7z00_2321
																																			=
																																			(BgL_nz00_2185
																																			& 63L);
																																		BgL_yyyyz00_2322
																																			=
																																			(
																																			(BgL_nz00_2185
																																				& 1023L)
																																			>>
																																			(int)
																																			(6L));
																																		{	/* Llib/unicode.scm 1078 */
																																			unsigned
																																				char
																																				BgL_auxz00_9812;
																																			long
																																				BgL_tmpz00_9810;
																																			BgL_auxz00_9812
																																				=
																																				(
																																				(BgL_za7za7za7za7za7za7z00_2321
																																					+
																																					128L));
																																			BgL_tmpz00_9810
																																				=
																																				(BgL_wz00_2178
																																				+ 3L);
																																			STRING_SET
																																				(BgL_resz00_2175,
																																				BgL_tmpz00_9810,
																																				BgL_auxz00_9812);
																																		}
																																		{	/* Llib/unicode.scm 1080 */
																																			unsigned
																																				char
																																				BgL_auxz00_9818;
																																			long
																																				BgL_tmpz00_9816;
																																			BgL_auxz00_9818
																																				=
																																				(
																																				(BgL_yyyyz00_2322
																																					+
																																					128L));
																																			BgL_tmpz00_9816
																																				=
																																				(BgL_wz00_2178
																																				+ 2L);
																																			STRING_SET
																																				(BgL_resz00_2175,
																																				BgL_tmpz00_9816,
																																				BgL_auxz00_9818);
																																		}
																																		{	/* Llib/unicode.scm 1082 */
																																			long
																																				BgL_tmpz00_9822;
																																			BgL_tmpz00_9822
																																				=
																																				(BgL_wz00_2178
																																				+ 1L);
																																			STRING_SET
																																				(BgL_resz00_2175,
																																				BgL_tmpz00_9822,
																																				((unsigned char) 128));
																																		}
																																		STRING_SET
																																			(BgL_resz00_2175,
																																			BgL_wz00_2178,
																																			((unsigned
																																					char)
																																				252));
																																		{	/* Llib/unicode.scm 1088 */
																																			bool_t
																																				BgL_test4204z00_9826;
																																			if (
																																				(BgL_wz00_2178
																																					>=
																																					4L))
																																				{	/* Llib/unicode.scm 1089 */
																																					bool_t
																																						BgL_test4206z00_9829;
																																					{	/* Llib/unicode.scm 1089 */
																																						long
																																							BgL_indexz00_5393;
																																						BgL_indexz00_5393
																																							=
																																							(BgL_wz00_2178
																																							-
																																							4L);
																																						if (
																																							((BgL_wz00_2178 + 4L) >= (BgL_indexz00_5393 + 4L)))
																																							{	/* Llib/unicode.scm 1285 */
																																								BgL_test4206z00_9829
																																									=
																																									(
																																									(STRING_REF
																																										(BgL_resz00_2175,
																																											BgL_indexz00_5393))
																																									==
																																									248L);
																																							}
																																						else
																																							{	/* Llib/unicode.scm 1285 */
																																								BgL_test4206z00_9829
																																									=
																																									(
																																									(bool_t)
																																									0);
																																							}
																																					}
																																					if (BgL_test4206z00_9829)
																																						{	/* Llib/unicode.scm 1089 */
																																							if (((BgL_wz00_2178 + 4L) >= (BgL_wz00_2178 + 4L)))
																																								{	/* Llib/unicode.scm 1295 */
																																									BgL_test4204z00_9826
																																										=
																																										(
																																										(STRING_REF
																																											(BgL_resz00_2175,
																																												BgL_wz00_2178))
																																										==
																																										252L);
																																								}
																																							else
																																								{	/* Llib/unicode.scm 1295 */
																																									BgL_test4204z00_9826
																																										=
																																										(
																																										(bool_t)
																																										0);
																																								}
																																						}
																																					else
																																						{	/* Llib/unicode.scm 1089 */
																																							BgL_test4204z00_9826
																																								=
																																								(
																																								(bool_t)
																																								0);
																																						}
																																				}
																																			else
																																				{	/* Llib/unicode.scm 1088 */
																																					BgL_test4204z00_9826
																																						=
																																						(
																																						(bool_t)
																																						0);
																																				}
																																			if (BgL_test4204z00_9826)
																																				{	/* Llib/unicode.scm 1088 */
																																					BGl_utf8zd2collapsez12zc0zz__unicodez00
																																						(BgL_resz00_2175,
																																						BgL_wz00_2178,
																																						BgL_resz00_2175,
																																						BINT
																																						(BgL_wz00_2178));
																																					{
																																						bool_t
																																							BgL_asciiz00_9849;
																																						long
																																							BgL_rz00_9847;
																																						BgL_rz00_9847
																																							=
																																							(BgL_rz00_2177
																																							+
																																							3L);
																																						BgL_asciiz00_9849
																																							=
																																							(
																																							(bool_t)
																																							0);
																																						BgL_asciiz00_2179
																																							=
																																							BgL_asciiz00_9849;
																																						BgL_rz00_2177
																																							=
																																							BgL_rz00_9847;
																																						goto
																																							BgL_zc3z04anonymousza32070ze3z87_2180;
																																					}
																																				}
																																			else
																																				{
																																					bool_t
																																						BgL_asciiz00_9854;
																																					long
																																						BgL_wz00_9852;
																																					long
																																						BgL_rz00_9850;
																																					BgL_rz00_9850
																																						=
																																						(BgL_rz00_2177
																																						+
																																						3L);
																																					BgL_wz00_9852
																																						=
																																						(BgL_wz00_2178
																																						+
																																						4L);
																																					BgL_asciiz00_9854
																																						=
																																						(
																																						(bool_t)
																																						0);
																																					BgL_asciiz00_2179
																																						=
																																						BgL_asciiz00_9854;
																																					BgL_wz00_2178
																																						=
																																						BgL_wz00_9852;
																																					BgL_rz00_2177
																																						=
																																						BgL_rz00_9850;
																																					goto
																																						BgL_zc3z04anonymousza32070ze3z87_2180;
																																				}
																																		}
																																	}
																															}
																													}
																											}
																										}
																									}
																								}
																							else
																								{	/* Llib/unicode.scm 1007 */
																									STRING_SET(BgL_resz00_2175,
																										BgL_wz00_2178,
																										((unsigned char) 239));
																									{	/* Llib/unicode.scm 956 */
																										long BgL_tmpz00_9856;

																										BgL_tmpz00_9856 =
																											(BgL_wz00_2178 + 1L);
																										STRING_SET(BgL_resz00_2175,
																											BgL_tmpz00_9856,
																											((unsigned char) 191));
																									}
																									{	/* Llib/unicode.scm 957 */
																										long BgL_tmpz00_9859;

																										BgL_tmpz00_9859 =
																											(BgL_wz00_2178 + 2L);
																										STRING_SET(BgL_resz00_2175,
																											BgL_tmpz00_9859,
																											((unsigned char) 189));
																									}
																									{
																										bool_t BgL_asciiz00_9866;
																										long BgL_wz00_9864;
																										long BgL_rz00_9862;

																										BgL_rz00_9862 =
																											(BgL_rz00_2177 + 1L);
																										BgL_wz00_9864 =
																											(BgL_wz00_2178 + 3L);
																										BgL_asciiz00_9866 =
																											((bool_t) 0);
																										BgL_asciiz00_2179 =
																											BgL_asciiz00_9866;
																										BgL_wz00_2178 =
																											BgL_wz00_9864;
																										BgL_rz00_2177 =
																											BgL_rz00_9862;
																										goto
																											BgL_zc3z04anonymousza32070ze3z87_2180;
																									}
																								}
																						}
																					else
																						{	/* Llib/unicode.scm 1005 */
																							if ((BgL_nz00_2185 <= 239L))
																								{	/* Llib/unicode.scm 1102 */
																									bool_t BgL_test4210z00_9869;

																									if (
																										(BgL_rz00_2177 <
																											(BgL_endz00_81 - 2L)))
																										{	/* Llib/unicode.scm 1103 */
																											bool_t
																												BgL_test4212z00_9873;
																											{	/* Llib/unicode.scm 950 */
																												long BgL_nz00_5445;

																												BgL_nz00_5445 =
																													(STRING_REF
																													(BgL_strz00_78,
																														(BgL_rz00_2177 +
																															1L)));
																												if ((BgL_nz00_5445 >=
																														128L))
																													{	/* Llib/unicode.scm 951 */
																														BgL_test4212z00_9873
																															=
																															(BgL_nz00_5445 <=
																															191L);
																													}
																												else
																													{	/* Llib/unicode.scm 951 */
																														BgL_test4212z00_9873
																															= ((bool_t) 0);
																													}
																											}
																											if (BgL_test4212z00_9873)
																												{	/* Llib/unicode.scm 950 */
																													long BgL_nz00_5453;

																													BgL_nz00_5453 =
																														(STRING_REF
																														(BgL_strz00_78,
																															(BgL_rz00_2177 +
																																2L)));
																													if ((BgL_nz00_5453 >=
																															128L))
																														{	/* Llib/unicode.scm 951 */
																															BgL_test4210z00_9869
																																=
																																(BgL_nz00_5453
																																<= 191L);
																														}
																													else
																														{	/* Llib/unicode.scm 951 */
																															BgL_test4210z00_9869
																																= ((bool_t) 0);
																														}
																												}
																											else
																												{	/* Llib/unicode.scm 1103 */
																													BgL_test4210z00_9869 =
																														((bool_t) 0);
																												}
																										}
																									else
																										{	/* Llib/unicode.scm 1102 */
																											BgL_test4210z00_9869 =
																												((bool_t) 0);
																										}
																									if (BgL_test4210z00_9869)
																										{	/* Llib/unicode.scm 1102 */
																											STRING_SET
																												(BgL_resz00_2175,
																												BgL_wz00_2178,
																												BgL_cz00_2184);
																											{	/* Llib/unicode.scm 1107 */
																												unsigned char
																													BgL_auxz00_9889;
																												long BgL_tmpz00_9887;

																												BgL_auxz00_9889 =
																													STRING_REF
																													(BgL_strz00_78,
																													(BgL_rz00_2177 + 1L));
																												BgL_tmpz00_9887 =
																													(BgL_wz00_2178 + 1L);
																												STRING_SET
																													(BgL_resz00_2175,
																													BgL_tmpz00_9887,
																													BgL_auxz00_9889);
																											}
																											{	/* Llib/unicode.scm 1108 */
																												unsigned char
																													BgL_auxz00_9895;
																												long BgL_tmpz00_9893;

																												BgL_auxz00_9895 =
																													STRING_REF
																													(BgL_strz00_78,
																													(BgL_rz00_2177 + 2L));
																												BgL_tmpz00_9893 =
																													(BgL_wz00_2178 + 2L);
																												STRING_SET
																													(BgL_resz00_2175,
																													BgL_tmpz00_9893,
																													BgL_auxz00_9895);
																											}
																											{
																												bool_t
																													BgL_asciiz00_9903;
																												long BgL_wz00_9901;
																												long BgL_rz00_9899;

																												BgL_rz00_9899 =
																													(BgL_rz00_2177 + 3L);
																												BgL_wz00_9901 =
																													(BgL_wz00_2178 + 3L);
																												BgL_asciiz00_9903 =
																													((bool_t) 0);
																												BgL_asciiz00_2179 =
																													BgL_asciiz00_9903;
																												BgL_wz00_2178 =
																													BgL_wz00_9901;
																												BgL_rz00_2177 =
																													BgL_rz00_9899;
																												goto
																													BgL_zc3z04anonymousza32070ze3z87_2180;
																											}
																										}
																									else
																										{	/* Llib/unicode.scm 1102 */
																											STRING_SET
																												(BgL_resz00_2175,
																												BgL_wz00_2178,
																												((unsigned char) 239));
																											{	/* Llib/unicode.scm 956 */
																												long BgL_tmpz00_9905;

																												BgL_tmpz00_9905 =
																													(BgL_wz00_2178 + 1L);
																												STRING_SET
																													(BgL_resz00_2175,
																													BgL_tmpz00_9905,
																													((unsigned char)
																														191));
																											}
																											{	/* Llib/unicode.scm 957 */
																												long BgL_tmpz00_9908;

																												BgL_tmpz00_9908 =
																													(BgL_wz00_2178 + 2L);
																												STRING_SET
																													(BgL_resz00_2175,
																													BgL_tmpz00_9908,
																													((unsigned char)
																														189));
																											}
																											{
																												bool_t
																													BgL_asciiz00_9915;
																												long BgL_wz00_9913;
																												long BgL_rz00_9911;

																												BgL_rz00_9911 =
																													(BgL_rz00_2177 + 1L);
																												BgL_wz00_9913 =
																													(BgL_wz00_2178 + 3L);
																												BgL_asciiz00_9915 =
																													((bool_t) 0);
																												BgL_asciiz00_2179 =
																													BgL_asciiz00_9915;
																												BgL_wz00_2178 =
																													BgL_wz00_9913;
																												BgL_rz00_2177 =
																													BgL_rz00_9911;
																												goto
																													BgL_zc3z04anonymousza32070ze3z87_2180;
																											}
																										}
																								}
																							else
																								{	/* Llib/unicode.scm 1100 */
																									if ((BgL_nz00_2185 == 240L))
																										{	/* Llib/unicode.scm 1115 */
																											bool_t
																												BgL_test4216z00_9918;
																											if ((BgL_rz00_2177 <
																													(BgL_endz00_81 - 3L)))
																												{	/* Llib/unicode.scm 1116 */
																													bool_t
																														BgL_test4218z00_9922;
																													{	/* Llib/unicode.scm 950 */
																														long BgL_nz00_5499;

																														BgL_nz00_5499 =
																															(STRING_REF
																															(BgL_strz00_78,
																																(BgL_rz00_2177 +
																																	1L)));
																														if ((BgL_nz00_5499
																																>= 144L))
																															{	/* Llib/unicode.scm 951 */
																																BgL_test4218z00_9922
																																	=
																																	(BgL_nz00_5499
																																	<= 191L);
																															}
																														else
																															{	/* Llib/unicode.scm 951 */
																																BgL_test4218z00_9922
																																	=
																																	((bool_t) 0);
																															}
																													}
																													if (BgL_test4218z00_9922)
																														{	/* Llib/unicode.scm 1117 */
																															bool_t
																																BgL_test4220z00_9929;
																															{	/* Llib/unicode.scm 950 */
																																long
																																	BgL_nz00_5507;
																																BgL_nz00_5507 =
																																	(STRING_REF
																																	(BgL_strz00_78,
																																		(BgL_rz00_2177
																																			+ 2L)));
																																if (
																																	(BgL_nz00_5507
																																		>= 128L))
																																	{	/* Llib/unicode.scm 951 */
																																		BgL_test4220z00_9929
																																			=
																																			(BgL_nz00_5507
																																			<= 191L);
																																	}
																																else
																																	{	/* Llib/unicode.scm 951 */
																																		BgL_test4220z00_9929
																																			=
																																			((bool_t)
																																			0);
																																	}
																															}
																															if (BgL_test4220z00_9929)
																																{	/* Llib/unicode.scm 950 */
																																	long
																																		BgL_nz00_5515;
																																	BgL_nz00_5515
																																		=
																																		(STRING_REF
																																		(BgL_strz00_78,
																																			(BgL_rz00_2177
																																				+ 3L)));
																																	if (
																																		(BgL_nz00_5515
																																			>= 128L))
																																		{	/* Llib/unicode.scm 951 */
																																			BgL_test4216z00_9918
																																				=
																																				(BgL_nz00_5515
																																				<=
																																				191L);
																																		}
																																	else
																																		{	/* Llib/unicode.scm 951 */
																																			BgL_test4216z00_9918
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																}
																															else
																																{	/* Llib/unicode.scm 1117 */
																																	BgL_test4216z00_9918
																																		=
																																		((bool_t)
																																		0);
																																}
																														}
																													else
																														{	/* Llib/unicode.scm 1116 */
																															BgL_test4216z00_9918
																																= ((bool_t) 0);
																														}
																												}
																											else
																												{	/* Llib/unicode.scm 1115 */
																													BgL_test4216z00_9918 =
																														((bool_t) 0);
																												}
																											if (BgL_test4216z00_9918)
																												{	/* Llib/unicode.scm 1115 */
																													STRING_SET
																														(BgL_resz00_2175,
																														BgL_wz00_2178,
																														BgL_cz00_2184);
																													{	/* Llib/unicode.scm 1121 */
																														unsigned char
																															BgL_auxz00_9945;
																														long
																															BgL_tmpz00_9943;
																														BgL_auxz00_9945 =
																															STRING_REF
																															(BgL_strz00_78,
																															(BgL_rz00_2177 +
																																1L));
																														BgL_tmpz00_9943 =
																															(BgL_wz00_2178 +
																															1L);
																														STRING_SET
																															(BgL_resz00_2175,
																															BgL_tmpz00_9943,
																															BgL_auxz00_9945);
																													}
																													{	/* Llib/unicode.scm 1122 */
																														unsigned char
																															BgL_auxz00_9951;
																														long
																															BgL_tmpz00_9949;
																														BgL_auxz00_9951 =
																															STRING_REF
																															(BgL_strz00_78,
																															(BgL_rz00_2177 +
																																2L));
																														BgL_tmpz00_9949 =
																															(BgL_wz00_2178 +
																															2L);
																														STRING_SET
																															(BgL_resz00_2175,
																															BgL_tmpz00_9949,
																															BgL_auxz00_9951);
																													}
																													{	/* Llib/unicode.scm 1123 */
																														unsigned char
																															BgL_auxz00_9957;
																														long
																															BgL_tmpz00_9955;
																														BgL_auxz00_9957 =
																															STRING_REF
																															(BgL_strz00_78,
																															(BgL_rz00_2177 +
																																3L));
																														BgL_tmpz00_9955 =
																															(BgL_wz00_2178 +
																															3L);
																														STRING_SET
																															(BgL_resz00_2175,
																															BgL_tmpz00_9955,
																															BgL_auxz00_9957);
																													}
																													{
																														bool_t
																															BgL_asciiz00_9965;
																														long BgL_wz00_9963;
																														long BgL_rz00_9961;

																														BgL_rz00_9961 =
																															(BgL_rz00_2177 +
																															4L);
																														BgL_wz00_9963 =
																															(BgL_wz00_2178 +
																															4L);
																														BgL_asciiz00_9965 =
																															((bool_t) 0);
																														BgL_asciiz00_2179 =
																															BgL_asciiz00_9965;
																														BgL_wz00_2178 =
																															BgL_wz00_9963;
																														BgL_rz00_2177 =
																															BgL_rz00_9961;
																														goto
																															BgL_zc3z04anonymousza32070ze3z87_2180;
																													}
																												}
																											else
																												{	/* Llib/unicode.scm 1115 */
																													STRING_SET
																														(BgL_resz00_2175,
																														BgL_wz00_2178,
																														((unsigned char)
																															239));
																													{	/* Llib/unicode.scm 956 */
																														long
																															BgL_tmpz00_9967;
																														BgL_tmpz00_9967 =
																															(BgL_wz00_2178 +
																															1L);
																														STRING_SET
																															(BgL_resz00_2175,
																															BgL_tmpz00_9967,
																															((unsigned char)
																																191));
																													}
																													{	/* Llib/unicode.scm 957 */
																														long
																															BgL_tmpz00_9970;
																														BgL_tmpz00_9970 =
																															(BgL_wz00_2178 +
																															2L);
																														STRING_SET
																															(BgL_resz00_2175,
																															BgL_tmpz00_9970,
																															((unsigned char)
																																189));
																													}
																													{
																														bool_t
																															BgL_asciiz00_9977;
																														long BgL_wz00_9975;
																														long BgL_rz00_9973;

																														BgL_rz00_9973 =
																															(BgL_rz00_2177 +
																															1L);
																														BgL_wz00_9975 =
																															(BgL_wz00_2178 +
																															3L);
																														BgL_asciiz00_9977 =
																															((bool_t) 0);
																														BgL_asciiz00_2179 =
																															BgL_asciiz00_9977;
																														BgL_wz00_2178 =
																															BgL_wz00_9975;
																														BgL_rz00_2177 =
																															BgL_rz00_9973;
																														goto
																															BgL_zc3z04anonymousza32070ze3z87_2180;
																													}
																												}
																										}
																									else
																										{	/* Llib/unicode.scm 1128 */
																											bool_t
																												BgL_test4223z00_9978;
																											if ((BgL_nz00_2185 ==
																													244L))
																												{	/* Llib/unicode.scm 1128 */
																													BgL_test4223z00_9978 =
																														((bool_t) 1);
																												}
																											else
																												{	/* Llib/unicode.scm 1129 */
																													bool_t
																														BgL_test4225z00_9981;
																													{	/* Llib/unicode.scm 1129 */
																														bool_t
																															BgL__ortest_1055z00_2640;
																														BgL__ortest_1055z00_2640
																															=
																															(BgL_nz00_2185 ==
																															248L);
																														if (BgL__ortest_1055z00_2640)
																															{	/* Llib/unicode.scm 1129 */
																																BgL_test4225z00_9981
																																	=
																																	BgL__ortest_1055z00_2640;
																															}
																														else
																															{	/* Llib/unicode.scm 1129 */
																																BgL_test4225z00_9981
																																	=
																																	(BgL_nz00_2185
																																	== 252L);
																															}
																													}
																													if (BgL_test4225z00_9981)
																														{	/* Llib/unicode.scm 1129 */
																															if (BgL_strictz00_79)
																																{	/* Llib/unicode.scm 1129 */
																																	BgL_test4223z00_9978
																																		=
																																		((bool_t)
																																		0);
																																}
																															else
																																{	/* Llib/unicode.scm 1129 */
																																	BgL_test4223z00_9978
																																		=
																																		((bool_t)
																																		1);
																																}
																														}
																													else
																														{	/* Llib/unicode.scm 1129 */
																															BgL_test4223z00_9978
																																= ((bool_t) 0);
																														}
																												}
																											if (BgL_test4223z00_9978)
																												{	/* Llib/unicode.scm 1131 */
																													bool_t
																														BgL_test4228z00_9986;
																													if ((BgL_rz00_2177 <
																															(BgL_endz00_81 -
																																3L)))
																														{	/* Llib/unicode.scm 1132 */
																															bool_t
																																BgL_test4230z00_9990;
																															{	/* Llib/unicode.scm 950 */
																																long
																																	BgL_nz00_5570;
																																BgL_nz00_5570 =
																																	(STRING_REF
																																	(BgL_strz00_78,
																																		(BgL_rz00_2177
																																			+ 1L)));
																																if (
																																	(BgL_nz00_5570
																																		>= 128L))
																																	{	/* Llib/unicode.scm 951 */
																																		BgL_test4230z00_9990
																																			=
																																			(BgL_nz00_5570
																																			<= 191L);
																																	}
																																else
																																	{	/* Llib/unicode.scm 951 */
																																		BgL_test4230z00_9990
																																			=
																																			((bool_t)
																																			0);
																																	}
																															}
																															if (BgL_test4230z00_9990)
																																{	/* Llib/unicode.scm 1133 */
																																	bool_t
																																		BgL_test4232z00_9997;
																																	{	/* Llib/unicode.scm 950 */
																																		long
																																			BgL_nz00_5578;
																																		BgL_nz00_5578
																																			=
																																			(STRING_REF
																																			(BgL_strz00_78,
																																				(BgL_rz00_2177
																																					+
																																					2L)));
																																		if (
																																			(BgL_nz00_5578
																																				>=
																																				128L))
																																			{	/* Llib/unicode.scm 951 */
																																				BgL_test4232z00_9997
																																					=
																																					(BgL_nz00_5578
																																					<=
																																					191L);
																																			}
																																		else
																																			{	/* Llib/unicode.scm 951 */
																																				BgL_test4232z00_9997
																																					=
																																					(
																																					(bool_t)
																																					0);
																																			}
																																	}
																																	if (BgL_test4232z00_9997)
																																		{	/* Llib/unicode.scm 950 */
																																			long
																																				BgL_nz00_5586;
																																			BgL_nz00_5586
																																				=
																																				(STRING_REF
																																				(BgL_strz00_78,
																																					(BgL_rz00_2177
																																						+
																																						3L)));
																																			if (
																																				(BgL_nz00_5586
																																					>=
																																					128L))
																																				{	/* Llib/unicode.scm 951 */
																																					BgL_test4228z00_9986
																																						=
																																						(BgL_nz00_5586
																																						<=
																																						191L);
																																				}
																																			else
																																				{	/* Llib/unicode.scm 951 */
																																					BgL_test4228z00_9986
																																						=
																																						(
																																						(bool_t)
																																						0);
																																				}
																																		}
																																	else
																																		{	/* Llib/unicode.scm 1133 */
																																			BgL_test4228z00_9986
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																}
																															else
																																{	/* Llib/unicode.scm 1132 */
																																	BgL_test4228z00_9986
																																		=
																																		((bool_t)
																																		0);
																																}
																														}
																													else
																														{	/* Llib/unicode.scm 1131 */
																															BgL_test4228z00_9986
																																= ((bool_t) 0);
																														}
																													if (BgL_test4228z00_9986)
																														{	/* Llib/unicode.scm 1131 */
																															STRING_SET
																																(BgL_resz00_2175,
																																BgL_wz00_2178,
																																BgL_cz00_2184);
																															{	/* Llib/unicode.scm 1137 */
																																unsigned char
																																	BgL_auxz00_10013;
																																long
																																	BgL_tmpz00_10011;
																																BgL_auxz00_10013
																																	=
																																	STRING_REF
																																	(BgL_strz00_78,
																																	(BgL_rz00_2177
																																		+ 1L));
																																BgL_tmpz00_10011
																																	=
																																	(BgL_wz00_2178
																																	+ 1L);
																																STRING_SET
																																	(BgL_resz00_2175,
																																	BgL_tmpz00_10011,
																																	BgL_auxz00_10013);
																															}
																															{	/* Llib/unicode.scm 1138 */
																																unsigned char
																																	BgL_auxz00_10019;
																																long
																																	BgL_tmpz00_10017;
																																BgL_auxz00_10019
																																	=
																																	STRING_REF
																																	(BgL_strz00_78,
																																	(BgL_rz00_2177
																																		+ 2L));
																																BgL_tmpz00_10017
																																	=
																																	(BgL_wz00_2178
																																	+ 2L);
																																STRING_SET
																																	(BgL_resz00_2175,
																																	BgL_tmpz00_10017,
																																	BgL_auxz00_10019);
																															}
																															{	/* Llib/unicode.scm 1139 */
																																unsigned char
																																	BgL_auxz00_10025;
																																long
																																	BgL_tmpz00_10023;
																																BgL_auxz00_10025
																																	=
																																	STRING_REF
																																	(BgL_strz00_78,
																																	(BgL_rz00_2177
																																		+ 3L));
																																BgL_tmpz00_10023
																																	=
																																	(BgL_wz00_2178
																																	+ 3L);
																																STRING_SET
																																	(BgL_resz00_2175,
																																	BgL_tmpz00_10023,
																																	BgL_auxz00_10025);
																															}
																															{
																																bool_t
																																	BgL_asciiz00_10033;
																																long
																																	BgL_wz00_10031;
																																long
																																	BgL_rz00_10029;
																																BgL_rz00_10029 =
																																	(BgL_rz00_2177
																																	+ 4L);
																																BgL_wz00_10031 =
																																	(BgL_wz00_2178
																																	+ 4L);
																																BgL_asciiz00_10033
																																	=
																																	((bool_t) 0);
																																BgL_asciiz00_2179
																																	=
																																	BgL_asciiz00_10033;
																																BgL_wz00_2178 =
																																	BgL_wz00_10031;
																																BgL_rz00_2177 =
																																	BgL_rz00_10029;
																																goto
																																	BgL_zc3z04anonymousza32070ze3z87_2180;
																															}
																														}
																													else
																														{	/* Llib/unicode.scm 1131 */
																															STRING_SET
																																(BgL_resz00_2175,
																																BgL_wz00_2178,
																																((unsigned char)
																																	239));
																															{	/* Llib/unicode.scm 956 */
																																long
																																	BgL_tmpz00_10035;
																																BgL_tmpz00_10035
																																	=
																																	(BgL_wz00_2178
																																	+ 1L);
																																STRING_SET
																																	(BgL_resz00_2175,
																																	BgL_tmpz00_10035,
																																	((unsigned
																																			char)
																																		191));
																															}
																															{	/* Llib/unicode.scm 957 */
																																long
																																	BgL_tmpz00_10038;
																																BgL_tmpz00_10038
																																	=
																																	(BgL_wz00_2178
																																	+ 2L);
																																STRING_SET
																																	(BgL_resz00_2175,
																																	BgL_tmpz00_10038,
																																	((unsigned
																																			char)
																																		189));
																															}
																															{
																																bool_t
																																	BgL_asciiz00_10045;
																																long
																																	BgL_wz00_10043;
																																long
																																	BgL_rz00_10041;
																																BgL_rz00_10041 =
																																	(BgL_rz00_2177
																																	+ 1L);
																																BgL_wz00_10043 =
																																	(BgL_wz00_2178
																																	+ 3L);
																																BgL_asciiz00_10045
																																	=
																																	((bool_t) 0);
																																BgL_asciiz00_2179
																																	=
																																	BgL_asciiz00_10045;
																																BgL_wz00_2178 =
																																	BgL_wz00_10043;
																																BgL_rz00_2177 =
																																	BgL_rz00_10041;
																																goto
																																	BgL_zc3z04anonymousza32070ze3z87_2180;
																															}
																														}
																												}
																											else
																												{	/* Llib/unicode.scm 1128 */
																													if (
																														(BgL_nz00_2185 <=
																															247L))
																														{	/* Llib/unicode.scm 1146 */
																															bool_t
																																BgL_test4236z00_10048;
																															if ((BgL_rz00_2177
																																	<
																																	(BgL_endz00_81
																																		- 3L)))
																																{	/* Llib/unicode.scm 1147 */
																																	bool_t
																																		BgL_test4238z00_10052;
																																	{	/* Llib/unicode.scm 950 */
																																		long
																																			BgL_nz00_5639;
																																		BgL_nz00_5639
																																			=
																																			(STRING_REF
																																			(BgL_strz00_78,
																																				(BgL_rz00_2177
																																					+
																																					1L)));
																																		if (
																																			(BgL_nz00_5639
																																				>=
																																				128L))
																																			{	/* Llib/unicode.scm 951 */
																																				BgL_test4238z00_10052
																																					=
																																					(BgL_nz00_5639
																																					<=
																																					191L);
																																			}
																																		else
																																			{	/* Llib/unicode.scm 951 */
																																				BgL_test4238z00_10052
																																					=
																																					(
																																					(bool_t)
																																					0);
																																			}
																																	}
																																	if (BgL_test4238z00_10052)
																																		{	/* Llib/unicode.scm 1148 */
																																			bool_t
																																				BgL_test4240z00_10059;
																																			{	/* Llib/unicode.scm 950 */
																																				long
																																					BgL_nz00_5647;
																																				BgL_nz00_5647
																																					=
																																					(STRING_REF
																																					(BgL_strz00_78,
																																						(BgL_rz00_2177
																																							+
																																							2L)));
																																				if (
																																					(BgL_nz00_5647
																																						>=
																																						128L))
																																					{	/* Llib/unicode.scm 951 */
																																						BgL_test4240z00_10059
																																							=
																																							(BgL_nz00_5647
																																							<=
																																							191L);
																																					}
																																				else
																																					{	/* Llib/unicode.scm 951 */
																																						BgL_test4240z00_10059
																																							=
																																							(
																																							(bool_t)
																																							0);
																																					}
																																			}
																																			if (BgL_test4240z00_10059)
																																				{	/* Llib/unicode.scm 950 */
																																					long
																																						BgL_nz00_5655;
																																					BgL_nz00_5655
																																						=
																																						(STRING_REF
																																						(BgL_strz00_78,
																																							(BgL_rz00_2177
																																								+
																																								3L)));
																																					if (
																																						(BgL_nz00_5655
																																							>=
																																							128L))
																																						{	/* Llib/unicode.scm 951 */
																																							BgL_test4236z00_10048
																																								=
																																								(BgL_nz00_5655
																																								<=
																																								191L);
																																						}
																																					else
																																						{	/* Llib/unicode.scm 951 */
																																							BgL_test4236z00_10048
																																								=
																																								(
																																								(bool_t)
																																								0);
																																						}
																																				}
																																			else
																																				{	/* Llib/unicode.scm 1148 */
																																					BgL_test4236z00_10048
																																						=
																																						(
																																						(bool_t)
																																						0);
																																				}
																																		}
																																	else
																																		{	/* Llib/unicode.scm 1147 */
																																			BgL_test4236z00_10048
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																}
																															else
																																{	/* Llib/unicode.scm 1146 */
																																	BgL_test4236z00_10048
																																		=
																																		((bool_t)
																																		0);
																																}
																															if (BgL_test4236z00_10048)
																																{	/* Llib/unicode.scm 1146 */
																																	STRING_SET
																																		(BgL_resz00_2175,
																																		BgL_wz00_2178,
																																		BgL_cz00_2184);
																																	{	/* Llib/unicode.scm 1152 */
																																		unsigned
																																			char
																																			BgL_auxz00_10075;
																																		long
																																			BgL_tmpz00_10073;
																																		BgL_auxz00_10075
																																			=
																																			STRING_REF
																																			(BgL_strz00_78,
																																			(BgL_rz00_2177
																																				+ 1L));
																																		BgL_tmpz00_10073
																																			=
																																			(BgL_wz00_2178
																																			+ 1L);
																																		STRING_SET
																																			(BgL_resz00_2175,
																																			BgL_tmpz00_10073,
																																			BgL_auxz00_10075);
																																	}
																																	{	/* Llib/unicode.scm 1153 */
																																		unsigned
																																			char
																																			BgL_auxz00_10081;
																																		long
																																			BgL_tmpz00_10079;
																																		BgL_auxz00_10081
																																			=
																																			STRING_REF
																																			(BgL_strz00_78,
																																			(BgL_rz00_2177
																																				+ 2L));
																																		BgL_tmpz00_10079
																																			=
																																			(BgL_wz00_2178
																																			+ 2L);
																																		STRING_SET
																																			(BgL_resz00_2175,
																																			BgL_tmpz00_10079,
																																			BgL_auxz00_10081);
																																	}
																																	{	/* Llib/unicode.scm 1154 */
																																		unsigned
																																			char
																																			BgL_auxz00_10087;
																																		long
																																			BgL_tmpz00_10085;
																																		BgL_auxz00_10087
																																			=
																																			STRING_REF
																																			(BgL_strz00_78,
																																			(BgL_rz00_2177
																																				+ 3L));
																																		BgL_tmpz00_10085
																																			=
																																			(BgL_wz00_2178
																																			+ 3L);
																																		STRING_SET
																																			(BgL_resz00_2175,
																																			BgL_tmpz00_10085,
																																			BgL_auxz00_10087);
																																	}
																																	{
																																		bool_t
																																			BgL_asciiz00_10095;
																																		long
																																			BgL_wz00_10093;
																																		long
																																			BgL_rz00_10091;
																																		BgL_rz00_10091
																																			=
																																			(BgL_rz00_2177
																																			+ 4L);
																																		BgL_wz00_10093
																																			=
																																			(BgL_wz00_2178
																																			+ 4L);
																																		BgL_asciiz00_10095
																																			=
																																			((bool_t)
																																			0);
																																		BgL_asciiz00_2179
																																			=
																																			BgL_asciiz00_10095;
																																		BgL_wz00_2178
																																			=
																																			BgL_wz00_10093;
																																		BgL_rz00_2177
																																			=
																																			BgL_rz00_10091;
																																		goto
																																			BgL_zc3z04anonymousza32070ze3z87_2180;
																																	}
																																}
																															else
																																{	/* Llib/unicode.scm 1146 */
																																	STRING_SET
																																		(BgL_resz00_2175,
																																		BgL_wz00_2178,
																																		((unsigned
																																				char)
																																			239));
																																	{	/* Llib/unicode.scm 956 */
																																		long
																																			BgL_tmpz00_10097;
																																		BgL_tmpz00_10097
																																			=
																																			(BgL_wz00_2178
																																			+ 1L);
																																		STRING_SET
																																			(BgL_resz00_2175,
																																			BgL_tmpz00_10097,
																																			((unsigned
																																					char)
																																				191));
																																	}
																																	{	/* Llib/unicode.scm 957 */
																																		long
																																			BgL_tmpz00_10100;
																																		BgL_tmpz00_10100
																																			=
																																			(BgL_wz00_2178
																																			+ 2L);
																																		STRING_SET
																																			(BgL_resz00_2175,
																																			BgL_tmpz00_10100,
																																			((unsigned
																																					char)
																																				189));
																																	}
																																	{
																																		bool_t
																																			BgL_asciiz00_10107;
																																		long
																																			BgL_wz00_10105;
																																		long
																																			BgL_rz00_10103;
																																		BgL_rz00_10103
																																			=
																																			(BgL_rz00_2177
																																			+ 1L);
																																		BgL_wz00_10105
																																			=
																																			(BgL_wz00_2178
																																			+ 3L);
																																		BgL_asciiz00_10107
																																			=
																																			((bool_t)
																																			0);
																																		BgL_asciiz00_2179
																																			=
																																			BgL_asciiz00_10107;
																																		BgL_wz00_2178
																																			=
																																			BgL_wz00_10105;
																																		BgL_rz00_2177
																																			=
																																			BgL_rz00_10103;
																																		goto
																																			BgL_zc3z04anonymousza32070ze3z87_2180;
																																	}
																																}
																														}
																													else
																														{	/* Llib/unicode.scm 1144 */
																															if (
																																(BgL_nz00_2185
																																	<= 251L))
																																{	/* Llib/unicode.scm 1160 */
																																	bool_t
																																		BgL_test4244z00_10110;
																																	if (
																																		(BgL_rz00_2177
																																			<
																																			(BgL_endz00_81
																																				- 4L)))
																																		{	/* Llib/unicode.scm 1161 */
																																			bool_t
																																				BgL_test4246z00_10114;
																																			{	/* Llib/unicode.scm 950 */
																																				long
																																					BgL_nz00_5708;
																																				BgL_nz00_5708
																																					=
																																					(STRING_REF
																																					(BgL_strz00_78,
																																						(BgL_rz00_2177
																																							+
																																							1L)));
																																				if (
																																					(BgL_nz00_5708
																																						>=
																																						128L))
																																					{	/* Llib/unicode.scm 951 */
																																						BgL_test4246z00_10114
																																							=
																																							(BgL_nz00_5708
																																							<=
																																							191L);
																																					}
																																				else
																																					{	/* Llib/unicode.scm 951 */
																																						BgL_test4246z00_10114
																																							=
																																							(
																																							(bool_t)
																																							0);
																																					}
																																			}
																																			if (BgL_test4246z00_10114)
																																				{	/* Llib/unicode.scm 1162 */
																																					bool_t
																																						BgL_test4248z00_10121;
																																					{	/* Llib/unicode.scm 950 */
																																						long
																																							BgL_nz00_5716;
																																						BgL_nz00_5716
																																							=
																																							(STRING_REF
																																							(BgL_strz00_78,
																																								(BgL_rz00_2177
																																									+
																																									2L)));
																																						if (
																																							(BgL_nz00_5716
																																								>=
																																								128L))
																																							{	/* Llib/unicode.scm 951 */
																																								BgL_test4248z00_10121
																																									=
																																									(BgL_nz00_5716
																																									<=
																																									191L);
																																							}
																																						else
																																							{	/* Llib/unicode.scm 951 */
																																								BgL_test4248z00_10121
																																									=
																																									(
																																									(bool_t)
																																									0);
																																							}
																																					}
																																					if (BgL_test4248z00_10121)
																																						{	/* Llib/unicode.scm 1163 */
																																							bool_t
																																								BgL_test4250z00_10128;
																																							{	/* Llib/unicode.scm 950 */
																																								long
																																									BgL_nz00_5724;
																																								BgL_nz00_5724
																																									=
																																									(STRING_REF
																																									(BgL_strz00_78,
																																										(BgL_rz00_2177
																																											+
																																											3L)));
																																								if ((BgL_nz00_5724 >= 128L))
																																									{	/* Llib/unicode.scm 951 */
																																										BgL_test4250z00_10128
																																											=
																																											(BgL_nz00_5724
																																											<=
																																											191L);
																																									}
																																								else
																																									{	/* Llib/unicode.scm 951 */
																																										BgL_test4250z00_10128
																																											=
																																											(
																																											(bool_t)
																																											0);
																																									}
																																							}
																																							if (BgL_test4250z00_10128)
																																								{	/* Llib/unicode.scm 950 */
																																									long
																																										BgL_nz00_5732;
																																									BgL_nz00_5732
																																										=
																																										(STRING_REF
																																										(BgL_strz00_78,
																																											(BgL_rz00_2177
																																												+
																																												4L)));
																																									if ((BgL_nz00_5732 >= 128L))
																																										{	/* Llib/unicode.scm 951 */
																																											BgL_test4244z00_10110
																																												=
																																												(BgL_nz00_5732
																																												<=
																																												191L);
																																										}
																																									else
																																										{	/* Llib/unicode.scm 951 */
																																											BgL_test4244z00_10110
																																												=
																																												(
																																												(bool_t)
																																												0);
																																										}
																																								}
																																							else
																																								{	/* Llib/unicode.scm 1163 */
																																									BgL_test4244z00_10110
																																										=
																																										(
																																										(bool_t)
																																										0);
																																								}
																																						}
																																					else
																																						{	/* Llib/unicode.scm 1162 */
																																							BgL_test4244z00_10110
																																								=
																																								(
																																								(bool_t)
																																								0);
																																						}
																																				}
																																			else
																																				{	/* Llib/unicode.scm 1161 */
																																					BgL_test4244z00_10110
																																						=
																																						(
																																						(bool_t)
																																						0);
																																				}
																																		}
																																	else
																																		{	/* Llib/unicode.scm 1160 */
																																			BgL_test4244z00_10110
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																	if (BgL_test4244z00_10110)
																																		{	/* Llib/unicode.scm 1160 */
																																			STRING_SET
																																				(BgL_resz00_2175,
																																				BgL_wz00_2178,
																																				BgL_cz00_2184);
																																			{	/* Llib/unicode.scm 1167 */
																																				unsigned
																																					char
																																					BgL_auxz00_10144;
																																				long
																																					BgL_tmpz00_10142;
																																				BgL_auxz00_10144
																																					=
																																					STRING_REF
																																					(BgL_strz00_78,
																																					(BgL_rz00_2177
																																						+
																																						1L));
																																				BgL_tmpz00_10142
																																					=
																																					(BgL_wz00_2178
																																					+ 1L);
																																				STRING_SET
																																					(BgL_resz00_2175,
																																					BgL_tmpz00_10142,
																																					BgL_auxz00_10144);
																																			}
																																			{	/* Llib/unicode.scm 1168 */
																																				unsigned
																																					char
																																					BgL_auxz00_10150;
																																				long
																																					BgL_tmpz00_10148;
																																				BgL_auxz00_10150
																																					=
																																					STRING_REF
																																					(BgL_strz00_78,
																																					(BgL_rz00_2177
																																						+
																																						2L));
																																				BgL_tmpz00_10148
																																					=
																																					(BgL_wz00_2178
																																					+ 2L);
																																				STRING_SET
																																					(BgL_resz00_2175,
																																					BgL_tmpz00_10148,
																																					BgL_auxz00_10150);
																																			}
																																			{	/* Llib/unicode.scm 1169 */
																																				unsigned
																																					char
																																					BgL_auxz00_10156;
																																				long
																																					BgL_tmpz00_10154;
																																				BgL_auxz00_10156
																																					=
																																					STRING_REF
																																					(BgL_strz00_78,
																																					(BgL_rz00_2177
																																						+
																																						3L));
																																				BgL_tmpz00_10154
																																					=
																																					(BgL_wz00_2178
																																					+ 3L);
																																				STRING_SET
																																					(BgL_resz00_2175,
																																					BgL_tmpz00_10154,
																																					BgL_auxz00_10156);
																																			}
																																			{	/* Llib/unicode.scm 1170 */
																																				unsigned
																																					char
																																					BgL_auxz00_10162;
																																				long
																																					BgL_tmpz00_10160;
																																				BgL_auxz00_10162
																																					=
																																					STRING_REF
																																					(BgL_strz00_78,
																																					(BgL_rz00_2177
																																						+
																																						4L));
																																				BgL_tmpz00_10160
																																					=
																																					(BgL_wz00_2178
																																					+ 4L);
																																				STRING_SET
																																					(BgL_resz00_2175,
																																					BgL_tmpz00_10160,
																																					BgL_auxz00_10162);
																																			}
																																			{
																																				bool_t
																																					BgL_asciiz00_10170;
																																				long
																																					BgL_wz00_10168;
																																				long
																																					BgL_rz00_10166;
																																				BgL_rz00_10166
																																					=
																																					(BgL_rz00_2177
																																					+ 5L);
																																				BgL_wz00_10168
																																					=
																																					(BgL_wz00_2178
																																					+ 5L);
																																				BgL_asciiz00_10170
																																					=
																																					(
																																					(bool_t)
																																					0);
																																				BgL_asciiz00_2179
																																					=
																																					BgL_asciiz00_10170;
																																				BgL_wz00_2178
																																					=
																																					BgL_wz00_10168;
																																				BgL_rz00_2177
																																					=
																																					BgL_rz00_10166;
																																				goto
																																					BgL_zc3z04anonymousza32070ze3z87_2180;
																																			}
																																		}
																																	else
																																		{	/* Llib/unicode.scm 1160 */
																																			STRING_SET
																																				(BgL_resz00_2175,
																																				BgL_wz00_2178,
																																				((unsigned char) 239));
																																			{	/* Llib/unicode.scm 956 */
																																				long
																																					BgL_tmpz00_10172;
																																				BgL_tmpz00_10172
																																					=
																																					(BgL_wz00_2178
																																					+ 1L);
																																				STRING_SET
																																					(BgL_resz00_2175,
																																					BgL_tmpz00_10172,
																																					((unsigned char) 191));
																																			}
																																			{	/* Llib/unicode.scm 957 */
																																				long
																																					BgL_tmpz00_10175;
																																				BgL_tmpz00_10175
																																					=
																																					(BgL_wz00_2178
																																					+ 2L);
																																				STRING_SET
																																					(BgL_resz00_2175,
																																					BgL_tmpz00_10175,
																																					((unsigned char) 189));
																																			}
																																			{
																																				bool_t
																																					BgL_asciiz00_10182;
																																				long
																																					BgL_wz00_10180;
																																				long
																																					BgL_rz00_10178;
																																				BgL_rz00_10178
																																					=
																																					(BgL_rz00_2177
																																					+ 1L);
																																				BgL_wz00_10180
																																					=
																																					(BgL_wz00_2178
																																					+ 3L);
																																				BgL_asciiz00_10182
																																					=
																																					(
																																					(bool_t)
																																					0);
																																				BgL_asciiz00_2179
																																					=
																																					BgL_asciiz00_10182;
																																				BgL_wz00_2178
																																					=
																																					BgL_wz00_10180;
																																				BgL_rz00_2177
																																					=
																																					BgL_rz00_10178;
																																				goto
																																					BgL_zc3z04anonymousza32070ze3z87_2180;
																																			}
																																		}
																																}
																															else
																																{	/* Llib/unicode.scm 1159 */
																																	if (
																																		(BgL_nz00_2185
																																			<= 253L))
																																		{	/* Llib/unicode.scm 1176 */
																																			bool_t
																																				BgL_test4254z00_10185;
																																			if (
																																				(BgL_rz00_2177
																																					<
																																					(BgL_endz00_81
																																						-
																																						5L)))
																																				{	/* Llib/unicode.scm 1177 */
																																					bool_t
																																						BgL_test4256z00_10189;
																																					{	/* Llib/unicode.scm 950 */
																																						long
																																							BgL_nz00_5792;
																																						BgL_nz00_5792
																																							=
																																							(STRING_REF
																																							(BgL_strz00_78,
																																								(BgL_rz00_2177
																																									+
																																									1L)));
																																						if (
																																							(BgL_nz00_5792
																																								>=
																																								128L))
																																							{	/* Llib/unicode.scm 951 */
																																								BgL_test4256z00_10189
																																									=
																																									(BgL_nz00_5792
																																									<=
																																									191L);
																																							}
																																						else
																																							{	/* Llib/unicode.scm 951 */
																																								BgL_test4256z00_10189
																																									=
																																									(
																																									(bool_t)
																																									0);
																																							}
																																					}
																																					if (BgL_test4256z00_10189)
																																						{	/* Llib/unicode.scm 1178 */
																																							bool_t
																																								BgL_test4258z00_10196;
																																							{	/* Llib/unicode.scm 950 */
																																								long
																																									BgL_nz00_5800;
																																								BgL_nz00_5800
																																									=
																																									(STRING_REF
																																									(BgL_strz00_78,
																																										(BgL_rz00_2177
																																											+
																																											2L)));
																																								if ((BgL_nz00_5800 >= 128L))
																																									{	/* Llib/unicode.scm 951 */
																																										BgL_test4258z00_10196
																																											=
																																											(BgL_nz00_5800
																																											<=
																																											191L);
																																									}
																																								else
																																									{	/* Llib/unicode.scm 951 */
																																										BgL_test4258z00_10196
																																											=
																																											(
																																											(bool_t)
																																											0);
																																									}
																																							}
																																							if (BgL_test4258z00_10196)
																																								{	/* Llib/unicode.scm 1179 */
																																									bool_t
																																										BgL_test4260z00_10203;
																																									{	/* Llib/unicode.scm 950 */
																																										long
																																											BgL_nz00_5808;
																																										BgL_nz00_5808
																																											=
																																											(STRING_REF
																																											(BgL_strz00_78,
																																												(BgL_rz00_2177
																																													+
																																													3L)));
																																										if ((BgL_nz00_5808 >= 128L))
																																											{	/* Llib/unicode.scm 951 */
																																												BgL_test4260z00_10203
																																													=
																																													(BgL_nz00_5808
																																													<=
																																													191L);
																																											}
																																										else
																																											{	/* Llib/unicode.scm 951 */
																																												BgL_test4260z00_10203
																																													=
																																													(
																																													(bool_t)
																																													0);
																																											}
																																									}
																																									if (BgL_test4260z00_10203)
																																										{	/* Llib/unicode.scm 1180 */
																																											bool_t
																																												BgL_test4262z00_10210;
																																											{	/* Llib/unicode.scm 950 */
																																												long
																																													BgL_nz00_5816;
																																												BgL_nz00_5816
																																													=
																																													(STRING_REF
																																													(BgL_strz00_78,
																																														(BgL_rz00_2177
																																															+
																																															4L)));
																																												if ((BgL_nz00_5816 >= 128L))
																																													{	/* Llib/unicode.scm 951 */
																																														BgL_test4262z00_10210
																																															=
																																															(BgL_nz00_5816
																																															<=
																																															191L);
																																													}
																																												else
																																													{	/* Llib/unicode.scm 951 */
																																														BgL_test4262z00_10210
																																															=
																																															(
																																															(bool_t)
																																															0);
																																													}
																																											}
																																											if (BgL_test4262z00_10210)
																																												{	/* Llib/unicode.scm 950 */
																																													long
																																														BgL_nz00_5824;
																																													BgL_nz00_5824
																																														=
																																														(STRING_REF
																																														(BgL_strz00_78,
																																															(BgL_rz00_2177
																																																+
																																																5L)));
																																													if ((BgL_nz00_5824 >= 128L))
																																														{	/* Llib/unicode.scm 951 */
																																															BgL_test4254z00_10185
																																																=
																																																(BgL_nz00_5824
																																																<=
																																																191L);
																																														}
																																													else
																																														{	/* Llib/unicode.scm 951 */
																																															BgL_test4254z00_10185
																																																=
																																																(
																																																(bool_t)
																																																0);
																																														}
																																												}
																																											else
																																												{	/* Llib/unicode.scm 1180 */
																																													BgL_test4254z00_10185
																																														=
																																														(
																																														(bool_t)
																																														0);
																																												}
																																										}
																																									else
																																										{	/* Llib/unicode.scm 1179 */
																																											BgL_test4254z00_10185
																																												=
																																												(
																																												(bool_t)
																																												0);
																																										}
																																								}
																																							else
																																								{	/* Llib/unicode.scm 1178 */
																																									BgL_test4254z00_10185
																																										=
																																										(
																																										(bool_t)
																																										0);
																																								}
																																						}
																																					else
																																						{	/* Llib/unicode.scm 1177 */
																																							BgL_test4254z00_10185
																																								=
																																								(
																																								(bool_t)
																																								0);
																																						}
																																				}
																																			else
																																				{	/* Llib/unicode.scm 1176 */
																																					BgL_test4254z00_10185
																																						=
																																						(
																																						(bool_t)
																																						0);
																																				}
																																			if (BgL_test4254z00_10185)
																																				{	/* Llib/unicode.scm 1176 */
																																					STRING_SET
																																						(BgL_resz00_2175,
																																						BgL_wz00_2178,
																																						BgL_cz00_2184);
																																					{	/* Llib/unicode.scm 1184 */
																																						unsigned
																																							char
																																							BgL_auxz00_10226;
																																						long
																																							BgL_tmpz00_10224;
																																						BgL_auxz00_10226
																																							=
																																							STRING_REF
																																							(BgL_strz00_78,
																																							(BgL_rz00_2177
																																								+
																																								1L));
																																						BgL_tmpz00_10224
																																							=
																																							(BgL_wz00_2178
																																							+
																																							1L);
																																						STRING_SET
																																							(BgL_resz00_2175,
																																							BgL_tmpz00_10224,
																																							BgL_auxz00_10226);
																																					}
																																					{	/* Llib/unicode.scm 1185 */
																																						unsigned
																																							char
																																							BgL_auxz00_10232;
																																						long
																																							BgL_tmpz00_10230;
																																						BgL_auxz00_10232
																																							=
																																							STRING_REF
																																							(BgL_strz00_78,
																																							(BgL_rz00_2177
																																								+
																																								2L));
																																						BgL_tmpz00_10230
																																							=
																																							(BgL_wz00_2178
																																							+
																																							2L);
																																						STRING_SET
																																							(BgL_resz00_2175,
																																							BgL_tmpz00_10230,
																																							BgL_auxz00_10232);
																																					}
																																					{	/* Llib/unicode.scm 1186 */
																																						unsigned
																																							char
																																							BgL_auxz00_10238;
																																						long
																																							BgL_tmpz00_10236;
																																						BgL_auxz00_10238
																																							=
																																							STRING_REF
																																							(BgL_strz00_78,
																																							(BgL_rz00_2177
																																								+
																																								3L));
																																						BgL_tmpz00_10236
																																							=
																																							(BgL_wz00_2178
																																							+
																																							3L);
																																						STRING_SET
																																							(BgL_resz00_2175,
																																							BgL_tmpz00_10236,
																																							BgL_auxz00_10238);
																																					}
																																					{	/* Llib/unicode.scm 1187 */
																																						unsigned
																																							char
																																							BgL_auxz00_10244;
																																						long
																																							BgL_tmpz00_10242;
																																						BgL_auxz00_10244
																																							=
																																							STRING_REF
																																							(BgL_strz00_78,
																																							(BgL_rz00_2177
																																								+
																																								4L));
																																						BgL_tmpz00_10242
																																							=
																																							(BgL_wz00_2178
																																							+
																																							4L);
																																						STRING_SET
																																							(BgL_resz00_2175,
																																							BgL_tmpz00_10242,
																																							BgL_auxz00_10244);
																																					}
																																					{	/* Llib/unicode.scm 1188 */
																																						unsigned
																																							char
																																							BgL_auxz00_10250;
																																						long
																																							BgL_tmpz00_10248;
																																						BgL_auxz00_10250
																																							=
																																							STRING_REF
																																							(BgL_strz00_78,
																																							(BgL_rz00_2177
																																								+
																																								5L));
																																						BgL_tmpz00_10248
																																							=
																																							(BgL_wz00_2178
																																							+
																																							5L);
																																						STRING_SET
																																							(BgL_resz00_2175,
																																							BgL_tmpz00_10248,
																																							BgL_auxz00_10250);
																																					}
																																					{
																																						bool_t
																																							BgL_asciiz00_10258;
																																						long
																																							BgL_wz00_10256;
																																						long
																																							BgL_rz00_10254;
																																						BgL_rz00_10254
																																							=
																																							(BgL_rz00_2177
																																							+
																																							6L);
																																						BgL_wz00_10256
																																							=
																																							(BgL_wz00_2178
																																							+
																																							6L);
																																						BgL_asciiz00_10258
																																							=
																																							(
																																							(bool_t)
																																							0);
																																						BgL_asciiz00_2179
																																							=
																																							BgL_asciiz00_10258;
																																						BgL_wz00_2178
																																							=
																																							BgL_wz00_10256;
																																						BgL_rz00_2177
																																							=
																																							BgL_rz00_10254;
																																						goto
																																							BgL_zc3z04anonymousza32070ze3z87_2180;
																																					}
																																				}
																																			else
																																				{	/* Llib/unicode.scm 1176 */
																																					STRING_SET
																																						(BgL_resz00_2175,
																																						BgL_wz00_2178,
																																						((unsigned char) 239));
																																					{	/* Llib/unicode.scm 956 */
																																						long
																																							BgL_tmpz00_10260;
																																						BgL_tmpz00_10260
																																							=
																																							(BgL_wz00_2178
																																							+
																																							1L);
																																						STRING_SET
																																							(BgL_resz00_2175,
																																							BgL_tmpz00_10260,
																																							((unsigned char) 191));
																																					}
																																					{	/* Llib/unicode.scm 957 */
																																						long
																																							BgL_tmpz00_10263;
																																						BgL_tmpz00_10263
																																							=
																																							(BgL_wz00_2178
																																							+
																																							2L);
																																						STRING_SET
																																							(BgL_resz00_2175,
																																							BgL_tmpz00_10263,
																																							((unsigned char) 189));
																																					}
																																					{
																																						bool_t
																																							BgL_asciiz00_10270;
																																						long
																																							BgL_wz00_10268;
																																						long
																																							BgL_rz00_10266;
																																						BgL_rz00_10266
																																							=
																																							(BgL_rz00_2177
																																							+
																																							1L);
																																						BgL_wz00_10268
																																							=
																																							(BgL_wz00_2178
																																							+
																																							3L);
																																						BgL_asciiz00_10270
																																							=
																																							(
																																							(bool_t)
																																							0);
																																						BgL_asciiz00_2179
																																							=
																																							BgL_asciiz00_10270;
																																						BgL_wz00_2178
																																							=
																																							BgL_wz00_10268;
																																						BgL_rz00_2177
																																							=
																																							BgL_rz00_10266;
																																						goto
																																							BgL_zc3z04anonymousza32070ze3z87_2180;
																																					}
																																				}
																																		}
																																	else
																																		{	/* Llib/unicode.scm 1175 */
																																			STRING_SET
																																				(BgL_resz00_2175,
																																				BgL_wz00_2178,
																																				((unsigned char) 239));
																																			{	/* Llib/unicode.scm 956 */
																																				long
																																					BgL_tmpz00_10272;
																																				BgL_tmpz00_10272
																																					=
																																					(BgL_wz00_2178
																																					+ 1L);
																																				STRING_SET
																																					(BgL_resz00_2175,
																																					BgL_tmpz00_10272,
																																					((unsigned char) 191));
																																			}
																																			{	/* Llib/unicode.scm 957 */
																																				long
																																					BgL_tmpz00_10275;
																																				BgL_tmpz00_10275
																																					=
																																					(BgL_wz00_2178
																																					+ 2L);
																																				STRING_SET
																																					(BgL_resz00_2175,
																																					BgL_tmpz00_10275,
																																					((unsigned char) 189));
																																			}
																																			{
																																				bool_t
																																					BgL_asciiz00_10282;
																																				long
																																					BgL_wz00_10280;
																																				long
																																					BgL_rz00_10278;
																																				BgL_rz00_10278
																																					=
																																					(BgL_rz00_2177
																																					+ 1L);
																																				BgL_wz00_10280
																																					=
																																					(BgL_wz00_2178
																																					+ 3L);
																																				BgL_asciiz00_10282
																																					=
																																					(
																																					(bool_t)
																																					0);
																																				BgL_asciiz00_2179
																																					=
																																					BgL_asciiz00_10282;
																																				BgL_wz00_2178
																																					=
																																					BgL_wz00_10280;
																																				BgL_rz00_2177
																																					=
																																					BgL_rz00_10278;
																																				goto
																																					BgL_zc3z04anonymousza32070ze3z87_2180;
																																			}
																																		}
																																}
																														}
																												}
																										}
																								}
																						}
																				}
																		}
																}
														}
												}
											}
										}
								}
							}
						}
					}
			}
		}

	}



/* utf8-char-size */
	BGL_EXPORTED_DEF long BGl_utf8zd2charzd2siza7eza7zz__unicodez00(unsigned char
		BgL_cz00_84)
	{
		{	/* Llib/unicode.scm 1201 */
			{	/* Llib/unicode.scm 1203 */
				long BgL_arg2537z00_7267;

				BgL_arg2537z00_7267 = (((unsigned char) (BgL_cz00_84)) >> (int) (4L));
				{	/* Llib/unicode.scm 1202 */
					obj_t BgL_vectorz00_7268;

					BgL_vectorz00_7268 = BGl_vector3673z00zz__unicodez00;
					return
						(long) CINT(VECTOR_REF(BgL_vectorz00_7268, BgL_arg2537z00_7267));
		}}}

	}



/* &utf8-char-size */
	obj_t BGl_z62utf8zd2charzd2siza7ezc5zz__unicodez00(obj_t BgL_envz00_6950,
		obj_t BgL_cz00_6951)
	{
		{	/* Llib/unicode.scm 1201 */
			{	/* Llib/unicode.scm 1203 */
				long BgL_tmpz00_10289;

				{	/* Llib/unicode.scm 1203 */
					unsigned char BgL_auxz00_10290;

					{	/* Llib/unicode.scm 1203 */
						obj_t BgL_tmpz00_10291;

						if (CHARP(BgL_cz00_6951))
							{	/* Llib/unicode.scm 1203 */
								BgL_tmpz00_10291 = BgL_cz00_6951;
							}
						else
							{
								obj_t BgL_auxz00_10294;

								BgL_auxz00_10294 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string3618z00zz__unicodez00, BINT(48323L),
									BGl_string3675z00zz__unicodez00,
									BGl_string3676z00zz__unicodez00, BgL_cz00_6951);
								FAILURE(BgL_auxz00_10294, BFALSE, BFALSE);
							}
						BgL_auxz00_10290 = CCHAR(BgL_tmpz00_10291);
					}
					BgL_tmpz00_10289 =
						BGl_utf8zd2charzd2siza7eza7zz__unicodez00(BgL_auxz00_10290);
				}
				return BINT(BgL_tmpz00_10289);
			}
		}

	}



/* utf8-string-length */
	BGL_EXPORTED_DEF long BGl_utf8zd2stringzd2lengthz00zz__unicodez00(obj_t
		BgL_strz00_86)
	{
		{	/* Llib/unicode.scm 1217 */
			{	/* Llib/unicode.scm 1218 */
				long BgL_lenz00_2665;

				BgL_lenz00_2665 = STRING_LENGTH(BgL_strz00_86);
				{
					long BgL_rz00_5947;
					long BgL_lz00_5948;

					BgL_rz00_5947 = 0L;
					BgL_lz00_5948 = 0L;
				BgL_loopz00_5946:
					if ((BgL_rz00_5947 == BgL_lenz00_2665))
						{	/* Llib/unicode.scm 1221 */
							return BgL_lz00_5948;
						}
					else
						{	/* Llib/unicode.scm 1223 */
							long BgL_arg2543z00_5955;
							long BgL_arg2545z00_5956;

							{	/* Llib/unicode.scm 1223 */
								long BgL_arg2546z00_5957;

								{	/* Llib/unicode.scm 1223 */
									unsigned char BgL_arg2548z00_5958;

									BgL_arg2548z00_5958 =
										STRING_REF(BgL_strz00_86, BgL_rz00_5947);
									{	/* Llib/unicode.scm 1223 */
										long BgL_res3316z00_5970;

										{	/* Llib/unicode.scm 1223 */
											unsigned char BgL_cz00_5963;

											BgL_cz00_5963 = (char) (BgL_arg2548z00_5958);
											{	/* Llib/unicode.scm 1203 */
												long BgL_arg2537z00_5964;

												BgL_arg2537z00_5964 =
													(((unsigned char) (BgL_cz00_5963)) >> (int) (4L));
												{	/* Llib/unicode.scm 1202 */
													obj_t BgL_vectorz00_5968;

													BgL_vectorz00_5968 = BGl_vector3673z00zz__unicodez00;
													BgL_res3316z00_5970 =
														(long) CINT(VECTOR_REF(BgL_vectorz00_5968,
															BgL_arg2537z00_5964));
										}}}
										BgL_arg2546z00_5957 = BgL_res3316z00_5970;
								}}
								BgL_arg2543z00_5955 = (BgL_rz00_5947 + BgL_arg2546z00_5957);
							}
							BgL_arg2545z00_5956 = (BgL_lz00_5948 + 1L);
							{
								long BgL_lz00_10315;
								long BgL_rz00_10314;

								BgL_rz00_10314 = BgL_arg2543z00_5955;
								BgL_lz00_10315 = BgL_arg2545z00_5956;
								BgL_lz00_5948 = BgL_lz00_10315;
								BgL_rz00_5947 = BgL_rz00_10314;
								goto BgL_loopz00_5946;
							}
						}
				}
			}
		}

	}



/* &utf8-string-length */
	obj_t BGl_z62utf8zd2stringzd2lengthz62zz__unicodez00(obj_t BgL_envz00_6952,
		obj_t BgL_strz00_6953)
	{
		{	/* Llib/unicode.scm 1217 */
			{	/* Llib/unicode.scm 1218 */
				long BgL_tmpz00_10316;

				{	/* Llib/unicode.scm 1218 */
					obj_t BgL_auxz00_10317;

					if (STRINGP(BgL_strz00_6953))
						{	/* Llib/unicode.scm 1218 */
							BgL_auxz00_10317 = BgL_strz00_6953;
						}
					else
						{
							obj_t BgL_auxz00_10320;

							BgL_auxz00_10320 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(49101L), BGl_string3677z00zz__unicodez00,
								BGl_string3661z00zz__unicodez00, BgL_strz00_6953);
							FAILURE(BgL_auxz00_10320, BFALSE, BFALSE);
						}
					BgL_tmpz00_10316 =
						BGl_utf8zd2stringzd2lengthz00zz__unicodez00(BgL_auxz00_10317);
				}
				return BINT(BgL_tmpz00_10316);
			}
		}

	}



/* utf8-string-ref */
	BGL_EXPORTED_DEF obj_t BGl_utf8zd2stringzd2refz00zz__unicodez00(obj_t
		BgL_strz00_87, long BgL_iz00_88)
	{
		{	/* Llib/unicode.scm 1228 */
			{
				long BgL_rz00_5995;
				long BgL_iz00_5996;

				BgL_rz00_5995 = 0L;
				BgL_iz00_5996 = BgL_iz00_88;
			BgL_loopz00_5994:
				{	/* Llib/unicode.scm 1231 */
					unsigned char BgL_cz00_5997;

					BgL_cz00_5997 = STRING_REF(BgL_strz00_87, BgL_rz00_5995);
					{	/* Llib/unicode.scm 1231 */
						long BgL_sz00_6000;

						{	/* Llib/unicode.scm 1232 */
							long BgL_res3317z00_6001;

							{	/* Llib/unicode.scm 1232 */
								unsigned char BgL_cz00_6002;

								BgL_cz00_6002 = (char) (BgL_cz00_5997);
								{	/* Llib/unicode.scm 1203 */
									long BgL_arg2537z00_6003;

									BgL_arg2537z00_6003 =
										(((unsigned char) (BgL_cz00_6002)) >> (int) (4L));
									{	/* Llib/unicode.scm 1202 */
										obj_t BgL_vectorz00_6007;

										BgL_vectorz00_6007 = BGl_vector3673z00zz__unicodez00;
										BgL_res3317z00_6001 =
											(long) CINT(VECTOR_REF(BgL_vectorz00_6007,
												BgL_arg2537z00_6003));
							}}}
							BgL_sz00_6000 = BgL_res3317z00_6001;
						}
						{	/* Llib/unicode.scm 1232 */

							if ((BgL_iz00_5996 == 0L))
								{	/* Llib/unicode.scm 1233 */
									return
										c_substring(BgL_strz00_87, BgL_rz00_5995,
										(BgL_rz00_5995 + BgL_sz00_6000));
								}
							else
								{
									long BgL_iz00_10340;
									long BgL_rz00_10338;

									BgL_rz00_10338 = (BgL_rz00_5995 + BgL_sz00_6000);
									BgL_iz00_10340 = (BgL_iz00_5996 - 1L);
									BgL_iz00_5996 = BgL_iz00_10340;
									BgL_rz00_5995 = BgL_rz00_10338;
									goto BgL_loopz00_5994;
								}
						}
					}
				}
			}
		}

	}



/* &utf8-string-ref */
	obj_t BGl_z62utf8zd2stringzd2refz62zz__unicodez00(obj_t BgL_envz00_6954,
		obj_t BgL_strz00_6955, obj_t BgL_iz00_6956)
	{
		{	/* Llib/unicode.scm 1228 */
			{	/* Llib/unicode.scm 1230 */
				long BgL_auxz00_10349;
				obj_t BgL_auxz00_10342;

				{	/* Llib/unicode.scm 1230 */
					obj_t BgL_tmpz00_10350;

					if (INTEGERP(BgL_iz00_6956))
						{	/* Llib/unicode.scm 1230 */
							BgL_tmpz00_10350 = BgL_iz00_6956;
						}
					else
						{
							obj_t BgL_auxz00_10353;

							BgL_auxz00_10353 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(49561L), BGl_string3678z00zz__unicodez00,
								BGl_string3620z00zz__unicodez00, BgL_iz00_6956);
							FAILURE(BgL_auxz00_10353, BFALSE, BFALSE);
						}
					BgL_auxz00_10349 = (long) CINT(BgL_tmpz00_10350);
				}
				if (STRINGP(BgL_strz00_6955))
					{	/* Llib/unicode.scm 1230 */
						BgL_auxz00_10342 = BgL_strz00_6955;
					}
				else
					{
						obj_t BgL_auxz00_10345;

						BgL_auxz00_10345 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
							BINT(49561L), BGl_string3678z00zz__unicodez00,
							BGl_string3661z00zz__unicodez00, BgL_strz00_6955);
						FAILURE(BgL_auxz00_10345, BFALSE, BFALSE);
					}
				return
					BGl_utf8zd2stringzd2refz00zz__unicodez00(BgL_auxz00_10342,
					BgL_auxz00_10349);
			}
		}

	}



/* utf8-string-index->string-index */
	BGL_EXPORTED_DEF long
		BGl_utf8zd2stringzd2indexzd2ze3stringzd2indexze3zz__unicodez00(obj_t
		BgL_strz00_89, long BgL_iz00_90)
	{
		{	/* Llib/unicode.scm 1240 */
			if ((BgL_iz00_90 < 0L))
				{	/* Llib/unicode.scm 1242 */
					return -1L;
				}
			else
				{	/* Llib/unicode.scm 1245 */
					long BgL_lenz00_2689;

					BgL_lenz00_2689 = STRING_LENGTH(BgL_strz00_89);
					{
						long BgL_rz00_6041;
						long BgL_iz00_6042;

						BgL_rz00_6041 = 0L;
						BgL_iz00_6042 = BgL_iz00_90;
					BgL_loopz00_6040:
						if ((BgL_iz00_6042 == 0L))
							{	/* Llib/unicode.scm 1248 */
								return BgL_rz00_6041;
							}
						else
							{	/* Llib/unicode.scm 1248 */
								if ((BgL_rz00_6041 < BgL_lenz00_2689))
									{	/* Llib/unicode.scm 1252 */
										long BgL_arg2560z00_6051;
										long BgL_arg2561z00_6052;

										{	/* Llib/unicode.scm 1252 */
											long BgL_arg2562z00_6053;

											{	/* Llib/unicode.scm 1252 */
												long BgL_res3318z00_6054;

												{	/* Llib/unicode.scm 1252 */
													unsigned char BgL_cz00_6055;

													BgL_cz00_6055 =
														(char) (STRING_REF(BgL_strz00_89, BgL_rz00_6041));
													{	/* Llib/unicode.scm 1203 */
														long BgL_arg2537z00_6056;

														BgL_arg2537z00_6056 =
															(((unsigned char) (BgL_cz00_6055)) >> (int) (4L));
														{	/* Llib/unicode.scm 1202 */
															obj_t BgL_vectorz00_6060;

															BgL_vectorz00_6060 =
																BGl_vector3673z00zz__unicodez00;
															BgL_res3318z00_6054 =
																(long) CINT(VECTOR_REF(BgL_vectorz00_6060,
																	BgL_arg2537z00_6056));
												}}}
												BgL_arg2562z00_6053 = BgL_res3318z00_6054;
											}
											BgL_arg2560z00_6051 =
												(BgL_rz00_6041 + BgL_arg2562z00_6053);
										}
										BgL_arg2561z00_6052 = (BgL_iz00_6042 - 1L);
										{
											long BgL_iz00_10377;
											long BgL_rz00_10376;

											BgL_rz00_10376 = BgL_arg2560z00_6051;
											BgL_iz00_10377 = BgL_arg2561z00_6052;
											BgL_iz00_6042 = BgL_iz00_10377;
											BgL_rz00_6041 = BgL_rz00_10376;
											goto BgL_loopz00_6040;
										}
									}
								else
									{	/* Llib/unicode.scm 1250 */
										return -1L;
									}
							}
					}
				}
		}

	}



/* &utf8-string-index->string-index */
	obj_t BGl_z62utf8zd2stringzd2indexzd2ze3stringzd2indexz81zz__unicodez00(obj_t
		BgL_envz00_6957, obj_t BgL_strz00_6958, obj_t BgL_iz00_6959)
	{
		{	/* Llib/unicode.scm 1240 */
			{	/* Llib/unicode.scm 1242 */
				long BgL_tmpz00_10378;

				{	/* Llib/unicode.scm 1242 */
					long BgL_auxz00_10386;
					obj_t BgL_auxz00_10379;

					{	/* Llib/unicode.scm 1242 */
						obj_t BgL_tmpz00_10387;

						if (INTEGERP(BgL_iz00_6959))
							{	/* Llib/unicode.scm 1242 */
								BgL_tmpz00_10387 = BgL_iz00_6959;
							}
						else
							{
								obj_t BgL_auxz00_10390;

								BgL_auxz00_10390 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string3618z00zz__unicodez00, BINT(50012L),
									BGl_string3679z00zz__unicodez00,
									BGl_string3620z00zz__unicodez00, BgL_iz00_6959);
								FAILURE(BgL_auxz00_10390, BFALSE, BFALSE);
							}
						BgL_auxz00_10386 = (long) CINT(BgL_tmpz00_10387);
					}
					if (STRINGP(BgL_strz00_6958))
						{	/* Llib/unicode.scm 1242 */
							BgL_auxz00_10379 = BgL_strz00_6958;
						}
					else
						{
							obj_t BgL_auxz00_10382;

							BgL_auxz00_10382 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(50012L), BGl_string3679z00zz__unicodez00,
								BGl_string3661z00zz__unicodez00, BgL_strz00_6958);
							FAILURE(BgL_auxz00_10382, BFALSE, BFALSE);
						}
					BgL_tmpz00_10378 =
						BGl_utf8zd2stringzd2indexzd2ze3stringzd2indexze3zz__unicodez00
						(BgL_auxz00_10379, BgL_auxz00_10386);
				}
				return BINT(BgL_tmpz00_10378);
			}
		}

	}



/* string-index->utf8-string-index */
	BGL_EXPORTED_DEF long
		BGl_stringzd2indexzd2ze3utf8zd2stringzd2indexze3zz__unicodez00(obj_t
		BgL_strz00_91, long BgL_iz00_92)
	{
		{	/* Llib/unicode.scm 1259 */
			if ((BgL_iz00_92 < 0L))
				{	/* Llib/unicode.scm 1261 */
					return -1L;
				}
			else
				{	/* Llib/unicode.scm 1264 */
					long BgL_lenz00_2702;

					BgL_lenz00_2702 = STRING_LENGTH(BgL_strz00_91);
					{
						long BgL_mz00_6088;
						long BgL_jz00_6089;
						long BgL_iz00_6090;

						BgL_mz00_6088 = 0L;
						BgL_jz00_6089 = BgL_iz00_92;
						BgL_iz00_6090 = BgL_iz00_92;
					BgL_loopz00_6087:
						if ((BgL_iz00_6090 <= 0L))
							{	/* Llib/unicode.scm 1269 */
								return BgL_jz00_6089;
							}
						else
							{	/* Llib/unicode.scm 1269 */
								if ((BgL_mz00_6088 >= BgL_lenz00_2702))
									{	/* Llib/unicode.scm 1271 */
										return -1L;
									}
								else
									{	/* Llib/unicode.scm 1274 */
										long BgL_sz00_6099;

										{	/* Llib/unicode.scm 1275 */
											long BgL_res3319z00_6100;

											{	/* Llib/unicode.scm 1275 */
												unsigned char BgL_cz00_6101;

												BgL_cz00_6101 =
													(char) (STRING_REF(BgL_strz00_91, BgL_mz00_6088));
												{	/* Llib/unicode.scm 1203 */
													long BgL_arg2537z00_6102;

													BgL_arg2537z00_6102 =
														(((unsigned char) (BgL_cz00_6101)) >> (int) (4L));
													{	/* Llib/unicode.scm 1202 */
														obj_t BgL_vectorz00_6106;

														BgL_vectorz00_6106 =
															BGl_vector3673z00zz__unicodez00;
														BgL_res3319z00_6100 =
															(long) CINT(VECTOR_REF(BgL_vectorz00_6106,
																BgL_arg2537z00_6102));
											}}}
											BgL_sz00_6099 = BgL_res3319z00_6100;
										}
										{	/* Llib/unicode.scm 1275 */

											{
												long BgL_iz00_10417;
												long BgL_jz00_10414;
												long BgL_mz00_10412;

												BgL_mz00_10412 = (BgL_mz00_6088 + BgL_sz00_6099);
												BgL_jz00_10414 = (BgL_jz00_6089 - (BgL_sz00_6099 - 1L));
												BgL_iz00_10417 = (BgL_iz00_6090 - BgL_sz00_6099);
												BgL_iz00_6090 = BgL_iz00_10417;
												BgL_jz00_6089 = BgL_jz00_10414;
												BgL_mz00_6088 = BgL_mz00_10412;
												goto BgL_loopz00_6087;
											}
										}
									}
							}
					}
				}
		}

	}



/* &string-index->utf8-string-index */
	obj_t BGl_z62stringzd2indexzd2ze3utf8zd2stringzd2indexz81zz__unicodez00(obj_t
		BgL_envz00_6960, obj_t BgL_strz00_6961, obj_t BgL_iz00_6962)
	{
		{	/* Llib/unicode.scm 1259 */
			{	/* Llib/unicode.scm 1261 */
				long BgL_tmpz00_10419;

				{	/* Llib/unicode.scm 1261 */
					long BgL_auxz00_10427;
					obj_t BgL_auxz00_10420;

					{	/* Llib/unicode.scm 1261 */
						obj_t BgL_tmpz00_10428;

						if (INTEGERP(BgL_iz00_6962))
							{	/* Llib/unicode.scm 1261 */
								BgL_tmpz00_10428 = BgL_iz00_6962;
							}
						else
							{
								obj_t BgL_auxz00_10431;

								BgL_auxz00_10431 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string3618z00zz__unicodez00, BINT(50551L),
									BGl_string3680z00zz__unicodez00,
									BGl_string3620z00zz__unicodez00, BgL_iz00_6962);
								FAILURE(BgL_auxz00_10431, BFALSE, BFALSE);
							}
						BgL_auxz00_10427 = (long) CINT(BgL_tmpz00_10428);
					}
					if (STRINGP(BgL_strz00_6961))
						{	/* Llib/unicode.scm 1261 */
							BgL_auxz00_10420 = BgL_strz00_6961;
						}
					else
						{
							obj_t BgL_auxz00_10423;

							BgL_auxz00_10423 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(50551L), BGl_string3680z00zz__unicodez00,
								BGl_string3661z00zz__unicodez00, BgL_strz00_6961);
							FAILURE(BgL_auxz00_10423, BFALSE, BFALSE);
						}
					BgL_tmpz00_10419 =
						BGl_stringzd2indexzd2ze3utf8zd2stringzd2indexze3zz__unicodez00
						(BgL_auxz00_10420, BgL_auxz00_10427);
				}
				return BINT(BgL_tmpz00_10419);
			}
		}

	}



/* utf8-string-left-replacement? */
	BGL_EXPORTED_DEF bool_t
		BGl_utf8zd2stringzd2leftzd2replacementzf3z21zz__unicodez00(obj_t
		BgL_strz00_93, long BgL_lenz00_94, long BgL_indexz00_95)
	{
		{	/* Llib/unicode.scm 1284 */
			if ((BgL_lenz00_94 >= (BgL_indexz00_95 + 4L)))
				{	/* Llib/unicode.scm 1285 */
					return ((STRING_REF(BgL_strz00_93, BgL_indexz00_95)) == 248L);
				}
			else
				{	/* Llib/unicode.scm 1285 */
					return ((bool_t) 0);
				}
		}

	}



/* &utf8-string-left-replacement? */
	obj_t BGl_z62utf8zd2stringzd2leftzd2replacementzf3z43zz__unicodez00(obj_t
		BgL_envz00_6963, obj_t BgL_strz00_6964, obj_t BgL_lenz00_6965,
		obj_t BgL_indexz00_6966)
	{
		{	/* Llib/unicode.scm 1284 */
			{	/* Llib/unicode.scm 1285 */
				bool_t BgL_tmpz00_10444;

				{	/* Llib/unicode.scm 1285 */
					long BgL_auxz00_10461;
					long BgL_auxz00_10452;
					obj_t BgL_auxz00_10445;

					{	/* Llib/unicode.scm 1285 */
						obj_t BgL_tmpz00_10462;

						if (INTEGERP(BgL_indexz00_6966))
							{	/* Llib/unicode.scm 1285 */
								BgL_tmpz00_10462 = BgL_indexz00_6966;
							}
						else
							{
								obj_t BgL_auxz00_10465;

								BgL_auxz00_10465 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string3618z00zz__unicodez00, BINT(51368L),
									BGl_string3681z00zz__unicodez00,
									BGl_string3620z00zz__unicodez00, BgL_indexz00_6966);
								FAILURE(BgL_auxz00_10465, BFALSE, BFALSE);
							}
						BgL_auxz00_10461 = (long) CINT(BgL_tmpz00_10462);
					}
					{	/* Llib/unicode.scm 1285 */
						obj_t BgL_tmpz00_10453;

						if (INTEGERP(BgL_lenz00_6965))
							{	/* Llib/unicode.scm 1285 */
								BgL_tmpz00_10453 = BgL_lenz00_6965;
							}
						else
							{
								obj_t BgL_auxz00_10456;

								BgL_auxz00_10456 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string3618z00zz__unicodez00, BINT(51368L),
									BGl_string3681z00zz__unicodez00,
									BGl_string3620z00zz__unicodez00, BgL_lenz00_6965);
								FAILURE(BgL_auxz00_10456, BFALSE, BFALSE);
							}
						BgL_auxz00_10452 = (long) CINT(BgL_tmpz00_10453);
					}
					if (STRINGP(BgL_strz00_6964))
						{	/* Llib/unicode.scm 1285 */
							BgL_auxz00_10445 = BgL_strz00_6964;
						}
					else
						{
							obj_t BgL_auxz00_10448;

							BgL_auxz00_10448 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(51368L), BGl_string3681z00zz__unicodez00,
								BGl_string3661z00zz__unicodez00, BgL_strz00_6964);
							FAILURE(BgL_auxz00_10448, BFALSE, BFALSE);
						}
					BgL_tmpz00_10444 =
						BGl_utf8zd2stringzd2leftzd2replacementzf3z21zz__unicodez00
						(BgL_auxz00_10445, BgL_auxz00_10452, BgL_auxz00_10461);
				}
				return BBOOL(BgL_tmpz00_10444);
			}
		}

	}



/* utf8-string-right-replacement? */
	BGL_EXPORTED_DEF bool_t
		BGl_utf8zd2stringzd2rightzd2replacementzf3z21zz__unicodez00(obj_t
		BgL_strz00_96, long BgL_lenz00_97, long BgL_indexz00_98)
	{
		{	/* Llib/unicode.scm 1294 */
			if ((BgL_lenz00_97 >= (BgL_indexz00_98 + 4L)))
				{	/* Llib/unicode.scm 1295 */
					return ((STRING_REF(BgL_strz00_96, BgL_indexz00_98)) == 252L);
				}
			else
				{	/* Llib/unicode.scm 1295 */
					return ((bool_t) 0);
				}
		}

	}



/* &utf8-string-right-replacement? */
	obj_t BGl_z62utf8zd2stringzd2rightzd2replacementzf3z43zz__unicodez00(obj_t
		BgL_envz00_6967, obj_t BgL_strz00_6968, obj_t BgL_lenz00_6969,
		obj_t BgL_indexz00_6970)
	{
		{	/* Llib/unicode.scm 1294 */
			{	/* Llib/unicode.scm 1295 */
				bool_t BgL_tmpz00_10478;

				{	/* Llib/unicode.scm 1295 */
					long BgL_auxz00_10495;
					long BgL_auxz00_10486;
					obj_t BgL_auxz00_10479;

					{	/* Llib/unicode.scm 1295 */
						obj_t BgL_tmpz00_10496;

						if (INTEGERP(BgL_indexz00_6970))
							{	/* Llib/unicode.scm 1295 */
								BgL_tmpz00_10496 = BgL_indexz00_6970;
							}
						else
							{
								obj_t BgL_auxz00_10499;

								BgL_auxz00_10499 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string3618z00zz__unicodez00, BINT(51964L),
									BGl_string3682z00zz__unicodez00,
									BGl_string3620z00zz__unicodez00, BgL_indexz00_6970);
								FAILURE(BgL_auxz00_10499, BFALSE, BFALSE);
							}
						BgL_auxz00_10495 = (long) CINT(BgL_tmpz00_10496);
					}
					{	/* Llib/unicode.scm 1295 */
						obj_t BgL_tmpz00_10487;

						if (INTEGERP(BgL_lenz00_6969))
							{	/* Llib/unicode.scm 1295 */
								BgL_tmpz00_10487 = BgL_lenz00_6969;
							}
						else
							{
								obj_t BgL_auxz00_10490;

								BgL_auxz00_10490 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string3618z00zz__unicodez00, BINT(51964L),
									BGl_string3682z00zz__unicodez00,
									BGl_string3620z00zz__unicodez00, BgL_lenz00_6969);
								FAILURE(BgL_auxz00_10490, BFALSE, BFALSE);
							}
						BgL_auxz00_10486 = (long) CINT(BgL_tmpz00_10487);
					}
					if (STRINGP(BgL_strz00_6968))
						{	/* Llib/unicode.scm 1295 */
							BgL_auxz00_10479 = BgL_strz00_6968;
						}
					else
						{
							obj_t BgL_auxz00_10482;

							BgL_auxz00_10482 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
								BINT(51964L), BGl_string3682z00zz__unicodez00,
								BGl_string3661z00zz__unicodez00, BgL_strz00_6968);
							FAILURE(BgL_auxz00_10482, BFALSE, BFALSE);
						}
					BgL_tmpz00_10478 =
						BGl_utf8zd2stringzd2rightzd2replacementzf3z21zz__unicodez00
						(BgL_auxz00_10479, BgL_auxz00_10486, BgL_auxz00_10495);
				}
				return BBOOL(BgL_tmpz00_10478);
			}
		}

	}



/* utf8-collapse! */
	obj_t BGl_utf8zd2collapsez12zc0zz__unicodez00(obj_t BgL_bufferz00_99,
		long BgL_indexbz00_100, obj_t BgL_strz00_101, obj_t BgL_indexsz00_102)
	{
		{	/* Llib/unicode.scm 1301 */
			{	/* Llib/unicode.scm 1303 */
				long BgL_cl1z00_2725;

				BgL_cl1z00_2725 =
					(STRING_REF(BgL_bufferz00_99, (BgL_indexbz00_100 - 4L)));
				{	/* Llib/unicode.scm 1303 */
					long BgL_cl2z00_2726;

					BgL_cl2z00_2726 =
						(STRING_REF(BgL_bufferz00_99, (BgL_indexbz00_100 - 3L)));
					{	/* Llib/unicode.scm 1304 */
						long BgL_cl3z00_2727;

						BgL_cl3z00_2727 =
							(STRING_REF(BgL_bufferz00_99, (BgL_indexbz00_100 - 2L)));
						{	/* Llib/unicode.scm 1305 */
							long BgL_cl4z00_2728;

							BgL_cl4z00_2728 =
								(STRING_REF(BgL_bufferz00_99, (BgL_indexbz00_100 - 1L)));
							{	/* Llib/unicode.scm 1307 */
								long BgL_cr3z00_2730;

								BgL_cr3z00_2730 =
									(STRING_REF(BgL_strz00_101,
										((long) CINT(BgL_indexsz00_102) + 2L)));
								{	/* Llib/unicode.scm 1308 */
									long BgL_cr4z00_2731;

									BgL_cr4z00_2731 =
										(STRING_REF(BgL_strz00_101,
											((long) CINT(BgL_indexsz00_102) + 3L)));
									{	/* Llib/unicode.scm 1310 */
										long BgL_yyyyz00_2733;

										BgL_yyyyz00_2733 = (15L & BgL_cr3z00_2730);
										{	/* Llib/unicode.scm 1311 */
											long BgL_xxz00_2734;

											BgL_xxz00_2734 = ((BgL_cl3z00_2727 >> (int) (4L)) & 3L);
											{	/* Llib/unicode.scm 1313 */
												long BgL_uuuuuz00_2736;

												BgL_uuuuuz00_2736 =
													(
													((BgL_cl4z00_2728 & 7L) <<
														(int) (2L)) |
													((BgL_cl2z00_2726 >> (int) (4L)) & 3L));
												{	/* Llib/unicode.scm 1314 */

													{	/* Llib/unicode.scm 1318 */
														unsigned char BgL_auxz00_10539;
														long BgL_tmpz00_10537;

														BgL_auxz00_10539 =
															(
															((BgL_cl1z00_2725 & 240L) |
																(BgL_uuuuuz00_2736 >> (int) (2L))));
														BgL_tmpz00_10537 = (BgL_indexbz00_100 - 4L);
														STRING_SET(BgL_bufferz00_99, BgL_tmpz00_10537,
															BgL_auxz00_10539);
													}
													{	/* Llib/unicode.scm 1324 */
														unsigned char BgL_auxz00_10548;
														long BgL_tmpz00_10546;

														BgL_auxz00_10548 = (BgL_cl2z00_2726);
														BgL_tmpz00_10546 = (BgL_indexbz00_100 - 3L);
														STRING_SET(BgL_bufferz00_99, BgL_tmpz00_10546,
															BgL_auxz00_10548);
													}
													{	/* Llib/unicode.scm 1327 */
														unsigned char BgL_auxz00_10553;
														long BgL_tmpz00_10551;

														BgL_auxz00_10553 =
															(
															(128L |
																((BgL_xxz00_2734 <<
																		(int) (4L)) | BgL_yyyyz00_2733)));
														BgL_tmpz00_10551 = (BgL_indexbz00_100 - 2L);
														STRING_SET(BgL_bufferz00_99, BgL_tmpz00_10551,
															BgL_auxz00_10553);
													}
													{	/* Llib/unicode.scm 1332 */
														unsigned char BgL_auxz00_10562;
														long BgL_tmpz00_10560;

														BgL_auxz00_10562 = (BgL_cr4z00_2731);
														BgL_tmpz00_10560 = (BgL_indexbz00_100 - 1L);
														return
															STRING_SET(BgL_bufferz00_99, BgL_tmpz00_10560,
															BgL_auxz00_10562);
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* _utf8-string-append-fill! */
	obj_t BGl__utf8zd2stringzd2appendzd2fillz12zc0zz__unicodez00(obj_t
		BgL_env1142z00_108, obj_t BgL_opt1141z00_107)
	{
		{	/* Llib/unicode.scm 1344 */
			{	/* Llib/unicode.scm 1344 */
				obj_t BgL_g1143z00_2770;
				obj_t BgL_g1144z00_2771;
				obj_t BgL_g1145z00_2772;

				BgL_g1143z00_2770 = VECTOR_REF(BgL_opt1141z00_107, 0L);
				BgL_g1144z00_2771 = VECTOR_REF(BgL_opt1141z00_107, 1L);
				BgL_g1145z00_2772 = VECTOR_REF(BgL_opt1141z00_107, 2L);
				switch (VECTOR_LENGTH(BgL_opt1141z00_107))
					{
					case 3L:

						{	/* Llib/unicode.scm 1344 */

							{	/* Llib/unicode.scm 1344 */
								long BgL_tmpz00_10568;

								{	/* Llib/unicode.scm 1344 */
									obj_t BgL_auxz00_10585;
									long BgL_auxz00_10576;
									obj_t BgL_auxz00_10569;

									if (STRINGP(BgL_g1145z00_2772))
										{	/* Llib/unicode.scm 1344 */
											BgL_auxz00_10585 = BgL_g1145z00_2772;
										}
									else
										{
											obj_t BgL_auxz00_10588;

											BgL_auxz00_10588 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3618z00zz__unicodez00, BINT(54131L),
												BGl_string3683z00zz__unicodez00,
												BGl_string3661z00zz__unicodez00, BgL_g1145z00_2772);
											FAILURE(BgL_auxz00_10588, BFALSE, BFALSE);
										}
									{	/* Llib/unicode.scm 1344 */
										obj_t BgL_tmpz00_10577;

										if (INTEGERP(BgL_g1144z00_2771))
											{	/* Llib/unicode.scm 1344 */
												BgL_tmpz00_10577 = BgL_g1144z00_2771;
											}
										else
											{
												obj_t BgL_auxz00_10580;

												BgL_auxz00_10580 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3618z00zz__unicodez00, BINT(54131L),
													BGl_string3683z00zz__unicodez00,
													BGl_string3620z00zz__unicodez00, BgL_g1144z00_2771);
												FAILURE(BgL_auxz00_10580, BFALSE, BFALSE);
											}
										BgL_auxz00_10576 = (long) CINT(BgL_tmpz00_10577);
									}
									if (STRINGP(BgL_g1143z00_2770))
										{	/* Llib/unicode.scm 1344 */
											BgL_auxz00_10569 = BgL_g1143z00_2770;
										}
									else
										{
											obj_t BgL_auxz00_10572;

											BgL_auxz00_10572 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3618z00zz__unicodez00, BINT(54131L),
												BGl_string3683z00zz__unicodez00,
												BGl_string3661z00zz__unicodez00, BgL_g1143z00_2770);
											FAILURE(BgL_auxz00_10572, BFALSE, BFALSE);
										}
									BgL_tmpz00_10568 =
										BGl_utf8zd2stringzd2appendzd2fillz12zc0zz__unicodez00
										(BgL_auxz00_10569, BgL_auxz00_10576, BgL_auxz00_10585,
										BINT(0L));
								}
								return BINT(BgL_tmpz00_10568);
							}
						}
						break;
					case 4L:

						{	/* Llib/unicode.scm 1344 */
							obj_t BgL_offsetz00_2776;

							BgL_offsetz00_2776 = VECTOR_REF(BgL_opt1141z00_107, 3L);
							{	/* Llib/unicode.scm 1344 */

								{	/* Llib/unicode.scm 1344 */
									long BgL_tmpz00_10596;

									{	/* Llib/unicode.scm 1344 */
										obj_t BgL_auxz00_10613;
										long BgL_auxz00_10604;
										obj_t BgL_auxz00_10597;

										if (STRINGP(BgL_g1145z00_2772))
											{	/* Llib/unicode.scm 1344 */
												BgL_auxz00_10613 = BgL_g1145z00_2772;
											}
										else
											{
												obj_t BgL_auxz00_10616;

												BgL_auxz00_10616 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3618z00zz__unicodez00, BINT(54131L),
													BGl_string3683z00zz__unicodez00,
													BGl_string3661z00zz__unicodez00, BgL_g1145z00_2772);
												FAILURE(BgL_auxz00_10616, BFALSE, BFALSE);
											}
										{	/* Llib/unicode.scm 1344 */
											obj_t BgL_tmpz00_10605;

											if (INTEGERP(BgL_g1144z00_2771))
												{	/* Llib/unicode.scm 1344 */
													BgL_tmpz00_10605 = BgL_g1144z00_2771;
												}
											else
												{
													obj_t BgL_auxz00_10608;

													BgL_auxz00_10608 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string3618z00zz__unicodez00, BINT(54131L),
														BGl_string3683z00zz__unicodez00,
														BGl_string3620z00zz__unicodez00, BgL_g1144z00_2771);
													FAILURE(BgL_auxz00_10608, BFALSE, BFALSE);
												}
											BgL_auxz00_10604 = (long) CINT(BgL_tmpz00_10605);
										}
										if (STRINGP(BgL_g1143z00_2770))
											{	/* Llib/unicode.scm 1344 */
												BgL_auxz00_10597 = BgL_g1143z00_2770;
											}
										else
											{
												obj_t BgL_auxz00_10600;

												BgL_auxz00_10600 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3618z00zz__unicodez00, BINT(54131L),
													BGl_string3683z00zz__unicodez00,
													BGl_string3661z00zz__unicodez00, BgL_g1143z00_2770);
												FAILURE(BgL_auxz00_10600, BFALSE, BFALSE);
											}
										BgL_tmpz00_10596 =
											BGl_utf8zd2stringzd2appendzd2fillz12zc0zz__unicodez00
											(BgL_auxz00_10597, BgL_auxz00_10604, BgL_auxz00_10613,
											BgL_offsetz00_2776);
									}
									return BINT(BgL_tmpz00_10596);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* utf8-string-append-fill! */
	BGL_EXPORTED_DEF long
		BGl_utf8zd2stringzd2appendzd2fillz12zc0zz__unicodez00(obj_t
		BgL_bufferz00_103, long BgL_indexz00_104, obj_t BgL_strz00_105,
		obj_t BgL_offsetz00_106)
	{
		{	/* Llib/unicode.scm 1344 */
			{	/* Llib/unicode.scm 1345 */
				long BgL_lenz00_2777;

				BgL_lenz00_2777 = STRING_LENGTH(BgL_strz00_105);
				{	/* Llib/unicode.scm 1347 */
					bool_t BgL_test4295z00_10625;

					if ((BgL_indexz00_104 >= 4L))
						{	/* Llib/unicode.scm 1348 */
							bool_t BgL_test4297z00_10628;

							{	/* Llib/unicode.scm 1348 */
								long BgL_indexz00_6212;

								BgL_indexz00_6212 = (long) CINT(BgL_offsetz00_106);
								if ((BgL_lenz00_2777 >= (BgL_indexz00_6212 + 4L)))
									{	/* Llib/unicode.scm 1295 */
										BgL_test4297z00_10628 =
											((STRING_REF(BgL_strz00_105, BgL_indexz00_6212)) == 252L);
									}
								else
									{	/* Llib/unicode.scm 1295 */
										BgL_test4297z00_10628 = ((bool_t) 0);
									}
							}
							if (BgL_test4297z00_10628)
								{	/* Llib/unicode.scm 1349 */
									long BgL_indexz00_6227;

									BgL_indexz00_6227 = (BgL_indexz00_104 - 4L);
									if ((BgL_indexz00_104 >= (BgL_indexz00_6227 + 4L)))
										{	/* Llib/unicode.scm 1285 */
											BgL_test4295z00_10625 =
												(
												(STRING_REF(BgL_bufferz00_103,
														BgL_indexz00_6227)) == 248L);
										}
									else
										{	/* Llib/unicode.scm 1285 */
											BgL_test4295z00_10625 = ((bool_t) 0);
										}
								}
							else
								{	/* Llib/unicode.scm 1348 */
									BgL_test4295z00_10625 = ((bool_t) 0);
								}
						}
					else
						{	/* Llib/unicode.scm 1347 */
							BgL_test4295z00_10625 = ((bool_t) 0);
						}
					if (BgL_test4295z00_10625)
						{	/* Llib/unicode.scm 1347 */
							blit_string(BgL_strz00_105, 4L, BgL_bufferz00_103,
								BgL_indexz00_104, (BgL_lenz00_2777 - 4L));
							BGl_utf8zd2collapsez12zc0zz__unicodez00(BgL_bufferz00_103,
								BgL_indexz00_104, BgL_strz00_105, BgL_offsetz00_106);
							return (BgL_indexz00_104 + (BgL_lenz00_2777 - 4L));
						}
					else
						{	/* Llib/unicode.scm 1347 */
							blit_string(BgL_strz00_105,
								(long) CINT(BgL_offsetz00_106), BgL_bufferz00_103,
								BgL_indexz00_104,
								(BgL_lenz00_2777 - (long) CINT(BgL_offsetz00_106)));
							return (BgL_indexz00_104 + (BgL_lenz00_2777 -
									(long) CINT(BgL_offsetz00_106)));
		}}}}

	}



/* utf8-string-append */
	BGL_EXPORTED_DEF obj_t BGl_utf8zd2stringzd2appendz00zz__unicodez00(obj_t
		BgL_leftz00_109, obj_t BgL_rightz00_110)
	{
		{	/* Llib/unicode.scm 1366 */
			{	/* Llib/unicode.scm 1367 */
				long BgL_llenz00_2789;

				BgL_llenz00_2789 = STRING_LENGTH(BgL_leftz00_109);
				{	/* Llib/unicode.scm 1367 */
					long BgL_rlenz00_2790;

					BgL_rlenz00_2790 = STRING_LENGTH(BgL_rightz00_110);
					{	/* Llib/unicode.scm 1368 */
						obj_t BgL_bufferz00_2791;

						BgL_bufferz00_2791 =
							make_string_sans_fill((BgL_llenz00_2789 + BgL_rlenz00_2790));
						{	/* Llib/unicode.scm 1369 */

							blit_string(BgL_leftz00_109, 0L, BgL_bufferz00_2791, 0L,
								BgL_llenz00_2789);
							{	/* Llib/unicode.scm 1371 */
								long BgL_nindexz00_2792;

								{	/* Llib/unicode.scm 181 */

									BgL_nindexz00_2792 =
										BGl_utf8zd2stringzd2appendzd2fillz12zc0zz__unicodez00
										(BgL_bufferz00_2791, BgL_llenz00_2789, BgL_rightz00_110,
										BINT(0L));
								}
								return
									bgl_string_shrink(BgL_bufferz00_2791, BgL_nindexz00_2792);
							}
						}
					}
				}
			}
		}

	}



/* &utf8-string-append */
	obj_t BGl_z62utf8zd2stringzd2appendz62zz__unicodez00(obj_t BgL_envz00_6971,
		obj_t BgL_leftz00_6972, obj_t BgL_rightz00_6973)
	{
		{	/* Llib/unicode.scm 1366 */
			{	/* Llib/unicode.scm 1367 */
				obj_t BgL_auxz00_10670;
				obj_t BgL_auxz00_10663;

				if (STRINGP(BgL_rightz00_6973))
					{	/* Llib/unicode.scm 1367 */
						BgL_auxz00_10670 = BgL_rightz00_6973;
					}
				else
					{
						obj_t BgL_auxz00_10673;

						BgL_auxz00_10673 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
							BINT(55284L), BGl_string3684z00zz__unicodez00,
							BGl_string3661z00zz__unicodez00, BgL_rightz00_6973);
						FAILURE(BgL_auxz00_10673, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_leftz00_6972))
					{	/* Llib/unicode.scm 1367 */
						BgL_auxz00_10663 = BgL_leftz00_6972;
					}
				else
					{
						obj_t BgL_auxz00_10666;

						BgL_auxz00_10666 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
							BINT(55284L), BGl_string3684z00zz__unicodez00,
							BGl_string3661z00zz__unicodez00, BgL_leftz00_6972);
						FAILURE(BgL_auxz00_10666, BFALSE, BFALSE);
					}
				return
					BGl_utf8zd2stringzd2appendz00zz__unicodez00(BgL_auxz00_10663,
					BgL_auxz00_10670);
			}
		}

	}



/* utf8-string-append* */
	BGL_EXPORTED_DEF obj_t BGl_utf8zd2stringzd2appendza2za2zz__unicodez00(obj_t
		BgL_stringsz00_111)
	{
		{	/* Llib/unicode.scm 1379 */
			{	/* Llib/unicode.scm 1380 */
				long BgL_lenz00_2798;

				BgL_lenz00_2798 = 0L;
				{
					obj_t BgL_l1105z00_2800;

					BgL_l1105z00_2800 = BgL_stringsz00_111;
				BgL_zc3z04anonymousza32641ze3z87_2801:
					if (PAIRP(BgL_l1105z00_2800))
						{	/* Llib/unicode.scm 1381 */
							{	/* Llib/unicode.scm 1382 */
								obj_t BgL_strz00_2803;

								BgL_strz00_2803 = CAR(BgL_l1105z00_2800);
								{	/* Llib/unicode.scm 1382 */
									long BgL_za71za7_6269;

									BgL_za71za7_6269 = BgL_lenz00_2798;
									BgL_lenz00_2798 =
										(BgL_za71za7_6269 +
										STRING_LENGTH(((obj_t) BgL_strz00_2803)));
							}}
							{
								obj_t BgL_l1105z00_10684;

								BgL_l1105z00_10684 = CDR(BgL_l1105z00_2800);
								BgL_l1105z00_2800 = BgL_l1105z00_10684;
								goto BgL_zc3z04anonymousza32641ze3z87_2801;
							}
						}
					else
						{	/* Llib/unicode.scm 1381 */
							((bool_t) 1);
						}
				}
				{	/* Llib/unicode.scm 1384 */
					obj_t BgL_bufferz00_2807;
					long BgL_indexz00_2808;

					BgL_bufferz00_2807 = make_string_sans_fill(BgL_lenz00_2798);
					BgL_indexz00_2808 = 0L;
					{
						obj_t BgL_l1107z00_2810;

						BgL_l1107z00_2810 = BgL_stringsz00_111;
					BgL_zc3z04anonymousza32645ze3z87_2811:
						if (PAIRP(BgL_l1107z00_2810))
							{	/* Llib/unicode.scm 1386 */
								{	/* Llib/unicode.scm 1387 */
									obj_t BgL_strz00_2813;

									BgL_strz00_2813 = CAR(BgL_l1107z00_2810);
									{	/* Llib/unicode.scm 1387 */
										long BgL_g1144z00_2815;

										BgL_g1144z00_2815 = BgL_indexz00_2808;
										{	/* Llib/unicode.scm 181 */

											BgL_indexz00_2808 =
												BGl_utf8zd2stringzd2appendzd2fillz12zc0zz__unicodez00
												(BgL_bufferz00_2807, BgL_g1144z00_2815, BgL_strz00_2813,
												BINT(0L));
								}}}
								{
									obj_t BgL_l1107z00_10692;

									BgL_l1107z00_10692 = CDR(BgL_l1107z00_2810);
									BgL_l1107z00_2810 = BgL_l1107z00_10692;
									goto BgL_zc3z04anonymousza32645ze3z87_2811;
								}
							}
						else
							{	/* Llib/unicode.scm 1386 */
								((bool_t) 1);
							}
					}
					return bgl_string_shrink(BgL_bufferz00_2807, BgL_indexz00_2808);
				}
			}
		}

	}



/* &utf8-string-append* */
	obj_t BGl_z62utf8zd2stringzd2appendza2zc0zz__unicodez00(obj_t BgL_envz00_6974,
		obj_t BgL_stringsz00_6975)
	{
		{	/* Llib/unicode.scm 1379 */
			return
				BGl_utf8zd2stringzd2appendza2za2zz__unicodez00(BgL_stringsz00_6975);
		}

	}



/* _utf8-substring */
	obj_t BGl__utf8zd2substringzd2zz__unicodez00(obj_t BgL_env1149z00_116,
		obj_t BgL_opt1148z00_115)
	{
		{	/* Llib/unicode.scm 1394 */
			{	/* Llib/unicode.scm 1394 */
				obj_t BgL_strz00_2820;
				obj_t BgL_g1150z00_2821;

				BgL_strz00_2820 = VECTOR_REF(BgL_opt1148z00_115, 0L);
				BgL_g1150z00_2821 = VECTOR_REF(BgL_opt1148z00_115, 1L);
				switch (VECTOR_LENGTH(BgL_opt1148z00_115))
					{
					case 2L:

						{	/* Llib/unicode.scm 1395 */
							long BgL_endz00_2824;

							{	/* Llib/unicode.scm 1395 */
								obj_t BgL_auxz00_10698;

								if (STRINGP(BgL_strz00_2820))
									{	/* Llib/unicode.scm 1395 */
										BgL_auxz00_10698 = BgL_strz00_2820;
									}
								else
									{
										obj_t BgL_auxz00_10701;

										BgL_auxz00_10701 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string3618z00zz__unicodez00, BINT(56588L),
											BGl_string3685z00zz__unicodez00,
											BGl_string3661z00zz__unicodez00, BgL_strz00_2820);
										FAILURE(BgL_auxz00_10701, BFALSE, BFALSE);
									}
								BgL_endz00_2824 =
									BGl_utf8zd2stringzd2lengthz00zz__unicodez00(BgL_auxz00_10698);
							}
							{	/* Llib/unicode.scm 1394 */

								{	/* Llib/unicode.scm 1394 */
									long BgL_auxz00_10713;
									obj_t BgL_auxz00_10706;

									{	/* Llib/unicode.scm 1394 */
										obj_t BgL_tmpz00_10714;

										if (INTEGERP(BgL_g1150z00_2821))
											{	/* Llib/unicode.scm 1394 */
												BgL_tmpz00_10714 = BgL_g1150z00_2821;
											}
										else
											{
												obj_t BgL_auxz00_10717;

												BgL_auxz00_10717 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3618z00zz__unicodez00, BINT(56493L),
													BGl_string3685z00zz__unicodez00,
													BGl_string3620z00zz__unicodez00, BgL_g1150z00_2821);
												FAILURE(BgL_auxz00_10717, BFALSE, BFALSE);
											}
										BgL_auxz00_10713 = (long) CINT(BgL_tmpz00_10714);
									}
									if (STRINGP(BgL_strz00_2820))
										{	/* Llib/unicode.scm 1394 */
											BgL_auxz00_10706 = BgL_strz00_2820;
										}
									else
										{
											obj_t BgL_auxz00_10709;

											BgL_auxz00_10709 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3618z00zz__unicodez00, BINT(56493L),
												BGl_string3685z00zz__unicodez00,
												BGl_string3661z00zz__unicodez00, BgL_strz00_2820);
											FAILURE(BgL_auxz00_10709, BFALSE, BFALSE);
										}
									return
										BGl_utf8zd2substringzd2zz__unicodez00(BgL_auxz00_10706,
										BgL_auxz00_10713, BgL_endz00_2824);
								}
							}
						}
						break;
					case 3L:

						{	/* Llib/unicode.scm 1394 */
							obj_t BgL_endz00_2825;

							BgL_endz00_2825 = VECTOR_REF(BgL_opt1148z00_115, 2L);
							{	/* Llib/unicode.scm 1394 */

								{	/* Llib/unicode.scm 1394 */
									long BgL_auxz00_10740;
									long BgL_auxz00_10731;
									obj_t BgL_auxz00_10724;

									{	/* Llib/unicode.scm 1394 */
										obj_t BgL_tmpz00_10741;

										if (INTEGERP(BgL_endz00_2825))
											{	/* Llib/unicode.scm 1394 */
												BgL_tmpz00_10741 = BgL_endz00_2825;
											}
										else
											{
												obj_t BgL_auxz00_10744;

												BgL_auxz00_10744 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3618z00zz__unicodez00, BINT(56493L),
													BGl_string3685z00zz__unicodez00,
													BGl_string3620z00zz__unicodez00, BgL_endz00_2825);
												FAILURE(BgL_auxz00_10744, BFALSE, BFALSE);
											}
										BgL_auxz00_10740 = (long) CINT(BgL_tmpz00_10741);
									}
									{	/* Llib/unicode.scm 1394 */
										obj_t BgL_tmpz00_10732;

										if (INTEGERP(BgL_g1150z00_2821))
											{	/* Llib/unicode.scm 1394 */
												BgL_tmpz00_10732 = BgL_g1150z00_2821;
											}
										else
											{
												obj_t BgL_auxz00_10735;

												BgL_auxz00_10735 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3618z00zz__unicodez00, BINT(56493L),
													BGl_string3685z00zz__unicodez00,
													BGl_string3620z00zz__unicodez00, BgL_g1150z00_2821);
												FAILURE(BgL_auxz00_10735, BFALSE, BFALSE);
											}
										BgL_auxz00_10731 = (long) CINT(BgL_tmpz00_10732);
									}
									if (STRINGP(BgL_strz00_2820))
										{	/* Llib/unicode.scm 1394 */
											BgL_auxz00_10724 = BgL_strz00_2820;
										}
									else
										{
											obj_t BgL_auxz00_10727;

											BgL_auxz00_10727 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3618z00zz__unicodez00, BINT(56493L),
												BGl_string3685z00zz__unicodez00,
												BGl_string3661z00zz__unicodez00, BgL_strz00_2820);
											FAILURE(BgL_auxz00_10727, BFALSE, BFALSE);
										}
									return
										BGl_utf8zd2substringzd2zz__unicodez00(BgL_auxz00_10724,
										BgL_auxz00_10731, BgL_auxz00_10740);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* utf8-substring */
	BGL_EXPORTED_DEF obj_t BGl_utf8zd2substringzd2zz__unicodez00(obj_t
		BgL_strz00_112, long BgL_startz00_113, long BgL_endz00_114)
	{
		{	/* Llib/unicode.scm 1394 */
			{	/* Llib/unicode.scm 1396 */
				long BgL_lenz00_2826;

				BgL_lenz00_2826 = STRING_LENGTH(BgL_strz00_112);
				{	/* Llib/unicode.scm 1398 */
					bool_t BgL_test4310z00_10753;

					if ((BgL_startz00_113 < 0L))
						{	/* Llib/unicode.scm 1398 */
							BgL_test4310z00_10753 = ((bool_t) 1);
						}
					else
						{	/* Llib/unicode.scm 1398 */
							BgL_test4310z00_10753 = (BgL_startz00_113 > BgL_lenz00_2826);
						}
					if (BgL_test4310z00_10753)
						{	/* Llib/unicode.scm 1398 */
							return
								BGl_errorz00zz__errorz00(BGl_string3686z00zz__unicodez00,
								string_append_3(BGl_string3687z00zz__unicodez00, BgL_strz00_112,
									BGl_string3688z00zz__unicodez00), BINT(BgL_startz00_113));
						}
					else
						{	/* Llib/unicode.scm 1398 */
							if ((BgL_endz00_114 < 0L))
								{	/* Llib/unicode.scm 1402 */
									return
										BGl_errorz00zz__errorz00(BGl_string3686z00zz__unicodez00,
										string_append_3(BGl_string3689z00zz__unicodez00,
											BgL_strz00_112, BGl_string3688z00zz__unicodez00),
										BINT(BgL_endz00_114));
								}
							else
								{	/* Llib/unicode.scm 1406 */
									bool_t BgL_test4313z00_10765;

									if ((BgL_endz00_114 < BgL_startz00_113))
										{	/* Llib/unicode.scm 1406 */
											BgL_test4313z00_10765 = ((bool_t) 1);
										}
									else
										{	/* Llib/unicode.scm 1406 */
											BgL_test4313z00_10765 =
												(BgL_endz00_114 > BgL_lenz00_2826);
										}
									if (BgL_test4313z00_10765)
										{	/* Llib/unicode.scm 1406 */
											return
												BGl_errorz00zz__errorz00
												(BGl_string3686z00zz__unicodez00,
												string_append_3(BGl_string3689z00zz__unicodez00,
													BgL_strz00_112, BGl_string3688z00zz__unicodez00),
												BINT(BgL_endz00_114));
										}
									else
										{	/* Llib/unicode.scm 1406 */
											if ((BgL_startz00_113 == BgL_endz00_114))
												{	/* Llib/unicode.scm 1410 */
													return BGl_string3591z00zz__unicodez00;
												}
											else
												{
													long BgL_rz00_6316;
													long BgL_nz00_6317;
													long BgL_iz00_6318;

													BgL_rz00_6316 = 0L;
													BgL_nz00_6317 = 0L;
													BgL_iz00_6318 = 0L;
												BgL_loopz00_6315:
													if ((BgL_rz00_6316 == BgL_lenz00_2826))
														{	/* Llib/unicode.scm 1416 */
															BGL_TAIL return
																c_substring(BgL_strz00_112, BgL_iz00_6318,
																BgL_rz00_6316);
														}
													else
														{	/* Llib/unicode.scm 1418 */
															unsigned char BgL_cz00_6325;

															BgL_cz00_6325 =
																STRING_REF(BgL_strz00_112, BgL_rz00_6316);
															{	/* Llib/unicode.scm 1418 */
																long BgL_sz00_6328;

																{	/* Llib/unicode.scm 1419 */
																	long BgL_res3320z00_6329;

																	{	/* Llib/unicode.scm 1419 */
																		unsigned char BgL_cz00_6330;

																		BgL_cz00_6330 = (char) (BgL_cz00_6325);
																		{	/* Llib/unicode.scm 1203 */
																			long BgL_arg2537z00_6331;

																			BgL_arg2537z00_6331 =
																				(
																				((unsigned char) (BgL_cz00_6330)) >>
																				(int) (4L));
																			{	/* Llib/unicode.scm 1202 */
																				obj_t BgL_vectorz00_6335;

																				BgL_vectorz00_6335 =
																					BGl_vector3673z00zz__unicodez00;
																				BgL_res3320z00_6329 =
																					(long)
																					CINT(VECTOR_REF(BgL_vectorz00_6335,
																						BgL_arg2537z00_6331));
																	}}}
																	BgL_sz00_6328 = BgL_res3320z00_6329;
																}
																{	/* Llib/unicode.scm 1419 */

																	if ((BgL_nz00_6317 == BgL_startz00_113))
																		{
																			long BgL_iz00_10791;
																			long BgL_nz00_10789;
																			long BgL_rz00_10787;

																			BgL_rz00_10787 =
																				(BgL_rz00_6316 + BgL_sz00_6328);
																			BgL_nz00_10789 = (BgL_nz00_6317 + 1L);
																			BgL_iz00_10791 = BgL_rz00_6316;
																			BgL_iz00_6318 = BgL_iz00_10791;
																			BgL_nz00_6317 = BgL_nz00_10789;
																			BgL_rz00_6316 = BgL_rz00_10787;
																			goto BgL_loopz00_6315;
																		}
																	else
																		{	/* Llib/unicode.scm 1421 */
																			if ((BgL_nz00_6317 == BgL_endz00_114))
																				{	/* Llib/unicode.scm 1423 */
																					BGL_TAIL return
																						c_substring(BgL_strz00_112,
																						BgL_iz00_6318, BgL_rz00_6316);
																				}
																			else
																				{
																					long BgL_nz00_10797;
																					long BgL_rz00_10795;

																					BgL_rz00_10795 =
																						(BgL_rz00_6316 + BgL_sz00_6328);
																					BgL_nz00_10797 = (BgL_nz00_6317 + 1L);
																					BgL_nz00_6317 = BgL_nz00_10797;
																					BgL_rz00_6316 = BgL_rz00_10795;
																					goto BgL_loopz00_6315;
																				}
																		}
																}
															}
														}
												}
										}
								}
						}
				}
			}
		}

	}



/* utf8->8bits-fill! */
	obj_t BGl_utf8zd2ze38bitszd2fillz12zf1zz__unicodez00(obj_t BgL_nstrz00_117,
		obj_t BgL_strz00_118, int BgL_lenz00_119, obj_t BgL_tablez00_120)
	{
		{	/* Llib/unicode.scm 1431 */
			{
				obj_t BgL_rz00_2900;

				{
					obj_t BgL_rz00_2858;
					long BgL_wz00_2859;

					BgL_rz00_2858 = BINT(0L);
					BgL_wz00_2859 = 0L;
				BgL_zc3z04anonymousza32668ze3z87_2860:
					if (((long) CINT(BgL_rz00_2858) == (long) (BgL_lenz00_119)))
						{	/* Llib/unicode.scm 1467 */
							return BgL_nstrz00_117;
						}
					else
						{	/* Llib/unicode.scm 1469 */
							unsigned char BgL_cz00_2862;

							BgL_cz00_2862 =
								STRING_REF(BgL_strz00_118, (long) CINT(BgL_rz00_2858));
							{	/* Llib/unicode.scm 1469 */
								long BgL_nz00_2863;

								BgL_nz00_2863 = (BgL_cz00_2862);
								{	/* Llib/unicode.scm 1470 */

									if ((BgL_nz00_2863 <= 127L))
										{	/* Llib/unicode.scm 1472 */
											STRING_SET(BgL_nstrz00_117, BgL_wz00_2859, BgL_cz00_2862);
											{	/* Llib/unicode.scm 1474 */
												long BgL_arg2672z00_2865;
												long BgL_arg2673z00_2866;

												BgL_arg2672z00_2865 = ((long) CINT(BgL_rz00_2858) + 1L);
												BgL_arg2673z00_2866 = (BgL_wz00_2859 + 1L);
												{
													long BgL_wz00_10814;
													obj_t BgL_rz00_10812;

													BgL_rz00_10812 = BINT(BgL_arg2672z00_2865);
													BgL_wz00_10814 = BgL_arg2673z00_2866;
													BgL_wz00_2859 = BgL_wz00_10814;
													BgL_rz00_2858 = BgL_rz00_10812;
													goto BgL_zc3z04anonymousza32668ze3z87_2860;
												}
											}
										}
									else
										{	/* Llib/unicode.scm 1472 */
											if ((BgL_nz00_2863 < 194L))
												{	/* Llib/unicode.scm 1475 */
													BgL_rz00_2900 = BgL_rz00_2858;
												BgL_zc3z04anonymousza32697ze3z87_2901:
													{	/* Llib/unicode.scm 1441 */
														obj_t BgL_arg2698z00_2902;
														obj_t BgL_arg2699z00_2903;

														{	/* Llib/unicode.scm 1441 */
															long BgL_arg2700z00_2904;

															BgL_arg2700z00_2904 =
																(STRING_REF(BgL_strz00_118,
																	(long) CINT(BgL_rz00_2900)));
															{	/* Llib/unicode.scm 1440 */
																obj_t BgL_list2701z00_2905;

																{	/* Llib/unicode.scm 1440 */
																	obj_t BgL_arg2702z00_2906;

																	BgL_arg2702z00_2906 =
																		MAKE_YOUNG_PAIR(BgL_rz00_2900, BNIL);
																	BgL_list2701z00_2905 =
																		MAKE_YOUNG_PAIR(BINT(BgL_arg2700z00_2904),
																		BgL_arg2702z00_2906);
																}
																BgL_arg2698z00_2902 =
																	BGl_formatz00zz__r4_output_6_10_3z00
																	(BGl_string3690z00zz__unicodez00,
																	BgL_list2701z00_2905);
														}}
														{	/* Llib/unicode.scm 1443 */
															obj_t BgL_arg2704z00_2908;

															{	/* Llib/unicode.scm 1443 */
																int BgL_arg2705z00_2909;

																{	/* Llib/unicode.scm 1443 */
																	long BgL_bz00_2911;

																	BgL_bz00_2911 =
																		((long) CINT(BgL_rz00_2900) + 10L);
																	if (((long) (BgL_lenz00_119) < BgL_bz00_2911))
																		{	/* Llib/unicode.scm 1443 */
																			BgL_arg2705z00_2909 = BgL_lenz00_119;
																		}
																	else
																		{	/* Llib/unicode.scm 1443 */
																			BgL_arg2705z00_2909 =
																				(int) (BgL_bz00_2911);
																}}
																BgL_arg2704z00_2908 =
																	c_substring(BgL_strz00_118,
																	(long) CINT(BgL_rz00_2900),
																	(long) (BgL_arg2705z00_2909));
															}
															BgL_arg2699z00_2903 =
																string_for_read(BgL_arg2704z00_2908);
														}
														return
															BGl_errorz00zz__errorz00
															(BGl_string3691z00zz__unicodez00,
															BgL_arg2698z00_2902, BgL_arg2699z00_2903);
													}
												}
											else
												{	/* Llib/unicode.scm 1477 */
													obj_t BgL_g1063z00_2868;

													if (CBOOL(BgL_tablez00_120))
														{	/* Llib/unicode.scm 1477 */
															BgL_g1063z00_2868 =
																BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BINT
																(BgL_nz00_2863), BgL_tablez00_120);
														}
													else
														{	/* Llib/unicode.scm 1477 */
															BgL_g1063z00_2868 = BFALSE;
														}
													if (CBOOL(BgL_g1063z00_2868))
														{
															long BgL_wz00_10843;
															obj_t BgL_rz00_10841;

															BgL_rz00_10841 =
																BGl_tablezd2ze38bitsze70zd6zz__unicodez00
																(BgL_nstrz00_117, BgL_tablez00_120,
																BgL_strz00_118, BgL_lenz00_119,
																BgL_g1063z00_2868, BgL_rz00_2858, BgL_wz00_2859,
																BgL_nz00_2863);
															BgL_wz00_10843 = (BgL_wz00_2859 + 1L);
															BgL_wz00_2859 = BgL_wz00_10843;
															BgL_rz00_2858 = BgL_rz00_10841;
															goto BgL_zc3z04anonymousza32668ze3z87_2860;
														}
													else
														{	/* Llib/unicode.scm 1477 */
															if ((BgL_nz00_2863 <= 223L))
																{	/* Llib/unicode.scm 1481 */
																	if (
																		((long) CINT(BgL_rz00_2858) ==
																			((long) (BgL_lenz00_119) - 1L)))
																		{	/* Llib/unicode.scm 1482 */
																			return
																				BGl_errorzd2toozd2shortze70ze7zz__unicodez00
																				(BgL_strz00_118, BgL_lenz00_119,
																				BgL_rz00_2858);
																		}
																	else
																		{	/* Llib/unicode.scm 1484 */
																			unsigned char BgL_ncz00_2876;

																			BgL_ncz00_2876 =
																				STRING_REF(BgL_strz00_118,
																				((long) CINT(BgL_rz00_2858) + 1L));
																			{	/* Llib/unicode.scm 1484 */
																				long BgL_nnz00_2877;

																				BgL_nnz00_2877 = (BgL_ncz00_2876);
																				{	/* Llib/unicode.scm 1485 */

																					{	/* Llib/unicode.scm 1486 */
																						long BgL_mz00_2878;

																						BgL_mz00_2878 =
																							(
																							((BgL_nz00_2863 & 31L) <<
																								(int) (6L)) |
																							(63L & BgL_nnz00_2877));
																						if ((BgL_mz00_2878 > 255L))
																							{	/* Llib/unicode.scm 1488 */
																								STRING_SET(BgL_nstrz00_117,
																									BgL_wz00_2859,
																									((unsigned char) '.'));
																								{	/* Llib/unicode.scm 1494 */
																									long BgL_arg2681z00_2880;
																									long BgL_arg2682z00_2881;

																									BgL_arg2681z00_2880 =
																										(
																										(long) CINT(BgL_rz00_2858) +
																										2L);
																									BgL_arg2682z00_2881 =
																										(BgL_wz00_2859 + 1L);
																									{
																										long BgL_wz00_10870;
																										obj_t BgL_rz00_10868;

																										BgL_rz00_10868 =
																											BINT(BgL_arg2681z00_2880);
																										BgL_wz00_10870 =
																											BgL_arg2682z00_2881;
																										BgL_wz00_2859 =
																											BgL_wz00_10870;
																										BgL_rz00_2858 =
																											BgL_rz00_10868;
																										goto
																											BgL_zc3z04anonymousza32668ze3z87_2860;
																									}
																								}
																							}
																						else
																							{	/* Llib/unicode.scm 1488 */
																								{	/* Llib/unicode.scm 1496 */
																									unsigned char
																										BgL_tmpz00_10871;
																									BgL_tmpz00_10871 =
																										(BgL_mz00_2878);
																									STRING_SET(BgL_nstrz00_117,
																										BgL_wz00_2859,
																										BgL_tmpz00_10871);
																								}
																								{	/* Llib/unicode.scm 1497 */
																									long BgL_arg2685z00_2883;
																									long BgL_arg2686z00_2884;

																									BgL_arg2685z00_2883 =
																										(
																										(long) CINT(BgL_rz00_2858) +
																										2L);
																									BgL_arg2686z00_2884 =
																										(BgL_wz00_2859 + 1L);
																									{
																										long BgL_wz00_10879;
																										obj_t BgL_rz00_10877;

																										BgL_rz00_10877 =
																											BINT(BgL_arg2685z00_2883);
																										BgL_wz00_10879 =
																											BgL_arg2686z00_2884;
																										BgL_wz00_2859 =
																											BgL_wz00_10879;
																										BgL_rz00_2858 =
																											BgL_rz00_10877;
																										goto
																											BgL_zc3z04anonymousza32668ze3z87_2860;
																									}
																								}
																							}
																					}
																				}
																			}
																		}
																}
															else
																{
																	obj_t BgL_rz00_10880;

																	BgL_rz00_10880 = BgL_rz00_2858;
																	BgL_rz00_2900 = BgL_rz00_10880;
																	goto BgL_zc3z04anonymousza32697ze3z87_2901;
																}
														}
												}
										}
								}
							}
						}
				}
			}
		}

	}



/* error-too-short~0 */
	obj_t BGl_errorzd2toozd2shortze70ze7zz__unicodez00(obj_t BgL_strz00_7015,
		int BgL_lenz00_7014, obj_t BgL_rz00_2892)
	{
		{	/* Llib/unicode.scm 1436 */
			{	/* Llib/unicode.scm 1436 */
				obj_t BgL_arg2693z00_2894;

				{	/* Llib/unicode.scm 1436 */
					obj_t BgL_arg2694z00_2895;

					{	/* Llib/unicode.scm 1436 */
						long BgL_arg2695z00_2896;

						{	/* Llib/unicode.scm 1436 */
							long BgL_bz00_2898;

							BgL_bz00_2898 = ((long) CINT(BgL_rz00_2892) - 10L);
							if ((0L > BgL_bz00_2898))
								{	/* Llib/unicode.scm 1436 */
									BgL_arg2695z00_2896 = 0L;
								}
							else
								{	/* Llib/unicode.scm 1436 */
									BgL_arg2695z00_2896 = BgL_bz00_2898;
								}
						}
						BgL_arg2694z00_2895 =
							c_substring(BgL_strz00_7015, BgL_arg2695z00_2896,
							(long) (BgL_lenz00_7014));
					}
					BgL_arg2693z00_2894 = string_for_read(BgL_arg2694z00_2895);
				}
				return
					BGl_errorz00zz__errorz00(BGl_string3691z00zz__unicodez00,
					BGl_string3692z00zz__unicodez00, BgL_arg2693z00_2894);
			}
		}

	}



/* table->8bits~0 */
	obj_t BGl_tablezd2ze38bitsze70zd6zz__unicodez00(obj_t BgL_nstrz00_7019,
		obj_t BgL_tablez00_7018, obj_t BgL_strz00_7017, int BgL_lenz00_7016,
		obj_t BgL_subtablez00_2925, obj_t BgL_rz00_2926, long BgL_wz00_2927,
		long BgL_nz00_2928)
	{
		{	/* Llib/unicode.scm 1463 */
			{
				obj_t BgL_rz00_2913;

				{	/* Llib/unicode.scm 1451 */
					obj_t BgL_g1060z00_2930;
					long BgL_g1061z00_2931;

					BgL_g1060z00_2930 =
						BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BINT(BgL_nz00_2928),
						BgL_tablez00_7018);
					BgL_g1061z00_2931 = ((long) CINT(BgL_rz00_2926) + 1L);
					{
						obj_t BgL_subtablez00_2933;
						long BgL_nrz00_2934;

						BgL_subtablez00_2933 = BgL_g1060z00_2930;
						BgL_nrz00_2934 = BgL_g1061z00_2931;
					BgL_zc3z04anonymousza32715ze3z87_2935:
						if (CBOOL(BgL_subtablez00_2933))
							{	/* Llib/unicode.scm 1456 */
								bool_t BgL_test4330z00_10896;

								{	/* Llib/unicode.scm 1456 */
									obj_t BgL_tmpz00_10897;

									BgL_tmpz00_10897 = CDR(((obj_t) BgL_subtablez00_2933));
									BgL_test4330z00_10896 = CHARP(BgL_tmpz00_10897);
								}
								if (BgL_test4330z00_10896)
									{	/* Llib/unicode.scm 1456 */
										{	/* Llib/unicode.scm 1457 */
											obj_t BgL_arg2719z00_2938;

											BgL_arg2719z00_2938 = CDR(((obj_t) BgL_subtablez00_2933));
											{	/* Llib/unicode.scm 1457 */
												unsigned char BgL_tmpz00_10903;

												BgL_tmpz00_10903 = CCHAR(BgL_arg2719z00_2938);
												STRING_SET(BgL_nstrz00_7019, BgL_wz00_2927,
													BgL_tmpz00_10903);
										}}
										return BINT(BgL_nrz00_2934);
									}
								else
									{	/* Llib/unicode.scm 1456 */
										if ((BgL_nrz00_2934 == (long) (BgL_lenz00_7016)))
											{	/* Llib/unicode.scm 1459 */
												return
													BGl_errorzd2toozd2shortze70ze7zz__unicodez00
													(BgL_strz00_7017, BgL_lenz00_7016, BgL_rz00_2926);
											}
										else
											{	/* Llib/unicode.scm 1462 */
												long BgL_ncz00_2940;

												BgL_ncz00_2940 =
													(STRING_REF(BgL_strz00_7017, BgL_nrz00_2934));
												{	/* Llib/unicode.scm 1463 */
													obj_t BgL_arg2721z00_2941;
													long BgL_arg2722z00_2942;

													BgL_arg2721z00_2941 =
														BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BINT
														(BgL_ncz00_2940),
														CDR(((obj_t) BgL_subtablez00_2933)));
													BgL_arg2722z00_2942 = (BgL_nrz00_2934 + 1L);
													{
														long BgL_nrz00_10919;
														obj_t BgL_subtablez00_10918;

														BgL_subtablez00_10918 = BgL_arg2721z00_2941;
														BgL_nrz00_10919 = BgL_arg2722z00_2942;
														BgL_nrz00_2934 = BgL_nrz00_10919;
														BgL_subtablez00_2933 = BgL_subtablez00_10918;
														goto BgL_zc3z04anonymousza32715ze3z87_2935;
													}
												}
											}
									}
							}
						else
							{	/* Llib/unicode.scm 1454 */
								BgL_rz00_2913 = BgL_rz00_2926;
								{	/* Llib/unicode.scm 1447 */
									obj_t BgL_arg2708z00_2915;
									obj_t BgL_arg2709z00_2916;

									{	/* Llib/unicode.scm 1447 */
										obj_t BgL_arg2710z00_2917;

										{	/* Ieee/fixnum.scm 1001 */

											BgL_arg2710z00_2917 =
												BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
												(long) CINT(BgL_rz00_2913), 10L);
										}
										BgL_arg2708z00_2915 =
											string_append(BGl_string3693z00zz__unicodez00,
											BgL_arg2710z00_2917);
									}
									{	/* Llib/unicode.scm 1448 */
										obj_t BgL_arg2711z00_2920;

										{	/* Llib/unicode.scm 1448 */
											int BgL_arg2712z00_2921;

											{	/* Llib/unicode.scm 1448 */
												long BgL_bz00_2923;

												BgL_bz00_2923 = ((long) CINT(BgL_rz00_2913) + 10L);
												if (((long) (BgL_lenz00_7016) < BgL_bz00_2923))
													{	/* Llib/unicode.scm 1448 */
														BgL_arg2712z00_2921 = BgL_lenz00_7016;
													}
												else
													{	/* Llib/unicode.scm 1448 */
														BgL_arg2712z00_2921 = (int) (BgL_bz00_2923);
											}}
											BgL_arg2711z00_2920 =
												c_substring(BgL_strz00_7017,
												(long) CINT(BgL_rz00_2913),
												(long) (BgL_arg2712z00_2921));
										}
										BgL_arg2709z00_2916 = string_for_read(BgL_arg2711z00_2920);
									}
									return
										BGl_errorz00zz__errorz00(BGl_string3691z00zz__unicodez00,
										BgL_arg2708z00_2915, BgL_arg2709z00_2916);
								}
							}
					}
				}
			}
		}

	}



/* utf8->8bits */
	BGL_EXPORTED_DEF obj_t BGl_utf8zd2ze38bitsz31zz__unicodez00(obj_t
		BgL_strz00_121, obj_t BgL_tablez00_122)
	{
		{	/* Llib/unicode.scm 1504 */
			{	/* Llib/unicode.scm 1505 */
				long BgL_lenz00_6431;

				BgL_lenz00_6431 = STRING_LENGTH(BgL_strz00_121);
				{	/* Llib/unicode.scm 1505 */
					long BgL_nlenz00_6432;

					BgL_nlenz00_6432 =
						BGl_utf8zd2ze38bitszd2lengthze3zz__unicodez00(BgL_strz00_121,
						BgL_lenz00_6431);
					{	/* Llib/unicode.scm 1506 */

						if ((BgL_lenz00_6431 == BgL_nlenz00_6432))
							{	/* Llib/unicode.scm 1507 */
								return
									BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_strz00_121);
							}
						else
							{	/* Llib/unicode.scm 1507 */
								return
									BGl_utf8zd2ze38bitszd2fillz12zf1zz__unicodez00
									(make_string_sans_fill(BgL_nlenz00_6432), BgL_strz00_121,
									(int) (BgL_lenz00_6431), BgL_tablez00_122);
		}}}}}

	}



/* &utf8->8bits */
	obj_t BGl_z62utf8zd2ze38bitsz53zz__unicodez00(obj_t BgL_envz00_6976,
		obj_t BgL_strz00_6977, obj_t BgL_tablez00_6978)
	{
		{	/* Llib/unicode.scm 1504 */
			{	/* Llib/unicode.scm 1505 */
				obj_t BgL_auxz00_10942;

				if (STRINGP(BgL_strz00_6977))
					{	/* Llib/unicode.scm 1505 */
						BgL_auxz00_10942 = BgL_strz00_6977;
					}
				else
					{
						obj_t BgL_auxz00_10945;

						BgL_auxz00_10945 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
							BINT(59828L), BGl_string3694z00zz__unicodez00,
							BGl_string3661z00zz__unicodez00, BgL_strz00_6977);
						FAILURE(BgL_auxz00_10945, BFALSE, BFALSE);
					}
				return
					BGl_utf8zd2ze38bitsz31zz__unicodez00(BgL_auxz00_10942,
					BgL_tablez00_6978);
			}
		}

	}



/* utf8->8bits! */
	BGL_EXPORTED_DEF obj_t BGl_utf8zd2ze38bitsz12z23zz__unicodez00(obj_t
		BgL_strz00_123, obj_t BgL_tablez00_124)
	{
		{	/* Llib/unicode.scm 1514 */
			{	/* Llib/unicode.scm 1515 */
				long BgL_lenz00_6438;

				BgL_lenz00_6438 = STRING_LENGTH(BgL_strz00_123);
				{	/* Llib/unicode.scm 1515 */
					long BgL_nlenz00_6439;

					BgL_nlenz00_6439 =
						BGl_utf8zd2ze38bitszd2lengthze3zz__unicodez00(BgL_strz00_123,
						BgL_lenz00_6438);
					{	/* Llib/unicode.scm 1516 */

						if ((BgL_lenz00_6438 == BgL_nlenz00_6439))
							{	/* Llib/unicode.scm 1517 */
								return BgL_strz00_123;
							}
						else
							{	/* Llib/unicode.scm 1517 */
								return
									BGl_utf8zd2ze38bitszd2fillz12zf1zz__unicodez00
									(make_string_sans_fill(BgL_nlenz00_6439), BgL_strz00_123,
									(int) (BgL_lenz00_6438), BgL_tablez00_124);
		}}}}}

	}



/* &utf8->8bits! */
	obj_t BGl_z62utf8zd2ze38bitsz12z41zz__unicodez00(obj_t BgL_envz00_6979,
		obj_t BgL_strz00_6980, obj_t BgL_tablez00_6981)
	{
		{	/* Llib/unicode.scm 1514 */
			{	/* Llib/unicode.scm 1515 */
				obj_t BgL_auxz00_10957;

				if (STRINGP(BgL_strz00_6980))
					{	/* Llib/unicode.scm 1515 */
						BgL_auxz00_10957 = BgL_strz00_6980;
					}
				else
					{
						obj_t BgL_auxz00_10960;

						BgL_auxz00_10960 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
							BINT(60274L), BGl_string3695z00zz__unicodez00,
							BGl_string3661z00zz__unicodez00, BgL_strz00_6980);
						FAILURE(BgL_auxz00_10960, BFALSE, BFALSE);
					}
				return
					BGl_utf8zd2ze38bitsz12z23zz__unicodez00(BgL_auxz00_10957,
					BgL_tablez00_6981);
			}
		}

	}



/* utf8->iso-latin */
	BGL_EXPORTED_DEF obj_t BGl_utf8zd2ze3isozd2latinze3zz__unicodez00(obj_t
		BgL_strz00_125)
	{
		{	/* Llib/unicode.scm 1524 */
			{	/* Llib/unicode.scm 1525 */
				obj_t BgL_res3321z00_6454;

				{	/* Llib/unicode.scm 1525 */
					obj_t BgL_tablez00_6446;

					BgL_tablez00_6446 = BGl_list3549z00zz__unicodez00;
					{	/* Llib/unicode.scm 1505 */
						long BgL_lenz00_6447;

						BgL_lenz00_6447 = STRING_LENGTH(BgL_strz00_125);
						{	/* Llib/unicode.scm 1505 */
							long BgL_nlenz00_6448;

							BgL_nlenz00_6448 =
								BGl_utf8zd2ze38bitszd2lengthze3zz__unicodez00(BgL_strz00_125,
								BgL_lenz00_6447);
							{	/* Llib/unicode.scm 1506 */

								if ((BgL_lenz00_6447 == BgL_nlenz00_6448))
									{	/* Llib/unicode.scm 1507 */
										BgL_res3321z00_6454 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_strz00_125);
									}
								else
									{	/* Llib/unicode.scm 1507 */
										BgL_res3321z00_6454 =
											BGl_utf8zd2ze38bitszd2fillz12zf1zz__unicodez00
											(make_string_sans_fill(BgL_nlenz00_6448), BgL_strz00_125,
											(int) (BgL_lenz00_6447), BgL_tablez00_6446);
				}}}}}
				return BgL_res3321z00_6454;
			}
		}

	}



/* &utf8->iso-latin */
	obj_t BGl_z62utf8zd2ze3isozd2latinz81zz__unicodez00(obj_t BgL_envz00_6982,
		obj_t BgL_strz00_6983)
	{
		{	/* Llib/unicode.scm 1524 */
			{	/* Llib/unicode.scm 1525 */
				obj_t BgL_auxz00_10973;

				if (STRINGP(BgL_strz00_6983))
					{	/* Llib/unicode.scm 1525 */
						BgL_auxz00_10973 = BgL_strz00_6983;
					}
				else
					{
						obj_t BgL_auxz00_10976;

						BgL_auxz00_10976 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
							BINT(60703L), BGl_string3696z00zz__unicodez00,
							BGl_string3661z00zz__unicodez00, BgL_strz00_6983);
						FAILURE(BgL_auxz00_10976, BFALSE, BFALSE);
					}
				return BGl_utf8zd2ze3isozd2latinze3zz__unicodez00(BgL_auxz00_10973);
			}
		}

	}



/* utf8->iso-latin! */
	BGL_EXPORTED_DEF obj_t BGl_utf8zd2ze3isozd2latinz12zf1zz__unicodez00(obj_t
		BgL_strz00_126)
	{
		{	/* Llib/unicode.scm 1530 */
			{	/* Llib/unicode.scm 1531 */
				obj_t BgL_res3322z00_6464;

				{	/* Llib/unicode.scm 1531 */
					obj_t BgL_tablez00_6456;

					BgL_tablez00_6456 = BGl_list3549z00zz__unicodez00;
					{	/* Llib/unicode.scm 1515 */
						long BgL_lenz00_6457;

						BgL_lenz00_6457 = STRING_LENGTH(BgL_strz00_126);
						{	/* Llib/unicode.scm 1515 */
							long BgL_nlenz00_6458;

							BgL_nlenz00_6458 =
								BGl_utf8zd2ze38bitszd2lengthze3zz__unicodez00(BgL_strz00_126,
								BgL_lenz00_6457);
							{	/* Llib/unicode.scm 1516 */

								if ((BgL_lenz00_6457 == BgL_nlenz00_6458))
									{	/* Llib/unicode.scm 1517 */
										BgL_res3322z00_6464 = BgL_strz00_126;
									}
								else
									{	/* Llib/unicode.scm 1517 */
										BgL_res3322z00_6464 =
											BGl_utf8zd2ze38bitszd2fillz12zf1zz__unicodez00
											(make_string_sans_fill(BgL_nlenz00_6458), BgL_strz00_126,
											(int) (BgL_lenz00_6457), BgL_tablez00_6456);
				}}}}}
				return BgL_res3322z00_6464;
			}
		}

	}



/* &utf8->iso-latin! */
	obj_t BGl_z62utf8zd2ze3isozd2latinz12z93zz__unicodez00(obj_t BgL_envz00_6984,
		obj_t BgL_strz00_6985)
	{
		{	/* Llib/unicode.scm 1530 */
			{	/* Llib/unicode.scm 1531 */
				obj_t BgL_auxz00_10988;

				if (STRINGP(BgL_strz00_6985))
					{	/* Llib/unicode.scm 1531 */
						BgL_auxz00_10988 = BgL_strz00_6985;
					}
				else
					{
						obj_t BgL_auxz00_10991;

						BgL_auxz00_10991 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
							BINT(60989L), BGl_string3697z00zz__unicodez00,
							BGl_string3661z00zz__unicodez00, BgL_strz00_6985);
						FAILURE(BgL_auxz00_10991, BFALSE, BFALSE);
					}
				return BGl_utf8zd2ze3isozd2latinz12zf1zz__unicodez00(BgL_auxz00_10988);
			}
		}

	}



/* utf8->iso-latin-15 */
	BGL_EXPORTED_DEF obj_t BGl_utf8zd2ze3isozd2latinzd215z31zz__unicodez00(obj_t
		BgL_strz00_127)
	{
		{	/* Llib/unicode.scm 1536 */
			{	/* Llib/unicode.scm 1537 */
				obj_t BgL_res3323z00_6474;

				{	/* Llib/unicode.scm 1537 */
					obj_t BgL_tablez00_6466;

					BgL_tablez00_6466 = BGl_list3576z00zz__unicodez00;
					{	/* Llib/unicode.scm 1505 */
						long BgL_lenz00_6467;

						BgL_lenz00_6467 = STRING_LENGTH(BgL_strz00_127);
						{	/* Llib/unicode.scm 1505 */
							long BgL_nlenz00_6468;

							BgL_nlenz00_6468 =
								BGl_utf8zd2ze38bitszd2lengthze3zz__unicodez00(BgL_strz00_127,
								BgL_lenz00_6467);
							{	/* Llib/unicode.scm 1506 */

								if ((BgL_lenz00_6467 == BgL_nlenz00_6468))
									{	/* Llib/unicode.scm 1507 */
										BgL_res3323z00_6474 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_strz00_127);
									}
								else
									{	/* Llib/unicode.scm 1507 */
										BgL_res3323z00_6474 =
											BGl_utf8zd2ze38bitszd2fillz12zf1zz__unicodez00
											(make_string_sans_fill(BgL_nlenz00_6468), BgL_strz00_127,
											(int) (BgL_lenz00_6467), BgL_tablez00_6466);
				}}}}}
				return BgL_res3323z00_6474;
			}
		}

	}



/* &utf8->iso-latin-15 */
	obj_t BGl_z62utf8zd2ze3isozd2latinzd215z53zz__unicodez00(obj_t
		BgL_envz00_6986, obj_t BgL_strz00_6987)
	{
		{	/* Llib/unicode.scm 1536 */
			{	/* Llib/unicode.scm 1537 */
				obj_t BgL_auxz00_11004;

				if (STRINGP(BgL_strz00_6987))
					{	/* Llib/unicode.scm 1537 */
						BgL_auxz00_11004 = BgL_strz00_6987;
					}
				else
					{
						obj_t BgL_auxz00_11007;

						BgL_auxz00_11007 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
							BINT(61278L), BGl_string3698z00zz__unicodez00,
							BGl_string3661z00zz__unicodez00, BgL_strz00_6987);
						FAILURE(BgL_auxz00_11007, BFALSE, BFALSE);
					}
				return
					BGl_utf8zd2ze3isozd2latinzd215z31zz__unicodez00(BgL_auxz00_11004);
			}
		}

	}



/* utf8->iso-latin-15! */
	BGL_EXPORTED_DEF obj_t
		BGl_utf8zd2ze3isozd2latinzd215z12z23zz__unicodez00(obj_t BgL_strz00_128)
	{
		{	/* Llib/unicode.scm 1542 */
			{	/* Llib/unicode.scm 1543 */
				obj_t BgL_res3324z00_6484;

				{	/* Llib/unicode.scm 1543 */
					obj_t BgL_tablez00_6476;

					BgL_tablez00_6476 = BGl_list3576z00zz__unicodez00;
					{	/* Llib/unicode.scm 1515 */
						long BgL_lenz00_6477;

						BgL_lenz00_6477 = STRING_LENGTH(BgL_strz00_128);
						{	/* Llib/unicode.scm 1515 */
							long BgL_nlenz00_6478;

							BgL_nlenz00_6478 =
								BGl_utf8zd2ze38bitszd2lengthze3zz__unicodez00(BgL_strz00_128,
								BgL_lenz00_6477);
							{	/* Llib/unicode.scm 1516 */

								if ((BgL_lenz00_6477 == BgL_nlenz00_6478))
									{	/* Llib/unicode.scm 1517 */
										BgL_res3324z00_6484 = BgL_strz00_128;
									}
								else
									{	/* Llib/unicode.scm 1517 */
										BgL_res3324z00_6484 =
											BGl_utf8zd2ze38bitszd2fillz12zf1zz__unicodez00
											(make_string_sans_fill(BgL_nlenz00_6478), BgL_strz00_128,
											(int) (BgL_lenz00_6477), BgL_tablez00_6476);
				}}}}}
				return BgL_res3324z00_6484;
			}
		}

	}



/* &utf8->iso-latin-15! */
	obj_t BGl_z62utf8zd2ze3isozd2latinzd215z12z41zz__unicodez00(obj_t
		BgL_envz00_6988, obj_t BgL_strz00_6989)
	{
		{	/* Llib/unicode.scm 1542 */
			{	/* Llib/unicode.scm 1543 */
				obj_t BgL_auxz00_11019;

				if (STRINGP(BgL_strz00_6989))
					{	/* Llib/unicode.scm 1543 */
						BgL_auxz00_11019 = BgL_strz00_6989;
					}
				else
					{
						obj_t BgL_auxz00_11022;

						BgL_auxz00_11022 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
							BINT(61576L), BGl_string3699z00zz__unicodez00,
							BGl_string3661z00zz__unicodez00, BgL_strz00_6989);
						FAILURE(BgL_auxz00_11022, BFALSE, BFALSE);
					}
				return
					BGl_utf8zd2ze3isozd2latinzd215z12z23zz__unicodez00(BgL_auxz00_11019);
			}
		}

	}



/* utf8->cp1252 */
	BGL_EXPORTED_DEF obj_t BGl_utf8zd2ze3cp1252z31zz__unicodez00(obj_t
		BgL_strz00_129)
	{
		{	/* Llib/unicode.scm 1548 */
			if (CBOOL(BGl_cp1252zd2invzd2zz__unicodez00))
				{	/* Llib/unicode.scm 1549 */
					BFALSE;
				}
			else
				{	/* Llib/unicode.scm 1549 */
					BGl_cp1252zd2invzd2zz__unicodez00 =
						BGl_inversezd2utf8zd2tablez00zz__unicodez00
						(BGl_vector3588z00zz__unicodez00);
				}
			{	/* Llib/unicode.scm 1550 */
				obj_t BgL_res3325z00_6494;

				{	/* Llib/unicode.scm 1550 */
					obj_t BgL_tablez00_6486;

					BgL_tablez00_6486 = BGl_cp1252zd2invzd2zz__unicodez00;
					{	/* Llib/unicode.scm 1505 */
						long BgL_lenz00_6487;

						BgL_lenz00_6487 = STRING_LENGTH(BgL_strz00_129);
						{	/* Llib/unicode.scm 1505 */
							long BgL_nlenz00_6488;

							BgL_nlenz00_6488 =
								BGl_utf8zd2ze38bitszd2lengthze3zz__unicodez00(BgL_strz00_129,
								BgL_lenz00_6487);
							{	/* Llib/unicode.scm 1506 */

								if ((BgL_lenz00_6487 == BgL_nlenz00_6488))
									{	/* Llib/unicode.scm 1507 */
										BgL_res3325z00_6494 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_strz00_129);
									}
								else
									{	/* Llib/unicode.scm 1507 */
										BgL_res3325z00_6494 =
											BGl_utf8zd2ze38bitszd2fillz12zf1zz__unicodez00
											(make_string_sans_fill(BgL_nlenz00_6488), BgL_strz00_129,
											(int) (BgL_lenz00_6487), BgL_tablez00_6486);
				}}}}}
				return BgL_res3325z00_6494;
			}
		}

	}



/* &utf8->cp1252 */
	obj_t BGl_z62utf8zd2ze3cp1252z53zz__unicodez00(obj_t BgL_envz00_6990,
		obj_t BgL_strz00_6991)
	{
		{	/* Llib/unicode.scm 1548 */
			{	/* Llib/unicode.scm 1549 */
				obj_t BgL_auxz00_11038;

				if (STRINGP(BgL_strz00_6991))
					{	/* Llib/unicode.scm 1549 */
						BgL_auxz00_11038 = BgL_strz00_6991;
					}
				else
					{
						obj_t BgL_auxz00_11041;

						BgL_auxz00_11041 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
							BINT(61932L), BGl_string3700z00zz__unicodez00,
							BGl_string3661z00zz__unicodez00, BgL_strz00_6991);
						FAILURE(BgL_auxz00_11041, BFALSE, BFALSE);
					}
				return BGl_utf8zd2ze3cp1252z31zz__unicodez00(BgL_auxz00_11038);
			}
		}

	}



/* utf8->cp1252! */
	BGL_EXPORTED_DEF obj_t BGl_utf8zd2ze3cp1252z12z23zz__unicodez00(obj_t
		BgL_strz00_130)
	{
		{	/* Llib/unicode.scm 1555 */
			if (CBOOL(BGl_cp1252zd2invzd2zz__unicodez00))
				{	/* Llib/unicode.scm 1556 */
					BFALSE;
				}
			else
				{	/* Llib/unicode.scm 1556 */
					BGl_cp1252zd2invzd2zz__unicodez00 =
						BGl_inversezd2utf8zd2tablez00zz__unicodez00
						(BGl_vector3588z00zz__unicodez00);
				}
			{	/* Llib/unicode.scm 1557 */
				obj_t BgL_res3326z00_6504;

				{	/* Llib/unicode.scm 1557 */
					obj_t BgL_tablez00_6496;

					BgL_tablez00_6496 = BGl_cp1252zd2invzd2zz__unicodez00;
					{	/* Llib/unicode.scm 1515 */
						long BgL_lenz00_6497;

						BgL_lenz00_6497 = STRING_LENGTH(BgL_strz00_130);
						{	/* Llib/unicode.scm 1515 */
							long BgL_nlenz00_6498;

							BgL_nlenz00_6498 =
								BGl_utf8zd2ze38bitszd2lengthze3zz__unicodez00(BgL_strz00_130,
								BgL_lenz00_6497);
							{	/* Llib/unicode.scm 1516 */

								if ((BgL_lenz00_6497 == BgL_nlenz00_6498))
									{	/* Llib/unicode.scm 1517 */
										BgL_res3326z00_6504 = BgL_strz00_130;
									}
								else
									{	/* Llib/unicode.scm 1517 */
										BgL_res3326z00_6504 =
											BGl_utf8zd2ze38bitszd2fillz12zf1zz__unicodez00
											(make_string_sans_fill(BgL_nlenz00_6498), BgL_strz00_130,
											(int) (BgL_lenz00_6497), BgL_tablez00_6496);
				}}}}}
				return BgL_res3326z00_6504;
			}
		}

	}



/* &utf8->cp1252! */
	obj_t BGl_z62utf8zd2ze3cp1252z12z41zz__unicodez00(obj_t BgL_envz00_6992,
		obj_t BgL_strz00_6993)
	{
		{	/* Llib/unicode.scm 1555 */
			{	/* Llib/unicode.scm 1556 */
				obj_t BgL_auxz00_11056;

				if (STRINGP(BgL_strz00_6993))
					{	/* Llib/unicode.scm 1556 */
						BgL_auxz00_11056 = BgL_strz00_6993;
					}
				else
					{
						obj_t BgL_auxz00_11059;

						BgL_auxz00_11059 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
							BINT(62285L), BGl_string3701z00zz__unicodez00,
							BGl_string3661z00zz__unicodez00, BgL_strz00_6993);
						FAILURE(BgL_auxz00_11059, BFALSE, BFALSE);
					}
				return BGl_utf8zd2ze3cp1252z12z23zz__unicodez00(BgL_auxz00_11056);
			}
		}

	}



/* 8bits->utf8-length */
	long BGl_8bitszd2ze3utf8zd2lengthze3zz__unicodez00(obj_t BgL_strz00_131,
		long BgL_lenz00_132, obj_t BgL_tablez00_133)
	{
		{	/* Llib/unicode.scm 1562 */
			{
				long BgL_cz00_2976;

				{
					long BgL_siza7eza7_2961;
					long BgL_iz00_2962;

					BgL_siza7eza7_2961 = 0L;
					BgL_iz00_2962 = 0L;
				BgL_zc3z04anonymousza32730ze3z87_2963:
					if ((BgL_iz00_2962 == BgL_lenz00_132))
						{	/* Llib/unicode.scm 1572 */
							return BgL_siza7eza7_2961;
						}
					else
						{	/* Llib/unicode.scm 1574 */
							long BgL_cz00_2965;

							BgL_cz00_2965 = (STRING_REF(BgL_strz00_131, BgL_iz00_2962));
							if ((BgL_cz00_2965 >= 128L))
								{	/* Llib/unicode.scm 1575 */
									if (CBOOL(BgL_tablez00_133))
										{
											long BgL_iz00_11084;
											long BgL_siza7eza7_11072;

											{	/* Llib/unicode.scm 1577 */
												long BgL_tmpz00_11073;

												BgL_cz00_2976 = BgL_cz00_2965;
												{	/* Llib/unicode.scm 1565 */
													long BgL_vz00_2978;

													BgL_vz00_2978 = (BgL_cz00_2976 - 128L);
													if (
														(BgL_vz00_2978 <
															VECTOR_LENGTH(((obj_t) BgL_tablez00_133))))
														{	/* Llib/unicode.scm 1567 */
															obj_t BgL_arg2746z00_2981;

															BgL_arg2746z00_2981 =
																VECTOR_REF(
																((obj_t) BgL_tablez00_133), BgL_vz00_2978);
															BgL_tmpz00_11073 =
																STRING_LENGTH(((obj_t) BgL_arg2746z00_2981));
														}
													else
														{	/* Llib/unicode.scm 1566 */
															BgL_tmpz00_11073 = 2L;
														}
												}
												BgL_siza7eza7_11072 =
													(BgL_siza7eza7_2961 + BgL_tmpz00_11073);
											}
											BgL_iz00_11084 = (BgL_iz00_2962 + 1L);
											BgL_iz00_2962 = BgL_iz00_11084;
											BgL_siza7eza7_2961 = BgL_siza7eza7_11072;
											goto BgL_zc3z04anonymousza32730ze3z87_2963;
										}
									else
										{
											long BgL_iz00_11088;
											long BgL_siza7eza7_11086;

											BgL_siza7eza7_11086 = (BgL_siza7eza7_2961 + 2L);
											BgL_iz00_11088 = (BgL_iz00_2962 + 1L);
											BgL_iz00_2962 = BgL_iz00_11088;
											BgL_siza7eza7_2961 = BgL_siza7eza7_11086;
											goto BgL_zc3z04anonymousza32730ze3z87_2963;
										}
								}
							else
								{
									long BgL_iz00_11092;
									long BgL_siza7eza7_11090;

									BgL_siza7eza7_11090 = (BgL_siza7eza7_2961 + 1L);
									BgL_iz00_11092 = (BgL_iz00_2962 + 1L);
									BgL_iz00_2962 = BgL_iz00_11092;
									BgL_siza7eza7_2961 = BgL_siza7eza7_11090;
									goto BgL_zc3z04anonymousza32730ze3z87_2963;
								}
						}
				}
			}
		}

	}



/* 8bits->utf8-fill! */
	obj_t BGl_8bitszd2ze3utf8zd2fillz12zf1zz__unicodez00(obj_t BgL_nstrz00_134,
		obj_t BgL_strz00_135, long BgL_lenz00_136, obj_t BgL_tablez00_137)
	{
		{	/* Llib/unicode.scm 1584 */
			{
				long BgL_rz00_2985;
				long BgL_wz00_2986;

				BgL_rz00_2985 = 0L;
				BgL_wz00_2986 = 0L;
			BgL_zc3z04anonymousza32748ze3z87_2987:
				if ((BgL_rz00_2985 == BgL_lenz00_136))
					{	/* Llib/unicode.scm 1587 */
						return BgL_nstrz00_134;
					}
				else
					{	/* Llib/unicode.scm 1589 */
						long BgL_cz00_2989;

						BgL_cz00_2989 = (STRING_REF(BgL_strz00_135, BgL_rz00_2985));
						if ((BgL_cz00_2989 >= 192L))
							{	/* Llib/unicode.scm 1591 */
								STRING_SET(BgL_nstrz00_134, BgL_wz00_2986,
									((unsigned char) 195));
								{	/* Llib/unicode.scm 1593 */
									unsigned char BgL_auxz00_11103;
									long BgL_tmpz00_11101;

									BgL_auxz00_11103 = ((BgL_cz00_2989 - 64L));
									BgL_tmpz00_11101 = (BgL_wz00_2986 + 1L);
									STRING_SET(BgL_nstrz00_134, BgL_tmpz00_11101,
										BgL_auxz00_11103);
								}
								{
									long BgL_wz00_11109;
									long BgL_rz00_11107;

									BgL_rz00_11107 = (BgL_rz00_2985 + 1L);
									BgL_wz00_11109 = (BgL_wz00_2986 + 2L);
									BgL_wz00_2986 = BgL_wz00_11109;
									BgL_rz00_2985 = BgL_rz00_11107;
									goto BgL_zc3z04anonymousza32748ze3z87_2987;
								}
							}
						else
							{	/* Llib/unicode.scm 1591 */
								if ((BgL_cz00_2989 >= 128L))
									{	/* Llib/unicode.scm 1595 */
										if (CBOOL(BgL_tablez00_137))
											{	/* Llib/unicode.scm 1597 */
												long BgL_vz00_2997;

												BgL_vz00_2997 = (BgL_cz00_2989 - 128L);
												if (
													(BgL_vz00_2997 <
														VECTOR_LENGTH(((obj_t) BgL_tablez00_137))))
													{	/* Llib/unicode.scm 1599 */
														obj_t BgL_encz00_3000;

														BgL_encz00_3000 =
															VECTOR_REF(
															((obj_t) BgL_tablez00_137), BgL_vz00_2997);
														{	/* Llib/unicode.scm 1599 */
															long BgL_lenz00_3001;

															BgL_lenz00_3001 =
																STRING_LENGTH(((obj_t) BgL_encz00_3000));
															{	/* Llib/unicode.scm 1600 */

																blit_string(
																	((obj_t) BgL_encz00_3000), 0L,
																	BgL_nstrz00_134, BgL_wz00_2986,
																	BgL_lenz00_3001);
																{
																	long BgL_wz00_11128;
																	long BgL_rz00_11126;

																	BgL_rz00_11126 = (BgL_rz00_2985 + 1L);
																	BgL_wz00_11128 =
																		(BgL_wz00_2986 + BgL_lenz00_3001);
																	BgL_wz00_2986 = BgL_wz00_11128;
																	BgL_rz00_2985 = BgL_rz00_11126;
																	goto BgL_zc3z04anonymousza32748ze3z87_2987;
																}
															}
														}
													}
												else
													{	/* Llib/unicode.scm 1598 */
														STRING_SET(BgL_nstrz00_134, BgL_wz00_2986,
															((unsigned char) 194));
														{	/* Llib/unicode.scm 1605 */
															unsigned char BgL_auxz00_11133;
															long BgL_tmpz00_11131;

															BgL_auxz00_11133 = (BgL_cz00_2989);
															BgL_tmpz00_11131 = (BgL_wz00_2986 + 1L);
															STRING_SET(BgL_nstrz00_134, BgL_tmpz00_11131,
																BgL_auxz00_11133);
														}
														{
															long BgL_wz00_11138;
															long BgL_rz00_11136;

															BgL_rz00_11136 = (BgL_rz00_2985 + 1L);
															BgL_wz00_11138 = (BgL_wz00_2986 + 2L);
															BgL_wz00_2986 = BgL_wz00_11138;
															BgL_rz00_2985 = BgL_rz00_11136;
															goto BgL_zc3z04anonymousza32748ze3z87_2987;
														}
													}
											}
										else
											{	/* Llib/unicode.scm 1596 */
												STRING_SET(BgL_nstrz00_134, BgL_wz00_2986,
													((unsigned char) 194));
												{	/* Llib/unicode.scm 1609 */
													unsigned char BgL_auxz00_11143;
													long BgL_tmpz00_11141;

													BgL_auxz00_11143 = (BgL_cz00_2989);
													BgL_tmpz00_11141 = (BgL_wz00_2986 + 1L);
													STRING_SET(BgL_nstrz00_134, BgL_tmpz00_11141,
														BgL_auxz00_11143);
												}
												{
													long BgL_wz00_11148;
													long BgL_rz00_11146;

													BgL_rz00_11146 = (BgL_rz00_2985 + 1L);
													BgL_wz00_11148 = (BgL_wz00_2986 + 2L);
													BgL_wz00_2986 = BgL_wz00_11148;
													BgL_rz00_2985 = BgL_rz00_11146;
													goto BgL_zc3z04anonymousza32748ze3z87_2987;
												}
											}
									}
								else
									{	/* Llib/unicode.scm 1595 */
										{	/* Llib/unicode.scm 1612 */
											unsigned char BgL_tmpz00_11150;

											BgL_tmpz00_11150 = (BgL_cz00_2989);
											STRING_SET(BgL_nstrz00_134, BgL_wz00_2986,
												BgL_tmpz00_11150);
										}
										{
											long BgL_wz00_11155;
											long BgL_rz00_11153;

											BgL_rz00_11153 = (BgL_rz00_2985 + 1L);
											BgL_wz00_11155 = (BgL_wz00_2986 + 1L);
											BgL_wz00_2986 = BgL_wz00_11155;
											BgL_rz00_2985 = BgL_rz00_11153;
											goto BgL_zc3z04anonymousza32748ze3z87_2987;
										}
									}
							}
					}
			}
		}

	}



/* 8bits->utf8 */
	BGL_EXPORTED_DEF obj_t BGl_8bitszd2ze3utf8z31zz__unicodez00(obj_t
		BgL_strz00_138, obj_t BgL_tablez00_139)
	{
		{	/* Llib/unicode.scm 1618 */
			{	/* Llib/unicode.scm 1619 */
				long BgL_lenz00_6583;

				BgL_lenz00_6583 = STRING_LENGTH(BgL_strz00_138);
				{	/* Llib/unicode.scm 1619 */
					long BgL_nlenz00_6584;

					BgL_nlenz00_6584 =
						BGl_8bitszd2ze3utf8zd2lengthze3zz__unicodez00(BgL_strz00_138,
						BgL_lenz00_6583, BgL_tablez00_139);
					{	/* Llib/unicode.scm 1620 */

						if ((BgL_lenz00_6583 == BgL_nlenz00_6584))
							{	/* Llib/unicode.scm 1621 */
								return
									BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_strz00_138);
							}
						else
							{	/* Llib/unicode.scm 1621 */
								return
									BGl_8bitszd2ze3utf8zd2fillz12zf1zz__unicodez00
									(make_string_sans_fill(BgL_nlenz00_6584), BgL_strz00_138,
									BgL_lenz00_6583, BgL_tablez00_139);
							}
					}
				}
			}
		}

	}



/* &8bits->utf8 */
	obj_t BGl_z628bitszd2ze3utf8z53zz__unicodez00(obj_t BgL_envz00_6994,
		obj_t BgL_strz00_6995, obj_t BgL_tablez00_6996)
	{
		{	/* Llib/unicode.scm 1618 */
			{	/* Llib/unicode.scm 1619 */
				obj_t BgL_auxz00_11164;

				if (STRINGP(BgL_strz00_6995))
					{	/* Llib/unicode.scm 1619 */
						BgL_auxz00_11164 = BgL_strz00_6995;
					}
				else
					{
						obj_t BgL_auxz00_11167;

						BgL_auxz00_11167 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
							BINT(64506L), BGl_string3702z00zz__unicodez00,
							BGl_string3661z00zz__unicodez00, BgL_strz00_6995);
						FAILURE(BgL_auxz00_11167, BFALSE, BFALSE);
					}
				return
					BGl_8bitszd2ze3utf8z31zz__unicodez00(BgL_auxz00_11164,
					BgL_tablez00_6996);
			}
		}

	}



/* 8bits->utf8! */
	BGL_EXPORTED_DEF obj_t BGl_8bitszd2ze3utf8z12z23zz__unicodez00(obj_t
		BgL_strz00_140, obj_t BgL_tablez00_141)
	{
		{	/* Llib/unicode.scm 1628 */
			{	/* Llib/unicode.scm 1629 */
				long BgL_lenz00_6590;

				BgL_lenz00_6590 = STRING_LENGTH(BgL_strz00_140);
				{	/* Llib/unicode.scm 1629 */
					long BgL_nlenz00_6591;

					BgL_nlenz00_6591 =
						BGl_8bitszd2ze3utf8zd2lengthze3zz__unicodez00(BgL_strz00_140,
						BgL_lenz00_6590, BgL_tablez00_141);
					{	/* Llib/unicode.scm 1630 */

						if ((BgL_lenz00_6590 == BgL_nlenz00_6591))
							{	/* Llib/unicode.scm 1631 */
								return BgL_strz00_140;
							}
						else
							{	/* Llib/unicode.scm 1631 */
								return
									BGl_8bitszd2ze3utf8zd2fillz12zf1zz__unicodez00
									(make_string_sans_fill(BgL_nlenz00_6591), BgL_strz00_140,
									BgL_lenz00_6590, BgL_tablez00_141);
							}
					}
				}
			}
		}

	}



/* &8bits->utf8! */
	obj_t BGl_z628bitszd2ze3utf8z12z41zz__unicodez00(obj_t BgL_envz00_6997,
		obj_t BgL_strz00_6998, obj_t BgL_tablez00_6999)
	{
		{	/* Llib/unicode.scm 1628 */
			{	/* Llib/unicode.scm 1629 */
				obj_t BgL_auxz00_11178;

				if (STRINGP(BgL_strz00_6998))
					{	/* Llib/unicode.scm 1629 */
						BgL_auxz00_11178 = BgL_strz00_6998;
					}
				else
					{
						obj_t BgL_auxz00_11181;

						BgL_auxz00_11181 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
							BINT(64958L), BGl_string3703z00zz__unicodez00,
							BGl_string3661z00zz__unicodez00, BgL_strz00_6998);
						FAILURE(BgL_auxz00_11181, BFALSE, BFALSE);
					}
				return
					BGl_8bitszd2ze3utf8z12z23zz__unicodez00(BgL_auxz00_11178,
					BgL_tablez00_6999);
			}
		}

	}



/* iso-latin->utf8 */
	BGL_EXPORTED_DEF obj_t BGl_isozd2latinzd2ze3utf8ze3zz__unicodez00(obj_t
		BgL_strz00_142)
	{
		{	/* Llib/unicode.scm 1638 */
			{	/* Llib/unicode.scm 1639 */
				obj_t BgL_res3327z00_6605;

				{	/* Llib/unicode.scm 1619 */
					long BgL_lenz00_6598;

					BgL_lenz00_6598 = STRING_LENGTH(BgL_strz00_142);
					{	/* Llib/unicode.scm 1619 */
						long BgL_nlenz00_6599;

						BgL_nlenz00_6599 =
							BGl_8bitszd2ze3utf8zd2lengthze3zz__unicodez00(BgL_strz00_142,
							BgL_lenz00_6598, BFALSE);
						{	/* Llib/unicode.scm 1620 */

							if ((BgL_lenz00_6598 == BgL_nlenz00_6599))
								{	/* Llib/unicode.scm 1621 */
									BgL_res3327z00_6605 =
										BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_strz00_142);
								}
							else
								{	/* Llib/unicode.scm 1621 */
									BgL_res3327z00_6605 =
										BGl_8bitszd2ze3utf8zd2fillz12zf1zz__unicodez00
										(make_string_sans_fill(BgL_nlenz00_6599), BgL_strz00_142,
										BgL_lenz00_6598, BFALSE);
								}
						}
					}
				}
				return BgL_res3327z00_6605;
			}
		}

	}



/* &iso-latin->utf8 */
	obj_t BGl_z62isozd2latinzd2ze3utf8z81zz__unicodez00(obj_t BgL_envz00_7000,
		obj_t BgL_strz00_7001)
	{
		{	/* Llib/unicode.scm 1638 */
			{	/* Llib/unicode.scm 1639 */
				obj_t BgL_auxz00_11193;

				if (STRINGP(BgL_strz00_7001))
					{	/* Llib/unicode.scm 1639 */
						BgL_auxz00_11193 = BgL_strz00_7001;
					}
				else
					{
						obj_t BgL_auxz00_11196;

						BgL_auxz00_11196 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
							BINT(65393L), BGl_string3704z00zz__unicodez00,
							BGl_string3661z00zz__unicodez00, BgL_strz00_7001);
						FAILURE(BgL_auxz00_11196, BFALSE, BFALSE);
					}
				return BGl_isozd2latinzd2ze3utf8ze3zz__unicodez00(BgL_auxz00_11193);
			}
		}

	}



/* iso-latin->utf8! */
	BGL_EXPORTED_DEF obj_t BGl_isozd2latinzd2ze3utf8z12zf1zz__unicodez00(obj_t
		BgL_strz00_143)
	{
		{	/* Llib/unicode.scm 1644 */
			{	/* Llib/unicode.scm 1645 */
				obj_t BgL_res3328z00_6614;

				{	/* Llib/unicode.scm 1629 */
					long BgL_lenz00_6607;

					BgL_lenz00_6607 = STRING_LENGTH(BgL_strz00_143);
					{	/* Llib/unicode.scm 1629 */
						long BgL_nlenz00_6608;

						BgL_nlenz00_6608 =
							BGl_8bitszd2ze3utf8zd2lengthze3zz__unicodez00(BgL_strz00_143,
							BgL_lenz00_6607, BFALSE);
						{	/* Llib/unicode.scm 1630 */

							if ((BgL_lenz00_6607 == BgL_nlenz00_6608))
								{	/* Llib/unicode.scm 1631 */
									BgL_res3328z00_6614 = BgL_strz00_143;
								}
							else
								{	/* Llib/unicode.scm 1631 */
									BgL_res3328z00_6614 =
										BGl_8bitszd2ze3utf8zd2fillz12zf1zz__unicodez00
										(make_string_sans_fill(BgL_nlenz00_6608), BgL_strz00_143,
										BgL_lenz00_6607, BFALSE);
								}
						}
					}
				}
				return BgL_res3328z00_6614;
			}
		}

	}



/* &iso-latin->utf8! */
	obj_t BGl_z62isozd2latinzd2ze3utf8z12z93zz__unicodez00(obj_t BgL_envz00_7002,
		obj_t BgL_strz00_7003)
	{
		{	/* Llib/unicode.scm 1644 */
			{	/* Llib/unicode.scm 1645 */
				obj_t BgL_auxz00_11207;

				if (STRINGP(BgL_strz00_7003))
					{	/* Llib/unicode.scm 1645 */
						BgL_auxz00_11207 = BgL_strz00_7003;
					}
				else
					{
						obj_t BgL_auxz00_11210;

						BgL_auxz00_11210 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
							BINT(65675L), BGl_string3705z00zz__unicodez00,
							BGl_string3661z00zz__unicodez00, BgL_strz00_7003);
						FAILURE(BgL_auxz00_11210, BFALSE, BFALSE);
					}
				return BGl_isozd2latinzd2ze3utf8z12zf1zz__unicodez00(BgL_auxz00_11207);
			}
		}

	}



/* cp1252->utf8 */
	BGL_EXPORTED_DEF obj_t BGl_cp1252zd2ze3utf8z31zz__unicodez00(obj_t
		BgL_strz00_144)
	{
		{	/* Llib/unicode.scm 1650 */
			{	/* Llib/unicode.scm 1651 */
				obj_t BgL_res3329z00_6624;

				{	/* Llib/unicode.scm 1651 */
					obj_t BgL_tablez00_6616;

					BgL_tablez00_6616 = BGl_vector3588z00zz__unicodez00;
					{	/* Llib/unicode.scm 1619 */
						long BgL_lenz00_6617;

						BgL_lenz00_6617 = STRING_LENGTH(BgL_strz00_144);
						{	/* Llib/unicode.scm 1619 */
							long BgL_nlenz00_6618;

							BgL_nlenz00_6618 =
								BGl_8bitszd2ze3utf8zd2lengthze3zz__unicodez00(BgL_strz00_144,
								BgL_lenz00_6617, BgL_tablez00_6616);
							{	/* Llib/unicode.scm 1620 */

								if ((BgL_lenz00_6617 == BgL_nlenz00_6618))
									{	/* Llib/unicode.scm 1621 */
										BgL_res3329z00_6624 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_strz00_144);
									}
								else
									{	/* Llib/unicode.scm 1621 */
										BgL_res3329z00_6624 =
											BGl_8bitszd2ze3utf8zd2fillz12zf1zz__unicodez00
											(make_string_sans_fill(BgL_nlenz00_6618), BgL_strz00_144,
											BgL_lenz00_6617, BgL_tablez00_6616);
									}
							}
						}
					}
				}
				return BgL_res3329z00_6624;
			}
		}

	}



/* &cp1252->utf8 */
	obj_t BGl_z62cp1252zd2ze3utf8z53zz__unicodez00(obj_t BgL_envz00_7004,
		obj_t BgL_strz00_7005)
	{
		{	/* Llib/unicode.scm 1650 */
			{	/* Llib/unicode.scm 1651 */
				obj_t BgL_auxz00_11222;

				if (STRINGP(BgL_strz00_7005))
					{	/* Llib/unicode.scm 1651 */
						BgL_auxz00_11222 = BgL_strz00_7005;
					}
				else
					{
						obj_t BgL_auxz00_11225;

						BgL_auxz00_11225 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
							BINT(65951L), BGl_string3706z00zz__unicodez00,
							BGl_string3661z00zz__unicodez00, BgL_strz00_7005);
						FAILURE(BgL_auxz00_11225, BFALSE, BFALSE);
					}
				return BGl_cp1252zd2ze3utf8z31zz__unicodez00(BgL_auxz00_11222);
			}
		}

	}



/* cp1252->utf8! */
	BGL_EXPORTED_DEF obj_t BGl_cp1252zd2ze3utf8z12z23zz__unicodez00(obj_t
		BgL_strz00_145)
	{
		{	/* Llib/unicode.scm 1656 */
			{	/* Llib/unicode.scm 1657 */
				obj_t BgL_res3330z00_6634;

				{	/* Llib/unicode.scm 1657 */
					obj_t BgL_tablez00_6626;

					BgL_tablez00_6626 = BGl_vector3588z00zz__unicodez00;
					{	/* Llib/unicode.scm 1629 */
						long BgL_lenz00_6627;

						BgL_lenz00_6627 = STRING_LENGTH(BgL_strz00_145);
						{	/* Llib/unicode.scm 1629 */
							long BgL_nlenz00_6628;

							BgL_nlenz00_6628 =
								BGl_8bitszd2ze3utf8zd2lengthze3zz__unicodez00(BgL_strz00_145,
								BgL_lenz00_6627, BgL_tablez00_6626);
							{	/* Llib/unicode.scm 1630 */

								if ((BgL_lenz00_6627 == BgL_nlenz00_6628))
									{	/* Llib/unicode.scm 1631 */
										BgL_res3330z00_6634 = BgL_strz00_145;
									}
								else
									{	/* Llib/unicode.scm 1631 */
										BgL_res3330z00_6634 =
											BGl_8bitszd2ze3utf8zd2fillz12zf1zz__unicodez00
											(make_string_sans_fill(BgL_nlenz00_6628), BgL_strz00_145,
											BgL_lenz00_6627, BgL_tablez00_6626);
									}
							}
						}
					}
				}
				return BgL_res3330z00_6634;
			}
		}

	}



/* &cp1252->utf8! */
	obj_t BGl_z62cp1252zd2ze3utf8z12z41zz__unicodez00(obj_t BgL_envz00_7006,
		obj_t BgL_strz00_7007)
	{
		{	/* Llib/unicode.scm 1656 */
			{	/* Llib/unicode.scm 1657 */
				obj_t BgL_auxz00_11236;

				if (STRINGP(BgL_strz00_7007))
					{	/* Llib/unicode.scm 1657 */
						BgL_auxz00_11236 = BgL_strz00_7007;
					}
				else
					{
						obj_t BgL_auxz00_11239;

						BgL_auxz00_11239 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
							BINT(66234L), BGl_string3707z00zz__unicodez00,
							BGl_string3661z00zz__unicodez00, BgL_strz00_7007);
						FAILURE(BgL_auxz00_11239, BFALSE, BFALSE);
					}
				return BGl_cp1252zd2ze3utf8z12z23zz__unicodez00(BgL_auxz00_11236);
			}
		}

	}



/* string-minimal-charset */
	BGL_EXPORTED_DEF obj_t BGl_stringzd2minimalzd2charsetz00zz__unicodez00(obj_t
		BgL_strz00_146)
	{
		{	/* Llib/unicode.scm 1665 */
			{
				long BgL_iz00_3028;

				BgL_iz00_3028 = (STRING_LENGTH(BgL_strz00_146) - 1L);
			BgL_zc3z04anonymousza32787ze3z87_3029:
				if ((BgL_iz00_3028 == -1L))
					{	/* Llib/unicode.scm 1668 */
						return BGl_symbol3662z00zz__unicodez00;
					}
				else
					{	/* Llib/unicode.scm 1668 */
						if (
							(STRING_REF(BgL_strz00_146,
									BgL_iz00_3028) <= ((unsigned char) '')))
							{
								long BgL_iz00_11249;

								BgL_iz00_11249 = (BgL_iz00_3028 - 1L);
								BgL_iz00_3028 = BgL_iz00_11249;
								goto BgL_zc3z04anonymousza32787ze3z87_3029;
							}
						else
							{	/* Llib/unicode.scm 1669 */
								return BGl_symbol3708z00zz__unicodez00;
							}
					}
			}
		}

	}



/* &string-minimal-charset */
	obj_t BGl_z62stringzd2minimalzd2charsetz62zz__unicodez00(obj_t
		BgL_envz00_7008, obj_t BgL_strz00_7009)
	{
		{	/* Llib/unicode.scm 1665 */
			{	/* Llib/unicode.scm 1666 */
				obj_t BgL_auxz00_11253;

				if (STRINGP(BgL_strz00_7009))
					{	/* Llib/unicode.scm 1666 */
						BgL_auxz00_11253 = BgL_strz00_7009;
					}
				else
					{
						obj_t BgL_auxz00_11256;

						BgL_auxz00_11256 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
							BINT(66746L), BGl_string3710z00zz__unicodez00,
							BGl_string3661z00zz__unicodez00, BgL_strz00_7009);
						FAILURE(BgL_auxz00_11256, BFALSE, BFALSE);
					}
				return
					BGl_stringzd2minimalzd2charsetz00zz__unicodez00(BgL_auxz00_11253);
			}
		}

	}



/* utf8-string-minimal-charset */
	BGL_EXPORTED_DEF obj_t
		BGl_utf8zd2stringzd2minimalzd2charsetzd2zz__unicodez00(obj_t BgL_strz00_147)
	{
		{	/* Llib/unicode.scm 1678 */
			{	/* Llib/unicode.scm 1680 */
				long BgL_lenz00_3037;

				BgL_lenz00_3037 = (STRING_LENGTH(BgL_strz00_147) - 1L);
				{
					long BgL_iz00_3040;
					obj_t BgL_charsetz00_3041;

					BgL_iz00_3040 = 0L;
					BgL_charsetz00_3041 = BGl_symbol3662z00zz__unicodez00;
				BgL_zc3z04anonymousza32800ze3z87_3042:
					if ((BgL_iz00_3040 >= BgL_lenz00_3037))
						{	/* Llib/unicode.scm 1683 */
							return BgL_charsetz00_3041;
						}
					else
						{	/* Llib/unicode.scm 1685 */
							long BgL_nz00_3044;

							BgL_nz00_3044 = (STRING_REF(BgL_strz00_147, BgL_iz00_3040));
							if ((BgL_nz00_3044 <= 127L))
								{
									long BgL_iz00_11269;

									BgL_iz00_11269 = (BgL_iz00_3040 + 1L);
									BgL_iz00_3040 = BgL_iz00_11269;
									goto BgL_zc3z04anonymousza32800ze3z87_3042;
								}
							else
								{	/* Llib/unicode.scm 1689 */
									bool_t BgL_test4377z00_11271;

									if ((BgL_nz00_3044 == 194L))
										{	/* Llib/unicode.scm 1689 */
											BgL_test4377z00_11271 = ((bool_t) 1);
										}
									else
										{	/* Llib/unicode.scm 1689 */
											BgL_test4377z00_11271 = (BgL_nz00_3044 == 195L);
										}
									if (BgL_test4377z00_11271)
										{	/* Llib/unicode.scm 1690 */
											long BgL_mz00_3049;

											BgL_mz00_3049 =
												(STRING_REF(BgL_strz00_147, (BgL_iz00_3040 + 1L)));
											{	/* Llib/unicode.scm 1691 */
												bool_t BgL_test4379z00_11278;

												if ((BgL_mz00_3049 >= 128L))
													{	/* Llib/unicode.scm 1691 */
														BgL_test4379z00_11278 = (BgL_mz00_3049 <= 191L);
													}
												else
													{	/* Llib/unicode.scm 1691 */
														BgL_test4379z00_11278 = ((bool_t) 0);
													}
												if (BgL_test4379z00_11278)
													{
														obj_t BgL_charsetz00_11284;
														long BgL_iz00_11282;

														BgL_iz00_11282 = (BgL_iz00_3040 + 2L);
														BgL_charsetz00_11284 =
															BGl_symbol3708z00zz__unicodez00;
														BgL_charsetz00_3041 = BgL_charsetz00_11284;
														BgL_iz00_3040 = BgL_iz00_11282;
														goto BgL_zc3z04anonymousza32800ze3z87_3042;
													}
												else
													{	/* Llib/unicode.scm 1691 */
														return BGl_symbol3671z00zz__unicodez00;
													}
											}
										}
									else
										{	/* Llib/unicode.scm 1689 */
											return BGl_symbol3671z00zz__unicodez00;
										}
								}
						}
				}
			}
		}

	}



/* &utf8-string-minimal-charset */
	obj_t BGl_z62utf8zd2stringzd2minimalzd2charsetzb0zz__unicodez00(obj_t
		BgL_envz00_7010, obj_t BgL_strz00_7011)
	{
		{	/* Llib/unicode.scm 1678 */
			{	/* Llib/unicode.scm 1680 */
				obj_t BgL_auxz00_11285;

				if (STRINGP(BgL_strz00_7011))
					{	/* Llib/unicode.scm 1680 */
						BgL_auxz00_11285 = BgL_strz00_7011;
					}
				else
					{
						obj_t BgL_auxz00_11288;

						BgL_auxz00_11288 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
							BINT(67447L), BGl_string3711z00zz__unicodez00,
							BGl_string3661z00zz__unicodez00, BgL_strz00_7011);
						FAILURE(BgL_auxz00_11288, BFALSE, BFALSE);
					}
				return
					BGl_utf8zd2stringzd2minimalzd2charsetzd2zz__unicodez00
					(BgL_auxz00_11285);
			}
		}

	}



/* ucs2-string-minimal-charset */
	BGL_EXPORTED_DEF obj_t
		BGl_ucs2zd2stringzd2minimalzd2charsetzd2zz__unicodez00(obj_t BgL_strz00_148)
	{
		{	/* Llib/unicode.scm 1703 */
			{	/* Llib/unicode.scm 1705 */
				long BgL_lenz00_3060;

				BgL_lenz00_3060 = ((long) (UCS2_STRING_LENGTH(BgL_strz00_148)) - 1L);
				{
					long BgL_iz00_3063;
					obj_t BgL_charsetz00_3064;

					BgL_iz00_3063 = 0L;
					BgL_charsetz00_3064 = BGl_symbol3662z00zz__unicodez00;
				BgL_zc3z04anonymousza32817ze3z87_3065:
					if ((BgL_iz00_3063 >= BgL_lenz00_3060))
						{	/* Llib/unicode.scm 1708 */
							return BgL_charsetz00_3064;
						}
					else
						{	/* Llib/unicode.scm 1710 */
							int BgL_nz00_3067;

							{	/* Llib/unicode.scm 1710 */
								ucs2_t BgL_arg2828z00_3077;

								{	/* Llib/unicode.scm 277 */
									int BgL_tmpz00_11298;

									BgL_tmpz00_11298 = (int) (BgL_iz00_3063);
									BgL_arg2828z00_3077 =
										UCS2_STRING_REF(BgL_strz00_148, BgL_tmpz00_11298);
								}
								{	/* Llib/unicode.scm 1710 */
									obj_t BgL_tmpz00_11301;

									BgL_tmpz00_11301 = BUCS2(BgL_arg2828z00_3077);
									BgL_nz00_3067 = CUCS2(BgL_tmpz00_11301);
							}}
							if (((long) (BgL_nz00_3067) <= 127L))
								{
									long BgL_iz00_11307;

									BgL_iz00_11307 = (BgL_iz00_3063 + 1L);
									BgL_iz00_3063 = BgL_iz00_11307;
									goto BgL_zc3z04anonymousza32817ze3z87_3065;
								}
							else
								{	/* Llib/unicode.scm 1712 */
									if (((long) (BgL_nz00_3067) <= 255L))
										{
											obj_t BgL_charsetz00_11314;
											long BgL_iz00_11312;

											BgL_iz00_11312 = (BgL_iz00_3063 + 1L);
											if (
												(BgL_charsetz00_3064 ==
													BGl_symbol3712z00zz__unicodez00))
												{	/* Llib/unicode.scm 1715 */
													BgL_charsetz00_11314 =
														BGl_symbol3712z00zz__unicodez00;
												}
											else
												{	/* Llib/unicode.scm 1715 */
													BgL_charsetz00_11314 =
														BGl_symbol3708z00zz__unicodez00;
												}
											BgL_charsetz00_3064 = BgL_charsetz00_11314;
											BgL_iz00_3063 = BgL_iz00_11312;
											goto BgL_zc3z04anonymousza32817ze3z87_3065;
										}
									else
										{	/* Llib/unicode.scm 1716 */
											bool_t BgL_test4386z00_11317;

											if (((long) (BgL_nz00_3067) < 56320L))
												{	/* Llib/unicode.scm 1716 */
													BgL_test4386z00_11317 = ((bool_t) 1);
												}
											else
												{	/* Llib/unicode.scm 1716 */
													BgL_test4386z00_11317 =
														((long) (BgL_nz00_3067) > 56319L);
												}
											if (BgL_test4386z00_11317)
												{
													obj_t BgL_charsetz00_11325;
													long BgL_iz00_11323;

													BgL_iz00_11323 = (BgL_iz00_3063 + 1L);
													BgL_charsetz00_11325 =
														BGl_symbol3712z00zz__unicodez00;
													BgL_charsetz00_3064 = BgL_charsetz00_11325;
													BgL_iz00_3063 = BgL_iz00_11323;
													goto BgL_zc3z04anonymousza32817ze3z87_3065;
												}
											else
												{	/* Llib/unicode.scm 1716 */
													return BGl_symbol3714z00zz__unicodez00;
												}
										}
								}
						}
				}
			}
		}

	}



/* &ucs2-string-minimal-charset */
	obj_t BGl_z62ucs2zd2stringzd2minimalzd2charsetzb0zz__unicodez00(obj_t
		BgL_envz00_7012, obj_t BgL_strz00_7013)
	{
		{	/* Llib/unicode.scm 1703 */
			{	/* Llib/unicode.scm 1705 */
				obj_t BgL_auxz00_11326;

				if (UCS2_STRINGP(BgL_strz00_7013))
					{	/* Llib/unicode.scm 1705 */
						BgL_auxz00_11326 = BgL_strz00_7013;
					}
				else
					{
						obj_t BgL_auxz00_11329;

						BgL_auxz00_11329 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3618z00zz__unicodez00,
							BINT(68449L), BGl_string3716z00zz__unicodez00,
							BGl_string3623z00zz__unicodez00, BgL_strz00_7013);
						FAILURE(BgL_auxz00_11329, BFALSE, BFALSE);
					}
				return
					BGl_ucs2zd2stringzd2minimalzd2charsetzd2zz__unicodez00
					(BgL_auxz00_11326);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__unicodez00(void)
	{
		{	/* Llib/unicode.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__unicodez00(void)
	{
		{	/* Llib/unicode.scm 14 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__unicodez00(void)
	{
		{	/* Llib/unicode.scm 14 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__unicodez00(void)
	{
		{	/* Llib/unicode.scm 14 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string3717z00zz__unicodez00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string3717z00zz__unicodez00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string3717z00zz__unicodez00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string3717z00zz__unicodez00));
			return
				BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string3717z00zz__unicodez00));
		}

	}

#ifdef __cplusplus
}
#endif
