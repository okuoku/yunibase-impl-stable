/*===========================================================================*/
/*   (Llib/binary.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Llib/binary.scm -indent -o objs/obj_u/Llib/binary.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___BINARY_TYPE_DEFINITIONS
#define BGL___BINARY_TYPE_DEFINITIONS
#endif													// BGL___BINARY_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62inputzd2stringzb0zz__binaryz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62closezd2binaryzd2portz62zz__binaryz00(obj_t, obj_t);
	static obj_t BGl_z62outputzd2bytezb0zz__binaryz00(obj_t, obj_t, obj_t);
	extern obj_t append_output_binary_file(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__binaryz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_inputzd2stringzd2zz__binaryz00(obj_t, int);
	static obj_t BGl_z62openzd2inputzd2binaryzd2filezb0zz__binaryz00(obj_t,
		obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62outputzd2stringzb0zz__binaryz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62appendzd2outputzd2binaryzd2filezb0zz__binaryz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__binaryz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__intextz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern int bgl_input_fill_string(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_closezd2binaryzd2portz00zz__binaryz00(obj_t);
	extern obj_t output_obj(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_outputzd2objzd2zz__binaryz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_outputzd2charzd2zz__binaryz00(obj_t,
		unsigned char);
	extern obj_t input_obj(obj_t);
	static obj_t BGl_genericzd2initzd2zz__binaryz00(void);
	extern obj_t bgl_flush_binary_port(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__binaryz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__binaryz00(void);
	extern obj_t open_input_binary_file(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_appendzd2outputzd2binaryzd2filezd2zz__binaryz00(obj_t);
	static obj_t BGl_objectzd2initzd2zz__binaryz00(void);
	BGL_EXPORTED_DECL int BGl_inputzd2fillzd2stringz12z12zz__binaryz00(obj_t,
		obj_t);
	extern obj_t bgl_input_string(obj_t, int);
	static obj_t BGl_z62binaryzd2portzf3z43zz__binaryz00(obj_t, obj_t);
	static obj_t BGl_z62inputzd2objzb0zz__binaryz00(obj_t, obj_t);
	static obj_t BGl_z62outputzd2objzb0zz__binaryz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62flushzd2binaryzd2portz62zz__binaryz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_openzd2inputzd2binaryzd2filezd2zz__binaryz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_outputzd2bytezd2zz__binaryz00(obj_t, char);
	extern int bgl_output_string(obj_t, obj_t);
	static obj_t BGl_z62inputzd2fillzd2stringz12z70zz__binaryz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_openzd2outputzd2binaryzd2filezd2zz__binaryz00(obj_t);
	static obj_t BGl_z62openzd2outputzd2binaryzd2filezb0zz__binaryz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_binaryzd2portzf3z21zz__binaryz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_inputzd2charzd2zz__binaryz00(obj_t);
	static obj_t BGl_methodzd2initzd2zz__binaryz00(void);
	extern obj_t open_output_binary_file(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_inputzd2objzd2zz__binaryz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_flushzd2binaryzd2portz00zz__binaryz00(obj_t);
	static obj_t BGl_z62inputzd2charzb0zz__binaryz00(obj_t, obj_t);
	extern obj_t close_binary_port(obj_t);
	static obj_t BGl_z62outputzd2charzb0zz__binaryz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_outputzd2stringzd2zz__binaryz00(obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_inputzd2stringzd2envz00zz__binaryz00,
		BgL_bgl_za762inputza7d2strin1647z00, BGl_z62inputzd2stringzb0zz__binaryz00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_binaryzd2portzf3zd2envzf3zz__binaryz00,
		BgL_bgl_za762binaryza7d2port1648z00,
		BGl_z62binaryzd2portzf3z43zz__binaryz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_outputzd2stringzd2envz00zz__binaryz00,
		BgL_bgl_za762outputza7d2stri1649z00, BGl_z62outputzd2stringzb0zz__binaryz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_flushzd2binaryzd2portzd2envzd2zz__binaryz00,
		BgL_bgl_za762flushza7d2binar1650z00,
		BGl_z62flushzd2binaryzd2portz62zz__binaryz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_openzd2outputzd2binaryzd2filezd2envz00zz__binaryz00,
		BgL_bgl_za762openza7d2output1651z00,
		BGl_z62openzd2outputzd2binaryzd2filezb0zz__binaryz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_outputzd2charzd2envz00zz__binaryz00,
		BgL_bgl_za762outputza7d2char1652z00, BGl_z62outputzd2charzb0zz__binaryz00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_inputzd2charzd2envz00zz__binaryz00,
		BgL_bgl_za762inputza7d2charza71653za7, BGl_z62inputzd2charzb0zz__binaryz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1628z00zz__binaryz00,
		BgL_bgl_string1628za700za7za7_1654za7,
		"/tmp/bigloo/runtime/Llib/binary.scm", 35);
	      DEFINE_STRING(BGl_string1629z00zz__binaryz00,
		BgL_bgl_string1629za700za7za7_1655za7, "&open-output-binary-file", 24);
	      DEFINE_STRING(BGl_string1630z00zz__binaryz00,
		BgL_bgl_string1630za700za7za7_1656za7, "bstring", 7);
	      DEFINE_STRING(BGl_string1631z00zz__binaryz00,
		BgL_bgl_string1631za700za7za7_1657za7, "&append-output-binary-file", 26);
	      DEFINE_STRING(BGl_string1632z00zz__binaryz00,
		BgL_bgl_string1632za700za7za7_1658za7, "&open-input-binary-file", 23);
	      DEFINE_STRING(BGl_string1633z00zz__binaryz00,
		BgL_bgl_string1633za700za7za7_1659za7, "&close-binary-port", 18);
	      DEFINE_STRING(BGl_string1634z00zz__binaryz00,
		BgL_bgl_string1634za700za7za7_1660za7, "binary-port", 11);
	      DEFINE_STRING(BGl_string1635z00zz__binaryz00,
		BgL_bgl_string1635za700za7za7_1661za7, "&flush-binary-port", 18);
	      DEFINE_STRING(BGl_string1636z00zz__binaryz00,
		BgL_bgl_string1636za700za7za7_1662za7, "&input-obj", 10);
	      DEFINE_STRING(BGl_string1637z00zz__binaryz00,
		BgL_bgl_string1637za700za7za7_1663za7, "&output-obj", 11);
	      DEFINE_STRING(BGl_string1638z00zz__binaryz00,
		BgL_bgl_string1638za700za7za7_1664za7, "&output-char", 12);
	      DEFINE_STRING(BGl_string1639z00zz__binaryz00,
		BgL_bgl_string1639za700za7za7_1665za7, "bchar", 5);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_outputzd2objzd2envz00zz__binaryz00,
		BgL_bgl_za762outputza7d2objza71666za7, BGl_z62outputzd2objzb0zz__binaryz00,
		0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1640z00zz__binaryz00,
		BgL_bgl_string1640za700za7za7_1667za7, "&output-byte", 12);
	      DEFINE_STRING(BGl_string1641z00zz__binaryz00,
		BgL_bgl_string1641za700za7za7_1668za7, "bint", 4);
	      DEFINE_STRING(BGl_string1642z00zz__binaryz00,
		BgL_bgl_string1642za700za7za7_1669za7, "&input-char", 11);
	      DEFINE_STRING(BGl_string1643z00zz__binaryz00,
		BgL_bgl_string1643za700za7za7_1670za7, "&output-string", 14);
	      DEFINE_STRING(BGl_string1644z00zz__binaryz00,
		BgL_bgl_string1644za700za7za7_1671za7, "&input-string", 13);
	      DEFINE_STRING(BGl_string1645z00zz__binaryz00,
		BgL_bgl_string1645za700za7za7_1672za7, "&input-fill-string!", 19);
	      DEFINE_STRING(BGl_string1646z00zz__binaryz00,
		BgL_bgl_string1646za700za7za7_1673za7, "__binary", 8);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_appendzd2outputzd2binaryzd2filezd2envz00zz__binaryz00,
		BgL_bgl_za762appendza7d2outp1674z00,
		BGl_z62appendzd2outputzd2binaryzd2filezb0zz__binaryz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_inputzd2fillzd2stringz12zd2envzc0zz__binaryz00,
		BgL_bgl_za762inputza7d2fillza71675za7,
		BGl_z62inputzd2fillzd2stringz12z70zz__binaryz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_inputzd2objzd2envz00zz__binaryz00,
		BgL_bgl_za762inputza7d2objza7b1676za7, BGl_z62inputzd2objzb0zz__binaryz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_closezd2binaryzd2portzd2envzd2zz__binaryz00,
		BgL_bgl_za762closeza7d2binar1677z00,
		BGl_z62closezd2binaryzd2portz62zz__binaryz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_openzd2inputzd2binaryzd2filezd2envz00zz__binaryz00,
		BgL_bgl_za762openza7d2inputza71678za7,
		BGl_z62openzd2inputzd2binaryzd2filezb0zz__binaryz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_outputzd2bytezd2envz00zz__binaryz00,
		BgL_bgl_za762outputza7d2byte1679z00, BGl_z62outputzd2bytezb0zz__binaryz00,
		0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__binaryz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__binaryz00(long
		BgL_checksumz00_1928, char *BgL_fromz00_1929)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__binaryz00))
				{
					BGl_requirezd2initializa7ationz75zz__binaryz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__binaryz00();
					BGl_importedzd2moduleszd2initz00zz__binaryz00();
					return BGl_methodzd2initzd2zz__binaryz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__binaryz00(void)
	{
		{	/* Llib/binary.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* binary-port? */
	BGL_EXPORTED_DEF bool_t BGl_binaryzd2portzf3z21zz__binaryz00(obj_t
		BgL_objz00_3)
	{
		{	/* Llib/binary.scm 138 */
			return BINARY_PORTP(BgL_objz00_3);
		}

	}



/* &binary-port? */
	obj_t BGl_z62binaryzd2portzf3z43zz__binaryz00(obj_t BgL_envz00_1859,
		obj_t BgL_objz00_1860)
	{
		{	/* Llib/binary.scm 138 */
			return BBOOL(BGl_binaryzd2portzf3z21zz__binaryz00(BgL_objz00_1860));
		}

	}



/* open-output-binary-file */
	BGL_EXPORTED_DEF obj_t BGl_openzd2outputzd2binaryzd2filezd2zz__binaryz00(obj_t
		BgL_strz00_4)
	{
		{	/* Llib/binary.scm 144 */
			BGL_TAIL return open_output_binary_file(BgL_strz00_4);
		}

	}



/* &open-output-binary-file */
	obj_t BGl_z62openzd2outputzd2binaryzd2filezb0zz__binaryz00(obj_t
		BgL_envz00_1861, obj_t BgL_strz00_1862)
	{
		{	/* Llib/binary.scm 144 */
			{	/* Llib/binary.scm 145 */
				obj_t BgL_auxz00_1941;

				if (STRINGP(BgL_strz00_1862))
					{	/* Llib/binary.scm 145 */
						BgL_auxz00_1941 = BgL_strz00_1862;
					}
				else
					{
						obj_t BgL_auxz00_1944;

						BgL_auxz00_1944 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1628z00zz__binaryz00,
							BINT(5623L), BGl_string1629z00zz__binaryz00,
							BGl_string1630z00zz__binaryz00, BgL_strz00_1862);
						FAILURE(BgL_auxz00_1944, BFALSE, BFALSE);
					}
				return
					BGl_openzd2outputzd2binaryzd2filezd2zz__binaryz00(BgL_auxz00_1941);
			}
		}

	}



/* append-output-binary-file */
	BGL_EXPORTED_DEF obj_t
		BGl_appendzd2outputzd2binaryzd2filezd2zz__binaryz00(obj_t BgL_strz00_5)
	{
		{	/* Llib/binary.scm 150 */
			BGL_TAIL return append_output_binary_file(BgL_strz00_5);
		}

	}



/* &append-output-binary-file */
	obj_t BGl_z62appendzd2outputzd2binaryzd2filezb0zz__binaryz00(obj_t
		BgL_envz00_1863, obj_t BgL_strz00_1864)
	{
		{	/* Llib/binary.scm 150 */
			{	/* Llib/binary.scm 151 */
				obj_t BgL_auxz00_1950;

				if (STRINGP(BgL_strz00_1864))
					{	/* Llib/binary.scm 151 */
						BgL_auxz00_1950 = BgL_strz00_1864;
					}
				else
					{
						obj_t BgL_auxz00_1953;

						BgL_auxz00_1953 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1628z00zz__binaryz00,
							BINT(5921L), BGl_string1631z00zz__binaryz00,
							BGl_string1630z00zz__binaryz00, BgL_strz00_1864);
						FAILURE(BgL_auxz00_1953, BFALSE, BFALSE);
					}
				return
					BGl_appendzd2outputzd2binaryzd2filezd2zz__binaryz00(BgL_auxz00_1950);
			}
		}

	}



/* open-input-binary-file */
	BGL_EXPORTED_DEF obj_t BGl_openzd2inputzd2binaryzd2filezd2zz__binaryz00(obj_t
		BgL_strz00_6)
	{
		{	/* Llib/binary.scm 156 */
			BGL_TAIL return open_input_binary_file(BgL_strz00_6);
		}

	}



/* &open-input-binary-file */
	obj_t BGl_z62openzd2inputzd2binaryzd2filezb0zz__binaryz00(obj_t
		BgL_envz00_1865, obj_t BgL_strz00_1866)
	{
		{	/* Llib/binary.scm 156 */
			{	/* Llib/binary.scm 157 */
				obj_t BgL_auxz00_1959;

				if (STRINGP(BgL_strz00_1866))
					{	/* Llib/binary.scm 157 */
						BgL_auxz00_1959 = BgL_strz00_1866;
					}
				else
					{
						obj_t BgL_auxz00_1962;

						BgL_auxz00_1962 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1628z00zz__binaryz00,
							BINT(6218L), BGl_string1632z00zz__binaryz00,
							BGl_string1630z00zz__binaryz00, BgL_strz00_1866);
						FAILURE(BgL_auxz00_1962, BFALSE, BFALSE);
					}
				return
					BGl_openzd2inputzd2binaryzd2filezd2zz__binaryz00(BgL_auxz00_1959);
			}
		}

	}



/* close-binary-port */
	BGL_EXPORTED_DEF obj_t BGl_closezd2binaryzd2portz00zz__binaryz00(obj_t
		BgL_portz00_7)
	{
		{	/* Llib/binary.scm 162 */
			BGL_TAIL return close_binary_port(BgL_portz00_7);
		}

	}



/* &close-binary-port */
	obj_t BGl_z62closezd2binaryzd2portz62zz__binaryz00(obj_t BgL_envz00_1867,
		obj_t BgL_portz00_1868)
	{
		{	/* Llib/binary.scm 162 */
			{	/* Llib/binary.scm 163 */
				obj_t BgL_auxz00_1968;

				if (BINARY_PORTP(BgL_portz00_1868))
					{	/* Llib/binary.scm 163 */
						BgL_auxz00_1968 = BgL_portz00_1868;
					}
				else
					{
						obj_t BgL_auxz00_1971;

						BgL_auxz00_1971 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1628z00zz__binaryz00,
							BINT(6515L), BGl_string1633z00zz__binaryz00,
							BGl_string1634z00zz__binaryz00, BgL_portz00_1868);
						FAILURE(BgL_auxz00_1971, BFALSE, BFALSE);
					}
				return BGl_closezd2binaryzd2portz00zz__binaryz00(BgL_auxz00_1968);
			}
		}

	}



/* flush-binary-port */
	BGL_EXPORTED_DEF obj_t BGl_flushzd2binaryzd2portz00zz__binaryz00(obj_t
		BgL_portz00_8)
	{
		{	/* Llib/binary.scm 168 */
			BGL_TAIL return bgl_flush_binary_port(BgL_portz00_8);
		}

	}



/* &flush-binary-port */
	obj_t BGl_z62flushzd2binaryzd2portz62zz__binaryz00(obj_t BgL_envz00_1869,
		obj_t BgL_portz00_1870)
	{
		{	/* Llib/binary.scm 168 */
			{	/* Llib/binary.scm 169 */
				obj_t BgL_auxz00_1977;

				if (BINARY_PORTP(BgL_portz00_1870))
					{	/* Llib/binary.scm 169 */
						BgL_auxz00_1977 = BgL_portz00_1870;
					}
				else
					{
						obj_t BgL_auxz00_1980;

						BgL_auxz00_1980 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1628z00zz__binaryz00,
							BINT(6808L), BGl_string1635z00zz__binaryz00,
							BGl_string1634z00zz__binaryz00, BgL_portz00_1870);
						FAILURE(BgL_auxz00_1980, BFALSE, BFALSE);
					}
				return BGl_flushzd2binaryzd2portz00zz__binaryz00(BgL_auxz00_1977);
			}
		}

	}



/* input-obj */
	BGL_EXPORTED_DEF obj_t BGl_inputzd2objzd2zz__binaryz00(obj_t BgL_portz00_9)
	{
		{	/* Llib/binary.scm 174 */
			BGL_TAIL return input_obj(BgL_portz00_9);
		}

	}



/* &input-obj */
	obj_t BGl_z62inputzd2objzb0zz__binaryz00(obj_t BgL_envz00_1871,
		obj_t BgL_portz00_1872)
	{
		{	/* Llib/binary.scm 174 */
			{	/* Llib/binary.scm 175 */
				obj_t BgL_auxz00_1986;

				if (BINARY_PORTP(BgL_portz00_1872))
					{	/* Llib/binary.scm 175 */
						BgL_auxz00_1986 = BgL_portz00_1872;
					}
				else
					{
						obj_t BgL_auxz00_1989;

						BgL_auxz00_1989 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1628z00zz__binaryz00,
							BINT(7093L), BGl_string1636z00zz__binaryz00,
							BGl_string1634z00zz__binaryz00, BgL_portz00_1872);
						FAILURE(BgL_auxz00_1989, BFALSE, BFALSE);
					}
				return BGl_inputzd2objzd2zz__binaryz00(BgL_auxz00_1986);
			}
		}

	}



/* output-obj */
	BGL_EXPORTED_DEF obj_t BGl_outputzd2objzd2zz__binaryz00(obj_t BgL_portz00_10,
		obj_t BgL_objz00_11)
	{
		{	/* Llib/binary.scm 180 */
			BGL_TAIL return output_obj(BgL_portz00_10, BgL_objz00_11);
		}

	}



/* &output-obj */
	obj_t BGl_z62outputzd2objzb0zz__binaryz00(obj_t BgL_envz00_1873,
		obj_t BgL_portz00_1874, obj_t BgL_objz00_1875)
	{
		{	/* Llib/binary.scm 180 */
			{	/* Llib/binary.scm 181 */
				obj_t BgL_auxz00_1995;

				if (BINARY_PORTP(BgL_portz00_1874))
					{	/* Llib/binary.scm 181 */
						BgL_auxz00_1995 = BgL_portz00_1874;
					}
				else
					{
						obj_t BgL_auxz00_1998;

						BgL_auxz00_1998 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1628z00zz__binaryz00,
							BINT(7375L), BGl_string1637z00zz__binaryz00,
							BGl_string1634z00zz__binaryz00, BgL_portz00_1874);
						FAILURE(BgL_auxz00_1998, BFALSE, BFALSE);
					}
				return
					BGl_outputzd2objzd2zz__binaryz00(BgL_auxz00_1995, BgL_objz00_1875);
			}
		}

	}



/* output-char */
	BGL_EXPORTED_DEF obj_t BGl_outputzd2charzd2zz__binaryz00(obj_t BgL_portz00_12,
		unsigned char BgL_charz00_13)
	{
		{	/* Llib/binary.scm 186 */
			return BGL_OUTPUT_CHAR(BgL_portz00_12, BgL_charz00_13);
		}

	}



/* &output-char */
	obj_t BGl_z62outputzd2charzb0zz__binaryz00(obj_t BgL_envz00_1876,
		obj_t BgL_portz00_1877, obj_t BgL_charz00_1878)
	{
		{	/* Llib/binary.scm 186 */
			{	/* Llib/binary.scm 187 */
				unsigned char BgL_auxz00_2011;
				obj_t BgL_auxz00_2004;

				{	/* Llib/binary.scm 187 */
					obj_t BgL_tmpz00_2012;

					if (CHARP(BgL_charz00_1878))
						{	/* Llib/binary.scm 187 */
							BgL_tmpz00_2012 = BgL_charz00_1878;
						}
					else
						{
							obj_t BgL_auxz00_2015;

							BgL_auxz00_2015 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1628z00zz__binaryz00,
								BINT(7669L), BGl_string1638z00zz__binaryz00,
								BGl_string1639z00zz__binaryz00, BgL_charz00_1878);
							FAILURE(BgL_auxz00_2015, BFALSE, BFALSE);
						}
					BgL_auxz00_2011 = CCHAR(BgL_tmpz00_2012);
				}
				if (BINARY_PORTP(BgL_portz00_1877))
					{	/* Llib/binary.scm 187 */
						BgL_auxz00_2004 = BgL_portz00_1877;
					}
				else
					{
						obj_t BgL_auxz00_2007;

						BgL_auxz00_2007 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1628z00zz__binaryz00,
							BINT(7669L), BGl_string1638z00zz__binaryz00,
							BGl_string1634z00zz__binaryz00, BgL_portz00_1877);
						FAILURE(BgL_auxz00_2007, BFALSE, BFALSE);
					}
				return
					BGl_outputzd2charzd2zz__binaryz00(BgL_auxz00_2004, BgL_auxz00_2011);
			}
		}

	}



/* output-byte */
	BGL_EXPORTED_DEF obj_t BGl_outputzd2bytezd2zz__binaryz00(obj_t BgL_portz00_14,
		char BgL_charz00_15)
	{
		{	/* Llib/binary.scm 192 */
			return BGL_OUTPUT_CHAR(BgL_portz00_14, BgL_charz00_15);
		}

	}



/* &output-byte */
	obj_t BGl_z62outputzd2bytezb0zz__binaryz00(obj_t BgL_envz00_1879,
		obj_t BgL_portz00_1880, obj_t BgL_charz00_1881)
	{
		{	/* Llib/binary.scm 192 */
			{	/* Llib/binary.scm 193 */
				char BgL_auxz00_2029;
				obj_t BgL_auxz00_2022;

				{	/* Llib/binary.scm 193 */
					obj_t BgL_tmpz00_2030;

					if (INTEGERP(BgL_charz00_1881))
						{	/* Llib/binary.scm 193 */
							BgL_tmpz00_2030 = BgL_charz00_1881;
						}
					else
						{
							obj_t BgL_auxz00_2033;

							BgL_auxz00_2033 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1628z00zz__binaryz00,
								BINT(7960L), BGl_string1640z00zz__binaryz00,
								BGl_string1641z00zz__binaryz00, BgL_charz00_1881);
							FAILURE(BgL_auxz00_2033, BFALSE, BFALSE);
						}
					BgL_auxz00_2029 = (signed char) CINT(BgL_tmpz00_2030);
				}
				if (BINARY_PORTP(BgL_portz00_1880))
					{	/* Llib/binary.scm 193 */
						BgL_auxz00_2022 = BgL_portz00_1880;
					}
				else
					{
						obj_t BgL_auxz00_2025;

						BgL_auxz00_2025 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1628z00zz__binaryz00,
							BINT(7960L), BGl_string1640z00zz__binaryz00,
							BGl_string1634z00zz__binaryz00, BgL_portz00_1880);
						FAILURE(BgL_auxz00_2025, BFALSE, BFALSE);
					}
				return
					BGl_outputzd2bytezd2zz__binaryz00(BgL_auxz00_2022, BgL_auxz00_2029);
			}
		}

	}



/* input-char */
	BGL_EXPORTED_DEF obj_t BGl_inputzd2charzd2zz__binaryz00(obj_t BgL_portz00_16)
	{
		{	/* Llib/binary.scm 198 */
			{	/* Llib/binary.scm 199 */
				int BgL_charz00_1927;

				BgL_charz00_1927 = BGL_INPUT_CHAR(BgL_portz00_16);
				if (BGL_INT_EOFP(BgL_charz00_1927))
					{	/* Llib/binary.scm 200 */
						return BEOF;
					}
				else
					{	/* Llib/binary.scm 200 */
						return BCHAR(((long) (BgL_charz00_1927)));
		}}}

	}



/* &input-char */
	obj_t BGl_z62inputzd2charzb0zz__binaryz00(obj_t BgL_envz00_1882,
		obj_t BgL_portz00_1883)
	{
		{	/* Llib/binary.scm 198 */
			{	/* Llib/binary.scm 199 */
				obj_t BgL_auxz00_2045;

				if (BINARY_PORTP(BgL_portz00_1883))
					{	/* Llib/binary.scm 199 */
						BgL_auxz00_2045 = BgL_portz00_1883;
					}
				else
					{
						obj_t BgL_auxz00_2048;

						BgL_auxz00_2048 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1628z00zz__binaryz00,
							BINT(8245L), BGl_string1642z00zz__binaryz00,
							BGl_string1634z00zz__binaryz00, BgL_portz00_1883);
						FAILURE(BgL_auxz00_2048, BFALSE, BFALSE);
					}
				return BGl_inputzd2charzd2zz__binaryz00(BgL_auxz00_2045);
			}
		}

	}



/* output-string */
	BGL_EXPORTED_DEF obj_t BGl_outputzd2stringzd2zz__binaryz00(obj_t
		BgL_portz00_17, obj_t BgL_stringz00_18)
	{
		{	/* Llib/binary.scm 207 */
			return BINT(bgl_output_string(BgL_portz00_17, BgL_stringz00_18));
		}

	}



/* &output-string */
	obj_t BGl_z62outputzd2stringzb0zz__binaryz00(obj_t BgL_envz00_1884,
		obj_t BgL_portz00_1885, obj_t BgL_stringz00_1886)
	{
		{	/* Llib/binary.scm 207 */
			{	/* Llib/binary.scm 208 */
				obj_t BgL_auxz00_2062;
				obj_t BgL_auxz00_2055;

				if (STRINGP(BgL_stringz00_1886))
					{	/* Llib/binary.scm 208 */
						BgL_auxz00_2062 = BgL_stringz00_1886;
					}
				else
					{
						obj_t BgL_auxz00_2065;

						BgL_auxz00_2065 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1628z00zz__binaryz00,
							BINT(8609L), BGl_string1643z00zz__binaryz00,
							BGl_string1630z00zz__binaryz00, BgL_stringz00_1886);
						FAILURE(BgL_auxz00_2065, BFALSE, BFALSE);
					}
				if (BINARY_PORTP(BgL_portz00_1885))
					{	/* Llib/binary.scm 208 */
						BgL_auxz00_2055 = BgL_portz00_1885;
					}
				else
					{
						obj_t BgL_auxz00_2058;

						BgL_auxz00_2058 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1628z00zz__binaryz00,
							BINT(8609L), BGl_string1643z00zz__binaryz00,
							BGl_string1634z00zz__binaryz00, BgL_portz00_1885);
						FAILURE(BgL_auxz00_2058, BFALSE, BFALSE);
					}
				return
					BGl_outputzd2stringzd2zz__binaryz00(BgL_auxz00_2055, BgL_auxz00_2062);
			}
		}

	}



/* input-string */
	BGL_EXPORTED_DEF obj_t BGl_inputzd2stringzd2zz__binaryz00(obj_t
		BgL_portz00_19, int BgL_lenz00_20)
	{
		{	/* Llib/binary.scm 213 */
			BGL_TAIL return bgl_input_string(BgL_portz00_19, BgL_lenz00_20);
		}

	}



/* &input-string */
	obj_t BGl_z62inputzd2stringzb0zz__binaryz00(obj_t BgL_envz00_1887,
		obj_t BgL_portz00_1888, obj_t BgL_lenz00_1889)
	{
		{	/* Llib/binary.scm 213 */
			{	/* Llib/binary.scm 214 */
				int BgL_auxz00_2078;
				obj_t BgL_auxz00_2071;

				{	/* Llib/binary.scm 214 */
					obj_t BgL_tmpz00_2079;

					if (INTEGERP(BgL_lenz00_1889))
						{	/* Llib/binary.scm 214 */
							BgL_tmpz00_2079 = BgL_lenz00_1889;
						}
					else
						{
							obj_t BgL_auxz00_2082;

							BgL_auxz00_2082 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1628z00zz__binaryz00,
								BINT(8904L), BGl_string1644z00zz__binaryz00,
								BGl_string1641z00zz__binaryz00, BgL_lenz00_1889);
							FAILURE(BgL_auxz00_2082, BFALSE, BFALSE);
						}
					BgL_auxz00_2078 = CINT(BgL_tmpz00_2079);
				}
				if (BINARY_PORTP(BgL_portz00_1888))
					{	/* Llib/binary.scm 214 */
						BgL_auxz00_2071 = BgL_portz00_1888;
					}
				else
					{
						obj_t BgL_auxz00_2074;

						BgL_auxz00_2074 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1628z00zz__binaryz00,
							BINT(8904L), BGl_string1644z00zz__binaryz00,
							BGl_string1634z00zz__binaryz00, BgL_portz00_1888);
						FAILURE(BgL_auxz00_2074, BFALSE, BFALSE);
					}
				return
					BGl_inputzd2stringzd2zz__binaryz00(BgL_auxz00_2071, BgL_auxz00_2078);
			}
		}

	}



/* input-fill-string! */
	BGL_EXPORTED_DEF int BGl_inputzd2fillzd2stringz12z12zz__binaryz00(obj_t
		BgL_portz00_21, obj_t BgL_strz00_22)
	{
		{	/* Llib/binary.scm 219 */
			BGL_TAIL return bgl_input_fill_string(BgL_portz00_21, BgL_strz00_22);
		}

	}



/* &input-fill-string! */
	obj_t BGl_z62inputzd2fillzd2stringz12z70zz__binaryz00(obj_t BgL_envz00_1890,
		obj_t BgL_portz00_1891, obj_t BgL_strz00_1892)
	{
		{	/* Llib/binary.scm 219 */
			{	/* Llib/binary.scm 220 */
				int BgL_tmpz00_2089;

				{	/* Llib/binary.scm 220 */
					obj_t BgL_auxz00_2097;
					obj_t BgL_auxz00_2090;

					if (STRINGP(BgL_strz00_1892))
						{	/* Llib/binary.scm 220 */
							BgL_auxz00_2097 = BgL_strz00_1892;
						}
					else
						{
							obj_t BgL_auxz00_2100;

							BgL_auxz00_2100 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1628z00zz__binaryz00,
								BINT(9201L), BGl_string1645z00zz__binaryz00,
								BGl_string1630z00zz__binaryz00, BgL_strz00_1892);
							FAILURE(BgL_auxz00_2100, BFALSE, BFALSE);
						}
					if (BINARY_PORTP(BgL_portz00_1891))
						{	/* Llib/binary.scm 220 */
							BgL_auxz00_2090 = BgL_portz00_1891;
						}
					else
						{
							obj_t BgL_auxz00_2093;

							BgL_auxz00_2093 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1628z00zz__binaryz00,
								BINT(9201L), BGl_string1645z00zz__binaryz00,
								BGl_string1634z00zz__binaryz00, BgL_portz00_1891);
							FAILURE(BgL_auxz00_2093, BFALSE, BFALSE);
						}
					BgL_tmpz00_2089 =
						BGl_inputzd2fillzd2stringz12z12zz__binaryz00(BgL_auxz00_2090,
						BgL_auxz00_2097);
				}
				return BINT(BgL_tmpz00_2089);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__binaryz00(void)
	{
		{	/* Llib/binary.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__binaryz00(void)
	{
		{	/* Llib/binary.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__binaryz00(void)
	{
		{	/* Llib/binary.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__binaryz00(void)
	{
		{	/* Llib/binary.scm 15 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1646z00zz__binaryz00));
			BGl_modulezd2initializa7ationz75zz__intextz00(6305717L,
				BSTRING_TO_STRING(BGl_string1646z00zz__binaryz00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1646z00zz__binaryz00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string1646z00zz__binaryz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string1646z00zz__binaryz00));
			return
				BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string1646z00zz__binaryz00));
		}

	}

#ifdef __cplusplus
}
#endif
