/*===========================================================================*/
/*   (Llib/foreign.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Llib/foreign.scm -indent -o objs/obj_u/Llib/foreign.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___FOREIGN_TYPE_DEFINITIONS
#define BGL___FOREIGN_TYPE_DEFINITIONS
#endif													// BGL___FOREIGN_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zz__foreignz00 = BUNSPEC;
	static obj_t BGl_z62foreignzd2eqzf3z43zz__foreignz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62makezd2stringzd2ptrzd2nullzb0zz__foreignz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_foreignzf3zf3zz__foreignz00(obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__foreignz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_EXPORTED_DECL long BGl_objzd2ze3cobjz31zz__foreignz00(obj_t);
	BGL_EXPORTED_DECL char *BGl_makezd2stringzd2ptrzd2nullzd2zz__foreignz00(void);
	static obj_t BGl_z62stringzd2ptrzd2nullzf3z91zz__foreignz00(obj_t, obj_t);
	static obj_t BGl_z62voidza2zd2nullzf3ze1zz__foreignz00(obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__foreignz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__foreignz00(void);
	static obj_t BGl_z62foreignzf3z91zz__foreignz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_foreignzd2nullzf3z21zz__foreignz00(obj_t);
	extern obj_t void_star_to_obj(void *);
	BGL_EXPORTED_DECL bool_t BGl_foreignzd2eqzf3z21zz__foreignz00(obj_t, obj_t);
	BGL_EXPORTED_DECL void *BGl_makezd2voidza2zd2nullza2zz__foreignz00(void);
	static obj_t BGl_z62makezd2voidza2zd2nullzc0zz__foreignz00(obj_t);
	static obj_t BGl_z62foreignzd2nullzf3z43zz__foreignz00(obj_t, obj_t);
	extern long obj_to_cobj(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_stringzd2ptrzd2nullzf3zf3zz__foreignz00(char *);
	BGL_EXPORTED_DECL bool_t BGl_voidza2zd2nullzf3z83zz__foreignz00(void *);
	static obj_t BGl_z62objzd2ze3cobjz53zz__foreignz00(obj_t, obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2ptrzd2nullzf3zd2envz21zz__foreignz00,
		BgL_bgl_za762stringza7d2ptrza71415za7,
		BGl_z62stringzd2ptrzd2nullzf3z91zz__foreignz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1407z00zz__foreignz00,
		BgL_bgl_string1407za700za7za7_1416za7, "foreign-null?", 13);
	      DEFINE_STRING(BGl_string1408z00zz__foreignz00,
		BgL_bgl_string1408za700za7za7_1417za7, "not a foreign object", 20);
	      DEFINE_STRING(BGl_string1409z00zz__foreignz00,
		BgL_bgl_string1409za700za7za7_1418za7,
		"/tmp/bigloo/runtime/Llib/foreign.scm", 36);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_objzd2ze3cobjzd2envze3zz__foreignz00,
		BgL_bgl_za762objza7d2za7e3cobj1419za7,
		BGl_z62objzd2ze3cobjz53zz__foreignz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1410z00zz__foreignz00,
		BgL_bgl_string1410za700za7za7_1420za7, "&string-ptr-null?", 17);
	      DEFINE_STRING(BGl_string1411z00zz__foreignz00,
		BgL_bgl_string1411za700za7za7_1421za7, "bstring", 7);
	      DEFINE_STRING(BGl_string1412z00zz__foreignz00,
		BgL_bgl_string1412za700za7za7_1422za7, "&void*-null?", 12);
	      DEFINE_STRING(BGl_string1413z00zz__foreignz00,
		BgL_bgl_string1413za700za7za7_1423za7, "void*", 5);
	      DEFINE_STRING(BGl_string1414z00zz__foreignz00,
		BgL_bgl_string1414za700za7za7_1424za7, "__foreign", 9);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_foreignzf3zd2envz21zz__foreignz00,
		BgL_bgl_za762foreignza7f3za7911425za7, BGl_z62foreignzf3z91zz__foreignz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2voidza2zd2nullzd2envz70zz__foreignz00,
		BgL_bgl_za762makeza7d2voidza7a1426za7,
		BGl_z62makezd2voidza2zd2nullzc0zz__foreignz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2stringzd2ptrzd2nullzd2envz00zz__foreignz00,
		BgL_bgl_za762makeza7d2string1427z00,
		BGl_z62makezd2stringzd2ptrzd2nullzb0zz__foreignz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_foreignzd2eqzf3zd2envzf3zz__foreignz00,
		BgL_bgl_za762foreignza7d2eqza71428za7,
		BGl_z62foreignzd2eqzf3z43zz__foreignz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_voidza2zd2nullzf3zd2envz51zz__foreignz00,
		BgL_bgl_za762voidza7a2za7d2nul1429za7,
		BGl_z62voidza2zd2nullzf3ze1zz__foreignz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_foreignzd2nullzf3zd2envzf3zz__foreignz00,
		BgL_bgl_za762foreignza7d2nul1430z00,
		BGl_z62foreignzd2nullzf3z43zz__foreignz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__foreignz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__foreignz00(long
		BgL_checksumz00_1047, char *BgL_fromz00_1048)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__foreignz00))
				{
					BGl_requirezd2initializa7ationz75zz__foreignz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__foreignz00();
					return BGl_importedzd2moduleszd2initz00zz__foreignz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__foreignz00(void)
	{
		{	/* Llib/foreign.scm 18 */
			return bgl_gc_roots_register();
		}

	}



/* foreign? */
	BGL_EXPORTED_DEF bool_t BGl_foreignzf3zf3zz__foreignz00(obj_t BgL_objz00_3)
	{
		{	/* Llib/foreign.scm 91 */
			return FOREIGNP(BgL_objz00_3);
		}

	}



/* &foreign? */
	obj_t BGl_z62foreignzf3z91zz__foreignz00(obj_t BgL_envz00_1028,
		obj_t BgL_objz00_1029)
	{
		{	/* Llib/foreign.scm 91 */
			return BBOOL(BGl_foreignzf3zf3zz__foreignz00(BgL_objz00_1029));
		}

	}



/* foreign-eq? */
	BGL_EXPORTED_DEF bool_t BGl_foreignzd2eqzf3z21zz__foreignz00(obj_t
		BgL_o1z00_4, obj_t BgL_o2z00_5)
	{
		{	/* Llib/foreign.scm 97 */
			return FOREIGN_EQP(BgL_o1z00_4, BgL_o2z00_5);
		}

	}



/* &foreign-eq? */
	obj_t BGl_z62foreignzd2eqzf3z43zz__foreignz00(obj_t BgL_envz00_1030,
		obj_t BgL_o1z00_1031, obj_t BgL_o2z00_1032)
	{
		{	/* Llib/foreign.scm 97 */
			return
				BBOOL(BGl_foreignzd2eqzf3z21zz__foreignz00(BgL_o1z00_1031,
					BgL_o2z00_1032));
		}

	}



/* foreign-null? */
	BGL_EXPORTED_DEF bool_t BGl_foreignzd2nullzf3z21zz__foreignz00(obj_t
		BgL_objz00_6)
	{
		{	/* Llib/foreign.scm 103 */
			if (FOREIGNP(BgL_objz00_6))
				{	/* Llib/foreign.scm 104 */
					return FOREIGN_NULLP(BgL_objz00_6);
				}
			else
				{	/* Llib/foreign.scm 104 */
					return
						CBOOL(BGl_errorz00zz__errorz00(BGl_string1407z00zz__foreignz00,
							BGl_string1408z00zz__foreignz00, BgL_objz00_6));
				}
		}

	}



/* &foreign-null? */
	obj_t BGl_z62foreignzd2nullzf3z43zz__foreignz00(obj_t BgL_envz00_1033,
		obj_t BgL_objz00_1034)
	{
		{	/* Llib/foreign.scm 103 */
			return BBOOL(BGl_foreignzd2nullzf3z21zz__foreignz00(BgL_objz00_1034));
		}

	}



/* string-ptr-null? */
	BGL_EXPORTED_DEF bool_t BGl_stringzd2ptrzd2nullzf3zf3zz__foreignz00(char
		*BgL_objz00_7)
	{
		{	/* Llib/foreign.scm 111 */
			return STRING_PTR_NULL(BgL_objz00_7);
		}

	}



/* &string-ptr-null? */
	obj_t BGl_z62stringzd2ptrzd2nullzf3z91zz__foreignz00(obj_t BgL_envz00_1035,
		obj_t BgL_objz00_1036)
	{
		{	/* Llib/foreign.scm 111 */
			{	/* Llib/foreign.scm 112 */
				bool_t BgL_tmpz00_1069;

				{	/* Llib/foreign.scm 112 */
					char *BgL_auxz00_1070;

					{	/* Llib/foreign.scm 112 */
						obj_t BgL_tmpz00_1071;

						if (STRINGP(BgL_objz00_1036))
							{	/* Llib/foreign.scm 112 */
								BgL_tmpz00_1071 = BgL_objz00_1036;
							}
						else
							{
								obj_t BgL_auxz00_1074;

								BgL_auxz00_1074 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1409z00zz__foreignz00, BINT(4854L),
									BGl_string1410z00zz__foreignz00,
									BGl_string1411z00zz__foreignz00, BgL_objz00_1036);
								FAILURE(BgL_auxz00_1074, BFALSE, BFALSE);
							}
						BgL_auxz00_1070 = BSTRING_TO_STRING(BgL_tmpz00_1071);
					}
					BgL_tmpz00_1069 =
						BGl_stringzd2ptrzd2nullzf3zf3zz__foreignz00(BgL_auxz00_1070);
				}
				return BBOOL(BgL_tmpz00_1069);
			}
		}

	}



/* obj->cobj */
	BGL_EXPORTED_DEF long BGl_objzd2ze3cobjz31zz__foreignz00(obj_t BgL_objz00_8)
	{
		{	/* Llib/foreign.scm 117 */
			BGL_TAIL return obj_to_cobj(BgL_objz00_8);
		}

	}



/* &obj->cobj */
	obj_t BGl_z62objzd2ze3cobjz53zz__foreignz00(obj_t BgL_envz00_1037,
		obj_t BgL_objz00_1038)
	{
		{	/* Llib/foreign.scm 117 */
			return (obj_t) (BGl_objzd2ze3cobjz31zz__foreignz00(BgL_objz00_1038));
		}

	}



/* void*-null? */
	BGL_EXPORTED_DEF bool_t BGl_voidza2zd2nullzf3z83zz__foreignz00(void
		*BgL_objz00_9)
	{
		{	/* Llib/foreign.scm 123 */
			return FOREIGN_PTR_NULL(BgL_objz00_9);
		}

	}



/* &void*-null? */
	obj_t BGl_z62voidza2zd2nullzf3ze1zz__foreignz00(obj_t BgL_envz00_1039,
		obj_t BgL_objz00_1040)
	{
		{	/* Llib/foreign.scm 123 */
			{	/* Llib/foreign.scm 124 */
				bool_t BgL_tmpz00_1085;

				{	/* Llib/foreign.scm 124 */
					void *BgL_auxz00_1086;

					if (FOREIGNP(BgL_objz00_1040))
						{	/* Llib/foreign.scm 124 */
							BgL_auxz00_1086 = FOREIGN_TO_COBJ(BgL_objz00_1040);
						}
					else
						{
							obj_t BgL_auxz00_1090;

							BgL_auxz00_1090 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1409z00zz__foreignz00,
								BINT(5421L), BGl_string1412z00zz__foreignz00,
								BGl_string1413z00zz__foreignz00, BgL_objz00_1040);
							FAILURE(BgL_auxz00_1090, BFALSE, BFALSE);
						}
					BgL_tmpz00_1085 =
						BGl_voidza2zd2nullzf3z83zz__foreignz00(BgL_auxz00_1086);
				}
				return BBOOL(BgL_tmpz00_1085);
			}
		}

	}



/* make-string-ptr-null */
	BGL_EXPORTED_DEF char *BGl_makezd2stringzd2ptrzd2nullzd2zz__foreignz00(void)
	{
		{	/* Llib/foreign.scm 129 */
			return (0L);
		}

	}



/* &make-string-ptr-null */
	obj_t BGl_z62makezd2stringzd2ptrzd2nullzb0zz__foreignz00(obj_t
		BgL_envz00_1041)
	{
		{	/* Llib/foreign.scm 129 */
			return
				string_to_bstring(BGl_makezd2stringzd2ptrzd2nullzd2zz__foreignz00());
		}

	}



/* make-void*-null */
	BGL_EXPORTED_DEF void *BGl_makezd2voidza2zd2nullza2zz__foreignz00(void)
	{
		{	/* Llib/foreign.scm 135 */
			return (0L);
		}

	}



/* &make-void*-null */
	obj_t BGl_z62makezd2voidza2zd2nullzc0zz__foreignz00(obj_t BgL_envz00_1042)
	{
		{	/* Llib/foreign.scm 135 */
			return void_star_to_obj(BGl_makezd2voidza2zd2nullza2zz__foreignz00());
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__foreignz00(void)
	{
		{	/* Llib/foreign.scm 18 */
			return
				BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1414z00zz__foreignz00));
		}

	}

#ifdef __cplusplus
}
#endif
