/*===========================================================================*/
/*   (Llib/bit.scm)                                                          */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Llib/bit.scm -indent -o objs/obj_u/Llib/bit.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___BIT_TYPE_DEFINITIONS
#define BGL___BIT_TYPE_DEFINITIONS
#endif													// BGL___BIT_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL long BGl_bitzd2andzd2zz__bitz00(long, long);
	static obj_t BGl_z62bitzd2oru8zb0zz__bitz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bitzd2andu16zb0zz__bitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL uint16_t BGl_bitzd2xoru16zd2zz__bitz00(uint16_t, uint16_t);
	static obj_t BGl_z62bitzd2xors32zb0zz__bitz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bitzd2lshs8zb0zz__bitz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bitzd2andzb0zz__bitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL long BGl_bitzd2xorzd2zz__bitz00(long, long);
	BGL_EXPORTED_DECL int8_t BGl_bitzd2ands8zd2zz__bitz00(int8_t, int8_t);
	static obj_t BGl_z62bitzd2xorzb0zz__bitz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bitzd2urshu32zb0zz__bitz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bitzd2ors64zb0zz__bitz00(obj_t, obj_t, obj_t);
	extern obj_t bgl_bignum_and(obj_t, obj_t);
	static obj_t BGl_z62bitzd2lshbxzb0zz__bitz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bitzd2rshelongzb0zz__bitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_bitzd2andbxzd2zz__bitz00(obj_t, obj_t);
	extern obj_t bgl_bignum_or(obj_t, obj_t);
	static obj_t BGl_z62bitzd2ands64zb0zz__bitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL int64_t BGl_bitzd2xors64zd2zz__bitz00(int64_t, int64_t);
	BGL_EXPORTED_DECL int32_t BGl_bitzd2ors32zd2zz__bitz00(int32_t, int32_t);
	static obj_t BGl_z62bitzd2xorllongzb0zz__bitz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bitzd2oru32zb0zz__bitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL int16_t BGl_bitzd2lshs16zd2zz__bitz00(int16_t, long);
	BGL_EXPORTED_DECL int32_t BGl_bitzd2rshs32zd2zz__bitz00(int32_t, long);
	static obj_t BGl_requirezd2initializa7ationz75zz__bitz00 = BUNSPEC;
	static obj_t BGl_z62bitzd2urshzb0zz__bitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL int32_t BGl_bitzd2nots32zd2zz__bitz00(int32_t);
	static obj_t BGl_z62bitzd2rshs16zb0zz__bitz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bitzd2nots16zb0zz__bitz00(obj_t, obj_t);
	static obj_t BGl_z62bitzd2andu32zb0zz__bitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL uint32_t BGl_bitzd2xoru32zd2zz__bitz00(uint32_t, uint32_t);
	static obj_t BGl_z62bitzd2lshu8zb0zz__bitz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bitzd2xoru16zb0zz__bitz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bitzd2orelongzb0zz__bitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL int16_t BGl_bitzd2urshs16zd2zz__bitz00(int16_t, long);
	BGL_EXPORTED_DECL uint8_t BGl_bitzd2andu8zd2zz__bitz00(uint8_t, uint8_t);
	BGL_EXPORTED_DECL long BGl_bitzd2rshelongzd2zz__bitz00(long, long);
	static obj_t BGl_z62bitzd2lshllongzb0zz__bitz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_EXPORTED_DECL uint64_t BGl_bitzd2andu64zd2zz__bitz00(uint64_t, uint64_t);
	static obj_t BGl_z62bitzd2andllongzb0zz__bitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BGL_LONGLONG_T
		BGl_bitzd2xorllongzd2zz__bitz00(BGL_LONGLONG_T, BGL_LONGLONG_T);
	static obj_t BGl_z62bitzd2xors64zb0zz__bitz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bitzd2urshelongzb0zz__bitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL uint16_t BGl_bitzd2oru16zd2zz__bitz00(uint16_t, uint16_t);
	BGL_EXPORTED_DECL int32_t BGl_bitzd2lshs32zd2zz__bitz00(int32_t, long);
	BGL_EXPORTED_DECL obj_t BGl_bitzd2maskbxzd2zz__bitz00(obj_t, long);
	static obj_t BGl_z62bitzd2lshs16zb0zz__bitz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bitzd2urshu64zb0zz__bitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL int16_t BGl_bitzd2rshu16zd2zz__bitz00(int16_t, long);
	static obj_t BGl_z62bitzd2rshs32zb0zz__bitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL uint16_t BGl_bitzd2notu16zd2zz__bitz00(uint16_t);
	static obj_t BGl_z62bitzd2nots32zb0zz__bitz00(obj_t, obj_t);
	BGL_EXPORTED_DECL int8_t BGl_bitzd2lshs8zd2zz__bitz00(int8_t, long);
	extern obj_t bgl_bignum_lsh(obj_t, long);
	static obj_t BGl_z62bitzd2xoru32zb0zz__bitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL int32_t BGl_bitzd2urshs32zd2zz__bitz00(int32_t, long);
	BGL_EXPORTED_DECL int64_t BGl_bitzd2ors64zd2zz__bitz00(int64_t, int64_t);
	BGL_EXPORTED_DECL obj_t BGl_bitzd2lshbxzd2zz__bitz00(obj_t, long);
	static obj_t BGl_z62bitzd2nots8zb0zz__bitz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BGL_LONGLONG_T
		BGl_bitzd2lshllongzd2zz__bitz00(BGL_LONGLONG_T, long);
	static obj_t BGl_z62bitzd2oru64zb0zz__bitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL int64_t BGl_bitzd2rshs64zd2zz__bitz00(int64_t, long);
	BGL_EXPORTED_DECL int64_t BGl_bitzd2nots64zd2zz__bitz00(int64_t);
	BGL_EXPORTED_DECL BGL_LONGLONG_T
		BGl_bitzd2andllongzd2zz__bitz00(BGL_LONGLONG_T, BGL_LONGLONG_T);
	static obj_t BGl_z62bitzd2notbxzb0zz__bitz00(obj_t, obj_t);
	static obj_t BGl_z62bitzd2notllongzb0zz__bitz00(obj_t, obj_t);
	static obj_t BGl_z62bitzd2xorelongzb0zz__bitz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bitzd2andu64zb0zz__bitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL uint64_t BGl_bitzd2xoru64zd2zz__bitz00(uint64_t, uint64_t);
	BGL_EXPORTED_DECL uint32_t BGl_bitzd2oru32zd2zz__bitz00(uint32_t, uint32_t);
	BGL_EXPORTED_DECL uint16_t BGl_bitzd2lshu16zd2zz__bitz00(uint16_t, long);
	static obj_t BGl_z62bitzd2lshs32zb0zz__bitz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bitzd2orzb0zz__bitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL uint32_t BGl_bitzd2rshu32zd2zz__bitz00(uint32_t, long);
	BGL_EXPORTED_DECL uint32_t BGl_bitzd2notu32zd2zz__bitz00(uint32_t);
	static obj_t BGl_z62bitzd2maskbxzb0zz__bitz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bitzd2rshu16zb0zz__bitz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bitzd2notu16zb0zz__bitz00(obj_t, obj_t);
	BGL_EXPORTED_DECL uint8_t BGl_bitzd2lshu8zd2zz__bitz00(uint8_t, long);
	BGL_EXPORTED_DECL BGL_LONGLONG_T
		BGl_bitzd2orllongzd2zz__bitz00(BGL_LONGLONG_T, BGL_LONGLONG_T);
	BGL_EXPORTED_DECL uint16_t BGl_bitzd2urshu16zd2zz__bitz00(uint16_t, long);
	static obj_t BGl_importedzd2moduleszd2initz00zz__bitz00(void);
	static obj_t BGl_z62bitzd2lshelongzb0zz__bitz00(obj_t, obj_t, obj_t);
	static obj_t BGl_gczd2rootszd2initz00zz__bitz00(void);
	static obj_t BGl_z62bitzd2notu8zb0zz__bitz00(obj_t, obj_t);
	BGL_EXPORTED_DECL int64_t BGl_bitzd2lshs64zd2zz__bitz00(int64_t, long);
	BGL_EXPORTED_DECL BGL_LONGLONG_T
		BGl_bitzd2notllongzd2zz__bitz00(BGL_LONGLONG_T);
	static obj_t BGl_z62bitzd2andelongzb0zz__bitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL long BGl_bitzd2xorelongzd2zz__bitz00(long, long);
	static obj_t BGl_z62bitzd2rshs64zb0zz__bitz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bitzd2nots64zb0zz__bitz00(obj_t, obj_t);
	static obj_t BGl_z62bitzd2xoru64zb0zz__bitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL int64_t BGl_bitzd2urshs64zd2zz__bitz00(int64_t, long);
	BGL_EXPORTED_DECL uint32_t BGl_bitzd2lshu32zd2zz__bitz00(uint32_t, long);
	static obj_t BGl_z62bitzd2lshu16zb0zz__bitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL int8_t BGl_bitzd2ors8zd2zz__bitz00(int8_t, int8_t);
	static obj_t BGl_z62bitzd2rshu32zb0zz__bitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL long BGl_bitzd2rshzd2zz__bitz00(long, long);
	static obj_t BGl_z62bitzd2notu32zb0zz__bitz00(obj_t, obj_t);
	static obj_t BGl_z62bitzd2rshzb0zz__bitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_bitzd2orbxzd2zz__bitz00(obj_t, obj_t);
	BGL_EXPORTED_DECL long BGl_bitzd2lshelongzd2zz__bitz00(long, long);
	BGL_EXPORTED_DECL uint32_t BGl_bitzd2urshu32zd2zz__bitz00(uint32_t, long);
	BGL_EXPORTED_DECL int8_t BGl_bitzd2nots8zd2zz__bitz00(int8_t);
	static obj_t BGl_z62bitzd2rshs8zb0zz__bitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL uint64_t BGl_bitzd2oru64zd2zz__bitz00(uint64_t, uint64_t);
	extern obj_t bgl_bignum_not(obj_t);
	BGL_EXPORTED_DECL long BGl_bitzd2andelongzd2zz__bitz00(long, long);
	static obj_t BGl_z62bitzd2lshs64zb0zz__bitz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bitzd2xors8zb0zz__bitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_bitzd2notbxzd2zz__bitz00(obj_t);
	static obj_t BGl_z62bitzd2notelongzb0zz__bitz00(obj_t, obj_t);
	BGL_EXPORTED_DECL uint64_t BGl_bitzd2rshu64zd2zz__bitz00(uint64_t, long);
	static obj_t BGl_z62bitzd2rshbxzb0zz__bitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL uint64_t BGl_bitzd2notu64zd2zz__bitz00(uint64_t);
	static obj_t BGl_z62bitzd2xorbxzb0zz__bitz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bitzd2lshu32zb0zz__bitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL int8_t BGl_bitzd2urshs8zd2zz__bitz00(int8_t, long);
	BGL_EXPORTED_DECL int16_t BGl_bitzd2ands16zd2zz__bitz00(int16_t, int16_t);
	BGL_EXPORTED_DECL uint8_t BGl_bitzd2oru8zd2zz__bitz00(uint8_t, uint8_t);
	BGL_EXPORTED_DECL long BGl_bitzd2orelongzd2zz__bitz00(long, long);
	extern obj_t bgl_bignum_mask(obj_t, long);
	BGL_EXPORTED_DECL uint8_t BGl_bitzd2notu8zd2zz__bitz00(uint8_t);
	static obj_t BGl_z62bitzd2rshu8zb0zz__bitz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bitzd2urshs16zb0zz__bitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL uint64_t BGl_bitzd2lshu64zd2zz__bitz00(uint64_t, long);
	BGL_EXPORTED_DECL long BGl_bitzd2notelongzd2zz__bitz00(long);
	static obj_t BGl_z62bitzd2xoru8zb0zz__bitz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bitzd2rshu64zb0zz__bitz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bitzd2notu64zb0zz__bitz00(obj_t, obj_t);
	BGL_EXPORTED_DECL unsigned BGL_LONGLONG_T
		BGl_bitzd2urshllongzd2zz__bitz00(unsigned BGL_LONGLONG_T, long);
	static obj_t BGl_z62bitzd2ors16zb0zz__bitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL uint64_t BGl_bitzd2urshu64zd2zz__bitz00(uint64_t, long);
	BGL_EXPORTED_DECL long BGl_bitzd2orzd2zz__bitz00(long, long);
	BGL_EXPORTED_DECL unsigned long BGl_bitzd2urshzd2zz__bitz00(unsigned long,
		long);
	BGL_EXPORTED_DECL long BGl_bitzd2lshzd2zz__bitz00(long, long);
	BGL_EXPORTED_DECL int32_t BGl_bitzd2ands32zd2zz__bitz00(int32_t, int32_t);
	static obj_t BGl_z62bitzd2lshzb0zz__bitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL uint8_t BGl_bitzd2urshu8zd2zz__bitz00(uint8_t, long);
	BGL_EXPORTED_DECL long BGl_bitzd2notzd2zz__bitz00(long);
	static obj_t BGl_z62bitzd2urshs8zb0zz__bitz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bitzd2ands16zb0zz__bitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL int16_t BGl_bitzd2xors16zd2zz__bitz00(int16_t, int16_t);
	static obj_t BGl_z62bitzd2notzb0zz__bitz00(obj_t, obj_t);
	BGL_EXPORTED_DECL int8_t BGl_bitzd2rshs8zd2zz__bitz00(int8_t, long);
	extern obj_t bgl_bignum_rsh(obj_t, long);
	static obj_t BGl_z62bitzd2urshs32zb0zz__bitz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bitzd2ands8zb0zz__bitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL int8_t BGl_bitzd2xors8zd2zz__bitz00(int8_t, int8_t);
	BGL_EXPORTED_DECL obj_t BGl_bitzd2rshbxzd2zz__bitz00(obj_t, long);
	extern obj_t bgl_bignum_xor(obj_t, obj_t);
	static obj_t BGl_z62bitzd2lshu64zb0zz__bitz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bitzd2rshllongzb0zz__bitz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bitzd2andbxzb0zz__bitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_bitzd2xorbxzd2zz__bitz00(obj_t, obj_t);
	static obj_t BGl_z62bitzd2ors32zb0zz__bitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL uint16_t BGl_bitzd2andu16zd2zz__bitz00(uint16_t, uint16_t);
	static obj_t BGl_z62bitzd2ands32zb0zz__bitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL int32_t BGl_bitzd2xors32zd2zz__bitz00(int32_t, int32_t);
	static obj_t BGl_z62bitzd2ors8zb0zz__bitz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bitzd2urshu8zb0zz__bitz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bitzd2xors16zb0zz__bitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL uint8_t BGl_bitzd2rshu8zd2zz__bitz00(uint8_t, long);
	static obj_t BGl_z62bitzd2orbxzb0zz__bitz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bitzd2orllongzb0zz__bitz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bitzd2urshu16zb0zz__bitz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bitzd2andu8zb0zz__bitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL uint8_t BGl_bitzd2xoru8zd2zz__bitz00(uint8_t, uint8_t);
	BGL_EXPORTED_DECL BGL_LONGLONG_T
		BGl_bitzd2rshllongzd2zz__bitz00(BGL_LONGLONG_T, long);
	BGL_EXPORTED_DECL int64_t BGl_bitzd2ands64zd2zz__bitz00(int64_t, int64_t);
	BGL_EXPORTED_DECL unsigned long BGl_bitzd2urshelongzd2zz__bitz00(unsigned
		long, long);
	BGL_EXPORTED_DECL int16_t BGl_bitzd2ors16zd2zz__bitz00(int16_t, int16_t);
	static obj_t BGl_z62bitzd2oru16zb0zz__bitz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bitzd2urshllongzb0zz__bitz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bitzd2urshs64zb0zz__bitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL int16_t BGl_bitzd2rshs16zd2zz__bitz00(int16_t, long);
	BGL_EXPORTED_DECL int16_t BGl_bitzd2nots16zd2zz__bitz00(int16_t);
	BGL_EXPORTED_DECL uint32_t BGl_bitzd2andu32zd2zz__bitz00(uint32_t, uint32_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2oru32zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2oru32za7b1785za7, BGl_z62bitzd2oru32zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2lshu16zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2lshu16za71786za7, BGl_z62bitzd2lshu16zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2rshu32zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2rshu32za71787za7, BGl_z62bitzd2rshu32zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2notu32zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2notu32za71788za7, BGl_z62bitzd2notu32zb0zz__bitz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2oru8zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2oru8za7b01789za7, BGl_z62bitzd2oru8zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2xors32zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2xors32za71790za7, BGl_z62bitzd2xors32zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2orelongzd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2orelong1791z00, BGl_z62bitzd2orelongzb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2urshs16zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2urshs161792z00, BGl_z62bitzd2urshs16zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2notu8zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2notu8za7b1793za7, BGl_z62bitzd2notu8zb0zz__bitz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2andu8zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2andu8za7b1794za7, BGl_z62bitzd2andu8zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2lshu64zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2lshu64za71795za7, BGl_z62bitzd2lshu64zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2notelongzd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2notelon1796z00, BGl_z62bitzd2notelongzb0zz__bitz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2rshllongzd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2rshllon1797z00, BGl_z62bitzd2rshllongzb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2urshllongzd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2urshllo1798z00, BGl_z62bitzd2urshllongzb0zz__bitz00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2urshs64zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2urshs641799z00, BGl_z62bitzd2urshs64zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2urshzd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2urshza7b01800za7, BGl_z62bitzd2urshzb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2andu32zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2andu32za71801za7, BGl_z62bitzd2andu32zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2xoru16zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2xoru16za71802za7, BGl_z62bitzd2xoru16zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2xorzd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2xorza7b0za71803z00, BGl_z62bitzd2xorzb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2orbxzd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2orbxza7b01804za7, BGl_z62bitzd2orbxzb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2notbxzd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2notbxza7b1805za7, BGl_z62bitzd2notbxzb0zz__bitz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2andbxzd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2andbxza7b1806za7, BGl_z62bitzd2andbxzb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2xoru64zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2xoru64za71807za7, BGl_z62bitzd2xoru64zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2ors32zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2ors32za7b1808za7, BGl_z62bitzd2ors32zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2lshs16zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2lshs16za71809za7, BGl_z62bitzd2lshs16zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2rshs32zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2rshs32za71810za7, BGl_z62bitzd2rshs32zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2nots32zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2nots32za71811za7, BGl_z62bitzd2nots32zb0zz__bitz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2lshu8zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2lshu8za7b1812za7, BGl_z62bitzd2lshu8zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2rshu8zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2rshu8za7b1813za7, BGl_z62bitzd2rshu8zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2lshs64zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2lshs64za71814za7, BGl_z62bitzd2lshs64zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2xoru8zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2xoru8za7b1815za7, BGl_z62bitzd2xoru8zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2xorllongzd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2xorllon1816z00, BGl_z62bitzd2xorllongzb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2oru16zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2oru16za7b1817za7, BGl_z62bitzd2oru16zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2lshzd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2lshza7b0za71818z00, BGl_z62bitzd2lshzb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2rshu16zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2rshu16za71819za7, BGl_z62bitzd2rshu16zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2ands32zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2ands32za71820za7, BGl_z62bitzd2ands32zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2ors8zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2ors8za7b01821za7, BGl_z62bitzd2ors8zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2notu16zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2notu16za71822za7, BGl_z62bitzd2notu16zb0zz__bitz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2urshu8zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2urshu8za71823za7, BGl_z62bitzd2urshu8zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2xors16zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2xors16za71824za7, BGl_z62bitzd2xors16zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2lshelongzd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2lshelon1825z00, BGl_z62bitzd2lshelongzb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2urshu32zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2urshu321826z00, BGl_z62bitzd2urshu32zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2nots8zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2nots8za7b1827za7, BGl_z62bitzd2nots8zb0zz__bitz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1700z00zz__bitz00,
		BgL_bgl_string1700za700za7za7_1828za7, "&bit-oru16", 10);
	      DEFINE_STRING(BGl_string1701z00zz__bitz00,
		BgL_bgl_string1701za700za7za7_1829za7, "buint16", 7);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2ands8zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2ands8za7b1830za7, BGl_z62bitzd2ands8zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1702z00zz__bitz00,
		BgL_bgl_string1702za700za7za7_1831za7, "&bit-ors32", 10);
	      DEFINE_STRING(BGl_string1703z00zz__bitz00,
		BgL_bgl_string1703za700za7za7_1832za7, "bint32", 6);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2oru64zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2oru64za7b1833za7, BGl_z62bitzd2oru64zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1704z00zz__bitz00,
		BgL_bgl_string1704za700za7za7_1834za7, "&bit-oru32", 10);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2lshbxzd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2lshbxza7b1835za7, BGl_z62bitzd2lshbxzb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1705z00zz__bitz00,
		BgL_bgl_string1705za700za7za7_1836za7, "buint32", 7);
	      DEFINE_STRING(BGl_string1706z00zz__bitz00,
		BgL_bgl_string1706za700za7za7_1837za7, "&bit-ors64", 10);
	      DEFINE_STRING(BGl_string1707z00zz__bitz00,
		BgL_bgl_string1707za700za7za7_1838za7, "bint64", 6);
	      DEFINE_STRING(BGl_string1708z00zz__bitz00,
		BgL_bgl_string1708za700za7za7_1839za7, "&bit-oru64", 10);
	      DEFINE_STRING(BGl_string1709z00zz__bitz00,
		BgL_bgl_string1709za700za7za7_1840za7, "buint64", 7);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2rshbxzd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2rshbxza7b1841za7, BGl_z62bitzd2rshbxzb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2andelongzd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2andelon1842z00, BGl_z62bitzd2andelongzb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2rshu64zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2rshu64za71843za7, BGl_z62bitzd2rshu64zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2notu64zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2notu64za71844za7, BGl_z62bitzd2notu64zb0zz__bitz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2xorbxzd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2xorbxza7b1845za7, BGl_z62bitzd2xorbxzb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2xors64zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2xors64za71846za7, BGl_z62bitzd2xors64zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1710z00zz__bitz00,
		BgL_bgl_string1710za700za7za7_1847za7, "&bit-orbx", 9);
	      DEFINE_STRING(BGl_string1711z00zz__bitz00,
		BgL_bgl_string1711za700za7za7_1848za7, "bignum", 6);
	      DEFINE_STRING(BGl_string1712z00zz__bitz00,
		BgL_bgl_string1712za700za7za7_1849za7, "&bit-and", 8);
	      DEFINE_STRING(BGl_string1713z00zz__bitz00,
		BgL_bgl_string1713za700za7za7_1850za7, "&bit-andelong", 13);
	      DEFINE_STRING(BGl_string1714z00zz__bitz00,
		BgL_bgl_string1714za700za7za7_1851za7, "&bit-andllong", 13);
	      DEFINE_STRING(BGl_string1715z00zz__bitz00,
		BgL_bgl_string1715za700za7za7_1852za7, "&bit-ands8", 10);
	      DEFINE_STRING(BGl_string1716z00zz__bitz00,
		BgL_bgl_string1716za700za7za7_1853za7, "&bit-andu8", 10);
	      DEFINE_STRING(BGl_string1717z00zz__bitz00,
		BgL_bgl_string1717za700za7za7_1854za7, "&bit-ands16", 11);
	      DEFINE_STRING(BGl_string1718z00zz__bitz00,
		BgL_bgl_string1718za700za7za7_1855za7, "&bit-andu16", 11);
	      DEFINE_STRING(BGl_string1719z00zz__bitz00,
		BgL_bgl_string1719za700za7za7_1856za7, "&bit-ands32", 11);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2andu16zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2andu16za71857za7, BGl_z62bitzd2andu16zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2orllongzd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2orllong1858z00, BGl_z62bitzd2orllongzb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1720z00zz__bitz00,
		BgL_bgl_string1720za700za7za7_1859za7, "&bit-andu32", 11);
	      DEFINE_STRING(BGl_string1721z00zz__bitz00,
		BgL_bgl_string1721za700za7za7_1860za7, "&bit-ands64", 11);
	      DEFINE_STRING(BGl_string1722z00zz__bitz00,
		BgL_bgl_string1722za700za7za7_1861za7, "&bit-andu64", 11);
	      DEFINE_STRING(BGl_string1723z00zz__bitz00,
		BgL_bgl_string1723za700za7za7_1862za7, "&bit-andbx", 10);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2rshelongzd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2rshelon1863z00, BGl_z62bitzd2rshelongzb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1724z00zz__bitz00,
		BgL_bgl_string1724za700za7za7_1864za7, "&bit-maskbx", 11);
	      DEFINE_STRING(BGl_string1725z00zz__bitz00,
		BgL_bgl_string1725za700za7za7_1865za7, "&bit-xor", 8);
	      DEFINE_STRING(BGl_string1726z00zz__bitz00,
		BgL_bgl_string1726za700za7za7_1866za7, "&bit-xorelong", 13);
	      DEFINE_STRING(BGl_string1727z00zz__bitz00,
		BgL_bgl_string1727za700za7za7_1867za7, "&bit-xorllong", 13);
	      DEFINE_STRING(BGl_string1728z00zz__bitz00,
		BgL_bgl_string1728za700za7za7_1868za7, "&bit-xors8", 10);
	      DEFINE_STRING(BGl_string1729z00zz__bitz00,
		BgL_bgl_string1729za700za7za7_1869za7, "&bit-xoru8", 10);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2notllongzd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2notllon1870z00, BGl_z62bitzd2notllongzb0zz__bitz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2andu64zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2andu64za71871za7, BGl_z62bitzd2andu64zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2urshelongzd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2urshelo1872z00, BGl_z62bitzd2urshelongzb0zz__bitz00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2ors16zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2ors16za7b1873za7, BGl_z62bitzd2ors16zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1730z00zz__bitz00,
		BgL_bgl_string1730za700za7za7_1874za7, "&bit-xors16", 11);
	      DEFINE_STRING(BGl_string1731z00zz__bitz00,
		BgL_bgl_string1731za700za7za7_1875za7, "&bit-xoru16", 11);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2lshu32zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2lshu32za71876za7, BGl_z62bitzd2lshu32zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1732z00zz__bitz00,
		BgL_bgl_string1732za700za7za7_1877za7, "&bit-xors32", 11);
	      DEFINE_STRING(BGl_string1733z00zz__bitz00,
		BgL_bgl_string1733za700za7za7_1878za7, "&bit-xoru32", 11);
	      DEFINE_STRING(BGl_string1734z00zz__bitz00,
		BgL_bgl_string1734za700za7za7_1879za7, "&bit-xors64", 11);
	      DEFINE_STRING(BGl_string1735z00zz__bitz00,
		BgL_bgl_string1735za700za7za7_1880za7, "&bit-xoru64", 11);
	      DEFINE_STRING(BGl_string1736z00zz__bitz00,
		BgL_bgl_string1736za700za7za7_1881za7, "&bit-xorbx", 10);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2maskbxzd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2maskbxza71882za7, BGl_z62bitzd2maskbxzb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1737z00zz__bitz00,
		BgL_bgl_string1737za700za7za7_1883za7, "&bit-not", 8);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2rshs16zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2rshs16za71884za7, BGl_z62bitzd2rshs16zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1738z00zz__bitz00,
		BgL_bgl_string1738za700za7za7_1885za7, "&bit-notelong", 13);
	      DEFINE_STRING(BGl_string1739z00zz__bitz00,
		BgL_bgl_string1739za700za7za7_1886za7, "&bit-notllong", 13);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2nots16zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2nots16za71887za7, BGl_z62bitzd2nots16zb0zz__bitz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2rshzd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2rshza7b0za71888z00, BGl_z62bitzd2rshzb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2lshs8zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2lshs8za7b1889za7, BGl_z62bitzd2lshs8zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2andzd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2andza7b0za71890z00, BGl_z62bitzd2andzb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2rshs8zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2rshs8za7b1891za7, BGl_z62bitzd2rshs8zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2urshs32zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2urshs321892z00, BGl_z62bitzd2urshs32zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2ors64zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2ors64za7b1893za7, BGl_z62bitzd2ors64zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1740z00zz__bitz00,
		BgL_bgl_string1740za700za7za7_1894za7, "&bit-nots8", 10);
	      DEFINE_STRING(BGl_string1741z00zz__bitz00,
		BgL_bgl_string1741za700za7za7_1895za7, "&bit-notu8", 10);
	      DEFINE_STRING(BGl_string1742z00zz__bitz00,
		BgL_bgl_string1742za700za7za7_1896za7, "&bit-nots16", 11);
	      DEFINE_STRING(BGl_string1743z00zz__bitz00,
		BgL_bgl_string1743za700za7za7_1897za7, "&bit-notu16", 11);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2xors8zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2xors8za7b1898za7, BGl_z62bitzd2xors8zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1744z00zz__bitz00,
		BgL_bgl_string1744za700za7za7_1899za7, "&bit-nots32", 11);
	      DEFINE_STRING(BGl_string1745z00zz__bitz00,
		BgL_bgl_string1745za700za7za7_1900za7, "&bit-notu32", 11);
	      DEFINE_STRING(BGl_string1746z00zz__bitz00,
		BgL_bgl_string1746za700za7za7_1901za7, "&bit-nots64", 11);
	      DEFINE_STRING(BGl_string1747z00zz__bitz00,
		BgL_bgl_string1747za700za7za7_1902za7, "&bit-notu64", 11);
	      DEFINE_STRING(BGl_string1748z00zz__bitz00,
		BgL_bgl_string1748za700za7za7_1903za7, "&bit-notbx", 10);
	      DEFINE_STRING(BGl_string1749z00zz__bitz00,
		BgL_bgl_string1749za700za7za7_1904za7, "&bit-rsh", 8);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2rshs64zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2rshs64za71905za7, BGl_z62bitzd2rshs64zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2nots64zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2nots64za71906za7, BGl_z62bitzd2nots64zb0zz__bitz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1750z00zz__bitz00,
		BgL_bgl_string1750za700za7za7_1907za7, "&bit-rshelong", 13);
	      DEFINE_STRING(BGl_string1751z00zz__bitz00,
		BgL_bgl_string1751za700za7za7_1908za7, "&bit-rshllong", 13);
	      DEFINE_STRING(BGl_string1752z00zz__bitz00,
		BgL_bgl_string1752za700za7za7_1909za7, "&bit-rshs8", 10);
	      DEFINE_STRING(BGl_string1753z00zz__bitz00,
		BgL_bgl_string1753za700za7za7_1910za7, "&bit-rshu8", 10);
	      DEFINE_STRING(BGl_string1754z00zz__bitz00,
		BgL_bgl_string1754za700za7za7_1911za7, "&bit-rshs16", 11);
	      DEFINE_STRING(BGl_string1755z00zz__bitz00,
		BgL_bgl_string1755za700za7za7_1912za7, "&bit-rshu16", 11);
	      DEFINE_STRING(BGl_string1756z00zz__bitz00,
		BgL_bgl_string1756za700za7za7_1913za7, "&bit-rshs32", 11);
	      DEFINE_STRING(BGl_string1757z00zz__bitz00,
		BgL_bgl_string1757za700za7za7_1914za7, "&bit-rshu32", 11);
	      DEFINE_STRING(BGl_string1758z00zz__bitz00,
		BgL_bgl_string1758za700za7za7_1915za7, "&bit-rshs64", 11);
	      DEFINE_STRING(BGl_string1759z00zz__bitz00,
		BgL_bgl_string1759za700za7za7_1916za7, "&bit-rshu64", 11);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2urshs8zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2urshs8za71917za7, BGl_z62bitzd2urshs8zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2ands16zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2ands16za71918za7, BGl_z62bitzd2ands16zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2xoru32zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2xoru32za71919za7, BGl_z62bitzd2xoru32zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2urshu16zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2urshu161920z00, BGl_z62bitzd2urshu16zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1760z00zz__bitz00,
		BgL_bgl_string1760za700za7za7_1921za7, "&bit-rshbx", 10);
	      DEFINE_STRING(BGl_string1761z00zz__bitz00,
		BgL_bgl_string1761za700za7za7_1922za7, "&bit-ursh", 9);
	      DEFINE_STRING(BGl_string1762z00zz__bitz00,
		BgL_bgl_string1762za700za7za7_1923za7, "&bit-urshelong", 14);
	      DEFINE_STRING(BGl_string1763z00zz__bitz00,
		BgL_bgl_string1763za700za7za7_1924za7, "&bit-urshllong", 14);
	      DEFINE_STRING(BGl_string1764z00zz__bitz00,
		BgL_bgl_string1764za700za7za7_1925za7, "&bit-urshu8", 11);
	      DEFINE_STRING(BGl_string1765z00zz__bitz00,
		BgL_bgl_string1765za700za7za7_1926za7, "&bit-urshs8", 11);
	      DEFINE_STRING(BGl_string1766z00zz__bitz00,
		BgL_bgl_string1766za700za7za7_1927za7, "&bit-urshs16", 12);
	      DEFINE_STRING(BGl_string1767z00zz__bitz00,
		BgL_bgl_string1767za700za7za7_1928za7, "&bit-urshu16", 12);
	      DEFINE_STRING(BGl_string1768z00zz__bitz00,
		BgL_bgl_string1768za700za7za7_1929za7, "&bit-urshs32", 12);
	      DEFINE_STRING(BGl_string1687z00zz__bitz00,
		BgL_bgl_string1687za700za7za7_1930za7, "/tmp/bigloo/runtime/Llib/bit.scm",
		32);
	      DEFINE_STRING(BGl_string1769z00zz__bitz00,
		BgL_bgl_string1769za700za7za7_1931za7, "&bit-urshu32", 12);
	      DEFINE_STRING(BGl_string1688z00zz__bitz00,
		BgL_bgl_string1688za700za7za7_1932za7, "&bit-or", 7);
	      DEFINE_STRING(BGl_string1689z00zz__bitz00,
		BgL_bgl_string1689za700za7za7_1933za7, "bint", 4);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2xorelongzd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2xorelon1934z00, BGl_z62bitzd2xorelongzb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2ands64zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2ands64za71935za7, BGl_z62bitzd2ands64zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2lshs32zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2lshs32za71936za7, BGl_z62bitzd2lshs32zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2urshu64zd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2urshu641937z00, BGl_z62bitzd2urshu64zb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2orzd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2orza7b0za7za71938za7, BGl_z62bitzd2orzb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1770z00zz__bitz00,
		BgL_bgl_string1770za700za7za7_1939za7, "&bit-urshs64", 12);
	      DEFINE_STRING(BGl_string1771z00zz__bitz00,
		BgL_bgl_string1771za700za7za7_1940za7, "&bit-urshu64", 12);
	      DEFINE_STRING(BGl_string1690z00zz__bitz00,
		BgL_bgl_string1690za700za7za7_1941za7, "&bit-orelong", 12);
	      DEFINE_STRING(BGl_string1772z00zz__bitz00,
		BgL_bgl_string1772za700za7za7_1942za7, "&bit-lsh", 8);
	      DEFINE_STRING(BGl_string1691z00zz__bitz00,
		BgL_bgl_string1691za700za7za7_1943za7, "belong", 6);
	      DEFINE_STRING(BGl_string1773z00zz__bitz00,
		BgL_bgl_string1773za700za7za7_1944za7, "&bit-lshelong", 13);
	      DEFINE_STRING(BGl_string1692z00zz__bitz00,
		BgL_bgl_string1692za700za7za7_1945za7, "&bit-orllong", 12);
	      DEFINE_STRING(BGl_string1774z00zz__bitz00,
		BgL_bgl_string1774za700za7za7_1946za7, "&bit-lshllong", 13);
	      DEFINE_STRING(BGl_string1693z00zz__bitz00,
		BgL_bgl_string1693za700za7za7_1947za7, "bllong", 6);
	      DEFINE_STRING(BGl_string1775z00zz__bitz00,
		BgL_bgl_string1775za700za7za7_1948za7, "&bit-lshs8", 10);
	      DEFINE_STRING(BGl_string1694z00zz__bitz00,
		BgL_bgl_string1694za700za7za7_1949za7, "&bit-ors8", 9);
	      DEFINE_STRING(BGl_string1776z00zz__bitz00,
		BgL_bgl_string1776za700za7za7_1950za7, "&bit-lshu8", 10);
	      DEFINE_STRING(BGl_string1695z00zz__bitz00,
		BgL_bgl_string1695za700za7za7_1951za7, "bint8", 5);
	      DEFINE_STRING(BGl_string1777z00zz__bitz00,
		BgL_bgl_string1777za700za7za7_1952za7, "&bit-lshs16", 11);
	      DEFINE_STRING(BGl_string1696z00zz__bitz00,
		BgL_bgl_string1696za700za7za7_1953za7, "&bit-oru8", 9);
	      DEFINE_STRING(BGl_string1778z00zz__bitz00,
		BgL_bgl_string1778za700za7za7_1954za7, "&bit-lshu16", 11);
	      DEFINE_STRING(BGl_string1697z00zz__bitz00,
		BgL_bgl_string1697za700za7za7_1955za7, "buint8", 6);
	      DEFINE_STRING(BGl_string1779z00zz__bitz00,
		BgL_bgl_string1779za700za7za7_1956za7, "&bit-lshs32", 11);
	      DEFINE_STRING(BGl_string1698z00zz__bitz00,
		BgL_bgl_string1698za700za7za7_1957za7, "&bit-ors16", 10);
	      DEFINE_STRING(BGl_string1699z00zz__bitz00,
		BgL_bgl_string1699za700za7za7_1958za7, "bint16", 6);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2notzd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2notza7b0za71959z00, BGl_z62bitzd2notzb0zz__bitz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2lshllongzd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2lshllon1960z00, BGl_z62bitzd2lshllongzb0zz__bitz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1780z00zz__bitz00,
		BgL_bgl_string1780za700za7za7_1961za7, "&bit-lshu32", 11);
	      DEFINE_STRING(BGl_string1781z00zz__bitz00,
		BgL_bgl_string1781za700za7za7_1962za7, "&bit-lshs64", 11);
	      DEFINE_STRING(BGl_string1782z00zz__bitz00,
		BgL_bgl_string1782za700za7za7_1963za7, "&bit-lshu64", 11);
	      DEFINE_STRING(BGl_string1783z00zz__bitz00,
		BgL_bgl_string1783za700za7za7_1964za7, "&bit-lshbx", 10);
	      DEFINE_STRING(BGl_string1784z00zz__bitz00,
		BgL_bgl_string1784za700za7za7_1965za7, "__bit", 5);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bitzd2andllongzd2envz00zz__bitz00,
		BgL_bgl_za762bitza7d2andllon1966z00, BGl_z62bitzd2andllongzb0zz__bitz00, 0L,
		BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__bitz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long
		BgL_checksumz00_1709, char *BgL_fromz00_1710)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__bitz00))
				{
					BGl_requirezd2initializa7ationz75zz__bitz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__bitz00();
					return BGl_importedzd2moduleszd2initz00zz__bitz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__bitz00(void)
	{
		{	/* Llib/bit.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* bit-or */
	BGL_EXPORTED_DEF long BGl_bitzd2orzd2zz__bitz00(long BgL_xz00_3,
		long BgL_yz00_4)
	{
		{	/* Llib/bit.scm 543 */
			return (BgL_xz00_3 | BgL_yz00_4);
		}

	}



/* &bit-or */
	obj_t BGl_z62bitzd2orzb0zz__bitz00(obj_t BgL_envz00_1203, obj_t BgL_xz00_1204,
		obj_t BgL_yz00_1205)
	{
		{	/* Llib/bit.scm 543 */
			{	/* Llib/bit.scm 543 */
				long BgL_tmpz00_1718;

				{	/* Llib/bit.scm 543 */
					long BgL_auxz00_1728;
					long BgL_auxz00_1719;

					{	/* Llib/bit.scm 543 */
						obj_t BgL_tmpz00_1729;

						if (INTEGERP(BgL_yz00_1205))
							{	/* Llib/bit.scm 543 */
								BgL_tmpz00_1729 = BgL_yz00_1205;
							}
						else
							{
								obj_t BgL_auxz00_1732;

								BgL_auxz00_1732 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(25198L), BGl_string1688z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_yz00_1205);
								FAILURE(BgL_auxz00_1732, BFALSE, BFALSE);
							}
						BgL_auxz00_1728 = (long) CINT(BgL_tmpz00_1729);
					}
					{	/* Llib/bit.scm 543 */
						obj_t BgL_tmpz00_1720;

						if (INTEGERP(BgL_xz00_1204))
							{	/* Llib/bit.scm 543 */
								BgL_tmpz00_1720 = BgL_xz00_1204;
							}
						else
							{
								obj_t BgL_auxz00_1723;

								BgL_auxz00_1723 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(25198L), BGl_string1688z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_xz00_1204);
								FAILURE(BgL_auxz00_1723, BFALSE, BFALSE);
							}
						BgL_auxz00_1719 = (long) CINT(BgL_tmpz00_1720);
					}
					BgL_tmpz00_1718 =
						BGl_bitzd2orzd2zz__bitz00(BgL_auxz00_1719, BgL_auxz00_1728);
				}
				return BINT(BgL_tmpz00_1718);
			}
		}

	}



/* bit-orelong */
	BGL_EXPORTED_DEF long BGl_bitzd2orelongzd2zz__bitz00(long BgL_xz00_5,
		long BgL_yz00_6)
	{
		{	/* Llib/bit.scm 544 */
			return (BgL_xz00_5 | BgL_yz00_6);
		}

	}



/* &bit-orelong */
	obj_t BGl_z62bitzd2orelongzb0zz__bitz00(obj_t BgL_envz00_1206,
		obj_t BgL_xz00_1207, obj_t BgL_yz00_1208)
	{
		{	/* Llib/bit.scm 544 */
			{	/* Llib/bit.scm 544 */
				long BgL_tmpz00_1740;

				{	/* Llib/bit.scm 544 */
					long BgL_auxz00_1750;
					long BgL_auxz00_1741;

					{	/* Llib/bit.scm 544 */
						obj_t BgL_tmpz00_1751;

						if (ELONGP(BgL_yz00_1208))
							{	/* Llib/bit.scm 544 */
								BgL_tmpz00_1751 = BgL_yz00_1208;
							}
						else
							{
								obj_t BgL_auxz00_1754;

								BgL_auxz00_1754 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(25245L), BGl_string1690z00zz__bitz00,
									BGl_string1691z00zz__bitz00, BgL_yz00_1208);
								FAILURE(BgL_auxz00_1754, BFALSE, BFALSE);
							}
						BgL_auxz00_1750 = BELONG_TO_LONG(BgL_tmpz00_1751);
					}
					{	/* Llib/bit.scm 544 */
						obj_t BgL_tmpz00_1742;

						if (ELONGP(BgL_xz00_1207))
							{	/* Llib/bit.scm 544 */
								BgL_tmpz00_1742 = BgL_xz00_1207;
							}
						else
							{
								obj_t BgL_auxz00_1745;

								BgL_auxz00_1745 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(25245L), BGl_string1690z00zz__bitz00,
									BGl_string1691z00zz__bitz00, BgL_xz00_1207);
								FAILURE(BgL_auxz00_1745, BFALSE, BFALSE);
							}
						BgL_auxz00_1741 = BELONG_TO_LONG(BgL_tmpz00_1742);
					}
					BgL_tmpz00_1740 =
						BGl_bitzd2orelongzd2zz__bitz00(BgL_auxz00_1741, BgL_auxz00_1750);
				}
				return make_belong(BgL_tmpz00_1740);
			}
		}

	}



/* bit-orllong */
	BGL_EXPORTED_DEF BGL_LONGLONG_T BGl_bitzd2orllongzd2zz__bitz00(BGL_LONGLONG_T
		BgL_xz00_7, BGL_LONGLONG_T BgL_yz00_8)
	{
		{	/* Llib/bit.scm 545 */
			return (BgL_xz00_7 | BgL_yz00_8);
		}

	}



/* &bit-orllong */
	obj_t BGl_z62bitzd2orllongzb0zz__bitz00(obj_t BgL_envz00_1209,
		obj_t BgL_xz00_1210, obj_t BgL_yz00_1211)
	{
		{	/* Llib/bit.scm 545 */
			{	/* Llib/bit.scm 545 */
				BGL_LONGLONG_T BgL_tmpz00_1762;

				{	/* Llib/bit.scm 545 */
					BGL_LONGLONG_T BgL_auxz00_1772;
					BGL_LONGLONG_T BgL_auxz00_1763;

					{	/* Llib/bit.scm 545 */
						obj_t BgL_tmpz00_1773;

						if (LLONGP(BgL_yz00_1211))
							{	/* Llib/bit.scm 545 */
								BgL_tmpz00_1773 = BgL_yz00_1211;
							}
						else
							{
								obj_t BgL_auxz00_1776;

								BgL_auxz00_1776 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(25297L), BGl_string1692z00zz__bitz00,
									BGl_string1693z00zz__bitz00, BgL_yz00_1211);
								FAILURE(BgL_auxz00_1776, BFALSE, BFALSE);
							}
						BgL_auxz00_1772 = BLLONG_TO_LLONG(BgL_tmpz00_1773);
					}
					{	/* Llib/bit.scm 545 */
						obj_t BgL_tmpz00_1764;

						if (LLONGP(BgL_xz00_1210))
							{	/* Llib/bit.scm 545 */
								BgL_tmpz00_1764 = BgL_xz00_1210;
							}
						else
							{
								obj_t BgL_auxz00_1767;

								BgL_auxz00_1767 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(25297L), BGl_string1692z00zz__bitz00,
									BGl_string1693z00zz__bitz00, BgL_xz00_1210);
								FAILURE(BgL_auxz00_1767, BFALSE, BFALSE);
							}
						BgL_auxz00_1763 = BLLONG_TO_LLONG(BgL_tmpz00_1764);
					}
					BgL_tmpz00_1762 =
						BGl_bitzd2orllongzd2zz__bitz00(BgL_auxz00_1763, BgL_auxz00_1772);
				}
				return make_bllong(BgL_tmpz00_1762);
			}
		}

	}



/* bit-ors8 */
	BGL_EXPORTED_DEF int8_t BGl_bitzd2ors8zd2zz__bitz00(int8_t BgL_xz00_9,
		int8_t BgL_yz00_10)
	{
		{	/* Llib/bit.scm 546 */
			return (BgL_xz00_9 | BgL_yz00_10);
		}

	}



/* &bit-ors8 */
	obj_t BGl_z62bitzd2ors8zb0zz__bitz00(obj_t BgL_envz00_1212,
		obj_t BgL_xz00_1213, obj_t BgL_yz00_1214)
	{
		{	/* Llib/bit.scm 546 */
			{	/* Llib/bit.scm 546 */
				int8_t BgL_tmpz00_1784;

				{	/* Llib/bit.scm 546 */
					int8_t BgL_auxz00_1794;
					int8_t BgL_auxz00_1785;

					{	/* Llib/bit.scm 546 */
						obj_t BgL_tmpz00_1795;

						if (BGL_INT8P(BgL_yz00_1214))
							{	/* Llib/bit.scm 546 */
								BgL_tmpz00_1795 = BgL_yz00_1214;
							}
						else
							{
								obj_t BgL_auxz00_1798;

								BgL_auxz00_1798 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(25346L), BGl_string1694z00zz__bitz00,
									BGl_string1695z00zz__bitz00, BgL_yz00_1214);
								FAILURE(BgL_auxz00_1798, BFALSE, BFALSE);
							}
						BgL_auxz00_1794 = BGL_BINT8_TO_INT8(BgL_tmpz00_1795);
					}
					{	/* Llib/bit.scm 546 */
						obj_t BgL_tmpz00_1786;

						if (BGL_INT8P(BgL_xz00_1213))
							{	/* Llib/bit.scm 546 */
								BgL_tmpz00_1786 = BgL_xz00_1213;
							}
						else
							{
								obj_t BgL_auxz00_1789;

								BgL_auxz00_1789 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(25346L), BGl_string1694z00zz__bitz00,
									BGl_string1695z00zz__bitz00, BgL_xz00_1213);
								FAILURE(BgL_auxz00_1789, BFALSE, BFALSE);
							}
						BgL_auxz00_1785 = BGL_BINT8_TO_INT8(BgL_tmpz00_1786);
					}
					BgL_tmpz00_1784 =
						BGl_bitzd2ors8zd2zz__bitz00(BgL_auxz00_1785, BgL_auxz00_1794);
				}
				return BGL_INT8_TO_BINT8(BgL_tmpz00_1784);
			}
		}

	}



/* bit-oru8 */
	BGL_EXPORTED_DEF uint8_t BGl_bitzd2oru8zd2zz__bitz00(uint8_t BgL_xz00_11,
		uint8_t BgL_yz00_12)
	{
		{	/* Llib/bit.scm 547 */
			return (BgL_xz00_11 | BgL_yz00_12);
		}

	}



/* &bit-oru8 */
	obj_t BGl_z62bitzd2oru8zb0zz__bitz00(obj_t BgL_envz00_1215,
		obj_t BgL_xz00_1216, obj_t BgL_yz00_1217)
	{
		{	/* Llib/bit.scm 547 */
			{	/* Llib/bit.scm 547 */
				uint8_t BgL_tmpz00_1806;

				{	/* Llib/bit.scm 547 */
					uint8_t BgL_auxz00_1816;
					uint8_t BgL_auxz00_1807;

					{	/* Llib/bit.scm 547 */
						obj_t BgL_tmpz00_1817;

						if (BGL_UINT8P(BgL_yz00_1217))
							{	/* Llib/bit.scm 547 */
								BgL_tmpz00_1817 = BgL_yz00_1217;
							}
						else
							{
								obj_t BgL_auxz00_1820;

								BgL_auxz00_1820 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(25392L), BGl_string1696z00zz__bitz00,
									BGl_string1697z00zz__bitz00, BgL_yz00_1217);
								FAILURE(BgL_auxz00_1820, BFALSE, BFALSE);
							}
						BgL_auxz00_1816 = BGL_BUINT8_TO_UINT8(BgL_tmpz00_1817);
					}
					{	/* Llib/bit.scm 547 */
						obj_t BgL_tmpz00_1808;

						if (BGL_UINT8P(BgL_xz00_1216))
							{	/* Llib/bit.scm 547 */
								BgL_tmpz00_1808 = BgL_xz00_1216;
							}
						else
							{
								obj_t BgL_auxz00_1811;

								BgL_auxz00_1811 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(25392L), BGl_string1696z00zz__bitz00,
									BGl_string1697z00zz__bitz00, BgL_xz00_1216);
								FAILURE(BgL_auxz00_1811, BFALSE, BFALSE);
							}
						BgL_auxz00_1807 = BGL_BUINT8_TO_UINT8(BgL_tmpz00_1808);
					}
					BgL_tmpz00_1806 =
						BGl_bitzd2oru8zd2zz__bitz00(BgL_auxz00_1807, BgL_auxz00_1816);
				}
				return BGL_UINT8_TO_BUINT8(BgL_tmpz00_1806);
			}
		}

	}



/* bit-ors16 */
	BGL_EXPORTED_DEF int16_t BGl_bitzd2ors16zd2zz__bitz00(int16_t BgL_xz00_13,
		int16_t BgL_yz00_14)
	{
		{	/* Llib/bit.scm 548 */
			return (BgL_xz00_13 | BgL_yz00_14);
		}

	}



/* &bit-ors16 */
	obj_t BGl_z62bitzd2ors16zb0zz__bitz00(obj_t BgL_envz00_1218,
		obj_t BgL_xz00_1219, obj_t BgL_yz00_1220)
	{
		{	/* Llib/bit.scm 548 */
			{	/* Llib/bit.scm 548 */
				int16_t BgL_tmpz00_1828;

				{	/* Llib/bit.scm 548 */
					int16_t BgL_auxz00_1838;
					int16_t BgL_auxz00_1829;

					{	/* Llib/bit.scm 548 */
						obj_t BgL_tmpz00_1839;

						if (BGL_INT16P(BgL_yz00_1220))
							{	/* Llib/bit.scm 548 */
								BgL_tmpz00_1839 = BgL_yz00_1220;
							}
						else
							{
								obj_t BgL_auxz00_1842;

								BgL_auxz00_1842 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(25439L), BGl_string1698z00zz__bitz00,
									BGl_string1699z00zz__bitz00, BgL_yz00_1220);
								FAILURE(BgL_auxz00_1842, BFALSE, BFALSE);
							}
						BgL_auxz00_1838 = BGL_BINT16_TO_INT16(BgL_tmpz00_1839);
					}
					{	/* Llib/bit.scm 548 */
						obj_t BgL_tmpz00_1830;

						if (BGL_INT16P(BgL_xz00_1219))
							{	/* Llib/bit.scm 548 */
								BgL_tmpz00_1830 = BgL_xz00_1219;
							}
						else
							{
								obj_t BgL_auxz00_1833;

								BgL_auxz00_1833 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(25439L), BGl_string1698z00zz__bitz00,
									BGl_string1699z00zz__bitz00, BgL_xz00_1219);
								FAILURE(BgL_auxz00_1833, BFALSE, BFALSE);
							}
						BgL_auxz00_1829 = BGL_BINT16_TO_INT16(BgL_tmpz00_1830);
					}
					BgL_tmpz00_1828 =
						BGl_bitzd2ors16zd2zz__bitz00(BgL_auxz00_1829, BgL_auxz00_1838);
				}
				return BGL_INT16_TO_BINT16(BgL_tmpz00_1828);
			}
		}

	}



/* bit-oru16 */
	BGL_EXPORTED_DEF uint16_t BGl_bitzd2oru16zd2zz__bitz00(uint16_t BgL_xz00_15,
		uint16_t BgL_yz00_16)
	{
		{	/* Llib/bit.scm 549 */
			return (BgL_xz00_15 | BgL_yz00_16);
		}

	}



/* &bit-oru16 */
	obj_t BGl_z62bitzd2oru16zb0zz__bitz00(obj_t BgL_envz00_1221,
		obj_t BgL_xz00_1222, obj_t BgL_yz00_1223)
	{
		{	/* Llib/bit.scm 549 */
			{	/* Llib/bit.scm 549 */
				uint16_t BgL_tmpz00_1850;

				{	/* Llib/bit.scm 549 */
					uint16_t BgL_auxz00_1860;
					uint16_t BgL_auxz00_1851;

					{	/* Llib/bit.scm 549 */
						obj_t BgL_tmpz00_1861;

						if (BGL_UINT16P(BgL_yz00_1223))
							{	/* Llib/bit.scm 549 */
								BgL_tmpz00_1861 = BgL_yz00_1223;
							}
						else
							{
								obj_t BgL_auxz00_1864;

								BgL_auxz00_1864 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(25487L), BGl_string1700z00zz__bitz00,
									BGl_string1701z00zz__bitz00, BgL_yz00_1223);
								FAILURE(BgL_auxz00_1864, BFALSE, BFALSE);
							}
						BgL_auxz00_1860 = BGL_BUINT16_TO_UINT16(BgL_tmpz00_1861);
					}
					{	/* Llib/bit.scm 549 */
						obj_t BgL_tmpz00_1852;

						if (BGL_UINT16P(BgL_xz00_1222))
							{	/* Llib/bit.scm 549 */
								BgL_tmpz00_1852 = BgL_xz00_1222;
							}
						else
							{
								obj_t BgL_auxz00_1855;

								BgL_auxz00_1855 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(25487L), BGl_string1700z00zz__bitz00,
									BGl_string1701z00zz__bitz00, BgL_xz00_1222);
								FAILURE(BgL_auxz00_1855, BFALSE, BFALSE);
							}
						BgL_auxz00_1851 = BGL_BUINT16_TO_UINT16(BgL_tmpz00_1852);
					}
					BgL_tmpz00_1850 =
						BGl_bitzd2oru16zd2zz__bitz00(BgL_auxz00_1851, BgL_auxz00_1860);
				}
				return BGL_UINT16_TO_BUINT16(BgL_tmpz00_1850);
			}
		}

	}



/* bit-ors32 */
	BGL_EXPORTED_DEF int32_t BGl_bitzd2ors32zd2zz__bitz00(int32_t BgL_xz00_17,
		int32_t BgL_yz00_18)
	{
		{	/* Llib/bit.scm 550 */
			return (BgL_xz00_17 | BgL_yz00_18);
		}

	}



/* &bit-ors32 */
	obj_t BGl_z62bitzd2ors32zb0zz__bitz00(obj_t BgL_envz00_1224,
		obj_t BgL_xz00_1225, obj_t BgL_yz00_1226)
	{
		{	/* Llib/bit.scm 550 */
			{	/* Llib/bit.scm 550 */
				int32_t BgL_tmpz00_1872;

				{	/* Llib/bit.scm 550 */
					int32_t BgL_auxz00_1882;
					int32_t BgL_auxz00_1873;

					{	/* Llib/bit.scm 550 */
						obj_t BgL_tmpz00_1883;

						if (BGL_INT32P(BgL_yz00_1226))
							{	/* Llib/bit.scm 550 */
								BgL_tmpz00_1883 = BgL_yz00_1226;
							}
						else
							{
								obj_t BgL_auxz00_1886;

								BgL_auxz00_1886 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(25535L), BGl_string1702z00zz__bitz00,
									BGl_string1703z00zz__bitz00, BgL_yz00_1226);
								FAILURE(BgL_auxz00_1886, BFALSE, BFALSE);
							}
						BgL_auxz00_1882 = BGL_BINT32_TO_INT32(BgL_tmpz00_1883);
					}
					{	/* Llib/bit.scm 550 */
						obj_t BgL_tmpz00_1874;

						if (BGL_INT32P(BgL_xz00_1225))
							{	/* Llib/bit.scm 550 */
								BgL_tmpz00_1874 = BgL_xz00_1225;
							}
						else
							{
								obj_t BgL_auxz00_1877;

								BgL_auxz00_1877 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(25535L), BGl_string1702z00zz__bitz00,
									BGl_string1703z00zz__bitz00, BgL_xz00_1225);
								FAILURE(BgL_auxz00_1877, BFALSE, BFALSE);
							}
						BgL_auxz00_1873 = BGL_BINT32_TO_INT32(BgL_tmpz00_1874);
					}
					BgL_tmpz00_1872 =
						BGl_bitzd2ors32zd2zz__bitz00(BgL_auxz00_1873, BgL_auxz00_1882);
				}
				return BGL_INT32_TO_BINT32(BgL_tmpz00_1872);
			}
		}

	}



/* bit-oru32 */
	BGL_EXPORTED_DEF uint32_t BGl_bitzd2oru32zd2zz__bitz00(uint32_t BgL_xz00_19,
		uint32_t BgL_yz00_20)
	{
		{	/* Llib/bit.scm 551 */
			return (BgL_xz00_19 | BgL_yz00_20);
		}

	}



/* &bit-oru32 */
	obj_t BGl_z62bitzd2oru32zb0zz__bitz00(obj_t BgL_envz00_1227,
		obj_t BgL_xz00_1228, obj_t BgL_yz00_1229)
	{
		{	/* Llib/bit.scm 551 */
			{	/* Llib/bit.scm 551 */
				uint32_t BgL_tmpz00_1894;

				{	/* Llib/bit.scm 551 */
					uint32_t BgL_auxz00_1904;
					uint32_t BgL_auxz00_1895;

					{	/* Llib/bit.scm 551 */
						obj_t BgL_tmpz00_1905;

						if (BGL_UINT32P(BgL_yz00_1229))
							{	/* Llib/bit.scm 551 */
								BgL_tmpz00_1905 = BgL_yz00_1229;
							}
						else
							{
								obj_t BgL_auxz00_1908;

								BgL_auxz00_1908 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(25583L), BGl_string1704z00zz__bitz00,
									BGl_string1705z00zz__bitz00, BgL_yz00_1229);
								FAILURE(BgL_auxz00_1908, BFALSE, BFALSE);
							}
						BgL_auxz00_1904 = BGL_BUINT32_TO_UINT32(BgL_tmpz00_1905);
					}
					{	/* Llib/bit.scm 551 */
						obj_t BgL_tmpz00_1896;

						if (BGL_UINT32P(BgL_xz00_1228))
							{	/* Llib/bit.scm 551 */
								BgL_tmpz00_1896 = BgL_xz00_1228;
							}
						else
							{
								obj_t BgL_auxz00_1899;

								BgL_auxz00_1899 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(25583L), BGl_string1704z00zz__bitz00,
									BGl_string1705z00zz__bitz00, BgL_xz00_1228);
								FAILURE(BgL_auxz00_1899, BFALSE, BFALSE);
							}
						BgL_auxz00_1895 = BGL_BUINT32_TO_UINT32(BgL_tmpz00_1896);
					}
					BgL_tmpz00_1894 =
						BGl_bitzd2oru32zd2zz__bitz00(BgL_auxz00_1895, BgL_auxz00_1904);
				}
				return BGL_UINT32_TO_BUINT32(BgL_tmpz00_1894);
			}
		}

	}



/* bit-ors64 */
	BGL_EXPORTED_DEF int64_t BGl_bitzd2ors64zd2zz__bitz00(int64_t BgL_xz00_21,
		int64_t BgL_yz00_22)
	{
		{	/* Llib/bit.scm 552 */
			return (BgL_xz00_21 | BgL_yz00_22);
		}

	}



/* &bit-ors64 */
	obj_t BGl_z62bitzd2ors64zb0zz__bitz00(obj_t BgL_envz00_1230,
		obj_t BgL_xz00_1231, obj_t BgL_yz00_1232)
	{
		{	/* Llib/bit.scm 552 */
			{	/* Llib/bit.scm 552 */
				int64_t BgL_tmpz00_1916;

				{	/* Llib/bit.scm 552 */
					int64_t BgL_auxz00_1926;
					int64_t BgL_auxz00_1917;

					{	/* Llib/bit.scm 552 */
						obj_t BgL_tmpz00_1927;

						if (BGL_INT64P(BgL_yz00_1232))
							{	/* Llib/bit.scm 552 */
								BgL_tmpz00_1927 = BgL_yz00_1232;
							}
						else
							{
								obj_t BgL_auxz00_1930;

								BgL_auxz00_1930 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(25631L), BGl_string1706z00zz__bitz00,
									BGl_string1707z00zz__bitz00, BgL_yz00_1232);
								FAILURE(BgL_auxz00_1930, BFALSE, BFALSE);
							}
						BgL_auxz00_1926 = BGL_BINT64_TO_INT64(BgL_tmpz00_1927);
					}
					{	/* Llib/bit.scm 552 */
						obj_t BgL_tmpz00_1918;

						if (BGL_INT64P(BgL_xz00_1231))
							{	/* Llib/bit.scm 552 */
								BgL_tmpz00_1918 = BgL_xz00_1231;
							}
						else
							{
								obj_t BgL_auxz00_1921;

								BgL_auxz00_1921 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(25631L), BGl_string1706z00zz__bitz00,
									BGl_string1707z00zz__bitz00, BgL_xz00_1231);
								FAILURE(BgL_auxz00_1921, BFALSE, BFALSE);
							}
						BgL_auxz00_1917 = BGL_BINT64_TO_INT64(BgL_tmpz00_1918);
					}
					BgL_tmpz00_1916 =
						BGl_bitzd2ors64zd2zz__bitz00(BgL_auxz00_1917, BgL_auxz00_1926);
				}
				return BGL_INT64_TO_BINT64(BgL_tmpz00_1916);
			}
		}

	}



/* bit-oru64 */
	BGL_EXPORTED_DEF uint64_t BGl_bitzd2oru64zd2zz__bitz00(uint64_t BgL_xz00_23,
		uint64_t BgL_yz00_24)
	{
		{	/* Llib/bit.scm 553 */
			return (BgL_xz00_23 | BgL_yz00_24);
		}

	}



/* &bit-oru64 */
	obj_t BGl_z62bitzd2oru64zb0zz__bitz00(obj_t BgL_envz00_1233,
		obj_t BgL_xz00_1234, obj_t BgL_yz00_1235)
	{
		{	/* Llib/bit.scm 553 */
			{	/* Llib/bit.scm 553 */
				uint64_t BgL_tmpz00_1938;

				{	/* Llib/bit.scm 553 */
					uint64_t BgL_auxz00_1948;
					uint64_t BgL_auxz00_1939;

					{	/* Llib/bit.scm 553 */
						obj_t BgL_tmpz00_1949;

						if (BGL_UINT64P(BgL_yz00_1235))
							{	/* Llib/bit.scm 553 */
								BgL_tmpz00_1949 = BgL_yz00_1235;
							}
						else
							{
								obj_t BgL_auxz00_1952;

								BgL_auxz00_1952 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(25679L), BGl_string1708z00zz__bitz00,
									BGl_string1709z00zz__bitz00, BgL_yz00_1235);
								FAILURE(BgL_auxz00_1952, BFALSE, BFALSE);
							}
						BgL_auxz00_1948 = BGL_BINT64_TO_INT64(BgL_tmpz00_1949);
					}
					{	/* Llib/bit.scm 553 */
						obj_t BgL_tmpz00_1940;

						if (BGL_UINT64P(BgL_xz00_1234))
							{	/* Llib/bit.scm 553 */
								BgL_tmpz00_1940 = BgL_xz00_1234;
							}
						else
							{
								obj_t BgL_auxz00_1943;

								BgL_auxz00_1943 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(25679L), BGl_string1708z00zz__bitz00,
									BGl_string1709z00zz__bitz00, BgL_xz00_1234);
								FAILURE(BgL_auxz00_1943, BFALSE, BFALSE);
							}
						BgL_auxz00_1939 = BGL_BINT64_TO_INT64(BgL_tmpz00_1940);
					}
					BgL_tmpz00_1938 =
						BGl_bitzd2oru64zd2zz__bitz00(BgL_auxz00_1939, BgL_auxz00_1948);
				}
				return BGL_UINT64_TO_BUINT64(BgL_tmpz00_1938);
			}
		}

	}



/* bit-orbx */
	BGL_EXPORTED_DEF obj_t BGl_bitzd2orbxzd2zz__bitz00(obj_t BgL_xz00_25,
		obj_t BgL_yz00_26)
	{
		{	/* Llib/bit.scm 554 */
			BGL_TAIL return bgl_bignum_or(BgL_xz00_25, BgL_yz00_26);
		}

	}



/* &bit-orbx */
	obj_t BGl_z62bitzd2orbxzb0zz__bitz00(obj_t BgL_envz00_1236,
		obj_t BgL_xz00_1237, obj_t BgL_yz00_1238)
	{
		{	/* Llib/bit.scm 554 */
			{	/* Llib/bit.scm 554 */
				obj_t BgL_auxz00_1967;
				obj_t BgL_auxz00_1960;

				if (BIGNUMP(BgL_yz00_1238))
					{	/* Llib/bit.scm 554 */
						BgL_auxz00_1967 = BgL_yz00_1238;
					}
				else
					{
						obj_t BgL_auxz00_1970;

						BgL_auxz00_1970 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
							BINT(25726L), BGl_string1710z00zz__bitz00,
							BGl_string1711z00zz__bitz00, BgL_yz00_1238);
						FAILURE(BgL_auxz00_1970, BFALSE, BFALSE);
					}
				if (BIGNUMP(BgL_xz00_1237))
					{	/* Llib/bit.scm 554 */
						BgL_auxz00_1960 = BgL_xz00_1237;
					}
				else
					{
						obj_t BgL_auxz00_1963;

						BgL_auxz00_1963 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
							BINT(25726L), BGl_string1710z00zz__bitz00,
							BGl_string1711z00zz__bitz00, BgL_xz00_1237);
						FAILURE(BgL_auxz00_1963, BFALSE, BFALSE);
					}
				return BGl_bitzd2orbxzd2zz__bitz00(BgL_auxz00_1960, BgL_auxz00_1967);
			}
		}

	}



/* bit-and */
	BGL_EXPORTED_DEF long BGl_bitzd2andzd2zz__bitz00(long BgL_xz00_27,
		long BgL_yz00_28)
	{
		{	/* Llib/bit.scm 559 */
			return (BgL_xz00_27 & BgL_yz00_28);
		}

	}



/* &bit-and */
	obj_t BGl_z62bitzd2andzb0zz__bitz00(obj_t BgL_envz00_1239,
		obj_t BgL_xz00_1240, obj_t BgL_yz00_1241)
	{
		{	/* Llib/bit.scm 559 */
			{	/* Llib/bit.scm 559 */
				long BgL_tmpz00_1976;

				{	/* Llib/bit.scm 559 */
					long BgL_auxz00_1986;
					long BgL_auxz00_1977;

					{	/* Llib/bit.scm 559 */
						obj_t BgL_tmpz00_1987;

						if (INTEGERP(BgL_yz00_1241))
							{	/* Llib/bit.scm 559 */
								BgL_tmpz00_1987 = BgL_yz00_1241;
							}
						else
							{
								obj_t BgL_auxz00_1990;

								BgL_auxz00_1990 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(25994L), BGl_string1712z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_yz00_1241);
								FAILURE(BgL_auxz00_1990, BFALSE, BFALSE);
							}
						BgL_auxz00_1986 = (long) CINT(BgL_tmpz00_1987);
					}
					{	/* Llib/bit.scm 559 */
						obj_t BgL_tmpz00_1978;

						if (INTEGERP(BgL_xz00_1240))
							{	/* Llib/bit.scm 559 */
								BgL_tmpz00_1978 = BgL_xz00_1240;
							}
						else
							{
								obj_t BgL_auxz00_1981;

								BgL_auxz00_1981 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(25994L), BGl_string1712z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_xz00_1240);
								FAILURE(BgL_auxz00_1981, BFALSE, BFALSE);
							}
						BgL_auxz00_1977 = (long) CINT(BgL_tmpz00_1978);
					}
					BgL_tmpz00_1976 =
						BGl_bitzd2andzd2zz__bitz00(BgL_auxz00_1977, BgL_auxz00_1986);
				}
				return BINT(BgL_tmpz00_1976);
			}
		}

	}



/* bit-andelong */
	BGL_EXPORTED_DEF long BGl_bitzd2andelongzd2zz__bitz00(long BgL_xz00_29,
		long BgL_yz00_30)
	{
		{	/* Llib/bit.scm 560 */
			return (BgL_xz00_29 & BgL_yz00_30);
		}

	}



/* &bit-andelong */
	obj_t BGl_z62bitzd2andelongzb0zz__bitz00(obj_t BgL_envz00_1242,
		obj_t BgL_xz00_1243, obj_t BgL_yz00_1244)
	{
		{	/* Llib/bit.scm 560 */
			{	/* Llib/bit.scm 560 */
				long BgL_tmpz00_1998;

				{	/* Llib/bit.scm 560 */
					long BgL_auxz00_2008;
					long BgL_auxz00_1999;

					{	/* Llib/bit.scm 560 */
						obj_t BgL_tmpz00_2009;

						if (ELONGP(BgL_yz00_1244))
							{	/* Llib/bit.scm 560 */
								BgL_tmpz00_2009 = BgL_yz00_1244;
							}
						else
							{
								obj_t BgL_auxz00_2012;

								BgL_auxz00_2012 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(26043L), BGl_string1713z00zz__bitz00,
									BGl_string1691z00zz__bitz00, BgL_yz00_1244);
								FAILURE(BgL_auxz00_2012, BFALSE, BFALSE);
							}
						BgL_auxz00_2008 = BELONG_TO_LONG(BgL_tmpz00_2009);
					}
					{	/* Llib/bit.scm 560 */
						obj_t BgL_tmpz00_2000;

						if (ELONGP(BgL_xz00_1243))
							{	/* Llib/bit.scm 560 */
								BgL_tmpz00_2000 = BgL_xz00_1243;
							}
						else
							{
								obj_t BgL_auxz00_2003;

								BgL_auxz00_2003 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(26043L), BGl_string1713z00zz__bitz00,
									BGl_string1691z00zz__bitz00, BgL_xz00_1243);
								FAILURE(BgL_auxz00_2003, BFALSE, BFALSE);
							}
						BgL_auxz00_1999 = BELONG_TO_LONG(BgL_tmpz00_2000);
					}
					BgL_tmpz00_1998 =
						BGl_bitzd2andelongzd2zz__bitz00(BgL_auxz00_1999, BgL_auxz00_2008);
				}
				return make_belong(BgL_tmpz00_1998);
			}
		}

	}



/* bit-andllong */
	BGL_EXPORTED_DEF BGL_LONGLONG_T BGl_bitzd2andllongzd2zz__bitz00(BGL_LONGLONG_T
		BgL_xz00_31, BGL_LONGLONG_T BgL_yz00_32)
	{
		{	/* Llib/bit.scm 561 */
			return (BgL_xz00_31 & BgL_yz00_32);
		}

	}



/* &bit-andllong */
	obj_t BGl_z62bitzd2andllongzb0zz__bitz00(obj_t BgL_envz00_1245,
		obj_t BgL_xz00_1246, obj_t BgL_yz00_1247)
	{
		{	/* Llib/bit.scm 561 */
			{	/* Llib/bit.scm 561 */
				BGL_LONGLONG_T BgL_tmpz00_2020;

				{	/* Llib/bit.scm 561 */
					BGL_LONGLONG_T BgL_auxz00_2030;
					BGL_LONGLONG_T BgL_auxz00_2021;

					{	/* Llib/bit.scm 561 */
						obj_t BgL_tmpz00_2031;

						if (LLONGP(BgL_yz00_1247))
							{	/* Llib/bit.scm 561 */
								BgL_tmpz00_2031 = BgL_yz00_1247;
							}
						else
							{
								obj_t BgL_auxz00_2034;

								BgL_auxz00_2034 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(26097L), BGl_string1714z00zz__bitz00,
									BGl_string1693z00zz__bitz00, BgL_yz00_1247);
								FAILURE(BgL_auxz00_2034, BFALSE, BFALSE);
							}
						BgL_auxz00_2030 = BLLONG_TO_LLONG(BgL_tmpz00_2031);
					}
					{	/* Llib/bit.scm 561 */
						obj_t BgL_tmpz00_2022;

						if (LLONGP(BgL_xz00_1246))
							{	/* Llib/bit.scm 561 */
								BgL_tmpz00_2022 = BgL_xz00_1246;
							}
						else
							{
								obj_t BgL_auxz00_2025;

								BgL_auxz00_2025 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(26097L), BGl_string1714z00zz__bitz00,
									BGl_string1693z00zz__bitz00, BgL_xz00_1246);
								FAILURE(BgL_auxz00_2025, BFALSE, BFALSE);
							}
						BgL_auxz00_2021 = BLLONG_TO_LLONG(BgL_tmpz00_2022);
					}
					BgL_tmpz00_2020 =
						BGl_bitzd2andllongzd2zz__bitz00(BgL_auxz00_2021, BgL_auxz00_2030);
				}
				return make_bllong(BgL_tmpz00_2020);
			}
		}

	}



/* bit-ands8 */
	BGL_EXPORTED_DEF int8_t BGl_bitzd2ands8zd2zz__bitz00(int8_t BgL_xz00_33,
		int8_t BgL_yz00_34)
	{
		{	/* Llib/bit.scm 562 */
			return (BgL_xz00_33 & BgL_yz00_34);
		}

	}



/* &bit-ands8 */
	obj_t BGl_z62bitzd2ands8zb0zz__bitz00(obj_t BgL_envz00_1248,
		obj_t BgL_xz00_1249, obj_t BgL_yz00_1250)
	{
		{	/* Llib/bit.scm 562 */
			{	/* Llib/bit.scm 562 */
				int8_t BgL_tmpz00_2042;

				{	/* Llib/bit.scm 562 */
					int8_t BgL_auxz00_2052;
					int8_t BgL_auxz00_2043;

					{	/* Llib/bit.scm 562 */
						obj_t BgL_tmpz00_2053;

						if (BGL_INT8P(BgL_yz00_1250))
							{	/* Llib/bit.scm 562 */
								BgL_tmpz00_2053 = BgL_yz00_1250;
							}
						else
							{
								obj_t BgL_auxz00_2056;

								BgL_auxz00_2056 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(26148L), BGl_string1715z00zz__bitz00,
									BGl_string1695z00zz__bitz00, BgL_yz00_1250);
								FAILURE(BgL_auxz00_2056, BFALSE, BFALSE);
							}
						BgL_auxz00_2052 = BGL_BINT8_TO_INT8(BgL_tmpz00_2053);
					}
					{	/* Llib/bit.scm 562 */
						obj_t BgL_tmpz00_2044;

						if (BGL_INT8P(BgL_xz00_1249))
							{	/* Llib/bit.scm 562 */
								BgL_tmpz00_2044 = BgL_xz00_1249;
							}
						else
							{
								obj_t BgL_auxz00_2047;

								BgL_auxz00_2047 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(26148L), BGl_string1715z00zz__bitz00,
									BGl_string1695z00zz__bitz00, BgL_xz00_1249);
								FAILURE(BgL_auxz00_2047, BFALSE, BFALSE);
							}
						BgL_auxz00_2043 = BGL_BINT8_TO_INT8(BgL_tmpz00_2044);
					}
					BgL_tmpz00_2042 =
						BGl_bitzd2ands8zd2zz__bitz00(BgL_auxz00_2043, BgL_auxz00_2052);
				}
				return BGL_INT8_TO_BINT8(BgL_tmpz00_2042);
			}
		}

	}



/* bit-andu8 */
	BGL_EXPORTED_DEF uint8_t BGl_bitzd2andu8zd2zz__bitz00(uint8_t BgL_xz00_35,
		uint8_t BgL_yz00_36)
	{
		{	/* Llib/bit.scm 563 */
			return (BgL_xz00_35 & BgL_yz00_36);
		}

	}



/* &bit-andu8 */
	obj_t BGl_z62bitzd2andu8zb0zz__bitz00(obj_t BgL_envz00_1251,
		obj_t BgL_xz00_1252, obj_t BgL_yz00_1253)
	{
		{	/* Llib/bit.scm 563 */
			{	/* Llib/bit.scm 563 */
				uint8_t BgL_tmpz00_2064;

				{	/* Llib/bit.scm 563 */
					uint8_t BgL_auxz00_2074;
					uint8_t BgL_auxz00_2065;

					{	/* Llib/bit.scm 563 */
						obj_t BgL_tmpz00_2075;

						if (BGL_UINT8P(BgL_yz00_1253))
							{	/* Llib/bit.scm 563 */
								BgL_tmpz00_2075 = BgL_yz00_1253;
							}
						else
							{
								obj_t BgL_auxz00_2078;

								BgL_auxz00_2078 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(26196L), BGl_string1716z00zz__bitz00,
									BGl_string1697z00zz__bitz00, BgL_yz00_1253);
								FAILURE(BgL_auxz00_2078, BFALSE, BFALSE);
							}
						BgL_auxz00_2074 = BGL_BUINT8_TO_UINT8(BgL_tmpz00_2075);
					}
					{	/* Llib/bit.scm 563 */
						obj_t BgL_tmpz00_2066;

						if (BGL_UINT8P(BgL_xz00_1252))
							{	/* Llib/bit.scm 563 */
								BgL_tmpz00_2066 = BgL_xz00_1252;
							}
						else
							{
								obj_t BgL_auxz00_2069;

								BgL_auxz00_2069 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(26196L), BGl_string1716z00zz__bitz00,
									BGl_string1697z00zz__bitz00, BgL_xz00_1252);
								FAILURE(BgL_auxz00_2069, BFALSE, BFALSE);
							}
						BgL_auxz00_2065 = BGL_BUINT8_TO_UINT8(BgL_tmpz00_2066);
					}
					BgL_tmpz00_2064 =
						BGl_bitzd2andu8zd2zz__bitz00(BgL_auxz00_2065, BgL_auxz00_2074);
				}
				return BGL_UINT8_TO_BUINT8(BgL_tmpz00_2064);
			}
		}

	}



/* bit-ands16 */
	BGL_EXPORTED_DEF int16_t BGl_bitzd2ands16zd2zz__bitz00(int16_t BgL_xz00_37,
		int16_t BgL_yz00_38)
	{
		{	/* Llib/bit.scm 564 */
			return (BgL_xz00_37 & BgL_yz00_38);
		}

	}



/* &bit-ands16 */
	obj_t BGl_z62bitzd2ands16zb0zz__bitz00(obj_t BgL_envz00_1254,
		obj_t BgL_xz00_1255, obj_t BgL_yz00_1256)
	{
		{	/* Llib/bit.scm 564 */
			{	/* Llib/bit.scm 564 */
				int16_t BgL_tmpz00_2086;

				{	/* Llib/bit.scm 564 */
					int16_t BgL_auxz00_2096;
					int16_t BgL_auxz00_2087;

					{	/* Llib/bit.scm 564 */
						obj_t BgL_tmpz00_2097;

						if (BGL_INT16P(BgL_yz00_1256))
							{	/* Llib/bit.scm 564 */
								BgL_tmpz00_2097 = BgL_yz00_1256;
							}
						else
							{
								obj_t BgL_auxz00_2100;

								BgL_auxz00_2100 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(26245L), BGl_string1717z00zz__bitz00,
									BGl_string1699z00zz__bitz00, BgL_yz00_1256);
								FAILURE(BgL_auxz00_2100, BFALSE, BFALSE);
							}
						BgL_auxz00_2096 = BGL_BINT16_TO_INT16(BgL_tmpz00_2097);
					}
					{	/* Llib/bit.scm 564 */
						obj_t BgL_tmpz00_2088;

						if (BGL_INT16P(BgL_xz00_1255))
							{	/* Llib/bit.scm 564 */
								BgL_tmpz00_2088 = BgL_xz00_1255;
							}
						else
							{
								obj_t BgL_auxz00_2091;

								BgL_auxz00_2091 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(26245L), BGl_string1717z00zz__bitz00,
									BGl_string1699z00zz__bitz00, BgL_xz00_1255);
								FAILURE(BgL_auxz00_2091, BFALSE, BFALSE);
							}
						BgL_auxz00_2087 = BGL_BINT16_TO_INT16(BgL_tmpz00_2088);
					}
					BgL_tmpz00_2086 =
						BGl_bitzd2ands16zd2zz__bitz00(BgL_auxz00_2087, BgL_auxz00_2096);
				}
				return BGL_INT16_TO_BINT16(BgL_tmpz00_2086);
			}
		}

	}



/* bit-andu16 */
	BGL_EXPORTED_DEF uint16_t BGl_bitzd2andu16zd2zz__bitz00(uint16_t BgL_xz00_39,
		uint16_t BgL_yz00_40)
	{
		{	/* Llib/bit.scm 565 */
			return (BgL_xz00_39 & BgL_yz00_40);
		}

	}



/* &bit-andu16 */
	obj_t BGl_z62bitzd2andu16zb0zz__bitz00(obj_t BgL_envz00_1257,
		obj_t BgL_xz00_1258, obj_t BgL_yz00_1259)
	{
		{	/* Llib/bit.scm 565 */
			{	/* Llib/bit.scm 565 */
				uint16_t BgL_tmpz00_2108;

				{	/* Llib/bit.scm 565 */
					uint16_t BgL_auxz00_2118;
					uint16_t BgL_auxz00_2109;

					{	/* Llib/bit.scm 565 */
						obj_t BgL_tmpz00_2119;

						if (BGL_UINT16P(BgL_yz00_1259))
							{	/* Llib/bit.scm 565 */
								BgL_tmpz00_2119 = BgL_yz00_1259;
							}
						else
							{
								obj_t BgL_auxz00_2122;

								BgL_auxz00_2122 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(26295L), BGl_string1718z00zz__bitz00,
									BGl_string1701z00zz__bitz00, BgL_yz00_1259);
								FAILURE(BgL_auxz00_2122, BFALSE, BFALSE);
							}
						BgL_auxz00_2118 = BGL_BUINT16_TO_UINT16(BgL_tmpz00_2119);
					}
					{	/* Llib/bit.scm 565 */
						obj_t BgL_tmpz00_2110;

						if (BGL_UINT16P(BgL_xz00_1258))
							{	/* Llib/bit.scm 565 */
								BgL_tmpz00_2110 = BgL_xz00_1258;
							}
						else
							{
								obj_t BgL_auxz00_2113;

								BgL_auxz00_2113 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(26295L), BGl_string1718z00zz__bitz00,
									BGl_string1701z00zz__bitz00, BgL_xz00_1258);
								FAILURE(BgL_auxz00_2113, BFALSE, BFALSE);
							}
						BgL_auxz00_2109 = BGL_BUINT16_TO_UINT16(BgL_tmpz00_2110);
					}
					BgL_tmpz00_2108 =
						BGl_bitzd2andu16zd2zz__bitz00(BgL_auxz00_2109, BgL_auxz00_2118);
				}
				return BGL_UINT16_TO_BUINT16(BgL_tmpz00_2108);
			}
		}

	}



/* bit-ands32 */
	BGL_EXPORTED_DEF int32_t BGl_bitzd2ands32zd2zz__bitz00(int32_t BgL_xz00_41,
		int32_t BgL_yz00_42)
	{
		{	/* Llib/bit.scm 566 */
			return (BgL_xz00_41 & BgL_yz00_42);
		}

	}



/* &bit-ands32 */
	obj_t BGl_z62bitzd2ands32zb0zz__bitz00(obj_t BgL_envz00_1260,
		obj_t BgL_xz00_1261, obj_t BgL_yz00_1262)
	{
		{	/* Llib/bit.scm 566 */
			{	/* Llib/bit.scm 566 */
				int32_t BgL_tmpz00_2130;

				{	/* Llib/bit.scm 566 */
					int32_t BgL_auxz00_2140;
					int32_t BgL_auxz00_2131;

					{	/* Llib/bit.scm 566 */
						obj_t BgL_tmpz00_2141;

						if (BGL_INT32P(BgL_yz00_1262))
							{	/* Llib/bit.scm 566 */
								BgL_tmpz00_2141 = BgL_yz00_1262;
							}
						else
							{
								obj_t BgL_auxz00_2144;

								BgL_auxz00_2144 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(26345L), BGl_string1719z00zz__bitz00,
									BGl_string1703z00zz__bitz00, BgL_yz00_1262);
								FAILURE(BgL_auxz00_2144, BFALSE, BFALSE);
							}
						BgL_auxz00_2140 = BGL_BINT32_TO_INT32(BgL_tmpz00_2141);
					}
					{	/* Llib/bit.scm 566 */
						obj_t BgL_tmpz00_2132;

						if (BGL_INT32P(BgL_xz00_1261))
							{	/* Llib/bit.scm 566 */
								BgL_tmpz00_2132 = BgL_xz00_1261;
							}
						else
							{
								obj_t BgL_auxz00_2135;

								BgL_auxz00_2135 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(26345L), BGl_string1719z00zz__bitz00,
									BGl_string1703z00zz__bitz00, BgL_xz00_1261);
								FAILURE(BgL_auxz00_2135, BFALSE, BFALSE);
							}
						BgL_auxz00_2131 = BGL_BINT32_TO_INT32(BgL_tmpz00_2132);
					}
					BgL_tmpz00_2130 =
						BGl_bitzd2ands32zd2zz__bitz00(BgL_auxz00_2131, BgL_auxz00_2140);
				}
				return BGL_INT32_TO_BINT32(BgL_tmpz00_2130);
			}
		}

	}



/* bit-andu32 */
	BGL_EXPORTED_DEF uint32_t BGl_bitzd2andu32zd2zz__bitz00(uint32_t BgL_xz00_43,
		uint32_t BgL_yz00_44)
	{
		{	/* Llib/bit.scm 567 */
			return (BgL_xz00_43 & BgL_yz00_44);
		}

	}



/* &bit-andu32 */
	obj_t BGl_z62bitzd2andu32zb0zz__bitz00(obj_t BgL_envz00_1263,
		obj_t BgL_xz00_1264, obj_t BgL_yz00_1265)
	{
		{	/* Llib/bit.scm 567 */
			{	/* Llib/bit.scm 567 */
				uint32_t BgL_tmpz00_2152;

				{	/* Llib/bit.scm 567 */
					uint32_t BgL_auxz00_2162;
					uint32_t BgL_auxz00_2153;

					{	/* Llib/bit.scm 567 */
						obj_t BgL_tmpz00_2163;

						if (BGL_UINT32P(BgL_yz00_1265))
							{	/* Llib/bit.scm 567 */
								BgL_tmpz00_2163 = BgL_yz00_1265;
							}
						else
							{
								obj_t BgL_auxz00_2166;

								BgL_auxz00_2166 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(26395L), BGl_string1720z00zz__bitz00,
									BGl_string1705z00zz__bitz00, BgL_yz00_1265);
								FAILURE(BgL_auxz00_2166, BFALSE, BFALSE);
							}
						BgL_auxz00_2162 = BGL_BUINT32_TO_UINT32(BgL_tmpz00_2163);
					}
					{	/* Llib/bit.scm 567 */
						obj_t BgL_tmpz00_2154;

						if (BGL_UINT32P(BgL_xz00_1264))
							{	/* Llib/bit.scm 567 */
								BgL_tmpz00_2154 = BgL_xz00_1264;
							}
						else
							{
								obj_t BgL_auxz00_2157;

								BgL_auxz00_2157 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(26395L), BGl_string1720z00zz__bitz00,
									BGl_string1705z00zz__bitz00, BgL_xz00_1264);
								FAILURE(BgL_auxz00_2157, BFALSE, BFALSE);
							}
						BgL_auxz00_2153 = BGL_BUINT32_TO_UINT32(BgL_tmpz00_2154);
					}
					BgL_tmpz00_2152 =
						BGl_bitzd2andu32zd2zz__bitz00(BgL_auxz00_2153, BgL_auxz00_2162);
				}
				return BGL_UINT32_TO_BUINT32(BgL_tmpz00_2152);
			}
		}

	}



/* bit-ands64 */
	BGL_EXPORTED_DEF int64_t BGl_bitzd2ands64zd2zz__bitz00(int64_t BgL_xz00_45,
		int64_t BgL_yz00_46)
	{
		{	/* Llib/bit.scm 568 */
			return (BgL_xz00_45 & BgL_yz00_46);
		}

	}



/* &bit-ands64 */
	obj_t BGl_z62bitzd2ands64zb0zz__bitz00(obj_t BgL_envz00_1266,
		obj_t BgL_xz00_1267, obj_t BgL_yz00_1268)
	{
		{	/* Llib/bit.scm 568 */
			{	/* Llib/bit.scm 568 */
				int64_t BgL_tmpz00_2174;

				{	/* Llib/bit.scm 568 */
					int64_t BgL_auxz00_2184;
					int64_t BgL_auxz00_2175;

					{	/* Llib/bit.scm 568 */
						obj_t BgL_tmpz00_2185;

						if (BGL_INT64P(BgL_yz00_1268))
							{	/* Llib/bit.scm 568 */
								BgL_tmpz00_2185 = BgL_yz00_1268;
							}
						else
							{
								obj_t BgL_auxz00_2188;

								BgL_auxz00_2188 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(26445L), BGl_string1721z00zz__bitz00,
									BGl_string1707z00zz__bitz00, BgL_yz00_1268);
								FAILURE(BgL_auxz00_2188, BFALSE, BFALSE);
							}
						BgL_auxz00_2184 = BGL_BINT64_TO_INT64(BgL_tmpz00_2185);
					}
					{	/* Llib/bit.scm 568 */
						obj_t BgL_tmpz00_2176;

						if (BGL_INT64P(BgL_xz00_1267))
							{	/* Llib/bit.scm 568 */
								BgL_tmpz00_2176 = BgL_xz00_1267;
							}
						else
							{
								obj_t BgL_auxz00_2179;

								BgL_auxz00_2179 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(26445L), BGl_string1721z00zz__bitz00,
									BGl_string1707z00zz__bitz00, BgL_xz00_1267);
								FAILURE(BgL_auxz00_2179, BFALSE, BFALSE);
							}
						BgL_auxz00_2175 = BGL_BINT64_TO_INT64(BgL_tmpz00_2176);
					}
					BgL_tmpz00_2174 =
						BGl_bitzd2ands64zd2zz__bitz00(BgL_auxz00_2175, BgL_auxz00_2184);
				}
				return BGL_INT64_TO_BINT64(BgL_tmpz00_2174);
			}
		}

	}



/* bit-andu64 */
	BGL_EXPORTED_DEF uint64_t BGl_bitzd2andu64zd2zz__bitz00(uint64_t BgL_xz00_47,
		uint64_t BgL_yz00_48)
	{
		{	/* Llib/bit.scm 569 */
			return (BgL_xz00_47 & BgL_yz00_48);
		}

	}



/* &bit-andu64 */
	obj_t BGl_z62bitzd2andu64zb0zz__bitz00(obj_t BgL_envz00_1269,
		obj_t BgL_xz00_1270, obj_t BgL_yz00_1271)
	{
		{	/* Llib/bit.scm 569 */
			{	/* Llib/bit.scm 569 */
				uint64_t BgL_tmpz00_2196;

				{	/* Llib/bit.scm 569 */
					uint64_t BgL_auxz00_2206;
					uint64_t BgL_auxz00_2197;

					{	/* Llib/bit.scm 569 */
						obj_t BgL_tmpz00_2207;

						if (BGL_UINT64P(BgL_yz00_1271))
							{	/* Llib/bit.scm 569 */
								BgL_tmpz00_2207 = BgL_yz00_1271;
							}
						else
							{
								obj_t BgL_auxz00_2210;

								BgL_auxz00_2210 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(26495L), BGl_string1722z00zz__bitz00,
									BGl_string1709z00zz__bitz00, BgL_yz00_1271);
								FAILURE(BgL_auxz00_2210, BFALSE, BFALSE);
							}
						BgL_auxz00_2206 = BGL_BINT64_TO_INT64(BgL_tmpz00_2207);
					}
					{	/* Llib/bit.scm 569 */
						obj_t BgL_tmpz00_2198;

						if (BGL_UINT64P(BgL_xz00_1270))
							{	/* Llib/bit.scm 569 */
								BgL_tmpz00_2198 = BgL_xz00_1270;
							}
						else
							{
								obj_t BgL_auxz00_2201;

								BgL_auxz00_2201 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(26495L), BGl_string1722z00zz__bitz00,
									BGl_string1709z00zz__bitz00, BgL_xz00_1270);
								FAILURE(BgL_auxz00_2201, BFALSE, BFALSE);
							}
						BgL_auxz00_2197 = BGL_BINT64_TO_INT64(BgL_tmpz00_2198);
					}
					BgL_tmpz00_2196 =
						BGl_bitzd2andu64zd2zz__bitz00(BgL_auxz00_2197, BgL_auxz00_2206);
				}
				return BGL_UINT64_TO_BUINT64(BgL_tmpz00_2196);
			}
		}

	}



/* bit-andbx */
	BGL_EXPORTED_DEF obj_t BGl_bitzd2andbxzd2zz__bitz00(obj_t BgL_xz00_49,
		obj_t BgL_yz00_50)
	{
		{	/* Llib/bit.scm 570 */
			BGL_TAIL return bgl_bignum_and(BgL_xz00_49, BgL_yz00_50);
		}

	}



/* &bit-andbx */
	obj_t BGl_z62bitzd2andbxzb0zz__bitz00(obj_t BgL_envz00_1272,
		obj_t BgL_xz00_1273, obj_t BgL_yz00_1274)
	{
		{	/* Llib/bit.scm 570 */
			{	/* Llib/bit.scm 570 */
				obj_t BgL_auxz00_2225;
				obj_t BgL_auxz00_2218;

				if (BIGNUMP(BgL_yz00_1274))
					{	/* Llib/bit.scm 570 */
						BgL_auxz00_2225 = BgL_yz00_1274;
					}
				else
					{
						obj_t BgL_auxz00_2228;

						BgL_auxz00_2228 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
							BINT(26544L), BGl_string1723z00zz__bitz00,
							BGl_string1711z00zz__bitz00, BgL_yz00_1274);
						FAILURE(BgL_auxz00_2228, BFALSE, BFALSE);
					}
				if (BIGNUMP(BgL_xz00_1273))
					{	/* Llib/bit.scm 570 */
						BgL_auxz00_2218 = BgL_xz00_1273;
					}
				else
					{
						obj_t BgL_auxz00_2221;

						BgL_auxz00_2221 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
							BINT(26544L), BGl_string1723z00zz__bitz00,
							BGl_string1711z00zz__bitz00, BgL_xz00_1273);
						FAILURE(BgL_auxz00_2221, BFALSE, BFALSE);
					}
				return BGl_bitzd2andbxzd2zz__bitz00(BgL_auxz00_2218, BgL_auxz00_2225);
			}
		}

	}



/* bit-maskbx */
	BGL_EXPORTED_DEF obj_t BGl_bitzd2maskbxzd2zz__bitz00(obj_t BgL_xz00_51,
		long BgL_nz00_52)
	{
		{	/* Llib/bit.scm 572 */
			BGL_TAIL return bgl_bignum_mask(BgL_xz00_51, BgL_nz00_52);
		}

	}



/* &bit-maskbx */
	obj_t BGl_z62bitzd2maskbxzb0zz__bitz00(obj_t BgL_envz00_1275,
		obj_t BgL_xz00_1276, obj_t BgL_nz00_1277)
	{
		{	/* Llib/bit.scm 572 */
			{	/* Llib/bit.scm 572 */
				long BgL_auxz00_2241;
				obj_t BgL_auxz00_2234;

				{	/* Llib/bit.scm 572 */
					obj_t BgL_tmpz00_2242;

					if (INTEGERP(BgL_nz00_1277))
						{	/* Llib/bit.scm 572 */
							BgL_tmpz00_2242 = BgL_nz00_1277;
						}
					else
						{
							obj_t BgL_auxz00_2245;

							BgL_auxz00_2245 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
								BINT(26594L), BGl_string1724z00zz__bitz00,
								BGl_string1689z00zz__bitz00, BgL_nz00_1277);
							FAILURE(BgL_auxz00_2245, BFALSE, BFALSE);
						}
					BgL_auxz00_2241 = (long) CINT(BgL_tmpz00_2242);
				}
				if (BIGNUMP(BgL_xz00_1276))
					{	/* Llib/bit.scm 572 */
						BgL_auxz00_2234 = BgL_xz00_1276;
					}
				else
					{
						obj_t BgL_auxz00_2237;

						BgL_auxz00_2237 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
							BINT(26594L), BGl_string1724z00zz__bitz00,
							BGl_string1711z00zz__bitz00, BgL_xz00_1276);
						FAILURE(BgL_auxz00_2237, BFALSE, BFALSE);
					}
				return BGl_bitzd2maskbxzd2zz__bitz00(BgL_auxz00_2234, BgL_auxz00_2241);
			}
		}

	}



/* bit-xor */
	BGL_EXPORTED_DEF long BGl_bitzd2xorzd2zz__bitz00(long BgL_xz00_53,
		long BgL_yz00_54)
	{
		{	/* Llib/bit.scm 577 */
			return (BgL_xz00_53 ^ BgL_yz00_54);
		}

	}



/* &bit-xor */
	obj_t BGl_z62bitzd2xorzb0zz__bitz00(obj_t BgL_envz00_1278,
		obj_t BgL_xz00_1279, obj_t BgL_yz00_1280)
	{
		{	/* Llib/bit.scm 577 */
			{	/* Llib/bit.scm 577 */
				long BgL_tmpz00_2252;

				{	/* Llib/bit.scm 577 */
					long BgL_auxz00_2262;
					long BgL_auxz00_2253;

					{	/* Llib/bit.scm 577 */
						obj_t BgL_tmpz00_2263;

						if (INTEGERP(BgL_yz00_1280))
							{	/* Llib/bit.scm 577 */
								BgL_tmpz00_2263 = BgL_yz00_1280;
							}
						else
							{
								obj_t BgL_auxz00_2266;

								BgL_auxz00_2266 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(26864L), BGl_string1725z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_yz00_1280);
								FAILURE(BgL_auxz00_2266, BFALSE, BFALSE);
							}
						BgL_auxz00_2262 = (long) CINT(BgL_tmpz00_2263);
					}
					{	/* Llib/bit.scm 577 */
						obj_t BgL_tmpz00_2254;

						if (INTEGERP(BgL_xz00_1279))
							{	/* Llib/bit.scm 577 */
								BgL_tmpz00_2254 = BgL_xz00_1279;
							}
						else
							{
								obj_t BgL_auxz00_2257;

								BgL_auxz00_2257 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(26864L), BGl_string1725z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_xz00_1279);
								FAILURE(BgL_auxz00_2257, BFALSE, BFALSE);
							}
						BgL_auxz00_2253 = (long) CINT(BgL_tmpz00_2254);
					}
					BgL_tmpz00_2252 =
						BGl_bitzd2xorzd2zz__bitz00(BgL_auxz00_2253, BgL_auxz00_2262);
				}
				return BINT(BgL_tmpz00_2252);
			}
		}

	}



/* bit-xorelong */
	BGL_EXPORTED_DEF long BGl_bitzd2xorelongzd2zz__bitz00(long BgL_xz00_55,
		long BgL_yz00_56)
	{
		{	/* Llib/bit.scm 578 */
			return (BgL_xz00_55 ^ BgL_yz00_56);
		}

	}



/* &bit-xorelong */
	obj_t BGl_z62bitzd2xorelongzb0zz__bitz00(obj_t BgL_envz00_1281,
		obj_t BgL_xz00_1282, obj_t BgL_yz00_1283)
	{
		{	/* Llib/bit.scm 578 */
			{	/* Llib/bit.scm 578 */
				long BgL_tmpz00_2274;

				{	/* Llib/bit.scm 578 */
					long BgL_auxz00_2284;
					long BgL_auxz00_2275;

					{	/* Llib/bit.scm 578 */
						obj_t BgL_tmpz00_2285;

						if (ELONGP(BgL_yz00_1283))
							{	/* Llib/bit.scm 578 */
								BgL_tmpz00_2285 = BgL_yz00_1283;
							}
						else
							{
								obj_t BgL_auxz00_2288;

								BgL_auxz00_2288 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(26913L), BGl_string1726z00zz__bitz00,
									BGl_string1691z00zz__bitz00, BgL_yz00_1283);
								FAILURE(BgL_auxz00_2288, BFALSE, BFALSE);
							}
						BgL_auxz00_2284 = BELONG_TO_LONG(BgL_tmpz00_2285);
					}
					{	/* Llib/bit.scm 578 */
						obj_t BgL_tmpz00_2276;

						if (ELONGP(BgL_xz00_1282))
							{	/* Llib/bit.scm 578 */
								BgL_tmpz00_2276 = BgL_xz00_1282;
							}
						else
							{
								obj_t BgL_auxz00_2279;

								BgL_auxz00_2279 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(26913L), BGl_string1726z00zz__bitz00,
									BGl_string1691z00zz__bitz00, BgL_xz00_1282);
								FAILURE(BgL_auxz00_2279, BFALSE, BFALSE);
							}
						BgL_auxz00_2275 = BELONG_TO_LONG(BgL_tmpz00_2276);
					}
					BgL_tmpz00_2274 =
						BGl_bitzd2xorelongzd2zz__bitz00(BgL_auxz00_2275, BgL_auxz00_2284);
				}
				return make_belong(BgL_tmpz00_2274);
			}
		}

	}



/* bit-xorllong */
	BGL_EXPORTED_DEF BGL_LONGLONG_T BGl_bitzd2xorllongzd2zz__bitz00(BGL_LONGLONG_T
		BgL_xz00_57, BGL_LONGLONG_T BgL_yz00_58)
	{
		{	/* Llib/bit.scm 579 */
			return (BgL_xz00_57 ^ BgL_yz00_58);
		}

	}



/* &bit-xorllong */
	obj_t BGl_z62bitzd2xorllongzb0zz__bitz00(obj_t BgL_envz00_1284,
		obj_t BgL_xz00_1285, obj_t BgL_yz00_1286)
	{
		{	/* Llib/bit.scm 579 */
			{	/* Llib/bit.scm 579 */
				BGL_LONGLONG_T BgL_tmpz00_2296;

				{	/* Llib/bit.scm 579 */
					BGL_LONGLONG_T BgL_auxz00_2306;
					BGL_LONGLONG_T BgL_auxz00_2297;

					{	/* Llib/bit.scm 579 */
						obj_t BgL_tmpz00_2307;

						if (LLONGP(BgL_yz00_1286))
							{	/* Llib/bit.scm 579 */
								BgL_tmpz00_2307 = BgL_yz00_1286;
							}
						else
							{
								obj_t BgL_auxz00_2310;

								BgL_auxz00_2310 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(26967L), BGl_string1727z00zz__bitz00,
									BGl_string1693z00zz__bitz00, BgL_yz00_1286);
								FAILURE(BgL_auxz00_2310, BFALSE, BFALSE);
							}
						BgL_auxz00_2306 = BLLONG_TO_LLONG(BgL_tmpz00_2307);
					}
					{	/* Llib/bit.scm 579 */
						obj_t BgL_tmpz00_2298;

						if (LLONGP(BgL_xz00_1285))
							{	/* Llib/bit.scm 579 */
								BgL_tmpz00_2298 = BgL_xz00_1285;
							}
						else
							{
								obj_t BgL_auxz00_2301;

								BgL_auxz00_2301 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(26967L), BGl_string1727z00zz__bitz00,
									BGl_string1693z00zz__bitz00, BgL_xz00_1285);
								FAILURE(BgL_auxz00_2301, BFALSE, BFALSE);
							}
						BgL_auxz00_2297 = BLLONG_TO_LLONG(BgL_tmpz00_2298);
					}
					BgL_tmpz00_2296 =
						BGl_bitzd2xorllongzd2zz__bitz00(BgL_auxz00_2297, BgL_auxz00_2306);
				}
				return make_bllong(BgL_tmpz00_2296);
			}
		}

	}



/* bit-xors8 */
	BGL_EXPORTED_DEF int8_t BGl_bitzd2xors8zd2zz__bitz00(int8_t BgL_xz00_59,
		int8_t BgL_yz00_60)
	{
		{	/* Llib/bit.scm 580 */
			return (BgL_xz00_59 ^ BgL_yz00_60);
		}

	}



/* &bit-xors8 */
	obj_t BGl_z62bitzd2xors8zb0zz__bitz00(obj_t BgL_envz00_1287,
		obj_t BgL_xz00_1288, obj_t BgL_yz00_1289)
	{
		{	/* Llib/bit.scm 580 */
			{	/* Llib/bit.scm 580 */
				int8_t BgL_tmpz00_2318;

				{	/* Llib/bit.scm 580 */
					int8_t BgL_auxz00_2328;
					int8_t BgL_auxz00_2319;

					{	/* Llib/bit.scm 580 */
						obj_t BgL_tmpz00_2329;

						if (BGL_INT8P(BgL_yz00_1289))
							{	/* Llib/bit.scm 580 */
								BgL_tmpz00_2329 = BgL_yz00_1289;
							}
						else
							{
								obj_t BgL_auxz00_2332;

								BgL_auxz00_2332 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(27018L), BGl_string1728z00zz__bitz00,
									BGl_string1695z00zz__bitz00, BgL_yz00_1289);
								FAILURE(BgL_auxz00_2332, BFALSE, BFALSE);
							}
						BgL_auxz00_2328 = BGL_BINT8_TO_INT8(BgL_tmpz00_2329);
					}
					{	/* Llib/bit.scm 580 */
						obj_t BgL_tmpz00_2320;

						if (BGL_INT8P(BgL_xz00_1288))
							{	/* Llib/bit.scm 580 */
								BgL_tmpz00_2320 = BgL_xz00_1288;
							}
						else
							{
								obj_t BgL_auxz00_2323;

								BgL_auxz00_2323 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(27018L), BGl_string1728z00zz__bitz00,
									BGl_string1695z00zz__bitz00, BgL_xz00_1288);
								FAILURE(BgL_auxz00_2323, BFALSE, BFALSE);
							}
						BgL_auxz00_2319 = BGL_BINT8_TO_INT8(BgL_tmpz00_2320);
					}
					BgL_tmpz00_2318 =
						BGl_bitzd2xors8zd2zz__bitz00(BgL_auxz00_2319, BgL_auxz00_2328);
				}
				return BGL_INT8_TO_BINT8(BgL_tmpz00_2318);
			}
		}

	}



/* bit-xoru8 */
	BGL_EXPORTED_DEF uint8_t BGl_bitzd2xoru8zd2zz__bitz00(uint8_t BgL_xz00_61,
		uint8_t BgL_yz00_62)
	{
		{	/* Llib/bit.scm 581 */
			return (BgL_xz00_61 ^ BgL_yz00_62);
		}

	}



/* &bit-xoru8 */
	obj_t BGl_z62bitzd2xoru8zb0zz__bitz00(obj_t BgL_envz00_1290,
		obj_t BgL_xz00_1291, obj_t BgL_yz00_1292)
	{
		{	/* Llib/bit.scm 581 */
			{	/* Llib/bit.scm 581 */
				uint8_t BgL_tmpz00_2340;

				{	/* Llib/bit.scm 581 */
					uint8_t BgL_auxz00_2350;
					uint8_t BgL_auxz00_2341;

					{	/* Llib/bit.scm 581 */
						obj_t BgL_tmpz00_2351;

						if (BGL_UINT8P(BgL_yz00_1292))
							{	/* Llib/bit.scm 581 */
								BgL_tmpz00_2351 = BgL_yz00_1292;
							}
						else
							{
								obj_t BgL_auxz00_2354;

								BgL_auxz00_2354 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(27066L), BGl_string1729z00zz__bitz00,
									BGl_string1697z00zz__bitz00, BgL_yz00_1292);
								FAILURE(BgL_auxz00_2354, BFALSE, BFALSE);
							}
						BgL_auxz00_2350 = BGL_BUINT8_TO_UINT8(BgL_tmpz00_2351);
					}
					{	/* Llib/bit.scm 581 */
						obj_t BgL_tmpz00_2342;

						if (BGL_UINT8P(BgL_xz00_1291))
							{	/* Llib/bit.scm 581 */
								BgL_tmpz00_2342 = BgL_xz00_1291;
							}
						else
							{
								obj_t BgL_auxz00_2345;

								BgL_auxz00_2345 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(27066L), BGl_string1729z00zz__bitz00,
									BGl_string1697z00zz__bitz00, BgL_xz00_1291);
								FAILURE(BgL_auxz00_2345, BFALSE, BFALSE);
							}
						BgL_auxz00_2341 = BGL_BUINT8_TO_UINT8(BgL_tmpz00_2342);
					}
					BgL_tmpz00_2340 =
						BGl_bitzd2xoru8zd2zz__bitz00(BgL_auxz00_2341, BgL_auxz00_2350);
				}
				return BGL_UINT8_TO_BUINT8(BgL_tmpz00_2340);
			}
		}

	}



/* bit-xors16 */
	BGL_EXPORTED_DEF int16_t BGl_bitzd2xors16zd2zz__bitz00(int16_t BgL_xz00_63,
		int16_t BgL_yz00_64)
	{
		{	/* Llib/bit.scm 582 */
			return (BgL_xz00_63 ^ BgL_yz00_64);
		}

	}



/* &bit-xors16 */
	obj_t BGl_z62bitzd2xors16zb0zz__bitz00(obj_t BgL_envz00_1293,
		obj_t BgL_xz00_1294, obj_t BgL_yz00_1295)
	{
		{	/* Llib/bit.scm 582 */
			{	/* Llib/bit.scm 582 */
				int16_t BgL_tmpz00_2362;

				{	/* Llib/bit.scm 582 */
					int16_t BgL_auxz00_2372;
					int16_t BgL_auxz00_2363;

					{	/* Llib/bit.scm 582 */
						obj_t BgL_tmpz00_2373;

						if (BGL_INT16P(BgL_yz00_1295))
							{	/* Llib/bit.scm 582 */
								BgL_tmpz00_2373 = BgL_yz00_1295;
							}
						else
							{
								obj_t BgL_auxz00_2376;

								BgL_auxz00_2376 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(27115L), BGl_string1730z00zz__bitz00,
									BGl_string1699z00zz__bitz00, BgL_yz00_1295);
								FAILURE(BgL_auxz00_2376, BFALSE, BFALSE);
							}
						BgL_auxz00_2372 = BGL_BINT16_TO_INT16(BgL_tmpz00_2373);
					}
					{	/* Llib/bit.scm 582 */
						obj_t BgL_tmpz00_2364;

						if (BGL_INT16P(BgL_xz00_1294))
							{	/* Llib/bit.scm 582 */
								BgL_tmpz00_2364 = BgL_xz00_1294;
							}
						else
							{
								obj_t BgL_auxz00_2367;

								BgL_auxz00_2367 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(27115L), BGl_string1730z00zz__bitz00,
									BGl_string1699z00zz__bitz00, BgL_xz00_1294);
								FAILURE(BgL_auxz00_2367, BFALSE, BFALSE);
							}
						BgL_auxz00_2363 = BGL_BINT16_TO_INT16(BgL_tmpz00_2364);
					}
					BgL_tmpz00_2362 =
						BGl_bitzd2xors16zd2zz__bitz00(BgL_auxz00_2363, BgL_auxz00_2372);
				}
				return BGL_INT16_TO_BINT16(BgL_tmpz00_2362);
			}
		}

	}



/* bit-xoru16 */
	BGL_EXPORTED_DEF uint16_t BGl_bitzd2xoru16zd2zz__bitz00(uint16_t BgL_xz00_65,
		uint16_t BgL_yz00_66)
	{
		{	/* Llib/bit.scm 583 */
			return (BgL_xz00_65 ^ BgL_yz00_66);
		}

	}



/* &bit-xoru16 */
	obj_t BGl_z62bitzd2xoru16zb0zz__bitz00(obj_t BgL_envz00_1296,
		obj_t BgL_xz00_1297, obj_t BgL_yz00_1298)
	{
		{	/* Llib/bit.scm 583 */
			{	/* Llib/bit.scm 583 */
				uint16_t BgL_tmpz00_2384;

				{	/* Llib/bit.scm 583 */
					uint16_t BgL_auxz00_2394;
					uint16_t BgL_auxz00_2385;

					{	/* Llib/bit.scm 583 */
						obj_t BgL_tmpz00_2395;

						if (BGL_UINT16P(BgL_yz00_1298))
							{	/* Llib/bit.scm 583 */
								BgL_tmpz00_2395 = BgL_yz00_1298;
							}
						else
							{
								obj_t BgL_auxz00_2398;

								BgL_auxz00_2398 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(27165L), BGl_string1731z00zz__bitz00,
									BGl_string1701z00zz__bitz00, BgL_yz00_1298);
								FAILURE(BgL_auxz00_2398, BFALSE, BFALSE);
							}
						BgL_auxz00_2394 = BGL_BUINT16_TO_UINT16(BgL_tmpz00_2395);
					}
					{	/* Llib/bit.scm 583 */
						obj_t BgL_tmpz00_2386;

						if (BGL_UINT16P(BgL_xz00_1297))
							{	/* Llib/bit.scm 583 */
								BgL_tmpz00_2386 = BgL_xz00_1297;
							}
						else
							{
								obj_t BgL_auxz00_2389;

								BgL_auxz00_2389 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(27165L), BGl_string1731z00zz__bitz00,
									BGl_string1701z00zz__bitz00, BgL_xz00_1297);
								FAILURE(BgL_auxz00_2389, BFALSE, BFALSE);
							}
						BgL_auxz00_2385 = BGL_BUINT16_TO_UINT16(BgL_tmpz00_2386);
					}
					BgL_tmpz00_2384 =
						BGl_bitzd2xoru16zd2zz__bitz00(BgL_auxz00_2385, BgL_auxz00_2394);
				}
				return BGL_UINT16_TO_BUINT16(BgL_tmpz00_2384);
			}
		}

	}



/* bit-xors32 */
	BGL_EXPORTED_DEF int32_t BGl_bitzd2xors32zd2zz__bitz00(int32_t BgL_xz00_67,
		int32_t BgL_yz00_68)
	{
		{	/* Llib/bit.scm 584 */
			return (BgL_xz00_67 ^ BgL_yz00_68);
		}

	}



/* &bit-xors32 */
	obj_t BGl_z62bitzd2xors32zb0zz__bitz00(obj_t BgL_envz00_1299,
		obj_t BgL_xz00_1300, obj_t BgL_yz00_1301)
	{
		{	/* Llib/bit.scm 584 */
			{	/* Llib/bit.scm 584 */
				int32_t BgL_tmpz00_2406;

				{	/* Llib/bit.scm 584 */
					int32_t BgL_auxz00_2416;
					int32_t BgL_auxz00_2407;

					{	/* Llib/bit.scm 584 */
						obj_t BgL_tmpz00_2417;

						if (BGL_INT32P(BgL_yz00_1301))
							{	/* Llib/bit.scm 584 */
								BgL_tmpz00_2417 = BgL_yz00_1301;
							}
						else
							{
								obj_t BgL_auxz00_2420;

								BgL_auxz00_2420 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(27215L), BGl_string1732z00zz__bitz00,
									BGl_string1703z00zz__bitz00, BgL_yz00_1301);
								FAILURE(BgL_auxz00_2420, BFALSE, BFALSE);
							}
						BgL_auxz00_2416 = BGL_BINT32_TO_INT32(BgL_tmpz00_2417);
					}
					{	/* Llib/bit.scm 584 */
						obj_t BgL_tmpz00_2408;

						if (BGL_INT32P(BgL_xz00_1300))
							{	/* Llib/bit.scm 584 */
								BgL_tmpz00_2408 = BgL_xz00_1300;
							}
						else
							{
								obj_t BgL_auxz00_2411;

								BgL_auxz00_2411 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(27215L), BGl_string1732z00zz__bitz00,
									BGl_string1703z00zz__bitz00, BgL_xz00_1300);
								FAILURE(BgL_auxz00_2411, BFALSE, BFALSE);
							}
						BgL_auxz00_2407 = BGL_BINT32_TO_INT32(BgL_tmpz00_2408);
					}
					BgL_tmpz00_2406 =
						BGl_bitzd2xors32zd2zz__bitz00(BgL_auxz00_2407, BgL_auxz00_2416);
				}
				return BGL_INT32_TO_BINT32(BgL_tmpz00_2406);
			}
		}

	}



/* bit-xoru32 */
	BGL_EXPORTED_DEF uint32_t BGl_bitzd2xoru32zd2zz__bitz00(uint32_t BgL_xz00_69,
		uint32_t BgL_yz00_70)
	{
		{	/* Llib/bit.scm 585 */
			return (BgL_xz00_69 ^ BgL_yz00_70);
		}

	}



/* &bit-xoru32 */
	obj_t BGl_z62bitzd2xoru32zb0zz__bitz00(obj_t BgL_envz00_1302,
		obj_t BgL_xz00_1303, obj_t BgL_yz00_1304)
	{
		{	/* Llib/bit.scm 585 */
			{	/* Llib/bit.scm 585 */
				uint32_t BgL_tmpz00_2428;

				{	/* Llib/bit.scm 585 */
					uint32_t BgL_auxz00_2438;
					uint32_t BgL_auxz00_2429;

					{	/* Llib/bit.scm 585 */
						obj_t BgL_tmpz00_2439;

						if (BGL_UINT32P(BgL_yz00_1304))
							{	/* Llib/bit.scm 585 */
								BgL_tmpz00_2439 = BgL_yz00_1304;
							}
						else
							{
								obj_t BgL_auxz00_2442;

								BgL_auxz00_2442 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(27265L), BGl_string1733z00zz__bitz00,
									BGl_string1705z00zz__bitz00, BgL_yz00_1304);
								FAILURE(BgL_auxz00_2442, BFALSE, BFALSE);
							}
						BgL_auxz00_2438 = BGL_BUINT32_TO_UINT32(BgL_tmpz00_2439);
					}
					{	/* Llib/bit.scm 585 */
						obj_t BgL_tmpz00_2430;

						if (BGL_UINT32P(BgL_xz00_1303))
							{	/* Llib/bit.scm 585 */
								BgL_tmpz00_2430 = BgL_xz00_1303;
							}
						else
							{
								obj_t BgL_auxz00_2433;

								BgL_auxz00_2433 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(27265L), BGl_string1733z00zz__bitz00,
									BGl_string1705z00zz__bitz00, BgL_xz00_1303);
								FAILURE(BgL_auxz00_2433, BFALSE, BFALSE);
							}
						BgL_auxz00_2429 = BGL_BUINT32_TO_UINT32(BgL_tmpz00_2430);
					}
					BgL_tmpz00_2428 =
						BGl_bitzd2xoru32zd2zz__bitz00(BgL_auxz00_2429, BgL_auxz00_2438);
				}
				return BGL_UINT32_TO_BUINT32(BgL_tmpz00_2428);
			}
		}

	}



/* bit-xors64 */
	BGL_EXPORTED_DEF int64_t BGl_bitzd2xors64zd2zz__bitz00(int64_t BgL_xz00_71,
		int64_t BgL_yz00_72)
	{
		{	/* Llib/bit.scm 586 */
			return (BgL_xz00_71 ^ BgL_yz00_72);
		}

	}



/* &bit-xors64 */
	obj_t BGl_z62bitzd2xors64zb0zz__bitz00(obj_t BgL_envz00_1305,
		obj_t BgL_xz00_1306, obj_t BgL_yz00_1307)
	{
		{	/* Llib/bit.scm 586 */
			{	/* Llib/bit.scm 586 */
				int64_t BgL_tmpz00_2450;

				{	/* Llib/bit.scm 586 */
					int64_t BgL_auxz00_2460;
					int64_t BgL_auxz00_2451;

					{	/* Llib/bit.scm 586 */
						obj_t BgL_tmpz00_2461;

						if (BGL_INT64P(BgL_yz00_1307))
							{	/* Llib/bit.scm 586 */
								BgL_tmpz00_2461 = BgL_yz00_1307;
							}
						else
							{
								obj_t BgL_auxz00_2464;

								BgL_auxz00_2464 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(27315L), BGl_string1734z00zz__bitz00,
									BGl_string1707z00zz__bitz00, BgL_yz00_1307);
								FAILURE(BgL_auxz00_2464, BFALSE, BFALSE);
							}
						BgL_auxz00_2460 = BGL_BINT64_TO_INT64(BgL_tmpz00_2461);
					}
					{	/* Llib/bit.scm 586 */
						obj_t BgL_tmpz00_2452;

						if (BGL_INT64P(BgL_xz00_1306))
							{	/* Llib/bit.scm 586 */
								BgL_tmpz00_2452 = BgL_xz00_1306;
							}
						else
							{
								obj_t BgL_auxz00_2455;

								BgL_auxz00_2455 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(27315L), BGl_string1734z00zz__bitz00,
									BGl_string1707z00zz__bitz00, BgL_xz00_1306);
								FAILURE(BgL_auxz00_2455, BFALSE, BFALSE);
							}
						BgL_auxz00_2451 = BGL_BINT64_TO_INT64(BgL_tmpz00_2452);
					}
					BgL_tmpz00_2450 =
						BGl_bitzd2xors64zd2zz__bitz00(BgL_auxz00_2451, BgL_auxz00_2460);
				}
				return BGL_INT64_TO_BINT64(BgL_tmpz00_2450);
			}
		}

	}



/* bit-xoru64 */
	BGL_EXPORTED_DEF uint64_t BGl_bitzd2xoru64zd2zz__bitz00(uint64_t BgL_xz00_73,
		uint64_t BgL_yz00_74)
	{
		{	/* Llib/bit.scm 587 */
			return (BgL_xz00_73 ^ BgL_yz00_74);
		}

	}



/* &bit-xoru64 */
	obj_t BGl_z62bitzd2xoru64zb0zz__bitz00(obj_t BgL_envz00_1308,
		obj_t BgL_xz00_1309, obj_t BgL_yz00_1310)
	{
		{	/* Llib/bit.scm 587 */
			{	/* Llib/bit.scm 587 */
				uint64_t BgL_tmpz00_2472;

				{	/* Llib/bit.scm 587 */
					uint64_t BgL_auxz00_2482;
					uint64_t BgL_auxz00_2473;

					{	/* Llib/bit.scm 587 */
						obj_t BgL_tmpz00_2483;

						if (BGL_UINT64P(BgL_yz00_1310))
							{	/* Llib/bit.scm 587 */
								BgL_tmpz00_2483 = BgL_yz00_1310;
							}
						else
							{
								obj_t BgL_auxz00_2486;

								BgL_auxz00_2486 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(27365L), BGl_string1735z00zz__bitz00,
									BGl_string1709z00zz__bitz00, BgL_yz00_1310);
								FAILURE(BgL_auxz00_2486, BFALSE, BFALSE);
							}
						BgL_auxz00_2482 = BGL_BINT64_TO_INT64(BgL_tmpz00_2483);
					}
					{	/* Llib/bit.scm 587 */
						obj_t BgL_tmpz00_2474;

						if (BGL_UINT64P(BgL_xz00_1309))
							{	/* Llib/bit.scm 587 */
								BgL_tmpz00_2474 = BgL_xz00_1309;
							}
						else
							{
								obj_t BgL_auxz00_2477;

								BgL_auxz00_2477 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(27365L), BGl_string1735z00zz__bitz00,
									BGl_string1709z00zz__bitz00, BgL_xz00_1309);
								FAILURE(BgL_auxz00_2477, BFALSE, BFALSE);
							}
						BgL_auxz00_2473 = BGL_BINT64_TO_INT64(BgL_tmpz00_2474);
					}
					BgL_tmpz00_2472 =
						BGl_bitzd2xoru64zd2zz__bitz00(BgL_auxz00_2473, BgL_auxz00_2482);
				}
				return BGL_UINT64_TO_BUINT64(BgL_tmpz00_2472);
			}
		}

	}



/* bit-xorbx */
	BGL_EXPORTED_DEF obj_t BGl_bitzd2xorbxzd2zz__bitz00(obj_t BgL_xz00_75,
		obj_t BgL_yz00_76)
	{
		{	/* Llib/bit.scm 588 */
			BGL_TAIL return bgl_bignum_xor(BgL_xz00_75, BgL_yz00_76);
		}

	}



/* &bit-xorbx */
	obj_t BGl_z62bitzd2xorbxzb0zz__bitz00(obj_t BgL_envz00_1311,
		obj_t BgL_xz00_1312, obj_t BgL_yz00_1313)
	{
		{	/* Llib/bit.scm 588 */
			{	/* Llib/bit.scm 588 */
				obj_t BgL_auxz00_2501;
				obj_t BgL_auxz00_2494;

				if (BIGNUMP(BgL_yz00_1313))
					{	/* Llib/bit.scm 588 */
						BgL_auxz00_2501 = BgL_yz00_1313;
					}
				else
					{
						obj_t BgL_auxz00_2504;

						BgL_auxz00_2504 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
							BINT(27414L), BGl_string1736z00zz__bitz00,
							BGl_string1711z00zz__bitz00, BgL_yz00_1313);
						FAILURE(BgL_auxz00_2504, BFALSE, BFALSE);
					}
				if (BIGNUMP(BgL_xz00_1312))
					{	/* Llib/bit.scm 588 */
						BgL_auxz00_2494 = BgL_xz00_1312;
					}
				else
					{
						obj_t BgL_auxz00_2497;

						BgL_auxz00_2497 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
							BINT(27414L), BGl_string1736z00zz__bitz00,
							BGl_string1711z00zz__bitz00, BgL_xz00_1312);
						FAILURE(BgL_auxz00_2497, BFALSE, BFALSE);
					}
				return BGl_bitzd2xorbxzd2zz__bitz00(BgL_auxz00_2494, BgL_auxz00_2501);
			}
		}

	}



/* bit-not */
	BGL_EXPORTED_DEF long BGl_bitzd2notzd2zz__bitz00(long BgL_xz00_77)
	{
		{	/* Llib/bit.scm 593 */
			return ~(BgL_xz00_77);
		}

	}



/* &bit-not */
	obj_t BGl_z62bitzd2notzb0zz__bitz00(obj_t BgL_envz00_1314,
		obj_t BgL_xz00_1315)
	{
		{	/* Llib/bit.scm 593 */
			{	/* Llib/bit.scm 593 */
				long BgL_tmpz00_2510;

				{	/* Llib/bit.scm 593 */
					long BgL_auxz00_2511;

					{	/* Llib/bit.scm 593 */
						obj_t BgL_tmpz00_2512;

						if (INTEGERP(BgL_xz00_1315))
							{	/* Llib/bit.scm 593 */
								BgL_tmpz00_2512 = BgL_xz00_1315;
							}
						else
							{
								obj_t BgL_auxz00_2515;

								BgL_auxz00_2515 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(27681L), BGl_string1737z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_xz00_1315);
								FAILURE(BgL_auxz00_2515, BFALSE, BFALSE);
							}
						BgL_auxz00_2511 = (long) CINT(BgL_tmpz00_2512);
					}
					BgL_tmpz00_2510 = BGl_bitzd2notzd2zz__bitz00(BgL_auxz00_2511);
				}
				return BINT(BgL_tmpz00_2510);
			}
		}

	}



/* bit-notelong */
	BGL_EXPORTED_DEF long BGl_bitzd2notelongzd2zz__bitz00(long BgL_xz00_78)
	{
		{	/* Llib/bit.scm 594 */
			return ~(BgL_xz00_78);
		}

	}



/* &bit-notelong */
	obj_t BGl_z62bitzd2notelongzb0zz__bitz00(obj_t BgL_envz00_1316,
		obj_t BgL_xz00_1317)
	{
		{	/* Llib/bit.scm 594 */
			{	/* Llib/bit.scm 594 */
				long BgL_tmpz00_2523;

				{	/* Llib/bit.scm 594 */
					long BgL_auxz00_2524;

					{	/* Llib/bit.scm 594 */
						obj_t BgL_tmpz00_2525;

						if (ELONGP(BgL_xz00_1317))
							{	/* Llib/bit.scm 594 */
								BgL_tmpz00_2525 = BgL_xz00_1317;
							}
						else
							{
								obj_t BgL_auxz00_2528;

								BgL_auxz00_2528 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(27726L), BGl_string1738z00zz__bitz00,
									BGl_string1691z00zz__bitz00, BgL_xz00_1317);
								FAILURE(BgL_auxz00_2528, BFALSE, BFALSE);
							}
						BgL_auxz00_2524 = BELONG_TO_LONG(BgL_tmpz00_2525);
					}
					BgL_tmpz00_2523 = BGl_bitzd2notelongzd2zz__bitz00(BgL_auxz00_2524);
				}
				return make_belong(BgL_tmpz00_2523);
			}
		}

	}



/* bit-notllong */
	BGL_EXPORTED_DEF BGL_LONGLONG_T BGl_bitzd2notllongzd2zz__bitz00(BGL_LONGLONG_T
		BgL_xz00_79)
	{
		{	/* Llib/bit.scm 595 */
			return ~(BgL_xz00_79);
		}

	}



/* &bit-notllong */
	obj_t BGl_z62bitzd2notllongzb0zz__bitz00(obj_t BgL_envz00_1318,
		obj_t BgL_xz00_1319)
	{
		{	/* Llib/bit.scm 595 */
			{	/* Llib/bit.scm 595 */
				BGL_LONGLONG_T BgL_tmpz00_2536;

				{	/* Llib/bit.scm 595 */
					BGL_LONGLONG_T BgL_auxz00_2537;

					{	/* Llib/bit.scm 595 */
						obj_t BgL_tmpz00_2538;

						if (LLONGP(BgL_xz00_1319))
							{	/* Llib/bit.scm 595 */
								BgL_tmpz00_2538 = BgL_xz00_1319;
							}
						else
							{
								obj_t BgL_auxz00_2541;

								BgL_auxz00_2541 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(27776L), BGl_string1739z00zz__bitz00,
									BGl_string1693z00zz__bitz00, BgL_xz00_1319);
								FAILURE(BgL_auxz00_2541, BFALSE, BFALSE);
							}
						BgL_auxz00_2537 = BLLONG_TO_LLONG(BgL_tmpz00_2538);
					}
					BgL_tmpz00_2536 = BGl_bitzd2notllongzd2zz__bitz00(BgL_auxz00_2537);
				}
				return make_bllong(BgL_tmpz00_2536);
			}
		}

	}



/* bit-nots8 */
	BGL_EXPORTED_DEF int8_t BGl_bitzd2nots8zd2zz__bitz00(int8_t BgL_xz00_80)
	{
		{	/* Llib/bit.scm 596 */
			return ~(BgL_xz00_80);
		}

	}



/* &bit-nots8 */
	obj_t BGl_z62bitzd2nots8zb0zz__bitz00(obj_t BgL_envz00_1320,
		obj_t BgL_xz00_1321)
	{
		{	/* Llib/bit.scm 596 */
			{	/* Llib/bit.scm 596 */
				int8_t BgL_tmpz00_2549;

				{	/* Llib/bit.scm 596 */
					int8_t BgL_auxz00_2550;

					{	/* Llib/bit.scm 596 */
						obj_t BgL_tmpz00_2551;

						if (BGL_INT8P(BgL_xz00_1321))
							{	/* Llib/bit.scm 596 */
								BgL_tmpz00_2551 = BgL_xz00_1321;
							}
						else
							{
								obj_t BgL_auxz00_2554;

								BgL_auxz00_2554 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(27823L), BGl_string1740z00zz__bitz00,
									BGl_string1695z00zz__bitz00, BgL_xz00_1321);
								FAILURE(BgL_auxz00_2554, BFALSE, BFALSE);
							}
						BgL_auxz00_2550 = BGL_BINT8_TO_INT8(BgL_tmpz00_2551);
					}
					BgL_tmpz00_2549 = BGl_bitzd2nots8zd2zz__bitz00(BgL_auxz00_2550);
				}
				return BGL_INT8_TO_BINT8(BgL_tmpz00_2549);
			}
		}

	}



/* bit-notu8 */
	BGL_EXPORTED_DEF uint8_t BGl_bitzd2notu8zd2zz__bitz00(uint8_t BgL_xz00_81)
	{
		{	/* Llib/bit.scm 597 */
			return ~(BgL_xz00_81);
		}

	}



/* &bit-notu8 */
	obj_t BGl_z62bitzd2notu8zb0zz__bitz00(obj_t BgL_envz00_1322,
		obj_t BgL_xz00_1323)
	{
		{	/* Llib/bit.scm 597 */
			{	/* Llib/bit.scm 597 */
				uint8_t BgL_tmpz00_2562;

				{	/* Llib/bit.scm 597 */
					uint8_t BgL_auxz00_2563;

					{	/* Llib/bit.scm 597 */
						obj_t BgL_tmpz00_2564;

						if (BGL_UINT8P(BgL_xz00_1323))
							{	/* Llib/bit.scm 597 */
								BgL_tmpz00_2564 = BgL_xz00_1323;
							}
						else
							{
								obj_t BgL_auxz00_2567;

								BgL_auxz00_2567 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(27867L), BGl_string1741z00zz__bitz00,
									BGl_string1697z00zz__bitz00, BgL_xz00_1323);
								FAILURE(BgL_auxz00_2567, BFALSE, BFALSE);
							}
						BgL_auxz00_2563 = BGL_BUINT8_TO_UINT8(BgL_tmpz00_2564);
					}
					BgL_tmpz00_2562 = BGl_bitzd2notu8zd2zz__bitz00(BgL_auxz00_2563);
				}
				return BGL_UINT8_TO_BUINT8(BgL_tmpz00_2562);
			}
		}

	}



/* bit-nots16 */
	BGL_EXPORTED_DEF int16_t BGl_bitzd2nots16zd2zz__bitz00(int16_t BgL_xz00_82)
	{
		{	/* Llib/bit.scm 598 */
			return ~(BgL_xz00_82);
		}

	}



/* &bit-nots16 */
	obj_t BGl_z62bitzd2nots16zb0zz__bitz00(obj_t BgL_envz00_1324,
		obj_t BgL_xz00_1325)
	{
		{	/* Llib/bit.scm 598 */
			{	/* Llib/bit.scm 598 */
				int16_t BgL_tmpz00_2575;

				{	/* Llib/bit.scm 598 */
					int16_t BgL_auxz00_2576;

					{	/* Llib/bit.scm 598 */
						obj_t BgL_tmpz00_2577;

						if (BGL_INT16P(BgL_xz00_1325))
							{	/* Llib/bit.scm 598 */
								BgL_tmpz00_2577 = BgL_xz00_1325;
							}
						else
							{
								obj_t BgL_auxz00_2580;

								BgL_auxz00_2580 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(27912L), BGl_string1742z00zz__bitz00,
									BGl_string1699z00zz__bitz00, BgL_xz00_1325);
								FAILURE(BgL_auxz00_2580, BFALSE, BFALSE);
							}
						BgL_auxz00_2576 = BGL_BINT16_TO_INT16(BgL_tmpz00_2577);
					}
					BgL_tmpz00_2575 = BGl_bitzd2nots16zd2zz__bitz00(BgL_auxz00_2576);
				}
				return BGL_INT16_TO_BINT16(BgL_tmpz00_2575);
			}
		}

	}



/* bit-notu16 */
	BGL_EXPORTED_DEF uint16_t BGl_bitzd2notu16zd2zz__bitz00(uint16_t BgL_xz00_83)
	{
		{	/* Llib/bit.scm 599 */
			return ~(BgL_xz00_83);
		}

	}



/* &bit-notu16 */
	obj_t BGl_z62bitzd2notu16zb0zz__bitz00(obj_t BgL_envz00_1326,
		obj_t BgL_xz00_1327)
	{
		{	/* Llib/bit.scm 599 */
			{	/* Llib/bit.scm 599 */
				uint16_t BgL_tmpz00_2588;

				{	/* Llib/bit.scm 599 */
					uint16_t BgL_auxz00_2589;

					{	/* Llib/bit.scm 599 */
						obj_t BgL_tmpz00_2590;

						if (BGL_UINT16P(BgL_xz00_1327))
							{	/* Llib/bit.scm 599 */
								BgL_tmpz00_2590 = BgL_xz00_1327;
							}
						else
							{
								obj_t BgL_auxz00_2593;

								BgL_auxz00_2593 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(27958L), BGl_string1743z00zz__bitz00,
									BGl_string1701z00zz__bitz00, BgL_xz00_1327);
								FAILURE(BgL_auxz00_2593, BFALSE, BFALSE);
							}
						BgL_auxz00_2589 = BGL_BUINT16_TO_UINT16(BgL_tmpz00_2590);
					}
					BgL_tmpz00_2588 = BGl_bitzd2notu16zd2zz__bitz00(BgL_auxz00_2589);
				}
				return BGL_UINT16_TO_BUINT16(BgL_tmpz00_2588);
			}
		}

	}



/* bit-nots32 */
	BGL_EXPORTED_DEF int32_t BGl_bitzd2nots32zd2zz__bitz00(int32_t BgL_xz00_84)
	{
		{	/* Llib/bit.scm 600 */
			return ~(BgL_xz00_84);
		}

	}



/* &bit-nots32 */
	obj_t BGl_z62bitzd2nots32zb0zz__bitz00(obj_t BgL_envz00_1328,
		obj_t BgL_xz00_1329)
	{
		{	/* Llib/bit.scm 600 */
			{	/* Llib/bit.scm 600 */
				int32_t BgL_tmpz00_2601;

				{	/* Llib/bit.scm 600 */
					int32_t BgL_auxz00_2602;

					{	/* Llib/bit.scm 600 */
						obj_t BgL_tmpz00_2603;

						if (BGL_INT32P(BgL_xz00_1329))
							{	/* Llib/bit.scm 600 */
								BgL_tmpz00_2603 = BgL_xz00_1329;
							}
						else
							{
								obj_t BgL_auxz00_2606;

								BgL_auxz00_2606 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(28004L), BGl_string1744z00zz__bitz00,
									BGl_string1703z00zz__bitz00, BgL_xz00_1329);
								FAILURE(BgL_auxz00_2606, BFALSE, BFALSE);
							}
						BgL_auxz00_2602 = BGL_BINT32_TO_INT32(BgL_tmpz00_2603);
					}
					BgL_tmpz00_2601 = BGl_bitzd2nots32zd2zz__bitz00(BgL_auxz00_2602);
				}
				return BGL_INT32_TO_BINT32(BgL_tmpz00_2601);
			}
		}

	}



/* bit-notu32 */
	BGL_EXPORTED_DEF uint32_t BGl_bitzd2notu32zd2zz__bitz00(uint32_t BgL_xz00_85)
	{
		{	/* Llib/bit.scm 601 */
			return ~(BgL_xz00_85);
		}

	}



/* &bit-notu32 */
	obj_t BGl_z62bitzd2notu32zb0zz__bitz00(obj_t BgL_envz00_1330,
		obj_t BgL_xz00_1331)
	{
		{	/* Llib/bit.scm 601 */
			{	/* Llib/bit.scm 601 */
				uint32_t BgL_tmpz00_2614;

				{	/* Llib/bit.scm 601 */
					uint32_t BgL_auxz00_2615;

					{	/* Llib/bit.scm 601 */
						obj_t BgL_tmpz00_2616;

						if (BGL_UINT32P(BgL_xz00_1331))
							{	/* Llib/bit.scm 601 */
								BgL_tmpz00_2616 = BgL_xz00_1331;
							}
						else
							{
								obj_t BgL_auxz00_2619;

								BgL_auxz00_2619 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(28050L), BGl_string1745z00zz__bitz00,
									BGl_string1705z00zz__bitz00, BgL_xz00_1331);
								FAILURE(BgL_auxz00_2619, BFALSE, BFALSE);
							}
						BgL_auxz00_2615 = BGL_BUINT32_TO_UINT32(BgL_tmpz00_2616);
					}
					BgL_tmpz00_2614 = BGl_bitzd2notu32zd2zz__bitz00(BgL_auxz00_2615);
				}
				return BGL_UINT32_TO_BUINT32(BgL_tmpz00_2614);
			}
		}

	}



/* bit-nots64 */
	BGL_EXPORTED_DEF int64_t BGl_bitzd2nots64zd2zz__bitz00(int64_t BgL_xz00_86)
	{
		{	/* Llib/bit.scm 602 */
			return ~(BgL_xz00_86);
		}

	}



/* &bit-nots64 */
	obj_t BGl_z62bitzd2nots64zb0zz__bitz00(obj_t BgL_envz00_1332,
		obj_t BgL_xz00_1333)
	{
		{	/* Llib/bit.scm 602 */
			{	/* Llib/bit.scm 602 */
				int64_t BgL_tmpz00_2627;

				{	/* Llib/bit.scm 602 */
					int64_t BgL_auxz00_2628;

					{	/* Llib/bit.scm 602 */
						obj_t BgL_tmpz00_2629;

						if (BGL_INT64P(BgL_xz00_1333))
							{	/* Llib/bit.scm 602 */
								BgL_tmpz00_2629 = BgL_xz00_1333;
							}
						else
							{
								obj_t BgL_auxz00_2632;

								BgL_auxz00_2632 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(28096L), BGl_string1746z00zz__bitz00,
									BGl_string1707z00zz__bitz00, BgL_xz00_1333);
								FAILURE(BgL_auxz00_2632, BFALSE, BFALSE);
							}
						BgL_auxz00_2628 = BGL_BINT64_TO_INT64(BgL_tmpz00_2629);
					}
					BgL_tmpz00_2627 = BGl_bitzd2nots64zd2zz__bitz00(BgL_auxz00_2628);
				}
				return BGL_INT64_TO_BINT64(BgL_tmpz00_2627);
			}
		}

	}



/* bit-notu64 */
	BGL_EXPORTED_DEF uint64_t BGl_bitzd2notu64zd2zz__bitz00(uint64_t BgL_xz00_87)
	{
		{	/* Llib/bit.scm 603 */
			return ~(BgL_xz00_87);
		}

	}



/* &bit-notu64 */
	obj_t BGl_z62bitzd2notu64zb0zz__bitz00(obj_t BgL_envz00_1334,
		obj_t BgL_xz00_1335)
	{
		{	/* Llib/bit.scm 603 */
			{	/* Llib/bit.scm 603 */
				uint64_t BgL_tmpz00_2640;

				{	/* Llib/bit.scm 603 */
					uint64_t BgL_auxz00_2641;

					{	/* Llib/bit.scm 603 */
						obj_t BgL_tmpz00_2642;

						if (BGL_UINT64P(BgL_xz00_1335))
							{	/* Llib/bit.scm 603 */
								BgL_tmpz00_2642 = BgL_xz00_1335;
							}
						else
							{
								obj_t BgL_auxz00_2645;

								BgL_auxz00_2645 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(28142L), BGl_string1747z00zz__bitz00,
									BGl_string1709z00zz__bitz00, BgL_xz00_1335);
								FAILURE(BgL_auxz00_2645, BFALSE, BFALSE);
							}
						BgL_auxz00_2641 = BGL_BINT64_TO_INT64(BgL_tmpz00_2642);
					}
					BgL_tmpz00_2640 = BGl_bitzd2notu64zd2zz__bitz00(BgL_auxz00_2641);
				}
				return BGL_UINT64_TO_BUINT64(BgL_tmpz00_2640);
			}
		}

	}



/* bit-notbx */
	BGL_EXPORTED_DEF obj_t BGl_bitzd2notbxzd2zz__bitz00(obj_t BgL_xz00_88)
	{
		{	/* Llib/bit.scm 604 */
			BGL_TAIL return bgl_bignum_not(BgL_xz00_88);
		}

	}



/* &bit-notbx */
	obj_t BGl_z62bitzd2notbxzb0zz__bitz00(obj_t BgL_envz00_1336,
		obj_t BgL_xz00_1337)
	{
		{	/* Llib/bit.scm 604 */
			{	/* Llib/bit.scm 604 */
				obj_t BgL_auxz00_2653;

				if (BIGNUMP(BgL_xz00_1337))
					{	/* Llib/bit.scm 604 */
						BgL_auxz00_2653 = BgL_xz00_1337;
					}
				else
					{
						obj_t BgL_auxz00_2656;

						BgL_auxz00_2656 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
							BINT(28187L), BGl_string1748z00zz__bitz00,
							BGl_string1711z00zz__bitz00, BgL_xz00_1337);
						FAILURE(BgL_auxz00_2656, BFALSE, BFALSE);
					}
				return BGl_bitzd2notbxzd2zz__bitz00(BgL_auxz00_2653);
			}
		}

	}



/* bit-rsh */
	BGL_EXPORTED_DEF long BGl_bitzd2rshzd2zz__bitz00(long BgL_xz00_89,
		long BgL_yz00_90)
	{
		{	/* Llib/bit.scm 609 */
			return (BgL_xz00_89 >> (int) (BgL_yz00_90));
		}

	}



/* &bit-rsh */
	obj_t BGl_z62bitzd2rshzb0zz__bitz00(obj_t BgL_envz00_1338,
		obj_t BgL_xz00_1339, obj_t BgL_yz00_1340)
	{
		{	/* Llib/bit.scm 609 */
			{	/* Llib/bit.scm 609 */
				long BgL_tmpz00_2663;

				{	/* Llib/bit.scm 609 */
					long BgL_auxz00_2673;
					long BgL_auxz00_2664;

					{	/* Llib/bit.scm 609 */
						obj_t BgL_tmpz00_2674;

						if (INTEGERP(BgL_yz00_1340))
							{	/* Llib/bit.scm 609 */
								BgL_tmpz00_2674 = BgL_yz00_1340;
							}
						else
							{
								obj_t BgL_auxz00_2677;

								BgL_auxz00_2677 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(28457L), BGl_string1749z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_yz00_1340);
								FAILURE(BgL_auxz00_2677, BFALSE, BFALSE);
							}
						BgL_auxz00_2673 = (long) CINT(BgL_tmpz00_2674);
					}
					{	/* Llib/bit.scm 609 */
						obj_t BgL_tmpz00_2665;

						if (INTEGERP(BgL_xz00_1339))
							{	/* Llib/bit.scm 609 */
								BgL_tmpz00_2665 = BgL_xz00_1339;
							}
						else
							{
								obj_t BgL_auxz00_2668;

								BgL_auxz00_2668 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(28457L), BGl_string1749z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_xz00_1339);
								FAILURE(BgL_auxz00_2668, BFALSE, BFALSE);
							}
						BgL_auxz00_2664 = (long) CINT(BgL_tmpz00_2665);
					}
					BgL_tmpz00_2663 =
						BGl_bitzd2rshzd2zz__bitz00(BgL_auxz00_2664, BgL_auxz00_2673);
				}
				return BINT(BgL_tmpz00_2663);
			}
		}

	}



/* bit-rshelong */
	BGL_EXPORTED_DEF long BGl_bitzd2rshelongzd2zz__bitz00(long BgL_xz00_91,
		long BgL_yz00_92)
	{
		{	/* Llib/bit.scm 610 */
			return (BgL_xz00_91 >> (int) (BgL_yz00_92));
		}

	}



/* &bit-rshelong */
	obj_t BGl_z62bitzd2rshelongzb0zz__bitz00(obj_t BgL_envz00_1341,
		obj_t BgL_xz00_1342, obj_t BgL_yz00_1343)
	{
		{	/* Llib/bit.scm 610 */
			{	/* Llib/bit.scm 610 */
				long BgL_tmpz00_2686;

				{	/* Llib/bit.scm 610 */
					long BgL_auxz00_2696;
					long BgL_auxz00_2687;

					{	/* Llib/bit.scm 610 */
						obj_t BgL_tmpz00_2697;

						if (INTEGERP(BgL_yz00_1343))
							{	/* Llib/bit.scm 610 */
								BgL_tmpz00_2697 = BgL_yz00_1343;
							}
						else
							{
								obj_t BgL_auxz00_2700;

								BgL_auxz00_2700 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(28506L), BGl_string1750z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_yz00_1343);
								FAILURE(BgL_auxz00_2700, BFALSE, BFALSE);
							}
						BgL_auxz00_2696 = (long) CINT(BgL_tmpz00_2697);
					}
					{	/* Llib/bit.scm 610 */
						obj_t BgL_tmpz00_2688;

						if (ELONGP(BgL_xz00_1342))
							{	/* Llib/bit.scm 610 */
								BgL_tmpz00_2688 = BgL_xz00_1342;
							}
						else
							{
								obj_t BgL_auxz00_2691;

								BgL_auxz00_2691 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(28506L), BGl_string1750z00zz__bitz00,
									BGl_string1691z00zz__bitz00, BgL_xz00_1342);
								FAILURE(BgL_auxz00_2691, BFALSE, BFALSE);
							}
						BgL_auxz00_2687 = BELONG_TO_LONG(BgL_tmpz00_2688);
					}
					BgL_tmpz00_2686 =
						BGl_bitzd2rshelongzd2zz__bitz00(BgL_auxz00_2687, BgL_auxz00_2696);
				}
				return make_belong(BgL_tmpz00_2686);
			}
		}

	}



/* bit-rshllong */
	BGL_EXPORTED_DEF BGL_LONGLONG_T BGl_bitzd2rshllongzd2zz__bitz00(BGL_LONGLONG_T
		BgL_xz00_93, long BgL_yz00_94)
	{
		{	/* Llib/bit.scm 611 */
			return (BgL_xz00_93 >> (int) (BgL_yz00_94));
		}

	}



/* &bit-rshllong */
	obj_t BGl_z62bitzd2rshllongzb0zz__bitz00(obj_t BgL_envz00_1344,
		obj_t BgL_xz00_1345, obj_t BgL_yz00_1346)
	{
		{	/* Llib/bit.scm 611 */
			{	/* Llib/bit.scm 611 */
				BGL_LONGLONG_T BgL_tmpz00_2709;

				{	/* Llib/bit.scm 611 */
					long BgL_auxz00_2719;
					BGL_LONGLONG_T BgL_auxz00_2710;

					{	/* Llib/bit.scm 611 */
						obj_t BgL_tmpz00_2720;

						if (INTEGERP(BgL_yz00_1346))
							{	/* Llib/bit.scm 611 */
								BgL_tmpz00_2720 = BgL_yz00_1346;
							}
						else
							{
								obj_t BgL_auxz00_2723;

								BgL_auxz00_2723 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(28560L), BGl_string1751z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_yz00_1346);
								FAILURE(BgL_auxz00_2723, BFALSE, BFALSE);
							}
						BgL_auxz00_2719 = (long) CINT(BgL_tmpz00_2720);
					}
					{	/* Llib/bit.scm 611 */
						obj_t BgL_tmpz00_2711;

						if (LLONGP(BgL_xz00_1345))
							{	/* Llib/bit.scm 611 */
								BgL_tmpz00_2711 = BgL_xz00_1345;
							}
						else
							{
								obj_t BgL_auxz00_2714;

								BgL_auxz00_2714 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(28560L), BGl_string1751z00zz__bitz00,
									BGl_string1693z00zz__bitz00, BgL_xz00_1345);
								FAILURE(BgL_auxz00_2714, BFALSE, BFALSE);
							}
						BgL_auxz00_2710 = BLLONG_TO_LLONG(BgL_tmpz00_2711);
					}
					BgL_tmpz00_2709 =
						BGl_bitzd2rshllongzd2zz__bitz00(BgL_auxz00_2710, BgL_auxz00_2719);
				}
				return make_bllong(BgL_tmpz00_2709);
			}
		}

	}



/* bit-rshs8 */
	BGL_EXPORTED_DEF int8_t BGl_bitzd2rshs8zd2zz__bitz00(int8_t BgL_xz00_95,
		long BgL_yz00_96)
	{
		{	/* Llib/bit.scm 612 */
			return (BgL_xz00_95 >> (int) (BgL_yz00_96));
		}

	}



/* &bit-rshs8 */
	obj_t BGl_z62bitzd2rshs8zb0zz__bitz00(obj_t BgL_envz00_1347,
		obj_t BgL_xz00_1348, obj_t BgL_yz00_1349)
	{
		{	/* Llib/bit.scm 612 */
			{	/* Llib/bit.scm 612 */
				int8_t BgL_tmpz00_2732;

				{	/* Llib/bit.scm 612 */
					long BgL_auxz00_2742;
					int8_t BgL_auxz00_2733;

					{	/* Llib/bit.scm 612 */
						obj_t BgL_tmpz00_2743;

						if (INTEGERP(BgL_yz00_1349))
							{	/* Llib/bit.scm 612 */
								BgL_tmpz00_2743 = BgL_yz00_1349;
							}
						else
							{
								obj_t BgL_auxz00_2746;

								BgL_auxz00_2746 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(28611L), BGl_string1752z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_yz00_1349);
								FAILURE(BgL_auxz00_2746, BFALSE, BFALSE);
							}
						BgL_auxz00_2742 = (long) CINT(BgL_tmpz00_2743);
					}
					{	/* Llib/bit.scm 612 */
						obj_t BgL_tmpz00_2734;

						if (BGL_INT8P(BgL_xz00_1348))
							{	/* Llib/bit.scm 612 */
								BgL_tmpz00_2734 = BgL_xz00_1348;
							}
						else
							{
								obj_t BgL_auxz00_2737;

								BgL_auxz00_2737 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(28611L), BGl_string1752z00zz__bitz00,
									BGl_string1695z00zz__bitz00, BgL_xz00_1348);
								FAILURE(BgL_auxz00_2737, BFALSE, BFALSE);
							}
						BgL_auxz00_2733 = BGL_BINT8_TO_INT8(BgL_tmpz00_2734);
					}
					BgL_tmpz00_2732 =
						BGl_bitzd2rshs8zd2zz__bitz00(BgL_auxz00_2733, BgL_auxz00_2742);
				}
				return BGL_INT8_TO_BINT8(BgL_tmpz00_2732);
			}
		}

	}



/* bit-rshu8 */
	BGL_EXPORTED_DEF uint8_t BGl_bitzd2rshu8zd2zz__bitz00(uint8_t BgL_xz00_97,
		long BgL_yz00_98)
	{
		{	/* Llib/bit.scm 613 */
			return (BgL_xz00_97 >> (int) (BgL_yz00_98));
		}

	}



/* &bit-rshu8 */
	obj_t BGl_z62bitzd2rshu8zb0zz__bitz00(obj_t BgL_envz00_1350,
		obj_t BgL_xz00_1351, obj_t BgL_yz00_1352)
	{
		{	/* Llib/bit.scm 613 */
			{	/* Llib/bit.scm 613 */
				uint8_t BgL_tmpz00_2755;

				{	/* Llib/bit.scm 613 */
					long BgL_auxz00_2765;
					uint8_t BgL_auxz00_2756;

					{	/* Llib/bit.scm 613 */
						obj_t BgL_tmpz00_2766;

						if (INTEGERP(BgL_yz00_1352))
							{	/* Llib/bit.scm 613 */
								BgL_tmpz00_2766 = BgL_yz00_1352;
							}
						else
							{
								obj_t BgL_auxz00_2769;

								BgL_auxz00_2769 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(28659L), BGl_string1753z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_yz00_1352);
								FAILURE(BgL_auxz00_2769, BFALSE, BFALSE);
							}
						BgL_auxz00_2765 = (long) CINT(BgL_tmpz00_2766);
					}
					{	/* Llib/bit.scm 613 */
						obj_t BgL_tmpz00_2757;

						if (BGL_UINT8P(BgL_xz00_1351))
							{	/* Llib/bit.scm 613 */
								BgL_tmpz00_2757 = BgL_xz00_1351;
							}
						else
							{
								obj_t BgL_auxz00_2760;

								BgL_auxz00_2760 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(28659L), BGl_string1753z00zz__bitz00,
									BGl_string1697z00zz__bitz00, BgL_xz00_1351);
								FAILURE(BgL_auxz00_2760, BFALSE, BFALSE);
							}
						BgL_auxz00_2756 = BGL_BUINT8_TO_UINT8(BgL_tmpz00_2757);
					}
					BgL_tmpz00_2755 =
						BGl_bitzd2rshu8zd2zz__bitz00(BgL_auxz00_2756, BgL_auxz00_2765);
				}
				return BGL_UINT8_TO_BUINT8(BgL_tmpz00_2755);
			}
		}

	}



/* bit-rshs16 */
	BGL_EXPORTED_DEF int16_t BGl_bitzd2rshs16zd2zz__bitz00(int16_t BgL_xz00_99,
		long BgL_yz00_100)
	{
		{	/* Llib/bit.scm 614 */
			return (BgL_xz00_99 >> (int) (BgL_yz00_100));
		}

	}



/* &bit-rshs16 */
	obj_t BGl_z62bitzd2rshs16zb0zz__bitz00(obj_t BgL_envz00_1353,
		obj_t BgL_xz00_1354, obj_t BgL_yz00_1355)
	{
		{	/* Llib/bit.scm 614 */
			{	/* Llib/bit.scm 614 */
				int16_t BgL_tmpz00_2778;

				{	/* Llib/bit.scm 614 */
					long BgL_auxz00_2788;
					int16_t BgL_auxz00_2779;

					{	/* Llib/bit.scm 614 */
						obj_t BgL_tmpz00_2789;

						if (INTEGERP(BgL_yz00_1355))
							{	/* Llib/bit.scm 614 */
								BgL_tmpz00_2789 = BgL_yz00_1355;
							}
						else
							{
								obj_t BgL_auxz00_2792;

								BgL_auxz00_2792 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(28708L), BGl_string1754z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_yz00_1355);
								FAILURE(BgL_auxz00_2792, BFALSE, BFALSE);
							}
						BgL_auxz00_2788 = (long) CINT(BgL_tmpz00_2789);
					}
					{	/* Llib/bit.scm 614 */
						obj_t BgL_tmpz00_2780;

						if (BGL_INT16P(BgL_xz00_1354))
							{	/* Llib/bit.scm 614 */
								BgL_tmpz00_2780 = BgL_xz00_1354;
							}
						else
							{
								obj_t BgL_auxz00_2783;

								BgL_auxz00_2783 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(28708L), BGl_string1754z00zz__bitz00,
									BGl_string1699z00zz__bitz00, BgL_xz00_1354);
								FAILURE(BgL_auxz00_2783, BFALSE, BFALSE);
							}
						BgL_auxz00_2779 = BGL_BINT16_TO_INT16(BgL_tmpz00_2780);
					}
					BgL_tmpz00_2778 =
						BGl_bitzd2rshs16zd2zz__bitz00(BgL_auxz00_2779, BgL_auxz00_2788);
				}
				return BGL_INT16_TO_BINT16(BgL_tmpz00_2778);
			}
		}

	}



/* bit-rshu16 */
	BGL_EXPORTED_DEF int16_t BGl_bitzd2rshu16zd2zz__bitz00(int16_t BgL_xz00_101,
		long BgL_yz00_102)
	{
		{	/* Llib/bit.scm 615 */
			{	/* Llib/bit.scm 615 */
				uint16_t BgL_tmpz00_2799;

				BgL_tmpz00_2799 = ((uint16_t) (BgL_xz00_101) >> (int) (BgL_yz00_102));
				return (int16_t) (BgL_tmpz00_2799);
			}
		}

	}



/* &bit-rshu16 */
	obj_t BGl_z62bitzd2rshu16zb0zz__bitz00(obj_t BgL_envz00_1356,
		obj_t BgL_xz00_1357, obj_t BgL_yz00_1358)
	{
		{	/* Llib/bit.scm 615 */
			{	/* Llib/bit.scm 615 */
				int16_t BgL_tmpz00_2804;

				{	/* Llib/bit.scm 615 */
					long BgL_auxz00_2814;
					int16_t BgL_auxz00_2805;

					{	/* Llib/bit.scm 615 */
						obj_t BgL_tmpz00_2815;

						if (INTEGERP(BgL_yz00_1358))
							{	/* Llib/bit.scm 615 */
								BgL_tmpz00_2815 = BgL_yz00_1358;
							}
						else
							{
								obj_t BgL_auxz00_2818;

								BgL_auxz00_2818 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(28758L), BGl_string1755z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_yz00_1358);
								FAILURE(BgL_auxz00_2818, BFALSE, BFALSE);
							}
						BgL_auxz00_2814 = (long) CINT(BgL_tmpz00_2815);
					}
					{	/* Llib/bit.scm 615 */
						obj_t BgL_tmpz00_2806;

						if (BGL_INT16P(BgL_xz00_1357))
							{	/* Llib/bit.scm 615 */
								BgL_tmpz00_2806 = BgL_xz00_1357;
							}
						else
							{
								obj_t BgL_auxz00_2809;

								BgL_auxz00_2809 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(28758L), BGl_string1755z00zz__bitz00,
									BGl_string1699z00zz__bitz00, BgL_xz00_1357);
								FAILURE(BgL_auxz00_2809, BFALSE, BFALSE);
							}
						BgL_auxz00_2805 = BGL_BINT16_TO_INT16(BgL_tmpz00_2806);
					}
					BgL_tmpz00_2804 =
						BGl_bitzd2rshu16zd2zz__bitz00(BgL_auxz00_2805, BgL_auxz00_2814);
				}
				return BGL_INT16_TO_BINT16(BgL_tmpz00_2804);
			}
		}

	}



/* bit-rshs32 */
	BGL_EXPORTED_DEF int32_t BGl_bitzd2rshs32zd2zz__bitz00(int32_t BgL_xz00_103,
		long BgL_yz00_104)
	{
		{	/* Llib/bit.scm 616 */
			return (BgL_xz00_103 >> (int) (BgL_yz00_104));
		}

	}



/* &bit-rshs32 */
	obj_t BGl_z62bitzd2rshs32zb0zz__bitz00(obj_t BgL_envz00_1359,
		obj_t BgL_xz00_1360, obj_t BgL_yz00_1361)
	{
		{	/* Llib/bit.scm 616 */
			{	/* Llib/bit.scm 616 */
				int32_t BgL_tmpz00_2827;

				{	/* Llib/bit.scm 616 */
					long BgL_auxz00_2837;
					int32_t BgL_auxz00_2828;

					{	/* Llib/bit.scm 616 */
						obj_t BgL_tmpz00_2838;

						if (INTEGERP(BgL_yz00_1361))
							{	/* Llib/bit.scm 616 */
								BgL_tmpz00_2838 = BgL_yz00_1361;
							}
						else
							{
								obj_t BgL_auxz00_2841;

								BgL_auxz00_2841 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(28808L), BGl_string1756z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_yz00_1361);
								FAILURE(BgL_auxz00_2841, BFALSE, BFALSE);
							}
						BgL_auxz00_2837 = (long) CINT(BgL_tmpz00_2838);
					}
					{	/* Llib/bit.scm 616 */
						obj_t BgL_tmpz00_2829;

						if (BGL_INT32P(BgL_xz00_1360))
							{	/* Llib/bit.scm 616 */
								BgL_tmpz00_2829 = BgL_xz00_1360;
							}
						else
							{
								obj_t BgL_auxz00_2832;

								BgL_auxz00_2832 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(28808L), BGl_string1756z00zz__bitz00,
									BGl_string1703z00zz__bitz00, BgL_xz00_1360);
								FAILURE(BgL_auxz00_2832, BFALSE, BFALSE);
							}
						BgL_auxz00_2828 = BGL_BINT32_TO_INT32(BgL_tmpz00_2829);
					}
					BgL_tmpz00_2827 =
						BGl_bitzd2rshs32zd2zz__bitz00(BgL_auxz00_2828, BgL_auxz00_2837);
				}
				return BGL_INT32_TO_BINT32(BgL_tmpz00_2827);
			}
		}

	}



/* bit-rshu32 */
	BGL_EXPORTED_DEF uint32_t BGl_bitzd2rshu32zd2zz__bitz00(uint32_t BgL_xz00_105,
		long BgL_yz00_106)
	{
		{	/* Llib/bit.scm 617 */
			return (BgL_xz00_105 >> (int) (BgL_yz00_106));
		}

	}



/* &bit-rshu32 */
	obj_t BGl_z62bitzd2rshu32zb0zz__bitz00(obj_t BgL_envz00_1362,
		obj_t BgL_xz00_1363, obj_t BgL_yz00_1364)
	{
		{	/* Llib/bit.scm 617 */
			{	/* Llib/bit.scm 617 */
				uint32_t BgL_tmpz00_2850;

				{	/* Llib/bit.scm 617 */
					long BgL_auxz00_2860;
					uint32_t BgL_auxz00_2851;

					{	/* Llib/bit.scm 617 */
						obj_t BgL_tmpz00_2861;

						if (INTEGERP(BgL_yz00_1364))
							{	/* Llib/bit.scm 617 */
								BgL_tmpz00_2861 = BgL_yz00_1364;
							}
						else
							{
								obj_t BgL_auxz00_2864;

								BgL_auxz00_2864 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(28858L), BGl_string1757z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_yz00_1364);
								FAILURE(BgL_auxz00_2864, BFALSE, BFALSE);
							}
						BgL_auxz00_2860 = (long) CINT(BgL_tmpz00_2861);
					}
					{	/* Llib/bit.scm 617 */
						obj_t BgL_tmpz00_2852;

						if (BGL_UINT32P(BgL_xz00_1363))
							{	/* Llib/bit.scm 617 */
								BgL_tmpz00_2852 = BgL_xz00_1363;
							}
						else
							{
								obj_t BgL_auxz00_2855;

								BgL_auxz00_2855 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(28858L), BGl_string1757z00zz__bitz00,
									BGl_string1705z00zz__bitz00, BgL_xz00_1363);
								FAILURE(BgL_auxz00_2855, BFALSE, BFALSE);
							}
						BgL_auxz00_2851 = BGL_BUINT32_TO_UINT32(BgL_tmpz00_2852);
					}
					BgL_tmpz00_2850 =
						BGl_bitzd2rshu32zd2zz__bitz00(BgL_auxz00_2851, BgL_auxz00_2860);
				}
				return BGL_UINT32_TO_BUINT32(BgL_tmpz00_2850);
			}
		}

	}



/* bit-rshs64 */
	BGL_EXPORTED_DEF int64_t BGl_bitzd2rshs64zd2zz__bitz00(int64_t BgL_xz00_107,
		long BgL_yz00_108)
	{
		{	/* Llib/bit.scm 618 */
			return (BgL_xz00_107 >> (int) (BgL_yz00_108));
		}

	}



/* &bit-rshs64 */
	obj_t BGl_z62bitzd2rshs64zb0zz__bitz00(obj_t BgL_envz00_1365,
		obj_t BgL_xz00_1366, obj_t BgL_yz00_1367)
	{
		{	/* Llib/bit.scm 618 */
			{	/* Llib/bit.scm 618 */
				int64_t BgL_tmpz00_2873;

				{	/* Llib/bit.scm 618 */
					long BgL_auxz00_2883;
					int64_t BgL_auxz00_2874;

					{	/* Llib/bit.scm 618 */
						obj_t BgL_tmpz00_2884;

						if (INTEGERP(BgL_yz00_1367))
							{	/* Llib/bit.scm 618 */
								BgL_tmpz00_2884 = BgL_yz00_1367;
							}
						else
							{
								obj_t BgL_auxz00_2887;

								BgL_auxz00_2887 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(28908L), BGl_string1758z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_yz00_1367);
								FAILURE(BgL_auxz00_2887, BFALSE, BFALSE);
							}
						BgL_auxz00_2883 = (long) CINT(BgL_tmpz00_2884);
					}
					{	/* Llib/bit.scm 618 */
						obj_t BgL_tmpz00_2875;

						if (BGL_INT64P(BgL_xz00_1366))
							{	/* Llib/bit.scm 618 */
								BgL_tmpz00_2875 = BgL_xz00_1366;
							}
						else
							{
								obj_t BgL_auxz00_2878;

								BgL_auxz00_2878 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(28908L), BGl_string1758z00zz__bitz00,
									BGl_string1707z00zz__bitz00, BgL_xz00_1366);
								FAILURE(BgL_auxz00_2878, BFALSE, BFALSE);
							}
						BgL_auxz00_2874 = BGL_BINT64_TO_INT64(BgL_tmpz00_2875);
					}
					BgL_tmpz00_2873 =
						BGl_bitzd2rshs64zd2zz__bitz00(BgL_auxz00_2874, BgL_auxz00_2883);
				}
				return BGL_INT64_TO_BINT64(BgL_tmpz00_2873);
			}
		}

	}



/* bit-rshu64 */
	BGL_EXPORTED_DEF uint64_t BGl_bitzd2rshu64zd2zz__bitz00(uint64_t BgL_xz00_109,
		long BgL_yz00_110)
	{
		{	/* Llib/bit.scm 619 */
			return (BgL_xz00_109 >> (int) (BgL_yz00_110));
		}

	}



/* &bit-rshu64 */
	obj_t BGl_z62bitzd2rshu64zb0zz__bitz00(obj_t BgL_envz00_1368,
		obj_t BgL_xz00_1369, obj_t BgL_yz00_1370)
	{
		{	/* Llib/bit.scm 619 */
			{	/* Llib/bit.scm 619 */
				uint64_t BgL_tmpz00_2896;

				{	/* Llib/bit.scm 619 */
					long BgL_auxz00_2906;
					uint64_t BgL_auxz00_2897;

					{	/* Llib/bit.scm 619 */
						obj_t BgL_tmpz00_2907;

						if (INTEGERP(BgL_yz00_1370))
							{	/* Llib/bit.scm 619 */
								BgL_tmpz00_2907 = BgL_yz00_1370;
							}
						else
							{
								obj_t BgL_auxz00_2910;

								BgL_auxz00_2910 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(28958L), BGl_string1759z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_yz00_1370);
								FAILURE(BgL_auxz00_2910, BFALSE, BFALSE);
							}
						BgL_auxz00_2906 = (long) CINT(BgL_tmpz00_2907);
					}
					{	/* Llib/bit.scm 619 */
						obj_t BgL_tmpz00_2898;

						if (BGL_UINT64P(BgL_xz00_1369))
							{	/* Llib/bit.scm 619 */
								BgL_tmpz00_2898 = BgL_xz00_1369;
							}
						else
							{
								obj_t BgL_auxz00_2901;

								BgL_auxz00_2901 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(28958L), BGl_string1759z00zz__bitz00,
									BGl_string1709z00zz__bitz00, BgL_xz00_1369);
								FAILURE(BgL_auxz00_2901, BFALSE, BFALSE);
							}
						BgL_auxz00_2897 = BGL_BINT64_TO_INT64(BgL_tmpz00_2898);
					}
					BgL_tmpz00_2896 =
						BGl_bitzd2rshu64zd2zz__bitz00(BgL_auxz00_2897, BgL_auxz00_2906);
				}
				return BGL_UINT64_TO_BUINT64(BgL_tmpz00_2896);
			}
		}

	}



/* bit-rshbx */
	BGL_EXPORTED_DEF obj_t BGl_bitzd2rshbxzd2zz__bitz00(obj_t BgL_xz00_111,
		long BgL_yz00_112)
	{
		{	/* Llib/bit.scm 620 */
			BGL_TAIL return bgl_bignum_rsh(BgL_xz00_111, BgL_yz00_112);
		}

	}



/* &bit-rshbx */
	obj_t BGl_z62bitzd2rshbxzb0zz__bitz00(obj_t BgL_envz00_1371,
		obj_t BgL_xz00_1372, obj_t BgL_yz00_1373)
	{
		{	/* Llib/bit.scm 620 */
			{	/* Llib/bit.scm 620 */
				long BgL_auxz00_2925;
				obj_t BgL_auxz00_2918;

				{	/* Llib/bit.scm 620 */
					obj_t BgL_tmpz00_2926;

					if (INTEGERP(BgL_yz00_1373))
						{	/* Llib/bit.scm 620 */
							BgL_tmpz00_2926 = BgL_yz00_1373;
						}
					else
						{
							obj_t BgL_auxz00_2929;

							BgL_auxz00_2929 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
								BINT(29007L), BGl_string1760z00zz__bitz00,
								BGl_string1689z00zz__bitz00, BgL_yz00_1373);
							FAILURE(BgL_auxz00_2929, BFALSE, BFALSE);
						}
					BgL_auxz00_2925 = (long) CINT(BgL_tmpz00_2926);
				}
				if (BIGNUMP(BgL_xz00_1372))
					{	/* Llib/bit.scm 620 */
						BgL_auxz00_2918 = BgL_xz00_1372;
					}
				else
					{
						obj_t BgL_auxz00_2921;

						BgL_auxz00_2921 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
							BINT(29007L), BGl_string1760z00zz__bitz00,
							BGl_string1711z00zz__bitz00, BgL_xz00_1372);
						FAILURE(BgL_auxz00_2921, BFALSE, BFALSE);
					}
				return BGl_bitzd2rshbxzd2zz__bitz00(BgL_auxz00_2918, BgL_auxz00_2925);
			}
		}

	}



/* bit-ursh */
	BGL_EXPORTED_DEF unsigned long BGl_bitzd2urshzd2zz__bitz00(unsigned long
		BgL_xz00_113, long BgL_yz00_114)
	{
		{	/* Llib/bit.scm 625 */
			return (BgL_xz00_113 >> (int) (BgL_yz00_114));
		}

	}



/* &bit-ursh */
	obj_t BGl_z62bitzd2urshzb0zz__bitz00(obj_t BgL_envz00_1374,
		obj_t BgL_xz00_1375, obj_t BgL_yz00_1376)
	{
		{	/* Llib/bit.scm 625 */
			{	/* Llib/bit.scm 625 */
				unsigned long BgL_tmpz00_2937;

				{	/* Llib/bit.scm 625 */
					long BgL_auxz00_2947;
					unsigned long BgL_auxz00_2938;

					{	/* Llib/bit.scm 625 */
						obj_t BgL_tmpz00_2948;

						if (INTEGERP(BgL_yz00_1376))
							{	/* Llib/bit.scm 625 */
								BgL_tmpz00_2948 = BgL_yz00_1376;
							}
						else
							{
								obj_t BgL_auxz00_2951;

								BgL_auxz00_2951 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(29277L), BGl_string1761z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_yz00_1376);
								FAILURE(BgL_auxz00_2951, BFALSE, BFALSE);
							}
						BgL_auxz00_2947 = (long) CINT(BgL_tmpz00_2948);
					}
					{	/* Llib/bit.scm 625 */
						obj_t BgL_tmpz00_2939;

						if (INTEGERP(BgL_xz00_1375))
							{	/* Llib/bit.scm 625 */
								BgL_tmpz00_2939 = BgL_xz00_1375;
							}
						else
							{
								obj_t BgL_auxz00_2942;

								BgL_auxz00_2942 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(29277L), BGl_string1761z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_xz00_1375);
								FAILURE(BgL_auxz00_2942, BFALSE, BFALSE);
							}
						BgL_auxz00_2938 = (unsigned long) CINT(BgL_tmpz00_2939);
					}
					BgL_tmpz00_2937 =
						BGl_bitzd2urshzd2zz__bitz00(BgL_auxz00_2938, BgL_auxz00_2947);
				}
				return BINT(BgL_tmpz00_2937);
			}
		}

	}



/* bit-urshelong */
	BGL_EXPORTED_DEF unsigned long BGl_bitzd2urshelongzd2zz__bitz00(unsigned long
		BgL_xz00_115, long BgL_yz00_116)
	{
		{	/* Llib/bit.scm 626 */
			return (BgL_xz00_115 >> (int) (BgL_yz00_116));
		}

	}



/* &bit-urshelong */
	obj_t BGl_z62bitzd2urshelongzb0zz__bitz00(obj_t BgL_envz00_1377,
		obj_t BgL_xz00_1378, obj_t BgL_yz00_1379)
	{
		{	/* Llib/bit.scm 626 */
			{	/* Llib/bit.scm 626 */
				long BgL_tmpz00_2960;

				{	/* Llib/bit.scm 626 */
					long BgL_auxz00_2970;
					unsigned long BgL_auxz00_2961;

					{	/* Llib/bit.scm 626 */
						obj_t BgL_tmpz00_2971;

						if (INTEGERP(BgL_yz00_1379))
							{	/* Llib/bit.scm 626 */
								BgL_tmpz00_2971 = BgL_yz00_1379;
							}
						else
							{
								obj_t BgL_auxz00_2974;

								BgL_auxz00_2974 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(29328L), BGl_string1762z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_yz00_1379);
								FAILURE(BgL_auxz00_2974, BFALSE, BFALSE);
							}
						BgL_auxz00_2970 = (long) CINT(BgL_tmpz00_2971);
					}
					{	/* Llib/bit.scm 626 */
						obj_t BgL_tmpz00_2962;

						if (ELONGP(BgL_xz00_1378))
							{	/* Llib/bit.scm 626 */
								BgL_tmpz00_2962 = BgL_xz00_1378;
							}
						else
							{
								obj_t BgL_auxz00_2965;

								BgL_auxz00_2965 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(29328L), BGl_string1762z00zz__bitz00,
									BGl_string1691z00zz__bitz00, BgL_xz00_1378);
								FAILURE(BgL_auxz00_2965, BFALSE, BFALSE);
							}
						BgL_auxz00_2961 = BELONG_TO_LONG(BgL_tmpz00_2962);
					}
					BgL_tmpz00_2960 =
						BGl_bitzd2urshelongzd2zz__bitz00(BgL_auxz00_2961, BgL_auxz00_2970);
				}
				return make_belong(BgL_tmpz00_2960);
			}
		}

	}



/* bit-urshllong */
	BGL_EXPORTED_DEF unsigned BGL_LONGLONG_T
		BGl_bitzd2urshllongzd2zz__bitz00(unsigned BGL_LONGLONG_T BgL_xz00_117,
		long BgL_yz00_118)
	{
		{	/* Llib/bit.scm 627 */
			return (BgL_xz00_117 >> (int) (BgL_yz00_118));
		}

	}



/* &bit-urshllong */
	obj_t BGl_z62bitzd2urshllongzb0zz__bitz00(obj_t BgL_envz00_1380,
		obj_t BgL_xz00_1381, obj_t BgL_yz00_1382)
	{
		{	/* Llib/bit.scm 627 */
			{	/* Llib/bit.scm 627 */
				unsigned BGL_LONGLONG_T BgL_tmpz00_2983;

				{	/* Llib/bit.scm 627 */
					long BgL_auxz00_2993;
					unsigned BGL_LONGLONG_T BgL_auxz00_2984;

					{	/* Llib/bit.scm 627 */
						obj_t BgL_tmpz00_2994;

						if (INTEGERP(BgL_yz00_1382))
							{	/* Llib/bit.scm 627 */
								BgL_tmpz00_2994 = BgL_yz00_1382;
							}
						else
							{
								obj_t BgL_auxz00_2997;

								BgL_auxz00_2997 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(29384L), BGl_string1763z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_yz00_1382);
								FAILURE(BgL_auxz00_2997, BFALSE, BFALSE);
							}
						BgL_auxz00_2993 = (long) CINT(BgL_tmpz00_2994);
					}
					{	/* Llib/bit.scm 627 */
						obj_t BgL_tmpz00_2985;

						if (LLONGP(BgL_xz00_1381))
							{	/* Llib/bit.scm 627 */
								BgL_tmpz00_2985 = BgL_xz00_1381;
							}
						else
							{
								obj_t BgL_auxz00_2988;

								BgL_auxz00_2988 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(29384L), BGl_string1763z00zz__bitz00,
									BGl_string1693z00zz__bitz00, BgL_xz00_1381);
								FAILURE(BgL_auxz00_2988, BFALSE, BFALSE);
							}
						BgL_auxz00_2984 = BLLONG_TO_LLONG(BgL_tmpz00_2985);
					}
					BgL_tmpz00_2983 =
						BGl_bitzd2urshllongzd2zz__bitz00(BgL_auxz00_2984, BgL_auxz00_2993);
				}
				return make_bllong(BgL_tmpz00_2983);
			}
		}

	}



/* bit-urshu8 */
	BGL_EXPORTED_DEF uint8_t BGl_bitzd2urshu8zd2zz__bitz00(uint8_t BgL_xz00_119,
		long BgL_yz00_120)
	{
		{	/* Llib/bit.scm 628 */
			return (BgL_xz00_119 >> (int) (BgL_yz00_120));
		}

	}



/* &bit-urshu8 */
	obj_t BGl_z62bitzd2urshu8zb0zz__bitz00(obj_t BgL_envz00_1383,
		obj_t BgL_xz00_1384, obj_t BgL_yz00_1385)
	{
		{	/* Llib/bit.scm 628 */
			{	/* Llib/bit.scm 628 */
				uint8_t BgL_tmpz00_3006;

				{	/* Llib/bit.scm 628 */
					long BgL_auxz00_3016;
					uint8_t BgL_auxz00_3007;

					{	/* Llib/bit.scm 628 */
						obj_t BgL_tmpz00_3017;

						if (INTEGERP(BgL_yz00_1385))
							{	/* Llib/bit.scm 628 */
								BgL_tmpz00_3017 = BgL_yz00_1385;
							}
						else
							{
								obj_t BgL_auxz00_3020;

								BgL_auxz00_3020 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(29437L), BGl_string1764z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_yz00_1385);
								FAILURE(BgL_auxz00_3020, BFALSE, BFALSE);
							}
						BgL_auxz00_3016 = (long) CINT(BgL_tmpz00_3017);
					}
					{	/* Llib/bit.scm 628 */
						obj_t BgL_tmpz00_3008;

						if (BGL_UINT8P(BgL_xz00_1384))
							{	/* Llib/bit.scm 628 */
								BgL_tmpz00_3008 = BgL_xz00_1384;
							}
						else
							{
								obj_t BgL_auxz00_3011;

								BgL_auxz00_3011 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(29437L), BGl_string1764z00zz__bitz00,
									BGl_string1697z00zz__bitz00, BgL_xz00_1384);
								FAILURE(BgL_auxz00_3011, BFALSE, BFALSE);
							}
						BgL_auxz00_3007 = BGL_BUINT8_TO_UINT8(BgL_tmpz00_3008);
					}
					BgL_tmpz00_3006 =
						BGl_bitzd2urshu8zd2zz__bitz00(BgL_auxz00_3007, BgL_auxz00_3016);
				}
				return BGL_UINT8_TO_BUINT8(BgL_tmpz00_3006);
			}
		}

	}



/* bit-urshs8 */
	BGL_EXPORTED_DEF int8_t BGl_bitzd2urshs8zd2zz__bitz00(int8_t BgL_xz00_121,
		long BgL_yz00_122)
	{
		{	/* Llib/bit.scm 629 */
			return (BgL_xz00_121 >> (int) (BgL_yz00_122));
		}

	}



/* &bit-urshs8 */
	obj_t BGl_z62bitzd2urshs8zb0zz__bitz00(obj_t BgL_envz00_1386,
		obj_t BgL_xz00_1387, obj_t BgL_yz00_1388)
	{
		{	/* Llib/bit.scm 629 */
			{	/* Llib/bit.scm 629 */
				int8_t BgL_tmpz00_3029;

				{	/* Llib/bit.scm 629 */
					long BgL_auxz00_3039;
					int8_t BgL_auxz00_3030;

					{	/* Llib/bit.scm 629 */
						obj_t BgL_tmpz00_3040;

						if (INTEGERP(BgL_yz00_1388))
							{	/* Llib/bit.scm 629 */
								BgL_tmpz00_3040 = BgL_yz00_1388;
							}
						else
							{
								obj_t BgL_auxz00_3043;

								BgL_auxz00_3043 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(29487L), BGl_string1765z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_yz00_1388);
								FAILURE(BgL_auxz00_3043, BFALSE, BFALSE);
							}
						BgL_auxz00_3039 = (long) CINT(BgL_tmpz00_3040);
					}
					{	/* Llib/bit.scm 629 */
						obj_t BgL_tmpz00_3031;

						if (BGL_INT8P(BgL_xz00_1387))
							{	/* Llib/bit.scm 629 */
								BgL_tmpz00_3031 = BgL_xz00_1387;
							}
						else
							{
								obj_t BgL_auxz00_3034;

								BgL_auxz00_3034 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(29487L), BGl_string1765z00zz__bitz00,
									BGl_string1695z00zz__bitz00, BgL_xz00_1387);
								FAILURE(BgL_auxz00_3034, BFALSE, BFALSE);
							}
						BgL_auxz00_3030 = BGL_BINT8_TO_INT8(BgL_tmpz00_3031);
					}
					BgL_tmpz00_3029 =
						BGl_bitzd2urshs8zd2zz__bitz00(BgL_auxz00_3030, BgL_auxz00_3039);
				}
				return BGL_INT8_TO_BINT8(BgL_tmpz00_3029);
			}
		}

	}



/* bit-urshs16 */
	BGL_EXPORTED_DEF int16_t BGl_bitzd2urshs16zd2zz__bitz00(int16_t BgL_xz00_123,
		long BgL_yz00_124)
	{
		{	/* Llib/bit.scm 630 */
			return (BgL_xz00_123 >> (int) (BgL_yz00_124));
		}

	}



/* &bit-urshs16 */
	obj_t BGl_z62bitzd2urshs16zb0zz__bitz00(obj_t BgL_envz00_1389,
		obj_t BgL_xz00_1390, obj_t BgL_yz00_1391)
	{
		{	/* Llib/bit.scm 630 */
			{	/* Llib/bit.scm 630 */
				int16_t BgL_tmpz00_3052;

				{	/* Llib/bit.scm 630 */
					long BgL_auxz00_3062;
					int16_t BgL_auxz00_3053;

					{	/* Llib/bit.scm 630 */
						obj_t BgL_tmpz00_3063;

						if (INTEGERP(BgL_yz00_1391))
							{	/* Llib/bit.scm 630 */
								BgL_tmpz00_3063 = BgL_yz00_1391;
							}
						else
							{
								obj_t BgL_auxz00_3066;

								BgL_auxz00_3066 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(29538L), BGl_string1766z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_yz00_1391);
								FAILURE(BgL_auxz00_3066, BFALSE, BFALSE);
							}
						BgL_auxz00_3062 = (long) CINT(BgL_tmpz00_3063);
					}
					{	/* Llib/bit.scm 630 */
						obj_t BgL_tmpz00_3054;

						if (BGL_INT16P(BgL_xz00_1390))
							{	/* Llib/bit.scm 630 */
								BgL_tmpz00_3054 = BgL_xz00_1390;
							}
						else
							{
								obj_t BgL_auxz00_3057;

								BgL_auxz00_3057 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(29538L), BGl_string1766z00zz__bitz00,
									BGl_string1699z00zz__bitz00, BgL_xz00_1390);
								FAILURE(BgL_auxz00_3057, BFALSE, BFALSE);
							}
						BgL_auxz00_3053 = BGL_BINT16_TO_INT16(BgL_tmpz00_3054);
					}
					BgL_tmpz00_3052 =
						BGl_bitzd2urshs16zd2zz__bitz00(BgL_auxz00_3053, BgL_auxz00_3062);
				}
				return BGL_INT16_TO_BINT16(BgL_tmpz00_3052);
			}
		}

	}



/* bit-urshu16 */
	BGL_EXPORTED_DEF uint16_t BGl_bitzd2urshu16zd2zz__bitz00(uint16_t
		BgL_xz00_125, long BgL_yz00_126)
	{
		{	/* Llib/bit.scm 631 */
			return (BgL_xz00_125 >> (int) (BgL_yz00_126));
		}

	}



/* &bit-urshu16 */
	obj_t BGl_z62bitzd2urshu16zb0zz__bitz00(obj_t BgL_envz00_1392,
		obj_t BgL_xz00_1393, obj_t BgL_yz00_1394)
	{
		{	/* Llib/bit.scm 631 */
			{	/* Llib/bit.scm 631 */
				uint16_t BgL_tmpz00_3075;

				{	/* Llib/bit.scm 631 */
					long BgL_auxz00_3085;
					uint16_t BgL_auxz00_3076;

					{	/* Llib/bit.scm 631 */
						obj_t BgL_tmpz00_3086;

						if (INTEGERP(BgL_yz00_1394))
							{	/* Llib/bit.scm 631 */
								BgL_tmpz00_3086 = BgL_yz00_1394;
							}
						else
							{
								obj_t BgL_auxz00_3089;

								BgL_auxz00_3089 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(29590L), BGl_string1767z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_yz00_1394);
								FAILURE(BgL_auxz00_3089, BFALSE, BFALSE);
							}
						BgL_auxz00_3085 = (long) CINT(BgL_tmpz00_3086);
					}
					{	/* Llib/bit.scm 631 */
						obj_t BgL_tmpz00_3077;

						if (BGL_UINT16P(BgL_xz00_1393))
							{	/* Llib/bit.scm 631 */
								BgL_tmpz00_3077 = BgL_xz00_1393;
							}
						else
							{
								obj_t BgL_auxz00_3080;

								BgL_auxz00_3080 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(29590L), BGl_string1767z00zz__bitz00,
									BGl_string1701z00zz__bitz00, BgL_xz00_1393);
								FAILURE(BgL_auxz00_3080, BFALSE, BFALSE);
							}
						BgL_auxz00_3076 = BGL_BUINT16_TO_UINT16(BgL_tmpz00_3077);
					}
					BgL_tmpz00_3075 =
						BGl_bitzd2urshu16zd2zz__bitz00(BgL_auxz00_3076, BgL_auxz00_3085);
				}
				return BGL_UINT16_TO_BUINT16(BgL_tmpz00_3075);
			}
		}

	}



/* bit-urshs32 */
	BGL_EXPORTED_DEF int32_t BGl_bitzd2urshs32zd2zz__bitz00(int32_t BgL_xz00_127,
		long BgL_yz00_128)
	{
		{	/* Llib/bit.scm 632 */
			return (BgL_xz00_127 >> (int) (BgL_yz00_128));
		}

	}



/* &bit-urshs32 */
	obj_t BGl_z62bitzd2urshs32zb0zz__bitz00(obj_t BgL_envz00_1395,
		obj_t BgL_xz00_1396, obj_t BgL_yz00_1397)
	{
		{	/* Llib/bit.scm 632 */
			{	/* Llib/bit.scm 632 */
				int32_t BgL_tmpz00_3098;

				{	/* Llib/bit.scm 632 */
					long BgL_auxz00_3108;
					int32_t BgL_auxz00_3099;

					{	/* Llib/bit.scm 632 */
						obj_t BgL_tmpz00_3109;

						if (INTEGERP(BgL_yz00_1397))
							{	/* Llib/bit.scm 632 */
								BgL_tmpz00_3109 = BgL_yz00_1397;
							}
						else
							{
								obj_t BgL_auxz00_3112;

								BgL_auxz00_3112 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(29642L), BGl_string1768z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_yz00_1397);
								FAILURE(BgL_auxz00_3112, BFALSE, BFALSE);
							}
						BgL_auxz00_3108 = (long) CINT(BgL_tmpz00_3109);
					}
					{	/* Llib/bit.scm 632 */
						obj_t BgL_tmpz00_3100;

						if (BGL_INT32P(BgL_xz00_1396))
							{	/* Llib/bit.scm 632 */
								BgL_tmpz00_3100 = BgL_xz00_1396;
							}
						else
							{
								obj_t BgL_auxz00_3103;

								BgL_auxz00_3103 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(29642L), BGl_string1768z00zz__bitz00,
									BGl_string1703z00zz__bitz00, BgL_xz00_1396);
								FAILURE(BgL_auxz00_3103, BFALSE, BFALSE);
							}
						BgL_auxz00_3099 = BGL_BINT32_TO_INT32(BgL_tmpz00_3100);
					}
					BgL_tmpz00_3098 =
						BGl_bitzd2urshs32zd2zz__bitz00(BgL_auxz00_3099, BgL_auxz00_3108);
				}
				return BGL_INT32_TO_BINT32(BgL_tmpz00_3098);
			}
		}

	}



/* bit-urshu32 */
	BGL_EXPORTED_DEF uint32_t BGl_bitzd2urshu32zd2zz__bitz00(uint32_t
		BgL_xz00_129, long BgL_yz00_130)
	{
		{	/* Llib/bit.scm 633 */
			return (BgL_xz00_129 >> (int) (BgL_yz00_130));
		}

	}



/* &bit-urshu32 */
	obj_t BGl_z62bitzd2urshu32zb0zz__bitz00(obj_t BgL_envz00_1398,
		obj_t BgL_xz00_1399, obj_t BgL_yz00_1400)
	{
		{	/* Llib/bit.scm 633 */
			{	/* Llib/bit.scm 633 */
				uint32_t BgL_tmpz00_3121;

				{	/* Llib/bit.scm 633 */
					long BgL_auxz00_3131;
					uint32_t BgL_auxz00_3122;

					{	/* Llib/bit.scm 633 */
						obj_t BgL_tmpz00_3132;

						if (INTEGERP(BgL_yz00_1400))
							{	/* Llib/bit.scm 633 */
								BgL_tmpz00_3132 = BgL_yz00_1400;
							}
						else
							{
								obj_t BgL_auxz00_3135;

								BgL_auxz00_3135 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(29694L), BGl_string1769z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_yz00_1400);
								FAILURE(BgL_auxz00_3135, BFALSE, BFALSE);
							}
						BgL_auxz00_3131 = (long) CINT(BgL_tmpz00_3132);
					}
					{	/* Llib/bit.scm 633 */
						obj_t BgL_tmpz00_3123;

						if (BGL_UINT32P(BgL_xz00_1399))
							{	/* Llib/bit.scm 633 */
								BgL_tmpz00_3123 = BgL_xz00_1399;
							}
						else
							{
								obj_t BgL_auxz00_3126;

								BgL_auxz00_3126 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(29694L), BGl_string1769z00zz__bitz00,
									BGl_string1705z00zz__bitz00, BgL_xz00_1399);
								FAILURE(BgL_auxz00_3126, BFALSE, BFALSE);
							}
						BgL_auxz00_3122 = BGL_BUINT32_TO_UINT32(BgL_tmpz00_3123);
					}
					BgL_tmpz00_3121 =
						BGl_bitzd2urshu32zd2zz__bitz00(BgL_auxz00_3122, BgL_auxz00_3131);
				}
				return BGL_UINT32_TO_BUINT32(BgL_tmpz00_3121);
			}
		}

	}



/* bit-urshs64 */
	BGL_EXPORTED_DEF int64_t BGl_bitzd2urshs64zd2zz__bitz00(int64_t BgL_xz00_131,
		long BgL_yz00_132)
	{
		{	/* Llib/bit.scm 634 */
			return (BgL_xz00_131 >> (int) (BgL_yz00_132));
		}

	}



/* &bit-urshs64 */
	obj_t BGl_z62bitzd2urshs64zb0zz__bitz00(obj_t BgL_envz00_1401,
		obj_t BgL_xz00_1402, obj_t BgL_yz00_1403)
	{
		{	/* Llib/bit.scm 634 */
			{	/* Llib/bit.scm 634 */
				int64_t BgL_tmpz00_3144;

				{	/* Llib/bit.scm 634 */
					long BgL_auxz00_3154;
					int64_t BgL_auxz00_3145;

					{	/* Llib/bit.scm 634 */
						obj_t BgL_tmpz00_3155;

						if (INTEGERP(BgL_yz00_1403))
							{	/* Llib/bit.scm 634 */
								BgL_tmpz00_3155 = BgL_yz00_1403;
							}
						else
							{
								obj_t BgL_auxz00_3158;

								BgL_auxz00_3158 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(29746L), BGl_string1770z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_yz00_1403);
								FAILURE(BgL_auxz00_3158, BFALSE, BFALSE);
							}
						BgL_auxz00_3154 = (long) CINT(BgL_tmpz00_3155);
					}
					{	/* Llib/bit.scm 634 */
						obj_t BgL_tmpz00_3146;

						if (BGL_INT64P(BgL_xz00_1402))
							{	/* Llib/bit.scm 634 */
								BgL_tmpz00_3146 = BgL_xz00_1402;
							}
						else
							{
								obj_t BgL_auxz00_3149;

								BgL_auxz00_3149 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(29746L), BGl_string1770z00zz__bitz00,
									BGl_string1707z00zz__bitz00, BgL_xz00_1402);
								FAILURE(BgL_auxz00_3149, BFALSE, BFALSE);
							}
						BgL_auxz00_3145 = BGL_BINT64_TO_INT64(BgL_tmpz00_3146);
					}
					BgL_tmpz00_3144 =
						BGl_bitzd2urshs64zd2zz__bitz00(BgL_auxz00_3145, BgL_auxz00_3154);
				}
				return BGL_INT64_TO_BINT64(BgL_tmpz00_3144);
			}
		}

	}



/* bit-urshu64 */
	BGL_EXPORTED_DEF uint64_t BGl_bitzd2urshu64zd2zz__bitz00(uint64_t
		BgL_xz00_133, long BgL_yz00_134)
	{
		{	/* Llib/bit.scm 635 */
			return (BgL_xz00_133 >> (int) (BgL_yz00_134));
		}

	}



/* &bit-urshu64 */
	obj_t BGl_z62bitzd2urshu64zb0zz__bitz00(obj_t BgL_envz00_1404,
		obj_t BgL_xz00_1405, obj_t BgL_yz00_1406)
	{
		{	/* Llib/bit.scm 635 */
			{	/* Llib/bit.scm 635 */
				uint64_t BgL_tmpz00_3167;

				{	/* Llib/bit.scm 635 */
					long BgL_auxz00_3177;
					uint64_t BgL_auxz00_3168;

					{	/* Llib/bit.scm 635 */
						obj_t BgL_tmpz00_3178;

						if (INTEGERP(BgL_yz00_1406))
							{	/* Llib/bit.scm 635 */
								BgL_tmpz00_3178 = BgL_yz00_1406;
							}
						else
							{
								obj_t BgL_auxz00_3181;

								BgL_auxz00_3181 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(29798L), BGl_string1771z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_yz00_1406);
								FAILURE(BgL_auxz00_3181, BFALSE, BFALSE);
							}
						BgL_auxz00_3177 = (long) CINT(BgL_tmpz00_3178);
					}
					{	/* Llib/bit.scm 635 */
						obj_t BgL_tmpz00_3169;

						if (BGL_UINT64P(BgL_xz00_1405))
							{	/* Llib/bit.scm 635 */
								BgL_tmpz00_3169 = BgL_xz00_1405;
							}
						else
							{
								obj_t BgL_auxz00_3172;

								BgL_auxz00_3172 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(29798L), BGl_string1771z00zz__bitz00,
									BGl_string1709z00zz__bitz00, BgL_xz00_1405);
								FAILURE(BgL_auxz00_3172, BFALSE, BFALSE);
							}
						BgL_auxz00_3168 = BGL_BINT64_TO_INT64(BgL_tmpz00_3169);
					}
					BgL_tmpz00_3167 =
						BGl_bitzd2urshu64zd2zz__bitz00(BgL_auxz00_3168, BgL_auxz00_3177);
				}
				return BGL_UINT64_TO_BUINT64(BgL_tmpz00_3167);
			}
		}

	}



/* bit-lsh */
	BGL_EXPORTED_DEF long BGl_bitzd2lshzd2zz__bitz00(long BgL_xz00_135,
		long BgL_yz00_136)
	{
		{	/* Llib/bit.scm 640 */
			return (BgL_xz00_135 << (int) (BgL_yz00_136));
		}

	}



/* &bit-lsh */
	obj_t BGl_z62bitzd2lshzb0zz__bitz00(obj_t BgL_envz00_1407,
		obj_t BgL_xz00_1408, obj_t BgL_yz00_1409)
	{
		{	/* Llib/bit.scm 640 */
			{	/* Llib/bit.scm 640 */
				long BgL_tmpz00_3190;

				{	/* Llib/bit.scm 640 */
					long BgL_auxz00_3200;
					long BgL_auxz00_3191;

					{	/* Llib/bit.scm 640 */
						obj_t BgL_tmpz00_3201;

						if (INTEGERP(BgL_yz00_1409))
							{	/* Llib/bit.scm 640 */
								BgL_tmpz00_3201 = BgL_yz00_1409;
							}
						else
							{
								obj_t BgL_auxz00_3204;

								BgL_auxz00_3204 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(30076L), BGl_string1772z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_yz00_1409);
								FAILURE(BgL_auxz00_3204, BFALSE, BFALSE);
							}
						BgL_auxz00_3200 = (long) CINT(BgL_tmpz00_3201);
					}
					{	/* Llib/bit.scm 640 */
						obj_t BgL_tmpz00_3192;

						if (INTEGERP(BgL_xz00_1408))
							{	/* Llib/bit.scm 640 */
								BgL_tmpz00_3192 = BgL_xz00_1408;
							}
						else
							{
								obj_t BgL_auxz00_3195;

								BgL_auxz00_3195 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(30076L), BGl_string1772z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_xz00_1408);
								FAILURE(BgL_auxz00_3195, BFALSE, BFALSE);
							}
						BgL_auxz00_3191 = (long) CINT(BgL_tmpz00_3192);
					}
					BgL_tmpz00_3190 =
						BGl_bitzd2lshzd2zz__bitz00(BgL_auxz00_3191, BgL_auxz00_3200);
				}
				return BINT(BgL_tmpz00_3190);
			}
		}

	}



/* bit-lshelong */
	BGL_EXPORTED_DEF long BGl_bitzd2lshelongzd2zz__bitz00(long BgL_xz00_137,
		long BgL_yz00_138)
	{
		{	/* Llib/bit.scm 641 */
			return (BgL_xz00_137 << (int) (BgL_yz00_138));
		}

	}



/* &bit-lshelong */
	obj_t BGl_z62bitzd2lshelongzb0zz__bitz00(obj_t BgL_envz00_1410,
		obj_t BgL_xz00_1411, obj_t BgL_yz00_1412)
	{
		{	/* Llib/bit.scm 641 */
			{	/* Llib/bit.scm 641 */
				long BgL_tmpz00_3213;

				{	/* Llib/bit.scm 641 */
					long BgL_auxz00_3223;
					long BgL_auxz00_3214;

					{	/* Llib/bit.scm 641 */
						obj_t BgL_tmpz00_3224;

						if (INTEGERP(BgL_yz00_1412))
							{	/* Llib/bit.scm 641 */
								BgL_tmpz00_3224 = BgL_yz00_1412;
							}
						else
							{
								obj_t BgL_auxz00_3227;

								BgL_auxz00_3227 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(30125L), BGl_string1773z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_yz00_1412);
								FAILURE(BgL_auxz00_3227, BFALSE, BFALSE);
							}
						BgL_auxz00_3223 = (long) CINT(BgL_tmpz00_3224);
					}
					{	/* Llib/bit.scm 641 */
						obj_t BgL_tmpz00_3215;

						if (ELONGP(BgL_xz00_1411))
							{	/* Llib/bit.scm 641 */
								BgL_tmpz00_3215 = BgL_xz00_1411;
							}
						else
							{
								obj_t BgL_auxz00_3218;

								BgL_auxz00_3218 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(30125L), BGl_string1773z00zz__bitz00,
									BGl_string1691z00zz__bitz00, BgL_xz00_1411);
								FAILURE(BgL_auxz00_3218, BFALSE, BFALSE);
							}
						BgL_auxz00_3214 = BELONG_TO_LONG(BgL_tmpz00_3215);
					}
					BgL_tmpz00_3213 =
						BGl_bitzd2lshelongzd2zz__bitz00(BgL_auxz00_3214, BgL_auxz00_3223);
				}
				return make_belong(BgL_tmpz00_3213);
			}
		}

	}



/* bit-lshllong */
	BGL_EXPORTED_DEF BGL_LONGLONG_T BGl_bitzd2lshllongzd2zz__bitz00(BGL_LONGLONG_T
		BgL_xz00_139, long BgL_yz00_140)
	{
		{	/* Llib/bit.scm 642 */
			return (BgL_xz00_139 << (int) (BgL_yz00_140));
		}

	}



/* &bit-lshllong */
	obj_t BGl_z62bitzd2lshllongzb0zz__bitz00(obj_t BgL_envz00_1413,
		obj_t BgL_xz00_1414, obj_t BgL_yz00_1415)
	{
		{	/* Llib/bit.scm 642 */
			{	/* Llib/bit.scm 642 */
				BGL_LONGLONG_T BgL_tmpz00_3236;

				{	/* Llib/bit.scm 642 */
					long BgL_auxz00_3246;
					BGL_LONGLONG_T BgL_auxz00_3237;

					{	/* Llib/bit.scm 642 */
						obj_t BgL_tmpz00_3247;

						if (INTEGERP(BgL_yz00_1415))
							{	/* Llib/bit.scm 642 */
								BgL_tmpz00_3247 = BgL_yz00_1415;
							}
						else
							{
								obj_t BgL_auxz00_3250;

								BgL_auxz00_3250 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(30179L), BGl_string1774z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_yz00_1415);
								FAILURE(BgL_auxz00_3250, BFALSE, BFALSE);
							}
						BgL_auxz00_3246 = (long) CINT(BgL_tmpz00_3247);
					}
					{	/* Llib/bit.scm 642 */
						obj_t BgL_tmpz00_3238;

						if (LLONGP(BgL_xz00_1414))
							{	/* Llib/bit.scm 642 */
								BgL_tmpz00_3238 = BgL_xz00_1414;
							}
						else
							{
								obj_t BgL_auxz00_3241;

								BgL_auxz00_3241 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(30179L), BGl_string1774z00zz__bitz00,
									BGl_string1693z00zz__bitz00, BgL_xz00_1414);
								FAILURE(BgL_auxz00_3241, BFALSE, BFALSE);
							}
						BgL_auxz00_3237 = BLLONG_TO_LLONG(BgL_tmpz00_3238);
					}
					BgL_tmpz00_3236 =
						BGl_bitzd2lshllongzd2zz__bitz00(BgL_auxz00_3237, BgL_auxz00_3246);
				}
				return make_bllong(BgL_tmpz00_3236);
			}
		}

	}



/* bit-lshs8 */
	BGL_EXPORTED_DEF int8_t BGl_bitzd2lshs8zd2zz__bitz00(int8_t BgL_xz00_141,
		long BgL_yz00_142)
	{
		{	/* Llib/bit.scm 643 */
			return (BgL_xz00_141 << (int) (BgL_yz00_142));
		}

	}



/* &bit-lshs8 */
	obj_t BGl_z62bitzd2lshs8zb0zz__bitz00(obj_t BgL_envz00_1416,
		obj_t BgL_xz00_1417, obj_t BgL_yz00_1418)
	{
		{	/* Llib/bit.scm 643 */
			{	/* Llib/bit.scm 643 */
				int8_t BgL_tmpz00_3259;

				{	/* Llib/bit.scm 643 */
					long BgL_auxz00_3269;
					int8_t BgL_auxz00_3260;

					{	/* Llib/bit.scm 643 */
						obj_t BgL_tmpz00_3270;

						if (INTEGERP(BgL_yz00_1418))
							{	/* Llib/bit.scm 643 */
								BgL_tmpz00_3270 = BgL_yz00_1418;
							}
						else
							{
								obj_t BgL_auxz00_3273;

								BgL_auxz00_3273 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(30230L), BGl_string1775z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_yz00_1418);
								FAILURE(BgL_auxz00_3273, BFALSE, BFALSE);
							}
						BgL_auxz00_3269 = (long) CINT(BgL_tmpz00_3270);
					}
					{	/* Llib/bit.scm 643 */
						obj_t BgL_tmpz00_3261;

						if (BGL_INT8P(BgL_xz00_1417))
							{	/* Llib/bit.scm 643 */
								BgL_tmpz00_3261 = BgL_xz00_1417;
							}
						else
							{
								obj_t BgL_auxz00_3264;

								BgL_auxz00_3264 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(30230L), BGl_string1775z00zz__bitz00,
									BGl_string1695z00zz__bitz00, BgL_xz00_1417);
								FAILURE(BgL_auxz00_3264, BFALSE, BFALSE);
							}
						BgL_auxz00_3260 = BGL_BINT8_TO_INT8(BgL_tmpz00_3261);
					}
					BgL_tmpz00_3259 =
						BGl_bitzd2lshs8zd2zz__bitz00(BgL_auxz00_3260, BgL_auxz00_3269);
				}
				return BGL_INT8_TO_BINT8(BgL_tmpz00_3259);
			}
		}

	}



/* bit-lshu8 */
	BGL_EXPORTED_DEF uint8_t BGl_bitzd2lshu8zd2zz__bitz00(uint8_t BgL_xz00_143,
		long BgL_yz00_144)
	{
		{	/* Llib/bit.scm 644 */
			return (BgL_xz00_143 << (int) (BgL_yz00_144));
		}

	}



/* &bit-lshu8 */
	obj_t BGl_z62bitzd2lshu8zb0zz__bitz00(obj_t BgL_envz00_1419,
		obj_t BgL_xz00_1420, obj_t BgL_yz00_1421)
	{
		{	/* Llib/bit.scm 644 */
			{	/* Llib/bit.scm 644 */
				uint8_t BgL_tmpz00_3282;

				{	/* Llib/bit.scm 644 */
					long BgL_auxz00_3292;
					uint8_t BgL_auxz00_3283;

					{	/* Llib/bit.scm 644 */
						obj_t BgL_tmpz00_3293;

						if (INTEGERP(BgL_yz00_1421))
							{	/* Llib/bit.scm 644 */
								BgL_tmpz00_3293 = BgL_yz00_1421;
							}
						else
							{
								obj_t BgL_auxz00_3296;

								BgL_auxz00_3296 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(30278L), BGl_string1776z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_yz00_1421);
								FAILURE(BgL_auxz00_3296, BFALSE, BFALSE);
							}
						BgL_auxz00_3292 = (long) CINT(BgL_tmpz00_3293);
					}
					{	/* Llib/bit.scm 644 */
						obj_t BgL_tmpz00_3284;

						if (BGL_UINT8P(BgL_xz00_1420))
							{	/* Llib/bit.scm 644 */
								BgL_tmpz00_3284 = BgL_xz00_1420;
							}
						else
							{
								obj_t BgL_auxz00_3287;

								BgL_auxz00_3287 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(30278L), BGl_string1776z00zz__bitz00,
									BGl_string1697z00zz__bitz00, BgL_xz00_1420);
								FAILURE(BgL_auxz00_3287, BFALSE, BFALSE);
							}
						BgL_auxz00_3283 = BGL_BUINT8_TO_UINT8(BgL_tmpz00_3284);
					}
					BgL_tmpz00_3282 =
						BGl_bitzd2lshu8zd2zz__bitz00(BgL_auxz00_3283, BgL_auxz00_3292);
				}
				return BGL_UINT8_TO_BUINT8(BgL_tmpz00_3282);
			}
		}

	}



/* bit-lshs16 */
	BGL_EXPORTED_DEF int16_t BGl_bitzd2lshs16zd2zz__bitz00(int16_t BgL_xz00_145,
		long BgL_yz00_146)
	{
		{	/* Llib/bit.scm 645 */
			return (BgL_xz00_145 << (int) (BgL_yz00_146));
		}

	}



/* &bit-lshs16 */
	obj_t BGl_z62bitzd2lshs16zb0zz__bitz00(obj_t BgL_envz00_1422,
		obj_t BgL_xz00_1423, obj_t BgL_yz00_1424)
	{
		{	/* Llib/bit.scm 645 */
			{	/* Llib/bit.scm 645 */
				int16_t BgL_tmpz00_3305;

				{	/* Llib/bit.scm 645 */
					long BgL_auxz00_3315;
					int16_t BgL_auxz00_3306;

					{	/* Llib/bit.scm 645 */
						obj_t BgL_tmpz00_3316;

						if (INTEGERP(BgL_yz00_1424))
							{	/* Llib/bit.scm 645 */
								BgL_tmpz00_3316 = BgL_yz00_1424;
							}
						else
							{
								obj_t BgL_auxz00_3319;

								BgL_auxz00_3319 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(30327L), BGl_string1777z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_yz00_1424);
								FAILURE(BgL_auxz00_3319, BFALSE, BFALSE);
							}
						BgL_auxz00_3315 = (long) CINT(BgL_tmpz00_3316);
					}
					{	/* Llib/bit.scm 645 */
						obj_t BgL_tmpz00_3307;

						if (BGL_INT16P(BgL_xz00_1423))
							{	/* Llib/bit.scm 645 */
								BgL_tmpz00_3307 = BgL_xz00_1423;
							}
						else
							{
								obj_t BgL_auxz00_3310;

								BgL_auxz00_3310 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(30327L), BGl_string1777z00zz__bitz00,
									BGl_string1699z00zz__bitz00, BgL_xz00_1423);
								FAILURE(BgL_auxz00_3310, BFALSE, BFALSE);
							}
						BgL_auxz00_3306 = BGL_BINT16_TO_INT16(BgL_tmpz00_3307);
					}
					BgL_tmpz00_3305 =
						BGl_bitzd2lshs16zd2zz__bitz00(BgL_auxz00_3306, BgL_auxz00_3315);
				}
				return BGL_INT16_TO_BINT16(BgL_tmpz00_3305);
			}
		}

	}



/* bit-lshu16 */
	BGL_EXPORTED_DEF uint16_t BGl_bitzd2lshu16zd2zz__bitz00(uint16_t BgL_xz00_147,
		long BgL_yz00_148)
	{
		{	/* Llib/bit.scm 646 */
			return (BgL_xz00_147 << (int) (BgL_yz00_148));
		}

	}



/* &bit-lshu16 */
	obj_t BGl_z62bitzd2lshu16zb0zz__bitz00(obj_t BgL_envz00_1425,
		obj_t BgL_xz00_1426, obj_t BgL_yz00_1427)
	{
		{	/* Llib/bit.scm 646 */
			{	/* Llib/bit.scm 646 */
				uint16_t BgL_tmpz00_3328;

				{	/* Llib/bit.scm 646 */
					long BgL_auxz00_3338;
					uint16_t BgL_auxz00_3329;

					{	/* Llib/bit.scm 646 */
						obj_t BgL_tmpz00_3339;

						if (INTEGERP(BgL_yz00_1427))
							{	/* Llib/bit.scm 646 */
								BgL_tmpz00_3339 = BgL_yz00_1427;
							}
						else
							{
								obj_t BgL_auxz00_3342;

								BgL_auxz00_3342 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(30377L), BGl_string1778z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_yz00_1427);
								FAILURE(BgL_auxz00_3342, BFALSE, BFALSE);
							}
						BgL_auxz00_3338 = (long) CINT(BgL_tmpz00_3339);
					}
					{	/* Llib/bit.scm 646 */
						obj_t BgL_tmpz00_3330;

						if (BGL_UINT16P(BgL_xz00_1426))
							{	/* Llib/bit.scm 646 */
								BgL_tmpz00_3330 = BgL_xz00_1426;
							}
						else
							{
								obj_t BgL_auxz00_3333;

								BgL_auxz00_3333 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(30377L), BGl_string1778z00zz__bitz00,
									BGl_string1701z00zz__bitz00, BgL_xz00_1426);
								FAILURE(BgL_auxz00_3333, BFALSE, BFALSE);
							}
						BgL_auxz00_3329 = BGL_BUINT16_TO_UINT16(BgL_tmpz00_3330);
					}
					BgL_tmpz00_3328 =
						BGl_bitzd2lshu16zd2zz__bitz00(BgL_auxz00_3329, BgL_auxz00_3338);
				}
				return BGL_UINT16_TO_BUINT16(BgL_tmpz00_3328);
			}
		}

	}



/* bit-lshs32 */
	BGL_EXPORTED_DEF int32_t BGl_bitzd2lshs32zd2zz__bitz00(int32_t BgL_xz00_149,
		long BgL_yz00_150)
	{
		{	/* Llib/bit.scm 647 */
			return (BgL_xz00_149 << (int) (BgL_yz00_150));
		}

	}



/* &bit-lshs32 */
	obj_t BGl_z62bitzd2lshs32zb0zz__bitz00(obj_t BgL_envz00_1428,
		obj_t BgL_xz00_1429, obj_t BgL_yz00_1430)
	{
		{	/* Llib/bit.scm 647 */
			{	/* Llib/bit.scm 647 */
				int32_t BgL_tmpz00_3351;

				{	/* Llib/bit.scm 647 */
					long BgL_auxz00_3361;
					int32_t BgL_auxz00_3352;

					{	/* Llib/bit.scm 647 */
						obj_t BgL_tmpz00_3362;

						if (INTEGERP(BgL_yz00_1430))
							{	/* Llib/bit.scm 647 */
								BgL_tmpz00_3362 = BgL_yz00_1430;
							}
						else
							{
								obj_t BgL_auxz00_3365;

								BgL_auxz00_3365 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(30427L), BGl_string1779z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_yz00_1430);
								FAILURE(BgL_auxz00_3365, BFALSE, BFALSE);
							}
						BgL_auxz00_3361 = (long) CINT(BgL_tmpz00_3362);
					}
					{	/* Llib/bit.scm 647 */
						obj_t BgL_tmpz00_3353;

						if (BGL_INT32P(BgL_xz00_1429))
							{	/* Llib/bit.scm 647 */
								BgL_tmpz00_3353 = BgL_xz00_1429;
							}
						else
							{
								obj_t BgL_auxz00_3356;

								BgL_auxz00_3356 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(30427L), BGl_string1779z00zz__bitz00,
									BGl_string1703z00zz__bitz00, BgL_xz00_1429);
								FAILURE(BgL_auxz00_3356, BFALSE, BFALSE);
							}
						BgL_auxz00_3352 = BGL_BINT32_TO_INT32(BgL_tmpz00_3353);
					}
					BgL_tmpz00_3351 =
						BGl_bitzd2lshs32zd2zz__bitz00(BgL_auxz00_3352, BgL_auxz00_3361);
				}
				return BGL_INT32_TO_BINT32(BgL_tmpz00_3351);
			}
		}

	}



/* bit-lshu32 */
	BGL_EXPORTED_DEF uint32_t BGl_bitzd2lshu32zd2zz__bitz00(uint32_t BgL_xz00_151,
		long BgL_yz00_152)
	{
		{	/* Llib/bit.scm 648 */
			return (BgL_xz00_151 << (int) (BgL_yz00_152));
		}

	}



/* &bit-lshu32 */
	obj_t BGl_z62bitzd2lshu32zb0zz__bitz00(obj_t BgL_envz00_1431,
		obj_t BgL_xz00_1432, obj_t BgL_yz00_1433)
	{
		{	/* Llib/bit.scm 648 */
			{	/* Llib/bit.scm 648 */
				uint32_t BgL_tmpz00_3374;

				{	/* Llib/bit.scm 648 */
					long BgL_auxz00_3384;
					uint32_t BgL_auxz00_3375;

					{	/* Llib/bit.scm 648 */
						obj_t BgL_tmpz00_3385;

						if (INTEGERP(BgL_yz00_1433))
							{	/* Llib/bit.scm 648 */
								BgL_tmpz00_3385 = BgL_yz00_1433;
							}
						else
							{
								obj_t BgL_auxz00_3388;

								BgL_auxz00_3388 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(30477L), BGl_string1780z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_yz00_1433);
								FAILURE(BgL_auxz00_3388, BFALSE, BFALSE);
							}
						BgL_auxz00_3384 = (long) CINT(BgL_tmpz00_3385);
					}
					{	/* Llib/bit.scm 648 */
						obj_t BgL_tmpz00_3376;

						if (BGL_UINT32P(BgL_xz00_1432))
							{	/* Llib/bit.scm 648 */
								BgL_tmpz00_3376 = BgL_xz00_1432;
							}
						else
							{
								obj_t BgL_auxz00_3379;

								BgL_auxz00_3379 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(30477L), BGl_string1780z00zz__bitz00,
									BGl_string1705z00zz__bitz00, BgL_xz00_1432);
								FAILURE(BgL_auxz00_3379, BFALSE, BFALSE);
							}
						BgL_auxz00_3375 = BGL_BUINT32_TO_UINT32(BgL_tmpz00_3376);
					}
					BgL_tmpz00_3374 =
						BGl_bitzd2lshu32zd2zz__bitz00(BgL_auxz00_3375, BgL_auxz00_3384);
				}
				return BGL_UINT32_TO_BUINT32(BgL_tmpz00_3374);
			}
		}

	}



/* bit-lshs64 */
	BGL_EXPORTED_DEF int64_t BGl_bitzd2lshs64zd2zz__bitz00(int64_t BgL_xz00_153,
		long BgL_yz00_154)
	{
		{	/* Llib/bit.scm 649 */
			return (BgL_xz00_153 << (int) (BgL_yz00_154));
		}

	}



/* &bit-lshs64 */
	obj_t BGl_z62bitzd2lshs64zb0zz__bitz00(obj_t BgL_envz00_1434,
		obj_t BgL_xz00_1435, obj_t BgL_yz00_1436)
	{
		{	/* Llib/bit.scm 649 */
			{	/* Llib/bit.scm 649 */
				int64_t BgL_tmpz00_3397;

				{	/* Llib/bit.scm 649 */
					long BgL_auxz00_3407;
					int64_t BgL_auxz00_3398;

					{	/* Llib/bit.scm 649 */
						obj_t BgL_tmpz00_3408;

						if (INTEGERP(BgL_yz00_1436))
							{	/* Llib/bit.scm 649 */
								BgL_tmpz00_3408 = BgL_yz00_1436;
							}
						else
							{
								obj_t BgL_auxz00_3411;

								BgL_auxz00_3411 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(30527L), BGl_string1781z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_yz00_1436);
								FAILURE(BgL_auxz00_3411, BFALSE, BFALSE);
							}
						BgL_auxz00_3407 = (long) CINT(BgL_tmpz00_3408);
					}
					{	/* Llib/bit.scm 649 */
						obj_t BgL_tmpz00_3399;

						if (BGL_INT64P(BgL_xz00_1435))
							{	/* Llib/bit.scm 649 */
								BgL_tmpz00_3399 = BgL_xz00_1435;
							}
						else
							{
								obj_t BgL_auxz00_3402;

								BgL_auxz00_3402 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(30527L), BGl_string1781z00zz__bitz00,
									BGl_string1707z00zz__bitz00, BgL_xz00_1435);
								FAILURE(BgL_auxz00_3402, BFALSE, BFALSE);
							}
						BgL_auxz00_3398 = BGL_BINT64_TO_INT64(BgL_tmpz00_3399);
					}
					BgL_tmpz00_3397 =
						BGl_bitzd2lshs64zd2zz__bitz00(BgL_auxz00_3398, BgL_auxz00_3407);
				}
				return BGL_INT64_TO_BINT64(BgL_tmpz00_3397);
			}
		}

	}



/* bit-lshu64 */
	BGL_EXPORTED_DEF uint64_t BGl_bitzd2lshu64zd2zz__bitz00(uint64_t BgL_xz00_155,
		long BgL_yz00_156)
	{
		{	/* Llib/bit.scm 650 */
			return (BgL_xz00_155 << (int) (BgL_yz00_156));
		}

	}



/* &bit-lshu64 */
	obj_t BGl_z62bitzd2lshu64zb0zz__bitz00(obj_t BgL_envz00_1437,
		obj_t BgL_xz00_1438, obj_t BgL_yz00_1439)
	{
		{	/* Llib/bit.scm 650 */
			{	/* Llib/bit.scm 650 */
				uint64_t BgL_tmpz00_3420;

				{	/* Llib/bit.scm 650 */
					long BgL_auxz00_3430;
					uint64_t BgL_auxz00_3421;

					{	/* Llib/bit.scm 650 */
						obj_t BgL_tmpz00_3431;

						if (INTEGERP(BgL_yz00_1439))
							{	/* Llib/bit.scm 650 */
								BgL_tmpz00_3431 = BgL_yz00_1439;
							}
						else
							{
								obj_t BgL_auxz00_3434;

								BgL_auxz00_3434 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(30577L), BGl_string1782z00zz__bitz00,
									BGl_string1689z00zz__bitz00, BgL_yz00_1439);
								FAILURE(BgL_auxz00_3434, BFALSE, BFALSE);
							}
						BgL_auxz00_3430 = (long) CINT(BgL_tmpz00_3431);
					}
					{	/* Llib/bit.scm 650 */
						obj_t BgL_tmpz00_3422;

						if (BGL_UINT64P(BgL_xz00_1438))
							{	/* Llib/bit.scm 650 */
								BgL_tmpz00_3422 = BgL_xz00_1438;
							}
						else
							{
								obj_t BgL_auxz00_3425;

								BgL_auxz00_3425 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
									BINT(30577L), BGl_string1782z00zz__bitz00,
									BGl_string1709z00zz__bitz00, BgL_xz00_1438);
								FAILURE(BgL_auxz00_3425, BFALSE, BFALSE);
							}
						BgL_auxz00_3421 = BGL_BINT64_TO_INT64(BgL_tmpz00_3422);
					}
					BgL_tmpz00_3420 =
						BGl_bitzd2lshu64zd2zz__bitz00(BgL_auxz00_3421, BgL_auxz00_3430);
				}
				return BGL_UINT64_TO_BUINT64(BgL_tmpz00_3420);
			}
		}

	}



/* bit-lshbx */
	BGL_EXPORTED_DEF obj_t BGl_bitzd2lshbxzd2zz__bitz00(obj_t BgL_xz00_157,
		long BgL_yz00_158)
	{
		{	/* Llib/bit.scm 651 */
			BGL_TAIL return bgl_bignum_lsh(BgL_xz00_157, BgL_yz00_158);
		}

	}



/* &bit-lshbx */
	obj_t BGl_z62bitzd2lshbxzb0zz__bitz00(obj_t BgL_envz00_1440,
		obj_t BgL_xz00_1441, obj_t BgL_yz00_1442)
	{
		{	/* Llib/bit.scm 651 */
			{	/* Llib/bit.scm 651 */
				long BgL_auxz00_3449;
				obj_t BgL_auxz00_3442;

				{	/* Llib/bit.scm 651 */
					obj_t BgL_tmpz00_3450;

					if (INTEGERP(BgL_yz00_1442))
						{	/* Llib/bit.scm 651 */
							BgL_tmpz00_3450 = BgL_yz00_1442;
						}
					else
						{
							obj_t BgL_auxz00_3453;

							BgL_auxz00_3453 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
								BINT(30626L), BGl_string1783z00zz__bitz00,
								BGl_string1689z00zz__bitz00, BgL_yz00_1442);
							FAILURE(BgL_auxz00_3453, BFALSE, BFALSE);
						}
					BgL_auxz00_3449 = (long) CINT(BgL_tmpz00_3450);
				}
				if (BIGNUMP(BgL_xz00_1441))
					{	/* Llib/bit.scm 651 */
						BgL_auxz00_3442 = BgL_xz00_1441;
					}
				else
					{
						obj_t BgL_auxz00_3445;

						BgL_auxz00_3445 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1687z00zz__bitz00,
							BINT(30626L), BGl_string1783z00zz__bitz00,
							BGl_string1711z00zz__bitz00, BgL_xz00_1441);
						FAILURE(BgL_auxz00_3445, BFALSE, BFALSE);
					}
				return BGl_bitzd2lshbxzd2zz__bitz00(BgL_auxz00_3442, BgL_auxz00_3449);
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__bitz00(void)
	{
		{	/* Llib/bit.scm 14 */
			return
				BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1784z00zz__bitz00));
		}

	}

#ifdef __cplusplus
}
#endif
