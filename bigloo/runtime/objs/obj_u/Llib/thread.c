/*===========================================================================*/
/*   (Llib/thread.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Llib/thread.scm -indent -o objs/obj_u/Llib/thread.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___THREAD_TYPE_DEFINITIONS
#define BGL___THREAD_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_nothreadzd2backendzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_namez00;
	}                            *BgL_nothreadzd2backendzd2_bglt;

	typedef struct BgL_threadzd2backendzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_namez00;
	}                          *BgL_threadzd2backendzd2_bglt;

	typedef struct BgL_threadz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_namez00;
	}                *BgL_threadz00_bglt;

	typedef struct BgL_nothreadz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_namez00;
		obj_t BgL_bodyz00;
		obj_t BgL_z52specificz52;
		obj_t BgL_z52cleanupz52;
		obj_t BgL_endzd2resultzd2;
		obj_t BgL_endzd2exceptionzd2;
		obj_t BgL_z52namez52;
	}                  *BgL_nothreadz00_bglt;


#endif													// BGL___THREAD_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62threadzd2killz12zd2nothrea1242z70zz__threadz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62threadzd2namezd2setz12z70zz__threadz00(obj_t, obj_t,
		obj_t);
	extern obj_t bgl_make_nil_condvar(void);
	static obj_t BGl_z52currentzd2threadz80zz__threadz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_tbzd2condvarzd2initializa7ez12zb5zz__threadz00
		(BgL_threadzd2backendzd2_bglt, obj_t);
	static obj_t BGl_z62threadzd2namezd2setz12zd2not1254za2zz__threadz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62threadzd2joinz12za2zz__threadz00(obj_t, obj_t, obj_t);
	static obj_t BGl__makezd2mutexzd2zz__threadz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_mutexzd2lockz12zc0zz__threadz00(obj_t, long);
	BGL_EXPORTED_DECL obj_t
		BGl_threadzd2specificzd2zz__threadz00(BgL_threadz00_bglt);
	static obj_t BGl_z62threadzd2specificzb0zz__threadz00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__threadz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_withzd2lockzd2zz__threadz00(obj_t, obj_t);
	extern obj_t BGl_raisez00zz__errorz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_threadzd2cleanupzd2setz12z12zz__threadz00(BgL_threadz00_bglt, obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_conditionzd2variablezd2broadcastz12z12zz__threadz00(obj_t);
	static obj_t BGl_z62tbzd2mutexzd2initializa7ez12zd7zz__threadz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_makezd2mutexzd2zz__threadz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_threadzd2sleepz12zc0zz__threadz00(obj_t);
	extern obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_z62threadzd2parameterzb0zz__threadz00(obj_t, obj_t);
	extern obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62threadzd2startzd2joinabl1199z62zz__threadz00(obj_t,
		obj_t);
	static obj_t BGl_z62z52userzd2threadzd2sleepz121229z22zz__threadz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_nothreadzd2backendzd2zz__threadz00 = BUNSPEC;
	static obj_t BGl_z62threadzd2startzd2joinablez12z70zz__threadz00(obj_t,
		obj_t);
	static obj_t BGl_toplevelzd2initzd2zz__threadz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_z52userzd2threadzd2yieldz12z40zz__threadz00(BgL_threadz00_bglt);
	static obj_t BGl_z62conditionzd2variablezd2namez62zz__threadz00(obj_t, obj_t);
	extern obj_t bgl_make_nil_mutex(void);
	BGL_EXPORTED_DECL obj_t BGl_withzd2timedzd2lockz00zz__threadz00(obj_t, int,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tbzd2mutexzd2initializa7ez12zb5zz__threadz00
		(BgL_threadzd2backendzd2_bglt, obj_t);
	static obj_t BGl_z62withzd2timedzd2lockz62zz__threadz00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_currentzd2threadzd2backendz00zz__threadz00(void);
	static BgL_nothreadzd2backendzd2_bglt
		BGl_za2nothreadzd2backendza2zd2zz__threadz00;
	static obj_t BGl_z62currentzd2dynamiczd2envz62zz__threadz00(obj_t);
	extern obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62threadzd2name1218zb0zz__threadz00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zz__threadz00(void);
	BGL_EXPORTED_DECL obj_t BGl_mutexzd2namezd2zz__threadz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31400ze3ze5zz__threadz00(obj_t, obj_t);
	static obj_t BGl_z62mutexzd2nilzb0zz__threadz00(obj_t);
	static obj_t BGl__makezd2spinlockzd2zz__threadz00(obj_t, obj_t);
	extern obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_currentzd2threadzd2backendzd2setz12zc0zz__threadz00
		(BgL_threadzd2backendzd2_bglt);
	static obj_t BGl_objectzd2initzd2zz__threadz00(void);
	extern obj_t BGl_2za2za2zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62threadzd2specific1209zb0zz__threadz00(obj_t, obj_t);
	static obj_t BGl_z62defaultzd2threadzd2backendzd2setz12za2zz__threadz00(obj_t,
		obj_t);
	static obj_t BGl_z62threadzd2startz12zd2nothre1234z70zz__threadz00(obj_t,
		obj_t, obj_t);
	extern double BGl_numberzd2ze3flonumz31zz__r4_numbers_6_5z00(obj_t);
	static obj_t BGl_z62threadzd2specificzd2setz12z70zz__threadz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_z62exceptionz62zz__objectz00;
	static obj_t BGl_z62mutexzd2namezb0zz__threadz00(obj_t, obj_t);
	static obj_t BGl_z62objectzd2writezd2thread1192z62zz__threadz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31402ze3ze5zz__threadz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_threadzd2killz12zc0zz__threadz00(BgL_threadz00_bglt, int);
	static obj_t BGl_z62z52userzd2threadzd2sleepz12z22zz__threadz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_threadzd2specificzd2setz12z12zz__threadz00(BgL_threadz00_bglt, obj_t);
	static obj_t BGl_z62threadzd2terminatez12zd2no1240z70zz__threadz00(obj_t,
		obj_t);
	static obj_t BGl_z62threadzd2terminatez12za2zz__threadz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_dynamiczd2envzf3z21zz__threadz00(obj_t);
	static obj_t BGl_z62threadzd2startz12za2zz__threadz00(obj_t, obj_t, obj_t);
	extern bool_t BGl_isazf3zf3zz__objectz00(obj_t, obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_z62getzd2threadzd2backendz62zz__threadz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_mutexzd2nilzd2zz__threadz00(void);
	static obj_t BGl_z62mutexzd2unlockz12za2zz__threadz00(obj_t, obj_t);
	static obj_t BGl_z62threadzd2cleanupzb0zz__threadz00(obj_t, obj_t);
	static obj_t BGl_z62tbzd2condvarzd2initializa7ez12zd7zz__threadz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62threadzd2initializa7ez12z05zz__threadz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zz__threadz00(void);
	static obj_t BGl_z62threadzd2yieldz12za2zz__threadz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_threadzd2namezd2zz__threadz00(BgL_threadz00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_makezd2spinlockzd2zz__threadz00(obj_t);
	static obj_t BGl_z62threadzd2initializa7ez121195z05zz__threadz00(obj_t,
		obj_t);
	extern obj_t BGl_objectzd2writezd2zz__objectz00(BgL_objectz00_bglt, obj_t);
	static obj_t BGl_z62tbzd2currentzd2threadz62zz__threadz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_nothreadz00zz__threadz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_threadzd2parameterzd2setz12z12zz__threadz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31511ze3ze5zz__threadz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31414ze3ze5zz__threadz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_mutexzf3zf3zz__threadz00(obj_t);
	static obj_t BGl_z62tbzd2makezd2thread1177z62zz__threadz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_currentzd2dynamiczd2envz00zz__threadz00(void);
	BGL_EXPORTED_DECL bool_t
		BGl_conditionzd2variablezd2waitz12z12zz__threadz00(obj_t, obj_t, long);
	static obj_t BGl_za2conditionzd2variablezd2nilza2z00zz__threadz00 = BUNSPEC;
	static obj_t BGl__makezd2conditionzd2variablez00zz__threadz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31440ze3ze5zz__threadz00(obj_t);
	static obj_t BGl_z62objectzd2displayzd2threa1190z62zz__threadz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62currentzd2threadzb0zz__threadz00(obj_t);
	static obj_t BGl_z62currentzd2threadzd2backendzd2setz12za2zz__threadz00(obj_t,
		obj_t);
	static obj_t BGl_z62currentzd2threadzd2backendz62zz__threadz00(obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_threadzd2backendzd2zz__threadz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31433ze3ze5zz__threadz00(obj_t);
	extern obj_t BGl_callzd2virtualzd2setterz00zz__objectz00(BgL_objectz00_bglt,
		int, obj_t);
	static obj_t BGl_z62threadzd2namezd2nothread1252z62zz__threadz00(obj_t,
		obj_t);
	static obj_t BGl_z62threadzd2specificzd2noth1244z62zz__threadz00(obj_t,
		obj_t);
	static obj_t BGl_z62threadzd2parameterzd2setz12z70zz__threadz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62z52userzd2currentzd2thread1227z30zz__threadz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_threadzd2cleanupzd2zz__threadz00(BgL_threadz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_z52userzd2threadzd2sleepz12z40zz__threadz00(BgL_threadz00_bglt, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31345ze3ze5zz__threadz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31426ze3ze5zz__threadz00(obj_t);
	static obj_t BGl_z62threadzd2cleanupzd2setz121215z70zz__threadz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62z52userzd2threadzd2yieldz121231z22zz__threadz00(obj_t,
		obj_t);
	static obj_t BGl_z62z52userzd2currentzd2threadz30zz__threadz00(obj_t, obj_t);
	extern obj_t BGl_classzd2constructorzd2zz__objectz00(obj_t);
	static obj_t BGl_z62tbzd2currentzd2threadzd2no1184zb0zz__threadz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_conditionzd2variablezd2nilz00zz__threadz00(void);
	static obj_t BGl_za2threadzd2backendsza2zd2zz__threadz00 = BUNSPEC;
	extern obj_t bgl_remq_bang(obj_t, obj_t);
	extern obj_t BGl_exitz00zz__errorz00(obj_t);
	static obj_t BGl_z62threadzd2joinz121202za2zz__threadz00(obj_t, obj_t, obj_t);
	extern void bgl_sleep(long);
	BGL_EXPORTED_DECL obj_t
		BGl_threadzd2joinz12zc0zz__threadz00(BgL_threadz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_z52userzd2currentzd2threadz52zz__threadz00(BgL_threadz00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_withzd2outputzd2tozd2portzd2zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_defaultzd2threadzd2backendz00zz__threadz00(void);
	extern obj_t BGl_callzd2virtualzd2getterz00zz__objectz00(BgL_objectz00_bglt,
		int);
	extern obj_t bgl_nanoseconds_to_date(BGL_LONGLONG_T);
	static obj_t BGl_z62threadzd2namezb0zz__threadz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31380ze3ze5zz__threadz00(obj_t, obj_t);
	static BgL_nothreadz00_bglt BGl_z62lambda1409z62zz__threadz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2200z00zz__threadz00 = BUNSPEC;
	static obj_t BGl_symbol2204z00zz__threadz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_mutexzd2statezd2zz__threadz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_currentzd2threadzd2zz__threadz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_conditionzd2variablezd2namez00zz__threadz00(obj_t);
	static obj_t
		BGl_threadzd2startz12zd2nothre1234ze70zf5zz__threadz00(BgL_nothreadz00_bglt,
		obj_t);
	static BgL_nothreadz00_bglt BGl_z62lambda1412z62zz__threadz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31381ze3ze5zz__threadz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31454ze3ze5zz__threadz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31357ze3ze5zz__threadz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1418z62zz__threadz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1419z62zz__threadz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62threadzd2cleanupzd2nothr1248z62zz__threadz00(obj_t,
		obj_t);
	static obj_t BGl_symbol2214z00zz__threadz00 = BUNSPEC;
	static obj_t BGl_symbol2216z00zz__threadz00 = BUNSPEC;
	static obj_t BGl_z62threadzd2specificzd2setz121211z70zz__threadz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62lambda1424z62zz__threadz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31447ze3ze5zz__threadz00(obj_t);
	static obj_t BGl_z62lambda1425z62zz__threadz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31528ze3ze5zz__threadz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_threadzd2startz12zc0zz__threadz00(BgL_threadz00_bglt, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__threadz00(void);
	static obj_t BGl_symbol2300z00zz__threadz00 = BUNSPEC;
	static obj_t BGl_symbol2220z00zz__threadz00 = BUNSPEC;
	static obj_t BGl_symbol2301z00zz__threadz00 = BUNSPEC;
	static obj_t BGl_symbol2302z00zz__threadz00 = BUNSPEC;
	static obj_t BGl_symbol2303z00zz__threadz00 = BUNSPEC;
	static obj_t BGl_symbol2304z00zz__threadz00 = BUNSPEC;
	static obj_t BGl_symbol2305z00zz__threadz00 = BUNSPEC;
	static obj_t BGl_symbol2225z00zz__threadz00 = BUNSPEC;
	static obj_t BGl_symbol2306z00zz__threadz00 = BUNSPEC;
	static obj_t BGl_symbol2307z00zz__threadz00 = BUNSPEC;
	static obj_t BGl_z62tbzd2currentzd2thread1179z62zz__threadz00(obj_t, obj_t);
	static obj_t BGl_symbol2308z00zz__threadz00 = BUNSPEC;
	static obj_t BGl_symbol2309z00zz__threadz00 = BUNSPEC;
	extern obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__threadz00(void);
	static BgL_threadz00_bglt BGl_z62tbzd2makezd2threadz62zz__threadz00(obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_mutexzd2unlockz12zc0zz__threadz00(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zz__threadz00(void);
	static obj_t BGl_z62lambda1431z62zz__threadz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1432z62zz__threadz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31391ze3ze5zz__threadz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31464ze3ze5zz__threadz00(obj_t, obj_t);
	static BgL_threadzd2backendzd2_bglt BGl_z62lambda1352z62zz__threadz00(obj_t,
		obj_t);
	static BgL_threadzd2backendzd2_bglt BGl_z62lambda1355z62zz__threadz00(obj_t);
	static obj_t BGl_z62lambda1438z62zz__threadz00(obj_t, obj_t);
	static obj_t BGl_z62mutexzf3z91zz__threadz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1439z62zz__threadz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62threadzd2killz12za2zz__threadz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_threadzd2yieldz12zc0zz__threadz00(void);
	static obj_t BGl_symbol2230z00zz__threadz00 = BUNSPEC;
	static obj_t BGl_z62conditionzd2variablezf3z43zz__threadz00(obj_t, obj_t);
	static obj_t BGl_z62conditionzd2variablezd2broadcastz12z70zz__threadz00(obj_t,
		obj_t);
	static obj_t BGl__makezd2threadzd2zz__threadz00(obj_t, obj_t);
	static obj_t BGl_symbol2235z00zz__threadz00 = BUNSPEC;
	static obj_t BGl_z62threadzd2sleepz12za2zz__threadz00(obj_t, obj_t);
	static obj_t BGl_symbol2157z00zz__threadz00 = BUNSPEC;
	static obj_t BGl_z62lambda1361z62zz__threadz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1362z62zz__threadz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31376ze3ze5zz__threadz00(obj_t);
	static obj_t BGl_z62lambda1445z62zz__threadz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1446z62zz__threadz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_objectz00zz__objectz00;
	static BgL_threadz00_bglt BGl_z62lambda1367z62zz__threadz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_getzd2threadzd2backendz00zz__threadz00(obj_t);
	extern obj_t BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_symbol2240z00zz__threadz00 = BUNSPEC;
	static obj_t BGl_z62dynamiczd2envzf3z43zz__threadz00(obj_t, obj_t);
	static obj_t BGl_z62threadzd2killz121207za2zz__threadz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2164z00zz__threadz00 = BUNSPEC;
	static obj_t BGl_symbol2245z00zz__threadz00 = BUNSPEC;
	static obj_t BGl_symbol2166z00zz__threadz00 = BUNSPEC;
	static obj_t BGl_symbol2249z00zz__threadz00 = BUNSPEC;
	static obj_t BGl_z62lambda1452z62zz__threadz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31393ze3ze5zz__threadz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda1453z62zz__threadz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31385ze3ze5zz__threadz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31369ze3ze5zz__threadz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1374z62zz__threadz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1375z62zz__threadz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_threadzd2initializa7ez12z67zz__threadz00(BgL_threadz00_bglt);
	static obj_t BGl_z62threadzd2specificzd2setz121246z70zz__threadz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62threadzd2cleanup1213zb0zz__threadz00(obj_t, obj_t);
	extern obj_t BGl_displayza2za2zz__r4_output_6_10_3z00(obj_t);
	extern obj_t BGl_classzd2namezd2zz__objectz00(obj_t);
	static obj_t BGl_symbol2175z00zz__threadz00 = BUNSPEC;
	static obj_t BGl_z62threadzd2joinz12zd2nothrea1238z70zz__threadz00(obj_t,
		obj_t, obj_t);
	static BgL_nothreadzd2backendzd2_bglt BGl_z62lambda1460z62zz__threadz00(obj_t,
		obj_t);
	static BgL_nothreadzd2backendzd2_bglt
		BGl_z62lambda1462z62zz__threadz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31386ze3ze5zz__threadz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31548ze3ze5zz__threadz00(obj_t);
	extern BGL_LONGLONG_T bgl_current_nanoseconds(void);
	BGL_EXPORTED_DECL obj_t
		BGl_threadzd2namezd2setz12z12zz__threadz00(BgL_threadz00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_threadz00_bglt BGl_makezd2threadzd2zz__threadz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_threadzd2startzd2joinablez12z12zz__threadz00(BgL_threadz00_bglt);
	static obj_t BGl_symbol2184z00zz__threadz00 = BUNSPEC;
	BGL_EXPORTED_DECL bool_t
		BGl_conditionzd2variablezd2signalz12z12zz__threadz00(obj_t);
	static obj_t BGl_symbol2186z00zz__threadz00 = BUNSPEC;
	static obj_t BGl_z62tbzd2mutexzd2initializa7ez121185zd7zz__threadz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62threadzd2startz121197za2zz__threadz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62threadzd2cleanupzd2setz12z70zz__threadz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	extern obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_bigloozd2typezd2errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2190z00zz__threadz00 = BUNSPEC;
	static obj_t BGl_symbol2191z00zz__threadz00 = BUNSPEC;
	static obj_t BGl_symbol2196z00zz__threadz00 = BUNSPEC;
	static obj_t BGl_z62threadzd2namezd2setz121220z70zz__threadz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62conditionzd2variablezd2nilz62zz__threadz00(obj_t);
	static obj_t BGl_z62threadzd2cleanupzd2setz12zd21250za2zz__threadz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_threadz00_bglt
		BGl_tbzd2makezd2threadz00zz__threadz00(BgL_threadzd2backendzd2_bglt, obj_t,
		obj_t);
	static obj_t BGl_z62mutexzd2statezb0zz__threadz00(obj_t, obj_t);
	static obj_t BGl_z62objectzd2printzd2thread1194z62zz__threadz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62conditionzd2variablezd2signalz12z70zz__threadz00(obj_t,
		obj_t);
	extern long bgl_date_to_seconds(obj_t);
	static BgL_threadz00_bglt
		BGl_z62tbzd2makezd2threadzd2nothr1182zb0zz__threadz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62z52userzd2threadzd2yieldz12z22zz__threadz00(obj_t, obj_t);
	static obj_t BGl_za2nothreadzd2currentza2zd2zz__threadz00 = BUNSPEC;
	static obj_t BGl_symbol2291z00zz__threadz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2conditionzd2variablez00zz__threadz00(obj_t);
	static obj_t BGl_z62defaultzd2threadzd2backendz62zz__threadz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_threadzd2parameterzd2zz__threadz00(obj_t);
	static obj_t BGl_symbol2294z00zz__threadz00 = BUNSPEC;
	static obj_t BGl_symbol2296z00zz__threadz00 = BUNSPEC;
	static obj_t BGl_symbol2297z00zz__threadz00 = BUNSPEC;
	static obj_t BGl_symbol2298z00zz__threadz00 = BUNSPEC;
	static obj_t BGl_symbol2299z00zz__threadz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_threadzd2terminatez12zc0zz__threadz00(BgL_threadz00_bglt);
	static obj_t BGl_za2mutexzd2nilza2zd2zz__threadz00 = BUNSPEC;
	static obj_t BGl_z62withzd2lockzb0zz__threadz00(obj_t, obj_t, obj_t);
	static obj_t BGl__conditionzd2variablezd2waitz12z12zz__threadz00(obj_t,
		obj_t);
	static obj_t BGl__mutexzd2lockz12zc0zz__threadz00(obj_t, obj_t);
	static obj_t
		BGl_zc3z04exitza31544ze3ze70z60zz__threadz00(BgL_nothreadz00_bglt, obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_threadz00zz__threadz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_defaultzd2threadzd2backendzd2setz12zc0zz__threadz00
		(BgL_threadzd2backendzd2_bglt);
	BGL_EXPORTED_DECL bool_t BGl_conditionzd2variablezf3z21zz__threadz00(obj_t);
	static obj_t BGl_z62threadzd2terminatez121204za2zz__threadz00(obj_t, obj_t);
	static obj_t BGl_z62tbzd2condvarzd2initializa71187zc5zz__threadz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tbzd2currentzd2threadz00zz__threadz00(BgL_threadzd2backendzd2_bglt);
	static obj_t BGl_z62threadzd2startzd2joinabl1236z62zz__threadz00(obj_t,
		obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string2201z00zz__threadz00,
		BgL_bgl_string2201za700za7za7_2364za7, "specific", 8);
	      DEFINE_STRING(BGl_string2205z00zz__threadz00,
		BgL_bgl_string2205za700za7za7_2365za7, "cleanup", 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_conditionzd2variablezd2waitz12zd2envzc0zz__threadz00,
		BgL_bgl__conditionza7d2var2366za7, opt_generic_entry,
		BGl__conditionzd2variablezd2waitz12z12zz__threadz00, BFALSE, -1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2202z00zz__threadz00,
		BgL_bgl_za762za7c3za704anonymo2367za7,
		BGl_z62zc3z04anonymousza31386ze3ze5zz__threadz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2203z00zz__threadz00,
		BgL_bgl_za762za7c3za704anonymo2368za7,
		BGl_z62zc3z04anonymousza31385ze3ze5zz__threadz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2206z00zz__threadz00,
		BgL_bgl_za762za7c3za704anonymo2369za7,
		BGl_z62zc3z04anonymousza31391ze3ze5zz__threadz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2207z00zz__threadz00,
		BgL_bgl_za762za7c3za704anonymo2370za7,
		BGl_z62zc3z04anonymousza31393ze3ze5zz__threadz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2208z00zz__threadz00,
		BgL_bgl_za762za7c3za704anonymo2371za7,
		BGl_z62zc3z04anonymousza31400ze3ze5zz__threadz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2209z00zz__threadz00,
		BgL_bgl_za762za7c3za704anonymo2372za7,
		BGl_z62zc3z04anonymousza31402ze3ze5zz__threadz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2215z00zz__threadz00,
		BgL_bgl_string2215za700za7za7_2373za7, "body", 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_currentzd2threadzd2backendzd2setz12zd2envz12zz__threadz00,
		BgL_bgl_za762currentza7d2thr2374z00,
		BGl_z62currentzd2threadzd2backendzd2setz12za2zz__threadz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_defaultzd2threadzd2backendzd2setz12zd2envz12zz__threadz00,
		BgL_bgl_za762defaultza7d2thr2375z00,
		BGl_z62defaultzd2threadzd2backendzd2setz12za2zz__threadz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_conditionzd2variablezf3zd2envzf3zz__threadz00,
		BgL_bgl_za762conditionza7d2v2376z00,
		BGl_z62conditionzd2variablezf3z43zz__threadz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2210z00zz__threadz00,
		BgL_bgl_za762za7c3za704anonymo2377za7,
		BGl_z62zc3z04anonymousza31369ze3ze5zz__threadz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2211z00zz__threadz00,
		BgL_bgl_za762lambda1367za7622378z00, BGl_z62lambda1367z62zz__threadz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2212z00zz__threadz00,
		BgL_bgl_za762lambda1419za7622379z00, BGl_z62lambda1419z62zz__threadz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_threadzd2namezd2envz00zz__threadz00,
		BgL_bgl_za762threadza7d2name2380z00, BGl_z62threadzd2namezb0zz__threadz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2213z00zz__threadz00,
		BgL_bgl_za762lambda1418za7622381z00, BGl_z62lambda1418z62zz__threadz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2221z00zz__threadz00,
		BgL_bgl_string2221za700za7za7_2382za7, "%specific", 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2217z00zz__threadz00,
		BgL_bgl_za762za7c3za704anonymo2383za7,
		BGl_z62zc3z04anonymousza31426ze3ze5zz__threadz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2218z00zz__threadz00,
		BgL_bgl_za762lambda1425za7622384z00, BGl_z62lambda1425z62zz__threadz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2219z00zz__threadz00,
		BgL_bgl_za762lambda1424za7622385z00, BGl_z62lambda1424z62zz__threadz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2226z00zz__threadz00,
		BgL_bgl_string2226za700za7za7_2386za7, "%cleanup", 8);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2spinlockzd2envz00zz__threadz00,
		BgL_bgl__makeza7d2spinlock2387za7, opt_generic_entry,
		BGl__makezd2spinlockzd2zz__threadz00, BFALSE, -1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2222z00zz__threadz00,
		BgL_bgl_za762za7c3za704anonymo2388za7,
		BGl_z62zc3z04anonymousza31433ze3ze5zz__threadz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2223z00zz__threadz00,
		BgL_bgl_za762lambda1432za7622389z00, BGl_z62lambda1432z62zz__threadz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2310z00zz__threadz00,
		BgL_bgl_string2310za700za7za7_2390za7, "&tb-make-thread", 15);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2224z00zz__threadz00,
		BgL_bgl_za762lambda1431za7622391z00, BGl_z62lambda1431z62zz__threadz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2311z00zz__threadz00,
		BgL_bgl_string2311za700za7za7_2392za7, "&tb-current-thread", 18);
	      DEFINE_STRING(BGl_string2150z00zz__threadz00,
		BgL_bgl_string2150za700za7za7_2393za7, "nothread", 8);
	      DEFINE_STRING(BGl_string2231z00zz__threadz00,
		BgL_bgl_string2231za700za7za7_2394za7, "end-result", 10);
	      DEFINE_STRING(BGl_string2312z00zz__threadz00,
		BgL_bgl_string2312za700za7za7_2395za7, "&tb-mutex-initialize!", 21);
	      DEFINE_STRING(BGl_string2313z00zz__threadz00,
		BgL_bgl_string2313za700za7za7_2396za7, "&tb-condvar-initialize!", 23);
	      DEFINE_STRING(BGl_string2151z00zz__threadz00,
		BgL_bgl_string2151za700za7za7_2397za7,
		"/tmp/bigloo/runtime/Llib/thread.scm", 35);
	      DEFINE_STRING(BGl_string2314z00zz__threadz00,
		BgL_bgl_string2314za700za7za7_2398za7, "&thread-initialize!", 19);
	      DEFINE_STRING(BGl_string2152z00zz__threadz00,
		BgL_bgl_string2152za700za7za7_2399za7, "&current-thread-backend-set!", 28);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2227z00zz__threadz00,
		BgL_bgl_za762za7c3za704anonymo2400za7,
		BGl_z62zc3z04anonymousza31440ze3ze5zz__threadz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2315z00zz__threadz00,
		BgL_bgl_string2315za700za7za7_2401za7, "&thread-start!", 14);
	      DEFINE_STRING(BGl_string2153z00zz__threadz00,
		BgL_bgl_string2153za700za7za7_2402za7, "thread-backend", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2228z00zz__threadz00,
		BgL_bgl_za762lambda1439za7622403z00, BGl_z62lambda1439z62zz__threadz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2316z00zz__threadz00,
		BgL_bgl_string2316za700za7za7_2404za7, "&thread-start-joinable!", 23);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_threadzd2cleanupzd2envz00zz__threadz00,
		BgL_bgl_za762threadza7d2clea2405z00,
		BGl_z62threadzd2cleanupzb0zz__threadz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2154z00zz__threadz00,
		BgL_bgl_string2154za700za7za7_2406za7, "&default-thread-backend-set!", 28);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2229z00zz__threadz00,
		BgL_bgl_za762lambda1438za7622407z00, BGl_z62lambda1438z62zz__threadz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2317z00zz__threadz00,
		BgL_bgl_string2317za700za7za7_2408za7, "&thread-join!", 13);
	      DEFINE_STRING(BGl_string2155z00zz__threadz00,
		BgL_bgl_string2155za700za7za7_2409za7, "&get-thread-backend", 19);
	      DEFINE_STRING(BGl_string2236z00zz__threadz00,
		BgL_bgl_string2236za700za7za7_2410za7, "end-exception", 13);
	      DEFINE_STRING(BGl_string2318z00zz__threadz00,
		BgL_bgl_string2318za700za7za7_2411za7, "&thread-terminate!", 18);
	      DEFINE_STRING(BGl_string2156z00zz__threadz00,
		BgL_bgl_string2156za700za7za7_2412za7, "bstring", 7);
	      DEFINE_STRING(BGl_string2319z00zz__threadz00,
		BgL_bgl_string2319za700za7za7_2413za7, "&thread-kill!", 13);
	      DEFINE_STRING(BGl_string2158z00zz__threadz00,
		BgL_bgl_string2158za700za7za7_2414za7, "thread", 6);
	      DEFINE_STRING(BGl_string2159z00zz__threadz00,
		BgL_bgl_string2159za700za7za7_2415za7, "_make-thread", 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2232z00zz__threadz00,
		BgL_bgl_za762za7c3za704anonymo2416za7,
		BGl_z62zc3z04anonymousza31447ze3ze5zz__threadz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2320z00zz__threadz00,
		BgL_bgl_string2320za700za7za7_2417za7, "&thread-specific", 16);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2233z00zz__threadz00,
		BgL_bgl_za762lambda1446za7622418z00, BGl_z62lambda1446z62zz__threadz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2321z00zz__threadz00,
		BgL_bgl_string2321za700za7za7_2419za7, "&thread-specific-set!", 21);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2234z00zz__threadz00,
		BgL_bgl_za762lambda1445za7622420z00, BGl_z62lambda1445z62zz__threadz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2322z00zz__threadz00,
		BgL_bgl_string2322za700za7za7_2421za7, "&thread-cleanup", 15);
	      DEFINE_STRING(BGl_string2160z00zz__threadz00,
		BgL_bgl_string2160za700za7za7_2422za7, "procedure", 9);
	      DEFINE_STRING(BGl_string2241z00zz__threadz00,
		BgL_bgl_string2241za700za7za7_2423za7, "%name", 5);
	      DEFINE_STRING(BGl_string2323z00zz__threadz00,
		BgL_bgl_string2323za700za7za7_2424za7, "&thread-cleanup-set!", 20);
	      DEFINE_STRING(BGl_string2161z00zz__threadz00,
		BgL_bgl_string2161za700za7za7_2425za7, "&thread-parameter", 17);
	      DEFINE_STRING(BGl_string2324z00zz__threadz00,
		BgL_bgl_string2324za700za7za7_2426za7, "&thread-name", 12);
	      DEFINE_STRING(BGl_string2162z00zz__threadz00,
		BgL_bgl_string2162za700za7za7_2427za7, "symbol", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2237z00zz__threadz00,
		BgL_bgl_za762za7c3za704anonymo2428za7,
		BGl_z62zc3z04anonymousza31454ze3ze5zz__threadz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2325z00zz__threadz00,
		BgL_bgl_string2325za700za7za7_2429za7, "&thread-name-set!", 17);
	      DEFINE_STRING(BGl_string2163z00zz__threadz00,
		BgL_bgl_string2163za700za7za7_2430za7, "&thread-parameter-set!", 22);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2238z00zz__threadz00,
		BgL_bgl_za762lambda1453za7622431z00, BGl_z62lambda1453z62zz__threadz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2326z00zz__threadz00,
		BgL_bgl_string2326za700za7za7_2432za7, "&%user-current-thread", 21);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2239z00zz__threadz00,
		BgL_bgl_za762lambda1452za7622433z00, BGl_z62lambda1452z62zz__threadz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2327z00zz__threadz00,
		BgL_bgl_string2327za700za7za7_2434za7, "&%user-thread-sleep!", 20);
	      DEFINE_STRING(BGl_string2165z00zz__threadz00,
		BgL_bgl_string2165za700za7za7_2435za7, "mutex", 5);
	      DEFINE_STRING(BGl_string2328z00zz__threadz00,
		BgL_bgl_string2328za700za7za7_2436za7, "&%user-thread-yield!", 20);
	      DEFINE_STRING(BGl_string2167z00zz__threadz00,
		BgL_bgl_string2167za700za7za7_2437za7, "spinlock", 8);
	      DEFINE_STRING(BGl_string2168z00zz__threadz00,
		BgL_bgl_string2168za700za7za7_2438za7, "&mutex-name", 11);
	      DEFINE_STRING(BGl_string2169z00zz__threadz00,
		BgL_bgl_string2169za700za7za7_2439za7, "_mutex-lock!", 12);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_threadzd2startz12zd2envz12zz__threadz00,
		BgL_bgl_za762threadza7d2star2440z00, va_generic_entry,
		BGl_z62threadzd2startz12za2zz__threadz00, BUNSPEC, -2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2threadzd2envz00zz__threadz00,
		BgL_bgl__makeza7d2threadza7d2441z00, opt_generic_entry,
		BGl__makezd2threadzd2zz__threadz00, BFALSE, -1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2242z00zz__threadz00,
		BgL_bgl_za762za7c3za704anonymo2442za7,
		BGl_z62zc3z04anonymousza31414ze3ze5zz__threadz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2330z00zz__threadz00,
		BgL_bgl_string2330za700za7za7_2443za7, "tb-make-thread", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2243z00zz__threadz00,
		BgL_bgl_za762lambda1412za7622444z00, BGl_z62lambda1412z62zz__threadz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2244z00zz__threadz00,
		BgL_bgl_za762lambda1409za7622445z00, BGl_z62lambda1409z62zz__threadz00, 0L,
		BUNSPEC, 7);
	      DEFINE_STRING(BGl_string2250z00zz__threadz00,
		BgL_bgl_string2250za700za7za7_2446za7, "nothread-backend", 16);
	      DEFINE_STRING(BGl_string2332z00zz__threadz00,
		BgL_bgl_string2332za700za7za7_2447za7, "tb-current-thread", 17);
	      DEFINE_STRING(BGl_string2170z00zz__threadz00,
		BgL_bgl_string2170za700za7za7_2448za7, "bint", 4);
	      DEFINE_STRING(BGl_string2251z00zz__threadz00,
		BgL_bgl_string2251za700za7za7_2449za7, "", 0);
	      DEFINE_STRING(BGl_string2171z00zz__threadz00,
		BgL_bgl_string2171za700za7za7_2450za7, "&mutex-unlock!", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2246z00zz__threadz00,
		BgL_bgl_za762za7c3za704anonymo2451za7,
		BGl_z62zc3z04anonymousza31464ze3ze5zz__threadz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2252z00zz__threadz00,
		BgL_bgl_string2252za700za7za7_2452za7, "bigloo", 6);
	      DEFINE_STRING(BGl_string2334z00zz__threadz00,
		BgL_bgl_string2334za700za7za7_2453za7, "object-display", 14);
	      DEFINE_STRING(BGl_string2172z00zz__threadz00,
		BgL_bgl_string2172za700za7za7_2454za7, "&mutex-state", 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2247z00zz__threadz00,
		BgL_bgl_za762lambda1462za7622455z00, BGl_z62lambda1462z62zz__threadz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2329z00zz__threadz00,
		BgL_bgl_za762tbza7d2makeza7d2t2456za7,
		BGl_z62tbzd2makezd2threadzd2nothr1182zb0zz__threadz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2173z00zz__threadz00,
		BgL_bgl_string2173za700za7za7_2457za7, "&with-lock", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2248z00zz__threadz00,
		BgL_bgl_za762lambda1460za7622458z00, BGl_z62lambda1460z62zz__threadz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2254z00zz__threadz00,
		BgL_bgl_string2254za700za7za7_2459za7, "tb-make-thread1177", 18);
	      DEFINE_STRING(BGl_string2336z00zz__threadz00,
		BgL_bgl_string2336za700za7za7_2460za7, "object-write", 12);
	      DEFINE_STRING(BGl_string2174z00zz__threadz00,
		BgL_bgl_string2174za700za7za7_2461za7, "&with-timed-lock", 16);
	      DEFINE_STRING(BGl_string2256z00zz__threadz00,
		BgL_bgl_string2256za700za7za7_2462za7, "tb-current-thread1179", 21);
	      DEFINE_STRING(BGl_string2338z00zz__threadz00,
		BgL_bgl_string2338za700za7za7_2463za7, "object-print", 12);
	      DEFINE_STRING(BGl_string2176z00zz__threadz00,
		BgL_bgl_string2176za700za7za7_2464za7, "condition-variable", 18);
	      DEFINE_STRING(BGl_string2177z00zz__threadz00,
		BgL_bgl_string2177za700za7za7_2465za7, "&condition-variable-name", 24);
	      DEFINE_STRING(BGl_string2258z00zz__threadz00,
		BgL_bgl_string2258za700za7za7_2466za7, "tb-mutex-initialize!1185", 24);
	      DEFINE_STRING(BGl_string2178z00zz__threadz00,
		BgL_bgl_string2178za700za7za7_2467za7, "condvar", 7);
	      DEFINE_STRING(BGl_string2179z00zz__threadz00,
		BgL_bgl_string2179za700za7za7_2468za7, "_condition-variable-wait!", 25);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2331z00zz__threadz00,
		BgL_bgl_za762tbza7d2currentza72469za7,
		BGl_z62tbzd2currentzd2threadzd2no1184zb0zz__threadz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2333z00zz__threadz00,
		BgL_bgl_za762objectza7d2disp2470z00, va_generic_entry,
		BGl_z62objectzd2displayzd2threa1190z62zz__threadz00, BUNSPEC, -2);
	      DEFINE_STRING(BGl_string2340z00zz__threadz00,
		BgL_bgl_string2340za700za7za7_2471za7, "thread-start!", 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2253z00zz__threadz00,
		BgL_bgl_za762tbza7d2makeza7d2t2472za7,
		BGl_z62tbzd2makezd2thread1177z62zz__threadz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2335z00zz__threadz00,
		BgL_bgl_za762objectza7d2writ2473z00, va_generic_entry,
		BGl_z62objectzd2writezd2thread1192z62zz__threadz00, BUNSPEC, -2);
	      DEFINE_STRING(BGl_string2260z00zz__threadz00,
		BgL_bgl_string2260za700za7za7_2474za7, "tb-condvar-initializ1187", 24);
	      DEFINE_STRING(BGl_string2342z00zz__threadz00,
		BgL_bgl_string2342za700za7za7_2475za7, "thread-start-joinable!", 22);
	      DEFINE_STRING(BGl_string2180z00zz__threadz00,
		BgL_bgl_string2180za700za7za7_2476za7, "&condition-variable-signal!", 27);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2255z00zz__threadz00,
		BgL_bgl_za762tbza7d2currentza72477za7,
		BGl_z62tbzd2currentzd2thread1179z62zz__threadz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2337z00zz__threadz00,
		BgL_bgl_za762objectza7d2prin2478z00,
		BGl_z62objectzd2printzd2thread1194z62zz__threadz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2181z00zz__threadz00,
		BgL_bgl_string2181za700za7za7_2479za7, "&condition-variable-broadcast!",
		30);
	      DEFINE_STRING(BGl_string2262z00zz__threadz00,
		BgL_bgl_string2262za700za7za7_2480za7, "thread-initialize!1195", 22);
	      DEFINE_STRING(BGl_string2344z00zz__threadz00,
		BgL_bgl_string2344za700za7za7_2481za7, "thread-join!", 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2257z00zz__threadz00,
		BgL_bgl_za762tbza7d2mutexza7d22482za7,
		BGl_z62tbzd2mutexzd2initializa7ez121185zd7zz__threadz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2339z00zz__threadz00,
		BgL_bgl_za762threadza7d2star2483z00, va_generic_entry,
		BGl_z62threadzd2startz12zd2nothre1234z70zz__threadz00, BUNSPEC, -2);
	extern obj_t BGl_objectzd2writezd2envz00zz__objectz00;
	   
		 
		DEFINE_STRING(BGl_string2264z00zz__threadz00,
		BgL_bgl_string2264za700za7za7_2484za7, "thread-start!1197", 17);
	      DEFINE_STRING(BGl_string2346z00zz__threadz00,
		BgL_bgl_string2346za700za7za7_2485za7, "thread-terminate!", 17);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2259z00zz__threadz00,
		BgL_bgl_za762tbza7d2condvarza72486za7,
		BGl_z62tbzd2condvarzd2initializa71187zc5zz__threadz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2185z00zz__threadz00,
		BgL_bgl_string2185za700za7za7_2487za7, "name", 4);
	      DEFINE_STRING(BGl_string2266z00zz__threadz00,
		BgL_bgl_string2266za700za7za7_2488za7, "thread-start-joinabl1199", 24);
	      DEFINE_STRING(BGl_string2348z00zz__threadz00,
		BgL_bgl_string2348za700za7za7_2489za7, "thread-kill!", 12);
	      DEFINE_STRING(BGl_string2268z00zz__threadz00,
		BgL_bgl_string2268za700za7za7_2490za7, "thread-join!1202", 16);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2341z00zz__threadz00,
		BgL_bgl_za762threadza7d2star2491z00,
		BGl_z62threadzd2startzd2joinabl1236z62zz__threadz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2261z00zz__threadz00,
		BgL_bgl_za762threadza7d2init2492z00,
		BGl_z62threadzd2initializa7ez121195z05zz__threadz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2343z00zz__threadz00,
		BgL_bgl_za762threadza7d2join2493z00, va_generic_entry,
		BGl_z62threadzd2joinz12zd2nothrea1238z70zz__threadz00, BUNSPEC, -2);
	      DEFINE_STRING(BGl_string2350z00zz__threadz00,
		BgL_bgl_string2350za700za7za7_2494za7, "thread-specific", 15);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2182z00zz__threadz00,
		BgL_bgl_za762lambda1362za7622495z00, BGl_z62lambda1362z62zz__threadz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2263z00zz__threadz00,
		BgL_bgl_za762threadza7d2star2496z00, va_generic_entry,
		BGl_z62threadzd2startz121197za2zz__threadz00, BUNSPEC, -2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2345z00zz__threadz00,
		BgL_bgl_za762threadza7d2term2497z00,
		BGl_z62threadzd2terminatez12zd2no1240z70zz__threadz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2183z00zz__threadz00,
		BgL_bgl_za762lambda1361za7622498z00, BGl_z62lambda1361z62zz__threadz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2270z00zz__threadz00,
		BgL_bgl_string2270za700za7za7_2499za7, "thread-terminate!1204", 21);
	      DEFINE_STRING(BGl_string2352z00zz__threadz00,
		BgL_bgl_string2352za700za7za7_2500za7, "thread-specific-set!", 20);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2265z00zz__threadz00,
		BgL_bgl_za762threadza7d2star2501z00,
		BGl_z62threadzd2startzd2joinabl1199z62zz__threadz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2347z00zz__threadz00,
		BgL_bgl_za762threadza7d2kill2502z00,
		BGl_z62threadzd2killz12zd2nothrea1242z70zz__threadz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2272z00zz__threadz00,
		BgL_bgl_string2272za700za7za7_2503za7, "thread-kill!1207", 16);
	      DEFINE_STRING(BGl_string2354z00zz__threadz00,
		BgL_bgl_string2354za700za7za7_2504za7, "thread-cleanup", 14);
	      DEFINE_STRING(BGl_string2192z00zz__threadz00,
		BgL_bgl_string2192za700za7za7_2505za7, "__thread", 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2267z00zz__threadz00,
		BgL_bgl_za762threadza7d2join2506z00, va_generic_entry,
		BGl_z62threadzd2joinz121202za2zz__threadz00, BUNSPEC, -2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2349z00zz__threadz00,
		BgL_bgl_za762threadza7d2spec2507z00,
		BGl_z62threadzd2specificzd2noth1244z62zz__threadz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2187z00zz__threadz00,
		BgL_bgl_za762za7c3za704anonymo2508za7,
		BGl_z62zc3z04anonymousza31357ze3ze5zz__threadz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2274z00zz__threadz00,
		BgL_bgl_string2274za700za7za7_2509za7, "thread-specific1209", 19);
	      DEFINE_STRING(BGl_string2356z00zz__threadz00,
		BgL_bgl_string2356za700za7za7_2510za7, "thread-cleanup-set!", 19);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2188z00zz__threadz00,
		BgL_bgl_za762lambda1355za7622511z00, BGl_z62lambda1355z62zz__threadz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2269z00zz__threadz00,
		BgL_bgl_za762threadza7d2term2512z00,
		BGl_z62threadzd2terminatez121204za2zz__threadz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2189z00zz__threadz00,
		BgL_bgl_za762lambda1352za7622513z00, BGl_z62lambda1352z62zz__threadz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2276z00zz__threadz00,
		BgL_bgl_string2276za700za7za7_2514za7, "thread-specific-set!1211", 24);
	      DEFINE_STRING(BGl_string2358z00zz__threadz00,
		BgL_bgl_string2358za700za7za7_2515za7, "thread-name", 11);
	      DEFINE_STRING(BGl_string2197z00zz__threadz00,
		BgL_bgl_string2197za700za7za7_2516za7, "obj", 3);
	      DEFINE_STRING(BGl_string2278z00zz__threadz00,
		BgL_bgl_string2278za700za7za7_2517za7, "thread-cleanup1213", 18);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2351z00zz__threadz00,
		BgL_bgl_za762threadza7d2spec2518z00,
		BGl_z62threadzd2specificzd2setz121246z70zz__threadz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_mutexzd2lockz12zd2envz12zz__threadz00,
		BgL_bgl__mutexza7d2lockza7122519z00, opt_generic_entry,
		BGl__mutexzd2lockz12zc0zz__threadz00, BFALSE, -1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2271z00zz__threadz00,
		BgL_bgl_za762threadza7d2kill2520z00,
		BGl_z62threadzd2killz121207za2zz__threadz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2353z00zz__threadz00,
		BgL_bgl_za762threadza7d2clea2521z00,
		BGl_z62threadzd2cleanupzd2nothr1248z62zz__threadz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2360z00zz__threadz00,
		BgL_bgl_string2360za700za7za7_2522za7, "thread-name-set!", 16);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2273z00zz__threadz00,
		BgL_bgl_za762threadza7d2spec2523z00,
		BGl_z62threadzd2specific1209zb0zz__threadz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2361z00zz__threadz00,
		BgL_bgl_string2361za700za7za7_2524za7, ">", 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2355z00zz__threadz00,
		BgL_bgl_za762threadza7d2clea2525z00,
		BGl_z62threadzd2cleanupzd2setz12zd21250za2zz__threadz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2193z00zz__threadz00,
		BgL_bgl_za762za7c3za704anonymo2526za7,
		BGl_z62zc3z04anonymousza31376ze3ze5zz__threadz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2280z00zz__threadz00,
		BgL_bgl_string2280za700za7za7_2527za7, "thread-cleanup-set!1215", 23);
	      DEFINE_STRING(BGl_string2362z00zz__threadz00,
		BgL_bgl_string2362za700za7za7_2528za7, ":", 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2194z00zz__threadz00,
		BgL_bgl_za762lambda1375za7622529z00, BGl_z62lambda1375z62zz__threadz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2275z00zz__threadz00,
		BgL_bgl_za762threadza7d2spec2530z00,
		BGl_z62threadzd2specificzd2setz121211z70zz__threadz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2363z00zz__threadz00,
		BgL_bgl_string2363za700za7za7_2531za7, "#<", 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2357z00zz__threadz00,
		BgL_bgl_za762threadza7d2name2532z00,
		BGl_z62threadzd2namezd2nothread1252z62zz__threadz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2195z00zz__threadz00,
		BgL_bgl_za762lambda1374za7622533z00, BGl_z62lambda1374z62zz__threadz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2282z00zz__threadz00,
		BgL_bgl_string2282za700za7za7_2534za7, "thread-name1218", 15);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2277z00zz__threadz00,
		BgL_bgl_za762threadza7d2clea2535z00,
		BGl_z62threadzd2cleanup1213zb0zz__threadz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2359z00zz__threadz00,
		BgL_bgl_za762threadza7d2name2536z00,
		BGl_z62threadzd2namezd2setz12zd2not1254za2zz__threadz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2284z00zz__threadz00,
		BgL_bgl_string2284za700za7za7_2537za7, "thread-name-set!1220", 20);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_withzd2lockzd2envz00zz__threadz00,
		BgL_bgl_za762withza7d2lockza7b2538za7, BGl_z62withzd2lockzb0zz__threadz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2198z00zz__threadz00,
		BgL_bgl_za762za7c3za704anonymo2539za7,
		BGl_z62zc3z04anonymousza31381ze3ze5zz__threadz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2279z00zz__threadz00,
		BgL_bgl_za762threadza7d2clea2540z00,
		BGl_z62threadzd2cleanupzd2setz121215z70zz__threadz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2199z00zz__threadz00,
		BgL_bgl_za762za7c3za704anonymo2541za7,
		BGl_z62zc3z04anonymousza31380ze3ze5zz__threadz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2286z00zz__threadz00,
		BgL_bgl_string2286za700za7za7_2542za7, "%user-current-thread1227", 24);
	      DEFINE_STRING(BGl_string2288z00zz__threadz00,
		BgL_bgl_string2288za700za7za7_2543za7, "%user-thread-sleep!1229", 23);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_mutexzd2namezd2envz00zz__threadz00,
		BgL_bgl_za762mutexza7d2nameza72544za7, BGl_z62mutexzd2namezb0zz__threadz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2281z00zz__threadz00,
		BgL_bgl_za762threadza7d2name2545z00,
		BGl_z62threadzd2name1218zb0zz__threadz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_mutexzd2unlockz12zd2envz12zz__threadz00,
		BgL_bgl_za762mutexza7d2unloc2546z00,
		BGl_z62mutexzd2unlockz12za2zz__threadz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2283z00zz__threadz00,
		BgL_bgl_za762threadza7d2name2547z00,
		BGl_z62threadzd2namezd2setz121220z70zz__threadz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2290z00zz__threadz00,
		BgL_bgl_string2290za700za7za7_2548za7, "%user-thread-yield!1231", 23);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2285z00zz__threadz00,
		BgL_bgl_za762za752userza7d2cur2549za7,
		BGl_z62z52userzd2currentzd2thread1227z30zz__threadz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2292z00zz__threadz00,
		BgL_bgl_string2292za700za7za7_2550za7, "thread-sleep!", 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2287z00zz__threadz00,
		BgL_bgl_za762za752userza7d2thr2551za7,
		BGl_z62z52userzd2threadzd2sleepz121229z22zz__threadz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2293z00zz__threadz00,
		BgL_bgl_string2293za700za7za7_2552za7, "date, real, or integer", 22);
	extern obj_t BGl_conszd2envzd2zz__r4_pairs_and_lists_6_3z00;
	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2289z00zz__threadz00,
		BgL_bgl_za762za752userza7d2thr2553za7,
		BGl_z62z52userzd2threadzd2yieldz121231z22zz__threadz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2295z00zz__threadz00,
		BgL_bgl_string2295za700za7za7_2554za7, "No method for this object", 25);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_threadzd2joinz12zd2envz12zz__threadz00,
		BgL_bgl_za762threadza7d2join2555z00, va_generic_entry,
		BGl_z62threadzd2joinz12za2zz__threadz00, BUNSPEC, -2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_threadzd2yieldz12zd2envz12zz__threadz00,
		BgL_bgl_za762threadza7d2yiel2556z00,
		BGl_z62threadzd2yieldz12za2zz__threadz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_GENERIC(BGl_tbzd2currentzd2threadzd2envzd2zz__threadz00,
		BgL_bgl_za762tbza7d2currentza72557za7,
		BGl_z62tbzd2currentzd2threadz62zz__threadz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_threadzd2parameterzd2setz12zd2envzc0zz__threadz00,
		BgL_bgl_za762threadza7d2para2558z00,
		BGl_z62threadzd2parameterzd2setz12z70zz__threadz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_withzd2timedzd2lockzd2envzd2zz__threadz00,
		BgL_bgl_za762withza7d2timedza72559za7,
		BGl_z62withzd2timedzd2lockz62zz__threadz00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_threadzd2specificzd2envz00zz__threadz00,
		BgL_bgl_za762threadza7d2spec2560z00,
		BGl_z62threadzd2specificzb0zz__threadz00, 0L, BUNSPEC, 1);
	extern obj_t BGl_objectzd2printzd2envz00zz__objectz00;
	extern obj_t BGl_objectzd2displayzd2envz00zz__objectz00;
	   
		 
		DEFINE_EXPORT_BGL_GENERIC
		(BGl_threadzd2startzd2joinablez12zd2envzc0zz__threadz00,
		BgL_bgl_za762threadza7d2star2561z00,
		BGl_z62threadzd2startzd2joinablez12z70zz__threadz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_conditionzd2variablezd2broadcastz12zd2envzc0zz__threadz00,
		BgL_bgl_za762conditionza7d2v2562z00,
		BGl_z62conditionzd2variablezd2broadcastz12z70zz__threadz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_defaultzd2threadzd2backendzd2envzd2zz__threadz00,
		BgL_bgl_za762defaultza7d2thr2563z00,
		BGl_z62defaultzd2threadzd2backendz62zz__threadz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_GENERIC
		(BGl_z52userzd2threadzd2sleepz12zd2envz92zz__threadz00,
		BgL_bgl_za762za752userza7d2thr2564za7,
		BGl_z62z52userzd2threadzd2sleepz12z22zz__threadz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_threadzd2killz12zd2envz12zz__threadz00,
		BgL_bgl_za762threadza7d2kill2565z00,
		BGl_z62threadzd2killz12za2zz__threadz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2conditionzd2variablezd2envzd2zz__threadz00,
		BgL_bgl__makeza7d2conditio2566za7, opt_generic_entry,
		BGl__makezd2conditionzd2variablez00zz__threadz00, BFALSE, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_dynamiczd2envzf3zd2envzf3zz__threadz00,
		BgL_bgl_za762dynamicza7d2env2567z00,
		BGl_z62dynamiczd2envzf3z43zz__threadz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_conditionzd2variablezd2signalz12zd2envzc0zz__threadz00,
		BgL_bgl_za762conditionza7d2v2568z00,
		BGl_z62conditionzd2variablezd2signalz12z70zz__threadz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_tbzd2makezd2threadzd2envzd2zz__threadz00,
		BgL_bgl_za762tbza7d2makeza7d2t2569za7,
		BGl_z62tbzd2makezd2threadz62zz__threadz00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_currentzd2threadzd2envz00zz__threadz00,
		BgL_bgl_za762currentza7d2thr2570z00,
		BGl_z62currentzd2threadzb0zz__threadz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_conditionzd2variablezd2namezd2envzd2zz__threadz00,
		BgL_bgl_za762conditionza7d2v2571z00,
		BGl_z62conditionzd2variablezd2namez62zz__threadz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_mutexzf3zd2envz21zz__threadz00,
		BgL_bgl_za762mutexza7f3za791za7za72572za7, BGl_z62mutexzf3z91zz__threadz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_threadzd2parameterzd2envz00zz__threadz00,
		BgL_bgl_za762threadza7d2para2573z00,
		BGl_z62threadzd2parameterzb0zz__threadz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_mutexzd2nilzd2envz00zz__threadz00,
		BgL_bgl_za762mutexza7d2nilza7b2574za7, BGl_z62mutexzd2nilzb0zz__threadz00,
		0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_GENERIC
		(BGl_z52userzd2currentzd2threadzd2envz80zz__threadz00,
		BgL_bgl_za762za752userza7d2cur2575za7,
		BGl_z62z52userzd2currentzd2threadz30zz__threadz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_mutexzd2statezd2envz00zz__threadz00,
		BgL_bgl_za762mutexza7d2state2576z00, BGl_z62mutexzd2statezb0zz__threadz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_GENERIC
		(BGl_threadzd2initializa7ez12zd2envzb5zz__threadz00,
		BgL_bgl_za762threadza7d2init2577z00,
		BGl_z62threadzd2initializa7ez12z05zz__threadz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_currentzd2threadzd2backendzd2envzd2zz__threadz00,
		BgL_bgl_za762currentza7d2thr2578z00,
		BGl_z62currentzd2threadzd2backendz62zz__threadz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_conditionzd2variablezd2nilzd2envzd2zz__threadz00,
		BgL_bgl_za762conditionza7d2v2579z00,
		BGl_z62conditionzd2variablezd2nilz62zz__threadz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_currentzd2dynamiczd2envzd2envzd2zz__threadz00,
		BgL_bgl_za762currentza7d2dyn2580z00,
		BGl_z62currentzd2dynamiczd2envz62zz__threadz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_GENERIC(BGl_threadzd2terminatez12zd2envz12zz__threadz00,
		BgL_bgl_za762threadza7d2term2581z00,
		BGl_z62threadzd2terminatez12za2zz__threadz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_GENERIC(BGl_threadzd2namezd2setz12zd2envzc0zz__threadz00,
		BgL_bgl_za762threadza7d2name2582z00,
		BGl_z62threadzd2namezd2setz12z70zz__threadz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2mutexzd2envz00zz__threadz00,
		BgL_bgl__makeza7d2mutexza7d22583z00, opt_generic_entry,
		BGl__makezd2mutexzd2zz__threadz00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_GENERIC
		(BGl_z52userzd2threadzd2yieldz12zd2envz92zz__threadz00,
		BgL_bgl_za762za752userza7d2thr2584za7,
		BGl_z62z52userzd2threadzd2yieldz12z22zz__threadz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_GENERIC
		(BGl_tbzd2mutexzd2initializa7ez12zd2envz67zz__threadz00,
		BgL_bgl_za762tbza7d2mutexza7d22585za7,
		BGl_z62tbzd2mutexzd2initializa7ez12zd7zz__threadz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_GENERIC
		(BGl_threadzd2specificzd2setz12zd2envzc0zz__threadz00,
		BgL_bgl_za762threadza7d2spec2586z00,
		BGl_z62threadzd2specificzd2setz12z70zz__threadz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_GENERIC
		(BGl_threadzd2cleanupzd2setz12zd2envzc0zz__threadz00,
		BgL_bgl_za762threadza7d2clea2587z00,
		BGl_z62threadzd2cleanupzd2setz12z70zz__threadz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_threadzd2sleepz12zd2envz12zz__threadz00,
		BgL_bgl_za762threadza7d2slee2588z00,
		BGl_z62threadzd2sleepz12za2zz__threadz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2threadzd2backendzd2envzd2zz__threadz00,
		BgL_bgl_za762getza7d2threadza72589za7,
		BGl_z62getzd2threadzd2backendz62zz__threadz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_GENERIC
		(BGl_tbzd2condvarzd2initializa7ez12zd2envz67zz__threadz00,
		BgL_bgl_za762tbza7d2condvarza72590za7,
		BGl_z62tbzd2condvarzd2initializa7ez12zd7zz__threadz00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__threadz00));
		     ADD_ROOT((void *) (&BGl_nothreadzd2backendzd2zz__threadz00));
		     ADD_ROOT((void *) (&BGl_za2nothreadzd2backendza2zd2zz__threadz00));
		     ADD_ROOT((void *) (&BGl_nothreadz00zz__threadz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2conditionzd2variablezd2nilza2z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_threadzd2backendzd2zz__threadz00));
		     ADD_ROOT((void *) (&BGl_za2threadzd2backendsza2zd2zz__threadz00));
		     ADD_ROOT((void *) (&BGl_symbol2200z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_symbol2204z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_symbol2214z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_symbol2216z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_symbol2300z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_symbol2220z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_symbol2301z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_symbol2302z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_symbol2303z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_symbol2304z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_symbol2305z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_symbol2225z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_symbol2306z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_symbol2307z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_symbol2308z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_symbol2309z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_symbol2230z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_symbol2235z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_symbol2157z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_symbol2240z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_symbol2164z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_symbol2245z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_symbol2166z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_symbol2249z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_symbol2175z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_symbol2184z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_symbol2186z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_symbol2190z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_symbol2191z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_symbol2196z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_za2nothreadzd2currentza2zd2zz__threadz00));
		     ADD_ROOT((void *) (&BGl_symbol2291z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_symbol2294z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_symbol2296z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_symbol2297z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_symbol2298z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_symbol2299z00zz__threadz00));
		     ADD_ROOT((void *) (&BGl_za2mutexzd2nilza2zd2zz__threadz00));
		     ADD_ROOT((void *) (&BGl_threadz00zz__threadz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long
		BgL_checksumz00_4965, char *BgL_fromz00_4966)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__threadz00))
				{
					BGl_requirezd2initializa7ationz75zz__threadz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__threadz00();
					BGl_cnstzd2initzd2zz__threadz00();
					BGl_importedzd2moduleszd2initz00zz__threadz00();
					BGl_objectzd2initzd2zz__threadz00();
					BGl_genericzd2initzd2zz__threadz00();
					BGl_methodzd2initzd2zz__threadz00();
					return BGl_toplevelzd2initzd2zz__threadz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__threadz00(void)
	{
		{	/* Llib/thread.scm 17 */
			BGl_symbol2157z00zz__threadz00 =
				bstring_to_symbol(BGl_string2158z00zz__threadz00);
			BGl_symbol2164z00zz__threadz00 =
				bstring_to_symbol(BGl_string2165z00zz__threadz00);
			BGl_symbol2166z00zz__threadz00 =
				bstring_to_symbol(BGl_string2167z00zz__threadz00);
			BGl_symbol2175z00zz__threadz00 =
				bstring_to_symbol(BGl_string2176z00zz__threadz00);
			BGl_symbol2184z00zz__threadz00 =
				bstring_to_symbol(BGl_string2185z00zz__threadz00);
			BGl_symbol2186z00zz__threadz00 =
				bstring_to_symbol(BGl_string2156z00zz__threadz00);
			BGl_symbol2190z00zz__threadz00 =
				bstring_to_symbol(BGl_string2153z00zz__threadz00);
			BGl_symbol2191z00zz__threadz00 =
				bstring_to_symbol(BGl_string2192z00zz__threadz00);
			BGl_symbol2196z00zz__threadz00 =
				bstring_to_symbol(BGl_string2197z00zz__threadz00);
			BGl_symbol2200z00zz__threadz00 =
				bstring_to_symbol(BGl_string2201z00zz__threadz00);
			BGl_symbol2204z00zz__threadz00 =
				bstring_to_symbol(BGl_string2205z00zz__threadz00);
			BGl_symbol2214z00zz__threadz00 =
				bstring_to_symbol(BGl_string2215z00zz__threadz00);
			BGl_symbol2216z00zz__threadz00 =
				bstring_to_symbol(BGl_string2160z00zz__threadz00);
			BGl_symbol2220z00zz__threadz00 =
				bstring_to_symbol(BGl_string2221z00zz__threadz00);
			BGl_symbol2225z00zz__threadz00 =
				bstring_to_symbol(BGl_string2226z00zz__threadz00);
			BGl_symbol2230z00zz__threadz00 =
				bstring_to_symbol(BGl_string2231z00zz__threadz00);
			BGl_symbol2235z00zz__threadz00 =
				bstring_to_symbol(BGl_string2236z00zz__threadz00);
			BGl_symbol2240z00zz__threadz00 =
				bstring_to_symbol(BGl_string2241z00zz__threadz00);
			BGl_symbol2245z00zz__threadz00 =
				bstring_to_symbol(BGl_string2150z00zz__threadz00);
			BGl_symbol2249z00zz__threadz00 =
				bstring_to_symbol(BGl_string2250z00zz__threadz00);
			BGl_symbol2291z00zz__threadz00 =
				bstring_to_symbol(BGl_string2292z00zz__threadz00);
			BGl_symbol2294z00zz__threadz00 =
				bstring_to_symbol(BGl_string2284z00zz__threadz00);
			BGl_symbol2296z00zz__threadz00 =
				bstring_to_symbol(BGl_string2282z00zz__threadz00);
			BGl_symbol2297z00zz__threadz00 =
				bstring_to_symbol(BGl_string2280z00zz__threadz00);
			BGl_symbol2298z00zz__threadz00 =
				bstring_to_symbol(BGl_string2278z00zz__threadz00);
			BGl_symbol2299z00zz__threadz00 =
				bstring_to_symbol(BGl_string2276z00zz__threadz00);
			BGl_symbol2300z00zz__threadz00 =
				bstring_to_symbol(BGl_string2274z00zz__threadz00);
			BGl_symbol2301z00zz__threadz00 =
				bstring_to_symbol(BGl_string2272z00zz__threadz00);
			BGl_symbol2302z00zz__threadz00 =
				bstring_to_symbol(BGl_string2270z00zz__threadz00);
			BGl_symbol2303z00zz__threadz00 =
				bstring_to_symbol(BGl_string2268z00zz__threadz00);
			BGl_symbol2304z00zz__threadz00 =
				bstring_to_symbol(BGl_string2266z00zz__threadz00);
			BGl_symbol2305z00zz__threadz00 =
				bstring_to_symbol(BGl_string2264z00zz__threadz00);
			BGl_symbol2306z00zz__threadz00 =
				bstring_to_symbol(BGl_string2260z00zz__threadz00);
			BGl_symbol2307z00zz__threadz00 =
				bstring_to_symbol(BGl_string2258z00zz__threadz00);
			BGl_symbol2308z00zz__threadz00 =
				bstring_to_symbol(BGl_string2256z00zz__threadz00);
			return (BGl_symbol2309z00zz__threadz00 =
				bstring_to_symbol(BGl_string2254z00zz__threadz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__threadz00(void)
	{
		{	/* Llib/thread.scm 17 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__threadz00(void)
	{
		{	/* Llib/thread.scm 17 */
			{	/* Llib/thread.scm 272 */
				BgL_nothreadzd2backendzd2_bglt BgL_new1058z00_1223;

				{	/* Llib/thread.scm 273 */
					BgL_nothreadzd2backendzd2_bglt BgL_new1057z00_1224;

					BgL_new1057z00_1224 =
						((BgL_nothreadzd2backendzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_nothreadzd2backendzd2_bgl))));
					{	/* Llib/thread.scm 273 */
						long BgL_arg1316z00_1225;

						BgL_arg1316z00_1225 =
							BGL_CLASS_NUM(BGl_nothreadzd2backendzd2zz__threadz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1057z00_1224), BgL_arg1316z00_1225);
					}
					BgL_new1058z00_1223 = BgL_new1057z00_1224;
				}
				((((BgL_threadzd2backendzd2_bglt) COBJECT(
								((BgL_threadzd2backendzd2_bglt) BgL_new1058z00_1223)))->
						BgL_namez00) = ((obj_t) BGl_string2150z00zz__threadz00), BUNSPEC);
				BGl_za2nothreadzd2backendza2zd2zz__threadz00 = BgL_new1058z00_1223;
			}
			{	/* Llib/thread.scm 279 */
				obj_t BgL_list1317z00_1226;

				BgL_list1317z00_1226 =
					MAKE_YOUNG_PAIR(
					((obj_t) BGl_za2nothreadzd2backendza2zd2zz__threadz00), BNIL);
				BGl_za2threadzd2backendsza2zd2zz__threadz00 = BgL_list1317z00_1226;
			}
			BGl_za2nothreadzd2currentza2zd2zz__threadz00 = BFALSE;
			BGl_za2mutexzd2nilza2zd2zz__threadz00 = bgl_make_nil_mutex();
			return (BGl_za2conditionzd2variablezd2nilza2z00zz__threadz00 =
				bgl_make_nil_condvar(), BUNSPEC);
		}

	}



/* dynamic-env? */
	BGL_EXPORTED_DEF bool_t BGl_dynamiczd2envzf3z21zz__threadz00(obj_t
		BgL_objz00_3)
	{
		{	/* Llib/thread.scm 259 */
			return BGL_DYNAMIC_ENVP(BgL_objz00_3);
		}

	}



/* &dynamic-env? */
	obj_t BGl_z62dynamiczd2envzf3z43zz__threadz00(obj_t BgL_envz00_3455,
		obj_t BgL_objz00_3456)
	{
		{	/* Llib/thread.scm 259 */
			return BBOOL(BGl_dynamiczd2envzf3z21zz__threadz00(BgL_objz00_3456));
		}

	}



/* current-dynamic-env */
	BGL_EXPORTED_DEF obj_t BGl_currentzd2dynamiczd2envz00zz__threadz00(void)
	{
		{	/* Llib/thread.scm 265 */
			return BGL_CURRENT_DYNAMIC_ENV();
		}

	}



/* &current-dynamic-env */
	obj_t BGl_z62currentzd2dynamiczd2envz62zz__threadz00(obj_t BgL_envz00_3457)
	{
		{	/* Llib/thread.scm 265 */
			return BGl_currentzd2dynamiczd2envz00zz__threadz00();
		}

	}



/* current-thread-backend */
	BGL_EXPORTED_DEF obj_t BGl_currentzd2threadzd2backendz00zz__threadz00(void)
	{
		{	/* Llib/thread.scm 284 */
			return BGL_THREAD_BACKEND();
		}

	}



/* &current-thread-backend */
	obj_t BGl_z62currentzd2threadzd2backendz62zz__threadz00(obj_t BgL_envz00_3458)
	{
		{	/* Llib/thread.scm 284 */
			return BGl_currentzd2threadzd2backendz00zz__threadz00();
		}

	}



/* current-thread-backend-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_currentzd2threadzd2backendzd2setz12zc0zz__threadz00
		(BgL_threadzd2backendzd2_bglt BgL_tbz00_4)
	{
		{	/* Llib/thread.scm 290 */
			{	/* Llib/thread.scm 291 */
				obj_t BgL_tmpz00_5031;

				BgL_tmpz00_5031 = ((obj_t) BgL_tbz00_4);
				BGL_THREAD_BACKEND_SET(BgL_tmpz00_5031);
			}
			BUNSPEC;
			return ((obj_t) BgL_tbz00_4);
		}

	}



/* &current-thread-backend-set! */
	obj_t BGl_z62currentzd2threadzd2backendzd2setz12za2zz__threadz00(obj_t
		BgL_envz00_3459, obj_t BgL_tbz00_3460)
	{
		{	/* Llib/thread.scm 290 */
			{	/* Llib/thread.scm 291 */
				BgL_threadzd2backendzd2_bglt BgL_auxz00_5035;

				if (BGl_isazf3zf3zz__objectz00(BgL_tbz00_3460,
						BGl_threadzd2backendzd2zz__threadz00))
					{	/* Llib/thread.scm 291 */
						BgL_auxz00_5035 = ((BgL_threadzd2backendzd2_bglt) BgL_tbz00_3460);
					}
				else
					{
						obj_t BgL_auxz00_5039;

						BgL_auxz00_5039 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
							BINT(11215L), BGl_string2152z00zz__threadz00,
							BGl_string2153z00zz__threadz00, BgL_tbz00_3460);
						FAILURE(BgL_auxz00_5039, BFALSE, BFALSE);
					}
				return
					BGl_currentzd2threadzd2backendzd2setz12zc0zz__threadz00
					(BgL_auxz00_5035);
			}
		}

	}



/* default-thread-backend */
	BGL_EXPORTED_DEF obj_t BGl_defaultzd2threadzd2backendz00zz__threadz00(void)
	{
		{	/* Llib/thread.scm 297 */
			return CAR(BGl_za2threadzd2backendsza2zd2zz__threadz00);
		}

	}



/* &default-thread-backend */
	obj_t BGl_z62defaultzd2threadzd2backendz62zz__threadz00(obj_t BgL_envz00_3461)
	{
		{	/* Llib/thread.scm 297 */
			return BGl_defaultzd2threadzd2backendz00zz__threadz00();
		}

	}



/* default-thread-backend-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_defaultzd2threadzd2backendzd2setz12zc0zz__threadz00
		(BgL_threadzd2backendzd2_bglt BgL_tbz00_5)
	{
		{	/* Llib/thread.scm 303 */
			BGl_za2threadzd2backendsza2zd2zz__threadz00 =
				bgl_remq_bang(
				((obj_t) BgL_tbz00_5), BGl_za2threadzd2backendsza2zd2zz__threadz00);
			return (BGl_za2threadzd2backendsza2zd2zz__threadz00 =
				MAKE_YOUNG_PAIR(
					((obj_t) BgL_tbz00_5), BGl_za2threadzd2backendsza2zd2zz__threadz00),
				BUNSPEC);
		}

	}



/* &default-thread-backend-set! */
	obj_t BGl_z62defaultzd2threadzd2backendzd2setz12za2zz__threadz00(obj_t
		BgL_envz00_3462, obj_t BgL_tbz00_3463)
	{
		{	/* Llib/thread.scm 303 */
			{	/* Llib/thread.scm 304 */
				BgL_threadzd2backendzd2_bglt BgL_auxz00_5050;

				if (BGl_isazf3zf3zz__objectz00(BgL_tbz00_3463,
						BGl_threadzd2backendzd2zz__threadz00))
					{	/* Llib/thread.scm 304 */
						BgL_auxz00_5050 = ((BgL_threadzd2backendzd2_bglt) BgL_tbz00_3463);
					}
				else
					{
						obj_t BgL_auxz00_5054;

						BgL_auxz00_5054 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
							BINT(11833L), BGl_string2154z00zz__threadz00,
							BGl_string2153z00zz__threadz00, BgL_tbz00_3463);
						FAILURE(BgL_auxz00_5054, BFALSE, BFALSE);
					}
				return
					BGl_defaultzd2threadzd2backendzd2setz12zc0zz__threadz00
					(BgL_auxz00_5050);
			}
		}

	}



/* get-thread-backend */
	BGL_EXPORTED_DEF obj_t BGl_getzd2threadzd2backendz00zz__threadz00(obj_t
		BgL_namez00_6)
	{
		{	/* Llib/thread.scm 310 */
			{
				obj_t BgL_tbsz00_1236;

				BgL_tbsz00_1236 = BGl_za2threadzd2backendsza2zd2zz__threadz00;
			BgL_zc3z04anonymousza31321ze3z87_1237:
				if (PAIRP(BgL_tbsz00_1236))
					{	/* Llib/thread.scm 313 */
						BgL_threadzd2backendzd2_bglt BgL_i1059z00_1239;

						BgL_i1059z00_1239 =
							((BgL_threadzd2backendzd2_bglt) CAR(BgL_tbsz00_1236));
						{	/* Llib/thread.scm 314 */
							bool_t BgL_test2595z00_5063;

							{	/* Llib/thread.scm 314 */
								obj_t BgL_arg1326z00_1243;

								BgL_arg1326z00_1243 =
									(((BgL_threadzd2backendzd2_bglt) COBJECT(BgL_i1059z00_1239))->
									BgL_namez00);
								{	/* Llib/thread.scm 314 */
									long BgL_l1z00_2221;

									BgL_l1z00_2221 = STRING_LENGTH(BgL_arg1326z00_1243);
									if ((BgL_l1z00_2221 == STRING_LENGTH(BgL_namez00_6)))
										{	/* Llib/thread.scm 314 */
											int BgL_arg1626z00_2224;

											{	/* Llib/thread.scm 314 */
												char *BgL_auxz00_5071;
												char *BgL_tmpz00_5069;

												BgL_auxz00_5071 = BSTRING_TO_STRING(BgL_namez00_6);
												BgL_tmpz00_5069 =
													BSTRING_TO_STRING(BgL_arg1326z00_1243);
												BgL_arg1626z00_2224 =
													memcmp(BgL_tmpz00_5069, BgL_auxz00_5071,
													BgL_l1z00_2221);
											}
											BgL_test2595z00_5063 =
												((long) (BgL_arg1626z00_2224) == 0L);
										}
									else
										{	/* Llib/thread.scm 314 */
											BgL_test2595z00_5063 = ((bool_t) 0);
										}
								}
							}
							if (BgL_test2595z00_5063)
								{	/* Llib/thread.scm 314 */
									return CAR(BgL_tbsz00_1236);
								}
							else
								{
									obj_t BgL_tbsz00_5077;

									BgL_tbsz00_5077 = CDR(BgL_tbsz00_1236);
									BgL_tbsz00_1236 = BgL_tbsz00_5077;
									goto BgL_zc3z04anonymousza31321ze3z87_1237;
								}
						}
					}
				else
					{	/* Llib/thread.scm 312 */
						return BFALSE;
					}
			}
		}

	}



/* &get-thread-backend */
	obj_t BGl_z62getzd2threadzd2backendz62zz__threadz00(obj_t BgL_envz00_3464,
		obj_t BgL_namez00_3465)
	{
		{	/* Llib/thread.scm 310 */
			{	/* Llib/thread.scm 312 */
				obj_t BgL_auxz00_5079;

				if (STRINGP(BgL_namez00_3465))
					{	/* Llib/thread.scm 312 */
						BgL_auxz00_5079 = BgL_namez00_3465;
					}
				else
					{
						obj_t BgL_auxz00_5082;

						BgL_auxz00_5082 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
							BINT(12194L), BGl_string2155z00zz__threadz00,
							BGl_string2156z00zz__threadz00, BgL_namez00_3465);
						FAILURE(BgL_auxz00_5082, BFALSE, BFALSE);
					}
				return BGl_getzd2threadzd2backendz00zz__threadz00(BgL_auxz00_5079);
			}
		}

	}



/* _make-thread */
	obj_t BGl__makezd2threadzd2zz__threadz00(obj_t BgL_env1223z00_47,
		obj_t BgL_opt1222z00_46)
	{
		{	/* Llib/thread.scm 445 */
			{	/* Llib/thread.scm 445 */
				obj_t BgL_g1224z00_4018;

				BgL_g1224z00_4018 = VECTOR_REF(BgL_opt1222z00_46, 0L);
				switch (VECTOR_LENGTH(BgL_opt1222z00_46))
					{
					case 1L:

						{	/* Llib/thread.scm 445 */
							obj_t BgL_namez00_4019;

							BgL_namez00_4019 =
								BGl_gensymz00zz__r4_symbols_6_4z00
								(BGl_symbol2157z00zz__threadz00);
							{	/* Llib/thread.scm 445 */

								{	/* Llib/thread.scm 445 */
									obj_t BgL_bodyz00_4020;

									if (PROCEDUREP(BgL_g1224z00_4018))
										{	/* Llib/thread.scm 445 */
											BgL_bodyz00_4020 = BgL_g1224z00_4018;
										}
									else
										{
											obj_t BgL_auxz00_5091;

											BgL_auxz00_5091 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2151z00zz__threadz00, BINT(19158L),
												BGl_string2159z00zz__threadz00,
												BGl_string2160z00zz__threadz00, BgL_g1224z00_4018);
											FAILURE(BgL_auxz00_5091, BFALSE, BFALSE);
										}
									{	/* Llib/thread.scm 446 */
										obj_t BgL_arg1327z00_4021;

										BgL_arg1327z00_4021 =
											BGl_defaultzd2threadzd2backendz00zz__threadz00();
										{
											BgL_threadz00_bglt BgL_auxz00_5096;

											{	/* Llib/thread.scm 446 */
												BgL_threadzd2backendzd2_bglt BgL_auxz00_5097;

												if (BGl_isazf3zf3zz__objectz00(BgL_arg1327z00_4021,
														BGl_threadzd2backendzd2zz__threadz00))
													{	/* Llib/thread.scm 446 */
														BgL_auxz00_5097 =
															((BgL_threadzd2backendzd2_bglt)
															BgL_arg1327z00_4021);
													}
												else
													{
														obj_t BgL_auxz00_5101;

														BgL_auxz00_5101 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2151z00zz__threadz00, BINT(19269L),
															BGl_string2159z00zz__threadz00,
															BGl_string2153z00zz__threadz00,
															BgL_arg1327z00_4021);
														FAILURE(BgL_auxz00_5101, BFALSE, BFALSE);
													}
												BgL_auxz00_5096 =
													BGl_tbzd2makezd2threadz00zz__threadz00
													(BgL_auxz00_5097, BgL_bodyz00_4020, BgL_namez00_4019);
											}
											return ((obj_t) BgL_auxz00_5096);
										}
									}
								}
							}
						}
						break;
					case 2L:

						{	/* Llib/thread.scm 445 */
							obj_t BgL_namez00_4022;

							BgL_namez00_4022 = VECTOR_REF(BgL_opt1222z00_46, 1L);
							{	/* Llib/thread.scm 445 */

								{	/* Llib/thread.scm 445 */
									obj_t BgL_bodyz00_4023;

									if (PROCEDUREP(BgL_g1224z00_4018))
										{	/* Llib/thread.scm 445 */
											BgL_bodyz00_4023 = BgL_g1224z00_4018;
										}
									else
										{
											obj_t BgL_auxz00_5110;

											BgL_auxz00_5110 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2151z00zz__threadz00, BINT(19158L),
												BGl_string2159z00zz__threadz00,
												BGl_string2160z00zz__threadz00, BgL_g1224z00_4018);
											FAILURE(BgL_auxz00_5110, BFALSE, BFALSE);
										}
									{	/* Llib/thread.scm 446 */
										obj_t BgL_arg1327z00_4024;

										BgL_arg1327z00_4024 =
											BGl_defaultzd2threadzd2backendz00zz__threadz00();
										{
											BgL_threadz00_bglt BgL_auxz00_5115;

											{	/* Llib/thread.scm 446 */
												BgL_threadzd2backendzd2_bglt BgL_auxz00_5116;

												if (BGl_isazf3zf3zz__objectz00(BgL_arg1327z00_4024,
														BGl_threadzd2backendzd2zz__threadz00))
													{	/* Llib/thread.scm 446 */
														BgL_auxz00_5116 =
															((BgL_threadzd2backendzd2_bglt)
															BgL_arg1327z00_4024);
													}
												else
													{
														obj_t BgL_auxz00_5120;

														BgL_auxz00_5120 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2151z00zz__threadz00, BINT(19269L),
															BGl_string2159z00zz__threadz00,
															BGl_string2153z00zz__threadz00,
															BgL_arg1327z00_4024);
														FAILURE(BgL_auxz00_5120, BFALSE, BFALSE);
													}
												BgL_auxz00_5115 =
													BGl_tbzd2makezd2threadz00zz__threadz00
													(BgL_auxz00_5116, BgL_bodyz00_4023, BgL_namez00_4022);
											}
											return ((obj_t) BgL_auxz00_5115);
										}
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* make-thread */
	BGL_EXPORTED_DEF BgL_threadz00_bglt BGl_makezd2threadzd2zz__threadz00(obj_t
		BgL_bodyz00_44, obj_t BgL_namez00_45)
	{
		{	/* Llib/thread.scm 445 */
			{	/* Llib/thread.scm 446 */
				obj_t BgL_arg1327z00_4025;

				BgL_arg1327z00_4025 = BGl_defaultzd2threadzd2backendz00zz__threadz00();
				return
					BGl_tbzd2makezd2threadz00zz__threadz00(
					((BgL_threadzd2backendzd2_bglt) BgL_arg1327z00_4025), BgL_bodyz00_44,
					BgL_namez00_45);
			}
		}

	}



/* %current-thread */
	obj_t BGl_z52currentzd2threadz80zz__threadz00(void)
	{
		{	/* Llib/thread.scm 457 */
			{	/* Llib/thread.scm 458 */
				obj_t BgL_tbz00_1251;

				BgL_tbz00_1251 = BGL_THREAD_BACKEND();
				{	/* Llib/thread.scm 459 */
					bool_t BgL_test2602z00_5132;

					{	/* Llib/thread.scm 459 */
						obj_t BgL_classz00_2242;

						BgL_classz00_2242 = BGl_threadzd2backendzd2zz__threadz00;
						if (BGL_OBJECTP(BgL_tbz00_1251))
							{	/* Llib/thread.scm 459 */
								BgL_objectz00_bglt BgL_arg1916z00_2244;

								BgL_arg1916z00_2244 = (BgL_objectz00_bglt) (BgL_tbz00_1251);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Llib/thread.scm 459 */
										long BgL_idxz00_2250;

										BgL_idxz00_2250 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1916z00_2244);
										BgL_test2602z00_5132 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2250 + 1L)) == BgL_classz00_2242);
									}
								else
									{	/* Llib/thread.scm 459 */
										bool_t BgL_res1931z00_2275;

										{	/* Llib/thread.scm 459 */
											obj_t BgL_oclassz00_2258;

											{	/* Llib/thread.scm 459 */
												obj_t BgL_arg1925z00_2266;
												long BgL_arg1926z00_2267;

												BgL_arg1925z00_2266 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Llib/thread.scm 459 */
													long BgL_arg1927z00_2268;

													BgL_arg1927z00_2268 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1916z00_2244);
													BgL_arg1926z00_2267 =
														(BgL_arg1927z00_2268 - OBJECT_TYPE);
												}
												BgL_oclassz00_2258 =
													VECTOR_REF(BgL_arg1925z00_2266, BgL_arg1926z00_2267);
											}
											{	/* Llib/thread.scm 459 */
												bool_t BgL__ortest_1147z00_2259;

												BgL__ortest_1147z00_2259 =
													(BgL_classz00_2242 == BgL_oclassz00_2258);
												if (BgL__ortest_1147z00_2259)
													{	/* Llib/thread.scm 459 */
														BgL_res1931z00_2275 = BgL__ortest_1147z00_2259;
													}
												else
													{	/* Llib/thread.scm 459 */
														long BgL_odepthz00_2260;

														{	/* Llib/thread.scm 459 */
															obj_t BgL_arg1912z00_2261;

															BgL_arg1912z00_2261 = (BgL_oclassz00_2258);
															BgL_odepthz00_2260 =
																BGL_CLASS_DEPTH(BgL_arg1912z00_2261);
														}
														if ((1L < BgL_odepthz00_2260))
															{	/* Llib/thread.scm 459 */
																obj_t BgL_arg1910z00_2263;

																{	/* Llib/thread.scm 459 */
																	obj_t BgL_arg1911z00_2264;

																	BgL_arg1911z00_2264 = (BgL_oclassz00_2258);
																	BgL_arg1910z00_2263 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1911z00_2264,
																		1L);
																}
																BgL_res1931z00_2275 =
																	(BgL_arg1910z00_2263 == BgL_classz00_2242);
															}
														else
															{	/* Llib/thread.scm 459 */
																BgL_res1931z00_2275 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2602z00_5132 = BgL_res1931z00_2275;
									}
							}
						else
							{	/* Llib/thread.scm 459 */
								BgL_test2602z00_5132 = ((bool_t) 0);
							}
					}
					if (BgL_test2602z00_5132)
						{	/* Llib/thread.scm 459 */
							return
								BGl_tbzd2currentzd2threadz00zz__threadz00(
								((BgL_threadzd2backendzd2_bglt) BgL_tbz00_1251));
						}
					else
						{	/* Llib/thread.scm 459 */
							return BFALSE;
						}
				}
			}
		}

	}



/* current-thread */
	BGL_EXPORTED_DEF obj_t BGl_currentzd2threadzd2zz__threadz00(void)
	{
		{	/* Llib/thread.scm 465 */
			{	/* Llib/thread.scm 466 */
				obj_t BgL_thz00_1253;

				BgL_thz00_1253 = BGl_z52currentzd2threadz80zz__threadz00();
				{	/* Llib/thread.scm 467 */
					bool_t BgL_test2607z00_5158;

					{	/* Llib/thread.scm 467 */
						obj_t BgL_classz00_2276;

						BgL_classz00_2276 = BGl_threadz00zz__threadz00;
						if (BGL_OBJECTP(BgL_thz00_1253))
							{	/* Llib/thread.scm 467 */
								BgL_objectz00_bglt BgL_arg1916z00_2278;

								BgL_arg1916z00_2278 = (BgL_objectz00_bglt) (BgL_thz00_1253);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Llib/thread.scm 467 */
										long BgL_idxz00_2284;

										BgL_idxz00_2284 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1916z00_2278);
										BgL_test2607z00_5158 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2284 + 1L)) == BgL_classz00_2276);
									}
								else
									{	/* Llib/thread.scm 467 */
										bool_t BgL_res1932z00_2309;

										{	/* Llib/thread.scm 467 */
											obj_t BgL_oclassz00_2292;

											{	/* Llib/thread.scm 467 */
												obj_t BgL_arg1925z00_2300;
												long BgL_arg1926z00_2301;

												BgL_arg1925z00_2300 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Llib/thread.scm 467 */
													long BgL_arg1927z00_2302;

													BgL_arg1927z00_2302 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1916z00_2278);
													BgL_arg1926z00_2301 =
														(BgL_arg1927z00_2302 - OBJECT_TYPE);
												}
												BgL_oclassz00_2292 =
													VECTOR_REF(BgL_arg1925z00_2300, BgL_arg1926z00_2301);
											}
											{	/* Llib/thread.scm 467 */
												bool_t BgL__ortest_1147z00_2293;

												BgL__ortest_1147z00_2293 =
													(BgL_classz00_2276 == BgL_oclassz00_2292);
												if (BgL__ortest_1147z00_2293)
													{	/* Llib/thread.scm 467 */
														BgL_res1932z00_2309 = BgL__ortest_1147z00_2293;
													}
												else
													{	/* Llib/thread.scm 467 */
														long BgL_odepthz00_2294;

														{	/* Llib/thread.scm 467 */
															obj_t BgL_arg1912z00_2295;

															BgL_arg1912z00_2295 = (BgL_oclassz00_2292);
															BgL_odepthz00_2294 =
																BGL_CLASS_DEPTH(BgL_arg1912z00_2295);
														}
														if ((1L < BgL_odepthz00_2294))
															{	/* Llib/thread.scm 467 */
																obj_t BgL_arg1910z00_2297;

																{	/* Llib/thread.scm 467 */
																	obj_t BgL_arg1911z00_2298;

																	BgL_arg1911z00_2298 = (BgL_oclassz00_2292);
																	BgL_arg1910z00_2297 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1911z00_2298,
																		1L);
																}
																BgL_res1932z00_2309 =
																	(BgL_arg1910z00_2297 == BgL_classz00_2276);
															}
														else
															{	/* Llib/thread.scm 467 */
																BgL_res1932z00_2309 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2607z00_5158 = BgL_res1932z00_2309;
									}
							}
						else
							{	/* Llib/thread.scm 467 */
								BgL_test2607z00_5158 = ((bool_t) 0);
							}
					}
					if (BgL_test2607z00_5158)
						{	/* Llib/thread.scm 467 */
							return
								BGl_z52userzd2currentzd2threadz52zz__threadz00(
								((BgL_threadz00_bglt) BgL_thz00_1253));
						}
					else
						{	/* Llib/thread.scm 467 */
							return BFALSE;
						}
				}
			}
		}

	}



/* &current-thread */
	obj_t BGl_z62currentzd2threadzb0zz__threadz00(obj_t BgL_envz00_3466)
	{
		{	/* Llib/thread.scm 465 */
			return BGl_currentzd2threadzd2zz__threadz00();
		}

	}



/* thread-sleep! */
	BGL_EXPORTED_DEF obj_t BGl_threadzd2sleepz12zc0zz__threadz00(obj_t
		BgL_objz00_51)
	{
		{	/* Llib/thread.scm 494 */
			{	/* Llib/thread.scm 495 */
				obj_t BgL_arg1331z00_4026;

				BgL_arg1331z00_4026 = BGl_currentzd2threadzd2zz__threadz00();
				return
					BGl_z52userzd2threadzd2sleepz12z40zz__threadz00(
					((BgL_threadz00_bglt) BgL_arg1331z00_4026), BgL_objz00_51);
			}
		}

	}



/* &thread-sleep! */
	obj_t BGl_z62threadzd2sleepz12za2zz__threadz00(obj_t BgL_envz00_3467,
		obj_t BgL_objz00_3468)
	{
		{	/* Llib/thread.scm 494 */
			return BGl_threadzd2sleepz12zc0zz__threadz00(BgL_objz00_3468);
		}

	}



/* thread-yield! */
	BGL_EXPORTED_DEF obj_t BGl_threadzd2yieldz12zc0zz__threadz00(void)
	{
		{	/* Llib/thread.scm 506 */
			{	/* Llib/thread.scm 507 */
				obj_t BgL_arg1332z00_4027;

				BgL_arg1332z00_4027 = BGl_currentzd2threadzd2zz__threadz00();
				return
					BGl_z52userzd2threadzd2yieldz12z40zz__threadz00(
					((BgL_threadz00_bglt) BgL_arg1332z00_4027));
			}
		}

	}



/* &thread-yield! */
	obj_t BGl_z62threadzd2yieldz12za2zz__threadz00(obj_t BgL_envz00_3469)
	{
		{	/* Llib/thread.scm 506 */
			return BGl_threadzd2yieldz12zc0zz__threadz00();
		}

	}



/* thread-parameter */
	BGL_EXPORTED_DEF obj_t BGl_threadzd2parameterzd2zz__threadz00(obj_t
		BgL_idz00_53)
	{
		{	/* Llib/thread.scm 512 */
			{	/* Llib/thread.scm 513 */
				obj_t BgL_cz00_2312;

				{	/* Llib/thread.scm 513 */
					obj_t BgL_arg1334z00_2313;

					BgL_arg1334z00_2313 = BGL_PARAMETERS();
					BgL_cz00_2312 =
						BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_idz00_53,
						BgL_arg1334z00_2313);
				}
				if (PAIRP(BgL_cz00_2312))
					{	/* Llib/thread.scm 514 */
						return CDR(BgL_cz00_2312);
					}
				else
					{	/* Llib/thread.scm 514 */
						return BFALSE;
					}
			}
		}

	}



/* &thread-parameter */
	obj_t BGl_z62threadzd2parameterzb0zz__threadz00(obj_t BgL_envz00_3470,
		obj_t BgL_idz00_3471)
	{
		{	/* Llib/thread.scm 512 */
			{	/* Llib/thread.scm 513 */
				obj_t BgL_auxz00_5197;

				if (SYMBOLP(BgL_idz00_3471))
					{	/* Llib/thread.scm 513 */
						BgL_auxz00_5197 = BgL_idz00_3471;
					}
				else
					{
						obj_t BgL_auxz00_5200;

						BgL_auxz00_5200 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
							BINT(22199L), BGl_string2161z00zz__threadz00,
							BGl_string2162z00zz__threadz00, BgL_idz00_3471);
						FAILURE(BgL_auxz00_5200, BFALSE, BFALSE);
					}
				return BGl_threadzd2parameterzd2zz__threadz00(BgL_auxz00_5197);
			}
		}

	}



/* thread-parameter-set! */
	BGL_EXPORTED_DEF obj_t BGl_threadzd2parameterzd2setz12z12zz__threadz00(obj_t
		BgL_idz00_54, obj_t BgL_valz00_55)
	{
		{	/* Llib/thread.scm 521 */
			{	/* Llib/thread.scm 522 */
				obj_t BgL_cz00_2316;

				{	/* Llib/thread.scm 522 */
					obj_t BgL_arg1339z00_2317;

					BgL_arg1339z00_2317 = BGL_PARAMETERS();
					BgL_cz00_2316 =
						BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_idz00_54,
						BgL_arg1339z00_2317);
				}
				if (PAIRP(BgL_cz00_2316))
					{	/* Llib/thread.scm 523 */
						return SET_CDR(BgL_cz00_2316, BgL_valz00_55);
					}
				else
					{	/* Llib/thread.scm 523 */
						{	/* Llib/thread.scm 527 */
							obj_t BgL_arg1336z00_2319;

							{	/* Llib/thread.scm 527 */
								obj_t BgL_arg1337z00_2320;
								obj_t BgL_arg1338z00_2321;

								BgL_arg1337z00_2320 =
									MAKE_YOUNG_PAIR(BgL_idz00_54, BgL_valz00_55);
								BgL_arg1338z00_2321 = BGL_PARAMETERS();
								BgL_arg1336z00_2319 =
									MAKE_YOUNG_PAIR(BgL_arg1337z00_2320, BgL_arg1338z00_2321);
							}
							BGL_PARAMETERS_SET(BgL_arg1336z00_2319);
						}
						return BgL_valz00_55;
					}
			}
		}

	}



/* &thread-parameter-set! */
	obj_t BGl_z62threadzd2parameterzd2setz12z70zz__threadz00(obj_t
		BgL_envz00_3472, obj_t BgL_idz00_3473, obj_t BgL_valz00_3474)
	{
		{	/* Llib/thread.scm 521 */
			{	/* Llib/thread.scm 522 */
				obj_t BgL_auxz00_5214;

				if (SYMBOLP(BgL_idz00_3473))
					{	/* Llib/thread.scm 522 */
						BgL_auxz00_5214 = BgL_idz00_3473;
					}
				else
					{
						obj_t BgL_auxz00_5217;

						BgL_auxz00_5217 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
							BINT(22546L), BGl_string2163z00zz__threadz00,
							BGl_string2162z00zz__threadz00, BgL_idz00_3473);
						FAILURE(BgL_auxz00_5217, BFALSE, BFALSE);
					}
				return
					BGl_threadzd2parameterzd2setz12z12zz__threadz00(BgL_auxz00_5214,
					BgL_valz00_3474);
			}
		}

	}



/* mutex? */
	BGL_EXPORTED_DEF bool_t BGl_mutexzf3zf3zz__threadz00(obj_t BgL_objz00_73)
	{
		{	/* Llib/thread.scm 614 */
			return BGL_MUTEXP(BgL_objz00_73);
		}

	}



/* &mutex? */
	obj_t BGl_z62mutexzf3z91zz__threadz00(obj_t BgL_envz00_3475,
		obj_t BgL_objz00_3476)
	{
		{	/* Llib/thread.scm 614 */
			return BBOOL(BGl_mutexzf3zf3zz__threadz00(BgL_objz00_3476));
		}

	}



/* _make-mutex */
	obj_t BGl__makezd2mutexzd2zz__threadz00(obj_t BgL_env1256z00_76,
		obj_t BgL_opt1255z00_75)
	{
		{	/* Llib/thread.scm 620 */
			{	/* Llib/thread.scm 620 */

				switch (VECTOR_LENGTH(BgL_opt1255z00_75))
					{
					case 0L:

						{	/* Llib/thread.scm 620 */
							obj_t BgL_namez00_4028;

							BgL_namez00_4028 =
								BGl_gensymz00zz__r4_symbols_6_4z00
								(BGl_symbol2164z00zz__threadz00);
							{	/* Llib/thread.scm 620 */

								return bgl_make_mutex(BgL_namez00_4028);
							}
						}
						break;
					case 1L:

						{	/* Llib/thread.scm 620 */
							obj_t BgL_namez00_4029;

							BgL_namez00_4029 = VECTOR_REF(BgL_opt1255z00_75, 0L);
							{	/* Llib/thread.scm 620 */

								return bgl_make_mutex(BgL_namez00_4029);
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* make-mutex */
	BGL_EXPORTED_DEF obj_t BGl_makezd2mutexzd2zz__threadz00(obj_t BgL_namez00_74)
	{
		{	/* Llib/thread.scm 620 */
			return bgl_make_mutex(BgL_namez00_74);
		}

	}



/* _make-spinlock */
	obj_t BGl__makezd2spinlockzd2zz__threadz00(obj_t BgL_env1260z00_79,
		obj_t BgL_opt1259z00_78)
	{
		{	/* Llib/thread.scm 626 */
			{	/* Llib/thread.scm 626 */

				switch (VECTOR_LENGTH(BgL_opt1259z00_78))
					{
					case 0L:

						{	/* Llib/thread.scm 626 */
							obj_t BgL_namez00_4030;

							BgL_namez00_4030 =
								BGl_gensymz00zz__r4_symbols_6_4z00
								(BGl_symbol2166z00zz__threadz00);
							{	/* Llib/thread.scm 626 */

								return bgl_make_spinlock(BgL_namez00_4030);
							}
						}
						break;
					case 1L:

						{	/* Llib/thread.scm 626 */
							obj_t BgL_namez00_4031;

							BgL_namez00_4031 = VECTOR_REF(BgL_opt1259z00_78, 0L);
							{	/* Llib/thread.scm 626 */

								return bgl_make_spinlock(BgL_namez00_4031);
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* make-spinlock */
	BGL_EXPORTED_DEF obj_t BGl_makezd2spinlockzd2zz__threadz00(obj_t
		BgL_namez00_77)
	{
		{	/* Llib/thread.scm 626 */
			return bgl_make_spinlock(BgL_namez00_77);
		}

	}



/* mutex-nil */
	BGL_EXPORTED_DEF obj_t BGl_mutexzd2nilzd2zz__threadz00(void)
	{
		{	/* Llib/thread.scm 633 */
			return BGl_za2mutexzd2nilza2zd2zz__threadz00;
		}

	}



/* &mutex-nil */
	obj_t BGl_z62mutexzd2nilzb0zz__threadz00(obj_t BgL_envz00_3477)
	{
		{	/* Llib/thread.scm 633 */
			return BGl_mutexzd2nilzd2zz__threadz00();
		}

	}



/* mutex-name */
	BGL_EXPORTED_DEF obj_t BGl_mutexzd2namezd2zz__threadz00(obj_t BgL_objz00_80)
	{
		{	/* Llib/thread.scm 638 */
			return BGL_MUTEX_NAME(BgL_objz00_80);
		}

	}



/* &mutex-name */
	obj_t BGl_z62mutexzd2namezb0zz__threadz00(obj_t BgL_envz00_3478,
		obj_t BgL_objz00_3479)
	{
		{	/* Llib/thread.scm 638 */
			{	/* Llib/thread.scm 639 */
				obj_t BgL_auxz00_5241;

				if (BGL_MUTEXP(BgL_objz00_3479))
					{	/* Llib/thread.scm 639 */
						BgL_auxz00_5241 = BgL_objz00_3479;
					}
				else
					{
						obj_t BgL_auxz00_5244;

						BgL_auxz00_5244 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
							BINT(28093L), BGl_string2168z00zz__threadz00,
							BGl_string2165z00zz__threadz00, BgL_objz00_3479);
						FAILURE(BgL_auxz00_5244, BFALSE, BFALSE);
					}
				return BGl_mutexzd2namezd2zz__threadz00(BgL_auxz00_5241);
			}
		}

	}



/* _mutex-lock! */
	obj_t BGl__mutexzd2lockz12zc0zz__threadz00(obj_t BgL_env1264z00_84,
		obj_t BgL_opt1263z00_83)
	{
		{	/* Llib/thread.scm 644 */
			{	/* Llib/thread.scm 644 */
				obj_t BgL_g1265z00_4032;

				BgL_g1265z00_4032 = VECTOR_REF(BgL_opt1263z00_83, 0L);
				switch (VECTOR_LENGTH(BgL_opt1263z00_83))
					{
					case 1L:

						{	/* Llib/thread.scm 644 */

							{	/* Llib/thread.scm 644 */
								obj_t BgL_mz00_4033;

								if (BGL_MUTEXP(BgL_g1265z00_4032))
									{	/* Llib/thread.scm 644 */
										BgL_mz00_4033 = BgL_g1265z00_4032;
									}
								else
									{
										obj_t BgL_auxz00_5252;

										BgL_auxz00_5252 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2151z00zz__threadz00, BINT(28335L),
											BGl_string2169z00zz__threadz00,
											BGl_string2165z00zz__threadz00, BgL_g1265z00_4032);
										FAILURE(BgL_auxz00_5252, BFALSE, BFALSE);
									}
								{	/* Llib/thread.scm 646 */
									int BgL_arg1341z00_4034;

									BgL_arg1341z00_4034 = BGL_MUTEX_LOCK(BgL_mz00_4033);
									return BBOOL(((long) (BgL_arg1341z00_4034) == 0L));
						}}} break;
					case 2L:

						{	/* Llib/thread.scm 644 */
							obj_t BgL_timeoutz00_4035;

							BgL_timeoutz00_4035 = VECTOR_REF(BgL_opt1263z00_83, 1L);
							{	/* Llib/thread.scm 644 */

								{	/* Llib/thread.scm 644 */
									obj_t BgL_mz00_4036;
									long BgL_timeoutz00_4037;

									if (BGL_MUTEXP(BgL_g1265z00_4032))
										{	/* Llib/thread.scm 644 */
											BgL_mz00_4036 = BgL_g1265z00_4032;
										}
									else
										{
											obj_t BgL_auxz00_5263;

											BgL_auxz00_5263 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2151z00zz__threadz00, BINT(28335L),
												BGl_string2169z00zz__threadz00,
												BGl_string2165z00zz__threadz00, BgL_g1265z00_4032);
											FAILURE(BgL_auxz00_5263, BFALSE, BFALSE);
										}
									{	/* Llib/thread.scm 644 */
										obj_t BgL_tmpz00_5267;

										if (INTEGERP(BgL_timeoutz00_4035))
											{	/* Llib/thread.scm 644 */
												BgL_tmpz00_5267 = BgL_timeoutz00_4035;
											}
										else
											{
												obj_t BgL_auxz00_5270;

												BgL_auxz00_5270 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2151z00zz__threadz00, BINT(28335L),
													BGl_string2169z00zz__threadz00,
													BGl_string2170z00zz__threadz00, BgL_timeoutz00_4035);
												FAILURE(BgL_auxz00_5270, BFALSE, BFALSE);
											}
										BgL_timeoutz00_4037 = (long) CINT(BgL_tmpz00_5267);
									}
									if ((BgL_timeoutz00_4037 == 0L))
										{	/* Llib/thread.scm 646 */
											int BgL_arg1341z00_4038;

											BgL_arg1341z00_4038 = BGL_MUTEX_LOCK(BgL_mz00_4036);
											return BBOOL(((long) (BgL_arg1341z00_4038) == 0L));
										}
									else
										{	/* Llib/thread.scm 647 */
											int BgL_arg1342z00_4039;

											BgL_arg1342z00_4039 =
												BGL_MUTEX_TIMED_LOCK(BgL_mz00_4036,
												BgL_timeoutz00_4037);
											return BBOOL(((long) (BgL_arg1342z00_4039) == 0L));
						}}}} break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* mutex-lock! */
	BGL_EXPORTED_DEF obj_t BGl_mutexzd2lockz12zc0zz__threadz00(obj_t BgL_mz00_81,
		long BgL_timeoutz00_82)
	{
		{	/* Llib/thread.scm 644 */
			if ((BgL_timeoutz00_82 == 0L))
				{	/* Llib/thread.scm 646 */
					int BgL_arg1341z00_4040;

					BgL_arg1341z00_4040 = BGL_MUTEX_LOCK(BgL_mz00_81);
					return BBOOL(((long) (BgL_arg1341z00_4040) == 0L));
				}
			else
				{	/* Llib/thread.scm 647 */
					int BgL_arg1342z00_4041;

					BgL_arg1342z00_4041 =
						BGL_MUTEX_TIMED_LOCK(BgL_mz00_81, BgL_timeoutz00_82);
					return BBOOL(((long) (BgL_arg1342z00_4041) == 0L));
		}}

	}



/* mutex-unlock! */
	BGL_EXPORTED_DEF obj_t BGl_mutexzd2unlockz12zc0zz__threadz00(obj_t
		BgL_mz00_85)
	{
		{	/* Llib/thread.scm 652 */
			{	/* Llib/thread.scm 653 */
				int BgL_arg1343z00_4042;

				BgL_arg1343z00_4042 = BGL_MUTEX_UNLOCK(BgL_mz00_85);
				return BBOOL(((long) (BgL_arg1343z00_4042) == 0L));
		}}

	}



/* &mutex-unlock! */
	obj_t BGl_z62mutexzd2unlockz12za2zz__threadz00(obj_t BgL_envz00_3480,
		obj_t BgL_mz00_3481)
	{
		{	/* Llib/thread.scm 652 */
			{	/* Llib/thread.scm 653 */
				obj_t BgL_auxz00_5301;

				if (BGL_MUTEXP(BgL_mz00_3481))
					{	/* Llib/thread.scm 653 */
						BgL_auxz00_5301 = BgL_mz00_3481;
					}
				else
					{
						obj_t BgL_auxz00_5304;

						BgL_auxz00_5304 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
							BINT(28760L), BGl_string2171z00zz__threadz00,
							BGl_string2165z00zz__threadz00, BgL_mz00_3481);
						FAILURE(BgL_auxz00_5304, BFALSE, BFALSE);
					}
				return BGl_mutexzd2unlockz12zc0zz__threadz00(BgL_auxz00_5301);
			}
		}

	}



/* mutex-state */
	BGL_EXPORTED_DEF obj_t BGl_mutexzd2statezd2zz__threadz00(obj_t
		BgL_mutexz00_86)
	{
		{	/* Llib/thread.scm 658 */
			return BGL_MUTEX_STATE(BgL_mutexz00_86);
		}

	}



/* &mutex-state */
	obj_t BGl_z62mutexzd2statezb0zz__threadz00(obj_t BgL_envz00_3482,
		obj_t BgL_mutexz00_3483)
	{
		{	/* Llib/thread.scm 658 */
			{	/* Llib/thread.scm 659 */
				obj_t BgL_auxz00_5310;

				if (BGL_MUTEXP(BgL_mutexz00_3483))
					{	/* Llib/thread.scm 659 */
						BgL_auxz00_5310 = BgL_mutexz00_3483;
					}
				else
					{
						obj_t BgL_auxz00_5313;

						BgL_auxz00_5313 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
							BINT(29043L), BGl_string2172z00zz__threadz00,
							BGl_string2165z00zz__threadz00, BgL_mutexz00_3483);
						FAILURE(BgL_auxz00_5313, BFALSE, BFALSE);
					}
				return BGl_mutexzd2statezd2zz__threadz00(BgL_auxz00_5310);
			}
		}

	}



/* with-lock */
	BGL_EXPORTED_DEF obj_t BGl_withzd2lockzd2zz__threadz00(obj_t BgL_mutexz00_88,
		obj_t BgL_thunkz00_89)
	{
		{	/* Llib/thread.scm 670 */
			{	/* Llib/thread.scm 671 */
				obj_t BgL_top2625z00_5319;

				BgL_top2625z00_5319 = BGL_EXITD_TOP_AS_OBJ();
				BGL_MUTEX_LOCK(BgL_mutexz00_88);
				BGL_EXITD_PUSH_PROTECT(BgL_top2625z00_5319, BgL_mutexz00_88);
				BUNSPEC;
				{	/* Llib/thread.scm 671 */
					obj_t BgL_tmp2624z00_5318;

					BgL_tmp2624z00_5318 = BGL_PROCEDURE_CALL0(BgL_thunkz00_89);
					BGL_EXITD_POP_PROTECT(BgL_top2625z00_5319);
					BUNSPEC;
					BGL_MUTEX_UNLOCK(BgL_mutexz00_88);
					return BgL_tmp2624z00_5318;
				}
			}
		}

	}



/* &with-lock */
	obj_t BGl_z62withzd2lockzb0zz__threadz00(obj_t BgL_envz00_3484,
		obj_t BgL_mutexz00_3485, obj_t BgL_thunkz00_3486)
	{
		{	/* Llib/thread.scm 670 */
			{	/* Llib/thread.scm 671 */
				obj_t BgL_auxz00_5335;
				obj_t BgL_auxz00_5328;

				if (PROCEDUREP(BgL_thunkz00_3486))
					{	/* Llib/thread.scm 671 */
						BgL_auxz00_5335 = BgL_thunkz00_3486;
					}
				else
					{
						obj_t BgL_auxz00_5338;

						BgL_auxz00_5338 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
							BINT(29606L), BGl_string2173z00zz__threadz00,
							BGl_string2160z00zz__threadz00, BgL_thunkz00_3486);
						FAILURE(BgL_auxz00_5338, BFALSE, BFALSE);
					}
				if (BGL_MUTEXP(BgL_mutexz00_3485))
					{	/* Llib/thread.scm 671 */
						BgL_auxz00_5328 = BgL_mutexz00_3485;
					}
				else
					{
						obj_t BgL_auxz00_5331;

						BgL_auxz00_5331 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
							BINT(29606L), BGl_string2173z00zz__threadz00,
							BGl_string2165z00zz__threadz00, BgL_mutexz00_3485);
						FAILURE(BgL_auxz00_5331, BFALSE, BFALSE);
					}
				return
					BGl_withzd2lockzd2zz__threadz00(BgL_auxz00_5328, BgL_auxz00_5335);
			}
		}

	}



/* with-timed-lock */
	BGL_EXPORTED_DEF obj_t BGl_withzd2timedzd2lockz00zz__threadz00(obj_t
		BgL_mutexz00_90, int BgL_timeoutz00_91, obj_t BgL_thunkz00_92)
	{
		{	/* Llib/thread.scm 677 */
			{	/* Llib/thread.scm 678 */
				bool_t BgL_test2628z00_5343;

				{	/* Llib/thread.scm 678 */
					long BgL_timeoutz00_2365;

					BgL_timeoutz00_2365 = (long) (BgL_timeoutz00_91);
					if ((BgL_timeoutz00_2365 == 0L))
						{	/* Llib/thread.scm 646 */
							int BgL_arg1341z00_2367;

							BgL_arg1341z00_2367 = BGL_MUTEX_LOCK(BgL_mutexz00_90);
							BgL_test2628z00_5343 = ((long) (BgL_arg1341z00_2367) == 0L);
						}
					else
						{	/* Llib/thread.scm 647 */
							int BgL_arg1342z00_2368;

							BgL_arg1342z00_2368 =
								BGL_MUTEX_TIMED_LOCK(BgL_mutexz00_90, BgL_timeoutz00_2365);
							BgL_test2628z00_5343 = ((long) (BgL_arg1342z00_2368) == 0L);
				}}
				if (BgL_test2628z00_5343)
					{	/* Llib/thread.scm 679 */
						obj_t BgL_exitd1103z00_2361;

						BgL_exitd1103z00_2361 = BGL_EXITD_TOP_AS_OBJ();
						{	/* Llib/thread.scm 681 */
							obj_t BgL_zc3z04anonymousza31345ze3z87_3487;

							BgL_zc3z04anonymousza31345ze3z87_3487 =
								MAKE_FX_PROCEDURE
								(BGl_z62zc3z04anonymousza31345ze3ze5zz__threadz00, (int) (0L),
								(int) (1L));
							PROCEDURE_SET(BgL_zc3z04anonymousza31345ze3z87_3487, (int) (0L),
								BgL_mutexz00_90);
							{	/* Llib/thread.scm 679 */
								obj_t BgL_arg1899z00_2375;

								{	/* Llib/thread.scm 679 */
									obj_t BgL_arg1901z00_2376;

									BgL_arg1901z00_2376 =
										BGL_EXITD_PROTECT(BgL_exitd1103z00_2361);
									BgL_arg1899z00_2375 =
										MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31345ze3z87_3487,
										BgL_arg1901z00_2376);
								}
								BGL_EXITD_PROTECT_SET(BgL_exitd1103z00_2361,
									BgL_arg1899z00_2375);
								BUNSPEC;
							}
							{	/* Llib/thread.scm 680 */
								obj_t BgL_tmp1105z00_2363;

								BgL_tmp1105z00_2363 = BGL_PROCEDURE_CALL0(BgL_thunkz00_92);
								{	/* Llib/thread.scm 679 */
									bool_t BgL_test2630z00_5365;

									{	/* Llib/thread.scm 679 */
										obj_t BgL_arg1898z00_2378;

										BgL_arg1898z00_2378 =
											BGL_EXITD_PROTECT(BgL_exitd1103z00_2361);
										BgL_test2630z00_5365 = PAIRP(BgL_arg1898z00_2378);
									}
									if (BgL_test2630z00_5365)
										{	/* Llib/thread.scm 679 */
											obj_t BgL_arg1896z00_2379;

											{	/* Llib/thread.scm 679 */
												obj_t BgL_arg1897z00_2380;

												BgL_arg1897z00_2380 =
													BGL_EXITD_PROTECT(BgL_exitd1103z00_2361);
												BgL_arg1896z00_2379 =
													CDR(((obj_t) BgL_arg1897z00_2380));
											}
											BGL_EXITD_PROTECT_SET(BgL_exitd1103z00_2361,
												BgL_arg1896z00_2379);
											BUNSPEC;
										}
									else
										{	/* Llib/thread.scm 679 */
											BFALSE;
										}
								}
								{	/* Llib/thread.scm 653 */
									int BgL_arg1343z00_2383;

									BgL_arg1343z00_2383 = BGL_MUTEX_UNLOCK(BgL_mutexz00_90);
									((long) (BgL_arg1343z00_2383) == 0L);
								}
								return BgL_tmp1105z00_2363;
							}
						}
					}
				else
					{	/* Llib/thread.scm 678 */
						return BFALSE;
					}
			}
		}

	}



/* &with-timed-lock */
	obj_t BGl_z62withzd2timedzd2lockz62zz__threadz00(obj_t BgL_envz00_3488,
		obj_t BgL_mutexz00_3489, obj_t BgL_timeoutz00_3490, obj_t BgL_thunkz00_3491)
	{
		{	/* Llib/thread.scm 677 */
			{	/* Llib/thread.scm 678 */
				obj_t BgL_auxz00_5391;
				int BgL_auxz00_5382;
				obj_t BgL_auxz00_5375;

				if (PROCEDUREP(BgL_thunkz00_3491))
					{	/* Llib/thread.scm 678 */
						BgL_auxz00_5391 = BgL_thunkz00_3491;
					}
				else
					{
						obj_t BgL_auxz00_5394;

						BgL_auxz00_5394 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
							BINT(29913L), BGl_string2174z00zz__threadz00,
							BGl_string2160z00zz__threadz00, BgL_thunkz00_3491);
						FAILURE(BgL_auxz00_5394, BFALSE, BFALSE);
					}
				{	/* Llib/thread.scm 678 */
					obj_t BgL_tmpz00_5383;

					if (INTEGERP(BgL_timeoutz00_3490))
						{	/* Llib/thread.scm 678 */
							BgL_tmpz00_5383 = BgL_timeoutz00_3490;
						}
					else
						{
							obj_t BgL_auxz00_5386;

							BgL_auxz00_5386 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
								BINT(29913L), BGl_string2174z00zz__threadz00,
								BGl_string2170z00zz__threadz00, BgL_timeoutz00_3490);
							FAILURE(BgL_auxz00_5386, BFALSE, BFALSE);
						}
					BgL_auxz00_5382 = CINT(BgL_tmpz00_5383);
				}
				if (BGL_MUTEXP(BgL_mutexz00_3489))
					{	/* Llib/thread.scm 678 */
						BgL_auxz00_5375 = BgL_mutexz00_3489;
					}
				else
					{
						obj_t BgL_auxz00_5378;

						BgL_auxz00_5378 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
							BINT(29913L), BGl_string2174z00zz__threadz00,
							BGl_string2165z00zz__threadz00, BgL_mutexz00_3489);
						FAILURE(BgL_auxz00_5378, BFALSE, BFALSE);
					}
				return
					BGl_withzd2timedzd2lockz00zz__threadz00(BgL_auxz00_5375,
					BgL_auxz00_5382, BgL_auxz00_5391);
			}
		}

	}



/* &<@anonymous:1345> */
	obj_t BGl_z62zc3z04anonymousza31345ze3ze5zz__threadz00(obj_t BgL_envz00_3492)
	{
		{	/* Llib/thread.scm 679 */
			{	/* Llib/thread.scm 653 */
				obj_t BgL_mutexz00_3493;

				BgL_mutexz00_3493 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3492, (int) (0L)));
				{	/* Llib/thread.scm 653 */
					bool_t BgL_tmpz00_5402;

					{	/* Llib/thread.scm 653 */
						int BgL_arg1343z00_4043;

						BgL_arg1343z00_4043 = BGL_MUTEX_UNLOCK(BgL_mutexz00_3493);
						BgL_tmpz00_5402 = ((long) (BgL_arg1343z00_4043) == 0L);
					}
					return BBOOL(BgL_tmpz00_5402);
				}
			}
		}

	}



/* condition-variable? */
	BGL_EXPORTED_DEF bool_t BGl_conditionzd2variablezf3z21zz__threadz00(obj_t
		BgL_objz00_93)
	{
		{	/* Llib/thread.scm 686 */
			return BGL_CONDVARP(BgL_objz00_93);
		}

	}



/* &condition-variable? */
	obj_t BGl_z62conditionzd2variablezf3z43zz__threadz00(obj_t BgL_envz00_3494,
		obj_t BgL_objz00_3495)
	{
		{	/* Llib/thread.scm 686 */
			return
				BBOOL(BGl_conditionzd2variablezf3z21zz__threadz00(BgL_objz00_3495));
		}

	}



/* _make-condition-variable */
	obj_t BGl__makezd2conditionzd2variablez00zz__threadz00(obj_t
		BgL_env1269z00_96, obj_t BgL_opt1268z00_95)
	{
		{	/* Llib/thread.scm 692 */
			{	/* Llib/thread.scm 692 */

				switch (VECTOR_LENGTH(BgL_opt1268z00_95))
					{
					case 0L:

						{	/* Llib/thread.scm 693 */
							obj_t BgL_namez00_4044;

							BgL_namez00_4044 =
								BGl_gensymz00zz__r4_symbols_6_4z00
								(BGl_symbol2175z00zz__threadz00);
							{	/* Llib/thread.scm 692 */

								return bgl_make_condvar(BgL_namez00_4044);
							}
						}
						break;
					case 1L:

						{	/* Llib/thread.scm 692 */
							obj_t BgL_namez00_4045;

							BgL_namez00_4045 = VECTOR_REF(BgL_opt1268z00_95, 0L);
							{	/* Llib/thread.scm 692 */

								return bgl_make_condvar(BgL_namez00_4045);
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* make-condition-variable */
	BGL_EXPORTED_DEF obj_t BGl_makezd2conditionzd2variablez00zz__threadz00(obj_t
		BgL_namez00_94)
	{
		{	/* Llib/thread.scm 692 */
			return bgl_make_condvar(BgL_namez00_94);
		}

	}



/* condition-variable-nil */
	BGL_EXPORTED_DEF obj_t BGl_conditionzd2variablezd2nilz00zz__threadz00(void)
	{
		{	/* Llib/thread.scm 700 */
			return BGl_za2conditionzd2variablezd2nilza2z00zz__threadz00;
		}

	}



/* &condition-variable-nil */
	obj_t BGl_z62conditionzd2variablezd2nilz62zz__threadz00(obj_t BgL_envz00_3496)
	{
		{	/* Llib/thread.scm 700 */
			return BGl_conditionzd2variablezd2nilz00zz__threadz00();
		}

	}



/* condition-variable-name */
	BGL_EXPORTED_DEF obj_t BGl_conditionzd2variablezd2namez00zz__threadz00(obj_t
		BgL_objz00_97)
	{
		{	/* Llib/thread.scm 705 */
			return BGL_CONDVAR_NAME(BgL_objz00_97);
		}

	}



/* &condition-variable-name */
	obj_t BGl_z62conditionzd2variablezd2namez62zz__threadz00(obj_t
		BgL_envz00_3497, obj_t BgL_objz00_3498)
	{
		{	/* Llib/thread.scm 705 */
			{	/* Llib/thread.scm 706 */
				obj_t BgL_auxz00_5419;

				if (BGL_CONDVARP(BgL_objz00_3498))
					{	/* Llib/thread.scm 706 */
						BgL_auxz00_5419 = BgL_objz00_3498;
					}
				else
					{
						obj_t BgL_auxz00_5422;

						BgL_auxz00_5422 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
							BINT(31238L), BGl_string2177z00zz__threadz00,
							BGl_string2178z00zz__threadz00, BgL_objz00_3498);
						FAILURE(BgL_auxz00_5422, BFALSE, BFALSE);
					}
				return BGl_conditionzd2variablezd2namez00zz__threadz00(BgL_auxz00_5419);
			}
		}

	}



/* _condition-variable-wait! */
	obj_t BGl__conditionzd2variablezd2waitz12z12zz__threadz00(obj_t
		BgL_env1273z00_102, obj_t BgL_opt1272z00_101)
	{
		{	/* Llib/thread.scm 711 */
			{	/* Llib/thread.scm 711 */
				obj_t BgL_g1274z00_4046;
				obj_t BgL_g1275z00_4047;

				BgL_g1274z00_4046 = VECTOR_REF(BgL_opt1272z00_101, 0L);
				BgL_g1275z00_4047 = VECTOR_REF(BgL_opt1272z00_101, 1L);
				switch (VECTOR_LENGTH(BgL_opt1272z00_101))
					{
					case 2L:

						{	/* Llib/thread.scm 711 */

							{	/* Llib/thread.scm 711 */
								obj_t BgL_cz00_4048;
								obj_t BgL_mz00_4049;

								if (BGL_CONDVARP(BgL_g1274z00_4046))
									{	/* Llib/thread.scm 711 */
										BgL_cz00_4048 = BgL_g1274z00_4046;
									}
								else
									{
										obj_t BgL_auxz00_5431;

										BgL_auxz00_5431 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2151z00zz__threadz00, BINT(31482L),
											BGl_string2179z00zz__threadz00,
											BGl_string2178z00zz__threadz00, BgL_g1274z00_4046);
										FAILURE(BgL_auxz00_5431, BFALSE, BFALSE);
									}
								if (BGL_MUTEXP(BgL_g1275z00_4047))
									{	/* Llib/thread.scm 711 */
										BgL_mz00_4049 = BgL_g1275z00_4047;
									}
								else
									{
										obj_t BgL_auxz00_5437;

										BgL_auxz00_5437 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2151z00zz__threadz00, BINT(31482L),
											BGl_string2179z00zz__threadz00,
											BGl_string2165z00zz__threadz00, BgL_g1275z00_4047);
										FAILURE(BgL_auxz00_5437, BFALSE, BFALSE);
									}
								return BBOOL(BGL_CONDVAR_WAIT(BgL_cz00_4048, BgL_mz00_4049));
							}
						}
						break;
					case 3L:

						{	/* Llib/thread.scm 711 */
							obj_t BgL_timeoutz00_4050;

							BgL_timeoutz00_4050 = VECTOR_REF(BgL_opt1272z00_101, 2L);
							{	/* Llib/thread.scm 711 */

								{	/* Llib/thread.scm 711 */
									obj_t BgL_cz00_4051;
									obj_t BgL_mz00_4052;
									long BgL_timeoutz00_4053;

									if (BGL_CONDVARP(BgL_g1274z00_4046))
										{	/* Llib/thread.scm 711 */
											BgL_cz00_4051 = BgL_g1274z00_4046;
										}
									else
										{
											obj_t BgL_auxz00_5446;

											BgL_auxz00_5446 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2151z00zz__threadz00, BINT(31482L),
												BGl_string2179z00zz__threadz00,
												BGl_string2178z00zz__threadz00, BgL_g1274z00_4046);
											FAILURE(BgL_auxz00_5446, BFALSE, BFALSE);
										}
									if (BGL_MUTEXP(BgL_g1275z00_4047))
										{	/* Llib/thread.scm 711 */
											BgL_mz00_4052 = BgL_g1275z00_4047;
										}
									else
										{
											obj_t BgL_auxz00_5452;

											BgL_auxz00_5452 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2151z00zz__threadz00, BINT(31482L),
												BGl_string2179z00zz__threadz00,
												BGl_string2165z00zz__threadz00, BgL_g1275z00_4047);
											FAILURE(BgL_auxz00_5452, BFALSE, BFALSE);
										}
									{	/* Llib/thread.scm 711 */
										obj_t BgL_tmpz00_5456;

										if (INTEGERP(BgL_timeoutz00_4050))
											{	/* Llib/thread.scm 711 */
												BgL_tmpz00_5456 = BgL_timeoutz00_4050;
											}
										else
											{
												obj_t BgL_auxz00_5459;

												BgL_auxz00_5459 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2151z00zz__threadz00, BINT(31482L),
													BGl_string2179z00zz__threadz00,
													BGl_string2170z00zz__threadz00, BgL_timeoutz00_4050);
												FAILURE(BgL_auxz00_5459, BFALSE, BFALSE);
											}
										BgL_timeoutz00_4053 = (long) CINT(BgL_tmpz00_5456);
									}
									if ((BgL_timeoutz00_4053 == 0L))
										{	/* Llib/thread.scm 712 */
											return
												BBOOL(BGL_CONDVAR_WAIT(BgL_cz00_4051, BgL_mz00_4052));
										}
									else
										{	/* Llib/thread.scm 712 */
											return
												BBOOL(BGL_CONDVAR_TIMED_WAIT(BgL_cz00_4051,
													BgL_mz00_4052, BgL_timeoutz00_4053));
										}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* condition-variable-wait! */
	BGL_EXPORTED_DEF bool_t
		BGl_conditionzd2variablezd2waitz12z12zz__threadz00(obj_t BgL_cz00_98,
		obj_t BgL_mz00_99, long BgL_timeoutz00_100)
	{
		{	/* Llib/thread.scm 711 */
			if ((BgL_timeoutz00_100 == 0L))
				{	/* Llib/thread.scm 712 */
					return BGL_CONDVAR_WAIT(BgL_cz00_98, BgL_mz00_99);
				}
			else
				{	/* Llib/thread.scm 712 */
					return
						BGL_CONDVAR_TIMED_WAIT(BgL_cz00_98, BgL_mz00_99,
						BgL_timeoutz00_100);
				}
		}

	}



/* condition-variable-signal! */
	BGL_EXPORTED_DEF bool_t
		BGl_conditionzd2variablezd2signalz12z12zz__threadz00(obj_t BgL_cz00_103)
	{
		{	/* Llib/thread.scm 719 */
			return BGL_CONDVAR_SIGNAL(BgL_cz00_103);
		}

	}



/* &condition-variable-signal! */
	obj_t BGl_z62conditionzd2variablezd2signalz12z70zz__threadz00(obj_t
		BgL_envz00_3499, obj_t BgL_cz00_3500)
	{
		{	/* Llib/thread.scm 719 */
			{	/* Llib/thread.scm 720 */
				bool_t BgL_tmpz00_5477;

				{	/* Llib/thread.scm 720 */
					obj_t BgL_auxz00_5478;

					if (BGL_CONDVARP(BgL_cz00_3500))
						{	/* Llib/thread.scm 720 */
							BgL_auxz00_5478 = BgL_cz00_3500;
						}
					else
						{
							obj_t BgL_auxz00_5481;

							BgL_auxz00_5481 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
								BINT(31924L), BGl_string2180z00zz__threadz00,
								BGl_string2178z00zz__threadz00, BgL_cz00_3500);
							FAILURE(BgL_auxz00_5481, BFALSE, BFALSE);
						}
					BgL_tmpz00_5477 =
						BGl_conditionzd2variablezd2signalz12z12zz__threadz00
						(BgL_auxz00_5478);
				}
				return BBOOL(BgL_tmpz00_5477);
			}
		}

	}



/* condition-variable-broadcast! */
	BGL_EXPORTED_DEF bool_t
		BGl_conditionzd2variablezd2broadcastz12z12zz__threadz00(obj_t BgL_cz00_104)
	{
		{	/* Llib/thread.scm 725 */
			return BGL_CONDVAR_BROADCAST(BgL_cz00_104);
		}

	}



/* &condition-variable-broadcast! */
	obj_t BGl_z62conditionzd2variablezd2broadcastz12z70zz__threadz00(obj_t
		BgL_envz00_3501, obj_t BgL_cz00_3502)
	{
		{	/* Llib/thread.scm 725 */
			{	/* Llib/thread.scm 726 */
				bool_t BgL_tmpz00_5488;

				{	/* Llib/thread.scm 726 */
					obj_t BgL_auxz00_5489;

					if (BGL_CONDVARP(BgL_cz00_3502))
						{	/* Llib/thread.scm 726 */
							BgL_auxz00_5489 = BgL_cz00_3502;
						}
					else
						{
							obj_t BgL_auxz00_5492;

							BgL_auxz00_5492 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
								BINT(32221L), BGl_string2181z00zz__threadz00,
								BGl_string2178z00zz__threadz00, BgL_cz00_3502);
							FAILURE(BgL_auxz00_5492, BFALSE, BFALSE);
						}
					BgL_tmpz00_5488 =
						BGl_conditionzd2variablezd2broadcastz12z12zz__threadz00
						(BgL_auxz00_5489);
				}
				return BBOOL(BgL_tmpz00_5488);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__threadz00(void)
	{
		{	/* Llib/thread.scm 17 */
			{	/* Llib/thread.scm 161 */
				obj_t BgL_arg1350z00_1303;
				obj_t BgL_arg1351z00_1304;

				{	/* Llib/thread.scm 161 */
					obj_t BgL_v1162z00_1315;

					BgL_v1162z00_1315 = create_vector(1L);
					VECTOR_SET(BgL_v1162z00_1315, 0L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2184z00zz__threadz00, BGl_proc2183z00zz__threadz00,
							BGl_proc2182z00zz__threadz00, ((bool_t) 1), ((bool_t) 0), BFALSE,
							BFALSE, BGl_symbol2186z00zz__threadz00));
					BgL_arg1350z00_1303 = BgL_v1162z00_1315;
				}
				{	/* Llib/thread.scm 161 */
					obj_t BgL_v1163z00_1326;

					BgL_v1163z00_1326 = create_vector(0L);
					BgL_arg1351z00_1304 = BgL_v1163z00_1326;
				}
				BGl_threadzd2backendzd2zz__threadz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol2190z00zz__threadz00, BGl_symbol2191z00zz__threadz00,
					BGl_objectz00zz__objectz00, 20365L, BGl_proc2189z00zz__threadz00,
					BGl_proc2188z00zz__threadz00, BFALSE, BGl_proc2187z00zz__threadz00,
					BFALSE, BgL_arg1350z00_1303, BgL_arg1351z00_1304);
			}
			{	/* Llib/thread.scm 164 */
				obj_t BgL_arg1365z00_1332;
				obj_t BgL_arg1366z00_1333;

				{	/* Llib/thread.scm 164 */
					obj_t BgL_v1164z00_1339;

					BgL_v1164z00_1339 = create_vector(3L);
					VECTOR_SET(BgL_v1164z00_1339, 0L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2184z00zz__threadz00, BGl_proc2195z00zz__threadz00,
							BGl_proc2194z00zz__threadz00, ((bool_t) 0), ((bool_t) 0), BFALSE,
							BGl_proc2193z00zz__threadz00, BGl_symbol2196z00zz__threadz00));
					VECTOR_SET(BgL_v1164z00_1339, 1L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2200z00zz__threadz00, BGl_proc2199z00zz__threadz00,
							BGl_proc2198z00zz__threadz00, ((bool_t) 0), ((bool_t) 1), BFALSE,
							BFALSE, BGl_symbol2196z00zz__threadz00));
					VECTOR_SET(BgL_v1164z00_1339, 2L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2204z00zz__threadz00, BGl_proc2203z00zz__threadz00,
							BGl_proc2202z00zz__threadz00, ((bool_t) 0), ((bool_t) 1), BFALSE,
							BFALSE, BGl_symbol2196z00zz__threadz00));
					BgL_arg1365z00_1332 = BgL_v1164z00_1339;
				}
				{	/* Llib/thread.scm 164 */
					obj_t BgL_v1165z00_1373;

					BgL_v1165z00_1373 = create_vector(2L);
					{	/* Llib/thread.scm 167 */
						obj_t BgL_arg1387z00_1374;

						{	/* Llib/thread.scm 167 */
							obj_t BgL_arg1388z00_1375;

							BgL_arg1388z00_1375 =
								MAKE_YOUNG_PAIR(BGl_proc2206z00zz__threadz00,
								BGl_proc2207z00zz__threadz00);
							BgL_arg1387z00_1374 =
								MAKE_YOUNG_PAIR(BINT(0L), BgL_arg1388z00_1375);
						}
						VECTOR_SET(BgL_v1165z00_1373, 0L, BgL_arg1387z00_1374);
					}
					{	/* Llib/thread.scm 168 */
						obj_t BgL_arg1395z00_1391;

						{	/* Llib/thread.scm 168 */
							obj_t BgL_arg1396z00_1392;

							BgL_arg1396z00_1392 =
								MAKE_YOUNG_PAIR(BGl_proc2208z00zz__threadz00,
								BGl_proc2209z00zz__threadz00);
							BgL_arg1395z00_1391 =
								MAKE_YOUNG_PAIR(BINT(1L), BgL_arg1396z00_1392);
						}
						VECTOR_SET(BgL_v1165z00_1373, 1L, BgL_arg1395z00_1391);
					}
					BgL_arg1366z00_1333 = BgL_v1165z00_1373;
				}
				BGl_threadz00zz__threadz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol2157z00zz__threadz00, BGl_symbol2191z00zz__threadz00,
					BGl_objectz00zz__objectz00, 35587L, BFALSE,
					BGl_proc2211z00zz__threadz00,
					BGl_threadzd2initializa7ez12zd2envzb5zz__threadz00,
					BGl_proc2210z00zz__threadz00, BFALSE, BgL_arg1365z00_1332,
					BgL_arg1366z00_1333);
			}
			{	/* Llib/thread.scm 170 */
				obj_t BgL_arg1407z00_1413;
				obj_t BgL_arg1408z00_1414;

				{	/* Llib/thread.scm 170 */
					obj_t BgL_v1166z00_1432;

					BgL_v1166z00_1432 = create_vector(6L);
					VECTOR_SET(BgL_v1166z00_1432, 0L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2214z00zz__threadz00, BGl_proc2213z00zz__threadz00,
							BGl_proc2212z00zz__threadz00, ((bool_t) 1), ((bool_t) 0), BFALSE,
							BFALSE, BGl_symbol2216z00zz__threadz00));
					VECTOR_SET(BgL_v1166z00_1432, 1L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2220z00zz__threadz00, BGl_proc2219z00zz__threadz00,
							BGl_proc2218z00zz__threadz00, ((bool_t) 0), ((bool_t) 0), BFALSE,
							BGl_proc2217z00zz__threadz00, BGl_symbol2196z00zz__threadz00));
					VECTOR_SET(BgL_v1166z00_1432, 2L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2225z00zz__threadz00, BGl_proc2224z00zz__threadz00,
							BGl_proc2223z00zz__threadz00, ((bool_t) 0), ((bool_t) 0), BFALSE,
							BGl_proc2222z00zz__threadz00, BGl_symbol2196z00zz__threadz00));
					VECTOR_SET(BgL_v1166z00_1432, 3L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2230z00zz__threadz00, BGl_proc2229z00zz__threadz00,
							BGl_proc2228z00zz__threadz00, ((bool_t) 0), ((bool_t) 0), BFALSE,
							BGl_proc2227z00zz__threadz00, BGl_symbol2196z00zz__threadz00));
					VECTOR_SET(BgL_v1166z00_1432, 4L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2235z00zz__threadz00, BGl_proc2234z00zz__threadz00,
							BGl_proc2233z00zz__threadz00, ((bool_t) 0), ((bool_t) 0), BFALSE,
							BGl_proc2232z00zz__threadz00, BGl_symbol2196z00zz__threadz00));
					VECTOR_SET(BgL_v1166z00_1432, 5L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2240z00zz__threadz00, BGl_proc2239z00zz__threadz00,
							BGl_proc2238z00zz__threadz00, ((bool_t) 0), ((bool_t) 0), BFALSE,
							BGl_proc2237z00zz__threadz00, BGl_symbol2186z00zz__threadz00));
					BgL_arg1407z00_1413 = BgL_v1166z00_1432;
				}
				{	/* Llib/thread.scm 170 */
					obj_t BgL_v1167z00_1508;

					BgL_v1167z00_1508 = create_vector(0L);
					BgL_arg1408z00_1414 = BgL_v1167z00_1508;
				}
				BGl_nothreadz00zz__threadz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol2245z00zz__threadz00, BGl_symbol2191z00zz__threadz00,
					BGl_threadz00zz__threadz00, 50609L, BGl_proc2244z00zz__threadz00,
					BGl_proc2243z00zz__threadz00,
					BGl_threadzd2initializa7ez12zd2envzb5zz__threadz00,
					BGl_proc2242z00zz__threadz00, BFALSE, BgL_arg1407z00_1413,
					BgL_arg1408z00_1414);
			}
			{	/* Llib/thread.scm 159 */
				obj_t BgL_arg1458z00_1515;
				obj_t BgL_arg1459z00_1516;

				{	/* Llib/thread.scm 159 */
					obj_t BgL_v1168z00_1527;

					BgL_v1168z00_1527 = create_vector(0L);
					BgL_arg1458z00_1515 = BgL_v1168z00_1527;
				}
				{	/* Llib/thread.scm 159 */
					obj_t BgL_v1169z00_1528;

					BgL_v1169z00_1528 = create_vector(0L);
					BgL_arg1459z00_1516 = BgL_v1169z00_1528;
				}
				return (BGl_nothreadzd2backendzd2zz__threadz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol2249z00zz__threadz00, BGl_symbol2191z00zz__threadz00,
						BGl_threadzd2backendzd2zz__threadz00, 1284L,
						BGl_proc2248z00zz__threadz00, BGl_proc2247z00zz__threadz00, BFALSE,
						BGl_proc2246z00zz__threadz00, BFALSE, BgL_arg1458z00_1515,
						BgL_arg1459z00_1516), BUNSPEC);
			}
		}

	}



/* &<@anonymous:1464> */
	obj_t BGl_z62zc3z04anonymousza31464ze3ze5zz__threadz00(obj_t BgL_envz00_3547,
		obj_t BgL_new1056z00_3548)
	{
		{	/* Llib/thread.scm 159 */
			{
				BgL_nothreadzd2backendzd2_bglt BgL_auxz00_5538;

				((((BgL_threadzd2backendzd2_bglt) COBJECT(
								((BgL_threadzd2backendzd2_bglt)
									((BgL_nothreadzd2backendzd2_bglt) BgL_new1056z00_3548))))->
						BgL_namez00) = ((obj_t) BGl_string2251z00zz__threadz00), BUNSPEC);
				BgL_auxz00_5538 =
					((BgL_nothreadzd2backendzd2_bglt) BgL_new1056z00_3548);
				return ((obj_t) BgL_auxz00_5538);
			}
		}

	}



/* &lambda1462 */
	BgL_nothreadzd2backendzd2_bglt BGl_z62lambda1462z62zz__threadz00(obj_t
		BgL_envz00_3549)
	{
		{	/* Llib/thread.scm 159 */
			{	/* Llib/thread.scm 159 */
				BgL_nothreadzd2backendzd2_bglt BgL_new1055z00_4055;

				BgL_new1055z00_4055 =
					((BgL_nothreadzd2backendzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_nothreadzd2backendzd2_bgl))));
				{	/* Llib/thread.scm 159 */
					long BgL_arg1463z00_4056;

					BgL_arg1463z00_4056 =
						BGL_CLASS_NUM(BGl_nothreadzd2backendzd2zz__threadz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1055z00_4055), BgL_arg1463z00_4056);
				}
				return BgL_new1055z00_4055;
			}
		}

	}



/* &lambda1460 */
	BgL_nothreadzd2backendzd2_bglt BGl_z62lambda1460z62zz__threadz00(obj_t
		BgL_envz00_3550, obj_t BgL_name1054z00_3551)
	{
		{	/* Llib/thread.scm 159 */
			{	/* Llib/thread.scm 159 */
				BgL_nothreadzd2backendzd2_bglt BgL_new1118z00_4058;

				{	/* Llib/thread.scm 159 */
					BgL_nothreadzd2backendzd2_bglt BgL_new1116z00_4059;

					BgL_new1116z00_4059 =
						((BgL_nothreadzd2backendzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_nothreadzd2backendzd2_bgl))));
					{	/* Llib/thread.scm 159 */
						long BgL_arg1461z00_4060;

						BgL_arg1461z00_4060 =
							BGL_CLASS_NUM(BGl_nothreadzd2backendzd2zz__threadz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1116z00_4059), BgL_arg1461z00_4060);
					}
					BgL_new1118z00_4058 = BgL_new1116z00_4059;
				}
				((((BgL_threadzd2backendzd2_bglt) COBJECT(
								((BgL_threadzd2backendzd2_bglt) BgL_new1118z00_4058)))->
						BgL_namez00) = ((obj_t) ((obj_t) BgL_name1054z00_3551)), BUNSPEC);
				return BgL_new1118z00_4058;
			}
		}

	}



/* &<@anonymous:1414> */
	obj_t BGl_z62zc3z04anonymousza31414ze3ze5zz__threadz00(obj_t BgL_envz00_3552,
		obj_t BgL_new1052z00_3553)
	{
		{	/* Llib/thread.scm 170 */
			{
				BgL_nothreadz00_bglt BgL_auxz00_5555;

				((((BgL_threadz00_bglt) COBJECT(
								((BgL_threadz00_bglt)
									((BgL_nothreadz00_bglt) BgL_new1052z00_3553))))->
						BgL_namez00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nothreadz00_bglt) COBJECT(((BgL_nothreadz00_bglt)
									BgL_new1052z00_3553)))->BgL_bodyz00) =
					((obj_t) BGl_conszd2envzd2zz__r4_pairs_and_lists_6_3z00), BUNSPEC);
				((((BgL_nothreadz00_bglt) COBJECT(((BgL_nothreadz00_bglt)
									BgL_new1052z00_3553)))->BgL_z52specificz52) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nothreadz00_bglt) COBJECT(((BgL_nothreadz00_bglt)
									BgL_new1052z00_3553)))->BgL_z52cleanupz52) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nothreadz00_bglt) COBJECT(((BgL_nothreadz00_bglt)
									BgL_new1052z00_3553)))->BgL_endzd2resultzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nothreadz00_bglt) COBJECT(((BgL_nothreadz00_bglt)
									BgL_new1052z00_3553)))->BgL_endzd2exceptionzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nothreadz00_bglt) COBJECT(((BgL_nothreadz00_bglt)
									BgL_new1052z00_3553)))->BgL_z52namez52) =
					((obj_t) BGl_string2251z00zz__threadz00), BUNSPEC);
				BgL_auxz00_5555 = ((BgL_nothreadz00_bglt) BgL_new1052z00_3553);
				return ((obj_t) BgL_auxz00_5555);
			}
		}

	}



/* &lambda1412 */
	BgL_nothreadz00_bglt BGl_z62lambda1412z62zz__threadz00(obj_t BgL_envz00_3554)
	{
		{	/* Llib/thread.scm 170 */
			{	/* Llib/thread.scm 170 */
				BgL_nothreadz00_bglt BgL_new1051z00_4062;

				BgL_new1051z00_4062 =
					((BgL_nothreadz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_nothreadz00_bgl))));
				{	/* Llib/thread.scm 170 */
					long BgL_arg1413z00_4063;

					BgL_arg1413z00_4063 = BGL_CLASS_NUM(BGl_nothreadz00zz__threadz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1051z00_4062), BgL_arg1413z00_4063);
				}
				return BgL_new1051z00_4062;
			}
		}

	}



/* &lambda1409 */
	BgL_nothreadz00_bglt BGl_z62lambda1409z62zz__threadz00(obj_t BgL_envz00_3555,
		obj_t BgL_name1044z00_3556, obj_t BgL_body1045z00_3557,
		obj_t BgL_z52specific1046z52_3558, obj_t BgL_z52cleanup1047z52_3559,
		obj_t BgL_endzd2result1048zd2_3560, obj_t BgL_endzd2exception1049zd2_3561,
		obj_t BgL_z52name1050z52_3562)
	{
		{	/* Llib/thread.scm 170 */
			{	/* Llib/thread.scm 170 */
				BgL_nothreadz00_bglt BgL_new1115z00_4066;

				{	/* Llib/thread.scm 170 */
					BgL_nothreadz00_bglt BgL_new1114z00_4067;

					BgL_new1114z00_4067 =
						((BgL_nothreadz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_nothreadz00_bgl))));
					{	/* Llib/thread.scm 170 */
						long BgL_arg1411z00_4068;

						BgL_arg1411z00_4068 = BGL_CLASS_NUM(BGl_nothreadz00zz__threadz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1114z00_4067), BgL_arg1411z00_4068);
					}
					BgL_new1115z00_4066 = BgL_new1114z00_4067;
				}
				((((BgL_threadz00_bglt) COBJECT(
								((BgL_threadz00_bglt) BgL_new1115z00_4066)))->BgL_namez00) =
					((obj_t) BgL_name1044z00_3556), BUNSPEC);
				((((BgL_nothreadz00_bglt) COBJECT(BgL_new1115z00_4066))->BgL_bodyz00) =
					((obj_t) ((obj_t) BgL_body1045z00_3557)), BUNSPEC);
				((((BgL_nothreadz00_bglt) COBJECT(BgL_new1115z00_4066))->
						BgL_z52specificz52) =
					((obj_t) BgL_z52specific1046z52_3558), BUNSPEC);
				((((BgL_nothreadz00_bglt) COBJECT(BgL_new1115z00_4066))->
						BgL_z52cleanupz52) = ((obj_t) BgL_z52cleanup1047z52_3559), BUNSPEC);
				((((BgL_nothreadz00_bglt) COBJECT(BgL_new1115z00_4066))->
						BgL_endzd2resultzd2) =
					((obj_t) BgL_endzd2result1048zd2_3560), BUNSPEC);
				((((BgL_nothreadz00_bglt) COBJECT(BgL_new1115z00_4066))->
						BgL_endzd2exceptionzd2) =
					((obj_t) BgL_endzd2exception1049zd2_3561), BUNSPEC);
				((((BgL_nothreadz00_bglt) COBJECT(BgL_new1115z00_4066))->
						BgL_z52namez52) =
					((obj_t) ((obj_t) BgL_z52name1050z52_3562)), BUNSPEC);
				{	/* Llib/thread.scm 170 */
					obj_t BgL_fun1410z00_4069;

					BgL_fun1410z00_4069 =
						BGl_classzd2constructorzd2zz__objectz00
						(BGl_nothreadz00zz__threadz00);
					BGL_PROCEDURE_CALL1(BgL_fun1410z00_4069,
						((obj_t) BgL_new1115z00_4066));
				}
				return BgL_new1115z00_4066;
			}
		}

	}



/* &<@anonymous:1454> */
	obj_t BGl_z62zc3z04anonymousza31454ze3ze5zz__threadz00(obj_t BgL_envz00_3563)
	{
		{	/* Llib/thread.scm 170 */
			return BGl_string2252z00zz__threadz00;
		}

	}



/* &lambda1453 */
	obj_t BGl_z62lambda1453z62zz__threadz00(obj_t BgL_envz00_3564,
		obj_t BgL_oz00_3565, obj_t BgL_vz00_3566)
	{
		{	/* Llib/thread.scm 170 */
			return
				((((BgL_nothreadz00_bglt) COBJECT(
							((BgL_nothreadz00_bglt) BgL_oz00_3565)))->BgL_z52namez52) =
				((obj_t) ((obj_t) BgL_vz00_3566)), BUNSPEC);
		}

	}



/* &lambda1452 */
	obj_t BGl_z62lambda1452z62zz__threadz00(obj_t BgL_envz00_3567,
		obj_t BgL_oz00_3568)
	{
		{	/* Llib/thread.scm 170 */
			return
				(((BgL_nothreadz00_bglt) COBJECT(
						((BgL_nothreadz00_bglt) BgL_oz00_3568)))->BgL_z52namez52);
		}

	}



/* &<@anonymous:1447> */
	obj_t BGl_z62zc3z04anonymousza31447ze3ze5zz__threadz00(obj_t BgL_envz00_3569)
	{
		{	/* Llib/thread.scm 170 */
			return BUNSPEC;
		}

	}



/* &lambda1446 */
	obj_t BGl_z62lambda1446z62zz__threadz00(obj_t BgL_envz00_3570,
		obj_t BgL_oz00_3571, obj_t BgL_vz00_3572)
	{
		{	/* Llib/thread.scm 170 */
			return
				((((BgL_nothreadz00_bglt) COBJECT(
							((BgL_nothreadz00_bglt) BgL_oz00_3571)))->
					BgL_endzd2exceptionzd2) = ((obj_t) BgL_vz00_3572), BUNSPEC);
		}

	}



/* &lambda1445 */
	obj_t BGl_z62lambda1445z62zz__threadz00(obj_t BgL_envz00_3573,
		obj_t BgL_oz00_3574)
	{
		{	/* Llib/thread.scm 170 */
			return
				(((BgL_nothreadz00_bglt) COBJECT(
						((BgL_nothreadz00_bglt) BgL_oz00_3574)))->BgL_endzd2exceptionzd2);
		}

	}



/* &<@anonymous:1440> */
	obj_t BGl_z62zc3z04anonymousza31440ze3ze5zz__threadz00(obj_t BgL_envz00_3575)
	{
		{	/* Llib/thread.scm 170 */
			return BUNSPEC;
		}

	}



/* &lambda1439 */
	obj_t BGl_z62lambda1439z62zz__threadz00(obj_t BgL_envz00_3576,
		obj_t BgL_oz00_3577, obj_t BgL_vz00_3578)
	{
		{	/* Llib/thread.scm 170 */
			return
				((((BgL_nothreadz00_bglt) COBJECT(
							((BgL_nothreadz00_bglt) BgL_oz00_3577)))->BgL_endzd2resultzd2) =
				((obj_t) BgL_vz00_3578), BUNSPEC);
		}

	}



/* &lambda1438 */
	obj_t BGl_z62lambda1438z62zz__threadz00(obj_t BgL_envz00_3579,
		obj_t BgL_oz00_3580)
	{
		{	/* Llib/thread.scm 170 */
			return
				(((BgL_nothreadz00_bglt) COBJECT(
						((BgL_nothreadz00_bglt) BgL_oz00_3580)))->BgL_endzd2resultzd2);
		}

	}



/* &<@anonymous:1433> */
	obj_t BGl_z62zc3z04anonymousza31433ze3ze5zz__threadz00(obj_t BgL_envz00_3581)
	{
		{	/* Llib/thread.scm 170 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1432 */
	obj_t BGl_z62lambda1432z62zz__threadz00(obj_t BgL_envz00_3582,
		obj_t BgL_oz00_3583, obj_t BgL_vz00_3584)
	{
		{	/* Llib/thread.scm 170 */
			return
				((((BgL_nothreadz00_bglt) COBJECT(
							((BgL_nothreadz00_bglt) BgL_oz00_3583)))->BgL_z52cleanupz52) =
				((obj_t) BgL_vz00_3584), BUNSPEC);
		}

	}



/* &lambda1431 */
	obj_t BGl_z62lambda1431z62zz__threadz00(obj_t BgL_envz00_3585,
		obj_t BgL_oz00_3586)
	{
		{	/* Llib/thread.scm 170 */
			return
				(((BgL_nothreadz00_bglt) COBJECT(
						((BgL_nothreadz00_bglt) BgL_oz00_3586)))->BgL_z52cleanupz52);
		}

	}



/* &<@anonymous:1426> */
	obj_t BGl_z62zc3z04anonymousza31426ze3ze5zz__threadz00(obj_t BgL_envz00_3587)
	{
		{	/* Llib/thread.scm 170 */
			return BUNSPEC;
		}

	}



/* &lambda1425 */
	obj_t BGl_z62lambda1425z62zz__threadz00(obj_t BgL_envz00_3588,
		obj_t BgL_oz00_3589, obj_t BgL_vz00_3590)
	{
		{	/* Llib/thread.scm 170 */
			return
				((((BgL_nothreadz00_bglt) COBJECT(
							((BgL_nothreadz00_bglt) BgL_oz00_3589)))->BgL_z52specificz52) =
				((obj_t) BgL_vz00_3590), BUNSPEC);
		}

	}



/* &lambda1424 */
	obj_t BGl_z62lambda1424z62zz__threadz00(obj_t BgL_envz00_3591,
		obj_t BgL_oz00_3592)
	{
		{	/* Llib/thread.scm 170 */
			return
				(((BgL_nothreadz00_bglt) COBJECT(
						((BgL_nothreadz00_bglt) BgL_oz00_3592)))->BgL_z52specificz52);
		}

	}



/* &lambda1419 */
	obj_t BGl_z62lambda1419z62zz__threadz00(obj_t BgL_envz00_3593,
		obj_t BgL_oz00_3594, obj_t BgL_vz00_3595)
	{
		{	/* Llib/thread.scm 170 */
			return
				((((BgL_nothreadz00_bglt) COBJECT(
							((BgL_nothreadz00_bglt) BgL_oz00_3594)))->BgL_bodyz00) = ((obj_t)
					((obj_t) BgL_vz00_3595)), BUNSPEC);
		}

	}



/* &lambda1418 */
	obj_t BGl_z62lambda1418z62zz__threadz00(obj_t BgL_envz00_3596,
		obj_t BgL_oz00_3597)
	{
		{	/* Llib/thread.scm 170 */
			return
				(((BgL_nothreadz00_bglt) COBJECT(
						((BgL_nothreadz00_bglt) BgL_oz00_3597)))->BgL_bodyz00);
		}

	}



/* &<@anonymous:1369> */
	obj_t BGl_z62zc3z04anonymousza31369ze3ze5zz__threadz00(obj_t BgL_envz00_3598,
		obj_t BgL_new1042z00_3599)
	{
		{	/* Llib/thread.scm 164 */
			{
				BgL_threadz00_bglt BgL_auxz00_5624;

				((((BgL_threadz00_bglt) COBJECT(
								((BgL_threadz00_bglt) BgL_new1042z00_3599)))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_5624 = ((BgL_threadz00_bglt) BgL_new1042z00_3599);
				return ((obj_t) BgL_auxz00_5624);
			}
		}

	}



/* &lambda1367 */
	BgL_threadz00_bglt BGl_z62lambda1367z62zz__threadz00(obj_t BgL_envz00_3600)
	{
		{	/* Llib/thread.scm 164 */
			{	/* Llib/thread.scm 164 */
				BgL_threadz00_bglt BgL_new1041z00_4085;

				BgL_new1041z00_4085 =
					((BgL_threadz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_threadz00_bgl))));
				{	/* Llib/thread.scm 164 */
					long BgL_arg1368z00_4086;

					BgL_arg1368z00_4086 = BGL_CLASS_NUM(BGl_threadz00zz__threadz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1041z00_4085), BgL_arg1368z00_4086);
				}
				return BgL_new1041z00_4085;
			}
		}

	}



/* &<@anonymous:1402> */
	obj_t BGl_z62zc3z04anonymousza31402ze3ze5zz__threadz00(obj_t BgL_envz00_3601,
		obj_t BgL_oz00_3602, obj_t BgL_vz00_3603)
	{
		{	/* Llib/thread.scm 168 */
			return
				BGl_threadzd2cleanupzd2setz12z12zz__threadz00(
				((BgL_threadz00_bglt) BgL_oz00_3602), BgL_vz00_3603);
		}

	}



/* &<@anonymous:1400> */
	obj_t BGl_z62zc3z04anonymousza31400ze3ze5zz__threadz00(obj_t BgL_envz00_3604,
		obj_t BgL_oz00_3605)
	{
		{	/* Llib/thread.scm 168 */
			return
				BGl_threadzd2cleanupzd2zz__threadz00(
				((BgL_threadz00_bglt) BgL_oz00_3605));
		}

	}



/* &<@anonymous:1393> */
	obj_t BGl_z62zc3z04anonymousza31393ze3ze5zz__threadz00(obj_t BgL_envz00_3606,
		obj_t BgL_oz00_3607, obj_t BgL_vz00_3608)
	{
		{	/* Llib/thread.scm 167 */
			return
				BGl_threadzd2specificzd2setz12z12zz__threadz00(
				((BgL_threadz00_bglt) BgL_oz00_3607), BgL_vz00_3608);
		}

	}



/* &<@anonymous:1391> */
	obj_t BGl_z62zc3z04anonymousza31391ze3ze5zz__threadz00(obj_t BgL_envz00_3609,
		obj_t BgL_oz00_3610)
	{
		{	/* Llib/thread.scm 167 */
			return
				BGl_threadzd2specificzd2zz__threadz00(
				((BgL_threadz00_bglt) BgL_oz00_3610));
		}

	}



/* &<@anonymous:1386> */
	obj_t BGl_z62zc3z04anonymousza31386ze3ze5zz__threadz00(obj_t BgL_envz00_3611,
		obj_t BgL_oz00_3612, obj_t BgL_vz00_3613)
	{
		{	/* Llib/thread.scm 164 */
			return
				BGl_callzd2virtualzd2setterz00zz__objectz00(
				((BgL_objectz00_bglt) BgL_oz00_3612), (int) (1L), BgL_vz00_3613);
		}

	}



/* &<@anonymous:1385> */
	obj_t BGl_z62zc3z04anonymousza31385ze3ze5zz__threadz00(obj_t BgL_envz00_3614,
		obj_t BgL_oz00_3615)
	{
		{	/* Llib/thread.scm 164 */
			return
				BGl_callzd2virtualzd2getterz00zz__objectz00(
				((BgL_objectz00_bglt) BgL_oz00_3615), (int) (1L));
		}

	}



/* &<@anonymous:1381> */
	obj_t BGl_z62zc3z04anonymousza31381ze3ze5zz__threadz00(obj_t BgL_envz00_3616,
		obj_t BgL_oz00_3617, obj_t BgL_vz00_3618)
	{
		{	/* Llib/thread.scm 164 */
			return
				BGl_callzd2virtualzd2setterz00zz__objectz00(
				((BgL_objectz00_bglt) BgL_oz00_3617), (int) (0L), BgL_vz00_3618);
		}

	}



/* &<@anonymous:1380> */
	obj_t BGl_z62zc3z04anonymousza31380ze3ze5zz__threadz00(obj_t BgL_envz00_3619,
		obj_t BgL_oz00_3620)
	{
		{	/* Llib/thread.scm 164 */
			return
				BGl_callzd2virtualzd2getterz00zz__objectz00(
				((BgL_objectz00_bglt) BgL_oz00_3620), (int) (0L));
		}

	}



/* &<@anonymous:1376> */
	obj_t BGl_z62zc3z04anonymousza31376ze3ze5zz__threadz00(obj_t BgL_envz00_3621)
	{
		{	/* Llib/thread.scm 164 */
			return BGl_gensymz00zz__r4_symbols_6_4z00(BGl_symbol2157z00zz__threadz00);
		}

	}



/* &lambda1375 */
	obj_t BGl_z62lambda1375z62zz__threadz00(obj_t BgL_envz00_3622,
		obj_t BgL_oz00_3623, obj_t BgL_vz00_3624)
	{
		{	/* Llib/thread.scm 164 */
			return
				((((BgL_threadz00_bglt) COBJECT(
							((BgL_threadz00_bglt) BgL_oz00_3623)))->BgL_namez00) =
				((obj_t) BgL_vz00_3624), BUNSPEC);
		}

	}



/* &lambda1374 */
	obj_t BGl_z62lambda1374z62zz__threadz00(obj_t BgL_envz00_3625,
		obj_t BgL_oz00_3626)
	{
		{	/* Llib/thread.scm 164 */
			return
				(((BgL_threadz00_bglt) COBJECT(
						((BgL_threadz00_bglt) BgL_oz00_3626)))->BgL_namez00);
		}

	}



/* &<@anonymous:1357> */
	obj_t BGl_z62zc3z04anonymousza31357ze3ze5zz__threadz00(obj_t BgL_envz00_3627,
		obj_t BgL_new1039z00_3628)
	{
		{	/* Llib/thread.scm 161 */
			{
				BgL_threadzd2backendzd2_bglt BgL_auxz00_5658;

				((((BgL_threadzd2backendzd2_bglt) COBJECT(
								((BgL_threadzd2backendzd2_bglt) BgL_new1039z00_3628)))->
						BgL_namez00) = ((obj_t) BGl_string2251z00zz__threadz00), BUNSPEC);
				BgL_auxz00_5658 = ((BgL_threadzd2backendzd2_bglt) BgL_new1039z00_3628);
				return ((obj_t) BgL_auxz00_5658);
			}
		}

	}



/* &lambda1355 */
	BgL_threadzd2backendzd2_bglt BGl_z62lambda1355z62zz__threadz00(obj_t
		BgL_envz00_3629)
	{
		{	/* Llib/thread.scm 161 */
			{	/* Llib/thread.scm 161 */
				BgL_threadzd2backendzd2_bglt BgL_new1038z00_4090;

				BgL_new1038z00_4090 =
					((BgL_threadzd2backendzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_threadzd2backendzd2_bgl))));
				{	/* Llib/thread.scm 161 */
					long BgL_arg1356z00_4091;

					BgL_arg1356z00_4091 =
						BGL_CLASS_NUM(BGl_threadzd2backendzd2zz__threadz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1038z00_4090), BgL_arg1356z00_4091);
				}
				return BgL_new1038z00_4090;
			}
		}

	}



/* &lambda1352 */
	BgL_threadzd2backendzd2_bglt BGl_z62lambda1352z62zz__threadz00(obj_t
		BgL_envz00_3630, obj_t BgL_name1037z00_3631)
	{
		{	/* Llib/thread.scm 161 */
			{	/* Llib/thread.scm 161 */
				BgL_threadzd2backendzd2_bglt BgL_new1113z00_4093;

				{	/* Llib/thread.scm 161 */
					BgL_threadzd2backendzd2_bglt BgL_new1111z00_4094;

					BgL_new1111z00_4094 =
						((BgL_threadzd2backendzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_threadzd2backendzd2_bgl))));
					{	/* Llib/thread.scm 161 */
						long BgL_arg1354z00_4095;

						BgL_arg1354z00_4095 =
							BGL_CLASS_NUM(BGl_threadzd2backendzd2zz__threadz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1111z00_4094), BgL_arg1354z00_4095);
					}
					BgL_new1113z00_4093 = BgL_new1111z00_4094;
				}
				((((BgL_threadzd2backendzd2_bglt) COBJECT(BgL_new1113z00_4093))->
						BgL_namez00) = ((obj_t) ((obj_t) BgL_name1037z00_3631)), BUNSPEC);
				return BgL_new1113z00_4093;
			}
		}

	}



/* &lambda1362 */
	obj_t BGl_z62lambda1362z62zz__threadz00(obj_t BgL_envz00_3632,
		obj_t BgL_oz00_3633, obj_t BgL_vz00_3634)
	{
		{	/* Llib/thread.scm 161 */
			return
				((((BgL_threadzd2backendzd2_bglt) COBJECT(
							((BgL_threadzd2backendzd2_bglt) BgL_oz00_3633)))->BgL_namez00) =
				((obj_t) ((obj_t) BgL_vz00_3634)), BUNSPEC);
		}

	}



/* &lambda1361 */
	obj_t BGl_z62lambda1361z62zz__threadz00(obj_t BgL_envz00_3635,
		obj_t BgL_oz00_3636)
	{
		{	/* Llib/thread.scm 161 */
			return
				(((BgL_threadzd2backendzd2_bglt) COBJECT(
						((BgL_threadzd2backendzd2_bglt) BgL_oz00_3636)))->BgL_namez00);
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__threadz00(void)
	{
		{	/* Llib/thread.scm 17 */
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_tbzd2makezd2threadzd2envzd2zz__threadz00,
				BGl_proc2253z00zz__threadz00, BGl_threadzd2backendzd2zz__threadz00,
				BGl_string2254z00zz__threadz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_tbzd2currentzd2threadzd2envzd2zz__threadz00,
				BGl_proc2255z00zz__threadz00, BGl_threadzd2backendzd2zz__threadz00,
				BGl_string2256z00zz__threadz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_tbzd2mutexzd2initializa7ez12zd2envz67zz__threadz00,
				BGl_proc2257z00zz__threadz00, BGl_threadzd2backendzd2zz__threadz00,
				BGl_string2258z00zz__threadz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_tbzd2condvarzd2initializa7ez12zd2envz67zz__threadz00,
				BGl_proc2259z00zz__threadz00, BGl_threadzd2backendzd2zz__threadz00,
				BGl_string2260z00zz__threadz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_threadzd2initializa7ez12zd2envzb5zz__threadz00,
				BGl_proc2261z00zz__threadz00, BGl_threadz00zz__threadz00,
				BGl_string2262z00zz__threadz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_threadzd2startz12zd2envz12zz__threadz00,
				BGl_proc2263z00zz__threadz00, BGl_threadz00zz__threadz00,
				BGl_string2264z00zz__threadz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_threadzd2startzd2joinablez12zd2envzc0zz__threadz00,
				BGl_proc2265z00zz__threadz00, BGl_threadz00zz__threadz00,
				BGl_string2266z00zz__threadz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_threadzd2joinz12zd2envz12zz__threadz00,
				BGl_proc2267z00zz__threadz00, BGl_threadz00zz__threadz00,
				BGl_string2268z00zz__threadz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_threadzd2terminatez12zd2envz12zz__threadz00,
				BGl_proc2269z00zz__threadz00, BGl_threadz00zz__threadz00,
				BGl_string2270z00zz__threadz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_threadzd2killz12zd2envz12zz__threadz00,
				BGl_proc2271z00zz__threadz00, BGl_threadz00zz__threadz00,
				BGl_string2272z00zz__threadz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_threadzd2specificzd2envz00zz__threadz00,
				BGl_proc2273z00zz__threadz00, BGl_threadz00zz__threadz00,
				BGl_string2274z00zz__threadz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_threadzd2specificzd2setz12zd2envzc0zz__threadz00,
				BGl_proc2275z00zz__threadz00, BGl_threadz00zz__threadz00,
				BGl_string2276z00zz__threadz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_threadzd2cleanupzd2envz00zz__threadz00,
				BGl_proc2277z00zz__threadz00, BGl_threadz00zz__threadz00,
				BGl_string2278z00zz__threadz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_threadzd2cleanupzd2setz12zd2envzc0zz__threadz00,
				BGl_proc2279z00zz__threadz00, BGl_threadz00zz__threadz00,
				BGl_string2280z00zz__threadz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_threadzd2namezd2envz00zz__threadz00, BGl_proc2281z00zz__threadz00,
				BGl_threadz00zz__threadz00, BGl_string2282z00zz__threadz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_threadzd2namezd2setz12zd2envzc0zz__threadz00,
				BGl_proc2283z00zz__threadz00, BGl_threadz00zz__threadz00,
				BGl_string2284z00zz__threadz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_z52userzd2currentzd2threadzd2envz80zz__threadz00,
				BGl_proc2285z00zz__threadz00, BGl_threadz00zz__threadz00,
				BGl_string2286z00zz__threadz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_z52userzd2threadzd2sleepz12zd2envz92zz__threadz00,
				BGl_proc2287z00zz__threadz00, BGl_threadz00zz__threadz00,
				BGl_string2288z00zz__threadz00);
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_z52userzd2threadzd2yieldz12zd2envz92zz__threadz00,
				BGl_proc2289z00zz__threadz00, BGl_threadz00zz__threadz00,
				BGl_string2290z00zz__threadz00);
		}

	}



/* &%user-thread-yield!1231 */
	obj_t BGl_z62z52userzd2threadzd2yieldz121231z22zz__threadz00(obj_t
		BgL_envz00_3658, obj_t BgL_oz00_3659)
	{
		{	/* Llib/thread.scm 500 */
			return BUNSPEC;
		}

	}



/* &%user-thread-sleep!1229 */
	obj_t BGl_z62z52userzd2threadzd2sleepz121229z22zz__threadz00(obj_t
		BgL_envz00_3660, obj_t BgL_oz00_3661, obj_t BgL_dz00_3662)
	{
		{	/* Llib/thread.scm 473 */
			if (BGL_DATEP(BgL_dz00_3662))
				{	/* Llib/thread.scm 476 */
					long BgL_cdtz00_4101;

					BgL_cdtz00_4101 =
						bgl_date_to_seconds(bgl_nanoseconds_to_date(bgl_current_nanoseconds
							()));
					{	/* Llib/thread.scm 476 */
						long BgL_dtz00_4102;

						BgL_dtz00_4102 = bgl_date_to_seconds(BgL_dz00_3662);
						{	/* Llib/thread.scm 477 */
							obj_t BgL_az00_4103;

							{	/* Llib/thread.scm 478 */
								long BgL_a1151z00_4104;

								{	/* Llib/thread.scm 478 */
									long BgL_res1933z00_4105;

									{	/* Llib/thread.scm 478 */
										long BgL_tmpz00_5703;

										BgL_tmpz00_5703 = (BgL_dtz00_4102 - BgL_cdtz00_4101);
										BgL_res1933z00_4105 = (long) (BgL_tmpz00_5703);
									}
									BgL_a1151z00_4104 = BgL_res1933z00_4105;
								}
								{	/* Llib/thread.scm 478 */

									BgL_az00_4103 =
										BGl_2za2za2zz__r4_numbers_6_5z00(make_belong
										(BgL_a1151z00_4104), BINT(1000000L));
							}}
							{	/* Llib/thread.scm 478 */

								{	/* Llib/thread.scm 479 */
									bool_t BgL_test2645z00_5709;

									{	/* Llib/thread.scm 479 */
										long BgL_n1z00_4106;

										BgL_n1z00_4106 = BELONG_TO_LONG(BgL_az00_4103);
										BgL_test2645z00_5709 = (BgL_n1z00_4106 > ((long) 0));
									}
									if (BgL_test2645z00_5709)
										{	/* Llib/thread.scm 479 */
											long BgL_arg1488z00_4107;

											{	/* Llib/thread.scm 479 */
												long BgL_xz00_4108;

												BgL_xz00_4108 = BELONG_TO_LONG(BgL_az00_4103);
												BgL_arg1488z00_4107 = (long) (BgL_xz00_4108);
											}
											bgl_sleep(BgL_arg1488z00_4107);
											BUNSPEC;
											return BINT(BgL_arg1488z00_4107);
										}
									else
										{	/* Llib/thread.scm 479 */
											return BFALSE;
										}
								}
							}
						}
					}
				}
			else
				{	/* Llib/thread.scm 475 */
					if (INTEGERP(BgL_dz00_3662))
						{	/* Llib/thread.scm 481 */
							obj_t BgL_arg1492z00_4109;

							BgL_arg1492z00_4109 = BINT(((long) CINT(BgL_dz00_3662) * 1000L));
							{	/* Llib/thread.scm 481 */
								long BgL_msz00_4110;

								BgL_msz00_4110 = (long) CINT(BgL_arg1492z00_4109);
								bgl_sleep(BgL_msz00_4110);
								BUNSPEC;
								return BINT(BgL_msz00_4110);
							}
						}
					else
						{	/* Llib/thread.scm 480 */
							if (ELONGP(BgL_dz00_3662))
								{	/* Llib/thread.scm 483 */
									long BgL_arg1494z00_4111;

									{	/* Llib/thread.scm 483 */
										long BgL_a1153z00_4112;

										{	/* Llib/thread.scm 483 */
											long BgL_xz00_4113;

											BgL_xz00_4113 = BELONG_TO_LONG(BgL_dz00_3662);
											BgL_a1153z00_4112 = (long) (BgL_xz00_4113);
										}
										{	/* Llib/thread.scm 483 */

											BgL_arg1494z00_4111 = (BgL_a1153z00_4112 * 1000L);
									}}
									bgl_sleep(BgL_arg1494z00_4111);
									BUNSPEC;
									return BINT(BgL_arg1494z00_4111);
								}
							else
								{	/* Llib/thread.scm 482 */
									if (LLONGP(BgL_dz00_3662))
										{	/* Llib/thread.scm 485 */
											long BgL_arg1497z00_4114;

											{	/* Llib/thread.scm 485 */
												long BgL_a1155z00_4115;

												{	/* Llib/thread.scm 485 */
													BGL_LONGLONG_T BgL_tmpz00_5733;

													BgL_tmpz00_5733 = BLLONG_TO_LLONG(BgL_dz00_3662);
													BgL_a1155z00_4115 = LLONG_TO_LONG(BgL_tmpz00_5733);
												}
												{	/* Llib/thread.scm 485 */

													BgL_arg1497z00_4114 = (BgL_a1155z00_4115 * 1000L);
											}}
											bgl_sleep(BgL_arg1497z00_4114);
											BUNSPEC;
											return BINT(BgL_arg1497z00_4114);
										}
									else
										{	/* Llib/thread.scm 486 */
											bool_t BgL_test2649z00_5739;

											if (INTEGERP(BgL_dz00_3662))
												{	/* Llib/thread.scm 486 */
													BgL_test2649z00_5739 = ((bool_t) 1);
												}
											else
												{	/* Llib/thread.scm 486 */
													BgL_test2649z00_5739 = REALP(BgL_dz00_3662);
												}
											if (BgL_test2649z00_5739)
												{	/* Llib/thread.scm 487 */
													long BgL_arg1500z00_4116;

													{	/* Llib/thread.scm 487 */
														double BgL_arg1501z00_4117;

														if (REALP(BgL_dz00_3662))
															{	/* Llib/thread.scm 487 */
																BgL_arg1501z00_4117 =
																	(REAL_TO_DOUBLE(BgL_dz00_3662) *
																	((double) 1000000.0));
															}
														else
															{	/* Llib/thread.scm 487 */
																BgL_arg1501z00_4117 =
																	(BGl_numberzd2ze3flonumz31zz__r4_numbers_6_5z00
																	(BgL_dz00_3662) * ((double) 1000000.0));
															}
														BgL_arg1500z00_4116 = (long) (BgL_arg1501z00_4117);
													}
													bgl_sleep(BgL_arg1500z00_4116);
													BUNSPEC;
													return BINT(BgL_arg1500z00_4116);
												}
											else
												{	/* Llib/thread.scm 486 */
													return
														BGl_bigloozd2typezd2errorz00zz__errorz00
														(BGl_symbol2291z00zz__threadz00,
														BGl_string2293z00zz__threadz00, BgL_dz00_3662);
												}
										}
								}
						}
				}
		}

	}



/* &%user-current-thread1227 */
	obj_t BGl_z62z52userzd2currentzd2thread1227z30zz__threadz00(obj_t
		BgL_envz00_3663, obj_t BgL_oz00_3664)
	{
		{	/* Llib/thread.scm 451 */
			return ((obj_t) ((BgL_threadz00_bglt) BgL_oz00_3664));
		}

	}



/* &thread-name-set!1220 */
	obj_t BGl_z62threadzd2namezd2setz121220z70zz__threadz00(obj_t BgL_envz00_3665,
		obj_t BgL_thz00_3666, obj_t BgL_vz00_3667)
	{
		{	/* Llib/thread.scm 440 */
			return
				BGl_errorz00zz__errorz00(BGl_symbol2294z00zz__threadz00,
				BGl_string2295z00zz__threadz00,
				((obj_t) ((BgL_threadz00_bglt) BgL_thz00_3666)));
		}

	}



/* &thread-name1218 */
	obj_t BGl_z62threadzd2name1218zb0zz__threadz00(obj_t BgL_envz00_3668,
		obj_t BgL_thz00_3669)
	{
		{	/* Llib/thread.scm 435 */
			return
				BGl_errorz00zz__errorz00(BGl_symbol2296z00zz__threadz00,
				BGl_string2295z00zz__threadz00,
				((obj_t) ((BgL_threadz00_bglt) BgL_thz00_3669)));
		}

	}



/* &thread-cleanup-set!1215 */
	obj_t BGl_z62threadzd2cleanupzd2setz121215z70zz__threadz00(obj_t
		BgL_envz00_3670, obj_t BgL_thz00_3671, obj_t BgL_vz00_3672)
	{
		{	/* Llib/thread.scm 430 */
			return
				BGl_errorz00zz__errorz00(BGl_symbol2297z00zz__threadz00,
				BGl_string2295z00zz__threadz00,
				((obj_t) ((BgL_threadz00_bglt) BgL_thz00_3671)));
		}

	}



/* &thread-cleanup1213 */
	obj_t BGl_z62threadzd2cleanup1213zb0zz__threadz00(obj_t BgL_envz00_3673,
		obj_t BgL_thz00_3674)
	{
		{	/* Llib/thread.scm 425 */
			return
				BGl_errorz00zz__errorz00(BGl_symbol2298z00zz__threadz00,
				BGl_string2295z00zz__threadz00,
				((obj_t) ((BgL_threadz00_bglt) BgL_thz00_3674)));
		}

	}



/* &thread-specific-set!1211 */
	obj_t BGl_z62threadzd2specificzd2setz121211z70zz__threadz00(obj_t
		BgL_envz00_3675, obj_t BgL_thz00_3676, obj_t BgL_vz00_3677)
	{
		{	/* Llib/thread.scm 420 */
			return
				BGl_errorz00zz__errorz00(BGl_symbol2299z00zz__threadz00,
				BGl_string2295z00zz__threadz00,
				((obj_t) ((BgL_threadz00_bglt) BgL_thz00_3676)));
		}

	}



/* &thread-specific1209 */
	obj_t BGl_z62threadzd2specific1209zb0zz__threadz00(obj_t BgL_envz00_3678,
		obj_t BgL_thz00_3679)
	{
		{	/* Llib/thread.scm 415 */
			return
				BGl_errorz00zz__errorz00(BGl_symbol2300z00zz__threadz00,
				BGl_string2295z00zz__threadz00,
				((obj_t) ((BgL_threadz00_bglt) BgL_thz00_3679)));
		}

	}



/* &thread-kill!1207 */
	obj_t BGl_z62threadzd2killz121207za2zz__threadz00(obj_t BgL_envz00_3680,
		obj_t BgL_thz00_3681, obj_t BgL_nz00_3682)
	{
		{	/* Llib/thread.scm 410 */
			return
				BGl_errorz00zz__errorz00(BGl_symbol2301z00zz__threadz00,
				BGl_string2295z00zz__threadz00,
				((obj_t) ((BgL_threadz00_bglt) BgL_thz00_3681)));
		}

	}



/* &thread-terminate!1204 */
	obj_t BGl_z62threadzd2terminatez121204za2zz__threadz00(obj_t BgL_envz00_3683,
		obj_t BgL_thz00_3684)
	{
		{	/* Llib/thread.scm 405 */
			return
				BGl_errorz00zz__errorz00(BGl_symbol2302z00zz__threadz00,
				BGl_string2295z00zz__threadz00,
				((obj_t) ((BgL_threadz00_bglt) BgL_thz00_3684)));
		}

	}



/* &thread-join!1202 */
	obj_t BGl_z62threadzd2joinz121202za2zz__threadz00(obj_t BgL_envz00_3685,
		obj_t BgL_thz00_3686, obj_t BgL_timeoutz00_3687)
	{
		{	/* Llib/thread.scm 400 */
			return
				BGl_errorz00zz__errorz00(BGl_symbol2303z00zz__threadz00,
				BGl_string2295z00zz__threadz00,
				((obj_t) ((BgL_threadz00_bglt) BgL_thz00_3686)));
		}

	}



/* &thread-start-joinabl1199 */
	obj_t BGl_z62threadzd2startzd2joinabl1199z62zz__threadz00(obj_t
		BgL_envz00_3688, obj_t BgL_thz00_3689)
	{
		{	/* Llib/thread.scm 395 */
			return
				BGl_errorz00zz__errorz00(BGl_symbol2304z00zz__threadz00,
				BGl_string2295z00zz__threadz00,
				((obj_t) ((BgL_threadz00_bglt) BgL_thz00_3689)));
		}

	}



/* &thread-start!1197 */
	obj_t BGl_z62threadzd2startz121197za2zz__threadz00(obj_t BgL_envz00_3690,
		obj_t BgL_thz00_3691, obj_t BgL_scz00_3692)
	{
		{	/* Llib/thread.scm 390 */
			return
				BGl_errorz00zz__errorz00(BGl_symbol2305z00zz__threadz00,
				BGl_string2295z00zz__threadz00,
				((obj_t) ((BgL_threadz00_bglt) BgL_thz00_3691)));
		}

	}



/* &thread-initialize!1195 */
	obj_t BGl_z62threadzd2initializa7ez121195z05zz__threadz00(obj_t
		BgL_envz00_3693, obj_t BgL_thz00_3694)
	{
		{	/* Llib/thread.scm 384 */
			return ((obj_t) ((BgL_threadz00_bglt) BgL_thz00_3694));
		}

	}



/* &tb-condvar-initializ1187 */
	obj_t BGl_z62tbzd2condvarzd2initializa71187zc5zz__threadz00(obj_t
		BgL_envz00_3695, obj_t BgL_tbz00_3696, obj_t BgL_condvarz00_3697)
	{
		{	/* Llib/thread.scm 355 */
			return
				BGl_errorz00zz__errorz00(BGl_symbol2306z00zz__threadz00,
				BGl_string2295z00zz__threadz00,
				((obj_t) ((BgL_threadzd2backendzd2_bglt) BgL_tbz00_3696)));
		}

	}



/* &tb-mutex-initialize!1185 */
	obj_t BGl_z62tbzd2mutexzd2initializa7ez121185zd7zz__threadz00(obj_t
		BgL_envz00_3698, obj_t BgL_tbz00_3699, obj_t BgL_mutexz00_3700)
	{
		{	/* Llib/thread.scm 350 */
			return
				BGl_errorz00zz__errorz00(BGl_symbol2307z00zz__threadz00,
				BGl_string2295z00zz__threadz00,
				((obj_t) ((BgL_threadzd2backendzd2_bglt) BgL_tbz00_3699)));
		}

	}



/* &tb-current-thread1179 */
	obj_t BGl_z62tbzd2currentzd2thread1179z62zz__threadz00(obj_t BgL_envz00_3701,
		obj_t BgL_tbz00_3702)
	{
		{	/* Llib/thread.scm 326 */
			return
				BGl_errorz00zz__errorz00(BGl_symbol2308z00zz__threadz00,
				BGl_string2295z00zz__threadz00,
				((obj_t) ((BgL_threadzd2backendzd2_bglt) BgL_tbz00_3702)));
		}

	}



/* &tb-make-thread1177 */
	obj_t BGl_z62tbzd2makezd2thread1177z62zz__threadz00(obj_t BgL_envz00_3703,
		obj_t BgL_tbz00_3704, obj_t BgL_bodyz00_3705, obj_t BgL_namez00_3706)
	{
		{	/* Llib/thread.scm 321 */
			return
				BGl_errorz00zz__errorz00(BGl_symbol2309z00zz__threadz00,
				BGl_string2295z00zz__threadz00,
				((obj_t) ((BgL_threadzd2backendzd2_bglt) BgL_tbz00_3704)));
		}

	}



/* tb-make-thread */
	BGL_EXPORTED_DEF BgL_threadz00_bglt
		BGl_tbzd2makezd2threadz00zz__threadz00(BgL_threadzd2backendzd2_bglt
		BgL_tbz00_7, obj_t BgL_bodyz00_8, obj_t BgL_namez00_9)
	{
		{	/* Llib/thread.scm 321 */
			{	/* Llib/thread.scm 321 */
				obj_t BgL_method1178z00_1645;

				{	/* Llib/thread.scm 321 */
					obj_t BgL_res1938z00_2516;

					{	/* Llib/thread.scm 321 */
						long BgL_objzd2classzd2numz00_2487;

						BgL_objzd2classzd2numz00_2487 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_tbz00_7));
						{	/* Llib/thread.scm 321 */
							obj_t BgL_arg1920z00_2488;

							BgL_arg1920z00_2488 =
								PROCEDURE_REF(BGl_tbzd2makezd2threadzd2envzd2zz__threadz00,
								(int) (1L));
							{	/* Llib/thread.scm 321 */
								int BgL_offsetz00_2491;

								BgL_offsetz00_2491 = (int) (BgL_objzd2classzd2numz00_2487);
								{	/* Llib/thread.scm 321 */
									long BgL_offsetz00_2492;

									BgL_offsetz00_2492 =
										((long) (BgL_offsetz00_2491) - OBJECT_TYPE);
									{	/* Llib/thread.scm 321 */
										long BgL_modz00_2493;

										BgL_modz00_2493 =
											(BgL_offsetz00_2492 >> (int) ((long) ((int) (4L))));
										{	/* Llib/thread.scm 321 */
											long BgL_restz00_2495;

											BgL_restz00_2495 =
												(BgL_offsetz00_2492 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Llib/thread.scm 321 */

												{	/* Llib/thread.scm 321 */
													obj_t BgL_bucketz00_2497;

													BgL_bucketz00_2497 =
														VECTOR_REF(
														((obj_t) BgL_arg1920z00_2488), BgL_modz00_2493);
													BgL_res1938z00_2516 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2497), BgL_restz00_2495);
					}}}}}}}}
					BgL_method1178z00_1645 = BgL_res1938z00_2516;
				}
				return
					((BgL_threadz00_bglt)
					BGL_PROCEDURE_CALL3(BgL_method1178z00_1645,
						((obj_t) BgL_tbz00_7), BgL_bodyz00_8, BgL_namez00_9));
			}
		}

	}



/* &tb-make-thread */
	BgL_threadz00_bglt BGl_z62tbzd2makezd2threadz62zz__threadz00(obj_t
		BgL_envz00_3707, obj_t BgL_tbz00_3708, obj_t BgL_bodyz00_3709,
		obj_t BgL_namez00_3710)
	{
		{	/* Llib/thread.scm 321 */
			{	/* Llib/thread.scm 321 */
				obj_t BgL_auxz00_5865;
				BgL_threadzd2backendzd2_bglt BgL_auxz00_5835;

				if (PROCEDUREP(BgL_bodyz00_3709))
					{	/* Llib/thread.scm 321 */
						BgL_auxz00_5865 = BgL_bodyz00_3709;
					}
				else
					{
						obj_t BgL_auxz00_5868;

						BgL_auxz00_5868 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
							BINT(12558L), BGl_string2310z00zz__threadz00,
							BGl_string2160z00zz__threadz00, BgL_bodyz00_3709);
						FAILURE(BgL_auxz00_5868, BFALSE, BFALSE);
					}
				{	/* Llib/thread.scm 321 */
					bool_t BgL_test2652z00_5836;

					{	/* Llib/thread.scm 321 */
						obj_t BgL_classz00_4140;

						BgL_classz00_4140 = BGl_threadzd2backendzd2zz__threadz00;
						if (BGL_OBJECTP(BgL_tbz00_3708))
							{	/* Llib/thread.scm 321 */
								BgL_objectz00_bglt BgL_arg1918z00_4142;
								long BgL_arg1919z00_4143;

								BgL_arg1918z00_4142 = (BgL_objectz00_bglt) (BgL_tbz00_3708);
								BgL_arg1919z00_4143 = BGL_CLASS_DEPTH(BgL_classz00_4140);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Llib/thread.scm 321 */
										long BgL_idxz00_4151;

										BgL_idxz00_4151 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1918z00_4142);
										{	/* Llib/thread.scm 321 */
											obj_t BgL_arg1904z00_4152;

											{	/* Llib/thread.scm 321 */
												long BgL_arg1906z00_4153;

												BgL_arg1906z00_4153 =
													(BgL_idxz00_4151 + BgL_arg1919z00_4143);
												BgL_arg1904z00_4152 =
													VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													BgL_arg1906z00_4153);
											}
											BgL_test2652z00_5836 =
												(BgL_arg1904z00_4152 == BgL_classz00_4140);
									}}
								else
									{	/* Llib/thread.scm 321 */
										bool_t BgL_res2031z00_4158;

										{	/* Llib/thread.scm 321 */
											obj_t BgL_oclassz00_4162;

											{	/* Llib/thread.scm 321 */
												obj_t BgL_arg1925z00_4164;
												long BgL_arg1926z00_4165;

												BgL_arg1925z00_4164 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Llib/thread.scm 321 */
													long BgL_arg1927z00_4166;

													BgL_arg1927z00_4166 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1918z00_4142);
													BgL_arg1926z00_4165 =
														(BgL_arg1927z00_4166 - OBJECT_TYPE);
												}
												BgL_oclassz00_4162 =
													VECTOR_REF(BgL_arg1925z00_4164, BgL_arg1926z00_4165);
											}
											{	/* Llib/thread.scm 321 */
												bool_t BgL__ortest_1147z00_4172;

												BgL__ortest_1147z00_4172 =
													(BgL_classz00_4140 == BgL_oclassz00_4162);
												if (BgL__ortest_1147z00_4172)
													{	/* Llib/thread.scm 321 */
														BgL_res2031z00_4158 = BgL__ortest_1147z00_4172;
													}
												else
													{	/* Llib/thread.scm 321 */
														long BgL_odepthz00_4173;

														{	/* Llib/thread.scm 321 */
															obj_t BgL_arg1912z00_4174;

															BgL_arg1912z00_4174 = (BgL_oclassz00_4162);
															BgL_odepthz00_4173 =
																BGL_CLASS_DEPTH(BgL_arg1912z00_4174);
														}
														if ((BgL_arg1919z00_4143 < BgL_odepthz00_4173))
															{	/* Llib/thread.scm 321 */
																obj_t BgL_arg1910z00_4178;

																{	/* Llib/thread.scm 321 */
																	obj_t BgL_arg1911z00_4179;

																	BgL_arg1911z00_4179 = (BgL_oclassz00_4162);
																	BgL_arg1910z00_4178 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1911z00_4179,
																		BgL_arg1919z00_4143);
																}
																BgL_res2031z00_4158 =
																	(BgL_arg1910z00_4178 == BgL_classz00_4140);
															}
														else
															{	/* Llib/thread.scm 321 */
																BgL_res2031z00_4158 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2652z00_5836 = BgL_res2031z00_4158;
									}
							}
						else
							{	/* Llib/thread.scm 321 */
								BgL_test2652z00_5836 = ((bool_t) 0);
							}
					}
					if (BgL_test2652z00_5836)
						{	/* Llib/thread.scm 321 */
							BgL_auxz00_5835 = ((BgL_threadzd2backendzd2_bglt) BgL_tbz00_3708);
						}
					else
						{
							obj_t BgL_auxz00_5861;

							BgL_auxz00_5861 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
								BINT(12558L), BGl_string2310z00zz__threadz00,
								BGl_string2153z00zz__threadz00, BgL_tbz00_3708);
							FAILURE(BgL_auxz00_5861, BFALSE, BFALSE);
						}
				}
				return
					BGl_tbzd2makezd2threadz00zz__threadz00(BgL_auxz00_5835,
					BgL_auxz00_5865, BgL_namez00_3710);
			}
		}

	}



/* tb-current-thread */
	BGL_EXPORTED_DEF obj_t
		BGl_tbzd2currentzd2threadz00zz__threadz00(BgL_threadzd2backendzd2_bglt
		BgL_tbz00_10)
	{
		{	/* Llib/thread.scm 326 */
			{	/* Llib/thread.scm 326 */
				obj_t BgL_method1180z00_1646;

				{	/* Llib/thread.scm 326 */
					obj_t BgL_res1943z00_2547;

					{	/* Llib/thread.scm 326 */
						long BgL_objzd2classzd2numz00_2518;

						BgL_objzd2classzd2numz00_2518 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_tbz00_10));
						{	/* Llib/thread.scm 326 */
							obj_t BgL_arg1920z00_2519;

							BgL_arg1920z00_2519 =
								PROCEDURE_REF(BGl_tbzd2currentzd2threadzd2envzd2zz__threadz00,
								(int) (1L));
							{	/* Llib/thread.scm 326 */
								int BgL_offsetz00_2522;

								BgL_offsetz00_2522 = (int) (BgL_objzd2classzd2numz00_2518);
								{	/* Llib/thread.scm 326 */
									long BgL_offsetz00_2523;

									BgL_offsetz00_2523 =
										((long) (BgL_offsetz00_2522) - OBJECT_TYPE);
									{	/* Llib/thread.scm 326 */
										long BgL_modz00_2524;

										BgL_modz00_2524 =
											(BgL_offsetz00_2523 >> (int) ((long) ((int) (4L))));
										{	/* Llib/thread.scm 326 */
											long BgL_restz00_2526;

											BgL_restz00_2526 =
												(BgL_offsetz00_2523 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Llib/thread.scm 326 */

												{	/* Llib/thread.scm 326 */
													obj_t BgL_bucketz00_2528;

													BgL_bucketz00_2528 =
														VECTOR_REF(
														((obj_t) BgL_arg1920z00_2519), BgL_modz00_2524);
													BgL_res1943z00_2547 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2528), BgL_restz00_2526);
					}}}}}}}}
					BgL_method1180z00_1646 = BgL_res1943z00_2547;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1180z00_1646, ((obj_t) BgL_tbz00_10));
			}
		}

	}



/* &tb-current-thread */
	obj_t BGl_z62tbzd2currentzd2threadz62zz__threadz00(obj_t BgL_envz00_3711,
		obj_t BgL_tbz00_3712)
	{
		{	/* Llib/thread.scm 326 */
			{	/* Llib/thread.scm 326 */
				BgL_threadzd2backendzd2_bglt BgL_auxz00_5903;

				{	/* Llib/thread.scm 326 */
					bool_t BgL_test2658z00_5904;

					{	/* Llib/thread.scm 326 */
						obj_t BgL_classz00_4180;

						BgL_classz00_4180 = BGl_threadzd2backendzd2zz__threadz00;
						if (BGL_OBJECTP(BgL_tbz00_3712))
							{	/* Llib/thread.scm 326 */
								BgL_objectz00_bglt BgL_arg1918z00_4182;
								long BgL_arg1919z00_4183;

								BgL_arg1918z00_4182 = (BgL_objectz00_bglt) (BgL_tbz00_3712);
								BgL_arg1919z00_4183 = BGL_CLASS_DEPTH(BgL_classz00_4180);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Llib/thread.scm 326 */
										long BgL_idxz00_4191;

										BgL_idxz00_4191 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1918z00_4182);
										{	/* Llib/thread.scm 326 */
											obj_t BgL_arg1904z00_4192;

											{	/* Llib/thread.scm 326 */
												long BgL_arg1906z00_4193;

												BgL_arg1906z00_4193 =
													(BgL_idxz00_4191 + BgL_arg1919z00_4183);
												BgL_arg1904z00_4192 =
													VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													BgL_arg1906z00_4193);
											}
											BgL_test2658z00_5904 =
												(BgL_arg1904z00_4192 == BgL_classz00_4180);
									}}
								else
									{	/* Llib/thread.scm 326 */
										bool_t BgL_res2031z00_4198;

										{	/* Llib/thread.scm 326 */
											obj_t BgL_oclassz00_4202;

											{	/* Llib/thread.scm 326 */
												obj_t BgL_arg1925z00_4204;
												long BgL_arg1926z00_4205;

												BgL_arg1925z00_4204 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Llib/thread.scm 326 */
													long BgL_arg1927z00_4206;

													BgL_arg1927z00_4206 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1918z00_4182);
													BgL_arg1926z00_4205 =
														(BgL_arg1927z00_4206 - OBJECT_TYPE);
												}
												BgL_oclassz00_4202 =
													VECTOR_REF(BgL_arg1925z00_4204, BgL_arg1926z00_4205);
											}
											{	/* Llib/thread.scm 326 */
												bool_t BgL__ortest_1147z00_4212;

												BgL__ortest_1147z00_4212 =
													(BgL_classz00_4180 == BgL_oclassz00_4202);
												if (BgL__ortest_1147z00_4212)
													{	/* Llib/thread.scm 326 */
														BgL_res2031z00_4198 = BgL__ortest_1147z00_4212;
													}
												else
													{	/* Llib/thread.scm 326 */
														long BgL_odepthz00_4213;

														{	/* Llib/thread.scm 326 */
															obj_t BgL_arg1912z00_4214;

															BgL_arg1912z00_4214 = (BgL_oclassz00_4202);
															BgL_odepthz00_4213 =
																BGL_CLASS_DEPTH(BgL_arg1912z00_4214);
														}
														if ((BgL_arg1919z00_4183 < BgL_odepthz00_4213))
															{	/* Llib/thread.scm 326 */
																obj_t BgL_arg1910z00_4218;

																{	/* Llib/thread.scm 326 */
																	obj_t BgL_arg1911z00_4219;

																	BgL_arg1911z00_4219 = (BgL_oclassz00_4202);
																	BgL_arg1910z00_4218 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1911z00_4219,
																		BgL_arg1919z00_4183);
																}
																BgL_res2031z00_4198 =
																	(BgL_arg1910z00_4218 == BgL_classz00_4180);
															}
														else
															{	/* Llib/thread.scm 326 */
																BgL_res2031z00_4198 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2658z00_5904 = BgL_res2031z00_4198;
									}
							}
						else
							{	/* Llib/thread.scm 326 */
								BgL_test2658z00_5904 = ((bool_t) 0);
							}
					}
					if (BgL_test2658z00_5904)
						{	/* Llib/thread.scm 326 */
							BgL_auxz00_5903 = ((BgL_threadzd2backendzd2_bglt) BgL_tbz00_3712);
						}
					else
						{
							obj_t BgL_auxz00_5929;

							BgL_auxz00_5929 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
								BINT(12844L), BGl_string2311z00zz__threadz00,
								BGl_string2153z00zz__threadz00, BgL_tbz00_3712);
							FAILURE(BgL_auxz00_5929, BFALSE, BFALSE);
						}
				}
				return BGl_tbzd2currentzd2threadz00zz__threadz00(BgL_auxz00_5903);
			}
		}

	}



/* tb-mutex-initialize! */
	BGL_EXPORTED_DEF obj_t
		BGl_tbzd2mutexzd2initializa7ez12zb5zz__threadz00
		(BgL_threadzd2backendzd2_bglt BgL_tbz00_15, obj_t BgL_mutexz00_16)
	{
		{	/* Llib/thread.scm 350 */
			{	/* Llib/thread.scm 350 */
				obj_t BgL_method1186z00_1647;

				{	/* Llib/thread.scm 350 */
					obj_t BgL_res1948z00_2578;

					{	/* Llib/thread.scm 350 */
						long BgL_objzd2classzd2numz00_2549;

						BgL_objzd2classzd2numz00_2549 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_tbz00_15));
						{	/* Llib/thread.scm 350 */
							obj_t BgL_arg1920z00_2550;

							BgL_arg1920z00_2550 =
								PROCEDURE_REF
								(BGl_tbzd2mutexzd2initializa7ez12zd2envz67zz__threadz00,
								(int) (1L));
							{	/* Llib/thread.scm 350 */
								int BgL_offsetz00_2553;

								BgL_offsetz00_2553 = (int) (BgL_objzd2classzd2numz00_2549);
								{	/* Llib/thread.scm 350 */
									long BgL_offsetz00_2554;

									BgL_offsetz00_2554 =
										((long) (BgL_offsetz00_2553) - OBJECT_TYPE);
									{	/* Llib/thread.scm 350 */
										long BgL_modz00_2555;

										BgL_modz00_2555 =
											(BgL_offsetz00_2554 >> (int) ((long) ((int) (4L))));
										{	/* Llib/thread.scm 350 */
											long BgL_restz00_2557;

											BgL_restz00_2557 =
												(BgL_offsetz00_2554 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Llib/thread.scm 350 */

												{	/* Llib/thread.scm 350 */
													obj_t BgL_bucketz00_2559;

													BgL_bucketz00_2559 =
														VECTOR_REF(
														((obj_t) BgL_arg1920z00_2550), BgL_modz00_2555);
													BgL_res1948z00_2578 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2559), BgL_restz00_2557);
					}}}}}}}}
					BgL_method1186z00_1647 = BgL_res1948z00_2578;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1186z00_1647,
					((obj_t) BgL_tbz00_15), BgL_mutexz00_16);
			}
		}

	}



/* &tb-mutex-initialize! */
	obj_t BGl_z62tbzd2mutexzd2initializa7ez12zd7zz__threadz00(obj_t
		BgL_envz00_3713, obj_t BgL_tbz00_3714, obj_t BgL_mutexz00_3715)
	{
		{	/* Llib/thread.scm 350 */
			{	/* Llib/thread.scm 350 */
				obj_t BgL_auxz00_5995;
				BgL_threadzd2backendzd2_bglt BgL_auxz00_5965;

				if (BGL_MUTEXP(BgL_mutexz00_3715))
					{	/* Llib/thread.scm 350 */
						BgL_auxz00_5995 = BgL_mutexz00_3715;
					}
				else
					{
						obj_t BgL_auxz00_5998;

						BgL_auxz00_5998 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
							BINT(14029L), BGl_string2312z00zz__threadz00,
							BGl_string2165z00zz__threadz00, BgL_mutexz00_3715);
						FAILURE(BgL_auxz00_5998, BFALSE, BFALSE);
					}
				{	/* Llib/thread.scm 350 */
					bool_t BgL_test2663z00_5966;

					{	/* Llib/thread.scm 350 */
						obj_t BgL_classz00_4220;

						BgL_classz00_4220 = BGl_threadzd2backendzd2zz__threadz00;
						if (BGL_OBJECTP(BgL_tbz00_3714))
							{	/* Llib/thread.scm 350 */
								BgL_objectz00_bglt BgL_arg1918z00_4222;
								long BgL_arg1919z00_4223;

								BgL_arg1918z00_4222 = (BgL_objectz00_bglt) (BgL_tbz00_3714);
								BgL_arg1919z00_4223 = BGL_CLASS_DEPTH(BgL_classz00_4220);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Llib/thread.scm 350 */
										long BgL_idxz00_4231;

										BgL_idxz00_4231 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1918z00_4222);
										{	/* Llib/thread.scm 350 */
											obj_t BgL_arg1904z00_4232;

											{	/* Llib/thread.scm 350 */
												long BgL_arg1906z00_4233;

												BgL_arg1906z00_4233 =
													(BgL_idxz00_4231 + BgL_arg1919z00_4223);
												BgL_arg1904z00_4232 =
													VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													BgL_arg1906z00_4233);
											}
											BgL_test2663z00_5966 =
												(BgL_arg1904z00_4232 == BgL_classz00_4220);
									}}
								else
									{	/* Llib/thread.scm 350 */
										bool_t BgL_res2031z00_4238;

										{	/* Llib/thread.scm 350 */
											obj_t BgL_oclassz00_4242;

											{	/* Llib/thread.scm 350 */
												obj_t BgL_arg1925z00_4244;
												long BgL_arg1926z00_4245;

												BgL_arg1925z00_4244 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Llib/thread.scm 350 */
													long BgL_arg1927z00_4246;

													BgL_arg1927z00_4246 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1918z00_4222);
													BgL_arg1926z00_4245 =
														(BgL_arg1927z00_4246 - OBJECT_TYPE);
												}
												BgL_oclassz00_4242 =
													VECTOR_REF(BgL_arg1925z00_4244, BgL_arg1926z00_4245);
											}
											{	/* Llib/thread.scm 350 */
												bool_t BgL__ortest_1147z00_4252;

												BgL__ortest_1147z00_4252 =
													(BgL_classz00_4220 == BgL_oclassz00_4242);
												if (BgL__ortest_1147z00_4252)
													{	/* Llib/thread.scm 350 */
														BgL_res2031z00_4238 = BgL__ortest_1147z00_4252;
													}
												else
													{	/* Llib/thread.scm 350 */
														long BgL_odepthz00_4253;

														{	/* Llib/thread.scm 350 */
															obj_t BgL_arg1912z00_4254;

															BgL_arg1912z00_4254 = (BgL_oclassz00_4242);
															BgL_odepthz00_4253 =
																BGL_CLASS_DEPTH(BgL_arg1912z00_4254);
														}
														if ((BgL_arg1919z00_4223 < BgL_odepthz00_4253))
															{	/* Llib/thread.scm 350 */
																obj_t BgL_arg1910z00_4258;

																{	/* Llib/thread.scm 350 */
																	obj_t BgL_arg1911z00_4259;

																	BgL_arg1911z00_4259 = (BgL_oclassz00_4242);
																	BgL_arg1910z00_4258 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1911z00_4259,
																		BgL_arg1919z00_4223);
																}
																BgL_res2031z00_4238 =
																	(BgL_arg1910z00_4258 == BgL_classz00_4220);
															}
														else
															{	/* Llib/thread.scm 350 */
																BgL_res2031z00_4238 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2663z00_5966 = BgL_res2031z00_4238;
									}
							}
						else
							{	/* Llib/thread.scm 350 */
								BgL_test2663z00_5966 = ((bool_t) 0);
							}
					}
					if (BgL_test2663z00_5966)
						{	/* Llib/thread.scm 350 */
							BgL_auxz00_5965 = ((BgL_threadzd2backendzd2_bglt) BgL_tbz00_3714);
						}
					else
						{
							obj_t BgL_auxz00_5991;

							BgL_auxz00_5991 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
								BINT(14029L), BGl_string2312z00zz__threadz00,
								BGl_string2153z00zz__threadz00, BgL_tbz00_3714);
							FAILURE(BgL_auxz00_5991, BFALSE, BFALSE);
						}
				}
				return
					BGl_tbzd2mutexzd2initializa7ez12zb5zz__threadz00(BgL_auxz00_5965,
					BgL_auxz00_5995);
			}
		}

	}



/* tb-condvar-initialize! */
	BGL_EXPORTED_DEF obj_t
		BGl_tbzd2condvarzd2initializa7ez12zb5zz__threadz00
		(BgL_threadzd2backendzd2_bglt BgL_tbz00_17, obj_t BgL_condvarz00_18)
	{
		{	/* Llib/thread.scm 355 */
			{	/* Llib/thread.scm 355 */
				obj_t BgL_method1188z00_1648;

				{	/* Llib/thread.scm 355 */
					obj_t BgL_res1953z00_2609;

					{	/* Llib/thread.scm 355 */
						long BgL_objzd2classzd2numz00_2580;

						BgL_objzd2classzd2numz00_2580 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_tbz00_17));
						{	/* Llib/thread.scm 355 */
							obj_t BgL_arg1920z00_2581;

							BgL_arg1920z00_2581 =
								PROCEDURE_REF
								(BGl_tbzd2condvarzd2initializa7ez12zd2envz67zz__threadz00,
								(int) (1L));
							{	/* Llib/thread.scm 355 */
								int BgL_offsetz00_2584;

								BgL_offsetz00_2584 = (int) (BgL_objzd2classzd2numz00_2580);
								{	/* Llib/thread.scm 355 */
									long BgL_offsetz00_2585;

									BgL_offsetz00_2585 =
										((long) (BgL_offsetz00_2584) - OBJECT_TYPE);
									{	/* Llib/thread.scm 355 */
										long BgL_modz00_2586;

										BgL_modz00_2586 =
											(BgL_offsetz00_2585 >> (int) ((long) ((int) (4L))));
										{	/* Llib/thread.scm 355 */
											long BgL_restz00_2588;

											BgL_restz00_2588 =
												(BgL_offsetz00_2585 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Llib/thread.scm 355 */

												{	/* Llib/thread.scm 355 */
													obj_t BgL_bucketz00_2590;

													BgL_bucketz00_2590 =
														VECTOR_REF(
														((obj_t) BgL_arg1920z00_2581), BgL_modz00_2586);
													BgL_res1953z00_2609 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2590), BgL_restz00_2588);
					}}}}}}}}
					BgL_method1188z00_1648 = BgL_res1953z00_2609;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1188z00_1648,
					((obj_t) BgL_tbz00_17), BgL_condvarz00_18);
			}
		}

	}



/* &tb-condvar-initialize! */
	obj_t BGl_z62tbzd2condvarzd2initializa7ez12zd7zz__threadz00(obj_t
		BgL_envz00_3716, obj_t BgL_tbz00_3717, obj_t BgL_condvarz00_3718)
	{
		{	/* Llib/thread.scm 355 */
			{	/* Llib/thread.scm 355 */
				obj_t BgL_auxz00_6064;
				BgL_threadzd2backendzd2_bglt BgL_auxz00_6034;

				if (BGL_CONDVARP(BgL_condvarz00_3718))
					{	/* Llib/thread.scm 355 */
						BgL_auxz00_6064 = BgL_condvarz00_3718;
					}
				else
					{
						obj_t BgL_auxz00_6067;

						BgL_auxz00_6067 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
							BINT(14317L), BGl_string2313z00zz__threadz00,
							BGl_string2178z00zz__threadz00, BgL_condvarz00_3718);
						FAILURE(BgL_auxz00_6067, BFALSE, BFALSE);
					}
				{	/* Llib/thread.scm 355 */
					bool_t BgL_test2669z00_6035;

					{	/* Llib/thread.scm 355 */
						obj_t BgL_classz00_4260;

						BgL_classz00_4260 = BGl_threadzd2backendzd2zz__threadz00;
						if (BGL_OBJECTP(BgL_tbz00_3717))
							{	/* Llib/thread.scm 355 */
								BgL_objectz00_bglt BgL_arg1918z00_4262;
								long BgL_arg1919z00_4263;

								BgL_arg1918z00_4262 = (BgL_objectz00_bglt) (BgL_tbz00_3717);
								BgL_arg1919z00_4263 = BGL_CLASS_DEPTH(BgL_classz00_4260);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Llib/thread.scm 355 */
										long BgL_idxz00_4271;

										BgL_idxz00_4271 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1918z00_4262);
										{	/* Llib/thread.scm 355 */
											obj_t BgL_arg1904z00_4272;

											{	/* Llib/thread.scm 355 */
												long BgL_arg1906z00_4273;

												BgL_arg1906z00_4273 =
													(BgL_idxz00_4271 + BgL_arg1919z00_4263);
												BgL_arg1904z00_4272 =
													VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													BgL_arg1906z00_4273);
											}
											BgL_test2669z00_6035 =
												(BgL_arg1904z00_4272 == BgL_classz00_4260);
									}}
								else
									{	/* Llib/thread.scm 355 */
										bool_t BgL_res2031z00_4278;

										{	/* Llib/thread.scm 355 */
											obj_t BgL_oclassz00_4282;

											{	/* Llib/thread.scm 355 */
												obj_t BgL_arg1925z00_4284;
												long BgL_arg1926z00_4285;

												BgL_arg1925z00_4284 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Llib/thread.scm 355 */
													long BgL_arg1927z00_4286;

													BgL_arg1927z00_4286 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1918z00_4262);
													BgL_arg1926z00_4285 =
														(BgL_arg1927z00_4286 - OBJECT_TYPE);
												}
												BgL_oclassz00_4282 =
													VECTOR_REF(BgL_arg1925z00_4284, BgL_arg1926z00_4285);
											}
											{	/* Llib/thread.scm 355 */
												bool_t BgL__ortest_1147z00_4292;

												BgL__ortest_1147z00_4292 =
													(BgL_classz00_4260 == BgL_oclassz00_4282);
												if (BgL__ortest_1147z00_4292)
													{	/* Llib/thread.scm 355 */
														BgL_res2031z00_4278 = BgL__ortest_1147z00_4292;
													}
												else
													{	/* Llib/thread.scm 355 */
														long BgL_odepthz00_4293;

														{	/* Llib/thread.scm 355 */
															obj_t BgL_arg1912z00_4294;

															BgL_arg1912z00_4294 = (BgL_oclassz00_4282);
															BgL_odepthz00_4293 =
																BGL_CLASS_DEPTH(BgL_arg1912z00_4294);
														}
														if ((BgL_arg1919z00_4263 < BgL_odepthz00_4293))
															{	/* Llib/thread.scm 355 */
																obj_t BgL_arg1910z00_4298;

																{	/* Llib/thread.scm 355 */
																	obj_t BgL_arg1911z00_4299;

																	BgL_arg1911z00_4299 = (BgL_oclassz00_4282);
																	BgL_arg1910z00_4298 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1911z00_4299,
																		BgL_arg1919z00_4263);
																}
																BgL_res2031z00_4278 =
																	(BgL_arg1910z00_4298 == BgL_classz00_4260);
															}
														else
															{	/* Llib/thread.scm 355 */
																BgL_res2031z00_4278 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2669z00_6035 = BgL_res2031z00_4278;
									}
							}
						else
							{	/* Llib/thread.scm 355 */
								BgL_test2669z00_6035 = ((bool_t) 0);
							}
					}
					if (BgL_test2669z00_6035)
						{	/* Llib/thread.scm 355 */
							BgL_auxz00_6034 = ((BgL_threadzd2backendzd2_bglt) BgL_tbz00_3717);
						}
					else
						{
							obj_t BgL_auxz00_6060;

							BgL_auxz00_6060 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
								BINT(14317L), BGl_string2313z00zz__threadz00,
								BGl_string2153z00zz__threadz00, BgL_tbz00_3717);
							FAILURE(BgL_auxz00_6060, BFALSE, BFALSE);
						}
				}
				return
					BGl_tbzd2condvarzd2initializa7ez12zb5zz__threadz00(BgL_auxz00_6034,
					BgL_auxz00_6064);
			}
		}

	}



/* thread-initialize! */
	BGL_EXPORTED_DEF obj_t
		BGl_threadzd2initializa7ez12z67zz__threadz00(BgL_threadz00_bglt
		BgL_thz00_26)
	{
		{	/* Llib/thread.scm 384 */
			{	/* Llib/thread.scm 384 */
				obj_t BgL_method1196z00_1649;

				{	/* Llib/thread.scm 384 */
					obj_t BgL_res1958z00_2640;

					{	/* Llib/thread.scm 384 */
						long BgL_objzd2classzd2numz00_2611;

						BgL_objzd2classzd2numz00_2611 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_thz00_26));
						{	/* Llib/thread.scm 384 */
							obj_t BgL_arg1920z00_2612;

							BgL_arg1920z00_2612 =
								PROCEDURE_REF
								(BGl_threadzd2initializa7ez12zd2envzb5zz__threadz00,
								(int) (1L));
							{	/* Llib/thread.scm 384 */
								int BgL_offsetz00_2615;

								BgL_offsetz00_2615 = (int) (BgL_objzd2classzd2numz00_2611);
								{	/* Llib/thread.scm 384 */
									long BgL_offsetz00_2616;

									BgL_offsetz00_2616 =
										((long) (BgL_offsetz00_2615) - OBJECT_TYPE);
									{	/* Llib/thread.scm 384 */
										long BgL_modz00_2617;

										BgL_modz00_2617 =
											(BgL_offsetz00_2616 >> (int) ((long) ((int) (4L))));
										{	/* Llib/thread.scm 384 */
											long BgL_restz00_2619;

											BgL_restz00_2619 =
												(BgL_offsetz00_2616 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Llib/thread.scm 384 */

												{	/* Llib/thread.scm 384 */
													obj_t BgL_bucketz00_2621;

													BgL_bucketz00_2621 =
														VECTOR_REF(
														((obj_t) BgL_arg1920z00_2612), BgL_modz00_2617);
													BgL_res1958z00_2640 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2621), BgL_restz00_2619);
					}}}}}}}}
					BgL_method1196z00_1649 = BgL_res1958z00_2640;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1196z00_1649, ((obj_t) BgL_thz00_26));
			}
		}

	}



/* &thread-initialize! */
	obj_t BGl_z62threadzd2initializa7ez12z05zz__threadz00(obj_t BgL_envz00_3637,
		obj_t BgL_thz00_3638)
	{
		{	/* Llib/thread.scm 384 */
			{	/* Llib/thread.scm 384 */
				BgL_threadz00_bglt BgL_auxz00_6102;

				{	/* Llib/thread.scm 384 */
					bool_t BgL_test2675z00_6103;

					{	/* Llib/thread.scm 384 */
						obj_t BgL_classz00_4300;

						BgL_classz00_4300 = BGl_threadz00zz__threadz00;
						if (BGL_OBJECTP(BgL_thz00_3638))
							{	/* Llib/thread.scm 384 */
								BgL_objectz00_bglt BgL_arg1918z00_4302;
								long BgL_arg1919z00_4303;

								BgL_arg1918z00_4302 = (BgL_objectz00_bglt) (BgL_thz00_3638);
								BgL_arg1919z00_4303 = BGL_CLASS_DEPTH(BgL_classz00_4300);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Llib/thread.scm 384 */
										long BgL_idxz00_4311;

										BgL_idxz00_4311 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1918z00_4302);
										{	/* Llib/thread.scm 384 */
											obj_t BgL_arg1904z00_4312;

											{	/* Llib/thread.scm 384 */
												long BgL_arg1906z00_4313;

												BgL_arg1906z00_4313 =
													(BgL_idxz00_4311 + BgL_arg1919z00_4303);
												BgL_arg1904z00_4312 =
													VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													BgL_arg1906z00_4313);
											}
											BgL_test2675z00_6103 =
												(BgL_arg1904z00_4312 == BgL_classz00_4300);
									}}
								else
									{	/* Llib/thread.scm 384 */
										bool_t BgL_res2031z00_4318;

										{	/* Llib/thread.scm 384 */
											obj_t BgL_oclassz00_4322;

											{	/* Llib/thread.scm 384 */
												obj_t BgL_arg1925z00_4324;
												long BgL_arg1926z00_4325;

												BgL_arg1925z00_4324 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Llib/thread.scm 384 */
													long BgL_arg1927z00_4326;

													BgL_arg1927z00_4326 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1918z00_4302);
													BgL_arg1926z00_4325 =
														(BgL_arg1927z00_4326 - OBJECT_TYPE);
												}
												BgL_oclassz00_4322 =
													VECTOR_REF(BgL_arg1925z00_4324, BgL_arg1926z00_4325);
											}
											{	/* Llib/thread.scm 384 */
												bool_t BgL__ortest_1147z00_4332;

												BgL__ortest_1147z00_4332 =
													(BgL_classz00_4300 == BgL_oclassz00_4322);
												if (BgL__ortest_1147z00_4332)
													{	/* Llib/thread.scm 384 */
														BgL_res2031z00_4318 = BgL__ortest_1147z00_4332;
													}
												else
													{	/* Llib/thread.scm 384 */
														long BgL_odepthz00_4333;

														{	/* Llib/thread.scm 384 */
															obj_t BgL_arg1912z00_4334;

															BgL_arg1912z00_4334 = (BgL_oclassz00_4322);
															BgL_odepthz00_4333 =
																BGL_CLASS_DEPTH(BgL_arg1912z00_4334);
														}
														if ((BgL_arg1919z00_4303 < BgL_odepthz00_4333))
															{	/* Llib/thread.scm 384 */
																obj_t BgL_arg1910z00_4338;

																{	/* Llib/thread.scm 384 */
																	obj_t BgL_arg1911z00_4339;

																	BgL_arg1911z00_4339 = (BgL_oclassz00_4322);
																	BgL_arg1910z00_4338 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1911z00_4339,
																		BgL_arg1919z00_4303);
																}
																BgL_res2031z00_4318 =
																	(BgL_arg1910z00_4338 == BgL_classz00_4300);
															}
														else
															{	/* Llib/thread.scm 384 */
																BgL_res2031z00_4318 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2675z00_6103 = BgL_res2031z00_4318;
									}
							}
						else
							{	/* Llib/thread.scm 384 */
								BgL_test2675z00_6103 = ((bool_t) 0);
							}
					}
					if (BgL_test2675z00_6103)
						{	/* Llib/thread.scm 384 */
							BgL_auxz00_6102 = ((BgL_threadz00_bglt) BgL_thz00_3638);
						}
					else
						{
							obj_t BgL_auxz00_6128;

							BgL_auxz00_6128 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
								BINT(15868L), BGl_string2314z00zz__threadz00,
								BGl_string2158z00zz__threadz00, BgL_thz00_3638);
							FAILURE(BgL_auxz00_6128, BFALSE, BFALSE);
						}
				}
				return BGl_threadzd2initializa7ez12z67zz__threadz00(BgL_auxz00_6102);
			}
		}

	}



/* thread-start! */
	BGL_EXPORTED_DEF obj_t
		BGl_threadzd2startz12zc0zz__threadz00(BgL_threadz00_bglt BgL_thz00_27,
		obj_t BgL_scz00_28)
	{
		{	/* Llib/thread.scm 390 */
			{	/* Llib/thread.scm 390 */
				obj_t BgL_method1198z00_1650;

				{	/* Llib/thread.scm 390 */
					obj_t BgL_res1963z00_2671;

					{	/* Llib/thread.scm 390 */
						long BgL_objzd2classzd2numz00_2642;

						BgL_objzd2classzd2numz00_2642 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_thz00_27));
						{	/* Llib/thread.scm 390 */
							obj_t BgL_arg1920z00_2643;

							BgL_arg1920z00_2643 =
								PROCEDURE_REF(BGl_threadzd2startz12zd2envz12zz__threadz00,
								(int) (1L));
							{	/* Llib/thread.scm 390 */
								int BgL_offsetz00_2646;

								BgL_offsetz00_2646 = (int) (BgL_objzd2classzd2numz00_2642);
								{	/* Llib/thread.scm 390 */
									long BgL_offsetz00_2647;

									BgL_offsetz00_2647 =
										((long) (BgL_offsetz00_2646) - OBJECT_TYPE);
									{	/* Llib/thread.scm 390 */
										long BgL_modz00_2648;

										BgL_modz00_2648 =
											(BgL_offsetz00_2647 >> (int) ((long) ((int) (4L))));
										{	/* Llib/thread.scm 390 */
											long BgL_restz00_2650;

											BgL_restz00_2650 =
												(BgL_offsetz00_2647 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Llib/thread.scm 390 */

												{	/* Llib/thread.scm 390 */
													obj_t BgL_bucketz00_2652;

													BgL_bucketz00_2652 =
														VECTOR_REF(
														((obj_t) BgL_arg1920z00_2643), BgL_modz00_2648);
													BgL_res1963z00_2671 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2652), BgL_restz00_2650);
					}}}}}}}}
					BgL_method1198z00_1650 = BgL_res1963z00_2671;
				}
				{	/* Llib/thread.scm 390 */
					obj_t BgL_auxz00_6158;

					{	/* Llib/thread.scm 390 */
						obj_t BgL_list1504z00_1651;

						BgL_list1504z00_1651 = MAKE_YOUNG_PAIR(BgL_scz00_28, BNIL);
						BgL_auxz00_6158 =
							BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(
							((obj_t) BgL_thz00_27), BgL_list1504z00_1651);
					}
					return apply(BgL_method1198z00_1650, BgL_auxz00_6158);
				}
			}
		}

	}



/* &thread-start! */
	obj_t BGl_z62threadzd2startz12za2zz__threadz00(obj_t BgL_envz00_3719,
		obj_t BgL_thz00_3720, obj_t BgL_scz00_3721)
	{
		{	/* Llib/thread.scm 390 */
			{	/* Llib/thread.scm 390 */
				BgL_threadz00_bglt BgL_auxz00_6163;

				{	/* Llib/thread.scm 390 */
					bool_t BgL_test2680z00_6164;

					{	/* Llib/thread.scm 390 */
						obj_t BgL_classz00_4340;

						BgL_classz00_4340 = BGl_threadz00zz__threadz00;
						if (BGL_OBJECTP(BgL_thz00_3720))
							{	/* Llib/thread.scm 390 */
								BgL_objectz00_bglt BgL_arg1918z00_4342;
								long BgL_arg1919z00_4343;

								BgL_arg1918z00_4342 = (BgL_objectz00_bglt) (BgL_thz00_3720);
								BgL_arg1919z00_4343 = BGL_CLASS_DEPTH(BgL_classz00_4340);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Llib/thread.scm 390 */
										long BgL_idxz00_4351;

										BgL_idxz00_4351 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1918z00_4342);
										{	/* Llib/thread.scm 390 */
											obj_t BgL_arg1904z00_4352;

											{	/* Llib/thread.scm 390 */
												long BgL_arg1906z00_4353;

												BgL_arg1906z00_4353 =
													(BgL_idxz00_4351 + BgL_arg1919z00_4343);
												BgL_arg1904z00_4352 =
													VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													BgL_arg1906z00_4353);
											}
											BgL_test2680z00_6164 =
												(BgL_arg1904z00_4352 == BgL_classz00_4340);
									}}
								else
									{	/* Llib/thread.scm 390 */
										bool_t BgL_res2031z00_4358;

										{	/* Llib/thread.scm 390 */
											obj_t BgL_oclassz00_4362;

											{	/* Llib/thread.scm 390 */
												obj_t BgL_arg1925z00_4364;
												long BgL_arg1926z00_4365;

												BgL_arg1925z00_4364 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Llib/thread.scm 390 */
													long BgL_arg1927z00_4366;

													BgL_arg1927z00_4366 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1918z00_4342);
													BgL_arg1926z00_4365 =
														(BgL_arg1927z00_4366 - OBJECT_TYPE);
												}
												BgL_oclassz00_4362 =
													VECTOR_REF(BgL_arg1925z00_4364, BgL_arg1926z00_4365);
											}
											{	/* Llib/thread.scm 390 */
												bool_t BgL__ortest_1147z00_4372;

												BgL__ortest_1147z00_4372 =
													(BgL_classz00_4340 == BgL_oclassz00_4362);
												if (BgL__ortest_1147z00_4372)
													{	/* Llib/thread.scm 390 */
														BgL_res2031z00_4358 = BgL__ortest_1147z00_4372;
													}
												else
													{	/* Llib/thread.scm 390 */
														long BgL_odepthz00_4373;

														{	/* Llib/thread.scm 390 */
															obj_t BgL_arg1912z00_4374;

															BgL_arg1912z00_4374 = (BgL_oclassz00_4362);
															BgL_odepthz00_4373 =
																BGL_CLASS_DEPTH(BgL_arg1912z00_4374);
														}
														if ((BgL_arg1919z00_4343 < BgL_odepthz00_4373))
															{	/* Llib/thread.scm 390 */
																obj_t BgL_arg1910z00_4378;

																{	/* Llib/thread.scm 390 */
																	obj_t BgL_arg1911z00_4379;

																	BgL_arg1911z00_4379 = (BgL_oclassz00_4362);
																	BgL_arg1910z00_4378 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1911z00_4379,
																		BgL_arg1919z00_4343);
																}
																BgL_res2031z00_4358 =
																	(BgL_arg1910z00_4378 == BgL_classz00_4340);
															}
														else
															{	/* Llib/thread.scm 390 */
																BgL_res2031z00_4358 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2680z00_6164 = BgL_res2031z00_4358;
									}
							}
						else
							{	/* Llib/thread.scm 390 */
								BgL_test2680z00_6164 = ((bool_t) 0);
							}
					}
					if (BgL_test2680z00_6164)
						{	/* Llib/thread.scm 390 */
							BgL_auxz00_6163 = ((BgL_threadz00_bglt) BgL_thz00_3720);
						}
					else
						{
							obj_t BgL_auxz00_6189;

							BgL_auxz00_6189 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
								BINT(16146L), BGl_string2315z00zz__threadz00,
								BGl_string2158z00zz__threadz00, BgL_thz00_3720);
							FAILURE(BgL_auxz00_6189, BFALSE, BFALSE);
						}
				}
				return
					BGl_threadzd2startz12zc0zz__threadz00(BgL_auxz00_6163,
					BgL_scz00_3721);
			}
		}

	}



/* thread-start-joinable! */
	BGL_EXPORTED_DEF obj_t
		BGl_threadzd2startzd2joinablez12z12zz__threadz00(BgL_threadz00_bglt
		BgL_thz00_29)
	{
		{	/* Llib/thread.scm 395 */
			{	/* Llib/thread.scm 395 */
				obj_t BgL_method1201z00_1652;

				{	/* Llib/thread.scm 395 */
					obj_t BgL_res1968z00_2702;

					{	/* Llib/thread.scm 395 */
						long BgL_objzd2classzd2numz00_2673;

						BgL_objzd2classzd2numz00_2673 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_thz00_29));
						{	/* Llib/thread.scm 395 */
							obj_t BgL_arg1920z00_2674;

							BgL_arg1920z00_2674 =
								PROCEDURE_REF
								(BGl_threadzd2startzd2joinablez12zd2envzc0zz__threadz00,
								(int) (1L));
							{	/* Llib/thread.scm 395 */
								int BgL_offsetz00_2677;

								BgL_offsetz00_2677 = (int) (BgL_objzd2classzd2numz00_2673);
								{	/* Llib/thread.scm 395 */
									long BgL_offsetz00_2678;

									BgL_offsetz00_2678 =
										((long) (BgL_offsetz00_2677) - OBJECT_TYPE);
									{	/* Llib/thread.scm 395 */
										long BgL_modz00_2679;

										BgL_modz00_2679 =
											(BgL_offsetz00_2678 >> (int) ((long) ((int) (4L))));
										{	/* Llib/thread.scm 395 */
											long BgL_restz00_2681;

											BgL_restz00_2681 =
												(BgL_offsetz00_2678 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Llib/thread.scm 395 */

												{	/* Llib/thread.scm 395 */
													obj_t BgL_bucketz00_2683;

													BgL_bucketz00_2683 =
														VECTOR_REF(
														((obj_t) BgL_arg1920z00_2674), BgL_modz00_2679);
													BgL_res1968z00_2702 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2683), BgL_restz00_2681);
					}}}}}}}}
					BgL_method1201z00_1652 = BgL_res1968z00_2702;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1201z00_1652, ((obj_t) BgL_thz00_29));
			}
		}

	}



/* &thread-start-joinable! */
	obj_t BGl_z62threadzd2startzd2joinablez12z70zz__threadz00(obj_t
		BgL_envz00_3722, obj_t BgL_thz00_3723)
	{
		{	/* Llib/thread.scm 395 */
			{	/* Llib/thread.scm 395 */
				BgL_threadz00_bglt BgL_auxz00_6224;

				{	/* Llib/thread.scm 395 */
					bool_t BgL_test2685z00_6225;

					{	/* Llib/thread.scm 395 */
						obj_t BgL_classz00_4380;

						BgL_classz00_4380 = BGl_threadz00zz__threadz00;
						if (BGL_OBJECTP(BgL_thz00_3723))
							{	/* Llib/thread.scm 395 */
								BgL_objectz00_bglt BgL_arg1918z00_4382;
								long BgL_arg1919z00_4383;

								BgL_arg1918z00_4382 = (BgL_objectz00_bglt) (BgL_thz00_3723);
								BgL_arg1919z00_4383 = BGL_CLASS_DEPTH(BgL_classz00_4380);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Llib/thread.scm 395 */
										long BgL_idxz00_4391;

										BgL_idxz00_4391 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1918z00_4382);
										{	/* Llib/thread.scm 395 */
											obj_t BgL_arg1904z00_4392;

											{	/* Llib/thread.scm 395 */
												long BgL_arg1906z00_4393;

												BgL_arg1906z00_4393 =
													(BgL_idxz00_4391 + BgL_arg1919z00_4383);
												BgL_arg1904z00_4392 =
													VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													BgL_arg1906z00_4393);
											}
											BgL_test2685z00_6225 =
												(BgL_arg1904z00_4392 == BgL_classz00_4380);
									}}
								else
									{	/* Llib/thread.scm 395 */
										bool_t BgL_res2031z00_4398;

										{	/* Llib/thread.scm 395 */
											obj_t BgL_oclassz00_4402;

											{	/* Llib/thread.scm 395 */
												obj_t BgL_arg1925z00_4404;
												long BgL_arg1926z00_4405;

												BgL_arg1925z00_4404 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Llib/thread.scm 395 */
													long BgL_arg1927z00_4406;

													BgL_arg1927z00_4406 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1918z00_4382);
													BgL_arg1926z00_4405 =
														(BgL_arg1927z00_4406 - OBJECT_TYPE);
												}
												BgL_oclassz00_4402 =
													VECTOR_REF(BgL_arg1925z00_4404, BgL_arg1926z00_4405);
											}
											{	/* Llib/thread.scm 395 */
												bool_t BgL__ortest_1147z00_4412;

												BgL__ortest_1147z00_4412 =
													(BgL_classz00_4380 == BgL_oclassz00_4402);
												if (BgL__ortest_1147z00_4412)
													{	/* Llib/thread.scm 395 */
														BgL_res2031z00_4398 = BgL__ortest_1147z00_4412;
													}
												else
													{	/* Llib/thread.scm 395 */
														long BgL_odepthz00_4413;

														{	/* Llib/thread.scm 395 */
															obj_t BgL_arg1912z00_4414;

															BgL_arg1912z00_4414 = (BgL_oclassz00_4402);
															BgL_odepthz00_4413 =
																BGL_CLASS_DEPTH(BgL_arg1912z00_4414);
														}
														if ((BgL_arg1919z00_4383 < BgL_odepthz00_4413))
															{	/* Llib/thread.scm 395 */
																obj_t BgL_arg1910z00_4418;

																{	/* Llib/thread.scm 395 */
																	obj_t BgL_arg1911z00_4419;

																	BgL_arg1911z00_4419 = (BgL_oclassz00_4402);
																	BgL_arg1910z00_4418 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1911z00_4419,
																		BgL_arg1919z00_4383);
																}
																BgL_res2031z00_4398 =
																	(BgL_arg1910z00_4418 == BgL_classz00_4380);
															}
														else
															{	/* Llib/thread.scm 395 */
																BgL_res2031z00_4398 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2685z00_6225 = BgL_res2031z00_4398;
									}
							}
						else
							{	/* Llib/thread.scm 395 */
								BgL_test2685z00_6225 = ((bool_t) 0);
							}
					}
					if (BgL_test2685z00_6225)
						{	/* Llib/thread.scm 395 */
							BgL_auxz00_6224 = ((BgL_threadz00_bglt) BgL_thz00_3723);
						}
					else
						{
							obj_t BgL_auxz00_6250;

							BgL_auxz00_6250 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
								BINT(16418L), BGl_string2316z00zz__threadz00,
								BGl_string2158z00zz__threadz00, BgL_thz00_3723);
							FAILURE(BgL_auxz00_6250, BFALSE, BFALSE);
						}
				}
				return
					BGl_threadzd2startzd2joinablez12z12zz__threadz00(BgL_auxz00_6224);
			}
		}

	}



/* thread-join! */
	BGL_EXPORTED_DEF obj_t BGl_threadzd2joinz12zc0zz__threadz00(BgL_threadz00_bglt
		BgL_thz00_30, obj_t BgL_timeoutz00_31)
	{
		{	/* Llib/thread.scm 400 */
			{	/* Llib/thread.scm 400 */
				obj_t BgL_method1203z00_1653;

				{	/* Llib/thread.scm 400 */
					obj_t BgL_res1973z00_2733;

					{	/* Llib/thread.scm 400 */
						long BgL_objzd2classzd2numz00_2704;

						BgL_objzd2classzd2numz00_2704 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_thz00_30));
						{	/* Llib/thread.scm 400 */
							obj_t BgL_arg1920z00_2705;

							BgL_arg1920z00_2705 =
								PROCEDURE_REF(BGl_threadzd2joinz12zd2envz12zz__threadz00,
								(int) (1L));
							{	/* Llib/thread.scm 400 */
								int BgL_offsetz00_2708;

								BgL_offsetz00_2708 = (int) (BgL_objzd2classzd2numz00_2704);
								{	/* Llib/thread.scm 400 */
									long BgL_offsetz00_2709;

									BgL_offsetz00_2709 =
										((long) (BgL_offsetz00_2708) - OBJECT_TYPE);
									{	/* Llib/thread.scm 400 */
										long BgL_modz00_2710;

										BgL_modz00_2710 =
											(BgL_offsetz00_2709 >> (int) ((long) ((int) (4L))));
										{	/* Llib/thread.scm 400 */
											long BgL_restz00_2712;

											BgL_restz00_2712 =
												(BgL_offsetz00_2709 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Llib/thread.scm 400 */

												{	/* Llib/thread.scm 400 */
													obj_t BgL_bucketz00_2714;

													BgL_bucketz00_2714 =
														VECTOR_REF(
														((obj_t) BgL_arg1920z00_2705), BgL_modz00_2710);
													BgL_res1973z00_2733 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2714), BgL_restz00_2712);
					}}}}}}}}
					BgL_method1203z00_1653 = BgL_res1973z00_2733;
				}
				{	/* Llib/thread.scm 400 */
					obj_t BgL_auxz00_6280;

					{	/* Llib/thread.scm 400 */
						obj_t BgL_list1505z00_1654;

						BgL_list1505z00_1654 = MAKE_YOUNG_PAIR(BgL_timeoutz00_31, BNIL);
						BgL_auxz00_6280 =
							BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(
							((obj_t) BgL_thz00_30), BgL_list1505z00_1654);
					}
					return apply(BgL_method1203z00_1653, BgL_auxz00_6280);
				}
			}
		}

	}



/* &thread-join! */
	obj_t BGl_z62threadzd2joinz12za2zz__threadz00(obj_t BgL_envz00_3724,
		obj_t BgL_thz00_3725, obj_t BgL_timeoutz00_3726)
	{
		{	/* Llib/thread.scm 400 */
			{	/* Llib/thread.scm 400 */
				BgL_threadz00_bglt BgL_auxz00_6285;

				{	/* Llib/thread.scm 400 */
					bool_t BgL_test2690z00_6286;

					{	/* Llib/thread.scm 400 */
						obj_t BgL_classz00_4420;

						BgL_classz00_4420 = BGl_threadz00zz__threadz00;
						if (BGL_OBJECTP(BgL_thz00_3725))
							{	/* Llib/thread.scm 400 */
								BgL_objectz00_bglt BgL_arg1918z00_4422;
								long BgL_arg1919z00_4423;

								BgL_arg1918z00_4422 = (BgL_objectz00_bglt) (BgL_thz00_3725);
								BgL_arg1919z00_4423 = BGL_CLASS_DEPTH(BgL_classz00_4420);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Llib/thread.scm 400 */
										long BgL_idxz00_4431;

										BgL_idxz00_4431 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1918z00_4422);
										{	/* Llib/thread.scm 400 */
											obj_t BgL_arg1904z00_4432;

											{	/* Llib/thread.scm 400 */
												long BgL_arg1906z00_4433;

												BgL_arg1906z00_4433 =
													(BgL_idxz00_4431 + BgL_arg1919z00_4423);
												BgL_arg1904z00_4432 =
													VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													BgL_arg1906z00_4433);
											}
											BgL_test2690z00_6286 =
												(BgL_arg1904z00_4432 == BgL_classz00_4420);
									}}
								else
									{	/* Llib/thread.scm 400 */
										bool_t BgL_res2031z00_4438;

										{	/* Llib/thread.scm 400 */
											obj_t BgL_oclassz00_4442;

											{	/* Llib/thread.scm 400 */
												obj_t BgL_arg1925z00_4444;
												long BgL_arg1926z00_4445;

												BgL_arg1925z00_4444 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Llib/thread.scm 400 */
													long BgL_arg1927z00_4446;

													BgL_arg1927z00_4446 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1918z00_4422);
													BgL_arg1926z00_4445 =
														(BgL_arg1927z00_4446 - OBJECT_TYPE);
												}
												BgL_oclassz00_4442 =
													VECTOR_REF(BgL_arg1925z00_4444, BgL_arg1926z00_4445);
											}
											{	/* Llib/thread.scm 400 */
												bool_t BgL__ortest_1147z00_4452;

												BgL__ortest_1147z00_4452 =
													(BgL_classz00_4420 == BgL_oclassz00_4442);
												if (BgL__ortest_1147z00_4452)
													{	/* Llib/thread.scm 400 */
														BgL_res2031z00_4438 = BgL__ortest_1147z00_4452;
													}
												else
													{	/* Llib/thread.scm 400 */
														long BgL_odepthz00_4453;

														{	/* Llib/thread.scm 400 */
															obj_t BgL_arg1912z00_4454;

															BgL_arg1912z00_4454 = (BgL_oclassz00_4442);
															BgL_odepthz00_4453 =
																BGL_CLASS_DEPTH(BgL_arg1912z00_4454);
														}
														if ((BgL_arg1919z00_4423 < BgL_odepthz00_4453))
															{	/* Llib/thread.scm 400 */
																obj_t BgL_arg1910z00_4458;

																{	/* Llib/thread.scm 400 */
																	obj_t BgL_arg1911z00_4459;

																	BgL_arg1911z00_4459 = (BgL_oclassz00_4442);
																	BgL_arg1910z00_4458 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1911z00_4459,
																		BgL_arg1919z00_4423);
																}
																BgL_res2031z00_4438 =
																	(BgL_arg1910z00_4458 == BgL_classz00_4420);
															}
														else
															{	/* Llib/thread.scm 400 */
																BgL_res2031z00_4438 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2690z00_6286 = BgL_res2031z00_4438;
									}
							}
						else
							{	/* Llib/thread.scm 400 */
								BgL_test2690z00_6286 = ((bool_t) 0);
							}
					}
					if (BgL_test2690z00_6286)
						{	/* Llib/thread.scm 400 */
							BgL_auxz00_6285 = ((BgL_threadz00_bglt) BgL_thz00_3725);
						}
					else
						{
							obj_t BgL_auxz00_6311;

							BgL_auxz00_6311 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
								BINT(16694L), BGl_string2317z00zz__threadz00,
								BGl_string2158z00zz__threadz00, BgL_thz00_3725);
							FAILURE(BgL_auxz00_6311, BFALSE, BFALSE);
						}
				}
				return
					BGl_threadzd2joinz12zc0zz__threadz00(BgL_auxz00_6285,
					BgL_timeoutz00_3726);
			}
		}

	}



/* thread-terminate! */
	BGL_EXPORTED_DEF obj_t
		BGl_threadzd2terminatez12zc0zz__threadz00(BgL_threadz00_bglt BgL_thz00_32)
	{
		{	/* Llib/thread.scm 405 */
			{	/* Llib/thread.scm 405 */
				obj_t BgL_method1206z00_1655;

				{	/* Llib/thread.scm 405 */
					obj_t BgL_res1978z00_2764;

					{	/* Llib/thread.scm 405 */
						long BgL_objzd2classzd2numz00_2735;

						BgL_objzd2classzd2numz00_2735 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_thz00_32));
						{	/* Llib/thread.scm 405 */
							obj_t BgL_arg1920z00_2736;

							BgL_arg1920z00_2736 =
								PROCEDURE_REF(BGl_threadzd2terminatez12zd2envz12zz__threadz00,
								(int) (1L));
							{	/* Llib/thread.scm 405 */
								int BgL_offsetz00_2739;

								BgL_offsetz00_2739 = (int) (BgL_objzd2classzd2numz00_2735);
								{	/* Llib/thread.scm 405 */
									long BgL_offsetz00_2740;

									BgL_offsetz00_2740 =
										((long) (BgL_offsetz00_2739) - OBJECT_TYPE);
									{	/* Llib/thread.scm 405 */
										long BgL_modz00_2741;

										BgL_modz00_2741 =
											(BgL_offsetz00_2740 >> (int) ((long) ((int) (4L))));
										{	/* Llib/thread.scm 405 */
											long BgL_restz00_2743;

											BgL_restz00_2743 =
												(BgL_offsetz00_2740 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Llib/thread.scm 405 */

												{	/* Llib/thread.scm 405 */
													obj_t BgL_bucketz00_2745;

													BgL_bucketz00_2745 =
														VECTOR_REF(
														((obj_t) BgL_arg1920z00_2736), BgL_modz00_2741);
													BgL_res1978z00_2764 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2745), BgL_restz00_2743);
					}}}}}}}}
					BgL_method1206z00_1655 = BgL_res1978z00_2764;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1206z00_1655, ((obj_t) BgL_thz00_32));
			}
		}

	}



/* &thread-terminate! */
	obj_t BGl_z62threadzd2terminatez12za2zz__threadz00(obj_t BgL_envz00_3727,
		obj_t BgL_thz00_3728)
	{
		{	/* Llib/thread.scm 405 */
			{	/* Llib/thread.scm 405 */
				BgL_threadz00_bglt BgL_auxz00_6346;

				{	/* Llib/thread.scm 405 */
					bool_t BgL_test2695z00_6347;

					{	/* Llib/thread.scm 405 */
						obj_t BgL_classz00_4460;

						BgL_classz00_4460 = BGl_threadz00zz__threadz00;
						if (BGL_OBJECTP(BgL_thz00_3728))
							{	/* Llib/thread.scm 405 */
								BgL_objectz00_bglt BgL_arg1918z00_4462;
								long BgL_arg1919z00_4463;

								BgL_arg1918z00_4462 = (BgL_objectz00_bglt) (BgL_thz00_3728);
								BgL_arg1919z00_4463 = BGL_CLASS_DEPTH(BgL_classz00_4460);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Llib/thread.scm 405 */
										long BgL_idxz00_4471;

										BgL_idxz00_4471 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1918z00_4462);
										{	/* Llib/thread.scm 405 */
											obj_t BgL_arg1904z00_4472;

											{	/* Llib/thread.scm 405 */
												long BgL_arg1906z00_4473;

												BgL_arg1906z00_4473 =
													(BgL_idxz00_4471 + BgL_arg1919z00_4463);
												BgL_arg1904z00_4472 =
													VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													BgL_arg1906z00_4473);
											}
											BgL_test2695z00_6347 =
												(BgL_arg1904z00_4472 == BgL_classz00_4460);
									}}
								else
									{	/* Llib/thread.scm 405 */
										bool_t BgL_res2031z00_4478;

										{	/* Llib/thread.scm 405 */
											obj_t BgL_oclassz00_4482;

											{	/* Llib/thread.scm 405 */
												obj_t BgL_arg1925z00_4484;
												long BgL_arg1926z00_4485;

												BgL_arg1925z00_4484 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Llib/thread.scm 405 */
													long BgL_arg1927z00_4486;

													BgL_arg1927z00_4486 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1918z00_4462);
													BgL_arg1926z00_4485 =
														(BgL_arg1927z00_4486 - OBJECT_TYPE);
												}
												BgL_oclassz00_4482 =
													VECTOR_REF(BgL_arg1925z00_4484, BgL_arg1926z00_4485);
											}
											{	/* Llib/thread.scm 405 */
												bool_t BgL__ortest_1147z00_4492;

												BgL__ortest_1147z00_4492 =
													(BgL_classz00_4460 == BgL_oclassz00_4482);
												if (BgL__ortest_1147z00_4492)
													{	/* Llib/thread.scm 405 */
														BgL_res2031z00_4478 = BgL__ortest_1147z00_4492;
													}
												else
													{	/* Llib/thread.scm 405 */
														long BgL_odepthz00_4493;

														{	/* Llib/thread.scm 405 */
															obj_t BgL_arg1912z00_4494;

															BgL_arg1912z00_4494 = (BgL_oclassz00_4482);
															BgL_odepthz00_4493 =
																BGL_CLASS_DEPTH(BgL_arg1912z00_4494);
														}
														if ((BgL_arg1919z00_4463 < BgL_odepthz00_4493))
															{	/* Llib/thread.scm 405 */
																obj_t BgL_arg1910z00_4498;

																{	/* Llib/thread.scm 405 */
																	obj_t BgL_arg1911z00_4499;

																	BgL_arg1911z00_4499 = (BgL_oclassz00_4482);
																	BgL_arg1910z00_4498 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1911z00_4499,
																		BgL_arg1919z00_4463);
																}
																BgL_res2031z00_4478 =
																	(BgL_arg1910z00_4498 == BgL_classz00_4460);
															}
														else
															{	/* Llib/thread.scm 405 */
																BgL_res2031z00_4478 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2695z00_6347 = BgL_res2031z00_4478;
									}
							}
						else
							{	/* Llib/thread.scm 405 */
								BgL_test2695z00_6347 = ((bool_t) 0);
							}
					}
					if (BgL_test2695z00_6347)
						{	/* Llib/thread.scm 405 */
							BgL_auxz00_6346 = ((BgL_threadz00_bglt) BgL_thz00_3728);
						}
					else
						{
							obj_t BgL_auxz00_6372;

							BgL_auxz00_6372 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
								BINT(16970L), BGl_string2318z00zz__threadz00,
								BGl_string2158z00zz__threadz00, BgL_thz00_3728);
							FAILURE(BgL_auxz00_6372, BFALSE, BFALSE);
						}
				}
				return BGl_threadzd2terminatez12zc0zz__threadz00(BgL_auxz00_6346);
			}
		}

	}



/* thread-kill! */
	BGL_EXPORTED_DEF obj_t BGl_threadzd2killz12zc0zz__threadz00(BgL_threadz00_bglt
		BgL_thz00_33, int BgL_nz00_34)
	{
		{	/* Llib/thread.scm 410 */
			{	/* Llib/thread.scm 410 */
				obj_t BgL_method1208z00_1656;

				{	/* Llib/thread.scm 410 */
					obj_t BgL_res1983z00_2795;

					{	/* Llib/thread.scm 410 */
						long BgL_objzd2classzd2numz00_2766;

						BgL_objzd2classzd2numz00_2766 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_thz00_33));
						{	/* Llib/thread.scm 410 */
							obj_t BgL_arg1920z00_2767;

							BgL_arg1920z00_2767 =
								PROCEDURE_REF(BGl_threadzd2killz12zd2envz12zz__threadz00,
								(int) (1L));
							{	/* Llib/thread.scm 410 */
								int BgL_offsetz00_2770;

								BgL_offsetz00_2770 = (int) (BgL_objzd2classzd2numz00_2766);
								{	/* Llib/thread.scm 410 */
									long BgL_offsetz00_2771;

									BgL_offsetz00_2771 =
										((long) (BgL_offsetz00_2770) - OBJECT_TYPE);
									{	/* Llib/thread.scm 410 */
										long BgL_modz00_2772;

										BgL_modz00_2772 =
											(BgL_offsetz00_2771 >> (int) ((long) ((int) (4L))));
										{	/* Llib/thread.scm 410 */
											long BgL_restz00_2774;

											BgL_restz00_2774 =
												(BgL_offsetz00_2771 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Llib/thread.scm 410 */

												{	/* Llib/thread.scm 410 */
													obj_t BgL_bucketz00_2776;

													BgL_bucketz00_2776 =
														VECTOR_REF(
														((obj_t) BgL_arg1920z00_2767), BgL_modz00_2772);
													BgL_res1983z00_2795 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2776), BgL_restz00_2774);
					}}}}}}}}
					BgL_method1208z00_1656 = BgL_res1983z00_2795;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1208z00_1656,
					((obj_t) BgL_thz00_33), BINT(BgL_nz00_34));
			}
		}

	}



/* &thread-kill! */
	obj_t BGl_z62threadzd2killz12za2zz__threadz00(obj_t BgL_envz00_3729,
		obj_t BgL_thz00_3730, obj_t BgL_nz00_3731)
	{
		{	/* Llib/thread.scm 410 */
			{	/* Llib/thread.scm 410 */
				int BgL_auxz00_6439;
				BgL_threadz00_bglt BgL_auxz00_6409;

				{	/* Llib/thread.scm 410 */
					obj_t BgL_tmpz00_6440;

					if (INTEGERP(BgL_nz00_3731))
						{	/* Llib/thread.scm 410 */
							BgL_tmpz00_6440 = BgL_nz00_3731;
						}
					else
						{
							obj_t BgL_auxz00_6443;

							BgL_auxz00_6443 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
								BINT(17241L), BGl_string2319z00zz__threadz00,
								BGl_string2170z00zz__threadz00, BgL_nz00_3731);
							FAILURE(BgL_auxz00_6443, BFALSE, BFALSE);
						}
					BgL_auxz00_6439 = CINT(BgL_tmpz00_6440);
				}
				{	/* Llib/thread.scm 410 */
					bool_t BgL_test2700z00_6410;

					{	/* Llib/thread.scm 410 */
						obj_t BgL_classz00_4500;

						BgL_classz00_4500 = BGl_threadz00zz__threadz00;
						if (BGL_OBJECTP(BgL_thz00_3730))
							{	/* Llib/thread.scm 410 */
								BgL_objectz00_bglt BgL_arg1918z00_4502;
								long BgL_arg1919z00_4503;

								BgL_arg1918z00_4502 = (BgL_objectz00_bglt) (BgL_thz00_3730);
								BgL_arg1919z00_4503 = BGL_CLASS_DEPTH(BgL_classz00_4500);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Llib/thread.scm 410 */
										long BgL_idxz00_4511;

										BgL_idxz00_4511 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1918z00_4502);
										{	/* Llib/thread.scm 410 */
											obj_t BgL_arg1904z00_4512;

											{	/* Llib/thread.scm 410 */
												long BgL_arg1906z00_4513;

												BgL_arg1906z00_4513 =
													(BgL_idxz00_4511 + BgL_arg1919z00_4503);
												BgL_arg1904z00_4512 =
													VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													BgL_arg1906z00_4513);
											}
											BgL_test2700z00_6410 =
												(BgL_arg1904z00_4512 == BgL_classz00_4500);
									}}
								else
									{	/* Llib/thread.scm 410 */
										bool_t BgL_res2031z00_4518;

										{	/* Llib/thread.scm 410 */
											obj_t BgL_oclassz00_4522;

											{	/* Llib/thread.scm 410 */
												obj_t BgL_arg1925z00_4524;
												long BgL_arg1926z00_4525;

												BgL_arg1925z00_4524 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Llib/thread.scm 410 */
													long BgL_arg1927z00_4526;

													BgL_arg1927z00_4526 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1918z00_4502);
													BgL_arg1926z00_4525 =
														(BgL_arg1927z00_4526 - OBJECT_TYPE);
												}
												BgL_oclassz00_4522 =
													VECTOR_REF(BgL_arg1925z00_4524, BgL_arg1926z00_4525);
											}
											{	/* Llib/thread.scm 410 */
												bool_t BgL__ortest_1147z00_4532;

												BgL__ortest_1147z00_4532 =
													(BgL_classz00_4500 == BgL_oclassz00_4522);
												if (BgL__ortest_1147z00_4532)
													{	/* Llib/thread.scm 410 */
														BgL_res2031z00_4518 = BgL__ortest_1147z00_4532;
													}
												else
													{	/* Llib/thread.scm 410 */
														long BgL_odepthz00_4533;

														{	/* Llib/thread.scm 410 */
															obj_t BgL_arg1912z00_4534;

															BgL_arg1912z00_4534 = (BgL_oclassz00_4522);
															BgL_odepthz00_4533 =
																BGL_CLASS_DEPTH(BgL_arg1912z00_4534);
														}
														if ((BgL_arg1919z00_4503 < BgL_odepthz00_4533))
															{	/* Llib/thread.scm 410 */
																obj_t BgL_arg1910z00_4538;

																{	/* Llib/thread.scm 410 */
																	obj_t BgL_arg1911z00_4539;

																	BgL_arg1911z00_4539 = (BgL_oclassz00_4522);
																	BgL_arg1910z00_4538 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1911z00_4539,
																		BgL_arg1919z00_4503);
																}
																BgL_res2031z00_4518 =
																	(BgL_arg1910z00_4538 == BgL_classz00_4500);
															}
														else
															{	/* Llib/thread.scm 410 */
																BgL_res2031z00_4518 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2700z00_6410 = BgL_res2031z00_4518;
									}
							}
						else
							{	/* Llib/thread.scm 410 */
								BgL_test2700z00_6410 = ((bool_t) 0);
							}
					}
					if (BgL_test2700z00_6410)
						{	/* Llib/thread.scm 410 */
							BgL_auxz00_6409 = ((BgL_threadz00_bglt) BgL_thz00_3730);
						}
					else
						{
							obj_t BgL_auxz00_6435;

							BgL_auxz00_6435 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
								BINT(17241L), BGl_string2319z00zz__threadz00,
								BGl_string2158z00zz__threadz00, BgL_thz00_3730);
							FAILURE(BgL_auxz00_6435, BFALSE, BFALSE);
						}
				}
				return
					BGl_threadzd2killz12zc0zz__threadz00(BgL_auxz00_6409,
					BgL_auxz00_6439);
			}
		}

	}



/* thread-specific */
	BGL_EXPORTED_DEF obj_t
		BGl_threadzd2specificzd2zz__threadz00(BgL_threadz00_bglt BgL_thz00_35)
	{
		{	/* Llib/thread.scm 415 */
			{	/* Llib/thread.scm 415 */
				obj_t BgL_method1210z00_1657;

				{	/* Llib/thread.scm 415 */
					obj_t BgL_res1988z00_2826;

					{	/* Llib/thread.scm 415 */
						long BgL_objzd2classzd2numz00_2797;

						BgL_objzd2classzd2numz00_2797 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_thz00_35));
						{	/* Llib/thread.scm 415 */
							obj_t BgL_arg1920z00_2798;

							BgL_arg1920z00_2798 =
								PROCEDURE_REF(BGl_threadzd2specificzd2envz00zz__threadz00,
								(int) (1L));
							{	/* Llib/thread.scm 415 */
								int BgL_offsetz00_2801;

								BgL_offsetz00_2801 = (int) (BgL_objzd2classzd2numz00_2797);
								{	/* Llib/thread.scm 415 */
									long BgL_offsetz00_2802;

									BgL_offsetz00_2802 =
										((long) (BgL_offsetz00_2801) - OBJECT_TYPE);
									{	/* Llib/thread.scm 415 */
										long BgL_modz00_2803;

										BgL_modz00_2803 =
											(BgL_offsetz00_2802 >> (int) ((long) ((int) (4L))));
										{	/* Llib/thread.scm 415 */
											long BgL_restz00_2805;

											BgL_restz00_2805 =
												(BgL_offsetz00_2802 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Llib/thread.scm 415 */

												{	/* Llib/thread.scm 415 */
													obj_t BgL_bucketz00_2807;

													BgL_bucketz00_2807 =
														VECTOR_REF(
														((obj_t) BgL_arg1920z00_2798), BgL_modz00_2803);
													BgL_res1988z00_2826 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2807), BgL_restz00_2805);
					}}}}}}}}
					BgL_method1210z00_1657 = BgL_res1988z00_2826;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1210z00_1657, ((obj_t) BgL_thz00_35));
			}
		}

	}



/* &thread-specific */
	obj_t BGl_z62threadzd2specificzb0zz__threadz00(obj_t BgL_envz00_3732,
		obj_t BgL_thz00_3733)
	{
		{	/* Llib/thread.scm 415 */
			{	/* Llib/thread.scm 415 */
				BgL_threadz00_bglt BgL_auxz00_6479;

				{	/* Llib/thread.scm 415 */
					bool_t BgL_test2706z00_6480;

					{	/* Llib/thread.scm 415 */
						obj_t BgL_classz00_4540;

						BgL_classz00_4540 = BGl_threadz00zz__threadz00;
						if (BGL_OBJECTP(BgL_thz00_3733))
							{	/* Llib/thread.scm 415 */
								BgL_objectz00_bglt BgL_arg1918z00_4542;
								long BgL_arg1919z00_4543;

								BgL_arg1918z00_4542 = (BgL_objectz00_bglt) (BgL_thz00_3733);
								BgL_arg1919z00_4543 = BGL_CLASS_DEPTH(BgL_classz00_4540);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Llib/thread.scm 415 */
										long BgL_idxz00_4551;

										BgL_idxz00_4551 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1918z00_4542);
										{	/* Llib/thread.scm 415 */
											obj_t BgL_arg1904z00_4552;

											{	/* Llib/thread.scm 415 */
												long BgL_arg1906z00_4553;

												BgL_arg1906z00_4553 =
													(BgL_idxz00_4551 + BgL_arg1919z00_4543);
												BgL_arg1904z00_4552 =
													VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													BgL_arg1906z00_4553);
											}
											BgL_test2706z00_6480 =
												(BgL_arg1904z00_4552 == BgL_classz00_4540);
									}}
								else
									{	/* Llib/thread.scm 415 */
										bool_t BgL_res2031z00_4558;

										{	/* Llib/thread.scm 415 */
											obj_t BgL_oclassz00_4562;

											{	/* Llib/thread.scm 415 */
												obj_t BgL_arg1925z00_4564;
												long BgL_arg1926z00_4565;

												BgL_arg1925z00_4564 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Llib/thread.scm 415 */
													long BgL_arg1927z00_4566;

													BgL_arg1927z00_4566 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1918z00_4542);
													BgL_arg1926z00_4565 =
														(BgL_arg1927z00_4566 - OBJECT_TYPE);
												}
												BgL_oclassz00_4562 =
													VECTOR_REF(BgL_arg1925z00_4564, BgL_arg1926z00_4565);
											}
											{	/* Llib/thread.scm 415 */
												bool_t BgL__ortest_1147z00_4572;

												BgL__ortest_1147z00_4572 =
													(BgL_classz00_4540 == BgL_oclassz00_4562);
												if (BgL__ortest_1147z00_4572)
													{	/* Llib/thread.scm 415 */
														BgL_res2031z00_4558 = BgL__ortest_1147z00_4572;
													}
												else
													{	/* Llib/thread.scm 415 */
														long BgL_odepthz00_4573;

														{	/* Llib/thread.scm 415 */
															obj_t BgL_arg1912z00_4574;

															BgL_arg1912z00_4574 = (BgL_oclassz00_4562);
															BgL_odepthz00_4573 =
																BGL_CLASS_DEPTH(BgL_arg1912z00_4574);
														}
														if ((BgL_arg1919z00_4543 < BgL_odepthz00_4573))
															{	/* Llib/thread.scm 415 */
																obj_t BgL_arg1910z00_4578;

																{	/* Llib/thread.scm 415 */
																	obj_t BgL_arg1911z00_4579;

																	BgL_arg1911z00_4579 = (BgL_oclassz00_4562);
																	BgL_arg1910z00_4578 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1911z00_4579,
																		BgL_arg1919z00_4543);
																}
																BgL_res2031z00_4558 =
																	(BgL_arg1910z00_4578 == BgL_classz00_4540);
															}
														else
															{	/* Llib/thread.scm 415 */
																BgL_res2031z00_4558 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2706z00_6480 = BgL_res2031z00_4558;
									}
							}
						else
							{	/* Llib/thread.scm 415 */
								BgL_test2706z00_6480 = ((bool_t) 0);
							}
					}
					if (BgL_test2706z00_6480)
						{	/* Llib/thread.scm 415 */
							BgL_auxz00_6479 = ((BgL_threadz00_bglt) BgL_thz00_3733);
						}
					else
						{
							obj_t BgL_auxz00_6505;

							BgL_auxz00_6505 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
								BINT(17514L), BGl_string2320z00zz__threadz00,
								BGl_string2158z00zz__threadz00, BgL_thz00_3733);
							FAILURE(BgL_auxz00_6505, BFALSE, BFALSE);
						}
				}
				return BGl_threadzd2specificzd2zz__threadz00(BgL_auxz00_6479);
			}
		}

	}



/* thread-specific-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_threadzd2specificzd2setz12z12zz__threadz00(BgL_threadz00_bglt
		BgL_thz00_36, obj_t BgL_vz00_37)
	{
		{	/* Llib/thread.scm 420 */
			{	/* Llib/thread.scm 420 */
				obj_t BgL_method1212z00_1658;

				{	/* Llib/thread.scm 420 */
					obj_t BgL_res1993z00_2857;

					{	/* Llib/thread.scm 420 */
						long BgL_objzd2classzd2numz00_2828;

						BgL_objzd2classzd2numz00_2828 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_thz00_36));
						{	/* Llib/thread.scm 420 */
							obj_t BgL_arg1920z00_2829;

							BgL_arg1920z00_2829 =
								PROCEDURE_REF
								(BGl_threadzd2specificzd2setz12zd2envzc0zz__threadz00,
								(int) (1L));
							{	/* Llib/thread.scm 420 */
								int BgL_offsetz00_2832;

								BgL_offsetz00_2832 = (int) (BgL_objzd2classzd2numz00_2828);
								{	/* Llib/thread.scm 420 */
									long BgL_offsetz00_2833;

									BgL_offsetz00_2833 =
										((long) (BgL_offsetz00_2832) - OBJECT_TYPE);
									{	/* Llib/thread.scm 420 */
										long BgL_modz00_2834;

										BgL_modz00_2834 =
											(BgL_offsetz00_2833 >> (int) ((long) ((int) (4L))));
										{	/* Llib/thread.scm 420 */
											long BgL_restz00_2836;

											BgL_restz00_2836 =
												(BgL_offsetz00_2833 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Llib/thread.scm 420 */

												{	/* Llib/thread.scm 420 */
													obj_t BgL_bucketz00_2838;

													BgL_bucketz00_2838 =
														VECTOR_REF(
														((obj_t) BgL_arg1920z00_2829), BgL_modz00_2834);
													BgL_res1993z00_2857 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2838), BgL_restz00_2836);
					}}}}}}}}
					BgL_method1212z00_1658 = BgL_res1993z00_2857;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1212z00_1658,
					((obj_t) BgL_thz00_36), BgL_vz00_37);
			}
		}

	}



/* &thread-specific-set! */
	obj_t BGl_z62threadzd2specificzd2setz12z70zz__threadz00(obj_t BgL_envz00_3734,
		obj_t BgL_thz00_3735, obj_t BgL_vz00_3736)
	{
		{	/* Llib/thread.scm 420 */
			{	/* Llib/thread.scm 420 */
				BgL_threadz00_bglt BgL_auxz00_6541;

				{	/* Llib/thread.scm 420 */
					bool_t BgL_test2711z00_6542;

					{	/* Llib/thread.scm 420 */
						obj_t BgL_classz00_4580;

						BgL_classz00_4580 = BGl_threadz00zz__threadz00;
						if (BGL_OBJECTP(BgL_thz00_3735))
							{	/* Llib/thread.scm 420 */
								BgL_objectz00_bglt BgL_arg1918z00_4582;
								long BgL_arg1919z00_4583;

								BgL_arg1918z00_4582 = (BgL_objectz00_bglt) (BgL_thz00_3735);
								BgL_arg1919z00_4583 = BGL_CLASS_DEPTH(BgL_classz00_4580);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Llib/thread.scm 420 */
										long BgL_idxz00_4591;

										BgL_idxz00_4591 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1918z00_4582);
										{	/* Llib/thread.scm 420 */
											obj_t BgL_arg1904z00_4592;

											{	/* Llib/thread.scm 420 */
												long BgL_arg1906z00_4593;

												BgL_arg1906z00_4593 =
													(BgL_idxz00_4591 + BgL_arg1919z00_4583);
												BgL_arg1904z00_4592 =
													VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													BgL_arg1906z00_4593);
											}
											BgL_test2711z00_6542 =
												(BgL_arg1904z00_4592 == BgL_classz00_4580);
									}}
								else
									{	/* Llib/thread.scm 420 */
										bool_t BgL_res2031z00_4598;

										{	/* Llib/thread.scm 420 */
											obj_t BgL_oclassz00_4602;

											{	/* Llib/thread.scm 420 */
												obj_t BgL_arg1925z00_4604;
												long BgL_arg1926z00_4605;

												BgL_arg1925z00_4604 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Llib/thread.scm 420 */
													long BgL_arg1927z00_4606;

													BgL_arg1927z00_4606 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1918z00_4582);
													BgL_arg1926z00_4605 =
														(BgL_arg1927z00_4606 - OBJECT_TYPE);
												}
												BgL_oclassz00_4602 =
													VECTOR_REF(BgL_arg1925z00_4604, BgL_arg1926z00_4605);
											}
											{	/* Llib/thread.scm 420 */
												bool_t BgL__ortest_1147z00_4612;

												BgL__ortest_1147z00_4612 =
													(BgL_classz00_4580 == BgL_oclassz00_4602);
												if (BgL__ortest_1147z00_4612)
													{	/* Llib/thread.scm 420 */
														BgL_res2031z00_4598 = BgL__ortest_1147z00_4612;
													}
												else
													{	/* Llib/thread.scm 420 */
														long BgL_odepthz00_4613;

														{	/* Llib/thread.scm 420 */
															obj_t BgL_arg1912z00_4614;

															BgL_arg1912z00_4614 = (BgL_oclassz00_4602);
															BgL_odepthz00_4613 =
																BGL_CLASS_DEPTH(BgL_arg1912z00_4614);
														}
														if ((BgL_arg1919z00_4583 < BgL_odepthz00_4613))
															{	/* Llib/thread.scm 420 */
																obj_t BgL_arg1910z00_4618;

																{	/* Llib/thread.scm 420 */
																	obj_t BgL_arg1911z00_4619;

																	BgL_arg1911z00_4619 = (BgL_oclassz00_4602);
																	BgL_arg1910z00_4618 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1911z00_4619,
																		BgL_arg1919z00_4583);
																}
																BgL_res2031z00_4598 =
																	(BgL_arg1910z00_4618 == BgL_classz00_4580);
															}
														else
															{	/* Llib/thread.scm 420 */
																BgL_res2031z00_4598 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2711z00_6542 = BgL_res2031z00_4598;
									}
							}
						else
							{	/* Llib/thread.scm 420 */
								BgL_test2711z00_6542 = ((bool_t) 0);
							}
					}
					if (BgL_test2711z00_6542)
						{	/* Llib/thread.scm 420 */
							BgL_auxz00_6541 = ((BgL_threadz00_bglt) BgL_thz00_3735);
						}
					else
						{
							obj_t BgL_auxz00_6567;

							BgL_auxz00_6567 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
								BINT(17783L), BGl_string2321z00zz__threadz00,
								BGl_string2158z00zz__threadz00, BgL_thz00_3735);
							FAILURE(BgL_auxz00_6567, BFALSE, BFALSE);
						}
				}
				return
					BGl_threadzd2specificzd2setz12z12zz__threadz00(BgL_auxz00_6541,
					BgL_vz00_3736);
			}
		}

	}



/* thread-cleanup */
	BGL_EXPORTED_DEF obj_t BGl_threadzd2cleanupzd2zz__threadz00(BgL_threadz00_bglt
		BgL_thz00_38)
	{
		{	/* Llib/thread.scm 425 */
			{	/* Llib/thread.scm 425 */
				obj_t BgL_method1214z00_1659;

				{	/* Llib/thread.scm 425 */
					obj_t BgL_res1998z00_2888;

					{	/* Llib/thread.scm 425 */
						long BgL_objzd2classzd2numz00_2859;

						BgL_objzd2classzd2numz00_2859 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_thz00_38));
						{	/* Llib/thread.scm 425 */
							obj_t BgL_arg1920z00_2860;

							BgL_arg1920z00_2860 =
								PROCEDURE_REF(BGl_threadzd2cleanupzd2envz00zz__threadz00,
								(int) (1L));
							{	/* Llib/thread.scm 425 */
								int BgL_offsetz00_2863;

								BgL_offsetz00_2863 = (int) (BgL_objzd2classzd2numz00_2859);
								{	/* Llib/thread.scm 425 */
									long BgL_offsetz00_2864;

									BgL_offsetz00_2864 =
										((long) (BgL_offsetz00_2863) - OBJECT_TYPE);
									{	/* Llib/thread.scm 425 */
										long BgL_modz00_2865;

										BgL_modz00_2865 =
											(BgL_offsetz00_2864 >> (int) ((long) ((int) (4L))));
										{	/* Llib/thread.scm 425 */
											long BgL_restz00_2867;

											BgL_restz00_2867 =
												(BgL_offsetz00_2864 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Llib/thread.scm 425 */

												{	/* Llib/thread.scm 425 */
													obj_t BgL_bucketz00_2869;

													BgL_bucketz00_2869 =
														VECTOR_REF(
														((obj_t) BgL_arg1920z00_2860), BgL_modz00_2865);
													BgL_res1998z00_2888 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2869), BgL_restz00_2867);
					}}}}}}}}
					BgL_method1214z00_1659 = BgL_res1998z00_2888;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1214z00_1659, ((obj_t) BgL_thz00_38));
			}
		}

	}



/* &thread-cleanup */
	obj_t BGl_z62threadzd2cleanupzb0zz__threadz00(obj_t BgL_envz00_3737,
		obj_t BgL_thz00_3738)
	{
		{	/* Llib/thread.scm 425 */
			{	/* Llib/thread.scm 425 */
				BgL_threadz00_bglt BgL_auxz00_6602;

				{	/* Llib/thread.scm 425 */
					bool_t BgL_test2716z00_6603;

					{	/* Llib/thread.scm 425 */
						obj_t BgL_classz00_4620;

						BgL_classz00_4620 = BGl_threadz00zz__threadz00;
						if (BGL_OBJECTP(BgL_thz00_3738))
							{	/* Llib/thread.scm 425 */
								BgL_objectz00_bglt BgL_arg1918z00_4622;
								long BgL_arg1919z00_4623;

								BgL_arg1918z00_4622 = (BgL_objectz00_bglt) (BgL_thz00_3738);
								BgL_arg1919z00_4623 = BGL_CLASS_DEPTH(BgL_classz00_4620);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Llib/thread.scm 425 */
										long BgL_idxz00_4631;

										BgL_idxz00_4631 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1918z00_4622);
										{	/* Llib/thread.scm 425 */
											obj_t BgL_arg1904z00_4632;

											{	/* Llib/thread.scm 425 */
												long BgL_arg1906z00_4633;

												BgL_arg1906z00_4633 =
													(BgL_idxz00_4631 + BgL_arg1919z00_4623);
												BgL_arg1904z00_4632 =
													VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													BgL_arg1906z00_4633);
											}
											BgL_test2716z00_6603 =
												(BgL_arg1904z00_4632 == BgL_classz00_4620);
									}}
								else
									{	/* Llib/thread.scm 425 */
										bool_t BgL_res2031z00_4638;

										{	/* Llib/thread.scm 425 */
											obj_t BgL_oclassz00_4642;

											{	/* Llib/thread.scm 425 */
												obj_t BgL_arg1925z00_4644;
												long BgL_arg1926z00_4645;

												BgL_arg1925z00_4644 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Llib/thread.scm 425 */
													long BgL_arg1927z00_4646;

													BgL_arg1927z00_4646 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1918z00_4622);
													BgL_arg1926z00_4645 =
														(BgL_arg1927z00_4646 - OBJECT_TYPE);
												}
												BgL_oclassz00_4642 =
													VECTOR_REF(BgL_arg1925z00_4644, BgL_arg1926z00_4645);
											}
											{	/* Llib/thread.scm 425 */
												bool_t BgL__ortest_1147z00_4652;

												BgL__ortest_1147z00_4652 =
													(BgL_classz00_4620 == BgL_oclassz00_4642);
												if (BgL__ortest_1147z00_4652)
													{	/* Llib/thread.scm 425 */
														BgL_res2031z00_4638 = BgL__ortest_1147z00_4652;
													}
												else
													{	/* Llib/thread.scm 425 */
														long BgL_odepthz00_4653;

														{	/* Llib/thread.scm 425 */
															obj_t BgL_arg1912z00_4654;

															BgL_arg1912z00_4654 = (BgL_oclassz00_4642);
															BgL_odepthz00_4653 =
																BGL_CLASS_DEPTH(BgL_arg1912z00_4654);
														}
														if ((BgL_arg1919z00_4623 < BgL_odepthz00_4653))
															{	/* Llib/thread.scm 425 */
																obj_t BgL_arg1910z00_4658;

																{	/* Llib/thread.scm 425 */
																	obj_t BgL_arg1911z00_4659;

																	BgL_arg1911z00_4659 = (BgL_oclassz00_4642);
																	BgL_arg1910z00_4658 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1911z00_4659,
																		BgL_arg1919z00_4623);
																}
																BgL_res2031z00_4638 =
																	(BgL_arg1910z00_4658 == BgL_classz00_4620);
															}
														else
															{	/* Llib/thread.scm 425 */
																BgL_res2031z00_4638 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2716z00_6603 = BgL_res2031z00_4638;
									}
							}
						else
							{	/* Llib/thread.scm 425 */
								BgL_test2716z00_6603 = ((bool_t) 0);
							}
					}
					if (BgL_test2716z00_6603)
						{	/* Llib/thread.scm 425 */
							BgL_auxz00_6602 = ((BgL_threadz00_bglt) BgL_thz00_3738);
						}
					else
						{
							obj_t BgL_auxz00_6628;

							BgL_auxz00_6628 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
								BINT(18064L), BGl_string2322z00zz__threadz00,
								BGl_string2158z00zz__threadz00, BgL_thz00_3738);
							FAILURE(BgL_auxz00_6628, BFALSE, BFALSE);
						}
				}
				return BGl_threadzd2cleanupzd2zz__threadz00(BgL_auxz00_6602);
			}
		}

	}



/* thread-cleanup-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_threadzd2cleanupzd2setz12z12zz__threadz00(BgL_threadz00_bglt
		BgL_thz00_39, obj_t BgL_vz00_40)
	{
		{	/* Llib/thread.scm 430 */
			{	/* Llib/thread.scm 430 */
				obj_t BgL_method1217z00_1660;

				{	/* Llib/thread.scm 430 */
					obj_t BgL_res2003z00_2919;

					{	/* Llib/thread.scm 430 */
						long BgL_objzd2classzd2numz00_2890;

						BgL_objzd2classzd2numz00_2890 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_thz00_39));
						{	/* Llib/thread.scm 430 */
							obj_t BgL_arg1920z00_2891;

							BgL_arg1920z00_2891 =
								PROCEDURE_REF
								(BGl_threadzd2cleanupzd2setz12zd2envzc0zz__threadz00,
								(int) (1L));
							{	/* Llib/thread.scm 430 */
								int BgL_offsetz00_2894;

								BgL_offsetz00_2894 = (int) (BgL_objzd2classzd2numz00_2890);
								{	/* Llib/thread.scm 430 */
									long BgL_offsetz00_2895;

									BgL_offsetz00_2895 =
										((long) (BgL_offsetz00_2894) - OBJECT_TYPE);
									{	/* Llib/thread.scm 430 */
										long BgL_modz00_2896;

										BgL_modz00_2896 =
											(BgL_offsetz00_2895 >> (int) ((long) ((int) (4L))));
										{	/* Llib/thread.scm 430 */
											long BgL_restz00_2898;

											BgL_restz00_2898 =
												(BgL_offsetz00_2895 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Llib/thread.scm 430 */

												{	/* Llib/thread.scm 430 */
													obj_t BgL_bucketz00_2900;

													BgL_bucketz00_2900 =
														VECTOR_REF(
														((obj_t) BgL_arg1920z00_2891), BgL_modz00_2896);
													BgL_res2003z00_2919 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2900), BgL_restz00_2898);
					}}}}}}}}
					BgL_method1217z00_1660 = BgL_res2003z00_2919;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1217z00_1660,
					((obj_t) BgL_thz00_39), BgL_vz00_40);
			}
		}

	}



/* &thread-cleanup-set! */
	obj_t BGl_z62threadzd2cleanupzd2setz12z70zz__threadz00(obj_t BgL_envz00_3739,
		obj_t BgL_thz00_3740, obj_t BgL_vz00_3741)
	{
		{	/* Llib/thread.scm 430 */
			{	/* Llib/thread.scm 430 */
				BgL_threadz00_bglt BgL_auxz00_6664;

				{	/* Llib/thread.scm 430 */
					bool_t BgL_test2721z00_6665;

					{	/* Llib/thread.scm 430 */
						obj_t BgL_classz00_4660;

						BgL_classz00_4660 = BGl_threadz00zz__threadz00;
						if (BGL_OBJECTP(BgL_thz00_3740))
							{	/* Llib/thread.scm 430 */
								BgL_objectz00_bglt BgL_arg1918z00_4662;
								long BgL_arg1919z00_4663;

								BgL_arg1918z00_4662 = (BgL_objectz00_bglt) (BgL_thz00_3740);
								BgL_arg1919z00_4663 = BGL_CLASS_DEPTH(BgL_classz00_4660);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Llib/thread.scm 430 */
										long BgL_idxz00_4671;

										BgL_idxz00_4671 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1918z00_4662);
										{	/* Llib/thread.scm 430 */
											obj_t BgL_arg1904z00_4672;

											{	/* Llib/thread.scm 430 */
												long BgL_arg1906z00_4673;

												BgL_arg1906z00_4673 =
													(BgL_idxz00_4671 + BgL_arg1919z00_4663);
												BgL_arg1904z00_4672 =
													VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													BgL_arg1906z00_4673);
											}
											BgL_test2721z00_6665 =
												(BgL_arg1904z00_4672 == BgL_classz00_4660);
									}}
								else
									{	/* Llib/thread.scm 430 */
										bool_t BgL_res2031z00_4678;

										{	/* Llib/thread.scm 430 */
											obj_t BgL_oclassz00_4682;

											{	/* Llib/thread.scm 430 */
												obj_t BgL_arg1925z00_4684;
												long BgL_arg1926z00_4685;

												BgL_arg1925z00_4684 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Llib/thread.scm 430 */
													long BgL_arg1927z00_4686;

													BgL_arg1927z00_4686 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1918z00_4662);
													BgL_arg1926z00_4685 =
														(BgL_arg1927z00_4686 - OBJECT_TYPE);
												}
												BgL_oclassz00_4682 =
													VECTOR_REF(BgL_arg1925z00_4684, BgL_arg1926z00_4685);
											}
											{	/* Llib/thread.scm 430 */
												bool_t BgL__ortest_1147z00_4692;

												BgL__ortest_1147z00_4692 =
													(BgL_classz00_4660 == BgL_oclassz00_4682);
												if (BgL__ortest_1147z00_4692)
													{	/* Llib/thread.scm 430 */
														BgL_res2031z00_4678 = BgL__ortest_1147z00_4692;
													}
												else
													{	/* Llib/thread.scm 430 */
														long BgL_odepthz00_4693;

														{	/* Llib/thread.scm 430 */
															obj_t BgL_arg1912z00_4694;

															BgL_arg1912z00_4694 = (BgL_oclassz00_4682);
															BgL_odepthz00_4693 =
																BGL_CLASS_DEPTH(BgL_arg1912z00_4694);
														}
														if ((BgL_arg1919z00_4663 < BgL_odepthz00_4693))
															{	/* Llib/thread.scm 430 */
																obj_t BgL_arg1910z00_4698;

																{	/* Llib/thread.scm 430 */
																	obj_t BgL_arg1911z00_4699;

																	BgL_arg1911z00_4699 = (BgL_oclassz00_4682);
																	BgL_arg1910z00_4698 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1911z00_4699,
																		BgL_arg1919z00_4663);
																}
																BgL_res2031z00_4678 =
																	(BgL_arg1910z00_4698 == BgL_classz00_4660);
															}
														else
															{	/* Llib/thread.scm 430 */
																BgL_res2031z00_4678 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2721z00_6665 = BgL_res2031z00_4678;
									}
							}
						else
							{	/* Llib/thread.scm 430 */
								BgL_test2721z00_6665 = ((bool_t) 0);
							}
					}
					if (BgL_test2721z00_6665)
						{	/* Llib/thread.scm 430 */
							BgL_auxz00_6664 = ((BgL_threadz00_bglt) BgL_thz00_3740);
						}
					else
						{
							obj_t BgL_auxz00_6690;

							BgL_auxz00_6690 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
								BINT(18332L), BGl_string2323z00zz__threadz00,
								BGl_string2158z00zz__threadz00, BgL_thz00_3740);
							FAILURE(BgL_auxz00_6690, BFALSE, BFALSE);
						}
				}
				return
					BGl_threadzd2cleanupzd2setz12z12zz__threadz00(BgL_auxz00_6664,
					BgL_vz00_3741);
			}
		}

	}



/* thread-name */
	BGL_EXPORTED_DEF obj_t BGl_threadzd2namezd2zz__threadz00(BgL_threadz00_bglt
		BgL_thz00_41)
	{
		{	/* Llib/thread.scm 435 */
			{	/* Llib/thread.scm 435 */
				obj_t BgL_method1219z00_1661;

				{	/* Llib/thread.scm 435 */
					obj_t BgL_res2008z00_2950;

					{	/* Llib/thread.scm 435 */
						long BgL_objzd2classzd2numz00_2921;

						BgL_objzd2classzd2numz00_2921 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_thz00_41));
						{	/* Llib/thread.scm 435 */
							obj_t BgL_arg1920z00_2922;

							BgL_arg1920z00_2922 =
								PROCEDURE_REF(BGl_threadzd2namezd2envz00zz__threadz00,
								(int) (1L));
							{	/* Llib/thread.scm 435 */
								int BgL_offsetz00_2925;

								BgL_offsetz00_2925 = (int) (BgL_objzd2classzd2numz00_2921);
								{	/* Llib/thread.scm 435 */
									long BgL_offsetz00_2926;

									BgL_offsetz00_2926 =
										((long) (BgL_offsetz00_2925) - OBJECT_TYPE);
									{	/* Llib/thread.scm 435 */
										long BgL_modz00_2927;

										BgL_modz00_2927 =
											(BgL_offsetz00_2926 >> (int) ((long) ((int) (4L))));
										{	/* Llib/thread.scm 435 */
											long BgL_restz00_2929;

											BgL_restz00_2929 =
												(BgL_offsetz00_2926 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Llib/thread.scm 435 */

												{	/* Llib/thread.scm 435 */
													obj_t BgL_bucketz00_2931;

													BgL_bucketz00_2931 =
														VECTOR_REF(
														((obj_t) BgL_arg1920z00_2922), BgL_modz00_2927);
													BgL_res2008z00_2950 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2931), BgL_restz00_2929);
					}}}}}}}}
					BgL_method1219z00_1661 = BgL_res2008z00_2950;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1219z00_1661, ((obj_t) BgL_thz00_41));
			}
		}

	}



/* &thread-name */
	obj_t BGl_z62threadzd2namezb0zz__threadz00(obj_t BgL_envz00_3742,
		obj_t BgL_thz00_3743)
	{
		{	/* Llib/thread.scm 435 */
			{	/* Llib/thread.scm 435 */
				BgL_threadz00_bglt BgL_auxz00_6725;

				{	/* Llib/thread.scm 435 */
					bool_t BgL_test2726z00_6726;

					{	/* Llib/thread.scm 435 */
						obj_t BgL_classz00_4700;

						BgL_classz00_4700 = BGl_threadz00zz__threadz00;
						if (BGL_OBJECTP(BgL_thz00_3743))
							{	/* Llib/thread.scm 435 */
								BgL_objectz00_bglt BgL_arg1918z00_4702;
								long BgL_arg1919z00_4703;

								BgL_arg1918z00_4702 = (BgL_objectz00_bglt) (BgL_thz00_3743);
								BgL_arg1919z00_4703 = BGL_CLASS_DEPTH(BgL_classz00_4700);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Llib/thread.scm 435 */
										long BgL_idxz00_4711;

										BgL_idxz00_4711 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1918z00_4702);
										{	/* Llib/thread.scm 435 */
											obj_t BgL_arg1904z00_4712;

											{	/* Llib/thread.scm 435 */
												long BgL_arg1906z00_4713;

												BgL_arg1906z00_4713 =
													(BgL_idxz00_4711 + BgL_arg1919z00_4703);
												BgL_arg1904z00_4712 =
													VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													BgL_arg1906z00_4713);
											}
											BgL_test2726z00_6726 =
												(BgL_arg1904z00_4712 == BgL_classz00_4700);
									}}
								else
									{	/* Llib/thread.scm 435 */
										bool_t BgL_res2031z00_4718;

										{	/* Llib/thread.scm 435 */
											obj_t BgL_oclassz00_4722;

											{	/* Llib/thread.scm 435 */
												obj_t BgL_arg1925z00_4724;
												long BgL_arg1926z00_4725;

												BgL_arg1925z00_4724 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Llib/thread.scm 435 */
													long BgL_arg1927z00_4726;

													BgL_arg1927z00_4726 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1918z00_4702);
													BgL_arg1926z00_4725 =
														(BgL_arg1927z00_4726 - OBJECT_TYPE);
												}
												BgL_oclassz00_4722 =
													VECTOR_REF(BgL_arg1925z00_4724, BgL_arg1926z00_4725);
											}
											{	/* Llib/thread.scm 435 */
												bool_t BgL__ortest_1147z00_4732;

												BgL__ortest_1147z00_4732 =
													(BgL_classz00_4700 == BgL_oclassz00_4722);
												if (BgL__ortest_1147z00_4732)
													{	/* Llib/thread.scm 435 */
														BgL_res2031z00_4718 = BgL__ortest_1147z00_4732;
													}
												else
													{	/* Llib/thread.scm 435 */
														long BgL_odepthz00_4733;

														{	/* Llib/thread.scm 435 */
															obj_t BgL_arg1912z00_4734;

															BgL_arg1912z00_4734 = (BgL_oclassz00_4722);
															BgL_odepthz00_4733 =
																BGL_CLASS_DEPTH(BgL_arg1912z00_4734);
														}
														if ((BgL_arg1919z00_4703 < BgL_odepthz00_4733))
															{	/* Llib/thread.scm 435 */
																obj_t BgL_arg1910z00_4738;

																{	/* Llib/thread.scm 435 */
																	obj_t BgL_arg1911z00_4739;

																	BgL_arg1911z00_4739 = (BgL_oclassz00_4722);
																	BgL_arg1910z00_4738 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1911z00_4739,
																		BgL_arg1919z00_4703);
																}
																BgL_res2031z00_4718 =
																	(BgL_arg1910z00_4738 == BgL_classz00_4700);
															}
														else
															{	/* Llib/thread.scm 435 */
																BgL_res2031z00_4718 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2726z00_6726 = BgL_res2031z00_4718;
									}
							}
						else
							{	/* Llib/thread.scm 435 */
								BgL_test2726z00_6726 = ((bool_t) 0);
							}
					}
					if (BgL_test2726z00_6726)
						{	/* Llib/thread.scm 435 */
							BgL_auxz00_6725 = ((BgL_threadz00_bglt) BgL_thz00_3743);
						}
					else
						{
							obj_t BgL_auxz00_6751;

							BgL_auxz00_6751 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
								BINT(18612L), BGl_string2324z00zz__threadz00,
								BGl_string2158z00zz__threadz00, BgL_thz00_3743);
							FAILURE(BgL_auxz00_6751, BFALSE, BFALSE);
						}
				}
				return BGl_threadzd2namezd2zz__threadz00(BgL_auxz00_6725);
			}
		}

	}



/* thread-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_threadzd2namezd2setz12z12zz__threadz00(BgL_threadz00_bglt BgL_thz00_42,
		obj_t BgL_vz00_43)
	{
		{	/* Llib/thread.scm 440 */
			{	/* Llib/thread.scm 440 */
				obj_t BgL_method1221z00_1662;

				{	/* Llib/thread.scm 440 */
					obj_t BgL_res2013z00_2981;

					{	/* Llib/thread.scm 440 */
						long BgL_objzd2classzd2numz00_2952;

						BgL_objzd2classzd2numz00_2952 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_thz00_42));
						{	/* Llib/thread.scm 440 */
							obj_t BgL_arg1920z00_2953;

							BgL_arg1920z00_2953 =
								PROCEDURE_REF(BGl_threadzd2namezd2setz12zd2envzc0zz__threadz00,
								(int) (1L));
							{	/* Llib/thread.scm 440 */
								int BgL_offsetz00_2956;

								BgL_offsetz00_2956 = (int) (BgL_objzd2classzd2numz00_2952);
								{	/* Llib/thread.scm 440 */
									long BgL_offsetz00_2957;

									BgL_offsetz00_2957 =
										((long) (BgL_offsetz00_2956) - OBJECT_TYPE);
									{	/* Llib/thread.scm 440 */
										long BgL_modz00_2958;

										BgL_modz00_2958 =
											(BgL_offsetz00_2957 >> (int) ((long) ((int) (4L))));
										{	/* Llib/thread.scm 440 */
											long BgL_restz00_2960;

											BgL_restz00_2960 =
												(BgL_offsetz00_2957 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Llib/thread.scm 440 */

												{	/* Llib/thread.scm 440 */
													obj_t BgL_bucketz00_2962;

													BgL_bucketz00_2962 =
														VECTOR_REF(
														((obj_t) BgL_arg1920z00_2953), BgL_modz00_2958);
													BgL_res2013z00_2981 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2962), BgL_restz00_2960);
					}}}}}}}}
					BgL_method1221z00_1662 = BgL_res2013z00_2981;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1221z00_1662,
					((obj_t) BgL_thz00_42), BgL_vz00_43);
			}
		}

	}



/* &thread-name-set! */
	obj_t BGl_z62threadzd2namezd2setz12z70zz__threadz00(obj_t BgL_envz00_3744,
		obj_t BgL_thz00_3745, obj_t BgL_vz00_3746)
	{
		{	/* Llib/thread.scm 440 */
			{	/* Llib/thread.scm 440 */
				obj_t BgL_auxz00_6817;
				BgL_threadz00_bglt BgL_auxz00_6787;

				if (STRINGP(BgL_vz00_3746))
					{	/* Llib/thread.scm 440 */
						BgL_auxz00_6817 = BgL_vz00_3746;
					}
				else
					{
						obj_t BgL_auxz00_6820;

						BgL_auxz00_6820 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
							BINT(18877L), BGl_string2325z00zz__threadz00,
							BGl_string2156z00zz__threadz00, BgL_vz00_3746);
						FAILURE(BgL_auxz00_6820, BFALSE, BFALSE);
					}
				{	/* Llib/thread.scm 440 */
					bool_t BgL_test2731z00_6788;

					{	/* Llib/thread.scm 440 */
						obj_t BgL_classz00_4740;

						BgL_classz00_4740 = BGl_threadz00zz__threadz00;
						if (BGL_OBJECTP(BgL_thz00_3745))
							{	/* Llib/thread.scm 440 */
								BgL_objectz00_bglt BgL_arg1918z00_4742;
								long BgL_arg1919z00_4743;

								BgL_arg1918z00_4742 = (BgL_objectz00_bglt) (BgL_thz00_3745);
								BgL_arg1919z00_4743 = BGL_CLASS_DEPTH(BgL_classz00_4740);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Llib/thread.scm 440 */
										long BgL_idxz00_4751;

										BgL_idxz00_4751 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1918z00_4742);
										{	/* Llib/thread.scm 440 */
											obj_t BgL_arg1904z00_4752;

											{	/* Llib/thread.scm 440 */
												long BgL_arg1906z00_4753;

												BgL_arg1906z00_4753 =
													(BgL_idxz00_4751 + BgL_arg1919z00_4743);
												BgL_arg1904z00_4752 =
													VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													BgL_arg1906z00_4753);
											}
											BgL_test2731z00_6788 =
												(BgL_arg1904z00_4752 == BgL_classz00_4740);
									}}
								else
									{	/* Llib/thread.scm 440 */
										bool_t BgL_res2031z00_4758;

										{	/* Llib/thread.scm 440 */
											obj_t BgL_oclassz00_4762;

											{	/* Llib/thread.scm 440 */
												obj_t BgL_arg1925z00_4764;
												long BgL_arg1926z00_4765;

												BgL_arg1925z00_4764 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Llib/thread.scm 440 */
													long BgL_arg1927z00_4766;

													BgL_arg1927z00_4766 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1918z00_4742);
													BgL_arg1926z00_4765 =
														(BgL_arg1927z00_4766 - OBJECT_TYPE);
												}
												BgL_oclassz00_4762 =
													VECTOR_REF(BgL_arg1925z00_4764, BgL_arg1926z00_4765);
											}
											{	/* Llib/thread.scm 440 */
												bool_t BgL__ortest_1147z00_4772;

												BgL__ortest_1147z00_4772 =
													(BgL_classz00_4740 == BgL_oclassz00_4762);
												if (BgL__ortest_1147z00_4772)
													{	/* Llib/thread.scm 440 */
														BgL_res2031z00_4758 = BgL__ortest_1147z00_4772;
													}
												else
													{	/* Llib/thread.scm 440 */
														long BgL_odepthz00_4773;

														{	/* Llib/thread.scm 440 */
															obj_t BgL_arg1912z00_4774;

															BgL_arg1912z00_4774 = (BgL_oclassz00_4762);
															BgL_odepthz00_4773 =
																BGL_CLASS_DEPTH(BgL_arg1912z00_4774);
														}
														if ((BgL_arg1919z00_4743 < BgL_odepthz00_4773))
															{	/* Llib/thread.scm 440 */
																obj_t BgL_arg1910z00_4778;

																{	/* Llib/thread.scm 440 */
																	obj_t BgL_arg1911z00_4779;

																	BgL_arg1911z00_4779 = (BgL_oclassz00_4762);
																	BgL_arg1910z00_4778 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1911z00_4779,
																		BgL_arg1919z00_4743);
																}
																BgL_res2031z00_4758 =
																	(BgL_arg1910z00_4778 == BgL_classz00_4740);
															}
														else
															{	/* Llib/thread.scm 440 */
																BgL_res2031z00_4758 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2731z00_6788 = BgL_res2031z00_4758;
									}
							}
						else
							{	/* Llib/thread.scm 440 */
								BgL_test2731z00_6788 = ((bool_t) 0);
							}
					}
					if (BgL_test2731z00_6788)
						{	/* Llib/thread.scm 440 */
							BgL_auxz00_6787 = ((BgL_threadz00_bglt) BgL_thz00_3745);
						}
					else
						{
							obj_t BgL_auxz00_6813;

							BgL_auxz00_6813 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
								BINT(18877L), BGl_string2325z00zz__threadz00,
								BGl_string2158z00zz__threadz00, BgL_thz00_3745);
							FAILURE(BgL_auxz00_6813, BFALSE, BFALSE);
						}
				}
				return
					BGl_threadzd2namezd2setz12z12zz__threadz00(BgL_auxz00_6787,
					BgL_auxz00_6817);
			}
		}

	}



/* %user-current-thread */
	BGL_EXPORTED_DEF obj_t
		BGl_z52userzd2currentzd2threadz52zz__threadz00(BgL_threadz00_bglt
		BgL_oz00_48)
	{
		{	/* Llib/thread.scm 451 */
			{	/* Llib/thread.scm 451 */
				obj_t BgL_method1228z00_1663;

				{	/* Llib/thread.scm 451 */
					obj_t BgL_res2018z00_3012;

					{	/* Llib/thread.scm 451 */
						long BgL_objzd2classzd2numz00_2983;

						BgL_objzd2classzd2numz00_2983 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_oz00_48));
						{	/* Llib/thread.scm 451 */
							obj_t BgL_arg1920z00_2984;

							BgL_arg1920z00_2984 =
								PROCEDURE_REF
								(BGl_z52userzd2currentzd2threadzd2envz80zz__threadz00,
								(int) (1L));
							{	/* Llib/thread.scm 451 */
								int BgL_offsetz00_2987;

								BgL_offsetz00_2987 = (int) (BgL_objzd2classzd2numz00_2983);
								{	/* Llib/thread.scm 451 */
									long BgL_offsetz00_2988;

									BgL_offsetz00_2988 =
										((long) (BgL_offsetz00_2987) - OBJECT_TYPE);
									{	/* Llib/thread.scm 451 */
										long BgL_modz00_2989;

										BgL_modz00_2989 =
											(BgL_offsetz00_2988 >> (int) ((long) ((int) (4L))));
										{	/* Llib/thread.scm 451 */
											long BgL_restz00_2991;

											BgL_restz00_2991 =
												(BgL_offsetz00_2988 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Llib/thread.scm 451 */

												{	/* Llib/thread.scm 451 */
													obj_t BgL_bucketz00_2993;

													BgL_bucketz00_2993 =
														VECTOR_REF(
														((obj_t) BgL_arg1920z00_2984), BgL_modz00_2989);
													BgL_res2018z00_3012 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2993), BgL_restz00_2991);
					}}}}}}}}
					BgL_method1228z00_1663 = BgL_res2018z00_3012;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1228z00_1663, ((obj_t) BgL_oz00_48));
			}
		}

	}



/* &%user-current-thread */
	obj_t BGl_z62z52userzd2currentzd2threadz30zz__threadz00(obj_t BgL_envz00_3747,
		obj_t BgL_oz00_3748)
	{
		{	/* Llib/thread.scm 451 */
			{	/* Llib/thread.scm 451 */
				BgL_threadz00_bglt BgL_auxz00_6855;

				{	/* Llib/thread.scm 451 */
					bool_t BgL_test2737z00_6856;

					{	/* Llib/thread.scm 451 */
						obj_t BgL_classz00_4780;

						BgL_classz00_4780 = BGl_threadz00zz__threadz00;
						if (BGL_OBJECTP(BgL_oz00_3748))
							{	/* Llib/thread.scm 451 */
								BgL_objectz00_bglt BgL_arg1918z00_4782;
								long BgL_arg1919z00_4783;

								BgL_arg1918z00_4782 = (BgL_objectz00_bglt) (BgL_oz00_3748);
								BgL_arg1919z00_4783 = BGL_CLASS_DEPTH(BgL_classz00_4780);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Llib/thread.scm 451 */
										long BgL_idxz00_4791;

										BgL_idxz00_4791 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1918z00_4782);
										{	/* Llib/thread.scm 451 */
											obj_t BgL_arg1904z00_4792;

											{	/* Llib/thread.scm 451 */
												long BgL_arg1906z00_4793;

												BgL_arg1906z00_4793 =
													(BgL_idxz00_4791 + BgL_arg1919z00_4783);
												BgL_arg1904z00_4792 =
													VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													BgL_arg1906z00_4793);
											}
											BgL_test2737z00_6856 =
												(BgL_arg1904z00_4792 == BgL_classz00_4780);
									}}
								else
									{	/* Llib/thread.scm 451 */
										bool_t BgL_res2031z00_4798;

										{	/* Llib/thread.scm 451 */
											obj_t BgL_oclassz00_4802;

											{	/* Llib/thread.scm 451 */
												obj_t BgL_arg1925z00_4804;
												long BgL_arg1926z00_4805;

												BgL_arg1925z00_4804 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Llib/thread.scm 451 */
													long BgL_arg1927z00_4806;

													BgL_arg1927z00_4806 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1918z00_4782);
													BgL_arg1926z00_4805 =
														(BgL_arg1927z00_4806 - OBJECT_TYPE);
												}
												BgL_oclassz00_4802 =
													VECTOR_REF(BgL_arg1925z00_4804, BgL_arg1926z00_4805);
											}
											{	/* Llib/thread.scm 451 */
												bool_t BgL__ortest_1147z00_4812;

												BgL__ortest_1147z00_4812 =
													(BgL_classz00_4780 == BgL_oclassz00_4802);
												if (BgL__ortest_1147z00_4812)
													{	/* Llib/thread.scm 451 */
														BgL_res2031z00_4798 = BgL__ortest_1147z00_4812;
													}
												else
													{	/* Llib/thread.scm 451 */
														long BgL_odepthz00_4813;

														{	/* Llib/thread.scm 451 */
															obj_t BgL_arg1912z00_4814;

															BgL_arg1912z00_4814 = (BgL_oclassz00_4802);
															BgL_odepthz00_4813 =
																BGL_CLASS_DEPTH(BgL_arg1912z00_4814);
														}
														if ((BgL_arg1919z00_4783 < BgL_odepthz00_4813))
															{	/* Llib/thread.scm 451 */
																obj_t BgL_arg1910z00_4818;

																{	/* Llib/thread.scm 451 */
																	obj_t BgL_arg1911z00_4819;

																	BgL_arg1911z00_4819 = (BgL_oclassz00_4802);
																	BgL_arg1910z00_4818 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1911z00_4819,
																		BgL_arg1919z00_4783);
																}
																BgL_res2031z00_4798 =
																	(BgL_arg1910z00_4818 == BgL_classz00_4780);
															}
														else
															{	/* Llib/thread.scm 451 */
																BgL_res2031z00_4798 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2737z00_6856 = BgL_res2031z00_4798;
									}
							}
						else
							{	/* Llib/thread.scm 451 */
								BgL_test2737z00_6856 = ((bool_t) 0);
							}
					}
					if (BgL_test2737z00_6856)
						{	/* Llib/thread.scm 451 */
							BgL_auxz00_6855 = ((BgL_threadz00_bglt) BgL_oz00_3748);
						}
					else
						{
							obj_t BgL_auxz00_6881;

							BgL_auxz00_6881 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
								BINT(19506L), BGl_string2326z00zz__threadz00,
								BGl_string2158z00zz__threadz00, BgL_oz00_3748);
							FAILURE(BgL_auxz00_6881, BFALSE, BFALSE);
						}
				}
				return BGl_z52userzd2currentzd2threadz52zz__threadz00(BgL_auxz00_6855);
			}
		}

	}



/* %user-thread-sleep! */
	BGL_EXPORTED_DEF obj_t
		BGl_z52userzd2threadzd2sleepz12z40zz__threadz00(BgL_threadz00_bglt
		BgL_oz00_49, obj_t BgL_dz00_50)
	{
		{	/* Llib/thread.scm 473 */
			{	/* Llib/thread.scm 473 */
				obj_t BgL_method1230z00_1664;

				{	/* Llib/thread.scm 473 */
					obj_t BgL_res2023z00_3043;

					{	/* Llib/thread.scm 473 */
						long BgL_objzd2classzd2numz00_3014;

						BgL_objzd2classzd2numz00_3014 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_oz00_49));
						{	/* Llib/thread.scm 473 */
							obj_t BgL_arg1920z00_3015;

							BgL_arg1920z00_3015 =
								PROCEDURE_REF
								(BGl_z52userzd2threadzd2sleepz12zd2envz92zz__threadz00,
								(int) (1L));
							{	/* Llib/thread.scm 473 */
								int BgL_offsetz00_3018;

								BgL_offsetz00_3018 = (int) (BgL_objzd2classzd2numz00_3014);
								{	/* Llib/thread.scm 473 */
									long BgL_offsetz00_3019;

									BgL_offsetz00_3019 =
										((long) (BgL_offsetz00_3018) - OBJECT_TYPE);
									{	/* Llib/thread.scm 473 */
										long BgL_modz00_3020;

										BgL_modz00_3020 =
											(BgL_offsetz00_3019 >> (int) ((long) ((int) (4L))));
										{	/* Llib/thread.scm 473 */
											long BgL_restz00_3022;

											BgL_restz00_3022 =
												(BgL_offsetz00_3019 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Llib/thread.scm 473 */

												{	/* Llib/thread.scm 473 */
													obj_t BgL_bucketz00_3024;

													BgL_bucketz00_3024 =
														VECTOR_REF(
														((obj_t) BgL_arg1920z00_3015), BgL_modz00_3020);
													BgL_res2023z00_3043 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_3024), BgL_restz00_3022);
					}}}}}}}}
					BgL_method1230z00_1664 = BgL_res2023z00_3043;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1230z00_1664,
					((obj_t) BgL_oz00_49), BgL_dz00_50);
			}
		}

	}



/* &%user-thread-sleep! */
	obj_t BGl_z62z52userzd2threadzd2sleepz12z22zz__threadz00(obj_t
		BgL_envz00_3749, obj_t BgL_oz00_3750, obj_t BgL_dz00_3751)
	{
		{	/* Llib/thread.scm 473 */
			{	/* Llib/thread.scm 473 */
				BgL_threadz00_bglt BgL_auxz00_6917;

				{	/* Llib/thread.scm 473 */
					bool_t BgL_test2742z00_6918;

					{	/* Llib/thread.scm 473 */
						obj_t BgL_classz00_4820;

						BgL_classz00_4820 = BGl_threadz00zz__threadz00;
						if (BGL_OBJECTP(BgL_oz00_3750))
							{	/* Llib/thread.scm 473 */
								BgL_objectz00_bglt BgL_arg1918z00_4822;
								long BgL_arg1919z00_4823;

								BgL_arg1918z00_4822 = (BgL_objectz00_bglt) (BgL_oz00_3750);
								BgL_arg1919z00_4823 = BGL_CLASS_DEPTH(BgL_classz00_4820);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Llib/thread.scm 473 */
										long BgL_idxz00_4831;

										BgL_idxz00_4831 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1918z00_4822);
										{	/* Llib/thread.scm 473 */
											obj_t BgL_arg1904z00_4832;

											{	/* Llib/thread.scm 473 */
												long BgL_arg1906z00_4833;

												BgL_arg1906z00_4833 =
													(BgL_idxz00_4831 + BgL_arg1919z00_4823);
												BgL_arg1904z00_4832 =
													VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													BgL_arg1906z00_4833);
											}
											BgL_test2742z00_6918 =
												(BgL_arg1904z00_4832 == BgL_classz00_4820);
									}}
								else
									{	/* Llib/thread.scm 473 */
										bool_t BgL_res2031z00_4838;

										{	/* Llib/thread.scm 473 */
											obj_t BgL_oclassz00_4842;

											{	/* Llib/thread.scm 473 */
												obj_t BgL_arg1925z00_4844;
												long BgL_arg1926z00_4845;

												BgL_arg1925z00_4844 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Llib/thread.scm 473 */
													long BgL_arg1927z00_4846;

													BgL_arg1927z00_4846 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1918z00_4822);
													BgL_arg1926z00_4845 =
														(BgL_arg1927z00_4846 - OBJECT_TYPE);
												}
												BgL_oclassz00_4842 =
													VECTOR_REF(BgL_arg1925z00_4844, BgL_arg1926z00_4845);
											}
											{	/* Llib/thread.scm 473 */
												bool_t BgL__ortest_1147z00_4852;

												BgL__ortest_1147z00_4852 =
													(BgL_classz00_4820 == BgL_oclassz00_4842);
												if (BgL__ortest_1147z00_4852)
													{	/* Llib/thread.scm 473 */
														BgL_res2031z00_4838 = BgL__ortest_1147z00_4852;
													}
												else
													{	/* Llib/thread.scm 473 */
														long BgL_odepthz00_4853;

														{	/* Llib/thread.scm 473 */
															obj_t BgL_arg1912z00_4854;

															BgL_arg1912z00_4854 = (BgL_oclassz00_4842);
															BgL_odepthz00_4853 =
																BGL_CLASS_DEPTH(BgL_arg1912z00_4854);
														}
														if ((BgL_arg1919z00_4823 < BgL_odepthz00_4853))
															{	/* Llib/thread.scm 473 */
																obj_t BgL_arg1910z00_4858;

																{	/* Llib/thread.scm 473 */
																	obj_t BgL_arg1911z00_4859;

																	BgL_arg1911z00_4859 = (BgL_oclassz00_4842);
																	BgL_arg1910z00_4858 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1911z00_4859,
																		BgL_arg1919z00_4823);
																}
																BgL_res2031z00_4838 =
																	(BgL_arg1910z00_4858 == BgL_classz00_4820);
															}
														else
															{	/* Llib/thread.scm 473 */
																BgL_res2031z00_4838 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2742z00_6918 = BgL_res2031z00_4838;
									}
							}
						else
							{	/* Llib/thread.scm 473 */
								BgL_test2742z00_6918 = ((bool_t) 0);
							}
					}
					if (BgL_test2742z00_6918)
						{	/* Llib/thread.scm 473 */
							BgL_auxz00_6917 = ((BgL_threadz00_bglt) BgL_oz00_3750);
						}
					else
						{
							obj_t BgL_auxz00_6943;

							BgL_auxz00_6943 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
								BINT(20480L), BGl_string2327z00zz__threadz00,
								BGl_string2158z00zz__threadz00, BgL_oz00_3750);
							FAILURE(BgL_auxz00_6943, BFALSE, BFALSE);
						}
				}
				return
					BGl_z52userzd2threadzd2sleepz12z40zz__threadz00(BgL_auxz00_6917,
					BgL_dz00_3751);
			}
		}

	}



/* %user-thread-yield! */
	BGL_EXPORTED_DEF obj_t
		BGl_z52userzd2threadzd2yieldz12z40zz__threadz00(BgL_threadz00_bglt
		BgL_oz00_52)
	{
		{	/* Llib/thread.scm 500 */
			{	/* Llib/thread.scm 500 */
				obj_t BgL_method1232z00_1665;

				{	/* Llib/thread.scm 500 */
					obj_t BgL_res2028z00_3074;

					{	/* Llib/thread.scm 500 */
						long BgL_objzd2classzd2numz00_3045;

						BgL_objzd2classzd2numz00_3045 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_oz00_52));
						{	/* Llib/thread.scm 500 */
							obj_t BgL_arg1920z00_3046;

							BgL_arg1920z00_3046 =
								PROCEDURE_REF
								(BGl_z52userzd2threadzd2yieldz12zd2envz92zz__threadz00,
								(int) (1L));
							{	/* Llib/thread.scm 500 */
								int BgL_offsetz00_3049;

								BgL_offsetz00_3049 = (int) (BgL_objzd2classzd2numz00_3045);
								{	/* Llib/thread.scm 500 */
									long BgL_offsetz00_3050;

									BgL_offsetz00_3050 =
										((long) (BgL_offsetz00_3049) - OBJECT_TYPE);
									{	/* Llib/thread.scm 500 */
										long BgL_modz00_3051;

										BgL_modz00_3051 =
											(BgL_offsetz00_3050 >> (int) ((long) ((int) (4L))));
										{	/* Llib/thread.scm 500 */
											long BgL_restz00_3053;

											BgL_restz00_3053 =
												(BgL_offsetz00_3050 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Llib/thread.scm 500 */

												{	/* Llib/thread.scm 500 */
													obj_t BgL_bucketz00_3055;

													BgL_bucketz00_3055 =
														VECTOR_REF(
														((obj_t) BgL_arg1920z00_3046), BgL_modz00_3051);
													BgL_res2028z00_3074 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_3055), BgL_restz00_3053);
					}}}}}}}}
					BgL_method1232z00_1665 = BgL_res2028z00_3074;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1232z00_1665, ((obj_t) BgL_oz00_52));
			}
		}

	}



/* &%user-thread-yield! */
	obj_t BGl_z62z52userzd2threadzd2yieldz12z22zz__threadz00(obj_t
		BgL_envz00_3752, obj_t BgL_oz00_3753)
	{
		{	/* Llib/thread.scm 500 */
			{	/* Llib/thread.scm 500 */
				BgL_threadz00_bglt BgL_auxz00_6978;

				{	/* Llib/thread.scm 500 */
					bool_t BgL_test2747z00_6979;

					{	/* Llib/thread.scm 500 */
						obj_t BgL_classz00_4860;

						BgL_classz00_4860 = BGl_threadz00zz__threadz00;
						if (BGL_OBJECTP(BgL_oz00_3753))
							{	/* Llib/thread.scm 500 */
								BgL_objectz00_bglt BgL_arg1918z00_4862;
								long BgL_arg1919z00_4863;

								BgL_arg1918z00_4862 = (BgL_objectz00_bglt) (BgL_oz00_3753);
								BgL_arg1919z00_4863 = BGL_CLASS_DEPTH(BgL_classz00_4860);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Llib/thread.scm 500 */
										long BgL_idxz00_4871;

										BgL_idxz00_4871 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1918z00_4862);
										{	/* Llib/thread.scm 500 */
											obj_t BgL_arg1904z00_4872;

											{	/* Llib/thread.scm 500 */
												long BgL_arg1906z00_4873;

												BgL_arg1906z00_4873 =
													(BgL_idxz00_4871 + BgL_arg1919z00_4863);
												BgL_arg1904z00_4872 =
													VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													BgL_arg1906z00_4873);
											}
											BgL_test2747z00_6979 =
												(BgL_arg1904z00_4872 == BgL_classz00_4860);
									}}
								else
									{	/* Llib/thread.scm 500 */
										bool_t BgL_res2031z00_4878;

										{	/* Llib/thread.scm 500 */
											obj_t BgL_oclassz00_4882;

											{	/* Llib/thread.scm 500 */
												obj_t BgL_arg1925z00_4884;
												long BgL_arg1926z00_4885;

												BgL_arg1925z00_4884 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Llib/thread.scm 500 */
													long BgL_arg1927z00_4886;

													BgL_arg1927z00_4886 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1918z00_4862);
													BgL_arg1926z00_4885 =
														(BgL_arg1927z00_4886 - OBJECT_TYPE);
												}
												BgL_oclassz00_4882 =
													VECTOR_REF(BgL_arg1925z00_4884, BgL_arg1926z00_4885);
											}
											{	/* Llib/thread.scm 500 */
												bool_t BgL__ortest_1147z00_4892;

												BgL__ortest_1147z00_4892 =
													(BgL_classz00_4860 == BgL_oclassz00_4882);
												if (BgL__ortest_1147z00_4892)
													{	/* Llib/thread.scm 500 */
														BgL_res2031z00_4878 = BgL__ortest_1147z00_4892;
													}
												else
													{	/* Llib/thread.scm 500 */
														long BgL_odepthz00_4893;

														{	/* Llib/thread.scm 500 */
															obj_t BgL_arg1912z00_4894;

															BgL_arg1912z00_4894 = (BgL_oclassz00_4882);
															BgL_odepthz00_4893 =
																BGL_CLASS_DEPTH(BgL_arg1912z00_4894);
														}
														if ((BgL_arg1919z00_4863 < BgL_odepthz00_4893))
															{	/* Llib/thread.scm 500 */
																obj_t BgL_arg1910z00_4898;

																{	/* Llib/thread.scm 500 */
																	obj_t BgL_arg1911z00_4899;

																	BgL_arg1911z00_4899 = (BgL_oclassz00_4882);
																	BgL_arg1910z00_4898 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1911z00_4899,
																		BgL_arg1919z00_4863);
																}
																BgL_res2031z00_4878 =
																	(BgL_arg1910z00_4898 == BgL_classz00_4860);
															}
														else
															{	/* Llib/thread.scm 500 */
																BgL_res2031z00_4878 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2747z00_6979 = BgL_res2031z00_4878;
									}
							}
						else
							{	/* Llib/thread.scm 500 */
								BgL_test2747z00_6979 = ((bool_t) 0);
							}
					}
					if (BgL_test2747z00_6979)
						{	/* Llib/thread.scm 500 */
							BgL_auxz00_6978 = ((BgL_threadz00_bglt) BgL_oz00_3753);
						}
					else
						{
							obj_t BgL_auxz00_7004;

							BgL_auxz00_7004 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2151z00zz__threadz00,
								BINT(21576L), BGl_string2328z00zz__threadz00,
								BGl_string2158z00zz__threadz00, BgL_oz00_3753);
							FAILURE(BgL_auxz00_7004, BFALSE, BFALSE);
						}
				}
				return BGl_z52userzd2threadzd2yieldz12z40zz__threadz00(BgL_auxz00_6978);
			}
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__threadz00(void)
	{
		{	/* Llib/thread.scm 17 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tbzd2makezd2threadzd2envzd2zz__threadz00,
				BGl_nothreadzd2backendzd2zz__threadz00, BGl_proc2329z00zz__threadz00,
				BGl_string2330z00zz__threadz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tbzd2currentzd2threadzd2envzd2zz__threadz00,
				BGl_nothreadzd2backendzd2zz__threadz00, BGl_proc2331z00zz__threadz00,
				BGl_string2332z00zz__threadz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_objectzd2displayzd2envz00zz__objectz00, BGl_threadz00zz__threadz00,
				BGl_proc2333z00zz__threadz00, BGl_string2334z00zz__threadz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_objectzd2writezd2envz00zz__objectz00, BGl_threadz00zz__threadz00,
				BGl_proc2335z00zz__threadz00, BGl_string2336z00zz__threadz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_objectzd2printzd2envz00zz__objectz00, BGl_threadz00zz__threadz00,
				BGl_proc2337z00zz__threadz00, BGl_string2338z00zz__threadz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_threadzd2startz12zd2envz12zz__threadz00,
				BGl_nothreadz00zz__threadz00, BGl_proc2339z00zz__threadz00,
				BGl_string2340z00zz__threadz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_threadzd2startzd2joinablez12zd2envzc0zz__threadz00,
				BGl_nothreadz00zz__threadz00, BGl_proc2341z00zz__threadz00,
				BGl_string2342z00zz__threadz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_threadzd2joinz12zd2envz12zz__threadz00,
				BGl_nothreadz00zz__threadz00, BGl_proc2343z00zz__threadz00,
				BGl_string2344z00zz__threadz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_threadzd2terminatez12zd2envz12zz__threadz00,
				BGl_nothreadz00zz__threadz00, BGl_proc2345z00zz__threadz00,
				BGl_string2346z00zz__threadz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_threadzd2killz12zd2envz12zz__threadz00,
				BGl_nothreadz00zz__threadz00, BGl_proc2347z00zz__threadz00,
				BGl_string2348z00zz__threadz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_threadzd2specificzd2envz00zz__threadz00,
				BGl_nothreadz00zz__threadz00, BGl_proc2349z00zz__threadz00,
				BGl_string2350z00zz__threadz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_threadzd2specificzd2setz12zd2envzc0zz__threadz00,
				BGl_nothreadz00zz__threadz00, BGl_proc2351z00zz__threadz00,
				BGl_string2352z00zz__threadz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_threadzd2cleanupzd2envz00zz__threadz00,
				BGl_nothreadz00zz__threadz00, BGl_proc2353z00zz__threadz00,
				BGl_string2354z00zz__threadz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_threadzd2cleanupzd2setz12zd2envzc0zz__threadz00,
				BGl_nothreadz00zz__threadz00, BGl_proc2355z00zz__threadz00,
				BGl_string2356z00zz__threadz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_threadzd2namezd2envz00zz__threadz00, BGl_nothreadz00zz__threadz00,
				BGl_proc2357z00zz__threadz00, BGl_string2358z00zz__threadz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_threadzd2namezd2setz12zd2envzc0zz__threadz00,
				BGl_nothreadz00zz__threadz00, BGl_proc2359z00zz__threadz00,
				BGl_string2360z00zz__threadz00);
		}

	}



/* &thread-name-set!-not1254 */
	obj_t BGl_z62threadzd2namezd2setz12zd2not1254za2zz__threadz00(obj_t
		BgL_envz00_3773, obj_t BgL_thz00_3774, obj_t BgL_vz00_3775)
	{
		{	/* Llib/thread.scm 608 */
			return
				((((BgL_nothreadz00_bglt) COBJECT(
							((BgL_nothreadz00_bglt) BgL_thz00_3774)))->BgL_z52namez52) =
				((obj_t) BgL_vz00_3775), BUNSPEC);
		}

	}



/* &thread-name-nothread1252 */
	obj_t BGl_z62threadzd2namezd2nothread1252z62zz__threadz00(obj_t
		BgL_envz00_3776, obj_t BgL_thz00_3777)
	{
		{	/* Llib/thread.scm 602 */
			return
				(((BgL_nothreadz00_bglt) COBJECT(
						((BgL_nothreadz00_bglt) BgL_thz00_3777)))->BgL_z52namez52);
		}

	}



/* &thread-cleanup-set!-1250 */
	obj_t BGl_z62threadzd2cleanupzd2setz12zd21250za2zz__threadz00(obj_t
		BgL_envz00_3778, obj_t BgL_thz00_3779, obj_t BgL_vz00_3780)
	{
		{	/* Llib/thread.scm 596 */
			return
				((((BgL_nothreadz00_bglt) COBJECT(
							((BgL_nothreadz00_bglt) BgL_thz00_3779)))->BgL_z52cleanupz52) =
				((obj_t) BgL_vz00_3780), BUNSPEC);
		}

	}



/* &thread-cleanup-nothr1248 */
	obj_t BGl_z62threadzd2cleanupzd2nothr1248z62zz__threadz00(obj_t
		BgL_envz00_3781, obj_t BgL_thz00_3782)
	{
		{	/* Llib/thread.scm 590 */
			return
				(((BgL_nothreadz00_bglt) COBJECT(
						((BgL_nothreadz00_bglt) BgL_thz00_3782)))->BgL_z52cleanupz52);
		}

	}



/* &thread-specific-set!1246 */
	obj_t BGl_z62threadzd2specificzd2setz121246z70zz__threadz00(obj_t
		BgL_envz00_3783, obj_t BgL_thz00_3784, obj_t BgL_vz00_3785)
	{
		{	/* Llib/thread.scm 584 */
			return
				((((BgL_nothreadz00_bglt) COBJECT(
							((BgL_nothreadz00_bglt) BgL_thz00_3784)))->BgL_z52specificz52) =
				((obj_t) BgL_vz00_3785), BUNSPEC);
		}

	}



/* &thread-specific-noth1244 */
	obj_t BGl_z62threadzd2specificzd2noth1244z62zz__threadz00(obj_t
		BgL_envz00_3786, obj_t BgL_thz00_3787)
	{
		{	/* Llib/thread.scm 578 */
			return
				(((BgL_nothreadz00_bglt) COBJECT(
						((BgL_nothreadz00_bglt) BgL_thz00_3787)))->BgL_z52specificz52);
		}

	}



/* &thread-kill!-nothrea1242 */
	obj_t BGl_z62threadzd2killz12zd2nothrea1242z70zz__threadz00(obj_t
		BgL_envz00_3788, obj_t BgL_thz00_3789, obj_t BgL_nz00_3790)
	{
		{	/* Llib/thread.scm 572 */
			return BUNSPEC;
		}

	}



/* &thread-terminate!-no1240 */
	obj_t BGl_z62threadzd2terminatez12zd2no1240z70zz__threadz00(obj_t
		BgL_envz00_3791, obj_t BgL_thz00_3792)
	{
		{	/* Llib/thread.scm 564 */
			{	/* Llib/thread.scm 566 */
				bool_t BgL_test2752z00_7037;

				{	/* Llib/thread.scm 566 */
					obj_t BgL_tmpz00_7038;

					BgL_tmpz00_7038 =
						(((BgL_nothreadz00_bglt) COBJECT(
								((BgL_nothreadz00_bglt) BgL_thz00_3792)))->BgL_z52cleanupz52);
					BgL_test2752z00_7037 = PROCEDUREP(BgL_tmpz00_7038);
				}
				if (BgL_test2752z00_7037)
					{	/* Llib/thread.scm 566 */
						obj_t BgL_fun1559z00_4909;

						BgL_fun1559z00_4909 =
							(((BgL_nothreadz00_bglt) COBJECT(
									((BgL_nothreadz00_bglt) BgL_thz00_3792)))->BgL_z52cleanupz52);
						BGL_PROCEDURE_CALL0(BgL_fun1559z00_4909);
					}
				else
					{	/* Llib/thread.scm 566 */
						BFALSE;
					}
			}
			{	/* Llib/thread.scm 567 */
				obj_t BgL_list1562z00_4910;

				BgL_list1562z00_4910 = MAKE_YOUNG_PAIR(BINT(0L), BNIL);
				return BGl_exitz00zz__errorz00(BgL_list1562z00_4910);
			}
		}

	}



/* &thread-join!-nothrea1238 */
	obj_t BGl_z62threadzd2joinz12zd2nothrea1238z70zz__threadz00(obj_t
		BgL_envz00_3793, obj_t BgL_thz00_3794, obj_t BgL_timeoutz00_3795)
	{
		{	/* Llib/thread.scm 555 */
			{	/* Llib/thread.scm 557 */
				bool_t BgL_test2753z00_7050;

				{	/* Llib/thread.scm 557 */
					obj_t BgL_arg1555z00_4912;

					BgL_arg1555z00_4912 =
						(((BgL_nothreadz00_bglt) COBJECT(
								((BgL_nothreadz00_bglt) BgL_thz00_3794)))->
						BgL_endzd2exceptionzd2);
					{	/* Llib/thread.scm 557 */
						obj_t BgL_classz00_4913;

						BgL_classz00_4913 = BGl_z62exceptionz62zz__objectz00;
						if (BGL_OBJECTP(BgL_arg1555z00_4912))
							{	/* Llib/thread.scm 557 */
								BgL_objectz00_bglt BgL_arg1916z00_4914;

								BgL_arg1916z00_4914 =
									(BgL_objectz00_bglt) (BgL_arg1555z00_4912);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Llib/thread.scm 557 */
										long BgL_idxz00_4915;

										BgL_idxz00_4915 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1916z00_4914);
										BgL_test2753z00_7050 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_4915 + 2L)) == BgL_classz00_4913);
									}
								else
									{	/* Llib/thread.scm 557 */
										bool_t BgL_res2029z00_4918;

										{	/* Llib/thread.scm 557 */
											obj_t BgL_oclassz00_4919;

											{	/* Llib/thread.scm 557 */
												obj_t BgL_arg1925z00_4920;
												long BgL_arg1926z00_4921;

												BgL_arg1925z00_4920 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Llib/thread.scm 557 */
													long BgL_arg1927z00_4922;

													BgL_arg1927z00_4922 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1916z00_4914);
													BgL_arg1926z00_4921 =
														(BgL_arg1927z00_4922 - OBJECT_TYPE);
												}
												BgL_oclassz00_4919 =
													VECTOR_REF(BgL_arg1925z00_4920, BgL_arg1926z00_4921);
											}
											{	/* Llib/thread.scm 557 */
												bool_t BgL__ortest_1147z00_4923;

												BgL__ortest_1147z00_4923 =
													(BgL_classz00_4913 == BgL_oclassz00_4919);
												if (BgL__ortest_1147z00_4923)
													{	/* Llib/thread.scm 557 */
														BgL_res2029z00_4918 = BgL__ortest_1147z00_4923;
													}
												else
													{	/* Llib/thread.scm 557 */
														long BgL_odepthz00_4924;

														{	/* Llib/thread.scm 557 */
															obj_t BgL_arg1912z00_4925;

															BgL_arg1912z00_4925 = (BgL_oclassz00_4919);
															BgL_odepthz00_4924 =
																BGL_CLASS_DEPTH(BgL_arg1912z00_4925);
														}
														if ((2L < BgL_odepthz00_4924))
															{	/* Llib/thread.scm 557 */
																obj_t BgL_arg1910z00_4926;

																{	/* Llib/thread.scm 557 */
																	obj_t BgL_arg1911z00_4927;

																	BgL_arg1911z00_4927 = (BgL_oclassz00_4919);
																	BgL_arg1910z00_4926 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1911z00_4927,
																		2L);
																}
																BgL_res2029z00_4918 =
																	(BgL_arg1910z00_4926 == BgL_classz00_4913);
															}
														else
															{	/* Llib/thread.scm 557 */
																BgL_res2029z00_4918 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2753z00_7050 = BgL_res2029z00_4918;
									}
							}
						else
							{	/* Llib/thread.scm 557 */
								BgL_test2753z00_7050 = ((bool_t) 0);
							}
					}
				}
				if (BgL_test2753z00_7050)
					{	/* Llib/thread.scm 557 */
						return
							BGl_raisez00zz__errorz00(
							(((BgL_nothreadz00_bglt) COBJECT(
										((BgL_nothreadz00_bglt) BgL_thz00_3794)))->
								BgL_endzd2exceptionzd2));
					}
				else
					{	/* Llib/thread.scm 557 */
						return
							(((BgL_nothreadz00_bglt) COBJECT(
									((BgL_nothreadz00_bglt) BgL_thz00_3794)))->
							BgL_endzd2resultzd2);
					}
			}
		}

	}



/* &thread-start-joinabl1236 */
	obj_t BGl_z62threadzd2startzd2joinabl1236z62zz__threadz00(obj_t
		BgL_envz00_3796, obj_t BgL_thz00_3797)
	{
		{	/* Llib/thread.scm 549 */
			return
				BGl_threadzd2startz12zc0zz__threadz00(
				((BgL_threadz00_bglt) ((BgL_nothreadz00_bglt) BgL_thz00_3797)), BNIL);
		}

	}



/* &thread-start!-nothre1234 */
	obj_t BGl_z62threadzd2startz12zd2nothre1234z70zz__threadz00(obj_t
		BgL_envz00_3798, obj_t BgL_thz00_3799, obj_t BgL_scdz00_3800)
	{
		{	/* Llib/thread.scm 533 */
			return
				BGl_threadzd2startz12zd2nothre1234ze70zf5zz__threadz00(
				((BgL_nothreadz00_bglt) BgL_thz00_3799), BgL_scdz00_3800);
		}

	}



/* <@exit:1544>~0 */
	obj_t BGl_zc3z04exitza31544ze3ze70z60zz__threadz00(BgL_nothreadz00_bglt
		BgL_i1071z00_3835, obj_t BgL_cell1075z00_3834, obj_t BgL_env1080z00_3833)
	{
		{	/* Llib/thread.scm 538 */
			jmp_buf_t jmpbuf;
			void *BgL_an_exit1084z00_1742;

			if (SET_EXIT(BgL_an_exit1084z00_1742))
				{
					return BGL_EXIT_VALUE();
				}
			else
				{
#if( SIGSETJMP_SAVESIGS == 0 )
				 // MS: CARE 5 jan 2021: see runtime/Clib/csystem.c
				 // bgl_restore_signal_handlers();
#endif

					BgL_an_exit1084z00_1742 = (void *) jmpbuf;
					PUSH_ENV_EXIT(BgL_env1080z00_3833, BgL_an_exit1084z00_1742, 1L);
					{	/* Llib/thread.scm 538 */
						obj_t BgL_escape1076z00_1743;

						BgL_escape1076z00_1743 =
							BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1080z00_3833);
						{	/* Llib/thread.scm 538 */
							obj_t BgL_res1087z00_1744;

							{	/* Llib/thread.scm 538 */
								obj_t BgL_ohs1072z00_1745;

								BgL_ohs1072z00_1745 =
									BGL_ENV_ERROR_HANDLER_GET(BgL_env1080z00_3833);
								{	/* Llib/thread.scm 538 */
									obj_t BgL_hds1073z00_1746;

									BgL_hds1073z00_1746 =
										MAKE_STACK_PAIR(BgL_escape1076z00_1743,
										BgL_cell1075z00_3834);
									BGL_ENV_ERROR_HANDLER_SET(BgL_env1080z00_3833,
										BgL_hds1073z00_1746);
									BUNSPEC;
									{	/* Llib/thread.scm 538 */
										obj_t BgL_exitd1081z00_1747;

										BgL_exitd1081z00_1747 =
											BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1080z00_3833);
										{	/* Llib/thread.scm 538 */
											obj_t BgL_tmp1083z00_1748;

											{	/* Llib/thread.scm 538 */
												obj_t BgL_arg1546z00_1751;

												BgL_arg1546z00_1751 =
													BGL_EXITD_PROTECT(BgL_exitd1081z00_1747);
												BgL_tmp1083z00_1748 =
													MAKE_YOUNG_PAIR(BgL_ohs1072z00_1745,
													BgL_arg1546z00_1751);
											}
											{	/* Llib/thread.scm 538 */

												BGL_EXITD_PROTECT_SET(BgL_exitd1081z00_1747,
													BgL_tmp1083z00_1748);
												BUNSPEC;
												{	/* Llib/thread.scm 538 */
													obj_t BgL_tmp1082z00_1749;

													{
														obj_t BgL_auxz00_7096;

														{	/* Llib/thread.scm 542 */
															obj_t BgL_fun1545z00_1750;

															BgL_fun1545z00_1750 =
																(((BgL_nothreadz00_bglt)
																	COBJECT(BgL_i1071z00_3835))->BgL_bodyz00);
															BgL_auxz00_7096 =
																BGL_PROCEDURE_CALL0(BgL_fun1545z00_1750);
														}
														BgL_tmp1082z00_1749 =
															((((BgL_nothreadz00_bglt)
																	COBJECT(BgL_i1071z00_3835))->
																BgL_endzd2resultzd2) =
															((obj_t) BgL_auxz00_7096), BUNSPEC);
													}
													{	/* Llib/thread.scm 538 */
														bool_t BgL_test2758z00_7102;

														{	/* Llib/thread.scm 538 */
															obj_t BgL_arg1898z00_3104;

															BgL_arg1898z00_3104 =
																BGL_EXITD_PROTECT(BgL_exitd1081z00_1747);
															BgL_test2758z00_7102 = PAIRP(BgL_arg1898z00_3104);
														}
														if (BgL_test2758z00_7102)
															{	/* Llib/thread.scm 538 */
																obj_t BgL_arg1896z00_3105;

																{	/* Llib/thread.scm 538 */
																	obj_t BgL_arg1897z00_3106;

																	BgL_arg1897z00_3106 =
																		BGL_EXITD_PROTECT(BgL_exitd1081z00_1747);
																	BgL_arg1896z00_3105 =
																		CDR(((obj_t) BgL_arg1897z00_3106));
																}
																BGL_EXITD_PROTECT_SET(BgL_exitd1081z00_1747,
																	BgL_arg1896z00_3105);
																BUNSPEC;
															}
														else
															{	/* Llib/thread.scm 538 */
																BFALSE;
															}
													}
													BGL_ENV_ERROR_HANDLER_SET(BgL_env1080z00_3833,
														BgL_ohs1072z00_1745);
													BUNSPEC;
													BgL_res1087z00_1744 = BgL_tmp1082z00_1749;
												}
											}
										}
									}
								}
							}
							POP_ENV_EXIT(BgL_env1080z00_3833);
							return BgL_res1087z00_1744;
						}
					}
				}
		}

	}



/* thread-start!-nothre1234~0 */
	obj_t
		BGl_threadzd2startz12zd2nothre1234ze70zf5zz__threadz00(BgL_nothreadz00_bglt
		BgL_thz00_1727, obj_t BgL_scdz00_1728)
	{
		{	/* Llib/thread.scm 533 */
			{	/* Llib/thread.scm 534 */
				obj_t BgL_threadz00_1731;

				BgL_threadz00_1731 = BGl_za2nothreadzd2currentza2zd2zz__threadz00;
				{	/* Llib/thread.scm 535 */
					obj_t BgL_exitd1067z00_1732;

					BgL_exitd1067z00_1732 = BGL_EXITD_TOP_AS_OBJ();
					{	/* Llib/thread.scm 544 */
						obj_t BgL_zc3z04anonymousza31548ze3z87_3764;

						BgL_zc3z04anonymousza31548ze3z87_3764 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31548ze3ze5zz__threadz00, (int) (0L),
							(int) (1L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31548ze3z87_3764, (int) (0L),
							BgL_threadz00_1731);
						{	/* Llib/thread.scm 535 */
							obj_t BgL_arg1899z00_3101;

							{	/* Llib/thread.scm 535 */
								obj_t BgL_arg1901z00_3102;

								BgL_arg1901z00_3102 = BGL_EXITD_PROTECT(BgL_exitd1067z00_1732);
								BgL_arg1899z00_3101 =
									MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31548ze3z87_3764,
									BgL_arg1901z00_3102);
							}
							BGL_EXITD_PROTECT_SET(BgL_exitd1067z00_1732, BgL_arg1899z00_3101);
							BUNSPEC;
						}
						{	/* Llib/thread.scm 536 */
							BgL_nothreadz00_bglt BgL_tmp1069z00_1734;

							BGl_za2nothreadzd2currentza2zd2zz__threadz00 =
								((obj_t) BgL_thz00_1727);
							{	/* Llib/thread.scm 538 */
								obj_t BgL_env1080z00_1737;

								BgL_env1080z00_1737 = BGL_CURRENT_DYNAMIC_ENV();
								{	/* Llib/thread.scm 538 */
									obj_t BgL_cell1075z00_1738;

									BgL_cell1075z00_1738 = MAKE_STACK_CELL(BUNSPEC);
									{	/* Llib/thread.scm 538 */
										obj_t BgL_val1079z00_1739;

										BgL_val1079z00_1739 =
											BGl_zc3z04exitza31544ze3ze70z60zz__threadz00
											(BgL_thz00_1727, BgL_cell1075z00_1738,
											BgL_env1080z00_1737);
										if ((BgL_val1079z00_1739 == BgL_cell1075z00_1738))
											{	/* Llib/thread.scm 538 */
												{	/* Llib/thread.scm 538 */
													int BgL_tmpz00_7126;

													BgL_tmpz00_7126 = (int) (0L);
													BGL_SIGSETMASK(BgL_tmpz00_7126);
												}
												{	/* Llib/thread.scm 538 */
													obj_t BgL_arg1543z00_1740;

													BgL_arg1543z00_1740 =
														CELL_REF(((obj_t) BgL_val1079z00_1739));
													((((BgL_nothreadz00_bglt) COBJECT(BgL_thz00_1727))->
															BgL_endzd2exceptionzd2) =
														((obj_t) BgL_arg1543z00_1740), BUNSPEC);
													BGl_raisez00zz__errorz00(BgL_arg1543z00_1740);
											}}
										else
											{	/* Llib/thread.scm 538 */
												BgL_val1079z00_1739;
											}
									}
								}
							}
							BgL_tmp1069z00_1734 = BgL_thz00_1727;
							{	/* Llib/thread.scm 535 */
								bool_t BgL_test2760z00_7133;

								{	/* Llib/thread.scm 535 */
									obj_t BgL_arg1898z00_3110;

									BgL_arg1898z00_3110 =
										BGL_EXITD_PROTECT(BgL_exitd1067z00_1732);
									BgL_test2760z00_7133 = PAIRP(BgL_arg1898z00_3110);
								}
								if (BgL_test2760z00_7133)
									{	/* Llib/thread.scm 535 */
										obj_t BgL_arg1896z00_3111;

										{	/* Llib/thread.scm 535 */
											obj_t BgL_arg1897z00_3112;

											BgL_arg1897z00_3112 =
												BGL_EXITD_PROTECT(BgL_exitd1067z00_1732);
											BgL_arg1896z00_3111 = CDR(((obj_t) BgL_arg1897z00_3112));
										}
										BGL_EXITD_PROTECT_SET(BgL_exitd1067z00_1732,
											BgL_arg1896z00_3111);
										BUNSPEC;
									}
								else
									{	/* Llib/thread.scm 535 */
										BFALSE;
									}
							}
							BGl_za2nothreadzd2currentza2zd2zz__threadz00 = BgL_threadz00_1731;
							return ((obj_t) BgL_tmp1069z00_1734);
						}
					}
				}
			}
		}

	}



/* &<@anonymous:1548> */
	obj_t BGl_z62zc3z04anonymousza31548ze3ze5zz__threadz00(obj_t BgL_envz00_3801)
	{
		{	/* Llib/thread.scm 535 */
			{	/* Llib/thread.scm 544 */
				obj_t BgL_threadz00_3802;

				BgL_threadz00_3802 = PROCEDURE_REF(BgL_envz00_3801, (int) (0L));
				return (BGl_za2nothreadzd2currentza2zd2zz__threadz00 =
					BgL_threadz00_3802, BUNSPEC);
			}
		}

	}



/* &object-print-thread1194 */
	obj_t BGl_z62objectzd2printzd2thread1194z62zz__threadz00(obj_t
		BgL_envz00_3803, obj_t BgL_oz00_3804, obj_t BgL_portz00_3805,
		obj_t BgL_printzd2slotzd2_3806)
	{
		{	/* Llib/thread.scm 378 */
			{	/* Llib/thread.scm 379 */
				obj_t BgL_list1542z00_4930;

				BgL_list1542z00_4930 = MAKE_YOUNG_PAIR(BgL_portz00_3805, BNIL);
				return
					BGl_objectzd2writezd2zz__objectz00(
					((BgL_objectz00_bglt)
						((BgL_threadz00_bglt) BgL_oz00_3804)), BgL_list1542z00_4930);
			}
		}

	}



/* &object-write-thread1192 */
	obj_t BGl_z62objectzd2writezd2thread1192z62zz__threadz00(obj_t
		BgL_envz00_3807, obj_t BgL_oz00_3808, obj_t BgL_portz00_3809)
	{
		{	/* Llib/thread.scm 369 */
			{	/* Llib/thread.scm 370 */
				obj_t BgL_arg1525z00_4932;

				if (PAIRP(BgL_portz00_3809))
					{	/* Llib/thread.scm 370 */
						BgL_arg1525z00_4932 = CAR(BgL_portz00_3809);
					}
				else
					{	/* Llib/thread.scm 370 */
						obj_t BgL_tmpz00_7150;

						BgL_tmpz00_7150 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_arg1525z00_4932 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_7150);
					}
				{	/* Llib/thread.scm 372 */
					obj_t BgL_zc3z04anonymousza31528ze3z87_4933;

					BgL_zc3z04anonymousza31528ze3z87_4933 =
						MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31528ze3ze5zz__threadz00,
						(int) (0L), (int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31528ze3z87_4933,
						(int) (0L), ((obj_t) ((BgL_threadz00_bglt) BgL_oz00_3808)));
					return
						BGl_withzd2outputzd2tozd2portzd2zz__r4_ports_6_10_1z00
						(BgL_arg1525z00_4932, BgL_zc3z04anonymousza31528ze3z87_4933);
				}
			}
		}

	}



/* &<@anonymous:1528> */
	obj_t BGl_z62zc3z04anonymousza31528ze3ze5zz__threadz00(obj_t BgL_envz00_3810)
	{
		{	/* Llib/thread.scm 371 */
			{	/* Llib/thread.scm 372 */
				BgL_threadz00_bglt BgL_oz00_3811;

				BgL_oz00_3811 =
					((BgL_threadz00_bglt) PROCEDURE_REF(BgL_envz00_3810, (int) (0L)));
				{	/* Llib/thread.scm 373 */
					obj_t BgL_arg1529z00_4934;
					obj_t BgL_arg1530z00_4935;

					{	/* Llib/thread.scm 373 */
						obj_t BgL_arg1540z00_4936;

						{	/* Llib/thread.scm 373 */
							obj_t BgL_arg1925z00_4937;
							long BgL_arg1926z00_4938;

							BgL_arg1925z00_4937 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Llib/thread.scm 373 */
								long BgL_arg1927z00_4939;

								BgL_arg1927z00_4939 =
									BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_oz00_3811));
								BgL_arg1926z00_4938 = (BgL_arg1927z00_4939 - OBJECT_TYPE);
							}
							BgL_arg1540z00_4936 =
								VECTOR_REF(BgL_arg1925z00_4937, BgL_arg1926z00_4938);
						}
						BgL_arg1529z00_4934 =
							BGl_classzd2namezd2zz__objectz00(BgL_arg1540z00_4936);
					}
					BgL_arg1530z00_4935 =
						(((BgL_threadz00_bglt) COBJECT(BgL_oz00_3811))->BgL_namez00);
					{	/* Llib/thread.scm 373 */
						obj_t BgL_list1531z00_4940;

						{	/* Llib/thread.scm 373 */
							obj_t BgL_arg1535z00_4941;

							{	/* Llib/thread.scm 373 */
								obj_t BgL_arg1536z00_4942;

								{	/* Llib/thread.scm 373 */
									obj_t BgL_arg1537z00_4943;

									{	/* Llib/thread.scm 373 */
										obj_t BgL_arg1539z00_4944;

										BgL_arg1539z00_4944 =
											MAKE_YOUNG_PAIR(BGl_string2361z00zz__threadz00, BNIL);
										BgL_arg1537z00_4943 =
											MAKE_YOUNG_PAIR(BgL_arg1530z00_4935, BgL_arg1539z00_4944);
									}
									BgL_arg1536z00_4942 =
										MAKE_YOUNG_PAIR(BGl_string2362z00zz__threadz00,
										BgL_arg1537z00_4943);
								}
								BgL_arg1535z00_4941 =
									MAKE_YOUNG_PAIR(BgL_arg1529z00_4934, BgL_arg1536z00_4942);
							}
							BgL_list1531z00_4940 =
								MAKE_YOUNG_PAIR(BGl_string2363z00zz__threadz00,
								BgL_arg1535z00_4941);
						}
						return
							BGl_displayza2za2zz__r4_output_6_10_3z00(BgL_list1531z00_4940);
					}
				}
			}
		}

	}



/* &object-display-threa1190 */
	obj_t BGl_z62objectzd2displayzd2threa1190z62zz__threadz00(obj_t
		BgL_envz00_3812, obj_t BgL_oz00_3813, obj_t BgL_portz00_3814)
	{
		{	/* Llib/thread.scm 360 */
			{	/* Llib/thread.scm 361 */
				obj_t BgL_arg1508z00_4946;

				if (PAIRP(BgL_portz00_3814))
					{	/* Llib/thread.scm 361 */
						BgL_arg1508z00_4946 = CAR(BgL_portz00_3814);
					}
				else
					{	/* Llib/thread.scm 361 */
						obj_t BgL_tmpz00_7180;

						BgL_tmpz00_7180 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_arg1508z00_4946 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_7180);
					}
				{	/* Llib/thread.scm 363 */
					obj_t BgL_zc3z04anonymousza31511ze3z87_4947;

					BgL_zc3z04anonymousza31511ze3z87_4947 =
						MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31511ze3ze5zz__threadz00,
						(int) (0L), (int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31511ze3z87_4947,
						(int) (0L), ((obj_t) ((BgL_threadz00_bglt) BgL_oz00_3813)));
					return
						BGl_withzd2outputzd2tozd2portzd2zz__r4_ports_6_10_1z00
						(BgL_arg1508z00_4946, BgL_zc3z04anonymousza31511ze3z87_4947);
				}
			}
		}

	}



/* &<@anonymous:1511> */
	obj_t BGl_z62zc3z04anonymousza31511ze3ze5zz__threadz00(obj_t BgL_envz00_3815)
	{
		{	/* Llib/thread.scm 362 */
			{	/* Llib/thread.scm 363 */
				BgL_threadz00_bglt BgL_oz00_3816;

				BgL_oz00_3816 =
					((BgL_threadz00_bglt) PROCEDURE_REF(BgL_envz00_3815, (int) (0L)));
				{	/* Llib/thread.scm 364 */
					obj_t BgL_arg1513z00_4948;
					obj_t BgL_arg1514z00_4949;

					{	/* Llib/thread.scm 364 */
						obj_t BgL_arg1523z00_4950;

						{	/* Llib/thread.scm 364 */
							obj_t BgL_arg1925z00_4951;
							long BgL_arg1926z00_4952;

							BgL_arg1925z00_4951 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Llib/thread.scm 364 */
								long BgL_arg1927z00_4953;

								BgL_arg1927z00_4953 =
									BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_oz00_3816));
								BgL_arg1926z00_4952 = (BgL_arg1927z00_4953 - OBJECT_TYPE);
							}
							BgL_arg1523z00_4950 =
								VECTOR_REF(BgL_arg1925z00_4951, BgL_arg1926z00_4952);
						}
						BgL_arg1513z00_4948 =
							BGl_classzd2namezd2zz__objectz00(BgL_arg1523z00_4950);
					}
					BgL_arg1514z00_4949 =
						(((BgL_threadz00_bglt) COBJECT(BgL_oz00_3816))->BgL_namez00);
					{	/* Llib/thread.scm 364 */
						obj_t BgL_list1515z00_4954;

						{	/* Llib/thread.scm 364 */
							obj_t BgL_arg1516z00_4955;

							{	/* Llib/thread.scm 364 */
								obj_t BgL_arg1517z00_4956;

								{	/* Llib/thread.scm 364 */
									obj_t BgL_arg1521z00_4957;

									{	/* Llib/thread.scm 364 */
										obj_t BgL_arg1522z00_4958;

										BgL_arg1522z00_4958 =
											MAKE_YOUNG_PAIR(BGl_string2361z00zz__threadz00, BNIL);
										BgL_arg1521z00_4957 =
											MAKE_YOUNG_PAIR(BgL_arg1514z00_4949, BgL_arg1522z00_4958);
									}
									BgL_arg1517z00_4956 =
										MAKE_YOUNG_PAIR(BGl_string2362z00zz__threadz00,
										BgL_arg1521z00_4957);
								}
								BgL_arg1516z00_4955 =
									MAKE_YOUNG_PAIR(BgL_arg1513z00_4948, BgL_arg1517z00_4956);
							}
							BgL_list1515z00_4954 =
								MAKE_YOUNG_PAIR(BGl_string2363z00zz__threadz00,
								BgL_arg1516z00_4955);
						}
						return
							BGl_displayza2za2zz__r4_output_6_10_3z00(BgL_list1515z00_4954);
					}
				}
			}
		}

	}



/* &tb-current-thread-no1184 */
	obj_t BGl_z62tbzd2currentzd2threadzd2no1184zb0zz__threadz00(obj_t
		BgL_envz00_3817, obj_t BgL_tbz00_3818)
	{
		{	/* Llib/thread.scm 344 */
			return BGl_za2nothreadzd2currentza2zd2zz__threadz00;
		}

	}



/* &tb-make-thread-nothr1182 */
	BgL_threadz00_bglt BGl_z62tbzd2makezd2threadzd2nothr1182zb0zz__threadz00(obj_t
		BgL_envz00_3819, obj_t BgL_tbz00_3820, obj_t BgL_bodyz00_3821,
		obj_t BgL_namez00_3822)
	{
		{	/* Llib/thread.scm 336 */
			{	/* Llib/thread.scm 337 */
				BgL_nothreadz00_bglt BgL_new1062z00_4961;

				{	/* Llib/thread.scm 339 */
					BgL_nothreadz00_bglt BgL_new1061z00_4962;

					BgL_new1061z00_4962 =
						((BgL_nothreadz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_nothreadz00_bgl))));
					{	/* Llib/thread.scm 339 */
						long BgL_arg1507z00_4963;

						BgL_arg1507z00_4963 = BGL_CLASS_NUM(BGl_nothreadz00zz__threadz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1061z00_4962), BgL_arg1507z00_4963);
					}
					BgL_new1062z00_4961 = BgL_new1061z00_4962;
				}
				((((BgL_threadz00_bglt) COBJECT(
								((BgL_threadz00_bglt) BgL_new1062z00_4961)))->BgL_namez00) =
					((obj_t) BgL_namez00_3822), BUNSPEC);
				((((BgL_nothreadz00_bglt) COBJECT(BgL_new1062z00_4961))->BgL_bodyz00) =
					((obj_t) BgL_bodyz00_3821), BUNSPEC);
				((((BgL_nothreadz00_bglt) COBJECT(BgL_new1062z00_4961))->
						BgL_z52specificz52) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nothreadz00_bglt) COBJECT(BgL_new1062z00_4961))->
						BgL_z52cleanupz52) = ((obj_t) BFALSE), BUNSPEC);
				((((BgL_nothreadz00_bglt) COBJECT(BgL_new1062z00_4961))->
						BgL_endzd2resultzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nothreadz00_bglt) COBJECT(BgL_new1062z00_4961))->
						BgL_endzd2exceptionzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nothreadz00_bglt) COBJECT(BgL_new1062z00_4961))->
						BgL_z52namez52) =
					((obj_t) BGl_string2252z00zz__threadz00), BUNSPEC);
				{	/* Llib/thread.scm 337 */
					obj_t BgL_fun1506z00_4964;

					BgL_fun1506z00_4964 =
						BGl_classzd2constructorzd2zz__objectz00
						(BGl_nothreadz00zz__threadz00);
					BGL_PROCEDURE_CALL1(BgL_fun1506z00_4964,
						((obj_t) BgL_new1062z00_4961));
				}
				return ((BgL_threadz00_bglt) BgL_new1062z00_4961);
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__threadz00(void)
	{
		{	/* Llib/thread.scm 17 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string2192z00zz__threadz00));
			return
				BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string2192z00zz__threadz00));
		}

	}

#ifdef __cplusplus
}
#endif
