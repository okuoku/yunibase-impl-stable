/*===========================================================================*/
/*   (Llib/object.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Llib/object.scm -indent -o objs/obj_u/Llib/object.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___OBJECT_TYPE_DEFINITIONS
#define BGL___OBJECT_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_z62conditionz62_bgl
	{
		header_t header;
		obj_t widening;
	}                      *BgL_z62conditionz62_bglt;

	typedef struct BgL_z62exceptionz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
	}                      *BgL_z62exceptionz62_bglt;

	typedef struct BgL_z62errorz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                  *BgL_z62errorz62_bglt;

	typedef struct BgL_z62typezd2errorzb0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
		obj_t BgL_typez00;
	}                         *BgL_z62typezd2errorzb0_bglt;

	typedef struct BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
		obj_t BgL_indexz00;
	}                                             
		*BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt;

	typedef struct BgL_z62iozd2errorzb0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                       *BgL_z62iozd2errorzb0_bglt;

	typedef struct BgL_z62iozd2portzd2errorz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                              *BgL_z62iozd2portzd2errorz62_bglt;

	typedef struct BgL_z62iozd2readzd2errorz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                              *BgL_z62iozd2readzd2errorz62_bglt;

	typedef struct BgL_z62iozd2writezd2errorz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                               *BgL_z62iozd2writezd2errorz62_bglt;

	typedef struct BgL_z62iozd2closedzd2errorz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                                *BgL_z62iozd2closedzd2errorz62_bglt;

	typedef struct BgL_z62iozd2filezd2notzd2foundzd2errorz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                                           
		*BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt;

	typedef struct BgL_z62iozd2parsezd2errorz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                               *BgL_z62iozd2parsezd2errorz62_bglt;

	typedef struct BgL_z62iozd2unknownzd2hostzd2errorzb0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                                       
		*BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt;

	typedef struct BgL_z62iozd2malformedzd2urlzd2errorzb0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                                        
		*BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt;

	typedef struct BgL_z62iozd2sigpipezd2errorz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                                 *BgL_z62iozd2sigpipezd2errorz62_bglt;

	typedef struct BgL_z62iozd2timeoutzd2errorz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                                 *BgL_z62iozd2timeoutzd2errorz62_bglt;

	typedef struct BgL_z62iozd2connectionzd2errorz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                                    *BgL_z62iozd2connectionzd2errorz62_bglt;

	typedef struct BgL_z62processzd2exceptionzb0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                                *BgL_z62processzd2exceptionzb0_bglt;

	typedef struct BgL_z62stackzd2overflowzd2errorz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                                    
		*BgL_z62stackzd2overflowzd2errorz62_bglt;

	typedef struct BgL_z62securityzd2exceptionzb0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_messagez00;
	}                                 *BgL_z62securityzd2exceptionzb0_bglt;

	typedef struct BgL_z62accesszd2controlzd2exceptionz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_messagez00;
		obj_t BgL_objz00;
		obj_t BgL_permissionz00;
	}                                        
		*BgL_z62accesszd2controlzd2exceptionz62_bglt;

	typedef struct BgL_z62warningz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_argsz00;
	}                    *BgL_z62warningz62_bglt;

	typedef struct BgL_z62evalzd2warningzb0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_argsz00;
	}                           *BgL_z62evalzd2warningzb0_bglt;


#endif													// BGL___OBJECT_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00(obj_t);
	static obj_t BGl_z62lambda1927z62zz__objectz00(obj_t, obj_t, obj_t);
	static BgL_objectz00_bglt BGl_z62lambda1846z62zz__objectz00(obj_t);
	static BgL_objectz00_bglt BGl_z62lambda1848z62zz__objectz00(obj_t);
	static obj_t BGl_z62objectzd2displayzb0zz__objectz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_z62processzd2exceptionzb0zz__objectz00 = BUNSPEC;
	static obj_t BGl_z62classzd2allzd2fieldsz62zz__objectz00(obj_t, obj_t);
	extern obj_t make_vector_uncollectable(long, obj_t);
	static obj_t BGl_z62objectzd2print1399zb0zz__objectz00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL long bgl_types_number(void);
	static obj_t BGl_z62findzd2methodzb0zz__objectz00(obj_t, obj_t, obj_t);
	static obj_t BGl_doublezd2nbzd2genericsz12z12zz__objectz00(void);
	static BgL_z62typezd2errorzb0_bglt BGl_z62lambda1933z62zz__objectz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_objectzd2wideningzd2zz__objectz00(BgL_objectz00_bglt);
	static BgL_z62typezd2errorzb0_bglt BGl_z62lambda1935z62zz__objectz00(obj_t);
	static obj_t BGl_z62z52isa64zd2objectzf2cdepthzf3ze3zz__objectz00(obj_t,
		obj_t, obj_t, obj_t);
	static BgL_z62conditionz62_bglt BGl_z62lambda1857z62zz__objectz00(obj_t);
	static BgL_z62conditionz62_bglt BGl_z62lambda1859z62zz__objectz00(obj_t);
	static obj_t BGl_z62bigloozd2genericzd2bucketzd2siza7ez17zz__objectz00(obj_t);
	static obj_t BGl_z62objectzd2wideningzb0zz__objectz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	static obj_t BGl_registerzd2genericzd2sanszd2lockz12zc0zz__objectz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62classzd2nilzd2initz12z70zz__objectz00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__objectz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31952ze3ze5zz__objectz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1941z62zz__objectz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1942z62zz__objectz00(obj_t, obj_t, obj_t);
	static BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt
		BGl_z62lambda1948z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_z52isazd2objectzf2finalzf3z81zz__objectz00(BgL_objectz00_bglt, obj_t);
	static BgL_z62exceptionz62_bglt BGl_z62lambda1869z62zz__objectz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62classzd2subclasseszb0zz__objectz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_classzd2fieldzf3z21zz__objectz00(obj_t);
	static obj_t BGl_z62classzd2fieldzd2mutatorz62zz__objectz00(obj_t, obj_t);
	extern obj_t bigloo_generic_mutex;
	BGL_EXPORTED_DECL int
		BGl_bigloozd2genericzd2bucketzd2powzd2zz__objectz00(void);
	static obj_t BGl_z62zc3z04anonymousza31880ze3ze5zz__objectz00(obj_t);
	extern obj_t bgl_display_obj(obj_t, obj_t);
	static BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt
		BGl_z62lambda1950z62zz__objectz00(obj_t);
	extern bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31937ze3ze5zz__objectz00(obj_t, obj_t);
	static BgL_z62exceptionz62_bglt BGl_z62lambda1871z62zz__objectz00(obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62classzd2numzb0zz__objectz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1956z62zz__objectz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1957z62zz__objectz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1878z62zz__objectz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1879z62zz__objectz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2910z00zz__objectz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2classesza2z00zz__objectz00 = BUNSPEC;
	BGL_EXPORTED_DECL bool_t
		BGl_classzd2fieldzd2defaultzd2valuezf3z21zz__objectz00(obj_t);
	static obj_t BGl_symbol2912z00zz__objectz00 = BUNSPEC;
	static obj_t BGl_symbol2913z00zz__objectz00 = BUNSPEC;
	static obj_t BGl_symbol2915z00zz__objectz00 = BUNSPEC;
	static obj_t BGl_symbol2917z00zz__objectz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_classzd2virtualzd2zz__objectz00(obj_t);
	static obj_t BGl_symbol2919z00zz__objectz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_z62iozd2sigpipezd2errorz62zz__objectz00 = BUNSPEC;
	extern obj_t BGl_warningz00zz__errorz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31873ze3ze5zz__objectz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_methodzd2arrayzd2refz00zz__objectz00(obj_t, obj_t,
		int);
	BGL_EXPORTED_DECL obj_t BGl_classzd2fieldszd2zz__objectz00(obj_t);
	static BgL_z62iozd2errorzb0_bglt BGl_z62lambda1963z62zz__objectz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_z62iozd2errorzb0_bglt BGl_z62lambda1965z62zz__objectz00(obj_t);
	static obj_t BGl_z62lambda1886z62zz__objectz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1887z62zz__objectz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2921z00zz__objectz00 = BUNSPEC;
	static obj_t BGl_initializa7ezd2objectsz12z67zz__objectz00(void);
	static obj_t BGl_toplevelzd2initzd2zz__objectz00(void);
	BGL_EXPORTED_DEF obj_t BGl_z62iozd2writezd2errorz62zz__objectz00 = BUNSPEC;
	static obj_t BGl_z62classzd2modulezb0zz__objectz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_findzd2methodzd2fromz00zz__objectz00(BgL_objectz00_bglt, obj_t, obj_t);
	static obj_t BGl_z62classzd2superzb0zz__objectz00(obj_t, obj_t);
	static BgL_z62iozd2portzd2errorz62_bglt
		BGl_z62lambda1973z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda1893z62zz__objectz00(obj_t, obj_t);
	static BgL_z62iozd2portzd2errorz62_bglt
		BGl_z62lambda1975z62zz__objectz00(obj_t);
	static obj_t BGl_z62lambda1894z62zz__objectz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_z62typezd2errorzb0zz__objectz00 = BUNSPEC;
	static obj_t BGl_z62genericzd2memoryzd2statisticsz62zz__objectz00(obj_t);
	static obj_t BGl_z62classzd2fieldzd2mutablezf3z91zz__objectz00(obj_t, obj_t);
	static obj_t BGl_z62classzd2fieldzd2defaultzd2valuezf3z43zz__objectz00(obj_t,
		obj_t);
	static obj_t BGl_z62objectzd2equalzf31402z43zz__objectz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32104ze3ze5zz__objectz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32007ze3ze5zz__objectz00(obj_t, obj_t);
	static obj_t BGl_z62evalzd2classzf3z43zz__objectz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_objectzd2displayzd2zz__objectz00(BgL_objectz00_bglt, obj_t);
	static obj_t BGl_z62objectzf3z91zz__objectz00(obj_t, obj_t);
	static BgL_z62iozd2readzd2errorz62_bglt
		BGl_z62lambda1983z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_z62iozd2parsezd2errorz62zz__objectz00 = BUNSPEC;
	static obj_t BGl_genericzd2initzd2zz__objectz00(void);
	static BgL_z62iozd2readzd2errorz62_bglt
		BGl_z62lambda1985z62zz__objectz00(obj_t);
	static obj_t BGl_z62objectzd2classzb0zz__objectz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_classzd2existszd2zz__objectz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32121ze3ze5zz__objectz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t,
		obj_t, bool_t, bool_t, obj_t, obj_t, obj_t);
	static obj_t BGl_za2nbzd2classeszd2maxza2z00zz__objectz00 = BUNSPEC;
	BGL_EXPORTED_DECL long BGl_classzd2numzd2zz__objectz00(obj_t);
	extern obj_t BGl_displayzd2tracezd2stackz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zz__objectz00(void);
	BGL_EXPORTED_DECL long BGl_classzd2indexzd2zz__objectz00(obj_t);
	extern obj_t BGl_copyzd2vectorzd2zz__r4_vectors_6_8z00(obj_t, long);
	BGL_EXPORTED_DECL bool_t
		BGl_classzd2fieldzd2mutablezf3zf3zz__objectz00(obj_t);
	static BgL_z62iozd2writezd2errorz62_bglt
		BGl_z62lambda1993z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static BgL_z62iozd2writezd2errorz62_bglt
		BGl_z62lambda1995z62zz__objectz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32114ze3ze5zz__objectz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32017ze3ze5zz__objectz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2inheritancesza2z00zz__objectz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_z62exceptionz62zz__objectz00 = BUNSPEC;
	static obj_t
		BGl_makezd2classzd2virtualzd2slotszd2vectorz00zz__objectz00(obj_t, obj_t);
	static obj_t BGl_z62callzd2virtualzd2setterz62zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_objectzd2equalzf3z21zz__objectz00(BgL_objectz00_bglt,
		BgL_objectz00_bglt);
	static obj_t BGl_z62isazf3z91zz__objectz00(obj_t, obj_t, obj_t);
	static obj_t BGl_extendzd2vectorzd2zz__objectz00(obj_t, obj_t, long);
	static obj_t BGl_z62zc3z04anonymousza32050ze3ze5zz__objectz00(obj_t, obj_t);
	static obj_t BGl_symbol2966z00zz__objectz00 = BUNSPEC;
	static obj_t BGl_symbol2967z00zz__objectz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_findzd2classzd2fieldz00zz__objectz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_classzd2abstractzf3z21zz__objectz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31967ze3ze5zz__objectz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_genericzd2memoryzd2statisticsz00zz__objectz00(void);
	static obj_t BGl_z62classzd2abstractzf3z43zz__objectz00(obj_t, obj_t);
	static obj_t BGl_z62classzd2fieldzd2virtualzf3z91zz__objectz00(obj_t, obj_t);
	static obj_t BGl_z62exceptionzd2notify1404zb0zz__objectz00(obj_t, obj_t);
	static obj_t BGl_symbol2972z00zz__objectz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza32132ze3ze5zz__objectz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32027ze3ze5zz__objectz00(obj_t, obj_t);
	static obj_t BGl_symbol2977z00zz__objectz00 = BUNSPEC;
	static obj_t BGl_symbol2979z00zz__objectz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_objectzd2printzd2zz__objectz00(BgL_objectz00_bglt,
		obj_t, obj_t);
	static obj_t BGl_z62exceptionzd2notifyzd2z62wa1416z00zz__objectz00(obj_t,
		obj_t);
	static obj_t BGl_z62widezd2objectzf3z43zz__objectz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_classzd2evdatazd2zz__objectz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31895ze3ze5zz__objectz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_isazf3zf3zz__objectz00(obj_t, obj_t);
	static obj_t BGl_z62methodzd2arrayzd2refz62zz__objectz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_classzd2fieldzd2mutatorz00zz__objectz00(obj_t);
	extern obj_t make_vector(long, obj_t);
	static obj_t BGl_symbol2984z00zz__objectz00 = BUNSPEC;
	static obj_t BGl_symbol2989z00zz__objectz00 = BUNSPEC;
	static obj_t BGl_z62findzd2methodzd2fromz62zz__objectz00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_classzd2fieldzd2virtualzf3zf3zz__objectz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31977ze3ze5zz__objectz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31888ze3ze5zz__objectz00(obj_t);
	static obj_t BGl_z62genericzd2methodzd2arrayz62zz__objectz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zz__objectz00(void);
	static obj_t BGl_symbol2994z00zz__objectz00 = BUNSPEC;
	static obj_t BGl_z62callzd2virtualzd2getterz62zz__objectz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_objectzd2writezd2zz__objectz00(BgL_objectz00_bglt,
		obj_t);
	static obj_t BGl_symbol2998z00zz__objectz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_classzd2fieldzd2namez00zz__objectz00(obj_t);
	static obj_t BGl_z62objectzd2display1389zb0zz__objectz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62objectzd2hashnumberzb0zz__objectz00(obj_t, obj_t);
	static obj_t BGl_z62classzd2fieldzd2accessorz62zz__objectz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32062ze3ze5zz__objectz00(obj_t, obj_t);
	static obj_t BGl_classzd2shrinkzd2zz__objectz00(obj_t);
	static obj_t BGl_za2inheritancezd2maxzd2depthza2z00zz__objectz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_genericzd2methodzd2arrayz00zz__objectz00(obj_t);
	static obj_t BGl_z62makezd2classzd2fieldz62zz__objectz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62objectzd2wideningzd2setz12z70zz__objectz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31987ze3ze5zz__objectz00(obj_t, obj_t);
	static obj_t BGl_za2classzd2keyza2zd2zz__objectz00 = BUNSPEC;
	static obj_t BGl_za2nbzd2genericsza2zd2zz__objectz00 = BUNSPEC;
	static obj_t BGl_z62findzd2classzd2byzd2hashzb0zz__objectz00(obj_t, obj_t);
	BGL_EXPORTED_DECL long BGl_classzd2hashzd2zz__objectz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_z52isazf2cdepthzf3z53zz__objectz00(obj_t, obj_t,
		long);
	static obj_t BGl_z62zc3z04anonymousza32039ze3ze5zz__objectz00(obj_t, obj_t);
	static obj_t BGl_z62findzd2superzd2classzd2methodzb0zz__objectz00(obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_classzd2allocatorzd2zz__objectz00(obj_t);
	static obj_t BGl_z62callzd2nextzd2virtualzd2setterzb0zz__objectz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_classzd2fieldzd2accessorz00zz__objectz00(obj_t);
	static obj_t BGl_z62nilzf3z91zz__objectz00(obj_t, obj_t);
	static obj_t BGl_z62classzd2allocatorzb0zz__objectz00(obj_t, obj_t);
	extern obj_t BGl_zb2zb2zz__r4_numbers_6_5z00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_classzd2widezf3z21zz__objectz00(obj_t);
	static BgL_z62iozd2closedzd2errorz62_bglt
		BGl_z62lambda2003z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static BgL_z62iozd2closedzd2errorz62_bglt
		BGl_z62lambda2005z62zz__objectz00(obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_z52isa32zd2objectzf2cdepthzf3z81zz__objectz00(BgL_objectz00_bglt, obj_t,
		long);
	BGL_EXPORTED_DECL obj_t
		BGl_objectzd2wideningzd2setz12z12zz__objectz00(BgL_objectz00_bglt, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_z62securityzd2exceptionzb0zz__objectz00 = BUNSPEC;
	static obj_t BGl_z62findzd2classzd2fieldz62zz__objectz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_z62iozd2unknownzd2hostzd2errorzb0zz__objectz00 =
		BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31997ze3ze5zz__objectz00(obj_t, obj_t);
	static obj_t BGl_z62classzd2indexzb0zz__objectz00(obj_t, obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62classzd2hashzb0zz__objectz00(obj_t, obj_t);
	static BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt
		BGl_z62lambda2013z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32073ze3ze5zz__objectz00(obj_t, obj_t);
	static BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt
		BGl_z62lambda2015z62zz__objectz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_callzd2virtualzd2setterz00zz__objectz00(BgL_objectz00_bglt, int, obj_t);
	static obj_t BGl_z62registerzd2genericz12za2zz__objectz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_z62iozd2closedzd2errorz62zz__objectz00 = BUNSPEC;
	static obj_t BGl_z62genericzd2addzd2evalzd2methodz12za2zz__objectz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_classzd2fieldzd2infoz00zz__objectz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_nilzf3zf3zz__objectz00(BgL_objectz00_bglt);
	static BgL_z62stackzd2overflowzd2errorz62_bglt
		BGl_z62lambda2100z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62classzd2fieldzf3z43zz__objectz00(obj_t, obj_t);
	static BgL_z62stackzd2overflowzd2errorz62_bglt
		BGl_z62lambda2102z62zz__objectz00(obj_t);
	static BgL_z62iozd2parsezd2errorz62_bglt
		BGl_z62lambda2023z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static BgL_z62iozd2parsezd2errorz62_bglt
		BGl_z62lambda2025z62zz__objectz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_z62evalzd2warningzb0zz__objectz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza32147ze3ze5zz__objectz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32139ze3ze5zz__objectz00(obj_t);
	static obj_t BGl_z62classzd2creatorzb0zz__objectz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_classzd2constructorzd2zz__objectz00(obj_t);
	static BgL_z62securityzd2exceptionzb0_bglt
		BGl_z62lambda2110z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_z62securityzd2exceptionzb0_bglt
		BGl_z62lambda2112z62zz__objectz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32172ze3ze5zz__objectz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32083ze3ze5zz__objectz00(obj_t, obj_t);
	static BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt
		BGl_z62lambda2035z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt
		BGl_z62lambda2037z62zz__objectz00(obj_t);
	static obj_t BGl_z62lambda2119z62zz__objectz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_exceptionzd2notifyzd2zz__objectz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_classzd2fieldzd2typez00zz__objectz00(obj_t);
	static obj_t BGl_z62procedurezd2ze3genericz53zz__objectz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_z62iozd2malformedzd2urlzd2errorzb0zz__objectz00 =
		BUNSPEC;
	static obj_t BGl_z62callzd2nextzd2virtualzd2getterzb0zz__objectz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62classzd2fieldzd2namez62zz__objectz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_z52objectzd2wideningz80zz__objectz00(BgL_objectz00_bglt);
	static obj_t BGl_z62classzd2fieldszb0zz__objectz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_objectz00_bglt
		BGl_allocatezd2instancezd2zz__objectz00(obj_t);
	static obj_t BGl_methodzd2arrayzd2setz12z12zz__objectz00(obj_t, obj_t, long,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_z62iozd2filezd2notzd2foundzd2errorz62zz__objectz00
		= BUNSPEC;
	static obj_t BGl_z62lambda2120z62zz__objectz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_errorzd2notifyzd2zz__errorz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_classzd2evdatazd2setz12z12zz__objectz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32157ze3ze5zz__objectz00(obj_t, obj_t);
	static BgL_z62accesszd2controlzd2exceptionz62_bglt
		BGl_z62lambda2127z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt
		BGl_z62lambda2046z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt
		BGl_z62lambda2048z62zz__objectz00(obj_t);
	static obj_t BGl_z62z52isa32zd2objectzf2cdepthzf3ze3zz__objectz00(obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long,
		char *);
	static obj_t BGl_symbol3002z00zz__objectz00 = BUNSPEC;
	static obj_t BGl_symbol3009z00zz__objectz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_callzd2virtualzd2getterz00zz__objectz00(BgL_objectz00_bglt, int);
	static obj_t BGl_z62genericzd2addzd2methodz12z70zz__objectz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static bool_t BGl_genericszd2addzd2classz12z12zz__objectz00(long, long);
	static obj_t BGl_z62exceptionzd2notifyzd2z62er1412z00zz__objectz00(obj_t,
		obj_t);
	static BgL_z62accesszd2controlzd2exceptionz62_bglt
		BGl_z62lambda2130z62zz__objectz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_z62errorz62zz__objectz00 = BUNSPEC;
	static bool_t BGl_doublezd2nbzd2classesz12z12zz__objectz00(void);
	static obj_t BGl_z62lambda2137z62zz__objectz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_classzf3zf3zz__objectz00(obj_t);
	static obj_t BGl_z62classzd2evfieldszd2setz12z70zz__objectz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda2138z62zz__objectz00(obj_t, obj_t, obj_t);
	static BgL_z62iozd2sigpipezd2errorz62_bglt
		BGl_z62lambda2058z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_z52isazf2finalzf3z53zz__objectz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_z62warningz62zz__objectz00 = BUNSPEC;
	static obj_t BGl_symbol3013z00zz__objectz00 = BUNSPEC;
	static obj_t BGl_symbol3018z00zz__objectz00 = BUNSPEC;
	static obj_t BGl_z62z52isazd2objectzf2finalzf3ze3zz__objectz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_currentzd2threadzd2zz__threadz00(void);
	static obj_t BGl_z62classzd2existszb0zz__objectz00(obj_t, obj_t);
	static obj_t BGl_z62objectzd2hashnumber1396zb0zz__objectz00(obj_t, obj_t);
	static BgL_z62iozd2sigpipezd2errorz62_bglt
		BGl_z62lambda2060z62zz__objectz00(obj_t);
	static obj_t BGl_za2inheritancezd2cntza2zd2zz__objectz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza32094ze3ze5zz__objectz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2145z62zz__objectz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_z62stackzd2overflowzd2errorz62zz__objectz00 =
		BUNSPEC;
	static obj_t BGl_z62lambda2146z62zz__objectz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_vectorzd2appendzd2zz__r4_vectors_6_8z00(obj_t, obj_t);
	static BgL_z62iozd2timeoutzd2errorz62_bglt
		BGl_z62lambda2069z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol3102z00zz__objectz00 = BUNSPEC;
	static obj_t BGl_symbol3022z00zz__objectz00 = BUNSPEC;
	static obj_t BGl_symbol3104z00zz__objectz00 = BUNSPEC;
	static obj_t BGl_symbol3027z00zz__objectz00 = BUNSPEC;
	static obj_t BGl_symbol3109z00zz__objectz00 = BUNSPEC;
	static obj_t BGl_za2genericsza2z00zz__objectz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_z62iozd2portzd2errorz62zz__objectz00 = BUNSPEC;
	static obj_t BGl_z62classzd2fieldzd2defaultzd2valuezb0zz__objectz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL int
		BGl_bigloozd2genericzd2bucketzd2maskzd2zz__objectz00(void);
	extern obj_t bgl_make_generic(obj_t);
	static BgL_z62iozd2timeoutzd2errorz62_bglt
		BGl_z62lambda2071z62zz__objectz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_z62iozd2connectionzd2errorz62zz__objectz00 =
		BUNSPEC;
	static BgL_z62warningz62_bglt BGl_z62lambda2153z62zz__objectz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static BgL_z62warningz62_bglt BGl_z62lambda2155z62zz__objectz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_objectzf3zf3zz__objectz00(obj_t);
	static obj_t BGl_cnstzd2initzd2zz__objectz00(void);
	static BgL_z62iozd2connectionzd2errorz62_bglt
		BGl_z62lambda2079z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z52addzd2methodz12z92zz__objectz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol3032z00zz__objectz00 = BUNSPEC;
	extern obj_t BGl_getzd2tracezd2stackz00zz__errorz00(obj_t);
	static obj_t BGl_symbol3117z00zz__objectz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_z62accesszd2controlzd2exceptionz62zz__objectz00 =
		BUNSPEC;
	static obj_t BGl_symbol3037z00zz__objectz00 = BUNSPEC;
	static obj_t BGl_z62classzd2fieldzd2infoz62zz__objectz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_classzd2modulezd2zz__objectz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_z62iozd2timeoutzd2errorz62zz__objectz00 = BUNSPEC;
	static obj_t BGl_z62classzd2nilzb0zz__objectz00(obj_t, obj_t);
	static obj_t BGl_gczd2rootszd2initz00zz__objectz00(void);
	static obj_t BGl_z62registerzd2classz12za2zz__objectz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62classzd2widezf3z43zz__objectz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2161z62zz__objectz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2162z62zz__objectz00(obj_t, obj_t, obj_t);
	static BgL_z62iozd2connectionzd2errorz62_bglt
		BGl_z62lambda2081z62zz__objectz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_z62iozd2errorzb0zz__objectz00 = BUNSPEC;
	static BgL_z62evalzd2warningzb0_bglt BGl_z62lambda2168z62zz__objectz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_symbol3122z00zz__objectz00 = BUNSPEC;
	static obj_t BGl_symbol3042z00zz__objectz00 = BUNSPEC;
	static obj_t BGl_symbol3126z00zz__objectz00 = BUNSPEC;
	static obj_t BGl_symbol3047z00zz__objectz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_findzd2classzd2zz__objectz00(obj_t);
	static obj_t BGl_z62bigloozd2genericzd2bucketzd2maskzb0zz__objectz00(obj_t);
	static obj_t BGl_z62classzd2virtualzb0zz__objectz00(obj_t, obj_t);
	static BgL_z62evalzd2warningzb0_bglt BGl_z62lambda2170z62zz__objectz00(obj_t);
	static obj_t BGl_z62z52isazd2objectzf2cdepthzf3ze3zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_z62processzd2exceptionzb0_bglt
		BGl_z62lambda2090z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t
		BGl_z62indexzd2outzd2ofzd2boundszd2errorz62zz__objectz00 = BUNSPEC;
	static BgL_z62processzd2exceptionzb0_bglt
		BGl_z62lambda2092z62zz__objectz00(obj_t);
	BGL_EXPORTED_DECL long
		BGl_objectzd2classzd2numz00zz__objectz00(BgL_objectz00_bglt);
	BGL_EXPORTED_DEF obj_t BGl_objectz00zz__objectz00 = BUNSPEC;
	static obj_t BGl_z62objectzd2printzb0zz__objectz00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_objectzd2classzd2zz__objectz00(BgL_objectz00_bglt);
	static obj_t BGl_z62exceptionzd2notifyzb0zz__objectz00(obj_t, obj_t);
	static obj_t BGl_z62classzd2fieldzd2typez62zz__objectz00(obj_t, obj_t);
	extern obj_t BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_symbol3131z00zz__objectz00 = BUNSPEC;
	static obj_t BGl_z62classzd2evdatazb0zz__objectz00(obj_t, obj_t);
	static obj_t BGl_symbol3052z00zz__objectz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_callzd2nextzd2virtualzd2setterzd2zz__objectz00(obj_t,
		BgL_objectz00_bglt, int, obj_t);
	static obj_t BGl_symbol3136z00zz__objectz00 = BUNSPEC;
	static obj_t BGl_z62z52objectzd2wideningze2zz__objectz00(obj_t, obj_t);
	static obj_t BGl_symbol3057z00zz__objectz00 = BUNSPEC;
	extern obj_t BGl_writezd2circlezd2zz__pp_circlez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_findzd2methodzd2zz__objectz00(BgL_objectz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_z52objectzd2wideningzd2setz12z40zz__objectz00(BgL_objectz00_bglt,
		obj_t);
	static obj_t BGl_z62bigloozd2typeszd2numberz62zz__objectz00(obj_t);
	static obj_t BGl_symbol3062z00zz__objectz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_classzd2namezd2zz__objectz00(obj_t);
	static obj_t BGl_symbol3067z00zz__objectz00 = BUNSPEC;
	static obj_t BGl_z62findzd2classzb0zz__objectz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_z62conditionz62zz__objectz00 = BUNSPEC;
	static obj_t BGl_z62objectzd2writezb0zz__objectz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_genericzd2addzd2evalzd2methodz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62genericzd2nozd2defaultzd2behaviorzb0zz__objectz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_classzd2allzd2fieldsz00zz__objectz00(obj_t);
	static obj_t BGl_symbol3072z00zz__objectz00 = BUNSPEC;
	static obj_t BGl_z62objectzd2write1392zb0zz__objectz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_classzd2nilzd2zz__objectz00(obj_t);
	static obj_t BGl_symbol3077z00zz__objectz00 = BUNSPEC;
	extern long bgl_obj_hash_number(obj_t);
	extern obj_t BGl_warningzd2notifyzd2zz__errorz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_findzd2classzd2byzd2hashzd2zz__objectz00(int);
	extern obj_t BGl_bigloozd2typezd2errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_symbol3082z00zz__objectz00 = BUNSPEC;
	static obj_t BGl_symbol3087z00zz__objectz00 = BUNSPEC;
	static obj_t BGl_z62classzd2namezb0zz__objectz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_widezd2objectzf3z21zz__objectz00(BgL_objectz00_bglt);
	static obj_t BGl_z62z52isazf2cdepthzf3z31zz__objectz00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_classzd2evfieldszd2setz12z12zz__objectz00(obj_t,
		obj_t);
	static obj_t BGl_z62exceptionzd2notifyzd2z62io1414z00zz__objectz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_objectzd2classzd2numzd2setz12zc0zz__objectz00(BgL_objectz00_bglt, long);
	BGL_EXPORTED_DECL obj_t
		BGl_callzd2nextzd2virtualzd2getterzd2zz__objectz00(obj_t,
		BgL_objectz00_bglt, int);
	BGL_EXPORTED_DECL obj_t BGl_classzd2subclasseszd2zz__objectz00(obj_t);
	static obj_t BGl_z62z52isazf2finalzf3z31zz__objectz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62objectzd2equalzf3z43zz__objectz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol3092z00zz__objectz00 = BUNSPEC;
	static bool_t BGl_loopze70ze7zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol3097z00zz__objectz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31850ze3ze5zz__objectz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_classzd2creatorzd2zz__objectz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31907ze3ze5zz__objectz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_z52isa64zd2objectzf2cdepthzf3z81zz__objectz00(BgL_objectz00_bglt, obj_t,
		long);
	static obj_t BGl_z62z52objectzd2wideningzd2setz12z22zz__objectz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_evalzd2classzf3z21zz__objectz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_findzd2superzd2classzd2methodzd2zz__objectz00(BgL_objectz00_bglt, obj_t,
		obj_t);
	static obj_t BGl_z62classzd2constructorzb0zz__objectz00(obj_t, obj_t);
	static BgL_z62errorz62_bglt BGl_z62lambda1902z62zz__objectz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_za2nbzd2genericszd2maxza2z00zz__objectz00 = BUNSPEC;
	static BgL_z62errorz62_bglt BGl_z62lambda1904z62zz__objectz00(obj_t);
	static obj_t BGl_makezd2methodzd2arrayz00zz__objectz00(obj_t);
	static obj_t BGl_z62classzf3z91zz__objectz00(obj_t, obj_t);
	static obj_t BGl_z62objectzd2classzd2numz62zz__objectz00(obj_t, obj_t);
	static obj_t BGl_za2nbzd2classesza2zd2zz__objectz00 = BUNSPEC;
	static BgL_objectz00_bglt BGl_z62allocatezd2instancezb0zz__objectz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_genericzd2defaultzd2zz__objectz00(obj_t);
	static obj_t BGl_z62genericzd2defaultzb0zz__objectz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1913z62zz__objectz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1914z62zz__objectz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62classzd2evdatazd2setz12z70zz__objectz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL int
		BGl_bigloozd2genericzd2bucketzd2siza7ez75zz__objectz00(void);
	static obj_t BGl_z62bigloozd2genericzd2bucketzd2powzb0zz__objectz00(obj_t);
	static obj_t BGl_z62lambda1919z62zz__objectz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_z62iozd2readzd2errorz62zz__objectz00 = BUNSPEC;
	BGL_EXPORTED_DECL bool_t
		BGl_z52isazd2objectzf2cdepthzf3z81zz__objectz00(BgL_objectz00_bglt, obj_t,
		long);
	BGL_EXPORTED_DECL obj_t BGl_procedurezd2ze3genericz31zz__objectz00(obj_t);
	BGL_EXPORTED_DECL long
		BGl_objectzd2hashnumberzd2zz__objectz00(BgL_objectz00_bglt);
	static obj_t BGl_z62lambda1920z62zz__objectz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31861ze3ze5zz__objectz00(obj_t, obj_t);
	static obj_t BGl_z62objectzd2classzd2numzd2setz12za2zz__objectz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62lambda1926z62zz__objectz00(obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string3010z00zz__objectz00,
		BgL_bgl_string3010za700za7za7_3166za7, "&error", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3004z00zz__objectz00,
		BgL_bgl_za762lambda1927za7623167z00, BGl_z62lambda1927z62zz__objectz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3005z00zz__objectz00,
		BgL_bgl_za762lambda1926za7623168z00, BGl_z62lambda1926z62zz__objectz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3006z00zz__objectz00,
		BgL_bgl_za762za7c3za704anonymo3169za7,
		BGl_z62zc3z04anonymousza31907ze3ze5zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3007z00zz__objectz00,
		BgL_bgl_za762lambda1904za7623170z00, BGl_z62lambda1904z62zz__objectz00, 0L,
		BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_classzd2nilzd2initz12zd2envzc0zz__objectz00,
		BgL_bgl_za762classza7d2nilza7d3171za7,
		BGl_z62classzd2nilzd2initz12z70zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3014z00zz__objectz00,
		BgL_bgl_string3014za700za7za7_3172za7, "type", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3008z00zz__objectz00,
		BgL_bgl_za762lambda1902za7623173z00, BGl_z62lambda1902z62zz__objectz00, 0L,
		BUNSPEC, 6);
	      DEFINE_STRING(BGl_string3019z00zz__objectz00,
		BgL_bgl_string3019za700za7za7_3174za7, "&type-error", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3011z00zz__objectz00,
		BgL_bgl_za762lambda1942za7623175z00, BGl_z62lambda1942z62zz__objectz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3012z00zz__objectz00,
		BgL_bgl_za762lambda1941za7623176z00, BGl_z62lambda1941z62zz__objectz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3015z00zz__objectz00,
		BgL_bgl_za762za7c3za704anonymo3177za7,
		BGl_z62zc3z04anonymousza31937ze3ze5zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3103z00zz__objectz00,
		BgL_bgl_string3103za700za7za7_3178za7, "message", 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3016z00zz__objectz00,
		BgL_bgl_za762lambda1935za7623179z00, BGl_z62lambda1935z62zz__objectz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STRING(BGl_string3023z00zz__objectz00,
		BgL_bgl_string3023za700za7za7_3180za7, "index", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3017z00zz__objectz00,
		BgL_bgl_za762lambda1933za7623181z00, BGl_z62lambda1933z62zz__objectz00, 0L,
		BUNSPEC, 7);
	      DEFINE_STRING(BGl_string3105z00zz__objectz00,
		BgL_bgl_string3105za700za7za7_3182za7, "bstring", 7);
	      DEFINE_STRING(BGl_string3028z00zz__objectz00,
		BgL_bgl_string3028za700za7za7_3183za7, "&index-out-of-bounds-error", 26);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2genericzd2bucketzd2siza7ezd2envza7zz__objectz00,
		BgL_bgl_za762biglooza7d2gene3184z00,
		BGl_z62bigloozd2genericzd2bucketzd2siza7ez17zz__objectz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3100z00zz__objectz00,
		BgL_bgl_za762lambda2120za7623185z00, BGl_z62lambda2120z62zz__objectz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_genericzd2addzd2methodz12zd2envzc0zz__objectz00,
		BgL_bgl_za762genericza7d2add3186z00,
		BGl_z62genericzd2addzd2methodz12z70zz__objectz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3101z00zz__objectz00,
		BgL_bgl_za762lambda2119za7623187z00, BGl_z62lambda2119z62zz__objectz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3020z00zz__objectz00,
		BgL_bgl_za762lambda1957za7623188z00, BGl_z62lambda1957z62zz__objectz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3021z00zz__objectz00,
		BgL_bgl_za762lambda1956za7623189z00, BGl_z62lambda1956z62zz__objectz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3110z00zz__objectz00,
		BgL_bgl_string3110za700za7za7_3190za7, "&security-exception", 19);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3024z00zz__objectz00,
		BgL_bgl_za762za7c3za704anonymo3191za7,
		BGl_z62zc3z04anonymousza31952ze3ze5zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3106z00zz__objectz00,
		BgL_bgl_za762za7c3za704anonymo3192za7,
		BGl_z62zc3z04anonymousza32114ze3ze5zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3025z00zz__objectz00,
		BgL_bgl_za762lambda1950za7623193z00, BGl_z62lambda1950z62zz__objectz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3107z00zz__objectz00,
		BgL_bgl_za762lambda2112za7623194z00, BGl_z62lambda2112z62zz__objectz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3026z00zz__objectz00,
		BgL_bgl_za762lambda1948za7623195z00, BGl_z62lambda1948z62zz__objectz00, 0L,
		BUNSPEC, 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3108z00zz__objectz00,
		BgL_bgl_za762lambda2110za7623196z00, BGl_z62lambda2110z62zz__objectz00, 0L,
		BUNSPEC, 4);
	      DEFINE_STRING(BGl_string3033z00zz__objectz00,
		BgL_bgl_string3033za700za7za7_3197za7, "&io-error", 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3029z00zz__objectz00,
		BgL_bgl_za762za7c3za704anonymo3198za7,
		BGl_z62zc3z04anonymousza31967ze3ze5zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3118z00zz__objectz00,
		BgL_bgl_string3118za700za7za7_3199za7, "permission", 10);
	      DEFINE_STRING(BGl_string3038z00zz__objectz00,
		BgL_bgl_string3038za700za7za7_3200za7, "&io-port-error", 14);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_registerzd2genericz12zd2envz12zz__objectz00,
		BgL_bgl_za762registerza7d2ge3201z00,
		BGl_z62registerzd2genericz12za2zz__objectz00, 0L, BUNSPEC, 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_procedurezd2ze3genericzd2envze3zz__objectz00,
		BgL_bgl_za762procedureza7d2za73202za7,
		BGl_z62procedurezd2ze3genericz53zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_objectzd2hashnumberzd2envz00zz__objectz00,
		BgL_bgl_za762objectza7d2hash3203z00,
		BGl_z62objectzd2hashnumberzb0zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3111z00zz__objectz00,
		BgL_bgl_za762za7c3za704anonymo3204za7,
		BGl_z62zc3z04anonymousza32139ze3ze5zz__objectz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3030z00zz__objectz00,
		BgL_bgl_za762lambda1965za7623205z00, BGl_z62lambda1965z62zz__objectz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3112z00zz__objectz00,
		BgL_bgl_za762lambda2138za7623206z00, BGl_z62lambda2138z62zz__objectz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3031z00zz__objectz00,
		BgL_bgl_za762lambda1963za7623207z00, BGl_z62lambda1963z62zz__objectz00, 0L,
		BUNSPEC, 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3113z00zz__objectz00,
		BgL_bgl_za762lambda2137za7623208z00, BGl_z62lambda2137z62zz__objectz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3114z00zz__objectz00,
		BgL_bgl_za762za7c3za704anonymo3209za7,
		BGl_z62zc3z04anonymousza32147ze3ze5zz__objectz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3115z00zz__objectz00,
		BgL_bgl_za762lambda2146za7623210z00, BGl_z62lambda2146z62zz__objectz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3034z00zz__objectz00,
		BgL_bgl_za762za7c3za704anonymo3211za7,
		BGl_z62zc3z04anonymousza31977ze3ze5zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3116z00zz__objectz00,
		BgL_bgl_za762lambda2145za7623212z00, BGl_z62lambda2145z62zz__objectz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3035z00zz__objectz00,
		BgL_bgl_za762lambda1975za7623213z00, BGl_z62lambda1975z62zz__objectz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STRING(BGl_string3123z00zz__objectz00,
		BgL_bgl_string3123za700za7za7_3214za7, "&access-control-exception", 25);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3036z00zz__objectz00,
		BgL_bgl_za762lambda1973za7623215z00, BGl_z62lambda1973z62zz__objectz00, 0L,
		BUNSPEC, 6);
	      DEFINE_STRING(BGl_string3043z00zz__objectz00,
		BgL_bgl_string3043za700za7za7_3216za7, "&io-read-error", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3119z00zz__objectz00,
		BgL_bgl_za762za7c3za704anonymo3217za7,
		BGl_z62zc3z04anonymousza32132ze3ze5zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3039z00zz__objectz00,
		BgL_bgl_za762za7c3za704anonymo3218za7,
		BGl_z62zc3z04anonymousza31987ze3ze5zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3127z00zz__objectz00,
		BgL_bgl_string3127za700za7za7_3219za7, "args", 4);
	      DEFINE_STRING(BGl_string3048z00zz__objectz00,
		BgL_bgl_string3048za700za7za7_3220za7, "&io-write-error", 15);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_findzd2methodzd2fromzd2envzd2zz__objectz00,
		BgL_bgl_za762findza7d2method3221z00,
		BGl_z62findzd2methodzd2fromz62zz__objectz00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_classzd2superzd2envz00zz__objectz00,
		BgL_bgl_za762classza7d2super3222z00, BGl_z62classzd2superzb0zz__objectz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_objectzd2wideningzd2envz00zz__objectz00,
		BgL_bgl_za762objectza7d2wide3223z00,
		BGl_z62objectzd2wideningzb0zz__objectz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classzd2fieldzd2defaultzd2valuezd2envz00zz__objectz00,
		BgL_bgl_za762classza7d2field3224z00,
		BGl_z62classzd2fieldzd2defaultzd2valuezb0zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3120z00zz__objectz00,
		BgL_bgl_za762lambda2130za7623225z00, BGl_z62lambda2130z62zz__objectz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3121z00zz__objectz00,
		BgL_bgl_za762lambda2127za7623226z00, BGl_z62lambda2127z62zz__objectz00, 0L,
		BUNSPEC, 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3040z00zz__objectz00,
		BgL_bgl_za762lambda1985za7623227z00, BGl_z62lambda1985z62zz__objectz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3041z00zz__objectz00,
		BgL_bgl_za762lambda1983za7623228z00, BGl_z62lambda1983z62zz__objectz00, 0L,
		BUNSPEC, 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3124z00zz__objectz00,
		BgL_bgl_za762lambda2162za7623229z00, BGl_z62lambda2162z62zz__objectz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3125z00zz__objectz00,
		BgL_bgl_za762lambda2161za7623230z00, BGl_z62lambda2161z62zz__objectz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3044z00zz__objectz00,
		BgL_bgl_za762za7c3za704anonymo3231za7,
		BGl_z62zc3z04anonymousza31997ze3ze5zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3132z00zz__objectz00,
		BgL_bgl_string3132za700za7za7_3232za7, "&warning", 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3045z00zz__objectz00,
		BgL_bgl_za762lambda1995za7623233z00, BGl_z62lambda1995z62zz__objectz00, 0L,
		BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_evalzd2classzf3zd2envzf3zz__objectz00,
		BgL_bgl_za762evalza7d2classza73234za7,
		BGl_z62evalzd2classzf3z43zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3046z00zz__objectz00,
		BgL_bgl_za762lambda1993za7623235z00, BGl_z62lambda1993z62zz__objectz00, 0L,
		BUNSPEC, 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3128z00zz__objectz00,
		BgL_bgl_za762za7c3za704anonymo3236za7,
		BGl_z62zc3z04anonymousza32157ze3ze5zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3053z00zz__objectz00,
		BgL_bgl_string3053za700za7za7_3237za7, "&io-closed-error", 16);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3129z00zz__objectz00,
		BgL_bgl_za762lambda2155za7623238z00, BGl_z62lambda2155z62zz__objectz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3049z00zz__objectz00,
		BgL_bgl_za762za7c3za704anonymo3239za7,
		BGl_z62zc3z04anonymousza32007ze3ze5zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3137z00zz__objectz00,
		BgL_bgl_string3137za700za7za7_3240za7, "&eval-warning", 13);
	      DEFINE_STRING(BGl_string3139z00zz__objectz00,
		BgL_bgl_string3139za700za7za7_3241za7, "object-display1389", 18);
	      DEFINE_STRING(BGl_string3058z00zz__objectz00,
		BgL_bgl_string3058za700za7za7_3242za7, "&io-file-not-found-error", 24);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_exceptionzd2notifyzd2envz00zz__objectz00,
		BgL_bgl_za762exceptionza7d2n3243z00,
		BGl_z62exceptionzd2notifyzb0zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3130z00zz__objectz00,
		BgL_bgl_za762lambda2153za7623244z00, BGl_z62lambda2153z62zz__objectz00, 0L,
		BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3050z00zz__objectz00,
		BgL_bgl_za762lambda2005za7623245z00, BGl_z62lambda2005z62zz__objectz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3051z00zz__objectz00,
		BgL_bgl_za762lambda2003za7623246z00, BGl_z62lambda2003z62zz__objectz00, 0L,
		BUNSPEC, 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3133z00zz__objectz00,
		BgL_bgl_za762za7c3za704anonymo3247za7,
		BGl_z62zc3z04anonymousza32172ze3ze5zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3134z00zz__objectz00,
		BgL_bgl_za762lambda2170za7623248z00, BGl_z62lambda2170z62zz__objectz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STRING(BGl_string3141z00zz__objectz00,
		BgL_bgl_string3141za700za7za7_3249za7, "object-write1392", 16);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3135z00zz__objectz00,
		BgL_bgl_za762lambda2168za7623250z00, BGl_z62lambda2168z62zz__objectz00, 0L,
		BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3054z00zz__objectz00,
		BgL_bgl_za762za7c3za704anonymo3251za7,
		BGl_z62zc3z04anonymousza32017ze3ze5zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3055z00zz__objectz00,
		BgL_bgl_za762lambda2015za7623252z00, BGl_z62lambda2015z62zz__objectz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STRING(BGl_string3143z00zz__objectz00,
		BgL_bgl_string3143za700za7za7_3253za7, "object-hashnumber1396", 21);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3056z00zz__objectz00,
		BgL_bgl_za762lambda2013za7623254z00, BGl_z62lambda2013z62zz__objectz00, 0L,
		BUNSPEC, 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3138z00zz__objectz00,
		BgL_bgl_za762objectza7d2disp3255z00, va_generic_entry,
		BGl_z62objectzd2display1389zb0zz__objectz00, BUNSPEC, -2);
	      DEFINE_STRING(BGl_string3063z00zz__objectz00,
		BgL_bgl_string3063za700za7za7_3256za7, "&io-parse-error", 15);
	      DEFINE_STRING(BGl_string3145z00zz__objectz00,
		BgL_bgl_string3145za700za7za7_3257za7, "object-print1399", 16);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3059z00zz__objectz00,
		BgL_bgl_za762za7c3za704anonymo3258za7,
		BGl_z62zc3z04anonymousza32027ze3ze5zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3147z00zz__objectz00,
		BgL_bgl_string3147za700za7za7_3259za7, "object-equal?1402", 17);
	      DEFINE_STRING(BGl_string3149z00zz__objectz00,
		BgL_bgl_string3149za700za7za7_3260za7, "exception-notify1404", 20);
	      DEFINE_STRING(BGl_string3068z00zz__objectz00,
		BgL_bgl_string3068za700za7za7_3261za7, "&io-unknown-host-error", 22);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_findzd2classzd2envz00zz__objectz00,
		BgL_bgl_za762findza7d2classza73262za7, BGl_z62findzd2classzb0zz__objectz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3140z00zz__objectz00,
		BgL_bgl_za762objectza7d2writ3263z00, va_generic_entry,
		BGl_z62objectzd2write1392zb0zz__objectz00, BUNSPEC, -2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3060z00zz__objectz00,
		BgL_bgl_za762lambda2025za7623264z00, BGl_z62lambda2025z62zz__objectz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3142z00zz__objectz00,
		BgL_bgl_za762objectza7d2hash3265z00,
		BGl_z62objectzd2hashnumber1396zb0zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3061z00zz__objectz00,
		BgL_bgl_za762lambda2023za7623266z00, BGl_z62lambda2023z62zz__objectz00, 0L,
		BUNSPEC, 6);
	      DEFINE_STRING(BGl_string3150z00zz__objectz00,
		BgL_bgl_string3150za700za7za7_3267za7, "*** UNKNOWN EXCEPTION: ", 23);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3144z00zz__objectz00,
		BgL_bgl_za762objectza7d2prin3268z00,
		BGl_z62objectzd2print1399zb0zz__objectz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string3151z00zz__objectz00,
		BgL_bgl_string3151za700za7za7_3269za7, " [[", 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3064z00zz__objectz00,
		BgL_bgl_za762za7c3za704anonymo3270za7,
		BGl_z62zc3z04anonymousza32039ze3ze5zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3152z00zz__objectz00,
		BgL_bgl_string3152za700za7za7_3271za7, "]]", 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3146z00zz__objectz00,
		BgL_bgl_za762objectza7d2equa3272z00,
		BGl_z62objectzd2equalzf31402z43zz__objectz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3065z00zz__objectz00,
		BgL_bgl_za762lambda2037za7623273z00, BGl_z62lambda2037z62zz__objectz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STRING(BGl_string3153z00zz__objectz00,
		BgL_bgl_string3153za700za7za7_3274za7, " [", 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3066z00zz__objectz00,
		BgL_bgl_za762lambda2035za7623275z00, BGl_z62lambda2035z62zz__objectz00, 0L,
		BUNSPEC, 6);
	      DEFINE_STRING(BGl_string3154z00zz__objectz00,
		BgL_bgl_string3154za700za7za7_3276za7, "#|", 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3148z00zz__objectz00,
		BgL_bgl_za762exceptionza7d2n3277z00,
		BGl_z62exceptionzd2notify1404zb0zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3073z00zz__objectz00,
		BgL_bgl_string3073za700za7za7_3278za7, "&io-malformed-url-error", 23);
	      DEFINE_STRING(BGl_string3155z00zz__objectz00,
		BgL_bgl_string3155za700za7za7_3279za7, " nil|", 5);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_objectzd2writezd2envz00zz__objectz00,
		BgL_bgl_za762objectza7d2writ3280z00, va_generic_entry,
		BGl_z62objectzd2writezb0zz__objectz00, BUNSPEC, -2);
	      DEFINE_STRING(BGl_string3156z00zz__objectz00,
		BgL_bgl_string3156za700za7za7_3281za7, "&object-display", 15);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3069z00zz__objectz00,
		BgL_bgl_za762za7c3za704anonymo3282za7,
		BGl_z62zc3z04anonymousza32050ze3ze5zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3157z00zz__objectz00,
		BgL_bgl_string3157za700za7za7_3283za7, "&object-write", 13);
	      DEFINE_STRING(BGl_string3158z00zz__objectz00,
		BgL_bgl_string3158za700za7za7_3284za7, "&object-hashnumber", 18);
	      DEFINE_STRING(BGl_string3159z00zz__objectz00,
		BgL_bgl_string3159za700za7za7_3285za7, "&object-print", 13);
	      DEFINE_STRING(BGl_string3078z00zz__objectz00,
		BgL_bgl_string3078za700za7za7_3286za7, "&io-sigpipe-error", 17);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_classzf3zd2envz21zz__objectz00,
		BgL_bgl_za762classza7f3za791za7za73287za7, BGl_z62classzf3z91zz__objectz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_classzd2subclasseszd2envz00zz__objectz00,
		BgL_bgl_za762classza7d2subcl3288z00,
		BGl_z62classzd2subclasseszb0zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3070z00zz__objectz00,
		BgL_bgl_za762lambda2048za7623289z00, BGl_z62lambda2048z62zz__objectz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3071z00zz__objectz00,
		BgL_bgl_za762lambda2046za7623290z00, BGl_z62lambda2046z62zz__objectz00, 0L,
		BUNSPEC, 6);
	      DEFINE_STRING(BGl_string3160z00zz__objectz00,
		BgL_bgl_string3160za700za7za7_3291za7, "output-port", 11);
	      DEFINE_STRING(BGl_string3161z00zz__objectz00,
		BgL_bgl_string3161za700za7za7_3292za7, "&object-equal?", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3074z00zz__objectz00,
		BgL_bgl_za762za7c3za704anonymo3293za7,
		BGl_z62zc3z04anonymousza32062ze3ze5zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3075z00zz__objectz00,
		BgL_bgl_za762lambda2060za7623294z00, BGl_z62lambda2060z62zz__objectz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STRING(BGl_string3163z00zz__objectz00,
		BgL_bgl_string3163za700za7za7_3295za7, "exception-notify", 16);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3076z00zz__objectz00,
		BgL_bgl_za762lambda2058za7623296z00, BGl_z62lambda2058z62zz__objectz00, 0L,
		BUNSPEC, 6);
	      DEFINE_STRING(BGl_string3083z00zz__objectz00,
		BgL_bgl_string3083za700za7za7_3297za7, "&io-timeout-error", 17);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3079z00zz__objectz00,
		BgL_bgl_za762za7c3za704anonymo3298za7,
		BGl_z62zc3z04anonymousza32073ze3ze5zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_classzd2creatorzd2envz00zz__objectz00,
		BgL_bgl_za762classza7d2creat3299z00, BGl_z62classzd2creatorzb0zz__objectz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3088z00zz__objectz00,
		BgL_bgl_string3088za700za7za7_3300za7, "&io-connection-error", 20);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_genericzd2methodzd2arrayzd2envzd2zz__objectz00,
		BgL_bgl_za762genericza7d2met3301z00,
		BGl_z62genericzd2methodzd2arrayz62zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_classzd2namezd2envz00zz__objectz00,
		BgL_bgl_za762classza7d2nameza73302za7, BGl_z62classzd2namezb0zz__objectz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_classzd2hashzd2envz00zz__objectz00,
		BgL_bgl_za762classza7d2hashza73303za7, BGl_z62classzd2hashzb0zz__objectz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3080z00zz__objectz00,
		BgL_bgl_za762lambda2071za7623304z00, BGl_z62lambda2071z62zz__objectz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3162z00zz__objectz00,
		BgL_bgl_za762exceptionza7d2n3305z00,
		BGl_z62exceptionzd2notifyzd2z62er1412z00zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3081z00zz__objectz00,
		BgL_bgl_za762lambda2069za7623306z00, BGl_z62lambda2069z62zz__objectz00, 0L,
		BUNSPEC, 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3164z00zz__objectz00,
		BgL_bgl_za762exceptionza7d2n3307z00,
		BGl_z62exceptionzd2notifyzd2z62io1414z00zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3165z00zz__objectz00,
		BgL_bgl_za762exceptionza7d2n3308z00,
		BGl_z62exceptionzd2notifyzd2z62wa1416z00zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3084z00zz__objectz00,
		BgL_bgl_za762za7c3za704anonymo3309za7,
		BGl_z62zc3z04anonymousza32083ze3ze5zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3085z00zz__objectz00,
		BgL_bgl_za762lambda2081za7623310z00, BGl_z62lambda2081z62zz__objectz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3086z00zz__objectz00,
		BgL_bgl_za762lambda2079za7623311z00, BGl_z62lambda2079z62zz__objectz00, 0L,
		BUNSPEC, 6);
	      DEFINE_STRING(BGl_string3093z00zz__objectz00,
		BgL_bgl_string3093za700za7za7_3312za7, "&process-exception", 18);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3089z00zz__objectz00,
		BgL_bgl_za762za7c3za704anonymo3313za7,
		BGl_z62zc3z04anonymousza32094ze3ze5zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3098z00zz__objectz00,
		BgL_bgl_string3098za700za7za7_3314za7, "&stack-overflow-error", 21);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_classzd2fieldzd2typezd2envzd2zz__objectz00,
		BgL_bgl_za762classza7d2field3315z00,
		BGl_z62classzd2fieldzd2typez62zz__objectz00, 0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_genericzd2nozd2defaultzd2behaviorzd2envz00zz__objectz00,
		BgL_bgl_za762genericza7d2noza73316za7, va_generic_entry,
		BGl_z62genericzd2nozd2defaultzd2behaviorzb0zz__objectz00, BUNSPEC, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_classzd2evdatazd2envz00zz__objectz00,
		BgL_bgl_za762classza7d2evdat3317z00, BGl_z62classzd2evdatazb0zz__objectz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_z52objectzd2wideningzd2envz52zz__objectz00,
		BgL_bgl_za762za752objectza7d2w3318za7,
		BGl_z62z52objectzd2wideningze2zz__objectz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classzd2fieldzd2mutatorzd2envzd2zz__objectz00,
		BgL_bgl_za762classza7d2field3319z00,
		BGl_z62classzd2fieldzd2mutatorz62zz__objectz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classzd2fieldzd2accessorzd2envzd2zz__objectz00,
		BgL_bgl_za762classza7d2field3320z00,
		BGl_z62classzd2fieldzd2accessorz62zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3090z00zz__objectz00,
		BgL_bgl_za762lambda2092za7623321z00, BGl_z62lambda2092z62zz__objectz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3091z00zz__objectz00,
		BgL_bgl_za762lambda2090za7623322z00, BGl_z62lambda2090z62zz__objectz00, 0L,
		BUNSPEC, 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3094z00zz__objectz00,
		BgL_bgl_za762za7c3za704anonymo3323za7,
		BGl_z62zc3z04anonymousza32104ze3ze5zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3095z00zz__objectz00,
		BgL_bgl_za762lambda2102za7623324z00, BGl_z62lambda2102z62zz__objectz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3096z00zz__objectz00,
		BgL_bgl_za762lambda2100za7623325z00, BGl_z62lambda2100z62zz__objectz00, 0L,
		BUNSPEC, 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3099z00zz__objectz00,
		BgL_bgl_za762za7c3za704anonymo3326za7,
		BGl_z62zc3z04anonymousza32121ze3ze5zz__objectz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classzd2fieldzd2defaultzd2valuezf3zd2envzf3zz__objectz00,
		BgL_bgl_za762classza7d2field3327z00,
		BGl_z62classzd2fieldzd2defaultzd2valuezf3z43zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_classzd2virtualzd2envz00zz__objectz00,
		BgL_bgl_za762classza7d2virtu3328z00, BGl_z62classzd2virtualzb0zz__objectz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_widezd2objectzf3zd2envzf3zz__objectz00,
		BgL_bgl_za762wideza7d2object3329z00,
		BGl_z62widezd2objectzf3z43zz__objectz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_callzd2nextzd2virtualzd2getterzd2envz00zz__objectz00,
		BgL_bgl_za762callza7d2nextza7d3330za7,
		BGl_z62callzd2nextzd2virtualzd2getterzb0zz__objectz00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_classzd2fieldszd2envz00zz__objectz00,
		BgL_bgl_za762classza7d2field3331z00, BGl_z62classzd2fieldszb0zz__objectz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_z52isazf2cdepthzf3zd2envz81zz__objectz00,
		BgL_bgl_za762za752isaza7f2cdep3332za7,
		BGl_z62z52isazf2cdepthzf3z31zz__objectz00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_objectzd2printzd2envz00zz__objectz00,
		BgL_bgl_za762objectza7d2prin3333z00, BGl_z62objectzd2printzb0zz__objectz00,
		0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_objectzf3zd2envz21zz__objectz00,
		BgL_bgl_za762objectza7f3za791za73334z00, BGl_z62objectzf3z91zz__objectz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_objectzd2displayzd2envz00zz__objectz00,
		BgL_bgl_za762objectza7d2disp3335z00, va_generic_entry,
		BGl_z62objectzd2displayzb0zz__objectz00, BUNSPEC, -2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_allocatezd2instancezd2envz00zz__objectz00,
		BgL_bgl_za762allocateza7d2in3336z00,
		BGl_z62allocatezd2instancezb0zz__objectz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2classzd2fieldzd2envzd2zz__objectz00,
		BgL_bgl_za762makeza7d2classza73337za7,
		BGl_z62makezd2classzd2fieldz62zz__objectz00, 0L, BUNSPEC, 8);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_findzd2classzd2byzd2hashzd2envz00zz__objectz00,
		BgL_bgl_za762findza7d2classza73338za7,
		BGl_z62findzd2classzd2byzd2hashzb0zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2900z00zz__objectz00,
		BgL_bgl_string2900za700za7za7_3339za7, "&class-nil-init!", 16);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_z52isazd2objectzf2cdepthzf3zd2envz53zz__objectz00,
		BgL_bgl_za762za752isaza7d2obje3340za7,
		BGl_z62z52isazd2objectzf2cdepthzf3ze3zz__objectz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2901z00zz__objectz00,
		BgL_bgl_string2901za700za7za7_3341za7, "&class-nil", 10);
	      DEFINE_STRING(BGl_string2902z00zz__objectz00,
		BgL_bgl_string2902za700za7za7_3342za7, "&object-class-num", 17);
	      DEFINE_STRING(BGl_string2903z00zz__objectz00,
		BgL_bgl_string2903za700za7za7_3343za7, "object", 6);
	      DEFINE_STRING(BGl_string2904z00zz__objectz00,
		BgL_bgl_string2904za700za7za7_3344za7, "&object-class-num-set!", 22);
	      DEFINE_STRING(BGl_string2905z00zz__objectz00,
		BgL_bgl_string2905za700za7za7_3345za7, "&object-class", 13);
	      DEFINE_STRING(BGl_string2906z00zz__objectz00,
		BgL_bgl_string2906za700za7za7_3346za7, "&generic-default", 16);
	      DEFINE_STRING(BGl_string2907z00zz__objectz00,
		BgL_bgl_string2907za700za7za7_3347za7, "procedure", 9);
	      DEFINE_STRING(BGl_string2908z00zz__objectz00,
		BgL_bgl_string2908za700za7za7_3348za7, "&generic-method-array", 21);
	      DEFINE_STRING(BGl_string2909z00zz__objectz00,
		BgL_bgl_string2909za700za7za7_3349za7, "&method-array-ref", 17);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_classzd2fieldzd2infozd2envzd2zz__objectz00,
		BgL_bgl_za762classza7d2field3350z00,
		BGl_z62classzd2fieldzd2infoz62zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2911z00zz__objectz00,
		BgL_bgl_string2911za700za7za7_3351za7, "generic", 7);
	      DEFINE_STRING(BGl_string2914z00zz__objectz00,
		BgL_bgl_string2914za700za7za7_3352za7, "mtable-size", 11);
	      DEFINE_STRING(BGl_string2916z00zz__objectz00,
		BgL_bgl_string2916za700za7za7_3353za7, "method-array-size", 17);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2typeszd2numberzd2envzd2zz__objectz00,
		BgL_bgl_za762biglooza7d2type3354z00,
		BGl_z62bigloozd2typeszd2numberz62zz__objectz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2918z00zz__objectz00,
		BgL_bgl_string2918za700za7za7_3355za7, "generic-bucket-size", 19);
	      DEFINE_STRING(BGl_string2920z00zz__objectz00,
		BgL_bgl_string2920za700za7za7_3356za7, "max-class", 9);
	      DEFINE_STRING(BGl_string2922z00zz__objectz00,
		BgL_bgl_string2922za700za7za7_3357za7, "max-generic", 11);
	      DEFINE_STRING(BGl_string2923z00zz__objectz00,
		BgL_bgl_string2923za700za7za7_3358za7, "Illegal super-class for class", 29);
	      DEFINE_STRING(BGl_string2924z00zz__objectz00,
		BgL_bgl_string2924za700za7za7_3359za7, "register-class!", 15);
	      DEFINE_STRING(BGl_string2925z00zz__objectz00,
		BgL_bgl_string2925za700za7za7_3360za7, "Fields not a vector", 19);
	      DEFINE_STRING(BGl_string2926z00zz__objectz00,
		BgL_bgl_string2926za700za7za7_3361za7, ")", 1);
	      DEFINE_STRING(BGl_string2927z00zz__objectz00,
		BgL_bgl_string2927za700za7za7_3362za7, "@", 1);
	      DEFINE_STRING(BGl_string2928z00zz__objectz00,
		BgL_bgl_string2928za700za7za7_3363za7, "\" (", 3);
	      DEFINE_STRING(BGl_string2929z00zz__objectz00,
		BgL_bgl_string2929za700za7za7_3364za7, "Dangerous class redefinition: \"",
		31);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_classzd2existszd2envz00zz__objectz00,
		BgL_bgl_za762classza7d2exist3365z00, BGl_z62classzd2existszb0zz__objectz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_classzd2nilzd2envz00zz__objectz00,
		BgL_bgl_za762classza7d2nilza7b3366za7, BGl_z62classzd2nilzb0zz__objectz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2930z00zz__objectz00,
		BgL_bgl_string2930za700za7za7_3367za7, "&register-class!", 16);
	      DEFINE_STRING(BGl_string2931z00zz__objectz00,
		BgL_bgl_string2931za700za7za7_3368za7,
		"unoptimal bigloo-generic-bucket-size: ", 38);
	      DEFINE_STRING(BGl_string2932z00zz__objectz00,
		BgL_bgl_string2932za700za7za7_3369za7, "make-method-array", 17);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_z52isa32zd2objectzf2cdepthzf3zd2envz53zz__objectz00,
		BgL_bgl_za762za752isa32za7d2ob3370za7,
		BGl_z62z52isa32zd2objectzf2cdepthzf3ze3zz__objectz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2933z00zz__objectz00,
		BgL_bgl_string2933za700za7za7_3371za7, "No default behavior", 19);
	      DEFINE_STRING(BGl_string2934z00zz__objectz00,
		BgL_bgl_string2934za700za7za7_3372za7, "&procedure->generic", 19);
	      DEFINE_STRING(BGl_string2853z00zz__objectz00,
		BgL_bgl_string2853za700za7za7_3373za7,
		"/tmp/bigloo/runtime/Llib/object.scm", 35);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_classzd2numzd2envz00zz__objectz00,
		BgL_bgl_za762classza7d2numza7b3374za7, BGl_z62classzd2numzb0zz__objectz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2935z00zz__objectz00,
		BgL_bgl_string2935za700za7za7_3375za7, "&register-generic!", 18);
	      DEFINE_STRING(BGl_string2854z00zz__objectz00,
		BgL_bgl_string2854za700za7za7_3376za7, "&class-exists", 13);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2genericzd2bucketzd2powzd2envz00zz__objectz00,
		BgL_bgl_za762biglooza7d2gene3377z00,
		BGl_z62bigloozd2genericzd2bucketzd2powzb0zz__objectz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2936z00zz__objectz00,
		BgL_bgl_string2936za700za7za7_3378za7, "", 0);
	      DEFINE_STRING(BGl_string2855z00zz__objectz00,
		BgL_bgl_string2855za700za7za7_3379za7, "symbol", 6);
	      DEFINE_STRING(BGl_string2937z00zz__objectz00,
		BgL_bgl_string2937za700za7za7_3380za7,
		"method/generic arity mismatch, expecting ~a", 43);
	      DEFINE_STRING(BGl_string2856z00zz__objectz00,
		BgL_bgl_string2856za700za7za7_3381za7, "find-class", 10);
	      DEFINE_STRING(BGl_string2938z00zz__objectz00,
		BgL_bgl_string2938za700za7za7_3382za7, "Illegal class for method", 24);
	      DEFINE_STRING(BGl_string2857z00zz__objectz00,
		BgL_bgl_string2857za700za7za7_3383za7, "Cannot find class", 17);
	      DEFINE_STRING(BGl_string2939z00zz__objectz00,
		BgL_bgl_string2939za700za7za7_3384za7, "&generic-add-method!", 20);
	      DEFINE_STRING(BGl_string2858z00zz__objectz00,
		BgL_bgl_string2858za700za7za7_3385za7, "&find-class", 11);
	      DEFINE_STRING(BGl_string2859z00zz__objectz00,
		BgL_bgl_string2859za700za7za7_3386za7, "&find-class-by-hash", 19);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classzd2fieldzd2virtualzf3zd2envz21zz__objectz00,
		BgL_bgl_za762classza7d2field3387z00,
		BGl_z62classzd2fieldzd2virtualzf3z91zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_classzd2indexzd2envz00zz__objectz00,
		BgL_bgl_za762classza7d2index3388z00, BGl_z62classzd2indexzb0zz__objectz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_callzd2virtualzd2getterzd2envzd2zz__objectz00,
		BgL_bgl_za762callza7d2virtua3389z00,
		BGl_z62callzd2virtualzd2getterz62zz__objectz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2940z00zz__objectz00,
		BgL_bgl_string2940za700za7za7_3390za7, "&generic-add-eval-method!", 25);
	      DEFINE_STRING(BGl_string2941z00zz__objectz00,
		BgL_bgl_string2941za700za7za7_3391za7, "&find-method", 12);
	      DEFINE_STRING(BGl_string2860z00zz__objectz00,
		BgL_bgl_string2860za700za7za7_3392za7, "bint", 4);
	      DEFINE_STRING(BGl_string2942z00zz__objectz00,
		BgL_bgl_string2942za700za7za7_3393za7, "&find-super-class-method", 24);
	      DEFINE_STRING(BGl_string2861z00zz__objectz00,
		BgL_bgl_string2861za700za7za7_3394za7, "&class-name", 11);
	      DEFINE_STRING(BGl_string2943z00zz__objectz00,
		BgL_bgl_string2943za700za7za7_3395za7, "&find-method-from", 17);
	      DEFINE_STRING(BGl_string2862z00zz__objectz00,
		BgL_bgl_string2862za700za7za7_3396za7, "class", 5);
	      DEFINE_STRING(BGl_string2944z00zz__objectz00,
		BgL_bgl_string2944za700za7za7_3397za7, "&nil?", 5);
	      DEFINE_STRING(BGl_string2863z00zz__objectz00,
		BgL_bgl_string2863za700za7za7_3398za7, "&class-module", 13);
	      DEFINE_STRING(BGl_string2945z00zz__objectz00,
		BgL_bgl_string2945za700za7za7_3399za7, "&isa?", 5);
	      DEFINE_STRING(BGl_string2864z00zz__objectz00,
		BgL_bgl_string2864za700za7za7_3400za7, "&class-index", 12);
	      DEFINE_STRING(BGl_string2946z00zz__objectz00,
		BgL_bgl_string2946za700za7za7_3401za7, "&%isa/cdepth?", 13);
	      DEFINE_STRING(BGl_string2865z00zz__objectz00,
		BgL_bgl_string2865za700za7za7_3402za7, "&class-num", 10);
	      DEFINE_STRING(BGl_string2947z00zz__objectz00,
		BgL_bgl_string2947za700za7za7_3403za7, "&%isa-object/cdepth?", 20);
	      DEFINE_STRING(BGl_string2866z00zz__objectz00,
		BgL_bgl_string2866za700za7za7_3404za7, "&class-virtual", 14);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_objectzd2classzd2numzd2envzd2zz__objectz00,
		BgL_bgl_za762objectza7d2clas3405z00,
		BGl_z62objectzd2classzd2numz62zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2948z00zz__objectz00,
		BgL_bgl_string2948za700za7za7_3406za7, "&%isa32-object/cdepth?", 22);
	      DEFINE_STRING(BGl_string2867z00zz__objectz00,
		BgL_bgl_string2867za700za7za7_3407za7, "&class-evdata", 13);
	      DEFINE_STRING(BGl_string2949z00zz__objectz00,
		BgL_bgl_string2949za700za7za7_3408za7, "&%isa64-object/cdepth?", 22);
	      DEFINE_STRING(BGl_string2868z00zz__objectz00,
		BgL_bgl_string2868za700za7za7_3409za7, "&class-evdata-set!", 18);
	      DEFINE_STRING(BGl_string2869z00zz__objectz00,
		BgL_bgl_string2869za700za7za7_3410za7, "class-evfields-set!", 19);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_objectzd2equalzf3zd2envzf3zz__objectz00,
		BgL_bgl_za762objectza7d2equa3411z00,
		BGl_z62objectzd2equalzf3z43zz__objectz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_findzd2methodzd2envz00zz__objectz00,
		BgL_bgl_za762findza7d2method3412z00, BGl_z62findzd2methodzb0zz__objectz00,
		0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2950z00zz__objectz00,
		BgL_bgl_string2950za700za7za7_3413za7, "&%isa/final?", 12);
	      DEFINE_STRING(BGl_string2951z00zz__objectz00,
		BgL_bgl_string2951za700za7za7_3414za7, "&%isa-object/final?", 19);
	      DEFINE_STRING(BGl_string2870z00zz__objectz00,
		BgL_bgl_string2870za700za7za7_3415za7, "Fields already set", 18);
	      DEFINE_STRING(BGl_string2952z00zz__objectz00,
		BgL_bgl_string2952za700za7za7_3416za7, "allocate-instance", 17);
	      DEFINE_STRING(BGl_string2871z00zz__objectz00,
		BgL_bgl_string2871za700za7za7_3417za7, "Not an eval class", 17);
	      DEFINE_STRING(BGl_string2953z00zz__objectz00,
		BgL_bgl_string2953za700za7za7_3418za7, "&allocate-instance", 18);
	      DEFINE_STRING(BGl_string2872z00zz__objectz00,
		BgL_bgl_string2872za700za7za7_3419za7, "&class-evfields-set!", 20);
	      DEFINE_STRING(BGl_string2954z00zz__objectz00,
		BgL_bgl_string2954za700za7za7_3420za7, "&wide-object?", 13);
	      DEFINE_STRING(BGl_string2873z00zz__objectz00,
		BgL_bgl_string2873za700za7za7_3421za7, "vector", 6);
	      DEFINE_STRING(BGl_string2955z00zz__objectz00,
		BgL_bgl_string2955za700za7za7_3422za7, "&call-virtual-getter", 20);
	      DEFINE_STRING(BGl_string2874z00zz__objectz00,
		BgL_bgl_string2874za700za7za7_3423za7, "&class-fields", 13);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_z52isa64zd2objectzf2cdepthzf3zd2envz53zz__objectz00,
		BgL_bgl_za762za752isa64za7d2ob3424za7,
		BGl_z62z52isa64zd2objectzf2cdepthzf3ze3zz__objectz00, 0L, BUNSPEC, 3);
	extern obj_t BGl_displayzd2envzd2zz__r4_output_6_10_3z00;
	   
		 
		DEFINE_STRING(BGl_string2956z00zz__objectz00,
		BgL_bgl_string2956za700za7za7_3425za7, "&call-virtual-setter", 20);
	      DEFINE_STRING(BGl_string2875z00zz__objectz00,
		BgL_bgl_string2875za700za7za7_3426za7, "&class-all-fields", 17);
	      DEFINE_STRING(BGl_string2957z00zz__objectz00,
		BgL_bgl_string2957za700za7za7_3427za7, "&call-next-virtual-getter", 25);
	      DEFINE_STRING(BGl_string2876z00zz__objectz00,
		BgL_bgl_string2876za700za7za7_3428za7, "&find-class-field", 17);
	      DEFINE_STRING(BGl_string2958z00zz__objectz00,
		BgL_bgl_string2958za700za7za7_3429za7, "&call-next-virtual-setter", 25);
	      DEFINE_STRING(BGl_string2877z00zz__objectz00,
		BgL_bgl_string2877za700za7za7_3430za7, "&make-class-field", 17);
	      DEFINE_STRING(BGl_string2959z00zz__objectz00,
		BgL_bgl_string2959za700za7za7_3431za7, "&object-widening", 16);
	      DEFINE_STRING(BGl_string2878z00zz__objectz00,
		BgL_bgl_string2878za700za7za7_3432za7, "&class-field-name", 17);
	      DEFINE_STRING(BGl_string2879z00zz__objectz00,
		BgL_bgl_string2879za700za7za7_3433za7, "class-field", 11);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_genericzd2memoryzd2statisticszd2envzd2zz__objectz00,
		BgL_bgl_za762genericza7d2mem3434z00,
		BGl_z62genericzd2memoryzd2statisticsz62zz__objectz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_classzd2constructorzd2envz00zz__objectz00,
		BgL_bgl_za762classza7d2const3435z00,
		BGl_z62classzd2constructorzb0zz__objectz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_findzd2superzd2classzd2methodzd2envz00zz__objectz00,
		BgL_bgl_za762findza7d2superza73436za7,
		BGl_z62findzd2superzd2classzd2methodzb0zz__objectz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2960z00zz__objectz00,
		BgL_bgl_string2960za700za7za7_3437za7, "&object-widening-set!", 21);
	      DEFINE_STRING(BGl_string2961z00zz__objectz00,
		BgL_bgl_string2961za700za7za7_3438za7, "&%object-widening", 17);
	      DEFINE_STRING(BGl_string2880z00zz__objectz00,
		BgL_bgl_string2880za700za7za7_3439za7, "&class-field-virtual?", 21);
	      DEFINE_STRING(BGl_string2962z00zz__objectz00,
		BgL_bgl_string2962za700za7za7_3440za7, "&%object-widening-set!", 22);
	      DEFINE_STRING(BGl_string2881z00zz__objectz00,
		BgL_bgl_string2881za700za7za7_3441za7, "&class-field-accessor", 21);
	      DEFINE_STRING(BGl_string2882z00zz__objectz00,
		BgL_bgl_string2882za700za7za7_3442za7, "&class-field-mutable?", 21);
	      DEFINE_STRING(BGl_string2883z00zz__objectz00,
		BgL_bgl_string2883za700za7za7_3443za7, "&class-field-mutator", 20);
	      DEFINE_STRING(BGl_string2884z00zz__objectz00,
		BgL_bgl_string2884za700za7za7_3444za7, "&class-field-info", 17);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_genericzd2addzd2evalzd2methodz12zd2envz12zz__objectz00,
		BgL_bgl_za762genericza7d2add3445z00,
		BGl_z62genericzd2addzd2evalzd2methodz12za2zz__objectz00, 0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string2885z00zz__objectz00,
		BgL_bgl_string2885za700za7za7_3446za7, "&class-field-default-value?", 27);
	      DEFINE_STRING(BGl_string2886z00zz__objectz00,
		BgL_bgl_string2886za700za7za7_3447za7, "class-field-default-value", 25);
	      DEFINE_STRING(BGl_string2968z00zz__objectz00,
		BgL_bgl_string2968za700za7za7_3448za7, "__object", 8);
	      DEFINE_STRING(BGl_string2887z00zz__objectz00,
		BgL_bgl_string2887za700za7za7_3449za7, "This field has no default value",
		31);
	      DEFINE_STRING(BGl_string2888z00zz__objectz00,
		BgL_bgl_string2888za700za7za7_3450za7, "&class-field-default-value", 26);
	      DEFINE_STRING(BGl_string2889z00zz__objectz00,
		BgL_bgl_string2889za700za7za7_3451za7, "&class-field-type", 17);
	extern obj_t BGl_writezd2envzd2zz__r4_output_6_10_3z00;
	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2963z00zz__objectz00,
		BgL_bgl_za762za7c3za704anonymo3452za7,
		BGl_z62zc3z04anonymousza31850ze3ze5zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2964z00zz__objectz00,
		BgL_bgl_za762lambda1848za7623453z00, BGl_z62lambda1848z62zz__objectz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2965z00zz__objectz00,
		BgL_bgl_za762lambda1846za7623454z00, BGl_z62lambda1846z62zz__objectz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2890z00zz__objectz00,
		BgL_bgl_string2890za700za7za7_3455za7, "&class-super", 12);
	      DEFINE_STRING(BGl_string2891z00zz__objectz00,
		BgL_bgl_string2891za700za7za7_3456za7, "class-allocator", 15);
	      DEFINE_STRING(BGl_string2973z00zz__objectz00,
		BgL_bgl_string2973za700za7za7_3457za7, "&condition", 10);
	      DEFINE_STRING(BGl_string2892z00zz__objectz00,
		BgL_bgl_string2892za700za7za7_3458za7, "&class-abstract?", 16);
	      DEFINE_STRING(BGl_string2893z00zz__objectz00,
		BgL_bgl_string2893za700za7za7_3459za7, "class-shrink", 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2969z00zz__objectz00,
		BgL_bgl_za762za7c3za704anonymo3460za7,
		BGl_z62zc3z04anonymousza31861ze3ze5zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2894z00zz__objectz00,
		BgL_bgl_string2894za700za7za7_3461za7, "&class-wide?", 12);
	      DEFINE_STRING(BGl_string2895z00zz__objectz00,
		BgL_bgl_string2895za700za7za7_3462za7, "&class-subclasses", 17);
	      DEFINE_STRING(BGl_string2896z00zz__objectz00,
		BgL_bgl_string2896za700za7za7_3463za7, "&class-allocator", 16);
	      DEFINE_STRING(BGl_string2978z00zz__objectz00,
		BgL_bgl_string2978za700za7za7_3464za7, "fname", 5);
	      DEFINE_STRING(BGl_string2897z00zz__objectz00,
		BgL_bgl_string2897za700za7za7_3465za7, "&class-hash", 11);
	      DEFINE_STRING(BGl_string2898z00zz__objectz00,
		BgL_bgl_string2898za700za7za7_3466za7, "&class-constructor", 18);
	      DEFINE_STRING(BGl_string2899z00zz__objectz00,
		BgL_bgl_string2899za700za7za7_3467za7, "&class-creator", 14);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classzd2evfieldszd2setz12zd2envzc0zz__objectz00,
		BgL_bgl_za762classza7d2evfie3468z00,
		BGl_z62classzd2evfieldszd2setz12z70zz__objectz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2970z00zz__objectz00,
		BgL_bgl_za762lambda1859za7623469z00, BGl_z62lambda1859z62zz__objectz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2971z00zz__objectz00,
		BgL_bgl_za762lambda1857za7623470z00, BGl_z62lambda1857z62zz__objectz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2980z00zz__objectz00,
		BgL_bgl_string2980za700za7za7_3471za7, "obj", 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2974z00zz__objectz00,
		BgL_bgl_za762za7c3za704anonymo3472za7,
		BGl_z62zc3z04anonymousza31880ze3ze5zz__objectz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2975z00zz__objectz00,
		BgL_bgl_za762lambda1879za7623473z00, BGl_z62lambda1879z62zz__objectz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2976z00zz__objectz00,
		BgL_bgl_za762lambda1878za7623474z00, BGl_z62lambda1878z62zz__objectz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2985z00zz__objectz00,
		BgL_bgl_string2985za700za7za7_3475za7, "location", 8);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_callzd2nextzd2virtualzd2setterzd2envz00zz__objectz00,
		BgL_bgl_za762callza7d2nextza7d3476za7,
		BGl_z62callzd2nextzd2virtualzd2setterzb0zz__objectz00, 0L, BUNSPEC, 4);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_nilzf3zd2envz21zz__objectz00,
		BgL_bgl_za762nilza7f3za791za7za7__3477za7, BGl_z62nilzf3z91zz__objectz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2981z00zz__objectz00,
		BgL_bgl_za762za7c3za704anonymo3478za7,
		BGl_z62zc3z04anonymousza31888ze3ze5zz__objectz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2982z00zz__objectz00,
		BgL_bgl_za762lambda1887za7623479z00, BGl_z62lambda1887z62zz__objectz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2983z00zz__objectz00,
		BgL_bgl_za762lambda1886za7623480z00, BGl_z62lambda1886z62zz__objectz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2990z00zz__objectz00,
		BgL_bgl_string2990za700za7za7_3481za7, "stack", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2986z00zz__objectz00,
		BgL_bgl_za762za7c3za704anonymo3482za7,
		BGl_z62zc3z04anonymousza31895ze3ze5zz__objectz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2987z00zz__objectz00,
		BgL_bgl_za762lambda1894za7623483z00, BGl_z62lambda1894z62zz__objectz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_z52objectzd2wideningzd2setz12zd2envz92zz__objectz00,
		BgL_bgl_za762za752objectza7d2w3484za7,
		BGl_z62z52objectzd2wideningzd2setz12z22zz__objectz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2988z00zz__objectz00,
		BgL_bgl_za762lambda1893za7623485z00, BGl_z62lambda1893z62zz__objectz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2995z00zz__objectz00,
		BgL_bgl_string2995za700za7za7_3486za7, "&exception", 10);
	      DEFINE_STRING(BGl_string2999z00zz__objectz00,
		BgL_bgl_string2999za700za7za7_3487za7, "proc", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2991z00zz__objectz00,
		BgL_bgl_za762za7c3za704anonymo3488za7,
		BGl_z62zc3z04anonymousza31873ze3ze5zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2992z00zz__objectz00,
		BgL_bgl_za762lambda1871za7623489z00, BGl_z62lambda1871z62zz__objectz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2993z00zz__objectz00,
		BgL_bgl_za762lambda1869za7623490z00, BGl_z62lambda1869z62zz__objectz00, 0L,
		BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2996z00zz__objectz00,
		BgL_bgl_za762lambda1914za7623491z00, BGl_z62lambda1914z62zz__objectz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2997z00zz__objectz00,
		BgL_bgl_za762lambda1913za7623492z00, BGl_z62lambda1913z62zz__objectz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_isazf3zd2envz21zz__objectz00,
		BgL_bgl_za762isaza7f3za791za7za7__3493za7, BGl_z62isazf3z91zz__objectz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_z52isazd2objectzf2finalzf3zd2envz53zz__objectz00,
		BgL_bgl_za762za752isaza7d2obje3494za7,
		BGl_z62z52isazd2objectzf2finalzf3ze3zz__objectz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_classzd2allzd2fieldszd2envzd2zz__objectz00,
		BgL_bgl_za762classza7d2allza7d3495za7,
		BGl_z62classzd2allzd2fieldsz62zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_classzd2fieldzf3zd2envzf3zz__objectz00,
		BgL_bgl_za762classza7d2field3496z00,
		BGl_z62classzd2fieldzf3z43zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_classzd2modulezd2envz00zz__objectz00,
		BgL_bgl_za762classza7d2modul3497z00, BGl_z62classzd2modulezb0zz__objectz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_registerzd2classz12zd2envz12zz__objectz00,
		BgL_bgl_za762registerza7d2cl3498z00,
		BGl_z62registerzd2classz12za2zz__objectz00, 0L, BUNSPEC, 11);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_classzd2widezf3zd2envzf3zz__objectz00,
		BgL_bgl_za762classza7d2wideza73499za7,
		BGl_z62classzd2widezf3z43zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_genericzd2defaultzd2envz00zz__objectz00,
		BgL_bgl_za762genericza7d2def3500z00,
		BGl_z62genericzd2defaultzb0zz__objectz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classzd2evdatazd2setz12zd2envzc0zz__objectz00,
		BgL_bgl_za762classza7d2evdat3501z00,
		BGl_z62classzd2evdatazd2setz12z70zz__objectz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_objectzd2wideningzd2setz12zd2envzc0zz__objectz00,
		BgL_bgl_za762objectza7d2wide3502z00,
		BGl_z62objectzd2wideningzd2setz12z70zz__objectz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classzd2fieldzd2mutablezf3zd2envz21zz__objectz00,
		BgL_bgl_za762classza7d2field3503z00,
		BGl_z62classzd2fieldzd2mutablezf3z91zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_z52isazf2finalzf3zd2envz81zz__objectz00,
		BgL_bgl_za762za752isaza7f2fina3504za7,
		BGl_z62z52isazf2finalzf3z31zz__objectz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_findzd2classzd2fieldzd2envzd2zz__objectz00,
		BgL_bgl_za762findza7d2classza73505za7,
		BGl_z62findzd2classzd2fieldz62zz__objectz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2genericzd2bucketzd2maskzd2envz00zz__objectz00,
		BgL_bgl_za762biglooza7d2gene3506z00,
		BGl_z62bigloozd2genericzd2bucketzd2maskzb0zz__objectz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_classzd2allocatorzd2envz00zz__objectz00,
		BgL_bgl_za762classza7d2alloc3507z00,
		BGl_z62classzd2allocatorzb0zz__objectz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_objectzd2classzd2numzd2setz12zd2envz12zz__objectz00,
		BgL_bgl_za762objectza7d2clas3508z00,
		BGl_z62objectzd2classzd2numzd2setz12za2zz__objectz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_callzd2virtualzd2setterzd2envzd2zz__objectz00,
		BgL_bgl_za762callza7d2virtua3509z00,
		BGl_z62callzd2virtualzd2setterz62zz__objectz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_classzd2fieldzd2namezd2envzd2zz__objectz00,
		BgL_bgl_za762classza7d2field3510z00,
		BGl_z62classzd2fieldzd2namez62zz__objectz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_methodzd2arrayzd2refzd2envzd2zz__objectz00,
		BgL_bgl_za762methodza7d2arra3511z00,
		BGl_z62methodzd2arrayzd2refz62zz__objectz00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_objectzd2classzd2envz00zz__objectz00,
		BgL_bgl_za762objectza7d2clas3512z00, BGl_z62objectzd2classzb0zz__objectz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3003z00zz__objectz00,
		BgL_bgl_string3003za700za7za7_3513za7, "msg", 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_classzd2abstractzf3zd2envzf3zz__objectz00,
		BgL_bgl_za762classza7d2abstr3514z00,
		BGl_z62classzd2abstractzf3z43zz__objectz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3000z00zz__objectz00,
		BgL_bgl_za762lambda1920za7623515z00, BGl_z62lambda1920z62zz__objectz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3001z00zz__objectz00,
		BgL_bgl_za762lambda1919za7623516z00, BGl_z62lambda1919z62zz__objectz00, 0L,
		BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_z62processzd2exceptionzb0zz__objectz00));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol2910z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_za2classesza2z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol2912z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol2913z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol2915z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol2917z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol2919z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_z62iozd2sigpipezd2errorz62zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol2921z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_z62iozd2writezd2errorz62zz__objectz00));
		     ADD_ROOT((void *) (&BGl_z62typezd2errorzb0zz__objectz00));
		     ADD_ROOT((void *) (&BGl_z62iozd2parsezd2errorz62zz__objectz00));
		     ADD_ROOT((void *) (&BGl_za2nbzd2classeszd2maxza2z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_za2inheritancesza2z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_z62exceptionz62zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol2966z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol2967z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol2972z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol2977z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol2979z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol2984z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol2989z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol2994z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol2998z00zz__objectz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2inheritancezd2maxzd2depthza2z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_za2classzd2keyza2zd2zz__objectz00));
		     ADD_ROOT((void *) (&BGl_za2nbzd2genericsza2zd2zz__objectz00));
		     ADD_ROOT((void *) (&BGl_z62securityzd2exceptionzb0zz__objectz00));
		   
			 ADD_ROOT((void *) (&BGl_z62iozd2unknownzd2hostzd2errorzb0zz__objectz00));
		     ADD_ROOT((void *) (&BGl_z62iozd2closedzd2errorz62zz__objectz00));
		     ADD_ROOT((void *) (&BGl_z62evalzd2warningzb0zz__objectz00));
		   
			 ADD_ROOT((void
				*) (&BGl_z62iozd2malformedzd2urlzd2errorzb0zz__objectz00));
		     ADD_ROOT((void
				*) (&BGl_z62iozd2filezd2notzd2foundzd2errorz62zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol3002z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol3009z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_z62errorz62zz__objectz00));
		     ADD_ROOT((void *) (&BGl_z62warningz62zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol3013z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol3018z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_za2inheritancezd2cntza2zd2zz__objectz00));
		     ADD_ROOT((void *) (&BGl_z62stackzd2overflowzd2errorz62zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol3102z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol3022z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol3104z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol3027z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol3109z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_za2genericsza2z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_z62iozd2portzd2errorz62zz__objectz00));
		     ADD_ROOT((void *) (&BGl_z62iozd2connectionzd2errorz62zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol3032z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol3117z00zz__objectz00));
		   
			 ADD_ROOT((void
				*) (&BGl_z62accesszd2controlzd2exceptionz62zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol3037z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_z62iozd2timeoutzd2errorz62zz__objectz00));
		     ADD_ROOT((void *) (&BGl_z62iozd2errorzb0zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol3122z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol3042z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol3126z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol3047z00zz__objectz00));
		   
			 ADD_ROOT((void
				*) (&BGl_z62indexzd2outzd2ofzd2boundszd2errorz62zz__objectz00));
		     ADD_ROOT((void *) (&BGl_objectz00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol3131z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol3052z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol3136z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol3057z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol3062z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol3067z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_z62conditionz62zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol3072z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol3077z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol3082z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol3087z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol3092z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_symbol3097z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_za2nbzd2genericszd2maxza2z00zz__objectz00));
		     ADD_ROOT((void *) (&BGl_za2nbzd2classesza2zd2zz__objectz00));
		     ADD_ROOT((void *) (&BGl_z62iozd2readzd2errorz62zz__objectz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long
		BgL_checksumz00_6394, char *BgL_fromz00_6395)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__objectz00))
				{
					BGl_requirezd2initializa7ationz75zz__objectz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__objectz00();
					BGl_cnstzd2initzd2zz__objectz00();
					BGl_objectzd2initzd2zz__objectz00();
					BGl_genericzd2initzd2zz__objectz00();
					BGl_methodzd2initzd2zz__objectz00();
					return BGl_toplevelzd2initzd2zz__objectz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__objectz00(void)
	{
		{	/* Llib/object.scm 17 */
			BGl_symbol2910z00zz__objectz00 =
				bstring_to_symbol(BGl_string2911z00zz__objectz00);
			BGl_symbol2912z00zz__objectz00 =
				bstring_to_symbol(BGl_string2862z00zz__objectz00);
			BGl_symbol2913z00zz__objectz00 =
				bstring_to_symbol(BGl_string2914z00zz__objectz00);
			BGl_symbol2915z00zz__objectz00 =
				bstring_to_symbol(BGl_string2916z00zz__objectz00);
			BGl_symbol2917z00zz__objectz00 =
				bstring_to_symbol(BGl_string2918z00zz__objectz00);
			BGl_symbol2919z00zz__objectz00 =
				bstring_to_symbol(BGl_string2920z00zz__objectz00);
			BGl_symbol2921z00zz__objectz00 =
				bstring_to_symbol(BGl_string2922z00zz__objectz00);
			BGl_symbol2966z00zz__objectz00 =
				bstring_to_symbol(BGl_string2903z00zz__objectz00);
			BGl_symbol2967z00zz__objectz00 =
				bstring_to_symbol(BGl_string2968z00zz__objectz00);
			BGl_symbol2972z00zz__objectz00 =
				bstring_to_symbol(BGl_string2973z00zz__objectz00);
			BGl_symbol2977z00zz__objectz00 =
				bstring_to_symbol(BGl_string2978z00zz__objectz00);
			BGl_symbol2979z00zz__objectz00 =
				bstring_to_symbol(BGl_string2980z00zz__objectz00);
			BGl_symbol2984z00zz__objectz00 =
				bstring_to_symbol(BGl_string2985z00zz__objectz00);
			BGl_symbol2989z00zz__objectz00 =
				bstring_to_symbol(BGl_string2990z00zz__objectz00);
			BGl_symbol2994z00zz__objectz00 =
				bstring_to_symbol(BGl_string2995z00zz__objectz00);
			BGl_symbol2998z00zz__objectz00 =
				bstring_to_symbol(BGl_string2999z00zz__objectz00);
			BGl_symbol3002z00zz__objectz00 =
				bstring_to_symbol(BGl_string3003z00zz__objectz00);
			BGl_symbol3009z00zz__objectz00 =
				bstring_to_symbol(BGl_string3010z00zz__objectz00);
			BGl_symbol3013z00zz__objectz00 =
				bstring_to_symbol(BGl_string3014z00zz__objectz00);
			BGl_symbol3018z00zz__objectz00 =
				bstring_to_symbol(BGl_string3019z00zz__objectz00);
			BGl_symbol3022z00zz__objectz00 =
				bstring_to_symbol(BGl_string3023z00zz__objectz00);
			BGl_symbol3027z00zz__objectz00 =
				bstring_to_symbol(BGl_string3028z00zz__objectz00);
			BGl_symbol3032z00zz__objectz00 =
				bstring_to_symbol(BGl_string3033z00zz__objectz00);
			BGl_symbol3037z00zz__objectz00 =
				bstring_to_symbol(BGl_string3038z00zz__objectz00);
			BGl_symbol3042z00zz__objectz00 =
				bstring_to_symbol(BGl_string3043z00zz__objectz00);
			BGl_symbol3047z00zz__objectz00 =
				bstring_to_symbol(BGl_string3048z00zz__objectz00);
			BGl_symbol3052z00zz__objectz00 =
				bstring_to_symbol(BGl_string3053z00zz__objectz00);
			BGl_symbol3057z00zz__objectz00 =
				bstring_to_symbol(BGl_string3058z00zz__objectz00);
			BGl_symbol3062z00zz__objectz00 =
				bstring_to_symbol(BGl_string3063z00zz__objectz00);
			BGl_symbol3067z00zz__objectz00 =
				bstring_to_symbol(BGl_string3068z00zz__objectz00);
			BGl_symbol3072z00zz__objectz00 =
				bstring_to_symbol(BGl_string3073z00zz__objectz00);
			BGl_symbol3077z00zz__objectz00 =
				bstring_to_symbol(BGl_string3078z00zz__objectz00);
			BGl_symbol3082z00zz__objectz00 =
				bstring_to_symbol(BGl_string3083z00zz__objectz00);
			BGl_symbol3087z00zz__objectz00 =
				bstring_to_symbol(BGl_string3088z00zz__objectz00);
			BGl_symbol3092z00zz__objectz00 =
				bstring_to_symbol(BGl_string3093z00zz__objectz00);
			BGl_symbol3097z00zz__objectz00 =
				bstring_to_symbol(BGl_string3098z00zz__objectz00);
			BGl_symbol3102z00zz__objectz00 =
				bstring_to_symbol(BGl_string3103z00zz__objectz00);
			BGl_symbol3104z00zz__objectz00 =
				bstring_to_symbol(BGl_string3105z00zz__objectz00);
			BGl_symbol3109z00zz__objectz00 =
				bstring_to_symbol(BGl_string3110z00zz__objectz00);
			BGl_symbol3117z00zz__objectz00 =
				bstring_to_symbol(BGl_string3118z00zz__objectz00);
			BGl_symbol3122z00zz__objectz00 =
				bstring_to_symbol(BGl_string3123z00zz__objectz00);
			BGl_symbol3126z00zz__objectz00 =
				bstring_to_symbol(BGl_string3127z00zz__objectz00);
			BGl_symbol3131z00zz__objectz00 =
				bstring_to_symbol(BGl_string3132z00zz__objectz00);
			return (BGl_symbol3136z00zz__objectz00 =
				bstring_to_symbol(BGl_string3137z00zz__objectz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__objectz00(void)
	{
		{	/* Llib/object.scm 17 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__objectz00(void)
	{
		{	/* Llib/object.scm 17 */
			BUNSPEC;
			BUNSPEC;
			BUNSPEC;
			BUNSPEC;
			BUNSPEC;
			BUNSPEC;
			BUNSPEC;
			BUNSPEC;
			BUNSPEC;
			BUNSPEC;
			return BUNSPEC;
		}

	}



/* bigloo-generic-bucket-pow */
	BGL_EXPORTED_DEF int BGl_bigloozd2genericzd2bucketzd2powzd2zz__objectz00(void)
	{
		{	/* Llib/object.scm 426 */
			return (int) (4L);
		}

	}



/* &bigloo-generic-bucket-pow */
	obj_t BGl_z62bigloozd2genericzd2bucketzd2powzb0zz__objectz00(obj_t
		BgL_envz00_5033)
	{
		{	/* Llib/object.scm 426 */
			return BINT(BGl_bigloozd2genericzd2bucketzd2powzd2zz__objectz00());
		}

	}



/* bigloo-generic-bucket-size */
	BGL_EXPORTED_DEF int
		BGl_bigloozd2genericzd2bucketzd2siza7ez75zz__objectz00(void)
	{
		{	/* Llib/object.scm 428 */
			return (int) ((1L << (int) ((long) ((int) (4L)))));
		}

	}



/* &bigloo-generic-bucket-size */
	obj_t BGl_z62bigloozd2genericzd2bucketzd2siza7ez17zz__objectz00(obj_t
		BgL_envz00_5034)
	{
		{	/* Llib/object.scm 428 */
			return BINT(BGl_bigloozd2genericzd2bucketzd2siza7ez75zz__objectz00());
		}

	}



/* bigloo-generic-bucket-mask */
	BGL_EXPORTED_DEF int
		BGl_bigloozd2genericzd2bucketzd2maskzd2zz__objectz00(void)
	{
		{	/* Llib/object.scm 430 */
			return
				(int) (((long) ((int) ((1L << (int) ((long) ((int) (4L)))))) - 1L));
		}

	}



/* &bigloo-generic-bucket-mask */
	obj_t BGl_z62bigloozd2genericzd2bucketzd2maskzb0zz__objectz00(obj_t
		BgL_envz00_5035)
	{
		{	/* Llib/object.scm 430 */
			return BINT(BGl_bigloozd2genericzd2bucketzd2maskzd2zz__objectz00());
		}

	}



/* bigloo-types-number */
	BGL_EXPORTED_DEF long bgl_types_number(void)
	{
		{	/* Llib/object.scm 440 */
			if (INTEGERP(BGl_za2nbzd2classesza2zd2zz__objectz00))
				{	/* Llib/object.scm 441 */
					return
						(OBJECT_TYPE + (long) CINT(BGl_za2nbzd2classesza2zd2zz__objectz00));
				}
			else
				{	/* Llib/object.scm 441 */
					return OBJECT_TYPE;
				}
		}

	}



/* &bigloo-types-number */
	obj_t BGl_z62bigloozd2typeszd2numberz62zz__objectz00(obj_t BgL_envz00_5036)
	{
		{	/* Llib/object.scm 440 */
			return BINT(bgl_types_number());
		}

	}



/* class? */
	BGL_EXPORTED_DEF bool_t BGl_classzf3zf3zz__objectz00(obj_t BgL_objz00_20)
	{
		{	/* Llib/object.scm 468 */
			return BGL_CLASSP(BgL_objz00_20);
		}

	}



/* &class? */
	obj_t BGl_z62classzf3z91zz__objectz00(obj_t BgL_envz00_5037,
		obj_t BgL_objz00_5038)
	{
		{	/* Llib/object.scm 468 */
			return BBOOL(BGl_classzf3zf3zz__objectz00(BgL_objz00_5038));
		}

	}



/* class-exists */
	BGL_EXPORTED_DEF obj_t BGl_classzd2existszd2zz__objectz00(obj_t
		BgL_cnamez00_21)
	{
		{	/* Llib/object.scm 474 */
			{
				long BgL_iz00_2981;

				BgL_iz00_2981 = 0L;
			BgL_loopz00_2980:
				if (
					(BgL_iz00_2981 ==
						(long) CINT(BGl_za2nbzd2classesza2zd2zz__objectz00)))
					{	/* Llib/object.scm 476 */
						return BFALSE;
					}
				else
					{	/* Llib/object.scm 477 */
						obj_t BgL_claz00_2985;

						BgL_claz00_2985 =
							VECTOR_REF(BGl_za2classesza2z00zz__objectz00, BgL_iz00_2981);
						{	/* Llib/object.scm 478 */
							bool_t BgL_test3520z00_6483;

							{	/* Llib/object.scm 478 */
								obj_t BgL_arg1495z00_2989;

								{	/* Llib/object.scm 510 */
									obj_t BgL_tmpz00_6484;

									BgL_tmpz00_6484 = ((obj_t) BgL_claz00_2985);
									BgL_arg1495z00_2989 = BGL_CLASS_NAME(BgL_tmpz00_6484);
								}
								BgL_test3520z00_6483 = (BgL_arg1495z00_2989 == BgL_cnamez00_21);
							}
							if (BgL_test3520z00_6483)
								{	/* Llib/object.scm 478 */
									return BgL_claz00_2985;
								}
							else
								{
									long BgL_iz00_6488;

									BgL_iz00_6488 = (BgL_iz00_2981 + 1L);
									BgL_iz00_2981 = BgL_iz00_6488;
									goto BgL_loopz00_2980;
								}
						}
					}
			}
		}

	}



/* &class-exists */
	obj_t BGl_z62classzd2existszb0zz__objectz00(obj_t BgL_envz00_5039,
		obj_t BgL_cnamez00_5040)
	{
		{	/* Llib/object.scm 474 */
			{	/* Llib/object.scm 475 */
				obj_t BgL_auxz00_6490;

				if (SYMBOLP(BgL_cnamez00_5040))
					{	/* Llib/object.scm 475 */
						BgL_auxz00_6490 = BgL_cnamez00_5040;
					}
				else
					{
						obj_t BgL_auxz00_6493;

						BgL_auxz00_6493 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(19665L), BGl_string2854z00zz__objectz00,
							BGl_string2855z00zz__objectz00, BgL_cnamez00_5040);
						FAILURE(BgL_auxz00_6493, BFALSE, BFALSE);
					}
				return BGl_classzd2existszd2zz__objectz00(BgL_auxz00_6490);
			}
		}

	}



/* find-class */
	BGL_EXPORTED_DEF obj_t BGl_findzd2classzd2zz__objectz00(obj_t BgL_cnamez00_22)
	{
		{	/* Llib/object.scm 485 */
			{	/* Llib/object.scm 486 */
				obj_t BgL__ortest_1213z00_2993;

				BgL__ortest_1213z00_2993 =
					BGl_classzd2existszd2zz__objectz00(BgL_cnamez00_22);
				if (CBOOL(BgL__ortest_1213z00_2993))
					{	/* Llib/object.scm 486 */
						return BgL__ortest_1213z00_2993;
					}
				else
					{	/* Llib/object.scm 486 */
						return
							BGl_errorz00zz__errorz00(BGl_string2856z00zz__objectz00,
							BGl_string2857z00zz__objectz00, BgL_cnamez00_22);
					}
			}
		}

	}



/* &find-class */
	obj_t BGl_z62findzd2classzb0zz__objectz00(obj_t BgL_envz00_5041,
		obj_t BgL_cnamez00_5042)
	{
		{	/* Llib/object.scm 485 */
			{	/* Llib/object.scm 486 */
				obj_t BgL_auxz00_6502;

				if (SYMBOLP(BgL_cnamez00_5042))
					{	/* Llib/object.scm 486 */
						BgL_auxz00_6502 = BgL_cnamez00_5042;
					}
				else
					{
						obj_t BgL_auxz00_6505;

						BgL_auxz00_6505 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(20086L), BGl_string2858z00zz__objectz00,
							BGl_string2855z00zz__objectz00, BgL_cnamez00_5042);
						FAILURE(BgL_auxz00_6505, BFALSE, BFALSE);
					}
				return BGl_findzd2classzd2zz__objectz00(BgL_auxz00_6502);
			}
		}

	}



/* find-class-by-hash */
	BGL_EXPORTED_DEF obj_t BGl_findzd2classzd2byzd2hashzd2zz__objectz00(int
		BgL_hashz00_23)
	{
		{	/* Llib/object.scm 492 */
			{
				long BgL_iz00_3001;

				BgL_iz00_3001 = 0L;
			BgL_loopz00_3000:
				if (
					(BgL_iz00_3001 ==
						(long) CINT(BGl_za2nbzd2classesza2zd2zz__objectz00)))
					{	/* Llib/object.scm 494 */
						return BFALSE;
					}
				else
					{	/* Llib/object.scm 495 */
						obj_t BgL_claz00_3005;

						BgL_claz00_3005 =
							VECTOR_REF(BGl_za2classesza2z00zz__objectz00, BgL_iz00_3001);
						{	/* Llib/object.scm 496 */
							bool_t BgL_test3525z00_6514;

							{	/* Llib/object.scm 496 */
								long BgL_arg1501z00_3009;

								{	/* Llib/object.scm 717 */
									obj_t BgL_tmpz00_6515;

									BgL_tmpz00_6515 = ((obj_t) BgL_claz00_3005);
									BgL_arg1501z00_3009 = BGL_CLASS_HASH(BgL_tmpz00_6515);
								}
								BgL_test3525z00_6514 =
									(BINT(BgL_arg1501z00_3009) == BINT(BgL_hashz00_23));
							}
							if (BgL_test3525z00_6514)
								{	/* Llib/object.scm 496 */
									return BgL_claz00_3005;
								}
							else
								{
									long BgL_iz00_6521;

									BgL_iz00_6521 = (BgL_iz00_3001 + 1L);
									BgL_iz00_3001 = BgL_iz00_6521;
									goto BgL_loopz00_3000;
								}
						}
					}
			}
		}

	}



/* &find-class-by-hash */
	obj_t BGl_z62findzd2classzd2byzd2hashzb0zz__objectz00(obj_t BgL_envz00_5043,
		obj_t BgL_hashz00_5044)
	{
		{	/* Llib/object.scm 492 */
			{	/* Llib/object.scm 493 */
				int BgL_auxz00_6523;

				{	/* Llib/object.scm 493 */
					obj_t BgL_tmpz00_6524;

					if (INTEGERP(BgL_hashz00_5044))
						{	/* Llib/object.scm 493 */
							BgL_tmpz00_6524 = BgL_hashz00_5044;
						}
					else
						{
							obj_t BgL_auxz00_6527;

							BgL_auxz00_6527 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
								BINT(20424L), BGl_string2859z00zz__objectz00,
								BGl_string2860z00zz__objectz00, BgL_hashz00_5044);
							FAILURE(BgL_auxz00_6527, BFALSE, BFALSE);
						}
					BgL_auxz00_6523 = CINT(BgL_tmpz00_6524);
				}
				return BGl_findzd2classzd2byzd2hashzd2zz__objectz00(BgL_auxz00_6523);
			}
		}

	}



/* eval-class? */
	BGL_EXPORTED_DEF bool_t BGl_evalzd2classzf3z21zz__objectz00(obj_t
		BgL_objz00_24)
	{
		{	/* Llib/object.scm 503 */
			if (BGL_CLASSP(BgL_objz00_24))
				{	/* Llib/object.scm 540 */
					obj_t BgL_tmpz00_6535;

					{	/* Llib/object.scm 540 */
						obj_t BgL_tmpz00_6536;

						BgL_tmpz00_6536 = ((obj_t) BgL_objz00_24);
						BgL_tmpz00_6535 = BGL_CLASS_EVDATA(BgL_tmpz00_6536);
					}
					return CBOOL(BgL_tmpz00_6535);
				}
			else
				{	/* Llib/object.scm 504 */
					return ((bool_t) 0);
				}
		}

	}



/* &eval-class? */
	obj_t BGl_z62evalzd2classzf3z43zz__objectz00(obj_t BgL_envz00_5045,
		obj_t BgL_objz00_5046)
	{
		{	/* Llib/object.scm 503 */
			return BBOOL(BGl_evalzd2classzf3z21zz__objectz00(BgL_objz00_5046));
		}

	}



/* class-name */
	BGL_EXPORTED_DEF obj_t BGl_classzd2namezd2zz__objectz00(obj_t BgL_classz00_25)
	{
		{	/* Llib/object.scm 509 */
			return BGL_CLASS_NAME(BgL_classz00_25);
		}

	}



/* &class-name */
	obj_t BGl_z62classzd2namezb0zz__objectz00(obj_t BgL_envz00_5047,
		obj_t BgL_classz00_5048)
	{
		{	/* Llib/object.scm 509 */
			{	/* Llib/object.scm 510 */
				obj_t BgL_auxz00_6543;

				if (BGl_classzf3zf3zz__objectz00(BgL_classz00_5048))
					{	/* Llib/object.scm 510 */
						BgL_auxz00_6543 = BgL_classz00_5048;
					}
				else
					{
						obj_t BgL_auxz00_6546;

						BgL_auxz00_6546 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(21131L), BGl_string2861z00zz__objectz00,
							BGl_string2862z00zz__objectz00, BgL_classz00_5048);
						FAILURE(BgL_auxz00_6546, BFALSE, BFALSE);
					}
				return BGl_classzd2namezd2zz__objectz00(BgL_auxz00_6543);
			}
		}

	}



/* class-module */
	BGL_EXPORTED_DEF obj_t BGl_classzd2modulezd2zz__objectz00(obj_t
		BgL_classz00_26)
	{
		{	/* Llib/object.scm 515 */
			return BGL_CLASS_MODULE(BgL_classz00_26);
		}

	}



/* &class-module */
	obj_t BGl_z62classzd2modulezb0zz__objectz00(obj_t BgL_envz00_5049,
		obj_t BgL_classz00_5050)
	{
		{	/* Llib/object.scm 515 */
			{	/* Llib/object.scm 516 */
				obj_t BgL_auxz00_6552;

				if (BGl_classzf3zf3zz__objectz00(BgL_classz00_5050))
					{	/* Llib/object.scm 516 */
						BgL_auxz00_6552 = BgL_classz00_5050;
					}
				else
					{
						obj_t BgL_auxz00_6555;

						BgL_auxz00_6555 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(21407L), BGl_string2863z00zz__objectz00,
							BGl_string2862z00zz__objectz00, BgL_classz00_5050);
						FAILURE(BgL_auxz00_6555, BFALSE, BFALSE);
					}
				return BGl_classzd2modulezd2zz__objectz00(BgL_auxz00_6552);
			}
		}

	}



/* class-index */
	BGL_EXPORTED_DEF long BGl_classzd2indexzd2zz__objectz00(obj_t BgL_classz00_27)
	{
		{	/* Llib/object.scm 521 */
			return BGL_CLASS_INDEX(BgL_classz00_27);
		}

	}



/* &class-index */
	obj_t BGl_z62classzd2indexzb0zz__objectz00(obj_t BgL_envz00_5051,
		obj_t BgL_classz00_5052)
	{
		{	/* Llib/object.scm 521 */
			{	/* Llib/object.scm 522 */
				long BgL_tmpz00_6561;

				{	/* Llib/object.scm 522 */
					obj_t BgL_auxz00_6562;

					if (BGl_classzf3zf3zz__objectz00(BgL_classz00_5052))
						{	/* Llib/object.scm 522 */
							BgL_auxz00_6562 = BgL_classz00_5052;
						}
					else
						{
							obj_t BgL_auxz00_6565;

							BgL_auxz00_6565 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
								BINT(21691L), BGl_string2864z00zz__objectz00,
								BGl_string2862z00zz__objectz00, BgL_classz00_5052);
							FAILURE(BgL_auxz00_6565, BFALSE, BFALSE);
						}
					BgL_tmpz00_6561 = BGl_classzd2indexzd2zz__objectz00(BgL_auxz00_6562);
				}
				return BINT(BgL_tmpz00_6561);
			}
		}

	}



/* class-num */
	BGL_EXPORTED_DEF long BGl_classzd2numzd2zz__objectz00(obj_t BgL_classz00_28)
	{
		{	/* Llib/object.scm 527 */
			return BGL_CLASS_NUM(BgL_classz00_28);
		}

	}



/* &class-num */
	obj_t BGl_z62classzd2numzb0zz__objectz00(obj_t BgL_envz00_5053,
		obj_t BgL_classz00_5054)
	{
		{	/* Llib/object.scm 527 */
			{	/* Llib/object.scm 528 */
				long BgL_tmpz00_6572;

				{	/* Llib/object.scm 528 */
					obj_t BgL_auxz00_6573;

					if (BGl_classzf3zf3zz__objectz00(BgL_classz00_5054))
						{	/* Llib/object.scm 528 */
							BgL_auxz00_6573 = BgL_classz00_5054;
						}
					else
						{
							obj_t BgL_auxz00_6576;

							BgL_auxz00_6576 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
								BINT(21972L), BGl_string2865z00zz__objectz00,
								BGl_string2862z00zz__objectz00, BgL_classz00_5054);
							FAILURE(BgL_auxz00_6576, BFALSE, BFALSE);
						}
					BgL_tmpz00_6572 = BGl_classzd2numzd2zz__objectz00(BgL_auxz00_6573);
				}
				return BINT(BgL_tmpz00_6572);
			}
		}

	}



/* class-virtual */
	BGL_EXPORTED_DEF obj_t BGl_classzd2virtualzd2zz__objectz00(obj_t
		BgL_classz00_29)
	{
		{	/* Llib/object.scm 533 */
			return BGL_CLASS_VIRTUAL_FIELDS(BgL_classz00_29);
		}

	}



/* &class-virtual */
	obj_t BGl_z62classzd2virtualzb0zz__objectz00(obj_t BgL_envz00_5055,
		obj_t BgL_classz00_5056)
	{
		{	/* Llib/object.scm 533 */
			{	/* Llib/object.scm 534 */
				obj_t BgL_auxz00_6583;

				if (BGl_classzf3zf3zz__objectz00(BgL_classz00_5056))
					{	/* Llib/object.scm 534 */
						BgL_auxz00_6583 = BgL_classz00_5056;
					}
				else
					{
						obj_t BgL_auxz00_6586;

						BgL_auxz00_6586 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(22255L), BGl_string2866z00zz__objectz00,
							BGl_string2862z00zz__objectz00, BgL_classz00_5056);
						FAILURE(BgL_auxz00_6586, BFALSE, BFALSE);
					}
				return BGl_classzd2virtualzd2zz__objectz00(BgL_auxz00_6583);
			}
		}

	}



/* class-evdata */
	BGL_EXPORTED_DEF obj_t BGl_classzd2evdatazd2zz__objectz00(obj_t
		BgL_classz00_30)
	{
		{	/* Llib/object.scm 539 */
			return BGL_CLASS_EVDATA(BgL_classz00_30);
		}

	}



/* &class-evdata */
	obj_t BGl_z62classzd2evdatazb0zz__objectz00(obj_t BgL_envz00_5057,
		obj_t BgL_classz00_5058)
	{
		{	/* Llib/object.scm 539 */
			{	/* Llib/object.scm 540 */
				obj_t BgL_auxz00_6592;

				if (BGl_classzf3zf3zz__objectz00(BgL_classz00_5058))
					{	/* Llib/object.scm 540 */
						BgL_auxz00_6592 = BgL_classz00_5058;
					}
				else
					{
						obj_t BgL_auxz00_6595;

						BgL_auxz00_6595 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(22541L), BGl_string2867z00zz__objectz00,
							BGl_string2862z00zz__objectz00, BgL_classz00_5058);
						FAILURE(BgL_auxz00_6595, BFALSE, BFALSE);
					}
				return BGl_classzd2evdatazd2zz__objectz00(BgL_auxz00_6592);
			}
		}

	}



/* class-evdata-set! */
	BGL_EXPORTED_DEF obj_t BGl_classzd2evdatazd2setz12z12zz__objectz00(obj_t
		BgL_classz00_31, obj_t BgL_dataz00_32)
	{
		{	/* Llib/object.scm 545 */
			return BGL_CLASS_EVDATA_SET(BgL_classz00_31, BgL_dataz00_32);
		}

	}



/* &class-evdata-set! */
	obj_t BGl_z62classzd2evdatazd2setz12z70zz__objectz00(obj_t BgL_envz00_5059,
		obj_t BgL_classz00_5060, obj_t BgL_dataz00_5061)
	{
		{	/* Llib/object.scm 545 */
			{	/* Llib/object.scm 546 */
				obj_t BgL_auxz00_6601;

				if (BGl_classzf3zf3zz__objectz00(BgL_classz00_5060))
					{	/* Llib/object.scm 546 */
						BgL_auxz00_6601 = BgL_classz00_5060;
					}
				else
					{
						obj_t BgL_auxz00_6604;

						BgL_auxz00_6604 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(22829L), BGl_string2868z00zz__objectz00,
							BGl_string2862z00zz__objectz00, BgL_classz00_5060);
						FAILURE(BgL_auxz00_6604, BFALSE, BFALSE);
					}
				return
					BGl_classzd2evdatazd2setz12z12zz__objectz00(BgL_auxz00_6601,
					BgL_dataz00_5061);
			}
		}

	}



/* class-evfields-set! */
	BGL_EXPORTED_DEF obj_t BGl_classzd2evfieldszd2setz12z12zz__objectz00(obj_t
		BgL_classz00_33, obj_t BgL_fieldsz00_34)
	{
		{	/* Llib/object.scm 551 */
			{	/* Llib/object.scm 553 */
				bool_t BgL_test3535z00_6609;

				{	/* Llib/object.scm 553 */
					bool_t BgL_res2532z00_3017;

					if (BGL_CLASSP(BgL_classz00_33))
						{	/* Llib/object.scm 504 */
							BgL_res2532z00_3017 = CBOOL(BGL_CLASS_EVDATA(BgL_classz00_33));
						}
					else
						{	/* Llib/object.scm 504 */
							BgL_res2532z00_3017 = ((bool_t) 0);
						}
					BgL_test3535z00_6609 = BgL_res2532z00_3017;
				}
				if (BgL_test3535z00_6609)
					{	/* Llib/object.scm 555 */
						bool_t BgL_test3537z00_6614;

						{	/* Llib/object.scm 555 */
							long BgL_arg1509z00_1314;

							{	/* Llib/object.scm 555 */
								obj_t BgL_arg1510z00_1315;

								BgL_arg1510z00_1315 = BGL_CLASS_DIRECT_FIELDS(BgL_classz00_33);
								BgL_arg1509z00_1314 = VECTOR_LENGTH(BgL_arg1510z00_1315);
							}
							BgL_test3537z00_6614 = (BgL_arg1509z00_1314 > 0L);
						}
						if (BgL_test3537z00_6614)
							{	/* Llib/object.scm 555 */
								return
									BGl_errorz00zz__errorz00(BGl_string2869z00zz__objectz00,
									BGl_string2870z00zz__objectz00, BgL_classz00_33);
							}
						else
							{	/* Llib/object.scm 558 */
								obj_t BgL_sfieldsz00_1310;

								{	/* Llib/object.scm 558 */
									obj_t BgL_arg1508z00_1313;

									BgL_arg1508z00_1313 = BGL_CLASS_SUPER(BgL_classz00_33);
									{	/* Llib/object.scm 572 */
										obj_t BgL_tmpz00_6620;

										BgL_tmpz00_6620 = ((obj_t) BgL_arg1508z00_1313);
										BgL_sfieldsz00_1310 = BGL_CLASS_ALL_FIELDS(BgL_tmpz00_6620);
									}
								}
								BGL_CLASS_DIRECT_FIELDS_SET(BgL_classz00_33, BgL_fieldsz00_34);
								{	/* Llib/object.scm 560 */
									obj_t BgL_arg1506z00_1311;

									{	/* Llib/object.scm 560 */
										obj_t BgL_list1507z00_1312;

										BgL_list1507z00_1312 =
											MAKE_YOUNG_PAIR(BgL_fieldsz00_34, BNIL);
										BgL_arg1506z00_1311 =
											BGl_vectorzd2appendzd2zz__r4_vectors_6_8z00
											(BgL_sfieldsz00_1310, BgL_list1507z00_1312);
									}
									return
										BGL_CLASS_ALL_FIELDS_SET(BgL_classz00_33,
										BgL_arg1506z00_1311);
								}
							}
					}
				else
					{	/* Llib/object.scm 553 */
						return
							BGl_errorz00zz__errorz00(BGl_string2869z00zz__objectz00,
							BGl_string2871z00zz__objectz00, BgL_classz00_33);
					}
			}
		}

	}



/* &class-evfields-set! */
	obj_t BGl_z62classzd2evfieldszd2setz12z70zz__objectz00(obj_t BgL_envz00_5062,
		obj_t BgL_classz00_5063, obj_t BgL_fieldsz00_5064)
	{
		{	/* Llib/object.scm 551 */
			{	/* Llib/object.scm 553 */
				obj_t BgL_auxz00_6635;
				obj_t BgL_auxz00_6628;

				if (VECTORP(BgL_fieldsz00_5064))
					{	/* Llib/object.scm 553 */
						BgL_auxz00_6635 = BgL_fieldsz00_5064;
					}
				else
					{
						obj_t BgL_auxz00_6638;

						BgL_auxz00_6638 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(23143L), BGl_string2872z00zz__objectz00,
							BGl_string2873z00zz__objectz00, BgL_fieldsz00_5064);
						FAILURE(BgL_auxz00_6638, BFALSE, BFALSE);
					}
				if (BGl_classzf3zf3zz__objectz00(BgL_classz00_5063))
					{	/* Llib/object.scm 553 */
						BgL_auxz00_6628 = BgL_classz00_5063;
					}
				else
					{
						obj_t BgL_auxz00_6631;

						BgL_auxz00_6631 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(23143L), BGl_string2872z00zz__objectz00,
							BGl_string2862z00zz__objectz00, BgL_classz00_5063);
						FAILURE(BgL_auxz00_6631, BFALSE, BFALSE);
					}
				return
					BGl_classzd2evfieldszd2setz12z12zz__objectz00(BgL_auxz00_6628,
					BgL_auxz00_6635);
			}
		}

	}



/* class-fields */
	BGL_EXPORTED_DEF obj_t BGl_classzd2fieldszd2zz__objectz00(obj_t
		BgL_classz00_35)
	{
		{	/* Llib/object.scm 565 */
			return BGL_CLASS_DIRECT_FIELDS(BgL_classz00_35);
		}

	}



/* &class-fields */
	obj_t BGl_z62classzd2fieldszb0zz__objectz00(obj_t BgL_envz00_5065,
		obj_t BgL_classz00_5066)
	{
		{	/* Llib/object.scm 565 */
			{	/* Llib/object.scm 566 */
				obj_t BgL_auxz00_6644;

				if (BGl_classzf3zf3zz__objectz00(BgL_classz00_5066))
					{	/* Llib/object.scm 566 */
						BgL_auxz00_6644 = BgL_classz00_5066;
					}
				else
					{
						obj_t BgL_auxz00_6647;

						BgL_auxz00_6647 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(23794L), BGl_string2874z00zz__objectz00,
							BGl_string2862z00zz__objectz00, BgL_classz00_5066);
						FAILURE(BgL_auxz00_6647, BFALSE, BFALSE);
					}
				return BGl_classzd2fieldszd2zz__objectz00(BgL_auxz00_6644);
			}
		}

	}



/* class-all-fields */
	BGL_EXPORTED_DEF obj_t BGl_classzd2allzd2fieldsz00zz__objectz00(obj_t
		BgL_classz00_36)
	{
		{	/* Llib/object.scm 571 */
			return BGL_CLASS_ALL_FIELDS(BgL_classz00_36);
		}

	}



/* &class-all-fields */
	obj_t BGl_z62classzd2allzd2fieldsz62zz__objectz00(obj_t BgL_envz00_5067,
		obj_t BgL_classz00_5068)
	{
		{	/* Llib/object.scm 571 */
			{	/* Llib/object.scm 572 */
				obj_t BgL_auxz00_6653;

				if (BGl_classzf3zf3zz__objectz00(BgL_classz00_5068))
					{	/* Llib/object.scm 572 */
						BgL_auxz00_6653 = BgL_classz00_5068;
					}
				else
					{
						obj_t BgL_auxz00_6656;

						BgL_auxz00_6656 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(24090L), BGl_string2875z00zz__objectz00,
							BGl_string2862z00zz__objectz00, BgL_classz00_5068);
						FAILURE(BgL_auxz00_6656, BFALSE, BFALSE);
					}
				return BGl_classzd2allzd2fieldsz00zz__objectz00(BgL_auxz00_6653);
			}
		}

	}



/* find-class-field */
	BGL_EXPORTED_DEF obj_t BGl_findzd2classzd2fieldz00zz__objectz00(obj_t
		BgL_classz00_37, obj_t BgL_namez00_38)
	{
		{	/* Llib/object.scm 577 */
			{	/* Llib/object.scm 578 */
				obj_t BgL_fieldsz00_1316;

				BgL_fieldsz00_1316 = BGL_CLASS_ALL_FIELDS(BgL_classz00_37);
				{	/* Llib/object.scm 579 */
					long BgL_g1215z00_1317;

					BgL_g1215z00_1317 = (VECTOR_LENGTH(BgL_fieldsz00_1316) - 1L);
					{
						long BgL_iz00_1319;

						BgL_iz00_1319 = BgL_g1215z00_1317;
					BgL_zc3z04anonymousza31511ze3z87_1320:
						if ((BgL_iz00_1319 == -1L))
							{	/* Llib/object.scm 580 */
								return BFALSE;
							}
						else
							{	/* Llib/object.scm 582 */
								obj_t BgL_fz00_1322;

								BgL_fz00_1322 = VECTOR_REF(BgL_fieldsz00_1316, BgL_iz00_1319);
								{	/* Llib/object.scm 583 */
									bool_t BgL_test3543z00_6667;

									{	/* Llib/object.scm 583 */
										obj_t BgL_arg1517z00_1326;

										{	/* Llib/object.scm 583 */
											obj_t BgL_res2533z00_3031;

											{	/* Llib/object.scm 606 */
												obj_t BgL_vectorz00_3030;

												BgL_vectorz00_3030 = ((obj_t) BgL_fz00_1322);
												BgL_res2533z00_3031 =
													VECTOR_REF(BgL_vectorz00_3030, 0L);
											}
											BgL_arg1517z00_1326 = BgL_res2533z00_3031;
										}
										BgL_test3543z00_6667 =
											(BgL_arg1517z00_1326 == BgL_namez00_38);
									}
									if (BgL_test3543z00_6667)
										{	/* Llib/object.scm 583 */
											return BgL_fz00_1322;
										}
									else
										{
											long BgL_iz00_6671;

											BgL_iz00_6671 = (BgL_iz00_1319 - 1L);
											BgL_iz00_1319 = BgL_iz00_6671;
											goto BgL_zc3z04anonymousza31511ze3z87_1320;
										}
								}
							}
					}
				}
			}
		}

	}



/* &find-class-field */
	obj_t BGl_z62findzd2classzd2fieldz62zz__objectz00(obj_t BgL_envz00_5069,
		obj_t BgL_classz00_5070, obj_t BgL_namez00_5071)
	{
		{	/* Llib/object.scm 577 */
			{	/* Llib/object.scm 578 */
				obj_t BgL_auxz00_6680;
				obj_t BgL_auxz00_6673;

				if (SYMBOLP(BgL_namez00_5071))
					{	/* Llib/object.scm 578 */
						BgL_auxz00_6680 = BgL_namez00_5071;
					}
				else
					{
						obj_t BgL_auxz00_6683;

						BgL_auxz00_6683 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(24389L), BGl_string2876z00zz__objectz00,
							BGl_string2855z00zz__objectz00, BgL_namez00_5071);
						FAILURE(BgL_auxz00_6683, BFALSE, BFALSE);
					}
				if (BGl_classzf3zf3zz__objectz00(BgL_classz00_5070))
					{	/* Llib/object.scm 578 */
						BgL_auxz00_6673 = BgL_classz00_5070;
					}
				else
					{
						obj_t BgL_auxz00_6676;

						BgL_auxz00_6676 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(24389L), BGl_string2876z00zz__objectz00,
							BGl_string2862z00zz__objectz00, BgL_classz00_5070);
						FAILURE(BgL_auxz00_6676, BFALSE, BFALSE);
					}
				return
					BGl_findzd2classzd2fieldz00zz__objectz00(BgL_auxz00_6673,
					BgL_auxz00_6680);
			}
		}

	}



/* make-class-field */
	BGL_EXPORTED_DEF obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t
		BgL_namez00_39, obj_t BgL_getterz00_40, obj_t BgL_setterz00_41,
		bool_t BgL_ronlyz00_42, bool_t BgL_virtualz00_43, obj_t BgL_infoz00_44,
		obj_t BgL_defaultz00_45, obj_t BgL_typez00_46)
	{
		{	/* Llib/object.scm 590 */
			{	/* Llib/object.scm 591 */
				obj_t BgL_v1316z00_3033;

				BgL_v1316z00_3033 = create_vector(9L);
				VECTOR_SET(BgL_v1316z00_3033, 0L, BgL_namez00_39);
				VECTOR_SET(BgL_v1316z00_3033, 1L, BgL_getterz00_40);
				VECTOR_SET(BgL_v1316z00_3033, 2L, BgL_setterz00_41);
				VECTOR_SET(BgL_v1316z00_3033, 3L, BBOOL(BgL_virtualz00_43));
				VECTOR_SET(BgL_v1316z00_3033, 4L,
					BGl_makezd2classzd2fieldzd2envzd2zz__objectz00);
				VECTOR_SET(BgL_v1316z00_3033, 5L, BgL_infoz00_44);
				VECTOR_SET(BgL_v1316z00_3033, 6L, BgL_defaultz00_45);
				VECTOR_SET(BgL_v1316z00_3033, 7L, BgL_typez00_46);
				{
					obj_t BgL_auxz00_6698;

					{	/* Llib/object.scm 591 */
						bool_t BgL_tmpz00_6699;

						if (BgL_ronlyz00_42)
							{	/* Llib/object.scm 592 */
								BgL_tmpz00_6699 = ((bool_t) 0);
							}
						else
							{	/* Llib/object.scm 592 */
								BgL_tmpz00_6699 = ((bool_t) 1);
							}
						BgL_auxz00_6698 = BBOOL(BgL_tmpz00_6699);
					}
					VECTOR_SET(BgL_v1316z00_3033, 8L, BgL_auxz00_6698);
				}
				return BgL_v1316z00_3033;
			}
		}

	}



/* &make-class-field */
	obj_t BGl_z62makezd2classzd2fieldz62zz__objectz00(obj_t BgL_envz00_5072,
		obj_t BgL_namez00_5073, obj_t BgL_getterz00_5074, obj_t BgL_setterz00_5075,
		obj_t BgL_ronlyz00_5076, obj_t BgL_virtualz00_5077, obj_t BgL_infoz00_5078,
		obj_t BgL_defaultz00_5079, obj_t BgL_typez00_5080)
	{
		{	/* Llib/object.scm 590 */
			{	/* Llib/object.scm 591 */
				obj_t BgL_auxz00_6703;

				if (SYMBOLP(BgL_namez00_5073))
					{	/* Llib/object.scm 591 */
						BgL_auxz00_6703 = BgL_namez00_5073;
					}
				else
					{
						obj_t BgL_auxz00_6706;

						BgL_auxz00_6706 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(24930L), BGl_string2877z00zz__objectz00,
							BGl_string2855z00zz__objectz00, BgL_namez00_5073);
						FAILURE(BgL_auxz00_6706, BFALSE, BFALSE);
					}
				return
					BGl_makezd2classzd2fieldz00zz__objectz00(BgL_auxz00_6703,
					BgL_getterz00_5074, BgL_setterz00_5075, CBOOL(BgL_ronlyz00_5076),
					CBOOL(BgL_virtualz00_5077), BgL_infoz00_5078, BgL_defaultz00_5079,
					BgL_typez00_5080);
			}
		}

	}



/* class-field? */
	BGL_EXPORTED_DEF bool_t BGl_classzd2fieldzf3z21zz__objectz00(obj_t
		BgL_objz00_47)
	{
		{	/* Llib/object.scm 597 */
			if (VECTORP(BgL_objz00_47))
				{	/* Llib/object.scm 598 */
					if ((VECTOR_LENGTH(BgL_objz00_47) == 9L))
						{	/* Llib/object.scm 599 */
							return
								(VECTOR_REF(BgL_objz00_47,
									4L) == BGl_makezd2classzd2fieldzd2envzd2zz__objectz00);
						}
					else
						{	/* Llib/object.scm 599 */
							return ((bool_t) 0);
						}
				}
			else
				{	/* Llib/object.scm 598 */
					return ((bool_t) 0);
				}
		}

	}



/* &class-field? */
	obj_t BGl_z62classzd2fieldzf3z43zz__objectz00(obj_t BgL_envz00_5081,
		obj_t BgL_objz00_5082)
	{
		{	/* Llib/object.scm 597 */
			return BBOOL(BGl_classzd2fieldzf3z21zz__objectz00(BgL_objz00_5082));
		}

	}



/* class-field-name */
	BGL_EXPORTED_DEF obj_t BGl_classzd2fieldzd2namez00zz__objectz00(obj_t
		BgL_fieldz00_48)
	{
		{	/* Llib/object.scm 605 */
			return VECTOR_REF(BgL_fieldz00_48, 0L);
		}

	}



/* &class-field-name */
	obj_t BGl_z62classzd2fieldzd2namez62zz__objectz00(obj_t BgL_envz00_5083,
		obj_t BgL_fieldz00_5084)
	{
		{	/* Llib/object.scm 605 */
			{	/* Llib/object.scm 606 */
				obj_t BgL_auxz00_6723;

				if (BGl_classzd2fieldzf3z21zz__objectz00(BgL_fieldz00_5084))
					{	/* Llib/object.scm 606 */
						BgL_auxz00_6723 = BgL_fieldz00_5084;
					}
				else
					{
						obj_t BgL_auxz00_6726;

						BgL_auxz00_6726 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(25638L), BGl_string2878z00zz__objectz00,
							BGl_string2879z00zz__objectz00, BgL_fieldz00_5084);
						FAILURE(BgL_auxz00_6726, BFALSE, BFALSE);
					}
				return BGl_classzd2fieldzd2namez00zz__objectz00(BgL_auxz00_6723);
			}
		}

	}



/* class-field-virtual? */
	BGL_EXPORTED_DEF bool_t BGl_classzd2fieldzd2virtualzf3zf3zz__objectz00(obj_t
		BgL_fieldz00_49)
	{
		{	/* Llib/object.scm 611 */
			{	/* Llib/object.scm 612 */
				obj_t BgL_vectorz00_3048;

				BgL_vectorz00_3048 = BgL_fieldz00_49;
				return CBOOL(VECTOR_REF(BgL_vectorz00_3048, 3L));
			}
		}

	}



/* &class-field-virtual? */
	obj_t BGl_z62classzd2fieldzd2virtualzf3z91zz__objectz00(obj_t BgL_envz00_5085,
		obj_t BgL_fieldz00_5086)
	{
		{	/* Llib/object.scm 611 */
			{	/* Llib/object.scm 612 */
				bool_t BgL_tmpz00_6733;

				{	/* Llib/object.scm 612 */
					obj_t BgL_auxz00_6734;

					if (BGl_classzd2fieldzf3z21zz__objectz00(BgL_fieldz00_5086))
						{	/* Llib/object.scm 612 */
							BgL_auxz00_6734 = BgL_fieldz00_5086;
						}
					else
						{
							obj_t BgL_auxz00_6737;

							BgL_auxz00_6737 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
								BINT(25932L), BGl_string2880z00zz__objectz00,
								BGl_string2879z00zz__objectz00, BgL_fieldz00_5086);
							FAILURE(BgL_auxz00_6737, BFALSE, BFALSE);
						}
					BgL_tmpz00_6733 =
						BGl_classzd2fieldzd2virtualzf3zf3zz__objectz00(BgL_auxz00_6734);
				}
				return BBOOL(BgL_tmpz00_6733);
			}
		}

	}



/* class-field-accessor */
	BGL_EXPORTED_DEF obj_t BGl_classzd2fieldzd2accessorz00zz__objectz00(obj_t
		BgL_fieldz00_50)
	{
		{	/* Llib/object.scm 617 */
			return VECTOR_REF(BgL_fieldz00_50, 1L);
		}

	}



/* &class-field-accessor */
	obj_t BGl_z62classzd2fieldzd2accessorz62zz__objectz00(obj_t BgL_envz00_5087,
		obj_t BgL_fieldz00_5088)
	{
		{	/* Llib/object.scm 617 */
			{	/* Llib/object.scm 618 */
				obj_t BgL_auxz00_6744;

				if (BGl_classzd2fieldzf3z21zz__objectz00(BgL_fieldz00_5088))
					{	/* Llib/object.scm 618 */
						BgL_auxz00_6744 = BgL_fieldz00_5088;
					}
				else
					{
						obj_t BgL_auxz00_6747;

						BgL_auxz00_6747 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(26231L), BGl_string2881z00zz__objectz00,
							BGl_string2879z00zz__objectz00, BgL_fieldz00_5088);
						FAILURE(BgL_auxz00_6747, BFALSE, BFALSE);
					}
				return BGl_classzd2fieldzd2accessorz00zz__objectz00(BgL_auxz00_6744);
			}
		}

	}



/* class-field-mutable? */
	BGL_EXPORTED_DEF bool_t BGl_classzd2fieldzd2mutablezf3zf3zz__objectz00(obj_t
		BgL_fieldz00_51)
	{
		{	/* Llib/object.scm 623 */
			{	/* Llib/object.scm 624 */
				obj_t BgL_vectorz00_3050;

				BgL_vectorz00_3050 = BgL_fieldz00_51;
				return CBOOL(VECTOR_REF(BgL_vectorz00_3050, 8L));
			}
		}

	}



/* &class-field-mutable? */
	obj_t BGl_z62classzd2fieldzd2mutablezf3z91zz__objectz00(obj_t BgL_envz00_5089,
		obj_t BgL_fieldz00_5090)
	{
		{	/* Llib/object.scm 623 */
			{	/* Llib/object.scm 624 */
				bool_t BgL_tmpz00_6754;

				{	/* Llib/object.scm 624 */
					obj_t BgL_auxz00_6755;

					if (BGl_classzd2fieldzf3z21zz__objectz00(BgL_fieldz00_5090))
						{	/* Llib/object.scm 624 */
							BgL_auxz00_6755 = BgL_fieldz00_5090;
						}
					else
						{
							obj_t BgL_auxz00_6758;

							BgL_auxz00_6758 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
								BINT(26525L), BGl_string2882z00zz__objectz00,
								BGl_string2879z00zz__objectz00, BgL_fieldz00_5090);
							FAILURE(BgL_auxz00_6758, BFALSE, BFALSE);
						}
					BgL_tmpz00_6754 =
						BGl_classzd2fieldzd2mutablezf3zf3zz__objectz00(BgL_auxz00_6755);
				}
				return BBOOL(BgL_tmpz00_6754);
			}
		}

	}



/* class-field-mutator */
	BGL_EXPORTED_DEF obj_t BGl_classzd2fieldzd2mutatorz00zz__objectz00(obj_t
		BgL_fieldz00_52)
	{
		{	/* Llib/object.scm 629 */
			return VECTOR_REF(BgL_fieldz00_52, 2L);
		}

	}



/* &class-field-mutator */
	obj_t BGl_z62classzd2fieldzd2mutatorz62zz__objectz00(obj_t BgL_envz00_5091,
		obj_t BgL_fieldz00_5092)
	{
		{	/* Llib/object.scm 629 */
			{	/* Llib/object.scm 630 */
				obj_t BgL_auxz00_6765;

				if (BGl_classzd2fieldzf3z21zz__objectz00(BgL_fieldz00_5092))
					{	/* Llib/object.scm 630 */
						BgL_auxz00_6765 = BgL_fieldz00_5092;
					}
				else
					{
						obj_t BgL_auxz00_6768;

						BgL_auxz00_6768 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(26823L), BGl_string2883z00zz__objectz00,
							BGl_string2879z00zz__objectz00, BgL_fieldz00_5092);
						FAILURE(BgL_auxz00_6768, BFALSE, BFALSE);
					}
				return BGl_classzd2fieldzd2mutatorz00zz__objectz00(BgL_auxz00_6765);
			}
		}

	}



/* class-field-info */
	BGL_EXPORTED_DEF obj_t BGl_classzd2fieldzd2infoz00zz__objectz00(obj_t
		BgL_fieldz00_53)
	{
		{	/* Llib/object.scm 635 */
			return VECTOR_REF(BgL_fieldz00_53, 5L);
		}

	}



/* &class-field-info */
	obj_t BGl_z62classzd2fieldzd2infoz62zz__objectz00(obj_t BgL_envz00_5093,
		obj_t BgL_fieldz00_5094)
	{
		{	/* Llib/object.scm 635 */
			{	/* Llib/object.scm 636 */
				obj_t BgL_auxz00_6774;

				if (BGl_classzd2fieldzf3z21zz__objectz00(BgL_fieldz00_5094))
					{	/* Llib/object.scm 636 */
						BgL_auxz00_6774 = BgL_fieldz00_5094;
					}
				else
					{
						obj_t BgL_auxz00_6777;

						BgL_auxz00_6777 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(27107L), BGl_string2884z00zz__objectz00,
							BGl_string2879z00zz__objectz00, BgL_fieldz00_5094);
						FAILURE(BgL_auxz00_6777, BFALSE, BFALSE);
					}
				return BGl_classzd2fieldzd2infoz00zz__objectz00(BgL_auxz00_6774);
			}
		}

	}



/* class-field-default-value? */
	BGL_EXPORTED_DEF bool_t
		BGl_classzd2fieldzd2defaultzd2valuezf3z21zz__objectz00(obj_t
		BgL_fieldz00_54)
	{
		{	/* Llib/object.scm 641 */
			{	/* Llib/object.scm 642 */
				obj_t BgL_tmpz00_6782;

				BgL_tmpz00_6782 = VECTOR_REF(BgL_fieldz00_54, 6L);
				return PROCEDUREP(BgL_tmpz00_6782);
			}
		}

	}



/* &class-field-default-value? */
	obj_t BGl_z62classzd2fieldzd2defaultzd2valuezf3z43zz__objectz00(obj_t
		BgL_envz00_5095, obj_t BgL_fieldz00_5096)
	{
		{	/* Llib/object.scm 641 */
			{	/* Llib/object.scm 642 */
				bool_t BgL_tmpz00_6785;

				{	/* Llib/object.scm 642 */
					obj_t BgL_auxz00_6786;

					if (BGl_classzd2fieldzf3z21zz__objectz00(BgL_fieldz00_5096))
						{	/* Llib/object.scm 642 */
							BgL_auxz00_6786 = BgL_fieldz00_5096;
						}
					else
						{
							obj_t BgL_auxz00_6789;

							BgL_auxz00_6789 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
								BINT(27413L), BGl_string2885z00zz__objectz00,
								BGl_string2879z00zz__objectz00, BgL_fieldz00_5096);
							FAILURE(BgL_auxz00_6789, BFALSE, BFALSE);
						}
					BgL_tmpz00_6785 =
						BGl_classzd2fieldzd2defaultzd2valuezf3z21zz__objectz00
						(BgL_auxz00_6786);
				}
				return BBOOL(BgL_tmpz00_6785);
			}
		}

	}



/* class-field-default-value */
	BGL_EXPORTED_DEF obj_t
		BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(obj_t BgL_fieldz00_55)
	{
		{	/* Llib/object.scm 647 */
			{	/* Llib/object.scm 650 */
				obj_t BgL_pz00_3055;

				BgL_pz00_3055 = VECTOR_REF(BgL_fieldz00_55, 6L);
				if (PROCEDUREP(BgL_pz00_3055))
					{	/* Llib/object.scm 651 */
						return BGL_PROCEDURE_CALL0(BgL_pz00_3055);
					}
				else
					{	/* Llib/object.scm 651 */
						return
							BGl_errorz00zz__errorz00(BGl_string2886z00zz__objectz00,
							BGl_string2887z00zz__objectz00, VECTOR_REF(BgL_fieldz00_55, 0L));
					}
			}
		}

	}



/* &class-field-default-value */
	obj_t BGl_z62classzd2fieldzd2defaultzd2valuezb0zz__objectz00(obj_t
		BgL_envz00_5097, obj_t BgL_fieldz00_5098)
	{
		{	/* Llib/object.scm 647 */
			{	/* Llib/object.scm 650 */
				obj_t BgL_auxz00_6803;

				if (BGl_classzd2fieldzf3z21zz__objectz00(BgL_fieldz00_5098))
					{	/* Llib/object.scm 650 */
						BgL_auxz00_6803 = BgL_fieldz00_5098;
					}
				else
					{
						obj_t BgL_auxz00_6806;

						BgL_auxz00_6806 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(27799L), BGl_string2888z00zz__objectz00,
							BGl_string2879z00zz__objectz00, BgL_fieldz00_5098);
						FAILURE(BgL_auxz00_6806, BFALSE, BFALSE);
					}
				return
					BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_auxz00_6803);
			}
		}

	}



/* class-field-type */
	BGL_EXPORTED_DEF obj_t BGl_classzd2fieldzd2typez00zz__objectz00(obj_t
		BgL_fieldz00_56)
	{
		{	/* Llib/object.scm 660 */
			return VECTOR_REF(BgL_fieldz00_56, 7L);
		}

	}



/* &class-field-type */
	obj_t BGl_z62classzd2fieldzd2typez62zz__objectz00(obj_t BgL_envz00_5099,
		obj_t BgL_fieldz00_5100)
	{
		{	/* Llib/object.scm 660 */
			{	/* Llib/object.scm 661 */
				obj_t BgL_auxz00_6812;

				if (BGl_classzd2fieldzf3z21zz__objectz00(BgL_fieldz00_5100))
					{	/* Llib/object.scm 661 */
						BgL_auxz00_6812 = BgL_fieldz00_5100;
					}
				else
					{
						obj_t BgL_auxz00_6815;

						BgL_auxz00_6815 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(28238L), BGl_string2889z00zz__objectz00,
							BGl_string2879z00zz__objectz00, BgL_fieldz00_5100);
						FAILURE(BgL_auxz00_6815, BFALSE, BFALSE);
					}
				return BGl_classzd2fieldzd2typez00zz__objectz00(BgL_auxz00_6812);
			}
		}

	}



/* class-super */
	BGL_EXPORTED_DEF obj_t BGl_classzd2superzd2zz__objectz00(obj_t
		BgL_classz00_57)
	{
		{	/* Llib/object.scm 666 */
			return BGL_CLASS_SUPER(BgL_classz00_57);
		}

	}



/* &class-super */
	obj_t BGl_z62classzd2superzb0zz__objectz00(obj_t BgL_envz00_5101,
		obj_t BgL_classz00_5102)
	{
		{	/* Llib/object.scm 666 */
			{	/* Llib/object.scm 667 */
				obj_t BgL_auxz00_6821;

				if (BGl_classzf3zf3zz__objectz00(BgL_classz00_5102))
					{	/* Llib/object.scm 667 */
						BgL_auxz00_6821 = BgL_classz00_5102;
					}
				else
					{
						obj_t BgL_auxz00_6824;

						BgL_auxz00_6824 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(28517L), BGl_string2890z00zz__objectz00,
							BGl_string2862z00zz__objectz00, BgL_classz00_5102);
						FAILURE(BgL_auxz00_6824, BFALSE, BFALSE);
					}
				return BGl_classzd2superzd2zz__objectz00(BgL_auxz00_6821);
			}
		}

	}



/* class-abstract? */
	BGL_EXPORTED_DEF bool_t BGl_classzd2abstractzf3z21zz__objectz00(obj_t
		BgL_classz00_61)
	{
		{	/* Llib/object.scm 684 */
			{	/* Llib/object.scm 685 */
				obj_t BgL_arg1530z00_3064;

				{	/* Llib/object.scm 685 */
					obj_t BgL_res2535z00_3067;

					if (BGL_CLASSP(BgL_classz00_61))
						{	/* Llib/object.scm 709 */
							BgL_res2535z00_3067 = BGL_CLASS_ALLOC_FUN(BgL_classz00_61);
						}
					else
						{	/* Llib/object.scm 709 */
							BgL_res2535z00_3067 =
								BGl_bigloozd2typezd2errorz00zz__errorz00
								(BGl_string2891z00zz__objectz00, BGl_string2862z00zz__objectz00,
								BgL_classz00_61);
						}
					BgL_arg1530z00_3064 = BgL_res2535z00_3067;
				}
				((bool_t) 1);
			}
			return ((bool_t) 0);
		}

	}



/* &class-abstract? */
	obj_t BGl_z62classzd2abstractzf3z43zz__objectz00(obj_t BgL_envz00_5103,
		obj_t BgL_classz00_5104)
	{
		{	/* Llib/object.scm 684 */
			{	/* Llib/object.scm 685 */
				bool_t BgL_tmpz00_6833;

				{	/* Llib/object.scm 685 */
					obj_t BgL_auxz00_6834;

					if (BGl_classzf3zf3zz__objectz00(BgL_classz00_5104))
						{	/* Llib/object.scm 685 */
							BgL_auxz00_6834 = BgL_classz00_5104;
						}
					else
						{
							obj_t BgL_auxz00_6837;

							BgL_auxz00_6837 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
								BINT(29406L), BGl_string2892z00zz__objectz00,
								BGl_string2862z00zz__objectz00, BgL_classz00_5104);
							FAILURE(BgL_auxz00_6837, BFALSE, BFALSE);
						}
					BgL_tmpz00_6833 =
						BGl_classzd2abstractzf3z21zz__objectz00(BgL_auxz00_6834);
				}
				return BBOOL(BgL_tmpz00_6833);
			}
		}

	}



/* class-wide? */
	BGL_EXPORTED_DEF bool_t BGl_classzd2widezf3z21zz__objectz00(obj_t
		BgL_classz00_62)
	{
		{	/* Llib/object.scm 690 */
			{	/* Llib/object.scm 691 */
				obj_t BgL_arg1531z00_3068;

				if (BGL_CLASSP(BgL_classz00_62))
					{	/* Llib/object.scm 765 */
						BgL_arg1531z00_3068 = BGL_CLASS_SHRINK(BgL_classz00_62);
					}
				else
					{	/* Llib/object.scm 765 */
						BgL_arg1531z00_3068 =
							BGl_bigloozd2typezd2errorz00zz__errorz00
							(BGl_string2893z00zz__objectz00, BGl_string2862z00zz__objectz00,
							BgL_classz00_62);
					}
				return PROCEDUREP(BgL_arg1531z00_3068);
			}
		}

	}



/* &class-wide? */
	obj_t BGl_z62classzd2widezf3z43zz__objectz00(obj_t BgL_envz00_5105,
		obj_t BgL_classz00_5106)
	{
		{	/* Llib/object.scm 690 */
			{	/* Llib/object.scm 691 */
				bool_t BgL_tmpz00_6848;

				{	/* Llib/object.scm 691 */
					obj_t BgL_auxz00_6849;

					if (BGl_classzf3zf3zz__objectz00(BgL_classz00_5106))
						{	/* Llib/object.scm 691 */
							BgL_auxz00_6849 = BgL_classz00_5106;
						}
					else
						{
							obj_t BgL_auxz00_6852;

							BgL_auxz00_6852 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
								BINT(29723L), BGl_string2894z00zz__objectz00,
								BGl_string2862z00zz__objectz00, BgL_classz00_5106);
							FAILURE(BgL_auxz00_6852, BFALSE, BFALSE);
						}
					BgL_tmpz00_6848 =
						BGl_classzd2widezf3z21zz__objectz00(BgL_auxz00_6849);
				}
				return BBOOL(BgL_tmpz00_6848);
			}
		}

	}



/* class-subclasses */
	BGL_EXPORTED_DEF obj_t BGl_classzd2subclasseszd2zz__objectz00(obj_t
		BgL_classz00_63)
	{
		{	/* Llib/object.scm 696 */
			return BGL_CLASS_SUBCLASSES(BgL_classz00_63);
		}

	}



/* &class-subclasses */
	obj_t BGl_z62classzd2subclasseszb0zz__objectz00(obj_t BgL_envz00_5107,
		obj_t BgL_classz00_5108)
	{
		{	/* Llib/object.scm 696 */
			{	/* Llib/object.scm 697 */
				obj_t BgL_auxz00_6859;

				if (BGl_classzf3zf3zz__objectz00(BgL_classz00_5108))
					{	/* Llib/object.scm 697 */
						BgL_auxz00_6859 = BgL_classz00_5108;
					}
				else
					{
						obj_t BgL_auxz00_6862;

						BgL_auxz00_6862 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(30012L), BGl_string2895z00zz__objectz00,
							BGl_string2862z00zz__objectz00, BgL_classz00_5108);
						FAILURE(BgL_auxz00_6862, BFALSE, BFALSE);
					}
				return BGl_classzd2subclasseszd2zz__objectz00(BgL_auxz00_6859);
			}
		}

	}



/* class-allocator */
	BGL_EXPORTED_DEF obj_t BGl_classzd2allocatorzd2zz__objectz00(obj_t
		BgL_classz00_66)
	{
		{	/* Llib/object.scm 708 */
			if (BGL_CLASSP(BgL_classz00_66))
				{	/* Llib/object.scm 709 */
					return BGL_CLASS_ALLOC_FUN(BgL_classz00_66);
				}
			else
				{	/* Llib/object.scm 709 */
					return
						BGl_bigloozd2typezd2errorz00zz__errorz00
						(BGl_string2891z00zz__objectz00, BGl_string2862z00zz__objectz00,
						BgL_classz00_66);
				}
		}

	}



/* &class-allocator */
	obj_t BGl_z62classzd2allocatorzb0zz__objectz00(obj_t BgL_envz00_5109,
		obj_t BgL_classz00_5110)
	{
		{	/* Llib/object.scm 708 */
			{	/* Llib/object.scm 709 */
				obj_t BgL_auxz00_6871;

				if (BGl_classzf3zf3zz__objectz00(BgL_classz00_5110))
					{	/* Llib/object.scm 709 */
						BgL_auxz00_6871 = BgL_classz00_5110;
					}
				else
					{
						obj_t BgL_auxz00_6874;

						BgL_auxz00_6874 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(30608L), BGl_string2896z00zz__objectz00,
							BGl_string2862z00zz__objectz00, BgL_classz00_5110);
						FAILURE(BgL_auxz00_6874, BFALSE, BFALSE);
					}
				return BGl_classzd2allocatorzd2zz__objectz00(BgL_auxz00_6871);
			}
		}

	}



/* class-hash */
	BGL_EXPORTED_DEF long BGl_classzd2hashzd2zz__objectz00(obj_t BgL_classz00_67)
	{
		{	/* Llib/object.scm 716 */
			return BGL_CLASS_HASH(BgL_classz00_67);
		}

	}



/* &class-hash */
	obj_t BGl_z62classzd2hashzb0zz__objectz00(obj_t BgL_envz00_5111,
		obj_t BgL_classz00_5112)
	{
		{	/* Llib/object.scm 716 */
			{	/* Llib/object.scm 717 */
				long BgL_tmpz00_6880;

				{	/* Llib/object.scm 717 */
					obj_t BgL_auxz00_6881;

					if (BGl_classzf3zf3zz__objectz00(BgL_classz00_5112))
						{	/* Llib/object.scm 717 */
							BgL_auxz00_6881 = BgL_classz00_5112;
						}
					else
						{
							obj_t BgL_auxz00_6884;

							BgL_auxz00_6884 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
								BINT(30973L), BGl_string2897z00zz__objectz00,
								BGl_string2862z00zz__objectz00, BgL_classz00_5112);
							FAILURE(BgL_auxz00_6884, BFALSE, BFALSE);
						}
					BgL_tmpz00_6880 = BGl_classzd2hashzd2zz__objectz00(BgL_auxz00_6881);
				}
				return BINT(BgL_tmpz00_6880);
			}
		}

	}



/* class-constructor */
	BGL_EXPORTED_DEF obj_t BGl_classzd2constructorzd2zz__objectz00(obj_t
		BgL_classz00_68)
	{
		{	/* Llib/object.scm 722 */
			return BGL_CLASS_CONSTRUCTOR(BgL_classz00_68);
		}

	}



/* &class-constructor */
	obj_t BGl_z62classzd2constructorzb0zz__objectz00(obj_t BgL_envz00_5113,
		obj_t BgL_classz00_5114)
	{
		{	/* Llib/object.scm 722 */
			{	/* Llib/object.scm 723 */
				obj_t BgL_auxz00_6891;

				if (BGl_classzf3zf3zz__objectz00(BgL_classz00_5114))
					{	/* Llib/object.scm 723 */
						BgL_auxz00_6891 = BgL_classz00_5114;
					}
				else
					{
						obj_t BgL_auxz00_6894;

						BgL_auxz00_6894 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(31254L), BGl_string2898z00zz__objectz00,
							BGl_string2862z00zz__objectz00, BgL_classz00_5114);
						FAILURE(BgL_auxz00_6894, BFALSE, BFALSE);
					}
				return BGl_classzd2constructorzd2zz__objectz00(BgL_auxz00_6891);
			}
		}

	}



/* class-creator */
	BGL_EXPORTED_DEF obj_t BGl_classzd2creatorzd2zz__objectz00(obj_t
		BgL_classz00_69)
	{
		{	/* Llib/object.scm 728 */
			return BGL_CLASS_NEW_FUN(BgL_classz00_69);
		}

	}



/* &class-creator */
	obj_t BGl_z62classzd2creatorzb0zz__objectz00(obj_t BgL_envz00_5115,
		obj_t BgL_classz00_5116)
	{
		{	/* Llib/object.scm 728 */
			{	/* Llib/object.scm 729 */
				obj_t BgL_auxz00_6900;

				if (BGl_classzf3zf3zz__objectz00(BgL_classz00_5116))
					{	/* Llib/object.scm 729 */
						BgL_auxz00_6900 = BgL_classz00_5116;
					}
				else
					{
						obj_t BgL_auxz00_6903;

						BgL_auxz00_6903 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(31538L), BGl_string2899z00zz__objectz00,
							BGl_string2862z00zz__objectz00, BgL_classz00_5116);
						FAILURE(BgL_auxz00_6903, BFALSE, BFALSE);
					}
				return BGl_classzd2creatorzd2zz__objectz00(BgL_auxz00_6900);
			}
		}

	}



/* class-nil-init! */
	BGL_EXPORTED_DEF obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t
		BgL_classz00_70)
	{
		{	/* Llib/object.scm 734 */
			{	/* Llib/object.scm 735 */
				obj_t BgL_procz00_1344;

				BgL_procz00_1344 = BGL_CLASS_NIL_FUN(BgL_classz00_70);
				{	/* Llib/object.scm 736 */
					bool_t BgL_test3571z00_6909;

					{	/* Llib/object.scm 691 */
						obj_t BgL_tmpz00_6910;

						BgL_tmpz00_6910 =
							BGl_classzd2shrinkzd2zz__objectz00(BgL_classz00_70);
						BgL_test3571z00_6909 = PROCEDUREP(BgL_tmpz00_6910);
					}
					if (BgL_test3571z00_6909)
						{	/* Llib/object.scm 737 */
							obj_t BgL_superz00_1346;

							BgL_superz00_1346 = BGL_CLASS_SUPER(BgL_classz00_70);
							{	/* Llib/object.scm 737 */
								obj_t BgL_oz00_1347;

								{	/* Llib/object.scm 738 */
									obj_t BgL_fun1536z00_1350;

									{	/* Llib/object.scm 738 */
										obj_t BgL_res2536z00_3076;

										{	/* Llib/object.scm 709 */
											bool_t BgL_test3572z00_6914;

											{	/* Llib/object.scm 469 */
												obj_t BgL_tmpz00_6915;

												BgL_tmpz00_6915 = ((obj_t) BgL_superz00_1346);
												BgL_test3572z00_6914 = BGL_CLASSP(BgL_tmpz00_6915);
											}
											if (BgL_test3572z00_6914)
												{	/* Llib/object.scm 710 */
													obj_t BgL_tmpz00_6918;

													BgL_tmpz00_6918 = ((obj_t) BgL_superz00_1346);
													BgL_res2536z00_3076 =
														BGL_CLASS_ALLOC_FUN(BgL_tmpz00_6918);
												}
											else
												{	/* Llib/object.scm 709 */
													BgL_res2536z00_3076 =
														BGl_bigloozd2typezd2errorz00zz__errorz00
														(BGl_string2891z00zz__objectz00,
														BGl_string2862z00zz__objectz00,
														((obj_t) BgL_superz00_1346));
												}
										}
										BgL_fun1536z00_1350 = BgL_res2536z00_3076;
									}
									BgL_oz00_1347 = BGL_PROCEDURE_CALL0(BgL_fun1536z00_1350);
								}
								{	/* Llib/object.scm 738 */
									obj_t BgL_woz00_1348;

									{	/* Llib/object.scm 739 */
										obj_t BgL_fun1534z00_1349;

										{	/* Llib/object.scm 739 */
											obj_t BgL_res2537z00_3079;

											if (BGL_CLASSP(BgL_classz00_70))
												{	/* Llib/object.scm 709 */
													BgL_res2537z00_3079 =
														BGL_CLASS_ALLOC_FUN(BgL_classz00_70);
												}
											else
												{	/* Llib/object.scm 709 */
													BgL_res2537z00_3079 =
														BGl_bigloozd2typezd2errorz00zz__errorz00
														(BGl_string2891z00zz__objectz00,
														BGl_string2862z00zz__objectz00, BgL_classz00_70);
												}
											BgL_fun1534z00_1349 = BgL_res2537z00_3079;
										}
										BgL_woz00_1348 =
											BGL_PROCEDURE_CALL1(BgL_fun1534z00_1349, BgL_oz00_1347);
									}
									{	/* Llib/object.scm 739 */

										BGL_CLASS_NIL_SET(BgL_classz00_70, BgL_woz00_1348);
										BGL_PROCEDURE_CALL1(BgL_procz00_1344, BgL_woz00_1348);
										return BgL_woz00_1348;
									}
								}
							}
						}
					else
						{	/* Llib/object.scm 743 */
							obj_t BgL_oz00_1351;

							{	/* Llib/object.scm 743 */
								obj_t BgL_fun1537z00_1352;

								{	/* Llib/object.scm 743 */
									obj_t BgL_res2538z00_3082;

									if (BGL_CLASSP(BgL_classz00_70))
										{	/* Llib/object.scm 709 */
											BgL_res2538z00_3082 =
												BGL_CLASS_ALLOC_FUN(BgL_classz00_70);
										}
									else
										{	/* Llib/object.scm 709 */
											BgL_res2538z00_3082 =
												BGl_bigloozd2typezd2errorz00zz__errorz00
												(BGl_string2891z00zz__objectz00,
												BGl_string2862z00zz__objectz00, BgL_classz00_70);
										}
									BgL_fun1537z00_1352 = BgL_res2538z00_3082;
								}
								BgL_oz00_1351 = BGL_PROCEDURE_CALL0(BgL_fun1537z00_1352);
							}
							BGL_CLASS_NIL_SET(BgL_classz00_70, BgL_oz00_1351);
							BGL_PROCEDURE_CALL1(BgL_procz00_1344, BgL_oz00_1351);
							return BgL_oz00_1351;
						}
				}
			}
		}

	}



/* &class-nil-init! */
	obj_t BGl_z62classzd2nilzd2initz12z70zz__objectz00(obj_t BgL_envz00_5117,
		obj_t BgL_classz00_5118)
	{
		{	/* Llib/object.scm 734 */
			{	/* Llib/object.scm 735 */
				obj_t BgL_auxz00_6951;

				if (BGl_classzf3zf3zz__objectz00(BgL_classz00_5118))
					{	/* Llib/object.scm 735 */
						BgL_auxz00_6951 = BgL_classz00_5118;
					}
				else
					{
						obj_t BgL_auxz00_6954;

						BgL_auxz00_6954 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(31820L), BGl_string2900z00zz__objectz00,
							BGl_string2862z00zz__objectz00, BgL_classz00_5118);
						FAILURE(BgL_auxz00_6954, BFALSE, BFALSE);
					}
				return BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_auxz00_6951);
			}
		}

	}



/* class-nil */
	BGL_EXPORTED_DEF obj_t BGl_classzd2nilzd2zz__objectz00(obj_t BgL_classz00_71)
	{
		{	/* Llib/object.scm 751 */
			{	/* Llib/object.scm 755 */
				obj_t BgL__ortest_1218z00_5975;

				BgL__ortest_1218z00_5975 = BGL_CLASS_NIL(BgL_classz00_71);
				if (CBOOL(BgL__ortest_1218z00_5975))
					{	/* Llib/object.scm 755 */
						return BgL__ortest_1218z00_5975;
					}
				else
					{	/* Llib/object.scm 755 */
						BGL_TAIL return
							BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_71);
					}
			}
		}

	}



/* &class-nil */
	obj_t BGl_z62classzd2nilzb0zz__objectz00(obj_t BgL_envz00_5119,
		obj_t BgL_classz00_5120)
	{
		{	/* Llib/object.scm 751 */
			{	/* Llib/object.scm 755 */
				obj_t BgL_auxz00_6963;

				if (BGl_classzf3zf3zz__objectz00(BgL_classz00_5120))
					{	/* Llib/object.scm 755 */
						BgL_auxz00_6963 = BgL_classz00_5120;
					}
				else
					{
						obj_t BgL_auxz00_6966;

						BgL_auxz00_6966 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(32495L), BGl_string2901z00zz__objectz00,
							BGl_string2862z00zz__objectz00, BgL_classz00_5120);
						FAILURE(BgL_auxz00_6966, BFALSE, BFALSE);
					}
				return BGl_classzd2nilzd2zz__objectz00(BgL_auxz00_6963);
			}
		}

	}



/* class-shrink */
	obj_t BGl_classzd2shrinkzd2zz__objectz00(obj_t BgL_classz00_72)
	{
		{	/* Llib/object.scm 764 */
			if (BGL_CLASSP(BgL_classz00_72))
				{	/* Llib/object.scm 765 */
					return BGL_CLASS_SHRINK(BgL_classz00_72);
				}
			else
				{	/* Llib/object.scm 765 */
					return
						BGl_bigloozd2typezd2errorz00zz__errorz00
						(BGl_string2893z00zz__objectz00, BGl_string2862z00zz__objectz00,
						BgL_classz00_72);
				}
		}

	}



/* initialize-objects! */
	obj_t BGl_initializa7ezd2objectsz12z67zz__objectz00(void)
	{
		{	/* Llib/object.scm 807 */
			if (INTEGERP(BGl_za2nbzd2classesza2zd2zz__objectz00))
				{	/* Llib/object.scm 808 */
					return BFALSE;
				}
			else
				{	/* Llib/object.scm 808 */
					BGl_za2nbzd2classesza2zd2zz__objectz00 = BINT(0L);
					BGl_za2nbzd2classeszd2maxza2z00zz__objectz00 = BINT(64L);
					BGl_za2classesza2z00zz__objectz00 =
						make_vector_uncollectable(
						(long) CINT(BGl_za2nbzd2classeszd2maxza2z00zz__objectz00), BFALSE);
					BGl_za2inheritancezd2cntza2zd2zz__objectz00 = BINT(0L);
					BGl_za2inheritancezd2maxzd2depthza2z00zz__objectz00 = BINT(128L);
					BGl_za2inheritancesza2z00zz__objectz00 =
						make_vector_uncollectable(256L, BFALSE);
					BGl_za2nbzd2genericszd2maxza2z00zz__objectz00 = BINT(64L);
					BGl_za2nbzd2genericsza2zd2zz__objectz00 = BINT(0L);
					BGl_za2genericsza2z00zz__objectz00 =
						make_vector_uncollectable(
						(long) CINT(BGl_za2nbzd2genericszd2maxza2z00zz__objectz00), BFALSE);
					if (PAIRP(BGl_za2classzd2keyza2zd2zz__objectz00))
						{	/* Llib/object.scm 823 */
							return BFALSE;
						}
					else
						{	/* Llib/object.scm 823 */
							return (BGl_za2classzd2keyza2zd2zz__objectz00 =
								MAKE_YOUNG_PAIR(BINT(1L), BINT(2L)), BUNSPEC);
						}
				}
		}

	}



/* extend-vector */
	obj_t BGl_extendzd2vectorzd2zz__objectz00(obj_t BgL_oldzd2veczd2_73,
		obj_t BgL_fillz00_74, long BgL_extendz00_75)
	{
		{	/* Llib/object.scm 828 */
			{	/* Llib/object.scm 829 */
				long BgL_newzd2lenzd2_3089;

				BgL_newzd2lenzd2_3089 =
					(BgL_extendz00_75 + VECTOR_LENGTH(((obj_t) BgL_oldzd2veczd2_73)));
				{	/* Llib/object.scm 830 */
					obj_t BgL_newzd2veczd2_3090;

					BgL_newzd2veczd2_3090 =
						make_vector_uncollectable(BgL_newzd2lenzd2_3089, BgL_fillz00_74);
					{	/* Llib/object.scm 831 */

						{
							long BgL_iz00_3107;

							BgL_iz00_3107 = 0L;
						BgL_loopz00_3106:
							if (
								(BgL_iz00_3107 == VECTOR_LENGTH(((obj_t) BgL_oldzd2veczd2_73))))
								{	/* Llib/object.scm 833 */
									return BgL_newzd2veczd2_3090;
								}
							else
								{	/* Llib/object.scm 833 */
									{	/* Llib/object.scm 836 */
										obj_t BgL_arg1543z00_3111;

										BgL_arg1543z00_3111 =
											VECTOR_REF(((obj_t) BgL_oldzd2veczd2_73), BgL_iz00_3107);
										VECTOR_SET(BgL_newzd2veczd2_3090, BgL_iz00_3107,
											BgL_arg1543z00_3111);
									}
									{
										long BgL_iz00_7004;

										BgL_iz00_7004 = (BgL_iz00_3107 + 1L);
										BgL_iz00_3107 = BgL_iz00_7004;
										goto BgL_loopz00_3106;
									}
								}
						}
					}
				}
			}
		}

	}



/* double-nb-classes! */
	bool_t BGl_doublezd2nbzd2classesz12z12zz__objectz00(void)
	{
		{	/* Llib/object.scm 850 */
			{	/* Llib/object.scm 851 */
				long BgL_za72za7_3139;

				BgL_za72za7_3139 =
					(long) CINT(BGl_za2nbzd2classeszd2maxza2z00zz__objectz00);
				BGl_za2nbzd2classeszd2maxza2z00zz__objectz00 =
					BINT((2L * BgL_za72za7_3139));
			}
			{	/* Llib/object.scm 852 */
				obj_t BgL_oldzd2veczd2_3140;

				BgL_oldzd2veczd2_3140 = BGl_za2classesza2z00zz__objectz00;
				{	/* Llib/object.scm 843 */
					obj_t BgL_newzd2veczd2_3141;

					BgL_newzd2veczd2_3141 =
						BGl_extendzd2vectorzd2zz__objectz00(BgL_oldzd2veczd2_3140, BFALSE,
						VECTOR_LENGTH(((obj_t) BgL_oldzd2veczd2_3140)));
					FREE_VECTOR_UNCOLLECTABLE(BgL_oldzd2veczd2_3140);
					BUNSPEC;
					BGl_za2classesza2z00zz__objectz00 = BgL_newzd2veczd2_3141;
			}}
			{
				long BgL_iz00_1370;

				BgL_iz00_1370 = 0L;
			BgL_zc3z04anonymousza31547ze3z87_1371:
				if (
					(BgL_iz00_1370 <
						(long) CINT(BGl_za2nbzd2genericsza2zd2zz__objectz00)))
					{	/* Llib/object.scm 856 */
						obj_t BgL_genz00_1373;

						BgL_genz00_1373 =
							VECTOR_REF(BGl_za2genericsza2z00zz__objectz00, BgL_iz00_1370);
						{	/* Llib/object.scm 856 */
							obj_t BgL_defaultzd2bucketzd2_1374;

							BgL_defaultzd2bucketzd2_1374 =
								PROCEDURE_REF(BgL_genz00_1373, (int) (2L));
							{	/* Llib/object.scm 857 */
								obj_t BgL_oldzd2methodzd2arrayz00_1375;

								BgL_oldzd2methodzd2arrayz00_1375 =
									PROCEDURE_REF(((obj_t) BgL_genz00_1373), (int) (1L));
								{	/* Llib/object.scm 858 */

									{	/* Llib/object.scm 861 */
										obj_t BgL_arg1549z00_1376;

										{	/* Llib/object.scm 843 */
											obj_t BgL_newzd2veczd2_3150;

											BgL_newzd2veczd2_3150 =
												BGl_extendzd2vectorzd2zz__objectz00
												(BgL_oldzd2methodzd2arrayz00_1375,
												BgL_defaultzd2bucketzd2_1374,
												VECTOR_LENGTH(((obj_t)
														BgL_oldzd2methodzd2arrayz00_1375)));
											FREE_VECTOR_UNCOLLECTABLE
												(BgL_oldzd2methodzd2arrayz00_1375);
											BUNSPEC;
											BgL_arg1549z00_1376 = BgL_newzd2veczd2_3150;
										}
										PROCEDURE_SET(
											((obj_t) BgL_genz00_1373),
											(int) (1L), BgL_arg1549z00_1376);
									}
									{
										long BgL_iz00_7029;

										BgL_iz00_7029 = (BgL_iz00_1370 + 1L);
										BgL_iz00_1370 = BgL_iz00_7029;
										goto BgL_zc3z04anonymousza31547ze3z87_1371;
									}
								}
							}
						}
					}
				else
					{	/* Llib/object.scm 855 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* double-nb-generics! */
	obj_t BGl_doublezd2nbzd2genericsz12z12zz__objectz00(void)
	{
		{	/* Llib/object.scm 867 */
			{	/* Llib/object.scm 868 */
				long BgL_za72za7_3156;

				BgL_za72za7_3156 =
					(long) CINT(BGl_za2nbzd2genericszd2maxza2z00zz__objectz00);
				BGl_za2nbzd2genericszd2maxza2z00zz__objectz00 =
					BINT((2L * BgL_za72za7_3156));
			}
			{	/* Llib/object.scm 869 */
				obj_t BgL_oldzd2veczd2_3157;

				BgL_oldzd2veczd2_3157 = BGl_za2genericsza2z00zz__objectz00;
				{	/* Llib/object.scm 843 */
					obj_t BgL_newzd2veczd2_3158;

					BgL_newzd2veczd2_3158 =
						BGl_extendzd2vectorzd2zz__objectz00(BgL_oldzd2veczd2_3157, BFALSE,
						VECTOR_LENGTH(((obj_t) BgL_oldzd2veczd2_3157)));
					FREE_VECTOR_UNCOLLECTABLE(BgL_oldzd2veczd2_3157);
					BUNSPEC;
					return (BGl_za2genericsza2z00zz__objectz00 =
						BgL_newzd2veczd2_3158, BUNSPEC);
				}
			}
		}

	}



/* object? */
	BGL_EXPORTED_DEF bool_t BGl_objectzf3zf3zz__objectz00(obj_t BgL_objz00_78)
	{
		{	/* Llib/object.scm 874 */
			return BGL_OBJECTP(BgL_objz00_78);
		}

	}



/* &object? */
	obj_t BGl_z62objectzf3z91zz__objectz00(obj_t BgL_envz00_5121,
		obj_t BgL_objz00_5122)
	{
		{	/* Llib/object.scm 874 */
			return BBOOL(BGl_objectzf3zf3zz__objectz00(BgL_objz00_5122));
		}

	}



/* object-class-num */
	BGL_EXPORTED_DEF long
		BGl_objectzd2classzd2numz00zz__objectz00(BgL_objectz00_bglt BgL_objz00_79)
	{
		{	/* Llib/object.scm 880 */
			return BGL_OBJECT_CLASS_NUM(BgL_objz00_79);
		}

	}



/* &object-class-num */
	obj_t BGl_z62objectzd2classzd2numz62zz__objectz00(obj_t BgL_envz00_5123,
		obj_t BgL_objz00_5124)
	{
		{	/* Llib/object.scm 880 */
			{	/* Llib/object.scm 881 */
				long BgL_tmpz00_7042;

				{	/* Llib/object.scm 881 */
					BgL_objectz00_bglt BgL_auxz00_7043;

					if (BGl_isazf3zf3zz__objectz00(BgL_objz00_5124,
							BGl_objectz00zz__objectz00))
						{	/* Llib/object.scm 881 */
							BgL_auxz00_7043 = ((BgL_objectz00_bglt) BgL_objz00_5124);
						}
					else
						{
							obj_t BgL_auxz00_7047;

							BgL_auxz00_7047 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
								BINT(38375L), BGl_string2902z00zz__objectz00,
								BGl_string2903z00zz__objectz00, BgL_objz00_5124);
							FAILURE(BgL_auxz00_7047, BFALSE, BFALSE);
						}
					BgL_tmpz00_7042 =
						BGl_objectzd2classzd2numz00zz__objectz00(BgL_auxz00_7043);
				}
				return BINT(BgL_tmpz00_7042);
			}
		}

	}



/* object-class-num-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_objectzd2classzd2numzd2setz12zc0zz__objectz00(BgL_objectz00_bglt
		BgL_objz00_80, long BgL_numz00_81)
	{
		{	/* Llib/object.scm 886 */
			return BGL_OBJECT_CLASS_NUM_SET(BgL_objz00_80, BgL_numz00_81);
		}

	}



/* &object-class-num-set! */
	obj_t BGl_z62objectzd2classzd2numzd2setz12za2zz__objectz00(obj_t
		BgL_envz00_5125, obj_t BgL_objz00_5126, obj_t BgL_numz00_5127)
	{
		{	/* Llib/object.scm 886 */
			{	/* Llib/object.scm 887 */
				long BgL_auxz00_7062;
				BgL_objectz00_bglt BgL_auxz00_7054;

				{	/* Llib/object.scm 887 */
					obj_t BgL_tmpz00_7063;

					if (INTEGERP(BgL_numz00_5127))
						{	/* Llib/object.scm 887 */
							BgL_tmpz00_7063 = BgL_numz00_5127;
						}
					else
						{
							obj_t BgL_auxz00_7066;

							BgL_auxz00_7066 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
								BINT(38687L), BGl_string2904z00zz__objectz00,
								BGl_string2860z00zz__objectz00, BgL_numz00_5127);
							FAILURE(BgL_auxz00_7066, BFALSE, BFALSE);
						}
					BgL_auxz00_7062 = (long) CINT(BgL_tmpz00_7063);
				}
				if (BGl_isazf3zf3zz__objectz00(BgL_objz00_5126,
						BGl_objectz00zz__objectz00))
					{	/* Llib/object.scm 887 */
						BgL_auxz00_7054 = ((BgL_objectz00_bglt) BgL_objz00_5126);
					}
				else
					{
						obj_t BgL_auxz00_7058;

						BgL_auxz00_7058 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(38687L), BGl_string2904z00zz__objectz00,
							BGl_string2903z00zz__objectz00, BgL_objz00_5126);
						FAILURE(BgL_auxz00_7058, BFALSE, BFALSE);
					}
				return
					BGl_objectzd2classzd2numzd2setz12zc0zz__objectz00(BgL_auxz00_7054,
					BgL_auxz00_7062);
			}
		}

	}



/* object-class */
	BGL_EXPORTED_DEF obj_t BGl_objectzd2classzd2zz__objectz00(BgL_objectz00_bglt
		BgL_objectz00_82)
	{
		{	/* Llib/object.scm 892 */
			{	/* Llib/object.scm 893 */
				obj_t BgL_arg1553z00_5976;
				long BgL_arg1554z00_5977;

				BgL_arg1553z00_5976 = (BGl_za2classesza2z00zz__objectz00);
				{	/* Llib/object.scm 894 */
					long BgL_arg1555z00_5978;

					BgL_arg1555z00_5978 = BGL_OBJECT_CLASS_NUM(BgL_objectz00_82);
					BgL_arg1554z00_5977 = (BgL_arg1555z00_5978 - OBJECT_TYPE);
				}
				return VECTOR_REF(BgL_arg1553z00_5976, BgL_arg1554z00_5977);
			}
		}

	}



/* &object-class */
	obj_t BGl_z62objectzd2classzb0zz__objectz00(obj_t BgL_envz00_5128,
		obj_t BgL_objectz00_5129)
	{
		{	/* Llib/object.scm 892 */
			{	/* Llib/object.scm 893 */
				BgL_objectz00_bglt BgL_auxz00_7076;

				if (BGl_isazf3zf3zz__objectz00(BgL_objectz00_5129,
						BGl_objectz00zz__objectz00))
					{	/* Llib/object.scm 893 */
						BgL_auxz00_7076 = ((BgL_objectz00_bglt) BgL_objectz00_5129);
					}
				else
					{
						obj_t BgL_auxz00_7080;

						BgL_auxz00_7080 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(39007L), BGl_string2905z00zz__objectz00,
							BGl_string2903z00zz__objectz00, BgL_objectz00_5129);
						FAILURE(BgL_auxz00_7080, BFALSE, BFALSE);
					}
				return BGl_objectzd2classzd2zz__objectz00(BgL_auxz00_7076);
			}
		}

	}



/* generic-default */
	BGL_EXPORTED_DEF obj_t BGl_genericzd2defaultzd2zz__objectz00(obj_t
		BgL_genericz00_83)
	{
		{	/* Llib/object.scm 899 */
			return PROCEDURE_REF(BgL_genericz00_83, (int) (0L));
		}

	}



/* &generic-default */
	obj_t BGl_z62genericzd2defaultzb0zz__objectz00(obj_t BgL_envz00_5130,
		obj_t BgL_genericz00_5131)
	{
		{	/* Llib/object.scm 899 */
			{	/* Llib/object.scm 900 */
				obj_t BgL_auxz00_7087;

				if (PROCEDUREP(BgL_genericz00_5131))
					{	/* Llib/object.scm 900 */
						BgL_auxz00_7087 = BgL_genericz00_5131;
					}
				else
					{
						obj_t BgL_auxz00_7090;

						BgL_auxz00_7090 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(39357L), BGl_string2906z00zz__objectz00,
							BGl_string2907z00zz__objectz00, BgL_genericz00_5131);
						FAILURE(BgL_auxz00_7090, BFALSE, BFALSE);
					}
				return BGl_genericzd2defaultzd2zz__objectz00(BgL_auxz00_7087);
			}
		}

	}



/* generic-method-array */
	BGL_EXPORTED_DEF obj_t BGl_genericzd2methodzd2arrayz00zz__objectz00(obj_t
		BgL_genericz00_86)
	{
		{	/* Llib/object.scm 908 */
			return PROCEDURE_REF(BgL_genericz00_86, (int) (1L));
		}

	}



/* &generic-method-array */
	obj_t BGl_z62genericzd2methodzd2arrayz62zz__objectz00(obj_t BgL_envz00_5132,
		obj_t BgL_genericz00_5133)
	{
		{	/* Llib/object.scm 908 */
			{	/* Llib/object.scm 909 */
				obj_t BgL_auxz00_7097;

				if (PROCEDUREP(BgL_genericz00_5133))
					{	/* Llib/object.scm 909 */
						BgL_auxz00_7097 = BgL_genericz00_5133;
					}
				else
					{
						obj_t BgL_auxz00_7100;

						BgL_auxz00_7100 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(39750L), BGl_string2908z00zz__objectz00,
							BGl_string2907z00zz__objectz00, BgL_genericz00_5133);
						FAILURE(BgL_auxz00_7100, BFALSE, BFALSE);
					}
				return BGl_genericzd2methodzd2arrayz00zz__objectz00(BgL_auxz00_7097);
			}
		}

	}



/* method-array-ref */
	BGL_EXPORTED_DEF obj_t BGl_methodzd2arrayzd2refz00zz__objectz00(obj_t
		BgL_genericz00_92, obj_t BgL_arrayz00_93, int BgL_offsetz00_94)
	{
		{	/* Llib/object.scm 930 */
			{	/* Llib/object.scm 931 */
				long BgL_offsetz00_5979;

				BgL_offsetz00_5979 = ((long) (BgL_offsetz00_94) - OBJECT_TYPE);
				{	/* Llib/object.scm 931 */
					long BgL_modz00_5980;

					BgL_modz00_5980 = (BgL_offsetz00_5979 >> (int) ((long) ((int) (4L))));
					{	/* Llib/object.scm 932 */
						long BgL_restz00_5981;

						BgL_restz00_5981 =
							(BgL_offsetz00_5979 &
							(long) (
								(int) (
									((long) (
											(int) ((1L << (int) ((long) ((int) (4L)))))) - 1L))));
						{	/* Llib/object.scm 933 */

							{	/* Llib/object.scm 934 */
								obj_t BgL_bucketz00_5982;

								BgL_bucketz00_5982 =
									VECTOR_REF(BgL_arrayz00_93, BgL_modz00_5980);
								return
									VECTOR_REF(((obj_t) BgL_bucketz00_5982), BgL_restz00_5981);
							}
						}
					}
				}
			}
		}

	}



/* &method-array-ref */
	obj_t BGl_z62methodzd2arrayzd2refz62zz__objectz00(obj_t BgL_envz00_5134,
		obj_t BgL_genericz00_5135, obj_t BgL_arrayz00_5136,
		obj_t BgL_offsetz00_5137)
	{
		{	/* Llib/object.scm 930 */
			{	/* Llib/object.scm 931 */
				int BgL_auxz00_7138;
				obj_t BgL_auxz00_7131;
				obj_t BgL_auxz00_7124;

				{	/* Llib/object.scm 931 */
					obj_t BgL_tmpz00_7139;

					if (INTEGERP(BgL_offsetz00_5137))
						{	/* Llib/object.scm 931 */
							BgL_tmpz00_7139 = BgL_offsetz00_5137;
						}
					else
						{
							obj_t BgL_auxz00_7142;

							BgL_auxz00_7142 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
								BINT(40903L), BGl_string2909z00zz__objectz00,
								BGl_string2860z00zz__objectz00, BgL_offsetz00_5137);
							FAILURE(BgL_auxz00_7142, BFALSE, BFALSE);
						}
					BgL_auxz00_7138 = CINT(BgL_tmpz00_7139);
				}
				if (VECTORP(BgL_arrayz00_5136))
					{	/* Llib/object.scm 931 */
						BgL_auxz00_7131 = BgL_arrayz00_5136;
					}
				else
					{
						obj_t BgL_auxz00_7134;

						BgL_auxz00_7134 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(40903L), BGl_string2909z00zz__objectz00,
							BGl_string2873z00zz__objectz00, BgL_arrayz00_5136);
						FAILURE(BgL_auxz00_7134, BFALSE, BFALSE);
					}
				if (PROCEDUREP(BgL_genericz00_5135))
					{	/* Llib/object.scm 931 */
						BgL_auxz00_7124 = BgL_genericz00_5135;
					}
				else
					{
						obj_t BgL_auxz00_7127;

						BgL_auxz00_7127 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(40903L), BGl_string2909z00zz__objectz00,
							BGl_string2907z00zz__objectz00, BgL_genericz00_5135);
						FAILURE(BgL_auxz00_7127, BFALSE, BFALSE);
					}
				return
					BGl_methodzd2arrayzd2refz00zz__objectz00(BgL_auxz00_7124,
					BgL_auxz00_7131, BgL_auxz00_7138);
			}
		}

	}



/* method-array-set! */
	obj_t BGl_methodzd2arrayzd2setz12z12zz__objectz00(obj_t BgL_genericz00_95,
		obj_t BgL_arrayz00_96, long BgL_offsetz00_97, obj_t BgL_methodz00_98)
	{
		{	/* Llib/object.scm 940 */
			{	/* Llib/object.scm 941 */
				long BgL_offsetz00_1388;

				BgL_offsetz00_1388 = (BgL_offsetz00_97 - OBJECT_TYPE);
				{	/* Llib/object.scm 941 */
					long BgL_modz00_1389;

					{	/* Llib/object.scm 944 */
						uint32_t BgL_arg1580z00_1409;

						{	/* Llib/object.scm 944 */
							uint32_t BgL_arg1582z00_1410;
							uint32_t BgL_arg1583z00_1411;

							BgL_arg1582z00_1410 = (uint32_t) (BgL_offsetz00_1388);
							{	/* Llib/object.scm 945 */
								int BgL_arg1584z00_1412;

								BgL_arg1584z00_1412 =
									(int) ((1L << (int) ((long) ((int) (4L)))));
								{	/* Llib/object.scm 945 */
									long BgL_tmpz00_7155;

									BgL_tmpz00_7155 = (long) (BgL_arg1584z00_1412);
									BgL_arg1583z00_1411 = (uint32_t) (BgL_tmpz00_7155);
							}}
							{	/* Llib/object.scm 943 */
								int32_t BgL_tmpz00_7158;

								BgL_tmpz00_7158 = (int32_t) (BgL_arg1583z00_1411);
								BgL_arg1580z00_1409 = (BgL_arg1582z00_1410 / BgL_tmpz00_7158);
						}}
						BgL_modz00_1389 = (long) (BgL_arg1580z00_1409);
					}
					{	/* Llib/object.scm 942 */
						long BgL_restz00_1390;

						{	/* Llib/object.scm 948 */
							uint32_t BgL_arg1575z00_1405;

							{	/* Llib/object.scm 948 */
								uint32_t BgL_arg1576z00_1406;
								uint32_t BgL_arg1578z00_1407;

								BgL_arg1576z00_1406 = (uint32_t) (BgL_offsetz00_1388);
								{	/* Llib/object.scm 949 */
									int BgL_arg1579z00_1408;

									BgL_arg1579z00_1408 =
										(int) ((1L << (int) ((long) ((int) (4L)))));
									{	/* Llib/object.scm 949 */
										long BgL_tmpz00_7168;

										BgL_tmpz00_7168 = (long) (BgL_arg1579z00_1408);
										BgL_arg1578z00_1407 = (uint32_t) (BgL_tmpz00_7168);
								}}
								{	/* Llib/object.scm 947 */
									int32_t BgL_tmpz00_7171;

									BgL_tmpz00_7171 = (int32_t) (BgL_arg1578z00_1407);
									BgL_arg1575z00_1405 = (BgL_arg1576z00_1406 % BgL_tmpz00_7171);
							}}
							BgL_restz00_1390 = (long) (BgL_arg1575z00_1405);
						}
						{	/* Llib/object.scm 946 */

							{	/* Llib/object.scm 950 */
								obj_t BgL_bucketz00_1391;

								BgL_bucketz00_1391 =
									VECTOR_REF(((obj_t) BgL_arrayz00_96), BgL_modz00_1389);
								{	/* Llib/object.scm 951 */
									bool_t BgL_test3592z00_7177;

									{	/* Llib/object.scm 951 */
										bool_t BgL_test3593z00_7178;

										{	/* Llib/object.scm 951 */
											obj_t BgL_tmpz00_7179;

											{	/* Llib/object.scm 951 */
												obj_t BgL_res2548z00_3216;

												BgL_res2548z00_3216 =
													PROCEDURE_REF(
													((obj_t) BgL_genericz00_95), (int) (0L));
												BgL_tmpz00_7179 = BgL_res2548z00_3216;
											}
											BgL_test3593z00_7178 =
												(BgL_methodz00_98 == BgL_tmpz00_7179);
										}
										if (BgL_test3593z00_7178)
											{	/* Llib/object.scm 951 */
												BgL_test3592z00_7177 = ((bool_t) 1);
											}
										else
											{	/* Llib/object.scm 951 */
												if (
													(BgL_bucketz00_1391 ==
														PROCEDURE_REF(BgL_genericz00_95, (int) (2L))))
													{	/* Llib/object.scm 952 */
														BgL_test3592z00_7177 = ((bool_t) 0);
													}
												else
													{	/* Llib/object.scm 952 */
														BgL_test3592z00_7177 = ((bool_t) 1);
													}
											}
									}
									if (BgL_test3592z00_7177)
										{	/* Llib/object.scm 951 */
											return
												VECTOR_SET(
												((obj_t) BgL_bucketz00_1391), BgL_restz00_1390,
												BgL_methodz00_98);
										}
									else
										{	/* Llib/object.scm 956 */
											obj_t BgL_bucketz00_1399;

											{	/* Llib/object.scm 956 */
												int BgL_arg1567z00_1400;

												BgL_arg1567z00_1400 =
													(int) ((1L << (int) ((long) ((int) (4L)))));
												BgL_bucketz00_1399 =
													BGl_copyzd2vectorzd2zz__r4_vectors_6_8z00
													(BgL_bucketz00_1391, (long) (BgL_arg1567z00_1400));
											}
											VECTOR_SET(BgL_bucketz00_1399, BgL_restz00_1390,
												BgL_methodz00_98);
											return VECTOR_SET(((obj_t) BgL_arrayz00_96),
												BgL_modz00_1389, BgL_bucketz00_1399);
										}
								}
							}
						}
					}
				}
			}
		}

	}



/* generic-memory-statistics */
	BGL_EXPORTED_DEF obj_t BGl_genericzd2memoryzd2statisticsz00zz__objectz00(void)
	{
		{	/* Llib/object.scm 963 */
			{	/* Llib/object.scm 965 */
				obj_t BgL_top3596z00_7201;

				BgL_top3596z00_7201 = BGL_EXITD_TOP_AS_OBJ();
				BGL_MUTEX_LOCK(bigloo_generic_mutex);
				BGL_EXITD_PUSH_PROTECT(BgL_top3596z00_7201, bigloo_generic_mutex);
				BUNSPEC;
				{	/* Llib/object.scm 965 */
					obj_t BgL_tmp3595z00_7200;

					{
						long BgL_gz00_1414;
						long BgL_siza7eza7_1415;

						BgL_gz00_1414 = 0L;
						BgL_siza7eza7_1415 = 0L;
					BgL_zc3z04anonymousza31585ze3z87_1416:
						if (
							(BgL_gz00_1414 ==
								(long) CINT(BGl_za2nbzd2genericsza2zd2zz__objectz00)))
							{	/* Llib/object.scm 969 */
								obj_t BgL_arg1587z00_1418;
								obj_t BgL_arg1589z00_1419;

								{	/* Llib/object.scm 969 */
									obj_t BgL_arg1591z00_1420;

									BgL_arg1591z00_1420 =
										MAKE_YOUNG_PAIR(BGl_za2nbzd2genericsza2zd2zz__objectz00,
										BNIL);
									BgL_arg1587z00_1418 =
										MAKE_YOUNG_PAIR(BGl_symbol2910z00zz__objectz00,
										BgL_arg1591z00_1420);
								}
								{	/* Llib/object.scm 970 */
									obj_t BgL_arg1593z00_1421;
									obj_t BgL_arg1594z00_1422;

									{	/* Llib/object.scm 970 */
										obj_t BgL_arg1595z00_1423;

										BgL_arg1595z00_1423 =
											MAKE_YOUNG_PAIR(BGl_za2nbzd2classesza2zd2zz__objectz00,
											BNIL);
										BgL_arg1593z00_1421 =
											MAKE_YOUNG_PAIR(BGl_symbol2912z00zz__objectz00,
											BgL_arg1595z00_1423);
									}
									{	/* Llib/object.scm 971 */
										obj_t BgL_arg1598z00_1424;
										obj_t BgL_arg1601z00_1425;

										{	/* Llib/object.scm 971 */
											obj_t BgL_arg1602z00_1426;

											BgL_arg1602z00_1426 =
												MAKE_YOUNG_PAIR(BINT(BgL_siza7eza7_1415), BNIL);
											BgL_arg1598z00_1424 =
												MAKE_YOUNG_PAIR(BGl_symbol2913z00zz__objectz00,
												BgL_arg1602z00_1426);
										}
										{	/* Llib/object.scm 974 */
											obj_t BgL_arg1603z00_1427;
											obj_t BgL_arg1605z00_1428;

											{	/* Llib/object.scm 974 */
												obj_t BgL_arg1606z00_1429;

												{	/* Llib/object.scm 974 */
													long BgL_arg1607z00_1430;

													{	/* Llib/object.scm 974 */
														obj_t BgL_arg1608z00_1431;

														{	/* Llib/object.scm 974 */
															obj_t BgL_arg1609z00_1432;

															BgL_arg1609z00_1432 =
																VECTOR_REF(BGl_za2genericsza2z00zz__objectz00,
																0L);
															BgL_arg1608z00_1431 =
																PROCEDURE_REF(((obj_t) BgL_arg1609z00_1432),
																(int) (1L));
														}
														BgL_arg1607z00_1430 =
															VECTOR_LENGTH(((obj_t) BgL_arg1608z00_1431));
													}
													BgL_arg1606z00_1429 =
														MAKE_YOUNG_PAIR(BINT(BgL_arg1607z00_1430), BNIL);
												}
												BgL_arg1603z00_1427 =
													MAKE_YOUNG_PAIR(BGl_symbol2915z00zz__objectz00,
													BgL_arg1606z00_1429);
											}
											{	/* Llib/object.scm 975 */
												obj_t BgL_arg1610z00_1433;
												obj_t BgL_arg1611z00_1434;

												{	/* Llib/object.scm 975 */
													obj_t BgL_arg1612z00_1435;

													{	/* Llib/object.scm 975 */
														int BgL_arg1613z00_1436;

														BgL_arg1613z00_1436 =
															(int) ((1L << (int) ((long) ((int) (4L)))));
														BgL_arg1612z00_1435 =
															MAKE_YOUNG_PAIR(BINT(BgL_arg1613z00_1436), BNIL);
													}
													BgL_arg1610z00_1433 =
														MAKE_YOUNG_PAIR(BGl_symbol2917z00zz__objectz00,
														BgL_arg1612z00_1435);
												}
												{	/* Llib/object.scm 976 */
													obj_t BgL_arg1615z00_1437;
													obj_t BgL_arg1616z00_1438;

													{	/* Llib/object.scm 976 */
														obj_t BgL_arg1617z00_1439;

														BgL_arg1617z00_1439 =
															MAKE_YOUNG_PAIR
															(BGl_za2nbzd2classeszd2maxza2z00zz__objectz00,
															BNIL);
														BgL_arg1615z00_1437 =
															MAKE_YOUNG_PAIR(BGl_symbol2919z00zz__objectz00,
															BgL_arg1617z00_1439);
													}
													{	/* Llib/object.scm 977 */
														obj_t BgL_arg1618z00_1440;

														{	/* Llib/object.scm 977 */
															obj_t BgL_arg1619z00_1441;

															BgL_arg1619z00_1441 =
																MAKE_YOUNG_PAIR
																(BGl_za2nbzd2genericszd2maxza2z00zz__objectz00,
																BNIL);
															BgL_arg1618z00_1440 =
																MAKE_YOUNG_PAIR(BGl_symbol2921z00zz__objectz00,
																BgL_arg1619z00_1441);
														}
														BgL_arg1616z00_1438 =
															MAKE_YOUNG_PAIR(BgL_arg1618z00_1440, BNIL);
													}
													BgL_arg1611z00_1434 =
														MAKE_YOUNG_PAIR(BgL_arg1615z00_1437,
														BgL_arg1616z00_1438);
												}
												BgL_arg1605z00_1428 =
													MAKE_YOUNG_PAIR(BgL_arg1610z00_1433,
													BgL_arg1611z00_1434);
											}
											BgL_arg1601z00_1425 =
												MAKE_YOUNG_PAIR(BgL_arg1603z00_1427,
												BgL_arg1605z00_1428);
										}
										BgL_arg1594z00_1422 =
											MAKE_YOUNG_PAIR(BgL_arg1598z00_1424, BgL_arg1601z00_1425);
									}
									BgL_arg1589z00_1419 =
										MAKE_YOUNG_PAIR(BgL_arg1593z00_1421, BgL_arg1594z00_1422);
								}
								BgL_tmp3595z00_7200 =
									MAKE_YOUNG_PAIR(BgL_arg1587z00_1418, BgL_arg1589z00_1419);
							}
						else
							{	/* Llib/object.scm 978 */
								obj_t BgL_genz00_1442;

								BgL_genz00_1442 =
									VECTOR_REF(BGl_za2genericsza2z00zz__objectz00, BgL_gz00_1414);
								{	/* Llib/object.scm 978 */
									obj_t BgL_dbuckz00_1443;

									BgL_dbuckz00_1443 =
										PROCEDURE_REF(BgL_genz00_1442, (int) (2L));
									{	/* Llib/object.scm 979 */
										long BgL_dza7za7_1444;

										BgL_dza7za7_1444 = 0L;
										{	/* Llib/object.scm 980 */
											obj_t BgL_sza7za7_1445;

											{	/* Llib/object.scm 981 */
												obj_t BgL_runner1637z00_1469;

												{	/* Llib/object.scm 981 */
													obj_t BgL_l1317z00_1453;

													{	/* Llib/object.scm 988 */
														obj_t BgL_arg1636z00_1468;

														BgL_arg1636z00_1468 =
															PROCEDURE_REF(
															((obj_t) BgL_genz00_1442), (int) (1L));
														BgL_l1317z00_1453 =
															BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00
															(BgL_arg1636z00_1468);
													}
													if (NULLP(BgL_l1317z00_1453))
														{	/* Llib/object.scm 981 */
															BgL_runner1637z00_1469 = BNIL;
														}
													else
														{	/* Llib/object.scm 981 */
															obj_t BgL_head1319z00_1455;

															BgL_head1319z00_1455 =
																MAKE_YOUNG_PAIR(BNIL, BNIL);
															{
																obj_t BgL_l1317z00_1457;
																obj_t BgL_tail1320z00_1458;

																BgL_l1317z00_1457 = BgL_l1317z00_1453;
																BgL_tail1320z00_1458 = BgL_head1319z00_1455;
															BgL_zc3z04anonymousza31627ze3z87_1459:
																if (NULLP(BgL_l1317z00_1457))
																	{	/* Llib/object.scm 981 */
																		BgL_runner1637z00_1469 =
																			CDR(BgL_head1319z00_1455);
																	}
																else
																	{	/* Llib/object.scm 981 */
																		obj_t BgL_newtail1321z00_1461;

																		{	/* Llib/object.scm 981 */
																			long BgL_arg1630z00_1463;

																			{	/* Llib/object.scm 981 */
																				obj_t BgL_bz00_1464;

																				BgL_bz00_1464 =
																					CAR(((obj_t) BgL_l1317z00_1457));
																				if (
																					(BgL_bz00_1464 == BgL_dbuckz00_1443))
																					{	/* Llib/object.scm 983 */
																						BgL_dza7za7_1444 =
																							(VECTOR_LENGTH(
																								((obj_t) BgL_bz00_1464)) * 4L);
																						BgL_arg1630z00_1463 = 0L;
																					}
																				else
																					{	/* Llib/object.scm 983 */
																						BgL_arg1630z00_1463 =
																							(4L *
																							VECTOR_LENGTH(
																								((obj_t) BgL_bz00_1464)));
																					}
																			}
																			BgL_newtail1321z00_1461 =
																				MAKE_YOUNG_PAIR(BINT
																				(BgL_arg1630z00_1463), BNIL);
																		}
																		SET_CDR(BgL_tail1320z00_1458,
																			BgL_newtail1321z00_1461);
																		{	/* Llib/object.scm 981 */
																			obj_t BgL_arg1629z00_1462;

																			BgL_arg1629z00_1462 =
																				CDR(((obj_t) BgL_l1317z00_1457));
																			{
																				obj_t BgL_tail1320z00_7272;
																				obj_t BgL_l1317z00_7271;

																				BgL_l1317z00_7271 = BgL_arg1629z00_1462;
																				BgL_tail1320z00_7272 =
																					BgL_newtail1321z00_1461;
																				BgL_tail1320z00_1458 =
																					BgL_tail1320z00_7272;
																				BgL_l1317z00_1457 = BgL_l1317z00_7271;
																				goto
																					BgL_zc3z04anonymousza31627ze3z87_1459;
																			}
																		}
																	}
															}
														}
												}
												BgL_sza7za7_1445 =
													BGl_zb2zb2zz__r4_numbers_6_5z00
													(BgL_runner1637z00_1469);
											}
											{	/* Llib/object.scm 981 */
												long BgL_bza7za7_1446;

												{	/* Llib/object.scm 989 */
													long BgL_arg1624z00_1451;

													{	/* Llib/object.scm 989 */
														obj_t BgL_arg1625z00_1452;

														BgL_arg1625z00_1452 =
															PROCEDURE_REF(
															((obj_t) BgL_genz00_1442), (int) (1L));
														BgL_arg1624z00_1451 =
															VECTOR_LENGTH(((obj_t) BgL_arg1625z00_1452));
													}
													BgL_bza7za7_1446 = (4L * BgL_arg1624z00_1451);
												}
												{	/* Llib/object.scm 989 */

													{	/* Llib/object.scm 990 */
														long BgL_arg1620z00_1447;
														long BgL_arg1621z00_1448;

														BgL_arg1620z00_1447 = (BgL_gz00_1414 + 1L);
														{	/* Llib/object.scm 991 */
															long BgL_arg1622z00_1449;

															{	/* Llib/object.scm 991 */
																long BgL_za72za7_3256;

																BgL_za72za7_3256 =
																	(long) CINT(BgL_sza7za7_1445);
																BgL_arg1622z00_1449 =
																	(
																	(BgL_bza7za7_1446 + BgL_dza7za7_1444) +
																	BgL_za72za7_3256);
															}
															BgL_arg1621z00_1448 =
																(BgL_siza7eza7_1415 + BgL_arg1622z00_1449);
														}
														{
															long BgL_siza7eza7_7286;
															long BgL_gz00_7285;

															BgL_gz00_7285 = BgL_arg1620z00_1447;
															BgL_siza7eza7_7286 = BgL_arg1621z00_1448;
															BgL_siza7eza7_1415 = BgL_siza7eza7_7286;
															BgL_gz00_1414 = BgL_gz00_7285;
															goto BgL_zc3z04anonymousza31585ze3z87_1416;
														}
													}
												}
											}
										}
									}
								}
							}
					}
					BGL_EXITD_POP_PROTECT(BgL_top3596z00_7201);
					BUNSPEC;
					BGL_MUTEX_UNLOCK(bigloo_generic_mutex);
					return BgL_tmp3595z00_7200;
				}
			}
		}

	}



/* &generic-memory-statistics */
	obj_t BGl_z62genericzd2memoryzd2statisticsz62zz__objectz00(obj_t
		BgL_envz00_5138)
	{
		{	/* Llib/object.scm 963 */
			return BGl_genericzd2memoryzd2statisticsz00zz__objectz00();
		}

	}



/* generics-add-class! */
	bool_t BGl_genericszd2addzd2classz12z12zz__objectz00(long
		BgL_classzd2idxzd2_99, long BgL_superzd2idxzd2_100)
	{
		{	/* Llib/object.scm 998 */
			{
				long BgL_gz00_1472;

				BgL_gz00_1472 = 0L;
			BgL_zc3z04anonymousza31638ze3z87_1473:
				if (
					(BgL_gz00_1472 <
						(long) CINT(BGl_za2nbzd2genericsza2zd2zz__objectz00)))
					{	/* Llib/object.scm 1001 */
						obj_t BgL_genz00_1475;

						BgL_genz00_1475 =
							VECTOR_REF(BGl_za2genericsza2z00zz__objectz00, BgL_gz00_1472);
						{	/* Llib/object.scm 1001 */
							obj_t BgL_methodzd2arrayzd2_1476;

							BgL_methodzd2arrayzd2_1476 =
								PROCEDURE_REF(((obj_t) BgL_genz00_1475), (int) (1L));
							{	/* Llib/object.scm 1002 */
								obj_t BgL_methodz00_1477;

								{	/* Llib/object.scm 1003 */
									int BgL_offsetz00_3266;

									BgL_offsetz00_3266 = (int) (BgL_superzd2idxzd2_100);
									{	/* Llib/object.scm 931 */
										long BgL_offsetz00_3267;

										BgL_offsetz00_3267 =
											((long) (BgL_offsetz00_3266) - OBJECT_TYPE);
										{	/* Llib/object.scm 931 */
											long BgL_modz00_3268;

											BgL_modz00_3268 =
												(BgL_offsetz00_3267 >> (int) ((long) ((int) (4L))));
											{	/* Llib/object.scm 932 */
												long BgL_restz00_3270;

												BgL_restz00_3270 =
													(BgL_offsetz00_3267 &
													(long) (
														(int) (
															((long) (
																	(int) (
																		(1L <<
																			(int) ((long) ((int) (4L)))))) - 1L))));
												{	/* Llib/object.scm 933 */

													{	/* Llib/object.scm 934 */
														obj_t BgL_bucketz00_3272;

														BgL_bucketz00_3272 =
															VECTOR_REF(
															((obj_t) BgL_methodzd2arrayzd2_1476),
															BgL_modz00_3268);
														BgL_methodz00_1477 =
															VECTOR_REF(((obj_t) BgL_bucketz00_3272),
															BgL_restz00_3270);
								}}}}}}
								{	/* Llib/object.scm 1003 */

									BGl_methodzd2arrayzd2setz12z12zz__objectz00(BgL_genz00_1475,
										BgL_methodzd2arrayzd2_1476, BgL_classzd2idxzd2_99,
										BgL_methodz00_1477);
									{
										long BgL_gz00_7319;

										BgL_gz00_7319 = (BgL_gz00_1472 + 1L);
										BgL_gz00_1472 = BgL_gz00_7319;
										goto BgL_zc3z04anonymousza31638ze3z87_1473;
									}
								}
							}
						}
					}
				else
					{	/* Llib/object.scm 1000 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* register-class! */
	BGL_EXPORTED_DEF obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t
		BgL_namez00_101, obj_t BgL_modulez00_102, obj_t BgL_superz00_103,
		long BgL_hashz00_104, obj_t BgL_creatorz00_105, obj_t BgL_allocatorz00_106,
		obj_t BgL_constructorz00_107, obj_t BgL_nilz00_108, obj_t BgL_shrinkz00_109,
		obj_t BgL_plainz00_110, obj_t BgL_virtualz00_111)
	{
		{	/* Llib/object.scm 1010 */
			{

				{	/* Llib/object.scm 1075 */
					obj_t BgL_top3603z00_7322;

					BgL_top3603z00_7322 = BGL_EXITD_TOP_AS_OBJ();
					BGL_MUTEX_LOCK(bigloo_generic_mutex);
					BGL_EXITD_PUSH_PROTECT(BgL_top3603z00_7322, bigloo_generic_mutex);
					BUNSPEC;
					{	/* Llib/object.scm 1075 */
						obj_t BgL_tmp3602z00_7321;

						BGl_initializa7ezd2objectsz12z67zz__objectz00();
						{	/* Llib/object.scm 1077 */
							bool_t BgL_test3604z00_7327;

							if (CBOOL(BgL_superz00_103))
								{	/* Llib/object.scm 1077 */
									if (BGL_CLASSP(BgL_superz00_103))
										{	/* Llib/object.scm 1077 */
											BgL_test3604z00_7327 = ((bool_t) 0);
										}
									else
										{	/* Llib/object.scm 1077 */
											BgL_test3604z00_7327 = ((bool_t) 1);
										}
								}
							else
								{	/* Llib/object.scm 1077 */
									BgL_test3604z00_7327 = ((bool_t) 0);
								}
							if (BgL_test3604z00_7327)
								{	/* Llib/object.scm 1077 */
									BGl_errorz00zz__errorz00(BgL_namez00_101,
										BGl_string2923z00zz__objectz00, BgL_superz00_103);
								}
							else
								{	/* Llib/object.scm 1077 */
									BFALSE;
								}
						}
						if (VECTORP(BgL_plainz00_110))
							{	/* Llib/object.scm 1079 */
								BFALSE;
							}
						else
							{	/* Llib/object.scm 1079 */
								BGl_errorz00zz__errorz00(BGl_string2924z00zz__objectz00,
									BGl_string2925z00zz__objectz00, BgL_plainz00_110);
							}
						{	/* Llib/object.scm 1081 */
							obj_t BgL_kz00_1485;

							BgL_kz00_1485 =
								BGl_classzd2existszd2zz__objectz00(BgL_namez00_101);
							if (BGL_CLASSP(BgL_kz00_1485))
								{	/* Llib/object.scm 1085 */
									bool_t BgL_test3609z00_7339;

									{	/* Llib/object.scm 1085 */
										long BgL_arg1663z00_1500;

										{	/* Llib/object.scm 717 */
											obj_t BgL_tmpz00_7340;

											BgL_tmpz00_7340 = ((obj_t) BgL_kz00_1485);
											BgL_arg1663z00_1500 = BGL_CLASS_HASH(BgL_tmpz00_7340);
										}
										BgL_test3609z00_7339 =
											(BgL_arg1663z00_1500 == BgL_hashz00_104);
									}
									if (BgL_test3609z00_7339)
										{	/* Llib/object.scm 1085 */
											BgL_tmp3602z00_7321 = BgL_kz00_1485;
										}
									else
										{	/* Llib/object.scm 1085 */
											{	/* Llib/object.scm 1090 */
												obj_t BgL_arg1648z00_1489;

												{	/* Llib/object.scm 516 */
													obj_t BgL_tmpz00_7344;

													BgL_tmpz00_7344 = ((obj_t) BgL_kz00_1485);
													BgL_arg1648z00_1489 =
														BGL_CLASS_MODULE(BgL_tmpz00_7344);
												}
												{	/* Llib/object.scm 1088 */
													obj_t BgL_list1649z00_1490;

													{	/* Llib/object.scm 1088 */
														obj_t BgL_arg1650z00_1491;

														{	/* Llib/object.scm 1088 */
															obj_t BgL_arg1651z00_1492;

															{	/* Llib/object.scm 1088 */
																obj_t BgL_arg1652z00_1493;

																{	/* Llib/object.scm 1088 */
																	obj_t BgL_arg1653z00_1494;

																	{	/* Llib/object.scm 1088 */
																		obj_t BgL_arg1654z00_1495;

																		{	/* Llib/object.scm 1088 */
																			obj_t BgL_arg1656z00_1496;

																			{	/* Llib/object.scm 1088 */
																				obj_t BgL_arg1657z00_1497;

																				{	/* Llib/object.scm 1088 */
																					obj_t BgL_arg1658z00_1498;

																					{	/* Llib/object.scm 1088 */
																						obj_t BgL_arg1661z00_1499;

																						BgL_arg1661z00_1499 =
																							MAKE_YOUNG_PAIR
																							(BGl_string2926z00zz__objectz00,
																							BNIL);
																						BgL_arg1658z00_1498 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1648z00_1489,
																							BgL_arg1661z00_1499);
																					}
																					BgL_arg1657z00_1497 =
																						MAKE_YOUNG_PAIR
																						(BGl_string2927z00zz__objectz00,
																						BgL_arg1658z00_1498);
																				}
																				BgL_arg1656z00_1496 =
																					MAKE_YOUNG_PAIR(BgL_namez00_101,
																					BgL_arg1657z00_1497);
																			}
																			BgL_arg1654z00_1495 =
																				MAKE_YOUNG_PAIR
																				(BGl_string2928z00zz__objectz00,
																				BgL_arg1656z00_1496);
																		}
																		BgL_arg1653z00_1494 =
																			MAKE_YOUNG_PAIR(BgL_modulez00_102,
																			BgL_arg1654z00_1495);
																	}
																	BgL_arg1652z00_1493 =
																		MAKE_YOUNG_PAIR
																		(BGl_string2927z00zz__objectz00,
																		BgL_arg1653z00_1494);
																}
																BgL_arg1651z00_1492 =
																	MAKE_YOUNG_PAIR(BgL_namez00_101,
																	BgL_arg1652z00_1493);
															}
															BgL_arg1650z00_1491 =
																MAKE_YOUNG_PAIR(BGl_string2929z00zz__objectz00,
																BgL_arg1651z00_1492);
														}
														BgL_list1649z00_1490 =
															MAKE_YOUNG_PAIR(BGl_string2924z00zz__objectz00,
															BgL_arg1650z00_1491);
													}
													BGl_warningz00zz__errorz00(BgL_list1649z00_1490);
												}
											}
										BgL_zc3z04anonymousza31664ze3z87_1501:
											if (
												((long) CINT(BGl_za2nbzd2classesza2zd2zz__objectz00) ==
													(long)
													CINT(BGl_za2nbzd2classeszd2maxza2z00zz__objectz00)))
												{	/* Llib/object.scm 1013 */
													BGl_doublezd2nbzd2classesz12z12zz__objectz00();
												}
											else
												{	/* Llib/object.scm 1013 */
													((bool_t) 0);
												}
											{	/* Llib/object.scm 1015 */
												long BgL_numz00_1503;

												BgL_numz00_1503 =
													(OBJECT_TYPE +
													(long) CINT(BGl_za2nbzd2classesza2zd2zz__objectz00));
												{	/* Llib/object.scm 1015 */
													long BgL_depthz00_1504;

													if (BGL_CLASSP(BgL_superz00_103))
														{	/* Llib/object.scm 1017 */
															long BgL_arg1699z00_1535;

															BgL_arg1699z00_1535 =
																BGL_CLASS_DEPTH(BgL_superz00_103);
															BgL_depthz00_1504 = (BgL_arg1699z00_1535 + 1L);
														}
													else
														{	/* Llib/object.scm 1016 */
															BgL_depthz00_1504 = 0L;
														}
													{	/* Llib/object.scm 1016 */
														obj_t BgL_fsz00_1505;

														if (BGL_CLASSP(BgL_superz00_103))
															{	/* Llib/object.scm 1020 */
																obj_t BgL_arg1691z00_1532;

																{	/* Llib/object.scm 572 */
																	obj_t BgL_tmpz00_7371;

																	BgL_tmpz00_7371 = ((obj_t) BgL_superz00_103);
																	BgL_arg1691z00_1532 =
																		BGL_CLASS_ALL_FIELDS(BgL_tmpz00_7371);
																}
																{	/* Llib/object.scm 1020 */
																	obj_t BgL_list1692z00_1533;

																	BgL_list1692z00_1533 =
																		MAKE_YOUNG_PAIR(BgL_plainz00_110, BNIL);
																	BgL_fsz00_1505 =
																		BGl_vectorzd2appendzd2zz__r4_vectors_6_8z00
																		(BgL_arg1691z00_1532, BgL_list1692z00_1533);
																}
															}
														else
															{	/* Llib/object.scm 1019 */
																BgL_fsz00_1505 = BgL_plainz00_110;
															}
														{	/* Llib/object.scm 1019 */
															obj_t BgL_vsz00_1506;

															BgL_vsz00_1506 =
																BGl_makezd2classzd2virtualzd2slotszd2vectorz00zz__objectz00
																(BgL_superz00_103, BgL_virtualz00_111);
															{	/* Llib/object.scm 1022 */
																obj_t BgL_classz00_1507;

																{	/* Llib/object.scm 1023 */
																	long BgL_inheritancezd2numzd2_3301;

																	BgL_inheritancezd2numzd2_3301 =
																		(long)
																		CINT
																		(BGl_za2inheritancezd2cntza2zd2zz__objectz00);
																	{	/* Llib/object.scm 457 */
																		obj_t BgL_auxz00_7382;
																		obj_t BgL_auxz00_7380;
																		obj_t BgL_tmpz00_7378;

																		BgL_auxz00_7382 = ((obj_t) BgL_fsz00_1505);
																		BgL_auxz00_7380 =
																			((obj_t) BgL_plainz00_110);
																		BgL_tmpz00_7378 =
																			((obj_t) BgL_allocatorz00_106);
																		BgL_classz00_1507 =
																			bgl_make_class(BgL_namez00_101,
																			BgL_modulez00_102, BgL_numz00_1503,
																			BgL_inheritancezd2numzd2_3301,
																			BgL_superz00_103, BNIL, BgL_tmpz00_7378,
																			BgL_hashz00_104, BgL_auxz00_7380,
																			BgL_auxz00_7382, BgL_constructorz00_107,
																			BgL_vsz00_1506, BgL_creatorz00_105,
																			BgL_nilz00_108, BgL_shrinkz00_109,
																			BgL_depthz00_1504, BFALSE);
																}}
																{	/* Llib/object.scm 1023 */

																	if (BGL_CLASSP(BgL_superz00_103))
																		{	/* Llib/object.scm 1042 */
																			obj_t BgL_arg1667z00_1509;

																			{	/* Llib/object.scm 1042 */
																				obj_t BgL_arg1668z00_1510;

																				{	/* Llib/object.scm 697 */
																					obj_t BgL_tmpz00_7387;

																					BgL_tmpz00_7387 =
																						((obj_t) BgL_superz00_103);
																					BgL_arg1668z00_1510 =
																						BGL_CLASS_SUBCLASSES
																						(BgL_tmpz00_7387);
																				}
																				BgL_arg1667z00_1509 =
																					MAKE_YOUNG_PAIR(BgL_classz00_1507,
																					BgL_arg1668z00_1510);
																			}
																			BGL_CLASS_SUBCLASSES_SET(BgL_superz00_103,
																				BgL_arg1667z00_1509);
																		}
																	else
																		{	/* Llib/object.scm 1039 */
																			BFALSE;
																		}
																	VECTOR_SET(BGl_za2classesza2z00zz__objectz00,
																		(long)
																		CINT
																		(BGl_za2nbzd2classesza2zd2zz__objectz00),
																		BgL_classz00_1507);
																	BGl_za2nbzd2classesza2zd2zz__objectz00 =
																		ADDFX
																		(BGl_za2nbzd2classesza2zd2zz__objectz00,
																		BINT(1L));
																	if ((BgL_depthz00_1504 >
																			(long)
																			CINT
																			(BGl_za2inheritancezd2maxzd2depthza2z00zz__objectz00)))
																		{	/* Llib/object.scm 1051 */
																			BGl_za2inheritancezd2maxzd2depthza2z00zz__objectz00
																				= BINT(BgL_depthz00_1504);
																		}
																	else
																		{	/* Llib/object.scm 1051 */
																			BFALSE;
																		}
																	{	/* Llib/object.scm 1053 */
																		long BgL_idxz00_1512;

																		BgL_idxz00_1512 =
																			(
																			(long)
																			CINT
																			(BGl_za2inheritancezd2cntza2zd2zz__objectz00)
																			+ BgL_depthz00_1504);
																		{	/* Llib/object.scm 1055 */
																			bool_t BgL_test3615z00_7402;

																			{	/* Llib/object.scm 1055 */
																				long BgL_arg1678z00_1519;

																				BgL_arg1678z00_1519 =
																					VECTOR_LENGTH
																					(BGl_za2inheritancesza2z00zz__objectz00);
																				BgL_test3615z00_7402 =
																					(BgL_idxz00_1512 >=
																					BgL_arg1678z00_1519);
																			}
																			if (BgL_test3615z00_7402)
																				{	/* Llib/object.scm 1056 */
																					obj_t BgL_ovecz00_1515;

																					BgL_ovecz00_1515 =
																						BGl_za2inheritancesza2z00zz__objectz00;
																					{	/* Llib/object.scm 1056 */
																						obj_t BgL_nvecz00_1516;

																						{	/* Llib/object.scm 1058 */
																							long BgL_arg1675z00_1517;

																							{	/* Llib/object.scm 1058 */
																								long BgL_za72za7_3323;

																								BgL_za72za7_3323 =
																									(long)
																									CINT
																									(BGl_za2inheritancezd2maxzd2depthza2z00zz__objectz00);
																								BgL_arg1675z00_1517 =
																									(VECTOR_LENGTH(((obj_t)
																											BgL_ovecz00_1515)) +
																									BgL_za72za7_3323);
																							}
																							{	/* Llib/object.scm 829 */
																								long BgL_newzd2lenzd2_3325;

																								BgL_newzd2lenzd2_3325 =
																									(BgL_arg1675z00_1517 +
																									VECTOR_LENGTH(
																										((obj_t)
																											BgL_ovecz00_1515)));
																								{	/* Llib/object.scm 830 */
																									obj_t BgL_newzd2veczd2_3326;

																									BgL_newzd2veczd2_3326 =
																										make_vector_uncollectable
																										(BgL_newzd2lenzd2_3325,
																										BFALSE);
																									{	/* Llib/object.scm 831 */

																										{
																											long BgL_iz00_3328;

																											BgL_iz00_3328 = 0L;
																										BgL_loopz00_3327:
																											if (
																												(BgL_iz00_3328 ==
																													VECTOR_LENGTH(
																														((obj_t)
																															BgL_ovecz00_1515))))
																												{	/* Llib/object.scm 833 */
																													BgL_nvecz00_1516 =
																														BgL_newzd2veczd2_3326;
																												}
																											else
																												{	/* Llib/object.scm 833 */
																													{	/* Llib/object.scm 836 */
																														obj_t
																															BgL_arg1543z00_3330;
																														BgL_arg1543z00_3330
																															=
																															VECTOR_REF((
																																(obj_t)
																																BgL_ovecz00_1515),
																															BgL_iz00_3328);
																														VECTOR_SET
																															(BgL_newzd2veczd2_3326,
																															BgL_iz00_3328,
																															BgL_arg1543z00_3330);
																													}
																													{
																														long BgL_iz00_7420;

																														BgL_iz00_7420 =
																															(BgL_iz00_3328 +
																															1L);
																														BgL_iz00_3328 =
																															BgL_iz00_7420;
																														goto
																															BgL_loopz00_3327;
																													}
																												}
																										}
																									}
																								}
																							}
																						}
																						{	/* Llib/object.scm 1057 */

																							BGl_za2inheritancesza2z00zz__objectz00
																								= BgL_nvecz00_1516;
																							FREE_VECTOR_UNCOLLECTABLE
																								(BgL_ovecz00_1515);
																							BUNSPEC;
																						}
																					}
																				}
																			else
																				{	/* Llib/object.scm 1055 */
																					BFALSE;
																				}
																		}
																		{
																			long BgL_iz00_3350;
																			obj_t BgL_jz00_3351;

																			BgL_iz00_3350 = 0L;
																			BgL_jz00_3351 =
																				BGl_za2inheritancezd2cntza2zd2zz__objectz00;
																		BgL_loopz00_3349:
																			if ((BgL_iz00_3350 <= BgL_depthz00_1504))
																				{	/* Llib/object.scm 1065 */
																					{	/* Llib/object.scm 1068 */
																						obj_t BgL_arg1681z00_3355;

																						BgL_arg1681z00_3355 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_classz00_1507,
																							BgL_iz00_3350);
																						VECTOR_SET
																							(BGl_za2inheritancesza2z00zz__objectz00,
																							(long) CINT(BgL_jz00_3351),
																							BgL_arg1681z00_3355);
																					}
																					{	/* Llib/object.scm 1069 */
																						long BgL_arg1684z00_3359;
																						long BgL_arg1685z00_3360;

																						BgL_arg1684z00_3359 =
																							(BgL_iz00_3350 + 1L);
																						BgL_arg1685z00_3360 =
																							((long) CINT(BgL_jz00_3351) + 1L);
																						{
																							obj_t BgL_jz00_7432;
																							long BgL_iz00_7431;

																							BgL_iz00_7431 =
																								BgL_arg1684z00_3359;
																							BgL_jz00_7432 =
																								BINT(BgL_arg1685z00_3360);
																							BgL_jz00_3351 = BgL_jz00_7432;
																							BgL_iz00_3350 = BgL_iz00_7431;
																							goto BgL_loopz00_3349;
																						}
																					}
																				}
																			else
																				{	/* Llib/object.scm 1065 */
																					BGl_za2inheritancezd2cntza2zd2zz__objectz00
																						= BgL_jz00_3351;
																				}
																		}
																	}
																	{	/* Llib/object.scm 1072 */
																		long BgL_arg1688z00_1529;

																		if (BGL_CLASSP(BgL_superz00_103))
																			{	/* Llib/object.scm 522 */
																				obj_t BgL_tmpz00_7436;

																				BgL_tmpz00_7436 =
																					((obj_t) BgL_superz00_103);
																				BgL_arg1688z00_1529 =
																					BGL_CLASS_INDEX(BgL_tmpz00_7436);
																			}
																		else
																			{	/* Llib/object.scm 1072 */
																				BgL_arg1688z00_1529 = BgL_numz00_1503;
																			}
																		BGl_genericszd2addzd2classz12z12zz__objectz00
																			(BgL_numz00_1503, BgL_arg1688z00_1529);
																	}
																	BgL_tmp3602z00_7321 = BgL_classz00_1507;
																}
															}
														}
													}
												}
											}
										}
								}
							else
								{	/* Llib/object.scm 1083 */
									goto BgL_zc3z04anonymousza31664ze3z87_1501;
								}
						}
						BGL_EXITD_POP_PROTECT(BgL_top3603z00_7322);
						BUNSPEC;
						BGL_MUTEX_UNLOCK(bigloo_generic_mutex);
						return BgL_tmp3602z00_7321;
					}
				}
			}
		}

	}



/* &register-class! */
	obj_t BGl_z62registerzd2classz12za2zz__objectz00(obj_t BgL_envz00_5139,
		obj_t BgL_namez00_5140, obj_t BgL_modulez00_5141, obj_t BgL_superz00_5142,
		obj_t BgL_hashz00_5143, obj_t BgL_creatorz00_5144,
		obj_t BgL_allocatorz00_5145, obj_t BgL_constructorz00_5146,
		obj_t BgL_nilz00_5147, obj_t BgL_shrinkz00_5148, obj_t BgL_plainz00_5149,
		obj_t BgL_virtualz00_5150)
	{
		{	/* Llib/object.scm 1010 */
			{	/* Llib/object.scm 1013 */
				obj_t BgL_auxz00_7472;
				obj_t BgL_auxz00_7465;
				long BgL_auxz00_7456;
				obj_t BgL_auxz00_7449;
				obj_t BgL_auxz00_7442;

				if (VECTORP(BgL_virtualz00_5150))
					{	/* Llib/object.scm 1013 */
						BgL_auxz00_7472 = BgL_virtualz00_5150;
					}
				else
					{
						obj_t BgL_auxz00_7475;

						BgL_auxz00_7475 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(44475L), BGl_string2930z00zz__objectz00,
							BGl_string2873z00zz__objectz00, BgL_virtualz00_5150);
						FAILURE(BgL_auxz00_7475, BFALSE, BFALSE);
					}
				if (PROCEDUREP(BgL_nilz00_5147))
					{	/* Llib/object.scm 1013 */
						BgL_auxz00_7465 = BgL_nilz00_5147;
					}
				else
					{
						obj_t BgL_auxz00_7468;

						BgL_auxz00_7468 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(44475L), BGl_string2930z00zz__objectz00,
							BGl_string2907z00zz__objectz00, BgL_nilz00_5147);
						FAILURE(BgL_auxz00_7468, BFALSE, BFALSE);
					}
				{	/* Llib/object.scm 1013 */
					obj_t BgL_tmpz00_7457;

					if (INTEGERP(BgL_hashz00_5143))
						{	/* Llib/object.scm 1013 */
							BgL_tmpz00_7457 = BgL_hashz00_5143;
						}
					else
						{
							obj_t BgL_auxz00_7460;

							BgL_auxz00_7460 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
								BINT(44475L), BGl_string2930z00zz__objectz00,
								BGl_string2860z00zz__objectz00, BgL_hashz00_5143);
							FAILURE(BgL_auxz00_7460, BFALSE, BFALSE);
						}
					BgL_auxz00_7456 = (long) CINT(BgL_tmpz00_7457);
				}
				if (SYMBOLP(BgL_modulez00_5141))
					{	/* Llib/object.scm 1013 */
						BgL_auxz00_7449 = BgL_modulez00_5141;
					}
				else
					{
						obj_t BgL_auxz00_7452;

						BgL_auxz00_7452 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(44475L), BGl_string2930z00zz__objectz00,
							BGl_string2855z00zz__objectz00, BgL_modulez00_5141);
						FAILURE(BgL_auxz00_7452, BFALSE, BFALSE);
					}
				if (SYMBOLP(BgL_namez00_5140))
					{	/* Llib/object.scm 1013 */
						BgL_auxz00_7442 = BgL_namez00_5140;
					}
				else
					{
						obj_t BgL_auxz00_7445;

						BgL_auxz00_7445 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(44475L), BGl_string2930z00zz__objectz00,
							BGl_string2855z00zz__objectz00, BgL_namez00_5140);
						FAILURE(BgL_auxz00_7445, BFALSE, BFALSE);
					}
				return
					BGl_registerzd2classz12zc0zz__objectz00(BgL_auxz00_7442,
					BgL_auxz00_7449, BgL_superz00_5142, BgL_auxz00_7456,
					BgL_creatorz00_5144, BgL_allocatorz00_5145, BgL_constructorz00_5146,
					BgL_auxz00_7465, BgL_shrinkz00_5148, BgL_plainz00_5149,
					BgL_auxz00_7472);
			}
		}

	}



/* make-class-virtual-slots-vector */
	obj_t BGl_makezd2classzd2virtualzd2slotszd2vectorz00zz__objectz00(obj_t
		BgL_superz00_112, obj_t BgL_virtualsz00_113)
	{
		{	/* Llib/object.scm 1096 */
			{
				obj_t BgL_vecz00_1555;

				if (BGL_CLASSP(BgL_superz00_112))
					{	/* Llib/object.scm 1109 */
						obj_t BgL_ovecz00_1539;

						{	/* Llib/object.scm 534 */
							obj_t BgL_tmpz00_7482;

							BgL_tmpz00_7482 = ((obj_t) BgL_superz00_112);
							BgL_ovecz00_1539 = BGL_CLASS_VIRTUAL_FIELDS(BgL_tmpz00_7482);
						}
						{	/* Llib/object.scm 1111 */
							obj_t BgL_vecz00_1542;

							{	/* Llib/object.scm 1112 */
								long BgL_arg1705z00_1550;

								BgL_arg1705z00_1550 =
									(VECTOR_LENGTH(BgL_ovecz00_1539) +
									VECTOR_LENGTH(((obj_t) BgL_virtualsz00_113)));
								BgL_vecz00_1542 = make_vector(BgL_arg1705z00_1550, BUNSPEC);
							}
							{	/* Llib/object.scm 1112 */

								{
									long BgL_iz00_3388;

									BgL_iz00_3388 = 0L;
								BgL_loopz00_3387:
									if ((BgL_iz00_3388 == VECTOR_LENGTH(BgL_ovecz00_1539)))
										{	/* Llib/object.scm 1114 */
											BgL_vecz00_1555 = BgL_vecz00_1542;
										BgL_lambda1708z00_1556:
											{	/* Llib/object.scm 1099 */
												obj_t BgL_g1324z00_1557;

												BgL_g1324z00_1557 =
													BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00
													(BgL_virtualsz00_113);
												{
													obj_t BgL_l1322z00_1559;

													BgL_l1322z00_1559 = BgL_g1324z00_1557;
												BgL_zc3z04anonymousza31709ze3z87_1560:
													if (PAIRP(BgL_l1322z00_1559))
														{	/* Llib/object.scm 1102 */
															{	/* Llib/object.scm 1100 */
																obj_t BgL_virtualz00_1562;

																BgL_virtualz00_1562 = CAR(BgL_l1322z00_1559);
																{	/* Llib/object.scm 1100 */
																	obj_t BgL_numz00_1563;

																	BgL_numz00_1563 =
																		CAR(((obj_t) BgL_virtualz00_1562));
																	{	/* Llib/object.scm 1101 */
																		obj_t BgL_arg1711z00_1564;

																		BgL_arg1711z00_1564 =
																			CDR(((obj_t) BgL_virtualz00_1562));
																		VECTOR_SET(BgL_vecz00_1555,
																			(long) CINT(BgL_numz00_1563),
																			BgL_arg1711z00_1564);
															}}}
															{
																obj_t BgL_l1322z00_7503;

																BgL_l1322z00_7503 = CDR(BgL_l1322z00_1559);
																BgL_l1322z00_1559 = BgL_l1322z00_7503;
																goto BgL_zc3z04anonymousza31709ze3z87_1560;
															}
														}
													else
														{	/* Llib/object.scm 1102 */
															((bool_t) 1);
														}
												}
											}
											return BgL_vecz00_1555;
										}
									else
										{	/* Llib/object.scm 1114 */
											VECTOR_SET(BgL_vecz00_1542, BgL_iz00_3388,
												VECTOR_REF(BgL_ovecz00_1539, BgL_iz00_3388));
											{
												long BgL_iz00_7507;

												BgL_iz00_7507 = (BgL_iz00_3388 + 1L);
												BgL_iz00_3388 = BgL_iz00_7507;
												goto BgL_loopz00_3387;
											}
										}
								}
							}
						}
					}
				else
					{	/* Llib/object.scm 1106 */
						obj_t BgL_vecz00_1553;

						BgL_vecz00_1553 =
							make_vector(VECTOR_LENGTH(
								((obj_t) BgL_virtualsz00_113)), BUNSPEC);
						{	/* Llib/object.scm 1107 */

							{
								obj_t BgL_vecz00_7512;

								BgL_vecz00_7512 = BgL_vecz00_1553;
								BgL_vecz00_1555 = BgL_vecz00_7512;
								goto BgL_lambda1708z00_1556;
							}
						}
					}
			}
		}

	}



/* make-method-array */
	obj_t BGl_makezd2methodzd2arrayz00zz__objectz00(obj_t BgL_defzd2bucketzd2_115)
	{
		{	/* Llib/object.scm 1129 */
			{	/* Llib/object.scm 1130 */
				long BgL_sz00_1569;
				long BgL_az00_1570;

				{	/* Llib/object.scm 1130 */
					int BgL_arg1724z00_1577;

					BgL_arg1724z00_1577 = (int) ((1L << (int) ((long) ((int) (4L)))));
					{	/* Llib/object.scm 1130 */
						long BgL_auxz00_7520;
						long BgL_tmpz00_7518;

						BgL_auxz00_7520 = (long) (BgL_arg1724z00_1577);
						BgL_tmpz00_7518 =
							(long) CINT(BGl_za2nbzd2classeszd2maxza2z00zz__objectz00);
						BgL_sz00_1569 = (BgL_tmpz00_7518 / BgL_auxz00_7520);
				}}
				{	/* Llib/object.scm 1131 */
					int BgL_arg1725z00_1578;

					BgL_arg1725z00_1578 = (int) ((1L << (int) ((long) ((int) (4L)))));
					{	/* Llib/object.scm 1131 */
						long BgL_n1z00_3413;
						long BgL_n2z00_3414;

						BgL_n1z00_3413 =
							(long) CINT(BGl_za2nbzd2classeszd2maxza2z00zz__objectz00);
						BgL_n2z00_3414 = (long) (BgL_arg1725z00_1578);
						{	/* Llib/object.scm 1131 */
							bool_t BgL_test3627z00_7530;

							{	/* Llib/object.scm 1131 */
								long BgL_arg2381z00_3416;

								BgL_arg2381z00_3416 =
									(((BgL_n1z00_3413) | (BgL_n2z00_3414)) & -2147483648);
								BgL_test3627z00_7530 = (BgL_arg2381z00_3416 == 0L);
							}
							if (BgL_test3627z00_7530)
								{	/* Llib/object.scm 1131 */
									int32_t BgL_arg2378z00_3417;

									{	/* Llib/object.scm 1131 */
										int32_t BgL_arg2379z00_3418;
										int32_t BgL_arg2380z00_3419;

										BgL_arg2379z00_3418 = (int32_t) (BgL_n1z00_3413);
										BgL_arg2380z00_3419 = (int32_t) (BgL_n2z00_3414);
										BgL_arg2378z00_3417 =
											(BgL_arg2379z00_3418 % BgL_arg2380z00_3419);
									}
									{	/* Llib/object.scm 1131 */
										long BgL_arg2502z00_3424;

										BgL_arg2502z00_3424 = (long) (BgL_arg2378z00_3417);
										BgL_az00_1570 = (long) (BgL_arg2502z00_3424);
								}}
							else
								{	/* Llib/object.scm 1131 */
									BgL_az00_1570 = (BgL_n1z00_3413 % BgL_n2z00_3414);
								}
						}
					}
				}
				if ((BgL_az00_1570 > 0L))
					{	/* Llib/object.scm 1132 */
						{	/* Llib/object.scm 1136 */
							int BgL_arg1717z00_1572;

							BgL_arg1717z00_1572 = (int) ((1L << (int) ((long) ((int) (4L)))));
							{	/* Llib/object.scm 1134 */
								obj_t BgL_list1718z00_1573;

								{	/* Llib/object.scm 1134 */
									obj_t BgL_arg1720z00_1574;

									{	/* Llib/object.scm 1134 */
										obj_t BgL_arg1722z00_1575;

										BgL_arg1722z00_1575 =
											MAKE_YOUNG_PAIR(BINT(BgL_arg1717z00_1572), BNIL);
										BgL_arg1720z00_1574 =
											MAKE_YOUNG_PAIR(BGl_string2931z00zz__objectz00,
											BgL_arg1722z00_1575);
									}
									BgL_list1718z00_1573 =
										MAKE_YOUNG_PAIR(BGl_string2932z00zz__objectz00,
										BgL_arg1720z00_1574);
								}
								BGl_warningz00zz__errorz00(BgL_list1718z00_1573);
						}}
						return
							make_vector_uncollectable(
							(BgL_sz00_1569 + 1L), BgL_defzd2bucketzd2_115);
					}
				else
					{	/* Llib/object.scm 1132 */
						return
							make_vector_uncollectable(BgL_sz00_1569, BgL_defzd2bucketzd2_115);
					}
			}
		}

	}



/* &generic-no-default-behavior */
	obj_t BGl_z62genericzd2nozd2defaultzd2behaviorzb0zz__objectz00(obj_t
		BgL_envz00_5151, obj_t BgL_lz00_5152)
	{
		{	/* Llib/object.scm 1143 */
			return
				BGl_errorz00zz__errorz00(BGl_string2911z00zz__objectz00,
				BGl_string2933z00zz__objectz00, BgL_lz00_5152);
		}

	}



/* procedure->generic */
	BGL_EXPORTED_DEF obj_t BGl_procedurezd2ze3genericz31zz__objectz00(obj_t
		BgL_procz00_117)
	{
		{	/* Llib/object.scm 1149 */
			BGL_TAIL return bgl_make_generic(BgL_procz00_117);
		}

	}



/* &procedure->generic */
	obj_t BGl_z62procedurezd2ze3genericz53zz__objectz00(obj_t BgL_envz00_5153,
		obj_t BgL_procz00_5154)
	{
		{	/* Llib/object.scm 1149 */
			{	/* Llib/object.scm 1150 */
				obj_t BgL_auxz00_7556;

				if (PROCEDUREP(BgL_procz00_5154))
					{	/* Llib/object.scm 1150 */
						BgL_auxz00_7556 = BgL_procz00_5154;
					}
				else
					{
						obj_t BgL_auxz00_7559;

						BgL_auxz00_7559 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(49610L), BGl_string2934z00zz__objectz00,
							BGl_string2907z00zz__objectz00, BgL_procz00_5154);
						FAILURE(BgL_auxz00_7559, BFALSE, BFALSE);
					}
				return BGl_procedurezd2ze3genericz31zz__objectz00(BgL_auxz00_7556);
			}
		}

	}



/* register-generic! */
	BGL_EXPORTED_DEF obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t
		BgL_genericz00_118, obj_t BgL_defaultz00_119, obj_t BgL_classzd2minzd2_120,
		obj_t BgL_namez00_121)
	{
		{	/* Llib/object.scm 1155 */
			{	/* Llib/object.scm 1156 */
				obj_t BgL_top3631z00_7565;

				BgL_top3631z00_7565 = BGL_EXITD_TOP_AS_OBJ();
				BGL_MUTEX_LOCK(bigloo_generic_mutex);
				BGL_EXITD_PUSH_PROTECT(BgL_top3631z00_7565, bigloo_generic_mutex);
				BUNSPEC;
				{	/* Llib/object.scm 1156 */
					obj_t BgL_tmp3630z00_7564;

					BgL_tmp3630z00_7564 =
						BGl_registerzd2genericzd2sanszd2lockz12zc0zz__objectz00
						(BgL_genericz00_118, BgL_defaultz00_119, BgL_namez00_121);
					BGL_EXITD_POP_PROTECT(BgL_top3631z00_7565);
					BUNSPEC;
					BGL_MUTEX_UNLOCK(bigloo_generic_mutex);
					return BgL_tmp3630z00_7564;
				}
			}
		}

	}



/* &register-generic! */
	obj_t BGl_z62registerzd2genericz12za2zz__objectz00(obj_t BgL_envz00_5155,
		obj_t BgL_genericz00_5156, obj_t BgL_defaultz00_5157,
		obj_t BgL_classzd2minzd2_5158, obj_t BgL_namez00_5159)
	{
		{	/* Llib/object.scm 1155 */
			{	/* Llib/object.scm 1156 */
				obj_t BgL_auxz00_7579;
				obj_t BgL_auxz00_7572;

				if (PROCEDUREP(BgL_defaultz00_5157))
					{	/* Llib/object.scm 1156 */
						BgL_auxz00_7579 = BgL_defaultz00_5157;
					}
				else
					{
						obj_t BgL_auxz00_7582;

						BgL_auxz00_7582 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(49917L), BGl_string2935z00zz__objectz00,
							BGl_string2907z00zz__objectz00, BgL_defaultz00_5157);
						FAILURE(BgL_auxz00_7582, BFALSE, BFALSE);
					}
				if (PROCEDUREP(BgL_genericz00_5156))
					{	/* Llib/object.scm 1156 */
						BgL_auxz00_7572 = BgL_genericz00_5156;
					}
				else
					{
						obj_t BgL_auxz00_7575;

						BgL_auxz00_7575 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(49917L), BGl_string2935z00zz__objectz00,
							BGl_string2907z00zz__objectz00, BgL_genericz00_5156);
						FAILURE(BgL_auxz00_7575, BFALSE, BFALSE);
					}
				return
					BGl_registerzd2genericz12zc0zz__objectz00(BgL_auxz00_7572,
					BgL_auxz00_7579, BgL_classzd2minzd2_5158, BgL_namez00_5159);
			}
		}

	}



/* register-generic-sans-lock! */
	obj_t BGl_registerzd2genericzd2sanszd2lockz12zc0zz__objectz00(obj_t
		BgL_genericz00_122, obj_t BgL_defaultz00_123, obj_t BgL_namez00_124)
	{
		{	/* Llib/object.scm 1169 */
			{	/* Llib/object.scm 1170 */
				bool_t BgL_test3634z00_7587;

				{	/* Llib/object.scm 1124 */
					obj_t BgL_tmpz00_7588;

					BgL_tmpz00_7588 =
						PROCEDURE_REF(((obj_t) BgL_genericz00_122), (int) (1L));
					BgL_test3634z00_7587 = VECTORP(BgL_tmpz00_7588);
				}
				if (BgL_test3634z00_7587)
					{	/* Llib/object.scm 1170 */
						if (PROCEDUREP(BgL_defaultz00_123))
							{	/* Llib/object.scm 1189 */
								obj_t BgL_oldzd2defzd2bucketz00_1581;
								obj_t BgL_newzd2defzd2bucketz00_1582;
								obj_t BgL_oldzd2defaultzd2_1583;

								BgL_oldzd2defzd2bucketz00_1581 =
									PROCEDURE_REF(BgL_genericz00_122, (int) (2L));
								{	/* Llib/object.scm 1191 */
									int BgL_arg1743z00_1606;

									BgL_arg1743z00_1606 =
										(int) ((1L << (int) ((long) ((int) (4L)))));
									BgL_newzd2defzd2bucketz00_1582 =
										make_vector_uncollectable(
										(long) (BgL_arg1743z00_1606), BgL_defaultz00_123);
								}
								{	/* Llib/object.scm 1192 */
									obj_t BgL_res2570z00_3441;

									BgL_res2570z00_3441 =
										PROCEDURE_REF(((obj_t) BgL_genericz00_122), (int) (0L));
									BgL_oldzd2defaultzd2_1583 = BgL_res2570z00_3441;
								}
								{	/* Llib/object.scm 1193 */
									obj_t BgL_marrayz00_1584;

									BgL_marrayz00_1584 =
										PROCEDURE_REF(((obj_t) BgL_genericz00_122), (int) (1L));
									{	/* Llib/object.scm 1194 */

										{
											long BgL_iz00_1587;

											BgL_iz00_1587 = 0L;
										BgL_zc3z04anonymousza31728ze3z87_1588:
											if (
												(BgL_iz00_1587 <
													VECTOR_LENGTH(((obj_t) BgL_marrayz00_1584))))
												{	/* Llib/object.scm 1197 */
													obj_t BgL_bucketz00_1590;

													BgL_bucketz00_1590 =
														VECTOR_REF(
														((obj_t) BgL_marrayz00_1584), BgL_iz00_1587);
													if (
														(BgL_bucketz00_1590 ==
															BgL_oldzd2defzd2bucketz00_1581))
														{	/* Llib/object.scm 1198 */
															VECTOR_SET(
																((obj_t) BgL_marrayz00_1584), BgL_iz00_1587,
																BgL_newzd2defzd2bucketz00_1582);
															{
																long BgL_iz00_7620;

																BgL_iz00_7620 = (BgL_iz00_1587 + 1L);
																BgL_iz00_1587 = BgL_iz00_7620;
																goto BgL_zc3z04anonymousza31728ze3z87_1588;
															}
														}
													else
														{
															long BgL_jz00_1593;

															BgL_jz00_1593 = 0L;
														BgL_zc3z04anonymousza31731ze3z87_1594:
															if (
																(BgL_jz00_1593 ==
																	(long) (
																		(int) (
																			(1L << (int) ((long) ((int) (4L))))))))
																{
																	long BgL_iz00_7630;

																	BgL_iz00_7630 = (BgL_iz00_1587 + 1L);
																	BgL_iz00_1587 = BgL_iz00_7630;
																	goto BgL_zc3z04anonymousza31728ze3z87_1588;
																}
															else
																{	/* Llib/object.scm 1206 */
																	bool_t BgL_test3639z00_7632;

																	{	/* Llib/object.scm 1206 */
																		obj_t BgL_arg1740z00_1602;

																		BgL_arg1740z00_1602 =
																			VECTOR_REF(
																			((obj_t) BgL_bucketz00_1590),
																			BgL_jz00_1593);
																		BgL_test3639z00_7632 =
																			(BgL_arg1740z00_1602 ==
																			BgL_oldzd2defaultzd2_1583);
																	}
																	if (BgL_test3639z00_7632)
																		{	/* Llib/object.scm 1206 */
																			VECTOR_SET(
																				((obj_t) BgL_bucketz00_1590),
																				BgL_jz00_1593, BgL_defaultz00_123);
																			{
																				long BgL_jz00_7638;

																				BgL_jz00_7638 = (BgL_jz00_1593 + 1L);
																				BgL_jz00_1593 = BgL_jz00_7638;
																				goto
																					BgL_zc3z04anonymousza31731ze3z87_1594;
																			}
																		}
																	else
																		{
																			long BgL_jz00_7640;

																			BgL_jz00_7640 = (BgL_jz00_1593 + 1L);
																			BgL_jz00_1593 = BgL_jz00_7640;
																			goto
																				BgL_zc3z04anonymousza31731ze3z87_1594;
																		}
																}
														}
												}
											else
												{	/* Llib/object.scm 1196 */
													PROCEDURE_SET(
														((obj_t) BgL_genericz00_122),
														(int) (0L), ((obj_t) ((obj_t) BgL_defaultz00_123)));
													PROCEDURE_SET(BgL_genericz00_122,
														(int) (2L), BgL_newzd2defzd2bucketz00_1582);
													FREE_VECTOR_UNCOLLECTABLE
														(BgL_oldzd2defzd2bucketz00_1581);
													BUNSPEC;
							}}}}}
						else
							{	/* Llib/object.scm 1185 */
								BFALSE;
							}
						return BUNSPEC;
					}
				else
					{	/* Llib/object.scm 1171 */
						obj_t BgL_defzd2metzd2_1607;

						if (PROCEDUREP(BgL_defaultz00_123))
							{	/* Llib/object.scm 1171 */
								BgL_defzd2metzd2_1607 = BgL_defaultz00_123;
							}
						else
							{	/* Llib/object.scm 1171 */
								BgL_defzd2metzd2_1607 =
									BGl_genericzd2nozd2defaultzd2behaviorzd2envz00zz__objectz00;
							}
						{	/* Llib/object.scm 1171 */
							obj_t BgL_defzd2bucketzd2_1608;

							{	/* Llib/object.scm 1175 */
								int BgL_arg1746z00_1611;

								BgL_arg1746z00_1611 =
									(int) ((1L << (int) ((long) ((int) (4L)))));
								BgL_defzd2bucketzd2_1608 =
									make_vector_uncollectable(
									(long) (BgL_arg1746z00_1611), BgL_defzd2metzd2_1607);
							}
							{	/* Llib/object.scm 1174 */

								if (
									((long) CINT(BGl_za2nbzd2genericsza2zd2zz__objectz00) ==
										(long) CINT(BGl_za2nbzd2genericszd2maxza2z00zz__objectz00)))
									{	/* Llib/object.scm 1176 */
										BGl_doublezd2nbzd2genericsz12z12zz__objectz00();
									}
								else
									{	/* Llib/object.scm 1176 */
										BFALSE;
									}
								VECTOR_SET(BGl_za2genericsza2z00zz__objectz00,
									(long) CINT(BGl_za2nbzd2genericsza2zd2zz__objectz00),
									BgL_genericz00_122);
								BGl_za2nbzd2genericsza2zd2zz__objectz00 =
									ADDFX(BGl_za2nbzd2genericsza2zd2zz__objectz00, BINT(1L));
								PROCEDURE_SET(((obj_t) BgL_genericz00_122), (int) (0L),
									((obj_t) ((obj_t) BgL_defzd2metzd2_1607)));
								PROCEDURE_SET(BgL_genericz00_122, (int) (2L),
									BgL_defzd2bucketzd2_1608);
								{	/* Llib/object.scm 1182 */
									obj_t BgL_arg1745z00_1610;

									BgL_arg1745z00_1610 =
										BGl_makezd2methodzd2arrayz00zz__objectz00
										(BgL_defzd2bucketzd2_1608);
									PROCEDURE_SET(((obj_t) BgL_genericz00_122), (int) (1L),
										BgL_arg1745z00_1610);
								}
								return BUNSPEC;
							}
						}
					}
			}
		}

	}



/* %add-method! */
	obj_t BGl_z52addzd2methodz12z92zz__objectz00(obj_t BgL_genericz00_125,
		obj_t BgL_classz00_126, obj_t BgL_methodz00_127)
	{
		{	/* Llib/object.scm 1221 */
			{	/* Llib/object.scm 1222 */
				obj_t BgL_top3643z00_7680;

				BgL_top3643z00_7680 = BGL_EXITD_TOP_AS_OBJ();
				BGL_MUTEX_LOCK(bigloo_generic_mutex);
				BGL_EXITD_PUSH_PROTECT(BgL_top3643z00_7680, bigloo_generic_mutex);
				BUNSPEC;
				{	/* Llib/object.scm 1222 */
					obj_t BgL_tmp3642z00_7679;

					{	/* Llib/object.scm 1223 */
						bool_t BgL_test3644z00_7684;

						{	/* Llib/object.scm 1124 */
							obj_t BgL_tmpz00_7685;

							BgL_tmpz00_7685 =
								PROCEDURE_REF(((obj_t) BgL_genericz00_125), (int) (1L));
							BgL_test3644z00_7684 = VECTORP(BgL_tmpz00_7685);
						}
						if (BgL_test3644z00_7684)
							{	/* Llib/object.scm 1223 */
								BFALSE;
							}
						else
							{	/* Llib/object.scm 1223 */
								BGl_registerzd2genericzd2sanszd2lockz12zc0zz__objectz00
									(BgL_genericz00_125, BFALSE, BGl_string2936z00zz__objectz00);
							}
					}
					{	/* Llib/object.scm 1227 */
						obj_t BgL_methodzd2arrayzd2_1614;

						BgL_methodzd2arrayzd2_1614 =
							PROCEDURE_REF(((obj_t) BgL_genericz00_125), (int) (1L));
						{	/* Llib/object.scm 1227 */
							long BgL_cnumz00_1615;

							{	/* Llib/object.scm 522 */
								obj_t BgL_tmpz00_7694;

								BgL_tmpz00_7694 = ((obj_t) BgL_classz00_126);
								BgL_cnumz00_1615 = BGL_CLASS_INDEX(BgL_tmpz00_7694);
							}
							{	/* Llib/object.scm 1228 */
								obj_t BgL_previousz00_1616;

								{	/* Llib/object.scm 1229 */
									int BgL_offsetz00_3488;

									BgL_offsetz00_3488 = (int) (BgL_cnumz00_1615);
									{	/* Llib/object.scm 931 */
										long BgL_offsetz00_3489;

										BgL_offsetz00_3489 =
											((long) (BgL_offsetz00_3488) - OBJECT_TYPE);
										{	/* Llib/object.scm 931 */
											long BgL_modz00_3490;

											BgL_modz00_3490 =
												(BgL_offsetz00_3489 >> (int) ((long) ((int) (4L))));
											{	/* Llib/object.scm 932 */
												long BgL_restz00_3492;

												BgL_restz00_3492 =
													(BgL_offsetz00_3489 &
													(long) (
														(int) (
															((long) (
																	(int) (
																		(1L <<
																			(int) ((long) ((int) (4L)))))) - 1L))));
												{	/* Llib/object.scm 933 */

													{	/* Llib/object.scm 934 */
														obj_t BgL_bucketz00_3494;

														BgL_bucketz00_3494 =
															VECTOR_REF(
															((obj_t) BgL_methodzd2arrayzd2_1614),
															BgL_modz00_3490);
														BgL_previousz00_1616 =
															VECTOR_REF(((obj_t) BgL_bucketz00_3494),
															BgL_restz00_3492);
								}}}}}}
								{	/* Llib/object.scm 1229 */
									obj_t BgL_defz00_1617;

									{	/* Llib/object.scm 1230 */
										obj_t BgL_res2579z00_3514;

										BgL_res2579z00_3514 =
											PROCEDURE_REF(((obj_t) BgL_genericz00_125), (int) (0L));
										BgL_defz00_1617 = BgL_res2579z00_3514;
									}
									{	/* Llib/object.scm 1230 */

										BGl_loopze70ze7zz__objectz00(BgL_defz00_1617,
											BgL_previousz00_1616, BgL_methodz00_127,
											BgL_genericz00_125, BgL_methodzd2arrayzd2_1614,
											BgL_classz00_126);
					}}}}}
					BgL_tmp3642z00_7679 = BgL_methodz00_127;
					BGL_EXITD_POP_PROTECT(BgL_top3643z00_7680);
					BUNSPEC;
					BGL_MUTEX_UNLOCK(bigloo_generic_mutex);
					return BgL_tmp3642z00_7679;
				}
			}
		}

	}



/* loop~0 */
	bool_t BGl_loopze70ze7zz__objectz00(obj_t BgL_defz00_5686,
		obj_t BgL_previousz00_5685, obj_t BgL_methodz00_5684,
		obj_t BgL_genericz00_5683, obj_t BgL_methodzd2arrayzd2_5682,
		obj_t BgL_claza7za7z00_1619)
	{
		{	/* Llib/object.scm 1231 */
			{	/* Llib/object.scm 1232 */
				long BgL_cnz00_1621;

				{	/* Llib/object.scm 522 */
					obj_t BgL_tmpz00_7724;

					BgL_tmpz00_7724 = ((obj_t) BgL_claza7za7z00_1619);
					BgL_cnz00_1621 = BGL_CLASS_INDEX(BgL_tmpz00_7724);
				}
				{	/* Llib/object.scm 1232 */
					obj_t BgL_currentz00_1622;

					{	/* Llib/object.scm 1233 */
						int BgL_offsetz00_3518;

						BgL_offsetz00_3518 = (int) (BgL_cnz00_1621);
						{	/* Llib/object.scm 931 */
							long BgL_offsetz00_3519;

							BgL_offsetz00_3519 = ((long) (BgL_offsetz00_3518) - OBJECT_TYPE);
							{	/* Llib/object.scm 931 */
								long BgL_modz00_3520;

								BgL_modz00_3520 =
									(BgL_offsetz00_3519 >> (int) ((long) ((int) (4L))));
								{	/* Llib/object.scm 932 */
									long BgL_restz00_3522;

									BgL_restz00_3522 =
										(BgL_offsetz00_3519 &
										(long) (
											(int) (
												((long) (
														(int) (
															(1L << (int) ((long) ((int) (4L)))))) - 1L))));
									{	/* Llib/object.scm 933 */

										{	/* Llib/object.scm 934 */
											obj_t BgL_bucketz00_3524;

											BgL_bucketz00_3524 =
												VECTOR_REF(
												((obj_t) BgL_methodzd2arrayzd2_5682), BgL_modz00_3520);
											BgL_currentz00_1622 =
												VECTOR_REF(
												((obj_t) BgL_bucketz00_3524), BgL_restz00_3522);
					}}}}}}
					{	/* Llib/object.scm 1233 */

						{	/* Llib/object.scm 1234 */
							bool_t BgL_test3645z00_7748;

							if ((BgL_currentz00_1622 == BgL_defz00_5686))
								{	/* Llib/object.scm 1234 */
									BgL_test3645z00_7748 = ((bool_t) 1);
								}
							else
								{	/* Llib/object.scm 1234 */
									BgL_test3645z00_7748 =
										(BgL_currentz00_1622 == BgL_previousz00_5685);
								}
							if (BgL_test3645z00_7748)
								{	/* Llib/object.scm 1234 */
									BGl_methodzd2arrayzd2setz12z12zz__objectz00
										(BgL_genericz00_5683, BgL_methodzd2arrayzd2_5682,
										BgL_cnz00_1621, BgL_methodz00_5684);
									{	/* Llib/object.scm 1239 */
										obj_t BgL_g1327z00_1624;

										{	/* Llib/object.scm 697 */
											obj_t BgL_tmpz00_7753;

											BgL_tmpz00_7753 = ((obj_t) BgL_claza7za7z00_1619);
											BgL_g1327z00_1624 = BGL_CLASS_SUBCLASSES(BgL_tmpz00_7753);
										}
										{
											obj_t BgL_l1325z00_3555;

											BgL_l1325z00_3555 = BgL_g1327z00_1624;
										BgL_zc3z04anonymousza31751ze3z87_3554:
											if (PAIRP(BgL_l1325z00_3555))
												{	/* Llib/object.scm 1239 */
													BGl_loopze70ze7zz__objectz00(BgL_defz00_5686,
														BgL_previousz00_5685, BgL_methodz00_5684,
														BgL_genericz00_5683, BgL_methodzd2arrayzd2_5682,
														CAR(BgL_l1325z00_3555));
													{
														obj_t BgL_l1325z00_7760;

														BgL_l1325z00_7760 = CDR(BgL_l1325z00_3555);
														BgL_l1325z00_3555 = BgL_l1325z00_7760;
														goto BgL_zc3z04anonymousza31751ze3z87_3554;
													}
												}
											else
												{	/* Llib/object.scm 1239 */
													return ((bool_t) 1);
												}
										}
									}
								}
							else
								{	/* Llib/object.scm 1234 */
									return ((bool_t) 0);
								}
						}
					}
				}
			}
		}

	}



/* generic-add-method! */
	BGL_EXPORTED_DEF obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t
		BgL_genericz00_128, obj_t BgL_classz00_129, obj_t BgL_methodz00_130,
		obj_t BgL_namez00_131)
	{
		{	/* Llib/object.scm 1245 */
			if (BGL_CLASSP(BgL_classz00_129))
				{	/* Llib/object.scm 1249 */
					bool_t BgL_test3649z00_7764;

					{	/* Llib/object.scm 1249 */
						bool_t BgL_test3650z00_7765;

						{	/* Llib/object.scm 1249 */
							int BgL_arg1768z00_1647;
							int BgL_arg1769z00_1648;

							BgL_arg1768z00_1647 = PROCEDURE_ARITY(BgL_genericz00_128);
							BgL_arg1769z00_1648 = PROCEDURE_ARITY(BgL_methodz00_130);
							BgL_test3650z00_7765 =
								((long) (BgL_arg1768z00_1647) == (long) (BgL_arg1769z00_1648));
						}
						if (BgL_test3650z00_7765)
							{	/* Llib/object.scm 1249 */
								BgL_test3649z00_7764 = ((bool_t) 0);
							}
						else
							{	/* Llib/object.scm 1250 */
								int BgL_arg1767z00_1646;

								BgL_arg1767z00_1646 = PROCEDURE_ARITY(BgL_genericz00_128);
								BgL_test3649z00_7764 = ((long) (BgL_arg1767z00_1646) >= 0L);
					}}
					if (BgL_test3649z00_7764)
						{	/* Llib/object.scm 1252 */
							obj_t BgL_arg1763z00_1641;
							int BgL_arg1764z00_1642;

							{	/* Llib/object.scm 1252 */
								int BgL_arg1765z00_1643;

								BgL_arg1765z00_1643 = PROCEDURE_ARITY(BgL_genericz00_128);
								{	/* Llib/object.scm 1251 */
									obj_t BgL_list1766z00_1644;

									BgL_list1766z00_1644 =
										MAKE_YOUNG_PAIR(BINT(BgL_arg1765z00_1643), BNIL);
									BgL_arg1763z00_1641 =
										BGl_formatz00zz__r4_output_6_10_3z00
										(BGl_string2937z00zz__objectz00, BgL_list1766z00_1644);
							}}
							BgL_arg1764z00_1642 = PROCEDURE_ARITY(BgL_methodz00_130);
							return
								BGl_errorz00zz__errorz00(BgL_namez00_131, BgL_arg1763z00_1641,
								BINT(BgL_arg1764z00_1642));
						}
					else
						{	/* Llib/object.scm 1249 */
							return
								BGl_z52addzd2methodz12z92zz__objectz00(BgL_genericz00_128,
								BgL_classz00_129, BgL_methodz00_130);
						}
				}
			else
				{	/* Llib/object.scm 1247 */
					return
						BGl_errorz00zz__errorz00(BgL_namez00_131,
						BGl_string2938z00zz__objectz00, BgL_classz00_129);
				}
		}

	}



/* &generic-add-method! */
	obj_t BGl_z62genericzd2addzd2methodz12z70zz__objectz00(obj_t BgL_envz00_5160,
		obj_t BgL_genericz00_5161, obj_t BgL_classz00_5162,
		obj_t BgL_methodz00_5163, obj_t BgL_namez00_5164)
	{
		{	/* Llib/object.scm 1245 */
			{	/* Llib/object.scm 1247 */
				obj_t BgL_auxz00_7790;
				obj_t BgL_auxz00_7783;

				if (PROCEDUREP(BgL_methodz00_5163))
					{	/* Llib/object.scm 1247 */
						BgL_auxz00_7790 = BgL_methodz00_5163;
					}
				else
					{
						obj_t BgL_auxz00_7793;

						BgL_auxz00_7793 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(53947L), BGl_string2939z00zz__objectz00,
							BGl_string2907z00zz__objectz00, BgL_methodz00_5163);
						FAILURE(BgL_auxz00_7793, BFALSE, BFALSE);
					}
				if (PROCEDUREP(BgL_genericz00_5161))
					{	/* Llib/object.scm 1247 */
						BgL_auxz00_7783 = BgL_genericz00_5161;
					}
				else
					{
						obj_t BgL_auxz00_7786;

						BgL_auxz00_7786 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(53947L), BGl_string2939z00zz__objectz00,
							BGl_string2907z00zz__objectz00, BgL_genericz00_5161);
						FAILURE(BgL_auxz00_7786, BFALSE, BFALSE);
					}
				return
					BGl_genericzd2addzd2methodz12z12zz__objectz00(BgL_auxz00_7783,
					BgL_classz00_5162, BgL_auxz00_7790, BgL_namez00_5164);
			}
		}

	}



/* generic-add-eval-method! */
	BGL_EXPORTED_DEF obj_t
		BGl_genericzd2addzd2evalzd2methodz12zc0zz__objectz00(obj_t
		BgL_genericz00_132, obj_t BgL_classz00_133, obj_t BgL_methodz00_134,
		obj_t BgL_namez00_135)
	{
		{	/* Llib/object.scm 1265 */
			if (BGL_CLASSP(BgL_classz00_133))
				{	/* Llib/object.scm 1269 */
					bool_t BgL_test3654z00_7800;

					{	/* Llib/object.scm 1269 */
						bool_t BgL_test3655z00_7801;

						{	/* Llib/object.scm 1269 */
							int BgL_arg1791z00_1668;
							int BgL_arg1792z00_1669;

							BgL_arg1791z00_1668 = PROCEDURE_ARITY(BgL_genericz00_132);
							BgL_arg1792z00_1669 = PROCEDURE_ARITY(BgL_methodz00_134);
							BgL_test3655z00_7801 =
								((long) (BgL_arg1791z00_1668) == (long) (BgL_arg1792z00_1669));
						}
						if (BgL_test3655z00_7801)
							{	/* Llib/object.scm 1269 */
								BgL_test3654z00_7800 = ((bool_t) 0);
							}
						else
							{	/* Llib/object.scm 1270 */
								bool_t BgL_test3656z00_7807;

								{	/* Llib/object.scm 1270 */
									int BgL_arg1790z00_1667;

									BgL_arg1790z00_1667 = PROCEDURE_ARITY(BgL_genericz00_132);
									BgL_test3656z00_7807 = ((long) (BgL_arg1790z00_1667) > 4L);
								}
								if (BgL_test3656z00_7807)
									{	/* Llib/object.scm 1271 */
										int BgL_arg1789z00_1666;

										BgL_arg1789z00_1666 = PROCEDURE_ARITY(BgL_methodz00_134);
										BgL_test3654z00_7800 = ((long) (BgL_arg1789z00_1666) >= 0L);
									}
								else
									{	/* Llib/object.scm 1270 */
										BgL_test3654z00_7800 = ((bool_t) 0);
									}
							}
					}
					if (BgL_test3654z00_7800)
						{	/* Llib/object.scm 1273 */
							obj_t BgL_arg1785z00_1660;
							int BgL_arg1786z00_1661;

							{	/* Llib/object.scm 1273 */
								int BgL_arg1787z00_1662;

								BgL_arg1787z00_1662 = PROCEDURE_ARITY(BgL_genericz00_132);
								{	/* Llib/object.scm 1272 */
									obj_t BgL_list1788z00_1663;

									BgL_list1788z00_1663 =
										MAKE_YOUNG_PAIR(BINT(BgL_arg1787z00_1662), BNIL);
									BgL_arg1785z00_1660 =
										BGl_formatz00zz__r4_output_6_10_3z00
										(BGl_string2937z00zz__objectz00, BgL_list1788z00_1663);
							}}
							BgL_arg1786z00_1661 = PROCEDURE_ARITY(BgL_methodz00_134);
							return
								BGl_errorz00zz__errorz00(BgL_namez00_135, BgL_arg1785z00_1660,
								BINT(BgL_arg1786z00_1661));
						}
					else
						{	/* Llib/object.scm 1269 */
							return
								BGl_z52addzd2methodz12z92zz__objectz00(BgL_genericz00_132,
								BgL_classz00_133, BgL_methodz00_134);
						}
				}
			else
				{	/* Llib/object.scm 1267 */
					return
						BGl_errorz00zz__errorz00(BgL_namez00_135,
						BGl_string2938z00zz__objectz00, BgL_classz00_133);
				}
		}

	}



/* &generic-add-eval-method! */
	obj_t BGl_z62genericzd2addzd2evalzd2methodz12za2zz__objectz00(obj_t
		BgL_envz00_5165, obj_t BgL_genericz00_5166, obj_t BgL_classz00_5167,
		obj_t BgL_methodz00_5168, obj_t BgL_namez00_5169)
	{
		{	/* Llib/object.scm 1265 */
			{	/* Llib/object.scm 1267 */
				obj_t BgL_auxz00_7830;
				obj_t BgL_auxz00_7823;

				if (PROCEDUREP(BgL_methodz00_5168))
					{	/* Llib/object.scm 1267 */
						BgL_auxz00_7830 = BgL_methodz00_5168;
					}
				else
					{
						obj_t BgL_auxz00_7833;

						BgL_auxz00_7833 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(55004L), BGl_string2940z00zz__objectz00,
							BGl_string2907z00zz__objectz00, BgL_methodz00_5168);
						FAILURE(BgL_auxz00_7833, BFALSE, BFALSE);
					}
				if (PROCEDUREP(BgL_genericz00_5166))
					{	/* Llib/object.scm 1267 */
						BgL_auxz00_7823 = BgL_genericz00_5166;
					}
				else
					{
						obj_t BgL_auxz00_7826;

						BgL_auxz00_7826 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(55004L), BGl_string2940z00zz__objectz00,
							BGl_string2907z00zz__objectz00, BgL_genericz00_5166);
						FAILURE(BgL_auxz00_7826, BFALSE, BFALSE);
					}
				return
					BGl_genericzd2addzd2evalzd2methodz12zc0zz__objectz00(BgL_auxz00_7823,
					BgL_classz00_5167, BgL_auxz00_7830, BgL_namez00_5169);
			}
		}

	}



/* find-method */
	BGL_EXPORTED_DEF obj_t BGl_findzd2methodzd2zz__objectz00(BgL_objectz00_bglt
		BgL_objz00_136, obj_t BgL_genericz00_137)
	{
		{	/* Llib/object.scm 1283 */
			{	/* Llib/object.scm 1284 */
				long BgL_objzd2classzd2numz00_5983;

				BgL_objzd2classzd2numz00_5983 = BGL_OBJECT_CLASS_NUM(BgL_objz00_136);
				{	/* Llib/object.scm 1285 */
					obj_t BgL_arg1793z00_5984;

					BgL_arg1793z00_5984 = PROCEDURE_REF(BgL_genericz00_137, (int) (1L));
					{	/* Llib/object.scm 1285 */
						int BgL_offsetz00_5985;

						BgL_offsetz00_5985 = (int) (BgL_objzd2classzd2numz00_5983);
						{	/* Llib/object.scm 931 */
							long BgL_offsetz00_5986;

							BgL_offsetz00_5986 = ((long) (BgL_offsetz00_5985) - OBJECT_TYPE);
							{	/* Llib/object.scm 931 */
								long BgL_modz00_5987;

								BgL_modz00_5987 =
									(BgL_offsetz00_5986 >> (int) ((long) ((int) (4L))));
								{	/* Llib/object.scm 932 */
									long BgL_restz00_5988;

									BgL_restz00_5988 =
										(BgL_offsetz00_5986 &
										(long) (
											(int) (
												((long) (
														(int) (
															(1L << (int) ((long) ((int) (4L)))))) - 1L))));
									{	/* Llib/object.scm 933 */

										{	/* Llib/object.scm 934 */
											obj_t BgL_bucketz00_5989;

											BgL_bucketz00_5989 =
												VECTOR_REF(
												((obj_t) BgL_arg1793z00_5984), BgL_modz00_5987);
											return
												VECTOR_REF(
												((obj_t) BgL_bucketz00_5989), BgL_restz00_5988);
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &find-method */
	obj_t BGl_z62findzd2methodzb0zz__objectz00(obj_t BgL_envz00_5170,
		obj_t BgL_objz00_5171, obj_t BgL_genericz00_5172)
	{
		{	/* Llib/object.scm 1283 */
			{	/* Llib/object.scm 1284 */
				obj_t BgL_auxz00_7870;
				BgL_objectz00_bglt BgL_auxz00_7862;

				if (PROCEDUREP(BgL_genericz00_5172))
					{	/* Llib/object.scm 1284 */
						BgL_auxz00_7870 = BgL_genericz00_5172;
					}
				else
					{
						obj_t BgL_auxz00_7873;

						BgL_auxz00_7873 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(55845L), BGl_string2941z00zz__objectz00,
							BGl_string2907z00zz__objectz00, BgL_genericz00_5172);
						FAILURE(BgL_auxz00_7873, BFALSE, BFALSE);
					}
				if (BGl_isazf3zf3zz__objectz00(BgL_objz00_5171,
						BGl_objectz00zz__objectz00))
					{	/* Llib/object.scm 1284 */
						BgL_auxz00_7862 = ((BgL_objectz00_bglt) BgL_objz00_5171);
					}
				else
					{
						obj_t BgL_auxz00_7866;

						BgL_auxz00_7866 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(55845L), BGl_string2941z00zz__objectz00,
							BGl_string2903z00zz__objectz00, BgL_objz00_5171);
						FAILURE(BgL_auxz00_7866, BFALSE, BFALSE);
					}
				return
					BGl_findzd2methodzd2zz__objectz00(BgL_auxz00_7862, BgL_auxz00_7870);
			}
		}

	}



/* find-super-class-method */
	BGL_EXPORTED_DEF obj_t
		BGl_findzd2superzd2classzd2methodzd2zz__objectz00(BgL_objectz00_bglt
		BgL_objz00_138, obj_t BgL_genericz00_139, obj_t BgL_classz00_140)
	{
		{	/* Llib/object.scm 1292 */
			{	/* Llib/object.scm 1293 */
				obj_t BgL_g1219z00_3613;

				{	/* Llib/object.scm 667 */
					obj_t BgL_tmpz00_7878;

					BgL_tmpz00_7878 = ((obj_t) BgL_classz00_140);
					BgL_g1219z00_3613 = BGL_CLASS_SUPER(BgL_tmpz00_7878);
				}
				{
					obj_t BgL_superz00_3615;

					BgL_superz00_3615 = BgL_g1219z00_3613;
				BgL_loopz00_3614:
					if (BGL_CLASSP(BgL_superz00_3615))
						{	/* Llib/object.scm 1296 */
							long BgL_objzd2superzd2classzd2numzd2_3617;

							{	/* Llib/object.scm 522 */
								obj_t BgL_tmpz00_7883;

								BgL_tmpz00_7883 = ((obj_t) BgL_superz00_3615);
								BgL_objzd2superzd2classzd2numzd2_3617 =
									BGL_CLASS_INDEX(BgL_tmpz00_7883);
							}
							{	/* Llib/object.scm 1297 */
								obj_t BgL_methodz00_3618;

								{	/* Llib/object.scm 1298 */
									obj_t BgL_arg1796z00_3619;

									BgL_arg1796z00_3619 =
										PROCEDURE_REF(BgL_genericz00_139, (int) (1L));
									{	/* Llib/object.scm 1297 */
										int BgL_offsetz00_3626;

										BgL_offsetz00_3626 =
											(int) (BgL_objzd2superzd2classzd2numzd2_3617);
										{	/* Llib/object.scm 931 */
											long BgL_offsetz00_3627;

											BgL_offsetz00_3627 =
												((long) (BgL_offsetz00_3626) - OBJECT_TYPE);
											{	/* Llib/object.scm 931 */
												long BgL_modz00_3628;

												BgL_modz00_3628 =
													(BgL_offsetz00_3627 >> (int) ((long) ((int) (4L))));
												{	/* Llib/object.scm 932 */
													long BgL_restz00_3630;

													BgL_restz00_3630 =
														(BgL_offsetz00_3627 &
														(long) (
															(int) (
																((long) (
																		(int) (
																			(1L <<
																				(int) ((long) ((int) (4L)))))) - 1L))));
													{	/* Llib/object.scm 933 */

														{	/* Llib/object.scm 934 */
															obj_t BgL_bucketz00_3632;

															BgL_bucketz00_3632 =
																VECTOR_REF(
																((obj_t) BgL_arg1796z00_3619), BgL_modz00_3628);
															BgL_methodz00_3618 =
																VECTOR_REF(
																((obj_t) BgL_bucketz00_3632), BgL_restz00_3630);
								}}}}}}}
								if (CBOOL(BgL_methodz00_3618))
									{	/* Llib/object.scm 1300 */
										return BgL_methodz00_3618;
									}
								else
									{	/* Llib/object.scm 1302 */
										obj_t BgL_newzd2superzd2_3620;

										{	/* Llib/object.scm 667 */
											obj_t BgL_tmpz00_7911;

											BgL_tmpz00_7911 = ((obj_t) BgL_superz00_3615);
											BgL_newzd2superzd2_3620 =
												BGL_CLASS_SUPER(BgL_tmpz00_7911);
										}
										{
											obj_t BgL_superz00_7914;

											BgL_superz00_7914 = BgL_newzd2superzd2_3620;
											BgL_superz00_3615 = BgL_superz00_7914;
											goto BgL_loopz00_3614;
										}
									}
							}
						}
					else
						{	/* Llib/object.scm 1294 */
							return PROCEDURE_REF(BgL_genericz00_139, (int) (0L));
		}}}}

	}



/* &find-super-class-method */
	obj_t BGl_z62findzd2superzd2classzd2methodzb0zz__objectz00(obj_t
		BgL_envz00_5173, obj_t BgL_objz00_5174, obj_t BgL_genericz00_5175,
		obj_t BgL_classz00_5176)
	{
		{	/* Llib/object.scm 1292 */
			{	/* Llib/object.scm 1293 */
				obj_t BgL_auxz00_7925;
				BgL_objectz00_bglt BgL_auxz00_7917;

				if (PROCEDUREP(BgL_genericz00_5175))
					{	/* Llib/object.scm 1293 */
						BgL_auxz00_7925 = BgL_genericz00_5175;
					}
				else
					{
						obj_t BgL_auxz00_7928;

						BgL_auxz00_7928 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(56397L), BGl_string2942z00zz__objectz00,
							BGl_string2907z00zz__objectz00, BgL_genericz00_5175);
						FAILURE(BgL_auxz00_7928, BFALSE, BFALSE);
					}
				if (BGl_isazf3zf3zz__objectz00(BgL_objz00_5174,
						BGl_objectz00zz__objectz00))
					{	/* Llib/object.scm 1293 */
						BgL_auxz00_7917 = ((BgL_objectz00_bglt) BgL_objz00_5174);
					}
				else
					{
						obj_t BgL_auxz00_7921;

						BgL_auxz00_7921 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(56397L), BGl_string2942z00zz__objectz00,
							BGl_string2903z00zz__objectz00, BgL_objz00_5174);
						FAILURE(BgL_auxz00_7921, BFALSE, BFALSE);
					}
				return
					BGl_findzd2superzd2classzd2methodzd2zz__objectz00(BgL_auxz00_7917,
					BgL_auxz00_7925, BgL_classz00_5176);
			}
		}

	}



/* find-method-from */
	BGL_EXPORTED_DEF obj_t
		BGl_findzd2methodzd2fromz00zz__objectz00(BgL_objectz00_bglt BgL_objz00_141,
		obj_t BgL_genericz00_142, obj_t BgL_classz00_143)
	{
		{	/* Llib/object.scm 1308 */
			{
				obj_t BgL_classz00_3655;

				BgL_classz00_3655 = BgL_classz00_143;
			BgL_loopz00_3654:
				if (BGL_CLASSP(BgL_classz00_3655))
					{	/* Llib/object.scm 1312 */
						long BgL_objzd2superzd2classzd2numzd2_3657;

						{	/* Llib/object.scm 522 */
							obj_t BgL_tmpz00_7935;

							BgL_tmpz00_7935 = ((obj_t) BgL_classz00_3655);
							BgL_objzd2superzd2classzd2numzd2_3657 =
								BGL_CLASS_INDEX(BgL_tmpz00_7935);
						}
						{	/* Llib/object.scm 1313 */
							obj_t BgL_methodz00_3658;

							{	/* Llib/object.scm 1314 */
								obj_t BgL_arg1800z00_3659;

								BgL_arg1800z00_3659 =
									PROCEDURE_REF(BgL_genericz00_142, (int) (1L));
								{	/* Llib/object.scm 1313 */
									int BgL_offsetz00_3665;

									BgL_offsetz00_3665 =
										(int) (BgL_objzd2superzd2classzd2numzd2_3657);
									{	/* Llib/object.scm 931 */
										long BgL_offsetz00_3666;

										BgL_offsetz00_3666 =
											((long) (BgL_offsetz00_3665) - OBJECT_TYPE);
										{	/* Llib/object.scm 931 */
											long BgL_modz00_3667;

											BgL_modz00_3667 =
												(BgL_offsetz00_3666 >> (int) ((long) ((int) (4L))));
											{	/* Llib/object.scm 932 */
												long BgL_restz00_3669;

												BgL_restz00_3669 =
													(BgL_offsetz00_3666 &
													(long) (
														(int) (
															((long) (
																	(int) (
																		(1L <<
																			(int) ((long) ((int) (4L)))))) - 1L))));
												{	/* Llib/object.scm 933 */

													{	/* Llib/object.scm 934 */
														obj_t BgL_bucketz00_3671;

														BgL_bucketz00_3671 =
															VECTOR_REF(
															((obj_t) BgL_arg1800z00_3659), BgL_modz00_3667);
														BgL_methodz00_3658 =
															VECTOR_REF(
															((obj_t) BgL_bucketz00_3671), BgL_restz00_3669);
							}}}}}}}
							if (CBOOL(BgL_methodz00_3658))
								{	/* Llib/object.scm 1316 */
									return MAKE_YOUNG_PAIR(BgL_classz00_3655, BgL_methodz00_3658);
								}
							else
								{	/* Llib/object.scm 1318 */
									obj_t BgL_arg1799z00_3660;

									{	/* Llib/object.scm 667 */
										obj_t BgL_tmpz00_7964;

										BgL_tmpz00_7964 = ((obj_t) BgL_classz00_3655);
										BgL_arg1799z00_3660 = BGL_CLASS_SUPER(BgL_tmpz00_7964);
									}
									{
										obj_t BgL_classz00_7967;

										BgL_classz00_7967 = BgL_arg1799z00_3660;
										BgL_classz00_3655 = BgL_classz00_7967;
										goto BgL_loopz00_3654;
									}
								}
						}
					}
				else
					{	/* Llib/object.scm 1310 */
						return MAKE_YOUNG_PAIR(BFALSE, BFALSE);
					}
			}
		}

	}



/* &find-method-from */
	obj_t BGl_z62findzd2methodzd2fromz62zz__objectz00(obj_t BgL_envz00_5177,
		obj_t BgL_objz00_5178, obj_t BgL_genericz00_5179, obj_t BgL_classz00_5180)
	{
		{	/* Llib/object.scm 1308 */
			{	/* Llib/object.scm 1310 */
				obj_t BgL_auxz00_7977;
				BgL_objectz00_bglt BgL_auxz00_7969;

				if (PROCEDUREP(BgL_genericz00_5179))
					{	/* Llib/object.scm 1310 */
						BgL_auxz00_7977 = BgL_genericz00_5179;
					}
				else
					{
						obj_t BgL_auxz00_7980;

						BgL_auxz00_7980 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(57074L), BGl_string2943z00zz__objectz00,
							BGl_string2907z00zz__objectz00, BgL_genericz00_5179);
						FAILURE(BgL_auxz00_7980, BFALSE, BFALSE);
					}
				if (BGl_isazf3zf3zz__objectz00(BgL_objz00_5178,
						BGl_objectz00zz__objectz00))
					{	/* Llib/object.scm 1310 */
						BgL_auxz00_7969 = ((BgL_objectz00_bglt) BgL_objz00_5178);
					}
				else
					{
						obj_t BgL_auxz00_7973;

						BgL_auxz00_7973 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(57074L), BGl_string2943z00zz__objectz00,
							BGl_string2903z00zz__objectz00, BgL_objz00_5178);
						FAILURE(BgL_auxz00_7973, BFALSE, BFALSE);
					}
				return
					BGl_findzd2methodzd2fromz00zz__objectz00(BgL_auxz00_7969,
					BgL_auxz00_7977, BgL_classz00_5180);
			}
		}

	}



/* nil? */
	BGL_EXPORTED_DEF bool_t BGl_nilzf3zf3zz__objectz00(BgL_objectz00_bglt
		BgL_objz00_144)
	{
		{	/* Llib/object.scm 1323 */
			{	/* Llib/object.scm 1324 */
				obj_t BgL_klassz00_3691;

				{	/* Llib/object.scm 893 */
					obj_t BgL_arg1553z00_3694;
					long BgL_arg1554z00_3695;

					BgL_arg1553z00_3694 = (BGl_za2classesza2z00zz__objectz00);
					{	/* Llib/object.scm 894 */
						long BgL_arg1555z00_3696;

						BgL_arg1555z00_3696 = BGL_OBJECT_CLASS_NUM(BgL_objz00_144);
						BgL_arg1554z00_3695 = (BgL_arg1555z00_3696 - OBJECT_TYPE);
					}
					BgL_klassz00_3691 =
						VECTOR_REF(BgL_arg1553z00_3694, BgL_arg1554z00_3695);
				}
				{	/* Llib/object.scm 1325 */
					obj_t BgL_arg1801z00_3692;

					{	/* Llib/object.scm 755 */
						obj_t BgL__ortest_1218z00_3703;

						{	/* Llib/object.scm 755 */
							obj_t BgL_tmpz00_7989;

							BgL_tmpz00_7989 = ((obj_t) BgL_klassz00_3691);
							BgL__ortest_1218z00_3703 = BGL_CLASS_NIL(BgL_tmpz00_7989);
						}
						if (CBOOL(BgL__ortest_1218z00_3703))
							{	/* Llib/object.scm 755 */
								BgL_arg1801z00_3692 = BgL__ortest_1218z00_3703;
							}
						else
							{	/* Llib/object.scm 755 */
								BgL_arg1801z00_3692 =
									BGl_classzd2nilzd2initz12z12zz__objectz00(
									((obj_t) BgL_klassz00_3691));
							}
					}
					return (BgL_arg1801z00_3692 == ((obj_t) BgL_objz00_144));
				}
			}
		}

	}



/* &nil? */
	obj_t BGl_z62nilzf3z91zz__objectz00(obj_t BgL_envz00_5181,
		obj_t BgL_objz00_5182)
	{
		{	/* Llib/object.scm 1323 */
			{	/* Llib/object.scm 1324 */
				bool_t BgL_tmpz00_7998;

				{	/* Llib/object.scm 1324 */
					BgL_objectz00_bglt BgL_auxz00_7999;

					if (BGl_isazf3zf3zz__objectz00(BgL_objz00_5182,
							BGl_objectz00zz__objectz00))
						{	/* Llib/object.scm 1324 */
							BgL_auxz00_7999 = ((BgL_objectz00_bglt) BgL_objz00_5182);
						}
					else
						{
							obj_t BgL_auxz00_8003;

							BgL_auxz00_8003 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
								BINT(57619L), BGl_string2944z00zz__objectz00,
								BGl_string2903z00zz__objectz00, BgL_objz00_5182);
							FAILURE(BgL_auxz00_8003, BFALSE, BFALSE);
						}
					BgL_tmpz00_7998 = BGl_nilzf3zf3zz__objectz00(BgL_auxz00_7999);
				}
				return BBOOL(BgL_tmpz00_7998);
			}
		}

	}



/* isa? */
	BGL_EXPORTED_DEF bool_t BGl_isazf3zf3zz__objectz00(obj_t BgL_objz00_145,
		obj_t BgL_classz00_146)
	{
		{	/* Llib/object.scm 1332 */
			if (BGL_OBJECTP(BgL_objz00_145))
				{	/* Llib/object.scm 1334 */
					BgL_objectz00_bglt BgL_arg1803z00_5990;
					long BgL_arg1804z00_5991;

					BgL_arg1803z00_5990 = (BgL_objectz00_bglt) (BgL_objz00_145);
					BgL_arg1804z00_5991 = BGL_CLASS_DEPTH(BgL_classz00_146);
					if (BGL_CONDEXPAND_ISA_ARCH64())
						{	/* Llib/object.scm 1375 */
							long BgL_idxz00_5992;

							BgL_idxz00_5992 = BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_5990);
							return
								(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
									(BgL_idxz00_5992 + BgL_arg1804z00_5991)) == BgL_classz00_146);
						}
					else
						{	/* Llib/object.scm 1354 */
							bool_t BgL_res2597z00_5995;

							{	/* Llib/object.scm 1362 */
								obj_t BgL_oclassz00_5996;

								{	/* Llib/object.scm 893 */
									obj_t BgL_arg1553z00_5997;
									long BgL_arg1554z00_5998;

									BgL_arg1553z00_5997 = (BGl_za2classesza2z00zz__objectz00);
									{	/* Llib/object.scm 894 */
										long BgL_arg1555z00_5999;

										BgL_arg1555z00_5999 =
											BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_5990);
										BgL_arg1554z00_5998 = (BgL_arg1555z00_5999 - OBJECT_TYPE);
									}
									BgL_oclassz00_5996 =
										VECTOR_REF(BgL_arg1553z00_5997, BgL_arg1554z00_5998);
								}
								{	/* Llib/object.scm 1363 */
									bool_t BgL__ortest_1220z00_6000;

									BgL__ortest_1220z00_6000 =
										(BgL_classz00_146 == BgL_oclassz00_5996);
									if (BgL__ortest_1220z00_6000)
										{	/* Llib/object.scm 1363 */
											BgL_res2597z00_5995 = BgL__ortest_1220z00_6000;
										}
									else
										{	/* Llib/object.scm 1364 */
											long BgL_odepthz00_6001;

											{	/* Llib/object.scm 1364 */
												obj_t BgL_arg1810z00_6002;

												BgL_arg1810z00_6002 = (BgL_oclassz00_5996);
												BgL_odepthz00_6001 =
													BGL_CLASS_DEPTH(BgL_arg1810z00_6002);
											}
											if ((BgL_arg1804z00_5991 < BgL_odepthz00_6001))
												{	/* Llib/object.scm 1366 */
													obj_t BgL_arg1808z00_6003;

													{	/* Llib/object.scm 1366 */
														obj_t BgL_arg1809z00_6004;

														BgL_arg1809z00_6004 = (BgL_oclassz00_5996);
														BgL_arg1808z00_6003 =
															BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_6004,
															BgL_arg1804z00_5991);
													}
													BgL_res2597z00_5995 =
														(BgL_arg1808z00_6003 == BgL_classz00_146);
												}
											else
												{	/* Llib/object.scm 1365 */
													BgL_res2597z00_5995 = ((bool_t) 0);
												}
										}
								}
							}
							return BgL_res2597z00_5995;
						}
				}
			else
				{	/* Llib/object.scm 1333 */
					return ((bool_t) 0);
				}
		}

	}



/* &isa? */
	obj_t BGl_z62isazf3z91zz__objectz00(obj_t BgL_envz00_5183,
		obj_t BgL_objz00_5184, obj_t BgL_classz00_5185)
	{
		{	/* Llib/object.scm 1332 */
			{	/* Llib/object.scm 1333 */
				bool_t BgL_tmpz00_8032;

				{	/* Llib/object.scm 1333 */
					obj_t BgL_auxz00_8033;

					if (BGl_classzf3zf3zz__objectz00(BgL_classz00_5185))
						{	/* Llib/object.scm 1333 */
							BgL_auxz00_8033 = BgL_classz00_5185;
						}
					else
						{
							obj_t BgL_auxz00_8036;

							BgL_auxz00_8036 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
								BINT(58095L), BGl_string2945z00zz__objectz00,
								BGl_string2862z00zz__objectz00, BgL_classz00_5185);
							FAILURE(BgL_auxz00_8036, BFALSE, BFALSE);
						}
					BgL_tmpz00_8032 =
						BGl_isazf3zf3zz__objectz00(BgL_objz00_5184, BgL_auxz00_8033);
				}
				return BBOOL(BgL_tmpz00_8032);
			}
		}

	}



/* %isa/cdepth? */
	BGL_EXPORTED_DEF bool_t BGl_z52isazf2cdepthzf3z53zz__objectz00(obj_t
		BgL_objz00_147, obj_t BgL_classz00_148, long BgL_cdepthz00_149)
	{
		{	/* Llib/object.scm 1339 */
			if (BGL_OBJECTP(BgL_objz00_147))
				{	/* Llib/object.scm 1341 */
					BgL_objectz00_bglt BgL_arg1806z00_6005;

					BgL_arg1806z00_6005 = (BgL_objectz00_bglt) (BgL_objz00_147);
					if (BGL_CONDEXPAND_ISA_ARCH64())
						{	/* Llib/object.scm 1375 */
							long BgL_idxz00_6006;

							BgL_idxz00_6006 = BGL_OBJECT_INHERITANCE_NUM(BgL_arg1806z00_6005);
							return
								(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
									(BgL_idxz00_6006 + BgL_cdepthz00_149)) == BgL_classz00_148);
						}
					else
						{	/* Llib/object.scm 1354 */
							bool_t BgL_res2598z00_6009;

							{	/* Llib/object.scm 1362 */
								obj_t BgL_oclassz00_6010;

								{	/* Llib/object.scm 893 */
									obj_t BgL_arg1553z00_6011;
									long BgL_arg1554z00_6012;

									BgL_arg1553z00_6011 = (BGl_za2classesza2z00zz__objectz00);
									{	/* Llib/object.scm 894 */
										long BgL_arg1555z00_6013;

										BgL_arg1555z00_6013 =
											BGL_OBJECT_CLASS_NUM(BgL_arg1806z00_6005);
										BgL_arg1554z00_6012 = (BgL_arg1555z00_6013 - OBJECT_TYPE);
									}
									BgL_oclassz00_6010 =
										VECTOR_REF(BgL_arg1553z00_6011, BgL_arg1554z00_6012);
								}
								{	/* Llib/object.scm 1363 */
									bool_t BgL__ortest_1220z00_6014;

									BgL__ortest_1220z00_6014 =
										(BgL_classz00_148 == BgL_oclassz00_6010);
									if (BgL__ortest_1220z00_6014)
										{	/* Llib/object.scm 1363 */
											BgL_res2598z00_6009 = BgL__ortest_1220z00_6014;
										}
									else
										{	/* Llib/object.scm 1364 */
											long BgL_odepthz00_6015;

											{	/* Llib/object.scm 1364 */
												obj_t BgL_arg1810z00_6016;

												BgL_arg1810z00_6016 = (BgL_oclassz00_6010);
												BgL_odepthz00_6015 =
													BGL_CLASS_DEPTH(BgL_arg1810z00_6016);
											}
											if ((BgL_cdepthz00_149 < BgL_odepthz00_6015))
												{	/* Llib/object.scm 1366 */
													obj_t BgL_arg1808z00_6017;

													{	/* Llib/object.scm 1366 */
														obj_t BgL_arg1809z00_6018;

														BgL_arg1809z00_6018 = (BgL_oclassz00_6010);
														BgL_arg1808z00_6017 =
															BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_6018,
															BgL_cdepthz00_149);
													}
													BgL_res2598z00_6009 =
														(BgL_arg1808z00_6017 == BgL_classz00_148);
												}
											else
												{	/* Llib/object.scm 1365 */
													BgL_res2598z00_6009 = ((bool_t) 0);
												}
										}
								}
							}
							return BgL_res2598z00_6009;
						}
				}
			else
				{	/* Llib/object.scm 1340 */
					return ((bool_t) 0);
				}
		}

	}



/* &%isa/cdepth? */
	obj_t BGl_z62z52isazf2cdepthzf3z31zz__objectz00(obj_t BgL_envz00_5186,
		obj_t BgL_objz00_5187, obj_t BgL_classz00_5188, obj_t BgL_cdepthz00_5189)
	{
		{	/* Llib/object.scm 1339 */
			{	/* Llib/object.scm 1340 */
				bool_t BgL_tmpz00_8064;

				{	/* Llib/object.scm 1340 */
					long BgL_auxz00_8072;
					obj_t BgL_auxz00_8065;

					{	/* Llib/object.scm 1340 */
						obj_t BgL_tmpz00_8073;

						if (INTEGERP(BgL_cdepthz00_5189))
							{	/* Llib/object.scm 1340 */
								BgL_tmpz00_8073 = BgL_cdepthz00_5189;
							}
						else
							{
								obj_t BgL_auxz00_8076;

								BgL_auxz00_8076 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string2853z00zz__objectz00, BINT(58462L),
									BGl_string2946z00zz__objectz00,
									BGl_string2860z00zz__objectz00, BgL_cdepthz00_5189);
								FAILURE(BgL_auxz00_8076, BFALSE, BFALSE);
							}
						BgL_auxz00_8072 = (long) CINT(BgL_tmpz00_8073);
					}
					if (BGl_classzf3zf3zz__objectz00(BgL_classz00_5188))
						{	/* Llib/object.scm 1340 */
							BgL_auxz00_8065 = BgL_classz00_5188;
						}
					else
						{
							obj_t BgL_auxz00_8068;

							BgL_auxz00_8068 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
								BINT(58462L), BGl_string2946z00zz__objectz00,
								BGl_string2862z00zz__objectz00, BgL_classz00_5188);
							FAILURE(BgL_auxz00_8068, BFALSE, BFALSE);
						}
					BgL_tmpz00_8064 =
						BGl_z52isazf2cdepthzf3z53zz__objectz00(BgL_objz00_5187,
						BgL_auxz00_8065, BgL_auxz00_8072);
				}
				return BBOOL(BgL_tmpz00_8064);
			}
		}

	}



/* %isa-object/cdepth? */
	BGL_EXPORTED_DEF bool_t
		BGl_z52isazd2objectzf2cdepthzf3z81zz__objectz00(BgL_objectz00_bglt
		BgL_objz00_150, obj_t BgL_classz00_151, long BgL_cdepthz00_152)
	{
		{	/* Llib/object.scm 1346 */
			if (BGL_CONDEXPAND_ISA_ARCH64())
				{	/* Llib/object.scm 1375 */
					long BgL_idxz00_6019;

					BgL_idxz00_6019 = BGL_OBJECT_INHERITANCE_NUM(BgL_objz00_150);
					return
						(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
							(BgL_idxz00_6019 + BgL_cdepthz00_152)) == BgL_classz00_151);
				}
			else
				{	/* Llib/object.scm 1354 */
					bool_t BgL_res2599z00_6022;

					{	/* Llib/object.scm 1362 */
						obj_t BgL_oclassz00_6023;

						{	/* Llib/object.scm 893 */
							obj_t BgL_arg1553z00_6024;
							long BgL_arg1554z00_6025;

							BgL_arg1553z00_6024 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Llib/object.scm 894 */
								long BgL_arg1555z00_6026;

								BgL_arg1555z00_6026 = BGL_OBJECT_CLASS_NUM(BgL_objz00_150);
								BgL_arg1554z00_6025 = (BgL_arg1555z00_6026 - OBJECT_TYPE);
							}
							BgL_oclassz00_6023 =
								VECTOR_REF(BgL_arg1553z00_6024, BgL_arg1554z00_6025);
						}
						{	/* Llib/object.scm 1363 */
							bool_t BgL__ortest_1220z00_6027;

							BgL__ortest_1220z00_6027 =
								(BgL_classz00_151 == BgL_oclassz00_6023);
							if (BgL__ortest_1220z00_6027)
								{	/* Llib/object.scm 1363 */
									BgL_res2599z00_6022 = BgL__ortest_1220z00_6027;
								}
							else
								{	/* Llib/object.scm 1364 */
									long BgL_odepthz00_6028;

									{	/* Llib/object.scm 1364 */
										obj_t BgL_arg1810z00_6029;

										BgL_arg1810z00_6029 = (BgL_oclassz00_6023);
										BgL_odepthz00_6028 = BGL_CLASS_DEPTH(BgL_arg1810z00_6029);
									}
									if ((BgL_cdepthz00_152 < BgL_odepthz00_6028))
										{	/* Llib/object.scm 1366 */
											obj_t BgL_arg1808z00_6030;

											{	/* Llib/object.scm 1366 */
												obj_t BgL_arg1809z00_6031;

												BgL_arg1809z00_6031 = (BgL_oclassz00_6023);
												BgL_arg1808z00_6030 =
													BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_6031,
													BgL_cdepthz00_152);
											}
											BgL_res2599z00_6022 =
												(BgL_arg1808z00_6030 == BgL_classz00_151);
										}
									else
										{	/* Llib/object.scm 1365 */
											BgL_res2599z00_6022 = ((bool_t) 0);
										}
								}
						}
					}
					return BgL_res2599z00_6022;
				}
		}

	}



/* &%isa-object/cdepth? */
	obj_t BGl_z62z52isazd2objectzf2cdepthzf3ze3zz__objectz00(obj_t
		BgL_envz00_5190, obj_t BgL_objz00_5191, obj_t BgL_classz00_5192,
		obj_t BgL_cdepthz00_5193)
	{
		{	/* Llib/object.scm 1346 */
			{	/* Llib/object.scm 1352 */
				bool_t BgL_tmpz00_8102;

				{	/* Llib/object.scm 1352 */
					long BgL_auxz00_8118;
					obj_t BgL_auxz00_8111;
					BgL_objectz00_bglt BgL_auxz00_8103;

					{	/* Llib/object.scm 1352 */
						obj_t BgL_tmpz00_8119;

						if (INTEGERP(BgL_cdepthz00_5193))
							{	/* Llib/object.scm 1352 */
								BgL_tmpz00_8119 = BgL_cdepthz00_5193;
							}
						else
							{
								obj_t BgL_auxz00_8122;

								BgL_auxz00_8122 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string2853z00zz__objectz00, BINT(59046L),
									BGl_string2947z00zz__objectz00,
									BGl_string2860z00zz__objectz00, BgL_cdepthz00_5193);
								FAILURE(BgL_auxz00_8122, BFALSE, BFALSE);
							}
						BgL_auxz00_8118 = (long) CINT(BgL_tmpz00_8119);
					}
					if (BGl_classzf3zf3zz__objectz00(BgL_classz00_5192))
						{	/* Llib/object.scm 1352 */
							BgL_auxz00_8111 = BgL_classz00_5192;
						}
					else
						{
							obj_t BgL_auxz00_8114;

							BgL_auxz00_8114 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
								BINT(59046L), BGl_string2947z00zz__objectz00,
								BGl_string2862z00zz__objectz00, BgL_classz00_5192);
							FAILURE(BgL_auxz00_8114, BFALSE, BFALSE);
						}
					if (BGl_isazf3zf3zz__objectz00(BgL_objz00_5191,
							BGl_objectz00zz__objectz00))
						{	/* Llib/object.scm 1352 */
							BgL_auxz00_8103 = ((BgL_objectz00_bglt) BgL_objz00_5191);
						}
					else
						{
							obj_t BgL_auxz00_8107;

							BgL_auxz00_8107 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
								BINT(59046L), BGl_string2947z00zz__objectz00,
								BGl_string2903z00zz__objectz00, BgL_objz00_5191);
							FAILURE(BgL_auxz00_8107, BFALSE, BFALSE);
						}
					BgL_tmpz00_8102 =
						BGl_z52isazd2objectzf2cdepthzf3z81zz__objectz00(BgL_auxz00_8103,
						BgL_auxz00_8111, BgL_auxz00_8118);
				}
				return BBOOL(BgL_tmpz00_8102);
			}
		}

	}



/* %isa32-object/cdepth? */
	BGL_EXPORTED_DEF bool_t
		BGl_z52isa32zd2objectzf2cdepthzf3z81zz__objectz00(BgL_objectz00_bglt
		BgL_objz00_153, obj_t BgL_classz00_154, long BgL_cdepthz00_155)
	{
		{	/* Llib/object.scm 1361 */
			{	/* Llib/object.scm 1362 */
				obj_t BgL_oclassz00_6032;

				{	/* Llib/object.scm 893 */
					obj_t BgL_arg1553z00_6033;
					long BgL_arg1554z00_6034;

					BgL_arg1553z00_6033 = (BGl_za2classesza2z00zz__objectz00);
					{	/* Llib/object.scm 894 */
						long BgL_arg1555z00_6035;

						BgL_arg1555z00_6035 = BGL_OBJECT_CLASS_NUM(BgL_objz00_153);
						BgL_arg1554z00_6034 = (BgL_arg1555z00_6035 - OBJECT_TYPE);
					}
					BgL_oclassz00_6032 =
						VECTOR_REF(BgL_arg1553z00_6033, BgL_arg1554z00_6034);
				}
				{	/* Llib/object.scm 1363 */
					bool_t BgL__ortest_1220z00_6036;

					BgL__ortest_1220z00_6036 = (BgL_classz00_154 == BgL_oclassz00_6032);
					if (BgL__ortest_1220z00_6036)
						{	/* Llib/object.scm 1363 */
							return BgL__ortest_1220z00_6036;
						}
					else
						{	/* Llib/object.scm 1364 */
							long BgL_odepthz00_6037;

							{	/* Llib/object.scm 1364 */
								obj_t BgL_arg1810z00_6038;

								BgL_arg1810z00_6038 = (BgL_oclassz00_6032);
								BgL_odepthz00_6037 = BGL_CLASS_DEPTH(BgL_arg1810z00_6038);
							}
							if ((BgL_cdepthz00_155 < BgL_odepthz00_6037))
								{	/* Llib/object.scm 1366 */
									obj_t BgL_arg1808z00_6039;

									{	/* Llib/object.scm 1366 */
										obj_t BgL_arg1809z00_6040;

										BgL_arg1809z00_6040 = (BgL_oclassz00_6032);
										BgL_arg1808z00_6039 =
											BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_6040,
											BgL_cdepthz00_155);
									}
									return (BgL_arg1808z00_6039 == BgL_classz00_154);
								}
							else
								{	/* Llib/object.scm 1365 */
									return ((bool_t) 0);
								}
						}
				}
			}
		}

	}



/* &%isa32-object/cdepth? */
	obj_t BGl_z62z52isa32zd2objectzf2cdepthzf3ze3zz__objectz00(obj_t
		BgL_envz00_5194, obj_t BgL_objz00_5195, obj_t BgL_classz00_5196,
		obj_t BgL_cdepthz00_5197)
	{
		{	/* Llib/object.scm 1361 */
			{	/* Llib/object.scm 1362 */
				bool_t BgL_tmpz00_8142;

				{	/* Llib/object.scm 1362 */
					long BgL_auxz00_8158;
					obj_t BgL_auxz00_8151;
					BgL_objectz00_bglt BgL_auxz00_8143;

					{	/* Llib/object.scm 1362 */
						obj_t BgL_tmpz00_8159;

						if (INTEGERP(BgL_cdepthz00_5197))
							{	/* Llib/object.scm 1362 */
								BgL_tmpz00_8159 = BgL_cdepthz00_5197;
							}
						else
							{
								obj_t BgL_auxz00_8162;

								BgL_auxz00_8162 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string2853z00zz__objectz00, BINT(59514L),
									BGl_string2948z00zz__objectz00,
									BGl_string2860z00zz__objectz00, BgL_cdepthz00_5197);
								FAILURE(BgL_auxz00_8162, BFALSE, BFALSE);
							}
						BgL_auxz00_8158 = (long) CINT(BgL_tmpz00_8159);
					}
					if (BGl_classzf3zf3zz__objectz00(BgL_classz00_5196))
						{	/* Llib/object.scm 1362 */
							BgL_auxz00_8151 = BgL_classz00_5196;
						}
					else
						{
							obj_t BgL_auxz00_8154;

							BgL_auxz00_8154 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
								BINT(59514L), BGl_string2948z00zz__objectz00,
								BGl_string2862z00zz__objectz00, BgL_classz00_5196);
							FAILURE(BgL_auxz00_8154, BFALSE, BFALSE);
						}
					if (BGl_isazf3zf3zz__objectz00(BgL_objz00_5195,
							BGl_objectz00zz__objectz00))
						{	/* Llib/object.scm 1362 */
							BgL_auxz00_8143 = ((BgL_objectz00_bglt) BgL_objz00_5195);
						}
					else
						{
							obj_t BgL_auxz00_8147;

							BgL_auxz00_8147 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
								BINT(59514L), BGl_string2948z00zz__objectz00,
								BGl_string2903z00zz__objectz00, BgL_objz00_5195);
							FAILURE(BgL_auxz00_8147, BFALSE, BFALSE);
						}
					BgL_tmpz00_8142 =
						BGl_z52isa32zd2objectzf2cdepthzf3z81zz__objectz00(BgL_auxz00_8143,
						BgL_auxz00_8151, BgL_auxz00_8158);
				}
				return BBOOL(BgL_tmpz00_8142);
			}
		}

	}



/* %isa64-object/cdepth? */
	BGL_EXPORTED_DEF bool_t
		BGl_z52isa64zd2objectzf2cdepthzf3z81zz__objectz00(BgL_objectz00_bglt
		BgL_objz00_156, obj_t BgL_classz00_157, long BgL_cdepthz00_158)
	{
		{	/* Llib/object.scm 1372 */
			{	/* Llib/object.scm 1375 */
				long BgL_idxz00_6041;

				BgL_idxz00_6041 = BGL_OBJECT_INHERITANCE_NUM(BgL_objz00_156);
				return
					(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
						(BgL_idxz00_6041 + BgL_cdepthz00_158)) == BgL_classz00_157);
			}
		}

	}



/* &%isa64-object/cdepth? */
	obj_t BGl_z62z52isa64zd2objectzf2cdepthzf3ze3zz__objectz00(obj_t
		BgL_envz00_5198, obj_t BgL_objz00_5199, obj_t BgL_classz00_5200,
		obj_t BgL_cdepthz00_5201)
	{
		{	/* Llib/object.scm 1372 */
			{	/* Llib/object.scm 1375 */
				bool_t BgL_tmpz00_8173;

				{	/* Llib/object.scm 1375 */
					long BgL_auxz00_8189;
					obj_t BgL_auxz00_8182;
					BgL_objectz00_bglt BgL_auxz00_8174;

					{	/* Llib/object.scm 1375 */
						obj_t BgL_tmpz00_8190;

						if (INTEGERP(BgL_cdepthz00_5201))
							{	/* Llib/object.scm 1375 */
								BgL_tmpz00_8190 = BgL_cdepthz00_5201;
							}
						else
							{
								obj_t BgL_auxz00_8193;

								BgL_auxz00_8193 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string2853z00zz__objectz00, BINT(60057L),
									BGl_string2949z00zz__objectz00,
									BGl_string2860z00zz__objectz00, BgL_cdepthz00_5201);
								FAILURE(BgL_auxz00_8193, BFALSE, BFALSE);
							}
						BgL_auxz00_8189 = (long) CINT(BgL_tmpz00_8190);
					}
					if (BGl_classzf3zf3zz__objectz00(BgL_classz00_5200))
						{	/* Llib/object.scm 1375 */
							BgL_auxz00_8182 = BgL_classz00_5200;
						}
					else
						{
							obj_t BgL_auxz00_8185;

							BgL_auxz00_8185 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
								BINT(60057L), BGl_string2949z00zz__objectz00,
								BGl_string2862z00zz__objectz00, BgL_classz00_5200);
							FAILURE(BgL_auxz00_8185, BFALSE, BFALSE);
						}
					if (BGl_isazf3zf3zz__objectz00(BgL_objz00_5199,
							BGl_objectz00zz__objectz00))
						{	/* Llib/object.scm 1375 */
							BgL_auxz00_8174 = ((BgL_objectz00_bglt) BgL_objz00_5199);
						}
					else
						{
							obj_t BgL_auxz00_8178;

							BgL_auxz00_8178 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
								BINT(60057L), BGl_string2949z00zz__objectz00,
								BGl_string2903z00zz__objectz00, BgL_objz00_5199);
							FAILURE(BgL_auxz00_8178, BFALSE, BFALSE);
						}
					BgL_tmpz00_8173 =
						BGl_z52isa64zd2objectzf2cdepthzf3z81zz__objectz00(BgL_auxz00_8174,
						BgL_auxz00_8182, BgL_auxz00_8189);
				}
				return BBOOL(BgL_tmpz00_8173);
			}
		}

	}



/* %isa/final? */
	BGL_EXPORTED_DEF bool_t BGl_z52isazf2finalzf3z53zz__objectz00(obj_t
		BgL_objz00_159, obj_t BgL_classz00_160)
	{
		{	/* Llib/object.scm 1383 */
			if (BGL_OBJECTP(BgL_objz00_159))
				{	/* Llib/object.scm 1385 */
					obj_t BgL_oclassz00_6044;

					{	/* Llib/object.scm 893 */
						obj_t BgL_arg1553z00_6045;
						long BgL_arg1554z00_6046;

						BgL_arg1553z00_6045 = (BGl_za2classesza2z00zz__objectz00);
						{	/* Llib/object.scm 894 */
							long BgL_arg1555z00_6047;

							BgL_arg1555z00_6047 =
								BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_objz00_159));
							BgL_arg1554z00_6046 = (BgL_arg1555z00_6047 - OBJECT_TYPE);
						}
						BgL_oclassz00_6044 =
							VECTOR_REF(BgL_arg1553z00_6045, BgL_arg1554z00_6046);
					}
					return (BgL_oclassz00_6044 == BgL_classz00_160);
				}
			else
				{	/* Llib/object.scm 1384 */
					return ((bool_t) 0);
				}
		}

	}



/* &%isa/final? */
	obj_t BGl_z62z52isazf2finalzf3z31zz__objectz00(obj_t BgL_envz00_5202,
		obj_t BgL_objz00_5203, obj_t BgL_classz00_5204)
	{
		{	/* Llib/object.scm 1383 */
			{	/* Llib/object.scm 1384 */
				bool_t BgL_tmpz00_8208;

				{	/* Llib/object.scm 1384 */
					obj_t BgL_auxz00_8209;

					if (BGl_classzf3zf3zz__objectz00(BgL_classz00_5204))
						{	/* Llib/object.scm 1384 */
							BgL_auxz00_8209 = BgL_classz00_5204;
						}
					else
						{
							obj_t BgL_auxz00_8212;

							BgL_auxz00_8212 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
								BINT(60452L), BGl_string2950z00zz__objectz00,
								BGl_string2862z00zz__objectz00, BgL_classz00_5204);
							FAILURE(BgL_auxz00_8212, BFALSE, BFALSE);
						}
					BgL_tmpz00_8208 =
						BGl_z52isazf2finalzf3z53zz__objectz00(BgL_objz00_5203,
						BgL_auxz00_8209);
				}
				return BBOOL(BgL_tmpz00_8208);
			}
		}

	}



/* %isa-object/final? */
	BGL_EXPORTED_DEF bool_t
		BGl_z52isazd2objectzf2finalzf3z81zz__objectz00(BgL_objectz00_bglt
		BgL_objz00_161, obj_t BgL_classz00_162)
	{
		{	/* Llib/object.scm 1391 */
			{	/* Llib/object.scm 1392 */
				obj_t BgL_oclassz00_6048;

				{	/* Llib/object.scm 893 */
					obj_t BgL_arg1553z00_6049;
					long BgL_arg1554z00_6050;

					BgL_arg1553z00_6049 = (BGl_za2classesza2z00zz__objectz00);
					{	/* Llib/object.scm 894 */
						long BgL_arg1555z00_6051;

						BgL_arg1555z00_6051 = BGL_OBJECT_CLASS_NUM(BgL_objz00_161);
						BgL_arg1554z00_6050 = (BgL_arg1555z00_6051 - OBJECT_TYPE);
					}
					BgL_oclassz00_6048 =
						VECTOR_REF(BgL_arg1553z00_6049, BgL_arg1554z00_6050);
				}
				return (BgL_oclassz00_6048 == BgL_classz00_162);
			}
		}

	}



/* &%isa-object/final? */
	obj_t BGl_z62z52isazd2objectzf2finalzf3ze3zz__objectz00(obj_t BgL_envz00_5205,
		obj_t BgL_objz00_5206, obj_t BgL_classz00_5207)
	{
		{	/* Llib/object.scm 1391 */
			{	/* Llib/object.scm 1392 */
				bool_t BgL_tmpz00_8223;

				{	/* Llib/object.scm 1392 */
					obj_t BgL_auxz00_8232;
					BgL_objectz00_bglt BgL_auxz00_8224;

					if (BGl_classzf3zf3zz__objectz00(BgL_classz00_5207))
						{	/* Llib/object.scm 1392 */
							BgL_auxz00_8232 = BgL_classz00_5207;
						}
					else
						{
							obj_t BgL_auxz00_8235;

							BgL_auxz00_8235 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
								BINT(60809L), BGl_string2951z00zz__objectz00,
								BGl_string2862z00zz__objectz00, BgL_classz00_5207);
							FAILURE(BgL_auxz00_8235, BFALSE, BFALSE);
						}
					if (BGl_isazf3zf3zz__objectz00(BgL_objz00_5206,
							BGl_objectz00zz__objectz00))
						{	/* Llib/object.scm 1392 */
							BgL_auxz00_8224 = ((BgL_objectz00_bglt) BgL_objz00_5206);
						}
					else
						{
							obj_t BgL_auxz00_8228;

							BgL_auxz00_8228 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
								BINT(60809L), BGl_string2951z00zz__objectz00,
								BGl_string2903z00zz__objectz00, BgL_objz00_5206);
							FAILURE(BgL_auxz00_8228, BFALSE, BFALSE);
						}
					BgL_tmpz00_8223 =
						BGl_z52isazd2objectzf2finalzf3z81zz__objectz00(BgL_auxz00_8224,
						BgL_auxz00_8232);
				}
				return BBOOL(BgL_tmpz00_8223);
			}
		}

	}



/* allocate-instance */
	BGL_EXPORTED_DEF BgL_objectz00_bglt
		BGl_allocatezd2instancezd2zz__objectz00(obj_t BgL_cnamez00_168)
	{
		{	/* Llib/object.scm 1418 */
			{
				long BgL_iz00_1713;

				{
					obj_t BgL_auxz00_8241;

					BgL_iz00_1713 = 0L;
				BgL_zc3z04anonymousza31814ze3z87_1714:
					if (
						(BgL_iz00_1713 ==
							(long) CINT(BGl_za2nbzd2classesza2zd2zz__objectz00)))
						{	/* Llib/object.scm 1420 */
							BgL_auxz00_8241 =
								BGl_errorz00zz__errorz00(BGl_string2952z00zz__objectz00,
								BGl_string2857z00zz__objectz00, BgL_cnamez00_168);
						}
					else
						{	/* Llib/object.scm 1422 */
							obj_t BgL_classz00_1716;

							BgL_classz00_1716 =
								VECTOR_REF(BGl_za2classesza2z00zz__objectz00, BgL_iz00_1713);
							{	/* Llib/object.scm 1423 */
								bool_t BgL_test3701z00_8247;

								{	/* Llib/object.scm 1423 */
									obj_t BgL_arg1826z00_1728;

									{	/* Llib/object.scm 510 */
										obj_t BgL_tmpz00_8248;

										BgL_tmpz00_8248 = ((obj_t) BgL_classz00_1716);
										BgL_arg1826z00_1728 = BGL_CLASS_NAME(BgL_tmpz00_8248);
									}
									BgL_test3701z00_8247 =
										(BgL_arg1826z00_1728 == BgL_cnamez00_168);
								}
								if (BgL_test3701z00_8247)
									{	/* Llib/object.scm 1424 */
										obj_t BgL_allocz00_1719;

										{	/* Llib/object.scm 1424 */
											obj_t BgL_res2600z00_3867;

											{	/* Llib/object.scm 709 */
												bool_t BgL_test3702z00_8252;

												{	/* Llib/object.scm 469 */
													obj_t BgL_tmpz00_8253;

													BgL_tmpz00_8253 = ((obj_t) BgL_classz00_1716);
													BgL_test3702z00_8252 = BGL_CLASSP(BgL_tmpz00_8253);
												}
												if (BgL_test3702z00_8252)
													{	/* Llib/object.scm 710 */
														obj_t BgL_tmpz00_8256;

														BgL_tmpz00_8256 = ((obj_t) BgL_classz00_1716);
														BgL_res2600z00_3867 =
															BGL_CLASS_ALLOC_FUN(BgL_tmpz00_8256);
													}
												else
													{	/* Llib/object.scm 709 */
														BgL_res2600z00_3867 =
															BGl_bigloozd2typezd2errorz00zz__errorz00
															(BGl_string2891z00zz__objectz00,
															BGl_string2862z00zz__objectz00,
															((obj_t) BgL_classz00_1716));
													}
											}
											BgL_allocz00_1719 = BgL_res2600z00_3867;
										}
										{	/* Llib/object.scm 1425 */
											bool_t BgL_test3703z00_8261;

											{	/* Llib/object.scm 691 */
												obj_t BgL_arg1531z00_3869;

												BgL_arg1531z00_3869 =
													BGl_classzd2shrinkzd2zz__objectz00(
													((obj_t) BgL_classz00_1716));
												BgL_test3703z00_8261 = PROCEDUREP(BgL_arg1531z00_3869);
											}
											if (BgL_test3703z00_8261)
												{	/* Llib/object.scm 1428 */
													bool_t BgL_test3704z00_8265;

													{	/* Llib/object.scm 1428 */
														int BgL_arg1822z00_1726;

														BgL_arg1822z00_1726 =
															PROCEDURE_ARITY(BgL_allocz00_1719);
														BgL_test3704z00_8265 =
															((long) (BgL_arg1822z00_1726) == 0L);
													}
													if (BgL_test3704z00_8265)
														{	/* Llib/object.scm 1428 */
															BgL_auxz00_8241 =
																BGL_PROCEDURE_CALL0(BgL_allocz00_1719);
														}
													else
														{	/* Llib/object.scm 1430 */
															obj_t BgL_superz00_1723;

															{	/* Llib/object.scm 667 */
																obj_t BgL_tmpz00_8272;

																BgL_tmpz00_8272 = ((obj_t) BgL_classz00_1716);
																BgL_superz00_1723 =
																	BGL_CLASS_SUPER(BgL_tmpz00_8272);
															}
															{	/* Llib/object.scm 1430 */
																obj_t BgL_oz00_1724;

																{	/* Llib/object.scm 1431 */
																	obj_t BgL_fun1821z00_1725;

																	{	/* Llib/object.scm 1431 */
																		obj_t BgL_res2601z00_3875;

																		{	/* Llib/object.scm 709 */
																			bool_t BgL_test3705z00_8275;

																			{	/* Llib/object.scm 469 */
																				obj_t BgL_tmpz00_8276;

																				BgL_tmpz00_8276 =
																					((obj_t) BgL_superz00_1723);
																				BgL_test3705z00_8275 =
																					BGL_CLASSP(BgL_tmpz00_8276);
																			}
																			if (BgL_test3705z00_8275)
																				{	/* Llib/object.scm 710 */
																					obj_t BgL_tmpz00_8279;

																					BgL_tmpz00_8279 =
																						((obj_t) BgL_superz00_1723);
																					BgL_res2601z00_3875 =
																						BGL_CLASS_ALLOC_FUN
																						(BgL_tmpz00_8279);
																				}
																			else
																				{	/* Llib/object.scm 709 */
																					BgL_res2601z00_3875 =
																						BGl_bigloozd2typezd2errorz00zz__errorz00
																						(BGl_string2891z00zz__objectz00,
																						BGl_string2862z00zz__objectz00,
																						((obj_t) BgL_superz00_1723));
																				}
																		}
																		BgL_fun1821z00_1725 = BgL_res2601z00_3875;
																	}
																	BgL_oz00_1724 =
																		BGL_PROCEDURE_CALL0(BgL_fun1821z00_1725);
																}
																{	/* Llib/object.scm 1431 */

																	BgL_auxz00_8241 =
																		BGL_PROCEDURE_CALL1(BgL_allocz00_1719,
																		BgL_oz00_1724);
																}
															}
														}
												}
											else
												{	/* Llib/object.scm 1425 */
													BgL_auxz00_8241 =
														BGL_PROCEDURE_CALL0(BgL_allocz00_1719);
												}
										}
									}
								else
									{
										long BgL_iz00_8294;

										BgL_iz00_8294 = (BgL_iz00_1713 + 1L);
										BgL_iz00_1713 = BgL_iz00_8294;
										goto BgL_zc3z04anonymousza31814ze3z87_1714;
									}
							}
						}
					return ((BgL_objectz00_bglt) BgL_auxz00_8241);
				}
			}
		}

	}



/* &allocate-instance */
	BgL_objectz00_bglt BGl_z62allocatezd2instancezb0zz__objectz00(obj_t
		BgL_envz00_5208, obj_t BgL_cnamez00_5209)
	{
		{	/* Llib/object.scm 1418 */
			{	/* Llib/object.scm 1420 */
				obj_t BgL_auxz00_8297;

				if (SYMBOLP(BgL_cnamez00_5209))
					{	/* Llib/object.scm 1420 */
						BgL_auxz00_8297 = BgL_cnamez00_5209;
					}
				else
					{
						obj_t BgL_auxz00_8300;

						BgL_auxz00_8300 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(62245L), BGl_string2953z00zz__objectz00,
							BGl_string2855z00zz__objectz00, BgL_cnamez00_5209);
						FAILURE(BgL_auxz00_8300, BFALSE, BFALSE);
					}
				return BGl_allocatezd2instancezd2zz__objectz00(BgL_auxz00_8297);
			}
		}

	}



/* wide-object? */
	BGL_EXPORTED_DEF bool_t
		BGl_widezd2objectzf3z21zz__objectz00(BgL_objectz00_bglt BgL_objectz00_169)
	{
		{	/* Llib/object.scm 1439 */
			return CBOOL(BGL_OBJECT_WIDENING(BgL_objectz00_169));
		}

	}



/* &wide-object? */
	obj_t BGl_z62widezd2objectzf3z43zz__objectz00(obj_t BgL_envz00_5210,
		obj_t BgL_objectz00_5211)
	{
		{	/* Llib/object.scm 1439 */
			{	/* Llib/object.scm 1440 */
				bool_t BgL_tmpz00_8307;

				{	/* Llib/object.scm 1440 */
					BgL_objectz00_bglt BgL_auxz00_8308;

					if (BGl_isazf3zf3zz__objectz00(BgL_objectz00_5211,
							BGl_objectz00zz__objectz00))
						{	/* Llib/object.scm 1440 */
							BgL_auxz00_8308 = ((BgL_objectz00_bglt) BgL_objectz00_5211);
						}
					else
						{
							obj_t BgL_auxz00_8312;

							BgL_auxz00_8312 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
								BINT(63032L), BGl_string2954z00zz__objectz00,
								BGl_string2903z00zz__objectz00, BgL_objectz00_5211);
							FAILURE(BgL_auxz00_8312, BFALSE, BFALSE);
						}
					BgL_tmpz00_8307 =
						BGl_widezd2objectzf3z21zz__objectz00(BgL_auxz00_8308);
				}
				return BBOOL(BgL_tmpz00_8307);
			}
		}

	}



/* call-virtual-getter */
	BGL_EXPORTED_DEF obj_t
		BGl_callzd2virtualzd2getterz00zz__objectz00(BgL_objectz00_bglt
		BgL_objz00_179, int BgL_numz00_180)
	{
		{	/* Llib/object.scm 1538 */
			{	/* Llib/object.scm 1539 */
				obj_t BgL_classz00_3879;

				{	/* Llib/object.scm 893 */
					obj_t BgL_arg1553z00_3884;
					long BgL_arg1554z00_3885;

					BgL_arg1553z00_3884 = (BGl_za2classesza2z00zz__objectz00);
					{	/* Llib/object.scm 894 */
						long BgL_arg1555z00_3886;

						BgL_arg1555z00_3886 = BGL_OBJECT_CLASS_NUM(BgL_objz00_179);
						BgL_arg1554z00_3885 = (BgL_arg1555z00_3886 - OBJECT_TYPE);
					}
					BgL_classz00_3879 =
						VECTOR_REF(BgL_arg1553z00_3884, BgL_arg1554z00_3885);
				}
				{	/* Llib/object.scm 1539 */
					obj_t BgL_getterz00_3880;

					{	/* Llib/object.scm 1540 */
						obj_t BgL_arg1828z00_3881;

						{	/* Llib/object.scm 1540 */
							obj_t BgL_arg1829z00_3882;

							{	/* Llib/object.scm 534 */
								obj_t BgL_tmpz00_8322;

								BgL_tmpz00_8322 = ((obj_t) BgL_classz00_3879);
								BgL_arg1829z00_3882 = BGL_CLASS_VIRTUAL_FIELDS(BgL_tmpz00_8322);
							}
							BgL_arg1828z00_3881 =
								VECTOR_REF(BgL_arg1829z00_3882, (long) (BgL_numz00_180));
						}
						BgL_getterz00_3880 = CAR(((obj_t) BgL_arg1828z00_3881));
					}
					{	/* Llib/object.scm 1540 */

						return
							BGL_PROCEDURE_CALL1(BgL_getterz00_3880, ((obj_t) BgL_objz00_179));
					}
				}
			}
		}

	}



/* &call-virtual-getter */
	obj_t BGl_z62callzd2virtualzd2getterz62zz__objectz00(obj_t BgL_envz00_5212,
		obj_t BgL_objz00_5213, obj_t BgL_numz00_5214)
	{
		{	/* Llib/object.scm 1538 */
			{	/* Llib/object.scm 1539 */
				int BgL_auxz00_8342;
				BgL_objectz00_bglt BgL_auxz00_8334;

				{	/* Llib/object.scm 1539 */
					obj_t BgL_tmpz00_8343;

					if (INTEGERP(BgL_numz00_5214))
						{	/* Llib/object.scm 1539 */
							BgL_tmpz00_8343 = BgL_numz00_5214;
						}
					else
						{
							obj_t BgL_auxz00_8346;

							BgL_auxz00_8346 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
								BINT(67157L), BGl_string2955z00zz__objectz00,
								BGl_string2860z00zz__objectz00, BgL_numz00_5214);
							FAILURE(BgL_auxz00_8346, BFALSE, BFALSE);
						}
					BgL_auxz00_8342 = CINT(BgL_tmpz00_8343);
				}
				if (BGl_isazf3zf3zz__objectz00(BgL_objz00_5213,
						BGl_objectz00zz__objectz00))
					{	/* Llib/object.scm 1539 */
						BgL_auxz00_8334 = ((BgL_objectz00_bglt) BgL_objz00_5213);
					}
				else
					{
						obj_t BgL_auxz00_8338;

						BgL_auxz00_8338 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(67157L), BGl_string2955z00zz__objectz00,
							BGl_string2903z00zz__objectz00, BgL_objz00_5213);
						FAILURE(BgL_auxz00_8338, BFALSE, BFALSE);
					}
				return
					BGl_callzd2virtualzd2getterz00zz__objectz00(BgL_auxz00_8334,
					BgL_auxz00_8342);
			}
		}

	}



/* call-virtual-setter */
	BGL_EXPORTED_DEF obj_t
		BGl_callzd2virtualzd2setterz00zz__objectz00(BgL_objectz00_bglt
		BgL_objz00_181, int BgL_numz00_182, obj_t BgL_valuez00_183)
	{
		{	/* Llib/object.scm 1551 */
			{	/* Llib/object.scm 1552 */
				obj_t BgL_classz00_3896;

				{	/* Llib/object.scm 893 */
					obj_t BgL_arg1553z00_3901;
					long BgL_arg1554z00_3902;

					BgL_arg1553z00_3901 = (BGl_za2classesza2z00zz__objectz00);
					{	/* Llib/object.scm 894 */
						long BgL_arg1555z00_3903;

						BgL_arg1555z00_3903 = BGL_OBJECT_CLASS_NUM(BgL_objz00_181);
						BgL_arg1554z00_3902 = (BgL_arg1555z00_3903 - OBJECT_TYPE);
					}
					BgL_classz00_3896 =
						VECTOR_REF(BgL_arg1553z00_3901, BgL_arg1554z00_3902);
				}
				{	/* Llib/object.scm 1552 */
					obj_t BgL_setterz00_3897;

					{	/* Llib/object.scm 1553 */
						obj_t BgL_arg1831z00_3898;

						{	/* Llib/object.scm 1553 */
							obj_t BgL_arg1832z00_3899;

							{	/* Llib/object.scm 534 */
								obj_t BgL_tmpz00_8356;

								BgL_tmpz00_8356 = ((obj_t) BgL_classz00_3896);
								BgL_arg1832z00_3899 = BGL_CLASS_VIRTUAL_FIELDS(BgL_tmpz00_8356);
							}
							BgL_arg1831z00_3898 =
								VECTOR_REF(BgL_arg1832z00_3899, (long) (BgL_numz00_182));
						}
						BgL_setterz00_3897 = CDR(((obj_t) BgL_arg1831z00_3898));
					}
					{	/* Llib/object.scm 1553 */

						return
							BGL_PROCEDURE_CALL2(BgL_setterz00_3897,
							((obj_t) BgL_objz00_181), BgL_valuez00_183);
					}
				}
			}
		}

	}



/* &call-virtual-setter */
	obj_t BGl_z62callzd2virtualzd2setterz62zz__objectz00(obj_t BgL_envz00_5215,
		obj_t BgL_objz00_5216, obj_t BgL_numz00_5217, obj_t BgL_valuez00_5218)
	{
		{	/* Llib/object.scm 1551 */
			{	/* Llib/object.scm 1552 */
				int BgL_auxz00_8377;
				BgL_objectz00_bglt BgL_auxz00_8369;

				{	/* Llib/object.scm 1552 */
					obj_t BgL_tmpz00_8378;

					if (INTEGERP(BgL_numz00_5217))
						{	/* Llib/object.scm 1552 */
							BgL_tmpz00_8378 = BgL_numz00_5217;
						}
					else
						{
							obj_t BgL_auxz00_8381;

							BgL_auxz00_8381 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
								BINT(67926L), BGl_string2956z00zz__objectz00,
								BGl_string2860z00zz__objectz00, BgL_numz00_5217);
							FAILURE(BgL_auxz00_8381, BFALSE, BFALSE);
						}
					BgL_auxz00_8377 = CINT(BgL_tmpz00_8378);
				}
				if (BGl_isazf3zf3zz__objectz00(BgL_objz00_5216,
						BGl_objectz00zz__objectz00))
					{	/* Llib/object.scm 1552 */
						BgL_auxz00_8369 = ((BgL_objectz00_bglt) BgL_objz00_5216);
					}
				else
					{
						obj_t BgL_auxz00_8373;

						BgL_auxz00_8373 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(67926L), BGl_string2956z00zz__objectz00,
							BGl_string2903z00zz__objectz00, BgL_objz00_5216);
						FAILURE(BgL_auxz00_8373, BFALSE, BFALSE);
					}
				return
					BGl_callzd2virtualzd2setterz00zz__objectz00(BgL_auxz00_8369,
					BgL_auxz00_8377, BgL_valuez00_5218);
			}
		}

	}



/* call-next-virtual-getter */
	BGL_EXPORTED_DEF obj_t
		BGl_callzd2nextzd2virtualzd2getterzd2zz__objectz00(obj_t BgL_classz00_184,
		BgL_objectz00_bglt BgL_objz00_185, int BgL_numz00_186)
	{
		{	/* Llib/object.scm 1574 */
			{	/* Llib/object.scm 1575 */
				obj_t BgL_nextzd2classzd2_3913;

				{	/* Llib/object.scm 667 */
					obj_t BgL_tmpz00_8387;

					BgL_tmpz00_8387 = ((obj_t) BgL_classz00_184);
					BgL_nextzd2classzd2_3913 = BGL_CLASS_SUPER(BgL_tmpz00_8387);
				}
				{	/* Llib/object.scm 1576 */
					obj_t BgL_fun1836z00_3914;

					{	/* Llib/object.scm 1576 */
						obj_t BgL_arg1833z00_3915;

						{	/* Llib/object.scm 1576 */
							obj_t BgL_arg1834z00_3916;

							{	/* Llib/object.scm 534 */
								obj_t BgL_tmpz00_8390;

								BgL_tmpz00_8390 = ((obj_t) BgL_nextzd2classzd2_3913);
								BgL_arg1834z00_3916 = BGL_CLASS_VIRTUAL_FIELDS(BgL_tmpz00_8390);
							}
							BgL_arg1833z00_3915 =
								VECTOR_REF(BgL_arg1834z00_3916, (long) (BgL_numz00_186));
						}
						BgL_fun1836z00_3914 = CAR(((obj_t) BgL_arg1833z00_3915));
					}
					return
						BGL_PROCEDURE_CALL1(BgL_fun1836z00_3914, ((obj_t) BgL_objz00_185));
				}
			}
		}

	}



/* &call-next-virtual-getter */
	obj_t BGl_z62callzd2nextzd2virtualzd2getterzb0zz__objectz00(obj_t
		BgL_envz00_5219, obj_t BgL_classz00_5220, obj_t BgL_objz00_5221,
		obj_t BgL_numz00_5222)
	{
		{	/* Llib/object.scm 1574 */
			{	/* Llib/object.scm 1575 */
				int BgL_auxz00_8410;
				BgL_objectz00_bglt BgL_auxz00_8402;

				{	/* Llib/object.scm 1575 */
					obj_t BgL_tmpz00_8411;

					if (INTEGERP(BgL_numz00_5222))
						{	/* Llib/object.scm 1575 */
							BgL_tmpz00_8411 = BgL_numz00_5222;
						}
					else
						{
							obj_t BgL_auxz00_8414;

							BgL_auxz00_8414 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
								BINT(69446L), BGl_string2957z00zz__objectz00,
								BGl_string2860z00zz__objectz00, BgL_numz00_5222);
							FAILURE(BgL_auxz00_8414, BFALSE, BFALSE);
						}
					BgL_auxz00_8410 = CINT(BgL_tmpz00_8411);
				}
				if (BGl_isazf3zf3zz__objectz00(BgL_objz00_5221,
						BGl_objectz00zz__objectz00))
					{	/* Llib/object.scm 1575 */
						BgL_auxz00_8402 = ((BgL_objectz00_bglt) BgL_objz00_5221);
					}
				else
					{
						obj_t BgL_auxz00_8406;

						BgL_auxz00_8406 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(69446L), BGl_string2957z00zz__objectz00,
							BGl_string2903z00zz__objectz00, BgL_objz00_5221);
						FAILURE(BgL_auxz00_8406, BFALSE, BFALSE);
					}
				return
					BGl_callzd2nextzd2virtualzd2getterzd2zz__objectz00(BgL_classz00_5220,
					BgL_auxz00_8402, BgL_auxz00_8410);
			}
		}

	}



/* call-next-virtual-setter */
	BGL_EXPORTED_DEF obj_t
		BGl_callzd2nextzd2virtualzd2setterzd2zz__objectz00(obj_t BgL_classz00_187,
		BgL_objectz00_bglt BgL_objz00_188, int BgL_numz00_189,
		obj_t BgL_valuez00_190)
	{
		{	/* Llib/object.scm 1586 */
			{	/* Llib/object.scm 1587 */
				obj_t BgL_nextzd2classzd2_3922;

				{	/* Llib/object.scm 667 */
					obj_t BgL_tmpz00_8420;

					BgL_tmpz00_8420 = ((obj_t) BgL_classz00_187);
					BgL_nextzd2classzd2_3922 = BGL_CLASS_SUPER(BgL_tmpz00_8420);
				}
				{	/* Llib/object.scm 1588 */
					obj_t BgL_fun1839z00_3923;

					{	/* Llib/object.scm 1588 */
						obj_t BgL_arg1837z00_3924;

						{	/* Llib/object.scm 1588 */
							obj_t BgL_arg1838z00_3925;

							{	/* Llib/object.scm 534 */
								obj_t BgL_tmpz00_8423;

								BgL_tmpz00_8423 = ((obj_t) BgL_nextzd2classzd2_3922);
								BgL_arg1838z00_3925 = BGL_CLASS_VIRTUAL_FIELDS(BgL_tmpz00_8423);
							}
							BgL_arg1837z00_3924 =
								VECTOR_REF(BgL_arg1838z00_3925, (long) (BgL_numz00_189));
						}
						BgL_fun1839z00_3923 = CDR(((obj_t) BgL_arg1837z00_3924));
					}
					return
						BGL_PROCEDURE_CALL2(BgL_fun1839z00_3923,
						((obj_t) BgL_objz00_188), BgL_valuez00_190);
				}
			}
		}

	}



/* &call-next-virtual-setter */
	obj_t BGl_z62callzd2nextzd2virtualzd2setterzb0zz__objectz00(obj_t
		BgL_envz00_5223, obj_t BgL_classz00_5224, obj_t BgL_objz00_5225,
		obj_t BgL_numz00_5226, obj_t BgL_valuez00_5227)
	{
		{	/* Llib/object.scm 1586 */
			{	/* Llib/object.scm 1587 */
				int BgL_auxz00_8444;
				BgL_objectz00_bglt BgL_auxz00_8436;

				{	/* Llib/object.scm 1587 */
					obj_t BgL_tmpz00_8445;

					if (INTEGERP(BgL_numz00_5226))
						{	/* Llib/object.scm 1587 */
							BgL_tmpz00_8445 = BgL_numz00_5226;
						}
					else
						{
							obj_t BgL_auxz00_8448;

							BgL_auxz00_8448 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
								BINT(70217L), BGl_string2958z00zz__objectz00,
								BGl_string2860z00zz__objectz00, BgL_numz00_5226);
							FAILURE(BgL_auxz00_8448, BFALSE, BFALSE);
						}
					BgL_auxz00_8444 = CINT(BgL_tmpz00_8445);
				}
				if (BGl_isazf3zf3zz__objectz00(BgL_objz00_5225,
						BGl_objectz00zz__objectz00))
					{	/* Llib/object.scm 1587 */
						BgL_auxz00_8436 = ((BgL_objectz00_bglt) BgL_objz00_5225);
					}
				else
					{
						obj_t BgL_auxz00_8440;

						BgL_auxz00_8440 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(70217L), BGl_string2958z00zz__objectz00,
							BGl_string2903z00zz__objectz00, BgL_objz00_5225);
						FAILURE(BgL_auxz00_8440, BFALSE, BFALSE);
					}
				return
					BGl_callzd2nextzd2virtualzd2setterzd2zz__objectz00(BgL_classz00_5224,
					BgL_auxz00_8436, BgL_auxz00_8444, BgL_valuez00_5227);
			}
		}

	}



/* object-widening */
	BGL_EXPORTED_DEF obj_t
		BGl_objectzd2wideningzd2zz__objectz00(BgL_objectz00_bglt BgL_oz00_191)
	{
		{	/* Llib/object.scm 1593 */
			return BGL_OBJECT_WIDENING(BgL_oz00_191);
		}

	}



/* &object-widening */
	obj_t BGl_z62objectzd2wideningzb0zz__objectz00(obj_t BgL_envz00_5228,
		obj_t BgL_oz00_5229)
	{
		{	/* Llib/object.scm 1593 */
			{	/* Llib/object.scm 1594 */
				BgL_objectz00_bglt BgL_auxz00_8455;

				if (BGl_isazf3zf3zz__objectz00(BgL_oz00_5229,
						BGl_objectz00zz__objectz00))
					{	/* Llib/object.scm 1594 */
						BgL_auxz00_8455 = ((BgL_objectz00_bglt) BgL_oz00_5229);
					}
				else
					{
						obj_t BgL_auxz00_8459;

						BgL_auxz00_8459 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(70591L), BGl_string2959z00zz__objectz00,
							BGl_string2903z00zz__objectz00, BgL_oz00_5229);
						FAILURE(BgL_auxz00_8459, BFALSE, BFALSE);
					}
				return BGl_objectzd2wideningzd2zz__objectz00(BgL_auxz00_8455);
			}
		}

	}



/* object-widening-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_objectzd2wideningzd2setz12z12zz__objectz00(BgL_objectz00_bglt
		BgL_oz00_192, obj_t BgL_vz00_193)
	{
		{	/* Llib/object.scm 1599 */
			BGL_OBJECT_WIDENING_SET(BgL_oz00_192, BgL_vz00_193);
			return ((obj_t) BgL_oz00_192);
		}

	}



/* &object-widening-set! */
	obj_t BGl_z62objectzd2wideningzd2setz12z70zz__objectz00(obj_t BgL_envz00_5230,
		obj_t BgL_oz00_5231, obj_t BgL_vz00_5232)
	{
		{	/* Llib/object.scm 1599 */
			{	/* Llib/object.scm 1600 */
				BgL_objectz00_bglt BgL_auxz00_8466;

				if (BGl_isazf3zf3zz__objectz00(BgL_oz00_5231,
						BGl_objectz00zz__objectz00))
					{	/* Llib/object.scm 1600 */
						BgL_auxz00_8466 = ((BgL_objectz00_bglt) BgL_oz00_5231);
					}
				else
					{
						obj_t BgL_auxz00_8470;

						BgL_auxz00_8470 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(70907L), BGl_string2960z00zz__objectz00,
							BGl_string2903z00zz__objectz00, BgL_oz00_5231);
						FAILURE(BgL_auxz00_8470, BFALSE, BFALSE);
					}
				return
					BGl_objectzd2wideningzd2setz12z12zz__objectz00(BgL_auxz00_8466,
					BgL_vz00_5232);
			}
		}

	}



/* %object-widening */
	BGL_EXPORTED_DEF obj_t
		BGl_z52objectzd2wideningz80zz__objectz00(BgL_objectz00_bglt BgL_oz00_194)
	{
		{	/* Llib/object.scm 1609 */
			return BGL_OBJECT_WIDENING(BgL_oz00_194);
		}

	}



/* &%object-widening */
	obj_t BGl_z62z52objectzd2wideningze2zz__objectz00(obj_t BgL_envz00_5233,
		obj_t BgL_oz00_5234)
	{
		{	/* Llib/object.scm 1609 */
			{	/* Llib/object.scm 1594 */
				BgL_objectz00_bglt BgL_auxz00_8476;

				if (BGl_isazf3zf3zz__objectz00(BgL_oz00_5234,
						BGl_objectz00zz__objectz00))
					{	/* Llib/object.scm 1594 */
						BgL_auxz00_8476 = ((BgL_objectz00_bglt) BgL_oz00_5234);
					}
				else
					{
						obj_t BgL_auxz00_8480;

						BgL_auxz00_8480 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(70591L), BGl_string2961z00zz__objectz00,
							BGl_string2903z00zz__objectz00, BgL_oz00_5234);
						FAILURE(BgL_auxz00_8480, BFALSE, BFALSE);
					}
				return BGl_z52objectzd2wideningz80zz__objectz00(BgL_auxz00_8476);
			}
		}

	}



/* %object-widening-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_z52objectzd2wideningzd2setz12z40zz__objectz00(BgL_objectz00_bglt
		BgL_oz00_195, obj_t BgL_wz00_196)
	{
		{	/* Llib/object.scm 1618 */
			BGL_OBJECT_WIDENING_SET(BgL_oz00_195, BgL_wz00_196);
			BgL_oz00_195;
			return ((obj_t) BgL_oz00_195);
		}

	}



/* &%object-widening-set! */
	obj_t BGl_z62z52objectzd2wideningzd2setz12z22zz__objectz00(obj_t
		BgL_envz00_5235, obj_t BgL_oz00_5236, obj_t BgL_wz00_5237)
	{
		{	/* Llib/object.scm 1618 */
			{	/* Llib/object.scm 1619 */
				BgL_objectz00_bglt BgL_auxz00_8487;

				if (BGl_isazf3zf3zz__objectz00(BgL_oz00_5236,
						BGl_objectz00zz__objectz00))
					{	/* Llib/object.scm 1619 */
						BgL_auxz00_8487 = ((BgL_objectz00_bglt) BgL_oz00_5236);
					}
				else
					{
						obj_t BgL_auxz00_8491;

						BgL_auxz00_8491 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(71922L), BGl_string2962z00zz__objectz00,
							BGl_string2903z00zz__objectz00, BgL_oz00_5236);
						FAILURE(BgL_auxz00_8491, BFALSE, BFALSE);
					}
				return
					BGl_z52objectzd2wideningzd2setz12z40zz__objectz00(BgL_auxz00_8487,
					BgL_wz00_5237);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__objectz00(void)
	{
		{	/* Llib/object.scm 17 */
			{	/* Llib/object.scm 236 */
				obj_t BgL_arg1844z00_1750;
				obj_t BgL_arg1845z00_1751;

				{	/* Llib/object.scm 236 */
					obj_t BgL_v1328z00_1761;

					BgL_v1328z00_1761 = create_vector(0L);
					BgL_arg1844z00_1750 = BgL_v1328z00_1761;
				}
				{	/* Llib/object.scm 236 */
					obj_t BgL_v1329z00_1762;

					BgL_v1329z00_1762 = create_vector(0L);
					BgL_arg1845z00_1751 = BgL_v1329z00_1762;
				}
				BGl_objectz00zz__objectz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol2966z00zz__objectz00, BGl_symbol2967z00zz__objectz00,
					BFALSE, 43933L, BGl_proc2965z00zz__objectz00,
					BGl_proc2964z00zz__objectz00, BFALSE, BGl_proc2963z00zz__objectz00,
					BFALSE, BgL_arg1844z00_1750, BgL_arg1845z00_1751);
			}
			{	/* Llib/object.scm 237 */
				obj_t BgL_arg1854z00_1769;
				obj_t BgL_arg1856z00_1770;

				{	/* Llib/object.scm 237 */
					obj_t BgL_v1330z00_1780;

					BgL_v1330z00_1780 = create_vector(0L);
					BgL_arg1854z00_1769 = BgL_v1330z00_1780;
				}
				{	/* Llib/object.scm 237 */
					obj_t BgL_v1331z00_1781;

					BgL_v1331z00_1781 = create_vector(0L);
					BgL_arg1856z00_1770 = BgL_v1331z00_1781;
				}
				BGl_z62conditionz62zz__objectz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol2972z00zz__objectz00, BGl_symbol2967z00zz__objectz00,
					BGl_objectz00zz__objectz00, 33606L, BGl_proc2971z00zz__objectz00,
					BGl_proc2970z00zz__objectz00, BFALSE, BGl_proc2969z00zz__objectz00,
					BFALSE, BgL_arg1854z00_1769, BgL_arg1856z00_1770);
			}
			{	/* Llib/object.scm 239 */
				obj_t BgL_arg1866z00_1788;
				obj_t BgL_arg1868z00_1789;

				{	/* Llib/object.scm 239 */
					obj_t BgL_v1332z00_1802;

					BgL_v1332z00_1802 = create_vector(3L);
					{	/* Llib/object.scm 239 */
						obj_t BgL_arg1874z00_1803;

						{	/* Llib/object.scm 239 */
							obj_t BgL_res2602z00_3964;

							{	/* Llib/object.scm 239 */
								obj_t BgL_namez00_3949;
								obj_t BgL_typez00_3952;

								BgL_namez00_3949 = BGl_symbol2977z00zz__objectz00;
								BgL_typez00_3952 = BGl_symbol2979z00zz__objectz00;
								{	/* Llib/object.scm 591 */
									obj_t BgL_v1316z00_3953;

									BgL_v1316z00_3953 = create_vector(9L);
									VECTOR_SET(BgL_v1316z00_3953, 0L, BgL_namez00_3949);
									VECTOR_SET(BgL_v1316z00_3953, 1L,
										BGl_proc2976z00zz__objectz00);
									VECTOR_SET(BgL_v1316z00_3953, 2L,
										BGl_proc2975z00zz__objectz00);
									VECTOR_SET(BgL_v1316z00_3953, 3L, BFALSE);
									VECTOR_SET(BgL_v1316z00_3953, 4L,
										BGl_makezd2classzd2fieldzd2envzd2zz__objectz00);
									VECTOR_SET(BgL_v1316z00_3953, 5L, BFALSE);
									VECTOR_SET(BgL_v1316z00_3953, 6L,
										BGl_proc2974z00zz__objectz00);
									VECTOR_SET(BgL_v1316z00_3953, 7L, BgL_typez00_3952);
									VECTOR_SET(BgL_v1316z00_3953, 8L, BTRUE);
									BgL_res2602z00_3964 = BgL_v1316z00_3953;
							}}
							BgL_arg1874z00_1803 = BgL_res2602z00_3964;
						}
						VECTOR_SET(BgL_v1332z00_1802, 0L, BgL_arg1874z00_1803);
					}
					{	/* Llib/object.scm 239 */
						obj_t BgL_arg1882z00_1816;

						{	/* Llib/object.scm 239 */
							obj_t BgL_res2603z00_3981;

							{	/* Llib/object.scm 239 */
								obj_t BgL_namez00_3966;
								obj_t BgL_typez00_3969;

								BgL_namez00_3966 = BGl_symbol2984z00zz__objectz00;
								BgL_typez00_3969 = BGl_symbol2979z00zz__objectz00;
								{	/* Llib/object.scm 591 */
									obj_t BgL_v1316z00_3970;

									BgL_v1316z00_3970 = create_vector(9L);
									VECTOR_SET(BgL_v1316z00_3970, 0L, BgL_namez00_3966);
									VECTOR_SET(BgL_v1316z00_3970, 1L,
										BGl_proc2983z00zz__objectz00);
									VECTOR_SET(BgL_v1316z00_3970, 2L,
										BGl_proc2982z00zz__objectz00);
									VECTOR_SET(BgL_v1316z00_3970, 3L, BFALSE);
									VECTOR_SET(BgL_v1316z00_3970, 4L,
										BGl_makezd2classzd2fieldzd2envzd2zz__objectz00);
									VECTOR_SET(BgL_v1316z00_3970, 5L, BFALSE);
									VECTOR_SET(BgL_v1316z00_3970, 6L,
										BGl_proc2981z00zz__objectz00);
									VECTOR_SET(BgL_v1316z00_3970, 7L, BgL_typez00_3969);
									VECTOR_SET(BgL_v1316z00_3970, 8L, BTRUE);
									BgL_res2603z00_3981 = BgL_v1316z00_3970;
							}}
							BgL_arg1882z00_1816 = BgL_res2603z00_3981;
						}
						VECTOR_SET(BgL_v1332z00_1802, 1L, BgL_arg1882z00_1816);
					}
					{	/* Llib/object.scm 239 */
						obj_t BgL_arg1889z00_1829;

						{	/* Llib/object.scm 239 */
							obj_t BgL_res2604z00_3999;

							{	/* Llib/object.scm 239 */
								obj_t BgL_namez00_3984;
								obj_t BgL_typez00_3987;

								BgL_namez00_3984 = BGl_symbol2989z00zz__objectz00;
								BgL_typez00_3987 = BGl_symbol2979z00zz__objectz00;
								{	/* Llib/object.scm 591 */
									obj_t BgL_v1316z00_3988;

									BgL_v1316z00_3988 = create_vector(9L);
									VECTOR_SET(BgL_v1316z00_3988, 0L, BgL_namez00_3984);
									VECTOR_SET(BgL_v1316z00_3988, 1L,
										BGl_proc2988z00zz__objectz00);
									VECTOR_SET(BgL_v1316z00_3988, 2L,
										BGl_proc2987z00zz__objectz00);
									VECTOR_SET(BgL_v1316z00_3988, 3L, BFALSE);
									VECTOR_SET(BgL_v1316z00_3988, 4L,
										BGl_makezd2classzd2fieldzd2envzd2zz__objectz00);
									VECTOR_SET(BgL_v1316z00_3988, 5L, BFALSE);
									VECTOR_SET(BgL_v1316z00_3988, 6L,
										BGl_proc2986z00zz__objectz00);
									VECTOR_SET(BgL_v1316z00_3988, 7L, BgL_typez00_3987);
									VECTOR_SET(BgL_v1316z00_3988, 8L, BTRUE);
									BgL_res2604z00_3999 = BgL_v1316z00_3988;
							}}
							BgL_arg1889z00_1829 = BgL_res2604z00_3999;
						}
						VECTOR_SET(BgL_v1332z00_1802, 2L, BgL_arg1889z00_1829);
					}
					BgL_arg1866z00_1788 = BgL_v1332z00_1802;
				}
				{	/* Llib/object.scm 239 */
					obj_t BgL_v1333z00_1843;

					BgL_v1333z00_1843 = create_vector(0L);
					BgL_arg1868z00_1789 = BgL_v1333z00_1843;
				}
				BGl_z62exceptionz62zz__objectz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol2994z00zz__objectz00, BGl_symbol2967z00zz__objectz00,
					BGl_z62conditionz62zz__objectz00, 42025L,
					BGl_proc2993z00zz__objectz00, BGl_proc2992z00zz__objectz00, BFALSE,
					BGl_proc2991z00zz__objectz00, BFALSE, BgL_arg1866z00_1788,
					BgL_arg1868z00_1789);
			}
			{	/* Llib/object.scm 244 */
				obj_t BgL_arg1899z00_1850;
				obj_t BgL_arg1901z00_1851;

				{	/* Llib/object.scm 244 */
					obj_t BgL_v1334z00_1867;

					BgL_v1334z00_1867 = create_vector(3L);
					{	/* Llib/object.scm 244 */
						obj_t BgL_arg1910z00_1868;

						{	/* Llib/object.scm 244 */
							obj_t BgL_res2605z00_4027;

							{	/* Llib/object.scm 244 */
								obj_t BgL_namez00_4012;
								obj_t BgL_typez00_4015;

								BgL_namez00_4012 = BGl_symbol2998z00zz__objectz00;
								BgL_typez00_4015 = BGl_symbol2979z00zz__objectz00;
								{	/* Llib/object.scm 591 */
									obj_t BgL_v1316z00_4016;

									BgL_v1316z00_4016 = create_vector(9L);
									VECTOR_SET(BgL_v1316z00_4016, 0L, BgL_namez00_4012);
									VECTOR_SET(BgL_v1316z00_4016, 1L,
										BGl_proc2997z00zz__objectz00);
									VECTOR_SET(BgL_v1316z00_4016, 2L,
										BGl_proc2996z00zz__objectz00);
									VECTOR_SET(BgL_v1316z00_4016, 3L, BFALSE);
									VECTOR_SET(BgL_v1316z00_4016, 4L,
										BGl_makezd2classzd2fieldzd2envzd2zz__objectz00);
									VECTOR_SET(BgL_v1316z00_4016, 5L, BFALSE);
									VECTOR_SET(BgL_v1316z00_4016, 6L, BFALSE);
									VECTOR_SET(BgL_v1316z00_4016, 7L, BgL_typez00_4015);
									VECTOR_SET(BgL_v1316z00_4016, 8L, BTRUE);
									BgL_res2605z00_4027 = BgL_v1316z00_4016;
							}}
							BgL_arg1910z00_1868 = BgL_res2605z00_4027;
						}
						VECTOR_SET(BgL_v1334z00_1867, 0L, BgL_arg1910z00_1868);
					}
					{	/* Llib/object.scm 244 */
						obj_t BgL_arg1916z00_1878;

						{	/* Llib/object.scm 244 */
							obj_t BgL_res2606z00_4044;

							{	/* Llib/object.scm 244 */
								obj_t BgL_namez00_4029;
								obj_t BgL_typez00_4032;

								BgL_namez00_4029 = BGl_symbol3002z00zz__objectz00;
								BgL_typez00_4032 = BGl_symbol2979z00zz__objectz00;
								{	/* Llib/object.scm 591 */
									obj_t BgL_v1316z00_4033;

									BgL_v1316z00_4033 = create_vector(9L);
									VECTOR_SET(BgL_v1316z00_4033, 0L, BgL_namez00_4029);
									VECTOR_SET(BgL_v1316z00_4033, 1L,
										BGl_proc3001z00zz__objectz00);
									VECTOR_SET(BgL_v1316z00_4033, 2L,
										BGl_proc3000z00zz__objectz00);
									VECTOR_SET(BgL_v1316z00_4033, 3L, BFALSE);
									VECTOR_SET(BgL_v1316z00_4033, 4L,
										BGl_makezd2classzd2fieldzd2envzd2zz__objectz00);
									VECTOR_SET(BgL_v1316z00_4033, 5L, BFALSE);
									VECTOR_SET(BgL_v1316z00_4033, 6L, BFALSE);
									VECTOR_SET(BgL_v1316z00_4033, 7L, BgL_typez00_4032);
									VECTOR_SET(BgL_v1316z00_4033, 8L, BFALSE);
									BgL_res2606z00_4044 = BgL_v1316z00_4033;
							}}
							BgL_arg1916z00_1878 = BgL_res2606z00_4044;
						}
						VECTOR_SET(BgL_v1334z00_1867, 1L, BgL_arg1916z00_1878);
					}
					{	/* Llib/object.scm 244 */
						obj_t BgL_arg1923z00_1888;

						{	/* Llib/object.scm 244 */
							obj_t BgL_res2607z00_4061;

							{	/* Llib/object.scm 244 */
								obj_t BgL_namez00_4046;
								obj_t BgL_typez00_4049;

								BgL_namez00_4046 = BGl_symbol2979z00zz__objectz00;
								BgL_typez00_4049 = BGl_symbol2979z00zz__objectz00;
								{	/* Llib/object.scm 591 */
									obj_t BgL_v1316z00_4050;

									BgL_v1316z00_4050 = create_vector(9L);
									VECTOR_SET(BgL_v1316z00_4050, 0L, BgL_namez00_4046);
									VECTOR_SET(BgL_v1316z00_4050, 1L,
										BGl_proc3005z00zz__objectz00);
									VECTOR_SET(BgL_v1316z00_4050, 2L,
										BGl_proc3004z00zz__objectz00);
									VECTOR_SET(BgL_v1316z00_4050, 3L, BFALSE);
									VECTOR_SET(BgL_v1316z00_4050, 4L,
										BGl_makezd2classzd2fieldzd2envzd2zz__objectz00);
									VECTOR_SET(BgL_v1316z00_4050, 5L, BFALSE);
									VECTOR_SET(BgL_v1316z00_4050, 6L, BFALSE);
									VECTOR_SET(BgL_v1316z00_4050, 7L, BgL_typez00_4049);
									VECTOR_SET(BgL_v1316z00_4050, 8L, BFALSE);
									BgL_res2607z00_4061 = BgL_v1316z00_4050;
							}}
							BgL_arg1923z00_1888 = BgL_res2607z00_4061;
						}
						VECTOR_SET(BgL_v1334z00_1867, 2L, BgL_arg1923z00_1888);
					}
					BgL_arg1899z00_1850 = BgL_v1334z00_1867;
				}
				{	/* Llib/object.scm 244 */
					obj_t BgL_v1335z00_1898;

					BgL_v1335z00_1898 = create_vector(0L);
					BgL_arg1901z00_1851 = BgL_v1335z00_1898;
				}
				BGl_z62errorz62zz__objectz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol3009z00zz__objectz00, BGl_symbol2967z00zz__objectz00,
					BGl_z62exceptionz62zz__objectz00, 27634L,
					BGl_proc3008z00zz__objectz00, BGl_proc3007z00zz__objectz00, BFALSE,
					BGl_proc3006z00zz__objectz00, BFALSE, BgL_arg1899z00_1850,
					BgL_arg1901z00_1851);
			}
			{	/* Llib/object.scm 248 */
				obj_t BgL_arg1931z00_1905;
				obj_t BgL_arg1932z00_1906;

				{	/* Llib/object.scm 248 */
					obj_t BgL_v1336z00_1923;

					BgL_v1336z00_1923 = create_vector(1L);
					{	/* Llib/object.scm 248 */
						obj_t BgL_arg1938z00_1924;

						{	/* Llib/object.scm 248 */
							obj_t BgL_res2608z00_4089;

							{	/* Llib/object.scm 248 */
								obj_t BgL_namez00_4074;
								obj_t BgL_typez00_4077;

								BgL_namez00_4074 = BGl_symbol3013z00zz__objectz00;
								BgL_typez00_4077 = BGl_symbol2979z00zz__objectz00;
								{	/* Llib/object.scm 591 */
									obj_t BgL_v1316z00_4078;

									BgL_v1316z00_4078 = create_vector(9L);
									VECTOR_SET(BgL_v1316z00_4078, 0L, BgL_namez00_4074);
									VECTOR_SET(BgL_v1316z00_4078, 1L,
										BGl_proc3012z00zz__objectz00);
									VECTOR_SET(BgL_v1316z00_4078, 2L,
										BGl_proc3011z00zz__objectz00);
									VECTOR_SET(BgL_v1316z00_4078, 3L, BFALSE);
									VECTOR_SET(BgL_v1316z00_4078, 4L,
										BGl_makezd2classzd2fieldzd2envzd2zz__objectz00);
									VECTOR_SET(BgL_v1316z00_4078, 5L, BFALSE);
									VECTOR_SET(BgL_v1316z00_4078, 6L, BFALSE);
									VECTOR_SET(BgL_v1316z00_4078, 7L, BgL_typez00_4077);
									VECTOR_SET(BgL_v1316z00_4078, 8L, BFALSE);
									BgL_res2608z00_4089 = BgL_v1316z00_4078;
							}}
							BgL_arg1938z00_1924 = BgL_res2608z00_4089;
						}
						VECTOR_SET(BgL_v1336z00_1923, 0L, BgL_arg1938z00_1924);
					}
					BgL_arg1931z00_1905 = BgL_v1336z00_1923;
				}
				{	/* Llib/object.scm 248 */
					obj_t BgL_v1337z00_1934;

					BgL_v1337z00_1934 = create_vector(0L);
					BgL_arg1932z00_1906 = BgL_v1337z00_1934;
				}
				BGl_z62typezd2errorzb0zz__objectz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol3018z00zz__objectz00, BGl_symbol2967z00zz__objectz00,
					BGl_z62errorz62zz__objectz00, 59179L, BGl_proc3017z00zz__objectz00,
					BGl_proc3016z00zz__objectz00, BFALSE, BGl_proc3015z00zz__objectz00,
					BFALSE, BgL_arg1931z00_1905, BgL_arg1932z00_1906);
			}
			{	/* Llib/object.scm 250 */
				obj_t BgL_arg1946z00_1941;
				obj_t BgL_arg1947z00_1942;

				{	/* Llib/object.scm 250 */
					obj_t BgL_v1338z00_1959;

					BgL_v1338z00_1959 = create_vector(1L);
					{	/* Llib/object.scm 250 */
						obj_t BgL_arg1953z00_1960;

						{	/* Llib/object.scm 250 */
							obj_t BgL_res2609z00_4117;

							{	/* Llib/object.scm 250 */
								obj_t BgL_namez00_4102;
								obj_t BgL_typez00_4105;

								BgL_namez00_4102 = BGl_symbol3022z00zz__objectz00;
								BgL_typez00_4105 = BGl_symbol2979z00zz__objectz00;
								{	/* Llib/object.scm 591 */
									obj_t BgL_v1316z00_4106;

									BgL_v1316z00_4106 = create_vector(9L);
									VECTOR_SET(BgL_v1316z00_4106, 0L, BgL_namez00_4102);
									VECTOR_SET(BgL_v1316z00_4106, 1L,
										BGl_proc3021z00zz__objectz00);
									VECTOR_SET(BgL_v1316z00_4106, 2L,
										BGl_proc3020z00zz__objectz00);
									VECTOR_SET(BgL_v1316z00_4106, 3L, BFALSE);
									VECTOR_SET(BgL_v1316z00_4106, 4L,
										BGl_makezd2classzd2fieldzd2envzd2zz__objectz00);
									VECTOR_SET(BgL_v1316z00_4106, 5L, BFALSE);
									VECTOR_SET(BgL_v1316z00_4106, 6L, BFALSE);
									VECTOR_SET(BgL_v1316z00_4106, 7L, BgL_typez00_4105);
									VECTOR_SET(BgL_v1316z00_4106, 8L, BFALSE);
									BgL_res2609z00_4117 = BgL_v1316z00_4106;
							}}
							BgL_arg1953z00_1960 = BgL_res2609z00_4117;
						}
						VECTOR_SET(BgL_v1338z00_1959, 0L, BgL_arg1953z00_1960);
					}
					BgL_arg1946z00_1941 = BgL_v1338z00_1959;
				}
				{	/* Llib/object.scm 250 */
					obj_t BgL_v1339z00_1970;

					BgL_v1339z00_1970 = create_vector(0L);
					BgL_arg1947z00_1942 = BgL_v1339z00_1970;
				}
				BGl_z62indexzd2outzd2ofzd2boundszd2errorz62zz__objectz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol3027z00zz__objectz00, BGl_symbol2967z00zz__objectz00,
					BGl_z62errorz62zz__objectz00, 17130L, BGl_proc3026z00zz__objectz00,
					BGl_proc3025z00zz__objectz00, BFALSE, BGl_proc3024z00zz__objectz00,
					BFALSE, BgL_arg1946z00_1941, BgL_arg1947z00_1942);
			}
			{	/* Llib/object.scm 252 */
				obj_t BgL_arg1961z00_1977;
				obj_t BgL_arg1962z00_1978;

				{	/* Llib/object.scm 252 */
					obj_t BgL_v1340z00_1994;

					BgL_v1340z00_1994 = create_vector(0L);
					BgL_arg1961z00_1977 = BgL_v1340z00_1994;
				}
				{	/* Llib/object.scm 252 */
					obj_t BgL_v1341z00_1995;

					BgL_v1341z00_1995 = create_vector(0L);
					BgL_arg1962z00_1978 = BgL_v1341z00_1995;
				}
				BGl_z62iozd2errorzb0zz__objectz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol3032z00zz__objectz00, BGl_symbol2967z00zz__objectz00,
					BGl_z62errorz62zz__objectz00, 63818L, BGl_proc3031z00zz__objectz00,
					BGl_proc3030z00zz__objectz00, BFALSE, BGl_proc3029z00zz__objectz00,
					BFALSE, BgL_arg1961z00_1977, BgL_arg1962z00_1978);
			}
			{	/* Llib/object.scm 253 */
				obj_t BgL_arg1971z00_2002;
				obj_t BgL_arg1972z00_2003;

				{	/* Llib/object.scm 253 */
					obj_t BgL_v1342z00_2019;

					BgL_v1342z00_2019 = create_vector(0L);
					BgL_arg1971z00_2002 = BgL_v1342z00_2019;
				}
				{	/* Llib/object.scm 253 */
					obj_t BgL_v1343z00_2020;

					BgL_v1343z00_2020 = create_vector(0L);
					BgL_arg1972z00_2003 = BgL_v1343z00_2020;
				}
				BGl_z62iozd2portzd2errorz62zz__objectz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol3037z00zz__objectz00, BGl_symbol2967z00zz__objectz00,
					BGl_z62iozd2errorzb0zz__objectz00, 55779L,
					BGl_proc3036z00zz__objectz00, BGl_proc3035z00zz__objectz00, BFALSE,
					BGl_proc3034z00zz__objectz00, BFALSE, BgL_arg1971z00_2002,
					BgL_arg1972z00_2003);
			}
			{	/* Llib/object.scm 254 */
				obj_t BgL_arg1981z00_2027;
				obj_t BgL_arg1982z00_2028;

				{	/* Llib/object.scm 254 */
					obj_t BgL_v1344z00_2044;

					BgL_v1344z00_2044 = create_vector(0L);
					BgL_arg1981z00_2027 = BgL_v1344z00_2044;
				}
				{	/* Llib/object.scm 254 */
					obj_t BgL_v1345z00_2045;

					BgL_v1345z00_2045 = create_vector(0L);
					BgL_arg1982z00_2028 = BgL_v1345z00_2045;
				}
				BGl_z62iozd2readzd2errorz62zz__objectz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol3042z00zz__objectz00, BGl_symbol2967z00zz__objectz00,
					BGl_z62iozd2portzd2errorz62zz__objectz00, 15626L,
					BGl_proc3041z00zz__objectz00, BGl_proc3040z00zz__objectz00, BFALSE,
					BGl_proc3039z00zz__objectz00, BFALSE, BgL_arg1981z00_2027,
					BgL_arg1982z00_2028);
			}
			{	/* Llib/object.scm 255 */
				obj_t BgL_arg1991z00_2052;
				obj_t BgL_arg1992z00_2053;

				{	/* Llib/object.scm 255 */
					obj_t BgL_v1346z00_2069;

					BgL_v1346z00_2069 = create_vector(0L);
					BgL_arg1991z00_2052 = BgL_v1346z00_2069;
				}
				{	/* Llib/object.scm 255 */
					obj_t BgL_v1347z00_2070;

					BgL_v1347z00_2070 = create_vector(0L);
					BgL_arg1992z00_2053 = BgL_v1347z00_2070;
				}
				BGl_z62iozd2writezd2errorz62zz__objectz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol3047z00zz__objectz00, BGl_symbol2967z00zz__objectz00,
					BGl_z62iozd2portzd2errorz62zz__objectz00, 35931L,
					BGl_proc3046z00zz__objectz00, BGl_proc3045z00zz__objectz00, BFALSE,
					BGl_proc3044z00zz__objectz00, BFALSE, BgL_arg1991z00_2052,
					BgL_arg1992z00_2053);
			}
			{	/* Llib/object.scm 256 */
				obj_t BgL_arg2001z00_2077;
				obj_t BgL_arg2002z00_2078;

				{	/* Llib/object.scm 256 */
					obj_t BgL_v1348z00_2094;

					BgL_v1348z00_2094 = create_vector(0L);
					BgL_arg2001z00_2077 = BgL_v1348z00_2094;
				}
				{	/* Llib/object.scm 256 */
					obj_t BgL_v1349z00_2095;

					BgL_v1349z00_2095 = create_vector(0L);
					BgL_arg2002z00_2078 = BgL_v1349z00_2095;
				}
				BGl_z62iozd2closedzd2errorz62zz__objectz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol3052z00zz__objectz00, BGl_symbol2967z00zz__objectz00,
					BGl_z62iozd2portzd2errorz62zz__objectz00, 8168L,
					BGl_proc3051z00zz__objectz00, BGl_proc3050z00zz__objectz00, BFALSE,
					BGl_proc3049z00zz__objectz00, BFALSE, BgL_arg2001z00_2077,
					BgL_arg2002z00_2078);
			}
			{	/* Llib/object.scm 257 */
				obj_t BgL_arg2011z00_2102;
				obj_t BgL_arg2012z00_2103;

				{	/* Llib/object.scm 257 */
					obj_t BgL_v1350z00_2119;

					BgL_v1350z00_2119 = create_vector(0L);
					BgL_arg2011z00_2102 = BgL_v1350z00_2119;
				}
				{	/* Llib/object.scm 257 */
					obj_t BgL_v1351z00_2120;

					BgL_v1351z00_2120 = create_vector(0L);
					BgL_arg2012z00_2103 = BgL_v1351z00_2120;
				}
				BGl_z62iozd2filezd2notzd2foundzd2errorz62zz__objectz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol3057z00zz__objectz00, BGl_symbol2967z00zz__objectz00,
					BGl_z62iozd2errorzb0zz__objectz00, 9101L,
					BGl_proc3056z00zz__objectz00, BGl_proc3055z00zz__objectz00, BFALSE,
					BGl_proc3054z00zz__objectz00, BFALSE, BgL_arg2011z00_2102,
					BgL_arg2012z00_2103);
			}
			{	/* Llib/object.scm 258 */
				obj_t BgL_arg2021z00_2127;
				obj_t BgL_arg2022z00_2128;

				{	/* Llib/object.scm 258 */
					obj_t BgL_v1352z00_2144;

					BgL_v1352z00_2144 = create_vector(0L);
					BgL_arg2021z00_2127 = BgL_v1352z00_2144;
				}
				{	/* Llib/object.scm 258 */
					obj_t BgL_v1353z00_2145;

					BgL_v1353z00_2145 = create_vector(0L);
					BgL_arg2022z00_2128 = BgL_v1353z00_2145;
				}
				BGl_z62iozd2parsezd2errorz62zz__objectz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol3062z00zz__objectz00, BGl_symbol2967z00zz__objectz00,
					BGl_z62iozd2errorzb0zz__objectz00, 21913L,
					BGl_proc3061z00zz__objectz00, BGl_proc3060z00zz__objectz00, BFALSE,
					BGl_proc3059z00zz__objectz00, BFALSE, BgL_arg2021z00_2127,
					BgL_arg2022z00_2128);
			}
			{	/* Llib/object.scm 259 */
				obj_t BgL_arg2033z00_2152;
				obj_t BgL_arg2034z00_2153;

				{	/* Llib/object.scm 259 */
					obj_t BgL_v1354z00_2169;

					BgL_v1354z00_2169 = create_vector(0L);
					BgL_arg2033z00_2152 = BgL_v1354z00_2169;
				}
				{	/* Llib/object.scm 259 */
					obj_t BgL_v1355z00_2170;

					BgL_v1355z00_2170 = create_vector(0L);
					BgL_arg2034z00_2153 = BgL_v1355z00_2170;
				}
				BGl_z62iozd2unknownzd2hostzd2errorzb0zz__objectz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol3067z00zz__objectz00, BGl_symbol2967z00zz__objectz00,
					BGl_z62iozd2errorzb0zz__objectz00, 40825L,
					BGl_proc3066z00zz__objectz00, BGl_proc3065z00zz__objectz00, BFALSE,
					BGl_proc3064z00zz__objectz00, BFALSE, BgL_arg2033z00_2152,
					BgL_arg2034z00_2153);
			}
			{	/* Llib/object.scm 260 */
				obj_t BgL_arg2044z00_2177;
				obj_t BgL_arg2045z00_2178;

				{	/* Llib/object.scm 260 */
					obj_t BgL_v1356z00_2194;

					BgL_v1356z00_2194 = create_vector(0L);
					BgL_arg2044z00_2177 = BgL_v1356z00_2194;
				}
				{	/* Llib/object.scm 260 */
					obj_t BgL_v1357z00_2195;

					BgL_v1357z00_2195 = create_vector(0L);
					BgL_arg2045z00_2178 = BgL_v1357z00_2195;
				}
				BGl_z62iozd2malformedzd2urlzd2errorzb0zz__objectz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol3072z00zz__objectz00, BGl_symbol2967z00zz__objectz00,
					BGl_z62iozd2errorzb0zz__objectz00, 43421L,
					BGl_proc3071z00zz__objectz00, BGl_proc3070z00zz__objectz00, BFALSE,
					BGl_proc3069z00zz__objectz00, BFALSE, BgL_arg2044z00_2177,
					BgL_arg2045z00_2178);
			}
			{	/* Llib/object.scm 261 */
				obj_t BgL_arg2056z00_2202;
				obj_t BgL_arg2057z00_2203;

				{	/* Llib/object.scm 261 */
					obj_t BgL_v1358z00_2219;

					BgL_v1358z00_2219 = create_vector(0L);
					BgL_arg2056z00_2202 = BgL_v1358z00_2219;
				}
				{	/* Llib/object.scm 261 */
					obj_t BgL_v1359z00_2220;

					BgL_v1359z00_2220 = create_vector(0L);
					BgL_arg2057z00_2203 = BgL_v1359z00_2220;
				}
				BGl_z62iozd2sigpipezd2errorz62zz__objectz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol3077z00zz__objectz00, BGl_symbol2967z00zz__objectz00,
					BGl_z62iozd2errorzb0zz__objectz00, 38807L,
					BGl_proc3076z00zz__objectz00, BGl_proc3075z00zz__objectz00, BFALSE,
					BGl_proc3074z00zz__objectz00, BFALSE, BgL_arg2056z00_2202,
					BgL_arg2057z00_2203);
			}
			{	/* Llib/object.scm 262 */
				obj_t BgL_arg2067z00_2227;
				obj_t BgL_arg2068z00_2228;

				{	/* Llib/object.scm 262 */
					obj_t BgL_v1360z00_2244;

					BgL_v1360z00_2244 = create_vector(0L);
					BgL_arg2067z00_2227 = BgL_v1360z00_2244;
				}
				{	/* Llib/object.scm 262 */
					obj_t BgL_v1361z00_2245;

					BgL_v1361z00_2245 = create_vector(0L);
					BgL_arg2068z00_2228 = BgL_v1361z00_2245;
				}
				BGl_z62iozd2timeoutzd2errorz62zz__objectz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol3082z00zz__objectz00, BGl_symbol2967z00zz__objectz00,
					BGl_z62iozd2errorzb0zz__objectz00, 13L, BGl_proc3081z00zz__objectz00,
					BGl_proc3080z00zz__objectz00, BFALSE, BGl_proc3079z00zz__objectz00,
					BFALSE, BgL_arg2067z00_2227, BgL_arg2068z00_2228);
			}
			{	/* Llib/object.scm 263 */
				obj_t BgL_arg2077z00_2252;
				obj_t BgL_arg2078z00_2253;

				{	/* Llib/object.scm 263 */
					obj_t BgL_v1362z00_2269;

					BgL_v1362z00_2269 = create_vector(0L);
					BgL_arg2077z00_2252 = BgL_v1362z00_2269;
				}
				{	/* Llib/object.scm 263 */
					obj_t BgL_v1363z00_2270;

					BgL_v1363z00_2270 = create_vector(0L);
					BgL_arg2078z00_2253 = BgL_v1363z00_2270;
				}
				BGl_z62iozd2connectionzd2errorz62zz__objectz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol3087z00zz__objectz00, BGl_symbol2967z00zz__objectz00,
					BGl_z62iozd2errorzb0zz__objectz00, 37044L,
					BGl_proc3086z00zz__objectz00, BGl_proc3085z00zz__objectz00, BFALSE,
					BGl_proc3084z00zz__objectz00, BFALSE, BgL_arg2077z00_2252,
					BgL_arg2078z00_2253);
			}
			{	/* Llib/object.scm 265 */
				obj_t BgL_arg2088z00_2277;
				obj_t BgL_arg2089z00_2278;

				{	/* Llib/object.scm 265 */
					obj_t BgL_v1364z00_2294;

					BgL_v1364z00_2294 = create_vector(0L);
					BgL_arg2088z00_2277 = BgL_v1364z00_2294;
				}
				{	/* Llib/object.scm 265 */
					obj_t BgL_v1365z00_2295;

					BgL_v1365z00_2295 = create_vector(0L);
					BgL_arg2089z00_2278 = BgL_v1365z00_2295;
				}
				BGl_z62processzd2exceptionzb0zz__objectz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol3092z00zz__objectz00, BGl_symbol2967z00zz__objectz00,
					BGl_z62errorz62zz__objectz00, 8734L, BGl_proc3091z00zz__objectz00,
					BGl_proc3090z00zz__objectz00, BFALSE, BGl_proc3089z00zz__objectz00,
					BFALSE, BgL_arg2088z00_2277, BgL_arg2089z00_2278);
			}
			{	/* Llib/object.scm 267 */
				obj_t BgL_arg2098z00_2302;
				obj_t BgL_arg2099z00_2303;

				{	/* Llib/object.scm 267 */
					obj_t BgL_v1366z00_2319;

					BgL_v1366z00_2319 = create_vector(0L);
					BgL_arg2098z00_2302 = BgL_v1366z00_2319;
				}
				{	/* Llib/object.scm 267 */
					obj_t BgL_v1367z00_2320;

					BgL_v1367z00_2320 = create_vector(0L);
					BgL_arg2099z00_2303 = BgL_v1367z00_2320;
				}
				BGl_z62stackzd2overflowzd2errorz62zz__objectz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol3097z00zz__objectz00, BGl_symbol2967z00zz__objectz00,
					BGl_z62errorz62zz__objectz00, 16267L, BGl_proc3096z00zz__objectz00,
					BGl_proc3095z00zz__objectz00, BFALSE, BGl_proc3094z00zz__objectz00,
					BFALSE, BgL_arg2098z00_2302, BgL_arg2099z00_2303);
			}
			{	/* Llib/object.scm 269 */
				obj_t BgL_arg2108z00_2327;
				obj_t BgL_arg2109z00_2328;

				{	/* Llib/object.scm 269 */
					obj_t BgL_v1368z00_2342;

					BgL_v1368z00_2342 = create_vector(1L);
					{	/* Llib/object.scm 269 */
						obj_t BgL_arg2115z00_2343;

						{	/* Llib/object.scm 269 */
							obj_t BgL_res2610z00_4299;

							{	/* Llib/object.scm 269 */
								obj_t BgL_namez00_4284;
								obj_t BgL_typez00_4287;

								BgL_namez00_4284 = BGl_symbol3102z00zz__objectz00;
								BgL_typez00_4287 = BGl_symbol3104z00zz__objectz00;
								{	/* Llib/object.scm 591 */
									obj_t BgL_v1316z00_4288;

									BgL_v1316z00_4288 = create_vector(9L);
									VECTOR_SET(BgL_v1316z00_4288, 0L, BgL_namez00_4284);
									VECTOR_SET(BgL_v1316z00_4288, 1L,
										BGl_proc3101z00zz__objectz00);
									VECTOR_SET(BgL_v1316z00_4288, 2L,
										BGl_proc3100z00zz__objectz00);
									VECTOR_SET(BgL_v1316z00_4288, 3L, BFALSE);
									VECTOR_SET(BgL_v1316z00_4288, 4L,
										BGl_makezd2classzd2fieldzd2envzd2zz__objectz00);
									VECTOR_SET(BgL_v1316z00_4288, 5L, BFALSE);
									VECTOR_SET(BgL_v1316z00_4288, 6L,
										BGl_proc3099z00zz__objectz00);
									VECTOR_SET(BgL_v1316z00_4288, 7L, BgL_typez00_4287);
									VECTOR_SET(BgL_v1316z00_4288, 8L, BFALSE);
									BgL_res2610z00_4299 = BgL_v1316z00_4288;
							}}
							BgL_arg2115z00_2343 = BgL_res2610z00_4299;
						}
						VECTOR_SET(BgL_v1368z00_2342, 0L, BgL_arg2115z00_2343);
					}
					BgL_arg2108z00_2327 = BgL_v1368z00_2342;
				}
				{	/* Llib/object.scm 269 */
					obj_t BgL_v1369z00_2356;

					BgL_v1369z00_2356 = create_vector(0L);
					BgL_arg2109z00_2328 = BgL_v1369z00_2356;
				}
				BGl_z62securityzd2exceptionzb0zz__objectz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol3109z00zz__objectz00, BGl_symbol2967z00zz__objectz00,
					BGl_z62exceptionz62zz__objectz00, 17339L,
					BGl_proc3108z00zz__objectz00, BGl_proc3107z00zz__objectz00, BFALSE,
					BGl_proc3106z00zz__objectz00, BFALSE, BgL_arg2108z00_2327,
					BgL_arg2109z00_2328);
			}
			{	/* Llib/object.scm 271 */
				obj_t BgL_arg2125z00_2363;
				obj_t BgL_arg2126z00_2364;

				{	/* Llib/object.scm 271 */
					obj_t BgL_v1370z00_2380;

					BgL_v1370z00_2380 = create_vector(2L);
					{	/* Llib/object.scm 271 */
						obj_t BgL_arg2133z00_2381;

						{	/* Llib/object.scm 271 */
							obj_t BgL_res2611z00_4327;

							{	/* Llib/object.scm 271 */
								obj_t BgL_namez00_4312;
								obj_t BgL_typez00_4315;

								BgL_namez00_4312 = BGl_symbol2979z00zz__objectz00;
								BgL_typez00_4315 = BGl_symbol2979z00zz__objectz00;
								{	/* Llib/object.scm 591 */
									obj_t BgL_v1316z00_4316;

									BgL_v1316z00_4316 = create_vector(9L);
									VECTOR_SET(BgL_v1316z00_4316, 0L, BgL_namez00_4312);
									VECTOR_SET(BgL_v1316z00_4316, 1L,
										BGl_proc3113z00zz__objectz00);
									VECTOR_SET(BgL_v1316z00_4316, 2L,
										BGl_proc3112z00zz__objectz00);
									VECTOR_SET(BgL_v1316z00_4316, 3L, BFALSE);
									VECTOR_SET(BgL_v1316z00_4316, 4L,
										BGl_makezd2classzd2fieldzd2envzd2zz__objectz00);
									VECTOR_SET(BgL_v1316z00_4316, 5L, BFALSE);
									VECTOR_SET(BgL_v1316z00_4316, 6L,
										BGl_proc3111z00zz__objectz00);
									VECTOR_SET(BgL_v1316z00_4316, 7L, BgL_typez00_4315);
									VECTOR_SET(BgL_v1316z00_4316, 8L, BFALSE);
									BgL_res2611z00_4327 = BgL_v1316z00_4316;
							}}
							BgL_arg2133z00_2381 = BgL_res2611z00_4327;
						}
						VECTOR_SET(BgL_v1370z00_2380, 0L, BgL_arg2133z00_2381);
					}
					{	/* Llib/object.scm 271 */
						obj_t BgL_arg2141z00_2394;

						{	/* Llib/object.scm 271 */
							obj_t BgL_res2612z00_4344;

							{	/* Llib/object.scm 271 */
								obj_t BgL_namez00_4329;
								obj_t BgL_typez00_4332;

								BgL_namez00_4329 = BGl_symbol3117z00zz__objectz00;
								BgL_typez00_4332 = BGl_symbol2979z00zz__objectz00;
								{	/* Llib/object.scm 591 */
									obj_t BgL_v1316z00_4333;

									BgL_v1316z00_4333 = create_vector(9L);
									VECTOR_SET(BgL_v1316z00_4333, 0L, BgL_namez00_4329);
									VECTOR_SET(BgL_v1316z00_4333, 1L,
										BGl_proc3116z00zz__objectz00);
									VECTOR_SET(BgL_v1316z00_4333, 2L,
										BGl_proc3115z00zz__objectz00);
									VECTOR_SET(BgL_v1316z00_4333, 3L, BFALSE);
									VECTOR_SET(BgL_v1316z00_4333, 4L,
										BGl_makezd2classzd2fieldzd2envzd2zz__objectz00);
									VECTOR_SET(BgL_v1316z00_4333, 5L, BFALSE);
									VECTOR_SET(BgL_v1316z00_4333, 6L,
										BGl_proc3114z00zz__objectz00);
									VECTOR_SET(BgL_v1316z00_4333, 7L, BgL_typez00_4332);
									VECTOR_SET(BgL_v1316z00_4333, 8L, BFALSE);
									BgL_res2612z00_4344 = BgL_v1316z00_4333;
							}}
							BgL_arg2141z00_2394 = BgL_res2612z00_4344;
						}
						VECTOR_SET(BgL_v1370z00_2380, 1L, BgL_arg2141z00_2394);
					}
					BgL_arg2125z00_2363 = BgL_v1370z00_2380;
				}
				{	/* Llib/object.scm 271 */
					obj_t BgL_v1371z00_2407;

					BgL_v1371z00_2407 = create_vector(0L);
					BgL_arg2126z00_2364 = BgL_v1371z00_2407;
				}
				BGl_z62accesszd2controlzd2exceptionz62zz__objectz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol3122z00zz__objectz00, BGl_symbol2967z00zz__objectz00,
					BGl_z62securityzd2exceptionzb0zz__objectz00, 64045L,
					BGl_proc3121z00zz__objectz00, BGl_proc3120z00zz__objectz00, BFALSE,
					BGl_proc3119z00zz__objectz00, BFALSE, BgL_arg2125z00_2363,
					BgL_arg2126z00_2364);
			}
			{	/* Llib/object.scm 275 */
				obj_t BgL_arg2151z00_2414;
				obj_t BgL_arg2152z00_2415;

				{	/* Llib/object.scm 275 */
					obj_t BgL_v1372z00_2429;

					BgL_v1372z00_2429 = create_vector(1L);
					{	/* Llib/object.scm 275 */
						obj_t BgL_arg2158z00_2430;

						{	/* Llib/object.scm 275 */
							obj_t BgL_res2613z00_4372;

							{	/* Llib/object.scm 275 */
								obj_t BgL_namez00_4357;
								obj_t BgL_typez00_4360;

								BgL_namez00_4357 = BGl_symbol3126z00zz__objectz00;
								BgL_typez00_4360 = BGl_symbol2979z00zz__objectz00;
								{	/* Llib/object.scm 591 */
									obj_t BgL_v1316z00_4361;

									BgL_v1316z00_4361 = create_vector(9L);
									VECTOR_SET(BgL_v1316z00_4361, 0L, BgL_namez00_4357);
									VECTOR_SET(BgL_v1316z00_4361, 1L,
										BGl_proc3125z00zz__objectz00);
									VECTOR_SET(BgL_v1316z00_4361, 2L,
										BGl_proc3124z00zz__objectz00);
									VECTOR_SET(BgL_v1316z00_4361, 3L, BFALSE);
									VECTOR_SET(BgL_v1316z00_4361, 4L,
										BGl_makezd2classzd2fieldzd2envzd2zz__objectz00);
									VECTOR_SET(BgL_v1316z00_4361, 5L, BFALSE);
									VECTOR_SET(BgL_v1316z00_4361, 6L, BFALSE);
									VECTOR_SET(BgL_v1316z00_4361, 7L, BgL_typez00_4360);
									VECTOR_SET(BgL_v1316z00_4361, 8L, BFALSE);
									BgL_res2613z00_4372 = BgL_v1316z00_4361;
							}}
							BgL_arg2158z00_2430 = BgL_res2613z00_4372;
						}
						VECTOR_SET(BgL_v1372z00_2429, 0L, BgL_arg2158z00_2430);
					}
					BgL_arg2151z00_2414 = BgL_v1372z00_2429;
				}
				{	/* Llib/object.scm 275 */
					obj_t BgL_v1373z00_2440;

					BgL_v1373z00_2440 = create_vector(0L);
					BgL_arg2152z00_2415 = BgL_v1373z00_2440;
				}
				BGl_z62warningz62zz__objectz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol3131z00zz__objectz00, BGl_symbol2967z00zz__objectz00,
					BGl_z62exceptionz62zz__objectz00, 33714L,
					BGl_proc3130z00zz__objectz00, BGl_proc3129z00zz__objectz00, BFALSE,
					BGl_proc3128z00zz__objectz00, BFALSE, BgL_arg2151z00_2414,
					BgL_arg2152z00_2415);
			}
			{	/* Llib/object.scm 277 */
				obj_t BgL_arg2166z00_2447;
				obj_t BgL_arg2167z00_2448;

				{	/* Llib/object.scm 277 */
					obj_t BgL_v1374z00_2462;

					BgL_v1374z00_2462 = create_vector(0L);
					BgL_arg2166z00_2447 = BgL_v1374z00_2462;
				}
				{	/* Llib/object.scm 277 */
					obj_t BgL_v1375z00_2463;

					BgL_v1375z00_2463 = create_vector(0L);
					BgL_arg2167z00_2448 = BgL_v1375z00_2463;
				}
				return (BGl_z62evalzd2warningzb0zz__objectz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol3136z00zz__objectz00, BGl_symbol2967z00zz__objectz00,
						BGl_z62warningz62zz__objectz00, 60850L,
						BGl_proc3135z00zz__objectz00, BGl_proc3134z00zz__objectz00, BFALSE,
						BGl_proc3133z00zz__objectz00, BFALSE, BgL_arg2166z00_2447,
						BgL_arg2167z00_2448), BUNSPEC);
			}
		}

	}



/* &<@anonymous:2172> */
	obj_t BGl_z62zc3z04anonymousza32172ze3ze5zz__objectz00(obj_t BgL_envz00_5340,
		obj_t BgL_new1209z00_5341)
	{
		{	/* Llib/object.scm 277 */
			{
				BgL_z62evalzd2warningzb0_bglt BgL_auxz00_8700;

				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt)
									((BgL_z62evalzd2warningzb0_bglt) BgL_new1209z00_5341))))->
						BgL_fnamez00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) (
										(BgL_z62evalzd2warningzb0_bglt) BgL_new1209z00_5341))))->
						BgL_locationz00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) (
										(BgL_z62evalzd2warningzb0_bglt) BgL_new1209z00_5341))))->
						BgL_stackz00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62warningz62_bglt)
							COBJECT(((BgL_z62warningz62_bglt) ((BgL_z62evalzd2warningzb0_bglt)
										BgL_new1209z00_5341))))->BgL_argsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_8700 = ((BgL_z62evalzd2warningzb0_bglt) BgL_new1209z00_5341);
				return ((obj_t) BgL_auxz00_8700);
			}
		}

	}



/* &lambda2170 */
	BgL_z62evalzd2warningzb0_bglt BGl_z62lambda2170z62zz__objectz00(obj_t
		BgL_envz00_5342)
	{
		{	/* Llib/object.scm 277 */
			{	/* Llib/object.scm 277 */
				BgL_z62evalzd2warningzb0_bglt BgL_new1208z00_6053;

				BgL_new1208z00_6053 =
					((BgL_z62evalzd2warningzb0_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_z62evalzd2warningzb0_bgl))));
				{	/* Llib/object.scm 277 */
					long BgL_arg2171z00_6054;

					BgL_arg2171z00_6054 =
						BGL_CLASS_NUM(BGl_z62evalzd2warningzb0zz__objectz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1208z00_6053), BgL_arg2171z00_6054);
				}
				return BgL_new1208z00_6053;
			}
		}

	}



/* &lambda2168 */
	BgL_z62evalzd2warningzb0_bglt BGl_z62lambda2168z62zz__objectz00(obj_t
		BgL_envz00_5343, obj_t BgL_fname1204z00_5344,
		obj_t BgL_location1205z00_5345, obj_t BgL_stack1206z00_5346,
		obj_t BgL_args1207z00_5347)
	{
		{	/* Llib/object.scm 277 */
			{	/* Llib/object.scm 277 */
				BgL_z62evalzd2warningzb0_bglt BgL_new1277z00_6055;

				{	/* Llib/object.scm 277 */
					BgL_z62evalzd2warningzb0_bglt BgL_new1276z00_6056;

					BgL_new1276z00_6056 =
						((BgL_z62evalzd2warningzb0_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_z62evalzd2warningzb0_bgl))));
					{	/* Llib/object.scm 277 */
						long BgL_arg2169z00_6057;

						BgL_arg2169z00_6057 =
							BGL_CLASS_NUM(BGl_z62evalzd2warningzb0zz__objectz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1276z00_6056), BgL_arg2169z00_6057);
					}
					BgL_new1277z00_6055 = BgL_new1276z00_6056;
				}
				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt) BgL_new1277z00_6055)))->
						BgL_fnamez00) = ((obj_t) BgL_fname1204z00_5344), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1277z00_6055)))->BgL_locationz00) =
					((obj_t) BgL_location1205z00_5345), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1277z00_6055)))->BgL_stackz00) =
					((obj_t) BgL_stack1206z00_5346), BUNSPEC);
				((((BgL_z62warningz62_bglt) COBJECT(((BgL_z62warningz62_bglt)
									BgL_new1277z00_6055)))->BgL_argsz00) =
					((obj_t) BgL_args1207z00_5347), BUNSPEC);
				return BgL_new1277z00_6055;
			}
		}

	}



/* &<@anonymous:2157> */
	obj_t BGl_z62zc3z04anonymousza32157ze3ze5zz__objectz00(obj_t BgL_envz00_5348,
		obj_t BgL_new1202z00_5349)
	{
		{	/* Llib/object.scm 275 */
			{
				BgL_z62warningz62_bglt BgL_auxz00_8731;

				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt)
									((BgL_z62warningz62_bglt) BgL_new1202z00_5349))))->
						BgL_fnamez00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) ((BgL_z62warningz62_bglt)
										BgL_new1202z00_5349))))->BgL_locationz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) ((BgL_z62warningz62_bglt)
										BgL_new1202z00_5349))))->BgL_stackz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62warningz62_bglt) COBJECT(((BgL_z62warningz62_bglt)
									BgL_new1202z00_5349)))->BgL_argsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_8731 = ((BgL_z62warningz62_bglt) BgL_new1202z00_5349);
				return ((obj_t) BgL_auxz00_8731);
			}
		}

	}



/* &lambda2155 */
	BgL_z62warningz62_bglt BGl_z62lambda2155z62zz__objectz00(obj_t
		BgL_envz00_5350)
	{
		{	/* Llib/object.scm 275 */
			{	/* Llib/object.scm 275 */
				BgL_z62warningz62_bglt BgL_new1201z00_6059;

				BgL_new1201z00_6059 =
					((BgL_z62warningz62_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_z62warningz62_bgl))));
				{	/* Llib/object.scm 275 */
					long BgL_arg2156z00_6060;

					BgL_arg2156z00_6060 = BGL_CLASS_NUM(BGl_z62warningz62zz__objectz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1201z00_6059), BgL_arg2156z00_6060);
				}
				return BgL_new1201z00_6059;
			}
		}

	}



/* &lambda2153 */
	BgL_z62warningz62_bglt BGl_z62lambda2153z62zz__objectz00(obj_t
		BgL_envz00_5351, obj_t BgL_fname1197z00_5352,
		obj_t BgL_location1198z00_5353, obj_t BgL_stack1199z00_5354,
		obj_t BgL_args1200z00_5355)
	{
		{	/* Llib/object.scm 275 */
			{	/* Llib/object.scm 275 */
				BgL_z62warningz62_bglt BgL_new1275z00_6061;

				{	/* Llib/object.scm 275 */
					BgL_z62warningz62_bglt BgL_new1274z00_6062;

					BgL_new1274z00_6062 =
						((BgL_z62warningz62_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_z62warningz62_bgl))));
					{	/* Llib/object.scm 275 */
						long BgL_arg2154z00_6063;

						BgL_arg2154z00_6063 = BGL_CLASS_NUM(BGl_z62warningz62zz__objectz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1274z00_6062), BgL_arg2154z00_6063);
					}
					BgL_new1275z00_6061 = BgL_new1274z00_6062;
				}
				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt) BgL_new1275z00_6061)))->
						BgL_fnamez00) = ((obj_t) BgL_fname1197z00_5352), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1275z00_6061)))->BgL_locationz00) =
					((obj_t) BgL_location1198z00_5353), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1275z00_6061)))->BgL_stackz00) =
					((obj_t) BgL_stack1199z00_5354), BUNSPEC);
				((((BgL_z62warningz62_bglt) COBJECT(BgL_new1275z00_6061))->
						BgL_argsz00) = ((obj_t) BgL_args1200z00_5355), BUNSPEC);
				return BgL_new1275z00_6061;
			}
		}

	}



/* &lambda2162 */
	obj_t BGl_z62lambda2162z62zz__objectz00(obj_t BgL_envz00_5356,
		obj_t BgL_oz00_5357, obj_t BgL_vz00_5358)
	{
		{	/* Llib/object.scm 275 */
			return
				((((BgL_z62warningz62_bglt) COBJECT(
							((BgL_z62warningz62_bglt) BgL_oz00_5357)))->BgL_argsz00) =
				((obj_t) BgL_vz00_5358), BUNSPEC);
		}

	}



/* &lambda2161 */
	obj_t BGl_z62lambda2161z62zz__objectz00(obj_t BgL_envz00_5359,
		obj_t BgL_oz00_5360)
	{
		{	/* Llib/object.scm 275 */
			return
				(((BgL_z62warningz62_bglt) COBJECT(
						((BgL_z62warningz62_bglt) BgL_oz00_5360)))->BgL_argsz00);
		}

	}



/* &<@anonymous:2132> */
	obj_t BGl_z62zc3z04anonymousza32132ze3ze5zz__objectz00(obj_t BgL_envz00_5361,
		obj_t BgL_new1195z00_5362)
	{
		{	/* Llib/object.scm 271 */
			{
				BgL_z62accesszd2controlzd2exceptionz62_bglt BgL_auxz00_8764;

				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt)
									((BgL_z62accesszd2controlzd2exceptionz62_bglt)
										BgL_new1195z00_5362))))->BgL_fnamez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) (
										(BgL_z62accesszd2controlzd2exceptionz62_bglt)
										BgL_new1195z00_5362))))->BgL_locationz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) (
										(BgL_z62accesszd2controlzd2exceptionz62_bglt)
										BgL_new1195z00_5362))))->BgL_stackz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62securityzd2exceptionzb0_bglt)
							COBJECT(((BgL_z62securityzd2exceptionzb0_bglt) (
										(BgL_z62accesszd2controlzd2exceptionz62_bglt)
										BgL_new1195z00_5362))))->BgL_messagez00) =
					((obj_t) BGl_string2936z00zz__objectz00), BUNSPEC);
				((((BgL_z62accesszd2controlzd2exceptionz62_bglt)
							COBJECT(((BgL_z62accesszd2controlzd2exceptionz62_bglt)
									BgL_new1195z00_5362)))->BgL_objz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62accesszd2controlzd2exceptionz62_bglt)
							COBJECT(((BgL_z62accesszd2controlzd2exceptionz62_bglt)
									BgL_new1195z00_5362)))->BgL_permissionz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_8764 =
					((BgL_z62accesszd2controlzd2exceptionz62_bglt) BgL_new1195z00_5362);
				return ((obj_t) BgL_auxz00_8764);
			}
		}

	}



/* &lambda2130 */
	BgL_z62accesszd2controlzd2exceptionz62_bglt
		BGl_z62lambda2130z62zz__objectz00(obj_t BgL_envz00_5363)
	{
		{	/* Llib/object.scm 271 */
			{	/* Llib/object.scm 271 */
				BgL_z62accesszd2controlzd2exceptionz62_bglt BgL_new1194z00_6067;

				BgL_new1194z00_6067 =
					((BgL_z62accesszd2controlzd2exceptionz62_bglt)
					BOBJECT(GC_MALLOC(sizeof(struct
								BgL_z62accesszd2controlzd2exceptionz62_bgl))));
				{	/* Llib/object.scm 271 */
					long BgL_arg2131z00_6068;

					BgL_arg2131z00_6068 =
						BGL_CLASS_NUM(BGl_z62accesszd2controlzd2exceptionz62zz__objectz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1194z00_6067), BgL_arg2131z00_6068);
				}
				return BgL_new1194z00_6067;
			}
		}

	}



/* &lambda2127 */
	BgL_z62accesszd2controlzd2exceptionz62_bglt
		BGl_z62lambda2127z62zz__objectz00(obj_t BgL_envz00_5364,
		obj_t BgL_fname1188z00_5365, obj_t BgL_location1189z00_5366,
		obj_t BgL_stack1190z00_5367, obj_t BgL_message1191z00_5368,
		obj_t BgL_obj1192z00_5369, obj_t BgL_permission1193z00_5370)
	{
		{	/* Llib/object.scm 271 */
			{	/* Llib/object.scm 271 */
				BgL_z62accesszd2controlzd2exceptionz62_bglt BgL_new1273z00_6070;

				{	/* Llib/object.scm 271 */
					BgL_z62accesszd2controlzd2exceptionz62_bglt BgL_new1272z00_6071;

					BgL_new1272z00_6071 =
						((BgL_z62accesszd2controlzd2exceptionz62_bglt)
						BOBJECT(GC_MALLOC(sizeof(struct
									BgL_z62accesszd2controlzd2exceptionz62_bgl))));
					{	/* Llib/object.scm 271 */
						long BgL_arg2129z00_6072;

						BgL_arg2129z00_6072 =
							BGL_CLASS_NUM
							(BGl_z62accesszd2controlzd2exceptionz62zz__objectz00);
						BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) BgL_new1272z00_6071),
							BgL_arg2129z00_6072);
					}
					BgL_new1273z00_6070 = BgL_new1272z00_6071;
				}
				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt) BgL_new1273z00_6070)))->
						BgL_fnamez00) = ((obj_t) BgL_fname1188z00_5365), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1273z00_6070)))->BgL_locationz00) =
					((obj_t) BgL_location1189z00_5366), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1273z00_6070)))->BgL_stackz00) =
					((obj_t) BgL_stack1190z00_5367), BUNSPEC);
				((((BgL_z62securityzd2exceptionzb0_bglt)
							COBJECT(((BgL_z62securityzd2exceptionzb0_bglt)
									BgL_new1273z00_6070)))->BgL_messagez00) =
					((obj_t) ((obj_t) BgL_message1191z00_5368)), BUNSPEC);
				((((BgL_z62accesszd2controlzd2exceptionz62_bglt)
							COBJECT(BgL_new1273z00_6070))->BgL_objz00) =
					((obj_t) BgL_obj1192z00_5369), BUNSPEC);
				((((BgL_z62accesszd2controlzd2exceptionz62_bglt)
							COBJECT(BgL_new1273z00_6070))->BgL_permissionz00) =
					((obj_t) BgL_permission1193z00_5370), BUNSPEC);
				return BgL_new1273z00_6070;
			}
		}

	}



/* &<@anonymous:2147> */
	obj_t BGl_z62zc3z04anonymousza32147ze3ze5zz__objectz00(obj_t BgL_envz00_5371)
	{
		{	/* Llib/object.scm 271 */
			return BUNSPEC;
		}

	}



/* &lambda2146 */
	obj_t BGl_z62lambda2146z62zz__objectz00(obj_t BgL_envz00_5372,
		obj_t BgL_oz00_5373, obj_t BgL_vz00_5374)
	{
		{	/* Llib/object.scm 271 */
			return
				((((BgL_z62accesszd2controlzd2exceptionz62_bglt) COBJECT(
							((BgL_z62accesszd2controlzd2exceptionz62_bglt) BgL_oz00_5373)))->
					BgL_permissionz00) = ((obj_t) BgL_vz00_5374), BUNSPEC);
		}

	}



/* &lambda2145 */
	obj_t BGl_z62lambda2145z62zz__objectz00(obj_t BgL_envz00_5375,
		obj_t BgL_oz00_5376)
	{
		{	/* Llib/object.scm 271 */
			return
				(((BgL_z62accesszd2controlzd2exceptionz62_bglt) COBJECT(
						((BgL_z62accesszd2controlzd2exceptionz62_bglt) BgL_oz00_5376)))->
				BgL_permissionz00);
		}

	}



/* &<@anonymous:2139> */
	obj_t BGl_z62zc3z04anonymousza32139ze3ze5zz__objectz00(obj_t BgL_envz00_5377)
	{
		{	/* Llib/object.scm 271 */
			return BUNSPEC;
		}

	}



/* &lambda2138 */
	obj_t BGl_z62lambda2138z62zz__objectz00(obj_t BgL_envz00_5378,
		obj_t BgL_oz00_5379, obj_t BgL_vz00_5380)
	{
		{	/* Llib/object.scm 271 */
			return
				((((BgL_z62accesszd2controlzd2exceptionz62_bglt) COBJECT(
							((BgL_z62accesszd2controlzd2exceptionz62_bglt) BgL_oz00_5379)))->
					BgL_objz00) = ((obj_t) BgL_vz00_5380), BUNSPEC);
		}

	}



/* &lambda2137 */
	obj_t BGl_z62lambda2137z62zz__objectz00(obj_t BgL_envz00_5381,
		obj_t BgL_oz00_5382)
	{
		{	/* Llib/object.scm 271 */
			return
				(((BgL_z62accesszd2controlzd2exceptionz62_bglt) COBJECT(
						((BgL_z62accesszd2controlzd2exceptionz62_bglt) BgL_oz00_5382)))->
				BgL_objz00);
		}

	}



/* &<@anonymous:2114> */
	obj_t BGl_z62zc3z04anonymousza32114ze3ze5zz__objectz00(obj_t BgL_envz00_5383,
		obj_t BgL_new1186z00_5384)
	{
		{	/* Llib/object.scm 269 */
			{
				BgL_z62securityzd2exceptionzb0_bglt BgL_auxz00_8810;

				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt)
									((BgL_z62securityzd2exceptionzb0_bglt)
										BgL_new1186z00_5384))))->BgL_fnamez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) (
										(BgL_z62securityzd2exceptionzb0_bglt)
										BgL_new1186z00_5384))))->BgL_locationz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) (
										(BgL_z62securityzd2exceptionzb0_bglt)
										BgL_new1186z00_5384))))->BgL_stackz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62securityzd2exceptionzb0_bglt)
							COBJECT(((BgL_z62securityzd2exceptionzb0_bglt)
									BgL_new1186z00_5384)))->BgL_messagez00) =
					((obj_t) BGl_string2936z00zz__objectz00), BUNSPEC);
				BgL_auxz00_8810 =
					((BgL_z62securityzd2exceptionzb0_bglt) BgL_new1186z00_5384);
				return ((obj_t) BgL_auxz00_8810);
			}
		}

	}



/* &lambda2112 */
	BgL_z62securityzd2exceptionzb0_bglt BGl_z62lambda2112z62zz__objectz00(obj_t
		BgL_envz00_5385)
	{
		{	/* Llib/object.scm 269 */
			{	/* Llib/object.scm 269 */
				BgL_z62securityzd2exceptionzb0_bglt BgL_new1185z00_6078;

				BgL_new1185z00_6078 =
					((BgL_z62securityzd2exceptionzb0_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_z62securityzd2exceptionzb0_bgl))));
				{	/* Llib/object.scm 269 */
					long BgL_arg2113z00_6079;

					BgL_arg2113z00_6079 =
						BGL_CLASS_NUM(BGl_z62securityzd2exceptionzb0zz__objectz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1185z00_6078), BgL_arg2113z00_6079);
				}
				return BgL_new1185z00_6078;
			}
		}

	}



/* &lambda2110 */
	BgL_z62securityzd2exceptionzb0_bglt BGl_z62lambda2110z62zz__objectz00(obj_t
		BgL_envz00_5386, obj_t BgL_fname1181z00_5387,
		obj_t BgL_location1182z00_5388, obj_t BgL_stack1183z00_5389,
		obj_t BgL_message1184z00_5390)
	{
		{	/* Llib/object.scm 269 */
			{	/* Llib/object.scm 269 */
				BgL_z62securityzd2exceptionzb0_bglt BgL_new1271z00_6081;

				{	/* Llib/object.scm 269 */
					BgL_z62securityzd2exceptionzb0_bglt BgL_new1270z00_6082;

					BgL_new1270z00_6082 =
						((BgL_z62securityzd2exceptionzb0_bglt)
						BOBJECT(GC_MALLOC(sizeof(struct
									BgL_z62securityzd2exceptionzb0_bgl))));
					{	/* Llib/object.scm 269 */
						long BgL_arg2111z00_6083;

						BgL_arg2111z00_6083 =
							BGL_CLASS_NUM(BGl_z62securityzd2exceptionzb0zz__objectz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1270z00_6082), BgL_arg2111z00_6083);
					}
					BgL_new1271z00_6081 = BgL_new1270z00_6082;
				}
				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt) BgL_new1271z00_6081)))->
						BgL_fnamez00) = ((obj_t) BgL_fname1181z00_5387), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1271z00_6081)))->BgL_locationz00) =
					((obj_t) BgL_location1182z00_5388), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1271z00_6081)))->BgL_stackz00) =
					((obj_t) BgL_stack1183z00_5389), BUNSPEC);
				((((BgL_z62securityzd2exceptionzb0_bglt) COBJECT(BgL_new1271z00_6081))->
						BgL_messagez00) =
					((obj_t) ((obj_t) BgL_message1184z00_5390)), BUNSPEC);
				return BgL_new1271z00_6081;
			}
		}

	}



/* &<@anonymous:2121> */
	obj_t BGl_z62zc3z04anonymousza32121ze3ze5zz__objectz00(obj_t BgL_envz00_5391)
	{
		{	/* Llib/object.scm 269 */
			return BGl_string2936z00zz__objectz00;
		}

	}



/* &lambda2120 */
	obj_t BGl_z62lambda2120z62zz__objectz00(obj_t BgL_envz00_5392,
		obj_t BgL_oz00_5393, obj_t BgL_vz00_5394)
	{
		{	/* Llib/object.scm 269 */
			return
				((((BgL_z62securityzd2exceptionzb0_bglt) COBJECT(
							((BgL_z62securityzd2exceptionzb0_bglt) BgL_oz00_5393)))->
					BgL_messagez00) = ((obj_t) ((obj_t) BgL_vz00_5394)), BUNSPEC);
		}

	}



/* &lambda2119 */
	obj_t BGl_z62lambda2119z62zz__objectz00(obj_t BgL_envz00_5395,
		obj_t BgL_oz00_5396)
	{
		{	/* Llib/object.scm 269 */
			return
				(((BgL_z62securityzd2exceptionzb0_bglt) COBJECT(
						((BgL_z62securityzd2exceptionzb0_bglt) BgL_oz00_5396)))->
				BgL_messagez00);
		}

	}



/* &<@anonymous:2104> */
	obj_t BGl_z62zc3z04anonymousza32104ze3ze5zz__objectz00(obj_t BgL_envz00_5397,
		obj_t BgL_new1179z00_5398)
	{
		{	/* Llib/object.scm 267 */
			{
				BgL_z62stackzd2overflowzd2errorz62_bglt BgL_auxz00_8845;

				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt)
									((BgL_z62stackzd2overflowzd2errorz62_bglt)
										BgL_new1179z00_5398))))->BgL_fnamez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) (
										(BgL_z62stackzd2overflowzd2errorz62_bglt)
										BgL_new1179z00_5398))))->BgL_locationz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) (
										(BgL_z62stackzd2overflowzd2errorz62_bglt)
										BgL_new1179z00_5398))))->BgL_stackz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62stackzd2overflowzd2errorz62_bglt)
										BgL_new1179z00_5398))))->BgL_procz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62stackzd2overflowzd2errorz62_bglt)
										BgL_new1179z00_5398))))->BgL_msgz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62stackzd2overflowzd2errorz62_bglt)
										BgL_new1179z00_5398))))->BgL_objz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_8845 =
					((BgL_z62stackzd2overflowzd2errorz62_bglt) BgL_new1179z00_5398);
				return ((obj_t) BgL_auxz00_8845);
			}
		}

	}



/* &lambda2102 */
	BgL_z62stackzd2overflowzd2errorz62_bglt
		BGl_z62lambda2102z62zz__objectz00(obj_t BgL_envz00_5399)
	{
		{	/* Llib/object.scm 267 */
			{	/* Llib/object.scm 267 */
				BgL_z62stackzd2overflowzd2errorz62_bglt BgL_new1178z00_6088;

				BgL_new1178z00_6088 =
					((BgL_z62stackzd2overflowzd2errorz62_bglt)
					BOBJECT(GC_MALLOC(sizeof(struct
								BgL_z62stackzd2overflowzd2errorz62_bgl))));
				{	/* Llib/object.scm 267 */
					long BgL_arg2103z00_6089;

					BgL_arg2103z00_6089 =
						BGL_CLASS_NUM(BGl_z62stackzd2overflowzd2errorz62zz__objectz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1178z00_6088), BgL_arg2103z00_6089);
				}
				return BgL_new1178z00_6088;
			}
		}

	}



/* &lambda2100 */
	BgL_z62stackzd2overflowzd2errorz62_bglt
		BGl_z62lambda2100z62zz__objectz00(obj_t BgL_envz00_5400,
		obj_t BgL_fname1172z00_5401, obj_t BgL_location1173z00_5402,
		obj_t BgL_stack1174z00_5403, obj_t BgL_proc1175z00_5404,
		obj_t BgL_msg1176z00_5405, obj_t BgL_obj1177z00_5406)
	{
		{	/* Llib/object.scm 267 */
			{	/* Llib/object.scm 267 */
				BgL_z62stackzd2overflowzd2errorz62_bglt BgL_new1269z00_6090;

				{	/* Llib/object.scm 267 */
					BgL_z62stackzd2overflowzd2errorz62_bglt BgL_new1268z00_6091;

					BgL_new1268z00_6091 =
						((BgL_z62stackzd2overflowzd2errorz62_bglt)
						BOBJECT(GC_MALLOC(sizeof(struct
									BgL_z62stackzd2overflowzd2errorz62_bgl))));
					{	/* Llib/object.scm 267 */
						long BgL_arg2101z00_6092;

						BgL_arg2101z00_6092 =
							BGL_CLASS_NUM(BGl_z62stackzd2overflowzd2errorz62zz__objectz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1268z00_6091), BgL_arg2101z00_6092);
					}
					BgL_new1269z00_6090 = BgL_new1268z00_6091;
				}
				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt) BgL_new1269z00_6090)))->
						BgL_fnamez00) = ((obj_t) BgL_fname1172z00_5401), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1269z00_6090)))->BgL_locationz00) =
					((obj_t) BgL_location1173z00_5402), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1269z00_6090)))->BgL_stackz00) =
					((obj_t) BgL_stack1174z00_5403), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1269z00_6090)))->BgL_procz00) =
					((obj_t) BgL_proc1175z00_5404), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1269z00_6090)))->BgL_msgz00) =
					((obj_t) BgL_msg1176z00_5405), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1269z00_6090)))->BgL_objz00) =
					((obj_t) BgL_obj1177z00_5406), BUNSPEC);
				return BgL_new1269z00_6090;
			}
		}

	}



/* &<@anonymous:2094> */
	obj_t BGl_z62zc3z04anonymousza32094ze3ze5zz__objectz00(obj_t BgL_envz00_5407,
		obj_t BgL_new1170z00_5408)
	{
		{	/* Llib/object.scm 265 */
			{
				BgL_z62processzd2exceptionzb0_bglt BgL_auxz00_8886;

				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt)
									((BgL_z62processzd2exceptionzb0_bglt)
										BgL_new1170z00_5408))))->BgL_fnamez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) (
										(BgL_z62processzd2exceptionzb0_bglt)
										BgL_new1170z00_5408))))->BgL_locationz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) (
										(BgL_z62processzd2exceptionzb0_bglt)
										BgL_new1170z00_5408))))->BgL_stackz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62processzd2exceptionzb0_bglt)
										BgL_new1170z00_5408))))->BgL_procz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62processzd2exceptionzb0_bglt)
										BgL_new1170z00_5408))))->BgL_msgz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62processzd2exceptionzb0_bglt)
										BgL_new1170z00_5408))))->BgL_objz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_8886 =
					((BgL_z62processzd2exceptionzb0_bglt) BgL_new1170z00_5408);
				return ((obj_t) BgL_auxz00_8886);
			}
		}

	}



/* &lambda2092 */
	BgL_z62processzd2exceptionzb0_bglt BGl_z62lambda2092z62zz__objectz00(obj_t
		BgL_envz00_5409)
	{
		{	/* Llib/object.scm 265 */
			{	/* Llib/object.scm 265 */
				BgL_z62processzd2exceptionzb0_bglt BgL_new1169z00_6094;

				BgL_new1169z00_6094 =
					((BgL_z62processzd2exceptionzb0_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_z62processzd2exceptionzb0_bgl))));
				{	/* Llib/object.scm 265 */
					long BgL_arg2093z00_6095;

					BgL_arg2093z00_6095 =
						BGL_CLASS_NUM(BGl_z62processzd2exceptionzb0zz__objectz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1169z00_6094), BgL_arg2093z00_6095);
				}
				return BgL_new1169z00_6094;
			}
		}

	}



/* &lambda2090 */
	BgL_z62processzd2exceptionzb0_bglt BGl_z62lambda2090z62zz__objectz00(obj_t
		BgL_envz00_5410, obj_t BgL_fname1163z00_5411,
		obj_t BgL_location1164z00_5412, obj_t BgL_stack1165z00_5413,
		obj_t BgL_proc1166z00_5414, obj_t BgL_msg1167z00_5415,
		obj_t BgL_obj1168z00_5416)
	{
		{	/* Llib/object.scm 265 */
			{	/* Llib/object.scm 265 */
				BgL_z62processzd2exceptionzb0_bglt BgL_new1267z00_6096;

				{	/* Llib/object.scm 265 */
					BgL_z62processzd2exceptionzb0_bglt BgL_new1266z00_6097;

					BgL_new1266z00_6097 =
						((BgL_z62processzd2exceptionzb0_bglt)
						BOBJECT(GC_MALLOC(sizeof(struct
									BgL_z62processzd2exceptionzb0_bgl))));
					{	/* Llib/object.scm 265 */
						long BgL_arg2091z00_6098;

						BgL_arg2091z00_6098 =
							BGL_CLASS_NUM(BGl_z62processzd2exceptionzb0zz__objectz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1266z00_6097), BgL_arg2091z00_6098);
					}
					BgL_new1267z00_6096 = BgL_new1266z00_6097;
				}
				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt) BgL_new1267z00_6096)))->
						BgL_fnamez00) = ((obj_t) BgL_fname1163z00_5411), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1267z00_6096)))->BgL_locationz00) =
					((obj_t) BgL_location1164z00_5412), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1267z00_6096)))->BgL_stackz00) =
					((obj_t) BgL_stack1165z00_5413), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1267z00_6096)))->BgL_procz00) =
					((obj_t) BgL_proc1166z00_5414), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1267z00_6096)))->BgL_msgz00) =
					((obj_t) BgL_msg1167z00_5415), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1267z00_6096)))->BgL_objz00) =
					((obj_t) BgL_obj1168z00_5416), BUNSPEC);
				return BgL_new1267z00_6096;
			}
		}

	}



/* &<@anonymous:2083> */
	obj_t BGl_z62zc3z04anonymousza32083ze3ze5zz__objectz00(obj_t BgL_envz00_5417,
		obj_t BgL_new1161z00_5418)
	{
		{	/* Llib/object.scm 263 */
			{
				BgL_z62iozd2connectionzd2errorz62_bglt BgL_auxz00_8927;

				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt)
									((BgL_z62iozd2connectionzd2errorz62_bglt)
										BgL_new1161z00_5418))))->BgL_fnamez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) (
										(BgL_z62iozd2connectionzd2errorz62_bglt)
										BgL_new1161z00_5418))))->BgL_locationz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) (
										(BgL_z62iozd2connectionzd2errorz62_bglt)
										BgL_new1161z00_5418))))->BgL_stackz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62iozd2connectionzd2errorz62_bglt)
										BgL_new1161z00_5418))))->BgL_procz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62iozd2connectionzd2errorz62_bglt)
										BgL_new1161z00_5418))))->BgL_msgz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62iozd2connectionzd2errorz62_bglt)
										BgL_new1161z00_5418))))->BgL_objz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_8927 =
					((BgL_z62iozd2connectionzd2errorz62_bglt) BgL_new1161z00_5418);
				return ((obj_t) BgL_auxz00_8927);
			}
		}

	}



/* &lambda2081 */
	BgL_z62iozd2connectionzd2errorz62_bglt BGl_z62lambda2081z62zz__objectz00(obj_t
		BgL_envz00_5419)
	{
		{	/* Llib/object.scm 263 */
			{	/* Llib/object.scm 263 */
				BgL_z62iozd2connectionzd2errorz62_bglt BgL_new1160z00_6100;

				BgL_new1160z00_6100 =
					((BgL_z62iozd2connectionzd2errorz62_bglt)
					BOBJECT(GC_MALLOC(sizeof(struct
								BgL_z62iozd2connectionzd2errorz62_bgl))));
				{	/* Llib/object.scm 263 */
					long BgL_arg2082z00_6101;

					BgL_arg2082z00_6101 =
						BGL_CLASS_NUM(BGl_z62iozd2connectionzd2errorz62zz__objectz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1160z00_6100), BgL_arg2082z00_6101);
				}
				return BgL_new1160z00_6100;
			}
		}

	}



/* &lambda2079 */
	BgL_z62iozd2connectionzd2errorz62_bglt BGl_z62lambda2079z62zz__objectz00(obj_t
		BgL_envz00_5420, obj_t BgL_fname1154z00_5421,
		obj_t BgL_location1155z00_5422, obj_t BgL_stack1156z00_5423,
		obj_t BgL_proc1157z00_5424, obj_t BgL_msg1158z00_5425,
		obj_t BgL_obj1159z00_5426)
	{
		{	/* Llib/object.scm 263 */
			{	/* Llib/object.scm 263 */
				BgL_z62iozd2connectionzd2errorz62_bglt BgL_new1265z00_6102;

				{	/* Llib/object.scm 263 */
					BgL_z62iozd2connectionzd2errorz62_bglt BgL_new1264z00_6103;

					BgL_new1264z00_6103 =
						((BgL_z62iozd2connectionzd2errorz62_bglt)
						BOBJECT(GC_MALLOC(sizeof(struct
									BgL_z62iozd2connectionzd2errorz62_bgl))));
					{	/* Llib/object.scm 263 */
						long BgL_arg2080z00_6104;

						BgL_arg2080z00_6104 =
							BGL_CLASS_NUM(BGl_z62iozd2connectionzd2errorz62zz__objectz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1264z00_6103), BgL_arg2080z00_6104);
					}
					BgL_new1265z00_6102 = BgL_new1264z00_6103;
				}
				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt) BgL_new1265z00_6102)))->
						BgL_fnamez00) = ((obj_t) BgL_fname1154z00_5421), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1265z00_6102)))->BgL_locationz00) =
					((obj_t) BgL_location1155z00_5422), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1265z00_6102)))->BgL_stackz00) =
					((obj_t) BgL_stack1156z00_5423), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1265z00_6102)))->BgL_procz00) =
					((obj_t) BgL_proc1157z00_5424), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1265z00_6102)))->BgL_msgz00) =
					((obj_t) BgL_msg1158z00_5425), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1265z00_6102)))->BgL_objz00) =
					((obj_t) BgL_obj1159z00_5426), BUNSPEC);
				return BgL_new1265z00_6102;
			}
		}

	}



/* &<@anonymous:2073> */
	obj_t BGl_z62zc3z04anonymousza32073ze3ze5zz__objectz00(obj_t BgL_envz00_5427,
		obj_t BgL_new1152z00_5428)
	{
		{	/* Llib/object.scm 262 */
			{
				BgL_z62iozd2timeoutzd2errorz62_bglt BgL_auxz00_8968;

				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt)
									((BgL_z62iozd2timeoutzd2errorz62_bglt)
										BgL_new1152z00_5428))))->BgL_fnamez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) (
										(BgL_z62iozd2timeoutzd2errorz62_bglt)
										BgL_new1152z00_5428))))->BgL_locationz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) (
										(BgL_z62iozd2timeoutzd2errorz62_bglt)
										BgL_new1152z00_5428))))->BgL_stackz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62iozd2timeoutzd2errorz62_bglt)
										BgL_new1152z00_5428))))->BgL_procz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62iozd2timeoutzd2errorz62_bglt)
										BgL_new1152z00_5428))))->BgL_msgz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62iozd2timeoutzd2errorz62_bglt)
										BgL_new1152z00_5428))))->BgL_objz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_8968 =
					((BgL_z62iozd2timeoutzd2errorz62_bglt) BgL_new1152z00_5428);
				return ((obj_t) BgL_auxz00_8968);
			}
		}

	}



/* &lambda2071 */
	BgL_z62iozd2timeoutzd2errorz62_bglt BGl_z62lambda2071z62zz__objectz00(obj_t
		BgL_envz00_5429)
	{
		{	/* Llib/object.scm 262 */
			{	/* Llib/object.scm 262 */
				BgL_z62iozd2timeoutzd2errorz62_bglt BgL_new1151z00_6106;

				BgL_new1151z00_6106 =
					((BgL_z62iozd2timeoutzd2errorz62_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_z62iozd2timeoutzd2errorz62_bgl))));
				{	/* Llib/object.scm 262 */
					long BgL_arg2072z00_6107;

					BgL_arg2072z00_6107 =
						BGL_CLASS_NUM(BGl_z62iozd2timeoutzd2errorz62zz__objectz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1151z00_6106), BgL_arg2072z00_6107);
				}
				return BgL_new1151z00_6106;
			}
		}

	}



/* &lambda2069 */
	BgL_z62iozd2timeoutzd2errorz62_bglt BGl_z62lambda2069z62zz__objectz00(obj_t
		BgL_envz00_5430, obj_t BgL_fname1145z00_5431,
		obj_t BgL_location1146z00_5432, obj_t BgL_stack1147z00_5433,
		obj_t BgL_proc1148z00_5434, obj_t BgL_msg1149z00_5435,
		obj_t BgL_obj1150z00_5436)
	{
		{	/* Llib/object.scm 262 */
			{	/* Llib/object.scm 262 */
				BgL_z62iozd2timeoutzd2errorz62_bglt BgL_new1263z00_6108;

				{	/* Llib/object.scm 262 */
					BgL_z62iozd2timeoutzd2errorz62_bglt BgL_new1262z00_6109;

					BgL_new1262z00_6109 =
						((BgL_z62iozd2timeoutzd2errorz62_bglt)
						BOBJECT(GC_MALLOC(sizeof(struct
									BgL_z62iozd2timeoutzd2errorz62_bgl))));
					{	/* Llib/object.scm 262 */
						long BgL_arg2070z00_6110;

						BgL_arg2070z00_6110 =
							BGL_CLASS_NUM(BGl_z62iozd2timeoutzd2errorz62zz__objectz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1262z00_6109), BgL_arg2070z00_6110);
					}
					BgL_new1263z00_6108 = BgL_new1262z00_6109;
				}
				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt) BgL_new1263z00_6108)))->
						BgL_fnamez00) = ((obj_t) BgL_fname1145z00_5431), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1263z00_6108)))->BgL_locationz00) =
					((obj_t) BgL_location1146z00_5432), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1263z00_6108)))->BgL_stackz00) =
					((obj_t) BgL_stack1147z00_5433), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1263z00_6108)))->BgL_procz00) =
					((obj_t) BgL_proc1148z00_5434), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1263z00_6108)))->BgL_msgz00) =
					((obj_t) BgL_msg1149z00_5435), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1263z00_6108)))->BgL_objz00) =
					((obj_t) BgL_obj1150z00_5436), BUNSPEC);
				return BgL_new1263z00_6108;
			}
		}

	}



/* &<@anonymous:2062> */
	obj_t BGl_z62zc3z04anonymousza32062ze3ze5zz__objectz00(obj_t BgL_envz00_5437,
		obj_t BgL_new1143z00_5438)
	{
		{	/* Llib/object.scm 261 */
			{
				BgL_z62iozd2sigpipezd2errorz62_bglt BgL_auxz00_9009;

				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt)
									((BgL_z62iozd2sigpipezd2errorz62_bglt)
										BgL_new1143z00_5438))))->BgL_fnamez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) (
										(BgL_z62iozd2sigpipezd2errorz62_bglt)
										BgL_new1143z00_5438))))->BgL_locationz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) (
										(BgL_z62iozd2sigpipezd2errorz62_bglt)
										BgL_new1143z00_5438))))->BgL_stackz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62iozd2sigpipezd2errorz62_bglt)
										BgL_new1143z00_5438))))->BgL_procz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62iozd2sigpipezd2errorz62_bglt)
										BgL_new1143z00_5438))))->BgL_msgz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62iozd2sigpipezd2errorz62_bglt)
										BgL_new1143z00_5438))))->BgL_objz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_9009 =
					((BgL_z62iozd2sigpipezd2errorz62_bglt) BgL_new1143z00_5438);
				return ((obj_t) BgL_auxz00_9009);
			}
		}

	}



/* &lambda2060 */
	BgL_z62iozd2sigpipezd2errorz62_bglt BGl_z62lambda2060z62zz__objectz00(obj_t
		BgL_envz00_5439)
	{
		{	/* Llib/object.scm 261 */
			{	/* Llib/object.scm 261 */
				BgL_z62iozd2sigpipezd2errorz62_bglt BgL_new1142z00_6112;

				BgL_new1142z00_6112 =
					((BgL_z62iozd2sigpipezd2errorz62_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_z62iozd2sigpipezd2errorz62_bgl))));
				{	/* Llib/object.scm 261 */
					long BgL_arg2061z00_6113;

					BgL_arg2061z00_6113 =
						BGL_CLASS_NUM(BGl_z62iozd2sigpipezd2errorz62zz__objectz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1142z00_6112), BgL_arg2061z00_6113);
				}
				return BgL_new1142z00_6112;
			}
		}

	}



/* &lambda2058 */
	BgL_z62iozd2sigpipezd2errorz62_bglt BGl_z62lambda2058z62zz__objectz00(obj_t
		BgL_envz00_5440, obj_t BgL_fname1136z00_5441,
		obj_t BgL_location1137z00_5442, obj_t BgL_stack1138z00_5443,
		obj_t BgL_proc1139z00_5444, obj_t BgL_msg1140z00_5445,
		obj_t BgL_obj1141z00_5446)
	{
		{	/* Llib/object.scm 261 */
			{	/* Llib/object.scm 261 */
				BgL_z62iozd2sigpipezd2errorz62_bglt BgL_new1261z00_6114;

				{	/* Llib/object.scm 261 */
					BgL_z62iozd2sigpipezd2errorz62_bglt BgL_new1260z00_6115;

					BgL_new1260z00_6115 =
						((BgL_z62iozd2sigpipezd2errorz62_bglt)
						BOBJECT(GC_MALLOC(sizeof(struct
									BgL_z62iozd2sigpipezd2errorz62_bgl))));
					{	/* Llib/object.scm 261 */
						long BgL_arg2059z00_6116;

						BgL_arg2059z00_6116 =
							BGL_CLASS_NUM(BGl_z62iozd2sigpipezd2errorz62zz__objectz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1260z00_6115), BgL_arg2059z00_6116);
					}
					BgL_new1261z00_6114 = BgL_new1260z00_6115;
				}
				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt) BgL_new1261z00_6114)))->
						BgL_fnamez00) = ((obj_t) BgL_fname1136z00_5441), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1261z00_6114)))->BgL_locationz00) =
					((obj_t) BgL_location1137z00_5442), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1261z00_6114)))->BgL_stackz00) =
					((obj_t) BgL_stack1138z00_5443), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1261z00_6114)))->BgL_procz00) =
					((obj_t) BgL_proc1139z00_5444), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1261z00_6114)))->BgL_msgz00) =
					((obj_t) BgL_msg1140z00_5445), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1261z00_6114)))->BgL_objz00) =
					((obj_t) BgL_obj1141z00_5446), BUNSPEC);
				return BgL_new1261z00_6114;
			}
		}

	}



/* &<@anonymous:2050> */
	obj_t BGl_z62zc3z04anonymousza32050ze3ze5zz__objectz00(obj_t BgL_envz00_5447,
		obj_t BgL_new1134z00_5448)
	{
		{	/* Llib/object.scm 260 */
			{
				BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt BgL_auxz00_9050;

				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt)
									((BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt)
										BgL_new1134z00_5448))))->BgL_fnamez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) (
										(BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt)
										BgL_new1134z00_5448))))->BgL_locationz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) (
										(BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt)
										BgL_new1134z00_5448))))->BgL_stackz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt)
										BgL_new1134z00_5448))))->BgL_procz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt)
										BgL_new1134z00_5448))))->BgL_msgz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt)
										BgL_new1134z00_5448))))->BgL_objz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_9050 =
					((BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt) BgL_new1134z00_5448);
				return ((obj_t) BgL_auxz00_9050);
			}
		}

	}



/* &lambda2048 */
	BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt
		BGl_z62lambda2048z62zz__objectz00(obj_t BgL_envz00_5449)
	{
		{	/* Llib/object.scm 260 */
			{	/* Llib/object.scm 260 */
				BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt BgL_new1133z00_6118;

				BgL_new1133z00_6118 =
					((BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt)
					BOBJECT(GC_MALLOC(sizeof(struct
								BgL_z62iozd2malformedzd2urlzd2errorzb0_bgl))));
				{	/* Llib/object.scm 260 */
					long BgL_arg2049z00_6119;

					BgL_arg2049z00_6119 =
						BGL_CLASS_NUM(BGl_z62iozd2malformedzd2urlzd2errorzb0zz__objectz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1133z00_6118), BgL_arg2049z00_6119);
				}
				return BgL_new1133z00_6118;
			}
		}

	}



/* &lambda2046 */
	BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt
		BGl_z62lambda2046z62zz__objectz00(obj_t BgL_envz00_5450,
		obj_t BgL_fname1127z00_5451, obj_t BgL_location1128z00_5452,
		obj_t BgL_stack1129z00_5453, obj_t BgL_proc1130z00_5454,
		obj_t BgL_msg1131z00_5455, obj_t BgL_obj1132z00_5456)
	{
		{	/* Llib/object.scm 260 */
			{	/* Llib/object.scm 260 */
				BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt BgL_new1258z00_6120;

				{	/* Llib/object.scm 260 */
					BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt BgL_new1257z00_6121;

					BgL_new1257z00_6121 =
						((BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt)
						BOBJECT(GC_MALLOC(sizeof(struct
									BgL_z62iozd2malformedzd2urlzd2errorzb0_bgl))));
					{	/* Llib/object.scm 260 */
						long BgL_arg2047z00_6122;

						BgL_arg2047z00_6122 =
							BGL_CLASS_NUM
							(BGl_z62iozd2malformedzd2urlzd2errorzb0zz__objectz00);
						BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) BgL_new1257z00_6121),
							BgL_arg2047z00_6122);
					}
					BgL_new1258z00_6120 = BgL_new1257z00_6121;
				}
				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt) BgL_new1258z00_6120)))->
						BgL_fnamez00) = ((obj_t) BgL_fname1127z00_5451), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1258z00_6120)))->BgL_locationz00) =
					((obj_t) BgL_location1128z00_5452), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1258z00_6120)))->BgL_stackz00) =
					((obj_t) BgL_stack1129z00_5453), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1258z00_6120)))->BgL_procz00) =
					((obj_t) BgL_proc1130z00_5454), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1258z00_6120)))->BgL_msgz00) =
					((obj_t) BgL_msg1131z00_5455), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1258z00_6120)))->BgL_objz00) =
					((obj_t) BgL_obj1132z00_5456), BUNSPEC);
				return BgL_new1258z00_6120;
			}
		}

	}



/* &<@anonymous:2039> */
	obj_t BGl_z62zc3z04anonymousza32039ze3ze5zz__objectz00(obj_t BgL_envz00_5457,
		obj_t BgL_new1125z00_5458)
	{
		{	/* Llib/object.scm 259 */
			{
				BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt BgL_auxz00_9091;

				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt)
									((BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt)
										BgL_new1125z00_5458))))->BgL_fnamez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) (
										(BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt)
										BgL_new1125z00_5458))))->BgL_locationz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) (
										(BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt)
										BgL_new1125z00_5458))))->BgL_stackz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt)
										BgL_new1125z00_5458))))->BgL_procz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt)
										BgL_new1125z00_5458))))->BgL_msgz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt)
										BgL_new1125z00_5458))))->BgL_objz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_9091 =
					((BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt) BgL_new1125z00_5458);
				return ((obj_t) BgL_auxz00_9091);
			}
		}

	}



/* &lambda2037 */
	BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt
		BGl_z62lambda2037z62zz__objectz00(obj_t BgL_envz00_5459)
	{
		{	/* Llib/object.scm 259 */
			{	/* Llib/object.scm 259 */
				BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt BgL_new1124z00_6124;

				BgL_new1124z00_6124 =
					((BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt)
					BOBJECT(GC_MALLOC(sizeof(struct
								BgL_z62iozd2unknownzd2hostzd2errorzb0_bgl))));
				{	/* Llib/object.scm 259 */
					long BgL_arg2038z00_6125;

					BgL_arg2038z00_6125 =
						BGL_CLASS_NUM(BGl_z62iozd2unknownzd2hostzd2errorzb0zz__objectz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1124z00_6124), BgL_arg2038z00_6125);
				}
				return BgL_new1124z00_6124;
			}
		}

	}



/* &lambda2035 */
	BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt
		BGl_z62lambda2035z62zz__objectz00(obj_t BgL_envz00_5460,
		obj_t BgL_fname1118z00_5461, obj_t BgL_location1119z00_5462,
		obj_t BgL_stack1120z00_5463, obj_t BgL_proc1121z00_5464,
		obj_t BgL_msg1122z00_5465, obj_t BgL_obj1123z00_5466)
	{
		{	/* Llib/object.scm 259 */
			{	/* Llib/object.scm 259 */
				BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt BgL_new1256z00_6126;

				{	/* Llib/object.scm 259 */
					BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt BgL_new1255z00_6127;

					BgL_new1255z00_6127 =
						((BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt)
						BOBJECT(GC_MALLOC(sizeof(struct
									BgL_z62iozd2unknownzd2hostzd2errorzb0_bgl))));
					{	/* Llib/object.scm 259 */
						long BgL_arg2036z00_6128;

						BgL_arg2036z00_6128 =
							BGL_CLASS_NUM(BGl_z62iozd2unknownzd2hostzd2errorzb0zz__objectz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1255z00_6127), BgL_arg2036z00_6128);
					}
					BgL_new1256z00_6126 = BgL_new1255z00_6127;
				}
				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt) BgL_new1256z00_6126)))->
						BgL_fnamez00) = ((obj_t) BgL_fname1118z00_5461), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1256z00_6126)))->BgL_locationz00) =
					((obj_t) BgL_location1119z00_5462), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1256z00_6126)))->BgL_stackz00) =
					((obj_t) BgL_stack1120z00_5463), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1256z00_6126)))->BgL_procz00) =
					((obj_t) BgL_proc1121z00_5464), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1256z00_6126)))->BgL_msgz00) =
					((obj_t) BgL_msg1122z00_5465), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1256z00_6126)))->BgL_objz00) =
					((obj_t) BgL_obj1123z00_5466), BUNSPEC);
				return BgL_new1256z00_6126;
			}
		}

	}



/* &<@anonymous:2027> */
	obj_t BGl_z62zc3z04anonymousza32027ze3ze5zz__objectz00(obj_t BgL_envz00_5467,
		obj_t BgL_new1116z00_5468)
	{
		{	/* Llib/object.scm 258 */
			{
				BgL_z62iozd2parsezd2errorz62_bglt BgL_auxz00_9132;

				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt)
									((BgL_z62iozd2parsezd2errorz62_bglt) BgL_new1116z00_5468))))->
						BgL_fnamez00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) (
										(BgL_z62iozd2parsezd2errorz62_bglt)
										BgL_new1116z00_5468))))->BgL_locationz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) (
										(BgL_z62iozd2parsezd2errorz62_bglt)
										BgL_new1116z00_5468))))->BgL_stackz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62iozd2parsezd2errorz62_bglt)
										BgL_new1116z00_5468))))->BgL_procz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62iozd2parsezd2errorz62_bglt)
										BgL_new1116z00_5468))))->BgL_msgz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62iozd2parsezd2errorz62_bglt)
										BgL_new1116z00_5468))))->BgL_objz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_9132 =
					((BgL_z62iozd2parsezd2errorz62_bglt) BgL_new1116z00_5468);
				return ((obj_t) BgL_auxz00_9132);
			}
		}

	}



/* &lambda2025 */
	BgL_z62iozd2parsezd2errorz62_bglt BGl_z62lambda2025z62zz__objectz00(obj_t
		BgL_envz00_5469)
	{
		{	/* Llib/object.scm 258 */
			{	/* Llib/object.scm 258 */
				BgL_z62iozd2parsezd2errorz62_bglt BgL_new1115z00_6130;

				BgL_new1115z00_6130 =
					((BgL_z62iozd2parsezd2errorz62_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_z62iozd2parsezd2errorz62_bgl))));
				{	/* Llib/object.scm 258 */
					long BgL_arg2026z00_6131;

					BgL_arg2026z00_6131 =
						BGL_CLASS_NUM(BGl_z62iozd2parsezd2errorz62zz__objectz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1115z00_6130), BgL_arg2026z00_6131);
				}
				return BgL_new1115z00_6130;
			}
		}

	}



/* &lambda2023 */
	BgL_z62iozd2parsezd2errorz62_bglt BGl_z62lambda2023z62zz__objectz00(obj_t
		BgL_envz00_5470, obj_t BgL_fname1109z00_5471,
		obj_t BgL_location1110z00_5472, obj_t BgL_stack1111z00_5473,
		obj_t BgL_proc1112z00_5474, obj_t BgL_msg1113z00_5475,
		obj_t BgL_obj1114z00_5476)
	{
		{	/* Llib/object.scm 258 */
			{	/* Llib/object.scm 258 */
				BgL_z62iozd2parsezd2errorz62_bglt BgL_new1254z00_6132;

				{	/* Llib/object.scm 258 */
					BgL_z62iozd2parsezd2errorz62_bglt BgL_new1253z00_6133;

					BgL_new1253z00_6133 =
						((BgL_z62iozd2parsezd2errorz62_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_z62iozd2parsezd2errorz62_bgl))));
					{	/* Llib/object.scm 258 */
						long BgL_arg2024z00_6134;

						BgL_arg2024z00_6134 =
							BGL_CLASS_NUM(BGl_z62iozd2parsezd2errorz62zz__objectz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1253z00_6133), BgL_arg2024z00_6134);
					}
					BgL_new1254z00_6132 = BgL_new1253z00_6133;
				}
				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt) BgL_new1254z00_6132)))->
						BgL_fnamez00) = ((obj_t) BgL_fname1109z00_5471), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1254z00_6132)))->BgL_locationz00) =
					((obj_t) BgL_location1110z00_5472), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1254z00_6132)))->BgL_stackz00) =
					((obj_t) BgL_stack1111z00_5473), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1254z00_6132)))->BgL_procz00) =
					((obj_t) BgL_proc1112z00_5474), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1254z00_6132)))->BgL_msgz00) =
					((obj_t) BgL_msg1113z00_5475), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1254z00_6132)))->BgL_objz00) =
					((obj_t) BgL_obj1114z00_5476), BUNSPEC);
				return BgL_new1254z00_6132;
			}
		}

	}



/* &<@anonymous:2017> */
	obj_t BGl_z62zc3z04anonymousza32017ze3ze5zz__objectz00(obj_t BgL_envz00_5477,
		obj_t BgL_new1107z00_5478)
	{
		{	/* Llib/object.scm 257 */
			{
				BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt BgL_auxz00_9173;

				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt)
									((BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt)
										BgL_new1107z00_5478))))->BgL_fnamez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) (
										(BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt)
										BgL_new1107z00_5478))))->BgL_locationz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) (
										(BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt)
										BgL_new1107z00_5478))))->BgL_stackz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt)
										BgL_new1107z00_5478))))->BgL_procz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt)
										BgL_new1107z00_5478))))->BgL_msgz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt)
										BgL_new1107z00_5478))))->BgL_objz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_9173 =
					((BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt)
					BgL_new1107z00_5478);
				return ((obj_t) BgL_auxz00_9173);
			}
		}

	}



/* &lambda2015 */
	BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt
		BGl_z62lambda2015z62zz__objectz00(obj_t BgL_envz00_5479)
	{
		{	/* Llib/object.scm 257 */
			{	/* Llib/object.scm 257 */
				BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt BgL_new1106z00_6136;

				BgL_new1106z00_6136 =
					((BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt)
					BOBJECT(GC_MALLOC(sizeof(struct
								BgL_z62iozd2filezd2notzd2foundzd2errorz62_bgl))));
				{	/* Llib/object.scm 257 */
					long BgL_arg2016z00_6137;

					BgL_arg2016z00_6137 =
						BGL_CLASS_NUM
						(BGl_z62iozd2filezd2notzd2foundzd2errorz62zz__objectz00);
					BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) BgL_new1106z00_6136),
						BgL_arg2016z00_6137);
				}
				return BgL_new1106z00_6136;
			}
		}

	}



/* &lambda2013 */
	BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt
		BGl_z62lambda2013z62zz__objectz00(obj_t BgL_envz00_5480,
		obj_t BgL_fname1100z00_5481, obj_t BgL_location1101z00_5482,
		obj_t BgL_stack1102z00_5483, obj_t BgL_proc1103z00_5484,
		obj_t BgL_msg1104z00_5485, obj_t BgL_obj1105z00_5486)
	{
		{	/* Llib/object.scm 257 */
			{	/* Llib/object.scm 257 */
				BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt BgL_new1252z00_6138;

				{	/* Llib/object.scm 257 */
					BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt BgL_new1251z00_6139;

					BgL_new1251z00_6139 =
						((BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt)
						BOBJECT(GC_MALLOC(sizeof(struct
									BgL_z62iozd2filezd2notzd2foundzd2errorz62_bgl))));
					{	/* Llib/object.scm 257 */
						long BgL_arg2014z00_6140;

						BgL_arg2014z00_6140 =
							BGL_CLASS_NUM
							(BGl_z62iozd2filezd2notzd2foundzd2errorz62zz__objectz00);
						BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) BgL_new1251z00_6139),
							BgL_arg2014z00_6140);
					}
					BgL_new1252z00_6138 = BgL_new1251z00_6139;
				}
				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt) BgL_new1252z00_6138)))->
						BgL_fnamez00) = ((obj_t) BgL_fname1100z00_5481), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1252z00_6138)))->BgL_locationz00) =
					((obj_t) BgL_location1101z00_5482), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1252z00_6138)))->BgL_stackz00) =
					((obj_t) BgL_stack1102z00_5483), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1252z00_6138)))->BgL_procz00) =
					((obj_t) BgL_proc1103z00_5484), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1252z00_6138)))->BgL_msgz00) =
					((obj_t) BgL_msg1104z00_5485), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1252z00_6138)))->BgL_objz00) =
					((obj_t) BgL_obj1105z00_5486), BUNSPEC);
				return BgL_new1252z00_6138;
			}
		}

	}



/* &<@anonymous:2007> */
	obj_t BGl_z62zc3z04anonymousza32007ze3ze5zz__objectz00(obj_t BgL_envz00_5487,
		obj_t BgL_new1098z00_5488)
	{
		{	/* Llib/object.scm 256 */
			{
				BgL_z62iozd2closedzd2errorz62_bglt BgL_auxz00_9214;

				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt)
									((BgL_z62iozd2closedzd2errorz62_bglt)
										BgL_new1098z00_5488))))->BgL_fnamez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) (
										(BgL_z62iozd2closedzd2errorz62_bglt)
										BgL_new1098z00_5488))))->BgL_locationz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) (
										(BgL_z62iozd2closedzd2errorz62_bglt)
										BgL_new1098z00_5488))))->BgL_stackz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62iozd2closedzd2errorz62_bglt)
										BgL_new1098z00_5488))))->BgL_procz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62iozd2closedzd2errorz62_bglt)
										BgL_new1098z00_5488))))->BgL_msgz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62iozd2closedzd2errorz62_bglt)
										BgL_new1098z00_5488))))->BgL_objz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_9214 =
					((BgL_z62iozd2closedzd2errorz62_bglt) BgL_new1098z00_5488);
				return ((obj_t) BgL_auxz00_9214);
			}
		}

	}



/* &lambda2005 */
	BgL_z62iozd2closedzd2errorz62_bglt BGl_z62lambda2005z62zz__objectz00(obj_t
		BgL_envz00_5489)
	{
		{	/* Llib/object.scm 256 */
			{	/* Llib/object.scm 256 */
				BgL_z62iozd2closedzd2errorz62_bglt BgL_new1097z00_6142;

				BgL_new1097z00_6142 =
					((BgL_z62iozd2closedzd2errorz62_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_z62iozd2closedzd2errorz62_bgl))));
				{	/* Llib/object.scm 256 */
					long BgL_arg2006z00_6143;

					BgL_arg2006z00_6143 =
						BGL_CLASS_NUM(BGl_z62iozd2closedzd2errorz62zz__objectz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1097z00_6142), BgL_arg2006z00_6143);
				}
				return BgL_new1097z00_6142;
			}
		}

	}



/* &lambda2003 */
	BgL_z62iozd2closedzd2errorz62_bglt BGl_z62lambda2003z62zz__objectz00(obj_t
		BgL_envz00_5490, obj_t BgL_fname1091z00_5491,
		obj_t BgL_location1092z00_5492, obj_t BgL_stack1093z00_5493,
		obj_t BgL_proc1094z00_5494, obj_t BgL_msg1095z00_5495,
		obj_t BgL_obj1096z00_5496)
	{
		{	/* Llib/object.scm 256 */
			{	/* Llib/object.scm 256 */
				BgL_z62iozd2closedzd2errorz62_bglt BgL_new1249z00_6144;

				{	/* Llib/object.scm 256 */
					BgL_z62iozd2closedzd2errorz62_bglt BgL_new1248z00_6145;

					BgL_new1248z00_6145 =
						((BgL_z62iozd2closedzd2errorz62_bglt)
						BOBJECT(GC_MALLOC(sizeof(struct
									BgL_z62iozd2closedzd2errorz62_bgl))));
					{	/* Llib/object.scm 256 */
						long BgL_arg2004z00_6146;

						BgL_arg2004z00_6146 =
							BGL_CLASS_NUM(BGl_z62iozd2closedzd2errorz62zz__objectz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1248z00_6145), BgL_arg2004z00_6146);
					}
					BgL_new1249z00_6144 = BgL_new1248z00_6145;
				}
				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt) BgL_new1249z00_6144)))->
						BgL_fnamez00) = ((obj_t) BgL_fname1091z00_5491), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1249z00_6144)))->BgL_locationz00) =
					((obj_t) BgL_location1092z00_5492), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1249z00_6144)))->BgL_stackz00) =
					((obj_t) BgL_stack1093z00_5493), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1249z00_6144)))->BgL_procz00) =
					((obj_t) BgL_proc1094z00_5494), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1249z00_6144)))->BgL_msgz00) =
					((obj_t) BgL_msg1095z00_5495), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1249z00_6144)))->BgL_objz00) =
					((obj_t) BgL_obj1096z00_5496), BUNSPEC);
				return BgL_new1249z00_6144;
			}
		}

	}



/* &<@anonymous:1997> */
	obj_t BGl_z62zc3z04anonymousza31997ze3ze5zz__objectz00(obj_t BgL_envz00_5497,
		obj_t BgL_new1089z00_5498)
	{
		{	/* Llib/object.scm 255 */
			{
				BgL_z62iozd2writezd2errorz62_bglt BgL_auxz00_9255;

				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt)
									((BgL_z62iozd2writezd2errorz62_bglt) BgL_new1089z00_5498))))->
						BgL_fnamez00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) (
										(BgL_z62iozd2writezd2errorz62_bglt)
										BgL_new1089z00_5498))))->BgL_locationz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) (
										(BgL_z62iozd2writezd2errorz62_bglt)
										BgL_new1089z00_5498))))->BgL_stackz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62iozd2writezd2errorz62_bglt)
										BgL_new1089z00_5498))))->BgL_procz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62iozd2writezd2errorz62_bglt)
										BgL_new1089z00_5498))))->BgL_msgz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62iozd2writezd2errorz62_bglt)
										BgL_new1089z00_5498))))->BgL_objz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_9255 =
					((BgL_z62iozd2writezd2errorz62_bglt) BgL_new1089z00_5498);
				return ((obj_t) BgL_auxz00_9255);
			}
		}

	}



/* &lambda1995 */
	BgL_z62iozd2writezd2errorz62_bglt BGl_z62lambda1995z62zz__objectz00(obj_t
		BgL_envz00_5499)
	{
		{	/* Llib/object.scm 255 */
			{	/* Llib/object.scm 255 */
				BgL_z62iozd2writezd2errorz62_bglt BgL_new1088z00_6148;

				BgL_new1088z00_6148 =
					((BgL_z62iozd2writezd2errorz62_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_z62iozd2writezd2errorz62_bgl))));
				{	/* Llib/object.scm 255 */
					long BgL_arg1996z00_6149;

					BgL_arg1996z00_6149 =
						BGL_CLASS_NUM(BGl_z62iozd2writezd2errorz62zz__objectz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1088z00_6148), BgL_arg1996z00_6149);
				}
				return BgL_new1088z00_6148;
			}
		}

	}



/* &lambda1993 */
	BgL_z62iozd2writezd2errorz62_bglt BGl_z62lambda1993z62zz__objectz00(obj_t
		BgL_envz00_5500, obj_t BgL_fname1082z00_5501,
		obj_t BgL_location1083z00_5502, obj_t BgL_stack1084z00_5503,
		obj_t BgL_proc1085z00_5504, obj_t BgL_msg1086z00_5505,
		obj_t BgL_obj1087z00_5506)
	{
		{	/* Llib/object.scm 255 */
			{	/* Llib/object.scm 255 */
				BgL_z62iozd2writezd2errorz62_bglt BgL_new1247z00_6150;

				{	/* Llib/object.scm 255 */
					BgL_z62iozd2writezd2errorz62_bglt BgL_new1246z00_6151;

					BgL_new1246z00_6151 =
						((BgL_z62iozd2writezd2errorz62_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_z62iozd2writezd2errorz62_bgl))));
					{	/* Llib/object.scm 255 */
						long BgL_arg1994z00_6152;

						BgL_arg1994z00_6152 =
							BGL_CLASS_NUM(BGl_z62iozd2writezd2errorz62zz__objectz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1246z00_6151), BgL_arg1994z00_6152);
					}
					BgL_new1247z00_6150 = BgL_new1246z00_6151;
				}
				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt) BgL_new1247z00_6150)))->
						BgL_fnamez00) = ((obj_t) BgL_fname1082z00_5501), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1247z00_6150)))->BgL_locationz00) =
					((obj_t) BgL_location1083z00_5502), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1247z00_6150)))->BgL_stackz00) =
					((obj_t) BgL_stack1084z00_5503), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1247z00_6150)))->BgL_procz00) =
					((obj_t) BgL_proc1085z00_5504), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1247z00_6150)))->BgL_msgz00) =
					((obj_t) BgL_msg1086z00_5505), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1247z00_6150)))->BgL_objz00) =
					((obj_t) BgL_obj1087z00_5506), BUNSPEC);
				return BgL_new1247z00_6150;
			}
		}

	}



/* &<@anonymous:1987> */
	obj_t BGl_z62zc3z04anonymousza31987ze3ze5zz__objectz00(obj_t BgL_envz00_5507,
		obj_t BgL_new1080z00_5508)
	{
		{	/* Llib/object.scm 254 */
			{
				BgL_z62iozd2readzd2errorz62_bglt BgL_auxz00_9296;

				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt)
									((BgL_z62iozd2readzd2errorz62_bglt) BgL_new1080z00_5508))))->
						BgL_fnamez00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) (
										(BgL_z62iozd2readzd2errorz62_bglt) BgL_new1080z00_5508))))->
						BgL_locationz00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) (
										(BgL_z62iozd2readzd2errorz62_bglt) BgL_new1080z00_5508))))->
						BgL_stackz00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62iozd2readzd2errorz62_bglt) BgL_new1080z00_5508))))->
						BgL_procz00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62iozd2readzd2errorz62_bglt) BgL_new1080z00_5508))))->
						BgL_msgz00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62iozd2readzd2errorz62_bglt) BgL_new1080z00_5508))))->
						BgL_objz00) = ((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_9296 =
					((BgL_z62iozd2readzd2errorz62_bglt) BgL_new1080z00_5508);
				return ((obj_t) BgL_auxz00_9296);
			}
		}

	}



/* &lambda1985 */
	BgL_z62iozd2readzd2errorz62_bglt BGl_z62lambda1985z62zz__objectz00(obj_t
		BgL_envz00_5509)
	{
		{	/* Llib/object.scm 254 */
			{	/* Llib/object.scm 254 */
				BgL_z62iozd2readzd2errorz62_bglt BgL_new1079z00_6154;

				BgL_new1079z00_6154 =
					((BgL_z62iozd2readzd2errorz62_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_z62iozd2readzd2errorz62_bgl))));
				{	/* Llib/object.scm 254 */
					long BgL_arg1986z00_6155;

					BgL_arg1986z00_6155 =
						BGL_CLASS_NUM(BGl_z62iozd2readzd2errorz62zz__objectz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1079z00_6154), BgL_arg1986z00_6155);
				}
				return BgL_new1079z00_6154;
			}
		}

	}



/* &lambda1983 */
	BgL_z62iozd2readzd2errorz62_bglt BGl_z62lambda1983z62zz__objectz00(obj_t
		BgL_envz00_5510, obj_t BgL_fname1073z00_5511,
		obj_t BgL_location1074z00_5512, obj_t BgL_stack1075z00_5513,
		obj_t BgL_proc1076z00_5514, obj_t BgL_msg1077z00_5515,
		obj_t BgL_obj1078z00_5516)
	{
		{	/* Llib/object.scm 254 */
			{	/* Llib/object.scm 254 */
				BgL_z62iozd2readzd2errorz62_bglt BgL_new1245z00_6156;

				{	/* Llib/object.scm 254 */
					BgL_z62iozd2readzd2errorz62_bglt BgL_new1244z00_6157;

					BgL_new1244z00_6157 =
						((BgL_z62iozd2readzd2errorz62_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_z62iozd2readzd2errorz62_bgl))));
					{	/* Llib/object.scm 254 */
						long BgL_arg1984z00_6158;

						BgL_arg1984z00_6158 =
							BGL_CLASS_NUM(BGl_z62iozd2readzd2errorz62zz__objectz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1244z00_6157), BgL_arg1984z00_6158);
					}
					BgL_new1245z00_6156 = BgL_new1244z00_6157;
				}
				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt) BgL_new1245z00_6156)))->
						BgL_fnamez00) = ((obj_t) BgL_fname1073z00_5511), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1245z00_6156)))->BgL_locationz00) =
					((obj_t) BgL_location1074z00_5512), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1245z00_6156)))->BgL_stackz00) =
					((obj_t) BgL_stack1075z00_5513), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1245z00_6156)))->BgL_procz00) =
					((obj_t) BgL_proc1076z00_5514), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1245z00_6156)))->BgL_msgz00) =
					((obj_t) BgL_msg1077z00_5515), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1245z00_6156)))->BgL_objz00) =
					((obj_t) BgL_obj1078z00_5516), BUNSPEC);
				return BgL_new1245z00_6156;
			}
		}

	}



/* &<@anonymous:1977> */
	obj_t BGl_z62zc3z04anonymousza31977ze3ze5zz__objectz00(obj_t BgL_envz00_5517,
		obj_t BgL_new1071z00_5518)
	{
		{	/* Llib/object.scm 253 */
			{
				BgL_z62iozd2portzd2errorz62_bglt BgL_auxz00_9337;

				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt)
									((BgL_z62iozd2portzd2errorz62_bglt) BgL_new1071z00_5518))))->
						BgL_fnamez00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) (
										(BgL_z62iozd2portzd2errorz62_bglt) BgL_new1071z00_5518))))->
						BgL_locationz00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) (
										(BgL_z62iozd2portzd2errorz62_bglt) BgL_new1071z00_5518))))->
						BgL_stackz00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62iozd2portzd2errorz62_bglt) BgL_new1071z00_5518))))->
						BgL_procz00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62iozd2portzd2errorz62_bglt) BgL_new1071z00_5518))))->
						BgL_msgz00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62iozd2portzd2errorz62_bglt) BgL_new1071z00_5518))))->
						BgL_objz00) = ((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_9337 =
					((BgL_z62iozd2portzd2errorz62_bglt) BgL_new1071z00_5518);
				return ((obj_t) BgL_auxz00_9337);
			}
		}

	}



/* &lambda1975 */
	BgL_z62iozd2portzd2errorz62_bglt BGl_z62lambda1975z62zz__objectz00(obj_t
		BgL_envz00_5519)
	{
		{	/* Llib/object.scm 253 */
			{	/* Llib/object.scm 253 */
				BgL_z62iozd2portzd2errorz62_bglt BgL_new1070z00_6160;

				BgL_new1070z00_6160 =
					((BgL_z62iozd2portzd2errorz62_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_z62iozd2portzd2errorz62_bgl))));
				{	/* Llib/object.scm 253 */
					long BgL_arg1976z00_6161;

					BgL_arg1976z00_6161 =
						BGL_CLASS_NUM(BGl_z62iozd2portzd2errorz62zz__objectz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1070z00_6160), BgL_arg1976z00_6161);
				}
				return BgL_new1070z00_6160;
			}
		}

	}



/* &lambda1973 */
	BgL_z62iozd2portzd2errorz62_bglt BGl_z62lambda1973z62zz__objectz00(obj_t
		BgL_envz00_5520, obj_t BgL_fname1064z00_5521,
		obj_t BgL_location1065z00_5522, obj_t BgL_stack1066z00_5523,
		obj_t BgL_proc1067z00_5524, obj_t BgL_msg1068z00_5525,
		obj_t BgL_obj1069z00_5526)
	{
		{	/* Llib/object.scm 253 */
			{	/* Llib/object.scm 253 */
				BgL_z62iozd2portzd2errorz62_bglt BgL_new1243z00_6162;

				{	/* Llib/object.scm 253 */
					BgL_z62iozd2portzd2errorz62_bglt BgL_new1242z00_6163;

					BgL_new1242z00_6163 =
						((BgL_z62iozd2portzd2errorz62_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_z62iozd2portzd2errorz62_bgl))));
					{	/* Llib/object.scm 253 */
						long BgL_arg1974z00_6164;

						BgL_arg1974z00_6164 =
							BGL_CLASS_NUM(BGl_z62iozd2portzd2errorz62zz__objectz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1242z00_6163), BgL_arg1974z00_6164);
					}
					BgL_new1243z00_6162 = BgL_new1242z00_6163;
				}
				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt) BgL_new1243z00_6162)))->
						BgL_fnamez00) = ((obj_t) BgL_fname1064z00_5521), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1243z00_6162)))->BgL_locationz00) =
					((obj_t) BgL_location1065z00_5522), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1243z00_6162)))->BgL_stackz00) =
					((obj_t) BgL_stack1066z00_5523), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1243z00_6162)))->BgL_procz00) =
					((obj_t) BgL_proc1067z00_5524), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1243z00_6162)))->BgL_msgz00) =
					((obj_t) BgL_msg1068z00_5525), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1243z00_6162)))->BgL_objz00) =
					((obj_t) BgL_obj1069z00_5526), BUNSPEC);
				return BgL_new1243z00_6162;
			}
		}

	}



/* &<@anonymous:1967> */
	obj_t BGl_z62zc3z04anonymousza31967ze3ze5zz__objectz00(obj_t BgL_envz00_5527,
		obj_t BgL_new1062z00_5528)
	{
		{	/* Llib/object.scm 252 */
			{
				BgL_z62iozd2errorzb0_bglt BgL_auxz00_9378;

				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt)
									((BgL_z62iozd2errorzb0_bglt) BgL_new1062z00_5528))))->
						BgL_fnamez00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) ((BgL_z62iozd2errorzb0_bglt)
										BgL_new1062z00_5528))))->BgL_locationz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) ((BgL_z62iozd2errorzb0_bglt)
										BgL_new1062z00_5528))))->BgL_stackz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) ((BgL_z62iozd2errorzb0_bglt)
										BgL_new1062z00_5528))))->BgL_procz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) ((BgL_z62iozd2errorzb0_bglt)
										BgL_new1062z00_5528))))->BgL_msgz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) ((BgL_z62iozd2errorzb0_bglt)
										BgL_new1062z00_5528))))->BgL_objz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_9378 = ((BgL_z62iozd2errorzb0_bglt) BgL_new1062z00_5528);
				return ((obj_t) BgL_auxz00_9378);
			}
		}

	}



/* &lambda1965 */
	BgL_z62iozd2errorzb0_bglt BGl_z62lambda1965z62zz__objectz00(obj_t
		BgL_envz00_5529)
	{
		{	/* Llib/object.scm 252 */
			{	/* Llib/object.scm 252 */
				BgL_z62iozd2errorzb0_bglt BgL_new1061z00_6166;

				BgL_new1061z00_6166 =
					((BgL_z62iozd2errorzb0_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_z62iozd2errorzb0_bgl))));
				{	/* Llib/object.scm 252 */
					long BgL_arg1966z00_6167;

					BgL_arg1966z00_6167 =
						BGL_CLASS_NUM(BGl_z62iozd2errorzb0zz__objectz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1061z00_6166), BgL_arg1966z00_6167);
				}
				return BgL_new1061z00_6166;
			}
		}

	}



/* &lambda1963 */
	BgL_z62iozd2errorzb0_bglt BGl_z62lambda1963z62zz__objectz00(obj_t
		BgL_envz00_5530, obj_t BgL_fname1054z00_5531,
		obj_t BgL_location1055z00_5532, obj_t BgL_stack1056z00_5533,
		obj_t BgL_proc1057z00_5534, obj_t BgL_msg1058z00_5535,
		obj_t BgL_obj1059z00_5536)
	{
		{	/* Llib/object.scm 252 */
			{	/* Llib/object.scm 252 */
				BgL_z62iozd2errorzb0_bglt BgL_new1240z00_6168;

				{	/* Llib/object.scm 252 */
					BgL_z62iozd2errorzb0_bglt BgL_new1239z00_6169;

					BgL_new1239z00_6169 =
						((BgL_z62iozd2errorzb0_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_z62iozd2errorzb0_bgl))));
					{	/* Llib/object.scm 252 */
						long BgL_arg1964z00_6170;

						BgL_arg1964z00_6170 =
							BGL_CLASS_NUM(BGl_z62iozd2errorzb0zz__objectz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1239z00_6169), BgL_arg1964z00_6170);
					}
					BgL_new1240z00_6168 = BgL_new1239z00_6169;
				}
				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt) BgL_new1240z00_6168)))->
						BgL_fnamez00) = ((obj_t) BgL_fname1054z00_5531), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1240z00_6168)))->BgL_locationz00) =
					((obj_t) BgL_location1055z00_5532), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1240z00_6168)))->BgL_stackz00) =
					((obj_t) BgL_stack1056z00_5533), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1240z00_6168)))->BgL_procz00) =
					((obj_t) BgL_proc1057z00_5534), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1240z00_6168)))->BgL_msgz00) =
					((obj_t) BgL_msg1058z00_5535), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1240z00_6168)))->BgL_objz00) =
					((obj_t) BgL_obj1059z00_5536), BUNSPEC);
				return BgL_new1240z00_6168;
			}
		}

	}



/* &<@anonymous:1952> */
	obj_t BGl_z62zc3z04anonymousza31952ze3ze5zz__objectz00(obj_t BgL_envz00_5537,
		obj_t BgL_new1052z00_5538)
	{
		{	/* Llib/object.scm 250 */
			{
				BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt BgL_auxz00_9419;

				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt)
									((BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt)
										BgL_new1052z00_5538))))->BgL_fnamez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) (
										(BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt)
										BgL_new1052z00_5538))))->BgL_locationz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) (
										(BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt)
										BgL_new1052z00_5538))))->BgL_stackz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt)
										BgL_new1052z00_5538))))->BgL_procz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt)
										BgL_new1052z00_5538))))->BgL_msgz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt)
										BgL_new1052z00_5538))))->BgL_objz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt)
							COBJECT(((BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt)
									BgL_new1052z00_5538)))->BgL_indexz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_9419 =
					((BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt)
					BgL_new1052z00_5538);
				return ((obj_t) BgL_auxz00_9419);
			}
		}

	}



/* &lambda1950 */
	BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt
		BGl_z62lambda1950z62zz__objectz00(obj_t BgL_envz00_5539)
	{
		{	/* Llib/object.scm 250 */
			{	/* Llib/object.scm 250 */
				BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt BgL_new1051z00_6172;

				BgL_new1051z00_6172 =
					((BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt)
					BOBJECT(GC_MALLOC(sizeof(struct
								BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bgl))));
				{	/* Llib/object.scm 250 */
					long BgL_arg1951z00_6173;

					BgL_arg1951z00_6173 =
						BGL_CLASS_NUM
						(BGl_z62indexzd2outzd2ofzd2boundszd2errorz62zz__objectz00);
					BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) BgL_new1051z00_6172),
						BgL_arg1951z00_6173);
				}
				return BgL_new1051z00_6172;
			}
		}

	}



/* &lambda1948 */
	BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt
		BGl_z62lambda1948z62zz__objectz00(obj_t BgL_envz00_5540,
		obj_t BgL_fname1044z00_5541, obj_t BgL_location1045z00_5542,
		obj_t BgL_stack1046z00_5543, obj_t BgL_proc1047z00_5544,
		obj_t BgL_msg1048z00_5545, obj_t BgL_obj1049z00_5546,
		obj_t BgL_index1050z00_5547)
	{
		{	/* Llib/object.scm 250 */
			{	/* Llib/object.scm 250 */
				BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt BgL_new1238z00_6174;

				{	/* Llib/object.scm 250 */
					BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt BgL_new1237z00_6175;

					BgL_new1237z00_6175 =
						((BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt)
						BOBJECT(GC_MALLOC(sizeof(struct
									BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bgl))));
					{	/* Llib/object.scm 250 */
						long BgL_arg1949z00_6176;

						BgL_arg1949z00_6176 =
							BGL_CLASS_NUM
							(BGl_z62indexzd2outzd2ofzd2boundszd2errorz62zz__objectz00);
						BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) BgL_new1237z00_6175),
							BgL_arg1949z00_6176);
					}
					BgL_new1238z00_6174 = BgL_new1237z00_6175;
				}
				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt) BgL_new1238z00_6174)))->
						BgL_fnamez00) = ((obj_t) BgL_fname1044z00_5541), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1238z00_6174)))->BgL_locationz00) =
					((obj_t) BgL_location1045z00_5542), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1238z00_6174)))->BgL_stackz00) =
					((obj_t) BgL_stack1046z00_5543), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1238z00_6174)))->BgL_procz00) =
					((obj_t) BgL_proc1047z00_5544), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1238z00_6174)))->BgL_msgz00) =
					((obj_t) BgL_msg1048z00_5545), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1238z00_6174)))->BgL_objz00) =
					((obj_t) BgL_obj1049z00_5546), BUNSPEC);
				((((BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt)
							COBJECT(BgL_new1238z00_6174))->BgL_indexz00) =
					((obj_t) BgL_index1050z00_5547), BUNSPEC);
				return BgL_new1238z00_6174;
			}
		}

	}



/* &lambda1957 */
	obj_t BGl_z62lambda1957z62zz__objectz00(obj_t BgL_envz00_5548,
		obj_t BgL_oz00_5549, obj_t BgL_vz00_5550)
	{
		{	/* Llib/object.scm 250 */
			return
				((((BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt) COBJECT(
							((BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt)
								BgL_oz00_5549)))->BgL_indexz00) =
				((obj_t) BgL_vz00_5550), BUNSPEC);
		}

	}



/* &lambda1956 */
	obj_t BGl_z62lambda1956z62zz__objectz00(obj_t BgL_envz00_5551,
		obj_t BgL_oz00_5552)
	{
		{	/* Llib/object.scm 250 */
			return
				(((BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt) COBJECT(
						((BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt)
							BgL_oz00_5552)))->BgL_indexz00);
		}

	}



/* &<@anonymous:1937> */
	obj_t BGl_z62zc3z04anonymousza31937ze3ze5zz__objectz00(obj_t BgL_envz00_5553,
		obj_t BgL_new1042z00_5554)
	{
		{	/* Llib/object.scm 248 */
			{
				BgL_z62typezd2errorzb0_bglt BgL_auxz00_9467;

				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt)
									((BgL_z62typezd2errorzb0_bglt) BgL_new1042z00_5554))))->
						BgL_fnamez00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) ((BgL_z62typezd2errorzb0_bglt)
										BgL_new1042z00_5554))))->BgL_locationz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) ((BgL_z62typezd2errorzb0_bglt)
										BgL_new1042z00_5554))))->BgL_stackz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) ((BgL_z62typezd2errorzb0_bglt)
										BgL_new1042z00_5554))))->BgL_procz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) ((BgL_z62typezd2errorzb0_bglt)
										BgL_new1042z00_5554))))->BgL_msgz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) ((BgL_z62typezd2errorzb0_bglt)
										BgL_new1042z00_5554))))->BgL_objz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62typezd2errorzb0_bglt) COBJECT(((BgL_z62typezd2errorzb0_bglt)
									BgL_new1042z00_5554)))->BgL_typez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_9467 = ((BgL_z62typezd2errorzb0_bglt) BgL_new1042z00_5554);
				return ((obj_t) BgL_auxz00_9467);
			}
		}

	}



/* &lambda1935 */
	BgL_z62typezd2errorzb0_bglt BGl_z62lambda1935z62zz__objectz00(obj_t
		BgL_envz00_5555)
	{
		{	/* Llib/object.scm 248 */
			{	/* Llib/object.scm 248 */
				BgL_z62typezd2errorzb0_bglt BgL_new1041z00_6180;

				BgL_new1041z00_6180 =
					((BgL_z62typezd2errorzb0_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_z62typezd2errorzb0_bgl))));
				{	/* Llib/object.scm 248 */
					long BgL_arg1936z00_6181;

					BgL_arg1936z00_6181 =
						BGL_CLASS_NUM(BGl_z62typezd2errorzb0zz__objectz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1041z00_6180), BgL_arg1936z00_6181);
				}
				return BgL_new1041z00_6180;
			}
		}

	}



/* &lambda1933 */
	BgL_z62typezd2errorzb0_bglt BGl_z62lambda1933z62zz__objectz00(obj_t
		BgL_envz00_5556, obj_t BgL_fname1034z00_5557,
		obj_t BgL_location1035z00_5558, obj_t BgL_stack1036z00_5559,
		obj_t BgL_proc1037z00_5560, obj_t BgL_msg1038z00_5561,
		obj_t BgL_obj1039z00_5562, obj_t BgL_type1040z00_5563)
	{
		{	/* Llib/object.scm 248 */
			{	/* Llib/object.scm 248 */
				BgL_z62typezd2errorzb0_bglt BgL_new1236z00_6182;

				{	/* Llib/object.scm 248 */
					BgL_z62typezd2errorzb0_bglt BgL_new1235z00_6183;

					BgL_new1235z00_6183 =
						((BgL_z62typezd2errorzb0_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_z62typezd2errorzb0_bgl))));
					{	/* Llib/object.scm 248 */
						long BgL_arg1934z00_6184;

						BgL_arg1934z00_6184 =
							BGL_CLASS_NUM(BGl_z62typezd2errorzb0zz__objectz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1235z00_6183), BgL_arg1934z00_6184);
					}
					BgL_new1236z00_6182 = BgL_new1235z00_6183;
				}
				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt) BgL_new1236z00_6182)))->
						BgL_fnamez00) = ((obj_t) BgL_fname1034z00_5557), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1236z00_6182)))->BgL_locationz00) =
					((obj_t) BgL_location1035z00_5558), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1236z00_6182)))->BgL_stackz00) =
					((obj_t) BgL_stack1036z00_5559), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1236z00_6182)))->BgL_procz00) =
					((obj_t) BgL_proc1037z00_5560), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1236z00_6182)))->BgL_msgz00) =
					((obj_t) BgL_msg1038z00_5561), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1236z00_6182)))->BgL_objz00) =
					((obj_t) BgL_obj1039z00_5562), BUNSPEC);
				((((BgL_z62typezd2errorzb0_bglt) COBJECT(BgL_new1236z00_6182))->
						BgL_typez00) = ((obj_t) BgL_type1040z00_5563), BUNSPEC);
				return BgL_new1236z00_6182;
			}
		}

	}



/* &lambda1942 */
	obj_t BGl_z62lambda1942z62zz__objectz00(obj_t BgL_envz00_5564,
		obj_t BgL_oz00_5565, obj_t BgL_vz00_5566)
	{
		{	/* Llib/object.scm 248 */
			return
				((((BgL_z62typezd2errorzb0_bglt) COBJECT(
							((BgL_z62typezd2errorzb0_bglt) BgL_oz00_5565)))->BgL_typez00) =
				((obj_t) BgL_vz00_5566), BUNSPEC);
		}

	}



/* &lambda1941 */
	obj_t BGl_z62lambda1941z62zz__objectz00(obj_t BgL_envz00_5567,
		obj_t BgL_oz00_5568)
	{
		{	/* Llib/object.scm 248 */
			return
				(((BgL_z62typezd2errorzb0_bglt) COBJECT(
						((BgL_z62typezd2errorzb0_bglt) BgL_oz00_5568)))->BgL_typez00);
		}

	}



/* &<@anonymous:1907> */
	obj_t BGl_z62zc3z04anonymousza31907ze3ze5zz__objectz00(obj_t BgL_envz00_5569,
		obj_t BgL_new1032z00_5570)
	{
		{	/* Llib/object.scm 244 */
			{
				BgL_z62errorz62_bglt BgL_auxz00_9515;

				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt)
									((BgL_z62errorz62_bglt) BgL_new1032z00_5570))))->
						BgL_fnamez00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) ((BgL_z62errorz62_bglt)
										BgL_new1032z00_5570))))->BgL_locationz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) ((BgL_z62errorz62_bglt)
										BgL_new1032z00_5570))))->BgL_stackz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1032z00_5570)))->BgL_procz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1032z00_5570)))->BgL_msgz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1032z00_5570)))->BgL_objz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_9515 = ((BgL_z62errorz62_bglt) BgL_new1032z00_5570);
				return ((obj_t) BgL_auxz00_9515);
			}
		}

	}



/* &lambda1904 */
	BgL_z62errorz62_bglt BGl_z62lambda1904z62zz__objectz00(obj_t BgL_envz00_5571)
	{
		{	/* Llib/object.scm 244 */
			{	/* Llib/object.scm 244 */
				BgL_z62errorz62_bglt BgL_new1031z00_6188;

				BgL_new1031z00_6188 =
					((BgL_z62errorz62_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_z62errorz62_bgl))));
				{	/* Llib/object.scm 244 */
					long BgL_arg1906z00_6189;

					BgL_arg1906z00_6189 = BGL_CLASS_NUM(BGl_z62errorz62zz__objectz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1031z00_6188), BgL_arg1906z00_6189);
				}
				return BgL_new1031z00_6188;
			}
		}

	}



/* &lambda1902 */
	BgL_z62errorz62_bglt BGl_z62lambda1902z62zz__objectz00(obj_t BgL_envz00_5572,
		obj_t BgL_fname1025z00_5573, obj_t BgL_location1026z00_5574,
		obj_t BgL_stack1027z00_5575, obj_t BgL_proc1028z00_5576,
		obj_t BgL_msg1029z00_5577, obj_t BgL_obj1030z00_5578)
	{
		{	/* Llib/object.scm 244 */
			{	/* Llib/object.scm 244 */
				BgL_z62errorz62_bglt BgL_new1234z00_6190;

				{	/* Llib/object.scm 244 */
					BgL_z62errorz62_bglt BgL_new1233z00_6191;

					BgL_new1233z00_6191 =
						((BgL_z62errorz62_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_z62errorz62_bgl))));
					{	/* Llib/object.scm 244 */
						long BgL_arg1903z00_6192;

						BgL_arg1903z00_6192 = BGL_CLASS_NUM(BGl_z62errorz62zz__objectz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1233z00_6191), BgL_arg1903z00_6192);
					}
					BgL_new1234z00_6190 = BgL_new1233z00_6191;
				}
				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt) BgL_new1234z00_6190)))->
						BgL_fnamez00) = ((obj_t) BgL_fname1025z00_5573), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1234z00_6190)))->BgL_locationz00) =
					((obj_t) BgL_location1026z00_5574), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1234z00_6190)))->BgL_stackz00) =
					((obj_t) BgL_stack1027z00_5575), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(BgL_new1234z00_6190))->BgL_procz00) =
					((obj_t) BgL_proc1028z00_5576), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(BgL_new1234z00_6190))->BgL_msgz00) =
					((obj_t) BgL_msg1029z00_5577), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(BgL_new1234z00_6190))->BgL_objz00) =
					((obj_t) BgL_obj1030z00_5578), BUNSPEC);
				return BgL_new1234z00_6190;
			}
		}

	}



/* &lambda1927 */
	obj_t BGl_z62lambda1927z62zz__objectz00(obj_t BgL_envz00_5579,
		obj_t BgL_oz00_5580, obj_t BgL_vz00_5581)
	{
		{	/* Llib/object.scm 244 */
			return
				((((BgL_z62errorz62_bglt) COBJECT(
							((BgL_z62errorz62_bglt) BgL_oz00_5580)))->BgL_objz00) =
				((obj_t) BgL_vz00_5581), BUNSPEC);
		}

	}



/* &lambda1926 */
	obj_t BGl_z62lambda1926z62zz__objectz00(obj_t BgL_envz00_5582,
		obj_t BgL_oz00_5583)
	{
		{	/* Llib/object.scm 244 */
			return
				(((BgL_z62errorz62_bglt) COBJECT(
						((BgL_z62errorz62_bglt) BgL_oz00_5583)))->BgL_objz00);
		}

	}



/* &lambda1920 */
	obj_t BGl_z62lambda1920z62zz__objectz00(obj_t BgL_envz00_5584,
		obj_t BgL_oz00_5585, obj_t BgL_vz00_5586)
	{
		{	/* Llib/object.scm 244 */
			return
				((((BgL_z62errorz62_bglt) COBJECT(
							((BgL_z62errorz62_bglt) BgL_oz00_5585)))->BgL_msgz00) =
				((obj_t) BgL_vz00_5586), BUNSPEC);
		}

	}



/* &lambda1919 */
	obj_t BGl_z62lambda1919z62zz__objectz00(obj_t BgL_envz00_5587,
		obj_t BgL_oz00_5588)
	{
		{	/* Llib/object.scm 244 */
			return
				(((BgL_z62errorz62_bglt) COBJECT(
						((BgL_z62errorz62_bglt) BgL_oz00_5588)))->BgL_msgz00);
		}

	}



/* &lambda1914 */
	obj_t BGl_z62lambda1914z62zz__objectz00(obj_t BgL_envz00_5589,
		obj_t BgL_oz00_5590, obj_t BgL_vz00_5591)
	{
		{	/* Llib/object.scm 244 */
			return
				((((BgL_z62errorz62_bglt) COBJECT(
							((BgL_z62errorz62_bglt) BgL_oz00_5590)))->BgL_procz00) =
				((obj_t) BgL_vz00_5591), BUNSPEC);
		}

	}



/* &lambda1913 */
	obj_t BGl_z62lambda1913z62zz__objectz00(obj_t BgL_envz00_5592,
		obj_t BgL_oz00_5593)
	{
		{	/* Llib/object.scm 244 */
			return
				(((BgL_z62errorz62_bglt) COBJECT(
						((BgL_z62errorz62_bglt) BgL_oz00_5593)))->BgL_procz00);
		}

	}



/* &<@anonymous:1873> */
	obj_t BGl_z62zc3z04anonymousza31873ze3ze5zz__objectz00(obj_t BgL_envz00_5594,
		obj_t BgL_new1023z00_5595)
	{
		{	/* Llib/object.scm 239 */
			{
				BgL_z62exceptionz62_bglt BgL_auxz00_9562;

				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt) BgL_new1023z00_5595)))->
						BgL_fnamez00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1023z00_5595)))->BgL_locationz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1023z00_5595)))->BgL_stackz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_9562 = ((BgL_z62exceptionz62_bglt) BgL_new1023z00_5595);
				return ((obj_t) BgL_auxz00_9562);
			}
		}

	}



/* &lambda1871 */
	BgL_z62exceptionz62_bglt BGl_z62lambda1871z62zz__objectz00(obj_t
		BgL_envz00_5596)
	{
		{	/* Llib/object.scm 239 */
			{	/* Llib/object.scm 239 */
				BgL_z62exceptionz62_bglt BgL_new1022z00_6200;

				BgL_new1022z00_6200 =
					((BgL_z62exceptionz62_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_z62exceptionz62_bgl))));
				{	/* Llib/object.scm 239 */
					long BgL_arg1872z00_6201;

					BgL_arg1872z00_6201 = BGL_CLASS_NUM(BGl_z62exceptionz62zz__objectz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1022z00_6200), BgL_arg1872z00_6201);
				}
				return BgL_new1022z00_6200;
			}
		}

	}



/* &lambda1869 */
	BgL_z62exceptionz62_bglt BGl_z62lambda1869z62zz__objectz00(obj_t
		BgL_envz00_5597, obj_t BgL_fname1019z00_5598,
		obj_t BgL_location1020z00_5599, obj_t BgL_stack1021z00_5600)
	{
		{	/* Llib/object.scm 239 */
			{	/* Llib/object.scm 239 */
				BgL_z62exceptionz62_bglt BgL_new1232z00_6202;

				{	/* Llib/object.scm 239 */
					BgL_z62exceptionz62_bglt BgL_new1231z00_6203;

					BgL_new1231z00_6203 =
						((BgL_z62exceptionz62_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_z62exceptionz62_bgl))));
					{	/* Llib/object.scm 239 */
						long BgL_arg1870z00_6204;

						BgL_arg1870z00_6204 =
							BGL_CLASS_NUM(BGl_z62exceptionz62zz__objectz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1231z00_6203), BgL_arg1870z00_6204);
					}
					BgL_new1232z00_6202 = BgL_new1231z00_6203;
				}
				((((BgL_z62exceptionz62_bglt) COBJECT(BgL_new1232z00_6202))->
						BgL_fnamez00) = ((obj_t) BgL_fname1019z00_5598), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(BgL_new1232z00_6202))->
						BgL_locationz00) = ((obj_t) BgL_location1020z00_5599), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(BgL_new1232z00_6202))->
						BgL_stackz00) = ((obj_t) BgL_stack1021z00_5600), BUNSPEC);
				return BgL_new1232z00_6202;
			}
		}

	}



/* &<@anonymous:1895> */
	obj_t BGl_z62zc3z04anonymousza31895ze3ze5zz__objectz00(obj_t BgL_envz00_5601)
	{
		{	/* Llib/object.scm 239 */
			{	/* Llib/object.scm 242 */

				{	/* Llib/object.scm 242 */

					return BGl_getzd2tracezd2stackz00zz__errorz00(BFALSE);
				}
			}
		}

	}



/* &lambda1894 */
	obj_t BGl_z62lambda1894z62zz__objectz00(obj_t BgL_envz00_5602,
		obj_t BgL_oz00_5603, obj_t BgL_vz00_5604)
	{
		{	/* Llib/object.scm 239 */
			return
				((((BgL_z62exceptionz62_bglt) COBJECT(
							((BgL_z62exceptionz62_bglt) BgL_oz00_5603)))->BgL_stackz00) =
				((obj_t) BgL_vz00_5604), BUNSPEC);
		}

	}



/* &lambda1893 */
	obj_t BGl_z62lambda1893z62zz__objectz00(obj_t BgL_envz00_5605,
		obj_t BgL_oz00_5606)
	{
		{	/* Llib/object.scm 239 */
			return
				(((BgL_z62exceptionz62_bglt) COBJECT(
						((BgL_z62exceptionz62_bglt) BgL_oz00_5606)))->BgL_stackz00);
		}

	}



/* &<@anonymous:1888> */
	obj_t BGl_z62zc3z04anonymousza31888ze3ze5zz__objectz00(obj_t BgL_envz00_5607)
	{
		{	/* Llib/object.scm 239 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1887 */
	obj_t BGl_z62lambda1887z62zz__objectz00(obj_t BgL_envz00_5608,
		obj_t BgL_oz00_5609, obj_t BgL_vz00_5610)
	{
		{	/* Llib/object.scm 239 */
			return
				((((BgL_z62exceptionz62_bglt) COBJECT(
							((BgL_z62exceptionz62_bglt) BgL_oz00_5609)))->BgL_locationz00) =
				((obj_t) BgL_vz00_5610), BUNSPEC);
		}

	}



/* &lambda1886 */
	obj_t BGl_z62lambda1886z62zz__objectz00(obj_t BgL_envz00_5611,
		obj_t BgL_oz00_5612)
	{
		{	/* Llib/object.scm 239 */
			return
				(((BgL_z62exceptionz62_bglt) COBJECT(
						((BgL_z62exceptionz62_bglt) BgL_oz00_5612)))->BgL_locationz00);
		}

	}



/* &<@anonymous:1880> */
	obj_t BGl_z62zc3z04anonymousza31880ze3ze5zz__objectz00(obj_t BgL_envz00_5613)
	{
		{	/* Llib/object.scm 239 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1879 */
	obj_t BGl_z62lambda1879z62zz__objectz00(obj_t BgL_envz00_5614,
		obj_t BgL_oz00_5615, obj_t BgL_vz00_5616)
	{
		{	/* Llib/object.scm 239 */
			return
				((((BgL_z62exceptionz62_bglt) COBJECT(
							((BgL_z62exceptionz62_bglt) BgL_oz00_5615)))->BgL_fnamez00) =
				((obj_t) BgL_vz00_5616), BUNSPEC);
		}

	}



/* &lambda1878 */
	obj_t BGl_z62lambda1878z62zz__objectz00(obj_t BgL_envz00_5617,
		obj_t BgL_oz00_5618)
	{
		{	/* Llib/object.scm 239 */
			return
				(((BgL_z62exceptionz62_bglt) COBJECT(
						((BgL_z62exceptionz62_bglt) BgL_oz00_5618)))->BgL_fnamez00);
		}

	}



/* &<@anonymous:1861> */
	obj_t BGl_z62zc3z04anonymousza31861ze3ze5zz__objectz00(obj_t BgL_envz00_5619,
		obj_t BgL_new1017z00_5620)
	{
		{	/* Llib/object.scm 237 */
			return ((obj_t) ((BgL_z62conditionz62_bglt) BgL_new1017z00_5620));
		}

	}



/* &lambda1859 */
	BgL_z62conditionz62_bglt BGl_z62lambda1859z62zz__objectz00(obj_t
		BgL_envz00_5621)
	{
		{	/* Llib/object.scm 237 */
			{	/* Llib/object.scm 237 */
				BgL_z62conditionz62_bglt BgL_new1016z00_6212;

				BgL_new1016z00_6212 =
					((BgL_z62conditionz62_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_z62conditionz62_bgl))));
				{	/* Llib/object.scm 237 */
					long BgL_arg1860z00_6213;

					BgL_arg1860z00_6213 = BGL_CLASS_NUM(BGl_z62conditionz62zz__objectz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1016z00_6212), BgL_arg1860z00_6213);
				}
				return BgL_new1016z00_6212;
			}
		}

	}



/* &lambda1857 */
	BgL_z62conditionz62_bglt BGl_z62lambda1857z62zz__objectz00(obj_t
		BgL_envz00_5622)
	{
		{	/* Llib/object.scm 237 */
			{	/* Llib/object.scm 237 */
				BgL_z62conditionz62_bglt BgL_new1230z00_6214;

				{	/* Llib/object.scm 237 */
					BgL_z62conditionz62_bglt BgL_new1229z00_6215;

					BgL_new1229z00_6215 =
						((BgL_z62conditionz62_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_z62conditionz62_bgl))));
					{	/* Llib/object.scm 237 */
						long BgL_arg1858z00_6216;

						BgL_arg1858z00_6216 =
							BGL_CLASS_NUM(BGl_z62conditionz62zz__objectz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1229z00_6215), BgL_arg1858z00_6216);
					}
					BgL_new1230z00_6214 = BgL_new1229z00_6215;
				}
				return BgL_new1230z00_6214;
			}
		}

	}



/* &<@anonymous:1850> */
	obj_t BGl_z62zc3z04anonymousza31850ze3ze5zz__objectz00(obj_t BgL_envz00_5623,
		obj_t BgL_new1014z00_5624)
	{
		{	/* Llib/object.scm 236 */
			return ((obj_t) ((BgL_objectz00_bglt) BgL_new1014z00_5624));
		}

	}



/* &lambda1848 */
	BgL_objectz00_bglt BGl_z62lambda1848z62zz__objectz00(obj_t BgL_envz00_5625)
	{
		{	/* Llib/object.scm 236 */
			{	/* Llib/object.scm 236 */
				BgL_objectz00_bglt BgL_new1013z00_6218;

				BgL_new1013z00_6218 =
					((BgL_objectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_objectz00_bgl))));
				{	/* Llib/object.scm 236 */
					long BgL_arg1849z00_6219;

					BgL_arg1849z00_6219 = BGL_CLASS_NUM(BGl_objectz00zz__objectz00);
					BGL_OBJECT_CLASS_NUM_SET(BgL_new1013z00_6218, BgL_arg1849z00_6219);
				}
				return BgL_new1013z00_6218;
			}
		}

	}



/* &lambda1846 */
	BgL_objectz00_bglt BGl_z62lambda1846z62zz__objectz00(obj_t BgL_envz00_5626)
	{
		{	/* Llib/object.scm 236 */
			{	/* Llib/object.scm 236 */
				BgL_objectz00_bglt BgL_new1228z00_6220;

				{	/* Llib/object.scm 236 */
					BgL_objectz00_bglt BgL_new1226z00_6221;

					BgL_new1226z00_6221 =
						((BgL_objectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_objectz00_bgl))));
					{	/* Llib/object.scm 236 */
						long BgL_arg1847z00_6222;

						BgL_arg1847z00_6222 = BGL_CLASS_NUM(BGl_objectz00zz__objectz00);
						BGL_OBJECT_CLASS_NUM_SET(BgL_new1226z00_6221, BgL_arg1847z00_6222);
					}
					BgL_new1228z00_6220 = BgL_new1226z00_6221;
				}
				return BgL_new1228z00_6220;
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__objectz00(void)
	{
		{	/* Llib/object.scm 17 */
			{	/* Llib/object.scm 17 */
				obj_t BgL_classzd2minzd2_4400;

				BgL_classzd2minzd2_4400 = BGl_objectz00zz__objectz00;
				{	/* Llib/object.scm 1156 */
					obj_t BgL_top3721z00_9616;

					BgL_top3721z00_9616 = BGL_EXITD_TOP_AS_OBJ();
					BGL_MUTEX_LOCK(bigloo_generic_mutex);
					BGL_EXITD_PUSH_PROTECT(BgL_top3721z00_9616, bigloo_generic_mutex);
					BUNSPEC;
					{	/* Llib/object.scm 1156 */
						obj_t BgL_tmp3720z00_9615;

						BgL_tmp3720z00_9615 =
							BGl_registerzd2genericzd2sanszd2lockz12zc0zz__objectz00
							(BGl_objectzd2displayzd2envz00zz__objectz00,
							BGl_proc3138z00zz__objectz00, BGl_string3139z00zz__objectz00);
						BGL_EXITD_POP_PROTECT(BgL_top3721z00_9616);
						BUNSPEC;
						BGL_MUTEX_UNLOCK(bigloo_generic_mutex);
						BgL_tmp3720z00_9615;
			}}}
			{	/* Llib/object.scm 17 */
				obj_t BgL_classzd2minzd2_4405;

				BgL_classzd2minzd2_4405 = BGl_objectz00zz__objectz00;
				{	/* Llib/object.scm 1156 */
					obj_t BgL_top3723z00_9624;

					BgL_top3723z00_9624 = BGL_EXITD_TOP_AS_OBJ();
					BGL_MUTEX_LOCK(bigloo_generic_mutex);
					BGL_EXITD_PUSH_PROTECT(BgL_top3723z00_9624, bigloo_generic_mutex);
					BUNSPEC;
					{	/* Llib/object.scm 1156 */
						obj_t BgL_tmp3722z00_9623;

						BgL_tmp3722z00_9623 =
							BGl_registerzd2genericzd2sanszd2lockz12zc0zz__objectz00
							(BGl_objectzd2writezd2envz00zz__objectz00,
							BGl_proc3140z00zz__objectz00, BGl_string3141z00zz__objectz00);
						BGL_EXITD_POP_PROTECT(BgL_top3723z00_9624);
						BUNSPEC;
						BGL_MUTEX_UNLOCK(bigloo_generic_mutex);
						BgL_tmp3722z00_9623;
			}}}
			{	/* Llib/object.scm 17 */
				obj_t BgL_classzd2minzd2_4406;

				BgL_classzd2minzd2_4406 = BGl_objectz00zz__objectz00;
				{	/* Llib/object.scm 1156 */
					obj_t BgL_top3725z00_9632;

					BgL_top3725z00_9632 = BGL_EXITD_TOP_AS_OBJ();
					BGL_MUTEX_LOCK(bigloo_generic_mutex);
					BGL_EXITD_PUSH_PROTECT(BgL_top3725z00_9632, bigloo_generic_mutex);
					BUNSPEC;
					{	/* Llib/object.scm 1156 */
						obj_t BgL_tmp3724z00_9631;

						BgL_tmp3724z00_9631 =
							BGl_registerzd2genericzd2sanszd2lockz12zc0zz__objectz00
							(BGl_objectzd2hashnumberzd2envz00zz__objectz00,
							BGl_proc3142z00zz__objectz00, BGl_string3143z00zz__objectz00);
						BGL_EXITD_POP_PROTECT(BgL_top3725z00_9632);
						BUNSPEC;
						BGL_MUTEX_UNLOCK(bigloo_generic_mutex);
						BgL_tmp3724z00_9631;
			}}}
			{	/* Llib/object.scm 17 */
				obj_t BgL_classzd2minzd2_4470;

				BgL_classzd2minzd2_4470 = BGl_objectz00zz__objectz00;
				{	/* Llib/object.scm 1156 */
					obj_t BgL_top3727z00_9640;

					BgL_top3727z00_9640 = BGL_EXITD_TOP_AS_OBJ();
					BGL_MUTEX_LOCK(bigloo_generic_mutex);
					BGL_EXITD_PUSH_PROTECT(BgL_top3727z00_9640, bigloo_generic_mutex);
					BUNSPEC;
					{	/* Llib/object.scm 1156 */
						obj_t BgL_tmp3726z00_9639;

						BgL_tmp3726z00_9639 =
							BGl_registerzd2genericzd2sanszd2lockz12zc0zz__objectz00
							(BGl_objectzd2printzd2envz00zz__objectz00,
							BGl_proc3144z00zz__objectz00, BGl_string3145z00zz__objectz00);
						BGL_EXITD_POP_PROTECT(BgL_top3727z00_9640);
						BUNSPEC;
						BGL_MUTEX_UNLOCK(bigloo_generic_mutex);
						BgL_tmp3726z00_9639;
			}}}
			{	/* Llib/object.scm 17 */
				obj_t BgL_classzd2minzd2_4508;

				BgL_classzd2minzd2_4508 = BGl_objectz00zz__objectz00;
				{	/* Llib/object.scm 1156 */
					obj_t BgL_top3729z00_9648;

					BgL_top3729z00_9648 = BGL_EXITD_TOP_AS_OBJ();
					BGL_MUTEX_LOCK(bigloo_generic_mutex);
					BGL_EXITD_PUSH_PROTECT(BgL_top3729z00_9648, bigloo_generic_mutex);
					BUNSPEC;
					{	/* Llib/object.scm 1156 */
						obj_t BgL_tmp3728z00_9647;

						BgL_tmp3728z00_9647 =
							BGl_registerzd2genericzd2sanszd2lockz12zc0zz__objectz00
							(BGl_objectzd2equalzf3zd2envzf3zz__objectz00,
							BGl_proc3146z00zz__objectz00, BGl_string3147z00zz__objectz00);
						BGL_EXITD_POP_PROTECT(BgL_top3729z00_9648);
						BUNSPEC;
						BGL_MUTEX_UNLOCK(bigloo_generic_mutex);
						BgL_tmp3728z00_9647;
			}}}
			{	/* Llib/object.scm 1156 */
				obj_t BgL_top3731z00_9656;

				BgL_top3731z00_9656 = BGL_EXITD_TOP_AS_OBJ();
				BGL_MUTEX_LOCK(bigloo_generic_mutex);
				BGL_EXITD_PUSH_PROTECT(BgL_top3731z00_9656, bigloo_generic_mutex);
				BUNSPEC;
				{	/* Llib/object.scm 1156 */
					obj_t BgL_tmp3730z00_9655;

					BgL_tmp3730z00_9655 =
						BGl_registerzd2genericzd2sanszd2lockz12zc0zz__objectz00
						(BGl_exceptionzd2notifyzd2envz00zz__objectz00,
						BGl_proc3148z00zz__objectz00, BGl_string3149z00zz__objectz00);
					BGL_EXITD_POP_PROTECT(BgL_top3731z00_9656);
					BUNSPEC;
					BGL_MUTEX_UNLOCK(bigloo_generic_mutex);
					return BgL_tmp3730z00_9655;
				}
			}
		}

	}



/* &exception-notify1404 */
	obj_t BGl_z62exceptionzd2notify1404zb0zz__objectz00(obj_t BgL_envz00_5639,
		obj_t BgL_excz00_5640)
	{
		{	/* Llib/object.scm 1495 */
			{	/* Llib/object.scm 1496 */
				obj_t BgL_portz00_6223;

				{	/* Llib/object.scm 1496 */
					obj_t BgL_tmpz00_9663;

					BgL_tmpz00_9663 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_portz00_6223 = BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_9663);
				}
				bgl_display_string(BGl_string3150z00zz__objectz00, BgL_portz00_6223);
				BGl_writezd2circlezd2zz__pp_circlez00(BgL_excz00_5640,
					BgL_portz00_6223);
				if (CBOOL(BGl_currentzd2threadzd2zz__threadz00()))
					{	/* Llib/object.scm 1499 */
						bgl_display_string(BGl_string3151z00zz__objectz00,
							BgL_portz00_6223);
						bgl_display_obj(BGl_currentzd2threadzd2zz__threadz00(),
							BgL_portz00_6223);
						bgl_display_string(BGl_string3152z00zz__objectz00,
							BgL_portz00_6223);
					}
				else
					{	/* Llib/object.scm 1499 */
						BFALSE;
					}
				bgl_display_char(((unsigned char) 10), BgL_portz00_6223);
				{	/* Llib/object.scm 1504 */
					obj_t BgL_stackz00_6224;

					{	/* Llib/object.scm 1504 */
						bool_t BgL_test3733z00_9676;

						{	/* Llib/object.scm 1504 */
							obj_t BgL_classz00_6225;

							BgL_classz00_6225 = BGl_z62exceptionz62zz__objectz00;
							if (BGL_OBJECTP(BgL_excz00_5640))
								{	/* Llib/object.scm 1341 */
									BgL_objectz00_bglt BgL_arg1806z00_6226;

									BgL_arg1806z00_6226 = (BgL_objectz00_bglt) (BgL_excz00_5640);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Llib/object.scm 1375 */
											long BgL_idxz00_6227;

											BgL_idxz00_6227 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1806z00_6226);
											BgL_test3733z00_9676 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_6227 + 2L)) == BgL_classz00_6225);
										}
									else
										{	/* Llib/object.scm 1354 */
											bool_t BgL_res2618z00_6230;

											{	/* Llib/object.scm 1362 */
												obj_t BgL_oclassz00_6231;

												{	/* Llib/object.scm 893 */
													obj_t BgL_arg1553z00_6232;
													long BgL_arg1554z00_6233;

													BgL_arg1553z00_6232 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Llib/object.scm 894 */
														long BgL_arg1555z00_6234;

														BgL_arg1555z00_6234 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1806z00_6226);
														BgL_arg1554z00_6233 =
															(BgL_arg1555z00_6234 - OBJECT_TYPE);
													}
													BgL_oclassz00_6231 =
														VECTOR_REF(BgL_arg1553z00_6232,
														BgL_arg1554z00_6233);
												}
												{	/* Llib/object.scm 1363 */
													bool_t BgL__ortest_1220z00_6235;

													BgL__ortest_1220z00_6235 =
														(BgL_classz00_6225 == BgL_oclassz00_6231);
													if (BgL__ortest_1220z00_6235)
														{	/* Llib/object.scm 1363 */
															BgL_res2618z00_6230 = BgL__ortest_1220z00_6235;
														}
													else
														{	/* Llib/object.scm 1364 */
															long BgL_odepthz00_6236;

															{	/* Llib/object.scm 1364 */
																obj_t BgL_arg1810z00_6237;

																BgL_arg1810z00_6237 = (BgL_oclassz00_6231);
																BgL_odepthz00_6236 =
																	BGL_CLASS_DEPTH(BgL_arg1810z00_6237);
															}
															if ((2L < BgL_odepthz00_6236))
																{	/* Llib/object.scm 1366 */
																	obj_t BgL_arg1808z00_6238;

																	{	/* Llib/object.scm 1366 */
																		obj_t BgL_arg1809z00_6239;

																		BgL_arg1809z00_6239 = (BgL_oclassz00_6231);
																		BgL_arg1808z00_6238 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1809z00_6239, 2L);
																	}
																	BgL_res2618z00_6230 =
																		(BgL_arg1808z00_6238 == BgL_classz00_6225);
																}
															else
																{	/* Llib/object.scm 1365 */
																	BgL_res2618z00_6230 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test3733z00_9676 = BgL_res2618z00_6230;
										}
								}
							else
								{	/* Llib/object.scm 1340 */
									BgL_test3733z00_9676 = ((bool_t) 0);
								}
						}
						if (BgL_test3733z00_9676)
							{	/* Llib/object.scm 1506 */
								obj_t BgL__ortest_1224z00_6240;

								BgL__ortest_1224z00_6240 =
									(((BgL_z62exceptionz62_bglt) COBJECT(
											((BgL_z62exceptionz62_bglt) BgL_excz00_5640)))->
									BgL_stackz00);
								if (CBOOL(BgL__ortest_1224z00_6240))
									{	/* Llib/object.scm 1506 */
										BgL_stackz00_6224 = BgL__ortest_1224z00_6240;
									}
								else
									{	/* Llib/object.scm 1506 */

										{	/* Llib/object.scm 1506 */

											BgL_stackz00_6224 =
												BGl_getzd2tracezd2stackz00zz__errorz00(BFALSE);
										}
									}
							}
						else
							{	/* Llib/object.scm 1507 */

								{	/* Llib/object.scm 1507 */

									BgL_stackz00_6224 =
										BGl_getzd2tracezd2stackz00zz__errorz00(BFALSE);
								}
							}
					}
					{	/* Llib/object.scm 1504 */

						return
							BGl_displayzd2tracezd2stackz00zz__errorz00(BgL_stackz00_6224,
							BgL_portz00_6223, BINT(1L));
					}
				}
			}
		}

	}



/* &object-equal?1402 */
	obj_t BGl_z62objectzd2equalzf31402z43zz__objectz00(obj_t BgL_envz00_5641,
		obj_t BgL_obj1z00_5642, obj_t BgL_obj2z00_5643)
	{
		{	/* Llib/object.scm 1475 */
			{	/* Llib/object.scm 1479 */
				bool_t BgL_tmpz00_9707;

				{	/* Llib/object.scm 1479 */
					obj_t BgL_class1z00_6243;
					obj_t BgL_class2z00_6244;

					{	/* Llib/object.scm 893 */
						obj_t BgL_arg1553z00_6245;
						long BgL_arg1554z00_6246;

						BgL_arg1553z00_6245 = (BGl_za2classesza2z00zz__objectz00);
						{	/* Llib/object.scm 894 */
							long BgL_arg1555z00_6247;

							BgL_arg1555z00_6247 =
								BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_obj1z00_5642));
							BgL_arg1554z00_6246 = (BgL_arg1555z00_6247 - OBJECT_TYPE);
						}
						BgL_class1z00_6243 =
							VECTOR_REF(BgL_arg1553z00_6245, BgL_arg1554z00_6246);
					}
					{	/* Llib/object.scm 893 */
						obj_t BgL_arg1553z00_6248;
						long BgL_arg1554z00_6249;

						BgL_arg1553z00_6248 = (BGl_za2classesza2z00zz__objectz00);
						{	/* Llib/object.scm 894 */
							long BgL_arg1555z00_6250;

							BgL_arg1555z00_6250 =
								BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_obj2z00_5643));
							BgL_arg1554z00_6249 = (BgL_arg1555z00_6250 - OBJECT_TYPE);
						}
						BgL_class2z00_6244 =
							VECTOR_REF(BgL_arg1553z00_6248, BgL_arg1554z00_6249);
					}
					if ((BgL_class1z00_6243 == BgL_class2z00_6244))
						{	/* Llib/object.scm 1482 */
							obj_t BgL_fieldsz00_6251;

							{	/* Llib/object.scm 572 */
								obj_t BgL_tmpz00_9720;

								BgL_tmpz00_9720 = ((obj_t) BgL_class1z00_6243);
								BgL_fieldsz00_6251 = BGL_CLASS_ALL_FIELDS(BgL_tmpz00_9720);
							}
							{	/* Llib/object.scm 1483 */
								long BgL_g1222z00_6252;

								BgL_g1222z00_6252 = (VECTOR_LENGTH(BgL_fieldsz00_6251) - 1L);
								{
									long BgL_iz00_6254;

									BgL_iz00_6254 = BgL_g1222z00_6252;
								BgL_loopz00_6253:
									if ((BgL_iz00_6254 == -1L))
										{	/* Llib/object.scm 1485 */
											BgL_tmpz00_9707 = ((bool_t) 1);
										}
									else
										{	/* Llib/object.scm 1487 */
											bool_t BgL_test3741z00_9727;

											{	/* Llib/object.scm 1487 */
												obj_t BgL_arg2192z00_6255;

												BgL_arg2192z00_6255 =
													VECTOR_REF(BgL_fieldsz00_6251, BgL_iz00_6254);
												{	/* Llib/object.scm 1477 */
													obj_t BgL_getzd2valuezd2_6256;

													{	/* Llib/object.scm 1477 */
														obj_t BgL_res2617z00_6257;

														{	/* Llib/object.scm 618 */
															obj_t BgL_vectorz00_6258;

															BgL_vectorz00_6258 =
																((obj_t) BgL_arg2192z00_6255);
															BgL_res2617z00_6257 =
																VECTOR_REF(BgL_vectorz00_6258, 1L);
														}
														BgL_getzd2valuezd2_6256 = BgL_res2617z00_6257;
													}
													{	/* Llib/object.scm 1478 */
														obj_t BgL_arg2196z00_6259;
														obj_t BgL_arg2197z00_6260;

														BgL_arg2196z00_6259 =
															BGL_PROCEDURE_CALL1(BgL_getzd2valuezd2_6256,
															((obj_t)
																((BgL_objectz00_bglt) BgL_obj1z00_5642)));
														BgL_arg2197z00_6260 =
															BGL_PROCEDURE_CALL1(BgL_getzd2valuezd2_6256,
															((obj_t)
																((BgL_objectz00_bglt) BgL_obj2z00_5643)));
														BgL_test3741z00_9727 =
															BGl_equalzf3zf3zz__r4_equivalence_6_2z00
															(BgL_arg2196z00_6259, BgL_arg2197z00_6260);
													}
												}
											}
											if (BgL_test3741z00_9727)
												{
													long BgL_iz00_9744;

													BgL_iz00_9744 = (BgL_iz00_6254 - 1L);
													BgL_iz00_6254 = BgL_iz00_9744;
													goto BgL_loopz00_6253;
												}
											else
												{	/* Llib/object.scm 1487 */
													BgL_tmpz00_9707 = ((bool_t) 0);
												}
										}
								}
							}
						}
					else
						{	/* Llib/object.scm 1481 */
							BgL_tmpz00_9707 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_9707);
			}
		}

	}



/* &object-print1399 */
	obj_t BGl_z62objectzd2print1399zb0zz__objectz00(obj_t BgL_envz00_5644,
		obj_t BgL_objz00_5645, obj_t BgL_portz00_5646,
		obj_t BgL_printzd2slotzd2_5647)
	{
		{	/* Llib/object.scm 1445 */
			{
				obj_t BgL_fieldz00_6265;

				{	/* Llib/object.scm 1457 */
					obj_t BgL_classz00_6273;

					{	/* Llib/object.scm 893 */
						obj_t BgL_arg1553z00_6274;
						long BgL_arg1554z00_6275;

						BgL_arg1553z00_6274 = (BGl_za2classesza2z00zz__objectz00);
						{	/* Llib/object.scm 894 */
							long BgL_arg1555z00_6276;

							BgL_arg1555z00_6276 =
								BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_objz00_5645));
							BgL_arg1554z00_6275 = (BgL_arg1555z00_6276 - OBJECT_TYPE);
						}
						BgL_classz00_6273 =
							VECTOR_REF(BgL_arg1553z00_6274, BgL_arg1554z00_6275);
					}
					{	/* Llib/object.scm 1457 */
						obj_t BgL_classzd2namezd2_6277;

						{	/* Llib/object.scm 510 */
							obj_t BgL_tmpz00_9752;

							BgL_tmpz00_9752 = ((obj_t) BgL_classz00_6273);
							BgL_classzd2namezd2_6277 = BGL_CLASS_NAME(BgL_tmpz00_9752);
						}
						{	/* Llib/object.scm 1458 */
							obj_t BgL_fieldsz00_6278;

							{	/* Llib/object.scm 572 */
								obj_t BgL_tmpz00_9755;

								BgL_tmpz00_9755 = ((obj_t) BgL_classz00_6273);
								BgL_fieldsz00_6278 = BGL_CLASS_ALL_FIELDS(BgL_tmpz00_9755);
							}
							{	/* Llib/object.scm 1460 */

								{	/* Llib/object.scm 1461 */
									obj_t BgL_tmpz00_9758;

									BgL_tmpz00_9758 = ((obj_t) BgL_portz00_5646);
									bgl_display_string(BGl_string3154z00zz__objectz00,
										BgL_tmpz00_9758);
								}
								bgl_display_obj(BgL_classzd2namezd2_6277,
									((obj_t) BgL_portz00_5646));
								{	/* Llib/object.scm 1463 */
									bool_t BgL_test3742z00_9763;

									{	/* Llib/object.scm 1324 */
										obj_t BgL_klassz00_6279;

										{	/* Llib/object.scm 893 */
											obj_t BgL_arg1553z00_6280;
											long BgL_arg1554z00_6281;

											BgL_arg1553z00_6280 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Llib/object.scm 894 */
												long BgL_arg1555z00_6282;

												BgL_arg1555z00_6282 =
													BGL_OBJECT_CLASS_NUM(
													((BgL_objectz00_bglt) BgL_objz00_5645));
												BgL_arg1554z00_6281 =
													(BgL_arg1555z00_6282 - OBJECT_TYPE);
											}
											BgL_klassz00_6279 =
												VECTOR_REF(BgL_arg1553z00_6280, BgL_arg1554z00_6281);
										}
										{	/* Llib/object.scm 1325 */
											obj_t BgL_arg1801z00_6283;

											{	/* Llib/object.scm 755 */
												obj_t BgL__ortest_1218z00_6284;

												{	/* Llib/object.scm 755 */
													obj_t BgL_tmpz00_9769;

													BgL_tmpz00_9769 = ((obj_t) BgL_klassz00_6279);
													BgL__ortest_1218z00_6284 =
														BGL_CLASS_NIL(BgL_tmpz00_9769);
												}
												if (CBOOL(BgL__ortest_1218z00_6284))
													{	/* Llib/object.scm 755 */
														BgL_arg1801z00_6283 = BgL__ortest_1218z00_6284;
													}
												else
													{	/* Llib/object.scm 755 */
														BgL_arg1801z00_6283 =
															BGl_classzd2nilzd2initz12z12zz__objectz00(
															((obj_t) BgL_klassz00_6279));
													}
											}
											BgL_test3742z00_9763 =
												(BgL_arg1801z00_6283 ==
												((obj_t) ((BgL_objectz00_bglt) BgL_objz00_5645)));
										}
									}
									if (BgL_test3742z00_9763)
										{	/* Llib/object.scm 1464 */
											obj_t BgL_tmpz00_9779;

											BgL_tmpz00_9779 = ((obj_t) BgL_portz00_5646);
											return
												bgl_display_string(BGl_string3155z00zz__objectz00,
												BgL_tmpz00_9779);
										}
									else
										{
											long BgL_iz00_6286;

											BgL_iz00_6286 = 0L;
										BgL_loopz00_6285:
											if ((BgL_iz00_6286 == VECTOR_LENGTH(BgL_fieldsz00_6278)))
												{	/* Llib/object.scm 1467 */
													obj_t BgL_tmpz00_9785;

													BgL_tmpz00_9785 = ((obj_t) BgL_portz00_5646);
													return
														bgl_display_char(((unsigned char) '|'),
														BgL_tmpz00_9785);
												}
											else
												{	/* Llib/object.scm 1466 */
													BgL_fieldz00_6265 =
														VECTOR_REF(BgL_fieldsz00_6278, BgL_iz00_6286);
													{	/* Llib/object.scm 1448 */
														obj_t BgL_namez00_6266;

														{	/* Llib/object.scm 1448 */
															obj_t BgL_res2614z00_6267;

															{	/* Llib/object.scm 606 */
																obj_t BgL_vectorz00_6268;

																BgL_vectorz00_6268 =
																	((obj_t) BgL_fieldz00_6265);
																BgL_res2614z00_6267 =
																	VECTOR_REF(BgL_vectorz00_6268, 0L);
															}
															BgL_namez00_6266 = BgL_res2614z00_6267;
														}
														{	/* Llib/object.scm 1448 */
															obj_t BgL_getzd2valuezd2_6269;

															{	/* Llib/object.scm 1449 */
																obj_t BgL_res2615z00_6270;

																{	/* Llib/object.scm 618 */
																	obj_t BgL_vectorz00_6271;

																	BgL_vectorz00_6271 =
																		((obj_t) BgL_fieldz00_6265);
																	BgL_res2615z00_6270 =
																		VECTOR_REF(BgL_vectorz00_6271, 1L);
																}
																BgL_getzd2valuezd2_6269 = BgL_res2615z00_6270;
															}
															{	/* Llib/object.scm 1449 */

																{	/* Llib/object.scm 1450 */
																	obj_t BgL_tmpz00_9792;

																	BgL_tmpz00_9792 = ((obj_t) BgL_portz00_5646);
																	bgl_display_string
																		(BGl_string3153z00zz__objectz00,
																		BgL_tmpz00_9792);
																}
																bgl_display_obj(BgL_namez00_6266,
																	((obj_t) BgL_portz00_5646));
																{	/* Llib/object.scm 1452 */
																	obj_t BgL_tmpz00_9797;

																	BgL_tmpz00_9797 = ((obj_t) BgL_portz00_5646);
																	bgl_display_char(((unsigned char) ':'),
																		BgL_tmpz00_9797);
																}
																{	/* Llib/object.scm 1453 */
																	obj_t BgL_tmpz00_9800;

																	BgL_tmpz00_9800 = ((obj_t) BgL_portz00_5646);
																	bgl_display_char(((unsigned char) ' '),
																		BgL_tmpz00_9800);
																}
																{	/* Llib/object.scm 1454 */
																	obj_t BgL_arg2185z00_6272;

																	BgL_arg2185z00_6272 =
																		BGL_PROCEDURE_CALL1(BgL_getzd2valuezd2_6269,
																		((obj_t)
																			((BgL_objectz00_bglt) BgL_objz00_5645)));
																	{	/* Llib/object.scm 1454 */
																		obj_t BgL_tmpfunz00_9815;

																		BgL_tmpfunz00_9815 =
																			((obj_t) BgL_printzd2slotzd2_5647);
																		(VA_PROCEDUREP(BgL_tmpfunz00_9815)
																			? ((obj_t(*)(obj_t,
																						...))
																				PROCEDURE_ENTRY(BgL_tmpfunz00_9815)) ((
																					(obj_t) BgL_printzd2slotzd2_5647),
																				BgL_arg2185z00_6272,
																				((obj_t) BgL_portz00_5646),
																				BEOA) : ((obj_t(*)(obj_t, obj_t,
																						obj_t))
																				PROCEDURE_ENTRY(BgL_tmpfunz00_9815)) ((
																					(obj_t) BgL_printzd2slotzd2_5647),
																				BgL_arg2185z00_6272,
																				((obj_t) BgL_portz00_5646)));
																}}
																{	/* Llib/object.scm 1455 */
																	obj_t BgL_tmpz00_9817;

																	BgL_tmpz00_9817 = ((obj_t) BgL_portz00_5646);
																	bgl_display_char(((unsigned char) ']'),
																		BgL_tmpz00_9817);
													}}}}
													{
														long BgL_iz00_9821;

														BgL_iz00_9821 = (BgL_iz00_6286 + 1L);
														BgL_iz00_6286 = BgL_iz00_9821;
														goto BgL_loopz00_6285;
													}
												}
										}
								}
							}
						}
					}
				}
			}
		}

	}



/* &object-hashnumber1396 */
	obj_t BGl_z62objectzd2hashnumber1396zb0zz__objectz00(obj_t BgL_envz00_5648,
		obj_t BgL_objectz00_5649)
	{
		{	/* Llib/object.scm 1412 */
			return
				BINT(bgl_obj_hash_number(
					((obj_t) ((BgL_objectz00_bglt) BgL_objectz00_5649))));
		}

	}



/* &object-write1392 */
	obj_t BGl_z62objectzd2write1392zb0zz__objectz00(obj_t BgL_envz00_5650,
		obj_t BgL_objz00_5651, obj_t BgL_portz00_5652)
	{
		{	/* Llib/object.scm 1405 */
			{	/* Llib/object.scm 1406 */
				obj_t BgL_portz00_6289;

				if (PAIRP(BgL_portz00_5652))
					{	/* Llib/object.scm 1406 */
						BgL_portz00_6289 = CAR(BgL_portz00_5652);
					}
				else
					{	/* Llib/object.scm 1406 */
						obj_t BgL_tmpz00_9830;

						BgL_tmpz00_9830 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_portz00_6289 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_9830);
					}
				return
					BGl_objectzd2printzd2zz__objectz00(
					((BgL_objectz00_bglt) BgL_objz00_5651), BgL_portz00_6289,
					BGl_writezd2envzd2zz__r4_output_6_10_3z00);
			}
		}

	}



/* &object-display1389 */
	obj_t BGl_z62objectzd2display1389zb0zz__objectz00(obj_t BgL_envz00_5653,
		obj_t BgL_objz00_5654, obj_t BgL_portz00_5655)
	{
		{	/* Llib/object.scm 1398 */
			{	/* Llib/object.scm 1399 */
				obj_t BgL_portz00_6291;

				if (PAIRP(BgL_portz00_5655))
					{	/* Llib/object.scm 1399 */
						BgL_portz00_6291 = CAR(BgL_portz00_5655);
					}
				else
					{	/* Llib/object.scm 1399 */
						obj_t BgL_tmpz00_9838;

						BgL_tmpz00_9838 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_portz00_6291 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_9838);
					}
				return
					BGl_objectzd2printzd2zz__objectz00(
					((BgL_objectz00_bglt) BgL_objz00_5654), BgL_portz00_6291,
					BGl_displayzd2envzd2zz__r4_output_6_10_3z00);
			}
		}

	}



/* object-display */
	BGL_EXPORTED_DEF obj_t BGl_objectzd2displayzd2zz__objectz00(BgL_objectz00_bglt
		BgL_objz00_163, obj_t BgL_portz00_164)
	{
		{	/* Llib/object.scm 1398 */
			{	/* Llib/object.scm 1398 */
				obj_t BgL_method1391z00_2552;

				{	/* Llib/object.scm 1398 */
					obj_t BgL_res2623z00_4581;

					{	/* Llib/object.scm 1284 */
						long BgL_objzd2classzd2numz00_4552;

						BgL_objzd2classzd2numz00_4552 =
							BGL_OBJECT_CLASS_NUM(BgL_objz00_163);
						{	/* Llib/object.scm 1285 */
							obj_t BgL_arg1793z00_4553;

							BgL_arg1793z00_4553 =
								PROCEDURE_REF(BGl_objectzd2displayzd2envz00zz__objectz00,
								(int) (1L));
							{	/* Llib/object.scm 1285 */
								int BgL_offsetz00_4556;

								BgL_offsetz00_4556 = (int) (BgL_objzd2classzd2numz00_4552);
								{	/* Llib/object.scm 931 */
									long BgL_offsetz00_4557;

									BgL_offsetz00_4557 =
										((long) (BgL_offsetz00_4556) - OBJECT_TYPE);
									{	/* Llib/object.scm 931 */
										long BgL_modz00_4558;

										BgL_modz00_4558 =
											(BgL_offsetz00_4557 >> (int) ((long) ((int) (4L))));
										{	/* Llib/object.scm 932 */
											long BgL_restz00_4560;

											BgL_restz00_4560 =
												(BgL_offsetz00_4557 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Llib/object.scm 933 */

												{	/* Llib/object.scm 934 */
													obj_t BgL_bucketz00_4562;

													BgL_bucketz00_4562 =
														VECTOR_REF(
														((obj_t) BgL_arg1793z00_4553), BgL_modz00_4558);
													BgL_res2623z00_4581 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_4562), BgL_restz00_4560);
					}}}}}}}}
					BgL_method1391z00_2552 = BgL_res2623z00_4581;
				}
				{	/* Llib/object.scm 1398 */
					obj_t BgL_auxz00_9867;

					{	/* Llib/object.scm 1398 */
						obj_t BgL_list2204z00_2553;

						BgL_list2204z00_2553 = MAKE_YOUNG_PAIR(BgL_portz00_164, BNIL);
						BgL_auxz00_9867 =
							BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(
							((obj_t) BgL_objz00_163), BgL_list2204z00_2553);
					}
					return apply(BgL_method1391z00_2552, BgL_auxz00_9867);
				}
			}
		}

	}



/* &object-display */
	obj_t BGl_z62objectzd2displayzb0zz__objectz00(obj_t BgL_envz00_5656,
		obj_t BgL_objz00_5657, obj_t BgL_portz00_5658)
	{
		{	/* Llib/object.scm 1398 */
			{	/* Llib/object.scm 1398 */
				BgL_objectz00_bglt BgL_auxz00_9872;

				{	/* Llib/object.scm 1398 */
					bool_t BgL_test3747z00_9873;

					{	/* Llib/object.scm 1398 */
						obj_t BgL_classz00_6292;

						BgL_classz00_6292 = BGl_objectz00zz__objectz00;
						if (BGL_OBJECTP(BgL_objz00_5657))
							{	/* Llib/object.scm 1334 */
								BgL_objectz00_bglt BgL_arg1803z00_6293;
								long BgL_arg1804z00_6294;

								BgL_arg1803z00_6293 = (BgL_objectz00_bglt) (BgL_objz00_5657);
								BgL_arg1804z00_6294 = BGL_CLASS_DEPTH(BgL_classz00_6292);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Llib/object.scm 1375 */
										long BgL_idxz00_6295;

										BgL_idxz00_6295 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_6293);
										BgL_test3747z00_9873 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_6295 + BgL_arg1804z00_6294)) ==
											BgL_classz00_6292);
									}
								else
									{	/* Llib/object.scm 1354 */
										bool_t BgL_res2597z00_6298;

										{	/* Llib/object.scm 1362 */
											obj_t BgL_oclassz00_6299;

											{	/* Llib/object.scm 893 */
												obj_t BgL_arg1553z00_6300;
												long BgL_arg1554z00_6301;

												BgL_arg1553z00_6300 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Llib/object.scm 894 */
													long BgL_arg1555z00_6302;

													BgL_arg1555z00_6302 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_6293);
													BgL_arg1554z00_6301 =
														(BgL_arg1555z00_6302 - OBJECT_TYPE);
												}
												BgL_oclassz00_6299 =
													VECTOR_REF(BgL_arg1553z00_6300, BgL_arg1554z00_6301);
											}
											{	/* Llib/object.scm 1363 */
												bool_t BgL__ortest_1220z00_6303;

												BgL__ortest_1220z00_6303 =
													(BgL_classz00_6292 == BgL_oclassz00_6299);
												if (BgL__ortest_1220z00_6303)
													{	/* Llib/object.scm 1363 */
														BgL_res2597z00_6298 = BgL__ortest_1220z00_6303;
													}
												else
													{	/* Llib/object.scm 1364 */
														long BgL_odepthz00_6304;

														{	/* Llib/object.scm 1364 */
															obj_t BgL_arg1810z00_6305;

															BgL_arg1810z00_6305 = (BgL_oclassz00_6299);
															BgL_odepthz00_6304 =
																BGL_CLASS_DEPTH(BgL_arg1810z00_6305);
														}
														if ((BgL_arg1804z00_6294 < BgL_odepthz00_6304))
															{	/* Llib/object.scm 1366 */
																obj_t BgL_arg1808z00_6306;

																{	/* Llib/object.scm 1366 */
																	obj_t BgL_arg1809z00_6307;

																	BgL_arg1809z00_6307 = (BgL_oclassz00_6299);
																	BgL_arg1808z00_6306 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_6307,
																		BgL_arg1804z00_6294);
																}
																BgL_res2597z00_6298 =
																	(BgL_arg1808z00_6306 == BgL_classz00_6292);
															}
														else
															{	/* Llib/object.scm 1365 */
																BgL_res2597z00_6298 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test3747z00_9873 = BgL_res2597z00_6298;
									}
							}
						else
							{	/* Llib/object.scm 1333 */
								BgL_test3747z00_9873 = ((bool_t) 0);
							}
					}
					if (BgL_test3747z00_9873)
						{	/* Llib/object.scm 1398 */
							BgL_auxz00_9872 = ((BgL_objectz00_bglt) BgL_objz00_5657);
						}
					else
						{
							obj_t BgL_auxz00_9898;

							BgL_auxz00_9898 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
								BINT(61094L), BGl_string3156z00zz__objectz00,
								BGl_string2903z00zz__objectz00, BgL_objz00_5657);
							FAILURE(BgL_auxz00_9898, BFALSE, BFALSE);
						}
				}
				return
					BGl_objectzd2displayzd2zz__objectz00(BgL_auxz00_9872,
					BgL_portz00_5658);
			}
		}

	}



/* object-write */
	BGL_EXPORTED_DEF obj_t BGl_objectzd2writezd2zz__objectz00(BgL_objectz00_bglt
		BgL_objz00_165, obj_t BgL_portz00_166)
	{
		{	/* Llib/object.scm 1405 */
			{	/* Llib/object.scm 1405 */
				obj_t BgL_method1395z00_2554;

				{	/* Llib/object.scm 1405 */
					obj_t BgL_res2628z00_4612;

					{	/* Llib/object.scm 1284 */
						long BgL_objzd2classzd2numz00_4583;

						BgL_objzd2classzd2numz00_4583 =
							BGL_OBJECT_CLASS_NUM(BgL_objz00_165);
						{	/* Llib/object.scm 1285 */
							obj_t BgL_arg1793z00_4584;

							BgL_arg1793z00_4584 =
								PROCEDURE_REF(BGl_objectzd2writezd2envz00zz__objectz00,
								(int) (1L));
							{	/* Llib/object.scm 1285 */
								int BgL_offsetz00_4587;

								BgL_offsetz00_4587 = (int) (BgL_objzd2classzd2numz00_4583);
								{	/* Llib/object.scm 931 */
									long BgL_offsetz00_4588;

									BgL_offsetz00_4588 =
										((long) (BgL_offsetz00_4587) - OBJECT_TYPE);
									{	/* Llib/object.scm 931 */
										long BgL_modz00_4589;

										BgL_modz00_4589 =
											(BgL_offsetz00_4588 >> (int) ((long) ((int) (4L))));
										{	/* Llib/object.scm 932 */
											long BgL_restz00_4591;

											BgL_restz00_4591 =
												(BgL_offsetz00_4588 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Llib/object.scm 933 */

												{	/* Llib/object.scm 934 */
													obj_t BgL_bucketz00_4593;

													BgL_bucketz00_4593 =
														VECTOR_REF(
														((obj_t) BgL_arg1793z00_4584), BgL_modz00_4589);
													BgL_res2628z00_4612 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_4593), BgL_restz00_4591);
					}}}}}}}}
					BgL_method1395z00_2554 = BgL_res2628z00_4612;
				}
				{	/* Llib/object.scm 1405 */
					obj_t BgL_auxz00_9927;

					{	/* Llib/object.scm 1405 */
						obj_t BgL_list2205z00_2555;

						BgL_list2205z00_2555 = MAKE_YOUNG_PAIR(BgL_portz00_166, BNIL);
						BgL_auxz00_9927 =
							BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(
							((obj_t) BgL_objz00_165), BgL_list2205z00_2555);
					}
					return apply(BgL_method1395z00_2554, BgL_auxz00_9927);
				}
			}
		}

	}



/* &object-write */
	obj_t BGl_z62objectzd2writezb0zz__objectz00(obj_t BgL_envz00_5659,
		obj_t BgL_objz00_5660, obj_t BgL_portz00_5661)
	{
		{	/* Llib/object.scm 1405 */
			{	/* Llib/object.scm 1405 */
				BgL_objectz00_bglt BgL_auxz00_9932;

				{	/* Llib/object.scm 1405 */
					bool_t BgL_test3752z00_9933;

					{	/* Llib/object.scm 1405 */
						obj_t BgL_classz00_6308;

						BgL_classz00_6308 = BGl_objectz00zz__objectz00;
						if (BGL_OBJECTP(BgL_objz00_5660))
							{	/* Llib/object.scm 1334 */
								BgL_objectz00_bglt BgL_arg1803z00_6309;
								long BgL_arg1804z00_6310;

								BgL_arg1803z00_6309 = (BgL_objectz00_bglt) (BgL_objz00_5660);
								BgL_arg1804z00_6310 = BGL_CLASS_DEPTH(BgL_classz00_6308);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Llib/object.scm 1375 */
										long BgL_idxz00_6311;

										BgL_idxz00_6311 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_6309);
										BgL_test3752z00_9933 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_6311 + BgL_arg1804z00_6310)) ==
											BgL_classz00_6308);
									}
								else
									{	/* Llib/object.scm 1354 */
										bool_t BgL_res2597z00_6314;

										{	/* Llib/object.scm 1362 */
											obj_t BgL_oclassz00_6315;

											{	/* Llib/object.scm 893 */
												obj_t BgL_arg1553z00_6316;
												long BgL_arg1554z00_6317;

												BgL_arg1553z00_6316 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Llib/object.scm 894 */
													long BgL_arg1555z00_6318;

													BgL_arg1555z00_6318 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_6309);
													BgL_arg1554z00_6317 =
														(BgL_arg1555z00_6318 - OBJECT_TYPE);
												}
												BgL_oclassz00_6315 =
													VECTOR_REF(BgL_arg1553z00_6316, BgL_arg1554z00_6317);
											}
											{	/* Llib/object.scm 1363 */
												bool_t BgL__ortest_1220z00_6319;

												BgL__ortest_1220z00_6319 =
													(BgL_classz00_6308 == BgL_oclassz00_6315);
												if (BgL__ortest_1220z00_6319)
													{	/* Llib/object.scm 1363 */
														BgL_res2597z00_6314 = BgL__ortest_1220z00_6319;
													}
												else
													{	/* Llib/object.scm 1364 */
														long BgL_odepthz00_6320;

														{	/* Llib/object.scm 1364 */
															obj_t BgL_arg1810z00_6321;

															BgL_arg1810z00_6321 = (BgL_oclassz00_6315);
															BgL_odepthz00_6320 =
																BGL_CLASS_DEPTH(BgL_arg1810z00_6321);
														}
														if ((BgL_arg1804z00_6310 < BgL_odepthz00_6320))
															{	/* Llib/object.scm 1366 */
																obj_t BgL_arg1808z00_6322;

																{	/* Llib/object.scm 1366 */
																	obj_t BgL_arg1809z00_6323;

																	BgL_arg1809z00_6323 = (BgL_oclassz00_6315);
																	BgL_arg1808z00_6322 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_6323,
																		BgL_arg1804z00_6310);
																}
																BgL_res2597z00_6314 =
																	(BgL_arg1808z00_6322 == BgL_classz00_6308);
															}
														else
															{	/* Llib/object.scm 1365 */
																BgL_res2597z00_6314 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test3752z00_9933 = BgL_res2597z00_6314;
									}
							}
						else
							{	/* Llib/object.scm 1333 */
								BgL_test3752z00_9933 = ((bool_t) 0);
							}
					}
					if (BgL_test3752z00_9933)
						{	/* Llib/object.scm 1405 */
							BgL_auxz00_9932 = ((BgL_objectz00_bglt) BgL_objz00_5660);
						}
					else
						{
							obj_t BgL_auxz00_9958;

							BgL_auxz00_9958 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
								BINT(61477L), BGl_string3157z00zz__objectz00,
								BGl_string2903z00zz__objectz00, BgL_objz00_5660);
							FAILURE(BgL_auxz00_9958, BFALSE, BFALSE);
						}
				}
				return
					BGl_objectzd2writezd2zz__objectz00(BgL_auxz00_9932, BgL_portz00_5661);
			}
		}

	}



/* object-hashnumber */
	BGL_EXPORTED_DEF long
		BGl_objectzd2hashnumberzd2zz__objectz00(BgL_objectz00_bglt
		BgL_objectz00_167)
	{
		{	/* Llib/object.scm 1412 */
			{	/* Llib/object.scm 1412 */
				obj_t BgL_method1398z00_2556;

				{	/* Llib/object.scm 1412 */
					obj_t BgL_res2633z00_4643;

					{	/* Llib/object.scm 1284 */
						long BgL_objzd2classzd2numz00_4614;

						BgL_objzd2classzd2numz00_4614 =
							BGL_OBJECT_CLASS_NUM(BgL_objectz00_167);
						{	/* Llib/object.scm 1285 */
							obj_t BgL_arg1793z00_4615;

							BgL_arg1793z00_4615 =
								PROCEDURE_REF(BGl_objectzd2hashnumberzd2envz00zz__objectz00,
								(int) (1L));
							{	/* Llib/object.scm 1285 */
								int BgL_offsetz00_4618;

								BgL_offsetz00_4618 = (int) (BgL_objzd2classzd2numz00_4614);
								{	/* Llib/object.scm 931 */
									long BgL_offsetz00_4619;

									BgL_offsetz00_4619 =
										((long) (BgL_offsetz00_4618) - OBJECT_TYPE);
									{	/* Llib/object.scm 931 */
										long BgL_modz00_4620;

										BgL_modz00_4620 =
											(BgL_offsetz00_4619 >> (int) ((long) ((int) (4L))));
										{	/* Llib/object.scm 932 */
											long BgL_restz00_4622;

											BgL_restz00_4622 =
												(BgL_offsetz00_4619 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Llib/object.scm 933 */

												{	/* Llib/object.scm 934 */
													obj_t BgL_bucketz00_4624;

													BgL_bucketz00_4624 =
														VECTOR_REF(
														((obj_t) BgL_arg1793z00_4615), BgL_modz00_4620);
													BgL_res2633z00_4643 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_4624), BgL_restz00_4622);
					}}}}}}}}
					BgL_method1398z00_2556 = BgL_res2633z00_4643;
				}
				return
					(long) CINT(BGL_PROCEDURE_CALL1(BgL_method1398z00_2556,
						((obj_t) BgL_objectz00_167)));
		}}

	}



/* &object-hashnumber */
	obj_t BGl_z62objectzd2hashnumberzb0zz__objectz00(obj_t BgL_envz00_5662,
		obj_t BgL_objectz00_5663)
	{
		{	/* Llib/object.scm 1412 */
			{	/* Llib/object.scm 1412 */
				long BgL_tmpz00_9993;

				{	/* Llib/object.scm 1412 */
					BgL_objectz00_bglt BgL_auxz00_9994;

					{	/* Llib/object.scm 1412 */
						bool_t BgL_test3757z00_9995;

						{	/* Llib/object.scm 1412 */
							obj_t BgL_classz00_6324;

							BgL_classz00_6324 = BGl_objectz00zz__objectz00;
							if (BGL_OBJECTP(BgL_objectz00_5663))
								{	/* Llib/object.scm 1334 */
									BgL_objectz00_bglt BgL_arg1803z00_6325;
									long BgL_arg1804z00_6326;

									BgL_arg1803z00_6325 =
										(BgL_objectz00_bglt) (BgL_objectz00_5663);
									BgL_arg1804z00_6326 = BGL_CLASS_DEPTH(BgL_classz00_6324);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Llib/object.scm 1375 */
											long BgL_idxz00_6327;

											BgL_idxz00_6327 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_6325);
											BgL_test3757z00_9995 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_6327 + BgL_arg1804z00_6326)) ==
												BgL_classz00_6324);
										}
									else
										{	/* Llib/object.scm 1354 */
											bool_t BgL_res2597z00_6330;

											{	/* Llib/object.scm 1362 */
												obj_t BgL_oclassz00_6331;

												{	/* Llib/object.scm 893 */
													obj_t BgL_arg1553z00_6332;
													long BgL_arg1554z00_6333;

													BgL_arg1553z00_6332 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Llib/object.scm 894 */
														long BgL_arg1555z00_6334;

														BgL_arg1555z00_6334 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_6325);
														BgL_arg1554z00_6333 =
															(BgL_arg1555z00_6334 - OBJECT_TYPE);
													}
													BgL_oclassz00_6331 =
														VECTOR_REF(BgL_arg1553z00_6332,
														BgL_arg1554z00_6333);
												}
												{	/* Llib/object.scm 1363 */
													bool_t BgL__ortest_1220z00_6335;

													BgL__ortest_1220z00_6335 =
														(BgL_classz00_6324 == BgL_oclassz00_6331);
													if (BgL__ortest_1220z00_6335)
														{	/* Llib/object.scm 1363 */
															BgL_res2597z00_6330 = BgL__ortest_1220z00_6335;
														}
													else
														{	/* Llib/object.scm 1364 */
															long BgL_odepthz00_6336;

															{	/* Llib/object.scm 1364 */
																obj_t BgL_arg1810z00_6337;

																BgL_arg1810z00_6337 = (BgL_oclassz00_6331);
																BgL_odepthz00_6336 =
																	BGL_CLASS_DEPTH(BgL_arg1810z00_6337);
															}
															if ((BgL_arg1804z00_6326 < BgL_odepthz00_6336))
																{	/* Llib/object.scm 1366 */
																	obj_t BgL_arg1808z00_6338;

																	{	/* Llib/object.scm 1366 */
																		obj_t BgL_arg1809z00_6339;

																		BgL_arg1809z00_6339 = (BgL_oclassz00_6331);
																		BgL_arg1808z00_6338 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1809z00_6339,
																			BgL_arg1804z00_6326);
																	}
																	BgL_res2597z00_6330 =
																		(BgL_arg1808z00_6338 == BgL_classz00_6324);
																}
															else
																{	/* Llib/object.scm 1365 */
																	BgL_res2597z00_6330 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test3757z00_9995 = BgL_res2597z00_6330;
										}
								}
							else
								{	/* Llib/object.scm 1333 */
									BgL_test3757z00_9995 = ((bool_t) 0);
								}
						}
						if (BgL_test3757z00_9995)
							{	/* Llib/object.scm 1412 */
								BgL_auxz00_9994 = ((BgL_objectz00_bglt) BgL_objectz00_5663);
							}
						else
							{
								obj_t BgL_auxz00_10020;

								BgL_auxz00_10020 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string2853z00zz__objectz00, BINT(61856L),
									BGl_string3158z00zz__objectz00,
									BGl_string2903z00zz__objectz00, BgL_objectz00_5663);
								FAILURE(BgL_auxz00_10020, BFALSE, BFALSE);
							}
					}
					BgL_tmpz00_9993 =
						BGl_objectzd2hashnumberzd2zz__objectz00(BgL_auxz00_9994);
				}
				return BINT(BgL_tmpz00_9993);
			}
		}

	}



/* object-print */
	BGL_EXPORTED_DEF obj_t BGl_objectzd2printzd2zz__objectz00(BgL_objectz00_bglt
		BgL_objz00_170, obj_t BgL_portz00_171, obj_t BgL_printzd2slotzd2_172)
	{
		{	/* Llib/object.scm 1445 */
			{	/* Llib/object.scm 1445 */
				obj_t BgL_method1401z00_2557;

				{	/* Llib/object.scm 1445 */
					obj_t BgL_res2638z00_4674;

					{	/* Llib/object.scm 1284 */
						long BgL_objzd2classzd2numz00_4645;

						BgL_objzd2classzd2numz00_4645 =
							BGL_OBJECT_CLASS_NUM(BgL_objz00_170);
						{	/* Llib/object.scm 1285 */
							obj_t BgL_arg1793z00_4646;

							BgL_arg1793z00_4646 =
								PROCEDURE_REF(BGl_objectzd2printzd2envz00zz__objectz00,
								(int) (1L));
							{	/* Llib/object.scm 1285 */
								int BgL_offsetz00_4649;

								BgL_offsetz00_4649 = (int) (BgL_objzd2classzd2numz00_4645);
								{	/* Llib/object.scm 931 */
									long BgL_offsetz00_4650;

									BgL_offsetz00_4650 =
										((long) (BgL_offsetz00_4649) - OBJECT_TYPE);
									{	/* Llib/object.scm 931 */
										long BgL_modz00_4651;

										BgL_modz00_4651 =
											(BgL_offsetz00_4650 >> (int) ((long) ((int) (4L))));
										{	/* Llib/object.scm 932 */
											long BgL_restz00_4653;

											BgL_restz00_4653 =
												(BgL_offsetz00_4650 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Llib/object.scm 933 */

												{	/* Llib/object.scm 934 */
													obj_t BgL_bucketz00_4655;

													BgL_bucketz00_4655 =
														VECTOR_REF(
														((obj_t) BgL_arg1793z00_4646), BgL_modz00_4651);
													BgL_res2638z00_4674 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_4655), BgL_restz00_4653);
					}}}}}}}}
					BgL_method1401z00_2557 = BgL_res2638z00_4674;
				}
				return
					BGL_PROCEDURE_CALL3(BgL_method1401z00_2557,
					((obj_t) BgL_objz00_170), BgL_portz00_171, BgL_printzd2slotzd2_172);
			}
		}

	}



/* &object-print */
	obj_t BGl_z62objectzd2printzb0zz__objectz00(obj_t BgL_envz00_5664,
		obj_t BgL_objz00_5665, obj_t BgL_portz00_5666,
		obj_t BgL_printzd2slotzd2_5667)
	{
		{	/* Llib/object.scm 1445 */
			{	/* Llib/object.scm 1445 */
				obj_t BgL_auxz00_10094;
				obj_t BgL_auxz00_10087;
				BgL_objectz00_bglt BgL_auxz00_10057;

				if (PROCEDUREP(BgL_printzd2slotzd2_5667))
					{	/* Llib/object.scm 1445 */
						BgL_auxz00_10094 = BgL_printzd2slotzd2_5667;
					}
				else
					{
						obj_t BgL_auxz00_10097;

						BgL_auxz00_10097 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(63293L), BGl_string3159z00zz__objectz00,
							BGl_string2907z00zz__objectz00, BgL_printzd2slotzd2_5667);
						FAILURE(BgL_auxz00_10097, BFALSE, BFALSE);
					}
				if (OUTPUT_PORTP(BgL_portz00_5666))
					{	/* Llib/object.scm 1445 */
						BgL_auxz00_10087 = BgL_portz00_5666;
					}
				else
					{
						obj_t BgL_auxz00_10090;

						BgL_auxz00_10090 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
							BINT(63293L), BGl_string3159z00zz__objectz00,
							BGl_string3160z00zz__objectz00, BgL_portz00_5666);
						FAILURE(BgL_auxz00_10090, BFALSE, BFALSE);
					}
				{	/* Llib/object.scm 1445 */
					bool_t BgL_test3762z00_10058;

					{	/* Llib/object.scm 1445 */
						obj_t BgL_classz00_6340;

						BgL_classz00_6340 = BGl_objectz00zz__objectz00;
						if (BGL_OBJECTP(BgL_objz00_5665))
							{	/* Llib/object.scm 1334 */
								BgL_objectz00_bglt BgL_arg1803z00_6341;
								long BgL_arg1804z00_6342;

								BgL_arg1803z00_6341 = (BgL_objectz00_bglt) (BgL_objz00_5665);
								BgL_arg1804z00_6342 = BGL_CLASS_DEPTH(BgL_classz00_6340);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Llib/object.scm 1375 */
										long BgL_idxz00_6343;

										BgL_idxz00_6343 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_6341);
										BgL_test3762z00_10058 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_6343 + BgL_arg1804z00_6342)) ==
											BgL_classz00_6340);
									}
								else
									{	/* Llib/object.scm 1354 */
										bool_t BgL_res2597z00_6346;

										{	/* Llib/object.scm 1362 */
											obj_t BgL_oclassz00_6347;

											{	/* Llib/object.scm 893 */
												obj_t BgL_arg1553z00_6348;
												long BgL_arg1554z00_6349;

												BgL_arg1553z00_6348 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Llib/object.scm 894 */
													long BgL_arg1555z00_6350;

													BgL_arg1555z00_6350 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_6341);
													BgL_arg1554z00_6349 =
														(BgL_arg1555z00_6350 - OBJECT_TYPE);
												}
												BgL_oclassz00_6347 =
													VECTOR_REF(BgL_arg1553z00_6348, BgL_arg1554z00_6349);
											}
											{	/* Llib/object.scm 1363 */
												bool_t BgL__ortest_1220z00_6351;

												BgL__ortest_1220z00_6351 =
													(BgL_classz00_6340 == BgL_oclassz00_6347);
												if (BgL__ortest_1220z00_6351)
													{	/* Llib/object.scm 1363 */
														BgL_res2597z00_6346 = BgL__ortest_1220z00_6351;
													}
												else
													{	/* Llib/object.scm 1364 */
														long BgL_odepthz00_6352;

														{	/* Llib/object.scm 1364 */
															obj_t BgL_arg1810z00_6353;

															BgL_arg1810z00_6353 = (BgL_oclassz00_6347);
															BgL_odepthz00_6352 =
																BGL_CLASS_DEPTH(BgL_arg1810z00_6353);
														}
														if ((BgL_arg1804z00_6342 < BgL_odepthz00_6352))
															{	/* Llib/object.scm 1366 */
																obj_t BgL_arg1808z00_6354;

																{	/* Llib/object.scm 1366 */
																	obj_t BgL_arg1809z00_6355;

																	BgL_arg1809z00_6355 = (BgL_oclassz00_6347);
																	BgL_arg1808z00_6354 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_6355,
																		BgL_arg1804z00_6342);
																}
																BgL_res2597z00_6346 =
																	(BgL_arg1808z00_6354 == BgL_classz00_6340);
															}
														else
															{	/* Llib/object.scm 1365 */
																BgL_res2597z00_6346 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test3762z00_10058 = BgL_res2597z00_6346;
									}
							}
						else
							{	/* Llib/object.scm 1333 */
								BgL_test3762z00_10058 = ((bool_t) 0);
							}
					}
					if (BgL_test3762z00_10058)
						{	/* Llib/object.scm 1445 */
							BgL_auxz00_10057 = ((BgL_objectz00_bglt) BgL_objz00_5665);
						}
					else
						{
							obj_t BgL_auxz00_10083;

							BgL_auxz00_10083 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2853z00zz__objectz00,
								BINT(63293L), BGl_string3159z00zz__objectz00,
								BGl_string2903z00zz__objectz00, BgL_objz00_5665);
							FAILURE(BgL_auxz00_10083, BFALSE, BFALSE);
						}
				}
				return
					BGl_objectzd2printzd2zz__objectz00(BgL_auxz00_10057, BgL_auxz00_10087,
					BgL_auxz00_10094);
			}
		}

	}



/* object-equal? */
	BGL_EXPORTED_DEF bool_t
		BGl_objectzd2equalzf3z21zz__objectz00(BgL_objectz00_bglt BgL_obj1z00_173,
		BgL_objectz00_bglt BgL_obj2z00_174)
	{
		{	/* Llib/object.scm 1475 */
			{	/* Llib/object.scm 1475 */
				obj_t BgL_method1403z00_2558;

				{	/* Llib/object.scm 1475 */
					obj_t BgL_res2643z00_4705;

					{	/* Llib/object.scm 1284 */
						long BgL_objzd2classzd2numz00_4676;

						BgL_objzd2classzd2numz00_4676 =
							BGL_OBJECT_CLASS_NUM(BgL_obj1z00_173);
						{	/* Llib/object.scm 1285 */
							obj_t BgL_arg1793z00_4677;

							BgL_arg1793z00_4677 =
								PROCEDURE_REF(BGl_objectzd2equalzf3zd2envzf3zz__objectz00,
								(int) (1L));
							{	/* Llib/object.scm 1285 */
								int BgL_offsetz00_4680;

								BgL_offsetz00_4680 = (int) (BgL_objzd2classzd2numz00_4676);
								{	/* Llib/object.scm 931 */
									long BgL_offsetz00_4681;

									BgL_offsetz00_4681 =
										((long) (BgL_offsetz00_4680) - OBJECT_TYPE);
									{	/* Llib/object.scm 931 */
										long BgL_modz00_4682;

										BgL_modz00_4682 =
											(BgL_offsetz00_4681 >> (int) ((long) ((int) (4L))));
										{	/* Llib/object.scm 932 */
											long BgL_restz00_4684;

											BgL_restz00_4684 =
												(BgL_offsetz00_4681 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Llib/object.scm 933 */

												{	/* Llib/object.scm 934 */
													obj_t BgL_bucketz00_4686;

													BgL_bucketz00_4686 =
														VECTOR_REF(
														((obj_t) BgL_arg1793z00_4677), BgL_modz00_4682);
													BgL_res2643z00_4705 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_4686), BgL_restz00_4684);
					}}}}}}}}
					BgL_method1403z00_2558 = BgL_res2643z00_4705;
				}
				return
					CBOOL(BGL_PROCEDURE_CALL2(BgL_method1403z00_2558,
						((obj_t) BgL_obj1z00_173), ((obj_t) BgL_obj2z00_174)));
			}
		}

	}



/* &object-equal? */
	obj_t BGl_z62objectzd2equalzf3z43zz__objectz00(obj_t BgL_envz00_5668,
		obj_t BgL_obj1z00_5669, obj_t BgL_obj2z00_5670)
	{
		{	/* Llib/object.scm 1475 */
			{	/* Llib/object.scm 1475 */
				bool_t BgL_tmpz00_10134;

				{	/* Llib/object.scm 1475 */
					BgL_objectz00_bglt BgL_auxz00_10165;
					BgL_objectz00_bglt BgL_auxz00_10135;

					{	/* Llib/object.scm 1475 */
						bool_t BgL_test3774z00_10166;

						{	/* Llib/object.scm 1475 */
							obj_t BgL_classz00_6372;

							BgL_classz00_6372 = BGl_objectz00zz__objectz00;
							if (BGL_OBJECTP(BgL_obj2z00_5670))
								{	/* Llib/object.scm 1334 */
									BgL_objectz00_bglt BgL_arg1803z00_6373;
									long BgL_arg1804z00_6374;

									BgL_arg1803z00_6373 = (BgL_objectz00_bglt) (BgL_obj2z00_5670);
									BgL_arg1804z00_6374 = BGL_CLASS_DEPTH(BgL_classz00_6372);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Llib/object.scm 1375 */
											long BgL_idxz00_6375;

											BgL_idxz00_6375 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_6373);
											BgL_test3774z00_10166 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_6375 + BgL_arg1804z00_6374)) ==
												BgL_classz00_6372);
										}
									else
										{	/* Llib/object.scm 1354 */
											bool_t BgL_res2597z00_6378;

											{	/* Llib/object.scm 1362 */
												obj_t BgL_oclassz00_6379;

												{	/* Llib/object.scm 893 */
													obj_t BgL_arg1553z00_6380;
													long BgL_arg1554z00_6381;

													BgL_arg1553z00_6380 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Llib/object.scm 894 */
														long BgL_arg1555z00_6382;

														BgL_arg1555z00_6382 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_6373);
														BgL_arg1554z00_6381 =
															(BgL_arg1555z00_6382 - OBJECT_TYPE);
													}
													BgL_oclassz00_6379 =
														VECTOR_REF(BgL_arg1553z00_6380,
														BgL_arg1554z00_6381);
												}
												{	/* Llib/object.scm 1363 */
													bool_t BgL__ortest_1220z00_6383;

													BgL__ortest_1220z00_6383 =
														(BgL_classz00_6372 == BgL_oclassz00_6379);
													if (BgL__ortest_1220z00_6383)
														{	/* Llib/object.scm 1363 */
															BgL_res2597z00_6378 = BgL__ortest_1220z00_6383;
														}
													else
														{	/* Llib/object.scm 1364 */
															long BgL_odepthz00_6384;

															{	/* Llib/object.scm 1364 */
																obj_t BgL_arg1810z00_6385;

																BgL_arg1810z00_6385 = (BgL_oclassz00_6379);
																BgL_odepthz00_6384 =
																	BGL_CLASS_DEPTH(BgL_arg1810z00_6385);
															}
															if ((BgL_arg1804z00_6374 < BgL_odepthz00_6384))
																{	/* Llib/object.scm 1366 */
																	obj_t BgL_arg1808z00_6386;

																	{	/* Llib/object.scm 1366 */
																		obj_t BgL_arg1809z00_6387;

																		BgL_arg1809z00_6387 = (BgL_oclassz00_6379);
																		BgL_arg1808z00_6386 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1809z00_6387,
																			BgL_arg1804z00_6374);
																	}
																	BgL_res2597z00_6378 =
																		(BgL_arg1808z00_6386 == BgL_classz00_6372);
																}
															else
																{	/* Llib/object.scm 1365 */
																	BgL_res2597z00_6378 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test3774z00_10166 = BgL_res2597z00_6378;
										}
								}
							else
								{	/* Llib/object.scm 1333 */
									BgL_test3774z00_10166 = ((bool_t) 0);
								}
						}
						if (BgL_test3774z00_10166)
							{	/* Llib/object.scm 1475 */
								BgL_auxz00_10165 = ((BgL_objectz00_bglt) BgL_obj2z00_5670);
							}
						else
							{
								obj_t BgL_auxz00_10191;

								BgL_auxz00_10191 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string2853z00zz__objectz00, BINT(64288L),
									BGl_string3161z00zz__objectz00,
									BGl_string2903z00zz__objectz00, BgL_obj2z00_5670);
								FAILURE(BgL_auxz00_10191, BFALSE, BFALSE);
							}
					}
					{	/* Llib/object.scm 1475 */
						bool_t BgL_test3769z00_10136;

						{	/* Llib/object.scm 1475 */
							obj_t BgL_classz00_6356;

							BgL_classz00_6356 = BGl_objectz00zz__objectz00;
							if (BGL_OBJECTP(BgL_obj1z00_5669))
								{	/* Llib/object.scm 1334 */
									BgL_objectz00_bglt BgL_arg1803z00_6357;
									long BgL_arg1804z00_6358;

									BgL_arg1803z00_6357 = (BgL_objectz00_bglt) (BgL_obj1z00_5669);
									BgL_arg1804z00_6358 = BGL_CLASS_DEPTH(BgL_classz00_6356);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Llib/object.scm 1375 */
											long BgL_idxz00_6359;

											BgL_idxz00_6359 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_6357);
											BgL_test3769z00_10136 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_6359 + BgL_arg1804z00_6358)) ==
												BgL_classz00_6356);
										}
									else
										{	/* Llib/object.scm 1354 */
											bool_t BgL_res2597z00_6362;

											{	/* Llib/object.scm 1362 */
												obj_t BgL_oclassz00_6363;

												{	/* Llib/object.scm 893 */
													obj_t BgL_arg1553z00_6364;
													long BgL_arg1554z00_6365;

													BgL_arg1553z00_6364 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Llib/object.scm 894 */
														long BgL_arg1555z00_6366;

														BgL_arg1555z00_6366 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_6357);
														BgL_arg1554z00_6365 =
															(BgL_arg1555z00_6366 - OBJECT_TYPE);
													}
													BgL_oclassz00_6363 =
														VECTOR_REF(BgL_arg1553z00_6364,
														BgL_arg1554z00_6365);
												}
												{	/* Llib/object.scm 1363 */
													bool_t BgL__ortest_1220z00_6367;

													BgL__ortest_1220z00_6367 =
														(BgL_classz00_6356 == BgL_oclassz00_6363);
													if (BgL__ortest_1220z00_6367)
														{	/* Llib/object.scm 1363 */
															BgL_res2597z00_6362 = BgL__ortest_1220z00_6367;
														}
													else
														{	/* Llib/object.scm 1364 */
															long BgL_odepthz00_6368;

															{	/* Llib/object.scm 1364 */
																obj_t BgL_arg1810z00_6369;

																BgL_arg1810z00_6369 = (BgL_oclassz00_6363);
																BgL_odepthz00_6368 =
																	BGL_CLASS_DEPTH(BgL_arg1810z00_6369);
															}
															if ((BgL_arg1804z00_6358 < BgL_odepthz00_6368))
																{	/* Llib/object.scm 1366 */
																	obj_t BgL_arg1808z00_6370;

																	{	/* Llib/object.scm 1366 */
																		obj_t BgL_arg1809z00_6371;

																		BgL_arg1809z00_6371 = (BgL_oclassz00_6363);
																		BgL_arg1808z00_6370 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1809z00_6371,
																			BgL_arg1804z00_6358);
																	}
																	BgL_res2597z00_6362 =
																		(BgL_arg1808z00_6370 == BgL_classz00_6356);
																}
															else
																{	/* Llib/object.scm 1365 */
																	BgL_res2597z00_6362 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test3769z00_10136 = BgL_res2597z00_6362;
										}
								}
							else
								{	/* Llib/object.scm 1333 */
									BgL_test3769z00_10136 = ((bool_t) 0);
								}
						}
						if (BgL_test3769z00_10136)
							{	/* Llib/object.scm 1475 */
								BgL_auxz00_10135 = ((BgL_objectz00_bglt) BgL_obj1z00_5669);
							}
						else
							{
								obj_t BgL_auxz00_10161;

								BgL_auxz00_10161 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string2853z00zz__objectz00, BINT(64288L),
									BGl_string3161z00zz__objectz00,
									BGl_string2903z00zz__objectz00, BgL_obj1z00_5669);
								FAILURE(BgL_auxz00_10161, BFALSE, BFALSE);
							}
					}
					BgL_tmpz00_10134 =
						BGl_objectzd2equalzf3z21zz__objectz00(BgL_auxz00_10135,
						BgL_auxz00_10165);
				}
				return BBOOL(BgL_tmpz00_10134);
			}
		}

	}



/* exception-notify */
	BGL_EXPORTED_DEF obj_t BGl_exceptionzd2notifyzd2zz__objectz00(obj_t
		BgL_excz00_175)
	{
		{	/* Llib/object.scm 1495 */
			if (BGL_OBJECTP(BgL_excz00_175))
				{	/* Llib/object.scm 1495 */
					obj_t BgL_method1408z00_2560;

					{	/* Llib/object.scm 1495 */
						obj_t BgL_res2648z00_4736;

						{	/* Llib/object.scm 1284 */
							long BgL_objzd2classzd2numz00_4707;

							BgL_objzd2classzd2numz00_4707 =
								BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_excz00_175));
							{	/* Llib/object.scm 1285 */
								obj_t BgL_arg1793z00_4708;

								BgL_arg1793z00_4708 =
									PROCEDURE_REF(BGl_exceptionzd2notifyzd2envz00zz__objectz00,
									(int) (1L));
								{	/* Llib/object.scm 1285 */
									int BgL_offsetz00_4711;

									BgL_offsetz00_4711 = (int) (BgL_objzd2classzd2numz00_4707);
									{	/* Llib/object.scm 931 */
										long BgL_offsetz00_4712;

										BgL_offsetz00_4712 =
											((long) (BgL_offsetz00_4711) - OBJECT_TYPE);
										{	/* Llib/object.scm 931 */
											long BgL_modz00_4713;

											BgL_modz00_4713 =
												(BgL_offsetz00_4712 >> (int) ((long) ((int) (4L))));
											{	/* Llib/object.scm 932 */
												long BgL_restz00_4715;

												BgL_restz00_4715 =
													(BgL_offsetz00_4712 &
													(long) (
														(int) (
															((long) (
																	(int) (
																		(1L <<
																			(int) ((long) ((int) (4L)))))) - 1L))));
												{	/* Llib/object.scm 933 */

													{	/* Llib/object.scm 934 */
														obj_t BgL_bucketz00_4717;

														BgL_bucketz00_4717 =
															VECTOR_REF(
															((obj_t) BgL_arg1793z00_4708), BgL_modz00_4713);
														BgL_res2648z00_4736 =
															VECTOR_REF(
															((obj_t) BgL_bucketz00_4717), BgL_restz00_4715);
						}}}}}}}}
						BgL_method1408z00_2560 = BgL_res2648z00_4736;
					}
					return BGL_PROCEDURE_CALL1(BgL_method1408z00_2560, BgL_excz00_175);
				}
			else
				{	/* Llib/object.scm 1495 */
					obj_t BgL_fun2207z00_2561;

					BgL_fun2207z00_2561 =
						PROCEDURE_REF(BGl_exceptionzd2notifyzd2envz00zz__objectz00,
						(int) (0L));
					return BGL_PROCEDURE_CALL1(BgL_fun2207z00_2561, BgL_excz00_175);
				}
		}

	}



/* &exception-notify */
	obj_t BGl_z62exceptionzd2notifyzb0zz__objectz00(obj_t BgL_envz00_5671,
		obj_t BgL_excz00_5672)
	{
		{	/* Llib/object.scm 1495 */
			return BGl_exceptionzd2notifyzd2zz__objectz00(BgL_excz00_5672);
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__objectz00(void)
	{
		{	/* Llib/object.scm 17 */
			{	/* Llib/object.scm 17 */
				obj_t BgL_res2650z00_4757;

				{	/* Llib/object.scm 17 */
					obj_t BgL_classz00_4738;

					BgL_classz00_4738 = BGl_z62errorz62zz__objectz00;
					if (BGL_CLASSP(BgL_classz00_4738))
						{	/* Llib/object.scm 1249 */
							bool_t BgL_test3781z00_10237;

							{	/* Llib/object.scm 1249 */
								bool_t BgL_test3782z00_10238;

								{	/* Llib/object.scm 1249 */
									int BgL_arg1768z00_4742;
									int BgL_arg1769z00_4743;

									BgL_arg1768z00_4742 =
										PROCEDURE_ARITY
										(BGl_exceptionzd2notifyzd2envz00zz__objectz00);
									BgL_arg1769z00_4743 =
										PROCEDURE_ARITY(BGl_proc3162z00zz__objectz00);
									BgL_test3782z00_10238 =
										((long) (BgL_arg1768z00_4742) ==
										(long) (BgL_arg1769z00_4743));
								}
								if (BgL_test3782z00_10238)
									{	/* Llib/object.scm 1249 */
										BgL_test3781z00_10237 = ((bool_t) 0);
									}
								else
									{	/* Llib/object.scm 1250 */
										int BgL_arg1767z00_4748;

										BgL_arg1767z00_4748 =
											PROCEDURE_ARITY
											(BGl_exceptionzd2notifyzd2envz00zz__objectz00);
										BgL_test3781z00_10237 =
											((long) (BgL_arg1767z00_4748) >= 0L);
							}}
							if (BgL_test3781z00_10237)
								{	/* Llib/object.scm 1252 */
									obj_t BgL_arg1763z00_4751;
									int BgL_arg1764z00_4752;

									{	/* Llib/object.scm 1252 */
										int BgL_arg1765z00_4753;

										BgL_arg1765z00_4753 =
											PROCEDURE_ARITY
											(BGl_exceptionzd2notifyzd2envz00zz__objectz00);
										{	/* Llib/object.scm 1251 */
											obj_t BgL_list1766z00_4755;

											BgL_list1766z00_4755 =
												MAKE_YOUNG_PAIR(BINT(BgL_arg1765z00_4753), BNIL);
											BgL_arg1763z00_4751 =
												BGl_formatz00zz__r4_output_6_10_3z00
												(BGl_string2937z00zz__objectz00, BgL_list1766z00_4755);
									}}
									BgL_arg1764z00_4752 =
										PROCEDURE_ARITY(BGl_proc3162z00zz__objectz00);
									BgL_res2650z00_4757 =
										BGl_errorz00zz__errorz00(BGl_string3163z00zz__objectz00,
										BgL_arg1763z00_4751, BINT(BgL_arg1764z00_4752));
								}
							else
								{	/* Llib/object.scm 1249 */
									BgL_res2650z00_4757 =
										BGl_z52addzd2methodz12z92zz__objectz00
										(BGl_exceptionzd2notifyzd2envz00zz__objectz00,
										BgL_classz00_4738, BGl_proc3162z00zz__objectz00);
								}
						}
					else
						{	/* Llib/object.scm 1247 */
							BgL_res2650z00_4757 =
								BGl_errorz00zz__errorz00(BGl_string3163z00zz__objectz00,
								BGl_string2938z00zz__objectz00, BgL_classz00_4738);
						}
				}
				BgL_res2650z00_4757;
			}
			{	/* Llib/object.scm 17 */
				obj_t BgL_res2657z00_4821;

				{	/* Llib/object.scm 17 */
					obj_t BgL_classz00_4802;

					BgL_classz00_4802 = BGl_z62iozd2writezd2errorz62zz__objectz00;
					if (BGL_CLASSP(BgL_classz00_4802))
						{	/* Llib/object.scm 1249 */
							bool_t BgL_test3784z00_10258;

							{	/* Llib/object.scm 1249 */
								bool_t BgL_test3785z00_10259;

								{	/* Llib/object.scm 1249 */
									int BgL_arg1768z00_4806;
									int BgL_arg1769z00_4807;

									BgL_arg1768z00_4806 =
										PROCEDURE_ARITY
										(BGl_exceptionzd2notifyzd2envz00zz__objectz00);
									BgL_arg1769z00_4807 =
										PROCEDURE_ARITY(BGl_proc3164z00zz__objectz00);
									BgL_test3785z00_10259 =
										((long) (BgL_arg1768z00_4806) ==
										(long) (BgL_arg1769z00_4807));
								}
								if (BgL_test3785z00_10259)
									{	/* Llib/object.scm 1249 */
										BgL_test3784z00_10258 = ((bool_t) 0);
									}
								else
									{	/* Llib/object.scm 1250 */
										int BgL_arg1767z00_4812;

										BgL_arg1767z00_4812 =
											PROCEDURE_ARITY
											(BGl_exceptionzd2notifyzd2envz00zz__objectz00);
										BgL_test3784z00_10258 =
											((long) (BgL_arg1767z00_4812) >= 0L);
							}}
							if (BgL_test3784z00_10258)
								{	/* Llib/object.scm 1252 */
									obj_t BgL_arg1763z00_4815;
									int BgL_arg1764z00_4816;

									{	/* Llib/object.scm 1252 */
										int BgL_arg1765z00_4817;

										BgL_arg1765z00_4817 =
											PROCEDURE_ARITY
											(BGl_exceptionzd2notifyzd2envz00zz__objectz00);
										{	/* Llib/object.scm 1251 */
											obj_t BgL_list1766z00_4819;

											BgL_list1766z00_4819 =
												MAKE_YOUNG_PAIR(BINT(BgL_arg1765z00_4817), BNIL);
											BgL_arg1763z00_4815 =
												BGl_formatz00zz__r4_output_6_10_3z00
												(BGl_string2937z00zz__objectz00, BgL_list1766z00_4819);
									}}
									BgL_arg1764z00_4816 =
										PROCEDURE_ARITY(BGl_proc3164z00zz__objectz00);
									BgL_res2657z00_4821 =
										BGl_errorz00zz__errorz00(BGl_string3163z00zz__objectz00,
										BgL_arg1763z00_4815, BINT(BgL_arg1764z00_4816));
								}
							else
								{	/* Llib/object.scm 1249 */
									BgL_res2657z00_4821 =
										BGl_z52addzd2methodz12z92zz__objectz00
										(BGl_exceptionzd2notifyzd2envz00zz__objectz00,
										BgL_classz00_4802, BGl_proc3164z00zz__objectz00);
								}
						}
					else
						{	/* Llib/object.scm 1247 */
							BgL_res2657z00_4821 =
								BGl_errorz00zz__errorz00(BGl_string3163z00zz__objectz00,
								BGl_string2938z00zz__objectz00, BgL_classz00_4802);
						}
				}
				BgL_res2657z00_4821;
			}
			{	/* Llib/object.scm 17 */
				obj_t BgL_res2658z00_4841;

				{	/* Llib/object.scm 17 */
					obj_t BgL_classz00_4822;

					BgL_classz00_4822 = BGl_z62warningz62zz__objectz00;
					if (BGL_CLASSP(BgL_classz00_4822))
						{	/* Llib/object.scm 1249 */
							bool_t BgL_test3787z00_10279;

							{	/* Llib/object.scm 1249 */
								bool_t BgL_test3788z00_10280;

								{	/* Llib/object.scm 1249 */
									int BgL_arg1768z00_4826;
									int BgL_arg1769z00_4827;

									BgL_arg1768z00_4826 =
										PROCEDURE_ARITY
										(BGl_exceptionzd2notifyzd2envz00zz__objectz00);
									BgL_arg1769z00_4827 =
										PROCEDURE_ARITY(BGl_proc3165z00zz__objectz00);
									BgL_test3788z00_10280 =
										((long) (BgL_arg1768z00_4826) ==
										(long) (BgL_arg1769z00_4827));
								}
								if (BgL_test3788z00_10280)
									{	/* Llib/object.scm 1249 */
										BgL_test3787z00_10279 = ((bool_t) 0);
									}
								else
									{	/* Llib/object.scm 1250 */
										int BgL_arg1767z00_4832;

										BgL_arg1767z00_4832 =
											PROCEDURE_ARITY
											(BGl_exceptionzd2notifyzd2envz00zz__objectz00);
										BgL_test3787z00_10279 =
											((long) (BgL_arg1767z00_4832) >= 0L);
							}}
							if (BgL_test3787z00_10279)
								{	/* Llib/object.scm 1252 */
									obj_t BgL_arg1763z00_4835;
									int BgL_arg1764z00_4836;

									{	/* Llib/object.scm 1252 */
										int BgL_arg1765z00_4837;

										BgL_arg1765z00_4837 =
											PROCEDURE_ARITY
											(BGl_exceptionzd2notifyzd2envz00zz__objectz00);
										{	/* Llib/object.scm 1251 */
											obj_t BgL_list1766z00_4839;

											BgL_list1766z00_4839 =
												MAKE_YOUNG_PAIR(BINT(BgL_arg1765z00_4837), BNIL);
											BgL_arg1763z00_4835 =
												BGl_formatz00zz__r4_output_6_10_3z00
												(BGl_string2937z00zz__objectz00, BgL_list1766z00_4839);
									}}
									BgL_arg1764z00_4836 =
										PROCEDURE_ARITY(BGl_proc3165z00zz__objectz00);
									BgL_res2658z00_4841 =
										BGl_errorz00zz__errorz00(BGl_string3163z00zz__objectz00,
										BgL_arg1763z00_4835, BINT(BgL_arg1764z00_4836));
								}
							else
								{	/* Llib/object.scm 1249 */
									BgL_res2658z00_4841 =
										BGl_z52addzd2methodz12z92zz__objectz00
										(BGl_exceptionzd2notifyzd2envz00zz__objectz00,
										BgL_classz00_4822, BGl_proc3165z00zz__objectz00);
								}
						}
					else
						{	/* Llib/object.scm 1247 */
							BgL_res2658z00_4841 =
								BGl_errorz00zz__errorz00(BGl_string3163z00zz__objectz00,
								BGl_string2938z00zz__objectz00, BgL_classz00_4822);
						}
				}
				return BgL_res2658z00_4841;
			}
		}

	}



/* &exception-notify-&wa1416 */
	obj_t BGl_z62exceptionzd2notifyzd2z62wa1416z00zz__objectz00(obj_t
		BgL_envz00_5676, obj_t BgL_excz00_5677)
	{
		{	/* Llib/object.scm 1527 */
			return
				BGl_warningzd2notifyzd2zz__errorz00(
				((obj_t) ((BgL_z62warningz62_bglt) BgL_excz00_5677)));
		}

	}



/* &exception-notify-&io1414 */
	obj_t BGl_z62exceptionzd2notifyzd2z62io1414z00zz__objectz00(obj_t
		BgL_envz00_5678, obj_t BgL_excz00_5679)
	{
		{	/* Llib/object.scm 1519 */
			{	/* Llib/object.scm 1521 */
				bool_t BgL_test3789z00_10301;

				{	/* Llib/object.scm 1521 */
					obj_t BgL_arg2211z00_6390;
					obj_t BgL_arg2212z00_6391;

					BgL_arg2211z00_6390 =
						(((BgL_z62errorz62_bglt) COBJECT(
								((BgL_z62errorz62_bglt)
									((BgL_z62iozd2writezd2errorz62_bglt) BgL_excz00_5679))))->
						BgL_objz00);
					{	/* Llib/object.scm 1521 */
						obj_t BgL_tmpz00_10305;

						BgL_tmpz00_10305 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_arg2212z00_6391 = BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_10305);
					}
					BgL_test3789z00_10301 = (BgL_arg2211z00_6390 == BgL_arg2212z00_6391);
				}
				if (BgL_test3789z00_10301)
					{	/* Llib/object.scm 1521 */
						return BFALSE;
					}
				else
					{	/* Llib/object.scm 1519 */
						obj_t BgL_nextzd2method1413zd2_6392;

						BgL_nextzd2method1413zd2_6392 =
							BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
							((BgL_objectz00_bglt)
								((BgL_z62iozd2writezd2errorz62_bglt) BgL_excz00_5679)),
							BGl_exceptionzd2notifyzd2envz00zz__objectz00,
							BGl_z62iozd2writezd2errorz62zz__objectz00);
						return BGL_PROCEDURE_CALL1(BgL_nextzd2method1413zd2_6392,
							((obj_t) ((BgL_z62iozd2writezd2errorz62_bglt) BgL_excz00_5679)));
					}
			}
		}

	}



/* &exception-notify-&er1412 */
	obj_t BGl_z62exceptionzd2notifyzd2z62er1412z00zz__objectz00(obj_t
		BgL_envz00_5680, obj_t BgL_excz00_5681)
	{
		{	/* Llib/object.scm 1513 */
			return
				BGl_errorzd2notifyzd2zz__errorz00(
				((obj_t) ((BgL_z62errorz62_bglt) BgL_excz00_5681)));
		}

	}

#ifdef __cplusplus
}
#endif
