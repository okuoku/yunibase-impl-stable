/*===========================================================================*/
/*   (Llib/hash.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Llib/hash.scm -indent -o objs/obj_u/Llib/hash.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___HASH_TYPE_DEFINITIONS
#define BGL___HASH_TYPE_DEFINITIONS

/* object type definitions */

#endif													// BGL___HASH_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_plainzd2hashtablezd2getz00zz__hashz00(obj_t, obj_t);
	extern long bgl_symbol_hash_number(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_hashtablezd2clearz12zc0zz__hashz00(obj_t);
	static obj_t BGl_symbol2560z00zz__hashz00 = BUNSPEC;
	static obj_t BGl_symbol2562z00zz__hashz00 = BUNSPEC;
	static obj_t BGl_symbol2564z00zz__hashz00 = BUNSPEC;
	static obj_t BGl_list2532z00zz__hashz00 = BUNSPEC;
	extern obj_t BGl_weakzd2hashtablezd2putz12z12zz__weakhashz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol2567z00zz__hashz00 = BUNSPEC;
	extern long bgl_pointer_hashnumber(obj_t, long);
	static obj_t BGl_z62openzd2stringzd2hashtablezd2addz12za2zz__hashz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_weakzd2hashtablezd2getz00zz__weakhashz00(obj_t, obj_t);
	extern obj_t BGl_weakzd2hashtablezd2forzd2eachzd2zz__weakhashz00(obj_t,
		obj_t);
	static bool_t BGl_plainzd2hashtablezd2containszf3zf3zz__hashz00(obj_t, obj_t);
	extern long bgl_symbol_hash_number_persistent(obj_t);
	static obj_t BGl_z62openzd2stringzd2hashtablezd2putz12za2zz__hashz00(obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_openzd2stringzd2hashtablezd2getzd2zz__hashz00(obj_t, obj_t);
	extern obj_t BGl_weakzd2hashtablezd2keyzd2listzd2zz__weakhashz00(obj_t);
	static obj_t
		BGl_z62openzd2stringzd2hashtablezd2forzd2eachz62zz__hashz00(obj_t, obj_t,
		obj_t);
	extern obj_t bstring_to_keyword(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__hashz00 = BUNSPEC;
	static long BGl_defaultzd2maxzd2bucketzd2lengthzd2zz__hashz00 = 0L;
	static obj_t BGl_plainzd2hashtablezd2ze3listze3zz__hashz00(obj_t);
	static obj_t BGl_plainzd2hashtablezd2updatez12z12zz__hashz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62hashtablezd2weakzd2keyszf3z91zz__hashz00(obj_t, obj_t);
	extern obj_t BGl_weakzd2hashtablezd2updatez12z12zz__weakhashz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_makezd2hashtablezd2zz__hashz00(obj_t);
	static bool_t BGl_openzd2stringzd2hashtablezd2rehashz12zc0zz__hashz00(obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__hashz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__weakhashz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_z62hashtablezd2containszf3z43zz__hashz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_plainzd2hashtablezd2clearz12z12zz__hashz00(obj_t);
	extern long bgl_string_hash_persistent(char *, int, int);
	extern long bgl_keyword_hash_number(obj_t);
	static obj_t BGl_z62hashtablezd2removez12za2zz__hashz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62hashtablezd2ze3vectorz53zz__hashz00(obj_t, obj_t);
	BGL_EXPORTED_DECL long BGl_hashtablezd2siza7ez75zz__hashz00(obj_t);
	static obj_t BGl_z62hashtablezd2collisionszb0zz__hashz00(obj_t, obj_t);
	BGL_EXPORTED_DECL long BGl_stringzd2hashzd2numberz00zz__hashz00(obj_t);
	static obj_t BGl_z62hashtablezd2getzb0zz__hashz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_vectorzd2fillz12zc0zz__r4_vectors_6_8z00(obj_t, obj_t, long,
		long);
	static obj_t BGl_z62hashtablezd2addz12za2zz__hashz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_openzd2stringzd2hashtablezd2removez12zc0zz__hashz00(obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zz__hashz00(void);
	static obj_t BGl_openzd2stringzd2hashtablezd2ze3vectorz31zz__hashz00(obj_t);
	extern obj_t BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(long,
		long);
	static obj_t BGl_z62hashtablezd2putz12za2zz__hashz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62openzd2stringzd2hashtablezd2mapzb0zz__hashz00(obj_t,
		obj_t, obj_t);
	extern long bgl_obj_hash_number(obj_t);
	static obj_t BGl_z62hashtablezd2filterz12za2zz__hashz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	extern obj_t create_struct(obj_t, int);
	static obj_t BGl_z62getzd2pointerzd2hashnumberz62zz__hashz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_hashtablezd2collisionszd2zz__hashz00(obj_t);
	static obj_t BGl_cnstzd2initzd2zz__hashz00(void);
	static long BGl_objzd2hashze70z35zz__hashz00(obj_t);
	static obj_t BGl_genericzd2initzd2zz__hashz00(void);
	extern long bgl_list_length(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_openzd2stringzd2hashtablezd2filterz12zc0zz__hashz00(obj_t, obj_t);
	static obj_t BGl_z62stringzd2hashzd2numberz62zz__hashz00(obj_t, obj_t);
	static obj_t BGl_z62hashtablezd2clearz12za2zz__hashz00(obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__hashz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__hashz00(void);
	static obj_t BGl_openzd2stringzd2hashtablezd2ze3listz31zz__hashz00(obj_t);
	static obj_t BGl_objectzd2initzd2zz__hashz00(void);
	extern obj_t BGl_2za2za2zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_hashtablezd2forzd2eachz00zz__hashz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_openzd2stringzd2hashtablezd2forzd2eachz00zz__hashz00(obj_t, obj_t);
	extern obj_t BGl_homogeneouszd2vectorzd2infoz00zz__srfi4z00(obj_t);
	static obj_t BGl_z62stringzd2hashtablezd2getz62zz__hashz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_hashtablezd2removez12zc0zz__hashz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_hashtablezd2ze3vectorz31zz__hashz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_hashtablezd2keyzd2listz00zz__hashz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_createzd2hashtablezd2zz__hashz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_openzd2stringzd2hashtablezd2keyzd2listz00zz__hashz00(obj_t);
	extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31368ze3ze5zz__hashz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_hashtablezd2mapzd2zz__hashz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_stringzd2hashtablezd2putz12z12zz__hashz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_plainzd2hashtablezd2mapz00zz__hashz00(obj_t, obj_t);
	extern obj_t string_append_3(obj_t, obj_t, obj_t);
	static obj_t BGl__stringzd2hashzd2zz__hashz00(obj_t, obj_t);
	extern obj_t BGl_weakzd2hashtablezd2mapz00zz__weakhashz00(obj_t, obj_t);
	static obj_t BGl_z62openzd2stringzd2hashtablezd2removez12za2zz__hashz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_keyword2533z00zz__hashz00 = BUNSPEC;
	static obj_t BGl_keyword2535z00zz__hashz00 = BUNSPEC;
	static obj_t BGl_keyword2537z00zz__hashz00 = BUNSPEC;
	static obj_t BGl_keyword2539z00zz__hashz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_openzd2stringzd2hashtablezd2mapzd2zz__hashz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_stringzd2hashtablezd2getz00zz__hashz00(obj_t,
		obj_t);
	static obj_t BGl_z62getzd2hashnumberzb0zz__hashz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_hashtablezd2filterz12zc0zz__hashz00(obj_t, obj_t);
	static obj_t BGl_z62hashtablezd2forzd2eachz62zz__hashz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_filterz12z12zz__r4_control_features_6_9z00(obj_t, obj_t);
	extern long bgl_keyword_hash_number_persistent(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_hashtablezf3zf3zz__hashz00(obj_t);
	extern bool_t BGl_weakzd2hashtablezd2containszf3zf3zz__weakhashz00(obj_t,
		obj_t);
	static obj_t BGl_keyword2541z00zz__hashz00 = BUNSPEC;
	static obj_t BGl_z62hashtablezd2keyzd2listz62zz__hashz00(obj_t, obj_t);
	extern long bgl_foreign_hash_number(obj_t);
	static obj_t BGl_keyword2543z00zz__hashz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31556ze3ze5zz__hashz00(obj_t, obj_t);
	static obj_t BGl_keyword2545z00zz__hashz00 = BUNSPEC;
	static obj_t BGl_keyword2547z00zz__hashz00 = BUNSPEC;
	extern obj_t bstring_to_symbol(obj_t);
	extern obj_t make_vector(long, obj_t);
	static obj_t BGl_z62hashtablezd2updatez12za2zz__hashz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_hashtablezd2addz12zc0zz__hashz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_openzd2stringzd2hashtablezd2clearz12zc0zz__hashz00(obj_t);
	extern obj_t BGl_weakzd2hashtablezd2ze3listze3zz__weakhashz00(obj_t);
	static obj_t BGl_z62openzd2stringzd2hashtablezd2filterz12za2zz__hashz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_hashtablezd2putz12zc0zz__hashz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_hashtablezd2containszf3z21zz__hashz00(obj_t,
		obj_t);
	static obj_t BGl_z62getzd2hashnumberzd2persistentz62zz__hashz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_openzd2stringzd2hashtablezd2updatez12zc0zz__hashz00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_z42openzd2stringzd2hashtablezd2getz90zz__hashz00(obj_t, char *);
	static obj_t BGl_methodzd2initzd2zz__hashz00(void);
	static obj_t BGl_plainzd2hashtablezd2addz12z12zz__hashz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL long BGl_stringzd2hashzd2zz__hashz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL long BGl_getzd2pointerzd2hashnumberz00zz__hashz00(obj_t,
		long);
	static bool_t BGl_plainzd2hashtablezd2removez12z12zz__hashz00(obj_t, obj_t);
	static obj_t
		BGl_openzd2stringzd2hashtablezd2putzf2hashz12z32zz__hashz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_plainzd2hashtablezd2ze3vectorze3zz__hashz00(obj_t);
	static obj_t BGl_z62hashtablezf3z91zz__hashz00(obj_t, obj_t);
	static obj_t BGl_plainzd2hashtablezd2putz12z12zz__hashz00(obj_t, obj_t,
		obj_t);
	static long BGl_defaultzd2hashtablezd2bucketzd2lengthzd2zz__hashz00 = 0L;
	static obj_t BGl_z62stringzd2hashtablezd2putz12z70zz__hashz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62hashtablezd2mapzb0zz__hashz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_weakzd2hashtablezd2removez12z12zz__weakhashz00(obj_t, obj_t);
	static bool_t BGl_plainzd2hashtablezd2forzd2eachzd2zz__hashz00(obj_t, obj_t);
	extern obj_t BGl_weakzd2hashtablezd2ze3vectorze3zz__weakhashz00(obj_t);
	static obj_t BGl_symbol2607z00zz__hashz00 = BUNSPEC;
	static obj_t BGl_symbol2527z00zz__hashz00 = BUNSPEC;
	static obj_t BGl_plainzd2hashtablezd2keyzd2listzd2zz__hashz00(obj_t);
	extern long bgl_date_to_seconds(obj_t);
	static obj_t BGl_plainzd2hashtablezd2collisionsz00zz__hashz00(obj_t);
	static obj_t BGl_plainzd2hashtablezd2expandz12z12zz__hashz00(obj_t);
	static obj_t BGl_z62makezd2hashtablezb0zz__hashz00(obj_t, obj_t);
	extern obj_t obj_to_string(obj_t, obj_t);
	static obj_t BGl_symbol2530z00zz__hashz00 = BUNSPEC;
	static obj_t BGl_plainzd2hashtablezd2filterz12z12zz__hashz00(obj_t, obj_t);
	static obj_t BGl_z62openzd2stringzd2hashtablezd2getzb0zz__hashz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62z42openzd2stringzd2hashtablezd2getzf2zz__hashz00(obj_t,
		obj_t, obj_t);
	extern long bgl_string_hash(char *, int, int);
	static obj_t BGl_z62hashtablezd2weakzd2datazf3z91zz__hashz00(obj_t, obj_t);
	extern obj_t BGl_weakzd2hashtablezd2filterz12z12zz__weakhashz00(obj_t, obj_t);
	static obj_t BGl_z62hashtablezd2siza7ez17zz__hashz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_openzd2stringzd2hashtablezd2containszf3z21zz__hashz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_hashtablezd2updatez12zc0zz__hashz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_openzd2stringzd2hashtablezd2siza7ezd2incz12zb5zz__hashz00(obj_t);
	static obj_t
		BGl_z62openzd2stringzd2hashtablezd2containszf3z43zz__hashz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_openzd2stringzd2hashtablezd2addz12zc0zz__hashz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_symbol2549z00zz__hashz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_openzd2stringzd2hashtablezd2putz12zc0zz__hashz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL long BGl_getzd2hashnumberzd2persistentz00zz__hashz00(obj_t);
	BGL_EXPORTED_DECL long BGl_getzd2hashnumberzd2zz__hashz00(obj_t);
	static obj_t BGl_z62openzd2stringzd2hashtablezd2updatez12za2zz__hashz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_hashtablezd2weakzd2datazf3zf3zz__hashz00(obj_t);
	static obj_t BGl_symbol2556z00zz__hashz00 = BUNSPEC;
	static obj_t BGl_symbol2558z00zz__hashz00 = BUNSPEC;
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl__createzd2hashtablezd2zz__hashz00(obj_t, obj_t);
	extern long BGl_objectzd2hashnumberzd2zz__objectz00(BgL_objectz00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_hashtablezd2ze3listz31zz__hashz00(obj_t);
	extern obj_t BGl_weakzd2hashtablezd2clearz12z12zz__weakhashz00(obj_t);
	extern obj_t BGl_weakzd2hashtablezd2addz12z12zz__weakhashz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62hashtablezd2ze3listz53zz__hashz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_hashtablezd2getzd2zz__hashz00(obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_hashtablezd2updatez12zd2envz12zz__hashz00,
		BgL_bgl_za762hashtableza7d2u2613z00,
		BGl_z62hashtablezd2updatez12za2zz__hashz00, 0L, BUNSPEC, 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_openzd2stringzd2hashtablezd2filterz12zd2envz12zz__hashz00,
		BgL_bgl_za762openza7d2string2614z00,
		BGl_z62openzd2stringzd2hashtablezd2filterz12za2zz__hashz00, 0L, BUNSPEC, 2);
	extern obj_t BGl_listzd2envzd2zz__r4_pairs_and_lists_6_3z00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_hashtablezd2keyzd2listzd2envzd2zz__hashz00,
		BgL_bgl_za762hashtableza7d2k2615z00,
		BGl_z62hashtablezd2keyzd2listz62zz__hashz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_hashtablezd2siza7ezd2envza7zz__hashz00,
		BgL_bgl_za762hashtableza7d2s2616z00,
		BGl_z62hashtablezd2siza7ez17zz__hashz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2hashtablezd2putz12zd2envzc0zz__hashz00,
		BgL_bgl_za762stringza7d2hash2617z00,
		BGl_z62stringzd2hashtablezd2putz12z70zz__hashz00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_hashtablezd2mapzd2envz00zz__hashz00,
		BgL_bgl_za762hashtableza7d2m2618z00, BGl_z62hashtablezd2mapzb0zz__hashz00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_hashtablezd2getzd2envz00zz__hashz00,
		BgL_bgl_za762hashtableza7d2g2619z00, BGl_z62hashtablezd2getzb0zz__hashz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_hashtablezd2clearz12zd2envz12zz__hashz00,
		BgL_bgl_za762hashtableza7d2c2620z00,
		BGl_z62hashtablezd2clearz12za2zz__hashz00, 0L, BUNSPEC, 1);
	extern obj_t BGl_stringzd3zf3zd2envzf2zz__r4_strings_6_7z00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_hashtablezf3zd2envz21zz__hashz00,
		BgL_bgl_za762hashtableza7f3za72621za7, BGl_z62hashtablezf3z91zz__hashz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_hashtablezd2collisionszd2envz00zz__hashz00,
		BgL_bgl_za762hashtableza7d2c2622z00,
		BGl_z62hashtablezd2collisionszb0zz__hashz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_openzd2stringzd2hashtablezd2containszf3zd2envzf3zz__hashz00,
		BgL_bgl_za762openza7d2string2623z00,
		BGl_z62openzd2stringzd2hashtablezd2containszf3z43zz__hashz00, 0L, BUNSPEC,
		2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_hashtablezd2addz12zd2envz12zz__hashz00,
		BgL_bgl_za762hashtableza7d2a2624z00,
		BGl_z62hashtablezd2addz12za2zz__hashz00, 0L, BUNSPEC, 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_openzd2stringzd2hashtablezd2addz12zd2envz12zz__hashz00,
		BgL_bgl_za762openza7d2string2625z00,
		BGl_z62openzd2stringzd2hashtablezd2addz12za2zz__hashz00, 0L, BUNSPEC, 5);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_hashtablezd2putz12zd2envz12zz__hashz00,
		BgL_bgl_za762hashtableza7d2p2626z00,
		BGl_z62hashtablezd2putz12za2zz__hashz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_openzd2stringzd2hashtablezd2putz12zd2envz12zz__hashz00,
		BgL_bgl_za762openza7d2string2627z00,
		BGl_z62openzd2stringzd2hashtablezd2putz12za2zz__hashz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_hashtablezd2containszf3zd2envzf3zz__hashz00,
		BgL_bgl_za762hashtableza7d2c2628z00,
		BGl_z62hashtablezd2containszf3z43zz__hashz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_getzd2hashnumberzd2envz00zz__hashz00,
		BgL_bgl_za762getza7d2hashnum2629z00, BGl_z62getzd2hashnumberzb0zz__hashz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_hashtablezd2removez12zd2envz12zz__hashz00,
		BgL_bgl_za762hashtableza7d2r2630z00,
		BGl_z62hashtablezd2removez12za2zz__hashz00, 0L, BUNSPEC, 2);
	      DEFINE_REAL(BGl_real2529z00zz__hashz00,
		BgL_bgl_real2529za700za7za7__h2631za7, 1.2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_hashtablezd2weakzd2keyszf3zd2envz21zz__hashz00,
		BgL_bgl_za762hashtableza7d2w2632z00,
		BGl_z62hashtablezd2weakzd2keyszf3z91zz__hashz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_hashtablezd2filterz12zd2envz12zz__hashz00,
		BgL_bgl_za762hashtableza7d2f2633z00,
		BGl_z62hashtablezd2filterz12za2zz__hashz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2600z00zz__hashz00,
		BgL_bgl_string2600za700za7za7_2634za7, "&hashtable-remove!", 18);
	      DEFINE_STRING(BGl_string2601z00zz__hashz00,
		BgL_bgl_string2601za700za7za7_2635za7, "&open-string-hashtable-remove!",
		30);
	      DEFINE_STRING(BGl_string2602z00zz__hashz00,
		BgL_bgl_string2602za700za7za7_2636za7,
		"Hashtable too large (new-len=~a/~a, size=~a)", 44);
	      DEFINE_STRING(BGl_string2603z00zz__hashz00,
		BgL_bgl_string2603za700za7za7_2637za7, "hashtable-put!", 14);
	      DEFINE_STRING(BGl_string2522z00zz__hashz00,
		BgL_bgl_string2522za700za7za7_2638za7, "make-hashtable", 14);
	      DEFINE_STRING(BGl_string2604z00zz__hashz00,
		BgL_bgl_string2604za700za7za7_2639za7, "&hashtable-collisions", 21);
	      DEFINE_STRING(BGl_string2523z00zz__hashz00,
		BgL_bgl_string2523za700za7za7_2640za7, "Illegal default bucket length", 29);
	      DEFINE_STRING(BGl_string2605z00zz__hashz00,
		BgL_bgl_string2605za700za7za7_2641za7, "index out of range [0..", 23);
	      DEFINE_STRING(BGl_string2524z00zz__hashz00,
		BgL_bgl_string2524za700za7za7_2642za7, "Illegal max bucket length", 25);
	      DEFINE_STRING(BGl_string2606z00zz__hashz00,
		BgL_bgl_string2606za700za7za7_2643za7, "]", 1);
	      DEFINE_STRING(BGl_string2525z00zz__hashz00,
		BgL_bgl_string2525za700za7za7_2644za7, "Illegal equality test", 21);
	      DEFINE_STRING(BGl_string2526z00zz__hashz00,
		BgL_bgl_string2526za700za7za7_2645za7, "Illegal hashnumber function", 27);
	      DEFINE_STRING(BGl_string2608z00zz__hashz00,
		BgL_bgl_string2608za700za7za7_2646za7, "ucs2-string-ref", 15);
	      DEFINE_STRING(BGl_string2609z00zz__hashz00,
		BgL_bgl_string2609za700za7za7_2647za7, "&get-pointer-hashnumber", 23);
	      DEFINE_STRING(BGl_string2528z00zz__hashz00,
		BgL_bgl_string2528za700za7za7_2648za7, "%hashtable", 10);
	      DEFINE_STRING(BGl_string2610z00zz__hashz00,
		BgL_bgl_string2610za700za7za7_2649za7, "_string-hash", 12);
	      DEFINE_STRING(BGl_string2611z00zz__hashz00,
		BgL_bgl_string2611za700za7za7_2650za7, "&string-hash-number", 19);
	      DEFINE_STRING(BGl_string2612z00zz__hashz00,
		BgL_bgl_string2612za700za7za7_2651za7, "__hash", 6);
	      DEFINE_STRING(BGl_string2531z00zz__hashz00,
		BgL_bgl_string2531za700za7za7_2652za7, "none", 4);
	      DEFINE_STRING(BGl_string2534z00zz__hashz00,
		BgL_bgl_string2534za700za7za7_2653za7, "bucket-expansion", 16);
	      DEFINE_STRING(BGl_string2536z00zz__hashz00,
		BgL_bgl_string2536za700za7za7_2654za7, "eqtest", 6);
	      DEFINE_STRING(BGl_string2538z00zz__hashz00,
		BgL_bgl_string2538za700za7za7_2655za7, "hash", 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_openzd2stringzd2hashtablezd2updatez12zd2envz12zz__hashz00,
		BgL_bgl_za762openza7d2string2656z00,
		BGl_z62openzd2stringzd2hashtablezd2updatez12za2zz__hashz00, 0L, BUNSPEC, 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_z42openzd2stringzd2hashtablezd2getzd2envz42zz__hashz00,
		BgL_bgl_za762za742openza7d2str2657za7,
		BGl_z62z42openzd2stringzd2hashtablezd2getzf2zz__hashz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_openzd2stringzd2hashtablezd2forzd2eachzd2envzd2zz__hashz00,
		BgL_bgl_za762openza7d2string2658z00,
		BGl_z62openzd2stringzd2hashtablezd2forzd2eachz62zz__hashz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_hashtablezd2weakzd2datazf3zd2envz21zz__hashz00,
		BgL_bgl_za762hashtableza7d2w2659z00,
		BGl_z62hashtablezd2weakzd2datazf3z91zz__hashz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_stringzd2hashzd2envz00zz__hashz00,
		BgL_bgl__stringza7d2hashza7d2660z00, opt_generic_entry,
		BGl__stringzd2hashzd2zz__hashz00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2pointerzd2hashnumberzd2envzd2zz__hashz00,
		BgL_bgl_za762getza7d2pointer2661z00,
		BGl_z62getzd2pointerzd2hashnumberz62zz__hashz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2540z00zz__hashz00,
		BgL_bgl_string2540za700za7za7_2662za7, "max-bucket-length", 17);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_createzd2hashtablezd2envz00zz__hashz00,
		BgL_bgl__createza7d2hashta2663za7, opt_generic_entry,
		BGl__createzd2hashtablezd2zz__hashz00, BFALSE, -1);
	      DEFINE_STRING(BGl_string2542z00zz__hashz00,
		BgL_bgl_string2542za700za7za7_2664za7, "max-length", 10);
	      DEFINE_STRING(BGl_string2544z00zz__hashz00,
		BgL_bgl_string2544za700za7za7_2665za7, "persistent", 10);
	      DEFINE_STRING(BGl_string2546z00zz__hashz00,
		BgL_bgl_string2546za700za7za7_2666za7, "size", 4);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_hashtablezd2ze3listzd2envze3zz__hashz00,
		BgL_bgl_za762hashtableza7d2za72667za7,
		BGl_z62hashtablezd2ze3listz53zz__hashz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2548z00zz__hashz00,
		BgL_bgl_string2548za700za7za7_2668za7, "weak", 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_stringzd2hashzd2numberzd2envzd2zz__hashz00,
		BgL_bgl_za762stringza7d2hash2669z00,
		BGl_z62stringzd2hashzd2numberz62zz__hashz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2550z00zz__hashz00,
		BgL_bgl_string2550za700za7za7_2670za7, "create-hashtable", 16);
	      DEFINE_STRING(BGl_string2551z00zz__hashz00,
		BgL_bgl_string2551za700za7za7_2671za7, "Illegal keyword argument", 24);
	      DEFINE_STRING(BGl_string2552z00zz__hashz00,
		BgL_bgl_string2552za700za7za7_2672za7,
		"wrong number of arguments: [0..8] expected, provided", 52);
	      DEFINE_STRING(BGl_string2553z00zz__hashz00,
		BgL_bgl_string2553za700za7za7_2673za7, "/tmp/bigloo/runtime/Llib/hash.scm",
		33);
	      DEFINE_STRING(BGl_string2554z00zz__hashz00,
		BgL_bgl_string2554za700za7za7_2674za7, "_create-hashtable", 17);
	      DEFINE_STRING(BGl_string2555z00zz__hashz00,
		BgL_bgl_string2555za700za7za7_2675za7, "bint", 4);
	      DEFINE_STRING(BGl_string2557z00zz__hashz00,
		BgL_bgl_string2557za700za7za7_2676za7, "keys", 4);
	      DEFINE_STRING(BGl_string2559z00zz__hashz00,
		BgL_bgl_string2559za700za7za7_2677za7, "data", 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2hashtablezd2getzd2envzd2zz__hashz00,
		BgL_bgl_za762stringza7d2hash2678z00,
		BGl_z62stringzd2hashtablezd2getz62zz__hashz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2561z00zz__hashz00,
		BgL_bgl_string2561za700za7za7_2679za7, "both", 4);
	      DEFINE_STRING(BGl_string2563z00zz__hashz00,
		BgL_bgl_string2563za700za7za7_2680za7, "open-string", 11);
	      DEFINE_STRING(BGl_string2565z00zz__hashz00,
		BgL_bgl_string2565za700za7za7_2681za7, "string", 6);
	      DEFINE_STRING(BGl_string2566z00zz__hashz00,
		BgL_bgl_string2566za700za7za7_2682za7,
		"Persistent hashtable cannot use custom hash function", 52);
	      DEFINE_STRING(BGl_string2568z00zz__hashz00,
		BgL_bgl_string2568za700za7za7_2683za7,
		"Cannot provide eqtest for string hashtable", 42);
	      DEFINE_STRING(BGl_string2569z00zz__hashz00,
		BgL_bgl_string2569za700za7za7_2684za7,
		"Cannot provide hash for string hashtable", 40);
	      DEFINE_STRING(BGl_string2571z00zz__hashz00,
		BgL_bgl_string2571za700za7za7_2685za7, "&hashtable-weak-keys?", 21);
	      DEFINE_STRING(BGl_string2572z00zz__hashz00,
		BgL_bgl_string2572za700za7za7_2686za7, "struct", 6);
	      DEFINE_STRING(BGl_string2573z00zz__hashz00,
		BgL_bgl_string2573za700za7za7_2687za7, "&hashtable-weak-data?", 21);
	      DEFINE_STRING(BGl_string2574z00zz__hashz00,
		BgL_bgl_string2574za700za7za7_2688za7, "&hashtable-size", 15);
	      DEFINE_STRING(BGl_string2575z00zz__hashz00,
		BgL_bgl_string2575za700za7za7_2689za7, "&hashtable->vector", 18);
	      DEFINE_STRING(BGl_string2576z00zz__hashz00,
		BgL_bgl_string2576za700za7za7_2690za7, "&hashtable->list", 16);
	extern obj_t BGl_eqzf3zd2envz21zz__r4_equivalence_6_2z00;
	   
		 
		DEFINE_STRING(BGl_string2577z00zz__hashz00,
		BgL_bgl_string2577za700za7za7_2691za7, "&hashtable-key-list", 19);
	      DEFINE_STRING(BGl_string2578z00zz__hashz00,
		BgL_bgl_string2578za700za7za7_2692za7, "&hashtable-map", 14);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2hashtablezd2envz00zz__hashz00,
		BgL_bgl_za762makeza7d2hashta2693z00, va_generic_entry,
		BGl_z62makezd2hashtablezb0zz__hashz00, BUNSPEC, -1);
	      DEFINE_STRING(BGl_string2579z00zz__hashz00,
		BgL_bgl_string2579za700za7za7_2694za7, "procedure", 9);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2hashnumberzd2persistentzd2envzd2zz__hashz00,
		BgL_bgl_za762getza7d2hashnum2695z00,
		BGl_z62getzd2hashnumberzd2persistentz62zz__hashz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_hashtablezd2forzd2eachzd2envzd2zz__hashz00,
		BgL_bgl_za762hashtableza7d2f2696z00,
		BGl_z62hashtablezd2forzd2eachz62zz__hashz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_hashtablezd2ze3vectorzd2envze3zz__hashz00,
		BgL_bgl_za762hashtableza7d2za72697za7,
		BGl_z62hashtablezd2ze3vectorz53zz__hashz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2570z00zz__hashz00,
		BgL_bgl_za762za7c3za704anonymo2698za7,
		BGl_z62zc3z04anonymousza31368ze3ze5zz__hashz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2580z00zz__hashz00,
		BgL_bgl_string2580za700za7za7_2699za7, "&open-string-hashtable-map", 26);
	      DEFINE_STRING(BGl_string2581z00zz__hashz00,
		BgL_bgl_string2581za700za7za7_2700za7, "&hashtable-for-each", 19);
	      DEFINE_STRING(BGl_string2582z00zz__hashz00,
		BgL_bgl_string2582za700za7za7_2701za7, "&open-string-hashtable-for-each",
		31);
	      DEFINE_STRING(BGl_string2583z00zz__hashz00,
		BgL_bgl_string2583za700za7za7_2702za7, "&hashtable-filter!", 18);
	      DEFINE_STRING(BGl_string2584z00zz__hashz00,
		BgL_bgl_string2584za700za7za7_2703za7, "&open-string-hashtable-filter!",
		30);
	      DEFINE_STRING(BGl_string2585z00zz__hashz00,
		BgL_bgl_string2585za700za7za7_2704za7, "&hashtable-clear!", 17);
	      DEFINE_STRING(BGl_string2586z00zz__hashz00,
		BgL_bgl_string2586za700za7za7_2705za7, "&hashtable-contains?", 20);
	      DEFINE_STRING(BGl_string2587z00zz__hashz00,
		BgL_bgl_string2587za700za7za7_2706za7, "&open-string-hashtable-contains?",
		32);
	      DEFINE_STRING(BGl_string2588z00zz__hashz00,
		BgL_bgl_string2588za700za7za7_2707za7, "bstring", 7);
	      DEFINE_STRING(BGl_string2589z00zz__hashz00,
		BgL_bgl_string2589za700za7za7_2708za7, "&hashtable-get", 14);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_openzd2stringzd2hashtablezd2removez12zd2envz12zz__hashz00,
		BgL_bgl_za762openza7d2string2709z00,
		BGl_z62openzd2stringzd2hashtablezd2removez12za2zz__hashz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2590z00zz__hashz00,
		BgL_bgl_string2590za700za7za7_2710za7, "&string-hashtable-get", 21);
	      DEFINE_STRING(BGl_string2591z00zz__hashz00,
		BgL_bgl_string2591za700za7za7_2711za7, "&open-string-hashtable-get", 26);
	      DEFINE_STRING(BGl_string2592z00zz__hashz00,
		BgL_bgl_string2592za700za7za7_2712za7, "&$open-string-hashtable-get", 27);
	      DEFINE_STRING(BGl_string2593z00zz__hashz00,
		BgL_bgl_string2593za700za7za7_2713za7, "&hashtable-put!", 15);
	      DEFINE_STRING(BGl_string2594z00zz__hashz00,
		BgL_bgl_string2594za700za7za7_2714za7, "&open-string-hashtable-put!", 27);
	      DEFINE_STRING(BGl_string2595z00zz__hashz00,
		BgL_bgl_string2595za700za7za7_2715za7, "&string-hashtable-put!", 22);
	      DEFINE_STRING(BGl_string2596z00zz__hashz00,
		BgL_bgl_string2596za700za7za7_2716za7, "&hashtable-update!", 18);
	      DEFINE_STRING(BGl_string2597z00zz__hashz00,
		BgL_bgl_string2597za700za7za7_2717za7, "&open-string-hashtable-update!",
		30);
	      DEFINE_STRING(BGl_string2598z00zz__hashz00,
		BgL_bgl_string2598za700za7za7_2718za7, "&hashtable-add!", 15);
	      DEFINE_STRING(BGl_string2599z00zz__hashz00,
		BgL_bgl_string2599za700za7za7_2719za7, "&open-string-hashtable-add!", 27);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_openzd2stringzd2hashtablezd2mapzd2envz00zz__hashz00,
		BgL_bgl_za762openza7d2string2720z00,
		BGl_z62openzd2stringzd2hashtablezd2mapzb0zz__hashz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_openzd2stringzd2hashtablezd2getzd2envz00zz__hashz00,
		BgL_bgl_za762openza7d2string2721z00,
		BGl_z62openzd2stringzd2hashtablezd2getzb0zz__hashz00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_symbol2560z00zz__hashz00));
		     ADD_ROOT((void *) (&BGl_symbol2562z00zz__hashz00));
		     ADD_ROOT((void *) (&BGl_symbol2564z00zz__hashz00));
		     ADD_ROOT((void *) (&BGl_list2532z00zz__hashz00));
		     ADD_ROOT((void *) (&BGl_symbol2567z00zz__hashz00));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__hashz00));
		     ADD_ROOT((void *) (&BGl_keyword2533z00zz__hashz00));
		     ADD_ROOT((void *) (&BGl_keyword2535z00zz__hashz00));
		     ADD_ROOT((void *) (&BGl_keyword2537z00zz__hashz00));
		     ADD_ROOT((void *) (&BGl_keyword2539z00zz__hashz00));
		     ADD_ROOT((void *) (&BGl_keyword2541z00zz__hashz00));
		     ADD_ROOT((void *) (&BGl_keyword2543z00zz__hashz00));
		     ADD_ROOT((void *) (&BGl_keyword2545z00zz__hashz00));
		     ADD_ROOT((void *) (&BGl_keyword2547z00zz__hashz00));
		     ADD_ROOT((void *) (&BGl_symbol2607z00zz__hashz00));
		     ADD_ROOT((void *) (&BGl_symbol2527z00zz__hashz00));
		     ADD_ROOT((void *) (&BGl_symbol2530z00zz__hashz00));
		     ADD_ROOT((void *) (&BGl_symbol2549z00zz__hashz00));
		     ADD_ROOT((void *) (&BGl_symbol2556z00zz__hashz00));
		     ADD_ROOT((void *) (&BGl_symbol2558z00zz__hashz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__hashz00(long
		BgL_checksumz00_5652, char *BgL_fromz00_5653)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__hashz00))
				{
					BGl_requirezd2initializa7ationz75zz__hashz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__hashz00();
					BGl_cnstzd2initzd2zz__hashz00();
					BGl_importedzd2moduleszd2initz00zz__hashz00();
					return BGl_toplevelzd2initzd2zz__hashz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__hashz00(void)
	{
		{	/* Llib/hash.scm 18 */
			BGl_symbol2527z00zz__hashz00 =
				bstring_to_symbol(BGl_string2528z00zz__hashz00);
			BGl_symbol2530z00zz__hashz00 =
				bstring_to_symbol(BGl_string2531z00zz__hashz00);
			BGl_keyword2533z00zz__hashz00 =
				bstring_to_keyword(BGl_string2534z00zz__hashz00);
			BGl_keyword2535z00zz__hashz00 =
				bstring_to_keyword(BGl_string2536z00zz__hashz00);
			BGl_keyword2537z00zz__hashz00 =
				bstring_to_keyword(BGl_string2538z00zz__hashz00);
			BGl_keyword2539z00zz__hashz00 =
				bstring_to_keyword(BGl_string2540z00zz__hashz00);
			BGl_keyword2541z00zz__hashz00 =
				bstring_to_keyword(BGl_string2542z00zz__hashz00);
			BGl_keyword2543z00zz__hashz00 =
				bstring_to_keyword(BGl_string2544z00zz__hashz00);
			BGl_keyword2545z00zz__hashz00 =
				bstring_to_keyword(BGl_string2546z00zz__hashz00);
			BGl_keyword2547z00zz__hashz00 =
				bstring_to_keyword(BGl_string2548z00zz__hashz00);
			BGl_list2532z00zz__hashz00 =
				MAKE_YOUNG_PAIR(BGl_keyword2533z00zz__hashz00,
				MAKE_YOUNG_PAIR(BGl_keyword2535z00zz__hashz00,
					MAKE_YOUNG_PAIR(BGl_keyword2537z00zz__hashz00,
						MAKE_YOUNG_PAIR(BGl_keyword2539z00zz__hashz00,
							MAKE_YOUNG_PAIR(BGl_keyword2541z00zz__hashz00,
								MAKE_YOUNG_PAIR(BGl_keyword2543z00zz__hashz00,
									MAKE_YOUNG_PAIR(BGl_keyword2545z00zz__hashz00,
										MAKE_YOUNG_PAIR(BGl_keyword2547z00zz__hashz00, BNIL))))))));
			BGl_symbol2549z00zz__hashz00 =
				bstring_to_symbol(BGl_string2550z00zz__hashz00);
			BGl_symbol2556z00zz__hashz00 =
				bstring_to_symbol(BGl_string2557z00zz__hashz00);
			BGl_symbol2558z00zz__hashz00 =
				bstring_to_symbol(BGl_string2559z00zz__hashz00);
			BGl_symbol2560z00zz__hashz00 =
				bstring_to_symbol(BGl_string2561z00zz__hashz00);
			BGl_symbol2562z00zz__hashz00 =
				bstring_to_symbol(BGl_string2563z00zz__hashz00);
			BGl_symbol2564z00zz__hashz00 =
				bstring_to_symbol(BGl_string2565z00zz__hashz00);
			BGl_symbol2567z00zz__hashz00 =
				bstring_to_symbol(BGl_string2544z00zz__hashz00);
			return (BGl_symbol2607z00zz__hashz00 =
				bstring_to_symbol(BGl_string2608z00zz__hashz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__hashz00(void)
	{
		{	/* Llib/hash.scm 18 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__hashz00(void)
	{
		{	/* Llib/hash.scm 18 */
			BGl_defaultzd2hashtablezd2bucketzd2lengthzd2zz__hashz00 = 128L;
			return (BGl_defaultzd2maxzd2bucketzd2lengthzd2zz__hashz00 = 10L, BUNSPEC);
		}

	}



/* make-hashtable */
	BGL_EXPORTED_DEF obj_t BGl_makezd2hashtablezd2zz__hashz00(obj_t
		BgL_argsz00_42)
	{
		{	/* Llib/hash.scm 162 */
			{	/* Llib/hash.scm 163 */
				obj_t BgL_siza7eza7_1531;

				if (PAIRP(BgL_argsz00_42))
					{	/* Llib/hash.scm 164 */
						obj_t BgL_siza7eza7_1563;

						BgL_siza7eza7_1563 = CAR(BgL_argsz00_42);
						BgL_argsz00_42 = CDR(BgL_argsz00_42);
						{	/* Llib/hash.scm 167 */
							bool_t BgL_test2724z00_5692;

							if (INTEGERP(BgL_siza7eza7_1563))
								{	/* Llib/hash.scm 167 */
									BgL_test2724z00_5692 =
										((long) CINT(BgL_siza7eza7_1563) >= 1L);
								}
							else
								{	/* Llib/hash.scm 167 */
									BgL_test2724z00_5692 = ((bool_t) 0);
								}
							if (BgL_test2724z00_5692)
								{	/* Llib/hash.scm 167 */
									BgL_siza7eza7_1531 = BgL_siza7eza7_1563;
								}
							else
								{	/* Llib/hash.scm 167 */
									if ((BgL_siza7eza7_1563 == BUNSPEC))
										{	/* Llib/hash.scm 169 */
											BgL_siza7eza7_1531 = BINT(128L);
										}
									else
										{	/* Llib/hash.scm 169 */
											BgL_siza7eza7_1531 =
												BGl_errorz00zz__errorz00(BGl_string2522z00zz__hashz00,
												BGl_string2523z00zz__hashz00, BgL_siza7eza7_1563);
										}
								}
						}
					}
				else
					{	/* Llib/hash.scm 163 */
						BgL_siza7eza7_1531 = BINT(128L);
					}
				{	/* Llib/hash.scm 163 */
					obj_t BgL_mblenz00_1532;

					if (PAIRP(BgL_argsz00_42))
						{	/* Llib/hash.scm 177 */
							obj_t BgL_mblenz00_1558;

							BgL_mblenz00_1558 = CAR(BgL_argsz00_42);
							BgL_argsz00_42 = CDR(BgL_argsz00_42);
							{	/* Llib/hash.scm 180 */
								bool_t BgL_test2728z00_5706;

								if (INTEGERP(BgL_mblenz00_1558))
									{	/* Llib/hash.scm 180 */
										BgL_test2728z00_5706 =
											((long) CINT(BgL_mblenz00_1558) >= 1L);
									}
								else
									{	/* Llib/hash.scm 180 */
										BgL_test2728z00_5706 = ((bool_t) 0);
									}
								if (BgL_test2728z00_5706)
									{	/* Llib/hash.scm 180 */
										BgL_mblenz00_1532 = BgL_mblenz00_1558;
									}
								else
									{	/* Llib/hash.scm 180 */
										if ((BgL_mblenz00_1558 == BUNSPEC))
											{	/* Llib/hash.scm 182 */
												BgL_mblenz00_1532 = BINT(10L);
											}
										else
											{	/* Llib/hash.scm 182 */
												BgL_mblenz00_1532 =
													BGl_errorz00zz__errorz00(BGl_string2522z00zz__hashz00,
													BGl_string2524z00zz__hashz00, BgL_mblenz00_1558);
											}
									}
							}
						}
					else
						{	/* Llib/hash.scm 176 */
							BgL_mblenz00_1532 = BINT(10L);
						}
					{	/* Llib/hash.scm 176 */
						obj_t BgL_eqtestz00_1533;

						if (PAIRP(BgL_argsz00_42))
							{	/* Llib/hash.scm 190 */
								obj_t BgL_eqtestz00_1553;

								BgL_eqtestz00_1553 = CAR(BgL_argsz00_42);
								BgL_argsz00_42 = CDR(BgL_argsz00_42);
								{	/* Llib/hash.scm 193 */
									bool_t BgL_test2732z00_5720;

									if (PROCEDUREP(BgL_eqtestz00_1553))
										{	/* Llib/hash.scm 193 */
											BgL_test2732z00_5720 =
												PROCEDURE_CORRECT_ARITYP(BgL_eqtestz00_1553,
												(int) (2L));
										}
									else
										{	/* Llib/hash.scm 193 */
											BgL_test2732z00_5720 = ((bool_t) 0);
										}
									if (BgL_test2732z00_5720)
										{	/* Llib/hash.scm 193 */
											BgL_eqtestz00_1533 = BgL_eqtestz00_1553;
										}
									else
										{	/* Llib/hash.scm 193 */
											if ((BgL_eqtestz00_1553 == BUNSPEC))
												{	/* Llib/hash.scm 195 */
													BgL_eqtestz00_1533 = BFALSE;
												}
											else
												{	/* Llib/hash.scm 195 */
													BgL_eqtestz00_1533 =
														BGl_errorz00zz__errorz00
														(BGl_string2522z00zz__hashz00,
														BGl_string2525z00zz__hashz00, BgL_eqtestz00_1553);
												}
										}
								}
							}
						else
							{	/* Llib/hash.scm 189 */
								BgL_eqtestz00_1533 = BFALSE;
							}
						{	/* Llib/hash.scm 189 */
							obj_t BgL_hashnz00_1534;

							if (PAIRP(BgL_argsz00_42))
								{	/* Llib/hash.scm 203 */
									obj_t BgL_hnz00_1548;

									BgL_hnz00_1548 = CAR(BgL_argsz00_42);
									BgL_argsz00_42 = CDR(BgL_argsz00_42);
									{	/* Llib/hash.scm 206 */
										bool_t BgL_test2736z00_5732;

										if (PROCEDUREP(BgL_hnz00_1548))
											{	/* Llib/hash.scm 206 */
												BgL_test2736z00_5732 =
													PROCEDURE_CORRECT_ARITYP(BgL_hnz00_1548, (int) (1L));
											}
										else
											{	/* Llib/hash.scm 206 */
												BgL_test2736z00_5732 = ((bool_t) 0);
											}
										if (BgL_test2736z00_5732)
											{	/* Llib/hash.scm 206 */
												BgL_hashnz00_1534 = BgL_hnz00_1548;
											}
										else
											{	/* Llib/hash.scm 206 */
												if ((BgL_hnz00_1548 == BUNSPEC))
													{	/* Llib/hash.scm 208 */
														BgL_hashnz00_1534 = BFALSE;
													}
												else
													{	/* Llib/hash.scm 208 */
														BgL_hashnz00_1534 =
															BGl_errorz00zz__errorz00
															(BGl_string2522z00zz__hashz00,
															BGl_string2526z00zz__hashz00, BgL_hnz00_1548);
													}
											}
									}
								}
							else
								{	/* Llib/hash.scm 202 */
									BgL_hashnz00_1534 = BFALSE;
								}
							{	/* Llib/hash.scm 202 */
								long BgL_wkkz00_1535;

								if (PAIRP(BgL_argsz00_42))
									{	/* Llib/hash.scm 216 */
										obj_t BgL_wkkz00_1545;

										BgL_wkkz00_1545 = CAR(BgL_argsz00_42);
										BgL_argsz00_42 = CDR(BgL_argsz00_42);
										{	/* Llib/hash.scm 218 */
											bool_t BgL_test2740z00_5744;

											if ((BgL_wkkz00_1545 == BUNSPEC))
												{	/* Llib/hash.scm 218 */
													BgL_test2740z00_5744 = ((bool_t) 0);
												}
											else
												{	/* Llib/hash.scm 218 */
													BgL_test2740z00_5744 = CBOOL(BgL_wkkz00_1545);
												}
											if (BgL_test2740z00_5744)
												{	/* Llib/hash.scm 218 */
													BgL_wkkz00_1535 = 1L;
												}
											else
												{	/* Llib/hash.scm 218 */
													BgL_wkkz00_1535 = 0L;
												}
										}
									}
								else
									{	/* Llib/hash.scm 215 */
										BgL_wkkz00_1535 = 0L;
									}
								{	/* Llib/hash.scm 215 */
									long BgL_wkdz00_1536;

									if (PAIRP(BgL_argsz00_42))
										{	/* Llib/hash.scm 223 */
											obj_t BgL_wkdz00_1542;

											BgL_wkdz00_1542 = CAR(BgL_argsz00_42);
											{	/* Llib/hash.scm 224 */
												bool_t BgL_test2743z00_5751;

												if ((BgL_wkdz00_1542 == BUNSPEC))
													{	/* Llib/hash.scm 224 */
														BgL_test2743z00_5751 = ((bool_t) 0);
													}
												else
													{	/* Llib/hash.scm 224 */
														BgL_test2743z00_5751 = CBOOL(BgL_wkdz00_1542);
													}
												if (BgL_test2743z00_5751)
													{	/* Llib/hash.scm 224 */
														BgL_wkdz00_1536 = 2L;
													}
												else
													{	/* Llib/hash.scm 224 */
														BgL_wkdz00_1536 = 0L;
													}
											}
										}
									else
										{	/* Llib/hash.scm 222 */
											BgL_wkdz00_1536 = 0L;
										}
									{	/* Llib/hash.scm 222 */
										long BgL_wkz00_1537;

										BgL_wkz00_1537 = (BgL_wkkz00_1535 | BgL_wkdz00_1536);
										{	/* Llib/hash.scm 228 */

											if ((BINT(BgL_wkz00_1537) == BINT(8L)))
												{	/* Llib/hash.scm 230 */
													obj_t BgL_arg1322z00_1538;

													BgL_arg1322z00_1538 =
														make_vector(
														(3L * (long) CINT(BgL_siza7eza7_1531)), BFALSE);
													{	/* Llib/hash.scm 230 */
														obj_t BgL_newz00_3126;

														BgL_newz00_3126 =
															create_struct(BGl_symbol2527z00zz__hashz00,
															(int) (8L));
														{	/* Llib/hash.scm 230 */
															obj_t BgL_auxz00_5767;
															int BgL_tmpz00_5765;

															BgL_auxz00_5767 = BINT(0L);
															BgL_tmpz00_5765 = (int) (7L);
															STRUCT_SET(BgL_newz00_3126, BgL_tmpz00_5765,
																BgL_auxz00_5767);
														}
														{	/* Llib/hash.scm 230 */
															obj_t BgL_auxz00_5772;
															int BgL_tmpz00_5770;

															BgL_auxz00_5772 = BINT(0L);
															BgL_tmpz00_5770 = (int) (6L);
															STRUCT_SET(BgL_newz00_3126, BgL_tmpz00_5770,
																BgL_auxz00_5772);
														}
														{	/* Llib/hash.scm 230 */
															obj_t BgL_auxz00_5777;
															int BgL_tmpz00_5775;

															BgL_auxz00_5777 = BINT(BgL_wkz00_1537);
															BgL_tmpz00_5775 = (int) (5L);
															STRUCT_SET(BgL_newz00_3126, BgL_tmpz00_5775,
																BgL_auxz00_5777);
														}
														{	/* Llib/hash.scm 230 */
															int BgL_tmpz00_5780;

															BgL_tmpz00_5780 = (int) (4L);
															STRUCT_SET(BgL_newz00_3126, BgL_tmpz00_5780,
																BGl_listzd2envzd2zz__r4_pairs_and_lists_6_3z00);
														}
														{	/* Llib/hash.scm 230 */
															int BgL_tmpz00_5783;

															BgL_tmpz00_5783 = (int) (3L);
															STRUCT_SET(BgL_newz00_3126, BgL_tmpz00_5783,
																BGl_eqzf3zd2envz21zz__r4_equivalence_6_2z00);
														}
														{	/* Llib/hash.scm 230 */
															int BgL_tmpz00_5786;

															BgL_tmpz00_5786 = (int) (2L);
															STRUCT_SET(BgL_newz00_3126, BgL_tmpz00_5786,
																BgL_arg1322z00_1538);
														}
														{	/* Llib/hash.scm 230 */
															int BgL_tmpz00_5789;

															BgL_tmpz00_5789 = (int) (1L);
															STRUCT_SET(BgL_newz00_3126, BgL_tmpz00_5789,
																BgL_siza7eza7_1531);
														}
														{	/* Llib/hash.scm 230 */
															obj_t BgL_auxz00_5794;
															int BgL_tmpz00_5792;

															BgL_auxz00_5794 = BINT(0L);
															BgL_tmpz00_5792 = (int) (0L);
															STRUCT_SET(BgL_newz00_3126, BgL_tmpz00_5792,
																BgL_auxz00_5794);
														}
														return BgL_newz00_3126;
													}
												}
											else
												{	/* Llib/hash.scm 231 */
													obj_t BgL_arg1325z00_1540;

													BgL_arg1325z00_1540 =
														make_vector((long) CINT(BgL_siza7eza7_1531), BNIL);
													{	/* Llib/hash.scm 231 */
														obj_t BgL_newz00_3135;

														BgL_newz00_3135 =
															create_struct(BGl_symbol2527z00zz__hashz00,
															(int) (8L));
														{	/* Llib/hash.scm 231 */
															int BgL_tmpz00_5801;

															BgL_tmpz00_5801 = (int) (7L);
															STRUCT_SET(BgL_newz00_3135, BgL_tmpz00_5801,
																BGL_REAL_CNST(BGl_real2529z00zz__hashz00));
														}
														{	/* Llib/hash.scm 231 */
															obj_t BgL_auxz00_5806;
															int BgL_tmpz00_5804;

															BgL_auxz00_5806 = BINT(-1L);
															BgL_tmpz00_5804 = (int) (6L);
															STRUCT_SET(BgL_newz00_3135, BgL_tmpz00_5804,
																BgL_auxz00_5806);
														}
														{	/* Llib/hash.scm 231 */
															obj_t BgL_auxz00_5811;
															int BgL_tmpz00_5809;

															BgL_auxz00_5811 = BINT(BgL_wkz00_1537);
															BgL_tmpz00_5809 = (int) (5L);
															STRUCT_SET(BgL_newz00_3135, BgL_tmpz00_5809,
																BgL_auxz00_5811);
														}
														{	/* Llib/hash.scm 231 */
															int BgL_tmpz00_5814;

															BgL_tmpz00_5814 = (int) (4L);
															STRUCT_SET(BgL_newz00_3135, BgL_tmpz00_5814,
																BgL_hashnz00_1534);
														}
														{	/* Llib/hash.scm 231 */
															int BgL_tmpz00_5817;

															BgL_tmpz00_5817 = (int) (3L);
															STRUCT_SET(BgL_newz00_3135, BgL_tmpz00_5817,
																BgL_eqtestz00_1533);
														}
														{	/* Llib/hash.scm 231 */
															int BgL_tmpz00_5820;

															BgL_tmpz00_5820 = (int) (2L);
															STRUCT_SET(BgL_newz00_3135, BgL_tmpz00_5820,
																BgL_arg1325z00_1540);
														}
														{	/* Llib/hash.scm 231 */
															int BgL_tmpz00_5823;

															BgL_tmpz00_5823 = (int) (1L);
															STRUCT_SET(BgL_newz00_3135, BgL_tmpz00_5823,
																BgL_mblenz00_1532);
														}
														{	/* Llib/hash.scm 231 */
															obj_t BgL_auxz00_5828;
															int BgL_tmpz00_5826;

															BgL_auxz00_5828 = BINT(0L);
															BgL_tmpz00_5826 = (int) (0L);
															STRUCT_SET(BgL_newz00_3135, BgL_tmpz00_5826,
																BgL_auxz00_5828);
														}
														return BgL_newz00_3135;
													}
												}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &make-hashtable */
	obj_t BGl_z62makezd2hashtablezb0zz__hashz00(obj_t BgL_envz00_5375,
		obj_t BgL_argsz00_5376)
	{
		{	/* Llib/hash.scm 162 */
			return BGl_makezd2hashtablezd2zz__hashz00(BgL_argsz00_5376);
		}

	}



/* _create-hashtable */
	obj_t BGl__createzd2hashtablezd2zz__hashz00(obj_t BgL_env1160z00_52,
		obj_t BgL_opt1159z00_51)
	{
		{	/* Llib/hash.scm 236 */
			{	/* Llib/hash.scm 236 */

				{	/* Llib/hash.scm 236 */
					obj_t BgL_bucketzd2expansionzd2_1571;

					BgL_bucketzd2expansionzd2_1571 =
						BGL_REAL_CNST(BGl_real2529z00zz__hashz00);
					{	/* Llib/hash.scm 236 */
						obj_t BgL_eqtestz00_1572;

						BgL_eqtestz00_1572 = BFALSE;
						{	/* Llib/hash.scm 236 */
							obj_t BgL_hashz00_1573;

							BgL_hashz00_1573 = BFALSE;
							{	/* Llib/hash.scm 236 */
								obj_t BgL_maxzd2bucketzd2lengthz00_1574;

								BgL_maxzd2bucketzd2lengthz00_1574 = BINT(10L);
								{	/* Llib/hash.scm 236 */
									obj_t BgL_maxzd2lengthzd2_1575;

									BgL_maxzd2lengthzd2_1575 = BINT(16384L);
									{	/* Llib/hash.scm 236 */
										obj_t BgL_persistentz00_1576;

										BgL_persistentz00_1576 = BFALSE;
										{	/* Llib/hash.scm 236 */
											obj_t BgL_siza7eza7_1577;

											BgL_siza7eza7_1577 = BINT(128L);
											{	/* Llib/hash.scm 241 */
												obj_t BgL_weakz00_1578;

												BgL_weakz00_1578 = BGl_symbol2530z00zz__hashz00;
												{	/* Llib/hash.scm 236 */

													{
														long BgL_iz00_1579;

														BgL_iz00_1579 = 0L;
													BgL_check1163z00_1580:
														if (
															(BgL_iz00_1579 ==
																VECTOR_LENGTH(BgL_opt1159z00_51)))
															{	/* Llib/hash.scm 236 */
																BNIL;
															}
														else
															{	/* Llib/hash.scm 236 */
																bool_t BgL_test2747z00_5838;

																{	/* Llib/hash.scm 236 */
																	obj_t BgL_arg1348z00_1586;

																	BgL_arg1348z00_1586 =
																		VECTOR_REF(BgL_opt1159z00_51,
																		BgL_iz00_1579);
																	BgL_test2747z00_5838 =
																		CBOOL
																		(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																		(BgL_arg1348z00_1586,
																			BGl_list2532z00zz__hashz00));
																}
																if (BgL_test2747z00_5838)
																	{
																		long BgL_iz00_5842;

																		BgL_iz00_5842 = (BgL_iz00_1579 + 2L);
																		BgL_iz00_1579 = BgL_iz00_5842;
																		goto BgL_check1163z00_1580;
																	}
																else
																	{	/* Llib/hash.scm 236 */
																		obj_t BgL_arg1347z00_1585;

																		BgL_arg1347z00_1585 =
																			VECTOR_REF(BgL_opt1159z00_51,
																			BgL_iz00_1579);
																		BGl_errorz00zz__errorz00
																			(BGl_symbol2549z00zz__hashz00,
																			BGl_string2551z00zz__hashz00,
																			BgL_arg1347z00_1585);
																	}
															}
													}
													{	/* Llib/hash.scm 236 */
														obj_t BgL_index1165z00_1587;

														{
															long BgL_iz00_3160;

															BgL_iz00_3160 = 0L;
														BgL_search1162z00_3159:
															if (
																(BgL_iz00_3160 ==
																	VECTOR_LENGTH(BgL_opt1159z00_51)))
																{	/* Llib/hash.scm 236 */
																	BgL_index1165z00_1587 = BINT(-1L);
																}
															else
																{	/* Llib/hash.scm 236 */
																	if (
																		(BgL_iz00_3160 ==
																			(VECTOR_LENGTH(BgL_opt1159z00_51) - 1L)))
																		{	/* Llib/hash.scm 236 */
																			BgL_index1165z00_1587 =
																				BGl_errorz00zz__errorz00
																				(BGl_symbol2549z00zz__hashz00,
																				BGl_string2552z00zz__hashz00,
																				BINT(VECTOR_LENGTH(BgL_opt1159z00_51)));
																		}
																	else
																		{	/* Llib/hash.scm 236 */
																			obj_t BgL_vz00_3170;

																			BgL_vz00_3170 =
																				VECTOR_REF(BgL_opt1159z00_51,
																				BgL_iz00_3160);
																			if ((BgL_vz00_3170 ==
																					BGl_keyword2533z00zz__hashz00))
																				{	/* Llib/hash.scm 236 */
																					BgL_index1165z00_1587 =
																						BINT((BgL_iz00_3160 + 1L));
																				}
																			else
																				{
																					long BgL_iz00_5862;

																					BgL_iz00_5862 = (BgL_iz00_3160 + 2L);
																					BgL_iz00_3160 = BgL_iz00_5862;
																					goto BgL_search1162z00_3159;
																				}
																		}
																}
														}
														{	/* Llib/hash.scm 236 */
															bool_t BgL_test2751z00_5864;

															{	/* Llib/hash.scm 236 */
																long BgL_n1z00_3174;

																{	/* Llib/hash.scm 236 */
																	obj_t BgL_tmpz00_5865;

																	if (INTEGERP(BgL_index1165z00_1587))
																		{	/* Llib/hash.scm 236 */
																			BgL_tmpz00_5865 = BgL_index1165z00_1587;
																		}
																	else
																		{
																			obj_t BgL_auxz00_5868;

																			BgL_auxz00_5868 =
																				BGl_typezd2errorzd2zz__errorz00
																				(BGl_string2553z00zz__hashz00,
																				BINT(8806L),
																				BGl_string2554z00zz__hashz00,
																				BGl_string2555z00zz__hashz00,
																				BgL_index1165z00_1587);
																			FAILURE(BgL_auxz00_5868, BFALSE, BFALSE);
																		}
																	BgL_n1z00_3174 = (long) CINT(BgL_tmpz00_5865);
																}
																BgL_test2751z00_5864 = (BgL_n1z00_3174 >= 0L);
															}
															if (BgL_test2751z00_5864)
																{
																	long BgL_auxz00_5874;

																	{	/* Llib/hash.scm 236 */
																		obj_t BgL_tmpz00_5875;

																		if (INTEGERP(BgL_index1165z00_1587))
																			{	/* Llib/hash.scm 236 */
																				BgL_tmpz00_5875 = BgL_index1165z00_1587;
																			}
																		else
																			{
																				obj_t BgL_auxz00_5878;

																				BgL_auxz00_5878 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string2553z00zz__hashz00,
																					BINT(8806L),
																					BGl_string2554z00zz__hashz00,
																					BGl_string2555z00zz__hashz00,
																					BgL_index1165z00_1587);
																				FAILURE(BgL_auxz00_5878, BFALSE,
																					BFALSE);
																			}
																		BgL_auxz00_5874 =
																			(long) CINT(BgL_tmpz00_5875);
																	}
																	BgL_bucketzd2expansionzd2_1571 =
																		VECTOR_REF(BgL_opt1159z00_51,
																		BgL_auxz00_5874);
																}
															else
																{	/* Llib/hash.scm 236 */
																	BFALSE;
																}
														}
													}
													{	/* Llib/hash.scm 236 */
														obj_t BgL_index1166z00_1589;

														{
															long BgL_iz00_3176;

															BgL_iz00_3176 = 0L;
														BgL_search1162z00_3175:
															if (
																(BgL_iz00_3176 ==
																	VECTOR_LENGTH(BgL_opt1159z00_51)))
																{	/* Llib/hash.scm 236 */
																	BgL_index1166z00_1589 = BINT(-1L);
																}
															else
																{	/* Llib/hash.scm 236 */
																	if (
																		(BgL_iz00_3176 ==
																			(VECTOR_LENGTH(BgL_opt1159z00_51) - 1L)))
																		{	/* Llib/hash.scm 236 */
																			BgL_index1166z00_1589 =
																				BGl_errorz00zz__errorz00
																				(BGl_symbol2549z00zz__hashz00,
																				BGl_string2552z00zz__hashz00,
																				BINT(VECTOR_LENGTH(BgL_opt1159z00_51)));
																		}
																	else
																		{	/* Llib/hash.scm 236 */
																			obj_t BgL_vz00_3186;

																			BgL_vz00_3186 =
																				VECTOR_REF(BgL_opt1159z00_51,
																				BgL_iz00_3176);
																			if ((BgL_vz00_3186 ==
																					BGl_keyword2535z00zz__hashz00))
																				{	/* Llib/hash.scm 236 */
																					BgL_index1166z00_1589 =
																						BINT((BgL_iz00_3176 + 1L));
																				}
																			else
																				{
																					long BgL_iz00_5900;

																					BgL_iz00_5900 = (BgL_iz00_3176 + 2L);
																					BgL_iz00_3176 = BgL_iz00_5900;
																					goto BgL_search1162z00_3175;
																				}
																		}
																}
														}
														{	/* Llib/hash.scm 236 */
															bool_t BgL_test2757z00_5902;

															{	/* Llib/hash.scm 236 */
																long BgL_n1z00_3190;

																{	/* Llib/hash.scm 236 */
																	obj_t BgL_tmpz00_5903;

																	if (INTEGERP(BgL_index1166z00_1589))
																		{	/* Llib/hash.scm 236 */
																			BgL_tmpz00_5903 = BgL_index1166z00_1589;
																		}
																	else
																		{
																			obj_t BgL_auxz00_5906;

																			BgL_auxz00_5906 =
																				BGl_typezd2errorzd2zz__errorz00
																				(BGl_string2553z00zz__hashz00,
																				BINT(8806L),
																				BGl_string2554z00zz__hashz00,
																				BGl_string2555z00zz__hashz00,
																				BgL_index1166z00_1589);
																			FAILURE(BgL_auxz00_5906, BFALSE, BFALSE);
																		}
																	BgL_n1z00_3190 = (long) CINT(BgL_tmpz00_5903);
																}
																BgL_test2757z00_5902 = (BgL_n1z00_3190 >= 0L);
															}
															if (BgL_test2757z00_5902)
																{
																	long BgL_auxz00_5912;

																	{	/* Llib/hash.scm 236 */
																		obj_t BgL_tmpz00_5913;

																		if (INTEGERP(BgL_index1166z00_1589))
																			{	/* Llib/hash.scm 236 */
																				BgL_tmpz00_5913 = BgL_index1166z00_1589;
																			}
																		else
																			{
																				obj_t BgL_auxz00_5916;

																				BgL_auxz00_5916 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string2553z00zz__hashz00,
																					BINT(8806L),
																					BGl_string2554z00zz__hashz00,
																					BGl_string2555z00zz__hashz00,
																					BgL_index1166z00_1589);
																				FAILURE(BgL_auxz00_5916, BFALSE,
																					BFALSE);
																			}
																		BgL_auxz00_5912 =
																			(long) CINT(BgL_tmpz00_5913);
																	}
																	BgL_eqtestz00_1572 =
																		VECTOR_REF(BgL_opt1159z00_51,
																		BgL_auxz00_5912);
																}
															else
																{	/* Llib/hash.scm 236 */
																	BFALSE;
																}
														}
													}
													{	/* Llib/hash.scm 236 */
														obj_t BgL_index1167z00_1591;

														{
															long BgL_iz00_3192;

															BgL_iz00_3192 = 0L;
														BgL_search1162z00_3191:
															if (
																(BgL_iz00_3192 ==
																	VECTOR_LENGTH(BgL_opt1159z00_51)))
																{	/* Llib/hash.scm 236 */
																	BgL_index1167z00_1591 = BINT(-1L);
																}
															else
																{	/* Llib/hash.scm 236 */
																	if (
																		(BgL_iz00_3192 ==
																			(VECTOR_LENGTH(BgL_opt1159z00_51) - 1L)))
																		{	/* Llib/hash.scm 236 */
																			BgL_index1167z00_1591 =
																				BGl_errorz00zz__errorz00
																				(BGl_symbol2549z00zz__hashz00,
																				BGl_string2552z00zz__hashz00,
																				BINT(VECTOR_LENGTH(BgL_opt1159z00_51)));
																		}
																	else
																		{	/* Llib/hash.scm 236 */
																			obj_t BgL_vz00_3202;

																			BgL_vz00_3202 =
																				VECTOR_REF(BgL_opt1159z00_51,
																				BgL_iz00_3192);
																			if ((BgL_vz00_3202 ==
																					BGl_keyword2537z00zz__hashz00))
																				{	/* Llib/hash.scm 236 */
																					BgL_index1167z00_1591 =
																						BINT((BgL_iz00_3192 + 1L));
																				}
																			else
																				{
																					long BgL_iz00_5938;

																					BgL_iz00_5938 = (BgL_iz00_3192 + 2L);
																					BgL_iz00_3192 = BgL_iz00_5938;
																					goto BgL_search1162z00_3191;
																				}
																		}
																}
														}
														{	/* Llib/hash.scm 236 */
															bool_t BgL_test2763z00_5940;

															{	/* Llib/hash.scm 236 */
																long BgL_n1z00_3206;

																{	/* Llib/hash.scm 236 */
																	obj_t BgL_tmpz00_5941;

																	if (INTEGERP(BgL_index1167z00_1591))
																		{	/* Llib/hash.scm 236 */
																			BgL_tmpz00_5941 = BgL_index1167z00_1591;
																		}
																	else
																		{
																			obj_t BgL_auxz00_5944;

																			BgL_auxz00_5944 =
																				BGl_typezd2errorzd2zz__errorz00
																				(BGl_string2553z00zz__hashz00,
																				BINT(8806L),
																				BGl_string2554z00zz__hashz00,
																				BGl_string2555z00zz__hashz00,
																				BgL_index1167z00_1591);
																			FAILURE(BgL_auxz00_5944, BFALSE, BFALSE);
																		}
																	BgL_n1z00_3206 = (long) CINT(BgL_tmpz00_5941);
																}
																BgL_test2763z00_5940 = (BgL_n1z00_3206 >= 0L);
															}
															if (BgL_test2763z00_5940)
																{
																	long BgL_auxz00_5950;

																	{	/* Llib/hash.scm 236 */
																		obj_t BgL_tmpz00_5951;

																		if (INTEGERP(BgL_index1167z00_1591))
																			{	/* Llib/hash.scm 236 */
																				BgL_tmpz00_5951 = BgL_index1167z00_1591;
																			}
																		else
																			{
																				obj_t BgL_auxz00_5954;

																				BgL_auxz00_5954 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string2553z00zz__hashz00,
																					BINT(8806L),
																					BGl_string2554z00zz__hashz00,
																					BGl_string2555z00zz__hashz00,
																					BgL_index1167z00_1591);
																				FAILURE(BgL_auxz00_5954, BFALSE,
																					BFALSE);
																			}
																		BgL_auxz00_5950 =
																			(long) CINT(BgL_tmpz00_5951);
																	}
																	BgL_hashz00_1573 =
																		VECTOR_REF(BgL_opt1159z00_51,
																		BgL_auxz00_5950);
																}
															else
																{	/* Llib/hash.scm 236 */
																	BFALSE;
																}
														}
													}
													{	/* Llib/hash.scm 236 */
														obj_t BgL_index1168z00_1593;

														{
															long BgL_iz00_3208;

															BgL_iz00_3208 = 0L;
														BgL_search1162z00_3207:
															if (
																(BgL_iz00_3208 ==
																	VECTOR_LENGTH(BgL_opt1159z00_51)))
																{	/* Llib/hash.scm 236 */
																	BgL_index1168z00_1593 = BINT(-1L);
																}
															else
																{	/* Llib/hash.scm 236 */
																	if (
																		(BgL_iz00_3208 ==
																			(VECTOR_LENGTH(BgL_opt1159z00_51) - 1L)))
																		{	/* Llib/hash.scm 236 */
																			BgL_index1168z00_1593 =
																				BGl_errorz00zz__errorz00
																				(BGl_symbol2549z00zz__hashz00,
																				BGl_string2552z00zz__hashz00,
																				BINT(VECTOR_LENGTH(BgL_opt1159z00_51)));
																		}
																	else
																		{	/* Llib/hash.scm 236 */
																			obj_t BgL_vz00_3218;

																			BgL_vz00_3218 =
																				VECTOR_REF(BgL_opt1159z00_51,
																				BgL_iz00_3208);
																			if ((BgL_vz00_3218 ==
																					BGl_keyword2539z00zz__hashz00))
																				{	/* Llib/hash.scm 236 */
																					BgL_index1168z00_1593 =
																						BINT((BgL_iz00_3208 + 1L));
																				}
																			else
																				{
																					long BgL_iz00_5976;

																					BgL_iz00_5976 = (BgL_iz00_3208 + 2L);
																					BgL_iz00_3208 = BgL_iz00_5976;
																					goto BgL_search1162z00_3207;
																				}
																		}
																}
														}
														{	/* Llib/hash.scm 236 */
															bool_t BgL_test2769z00_5978;

															{	/* Llib/hash.scm 236 */
																long BgL_n1z00_3222;

																{	/* Llib/hash.scm 236 */
																	obj_t BgL_tmpz00_5979;

																	if (INTEGERP(BgL_index1168z00_1593))
																		{	/* Llib/hash.scm 236 */
																			BgL_tmpz00_5979 = BgL_index1168z00_1593;
																		}
																	else
																		{
																			obj_t BgL_auxz00_5982;

																			BgL_auxz00_5982 =
																				BGl_typezd2errorzd2zz__errorz00
																				(BGl_string2553z00zz__hashz00,
																				BINT(8806L),
																				BGl_string2554z00zz__hashz00,
																				BGl_string2555z00zz__hashz00,
																				BgL_index1168z00_1593);
																			FAILURE(BgL_auxz00_5982, BFALSE, BFALSE);
																		}
																	BgL_n1z00_3222 = (long) CINT(BgL_tmpz00_5979);
																}
																BgL_test2769z00_5978 = (BgL_n1z00_3222 >= 0L);
															}
															if (BgL_test2769z00_5978)
																{
																	long BgL_auxz00_5988;

																	{	/* Llib/hash.scm 236 */
																		obj_t BgL_tmpz00_5989;

																		if (INTEGERP(BgL_index1168z00_1593))
																			{	/* Llib/hash.scm 236 */
																				BgL_tmpz00_5989 = BgL_index1168z00_1593;
																			}
																		else
																			{
																				obj_t BgL_auxz00_5992;

																				BgL_auxz00_5992 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string2553z00zz__hashz00,
																					BINT(8806L),
																					BGl_string2554z00zz__hashz00,
																					BGl_string2555z00zz__hashz00,
																					BgL_index1168z00_1593);
																				FAILURE(BgL_auxz00_5992, BFALSE,
																					BFALSE);
																			}
																		BgL_auxz00_5988 =
																			(long) CINT(BgL_tmpz00_5989);
																	}
																	BgL_maxzd2bucketzd2lengthz00_1574 =
																		VECTOR_REF(BgL_opt1159z00_51,
																		BgL_auxz00_5988);
																}
															else
																{	/* Llib/hash.scm 236 */
																	BFALSE;
																}
														}
													}
													{	/* Llib/hash.scm 236 */
														obj_t BgL_index1169z00_1595;

														{
															long BgL_iz00_3224;

															BgL_iz00_3224 = 0L;
														BgL_search1162z00_3223:
															if (
																(BgL_iz00_3224 ==
																	VECTOR_LENGTH(BgL_opt1159z00_51)))
																{	/* Llib/hash.scm 236 */
																	BgL_index1169z00_1595 = BINT(-1L);
																}
															else
																{	/* Llib/hash.scm 236 */
																	if (
																		(BgL_iz00_3224 ==
																			(VECTOR_LENGTH(BgL_opt1159z00_51) - 1L)))
																		{	/* Llib/hash.scm 236 */
																			BgL_index1169z00_1595 =
																				BGl_errorz00zz__errorz00
																				(BGl_symbol2549z00zz__hashz00,
																				BGl_string2552z00zz__hashz00,
																				BINT(VECTOR_LENGTH(BgL_opt1159z00_51)));
																		}
																	else
																		{	/* Llib/hash.scm 236 */
																			obj_t BgL_vz00_3234;

																			BgL_vz00_3234 =
																				VECTOR_REF(BgL_opt1159z00_51,
																				BgL_iz00_3224);
																			if ((BgL_vz00_3234 ==
																					BGl_keyword2541z00zz__hashz00))
																				{	/* Llib/hash.scm 236 */
																					BgL_index1169z00_1595 =
																						BINT((BgL_iz00_3224 + 1L));
																				}
																			else
																				{
																					long BgL_iz00_6014;

																					BgL_iz00_6014 = (BgL_iz00_3224 + 2L);
																					BgL_iz00_3224 = BgL_iz00_6014;
																					goto BgL_search1162z00_3223;
																				}
																		}
																}
														}
														{	/* Llib/hash.scm 236 */
															bool_t BgL_test2775z00_6016;

															{	/* Llib/hash.scm 236 */
																long BgL_n1z00_3238;

																{	/* Llib/hash.scm 236 */
																	obj_t BgL_tmpz00_6017;

																	if (INTEGERP(BgL_index1169z00_1595))
																		{	/* Llib/hash.scm 236 */
																			BgL_tmpz00_6017 = BgL_index1169z00_1595;
																		}
																	else
																		{
																			obj_t BgL_auxz00_6020;

																			BgL_auxz00_6020 =
																				BGl_typezd2errorzd2zz__errorz00
																				(BGl_string2553z00zz__hashz00,
																				BINT(8806L),
																				BGl_string2554z00zz__hashz00,
																				BGl_string2555z00zz__hashz00,
																				BgL_index1169z00_1595);
																			FAILURE(BgL_auxz00_6020, BFALSE, BFALSE);
																		}
																	BgL_n1z00_3238 = (long) CINT(BgL_tmpz00_6017);
																}
																BgL_test2775z00_6016 = (BgL_n1z00_3238 >= 0L);
															}
															if (BgL_test2775z00_6016)
																{
																	long BgL_auxz00_6026;

																	{	/* Llib/hash.scm 236 */
																		obj_t BgL_tmpz00_6027;

																		if (INTEGERP(BgL_index1169z00_1595))
																			{	/* Llib/hash.scm 236 */
																				BgL_tmpz00_6027 = BgL_index1169z00_1595;
																			}
																		else
																			{
																				obj_t BgL_auxz00_6030;

																				BgL_auxz00_6030 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string2553z00zz__hashz00,
																					BINT(8806L),
																					BGl_string2554z00zz__hashz00,
																					BGl_string2555z00zz__hashz00,
																					BgL_index1169z00_1595);
																				FAILURE(BgL_auxz00_6030, BFALSE,
																					BFALSE);
																			}
																		BgL_auxz00_6026 =
																			(long) CINT(BgL_tmpz00_6027);
																	}
																	BgL_maxzd2lengthzd2_1575 =
																		VECTOR_REF(BgL_opt1159z00_51,
																		BgL_auxz00_6026);
																}
															else
																{	/* Llib/hash.scm 236 */
																	BFALSE;
																}
														}
													}
													{	/* Llib/hash.scm 236 */
														obj_t BgL_index1170z00_1597;

														{
															long BgL_iz00_3240;

															BgL_iz00_3240 = 0L;
														BgL_search1162z00_3239:
															if (
																(BgL_iz00_3240 ==
																	VECTOR_LENGTH(BgL_opt1159z00_51)))
																{	/* Llib/hash.scm 236 */
																	BgL_index1170z00_1597 = BINT(-1L);
																}
															else
																{	/* Llib/hash.scm 236 */
																	if (
																		(BgL_iz00_3240 ==
																			(VECTOR_LENGTH(BgL_opt1159z00_51) - 1L)))
																		{	/* Llib/hash.scm 236 */
																			BgL_index1170z00_1597 =
																				BGl_errorz00zz__errorz00
																				(BGl_symbol2549z00zz__hashz00,
																				BGl_string2552z00zz__hashz00,
																				BINT(VECTOR_LENGTH(BgL_opt1159z00_51)));
																		}
																	else
																		{	/* Llib/hash.scm 236 */
																			obj_t BgL_vz00_3250;

																			BgL_vz00_3250 =
																				VECTOR_REF(BgL_opt1159z00_51,
																				BgL_iz00_3240);
																			if ((BgL_vz00_3250 ==
																					BGl_keyword2543z00zz__hashz00))
																				{	/* Llib/hash.scm 236 */
																					BgL_index1170z00_1597 =
																						BINT((BgL_iz00_3240 + 1L));
																				}
																			else
																				{
																					long BgL_iz00_6052;

																					BgL_iz00_6052 = (BgL_iz00_3240 + 2L);
																					BgL_iz00_3240 = BgL_iz00_6052;
																					goto BgL_search1162z00_3239;
																				}
																		}
																}
														}
														{	/* Llib/hash.scm 236 */
															bool_t BgL_test2781z00_6054;

															{	/* Llib/hash.scm 236 */
																long BgL_n1z00_3254;

																{	/* Llib/hash.scm 236 */
																	obj_t BgL_tmpz00_6055;

																	if (INTEGERP(BgL_index1170z00_1597))
																		{	/* Llib/hash.scm 236 */
																			BgL_tmpz00_6055 = BgL_index1170z00_1597;
																		}
																	else
																		{
																			obj_t BgL_auxz00_6058;

																			BgL_auxz00_6058 =
																				BGl_typezd2errorzd2zz__errorz00
																				(BGl_string2553z00zz__hashz00,
																				BINT(8806L),
																				BGl_string2554z00zz__hashz00,
																				BGl_string2555z00zz__hashz00,
																				BgL_index1170z00_1597);
																			FAILURE(BgL_auxz00_6058, BFALSE, BFALSE);
																		}
																	BgL_n1z00_3254 = (long) CINT(BgL_tmpz00_6055);
																}
																BgL_test2781z00_6054 = (BgL_n1z00_3254 >= 0L);
															}
															if (BgL_test2781z00_6054)
																{
																	long BgL_auxz00_6064;

																	{	/* Llib/hash.scm 236 */
																		obj_t BgL_tmpz00_6065;

																		if (INTEGERP(BgL_index1170z00_1597))
																			{	/* Llib/hash.scm 236 */
																				BgL_tmpz00_6065 = BgL_index1170z00_1597;
																			}
																		else
																			{
																				obj_t BgL_auxz00_6068;

																				BgL_auxz00_6068 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string2553z00zz__hashz00,
																					BINT(8806L),
																					BGl_string2554z00zz__hashz00,
																					BGl_string2555z00zz__hashz00,
																					BgL_index1170z00_1597);
																				FAILURE(BgL_auxz00_6068, BFALSE,
																					BFALSE);
																			}
																		BgL_auxz00_6064 =
																			(long) CINT(BgL_tmpz00_6065);
																	}
																	BgL_persistentz00_1576 =
																		VECTOR_REF(BgL_opt1159z00_51,
																		BgL_auxz00_6064);
																}
															else
																{	/* Llib/hash.scm 236 */
																	BFALSE;
																}
														}
													}
													{	/* Llib/hash.scm 236 */
														obj_t BgL_index1171z00_1599;

														{
															long BgL_iz00_3256;

															BgL_iz00_3256 = 0L;
														BgL_search1162z00_3255:
															if (
																(BgL_iz00_3256 ==
																	VECTOR_LENGTH(BgL_opt1159z00_51)))
																{	/* Llib/hash.scm 236 */
																	BgL_index1171z00_1599 = BINT(-1L);
																}
															else
																{	/* Llib/hash.scm 236 */
																	if (
																		(BgL_iz00_3256 ==
																			(VECTOR_LENGTH(BgL_opt1159z00_51) - 1L)))
																		{	/* Llib/hash.scm 236 */
																			BgL_index1171z00_1599 =
																				BGl_errorz00zz__errorz00
																				(BGl_symbol2549z00zz__hashz00,
																				BGl_string2552z00zz__hashz00,
																				BINT(VECTOR_LENGTH(BgL_opt1159z00_51)));
																		}
																	else
																		{	/* Llib/hash.scm 236 */
																			obj_t BgL_vz00_3266;

																			BgL_vz00_3266 =
																				VECTOR_REF(BgL_opt1159z00_51,
																				BgL_iz00_3256);
																			if ((BgL_vz00_3266 ==
																					BGl_keyword2545z00zz__hashz00))
																				{	/* Llib/hash.scm 236 */
																					BgL_index1171z00_1599 =
																						BINT((BgL_iz00_3256 + 1L));
																				}
																			else
																				{
																					long BgL_iz00_6090;

																					BgL_iz00_6090 = (BgL_iz00_3256 + 2L);
																					BgL_iz00_3256 = BgL_iz00_6090;
																					goto BgL_search1162z00_3255;
																				}
																		}
																}
														}
														{	/* Llib/hash.scm 236 */
															bool_t BgL_test2787z00_6092;

															{	/* Llib/hash.scm 236 */
																long BgL_n1z00_3270;

																{	/* Llib/hash.scm 236 */
																	obj_t BgL_tmpz00_6093;

																	if (INTEGERP(BgL_index1171z00_1599))
																		{	/* Llib/hash.scm 236 */
																			BgL_tmpz00_6093 = BgL_index1171z00_1599;
																		}
																	else
																		{
																			obj_t BgL_auxz00_6096;

																			BgL_auxz00_6096 =
																				BGl_typezd2errorzd2zz__errorz00
																				(BGl_string2553z00zz__hashz00,
																				BINT(8806L),
																				BGl_string2554z00zz__hashz00,
																				BGl_string2555z00zz__hashz00,
																				BgL_index1171z00_1599);
																			FAILURE(BgL_auxz00_6096, BFALSE, BFALSE);
																		}
																	BgL_n1z00_3270 = (long) CINT(BgL_tmpz00_6093);
																}
																BgL_test2787z00_6092 = (BgL_n1z00_3270 >= 0L);
															}
															if (BgL_test2787z00_6092)
																{
																	long BgL_auxz00_6102;

																	{	/* Llib/hash.scm 236 */
																		obj_t BgL_tmpz00_6103;

																		if (INTEGERP(BgL_index1171z00_1599))
																			{	/* Llib/hash.scm 236 */
																				BgL_tmpz00_6103 = BgL_index1171z00_1599;
																			}
																		else
																			{
																				obj_t BgL_auxz00_6106;

																				BgL_auxz00_6106 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string2553z00zz__hashz00,
																					BINT(8806L),
																					BGl_string2554z00zz__hashz00,
																					BGl_string2555z00zz__hashz00,
																					BgL_index1171z00_1599);
																				FAILURE(BgL_auxz00_6106, BFALSE,
																					BFALSE);
																			}
																		BgL_auxz00_6102 =
																			(long) CINT(BgL_tmpz00_6103);
																	}
																	BgL_siza7eza7_1577 =
																		VECTOR_REF(BgL_opt1159z00_51,
																		BgL_auxz00_6102);
																}
															else
																{	/* Llib/hash.scm 236 */
																	BFALSE;
																}
														}
													}
													{	/* Llib/hash.scm 236 */
														obj_t BgL_index1172z00_1601;

														{
															long BgL_iz00_3272;

															BgL_iz00_3272 = 0L;
														BgL_search1162z00_3271:
															if (
																(BgL_iz00_3272 ==
																	VECTOR_LENGTH(BgL_opt1159z00_51)))
																{	/* Llib/hash.scm 236 */
																	BgL_index1172z00_1601 = BINT(-1L);
																}
															else
																{	/* Llib/hash.scm 236 */
																	if (
																		(BgL_iz00_3272 ==
																			(VECTOR_LENGTH(BgL_opt1159z00_51) - 1L)))
																		{	/* Llib/hash.scm 236 */
																			BgL_index1172z00_1601 =
																				BGl_errorz00zz__errorz00
																				(BGl_symbol2549z00zz__hashz00,
																				BGl_string2552z00zz__hashz00,
																				BINT(VECTOR_LENGTH(BgL_opt1159z00_51)));
																		}
																	else
																		{	/* Llib/hash.scm 236 */
																			obj_t BgL_vz00_3282;

																			BgL_vz00_3282 =
																				VECTOR_REF(BgL_opt1159z00_51,
																				BgL_iz00_3272);
																			if ((BgL_vz00_3282 ==
																					BGl_keyword2547z00zz__hashz00))
																				{	/* Llib/hash.scm 236 */
																					BgL_index1172z00_1601 =
																						BINT((BgL_iz00_3272 + 1L));
																				}
																			else
																				{
																					long BgL_iz00_6128;

																					BgL_iz00_6128 = (BgL_iz00_3272 + 2L);
																					BgL_iz00_3272 = BgL_iz00_6128;
																					goto BgL_search1162z00_3271;
																				}
																		}
																}
														}
														{	/* Llib/hash.scm 236 */
															bool_t BgL_test2793z00_6130;

															{	/* Llib/hash.scm 236 */
																long BgL_n1z00_3286;

																{	/* Llib/hash.scm 236 */
																	obj_t BgL_tmpz00_6131;

																	if (INTEGERP(BgL_index1172z00_1601))
																		{	/* Llib/hash.scm 236 */
																			BgL_tmpz00_6131 = BgL_index1172z00_1601;
																		}
																	else
																		{
																			obj_t BgL_auxz00_6134;

																			BgL_auxz00_6134 =
																				BGl_typezd2errorzd2zz__errorz00
																				(BGl_string2553z00zz__hashz00,
																				BINT(8806L),
																				BGl_string2554z00zz__hashz00,
																				BGl_string2555z00zz__hashz00,
																				BgL_index1172z00_1601);
																			FAILURE(BgL_auxz00_6134, BFALSE, BFALSE);
																		}
																	BgL_n1z00_3286 = (long) CINT(BgL_tmpz00_6131);
																}
																BgL_test2793z00_6130 = (BgL_n1z00_3286 >= 0L);
															}
															if (BgL_test2793z00_6130)
																{
																	long BgL_auxz00_6140;

																	{	/* Llib/hash.scm 236 */
																		obj_t BgL_tmpz00_6141;

																		if (INTEGERP(BgL_index1172z00_1601))
																			{	/* Llib/hash.scm 236 */
																				BgL_tmpz00_6141 = BgL_index1172z00_1601;
																			}
																		else
																			{
																				obj_t BgL_auxz00_6144;

																				BgL_auxz00_6144 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string2553z00zz__hashz00,
																					BINT(8806L),
																					BGl_string2554z00zz__hashz00,
																					BGl_string2555z00zz__hashz00,
																					BgL_index1172z00_1601);
																				FAILURE(BgL_auxz00_6144, BFALSE,
																					BFALSE);
																			}
																		BgL_auxz00_6140 =
																			(long) CINT(BgL_tmpz00_6141);
																	}
																	BgL_weakz00_1578 =
																		VECTOR_REF(BgL_opt1159z00_51,
																		BgL_auxz00_6140);
																}
															else
																{	/* Llib/hash.scm 236 */
																	BFALSE;
																}
														}
													}
													{	/* Llib/hash.scm 236 */
														obj_t BgL_bucketzd2expansionzd2_1603;

														BgL_bucketzd2expansionzd2_1603 =
															BgL_bucketzd2expansionzd2_1571;
														{	/* Llib/hash.scm 236 */
															obj_t BgL_eqtestz00_1604;

															BgL_eqtestz00_1604 = BgL_eqtestz00_1572;
															{	/* Llib/hash.scm 236 */
																obj_t BgL_hashz00_1605;

																BgL_hashz00_1605 = BgL_hashz00_1573;
																{	/* Llib/hash.scm 236 */
																	obj_t BgL_maxzd2bucketzd2lengthz00_1606;

																	BgL_maxzd2bucketzd2lengthz00_1606 =
																		BgL_maxzd2bucketzd2lengthz00_1574;
																	{	/* Llib/hash.scm 236 */
																		obj_t BgL_maxzd2lengthzd2_1607;

																		BgL_maxzd2lengthzd2_1607 =
																			BgL_maxzd2lengthzd2_1575;
																		{	/* Llib/hash.scm 236 */
																			obj_t BgL_persistentz00_1608;

																			BgL_persistentz00_1608 =
																				BgL_persistentz00_1576;
																			{	/* Llib/hash.scm 236 */
																				obj_t BgL_siza7eza7_1609;

																				BgL_siza7eza7_1609 = BgL_siza7eza7_1577;
																				return
																					BGl_createzd2hashtablezd2zz__hashz00
																					(BgL_bucketzd2expansionzd2_1603,
																					BgL_eqtestz00_1604, BgL_hashz00_1605,
																					BgL_maxzd2bucketzd2lengthz00_1606,
																					BgL_maxzd2lengthzd2_1607,
																					BgL_persistentz00_1608,
																					BgL_siza7eza7_1609, BgL_weakz00_1578);
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* create-hashtable */
	BGL_EXPORTED_DEF obj_t BGl_createzd2hashtablezd2zz__hashz00(obj_t
		BgL_bucketzd2expansionzd2_43, obj_t BgL_eqtestz00_44, obj_t BgL_hashz00_45,
		obj_t BgL_maxzd2bucketzd2lengthz00_46, obj_t BgL_maxzd2lengthzd2_47,
		obj_t BgL_persistentz00_48, obj_t BgL_siza7eza7_49, obj_t BgL_weakz00_50)
	{
		{	/* Llib/hash.scm 236 */
			{	/* Llib/hash.scm 245 */
				long BgL_weakz00_1618;

				if ((BgL_weakz00_50 == BGl_symbol2556z00zz__hashz00))
					{	/* Llib/hash.scm 245 */
						BgL_weakz00_1618 = 1L;
					}
				else
					{	/* Llib/hash.scm 245 */
						if ((BgL_weakz00_50 == BGl_symbol2558z00zz__hashz00))
							{	/* Llib/hash.scm 245 */
								BgL_weakz00_1618 = 2L;
							}
						else
							{	/* Llib/hash.scm 245 */
								if ((BgL_weakz00_50 == BGl_symbol2560z00zz__hashz00))
									{	/* Llib/hash.scm 245 */
										BgL_weakz00_1618 = 3L;
									}
								else
									{	/* Llib/hash.scm 245 */
										if ((BgL_weakz00_50 == BGl_symbol2530z00zz__hashz00))
											{	/* Llib/hash.scm 245 */
												BgL_weakz00_1618 = 0L;
											}
										else
											{	/* Llib/hash.scm 245 */
												if ((BgL_weakz00_50 == BGl_symbol2562z00zz__hashz00))
													{	/* Llib/hash.scm 245 */
														BgL_weakz00_1618 = 8L;
													}
												else
													{	/* Llib/hash.scm 245 */
														if (
															(BgL_weakz00_50 == BGl_symbol2564z00zz__hashz00))
															{	/* Llib/hash.scm 245 */
																BgL_weakz00_1618 = 4L;
															}
														else
															{	/* Llib/hash.scm 245 */
																if (CBOOL(BgL_weakz00_50))
																	{	/* Llib/hash.scm 252 */
																		BgL_weakz00_1618 = 2L;
																	}
																else
																	{	/* Llib/hash.scm 252 */
																		BgL_weakz00_1618 = 0L;
																	}
															}
													}
											}
									}
							}
					}
				if (CBOOL(BgL_persistentz00_48))
					{	/* Llib/hash.scm 253 */
						if (CBOOL(BgL_hashz00_45))
							{	/* Llib/hash.scm 254 */
								BGl_errorz00zz__errorz00(BGl_string2550z00zz__hashz00,
									BGl_string2566z00zz__hashz00, BgL_hashz00_45);
							}
						else
							{	/* Llib/hash.scm 254 */
								BgL_hashz00_45 = BGl_symbol2567z00zz__hashz00;
							}
					}
				else
					{	/* Llib/hash.scm 253 */
						BFALSE;
					}
				{	/* Llib/hash.scm 259 */
					bool_t BgL_test2805z00_6170;

					if ((BINT(BgL_weakz00_1618) == BINT(8L)))
						{	/* Llib/hash.scm 259 */
							BgL_test2805z00_6170 = ((bool_t) 1);
						}
					else
						{	/* Llib/hash.scm 259 */
							BgL_test2805z00_6170 = (BINT(BgL_weakz00_1618) == BINT(4L));
						}
					if (BgL_test2805z00_6170)
						{	/* Llib/hash.scm 259 */
							if (CBOOL(BgL_eqtestz00_44))
								{	/* Llib/hash.scm 261 */
									return
										BGl_errorz00zz__errorz00(BGl_string2550z00zz__hashz00,
										BGl_string2568z00zz__hashz00, BgL_eqtestz00_44);
								}
							else
								{	/* Llib/hash.scm 261 */
									if (CBOOL(BgL_hashz00_45))
										{	/* Llib/hash.scm 264 */
											return
												BGl_errorz00zz__errorz00(BGl_string2550z00zz__hashz00,
												BGl_string2569z00zz__hashz00, BgL_hashz00_45);
										}
									else
										{	/* Llib/hash.scm 264 */
											if ((BINT(BgL_weakz00_1618) == BINT(8L)))
												{	/* Llib/hash.scm 268 */
													obj_t BgL_arg1364z00_1620;

													BgL_arg1364z00_1620 =
														make_vector(
														(3L * (long) CINT(BgL_siza7eza7_49)), BFALSE);
													{	/* Llib/hash.scm 268 */
														obj_t BgL_newz00_3294;

														BgL_newz00_3294 =
															create_struct(BGl_symbol2527z00zz__hashz00,
															(int) (8L));
														{	/* Llib/hash.scm 268 */
															obj_t BgL_auxz00_6195;
															int BgL_tmpz00_6193;

															BgL_auxz00_6195 = BINT(0L);
															BgL_tmpz00_6193 = (int) (7L);
															STRUCT_SET(BgL_newz00_3294, BgL_tmpz00_6193,
																BgL_auxz00_6195);
														}
														{	/* Llib/hash.scm 268 */
															obj_t BgL_auxz00_6200;
															int BgL_tmpz00_6198;

															BgL_auxz00_6200 = BINT(0L);
															BgL_tmpz00_6198 = (int) (6L);
															STRUCT_SET(BgL_newz00_3294, BgL_tmpz00_6198,
																BgL_auxz00_6200);
														}
														{	/* Llib/hash.scm 268 */
															obj_t BgL_auxz00_6205;
															int BgL_tmpz00_6203;

															BgL_auxz00_6205 = BINT(BgL_weakz00_1618);
															BgL_tmpz00_6203 = (int) (5L);
															STRUCT_SET(BgL_newz00_3294, BgL_tmpz00_6203,
																BgL_auxz00_6205);
														}
														{	/* Llib/hash.scm 268 */
															int BgL_tmpz00_6208;

															BgL_tmpz00_6208 = (int) (4L);
															STRUCT_SET(BgL_newz00_3294, BgL_tmpz00_6208,
																BGl_listzd2envzd2zz__r4_pairs_and_lists_6_3z00);
														}
														{	/* Llib/hash.scm 268 */
															int BgL_tmpz00_6211;

															BgL_tmpz00_6211 = (int) (3L);
															STRUCT_SET(BgL_newz00_3294, BgL_tmpz00_6211,
																BGl_eqzf3zd2envz21zz__r4_equivalence_6_2z00);
														}
														{	/* Llib/hash.scm 268 */
															int BgL_tmpz00_6214;

															BgL_tmpz00_6214 = (int) (2L);
															STRUCT_SET(BgL_newz00_3294, BgL_tmpz00_6214,
																BgL_arg1364z00_1620);
														}
														{	/* Llib/hash.scm 268 */
															int BgL_tmpz00_6217;

															BgL_tmpz00_6217 = (int) (1L);
															STRUCT_SET(BgL_newz00_3294, BgL_tmpz00_6217,
																BgL_siza7eza7_49);
														}
														{	/* Llib/hash.scm 268 */
															obj_t BgL_auxz00_6222;
															int BgL_tmpz00_6220;

															BgL_auxz00_6222 = BINT(0L);
															BgL_tmpz00_6220 = (int) (0L);
															STRUCT_SET(BgL_newz00_3294, BgL_tmpz00_6220,
																BgL_auxz00_6222);
														}
														return BgL_newz00_3294;
													}
												}
											else
												{	/* Llib/hash.scm 270 */
													obj_t BgL_arg1366z00_1622;

													BgL_arg1366z00_1622 =
														make_vector((long) CINT(BgL_siza7eza7_49), BNIL);
													{	/* Llib/hash.scm 270 */
														obj_t BgL_newz00_3305;

														BgL_newz00_3305 =
															create_struct(BGl_symbol2527z00zz__hashz00,
															(int) (8L));
														{	/* Llib/hash.scm 270 */
															int BgL_tmpz00_6229;

															BgL_tmpz00_6229 = (int) (7L);
															STRUCT_SET(BgL_newz00_3305, BgL_tmpz00_6229,
																BgL_bucketzd2expansionzd2_43);
														}
														{	/* Llib/hash.scm 270 */
															int BgL_tmpz00_6232;

															BgL_tmpz00_6232 = (int) (6L);
															STRUCT_SET(BgL_newz00_3305, BgL_tmpz00_6232,
																BgL_maxzd2lengthzd2_47);
														}
														{	/* Llib/hash.scm 270 */
															obj_t BgL_auxz00_6237;
															int BgL_tmpz00_6235;

															BgL_auxz00_6237 = BINT(BgL_weakz00_1618);
															BgL_tmpz00_6235 = (int) (5L);
															STRUCT_SET(BgL_newz00_3305, BgL_tmpz00_6235,
																BgL_auxz00_6237);
														}
														{	/* Llib/hash.scm 270 */
															int BgL_tmpz00_6240;

															BgL_tmpz00_6240 = (int) (4L);
															STRUCT_SET(BgL_newz00_3305, BgL_tmpz00_6240,
																BGl_proc2570z00zz__hashz00);
														}
														{	/* Llib/hash.scm 270 */
															int BgL_tmpz00_6243;

															BgL_tmpz00_6243 = (int) (3L);
															STRUCT_SET(BgL_newz00_3305, BgL_tmpz00_6243,
																BGl_stringzd3zf3zd2envzf2zz__r4_strings_6_7z00);
														}
														{	/* Llib/hash.scm 270 */
															int BgL_tmpz00_6246;

															BgL_tmpz00_6246 = (int) (2L);
															STRUCT_SET(BgL_newz00_3305, BgL_tmpz00_6246,
																BgL_arg1366z00_1622);
														}
														{	/* Llib/hash.scm 270 */
															int BgL_tmpz00_6249;

															BgL_tmpz00_6249 = (int) (1L);
															STRUCT_SET(BgL_newz00_3305, BgL_tmpz00_6249,
																BgL_maxzd2bucketzd2lengthz00_46);
														}
														{	/* Llib/hash.scm 270 */
															obj_t BgL_auxz00_6254;
															int BgL_tmpz00_6252;

															BgL_auxz00_6254 = BINT(0L);
															BgL_tmpz00_6252 = (int) (0L);
															STRUCT_SET(BgL_newz00_3305, BgL_tmpz00_6252,
																BgL_auxz00_6254);
														}
														return BgL_newz00_3305;
													}
												}
										}
								}
						}
					else
						{	/* Llib/hash.scm 274 */
							obj_t BgL_arg1370z00_1628;

							BgL_arg1370z00_1628 =
								make_vector((long) CINT(BgL_siza7eza7_49), BNIL);
							{	/* Llib/hash.scm 274 */
								obj_t BgL_hashnz00_3314;

								BgL_hashnz00_3314 = BgL_hashz00_45;
								{	/* Llib/hash.scm 274 */
									obj_t BgL_newz00_3315;

									BgL_newz00_3315 =
										create_struct(BGl_symbol2527z00zz__hashz00, (int) (8L));
									{	/* Llib/hash.scm 274 */
										int BgL_tmpz00_6261;

										BgL_tmpz00_6261 = (int) (7L);
										STRUCT_SET(BgL_newz00_3315, BgL_tmpz00_6261,
											BgL_bucketzd2expansionzd2_43);
									}
									{	/* Llib/hash.scm 274 */
										int BgL_tmpz00_6264;

										BgL_tmpz00_6264 = (int) (6L);
										STRUCT_SET(BgL_newz00_3315, BgL_tmpz00_6264,
											BgL_maxzd2lengthzd2_47);
									}
									{	/* Llib/hash.scm 274 */
										obj_t BgL_auxz00_6269;
										int BgL_tmpz00_6267;

										BgL_auxz00_6269 = BINT(BgL_weakz00_1618);
										BgL_tmpz00_6267 = (int) (5L);
										STRUCT_SET(BgL_newz00_3315, BgL_tmpz00_6267,
											BgL_auxz00_6269);
									}
									{	/* Llib/hash.scm 274 */
										int BgL_tmpz00_6272;

										BgL_tmpz00_6272 = (int) (4L);
										STRUCT_SET(BgL_newz00_3315, BgL_tmpz00_6272,
											BgL_hashnz00_3314);
									}
									{	/* Llib/hash.scm 274 */
										int BgL_tmpz00_6275;

										BgL_tmpz00_6275 = (int) (3L);
										STRUCT_SET(BgL_newz00_3315, BgL_tmpz00_6275,
											BgL_eqtestz00_44);
									}
									{	/* Llib/hash.scm 274 */
										int BgL_tmpz00_6278;

										BgL_tmpz00_6278 = (int) (2L);
										STRUCT_SET(BgL_newz00_3315, BgL_tmpz00_6278,
											BgL_arg1370z00_1628);
									}
									{	/* Llib/hash.scm 274 */
										int BgL_tmpz00_6281;

										BgL_tmpz00_6281 = (int) (1L);
										STRUCT_SET(BgL_newz00_3315, BgL_tmpz00_6281,
											BgL_maxzd2bucketzd2lengthz00_46);
									}
									{	/* Llib/hash.scm 274 */
										obj_t BgL_auxz00_6286;
										int BgL_tmpz00_6284;

										BgL_auxz00_6286 = BINT(0L);
										BgL_tmpz00_6284 = (int) (0L);
										STRUCT_SET(BgL_newz00_3315, BgL_tmpz00_6284,
											BgL_auxz00_6286);
									}
									return BgL_newz00_3315;
								}
							}
						}
				}
			}
		}

	}



/* &<@anonymous:1368> */
	obj_t BGl_z62zc3z04anonymousza31368ze3ze5zz__hashz00(obj_t BgL_envz00_5383,
		obj_t BgL_sz00_5384)
	{
		{	/* Llib/hash.scm 272 */
			{	/* Llib/hash.scm 272 */
				long BgL_tmpz00_6289;

				{	/* Llib/hash.scm 272 */
					long BgL_arg1369z00_5648;

					BgL_arg1369z00_5648 = STRING_LENGTH(((obj_t) BgL_sz00_5384));
					BgL_tmpz00_6289 =
						bgl_string_hash(BSTRING_TO_STRING(BgL_sz00_5384),
						(int) (0L), (int) (BgL_arg1369z00_5648));
				}
				return BINT(BgL_tmpz00_6289);
			}
		}

	}



/* hashtable? */
	BGL_EXPORTED_DEF bool_t BGl_hashtablezf3zf3zz__hashz00(obj_t BgL_objz00_53)
	{
		{	/* Llib/hash.scm 281 */
			if (STRUCTP(BgL_objz00_53))
				{	/* Llib/hash.scm 282 */
					return (STRUCT_KEY(BgL_objz00_53) == BGl_symbol2527z00zz__hashz00);
				}
			else
				{	/* Llib/hash.scm 282 */
					return ((bool_t) 0);
				}
		}

	}



/* &hashtable? */
	obj_t BGl_z62hashtablezf3z91zz__hashz00(obj_t BgL_envz00_5388,
		obj_t BgL_objz00_5389)
	{
		{	/* Llib/hash.scm 281 */
			return BBOOL(BGl_hashtablezf3zf3zz__hashz00(BgL_objz00_5389));
		}

	}



/* hashtable-weak-keys? */
	BGL_EXPORTED_DEF bool_t BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(obj_t
		BgL_tablez00_57)
	{
		{	/* Llib/hash.scm 305 */
			if ((0L == (1L & (long) CINT(STRUCT_REF(BgL_tablez00_57, (int) (5L))))))
				{	/* Llib/hash.scm 306 */
					return ((bool_t) 0);
				}
			else
				{	/* Llib/hash.scm 306 */
					return ((bool_t) 1);
				}
		}

	}



/* &hashtable-weak-keys? */
	obj_t BGl_z62hashtablezd2weakzd2keyszf3z91zz__hashz00(obj_t BgL_envz00_5390,
		obj_t BgL_tablez00_5391)
	{
		{	/* Llib/hash.scm 305 */
			{	/* Llib/hash.scm 306 */
				bool_t BgL_tmpz00_6309;

				{	/* Llib/hash.scm 306 */
					obj_t BgL_auxz00_6310;

					if (STRUCTP(BgL_tablez00_5391))
						{	/* Llib/hash.scm 306 */
							BgL_auxz00_6310 = BgL_tablez00_5391;
						}
					else
						{
							obj_t BgL_auxz00_6313;

							BgL_auxz00_6313 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
								BINT(11661L), BGl_string2571z00zz__hashz00,
								BGl_string2572z00zz__hashz00, BgL_tablez00_5391);
							FAILURE(BgL_auxz00_6313, BFALSE, BFALSE);
						}
					BgL_tmpz00_6309 =
						BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(BgL_auxz00_6310);
				}
				return BBOOL(BgL_tmpz00_6309);
			}
		}

	}



/* hashtable-weak-data? */
	BGL_EXPORTED_DEF bool_t BGl_hashtablezd2weakzd2datazf3zf3zz__hashz00(obj_t
		BgL_tablez00_58)
	{
		{	/* Llib/hash.scm 311 */
			if ((0L == (2L & (long) CINT(STRUCT_REF(BgL_tablez00_58, (int) (5L))))))
				{	/* Llib/hash.scm 312 */
					return ((bool_t) 0);
				}
			else
				{	/* Llib/hash.scm 312 */
					return ((bool_t) 1);
				}
		}

	}



/* &hashtable-weak-data? */
	obj_t BGl_z62hashtablezd2weakzd2datazf3z91zz__hashz00(obj_t BgL_envz00_5392,
		obj_t BgL_tablez00_5393)
	{
		{	/* Llib/hash.scm 311 */
			{	/* Llib/hash.scm 312 */
				bool_t BgL_tmpz00_6325;

				{	/* Llib/hash.scm 312 */
					obj_t BgL_auxz00_6326;

					if (STRUCTP(BgL_tablez00_5393))
						{	/* Llib/hash.scm 312 */
							BgL_auxz00_6326 = BgL_tablez00_5393;
						}
					else
						{
							obj_t BgL_auxz00_6329;

							BgL_auxz00_6329 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
								BINT(11999L), BGl_string2573z00zz__hashz00,
								BGl_string2572z00zz__hashz00, BgL_tablez00_5393);
							FAILURE(BgL_auxz00_6329, BFALSE, BFALSE);
						}
					BgL_tmpz00_6325 =
						BGl_hashtablezd2weakzd2datazf3zf3zz__hashz00(BgL_auxz00_6326);
				}
				return BBOOL(BgL_tmpz00_6325);
			}
		}

	}



/* hashtable-size */
	BGL_EXPORTED_DEF long BGl_hashtablezd2siza7ez75zz__hashz00(obj_t
		BgL_tablez00_59)
	{
		{	/* Llib/hash.scm 317 */
			return (long) CINT(STRUCT_REF(BgL_tablez00_59, (int) (0L)));
		}

	}



/* &hashtable-size */
	obj_t BGl_z62hashtablezd2siza7ez17zz__hashz00(obj_t BgL_envz00_5394,
		obj_t BgL_tablez00_5395)
	{
		{	/* Llib/hash.scm 317 */
			{	/* Llib/hash.scm 318 */
				long BgL_tmpz00_6338;

				{	/* Llib/hash.scm 318 */
					obj_t BgL_auxz00_6339;

					if (STRUCTP(BgL_tablez00_5395))
						{	/* Llib/hash.scm 318 */
							BgL_auxz00_6339 = BgL_tablez00_5395;
						}
					else
						{
							obj_t BgL_auxz00_6342;

							BgL_auxz00_6342 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
								BINT(12331L), BGl_string2574z00zz__hashz00,
								BGl_string2572z00zz__hashz00, BgL_tablez00_5395);
							FAILURE(BgL_auxz00_6342, BFALSE, BFALSE);
						}
					BgL_tmpz00_6338 =
						BGl_hashtablezd2siza7ez75zz__hashz00(BgL_auxz00_6339);
				}
				return BINT(BgL_tmpz00_6338);
			}
		}

	}



/* hashtable->vector */
	BGL_EXPORTED_DEF obj_t BGl_hashtablezd2ze3vectorz31zz__hashz00(obj_t
		BgL_tablez00_60)
	{
		{	/* Llib/hash.scm 323 */
			{	/* Llib/hash.scm 325 */
				bool_t BgL_test2816z00_6348;

				if ((0L == ((long) CINT(STRUCT_REF(BgL_tablez00_60, (int) (5L))) & 8L)))
					{	/* Llib/hash.scm 294 */
						BgL_test2816z00_6348 = ((bool_t) 0);
					}
				else
					{	/* Llib/hash.scm 294 */
						BgL_test2816z00_6348 = ((bool_t) 1);
					}
				if (BgL_test2816z00_6348)
					{	/* Llib/hash.scm 325 */
						BGL_TAIL return
							BGl_openzd2stringzd2hashtablezd2ze3vectorz31zz__hashz00
							(BgL_tablez00_60);
					}
				else
					{	/* Llib/hash.scm 327 */
						bool_t BgL_test2818z00_6356;

						if (
							(0L ==
								((long) CINT(STRUCT_REF(BgL_tablez00_60, (int) (5L))) & 3L)))
							{	/* Llib/hash.scm 288 */
								BgL_test2818z00_6356 = ((bool_t) 0);
							}
						else
							{	/* Llib/hash.scm 288 */
								BgL_test2818z00_6356 = ((bool_t) 1);
							}
						if (BgL_test2818z00_6356)
							{	/* Llib/hash.scm 327 */
								BGL_TAIL return
									BGl_weakzd2hashtablezd2ze3vectorze3zz__weakhashz00
									(BgL_tablez00_60);
							}
						else
							{	/* Llib/hash.scm 327 */
								BGL_TAIL return
									BGl_plainzd2hashtablezd2ze3vectorze3zz__hashz00
									(BgL_tablez00_60);
							}
					}
			}
		}

	}



/* &hashtable->vector */
	obj_t BGl_z62hashtablezd2ze3vectorz53zz__hashz00(obj_t BgL_envz00_5396,
		obj_t BgL_tablez00_5397)
	{
		{	/* Llib/hash.scm 323 */
			{	/* Llib/hash.scm 325 */
				obj_t BgL_auxz00_6365;

				if (STRUCTP(BgL_tablez00_5397))
					{	/* Llib/hash.scm 325 */
						BgL_auxz00_6365 = BgL_tablez00_5397;
					}
				else
					{
						obj_t BgL_auxz00_6368;

						BgL_auxz00_6368 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(12636L), BGl_string2575z00zz__hashz00,
							BGl_string2572z00zz__hashz00, BgL_tablez00_5397);
						FAILURE(BgL_auxz00_6368, BFALSE, BFALSE);
					}
				return BGl_hashtablezd2ze3vectorz31zz__hashz00(BgL_auxz00_6365);
			}
		}

	}



/* open-string-hashtable->vector */
	obj_t BGl_openzd2stringzd2hashtablezd2ze3vectorz31zz__hashz00(obj_t
		BgL_tablez00_61)
	{
		{	/* Llib/hash.scm 335 */
			{	/* Llib/hash.scm 336 */
				obj_t BgL_siza7eza7_1659;

				BgL_siza7eza7_1659 = STRUCT_REF(BgL_tablez00_61, (int) (1L));
				{	/* Llib/hash.scm 336 */
					long BgL_siza7e3za7_1660;

					BgL_siza7e3za7_1660 = (3L * (long) CINT(BgL_siza7eza7_1659));
					{	/* Llib/hash.scm 337 */
						obj_t BgL_bucketsz00_1661;

						BgL_bucketsz00_1661 = STRUCT_REF(BgL_tablez00_61, (int) (2L));
						{	/* Llib/hash.scm 338 */
							obj_t BgL_vecz00_1662;

							BgL_vecz00_1662 =
								make_vector((long) CINT(BgL_siza7eza7_1659), BUNSPEC);
							{	/* Llib/hash.scm 339 */

								{
									long BgL_iz00_1664;
									long BgL_wz00_1665;

									BgL_iz00_1664 = 0L;
									BgL_wz00_1665 = 0L;
								BgL_zc3z04anonymousza31403ze3z87_1666:
									if ((BgL_iz00_1664 == BgL_siza7e3za7_1660))
										{	/* Llib/hash.scm 342 */
											return BgL_vecz00_1662;
										}
									else
										{	/* Llib/hash.scm 344 */
											bool_t BgL_test2822z00_6383;

											if (CBOOL(VECTOR_REF(
														((obj_t) BgL_bucketsz00_1661), BgL_iz00_1664)))
												{	/* Llib/hash.scm 344 */
													long BgL_arg1414z00_1677;

													BgL_arg1414z00_1677 = (BgL_iz00_1664 + 2L);
													BgL_test2822z00_6383 =
														CBOOL(VECTOR_REF(
															((obj_t) BgL_bucketsz00_1661),
															BgL_arg1414z00_1677));
												}
											else
												{	/* Llib/hash.scm 344 */
													BgL_test2822z00_6383 = ((bool_t) 0);
												}
											if (BgL_test2822z00_6383)
												{	/* Llib/hash.scm 344 */
													{	/* Llib/hash.scm 346 */
														obj_t BgL_arg1408z00_1671;

														{	/* Llib/hash.scm 346 */
															long BgL_arg1410z00_1672;

															BgL_arg1410z00_1672 = (BgL_iz00_1664 + 1L);
															BgL_arg1408z00_1671 =
																VECTOR_REF(
																((obj_t) BgL_bucketsz00_1661),
																BgL_arg1410z00_1672);
														}
														VECTOR_SET(BgL_vecz00_1662, BgL_wz00_1665,
															BgL_arg1408z00_1671);
													}
													{
														long BgL_wz00_6398;
														long BgL_iz00_6396;

														BgL_iz00_6396 = (BgL_iz00_1664 + 3L);
														BgL_wz00_6398 = (BgL_wz00_1665 + 1L);
														BgL_wz00_1665 = BgL_wz00_6398;
														BgL_iz00_1664 = BgL_iz00_6396;
														goto BgL_zc3z04anonymousza31403ze3z87_1666;
													}
												}
											else
												{
													long BgL_iz00_6400;

													BgL_iz00_6400 = (BgL_iz00_1664 + 3L);
													BgL_iz00_1664 = BgL_iz00_6400;
													goto BgL_zc3z04anonymousza31403ze3z87_1666;
												}
										}
								}
							}
						}
					}
				}
			}
		}

	}



/* plain-hashtable->vector */
	obj_t BGl_plainzd2hashtablezd2ze3vectorze3zz__hashz00(obj_t BgL_tablez00_62)
	{
		{	/* Llib/hash.scm 353 */
			{	/* Llib/hash.scm 354 */
				obj_t BgL_vecz00_1680;

				BgL_vecz00_1680 =
					make_vector(
					(long) CINT(STRUCT_REF(BgL_tablez00_62, (int) (0L))), BUNSPEC);
				{	/* Llib/hash.scm 354 */
					obj_t BgL_bucketsz00_1681;

					BgL_bucketsz00_1681 = STRUCT_REF(BgL_tablez00_62, (int) (2L));
					{	/* Llib/hash.scm 356 */

						{
							long BgL_iz00_3411;
							long BgL_wz00_3412;

							BgL_iz00_3411 = 0L;
							BgL_wz00_3412 = 0L;
						BgL_loopz00_3410:
							if (
								(BgL_iz00_3411 == VECTOR_LENGTH(((obj_t) BgL_bucketsz00_1681))))
								{	/* Llib/hash.scm 359 */
									return BgL_vecz00_1680;
								}
							else
								{	/* Llib/hash.scm 361 */
									obj_t BgL_g1058z00_3416;

									BgL_g1058z00_3416 =
										VECTOR_REF(((obj_t) BgL_bucketsz00_1681), BgL_iz00_3411);
									{
										obj_t BgL_bucketz00_3420;
										long BgL_wz00_3421;

										BgL_bucketz00_3420 = BgL_g1058z00_3416;
										BgL_wz00_3421 = BgL_wz00_3412;
									BgL_liipz00_3419:
										if (NULLP(BgL_bucketz00_3420))
											{
												long BgL_wz00_6418;
												long BgL_iz00_6416;

												BgL_iz00_6416 = (BgL_iz00_3411 + 1L);
												BgL_wz00_6418 = BgL_wz00_3421;
												BgL_wz00_3412 = BgL_wz00_6418;
												BgL_iz00_3411 = BgL_iz00_6416;
												goto BgL_loopz00_3410;
											}
										else
											{	/* Llib/hash.scm 363 */
												{	/* Llib/hash.scm 366 */
													obj_t BgL_arg1421z00_3425;

													{	/* Llib/hash.scm 366 */
														obj_t BgL_pairz00_3429;

														BgL_pairz00_3429 =
															CAR(((obj_t) BgL_bucketz00_3420));
														BgL_arg1421z00_3425 = CDR(BgL_pairz00_3429);
													}
													VECTOR_SET(BgL_vecz00_1680, BgL_wz00_3421,
														BgL_arg1421z00_3425);
												}
												{	/* Llib/hash.scm 367 */
													obj_t BgL_arg1422z00_3432;
													long BgL_arg1423z00_3433;

													BgL_arg1422z00_3432 =
														CDR(((obj_t) BgL_bucketz00_3420));
													BgL_arg1423z00_3433 = (BgL_wz00_3421 + 1L);
													{
														long BgL_wz00_6427;
														obj_t BgL_bucketz00_6426;

														BgL_bucketz00_6426 = BgL_arg1422z00_3432;
														BgL_wz00_6427 = BgL_arg1423z00_3433;
														BgL_wz00_3421 = BgL_wz00_6427;
														BgL_bucketz00_3420 = BgL_bucketz00_6426;
														goto BgL_liipz00_3419;
													}
												}
											}
									}
								}
						}
					}
				}
			}
		}

	}



/* hashtable->list */
	BGL_EXPORTED_DEF obj_t BGl_hashtablezd2ze3listz31zz__hashz00(obj_t
		BgL_tablez00_63)
	{
		{	/* Llib/hash.scm 372 */
			{	/* Llib/hash.scm 374 */
				bool_t BgL_test2826z00_6428;

				if ((0L == ((long) CINT(STRUCT_REF(BgL_tablez00_63, (int) (5L))) & 8L)))
					{	/* Llib/hash.scm 294 */
						BgL_test2826z00_6428 = ((bool_t) 0);
					}
				else
					{	/* Llib/hash.scm 294 */
						BgL_test2826z00_6428 = ((bool_t) 1);
					}
				if (BgL_test2826z00_6428)
					{	/* Llib/hash.scm 374 */
						BGL_TAIL return
							BGl_openzd2stringzd2hashtablezd2ze3listz31zz__hashz00
							(BgL_tablez00_63);
					}
				else
					{	/* Llib/hash.scm 376 */
						bool_t BgL_test2828z00_6436;

						if (
							(0L ==
								((long) CINT(STRUCT_REF(BgL_tablez00_63, (int) (5L))) & 3L)))
							{	/* Llib/hash.scm 288 */
								BgL_test2828z00_6436 = ((bool_t) 0);
							}
						else
							{	/* Llib/hash.scm 288 */
								BgL_test2828z00_6436 = ((bool_t) 1);
							}
						if (BgL_test2828z00_6436)
							{	/* Llib/hash.scm 376 */
								BGL_TAIL return
									BGl_weakzd2hashtablezd2ze3listze3zz__weakhashz00
									(BgL_tablez00_63);
							}
						else
							{	/* Llib/hash.scm 376 */
								BGL_TAIL return
									BGl_plainzd2hashtablezd2ze3listze3zz__hashz00
									(BgL_tablez00_63);
							}
					}
			}
		}

	}



/* &hashtable->list */
	obj_t BGl_z62hashtablezd2ze3listz53zz__hashz00(obj_t BgL_envz00_5398,
		obj_t BgL_tablez00_5399)
	{
		{	/* Llib/hash.scm 372 */
			{	/* Llib/hash.scm 374 */
				obj_t BgL_auxz00_6445;

				if (STRUCTP(BgL_tablez00_5399))
					{	/* Llib/hash.scm 374 */
						BgL_auxz00_6445 = BgL_tablez00_5399;
					}
				else
					{
						obj_t BgL_auxz00_6448;

						BgL_auxz00_6448 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(14486L), BGl_string2576z00zz__hashz00,
							BGl_string2572z00zz__hashz00, BgL_tablez00_5399);
						FAILURE(BgL_auxz00_6448, BFALSE, BFALSE);
					}
				return BGl_hashtablezd2ze3listz31zz__hashz00(BgL_auxz00_6445);
			}
		}

	}



/* open-string-hashtable->list */
	obj_t BGl_openzd2stringzd2hashtablezd2ze3listz31zz__hashz00(obj_t
		BgL_tablez00_64)
	{
		{	/* Llib/hash.scm 384 */
			{	/* Llib/hash.scm 385 */
				obj_t BgL_siza7eza7_1704;

				BgL_siza7eza7_1704 = STRUCT_REF(BgL_tablez00_64, (int) (1L));
				{	/* Llib/hash.scm 385 */
					long BgL_siza7e3za7_1705;

					BgL_siza7e3za7_1705 = (3L * (long) CINT(BgL_siza7eza7_1704));
					{	/* Llib/hash.scm 386 */
						obj_t BgL_bucketsz00_1706;

						BgL_bucketsz00_1706 = STRUCT_REF(BgL_tablez00_64, (int) (2L));
						{	/* Llib/hash.scm 387 */

							{
								long BgL_iz00_1709;
								obj_t BgL_resz00_1710;

								BgL_iz00_1709 = 0L;
								BgL_resz00_1710 = BNIL;
							BgL_zc3z04anonymousza31428ze3z87_1711:
								if ((BgL_iz00_1709 == BgL_siza7e3za7_1705))
									{	/* Llib/hash.scm 390 */
										return BgL_resz00_1710;
									}
								else
									{	/* Llib/hash.scm 392 */
										bool_t BgL_test2832z00_6461;

										if (CBOOL(VECTOR_REF(
													((obj_t) BgL_bucketsz00_1706), BgL_iz00_1709)))
											{	/* Llib/hash.scm 392 */
												long BgL_arg1439z00_1722;

												BgL_arg1439z00_1722 = (BgL_iz00_1709 + 2L);
												BgL_test2832z00_6461 =
													CBOOL(VECTOR_REF(
														((obj_t) BgL_bucketsz00_1706),
														BgL_arg1439z00_1722));
											}
										else
											{	/* Llib/hash.scm 392 */
												BgL_test2832z00_6461 = ((bool_t) 0);
											}
										if (BgL_test2832z00_6461)
											{	/* Llib/hash.scm 393 */
												long BgL_arg1434z00_1716;
												obj_t BgL_arg1435z00_1717;

												BgL_arg1434z00_1716 = (BgL_iz00_1709 + 3L);
												{	/* Llib/hash.scm 393 */
													obj_t BgL_arg1436z00_1718;

													{	/* Llib/hash.scm 393 */
														long BgL_arg1437z00_1719;

														BgL_arg1437z00_1719 = (BgL_iz00_1709 + 1L);
														BgL_arg1436z00_1718 =
															VECTOR_REF(
															((obj_t) BgL_bucketsz00_1706),
															BgL_arg1437z00_1719);
													}
													BgL_arg1435z00_1717 =
														MAKE_YOUNG_PAIR(BgL_arg1436z00_1718,
														BgL_resz00_1710);
												}
												{
													obj_t BgL_resz00_6476;
													long BgL_iz00_6475;

													BgL_iz00_6475 = BgL_arg1434z00_1716;
													BgL_resz00_6476 = BgL_arg1435z00_1717;
													BgL_resz00_1710 = BgL_resz00_6476;
													BgL_iz00_1709 = BgL_iz00_6475;
													goto BgL_zc3z04anonymousza31428ze3z87_1711;
												}
											}
										else
											{
												long BgL_iz00_6477;

												BgL_iz00_6477 = (BgL_iz00_1709 + 3L);
												BgL_iz00_1709 = BgL_iz00_6477;
												goto BgL_zc3z04anonymousza31428ze3z87_1711;
											}
									}
							}
						}
					}
				}
			}
		}

	}



/* plain-hashtable->list */
	obj_t BGl_plainzd2hashtablezd2ze3listze3zz__hashz00(obj_t BgL_tablez00_65)
	{
		{	/* Llib/hash.scm 399 */
			{	/* Llib/hash.scm 400 */
				obj_t BgL_vecz00_1724;

				BgL_vecz00_1724 =
					make_vector(
					(long) CINT(STRUCT_REF(BgL_tablez00_65, (int) (0L))), BUNSPEC);
				{	/* Llib/hash.scm 400 */
					obj_t BgL_bucketsz00_1725;

					BgL_bucketsz00_1725 = STRUCT_REF(BgL_tablez00_65, (int) (2L));
					{	/* Llib/hash.scm 402 */

						{
							long BgL_iz00_1729;
							obj_t BgL_resz00_1730;

							BgL_iz00_1729 = 0L;
							BgL_resz00_1730 = BNIL;
						BgL_zc3z04anonymousza31440ze3z87_1731:
							if (
								(BgL_iz00_1729 == VECTOR_LENGTH(((obj_t) BgL_bucketsz00_1725))))
								{	/* Llib/hash.scm 405 */
									return BgL_resz00_1730;
								}
							else
								{	/* Llib/hash.scm 407 */
									obj_t BgL_g1061z00_1733;

									BgL_g1061z00_1733 =
										VECTOR_REF(((obj_t) BgL_bucketsz00_1725), BgL_iz00_1729);
									{
										obj_t BgL_bucketz00_3500;
										obj_t BgL_resz00_3501;

										BgL_bucketz00_3500 = BgL_g1061z00_1733;
										BgL_resz00_3501 = BgL_resz00_1730;
									BgL_liipz00_3499:
										if (NULLP(BgL_bucketz00_3500))
											{
												obj_t BgL_resz00_6495;
												long BgL_iz00_6493;

												BgL_iz00_6493 = (BgL_iz00_1729 + 1L);
												BgL_resz00_6495 = BgL_resz00_3501;
												BgL_resz00_1730 = BgL_resz00_6495;
												BgL_iz00_1729 = BgL_iz00_6493;
												goto BgL_zc3z04anonymousza31440ze3z87_1731;
											}
										else
											{	/* Llib/hash.scm 411 */
												obj_t BgL_arg1445z00_3509;
												obj_t BgL_arg1446z00_3510;

												BgL_arg1445z00_3509 = CDR(((obj_t) BgL_bucketz00_3500));
												{	/* Llib/hash.scm 411 */
													obj_t BgL_arg1447z00_3511;

													{	/* Llib/hash.scm 411 */
														obj_t BgL_pairz00_3517;

														BgL_pairz00_3517 =
															CAR(((obj_t) BgL_bucketz00_3500));
														BgL_arg1447z00_3511 = CDR(BgL_pairz00_3517);
													}
													BgL_arg1446z00_3510 =
														MAKE_YOUNG_PAIR(BgL_arg1447z00_3511,
														BgL_resz00_3501);
												}
												{
													obj_t BgL_resz00_6503;
													obj_t BgL_bucketz00_6502;

													BgL_bucketz00_6502 = BgL_arg1445z00_3509;
													BgL_resz00_6503 = BgL_arg1446z00_3510;
													BgL_resz00_3501 = BgL_resz00_6503;
													BgL_bucketz00_3500 = BgL_bucketz00_6502;
													goto BgL_liipz00_3499;
												}
											}
									}
								}
						}
					}
				}
			}
		}

	}



/* hashtable-key-list */
	BGL_EXPORTED_DEF obj_t BGl_hashtablezd2keyzd2listz00zz__hashz00(obj_t
		BgL_tablez00_66)
	{
		{	/* Llib/hash.scm 416 */
			{	/* Llib/hash.scm 418 */
				bool_t BgL_test2836z00_6504;

				if ((0L == ((long) CINT(STRUCT_REF(BgL_tablez00_66, (int) (5L))) & 8L)))
					{	/* Llib/hash.scm 294 */
						BgL_test2836z00_6504 = ((bool_t) 0);
					}
				else
					{	/* Llib/hash.scm 294 */
						BgL_test2836z00_6504 = ((bool_t) 1);
					}
				if (BgL_test2836z00_6504)
					{	/* Llib/hash.scm 418 */
						BGL_TAIL return
							BGl_openzd2stringzd2hashtablezd2keyzd2listz00zz__hashz00
							(BgL_tablez00_66);
					}
				else
					{	/* Llib/hash.scm 420 */
						bool_t BgL_test2838z00_6512;

						if (
							(0L ==
								((long) CINT(STRUCT_REF(BgL_tablez00_66, (int) (5L))) & 3L)))
							{	/* Llib/hash.scm 288 */
								BgL_test2838z00_6512 = ((bool_t) 0);
							}
						else
							{	/* Llib/hash.scm 288 */
								BgL_test2838z00_6512 = ((bool_t) 1);
							}
						if (BgL_test2838z00_6512)
							{	/* Llib/hash.scm 420 */
								BGL_TAIL return
									BGl_weakzd2hashtablezd2keyzd2listzd2zz__weakhashz00
									(BgL_tablez00_66);
							}
						else
							{	/* Llib/hash.scm 420 */
								BGL_TAIL return
									BGl_plainzd2hashtablezd2keyzd2listzd2zz__hashz00
									(BgL_tablez00_66);
							}
					}
			}
		}

	}



/* &hashtable-key-list */
	obj_t BGl_z62hashtablezd2keyzd2listz62zz__hashz00(obj_t BgL_envz00_5400,
		obj_t BgL_tablez00_5401)
	{
		{	/* Llib/hash.scm 416 */
			{	/* Llib/hash.scm 418 */
				obj_t BgL_auxz00_6521;

				if (STRUCTP(BgL_tablez00_5401))
					{	/* Llib/hash.scm 418 */
						BgL_auxz00_6521 = BgL_tablez00_5401;
					}
				else
					{
						obj_t BgL_auxz00_6524;

						BgL_auxz00_6524 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(16230L), BGl_string2577z00zz__hashz00,
							BGl_string2572z00zz__hashz00, BgL_tablez00_5401);
						FAILURE(BgL_auxz00_6524, BFALSE, BFALSE);
					}
				return BGl_hashtablezd2keyzd2listz00zz__hashz00(BgL_auxz00_6521);
			}
		}

	}



/* open-string-hashtable-key-list */
	obj_t BGl_openzd2stringzd2hashtablezd2keyzd2listz00zz__hashz00(obj_t
		BgL_tablez00_67)
	{
		{	/* Llib/hash.scm 428 */
			{	/* Llib/hash.scm 429 */
				obj_t BgL_siza7eza7_1749;

				BgL_siza7eza7_1749 = STRUCT_REF(BgL_tablez00_67, (int) (1L));
				{	/* Llib/hash.scm 429 */
					long BgL_siza7e3za7_1750;

					BgL_siza7e3za7_1750 = (3L * (long) CINT(BgL_siza7eza7_1749));
					{	/* Llib/hash.scm 430 */
						obj_t BgL_bucketsz00_1751;

						BgL_bucketsz00_1751 = STRUCT_REF(BgL_tablez00_67, (int) (2L));
						{	/* Llib/hash.scm 431 */

							{
								long BgL_iz00_1754;
								obj_t BgL_resz00_1755;

								BgL_iz00_1754 = 0L;
								BgL_resz00_1755 = BNIL;
							BgL_zc3z04anonymousza31452ze3z87_1756:
								if ((BgL_iz00_1754 == BgL_siza7e3za7_1750))
									{	/* Llib/hash.scm 434 */
										return BgL_resz00_1755;
									}
								else
									{	/* Llib/hash.scm 436 */
										bool_t BgL_test2842z00_6537;

										if (CBOOL(VECTOR_REF(
													((obj_t) BgL_bucketsz00_1751), BgL_iz00_1754)))
											{	/* Llib/hash.scm 436 */
												long BgL_arg1461z00_1766;

												BgL_arg1461z00_1766 = (BgL_iz00_1754 + 2L);
												BgL_test2842z00_6537 =
													CBOOL(VECTOR_REF(
														((obj_t) BgL_bucketsz00_1751),
														BgL_arg1461z00_1766));
											}
										else
											{	/* Llib/hash.scm 436 */
												BgL_test2842z00_6537 = ((bool_t) 0);
											}
										if (BgL_test2842z00_6537)
											{	/* Llib/hash.scm 437 */
												long BgL_arg1457z00_1761;
												obj_t BgL_arg1458z00_1762;

												BgL_arg1457z00_1761 = (BgL_iz00_1754 + 3L);
												{	/* Llib/hash.scm 437 */
													obj_t BgL_arg1459z00_1763;

													BgL_arg1459z00_1763 =
														VECTOR_REF(
														((obj_t) BgL_bucketsz00_1751), BgL_iz00_1754);
													BgL_arg1458z00_1762 =
														MAKE_YOUNG_PAIR(BgL_arg1459z00_1763,
														BgL_resz00_1755);
												}
												{
													obj_t BgL_resz00_6551;
													long BgL_iz00_6550;

													BgL_iz00_6550 = BgL_arg1457z00_1761;
													BgL_resz00_6551 = BgL_arg1458z00_1762;
													BgL_resz00_1755 = BgL_resz00_6551;
													BgL_iz00_1754 = BgL_iz00_6550;
													goto BgL_zc3z04anonymousza31452ze3z87_1756;
												}
											}
										else
											{
												long BgL_iz00_6552;

												BgL_iz00_6552 = (BgL_iz00_1754 + 3L);
												BgL_iz00_1754 = BgL_iz00_6552;
												goto BgL_zc3z04anonymousza31452ze3z87_1756;
											}
									}
							}
						}
					}
				}
			}
		}

	}



/* plain-hashtable-key-list */
	obj_t BGl_plainzd2hashtablezd2keyzd2listzd2zz__hashz00(obj_t BgL_tablez00_68)
	{
		{	/* Llib/hash.scm 443 */
			{	/* Llib/hash.scm 444 */
				obj_t BgL_vecz00_1768;

				BgL_vecz00_1768 =
					make_vector(
					(long) CINT(STRUCT_REF(BgL_tablez00_68, (int) (0L))), BUNSPEC);
				{	/* Llib/hash.scm 444 */
					obj_t BgL_bucketsz00_1769;

					BgL_bucketsz00_1769 = STRUCT_REF(BgL_tablez00_68, (int) (2L));
					{	/* Llib/hash.scm 446 */

						{
							long BgL_iz00_1773;
							obj_t BgL_resz00_1774;

							BgL_iz00_1773 = 0L;
							BgL_resz00_1774 = BNIL;
						BgL_zc3z04anonymousza31462ze3z87_1775:
							if (
								(BgL_iz00_1773 == VECTOR_LENGTH(((obj_t) BgL_bucketsz00_1769))))
								{	/* Llib/hash.scm 449 */
									return BgL_resz00_1774;
								}
							else
								{	/* Llib/hash.scm 451 */
									obj_t BgL_g1064z00_1777;

									BgL_g1064z00_1777 =
										VECTOR_REF(((obj_t) BgL_bucketsz00_1769), BgL_iz00_1773);
									{
										obj_t BgL_bucketz00_3581;
										obj_t BgL_resz00_3582;

										BgL_bucketz00_3581 = BgL_g1064z00_1777;
										BgL_resz00_3582 = BgL_resz00_1774;
									BgL_liipz00_3580:
										if (NULLP(BgL_bucketz00_3581))
											{
												obj_t BgL_resz00_6570;
												long BgL_iz00_6568;

												BgL_iz00_6568 = (BgL_iz00_1773 + 1L);
												BgL_resz00_6570 = BgL_resz00_3582;
												BgL_resz00_1774 = BgL_resz00_6570;
												BgL_iz00_1773 = BgL_iz00_6568;
												goto BgL_zc3z04anonymousza31462ze3z87_1775;
											}
										else
											{	/* Llib/hash.scm 455 */
												obj_t BgL_arg1467z00_3590;
												obj_t BgL_arg1468z00_3591;

												BgL_arg1467z00_3590 = CDR(((obj_t) BgL_bucketz00_3581));
												{	/* Llib/hash.scm 455 */
													obj_t BgL_arg1469z00_3592;

													{	/* Llib/hash.scm 455 */
														obj_t BgL_pairz00_3598;

														BgL_pairz00_3598 =
															CAR(((obj_t) BgL_bucketz00_3581));
														BgL_arg1469z00_3592 = CAR(BgL_pairz00_3598);
													}
													BgL_arg1468z00_3591 =
														MAKE_YOUNG_PAIR(BgL_arg1469z00_3592,
														BgL_resz00_3582);
												}
												{
													obj_t BgL_resz00_6578;
													obj_t BgL_bucketz00_6577;

													BgL_bucketz00_6577 = BgL_arg1467z00_3590;
													BgL_resz00_6578 = BgL_arg1468z00_3591;
													BgL_resz00_3582 = BgL_resz00_6578;
													BgL_bucketz00_3581 = BgL_bucketz00_6577;
													goto BgL_liipz00_3580;
												}
											}
									}
								}
						}
					}
				}
			}
		}

	}



/* hashtable-map */
	BGL_EXPORTED_DEF obj_t BGl_hashtablezd2mapzd2zz__hashz00(obj_t
		BgL_tablez00_69, obj_t BgL_funz00_70)
	{
		{	/* Llib/hash.scm 460 */
			{	/* Llib/hash.scm 462 */
				bool_t BgL_test2846z00_6579;

				if ((0L == ((long) CINT(STRUCT_REF(BgL_tablez00_69, (int) (5L))) & 8L)))
					{	/* Llib/hash.scm 294 */
						BgL_test2846z00_6579 = ((bool_t) 0);
					}
				else
					{	/* Llib/hash.scm 294 */
						BgL_test2846z00_6579 = ((bool_t) 1);
					}
				if (BgL_test2846z00_6579)
					{	/* Llib/hash.scm 462 */
						return
							BGl_openzd2stringzd2hashtablezd2ze3listz31zz__hashz00
							(BgL_tablez00_69);
					}
				else
					{	/* Llib/hash.scm 464 */
						bool_t BgL_test2848z00_6587;

						if (
							(0L ==
								((long) CINT(STRUCT_REF(BgL_tablez00_69, (int) (5L))) & 3L)))
							{	/* Llib/hash.scm 288 */
								BgL_test2848z00_6587 = ((bool_t) 0);
							}
						else
							{	/* Llib/hash.scm 288 */
								BgL_test2848z00_6587 = ((bool_t) 1);
							}
						if (BgL_test2848z00_6587)
							{	/* Llib/hash.scm 464 */
								BGL_TAIL return
									BGl_weakzd2hashtablezd2mapz00zz__weakhashz00(BgL_tablez00_69,
									BgL_funz00_70);
							}
						else
							{	/* Llib/hash.scm 464 */
								BGL_TAIL return
									BGl_plainzd2hashtablezd2mapz00zz__hashz00(BgL_tablez00_69,
									BgL_funz00_70);
							}
					}
			}
		}

	}



/* &hashtable-map */
	obj_t BGl_z62hashtablezd2mapzb0zz__hashz00(obj_t BgL_envz00_5402,
		obj_t BgL_tablez00_5403, obj_t BgL_funz00_5404)
	{
		{	/* Llib/hash.scm 460 */
			{	/* Llib/hash.scm 462 */
				obj_t BgL_auxz00_6603;
				obj_t BgL_auxz00_6596;

				if (PROCEDUREP(BgL_funz00_5404))
					{	/* Llib/hash.scm 462 */
						BgL_auxz00_6603 = BgL_funz00_5404;
					}
				else
					{
						obj_t BgL_auxz00_6606;

						BgL_auxz00_6606 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(17991L), BGl_string2578z00zz__hashz00,
							BGl_string2579z00zz__hashz00, BgL_funz00_5404);
						FAILURE(BgL_auxz00_6606, BFALSE, BFALSE);
					}
				if (STRUCTP(BgL_tablez00_5403))
					{	/* Llib/hash.scm 462 */
						BgL_auxz00_6596 = BgL_tablez00_5403;
					}
				else
					{
						obj_t BgL_auxz00_6599;

						BgL_auxz00_6599 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(17991L), BGl_string2578z00zz__hashz00,
							BGl_string2572z00zz__hashz00, BgL_tablez00_5403);
						FAILURE(BgL_auxz00_6599, BFALSE, BFALSE);
					}
				return
					BGl_hashtablezd2mapzd2zz__hashz00(BgL_auxz00_6596, BgL_auxz00_6603);
			}
		}

	}



/* open-string-hashtable-map */
	BGL_EXPORTED_DEF obj_t BGl_openzd2stringzd2hashtablezd2mapzd2zz__hashz00(obj_t
		BgL_tablez00_71, obj_t BgL_funz00_72)
	{
		{	/* Llib/hash.scm 472 */
			{	/* Llib/hash.scm 473 */
				obj_t BgL_siza7eza7_1793;

				BgL_siza7eza7_1793 = STRUCT_REF(BgL_tablez00_71, (int) (1L));
				{	/* Llib/hash.scm 473 */
					long BgL_siza7e3za7_1794;

					BgL_siza7e3za7_1794 = (3L * (long) CINT(BgL_siza7eza7_1793));
					{	/* Llib/hash.scm 474 */
						obj_t BgL_bucketsz00_1795;

						BgL_bucketsz00_1795 = STRUCT_REF(BgL_tablez00_71, (int) (2L));
						{	/* Llib/hash.scm 475 */

							{
								long BgL_iz00_1798;
								obj_t BgL_resz00_1799;

								BgL_iz00_1798 = 0L;
								BgL_resz00_1799 = BNIL;
							BgL_zc3z04anonymousza31476ze3z87_1800:
								if ((BgL_iz00_1798 == BgL_siza7e3za7_1794))
									{	/* Llib/hash.scm 478 */
										return BgL_resz00_1799;
									}
								else
									{	/* Llib/hash.scm 480 */
										bool_t BgL_test2853z00_6619;

										if (CBOOL(VECTOR_REF(
													((obj_t) BgL_bucketsz00_1795), BgL_iz00_1798)))
											{	/* Llib/hash.scm 480 */
												long BgL_arg1488z00_1813;

												BgL_arg1488z00_1813 = (BgL_iz00_1798 + 2L);
												BgL_test2853z00_6619 =
													CBOOL(VECTOR_REF(
														((obj_t) BgL_bucketsz00_1795),
														BgL_arg1488z00_1813));
											}
										else
											{	/* Llib/hash.scm 480 */
												BgL_test2853z00_6619 = ((bool_t) 0);
											}
										if (BgL_test2853z00_6619)
											{	/* Llib/hash.scm 481 */
												long BgL_arg1481z00_1805;
												obj_t BgL_arg1482z00_1806;

												BgL_arg1481z00_1805 = (BgL_iz00_1798 + 3L);
												{	/* Llib/hash.scm 483 */
													obj_t BgL_arg1483z00_1807;

													{	/* Llib/hash.scm 483 */
														obj_t BgL_arg1484z00_1808;
														obj_t BgL_arg1485z00_1809;

														BgL_arg1484z00_1808 =
															VECTOR_REF(
															((obj_t) BgL_bucketsz00_1795), BgL_iz00_1798);
														{	/* Llib/hash.scm 483 */
															long BgL_arg1486z00_1810;

															BgL_arg1486z00_1810 = (BgL_iz00_1798 + 1L);
															BgL_arg1485z00_1809 =
																VECTOR_REF(
																((obj_t) BgL_bucketsz00_1795),
																BgL_arg1486z00_1810);
														}
														BgL_arg1483z00_1807 =
															BGL_PROCEDURE_CALL2(BgL_funz00_72,
															BgL_arg1484z00_1808, BgL_arg1485z00_1809);
													}
													BgL_arg1482z00_1806 =
														MAKE_YOUNG_PAIR(BgL_arg1483z00_1807,
														BgL_resz00_1799);
												}
												{
													obj_t BgL_resz00_6641;
													long BgL_iz00_6640;

													BgL_iz00_6640 = BgL_arg1481z00_1805;
													BgL_resz00_6641 = BgL_arg1482z00_1806;
													BgL_resz00_1799 = BgL_resz00_6641;
													BgL_iz00_1798 = BgL_iz00_6640;
													goto BgL_zc3z04anonymousza31476ze3z87_1800;
												}
											}
										else
											{
												long BgL_iz00_6642;

												BgL_iz00_6642 = (BgL_iz00_1798 + 3L);
												BgL_iz00_1798 = BgL_iz00_6642;
												goto BgL_zc3z04anonymousza31476ze3z87_1800;
											}
									}
							}
						}
					}
				}
			}
		}

	}



/* &open-string-hashtable-map */
	obj_t BGl_z62openzd2stringzd2hashtablezd2mapzb0zz__hashz00(obj_t
		BgL_envz00_5405, obj_t BgL_tablez00_5406, obj_t BgL_funz00_5407)
	{
		{	/* Llib/hash.scm 472 */
			{	/* Llib/hash.scm 473 */
				obj_t BgL_auxz00_6651;
				obj_t BgL_auxz00_6644;

				if (PROCEDUREP(BgL_funz00_5407))
					{	/* Llib/hash.scm 473 */
						BgL_auxz00_6651 = BgL_funz00_5407;
					}
				else
					{
						obj_t BgL_auxz00_6654;

						BgL_auxz00_6654 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(18471L), BGl_string2580z00zz__hashz00,
							BGl_string2579z00zz__hashz00, BgL_funz00_5407);
						FAILURE(BgL_auxz00_6654, BFALSE, BFALSE);
					}
				if (STRUCTP(BgL_tablez00_5406))
					{	/* Llib/hash.scm 473 */
						BgL_auxz00_6644 = BgL_tablez00_5406;
					}
				else
					{
						obj_t BgL_auxz00_6647;

						BgL_auxz00_6647 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(18471L), BGl_string2580z00zz__hashz00,
							BGl_string2572z00zz__hashz00, BgL_tablez00_5406);
						FAILURE(BgL_auxz00_6647, BFALSE, BFALSE);
					}
				return
					BGl_openzd2stringzd2hashtablezd2mapzd2zz__hashz00(BgL_auxz00_6644,
					BgL_auxz00_6651);
			}
		}

	}



/* plain-hashtable-map */
	obj_t BGl_plainzd2hashtablezd2mapz00zz__hashz00(obj_t BgL_tablez00_73,
		obj_t BgL_funz00_74)
	{
		{	/* Llib/hash.scm 490 */
			{	/* Llib/hash.scm 491 */
				obj_t BgL_bucketsz00_1815;

				BgL_bucketsz00_1815 = STRUCT_REF(BgL_tablez00_73, (int) (2L));
				{	/* Llib/hash.scm 492 */

					{
						long BgL_iz00_1819;
						obj_t BgL_resz00_1820;

						BgL_iz00_1819 = 0L;
						BgL_resz00_1820 = BNIL;
					BgL_zc3z04anonymousza31489ze3z87_1821:
						if ((BgL_iz00_1819 < VECTOR_LENGTH(((obj_t) BgL_bucketsz00_1815))))
							{	/* Llib/hash.scm 496 */
								obj_t BgL_g1067z00_1823;

								BgL_g1067z00_1823 =
									VECTOR_REF(((obj_t) BgL_bucketsz00_1815), BgL_iz00_1819);
								{
									obj_t BgL_lstz00_1825;
									obj_t BgL_resz00_1826;

									BgL_lstz00_1825 = BgL_g1067z00_1823;
									BgL_resz00_1826 = BgL_resz00_1820;
								BgL_zc3z04anonymousza31491ze3z87_1827:
									if (NULLP(BgL_lstz00_1825))
										{
											obj_t BgL_resz00_6671;
											long BgL_iz00_6669;

											BgL_iz00_6669 = (BgL_iz00_1819 + 1L);
											BgL_resz00_6671 = BgL_resz00_1826;
											BgL_resz00_1820 = BgL_resz00_6671;
											BgL_iz00_1819 = BgL_iz00_6669;
											goto BgL_zc3z04anonymousza31489ze3z87_1821;
										}
									else
										{	/* Llib/hash.scm 500 */
											obj_t BgL_cellz00_1830;

											BgL_cellz00_1830 = CAR(((obj_t) BgL_lstz00_1825));
											{	/* Llib/hash.scm 501 */
												obj_t BgL_arg1495z00_1831;
												obj_t BgL_arg1497z00_1832;

												BgL_arg1495z00_1831 = CDR(((obj_t) BgL_lstz00_1825));
												{	/* Llib/hash.scm 502 */
													obj_t BgL_arg1498z00_1833;

													{	/* Llib/hash.scm 502 */
														obj_t BgL_arg1499z00_1834;
														obj_t BgL_arg1500z00_1835;

														BgL_arg1499z00_1834 =
															CAR(((obj_t) BgL_cellz00_1830));
														BgL_arg1500z00_1835 =
															CDR(((obj_t) BgL_cellz00_1830));
														BgL_arg1498z00_1833 =
															BGL_PROCEDURE_CALL2(BgL_funz00_74,
															BgL_arg1499z00_1834, BgL_arg1500z00_1835);
													}
													BgL_arg1497z00_1832 =
														MAKE_YOUNG_PAIR(BgL_arg1498z00_1833,
														BgL_resz00_1826);
												}
												{
													obj_t BgL_resz00_6687;
													obj_t BgL_lstz00_6686;

													BgL_lstz00_6686 = BgL_arg1495z00_1831;
													BgL_resz00_6687 = BgL_arg1497z00_1832;
													BgL_resz00_1826 = BgL_resz00_6687;
													BgL_lstz00_1825 = BgL_lstz00_6686;
													goto BgL_zc3z04anonymousza31491ze3z87_1827;
												}
											}
										}
								}
							}
						else
							{	/* Llib/hash.scm 495 */
								return BgL_resz00_1820;
							}
					}
				}
			}
		}

	}



/* hashtable-for-each */
	BGL_EXPORTED_DEF obj_t BGl_hashtablezd2forzd2eachz00zz__hashz00(obj_t
		BgL_tablez00_75, obj_t BgL_funz00_76)
	{
		{	/* Llib/hash.scm 508 */
			{	/* Llib/hash.scm 510 */
				bool_t BgL_test2859z00_6688;

				if ((0L == ((long) CINT(STRUCT_REF(BgL_tablez00_75, (int) (5L))) & 8L)))
					{	/* Llib/hash.scm 294 */
						BgL_test2859z00_6688 = ((bool_t) 0);
					}
				else
					{	/* Llib/hash.scm 294 */
						BgL_test2859z00_6688 = ((bool_t) 1);
					}
				if (BgL_test2859z00_6688)
					{	/* Llib/hash.scm 510 */
						BGL_TAIL return
							BGl_openzd2stringzd2hashtablezd2forzd2eachz00zz__hashz00
							(BgL_tablez00_75, BgL_funz00_76);
					}
				else
					{	/* Llib/hash.scm 512 */
						bool_t BgL_test2861z00_6696;

						if (
							(0L ==
								((long) CINT(STRUCT_REF(BgL_tablez00_75, (int) (5L))) & 3L)))
							{	/* Llib/hash.scm 288 */
								BgL_test2861z00_6696 = ((bool_t) 0);
							}
						else
							{	/* Llib/hash.scm 288 */
								BgL_test2861z00_6696 = ((bool_t) 1);
							}
						if (BgL_test2861z00_6696)
							{	/* Llib/hash.scm 512 */
								BGL_TAIL return
									BGl_weakzd2hashtablezd2forzd2eachzd2zz__weakhashz00
									(BgL_tablez00_75, BgL_funz00_76);
							}
						else
							{	/* Llib/hash.scm 512 */
								return
									BBOOL(BGl_plainzd2hashtablezd2forzd2eachzd2zz__hashz00
									(BgL_tablez00_75, BgL_funz00_76));
							}
					}
			}
		}

	}



/* &hashtable-for-each */
	obj_t BGl_z62hashtablezd2forzd2eachz62zz__hashz00(obj_t BgL_envz00_5408,
		obj_t BgL_tablez00_5409, obj_t BgL_funz00_5410)
	{
		{	/* Llib/hash.scm 508 */
			{	/* Llib/hash.scm 510 */
				obj_t BgL_auxz00_6713;
				obj_t BgL_auxz00_6706;

				if (PROCEDUREP(BgL_funz00_5410))
					{	/* Llib/hash.scm 510 */
						BgL_auxz00_6713 = BgL_funz00_5410;
					}
				else
					{
						obj_t BgL_auxz00_6716;

						BgL_auxz00_6716 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(19823L), BGl_string2581z00zz__hashz00,
							BGl_string2579z00zz__hashz00, BgL_funz00_5410);
						FAILURE(BgL_auxz00_6716, BFALSE, BFALSE);
					}
				if (STRUCTP(BgL_tablez00_5409))
					{	/* Llib/hash.scm 510 */
						BgL_auxz00_6706 = BgL_tablez00_5409;
					}
				else
					{
						obj_t BgL_auxz00_6709;

						BgL_auxz00_6709 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(19823L), BGl_string2581z00zz__hashz00,
							BGl_string2572z00zz__hashz00, BgL_tablez00_5409);
						FAILURE(BgL_auxz00_6709, BFALSE, BFALSE);
					}
				return
					BGl_hashtablezd2forzd2eachz00zz__hashz00(BgL_auxz00_6706,
					BgL_auxz00_6713);
			}
		}

	}



/* open-string-hashtable-for-each */
	BGL_EXPORTED_DEF obj_t
		BGl_openzd2stringzd2hashtablezd2forzd2eachz00zz__hashz00(obj_t
		BgL_tablez00_77, obj_t BgL_funz00_78)
	{
		{	/* Llib/hash.scm 520 */
			{	/* Llib/hash.scm 521 */
				obj_t BgL_siza7eza7_1840;

				BgL_siza7eza7_1840 = STRUCT_REF(BgL_tablez00_77, (int) (1L));
				{	/* Llib/hash.scm 521 */
					long BgL_siza7e3za7_1841;

					BgL_siza7e3za7_1841 = (3L * (long) CINT(BgL_siza7eza7_1840));
					{	/* Llib/hash.scm 522 */
						obj_t BgL_bucketsz00_1842;

						BgL_bucketsz00_1842 = STRUCT_REF(BgL_tablez00_77, (int) (2L));
						{	/* Llib/hash.scm 523 */

							{
								long BgL_iz00_1844;

								{	/* Llib/hash.scm 524 */
									bool_t BgL_tmpz00_6727;

									BgL_iz00_1844 = 0L;
								BgL_zc3z04anonymousza31503ze3z87_1845:
									if ((BgL_iz00_1844 == BgL_siza7e3za7_1841))
										{	/* Llib/hash.scm 525 */
											BgL_tmpz00_6727 = ((bool_t) 0);
										}
									else
										{	/* Llib/hash.scm 525 */
											{	/* Llib/hash.scm 526 */
												bool_t BgL_test2866z00_6730;

												if (CBOOL(VECTOR_REF(
															((obj_t) BgL_bucketsz00_1842), BgL_iz00_1844)))
													{	/* Llib/hash.scm 526 */
														long BgL_arg1511z00_1854;

														BgL_arg1511z00_1854 = (BgL_iz00_1844 + 2L);
														BgL_test2866z00_6730 =
															CBOOL(VECTOR_REF(
																((obj_t) BgL_bucketsz00_1842),
																BgL_arg1511z00_1854));
													}
												else
													{	/* Llib/hash.scm 526 */
														BgL_test2866z00_6730 = ((bool_t) 0);
													}
												if (BgL_test2866z00_6730)
													{	/* Llib/hash.scm 527 */
														obj_t BgL_arg1508z00_1850;
														obj_t BgL_arg1509z00_1851;

														BgL_arg1508z00_1850 =
															VECTOR_REF(
															((obj_t) BgL_bucketsz00_1842), BgL_iz00_1844);
														{	/* Llib/hash.scm 527 */
															long BgL_arg1510z00_1852;

															BgL_arg1510z00_1852 = (BgL_iz00_1844 + 1L);
															BgL_arg1509z00_1851 =
																VECTOR_REF(
																((obj_t) BgL_bucketsz00_1842),
																BgL_arg1510z00_1852);
														}
														BGL_PROCEDURE_CALL2(BgL_funz00_78,
															BgL_arg1508z00_1850, BgL_arg1509z00_1851);
													}
												else
													{	/* Llib/hash.scm 526 */
														BFALSE;
													}
											}
											{
												long BgL_iz00_6749;

												BgL_iz00_6749 = (BgL_iz00_1844 + 3L);
												BgL_iz00_1844 = BgL_iz00_6749;
												goto BgL_zc3z04anonymousza31503ze3z87_1845;
											}
										}
									return BBOOL(BgL_tmpz00_6727);
								}
							}
						}
					}
				}
			}
		}

	}



/* &open-string-hashtable-for-each */
	obj_t BGl_z62openzd2stringzd2hashtablezd2forzd2eachz62zz__hashz00(obj_t
		BgL_envz00_5411, obj_t BgL_tablez00_5412, obj_t BgL_funz00_5413)
	{
		{	/* Llib/hash.scm 520 */
			{	/* Llib/hash.scm 521 */
				obj_t BgL_auxz00_6759;
				obj_t BgL_auxz00_6752;

				if (PROCEDUREP(BgL_funz00_5413))
					{	/* Llib/hash.scm 521 */
						BgL_auxz00_6759 = BgL_funz00_5413;
					}
				else
					{
						obj_t BgL_auxz00_6762;

						BgL_auxz00_6762 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(20325L), BGl_string2582z00zz__hashz00,
							BGl_string2579z00zz__hashz00, BgL_funz00_5413);
						FAILURE(BgL_auxz00_6762, BFALSE, BFALSE);
					}
				if (STRUCTP(BgL_tablez00_5412))
					{	/* Llib/hash.scm 521 */
						BgL_auxz00_6752 = BgL_tablez00_5412;
					}
				else
					{
						obj_t BgL_auxz00_6755;

						BgL_auxz00_6755 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(20325L), BGl_string2582z00zz__hashz00,
							BGl_string2572z00zz__hashz00, BgL_tablez00_5412);
						FAILURE(BgL_auxz00_6755, BFALSE, BFALSE);
					}
				return
					BGl_openzd2stringzd2hashtablezd2forzd2eachz00zz__hashz00
					(BgL_auxz00_6752, BgL_auxz00_6759);
			}
		}

	}



/* plain-hashtable-for-each */
	bool_t BGl_plainzd2hashtablezd2forzd2eachzd2zz__hashz00(obj_t BgL_tablez00_79,
		obj_t BgL_funz00_80)
	{
		{	/* Llib/hash.scm 533 */
			{	/* Llib/hash.scm 534 */
				obj_t BgL_bucketsz00_1857;

				BgL_bucketsz00_1857 = STRUCT_REF(BgL_tablez00_79, (int) (2L));
				{	/* Llib/hash.scm 535 */

					{
						long BgL_iz00_1860;

						BgL_iz00_1860 = 0L;
					BgL_zc3z04anonymousza31514ze3z87_1861:
						if ((BgL_iz00_1860 < VECTOR_LENGTH(((obj_t) BgL_bucketsz00_1857))))
							{	/* Llib/hash.scm 537 */
								{	/* Llib/hash.scm 539 */
									obj_t BgL_g1137z00_1863;

									BgL_g1137z00_1863 =
										VECTOR_REF(((obj_t) BgL_bucketsz00_1857), BgL_iz00_1860);
									{
										obj_t BgL_l1135z00_1865;

										BgL_l1135z00_1865 = BgL_g1137z00_1863;
									BgL_zc3z04anonymousza31516ze3z87_1866:
										if (PAIRP(BgL_l1135z00_1865))
											{	/* Llib/hash.scm 541 */
												{	/* Llib/hash.scm 540 */
													obj_t BgL_cellz00_1868;

													BgL_cellz00_1868 = CAR(BgL_l1135z00_1865);
													{	/* Llib/hash.scm 540 */
														obj_t BgL_arg1521z00_1869;
														obj_t BgL_arg1522z00_1870;

														BgL_arg1521z00_1869 =
															CAR(((obj_t) BgL_cellz00_1868));
														BgL_arg1522z00_1870 =
															CDR(((obj_t) BgL_cellz00_1868));
														BGL_PROCEDURE_CALL2(BgL_funz00_80,
															BgL_arg1521z00_1869, BgL_arg1522z00_1870);
													}
												}
												{
													obj_t BgL_l1135z00_6787;

													BgL_l1135z00_6787 = CDR(BgL_l1135z00_1865);
													BgL_l1135z00_1865 = BgL_l1135z00_6787;
													goto BgL_zc3z04anonymousza31516ze3z87_1866;
												}
											}
										else
											{	/* Llib/hash.scm 541 */
												((bool_t) 1);
											}
									}
								}
								{
									long BgL_iz00_6789;

									BgL_iz00_6789 = (BgL_iz00_1860 + 1L);
									BgL_iz00_1860 = BgL_iz00_6789;
									goto BgL_zc3z04anonymousza31514ze3z87_1861;
								}
							}
						else
							{	/* Llib/hash.scm 537 */
								return ((bool_t) 0);
							}
					}
				}
			}
		}

	}



/* hashtable-filter! */
	BGL_EXPORTED_DEF obj_t BGl_hashtablezd2filterz12zc0zz__hashz00(obj_t
		BgL_tablez00_81, obj_t BgL_funz00_82)
	{
		{	/* Llib/hash.scm 547 */
			{	/* Llib/hash.scm 549 */
				bool_t BgL_test2872z00_6791;

				if ((0L == ((long) CINT(STRUCT_REF(BgL_tablez00_81, (int) (5L))) & 8L)))
					{	/* Llib/hash.scm 294 */
						BgL_test2872z00_6791 = ((bool_t) 0);
					}
				else
					{	/* Llib/hash.scm 294 */
						BgL_test2872z00_6791 = ((bool_t) 1);
					}
				if (BgL_test2872z00_6791)
					{	/* Llib/hash.scm 549 */
						BGL_TAIL return
							BGl_openzd2stringzd2hashtablezd2filterz12zc0zz__hashz00
							(BgL_tablez00_81, BgL_funz00_82);
					}
				else
					{	/* Llib/hash.scm 551 */
						bool_t BgL_test2874z00_6799;

						if (
							(0L ==
								((long) CINT(STRUCT_REF(BgL_tablez00_81, (int) (5L))) & 3L)))
							{	/* Llib/hash.scm 288 */
								BgL_test2874z00_6799 = ((bool_t) 0);
							}
						else
							{	/* Llib/hash.scm 288 */
								BgL_test2874z00_6799 = ((bool_t) 1);
							}
						if (BgL_test2874z00_6799)
							{	/* Llib/hash.scm 551 */
								BGL_TAIL return
									BGl_weakzd2hashtablezd2filterz12z12zz__weakhashz00
									(BgL_tablez00_81, BgL_funz00_82);
							}
						else
							{	/* Llib/hash.scm 551 */
								BGL_TAIL return
									BGl_plainzd2hashtablezd2filterz12z12zz__hashz00
									(BgL_tablez00_81, BgL_funz00_82);
							}
					}
			}
		}

	}



/* &hashtable-filter! */
	obj_t BGl_z62hashtablezd2filterz12za2zz__hashz00(obj_t BgL_envz00_5414,
		obj_t BgL_tablez00_5415, obj_t BgL_funz00_5416)
	{
		{	/* Llib/hash.scm 547 */
			{	/* Llib/hash.scm 549 */
				obj_t BgL_auxz00_6815;
				obj_t BgL_auxz00_6808;

				if (PROCEDUREP(BgL_funz00_5416))
					{	/* Llib/hash.scm 549 */
						BgL_auxz00_6815 = BgL_funz00_5416;
					}
				else
					{
						obj_t BgL_auxz00_6818;

						BgL_auxz00_6818 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(21506L), BGl_string2583z00zz__hashz00,
							BGl_string2579z00zz__hashz00, BgL_funz00_5416);
						FAILURE(BgL_auxz00_6818, BFALSE, BFALSE);
					}
				if (STRUCTP(BgL_tablez00_5415))
					{	/* Llib/hash.scm 549 */
						BgL_auxz00_6808 = BgL_tablez00_5415;
					}
				else
					{
						obj_t BgL_auxz00_6811;

						BgL_auxz00_6811 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(21506L), BGl_string2583z00zz__hashz00,
							BGl_string2572z00zz__hashz00, BgL_tablez00_5415);
						FAILURE(BgL_auxz00_6811, BFALSE, BFALSE);
					}
				return
					BGl_hashtablezd2filterz12zc0zz__hashz00(BgL_auxz00_6808,
					BgL_auxz00_6815);
			}
		}

	}



/* open-string-hashtable-filter! */
	BGL_EXPORTED_DEF obj_t
		BGl_openzd2stringzd2hashtablezd2filterz12zc0zz__hashz00(obj_t
		BgL_tablez00_83, obj_t BgL_funz00_84)
	{
		{	/* Llib/hash.scm 559 */
			{	/* Llib/hash.scm 560 */
				obj_t BgL_siza7eza7_1877;

				BgL_siza7eza7_1877 = STRUCT_REF(BgL_tablez00_83, (int) (1L));
				{	/* Llib/hash.scm 560 */
					long BgL_siza7e3za7_1878;

					BgL_siza7e3za7_1878 = (3L * (long) CINT(BgL_siza7eza7_1877));
					{	/* Llib/hash.scm 561 */
						obj_t BgL_bucketsz00_1879;

						BgL_bucketsz00_1879 = STRUCT_REF(BgL_tablez00_83, (int) (2L));
						{	/* Llib/hash.scm 562 */

							{
								long BgL_iz00_1881;

								{	/* Llib/hash.scm 563 */
									bool_t BgL_tmpz00_6829;

									BgL_iz00_1881 = 0L;
								BgL_zc3z04anonymousza31527ze3z87_1882:
									if ((BgL_iz00_1881 == BgL_siza7e3za7_1878))
										{	/* Llib/hash.scm 564 */
											BgL_tmpz00_6829 = ((bool_t) 0);
										}
									else
										{	/* Llib/hash.scm 564 */
											{	/* Llib/hash.scm 565 */
												bool_t BgL_test2879z00_6832;

												if (CBOOL(VECTOR_REF(
															((obj_t) BgL_bucketsz00_1879), BgL_iz00_1881)))
													{	/* Llib/hash.scm 565 */
														long BgL_arg1547z00_1897;

														BgL_arg1547z00_1897 = (BgL_iz00_1881 + 2L);
														BgL_test2879z00_6832 =
															CBOOL(VECTOR_REF(
																((obj_t) BgL_bucketsz00_1879),
																BgL_arg1547z00_1897));
													}
												else
													{	/* Llib/hash.scm 565 */
														BgL_test2879z00_6832 = ((bool_t) 0);
													}
												if (BgL_test2879z00_6832)
													{	/* Llib/hash.scm 566 */
														bool_t BgL_test2881z00_6841;

														{	/* Llib/hash.scm 566 */
															obj_t BgL_arg1543z00_1893;
															obj_t BgL_arg1544z00_1894;

															BgL_arg1543z00_1893 =
																VECTOR_REF(
																((obj_t) BgL_bucketsz00_1879), BgL_iz00_1881);
															{	/* Llib/hash.scm 566 */
																long BgL_arg1546z00_1895;

																BgL_arg1546z00_1895 = (BgL_iz00_1881 + 1L);
																BgL_arg1544z00_1894 =
																	VECTOR_REF(
																	((obj_t) BgL_bucketsz00_1879),
																	BgL_arg1546z00_1895);
															}
															BgL_test2881z00_6841 =
																CBOOL(BGL_PROCEDURE_CALL2(BgL_funz00_84,
																	BgL_arg1543z00_1893, BgL_arg1544z00_1894));
														}
														if (BgL_test2881z00_6841)
															{	/* Llib/hash.scm 566 */
																BFALSE;
															}
														else
															{	/* Llib/hash.scm 566 */
																{	/* Llib/hash.scm 567 */
																	long BgL_arg1539z00_1891;

																	BgL_arg1539z00_1891 = (BgL_iz00_1881 + 1L);
																	VECTOR_SET(
																		((obj_t) BgL_bucketsz00_1879),
																		BgL_arg1539z00_1891, BFALSE);
																}
																{	/* Llib/hash.scm 568 */
																	long BgL_arg1540z00_1892;

																	BgL_arg1540z00_1892 = (BgL_iz00_1881 + 2L);
																	VECTOR_SET(
																		((obj_t) BgL_bucketsz00_1879),
																		BgL_arg1540z00_1892, BFALSE);
																}
																{	/* Llib/hash.scm 1303 */
																	long BgL_arg1978z00_3723;

																	BgL_arg1978z00_3723 =
																		(
																		(long) CINT(STRUCT_REF(BgL_tablez00_83,
																				(int) (6L))) + 1L);
																	{	/* Llib/hash.scm 1273 */
																		obj_t BgL_auxz00_6865;
																		int BgL_tmpz00_6863;

																		BgL_auxz00_6865 = BINT(BgL_arg1978z00_3723);
																		BgL_tmpz00_6863 = (int) (6L);
																		STRUCT_SET(BgL_tablez00_83, BgL_tmpz00_6863,
																			BgL_auxz00_6865);
													}}}}
												else
													{	/* Llib/hash.scm 565 */
														BFALSE;
													}
											}
											{
												long BgL_iz00_6868;

												BgL_iz00_6868 = (BgL_iz00_1881 + 3L);
												BgL_iz00_1881 = BgL_iz00_6868;
												goto BgL_zc3z04anonymousza31527ze3z87_1882;
											}
										}
									return BBOOL(BgL_tmpz00_6829);
								}
							}
						}
					}
				}
			}
		}

	}



/* &open-string-hashtable-filter! */
	obj_t BGl_z62openzd2stringzd2hashtablezd2filterz12za2zz__hashz00(obj_t
		BgL_envz00_5417, obj_t BgL_tablez00_5418, obj_t BgL_funz00_5419)
	{
		{	/* Llib/hash.scm 559 */
			{	/* Llib/hash.scm 560 */
				obj_t BgL_auxz00_6878;
				obj_t BgL_auxz00_6871;

				if (PROCEDUREP(BgL_funz00_5419))
					{	/* Llib/hash.scm 560 */
						BgL_auxz00_6878 = BgL_funz00_5419;
					}
				else
					{
						obj_t BgL_auxz00_6881;

						BgL_auxz00_6881 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(22004L), BGl_string2584z00zz__hashz00,
							BGl_string2579z00zz__hashz00, BgL_funz00_5419);
						FAILURE(BgL_auxz00_6881, BFALSE, BFALSE);
					}
				if (STRUCTP(BgL_tablez00_5418))
					{	/* Llib/hash.scm 560 */
						BgL_auxz00_6871 = BgL_tablez00_5418;
					}
				else
					{
						obj_t BgL_auxz00_6874;

						BgL_auxz00_6874 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(22004L), BGl_string2584z00zz__hashz00,
							BGl_string2572z00zz__hashz00, BgL_tablez00_5418);
						FAILURE(BgL_auxz00_6874, BFALSE, BFALSE);
					}
				return
					BGl_openzd2stringzd2hashtablezd2filterz12zc0zz__hashz00
					(BgL_auxz00_6871, BgL_auxz00_6878);
			}
		}

	}



/* plain-hashtable-filter! */
	obj_t BGl_plainzd2hashtablezd2filterz12z12zz__hashz00(obj_t BgL_tablez00_85,
		obj_t BgL_funz00_86)
	{
		{	/* Llib/hash.scm 575 */
			{	/* Llib/hash.scm 576 */
				obj_t BgL_bucketsz00_1900;

				BgL_bucketsz00_1900 = STRUCT_REF(BgL_tablez00_85, (int) (2L));
				{	/* Llib/hash.scm 577 */

					{
						long BgL_iz00_1903;
						long BgL_deltaz00_1904;

						BgL_iz00_1903 = 0L;
						BgL_deltaz00_1904 = 0L;
					BgL_zc3z04anonymousza31550ze3z87_1905:
						if ((BgL_iz00_1903 < VECTOR_LENGTH(((obj_t) BgL_bucketsz00_1900))))
							{	/* Llib/hash.scm 580 */
								obj_t BgL_lz00_1907;

								BgL_lz00_1907 =
									VECTOR_REF(((obj_t) BgL_bucketsz00_1900), BgL_iz00_1903);
								{	/* Llib/hash.scm 580 */
									long BgL_oldzd2lenzd2_1908;

									BgL_oldzd2lenzd2_1908 = bgl_list_length(BgL_lz00_1907);
									{	/* Llib/hash.scm 581 */
										obj_t BgL_newlz00_1909;

										{	/* Llib/hash.scm 583 */
											obj_t BgL_zc3z04anonymousza31556ze3z87_5420;

											BgL_zc3z04anonymousza31556ze3z87_5420 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31556ze3ze5zz__hashz00,
												(int) (1L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31556ze3z87_5420,
												(int) (0L), BgL_funz00_86);
											BgL_newlz00_1909 =
												BGl_filterz12z12zz__r4_control_features_6_9z00
												(BgL_zc3z04anonymousza31556ze3z87_5420, BgL_lz00_1907);
										}
										{	/* Llib/hash.scm 582 */
											long BgL_newzd2lenzd2_1910;

											BgL_newzd2lenzd2_1910 = bgl_list_length(BgL_newlz00_1909);
											{	/* Llib/hash.scm 585 */

												VECTOR_SET(
													((obj_t) BgL_bucketsz00_1900), BgL_iz00_1903,
													BgL_newlz00_1909);
												{
													long BgL_deltaz00_6906;
													long BgL_iz00_6904;

													BgL_iz00_6904 = (BgL_iz00_1903 + 1L);
													BgL_deltaz00_6906 =
														(BgL_deltaz00_1904 +
														(BgL_newzd2lenzd2_1910 - BgL_oldzd2lenzd2_1908));
													BgL_deltaz00_1904 = BgL_deltaz00_6906;
													BgL_iz00_1903 = BgL_iz00_6904;
													goto BgL_zc3z04anonymousza31550ze3z87_1905;
												}
											}
										}
									}
								}
							}
						else
							{	/* Llib/hash.scm 589 */
								long BgL_arg1559z00_1920;

								BgL_arg1559z00_1920 =
									(BgL_deltaz00_1904 +
									(long) CINT(STRUCT_REF(BgL_tablez00_85, (int) (0L))));
								{	/* Llib/hash.scm 588 */
									obj_t BgL_xz00_5552;

									{	/* Llib/hash.scm 588 */
										obj_t BgL_auxz00_6915;
										int BgL_tmpz00_6913;

										BgL_auxz00_6915 = BINT(BgL_arg1559z00_1920);
										BgL_tmpz00_6913 = (int) (0L);
										BgL_xz00_5552 =
											STRUCT_SET(BgL_tablez00_85, BgL_tmpz00_6913,
											BgL_auxz00_6915);
									}
									return BUNSPEC;
								}
							}
					}
				}
			}
		}

	}



/* &<@anonymous:1556> */
	obj_t BGl_z62zc3z04anonymousza31556ze3ze5zz__hashz00(obj_t BgL_envz00_5421,
		obj_t BgL_cellz00_5423)
	{
		{	/* Llib/hash.scm 582 */
			{	/* Llib/hash.scm 583 */
				obj_t BgL_funz00_5422;

				BgL_funz00_5422 = ((obj_t) PROCEDURE_REF(BgL_envz00_5421, (int) (0L)));
				{	/* Llib/hash.scm 583 */
					obj_t BgL_arg1557z00_5649;
					obj_t BgL_arg1558z00_5650;

					BgL_arg1557z00_5649 = CAR(((obj_t) BgL_cellz00_5423));
					BgL_arg1558z00_5650 = CDR(((obj_t) BgL_cellz00_5423));
					return
						BGL_PROCEDURE_CALL2(BgL_funz00_5422, BgL_arg1557z00_5649,
						BgL_arg1558z00_5650);
				}
			}
		}

	}



/* hashtable-clear! */
	BGL_EXPORTED_DEF obj_t BGl_hashtablezd2clearz12zc0zz__hashz00(obj_t
		BgL_tablez00_87)
	{
		{	/* Llib/hash.scm 594 */
			{	/* Llib/hash.scm 596 */
				bool_t BgL_test2885z00_6930;

				if ((0L == ((long) CINT(STRUCT_REF(BgL_tablez00_87, (int) (5L))) & 8L)))
					{	/* Llib/hash.scm 294 */
						BgL_test2885z00_6930 = ((bool_t) 0);
					}
				else
					{	/* Llib/hash.scm 294 */
						BgL_test2885z00_6930 = ((bool_t) 1);
					}
				if (BgL_test2885z00_6930)
					{	/* Llib/hash.scm 596 */
						BGL_TAIL return
							BGl_openzd2stringzd2hashtablezd2clearz12zc0zz__hashz00
							(BgL_tablez00_87);
					}
				else
					{	/* Llib/hash.scm 598 */
						bool_t BgL_test2887z00_6938;

						if (
							(0L ==
								((long) CINT(STRUCT_REF(BgL_tablez00_87, (int) (5L))) & 3L)))
							{	/* Llib/hash.scm 288 */
								BgL_test2887z00_6938 = ((bool_t) 0);
							}
						else
							{	/* Llib/hash.scm 288 */
								BgL_test2887z00_6938 = ((bool_t) 1);
							}
						if (BgL_test2887z00_6938)
							{	/* Llib/hash.scm 598 */
								BGL_TAIL return
									BGl_weakzd2hashtablezd2clearz12z12zz__weakhashz00
									(BgL_tablez00_87);
							}
						else
							{	/* Llib/hash.scm 598 */
								BGL_TAIL return
									BGl_plainzd2hashtablezd2clearz12z12zz__hashz00
									(BgL_tablez00_87);
							}
					}
			}
		}

	}



/* &hashtable-clear! */
	obj_t BGl_z62hashtablezd2clearz12za2zz__hashz00(obj_t BgL_envz00_5424,
		obj_t BgL_tablez00_5425)
	{
		{	/* Llib/hash.scm 594 */
			{	/* Llib/hash.scm 596 */
				obj_t BgL_auxz00_6947;

				if (STRUCTP(BgL_tablez00_5425))
					{	/* Llib/hash.scm 596 */
						BgL_auxz00_6947 = BgL_tablez00_5425;
					}
				else
					{
						obj_t BgL_auxz00_6950;

						BgL_auxz00_6950 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(23584L), BGl_string2585z00zz__hashz00,
							BGl_string2572z00zz__hashz00, BgL_tablez00_5425);
						FAILURE(BgL_auxz00_6950, BFALSE, BFALSE);
					}
				return BGl_hashtablezd2clearz12zc0zz__hashz00(BgL_auxz00_6947);
			}
		}

	}



/* open-string-hashtable-clear! */
	obj_t BGl_openzd2stringzd2hashtablezd2clearz12zc0zz__hashz00(obj_t
		BgL_tablez00_88)
	{
		{	/* Llib/hash.scm 606 */
			{	/* Llib/hash.scm 607 */
				obj_t BgL_arg1564z00_1925;

				BgL_arg1564z00_1925 = STRUCT_REF(BgL_tablez00_88, (int) (2L));
				{	/* Ieee/vector.scm 105 */

					BGl_vectorzd2fillz12zc0zz__r4_vectors_6_8z00(BgL_arg1564z00_1925,
						BFALSE, 0L, VECTOR_LENGTH(((obj_t) BgL_arg1564z00_1925)));
			}}
			{	/* Llib/hash.scm 1273 */
				obj_t BgL_auxz00_6962;
				int BgL_tmpz00_6960;

				BgL_auxz00_6962 = BINT(0L);
				BgL_tmpz00_6960 = (int) (6L);
				STRUCT_SET(BgL_tablez00_88, BgL_tmpz00_6960, BgL_auxz00_6962);
			}
			{	/* Llib/hash.scm 609 */
				obj_t BgL_xz00_5555;

				{	/* Llib/hash.scm 609 */
					obj_t BgL_auxz00_6967;
					int BgL_tmpz00_6965;

					BgL_auxz00_6967 = BINT(0L);
					BgL_tmpz00_6965 = (int) (0L);
					BgL_xz00_5555 =
						STRUCT_SET(BgL_tablez00_88, BgL_tmpz00_6965, BgL_auxz00_6967);
				}
				return BUNSPEC;
			}
		}

	}



/* plain-hashtable-clear! */
	obj_t BGl_plainzd2hashtablezd2clearz12z12zz__hashz00(obj_t BgL_tablez00_89)
	{
		{	/* Llib/hash.scm 614 */
			{	/* Llib/hash.scm 615 */
				obj_t BgL_bucketsz00_1930;

				BgL_bucketsz00_1930 = STRUCT_REF(BgL_tablez00_89, (int) (2L));
				{	/* Llib/hash.scm 616 */

					{
						long BgL_iz00_3779;

						BgL_iz00_3779 = 0L;
					BgL_loopz00_3778:
						if ((BgL_iz00_3779 < VECTOR_LENGTH(((obj_t) BgL_bucketsz00_1930))))
							{	/* Llib/hash.scm 618 */
								VECTOR_SET(((obj_t) BgL_bucketsz00_1930), BgL_iz00_3779, BNIL);
								{
									long BgL_iz00_6978;

									BgL_iz00_6978 = (BgL_iz00_3779 + 1L);
									BgL_iz00_3779 = BgL_iz00_6978;
									goto BgL_loopz00_3778;
								}
							}
						else
							{	/* Llib/hash.scm 622 */
								obj_t BgL_xz00_5556;

								{	/* Llib/hash.scm 622 */
									obj_t BgL_auxz00_6982;
									int BgL_tmpz00_6980;

									BgL_auxz00_6982 = BINT(0L);
									BgL_tmpz00_6980 = (int) (0L);
									BgL_xz00_5556 =
										STRUCT_SET(BgL_tablez00_89, BgL_tmpz00_6980,
										BgL_auxz00_6982);
								}
								return BUNSPEC;
							}
					}
				}
			}
		}

	}



/* hashtable-contains? */
	BGL_EXPORTED_DEF bool_t BGl_hashtablezd2containszf3z21zz__hashz00(obj_t
		BgL_tablez00_90, obj_t BgL_keyz00_91)
	{
		{	/* Llib/hash.scm 627 */
			{	/* Llib/hash.scm 629 */
				bool_t BgL_test2891z00_6985;

				if ((0L == ((long) CINT(STRUCT_REF(BgL_tablez00_90, (int) (5L))) & 8L)))
					{	/* Llib/hash.scm 294 */
						BgL_test2891z00_6985 = ((bool_t) 0);
					}
				else
					{	/* Llib/hash.scm 294 */
						BgL_test2891z00_6985 = ((bool_t) 1);
					}
				if (BgL_test2891z00_6985)
					{	/* Llib/hash.scm 629 */
						return
							CBOOL(BGl_openzd2stringzd2hashtablezd2containszf3z21zz__hashz00
							(BgL_tablez00_90, BgL_keyz00_91));
					}
				else
					{	/* Llib/hash.scm 631 */
						bool_t BgL_test2893z00_6994;

						if (
							(0L ==
								((long) CINT(STRUCT_REF(BgL_tablez00_90, (int) (5L))) & 3L)))
							{	/* Llib/hash.scm 288 */
								BgL_test2893z00_6994 = ((bool_t) 0);
							}
						else
							{	/* Llib/hash.scm 288 */
								BgL_test2893z00_6994 = ((bool_t) 1);
							}
						if (BgL_test2893z00_6994)
							{	/* Llib/hash.scm 631 */
								BGL_TAIL return
									BGl_weakzd2hashtablezd2containszf3zf3zz__weakhashz00
									(BgL_tablez00_90, BgL_keyz00_91);
							}
						else
							{	/* Llib/hash.scm 631 */
								BGL_TAIL return
									BGl_plainzd2hashtablezd2containszf3zf3zz__hashz00
									(BgL_tablez00_90, BgL_keyz00_91);
							}
					}
			}
		}

	}



/* &hashtable-contains? */
	obj_t BGl_z62hashtablezd2containszf3z43zz__hashz00(obj_t BgL_envz00_5426,
		obj_t BgL_tablez00_5427, obj_t BgL_keyz00_5428)
	{
		{	/* Llib/hash.scm 627 */
			{	/* Llib/hash.scm 629 */
				bool_t BgL_tmpz00_7003;

				{	/* Llib/hash.scm 629 */
					obj_t BgL_auxz00_7004;

					if (STRUCTP(BgL_tablez00_5427))
						{	/* Llib/hash.scm 629 */
							BgL_auxz00_7004 = BgL_tablez00_5427;
						}
					else
						{
							obj_t BgL_auxz00_7007;

							BgL_auxz00_7007 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
								BINT(24991L), BGl_string2586z00zz__hashz00,
								BGl_string2572z00zz__hashz00, BgL_tablez00_5427);
							FAILURE(BgL_auxz00_7007, BFALSE, BFALSE);
						}
					BgL_tmpz00_7003 =
						BGl_hashtablezd2containszf3z21zz__hashz00(BgL_auxz00_7004,
						BgL_keyz00_5428);
				}
				return BBOOL(BgL_tmpz00_7003);
			}
		}

	}



/* open-string-hashtable-contains? */
	BGL_EXPORTED_DEF obj_t
		BGl_openzd2stringzd2hashtablezd2containszf3z21zz__hashz00(obj_t BgL_tz00_92,
		obj_t BgL_keyz00_93)
	{
		{	/* Llib/hash.scm 639 */
			{	/* Llib/hash.scm 640 */
				obj_t BgL_siza7eza7_1940;

				BgL_siza7eza7_1940 = STRUCT_REF(BgL_tz00_92, (int) (1L));
				{	/* Llib/hash.scm 640 */
					obj_t BgL_bucketsz00_1941;

					BgL_bucketsz00_1941 = STRUCT_REF(BgL_tz00_92, (int) (2L));
					{	/* Llib/hash.scm 641 */
						long BgL_hashz00_1942;

						{	/* Llib/hash.scm 642 */
							long BgL_arg1587z00_1964;

							BgL_arg1587z00_1964 = STRING_LENGTH(BgL_keyz00_93);
							BgL_hashz00_1942 =
								bgl_string_hash(BSTRING_TO_STRING(BgL_keyz00_93),
								(int) (0L), (int) (BgL_arg1587z00_1964));
						}
						{	/* Llib/hash.scm 642 */

							{	/* Llib/hash.scm 644 */
								long BgL_g1068z00_1943;

								{	/* Llib/hash.scm 644 */
									long BgL_n1z00_3807;
									long BgL_n2z00_3808;

									BgL_n1z00_3807 = BgL_hashz00_1942;
									BgL_n2z00_3808 = (long) CINT(BgL_siza7eza7_1940);
									{	/* Llib/hash.scm 644 */
										bool_t BgL_test2896z00_7023;

										{	/* Llib/hash.scm 644 */
											long BgL_arg2151z00_3810;

											BgL_arg2151z00_3810 =
												(((BgL_n1z00_3807) | (BgL_n2z00_3808)) & -2147483648);
											BgL_test2896z00_7023 = (BgL_arg2151z00_3810 == 0L);
										}
										if (BgL_test2896z00_7023)
											{	/* Llib/hash.scm 644 */
												int32_t BgL_arg2148z00_3811;

												{	/* Llib/hash.scm 644 */
													int32_t BgL_arg2149z00_3812;
													int32_t BgL_arg2150z00_3813;

													BgL_arg2149z00_3812 = (int32_t) (BgL_n1z00_3807);
													BgL_arg2150z00_3813 = (int32_t) (BgL_n2z00_3808);
													BgL_arg2148z00_3811 =
														(BgL_arg2149z00_3812 % BgL_arg2150z00_3813);
												}
												{	/* Llib/hash.scm 644 */
													long BgL_arg2252z00_3818;

													BgL_arg2252z00_3818 = (long) (BgL_arg2148z00_3811);
													BgL_g1068z00_1943 = (long) (BgL_arg2252z00_3818);
											}}
										else
											{	/* Llib/hash.scm 644 */
												BgL_g1068z00_1943 = (BgL_n1z00_3807 % BgL_n2z00_3808);
											}
									}
								}
								{
									long BgL_offz00_1945;
									long BgL_iz00_1946;

									BgL_offz00_1945 = BgL_g1068z00_1943;
									BgL_iz00_1946 = 1L;
								BgL_zc3z04anonymousza31570ze3z87_1947:
									{	/* Llib/hash.scm 646 */
										long BgL_off3z00_1948;

										BgL_off3z00_1948 = (BgL_offz00_1945 * 3L);
										if (CBOOL(VECTOR_REF(
													((obj_t) BgL_bucketsz00_1941), BgL_off3z00_1948)))
											{	/* Llib/hash.scm 648 */
												bool_t BgL_test2898z00_7037;

												{	/* Llib/hash.scm 648 */
													obj_t BgL_arg1586z00_1962;

													BgL_arg1586z00_1962 =
														VECTOR_REF(
														((obj_t) BgL_bucketsz00_1941), BgL_off3z00_1948);
													{	/* Llib/hash.scm 648 */
														long BgL_l1z00_3827;

														BgL_l1z00_3827 =
															STRING_LENGTH(((obj_t) BgL_arg1586z00_1962));
														if (
															(BgL_l1z00_3827 == STRING_LENGTH(BgL_keyz00_93)))
															{	/* Llib/hash.scm 648 */
																int BgL_arg2041z00_3830;

																{	/* Llib/hash.scm 648 */
																	char *BgL_auxz00_7048;
																	char *BgL_tmpz00_7045;

																	BgL_auxz00_7048 =
																		BSTRING_TO_STRING(BgL_keyz00_93);
																	BgL_tmpz00_7045 =
																		BSTRING_TO_STRING(
																		((obj_t) BgL_arg1586z00_1962));
																	BgL_arg2041z00_3830 =
																		memcmp(BgL_tmpz00_7045, BgL_auxz00_7048,
																		BgL_l1z00_3827);
																}
																BgL_test2898z00_7037 =
																	((long) (BgL_arg2041z00_3830) == 0L);
															}
														else
															{	/* Llib/hash.scm 648 */
																BgL_test2898z00_7037 = ((bool_t) 0);
															}
													}
												}
												if (BgL_test2898z00_7037)
													{	/* Llib/hash.scm 649 */
														bool_t BgL_test2900z00_7053;

														{	/* Llib/hash.scm 649 */
															long BgL_arg1579z00_1955;

															BgL_arg1579z00_1955 = (BgL_off3z00_1948 + 1L);
															BgL_test2900z00_7053 =
																CBOOL(VECTOR_REF(
																	((obj_t) BgL_bucketsz00_1941),
																	BgL_arg1579z00_1955));
														}
														if (BgL_test2900z00_7053)
															{	/* Llib/hash.scm 650 */
																long BgL_arg1578z00_1954;

																BgL_arg1578z00_1954 = (BgL_off3z00_1948 + 1L);
																return
																	VECTOR_REF(
																	((obj_t) BgL_bucketsz00_1941),
																	BgL_arg1578z00_1954);
															}
														else
															{	/* Llib/hash.scm 649 */
																return BFALSE;
															}
													}
												else
													{	/* Llib/hash.scm 651 */
														long BgL_noffz00_1956;

														BgL_noffz00_1956 =
															(BgL_offz00_1945 +
															(BgL_iz00_1946 * BgL_iz00_1946));
														if (
															(BgL_noffz00_1956 >=
																(long) CINT(BgL_siza7eza7_1940)))
															{	/* Llib/hash.scm 653 */
																long BgL_arg1582z00_1958;
																long BgL_arg1583z00_1959;

																{	/* Llib/hash.scm 653 */
																	long BgL_n1z00_3848;
																	long BgL_n2z00_3849;

																	BgL_n1z00_3848 = BgL_noffz00_1956;
																	BgL_n2z00_3849 =
																		(long) CINT(BgL_siza7eza7_1940);
																	{	/* Llib/hash.scm 653 */
																		bool_t BgL_test2902z00_7067;

																		{	/* Llib/hash.scm 653 */
																			long BgL_arg2151z00_3851;

																			BgL_arg2151z00_3851 =
																				(((BgL_n1z00_3848) | (BgL_n2z00_3849)) &
																				-2147483648);
																			BgL_test2902z00_7067 =
																				(BgL_arg2151z00_3851 == 0L);
																		}
																		if (BgL_test2902z00_7067)
																			{	/* Llib/hash.scm 653 */
																				int32_t BgL_arg2148z00_3852;

																				{	/* Llib/hash.scm 653 */
																					int32_t BgL_arg2149z00_3853;
																					int32_t BgL_arg2150z00_3854;

																					BgL_arg2149z00_3853 =
																						(int32_t) (BgL_n1z00_3848);
																					BgL_arg2150z00_3854 =
																						(int32_t) (BgL_n2z00_3849);
																					BgL_arg2148z00_3852 =
																						(BgL_arg2149z00_3853 %
																						BgL_arg2150z00_3854);
																				}
																				{	/* Llib/hash.scm 653 */
																					long BgL_arg2252z00_3859;

																					BgL_arg2252z00_3859 =
																						(long) (BgL_arg2148z00_3852);
																					BgL_arg1582z00_1958 =
																						(long) (BgL_arg2252z00_3859);
																			}}
																		else
																			{	/* Llib/hash.scm 653 */
																				BgL_arg1582z00_1958 =
																					(BgL_n1z00_3848 % BgL_n2z00_3849);
																			}
																	}
																}
																BgL_arg1583z00_1959 = (BgL_iz00_1946 + 1L);
																{
																	long BgL_iz00_7078;
																	long BgL_offz00_7077;

																	BgL_offz00_7077 = BgL_arg1582z00_1958;
																	BgL_iz00_7078 = BgL_arg1583z00_1959;
																	BgL_iz00_1946 = BgL_iz00_7078;
																	BgL_offz00_1945 = BgL_offz00_7077;
																	goto BgL_zc3z04anonymousza31570ze3z87_1947;
																}
															}
														else
															{
																long BgL_iz00_7080;
																long BgL_offz00_7079;

																BgL_offz00_7079 = BgL_noffz00_1956;
																BgL_iz00_7080 = (BgL_iz00_1946 + 1L);
																BgL_iz00_1946 = BgL_iz00_7080;
																BgL_offz00_1945 = BgL_offz00_7079;
																goto BgL_zc3z04anonymousza31570ze3z87_1947;
															}
													}
											}
										else
											{	/* Llib/hash.scm 647 */
												return BFALSE;
											}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &open-string-hashtable-contains? */
	obj_t BGl_z62openzd2stringzd2hashtablezd2containszf3z43zz__hashz00(obj_t
		BgL_envz00_5429, obj_t BgL_tz00_5430, obj_t BgL_keyz00_5431)
	{
		{	/* Llib/hash.scm 639 */
			{	/* Llib/hash.scm 640 */
				obj_t BgL_auxz00_7089;
				obj_t BgL_auxz00_7082;

				if (STRINGP(BgL_keyz00_5431))
					{	/* Llib/hash.scm 640 */
						BgL_auxz00_7089 = BgL_keyz00_5431;
					}
				else
					{
						obj_t BgL_auxz00_7092;

						BgL_auxz00_7092 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(25485L), BGl_string2587z00zz__hashz00,
							BGl_string2588z00zz__hashz00, BgL_keyz00_5431);
						FAILURE(BgL_auxz00_7092, BFALSE, BFALSE);
					}
				if (STRUCTP(BgL_tz00_5430))
					{	/* Llib/hash.scm 640 */
						BgL_auxz00_7082 = BgL_tz00_5430;
					}
				else
					{
						obj_t BgL_auxz00_7085;

						BgL_auxz00_7085 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(25485L), BGl_string2587z00zz__hashz00,
							BGl_string2572z00zz__hashz00, BgL_tz00_5430);
						FAILURE(BgL_auxz00_7085, BFALSE, BFALSE);
					}
				return
					BGl_openzd2stringzd2hashtablezd2containszf3z21zz__hashz00
					(BgL_auxz00_7082, BgL_auxz00_7089);
			}
		}

	}



/* plain-hashtable-contains? */
	bool_t BGl_plainzd2hashtablezd2containszf3zf3zz__hashz00(obj_t
		BgL_tablez00_94, obj_t BgL_keyz00_95)
	{
		{	/* Llib/hash.scm 659 */
			{	/* Llib/hash.scm 660 */
				obj_t BgL_bucketsz00_1965;

				BgL_bucketsz00_1965 = STRUCT_REF(BgL_tablez00_94, (int) (2L));
				{	/* Llib/hash.scm 661 */
					long BgL_bucketzd2numzd2_1967;

					{	/* Llib/hash.scm 662 */
						long BgL_arg1595z00_1978;

						{	/* Llib/hash.scm 662 */
							obj_t BgL_hashnz00_3865;

							BgL_hashnz00_3865 = STRUCT_REF(BgL_tablez00_94, (int) (4L));
							if (PROCEDUREP(BgL_hashnz00_3865))
								{	/* Llib/hash.scm 662 */
									obj_t BgL_arg1318z00_3867;

									BgL_arg1318z00_3867 =
										BGL_PROCEDURE_CALL1(BgL_hashnz00_3865, BgL_keyz00_95);
									{	/* Llib/hash.scm 662 */
										long BgL_nz00_3869;

										BgL_nz00_3869 = (long) CINT(BgL_arg1318z00_3867);
										if ((BgL_nz00_3869 < 0L))
											{	/* Llib/hash.scm 662 */
												BgL_arg1595z00_1978 = NEG(BgL_nz00_3869);
											}
										else
											{	/* Llib/hash.scm 662 */
												BgL_arg1595z00_1978 = BgL_nz00_3869;
											}
									}
								}
							else
								{	/* Llib/hash.scm 662 */
									if ((BgL_hashnz00_3865 == BGl_symbol2567z00zz__hashz00))
										{	/* Llib/hash.scm 662 */
											BgL_arg1595z00_1978 =
												BGl_objzd2hashze70z35zz__hashz00(BgL_keyz00_95);
										}
									else
										{	/* Llib/hash.scm 662 */
											BgL_arg1595z00_1978 =
												BGl_getzd2hashnumberzd2zz__hashz00(BgL_keyz00_95);
										}
								}
						}
						{	/* Llib/hash.scm 662 */
							long BgL_n1z00_3873;
							long BgL_n2z00_3874;

							BgL_n1z00_3873 = BgL_arg1595z00_1978;
							BgL_n2z00_3874 = VECTOR_LENGTH(((obj_t) BgL_bucketsz00_1965));
							{	/* Llib/hash.scm 662 */
								bool_t BgL_test2908z00_7117;

								{	/* Llib/hash.scm 662 */
									long BgL_arg2151z00_3876;

									BgL_arg2151z00_3876 =
										(((BgL_n1z00_3873) | (BgL_n2z00_3874)) & -2147483648);
									BgL_test2908z00_7117 = (BgL_arg2151z00_3876 == 0L);
								}
								if (BgL_test2908z00_7117)
									{	/* Llib/hash.scm 662 */
										int32_t BgL_arg2148z00_3877;

										{	/* Llib/hash.scm 662 */
											int32_t BgL_arg2149z00_3878;
											int32_t BgL_arg2150z00_3879;

											BgL_arg2149z00_3878 = (int32_t) (BgL_n1z00_3873);
											BgL_arg2150z00_3879 = (int32_t) (BgL_n2z00_3874);
											BgL_arg2148z00_3877 =
												(BgL_arg2149z00_3878 % BgL_arg2150z00_3879);
										}
										{	/* Llib/hash.scm 662 */
											long BgL_arg2252z00_3884;

											BgL_arg2252z00_3884 = (long) (BgL_arg2148z00_3877);
											BgL_bucketzd2numzd2_1967 = (long) (BgL_arg2252z00_3884);
									}}
								else
									{	/* Llib/hash.scm 662 */
										BgL_bucketzd2numzd2_1967 =
											(BgL_n1z00_3873 % BgL_n2z00_3874);
									}
							}
						}
					}
					{	/* Llib/hash.scm 662 */
						obj_t BgL_bucketz00_1968;

						BgL_bucketz00_1968 =
							VECTOR_REF(
							((obj_t) BgL_bucketsz00_1965), BgL_bucketzd2numzd2_1967);
						{	/* Llib/hash.scm 663 */

							{
								obj_t BgL_bucketz00_1970;

								BgL_bucketz00_1970 = BgL_bucketz00_1968;
							BgL_zc3z04anonymousza31588ze3z87_1971:
								if (NULLP(BgL_bucketz00_1970))
									{	/* Llib/hash.scm 666 */
										return ((bool_t) 0);
									}
								else
									{	/* Llib/hash.scm 668 */
										bool_t BgL_test2910z00_7130;

										{	/* Llib/hash.scm 668 */
											obj_t BgL_arg1594z00_1976;

											{	/* Llib/hash.scm 668 */
												obj_t BgL_pairz00_3891;

												BgL_pairz00_3891 = CAR(((obj_t) BgL_bucketz00_1970));
												BgL_arg1594z00_1976 = CAR(BgL_pairz00_3891);
											}
											{	/* Llib/hash.scm 668 */
												obj_t BgL_eqtz00_3892;

												BgL_eqtz00_3892 =
													STRUCT_REF(BgL_tablez00_94, (int) (3L));
												if (PROCEDUREP(BgL_eqtz00_3892))
													{	/* Llib/hash.scm 668 */
														BgL_test2910z00_7130 =
															CBOOL(BGL_PROCEDURE_CALL2(BgL_eqtz00_3892,
																BgL_arg1594z00_1976, BgL_keyz00_95));
													}
												else
													{	/* Llib/hash.scm 668 */
														if ((BgL_arg1594z00_1976 == BgL_keyz00_95))
															{	/* Llib/hash.scm 668 */
																BgL_test2910z00_7130 = ((bool_t) 1);
															}
														else
															{	/* Llib/hash.scm 668 */
																if (STRINGP(BgL_arg1594z00_1976))
																	{	/* Llib/hash.scm 668 */
																		if (STRINGP(BgL_keyz00_95))
																			{	/* Llib/hash.scm 668 */
																				long BgL_l1z00_3899;

																				BgL_l1z00_3899 =
																					STRING_LENGTH(BgL_arg1594z00_1976);
																				if (
																					(BgL_l1z00_3899 ==
																						STRING_LENGTH(BgL_keyz00_95)))
																					{	/* Llib/hash.scm 668 */
																						int BgL_arg2041z00_3902;

																						{	/* Llib/hash.scm 668 */
																							char *BgL_auxz00_7156;
																							char *BgL_tmpz00_7154;

																							BgL_auxz00_7156 =
																								BSTRING_TO_STRING
																								(BgL_keyz00_95);
																							BgL_tmpz00_7154 =
																								BSTRING_TO_STRING
																								(BgL_arg1594z00_1976);
																							BgL_arg2041z00_3902 =
																								memcmp(BgL_tmpz00_7154,
																								BgL_auxz00_7156,
																								BgL_l1z00_3899);
																						}
																						BgL_test2910z00_7130 =
																							(
																							(long) (BgL_arg2041z00_3902) ==
																							0L);
																					}
																				else
																					{	/* Llib/hash.scm 668 */
																						BgL_test2910z00_7130 = ((bool_t) 0);
																					}
																			}
																		else
																			{	/* Llib/hash.scm 668 */
																				BgL_test2910z00_7130 = ((bool_t) 0);
																			}
																	}
																else
																	{	/* Llib/hash.scm 668 */
																		BgL_test2910z00_7130 = ((bool_t) 0);
																	}
															}
													}
											}
										}
										if (BgL_test2910z00_7130)
											{	/* Llib/hash.scm 668 */
												return ((bool_t) 1);
											}
										else
											{	/* Llib/hash.scm 671 */
												obj_t BgL_arg1593z00_1975;

												BgL_arg1593z00_1975 = CDR(((obj_t) BgL_bucketz00_1970));
												{
													obj_t BgL_bucketz00_7163;

													BgL_bucketz00_7163 = BgL_arg1593z00_1975;
													BgL_bucketz00_1970 = BgL_bucketz00_7163;
													goto BgL_zc3z04anonymousza31588ze3z87_1971;
												}
											}
									}
							}
						}
					}
				}
			}
		}

	}



/* hashtable-get */
	BGL_EXPORTED_DEF obj_t BGl_hashtablezd2getzd2zz__hashz00(obj_t
		BgL_tablez00_96, obj_t BgL_keyz00_97)
	{
		{	/* Llib/hash.scm 676 */
			{	/* Llib/hash.scm 678 */
				bool_t BgL_test2916z00_7164;

				if ((0L == ((long) CINT(STRUCT_REF(BgL_tablez00_96, (int) (5L))) & 8L)))
					{	/* Llib/hash.scm 294 */
						BgL_test2916z00_7164 = ((bool_t) 0);
					}
				else
					{	/* Llib/hash.scm 294 */
						BgL_test2916z00_7164 = ((bool_t) 1);
					}
				if (BgL_test2916z00_7164)
					{	/* Llib/hash.scm 678 */
						return
							BGl_openzd2stringzd2hashtablezd2getzd2zz__hashz00(BgL_tablez00_96,
							BgL_keyz00_97);
					}
				else
					{	/* Llib/hash.scm 678 */
						if (((long) CINT(STRUCT_REF(BgL_tablez00_96, (int) (5L))) == 4L))
							{	/* Llib/hash.scm 679 */
								return
									BGl_stringzd2hashtablezd2getz00zz__hashz00(BgL_tablez00_96,
									BgL_keyz00_97);
							}
						else
							{	/* Llib/hash.scm 680 */
								bool_t BgL_test2919z00_7178;

								if (
									(0L ==
										((long) CINT(STRUCT_REF(BgL_tablez00_96,
													(int) (5L))) & 3L)))
									{	/* Llib/hash.scm 288 */
										BgL_test2919z00_7178 = ((bool_t) 0);
									}
								else
									{	/* Llib/hash.scm 288 */
										BgL_test2919z00_7178 = ((bool_t) 1);
									}
								if (BgL_test2919z00_7178)
									{	/* Llib/hash.scm 680 */
										BGL_TAIL return
											BGl_weakzd2hashtablezd2getz00zz__weakhashz00
											(BgL_tablez00_96, BgL_keyz00_97);
									}
								else
									{	/* Llib/hash.scm 680 */
										BGL_TAIL return
											BGl_plainzd2hashtablezd2getz00zz__hashz00(BgL_tablez00_96,
											BgL_keyz00_97);
									}
							}
					}
			}
		}

	}



/* &hashtable-get */
	obj_t BGl_z62hashtablezd2getzb0zz__hashz00(obj_t BgL_envz00_5432,
		obj_t BgL_tablez00_5433, obj_t BgL_keyz00_5434)
	{
		{	/* Llib/hash.scm 676 */
			{	/* Llib/hash.scm 678 */
				obj_t BgL_auxz00_7187;

				if (STRUCTP(BgL_tablez00_5433))
					{	/* Llib/hash.scm 678 */
						BgL_auxz00_7187 = BgL_tablez00_5433;
					}
				else
					{
						obj_t BgL_auxz00_7190;

						BgL_auxz00_7190 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(27001L), BGl_string2589z00zz__hashz00,
							BGl_string2572z00zz__hashz00, BgL_tablez00_5433);
						FAILURE(BgL_auxz00_7190, BFALSE, BFALSE);
					}
				return
					BGl_hashtablezd2getzd2zz__hashz00(BgL_auxz00_7187, BgL_keyz00_5434);
			}
		}

	}



/* plain-hashtable-get */
	obj_t BGl_plainzd2hashtablezd2getz00zz__hashz00(obj_t BgL_tablez00_98,
		obj_t BgL_keyz00_99)
	{
		{	/* Llib/hash.scm 686 */
			{	/* Llib/hash.scm 687 */
				obj_t BgL_bucketsz00_1982;

				BgL_bucketsz00_1982 = STRUCT_REF(BgL_tablez00_98, (int) (2L));
				{	/* Llib/hash.scm 688 */
					long BgL_bucketzd2numzd2_1984;

					{	/* Llib/hash.scm 689 */
						long BgL_arg1606z00_1995;

						{	/* Llib/hash.scm 689 */
							obj_t BgL_hashnz00_3929;

							BgL_hashnz00_3929 = STRUCT_REF(BgL_tablez00_98, (int) (4L));
							if (PROCEDUREP(BgL_hashnz00_3929))
								{	/* Llib/hash.scm 689 */
									obj_t BgL_arg1318z00_3931;

									BgL_arg1318z00_3931 =
										BGL_PROCEDURE_CALL1(BgL_hashnz00_3929, BgL_keyz00_99);
									{	/* Llib/hash.scm 689 */
										long BgL_nz00_3933;

										BgL_nz00_3933 = (long) CINT(BgL_arg1318z00_3931);
										if ((BgL_nz00_3933 < 0L))
											{	/* Llib/hash.scm 689 */
												BgL_arg1606z00_1995 = NEG(BgL_nz00_3933);
											}
										else
											{	/* Llib/hash.scm 689 */
												BgL_arg1606z00_1995 = BgL_nz00_3933;
											}
									}
								}
							else
								{	/* Llib/hash.scm 689 */
									if ((BgL_hashnz00_3929 == BGl_symbol2567z00zz__hashz00))
										{	/* Llib/hash.scm 689 */
											BgL_arg1606z00_1995 =
												BGl_objzd2hashze70z35zz__hashz00(BgL_keyz00_99);
										}
									else
										{	/* Llib/hash.scm 689 */
											BgL_arg1606z00_1995 =
												BGl_getzd2hashnumberzd2zz__hashz00(BgL_keyz00_99);
										}
								}
						}
						{	/* Llib/hash.scm 689 */
							long BgL_n1z00_3937;
							long BgL_n2z00_3938;

							BgL_n1z00_3937 = BgL_arg1606z00_1995;
							BgL_n2z00_3938 = VECTOR_LENGTH(((obj_t) BgL_bucketsz00_1982));
							{	/* Llib/hash.scm 689 */
								bool_t BgL_test2925z00_7215;

								{	/* Llib/hash.scm 689 */
									long BgL_arg2151z00_3940;

									BgL_arg2151z00_3940 =
										(((BgL_n1z00_3937) | (BgL_n2z00_3938)) & -2147483648);
									BgL_test2925z00_7215 = (BgL_arg2151z00_3940 == 0L);
								}
								if (BgL_test2925z00_7215)
									{	/* Llib/hash.scm 689 */
										int32_t BgL_arg2148z00_3941;

										{	/* Llib/hash.scm 689 */
											int32_t BgL_arg2149z00_3942;
											int32_t BgL_arg2150z00_3943;

											BgL_arg2149z00_3942 = (int32_t) (BgL_n1z00_3937);
											BgL_arg2150z00_3943 = (int32_t) (BgL_n2z00_3938);
											BgL_arg2148z00_3941 =
												(BgL_arg2149z00_3942 % BgL_arg2150z00_3943);
										}
										{	/* Llib/hash.scm 689 */
											long BgL_arg2252z00_3948;

											BgL_arg2252z00_3948 = (long) (BgL_arg2148z00_3941);
											BgL_bucketzd2numzd2_1984 = (long) (BgL_arg2252z00_3948);
									}}
								else
									{	/* Llib/hash.scm 689 */
										BgL_bucketzd2numzd2_1984 =
											(BgL_n1z00_3937 % BgL_n2z00_3938);
									}
							}
						}
					}
					{	/* Llib/hash.scm 689 */
						obj_t BgL_bucketz00_1985;

						BgL_bucketz00_1985 =
							VECTOR_REF(
							((obj_t) BgL_bucketsz00_1982), BgL_bucketzd2numzd2_1984);
						{	/* Llib/hash.scm 690 */

							{
								obj_t BgL_bucketz00_1987;

								BgL_bucketz00_1987 = BgL_bucketz00_1985;
							BgL_zc3z04anonymousza31599ze3z87_1988:
								if (NULLP(BgL_bucketz00_1987))
									{	/* Llib/hash.scm 693 */
										return BFALSE;
									}
								else
									{	/* Llib/hash.scm 695 */
										bool_t BgL_test2927z00_7228;

										{	/* Llib/hash.scm 695 */
											obj_t BgL_arg1605z00_1993;

											{	/* Llib/hash.scm 695 */
												obj_t BgL_pairz00_3955;

												BgL_pairz00_3955 = CAR(((obj_t) BgL_bucketz00_1987));
												BgL_arg1605z00_1993 = CAR(BgL_pairz00_3955);
											}
											{	/* Llib/hash.scm 695 */
												obj_t BgL_eqtz00_3956;

												BgL_eqtz00_3956 =
													STRUCT_REF(BgL_tablez00_98, (int) (3L));
												if (PROCEDUREP(BgL_eqtz00_3956))
													{	/* Llib/hash.scm 695 */
														BgL_test2927z00_7228 =
															CBOOL(BGL_PROCEDURE_CALL2(BgL_eqtz00_3956,
																BgL_arg1605z00_1993, BgL_keyz00_99));
													}
												else
													{	/* Llib/hash.scm 695 */
														if ((BgL_arg1605z00_1993 == BgL_keyz00_99))
															{	/* Llib/hash.scm 695 */
																BgL_test2927z00_7228 = ((bool_t) 1);
															}
														else
															{	/* Llib/hash.scm 695 */
																if (STRINGP(BgL_arg1605z00_1993))
																	{	/* Llib/hash.scm 695 */
																		if (STRINGP(BgL_keyz00_99))
																			{	/* Llib/hash.scm 695 */
																				long BgL_l1z00_3963;

																				BgL_l1z00_3963 =
																					STRING_LENGTH(BgL_arg1605z00_1993);
																				if (
																					(BgL_l1z00_3963 ==
																						STRING_LENGTH(BgL_keyz00_99)))
																					{	/* Llib/hash.scm 695 */
																						int BgL_arg2041z00_3966;

																						{	/* Llib/hash.scm 695 */
																							char *BgL_auxz00_7254;
																							char *BgL_tmpz00_7252;

																							BgL_auxz00_7254 =
																								BSTRING_TO_STRING
																								(BgL_keyz00_99);
																							BgL_tmpz00_7252 =
																								BSTRING_TO_STRING
																								(BgL_arg1605z00_1993);
																							BgL_arg2041z00_3966 =
																								memcmp(BgL_tmpz00_7252,
																								BgL_auxz00_7254,
																								BgL_l1z00_3963);
																						}
																						BgL_test2927z00_7228 =
																							(
																							(long) (BgL_arg2041z00_3966) ==
																							0L);
																					}
																				else
																					{	/* Llib/hash.scm 695 */
																						BgL_test2927z00_7228 = ((bool_t) 0);
																					}
																			}
																		else
																			{	/* Llib/hash.scm 695 */
																				BgL_test2927z00_7228 = ((bool_t) 0);
																			}
																	}
																else
																	{	/* Llib/hash.scm 695 */
																		BgL_test2927z00_7228 = ((bool_t) 0);
																	}
															}
													}
											}
										}
										if (BgL_test2927z00_7228)
											{	/* Llib/hash.scm 696 */
												obj_t BgL_pairz00_3975;

												BgL_pairz00_3975 = CAR(((obj_t) BgL_bucketz00_1987));
												return CDR(BgL_pairz00_3975);
											}
										else
											{	/* Llib/hash.scm 698 */
												obj_t BgL_arg1603z00_1992;

												BgL_arg1603z00_1992 = CDR(((obj_t) BgL_bucketz00_1987));
												{
													obj_t BgL_bucketz00_7264;

													BgL_bucketz00_7264 = BgL_arg1603z00_1992;
													BgL_bucketz00_1987 = BgL_bucketz00_7264;
													goto BgL_zc3z04anonymousza31599ze3z87_1988;
												}
											}
									}
							}
						}
					}
				}
			}
		}

	}



/* string-hashtable-get */
	BGL_EXPORTED_DEF obj_t BGl_stringzd2hashtablezd2getz00zz__hashz00(obj_t
		BgL_tablez00_100, obj_t BgL_keyz00_101)
	{
		{	/* Llib/hash.scm 703 */
			{	/* Llib/hash.scm 704 */
				obj_t BgL_bucketsz00_1996;

				BgL_bucketsz00_1996 = STRUCT_REF(BgL_tablez00_100, (int) (2L));
				{	/* Llib/hash.scm 705 */
					long BgL_bucketzd2numzd2_1998;

					{	/* Llib/hash.scm 706 */
						long BgL_arg1613z00_2009;

						{	/* Llib/hash.scm 706 */
							long BgL_arg1615z00_2010;

							BgL_arg1615z00_2010 = STRING_LENGTH(BgL_keyz00_101);
							BgL_arg1613z00_2009 =
								bgl_string_hash(BSTRING_TO_STRING(BgL_keyz00_101),
								(int) (0L), (int) (BgL_arg1615z00_2010));
						}
						{	/* Llib/hash.scm 706 */
							long BgL_n1z00_3980;
							long BgL_n2z00_3981;

							BgL_n1z00_3980 = BgL_arg1613z00_2009;
							BgL_n2z00_3981 = VECTOR_LENGTH(((obj_t) BgL_bucketsz00_1996));
							{	/* Llib/hash.scm 706 */
								bool_t BgL_test2933z00_7274;

								{	/* Llib/hash.scm 706 */
									long BgL_arg2151z00_3983;

									BgL_arg2151z00_3983 =
										(((BgL_n1z00_3980) | (BgL_n2z00_3981)) & -2147483648);
									BgL_test2933z00_7274 = (BgL_arg2151z00_3983 == 0L);
								}
								if (BgL_test2933z00_7274)
									{	/* Llib/hash.scm 706 */
										int32_t BgL_arg2148z00_3984;

										{	/* Llib/hash.scm 706 */
											int32_t BgL_arg2149z00_3985;
											int32_t BgL_arg2150z00_3986;

											BgL_arg2149z00_3985 = (int32_t) (BgL_n1z00_3980);
											BgL_arg2150z00_3986 = (int32_t) (BgL_n2z00_3981);
											BgL_arg2148z00_3984 =
												(BgL_arg2149z00_3985 % BgL_arg2150z00_3986);
										}
										{	/* Llib/hash.scm 706 */
											long BgL_arg2252z00_3991;

											BgL_arg2252z00_3991 = (long) (BgL_arg2148z00_3984);
											BgL_bucketzd2numzd2_1998 = (long) (BgL_arg2252z00_3991);
									}}
								else
									{	/* Llib/hash.scm 706 */
										BgL_bucketzd2numzd2_1998 =
											(BgL_n1z00_3980 % BgL_n2z00_3981);
									}
							}
						}
					}
					{	/* Llib/hash.scm 706 */
						obj_t BgL_bucketz00_1999;

						BgL_bucketz00_1999 =
							VECTOR_REF(
							((obj_t) BgL_bucketsz00_1996), BgL_bucketzd2numzd2_1998);
						{	/* Llib/hash.scm 707 */

							{
								obj_t BgL_bucketz00_2001;

								BgL_bucketz00_2001 = BgL_bucketz00_1999;
							BgL_zc3z04anonymousza31607ze3z87_2002:
								if (NULLP(BgL_bucketz00_2001))
									{	/* Llib/hash.scm 710 */
										return BFALSE;
									}
								else
									{	/* Llib/hash.scm 712 */
										bool_t BgL_test2935z00_7287;

										{	/* Llib/hash.scm 712 */
											obj_t BgL_arg1612z00_2007;

											{	/* Llib/hash.scm 712 */
												obj_t BgL_pairz00_3998;

												BgL_pairz00_3998 = CAR(((obj_t) BgL_bucketz00_2001));
												BgL_arg1612z00_2007 = CAR(BgL_pairz00_3998);
											}
											{	/* Llib/hash.scm 712 */
												long BgL_l1z00_4001;

												BgL_l1z00_4001 =
													STRING_LENGTH(((obj_t) BgL_arg1612z00_2007));
												if ((BgL_l1z00_4001 == STRING_LENGTH(BgL_keyz00_101)))
													{	/* Llib/hash.scm 712 */
														int BgL_arg2041z00_4004;

														{	/* Llib/hash.scm 712 */
															char *BgL_auxz00_7299;
															char *BgL_tmpz00_7296;

															BgL_auxz00_7299 =
																BSTRING_TO_STRING(BgL_keyz00_101);
															BgL_tmpz00_7296 =
																BSTRING_TO_STRING(
																((obj_t) BgL_arg1612z00_2007));
															BgL_arg2041z00_4004 =
																memcmp(BgL_tmpz00_7296, BgL_auxz00_7299,
																BgL_l1z00_4001);
														}
														BgL_test2935z00_7287 =
															((long) (BgL_arg2041z00_4004) == 0L);
													}
												else
													{	/* Llib/hash.scm 712 */
														BgL_test2935z00_7287 = ((bool_t) 0);
													}
											}
										}
										if (BgL_test2935z00_7287)
											{	/* Llib/hash.scm 713 */
												obj_t BgL_pairz00_4013;

												BgL_pairz00_4013 = CAR(((obj_t) BgL_bucketz00_2001));
												return CDR(BgL_pairz00_4013);
											}
										else
											{	/* Llib/hash.scm 715 */
												obj_t BgL_arg1611z00_2006;

												BgL_arg1611z00_2006 = CDR(((obj_t) BgL_bucketz00_2001));
												{
													obj_t BgL_bucketz00_7309;

													BgL_bucketz00_7309 = BgL_arg1611z00_2006;
													BgL_bucketz00_2001 = BgL_bucketz00_7309;
													goto BgL_zc3z04anonymousza31607ze3z87_2002;
												}
											}
									}
							}
						}
					}
				}
			}
		}

	}



/* &string-hashtable-get */
	obj_t BGl_z62stringzd2hashtablezd2getz62zz__hashz00(obj_t BgL_envz00_5435,
		obj_t BgL_tablez00_5436, obj_t BgL_keyz00_5437)
	{
		{	/* Llib/hash.scm 703 */
			{	/* Llib/hash.scm 704 */
				obj_t BgL_auxz00_7317;
				obj_t BgL_auxz00_7310;

				if (STRINGP(BgL_keyz00_5437))
					{	/* Llib/hash.scm 704 */
						BgL_auxz00_7317 = BgL_keyz00_5437;
					}
				else
					{
						obj_t BgL_auxz00_7320;

						BgL_auxz00_7320 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(28203L), BGl_string2590z00zz__hashz00,
							BGl_string2588z00zz__hashz00, BgL_keyz00_5437);
						FAILURE(BgL_auxz00_7320, BFALSE, BFALSE);
					}
				if (STRUCTP(BgL_tablez00_5436))
					{	/* Llib/hash.scm 704 */
						BgL_auxz00_7310 = BgL_tablez00_5436;
					}
				else
					{
						obj_t BgL_auxz00_7313;

						BgL_auxz00_7313 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(28203L), BGl_string2590z00zz__hashz00,
							BGl_string2572z00zz__hashz00, BgL_tablez00_5436);
						FAILURE(BgL_auxz00_7313, BFALSE, BFALSE);
					}
				return
					BGl_stringzd2hashtablezd2getz00zz__hashz00(BgL_auxz00_7310,
					BgL_auxz00_7317);
			}
		}

	}



/* open-string-hashtable-get */
	BGL_EXPORTED_DEF obj_t BGl_openzd2stringzd2hashtablezd2getzd2zz__hashz00(obj_t
		BgL_tz00_102, obj_t BgL_keyz00_103)
	{
		{	/* Llib/hash.scm 720 */
			{	/* Llib/hash.scm 721 */
				obj_t BgL_siza7eza7_2011;

				BgL_siza7eza7_2011 = STRUCT_REF(BgL_tz00_102, (int) (1L));
				{	/* Llib/hash.scm 721 */
					obj_t BgL_bucketsz00_2012;

					BgL_bucketsz00_2012 = STRUCT_REF(BgL_tz00_102, (int) (2L));
					{	/* Llib/hash.scm 722 */
						long BgL_hashz00_2013;

						{	/* Llib/hash.scm 723 */
							long BgL_arg1630z00_2035;

							BgL_arg1630z00_2035 = STRING_LENGTH(BgL_keyz00_103);
							BgL_hashz00_2013 =
								bgl_string_hash(BSTRING_TO_STRING(BgL_keyz00_103),
								(int) (0L), (int) (BgL_arg1630z00_2035));
						}
						{	/* Llib/hash.scm 723 */

							{	/* Llib/hash.scm 724 */
								long BgL_g1069z00_2014;

								{	/* Llib/hash.scm 724 */
									long BgL_n1z00_4018;
									long BgL_n2z00_4019;

									BgL_n1z00_4018 = BgL_hashz00_2013;
									BgL_n2z00_4019 = (long) CINT(BgL_siza7eza7_2011);
									{	/* Llib/hash.scm 724 */
										bool_t BgL_test2939z00_7335;

										{	/* Llib/hash.scm 724 */
											long BgL_arg2151z00_4021;

											BgL_arg2151z00_4021 =
												(((BgL_n1z00_4018) | (BgL_n2z00_4019)) & -2147483648);
											BgL_test2939z00_7335 = (BgL_arg2151z00_4021 == 0L);
										}
										if (BgL_test2939z00_7335)
											{	/* Llib/hash.scm 724 */
												int32_t BgL_arg2148z00_4022;

												{	/* Llib/hash.scm 724 */
													int32_t BgL_arg2149z00_4023;
													int32_t BgL_arg2150z00_4024;

													BgL_arg2149z00_4023 = (int32_t) (BgL_n1z00_4018);
													BgL_arg2150z00_4024 = (int32_t) (BgL_n2z00_4019);
													BgL_arg2148z00_4022 =
														(BgL_arg2149z00_4023 % BgL_arg2150z00_4024);
												}
												{	/* Llib/hash.scm 724 */
													long BgL_arg2252z00_4029;

													BgL_arg2252z00_4029 = (long) (BgL_arg2148z00_4022);
													BgL_g1069z00_2014 = (long) (BgL_arg2252z00_4029);
											}}
										else
											{	/* Llib/hash.scm 724 */
												BgL_g1069z00_2014 = (BgL_n1z00_4018 % BgL_n2z00_4019);
											}
									}
								}
								{
									long BgL_offz00_2016;
									long BgL_iz00_2017;

									BgL_offz00_2016 = BgL_g1069z00_2014;
									BgL_iz00_2017 = 1L;
								BgL_zc3z04anonymousza31616ze3z87_2018:
									{	/* Llib/hash.scm 726 */
										long BgL_off3z00_2019;

										BgL_off3z00_2019 = (BgL_offz00_2016 * 3L);
										if (CBOOL(VECTOR_REF(
													((obj_t) BgL_bucketsz00_2012), BgL_off3z00_2019)))
											{	/* Llib/hash.scm 728 */
												bool_t BgL_test2941z00_7349;

												{	/* Llib/hash.scm 728 */
													obj_t BgL_arg1629z00_2033;

													BgL_arg1629z00_2033 =
														VECTOR_REF(
														((obj_t) BgL_bucketsz00_2012), BgL_off3z00_2019);
													{	/* Llib/hash.scm 728 */
														long BgL_l1z00_4038;

														BgL_l1z00_4038 =
															STRING_LENGTH(((obj_t) BgL_arg1629z00_2033));
														if (
															(BgL_l1z00_4038 == STRING_LENGTH(BgL_keyz00_103)))
															{	/* Llib/hash.scm 728 */
																int BgL_arg2041z00_4041;

																{	/* Llib/hash.scm 728 */
																	char *BgL_auxz00_7360;
																	char *BgL_tmpz00_7357;

																	BgL_auxz00_7360 =
																		BSTRING_TO_STRING(BgL_keyz00_103);
																	BgL_tmpz00_7357 =
																		BSTRING_TO_STRING(
																		((obj_t) BgL_arg1629z00_2033));
																	BgL_arg2041z00_4041 =
																		memcmp(BgL_tmpz00_7357, BgL_auxz00_7360,
																		BgL_l1z00_4038);
																}
																BgL_test2941z00_7349 =
																	((long) (BgL_arg2041z00_4041) == 0L);
															}
														else
															{	/* Llib/hash.scm 728 */
																BgL_test2941z00_7349 = ((bool_t) 0);
															}
													}
												}
												if (BgL_test2941z00_7349)
													{	/* Llib/hash.scm 729 */
														bool_t BgL_test2943z00_7365;

														{	/* Llib/hash.scm 729 */
															long BgL_arg1623z00_2026;

															BgL_arg1623z00_2026 = (BgL_off3z00_2019 + 2L);
															BgL_test2943z00_7365 =
																CBOOL(VECTOR_REF(
																	((obj_t) BgL_bucketsz00_2012),
																	BgL_arg1623z00_2026));
														}
														if (BgL_test2943z00_7365)
															{	/* Llib/hash.scm 730 */
																long BgL_arg1622z00_2025;

																BgL_arg1622z00_2025 = (BgL_off3z00_2019 + 1L);
																return
																	VECTOR_REF(
																	((obj_t) BgL_bucketsz00_2012),
																	BgL_arg1622z00_2025);
															}
														else
															{	/* Llib/hash.scm 729 */
																return BFALSE;
															}
													}
												else
													{	/* Llib/hash.scm 731 */
														long BgL_noffz00_2027;

														BgL_noffz00_2027 =
															(BgL_offz00_2016 +
															(BgL_iz00_2017 * BgL_iz00_2017));
														if (
															(BgL_noffz00_2027 >=
																(long) CINT(BgL_siza7eza7_2011)))
															{	/* Llib/hash.scm 733 */
																long BgL_arg1625z00_2029;
																long BgL_arg1626z00_2030;

																{	/* Llib/hash.scm 733 */
																	long BgL_n1z00_4059;
																	long BgL_n2z00_4060;

																	BgL_n1z00_4059 = BgL_noffz00_2027;
																	BgL_n2z00_4060 =
																		(long) CINT(BgL_siza7eza7_2011);
																	{	/* Llib/hash.scm 733 */
																		bool_t BgL_test2945z00_7379;

																		{	/* Llib/hash.scm 733 */
																			long BgL_arg2151z00_4062;

																			BgL_arg2151z00_4062 =
																				(((BgL_n1z00_4059) | (BgL_n2z00_4060)) &
																				-2147483648);
																			BgL_test2945z00_7379 =
																				(BgL_arg2151z00_4062 == 0L);
																		}
																		if (BgL_test2945z00_7379)
																			{	/* Llib/hash.scm 733 */
																				int32_t BgL_arg2148z00_4063;

																				{	/* Llib/hash.scm 733 */
																					int32_t BgL_arg2149z00_4064;
																					int32_t BgL_arg2150z00_4065;

																					BgL_arg2149z00_4064 =
																						(int32_t) (BgL_n1z00_4059);
																					BgL_arg2150z00_4065 =
																						(int32_t) (BgL_n2z00_4060);
																					BgL_arg2148z00_4063 =
																						(BgL_arg2149z00_4064 %
																						BgL_arg2150z00_4065);
																				}
																				{	/* Llib/hash.scm 733 */
																					long BgL_arg2252z00_4070;

																					BgL_arg2252z00_4070 =
																						(long) (BgL_arg2148z00_4063);
																					BgL_arg1625z00_2029 =
																						(long) (BgL_arg2252z00_4070);
																			}}
																		else
																			{	/* Llib/hash.scm 733 */
																				BgL_arg1625z00_2029 =
																					(BgL_n1z00_4059 % BgL_n2z00_4060);
																			}
																	}
																}
																BgL_arg1626z00_2030 = (BgL_iz00_2017 + 1L);
																{
																	long BgL_iz00_7390;
																	long BgL_offz00_7389;

																	BgL_offz00_7389 = BgL_arg1625z00_2029;
																	BgL_iz00_7390 = BgL_arg1626z00_2030;
																	BgL_iz00_2017 = BgL_iz00_7390;
																	BgL_offz00_2016 = BgL_offz00_7389;
																	goto BgL_zc3z04anonymousza31616ze3z87_2018;
																}
															}
														else
															{
																long BgL_iz00_7392;
																long BgL_offz00_7391;

																BgL_offz00_7391 = BgL_noffz00_2027;
																BgL_iz00_7392 = (BgL_iz00_2017 + 1L);
																BgL_iz00_2017 = BgL_iz00_7392;
																BgL_offz00_2016 = BgL_offz00_7391;
																goto BgL_zc3z04anonymousza31616ze3z87_2018;
															}
													}
											}
										else
											{	/* Llib/hash.scm 727 */
												return BFALSE;
											}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &open-string-hashtable-get */
	obj_t BGl_z62openzd2stringzd2hashtablezd2getzb0zz__hashz00(obj_t
		BgL_envz00_5438, obj_t BgL_tz00_5439, obj_t BgL_keyz00_5440)
	{
		{	/* Llib/hash.scm 720 */
			{	/* Llib/hash.scm 721 */
				obj_t BgL_auxz00_7401;
				obj_t BgL_auxz00_7394;

				if (STRINGP(BgL_keyz00_5440))
					{	/* Llib/hash.scm 721 */
						BgL_auxz00_7401 = BgL_keyz00_5440;
					}
				else
					{
						obj_t BgL_auxz00_7404;

						BgL_auxz00_7404 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(28856L), BGl_string2591z00zz__hashz00,
							BGl_string2588z00zz__hashz00, BgL_keyz00_5440);
						FAILURE(BgL_auxz00_7404, BFALSE, BFALSE);
					}
				if (STRUCTP(BgL_tz00_5439))
					{	/* Llib/hash.scm 721 */
						BgL_auxz00_7394 = BgL_tz00_5439;
					}
				else
					{
						obj_t BgL_auxz00_7397;

						BgL_auxz00_7397 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(28856L), BGl_string2591z00zz__hashz00,
							BGl_string2572z00zz__hashz00, BgL_tz00_5439);
						FAILURE(BgL_auxz00_7397, BFALSE, BFALSE);
					}
				return
					BGl_openzd2stringzd2hashtablezd2getzd2zz__hashz00(BgL_auxz00_7394,
					BgL_auxz00_7401);
			}
		}

	}



/* $open-string-hashtable-get */
	BGL_EXPORTED_DEF obj_t
		BGl_z42openzd2stringzd2hashtablezd2getz90zz__hashz00(obj_t BgL_tz00_104,
		char *BgL_keyz00_105)
	{
		{	/* Llib/hash.scm 741 */
			{	/* Llib/hash.scm 744 */
				obj_t BgL_siza7eza7_2036;

				BgL_siza7eza7_2036 = STRUCT_REF(BgL_tz00_104, (int) (1L));
				{	/* Llib/hash.scm 744 */
					obj_t BgL_bucketsz00_2037;

					BgL_bucketsz00_2037 = STRUCT_REF(BgL_tz00_104, (int) (2L));
					{	/* Llib/hash.scm 745 */
						long BgL_lenz00_2038;

						BgL_lenz00_2038 = strlen(BgL_keyz00_105);
						{	/* Llib/hash.scm 746 */
							long BgL_hashz00_2039;

							BgL_hashz00_2039 =
								bgl_string_hash(BgL_keyz00_105,
								(int) (0L), (int) (BgL_lenz00_2038));
							{	/* Llib/hash.scm 747 */

								{	/* Llib/hash.scm 748 */
									long BgL_g1070z00_2040;

									{	/* Llib/hash.scm 748 */
										long BgL_n1z00_4076;
										long BgL_n2z00_4077;

										BgL_n1z00_4076 = BgL_hashz00_2039;
										BgL_n2z00_4077 = (long) CINT(BgL_siza7eza7_2036);
										{	/* Llib/hash.scm 748 */
											bool_t BgL_test2948z00_7418;

											{	/* Llib/hash.scm 748 */
												long BgL_arg2151z00_4079;

												BgL_arg2151z00_4079 =
													(((BgL_n1z00_4076) | (BgL_n2z00_4077)) & -2147483648);
												BgL_test2948z00_7418 = (BgL_arg2151z00_4079 == 0L);
											}
											if (BgL_test2948z00_7418)
												{	/* Llib/hash.scm 748 */
													int32_t BgL_arg2148z00_4080;

													{	/* Llib/hash.scm 748 */
														int32_t BgL_arg2149z00_4081;
														int32_t BgL_arg2150z00_4082;

														BgL_arg2149z00_4081 = (int32_t) (BgL_n1z00_4076);
														BgL_arg2150z00_4082 = (int32_t) (BgL_n2z00_4077);
														BgL_arg2148z00_4080 =
															(BgL_arg2149z00_4081 % BgL_arg2150z00_4082);
													}
													{	/* Llib/hash.scm 748 */
														long BgL_arg2252z00_4087;

														BgL_arg2252z00_4087 = (long) (BgL_arg2148z00_4080);
														BgL_g1070z00_2040 = (long) (BgL_arg2252z00_4087);
												}}
											else
												{	/* Llib/hash.scm 748 */
													BgL_g1070z00_2040 = (BgL_n1z00_4076 % BgL_n2z00_4077);
												}
										}
									}
									{
										long BgL_offz00_2042;
										long BgL_iz00_2043;

										BgL_offz00_2042 = BgL_g1070z00_2040;
										BgL_iz00_2043 = 1L;
										{	/* Llib/hash.scm 750 */
											long BgL_off3z00_2045;

											BgL_off3z00_2045 = (BgL_offz00_2042 * 3L);
											if (CBOOL(VECTOR_REF(
														((obj_t) BgL_bucketsz00_2037), BgL_off3z00_2045)))
												{	/* Llib/hash.scm 751 */
													{	/* Llib/hash.scm 752 */
														obj_t BgL_arg1645z00_2059;

														BgL_arg1645z00_2059 =
															VECTOR_REF(
															((obj_t) BgL_bucketsz00_2037), BgL_off3z00_2045);
														{	/* Llib/hash.scm 752 */
															int BgL_xz00_5573;

															{	/* Llib/hash.scm 752 */
																char *BgL_tmpz00_7434;

																BgL_tmpz00_7434 =
																	BSTRING_TO_STRING(BgL_arg1645z00_2059);
																BgL_xz00_5573 =
																	memcmp(BgL_tmpz00_7434, BgL_keyz00_105,
																	BgL_lenz00_2038);
															} ((bool_t) 1);
													}}
													{	/* Llib/hash.scm 753 */
														bool_t BgL_test2950z00_7437;

														{	/* Llib/hash.scm 753 */
															long BgL_arg1639z00_2052;

															BgL_arg1639z00_2052 = (BgL_off3z00_2045 + 2L);
															BgL_test2950z00_7437 =
																CBOOL(VECTOR_REF(
																	((obj_t) BgL_bucketsz00_2037),
																	BgL_arg1639z00_2052));
														}
														if (BgL_test2950z00_7437)
															{	/* Llib/hash.scm 754 */
																long BgL_arg1638z00_2051;

																BgL_arg1638z00_2051 = (BgL_off3z00_2045 + 1L);
																return
																	VECTOR_REF(
																	((obj_t) BgL_bucketsz00_2037),
																	BgL_arg1638z00_2051);
															}
														else
															{	/* Llib/hash.scm 753 */
																return BFALSE;
															}
													}
												}
											else
												{	/* Llib/hash.scm 751 */
													return BFALSE;
												}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &$open-string-hashtable-get */
	obj_t BGl_z62z42openzd2stringzd2hashtablezd2getzf2zz__hashz00(obj_t
		BgL_envz00_5441, obj_t BgL_tz00_5442, obj_t BgL_keyz00_5443)
	{
		{	/* Llib/hash.scm 741 */
			{	/* Llib/hash.scm 744 */
				char *BgL_auxz00_7452;
				obj_t BgL_auxz00_7445;

				{	/* Llib/hash.scm 744 */
					obj_t BgL_tmpz00_7453;

					if (STRINGP(BgL_keyz00_5443))
						{	/* Llib/hash.scm 744 */
							BgL_tmpz00_7453 = BgL_keyz00_5443;
						}
					else
						{
							obj_t BgL_auxz00_7456;

							BgL_auxz00_7456 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
								BINT(29853L), BGl_string2592z00zz__hashz00,
								BGl_string2588z00zz__hashz00, BgL_keyz00_5443);
							FAILURE(BgL_auxz00_7456, BFALSE, BFALSE);
						}
					BgL_auxz00_7452 = BSTRING_TO_STRING(BgL_tmpz00_7453);
				}
				if (STRUCTP(BgL_tz00_5442))
					{	/* Llib/hash.scm 744 */
						BgL_auxz00_7445 = BgL_tz00_5442;
					}
				else
					{
						obj_t BgL_auxz00_7448;

						BgL_auxz00_7448 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(29853L), BGl_string2592z00zz__hashz00,
							BGl_string2572z00zz__hashz00, BgL_tz00_5442);
						FAILURE(BgL_auxz00_7448, BFALSE, BFALSE);
					}
				return
					BGl_z42openzd2stringzd2hashtablezd2getz90zz__hashz00(BgL_auxz00_7445,
					BgL_auxz00_7452);
			}
		}

	}



/* hashtable-put! */
	BGL_EXPORTED_DEF obj_t BGl_hashtablezd2putz12zc0zz__hashz00(obj_t
		BgL_tablez00_106, obj_t BgL_keyz00_107, obj_t BgL_objz00_108)
	{
		{	/* Llib/hash.scm 765 */
			{	/* Llib/hash.scm 767 */
				bool_t BgL_test2953z00_7462;

				if (
					(0L == ((long) CINT(STRUCT_REF(BgL_tablez00_106, (int) (5L))) & 8L)))
					{	/* Llib/hash.scm 294 */
						BgL_test2953z00_7462 = ((bool_t) 0);
					}
				else
					{	/* Llib/hash.scm 294 */
						BgL_test2953z00_7462 = ((bool_t) 1);
					}
				if (BgL_test2953z00_7462)
					{	/* Llib/hash.scm 779 */
						long BgL_arg1648z00_4132;

						{	/* Llib/hash.scm 779 */
							long BgL_arg1649z00_4133;

							BgL_arg1649z00_4133 = STRING_LENGTH(((obj_t) BgL_keyz00_107));
							BgL_arg1648z00_4132 =
								bgl_string_hash(BSTRING_TO_STRING(
									((obj_t) BgL_keyz00_107)),
								(int) (0L), (int) (BgL_arg1649z00_4133));
						}
						return
							BGl_openzd2stringzd2hashtablezd2putzf2hashz12z32zz__hashz00
							(BgL_tablez00_106, ((obj_t) BgL_keyz00_107), BgL_objz00_108,
							BINT(BgL_arg1648z00_4132));
					}
				else
					{	/* Llib/hash.scm 769 */
						bool_t BgL_test2955z00_7479;

						if (
							(0L ==
								((long) CINT(STRUCT_REF(BgL_tablez00_106, (int) (5L))) & 3L)))
							{	/* Llib/hash.scm 288 */
								BgL_test2955z00_7479 = ((bool_t) 0);
							}
						else
							{	/* Llib/hash.scm 288 */
								BgL_test2955z00_7479 = ((bool_t) 1);
							}
						if (BgL_test2955z00_7479)
							{	/* Llib/hash.scm 769 */
								BGL_TAIL return
									BGl_weakzd2hashtablezd2putz12z12zz__weakhashz00
									(BgL_tablez00_106, BgL_keyz00_107, BgL_objz00_108);
							}
						else
							{	/* Llib/hash.scm 769 */
								BGL_TAIL return
									BGl_plainzd2hashtablezd2putz12z12zz__hashz00(BgL_tablez00_106,
									BgL_keyz00_107, BgL_objz00_108);
							}
					}
			}
		}

	}



/* &hashtable-put! */
	obj_t BGl_z62hashtablezd2putz12za2zz__hashz00(obj_t BgL_envz00_5444,
		obj_t BgL_tablez00_5445, obj_t BgL_keyz00_5446, obj_t BgL_objz00_5447)
	{
		{	/* Llib/hash.scm 765 */
			{	/* Llib/hash.scm 767 */
				obj_t BgL_auxz00_7488;

				if (STRUCTP(BgL_tablez00_5445))
					{	/* Llib/hash.scm 767 */
						BgL_auxz00_7488 = BgL_tablez00_5445;
					}
				else
					{
						obj_t BgL_auxz00_7491;

						BgL_auxz00_7491 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(30779L), BGl_string2593z00zz__hashz00,
							BGl_string2572z00zz__hashz00, BgL_tablez00_5445);
						FAILURE(BgL_auxz00_7491, BFALSE, BFALSE);
					}
				return
					BGl_hashtablezd2putz12zc0zz__hashz00(BgL_auxz00_7488, BgL_keyz00_5446,
					BgL_objz00_5447);
			}
		}

	}



/* open-string-hashtable-put! */
	BGL_EXPORTED_DEF obj_t
		BGl_openzd2stringzd2hashtablezd2putz12zc0zz__hashz00(obj_t BgL_tablez00_109,
		obj_t BgL_keyz00_110, obj_t BgL_objz00_111)
	{
		{	/* Llib/hash.scm 777 */
			{	/* Llib/hash.scm 779 */
				long BgL_arg1648z00_4142;

				{	/* Llib/hash.scm 779 */
					long BgL_arg1649z00_4143;

					BgL_arg1649z00_4143 = STRING_LENGTH(BgL_keyz00_110);
					BgL_arg1648z00_4142 =
						bgl_string_hash(BSTRING_TO_STRING(BgL_keyz00_110),
						(int) (0L), (int) (BgL_arg1649z00_4143));
				}
				return
					BGl_openzd2stringzd2hashtablezd2putzf2hashz12z32zz__hashz00
					(BgL_tablez00_109, BgL_keyz00_110, BgL_objz00_111,
					BINT(BgL_arg1648z00_4142));
			}
		}

	}



/* &open-string-hashtable-put! */
	obj_t BGl_z62openzd2stringzd2hashtablezd2putz12za2zz__hashz00(obj_t
		BgL_envz00_5448, obj_t BgL_tablez00_5449, obj_t BgL_keyz00_5450,
		obj_t BgL_objz00_5451)
	{
		{	/* Llib/hash.scm 777 */
			{	/* Llib/hash.scm 779 */
				obj_t BgL_auxz00_7510;
				obj_t BgL_auxz00_7503;

				if (STRINGP(BgL_keyz00_5450))
					{	/* Llib/hash.scm 779 */
						BgL_auxz00_7510 = BgL_keyz00_5450;
					}
				else
					{
						obj_t BgL_auxz00_7513;

						BgL_auxz00_7513 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(31346L), BGl_string2594z00zz__hashz00,
							BGl_string2588z00zz__hashz00, BgL_keyz00_5450);
						FAILURE(BgL_auxz00_7513, BFALSE, BFALSE);
					}
				if (STRUCTP(BgL_tablez00_5449))
					{	/* Llib/hash.scm 779 */
						BgL_auxz00_7503 = BgL_tablez00_5449;
					}
				else
					{
						obj_t BgL_auxz00_7506;

						BgL_auxz00_7506 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(31346L), BGl_string2594z00zz__hashz00,
							BGl_string2572z00zz__hashz00, BgL_tablez00_5449);
						FAILURE(BgL_auxz00_7506, BFALSE, BFALSE);
					}
				return
					BGl_openzd2stringzd2hashtablezd2putz12zc0zz__hashz00(BgL_auxz00_7503,
					BgL_auxz00_7510, BgL_objz00_5451);
			}
		}

	}



/* open-string-hashtable-put/hash! */
	obj_t BGl_openzd2stringzd2hashtablezd2putzf2hashz12z32zz__hashz00(obj_t
		BgL_tz00_112, obj_t BgL_keyz00_113, obj_t BgL_valz00_114,
		obj_t BgL_hashz00_115)
	{
		{	/* Llib/hash.scm 784 */
		BGl_openzd2stringzd2hashtablezd2putzf2hashz12z32zz__hashz00:
			{	/* Llib/hash.scm 785 */
				obj_t BgL_siza7eza7_2065;
				obj_t BgL_bucketsz00_2066;

				BgL_siza7eza7_2065 = STRUCT_REF(BgL_tz00_112, (int) (1L));
				BgL_bucketsz00_2066 = STRUCT_REF(BgL_tz00_112, (int) (2L));
				{	/* Llib/hash.scm 787 */
					long BgL_g1071z00_2067;

					{	/* Llib/hash.scm 787 */
						long BgL_n1z00_4147;
						long BgL_n2z00_4148;

						BgL_n1z00_4147 = (long) CINT(BgL_hashz00_115);
						BgL_n2z00_4148 = (long) CINT(BgL_siza7eza7_2065);
						{	/* Llib/hash.scm 787 */
							bool_t BgL_test2960z00_7524;

							{	/* Llib/hash.scm 787 */
								long BgL_arg2151z00_4150;

								BgL_arg2151z00_4150 =
									(((BgL_n1z00_4147) | (BgL_n2z00_4148)) & -2147483648);
								BgL_test2960z00_7524 = (BgL_arg2151z00_4150 == 0L);
							}
							if (BgL_test2960z00_7524)
								{	/* Llib/hash.scm 787 */
									int32_t BgL_arg2148z00_4151;

									{	/* Llib/hash.scm 787 */
										int32_t BgL_arg2149z00_4152;
										int32_t BgL_arg2150z00_4153;

										BgL_arg2149z00_4152 = (int32_t) (BgL_n1z00_4147);
										BgL_arg2150z00_4153 = (int32_t) (BgL_n2z00_4148);
										BgL_arg2148z00_4151 =
											(BgL_arg2149z00_4152 % BgL_arg2150z00_4153);
									}
									{	/* Llib/hash.scm 787 */
										long BgL_arg2252z00_4158;

										BgL_arg2252z00_4158 = (long) (BgL_arg2148z00_4151);
										BgL_g1071z00_2067 = (long) (BgL_arg2252z00_4158);
								}}
							else
								{	/* Llib/hash.scm 787 */
									BgL_g1071z00_2067 = (BgL_n1z00_4147 % BgL_n2z00_4148);
								}
						}
					}
					{
						long BgL_offz00_2069;
						long BgL_iz00_2070;

						BgL_offz00_2069 = BgL_g1071z00_2067;
						BgL_iz00_2070 = 1L;
					BgL_zc3z04anonymousza31650ze3z87_2071:
						{	/* Llib/hash.scm 789 */
							long BgL_off3z00_2072;

							BgL_off3z00_2072 = (BgL_offz00_2069 * 3L);
							if (CBOOL(VECTOR_REF(
										((obj_t) BgL_bucketsz00_2066), BgL_off3z00_2072)))
								{	/* Llib/hash.scm 797 */
									bool_t BgL_test2962z00_7538;

									{	/* Llib/hash.scm 797 */
										obj_t BgL_arg1676z00_2091;

										BgL_arg1676z00_2091 =
											VECTOR_REF(
											((obj_t) BgL_bucketsz00_2066), BgL_off3z00_2072);
										{	/* Llib/hash.scm 797 */
											long BgL_l1z00_4167;

											BgL_l1z00_4167 =
												STRING_LENGTH(((obj_t) BgL_arg1676z00_2091));
											if (
												(BgL_l1z00_4167 ==
													STRING_LENGTH(((obj_t) BgL_keyz00_113))))
												{	/* Llib/hash.scm 797 */
													int BgL_arg2041z00_4170;

													{	/* Llib/hash.scm 797 */
														char *BgL_auxz00_7550;
														char *BgL_tmpz00_7547;

														BgL_auxz00_7550 =
															BSTRING_TO_STRING(((obj_t) BgL_keyz00_113));
														BgL_tmpz00_7547 =
															BSTRING_TO_STRING(((obj_t) BgL_arg1676z00_2091));
														BgL_arg2041z00_4170 =
															memcmp(BgL_tmpz00_7547, BgL_auxz00_7550,
															BgL_l1z00_4167);
													}
													BgL_test2962z00_7538 =
														((long) (BgL_arg2041z00_4170) == 0L);
												}
											else
												{	/* Llib/hash.scm 797 */
													BgL_test2962z00_7538 = ((bool_t) 0);
												}
										}
									}
									if (BgL_test2962z00_7538)
										{	/* Llib/hash.scm 797 */
											{	/* Llib/hash.scm 799 */
												long BgL_arg1654z00_2076;

												BgL_arg1654z00_2076 = (BgL_off3z00_2072 + 1L);
												VECTOR_SET(
													((obj_t) BgL_bucketsz00_2066), BgL_arg1654z00_2076,
													BgL_valz00_114);
											}
											{	/* Llib/hash.scm 800 */
												long BgL_arg1656z00_2077;

												BgL_arg1656z00_2077 = (BgL_off3z00_2072 + 2L);
												return
													VECTOR_SET(
													((obj_t) BgL_bucketsz00_2066), BgL_arg1656z00_2077,
													BgL_hashz00_115);
											}
										}
									else
										{	/* Llib/hash.scm 801 */
											bool_t BgL_test2964z00_7562;

											if ((BgL_iz00_2070 >= 5L))
												{	/* Llib/hash.scm 801 */
													BgL_test2964z00_7562 =
														(
														(long) CINT(STRUCT_REF(BgL_tz00_112,
																(int) (1L))) < 8388608L);
												}
											else
												{	/* Llib/hash.scm 801 */
													BgL_test2964z00_7562 = ((bool_t) 0);
												}
											if (BgL_test2964z00_7562)
												{	/* Llib/hash.scm 801 */
													BGl_openzd2stringzd2hashtablezd2rehashz12zc0zz__hashz00
														(BgL_tz00_112);
													{

														goto
															BGl_openzd2stringzd2hashtablezd2putzf2hashz12z32zz__hashz00;
													}
												}
											else
												{	/* Llib/hash.scm 809 */
													long BgL_noffz00_2082;

													BgL_noffz00_2082 =
														(BgL_offz00_2069 + (BgL_iz00_2070 * BgL_iz00_2070));
													if (
														(BgL_noffz00_2082 >=
															(long) CINT(BgL_siza7eza7_2065)))
														{	/* Llib/hash.scm 811 */
															long BgL_arg1664z00_2084;
															long BgL_arg1667z00_2085;

															{	/* Llib/hash.scm 811 */
																long BgL_n1z00_4192;
																long BgL_n2z00_4193;

																BgL_n1z00_4192 = BgL_noffz00_2082;
																BgL_n2z00_4193 =
																	(long) CINT(BgL_siza7eza7_2065);
																{	/* Llib/hash.scm 811 */
																	bool_t BgL_test2967z00_7576;

																	{	/* Llib/hash.scm 811 */
																		long BgL_arg2151z00_4195;

																		BgL_arg2151z00_4195 =
																			(((BgL_n1z00_4192) | (BgL_n2z00_4193)) &
																			-2147483648);
																		BgL_test2967z00_7576 =
																			(BgL_arg2151z00_4195 == 0L);
																	}
																	if (BgL_test2967z00_7576)
																		{	/* Llib/hash.scm 811 */
																			int32_t BgL_arg2148z00_4196;

																			{	/* Llib/hash.scm 811 */
																				int32_t BgL_arg2149z00_4197;
																				int32_t BgL_arg2150z00_4198;

																				BgL_arg2149z00_4197 =
																					(int32_t) (BgL_n1z00_4192);
																				BgL_arg2150z00_4198 =
																					(int32_t) (BgL_n2z00_4193);
																				BgL_arg2148z00_4196 =
																					(BgL_arg2149z00_4197 %
																					BgL_arg2150z00_4198);
																			}
																			{	/* Llib/hash.scm 811 */
																				long BgL_arg2252z00_4203;

																				BgL_arg2252z00_4203 =
																					(long) (BgL_arg2148z00_4196);
																				BgL_arg1664z00_2084 =
																					(long) (BgL_arg2252z00_4203);
																		}}
																	else
																		{	/* Llib/hash.scm 811 */
																			BgL_arg1664z00_2084 =
																				(BgL_n1z00_4192 % BgL_n2z00_4193);
																		}
																}
															}
															BgL_arg1667z00_2085 = (BgL_iz00_2070 + 1L);
															{
																long BgL_iz00_7587;
																long BgL_offz00_7586;

																BgL_offz00_7586 = BgL_arg1664z00_2084;
																BgL_iz00_7587 = BgL_arg1667z00_2085;
																BgL_iz00_2070 = BgL_iz00_7587;
																BgL_offz00_2069 = BgL_offz00_7586;
																goto BgL_zc3z04anonymousza31650ze3z87_2071;
															}
														}
													else
														{
															long BgL_iz00_7589;
															long BgL_offz00_7588;

															BgL_offz00_7588 = BgL_noffz00_2082;
															BgL_iz00_7589 = (BgL_iz00_2070 + 1L);
															BgL_iz00_2070 = BgL_iz00_7589;
															BgL_offz00_2069 = BgL_offz00_7588;
															goto BgL_zc3z04anonymousza31650ze3z87_2071;
														}
												}
										}
								}
							else
								{	/* Llib/hash.scm 791 */
									VECTOR_SET(
										((obj_t) BgL_bucketsz00_2066), BgL_off3z00_2072,
										BgL_keyz00_113);
									{	/* Llib/hash.scm 794 */
										long BgL_arg1678z00_2092;

										BgL_arg1678z00_2092 = (BgL_off3z00_2072 + 1L);
										VECTOR_SET(
											((obj_t) BgL_bucketsz00_2066), BgL_arg1678z00_2092,
											BgL_valz00_114);
									}
									{	/* Llib/hash.scm 795 */
										long BgL_arg1681z00_2093;

										BgL_arg1681z00_2093 = (BgL_off3z00_2072 + 2L);
										VECTOR_SET(
											((obj_t) BgL_bucketsz00_2066), BgL_arg1681z00_2093,
											BgL_hashz00_115);
									}
									return
										BGl_openzd2stringzd2hashtablezd2siza7ezd2incz12zb5zz__hashz00
										(BgL_tz00_112);
								}
						}
					}
				}
			}
		}

	}



/* string-hashtable-put! */
	BGL_EXPORTED_DEF obj_t BGl_stringzd2hashtablezd2putz12z12zz__hashz00(obj_t
		BgL_tablez00_116, obj_t BgL_keyz00_117, obj_t BgL_objz00_118)
	{
		{	/* Llib/hash.scm 817 */
			{	/* Llib/hash.scm 818 */
				obj_t BgL_bucketsz00_2095;

				BgL_bucketsz00_2095 = STRUCT_REF(BgL_tablez00_116, (int) (2L));
				{	/* Llib/hash.scm 819 */
					long BgL_bucketzd2numzd2_2097;

					{	/* Llib/hash.scm 820 */
						long BgL_arg1710z00_2124;

						{	/* Llib/hash.scm 820 */
							long BgL_arg1711z00_2125;

							BgL_arg1711z00_2125 = STRING_LENGTH(BgL_keyz00_117);
							BgL_arg1710z00_2124 =
								bgl_string_hash(BSTRING_TO_STRING(BgL_keyz00_117),
								(int) (0L), (int) (BgL_arg1711z00_2125));
						}
						{	/* Llib/hash.scm 820 */
							long BgL_n1z00_4218;
							long BgL_n2z00_4219;

							BgL_n1z00_4218 = BgL_arg1710z00_2124;
							BgL_n2z00_4219 = VECTOR_LENGTH(((obj_t) BgL_bucketsz00_2095));
							{	/* Llib/hash.scm 820 */
								bool_t BgL_test2968z00_7609;

								{	/* Llib/hash.scm 820 */
									long BgL_arg2151z00_4221;

									BgL_arg2151z00_4221 =
										(((BgL_n1z00_4218) | (BgL_n2z00_4219)) & -2147483648);
									BgL_test2968z00_7609 = (BgL_arg2151z00_4221 == 0L);
								}
								if (BgL_test2968z00_7609)
									{	/* Llib/hash.scm 820 */
										int32_t BgL_arg2148z00_4222;

										{	/* Llib/hash.scm 820 */
											int32_t BgL_arg2149z00_4223;
											int32_t BgL_arg2150z00_4224;

											BgL_arg2149z00_4223 = (int32_t) (BgL_n1z00_4218);
											BgL_arg2150z00_4224 = (int32_t) (BgL_n2z00_4219);
											BgL_arg2148z00_4222 =
												(BgL_arg2149z00_4223 % BgL_arg2150z00_4224);
										}
										{	/* Llib/hash.scm 820 */
											long BgL_arg2252z00_4229;

											BgL_arg2252z00_4229 = (long) (BgL_arg2148z00_4222);
											BgL_bucketzd2numzd2_2097 = (long) (BgL_arg2252z00_4229);
									}}
								else
									{	/* Llib/hash.scm 820 */
										BgL_bucketzd2numzd2_2097 =
											(BgL_n1z00_4218 % BgL_n2z00_4219);
									}
							}
						}
					}
					{	/* Llib/hash.scm 820 */
						obj_t BgL_bucketz00_2098;

						BgL_bucketz00_2098 =
							VECTOR_REF(
							((obj_t) BgL_bucketsz00_2095), BgL_bucketzd2numzd2_2097);
						{	/* Llib/hash.scm 821 */
							obj_t BgL_maxzd2bucketzd2lenz00_2099;

							BgL_maxzd2bucketzd2lenz00_2099 =
								STRUCT_REF(BgL_tablez00_116, (int) (1L));
							{	/* Llib/hash.scm 822 */

								if (NULLP(BgL_bucketz00_2098))
									{	/* Llib/hash.scm 829 */
										{	/* Llib/hash.scm 831 */
											long BgL_arg1684z00_2101;

											BgL_arg1684z00_2101 =
												(
												(long) CINT(STRUCT_REF(BgL_tablez00_116,
														(int) (0L))) + 1L);
											{	/* Llib/hash.scm 831 */
												obj_t BgL_auxz00_7630;
												int BgL_tmpz00_7628;

												BgL_auxz00_7630 = BINT(BgL_arg1684z00_2101);
												BgL_tmpz00_7628 = (int) (0L);
												STRUCT_SET(BgL_tablez00_116, BgL_tmpz00_7628,
													BgL_auxz00_7630);
										}}
										{	/* Llib/hash.scm 832 */
											obj_t BgL_arg1688z00_2103;

											{	/* Llib/hash.scm 832 */
												obj_t BgL_arg1689z00_2104;

												BgL_arg1689z00_2104 =
													MAKE_YOUNG_PAIR(BgL_keyz00_117, BgL_objz00_118);
												{	/* Llib/hash.scm 832 */
													obj_t BgL_list1690z00_2105;

													BgL_list1690z00_2105 =
														MAKE_YOUNG_PAIR(BgL_arg1689z00_2104, BNIL);
													BgL_arg1688z00_2103 = BgL_list1690z00_2105;
											}}
											VECTOR_SET(
												((obj_t) BgL_bucketsz00_2095), BgL_bucketzd2numzd2_2097,
												BgL_arg1688z00_2103);
										}
										return BgL_objz00_118;
									}
								else
									{
										obj_t BgL_buckz00_2107;
										long BgL_countz00_2108;

										BgL_buckz00_2107 = BgL_bucketz00_2098;
										BgL_countz00_2108 = 0L;
									BgL_zc3z04anonymousza31691ze3z87_2109:
										if (NULLP(BgL_buckz00_2107))
											{	/* Llib/hash.scm 837 */
												{	/* Llib/hash.scm 838 */
													long BgL_arg1699z00_2111;

													BgL_arg1699z00_2111 =
														(
														(long) CINT(STRUCT_REF(BgL_tablez00_116,
																(int) (0L))) + 1L);
													{	/* Llib/hash.scm 838 */
														obj_t BgL_auxz00_7645;
														int BgL_tmpz00_7643;

														BgL_auxz00_7645 = BINT(BgL_arg1699z00_2111);
														BgL_tmpz00_7643 = (int) (0L);
														STRUCT_SET(BgL_tablez00_116, BgL_tmpz00_7643,
															BgL_auxz00_7645);
												}}
												{	/* Llib/hash.scm 839 */
													obj_t BgL_arg1701z00_2113;

													{	/* Llib/hash.scm 839 */
														obj_t BgL_arg1702z00_2114;

														BgL_arg1702z00_2114 =
															MAKE_YOUNG_PAIR(BgL_keyz00_117, BgL_objz00_118);
														BgL_arg1701z00_2113 =
															MAKE_YOUNG_PAIR(BgL_arg1702z00_2114,
															BgL_bucketz00_2098);
													}
													VECTOR_SET(
														((obj_t) BgL_bucketsz00_2095),
														BgL_bucketzd2numzd2_2097, BgL_arg1701z00_2113);
												}
												if (
													(BgL_countz00_2108 >
														(long) CINT(BgL_maxzd2bucketzd2lenz00_2099)))
													{	/* Llib/hash.scm 840 */
														BGl_plainzd2hashtablezd2expandz12z12zz__hashz00
															(BgL_tablez00_116);
													}
												else
													{	/* Llib/hash.scm 840 */
														BFALSE;
													}
												return BgL_objz00_118;
											}
										else
											{	/* Llib/hash.scm 843 */
												bool_t BgL_test2972z00_7656;

												{	/* Llib/hash.scm 843 */
													obj_t BgL_arg1709z00_2122;

													{	/* Llib/hash.scm 843 */
														obj_t BgL_pairz00_4250;

														BgL_pairz00_4250 = CAR(((obj_t) BgL_buckz00_2107));
														BgL_arg1709z00_2122 = CAR(BgL_pairz00_4250);
													}
													{	/* Llib/hash.scm 843 */
														long BgL_l1z00_4253;

														BgL_l1z00_4253 =
															STRING_LENGTH(((obj_t) BgL_arg1709z00_2122));
														if (
															(BgL_l1z00_4253 == STRING_LENGTH(BgL_keyz00_117)))
															{	/* Llib/hash.scm 843 */
																int BgL_arg2041z00_4256;

																{	/* Llib/hash.scm 843 */
																	char *BgL_auxz00_7668;
																	char *BgL_tmpz00_7665;

																	BgL_auxz00_7668 =
																		BSTRING_TO_STRING(BgL_keyz00_117);
																	BgL_tmpz00_7665 =
																		BSTRING_TO_STRING(
																		((obj_t) BgL_arg1709z00_2122));
																	BgL_arg2041z00_4256 =
																		memcmp(BgL_tmpz00_7665, BgL_auxz00_7668,
																		BgL_l1z00_4253);
																}
																BgL_test2972z00_7656 =
																	((long) (BgL_arg2041z00_4256) == 0L);
															}
														else
															{	/* Llib/hash.scm 843 */
																BgL_test2972z00_7656 = ((bool_t) 0);
															}
													}
												}
												if (BgL_test2972z00_7656)
													{	/* Llib/hash.scm 844 */
														obj_t BgL_oldzd2objzd2_2118;

														{	/* Llib/hash.scm 844 */
															obj_t BgL_pairz00_4265;

															BgL_pairz00_4265 =
																CAR(((obj_t) BgL_buckz00_2107));
															BgL_oldzd2objzd2_2118 = CDR(BgL_pairz00_4265);
														}
														{	/* Llib/hash.scm 845 */
															obj_t BgL_arg1706z00_2119;

															BgL_arg1706z00_2119 =
																CAR(((obj_t) BgL_buckz00_2107));
															{	/* Llib/hash.scm 845 */
																obj_t BgL_tmpz00_7678;

																BgL_tmpz00_7678 = ((obj_t) BgL_arg1706z00_2119);
																SET_CDR(BgL_tmpz00_7678, BgL_objz00_118);
															}
														}
														return BgL_oldzd2objzd2_2118;
													}
												else
													{	/* Llib/hash.scm 848 */
														obj_t BgL_arg1707z00_2120;
														long BgL_arg1708z00_2121;

														BgL_arg1707z00_2120 =
															CDR(((obj_t) BgL_buckz00_2107));
														BgL_arg1708z00_2121 = (BgL_countz00_2108 + 1L);
														{
															long BgL_countz00_7685;
															obj_t BgL_buckz00_7684;

															BgL_buckz00_7684 = BgL_arg1707z00_2120;
															BgL_countz00_7685 = BgL_arg1708z00_2121;
															BgL_countz00_2108 = BgL_countz00_7685;
															BgL_buckz00_2107 = BgL_buckz00_7684;
															goto BgL_zc3z04anonymousza31691ze3z87_2109;
														}
													}
											}
									}
							}
						}
					}
				}
			}
		}

	}



/* &string-hashtable-put! */
	obj_t BGl_z62stringzd2hashtablezd2putz12z70zz__hashz00(obj_t BgL_envz00_5452,
		obj_t BgL_tablez00_5453, obj_t BgL_keyz00_5454, obj_t BgL_objz00_5455)
	{
		{	/* Llib/hash.scm 817 */
			{	/* Llib/hash.scm 818 */
				obj_t BgL_auxz00_7693;
				obj_t BgL_auxz00_7686;

				if (STRINGP(BgL_keyz00_5454))
					{	/* Llib/hash.scm 818 */
						BgL_auxz00_7693 = BgL_keyz00_5454;
					}
				else
					{
						obj_t BgL_auxz00_7696;

						BgL_auxz00_7696 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(32862L), BGl_string2595z00zz__hashz00,
							BGl_string2588z00zz__hashz00, BgL_keyz00_5454);
						FAILURE(BgL_auxz00_7696, BFALSE, BFALSE);
					}
				if (STRUCTP(BgL_tablez00_5453))
					{	/* Llib/hash.scm 818 */
						BgL_auxz00_7686 = BgL_tablez00_5453;
					}
				else
					{
						obj_t BgL_auxz00_7689;

						BgL_auxz00_7689 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(32862L), BGl_string2595z00zz__hashz00,
							BGl_string2572z00zz__hashz00, BgL_tablez00_5453);
						FAILURE(BgL_auxz00_7689, BFALSE, BFALSE);
					}
				return
					BGl_stringzd2hashtablezd2putz12z12zz__hashz00(BgL_auxz00_7686,
					BgL_auxz00_7693, BgL_objz00_5455);
			}
		}

	}



/* plain-hashtable-put! */
	obj_t BGl_plainzd2hashtablezd2putz12z12zz__hashz00(obj_t BgL_tablez00_119,
		obj_t BgL_keyz00_120, obj_t BgL_objz00_121)
	{
		{	/* Llib/hash.scm 853 */
			{	/* Llib/hash.scm 854 */
				obj_t BgL_bucketsz00_2126;

				BgL_bucketsz00_2126 = STRUCT_REF(BgL_tablez00_119, (int) (2L));
				{	/* Llib/hash.scm 855 */
					long BgL_bucketzd2numzd2_2128;

					{	/* Llib/hash.scm 856 */
						long BgL_arg1734z00_2155;

						{	/* Llib/hash.scm 856 */
							obj_t BgL_hashnz00_4272;

							BgL_hashnz00_4272 = STRUCT_REF(BgL_tablez00_119, (int) (4L));
							if (PROCEDUREP(BgL_hashnz00_4272))
								{	/* Llib/hash.scm 856 */
									obj_t BgL_arg1318z00_4274;

									BgL_arg1318z00_4274 =
										BGL_PROCEDURE_CALL1(BgL_hashnz00_4272, BgL_keyz00_120);
									{	/* Llib/hash.scm 856 */
										long BgL_nz00_4276;

										BgL_nz00_4276 = (long) CINT(BgL_arg1318z00_4274);
										if ((BgL_nz00_4276 < 0L))
											{	/* Llib/hash.scm 856 */
												BgL_arg1734z00_2155 = NEG(BgL_nz00_4276);
											}
										else
											{	/* Llib/hash.scm 856 */
												BgL_arg1734z00_2155 = BgL_nz00_4276;
											}
									}
								}
							else
								{	/* Llib/hash.scm 856 */
									if ((BgL_hashnz00_4272 == BGl_symbol2567z00zz__hashz00))
										{	/* Llib/hash.scm 856 */
											BgL_arg1734z00_2155 =
												BGl_objzd2hashze70z35zz__hashz00(BgL_keyz00_120);
										}
									else
										{	/* Llib/hash.scm 856 */
											BgL_arg1734z00_2155 =
												BGl_getzd2hashnumberzd2zz__hashz00(BgL_keyz00_120);
										}
								}
						}
						{	/* Llib/hash.scm 856 */
							long BgL_n1z00_4280;
							long BgL_n2z00_4281;

							BgL_n1z00_4280 = BgL_arg1734z00_2155;
							BgL_n2z00_4281 = VECTOR_LENGTH(((obj_t) BgL_bucketsz00_2126));
							{	/* Llib/hash.scm 856 */
								bool_t BgL_test2979z00_7721;

								{	/* Llib/hash.scm 856 */
									long BgL_arg2151z00_4283;

									BgL_arg2151z00_4283 =
										(((BgL_n1z00_4280) | (BgL_n2z00_4281)) & -2147483648);
									BgL_test2979z00_7721 = (BgL_arg2151z00_4283 == 0L);
								}
								if (BgL_test2979z00_7721)
									{	/* Llib/hash.scm 856 */
										int32_t BgL_arg2148z00_4284;

										{	/* Llib/hash.scm 856 */
											int32_t BgL_arg2149z00_4285;
											int32_t BgL_arg2150z00_4286;

											BgL_arg2149z00_4285 = (int32_t) (BgL_n1z00_4280);
											BgL_arg2150z00_4286 = (int32_t) (BgL_n2z00_4281);
											BgL_arg2148z00_4284 =
												(BgL_arg2149z00_4285 % BgL_arg2150z00_4286);
										}
										{	/* Llib/hash.scm 856 */
											long BgL_arg2252z00_4291;

											BgL_arg2252z00_4291 = (long) (BgL_arg2148z00_4284);
											BgL_bucketzd2numzd2_2128 = (long) (BgL_arg2252z00_4291);
									}}
								else
									{	/* Llib/hash.scm 856 */
										BgL_bucketzd2numzd2_2128 =
											(BgL_n1z00_4280 % BgL_n2z00_4281);
									}
							}
						}
					}
					{	/* Llib/hash.scm 856 */
						obj_t BgL_bucketz00_2129;

						BgL_bucketz00_2129 =
							VECTOR_REF(
							((obj_t) BgL_bucketsz00_2126), BgL_bucketzd2numzd2_2128);
						{	/* Llib/hash.scm 857 */
							obj_t BgL_maxzd2bucketzd2lenz00_2130;

							BgL_maxzd2bucketzd2lenz00_2130 =
								STRUCT_REF(BgL_tablez00_119, (int) (1L));
							{	/* Llib/hash.scm 858 */

								if (NULLP(BgL_bucketz00_2129))
									{	/* Llib/hash.scm 859 */
										{	/* Llib/hash.scm 861 */
											long BgL_arg1714z00_2132;

											BgL_arg1714z00_2132 =
												(
												(long) CINT(STRUCT_REF(BgL_tablez00_119,
														(int) (0L))) + 1L);
											{	/* Llib/hash.scm 861 */
												obj_t BgL_auxz00_7742;
												int BgL_tmpz00_7740;

												BgL_auxz00_7742 = BINT(BgL_arg1714z00_2132);
												BgL_tmpz00_7740 = (int) (0L);
												STRUCT_SET(BgL_tablez00_119, BgL_tmpz00_7740,
													BgL_auxz00_7742);
										}}
										{	/* Llib/hash.scm 862 */
											obj_t BgL_arg1717z00_2134;

											{	/* Llib/hash.scm 862 */
												obj_t BgL_arg1718z00_2135;

												BgL_arg1718z00_2135 =
													MAKE_YOUNG_PAIR(BgL_keyz00_120, BgL_objz00_121);
												{	/* Llib/hash.scm 862 */
													obj_t BgL_list1719z00_2136;

													BgL_list1719z00_2136 =
														MAKE_YOUNG_PAIR(BgL_arg1718z00_2135, BNIL);
													BgL_arg1717z00_2134 = BgL_list1719z00_2136;
											}}
											VECTOR_SET(
												((obj_t) BgL_bucketsz00_2126), BgL_bucketzd2numzd2_2128,
												BgL_arg1717z00_2134);
										}
										return BgL_objz00_121;
									}
								else
									{
										obj_t BgL_buckz00_2138;
										long BgL_countz00_2139;

										BgL_buckz00_2138 = BgL_bucketz00_2129;
										BgL_countz00_2139 = 0L;
									BgL_zc3z04anonymousza31720ze3z87_2140:
										if (NULLP(BgL_buckz00_2138))
											{	/* Llib/hash.scm 867 */
												{	/* Llib/hash.scm 868 */
													long BgL_arg1722z00_2142;

													BgL_arg1722z00_2142 =
														(
														(long) CINT(STRUCT_REF(BgL_tablez00_119,
																(int) (0L))) + 1L);
													{	/* Llib/hash.scm 868 */
														obj_t BgL_auxz00_7757;
														int BgL_tmpz00_7755;

														BgL_auxz00_7757 = BINT(BgL_arg1722z00_2142);
														BgL_tmpz00_7755 = (int) (0L);
														STRUCT_SET(BgL_tablez00_119, BgL_tmpz00_7755,
															BgL_auxz00_7757);
												}}
												{	/* Llib/hash.scm 869 */
													obj_t BgL_arg1724z00_2144;

													{	/* Llib/hash.scm 869 */
														obj_t BgL_arg1725z00_2145;

														BgL_arg1725z00_2145 =
															MAKE_YOUNG_PAIR(BgL_keyz00_120, BgL_objz00_121);
														BgL_arg1724z00_2144 =
															MAKE_YOUNG_PAIR(BgL_arg1725z00_2145,
															BgL_bucketz00_2129);
													}
													VECTOR_SET(
														((obj_t) BgL_bucketsz00_2126),
														BgL_bucketzd2numzd2_2128, BgL_arg1724z00_2144);
												}
												if (
													(BgL_countz00_2139 >
														(long) CINT(BgL_maxzd2bucketzd2lenz00_2130)))
													{	/* Llib/hash.scm 870 */
														BGl_plainzd2hashtablezd2expandz12z12zz__hashz00
															(BgL_tablez00_119);
													}
												else
													{	/* Llib/hash.scm 870 */
														BFALSE;
													}
												return BgL_objz00_121;
											}
										else
											{	/* Llib/hash.scm 873 */
												bool_t BgL_test2983z00_7768;

												{	/* Llib/hash.scm 873 */
													obj_t BgL_arg1733z00_2153;

													{	/* Llib/hash.scm 873 */
														obj_t BgL_pairz00_4312;

														BgL_pairz00_4312 = CAR(((obj_t) BgL_buckz00_2138));
														BgL_arg1733z00_2153 = CAR(BgL_pairz00_4312);
													}
													{	/* Llib/hash.scm 873 */
														obj_t BgL_eqtz00_4313;

														BgL_eqtz00_4313 =
															STRUCT_REF(BgL_tablez00_119, (int) (3L));
														if (PROCEDUREP(BgL_eqtz00_4313))
															{	/* Llib/hash.scm 873 */
																BgL_test2983z00_7768 =
																	CBOOL(BGL_PROCEDURE_CALL2(BgL_eqtz00_4313,
																		BgL_arg1733z00_2153, BgL_keyz00_120));
															}
														else
															{	/* Llib/hash.scm 873 */
																if ((BgL_arg1733z00_2153 == BgL_keyz00_120))
																	{	/* Llib/hash.scm 873 */
																		BgL_test2983z00_7768 = ((bool_t) 1);
																	}
																else
																	{	/* Llib/hash.scm 873 */
																		if (STRINGP(BgL_arg1733z00_2153))
																			{	/* Llib/hash.scm 873 */
																				if (STRINGP(BgL_keyz00_120))
																					{	/* Llib/hash.scm 873 */
																						long BgL_l1z00_4320;

																						BgL_l1z00_4320 =
																							STRING_LENGTH
																							(BgL_arg1733z00_2153);
																						if ((BgL_l1z00_4320 ==
																								STRING_LENGTH(BgL_keyz00_120)))
																							{	/* Llib/hash.scm 873 */
																								int BgL_arg2041z00_4323;

																								{	/* Llib/hash.scm 873 */
																									char *BgL_auxz00_7794;
																									char *BgL_tmpz00_7792;

																									BgL_auxz00_7794 =
																										BSTRING_TO_STRING
																										(BgL_keyz00_120);
																									BgL_tmpz00_7792 =
																										BSTRING_TO_STRING
																										(BgL_arg1733z00_2153);
																									BgL_arg2041z00_4323 =
																										memcmp(BgL_tmpz00_7792,
																										BgL_auxz00_7794,
																										BgL_l1z00_4320);
																								}
																								BgL_test2983z00_7768 =
																									(
																									(long) (BgL_arg2041z00_4323)
																									== 0L);
																							}
																						else
																							{	/* Llib/hash.scm 873 */
																								BgL_test2983z00_7768 =
																									((bool_t) 0);
																							}
																					}
																				else
																					{	/* Llib/hash.scm 873 */
																						BgL_test2983z00_7768 = ((bool_t) 0);
																					}
																			}
																		else
																			{	/* Llib/hash.scm 873 */
																				BgL_test2983z00_7768 = ((bool_t) 0);
																			}
																	}
															}
													}
												}
												if (BgL_test2983z00_7768)
													{	/* Llib/hash.scm 874 */
														obj_t BgL_oldzd2objzd2_2149;

														{	/* Llib/hash.scm 874 */
															obj_t BgL_pairz00_4332;

															BgL_pairz00_4332 =
																CAR(((obj_t) BgL_buckz00_2138));
															BgL_oldzd2objzd2_2149 = CDR(BgL_pairz00_4332);
														}
														{	/* Llib/hash.scm 875 */
															obj_t BgL_arg1729z00_2150;

															BgL_arg1729z00_2150 =
																CAR(((obj_t) BgL_buckz00_2138));
															{	/* Llib/hash.scm 875 */
																obj_t BgL_tmpz00_7804;

																BgL_tmpz00_7804 = ((obj_t) BgL_arg1729z00_2150);
																SET_CDR(BgL_tmpz00_7804, BgL_objz00_121);
															}
														}
														return BgL_oldzd2objzd2_2149;
													}
												else
													{	/* Llib/hash.scm 878 */
														obj_t BgL_arg1730z00_2151;
														long BgL_arg1731z00_2152;

														BgL_arg1730z00_2151 =
															CDR(((obj_t) BgL_buckz00_2138));
														BgL_arg1731z00_2152 = (BgL_countz00_2139 + 1L);
														{
															long BgL_countz00_7811;
															obj_t BgL_buckz00_7810;

															BgL_buckz00_7810 = BgL_arg1730z00_2151;
															BgL_countz00_7811 = BgL_arg1731z00_2152;
															BgL_countz00_2139 = BgL_countz00_7811;
															BgL_buckz00_2138 = BgL_buckz00_7810;
															goto BgL_zc3z04anonymousza31720ze3z87_2140;
														}
													}
											}
									}
							}
						}
					}
				}
			}
		}

	}



/* hashtable-update! */
	BGL_EXPORTED_DEF obj_t BGl_hashtablezd2updatez12zc0zz__hashz00(obj_t
		BgL_tablez00_122, obj_t BgL_keyz00_123, obj_t BgL_procz00_124,
		obj_t BgL_objz00_125)
	{
		{	/* Llib/hash.scm 883 */
			{	/* Llib/hash.scm 885 */
				bool_t BgL_test2989z00_7812;

				if (
					(0L == ((long) CINT(STRUCT_REF(BgL_tablez00_122, (int) (5L))) & 8L)))
					{	/* Llib/hash.scm 294 */
						BgL_test2989z00_7812 = ((bool_t) 0);
					}
				else
					{	/* Llib/hash.scm 294 */
						BgL_test2989z00_7812 = ((bool_t) 1);
					}
				if (BgL_test2989z00_7812)
					{	/* Llib/hash.scm 885 */
						return
							BGl_openzd2stringzd2hashtablezd2updatez12zc0zz__hashz00
							(BgL_tablez00_122, BgL_keyz00_123, BgL_procz00_124,
							BgL_objz00_125);
					}
				else
					{	/* Llib/hash.scm 887 */
						bool_t BgL_test2991z00_7820;

						if (
							(0L ==
								((long) CINT(STRUCT_REF(BgL_tablez00_122, (int) (5L))) & 3L)))
							{	/* Llib/hash.scm 288 */
								BgL_test2991z00_7820 = ((bool_t) 0);
							}
						else
							{	/* Llib/hash.scm 288 */
								BgL_test2991z00_7820 = ((bool_t) 1);
							}
						if (BgL_test2991z00_7820)
							{	/* Llib/hash.scm 887 */
								BGL_TAIL return
									BGl_weakzd2hashtablezd2updatez12z12zz__weakhashz00
									(BgL_tablez00_122, BgL_keyz00_123, BgL_procz00_124,
									BgL_objz00_125);
							}
						else
							{	/* Llib/hash.scm 887 */
								BGL_TAIL return
									BGl_plainzd2hashtablezd2updatez12z12zz__hashz00
									(BgL_tablez00_122, BgL_keyz00_123, BgL_procz00_124,
									BgL_objz00_125);
							}
					}
			}
		}

	}



/* &hashtable-update! */
	obj_t BGl_z62hashtablezd2updatez12za2zz__hashz00(obj_t BgL_envz00_5456,
		obj_t BgL_tablez00_5457, obj_t BgL_keyz00_5458, obj_t BgL_procz00_5459,
		obj_t BgL_objz00_5460)
	{
		{	/* Llib/hash.scm 883 */
			{	/* Llib/hash.scm 885 */
				obj_t BgL_auxz00_7836;
				obj_t BgL_auxz00_7829;

				if (PROCEDUREP(BgL_procz00_5459))
					{	/* Llib/hash.scm 885 */
						BgL_auxz00_7836 = BgL_procz00_5459;
					}
				else
					{
						obj_t BgL_auxz00_7839;

						BgL_auxz00_7839 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(35429L), BGl_string2596z00zz__hashz00,
							BGl_string2579z00zz__hashz00, BgL_procz00_5459);
						FAILURE(BgL_auxz00_7839, BFALSE, BFALSE);
					}
				if (STRUCTP(BgL_tablez00_5457))
					{	/* Llib/hash.scm 885 */
						BgL_auxz00_7829 = BgL_tablez00_5457;
					}
				else
					{
						obj_t BgL_auxz00_7832;

						BgL_auxz00_7832 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(35429L), BGl_string2596z00zz__hashz00,
							BGl_string2572z00zz__hashz00, BgL_tablez00_5457);
						FAILURE(BgL_auxz00_7832, BFALSE, BFALSE);
					}
				return
					BGl_hashtablezd2updatez12zc0zz__hashz00(BgL_auxz00_7829,
					BgL_keyz00_5458, BgL_auxz00_7836, BgL_objz00_5460);
			}
		}

	}



/* open-string-hashtable-update! */
	BGL_EXPORTED_DEF obj_t
		BGl_openzd2stringzd2hashtablezd2updatez12zc0zz__hashz00(obj_t
		BgL_tablez00_126, obj_t BgL_keyz00_127, obj_t BgL_procz00_128,
		obj_t BgL_objz00_129)
	{
		{	/* Llib/hash.scm 895 */
			{	/* Llib/hash.scm 896 */
				obj_t BgL_siza7eza7_2158;

				BgL_siza7eza7_2158 = STRUCT_REF(BgL_tablez00_126, (int) (1L));
				{	/* Llib/hash.scm 896 */
					obj_t BgL_bucketsz00_2159;

					BgL_bucketsz00_2159 = STRUCT_REF(BgL_tablez00_126, (int) (2L));
					{	/* Llib/hash.scm 897 */
						long BgL_hashz00_2160;

						{	/* Llib/hash.scm 898 */
							long BgL_arg1754z00_2186;

							BgL_arg1754z00_2186 = STRING_LENGTH(BgL_keyz00_127);
							BgL_hashz00_2160 =
								bgl_string_hash(BSTRING_TO_STRING(BgL_keyz00_127),
								(int) (0L), (int) (BgL_arg1754z00_2186));
						}
						{	/* Llib/hash.scm 898 */

							{	/* Llib/hash.scm 900 */
								long BgL_g1072z00_2161;

								{	/* Llib/hash.scm 900 */
									long BgL_n1z00_4356;
									long BgL_n2z00_4357;

									BgL_n1z00_4356 = BgL_hashz00_2160;
									BgL_n2z00_4357 = (long) CINT(BgL_siza7eza7_2158);
									{	/* Llib/hash.scm 900 */
										bool_t BgL_test2995z00_7854;

										{	/* Llib/hash.scm 900 */
											long BgL_arg2151z00_4359;

											BgL_arg2151z00_4359 =
												(((BgL_n1z00_4356) | (BgL_n2z00_4357)) & -2147483648);
											BgL_test2995z00_7854 = (BgL_arg2151z00_4359 == 0L);
										}
										if (BgL_test2995z00_7854)
											{	/* Llib/hash.scm 900 */
												int32_t BgL_arg2148z00_4360;

												{	/* Llib/hash.scm 900 */
													int32_t BgL_arg2149z00_4361;
													int32_t BgL_arg2150z00_4362;

													BgL_arg2149z00_4361 = (int32_t) (BgL_n1z00_4356);
													BgL_arg2150z00_4362 = (int32_t) (BgL_n2z00_4357);
													BgL_arg2148z00_4360 =
														(BgL_arg2149z00_4361 % BgL_arg2150z00_4362);
												}
												{	/* Llib/hash.scm 900 */
													long BgL_arg2252z00_4367;

													BgL_arg2252z00_4367 = (long) (BgL_arg2148z00_4360);
													BgL_g1072z00_2161 = (long) (BgL_arg2252z00_4367);
											}}
										else
											{	/* Llib/hash.scm 900 */
												BgL_g1072z00_2161 = (BgL_n1z00_4356 % BgL_n2z00_4357);
											}
									}
								}
								{
									long BgL_offz00_2163;
									long BgL_iz00_2164;

									BgL_offz00_2163 = BgL_g1072z00_2161;
									BgL_iz00_2164 = 1L;
								BgL_zc3z04anonymousza31737ze3z87_2165:
									{	/* Llib/hash.scm 902 */
										long BgL_off3z00_2166;

										BgL_off3z00_2166 = (BgL_offz00_2163 * 3L);
										if (CBOOL(VECTOR_REF(
													((obj_t) BgL_bucketsz00_2159), BgL_off3z00_2166)))
											{	/* Llib/hash.scm 904 */
												bool_t BgL_test2997z00_7868;

												{	/* Llib/hash.scm 904 */
													obj_t BgL_arg1753z00_2184;

													BgL_arg1753z00_2184 =
														VECTOR_REF(
														((obj_t) BgL_bucketsz00_2159), BgL_off3z00_2166);
													{	/* Llib/hash.scm 904 */
														long BgL_l1z00_4376;

														BgL_l1z00_4376 =
															STRING_LENGTH(((obj_t) BgL_arg1753z00_2184));
														if (
															(BgL_l1z00_4376 == STRING_LENGTH(BgL_keyz00_127)))
															{	/* Llib/hash.scm 904 */
																int BgL_arg2041z00_4379;

																{	/* Llib/hash.scm 904 */
																	char *BgL_auxz00_7879;
																	char *BgL_tmpz00_7876;

																	BgL_auxz00_7879 =
																		BSTRING_TO_STRING(BgL_keyz00_127);
																	BgL_tmpz00_7876 =
																		BSTRING_TO_STRING(
																		((obj_t) BgL_arg1753z00_2184));
																	BgL_arg2041z00_4379 =
																		memcmp(BgL_tmpz00_7876, BgL_auxz00_7879,
																		BgL_l1z00_4376);
																}
																BgL_test2997z00_7868 =
																	((long) (BgL_arg2041z00_4379) == 0L);
															}
														else
															{	/* Llib/hash.scm 904 */
																BgL_test2997z00_7868 = ((bool_t) 0);
															}
													}
												}
												if (BgL_test2997z00_7868)
													{	/* Llib/hash.scm 905 */
														bool_t BgL_test2999z00_7884;

														{	/* Llib/hash.scm 905 */
															long BgL_arg1747z00_2177;

															BgL_arg1747z00_2177 = (BgL_off3z00_2166 + 2L);
															BgL_test2999z00_7884 =
																CBOOL(VECTOR_REF(
																	((obj_t) BgL_bucketsz00_2159),
																	BgL_arg1747z00_2177));
														}
														if (BgL_test2999z00_7884)
															{	/* Llib/hash.scm 906 */
																obj_t BgL_ovalz00_2172;

																{	/* Llib/hash.scm 906 */
																	long BgL_arg1745z00_2175;

																	BgL_arg1745z00_2175 = (BgL_off3z00_2166 + 1L);
																	BgL_ovalz00_2172 =
																		VECTOR_REF(
																		((obj_t) BgL_bucketsz00_2159),
																		BgL_arg1745z00_2175);
																}
																{	/* Llib/hash.scm 907 */
																	long BgL_arg1743z00_2173;
																	obj_t BgL_arg1744z00_2174;

																	BgL_arg1743z00_2173 = (BgL_off3z00_2166 + 1L);
																	BgL_arg1744z00_2174 =
																		BGL_PROCEDURE_CALL1(BgL_procz00_128,
																		BgL_ovalz00_2172);
																	return VECTOR_SET(((obj_t)
																			BgL_bucketsz00_2159), BgL_arg1743z00_2173,
																		BgL_arg1744z00_2174);
																}
															}
														else
															{	/* Llib/hash.scm 908 */
																long BgL_arg1746z00_2176;

																BgL_arg1746z00_2176 = (BgL_off3z00_2166 + 1L);
																return
																	VECTOR_SET(
																	((obj_t) BgL_bucketsz00_2159),
																	BgL_arg1746z00_2176, BgL_objz00_129);
															}
													}
												else
													{	/* Llib/hash.scm 909 */
														long BgL_noffz00_2178;

														BgL_noffz00_2178 =
															(BgL_offz00_2163 +
															(BgL_iz00_2164 * BgL_iz00_2164));
														if (
															(BgL_noffz00_2178 >=
																(long) CINT(BgL_siza7eza7_2158)))
															{	/* Llib/hash.scm 911 */
																long BgL_arg1749z00_2180;
																long BgL_arg1750z00_2181;

																{	/* Llib/hash.scm 911 */
																	long BgL_n1z00_4403;
																	long BgL_n2z00_4404;

																	BgL_n1z00_4403 = BgL_noffz00_2178;
																	BgL_n2z00_4404 =
																		(long) CINT(BgL_siza7eza7_2158);
																	{	/* Llib/hash.scm 911 */
																		bool_t BgL_test3001z00_7908;

																		{	/* Llib/hash.scm 911 */
																			long BgL_arg2151z00_4406;

																			BgL_arg2151z00_4406 =
																				(((BgL_n1z00_4403) | (BgL_n2z00_4404)) &
																				-2147483648);
																			BgL_test3001z00_7908 =
																				(BgL_arg2151z00_4406 == 0L);
																		}
																		if (BgL_test3001z00_7908)
																			{	/* Llib/hash.scm 911 */
																				int32_t BgL_arg2148z00_4407;

																				{	/* Llib/hash.scm 911 */
																					int32_t BgL_arg2149z00_4408;
																					int32_t BgL_arg2150z00_4409;

																					BgL_arg2149z00_4408 =
																						(int32_t) (BgL_n1z00_4403);
																					BgL_arg2150z00_4409 =
																						(int32_t) (BgL_n2z00_4404);
																					BgL_arg2148z00_4407 =
																						(BgL_arg2149z00_4408 %
																						BgL_arg2150z00_4409);
																				}
																				{	/* Llib/hash.scm 911 */
																					long BgL_arg2252z00_4414;

																					BgL_arg2252z00_4414 =
																						(long) (BgL_arg2148z00_4407);
																					BgL_arg1749z00_2180 =
																						(long) (BgL_arg2252z00_4414);
																			}}
																		else
																			{	/* Llib/hash.scm 911 */
																				BgL_arg1749z00_2180 =
																					(BgL_n1z00_4403 % BgL_n2z00_4404);
																			}
																	}
																}
																BgL_arg1750z00_2181 = (BgL_iz00_2164 + 1L);
																{
																	long BgL_iz00_7919;
																	long BgL_offz00_7918;

																	BgL_offz00_7918 = BgL_arg1749z00_2180;
																	BgL_iz00_7919 = BgL_arg1750z00_2181;
																	BgL_iz00_2164 = BgL_iz00_7919;
																	BgL_offz00_2163 = BgL_offz00_7918;
																	goto BgL_zc3z04anonymousza31737ze3z87_2165;
																}
															}
														else
															{
																long BgL_iz00_7921;
																long BgL_offz00_7920;

																BgL_offz00_7920 = BgL_noffz00_2178;
																BgL_iz00_7921 = (BgL_iz00_2164 + 1L);
																BgL_iz00_2164 = BgL_iz00_7921;
																BgL_offz00_2163 = BgL_offz00_7920;
																goto BgL_zc3z04anonymousza31737ze3z87_2165;
															}
													}
											}
										else
											{	/* Llib/hash.scm 903 */
												return
													BGl_openzd2stringzd2hashtablezd2putzf2hashz12z32zz__hashz00
													(BgL_tablez00_126, BgL_keyz00_127, BgL_objz00_129,
													BINT(BgL_hashz00_2160));
											}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &open-string-hashtable-update! */
	obj_t BGl_z62openzd2stringzd2hashtablezd2updatez12za2zz__hashz00(obj_t
		BgL_envz00_5461, obj_t BgL_tablez00_5462, obj_t BgL_keyz00_5463,
		obj_t BgL_procz00_5464, obj_t BgL_objz00_5465)
	{
		{	/* Llib/hash.scm 895 */
			{	/* Llib/hash.scm 896 */
				obj_t BgL_auxz00_7939;
				obj_t BgL_auxz00_7932;
				obj_t BgL_auxz00_7925;

				if (PROCEDUREP(BgL_procz00_5464))
					{	/* Llib/hash.scm 896 */
						BgL_auxz00_7939 = BgL_procz00_5464;
					}
				else
					{
						obj_t BgL_auxz00_7942;

						BgL_auxz00_7942 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(35983L), BGl_string2597z00zz__hashz00,
							BGl_string2579z00zz__hashz00, BgL_procz00_5464);
						FAILURE(BgL_auxz00_7942, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_keyz00_5463))
					{	/* Llib/hash.scm 896 */
						BgL_auxz00_7932 = BgL_keyz00_5463;
					}
				else
					{
						obj_t BgL_auxz00_7935;

						BgL_auxz00_7935 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(35983L), BGl_string2597z00zz__hashz00,
							BGl_string2588z00zz__hashz00, BgL_keyz00_5463);
						FAILURE(BgL_auxz00_7935, BFALSE, BFALSE);
					}
				if (STRUCTP(BgL_tablez00_5462))
					{	/* Llib/hash.scm 896 */
						BgL_auxz00_7925 = BgL_tablez00_5462;
					}
				else
					{
						obj_t BgL_auxz00_7928;

						BgL_auxz00_7928 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(35983L), BGl_string2597z00zz__hashz00,
							BGl_string2572z00zz__hashz00, BgL_tablez00_5462);
						FAILURE(BgL_auxz00_7928, BFALSE, BFALSE);
					}
				return
					BGl_openzd2stringzd2hashtablezd2updatez12zc0zz__hashz00
					(BgL_auxz00_7925, BgL_auxz00_7932, BgL_auxz00_7939, BgL_objz00_5465);
			}
		}

	}



/* plain-hashtable-update! */
	obj_t BGl_plainzd2hashtablezd2updatez12z12zz__hashz00(obj_t BgL_tablez00_130,
		obj_t BgL_keyz00_131, obj_t BgL_procz00_132, obj_t BgL_objz00_133)
	{
		{	/* Llib/hash.scm 918 */
			{	/* Llib/hash.scm 919 */
				obj_t BgL_bucketsz00_2187;

				BgL_bucketsz00_2187 = STRUCT_REF(BgL_tablez00_130, (int) (2L));
				{	/* Llib/hash.scm 920 */
					long BgL_bucketzd2numzd2_2189;

					{	/* Llib/hash.scm 921 */
						long BgL_arg1775z00_2217;

						{	/* Llib/hash.scm 921 */
							obj_t BgL_hashnz00_4420;

							BgL_hashnz00_4420 = STRUCT_REF(BgL_tablez00_130, (int) (4L));
							if (PROCEDUREP(BgL_hashnz00_4420))
								{	/* Llib/hash.scm 921 */
									obj_t BgL_arg1318z00_4422;

									BgL_arg1318z00_4422 =
										BGL_PROCEDURE_CALL1(BgL_hashnz00_4420, BgL_keyz00_131);
									{	/* Llib/hash.scm 921 */
										long BgL_nz00_4424;

										BgL_nz00_4424 = (long) CINT(BgL_arg1318z00_4422);
										if ((BgL_nz00_4424 < 0L))
											{	/* Llib/hash.scm 921 */
												BgL_arg1775z00_2217 = NEG(BgL_nz00_4424);
											}
										else
											{	/* Llib/hash.scm 921 */
												BgL_arg1775z00_2217 = BgL_nz00_4424;
											}
									}
								}
							else
								{	/* Llib/hash.scm 921 */
									if ((BgL_hashnz00_4420 == BGl_symbol2567z00zz__hashz00))
										{	/* Llib/hash.scm 921 */
											BgL_arg1775z00_2217 =
												BGl_objzd2hashze70z35zz__hashz00(BgL_keyz00_131);
										}
									else
										{	/* Llib/hash.scm 921 */
											BgL_arg1775z00_2217 =
												BGl_getzd2hashnumberzd2zz__hashz00(BgL_keyz00_131);
										}
								}
						}
						{	/* Llib/hash.scm 921 */
							long BgL_n1z00_4428;
							long BgL_n2z00_4429;

							BgL_n1z00_4428 = BgL_arg1775z00_2217;
							BgL_n2z00_4429 = VECTOR_LENGTH(((obj_t) BgL_bucketsz00_2187));
							{	/* Llib/hash.scm 921 */
								bool_t BgL_test3008z00_7967;

								{	/* Llib/hash.scm 921 */
									long BgL_arg2151z00_4431;

									BgL_arg2151z00_4431 =
										(((BgL_n1z00_4428) | (BgL_n2z00_4429)) & -2147483648);
									BgL_test3008z00_7967 = (BgL_arg2151z00_4431 == 0L);
								}
								if (BgL_test3008z00_7967)
									{	/* Llib/hash.scm 921 */
										int32_t BgL_arg2148z00_4432;

										{	/* Llib/hash.scm 921 */
											int32_t BgL_arg2149z00_4433;
											int32_t BgL_arg2150z00_4434;

											BgL_arg2149z00_4433 = (int32_t) (BgL_n1z00_4428);
											BgL_arg2150z00_4434 = (int32_t) (BgL_n2z00_4429);
											BgL_arg2148z00_4432 =
												(BgL_arg2149z00_4433 % BgL_arg2150z00_4434);
										}
										{	/* Llib/hash.scm 921 */
											long BgL_arg2252z00_4439;

											BgL_arg2252z00_4439 = (long) (BgL_arg2148z00_4432);
											BgL_bucketzd2numzd2_2189 = (long) (BgL_arg2252z00_4439);
									}}
								else
									{	/* Llib/hash.scm 921 */
										BgL_bucketzd2numzd2_2189 =
											(BgL_n1z00_4428 % BgL_n2z00_4429);
									}
							}
						}
					}
					{	/* Llib/hash.scm 921 */
						obj_t BgL_bucketz00_2190;

						BgL_bucketz00_2190 =
							VECTOR_REF(
							((obj_t) BgL_bucketsz00_2187), BgL_bucketzd2numzd2_2189);
						{	/* Llib/hash.scm 922 */
							obj_t BgL_maxzd2bucketzd2lenz00_2191;

							BgL_maxzd2bucketzd2lenz00_2191 =
								STRUCT_REF(BgL_tablez00_130, (int) (1L));
							{	/* Llib/hash.scm 923 */

								if (NULLP(BgL_bucketz00_2190))
									{	/* Llib/hash.scm 924 */
										{	/* Llib/hash.scm 926 */
											long BgL_arg1756z00_2193;

											BgL_arg1756z00_2193 =
												(
												(long) CINT(STRUCT_REF(BgL_tablez00_130,
														(int) (0L))) + 1L);
											{	/* Llib/hash.scm 926 */
												obj_t BgL_auxz00_7988;
												int BgL_tmpz00_7986;

												BgL_auxz00_7988 = BINT(BgL_arg1756z00_2193);
												BgL_tmpz00_7986 = (int) (0L);
												STRUCT_SET(BgL_tablez00_130, BgL_tmpz00_7986,
													BgL_auxz00_7988);
										}}
										{	/* Llib/hash.scm 927 */
											obj_t BgL_arg1758z00_2195;

											{	/* Llib/hash.scm 927 */
												obj_t BgL_arg1759z00_2196;

												BgL_arg1759z00_2196 =
													MAKE_YOUNG_PAIR(BgL_keyz00_131, BgL_objz00_133);
												{	/* Llib/hash.scm 927 */
													obj_t BgL_list1760z00_2197;

													BgL_list1760z00_2197 =
														MAKE_YOUNG_PAIR(BgL_arg1759z00_2196, BNIL);
													BgL_arg1758z00_2195 = BgL_list1760z00_2197;
											}}
											VECTOR_SET(
												((obj_t) BgL_bucketsz00_2187), BgL_bucketzd2numzd2_2189,
												BgL_arg1758z00_2195);
										}
										return BgL_objz00_133;
									}
								else
									{
										obj_t BgL_buckz00_2199;
										long BgL_countz00_2200;

										BgL_buckz00_2199 = BgL_bucketz00_2190;
										BgL_countz00_2200 = 0L;
									BgL_zc3z04anonymousza31761ze3z87_2201:
										if (NULLP(BgL_buckz00_2199))
											{	/* Llib/hash.scm 932 */
												{	/* Llib/hash.scm 933 */
													long BgL_arg1763z00_2203;

													BgL_arg1763z00_2203 =
														(
														(long) CINT(STRUCT_REF(BgL_tablez00_130,
																(int) (0L))) + 1L);
													{	/* Llib/hash.scm 933 */
														obj_t BgL_auxz00_8003;
														int BgL_tmpz00_8001;

														BgL_auxz00_8003 = BINT(BgL_arg1763z00_2203);
														BgL_tmpz00_8001 = (int) (0L);
														STRUCT_SET(BgL_tablez00_130, BgL_tmpz00_8001,
															BgL_auxz00_8003);
												}}
												{	/* Llib/hash.scm 934 */
													obj_t BgL_arg1765z00_2205;

													{	/* Llib/hash.scm 934 */
														obj_t BgL_arg1766z00_2206;

														BgL_arg1766z00_2206 =
															MAKE_YOUNG_PAIR(BgL_keyz00_131, BgL_objz00_133);
														BgL_arg1765z00_2205 =
															MAKE_YOUNG_PAIR(BgL_arg1766z00_2206,
															BgL_bucketz00_2190);
													}
													VECTOR_SET(
														((obj_t) BgL_bucketsz00_2187),
														BgL_bucketzd2numzd2_2189, BgL_arg1765z00_2205);
												}
												if (
													(BgL_countz00_2200 >
														(long) CINT(BgL_maxzd2bucketzd2lenz00_2191)))
													{	/* Llib/hash.scm 935 */
														BGl_plainzd2hashtablezd2expandz12z12zz__hashz00
															(BgL_tablez00_130);
													}
												else
													{	/* Llib/hash.scm 935 */
														BFALSE;
													}
												return BgL_objz00_133;
											}
										else
											{	/* Llib/hash.scm 938 */
												bool_t BgL_test3012z00_8014;

												{	/* Llib/hash.scm 938 */
													obj_t BgL_arg1774z00_2215;

													{	/* Llib/hash.scm 938 */
														obj_t BgL_pairz00_4460;

														BgL_pairz00_4460 = CAR(((obj_t) BgL_buckz00_2199));
														BgL_arg1774z00_2215 = CAR(BgL_pairz00_4460);
													}
													{	/* Llib/hash.scm 938 */
														obj_t BgL_eqtz00_4461;

														BgL_eqtz00_4461 =
															STRUCT_REF(BgL_tablez00_130, (int) (3L));
														if (PROCEDUREP(BgL_eqtz00_4461))
															{	/* Llib/hash.scm 938 */
																BgL_test3012z00_8014 =
																	CBOOL(BGL_PROCEDURE_CALL2(BgL_eqtz00_4461,
																		BgL_arg1774z00_2215, BgL_keyz00_131));
															}
														else
															{	/* Llib/hash.scm 938 */
																if ((BgL_arg1774z00_2215 == BgL_keyz00_131))
																	{	/* Llib/hash.scm 938 */
																		BgL_test3012z00_8014 = ((bool_t) 1);
																	}
																else
																	{	/* Llib/hash.scm 938 */
																		if (STRINGP(BgL_arg1774z00_2215))
																			{	/* Llib/hash.scm 938 */
																				if (STRINGP(BgL_keyz00_131))
																					{	/* Llib/hash.scm 938 */
																						long BgL_l1z00_4468;

																						BgL_l1z00_4468 =
																							STRING_LENGTH
																							(BgL_arg1774z00_2215);
																						if ((BgL_l1z00_4468 ==
																								STRING_LENGTH(BgL_keyz00_131)))
																							{	/* Llib/hash.scm 938 */
																								int BgL_arg2041z00_4471;

																								{	/* Llib/hash.scm 938 */
																									char *BgL_auxz00_8040;
																									char *BgL_tmpz00_8038;

																									BgL_auxz00_8040 =
																										BSTRING_TO_STRING
																										(BgL_keyz00_131);
																									BgL_tmpz00_8038 =
																										BSTRING_TO_STRING
																										(BgL_arg1774z00_2215);
																									BgL_arg2041z00_4471 =
																										memcmp(BgL_tmpz00_8038,
																										BgL_auxz00_8040,
																										BgL_l1z00_4468);
																								}
																								BgL_test3012z00_8014 =
																									(
																									(long) (BgL_arg2041z00_4471)
																									== 0L);
																							}
																						else
																							{	/* Llib/hash.scm 938 */
																								BgL_test3012z00_8014 =
																									((bool_t) 0);
																							}
																					}
																				else
																					{	/* Llib/hash.scm 938 */
																						BgL_test3012z00_8014 = ((bool_t) 0);
																					}
																			}
																		else
																			{	/* Llib/hash.scm 938 */
																				BgL_test3012z00_8014 = ((bool_t) 0);
																			}
																	}
															}
													}
												}
												if (BgL_test3012z00_8014)
													{	/* Llib/hash.scm 939 */
														obj_t BgL_resz00_2210;

														{	/* Llib/hash.scm 939 */
															obj_t BgL_arg1771z00_2212;

															{	/* Llib/hash.scm 939 */
																obj_t BgL_pairz00_4480;

																BgL_pairz00_4480 =
																	CAR(((obj_t) BgL_buckz00_2199));
																BgL_arg1771z00_2212 = CDR(BgL_pairz00_4480);
															}
															BgL_resz00_2210 =
																BGL_PROCEDURE_CALL1(BgL_procz00_132,
																BgL_arg1771z00_2212);
														}
														{	/* Llib/hash.scm 940 */
															obj_t BgL_arg1770z00_2211;

															BgL_arg1770z00_2211 =
																CAR(((obj_t) BgL_buckz00_2199));
															{	/* Llib/hash.scm 940 */
																obj_t BgL_tmpz00_8054;

																BgL_tmpz00_8054 = ((obj_t) BgL_arg1770z00_2211);
																SET_CDR(BgL_tmpz00_8054, BgL_resz00_2210);
															}
														}
														return BgL_resz00_2210;
													}
												else
													{	/* Llib/hash.scm 943 */
														obj_t BgL_arg1772z00_2213;
														long BgL_arg1773z00_2214;

														BgL_arg1772z00_2213 =
															CDR(((obj_t) BgL_buckz00_2199));
														BgL_arg1773z00_2214 = (BgL_countz00_2200 + 1L);
														{
															long BgL_countz00_8061;
															obj_t BgL_buckz00_8060;

															BgL_buckz00_8060 = BgL_arg1772z00_2213;
															BgL_countz00_8061 = BgL_arg1773z00_2214;
															BgL_countz00_2200 = BgL_countz00_8061;
															BgL_buckz00_2199 = BgL_buckz00_8060;
															goto BgL_zc3z04anonymousza31761ze3z87_2201;
														}
													}
											}
									}
							}
						}
					}
				}
			}
		}

	}



/* hashtable-add! */
	BGL_EXPORTED_DEF obj_t BGl_hashtablezd2addz12zc0zz__hashz00(obj_t
		BgL_tablez00_134, obj_t BgL_keyz00_135, obj_t BgL_p2z00_136,
		obj_t BgL_objz00_137, obj_t BgL_initz00_138)
	{
		{	/* Llib/hash.scm 948 */
			{	/* Llib/hash.scm 950 */
				bool_t BgL_test3018z00_8062;

				if (
					(0L == ((long) CINT(STRUCT_REF(BgL_tablez00_134, (int) (5L))) & 8L)))
					{	/* Llib/hash.scm 294 */
						BgL_test3018z00_8062 = ((bool_t) 0);
					}
				else
					{	/* Llib/hash.scm 294 */
						BgL_test3018z00_8062 = ((bool_t) 1);
					}
				if (BgL_test3018z00_8062)
					{	/* Llib/hash.scm 950 */
						return
							BGl_openzd2stringzd2hashtablezd2addz12zc0zz__hashz00
							(BgL_tablez00_134, BgL_keyz00_135, BgL_p2z00_136, BgL_objz00_137,
							BgL_initz00_138);
					}
				else
					{	/* Llib/hash.scm 952 */
						bool_t BgL_test3020z00_8070;

						if (
							(0L ==
								((long) CINT(STRUCT_REF(BgL_tablez00_134, (int) (5L))) & 3L)))
							{	/* Llib/hash.scm 288 */
								BgL_test3020z00_8070 = ((bool_t) 0);
							}
						else
							{	/* Llib/hash.scm 288 */
								BgL_test3020z00_8070 = ((bool_t) 1);
							}
						if (BgL_test3020z00_8070)
							{	/* Llib/hash.scm 952 */
								BGL_TAIL return
									BGl_weakzd2hashtablezd2addz12z12zz__weakhashz00
									(BgL_tablez00_134, BgL_keyz00_135, BgL_p2z00_136,
									BgL_objz00_137, BgL_initz00_138);
							}
						else
							{	/* Llib/hash.scm 952 */
								BGL_TAIL return
									BGl_plainzd2hashtablezd2addz12z12zz__hashz00(BgL_tablez00_134,
									BgL_keyz00_135, BgL_p2z00_136, BgL_objz00_137,
									BgL_initz00_138);
							}
					}
			}
		}

	}



/* &hashtable-add! */
	obj_t BGl_z62hashtablezd2addz12za2zz__hashz00(obj_t BgL_envz00_5466,
		obj_t BgL_tablez00_5467, obj_t BgL_keyz00_5468, obj_t BgL_p2z00_5469,
		obj_t BgL_objz00_5470, obj_t BgL_initz00_5471)
	{
		{	/* Llib/hash.scm 948 */
			{	/* Llib/hash.scm 950 */
				obj_t BgL_auxz00_8086;
				obj_t BgL_auxz00_8079;

				if (PROCEDUREP(BgL_p2z00_5469))
					{	/* Llib/hash.scm 950 */
						BgL_auxz00_8086 = BgL_p2z00_5469;
					}
				else
					{
						obj_t BgL_auxz00_8089;

						BgL_auxz00_8089 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(38228L), BGl_string2598z00zz__hashz00,
							BGl_string2579z00zz__hashz00, BgL_p2z00_5469);
						FAILURE(BgL_auxz00_8089, BFALSE, BFALSE);
					}
				if (STRUCTP(BgL_tablez00_5467))
					{	/* Llib/hash.scm 950 */
						BgL_auxz00_8079 = BgL_tablez00_5467;
					}
				else
					{
						obj_t BgL_auxz00_8082;

						BgL_auxz00_8082 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(38228L), BGl_string2598z00zz__hashz00,
							BGl_string2572z00zz__hashz00, BgL_tablez00_5467);
						FAILURE(BgL_auxz00_8082, BFALSE, BFALSE);
					}
				return
					BGl_hashtablezd2addz12zc0zz__hashz00(BgL_auxz00_8079, BgL_keyz00_5468,
					BgL_auxz00_8086, BgL_objz00_5470, BgL_initz00_5471);
			}
		}

	}



/* open-string-hashtable-add! */
	BGL_EXPORTED_DEF obj_t
		BGl_openzd2stringzd2hashtablezd2addz12zc0zz__hashz00(obj_t BgL_tablez00_139,
		obj_t BgL_keyz00_140, obj_t BgL_procz00_141, obj_t BgL_objz00_142,
		obj_t BgL_initz00_143)
	{
		{	/* Llib/hash.scm 960 */
			{	/* Llib/hash.scm 961 */
				obj_t BgL_siza7eza7_2220;

				BgL_siza7eza7_2220 = STRUCT_REF(BgL_tablez00_139, (int) (1L));
				{	/* Llib/hash.scm 961 */
					obj_t BgL_bucketsz00_2221;

					BgL_bucketsz00_2221 = STRUCT_REF(BgL_tablez00_139, (int) (2L));
					{	/* Llib/hash.scm 962 */
						long BgL_hashz00_2222;

						{	/* Llib/hash.scm 963 */
							long BgL_arg1798z00_2250;

							BgL_arg1798z00_2250 = STRING_LENGTH(BgL_keyz00_140);
							BgL_hashz00_2222 =
								bgl_string_hash(BSTRING_TO_STRING(BgL_keyz00_140),
								(int) (0L), (int) (BgL_arg1798z00_2250));
						}
						{	/* Llib/hash.scm 963 */

							{	/* Llib/hash.scm 965 */
								long BgL_g1073z00_2223;

								{	/* Llib/hash.scm 965 */
									long BgL_n1z00_4504;
									long BgL_n2z00_4505;

									BgL_n1z00_4504 = BgL_hashz00_2222;
									BgL_n2z00_4505 = (long) CINT(BgL_siza7eza7_2220);
									{	/* Llib/hash.scm 965 */
										bool_t BgL_test3024z00_8104;

										{	/* Llib/hash.scm 965 */
											long BgL_arg2151z00_4507;

											BgL_arg2151z00_4507 =
												(((BgL_n1z00_4504) | (BgL_n2z00_4505)) & -2147483648);
											BgL_test3024z00_8104 = (BgL_arg2151z00_4507 == 0L);
										}
										if (BgL_test3024z00_8104)
											{	/* Llib/hash.scm 965 */
												int32_t BgL_arg2148z00_4508;

												{	/* Llib/hash.scm 965 */
													int32_t BgL_arg2149z00_4509;
													int32_t BgL_arg2150z00_4510;

													BgL_arg2149z00_4509 = (int32_t) (BgL_n1z00_4504);
													BgL_arg2150z00_4510 = (int32_t) (BgL_n2z00_4505);
													BgL_arg2148z00_4508 =
														(BgL_arg2149z00_4509 % BgL_arg2150z00_4510);
												}
												{	/* Llib/hash.scm 965 */
													long BgL_arg2252z00_4515;

													BgL_arg2252z00_4515 = (long) (BgL_arg2148z00_4508);
													BgL_g1073z00_2223 = (long) (BgL_arg2252z00_4515);
											}}
										else
											{	/* Llib/hash.scm 965 */
												BgL_g1073z00_2223 = (BgL_n1z00_4504 % BgL_n2z00_4505);
											}
									}
								}
								{
									long BgL_offz00_2225;
									long BgL_iz00_2226;

									BgL_offz00_2225 = BgL_g1073z00_2223;
									BgL_iz00_2226 = 1L;
								BgL_zc3z04anonymousza31778ze3z87_2227:
									{	/* Llib/hash.scm 967 */
										long BgL_off3z00_2228;

										BgL_off3z00_2228 = (BgL_offz00_2225 * 3L);
										if (CBOOL(VECTOR_REF(
													((obj_t) BgL_bucketsz00_2221), BgL_off3z00_2228)))
											{	/* Llib/hash.scm 969 */
												bool_t BgL_test3026z00_8118;

												{	/* Llib/hash.scm 969 */
													obj_t BgL_arg1796z00_2247;

													BgL_arg1796z00_2247 =
														VECTOR_REF(
														((obj_t) BgL_bucketsz00_2221), BgL_off3z00_2228);
													{	/* Llib/hash.scm 969 */
														long BgL_l1z00_4524;

														BgL_l1z00_4524 =
															STRING_LENGTH(((obj_t) BgL_arg1796z00_2247));
														if (
															(BgL_l1z00_4524 == STRING_LENGTH(BgL_keyz00_140)))
															{	/* Llib/hash.scm 969 */
																int BgL_arg2041z00_4527;

																{	/* Llib/hash.scm 969 */
																	char *BgL_auxz00_8129;
																	char *BgL_tmpz00_8126;

																	BgL_auxz00_8129 =
																		BSTRING_TO_STRING(BgL_keyz00_140);
																	BgL_tmpz00_8126 =
																		BSTRING_TO_STRING(
																		((obj_t) BgL_arg1796z00_2247));
																	BgL_arg2041z00_4527 =
																		memcmp(BgL_tmpz00_8126, BgL_auxz00_8129,
																		BgL_l1z00_4524);
																}
																BgL_test3026z00_8118 =
																	((long) (BgL_arg2041z00_4527) == 0L);
															}
														else
															{	/* Llib/hash.scm 969 */
																BgL_test3026z00_8118 = ((bool_t) 0);
															}
													}
												}
												if (BgL_test3026z00_8118)
													{	/* Llib/hash.scm 970 */
														bool_t BgL_test3028z00_8134;

														{	/* Llib/hash.scm 970 */
															long BgL_arg1790z00_2240;

															BgL_arg1790z00_2240 = (BgL_off3z00_2228 + 2L);
															BgL_test3028z00_8134 =
																CBOOL(VECTOR_REF(
																	((obj_t) BgL_bucketsz00_2221),
																	BgL_arg1790z00_2240));
														}
														if (BgL_test3028z00_8134)
															{	/* Llib/hash.scm 971 */
																obj_t BgL_ovalz00_2234;

																{	/* Llib/hash.scm 971 */
																	long BgL_arg1787z00_2237;

																	BgL_arg1787z00_2237 = (BgL_off3z00_2228 + 1L);
																	BgL_ovalz00_2234 =
																		VECTOR_REF(
																		((obj_t) BgL_bucketsz00_2221),
																		BgL_arg1787z00_2237);
																}
																{	/* Llib/hash.scm 972 */
																	long BgL_arg1785z00_2235;
																	obj_t BgL_arg1786z00_2236;

																	BgL_arg1785z00_2235 = (BgL_off3z00_2228 + 1L);
																	BgL_arg1786z00_2236 =
																		BGL_PROCEDURE_CALL2(BgL_procz00_141,
																		BgL_ovalz00_2234, BgL_initz00_143);
																	return VECTOR_SET(((obj_t)
																			BgL_bucketsz00_2221), BgL_arg1785z00_2235,
																		BgL_arg1786z00_2236);
																}
															}
														else
															{	/* Llib/hash.scm 974 */
																long BgL_arg1788z00_2238;
																obj_t BgL_arg1789z00_2239;

																BgL_arg1788z00_2238 = (BgL_off3z00_2228 + 1L);
																BgL_arg1789z00_2239 =
																	BGL_PROCEDURE_CALL2(BgL_procz00_141,
																	BgL_objz00_142, BgL_initz00_143);
																return VECTOR_SET(((obj_t) BgL_bucketsz00_2221),
																	BgL_arg1788z00_2238, BgL_arg1789z00_2239);
															}
													}
												else
													{	/* Llib/hash.scm 976 */
														long BgL_noffz00_2241;

														BgL_noffz00_2241 =
															(BgL_offz00_2225 +
															(BgL_iz00_2226 * BgL_iz00_2226));
														if (
															(BgL_noffz00_2241 >=
																(long) CINT(BgL_siza7eza7_2220)))
															{	/* Llib/hash.scm 978 */
																long BgL_arg1792z00_2243;
																long BgL_arg1793z00_2244;

																{	/* Llib/hash.scm 978 */
																	long BgL_n1z00_4551;
																	long BgL_n2z00_4552;

																	BgL_n1z00_4551 = BgL_noffz00_2241;
																	BgL_n2z00_4552 =
																		(long) CINT(BgL_siza7eza7_2220);
																	{	/* Llib/hash.scm 978 */
																		bool_t BgL_test3030z00_8164;

																		{	/* Llib/hash.scm 978 */
																			long BgL_arg2151z00_4554;

																			BgL_arg2151z00_4554 =
																				(((BgL_n1z00_4551) | (BgL_n2z00_4552)) &
																				-2147483648);
																			BgL_test3030z00_8164 =
																				(BgL_arg2151z00_4554 == 0L);
																		}
																		if (BgL_test3030z00_8164)
																			{	/* Llib/hash.scm 978 */
																				int32_t BgL_arg2148z00_4555;

																				{	/* Llib/hash.scm 978 */
																					int32_t BgL_arg2149z00_4556;
																					int32_t BgL_arg2150z00_4557;

																					BgL_arg2149z00_4556 =
																						(int32_t) (BgL_n1z00_4551);
																					BgL_arg2150z00_4557 =
																						(int32_t) (BgL_n2z00_4552);
																					BgL_arg2148z00_4555 =
																						(BgL_arg2149z00_4556 %
																						BgL_arg2150z00_4557);
																				}
																				{	/* Llib/hash.scm 978 */
																					long BgL_arg2252z00_4562;

																					BgL_arg2252z00_4562 =
																						(long) (BgL_arg2148z00_4555);
																					BgL_arg1792z00_2243 =
																						(long) (BgL_arg2252z00_4562);
																			}}
																		else
																			{	/* Llib/hash.scm 978 */
																				BgL_arg1792z00_2243 =
																					(BgL_n1z00_4551 % BgL_n2z00_4552);
																			}
																	}
																}
																BgL_arg1793z00_2244 = (BgL_iz00_2226 + 1L);
																{
																	long BgL_iz00_8175;
																	long BgL_offz00_8174;

																	BgL_offz00_8174 = BgL_arg1792z00_2243;
																	BgL_iz00_8175 = BgL_arg1793z00_2244;
																	BgL_iz00_2226 = BgL_iz00_8175;
																	BgL_offz00_2225 = BgL_offz00_8174;
																	goto BgL_zc3z04anonymousza31778ze3z87_2227;
																}
															}
														else
															{
																long BgL_iz00_8177;
																long BgL_offz00_8176;

																BgL_offz00_8176 = BgL_noffz00_2241;
																BgL_iz00_8177 = (BgL_iz00_2226 + 1L);
																BgL_iz00_2226 = BgL_iz00_8177;
																BgL_offz00_2225 = BgL_offz00_8176;
																goto BgL_zc3z04anonymousza31778ze3z87_2227;
															}
													}
											}
										else
											{	/* Llib/hash.scm 981 */
												obj_t BgL_arg1797z00_2248;

												BgL_arg1797z00_2248 =
													BGL_PROCEDURE_CALL2(BgL_procz00_141, BgL_objz00_142,
													BgL_initz00_143);
												return
													BGl_openzd2stringzd2hashtablezd2putzf2hashz12z32zz__hashz00
													(BgL_tablez00_139, BgL_keyz00_140,
													BgL_arg1797z00_2248, BINT(BgL_hashz00_2222));
											}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &open-string-hashtable-add! */
	obj_t BGl_z62openzd2stringzd2hashtablezd2addz12za2zz__hashz00(obj_t
		BgL_envz00_5472, obj_t BgL_tablez00_5473, obj_t BgL_keyz00_5474,
		obj_t BgL_procz00_5475, obj_t BgL_objz00_5476, obj_t BgL_initz00_5477)
	{
		{	/* Llib/hash.scm 960 */
			{	/* Llib/hash.scm 961 */
				obj_t BgL_auxz00_8200;
				obj_t BgL_auxz00_8193;
				obj_t BgL_auxz00_8186;

				if (PROCEDUREP(BgL_procz00_5475))
					{	/* Llib/hash.scm 961 */
						BgL_auxz00_8200 = BgL_procz00_5475;
					}
				else
					{
						obj_t BgL_auxz00_8203;

						BgL_auxz00_8203 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(38784L), BGl_string2599z00zz__hashz00,
							BGl_string2579z00zz__hashz00, BgL_procz00_5475);
						FAILURE(BgL_auxz00_8203, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_keyz00_5474))
					{	/* Llib/hash.scm 961 */
						BgL_auxz00_8193 = BgL_keyz00_5474;
					}
				else
					{
						obj_t BgL_auxz00_8196;

						BgL_auxz00_8196 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(38784L), BGl_string2599z00zz__hashz00,
							BGl_string2588z00zz__hashz00, BgL_keyz00_5474);
						FAILURE(BgL_auxz00_8196, BFALSE, BFALSE);
					}
				if (STRUCTP(BgL_tablez00_5473))
					{	/* Llib/hash.scm 961 */
						BgL_auxz00_8186 = BgL_tablez00_5473;
					}
				else
					{
						obj_t BgL_auxz00_8189;

						BgL_auxz00_8189 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(38784L), BGl_string2599z00zz__hashz00,
							BGl_string2572z00zz__hashz00, BgL_tablez00_5473);
						FAILURE(BgL_auxz00_8189, BFALSE, BFALSE);
					}
				return
					BGl_openzd2stringzd2hashtablezd2addz12zc0zz__hashz00(BgL_auxz00_8186,
					BgL_auxz00_8193, BgL_auxz00_8200, BgL_objz00_5476, BgL_initz00_5477);
			}
		}

	}



/* plain-hashtable-add! */
	obj_t BGl_plainzd2hashtablezd2addz12z12zz__hashz00(obj_t BgL_tablez00_144,
		obj_t BgL_keyz00_145, obj_t BgL_procz00_146, obj_t BgL_objz00_147,
		obj_t BgL_initz00_148)
	{
		{	/* Llib/hash.scm 986 */
			{	/* Llib/hash.scm 987 */
				obj_t BgL_bucketsz00_2251;

				BgL_bucketsz00_2251 = STRUCT_REF(BgL_tablez00_144, (int) (2L));
				{	/* Llib/hash.scm 988 */
					long BgL_bucketzd2numzd2_2253;

					{	/* Llib/hash.scm 989 */
						long BgL_arg1819z00_2283;

						{	/* Llib/hash.scm 989 */
							obj_t BgL_hashnz00_4568;

							BgL_hashnz00_4568 = STRUCT_REF(BgL_tablez00_144, (int) (4L));
							if (PROCEDUREP(BgL_hashnz00_4568))
								{	/* Llib/hash.scm 989 */
									obj_t BgL_arg1318z00_4570;

									BgL_arg1318z00_4570 =
										BGL_PROCEDURE_CALL1(BgL_hashnz00_4568, BgL_keyz00_145);
									{	/* Llib/hash.scm 989 */
										long BgL_nz00_4572;

										BgL_nz00_4572 = (long) CINT(BgL_arg1318z00_4570);
										if ((BgL_nz00_4572 < 0L))
											{	/* Llib/hash.scm 989 */
												BgL_arg1819z00_2283 = NEG(BgL_nz00_4572);
											}
										else
											{	/* Llib/hash.scm 989 */
												BgL_arg1819z00_2283 = BgL_nz00_4572;
											}
									}
								}
							else
								{	/* Llib/hash.scm 989 */
									if ((BgL_hashnz00_4568 == BGl_symbol2567z00zz__hashz00))
										{	/* Llib/hash.scm 989 */
											BgL_arg1819z00_2283 =
												BGl_objzd2hashze70z35zz__hashz00(BgL_keyz00_145);
										}
									else
										{	/* Llib/hash.scm 989 */
											BgL_arg1819z00_2283 =
												BGl_getzd2hashnumberzd2zz__hashz00(BgL_keyz00_145);
										}
								}
						}
						{	/* Llib/hash.scm 989 */
							long BgL_n1z00_4576;
							long BgL_n2z00_4577;

							BgL_n1z00_4576 = BgL_arg1819z00_2283;
							BgL_n2z00_4577 = VECTOR_LENGTH(((obj_t) BgL_bucketsz00_2251));
							{	/* Llib/hash.scm 989 */
								bool_t BgL_test3037z00_8228;

								{	/* Llib/hash.scm 989 */
									long BgL_arg2151z00_4579;

									BgL_arg2151z00_4579 =
										(((BgL_n1z00_4576) | (BgL_n2z00_4577)) & -2147483648);
									BgL_test3037z00_8228 = (BgL_arg2151z00_4579 == 0L);
								}
								if (BgL_test3037z00_8228)
									{	/* Llib/hash.scm 989 */
										int32_t BgL_arg2148z00_4580;

										{	/* Llib/hash.scm 989 */
											int32_t BgL_arg2149z00_4581;
											int32_t BgL_arg2150z00_4582;

											BgL_arg2149z00_4581 = (int32_t) (BgL_n1z00_4576);
											BgL_arg2150z00_4582 = (int32_t) (BgL_n2z00_4577);
											BgL_arg2148z00_4580 =
												(BgL_arg2149z00_4581 % BgL_arg2150z00_4582);
										}
										{	/* Llib/hash.scm 989 */
											long BgL_arg2252z00_4587;

											BgL_arg2252z00_4587 = (long) (BgL_arg2148z00_4580);
											BgL_bucketzd2numzd2_2253 = (long) (BgL_arg2252z00_4587);
									}}
								else
									{	/* Llib/hash.scm 989 */
										BgL_bucketzd2numzd2_2253 =
											(BgL_n1z00_4576 % BgL_n2z00_4577);
									}
							}
						}
					}
					{	/* Llib/hash.scm 989 */
						obj_t BgL_bucketz00_2254;

						BgL_bucketz00_2254 =
							VECTOR_REF(
							((obj_t) BgL_bucketsz00_2251), BgL_bucketzd2numzd2_2253);
						{	/* Llib/hash.scm 990 */
							obj_t BgL_maxzd2bucketzd2lenz00_2255;

							BgL_maxzd2bucketzd2lenz00_2255 =
								STRUCT_REF(BgL_tablez00_144, (int) (1L));
							{	/* Llib/hash.scm 991 */

								if (NULLP(BgL_bucketz00_2254))
									{	/* Llib/hash.scm 993 */
										obj_t BgL_vz00_2257;

										BgL_vz00_2257 =
											BGL_PROCEDURE_CALL2(BgL_procz00_146, BgL_objz00_147,
											BgL_initz00_148);
										{	/* Llib/hash.scm 994 */
											long BgL_arg1800z00_2258;

											BgL_arg1800z00_2258 =
												(
												(long) CINT(STRUCT_REF(BgL_tablez00_144,
														(int) (0L))) + 1L);
											{	/* Llib/hash.scm 994 */
												obj_t BgL_auxz00_8254;
												int BgL_tmpz00_8252;

												BgL_auxz00_8254 = BINT(BgL_arg1800z00_2258);
												BgL_tmpz00_8252 = (int) (0L);
												STRUCT_SET(BgL_tablez00_144, BgL_tmpz00_8252,
													BgL_auxz00_8254);
										}}
										{	/* Llib/hash.scm 995 */
											obj_t BgL_arg1802z00_2260;

											{	/* Llib/hash.scm 995 */
												obj_t BgL_arg1803z00_2261;

												BgL_arg1803z00_2261 =
													MAKE_YOUNG_PAIR(BgL_keyz00_145, BgL_vz00_2257);
												{	/* Llib/hash.scm 995 */
													obj_t BgL_list1804z00_2262;

													BgL_list1804z00_2262 =
														MAKE_YOUNG_PAIR(BgL_arg1803z00_2261, BNIL);
													BgL_arg1802z00_2260 = BgL_list1804z00_2262;
											}}
											VECTOR_SET(
												((obj_t) BgL_bucketsz00_2251), BgL_bucketzd2numzd2_2253,
												BgL_arg1802z00_2260);
										}
										return BgL_vz00_2257;
									}
								else
									{
										obj_t BgL_buckz00_2264;
										long BgL_countz00_2265;

										BgL_buckz00_2264 = BgL_bucketz00_2254;
										BgL_countz00_2265 = 0L;
									BgL_zc3z04anonymousza31805ze3z87_2266:
										if (NULLP(BgL_buckz00_2264))
											{	/* Llib/hash.scm 1001 */
												obj_t BgL_vz00_2268;

												BgL_vz00_2268 =
													BGL_PROCEDURE_CALL2(BgL_procz00_146, BgL_objz00_147,
													BgL_initz00_148);
												{	/* Llib/hash.scm 1002 */
													long BgL_arg1807z00_2269;

													BgL_arg1807z00_2269 =
														(
														(long) CINT(STRUCT_REF(BgL_tablez00_144,
																(int) (0L))) + 1L);
													{	/* Llib/hash.scm 1002 */
														obj_t BgL_auxz00_8274;
														int BgL_tmpz00_8272;

														BgL_auxz00_8274 = BINT(BgL_arg1807z00_2269);
														BgL_tmpz00_8272 = (int) (0L);
														STRUCT_SET(BgL_tablez00_144, BgL_tmpz00_8272,
															BgL_auxz00_8274);
												}}
												{	/* Llib/hash.scm 1003 */
													obj_t BgL_arg1809z00_2271;

													{	/* Llib/hash.scm 1003 */
														obj_t BgL_arg1810z00_2272;

														BgL_arg1810z00_2272 =
															MAKE_YOUNG_PAIR(BgL_keyz00_145, BgL_vz00_2268);
														BgL_arg1809z00_2271 =
															MAKE_YOUNG_PAIR(BgL_arg1810z00_2272,
															BgL_bucketz00_2254);
													}
													VECTOR_SET(
														((obj_t) BgL_bucketsz00_2251),
														BgL_bucketzd2numzd2_2253, BgL_arg1809z00_2271);
												}
												if (
													(BgL_countz00_2265 >
														(long) CINT(BgL_maxzd2bucketzd2lenz00_2255)))
													{	/* Llib/hash.scm 1004 */
														BGl_plainzd2hashtablezd2expandz12z12zz__hashz00
															(BgL_tablez00_144);
													}
												else
													{	/* Llib/hash.scm 1004 */
														BFALSE;
													}
												return BgL_vz00_2268;
											}
										else
											{	/* Llib/hash.scm 1007 */
												bool_t BgL_test3041z00_8285;

												{	/* Llib/hash.scm 1007 */
													obj_t BgL_arg1818z00_2281;

													{	/* Llib/hash.scm 1007 */
														obj_t BgL_pairz00_4608;

														BgL_pairz00_4608 = CAR(((obj_t) BgL_buckz00_2264));
														BgL_arg1818z00_2281 = CAR(BgL_pairz00_4608);
													}
													{	/* Llib/hash.scm 1007 */
														obj_t BgL_eqtz00_4609;

														BgL_eqtz00_4609 =
															STRUCT_REF(BgL_tablez00_144, (int) (3L));
														if (PROCEDUREP(BgL_eqtz00_4609))
															{	/* Llib/hash.scm 1007 */
																BgL_test3041z00_8285 =
																	CBOOL(BGL_PROCEDURE_CALL2(BgL_eqtz00_4609,
																		BgL_arg1818z00_2281, BgL_keyz00_145));
															}
														else
															{	/* Llib/hash.scm 1007 */
																if ((BgL_arg1818z00_2281 == BgL_keyz00_145))
																	{	/* Llib/hash.scm 1007 */
																		BgL_test3041z00_8285 = ((bool_t) 1);
																	}
																else
																	{	/* Llib/hash.scm 1007 */
																		if (STRINGP(BgL_arg1818z00_2281))
																			{	/* Llib/hash.scm 1007 */
																				if (STRINGP(BgL_keyz00_145))
																					{	/* Llib/hash.scm 1007 */
																						long BgL_l1z00_4616;

																						BgL_l1z00_4616 =
																							STRING_LENGTH
																							(BgL_arg1818z00_2281);
																						if ((BgL_l1z00_4616 ==
																								STRING_LENGTH(BgL_keyz00_145)))
																							{	/* Llib/hash.scm 1007 */
																								int BgL_arg2041z00_4619;

																								{	/* Llib/hash.scm 1007 */
																									char *BgL_auxz00_8311;
																									char *BgL_tmpz00_8309;

																									BgL_auxz00_8311 =
																										BSTRING_TO_STRING
																										(BgL_keyz00_145);
																									BgL_tmpz00_8309 =
																										BSTRING_TO_STRING
																										(BgL_arg1818z00_2281);
																									BgL_arg2041z00_4619 =
																										memcmp(BgL_tmpz00_8309,
																										BgL_auxz00_8311,
																										BgL_l1z00_4616);
																								}
																								BgL_test3041z00_8285 =
																									(
																									(long) (BgL_arg2041z00_4619)
																									== 0L);
																							}
																						else
																							{	/* Llib/hash.scm 1007 */
																								BgL_test3041z00_8285 =
																									((bool_t) 0);
																							}
																					}
																				else
																					{	/* Llib/hash.scm 1007 */
																						BgL_test3041z00_8285 = ((bool_t) 0);
																					}
																			}
																		else
																			{	/* Llib/hash.scm 1007 */
																				BgL_test3041z00_8285 = ((bool_t) 0);
																			}
																	}
															}
													}
												}
												if (BgL_test3041z00_8285)
													{	/* Llib/hash.scm 1008 */
														obj_t BgL_resz00_2276;

														{	/* Llib/hash.scm 1008 */
															obj_t BgL_arg1815z00_2278;

															{	/* Llib/hash.scm 1008 */
																obj_t BgL_pairz00_4628;

																BgL_pairz00_4628 =
																	CAR(((obj_t) BgL_buckz00_2264));
																BgL_arg1815z00_2278 = CDR(BgL_pairz00_4628);
															}
															BgL_resz00_2276 =
																BGL_PROCEDURE_CALL2(BgL_procz00_146,
																BgL_objz00_147, BgL_arg1815z00_2278);
														}
														{	/* Llib/hash.scm 1009 */
															obj_t BgL_arg1814z00_2277;

															BgL_arg1814z00_2277 =
																CAR(((obj_t) BgL_buckz00_2264));
															{	/* Llib/hash.scm 1009 */
																obj_t BgL_tmpz00_8326;

																BgL_tmpz00_8326 = ((obj_t) BgL_arg1814z00_2277);
																SET_CDR(BgL_tmpz00_8326, BgL_resz00_2276);
															}
														}
														return BgL_resz00_2276;
													}
												else
													{	/* Llib/hash.scm 1012 */
														obj_t BgL_arg1816z00_2279;
														long BgL_arg1817z00_2280;

														BgL_arg1816z00_2279 =
															CDR(((obj_t) BgL_buckz00_2264));
														BgL_arg1817z00_2280 = (BgL_countz00_2265 + 1L);
														{
															long BgL_countz00_8333;
															obj_t BgL_buckz00_8332;

															BgL_buckz00_8332 = BgL_arg1816z00_2279;
															BgL_countz00_8333 = BgL_arg1817z00_2280;
															BgL_countz00_2265 = BgL_countz00_8333;
															BgL_buckz00_2264 = BgL_buckz00_8332;
															goto BgL_zc3z04anonymousza31805ze3z87_2266;
														}
													}
											}
									}
							}
						}
					}
				}
			}
		}

	}



/* hashtable-remove! */
	BGL_EXPORTED_DEF bool_t BGl_hashtablezd2removez12zc0zz__hashz00(obj_t
		BgL_tablez00_149, obj_t BgL_keyz00_150)
	{
		{	/* Llib/hash.scm 1017 */
			{	/* Llib/hash.scm 1019 */
				bool_t BgL_test3047z00_8334;

				if (
					(0L == ((long) CINT(STRUCT_REF(BgL_tablez00_149, (int) (5L))) & 8L)))
					{	/* Llib/hash.scm 294 */
						BgL_test3047z00_8334 = ((bool_t) 0);
					}
				else
					{	/* Llib/hash.scm 294 */
						BgL_test3047z00_8334 = ((bool_t) 1);
					}
				if (BgL_test3047z00_8334)
					{	/* Llib/hash.scm 1019 */
						return
							CBOOL(BGl_openzd2stringzd2hashtablezd2removez12zc0zz__hashz00
							(BgL_tablez00_149, BgL_keyz00_150));
					}
				else
					{	/* Llib/hash.scm 1021 */
						bool_t BgL_test3049z00_8343;

						if (
							(0L ==
								((long) CINT(STRUCT_REF(BgL_tablez00_149, (int) (5L))) & 3L)))
							{	/* Llib/hash.scm 288 */
								BgL_test3049z00_8343 = ((bool_t) 0);
							}
						else
							{	/* Llib/hash.scm 288 */
								BgL_test3049z00_8343 = ((bool_t) 1);
							}
						if (BgL_test3049z00_8343)
							{	/* Llib/hash.scm 1021 */
								return
									CBOOL(BGl_weakzd2hashtablezd2removez12z12zz__weakhashz00
									(BgL_tablez00_149, BgL_keyz00_150));
							}
						else
							{	/* Llib/hash.scm 1021 */
								BGL_TAIL return
									BGl_plainzd2hashtablezd2removez12z12zz__hashz00
									(BgL_tablez00_149, BgL_keyz00_150);
							}
					}
			}
		}

	}



/* &hashtable-remove! */
	obj_t BGl_z62hashtablezd2removez12za2zz__hashz00(obj_t BgL_envz00_5478,
		obj_t BgL_tablez00_5479, obj_t BgL_keyz00_5480)
	{
		{	/* Llib/hash.scm 1017 */
			{	/* Llib/hash.scm 1019 */
				bool_t BgL_tmpz00_8353;

				{	/* Llib/hash.scm 1019 */
					obj_t BgL_auxz00_8354;

					if (STRUCTP(BgL_tablez00_5479))
						{	/* Llib/hash.scm 1019 */
							BgL_auxz00_8354 = BgL_tablez00_5479;
						}
					else
						{
							obj_t BgL_auxz00_8357;

							BgL_auxz00_8357 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
								BINT(41119L), BGl_string2600z00zz__hashz00,
								BGl_string2572z00zz__hashz00, BgL_tablez00_5479);
							FAILURE(BgL_auxz00_8357, BFALSE, BFALSE);
						}
					BgL_tmpz00_8353 =
						BGl_hashtablezd2removez12zc0zz__hashz00(BgL_auxz00_8354,
						BgL_keyz00_5480);
				}
				return BBOOL(BgL_tmpz00_8353);
			}
		}

	}



/* open-string-hashtable-remove! */
	BGL_EXPORTED_DEF obj_t
		BGl_openzd2stringzd2hashtablezd2removez12zc0zz__hashz00(obj_t BgL_tz00_151,
		obj_t BgL_keyz00_152)
	{
		{	/* Llib/hash.scm 1029 */
			{	/* Llib/hash.scm 1030 */
				obj_t BgL_siza7eza7_2286;

				BgL_siza7eza7_2286 = STRUCT_REF(BgL_tz00_151, (int) (1L));
				{	/* Llib/hash.scm 1030 */
					obj_t BgL_bucketsz00_2287;

					BgL_bucketsz00_2287 = STRUCT_REF(BgL_tz00_151, (int) (2L));
					{	/* Llib/hash.scm 1031 */
						long BgL_hashz00_2288;

						{	/* Llib/hash.scm 1032 */
							long BgL_arg1836z00_2308;

							BgL_arg1836z00_2308 = STRING_LENGTH(BgL_keyz00_152);
							BgL_hashz00_2288 =
								bgl_string_hash(BSTRING_TO_STRING(BgL_keyz00_152),
								(int) (0L), (int) (BgL_arg1836z00_2308));
						}
						{	/* Llib/hash.scm 1032 */

							{	/* Llib/hash.scm 1034 */
								long BgL_g1074z00_2289;

								{	/* Llib/hash.scm 1034 */
									long BgL_n1z00_4652;
									long BgL_n2z00_4653;

									BgL_n1z00_4652 = BgL_hashz00_2288;
									BgL_n2z00_4653 = (long) CINT(BgL_siza7eza7_2286);
									{	/* Llib/hash.scm 1034 */
										bool_t BgL_test3052z00_8373;

										{	/* Llib/hash.scm 1034 */
											long BgL_arg2151z00_4655;

											BgL_arg2151z00_4655 =
												(((BgL_n1z00_4652) | (BgL_n2z00_4653)) & -2147483648);
											BgL_test3052z00_8373 = (BgL_arg2151z00_4655 == 0L);
										}
										if (BgL_test3052z00_8373)
											{	/* Llib/hash.scm 1034 */
												int32_t BgL_arg2148z00_4656;

												{	/* Llib/hash.scm 1034 */
													int32_t BgL_arg2149z00_4657;
													int32_t BgL_arg2150z00_4658;

													BgL_arg2149z00_4657 = (int32_t) (BgL_n1z00_4652);
													BgL_arg2150z00_4658 = (int32_t) (BgL_n2z00_4653);
													BgL_arg2148z00_4656 =
														(BgL_arg2149z00_4657 % BgL_arg2150z00_4658);
												}
												{	/* Llib/hash.scm 1034 */
													long BgL_arg2252z00_4663;

													BgL_arg2252z00_4663 = (long) (BgL_arg2148z00_4656);
													BgL_g1074z00_2289 = (long) (BgL_arg2252z00_4663);
											}}
										else
											{	/* Llib/hash.scm 1034 */
												BgL_g1074z00_2289 = (BgL_n1z00_4652 % BgL_n2z00_4653);
											}
									}
								}
								{
									long BgL_offz00_2291;
									long BgL_iz00_2292;

									BgL_offz00_2291 = BgL_g1074z00_2289;
									BgL_iz00_2292 = 1L;
								BgL_zc3z04anonymousza31822ze3z87_2293:
									{	/* Llib/hash.scm 1036 */
										long BgL_off3z00_2294;

										BgL_off3z00_2294 = (BgL_offz00_2291 * 3L);
										if (CBOOL(VECTOR_REF(
													((obj_t) BgL_bucketsz00_2287), BgL_off3z00_2294)))
											{	/* Llib/hash.scm 1038 */
												bool_t BgL_test3054z00_8387;

												{	/* Llib/hash.scm 1038 */
													obj_t BgL_arg1835z00_2306;

													BgL_arg1835z00_2306 =
														VECTOR_REF(
														((obj_t) BgL_bucketsz00_2287), BgL_off3z00_2294);
													{	/* Llib/hash.scm 1038 */
														long BgL_l1z00_4672;

														BgL_l1z00_4672 =
															STRING_LENGTH(((obj_t) BgL_arg1835z00_2306));
														if (
															(BgL_l1z00_4672 == STRING_LENGTH(BgL_keyz00_152)))
															{	/* Llib/hash.scm 1038 */
																int BgL_arg2041z00_4675;

																{	/* Llib/hash.scm 1038 */
																	char *BgL_auxz00_8398;
																	char *BgL_tmpz00_8395;

																	BgL_auxz00_8398 =
																		BSTRING_TO_STRING(BgL_keyz00_152);
																	BgL_tmpz00_8395 =
																		BSTRING_TO_STRING(
																		((obj_t) BgL_arg1835z00_2306));
																	BgL_arg2041z00_4675 =
																		memcmp(BgL_tmpz00_8395, BgL_auxz00_8398,
																		BgL_l1z00_4672);
																}
																BgL_test3054z00_8387 =
																	((long) (BgL_arg2041z00_4675) == 0L);
															}
														else
															{	/* Llib/hash.scm 1038 */
																BgL_test3054z00_8387 = ((bool_t) 0);
															}
													}
												}
												if (BgL_test3054z00_8387)
													{	/* Llib/hash.scm 1038 */
														{	/* Llib/hash.scm 1040 */
															long BgL_arg1828z00_2298;

															BgL_arg1828z00_2298 = (BgL_off3z00_2294 + 1L);
															VECTOR_SET(
																((obj_t) BgL_bucketsz00_2287),
																BgL_arg1828z00_2298, BFALSE);
														}
														{	/* Llib/hash.scm 1041 */
															long BgL_arg1829z00_2299;

															BgL_arg1829z00_2299 = (BgL_off3z00_2294 + 2L);
															VECTOR_SET(
																((obj_t) BgL_bucketsz00_2287),
																BgL_arg1829z00_2299, BFALSE);
														}
														{	/* Llib/hash.scm 1303 */
															long BgL_arg1978z00_4687;

															BgL_arg1978z00_4687 =
																(
																(long) CINT(STRUCT_REF(BgL_tz00_151,
																		(int) (6L))) + 1L);
															{	/* Llib/hash.scm 1273 */
																obj_t BgL_auxz00_8415;
																int BgL_tmpz00_8413;

																BgL_auxz00_8415 = BINT(BgL_arg1978z00_4687);
																BgL_tmpz00_8413 = (int) (6L);
																return
																	STRUCT_SET(BgL_tz00_151, BgL_tmpz00_8413,
																	BgL_auxz00_8415);
															}
														}
													}
												else
													{	/* Llib/hash.scm 1043 */
														long BgL_noffz00_2300;

														BgL_noffz00_2300 =
															(BgL_offz00_2291 +
															(BgL_iz00_2292 * BgL_iz00_2292));
														if (
															(BgL_noffz00_2300 >=
																(long) CINT(BgL_siza7eza7_2286)))
															{	/* Llib/hash.scm 1045 */
																long BgL_arg1831z00_2302;
																long BgL_arg1832z00_2303;

																{	/* Llib/hash.scm 1045 */
																	long BgL_n1z00_4698;
																	long BgL_n2z00_4699;

																	BgL_n1z00_4698 = BgL_noffz00_2300;
																	BgL_n2z00_4699 =
																		(long) CINT(BgL_siza7eza7_2286);
																	{	/* Llib/hash.scm 1045 */
																		bool_t BgL_test3057z00_8424;

																		{	/* Llib/hash.scm 1045 */
																			long BgL_arg2151z00_4701;

																			BgL_arg2151z00_4701 =
																				(((BgL_n1z00_4698) | (BgL_n2z00_4699)) &
																				-2147483648);
																			BgL_test3057z00_8424 =
																				(BgL_arg2151z00_4701 == 0L);
																		}
																		if (BgL_test3057z00_8424)
																			{	/* Llib/hash.scm 1045 */
																				int32_t BgL_arg2148z00_4702;

																				{	/* Llib/hash.scm 1045 */
																					int32_t BgL_arg2149z00_4703;
																					int32_t BgL_arg2150z00_4704;

																					BgL_arg2149z00_4703 =
																						(int32_t) (BgL_n1z00_4698);
																					BgL_arg2150z00_4704 =
																						(int32_t) (BgL_n2z00_4699);
																					BgL_arg2148z00_4702 =
																						(BgL_arg2149z00_4703 %
																						BgL_arg2150z00_4704);
																				}
																				{	/* Llib/hash.scm 1045 */
																					long BgL_arg2252z00_4709;

																					BgL_arg2252z00_4709 =
																						(long) (BgL_arg2148z00_4702);
																					BgL_arg1831z00_2302 =
																						(long) (BgL_arg2252z00_4709);
																			}}
																		else
																			{	/* Llib/hash.scm 1045 */
																				BgL_arg1831z00_2302 =
																					(BgL_n1z00_4698 % BgL_n2z00_4699);
																			}
																	}
																}
																BgL_arg1832z00_2303 = (BgL_iz00_2292 + 1L);
																{
																	long BgL_iz00_8435;
																	long BgL_offz00_8434;

																	BgL_offz00_8434 = BgL_arg1831z00_2302;
																	BgL_iz00_8435 = BgL_arg1832z00_2303;
																	BgL_iz00_2292 = BgL_iz00_8435;
																	BgL_offz00_2291 = BgL_offz00_8434;
																	goto BgL_zc3z04anonymousza31822ze3z87_2293;
																}
															}
														else
															{
																long BgL_iz00_8437;
																long BgL_offz00_8436;

																BgL_offz00_8436 = BgL_noffz00_2300;
																BgL_iz00_8437 = (BgL_iz00_2292 + 1L);
																BgL_iz00_2292 = BgL_iz00_8437;
																BgL_offz00_2291 = BgL_offz00_8436;
																goto BgL_zc3z04anonymousza31822ze3z87_2293;
															}
													}
											}
										else
											{	/* Llib/hash.scm 1037 */
												return BFALSE;
											}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &open-string-hashtable-remove! */
	obj_t BGl_z62openzd2stringzd2hashtablezd2removez12za2zz__hashz00(obj_t
		BgL_envz00_5481, obj_t BgL_tz00_5482, obj_t BgL_keyz00_5483)
	{
		{	/* Llib/hash.scm 1029 */
			{	/* Llib/hash.scm 1030 */
				obj_t BgL_auxz00_8446;
				obj_t BgL_auxz00_8439;

				if (STRINGP(BgL_keyz00_5483))
					{	/* Llib/hash.scm 1030 */
						BgL_auxz00_8446 = BgL_keyz00_5483;
					}
				else
					{
						obj_t BgL_auxz00_8449;

						BgL_auxz00_8449 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(41605L), BGl_string2601z00zz__hashz00,
							BGl_string2588z00zz__hashz00, BgL_keyz00_5483);
						FAILURE(BgL_auxz00_8449, BFALSE, BFALSE);
					}
				if (STRUCTP(BgL_tz00_5482))
					{	/* Llib/hash.scm 1030 */
						BgL_auxz00_8439 = BgL_tz00_5482;
					}
				else
					{
						obj_t BgL_auxz00_8442;

						BgL_auxz00_8442 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(41605L), BGl_string2601z00zz__hashz00,
							BGl_string2572z00zz__hashz00, BgL_tz00_5482);
						FAILURE(BgL_auxz00_8442, BFALSE, BFALSE);
					}
				return
					BGl_openzd2stringzd2hashtablezd2removez12zc0zz__hashz00
					(BgL_auxz00_8439, BgL_auxz00_8446);
			}
		}

	}



/* plain-hashtable-remove! */
	bool_t BGl_plainzd2hashtablezd2removez12z12zz__hashz00(obj_t BgL_tablez00_153,
		obj_t BgL_keyz00_154)
	{
		{	/* Llib/hash.scm 1051 */
			{	/* Llib/hash.scm 1052 */
				obj_t BgL_bucketsz00_2309;

				BgL_bucketsz00_2309 = STRUCT_REF(BgL_tablez00_153, (int) (2L));
				{	/* Llib/hash.scm 1053 */
					long BgL_bucketzd2numzd2_2311;

					{	/* Llib/hash.scm 1054 */
						long BgL_arg1854z00_2334;

						{	/* Llib/hash.scm 1054 */
							obj_t BgL_hashnz00_4715;

							BgL_hashnz00_4715 = STRUCT_REF(BgL_tablez00_153, (int) (4L));
							if (PROCEDUREP(BgL_hashnz00_4715))
								{	/* Llib/hash.scm 1054 */
									obj_t BgL_arg1318z00_4717;

									BgL_arg1318z00_4717 =
										BGL_PROCEDURE_CALL1(BgL_hashnz00_4715, BgL_keyz00_154);
									{	/* Llib/hash.scm 1054 */
										long BgL_nz00_4719;

										BgL_nz00_4719 = (long) CINT(BgL_arg1318z00_4717);
										if ((BgL_nz00_4719 < 0L))
											{	/* Llib/hash.scm 1054 */
												BgL_arg1854z00_2334 = NEG(BgL_nz00_4719);
											}
										else
											{	/* Llib/hash.scm 1054 */
												BgL_arg1854z00_2334 = BgL_nz00_4719;
											}
									}
								}
							else
								{	/* Llib/hash.scm 1054 */
									if ((BgL_hashnz00_4715 == BGl_symbol2567z00zz__hashz00))
										{	/* Llib/hash.scm 1054 */
											BgL_arg1854z00_2334 =
												BGl_objzd2hashze70z35zz__hashz00(BgL_keyz00_154);
										}
									else
										{	/* Llib/hash.scm 1054 */
											BgL_arg1854z00_2334 =
												BGl_getzd2hashnumberzd2zz__hashz00(BgL_keyz00_154);
										}
								}
						}
						{	/* Llib/hash.scm 1054 */
							long BgL_n1z00_4723;
							long BgL_n2z00_4724;

							BgL_n1z00_4723 = BgL_arg1854z00_2334;
							BgL_n2z00_4724 = VECTOR_LENGTH(((obj_t) BgL_bucketsz00_2309));
							{	/* Llib/hash.scm 1054 */
								bool_t BgL_test3063z00_8474;

								{	/* Llib/hash.scm 1054 */
									long BgL_arg2151z00_4726;

									BgL_arg2151z00_4726 =
										(((BgL_n1z00_4723) | (BgL_n2z00_4724)) & -2147483648);
									BgL_test3063z00_8474 = (BgL_arg2151z00_4726 == 0L);
								}
								if (BgL_test3063z00_8474)
									{	/* Llib/hash.scm 1054 */
										int32_t BgL_arg2148z00_4727;

										{	/* Llib/hash.scm 1054 */
											int32_t BgL_arg2149z00_4728;
											int32_t BgL_arg2150z00_4729;

											BgL_arg2149z00_4728 = (int32_t) (BgL_n1z00_4723);
											BgL_arg2150z00_4729 = (int32_t) (BgL_n2z00_4724);
											BgL_arg2148z00_4727 =
												(BgL_arg2149z00_4728 % BgL_arg2150z00_4729);
										}
										{	/* Llib/hash.scm 1054 */
											long BgL_arg2252z00_4734;

											BgL_arg2252z00_4734 = (long) (BgL_arg2148z00_4727);
											BgL_bucketzd2numzd2_2311 = (long) (BgL_arg2252z00_4734);
									}}
								else
									{	/* Llib/hash.scm 1054 */
										BgL_bucketzd2numzd2_2311 =
											(BgL_n1z00_4723 % BgL_n2z00_4724);
									}
							}
						}
					}
					{	/* Llib/hash.scm 1054 */
						obj_t BgL_bucketz00_2312;

						BgL_bucketz00_2312 =
							VECTOR_REF(
							((obj_t) BgL_bucketsz00_2309), BgL_bucketzd2numzd2_2311);
						{	/* Llib/hash.scm 1055 */

							if (NULLP(BgL_bucketz00_2312))
								{	/* Llib/hash.scm 1057 */
									return ((bool_t) 0);
								}
							else
								{	/* Llib/hash.scm 1059 */
									bool_t BgL_test3065z00_8487;

									{	/* Llib/hash.scm 1059 */
										obj_t BgL_arg1853z00_2333;

										{	/* Llib/hash.scm 1059 */
											obj_t BgL_pairz00_4741;

											BgL_pairz00_4741 = CAR(((obj_t) BgL_bucketz00_2312));
											BgL_arg1853z00_2333 = CAR(BgL_pairz00_4741);
										}
										{	/* Llib/hash.scm 1059 */
											obj_t BgL_eqtz00_4742;

											BgL_eqtz00_4742 =
												STRUCT_REF(BgL_tablez00_153, (int) (3L));
											if (PROCEDUREP(BgL_eqtz00_4742))
												{	/* Llib/hash.scm 1059 */
													BgL_test3065z00_8487 =
														CBOOL(BGL_PROCEDURE_CALL2(BgL_eqtz00_4742,
															BgL_arg1853z00_2333, BgL_keyz00_154));
												}
											else
												{	/* Llib/hash.scm 1059 */
													if ((BgL_arg1853z00_2333 == BgL_keyz00_154))
														{	/* Llib/hash.scm 1059 */
															BgL_test3065z00_8487 = ((bool_t) 1);
														}
													else
														{	/* Llib/hash.scm 1059 */
															if (STRINGP(BgL_arg1853z00_2333))
																{	/* Llib/hash.scm 1059 */
																	if (STRINGP(BgL_keyz00_154))
																		{	/* Llib/hash.scm 1059 */
																			long BgL_l1z00_4749;

																			BgL_l1z00_4749 =
																				STRING_LENGTH(BgL_arg1853z00_2333);
																			if (
																				(BgL_l1z00_4749 ==
																					STRING_LENGTH(BgL_keyz00_154)))
																				{	/* Llib/hash.scm 1059 */
																					int BgL_arg2041z00_4752;

																					{	/* Llib/hash.scm 1059 */
																						char *BgL_auxz00_8513;
																						char *BgL_tmpz00_8511;

																						BgL_auxz00_8513 =
																							BSTRING_TO_STRING(BgL_keyz00_154);
																						BgL_tmpz00_8511 =
																							BSTRING_TO_STRING
																							(BgL_arg1853z00_2333);
																						BgL_arg2041z00_4752 =
																							memcmp(BgL_tmpz00_8511,
																							BgL_auxz00_8513, BgL_l1z00_4749);
																					}
																					BgL_test3065z00_8487 =
																						(
																						(long) (BgL_arg2041z00_4752) == 0L);
																				}
																			else
																				{	/* Llib/hash.scm 1059 */
																					BgL_test3065z00_8487 = ((bool_t) 0);
																				}
																		}
																	else
																		{	/* Llib/hash.scm 1059 */
																			BgL_test3065z00_8487 = ((bool_t) 0);
																		}
																}
															else
																{	/* Llib/hash.scm 1059 */
																	BgL_test3065z00_8487 = ((bool_t) 0);
																}
														}
												}
										}
									}
									if (BgL_test3065z00_8487)
										{	/* Llib/hash.scm 1059 */
											{	/* Llib/hash.scm 1060 */
												obj_t BgL_arg1840z00_2316;

												BgL_arg1840z00_2316 = CDR(((obj_t) BgL_bucketz00_2312));
												VECTOR_SET(
													((obj_t) BgL_bucketsz00_2309),
													BgL_bucketzd2numzd2_2311, BgL_arg1840z00_2316);
											}
											{	/* Llib/hash.scm 1061 */
												long BgL_arg1842z00_2317;

												BgL_arg1842z00_2317 =
													(
													(long) CINT(STRUCT_REF(BgL_tablez00_153,
															(int) (0L))) - 1L);
												{	/* Llib/hash.scm 1061 */
													obj_t BgL_auxz00_8528;
													int BgL_tmpz00_8526;

													BgL_auxz00_8528 = BINT(BgL_arg1842z00_2317);
													BgL_tmpz00_8526 = (int) (0L);
													STRUCT_SET(BgL_tablez00_153, BgL_tmpz00_8526,
														BgL_auxz00_8528);
											}}
											return ((bool_t) 1);
										}
									else
										{	/* Llib/hash.scm 1064 */
											obj_t BgL_g1075z00_2319;

											BgL_g1075z00_2319 = CDR(((obj_t) BgL_bucketz00_2312));
											{
												obj_t BgL_bucketz00_2321;
												obj_t BgL_prevz00_2322;

												BgL_bucketz00_2321 = BgL_g1075z00_2319;
												BgL_prevz00_2322 = BgL_bucketz00_2312;
											BgL_zc3z04anonymousza31844ze3z87_2323:
												if (PAIRP(BgL_bucketz00_2321))
													{	/* Llib/hash.scm 1067 */
														bool_t BgL_test3072z00_8535;

														{	/* Llib/hash.scm 1067 */
															obj_t BgL_arg1852z00_2331;

															BgL_arg1852z00_2331 =
																CAR(CAR(BgL_bucketz00_2321));
															{	/* Llib/hash.scm 1067 */
																obj_t BgL_eqtz00_4769;

																BgL_eqtz00_4769 =
																	STRUCT_REF(BgL_tablez00_153, (int) (3L));
																if (PROCEDUREP(BgL_eqtz00_4769))
																	{	/* Llib/hash.scm 1067 */
																		BgL_test3072z00_8535 =
																			CBOOL(BGL_PROCEDURE_CALL2(BgL_eqtz00_4769,
																				BgL_arg1852z00_2331, BgL_keyz00_154));
																	}
																else
																	{	/* Llib/hash.scm 1067 */
																		if ((BgL_arg1852z00_2331 == BgL_keyz00_154))
																			{	/* Llib/hash.scm 1067 */
																				BgL_test3072z00_8535 = ((bool_t) 1);
																			}
																		else
																			{	/* Llib/hash.scm 1067 */
																				if (STRINGP(BgL_arg1852z00_2331))
																					{	/* Llib/hash.scm 1067 */
																						if (STRINGP(BgL_keyz00_154))
																							{	/* Llib/hash.scm 1067 */
																								long BgL_l1z00_4776;

																								BgL_l1z00_4776 =
																									STRING_LENGTH
																									(BgL_arg1852z00_2331);
																								if ((BgL_l1z00_4776 ==
																										STRING_LENGTH
																										(BgL_keyz00_154)))
																									{	/* Llib/hash.scm 1067 */
																										int BgL_arg2041z00_4779;

																										{	/* Llib/hash.scm 1067 */
																											char *BgL_auxz00_8560;
																											char *BgL_tmpz00_8558;

																											BgL_auxz00_8560 =
																												BSTRING_TO_STRING
																												(BgL_keyz00_154);
																											BgL_tmpz00_8558 =
																												BSTRING_TO_STRING
																												(BgL_arg1852z00_2331);
																											BgL_arg2041z00_4779 =
																												memcmp(BgL_tmpz00_8558,
																												BgL_auxz00_8560,
																												BgL_l1z00_4776);
																										}
																										BgL_test3072z00_8535 =
																											(
																											(long)
																											(BgL_arg2041z00_4779) ==
																											0L);
																									}
																								else
																									{	/* Llib/hash.scm 1067 */
																										BgL_test3072z00_8535 =
																											((bool_t) 0);
																									}
																							}
																						else
																							{	/* Llib/hash.scm 1067 */
																								BgL_test3072z00_8535 =
																									((bool_t) 0);
																							}
																					}
																				else
																					{	/* Llib/hash.scm 1067 */
																						BgL_test3072z00_8535 = ((bool_t) 0);
																					}
																			}
																	}
															}
														}
														if (BgL_test3072z00_8535)
															{	/* Llib/hash.scm 1067 */
																{	/* Llib/hash.scm 1069 */
																	obj_t BgL_arg1848z00_2327;

																	BgL_arg1848z00_2327 = CDR(BgL_bucketz00_2321);
																	{	/* Llib/hash.scm 1069 */
																		obj_t BgL_tmpz00_8566;

																		BgL_tmpz00_8566 =
																			((obj_t) BgL_prevz00_2322);
																		SET_CDR(BgL_tmpz00_8566,
																			BgL_arg1848z00_2327);
																	}
																}
																{	/* Llib/hash.scm 1071 */
																	long BgL_arg1849z00_2328;

																	BgL_arg1849z00_2328 =
																		(
																		(long) CINT(STRUCT_REF(BgL_tablez00_153,
																				(int) (0L))) - 1L);
																	{	/* Llib/hash.scm 1070 */
																		obj_t BgL_auxz00_8575;
																		int BgL_tmpz00_8573;

																		BgL_auxz00_8575 = BINT(BgL_arg1849z00_2328);
																		BgL_tmpz00_8573 = (int) (0L);
																		STRUCT_SET(BgL_tablez00_153,
																			BgL_tmpz00_8573, BgL_auxz00_8575);
																}}
																return ((bool_t) 1);
															}
														else
															{
																obj_t BgL_prevz00_8580;
																obj_t BgL_bucketz00_8578;

																BgL_bucketz00_8578 = CDR(BgL_bucketz00_2321);
																BgL_prevz00_8580 = BgL_bucketz00_2321;
																BgL_prevz00_2322 = BgL_prevz00_8580;
																BgL_bucketz00_2321 = BgL_bucketz00_8578;
																goto BgL_zc3z04anonymousza31844ze3z87_2323;
															}
													}
												else
													{	/* Llib/hash.scm 1066 */
														return ((bool_t) 0);
													}
											}
										}
								}
						}
					}
				}
			}
		}

	}



/* plain-hashtable-expand! */
	obj_t BGl_plainzd2hashtablezd2expandz12z12zz__hashz00(obj_t BgL_tablez00_156)
	{
		{	/* Llib/hash.scm 1087 */
			{	/* Llib/hash.scm 1088 */
				obj_t BgL_oldzd2buckszd2_2336;

				BgL_oldzd2buckszd2_2336 = STRUCT_REF(BgL_tablez00_156, (int) (2L));
				{	/* Llib/hash.scm 1089 */
					long BgL_newzd2lenzd2_2338;

					BgL_newzd2lenzd2_2338 =
						(2L * VECTOR_LENGTH(((obj_t) BgL_oldzd2buckszd2_2336)));
					{	/* Llib/hash.scm 1090 */
						obj_t BgL_maxzd2lenzd2_2339;

						BgL_maxzd2lenzd2_2339 = STRUCT_REF(BgL_tablez00_156, (int) (6L));
						{	/* Llib/hash.scm 1091 */

							{	/* Llib/hash.scm 1093 */
								obj_t BgL_nmaxz00_2340;

								{	/* Llib/hash.scm 1093 */
									obj_t BgL_a1138z00_2343;

									BgL_a1138z00_2343 = STRUCT_REF(BgL_tablez00_156, (int) (1L));
									{	/* Llib/hash.scm 1094 */
										obj_t BgL_b1139z00_2344;

										BgL_b1139z00_2344 =
											STRUCT_REF(BgL_tablez00_156, (int) (7L));
										{	/* Llib/hash.scm 1093 */

											{	/* Llib/hash.scm 1093 */
												bool_t BgL_test3078z00_8592;

												if (INTEGERP(BgL_a1138z00_2343))
													{	/* Llib/hash.scm 1093 */
														BgL_test3078z00_8592 = INTEGERP(BgL_b1139z00_2344);
													}
												else
													{	/* Llib/hash.scm 1093 */
														BgL_test3078z00_8592 = ((bool_t) 0);
													}
												if (BgL_test3078z00_8592)
													{	/* Llib/hash.scm 1093 */
														BgL_nmaxz00_2340 =
															BINT(
															((long) CINT(BgL_a1138z00_2343) *
																(long) CINT(BgL_b1139z00_2344)));
													}
												else
													{	/* Llib/hash.scm 1093 */
														BgL_nmaxz00_2340 =
															BGl_2za2za2zz__r4_numbers_6_5z00
															(BgL_a1138z00_2343, BgL_b1139z00_2344);
													}
											}
										}
									}
								}
								{	/* Llib/hash.scm 1096 */
									obj_t BgL_arg1856z00_2341;

									if (REALP(BgL_nmaxz00_2340))
										{	/* Llib/hash.scm 1096 */
											BgL_arg1856z00_2341 =
												BINT((long) (REAL_TO_DOUBLE(BgL_nmaxz00_2340)));
										}
									else
										{	/* Llib/hash.scm 1096 */
											BgL_arg1856z00_2341 = BgL_nmaxz00_2340;
										}
									{	/* Llib/hash.scm 1095 */
										int BgL_tmpz00_8606;

										BgL_tmpz00_8606 = (int) (1L);
										STRUCT_SET(BgL_tablez00_156, BgL_tmpz00_8606,
											BgL_arg1856z00_2341);
							}}}
							{	/* Llib/hash.scm 1098 */
								bool_t BgL_test3081z00_8609;

								if (((long) CINT(BgL_maxzd2lenzd2_2339) < 0L))
									{	/* Llib/hash.scm 1098 */
										BgL_test3081z00_8609 = ((bool_t) 1);
									}
								else
									{	/* Llib/hash.scm 1098 */
										BgL_test3081z00_8609 =
											(BgL_newzd2lenzd2_2338 <=
											(long) CINT(BgL_maxzd2lenzd2_2339));
									}
								if (BgL_test3081z00_8609)
									{	/* Llib/hash.scm 1099 */
										obj_t BgL_newzd2buckszd2_2348;

										BgL_newzd2buckszd2_2348 =
											make_vector(BgL_newzd2lenzd2_2338, BNIL);
										{	/* Llib/hash.scm 1100 */
											int BgL_tmpz00_8616;

											BgL_tmpz00_8616 = (int) (2L);
											STRUCT_SET(BgL_tablez00_156, BgL_tmpz00_8616,
												BgL_newzd2buckszd2_2348);
										}
										{
											long BgL_iz00_2350;

											{	/* Llib/hash.scm 1101 */
												bool_t BgL_tmpz00_8619;

												BgL_iz00_2350 = 0L;
											BgL_zc3z04anonymousza31861ze3z87_2351:
												if (
													(BgL_iz00_2350 <
														VECTOR_LENGTH(((obj_t) BgL_oldzd2buckszd2_2336))))
													{	/* Llib/hash.scm 1102 */
														{	/* Llib/hash.scm 1103 */
															obj_t BgL_g1142z00_2353;

															BgL_g1142z00_2353 =
																VECTOR_REF(
																((obj_t) BgL_oldzd2buckszd2_2336),
																BgL_iz00_2350);
															{
																obj_t BgL_l1140z00_2355;

																BgL_l1140z00_2355 = BgL_g1142z00_2353;
															BgL_zc3z04anonymousza31863ze3z87_2356:
																if (PAIRP(BgL_l1140z00_2355))
																	{	/* Llib/hash.scm 1109 */
																		{	/* Llib/hash.scm 1104 */
																			obj_t BgL_cellz00_2358;

																			BgL_cellz00_2358 = CAR(BgL_l1140z00_2355);
																			{	/* Llib/hash.scm 1104 */
																				obj_t BgL_keyz00_2359;

																				BgL_keyz00_2359 =
																					CAR(((obj_t) BgL_cellz00_2358));
																				{	/* Llib/hash.scm 1104 */
																					long BgL_nz00_2360;

																					{	/* Llib/hash.scm 1105 */
																						obj_t BgL_hashnz00_4811;

																						BgL_hashnz00_4811 =
																							STRUCT_REF(BgL_tablez00_156,
																							(int) (4L));
																						if (PROCEDUREP(BgL_hashnz00_4811))
																							{	/* Llib/hash.scm 1105 */
																								obj_t BgL_arg1318z00_4813;

																								BgL_arg1318z00_4813 =
																									BGL_PROCEDURE_CALL1
																									(BgL_hashnz00_4811,
																									BgL_keyz00_2359);
																								{	/* Llib/hash.scm 1105 */
																									long BgL_nz00_4815;

																									BgL_nz00_4815 =
																										(long)
																										CINT(BgL_arg1318z00_4813);
																									if ((BgL_nz00_4815 < 0L))
																										{	/* Llib/hash.scm 1105 */
																											BgL_nz00_2360 =
																												NEG(BgL_nz00_4815);
																										}
																									else
																										{	/* Llib/hash.scm 1105 */
																											BgL_nz00_2360 =
																												BgL_nz00_4815;
																										}
																								}
																							}
																						else
																							{	/* Llib/hash.scm 1105 */
																								if (
																									(BgL_hashnz00_4811 ==
																										BGl_symbol2567z00zz__hashz00))
																									{	/* Llib/hash.scm 1105 */
																										BgL_nz00_2360 =
																											BGl_objzd2hashze70z35zz__hashz00
																											(BgL_keyz00_2359);
																									}
																								else
																									{	/* Llib/hash.scm 1105 */
																										BgL_nz00_2360 =
																											BGl_getzd2hashnumberzd2zz__hashz00
																											(BgL_keyz00_2359);
																									}
																							}
																					}
																					{	/* Llib/hash.scm 1105 */
																						long BgL_hz00_2361;

																						{	/* Llib/hash.scm 1106 */
																							long BgL_n1z00_4819;
																							long BgL_n2z00_4820;

																							BgL_n1z00_4819 = BgL_nz00_2360;
																							BgL_n2z00_4820 =
																								BgL_newzd2lenzd2_2338;
																							{	/* Llib/hash.scm 1106 */
																								bool_t BgL_test3088z00_8647;

																								{	/* Llib/hash.scm 1106 */
																									long BgL_arg2151z00_4822;

																									BgL_arg2151z00_4822 =
																										(((BgL_n1z00_4819) |
																											(BgL_n2z00_4820)) &
																										-2147483648);
																									BgL_test3088z00_8647 =
																										(BgL_arg2151z00_4822 == 0L);
																								}
																								if (BgL_test3088z00_8647)
																									{	/* Llib/hash.scm 1106 */
																										int32_t BgL_arg2148z00_4823;

																										{	/* Llib/hash.scm 1106 */
																											int32_t
																												BgL_arg2149z00_4824;
																											int32_t
																												BgL_arg2150z00_4825;
																											BgL_arg2149z00_4824 =
																												(int32_t)
																												(BgL_n1z00_4819);
																											BgL_arg2150z00_4825 =
																												(int32_t)
																												(BgL_n2z00_4820);
																											BgL_arg2148z00_4823 =
																												(BgL_arg2149z00_4824 %
																												BgL_arg2150z00_4825);
																										}
																										{	/* Llib/hash.scm 1106 */
																											long BgL_arg2252z00_4830;

																											BgL_arg2252z00_4830 =
																												(long)
																												(BgL_arg2148z00_4823);
																											BgL_hz00_2361 =
																												(long)
																												(BgL_arg2252z00_4830);
																									}}
																								else
																									{	/* Llib/hash.scm 1106 */
																										BgL_hz00_2361 =
																											(BgL_n1z00_4819 %
																											BgL_n2z00_4820);
																									}
																							}
																						}
																						{	/* Llib/hash.scm 1106 */

																							{	/* Llib/hash.scm 1108 */
																								obj_t BgL_arg1866z00_2362;

																								BgL_arg1866z00_2362 =
																									MAKE_YOUNG_PAIR
																									(BgL_cellz00_2358,
																									VECTOR_REF
																									(BgL_newzd2buckszd2_2348,
																										BgL_hz00_2361));
																								VECTOR_SET
																									(BgL_newzd2buckszd2_2348,
																									BgL_hz00_2361,
																									BgL_arg1866z00_2362);
																							}
																						}
																					}
																				}
																			}
																		}
																		{
																			obj_t BgL_l1140z00_8659;

																			BgL_l1140z00_8659 =
																				CDR(BgL_l1140z00_2355);
																			BgL_l1140z00_2355 = BgL_l1140z00_8659;
																			goto
																				BgL_zc3z04anonymousza31863ze3z87_2356;
																		}
																	}
																else
																	{	/* Llib/hash.scm 1109 */
																		((bool_t) 1);
																	}
															}
														}
														{
															long BgL_iz00_8661;

															BgL_iz00_8661 = (BgL_iz00_2350 + 1L);
															BgL_iz00_2350 = BgL_iz00_8661;
															goto BgL_zc3z04anonymousza31861ze3z87_2351;
														}
													}
												else
													{	/* Llib/hash.scm 1102 */
														BgL_tmpz00_8619 = ((bool_t) 0);
													}
												return BBOOL(BgL_tmpz00_8619);
											}
										}
									}
								else
									{	/* Llib/hash.scm 1114 */
										obj_t BgL_arg1872z00_2368;

										{	/* Llib/hash.scm 1114 */
											long BgL_arg1873z00_2369;

											BgL_arg1873z00_2369 =
												(long) CINT(STRUCT_REF(BgL_tablez00_156, (int) (0L)));
											{	/* Llib/hash.scm 1112 */
												obj_t BgL_list1874z00_2370;

												{	/* Llib/hash.scm 1112 */
													obj_t BgL_arg1875z00_2371;

													{	/* Llib/hash.scm 1112 */
														obj_t BgL_arg1876z00_2372;

														BgL_arg1876z00_2372 =
															MAKE_YOUNG_PAIR(BINT(BgL_arg1873z00_2369), BNIL);
														BgL_arg1875z00_2371 =
															MAKE_YOUNG_PAIR(BgL_maxzd2lenzd2_2339,
															BgL_arg1876z00_2372);
													}
													BgL_list1874z00_2370 =
														MAKE_YOUNG_PAIR(BINT(BgL_newzd2lenzd2_2338),
														BgL_arg1875z00_2371);
												}
												BgL_arg1872z00_2368 =
													BGl_formatz00zz__r4_output_6_10_3z00
													(BGl_string2602z00zz__hashz00, BgL_list1874z00_2370);
										}}
										return
											BGl_errorz00zz__errorz00(BGl_string2603z00zz__hashz00,
											BgL_arg1872z00_2368, BgL_tablez00_156);
									}
							}
						}
					}
				}
			}
		}

	}



/* hashtable-collisions */
	BGL_EXPORTED_DEF obj_t BGl_hashtablezd2collisionszd2zz__hashz00(obj_t
		BgL_tablez00_157)
	{
		{	/* Llib/hash.scm 1120 */
			{	/* Llib/hash.scm 1121 */
				bool_t BgL_test3089z00_8674;

				if (
					(0L == ((long) CINT(STRUCT_REF(BgL_tablez00_157, (int) (5L))) & 3L)))
					{	/* Llib/hash.scm 288 */
						BgL_test3089z00_8674 = ((bool_t) 0);
					}
				else
					{	/* Llib/hash.scm 288 */
						BgL_test3089z00_8674 = ((bool_t) 1);
					}
				if (BgL_test3089z00_8674)
					{	/* Llib/hash.scm 1121 */
						return BNIL;
					}
				else
					{	/* Llib/hash.scm 1121 */
						BGL_TAIL return
							BGl_plainzd2hashtablezd2collisionsz00zz__hashz00
							(BgL_tablez00_157);
					}
			}
		}

	}



/* &hashtable-collisions */
	obj_t BGl_z62hashtablezd2collisionszb0zz__hashz00(obj_t BgL_envz00_5484,
		obj_t BgL_tablez00_5485)
	{
		{	/* Llib/hash.scm 1120 */
			{	/* Llib/hash.scm 1121 */
				obj_t BgL_auxz00_8682;

				if (STRUCTP(BgL_tablez00_5485))
					{	/* Llib/hash.scm 1121 */
						BgL_auxz00_8682 = BgL_tablez00_5485;
					}
				else
					{
						obj_t BgL_auxz00_8685;

						BgL_auxz00_8685 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
							BINT(45206L), BGl_string2604z00zz__hashz00,
							BGl_string2572z00zz__hashz00, BgL_tablez00_5485);
						FAILURE(BgL_auxz00_8685, BFALSE, BFALSE);
					}
				return BGl_hashtablezd2collisionszd2zz__hashz00(BgL_auxz00_8682);
			}
		}

	}



/* plain-hashtable-collisions */
	obj_t BGl_plainzd2hashtablezd2collisionsz00zz__hashz00(obj_t BgL_tablez00_158)
	{
		{	/* Llib/hash.scm 1128 */
			{	/* Llib/hash.scm 1129 */
				obj_t BgL_bucketsz00_2375;

				BgL_bucketsz00_2375 = STRUCT_REF(BgL_tablez00_158, (int) (2L));
				{	/* Llib/hash.scm 1130 */

					{
						long BgL_iz00_2379;
						obj_t BgL_resz00_2380;

						BgL_iz00_2379 = 0L;
						BgL_resz00_2380 = BNIL;
					BgL_zc3z04anonymousza31878ze3z87_2381:
						if ((BgL_iz00_2379 == VECTOR_LENGTH(((obj_t) BgL_bucketsz00_2375))))
							{	/* Llib/hash.scm 1133 */
								return BgL_resz00_2380;
							}
						else
							{	/* Llib/hash.scm 1135 */
								obj_t BgL_g1078z00_2383;

								BgL_g1078z00_2383 =
									VECTOR_REF(((obj_t) BgL_bucketsz00_2375), BgL_iz00_2379);
								{
									obj_t BgL_bucketz00_4861;
									obj_t BgL_resz00_4862;
									long BgL_collz00_4863;

									BgL_bucketz00_4861 = BgL_g1078z00_2383;
									BgL_resz00_4862 = BgL_resz00_2380;
									BgL_collz00_4863 = 0L;
								BgL_liipz00_4860:
									if (NULLP(BgL_bucketz00_4861))
										{
											obj_t BgL_resz00_8702;
											long BgL_iz00_8700;

											BgL_iz00_8700 = (BgL_iz00_2379 + 1L);
											BgL_resz00_8702 = BgL_resz00_4862;
											BgL_resz00_2380 = BgL_resz00_8702;
											BgL_iz00_2379 = BgL_iz00_8700;
											goto BgL_zc3z04anonymousza31878ze3z87_2381;
										}
									else
										{	/* Llib/hash.scm 1140 */
											obj_t BgL_arg1883z00_4867;
											obj_t BgL_arg1884z00_4868;
											long BgL_arg1885z00_4869;

											BgL_arg1883z00_4867 = CDR(((obj_t) BgL_bucketz00_4861));
											if ((BgL_collz00_4863 > 0L))
												{	/* Llib/hash.scm 1141 */
													BgL_arg1884z00_4868 =
														MAKE_YOUNG_PAIR(BINT(BgL_collz00_4863),
														BgL_resz00_4862);
												}
											else
												{	/* Llib/hash.scm 1141 */
													BgL_arg1884z00_4868 = BgL_resz00_4862;
												}
											BgL_arg1885z00_4869 = (BgL_collz00_4863 + 1L);
											{
												long BgL_collz00_8712;
												obj_t BgL_resz00_8711;
												obj_t BgL_bucketz00_8710;

												BgL_bucketz00_8710 = BgL_arg1883z00_4867;
												BgL_resz00_8711 = BgL_arg1884z00_4868;
												BgL_collz00_8712 = BgL_arg1885z00_4869;
												BgL_collz00_4863 = BgL_collz00_8712;
												BgL_resz00_4862 = BgL_resz00_8711;
												BgL_bucketz00_4861 = BgL_bucketz00_8710;
												goto BgL_liipz00_4860;
											}
										}
								}
							}
					}
				}
			}
		}

	}



/* get-hashnumber */
	BGL_EXPORTED_DEF long BGl_getzd2hashnumberzd2zz__hashz00(obj_t BgL_keyz00_160)
	{
		{	/* Llib/hash.scm 1153 */
		BGl_getzd2hashnumberzd2zz__hashz00:
			if (STRINGP(BgL_keyz00_160))
				{	/* Llib/hash.scm 1156 */
					long BgL_arg1888z00_2398;

					{	/* Llib/hash.scm 1267 */
						long BgL_arg1969z00_4875;

						BgL_arg1969z00_4875 = STRING_LENGTH(BgL_keyz00_160);
						BgL_arg1888z00_2398 =
							bgl_string_hash(BSTRING_TO_STRING(BgL_keyz00_160),
							(int) (0L), (int) (BgL_arg1969z00_4875));
					}
					if ((BgL_arg1888z00_2398 < 0L))
						{	/* Llib/hash.scm 1156 */
							return NEG(BgL_arg1888z00_2398);
						}
					else
						{	/* Llib/hash.scm 1156 */
							return BgL_arg1888z00_2398;
						}
				}
			else
				{	/* Llib/hash.scm 1155 */
					if (SYMBOLP(BgL_keyz00_160))
						{	/* Llib/hash.scm 1158 */
							long BgL_arg1890z00_2400;

							BgL_arg1890z00_2400 = bgl_symbol_hash_number(BgL_keyz00_160);
							if ((BgL_arg1890z00_2400 < 0L))
								{	/* Llib/hash.scm 1158 */
									return NEG(BgL_arg1890z00_2400);
								}
							else
								{	/* Llib/hash.scm 1158 */
									return BgL_arg1890z00_2400;
								}
						}
					else
						{	/* Llib/hash.scm 1157 */
							if (KEYWORDP(BgL_keyz00_160))
								{	/* Llib/hash.scm 1160 */
									long BgL_arg1892z00_2402;

									BgL_arg1892z00_2402 = bgl_keyword_hash_number(BgL_keyz00_160);
									if ((BgL_arg1892z00_2402 < 0L))
										{	/* Llib/hash.scm 1160 */
											return NEG(BgL_arg1892z00_2402);
										}
									else
										{	/* Llib/hash.scm 1160 */
											return BgL_arg1892z00_2402;
										}
								}
							else
								{	/* Llib/hash.scm 1159 */
									if (INTEGERP(BgL_keyz00_160))
										{	/* Llib/hash.scm 1162 */
											long BgL_nz00_4889;

											BgL_nz00_4889 = (long) CINT(BgL_keyz00_160);
											if ((BgL_nz00_4889 < 0L))
												{	/* Llib/hash.scm 1162 */
													return NEG(BgL_nz00_4889);
												}
											else
												{	/* Llib/hash.scm 1162 */
													return BgL_nz00_4889;
												}
										}
									else
										{	/* Llib/hash.scm 1161 */
											if (ELONGP(BgL_keyz00_160))
												{	/* Llib/hash.scm 1164 */
													long BgL_arg1896z00_2405;

													{	/* Llib/hash.scm 1164 */
														long BgL_tmpz00_8743;

														BgL_tmpz00_8743 = BELONG_TO_LONG(BgL_keyz00_160);
														BgL_arg1896z00_2405 = (long) (BgL_tmpz00_8743);
													}
													if ((BgL_arg1896z00_2405 < 0L))
														{	/* Llib/hash.scm 1164 */
															return NEG(BgL_arg1896z00_2405);
														}
													else
														{	/* Llib/hash.scm 1164 */
															return BgL_arg1896z00_2405;
														}
												}
											else
												{	/* Llib/hash.scm 1163 */
													if (LLONGP(BgL_keyz00_160))
														{	/* Llib/hash.scm 1166 */
															long BgL_arg1898z00_2407;

															{	/* Llib/hash.scm 1166 */
																BGL_LONGLONG_T BgL_tmpz00_8751;

																BgL_tmpz00_8751 =
																	BLLONG_TO_LLONG(BgL_keyz00_160);
																BgL_arg1898z00_2407 = (long) (BgL_tmpz00_8751);
															}
															if ((BgL_arg1898z00_2407 < 0L))
																{	/* Llib/hash.scm 1166 */
																	return NEG(BgL_arg1898z00_2407);
																}
															else
																{	/* Llib/hash.scm 1166 */
																	return BgL_arg1898z00_2407;
																}
														}
													else
														{	/* Llib/hash.scm 1165 */
															if (BGL_OBJECTP(BgL_keyz00_160))
																{	/* Llib/hash.scm 1168 */
																	long BgL_arg1901z00_2409;

																	BgL_arg1901z00_2409 =
																		BGl_objectzd2hashnumberzd2zz__objectz00(
																		((BgL_objectz00_bglt) BgL_keyz00_160));
																	if ((BgL_arg1901z00_2409 < 0L))
																		{	/* Llib/hash.scm 1168 */
																			return NEG(BgL_arg1901z00_2409);
																		}
																	else
																		{	/* Llib/hash.scm 1168 */
																			return BgL_arg1901z00_2409;
																		}
																}
															else
																{	/* Llib/hash.scm 1167 */
																	if (FOREIGNP(BgL_keyz00_160))
																		{	/* Llib/hash.scm 1170 */
																			long BgL_arg1903z00_2411;

																			BgL_arg1903z00_2411 =
																				bgl_foreign_hash_number(BgL_keyz00_160);
																			if ((BgL_arg1903z00_2411 < 0L))
																				{	/* Llib/hash.scm 1170 */
																					return NEG(BgL_arg1903z00_2411);
																				}
																			else
																				{	/* Llib/hash.scm 1170 */
																					return BgL_arg1903z00_2411;
																				}
																		}
																	else
																		{	/* Llib/hash.scm 1171 */
																			bool_t BgL_test3111z00_8770;

																			if (INTEGERP(BgL_keyz00_160))
																				{	/* Llib/hash.scm 1171 */
																					BgL_test3111z00_8770 = ((bool_t) 1);
																				}
																			else
																				{	/* Llib/hash.scm 1171 */
																					BgL_test3111z00_8770 =
																						REALP(BgL_keyz00_160);
																				}
																			if (BgL_test3111z00_8770)
																				{	/* Llib/hash.scm 1172 */
																					long BgL_arg1906z00_2413;

																					BgL_arg1906z00_2413 =
																						(long) (REAL_TO_DOUBLE
																						(BgL_keyz00_160));
																					{
																						obj_t BgL_keyz00_8776;

																						BgL_keyz00_8776 =
																							BINT(BgL_arg1906z00_2413);
																						BgL_keyz00_160 = BgL_keyz00_8776;
																						goto
																							BGl_getzd2hashnumberzd2zz__hashz00;
																					}
																				}
																			else
																				{	/* Llib/hash.scm 1174 */
																					long BgL_arg1910z00_2414;

																					BgL_arg1910z00_2414 =
																						bgl_obj_hash_number(BgL_keyz00_160);
																					if ((BgL_arg1910z00_2414 < 0L))
																						{	/* Llib/hash.scm 1174 */
																							return NEG(BgL_arg1910z00_2414);
																						}
																					else
																						{	/* Llib/hash.scm 1174 */
																							return BgL_arg1910z00_2414;
																						}
																				}
																		}
																}
														}
												}
										}
								}
						}
				}
		}

	}



/* &get-hashnumber */
	obj_t BGl_z62getzd2hashnumberzb0zz__hashz00(obj_t BgL_envz00_5486,
		obj_t BgL_keyz00_5487)
	{
		{	/* Llib/hash.scm 1153 */
			return BINT(BGl_getzd2hashnumberzd2zz__hashz00(BgL_keyz00_5487));
		}

	}



/* get-hashnumber-persistent */
	BGL_EXPORTED_DEF long BGl_getzd2hashnumberzd2persistentz00zz__hashz00(obj_t
		BgL_keyz00_161)
	{
		{	/* Llib/hash.scm 1181 */
			BGL_TAIL return BGl_objzd2hashze70z35zz__hashz00(BgL_keyz00_161);
		}

	}



/* obj-hash~0 */
	long BGl_objzd2hashze70z35zz__hashz00(obj_t BgL_keyz00_2463)
	{
		{	/* Llib/hash.scm 1247 */
		BGl_objzd2hashze70z35zz__hashz00:
			{
				obj_t BgL_keyz00_2421;
				obj_t BgL_keyz00_2446;

				if (CNSTP(BgL_keyz00_2463))
					{	/* Llib/hash.scm 1212 */
						if ((BgL_keyz00_2463 == BTRUE))
							{	/* Llib/hash.scm 1214 */
								return 12L;
							}
						else
							{	/* Llib/hash.scm 1214 */
								if ((BgL_keyz00_2463 == BFALSE))
									{	/* Llib/hash.scm 1215 */
										return 445L;
									}
								else
									{	/* Llib/hash.scm 1215 */
										if ((BgL_keyz00_2463 == BUNSPEC))
											{	/* Llib/hash.scm 1216 */
												return 3199L;
											}
										else
											{	/* Llib/hash.scm 1216 */
												if ((BgL_keyz00_2463 == BNIL))
													{	/* Llib/hash.scm 1217 */
														return 453343L;
													}
												else
													{	/* Llib/hash.scm 1217 */
														return 21354L;
													}
											}
									}
							}
					}
				else
					{	/* Llib/hash.scm 1212 */
						if (STRINGP(BgL_keyz00_2463))
							{	/* Llib/hash.scm 1220 */
								long BgL_arg1938z00_2467;

								{	/* Llib/hash.scm 1220 */
									long BgL_arg1939z00_2468;

									BgL_arg1939z00_2468 = STRING_LENGTH(BgL_keyz00_2463);
									BgL_arg1938z00_2467 =
										bgl_string_hash_persistent(BSTRING_TO_STRING
										(BgL_keyz00_2463), (int) (0L), (int) (BgL_arg1939z00_2468));
								}
								return (134217727L & BgL_arg1938z00_2467);
							}
						else
							{	/* Llib/hash.scm 1219 */
								if (SYMBOLP(BgL_keyz00_2463))
									{	/* Llib/hash.scm 1221 */
										return
											(134217727L &
											bgl_symbol_hash_number_persistent(BgL_keyz00_2463));
									}
								else
									{	/* Llib/hash.scm 1221 */
										if (KEYWORDP(BgL_keyz00_2463))
											{	/* Llib/hash.scm 1223 */
												return
													(134217727L &
													bgl_keyword_hash_number_persistent(BgL_keyz00_2463));
											}
										else
											{	/* Llib/hash.scm 1223 */
												if (CHARP(BgL_keyz00_2463))
													{	/* Llib/hash.scm 1225 */
														return (CCHAR(BgL_keyz00_2463));
													}
												else
													{	/* Llib/hash.scm 1225 */
														if (INTEGERP(BgL_keyz00_2463))
															{	/* Llib/hash.scm 1227 */
																return
																	(134217727L & (long) CINT(BgL_keyz00_2463));
															}
														else
															{	/* Llib/hash.scm 1227 */
																if (ELONGP(BgL_keyz00_2463))
																	{	/* Llib/hash.scm 1230 */
																		long BgL_arg1947z00_2476;

																		{	/* Llib/hash.scm 1230 */
																			long BgL_tmpz00_8821;

																			BgL_tmpz00_8821 =
																				BELONG_TO_LONG(BgL_keyz00_2463);
																			BgL_arg1947z00_2476 =
																				(long) (BgL_tmpz00_8821);
																		}
																		return (134217727L & BgL_arg1947z00_2476);
																	}
																else
																	{	/* Llib/hash.scm 1229 */
																		if (LLONGP(BgL_keyz00_2463))
																			{	/* Llib/hash.scm 1232 */
																				long BgL_arg1949z00_2478;

																				{	/* Llib/hash.scm 1232 */
																					BGL_LONGLONG_T BgL_tmpz00_8827;

																					BgL_tmpz00_8827 =
																						BLLONG_TO_LLONG(BgL_keyz00_2463);
																					BgL_arg1949z00_2478 =
																						(long) (BgL_tmpz00_8827);
																				}
																				return
																					(134217727L & BgL_arg1949z00_2478);
																			}
																		else
																			{	/* Llib/hash.scm 1231 */
																				if (UCS2P(BgL_keyz00_2463))
																					{	/* Llib/hash.scm 1234 */
																						long BgL_arg1951z00_2480;

																						{	/* Llib/hash.scm 1234 */
																							int BgL_arg1952z00_2481;

																							{	/* Llib/hash.scm 1234 */
																								ucs2_t BgL_ucs2z00_5063;

																								BgL_ucs2z00_5063 =
																									CUCS2(BgL_keyz00_2463);
																								{	/* Llib/hash.scm 1234 */
																									obj_t BgL_tmpz00_8834;

																									BgL_tmpz00_8834 =
																										BUCS2(BgL_ucs2z00_5063);
																									BgL_arg1952z00_2481 =
																										CUCS2(BgL_tmpz00_8834);
																							}}
																							BgL_arg1951z00_2480 =
																								(39434L ^
																								(long) (BgL_arg1952z00_2481));
																						}
																						return
																							(134217727L &
																							BgL_arg1951z00_2480);
																					}
																				else
																					{	/* Llib/hash.scm 1233 */
																						if (BGL_DATEP(BgL_keyz00_2463))
																							{	/* Llib/hash.scm 1236 */
																								long BgL_arg1954z00_2483;

																								{	/* Llib/hash.scm 1236 */
																									long BgL_arg1955z00_2484;

																									{	/* Llib/hash.scm 1236 */
																										long BgL_arg1956z00_2485;

																										BgL_arg1956z00_2485 =
																											bgl_date_to_seconds
																											(BgL_keyz00_2463);
																										BgL_arg1955z00_2484 =
																											BGl_objzd2hashze70z35zz__hashz00
																											(make_belong
																											(BgL_arg1956z00_2485));
																									}
																									BgL_arg1954z00_2483 =
																										(908L ^
																										BgL_arg1955z00_2484);
																								}
																								return
																									(134217727L &
																									BgL_arg1954z00_2483);
																							}
																						else
																							{	/* Llib/hash.scm 1237 */
																								bool_t BgL_test3128z00_8847;

																								if (INTEGERP(BgL_keyz00_2463))
																									{	/* Llib/hash.scm 1237 */
																										BgL_test3128z00_8847 =
																											((bool_t) 1);
																									}
																								else
																									{	/* Llib/hash.scm 1237 */
																										BgL_test3128z00_8847 =
																											REALP(BgL_keyz00_2463);
																									}
																								if (BgL_test3128z00_8847)
																									{	/* Llib/hash.scm 1240 */
																										long BgL_arg1958z00_2487;

																										{	/* Llib/hash.scm 1240 */
																											int64_t
																												BgL_arg1959z00_2488;
																											{	/* Llib/hash.scm 1240 */
																												int64_t
																													BgL_arg1960z00_2489;
																												int64_t
																													BgL_arg1961z00_2490;
																												{	/* Llib/hash.scm 1240 */
																													double
																														BgL_arg1962z00_2491;
																													BgL_arg1962z00_2491 =
																														(REAL_TO_DOUBLE
																														(BgL_keyz00_2463) *
																														((double) 1000.0));
																													BgL_arg1960z00_2489 =
																														(int64_t)
																														(BgL_arg1962z00_2491);
																												}
																												BgL_arg1961z00_2490 =
																													((int64_t) (1) <<
																													(int) (29L));
																												BgL_arg1959z00_2488 =
																													(BgL_arg1960z00_2489 &
																													BgL_arg1961z00_2490);
																											}
																											{	/* Llib/hash.scm 1239 */
																												long
																													BgL_arg2251z00_5076;
																												BgL_arg2251z00_5076 =
																													(long)
																													(BgL_arg1959z00_2488);
																												BgL_arg1958z00_2487 =
																													(long)
																													(BgL_arg2251z00_5076);
																										}}
																										{
																											obj_t BgL_keyz00_8859;

																											BgL_keyz00_8859 =
																												BINT
																												(BgL_arg1958z00_2487);
																											BgL_keyz00_2463 =
																												BgL_keyz00_8859;
																											goto
																												BGl_objzd2hashze70z35zz__hashz00;
																										}
																									}
																								else
																									{	/* Llib/hash.scm 1237 */
																										if (UCS2_STRINGP
																											(BgL_keyz00_2463))
																											{	/* Llib/hash.scm 1184 */
																												long BgL_tmpz00_8863;

																												BgL_keyz00_2446 =
																													BgL_keyz00_2463;
																												{	/* Llib/hash.scm 1200 */
																													int BgL_lenz00_2448;

																													BgL_lenz00_2448 =
																														UCS2_STRING_LENGTH
																														(BgL_keyz00_2446);
																													{	/* Llib/hash.scm 1201 */
																														long
																															BgL_g1081z00_2449;
																														long
																															BgL_g1082z00_2450;
																														BgL_g1081z00_2449 =
																															((long)
																															(BgL_lenz00_2448)
																															- 1L);
																														BgL_g1082z00_2450 =
																															(134217727L &
																															(235643L ^
																																(long)
																																(BgL_lenz00_2448)));
																														{
																															long
																																BgL_iz00_5022;
																															long
																																BgL_accz00_5023;
																															BgL_iz00_5022 =
																																BgL_g1081z00_2449;
																															BgL_accz00_5023 =
																																BgL_g1082z00_2450;
																														BgL_loopz00_5021:
																															if (
																																(BgL_iz00_5022
																																	== -1L))
																																{	/* Llib/hash.scm 1203 */
																																	BgL_tmpz00_8863
																																		=
																																		BgL_accz00_5023;
																																}
																															else
																																{	/* Llib/hash.scm 1205 */
																																	long
																																		BgL_arg1929z00_5031;
																																	long
																																		BgL_arg1930z00_5032;
																																	BgL_arg1929z00_5031
																																		=
																																		(BgL_iz00_5022
																																		- 1L);
																																	{	/* Llib/hash.scm 1208 */
																																		long
																																			BgL_arg1931z00_5033;
																																		{	/* Llib/hash.scm 1208 */
																																			long
																																				BgL_arg1932z00_5034;
																																			{	/* Llib/hash.scm 1208 */
																																				ucs2_t
																																					BgL_arg1933z00_5035;
																																				{	/* Llib/hash.scm 1208 */
																																					ucs2_t
																																						BgL_res2395z00_5051;
																																					{	/* Llib/hash.scm 1208 */
																																						int
																																							BgL_kz00_5039;
																																						BgL_kz00_5039
																																							=
																																							(int)
																																							(BgL_iz00_5022);
																																						{	/* Llib/hash.scm 1208 */
																																							bool_t
																																								BgL_test3132z00_8874;
																																							{	/* Llib/hash.scm 1208 */
																																								long
																																									BgL_auxz00_8877;
																																								long
																																									BgL_tmpz00_8875;
																																								BgL_auxz00_8877
																																									=
																																									(long)
																																									(UCS2_STRING_LENGTH
																																									(BgL_keyz00_2446));
																																								BgL_tmpz00_8875
																																									=
																																									(long)
																																									(BgL_kz00_5039);
																																								BgL_test3132z00_8874
																																									=
																																									BOUND_CHECK
																																									(BgL_tmpz00_8875,
																																									BgL_auxz00_8877);
																																							}
																																							if (BgL_test3132z00_8874)
																																								{	/* Llib/hash.scm 1208 */
																																									BgL_res2395z00_5051
																																										=
																																										UCS2_STRING_REF
																																										(BgL_keyz00_2446,
																																										BgL_kz00_5039);
																																								}
																																							else
																																								{	/* Llib/hash.scm 1208 */
																																									obj_t
																																										BgL_arg2306z00_5042;
																																									{	/* Llib/hash.scm 1208 */
																																										obj_t
																																											BgL_arg2307z00_5043;
																																										{	/* Llib/hash.scm 1208 */

																																											BgL_arg2307z00_5043
																																												=
																																												BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
																																												(
																																												((long) (UCS2_STRING_LENGTH(BgL_keyz00_2446)) - 1L), 10L);
																																										}
																																										BgL_arg2306z00_5042
																																											=
																																											string_append_3
																																											(BGl_string2605z00zz__hashz00,
																																											BgL_arg2307z00_5043,
																																											BGl_string2606z00zz__hashz00);
																																									}
																																									BgL_res2395z00_5051
																																										=
																																										CUCS2
																																										(BGl_errorz00zz__errorz00
																																										(BGl_symbol2607z00zz__hashz00,
																																											BgL_arg2306z00_5042,
																																											BINT
																																											(BgL_kz00_5039)));
																																					}}}
																																					BgL_arg1933z00_5035
																																						=
																																						BgL_res2395z00_5051;
																																				}
																																				BgL_arg1932z00_5034
																																					=
																																					BGl_objzd2hashze70z35zz__hashz00
																																					(BUCS2
																																					(BgL_arg1933z00_5035));
																																			}
																																			BgL_arg1931z00_5033
																																				=
																																				(BgL_arg1932z00_5034
																																				^
																																				BgL_accz00_5023);
																																		}
																																		BgL_arg1930z00_5032
																																			=
																																			(134217727L
																																			&
																																			BgL_arg1931z00_5033);
																																	}
																																	{
																																		long
																																			BgL_accz00_8895;
																																		long
																																			BgL_iz00_8894;
																																		BgL_iz00_8894
																																			=
																																			BgL_arg1929z00_5031;
																																		BgL_accz00_8895
																																			=
																																			BgL_arg1930z00_5032;
																																		BgL_accz00_5023
																																			=
																																			BgL_accz00_8895;
																																		BgL_iz00_5022
																																			=
																																			BgL_iz00_8894;
																																		goto
																																			BgL_loopz00_5021;
																																	}
																																}
																														}
																													}
																												}
																												return
																													(134217727L &
																													BgL_tmpz00_8863);
																											}
																										else
																											{	/* Llib/hash.scm 1242 */
																												if (BGL_HVECTORP
																													(BgL_keyz00_2463))
																													{	/* Llib/hash.scm 1244 */
																														BgL_keyz00_2421 =
																															BgL_keyz00_2463;
																														{	/* Llib/hash.scm 1187 */
																															long
																																BgL_lenz00_2423;
																															BgL_lenz00_2423 =
																																BGL_HVECTOR_LENGTH
																																(BgL_keyz00_2421);
																															{	/* Llib/hash.scm 1188 */
																																obj_t
																																	BgL_tagz00_2424;
																																BgL_tagz00_2424
																																	=
																																	BGl_homogeneouszd2vectorzd2infoz00zz__srfi4z00
																																	(BgL_keyz00_2421);
																																{	/* Llib/hash.scm 1189 */
																																	obj_t
																																		BgL__z00_2425;
																																	obj_t
																																		BgL_getz00_2426;
																																	obj_t
																																		BgL__z00_2427;
																																	obj_t
																																		BgL__z00_2428;
																																	{	/* Llib/hash.scm 1190 */
																																		obj_t
																																			BgL_tmpz00_4915;
																																		{	/* Llib/hash.scm 1190 */
																																			int
																																				BgL_tmpz00_8901;
																																			BgL_tmpz00_8901
																																				=
																																				(int)
																																				(1L);
																																			BgL_tmpz00_4915
																																				=
																																				BGL_MVALUES_VAL
																																				(BgL_tmpz00_8901);
																																		}
																																		{	/* Llib/hash.scm 1190 */
																																			int
																																				BgL_tmpz00_8904;
																																			BgL_tmpz00_8904
																																				=
																																				(int)
																																				(1L);
																																			BGL_MVALUES_VAL_SET
																																				(BgL_tmpz00_8904,
																																				BUNSPEC);
																																		}
																																		BgL__z00_2425
																																			=
																																			BgL_tmpz00_4915;
																																	}
																																	{	/* Llib/hash.scm 1190 */
																																		obj_t
																																			BgL_tmpz00_4916;
																																		{	/* Llib/hash.scm 1190 */
																																			int
																																				BgL_tmpz00_8907;
																																			BgL_tmpz00_8907
																																				=
																																				(int)
																																				(2L);
																																			BgL_tmpz00_4916
																																				=
																																				BGL_MVALUES_VAL
																																				(BgL_tmpz00_8907);
																																		}
																																		{	/* Llib/hash.scm 1190 */
																																			int
																																				BgL_tmpz00_8910;
																																			BgL_tmpz00_8910
																																				=
																																				(int)
																																				(2L);
																																			BGL_MVALUES_VAL_SET
																																				(BgL_tmpz00_8910,
																																				BUNSPEC);
																																		}
																																		BgL_getz00_2426
																																			=
																																			BgL_tmpz00_4916;
																																	}
																																	{	/* Llib/hash.scm 1190 */
																																		obj_t
																																			BgL_tmpz00_4917;
																																		{	/* Llib/hash.scm 1190 */
																																			int
																																				BgL_tmpz00_8913;
																																			BgL_tmpz00_8913
																																				=
																																				(int)
																																				(3L);
																																			BgL_tmpz00_4917
																																				=
																																				BGL_MVALUES_VAL
																																				(BgL_tmpz00_8913);
																																		}
																																		{	/* Llib/hash.scm 1190 */
																																			int
																																				BgL_tmpz00_8916;
																																			BgL_tmpz00_8916
																																				=
																																				(int)
																																				(3L);
																																			BGL_MVALUES_VAL_SET
																																				(BgL_tmpz00_8916,
																																				BUNSPEC);
																																		}
																																		BgL__z00_2427
																																			=
																																			BgL_tmpz00_4917;
																																	}
																																	{	/* Llib/hash.scm 1190 */
																																		obj_t
																																			BgL_tmpz00_4918;
																																		{	/* Llib/hash.scm 1190 */
																																			int
																																				BgL_tmpz00_8919;
																																			BgL_tmpz00_8919
																																				=
																																				(int)
																																				(4L);
																																			BgL_tmpz00_4918
																																				=
																																				BGL_MVALUES_VAL
																																				(BgL_tmpz00_8919);
																																		}
																																		{	/* Llib/hash.scm 1190 */
																																			int
																																				BgL_tmpz00_8922;
																																			BgL_tmpz00_8922
																																				=
																																				(int)
																																				(4L);
																																			BGL_MVALUES_VAL_SET
																																				(BgL_tmpz00_8922,
																																				BUNSPEC);
																																		}
																																		BgL__z00_2428
																																			=
																																			BgL_tmpz00_4918;
																																	}
																																	{	/* Llib/hash.scm 1190 */
																																		long
																																			BgL_g1079z00_2429;
																																		long
																																			BgL_g1080z00_2430;
																																		BgL_g1079z00_2429
																																			=
																																			(BgL_lenz00_2423
																																			- 1L);
																																		BgL_g1080z00_2430
																																			=
																																			(134217727L
																																			& (98723L
																																				^
																																				(134217727L
																																					&
																																					(BgL_lenz00_2423
																																						^
																																						BGl_objzd2hashze70z35zz__hashz00
																																						(BgL_tagz00_2424)))));
																																		{
																																			long
																																				BgL_iz00_4948;
																																			long
																																				BgL_accz00_4949;
																																			BgL_iz00_4948
																																				=
																																				BgL_g1079z00_2429;
																																			BgL_accz00_4949
																																				=
																																				BgL_g1080z00_2430;
																																		BgL_loopz00_4947:
																																			if (
																																				(BgL_iz00_4948
																																					==
																																					-1L))
																																				{	/* Llib/hash.scm 1193 */
																																					return
																																						BgL_accz00_4949;
																																				}
																																			else
																																				{	/* Llib/hash.scm 1195 */
																																					obj_t
																																						BgL_oz00_4957;
																																					BgL_oz00_4957
																																						=
																																						BGL_PROCEDURE_CALL2
																																						(BgL_getz00_2426,
																																						BgL_keyz00_2421,
																																						BINT
																																						(BgL_iz00_4948));
																																					{
																																						long
																																							BgL_accz00_8941;
																																						long
																																							BgL_iz00_8939;
																																						BgL_iz00_8939
																																							=
																																							(BgL_iz00_4948
																																							-
																																							1L);
																																						BgL_accz00_8941
																																							=
																																							(134217727L
																																							&
																																							(BgL_accz00_4949
																																								^
																																								BGl_objzd2hashze70z35zz__hashz00
																																								(BgL_oz00_4957)));
																																						BgL_accz00_4949
																																							=
																																							BgL_accz00_8941;
																																						BgL_iz00_4948
																																							=
																																							BgL_iz00_8939;
																																						goto
																																							BgL_loopz00_4947;
																																					}
																																				}
																																		}
																																	}
																																}
																															}
																														}
																													}
																												else
																													{	/* Llib/hash.scm 1247 */
																														obj_t
																															BgL_arg1966z00_2495;
																														{	/* Llib/hash.scm 1247 */

																															BgL_arg1966z00_2495
																																=
																																obj_to_string
																																(BgL_keyz00_2463,
																																BFALSE);
																														}
																														{
																															obj_t
																																BgL_keyz00_8946;
																															BgL_keyz00_8946 =
																																BgL_arg1966z00_2495;
																															BgL_keyz00_2463 =
																																BgL_keyz00_8946;
																															goto
																																BGl_objzd2hashze70z35zz__hashz00;
																														}
																													}
																											}
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
									}
							}
					}
			}
		}

	}



/* &get-hashnumber-persistent */
	obj_t BGl_z62getzd2hashnumberzd2persistentz62zz__hashz00(obj_t
		BgL_envz00_5488, obj_t BgL_keyz00_5489)
	{
		{	/* Llib/hash.scm 1181 */
			return
				BINT(BGl_getzd2hashnumberzd2persistentz00zz__hashz00(BgL_keyz00_5489));
		}

	}



/* get-pointer-hashnumber */
	BGL_EXPORTED_DEF long BGl_getzd2pointerzd2hashnumberz00zz__hashz00(obj_t
		BgL_ptrz00_162, long BgL_powerz00_163)
	{
		{	/* Llib/hash.scm 1254 */
			BGL_TAIL return bgl_pointer_hashnumber(BgL_ptrz00_162, BgL_powerz00_163);
		}

	}



/* &get-pointer-hashnumber */
	obj_t BGl_z62getzd2pointerzd2hashnumberz62zz__hashz00(obj_t BgL_envz00_5490,
		obj_t BgL_ptrz00_5491, obj_t BgL_powerz00_5492)
	{
		{	/* Llib/hash.scm 1254 */
			{	/* Llib/hash.scm 1255 */
				long BgL_tmpz00_8950;

				{	/* Llib/hash.scm 1255 */
					long BgL_auxz00_8951;

					{	/* Llib/hash.scm 1255 */
						obj_t BgL_tmpz00_8952;

						if (INTEGERP(BgL_powerz00_5492))
							{	/* Llib/hash.scm 1255 */
								BgL_tmpz00_8952 = BgL_powerz00_5492;
							}
						else
							{
								obj_t BgL_auxz00_8955;

								BgL_auxz00_8955 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
									BINT(49622L), BGl_string2609z00zz__hashz00,
									BGl_string2555z00zz__hashz00, BgL_powerz00_5492);
								FAILURE(BgL_auxz00_8955, BFALSE, BFALSE);
							}
						BgL_auxz00_8951 = (long) CINT(BgL_tmpz00_8952);
					}
					BgL_tmpz00_8950 =
						BGl_getzd2pointerzd2hashnumberz00zz__hashz00(BgL_ptrz00_5491,
						BgL_auxz00_8951);
				}
				return BINT(BgL_tmpz00_8950);
			}
		}

	}



/* _string-hash */
	obj_t BGl__stringzd2hashzd2zz__hashz00(obj_t BgL_env1174z00_168,
		obj_t BgL_opt1173z00_167)
	{
		{	/* Llib/hash.scm 1260 */
			{	/* Llib/hash.scm 1260 */
				obj_t BgL_g1175z00_2502;

				BgL_g1175z00_2502 = VECTOR_REF(BgL_opt1173z00_167, 0L);
				switch (VECTOR_LENGTH(BgL_opt1173z00_167))
					{
					case 1L:

						{	/* Llib/hash.scm 1260 */

							{	/* Llib/hash.scm 1260 */
								obj_t BgL_stringz00_5079;

								if (STRINGP(BgL_g1175z00_2502))
									{	/* Llib/hash.scm 1260 */
										BgL_stringz00_5079 = BgL_g1175z00_2502;
									}
								else
									{
										obj_t BgL_auxz00_8965;

										BgL_auxz00_8965 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2553z00zz__hashz00, BINT(49879L),
											BGl_string2610z00zz__hashz00,
											BGl_string2588z00zz__hashz00, BgL_g1175z00_2502);
										FAILURE(BgL_auxz00_8965, BFALSE, BFALSE);
									}
								{	/* Llib/hash.scm 1261 */
									obj_t BgL_arg1968z00_5080;

									BgL_arg1968z00_5080 = BINT(STRING_LENGTH(BgL_stringz00_5079));
									{	/* Llib/hash.scm 1261 */
										long BgL_tmpz00_8971;

										{	/* Llib/hash.scm 1261 */
											int BgL_tmpz00_8972;

											{	/* Llib/hash.scm 1261 */
												obj_t BgL_tmpz00_8975;

												if (INTEGERP(BgL_arg1968z00_5080))
													{	/* Llib/hash.scm 1261 */
														BgL_tmpz00_8975 = BgL_arg1968z00_5080;
													}
												else
													{
														obj_t BgL_auxz00_8978;

														BgL_auxz00_8978 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2553z00zz__hashz00, BINT(49993L),
															BGl_string2610z00zz__hashz00,
															BGl_string2555z00zz__hashz00,
															BgL_arg1968z00_5080);
														FAILURE(BgL_auxz00_8978, BFALSE, BFALSE);
													}
												BgL_tmpz00_8972 = CINT(BgL_tmpz00_8975);
											}
											BgL_tmpz00_8971 =
												bgl_string_hash(BSTRING_TO_STRING(BgL_stringz00_5079),
												(int) (0L), BgL_tmpz00_8972);
										}
										return BINT(BgL_tmpz00_8971);
									}
								}
							}
						}
						break;
					case 2L:

						{	/* Llib/hash.scm 1260 */
							obj_t BgL_startz00_2507;

							BgL_startz00_2507 = VECTOR_REF(BgL_opt1173z00_167, 1L);
							{	/* Llib/hash.scm 1260 */

								{	/* Llib/hash.scm 1260 */
									obj_t BgL_stringz00_5083;

									if (STRINGP(BgL_g1175z00_2502))
										{	/* Llib/hash.scm 1260 */
											BgL_stringz00_5083 = BgL_g1175z00_2502;
										}
									else
										{
											obj_t BgL_auxz00_8988;

											BgL_auxz00_8988 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2553z00zz__hashz00, BINT(49879L),
												BGl_string2610z00zz__hashz00,
												BGl_string2588z00zz__hashz00, BgL_g1175z00_2502);
											FAILURE(BgL_auxz00_8988, BFALSE, BFALSE);
										}
									{	/* Llib/hash.scm 1261 */
										obj_t BgL_arg1968z00_5084;

										BgL_arg1968z00_5084 =
											BINT(STRING_LENGTH(BgL_stringz00_5083));
										{	/* Llib/hash.scm 1261 */
											long BgL_tmpz00_8994;

											{	/* Llib/hash.scm 1261 */
												int BgL_auxz00_9005;
												int BgL_tmpz00_8995;

												{	/* Llib/hash.scm 1261 */
													obj_t BgL_tmpz00_9006;

													if (INTEGERP(BgL_arg1968z00_5084))
														{	/* Llib/hash.scm 1261 */
															BgL_tmpz00_9006 = BgL_arg1968z00_5084;
														}
													else
														{
															obj_t BgL_auxz00_9009;

															BgL_auxz00_9009 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2553z00zz__hashz00, BINT(49993L),
																BGl_string2610z00zz__hashz00,
																BGl_string2555z00zz__hashz00,
																BgL_arg1968z00_5084);
															FAILURE(BgL_auxz00_9009, BFALSE, BFALSE);
														}
													BgL_auxz00_9005 = CINT(BgL_tmpz00_9006);
												}
												{	/* Llib/hash.scm 1261 */
													obj_t BgL_tmpz00_8997;

													if (INTEGERP(BgL_startz00_2507))
														{	/* Llib/hash.scm 1261 */
															BgL_tmpz00_8997 = BgL_startz00_2507;
														}
													else
														{
															obj_t BgL_auxz00_9000;

															BgL_auxz00_9000 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2553z00zz__hashz00, BINT(49957L),
																BGl_string2610z00zz__hashz00,
																BGl_string2555z00zz__hashz00,
																BgL_startz00_2507);
															FAILURE(BgL_auxz00_9000, BFALSE, BFALSE);
														}
													BgL_tmpz00_8995 = CINT(BgL_tmpz00_8997);
												}
												BgL_tmpz00_8994 =
													bgl_string_hash(BSTRING_TO_STRING(BgL_stringz00_5083),
													BgL_tmpz00_8995, BgL_auxz00_9005);
											}
											return BINT(BgL_tmpz00_8994);
										}
									}
								}
							}
						}
						break;
					case 3L:

						{	/* Llib/hash.scm 1260 */
							obj_t BgL_startz00_2509;

							BgL_startz00_2509 = VECTOR_REF(BgL_opt1173z00_167, 1L);
							{	/* Llib/hash.scm 1260 */
								obj_t BgL_lenz00_2510;

								BgL_lenz00_2510 = VECTOR_REF(BgL_opt1173z00_167, 2L);
								{	/* Llib/hash.scm 1260 */

									{	/* Llib/hash.scm 1260 */
										obj_t BgL_stringz00_5087;

										if (STRINGP(BgL_g1175z00_2502))
											{	/* Llib/hash.scm 1260 */
												BgL_stringz00_5087 = BgL_g1175z00_2502;
											}
										else
											{
												obj_t BgL_auxz00_9020;

												BgL_auxz00_9020 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2553z00zz__hashz00, BINT(49879L),
													BGl_string2610z00zz__hashz00,
													BGl_string2588z00zz__hashz00, BgL_g1175z00_2502);
												FAILURE(BgL_auxz00_9020, BFALSE, BFALSE);
											}
										{	/* Llib/hash.scm 1261 */
											obj_t BgL_arg1968z00_5088;

											if (CBOOL(BgL_lenz00_2510))
												{	/* Llib/hash.scm 1261 */
													BgL_arg1968z00_5088 = BgL_lenz00_2510;
												}
											else
												{	/* Llib/hash.scm 1261 */
													BgL_arg1968z00_5088 =
														BINT(STRING_LENGTH(BgL_stringz00_5087));
												}
											{	/* Llib/hash.scm 1261 */
												long BgL_tmpz00_9028;

												{	/* Llib/hash.scm 1261 */
													int BgL_auxz00_9039;
													int BgL_tmpz00_9029;

													{	/* Llib/hash.scm 1261 */
														obj_t BgL_tmpz00_9040;

														if (INTEGERP(BgL_arg1968z00_5088))
															{	/* Llib/hash.scm 1261 */
																BgL_tmpz00_9040 = BgL_arg1968z00_5088;
															}
														else
															{
																obj_t BgL_auxz00_9043;

																BgL_auxz00_9043 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string2553z00zz__hashz00, BINT(49993L),
																	BGl_string2610z00zz__hashz00,
																	BGl_string2555z00zz__hashz00,
																	BgL_arg1968z00_5088);
																FAILURE(BgL_auxz00_9043, BFALSE, BFALSE);
															}
														BgL_auxz00_9039 = CINT(BgL_tmpz00_9040);
													}
													{	/* Llib/hash.scm 1261 */
														obj_t BgL_tmpz00_9031;

														if (INTEGERP(BgL_startz00_2509))
															{	/* Llib/hash.scm 1261 */
																BgL_tmpz00_9031 = BgL_startz00_2509;
															}
														else
															{
																obj_t BgL_auxz00_9034;

																BgL_auxz00_9034 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string2553z00zz__hashz00, BINT(49957L),
																	BGl_string2610z00zz__hashz00,
																	BGl_string2555z00zz__hashz00,
																	BgL_startz00_2509);
																FAILURE(BgL_auxz00_9034, BFALSE, BFALSE);
															}
														BgL_tmpz00_9029 = CINT(BgL_tmpz00_9031);
													}
													BgL_tmpz00_9028 =
														bgl_string_hash(BSTRING_TO_STRING
														(BgL_stringz00_5087), BgL_tmpz00_9029,
														BgL_auxz00_9039);
												}
												return BINT(BgL_tmpz00_9028);
											}
										}
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* string-hash */
	BGL_EXPORTED_DEF long BGl_stringzd2hashzd2zz__hashz00(obj_t BgL_stringz00_164,
		obj_t BgL_startz00_165, obj_t BgL_lenz00_166)
	{
		{	/* Llib/hash.scm 1260 */
			{	/* Llib/hash.scm 1261 */
				obj_t BgL_arg1968z00_5091;

				if (CBOOL(BgL_lenz00_166))
					{	/* Llib/hash.scm 1261 */
						BgL_arg1968z00_5091 = BgL_lenz00_166;
					}
				else
					{	/* Llib/hash.scm 1261 */
						BgL_arg1968z00_5091 = BINT(STRING_LENGTH(BgL_stringz00_164));
					}
				return
					bgl_string_hash(BSTRING_TO_STRING(BgL_stringz00_164),
					CINT(BgL_startz00_165), CINT(BgL_arg1968z00_5091));
			}
		}

	}



/* string-hash-number */
	BGL_EXPORTED_DEF long BGl_stringzd2hashzd2numberz00zz__hashz00(obj_t
		BgL_stringz00_169)
	{
		{	/* Llib/hash.scm 1266 */
			{	/* Llib/hash.scm 1267 */
				long BgL_arg1969z00_5651;

				BgL_arg1969z00_5651 = STRING_LENGTH(BgL_stringz00_169);
				return
					bgl_string_hash(BSTRING_TO_STRING(BgL_stringz00_169),
					(int) (0L), (int) (BgL_arg1969z00_5651));
		}}

	}



/* &string-hash-number */
	obj_t BGl_z62stringzd2hashzd2numberz62zz__hashz00(obj_t BgL_envz00_5493,
		obj_t BgL_stringz00_5494)
	{
		{	/* Llib/hash.scm 1266 */
			{	/* Llib/hash.scm 1267 */
				long BgL_tmpz00_9065;

				{	/* Llib/hash.scm 1267 */
					obj_t BgL_auxz00_9066;

					if (STRINGP(BgL_stringz00_5494))
						{	/* Llib/hash.scm 1267 */
							BgL_auxz00_9066 = BgL_stringz00_5494;
						}
					else
						{
							obj_t BgL_auxz00_9069;

							BgL_auxz00_9069 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2553z00zz__hashz00,
								BINT(50289L), BGl_string2611z00zz__hashz00,
								BGl_string2588z00zz__hashz00, BgL_stringz00_5494);
							FAILURE(BgL_auxz00_9069, BFALSE, BFALSE);
						}
					BgL_tmpz00_9065 =
						BGl_stringzd2hashzd2numberz00zz__hashz00(BgL_auxz00_9066);
				}
				return BINT(BgL_tmpz00_9065);
			}
		}

	}



/* open-string-hashtable-rehash! */
	bool_t BGl_openzd2stringzd2hashtablezd2rehashz12zc0zz__hashz00(obj_t
		BgL_tz00_173)
	{
		{	/* Llib/hash.scm 1278 */
			{	/* Llib/hash.scm 1279 */
				obj_t BgL_osiza7eza7_2514;

				BgL_osiza7eza7_2514 = STRUCT_REF(BgL_tz00_173, (int) (1L));
				{	/* Llib/hash.scm 1279 */
					long BgL_osiza7e3za7_2515;

					BgL_osiza7e3za7_2515 = (3L * (long) CINT(BgL_osiza7eza7_2514));
					{	/* Llib/hash.scm 1280 */
						obj_t BgL_obucketsz00_2516;

						BgL_obucketsz00_2516 = STRUCT_REF(BgL_tz00_173, (int) (2L));
						{	/* Llib/hash.scm 1281 */
							long BgL_nsiza7eza7_2517;

							BgL_nsiza7eza7_2517 =
								(1L + ((long) CINT(BgL_osiza7eza7_2514) * 2L));
							{	/* Llib/hash.scm 1282 */
								obj_t BgL_nbucketsz00_2518;

								BgL_nbucketsz00_2518 =
									make_vector((BgL_nsiza7eza7_2517 * 3L), BFALSE);
								{	/* Llib/hash.scm 1283 */

									{	/* Llib/hash.scm 1284 */
										obj_t BgL_auxz00_9088;
										int BgL_tmpz00_9086;

										BgL_auxz00_9088 = BINT(BgL_nsiza7eza7_2517);
										BgL_tmpz00_9086 = (int) (1L);
										STRUCT_SET(BgL_tz00_173, BgL_tmpz00_9086, BgL_auxz00_9088);
									}
									{	/* Llib/hash.scm 1285 */
										int BgL_tmpz00_9091;

										BgL_tmpz00_9091 = (int) (2L);
										STRUCT_SET(BgL_tz00_173, BgL_tmpz00_9091,
											BgL_nbucketsz00_2518);
									}
									{	/* Llib/hash.scm 1273 */
										obj_t BgL_auxz00_9096;
										int BgL_tmpz00_9094;

										BgL_auxz00_9096 = BINT(0L);
										BgL_tmpz00_9094 = (int) (6L);
										STRUCT_SET(BgL_tz00_173, BgL_tmpz00_9094, BgL_auxz00_9096);
									}
									{	/* Llib/hash.scm 1287 */
										obj_t BgL_auxz00_9101;
										int BgL_tmpz00_9099;

										BgL_auxz00_9101 = BINT(0L);
										BgL_tmpz00_9099 = (int) (0L);
										STRUCT_SET(BgL_tz00_173, BgL_tmpz00_9099, BgL_auxz00_9101);
									}
									{
										long BgL_iz00_2520;

										BgL_iz00_2520 = 0L;
									BgL_zc3z04anonymousza31970ze3z87_2521:
										if ((BgL_iz00_2520 == BgL_osiza7e3za7_2515))
											{	/* Llib/hash.scm 1289 */
												return ((bool_t) 0);
											}
										else
											{	/* Llib/hash.scm 1290 */
												obj_t BgL_cz00_2523;

												BgL_cz00_2523 =
													VECTOR_REF(
													((obj_t) BgL_obucketsz00_2516), BgL_iz00_2520);
												if (CBOOL(BgL_cz00_2523))
													{	/* Llib/hash.scm 1292 */
														obj_t BgL_hz00_2524;

														{	/* Llib/hash.scm 1292 */
															long BgL_arg1974z00_2527;

															BgL_arg1974z00_2527 = (BgL_iz00_2520 + 2L);
															BgL_hz00_2524 =
																VECTOR_REF(
																((obj_t) BgL_obucketsz00_2516),
																BgL_arg1974z00_2527);
														}
														if (CBOOL(BgL_hz00_2524))
															{	/* Llib/hash.scm 1295 */
																obj_t BgL_arg1972z00_2525;

																{	/* Llib/hash.scm 1295 */
																	long BgL_arg1973z00_2526;

																	BgL_arg1973z00_2526 = (BgL_iz00_2520 + 1L);
																	BgL_arg1972z00_2525 =
																		VECTOR_REF(
																		((obj_t) BgL_obucketsz00_2516),
																		BgL_arg1973z00_2526);
																}
																BGl_openzd2stringzd2hashtablezd2putzf2hashz12z32zz__hashz00
																	(BgL_tz00_173, BgL_cz00_2523,
																	BgL_arg1972z00_2525, BgL_hz00_2524);
															}
														else
															{	/* Llib/hash.scm 1293 */
																BFALSE;
															}
													}
												else
													{	/* Llib/hash.scm 1291 */
														BFALSE;
													}
												{
													long BgL_iz00_9119;

													BgL_iz00_9119 = (BgL_iz00_2520 + 3L);
													BgL_iz00_2520 = BgL_iz00_9119;
													goto BgL_zc3z04anonymousza31970ze3z87_2521;
												}
											}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* open-string-hashtable-size-inc! */
	obj_t BGl_openzd2stringzd2hashtablezd2siza7ezd2incz12zb5zz__hashz00(obj_t
		BgL_tz00_175)
	{
		{	/* Llib/hash.scm 1308 */
			{	/* Llib/hash.scm 1309 */
				obj_t BgL_nz00_2534;

				BgL_nz00_2534 = STRUCT_REF(BgL_tz00_175, (int) (0L));
				if (
					(((long) CINT(BgL_nz00_2534) * 3L) >
						(2L * (long) CINT(STRUCT_REF(BgL_tz00_175, (int) (1L))))))
					{	/* Llib/hash.scm 1310 */
						return
							BBOOL(BGl_openzd2stringzd2hashtablezd2rehashz12zc0zz__hashz00
							(BgL_tz00_175));
					}
				else
					{	/* Llib/hash.scm 1312 */
						long BgL_arg1984z00_2539;

						BgL_arg1984z00_2539 = ((long) CINT(BgL_nz00_2534) + 1L);
						{	/* Llib/hash.scm 1312 */
							obj_t BgL_auxz00_9137;
							int BgL_tmpz00_9135;

							BgL_auxz00_9137 = BINT(BgL_arg1984z00_2539);
							BgL_tmpz00_9135 = (int) (0L);
							return STRUCT_SET(BgL_tz00_175, BgL_tmpz00_9135, BgL_auxz00_9137);
						}
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__hashz00(void)
	{
		{	/* Llib/hash.scm 18 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__hashz00(void)
	{
		{	/* Llib/hash.scm 18 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__hashz00(void)
	{
		{	/* Llib/hash.scm 18 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__hashz00(void)
	{
		{	/* Llib/hash.scm 18 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string2612z00zz__hashz00));
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L,
				BSTRING_TO_STRING(BGl_string2612z00zz__hashz00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string2612z00zz__hashz00));
			return
				BGl_modulezd2initializa7ationz75zz__weakhashz00(56552828L,
				BSTRING_TO_STRING(BGl_string2612z00zz__hashz00));
		}

	}

#ifdef __cplusplus
}
#endif
