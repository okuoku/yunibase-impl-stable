/*===========================================================================*/
/*   (Llib/os.scm)                                                           */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Llib/os.scm -indent -o objs/obj_u/Llib/os.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___OS_TYPE_DEFINITIONS
#define BGL___OS_TYPE_DEFINITIONS
#endif													// BGL___OS_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62makezd2sharedzd2libzd2namezb0zz__osz00(obj_t, obj_t,
		obj_t);
	extern bool_t bgl_chmod(char *, bool_t, bool_t, bool_t);
	BGL_EXPORTED_DECL obj_t BGl_setgidz00zz__osz00(int);
	extern bool_t bgl_setrlimit(long, long, long);
	BGL_EXPORTED_DECL int BGl_getuidz00zz__osz00(void);
	static obj_t BGl_z62basenamez62zz__osz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_getpwnamz00zz__osz00(obj_t);
	static obj_t BGl_z62getpidz62zz__osz00(obj_t);
	static obj_t BGl_symbol2481z00zz__osz00 = BUNSPEC;
	static obj_t BGl_symbol2483z00zz__osz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_systemzd2ze3stringz31zz__osz00(obj_t);
	static obj_t BGl_z62oszd2classzb0zz__osz00(obj_t);
	static obj_t BGl_z62prefixz62zz__osz00(obj_t, obj_t);
	static obj_t BGl_z62defaultzd2scriptzd2namez62zz__osz00(obj_t);
	static obj_t BGl_limitzd2resourcezd2noz00zz__osz00(obj_t, obj_t);
	static obj_t BGl_z62ioctlz62zz__osz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_defaultzd2basenamezd2zz__osz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__osz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_makezd2sharedzd2libzd2namezd2zz__osz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31782ze3ze5zz__osz00(obj_t, obj_t);
	extern int bgl_getgid(void);
	static obj_t BGl_z62openlogz62zz__osz00(obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_filezd2separatorzd2zz__osz00(void);
	BGL_EXPORTED_DECL int BGl_umaskz00zz__osz00(obj_t);
	static obj_t BGl_z62chmodz62zz__osz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_dynamiczd2unloadzd2zz__osz00(obj_t);
	extern obj_t bgl_display_obj(obj_t, obj_t);
	extern bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t bgl_getgroups(void);
	extern obj_t bgl_dload(char *, char *, char *);
	BGL_EXPORTED_DECL int BGl_getgidz00zz__osz00(void);
	static obj_t BGl__dynamiczd2loadzd2symbolz00zz__osz00(obj_t, obj_t);
	extern obj_t BGl_warningz00zz__errorz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_relativezd2filezd2namez00zz__osz00(obj_t, obj_t);
	extern obj_t BGl_assocz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62makezd2filezd2namez62zz__osz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_systemz00zz__osz00(obj_t);
	static obj_t BGl_toplevelzd2initzd2zz__osz00(void);
	BGL_EXPORTED_DECL obj_t BGl_commandzd2linezd2zz__osz00(void);
	static obj_t BGl_z62dynamiczd2loadzd2symbolzd2setzb0zz__osz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl__umaskz00zz__osz00(obj_t, obj_t);
	BGL_EXPORTED_DECL int BGl_syslogzd2facilityzd2zz__osz00(obj_t);
	BGL_EXPORTED_DECL int BGl_syslogzd2levelzd2zz__osz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_getenvz00zz__osz00(obj_t);
	static obj_t BGl_z62syslogzd2facilityzb0zz__osz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_filezd2namezd2canonicaliza7eza7zz__osz00(obj_t);
	extern bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	extern obj_t bigloo_module_mangle(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zz__osz00(void);
	extern obj_t bgl_getpwnam(char *);
	extern obj_t bgl_dlsym_set(obj_t, obj_t);
	static long BGl_requestzd2ze3elongz31zz__osz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_chdirz00zz__osz00(char *);
	static obj_t BGl_mingwzd2basenamezd2zz__osz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_oszd2namezd2zz__osz00(void);
	static obj_t BGl_z62filezd2namezd2canonicaliza7ez12zd7zz__osz00(obj_t, obj_t);
	static obj_t BGl_z62setuidz62zz__osz00(obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zz__osz00(void);
	extern obj_t bgl_get_signal_handler(int);
	static obj_t BGl__getenvz00zz__osz00(obj_t, obj_t);
	static obj_t BGl_z62makezd2staticzd2libraryzd2namezb0zz__osz00(obj_t, obj_t);
	extern int bgl_dunload(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_staticzd2libraryzd2suffixz00zz__osz00(void);
	BGL_EXPORTED_DECL char *BGl_executablezd2namezd2zz__osz00(void);
	static obj_t BGl_z62oszd2archzb0zz__osz00(obj_t);
	static obj_t BGl_filezd2namezd2canonicaliza7ezd2innerz75zz__osz00(obj_t,
		obj_t, long);
	static obj_t BGl_z62executablezd2namezb0zz__osz00(obj_t);
	extern obj_t bgl_reverse_bang(obj_t);
	extern obj_t string_append_3(obj_t, obj_t, obj_t);
	extern char *bgl_dload_error(void);
	BGL_EXPORTED_DECL obj_t BGl_dirnamez00zz__osz00(obj_t);
	extern obj_t BGl_stringzd2indexzd2rightz00zz__r4_strings_6_7z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62syslogz62zz__osz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62syslogzd2optionzb0zz__osz00(obj_t, obj_t);
	static obj_t BGl__dynamiczd2loadzd2zz__osz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_defaultzd2scriptzd2namez00zz__osz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_filezd2namezd2unixzd2canonicaliza7ez75zz__osz00(obj_t);
	static obj_t BGl_z62datez62zz__osz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_unixzd2pathzd2ze3listze3zz__osz00(obj_t);
	static obj_t BGl_z62unixzd2pathzd2ze3listz81zz__osz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_makezd2filezd2namez00zz__osz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_basenamez00zz__osz00(obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_oszd2classzd2zz__osz00(void);
	static obj_t BGl_z62setgidz62zz__osz00(obj_t, obj_t);
	static obj_t BGl_z62getuidz62zz__osz00(obj_t);
	static obj_t BGl_z62chdirz62zz__osz00(obj_t, obj_t);
	static obj_t BGl_z62getrlimitz62zz__osz00(obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zz__osz00(obj_t, obj_t);
	static obj_t BGl_z62getzd2signalzd2handlerz62zz__osz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_filezd2namezd2unixzd2canonicaliza7ez12z67zz__osz00(obj_t);
	BGL_EXPORTED_DECL char *BGl_datez00zz__osz00(void);
	static obj_t BGl_z62closelogz62zz__osz00(obj_t);
	static obj_t BGl_methodzd2initzd2zz__osz00(void);
	static obj_t BGl_z62staticzd2libraryzd2suffixz62zz__osz00(obj_t);
	extern obj_t bgl_getenv_all(void);
	BGL_EXPORTED_DECL obj_t BGl_oszd2tmpzd2zz__osz00(void);
	static obj_t BGl_z62dynamiczd2loadzd2symbolzd2getzb0zz__osz00(obj_t, obj_t);
	static obj_t BGl_z62getpwuidz62zz__osz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_dynamiczd2loadzd2zz__osz00(obj_t, obj_t, obj_t);
	extern obj_t bgl_signal(int, obj_t);
	extern obj_t make_string(long, unsigned char);
	static obj_t BGl_z62defaultzd2executablezd2namez62zz__osz00(obj_t);
	extern obj_t bgl_close_input_port(obj_t);
	static obj_t BGl_z62ioctlzd2registerzd2requestz12z70zz__osz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31318ze3ze5zz__osz00(obj_t);
	static obj_t BGl_defaultzd2dirnamezd2zz__osz00(obj_t);
	static obj_t BGl_z62getgidz62zz__osz00(obj_t);
	extern obj_t bgl_dlsym_get(obj_t);
	extern obj_t string_append(obj_t, obj_t);
	static obj_t BGl_z62systemz62zz__osz00(obj_t, obj_t);
	BGL_EXPORTED_DECL int BGl_getppidz00zz__osz00(void);
	static obj_t BGl_list2278z00zz__osz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_getrlimitz00zz__osz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_dynamiczd2loadzd2symbolz00zz__osz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_signalz00zz__osz00(int, obj_t);
	extern obj_t
		BGl_callzd2withzd2outputzd2stringzd2zz__r4_ports_6_10_1z00(obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62filezd2namezd2canonicaliza7ezc5zz__osz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_sharedzd2libraryzd2suffixz00zz__osz00(void);
	BGL_EXPORTED_DEF obj_t BGl_za2defaultzd2javazd2packageza2z00zz__osz00 =
		BUNSPEC;
	extern obj_t BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62pwdz62zz__osz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_sleepz00zz__osz00(long);
	static obj_t BGl_z62filezd2namezd2unixzd2canonicaliza7ez17zz__osz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_makezd2staticzd2libraryzd2namezd2zz__osz00(obj_t);
	static obj_t BGl_z62makezd2filezd2pathz62zz__osz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62makezd2staticzd2libzd2namezb0zz__osz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_openlogz00zz__osz00(obj_t, int, int);
	static obj_t BGl_z62makezd2sharedzd2libraryzd2namezb0zz__osz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_setrlimitz12z12zz__osz00(obj_t, long, long);
	extern void bgl_sleep(long);
	BGL_EXPORTED_DECL obj_t BGl_filezd2namezd2ze3listze3zz__osz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_oszd2charsetzd2zz__osz00(void);
	static obj_t BGl_z62commandzd2linezb0zz__osz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__osz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_z62filezd2namezd2ze3listz81zz__osz00(obj_t, obj_t);
	extern obj_t BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_putenvz00zz__osz00(char *, char *);
	extern int bgl_setenv(char *, char *);
	BGL_EXPORTED_DECL obj_t BGl_makezd2staticzd2libzd2namezd2zz__osz00(obj_t,
		obj_t);
	static obj_t BGl_z62syslogzd2levelzb0zz__osz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_oszd2versionzd2zz__osz00(void);
	extern bool_t fexists(char *);
	extern obj_t BGl_stringzd2appendzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_z62sharedzd2libraryzd2suffixz62zz__osz00(obj_t);
	static obj_t BGl_z62setrlimitz12z70zz__osz00(obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL int BGl_syslogzd2optionzd2zz__osz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_dynamiczd2loadzd2symbolzd2setzd2zz__osz00(obj_t,
		obj_t);
	static obj_t BGl_z62pathzd2separatorzb0zz__osz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_suffixz00zz__osz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_closelogz00zz__osz00(void);
	static obj_t BGl_z62oszd2charsetzb0zz__osz00(obj_t);
	static obj_t BGl_z62sleepz62zz__osz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2dynamiczd2loadzd2pathza2z00zz__osz00 = BUNSPEC;
	extern long bgl_bignum_to_long(obj_t);
	static obj_t BGl_z62getgroupsz62zz__osz00(obj_t);
	static obj_t BGl_mingwzd2dirnamezd2zz__osz00(obj_t);
	static obj_t BGl_z62findzd2filezf2pathz42zz__osz00(obj_t, obj_t, obj_t);
	extern obj_t command_line;
	static obj_t BGl_cnstzd2initzd2zz__osz00(void);
	static obj_t BGl_z62oszd2versionzb0zz__osz00(obj_t);
	BGL_EXPORTED_DECL int BGl_getpidz00zz__osz00(void);
	static obj_t BGl_z62relativezd2filezd2namez62zz__osz00(obj_t, obj_t, obj_t);
	extern long bgl_list_length(obj_t);
	static obj_t BGl_symbol2309z00zz__osz00 = BUNSPEC;
	static obj_t BGl_z62oszd2tmpzb0zz__osz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_getpwuidz00zz__osz00(int);
	static obj_t BGl_z62getpwnamz62zz__osz00(obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__osz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_filezd2namezd2canonicaliza7ez12zb5zz__osz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_prefixz00zz__osz00(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zz__osz00(void);
	static obj_t BGl_z62oszd2namezb0zz__osz00(obj_t);
	BGL_EXPORTED_DECL int BGl_sigsetmaskz00zz__osz00(int);
	static obj_t BGl_symbol2311z00zz__osz00 = BUNSPEC;
	extern bool_t bgl_ioctl(obj_t, long, long);
	static obj_t BGl_symbol2313z00zz__osz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_makezd2filezd2pathz00zz__osz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_oszd2archzd2zz__osz00(void);
	extern obj_t BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_symbol2401z00zz__osz00 = BUNSPEC;
	static obj_t BGl_symbol2322z00zz__osz00 = BUNSPEC;
	static obj_t BGl_symbol2403z00zz__osz00 = BUNSPEC;
	static obj_t BGl_symbol2405z00zz__osz00 = BUNSPEC;
	static obj_t BGl_symbol2325z00zz__osz00 = BUNSPEC;
	static obj_t BGl_symbol2407z00zz__osz00 = BUNSPEC;
	static obj_t BGl_symbol2328z00zz__osz00 = BUNSPEC;
	static obj_t BGl_symbol2409z00zz__osz00 = BUNSPEC;
	static obj_t BGl_z62dirnamez62zz__osz00(obj_t, obj_t);
	extern obj_t BGl_makezd2listzd2zz__r4_pairs_and_lists_6_3z00(int, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_pwdz00zz__osz00(void);
	extern obj_t bgl_dlsym(obj_t, obj_t, obj_t);
	extern obj_t bgl_getrlimit(long);
	BGL_EXPORTED_DECL obj_t BGl_getzd2signalzd2handlerz00zz__osz00(int);
	static obj_t BGl_symbol2411z00zz__osz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_getgroupsz00zz__osz00(void);
	static obj_t BGl_symbol2413z00zz__osz00 = BUNSPEC;
	static obj_t BGl_z62sigsetmaskz62zz__osz00(obj_t, obj_t);
	static obj_t BGl_symbol2415z00zz__osz00 = BUNSPEC;
	static obj_t BGl_z62filezd2namezd2unixzd2canonicaliza7ez12z05zz__osz00(obj_t,
		obj_t);
	static obj_t BGl_z62signalz62zz__osz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2417z00zz__osz00 = BUNSPEC;
	static obj_t BGl_symbol2419z00zz__osz00 = BUNSPEC;
	static obj_t BGl_symbol2421z00zz__osz00 = BUNSPEC;
	extern obj_t BGl_readzd2stringzd2zz__r4_input_6_10_2z00(obj_t);
	static obj_t BGl_symbol2423z00zz__osz00 = BUNSPEC;
	static obj_t BGl_symbol2425z00zz__osz00 = BUNSPEC;
	extern obj_t c_substring(obj_t, long, long);
	static obj_t BGl_symbol2427z00zz__osz00 = BUNSPEC;
	static obj_t BGl_symbol2429z00zz__osz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_pathzd2separatorzd2zz__osz00(void);
	static obj_t BGl_z62systemzd2ze3stringz53zz__osz00(obj_t, obj_t);
	extern int bgl_setuid(int);
	static obj_t BGl_symbol2350z00zz__osz00 = BUNSPEC;
	static obj_t BGl_symbol2431z00zz__osz00 = BUNSPEC;
	extern obj_t BGl_bigloozd2typezd2errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2433z00zz__osz00 = BUNSPEC;
	static obj_t BGl_symbol2354z00zz__osz00 = BUNSPEC;
	static obj_t BGl_symbol2356z00zz__osz00 = BUNSPEC;
	static obj_t BGl_symbol2438z00zz__osz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_findzd2filezf2pathz20zz__osz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_makezd2sharedzd2libraryzd2namezd2zz__osz00(obj_t);
	extern obj_t blit_string(obj_t, long, obj_t, long, long);
	static obj_t BGl_ioctlzd2requestszd2tablez00zz__osz00 = BUNSPEC;
	static obj_t BGl_symbol2440z00zz__osz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_dynamiczd2loadzd2symbolzd2getzd2zz__osz00(obj_t);
	static obj_t BGl_symbol2442z00zz__osz00 = BUNSPEC;
	static obj_t BGl_z62filezd2separatorzb0zz__osz00(obj_t);
	static obj_t BGl_symbol2444z00zz__osz00 = BUNSPEC;
	static obj_t BGl_symbol2284z00zz__osz00 = BUNSPEC;
	static obj_t BGl_symbol2446z00zz__osz00 = BUNSPEC;
	static obj_t BGl_symbol2286z00zz__osz00 = BUNSPEC;
	static obj_t BGl_symbol2448z00zz__osz00 = BUNSPEC;
	extern obj_t bgl_getpwuid(int);
	extern char *c_date(void);
	BGL_EXPORTED_DECL obj_t BGl_setuidz00zz__osz00(int);
	extern char *executable_name;
	BGL_EXPORTED_DECL bool_t BGl_ioctlz00zz__osz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62dynamiczd2unloadzb0zz__osz00(obj_t, obj_t);
	static obj_t BGl_symbol2450z00zz__osz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_defaultzd2executablezd2namez00zz__osz00(void);
	static obj_t BGl_symbol2452z00zz__osz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_ioctlzd2registerzd2requestz12z12zz__osz00(obj_t,
		uint64_t);
	static obj_t BGl_symbol2457z00zz__osz00 = BUNSPEC;
	static obj_t BGl_z62putenvz62zz__osz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2459z00zz__osz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_syslogz00zz__osz00(int, obj_t);
	static obj_t BGl_symbol2461z00zz__osz00 = BUNSPEC;
	extern int bgl_setgid(int);
	static obj_t BGl_symbol2463z00zz__osz00 = BUNSPEC;
	extern int bgl_getuid(void);
	static obj_t BGl_symbol2383z00zz__osz00 = BUNSPEC;
	static obj_t BGl_symbol2465z00zz__osz00 = BUNSPEC;
	static obj_t BGl_symbol2385z00zz__osz00 = BUNSPEC;
	static obj_t BGl_symbol2467z00zz__osz00 = BUNSPEC;
	static obj_t BGl_symbol2387z00zz__osz00 = BUNSPEC;
	static obj_t BGl_symbol2469z00zz__osz00 = BUNSPEC;
	BGL_EXPORTED_DECL bool_t BGl_chmodz00zz__osz00(obj_t, obj_t);
	static obj_t BGl_symbol2389z00zz__osz00 = BUNSPEC;
	static obj_t BGl_z62getppidz62zz__osz00(obj_t);
	static obj_t BGl_z62suffixz62zz__osz00(obj_t, obj_t);
	static obj_t BGl_symbol2471z00zz__osz00 = BUNSPEC;
	static obj_t BGl_symbol2391z00zz__osz00 = BUNSPEC;
	static obj_t BGl_symbol2473z00zz__osz00 = BUNSPEC;
	static obj_t BGl_symbol2475z00zz__osz00 = BUNSPEC;
	static obj_t BGl_symbol2395z00zz__osz00 = BUNSPEC;
	static obj_t BGl_symbol2477z00zz__osz00 = BUNSPEC;
	static obj_t BGl_symbol2397z00zz__osz00 = BUNSPEC;
	static obj_t BGl_symbol2479z00zz__osz00 = BUNSPEC;
	static obj_t BGl_symbol2399z00zz__osz00 = BUNSPEC;
	extern long BGl_stringzd2ze3elongz31zz__r4_numbers_6_5_fixnumz00(obj_t, long);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_openlogzd2envzd2zz__osz00,
		BgL_bgl_za762openlogza762za7za7_2492z00, BGl_z62openlogz62zz__osz00, 0L,
		BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_syslogzd2envzd2zz__osz00,
		BgL_bgl_za762syslogza762za7za7__2493z00, va_generic_entry,
		BGl_z62syslogz62zz__osz00, BUNSPEC, -2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_getppidzd2envzd2zz__osz00,
		BgL_bgl_za762getppidza762za7za7_2494z00, BGl_z62getppidz62zz__osz00, 0L,
		BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_dynamiczd2loadzd2symbolzd2envzd2zz__osz00,
		BgL_bgl__dynamicza7d2loadza72495z00, opt_generic_entry,
		BGl__dynamiczd2loadzd2symbolz00zz__osz00, BFALSE, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sigsetmaskzd2envzd2zz__osz00,
		BgL_bgl_za762sigsetmaskza7622496z00, BGl_z62sigsetmaskz62zz__osz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_datezd2envzd2zz__osz00,
		BgL_bgl_za762dateza762za7za7__os2497z00, BGl_z62datez62zz__osz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2300z00zz__osz00,
		BgL_bgl_string2300za700za7za7_2498za7, "bstring", 7);
	      DEFINE_STRING(BGl_string2301z00zz__osz00,
		BgL_bgl_string2301za700za7za7_2499za7, "| ", 2);
	      DEFINE_STRING(BGl_string2302z00zz__osz00,
		BgL_bgl_string2302za700za7za7_2500za7, "&chdir", 6);
	      DEFINE_STRING(BGl_string2303z00zz__osz00,
		BgL_bgl_string2303za700za7za7_2501za7, "mingw", 5);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_findzd2filezf2pathzd2envzf2zz__osz00,
		BgL_bgl_za762findza7d2fileza7f2502za7,
		BGl_z62findzd2filezf2pathz42zz__osz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2304z00zz__osz00,
		BgL_bgl_string2304za700za7za7_2503za7, "&basename", 9);
	      DEFINE_STRING(BGl_string2305z00zz__osz00,
		BgL_bgl_string2305za700za7za7_2504za7, "&prefix", 7);
	      DEFINE_STRING(BGl_string2306z00zz__osz00,
		BgL_bgl_string2306za700za7za7_2505za7, "&dirname", 8);
	      DEFINE_STRING(BGl_string2307z00zz__osz00,
		BgL_bgl_string2307za700za7za7_2506za7, "", 0);
	      DEFINE_STRING(BGl_string2308z00zz__osz00,
		BgL_bgl_string2308za700za7za7_2507za7, "&suffix", 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2sharedzd2libraryzd2namezd2envz00zz__osz00,
		BgL_bgl_za762makeza7d2shared2508z00,
		BGl_z62makezd2sharedzd2libraryzd2namezb0zz__osz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_dynamiczd2loadzd2symbolzd2getzd2envz00zz__osz00,
		BgL_bgl_za762dynamicza7d2loa2509z00,
		BGl_z62dynamiczd2loadzd2symbolzd2getzb0zz__osz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2310z00zz__osz00,
		BgL_bgl_string2310za700za7za7_2510za7, "read", 4);
	      DEFINE_STRING(BGl_string2312z00zz__osz00,
		BgL_bgl_string2312za700za7za7_2511za7, "write", 5);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_pwdzd2envzd2zz__osz00,
		BgL_bgl_za762pwdza762za7za7__osza72512za7, BGl_z62pwdz62zz__osz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2314z00zz__osz00,
		BgL_bgl_string2314za700za7za7_2513za7, "execute", 7);
	      DEFINE_STRING(BGl_string2315z00zz__osz00,
		BgL_bgl_string2315za700za7za7_2514za7, "chmod", 5);
	      DEFINE_STRING(BGl_string2316z00zz__osz00,
		BgL_bgl_string2316za700za7za7_2515za7, "Unknown mode", 12);
	      DEFINE_STRING(BGl_string2317z00zz__osz00,
		BgL_bgl_string2317za700za7za7_2516za7, "&chmod", 6);
	      DEFINE_STRING(BGl_string2318z00zz__osz00,
		BgL_bgl_string2318za700za7za7_2517za7, "&make-file-name", 15);
	      DEFINE_STRING(BGl_string2319z00zz__osz00,
		BgL_bgl_string2319za700za7za7_2518za7, "make-file-path", 14);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_defaultzd2executablezd2namezd2envzd2zz__osz00,
		BgL_bgl_za762defaultza7d2exe2519z00,
		BGl_z62defaultzd2executablezd2namez62zz__osz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2400z00zz__osz00,
		BgL_bgl_string2400za700za7za7_2520za7, "LOG_CRON", 8);
	      DEFINE_STRING(BGl_string2320z00zz__osz00,
		BgL_bgl_string2320za700za7za7_2521za7, "string", 6);
	      DEFINE_STRING(BGl_string2321z00zz__osz00,
		BgL_bgl_string2321za700za7za7_2522za7, "&make-file-path", 15);
	      DEFINE_STRING(BGl_string2402z00zz__osz00,
		BgL_bgl_string2402za700za7za7_2523za7, "LOG_DAEMON", 10);
	      DEFINE_STRING(BGl_string2323z00zz__osz00,
		BgL_bgl_string2323za700za7za7_2524za7, "bigloo-c", 8);
	      DEFINE_STRING(BGl_string2404z00zz__osz00,
		BgL_bgl_string2404za700za7za7_2525za7, "LOG_FTP", 7);
	      DEFINE_STRING(BGl_string2324z00zz__osz00,
		BgL_bgl_string2324za700za7za7_2526za7, "lib", 3);
	      DEFINE_STRING(BGl_string2406z00zz__osz00,
		BgL_bgl_string2406za700za7za7_2527za7, "LOG_KERN", 8);
	      DEFINE_STRING(BGl_string2326z00zz__osz00,
		BgL_bgl_string2326za700za7za7_2528za7, "bigloo-jvm", 10);
	      DEFINE_STRING(BGl_string2327z00zz__osz00,
		BgL_bgl_string2327za700za7za7_2529za7, ".zip", 4);
	      DEFINE_STRING(BGl_string2408z00zz__osz00,
		BgL_bgl_string2408za700za7za7_2530za7, "LOG_LOCAL0", 10);
	      DEFINE_STRING(BGl_string2329z00zz__osz00,
		BgL_bgl_string2329za700za7za7_2531za7, "bigloo-.net", 11);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_umaskzd2envzd2zz__osz00,
		BgL_bgl__umaskza700za7za7__osza72532z00, opt_generic_entry,
		BGl__umaskz00zz__osz00, BFALSE, -1);
	      DEFINE_STRING(BGl_string2410z00zz__osz00,
		BgL_bgl_string2410za700za7za7_2533za7, "LOG_LOCAL1", 10);
	      DEFINE_STRING(BGl_string2330z00zz__osz00,
		BgL_bgl_string2330za700za7za7_2534za7, ".dll", 4);
	      DEFINE_STRING(BGl_string2331z00zz__osz00,
		BgL_bgl_string2331za700za7za7_2535za7, "make-static-lib-name", 20);
	      DEFINE_STRING(BGl_string2412z00zz__osz00,
		BgL_bgl_string2412za700za7za7_2536za7, "LOG_LOCAL2", 10);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_pathzd2separatorzd2envz00zz__osz00,
		BgL_bgl_za762pathza7d2separa2537z00, BGl_z62pathzd2separatorzb0zz__osz00,
		0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2332z00zz__osz00,
		BgL_bgl_string2332za700za7za7_2538za7, "Unknown backend", 15);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_filezd2namezd2canonicaliza7ez12zd2envz67zz__osz00,
		BgL_bgl_za762fileza7d2nameza7d2539za7,
		BGl_z62filezd2namezd2canonicaliza7ez12zd7zz__osz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2333z00zz__osz00,
		BgL_bgl_string2333za700za7za7_2540za7, "&make-static-lib-name", 21);
	      DEFINE_STRING(BGl_string2414z00zz__osz00,
		BgL_bgl_string2414za700za7za7_2541za7, "LOG_LOCAL3", 10);
	      DEFINE_STRING(BGl_string2334z00zz__osz00,
		BgL_bgl_string2334za700za7za7_2542za7, "symbol", 6);
	      DEFINE_STRING(BGl_string2335z00zz__osz00,
		BgL_bgl_string2335za700za7za7_2543za7, "make-shared-lib-name", 20);
	      DEFINE_STRING(BGl_string2416z00zz__osz00,
		BgL_bgl_string2416za700za7za7_2544za7, "LOG_LOCAL4", 10);
	      DEFINE_STRING(BGl_string2336z00zz__osz00,
		BgL_bgl_string2336za700za7za7_2545za7, "&make-shared-lib-name", 21);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_oszd2namezd2envz00zz__osz00,
		BgL_bgl_za762osza7d2nameza7b0za72546z00, BGl_z62oszd2namezb0zz__osz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2337z00zz__osz00,
		BgL_bgl_string2337za700za7za7_2547za7, "&find-file/path", 15);
	      DEFINE_STRING(BGl_string2418z00zz__osz00,
		BgL_bgl_string2418za700za7za7_2548za7, "LOG_LOCAL5", 10);
	      DEFINE_STRING(BGl_string2338z00zz__osz00,
		BgL_bgl_string2338za700za7za7_2549za7, "&file-name->list", 16);
	      DEFINE_STRING(BGl_string2339z00zz__osz00,
		BgL_bgl_string2339za700za7za7_2550za7, "&file-name-canonicalize", 23);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_filezd2namezd2ze3listzd2envz31zz__osz00,
		BgL_bgl_za762fileza7d2nameza7d2551za7,
		BGl_z62filezd2namezd2ze3listz81zz__osz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_signalzd2envzd2zz__osz00,
		BgL_bgl_za762signalza762za7za7__2552z00, BGl_z62signalz62zz__osz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2staticzd2libzd2namezd2envz00zz__osz00,
		BgL_bgl_za762makeza7d2static2553z00,
		BGl_z62makezd2staticzd2libzd2namezb0zz__osz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2420z00zz__osz00,
		BgL_bgl_string2420za700za7za7_2554za7, "LOG_LOCAL6", 10);
	      DEFINE_STRING(BGl_string2340z00zz__osz00,
		BgL_bgl_string2340za700za7za7_2555za7, "&file-name-canonicalize!", 24);
	      DEFINE_STRING(BGl_string2341z00zz__osz00,
		BgL_bgl_string2341za700za7za7_2556za7, "..", 2);
	      DEFINE_STRING(BGl_string2422z00zz__osz00,
		BgL_bgl_string2422za700za7za7_2557za7, "LOG_LOCAL7", 10);
	      DEFINE_STRING(BGl_string2342z00zz__osz00,
		BgL_bgl_string2342za700za7za7_2558za7, "&file-name-unix-canonicalize", 28);
	      DEFINE_STRING(BGl_string2343z00zz__osz00,
		BgL_bgl_string2343za700za7za7_2559za7, "&file-name-unix-canonicalize!", 29);
	      DEFINE_STRING(BGl_string2424z00zz__osz00,
		BgL_bgl_string2424za700za7za7_2560za7, "LOG_LPR", 7);
	      DEFINE_STRING(BGl_string2344z00zz__osz00,
		BgL_bgl_string2344za700za7za7_2561za7, "&relative-file-name", 19);
	      DEFINE_STRING(BGl_string2345z00zz__osz00,
		BgL_bgl_string2345za700za7za7_2562za7, "&make-static-library-name", 25);
	      DEFINE_STRING(BGl_string2426z00zz__osz00,
		BgL_bgl_string2426za700za7za7_2563za7, "LOG_MAIL", 8);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_executablezd2namezd2envz00zz__osz00,
		BgL_bgl_za762executableza7d22564z00, BGl_z62executablezd2namezb0zz__osz00,
		0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2346z00zz__osz00,
		BgL_bgl_string2346za700za7za7_2565za7, "&make-shared-library-name", 25);
	      DEFINE_STRING(BGl_string2347z00zz__osz00,
		BgL_bgl_string2347za700za7za7_2566za7, "&sleep", 6);
	      DEFINE_STRING(BGl_string2428z00zz__osz00,
		BgL_bgl_string2428za700za7za7_2567za7, "LOG_NEWS", 8);
	      DEFINE_STRING(BGl_string2348z00zz__osz00,
		BgL_bgl_string2348za700za7za7_2568za7, "_dynamic-load", 13);
	      DEFINE_STRING(BGl_string2349z00zz__osz00,
		BgL_bgl_string2349za700za7za7_2569za7, "module-initialization", 21);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_oszd2versionzd2envz00zz__osz00,
		BgL_bgl_za762osza7d2versionza72570za7, BGl_z62oszd2versionzb0zz__osz00, 0L,
		BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_dirnamezd2envzd2zz__osz00,
		BgL_bgl_za762dirnameza762za7za7_2571z00, BGl_z62dirnamez62zz__osz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_getuidzd2envzd2zz__osz00,
		BgL_bgl_za762getuidza762za7za7__2572z00, BGl_z62getuidz62zz__osz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2430z00zz__osz00,
		BgL_bgl_string2430za700za7za7_2573za7, "LOG_SYSLOG", 10);
	      DEFINE_STRING(BGl_string2351z00zz__osz00,
		BgL_bgl_string2351za700za7za7_2574za7, "__dload_noarch", 14);
	      DEFINE_STRING(BGl_string2432z00zz__osz00,
		BgL_bgl_string2432za700za7za7_2575za7, "LOG_USER", 8);
	      DEFINE_STRING(BGl_string2352z00zz__osz00,
		BgL_bgl_string2352za700za7za7_2576za7, "dynamic-load:", 13);
	      DEFINE_STRING(BGl_string2353z00zz__osz00,
		BgL_bgl_string2353za700za7za7_2577za7, "Not supported on this architecture",
		34);
	      DEFINE_STRING(BGl_string2434z00zz__osz00,
		BgL_bgl_string2434za700za7za7_2578za7, "LOG_UUCP", 8);
	      DEFINE_STRING(BGl_string2435z00zz__osz00,
		BgL_bgl_string2435za700za7za7_2579za7, "syslog-facility", 15);
	      DEFINE_STRING(BGl_string2355z00zz__osz00,
		BgL_bgl_string2355za700za7za7_2580za7, "__dload_error", 13);
	      DEFINE_STRING(BGl_string2436z00zz__osz00,
		BgL_bgl_string2436za700za7za7_2581za7, "unknown facility", 16);
	      DEFINE_STRING(BGl_string2437z00zz__osz00,
		BgL_bgl_string2437za700za7za7_2582za7, "&syslog-facility", 16);
	      DEFINE_STRING(BGl_string2357z00zz__osz00,
		BgL_bgl_string2357za700za7za7_2583za7, "__dload_noinit", 14);
	      DEFINE_STRING(BGl_string2358z00zz__osz00,
		BgL_bgl_string2358za700za7za7_2584za7, "dynamic-load: ", 14);
	      DEFINE_STRING(BGl_string2439z00zz__osz00,
		BgL_bgl_string2439za700za7za7_2585za7, "LOG_EMERG", 9);
	      DEFINE_STRING(BGl_string2359z00zz__osz00,
		BgL_bgl_string2359za700za7za7_2586za7,
		"Cannot find library init entry point -- ", 40);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_suffixzd2envzd2zz__osz00,
		BgL_bgl_za762suffixza762za7za7__2587z00, BGl_z62suffixz62zz__osz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2279z00zz__osz00,
		BgL_bgl_string2279za700za7za7_2588za7, ".", 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ioctlzd2envzd2zz__osz00,
		BgL_bgl_za762ioctlza762za7za7__o2589z00, BGl_z62ioctlz62zz__osz00, 0L,
		BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_getenvzd2envzd2zz__osz00,
		BgL_bgl__getenvza700za7za7__os2590za7, opt_generic_entry,
		BGl__getenvz00zz__osz00, BFALSE, -1);
	      DEFINE_STRING(BGl_string2360z00zz__osz00,
		BgL_bgl_string2360za700za7za7_2591za7,
		"Cannot find library init entry point", 36);
	      DEFINE_STRING(BGl_string2441z00zz__osz00,
		BgL_bgl_string2441za700za7za7_2592za7, "LOG_ALERT", 9);
	      DEFINE_STRING(BGl_string2280z00zz__osz00,
		BgL_bgl_string2280za700za7za7_2593za7, "bigloo.foreign", 14);
	      DEFINE_STRING(BGl_string2361z00zz__osz00,
		BgL_bgl_string2361za700za7za7_2594za7, "Can't find library", 18);
	      DEFINE_STRING(BGl_string2281z00zz__osz00,
		BgL_bgl_string2281za700za7za7_2595za7, "LANG", 4);
	      DEFINE_STRING(BGl_string2362z00zz__osz00,
		BgL_bgl_string2362za700za7za7_2596za7, "dynamic-unload", 14);
	      DEFINE_STRING(BGl_string2443z00zz__osz00,
		BgL_bgl_string2443za700za7za7_2597za7, "LOG_CRIT", 8);
	      DEFINE_STRING(BGl_string2282z00zz__osz00,
		BgL_bgl_string2282za700za7za7_2598za7, "LC_CTYPE", 8);
	      DEFINE_STRING(BGl_string2363z00zz__osz00,
		BgL_bgl_string2363za700za7za7_2599za7, "&dynamic-unload", 15);
	      DEFINE_STRING(BGl_string2283z00zz__osz00,
		BgL_bgl_string2283za700za7za7_2600za7, "LC_ALL", 6);
	      DEFINE_STRING(BGl_string2364z00zz__osz00,
		BgL_bgl_string2364za700za7za7_2601za7, "_dynamic-load-symbol", 20);
	      DEFINE_STRING(BGl_string2445z00zz__osz00,
		BgL_bgl_string2445za700za7za7_2602za7, "LOG_ERR", 7);
	      DEFINE_STRING(BGl_string2365z00zz__osz00,
		BgL_bgl_string2365za700za7za7_2603za7, "&dynamic-load-symbol-get", 24);
	      DEFINE_STRING(BGl_string2285z00zz__osz00,
		BgL_bgl_string2285za700za7za7_2604za7, "ignore", 6);
	      DEFINE_STRING(BGl_string2366z00zz__osz00,
		BgL_bgl_string2366za700za7za7_2605za7, "custom", 6);
	      DEFINE_STRING(BGl_string2447z00zz__osz00,
		BgL_bgl_string2447za700za7za7_2606za7, "LOG_WARNING", 11);
	      DEFINE_STRING(BGl_string2367z00zz__osz00,
		BgL_bgl_string2367za700za7za7_2607za7, "&dynamic-load-symbol-set", 24);
	      DEFINE_STRING(BGl_string2287z00zz__osz00,
		BgL_bgl_string2287za700za7za7_2608za7, "default", 7);
	      DEFINE_STRING(BGl_string2368z00zz__osz00,
		BgL_bgl_string2368za700za7za7_2609za7, "&unix-path->list", 16);
	      DEFINE_STRING(BGl_string2449z00zz__osz00,
		BgL_bgl_string2449za700za7za7_2610za7, "LOG_NOTICE", 10);
	      DEFINE_STRING(BGl_string2288z00zz__osz00,
		BgL_bgl_string2288za700za7za7_2611za7, "signal", 6);
	      DEFINE_STRING(BGl_string2369z00zz__osz00,
		BgL_bgl_string2369za700za7za7_2612za7, "&setuid", 7);
	      DEFINE_STRING(BGl_string2289z00zz__osz00,
		BgL_bgl_string2289za700za7za7_2613za7, "Illegal signal", 14);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_getpidzd2envzd2zz__osz00,
		BgL_bgl_za762getpidza762za7za7__2614z00, BGl_z62getpidz62zz__osz00, 0L,
		BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_setrlimitz12zd2envzc0zz__osz00,
		BgL_bgl_za762setrlimitza712za72615za7, BGl_z62setrlimitz12z70zz__osz00, 0L,
		BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_chmodzd2envzd2zz__osz00,
		BgL_bgl_za762chmodza762za7za7__o2616z00, va_generic_entry,
		BGl_z62chmodz62zz__osz00, BUNSPEC, -2);
	      DEFINE_STRING(BGl_string2370z00zz__osz00,
		BgL_bgl_string2370za700za7za7_2617za7, "&setgid", 7);
	      DEFINE_STRING(BGl_string2451z00zz__osz00,
		BgL_bgl_string2451za700za7za7_2618za7, "LOG_INFO", 8);
	      DEFINE_STRING(BGl_string2290z00zz__osz00,
		BgL_bgl_string2290za700za7za7_2619za7, "Wrong number of arguments", 25);
	      DEFINE_STRING(BGl_string2371z00zz__osz00,
		BgL_bgl_string2371za700za7za7_2620za7, "&getpwnam", 9);
	      DEFINE_STRING(BGl_string2291z00zz__osz00,
		BgL_bgl_string2291za700za7za7_2621za7, "/tmp/bigloo/runtime/Llib/os.scm",
		31);
	      DEFINE_STRING(BGl_string2372z00zz__osz00,
		BgL_bgl_string2372za700za7za7_2622za7, "&getpwuid", 9);
	      DEFINE_STRING(BGl_string2453z00zz__osz00,
		BgL_bgl_string2453za700za7za7_2623za7, "LOG_DEBUG", 9);
	      DEFINE_STRING(BGl_string2292z00zz__osz00,
		BgL_bgl_string2292za700za7za7_2624za7, "&signal", 7);
	      DEFINE_STRING(BGl_string2373z00zz__osz00,
		BgL_bgl_string2373za700za7za7_2625za7, "&ioctl-register-request!", 24);
	      DEFINE_STRING(BGl_string2454z00zz__osz00,
		BgL_bgl_string2454za700za7za7_2626za7, "syslog-level", 12);
	      DEFINE_STRING(BGl_string2293z00zz__osz00,
		BgL_bgl_string2293za700za7za7_2627za7, "bint", 4);
	      DEFINE_STRING(BGl_string2374z00zz__osz00,
		BgL_bgl_string2374za700za7za7_2628za7, "buint64", 7);
	      DEFINE_STRING(BGl_string2455z00zz__osz00,
		BgL_bgl_string2455za700za7za7_2629za7, "unknown level", 13);
	      DEFINE_STRING(BGl_string2456z00zz__osz00,
		BgL_bgl_string2456za700za7za7_2630za7, "&syslog-level", 13);
	      DEFINE_STRING(BGl_string2294z00zz__osz00,
		BgL_bgl_string2294za700za7za7_2631za7, "&get-signal-handler", 19);
	      DEFINE_STRING(BGl_string2375z00zz__osz00,
		BgL_bgl_string2375za700za7za7_2632za7, "ioctl", 5);
	      DEFINE_STRING(BGl_string2295z00zz__osz00,
		BgL_bgl_string2295za700za7za7_2633za7, "&sigsetmask", 11);
	      DEFINE_STRING(BGl_string2376z00zz__osz00,
		BgL_bgl_string2376za700za7za7_2634za7, "number of string", 16);
	      DEFINE_STRING(BGl_string2458z00zz__osz00,
		BgL_bgl_string2458za700za7za7_2635za7, "CORE", 4);
	      DEFINE_STRING(BGl_string2296z00zz__osz00,
		BgL_bgl_string2296za700za7za7_2636za7, "win32", 5);
	      DEFINE_STRING(BGl_string2377z00zz__osz00,
		BgL_bgl_string2377za700za7za7_2637za7, "elong pair", 10);
	      DEFINE_STRING(BGl_string2297z00zz__osz00,
		BgL_bgl_string2297za700za7za7_2638za7, "HOME", 4);
	      DEFINE_STRING(BGl_string2378z00zz__osz00,
		BgL_bgl_string2378za700za7za7_2639za7, "_umask", 6);
	      DEFINE_STRING(BGl_string2298z00zz__osz00,
		BgL_bgl_string2298za700za7za7_2640za7, "USERPROFILE", 11);
	      DEFINE_STRING(BGl_string2379z00zz__osz00,
		BgL_bgl_string2379za700za7za7_2641za7, "long", 4);
	      DEFINE_STRING(BGl_string2299z00zz__osz00,
		BgL_bgl_string2299za700za7za7_2642za7, "&putenv", 7);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2filezd2pathzd2envzd2zz__osz00,
		BgL_bgl_za762makeza7d2fileza7d2643za7, va_generic_entry,
		BGl_z62makezd2filezd2pathz62zz__osz00, BUNSPEC, -3);
	      DEFINE_STRING(BGl_string2460z00zz__osz00,
		BgL_bgl_string2460za700za7za7_2644za7, "CPU", 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_oszd2archzd2envz00zz__osz00,
		BgL_bgl_za762osza7d2archza7b0za72645z00, BGl_z62oszd2archzb0zz__osz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2380z00zz__osz00,
		BgL_bgl_string2380za700za7za7_2646za7, "&openlog", 8);
	      DEFINE_STRING(BGl_string2462z00zz__osz00,
		BgL_bgl_string2462za700za7za7_2647za7, "DATA", 4);
	      DEFINE_STRING(BGl_string2381z00zz__osz00,
		BgL_bgl_string2381za700za7za7_2648za7, "%s", 2);
	      DEFINE_STRING(BGl_string2382z00zz__osz00,
		BgL_bgl_string2382za700za7za7_2649za7, "&syslog", 7);
	      DEFINE_STRING(BGl_string2464z00zz__osz00,
		BgL_bgl_string2464za700za7za7_2650za7, "FSIZE", 5);
	      DEFINE_STRING(BGl_string2384z00zz__osz00,
		BgL_bgl_string2384za700za7za7_2651za7, "LOG_CONS", 8);
	      DEFINE_STRING(BGl_string2466z00zz__osz00,
		BgL_bgl_string2466za700za7za7_2652za7, "LOCKS", 5);
	      DEFINE_STRING(BGl_string2386z00zz__osz00,
		BgL_bgl_string2386za700za7za7_2653za7, "LOG_NDELAY", 10);
	      DEFINE_STRING(BGl_string2468z00zz__osz00,
		BgL_bgl_string2468za700za7za7_2654za7, "MEMLOCK", 7);
	      DEFINE_STRING(BGl_string2388z00zz__osz00,
		BgL_bgl_string2388za700za7za7_2655za7, "LOG_NOWAIT", 10);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_oszd2tmpzd2envz00zz__osz00,
		BgL_bgl_za762osza7d2tmpza7b0za7za72656za7, BGl_z62oszd2tmpzb0zz__osz00, 0L,
		BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_getpwnamzd2envzd2zz__osz00,
		BgL_bgl_za762getpwnamza762za7za72657z00, BGl_z62getpwnamz62zz__osz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2470z00zz__osz00,
		BgL_bgl_string2470za700za7za7_2658za7, "MSGQUEUE", 8);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_dynamiczd2loadzd2symbolzd2setzd2envz00zz__osz00,
		BgL_bgl_za762dynamicza7d2loa2659z00,
		BGl_z62dynamiczd2loadzd2symbolzd2setzb0zz__osz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2390z00zz__osz00,
		BgL_bgl_string2390za700za7za7_2660za7, "LOG_ODELAY", 10);
	      DEFINE_STRING(BGl_string2472z00zz__osz00,
		BgL_bgl_string2472za700za7za7_2661za7, "NICE", 4);
	      DEFINE_STRING(BGl_string2392z00zz__osz00,
		BgL_bgl_string2392za700za7za7_2662za7, "LOG_PID", 7);
	      DEFINE_STRING(BGl_string2474z00zz__osz00,
		BgL_bgl_string2474za700za7za7_2663za7, "NOFILE", 6);
	      DEFINE_STRING(BGl_string2393z00zz__osz00,
		BgL_bgl_string2393za700za7za7_2664za7, "syslog-option", 13);
	      DEFINE_STRING(BGl_string2394z00zz__osz00,
		BgL_bgl_string2394za700za7za7_2665za7, "unknown option", 14);
	      DEFINE_STRING(BGl_string2476z00zz__osz00,
		BgL_bgl_string2476za700za7za7_2666za7, "NPROC", 5);
	      DEFINE_STRING(BGl_string2396z00zz__osz00,
		BgL_bgl_string2396za700za7za7_2667za7, "LOG_AUTH", 8);
	      DEFINE_STRING(BGl_string2478z00zz__osz00,
		BgL_bgl_string2478za700za7za7_2668za7, "RSS", 3);
	      DEFINE_STRING(BGl_string2398z00zz__osz00,
		BgL_bgl_string2398za700za7za7_2669za7, "LOG_AUTHPRIV", 12);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_ioctlzd2registerzd2requestz12zd2envzc0zz__osz00,
		BgL_bgl_za762ioctlza7d2regis2670z00,
		BGl_z62ioctlzd2registerzd2requestz12z70zz__osz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2480z00zz__osz00,
		BgL_bgl_string2480za700za7za7_2671za7, "RTTIME", 6);
	      DEFINE_STRING(BGl_string2482z00zz__osz00,
		BgL_bgl_string2482za700za7za7_2672za7, "SIGPENDING", 10);
	      DEFINE_STRING(BGl_string2484z00zz__osz00,
		BgL_bgl_string2484za700za7za7_2673za7, "STACK", 5);
	      DEFINE_STRING(BGl_string2485z00zz__osz00,
		BgL_bgl_string2485za700za7za7_2674za7, "illegal limit resource", 22);
	      DEFINE_STRING(BGl_string2486z00zz__osz00,
		BgL_bgl_string2486za700za7za7_2675za7, "integer-or-symbol", 17);
	      DEFINE_STRING(BGl_string2487z00zz__osz00,
		BgL_bgl_string2487za700za7za7_2676za7, "getrlimit", 9);
	      DEFINE_STRING(BGl_string2488z00zz__osz00,
		BgL_bgl_string2488za700za7za7_2677za7, "setrlimit!", 10);
	      DEFINE_STRING(BGl_string2489z00zz__osz00,
		BgL_bgl_string2489za700za7za7_2678za7, "&setrlimit!", 11);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2filezd2namezd2envzd2zz__osz00,
		BgL_bgl_za762makeza7d2fileza7d2679za7,
		BGl_z62makezd2filezd2namez62zz__osz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_chdirzd2envzd2zz__osz00,
		BgL_bgl_za762chdirza762za7za7__o2680z00, BGl_z62chdirz62zz__osz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_prefixzd2envzd2zz__osz00,
		BgL_bgl_za762prefixza762za7za7__2681z00, BGl_z62prefixz62zz__osz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2490z00zz__osz00,
		BgL_bgl_string2490za700za7za7_2682za7, "belong", 6);
	      DEFINE_STRING(BGl_string2491z00zz__osz00,
		BgL_bgl_string2491za700za7za7_2683za7, "__os", 4);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_syslogzd2optionzd2envz00zz__osz00,
		BgL_bgl_za762syslogza7d2opti2684z00, va_generic_entry,
		BGl_z62syslogzd2optionzb0zz__osz00, BUNSPEC, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_getzd2signalzd2handlerzd2envzd2zz__osz00,
		BgL_bgl_za762getza7d2signalza72685za7,
		BGl_z62getzd2signalzd2handlerz62zz__osz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_syslogzd2facilityzd2envz00zz__osz00,
		BgL_bgl_za762syslogza7d2faci2686z00, BGl_z62syslogzd2facilityzb0zz__osz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_filezd2namezd2canonicaliza7ezd2envz75zz__osz00,
		BgL_bgl_za762fileza7d2nameza7d2687za7,
		BGl_z62filezd2namezd2canonicaliza7ezc5zz__osz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2sharedzd2libzd2namezd2envz00zz__osz00,
		BgL_bgl_za762makeza7d2shared2688z00,
		BGl_z62makezd2sharedzd2libzd2namezb0zz__osz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_getrlimitzd2envzd2zz__osz00,
		BgL_bgl_za762getrlimitza762za72689za7, BGl_z62getrlimitz62zz__osz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_getgidzd2envzd2zz__osz00,
		BgL_bgl_za762getgidza762za7za7__2690z00, BGl_z62getgidz62zz__osz00, 0L,
		BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_setuidzd2envzd2zz__osz00,
		BgL_bgl_za762setuidza762za7za7__2691z00, BGl_z62setuidz62zz__osz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_dynamiczd2loadzd2envz00zz__osz00,
		BgL_bgl__dynamicza7d2loadza72692z00, opt_generic_entry,
		BGl__dynamiczd2loadzd2zz__osz00, BFALSE, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_commandzd2linezd2envz00zz__osz00,
		BgL_bgl_za762commandza7d2lin2693z00, BGl_z62commandzd2linezb0zz__osz00, 0L,
		BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_closelogzd2envzd2zz__osz00,
		BgL_bgl_za762closelogza762za7za72694z00, BGl_z62closelogz62zz__osz00, 0L,
		BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sleepzd2envzd2zz__osz00,
		BgL_bgl_za762sleepza762za7za7__o2695z00, BGl_z62sleepz62zz__osz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_dynamiczd2unloadzd2envz00zz__osz00,
		BgL_bgl_za762dynamicza7d2unl2696z00, BGl_z62dynamiczd2unloadzb0zz__osz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_oszd2charsetzd2envz00zz__osz00,
		BgL_bgl_za762osza7d2charsetza72697za7, BGl_z62oszd2charsetzb0zz__osz00, 0L,
		BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_staticzd2libraryzd2suffixzd2envzd2zz__osz00,
		BgL_bgl_za762staticza7d2libr2698z00,
		BGl_z62staticzd2libraryzd2suffixz62zz__osz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_systemzd2envzd2zz__osz00,
		BgL_bgl_za762systemza762za7za7__2699z00, va_generic_entry,
		BGl_z62systemz62zz__osz00, BUNSPEC, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_filezd2namezd2unixzd2canonicaliza7ezd2envza7zz__osz00,
		BgL_bgl_za762fileza7d2nameza7d2700za7,
		BGl_z62filezd2namezd2unixzd2canonicaliza7ez17zz__osz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_getgroupszd2envzd2zz__osz00,
		BgL_bgl_za762getgroupsza762za72701za7, BGl_z62getgroupsz62zz__osz00, 0L,
		BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_basenamezd2envzd2zz__osz00,
		BgL_bgl_za762basenameza762za7za72702z00, BGl_z62basenamez62zz__osz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_filezd2separatorzd2envz00zz__osz00,
		BgL_bgl_za762fileza7d2separa2703z00, BGl_z62filezd2separatorzb0zz__osz00,
		0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_getpwuidzd2envzd2zz__osz00,
		BgL_bgl_za762getpwuidza762za7za72704z00, BGl_z62getpwuidz62zz__osz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_putenvzd2envzd2zz__osz00,
		BgL_bgl_za762putenvza762za7za7__2705z00, BGl_z62putenvz62zz__osz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_sharedzd2libraryzd2suffixzd2envzd2zz__osz00,
		BgL_bgl_za762sharedza7d2libr2706z00,
		BGl_z62sharedzd2libraryzd2suffixz62zz__osz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_systemzd2ze3stringzd2envze3zz__osz00,
		BgL_bgl_za762systemza7d2za7e3s2707za7, va_generic_entry,
		BGl_z62systemzd2ze3stringz53zz__osz00, BUNSPEC, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_defaultzd2scriptzd2namezd2envzd2zz__osz00,
		BgL_bgl_za762defaultza7d2scr2708z00,
		BGl_z62defaultzd2scriptzd2namez62zz__osz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2staticzd2libraryzd2namezd2envz00zz__osz00,
		BgL_bgl_za762makeza7d2static2709z00,
		BGl_z62makezd2staticzd2libraryzd2namezb0zz__osz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_unixzd2pathzd2ze3listzd2envz31zz__osz00,
		BgL_bgl_za762unixza7d2pathza7d2710za7,
		BGl_z62unixzd2pathzd2ze3listz81zz__osz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_syslogzd2levelzd2envz00zz__osz00,
		BgL_bgl_za762syslogza7d2leve2711z00, BGl_z62syslogzd2levelzb0zz__osz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_oszd2classzd2envz00zz__osz00,
		BgL_bgl_za762osza7d2classza7b02712za7, BGl_z62oszd2classzb0zz__osz00, 0L,
		BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_filezd2namezd2unixzd2canonicaliza7ez12zd2envzb5zz__osz00,
		BgL_bgl_za762fileza7d2nameza7d2713za7,
		BGl_z62filezd2namezd2unixzd2canonicaliza7ez12z05zz__osz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_relativezd2filezd2namezd2envzd2zz__osz00,
		BgL_bgl_za762relativeza7d2fi2714z00,
		BGl_z62relativezd2filezd2namez62zz__osz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_setgidzd2envzd2zz__osz00,
		BgL_bgl_za762setgidza762za7za7__2715z00, BGl_z62setgidz62zz__osz00, 0L,
		BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_symbol2481z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2483z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__osz00));
		     ADD_ROOT((void *) (&BGl_list2278z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_za2defaultzd2javazd2packageza2z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_za2dynamiczd2loadzd2pathza2z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2309z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2311z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2313z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2401z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2322z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2403z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2405z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2325z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2407z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2328z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2409z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2411z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2413z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2415z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2417z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2419z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2421z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2423z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2425z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2427z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2429z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2350z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2431z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2433z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2354z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2356z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2438z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_ioctlzd2requestszd2tablez00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2440z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2442z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2444z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2284z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2446z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2286z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2448z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2450z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2452z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2457z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2459z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2461z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2463z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2383z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2465z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2385z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2467z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2387z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2469z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2389z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2471z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2391z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2473z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2475z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2395z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2477z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2397z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2479z00zz__osz00));
		     ADD_ROOT((void *) (&BGl_symbol2399z00zz__osz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__osz00(long
		BgL_checksumz00_3843, char *BgL_fromz00_3844)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__osz00))
				{
					BGl_requirezd2initializa7ationz75zz__osz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__osz00();
					BGl_cnstzd2initzd2zz__osz00();
					BGl_importedzd2moduleszd2initz00zz__osz00();
					return BGl_toplevelzd2initzd2zz__osz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__osz00(void)
	{
		{	/* Llib/os.scm 18 */
			BGl_list2278z00zz__osz00 =
				MAKE_YOUNG_PAIR(BGl_string2279z00zz__osz00, BNIL);
			BGl_symbol2284z00zz__osz00 =
				bstring_to_symbol(BGl_string2285z00zz__osz00);
			BGl_symbol2286z00zz__osz00 =
				bstring_to_symbol(BGl_string2287z00zz__osz00);
			BGl_symbol2309z00zz__osz00 =
				bstring_to_symbol(BGl_string2310z00zz__osz00);
			BGl_symbol2311z00zz__osz00 =
				bstring_to_symbol(BGl_string2312z00zz__osz00);
			BGl_symbol2313z00zz__osz00 =
				bstring_to_symbol(BGl_string2314z00zz__osz00);
			BGl_symbol2322z00zz__osz00 =
				bstring_to_symbol(BGl_string2323z00zz__osz00);
			BGl_symbol2325z00zz__osz00 =
				bstring_to_symbol(BGl_string2326z00zz__osz00);
			BGl_symbol2328z00zz__osz00 =
				bstring_to_symbol(BGl_string2329z00zz__osz00);
			BGl_symbol2350z00zz__osz00 =
				bstring_to_symbol(BGl_string2351z00zz__osz00);
			BGl_symbol2354z00zz__osz00 =
				bstring_to_symbol(BGl_string2355z00zz__osz00);
			BGl_symbol2356z00zz__osz00 =
				bstring_to_symbol(BGl_string2357z00zz__osz00);
			BGl_symbol2383z00zz__osz00 =
				bstring_to_symbol(BGl_string2384z00zz__osz00);
			BGl_symbol2385z00zz__osz00 =
				bstring_to_symbol(BGl_string2386z00zz__osz00);
			BGl_symbol2387z00zz__osz00 =
				bstring_to_symbol(BGl_string2388z00zz__osz00);
			BGl_symbol2389z00zz__osz00 =
				bstring_to_symbol(BGl_string2390z00zz__osz00);
			BGl_symbol2391z00zz__osz00 =
				bstring_to_symbol(BGl_string2392z00zz__osz00);
			BGl_symbol2395z00zz__osz00 =
				bstring_to_symbol(BGl_string2396z00zz__osz00);
			BGl_symbol2397z00zz__osz00 =
				bstring_to_symbol(BGl_string2398z00zz__osz00);
			BGl_symbol2399z00zz__osz00 =
				bstring_to_symbol(BGl_string2400z00zz__osz00);
			BGl_symbol2401z00zz__osz00 =
				bstring_to_symbol(BGl_string2402z00zz__osz00);
			BGl_symbol2403z00zz__osz00 =
				bstring_to_symbol(BGl_string2404z00zz__osz00);
			BGl_symbol2405z00zz__osz00 =
				bstring_to_symbol(BGl_string2406z00zz__osz00);
			BGl_symbol2407z00zz__osz00 =
				bstring_to_symbol(BGl_string2408z00zz__osz00);
			BGl_symbol2409z00zz__osz00 =
				bstring_to_symbol(BGl_string2410z00zz__osz00);
			BGl_symbol2411z00zz__osz00 =
				bstring_to_symbol(BGl_string2412z00zz__osz00);
			BGl_symbol2413z00zz__osz00 =
				bstring_to_symbol(BGl_string2414z00zz__osz00);
			BGl_symbol2415z00zz__osz00 =
				bstring_to_symbol(BGl_string2416z00zz__osz00);
			BGl_symbol2417z00zz__osz00 =
				bstring_to_symbol(BGl_string2418z00zz__osz00);
			BGl_symbol2419z00zz__osz00 =
				bstring_to_symbol(BGl_string2420z00zz__osz00);
			BGl_symbol2421z00zz__osz00 =
				bstring_to_symbol(BGl_string2422z00zz__osz00);
			BGl_symbol2423z00zz__osz00 =
				bstring_to_symbol(BGl_string2424z00zz__osz00);
			BGl_symbol2425z00zz__osz00 =
				bstring_to_symbol(BGl_string2426z00zz__osz00);
			BGl_symbol2427z00zz__osz00 =
				bstring_to_symbol(BGl_string2428z00zz__osz00);
			BGl_symbol2429z00zz__osz00 =
				bstring_to_symbol(BGl_string2430z00zz__osz00);
			BGl_symbol2431z00zz__osz00 =
				bstring_to_symbol(BGl_string2432z00zz__osz00);
			BGl_symbol2433z00zz__osz00 =
				bstring_to_symbol(BGl_string2434z00zz__osz00);
			BGl_symbol2438z00zz__osz00 =
				bstring_to_symbol(BGl_string2439z00zz__osz00);
			BGl_symbol2440z00zz__osz00 =
				bstring_to_symbol(BGl_string2441z00zz__osz00);
			BGl_symbol2442z00zz__osz00 =
				bstring_to_symbol(BGl_string2443z00zz__osz00);
			BGl_symbol2444z00zz__osz00 =
				bstring_to_symbol(BGl_string2445z00zz__osz00);
			BGl_symbol2446z00zz__osz00 =
				bstring_to_symbol(BGl_string2447z00zz__osz00);
			BGl_symbol2448z00zz__osz00 =
				bstring_to_symbol(BGl_string2449z00zz__osz00);
			BGl_symbol2450z00zz__osz00 =
				bstring_to_symbol(BGl_string2451z00zz__osz00);
			BGl_symbol2452z00zz__osz00 =
				bstring_to_symbol(BGl_string2453z00zz__osz00);
			BGl_symbol2457z00zz__osz00 =
				bstring_to_symbol(BGl_string2458z00zz__osz00);
			BGl_symbol2459z00zz__osz00 =
				bstring_to_symbol(BGl_string2460z00zz__osz00);
			BGl_symbol2461z00zz__osz00 =
				bstring_to_symbol(BGl_string2462z00zz__osz00);
			BGl_symbol2463z00zz__osz00 =
				bstring_to_symbol(BGl_string2464z00zz__osz00);
			BGl_symbol2465z00zz__osz00 =
				bstring_to_symbol(BGl_string2466z00zz__osz00);
			BGl_symbol2467z00zz__osz00 =
				bstring_to_symbol(BGl_string2468z00zz__osz00);
			BGl_symbol2469z00zz__osz00 =
				bstring_to_symbol(BGl_string2470z00zz__osz00);
			BGl_symbol2471z00zz__osz00 =
				bstring_to_symbol(BGl_string2472z00zz__osz00);
			BGl_symbol2473z00zz__osz00 =
				bstring_to_symbol(BGl_string2474z00zz__osz00);
			BGl_symbol2475z00zz__osz00 =
				bstring_to_symbol(BGl_string2476z00zz__osz00);
			BGl_symbol2477z00zz__osz00 =
				bstring_to_symbol(BGl_string2478z00zz__osz00);
			BGl_symbol2479z00zz__osz00 =
				bstring_to_symbol(BGl_string2480z00zz__osz00);
			BGl_symbol2481z00zz__osz00 =
				bstring_to_symbol(BGl_string2482z00zz__osz00);
			return (BGl_symbol2483z00zz__osz00 =
				bstring_to_symbol(BGl_string2484z00zz__osz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__osz00(void)
	{
		{	/* Llib/os.scm 18 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__osz00(void)
	{
		{	/* Llib/os.scm 18 */
			BGl_za2dynamiczd2loadzd2pathza2z00zz__osz00 = BGl_list2278z00zz__osz00;
			BGl_za2defaultzd2javazd2packageza2z00zz__osz00 =
				BGl_string2280z00zz__osz00;
			return (BGl_ioctlzd2requestszd2tablez00zz__osz00 = BNIL, BUNSPEC);
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zz__osz00(obj_t BgL_l1z00_1, obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1270;

				BgL_headz00_1270 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_2469;
					obj_t BgL_tailz00_2470;

					BgL_prevz00_2469 = BgL_headz00_1270;
					BgL_tailz00_2470 = BgL_l1z00_1;
				BgL_loopz00_2468:
					if (PAIRP(BgL_tailz00_2470))
						{
							obj_t BgL_newzd2prevzd2_2476;

							BgL_newzd2prevzd2_2476 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_2470), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_2469, BgL_newzd2prevzd2_2476);
							{
								obj_t BgL_tailz00_3919;
								obj_t BgL_prevz00_3918;

								BgL_prevz00_3918 = BgL_newzd2prevzd2_2476;
								BgL_tailz00_3919 = CDR(BgL_tailz00_2470);
								BgL_tailz00_2470 = BgL_tailz00_3919;
								BgL_prevz00_2469 = BgL_prevz00_3918;
								goto BgL_loopz00_2468;
							}
						}
					else
						{
							BNIL;
						}
				}
				return CDR(BgL_headz00_1270);
			}
		}

	}



/* default-executable-name */
	BGL_EXPORTED_DEF obj_t BGl_defaultzd2executablezd2namez00zz__osz00(void)
	{
		{	/* Llib/os.scm 316 */
			return string_to_bstring(BGL_DEFAULT_A_OUT);
		}

	}



/* &default-executable-name */
	obj_t BGl_z62defaultzd2executablezd2namez62zz__osz00(obj_t BgL_envz00_3543)
	{
		{	/* Llib/os.scm 316 */
			return BGl_defaultzd2executablezd2namez00zz__osz00();
		}

	}



/* default-script-name */
	BGL_EXPORTED_DEF obj_t BGl_defaultzd2scriptzd2namez00zz__osz00(void)
	{
		{	/* Llib/os.scm 317 */
			return string_to_bstring(BGL_DEFAULT_A_BAT);
		}

	}



/* &default-script-name */
	obj_t BGl_z62defaultzd2scriptzd2namez62zz__osz00(obj_t BgL_envz00_3544)
	{
		{	/* Llib/os.scm 317 */
			return BGl_defaultzd2scriptzd2namez00zz__osz00();
		}

	}



/* os-class */
	BGL_EXPORTED_DEF obj_t BGl_oszd2classzd2zz__osz00(void)
	{
		{	/* Llib/os.scm 318 */
			return string_to_bstring(OS_CLASS);
		}

	}



/* &os-class */
	obj_t BGl_z62oszd2classzb0zz__osz00(obj_t BgL_envz00_3545)
	{
		{	/* Llib/os.scm 318 */
			return BGl_oszd2classzd2zz__osz00();
		}

	}



/* os-name */
	BGL_EXPORTED_DEF obj_t BGl_oszd2namezd2zz__osz00(void)
	{
		{	/* Llib/os.scm 319 */
			return string_to_bstring(OS_NAME);
		}

	}



/* &os-name */
	obj_t BGl_z62oszd2namezb0zz__osz00(obj_t BgL_envz00_3546)
	{
		{	/* Llib/os.scm 319 */
			return BGl_oszd2namezd2zz__osz00();
		}

	}



/* os-arch */
	BGL_EXPORTED_DEF obj_t BGl_oszd2archzd2zz__osz00(void)
	{
		{	/* Llib/os.scm 320 */
			return string_to_bstring(OS_ARCH);
		}

	}



/* &os-arch */
	obj_t BGl_z62oszd2archzb0zz__osz00(obj_t BgL_envz00_3547)
	{
		{	/* Llib/os.scm 320 */
			return BGl_oszd2archzd2zz__osz00();
		}

	}



/* os-version */
	BGL_EXPORTED_DEF obj_t BGl_oszd2versionzd2zz__osz00(void)
	{
		{	/* Llib/os.scm 321 */
			return string_to_bstring(OS_VERSION);
		}

	}



/* &os-version */
	obj_t BGl_z62oszd2versionzb0zz__osz00(obj_t BgL_envz00_3548)
	{
		{	/* Llib/os.scm 321 */
			return BGl_oszd2versionzd2zz__osz00();
		}

	}



/* os-tmp */
	BGL_EXPORTED_DEF obj_t BGl_oszd2tmpzd2zz__osz00(void)
	{
		{	/* Llib/os.scm 322 */
			return string_to_bstring(OS_TMP);
		}

	}



/* &os-tmp */
	obj_t BGl_z62oszd2tmpzb0zz__osz00(obj_t BgL_envz00_3549)
	{
		{	/* Llib/os.scm 322 */
			return BGl_oszd2tmpzd2zz__osz00();
		}

	}



/* file-separator */
	BGL_EXPORTED_DEF obj_t BGl_filezd2separatorzd2zz__osz00(void)
	{
		{	/* Llib/os.scm 323 */
			return BCHAR(FILE_SEPARATOR);
		}

	}



/* &file-separator */
	obj_t BGl_z62filezd2separatorzb0zz__osz00(obj_t BgL_envz00_3550)
	{
		{	/* Llib/os.scm 323 */
			return BGl_filezd2separatorzd2zz__osz00();
		}

	}



/* path-separator */
	BGL_EXPORTED_DEF obj_t BGl_pathzd2separatorzd2zz__osz00(void)
	{
		{	/* Llib/os.scm 324 */
			return BCHAR(PATH_SEPARATOR);
		}

	}



/* &path-separator */
	obj_t BGl_z62pathzd2separatorzb0zz__osz00(obj_t BgL_envz00_3551)
	{
		{	/* Llib/os.scm 324 */
			return BGl_pathzd2separatorzd2zz__osz00();
		}

	}



/* static-library-suffix */
	BGL_EXPORTED_DEF obj_t BGl_staticzd2libraryzd2suffixz00zz__osz00(void)
	{
		{	/* Llib/os.scm 325 */
			return string_to_bstring(STATIC_LIB_SUFFIX);
		}

	}



/* &static-library-suffix */
	obj_t BGl_z62staticzd2libraryzd2suffixz62zz__osz00(obj_t BgL_envz00_3552)
	{
		{	/* Llib/os.scm 325 */
			return BGl_staticzd2libraryzd2suffixz00zz__osz00();
		}

	}



/* shared-library-suffix */
	BGL_EXPORTED_DEF obj_t BGl_sharedzd2libraryzd2suffixz00zz__osz00(void)
	{
		{	/* Llib/os.scm 326 */
			return string_to_bstring(SHARED_LIB_SUFFIX);
		}

	}



/* &shared-library-suffix */
	obj_t BGl_z62sharedzd2libraryzd2suffixz62zz__osz00(obj_t BgL_envz00_3553)
	{
		{	/* Llib/os.scm 326 */
			return BGl_sharedzd2libraryzd2suffixz00zz__osz00();
		}

	}



/* os-charset */
	BGL_EXPORTED_DEF obj_t BGl_oszd2charsetzd2zz__osz00(void)
	{
		{	/* Llib/os.scm 331 */
			{	/* Llib/os.scm 333 */
				obj_t BgL_g1039z00_1278;

				BgL_g1039z00_1278 = BGl_getenvz00zz__osz00(BGl_string2281z00zz__osz00);
				if (CBOOL(BgL_g1039z00_1278))
					{	/* Llib/os.scm 333 */
						return BgL_g1039z00_1278;
					}
				else
					{	/* Llib/os.scm 334 */
						obj_t BgL_g1041z00_1281;

						BgL_g1041z00_1281 =
							BGl_getenvz00zz__osz00(BGl_string2282z00zz__osz00);
						if (CBOOL(BgL_g1041z00_1281))
							{	/* Llib/os.scm 334 */
								return BgL_g1041z00_1281;
							}
						else
							{	/* Llib/os.scm 335 */
								obj_t BgL_g1043z00_1284;

								BgL_g1043z00_1284 =
									BGl_getenvz00zz__osz00(BGl_string2283z00zz__osz00);
								if (CBOOL(BgL_g1043z00_1284))
									{	/* Llib/os.scm 335 */
										return BgL_g1043z00_1284;
									}
								else
									{	/* Llib/os.scm 335 */
										return string_to_bstring(OS_CHARSET);
									}
							}
					}
			}
		}

	}



/* &os-charset */
	obj_t BGl_z62oszd2charsetzb0zz__osz00(obj_t BgL_envz00_3554)
	{
		{	/* Llib/os.scm 331 */
			return BGl_oszd2charsetzd2zz__osz00();
		}

	}



/* command-line */
	BGL_EXPORTED_DEF obj_t BGl_commandzd2linezd2zz__osz00(void)
	{
		{	/* Llib/os.scm 341 */
			return command_line;
		}

	}



/* &command-line */
	obj_t BGl_z62commandzd2linezb0zz__osz00(obj_t BgL_envz00_3555)
	{
		{	/* Llib/os.scm 341 */
			return BGl_commandzd2linezd2zz__osz00();
		}

	}



/* executable-name */
	BGL_EXPORTED_DEF char *BGl_executablezd2namezd2zz__osz00(void)
	{
		{	/* Llib/os.scm 347 */
			return executable_name;
		}

	}



/* &executable-name */
	obj_t BGl_z62executablezd2namezb0zz__osz00(obj_t BgL_envz00_3556)
	{
		{	/* Llib/os.scm 347 */
			return string_to_bstring(BGl_executablezd2namezd2zz__osz00());
		}

	}



/* signal */
	BGL_EXPORTED_DEF obj_t BGl_signalz00zz__osz00(int BgL_numz00_3,
		obj_t BgL_procz00_4)
	{
		{	/* Llib/os.scm 353 */
			if ((BgL_procz00_4 == BGl_symbol2284z00zz__osz00))
				{	/* Llib/os.scm 355 */
					BGL_TAIL return bgl_signal(BgL_numz00_3, BTRUE);
				}
			else
				{	/* Llib/os.scm 355 */
					if ((BgL_procz00_4 == BGl_symbol2286z00zz__osz00))
						{	/* Llib/os.scm 357 */
							BGL_TAIL return bgl_signal(BgL_numz00_3, BFALSE);
						}
					else
						{	/* Llib/os.scm 359 */
							bool_t BgL_test2723z00_3964;

							{	/* Llib/os.scm 359 */
								int BgL_arg1252z00_1291;

								BgL_arg1252z00_1291 = PROCEDURE_ARITY(((obj_t) BgL_procz00_4));
								BgL_test2723z00_3964 = ((long) (BgL_arg1252z00_1291) == 1L);
							}
							if (BgL_test2723z00_3964)
								{	/* Llib/os.scm 359 */
									if (((long) (BgL_numz00_3) < 0L))
										{	/* Llib/os.scm 361 */
											return BUNSPEC;
										}
									else
										{	/* Llib/os.scm 361 */
											if (((long) (BgL_numz00_3) > 31L))
												{	/* Llib/os.scm 363 */
													return
														BGl_errorz00zz__errorz00(BGl_string2288z00zz__osz00,
														BGl_string2289z00zz__osz00, BINT(BgL_numz00_3));
												}
											else
												{	/* Llib/os.scm 363 */
													BGL_TAIL return
														bgl_signal(BgL_numz00_3, BgL_procz00_4);
												}
										}
								}
							else
								{	/* Llib/os.scm 359 */
									return
										BGl_errorz00zz__errorz00(BGl_string2288z00zz__osz00,
										BGl_string2290z00zz__osz00, BgL_procz00_4);
								}
						}
				}
		}

	}



/* &signal */
	obj_t BGl_z62signalz62zz__osz00(obj_t BgL_envz00_3557, obj_t BgL_numz00_3558,
		obj_t BgL_procz00_3559)
	{
		{	/* Llib/os.scm 353 */
			{	/* Llib/os.scm 355 */
				int BgL_auxz00_3979;

				{	/* Llib/os.scm 355 */
					obj_t BgL_tmpz00_3980;

					if (INTEGERP(BgL_numz00_3558))
						{	/* Llib/os.scm 355 */
							BgL_tmpz00_3980 = BgL_numz00_3558;
						}
					else
						{
							obj_t BgL_auxz00_3983;

							BgL_auxz00_3983 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
								BINT(14192L), BGl_string2292z00zz__osz00,
								BGl_string2293z00zz__osz00, BgL_numz00_3558);
							FAILURE(BgL_auxz00_3983, BFALSE, BFALSE);
						}
					BgL_auxz00_3979 = CINT(BgL_tmpz00_3980);
				}
				return BGl_signalz00zz__osz00(BgL_auxz00_3979, BgL_procz00_3559);
			}
		}

	}



/* get-signal-handler */
	BGL_EXPORTED_DEF obj_t BGl_getzd2signalzd2handlerz00zz__osz00(int
		BgL_numz00_5)
	{
		{	/* Llib/os.scm 371 */
			{	/* Llib/os.scm 372 */
				obj_t BgL_vz00_2487;

				BgL_vz00_2487 = bgl_get_signal_handler(BgL_numz00_5);
				if ((BgL_vz00_2487 == BTRUE))
					{	/* Llib/os.scm 374 */
						return BGl_symbol2284z00zz__osz00;
					}
				else
					{	/* Llib/os.scm 374 */
						if ((BgL_vz00_2487 == BFALSE))
							{	/* Llib/os.scm 375 */
								return BGl_symbol2286z00zz__osz00;
							}
						else
							{	/* Llib/os.scm 375 */
								return BgL_vz00_2487;
							}
					}
			}
		}

	}



/* &get-signal-handler */
	obj_t BGl_z62getzd2signalzd2handlerz62zz__osz00(obj_t BgL_envz00_3560,
		obj_t BgL_numz00_3561)
	{
		{	/* Llib/os.scm 371 */
			{	/* Llib/os.scm 372 */
				int BgL_auxz00_3994;

				{	/* Llib/os.scm 372 */
					obj_t BgL_tmpz00_3995;

					if (INTEGERP(BgL_numz00_3561))
						{	/* Llib/os.scm 372 */
							BgL_tmpz00_3995 = BgL_numz00_3561;
						}
					else
						{
							obj_t BgL_auxz00_3998;

							BgL_auxz00_3998 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
								BINT(14797L), BGl_string2294z00zz__osz00,
								BGl_string2293z00zz__osz00, BgL_numz00_3561);
							FAILURE(BgL_auxz00_3998, BFALSE, BFALSE);
						}
					BgL_auxz00_3994 = CINT(BgL_tmpz00_3995);
				}
				return BGl_getzd2signalzd2handlerz00zz__osz00(BgL_auxz00_3994);
			}
		}

	}



/* sigsetmask */
	BGL_EXPORTED_DEF int BGl_sigsetmaskz00zz__osz00(int BgL_nz00_6)
	{
		{	/* Llib/os.scm 381 */
			return BGL_SIGSETMASK(BgL_nz00_6);
		}

	}



/* &sigsetmask */
	obj_t BGl_z62sigsetmaskz62zz__osz00(obj_t BgL_envz00_3562,
		obj_t BgL_nz00_3563)
	{
		{	/* Llib/os.scm 381 */
			{	/* Llib/os.scm 382 */
				int BgL_tmpz00_4005;

				{	/* Llib/os.scm 382 */
					int BgL_auxz00_4006;

					{	/* Llib/os.scm 382 */
						obj_t BgL_tmpz00_4007;

						if (INTEGERP(BgL_nz00_3563))
							{	/* Llib/os.scm 382 */
								BgL_tmpz00_4007 = BgL_nz00_3563;
							}
						else
							{
								obj_t BgL_auxz00_4010;

								BgL_auxz00_4010 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
									BINT(15163L), BGl_string2295z00zz__osz00,
									BGl_string2293z00zz__osz00, BgL_nz00_3563);
								FAILURE(BgL_auxz00_4010, BFALSE, BFALSE);
							}
						BgL_auxz00_4006 = CINT(BgL_tmpz00_4007);
					}
					BgL_tmpz00_4005 = BGl_sigsetmaskz00zz__osz00(BgL_auxz00_4006);
				}
				return BINT(BgL_tmpz00_4005);
			}
		}

	}



/* _getenv */
	obj_t BGl__getenvz00zz__osz00(obj_t BgL_env1140z00_9, obj_t BgL_opt1139z00_8)
	{
		{	/* Llib/os.scm 387 */
			{	/* Llib/os.scm 387 */

				switch (VECTOR_LENGTH(BgL_opt1139z00_8))
					{
					case 0L:

						{	/* Llib/os.scm 387 */

							return BGl_getenvz00zz__osz00(BFALSE);
						}
						break;
					case 1L:

						{	/* Llib/os.scm 387 */
							obj_t BgL_namez00_1296;

							BgL_namez00_1296 = VECTOR_REF(BgL_opt1139z00_8, 0L);
							{	/* Llib/os.scm 387 */

								return BGl_getenvz00zz__osz00(BgL_namez00_1296);
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* getenv */
	BGL_EXPORTED_DEF obj_t BGl_getenvz00zz__osz00(obj_t BgL_namez00_7)
	{
		{	/* Llib/os.scm 387 */
			if (STRINGP(BgL_namez00_7))
				{	/* Llib/os.scm 388 */
					{	/* Llib/os.scm 390 */
						bool_t BgL_test2732z00_4024;

						{	/* Llib/os.scm 390 */
							bool_t BgL_test2733z00_4025;

							{	/* Llib/os.scm 390 */
								obj_t BgL_string1z00_2489;

								BgL_string1z00_2489 = string_to_bstring(OS_CLASS);
								{	/* Llib/os.scm 390 */
									long BgL_l1z00_2491;

									BgL_l1z00_2491 = STRING_LENGTH(BgL_string1z00_2489);
									if ((BgL_l1z00_2491 == 5L))
										{	/* Llib/os.scm 390 */
											int BgL_arg1901z00_2494;

											{	/* Llib/os.scm 390 */
												char *BgL_auxz00_4032;
												char *BgL_tmpz00_4030;

												BgL_auxz00_4032 =
													BSTRING_TO_STRING(BGl_string2296z00zz__osz00);
												BgL_tmpz00_4030 =
													BSTRING_TO_STRING(BgL_string1z00_2489);
												BgL_arg1901z00_2494 =
													memcmp(BgL_tmpz00_4030, BgL_auxz00_4032,
													BgL_l1z00_2491);
											}
											BgL_test2733z00_4025 =
												((long) (BgL_arg1901z00_2494) == 0L);
										}
									else
										{	/* Llib/os.scm 390 */
											BgL_test2733z00_4025 = ((bool_t) 0);
										}
								}
							}
							if (BgL_test2733z00_4025)
								{	/* Llib/os.scm 390 */
									obj_t BgL_string1z00_2500;

									BgL_string1z00_2500 = BgL_namez00_7;
									{	/* Llib/os.scm 390 */
										long BgL_l1z00_2502;

										BgL_l1z00_2502 = STRING_LENGTH(BgL_string1z00_2500);
										if ((BgL_l1z00_2502 == 4L))
											{	/* Llib/os.scm 390 */
												int BgL_arg1901z00_2505;

												{	/* Llib/os.scm 390 */
													char *BgL_auxz00_4042;
													char *BgL_tmpz00_4040;

													BgL_auxz00_4042 =
														BSTRING_TO_STRING(BGl_string2297z00zz__osz00);
													BgL_tmpz00_4040 =
														BSTRING_TO_STRING(BgL_string1z00_2500);
													BgL_arg1901z00_2505 =
														memcmp(BgL_tmpz00_4040, BgL_auxz00_4042,
														BgL_l1z00_2502);
												}
												BgL_test2732z00_4024 =
													((long) (BgL_arg1901z00_2505) == 0L);
											}
										else
											{	/* Llib/os.scm 390 */
												BgL_test2732z00_4024 = ((bool_t) 0);
											}
									}
								}
							else
								{	/* Llib/os.scm 390 */
									BgL_test2732z00_4024 = ((bool_t) 0);
								}
						}
						if (BgL_test2732z00_4024)
							{	/* Llib/os.scm 390 */
								BgL_namez00_7 = BGl_string2298z00zz__osz00;
							}
						else
							{	/* Llib/os.scm 390 */
								BFALSE;
							}
					}
					{	/* Llib/os.scm 392 */
						bool_t BgL_test2736z00_4047;

						{	/* Llib/os.scm 392 */
							char *BgL_tmpz00_4048;

							BgL_tmpz00_4048 = BSTRING_TO_STRING(BgL_namez00_7);
							BgL_test2736z00_4047 = (long) getenv(BgL_tmpz00_4048);
						}
						if (BgL_test2736z00_4047)
							{	/* Llib/os.scm 393 */
								char *BgL_resultz00_1305;

								{	/* Llib/os.scm 393 */
									char *BgL_tmpz00_4051;

									BgL_tmpz00_4051 = BSTRING_TO_STRING(BgL_namez00_7);
									BgL_resultz00_1305 = (char *) getenv(BgL_tmpz00_4051);
								}
								if (STRING_PTR_NULL(BgL_resultz00_1305))
									{	/* Llib/os.scm 394 */
										return BFALSE;
									}
								else
									{	/* Llib/os.scm 394 */
										return string_to_bstring(BgL_resultz00_1305);
									}
							}
						else
							{	/* Llib/os.scm 392 */
								return BFALSE;
							}
					}
				}
			else
				{	/* Llib/os.scm 388 */
					return bgl_getenv_all();
				}
		}

	}



/* putenv */
	BGL_EXPORTED_DEF obj_t BGl_putenvz00zz__osz00(char *BgL_stringz00_10,
		char *BgL_valz00_11)
	{
		{	/* Llib/os.scm 403 */
			{	/* Llib/os.scm 404 */
				bool_t BgL_test2738z00_4058;

				{	/* Llib/os.scm 404 */
					bool_t BgL_test2739z00_4059;

					{	/* Llib/os.scm 404 */
						obj_t BgL_string1z00_2516;

						BgL_string1z00_2516 = string_to_bstring(OS_CLASS);
						{	/* Llib/os.scm 404 */
							long BgL_l1z00_2518;

							BgL_l1z00_2518 = STRING_LENGTH(BgL_string1z00_2516);
							if ((BgL_l1z00_2518 == 5L))
								{	/* Llib/os.scm 404 */
									int BgL_arg1901z00_2521;

									{	/* Llib/os.scm 404 */
										char *BgL_auxz00_4066;
										char *BgL_tmpz00_4064;

										BgL_auxz00_4066 =
											BSTRING_TO_STRING(BGl_string2296z00zz__osz00);
										BgL_tmpz00_4064 = BSTRING_TO_STRING(BgL_string1z00_2516);
										BgL_arg1901z00_2521 =
											memcmp(BgL_tmpz00_4064, BgL_auxz00_4066, BgL_l1z00_2518);
									}
									BgL_test2739z00_4059 = ((long) (BgL_arg1901z00_2521) == 0L);
								}
							else
								{	/* Llib/os.scm 404 */
									BgL_test2739z00_4059 = ((bool_t) 0);
								}
						}
					}
					if (BgL_test2739z00_4059)
						{	/* Llib/os.scm 405 */
							obj_t BgL_string1z00_2527;

							BgL_string1z00_2527 = string_to_bstring(BgL_stringz00_10);
							{	/* Llib/os.scm 405 */
								long BgL_l1z00_2529;

								BgL_l1z00_2529 = STRING_LENGTH(BgL_string1z00_2527);
								if ((BgL_l1z00_2529 == 4L))
									{	/* Llib/os.scm 405 */
										int BgL_arg1901z00_2532;

										{	/* Llib/os.scm 405 */
											char *BgL_auxz00_4077;
											char *BgL_tmpz00_4075;

											BgL_auxz00_4077 =
												BSTRING_TO_STRING(BGl_string2297z00zz__osz00);
											BgL_tmpz00_4075 = BSTRING_TO_STRING(BgL_string1z00_2527);
											BgL_arg1901z00_2532 =
												memcmp(BgL_tmpz00_4075, BgL_auxz00_4077,
												BgL_l1z00_2529);
										}
										BgL_test2738z00_4058 = ((long) (BgL_arg1901z00_2532) == 0L);
									}
								else
									{	/* Llib/os.scm 405 */
										BgL_test2738z00_4058 = ((bool_t) 0);
									}
							}
						}
					else
						{	/* Llib/os.scm 404 */
							BgL_test2738z00_4058 = ((bool_t) 0);
						}
				}
				if (BgL_test2738z00_4058)
					{	/* Llib/os.scm 404 */
						BgL_stringz00_10 = BSTRING_TO_STRING(BGl_string2298z00zz__osz00);
					}
				else
					{	/* Llib/os.scm 404 */
						BFALSE;
					}
			}
			{	/* Llib/os.scm 407 */
				int BgL_arg1309z00_2515;

				BgL_arg1309z00_2515 = bgl_setenv(BgL_stringz00_10, BgL_valz00_11);
				return BBOOL(((long) (BgL_arg1309z00_2515) == 0L));
		}}

	}



/* &putenv */
	obj_t BGl_z62putenvz62zz__osz00(obj_t BgL_envz00_3564,
		obj_t BgL_stringz00_3565, obj_t BgL_valz00_3566)
	{
		{	/* Llib/os.scm 403 */
			{	/* Llib/os.scm 406 */
				char *BgL_auxz00_4096;
				char *BgL_auxz00_4087;

				{	/* Llib/os.scm 406 */
					obj_t BgL_tmpz00_4097;

					if (STRINGP(BgL_valz00_3566))
						{	/* Llib/os.scm 406 */
							BgL_tmpz00_4097 = BgL_valz00_3566;
						}
					else
						{
							obj_t BgL_auxz00_4100;

							BgL_auxz00_4100 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
								BINT(16093L), BGl_string2299z00zz__osz00,
								BGl_string2300z00zz__osz00, BgL_valz00_3566);
							FAILURE(BgL_auxz00_4100, BFALSE, BFALSE);
						}
					BgL_auxz00_4096 = BSTRING_TO_STRING(BgL_tmpz00_4097);
				}
				{	/* Llib/os.scm 406 */
					obj_t BgL_tmpz00_4088;

					if (STRINGP(BgL_stringz00_3565))
						{	/* Llib/os.scm 406 */
							BgL_tmpz00_4088 = BgL_stringz00_3565;
						}
					else
						{
							obj_t BgL_auxz00_4091;

							BgL_auxz00_4091 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
								BINT(16093L), BGl_string2299z00zz__osz00,
								BGl_string2300z00zz__osz00, BgL_stringz00_3565);
							FAILURE(BgL_auxz00_4091, BFALSE, BFALSE);
						}
					BgL_auxz00_4087 = BSTRING_TO_STRING(BgL_tmpz00_4088);
				}
				return BGl_putenvz00zz__osz00(BgL_auxz00_4087, BgL_auxz00_4096);
			}
		}

	}



/* system */
	BGL_EXPORTED_DEF obj_t BGl_systemz00zz__osz00(obj_t BgL_stringsz00_12)
	{
		{	/* Llib/os.scm 412 */
			if (NULLP(BgL_stringsz00_12))
				{	/* Llib/os.scm 414 */
					return BFALSE;
				}
			else
				{	/* Llib/os.scm 414 */
					if (NULLP(CDR(((obj_t) BgL_stringsz00_12))))
						{	/* Llib/os.scm 417 */
							obj_t BgL_arg1314z00_1317;

							BgL_arg1314z00_1317 = CAR(((obj_t) BgL_stringsz00_12));
							{	/* Llib/os.scm 417 */
								int BgL_tmpz00_4114;

								{	/* Llib/os.scm 417 */
									char *BgL_tmpz00_4115;

									BgL_tmpz00_4115 = BSTRING_TO_STRING(BgL_arg1314z00_1317);
									BgL_tmpz00_4114 = system(BgL_tmpz00_4115);
								}
								return BINT(BgL_tmpz00_4114);
							}
						}
					else
						{	/* Llib/os.scm 419 */
							obj_t BgL_arg1315z00_1318;

							BgL_arg1315z00_1318 =
								BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_stringsz00_12);
							{	/* Llib/os.scm 419 */
								int BgL_tmpz00_4120;

								{	/* Llib/os.scm 419 */
									char *BgL_tmpz00_4121;

									BgL_tmpz00_4121 = BSTRING_TO_STRING(BgL_arg1315z00_1318);
									BgL_tmpz00_4120 = system(BgL_tmpz00_4121);
								}
								return BINT(BgL_tmpz00_4120);
							}
						}
				}
		}

	}



/* &system */
	obj_t BGl_z62systemz62zz__osz00(obj_t BgL_envz00_3567,
		obj_t BgL_stringsz00_3568)
	{
		{	/* Llib/os.scm 412 */
			return BGl_systemz00zz__osz00(BgL_stringsz00_3568);
		}

	}



/* system->string */
	BGL_EXPORTED_DEF obj_t BGl_systemzd2ze3stringz31zz__osz00(obj_t
		BgL_stringsz00_13)
	{
		{	/* Llib/os.scm 424 */
			{	/* Llib/os.scm 425 */
				obj_t BgL_pz00_1321;

				{	/* Llib/os.scm 425 */
					obj_t BgL_arg1319z00_1327;

					{	/* Llib/os.scm 425 */
						obj_t BgL_runner1322z00_1332;

						{	/* Llib/os.scm 425 */
							obj_t BgL_list1321z00_1331;

							BgL_list1321z00_1331 = MAKE_YOUNG_PAIR(BgL_stringsz00_13, BNIL);
							BgL_runner1322z00_1332 =
								BGl_consza2za2zz__r4_pairs_and_lists_6_3z00
								(BGl_string2301z00zz__osz00, BgL_list1321z00_1331);
						}
						BgL_arg1319z00_1327 =
							BGl_stringzd2appendzd2zz__r4_strings_6_7z00
							(BgL_runner1322z00_1332);
					}
					{	/* Ieee/port.scm 466 */

						BgL_pz00_1321 =
							BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00
							(BgL_arg1319z00_1327, BTRUE, BINT(5000000L));
					}
				}
				{	/* Llib/os.scm 426 */
					obj_t BgL_exitd1046z00_1322;

					BgL_exitd1046z00_1322 = BGL_EXITD_TOP_AS_OBJ();
					{	/* Llib/os.scm 428 */
						obj_t BgL_zc3z04anonymousza31318ze3z87_3569;

						BgL_zc3z04anonymousza31318ze3z87_3569 =
							MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31318ze3ze5zz__osz00,
							(int) (0L), (int) (1L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31318ze3z87_3569,
							(int) (0L), BgL_pz00_1321);
						{	/* Llib/os.scm 426 */
							obj_t BgL_arg2166z00_2542;

							{	/* Llib/os.scm 426 */
								obj_t BgL_arg2167z00_2543;

								BgL_arg2167z00_2543 = BGL_EXITD_PROTECT(BgL_exitd1046z00_1322);
								BgL_arg2166z00_2542 =
									MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31318ze3z87_3569,
									BgL_arg2167z00_2543);
							}
							BGL_EXITD_PROTECT_SET(BgL_exitd1046z00_1322, BgL_arg2166z00_2542);
							BUNSPEC;
						}
						{	/* Llib/os.scm 427 */
							obj_t BgL_tmp1048z00_1324;

							BgL_tmp1048z00_1324 =
								BGl_readzd2stringzd2zz__r4_input_6_10_2z00(BgL_pz00_1321);
							{	/* Llib/os.scm 426 */
								bool_t BgL_test2746z00_4141;

								{	/* Llib/os.scm 426 */
									obj_t BgL_arg2165z00_2545;

									BgL_arg2165z00_2545 =
										BGL_EXITD_PROTECT(BgL_exitd1046z00_1322);
									BgL_test2746z00_4141 = PAIRP(BgL_arg2165z00_2545);
								}
								if (BgL_test2746z00_4141)
									{	/* Llib/os.scm 426 */
										obj_t BgL_arg2163z00_2546;

										{	/* Llib/os.scm 426 */
											obj_t BgL_arg2164z00_2547;

											BgL_arg2164z00_2547 =
												BGL_EXITD_PROTECT(BgL_exitd1046z00_1322);
											BgL_arg2163z00_2546 = CDR(((obj_t) BgL_arg2164z00_2547));
										}
										BGL_EXITD_PROTECT_SET(BgL_exitd1046z00_1322,
											BgL_arg2163z00_2546);
										BUNSPEC;
									}
								else
									{	/* Llib/os.scm 426 */
										BFALSE;
									}
							}
							bgl_close_input_port(((obj_t) BgL_pz00_1321));
							return BgL_tmp1048z00_1324;
						}
					}
				}
			}
		}

	}



/* &system->string */
	obj_t BGl_z62systemzd2ze3stringz53zz__osz00(obj_t BgL_envz00_3570,
		obj_t BgL_stringsz00_3571)
	{
		{	/* Llib/os.scm 424 */
			return BGl_systemzd2ze3stringz31zz__osz00(BgL_stringsz00_3571);
		}

	}



/* &<@anonymous:1318> */
	obj_t BGl_z62zc3z04anonymousza31318ze3ze5zz__osz00(obj_t BgL_envz00_3572)
	{
		{	/* Llib/os.scm 426 */
			{	/* Llib/os.scm 428 */
				obj_t BgL_pz00_3573;

				BgL_pz00_3573 = PROCEDURE_REF(BgL_envz00_3572, (int) (0L));
				return bgl_close_input_port(((obj_t) BgL_pz00_3573));
			}
		}

	}



/* date */
	BGL_EXPORTED_DEF char *BGl_datez00zz__osz00(void)
	{
		{	/* Llib/os.scm 433 */
			{	/* Llib/os.scm 434 */
				char *BgL_dtz00_1333;

				BgL_dtz00_1333 = c_date();
				{	/* Llib/os.scm 434 */
					long BgL_lenz00_1334;

					BgL_lenz00_1334 = STRING_LENGTH(string_to_bstring(BgL_dtz00_1333));
					{	/* Llib/os.scm 435 */

						if (
							(STRING_REF(string_to_bstring(BgL_dtz00_1333),
									(BgL_lenz00_1334 - 1L)) == ((unsigned char) 10)))
							{	/* Llib/os.scm 437 */
								long BgL_arg1327z00_1338;

								BgL_arg1327z00_1338 = (BgL_lenz00_1334 - 1L);
								{	/* Llib/os.scm 437 */
									obj_t BgL_stringz00_2557;

									BgL_stringz00_2557 = string_to_bstring(BgL_dtz00_1333);
									return
										BSTRING_TO_STRING(c_substring(BgL_stringz00_2557, 0L,
											BgL_arg1327z00_1338));
								}
							}
						else
							{	/* Llib/os.scm 436 */
								return BgL_dtz00_1333;
							}
					}
				}
			}
		}

	}



/* &date */
	obj_t BGl_z62datez62zz__osz00(obj_t BgL_envz00_3574)
	{
		{	/* Llib/os.scm 433 */
			return string_to_bstring(BGl_datez00zz__osz00());
		}

	}



/* chdir */
	BGL_EXPORTED_DEF bool_t BGl_chdirz00zz__osz00(char *BgL_dirnamez00_14)
	{
		{	/* Llib/os.scm 443 */
			if (chdir(BgL_dirnamez00_14))
				{	/* Llib/os.scm 444 */
					return ((bool_t) 0);
				}
			else
				{	/* Llib/os.scm 444 */
					return ((bool_t) 1);
				}
		}

	}



/* &chdir */
	obj_t BGl_z62chdirz62zz__osz00(obj_t BgL_envz00_3575,
		obj_t BgL_dirnamez00_3576)
	{
		{	/* Llib/os.scm 443 */
			{	/* Llib/os.scm 444 */
				bool_t BgL_tmpz00_4171;

				{	/* Llib/os.scm 444 */
					char *BgL_auxz00_4172;

					{	/* Llib/os.scm 444 */
						obj_t BgL_tmpz00_4173;

						if (STRINGP(BgL_dirnamez00_3576))
							{	/* Llib/os.scm 444 */
								BgL_tmpz00_4173 = BgL_dirnamez00_3576;
							}
						else
							{
								obj_t BgL_auxz00_4176;

								BgL_auxz00_4176 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
									BINT(17587L), BGl_string2302z00zz__osz00,
									BGl_string2300z00zz__osz00, BgL_dirnamez00_3576);
								FAILURE(BgL_auxz00_4176, BFALSE, BFALSE);
							}
						BgL_auxz00_4172 = BSTRING_TO_STRING(BgL_tmpz00_4173);
					}
					BgL_tmpz00_4171 = BGl_chdirz00zz__osz00(BgL_auxz00_4172);
				}
				return BBOOL(BgL_tmpz00_4171);
			}
		}

	}



/* pwd */
	BGL_EXPORTED_DEF obj_t BGl_pwdz00zz__osz00(void)
	{
		{	/* Llib/os.scm 449 */
			{	/* Llib/os.scm 450 */
				obj_t BgL_stringz00_1342;

				{	/* Ieee/string.scm 172 */

					BgL_stringz00_1342 = make_string(1024L, ((unsigned char) ' '));
				}
				{	/* Llib/os.scm 451 */
					char *BgL_tmpz00_4184;

					{	/* Llib/os.scm 451 */
						int BgL_auxz00_4187;
						char *BgL_tmpz00_4185;

						BgL_auxz00_4187 = (int) (1024L);
						BgL_tmpz00_4185 = BSTRING_TO_STRING(BgL_stringz00_1342);
						BgL_tmpz00_4184 =
							(char *) (long) getcwd(BgL_tmpz00_4185, BgL_auxz00_4187);
					}
					return string_to_bstring(BgL_tmpz00_4184);
				}
			}
		}

	}



/* &pwd */
	obj_t BGl_z62pwdz62zz__osz00(obj_t BgL_envz00_3577)
	{
		{	/* Llib/os.scm 449 */
			return BGl_pwdz00zz__osz00();
		}

	}



/* basename */
	BGL_EXPORTED_DEF obj_t BGl_basenamez00zz__osz00(obj_t BgL_stringz00_15)
	{
		{	/* Llib/os.scm 456 */
			{	/* Llib/os.scm 457 */
				bool_t BgL_test2750z00_4192;

				{	/* Llib/os.scm 457 */
					obj_t BgL_string1z00_2563;

					BgL_string1z00_2563 = string_to_bstring(OS_CLASS);
					{	/* Llib/os.scm 457 */
						long BgL_l1z00_2565;

						BgL_l1z00_2565 = STRING_LENGTH(BgL_string1z00_2563);
						if ((BgL_l1z00_2565 == 5L))
							{	/* Llib/os.scm 457 */
								int BgL_arg1901z00_2568;

								{	/* Llib/os.scm 457 */
									char *BgL_auxz00_4199;
									char *BgL_tmpz00_4197;

									BgL_auxz00_4199 =
										BSTRING_TO_STRING(BGl_string2303z00zz__osz00);
									BgL_tmpz00_4197 = BSTRING_TO_STRING(BgL_string1z00_2563);
									BgL_arg1901z00_2568 =
										memcmp(BgL_tmpz00_4197, BgL_auxz00_4199, BgL_l1z00_2565);
								}
								BgL_test2750z00_4192 = ((long) (BgL_arg1901z00_2568) == 0L);
							}
						else
							{	/* Llib/os.scm 457 */
								BgL_test2750z00_4192 = ((bool_t) 0);
							}
					}
				}
				if (BgL_test2750z00_4192)
					{	/* Llib/os.scm 457 */
						BGL_TAIL return BGl_mingwzd2basenamezd2zz__osz00(BgL_stringz00_15);
					}
				else
					{	/* Llib/os.scm 457 */
						BGL_TAIL return
							BGl_defaultzd2basenamezd2zz__osz00(BgL_stringz00_15);
					}
			}
		}

	}



/* &basename */
	obj_t BGl_z62basenamez62zz__osz00(obj_t BgL_envz00_3578,
		obj_t BgL_stringz00_3579)
	{
		{	/* Llib/os.scm 456 */
			{	/* Llib/os.scm 457 */
				obj_t BgL_auxz00_4206;

				if (STRINGP(BgL_stringz00_3579))
					{	/* Llib/os.scm 457 */
						BgL_auxz00_4206 = BgL_stringz00_3579;
					}
				else
					{
						obj_t BgL_auxz00_4209;

						BgL_auxz00_4209 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
							BINT(18178L), BGl_string2304z00zz__osz00,
							BGl_string2300z00zz__osz00, BgL_stringz00_3579);
						FAILURE(BgL_auxz00_4209, BFALSE, BFALSE);
					}
				return BGl_basenamez00zz__osz00(BgL_auxz00_4206);
			}
		}

	}



/* mingw-basename */
	obj_t BGl_mingwzd2basenamezd2zz__osz00(obj_t BgL_stringz00_16)
	{
		{	/* Llib/os.scm 464 */
			{	/* Llib/os.scm 465 */
				long BgL_nz00_1348;
				bool_t BgL_stopz00_1349;

				BgL_nz00_1348 = STRING_LENGTH(BgL_stringz00_16);
				BgL_stopz00_1349 = ((bool_t) 0);
				{
					long BgL_iz00_1352;

					BgL_iz00_1352 = (BgL_nz00_1348 - 1L);
				BgL_zc3z04anonymousza31335ze3z87_1353:
					if ((BBOOL(BgL_stopz00_1349) == BTRUE))
						{	/* Llib/os.scm 467 */
							return
								c_substring(BgL_stringz00_16,
								(BgL_iz00_1352 + 2L), BgL_nz00_1348);
						}
					else
						{	/* Llib/os.scm 467 */
							if ((BgL_iz00_1352 < 0L))
								{	/* Llib/os.scm 470 */
									BgL_stopz00_1349 = ((bool_t) 1);
								}
							else
								{	/* Llib/os.scm 472 */
									bool_t BgL__ortest_1051z00_1356;

									BgL__ortest_1051z00_1356 =
										(STRING_REF(BgL_stringz00_16,
											BgL_iz00_1352) == ((unsigned char) '\\'));
									if (BgL__ortest_1051z00_1356)
										{	/* Llib/os.scm 472 */
											BgL_stopz00_1349 = BgL__ortest_1051z00_1356;
										}
									else
										{	/* Llib/os.scm 472 */
											BgL_stopz00_1349 =
												(STRING_REF(BgL_stringz00_16,
													BgL_iz00_1352) == ((unsigned char) '/'));
								}}
							{
								long BgL_iz00_4227;

								BgL_iz00_4227 = (BgL_iz00_1352 - 1L);
								BgL_iz00_1352 = BgL_iz00_4227;
								goto BgL_zc3z04anonymousza31335ze3z87_1353;
							}
						}
				}
			}
		}

	}



/* default-basename */
	obj_t BGl_defaultzd2basenamezd2zz__osz00(obj_t BgL_stringz00_17)
	{
		{	/* Llib/os.scm 478 */
			{	/* Llib/os.scm 479 */
				long BgL_lenz00_1361;

				BgL_lenz00_1361 = (STRING_LENGTH(BgL_stringz00_17) - 1L);
				{	/* Llib/os.scm 479 */
					long BgL_startz00_1362;

					{	/* Llib/os.scm 480 */
						bool_t BgL_test2756z00_4232;

						if ((BgL_lenz00_1361 > 0L))
							{	/* Llib/os.scm 480 */
								BgL_test2756z00_4232 =
									(STRING_REF(BgL_stringz00_17, BgL_lenz00_1361) ==
									(unsigned char) (FILE_SEPARATOR));
							}
						else
							{	/* Llib/os.scm 480 */
								BgL_test2756z00_4232 = ((bool_t) 0);
							}
						if (BgL_test2756z00_4232)
							{	/* Llib/os.scm 480 */
								BgL_startz00_1362 = (BgL_lenz00_1361 - 1L);
							}
						else
							{	/* Llib/os.scm 480 */
								BgL_startz00_1362 = BgL_lenz00_1361;
							}
					}
					{	/* Llib/os.scm 480 */

						{
							long BgL_indexz00_1364;

							BgL_indexz00_1364 = BgL_startz00_1362;
						BgL_zc3z04anonymousza31341ze3z87_1365:
							if ((BgL_indexz00_1364 == -1L))
								{	/* Llib/os.scm 487 */
									return BgL_stringz00_17;
								}
							else
								{	/* Llib/os.scm 487 */
									if (
										(STRING_REF(BgL_stringz00_17, BgL_indexz00_1364) ==
											(unsigned char) (FILE_SEPARATOR)))
										{	/* Llib/os.scm 489 */
											return
												c_substring(BgL_stringz00_17,
												(BgL_indexz00_1364 + 1L), (BgL_startz00_1362 + 1L));
										}
									else
										{
											long BgL_indexz00_4248;

											BgL_indexz00_4248 = (BgL_indexz00_1364 - 1L);
											BgL_indexz00_1364 = BgL_indexz00_4248;
											goto BgL_zc3z04anonymousza31341ze3z87_1365;
										}
								}
						}
					}
				}
			}
		}

	}



/* prefix */
	BGL_EXPORTED_DEF obj_t BGl_prefixz00zz__osz00(obj_t BgL_stringz00_18)
	{
		{	/* Llib/os.scm 497 */
			{	/* Llib/os.scm 498 */
				long BgL_lenz00_1380;

				BgL_lenz00_1380 = (STRING_LENGTH(BgL_stringz00_18) - 1L);
				{
					long BgL_ez00_1382;
					long BgL_sz00_1383;

					BgL_ez00_1382 = BgL_lenz00_1380;
					BgL_sz00_1383 = BgL_lenz00_1380;
				BgL_zc3z04anonymousza31357ze3z87_1384:
					if ((BgL_sz00_1383 <= 0L))
						{	/* Llib/os.scm 502 */
							return c_substring(BgL_stringz00_18, 0L, (1L + BgL_ez00_1382));
						}
					else
						{	/* Llib/os.scm 505 */
							bool_t BgL_test2761z00_4256;

							if (
								(BCHAR(STRING_REF(BgL_stringz00_18, BgL_sz00_1383)) ==
									BCHAR(((unsigned char) '.'))))
								{	/* Llib/os.scm 505 */
									BgL_test2761z00_4256 = (BgL_ez00_1382 == BgL_lenz00_1380);
								}
							else
								{	/* Llib/os.scm 505 */
									BgL_test2761z00_4256 = ((bool_t) 0);
								}
							if (BgL_test2761z00_4256)
								{
									long BgL_sz00_4265;
									long BgL_ez00_4263;

									BgL_ez00_4263 = (BgL_sz00_1383 - 1L);
									BgL_sz00_4265 = (BgL_sz00_1383 - 1L);
									BgL_sz00_1383 = BgL_sz00_4265;
									BgL_ez00_1382 = BgL_ez00_4263;
									goto BgL_zc3z04anonymousza31357ze3z87_1384;
								}
							else
								{
									long BgL_sz00_4267;

									BgL_sz00_4267 = (BgL_sz00_1383 - 1L);
									BgL_sz00_1383 = BgL_sz00_4267;
									goto BgL_zc3z04anonymousza31357ze3z87_1384;
								}
						}
				}
			}
		}

	}



/* &prefix */
	obj_t BGl_z62prefixz62zz__osz00(obj_t BgL_envz00_3580,
		obj_t BgL_stringz00_3581)
	{
		{	/* Llib/os.scm 497 */
			{	/* Llib/os.scm 498 */
				obj_t BgL_auxz00_4269;

				if (STRINGP(BgL_stringz00_3581))
					{	/* Llib/os.scm 498 */
						BgL_auxz00_4269 = BgL_stringz00_3581;
					}
				else
					{
						obj_t BgL_auxz00_4272;

						BgL_auxz00_4272 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
							BINT(19723L), BGl_string2305z00zz__osz00,
							BGl_string2300z00zz__osz00, BgL_stringz00_3581);
						FAILURE(BgL_auxz00_4272, BFALSE, BFALSE);
					}
				return BGl_prefixz00zz__osz00(BgL_auxz00_4269);
			}
		}

	}



/* dirname */
	BGL_EXPORTED_DEF obj_t BGl_dirnamez00zz__osz00(obj_t BgL_stringz00_19)
	{
		{	/* Llib/os.scm 513 */
			{	/* Llib/os.scm 514 */
				bool_t BgL_test2764z00_4277;

				{	/* Llib/os.scm 514 */
					obj_t BgL_string1z00_2624;

					BgL_string1z00_2624 = string_to_bstring(OS_CLASS);
					{	/* Llib/os.scm 514 */
						long BgL_l1z00_2626;

						BgL_l1z00_2626 = STRING_LENGTH(BgL_string1z00_2624);
						if ((BgL_l1z00_2626 == 5L))
							{	/* Llib/os.scm 514 */
								int BgL_arg1901z00_2629;

								{	/* Llib/os.scm 514 */
									char *BgL_auxz00_4284;
									char *BgL_tmpz00_4282;

									BgL_auxz00_4284 =
										BSTRING_TO_STRING(BGl_string2303z00zz__osz00);
									BgL_tmpz00_4282 = BSTRING_TO_STRING(BgL_string1z00_2624);
									BgL_arg1901z00_2629 =
										memcmp(BgL_tmpz00_4282, BgL_auxz00_4284, BgL_l1z00_2626);
								}
								BgL_test2764z00_4277 = ((long) (BgL_arg1901z00_2629) == 0L);
							}
						else
							{	/* Llib/os.scm 514 */
								BgL_test2764z00_4277 = ((bool_t) 0);
							}
					}
				}
				if (BgL_test2764z00_4277)
					{	/* Llib/os.scm 514 */
						BGL_TAIL return BGl_mingwzd2dirnamezd2zz__osz00(BgL_stringz00_19);
					}
				else
					{	/* Llib/os.scm 514 */
						BGL_TAIL return BGl_defaultzd2dirnamezd2zz__osz00(BgL_stringz00_19);
					}
			}
		}

	}



/* &dirname */
	obj_t BGl_z62dirnamez62zz__osz00(obj_t BgL_envz00_3582,
		obj_t BgL_stringz00_3583)
	{
		{	/* Llib/os.scm 513 */
			{	/* Llib/os.scm 514 */
				obj_t BgL_auxz00_4291;

				if (STRINGP(BgL_stringz00_3583))
					{	/* Llib/os.scm 514 */
						BgL_auxz00_4291 = BgL_stringz00_3583;
					}
				else
					{
						obj_t BgL_auxz00_4294;

						BgL_auxz00_4294 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
							BINT(20345L), BGl_string2306z00zz__osz00,
							BGl_string2300z00zz__osz00, BgL_stringz00_3583);
						FAILURE(BgL_auxz00_4294, BFALSE, BFALSE);
					}
				return BGl_dirnamez00zz__osz00(BgL_auxz00_4291);
			}
		}

	}



/* mingw-dirname */
	obj_t BGl_mingwzd2dirnamezd2zz__osz00(obj_t BgL_stringz00_20)
	{
		{	/* Llib/os.scm 521 */
			{	/* Llib/os.scm 522 */
				long BgL_nz00_1401;
				bool_t BgL_stopz00_1402;

				BgL_nz00_1401 = STRING_LENGTH(BgL_stringz00_20);
				BgL_stopz00_1402 = ((bool_t) 0);
				{
					long BgL_iz00_1405;

					BgL_iz00_1405 = (BgL_nz00_1401 - 1L);
				BgL_zc3z04anonymousza31373ze3z87_1406:
					if ((BBOOL(BgL_stopz00_1402) == BTRUE))
						{	/* Llib/os.scm 524 */
							if ((BgL_iz00_1405 < 0L))
								{	/* Llib/os.scm 526 */
									return BGl_string2279z00zz__osz00;
								}
							else
								{	/* Llib/os.scm 526 */
									return
										c_substring(BgL_stringz00_20, 0L, (BgL_iz00_1405 + 1L));
								}
						}
					else
						{	/* Llib/os.scm 524 */
							if ((BgL_iz00_1405 < 0L))
								{	/* Llib/os.scm 529 */
									BgL_stopz00_1402 = ((bool_t) 1);
								}
							else
								{	/* Llib/os.scm 531 */
									bool_t BgL__ortest_1053z00_1410;

									BgL__ortest_1053z00_1410 =
										(STRING_REF(BgL_stringz00_20,
											BgL_iz00_1405) == ((unsigned char) '\\'));
									if (BgL__ortest_1053z00_1410)
										{	/* Llib/os.scm 531 */
											BgL_stopz00_1402 = BgL__ortest_1053z00_1410;
										}
									else
										{	/* Llib/os.scm 531 */
											BgL_stopz00_1402 =
												(STRING_REF(BgL_stringz00_20,
													BgL_iz00_1405) == ((unsigned char) '/'));
								}}
							{
								long BgL_iz00_4314;

								BgL_iz00_4314 = (BgL_iz00_1405 - 1L);
								BgL_iz00_1405 = BgL_iz00_4314;
								goto BgL_zc3z04anonymousza31373ze3z87_1406;
							}
						}
				}
			}
		}

	}



/* default-dirname */
	obj_t BGl_defaultzd2dirnamezd2zz__osz00(obj_t BgL_stringz00_21)
	{
		{	/* Llib/os.scm 537 */
			{	/* Llib/os.scm 538 */
				long BgL_lenz00_1415;

				BgL_lenz00_1415 = (STRING_LENGTH(BgL_stringz00_21) - 1L);
				if ((BgL_lenz00_1415 == -1L))
					{	/* Llib/os.scm 539 */
						return BGl_string2279z00zz__osz00;
					}
				else
					{
						long BgL_readz00_1418;

						BgL_readz00_1418 = BgL_lenz00_1415;
					BgL_zc3z04anonymousza31381ze3z87_1419:
						if ((BgL_readz00_1418 == 0L))
							{	/* Llib/os.scm 543 */
								if (
									(STRING_REF(BgL_stringz00_21, BgL_readz00_1418) ==
										(unsigned char) (FILE_SEPARATOR)))
									{	/* Llib/os.scm 544 */
										return make_string(1L, (unsigned char) (FILE_SEPARATOR));
									}
								else
									{	/* Llib/os.scm 544 */
										return BGl_string2279z00zz__osz00;
									}
							}
						else
							{	/* Llib/os.scm 543 */
								if (
									(STRING_REF(BgL_stringz00_21, BgL_readz00_1418) ==
										(unsigned char) (FILE_SEPARATOR)))
									{	/* Llib/os.scm 547 */
										return c_substring(BgL_stringz00_21, 0L, BgL_readz00_1418);
									}
								else
									{
										long BgL_readz00_4334;

										BgL_readz00_4334 = (BgL_readz00_1418 - 1L);
										BgL_readz00_1418 = BgL_readz00_4334;
										goto BgL_zc3z04anonymousza31381ze3z87_1419;
									}
							}
					}
			}
		}

	}



/* suffix */
	BGL_EXPORTED_DEF obj_t BGl_suffixz00zz__osz00(obj_t BgL_stringz00_22)
	{
		{	/* Llib/os.scm 555 */
			{	/* Llib/os.scm 556 */
				long BgL_lenz00_1430;

				BgL_lenz00_1430 = STRING_LENGTH(BgL_stringz00_22);
				{	/* Llib/os.scm 556 */
					long BgL_lenzd21zd2_1431;

					BgL_lenzd21zd2_1431 = (BgL_lenz00_1430 - 1L);
					{	/* Llib/os.scm 557 */

						{
							long BgL_readz00_1433;

							BgL_readz00_1433 = BgL_lenzd21zd2_1431;
						BgL_zc3z04anonymousza31393ze3z87_1434:
							if ((BgL_readz00_1433 < 0L))
								{	/* Llib/os.scm 560 */
									return BGl_string2307z00zz__osz00;
								}
							else
								{	/* Llib/os.scm 560 */
									if (
										(STRING_REF(BgL_stringz00_22, BgL_readz00_1433) ==
											(unsigned char) (FILE_SEPARATOR)))
										{	/* Llib/os.scm 562 */
											return BGl_string2307z00zz__osz00;
										}
									else
										{	/* Llib/os.scm 562 */
											if (
												(STRING_REF(BgL_stringz00_22,
														BgL_readz00_1433) == ((unsigned char) '.')))
												{	/* Llib/os.scm 564 */
													if ((BgL_readz00_1433 == BgL_lenzd21zd2_1431))
														{	/* Llib/os.scm 566 */
															return BGl_string2307z00zz__osz00;
														}
													else
														{	/* Llib/os.scm 566 */
															return
																c_substring(BgL_stringz00_22,
																(BgL_readz00_1433 + 1L), BgL_lenz00_1430);
														}
												}
											else
												{
													long BgL_readz00_4351;

													BgL_readz00_4351 = (BgL_readz00_1433 - 1L);
													BgL_readz00_1433 = BgL_readz00_4351;
													goto BgL_zc3z04anonymousza31393ze3z87_1434;
												}
										}
								}
						}
					}
				}
			}
		}

	}



/* &suffix */
	obj_t BGl_z62suffixz62zz__osz00(obj_t BgL_envz00_3584,
		obj_t BgL_stringz00_3585)
	{
		{	/* Llib/os.scm 555 */
			{	/* Llib/os.scm 556 */
				obj_t BgL_auxz00_4353;

				if (STRINGP(BgL_stringz00_3585))
					{	/* Llib/os.scm 556 */
						BgL_auxz00_4353 = BgL_stringz00_3585;
					}
				else
					{
						obj_t BgL_auxz00_4356;

						BgL_auxz00_4356 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
							BINT(21884L), BGl_string2308z00zz__osz00,
							BGl_string2300z00zz__osz00, BgL_stringz00_3585);
						FAILURE(BgL_auxz00_4356, BFALSE, BFALSE);
					}
				return BGl_suffixz00zz__osz00(BgL_auxz00_4353);
			}
		}

	}



/* chmod */
	BGL_EXPORTED_DEF bool_t BGl_chmodz00zz__osz00(obj_t BgL_filez00_23,
		obj_t BgL_modez00_24)
	{
		{	/* Llib/os.scm 576 */
			{
				obj_t BgL_modez00_1447;
				bool_t BgL_readzf3zf3_1448;
				bool_t BgL_writezf3zf3_1449;
				bool_t BgL_execzf3zf3_1450;

				{	/* Llib/os.scm 577 */
					obj_t BgL_tmpz00_4361;

					BgL_modez00_1447 = BgL_modez00_24;
					BgL_readzf3zf3_1448 = ((bool_t) 0);
					BgL_writezf3zf3_1449 = ((bool_t) 0);
					BgL_execzf3zf3_1450 = ((bool_t) 0);
				BgL_zc3z04anonymousza31404ze3z87_1451:
					if (NULLP(BgL_modez00_1447))
						{	/* Llib/os.scm 582 */
							BgL_tmpz00_4361 =
								BBOOL(bgl_chmod(BSTRING_TO_STRING(BgL_filez00_23),
									BgL_readzf3zf3_1448, BgL_writezf3zf3_1449,
									BgL_execzf3zf3_1450));
						}
					else
						{	/* Llib/os.scm 582 */
							if (INTEGERP(CAR(((obj_t) BgL_modez00_1447))))
								{	/* Llib/os.scm 585 */
									obj_t BgL_arg1408z00_1455;

									BgL_arg1408z00_1455 = CAR(((obj_t) BgL_modez00_1447));
									{	/* Llib/os.scm 585 */
										bool_t BgL_tmpz00_4373;

										{	/* Llib/os.scm 585 */
											int BgL_auxz00_4376;
											char *BgL_tmpz00_4374;

											BgL_auxz00_4376 = CINT(BgL_arg1408z00_1455);
											BgL_tmpz00_4374 = BSTRING_TO_STRING(BgL_filez00_23);
											BgL_tmpz00_4373 = chmod(BgL_tmpz00_4374, BgL_auxz00_4376);
										}
										BgL_tmpz00_4361 = BBOOL(BgL_tmpz00_4373);
								}}
							else
								{	/* Llib/os.scm 584 */
									if (
										(CAR(
												((obj_t) BgL_modez00_1447)) ==
											BGl_symbol2309z00zz__osz00))
										{	/* Llib/os.scm 587 */
											obj_t BgL_arg1412z00_1458;

											BgL_arg1412z00_1458 = CDR(((obj_t) BgL_modez00_1447));
											{
												bool_t BgL_readzf3zf3_4387;
												obj_t BgL_modez00_4386;

												BgL_modez00_4386 = BgL_arg1412z00_1458;
												BgL_readzf3zf3_4387 = ((bool_t) 1);
												BgL_readzf3zf3_1448 = BgL_readzf3zf3_4387;
												BgL_modez00_1447 = BgL_modez00_4386;
												goto BgL_zc3z04anonymousza31404ze3z87_1451;
											}
										}
									else
										{	/* Llib/os.scm 586 */
											if (
												(CAR(
														((obj_t) BgL_modez00_1447)) ==
													BGl_symbol2311z00zz__osz00))
												{	/* Llib/os.scm 592 */
													obj_t BgL_arg1415z00_1461;

													BgL_arg1415z00_1461 = CDR(((obj_t) BgL_modez00_1447));
													{
														bool_t BgL_writezf3zf3_4395;
														obj_t BgL_modez00_4394;

														BgL_modez00_4394 = BgL_arg1415z00_1461;
														BgL_writezf3zf3_4395 = ((bool_t) 1);
														BgL_writezf3zf3_1449 = BgL_writezf3zf3_4395;
														BgL_modez00_1447 = BgL_modez00_4394;
														goto BgL_zc3z04anonymousza31404ze3z87_1451;
													}
												}
											else
												{	/* Llib/os.scm 591 */
													if (
														(CAR(
																((obj_t) BgL_modez00_1447)) ==
															BGl_symbol2313z00zz__osz00))
														{	/* Llib/os.scm 597 */
															obj_t BgL_arg1418z00_1464;

															BgL_arg1418z00_1464 =
																CDR(((obj_t) BgL_modez00_1447));
															{
																bool_t BgL_execzf3zf3_4403;
																obj_t BgL_modez00_4402;

																BgL_modez00_4402 = BgL_arg1418z00_1464;
																BgL_execzf3zf3_4403 = ((bool_t) 1);
																BgL_execzf3zf3_1450 = BgL_execzf3zf3_4403;
																BgL_modez00_1447 = BgL_modez00_4402;
																goto BgL_zc3z04anonymousza31404ze3z87_1451;
															}
														}
													else
														{	/* Llib/os.scm 596 */
															BgL_tmpz00_4361 =
																BGl_errorz00zz__errorz00
																(BGl_string2315z00zz__osz00,
																BGl_string2316z00zz__osz00, BgL_modez00_1447);
														}
												}
										}
								}
						}
					return CBOOL(BgL_tmpz00_4361);
				}
			}
		}

	}



/* &chmod */
	obj_t BGl_z62chmodz62zz__osz00(obj_t BgL_envz00_3586, obj_t BgL_filez00_3587,
		obj_t BgL_modez00_3588)
	{
		{	/* Llib/os.scm 576 */
			{	/* Llib/os.scm 582 */
				bool_t BgL_tmpz00_4406;

				{	/* Llib/os.scm 582 */
					obj_t BgL_auxz00_4407;

					if (STRINGP(BgL_filez00_3587))
						{	/* Llib/os.scm 582 */
							BgL_auxz00_4407 = BgL_filez00_3587;
						}
					else
						{
							obj_t BgL_auxz00_4410;

							BgL_auxz00_4410 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
								BINT(22735L), BGl_string2317z00zz__osz00,
								BGl_string2300z00zz__osz00, BgL_filez00_3587);
							FAILURE(BgL_auxz00_4410, BFALSE, BFALSE);
						}
					BgL_tmpz00_4406 =
						BGl_chmodz00zz__osz00(BgL_auxz00_4407, BgL_modez00_3588);
				}
				return BBOOL(BgL_tmpz00_4406);
			}
		}

	}



/* make-file-name */
	BGL_EXPORTED_DEF obj_t BGl_makezd2filezd2namez00zz__osz00(obj_t
		BgL_directoryz00_25, obj_t BgL_filez00_26)
	{
		{	/* Llib/os.scm 610 */
			{
				long BgL_ldirz00_1489;

				{	/* Llib/os.scm 618 */
					long BgL_ldirz00_1471;

					BgL_ldirz00_1471 = STRING_LENGTH(BgL_directoryz00_25);
					{	/* Llib/os.scm 620 */
						bool_t BgL_test2786z00_4417;

						if ((BgL_ldirz00_1471 == 1L))
							{	/* Llib/os.scm 620 */
								BgL_test2786z00_4417 =
									(STRING_REF(BgL_directoryz00_25,
										0L) == ((unsigned char) '.'));
							}
						else
							{	/* Llib/os.scm 620 */
								BgL_test2786z00_4417 = ((bool_t) 0);
							}
						if (BgL_test2786z00_4417)
							{	/* Llib/os.scm 620 */
								return BgL_filez00_26;
							}
						else
							{	/* Llib/os.scm 620 */
								if ((BgL_ldirz00_1471 == 0L))
									{	/* Llib/os.scm 623 */
										long BgL_lfilez00_1476;

										BgL_lfilez00_1476 = STRING_LENGTH(BgL_filez00_26);
										{	/* Llib/os.scm 623 */
											long BgL_lenz00_1477;

											BgL_lenz00_1477 = (1L + BgL_lfilez00_1476);
											{	/* Llib/os.scm 624 */
												obj_t BgL_strz00_1478;

												BgL_strz00_1478 =
													make_string(BgL_lenz00_1477,
													(unsigned char) (FILE_SEPARATOR));
												{	/* Llib/os.scm 625 */

													blit_string(BgL_filez00_26, 0L, BgL_strz00_1478, 1L,
														BgL_lfilez00_1476);
													return BgL_strz00_1478;
												}
											}
										}
									}
								else
									{	/* Llib/os.scm 622 */
										if (
											(STRING_REF(BgL_directoryz00_25,
													(BgL_ldirz00_1471 - 1L)) ==
												(unsigned char) (FILE_SEPARATOR)))
											{	/* Llib/os.scm 629 */
												long BgL_lfilez00_1482;

												BgL_lfilez00_1482 = STRING_LENGTH(BgL_filez00_26);
												{	/* Llib/os.scm 629 */
													long BgL_lenz00_1483;

													BgL_lenz00_1483 =
														(BgL_ldirz00_1471 + BgL_lfilez00_1482);
													{	/* Llib/os.scm 630 */
														obj_t BgL_strz00_1484;

														BgL_strz00_1484 =
															make_string(BgL_lenz00_1483,
															(unsigned char) (FILE_SEPARATOR));
														{	/* Llib/os.scm 631 */

															blit_string(BgL_directoryz00_25, 0L,
																BgL_strz00_1484, 0L, BgL_ldirz00_1471);
															blit_string(BgL_filez00_26, 0L, BgL_strz00_1484,
																BgL_ldirz00_1471, BgL_lfilez00_1482);
															return BgL_strz00_1484;
														}
													}
												}
											}
										else
											{	/* Llib/os.scm 628 */
												BgL_ldirz00_1489 = BgL_ldirz00_1471;
												{	/* Llib/os.scm 612 */
													long BgL_lfilez00_1491;

													BgL_lfilez00_1491 = STRING_LENGTH(BgL_filez00_26);
													{	/* Llib/os.scm 612 */
														long BgL_lenz00_1492;

														BgL_lenz00_1492 =
															(BgL_ldirz00_1489 + (BgL_lfilez00_1491 + 1L));
														{	/* Llib/os.scm 613 */
															obj_t BgL_strz00_1493;

															BgL_strz00_1493 =
																make_string(BgL_lenz00_1492,
																(unsigned char) (FILE_SEPARATOR));
															{	/* Llib/os.scm 614 */

																blit_string(BgL_directoryz00_25, 0L,
																	BgL_strz00_1493, 0L, BgL_ldirz00_1489);
																blit_string(BgL_filez00_26, 0L, BgL_strz00_1493,
																	(1L + BgL_ldirz00_1489), BgL_lfilez00_1491);
																return BgL_strz00_1493;
															}
														}
													}
												}
											}
									}
							}
					}
				}
			}
		}

	}



/* &make-file-name */
	obj_t BGl_z62makezd2filezd2namez62zz__osz00(obj_t BgL_envz00_3589,
		obj_t BgL_directoryz00_3590, obj_t BgL_filez00_3591)
	{
		{	/* Llib/os.scm 610 */
			{	/* Llib/os.scm 612 */
				obj_t BgL_auxz00_4455;
				obj_t BgL_auxz00_4448;

				if (STRINGP(BgL_filez00_3591))
					{	/* Llib/os.scm 612 */
						BgL_auxz00_4455 = BgL_filez00_3591;
					}
				else
					{
						obj_t BgL_auxz00_4458;

						BgL_auxz00_4458 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
							BINT(23650L), BGl_string2318z00zz__osz00,
							BGl_string2300z00zz__osz00, BgL_filez00_3591);
						FAILURE(BgL_auxz00_4458, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_directoryz00_3590))
					{	/* Llib/os.scm 612 */
						BgL_auxz00_4448 = BgL_directoryz00_3590;
					}
				else
					{
						obj_t BgL_auxz00_4451;

						BgL_auxz00_4451 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
							BINT(23650L), BGl_string2318z00zz__osz00,
							BGl_string2300z00zz__osz00, BgL_directoryz00_3590);
						FAILURE(BgL_auxz00_4451, BFALSE, BFALSE);
					}
				return
					BGl_makezd2filezd2namez00zz__osz00(BgL_auxz00_4448, BgL_auxz00_4455);
			}
		}

	}



/* make-file-path */
	BGL_EXPORTED_DEF obj_t BGl_makezd2filezd2pathz00zz__osz00(obj_t
		BgL_directoryz00_27, obj_t BgL_filez00_28, obj_t BgL_objz00_29)
	{
		{	/* Llib/os.scm 644 */
			{	/* Llib/os.scm 645 */
				bool_t BgL_test2792z00_4463;

				if ((STRING_LENGTH(BgL_directoryz00_27) == 0L))
					{	/* Llib/os.scm 645 */
						BgL_test2792z00_4463 = NULLP(BgL_objz00_29);
					}
				else
					{	/* Llib/os.scm 645 */
						BgL_test2792z00_4463 = ((bool_t) 0);
					}
				if (BgL_test2792z00_4463)
					{	/* Llib/os.scm 645 */
						return BgL_filez00_28;
					}
				else
					{	/* Llib/os.scm 647 */
						long BgL_ldirz00_1501;

						BgL_ldirz00_1501 = STRING_LENGTH(BgL_directoryz00_27);
						{	/* Llib/os.scm 647 */
							long BgL_lfilez00_1502;

							BgL_lfilez00_1502 = STRING_LENGTH(BgL_filez00_28);
							{	/* Llib/os.scm 648 */
								obj_t BgL_lenz00_1503;

								{	/* Llib/os.scm 649 */
									long BgL_g1054z00_1521;

									BgL_g1054z00_1521 =
										(BgL_ldirz00_1501 + (1L + BgL_lfilez00_1502));
									{
										obj_t BgL_objz00_1523;
										long BgL_lz00_1524;

										BgL_objz00_1523 = BgL_objz00_29;
										BgL_lz00_1524 = BgL_g1054z00_1521;
									BgL_zc3z04anonymousza31452ze3z87_1525:
										if (NULLP(BgL_objz00_1523))
											{	/* Llib/os.scm 652 */
												BgL_lenz00_1503 = BINT(BgL_lz00_1524);
											}
										else
											{	/* Llib/os.scm 654 */
												bool_t BgL_test2795z00_4475;

												{	/* Llib/os.scm 654 */
													obj_t BgL_tmpz00_4476;

													BgL_tmpz00_4476 = CAR(((obj_t) BgL_objz00_1523));
													BgL_test2795z00_4475 = STRINGP(BgL_tmpz00_4476);
												}
												if (BgL_test2795z00_4475)
													{	/* Llib/os.scm 658 */
														obj_t BgL_arg1456z00_1529;
														long BgL_arg1457z00_1530;

														BgL_arg1456z00_1529 =
															CDR(((obj_t) BgL_objz00_1523));
														{	/* Llib/os.scm 658 */
															long BgL_tmpz00_4482;

															{	/* Llib/os.scm 659 */
																long BgL_tmpz00_4483;

																{	/* Llib/os.scm 659 */
																	obj_t BgL_stringz00_2743;

																	BgL_stringz00_2743 =
																		CAR(((obj_t) BgL_objz00_1523));
																	BgL_tmpz00_4483 =
																		STRING_LENGTH(BgL_stringz00_2743);
																}
																BgL_tmpz00_4482 =
																	(BgL_tmpz00_4483 + BgL_lz00_1524);
															}
															BgL_arg1457z00_1530 = (1L + BgL_tmpz00_4482);
														}
														{
															long BgL_lz00_4490;
															obj_t BgL_objz00_4489;

															BgL_objz00_4489 = BgL_arg1456z00_1529;
															BgL_lz00_4490 = BgL_arg1457z00_1530;
															BgL_lz00_1524 = BgL_lz00_4490;
															BgL_objz00_1523 = BgL_objz00_4489;
															goto BgL_zc3z04anonymousza31452ze3z87_1525;
														}
													}
												else
													{	/* Llib/os.scm 656 */
														obj_t BgL_arg1461z00_1534;

														BgL_arg1461z00_1534 =
															CAR(((obj_t) BgL_objz00_1523));
														BgL_lenz00_1503 =
															BGl_bigloozd2typezd2errorz00zz__errorz00
															(BGl_string2319z00zz__osz00,
															BGl_string2320z00zz__osz00, BgL_arg1461z00_1534);
													}
											}
									}
								}
								{	/* Llib/os.scm 649 */
									obj_t BgL_strz00_1504;

									BgL_strz00_1504 =
										make_string(
										(long) CINT(BgL_lenz00_1503),
										(unsigned char) (FILE_SEPARATOR));
									{	/* Llib/os.scm 661 */

										blit_string(BgL_directoryz00_27, 0L, BgL_strz00_1504, 0L,
											BgL_ldirz00_1501);
										blit_string(BgL_filez00_28, 0L, BgL_strz00_1504,
											(1L + BgL_ldirz00_1501), BgL_lfilez00_1502);
										{	/* Llib/os.scm 664 */
											long BgL_g1055z00_1506;

											BgL_g1055z00_1506 =
												(1L + (BgL_ldirz00_1501 + BgL_lfilez00_1502));
											{
												obj_t BgL_objz00_1508;
												long BgL_wz00_1509;

												BgL_objz00_1508 = BgL_objz00_29;
												BgL_wz00_1509 = BgL_g1055z00_1506;
											BgL_zc3z04anonymousza31443ze3z87_1510:
												if (NULLP(BgL_objz00_1508))
													{	/* Llib/os.scm 666 */
														return BgL_strz00_1504;
													}
												else
													{	/* Llib/os.scm 668 */
														long BgL_loz00_1512;

														{	/* Llib/os.scm 668 */
															obj_t BgL_stringz00_2761;

															BgL_stringz00_2761 =
																CAR(((obj_t) BgL_objz00_1508));
															BgL_loz00_1512 =
																STRING_LENGTH(BgL_stringz00_2761);
														}
														{	/* Llib/os.scm 669 */
															obj_t BgL_arg1445z00_1513;
															long BgL_arg1446z00_1514;

															BgL_arg1445z00_1513 =
																CAR(((obj_t) BgL_objz00_1508));
															BgL_arg1446z00_1514 = (1L + BgL_wz00_1509);
															blit_string(
																((obj_t) BgL_arg1445z00_1513), 0L,
																BgL_strz00_1504, BgL_arg1446z00_1514,
																BgL_loz00_1512);
														}
														{	/* Llib/os.scm 670 */
															obj_t BgL_arg1447z00_1515;
															long BgL_arg1448z00_1516;

															BgL_arg1447z00_1515 =
																CDR(((obj_t) BgL_objz00_1508));
															BgL_arg1448z00_1516 =
																(BgL_wz00_1509 + (BgL_loz00_1512 + 1L));
															{
																long BgL_wz00_4517;
																obj_t BgL_objz00_4516;

																BgL_objz00_4516 = BgL_arg1447z00_1515;
																BgL_wz00_4517 = BgL_arg1448z00_1516;
																BgL_wz00_1509 = BgL_wz00_4517;
																BgL_objz00_1508 = BgL_objz00_4516;
																goto BgL_zc3z04anonymousza31443ze3z87_1510;
															}
														}
													}
											}
										}
									}
								}
							}
						}
					}
			}
		}

	}



/* &make-file-path */
	obj_t BGl_z62makezd2filezd2pathz62zz__osz00(obj_t BgL_envz00_3592,
		obj_t BgL_directoryz00_3593, obj_t BgL_filez00_3594, obj_t BgL_objz00_3595)
	{
		{	/* Llib/os.scm 644 */
			{	/* Llib/os.scm 645 */
				obj_t BgL_auxz00_4525;
				obj_t BgL_auxz00_4518;

				if (STRINGP(BgL_filez00_3594))
					{	/* Llib/os.scm 645 */
						BgL_auxz00_4525 = BgL_filez00_3594;
					}
				else
					{
						obj_t BgL_auxz00_4528;

						BgL_auxz00_4528 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
							BINT(25020L), BGl_string2321z00zz__osz00,
							BGl_string2300z00zz__osz00, BgL_filez00_3594);
						FAILURE(BgL_auxz00_4528, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_directoryz00_3593))
					{	/* Llib/os.scm 645 */
						BgL_auxz00_4518 = BgL_directoryz00_3593;
					}
				else
					{
						obj_t BgL_auxz00_4521;

						BgL_auxz00_4521 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
							BINT(25020L), BGl_string2321z00zz__osz00,
							BGl_string2300z00zz__osz00, BgL_directoryz00_3593);
						FAILURE(BgL_auxz00_4521, BFALSE, BFALSE);
					}
				return
					BGl_makezd2filezd2pathz00zz__osz00(BgL_auxz00_4518, BgL_auxz00_4525,
					BgL_objz00_3595);
			}
		}

	}



/* make-static-lib-name */
	BGL_EXPORTED_DEF obj_t BGl_makezd2staticzd2libzd2namezd2zz__osz00(obj_t
		BgL_libz00_30, obj_t BgL_backendz00_31)
	{
		{	/* Llib/os.scm 675 */
			if ((BgL_backendz00_31 == BGl_symbol2322z00zz__osz00))
				{	/* Llib/os.scm 678 */
					bool_t BgL_test2800z00_4535;

					{	/* Llib/os.scm 678 */
						obj_t BgL_string1z00_2773;

						BgL_string1z00_2773 = string_to_bstring(OS_CLASS);
						{	/* Llib/os.scm 678 */
							long BgL_l1z00_2775;

							BgL_l1z00_2775 = STRING_LENGTH(BgL_string1z00_2773);
							if ((BgL_l1z00_2775 == 5L))
								{	/* Llib/os.scm 678 */
									int BgL_arg1901z00_2778;

									{	/* Llib/os.scm 678 */
										char *BgL_auxz00_4542;
										char *BgL_tmpz00_4540;

										BgL_auxz00_4542 =
											BSTRING_TO_STRING(BGl_string2296z00zz__osz00);
										BgL_tmpz00_4540 = BSTRING_TO_STRING(BgL_string1z00_2773);
										BgL_arg1901z00_2778 =
											memcmp(BgL_tmpz00_4540, BgL_auxz00_4542, BgL_l1z00_2775);
									}
									BgL_test2800z00_4535 = ((long) (BgL_arg1901z00_2778) == 0L);
								}
							else
								{	/* Llib/os.scm 678 */
									BgL_test2800z00_4535 = ((bool_t) 0);
								}
						}
					}
					if (BgL_test2800z00_4535)
						{	/* Llib/os.scm 678 */
							return
								string_append_3(BgL_libz00_30, BGl_string2279z00zz__osz00,
								string_to_bstring(STATIC_LIB_SUFFIX));
						}
					else
						{	/* Llib/os.scm 680 */
							obj_t BgL_list1468z00_1544;

							{	/* Llib/os.scm 680 */
								obj_t BgL_arg1469z00_1545;

								{	/* Llib/os.scm 680 */
									obj_t BgL_arg1472z00_1546;

									{	/* Llib/os.scm 680 */
										obj_t BgL_arg1473z00_1547;

										BgL_arg1473z00_1547 =
											MAKE_YOUNG_PAIR(string_to_bstring(STATIC_LIB_SUFFIX),
											BNIL);
										BgL_arg1472z00_1546 =
											MAKE_YOUNG_PAIR(BGl_string2279z00zz__osz00,
											BgL_arg1473z00_1547);
									}
									BgL_arg1469z00_1545 =
										MAKE_YOUNG_PAIR(BgL_libz00_30, BgL_arg1472z00_1546);
								}
								BgL_list1468z00_1544 =
									MAKE_YOUNG_PAIR(BGl_string2324z00zz__osz00,
									BgL_arg1469z00_1545);
							}
							return
								BGl_stringzd2appendzd2zz__r4_strings_6_7z00
								(BgL_list1468z00_1544);
						}
				}
			else
				{	/* Llib/os.scm 676 */
					if ((BgL_backendz00_31 == BGl_symbol2325z00zz__osz00))
						{	/* Llib/os.scm 676 */
							return string_append(BgL_libz00_30, BGl_string2327z00zz__osz00);
						}
					else
						{	/* Llib/os.scm 676 */
							if ((BgL_backendz00_31 == BGl_symbol2328z00zz__osz00))
								{	/* Llib/os.scm 676 */
									return
										string_append(BgL_libz00_30, BGl_string2330z00zz__osz00);
								}
							else
								{	/* Llib/os.scm 676 */
									return
										BGl_errorz00zz__errorz00(BGl_string2331z00zz__osz00,
										BGl_string2332z00zz__osz00, BgL_backendz00_31);
								}
						}
				}
		}

	}



/* &make-static-lib-name */
	obj_t BGl_z62makezd2staticzd2libzd2namezb0zz__osz00(obj_t BgL_envz00_3596,
		obj_t BgL_libz00_3597, obj_t BgL_backendz00_3598)
	{
		{	/* Llib/os.scm 675 */
			{	/* Llib/os.scm 676 */
				obj_t BgL_auxz00_4569;
				obj_t BgL_auxz00_4562;

				if (SYMBOLP(BgL_backendz00_3598))
					{	/* Llib/os.scm 676 */
						BgL_auxz00_4569 = BgL_backendz00_3598;
					}
				else
					{
						obj_t BgL_auxz00_4572;

						BgL_auxz00_4572 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
							BINT(26136L), BGl_string2333z00zz__osz00,
							BGl_string2334z00zz__osz00, BgL_backendz00_3598);
						FAILURE(BgL_auxz00_4572, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_libz00_3597))
					{	/* Llib/os.scm 676 */
						BgL_auxz00_4562 = BgL_libz00_3597;
					}
				else
					{
						obj_t BgL_auxz00_4565;

						BgL_auxz00_4565 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
							BINT(26136L), BGl_string2333z00zz__osz00,
							BGl_string2300z00zz__osz00, BgL_libz00_3597);
						FAILURE(BgL_auxz00_4565, BFALSE, BFALSE);
					}
				return
					BGl_makezd2staticzd2libzd2namezd2zz__osz00(BgL_auxz00_4562,
					BgL_auxz00_4569);
			}
		}

	}



/* make-shared-lib-name */
	BGL_EXPORTED_DEF obj_t BGl_makezd2sharedzd2libzd2namezd2zz__osz00(obj_t
		BgL_libz00_32, obj_t BgL_backendz00_33)
	{
		{	/* Llib/os.scm 691 */
			if ((BgL_backendz00_33 == BGl_symbol2322z00zz__osz00))
				{	/* Llib/os.scm 694 */
					bool_t BgL_test2807z00_4579;

					{	/* Llib/os.scm 694 */
						obj_t BgL_string1z00_2787;

						BgL_string1z00_2787 = string_to_bstring(OS_CLASS);
						{	/* Llib/os.scm 694 */
							long BgL_l1z00_2789;

							BgL_l1z00_2789 = STRING_LENGTH(BgL_string1z00_2787);
							if ((BgL_l1z00_2789 == 5L))
								{	/* Llib/os.scm 694 */
									int BgL_arg1901z00_2792;

									{	/* Llib/os.scm 694 */
										char *BgL_auxz00_4586;
										char *BgL_tmpz00_4584;

										BgL_auxz00_4586 =
											BSTRING_TO_STRING(BGl_string2296z00zz__osz00);
										BgL_tmpz00_4584 = BSTRING_TO_STRING(BgL_string1z00_2787);
										BgL_arg1901z00_2792 =
											memcmp(BgL_tmpz00_4584, BgL_auxz00_4586, BgL_l1z00_2789);
									}
									BgL_test2807z00_4579 = ((long) (BgL_arg1901z00_2792) == 0L);
								}
							else
								{	/* Llib/os.scm 694 */
									BgL_test2807z00_4579 = ((bool_t) 0);
								}
						}
					}
					if (BgL_test2807z00_4579)
						{	/* Llib/os.scm 694 */
							return
								string_append_3(BgL_libz00_32, BGl_string2279z00zz__osz00,
								string_to_bstring(STATIC_LIB_SUFFIX));
						}
					else
						{	/* Llib/os.scm 696 */
							obj_t BgL_list1480z00_1555;

							{	/* Llib/os.scm 696 */
								obj_t BgL_arg1481z00_1556;

								{	/* Llib/os.scm 696 */
									obj_t BgL_arg1482z00_1557;

									{	/* Llib/os.scm 696 */
										obj_t BgL_arg1483z00_1558;

										BgL_arg1483z00_1558 =
											MAKE_YOUNG_PAIR(string_to_bstring(SHARED_LIB_SUFFIX),
											BNIL);
										BgL_arg1482z00_1557 =
											MAKE_YOUNG_PAIR(BGl_string2279z00zz__osz00,
											BgL_arg1483z00_1558);
									}
									BgL_arg1481z00_1556 =
										MAKE_YOUNG_PAIR(BgL_libz00_32, BgL_arg1482z00_1557);
								}
								BgL_list1480z00_1555 =
									MAKE_YOUNG_PAIR(BGl_string2324z00zz__osz00,
									BgL_arg1481z00_1556);
							}
							return
								BGl_stringzd2appendzd2zz__r4_strings_6_7z00
								(BgL_list1480z00_1555);
						}
				}
			else
				{	/* Llib/os.scm 692 */
					if ((BgL_backendz00_33 == BGl_symbol2325z00zz__osz00))
						{	/* Llib/os.scm 692 */
							return string_append(BgL_libz00_32, BGl_string2327z00zz__osz00);
						}
					else
						{	/* Llib/os.scm 692 */
							if ((BgL_backendz00_33 == BGl_symbol2328z00zz__osz00))
								{	/* Llib/os.scm 692 */
									return
										string_append(BgL_libz00_32, BGl_string2330z00zz__osz00);
								}
							else
								{	/* Llib/os.scm 692 */
									return
										BGl_errorz00zz__errorz00(BGl_string2335z00zz__osz00,
										BGl_string2332z00zz__osz00, BgL_backendz00_33);
								}
						}
				}
		}

	}



/* &make-shared-lib-name */
	obj_t BGl_z62makezd2sharedzd2libzd2namezb0zz__osz00(obj_t BgL_envz00_3599,
		obj_t BgL_libz00_3600, obj_t BgL_backendz00_3601)
	{
		{	/* Llib/os.scm 691 */
			{	/* Llib/os.scm 692 */
				obj_t BgL_auxz00_4613;
				obj_t BgL_auxz00_4606;

				if (SYMBOLP(BgL_backendz00_3601))
					{	/* Llib/os.scm 692 */
						BgL_auxz00_4613 = BgL_backendz00_3601;
					}
				else
					{
						obj_t BgL_auxz00_4616;

						BgL_auxz00_4616 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
							BINT(26792L), BGl_string2336z00zz__osz00,
							BGl_string2334z00zz__osz00, BgL_backendz00_3601);
						FAILURE(BgL_auxz00_4616, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_libz00_3600))
					{	/* Llib/os.scm 692 */
						BgL_auxz00_4606 = BgL_libz00_3600;
					}
				else
					{
						obj_t BgL_auxz00_4609;

						BgL_auxz00_4609 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
							BINT(26792L), BGl_string2336z00zz__osz00,
							BGl_string2300z00zz__osz00, BgL_libz00_3600);
						FAILURE(BgL_auxz00_4609, BFALSE, BFALSE);
					}
				return
					BGl_makezd2sharedzd2libzd2namezd2zz__osz00(BgL_auxz00_4606,
					BgL_auxz00_4613);
			}
		}

	}



/* find-file/path */
	BGL_EXPORTED_DEF obj_t BGl_findzd2filezf2pathz20zz__osz00(obj_t
		BgL_filezd2namezd2_34, obj_t BgL_pathz00_35)
	{
		{	/* Llib/os.scm 707 */
			{
				obj_t BgL_filezd2namezd2_1582;

				if ((STRING_LENGTH(BgL_filezd2namezd2_34) == 0L))
					{	/* Llib/os.scm 719 */
						return BFALSE;
					}
				else
					{	/* Llib/os.scm 721 */
						bool_t BgL_test2814z00_4624;

						if (
							(STRING_REF(BgL_filezd2namezd2_34, 0L) ==
								(unsigned char) (FILE_SEPARATOR)))
							{	/* Llib/os.scm 721 */
								BgL_test2814z00_4624 = ((bool_t) 1);
							}
						else
							{	/* Llib/os.scm 721 */
								BgL_filezd2namezd2_1582 = BgL_filezd2namezd2_34;
								{	/* Llib/os.scm 709 */
									bool_t BgL_test2816z00_4629;

									{	/* Llib/os.scm 709 */
										obj_t BgL_string1z00_2800;

										BgL_string1z00_2800 = string_to_bstring(OS_CLASS);
										{	/* Llib/os.scm 709 */
											long BgL_l1z00_2802;

											BgL_l1z00_2802 = STRING_LENGTH(BgL_string1z00_2800);
											if ((BgL_l1z00_2802 == 5L))
												{	/* Llib/os.scm 709 */
													int BgL_arg1901z00_2805;

													{	/* Llib/os.scm 709 */
														char *BgL_auxz00_4636;
														char *BgL_tmpz00_4634;

														BgL_auxz00_4636 =
															BSTRING_TO_STRING(BGl_string2303z00zz__osz00);
														BgL_tmpz00_4634 =
															BSTRING_TO_STRING(BgL_string1z00_2800);
														BgL_arg1901z00_2805 =
															memcmp(BgL_tmpz00_4634, BgL_auxz00_4636,
															BgL_l1z00_2802);
													}
													BgL_test2816z00_4629 =
														((long) (BgL_arg1901z00_2805) == 0L);
												}
											else
												{	/* Llib/os.scm 709 */
													BgL_test2816z00_4629 = ((bool_t) 0);
												}
										}
									}
									if (BgL_test2816z00_4629)
										{	/* Llib/os.scm 710 */
											bool_t BgL__ortest_1056z00_1586;

											BgL__ortest_1056z00_1586 =
												(STRING_REF(BgL_filezd2namezd2_1582,
													0L) == ((unsigned char) '/'));
											if (BgL__ortest_1056z00_1586)
												{	/* Llib/os.scm 710 */
													BgL_test2814z00_4624 = BgL__ortest_1056z00_1586;
												}
											else
												{	/* Llib/os.scm 711 */
													bool_t BgL__ortest_1057z00_1587;

													BgL__ortest_1057z00_1587 =
														(STRING_REF(BgL_filezd2namezd2_1582,
															0L) == ((unsigned char) '\\'));
													if (BgL__ortest_1057z00_1587)
														{	/* Llib/os.scm 711 */
															BgL_test2814z00_4624 = BgL__ortest_1057z00_1587;
														}
													else
														{	/* Llib/os.scm 711 */
															if ((STRING_LENGTH(BgL_filezd2namezd2_1582) > 2L))
																{	/* Llib/os.scm 712 */
																	if (
																		(STRING_REF(BgL_filezd2namezd2_1582,
																				1L) == ((unsigned char) ':')))
																		{	/* Llib/os.scm 714 */
																			bool_t BgL__ortest_1059z00_1591;

																			BgL__ortest_1059z00_1591 =
																				(STRING_REF(BgL_filezd2namezd2_1582,
																					2L) == ((unsigned char) '/'));
																			if (BgL__ortest_1059z00_1591)
																				{	/* Llib/os.scm 714 */
																					BgL_test2814z00_4624 =
																						BgL__ortest_1059z00_1591;
																				}
																			else
																				{	/* Llib/os.scm 714 */
																					BgL_test2814z00_4624 =
																						(STRING_REF(BgL_filezd2namezd2_1582,
																							2L) == ((unsigned char) '\\'));
																		}}
																	else
																		{	/* Llib/os.scm 713 */
																			BgL_test2814z00_4624 = ((bool_t) 0);
																		}
																}
															else
																{	/* Llib/os.scm 712 */
																	BgL_test2814z00_4624 = ((bool_t) 0);
																}
														}
												}
										}
									else
										{	/* Llib/os.scm 709 */
											BgL_test2814z00_4624 = ((bool_t) 0);
										}
								}
							}
						if (BgL_test2814z00_4624)
							{	/* Llib/os.scm 721 */
								if (fexists(BSTRING_TO_STRING(BgL_filezd2namezd2_34)))
									{	/* Llib/os.scm 723 */
										return BgL_filezd2namezd2_34;
									}
								else
									{	/* Llib/os.scm 723 */
										return BFALSE;
									}
							}
						else
							{
								obj_t BgL_pathz00_1571;

								BgL_pathz00_1571 = BgL_pathz00_35;
							BgL_zc3z04anonymousza31495ze3z87_1572:
								if (NULLP(BgL_pathz00_1571))
									{	/* Llib/os.scm 728 */
										return BFALSE;
									}
								else
									{	/* Llib/os.scm 730 */
										obj_t BgL_fnamez00_1574;

										{	/* Llib/os.scm 730 */
											obj_t BgL_arg1499z00_1577;

											BgL_arg1499z00_1577 = CAR(((obj_t) BgL_pathz00_1571));
											BgL_fnamez00_1574 =
												BGl_makezd2filezd2namez00zz__osz00(BgL_arg1499z00_1577,
												BgL_filezd2namezd2_34);
										}
										if (fexists(BSTRING_TO_STRING(BgL_fnamez00_1574)))
											{	/* Llib/os.scm 731 */
												return BgL_fnamez00_1574;
											}
										else
											{	/* Llib/os.scm 733 */
												obj_t BgL_arg1498z00_1576;

												BgL_arg1498z00_1576 = CDR(((obj_t) BgL_pathz00_1571));
												{
													obj_t BgL_pathz00_4671;

													BgL_pathz00_4671 = BgL_arg1498z00_1576;
													BgL_pathz00_1571 = BgL_pathz00_4671;
													goto BgL_zc3z04anonymousza31495ze3z87_1572;
												}
											}
									}
							}
					}
			}
		}

	}



/* &find-file/path */
	obj_t BGl_z62findzd2filezf2pathz42zz__osz00(obj_t BgL_envz00_3602,
		obj_t BgL_filezd2namezd2_3603, obj_t BgL_pathz00_3604)
	{
		{	/* Llib/os.scm 707 */
			{	/* Llib/os.scm 709 */
				obj_t BgL_auxz00_4672;

				if (STRINGP(BgL_filezd2namezd2_3603))
					{	/* Llib/os.scm 709 */
						BgL_auxz00_4672 = BgL_filezd2namezd2_3603;
					}
				else
					{
						obj_t BgL_auxz00_4675;

						BgL_auxz00_4675 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
							BINT(27498L), BGl_string2337z00zz__osz00,
							BGl_string2300z00zz__osz00, BgL_filezd2namezd2_3603);
						FAILURE(BgL_auxz00_4675, BFALSE, BFALSE);
					}
				return
					BGl_findzd2filezf2pathz20zz__osz00(BgL_auxz00_4672, BgL_pathz00_3604);
			}
		}

	}



/* file-name->list */
	BGL_EXPORTED_DEF obj_t BGl_filezd2namezd2ze3listze3zz__osz00(obj_t
		BgL_namez00_36)
	{
		{	/* Llib/os.scm 738 */
			{	/* Llib/os.scm 739 */
				long BgL_lenz00_1600;

				BgL_lenz00_1600 = STRING_LENGTH(BgL_namez00_36);
				{	/* Llib/os.scm 740 */
					bool_t BgL_test2827z00_4681;

					if ((BgL_lenz00_1600 == 1L))
						{	/* Llib/os.scm 740 */
							BgL_test2827z00_4681 =
								(STRING_REF(BgL_namez00_36, 0L) ==
								(unsigned char) (FILE_SEPARATOR));
						}
					else
						{	/* Llib/os.scm 740 */
							BgL_test2827z00_4681 = ((bool_t) 0);
						}
					if (BgL_test2827z00_4681)
						{	/* Llib/os.scm 741 */
							obj_t BgL_list1519z00_1605;

							BgL_list1519z00_1605 =
								MAKE_YOUNG_PAIR(BGl_string2307z00zz__osz00, BNIL);
							return BgL_list1519z00_1605;
						}
					else
						{
							long BgL_startz00_2859;
							long BgL_stopz00_2860;
							obj_t BgL_resz00_2861;

							BgL_startz00_2859 = 0L;
							BgL_stopz00_2860 = 0L;
							BgL_resz00_2861 = BNIL;
						BgL_loopz00_2858:
							if ((BgL_stopz00_2860 == BgL_lenz00_1600))
								{	/* Llib/os.scm 747 */
									obj_t BgL_arg1522z00_2865;

									BgL_arg1522z00_2865 =
										MAKE_YOUNG_PAIR(c_substring(BgL_namez00_36,
											BgL_startz00_2859, BgL_stopz00_2860), BgL_resz00_2861);
									return bgl_reverse_bang(BgL_arg1522z00_2865);
								}
							else
								{	/* Llib/os.scm 746 */
									if (
										(STRING_REF(BgL_namez00_36, BgL_stopz00_2860) ==
											(unsigned char) (FILE_SEPARATOR)))
										{	/* Llib/os.scm 749 */
											long BgL_arg1527z00_2877;
											long BgL_arg1528z00_2878;
											obj_t BgL_arg1529z00_2879;

											BgL_arg1527z00_2877 = (BgL_stopz00_2860 + 1L);
											BgL_arg1528z00_2878 = (BgL_stopz00_2860 + 1L);
											BgL_arg1529z00_2879 =
												MAKE_YOUNG_PAIR(c_substring(BgL_namez00_36,
													BgL_startz00_2859, BgL_stopz00_2860),
												BgL_resz00_2861);
											{
												obj_t BgL_resz00_4703;
												long BgL_stopz00_4702;
												long BgL_startz00_4701;

												BgL_startz00_4701 = BgL_arg1527z00_2877;
												BgL_stopz00_4702 = BgL_arg1528z00_2878;
												BgL_resz00_4703 = BgL_arg1529z00_2879;
												BgL_resz00_2861 = BgL_resz00_4703;
												BgL_stopz00_2860 = BgL_stopz00_4702;
												BgL_startz00_2859 = BgL_startz00_4701;
												goto BgL_loopz00_2858;
											}
										}
									else
										{
											long BgL_stopz00_4704;

											BgL_stopz00_4704 = (BgL_stopz00_2860 + 1L);
											BgL_stopz00_2860 = BgL_stopz00_4704;
											goto BgL_loopz00_2858;
										}
								}
						}
				}
			}
		}

	}



/* &file-name->list */
	obj_t BGl_z62filezd2namezd2ze3listz81zz__osz00(obj_t BgL_envz00_3605,
		obj_t BgL_namez00_3606)
	{
		{	/* Llib/os.scm 738 */
			{	/* Llib/os.scm 739 */
				obj_t BgL_auxz00_4706;

				if (STRINGP(BgL_namez00_3606))
					{	/* Llib/os.scm 739 */
						BgL_auxz00_4706 = BgL_namez00_3606;
					}
				else
					{
						obj_t BgL_auxz00_4709;

						BgL_auxz00_4709 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
							BINT(28537L), BGl_string2338z00zz__osz00,
							BGl_string2300z00zz__osz00, BgL_namez00_3606);
						FAILURE(BgL_auxz00_4709, BFALSE, BFALSE);
					}
				return BGl_filezd2namezd2ze3listze3zz__osz00(BgL_auxz00_4706);
			}
		}

	}



/* file-name-canonicalize-inner */
	obj_t BGl_filezd2namezd2canonicaliza7ezd2innerz75zz__osz00(obj_t
		BgL_srcz00_37, obj_t BgL_resz00_38, long BgL_indexz00_39)
	{
		{	/* Llib/os.scm 758 */
			{	/* Llib/os.scm 760 */
				long BgL_lenz00_1630;
				long BgL_iz00_1631;
				long BgL_jz00_1632;

				BgL_lenz00_1630 = STRING_LENGTH(BgL_srcz00_37);
				BgL_iz00_1631 = BgL_indexz00_39;
				BgL_jz00_1632 = BgL_indexz00_39;
				{

				BgL_zc3z04anonymousza31607ze3z87_1693:
					{	/* Llib/os.scm 874 */
						bool_t BgL_test2832z00_4715;

						if ((BgL_iz00_1631 < (BgL_lenz00_1630 - 3L)))
							{	/* Llib/os.scm 874 */
								if (
									(STRING_REF(BgL_srcz00_37,
											BgL_iz00_1631) == ((unsigned char) '.')))
									{	/* Llib/os.scm 875 */
										if (
											(STRING_REF(BgL_srcz00_37,
													(BgL_iz00_1631 + 1L)) == ((unsigned char) '.')))
											{	/* Llib/os.scm 876 */
												BgL_test2832z00_4715 =
													(STRING_REF(BgL_srcz00_37,
														(BgL_iz00_1631 + 2L)) ==
													(unsigned char) (FILE_SEPARATOR));
											}
										else
											{	/* Llib/os.scm 876 */
												BgL_test2832z00_4715 = ((bool_t) 0);
											}
									}
								else
									{	/* Llib/os.scm 875 */
										BgL_test2832z00_4715 = ((bool_t) 0);
									}
							}
						else
							{	/* Llib/os.scm 874 */
								BgL_test2832z00_4715 = ((bool_t) 0);
							}
						if (BgL_test2832z00_4715)
							{	/* Llib/os.scm 874 */
								STRING_SET(BgL_resz00_38, BgL_jz00_1632, ((unsigned char) '.'));
								{	/* Llib/os.scm 880 */
									long BgL_tmpz00_4731;

									BgL_tmpz00_4731 = (BgL_jz00_1632 + 1L);
									STRING_SET(BgL_resz00_38, BgL_tmpz00_4731,
										((unsigned char) '.'));
								}
								{	/* Llib/os.scm 881 */
									unsigned char BgL_auxz00_4736;
									long BgL_tmpz00_4734;

									BgL_auxz00_4736 = (unsigned char) (FILE_SEPARATOR);
									BgL_tmpz00_4734 = (BgL_jz00_1632 + 2L);
									STRING_SET(BgL_resz00_38, BgL_tmpz00_4734, BgL_auxz00_4736);
								}
								BgL_iz00_1631 = (BgL_iz00_1631 + 3L);
								BgL_jz00_1632 = (BgL_jz00_1632 + 3L);
								{

								BgL_zc3z04anonymousza31624ze3z87_1711:
									if ((BgL_iz00_1631 == BgL_lenz00_1630))
										{	/* Llib/os.scm 885 */
											return bgl_string_shrink(BgL_resz00_38, BgL_jz00_1632);
										}
									else
										{	/* Llib/os.scm 887 */
											bool_t BgL_test2837z00_4744;

											{	/* Llib/os.scm 887 */
												unsigned char BgL_char2z00_3072;

												BgL_char2z00_3072 = (unsigned char) (FILE_SEPARATOR);
												BgL_test2837z00_4744 =
													(STRING_REF(BgL_srcz00_37,
														BgL_iz00_1631) == BgL_char2z00_3072);
											}
											if (BgL_test2837z00_4744)
												{	/* Llib/os.scm 887 */
													BgL_iz00_1631 = (BgL_iz00_1631 + 1L);
													goto BgL_zc3z04anonymousza31624ze3z87_1711;
												}
											else
												{	/* Llib/os.scm 887 */
													goto BgL_zc3z04anonymousza31607ze3z87_1693;
												}
										}
								}
							}
						else
							{	/* Llib/os.scm 874 */
							BgL_zc3z04anonymousza31553ze3z87_1652:
								if ((BgL_iz00_1631 == BgL_lenz00_1630))
									{	/* Llib/os.scm 797 */
										return bgl_string_shrink(BgL_resz00_38, BgL_jz00_1632);
									}
								else
									{	/* Llib/os.scm 799 */
										unsigned char BgL_cz00_1654;

										BgL_cz00_1654 = STRING_REF(BgL_srcz00_37, BgL_iz00_1631);
										if ((BgL_cz00_1654 == (unsigned char) (FILE_SEPARATOR)))
											{	/* Llib/os.scm 801 */
												STRING_SET(BgL_resz00_38, BgL_jz00_1632, BgL_cz00_1654);
												BgL_iz00_1631 = (BgL_iz00_1631 + 1L);
												BgL_jz00_1632 = (BgL_jz00_1632 + 1L);
											BgL_zc3z04anonymousza31540ze3z87_1638:
												if ((BgL_iz00_1631 == BgL_lenz00_1630))
													{	/* Llib/os.scm 766 */
														return
															bgl_string_shrink(BgL_resz00_38, BgL_jz00_1632);
													}
												else
													{	/* Llib/os.scm 768 */
														bool_t BgL_test2841z00_4762;

														{	/* Llib/os.scm 768 */
															unsigned char BgL_char2z00_2896;

															BgL_char2z00_2896 =
																(unsigned char) (FILE_SEPARATOR);
															BgL_test2841z00_4762 =
																(STRING_REF(BgL_srcz00_37,
																	BgL_iz00_1631) == BgL_char2z00_2896);
														}
														if (BgL_test2841z00_4762)
															{	/* Llib/os.scm 768 */
																BgL_iz00_1631 = (BgL_iz00_1631 + 1L);
																goto BgL_zc3z04anonymousza31540ze3z87_1638;
															}
														else
															{	/* Llib/os.scm 768 */
																goto BgL_zc3z04anonymousza31553ze3z87_1652;
															}
													}
											}
										else
											{	/* Llib/os.scm 801 */
												if ((BgL_cz00_1654 == ((unsigned char) '.')))
													{	/* Llib/os.scm 807 */
														if ((BgL_iz00_1631 == (BgL_lenz00_1630 - 1L)))
															{	/* Llib/os.scm 809 */
																if ((BgL_jz00_1632 == 0L))
																	{	/* Llib/os.scm 811 */
																		STRING_SET(BgL_resz00_38, BgL_jz00_1632,
																			((unsigned char) '.'));
																		{	/* Llib/os.scm 814 */
																			long BgL_tmpz00_4775;

																			BgL_tmpz00_4775 = (BgL_jz00_1632 + 1L);
																			return
																				bgl_string_shrink(BgL_resz00_38,
																				BgL_tmpz00_4775);
																		}
																	}
																else
																	{	/* Llib/os.scm 815 */
																		long BgL_tmpz00_4778;

																		BgL_tmpz00_4778 = (BgL_jz00_1632 - 1L);
																		return
																			bgl_string_shrink(BgL_resz00_38,
																			BgL_tmpz00_4778);
																	}
															}
														else
															{	/* Llib/os.scm 809 */
																if (
																	(STRING_REF(BgL_srcz00_37,
																			(BgL_iz00_1631 + 1L)) ==
																		(unsigned char) (FILE_SEPARATOR)))
																	{	/* Llib/os.scm 816 */
																		if (
																			(BgL_iz00_1631 == (BgL_lenz00_1630 - 2L)))
																			{	/* Llib/os.scm 817 */
																				STRING_SET(BgL_resz00_38, BgL_jz00_1632,
																					((unsigned char) '.'));
																				{	/* Llib/os.scm 821 */
																					long BgL_tmpz00_4790;

																					BgL_tmpz00_4790 =
																						(BgL_jz00_1632 + 1L);
																					return
																						bgl_string_shrink(BgL_resz00_38,
																						BgL_tmpz00_4790);
																				}
																			}
																		else
																			{	/* Llib/os.scm 817 */
																				BgL_iz00_1631 = (BgL_iz00_1631 + 2L);
																				goto
																					BgL_zc3z04anonymousza31540ze3z87_1638;
																			}
																	}
																else
																	{	/* Llib/os.scm 816 */
																		if (
																			(STRING_REF(BgL_srcz00_37,
																					(BgL_iz00_1631 + 1L)) ==
																				((unsigned char) '.')))
																			{	/* Llib/os.scm 826 */
																				if (
																					(BgL_iz00_1631 ==
																						(BgL_lenz00_1630 - 2L)))
																					{	/* Llib/os.scm 829 */
																						if ((BgL_jz00_1632 == 0L))
																							{	/* Llib/os.scm 832 */
																								return
																									bgl_string_shrink
																									(BgL_resz00_38, 0L);
																							}
																						else
																							{	/* Llib/os.scm 832 */
																								if ((BgL_jz00_1632 == 1L))
																									{	/* Llib/os.scm 834 */
																										return
																											bgl_string_shrink
																											(BgL_resz00_38, 1L);
																									}
																								else
																									{	/* Llib/os.scm 834 */
																										{	/* Llib/os.scm 837 */
																											long BgL_jz00_2976;

																											BgL_jz00_2976 =
																												BgL_jz00_1632;
																											{	/* Llib/os.scm 791 */
																												obj_t BgL_njz00_2977;

																												{	/* Llib/os.scm 791 */
																													long
																														BgL_arg1552z00_2978;
																													BgL_arg1552z00_2978 =
																														(BgL_jz00_2976 -
																														1L);
																													BgL_njz00_2977 =
																														BGl_stringzd2indexzd2rightz00zz__r4_strings_6_7z00
																														(BgL_resz00_38,
																														BCHAR
																														(FILE_SEPARATOR),
																														BINT
																														(BgL_arg1552z00_2978));
																												}
																												if (INTEGERP
																													(BgL_njz00_2977))
																													{	/* Llib/os.scm 792 */
																														BgL_jz00_1632 =
																															(
																															(long)
																															CINT
																															(BgL_njz00_2977) +
																															1L);
																													}
																												else
																													{	/* Llib/os.scm 792 */
																														BgL_jz00_1632 = 0L;
																													}
																											}
																										}
																										if ((BgL_jz00_1632 > 1L))
																											{	/* Llib/os.scm 839 */
																												long BgL_tmpz00_4817;

																												BgL_tmpz00_4817 =
																													(BgL_jz00_1632 - 1L);
																												return
																													bgl_string_shrink
																													(BgL_resz00_38,
																													BgL_tmpz00_4817);
																											}
																										else
																											{	/* Llib/os.scm 838 */
																												return
																													bgl_string_shrink
																													(BgL_resz00_38,
																													BgL_jz00_1632);
																											}
																									}
																							}
																					}
																				else
																					{	/* Llib/os.scm 829 */
																						if (
																							(STRING_REF(BgL_srcz00_37,
																									(BgL_iz00_1631 + 2L)) ==
																								(unsigned
																									char) (FILE_SEPARATOR)))
																							{	/* Llib/os.scm 841 */
																								if ((BgL_jz00_1632 >= 2L))
																									{	/* Llib/os.scm 844 */
																										long BgL_jz00_2994;

																										BgL_jz00_2994 =
																											BgL_jz00_1632;
																										{	/* Llib/os.scm 791 */
																											obj_t BgL_njz00_2995;

																											{	/* Llib/os.scm 791 */
																												long
																													BgL_arg1552z00_2996;
																												BgL_arg1552z00_2996 =
																													(BgL_jz00_2994 - 1L);
																												BgL_njz00_2995 =
																													BGl_stringzd2indexzd2rightz00zz__r4_strings_6_7z00
																													(BgL_resz00_38,
																													BCHAR(FILE_SEPARATOR),
																													BINT
																													(BgL_arg1552z00_2996));
																											}
																											if (INTEGERP
																												(BgL_njz00_2995))
																												{	/* Llib/os.scm 792 */
																													BgL_jz00_1632 =
																														(
																														(long)
																														CINT(BgL_njz00_2995)
																														+ 1L);
																												}
																											else
																												{	/* Llib/os.scm 792 */
																													BgL_jz00_1632 = 0L;
																												}
																										}
																									}
																								else
																									{	/* Llib/os.scm 843 */
																										BFALSE;
																									}
																								BgL_iz00_1631 =
																									(BgL_iz00_1631 + 3L);
																								goto
																									BgL_zc3z04anonymousza31540ze3z87_1638;
																							}
																						else
																							{	/* Llib/os.scm 841 */
																								STRING_SET(BgL_resz00_38,
																									BgL_jz00_1632, BgL_cz00_1654);
																								BgL_iz00_1631 =
																									(BgL_iz00_1631 + 1L);
																								BgL_jz00_1632 =
																									(BgL_jz00_1632 + 1L);
																								{	/* Llib/os.scm 851 */
																									unsigned char
																										BgL_arg1589z00_1682;
																									BgL_arg1589z00_1682 =
																										STRING_REF(BgL_srcz00_37,
																										BgL_iz00_1631);
																									STRING_SET(BgL_resz00_38,
																										BgL_jz00_1632,
																										BgL_arg1589z00_1682);
																								}
																								BgL_iz00_1631 =
																									(BgL_iz00_1631 + 1L);
																								BgL_jz00_1632 =
																									(BgL_jz00_1632 + 1L);
																								{	/* Llib/os.scm 854 */
																									unsigned char
																										BgL_arg1591z00_1683;
																									BgL_arg1591z00_1683 =
																										STRING_REF(BgL_srcz00_37,
																										BgL_iz00_1631);
																									STRING_SET(BgL_resz00_38,
																										BgL_jz00_1632,
																										BgL_arg1591z00_1683);
																								}
																							BgL_zc3z04anonymousza31547ze3z87_1643:
																								if (
																									(BgL_iz00_1631 ==
																										BgL_lenz00_1630))
																									{	/* Llib/os.scm 775 */
																										return
																											bgl_string_shrink
																											(BgL_resz00_38,
																											BgL_jz00_1632);
																									}
																								else
																									{	/* Llib/os.scm 777 */
																										unsigned char BgL_cz00_1645;

																										BgL_cz00_1645 =
																											STRING_REF(BgL_srcz00_37,
																											BgL_iz00_1631);
																										if ((BgL_cz00_1645 ==
																												(unsigned
																													char)
																												(FILE_SEPARATOR)))
																											{	/* Llib/os.scm 779 */
																												STRING_SET
																													(BgL_resz00_38,
																													BgL_jz00_1632,
																													BgL_cz00_1645);
																												BgL_iz00_1631 =
																													(BgL_iz00_1631 + 1L);
																												BgL_jz00_1632 =
																													(BgL_jz00_1632 + 1L);
																												goto
																													BgL_zc3z04anonymousza31540ze3z87_1638;
																											}
																										else
																											{	/* Llib/os.scm 779 */
																												STRING_SET
																													(BgL_resz00_38,
																													BgL_jz00_1632,
																													BgL_cz00_1645);
																												BgL_iz00_1631 =
																													(BgL_iz00_1631 + 1L);
																												BgL_jz00_1632 =
																													(BgL_jz00_1632 + 1L);
																												goto
																													BgL_zc3z04anonymousza31547ze3z87_1643;
																											}
																									}
																							}
																					}
																			}
																		else
																			{	/* Llib/os.scm 826 */
																				STRING_SET(BgL_resz00_38, BgL_jz00_1632,
																					BgL_cz00_1654);
																				BgL_iz00_1631 = (BgL_iz00_1631 + 1L);
																				BgL_jz00_1632 = (BgL_jz00_1632 + 1L);
																				{	/* Llib/os.scm 861 */
																					unsigned char BgL_arg1598z00_1687;

																					BgL_arg1598z00_1687 =
																						STRING_REF(BgL_srcz00_37,
																						BgL_iz00_1631);
																					STRING_SET(BgL_resz00_38,
																						BgL_jz00_1632, BgL_arg1598z00_1687);
																				}
																				BgL_iz00_1631 = (BgL_iz00_1631 + 1L);
																				BgL_jz00_1632 = (BgL_jz00_1632 + 1L);
																				goto
																					BgL_zc3z04anonymousza31547ze3z87_1643;
																			}
																	}
															}
													}
												else
													{	/* Llib/os.scm 807 */
														STRING_SET(BgL_resz00_38, BgL_jz00_1632,
															BgL_cz00_1654);
														BgL_iz00_1631 = (BgL_iz00_1631 + 1L);
														BgL_jz00_1632 = (BgL_jz00_1632 + 1L);
														goto BgL_zc3z04anonymousza31547ze3z87_1643;
													}
											}
									}
							}
					}
				}
			}
		}

	}



/* file-name-canonicalize */
	BGL_EXPORTED_DEF obj_t BGl_filezd2namezd2canonicaliza7eza7zz__osz00(obj_t
		BgL_namez00_40)
	{
		{	/* Llib/os.scm 899 */
			{	/* Llib/os.scm 900 */
				obj_t BgL_arg1638z00_3074;

				{	/* Llib/os.scm 900 */
					long BgL_arg1639z00_3075;

					BgL_arg1639z00_3075 = STRING_LENGTH(BgL_namez00_40);
					{	/* Llib/os.scm 900 */

						BgL_arg1638z00_3074 =
							make_string(BgL_arg1639z00_3075, ((unsigned char) ' '));
				}}
				return
					BGl_filezd2namezd2canonicaliza7ezd2innerz75zz__osz00(BgL_namez00_40,
					BgL_arg1638z00_3074, 0L);
			}
		}

	}



/* &file-name-canonicalize */
	obj_t BGl_z62filezd2namezd2canonicaliza7ezc5zz__osz00(obj_t BgL_envz00_3607,
		obj_t BgL_namez00_3608)
	{
		{	/* Llib/os.scm 899 */
			{	/* Llib/os.scm 900 */
				obj_t BgL_auxz00_4872;

				if (STRINGP(BgL_namez00_3608))
					{	/* Llib/os.scm 900 */
						BgL_auxz00_4872 = BgL_namez00_3608;
					}
				else
					{
						obj_t BgL_auxz00_4875;

						BgL_auxz00_4875 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
							BINT(33025L), BGl_string2339z00zz__osz00,
							BGl_string2300z00zz__osz00, BgL_namez00_3608);
						FAILURE(BgL_auxz00_4875, BFALSE, BFALSE);
					}
				return BGl_filezd2namezd2canonicaliza7eza7zz__osz00(BgL_auxz00_4872);
			}
		}

	}



/* file-name-canonicalize! */
	BGL_EXPORTED_DEF obj_t BGl_filezd2namezd2canonicaliza7ez12zb5zz__osz00(obj_t
		BgL_namez00_41)
	{
		{	/* Llib/os.scm 905 */
			{	/* Llib/os.scm 906 */
				long BgL_lenz00_1735;

				BgL_lenz00_1735 = STRING_LENGTH(BgL_namez00_41);
				{
					long BgL_iz00_1738;
					long BgL_sz00_1739;

					BgL_iz00_1738 = 0L;
					BgL_sz00_1739 = 0L;
				BgL_zc3z04anonymousza31640ze3z87_1740:
					if ((BgL_iz00_1738 == BgL_lenz00_1735))
						{	/* Llib/os.scm 910 */
							return BgL_namez00_41;
						}
					else
						{	/* Llib/os.scm 912 */
							unsigned char BgL_cz00_1742;

							BgL_cz00_1742 = STRING_REF(BgL_namez00_41, BgL_iz00_1738);
							if ((BgL_cz00_1742 == (unsigned char) (FILE_SEPARATOR)))
								{	/* Llib/os.scm 914 */
									if ((BgL_sz00_1739 == (BgL_iz00_1738 - 1L)))
										{	/* Llib/os.scm 916 */
											obj_t BgL_resz00_1746;

											{	/* Ieee/string.scm 172 */

												BgL_resz00_1746 =
													make_string(BgL_lenz00_1735, ((unsigned char) ' '));
											}
											blit_string(BgL_namez00_41, 0L, BgL_resz00_1746, 0L,
												BgL_iz00_1738);
											return
												BGl_filezd2namezd2canonicaliza7ezd2innerz75zz__osz00
												(BgL_namez00_41, BgL_resz00_1746, BgL_sz00_1739);
										}
									else
										{
											long BgL_sz00_4895;
											long BgL_iz00_4893;

											BgL_iz00_4893 = (BgL_iz00_1738 + 1L);
											BgL_sz00_4895 = BgL_iz00_1738;
											BgL_sz00_1739 = BgL_sz00_4895;
											BgL_iz00_1738 = BgL_iz00_4893;
											goto BgL_zc3z04anonymousza31640ze3z87_1740;
										}
								}
							else
								{	/* Llib/os.scm 920 */
									bool_t BgL_test2862z00_4896;

									if ((BgL_cz00_1742 == ((unsigned char) '.')))
										{	/* Llib/os.scm 920 */
											BgL_test2862z00_4896 = (BgL_sz00_1739 >= 0L);
										}
									else
										{	/* Llib/os.scm 920 */
											BgL_test2862z00_4896 = ((bool_t) 0);
										}
									if (BgL_test2862z00_4896)
										{	/* Llib/os.scm 921 */
											obj_t BgL_resz00_1753;

											{	/* Ieee/string.scm 172 */

												BgL_resz00_1753 =
													make_string(BgL_lenz00_1735, ((unsigned char) ' '));
											}
											blit_string(BgL_namez00_41, 0L, BgL_resz00_1753, 0L,
												BgL_iz00_1738);
											return
												BGl_filezd2namezd2canonicaliza7ezd2innerz75zz__osz00
												(BgL_namez00_41, BgL_resz00_1753, BgL_sz00_1739);
										}
									else
										{
											long BgL_sz00_4905;
											long BgL_iz00_4903;

											BgL_iz00_4903 = (BgL_iz00_1738 + 1L);
											BgL_sz00_4905 = -1L;
											BgL_sz00_1739 = BgL_sz00_4905;
											BgL_iz00_1738 = BgL_iz00_4903;
											goto BgL_zc3z04anonymousza31640ze3z87_1740;
										}
								}
						}
				}
			}
		}

	}



/* &file-name-canonicalize! */
	obj_t BGl_z62filezd2namezd2canonicaliza7ez12zd7zz__osz00(obj_t
		BgL_envz00_3609, obj_t BgL_namez00_3610)
	{
		{	/* Llib/os.scm 905 */
			{	/* Llib/os.scm 906 */
				obj_t BgL_auxz00_4906;

				if (STRINGP(BgL_namez00_3610))
					{	/* Llib/os.scm 906 */
						BgL_auxz00_4906 = BgL_namez00_3610;
					}
				else
					{
						obj_t BgL_auxz00_4909;

						BgL_auxz00_4909 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
							BINT(33316L), BGl_string2340z00zz__osz00,
							BGl_string2300z00zz__osz00, BgL_namez00_3610);
						FAILURE(BgL_auxz00_4909, BFALSE, BFALSE);
					}
				return BGl_filezd2namezd2canonicaliza7ez12zb5zz__osz00(BgL_auxz00_4906);
			}
		}

	}



/* file-name-unix-canonicalize */
	BGL_EXPORTED_DEF obj_t
		BGl_filezd2namezd2unixzd2canonicaliza7ez75zz__osz00(obj_t BgL_namez00_42)
	{
		{	/* Llib/os.scm 930 */
			{	/* Llib/os.scm 931 */
				long BgL_lenz00_1759;

				BgL_lenz00_1759 = STRING_LENGTH(BgL_namez00_42);
				if ((BgL_lenz00_1759 == 0L))
					{	/* Llib/os.scm 933 */
						return BgL_namez00_42;
					}
				else
					{	/* Llib/os.scm 933 */
						if ((STRING_REF(BgL_namez00_42, 0L) == ((unsigned char) '~')))
							{	/* Llib/os.scm 935 */
								if ((BgL_lenz00_1759 == 1L))
									{	/* Llib/os.scm 937 */
										BGL_TAIL return
											BGl_filezd2namezd2canonicaliza7ez12zb5zz__osz00
											(BGl_getenvz00zz__osz00(BGl_string2297z00zz__osz00));
									}
								else
									{	/* Llib/os.scm 937 */
										if (
											(STRING_REF(BgL_namez00_42, 1L) ==
												(unsigned char) (FILE_SEPARATOR)))
											{	/* Llib/os.scm 940 */
												BGL_TAIL return
													BGl_filezd2namezd2canonicaliza7ez12zb5zz__osz00
													(string_append(BGl_getenvz00zz__osz00
														(BGl_string2297z00zz__osz00),
														c_substring(BgL_namez00_42, 1L, BgL_lenz00_1759)));
											}
										else
											{	/* Llib/os.scm 947 */
												obj_t BgL_arg1667z00_1771;

												{	/* Llib/os.scm 947 */
													obj_t BgL_arg1668z00_1772;
													obj_t BgL_arg1669z00_1773;

													BgL_arg1668z00_1772 =
														BGl_getenvz00zz__osz00(BGl_string2297z00zz__osz00);
													BgL_arg1669z00_1773 =
														c_substring(BgL_namez00_42, 1L, BgL_lenz00_1759);
													{	/* Llib/os.scm 947 */
														obj_t BgL_list1670z00_1774;

														BgL_list1670z00_1774 =
															MAKE_YOUNG_PAIR(BgL_arg1669z00_1773, BNIL);
														BgL_arg1667z00_1771 =
															BGl_makezd2filezd2pathz00zz__osz00
															(BgL_arg1668z00_1772, BGl_string2341z00zz__osz00,
															BgL_list1670z00_1774);
													}
												}
												BGL_TAIL return
													BGl_filezd2namezd2canonicaliza7ez12zb5zz__osz00
													(BgL_arg1667z00_1771);
											}
									}
							}
						else
							{	/* Llib/os.scm 900 */
								obj_t BgL_arg1638z00_3117;

								{	/* Llib/os.scm 900 */

									BgL_arg1638z00_3117 =
										make_string(BgL_lenz00_1759, ((unsigned char) ' '));
								}
								return
									BGl_filezd2namezd2canonicaliza7ezd2innerz75zz__osz00
									(BgL_namez00_42, BgL_arg1638z00_3117, 0L);
							}
					}
			}
		}

	}



/* &file-name-unix-canonicalize */
	obj_t BGl_z62filezd2namezd2unixzd2canonicaliza7ez17zz__osz00(obj_t
		BgL_envz00_3611, obj_t BgL_namez00_3612)
	{
		{	/* Llib/os.scm 930 */
			{	/* Llib/os.scm 931 */
				obj_t BgL_auxz00_4939;

				if (STRINGP(BgL_namez00_3612))
					{	/* Llib/os.scm 931 */
						BgL_auxz00_4939 = BgL_namez00_3612;
					}
				else
					{
						obj_t BgL_auxz00_4942;

						BgL_auxz00_4942 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
							BINT(34163L), BGl_string2342z00zz__osz00,
							BGl_string2300z00zz__osz00, BgL_namez00_3612);
						FAILURE(BgL_auxz00_4942, BFALSE, BFALSE);
					}
				return
					BGl_filezd2namezd2unixzd2canonicaliza7ez75zz__osz00(BgL_auxz00_4939);
			}
		}

	}



/* file-name-unix-canonicalize! */
	BGL_EXPORTED_DEF obj_t
		BGl_filezd2namezd2unixzd2canonicaliza7ez12z67zz__osz00(obj_t BgL_namez00_43)
	{
		{	/* Llib/os.scm 954 */
			{	/* Llib/os.scm 955 */
				long BgL_lenz00_1778;

				BgL_lenz00_1778 = STRING_LENGTH(BgL_namez00_43);
				if ((BgL_lenz00_1778 == 0L))
					{	/* Llib/os.scm 957 */
						return BgL_namez00_43;
					}
				else
					{	/* Llib/os.scm 957 */
						if ((STRING_REF(BgL_namez00_43, 0L) == ((unsigned char) '~')))
							{	/* Llib/os.scm 959 */
								BGL_TAIL return
									BGl_filezd2namezd2unixzd2canonicaliza7ez75zz__osz00
									(BgL_namez00_43);
							}
						else
							{	/* Llib/os.scm 959 */
								BGL_TAIL return
									BGl_filezd2namezd2canonicaliza7ez12zb5zz__osz00
									(BgL_namez00_43);
							}
					}
			}
		}

	}



/* &file-name-unix-canonicalize! */
	obj_t BGl_z62filezd2namezd2unixzd2canonicaliza7ez12z05zz__osz00(obj_t
		BgL_envz00_3613, obj_t BgL_namez00_3614)
	{
		{	/* Llib/os.scm 954 */
			{	/* Llib/os.scm 955 */
				obj_t BgL_auxz00_4955;

				if (STRINGP(BgL_namez00_3614))
					{	/* Llib/os.scm 955 */
						BgL_auxz00_4955 = BgL_namez00_3614;
					}
				else
					{
						obj_t BgL_auxz00_4958;

						BgL_auxz00_4958 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
							BINT(35039L), BGl_string2343z00zz__osz00,
							BGl_string2300z00zz__osz00, BgL_namez00_3614);
						FAILURE(BgL_auxz00_4958, BFALSE, BFALSE);
					}
				return
					BGl_filezd2namezd2unixzd2canonicaliza7ez12z67zz__osz00
					(BgL_auxz00_4955);
			}
		}

	}



/* relative-file-name */
	BGL_EXPORTED_DEF obj_t BGl_relativezd2filezd2namez00zz__osz00(obj_t
		BgL_namez00_44, obj_t BgL_basez00_45)
	{
		{	/* Llib/os.scm 967 */
			{
				obj_t BgL_fz00_1807;

				{	/* Llib/os.scm 976 */
					obj_t BgL_fz00_1784;

					BgL_fz00_1784 = BGl_filezd2namezd2ze3listze3zz__osz00(BgL_namez00_44);
					{	/* Llib/os.scm 977 */
						bool_t BgL_test2873z00_4964;

						{	/* Llib/os.scm 977 */
							obj_t BgL_arg1707z00_1806;

							BgL_arg1707z00_1806 = CAR(((obj_t) BgL_fz00_1784));
							{	/* Llib/os.scm 977 */
								long BgL_l1z00_3146;

								BgL_l1z00_3146 = STRING_LENGTH(((obj_t) BgL_arg1707z00_1806));
								if ((BgL_l1z00_3146 == 0L))
									{	/* Llib/os.scm 977 */
										int BgL_arg1901z00_3149;

										{	/* Llib/os.scm 977 */
											char *BgL_auxz00_4974;
											char *BgL_tmpz00_4971;

											BgL_auxz00_4974 =
												BSTRING_TO_STRING(BGl_string2307z00zz__osz00);
											BgL_tmpz00_4971 =
												BSTRING_TO_STRING(((obj_t) BgL_arg1707z00_1806));
											BgL_arg1901z00_3149 =
												memcmp(BgL_tmpz00_4971, BgL_auxz00_4974,
												BgL_l1z00_3146);
										}
										BgL_test2873z00_4964 = ((long) (BgL_arg1901z00_3149) == 0L);
									}
								else
									{	/* Llib/os.scm 977 */
										BgL_test2873z00_4964 = ((bool_t) 0);
									}
							}
						}
						if (BgL_test2873z00_4964)
							{	/* Llib/os.scm 979 */
								obj_t BgL_g1061z00_1787;

								BgL_g1061z00_1787 =
									BGl_filezd2namezd2ze3listze3zz__osz00(BgL_basez00_45);
								{
									obj_t BgL_fz00_1789;
									obj_t BgL_bz00_1790;

									BgL_fz00_1789 = BgL_fz00_1784;
									BgL_bz00_1790 = BgL_g1061z00_1787;
								BgL_zc3z04anonymousza31687ze3z87_1791:
									if (NULLP(BgL_fz00_1789))
										{	/* Llib/os.scm 982 */
											return BGl_string2307z00zz__osz00;
										}
									else
										{	/* Llib/os.scm 982 */
											if (NULLP(BgL_bz00_1790))
												{	/* Llib/os.scm 984 */
													BgL_fz00_1807 = BgL_fz00_1789;
												BgL_zc3z04anonymousza31708ze3z87_1808:
													if (NULLP(CDR(((obj_t) BgL_fz00_1807))))
														{	/* Llib/os.scm 970 */
															return CAR(((obj_t) BgL_fz00_1807));
														}
													else
														{	/* Llib/os.scm 972 */
															bool_t BgL_test2878z00_4990;

															{	/* Llib/os.scm 972 */
																obj_t BgL_tmpz00_4991;

																{	/* Llib/os.scm 972 */
																	obj_t BgL_pairz00_3133;

																	BgL_pairz00_3133 =
																		CDR(((obj_t) BgL_fz00_1807));
																	BgL_tmpz00_4991 = CDR(BgL_pairz00_3133);
																}
																BgL_test2878z00_4990 = NULLP(BgL_tmpz00_4991);
															}
															if (BgL_test2878z00_4990)
																{	/* Llib/os.scm 973 */
																	obj_t BgL_arg1714z00_1813;
																	obj_t BgL_arg1715z00_1814;

																	BgL_arg1714z00_1813 =
																		CAR(((obj_t) BgL_fz00_1807));
																	{	/* Llib/os.scm 973 */
																		obj_t BgL_pairz00_3138;

																		BgL_pairz00_3138 =
																			CDR(((obj_t) BgL_fz00_1807));
																		BgL_arg1715z00_1814 = CAR(BgL_pairz00_3138);
																	}
																	BGL_TAIL return
																		BGl_makezd2filezd2namez00zz__osz00
																		(BgL_arg1714z00_1813, BgL_arg1715z00_1814);
																}
															else
																{	/* Llib/os.scm 975 */
																	obj_t BgL_runner1718z00_1817;

																	BgL_runner1718z00_1817 = BgL_fz00_1807;
																	{	/* Llib/os.scm 975 */
																		obj_t BgL_aux1716z00_1815;

																		BgL_aux1716z00_1815 =
																			CAR(BgL_runner1718z00_1817);
																		BgL_runner1718z00_1817 =
																			CDR(BgL_runner1718z00_1817);
																		{	/* Llib/os.scm 975 */
																			obj_t BgL_aux1717z00_1816;

																			BgL_aux1717z00_1816 =
																				CAR(BgL_runner1718z00_1817);
																			BgL_runner1718z00_1817 =
																				CDR(BgL_runner1718z00_1817);
																			return
																				BGl_makezd2filezd2pathz00zz__osz00
																				(BgL_aux1716z00_1815,
																				BgL_aux1717z00_1816,
																				BgL_runner1718z00_1817);
																		}
																	}
																}
														}
												}
											else
												{	/* Llib/os.scm 986 */
													bool_t BgL_test2879z00_5007;

													{	/* Llib/os.scm 986 */
														obj_t BgL_arg1705z00_1803;
														obj_t BgL_arg1706z00_1804;

														BgL_arg1705z00_1803 = CAR(((obj_t) BgL_fz00_1789));
														BgL_arg1706z00_1804 = CAR(((obj_t) BgL_bz00_1790));
														{	/* Llib/os.scm 986 */
															long BgL_l1z00_3159;

															BgL_l1z00_3159 =
																STRING_LENGTH(((obj_t) BgL_arg1705z00_1803));
															if (
																(BgL_l1z00_3159 ==
																	STRING_LENGTH(((obj_t) BgL_arg1706z00_1804))))
																{	/* Llib/os.scm 986 */
																	int BgL_arg1901z00_3162;

																	{	/* Llib/os.scm 986 */
																		char *BgL_auxz00_5021;
																		char *BgL_tmpz00_5018;

																		BgL_auxz00_5021 =
																			BSTRING_TO_STRING(
																			((obj_t) BgL_arg1706z00_1804));
																		BgL_tmpz00_5018 =
																			BSTRING_TO_STRING(
																			((obj_t) BgL_arg1705z00_1803));
																		BgL_arg1901z00_3162 =
																			memcmp(BgL_tmpz00_5018, BgL_auxz00_5021,
																			BgL_l1z00_3159);
																	}
																	BgL_test2879z00_5007 =
																		((long) (BgL_arg1901z00_3162) == 0L);
																}
															else
																{	/* Llib/os.scm 986 */
																	BgL_test2879z00_5007 = ((bool_t) 0);
																}
														}
													}
													if (BgL_test2879z00_5007)
														{	/* Llib/os.scm 989 */
															obj_t BgL_arg1699z00_1797;
															obj_t BgL_arg1700z00_1798;

															BgL_arg1699z00_1797 =
																CDR(((obj_t) BgL_fz00_1789));
															BgL_arg1700z00_1798 =
																CDR(((obj_t) BgL_bz00_1790));
															{
																obj_t BgL_bz00_5032;
																obj_t BgL_fz00_5031;

																BgL_fz00_5031 = BgL_arg1699z00_1797;
																BgL_bz00_5032 = BgL_arg1700z00_1798;
																BgL_bz00_1790 = BgL_bz00_5032;
																BgL_fz00_1789 = BgL_fz00_5031;
																goto BgL_zc3z04anonymousza31687ze3z87_1791;
															}
														}
													else
														{	/* Llib/os.scm 987 */
															obj_t BgL_arg1701z00_1799;

															{	/* Llib/os.scm 987 */
																obj_t BgL_arg1702z00_1800;

																{	/* Llib/os.scm 987 */
																	long BgL_arg1703z00_1801;

																	BgL_arg1703z00_1801 =
																		bgl_list_length(BgL_bz00_1790);
																	{	/* Llib/os.scm 987 */
																		obj_t BgL_list1704z00_1802;

																		BgL_list1704z00_1802 =
																			MAKE_YOUNG_PAIR
																			(BGl_string2341z00zz__osz00, BNIL);
																		BgL_arg1702z00_1800 =
																			BGl_makezd2listzd2zz__r4_pairs_and_lists_6_3z00
																			((int) (BgL_arg1703z00_1801),
																			BgL_list1704z00_1802);
																}}
																BgL_arg1701z00_1799 =
																	BGl_appendzd221011zd2zz__osz00
																	(BgL_arg1702z00_1800, BgL_fz00_1789);
															}
															{
																obj_t BgL_fz00_5038;

																BgL_fz00_5038 = BgL_arg1701z00_1799;
																BgL_fz00_1807 = BgL_fz00_5038;
																goto BgL_zc3z04anonymousza31708ze3z87_1808;
															}
														}
												}
										}
								}
							}
						else
							{	/* Llib/os.scm 977 */
								return BgL_namez00_44;
							}
					}
				}
			}
		}

	}



/* &relative-file-name */
	obj_t BGl_z62relativezd2filezd2namez62zz__osz00(obj_t BgL_envz00_3615,
		obj_t BgL_namez00_3616, obj_t BgL_basez00_3617)
	{
		{	/* Llib/os.scm 967 */
			{	/* Llib/os.scm 970 */
				obj_t BgL_auxz00_5046;
				obj_t BgL_auxz00_5039;

				if (STRINGP(BgL_basez00_3617))
					{	/* Llib/os.scm 970 */
						BgL_auxz00_5046 = BgL_basez00_3617;
					}
				else
					{
						obj_t BgL_auxz00_5049;

						BgL_auxz00_5049 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
							BINT(35531L), BGl_string2344z00zz__osz00,
							BGl_string2300z00zz__osz00, BgL_basez00_3617);
						FAILURE(BgL_auxz00_5049, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_namez00_3616))
					{	/* Llib/os.scm 970 */
						BgL_auxz00_5039 = BgL_namez00_3616;
					}
				else
					{
						obj_t BgL_auxz00_5042;

						BgL_auxz00_5042 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
							BINT(35531L), BGl_string2344z00zz__osz00,
							BGl_string2300z00zz__osz00, BgL_namez00_3616);
						FAILURE(BgL_auxz00_5042, BFALSE, BFALSE);
					}
				return
					BGl_relativezd2filezd2namez00zz__osz00(BgL_auxz00_5039,
					BgL_auxz00_5046);
			}
		}

	}



/* make-static-library-name */
	BGL_EXPORTED_DEF obj_t BGl_makezd2staticzd2libraryzd2namezd2zz__osz00(obj_t
		BgL_libnamez00_46)
	{
		{	/* Llib/os.scm 996 */
			return
				string_append_3(BgL_libnamez00_46, BGl_string2279z00zz__osz00,
				string_to_bstring(STATIC_LIB_SUFFIX));
		}

	}



/* &make-static-library-name */
	obj_t BGl_z62makezd2staticzd2libraryzd2namezb0zz__osz00(obj_t BgL_envz00_3618,
		obj_t BgL_libnamez00_3619)
	{
		{	/* Llib/os.scm 996 */
			{	/* Llib/os.scm 997 */
				obj_t BgL_auxz00_5056;

				if (STRINGP(BgL_libnamez00_3619))
					{	/* Llib/os.scm 997 */
						BgL_auxz00_5056 = BgL_libnamez00_3619;
					}
				else
					{
						obj_t BgL_auxz00_5059;

						BgL_auxz00_5059 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
							BINT(36415L), BGl_string2345z00zz__osz00,
							BGl_string2300z00zz__osz00, BgL_libnamez00_3619);
						FAILURE(BgL_auxz00_5059, BFALSE, BFALSE);
					}
				return BGl_makezd2staticzd2libraryzd2namezd2zz__osz00(BgL_auxz00_5056);
			}
		}

	}



/* make-shared-library-name */
	BGL_EXPORTED_DEF obj_t BGl_makezd2sharedzd2libraryzd2namezd2zz__osz00(obj_t
		BgL_libnamez00_47)
	{
		{	/* Llib/os.scm 1004 */
			return
				string_append_3(BgL_libnamez00_47, BGl_string2279z00zz__osz00,
				string_to_bstring(SHARED_LIB_SUFFIX));
		}

	}



/* &make-shared-library-name */
	obj_t BGl_z62makezd2sharedzd2libraryzd2namezb0zz__osz00(obj_t BgL_envz00_3620,
		obj_t BgL_libnamez00_3621)
	{
		{	/* Llib/os.scm 1004 */
			{	/* Llib/os.scm 1005 */
				obj_t BgL_auxz00_5066;

				if (STRINGP(BgL_libnamez00_3621))
					{	/* Llib/os.scm 1005 */
						BgL_auxz00_5066 = BgL_libnamez00_3621;
					}
				else
					{
						obj_t BgL_auxz00_5069;

						BgL_auxz00_5069 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
							BINT(36900L), BGl_string2346z00zz__osz00,
							BGl_string2300z00zz__osz00, BgL_libnamez00_3621);
						FAILURE(BgL_auxz00_5069, BFALSE, BFALSE);
					}
				return BGl_makezd2sharedzd2libraryzd2namezd2zz__osz00(BgL_auxz00_5066);
			}
		}

	}



/* sleep */
	BGL_EXPORTED_DEF obj_t BGl_sleepz00zz__osz00(long BgL_msz00_48)
	{
		{	/* Llib/os.scm 1010 */
			bgl_sleep(BgL_msz00_48);
			BUNSPEC;
			return BINT(BgL_msz00_48);
		}

	}



/* &sleep */
	obj_t BGl_z62sleepz62zz__osz00(obj_t BgL_envz00_3622, obj_t BgL_msz00_3623)
	{
		{	/* Llib/os.scm 1010 */
			{	/* Llib/os.scm 1011 */
				long BgL_auxz00_5076;

				{	/* Llib/os.scm 1011 */
					obj_t BgL_tmpz00_5077;

					if (INTEGERP(BgL_msz00_3623))
						{	/* Llib/os.scm 1011 */
							BgL_tmpz00_5077 = BgL_msz00_3623;
						}
					else
						{
							obj_t BgL_auxz00_5080;

							BgL_auxz00_5080 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
								BINT(37222L), BGl_string2347z00zz__osz00,
								BGl_string2293z00zz__osz00, BgL_msz00_3623);
							FAILURE(BgL_auxz00_5080, BFALSE, BFALSE);
						}
					BgL_auxz00_5076 = (long) CINT(BgL_tmpz00_5077);
				}
				return BGl_sleepz00zz__osz00(BgL_auxz00_5076);
			}
		}

	}



/* _dynamic-load */
	obj_t BGl__dynamiczd2loadzd2zz__osz00(obj_t BgL_env1144z00_53,
		obj_t BgL_opt1143z00_52)
	{
		{	/* Llib/os.scm 1031 */
			{	/* Llib/os.scm 1031 */
				obj_t BgL_g1145z00_1821;

				BgL_g1145z00_1821 = VECTOR_REF(BgL_opt1143z00_52, 0L);
				switch (VECTOR_LENGTH(BgL_opt1143z00_52))
					{
					case 1L:

						{	/* Llib/os.scm 1031 */

							{	/* Llib/os.scm 1031 */
								obj_t BgL_auxz00_5087;

								if (STRINGP(BgL_g1145z00_1821))
									{	/* Llib/os.scm 1031 */
										BgL_auxz00_5087 = BgL_g1145z00_1821;
									}
								else
									{
										obj_t BgL_auxz00_5090;

										BgL_auxz00_5090 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2291z00zz__osz00, BINT(38281L),
											BGl_string2348z00zz__osz00, BGl_string2300z00zz__osz00,
											BgL_g1145z00_1821);
										FAILURE(BgL_auxz00_5090, BFALSE, BFALSE);
									}
								return
									BGl_dynamiczd2loadzd2zz__osz00(BgL_auxz00_5087,
									string_to_bstring(BGL_DYNAMIC_LOAD_INIT), BFALSE);
							}
						}
						break;
					case 2L:

						{	/* Llib/os.scm 1031 */
							obj_t BgL_initz00_1826;

							BgL_initz00_1826 = VECTOR_REF(BgL_opt1143z00_52, 1L);
							{	/* Llib/os.scm 1031 */

								{	/* Llib/os.scm 1031 */
									obj_t BgL_auxz00_5097;

									if (STRINGP(BgL_g1145z00_1821))
										{	/* Llib/os.scm 1031 */
											BgL_auxz00_5097 = BgL_g1145z00_1821;
										}
									else
										{
											obj_t BgL_auxz00_5100;

											BgL_auxz00_5100 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2291z00zz__osz00, BINT(38281L),
												BGl_string2348z00zz__osz00, BGl_string2300z00zz__osz00,
												BgL_g1145z00_1821);
											FAILURE(BgL_auxz00_5100, BFALSE, BFALSE);
										}
									return
										BGl_dynamiczd2loadzd2zz__osz00(BgL_auxz00_5097,
										BgL_initz00_1826, BFALSE);
								}
							}
						}
						break;
					case 3L:

						{	/* Llib/os.scm 1031 */
							obj_t BgL_initz00_1828;

							BgL_initz00_1828 = VECTOR_REF(BgL_opt1143z00_52, 1L);
							{	/* Llib/os.scm 1031 */
								obj_t BgL_modulez00_1829;

								BgL_modulez00_1829 = VECTOR_REF(BgL_opt1143z00_52, 2L);
								{	/* Llib/os.scm 1031 */

									{	/* Llib/os.scm 1031 */
										obj_t BgL_auxz00_5107;

										if (STRINGP(BgL_g1145z00_1821))
											{	/* Llib/os.scm 1031 */
												BgL_auxz00_5107 = BgL_g1145z00_1821;
											}
										else
											{
												obj_t BgL_auxz00_5110;

												BgL_auxz00_5110 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2291z00zz__osz00, BINT(38281L),
													BGl_string2348z00zz__osz00,
													BGl_string2300z00zz__osz00, BgL_g1145z00_1821);
												FAILURE(BgL_auxz00_5110, BFALSE, BFALSE);
											}
										return
											BGl_dynamiczd2loadzd2zz__osz00(BgL_auxz00_5107,
											BgL_initz00_1828, BgL_modulez00_1829);
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* dynamic-load */
	BGL_EXPORTED_DEF obj_t BGl_dynamiczd2loadzd2zz__osz00(obj_t BgL_libz00_49,
		obj_t BgL_initz00_50, obj_t BgL_modulez00_51)
	{
		{	/* Llib/os.scm 1031 */
			{	/* Llib/os.scm 1039 */
				obj_t BgL_flibz00_1832;
				obj_t BgL_modz00_1833;

				BgL_flibz00_1832 =
					BGl_findzd2filezf2pathz20zz__osz00(BgL_libz00_49,
					BGl_za2dynamiczd2loadzd2pathza2z00zz__osz00);
				if (CBOOL(BgL_modulez00_51))
					{	/* Llib/os.scm 1047 */
						obj_t BgL_arg1735z00_1854;

						BgL_arg1735z00_1854 = SYMBOL_TO_STRING(((obj_t) BgL_modulez00_51));
						BgL_modz00_1833 =
							bigloo_module_mangle(BGl_string2349z00zz__osz00,
							BgL_arg1735z00_1854);
					}
				else
					{	/* Llib/os.scm 1043 */
						BgL_modz00_1833 = BGl_string2307z00zz__osz00;
					}
				if (STRINGP(BgL_flibz00_1832))
					{	/* Llib/os.scm 1053 */
						obj_t BgL_iniz00_1835;

						if (CBOOL(BgL_initz00_50))
							{	/* Llib/os.scm 1053 */
								BgL_iniz00_1835 = BgL_initz00_50;
							}
						else
							{	/* Llib/os.scm 1053 */
								BgL_iniz00_1835 = BGl_string2307z00zz__osz00;
							}
						{	/* Llib/os.scm 1054 */
							obj_t BgL_valz00_1836;

							BgL_valz00_1836 =
								bgl_dload(BSTRING_TO_STRING(BgL_flibz00_1832),
								BSTRING_TO_STRING(BgL_iniz00_1835),
								BSTRING_TO_STRING(BgL_modz00_1833));
							{	/* Llib/os.scm 1055 */
								obj_t BgL_modz00_1837;

								{	/* Llib/os.scm 1056 */
									obj_t BgL_tmpz00_3172;

									{	/* Llib/os.scm 1056 */
										int BgL_tmpz00_5131;

										BgL_tmpz00_5131 = (int) (1L);
										BgL_tmpz00_3172 = BGL_MVALUES_VAL(BgL_tmpz00_5131);
									}
									{	/* Llib/os.scm 1056 */
										int BgL_tmpz00_5134;

										BgL_tmpz00_5134 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_5134, BUNSPEC);
									}
									BgL_modz00_1837 = BgL_tmpz00_3172;
								}
								if ((BgL_valz00_1836 == BGl_symbol2350z00zz__osz00))
									{	/* Llib/os.scm 1056 */
										return
											BGl_errorz00zz__errorz00(BGl_string2352z00zz__osz00,
											BGl_string2353z00zz__osz00, BgL_flibz00_1832);
									}
								else
									{	/* Llib/os.scm 1056 */
										if ((BgL_valz00_1836 == BGl_symbol2354z00zz__osz00))
											{	/* Llib/os.scm 1060 */
												char *BgL_arg1726z00_1841;

												BgL_arg1726z00_1841 = bgl_dload_error();
												{	/* Llib/os.scm 1034 */
													obj_t BgL_arg1737z00_3175;

													BgL_arg1737z00_3175 =
														string_append(BGl_string2352z00zz__osz00,
														BgL_flibz00_1832);
													return BGl_errorz00zz__errorz00(BgL_arg1737z00_3175,
														string_to_bstring(BgL_arg1726z00_1841),
														BgL_flibz00_1832);
												}
											}
										else
											{	/* Llib/os.scm 1056 */
												if ((BgL_valz00_1836 == BGl_symbol2356z00zz__osz00))
													{	/* Llib/os.scm 1063 */
														bool_t BgL_test2895z00_5148;

														if (BGl_equalzf3zf3zz__r4_equivalence_6_2z00
															(BgL_initz00_50,
																string_to_bstring(BGL_DYNAMIC_LOAD_INIT)))
															{	/* Llib/os.scm 1063 */
																if (CBOOL(BgL_modulez00_51))
																	{	/* Llib/os.scm 1063 */
																		BgL_test2895z00_5148 = ((bool_t) 0);
																	}
																else
																	{	/* Llib/os.scm 1063 */
																		BgL_test2895z00_5148 = ((bool_t) 1);
																	}
															}
														else
															{	/* Llib/os.scm 1063 */
																BgL_test2895z00_5148 = ((bool_t) 0);
															}
														if (BgL_test2895z00_5148)
															{	/* Llib/os.scm 1064 */
																obj_t BgL_arg1730z00_1845;

																BgL_arg1730z00_1845 =
																	string_append(BGl_string2358z00zz__osz00,
																	BgL_flibz00_1832);
																{	/* Llib/os.scm 1064 */
																	obj_t BgL_list1731z00_1846;

																	{	/* Llib/os.scm 1064 */
																		obj_t BgL_arg1733z00_1847;

																		{	/* Llib/os.scm 1064 */
																			obj_t BgL_arg1734z00_1848;

																			BgL_arg1734z00_1848 =
																				MAKE_YOUNG_PAIR(BgL_initz00_50, BNIL);
																			BgL_arg1733z00_1847 =
																				MAKE_YOUNG_PAIR
																				(BGl_string2359z00zz__osz00,
																				BgL_arg1734z00_1848);
																		}
																		BgL_list1731z00_1846 =
																			MAKE_YOUNG_PAIR(BgL_arg1730z00_1845,
																			BgL_arg1733z00_1847);
																	}
																	return
																		BGl_warningz00zz__errorz00
																		(BgL_list1731z00_1846);
																}
															}
														else
															{	/* Llib/os.scm 1063 */
																if (CBOOL(BgL_initz00_50))
																	{	/* Llib/os.scm 1067 */
																		return
																			BGl_errorz00zz__errorz00(string_append
																			(BGl_string2352z00zz__osz00,
																				BgL_flibz00_1832),
																			BGl_string2360z00zz__osz00,
																			BgL_initz00_50);
																	}
																else
																	{	/* Llib/os.scm 1067 */
																		{	/* Llib/os.scm 1068 */
																			int BgL_tmpz00_5163;

																			BgL_tmpz00_5163 = (int) (2L);
																			BGL_MVALUES_NUMBER_SET(BgL_tmpz00_5163);
																		}
																		{	/* Llib/os.scm 1068 */
																			int BgL_tmpz00_5166;

																			BgL_tmpz00_5166 = (int) (1L);
																			BGL_MVALUES_VAL_SET(BgL_tmpz00_5166,
																				BgL_modz00_1837);
																		}
																		return BUNSPEC;
																	}
															}
													}
												else
													{	/* Llib/os.scm 1056 */
														{	/* Llib/os.scm 1074 */
															int BgL_tmpz00_5169;

															BgL_tmpz00_5169 = (int) (2L);
															BGL_MVALUES_NUMBER_SET(BgL_tmpz00_5169);
														}
														{	/* Llib/os.scm 1074 */
															int BgL_tmpz00_5172;

															BgL_tmpz00_5172 = (int) (1L);
															BGL_MVALUES_VAL_SET(BgL_tmpz00_5172,
																BgL_modz00_1837);
														}
														return BgL_valz00_1836;
													}
											}
									}
							}
						}
					}
				else
					{	/* Llib/os.scm 1051 */
						return
							BGl_errorz00zz__errorz00(BGl_string2352z00zz__osz00,
							BGl_string2361z00zz__osz00, BgL_libz00_49);
					}
			}
		}

	}



/* dynamic-unload */
	BGL_EXPORTED_DEF obj_t BGl_dynamiczd2unloadzd2zz__osz00(obj_t BgL_libz00_54)
	{
		{	/* Llib/os.scm 1079 */
			{	/* Llib/os.scm 1080 */
				obj_t BgL_flibz00_1865;

				BgL_flibz00_1865 =
					BGl_findzd2filezf2pathz20zz__osz00(BgL_libz00_54,
					BGl_za2dynamiczd2loadzd2pathza2z00zz__osz00);
				if (STRINGP(BgL_flibz00_1865))
					{	/* Llib/os.scm 1089 */
						int BgL_arg1740z00_1867;

						BgL_arg1740z00_1867 = bgl_dunload(BgL_flibz00_1865);
						return BBOOL(((long) (BgL_arg1740z00_1867) == 0L));
					}
				else
					{	/* Llib/os.scm 1087 */
						return
							BGl_errorz00zz__errorz00(BGl_string2362z00zz__osz00,
							BGl_string2361z00zz__osz00, BgL_libz00_54);
					}
			}
		}

	}



/* &dynamic-unload */
	obj_t BGl_z62dynamiczd2unloadzb0zz__osz00(obj_t BgL_envz00_3624,
		obj_t BgL_libz00_3625)
	{
		{	/* Llib/os.scm 1079 */
			{	/* Llib/os.scm 1080 */
				obj_t BgL_auxz00_5184;

				if (STRINGP(BgL_libz00_3625))
					{	/* Llib/os.scm 1080 */
						BgL_auxz00_5184 = BgL_libz00_3625;
					}
				else
					{
						obj_t BgL_auxz00_5187;

						BgL_auxz00_5187 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
							BINT(39828L), BGl_string2363z00zz__osz00,
							BGl_string2300z00zz__osz00, BgL_libz00_3625);
						FAILURE(BgL_auxz00_5187, BFALSE, BFALSE);
					}
				return BGl_dynamiczd2unloadzd2zz__osz00(BgL_auxz00_5184);
			}
		}

	}



/* _dynamic-load-symbol */
	obj_t BGl__dynamiczd2loadzd2symbolz00zz__osz00(obj_t BgL_env1149z00_59,
		obj_t BgL_opt1148z00_58)
	{
		{	/* Llib/os.scm 1094 */
			{	/* Llib/os.scm 1094 */
				obj_t BgL_g1150z00_1868;
				obj_t BgL_g1151z00_1869;

				BgL_g1150z00_1868 = VECTOR_REF(BgL_opt1148z00_58, 0L);
				BgL_g1151z00_1869 = VECTOR_REF(BgL_opt1148z00_58, 1L);
				switch (VECTOR_LENGTH(BgL_opt1148z00_58))
					{
					case 2L:

						{	/* Llib/os.scm 1094 */

							{	/* Llib/os.scm 1094 */
								obj_t BgL_libz00_3179;
								obj_t BgL_namez00_3180;

								if (STRINGP(BgL_g1150z00_1868))
									{	/* Llib/os.scm 1094 */
										BgL_libz00_3179 = BgL_g1150z00_1868;
									}
								else
									{
										obj_t BgL_auxz00_5196;

										BgL_auxz00_5196 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2291z00zz__osz00, BINT(40337L),
											BGl_string2364z00zz__osz00, BGl_string2300z00zz__osz00,
											BgL_g1150z00_1868);
										FAILURE(BgL_auxz00_5196, BFALSE, BFALSE);
									}
								if (STRINGP(BgL_g1151z00_1869))
									{	/* Llib/os.scm 1094 */
										BgL_namez00_3180 = BgL_g1151z00_1869;
									}
								else
									{
										obj_t BgL_auxz00_5202;

										BgL_auxz00_5202 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2291z00zz__osz00, BINT(40337L),
											BGl_string2364z00zz__osz00, BGl_string2300z00zz__osz00,
											BgL_g1151z00_1869);
										FAILURE(BgL_auxz00_5202, BFALSE, BFALSE);
									}
								{	/* Llib/os.scm 1095 */
									obj_t BgL_flibz00_3182;

									BgL_flibz00_3182 =
										BGl_findzd2filezf2pathz20zz__osz00(BgL_libz00_3179,
										BGl_za2dynamiczd2loadzd2pathza2z00zz__osz00);
									{	/* Llib/os.scm 1103 */
										obj_t BgL_tmpz00_5207;

										if (STRINGP(BgL_flibz00_3182))
											{	/* Llib/os.scm 1103 */
												BgL_tmpz00_5207 = BgL_flibz00_3182;
											}
										else
											{
												obj_t BgL_auxz00_5210;

												BgL_auxz00_5210 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2291z00zz__osz00, BINT(40664L),
													BGl_string2364z00zz__osz00,
													BGl_string2300z00zz__osz00, BgL_flibz00_3182);
												FAILURE(BgL_auxz00_5210, BFALSE, BFALSE);
											}
										return
											bgl_dlsym(BgL_tmpz00_5207, BgL_namez00_3180,
											BgL_namez00_3180);
									}
								}
							}
						}
						break;
					case 3L:

						{	/* Llib/os.scm 1094 */
							obj_t BgL_modulez00_1873;

							BgL_modulez00_1873 = VECTOR_REF(BgL_opt1148z00_58, 2L);
							{	/* Llib/os.scm 1094 */

								{	/* Llib/os.scm 1094 */
									obj_t BgL_libz00_3184;
									obj_t BgL_namez00_3185;

									if (STRINGP(BgL_g1150z00_1868))
										{	/* Llib/os.scm 1094 */
											BgL_libz00_3184 = BgL_g1150z00_1868;
										}
									else
										{
											obj_t BgL_auxz00_5218;

											BgL_auxz00_5218 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2291z00zz__osz00, BINT(40337L),
												BGl_string2364z00zz__osz00, BGl_string2300z00zz__osz00,
												BgL_g1150z00_1868);
											FAILURE(BgL_auxz00_5218, BFALSE, BFALSE);
										}
									if (STRINGP(BgL_g1151z00_1869))
										{	/* Llib/os.scm 1094 */
											BgL_namez00_3185 = BgL_g1151z00_1869;
										}
									else
										{
											obj_t BgL_auxz00_5224;

											BgL_auxz00_5224 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2291z00zz__osz00, BINT(40337L),
												BGl_string2364z00zz__osz00, BGl_string2300z00zz__osz00,
												BgL_g1151z00_1869);
											FAILURE(BgL_auxz00_5224, BFALSE, BFALSE);
										}
									{	/* Llib/os.scm 1095 */
										obj_t BgL_symz00_3186;
										obj_t BgL_flibz00_3187;

										if (STRINGP(BgL_modulez00_1873))
											{	/* Llib/os.scm 1095 */
												BgL_symz00_3186 =
													bigloo_module_mangle(BgL_namez00_3185,
													BgL_modulez00_1873);
											}
										else
											{	/* Llib/os.scm 1095 */
												BgL_symz00_3186 = BgL_namez00_3185;
											}
										BgL_flibz00_3187 =
											BGl_findzd2filezf2pathz20zz__osz00(BgL_libz00_3184,
											BGl_za2dynamiczd2loadzd2pathza2z00zz__osz00);
										{	/* Llib/os.scm 1103 */
											obj_t BgL_tmpz00_5232;

											if (STRINGP(BgL_flibz00_3187))
												{	/* Llib/os.scm 1103 */
													BgL_tmpz00_5232 = BgL_flibz00_3187;
												}
											else
												{
													obj_t BgL_auxz00_5235;

													BgL_auxz00_5235 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2291z00zz__osz00, BINT(40664L),
														BGl_string2364z00zz__osz00,
														BGl_string2300z00zz__osz00, BgL_flibz00_3187);
													FAILURE(BgL_auxz00_5235, BFALSE, BFALSE);
												}
											return
												bgl_dlsym(BgL_tmpz00_5232, BgL_namez00_3185,
												BgL_symz00_3186);
										}
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* dynamic-load-symbol */
	BGL_EXPORTED_DEF obj_t BGl_dynamiczd2loadzd2symbolz00zz__osz00(obj_t
		BgL_libz00_55, obj_t BgL_namez00_56, obj_t BgL_modulez00_57)
	{
		{	/* Llib/os.scm 1094 */
			{	/* Llib/os.scm 1095 */
				obj_t BgL_symz00_3189;
				obj_t BgL_flibz00_3190;

				if (STRINGP(BgL_modulez00_57))
					{	/* Llib/os.scm 1095 */
						BgL_symz00_3189 =
							bigloo_module_mangle(BgL_namez00_56, BgL_modulez00_57);
					}
				else
					{	/* Llib/os.scm 1095 */
						BgL_symz00_3189 = BgL_namez00_56;
					}
				BgL_flibz00_3190 =
					BGl_findzd2filezf2pathz20zz__osz00(BgL_libz00_55,
					BGl_za2dynamiczd2loadzd2pathza2z00zz__osz00);
				return bgl_dlsym(BgL_flibz00_3190, BgL_namez00_56, BgL_symz00_3189);
			}
		}

	}



/* dynamic-load-symbol-get */
	BGL_EXPORTED_DEF obj_t BGl_dynamiczd2loadzd2symbolzd2getzd2zz__osz00(obj_t
		BgL_symz00_60)
	{
		{	/* Llib/os.scm 1108 */
			BGL_TAIL return bgl_dlsym_get(BgL_symz00_60);
		}

	}



/* &dynamic-load-symbol-get */
	obj_t BGl_z62dynamiczd2loadzd2symbolzd2getzb0zz__osz00(obj_t BgL_envz00_3626,
		obj_t BgL_symz00_3627)
	{
		{	/* Llib/os.scm 1108 */
			{	/* Llib/os.scm 1109 */
				obj_t BgL_auxz00_5248;

				if (CUSTOMP(BgL_symz00_3627))
					{	/* Llib/os.scm 1109 */
						BgL_auxz00_5248 = BgL_symz00_3627;
					}
				else
					{
						obj_t BgL_auxz00_5251;

						BgL_auxz00_5251 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
							BINT(40960L), BGl_string2365z00zz__osz00,
							BGl_string2366z00zz__osz00, BgL_symz00_3627);
						FAILURE(BgL_auxz00_5251, BFALSE, BFALSE);
					}
				return BGl_dynamiczd2loadzd2symbolzd2getzd2zz__osz00(BgL_auxz00_5248);
			}
		}

	}



/* dynamic-load-symbol-set */
	BGL_EXPORTED_DEF obj_t BGl_dynamiczd2loadzd2symbolzd2setzd2zz__osz00(obj_t
		BgL_symz00_61, obj_t BgL_valz00_62)
	{
		{	/* Llib/os.scm 1114 */
			BGL_TAIL return bgl_dlsym_set(BgL_symz00_61, BgL_valz00_62);
		}

	}



/* &dynamic-load-symbol-set */
	obj_t BGl_z62dynamiczd2loadzd2symbolzd2setzb0zz__osz00(obj_t BgL_envz00_3628,
		obj_t BgL_symz00_3629, obj_t BgL_valz00_3630)
	{
		{	/* Llib/os.scm 1114 */
			{	/* Llib/os.scm 1115 */
				obj_t BgL_auxz00_5257;

				if (CUSTOMP(BgL_symz00_3629))
					{	/* Llib/os.scm 1115 */
						BgL_auxz00_5257 = BgL_symz00_3629;
					}
				else
					{
						obj_t BgL_auxz00_5260;

						BgL_auxz00_5260 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
							BINT(41270L), BGl_string2367z00zz__osz00,
							BGl_string2366z00zz__osz00, BgL_symz00_3629);
						FAILURE(BgL_auxz00_5260, BFALSE, BFALSE);
					}
				return
					BGl_dynamiczd2loadzd2symbolzd2setzd2zz__osz00(BgL_auxz00_5257,
					BgL_valz00_3630);
			}
		}

	}



/* unix-path->list */
	BGL_EXPORTED_DEF obj_t BGl_unixzd2pathzd2ze3listze3zz__osz00(obj_t
		BgL_strz00_63)
	{
		{	/* Llib/os.scm 1120 */
			{	/* Llib/os.scm 1121 */
				long BgL_stopz00_1877;

				BgL_stopz00_1877 = STRING_LENGTH(BgL_strz00_63);
				{
					long BgL_markz00_1881;
					long BgL_rz00_1882;
					obj_t BgL_resz00_1883;

					BgL_markz00_1881 = 0L;
					BgL_rz00_1882 = 0L;
					BgL_resz00_1883 = BNIL;
				BgL_zc3z04anonymousza31742ze3z87_1884:
					if ((BgL_stopz00_1877 == BgL_rz00_1882))
						{	/* Llib/os.scm 1128 */
							obj_t BgL_resz00_1886;

							if ((BgL_markz00_1881 < BgL_rz00_1882))
								{	/* Llib/os.scm 1128 */
									BgL_resz00_1886 =
										MAKE_YOUNG_PAIR(c_substring(BgL_strz00_63, BgL_markz00_1881,
											BgL_rz00_1882), BgL_resz00_1883);
								}
							else
								{	/* Llib/os.scm 1128 */
									BgL_resz00_1886 = BgL_resz00_1883;
								}
							return bgl_reverse_bang(BgL_resz00_1886);
						}
					else
						{	/* Llib/os.scm 1127 */
							if (
								(STRING_REF(BgL_strz00_63, BgL_rz00_1882) ==
									(unsigned char) (PATH_SEPARATOR)))
								{	/* Llib/os.scm 1132 */
									if ((BgL_markz00_1881 < BgL_rz00_1882))
										{	/* Llib/os.scm 1134 */
											long BgL_arg1749z00_1892;
											long BgL_arg1750z00_1893;
											obj_t BgL_arg1751z00_1894;

											BgL_arg1749z00_1892 = (1L + BgL_rz00_1882);
											BgL_arg1750z00_1893 = (1L + BgL_rz00_1882);
											BgL_arg1751z00_1894 =
												MAKE_YOUNG_PAIR(c_substring(BgL_strz00_63,
													BgL_markz00_1881, BgL_rz00_1882), BgL_resz00_1883);
											{
												obj_t BgL_resz00_5285;
												long BgL_rz00_5284;
												long BgL_markz00_5283;

												BgL_markz00_5283 = BgL_arg1749z00_1892;
												BgL_rz00_5284 = BgL_arg1750z00_1893;
												BgL_resz00_5285 = BgL_arg1751z00_1894;
												BgL_resz00_1883 = BgL_resz00_5285;
												BgL_rz00_1882 = BgL_rz00_5284;
												BgL_markz00_1881 = BgL_markz00_5283;
												goto BgL_zc3z04anonymousza31742ze3z87_1884;
											}
										}
									else
										{
											long BgL_rz00_5288;
											long BgL_markz00_5286;

											BgL_markz00_5286 = (1L + BgL_rz00_1882);
											BgL_rz00_5288 = (1L + BgL_rz00_1882);
											BgL_rz00_1882 = BgL_rz00_5288;
											BgL_markz00_1881 = BgL_markz00_5286;
											goto BgL_zc3z04anonymousza31742ze3z87_1884;
										}
								}
							else
								{
									long BgL_rz00_5290;

									BgL_rz00_5290 = (1L + BgL_rz00_1882);
									BgL_rz00_1882 = BgL_rz00_5290;
									goto BgL_zc3z04anonymousza31742ze3z87_1884;
								}
						}
				}
			}
		}

	}



/* &unix-path->list */
	obj_t BGl_z62unixzd2pathzd2ze3listz81zz__osz00(obj_t BgL_envz00_3631,
		obj_t BgL_strz00_3632)
	{
		{	/* Llib/os.scm 1120 */
			{	/* Llib/os.scm 1121 */
				obj_t BgL_auxz00_5292;

				if (STRINGP(BgL_strz00_3632))
					{	/* Llib/os.scm 1121 */
						BgL_auxz00_5292 = BgL_strz00_3632;
					}
				else
					{
						obj_t BgL_auxz00_5295;

						BgL_auxz00_5295 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
							BINT(41552L), BGl_string2368z00zz__osz00,
							BGl_string2300z00zz__osz00, BgL_strz00_3632);
						FAILURE(BgL_auxz00_5295, BFALSE, BFALSE);
					}
				return BGl_unixzd2pathzd2ze3listze3zz__osz00(BgL_auxz00_5292);
			}
		}

	}



/* getuid */
	BGL_EXPORTED_DEF int BGl_getuidz00zz__osz00(void)
	{
		{	/* Llib/os.scm 1142 */
			BGL_TAIL return bgl_getuid();
		}

	}



/* &getuid */
	obj_t BGl_z62getuidz62zz__osz00(obj_t BgL_envz00_3633)
	{
		{	/* Llib/os.scm 1142 */
			return BINT(BGl_getuidz00zz__osz00());
		}

	}



/* setuid */
	BGL_EXPORTED_DEF obj_t BGl_setuidz00zz__osz00(int BgL_uidz00_64)
	{
		{	/* Llib/os.scm 1152 */
			return BINT(bgl_setuid(BgL_uidz00_64));
		}

	}



/* &setuid */
	obj_t BGl_z62setuidz62zz__osz00(obj_t BgL_envz00_3634, obj_t BgL_uidz00_3635)
	{
		{	/* Llib/os.scm 1152 */
			{	/* Llib/os.scm 1155 */
				int BgL_auxz00_5305;

				{	/* Llib/os.scm 1155 */
					obj_t BgL_tmpz00_5306;

					if (INTEGERP(BgL_uidz00_3635))
						{	/* Llib/os.scm 1155 */
							BgL_tmpz00_5306 = BgL_uidz00_3635;
						}
					else
						{
							obj_t BgL_auxz00_5309;

							BgL_auxz00_5309 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
								BINT(42607L), BGl_string2369z00zz__osz00,
								BGl_string2293z00zz__osz00, BgL_uidz00_3635);
							FAILURE(BgL_auxz00_5309, BFALSE, BFALSE);
						}
					BgL_auxz00_5305 = CINT(BgL_tmpz00_5306);
				}
				return BGl_setuidz00zz__osz00(BgL_auxz00_5305);
			}
		}

	}



/* getgid */
	BGL_EXPORTED_DEF int BGl_getgidz00zz__osz00(void)
	{
		{	/* Llib/os.scm 1162 */
			BGL_TAIL return bgl_getgid();
		}

	}



/* &getgid */
	obj_t BGl_z62getgidz62zz__osz00(obj_t BgL_envz00_3636)
	{
		{	/* Llib/os.scm 1162 */
			return BINT(BGl_getgidz00zz__osz00());
		}

	}



/* setgid */
	BGL_EXPORTED_DEF obj_t BGl_setgidz00zz__osz00(int BgL_uidz00_65)
	{
		{	/* Llib/os.scm 1172 */
			return BINT(bgl_setgid(BgL_uidz00_65));
		}

	}



/* &setgid */
	obj_t BGl_z62setgidz62zz__osz00(obj_t BgL_envz00_3637, obj_t BgL_uidz00_3638)
	{
		{	/* Llib/os.scm 1172 */
			{	/* Llib/os.scm 1175 */
				int BgL_auxz00_5320;

				{	/* Llib/os.scm 1175 */
					obj_t BgL_tmpz00_5321;

					if (INTEGERP(BgL_uidz00_3638))
						{	/* Llib/os.scm 1175 */
							BgL_tmpz00_5321 = BgL_uidz00_3638;
						}
					else
						{
							obj_t BgL_auxz00_5324;

							BgL_auxz00_5324 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
								BINT(43288L), BGl_string2370z00zz__osz00,
								BGl_string2293z00zz__osz00, BgL_uidz00_3638);
							FAILURE(BgL_auxz00_5324, BFALSE, BFALSE);
						}
					BgL_auxz00_5320 = CINT(BgL_tmpz00_5321);
				}
				return BGl_setgidz00zz__osz00(BgL_auxz00_5320);
			}
		}

	}



/* getpwnam */
	BGL_EXPORTED_DEF obj_t BGl_getpwnamz00zz__osz00(obj_t BgL_namez00_66)
	{
		{	/* Llib/os.scm 1182 */
			return bgl_getpwnam(BSTRING_TO_STRING(BgL_namez00_66));
		}

	}



/* &getpwnam */
	obj_t BGl_z62getpwnamz62zz__osz00(obj_t BgL_envz00_3639,
		obj_t BgL_namez00_3640)
	{
		{	/* Llib/os.scm 1182 */
			{	/* Llib/os.scm 1185 */
				obj_t BgL_auxz00_5332;

				if (STRINGP(BgL_namez00_3640))
					{	/* Llib/os.scm 1185 */
						BgL_auxz00_5332 = BgL_namez00_3640;
					}
				else
					{
						obj_t BgL_auxz00_5335;

						BgL_auxz00_5335 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
							BINT(43658L), BGl_string2371z00zz__osz00,
							BGl_string2300z00zz__osz00, BgL_namez00_3640);
						FAILURE(BgL_auxz00_5335, BFALSE, BFALSE);
					}
				return BGl_getpwnamz00zz__osz00(BgL_auxz00_5332);
			}
		}

	}



/* getpwuid */
	BGL_EXPORTED_DEF obj_t BGl_getpwuidz00zz__osz00(int BgL_uidz00_67)
	{
		{	/* Llib/os.scm 1192 */
			BGL_TAIL return bgl_getpwuid(BgL_uidz00_67);
		}

	}



/* &getpwuid */
	obj_t BGl_z62getpwuidz62zz__osz00(obj_t BgL_envz00_3641,
		obj_t BgL_uidz00_3642)
	{
		{	/* Llib/os.scm 1192 */
			{	/* Llib/os.scm 1195 */
				int BgL_auxz00_5341;

				{	/* Llib/os.scm 1195 */
					obj_t BgL_tmpz00_5342;

					if (INTEGERP(BgL_uidz00_3642))
						{	/* Llib/os.scm 1195 */
							BgL_tmpz00_5342 = BgL_uidz00_3642;
						}
					else
						{
							obj_t BgL_auxz00_5345;

							BgL_auxz00_5345 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
								BINT(43986L), BGl_string2372z00zz__osz00,
								BGl_string2293z00zz__osz00, BgL_uidz00_3642);
							FAILURE(BgL_auxz00_5345, BFALSE, BFALSE);
						}
					BgL_auxz00_5341 = CINT(BgL_tmpz00_5342);
				}
				return BGl_getpwuidz00zz__osz00(BgL_auxz00_5341);
			}
		}

	}



/* getpid */
	BGL_EXPORTED_DEF int BGl_getpidz00zz__osz00(void)
	{
		{	/* Llib/os.scm 1201 */
			return getpid();
		}

	}



/* &getpid */
	obj_t BGl_z62getpidz62zz__osz00(obj_t BgL_envz00_3643)
	{
		{	/* Llib/os.scm 1201 */
			return BINT(BGl_getpidz00zz__osz00());
		}

	}



/* getppid */
	BGL_EXPORTED_DEF int BGl_getppidz00zz__osz00(void)
	{
		{	/* Llib/os.scm 1209 */
			return getppid();
		}

	}



/* &getppid */
	obj_t BGl_z62getppidz62zz__osz00(obj_t BgL_envz00_3644)
	{
		{	/* Llib/os.scm 1209 */
			return BINT(BGl_getppidz00zz__osz00());
		}

	}



/* getgroups */
	BGL_EXPORTED_DEF obj_t BGl_getgroupsz00zz__osz00(void)
	{
		{	/* Llib/os.scm 1217 */
			BGL_TAIL return bgl_getgroups();
		}

	}



/* &getgroups */
	obj_t BGl_z62getgroupsz62zz__osz00(obj_t BgL_envz00_3645)
	{
		{	/* Llib/os.scm 1217 */
			return BGl_getgroupsz00zz__osz00();
		}

	}



/* ioctl-register-request! */
	BGL_EXPORTED_DEF obj_t BGl_ioctlzd2registerzd2requestz12z12zz__osz00(obj_t
		BgL_namez00_68, uint64_t BgL_valz00_69)
	{
		{	/* Llib/os.scm 1231 */
			{	/* Llib/os.scm 1232 */
				obj_t BgL_arg1757z00_3214;

				BgL_arg1757z00_3214 =
					MAKE_YOUNG_PAIR(BgL_namez00_68, BGL_UINT64_TO_BUINT64(BgL_valz00_69));
				return (BGl_ioctlzd2requestszd2tablez00zz__osz00 =
					MAKE_YOUNG_PAIR(BgL_arg1757z00_3214,
						BGl_ioctlzd2requestszd2tablez00zz__osz00), BUNSPEC);
			}
		}

	}



/* &ioctl-register-request! */
	obj_t BGl_z62ioctlzd2registerzd2requestz12z70zz__osz00(obj_t BgL_envz00_3646,
		obj_t BgL_namez00_3647, obj_t BgL_valz00_3648)
	{
		{	/* Llib/os.scm 1231 */
			{	/* Llib/os.scm 1232 */
				uint64_t BgL_auxz00_5369;
				obj_t BgL_auxz00_5362;

				{	/* Llib/os.scm 1232 */
					obj_t BgL_tmpz00_5370;

					if (BGL_UINT64P(BgL_valz00_3648))
						{	/* Llib/os.scm 1232 */
							BgL_tmpz00_5370 = BgL_valz00_3648;
						}
					else
						{
							obj_t BgL_auxz00_5373;

							BgL_auxz00_5373 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
								BINT(45505L), BGl_string2373z00zz__osz00,
								BGl_string2374z00zz__osz00, BgL_valz00_3648);
							FAILURE(BgL_auxz00_5373, BFALSE, BFALSE);
						}
					BgL_auxz00_5369 = BGL_BINT64_TO_INT64(BgL_tmpz00_5370);
				}
				if (STRINGP(BgL_namez00_3647))
					{	/* Llib/os.scm 1232 */
						BgL_auxz00_5362 = BgL_namez00_3647;
					}
				else
					{
						obj_t BgL_auxz00_5365;

						BgL_auxz00_5365 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
							BINT(45505L), BGl_string2373z00zz__osz00,
							BGl_string2300z00zz__osz00, BgL_namez00_3647);
						FAILURE(BgL_auxz00_5365, BFALSE, BFALSE);
					}
				return
					BGl_ioctlzd2registerzd2requestz12z12zz__osz00(BgL_auxz00_5362,
					BgL_auxz00_5369);
			}
		}

	}



/* request->elong */
	long BGl_requestzd2ze3elongz31zz__osz00(obj_t BgL_reqz00_70)
	{
		{	/* Llib/os.scm 1237 */
			{
				obj_t BgL_objz00_1903;

				{	/* Llib/os.scm 1238 */
					obj_t BgL_tmpz00_5379;

					BgL_objz00_1903 = BgL_reqz00_70;
				BgL_zc3z04anonymousza31758ze3z87_1904:
					if (ELONGP(BgL_objz00_1903))
						{	/* Llib/os.scm 1240 */
							BgL_tmpz00_5379 = BgL_objz00_1903;
						}
					else
						{	/* Llib/os.scm 1240 */
							if (INTEGERP(BgL_objz00_1903))
								{	/* Llib/os.scm 1243 */
									long BgL_xz00_3215;

									BgL_xz00_3215 = (long) CINT(BgL_objz00_1903);
									{	/* Llib/os.scm 1243 */
										long BgL_tmpz00_5385;

										BgL_tmpz00_5385 = (long) (BgL_xz00_3215);
										BgL_tmpz00_5379 = make_belong(BgL_tmpz00_5385);
								}}
							else
								{	/* Llib/os.scm 1242 */
									if (REALP(BgL_objz00_1903))
										{	/* Llib/os.scm 1245 */
											long BgL_res2169z00_3217;

											{	/* Llib/os.scm 1245 */
												double BgL_xz00_3216;

												BgL_xz00_3216 = REAL_TO_DOUBLE(BgL_objz00_1903);
												{	/* Llib/os.scm 1245 */
													long BgL_tmpz00_5391;

													BgL_tmpz00_5391 = (long) (BgL_xz00_3216);
													BgL_res2169z00_3217 = (long) (BgL_tmpz00_5391);
											}}
											BgL_tmpz00_5379 = make_belong(BgL_res2169z00_3217);
										}
									else
										{	/* Llib/os.scm 1244 */
											if (STRINGP(BgL_objz00_1903))
												{	/* Llib/os.scm 1247 */
													obj_t BgL_cellz00_1909;

													BgL_cellz00_1909 =
														BGl_assocz00zz__r4_pairs_and_lists_6_3z00
														(BgL_objz00_1903,
														BGl_ioctlzd2requestszd2tablez00zz__osz00);
													if (PAIRP(BgL_cellz00_1909))
														{	/* Llib/os.scm 1248 */
															BgL_tmpz00_5379 = CDR(BgL_cellz00_1909);
														}
													else
														{	/* Llib/os.scm 1250 */
															obj_t BgL_arg1764z00_1911;

															{	/* Ieee/number.scm 175 */

																BgL_arg1764z00_1911 =
																	BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00
																	(BgL_objz00_1903, BINT(10L));
															}
															{
																obj_t BgL_objz00_5403;

																BgL_objz00_5403 = BgL_arg1764z00_1911;
																BgL_objz00_1903 = BgL_objz00_5403;
																goto BgL_zc3z04anonymousza31758ze3z87_1904;
															}
														}
												}
											else
												{	/* Llib/os.scm 1246 */
													if (BIGNUMP(BgL_objz00_1903))
														{	/* Llib/os.scm 1252 */
															long BgL_tmpz00_5406;

															BgL_tmpz00_5406 =
																bgl_bignum_to_long(BgL_objz00_1903);
															BgL_tmpz00_5379 = make_belong(BgL_tmpz00_5406);
														}
													else
														{	/* Llib/os.scm 1251 */
															BgL_tmpz00_5379 =
																BGl_bigloozd2typezd2errorz00zz__errorz00
																(BGl_string2375z00zz__osz00,
																BGl_string2376z00zz__osz00, BgL_reqz00_70);
														}
												}
										}
								}
						}
					return BELONG_TO_LONG(BgL_tmpz00_5379);
				}
			}
		}

	}



/* ioctl */
	BGL_EXPORTED_DEF bool_t BGl_ioctlz00zz__osz00(obj_t BgL_devz00_71,
		obj_t BgL_requestz00_72, obj_t BgL_valz00_73)
	{
		{	/* Llib/os.scm 1259 */
			{
				obj_t BgL_nz00_1919;

				{	/* Llib/os.scm 1271 */
					long BgL_arg1767z00_1917;
					obj_t BgL_arg1768z00_1918;

					BgL_arg1767z00_1917 =
						BGl_requestzd2ze3elongz31zz__osz00(BgL_requestz00_72);
					BgL_nz00_1919 = BgL_valz00_73;
				BgL_lambda1769z00_1920:
					if (ELONGP(BgL_nz00_1919))
						{	/* Llib/os.scm 1263 */
							BgL_arg1768z00_1918 = BgL_nz00_1919;
						}
					else
						{	/* Llib/os.scm 1263 */
							if (INTEGERP(BgL_nz00_1919))
								{	/* Llib/os.scm 1264 */
									long BgL_xz00_3220;

									BgL_xz00_3220 = (long) CINT(BgL_nz00_1919);
									{	/* Llib/os.scm 1264 */
										long BgL_tmpz00_5417;

										BgL_tmpz00_5417 = (long) (BgL_xz00_3220);
										BgL_arg1768z00_1918 = make_belong(BgL_tmpz00_5417);
								}}
							else
								{	/* Llib/os.scm 1264 */
									if (BIGNUMP(BgL_nz00_1919))
										{	/* Llib/os.scm 1265 */
											long BgL_tmpz00_5422;

											BgL_tmpz00_5422 = bgl_bignum_to_long(BgL_nz00_1919);
											BgL_arg1768z00_1918 = make_belong(BgL_tmpz00_5422);
										}
									else
										{	/* Llib/os.scm 1265 */
											if (STRINGP(BgL_nz00_1919))
												{	/* Ieee/fixnum.scm 1007 */

													{	/* Ieee/fixnum.scm 1007 */
														long BgL_tmpz00_5427;

														BgL_tmpz00_5427 =
															BGl_stringzd2ze3elongz31zz__r4_numbers_6_5_fixnumz00
															(BgL_nz00_1919, 10L);
														BgL_arg1768z00_1918 = make_belong(BgL_tmpz00_5427);
												}}
											else
												{	/* Llib/os.scm 1267 */
													bool_t BgL_test2932z00_5430;

													if (INTEGERP(BgL_nz00_1919))
														{	/* Llib/os.scm 1267 */
															BgL_test2932z00_5430 = ((bool_t) 1);
														}
													else
														{	/* Llib/os.scm 1267 */
															BgL_test2932z00_5430 = REALP(BgL_nz00_1919);
														}
													if (BgL_test2932z00_5430)
														{	/* Llib/os.scm 1267 */
															long BgL_arg1777z00_1928;

															BgL_arg1777z00_1928 =
																(long) (REAL_TO_DOUBLE(BgL_nz00_1919));
															{
																obj_t BgL_nz00_5436;

																BgL_nz00_5436 = BINT(BgL_arg1777z00_1928);
																BgL_nz00_1919 = BgL_nz00_5436;
																goto BgL_lambda1769z00_1920;
															}
														}
													else
														{	/* Llib/os.scm 1267 */
															BgL_arg1768z00_1918 =
																BGl_bigloozd2typezd2errorz00zz__errorz00
																(BGl_string2375z00zz__osz00,
																BGl_string2377z00zz__osz00, BgL_nz00_1919);
														}
												}
										}
								}
						}
					return
						bgl_ioctl(BgL_devz00_71, BgL_arg1767z00_1917,
						BELONG_TO_LONG(BgL_arg1768z00_1918));
				}
			}
		}

	}



/* &ioctl */
	obj_t BGl_z62ioctlz62zz__osz00(obj_t BgL_envz00_3649, obj_t BgL_devz00_3650,
		obj_t BgL_requestz00_3651, obj_t BgL_valz00_3652)
	{
		{	/* Llib/os.scm 1259 */
			return
				BBOOL(BGl_ioctlz00zz__osz00(BgL_devz00_3650, BgL_requestz00_3651,
					BgL_valz00_3652));
		}

	}



/* _umask */
	obj_t BGl__umaskz00zz__osz00(obj_t BgL_env1155z00_76, obj_t BgL_opt1154z00_75)
	{
		{	/* Llib/os.scm 1277 */
			{	/* Llib/os.scm 1277 */

				switch (VECTOR_LENGTH(BgL_opt1154z00_75))
					{
					case 0L:

						{	/* Llib/os.scm 1277 */

							{	/* Llib/os.scm 1277 */
								int BgL_res2170z00_3225;

								if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BFALSE))
									{	/* Llib/os.scm 1279 */
										long BgL_tmpz00_5445;

										{	/* Llib/os.scm 1279 */
											long BgL_tmpz00_5446;

											{	/* Llib/os.scm 1277 */
												obj_t BgL_aux2263z00_3768;

												BgL_aux2263z00_3768 = BFALSE;
												{
													obj_t BgL_auxz00_5447;

													BgL_auxz00_5447 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2291z00zz__osz00, BINT(47121L),
														BGl_string2378z00zz__osz00,
														BGl_string2379z00zz__osz00, BgL_aux2263z00_3768);
													FAILURE(BgL_auxz00_5447, BFALSE, BFALSE);
											}}
											BgL_tmpz00_5446 = 0L;
											BgL_tmpz00_5445 = umask(BgL_tmpz00_5446);
										}
										BgL_res2170z00_3225 = (int) (BgL_tmpz00_5445);
									}
								else
									{	/* Llib/os.scm 1280 */
										long BgL_oldz00_3224;

										BgL_oldz00_3224 = umask(0L);
										umask(BgL_oldz00_3224);
										BgL_res2170z00_3225 = (int) (BgL_oldz00_3224);
									}
								return BINT(BgL_res2170z00_3225);
							}
						}
						break;
					case 1L:

						{	/* Llib/os.scm 1277 */
							obj_t BgL_maskz00_1933;

							BgL_maskz00_1933 = VECTOR_REF(BgL_opt1154z00_75, 0L);
							{	/* Llib/os.scm 1277 */

								{	/* Llib/os.scm 1277 */
									int BgL_res2171z00_3228;

									if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
										(BgL_maskz00_1933))
										{	/* Llib/os.scm 1279 */
											long BgL_tmpz00_5460;

											{	/* Llib/os.scm 1279 */
												long BgL_tmpz00_5461;

												{	/* Llib/os.scm 1279 */
													obj_t BgL_tmpz00_5462;

													if (INTEGERP(BgL_maskz00_1933))
														{	/* Llib/os.scm 1279 */
															BgL_tmpz00_5462 = BgL_maskz00_1933;
														}
													else
														{
															obj_t BgL_auxz00_5465;

															BgL_auxz00_5465 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2291z00zz__osz00, BINT(47191L),
																BGl_string2378z00zz__osz00,
																BGl_string2293z00zz__osz00, BgL_maskz00_1933);
															FAILURE(BgL_auxz00_5465, BFALSE, BFALSE);
														}
													BgL_tmpz00_5461 = (long) CINT(BgL_tmpz00_5462);
												}
												BgL_tmpz00_5460 = umask(BgL_tmpz00_5461);
											}
											BgL_res2171z00_3228 = (int) (BgL_tmpz00_5460);
										}
									else
										{	/* Llib/os.scm 1280 */
											long BgL_oldz00_3227;

											BgL_oldz00_3227 = umask(0L);
											umask(BgL_oldz00_3227);
											BgL_res2171z00_3228 = (int) (BgL_oldz00_3227);
										}
									return BINT(BgL_res2171z00_3228);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* umask */
	BGL_EXPORTED_DEF int BGl_umaskz00zz__osz00(obj_t BgL_maskz00_74)
	{
		{	/* Llib/os.scm 1277 */
			if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BgL_maskz00_74))
				{	/* Llib/os.scm 1279 */
					long BgL_tmpz00_5480;

					{	/* Llib/os.scm 1279 */
						long BgL_tmpz00_5481;

						BgL_tmpz00_5481 = (long) CINT(BgL_maskz00_74);
						BgL_tmpz00_5480 = umask(BgL_tmpz00_5481);
					}
					return (int) (BgL_tmpz00_5480);
				}
			else
				{	/* Llib/os.scm 1280 */
					long BgL_oldz00_3230;

					BgL_oldz00_3230 = umask(0L);
					umask(BgL_oldz00_3230);
					return (int) (BgL_oldz00_3230);
		}}

	}



/* openlog */
	BGL_EXPORTED_DEF obj_t BGl_openlogz00zz__osz00(obj_t BgL_identz00_77,
		int BgL_optionz00_78, int BgL_facilityz00_79)
	{
		{	/* Llib/os.scm 1287 */
			{	/* Llib/os.scm 1290 */
				char *BgL_tmpz00_5488;

				BgL_tmpz00_5488 = BSTRING_TO_STRING(BgL_identz00_77);
				openlog(BgL_tmpz00_5488, BgL_optionz00_78, BgL_facilityz00_79);
			} BUNSPEC;
			return BUNSPEC;
		}

	}



/* &openlog */
	obj_t BGl_z62openlogz62zz__osz00(obj_t BgL_envz00_3653,
		obj_t BgL_identz00_3654, obj_t BgL_optionz00_3655,
		obj_t BgL_facilityz00_3656)
	{
		{	/* Llib/os.scm 1287 */
			{	/* Llib/os.scm 1290 */
				int BgL_auxz00_5507;
				int BgL_auxz00_5498;
				obj_t BgL_auxz00_5491;

				{	/* Llib/os.scm 1290 */
					obj_t BgL_tmpz00_5508;

					if (INTEGERP(BgL_facilityz00_3656))
						{	/* Llib/os.scm 1290 */
							BgL_tmpz00_5508 = BgL_facilityz00_3656;
						}
					else
						{
							obj_t BgL_auxz00_5511;

							BgL_auxz00_5511 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
								BINT(47645L), BGl_string2380z00zz__osz00,
								BGl_string2293z00zz__osz00, BgL_facilityz00_3656);
							FAILURE(BgL_auxz00_5511, BFALSE, BFALSE);
						}
					BgL_auxz00_5507 = CINT(BgL_tmpz00_5508);
				}
				{	/* Llib/os.scm 1290 */
					obj_t BgL_tmpz00_5499;

					if (INTEGERP(BgL_optionz00_3655))
						{	/* Llib/os.scm 1290 */
							BgL_tmpz00_5499 = BgL_optionz00_3655;
						}
					else
						{
							obj_t BgL_auxz00_5502;

							BgL_auxz00_5502 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
								BINT(47645L), BGl_string2380z00zz__osz00,
								BGl_string2293z00zz__osz00, BgL_optionz00_3655);
							FAILURE(BgL_auxz00_5502, BFALSE, BFALSE);
						}
					BgL_auxz00_5498 = CINT(BgL_tmpz00_5499);
				}
				if (STRINGP(BgL_identz00_3654))
					{	/* Llib/os.scm 1290 */
						BgL_auxz00_5491 = BgL_identz00_3654;
					}
				else
					{
						obj_t BgL_auxz00_5494;

						BgL_auxz00_5494 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
							BINT(47645L), BGl_string2380z00zz__osz00,
							BGl_string2300z00zz__osz00, BgL_identz00_3654);
						FAILURE(BgL_auxz00_5494, BFALSE, BFALSE);
					}
				return
					BGl_openlogz00zz__osz00(BgL_auxz00_5491, BgL_auxz00_5498,
					BgL_auxz00_5507);
			}
		}

	}



/* syslog */
	BGL_EXPORTED_DEF obj_t BGl_syslogz00zz__osz00(int BgL_priorityz00_80,
		obj_t BgL_argsz00_81)
	{
		{	/* Llib/os.scm 1296 */
			{	/* Llib/os.scm 1302 */
				obj_t BgL_arg1779z00_1936;

				{	/* Llib/os.scm 1302 */
					obj_t BgL_zc3z04anonymousza31782ze3z87_3657;

					BgL_zc3z04anonymousza31782ze3z87_3657 =
						MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31782ze3ze5zz__osz00,
						(int) (1L), (int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31782ze3z87_3657,
						(int) (0L), BgL_argsz00_81);
					BgL_arg1779z00_1936 =
						BGl_callzd2withzd2outputzd2stringzd2zz__r4_ports_6_10_1z00
						(BgL_zc3z04anonymousza31782ze3z87_3657);
				}
				{	/* Llib/os.scm 1299 */
					char *BgL_auxz00_5525;
					char *BgL_tmpz00_5523;

					BgL_auxz00_5525 = BSTRING_TO_STRING(BgL_arg1779z00_1936);
					BgL_tmpz00_5523 = BSTRING_TO_STRING(BGl_string2381z00zz__osz00);
					syslog(BgL_priorityz00_80, BgL_tmpz00_5523, BgL_auxz00_5525);
				} BUNSPEC;
			}
			return BUNSPEC;
		}

	}



/* &syslog */
	obj_t BGl_z62syslogz62zz__osz00(obj_t BgL_envz00_3658,
		obj_t BgL_priorityz00_3659, obj_t BgL_argsz00_3660)
	{
		{	/* Llib/os.scm 1296 */
			{	/* Llib/os.scm 1302 */
				int BgL_auxz00_5528;

				{	/* Llib/os.scm 1302 */
					obj_t BgL_tmpz00_5529;

					if (INTEGERP(BgL_priorityz00_3659))
						{	/* Llib/os.scm 1302 */
							BgL_tmpz00_5529 = BgL_priorityz00_3659;
						}
					else
						{
							obj_t BgL_auxz00_5532;

							BgL_auxz00_5532 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
								BINT(48113L), BGl_string2382z00zz__osz00,
								BGl_string2293z00zz__osz00, BgL_priorityz00_3659);
							FAILURE(BgL_auxz00_5532, BFALSE, BFALSE);
						}
					BgL_auxz00_5528 = CINT(BgL_tmpz00_5529);
				}
				return BGl_syslogz00zz__osz00(BgL_auxz00_5528, BgL_argsz00_3660);
			}
		}

	}



/* &<@anonymous:1782> */
	obj_t BGl_z62zc3z04anonymousza31782ze3ze5zz__osz00(obj_t BgL_envz00_3661,
		obj_t BgL_opz00_3663)
	{
		{	/* Llib/os.scm 1301 */
			{	/* Llib/os.scm 1302 */
				obj_t BgL_argsz00_3662;

				BgL_argsz00_3662 = PROCEDURE_REF(BgL_envz00_3661, (int) (0L));
				{	/* Llib/os.scm 1302 */
					bool_t BgL_tmpz00_5540;

					{
						obj_t BgL_l1121z00_3842;

						BgL_l1121z00_3842 = BgL_argsz00_3662;
					BgL_zc3z04anonymousza31783ze3z87_3841:
						if (PAIRP(BgL_l1121z00_3842))
							{	/* Llib/os.scm 1302 */
								bgl_display_obj(CAR(BgL_l1121z00_3842), BgL_opz00_3663);
								{
									obj_t BgL_l1121z00_5545;

									BgL_l1121z00_5545 = CDR(BgL_l1121z00_3842);
									BgL_l1121z00_3842 = BgL_l1121z00_5545;
									goto BgL_zc3z04anonymousza31783ze3z87_3841;
								}
							}
						else
							{	/* Llib/os.scm 1302 */
								BgL_tmpz00_5540 = ((bool_t) 1);
							}
					}
					return BBOOL(BgL_tmpz00_5540);
				}
			}
		}

	}



/* closelog */
	BGL_EXPORTED_DEF obj_t BGl_closelogz00zz__osz00(void)
	{
		{	/* Llib/os.scm 1308 */
			closelog();
			BUNSPEC;
			return BUNSPEC;
		}

	}



/* &closelog */
	obj_t BGl_z62closelogz62zz__osz00(obj_t BgL_envz00_3664)
	{
		{	/* Llib/os.scm 1308 */
			return BGl_closelogz00zz__osz00();
		}

	}



/* syslog-option */
	BGL_EXPORTED_DEF int BGl_syslogzd2optionzd2zz__osz00(obj_t BgL_optsz00_82)
	{
		{	/* Llib/os.scm 1316 */
			{
				obj_t BgL_optsz00_1949;
				long BgL_oz00_1950;

				{	/* Llib/os.scm 1319 */
					long BgL_tmpz00_5550;

					BgL_optsz00_1949 = BgL_optsz00_82;
					BgL_oz00_1950 = 0L;
				BgL_zc3z04anonymousza31786ze3z87_1951:
					if (NULLP(BgL_optsz00_1949))
						{	/* Llib/os.scm 1321 */
							BgL_tmpz00_5550 = BgL_oz00_1950;
						}
					else
						{	/* Llib/os.scm 1323 */
							obj_t BgL_arg1788z00_1953;
							long BgL_arg1789z00_1954;

							BgL_arg1788z00_1953 = CDR(((obj_t) BgL_optsz00_1949));
							{	/* Llib/os.scm 1325 */
								obj_t BgL_arg1790z00_1955;

								{	/* Llib/os.scm 1325 */
									obj_t BgL_casezd2valuezd2_1956;

									BgL_casezd2valuezd2_1956 = CAR(((obj_t) BgL_optsz00_1949));
									if ((BgL_casezd2valuezd2_1956 == BGl_symbol2383z00zz__osz00))
										{	/* Llib/os.scm 1325 */
											BgL_arg1790z00_1955 = BINT(LOG_CONS);
										}
									else
										{	/* Llib/os.scm 1325 */
											if (
												(BgL_casezd2valuezd2_1956 ==
													BGl_symbol2385z00zz__osz00))
												{	/* Llib/os.scm 1325 */
													BgL_arg1790z00_1955 = BINT(LOG_NDELAY);
												}
											else
												{	/* Llib/os.scm 1325 */
													if (
														(BgL_casezd2valuezd2_1956 ==
															BGl_symbol2387z00zz__osz00))
														{	/* Llib/os.scm 1325 */
															BgL_arg1790z00_1955 = BINT(LOG_NOWAIT);
														}
													else
														{	/* Llib/os.scm 1325 */
															if (
																(BgL_casezd2valuezd2_1956 ==
																	BGl_symbol2389z00zz__osz00))
																{	/* Llib/os.scm 1325 */
																	BgL_arg1790z00_1955 = BINT(LOG_ODELAY);
																}
															else
																{	/* Llib/os.scm 1325 */
																	if (
																		(BgL_casezd2valuezd2_1956 ==
																			BGl_symbol2391z00zz__osz00))
																		{	/* Llib/os.scm 1325 */
																			BgL_arg1790z00_1955 = BINT(LOG_PID);
																		}
																	else
																		{	/* Llib/os.scm 1325 */
																			BgL_arg1790z00_1955 =
																				BGl_errorz00zz__errorz00
																				(BGl_string2393z00zz__osz00,
																				BGl_string2394z00zz__osz00,
																				BgL_casezd2valuezd2_1956);
																		}
																}
														}
												}
										}
								}
								BgL_arg1789z00_1954 =
									(BgL_oz00_1950 | (long) CINT(BgL_arg1790z00_1955));
							}
							{
								long BgL_oz00_5576;
								obj_t BgL_optsz00_5575;

								BgL_optsz00_5575 = BgL_arg1788z00_1953;
								BgL_oz00_5576 = BgL_arg1789z00_1954;
								BgL_oz00_1950 = BgL_oz00_5576;
								BgL_optsz00_1949 = BgL_optsz00_5575;
								goto BgL_zc3z04anonymousza31786ze3z87_1951;
							}
						}
					return (int) (BgL_tmpz00_5550);
		}}}

	}



/* &syslog-option */
	obj_t BGl_z62syslogzd2optionzb0zz__osz00(obj_t BgL_envz00_3665,
		obj_t BgL_optsz00_3666)
	{
		{	/* Llib/os.scm 1316 */
			return BINT(BGl_syslogzd2optionzd2zz__osz00(BgL_optsz00_3666));
		}

	}



/* syslog-facility */
	BGL_EXPORTED_DEF int BGl_syslogzd2facilityzd2zz__osz00(obj_t
		BgL_facilityz00_83)
	{
		{	/* Llib/os.scm 1340 */
			if ((BgL_facilityz00_83 == BGl_symbol2395z00zz__osz00))
				{	/* Llib/os.scm 1343 */
					return LOG_AUTH;
				}
			else
				{	/* Llib/os.scm 1343 */
					if ((BgL_facilityz00_83 == BGl_symbol2397z00zz__osz00))
						{	/* Llib/os.scm 1343 */
							return LOG_AUTHPRIV;
						}
					else
						{	/* Llib/os.scm 1343 */
							if ((BgL_facilityz00_83 == BGl_symbol2399z00zz__osz00))
								{	/* Llib/os.scm 1343 */
									return LOG_CRON;
								}
							else
								{	/* Llib/os.scm 1343 */
									if ((BgL_facilityz00_83 == BGl_symbol2401z00zz__osz00))
										{	/* Llib/os.scm 1343 */
											return LOG_DAEMON;
										}
									else
										{	/* Llib/os.scm 1343 */
											if ((BgL_facilityz00_83 == BGl_symbol2403z00zz__osz00))
												{	/* Llib/os.scm 1343 */
													return LOG_FTP;
												}
											else
												{	/* Llib/os.scm 1343 */
													if (
														(BgL_facilityz00_83 == BGl_symbol2405z00zz__osz00))
														{	/* Llib/os.scm 1343 */
															return LOG_KERN;
														}
													else
														{	/* Llib/os.scm 1343 */
															if (
																(BgL_facilityz00_83 ==
																	BGl_symbol2407z00zz__osz00))
																{	/* Llib/os.scm 1343 */
																	return LOG_LOCAL0;
																}
															else
																{	/* Llib/os.scm 1343 */
																	if (
																		(BgL_facilityz00_83 ==
																			BGl_symbol2409z00zz__osz00))
																		{	/* Llib/os.scm 1343 */
																			return LOG_LOCAL1;
																		}
																	else
																		{	/* Llib/os.scm 1343 */
																			if (
																				(BgL_facilityz00_83 ==
																					BGl_symbol2411z00zz__osz00))
																				{	/* Llib/os.scm 1343 */
																					return LOG_LOCAL2;
																				}
																			else
																				{	/* Llib/os.scm 1343 */
																					if (
																						(BgL_facilityz00_83 ==
																							BGl_symbol2413z00zz__osz00))
																						{	/* Llib/os.scm 1343 */
																							return LOG_LOCAL3;
																						}
																					else
																						{	/* Llib/os.scm 1343 */
																							if (
																								(BgL_facilityz00_83 ==
																									BGl_symbol2415z00zz__osz00))
																								{	/* Llib/os.scm 1343 */
																									return LOG_LOCAL4;
																								}
																							else
																								{	/* Llib/os.scm 1343 */
																									if (
																										(BgL_facilityz00_83 ==
																											BGl_symbol2417z00zz__osz00))
																										{	/* Llib/os.scm 1343 */
																											return LOG_LOCAL5;
																										}
																									else
																										{	/* Llib/os.scm 1343 */
																											if (
																												(BgL_facilityz00_83 ==
																													BGl_symbol2419z00zz__osz00))
																												{	/* Llib/os.scm 1343 */
																													return LOG_LOCAL6;
																												}
																											else
																												{	/* Llib/os.scm 1343 */
																													if (
																														(BgL_facilityz00_83
																															==
																															BGl_symbol2421z00zz__osz00))
																														{	/* Llib/os.scm 1343 */
																															return LOG_LOCAL7;
																														}
																													else
																														{	/* Llib/os.scm 1343 */
																															if (
																																(BgL_facilityz00_83
																																	==
																																	BGl_symbol2423z00zz__osz00))
																																{	/* Llib/os.scm 1343 */
																																	return
																																		LOG_LPR;
																																}
																															else
																																{	/* Llib/os.scm 1343 */
																																	if (
																																		(BgL_facilityz00_83
																																			==
																																			BGl_symbol2425z00zz__osz00))
																																		{	/* Llib/os.scm 1343 */
																																			return
																																				LOG_MAIL;
																																		}
																																	else
																																		{	/* Llib/os.scm 1343 */
																																			if (
																																				(BgL_facilityz00_83
																																					==
																																					BGl_symbol2427z00zz__osz00))
																																				{	/* Llib/os.scm 1343 */
																																					return
																																						LOG_NEWS;
																																				}
																																			else
																																				{	/* Llib/os.scm 1343 */
																																					if (
																																						(BgL_facilityz00_83
																																							==
																																							BGl_symbol2429z00zz__osz00))
																																						{	/* Llib/os.scm 1343 */
																																							return
																																								LOG_SYSLOG;
																																						}
																																					else
																																						{	/* Llib/os.scm 1343 */
																																							if ((BgL_facilityz00_83 == BGl_symbol2431z00zz__osz00))
																																								{	/* Llib/os.scm 1343 */
																																									return
																																										LOG_USER;
																																								}
																																							else
																																								{	/* Llib/os.scm 1343 */
																																									if ((BgL_facilityz00_83 == BGl_symbol2433z00zz__osz00))
																																										{	/* Llib/os.scm 1343 */
																																											return
																																												LOG_UUCP;
																																										}
																																									else
																																										{	/* Llib/os.scm 1343 */
																																											return
																																												CINT
																																												(BGl_errorz00zz__errorz00
																																												(BGl_string2435z00zz__osz00,
																																													BGl_string2436z00zz__osz00,
																																													BgL_facilityz00_83));
																																										}
																																								}
																																						}
																																				}
																																		}
																																}
																														}
																												}
																										}
																								}
																						}
																				}
																		}
																}
														}
												}
										}
								}
						}
				}
		}

	}



/* &syslog-facility */
	obj_t BGl_z62syslogzd2facilityzb0zz__osz00(obj_t BgL_envz00_3667,
		obj_t BgL_facilityz00_3668)
	{
		{	/* Llib/os.scm 1340 */
			{	/* Llib/os.scm 1343 */
				int BgL_tmpz00_5622;

				{	/* Llib/os.scm 1343 */
					obj_t BgL_auxz00_5623;

					if (SYMBOLP(BgL_facilityz00_3668))
						{	/* Llib/os.scm 1343 */
							BgL_auxz00_5623 = BgL_facilityz00_3668;
						}
					else
						{
							obj_t BgL_auxz00_5626;

							BgL_auxz00_5626 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
								BINT(49601L), BGl_string2437z00zz__osz00,
								BGl_string2334z00zz__osz00, BgL_facilityz00_3668);
							FAILURE(BgL_auxz00_5626, BFALSE, BFALSE);
						}
					BgL_tmpz00_5622 = BGl_syslogzd2facilityzd2zz__osz00(BgL_auxz00_5623);
				}
				return BINT(BgL_tmpz00_5622);
			}
		}

	}



/* syslog-level */
	BGL_EXPORTED_DEF int BGl_syslogzd2levelzd2zz__osz00(obj_t BgL_lvlz00_84)
	{
		{	/* Llib/os.scm 1371 */
			if ((BgL_lvlz00_84 == BGl_symbol2438z00zz__osz00))
				{	/* Llib/os.scm 1374 */
					return LOG_EMERG;
				}
			else
				{	/* Llib/os.scm 1374 */
					if ((BgL_lvlz00_84 == BGl_symbol2440z00zz__osz00))
						{	/* Llib/os.scm 1374 */
							return LOG_ALERT;
						}
					else
						{	/* Llib/os.scm 1374 */
							if ((BgL_lvlz00_84 == BGl_symbol2442z00zz__osz00))
								{	/* Llib/os.scm 1374 */
									return LOG_CRIT;
								}
							else
								{	/* Llib/os.scm 1374 */
									if ((BgL_lvlz00_84 == BGl_symbol2444z00zz__osz00))
										{	/* Llib/os.scm 1374 */
											return LOG_ERR;
										}
									else
										{	/* Llib/os.scm 1374 */
											if ((BgL_lvlz00_84 == BGl_symbol2446z00zz__osz00))
												{	/* Llib/os.scm 1374 */
													return LOG_WARNING;
												}
											else
												{	/* Llib/os.scm 1374 */
													if ((BgL_lvlz00_84 == BGl_symbol2448z00zz__osz00))
														{	/* Llib/os.scm 1374 */
															return LOG_NOTICE;
														}
													else
														{	/* Llib/os.scm 1374 */
															if ((BgL_lvlz00_84 == BGl_symbol2450z00zz__osz00))
																{	/* Llib/os.scm 1374 */
																	return LOG_INFO;
																}
															else
																{	/* Llib/os.scm 1374 */
																	if (
																		(BgL_lvlz00_84 ==
																			BGl_symbol2452z00zz__osz00))
																		{	/* Llib/os.scm 1374 */
																			return LOG_DEBUG;
																		}
																	else
																		{	/* Llib/os.scm 1374 */
																			return
																				CINT(BGl_errorz00zz__errorz00
																				(BGl_string2454z00zz__osz00,
																					BGl_string2455z00zz__osz00,
																					BgL_lvlz00_84));
																		}
																}
														}
												}
										}
								}
						}
				}
		}

	}



/* &syslog-level */
	obj_t BGl_z62syslogzd2levelzb0zz__osz00(obj_t BgL_envz00_3669,
		obj_t BgL_lvlz00_3670)
	{
		{	/* Llib/os.scm 1371 */
			{	/* Llib/os.scm 1374 */
				int BgL_tmpz00_5650;

				{	/* Llib/os.scm 1374 */
					obj_t BgL_auxz00_5651;

					if (SYMBOLP(BgL_lvlz00_3670))
						{	/* Llib/os.scm 1374 */
							BgL_auxz00_5651 = BgL_lvlz00_3670;
						}
					else
						{
							obj_t BgL_auxz00_5654;

							BgL_auxz00_5654 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
								BINT(50729L), BGl_string2456z00zz__osz00,
								BGl_string2334z00zz__osz00, BgL_lvlz00_3670);
							FAILURE(BgL_auxz00_5654, BFALSE, BFALSE);
						}
					BgL_tmpz00_5650 = BGl_syslogzd2levelzd2zz__osz00(BgL_auxz00_5651);
				}
				return BINT(BgL_tmpz00_5650);
			}
		}

	}



/* limit-resource-no */
	obj_t BGl_limitzd2resourcezd2noz00zz__osz00(obj_t BgL_rz00_85,
		obj_t BgL_idz00_86)
	{
		{	/* Llib/os.scm 1390 */
			{
				obj_t BgL_rz00_1997;

				if (INTEGERP(BgL_rz00_85))
					{	/* Llib/os.scm 1414 */
						return BgL_rz00_85;
					}
				else
					{	/* Llib/os.scm 1414 */
						if (SYMBOLP(BgL_rz00_85))
							{	/* Llib/os.scm 1415 */
								BgL_rz00_1997 = BgL_rz00_85;
								if ((BgL_rz00_1997 == BGl_symbol2457z00zz__osz00))
									{	/* Llib/os.scm 1395 */
										return BINT(RLIMIT_CORE);
									}
								else
									{	/* Llib/os.scm 1395 */
										if ((BgL_rz00_1997 == BGl_symbol2459z00zz__osz00))
											{	/* Llib/os.scm 1395 */
												return BINT(RLIMIT_CPU);
											}
										else
											{	/* Llib/os.scm 1395 */
												if ((BgL_rz00_1997 == BGl_symbol2461z00zz__osz00))
													{	/* Llib/os.scm 1395 */
														return BINT(RLIMIT_DATA);
													}
												else
													{	/* Llib/os.scm 1395 */
														if ((BgL_rz00_1997 == BGl_symbol2463z00zz__osz00))
															{	/* Llib/os.scm 1395 */
																return BINT(RLIMIT_FSIZE);
															}
														else
															{	/* Llib/os.scm 1395 */
																if (
																	(BgL_rz00_1997 == BGl_symbol2465z00zz__osz00))
																	{	/* Llib/os.scm 1395 */
																		return BINT(RLIMIT_LOCKS);
																	}
																else
																	{	/* Llib/os.scm 1395 */
																		if (
																			(BgL_rz00_1997 ==
																				BGl_symbol2467z00zz__osz00))
																			{	/* Llib/os.scm 1395 */
																				return BINT(RLIMIT_MEMLOCK);
																			}
																		else
																			{	/* Llib/os.scm 1395 */
																				if (
																					(BgL_rz00_1997 ==
																						BGl_symbol2469z00zz__osz00))
																					{	/* Llib/os.scm 1395 */
																						return BINT(RLIMIT_MSGQUEUE);
																					}
																				else
																					{	/* Llib/os.scm 1395 */
																						if (
																							(BgL_rz00_1997 ==
																								BGl_symbol2471z00zz__osz00))
																							{	/* Llib/os.scm 1395 */
																								return BINT(RLIMIT_NICE);
																							}
																						else
																							{	/* Llib/os.scm 1395 */
																								if (
																									(BgL_rz00_1997 ==
																										BGl_symbol2473z00zz__osz00))
																									{	/* Llib/os.scm 1395 */
																										return BINT(RLIMIT_NOFILE);
																									}
																								else
																									{	/* Llib/os.scm 1395 */
																										if (
																											(BgL_rz00_1997 ==
																												BGl_symbol2475z00zz__osz00))
																											{	/* Llib/os.scm 1395 */
																												return
																													BINT(RLIMIT_NPROC);
																											}
																										else
																											{	/* Llib/os.scm 1395 */
																												if (
																													(BgL_rz00_1997 ==
																														BGl_symbol2477z00zz__osz00))
																													{	/* Llib/os.scm 1395 */
																														return
																															BINT(RLIMIT_RSS);
																													}
																												else
																													{	/* Llib/os.scm 1395 */
																														if (
																															(BgL_rz00_1997 ==
																																BGl_symbol2479z00zz__osz00))
																															{	/* Llib/os.scm 1395 */
																																return
																																	BINT
																																	(RLIMIT_RTTIME);
																															}
																														else
																															{	/* Llib/os.scm 1395 */
																																if (
																																	(BgL_rz00_1997
																																		==
																																		BGl_symbol2481z00zz__osz00))
																																	{	/* Llib/os.scm 1395 */
																																		return
																																			BINT
																																			(RLIMIT_SIGPENDING);
																																	}
																																else
																																	{	/* Llib/os.scm 1395 */
																																		if (
																																			(BgL_rz00_1997
																																				==
																																				BGl_symbol2483z00zz__osz00))
																																			{	/* Llib/os.scm 1395 */
																																				return
																																					BINT
																																					(RLIMIT_STACK);
																																			}
																																		else
																																			{	/* Llib/os.scm 1395 */
																																				return
																																					BGl_errorz00zz__errorz00
																																					(BgL_idz00_86,
																																					BGl_string2485z00zz__osz00,
																																					BgL_rz00_1997);
																																			}
																																	}
																															}
																													}
																											}
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
									}
							}
						else
							{	/* Llib/os.scm 1415 */
								return
									BGl_bigloozd2typezd2errorz00zz__errorz00(BgL_idz00_86,
									BGl_string2486z00zz__osz00, BgL_rz00_85);
							}
					}
			}
		}

	}



/* getrlimit */
	BGL_EXPORTED_DEF obj_t BGl_getrlimitz00zz__osz00(obj_t BgL_rz00_87)
	{
		{	/* Llib/os.scm 1421 */
			{	/* Llib/os.scm 1424 */
				obj_t BgL_arg1842z00_3303;

				BgL_arg1842z00_3303 =
					BGl_limitzd2resourcezd2noz00zz__osz00(BgL_rz00_87,
					BGl_string2487z00zz__osz00);
				return bgl_getrlimit((long) CINT(BgL_arg1842z00_3303));
		}}

	}



/* &getrlimit */
	obj_t BGl_z62getrlimitz62zz__osz00(obj_t BgL_envz00_3671, obj_t BgL_rz00_3672)
	{
		{	/* Llib/os.scm 1421 */
			return BGl_getrlimitz00zz__osz00(BgL_rz00_3672);
		}

	}



/* setrlimit! */
	BGL_EXPORTED_DEF bool_t BGl_setrlimitz12z12zz__osz00(obj_t BgL_rz00_88,
		long BgL_softz00_89, long BgL_hardz00_90)
	{
		{	/* Llib/os.scm 1431 */
			{	/* Llib/os.scm 1434 */
				obj_t BgL_arg1843z00_3304;

				BgL_arg1843z00_3304 =
					BGl_limitzd2resourcezd2noz00zz__osz00(BgL_rz00_88,
					BGl_string2488z00zz__osz00);
				return bgl_setrlimit((long) CINT(BgL_arg1843z00_3304),
					(long) (BgL_softz00_89), (long) (BgL_hardz00_90));
		}}

	}



/* &setrlimit! */
	obj_t BGl_z62setrlimitz12z70zz__osz00(obj_t BgL_envz00_3673,
		obj_t BgL_rz00_3674, obj_t BgL_softz00_3675, obj_t BgL_hardz00_3676)
	{
		{	/* Llib/os.scm 1431 */
			{	/* Llib/os.scm 1434 */
				bool_t BgL_tmpz00_5717;

				{	/* Llib/os.scm 1434 */
					long BgL_auxz00_5727;
					long BgL_auxz00_5718;

					{	/* Llib/os.scm 1434 */
						obj_t BgL_tmpz00_5728;

						if (ELONGP(BgL_hardz00_3676))
							{	/* Llib/os.scm 1434 */
								BgL_tmpz00_5728 = BgL_hardz00_3676;
							}
						else
							{
								obj_t BgL_auxz00_5731;

								BgL_auxz00_5731 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
									BINT(52869L), BGl_string2489z00zz__osz00,
									BGl_string2490z00zz__osz00, BgL_hardz00_3676);
								FAILURE(BgL_auxz00_5731, BFALSE, BFALSE);
							}
						BgL_auxz00_5727 = BELONG_TO_LONG(BgL_tmpz00_5728);
					}
					{	/* Llib/os.scm 1434 */
						obj_t BgL_tmpz00_5719;

						if (ELONGP(BgL_softz00_3675))
							{	/* Llib/os.scm 1434 */
								BgL_tmpz00_5719 = BgL_softz00_3675;
							}
						else
							{
								obj_t BgL_auxz00_5722;

								BgL_auxz00_5722 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2291z00zz__osz00,
									BINT(52869L), BGl_string2489z00zz__osz00,
									BGl_string2490z00zz__osz00, BgL_softz00_3675);
								FAILURE(BgL_auxz00_5722, BFALSE, BFALSE);
							}
						BgL_auxz00_5718 = BELONG_TO_LONG(BgL_tmpz00_5719);
					}
					BgL_tmpz00_5717 =
						BGl_setrlimitz12z12zz__osz00(BgL_rz00_3674, BgL_auxz00_5718,
						BgL_auxz00_5727);
				}
				return BBOOL(BgL_tmpz00_5717);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__osz00(void)
	{
		{	/* Llib/os.scm 18 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__osz00(void)
	{
		{	/* Llib/os.scm 18 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__osz00(void)
	{
		{	/* Llib/os.scm 18 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__osz00(void)
	{
		{	/* Llib/os.scm 18 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string2491z00zz__osz00));
			return
				BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string2491z00zz__osz00));
		}

	}

#ifdef __cplusplus
}
#endif
