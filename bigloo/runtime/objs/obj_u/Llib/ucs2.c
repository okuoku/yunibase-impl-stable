/*===========================================================================*/
/*   (Llib/ucs2.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Llib/ucs2.scm -indent -o objs/obj_u/Llib/ucs2.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___UCS2_TYPE_DEFINITIONS
#define BGL___UCS2_TYPE_DEFINITIONS
#endif													// BGL___UCS2_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62ucs2ze3zf3z72zz__ucs2z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62ucs2zd2upcasezb0zz__ucs2z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_ucs2zd2cizc3zf3ze2zz__ucs2z00(ucs2_t, ucs2_t);
	BGL_EXPORTED_DECL bool_t BGl_ucs2zc3zd3zf3ze3zz__ucs2z00(ucs2_t, ucs2_t);
	static obj_t BGl_z62ucs2zc3zd3zf3z81zz__ucs2z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62ucs2zd2lowerzd2casezf3z91zz__ucs2z00(obj_t, obj_t);
	static obj_t BGl_z62ucs2zd2ze3charz53zz__ucs2z00(obj_t, obj_t);
	static obj_t BGl_z62integerzd2ze3ucs2zd2urz81zz__ucs2z00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__ucs2z00 = BUNSPEC;
	BGL_EXPORTED_DECL bool_t BGl_ucs2zd2cizd3zf3zf2zz__ucs2z00(ucs2_t, ucs2_t);
	BGL_EXPORTED_DECL bool_t BGl_ucs2zd2cize3zf3zc2zz__ucs2z00(ucs2_t, ucs2_t);
	BGL_EXPORTED_DECL ucs2_t BGl_integerzd2ze3ucs2z31zz__ucs2z00(int);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__ucs2z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_EXPORTED_DECL bool_t BGl_ucs2zc3zf3z30zz__ucs2z00(ucs2_t, ucs2_t);
	static obj_t BGl_z62ucs2zd2alphabeticzf3z43zz__ucs2z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_ucs2zf3zf3zz__ucs2z00(obj_t);
	extern bool_t ucs2_digitp(ucs2_t);
	BGL_EXPORTED_DECL int BGl_ucs2zd2ze3integerz31zz__ucs2z00(ucs2_t);
	BGL_EXPORTED_DECL bool_t BGl_ucs2ze3zd3zf3zc3zz__ucs2z00(ucs2_t, ucs2_t);
	BGL_EXPORTED_DECL bool_t BGl_ucs2zd3zf3z20zz__ucs2z00(ucs2_t, ucs2_t);
	BGL_EXPORTED_DECL bool_t BGl_ucs2zd2numericzf3z21zz__ucs2z00(ucs2_t);
	static obj_t BGl_z62ucs2ze3zd3zf3za1zz__ucs2z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL ucs2_t BGl_ucs2zd2downcasezd2zz__ucs2z00(ucs2_t);
	BGL_EXPORTED_DECL ucs2_t BGl_charzd2ze3ucs2z31zz__ucs2z00(unsigned char);
	static obj_t BGl_cnstzd2initzd2zz__ucs2z00(void);
	BGL_EXPORTED_DECL bool_t BGl_ucs2ze3zf3z10zz__ucs2z00(ucs2_t, ucs2_t);
	BGL_EXPORTED_DECL bool_t BGl_ucs2zd2upperzd2casezf3zf3zz__ucs2z00(ucs2_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__ucs2z00(void);
	BGL_EXPORTED_DECL bool_t BGl_ucs2zd2cizc3zd3zf3z31zz__ucs2z00(ucs2_t, ucs2_t);
	extern ucs2_t ucs2_tolower(ucs2_t);
	static obj_t BGl_gczd2rootszd2initz00zz__ucs2z00(void);
	static obj_t BGl_z62integerzd2ze3ucs2z53zz__ucs2z00(obj_t, obj_t);
	static obj_t BGl_z62charzd2ze3ucs2z53zz__ucs2z00(obj_t, obj_t);
	static obj_t BGl_symbol1512z00zz__ucs2z00 = BUNSPEC;
	static obj_t BGl_z62ucs2zf3z91zz__ucs2z00(obj_t, obj_t);
	BGL_EXPORTED_DECL ucs2_t BGl_ucs2zd2upcasezd2zz__ucs2z00(ucs2_t);
	static obj_t BGl_z62ucs2zd2cizc3zd3zf3z53zz__ucs2z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_ucs2zd2whitespacezf3z21zz__ucs2z00(ucs2_t);
	extern bool_t ucs2_upperp(ucs2_t);
	static obj_t BGl_z62ucs2zd2ze3integerz53zz__ucs2z00(obj_t, obj_t);
	static obj_t BGl_symbol1522z00zz__ucs2z00 = BUNSPEC;
	static obj_t BGl_z62ucs2zd2numericzf3z43zz__ucs2z00(obj_t, obj_t);
	static obj_t BGl_z62ucs2zd2downcasezb0zz__ucs2z00(obj_t, obj_t);
	extern bool_t ucs2_definedp(int);
	extern ucs2_t ucs2_toupper(ucs2_t);
	extern obj_t bstring_to_symbol(obj_t);
	extern bool_t ucs2_whitespacep(ucs2_t);
	BGL_EXPORTED_DECL bool_t BGl_ucs2zd2cize3zd3zf3z11zz__ucs2z00(ucs2_t, ucs2_t);
	BGL_EXPORTED_DECL bool_t BGl_ucs2zd2lowerzd2casezf3zf3zz__ucs2z00(ucs2_t);
	BGL_EXPORTED_DECL ucs2_t BGl_integerzd2ze3ucs2zd2urze3zz__ucs2z00(int);
	static obj_t BGl_z62ucs2zd2upperzd2casezf3z91zz__ucs2z00(obj_t, obj_t);
	extern bool_t ucs2_lowerp(ucs2_t);
	static obj_t BGl_z62ucs2zd2cize3zd3zf3z73zz__ucs2z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62ucs2zd2cizc3zf3z80zz__ucs2z00(obj_t, obj_t, obj_t);
	extern bool_t ucs2_letterp(ucs2_t);
	BGL_EXPORTED_DECL bool_t BGl_ucs2zd2alphabeticzf3z21zz__ucs2z00(ucs2_t);
	static obj_t BGl_z62ucs2zd2cizd3zf3z90zz__ucs2z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62ucs2zc3zf3z52zz__ucs2z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62ucs2zd2cize3zf3za0zz__ucs2z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62ucs2zd2whitespacezf3z43zz__ucs2z00(obj_t, obj_t);
	static obj_t BGl_z62ucs2zd3zf3z42zz__ucs2z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL unsigned char BGl_ucs2zd2ze3charz31zz__ucs2z00(ucs2_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_integerzd2ze3ucs2zd2urzd2envz31zz__ucs2z00,
		BgL_bgl_za762integerza7d2za7e31527za7,
		BGl_z62integerzd2ze3ucs2zd2urz81zz__ucs2z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ucs2zc3zf3zd2envze2zz__ucs2z00,
		BgL_bgl_za762ucs2za7c3za7f3za7521528z00, BGl_z62ucs2zc3zf3z52zz__ucs2z00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ucs2zf3zd2envz21zz__ucs2z00,
		BgL_bgl_za762ucs2za7f3za791za7za7_1529za7, BGl_z62ucs2zf3z91zz__ucs2z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1500z00zz__ucs2z00,
		BgL_bgl_string1500za700za7za7_1530za7, "&ucs2-ci=?", 10);
	      DEFINE_STRING(BGl_string1501z00zz__ucs2z00,
		BgL_bgl_string1501za700za7za7_1531za7, "&ucs2-ci<?", 10);
	      DEFINE_STRING(BGl_string1502z00zz__ucs2z00,
		BgL_bgl_string1502za700za7za7_1532za7, "&ucs2-ci>?", 10);
	      DEFINE_STRING(BGl_string1503z00zz__ucs2z00,
		BgL_bgl_string1503za700za7za7_1533za7, "&ucs2-ci<=?", 11);
	      DEFINE_STRING(BGl_string1504z00zz__ucs2z00,
		BgL_bgl_string1504za700za7za7_1534za7, "&ucs2-ci>=?", 11);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ucs2zd2upcasezd2envz00zz__ucs2z00,
		BgL_bgl_za762ucs2za7d2upcase1535z00, BGl_z62ucs2zd2upcasezb0zz__ucs2z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1505z00zz__ucs2z00,
		BgL_bgl_string1505za700za7za7_1536za7, "&ucs2-alphabetic?", 17);
	      DEFINE_STRING(BGl_string1506z00zz__ucs2z00,
		BgL_bgl_string1506za700za7za7_1537za7, "&ucs2-numeric?", 14);
	      DEFINE_STRING(BGl_string1507z00zz__ucs2z00,
		BgL_bgl_string1507za700za7za7_1538za7, "&ucs2-whitespace?", 17);
	      DEFINE_STRING(BGl_string1508z00zz__ucs2z00,
		BgL_bgl_string1508za700za7za7_1539za7, "&ucs2-upper-case?", 17);
	      DEFINE_STRING(BGl_string1509z00zz__ucs2z00,
		BgL_bgl_string1509za700za7za7_1540za7, "&ucs2-lower-case?", 17);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ucs2zd2cizc3zf3zd2envz30zz__ucs2z00,
		BgL_bgl_za762ucs2za7d2ciza7c3za71541z00,
		BGl_z62ucs2zd2cizc3zf3z80zz__ucs2z00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ucs2zc3zd3zf3zd2envz31zz__ucs2z00,
		BgL_bgl_za762ucs2za7c3za7d3za7f31542z00, BGl_z62ucs2zc3zd3zf3z81zz__ucs2z00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ucs2zd2numericzf3zd2envzf3zz__ucs2z00,
		BgL_bgl_za762ucs2za7d2numeri1543z00, BGl_z62ucs2zd2numericzf3z43zz__ucs2z00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1510z00zz__ucs2z00,
		BgL_bgl_string1510za700za7za7_1544za7, "&ucs2-upcase", 12);
	      DEFINE_STRING(BGl_string1511z00zz__ucs2z00,
		BgL_bgl_string1511za700za7za7_1545za7, "&ucs2-downcase", 14);
	      DEFINE_STRING(BGl_string1513z00zz__ucs2z00,
		BgL_bgl_string1513za700za7za7_1546za7, "integer->ucs2", 13);
	      DEFINE_STRING(BGl_string1514z00zz__ucs2z00,
		BgL_bgl_string1514za700za7za7_1547za7, "Undefined UCS-2 character", 25);
	      DEFINE_STRING(BGl_string1515z00zz__ucs2z00,
		BgL_bgl_string1515za700za7za7_1548za7, "integer out of range or ", 24);
	      DEFINE_STRING(BGl_string1516z00zz__ucs2z00,
		BgL_bgl_string1516za700za7za7_1549za7, "&integer->ucs2", 14);
	      DEFINE_STRING(BGl_string1517z00zz__ucs2z00,
		BgL_bgl_string1517za700za7za7_1550za7, "bint", 4);
	      DEFINE_STRING(BGl_string1518z00zz__ucs2z00,
		BgL_bgl_string1518za700za7za7_1551za7, "&integer->ucs2-ur", 17);
	      DEFINE_STRING(BGl_string1519z00zz__ucs2z00,
		BgL_bgl_string1519za700za7za7_1552za7, "&ucs2->integer", 14);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_ucs2zd2alphabeticzf3zd2envzf3zz__ucs2z00,
		BgL_bgl_za762ucs2za7d2alphab1553z00,
		BGl_z62ucs2zd2alphabeticzf3z43zz__ucs2z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1520z00zz__ucs2z00,
		BgL_bgl_string1520za700za7za7_1554za7, "&char->ucs2", 11);
	      DEFINE_STRING(BGl_string1521z00zz__ucs2z00,
		BgL_bgl_string1521za700za7za7_1555za7, "bchar", 5);
	      DEFINE_STRING(BGl_string1523z00zz__ucs2z00,
		BgL_bgl_string1523za700za7za7_1556za7, "ucs2->char", 10);
	      DEFINE_STRING(BGl_string1524z00zz__ucs2z00,
		BgL_bgl_string1524za700za7za7_1557za7,
		"UCS-2 character out of ISO-LATIN-1 range", 40);
	      DEFINE_STRING(BGl_string1525z00zz__ucs2z00,
		BgL_bgl_string1525za700za7za7_1558za7, "&ucs2->char", 11);
	      DEFINE_STRING(BGl_string1526z00zz__ucs2z00,
		BgL_bgl_string1526za700za7za7_1559za7, "__ucs2", 6);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_ucs2zd2lowerzd2casezf3zd2envz21zz__ucs2z00,
		BgL_bgl_za762ucs2za7d2lowerza71560za7,
		BGl_z62ucs2zd2lowerzd2casezf3z91zz__ucs2z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ucs2zd2ze3charzd2envze3zz__ucs2z00,
		BgL_bgl_za762ucs2za7d2za7e3cha1561za7, BGl_z62ucs2zd2ze3charz53zz__ucs2z00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ucs2zd3zf3zd2envzf2zz__ucs2z00,
		BgL_bgl_za762ucs2za7d3za7f3za7421562z00, BGl_z62ucs2zd3zf3z42zz__ucs2z00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ucs2zd2cizd3zf3zd2envz20zz__ucs2z00,
		BgL_bgl_za762ucs2za7d2ciza7d3za71563z00,
		BGl_z62ucs2zd2cizd3zf3z90zz__ucs2z00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ucs2zd2cize3zd3zf3zd2envzc3zz__ucs2z00,
		BgL_bgl_za762ucs2za7d2ciza7e3za71564z00,
		BGl_z62ucs2zd2cize3zd3zf3z73zz__ucs2z00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_integerzd2ze3ucs2zd2envze3zz__ucs2z00,
		BgL_bgl_za762integerza7d2za7e31565za7,
		BGl_z62integerzd2ze3ucs2z53zz__ucs2z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1493z00zz__ucs2z00,
		BgL_bgl_string1493za700za7za7_1566za7, "/tmp/bigloo/runtime/Llib/ucs2.scm",
		33);
	      DEFINE_STRING(BGl_string1494z00zz__ucs2z00,
		BgL_bgl_string1494za700za7za7_1567za7, "&ucs2=?", 7);
	      DEFINE_STRING(BGl_string1495z00zz__ucs2z00,
		BgL_bgl_string1495za700za7za7_1568za7, "bucs2", 5);
	      DEFINE_STRING(BGl_string1496z00zz__ucs2z00,
		BgL_bgl_string1496za700za7za7_1569za7, "&ucs2<?", 7);
	      DEFINE_STRING(BGl_string1497z00zz__ucs2z00,
		BgL_bgl_string1497za700za7za7_1570za7, "&ucs2>?", 7);
	      DEFINE_STRING(BGl_string1498z00zz__ucs2z00,
		BgL_bgl_string1498za700za7za7_1571za7, "&ucs2<=?", 8);
	      DEFINE_STRING(BGl_string1499z00zz__ucs2z00,
		BgL_bgl_string1499za700za7za7_1572za7, "&ucs2>=?", 8);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ucs2zd2ze3integerzd2envze3zz__ucs2z00,
		BgL_bgl_za762ucs2za7d2za7e3int1573za7,
		BGl_z62ucs2zd2ze3integerz53zz__ucs2z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ucs2zd2downcasezd2envz00zz__ucs2z00,
		BgL_bgl_za762ucs2za7d2downca1574z00, BGl_z62ucs2zd2downcasezb0zz__ucs2z00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_charzd2ze3ucs2zd2envze3zz__ucs2z00,
		BgL_bgl_za762charza7d2za7e3ucs1575za7, BGl_z62charzd2ze3ucs2z53zz__ucs2z00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ucs2ze3zf3zd2envzc2zz__ucs2z00,
		BgL_bgl_za762ucs2za7e3za7f3za7721576z00, BGl_z62ucs2ze3zf3z72zz__ucs2z00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_ucs2zd2upperzd2casezf3zd2envz21zz__ucs2z00,
		BgL_bgl_za762ucs2za7d2upperza71577za7,
		BGl_z62ucs2zd2upperzd2casezf3z91zz__ucs2z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ucs2zd2cizc3zd3zf3zd2envze3zz__ucs2z00,
		BgL_bgl_za762ucs2za7d2ciza7c3za71578z00,
		BGl_z62ucs2zd2cizc3zd3zf3z53zz__ucs2z00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ucs2zd2cize3zf3zd2envz10zz__ucs2z00,
		BgL_bgl_za762ucs2za7d2ciza7e3za71579z00,
		BGl_z62ucs2zd2cize3zf3za0zz__ucs2z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_ucs2zd2whitespacezf3zd2envzf3zz__ucs2z00,
		BgL_bgl_za762ucs2za7d2whites1580z00,
		BGl_z62ucs2zd2whitespacezf3z43zz__ucs2z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ucs2ze3zd3zf3zd2envz11zz__ucs2z00,
		BgL_bgl_za762ucs2za7e3za7d3za7f31581z00, BGl_z62ucs2ze3zd3zf3za1zz__ucs2z00,
		0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__ucs2z00));
		     ADD_ROOT((void *) (&BGl_symbol1512z00zz__ucs2z00));
		     ADD_ROOT((void *) (&BGl_symbol1522z00zz__ucs2z00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__ucs2z00(long
		BgL_checksumz00_1257, char *BgL_fromz00_1258)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__ucs2z00))
				{
					BGl_requirezd2initializa7ationz75zz__ucs2z00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__ucs2z00();
					BGl_cnstzd2initzd2zz__ucs2z00();
					return BGl_importedzd2moduleszd2initz00zz__ucs2z00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__ucs2z00(void)
	{
		{	/* Llib/ucs2.scm 14 */
			BGl_symbol1512z00zz__ucs2z00 =
				bstring_to_symbol(BGl_string1513z00zz__ucs2z00);
			return (BGl_symbol1522z00zz__ucs2z00 =
				bstring_to_symbol(BGl_string1523z00zz__ucs2z00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__ucs2z00(void)
	{
		{	/* Llib/ucs2.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* ucs2? */
	BGL_EXPORTED_DEF bool_t BGl_ucs2zf3zf3zz__ucs2z00(obj_t BgL_objz00_3)
	{
		{	/* Llib/ucs2.scm 146 */
			return UCS2P(BgL_objz00_3);
		}

	}



/* &ucs2? */
	obj_t BGl_z62ucs2zf3z91zz__ucs2z00(obj_t BgL_envz00_1137,
		obj_t BgL_objz00_1138)
	{
		{	/* Llib/ucs2.scm 146 */
			return BBOOL(BGl_ucs2zf3zf3zz__ucs2z00(BgL_objz00_1138));
		}

	}



/* ucs2=? */
	BGL_EXPORTED_DEF bool_t BGl_ucs2zd3zf3z20zz__ucs2z00(ucs2_t BgL_ucs2az00_4,
		ucs2_t BgL_ucs2bz00_5)
	{
		{	/* Llib/ucs2.scm 152 */
			return (BgL_ucs2az00_4 == BgL_ucs2bz00_5);
		}

	}



/* &ucs2=? */
	obj_t BGl_z62ucs2zd3zf3z42zz__ucs2z00(obj_t BgL_envz00_1139,
		obj_t BgL_ucs2az00_1140, obj_t BgL_ucs2bz00_1141)
	{
		{	/* Llib/ucs2.scm 152 */
			{	/* Llib/ucs2.scm 153 */
				bool_t BgL_tmpz00_1272;

				{	/* Llib/ucs2.scm 153 */
					ucs2_t BgL_auxz00_1282;
					ucs2_t BgL_auxz00_1273;

					{	/* Llib/ucs2.scm 153 */
						obj_t BgL_tmpz00_1283;

						if (UCS2P(BgL_ucs2bz00_1141))
							{	/* Llib/ucs2.scm 153 */
								BgL_tmpz00_1283 = BgL_ucs2bz00_1141;
							}
						else
							{
								obj_t BgL_auxz00_1286;

								BgL_auxz00_1286 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1493z00zz__ucs2z00,
									BINT(6361L), BGl_string1494z00zz__ucs2z00,
									BGl_string1495z00zz__ucs2z00, BgL_ucs2bz00_1141);
								FAILURE(BgL_auxz00_1286, BFALSE, BFALSE);
							}
						BgL_auxz00_1282 = CUCS2(BgL_tmpz00_1283);
					}
					{	/* Llib/ucs2.scm 153 */
						obj_t BgL_tmpz00_1274;

						if (UCS2P(BgL_ucs2az00_1140))
							{	/* Llib/ucs2.scm 153 */
								BgL_tmpz00_1274 = BgL_ucs2az00_1140;
							}
						else
							{
								obj_t BgL_auxz00_1277;

								BgL_auxz00_1277 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1493z00zz__ucs2z00,
									BINT(6361L), BGl_string1494z00zz__ucs2z00,
									BGl_string1495z00zz__ucs2z00, BgL_ucs2az00_1140);
								FAILURE(BgL_auxz00_1277, BFALSE, BFALSE);
							}
						BgL_auxz00_1273 = CUCS2(BgL_tmpz00_1274);
					}
					BgL_tmpz00_1272 =
						BGl_ucs2zd3zf3z20zz__ucs2z00(BgL_auxz00_1273, BgL_auxz00_1282);
				}
				return BBOOL(BgL_tmpz00_1272);
			}
		}

	}



/* ucs2<? */
	BGL_EXPORTED_DEF bool_t BGl_ucs2zc3zf3z30zz__ucs2z00(ucs2_t BgL_ucs2az00_6,
		ucs2_t BgL_ucs2bz00_7)
	{
		{	/* Llib/ucs2.scm 158 */
			return (BgL_ucs2az00_6 < BgL_ucs2bz00_7);
		}

	}



/* &ucs2<? */
	obj_t BGl_z62ucs2zc3zf3z52zz__ucs2z00(obj_t BgL_envz00_1142,
		obj_t BgL_ucs2az00_1143, obj_t BgL_ucs2bz00_1144)
	{
		{	/* Llib/ucs2.scm 158 */
			{	/* Llib/ucs2.scm 159 */
				bool_t BgL_tmpz00_1294;

				{	/* Llib/ucs2.scm 159 */
					ucs2_t BgL_auxz00_1304;
					ucs2_t BgL_auxz00_1295;

					{	/* Llib/ucs2.scm 159 */
						obj_t BgL_tmpz00_1305;

						if (UCS2P(BgL_ucs2bz00_1144))
							{	/* Llib/ucs2.scm 159 */
								BgL_tmpz00_1305 = BgL_ucs2bz00_1144;
							}
						else
							{
								obj_t BgL_auxz00_1308;

								BgL_auxz00_1308 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1493z00zz__ucs2z00,
									BINT(6647L), BGl_string1496z00zz__ucs2z00,
									BGl_string1495z00zz__ucs2z00, BgL_ucs2bz00_1144);
								FAILURE(BgL_auxz00_1308, BFALSE, BFALSE);
							}
						BgL_auxz00_1304 = CUCS2(BgL_tmpz00_1305);
					}
					{	/* Llib/ucs2.scm 159 */
						obj_t BgL_tmpz00_1296;

						if (UCS2P(BgL_ucs2az00_1143))
							{	/* Llib/ucs2.scm 159 */
								BgL_tmpz00_1296 = BgL_ucs2az00_1143;
							}
						else
							{
								obj_t BgL_auxz00_1299;

								BgL_auxz00_1299 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1493z00zz__ucs2z00,
									BINT(6647L), BGl_string1496z00zz__ucs2z00,
									BGl_string1495z00zz__ucs2z00, BgL_ucs2az00_1143);
								FAILURE(BgL_auxz00_1299, BFALSE, BFALSE);
							}
						BgL_auxz00_1295 = CUCS2(BgL_tmpz00_1296);
					}
					BgL_tmpz00_1294 =
						BGl_ucs2zc3zf3z30zz__ucs2z00(BgL_auxz00_1295, BgL_auxz00_1304);
				}
				return BBOOL(BgL_tmpz00_1294);
			}
		}

	}



/* ucs2>? */
	BGL_EXPORTED_DEF bool_t BGl_ucs2ze3zf3z10zz__ucs2z00(ucs2_t BgL_ucs2az00_8,
		ucs2_t BgL_ucs2bz00_9)
	{
		{	/* Llib/ucs2.scm 164 */
			return (BgL_ucs2az00_8 > BgL_ucs2bz00_9);
		}

	}



/* &ucs2>? */
	obj_t BGl_z62ucs2ze3zf3z72zz__ucs2z00(obj_t BgL_envz00_1145,
		obj_t BgL_ucs2az00_1146, obj_t BgL_ucs2bz00_1147)
	{
		{	/* Llib/ucs2.scm 164 */
			{	/* Llib/ucs2.scm 165 */
				bool_t BgL_tmpz00_1316;

				{	/* Llib/ucs2.scm 165 */
					ucs2_t BgL_auxz00_1326;
					ucs2_t BgL_auxz00_1317;

					{	/* Llib/ucs2.scm 165 */
						obj_t BgL_tmpz00_1327;

						if (UCS2P(BgL_ucs2bz00_1147))
							{	/* Llib/ucs2.scm 165 */
								BgL_tmpz00_1327 = BgL_ucs2bz00_1147;
							}
						else
							{
								obj_t BgL_auxz00_1330;

								BgL_auxz00_1330 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1493z00zz__ucs2z00,
									BINT(6934L), BGl_string1497z00zz__ucs2z00,
									BGl_string1495z00zz__ucs2z00, BgL_ucs2bz00_1147);
								FAILURE(BgL_auxz00_1330, BFALSE, BFALSE);
							}
						BgL_auxz00_1326 = CUCS2(BgL_tmpz00_1327);
					}
					{	/* Llib/ucs2.scm 165 */
						obj_t BgL_tmpz00_1318;

						if (UCS2P(BgL_ucs2az00_1146))
							{	/* Llib/ucs2.scm 165 */
								BgL_tmpz00_1318 = BgL_ucs2az00_1146;
							}
						else
							{
								obj_t BgL_auxz00_1321;

								BgL_auxz00_1321 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1493z00zz__ucs2z00,
									BINT(6934L), BGl_string1497z00zz__ucs2z00,
									BGl_string1495z00zz__ucs2z00, BgL_ucs2az00_1146);
								FAILURE(BgL_auxz00_1321, BFALSE, BFALSE);
							}
						BgL_auxz00_1317 = CUCS2(BgL_tmpz00_1318);
					}
					BgL_tmpz00_1316 =
						BGl_ucs2ze3zf3z10zz__ucs2z00(BgL_auxz00_1317, BgL_auxz00_1326);
				}
				return BBOOL(BgL_tmpz00_1316);
			}
		}

	}



/* ucs2<=? */
	BGL_EXPORTED_DEF bool_t BGl_ucs2zc3zd3zf3ze3zz__ucs2z00(ucs2_t
		BgL_ucs2az00_10, ucs2_t BgL_ucs2bz00_11)
	{
		{	/* Llib/ucs2.scm 170 */
			return (BgL_ucs2az00_10 <= BgL_ucs2bz00_11);
		}

	}



/* &ucs2<=? */
	obj_t BGl_z62ucs2zc3zd3zf3z81zz__ucs2z00(obj_t BgL_envz00_1148,
		obj_t BgL_ucs2az00_1149, obj_t BgL_ucs2bz00_1150)
	{
		{	/* Llib/ucs2.scm 170 */
			{	/* Llib/ucs2.scm 171 */
				bool_t BgL_tmpz00_1338;

				{	/* Llib/ucs2.scm 171 */
					ucs2_t BgL_auxz00_1348;
					ucs2_t BgL_auxz00_1339;

					{	/* Llib/ucs2.scm 171 */
						obj_t BgL_tmpz00_1349;

						if (UCS2P(BgL_ucs2bz00_1150))
							{	/* Llib/ucs2.scm 171 */
								BgL_tmpz00_1349 = BgL_ucs2bz00_1150;
							}
						else
							{
								obj_t BgL_auxz00_1352;

								BgL_auxz00_1352 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1493z00zz__ucs2z00,
									BINT(7221L), BGl_string1498z00zz__ucs2z00,
									BGl_string1495z00zz__ucs2z00, BgL_ucs2bz00_1150);
								FAILURE(BgL_auxz00_1352, BFALSE, BFALSE);
							}
						BgL_auxz00_1348 = CUCS2(BgL_tmpz00_1349);
					}
					{	/* Llib/ucs2.scm 171 */
						obj_t BgL_tmpz00_1340;

						if (UCS2P(BgL_ucs2az00_1149))
							{	/* Llib/ucs2.scm 171 */
								BgL_tmpz00_1340 = BgL_ucs2az00_1149;
							}
						else
							{
								obj_t BgL_auxz00_1343;

								BgL_auxz00_1343 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1493z00zz__ucs2z00,
									BINT(7221L), BGl_string1498z00zz__ucs2z00,
									BGl_string1495z00zz__ucs2z00, BgL_ucs2az00_1149);
								FAILURE(BgL_auxz00_1343, BFALSE, BFALSE);
							}
						BgL_auxz00_1339 = CUCS2(BgL_tmpz00_1340);
					}
					BgL_tmpz00_1338 =
						BGl_ucs2zc3zd3zf3ze3zz__ucs2z00(BgL_auxz00_1339, BgL_auxz00_1348);
				}
				return BBOOL(BgL_tmpz00_1338);
			}
		}

	}



/* ucs2>=? */
	BGL_EXPORTED_DEF bool_t BGl_ucs2ze3zd3zf3zc3zz__ucs2z00(ucs2_t
		BgL_ucs2az00_12, ucs2_t BgL_ucs2bz00_13)
	{
		{	/* Llib/ucs2.scm 176 */
			return (BgL_ucs2az00_12 >= BgL_ucs2bz00_13);
		}

	}



/* &ucs2>=? */
	obj_t BGl_z62ucs2ze3zd3zf3za1zz__ucs2z00(obj_t BgL_envz00_1151,
		obj_t BgL_ucs2az00_1152, obj_t BgL_ucs2bz00_1153)
	{
		{	/* Llib/ucs2.scm 176 */
			{	/* Llib/ucs2.scm 177 */
				bool_t BgL_tmpz00_1360;

				{	/* Llib/ucs2.scm 177 */
					ucs2_t BgL_auxz00_1370;
					ucs2_t BgL_auxz00_1361;

					{	/* Llib/ucs2.scm 177 */
						obj_t BgL_tmpz00_1371;

						if (UCS2P(BgL_ucs2bz00_1153))
							{	/* Llib/ucs2.scm 177 */
								BgL_tmpz00_1371 = BgL_ucs2bz00_1153;
							}
						else
							{
								obj_t BgL_auxz00_1374;

								BgL_auxz00_1374 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1493z00zz__ucs2z00,
									BINT(7510L), BGl_string1499z00zz__ucs2z00,
									BGl_string1495z00zz__ucs2z00, BgL_ucs2bz00_1153);
								FAILURE(BgL_auxz00_1374, BFALSE, BFALSE);
							}
						BgL_auxz00_1370 = CUCS2(BgL_tmpz00_1371);
					}
					{	/* Llib/ucs2.scm 177 */
						obj_t BgL_tmpz00_1362;

						if (UCS2P(BgL_ucs2az00_1152))
							{	/* Llib/ucs2.scm 177 */
								BgL_tmpz00_1362 = BgL_ucs2az00_1152;
							}
						else
							{
								obj_t BgL_auxz00_1365;

								BgL_auxz00_1365 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1493z00zz__ucs2z00,
									BINT(7510L), BGl_string1499z00zz__ucs2z00,
									BGl_string1495z00zz__ucs2z00, BgL_ucs2az00_1152);
								FAILURE(BgL_auxz00_1365, BFALSE, BFALSE);
							}
						BgL_auxz00_1361 = CUCS2(BgL_tmpz00_1362);
					}
					BgL_tmpz00_1360 =
						BGl_ucs2ze3zd3zf3zc3zz__ucs2z00(BgL_auxz00_1361, BgL_auxz00_1370);
				}
				return BBOOL(BgL_tmpz00_1360);
			}
		}

	}



/* ucs2-ci=? */
	BGL_EXPORTED_DEF bool_t BGl_ucs2zd2cizd3zf3zf2zz__ucs2z00(ucs2_t
		BgL_ucs2az00_14, ucs2_t BgL_ucs2bz00_15)
	{
		{	/* Llib/ucs2.scm 182 */
			return (ucs2_toupper(BgL_ucs2az00_14) == ucs2_toupper(BgL_ucs2bz00_15));
		}

	}



/* &ucs2-ci=? */
	obj_t BGl_z62ucs2zd2cizd3zf3z90zz__ucs2z00(obj_t BgL_envz00_1154,
		obj_t BgL_ucs2az00_1155, obj_t BgL_ucs2bz00_1156)
	{
		{	/* Llib/ucs2.scm 182 */
			{	/* Llib/ucs2.scm 183 */
				bool_t BgL_tmpz00_1384;

				{	/* Llib/ucs2.scm 183 */
					ucs2_t BgL_auxz00_1394;
					ucs2_t BgL_auxz00_1385;

					{	/* Llib/ucs2.scm 183 */
						obj_t BgL_tmpz00_1395;

						if (UCS2P(BgL_ucs2bz00_1156))
							{	/* Llib/ucs2.scm 183 */
								BgL_tmpz00_1395 = BgL_ucs2bz00_1156;
							}
						else
							{
								obj_t BgL_auxz00_1398;

								BgL_auxz00_1398 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1493z00zz__ucs2z00,
									BINT(7808L), BGl_string1500z00zz__ucs2z00,
									BGl_string1495z00zz__ucs2z00, BgL_ucs2bz00_1156);
								FAILURE(BgL_auxz00_1398, BFALSE, BFALSE);
							}
						BgL_auxz00_1394 = CUCS2(BgL_tmpz00_1395);
					}
					{	/* Llib/ucs2.scm 183 */
						obj_t BgL_tmpz00_1386;

						if (UCS2P(BgL_ucs2az00_1155))
							{	/* Llib/ucs2.scm 183 */
								BgL_tmpz00_1386 = BgL_ucs2az00_1155;
							}
						else
							{
								obj_t BgL_auxz00_1389;

								BgL_auxz00_1389 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1493z00zz__ucs2z00,
									BINT(7808L), BGl_string1500z00zz__ucs2z00,
									BGl_string1495z00zz__ucs2z00, BgL_ucs2az00_1155);
								FAILURE(BgL_auxz00_1389, BFALSE, BFALSE);
							}
						BgL_auxz00_1385 = CUCS2(BgL_tmpz00_1386);
					}
					BgL_tmpz00_1384 =
						BGl_ucs2zd2cizd3zf3zf2zz__ucs2z00(BgL_auxz00_1385, BgL_auxz00_1394);
				}
				return BBOOL(BgL_tmpz00_1384);
			}
		}

	}



/* ucs2-ci<? */
	BGL_EXPORTED_DEF bool_t BGl_ucs2zd2cizc3zf3ze2zz__ucs2z00(ucs2_t
		BgL_ucs2az00_16, ucs2_t BgL_ucs2bz00_17)
	{
		{	/* Llib/ucs2.scm 188 */
			return (ucs2_toupper(BgL_ucs2az00_16) < ucs2_toupper(BgL_ucs2bz00_17));
		}

	}



/* &ucs2-ci<? */
	obj_t BGl_z62ucs2zd2cizc3zf3z80zz__ucs2z00(obj_t BgL_envz00_1157,
		obj_t BgL_ucs2az00_1158, obj_t BgL_ucs2bz00_1159)
	{
		{	/* Llib/ucs2.scm 188 */
			{	/* Llib/ucs2.scm 189 */
				bool_t BgL_tmpz00_1408;

				{	/* Llib/ucs2.scm 189 */
					ucs2_t BgL_auxz00_1418;
					ucs2_t BgL_auxz00_1409;

					{	/* Llib/ucs2.scm 189 */
						obj_t BgL_tmpz00_1419;

						if (UCS2P(BgL_ucs2bz00_1159))
							{	/* Llib/ucs2.scm 189 */
								BgL_tmpz00_1419 = BgL_ucs2bz00_1159;
							}
						else
							{
								obj_t BgL_auxz00_1422;

								BgL_auxz00_1422 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1493z00zz__ucs2z00,
									BINT(8123L), BGl_string1501z00zz__ucs2z00,
									BGl_string1495z00zz__ucs2z00, BgL_ucs2bz00_1159);
								FAILURE(BgL_auxz00_1422, BFALSE, BFALSE);
							}
						BgL_auxz00_1418 = CUCS2(BgL_tmpz00_1419);
					}
					{	/* Llib/ucs2.scm 189 */
						obj_t BgL_tmpz00_1410;

						if (UCS2P(BgL_ucs2az00_1158))
							{	/* Llib/ucs2.scm 189 */
								BgL_tmpz00_1410 = BgL_ucs2az00_1158;
							}
						else
							{
								obj_t BgL_auxz00_1413;

								BgL_auxz00_1413 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1493z00zz__ucs2z00,
									BINT(8123L), BGl_string1501z00zz__ucs2z00,
									BGl_string1495z00zz__ucs2z00, BgL_ucs2az00_1158);
								FAILURE(BgL_auxz00_1413, BFALSE, BFALSE);
							}
						BgL_auxz00_1409 = CUCS2(BgL_tmpz00_1410);
					}
					BgL_tmpz00_1408 =
						BGl_ucs2zd2cizc3zf3ze2zz__ucs2z00(BgL_auxz00_1409, BgL_auxz00_1418);
				}
				return BBOOL(BgL_tmpz00_1408);
			}
		}

	}



/* ucs2-ci>? */
	BGL_EXPORTED_DEF bool_t BGl_ucs2zd2cize3zf3zc2zz__ucs2z00(ucs2_t
		BgL_ucs2az00_18, ucs2_t BgL_ucs2bz00_19)
	{
		{	/* Llib/ucs2.scm 194 */
			return (ucs2_toupper(BgL_ucs2az00_18) > ucs2_toupper(BgL_ucs2bz00_19));
		}

	}



/* &ucs2-ci>? */
	obj_t BGl_z62ucs2zd2cize3zf3za0zz__ucs2z00(obj_t BgL_envz00_1160,
		obj_t BgL_ucs2az00_1161, obj_t BgL_ucs2bz00_1162)
	{
		{	/* Llib/ucs2.scm 194 */
			{	/* Llib/ucs2.scm 195 */
				bool_t BgL_tmpz00_1432;

				{	/* Llib/ucs2.scm 195 */
					ucs2_t BgL_auxz00_1442;
					ucs2_t BgL_auxz00_1433;

					{	/* Llib/ucs2.scm 195 */
						obj_t BgL_tmpz00_1443;

						if (UCS2P(BgL_ucs2bz00_1162))
							{	/* Llib/ucs2.scm 195 */
								BgL_tmpz00_1443 = BgL_ucs2bz00_1162;
							}
						else
							{
								obj_t BgL_auxz00_1446;

								BgL_auxz00_1446 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1493z00zz__ucs2z00,
									BINT(8440L), BGl_string1502z00zz__ucs2z00,
									BGl_string1495z00zz__ucs2z00, BgL_ucs2bz00_1162);
								FAILURE(BgL_auxz00_1446, BFALSE, BFALSE);
							}
						BgL_auxz00_1442 = CUCS2(BgL_tmpz00_1443);
					}
					{	/* Llib/ucs2.scm 195 */
						obj_t BgL_tmpz00_1434;

						if (UCS2P(BgL_ucs2az00_1161))
							{	/* Llib/ucs2.scm 195 */
								BgL_tmpz00_1434 = BgL_ucs2az00_1161;
							}
						else
							{
								obj_t BgL_auxz00_1437;

								BgL_auxz00_1437 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1493z00zz__ucs2z00,
									BINT(8440L), BGl_string1502z00zz__ucs2z00,
									BGl_string1495z00zz__ucs2z00, BgL_ucs2az00_1161);
								FAILURE(BgL_auxz00_1437, BFALSE, BFALSE);
							}
						BgL_auxz00_1433 = CUCS2(BgL_tmpz00_1434);
					}
					BgL_tmpz00_1432 =
						BGl_ucs2zd2cize3zf3zc2zz__ucs2z00(BgL_auxz00_1433, BgL_auxz00_1442);
				}
				return BBOOL(BgL_tmpz00_1432);
			}
		}

	}



/* ucs2-ci<=? */
	BGL_EXPORTED_DEF bool_t BGl_ucs2zd2cizc3zd3zf3z31zz__ucs2z00(ucs2_t
		BgL_ucs2az00_20, ucs2_t BgL_ucs2bz00_21)
	{
		{	/* Llib/ucs2.scm 200 */
			return (ucs2_toupper(BgL_ucs2az00_20) <= ucs2_toupper(BgL_ucs2bz00_21));
		}

	}



/* &ucs2-ci<=? */
	obj_t BGl_z62ucs2zd2cizc3zd3zf3z53zz__ucs2z00(obj_t BgL_envz00_1163,
		obj_t BgL_ucs2az00_1164, obj_t BgL_ucs2bz00_1165)
	{
		{	/* Llib/ucs2.scm 200 */
			{	/* Llib/ucs2.scm 201 */
				bool_t BgL_tmpz00_1456;

				{	/* Llib/ucs2.scm 201 */
					ucs2_t BgL_auxz00_1466;
					ucs2_t BgL_auxz00_1457;

					{	/* Llib/ucs2.scm 201 */
						obj_t BgL_tmpz00_1467;

						if (UCS2P(BgL_ucs2bz00_1165))
							{	/* Llib/ucs2.scm 201 */
								BgL_tmpz00_1467 = BgL_ucs2bz00_1165;
							}
						else
							{
								obj_t BgL_auxz00_1470;

								BgL_auxz00_1470 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1493z00zz__ucs2z00,
									BINT(8757L), BGl_string1503z00zz__ucs2z00,
									BGl_string1495z00zz__ucs2z00, BgL_ucs2bz00_1165);
								FAILURE(BgL_auxz00_1470, BFALSE, BFALSE);
							}
						BgL_auxz00_1466 = CUCS2(BgL_tmpz00_1467);
					}
					{	/* Llib/ucs2.scm 201 */
						obj_t BgL_tmpz00_1458;

						if (UCS2P(BgL_ucs2az00_1164))
							{	/* Llib/ucs2.scm 201 */
								BgL_tmpz00_1458 = BgL_ucs2az00_1164;
							}
						else
							{
								obj_t BgL_auxz00_1461;

								BgL_auxz00_1461 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1493z00zz__ucs2z00,
									BINT(8757L), BGl_string1503z00zz__ucs2z00,
									BGl_string1495z00zz__ucs2z00, BgL_ucs2az00_1164);
								FAILURE(BgL_auxz00_1461, BFALSE, BFALSE);
							}
						BgL_auxz00_1457 = CUCS2(BgL_tmpz00_1458);
					}
					BgL_tmpz00_1456 =
						BGl_ucs2zd2cizc3zd3zf3z31zz__ucs2z00(BgL_auxz00_1457,
						BgL_auxz00_1466);
				}
				return BBOOL(BgL_tmpz00_1456);
			}
		}

	}



/* ucs2-ci>=? */
	BGL_EXPORTED_DEF bool_t BGl_ucs2zd2cize3zd3zf3z11zz__ucs2z00(ucs2_t
		BgL_ucs2az00_22, ucs2_t BgL_ucs2bz00_23)
	{
		{	/* Llib/ucs2.scm 206 */
			return (ucs2_toupper(BgL_ucs2az00_22) >= ucs2_toupper(BgL_ucs2bz00_23));
		}

	}



/* &ucs2-ci>=? */
	obj_t BGl_z62ucs2zd2cize3zd3zf3z73zz__ucs2z00(obj_t BgL_envz00_1166,
		obj_t BgL_ucs2az00_1167, obj_t BgL_ucs2bz00_1168)
	{
		{	/* Llib/ucs2.scm 206 */
			{	/* Llib/ucs2.scm 207 */
				bool_t BgL_tmpz00_1480;

				{	/* Llib/ucs2.scm 207 */
					ucs2_t BgL_auxz00_1490;
					ucs2_t BgL_auxz00_1481;

					{	/* Llib/ucs2.scm 207 */
						obj_t BgL_tmpz00_1491;

						if (UCS2P(BgL_ucs2bz00_1168))
							{	/* Llib/ucs2.scm 207 */
								BgL_tmpz00_1491 = BgL_ucs2bz00_1168;
							}
						else
							{
								obj_t BgL_auxz00_1494;

								BgL_auxz00_1494 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1493z00zz__ucs2z00,
									BINT(9074L), BGl_string1504z00zz__ucs2z00,
									BGl_string1495z00zz__ucs2z00, BgL_ucs2bz00_1168);
								FAILURE(BgL_auxz00_1494, BFALSE, BFALSE);
							}
						BgL_auxz00_1490 = CUCS2(BgL_tmpz00_1491);
					}
					{	/* Llib/ucs2.scm 207 */
						obj_t BgL_tmpz00_1482;

						if (UCS2P(BgL_ucs2az00_1167))
							{	/* Llib/ucs2.scm 207 */
								BgL_tmpz00_1482 = BgL_ucs2az00_1167;
							}
						else
							{
								obj_t BgL_auxz00_1485;

								BgL_auxz00_1485 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1493z00zz__ucs2z00,
									BINT(9074L), BGl_string1504z00zz__ucs2z00,
									BGl_string1495z00zz__ucs2z00, BgL_ucs2az00_1167);
								FAILURE(BgL_auxz00_1485, BFALSE, BFALSE);
							}
						BgL_auxz00_1481 = CUCS2(BgL_tmpz00_1482);
					}
					BgL_tmpz00_1480 =
						BGl_ucs2zd2cize3zd3zf3z11zz__ucs2z00(BgL_auxz00_1481,
						BgL_auxz00_1490);
				}
				return BBOOL(BgL_tmpz00_1480);
			}
		}

	}



/* ucs2-alphabetic? */
	BGL_EXPORTED_DEF bool_t BGl_ucs2zd2alphabeticzf3z21zz__ucs2z00(ucs2_t
		BgL_ucs2z00_24)
	{
		{	/* Llib/ucs2.scm 212 */
			BGL_TAIL return ucs2_letterp(BgL_ucs2z00_24);
		}

	}



/* &ucs2-alphabetic? */
	obj_t BGl_z62ucs2zd2alphabeticzf3z43zz__ucs2z00(obj_t BgL_envz00_1169,
		obj_t BgL_ucs2z00_1170)
	{
		{	/* Llib/ucs2.scm 212 */
			{	/* Llib/ucs2.scm 213 */
				bool_t BgL_tmpz00_1502;

				{	/* Llib/ucs2.scm 213 */
					ucs2_t BgL_auxz00_1503;

					{	/* Llib/ucs2.scm 213 */
						obj_t BgL_tmpz00_1504;

						if (UCS2P(BgL_ucs2z00_1170))
							{	/* Llib/ucs2.scm 213 */
								BgL_tmpz00_1504 = BgL_ucs2z00_1170;
							}
						else
							{
								obj_t BgL_auxz00_1507;

								BgL_auxz00_1507 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1493z00zz__ucs2z00,
									BINT(9381L), BGl_string1505z00zz__ucs2z00,
									BGl_string1495z00zz__ucs2z00, BgL_ucs2z00_1170);
								FAILURE(BgL_auxz00_1507, BFALSE, BFALSE);
							}
						BgL_auxz00_1503 = CUCS2(BgL_tmpz00_1504);
					}
					BgL_tmpz00_1502 =
						BGl_ucs2zd2alphabeticzf3z21zz__ucs2z00(BgL_auxz00_1503);
				}
				return BBOOL(BgL_tmpz00_1502);
			}
		}

	}



/* ucs2-numeric? */
	BGL_EXPORTED_DEF bool_t BGl_ucs2zd2numericzf3z21zz__ucs2z00(ucs2_t
		BgL_ucs2z00_25)
	{
		{	/* Llib/ucs2.scm 218 */
			BGL_TAIL return ucs2_digitp(BgL_ucs2z00_25);
		}

	}



/* &ucs2-numeric? */
	obj_t BGl_z62ucs2zd2numericzf3z43zz__ucs2z00(obj_t BgL_envz00_1171,
		obj_t BgL_ucs2z00_1172)
	{
		{	/* Llib/ucs2.scm 218 */
			{	/* Llib/ucs2.scm 219 */
				bool_t BgL_tmpz00_1515;

				{	/* Llib/ucs2.scm 219 */
					ucs2_t BgL_auxz00_1516;

					{	/* Llib/ucs2.scm 219 */
						obj_t BgL_tmpz00_1517;

						if (UCS2P(BgL_ucs2z00_1172))
							{	/* Llib/ucs2.scm 219 */
								BgL_tmpz00_1517 = BgL_ucs2z00_1172;
							}
						else
							{
								obj_t BgL_auxz00_1520;

								BgL_auxz00_1520 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1493z00zz__ucs2z00,
									BINT(9666L), BGl_string1506z00zz__ucs2z00,
									BGl_string1495z00zz__ucs2z00, BgL_ucs2z00_1172);
								FAILURE(BgL_auxz00_1520, BFALSE, BFALSE);
							}
						BgL_auxz00_1516 = CUCS2(BgL_tmpz00_1517);
					}
					BgL_tmpz00_1515 =
						BGl_ucs2zd2numericzf3z21zz__ucs2z00(BgL_auxz00_1516);
				}
				return BBOOL(BgL_tmpz00_1515);
			}
		}

	}



/* ucs2-whitespace? */
	BGL_EXPORTED_DEF bool_t BGl_ucs2zd2whitespacezf3z21zz__ucs2z00(ucs2_t
		BgL_ucs2z00_26)
	{
		{	/* Llib/ucs2.scm 224 */
			BGL_TAIL return ucs2_whitespacep(BgL_ucs2z00_26);
		}

	}



/* &ucs2-whitespace? */
	obj_t BGl_z62ucs2zd2whitespacezf3z43zz__ucs2z00(obj_t BgL_envz00_1173,
		obj_t BgL_ucs2z00_1174)
	{
		{	/* Llib/ucs2.scm 224 */
			{	/* Llib/ucs2.scm 225 */
				bool_t BgL_tmpz00_1528;

				{	/* Llib/ucs2.scm 225 */
					ucs2_t BgL_auxz00_1529;

					{	/* Llib/ucs2.scm 225 */
						obj_t BgL_tmpz00_1530;

						if (UCS2P(BgL_ucs2z00_1174))
							{	/* Llib/ucs2.scm 225 */
								BgL_tmpz00_1530 = BgL_ucs2z00_1174;
							}
						else
							{
								obj_t BgL_auxz00_1533;

								BgL_auxz00_1533 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1493z00zz__ucs2z00,
									BINT(9953L), BGl_string1507z00zz__ucs2z00,
									BGl_string1495z00zz__ucs2z00, BgL_ucs2z00_1174);
								FAILURE(BgL_auxz00_1533, BFALSE, BFALSE);
							}
						BgL_auxz00_1529 = CUCS2(BgL_tmpz00_1530);
					}
					BgL_tmpz00_1528 =
						BGl_ucs2zd2whitespacezf3z21zz__ucs2z00(BgL_auxz00_1529);
				}
				return BBOOL(BgL_tmpz00_1528);
			}
		}

	}



/* ucs2-upper-case? */
	BGL_EXPORTED_DEF bool_t BGl_ucs2zd2upperzd2casezf3zf3zz__ucs2z00(ucs2_t
		BgL_ucs2z00_27)
	{
		{	/* Llib/ucs2.scm 230 */
			BGL_TAIL return ucs2_upperp(BgL_ucs2z00_27);
		}

	}



/* &ucs2-upper-case? */
	obj_t BGl_z62ucs2zd2upperzd2casezf3z91zz__ucs2z00(obj_t BgL_envz00_1175,
		obj_t BgL_ucs2z00_1176)
	{
		{	/* Llib/ucs2.scm 230 */
			{	/* Llib/ucs2.scm 231 */
				bool_t BgL_tmpz00_1541;

				{	/* Llib/ucs2.scm 231 */
					ucs2_t BgL_auxz00_1542;

					{	/* Llib/ucs2.scm 231 */
						obj_t BgL_tmpz00_1543;

						if (UCS2P(BgL_ucs2z00_1176))
							{	/* Llib/ucs2.scm 231 */
								BgL_tmpz00_1543 = BgL_ucs2z00_1176;
							}
						else
							{
								obj_t BgL_auxz00_1546;

								BgL_auxz00_1546 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1493z00zz__ucs2z00,
									BINT(10245L), BGl_string1508z00zz__ucs2z00,
									BGl_string1495z00zz__ucs2z00, BgL_ucs2z00_1176);
								FAILURE(BgL_auxz00_1546, BFALSE, BFALSE);
							}
						BgL_auxz00_1542 = CUCS2(BgL_tmpz00_1543);
					}
					BgL_tmpz00_1541 =
						BGl_ucs2zd2upperzd2casezf3zf3zz__ucs2z00(BgL_auxz00_1542);
				}
				return BBOOL(BgL_tmpz00_1541);
			}
		}

	}



/* ucs2-lower-case? */
	BGL_EXPORTED_DEF bool_t BGl_ucs2zd2lowerzd2casezf3zf3zz__ucs2z00(ucs2_t
		BgL_ucs2z00_28)
	{
		{	/* Llib/ucs2.scm 236 */
			BGL_TAIL return ucs2_lowerp(BgL_ucs2z00_28);
		}

	}



/* &ucs2-lower-case? */
	obj_t BGl_z62ucs2zd2lowerzd2casezf3z91zz__ucs2z00(obj_t BgL_envz00_1177,
		obj_t BgL_ucs2z00_1178)
	{
		{	/* Llib/ucs2.scm 236 */
			{	/* Llib/ucs2.scm 237 */
				bool_t BgL_tmpz00_1554;

				{	/* Llib/ucs2.scm 237 */
					ucs2_t BgL_auxz00_1555;

					{	/* Llib/ucs2.scm 237 */
						obj_t BgL_tmpz00_1556;

						if (UCS2P(BgL_ucs2z00_1178))
							{	/* Llib/ucs2.scm 237 */
								BgL_tmpz00_1556 = BgL_ucs2z00_1178;
							}
						else
							{
								obj_t BgL_auxz00_1559;

								BgL_auxz00_1559 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1493z00zz__ucs2z00,
									BINT(10532L), BGl_string1509z00zz__ucs2z00,
									BGl_string1495z00zz__ucs2z00, BgL_ucs2z00_1178);
								FAILURE(BgL_auxz00_1559, BFALSE, BFALSE);
							}
						BgL_auxz00_1555 = CUCS2(BgL_tmpz00_1556);
					}
					BgL_tmpz00_1554 =
						BGl_ucs2zd2lowerzd2casezf3zf3zz__ucs2z00(BgL_auxz00_1555);
				}
				return BBOOL(BgL_tmpz00_1554);
			}
		}

	}



/* ucs2-upcase */
	BGL_EXPORTED_DEF ucs2_t BGl_ucs2zd2upcasezd2zz__ucs2z00(ucs2_t BgL_ucs2z00_29)
	{
		{	/* Llib/ucs2.scm 242 */
			BGL_TAIL return ucs2_toupper(BgL_ucs2z00_29);
		}

	}



/* &ucs2-upcase */
	obj_t BGl_z62ucs2zd2upcasezb0zz__ucs2z00(obj_t BgL_envz00_1179,
		obj_t BgL_ucs2z00_1180)
	{
		{	/* Llib/ucs2.scm 242 */
			{	/* Llib/ucs2.scm 243 */
				ucs2_t BgL_tmpz00_1567;

				{	/* Llib/ucs2.scm 243 */
					ucs2_t BgL_auxz00_1568;

					{	/* Llib/ucs2.scm 243 */
						obj_t BgL_tmpz00_1569;

						if (UCS2P(BgL_ucs2z00_1180))
							{	/* Llib/ucs2.scm 243 */
								BgL_tmpz00_1569 = BgL_ucs2z00_1180;
							}
						else
							{
								obj_t BgL_auxz00_1572;

								BgL_auxz00_1572 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1493z00zz__ucs2z00,
									BINT(10814L), BGl_string1510z00zz__ucs2z00,
									BGl_string1495z00zz__ucs2z00, BgL_ucs2z00_1180);
								FAILURE(BgL_auxz00_1572, BFALSE, BFALSE);
							}
						BgL_auxz00_1568 = CUCS2(BgL_tmpz00_1569);
					}
					BgL_tmpz00_1567 = BGl_ucs2zd2upcasezd2zz__ucs2z00(BgL_auxz00_1568);
				}
				return BUCS2(BgL_tmpz00_1567);
			}
		}

	}



/* ucs2-downcase */
	BGL_EXPORTED_DEF ucs2_t BGl_ucs2zd2downcasezd2zz__ucs2z00(ucs2_t
		BgL_ucs2z00_30)
	{
		{	/* Llib/ucs2.scm 248 */
			BGL_TAIL return ucs2_tolower(BgL_ucs2z00_30);
		}

	}



/* &ucs2-downcase */
	obj_t BGl_z62ucs2zd2downcasezb0zz__ucs2z00(obj_t BgL_envz00_1181,
		obj_t BgL_ucs2z00_1182)
	{
		{	/* Llib/ucs2.scm 248 */
			{	/* Llib/ucs2.scm 249 */
				ucs2_t BgL_tmpz00_1580;

				{	/* Llib/ucs2.scm 249 */
					ucs2_t BgL_auxz00_1581;

					{	/* Llib/ucs2.scm 249 */
						obj_t BgL_tmpz00_1582;

						if (UCS2P(BgL_ucs2z00_1182))
							{	/* Llib/ucs2.scm 249 */
								BgL_tmpz00_1582 = BgL_ucs2z00_1182;
							}
						else
							{
								obj_t BgL_auxz00_1585;

								BgL_auxz00_1585 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1493z00zz__ucs2z00,
									BINT(11098L), BGl_string1511z00zz__ucs2z00,
									BGl_string1495z00zz__ucs2z00, BgL_ucs2z00_1182);
								FAILURE(BgL_auxz00_1585, BFALSE, BFALSE);
							}
						BgL_auxz00_1581 = CUCS2(BgL_tmpz00_1582);
					}
					BgL_tmpz00_1580 = BGl_ucs2zd2downcasezd2zz__ucs2z00(BgL_auxz00_1581);
				}
				return BUCS2(BgL_tmpz00_1580);
			}
		}

	}



/* integer->ucs2 */
	BGL_EXPORTED_DEF ucs2_t BGl_integerzd2ze3ucs2z31zz__ucs2z00(int BgL_intz00_31)
	{
		{	/* Llib/ucs2.scm 254 */
			{	/* Llib/ucs2.scm 255 */
				bool_t BgL_test1612z00_1592;

				if (((long) (BgL_intz00_31) >= 0L))
					{	/* Llib/ucs2.scm 255 */
						BgL_test1612z00_1592 = ((long) (BgL_intz00_31) < 65536L);
					}
				else
					{	/* Llib/ucs2.scm 255 */
						BgL_test1612z00_1592 = ((bool_t) 0);
					}
				if (BgL_test1612z00_1592)
					{	/* Llib/ucs2.scm 255 */
						if (ucs2_definedp(BgL_intz00_31))
							{	/* Llib/ucs2.scm 256 */
								return BGL_INT_TO_UCS2(BgL_intz00_31);
							}
						else
							{	/* Llib/ucs2.scm 256 */
								return
									CUCS2(BGl_errorz00zz__errorz00(BGl_symbol1512z00zz__ucs2z00,
										BGl_string1514z00zz__ucs2z00, BINT(BgL_intz00_31)));
							}
					}
				else
					{	/* Llib/ucs2.scm 255 */
						return
							CUCS2(BGl_errorz00zz__errorz00(BGl_symbol1512z00zz__ucs2z00,
								BGl_string1515z00zz__ucs2z00, BINT(BgL_intz00_31)));
					}
			}
		}

	}



/* &integer->ucs2 */
	obj_t BGl_z62integerzd2ze3ucs2z53zz__ucs2z00(obj_t BgL_envz00_1183,
		obj_t BgL_intz00_1184)
	{
		{	/* Llib/ucs2.scm 254 */
			{	/* Llib/ucs2.scm 255 */
				ucs2_t BgL_tmpz00_1607;

				{	/* Llib/ucs2.scm 255 */
					int BgL_auxz00_1608;

					{	/* Llib/ucs2.scm 255 */
						obj_t BgL_tmpz00_1609;

						if (INTEGERP(BgL_intz00_1184))
							{	/* Llib/ucs2.scm 255 */
								BgL_tmpz00_1609 = BgL_intz00_1184;
							}
						else
							{
								obj_t BgL_auxz00_1612;

								BgL_auxz00_1612 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1493z00zz__ucs2z00,
									BINT(11394L), BGl_string1516z00zz__ucs2z00,
									BGl_string1517z00zz__ucs2z00, BgL_intz00_1184);
								FAILURE(BgL_auxz00_1612, BFALSE, BFALSE);
							}
						BgL_auxz00_1608 = CINT(BgL_tmpz00_1609);
					}
					BgL_tmpz00_1607 =
						BGl_integerzd2ze3ucs2z31zz__ucs2z00(BgL_auxz00_1608);
				}
				return BUCS2(BgL_tmpz00_1607);
			}
		}

	}



/* integer->ucs2-ur */
	BGL_EXPORTED_DEF ucs2_t BGl_integerzd2ze3ucs2zd2urze3zz__ucs2z00(int
		BgL_intz00_32)
	{
		{	/* Llib/ucs2.scm 264 */
			return BGL_INT_TO_UCS2(BgL_intz00_32);
		}

	}



/* &integer->ucs2-ur */
	obj_t BGl_z62integerzd2ze3ucs2zd2urz81zz__ucs2z00(obj_t BgL_envz00_1185,
		obj_t BgL_intz00_1186)
	{
		{	/* Llib/ucs2.scm 264 */
			{	/* Llib/ucs2.scm 265 */
				ucs2_t BgL_tmpz00_1620;

				{	/* Llib/ucs2.scm 265 */
					int BgL_auxz00_1621;

					{	/* Llib/ucs2.scm 265 */
						obj_t BgL_tmpz00_1622;

						if (INTEGERP(BgL_intz00_1186))
							{	/* Llib/ucs2.scm 265 */
								BgL_tmpz00_1622 = BgL_intz00_1186;
							}
						else
							{
								obj_t BgL_auxz00_1625;

								BgL_auxz00_1625 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1493z00zz__ucs2z00,
									BINT(11890L), BGl_string1518z00zz__ucs2z00,
									BGl_string1517z00zz__ucs2z00, BgL_intz00_1186);
								FAILURE(BgL_auxz00_1625, BFALSE, BFALSE);
							}
						BgL_auxz00_1621 = CINT(BgL_tmpz00_1622);
					}
					BgL_tmpz00_1620 =
						BGl_integerzd2ze3ucs2zd2urze3zz__ucs2z00(BgL_auxz00_1621);
				}
				return BUCS2(BgL_tmpz00_1620);
			}
		}

	}



/* ucs2->integer */
	BGL_EXPORTED_DEF int BGl_ucs2zd2ze3integerz31zz__ucs2z00(ucs2_t
		BgL_ucs2z00_33)
	{
		{	/* Llib/ucs2.scm 270 */
			{	/* Llib/ucs2.scm 271 */
				obj_t BgL_tmpz00_1632;

				BgL_tmpz00_1632 = BUCS2(BgL_ucs2z00_33);
				return CUCS2(BgL_tmpz00_1632);
			}
		}

	}



/* &ucs2->integer */
	obj_t BGl_z62ucs2zd2ze3integerz53zz__ucs2z00(obj_t BgL_envz00_1187,
		obj_t BgL_ucs2z00_1188)
	{
		{	/* Llib/ucs2.scm 270 */
			{	/* Llib/ucs2.scm 271 */
				int BgL_tmpz00_1635;

				{	/* Llib/ucs2.scm 271 */
					ucs2_t BgL_auxz00_1636;

					{	/* Llib/ucs2.scm 271 */
						obj_t BgL_tmpz00_1637;

						if (UCS2P(BgL_ucs2z00_1188))
							{	/* Llib/ucs2.scm 271 */
								BgL_tmpz00_1637 = BgL_ucs2z00_1188;
							}
						else
							{
								obj_t BgL_auxz00_1640;

								BgL_auxz00_1640 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1493z00zz__ucs2z00,
									BINT(12186L), BGl_string1519z00zz__ucs2z00,
									BGl_string1495z00zz__ucs2z00, BgL_ucs2z00_1188);
								FAILURE(BgL_auxz00_1640, BFALSE, BFALSE);
							}
						BgL_auxz00_1636 = CUCS2(BgL_tmpz00_1637);
					}
					BgL_tmpz00_1635 =
						BGl_ucs2zd2ze3integerz31zz__ucs2z00(BgL_auxz00_1636);
				}
				return BINT(BgL_tmpz00_1635);
			}
		}

	}



/* char->ucs2 */
	BGL_EXPORTED_DEF ucs2_t BGl_charzd2ze3ucs2z31zz__ucs2z00(unsigned char
		BgL_charz00_34)
	{
		{	/* Llib/ucs2.scm 276 */
			{	/* Llib/ucs2.scm 265 */
				int BgL_tmpz00_1647;

				BgL_tmpz00_1647 = (int) (((unsigned char) (BgL_charz00_34)));
				return BGL_INT_TO_UCS2(BgL_tmpz00_1647);
			}
		}

	}



/* &char->ucs2 */
	obj_t BGl_z62charzd2ze3ucs2z53zz__ucs2z00(obj_t BgL_envz00_1189,
		obj_t BgL_charz00_1190)
	{
		{	/* Llib/ucs2.scm 276 */
			{	/* Llib/ucs2.scm 277 */
				ucs2_t BgL_tmpz00_1652;

				{	/* Llib/ucs2.scm 277 */
					unsigned char BgL_auxz00_1653;

					{	/* Llib/ucs2.scm 277 */
						obj_t BgL_tmpz00_1654;

						if (CHARP(BgL_charz00_1190))
							{	/* Llib/ucs2.scm 277 */
								BgL_tmpz00_1654 = BgL_charz00_1190;
							}
						else
							{
								obj_t BgL_auxz00_1657;

								BgL_auxz00_1657 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1493z00zz__ucs2z00,
									BINT(12499L), BGl_string1520z00zz__ucs2z00,
									BGl_string1521z00zz__ucs2z00, BgL_charz00_1190);
								FAILURE(BgL_auxz00_1657, BFALSE, BFALSE);
							}
						BgL_auxz00_1653 = CCHAR(BgL_tmpz00_1654);
					}
					BgL_tmpz00_1652 = BGl_charzd2ze3ucs2z31zz__ucs2z00(BgL_auxz00_1653);
				}
				return BUCS2(BgL_tmpz00_1652);
			}
		}

	}



/* ucs2->char */
	BGL_EXPORTED_DEF unsigned char BGl_ucs2zd2ze3charz31zz__ucs2z00(ucs2_t
		BgL_ucs2z00_35)
	{
		{	/* Llib/ucs2.scm 282 */
			{	/* Llib/ucs2.scm 283 */
				int BgL_intz00_1079;

				{	/* Llib/ucs2.scm 271 */
					obj_t BgL_tmpz00_1664;

					BgL_tmpz00_1664 = BUCS2(BgL_ucs2z00_35);
					BgL_intz00_1079 = CUCS2(BgL_tmpz00_1664);
				}
				if (((long) (BgL_intz00_1079) < 256L))
					{	/* Llib/ucs2.scm 284 */
						return (char) (((long) (BgL_intz00_1079)));
					}
				else
					{	/* Llib/ucs2.scm 284 */
						return
							CCHAR(BGl_errorz00zz__errorz00(BGl_symbol1522z00zz__ucs2z00,
								BGl_string1524z00zz__ucs2z00, BUCS2(BgL_ucs2z00_35)));
					}
			}
		}

	}



/* &ucs2->char */
	obj_t BGl_z62ucs2zd2ze3charz53zz__ucs2z00(obj_t BgL_envz00_1191,
		obj_t BgL_ucs2z00_1192)
	{
		{	/* Llib/ucs2.scm 282 */
			{	/* Llib/ucs2.scm 283 */
				unsigned char BgL_tmpz00_1676;

				{	/* Llib/ucs2.scm 283 */
					ucs2_t BgL_auxz00_1677;

					{	/* Llib/ucs2.scm 283 */
						obj_t BgL_tmpz00_1678;

						if (UCS2P(BgL_ucs2z00_1192))
							{	/* Llib/ucs2.scm 283 */
								BgL_tmpz00_1678 = BgL_ucs2z00_1192;
							}
						else
							{
								obj_t BgL_auxz00_1681;

								BgL_auxz00_1681 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1493z00zz__ucs2z00,
									BINT(12786L), BGl_string1525z00zz__ucs2z00,
									BGl_string1495z00zz__ucs2z00, BgL_ucs2z00_1192);
								FAILURE(BgL_auxz00_1681, BFALSE, BFALSE);
							}
						BgL_auxz00_1677 = CUCS2(BgL_tmpz00_1678);
					}
					BgL_tmpz00_1676 = BGl_ucs2zd2ze3charz31zz__ucs2z00(BgL_auxz00_1677);
				}
				return BCHAR(BgL_tmpz00_1676);
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__ucs2z00(void)
	{
		{	/* Llib/ucs2.scm 14 */
			return
				BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1526z00zz__ucs2z00));
		}

	}

#ifdef __cplusplus
}
#endif
