/*===========================================================================*/
/*   (Llib/error.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Llib/error.scm -indent -o objs/obj_u/Llib/error.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___ERROR_TYPE_DEFINITIONS
#define BGL___ERROR_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_z62exceptionz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
	}                      *BgL_z62exceptionz62_bglt;

	typedef struct BgL_z62errorz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                  *BgL_z62errorz62_bglt;

	typedef struct BgL_z62typezd2errorzb0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
		obj_t BgL_typez00;
	}                         *BgL_z62typezd2errorzb0_bglt;

	typedef struct BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
		obj_t BgL_indexz00;
	}                                             
		*BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt;

	typedef struct BgL_z62iozd2errorzb0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                       *BgL_z62iozd2errorzb0_bglt;

	typedef struct BgL_z62iozd2portzd2errorz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                              *BgL_z62iozd2portzd2errorz62_bglt;

	typedef struct BgL_z62iozd2readzd2errorz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                              *BgL_z62iozd2readzd2errorz62_bglt;

	typedef struct BgL_z62iozd2writezd2errorz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                               *BgL_z62iozd2writezd2errorz62_bglt;

	typedef struct BgL_z62iozd2filezd2notzd2foundzd2errorz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                                           
		*BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt;

	typedef struct BgL_z62iozd2parsezd2errorz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                               *BgL_z62iozd2parsezd2errorz62_bglt;

	typedef struct BgL_z62iozd2unknownzd2hostzd2errorzb0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                                       
		*BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt;

	typedef struct BgL_z62iozd2malformedzd2urlzd2errorzb0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                                        
		*BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt;

	typedef struct BgL_z62iozd2sigpipezd2errorz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                                 *BgL_z62iozd2sigpipezd2errorz62_bglt;

	typedef struct BgL_z62iozd2timeoutzd2errorz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                                 *BgL_z62iozd2timeoutzd2errorz62_bglt;

	typedef struct BgL_z62iozd2connectionzd2errorz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                                    *BgL_z62iozd2connectionzd2errorz62_bglt;

	typedef struct BgL_z62processzd2exceptionzb0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                                *BgL_z62processzd2exceptionzb0_bglt;

	typedef struct BgL_z62stackzd2overflowzd2errorz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                                    
		*BgL_z62stackzd2overflowzd2errorz62_bglt;

	typedef struct BgL_z62warningz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_argsz00;
	}                    *BgL_z62warningz62_bglt;


#endif													// BGL___ERROR_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62exitz62zz__errorz00(obj_t, obj_t);
	extern obj_t BGl_substringz00zz__r4_strings_6_7z00(obj_t, long, long);
	extern obj_t BGl_z62processzd2exceptionzb0zz__objectz00;
	static obj_t BGl_zc3z04exitza32251ze3ze70z60zz__errorz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_setzd2errorzd2handlerz12z12zz__errorz00(obj_t);
	static obj_t BGl_z62errorzd2notifyzb0zz__errorz00(obj_t, obj_t);
	extern int BGl_bigloozd2warningzd2zz__paramz00(void);
	extern obj_t BGl_stringzd2replacezd2zz__r4_strings_6_7z00(obj_t,
		unsigned char, unsigned char);
	static obj_t BGl_symbol2815z00zz__errorz00 = BUNSPEC;
	static obj_t BGl_fixzd2tabulationz12zc0zz__errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t the_failure(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32253ze3ze5zz__errorz00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__errorz00 = BUNSPEC;
	extern obj_t BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_displayzd2tracezd2stackzd2sourcezd2zz__errorz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_raisez00zz__errorz00(obj_t);
	extern obj_t BGl_exceptionzd2notifyzd2zz__objectz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31928ze3ze5zz__errorz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_errorzd2notifyzf2locationz20zz__errorz00(obj_t,
		obj_t, int);
	extern obj_t BGl_z62iozd2malformedzd2urlzd2errorzb0zz__objectz00;
	BGL_EXPORTED_DECL obj_t BGl_exitz00zz__errorz00(obj_t);
	extern obj_t BGl_applyz00zz__r4_control_features_6_9z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62errorzf2czd2locationz42zz__errorz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_z62iozd2filezd2notzd2foundzd2errorz62zz__objectz00;
	static obj_t BGl_z62z62tryz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_errorzd2notifyzd2zz__errorz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_errorzf2sourcezd2locationz20zz__errorz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62setzd2errorzd2handlerz12z70zz__errorz00(obj_t, obj_t);
	static obj_t BGl_z62errorzf2locationz90zz__errorz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_envzd2getzd2errorzd2handlerzd2zz__errorz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(long,
		char *);
	extern obj_t BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t bgl_system_failure(int, obj_t, obj_t, obj_t);
	extern obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_z62indexzd2outzd2ofzd2bounds2760zb0zz__errorz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_unwindzd2untilz12zc0zz__bexitz00(obj_t, obj_t);
	extern obj_t BGl_z62errorz62zz__objectz00;
	extern obj_t BGl_z62iozd2sigpipezd2errorz62zz__objectz00;
	extern bool_t BGl_classzf3zf3zz__objectz00(obj_t);
	extern bool_t fexists(char *);
	BGL_EXPORTED_DECL obj_t BGl_warningz00zz__errorz00(obj_t);
	static obj_t BGl_relativezd2filezd2namez00zz__errorz00(obj_t);
	static obj_t BGl_z62displayzd2tracezd2stackzd2sourcezb0zz__errorz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_stringzd2appendzd2zz__r4_strings_6_7z00(obj_t);
	extern obj_t BGl_z62warningz62zz__objectz00;
	static obj_t BGl_z62sigsegvzd2errorzd2handlerz62zz__errorz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_z62tryz62zz__errorz00(obj_t, obj_t);
	extern obj_t BGl_everyz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zz__errorz00(void);
	extern obj_t BGl_z62iozd2writezd2errorz62zz__objectz00;
	BGL_EXPORTED_DECL obj_t BGl_warningzf2czd2locationz20zz__errorz00(char *,
		long, obj_t);
	extern obj_t BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(long,
		long);
	extern obj_t unwind_stack_until(obj_t, obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_z62stackzd2overflowzd2errorz62zz__objectz00;
	static obj_t BGl_z62typezd2error2759zb0zz__errorz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_z62typezd2errorzb0zz__objectz00;
	static obj_t BGl_uncygdrivez00zz__errorz00(obj_t);
	extern obj_t BGl_tprintz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_z62warningzd2notifyzb0zz__errorz00(obj_t, obj_t);
	static obj_t BGl_symbol2850z00zz__errorz00 = BUNSPEC;
	extern obj_t BGl_z62iozd2portzd2errorz62zz__objectz00;
	extern obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	extern obj_t BGl_getenvz00zz__osz00(obj_t);
	static obj_t BGl_locationzd2linezd2numz00zz__errorz00(obj_t);
	static obj_t BGl_filenamezd2forzd2errorz00zz__errorz00(obj_t, long);
	extern bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	static obj_t BGl_symbol2859z00zz__errorz00 = BUNSPEC;
	extern obj_t BGl_readzd2linezd2zz__r4_input_6_10_2z00(obj_t);
	extern obj_t BGl_z62iozd2connectionzd2errorz62zz__objectz00;
	BGL_EXPORTED_DECL obj_t BGl_notifyzd2interruptzd2zz__errorz00(int);
	static obj_t BGl_z62stackzd2overflowzd2erro2757z62zz__errorz00(obj_t);
	extern obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__errorz00(void);
	static obj_t BGl_z62raisez62zz__errorz00(obj_t, obj_t);
	extern bool_t BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	extern obj_t BGl_stringzd2indexzd2zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
	extern obj_t bgl_reverse(obj_t);
	static obj_t BGl_z62typeofz62zz__errorz00(obj_t, obj_t);
	extern obj_t BGl_z62iozd2parsezd2errorz62zz__objectz00;
	static obj_t BGl_z62errorzd2notifyzf2locationz42zz__errorz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zz__errorz00(void);
	extern obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_getzd2tracezd2stackz00zz__errorz00(obj_t);
	static obj_t BGl_z62escz62zz__errorz00(obj_t, obj_t);
	extern long bgl_list_length(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_warningzd2notifyzf2locationz20zz__errorz00(obj_t,
		obj_t, int);
	static obj_t BGl_symbol2861z00zz__errorz00 = BUNSPEC;
	extern obj_t BGl_z62iozd2timeoutzd2errorz62zz__objectz00;
	static obj_t BGl_z62warningzd2notifyzf2locationz42zz__errorz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__errorz00(void);
	BGL_EXPORTED_DECL obj_t bgl_stack_overflow_error(void);
	static obj_t BGl_gczd2rootszd2initz00zz__errorz00(void);
	static obj_t BGl_z62currentzd2exceptionzd2handlerz62zz__errorz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31456ze3ze5zz__errorz00(obj_t, obj_t);
	static obj_t BGl_z62warningzf2czd2locationz42zz__errorz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_z62iozd2errorzb0zz__objectz00;
	BGL_EXPORTED_DECL obj_t BGl_dumpzd2tracezd2stackz00zz__errorz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_displayzd2tracezd2stackz00zz__errorz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62errorzf2sourcezd2locationz42zz__errorz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_bigloozd2typezd2errorzd2msgzd2zz__errorz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zz__errorz00(void);
	static obj_t BGl_z62envzd2getzd2errorzd2handlerzb0zz__errorz00(obj_t, obj_t);
	extern obj_t BGl_listzd2ze3stringz31zz__r4_strings_6_7z00(obj_t);
	extern obj_t BGl_homogeneouszd2vectorzd2infoz00zz__srfi4z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_pushzd2errorzd2handlerz12z12zz__errorz00(obj_t,
		obj_t);
	static obj_t BGl_z62error2758z62zz__errorz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62bigloozd2typezd2errorz62zz__errorz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62warningzf2locz90zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62withzd2exceptionzd2handlerz62zz__errorz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62envzd2setzd2errorzd2handlerz12za2zz__errorz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62errorzf2sourcez90zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_z62indexzd2outzd2ofzd2boundszd2errorz62zz__objectz00;
	static obj_t BGl_z62sigbuszd2errorzd2handlerz62zz__errorz00(obj_t, obj_t);
	extern obj_t BGl_displayzd2circlezd2zz__pp_circlez00(obj_t, obj_t);
	extern obj_t BGl_z62exceptionz62zz__objectz00;
	static obj_t BGl_openzd2forzd2errorz00zz__errorz00(obj_t);
	extern obj_t BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t bgl_reverse_bang(obj_t);
	static obj_t BGl_dozd2warnzf2locationz20zz__errorz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_dirnamez00zz__osz00(obj_t);
	static obj_t BGl_z62modulezd2initzd2errorz62zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62sigfpezd2errorzd2handlerz62zz__errorz00(obj_t, obj_t);
	static obj_t BGl_locationzd2linezd2colze70ze7zz__errorz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62findzd2runtimezd2typez62zz__errorz00(obj_t, obj_t);
	static obj_t BGl_z62getzd2errorzd2handlerz62zz__errorz00(obj_t);
	static obj_t BGl_symbol2881z00zz__errorz00 = BUNSPEC;
	static obj_t BGl_symbol2883z00zz__errorz00 = BUNSPEC;
	extern obj_t BGl_pwdz00zz__osz00(void);
	static obj_t BGl_z62warning2761z62zz__errorz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_errorzf2locationzf2zz__errorz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31458ze3ze5zz__errorz00(obj_t, obj_t);
	static obj_t BGl_printzd2cursorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_warningzf2locationzf2zz__errorz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(obj_t);
	extern obj_t BGl_classzd2namezd2zz__objectz00(obj_t);
	static obj_t BGl_dirnamezd2ze3listz31zz__errorz00(obj_t);
	static obj_t BGl_locationzd2atze70z35zz__errorz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_errorzf2sourcezf2zz__errorz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol2894z00zz__errorz00 = BUNSPEC;
	extern obj_t BGl_z62conditionz62zz__objectz00;
	static obj_t BGl_z62the_failurez62zz__errorz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_zc3z04exitza31686ze3ze70z60zz__errorz00(obj_t, obj_t, obj_t);
	extern bool_t bgl_directoryp(char *);
	extern obj_t BGl_fprintfz00zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL char *bgl_show_type(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31895ze3ze5zz__errorz00(obj_t, obj_t);
	static obj_t BGl_displayzd2tracezd2stackzd2frameze70z35zz__errorz00(obj_t,
		obj_t, obj_t, long);
	extern obj_t BGl_basenamez00zz__osz00(obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_notifyzd2z62errorzf2locationzd2locz90zz__errorz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_currentzd2exceptionzd2handlerz00zz__errorz00(void);
	static obj_t BGl_warningzf2locationzd2filez20zz__errorz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62bigloozd2typezd2errorzd2msgzb0zz__errorz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_warningzd2notifyzd2zz__errorz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31799ze3ze5zz__errorz00(obj_t);
	static obj_t BGl_methodzd2initzd2zz__errorz00(void);
	BGL_EXPORTED_DECL obj_t BGl_bigloozd2typezd2errorz00zz__errorz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_notifyzd2z62errorzf2locz42zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62errorzf2errnoz90zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(obj_t, obj_t, obj_t,
		obj_t, int, int);
	static obj_t
		BGl_notifyzd2z62errorzf2locationzd2nozd2locz42zz__errorz00(obj_t);
	static obj_t BGl__displayzd2tracezd2stackz00zz__errorz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t bgl_typeof(obj_t);
	static obj_t BGl_z62notifyzd2interruptzb0zz__errorz00(obj_t, obj_t);
	extern int BGl_bigloozd2tracezd2stackzd2depthzd2zz__paramz00(void);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initzd2errorz00zz__errorz00(char *,
		char *);
	BGL_EXPORTED_DECL obj_t bgl_find_runtime_type(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_getzd2errorzd2handlerz00zz__errorz00(void);
	BGL_EXPORTED_DECL obj_t BGl_errorzf2czd2locationz20zz__errorz00(obj_t, obj_t,
		obj_t, char *, long);
	BGL_EXPORTED_DECL obj_t
		BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern bool_t BGl_stringzd2prefixzf3z21zz__r4_strings_6_7z00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_displayz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_z62dumpzd2tracezd2stackz62zz__errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_withzd2exceptionzd2handlerz00zz__errorz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_envzd2setzd2errorzd2handlerz12zc0zz__errorz00(obj_t, obj_t);
	extern obj_t make_string(long, unsigned char);
	extern obj_t bgl_close_input_port(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31835ze3ze5zz__errorz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31495ze3ze5zz__errorz00(obj_t, obj_t);
	static obj_t BGl_z62czd2debuggingzd2showzd2typezb0zz__errorz00(obj_t, obj_t);
	extern obj_t BGl_forzd2eachzd2zz__r4_control_features_6_9z00(obj_t, obj_t);
	static obj_t BGl_z62sigillzd2errorzd2handlerz62zz__errorz00(obj_t, obj_t);
	static obj_t BGl_defaultzd2exceptionzd2handlerz00zz__errorz00(obj_t);
	static obj_t BGl_z62bigloozd2typezd2errorzf2locationz90zz__errorz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	extern long BGl_stringzd2ze3integerz31zz__r4_numbers_6_5_fixnumz00(obj_t,
		long, long);
	extern obj_t BGl_fprintz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	extern obj_t BGl_z62iozd2unknownzd2hostzd2errorzb0zz__objectz00;
	static obj_t BGl__getzd2tracezd2stackz00zz__errorz00(obj_t, obj_t);
	extern obj_t BGl_signalz00zz__osz00(int, obj_t);
	extern obj_t BGl_z62iozd2readzd2errorz62zz__objectz00;
	extern bool_t bigloo_strncmp(obj_t, obj_t, long);
	BGL_EXPORTED_DECL obj_t BGl_warningzf2loczf2zz__errorz00(obj_t, obj_t);
	static obj_t BGl_z62warningzf2locationz90zz__errorz00(obj_t, obj_t, obj_t,
		obj_t);
	extern long BGl_maxfxz00zz__r4_numbers_6_5_fixnumz00(long, obj_t);
	static obj_t BGl_z62pushzd2errorzd2handlerz12z70zz__errorz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_valuesz00zz__r5_control_features_6_4z00(obj_t);
	extern obj_t BGl_newlinez00zz__r4_output_6_10_3z00(obj_t);
	static obj_t BGl_notifyzd2z62errorzb0zz__errorz00(obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_envzd2setzd2errorzd2handlerz12zd2envz12zz__errorz00,
		BgL_bgl_za762envza7d2setza7d2e2960za7,
		BGl_z62envzd2setzd2errorzd2handlerz12za2zz__errorz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_the_failurezd2envzd2zz__errorz00,
		BgL_bgl_za762the_failureza762961z00, BGl_z62the_failurez62zz__errorz00, 0L,
		BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_errorzf2sourcezd2envz20zz__errorz00,
		BgL_bgl_za762errorza7f2sourc2962z00, BGl_z62errorzf2sourcez90zz__errorz00,
		0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string2920z00zz__errorz00,
		BgL_bgl_string2920za700za7za7_2963za7, "socket", 6);
	      DEFINE_STRING(BGl_string2921z00zz__errorz00,
		BgL_bgl_string2921za700za7za7_2964za7, "datagram-socket", 15);
	      DEFINE_STRING(BGl_string2840z00zz__errorz00,
		BgL_bgl_string2840za700za7za7_2965za7, "\" provided", 10);
	      DEFINE_STRING(BGl_string2922z00zz__errorz00,
		BgL_bgl_string2922za700za7za7_2966za7, "process", 7);
	      DEFINE_STRING(BGl_string2841z00zz__errorz00,
		BgL_bgl_string2841za700za7za7_2967za7, "\" expected, \"", 13);
	      DEFINE_STRING(BGl_string2923z00zz__errorz00,
		BgL_bgl_string2923za700za7za7_2968za7, "custom", 6);
	      DEFINE_STRING(BGl_string2842z00zz__errorz00,
		BgL_bgl_string2842za700za7za7_2969za7, " \"", 2);
	      DEFINE_STRING(BGl_string2924z00zz__errorz00,
		BgL_bgl_string2924za700za7za7_2970za7, "opaque", 6);
	      DEFINE_STRING(BGl_string2843z00zz__errorz00,
		BgL_bgl_string2843za700za7za7_2971za7, "&bigloo-type-error-msg", 22);
	      DEFINE_STRING(BGl_string2925z00zz__errorz00,
		BgL_bgl_string2925za700za7za7_2972za7, "_", 1);
	      DEFINE_STRING(BGl_string2844z00zz__errorz00,
		BgL_bgl_string2844za700za7za7_2973za7, "]", 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_notifyzd2interruptzd2envz00zz__errorz00,
		BgL_bgl_za762notifyza7d2inte2974z00,
		BGl_z62notifyzd2interruptzb0zz__errorz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2926z00zz__errorz00,
		BgL_bgl_string2926za700za7za7_2975za7, "ucs2string", 10);
	      DEFINE_STRING(BGl_string2845z00zz__errorz00,
		BgL_bgl_string2845za700za7za7_2976za7, " out of range [0..", 18);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_raisezd2envzd2zz__errorz00,
		BgL_bgl_za762raiseza762za7za7__e2977z00, BGl_z62raisez62zz__errorz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2927z00zz__errorz00,
		BgL_bgl_string2927za700za7za7_2978za7, "ucs2", 4);
	      DEFINE_STRING(BGl_string2846z00zz__errorz00,
		BgL_bgl_string2846za700za7za7_2979za7, "index ", 6);
	      DEFINE_STRING(BGl_string2928z00zz__errorz00,
		BgL_bgl_string2928za700za7za7_2980za7, "elong", 5);
	      DEFINE_STRING(BGl_string2847z00zz__errorz00,
		BgL_bgl_string2847za700za7za7_2981za7, "&index-out-of-bounds2760", 24);
	      DEFINE_STRING(BGl_string2929z00zz__errorz00,
		BgL_bgl_string2929za700za7za7_2982za7, "llong", 5);
	      DEFINE_STRING(BGl_string2848z00zz__errorz00,
		BgL_bgl_string2848za700za7za7_2983za7, "&warning/c-location", 19);
	      DEFINE_STRING(BGl_string2849z00zz__errorz00,
		BgL_bgl_string2849za700za7za7_2984za7, ":\n", 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_errorzd2notifyzf2locationzd2envzf2zz__errorz00,
		BgL_bgl_za762errorza7d2notif2985z00,
		BGl_z62errorzd2notifyzf2locationz42zz__errorz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_getzd2tracezd2stackzd2envzd2zz__errorz00,
		BgL_bgl__getza7d2traceza7d2s2986z00, opt_generic_entry,
		BGl__getzd2tracezd2stackz00zz__errorz00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_currentzd2exceptionzd2handlerzd2envzd2zz__errorz00,
		BgL_bgl_za762currentza7d2exc2987z00,
		BGl_z62currentzd2exceptionzd2handlerz62zz__errorz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2930z00zz__errorz00,
		BgL_bgl_string2930za700za7za7_2988za7, "mutex", 5);
	      DEFINE_STRING(BGl_string2931z00zz__errorz00,
		BgL_bgl_string2931za700za7za7_2989za7, "condvar", 7);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_errorzd2notifyzd2envz00zz__errorz00,
		BgL_bgl_za762errorza7d2notif2990z00, BGl_z62errorzd2notifyzb0zz__errorz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_errorzf2sourcezd2locationzd2envzf2zz__errorz00,
		BgL_bgl_za762errorza7f2sourc2991z00,
		BGl_z62errorzf2sourcezd2locationz42zz__errorz00, 0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string2932z00zz__errorz00,
		BgL_bgl_string2932za700za7za7_2992za7, "date", 4);
	      DEFINE_STRING(BGl_string2851z00zz__errorz00,
		BgL_bgl_string2851za700za7za7_2993za7, "%no-error-obj", 13);
	      DEFINE_STRING(BGl_string2933z00zz__errorz00,
		BgL_bgl_string2933za700za7za7_2994za7, "bignum", 6);
	      DEFINE_STRING(BGl_string2852z00zz__errorz00,
		BgL_bgl_string2852za700za7za7_2995za7, " -- ", 4);
	      DEFINE_STRING(BGl_string2934z00zz__errorz00,
		BgL_bgl_string2934za700za7za7_2996za7, "mmap", 4);
	      DEFINE_STRING(BGl_string2853z00zz__errorz00,
		BgL_bgl_string2853za700za7za7_2997za7, "\", character ", 13);
	      DEFINE_STRING(BGl_string2935z00zz__errorz00,
		BgL_bgl_string2935za700za7za7_2998za7, "regexp", 6);
	      DEFINE_STRING(BGl_string2854z00zz__errorz00,
		BgL_bgl_string2854za700za7za7_2999za7, "File \"", 6);
	      DEFINE_STRING(BGl_string2936z00zz__errorz00,
		BgL_bgl_string2936za700za7za7_3000za7, "int8", 4);
	      DEFINE_STRING(BGl_string2855z00zz__errorz00,
		BgL_bgl_string2855za700za7za7_3001za7, "...", 3);
	      DEFINE_STRING(BGl_string2937z00zz__errorz00,
		BgL_bgl_string2937za700za7za7_3002za7, "uint8", 5);
	      DEFINE_STRING(BGl_string2856z00zz__errorz00,
		BgL_bgl_string2856za700za7za7_3003za7, "", 0);
	      DEFINE_STRING(BGl_string2938z00zz__errorz00,
		BgL_bgl_string2938za700za7za7_3004za7, "int16", 5);
	      DEFINE_STRING(BGl_string2857z00zz__errorz00,
		BgL_bgl_string2857za700za7za7_3005za7, "stdin", 5);
	      DEFINE_STRING(BGl_string2939z00zz__errorz00,
		BgL_bgl_string2939za700za7za7_3006za7, "uint16", 6);
	      DEFINE_STRING(BGl_string2858z00zz__errorz00,
		BgL_bgl_string2858za700za7za7_3007za7, "string://", 9);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_envzd2getzd2errorzd2handlerzd2envz00zz__errorz00,
		BgL_bgl_za762envza7d2getza7d2e3008za7,
		BGl_z62envzd2getzd2errorzd2handlerzb0zz__errorz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_bigloozd2typezd2errorzd2envzd2zz__errorz00,
		BgL_bgl_za762biglooza7d2type3009z00,
		BGl_z62bigloozd2typezd2errorz62zz__errorz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_pushzd2errorzd2handlerz12zd2envzc0zz__errorz00,
		BgL_bgl_za762pushza7d2errorza73010za7,
		BGl_z62pushzd2errorzd2handlerz12z70zz__errorz00, 0L, BUNSPEC, 2);
	extern obj_t BGl_pairzf3zd2envz21zz__r4_pairs_and_lists_6_3z00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexzd2outzd2ofzd2boundszd2errorzd2envzd2zz__errorz00,
		BgL_bgl_za762indexza7d2outza7d3011za7,
		BGl_z62indexzd2outzd2ofzd2bounds2760zb0zz__errorz00, 0L, BUNSPEC, 6);
	      DEFINE_STRING(BGl_string2940z00zz__errorz00,
		BgL_bgl_string2940za700za7za7_3012za7, "int32", 5);
	      DEFINE_STRING(BGl_string2941z00zz__errorz00,
		BgL_bgl_string2941za700za7za7_3013za7, "uint32", 6);
	      DEFINE_STRING(BGl_string2860z00zz__errorz00,
		BgL_bgl_string2860za700za7za7_3014za7, "line-col", 8);
	      DEFINE_STRING(BGl_string2942z00zz__errorz00,
		BgL_bgl_string2942za700za7za7_3015za7, "int64", 5);
	      DEFINE_STRING(BGl_string2943z00zz__errorz00,
		BgL_bgl_string2943za700za7za7_3016za7, "uint64", 6);
	      DEFINE_STRING(BGl_string2862z00zz__errorz00,
		BgL_bgl_string2862za700za7za7_3017za7, "line", 4);
	      DEFINE_STRING(BGl_string2944z00zz__errorz00,
		BgL_bgl_string2944za700za7za7_3018za7, "bcnst", 5);
	      DEFINE_STRING(BGl_string2863z00zz__errorz00,
		BgL_bgl_string2863za700za7za7_3019za7, "win32", 5);
	      DEFINE_STRING(BGl_string2945z00zz__errorz00,
		BgL_bgl_string2945za700za7za7_3020za7, "&&try", 5);
	      DEFINE_STRING(BGl_string2864z00zz__errorz00,
		BgL_bgl_string2864za700za7za7_3021za7, "<eof>", 5);
	      DEFINE_STRING(BGl_string2946z00zz__errorz00,
		BgL_bgl_string2946za700za7za7_3022za7, "&push-error-handler!", 20);
	      DEFINE_STRING(BGl_string2865z00zz__errorz00,
		BgL_bgl_string2865za700za7za7_3023za7, "*** CONDITION: ", 15);
	      DEFINE_STRING(BGl_string2947z00zz__errorz00,
		BgL_bgl_string2947za700za7za7_3024za7, "&env-set-error-handler!", 23);
	      DEFINE_STRING(BGl_string2866z00zz__errorz00,
		BgL_bgl_string2866za700za7za7_3025za7, "&error-notify/location", 22);
	      DEFINE_STRING(BGl_string2948z00zz__errorz00,
		BgL_bgl_string2948za700za7za7_3026za7, "dynamic-env", 11);
	      DEFINE_STRING(BGl_string2867z00zz__errorz00,
		BgL_bgl_string2867za700za7za7_3027za7, "*** WARNING: ", 13);
	      DEFINE_STRING(BGl_string2949z00zz__errorz00,
		BgL_bgl_string2949za700za7za7_3028za7, "&env-get-error-handler", 22);
	      DEFINE_STRING(BGl_string2869z00zz__errorz00,
		BgL_bgl_string2869za700za7za7_3029za7, "[string]", 8);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_modulezd2initzd2errorzd2envzd2zz__errorz00,
		BgL_bgl_za762moduleza7d2init3030z00,
		BGl_z62modulezd2initzd2errorz62zz__errorz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_getzd2errorzd2handlerzd2envzd2zz__errorz00,
		BgL_bgl_za762getza7d2errorza7d3031za7,
		BGl_z62getzd2errorzd2handlerz62zz__errorz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_errorzf2czd2locationzd2envzf2zz__errorz00,
		BgL_bgl_za762errorza7f2cza7d2l3032za7,
		BGl_z62errorzf2czd2locationz42zz__errorz00, 0L, BUNSPEC, 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2typezd2errorzf2locationzd2envz20zz__errorz00,
		BgL_bgl_za762biglooza7d2type3033z00,
		BGl_z62bigloozd2typezd2errorzf2locationz90zz__errorz00, 0L, BUNSPEC, 5);
	      DEFINE_STRING(BGl_string2950z00zz__errorz00,
		BgL_bgl_string2950za700za7za7_3034za7, "*** INTERRUPT:bigloo:", 21);
	      DEFINE_STRING(BGl_string2951z00zz__errorz00,
		BgL_bgl_string2951za700za7za7_3035za7, "&notify-interrupt", 17);
	      DEFINE_STRING(BGl_string2870z00zz__errorz00,
		BgL_bgl_string2870za700za7za7_3036za7, "[stdin]", 7);
	      DEFINE_STRING(BGl_string2952z00zz__errorz00,
		BgL_bgl_string2952za700za7za7_3037za7, "arithmetic procedure", 20);
	      DEFINE_STRING(BGl_string2871z00zz__errorz00,
		BgL_bgl_string2871za700za7za7_3038za7, "&warning-notify/location", 24);
	      DEFINE_STRING(BGl_string2953z00zz__errorz00,
		BgL_bgl_string2953za700za7za7_3039za7, "`floating point' exception", 26);
	      DEFINE_STRING(BGl_string2954z00zz__errorz00,
		BgL_bgl_string2954za700za7za7_3040za7, "raised", 6);
	      DEFINE_STRING(BGl_string2873z00zz__errorz00,
		BgL_bgl_string2873za700za7za7_3041za7, "_display-trace-stack", 20);
	      DEFINE_STRING(BGl_string2955z00zz__errorz00,
		BgL_bgl_string2955za700za7za7_3042za7, "bigloo", 6);
	      DEFINE_STRING(BGl_string2874z00zz__errorz00,
		BgL_bgl_string2874za700za7za7_3043za7, "output-port", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2868z00zz__errorz00,
		BgL_bgl_za762za7c3za704anonymo3044za7,
		BGl_z62zc3z04anonymousza31895ze3ze5zz__errorz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2956z00zz__errorz00,
		BgL_bgl_string2956za700za7za7_3045za7, "`illegal instruction' exception",
		31);
	      DEFINE_STRING(BGl_string2875z00zz__errorz00,
		BgL_bgl_string2875za700za7za7_3046za7,
		"\n*** INTERNAL ERROR: corrupted stack -- ~s\n", 43);
	      DEFINE_STRING(BGl_string2957z00zz__errorz00,
		BgL_bgl_string2957za700za7za7_3047za7, "`bus error' exception", 21);
	      DEFINE_STRING(BGl_string2876z00zz__errorz00,
		BgL_bgl_string2876za700za7za7_3048za7, "! ", 2);
	      DEFINE_STRING(BGl_string2958z00zz__errorz00,
		BgL_bgl_string2958za700za7za7_3049za7, "`segmentation violation' exception",
		34);
	      DEFINE_STRING(BGl_string2877z00zz__errorz00,
		BgL_bgl_string2877za700za7za7_3050za7, "    ", 4);
	      DEFINE_STRING(BGl_string2959z00zz__errorz00,
		BgL_bgl_string2959za700za7za7_3051za7, "__error", 7);
	      DEFINE_STRING(BGl_string2878z00zz__errorz00,
		BgL_bgl_string2878za700za7za7_3052za7, "   ", 3);
	      DEFINE_STRING(BGl_string2879z00zz__errorz00,
		BgL_bgl_string2879za700za7za7_3053za7, "  ", 2);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_sigbuszd2errorzd2handlerzd2envzd2zz__errorz00,
		BgL_bgl_za762sigbusza7d2erro3054z00,
		BGl_z62sigbuszd2errorzd2handlerz62zz__errorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2872z00zz__errorz00,
		BgL_bgl_za762za7c3za704anonymo3055za7,
		BGl_z62zc3z04anonymousza31928ze3ze5zz__errorz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2880z00zz__errorz00,
		BgL_bgl_string2880za700za7za7_3056za7, ". ", 2);
	      DEFINE_STRING(BGl_string2882z00zz__errorz00,
		BgL_bgl_string2882za700za7za7_3057za7, "margin", 6);
	      DEFINE_STRING(BGl_string2884z00zz__errorz00,
		BgL_bgl_string2884za700za7za7_3058za7, "format", 6);
	      DEFINE_STRING(BGl_string2885z00zz__errorz00,
		BgL_bgl_string2885za700za7za7_3059za7, " ", 1);
	      DEFINE_STRING(BGl_string2886z00zz__errorz00,
		BgL_bgl_string2886za700za7za7_3060za7, " (* ", 4);
	      DEFINE_STRING(BGl_string2887z00zz__errorz00,
		BgL_bgl_string2887za700za7za7_3061za7, ")", 1);
	      DEFINE_STRING(BGl_string2888z00zz__errorz00,
		BgL_bgl_string2888za700za7za7_3062za7, ".", 1);
	      DEFINE_STRING(BGl_string2889z00zz__errorz00,
		BgL_bgl_string2889za700za7za7_3063za7, ", ", 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_warningzd2notifyzf2locationzd2envzf2zz__errorz00,
		BgL_bgl_za762warningza7d2not3064z00,
		BGl_z62warningzd2notifyzf2locationz42zz__errorz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2890z00zz__errorz00,
		BgL_bgl_string2890za700za7za7_3065za7, "@", 1);
	      DEFINE_STRING(BGl_string2891z00zz__errorz00,
		BgL_bgl_string2891za700za7za7_3066za7, "File ~s, line ~d, character ~d\n",
		31);
	      DEFINE_STRING(BGl_string2892z00zz__errorz00,
		BgL_bgl_string2892za700za7za7_3067za7, "File ~s, character ~d\n", 22);
	      DEFINE_STRING(BGl_string2893z00zz__errorz00,
		BgL_bgl_string2893za700za7za7_3068za7, "&display-trace-stack-source", 27);
	      DEFINE_STRING(BGl_string2895z00zz__errorz00,
		BgL_bgl_string2895za700za7za7_3069za7, "done", 4);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_typezd2errorzd2envz00zz__errorz00,
		BgL_bgl_za762typeza7d2error23070z00, BGl_z62typezd2error2759zb0zz__errorz00,
		0L, BUNSPEC, 5);
	      DEFINE_STRING(BGl_string2896z00zz__errorz00,
		BgL_bgl_string2896za700za7za7_3071za7, "^", 1);
	      DEFINE_STRING(BGl_string2897z00zz__errorz00,
		BgL_bgl_string2897za700za7za7_3072za7, "#", 1);
	      DEFINE_STRING(BGl_string2898z00zz__errorz00,
		BgL_bgl_string2898za700za7za7_3073za7, ", character ", 12);
	      DEFINE_STRING(BGl_string2899z00zz__errorz00,
		BgL_bgl_string2899za700za7za7_3074za7, "\", line ", 8);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_errorzd2envzd2zz__errorz00,
		BgL_bgl_za762error2758za762za73075za7, BGl_z62error2758z62zz__errorz00, 0L,
		BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_typeofzd2envzd2zz__errorz00,
		BgL_bgl_za762typeofza762za7za7__3076z00, BGl_z62typeofz62zz__errorz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_sigsegvzd2errorzd2handlerzd2envzd2zz__errorz00,
		BgL_bgl_za762sigsegvza7d2err3077z00,
		BGl_z62sigsegvzd2errorzd2handlerz62zz__errorz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_z62tryzd2envzb0zz__errorz00,
		BgL_bgl_za762za762tryza700za7za7__3078za7, BGl_z62z62tryz00zz__errorz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_sigillzd2errorzd2handlerzd2envzd2zz__errorz00,
		BgL_bgl_za762sigillza7d2erro3079z00,
		BGl_z62sigillzd2errorzd2handlerz62zz__errorz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_withzd2exceptionzd2handlerzd2envzd2zz__errorz00,
		BgL_bgl_za762withza7d2except3080z00,
		BGl_z62withzd2exceptionzd2handlerz62zz__errorz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_exitzd2envzd2zz__errorz00,
		BgL_bgl_za762exitza762za7za7__er3081z00, va_generic_entry,
		BGl_z62exitz62zz__errorz00, BUNSPEC, -1);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_sigfpezd2errorzd2handlerzd2envzd2zz__errorz00,
		BgL_bgl_za762sigfpeza7d2erro3082z00,
		BGl_z62sigfpezd2errorzd2handlerz62zz__errorz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_dumpzd2tracezd2stackzd2envzd2zz__errorz00,
		BgL_bgl_za762dumpza7d2traceza73083za7,
		BGl_z62dumpzd2tracezd2stackz62zz__errorz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_displayzd2tracezd2stackzd2envzd2zz__errorz00,
		BgL_bgl__displayza7d2trace3084za7, opt_generic_entry,
		BGl__displayzd2tracezd2stackz00zz__errorz00, BFALSE, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_warningzd2notifyzd2envz00zz__errorz00,
		BgL_bgl_za762warningza7d2not3085z00, BGl_z62warningzd2notifyzb0zz__errorz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_warningzf2loczd2envz20zz__errorz00,
		BgL_bgl_za762warningza7f2loc3086z00, va_generic_entry,
		BGl_z62warningzf2locz90zz__errorz00, BUNSPEC, -2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_warningzd2envzd2zz__errorz00,
		BgL_bgl_za762warning2761za763087z00, va_generic_entry,
		BGl_z62warning2761z62zz__errorz00, BUNSPEC, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_warningzf2czd2locationzd2envzf2zz__errorz00,
		BgL_bgl_za762warningza7f2cza7d3088za7, va_generic_entry,
		BGl_z62warningzf2czd2locationz42zz__errorz00, BUNSPEC, -3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setzd2errorzd2handlerz12zd2envzc0zz__errorz00,
		BgL_bgl_za762setza7d2errorza7d3089za7,
		BGl_z62setzd2errorzd2handlerz12z70zz__errorz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2808z00zz__errorz00,
		BgL_bgl_string2808za700za7za7_3090za7, "BIGLOOSTACKDEPTH", 16);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_displayzd2tracezd2stackzd2sourcezd2envz00zz__errorz00,
		BgL_bgl_za762displayza7d2tra3091z00,
		BGl_z62displayzd2tracezd2stackzd2sourcezb0zz__errorz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2809z00zz__errorz00,
		BgL_bgl_string2809za700za7za7_3092za7, "\077\077?", 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_czd2debuggingzd2showzd2typezd2envz00zz__errorz00,
		BgL_bgl_za762cza7d2debugging3093z00,
		BGl_z62czd2debuggingzd2showzd2typezb0zz__errorz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2810z00zz__errorz00,
		BgL_bgl_string2810za700za7za7_3094za7, "Type", 4);
	      DEFINE_STRING(BGl_string2811z00zz__errorz00,
		BgL_bgl_string2811za700za7za7_3095za7, "/tmp/bigloo/runtime/Llib/error.scm",
		34);
	      DEFINE_STRING(BGl_string2812z00zz__errorz00,
		BgL_bgl_string2812za700za7za7_3096za7, "&error/errno", 12);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stackzd2overflowzd2errorzd2envzd2zz__errorz00,
		BgL_bgl_za762stackza7d2overf3097z00,
		BGl_z62stackzd2overflowzd2erro2757z62zz__errorz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2813z00zz__errorz00,
		BgL_bgl_string2813za700za7za7_3098za7, "bint", 4);
	      DEFINE_STRING(BGl_string2814z00zz__errorz00,
		BgL_bgl_string2814za700za7za7_3099za7, "stack overflow", 14);
	      DEFINE_STRING(BGl_string2816z00zz__errorz00,
		BgL_bgl_string2816za700za7za7_3100za7, "at", 2);
	      DEFINE_STRING(BGl_string2817z00zz__errorz00,
		BgL_bgl_string2817za700za7za7_3101za7, "with-exception-handler", 22);
	      DEFINE_STRING(BGl_string2818z00zz__errorz00,
		BgL_bgl_string2818za700za7za7_3102za7, "Incorrect thunk arity", 21);
	      DEFINE_STRING(BGl_string2819z00zz__errorz00,
		BgL_bgl_string2819za700za7za7_3103za7, "Incorrect handler arity", 23);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2typezd2errorzd2msgzd2envz00zz__errorz00,
		BgL_bgl_za762biglooza7d2type3104z00,
		BGl_z62bigloozd2typezd2errorzd2msgzb0zz__errorz00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_errorzf2errnozd2envz20zz__errorz00,
		BgL_bgl_za762errorza7f2errno3105z00, BGl_z62errorzf2errnoz90zz__errorz00,
		0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string2900z00zz__errorz00,
		BgL_bgl_string2900za700za7za7_3106za7, "../", 3);
	      DEFINE_STRING(BGl_string2901z00zz__errorz00,
		BgL_bgl_string2901za700za7za7_3107za7, "/", 1);
	      DEFINE_STRING(BGl_string2820z00zz__errorz00,
		BgL_bgl_string2820za700za7za7_3108za7, "&with-exception-handler", 23);
	      DEFINE_STRING(BGl_string2902z00zz__errorz00,
		BgL_bgl_string2902za700za7za7_3109za7, "/cygdrive/", 10);
	      DEFINE_STRING(BGl_string2821z00zz__errorz00,
		BgL_bgl_string2821za700za7za7_3110za7, "procedure", 9);
	      DEFINE_STRING(BGl_string2903z00zz__errorz00,
		BgL_bgl_string2903za700za7za7_3111za7, "real", 4);
	      DEFINE_STRING(BGl_string2904z00zz__errorz00,
		BgL_bgl_string2904za700za7za7_3112za7, "symbol", 6);
	      DEFINE_STRING(BGl_string2823z00zz__errorz00,
		BgL_bgl_string2823za700za7za7_3113za7,
		"*** INTERNAL ERROR: wrong error handler ", 40);
	      DEFINE_STRING(BGl_string2905z00zz__errorz00,
		BgL_bgl_string2905za700za7za7_3114za7, "keyword", 7);
	      DEFINE_STRING(BGl_string2824z00zz__errorz00,
		BgL_bgl_string2824za700za7za7_3115za7, ":", 1);
	      DEFINE_STRING(BGl_string2906z00zz__errorz00,
		BgL_bgl_string2906za700za7za7_3116za7, "bchar", 5);
	      DEFINE_STRING(BGl_string2825z00zz__errorz00,
		BgL_bgl_string2825za700za7za7_3117za7, ",", 1);
	      DEFINE_STRING(BGl_string2907z00zz__errorz00,
		BgL_bgl_string2907za700za7za7_3118za7, "bbool", 5);
	      DEFINE_STRING(BGl_string2826z00zz__errorz00,
		BgL_bgl_string2826za700za7za7_3119za7, "Llib/error.scm", 14);
	      DEFINE_STRING(BGl_string2908z00zz__errorz00,
		BgL_bgl_string2908za700za7za7_3120za7, "bnil", 4);
	      DEFINE_STRING(BGl_string2827z00zz__errorz00,
		BgL_bgl_string2827za700za7za7_3121za7, "when raising error: ", 20);
	      DEFINE_STRING(BGl_string2909z00zz__errorz00,
		BgL_bgl_string2909za700za7za7_3122za7, "unspecified", 11);
	      DEFINE_STRING(BGl_string2828z00zz__errorz00,
		BgL_bgl_string2828za700za7za7_3123za7, "raise", 5);
	      DEFINE_STRING(BGl_string2829z00zz__errorz00,
		BgL_bgl_string2829za700za7za7_3124za7, "uncaught exception", 18);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_findzd2runtimezd2typezd2envzd2zz__errorz00,
		BgL_bgl_za762findza7d2runtim3125z00,
		BGl_z62findzd2runtimezd2typez62zz__errorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2822z00zz__errorz00,
		BgL_bgl_za762za7c3za704anonymo3126za7,
		BGl_z62zc3z04anonymousza31458ze3ze5zz__errorz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2910z00zz__errorz00,
		BgL_bgl_string2910za700za7za7_3127za7, "epair", 5);
	      DEFINE_STRING(BGl_string2911z00zz__errorz00,
		BgL_bgl_string2911za700za7za7_3128za7, "pair", 4);
	      DEFINE_STRING(BGl_string2830z00zz__errorz00,
		BgL_bgl_string2830za700za7za7_3129za7,
		"' must be recompiled (see also -unsafev option).", 48);
	      DEFINE_STRING(BGl_string2912z00zz__errorz00,
		BgL_bgl_string2912za700za7za7_3130za7, "class", 5);
	      DEFINE_STRING(BGl_string2831z00zz__errorz00,
		BgL_bgl_string2831za700za7za7_3131za7, "At least `", 10);
	      DEFINE_STRING(BGl_string2913z00zz__errorz00,
		BgL_bgl_string2913za700za7za7_3132za7, "vector", 6);
	      DEFINE_STRING(BGl_string2832z00zz__errorz00,
		BgL_bgl_string2832za700za7za7_3133za7, "'.\n", 3);
	      DEFINE_STRING(BGl_string2914z00zz__errorz00,
		BgL_bgl_string2914za700za7za7_3134za7, "tvector", 7);
	      DEFINE_STRING(BGl_string2833z00zz__errorz00,
		BgL_bgl_string2833za700za7za7_3135za7,
		"' is inconsistently initialized by module `", 43);
	      DEFINE_STRING(BGl_string2915z00zz__errorz00,
		BgL_bgl_string2915za700za7za7_3136za7, "struct:", 7);
	      DEFINE_STRING(BGl_string2834z00zz__errorz00,
		BgL_bgl_string2834za700za7za7_3137za7, "Module `", 8);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_errorzf2locationzd2envz20zz__errorz00,
		BgL_bgl_za762errorza7f2locat3138z00, BGl_z62errorzf2locationz90zz__errorz00,
		0L, BUNSPEC, 5);
	      DEFINE_STRING(BGl_string2916z00zz__errorz00,
		BgL_bgl_string2916za700za7za7_3139za7, "input-port", 10);
	      DEFINE_STRING(BGl_string2835z00zz__errorz00,
		BgL_bgl_string2835za700za7za7_3140za7,
		":Inconsistent module initialization\n", 36);
	      DEFINE_STRING(BGl_string2917z00zz__errorz00,
		BgL_bgl_string2917za700za7za7_3141za7, "binary-port", 11);
	      DEFINE_STRING(BGl_string2836z00zz__errorz00,
		BgL_bgl_string2836za700za7za7_3142za7, "*** ERROR:", 10);
	      DEFINE_STRING(BGl_string2918z00zz__errorz00,
		BgL_bgl_string2918za700za7za7_3143za7, "cell", 4);
	      DEFINE_STRING(BGl_string2837z00zz__errorz00,
		BgL_bgl_string2837za700za7za7_3144za7, "&module-init-error", 18);
	      DEFINE_STRING(BGl_string2919z00zz__errorz00,
		BgL_bgl_string2919za700za7za7_3145za7, "foreign:", 8);
	      DEFINE_STRING(BGl_string2838z00zz__errorz00,
		BgL_bgl_string2838za700za7za7_3146za7, "bstring", 7);
	      DEFINE_STRING(BGl_string2839z00zz__errorz00,
		BgL_bgl_string2839za700za7za7_3147za7, "&error/c-location", 17);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_warningzf2locationzd2envz20zz__errorz00,
		BgL_bgl_za762warningza7f2loc3148z00, va_generic_entry,
		BGl_z62warningzf2locationz90zz__errorz00, BUNSPEC, -3);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_symbol2815z00zz__errorz00));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__errorz00));
		     ADD_ROOT((void *) (&BGl_symbol2850z00zz__errorz00));
		     ADD_ROOT((void *) (&BGl_symbol2859z00zz__errorz00));
		     ADD_ROOT((void *) (&BGl_symbol2861z00zz__errorz00));
		     ADD_ROOT((void *) (&BGl_symbol2881z00zz__errorz00));
		     ADD_ROOT((void *) (&BGl_symbol2883z00zz__errorz00));
		     ADD_ROOT((void *) (&BGl_symbol2894z00zz__errorz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long
		BgL_checksumz00_5510, char *BgL_fromz00_5511)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__errorz00))
				{
					BGl_requirezd2initializa7ationz75zz__errorz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__errorz00();
					BGl_cnstzd2initzd2zz__errorz00();
					BGl_importedzd2moduleszd2initz00zz__errorz00();
					return BGl_toplevelzd2initzd2zz__errorz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__errorz00(void)
	{
		{	/* Llib/error.scm 18 */
			BGl_symbol2815z00zz__errorz00 =
				bstring_to_symbol(BGl_string2816z00zz__errorz00);
			BGl_symbol2850z00zz__errorz00 =
				bstring_to_symbol(BGl_string2851z00zz__errorz00);
			BGl_symbol2859z00zz__errorz00 =
				bstring_to_symbol(BGl_string2860z00zz__errorz00);
			BGl_symbol2861z00zz__errorz00 =
				bstring_to_symbol(BGl_string2862z00zz__errorz00);
			BGl_symbol2881z00zz__errorz00 =
				bstring_to_symbol(BGl_string2882z00zz__errorz00);
			BGl_symbol2883z00zz__errorz00 =
				bstring_to_symbol(BGl_string2884z00zz__errorz00);
			return (BGl_symbol2894z00zz__errorz00 =
				bstring_to_symbol(BGl_string2895z00zz__errorz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__errorz00(void)
	{
		{	/* Llib/error.scm 18 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__errorz00(void)
	{
		{	/* Llib/error.scm 18 */
			BGl_signalz00zz__osz00(SIGFPE,
				BGl_sigfpezd2errorzd2handlerzd2envzd2zz__errorz00);
			BGl_signalz00zz__osz00(SIGTRAP,
				BGl_sigfpezd2errorzd2handlerzd2envzd2zz__errorz00);
			BGl_signalz00zz__osz00(SIGILL,
				BGl_sigillzd2errorzd2handlerzd2envzd2zz__errorz00);
			BGl_signalz00zz__osz00(SIGBUS,
				BGl_sigbuszd2errorzd2handlerzd2envzd2zz__errorz00);
			return BGl_signalz00zz__osz00(SIGSEGV,
				BGl_sigsegvzd2errorzd2handlerzd2envzd2zz__errorz00);
		}

	}



/* _get-trace-stack */
	obj_t BGl__getzd2tracezd2stackz00zz__errorz00(obj_t BgL_env1208z00_5,
		obj_t BgL_opt1207z00_4)
	{
		{	/* Llib/error.scm 308 */
			{	/* Llib/error.scm 308 */

				switch (VECTOR_LENGTH(BgL_opt1207z00_4))
					{
					case 0L:

						{	/* Llib/error.scm 308 */

							return BGl_getzd2tracezd2stackz00zz__errorz00(BFALSE);
						}
						break;
					case 1L:

						{	/* Llib/error.scm 308 */
							obj_t BgL_depthz00_1622;

							BgL_depthz00_1622 = VECTOR_REF(BgL_opt1207z00_4, 0L);
							{	/* Llib/error.scm 308 */

								return
									BGl_getzd2tracezd2stackz00zz__errorz00(BgL_depthz00_1622);
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* get-trace-stack */
	BGL_EXPORTED_DEF obj_t BGl_getzd2tracezd2stackz00zz__errorz00(obj_t
		BgL_depthz00_3)
	{
		{	/* Llib/error.scm 308 */
			{	/* Llib/error.scm 309 */
				obj_t BgL_dz00_1623;

				if (INTEGERP(BgL_depthz00_3))
					{	/* Llib/error.scm 310 */
						BgL_dz00_1623 = BgL_depthz00_3;
					}
				else
					{	/* Llib/error.scm 311 */
						obj_t BgL_g1040z00_1625;

						BgL_g1040z00_1625 =
							BGl_getenvz00zz__osz00(BGl_string2808z00zz__errorz00);
						if (CBOOL(BgL_g1040z00_1625))
							{	/* Ieee/fixnum.scm 1005 */

								BgL_dz00_1623 =
									BINT(BGl_stringzd2ze3integerz31zz__r4_numbers_6_5_fixnumz00
									(BgL_g1040z00_1625, 10L, 0L));
							}
						else
							{	/* Llib/error.scm 311 */
								{	/* Llib/error.scm 312 */
									int BgL_arg1338z00_1633;

									BgL_arg1338z00_1633 =
										BGl_bigloozd2tracezd2stackzd2depthzd2zz__paramz00();
									((bool_t) 1);
								}
								BgL_dz00_1623 =
									BINT(BGl_bigloozd2tracezd2stackzd2depthzd2zz__paramz00());
					}}
				{	/* Llib/error.scm 314 */
					int BgL_tmpz00_5547;

					BgL_tmpz00_5547 = CINT(BgL_dz00_1623);
					return bgl_get_trace_stack(BgL_tmpz00_5547);
				}
			}
		}

	}



/* the_failure */
	BGL_EXPORTED_DEF obj_t the_failure(obj_t BgL_procz00_6, obj_t BgL_msgz00_7,
		obj_t BgL_objz00_8)
	{
		{	/* Llib/error.scm 319 */
			{	/* Llib/error.scm 320 */
				bool_t BgL_test3152z00_5550;

				{	/* Llib/error.scm 320 */
					obj_t BgL_classz00_3494;

					BgL_classz00_3494 = BGl_z62exceptionz62zz__objectz00;
					if (BGL_OBJECTP(BgL_procz00_6))
						{	/* Llib/error.scm 320 */
							BgL_objectz00_bglt BgL_arg2721z00_3496;

							BgL_arg2721z00_3496 = (BgL_objectz00_bglt) (BgL_procz00_6);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Llib/error.scm 320 */
									long BgL_idxz00_3502;

									BgL_idxz00_3502 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg2721z00_3496);
									BgL_test3152z00_5550 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_3502 + 2L)) == BgL_classz00_3494);
								}
							else
								{	/* Llib/error.scm 320 */
									bool_t BgL_res2737z00_3527;

									{	/* Llib/error.scm 320 */
										obj_t BgL_oclassz00_3510;

										{	/* Llib/error.scm 320 */
											obj_t BgL_arg2728z00_3518;
											long BgL_arg2729z00_3519;

											BgL_arg2728z00_3518 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Llib/error.scm 320 */
												long BgL_arg2731z00_3520;

												BgL_arg2731z00_3520 =
													BGL_OBJECT_CLASS_NUM(BgL_arg2721z00_3496);
												BgL_arg2729z00_3519 =
													(BgL_arg2731z00_3520 - OBJECT_TYPE);
											}
											BgL_oclassz00_3510 =
												VECTOR_REF(BgL_arg2728z00_3518, BgL_arg2729z00_3519);
										}
										{	/* Llib/error.scm 320 */
											bool_t BgL__ortest_1198z00_3511;

											BgL__ortest_1198z00_3511 =
												(BgL_classz00_3494 == BgL_oclassz00_3510);
											if (BgL__ortest_1198z00_3511)
												{	/* Llib/error.scm 320 */
													BgL_res2737z00_3527 = BgL__ortest_1198z00_3511;
												}
											else
												{	/* Llib/error.scm 320 */
													long BgL_odepthz00_3512;

													{	/* Llib/error.scm 320 */
														obj_t BgL_arg2717z00_3513;

														BgL_arg2717z00_3513 = (BgL_oclassz00_3510);
														BgL_odepthz00_3512 =
															BGL_CLASS_DEPTH(BgL_arg2717z00_3513);
													}
													if ((2L < BgL_odepthz00_3512))
														{	/* Llib/error.scm 320 */
															obj_t BgL_arg2715z00_3515;

															{	/* Llib/error.scm 320 */
																obj_t BgL_arg2716z00_3516;

																BgL_arg2716z00_3516 = (BgL_oclassz00_3510);
																BgL_arg2715z00_3515 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg2716z00_3516,
																	2L);
															}
															BgL_res2737z00_3527 =
																(BgL_arg2715z00_3515 == BgL_classz00_3494);
														}
													else
														{	/* Llib/error.scm 320 */
															BgL_res2737z00_3527 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test3152z00_5550 = BgL_res2737z00_3527;
								}
						}
					else
						{	/* Llib/error.scm 320 */
							BgL_test3152z00_5550 = ((bool_t) 0);
						}
				}
				if (BgL_test3152z00_5550)
					{	/* Llib/error.scm 320 */
						return BGl_raisez00zz__errorz00(BgL_procz00_6);
					}
				else
					{	/* Llib/error.scm 320 */
						BGL_TAIL return
							BGl_errorz00zz__errorz00(BgL_procz00_6, BgL_msgz00_7,
							BgL_objz00_8);
					}
			}
		}

	}



/* &the_failure */
	obj_t BGl_z62the_failurez62zz__errorz00(obj_t BgL_envz00_5239,
		obj_t BgL_procz00_5240, obj_t BgL_msgz00_5241, obj_t BgL_objz00_5242)
	{
		{	/* Llib/error.scm 319 */
			return the_failure(BgL_procz00_5240, BgL_msgz00_5241, BgL_objz00_5242);
		}

	}



/* error/errno */
	BGL_EXPORTED_DEF obj_t bgl_system_failure(int BgL_sysnoz00_9,
		obj_t BgL_procz00_10, obj_t BgL_msgz00_11, obj_t BgL_objz00_12)
	{
		{	/* Llib/error.scm 327 */
			if (((long) (BgL_sysnoz00_9) == (long) (BGL_IO_ERROR)))
				{	/* Llib/error.scm 331 */
					BgL_z62iozd2errorzb0_bglt BgL_arg1341z00_1636;

					{	/* Llib/error.scm 331 */
						BgL_z62iozd2errorzb0_bglt BgL_new1043z00_1637;

						{	/* Llib/error.scm 331 */
							BgL_z62iozd2errorzb0_bglt BgL_new1042z00_1640;

							BgL_new1042z00_1640 =
								((BgL_z62iozd2errorzb0_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_z62iozd2errorzb0_bgl))));
							{	/* Llib/error.scm 331 */
								long BgL_arg1344z00_1641;

								BgL_arg1344z00_1641 =
									BGL_CLASS_NUM(BGl_z62iozd2errorzb0zz__objectz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1042z00_1640),
									BgL_arg1344z00_1641);
							}
							BgL_new1043z00_1637 = BgL_new1042z00_1640;
						}
						((((BgL_z62exceptionz62_bglt) COBJECT(
										((BgL_z62exceptionz62_bglt) BgL_new1043z00_1637)))->
								BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
						((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
											BgL_new1043z00_1637)))->BgL_locationz00) =
							((obj_t) BFALSE), BUNSPEC);
						{
							obj_t BgL_auxz00_5588;

							{	/* Llib/error.scm 331 */
								obj_t BgL_arg1342z00_1638;

								{	/* Llib/error.scm 331 */
									obj_t BgL_arg1343z00_1639;

									{	/* Llib/error.scm 331 */
										obj_t BgL_classz00_3533;

										BgL_classz00_3533 = BGl_z62iozd2errorzb0zz__objectz00;
										BgL_arg1343z00_1639 =
											BGL_CLASS_ALL_FIELDS(BgL_classz00_3533);
									}
									BgL_arg1342z00_1638 = VECTOR_REF(BgL_arg1343z00_1639, 2L);
								}
								BgL_auxz00_5588 =
									BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
									(BgL_arg1342z00_1638);
							}
							((((BgL_z62exceptionz62_bglt) COBJECT(
											((BgL_z62exceptionz62_bglt) BgL_new1043z00_1637)))->
									BgL_stackz00) = ((obj_t) BgL_auxz00_5588), BUNSPEC);
						}
						((((BgL_z62errorz62_bglt) COBJECT(
										((BgL_z62errorz62_bglt) BgL_new1043z00_1637)))->
								BgL_procz00) = ((obj_t) BgL_procz00_10), BUNSPEC);
						((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
											BgL_new1043z00_1637)))->BgL_msgz00) =
							((obj_t) BgL_msgz00_11), BUNSPEC);
						((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
											BgL_new1043z00_1637)))->BgL_objz00) =
							((obj_t) BgL_objz00_12), BUNSPEC);
						BgL_arg1341z00_1636 = BgL_new1043z00_1637;
					}
					return BGl_raisez00zz__errorz00(((obj_t) BgL_arg1341z00_1636));
				}
			else
				{	/* Llib/error.scm 329 */
					if (((long) (BgL_sysnoz00_9) == (long) (BGL_IO_PORT_ERROR)))
						{	/* Llib/error.scm 334 */
							BgL_z62iozd2portzd2errorz62_bglt BgL_arg1346z00_1643;

							{	/* Llib/error.scm 334 */
								BgL_z62iozd2portzd2errorz62_bglt BgL_new1045z00_1644;

								{	/* Llib/error.scm 334 */
									BgL_z62iozd2portzd2errorz62_bglt BgL_new1044z00_1647;

									BgL_new1044z00_1647 =
										((BgL_z62iozd2portzd2errorz62_bglt)
										BOBJECT(GC_MALLOC(sizeof(struct
													BgL_z62iozd2portzd2errorz62_bgl))));
									{	/* Llib/error.scm 334 */
										long BgL_arg1349z00_1648;

										BgL_arg1349z00_1648 =
											BGL_CLASS_NUM(BGl_z62iozd2portzd2errorz62zz__objectz00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1044z00_1647),
											BgL_arg1349z00_1648);
									}
									BgL_new1045z00_1644 = BgL_new1044z00_1647;
								}
								((((BgL_z62exceptionz62_bglt) COBJECT(
												((BgL_z62exceptionz62_bglt) BgL_new1045z00_1644)))->
										BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
								((((BgL_z62exceptionz62_bglt)
											COBJECT(((BgL_z62exceptionz62_bglt)
													BgL_new1045z00_1644)))->BgL_locationz00) =
									((obj_t) BFALSE), BUNSPEC);
								{
									obj_t BgL_auxz00_5614;

									{	/* Llib/error.scm 334 */
										obj_t BgL_arg1347z00_1645;

										{	/* Llib/error.scm 334 */
											obj_t BgL_arg1348z00_1646;

											{	/* Llib/error.scm 334 */
												obj_t BgL_classz00_3540;

												BgL_classz00_3540 =
													BGl_z62iozd2portzd2errorz62zz__objectz00;
												BgL_arg1348z00_1646 =
													BGL_CLASS_ALL_FIELDS(BgL_classz00_3540);
											}
											BgL_arg1347z00_1645 = VECTOR_REF(BgL_arg1348z00_1646, 2L);
										}
										BgL_auxz00_5614 =
											BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
											(BgL_arg1347z00_1645);
									}
									((((BgL_z62exceptionz62_bglt) COBJECT(
													((BgL_z62exceptionz62_bglt) BgL_new1045z00_1644)))->
											BgL_stackz00) = ((obj_t) BgL_auxz00_5614), BUNSPEC);
								}
								((((BgL_z62errorz62_bglt) COBJECT(
												((BgL_z62errorz62_bglt) BgL_new1045z00_1644)))->
										BgL_procz00) = ((obj_t) BgL_procz00_10), BUNSPEC);
								((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
													BgL_new1045z00_1644)))->BgL_msgz00) =
									((obj_t) BgL_msgz00_11), BUNSPEC);
								((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
													BgL_new1045z00_1644)))->BgL_objz00) =
									((obj_t) BgL_objz00_12), BUNSPEC);
								BgL_arg1346z00_1643 = BgL_new1045z00_1644;
							}
							return BGl_raisez00zz__errorz00(((obj_t) BgL_arg1346z00_1643));
						}
					else
						{	/* Llib/error.scm 332 */
							if (((long) (BgL_sysnoz00_9) == (long) (BGL_IO_READ_ERROR)))
								{	/* Llib/error.scm 337 */
									BgL_z62iozd2readzd2errorz62_bglt BgL_arg1351z00_1650;

									{	/* Llib/error.scm 337 */
										BgL_z62iozd2readzd2errorz62_bglt BgL_new1047z00_1651;

										{	/* Llib/error.scm 337 */
											BgL_z62iozd2readzd2errorz62_bglt BgL_new1046z00_1654;

											BgL_new1046z00_1654 =
												((BgL_z62iozd2readzd2errorz62_bglt)
												BOBJECT(GC_MALLOC(sizeof(struct
															BgL_z62iozd2readzd2errorz62_bgl))));
											{	/* Llib/error.scm 337 */
												long BgL_arg1356z00_1655;

												BgL_arg1356z00_1655 =
													BGL_CLASS_NUM
													(BGl_z62iozd2readzd2errorz62zz__objectz00);
												BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
														BgL_new1046z00_1654), BgL_arg1356z00_1655);
											}
											BgL_new1047z00_1651 = BgL_new1046z00_1654;
										}
										((((BgL_z62exceptionz62_bglt) COBJECT(
														((BgL_z62exceptionz62_bglt) BgL_new1047z00_1651)))->
												BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
										((((BgL_z62exceptionz62_bglt)
													COBJECT(((BgL_z62exceptionz62_bglt)
															BgL_new1047z00_1651)))->BgL_locationz00) =
											((obj_t) BFALSE), BUNSPEC);
										{
											obj_t BgL_auxz00_5640;

											{	/* Llib/error.scm 337 */
												obj_t BgL_arg1352z00_1652;

												{	/* Llib/error.scm 337 */
													obj_t BgL_arg1354z00_1653;

													{	/* Llib/error.scm 337 */
														obj_t BgL_classz00_3547;

														BgL_classz00_3547 =
															BGl_z62iozd2readzd2errorz62zz__objectz00;
														BgL_arg1354z00_1653 =
															BGL_CLASS_ALL_FIELDS(BgL_classz00_3547);
													}
													BgL_arg1352z00_1652 =
														VECTOR_REF(BgL_arg1354z00_1653, 2L);
												}
												BgL_auxz00_5640 =
													BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
													(BgL_arg1352z00_1652);
											}
											((((BgL_z62exceptionz62_bglt) COBJECT(
															((BgL_z62exceptionz62_bglt)
																BgL_new1047z00_1651)))->BgL_stackz00) =
												((obj_t) BgL_auxz00_5640), BUNSPEC);
										}
										((((BgL_z62errorz62_bglt) COBJECT(
														((BgL_z62errorz62_bglt) BgL_new1047z00_1651)))->
												BgL_procz00) = ((obj_t) BgL_procz00_10), BUNSPEC);
										((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
															BgL_new1047z00_1651)))->BgL_msgz00) =
											((obj_t) BgL_msgz00_11), BUNSPEC);
										((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
															BgL_new1047z00_1651)))->BgL_objz00) =
											((obj_t) BgL_objz00_12), BUNSPEC);
										BgL_arg1351z00_1650 = BgL_new1047z00_1651;
									}
									return
										BGl_raisez00zz__errorz00(((obj_t) BgL_arg1351z00_1650));
								}
							else
								{	/* Llib/error.scm 335 */
									if (((long) (BgL_sysnoz00_9) == (long) (BGL_IO_WRITE_ERROR)))
										{	/* Llib/error.scm 340 */
											BgL_z62iozd2writezd2errorz62_bglt BgL_arg1358z00_1657;

											{	/* Llib/error.scm 340 */
												BgL_z62iozd2writezd2errorz62_bglt BgL_new1049z00_1658;

												{	/* Llib/error.scm 340 */
													BgL_z62iozd2writezd2errorz62_bglt BgL_new1048z00_1661;

													BgL_new1048z00_1661 =
														((BgL_z62iozd2writezd2errorz62_bglt)
														BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_z62iozd2writezd2errorz62_bgl))));
													{	/* Llib/error.scm 340 */
														long BgL_arg1361z00_1662;

														BgL_arg1361z00_1662 =
															BGL_CLASS_NUM
															(BGl_z62iozd2writezd2errorz62zz__objectz00);
														BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
																BgL_new1048z00_1661), BgL_arg1361z00_1662);
													}
													BgL_new1049z00_1658 = BgL_new1048z00_1661;
												}
												((((BgL_z62exceptionz62_bglt) COBJECT(
																((BgL_z62exceptionz62_bglt)
																	BgL_new1049z00_1658)))->BgL_fnamez00) =
													((obj_t) BFALSE), BUNSPEC);
												((((BgL_z62exceptionz62_bglt)
															COBJECT(((BgL_z62exceptionz62_bglt)
																	BgL_new1049z00_1658)))->BgL_locationz00) =
													((obj_t) BFALSE), BUNSPEC);
												{
													obj_t BgL_auxz00_5666;

													{	/* Llib/error.scm 340 */
														obj_t BgL_arg1359z00_1659;

														{	/* Llib/error.scm 340 */
															obj_t BgL_arg1360z00_1660;

															{	/* Llib/error.scm 340 */
																obj_t BgL_classz00_3554;

																BgL_classz00_3554 =
																	BGl_z62iozd2writezd2errorz62zz__objectz00;
																BgL_arg1360z00_1660 =
																	BGL_CLASS_ALL_FIELDS(BgL_classz00_3554);
															}
															BgL_arg1359z00_1659 =
																VECTOR_REF(BgL_arg1360z00_1660, 2L);
														}
														BgL_auxz00_5666 =
															BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
															(BgL_arg1359z00_1659);
													}
													((((BgL_z62exceptionz62_bglt) COBJECT(
																	((BgL_z62exceptionz62_bglt)
																		BgL_new1049z00_1658)))->BgL_stackz00) =
														((obj_t) BgL_auxz00_5666), BUNSPEC);
												}
												((((BgL_z62errorz62_bglt) COBJECT(
																((BgL_z62errorz62_bglt) BgL_new1049z00_1658)))->
														BgL_procz00) = ((obj_t) BgL_procz00_10), BUNSPEC);
												((((BgL_z62errorz62_bglt)
															COBJECT(((BgL_z62errorz62_bglt)
																	BgL_new1049z00_1658)))->BgL_msgz00) =
													((obj_t) BgL_msgz00_11), BUNSPEC);
												((((BgL_z62errorz62_bglt)
															COBJECT(((BgL_z62errorz62_bglt)
																	BgL_new1049z00_1658)))->BgL_objz00) =
													((obj_t) BgL_objz00_12), BUNSPEC);
												BgL_arg1358z00_1657 = BgL_new1049z00_1658;
											}
											return
												BGl_raisez00zz__errorz00(((obj_t) BgL_arg1358z00_1657));
										}
									else
										{	/* Llib/error.scm 338 */
											if (
												((long) (BgL_sysnoz00_9) ==
													(long) (BGL_IO_UNKNOWN_HOST_ERROR)))
												{	/* Llib/error.scm 343 */
													BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt
														BgL_arg1363z00_1664;
													{	/* Llib/error.scm 343 */
														BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt
															BgL_new1051z00_1665;
														{	/* Llib/error.scm 343 */
															BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt
																BgL_new1050z00_1668;
															BgL_new1050z00_1668 =
																((BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_z62iozd2unknownzd2hostzd2errorzb0_bgl))));
															{	/* Llib/error.scm 343 */
																long BgL_arg1366z00_1669;

																BgL_arg1366z00_1669 =
																	BGL_CLASS_NUM
																	(BGl_z62iozd2unknownzd2hostzd2errorzb0zz__objectz00);
																BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
																		BgL_new1050z00_1668), BgL_arg1366z00_1669);
															}
															BgL_new1051z00_1665 = BgL_new1050z00_1668;
														}
														((((BgL_z62exceptionz62_bglt) COBJECT(
																		((BgL_z62exceptionz62_bglt)
																			BgL_new1051z00_1665)))->BgL_fnamez00) =
															((obj_t) BFALSE), BUNSPEC);
														((((BgL_z62exceptionz62_bglt)
																	COBJECT(((BgL_z62exceptionz62_bglt)
																			BgL_new1051z00_1665)))->BgL_locationz00) =
															((obj_t) BFALSE), BUNSPEC);
														{
															obj_t BgL_auxz00_5692;

															{	/* Llib/error.scm 343 */
																obj_t BgL_arg1364z00_1666;

																{	/* Llib/error.scm 343 */
																	obj_t BgL_arg1365z00_1667;

																	{	/* Llib/error.scm 343 */
																		obj_t BgL_classz00_3561;

																		BgL_classz00_3561 =
																			BGl_z62iozd2unknownzd2hostzd2errorzb0zz__objectz00;
																		BgL_arg1365z00_1667 =
																			BGL_CLASS_ALL_FIELDS(BgL_classz00_3561);
																	}
																	BgL_arg1364z00_1666 =
																		VECTOR_REF(BgL_arg1365z00_1667, 2L);
																}
																BgL_auxz00_5692 =
																	BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
																	(BgL_arg1364z00_1666);
															}
															((((BgL_z62exceptionz62_bglt) COBJECT(
																			((BgL_z62exceptionz62_bglt)
																				BgL_new1051z00_1665)))->BgL_stackz00) =
																((obj_t) BgL_auxz00_5692), BUNSPEC);
														}
														((((BgL_z62errorz62_bglt) COBJECT(
																		((BgL_z62errorz62_bglt)
																			BgL_new1051z00_1665)))->BgL_procz00) =
															((obj_t) BgL_procz00_10), BUNSPEC);
														((((BgL_z62errorz62_bglt)
																	COBJECT(((BgL_z62errorz62_bglt)
																			BgL_new1051z00_1665)))->BgL_msgz00) =
															((obj_t) BgL_msgz00_11), BUNSPEC);
														((((BgL_z62errorz62_bglt)
																	COBJECT(((BgL_z62errorz62_bglt)
																			BgL_new1051z00_1665)))->BgL_objz00) =
															((obj_t) BgL_objz00_12), BUNSPEC);
														BgL_arg1363z00_1664 = BgL_new1051z00_1665;
													}
													return
														BGl_raisez00zz__errorz00(
														((obj_t) BgL_arg1363z00_1664));
												}
											else
												{	/* Llib/error.scm 341 */
													if (
														((long) (BgL_sysnoz00_9) ==
															(long) (BGL_IO_FILE_NOT_FOUND_ERROR)))
														{	/* Llib/error.scm 346 */
															BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt
																BgL_arg1368z00_1671;
															{	/* Llib/error.scm 346 */
																BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt
																	BgL_new1053z00_1672;
																{	/* Llib/error.scm 346 */
																	BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt
																		BgL_new1052z00_1675;
																	BgL_new1052z00_1675 =
																		(
																		(BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_z62iozd2filezd2notzd2foundzd2errorz62_bgl))));
																	{	/* Llib/error.scm 346 */
																		long BgL_arg1371z00_1676;

																		BgL_arg1371z00_1676 =
																			BGL_CLASS_NUM
																			(BGl_z62iozd2filezd2notzd2foundzd2errorz62zz__objectz00);
																		BGL_OBJECT_CLASS_NUM_SET((
																				(BgL_objectz00_bglt)
																				BgL_new1052z00_1675),
																			BgL_arg1371z00_1676);
																	}
																	BgL_new1053z00_1672 = BgL_new1052z00_1675;
																}
																((((BgL_z62exceptionz62_bglt) COBJECT(
																				((BgL_z62exceptionz62_bglt)
																					BgL_new1053z00_1672)))->
																		BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
																((((BgL_z62exceptionz62_bglt)
																			COBJECT(((BgL_z62exceptionz62_bglt)
																					BgL_new1053z00_1672)))->
																		BgL_locationz00) =
																	((obj_t) BFALSE), BUNSPEC);
																{
																	obj_t BgL_auxz00_5718;

																	{	/* Llib/error.scm 346 */
																		obj_t BgL_arg1369z00_1673;

																		{	/* Llib/error.scm 346 */
																			obj_t BgL_arg1370z00_1674;

																			{	/* Llib/error.scm 346 */
																				obj_t BgL_classz00_3568;

																				BgL_classz00_3568 =
																					BGl_z62iozd2filezd2notzd2foundzd2errorz62zz__objectz00;
																				BgL_arg1370z00_1674 =
																					BGL_CLASS_ALL_FIELDS
																					(BgL_classz00_3568);
																			}
																			BgL_arg1369z00_1673 =
																				VECTOR_REF(BgL_arg1370z00_1674, 2L);
																		}
																		BgL_auxz00_5718 =
																			BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
																			(BgL_arg1369z00_1673);
																	}
																	((((BgL_z62exceptionz62_bglt) COBJECT(
																					((BgL_z62exceptionz62_bglt)
																						BgL_new1053z00_1672)))->
																			BgL_stackz00) =
																		((obj_t) BgL_auxz00_5718), BUNSPEC);
																}
																((((BgL_z62errorz62_bglt) COBJECT(
																				((BgL_z62errorz62_bglt)
																					BgL_new1053z00_1672)))->BgL_procz00) =
																	((obj_t) BgL_procz00_10), BUNSPEC);
																((((BgL_z62errorz62_bglt)
																			COBJECT(((BgL_z62errorz62_bglt)
																					BgL_new1053z00_1672)))->BgL_msgz00) =
																	((obj_t) BgL_msgz00_11), BUNSPEC);
																((((BgL_z62errorz62_bglt)
																			COBJECT(((BgL_z62errorz62_bglt)
																					BgL_new1053z00_1672)))->BgL_objz00) =
																	((obj_t) BgL_objz00_12), BUNSPEC);
																BgL_arg1368z00_1671 = BgL_new1053z00_1672;
															}
															return
																BGl_raisez00zz__errorz00(
																((obj_t) BgL_arg1368z00_1671));
														}
													else
														{	/* Llib/error.scm 344 */
															if (
																((long) (BgL_sysnoz00_9) ==
																	(long) (BGL_IO_PARSE_ERROR)))
																{	/* Llib/error.scm 349 */
																	BgL_z62iozd2parsezd2errorz62_bglt
																		BgL_arg1373z00_1678;
																	{	/* Llib/error.scm 349 */
																		BgL_z62iozd2parsezd2errorz62_bglt
																			BgL_new1055z00_1679;
																		{	/* Llib/error.scm 349 */
																			BgL_z62iozd2parsezd2errorz62_bglt
																				BgL_new1054z00_1682;
																			BgL_new1054z00_1682 =
																				((BgL_z62iozd2parsezd2errorz62_bglt)
																				BOBJECT(GC_MALLOC(sizeof(struct
																							BgL_z62iozd2parsezd2errorz62_bgl))));
																			{	/* Llib/error.scm 349 */
																				long BgL_arg1377z00_1683;

																				BgL_arg1377z00_1683 =
																					BGL_CLASS_NUM
																					(BGl_z62iozd2parsezd2errorz62zz__objectz00);
																				BGL_OBJECT_CLASS_NUM_SET((
																						(BgL_objectz00_bglt)
																						BgL_new1054z00_1682),
																					BgL_arg1377z00_1683);
																			}
																			BgL_new1055z00_1679 = BgL_new1054z00_1682;
																		}
																		((((BgL_z62exceptionz62_bglt) COBJECT(
																						((BgL_z62exceptionz62_bglt)
																							BgL_new1055z00_1679)))->
																				BgL_fnamez00) =
																			((obj_t) BFALSE), BUNSPEC);
																		((((BgL_z62exceptionz62_bglt)
																					COBJECT(((BgL_z62exceptionz62_bglt)
																							BgL_new1055z00_1679)))->
																				BgL_locationz00) =
																			((obj_t) BFALSE), BUNSPEC);
																		{
																			obj_t BgL_auxz00_5744;

																			{	/* Llib/error.scm 349 */
																				obj_t BgL_arg1375z00_1680;

																				{	/* Llib/error.scm 349 */
																					obj_t BgL_arg1376z00_1681;

																					{	/* Llib/error.scm 349 */
																						obj_t BgL_classz00_3575;

																						BgL_classz00_3575 =
																							BGl_z62iozd2parsezd2errorz62zz__objectz00;
																						BgL_arg1376z00_1681 =
																							BGL_CLASS_ALL_FIELDS
																							(BgL_classz00_3575);
																					}
																					BgL_arg1375z00_1680 =
																						VECTOR_REF(BgL_arg1376z00_1681, 2L);
																				}
																				BgL_auxz00_5744 =
																					BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
																					(BgL_arg1375z00_1680);
																			}
																			((((BgL_z62exceptionz62_bglt) COBJECT(
																							((BgL_z62exceptionz62_bglt)
																								BgL_new1055z00_1679)))->
																					BgL_stackz00) =
																				((obj_t) BgL_auxz00_5744), BUNSPEC);
																		}
																		((((BgL_z62errorz62_bglt) COBJECT(
																						((BgL_z62errorz62_bglt)
																							BgL_new1055z00_1679)))->
																				BgL_procz00) =
																			((obj_t) BgL_procz00_10), BUNSPEC);
																		((((BgL_z62errorz62_bglt)
																					COBJECT(((BgL_z62errorz62_bglt)
																							BgL_new1055z00_1679)))->
																				BgL_msgz00) =
																			((obj_t) BgL_msgz00_11), BUNSPEC);
																		((((BgL_z62errorz62_bglt)
																					COBJECT(((BgL_z62errorz62_bglt)
																							BgL_new1055z00_1679)))->
																				BgL_objz00) =
																			((obj_t) BgL_objz00_12), BUNSPEC);
																		BgL_arg1373z00_1678 = BgL_new1055z00_1679;
																	}
																	return
																		BGl_raisez00zz__errorz00(
																		((obj_t) BgL_arg1373z00_1678));
																}
															else
																{	/* Llib/error.scm 347 */
																	if (
																		((long) (BgL_sysnoz00_9) ==
																			(long) (BGL_IO_MALFORMED_URL_ERROR)))
																		{	/* Llib/error.scm 352 */
																			BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt
																				BgL_arg1379z00_1685;
																			{	/* Llib/error.scm 352 */
																				BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt
																					BgL_new1057z00_1686;
																				{	/* Llib/error.scm 352 */
																					BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt
																						BgL_new1056z00_1689;
																					BgL_new1056z00_1689 =
																						(
																						(BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt)
																						BOBJECT(GC_MALLOC(sizeof(struct
																									BgL_z62iozd2malformedzd2urlzd2errorzb0_bgl))));
																					{	/* Llib/error.scm 352 */
																						long BgL_arg1383z00_1690;

																						BgL_arg1383z00_1690 =
																							BGL_CLASS_NUM
																							(BGl_z62iozd2malformedzd2urlzd2errorzb0zz__objectz00);
																						BGL_OBJECT_CLASS_NUM_SET((
																								(BgL_objectz00_bglt)
																								BgL_new1056z00_1689),
																							BgL_arg1383z00_1690);
																					}
																					BgL_new1057z00_1686 =
																						BgL_new1056z00_1689;
																				}
																				((((BgL_z62exceptionz62_bglt) COBJECT(
																								((BgL_z62exceptionz62_bglt)
																									BgL_new1057z00_1686)))->
																						BgL_fnamez00) =
																					((obj_t) BFALSE), BUNSPEC);
																				((((BgL_z62exceptionz62_bglt)
																							COBJECT((
																									(BgL_z62exceptionz62_bglt)
																									BgL_new1057z00_1686)))->
																						BgL_locationz00) =
																					((obj_t) BFALSE), BUNSPEC);
																				{
																					obj_t BgL_auxz00_5770;

																					{	/* Llib/error.scm 352 */
																						obj_t BgL_arg1380z00_1687;

																						{	/* Llib/error.scm 352 */
																							obj_t BgL_arg1382z00_1688;

																							{	/* Llib/error.scm 352 */
																								obj_t BgL_classz00_3582;

																								BgL_classz00_3582 =
																									BGl_z62iozd2malformedzd2urlzd2errorzb0zz__objectz00;
																								BgL_arg1382z00_1688 =
																									BGL_CLASS_ALL_FIELDS
																									(BgL_classz00_3582);
																							}
																							BgL_arg1380z00_1687 =
																								VECTOR_REF(BgL_arg1382z00_1688,
																								2L);
																						}
																						BgL_auxz00_5770 =
																							BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
																							(BgL_arg1380z00_1687);
																					}
																					((((BgL_z62exceptionz62_bglt) COBJECT(
																									((BgL_z62exceptionz62_bglt)
																										BgL_new1057z00_1686)))->
																							BgL_stackz00) =
																						((obj_t) BgL_auxz00_5770), BUNSPEC);
																				}
																				((((BgL_z62errorz62_bglt) COBJECT(
																								((BgL_z62errorz62_bglt)
																									BgL_new1057z00_1686)))->
																						BgL_procz00) =
																					((obj_t) BgL_procz00_10), BUNSPEC);
																				((((BgL_z62errorz62_bglt)
																							COBJECT(((BgL_z62errorz62_bglt)
																									BgL_new1057z00_1686)))->
																						BgL_msgz00) =
																					((obj_t) BgL_msgz00_11), BUNSPEC);
																				((((BgL_z62errorz62_bglt)
																							COBJECT(((BgL_z62errorz62_bglt)
																									BgL_new1057z00_1686)))->
																						BgL_objz00) =
																					((obj_t) BgL_objz00_12), BUNSPEC);
																				BgL_arg1379z00_1685 =
																					BgL_new1057z00_1686;
																			}
																			return
																				BGl_raisez00zz__errorz00(
																				((obj_t) BgL_arg1379z00_1685));
																		}
																	else
																		{	/* Llib/error.scm 350 */
																			if (
																				((long) (BgL_sysnoz00_9) ==
																					(long) (BGL_IO_SIGPIPE_ERROR)))
																				{	/* Llib/error.scm 355 */
																					BgL_z62iozd2sigpipezd2errorz62_bglt
																						BgL_arg1387z00_1692;
																					{	/* Llib/error.scm 355 */
																						BgL_z62iozd2sigpipezd2errorz62_bglt
																							BgL_new1059z00_1693;
																						{	/* Llib/error.scm 355 */
																							BgL_z62iozd2sigpipezd2errorz62_bglt
																								BgL_new1058z00_1696;
																							BgL_new1058z00_1696 =
																								(
																								(BgL_z62iozd2sigpipezd2errorz62_bglt)
																								BOBJECT(GC_MALLOC(sizeof(struct
																											BgL_z62iozd2sigpipezd2errorz62_bgl))));
																							{	/* Llib/error.scm 355 */
																								long BgL_arg1390z00_1697;

																								BgL_arg1390z00_1697 =
																									BGL_CLASS_NUM
																									(BGl_z62iozd2sigpipezd2errorz62zz__objectz00);
																								BGL_OBJECT_CLASS_NUM_SET((
																										(BgL_objectz00_bglt)
																										BgL_new1058z00_1696),
																									BgL_arg1390z00_1697);
																							}
																							BgL_new1059z00_1693 =
																								BgL_new1058z00_1696;
																						}
																						((((BgL_z62exceptionz62_bglt)
																									COBJECT((
																											(BgL_z62exceptionz62_bglt)
																											BgL_new1059z00_1693)))->
																								BgL_fnamez00) =
																							((obj_t) BFALSE), BUNSPEC);
																						((((BgL_z62exceptionz62_bglt)
																									COBJECT((
																											(BgL_z62exceptionz62_bglt)
																											BgL_new1059z00_1693)))->
																								BgL_locationz00) =
																							((obj_t) BFALSE), BUNSPEC);
																						{
																							obj_t BgL_auxz00_5796;

																							{	/* Llib/error.scm 355 */
																								obj_t BgL_arg1388z00_1694;

																								{	/* Llib/error.scm 355 */
																									obj_t BgL_arg1389z00_1695;

																									{	/* Llib/error.scm 355 */
																										obj_t BgL_classz00_3589;

																										BgL_classz00_3589 =
																											BGl_z62iozd2sigpipezd2errorz62zz__objectz00;
																										BgL_arg1389z00_1695 =
																											BGL_CLASS_ALL_FIELDS
																											(BgL_classz00_3589);
																									}
																									BgL_arg1388z00_1694 =
																										VECTOR_REF
																										(BgL_arg1389z00_1695, 2L);
																								}
																								BgL_auxz00_5796 =
																									BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
																									(BgL_arg1388z00_1694);
																							}
																							((((BgL_z62exceptionz62_bglt)
																										COBJECT((
																												(BgL_z62exceptionz62_bglt)
																												BgL_new1059z00_1693)))->
																									BgL_stackz00) =
																								((obj_t) BgL_auxz00_5796),
																								BUNSPEC);
																						}
																						((((BgL_z62errorz62_bglt) COBJECT(
																										((BgL_z62errorz62_bglt)
																											BgL_new1059z00_1693)))->
																								BgL_procz00) =
																							((obj_t) BgL_procz00_10),
																							BUNSPEC);
																						((((BgL_z62errorz62_bglt)
																									COBJECT((
																											(BgL_z62errorz62_bglt)
																											BgL_new1059z00_1693)))->
																								BgL_msgz00) =
																							((obj_t) BgL_msgz00_11), BUNSPEC);
																						((((BgL_z62errorz62_bglt)
																									COBJECT((
																											(BgL_z62errorz62_bglt)
																											BgL_new1059z00_1693)))->
																								BgL_objz00) =
																							((obj_t) BgL_objz00_12), BUNSPEC);
																						BgL_arg1387z00_1692 =
																							BgL_new1059z00_1693;
																					}
																					return
																						BGl_raisez00zz__errorz00(
																						((obj_t) BgL_arg1387z00_1692));
																				}
																			else
																				{	/* Llib/error.scm 353 */
																					if (
																						((long) (BgL_sysnoz00_9) ==
																							(long) (BGL_IO_TIMEOUT_ERROR)))
																						{	/* Llib/error.scm 358 */
																							BgL_z62iozd2timeoutzd2errorz62_bglt
																								BgL_arg1392z00_1699;
																							{	/* Llib/error.scm 358 */
																								BgL_z62iozd2timeoutzd2errorz62_bglt
																									BgL_new1062z00_1700;
																								{	/* Llib/error.scm 358 */
																									BgL_z62iozd2timeoutzd2errorz62_bglt
																										BgL_new1061z00_1703;
																									BgL_new1061z00_1703 =
																										(
																										(BgL_z62iozd2timeoutzd2errorz62_bglt)
																										BOBJECT(GC_MALLOC(sizeof
																												(struct
																													BgL_z62iozd2timeoutzd2errorz62_bgl))));
																									{	/* Llib/error.scm 358 */
																										long BgL_arg1395z00_1704;

																										BgL_arg1395z00_1704 =
																											BGL_CLASS_NUM
																											(BGl_z62iozd2timeoutzd2errorz62zz__objectz00);
																										BGL_OBJECT_CLASS_NUM_SET((
																												(BgL_objectz00_bglt)
																												BgL_new1061z00_1703),
																											BgL_arg1395z00_1704);
																									}
																									BgL_new1062z00_1700 =
																										BgL_new1061z00_1703;
																								}
																								((((BgL_z62exceptionz62_bglt)
																											COBJECT((
																													(BgL_z62exceptionz62_bglt)
																													BgL_new1062z00_1700)))->
																										BgL_fnamez00) =
																									((obj_t) BFALSE), BUNSPEC);
																								((((BgL_z62exceptionz62_bglt)
																											COBJECT((
																													(BgL_z62exceptionz62_bglt)
																													BgL_new1062z00_1700)))->
																										BgL_locationz00) =
																									((obj_t) BFALSE), BUNSPEC);
																								{
																									obj_t BgL_auxz00_5822;

																									{	/* Llib/error.scm 358 */
																										obj_t BgL_arg1393z00_1701;

																										{	/* Llib/error.scm 358 */
																											obj_t BgL_arg1394z00_1702;

																											{	/* Llib/error.scm 358 */
																												obj_t BgL_classz00_3596;

																												BgL_classz00_3596 =
																													BGl_z62iozd2timeoutzd2errorz62zz__objectz00;
																												BgL_arg1394z00_1702 =
																													BGL_CLASS_ALL_FIELDS
																													(BgL_classz00_3596);
																											}
																											BgL_arg1393z00_1701 =
																												VECTOR_REF
																												(BgL_arg1394z00_1702,
																												2L);
																										}
																										BgL_auxz00_5822 =
																											BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
																											(BgL_arg1393z00_1701);
																									}
																									((((BgL_z62exceptionz62_bglt)
																												COBJECT((
																														(BgL_z62exceptionz62_bglt)
																														BgL_new1062z00_1700)))->
																											BgL_stackz00) =
																										((obj_t) BgL_auxz00_5822),
																										BUNSPEC);
																								}
																								((((BgL_z62errorz62_bglt)
																											COBJECT((
																													(BgL_z62errorz62_bglt)
																													BgL_new1062z00_1700)))->
																										BgL_procz00) =
																									((obj_t) BgL_procz00_10),
																									BUNSPEC);
																								((((BgL_z62errorz62_bglt)
																											COBJECT((
																													(BgL_z62errorz62_bglt)
																													BgL_new1062z00_1700)))->
																										BgL_msgz00) =
																									((obj_t) BgL_msgz00_11),
																									BUNSPEC);
																								((((BgL_z62errorz62_bglt)
																											COBJECT((
																													(BgL_z62errorz62_bglt)
																													BgL_new1062z00_1700)))->
																										BgL_objz00) =
																									((obj_t) BgL_objz00_12),
																									BUNSPEC);
																								BgL_arg1392z00_1699 =
																									BgL_new1062z00_1700;
																							}
																							return
																								BGl_raisez00zz__errorz00(
																								((obj_t) BgL_arg1392z00_1699));
																						}
																					else
																						{	/* Llib/error.scm 356 */
																							if (
																								((long) (BgL_sysnoz00_9) ==
																									(long)
																									(BGL_IO_CONNECTION_ERROR)))
																								{	/* Llib/error.scm 361 */
																									BgL_z62iozd2connectionzd2errorz62_bglt
																										BgL_arg1397z00_1706;
																									{	/* Llib/error.scm 361 */
																										BgL_z62iozd2connectionzd2errorz62_bglt
																											BgL_new1064z00_1707;
																										{	/* Llib/error.scm 361 */
																											BgL_z62iozd2connectionzd2errorz62_bglt
																												BgL_new1063z00_1710;
																											BgL_new1063z00_1710 =
																												(
																												(BgL_z62iozd2connectionzd2errorz62_bglt)
																												BOBJECT(GC_MALLOC(sizeof
																														(struct
																															BgL_z62iozd2connectionzd2errorz62_bgl))));
																											{	/* Llib/error.scm 361 */
																												long
																													BgL_arg1401z00_1711;
																												BgL_arg1401z00_1711 =
																													BGL_CLASS_NUM
																													(BGl_z62iozd2connectionzd2errorz62zz__objectz00);
																												BGL_OBJECT_CLASS_NUM_SET
																													(((BgL_objectz00_bglt)
																														BgL_new1063z00_1710),
																													BgL_arg1401z00_1711);
																											}
																											BgL_new1064z00_1707 =
																												BgL_new1063z00_1710;
																										}
																										((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt) BgL_new1064z00_1707)))->BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
																										((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt) BgL_new1064z00_1707)))->BgL_locationz00) = ((obj_t) BFALSE), BUNSPEC);
																										{
																											obj_t BgL_auxz00_5848;

																											{	/* Llib/error.scm 361 */
																												obj_t
																													BgL_arg1399z00_1708;
																												{	/* Llib/error.scm 361 */
																													obj_t
																														BgL_arg1400z00_1709;
																													{	/* Llib/error.scm 361 */
																														obj_t
																															BgL_classz00_3603;
																														BgL_classz00_3603 =
																															BGl_z62iozd2connectionzd2errorz62zz__objectz00;
																														BgL_arg1400z00_1709
																															=
																															BGL_CLASS_ALL_FIELDS
																															(BgL_classz00_3603);
																													}
																													BgL_arg1399z00_1708 =
																														VECTOR_REF
																														(BgL_arg1400z00_1709,
																														2L);
																												}
																												BgL_auxz00_5848 =
																													BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
																													(BgL_arg1399z00_1708);
																											}
																											((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt) BgL_new1064z00_1707)))->BgL_stackz00) = ((obj_t) BgL_auxz00_5848), BUNSPEC);
																										}
																										((((BgL_z62errorz62_bglt)
																													COBJECT((
																															(BgL_z62errorz62_bglt)
																															BgL_new1064z00_1707)))->
																												BgL_procz00) =
																											((obj_t) BgL_procz00_10),
																											BUNSPEC);
																										((((BgL_z62errorz62_bglt)
																													COBJECT((
																															(BgL_z62errorz62_bglt)
																															BgL_new1064z00_1707)))->
																												BgL_msgz00) =
																											((obj_t) BgL_msgz00_11),
																											BUNSPEC);
																										((((BgL_z62errorz62_bglt)
																													COBJECT((
																															(BgL_z62errorz62_bglt)
																															BgL_new1064z00_1707)))->
																												BgL_objz00) =
																											((obj_t) BgL_objz00_12),
																											BUNSPEC);
																										BgL_arg1397z00_1706 =
																											BgL_new1064z00_1707;
																									}
																									return
																										BGl_raisez00zz__errorz00(
																										((obj_t)
																											BgL_arg1397z00_1706));
																								}
																							else
																								{	/* Llib/error.scm 359 */
																									if (
																										((long) (BgL_sysnoz00_9) ==
																											(long)
																											(BGL_PROCESS_EXCEPTION)))
																										{	/* Llib/error.scm 364 */
																											BgL_z62processzd2exceptionzb0_bglt
																												BgL_arg1403z00_1713;
																											{	/* Llib/error.scm 364 */
																												BgL_z62processzd2exceptionzb0_bglt
																													BgL_new1066z00_1714;
																												{	/* Llib/error.scm 364 */
																													BgL_z62processzd2exceptionzb0_bglt
																														BgL_new1065z00_1717;
																													BgL_new1065z00_1717 =
																														(
																														(BgL_z62processzd2exceptionzb0_bglt)
																														BOBJECT(GC_MALLOC
																															(sizeof(struct
																																	BgL_z62processzd2exceptionzb0_bgl))));
																													{	/* Llib/error.scm 364 */
																														long
																															BgL_arg1406z00_1718;
																														BgL_arg1406z00_1718
																															=
																															BGL_CLASS_NUM
																															(BGl_z62processzd2exceptionzb0zz__objectz00);
																														BGL_OBJECT_CLASS_NUM_SET
																															(((BgL_objectz00_bglt) BgL_new1065z00_1717), BgL_arg1406z00_1718);
																													}
																													BgL_new1066z00_1714 =
																														BgL_new1065z00_1717;
																												}
																												((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt) BgL_new1066z00_1714)))->BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
																												((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt) BgL_new1066z00_1714)))->BgL_locationz00) = ((obj_t) BFALSE), BUNSPEC);
																												{
																													obj_t BgL_auxz00_5874;

																													{	/* Llib/error.scm 364 */
																														obj_t
																															BgL_arg1404z00_1715;
																														{	/* Llib/error.scm 364 */
																															obj_t
																																BgL_arg1405z00_1716;
																															{	/* Llib/error.scm 364 */
																																obj_t
																																	BgL_classz00_3610;
																																BgL_classz00_3610
																																	=
																																	BGl_z62processzd2exceptionzb0zz__objectz00;
																																BgL_arg1405z00_1716
																																	=
																																	BGL_CLASS_ALL_FIELDS
																																	(BgL_classz00_3610);
																															}
																															BgL_arg1404z00_1715
																																=
																																VECTOR_REF
																																(BgL_arg1405z00_1716,
																																2L);
																														}
																														BgL_auxz00_5874 =
																															BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
																															(BgL_arg1404z00_1715);
																													}
																													((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt) BgL_new1066z00_1714)))->BgL_stackz00) = ((obj_t) BgL_auxz00_5874), BUNSPEC);
																												}
																												((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt) BgL_new1066z00_1714)))->BgL_procz00) = ((obj_t) BgL_procz00_10), BUNSPEC);
																												((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt) BgL_new1066z00_1714)))->BgL_msgz00) = ((obj_t) BgL_msgz00_11), BUNSPEC);
																												((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt) BgL_new1066z00_1714)))->BgL_objz00) = ((obj_t) BgL_objz00_12), BUNSPEC);
																												BgL_arg1403z00_1713 =
																													BgL_new1066z00_1714;
																											}
																											return
																												BGl_raisez00zz__errorz00
																												(((obj_t)
																													BgL_arg1403z00_1713));
																										}
																									else
																										{	/* Llib/error.scm 362 */
																											if (
																												((long) (BgL_sysnoz00_9)
																													==
																													(long)
																													(BGL_TYPE_ERROR)))
																												{	/* Llib/error.scm 366 */
																													BgL_z62typezd2errorzb0_bglt
																														BgL_arg1408z00_1720;
																													{	/* Llib/error.scm 626 */
																														obj_t
																															BgL_tyz00_3614;
																														if (STRINGP
																															(BgL_msgz00_11))
																															{	/* Llib/error.scm 627 */
																																BgL_tyz00_3614 =
																																	BgL_msgz00_11;
																															}
																														else
																															{	/* Llib/error.scm 627 */
																																if (SYMBOLP
																																	(BgL_msgz00_11))
																																	{	/* Llib/error.scm 628 */
																																		obj_t
																																			BgL_arg2328z00_3625;
																																		BgL_arg2328z00_3625
																																			=
																																			SYMBOL_TO_STRING
																																			(BgL_msgz00_11);
																																		BgL_tyz00_3614
																																			=
																																			BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																			(BgL_arg2328z00_3625);
																																	}
																																else
																																	{	/* Llib/error.scm 628 */
																																		BgL_tyz00_3614
																																			=
																																			BGl_string2809z00zz__errorz00;
																																	}
																															}
																														{	/* Llib/error.scm 626 */
																															obj_t
																																BgL_msgz00_3617;
																															BgL_msgz00_3617 =
																																BGl_bigloozd2typezd2errorzd2msgzd2zz__errorz00
																																(BGl_string2810z00zz__errorz00,
																																BgL_tyz00_3614,
																																bgl_typeof
																																(BgL_objz00_12));
																															{	/* Llib/error.scm 630 */

																																{	/* Llib/error.scm 631 */
																																	BgL_z62typezd2errorzb0_bglt
																																		BgL_new1085z00_3619;
																																	{	/* Llib/error.scm 632 */
																																		BgL_z62typezd2errorzb0_bglt
																																			BgL_new1084z00_3620;
																																		BgL_new1084z00_3620
																																			=
																																			(
																																			(BgL_z62typezd2errorzb0_bglt)
																																			BOBJECT
																																			(GC_MALLOC
																																				(sizeof
																																					(struct
																																						BgL_z62typezd2errorzb0_bgl))));
																																		{	/* Llib/error.scm 632 */
																																			long
																																				BgL_arg1561z00_3621;
																																			BgL_arg1561z00_3621
																																				=
																																				BGL_CLASS_NUM
																																				(BGl_z62typezd2errorzb0zz__objectz00);
																																			BGL_OBJECT_CLASS_NUM_SET
																																				(((BgL_objectz00_bglt) BgL_new1084z00_3620), BgL_arg1561z00_3621);
																																		}
																																		BgL_new1085z00_3619
																																			=
																																			BgL_new1084z00_3620;
																																	}
																																	((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt) BgL_new1085z00_3619)))->BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
																																	((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt) BgL_new1085z00_3619)))->BgL_locationz00) = ((obj_t) BFALSE), BUNSPEC);
																																	{
																																		obj_t
																																			BgL_auxz00_5908;
																																		{	/* Llib/error.scm 633 */
																																			obj_t
																																				BgL_arg1558z00_3622;
																																			{	/* Llib/error.scm 633 */
																																				obj_t
																																					BgL_arg1559z00_3623;
																																				{	/* Llib/error.scm 633 */
																																					obj_t
																																						BgL_classz00_3629;
																																					BgL_classz00_3629
																																						=
																																						BGl_z62typezd2errorzb0zz__objectz00;
																																					BgL_arg1559z00_3623
																																						=
																																						BGL_CLASS_ALL_FIELDS
																																						(BgL_classz00_3629);
																																				}
																																				BgL_arg1558z00_3622
																																					=
																																					VECTOR_REF
																																					(BgL_arg1559z00_3623,
																																					2L);
																																			}
																																			BgL_auxz00_5908
																																				=
																																				BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
																																				(BgL_arg1558z00_3622);
																																		}
																																		((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt) BgL_new1085z00_3619)))->BgL_stackz00) = ((obj_t) BgL_auxz00_5908), BUNSPEC);
																																	}
																																	((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt) BgL_new1085z00_3619)))->BgL_procz00) = ((obj_t) BgL_procz00_10), BUNSPEC);
																																	((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt) BgL_new1085z00_3619)))->BgL_msgz00) = ((obj_t) BgL_msgz00_3617), BUNSPEC);
																																	((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt) BgL_new1085z00_3619)))->BgL_objz00) = ((obj_t) BgL_objz00_12), BUNSPEC);
																																	((((BgL_z62typezd2errorzb0_bglt) COBJECT(BgL_new1085z00_3619))->BgL_typez00) = ((obj_t) BgL_msgz00_11), BUNSPEC);
																																	BgL_arg1408z00_1720
																																		=
																																		BgL_new1085z00_3619;
																													}}}}
																													return
																														BGl_raisez00zz__errorz00
																														(((obj_t)
																															BgL_arg1408z00_1720));
																												}
																											else
																												{	/* Llib/error.scm 365 */
																													if (
																														((long)
																															(BgL_sysnoz00_9)
																															==
																															(long)
																															(BGL_TYPENAME_ERROR)))
																														{	/* Llib/error.scm 368 */
																															BgL_z62typezd2errorzb0_bglt
																																BgL_arg1410z00_1722;
																															{	/* Llib/error.scm 643 */
																																obj_t
																																	BgL_tyz00_3633;
																																if (STRINGP
																																	(BgL_msgz00_11))
																																	{	/* Llib/error.scm 644 */
																																		BgL_tyz00_3633
																																			=
																																			BgL_msgz00_11;
																																	}
																																else
																																	{	/* Llib/error.scm 644 */
																																		if (SYMBOLP
																																			(BgL_msgz00_11))
																																			{	/* Llib/error.scm 645 */
																																				obj_t
																																					BgL_arg2328z00_3643;
																																				BgL_arg2328z00_3643
																																					=
																																					SYMBOL_TO_STRING
																																					(BgL_msgz00_11);
																																				BgL_tyz00_3633
																																					=
																																					BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																					(BgL_arg2328z00_3643);
																																			}
																																		else
																																			{	/* Llib/error.scm 645 */
																																				BgL_tyz00_3633
																																					=
																																					BGl_string2809z00zz__errorz00;
																																			}
																																	}
																																{	/* Llib/error.scm 643 */
																																	obj_t
																																		BgL_msgz00_3636;
																																	BgL_msgz00_3636
																																		=
																																		BGl_bigloozd2typezd2errorzd2msgzd2zz__errorz00
																																		(BGl_string2810z00zz__errorz00,
																																		BgL_tyz00_3633,
																																		BgL_objz00_12);
																																	{	/* Llib/error.scm 647 */

																																		{	/* Llib/error.scm 648 */
																																			BgL_z62typezd2errorzb0_bglt
																																				BgL_new1087z00_3637;
																																			{	/* Llib/error.scm 649 */
																																				BgL_z62typezd2errorzb0_bglt
																																					BgL_new1086z00_3638;
																																				BgL_new1086z00_3638
																																					=
																																					(
																																					(BgL_z62typezd2errorzb0_bglt)
																																					BOBJECT
																																					(GC_MALLOC
																																						(sizeof
																																							(struct
																																								BgL_z62typezd2errorzb0_bgl))));
																																				{	/* Llib/error.scm 649 */
																																					long
																																						BgL_arg1571z00_3639;
																																					BgL_arg1571z00_3639
																																						=
																																						BGL_CLASS_NUM
																																						(BGl_z62typezd2errorzb0zz__objectz00);
																																					BGL_OBJECT_CLASS_NUM_SET
																																						(((BgL_objectz00_bglt) BgL_new1086z00_3638), BgL_arg1571z00_3639);
																																				}
																																				BgL_new1087z00_3637
																																					=
																																					BgL_new1086z00_3638;
																																			}
																																			((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt) BgL_new1087z00_3637)))->BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
																																			((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt) BgL_new1087z00_3637)))->BgL_locationz00) = ((obj_t) BFALSE), BUNSPEC);
																																			{
																																				obj_t
																																					BgL_auxz00_5942;
																																				{	/* Llib/error.scm 650 */
																																					obj_t
																																						BgL_arg1565z00_3640;
																																					{	/* Llib/error.scm 650 */
																																						obj_t
																																							BgL_arg1567z00_3641;
																																						{	/* Llib/error.scm 650 */
																																							obj_t
																																								BgL_classz00_3647;
																																							BgL_classz00_3647
																																								=
																																								BGl_z62typezd2errorzb0zz__objectz00;
																																							BgL_arg1567z00_3641
																																								=
																																								BGL_CLASS_ALL_FIELDS
																																								(BgL_classz00_3647);
																																						}
																																						BgL_arg1565z00_3640
																																							=
																																							VECTOR_REF
																																							(BgL_arg1567z00_3641,
																																							2L);
																																					}
																																					BgL_auxz00_5942
																																						=
																																						BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
																																						(BgL_arg1565z00_3640);
																																				}
																																				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt) BgL_new1087z00_3637)))->BgL_stackz00) = ((obj_t) BgL_auxz00_5942), BUNSPEC);
																																			}
																																			((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt) BgL_new1087z00_3637)))->BgL_procz00) = ((obj_t) BgL_procz00_10), BUNSPEC);
																																			((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt) BgL_new1087z00_3637)))->BgL_msgz00) = ((obj_t) BgL_msgz00_3636), BUNSPEC);
																																			((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt) BgL_new1087z00_3637)))->BgL_objz00) = ((obj_t) BUNSPEC), BUNSPEC);
																																			((((BgL_z62typezd2errorzb0_bglt) COBJECT(BgL_new1087z00_3637))->BgL_typez00) = ((obj_t) BgL_msgz00_11), BUNSPEC);
																																			BgL_arg1410z00_1722
																																				=
																																				BgL_new1087z00_3637;
																															}}}}
																															return
																																BGl_raisez00zz__errorz00
																																(((obj_t)
																																	BgL_arg1410z00_1722));
																														}
																													else
																														{	/* Llib/error.scm 367 */
																															if (
																																((long)
																																	(BgL_sysnoz00_9)
																																	==
																																	(long)
																																	(BGL_INDEX_OUT_OF_BOUND_ERROR)))
																																{	/* Llib/error.scm 370 */
																																	BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt
																																		BgL_arg1412z00_1724;
																																	{	/* Llib/error.scm 370 */
																																		BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt
																																			BgL_new1068z00_1725;
																																		{	/* Llib/error.scm 370 */
																																			BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt
																																				BgL_new1067z00_1728;
																																			BgL_new1067z00_1728
																																				=
																																				(
																																				(BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt)
																																				BOBJECT
																																				(GC_MALLOC
																																					(sizeof
																																						(struct
																																							BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bgl))));
																																			{	/* Llib/error.scm 370 */
																																				long
																																					BgL_arg1415z00_1729;
																																				BgL_arg1415z00_1729
																																					=
																																					BGL_CLASS_NUM
																																					(BGl_z62indexzd2outzd2ofzd2boundszd2errorz62zz__objectz00);
																																				BGL_OBJECT_CLASS_NUM_SET
																																					(((BgL_objectz00_bglt) BgL_new1067z00_1728), BgL_arg1415z00_1729);
																																			}
																																			BgL_new1068z00_1725
																																				=
																																				BgL_new1067z00_1728;
																																		}
																																		((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt) BgL_new1068z00_1725)))->BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
																																		((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt) BgL_new1068z00_1725)))->BgL_locationz00) = ((obj_t) BFALSE), BUNSPEC);
																																		{
																																			obj_t
																																				BgL_auxz00_5969;
																																			{	/* Llib/error.scm 370 */
																																				obj_t
																																					BgL_arg1413z00_1726;
																																				{	/* Llib/error.scm 370 */
																																					obj_t
																																						BgL_arg1414z00_1727;
																																					{	/* Llib/error.scm 370 */
																																						obj_t
																																							BgL_classz00_3654;
																																						BgL_classz00_3654
																																							=
																																							BGl_z62indexzd2outzd2ofzd2boundszd2errorz62zz__objectz00;
																																						BgL_arg1414z00_1727
																																							=
																																							BGL_CLASS_ALL_FIELDS
																																							(BgL_classz00_3654);
																																					}
																																					BgL_arg1413z00_1726
																																						=
																																						VECTOR_REF
																																						(BgL_arg1414z00_1727,
																																						2L);
																																				}
																																				BgL_auxz00_5969
																																					=
																																					BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
																																					(BgL_arg1413z00_1726);
																																			}
																																			((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt) BgL_new1068z00_1725)))->BgL_stackz00) = ((obj_t) BgL_auxz00_5969), BUNSPEC);
																																		}
																																		((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt) BgL_new1068z00_1725)))->BgL_procz00) = ((obj_t) BgL_procz00_10), BUNSPEC);
																																		((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt) BgL_new1068z00_1725)))->BgL_msgz00) = ((obj_t) BgL_msgz00_11), BUNSPEC);
																																		((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt) BgL_new1068z00_1725)))->BgL_objz00) = ((obj_t) BgL_objz00_12), BUNSPEC);
																																		((((BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt) COBJECT(BgL_new1068z00_1725))->BgL_indexz00) = ((obj_t) BINT(-1L)), BUNSPEC);
																																		BgL_arg1412z00_1724
																																			=
																																			BgL_new1068z00_1725;
																																	}
																																	return
																																		BGl_raisez00zz__errorz00
																																		(((obj_t)
																																			BgL_arg1412z00_1724));
																																}
																															else
																																{	/* Llib/error.scm 369 */
																																	return
																																		BGl_errorz00zz__errorz00
																																		(BgL_procz00_10,
																																		BgL_msgz00_11,
																																		BgL_objz00_12);
																																}
																														}
																												}
																										}
																								}
																						}
																				}
																		}
																}
														}
												}
										}
								}
						}
				}
		}

	}



/* &error/errno */
	obj_t BGl_z62errorzf2errnoz90zz__errorz00(obj_t BgL_envz00_5243,
		obj_t BgL_sysnoz00_5244, obj_t BgL_procz00_5245, obj_t BgL_msgz00_5246,
		obj_t BgL_objz00_5247)
	{
		{	/* Llib/error.scm 327 */
			{	/* Llib/error.scm 329 */
				int BgL_auxz00_5986;

				{	/* Llib/error.scm 329 */
					obj_t BgL_tmpz00_5987;

					if (INTEGERP(BgL_sysnoz00_5244))
						{	/* Llib/error.scm 329 */
							BgL_tmpz00_5987 = BgL_sysnoz00_5244;
						}
					else
						{
							obj_t BgL_auxz00_5990;

							BgL_auxz00_5990 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2811z00zz__errorz00,
								BINT(13566L), BGl_string2812z00zz__errorz00,
								BGl_string2813z00zz__errorz00, BgL_sysnoz00_5244);
							FAILURE(BgL_auxz00_5990, BFALSE, BFALSE);
						}
					BgL_auxz00_5986 = CINT(BgL_tmpz00_5987);
				}
				return
					bgl_system_failure(BgL_auxz00_5986, BgL_procz00_5245, BgL_msgz00_5246,
					BgL_objz00_5247);
			}
		}

	}



/* stack-overflow-error */
	BGL_EXPORTED_DEF obj_t bgl_stack_overflow_error(void)
	{
		{	/* Llib/error.scm 378 */
			{	/* Llib/error.scm 379 */
				obj_t BgL_stkz00_1730;

				{	/* Llib/error.scm 379 */

					{	/* Llib/error.scm 379 */

						BgL_stkz00_1730 = BGl_getzd2tracezd2stackz00zz__errorz00(BFALSE);
				}}
				{
					obj_t BgL_procz00_1735;

					if (PAIRP(BgL_stkz00_1730))
						{	/* Llib/error.scm 380 */
							obj_t BgL_carzd2111zd2_1740;

							BgL_carzd2111zd2_1740 = CAR(((obj_t) BgL_stkz00_1730));
							if (PAIRP(BgL_carzd2111zd2_1740))
								{	/* Llib/error.scm 380 */
									obj_t BgL_cdrzd2117zd2_1742;

									BgL_cdrzd2117zd2_1742 = CDR(BgL_carzd2111zd2_1740);
									if (PAIRP(BgL_cdrzd2117zd2_1742))
										{	/* Llib/error.scm 380 */
											obj_t BgL_carzd2121zd2_1744;

											BgL_carzd2121zd2_1744 = CAR(BgL_cdrzd2117zd2_1742);
											if (PAIRP(BgL_carzd2121zd2_1744))
												{	/* Llib/error.scm 380 */
													obj_t BgL_cdrzd2126zd2_1746;

													BgL_cdrzd2126zd2_1746 = CDR(BgL_carzd2121zd2_1744);
													if (
														(CAR(BgL_carzd2121zd2_1744) ==
															BGl_symbol2815z00zz__errorz00))
														{	/* Llib/error.scm 380 */
															if (PAIRP(BgL_cdrzd2126zd2_1746))
																{	/* Llib/error.scm 380 */
																	obj_t BgL_cdrzd2130zd2_1750;

																	BgL_cdrzd2130zd2_1750 =
																		CDR(BgL_cdrzd2126zd2_1746);
																	if (PAIRP(BgL_cdrzd2130zd2_1750))
																		{	/* Llib/error.scm 380 */
																			if (NULLP(CDR(BgL_cdrzd2130zd2_1750)))
																				{	/* Llib/error.scm 380 */
																					if (NULLP(CDR(BgL_cdrzd2117zd2_1742)))
																						{	/* Llib/error.scm 380 */
																							obj_t BgL_arg1428z00_1756;
																							obj_t BgL_arg1429z00_1757;
																							obj_t BgL_arg1430z00_1758;

																							BgL_arg1428z00_1756 =
																								CAR(BgL_carzd2111zd2_1740);
																							BgL_arg1429z00_1757 =
																								CAR(BgL_cdrzd2126zd2_1746);
																							BgL_arg1430z00_1758 =
																								CAR(BgL_cdrzd2130zd2_1750);
																							{	/* Llib/error.scm 383 */
																								BgL_z62stackzd2overflowzd2errorz62_bglt
																									BgL_arg1444z00_3681;
																								{	/* Llib/error.scm 383 */
																									BgL_z62stackzd2overflowzd2errorz62_bglt
																										BgL_new1070z00_3682;
																									{	/* Llib/error.scm 384 */
																										BgL_z62stackzd2overflowzd2errorz62_bglt
																											BgL_new1069z00_3683;
																										BgL_new1069z00_3683 =
																											(
																											(BgL_z62stackzd2overflowzd2errorz62_bglt)
																											BOBJECT(GC_MALLOC(sizeof
																													(struct
																														BgL_z62stackzd2overflowzd2errorz62_bgl))));
																										{	/* Llib/error.scm 384 */
																											long BgL_arg1445z00_3684;

																											BgL_arg1445z00_3684 =
																												BGL_CLASS_NUM
																												(BGl_z62stackzd2overflowzd2errorz62zz__objectz00);
																											BGL_OBJECT_CLASS_NUM_SET((
																													(BgL_objectz00_bglt)
																													BgL_new1069z00_3683),
																												BgL_arg1445z00_3684);
																										}
																										BgL_new1070z00_3682 =
																											BgL_new1069z00_3683;
																									}
																									((((BgL_z62exceptionz62_bglt)
																												COBJECT((
																														(BgL_z62exceptionz62_bglt)
																														BgL_new1070z00_3682)))->
																											BgL_fnamez00) =
																										((obj_t)
																											BgL_arg1429z00_1757),
																										BUNSPEC);
																									((((BgL_z62exceptionz62_bglt)
																												COBJECT((
																														(BgL_z62exceptionz62_bglt)
																														BgL_new1070z00_3682)))->
																											BgL_locationz00) =
																										((obj_t)
																											BgL_arg1430z00_1758),
																										BUNSPEC);
																									((((BgL_z62exceptionz62_bglt)
																												COBJECT((
																														(BgL_z62exceptionz62_bglt)
																														BgL_new1070z00_3682)))->
																											BgL_stackz00) =
																										((obj_t) BgL_stkz00_1730),
																										BUNSPEC);
																									((((BgL_z62errorz62_bglt)
																												COBJECT((
																														(BgL_z62errorz62_bglt)
																														BgL_new1070z00_3682)))->
																											BgL_procz00) =
																										((obj_t)
																											BgL_arg1428z00_1756),
																										BUNSPEC);
																									((((BgL_z62errorz62_bglt)
																												COBJECT((
																														(BgL_z62errorz62_bglt)
																														BgL_new1070z00_3682)))->
																											BgL_msgz00) =
																										((obj_t)
																											BGl_string2814z00zz__errorz00),
																										BUNSPEC);
																									((((BgL_z62errorz62_bglt)
																												COBJECT((
																														(BgL_z62errorz62_bglt)
																														BgL_new1070z00_3682)))->
																											BgL_objz00) =
																										((obj_t)
																											BGL_CURRENT_DYNAMIC_ENV
																											()), BUNSPEC);
																									BgL_arg1444z00_3681 =
																										BgL_new1070z00_3682;
																								}
																								return
																									BGl_raisez00zz__errorz00(
																									((obj_t)
																										BgL_arg1444z00_3681));
																							}
																						}
																					else
																						{	/* Llib/error.scm 380 */
																							BgL_procz00_1735 =
																								BgL_carzd2111zd2_1740;
																						BgL_tagzd2102zd2_1736:
																							{	/* Llib/error.scm 392 */
																								BgL_z62stackzd2overflowzd2errorz62_bglt
																									BgL_arg1446z00_1774;
																								{	/* Llib/error.scm 392 */
																									BgL_z62stackzd2overflowzd2errorz62_bglt
																										BgL_new1072z00_1775;
																									{	/* Llib/error.scm 392 */
																										BgL_z62stackzd2overflowzd2errorz62_bglt
																											BgL_new1071z00_1776;
																										BgL_new1071z00_1776 =
																											(
																											(BgL_z62stackzd2overflowzd2errorz62_bglt)
																											BOBJECT(GC_MALLOC(sizeof
																													(struct
																														BgL_z62stackzd2overflowzd2errorz62_bgl))));
																										{	/* Llib/error.scm 392 */
																											long BgL_arg1447z00_1777;

																											BgL_arg1447z00_1777 =
																												BGL_CLASS_NUM
																												(BGl_z62stackzd2overflowzd2errorz62zz__objectz00);
																											BGL_OBJECT_CLASS_NUM_SET((
																													(BgL_objectz00_bglt)
																													BgL_new1071z00_1776),
																												BgL_arg1447z00_1777);
																										}
																										BgL_new1072z00_1775 =
																											BgL_new1071z00_1776;
																									}
																									((((BgL_z62exceptionz62_bglt)
																												COBJECT((
																														(BgL_z62exceptionz62_bglt)
																														BgL_new1072z00_1775)))->
																											BgL_fnamez00) =
																										((obj_t) BFALSE), BUNSPEC);
																									((((BgL_z62exceptionz62_bglt)
																												COBJECT((
																														(BgL_z62exceptionz62_bglt)
																														BgL_new1072z00_1775)))->
																											BgL_locationz00) =
																										((obj_t) BFALSE), BUNSPEC);
																									((((BgL_z62exceptionz62_bglt)
																												COBJECT((
																														(BgL_z62exceptionz62_bglt)
																														BgL_new1072z00_1775)))->
																											BgL_stackz00) =
																										((obj_t) BgL_stkz00_1730),
																										BUNSPEC);
																									((((BgL_z62errorz62_bglt)
																												COBJECT((
																														(BgL_z62errorz62_bglt)
																														BgL_new1072z00_1775)))->
																											BgL_procz00) =
																										((obj_t) BgL_procz00_1735),
																										BUNSPEC);
																									((((BgL_z62errorz62_bglt)
																												COBJECT((
																														(BgL_z62errorz62_bglt)
																														BgL_new1072z00_1775)))->
																											BgL_msgz00) =
																										((obj_t)
																											BGl_string2814z00zz__errorz00),
																										BUNSPEC);
																									((((BgL_z62errorz62_bglt)
																												COBJECT((
																														(BgL_z62errorz62_bglt)
																														BgL_new1072z00_1775)))->
																											BgL_objz00) =
																										((obj_t)
																											BGL_CURRENT_DYNAMIC_ENV
																											()), BUNSPEC);
																									BgL_arg1446z00_1774 =
																										BgL_new1072z00_1775;
																								}
																								return
																									BGl_raisez00zz__errorz00(
																									((obj_t)
																										BgL_arg1446z00_1774));
																							}
																						}
																				}
																			else
																				{
																					obj_t BgL_procz00_6065;

																					BgL_procz00_6065 =
																						BgL_carzd2111zd2_1740;
																					BgL_procz00_1735 = BgL_procz00_6065;
																					goto BgL_tagzd2102zd2_1736;
																				}
																		}
																	else
																		{
																			obj_t BgL_procz00_6066;

																			BgL_procz00_6066 = BgL_carzd2111zd2_1740;
																			BgL_procz00_1735 = BgL_procz00_6066;
																			goto BgL_tagzd2102zd2_1736;
																		}
																}
															else
																{
																	obj_t BgL_procz00_6067;

																	BgL_procz00_6067 = BgL_carzd2111zd2_1740;
																	BgL_procz00_1735 = BgL_procz00_6067;
																	goto BgL_tagzd2102zd2_1736;
																}
														}
													else
														{
															obj_t BgL_procz00_6068;

															BgL_procz00_6068 = BgL_carzd2111zd2_1740;
															BgL_procz00_1735 = BgL_procz00_6068;
															goto BgL_tagzd2102zd2_1736;
														}
												}
											else
												{
													obj_t BgL_procz00_6069;

													BgL_procz00_6069 = BgL_carzd2111zd2_1740;
													BgL_procz00_1735 = BgL_procz00_6069;
													goto BgL_tagzd2102zd2_1736;
												}
										}
									else
										{
											obj_t BgL_procz00_6070;

											BgL_procz00_6070 = BgL_carzd2111zd2_1740;
											BgL_procz00_1735 = BgL_procz00_6070;
											goto BgL_tagzd2102zd2_1736;
										}
								}
							else
								{
									obj_t BgL_procz00_6071;

									BgL_procz00_6071 = BgL_carzd2111zd2_1740;
									BgL_procz00_1735 = BgL_procz00_6071;
									goto BgL_tagzd2102zd2_1736;
								}
						}
					else
						{	/* Llib/error.scm 380 */
							{	/* Llib/error.scm 399 */
								BgL_z62stackzd2overflowzd2errorz62_bglt BgL_arg1448z00_1778;

								{	/* Llib/error.scm 399 */
									BgL_z62stackzd2overflowzd2errorz62_bglt BgL_new1074z00_1779;

									{	/* Llib/error.scm 399 */
										BgL_z62stackzd2overflowzd2errorz62_bglt BgL_new1073z00_1780;

										BgL_new1073z00_1780 =
											((BgL_z62stackzd2overflowzd2errorz62_bglt)
											BOBJECT(GC_MALLOC(sizeof(struct
														BgL_z62stackzd2overflowzd2errorz62_bgl))));
										{	/* Llib/error.scm 399 */
											long BgL_arg1449z00_1781;

											BgL_arg1449z00_1781 =
												BGL_CLASS_NUM
												(BGl_z62stackzd2overflowzd2errorz62zz__objectz00);
											BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
													BgL_new1073z00_1780), BgL_arg1449z00_1781);
										}
										BgL_new1074z00_1779 = BgL_new1073z00_1780;
									}
									((((BgL_z62exceptionz62_bglt) COBJECT(
													((BgL_z62exceptionz62_bglt) BgL_new1074z00_1779)))->
											BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
									((((BgL_z62exceptionz62_bglt)
												COBJECT(((BgL_z62exceptionz62_bglt)
														BgL_new1074z00_1779)))->BgL_locationz00) =
										((obj_t) BFALSE), BUNSPEC);
									((((BgL_z62exceptionz62_bglt)
												COBJECT(((BgL_z62exceptionz62_bglt)
														BgL_new1074z00_1779)))->BgL_stackz00) =
										((obj_t) BgL_stkz00_1730), BUNSPEC);
									((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
														BgL_new1074z00_1779)))->BgL_procz00) =
										((obj_t) BFALSE), BUNSPEC);
									((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
														BgL_new1074z00_1779)))->BgL_msgz00) =
										((obj_t) BGl_string2814z00zz__errorz00), BUNSPEC);
									((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
														BgL_new1074z00_1779)))->BgL_objz00) =
										((obj_t) BGL_CURRENT_DYNAMIC_ENV()), BUNSPEC);
									BgL_arg1448z00_1778 = BgL_new1074z00_1779;
								}
								return BGl_raisez00zz__errorz00(((obj_t) BgL_arg1448z00_1778));
							}
						}
				}
			}
		}

	}



/* &stack-overflow-erro2757 */
	obj_t BGl_z62stackzd2overflowzd2erro2757z62zz__errorz00(obj_t BgL_envz00_5248)
	{
		{	/* Llib/error.scm 378 */
			return bgl_stack_overflow_error();
		}

	}



/* exit */
	BGL_EXPORTED_DEF obj_t BGl_exitz00zz__errorz00(obj_t BgL_nz00_13)
	{
		{	/* Llib/error.scm 408 */
			{	/* Llib/error.scm 409 */
				obj_t BgL_valz00_1783;

				if (NULLP(BgL_nz00_13))
					{	/* Llib/error.scm 410 */
						BgL_valz00_1783 = BINT(0L);
					}
				else
					{	/* Llib/error.scm 410 */
						if (INTEGERP(CAR(((obj_t) BgL_nz00_13))))
							{	/* Llib/error.scm 411 */
								BgL_valz00_1783 = CAR(((obj_t) BgL_nz00_13));
							}
						else
							{	/* Llib/error.scm 411 */
								BgL_valz00_1783 = BINT(0L);
							}
					}
				BIGLOO_EXIT(BgL_valz00_1783);
				return BgL_valz00_1783;
			}
		}

	}



/* &exit */
	obj_t BGl_z62exitz62zz__errorz00(obj_t BgL_envz00_5249, obj_t BgL_nz00_5250)
	{
		{	/* Llib/error.scm 408 */
			return BGl_exitz00zz__errorz00(BgL_nz00_5250);
		}

	}



/* with-exception-handler */
	BGL_EXPORTED_DEF obj_t BGl_withzd2exceptionzd2handlerz00zz__errorz00(obj_t
		BgL_handlerz00_14, obj_t BgL_thunkz00_15)
	{
		{	/* Llib/error.scm 419 */
			if (PROCEDURE_CORRECT_ARITYP(BgL_handlerz00_14, (int) (1L)))
				{	/* Llib/error.scm 421 */
					obj_t BgL_oldzd2handlerszd2_1788;

					BgL_oldzd2handlerszd2_1788 = BGL_ERROR_HANDLER_GET();
					{	/* Llib/error.scm 424 */
						obj_t BgL_arg1454z00_1789;

						{	/* Llib/error.scm 424 */
							obj_t BgL_zc3z04anonymousza31456ze3z87_5251;

							BgL_zc3z04anonymousza31456ze3z87_5251 =
								MAKE_FX_PROCEDURE
								(BGl_z62zc3z04anonymousza31456ze3ze5zz__errorz00, (int) (1L),
								(int) (2L));
							PROCEDURE_SET(BgL_zc3z04anonymousza31456ze3z87_5251, (int) (0L),
								BgL_oldzd2handlerszd2_1788);
							PROCEDURE_SET(BgL_zc3z04anonymousza31456ze3z87_5251, (int) (1L),
								((obj_t) BgL_handlerz00_14));
							BgL_arg1454z00_1789 =
								MAKE_STACK_PAIR(BgL_zc3z04anonymousza31456ze3z87_5251, BUNSPEC);
						}
						BGL_ERROR_HANDLER_SET(BgL_arg1454z00_1789);
						BUNSPEC;
					}
					{	/* Llib/error.scm 427 */
						obj_t BgL_exitd1075z00_1794;

						BgL_exitd1075z00_1794 = BGL_EXITD_TOP_AS_OBJ();
						{	/* Llib/error.scm 427 */
							obj_t BgL_tmp1077z00_1795;

							{	/* Llib/error.scm 427 */
								obj_t BgL_arg1457z00_1797;

								BgL_arg1457z00_1797 = BGL_EXITD_PROTECT(BgL_exitd1075z00_1794);
								BgL_tmp1077z00_1795 =
									MAKE_YOUNG_PAIR(BgL_oldzd2handlerszd2_1788,
									BgL_arg1457z00_1797);
							}
							{	/* Llib/error.scm 427 */

								BGL_EXITD_PROTECT_SET(BgL_exitd1075z00_1794,
									BgL_tmp1077z00_1795);
								BUNSPEC;
								{	/* Llib/error.scm 428 */
									obj_t BgL_tmp1076z00_1796;

									if (PROCEDURE_CORRECT_ARITYP(BgL_thunkz00_15, (int) (0L)))
										{	/* Llib/error.scm 428 */
											BgL_tmp1076z00_1796 =
												BGL_PROCEDURE_CALL0(BgL_thunkz00_15);
										}
									else
										{	/* Llib/error.scm 546 */
											BgL_z62errorz62_bglt BgL_arg1509z00_3698;

											{	/* Llib/error.scm 546 */
												BgL_z62errorz62_bglt BgL_new1079z00_3699;

												{	/* Llib/error.scm 546 */
													BgL_z62errorz62_bglt BgL_new1078z00_3700;

													BgL_new1078z00_3700 =
														((BgL_z62errorz62_bglt)
														BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_z62errorz62_bgl))));
													{	/* Llib/error.scm 546 */
														long BgL_arg1513z00_3701;

														BgL_arg1513z00_3701 =
															BGL_CLASS_NUM(BGl_z62errorz62zz__objectz00);
														BGL_OBJECT_CLASS_NUM_SET(
															((BgL_objectz00_bglt) BgL_new1078z00_3700),
															BgL_arg1513z00_3701);
													}
													BgL_new1079z00_3699 = BgL_new1078z00_3700;
												}
												((((BgL_z62exceptionz62_bglt) COBJECT(
																((BgL_z62exceptionz62_bglt)
																	BgL_new1079z00_3699)))->BgL_fnamez00) =
													((obj_t) BFALSE), BUNSPEC);
												((((BgL_z62exceptionz62_bglt)
															COBJECT(((BgL_z62exceptionz62_bglt)
																	BgL_new1079z00_3699)))->BgL_locationz00) =
													((obj_t) BFALSE), BUNSPEC);
												{
													obj_t BgL_auxz00_6136;

													{	/* Llib/error.scm 546 */
														obj_t BgL_arg1510z00_3702;

														{	/* Llib/error.scm 546 */
															obj_t BgL_arg1511z00_3703;

															{	/* Llib/error.scm 546 */
																obj_t BgL_classz00_3707;

																BgL_classz00_3707 =
																	BGl_z62errorz62zz__objectz00;
																BgL_arg1511z00_3703 =
																	BGL_CLASS_ALL_FIELDS(BgL_classz00_3707);
															}
															BgL_arg1510z00_3702 =
																VECTOR_REF(BgL_arg1511z00_3703, 2L);
														}
														BgL_auxz00_6136 =
															BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
															(BgL_arg1510z00_3702);
													}
													((((BgL_z62exceptionz62_bglt) COBJECT(
																	((BgL_z62exceptionz62_bglt)
																		BgL_new1079z00_3699)))->BgL_stackz00) =
														((obj_t) BgL_auxz00_6136), BUNSPEC);
												}
												((((BgL_z62errorz62_bglt)
															COBJECT(BgL_new1079z00_3699))->BgL_procz00) =
													((obj_t) BGl_string2817z00zz__errorz00), BUNSPEC);
												((((BgL_z62errorz62_bglt)
															COBJECT(BgL_new1079z00_3699))->BgL_msgz00) =
													((obj_t) BGl_string2818z00zz__errorz00), BUNSPEC);
												((((BgL_z62errorz62_bglt)
															COBJECT(BgL_new1079z00_3699))->BgL_objz00) =
													((obj_t) BgL_thunkz00_15), BUNSPEC);
												BgL_arg1509z00_3698 = BgL_new1079z00_3699;
											}
											BgL_tmp1076z00_1796 =
												BGl_raisez00zz__errorz00(((obj_t) BgL_arg1509z00_3698));
										}
									{	/* Llib/error.scm 427 */
										bool_t BgL_test3190z00_6147;

										{	/* Llib/error.scm 427 */
											obj_t BgL_arg2609z00_3710;

											BgL_arg2609z00_3710 =
												BGL_EXITD_PROTECT(BgL_exitd1075z00_1794);
											BgL_test3190z00_6147 = PAIRP(BgL_arg2609z00_3710);
										}
										if (BgL_test3190z00_6147)
											{	/* Llib/error.scm 427 */
												obj_t BgL_arg2607z00_3711;

												{	/* Llib/error.scm 427 */
													obj_t BgL_arg2608z00_3712;

													BgL_arg2608z00_3712 =
														BGL_EXITD_PROTECT(BgL_exitd1075z00_1794);
													BgL_arg2607z00_3711 =
														CDR(((obj_t) BgL_arg2608z00_3712));
												}
												BGL_EXITD_PROTECT_SET(BgL_exitd1075z00_1794,
													BgL_arg2607z00_3711);
												BUNSPEC;
											}
										else
											{	/* Llib/error.scm 427 */
												BFALSE;
											}
									}
									BGL_ERROR_HANDLER_SET(BgL_oldzd2handlerszd2_1788);
									BUNSPEC;
									return BgL_tmp1076z00_1796;
								}
							}
						}
					}
				}
			else
				{	/* Llib/error.scm 546 */
					BgL_z62errorz62_bglt BgL_arg1509z00_3714;

					{	/* Llib/error.scm 546 */
						BgL_z62errorz62_bglt BgL_new1079z00_3715;

						{	/* Llib/error.scm 546 */
							BgL_z62errorz62_bglt BgL_new1078z00_3716;

							BgL_new1078z00_3716 =
								((BgL_z62errorz62_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_z62errorz62_bgl))));
							{	/* Llib/error.scm 546 */
								long BgL_arg1513z00_3717;

								BgL_arg1513z00_3717 =
									BGL_CLASS_NUM(BGl_z62errorz62zz__objectz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1078z00_3716),
									BgL_arg1513z00_3717);
							}
							BgL_new1079z00_3715 = BgL_new1078z00_3716;
						}
						((((BgL_z62exceptionz62_bglt) COBJECT(
										((BgL_z62exceptionz62_bglt) BgL_new1079z00_3715)))->
								BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
						((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
											BgL_new1079z00_3715)))->BgL_locationz00) =
							((obj_t) BFALSE), BUNSPEC);
						{
							obj_t BgL_auxz00_6163;

							{	/* Llib/error.scm 546 */
								obj_t BgL_arg1510z00_3718;

								{	/* Llib/error.scm 546 */
									obj_t BgL_arg1511z00_3719;

									{	/* Llib/error.scm 546 */
										obj_t BgL_classz00_3723;

										BgL_classz00_3723 = BGl_z62errorz62zz__objectz00;
										BgL_arg1511z00_3719 =
											BGL_CLASS_ALL_FIELDS(BgL_classz00_3723);
									}
									BgL_arg1510z00_3718 = VECTOR_REF(BgL_arg1511z00_3719, 2L);
								}
								BgL_auxz00_6163 =
									BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
									(BgL_arg1510z00_3718);
							}
							((((BgL_z62exceptionz62_bglt) COBJECT(
											((BgL_z62exceptionz62_bglt) BgL_new1079z00_3715)))->
									BgL_stackz00) = ((obj_t) BgL_auxz00_6163), BUNSPEC);
						}
						((((BgL_z62errorz62_bglt) COBJECT(BgL_new1079z00_3715))->
								BgL_procz00) =
							((obj_t) BGl_string2817z00zz__errorz00), BUNSPEC);
						((((BgL_z62errorz62_bglt) COBJECT(BgL_new1079z00_3715))->
								BgL_msgz00) = ((obj_t) BGl_string2819z00zz__errorz00), BUNSPEC);
						((((BgL_z62errorz62_bglt) COBJECT(BgL_new1079z00_3715))->
								BgL_objz00) = ((obj_t) BgL_handlerz00_14), BUNSPEC);
						BgL_arg1509z00_3714 = BgL_new1079z00_3715;
					}
					return BGl_raisez00zz__errorz00(((obj_t) BgL_arg1509z00_3714));
				}
		}

	}



/* &with-exception-handler */
	obj_t BGl_z62withzd2exceptionzd2handlerz62zz__errorz00(obj_t BgL_envz00_5252,
		obj_t BgL_handlerz00_5253, obj_t BgL_thunkz00_5254)
	{
		{	/* Llib/error.scm 419 */
			{	/* Llib/error.scm 420 */
				obj_t BgL_auxz00_6181;
				obj_t BgL_auxz00_6174;

				if (PROCEDUREP(BgL_thunkz00_5254))
					{	/* Llib/error.scm 420 */
						BgL_auxz00_6181 = BgL_thunkz00_5254;
					}
				else
					{
						obj_t BgL_auxz00_6184;

						BgL_auxz00_6184 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2811z00zz__errorz00,
							BINT(16958L), BGl_string2820z00zz__errorz00,
							BGl_string2821z00zz__errorz00, BgL_thunkz00_5254);
						FAILURE(BgL_auxz00_6184, BFALSE, BFALSE);
					}
				if (PROCEDUREP(BgL_handlerz00_5253))
					{	/* Llib/error.scm 420 */
						BgL_auxz00_6174 = BgL_handlerz00_5253;
					}
				else
					{
						obj_t BgL_auxz00_6177;

						BgL_auxz00_6177 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2811z00zz__errorz00,
							BINT(16958L), BGl_string2820z00zz__errorz00,
							BGl_string2821z00zz__errorz00, BgL_handlerz00_5253);
						FAILURE(BgL_auxz00_6177, BFALSE, BFALSE);
					}
				return
					BGl_withzd2exceptionzd2handlerz00zz__errorz00(BgL_auxz00_6174,
					BgL_auxz00_6181);
			}
		}

	}



/* &<@anonymous:1456> */
	obj_t BGl_z62zc3z04anonymousza31456ze3ze5zz__errorz00(obj_t BgL_envz00_5255,
		obj_t BgL_cz00_5258)
	{
		{	/* Llib/error.scm 423 */
			{	/* Llib/error.scm 424 */
				obj_t BgL_oldzd2handlerszd2_5256;
				obj_t BgL_handlerz00_5257;

				BgL_oldzd2handlerszd2_5256 = PROCEDURE_REF(BgL_envz00_5255, (int) (0L));
				BgL_handlerz00_5257 = PROCEDURE_REF(BgL_envz00_5255, (int) (1L));
				BGL_ERROR_HANDLER_SET(BgL_oldzd2handlerszd2_5256);
				BUNSPEC;
				return BGL_PROCEDURE_CALL1(BgL_handlerz00_5257, BgL_cz00_5258);
			}
		}

	}



/* current-exception-handler */
	BGL_EXPORTED_DEF obj_t BGl_currentzd2exceptionzd2handlerz00zz__errorz00(void)
	{
		{	/* Llib/error.scm 437 */
			return BGl_proc2822z00zz__errorz00;
		}

	}



/* &current-exception-handler */
	obj_t BGl_z62currentzd2exceptionzd2handlerz62zz__errorz00(obj_t
		BgL_envz00_5260)
	{
		{	/* Llib/error.scm 437 */
			return BGl_currentzd2exceptionzd2handlerz00zz__errorz00();
		}

	}



/* &<@anonymous:1458> */
	obj_t BGl_z62zc3z04anonymousza31458ze3ze5zz__errorz00(obj_t BgL_envz00_5261,
		obj_t BgL_valz00_5262)
	{
		{	/* Llib/error.scm 438 */
			return BGl_raisez00zz__errorz00(BgL_valz00_5262);
		}

	}



/* raise */
	BGL_EXPORTED_DEF obj_t BGl_raisez00zz__errorz00(obj_t BgL_valz00_16)
	{
		{	/* Llib/error.scm 465 */
			{
				obj_t BgL_handlerz00_1829;
				obj_t BgL_valz00_1830;

				{	/* Llib/error.scm 502 */
					obj_t BgL_handlerz00_1804;

					BgL_handlerz00_1804 = BGL_ERROR_HANDLER_GET();
					if (PAIRP(BgL_handlerz00_1804))
						{	/* Llib/error.scm 506 */
							bool_t BgL_test3194z00_6203;

							{	/* Llib/error.scm 506 */
								obj_t BgL_tmpz00_6204;

								BgL_tmpz00_6204 = CDR(BgL_handlerz00_1804);
								BgL_test3194z00_6203 = BGL_DYNAMIC_ENVP(BgL_tmpz00_6204);
							}
							if (BgL_test3194z00_6203)
								{	/* Llib/error.scm 472 */
									obj_t BgL_denvz00_3740;
									obj_t BgL_hz00_3741;

									BgL_denvz00_3740 = CDR(BgL_handlerz00_1804);
									BgL_hz00_3741 = CAR(BgL_handlerz00_1804);
									SET_CDR(BgL_handlerz00_1804, BgL_valz00_16);
									return
										BGl_unwindzd2untilz12zc0zz__bexitz00(BgL_hz00_3741,
										BgL_denvz00_3740);
								}
							else
								{	/* Llib/error.scm 508 */
									bool_t BgL_test3195z00_6211;

									{	/* Llib/error.scm 508 */
										obj_t BgL_tmpz00_6212;

										BgL_tmpz00_6212 = CDR(BgL_handlerz00_1804);
										BgL_test3195z00_6211 = CELLP(BgL_tmpz00_6212);
									}
									if (BgL_test3195z00_6211)
										{	/* Llib/error.scm 482 */
											obj_t BgL_cellz00_3746;
											obj_t BgL_hz00_3747;

											BgL_cellz00_3746 = CDR(BgL_handlerz00_1804);
											BgL_hz00_3747 = CAR(BgL_handlerz00_1804);
											CELL_SET(((obj_t) BgL_cellz00_3746), BgL_valz00_16);
											return
												BGl_unwindzd2untilz12zc0zz__bexitz00(BgL_hz00_3747,
												BgL_cellz00_3746);
										}
									else
										{	/* Llib/error.scm 508 */
											if (CBOOL(CDR(BgL_handlerz00_1804)))
												{	/* Llib/error.scm 510 */
													if ((CDR(BgL_handlerz00_1804) == BUNSPEC))
														{	/* Llib/error.scm 514 */
															obj_t BgL_fun1467z00_1813;

															BgL_fun1467z00_1813 = CAR(BgL_handlerz00_1804);
															return
																BGL_PROCEDURE_CALL1(BgL_fun1467z00_1813,
																BgL_valz00_16);
														}
													else
														{	/* Llib/error.scm 513 */
															BgL_handlerz00_1829 = BgL_handlerz00_1804;
															BgL_valz00_1830 = BgL_valz00_16;
														BgL_zc3z04anonymousza31476ze3z87_1831:
															{	/* Llib/error.scm 498 */
																obj_t BgL_arg1477z00_1832;

																{	/* Llib/error.scm 498 */
																	obj_t BgL_tmpz00_6231;

																	BgL_tmpz00_6231 = BGL_CURRENT_DYNAMIC_ENV();
																	BgL_arg1477z00_1832 =
																		BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_6231);
																}
																{	/* Llib/error.scm 498 */
																	obj_t BgL_list1478z00_1833;

																	{	/* Llib/error.scm 498 */
																		obj_t BgL_arg1479z00_1834;

																		{	/* Llib/error.scm 498 */
																			obj_t BgL_arg1480z00_1835;

																			{	/* Llib/error.scm 498 */
																				obj_t BgL_arg1481z00_1836;

																				{	/* Llib/error.scm 498 */
																					obj_t BgL_arg1482z00_1837;

																					{	/* Llib/error.scm 498 */
																						obj_t BgL_arg1483z00_1838;

																						BgL_arg1483z00_1838 =
																							MAKE_YOUNG_PAIR
																							(BgL_handlerz00_1829, BNIL);
																						BgL_arg1482z00_1837 =
																							MAKE_YOUNG_PAIR
																							(BGl_string2823z00zz__errorz00,
																							BgL_arg1483z00_1838);
																					}
																					BgL_arg1481z00_1836 =
																						MAKE_YOUNG_PAIR
																						(BGl_string2824z00zz__errorz00,
																						BgL_arg1482z00_1837);
																				}
																				BgL_arg1480z00_1835 =
																					MAKE_YOUNG_PAIR(BINT(498L),
																					BgL_arg1481z00_1836);
																			}
																			BgL_arg1479z00_1834 =
																				MAKE_YOUNG_PAIR
																				(BGl_string2825z00zz__errorz00,
																				BgL_arg1480z00_1835);
																		}
																		BgL_list1478z00_1833 =
																			MAKE_YOUNG_PAIR
																			(BGl_string2826z00zz__errorz00,
																			BgL_arg1479z00_1834);
																	}
																	BGl_tprintz00zz__r4_output_6_10_3z00
																		(BgL_arg1477z00_1832, BgL_list1478z00_1833);
																}
															}
															{	/* Llib/error.scm 499 */
																obj_t BgL_arg1484z00_1839;

																{	/* Llib/error.scm 499 */
																	obj_t BgL_tmpz00_6242;

																	BgL_tmpz00_6242 = BGL_CURRENT_DYNAMIC_ENV();
																	BgL_arg1484z00_1839 =
																		BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_6242);
																}
																{	/* Llib/error.scm 499 */
																	obj_t BgL_list1485z00_1840;

																	{	/* Llib/error.scm 499 */
																		obj_t BgL_arg1486z00_1841;

																		{	/* Llib/error.scm 499 */
																			obj_t BgL_arg1487z00_1842;

																			{	/* Llib/error.scm 499 */
																				obj_t BgL_arg1488z00_1843;

																				{	/* Llib/error.scm 499 */
																					obj_t BgL_arg1489z00_1844;

																					{	/* Llib/error.scm 499 */
																						obj_t BgL_arg1490z00_1845;

																						BgL_arg1490z00_1845 =
																							MAKE_YOUNG_PAIR(BgL_valz00_1830,
																							BNIL);
																						BgL_arg1489z00_1844 =
																							MAKE_YOUNG_PAIR
																							(BGl_string2827z00zz__errorz00,
																							BgL_arg1490z00_1845);
																					}
																					BgL_arg1488z00_1843 =
																						MAKE_YOUNG_PAIR
																						(BGl_string2824z00zz__errorz00,
																						BgL_arg1489z00_1844);
																				}
																				BgL_arg1487z00_1842 =
																					MAKE_YOUNG_PAIR(BINT(499L),
																					BgL_arg1488z00_1843);
																			}
																			BgL_arg1486z00_1841 =
																				MAKE_YOUNG_PAIR
																				(BGl_string2825z00zz__errorz00,
																				BgL_arg1487z00_1842);
																		}
																		BgL_list1485z00_1840 =
																			MAKE_YOUNG_PAIR
																			(BGl_string2826z00zz__errorz00,
																			BgL_arg1486z00_1841);
																	}
																	BGl_tprintz00zz__r4_output_6_10_3z00
																		(BgL_arg1484z00_1839, BgL_list1485z00_1840);
																}
															}
															{	/* Llib/error.scm 500 */
																obj_t BgL_list1491z00_1846;

																BgL_list1491z00_1846 =
																	MAKE_YOUNG_PAIR(BINT(0L), BNIL);
																BGL_TAIL return
																	BGl_exitz00zz__errorz00(BgL_list1491z00_1846);
															}
														}
												}
											else
												{	/* Llib/error.scm 510 */
													BGl_defaultzd2exceptionzd2handlerz00zz__errorz00
														(BgL_valz00_16);
													{	/* Llib/error.scm 512 */
														obj_t BgL_res2739z00_3789;

														{	/* Llib/error.scm 320 */
															obj_t BgL_classz00_3755;

															BgL_classz00_3755 =
																BGl_z62exceptionz62zz__objectz00;
															((bool_t) 0);
														}
														BgL_res2739z00_3789 =
															BGl_errorz00zz__errorz00
															(BGl_string2828z00zz__errorz00,
															BGl_string2829z00zz__errorz00, BgL_valz00_16);
														return BgL_res2739z00_3789;
													}
												}
										}
								}
						}
					else
						{
							obj_t BgL_valz00_6259;
							obj_t BgL_handlerz00_6258;

							BgL_handlerz00_6258 = BgL_handlerz00_1804;
							BgL_valz00_6259 = BgL_valz00_16;
							BgL_valz00_1830 = BgL_valz00_6259;
							BgL_handlerz00_1829 = BgL_handlerz00_6258;
							goto BgL_zc3z04anonymousza31476ze3z87_1831;
						}
				}
			}
		}

	}



/* &raise */
	obj_t BGl_z62raisez62zz__errorz00(obj_t BgL_envz00_5263,
		obj_t BgL_valz00_5264)
	{
		{	/* Llib/error.scm 465 */
			return BGl_raisez00zz__errorz00(BgL_valz00_5264);
		}

	}



/* default-exception-handler */
	obj_t BGl_defaultzd2exceptionzd2handlerz00zz__errorz00(obj_t BgL_valz00_17)
	{
		{	/* Llib/error.scm 523 */
			BGl_exceptionzd2notifyzd2zz__objectz00(BgL_valz00_17);
			{	/* Llib/error.scm 525 */
				bool_t BgL_test3198z00_6262;

				{	/* Llib/error.scm 525 */
					obj_t BgL_classz00_3790;

					BgL_classz00_3790 = BGl_z62warningz62zz__objectz00;
					if (BGL_OBJECTP(BgL_valz00_17))
						{	/* Llib/error.scm 525 */
							BgL_objectz00_bglt BgL_arg2721z00_3792;

							BgL_arg2721z00_3792 = (BgL_objectz00_bglt) (BgL_valz00_17);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Llib/error.scm 525 */
									long BgL_idxz00_3798;

									BgL_idxz00_3798 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg2721z00_3792);
									BgL_test3198z00_6262 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_3798 + 3L)) == BgL_classz00_3790);
								}
							else
								{	/* Llib/error.scm 525 */
									bool_t BgL_res2740z00_3823;

									{	/* Llib/error.scm 525 */
										obj_t BgL_oclassz00_3806;

										{	/* Llib/error.scm 525 */
											obj_t BgL_arg2728z00_3814;
											long BgL_arg2729z00_3815;

											BgL_arg2728z00_3814 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Llib/error.scm 525 */
												long BgL_arg2731z00_3816;

												BgL_arg2731z00_3816 =
													BGL_OBJECT_CLASS_NUM(BgL_arg2721z00_3792);
												BgL_arg2729z00_3815 =
													(BgL_arg2731z00_3816 - OBJECT_TYPE);
											}
											BgL_oclassz00_3806 =
												VECTOR_REF(BgL_arg2728z00_3814, BgL_arg2729z00_3815);
										}
										{	/* Llib/error.scm 525 */
											bool_t BgL__ortest_1198z00_3807;

											BgL__ortest_1198z00_3807 =
												(BgL_classz00_3790 == BgL_oclassz00_3806);
											if (BgL__ortest_1198z00_3807)
												{	/* Llib/error.scm 525 */
													BgL_res2740z00_3823 = BgL__ortest_1198z00_3807;
												}
											else
												{	/* Llib/error.scm 525 */
													long BgL_odepthz00_3808;

													{	/* Llib/error.scm 525 */
														obj_t BgL_arg2717z00_3809;

														BgL_arg2717z00_3809 = (BgL_oclassz00_3806);
														BgL_odepthz00_3808 =
															BGL_CLASS_DEPTH(BgL_arg2717z00_3809);
													}
													if ((3L < BgL_odepthz00_3808))
														{	/* Llib/error.scm 525 */
															obj_t BgL_arg2715z00_3811;

															{	/* Llib/error.scm 525 */
																obj_t BgL_arg2716z00_3812;

																BgL_arg2716z00_3812 = (BgL_oclassz00_3806);
																BgL_arg2715z00_3811 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg2716z00_3812,
																	3L);
															}
															BgL_res2740z00_3823 =
																(BgL_arg2715z00_3811 == BgL_classz00_3790);
														}
													else
														{	/* Llib/error.scm 525 */
															BgL_res2740z00_3823 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test3198z00_6262 = BgL_res2740z00_3823;
								}
						}
					else
						{	/* Llib/error.scm 525 */
							BgL_test3198z00_6262 = ((bool_t) 0);
						}
				}
				if (BgL_test3198z00_6262)
					{	/* Llib/error.scm 525 */
						BFALSE;
					}
				else
					{	/* Llib/error.scm 526 */
						long BgL_retvalz00_1852;

						{	/* Llib/error.scm 526 */
							bool_t BgL_test3203z00_6285;

							{	/* Llib/error.scm 526 */
								obj_t BgL_classz00_3824;

								BgL_classz00_3824 = BGl_z62errorz62zz__objectz00;
								if (BGL_OBJECTP(BgL_valz00_17))
									{	/* Llib/error.scm 526 */
										BgL_objectz00_bglt BgL_arg2721z00_3826;

										BgL_arg2721z00_3826 = (BgL_objectz00_bglt) (BgL_valz00_17);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Llib/error.scm 526 */
												long BgL_idxz00_3832;

												BgL_idxz00_3832 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg2721z00_3826);
												BgL_test3203z00_6285 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_3832 + 3L)) == BgL_classz00_3824);
											}
										else
											{	/* Llib/error.scm 526 */
												bool_t BgL_res2741z00_3857;

												{	/* Llib/error.scm 526 */
													obj_t BgL_oclassz00_3840;

													{	/* Llib/error.scm 526 */
														obj_t BgL_arg2728z00_3848;
														long BgL_arg2729z00_3849;

														BgL_arg2728z00_3848 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Llib/error.scm 526 */
															long BgL_arg2731z00_3850;

															BgL_arg2731z00_3850 =
																BGL_OBJECT_CLASS_NUM(BgL_arg2721z00_3826);
															BgL_arg2729z00_3849 =
																(BgL_arg2731z00_3850 - OBJECT_TYPE);
														}
														BgL_oclassz00_3840 =
															VECTOR_REF(BgL_arg2728z00_3848,
															BgL_arg2729z00_3849);
													}
													{	/* Llib/error.scm 526 */
														bool_t BgL__ortest_1198z00_3841;

														BgL__ortest_1198z00_3841 =
															(BgL_classz00_3824 == BgL_oclassz00_3840);
														if (BgL__ortest_1198z00_3841)
															{	/* Llib/error.scm 526 */
																BgL_res2741z00_3857 = BgL__ortest_1198z00_3841;
															}
														else
															{	/* Llib/error.scm 526 */
																long BgL_odepthz00_3842;

																{	/* Llib/error.scm 526 */
																	obj_t BgL_arg2717z00_3843;

																	BgL_arg2717z00_3843 = (BgL_oclassz00_3840);
																	BgL_odepthz00_3842 =
																		BGL_CLASS_DEPTH(BgL_arg2717z00_3843);
																}
																if ((3L < BgL_odepthz00_3842))
																	{	/* Llib/error.scm 526 */
																		obj_t BgL_arg2715z00_3845;

																		{	/* Llib/error.scm 526 */
																			obj_t BgL_arg2716z00_3846;

																			BgL_arg2716z00_3846 =
																				(BgL_oclassz00_3840);
																			BgL_arg2715z00_3845 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg2716z00_3846, 3L);
																		}
																		BgL_res2741z00_3857 =
																			(BgL_arg2715z00_3845 ==
																			BgL_classz00_3824);
																	}
																else
																	{	/* Llib/error.scm 526 */
																		BgL_res2741z00_3857 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test3203z00_6285 = BgL_res2741z00_3857;
											}
									}
								else
									{	/* Llib/error.scm 526 */
										BgL_test3203z00_6285 = ((bool_t) 0);
									}
							}
							if (BgL_test3203z00_6285)
								{	/* Llib/error.scm 526 */
									BgL_retvalz00_1852 = 1L;
								}
							else
								{	/* Llib/error.scm 526 */
									BgL_retvalz00_1852 = 2L;
								}
						}
						{	/* Llib/error.scm 527 */
							obj_t BgL_zc3z04anonymousza31495ze3z87_5265;

							BgL_zc3z04anonymousza31495ze3z87_5265 =
								MAKE_FX_PROCEDURE
								(BGl_z62zc3z04anonymousza31495ze3ze5zz__errorz00, (int) (1L),
								(int) (1L));
							PROCEDURE_SET(BgL_zc3z04anonymousza31495ze3z87_5265, (int) (0L),
								BINT(BgL_retvalz00_1852));
							unwind_stack_until(BFALSE, BFALSE, BINT(BgL_retvalz00_1852),
								BgL_zc3z04anonymousza31495ze3z87_5265, BFALSE);
			}}}
			return BUNSPEC;
		}

	}



/* &<@anonymous:1495> */
	obj_t BGl_z62zc3z04anonymousza31495ze3ze5zz__errorz00(obj_t BgL_envz00_5266,
		obj_t BgL_xz00_5268)
	{
		{	/* Llib/error.scm 527 */
			{	/* Llib/error.scm 527 */
				long BgL_retvalz00_5267;

				BgL_retvalz00_5267 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_5266, (int) (0L)));
				{	/* Llib/error.scm 527 */
					obj_t BgL_tmpz00_6319;

					BgL_tmpz00_6319 = BINT(BgL_retvalz00_5267);
					return BIGLOO_EXIT(BgL_tmpz00_6319);
				}
			}
		}

	}



/* module-init-error */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initzd2errorz00zz__errorz00(char
		*BgL_currentz00_18, char *BgL_fromz00_19)
	{
		{	/* Llib/error.scm 533 */
			{	/* Llib/error.scm 534 */
				obj_t BgL_arg1497z00_1858;

				{	/* Llib/error.scm 534 */
					obj_t BgL_tmpz00_6322;

					BgL_tmpz00_6322 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg1497z00_1858 = BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_6322);
				}
				{	/* Llib/error.scm 534 */
					obj_t BgL_list1498z00_1859;

					{	/* Llib/error.scm 534 */
						obj_t BgL_arg1499z00_1860;

						{	/* Llib/error.scm 534 */
							obj_t BgL_arg1500z00_1861;

							{	/* Llib/error.scm 534 */
								obj_t BgL_arg1501z00_1862;

								{	/* Llib/error.scm 534 */
									obj_t BgL_arg1502z00_1863;

									{	/* Llib/error.scm 534 */
										obj_t BgL_arg1503z00_1864;

										{	/* Llib/error.scm 534 */
											obj_t BgL_arg1504z00_1865;

											{	/* Llib/error.scm 534 */
												obj_t BgL_arg1505z00_1866;

												{	/* Llib/error.scm 534 */
													obj_t BgL_arg1506z00_1867;

													{	/* Llib/error.scm 534 */
														obj_t BgL_arg1507z00_1868;

														{	/* Llib/error.scm 534 */
															obj_t BgL_arg1508z00_1869;

															BgL_arg1508z00_1869 =
																MAKE_YOUNG_PAIR(BGl_string2830z00zz__errorz00,
																BNIL);
															BgL_arg1507z00_1868 =
																MAKE_YOUNG_PAIR(string_to_bstring
																(BgL_fromz00_19), BgL_arg1508z00_1869);
														}
														BgL_arg1506z00_1867 =
															MAKE_YOUNG_PAIR(BGl_string2831z00zz__errorz00,
															BgL_arg1507z00_1868);
													}
													BgL_arg1505z00_1866 =
														MAKE_YOUNG_PAIR(BGl_string2832z00zz__errorz00,
														BgL_arg1506z00_1867);
												}
												BgL_arg1504z00_1865 =
													MAKE_YOUNG_PAIR(string_to_bstring(BgL_fromz00_19),
													BgL_arg1505z00_1866);
											}
											BgL_arg1503z00_1864 =
												MAKE_YOUNG_PAIR(BGl_string2833z00zz__errorz00,
												BgL_arg1504z00_1865);
										}
										BgL_arg1502z00_1863 =
											MAKE_YOUNG_PAIR(string_to_bstring(BgL_currentz00_18),
											BgL_arg1503z00_1864);
									}
									BgL_arg1501z00_1862 =
										MAKE_YOUNG_PAIR(BGl_string2834z00zz__errorz00,
										BgL_arg1502z00_1863);
								}
								BgL_arg1500z00_1861 =
									MAKE_YOUNG_PAIR(BGl_string2835z00zz__errorz00,
									BgL_arg1501z00_1862);
							}
							BgL_arg1499z00_1860 =
								MAKE_YOUNG_PAIR(string_to_bstring(BgL_currentz00_18),
								BgL_arg1500z00_1861);
						}
						BgL_list1498z00_1859 =
							MAKE_YOUNG_PAIR(BGl_string2836z00zz__errorz00,
							BgL_arg1499z00_1860);
					}
					BGl_fprintz00zz__r4_output_6_10_3z00(BgL_arg1497z00_1858,
						BgL_list1498z00_1859);
			}}
			{	/* Llib/error.scm 539 */
				obj_t BgL_tmpz00_6341;

				BgL_tmpz00_6341 = BINT(1L);
				return BIGLOO_EXIT(BgL_tmpz00_6341);
			}
		}

	}



/* &module-init-error */
	obj_t BGl_z62modulezd2initzd2errorz62zz__errorz00(obj_t BgL_envz00_5269,
		obj_t BgL_currentz00_5270, obj_t BgL_fromz00_5271)
	{
		{	/* Llib/error.scm 533 */
			{	/* Llib/error.scm 538 */
				char *BgL_auxz00_6353;
				char *BgL_auxz00_6344;

				{	/* Llib/error.scm 538 */
					obj_t BgL_tmpz00_6354;

					if (STRINGP(BgL_fromz00_5271))
						{	/* Llib/error.scm 538 */
							BgL_tmpz00_6354 = BgL_fromz00_5271;
						}
					else
						{
							obj_t BgL_auxz00_6357;

							BgL_auxz00_6357 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2811z00zz__errorz00,
								BINT(22087L), BGl_string2837z00zz__errorz00,
								BGl_string2838z00zz__errorz00, BgL_fromz00_5271);
							FAILURE(BgL_auxz00_6357, BFALSE, BFALSE);
						}
					BgL_auxz00_6353 = BSTRING_TO_STRING(BgL_tmpz00_6354);
				}
				{	/* Llib/error.scm 538 */
					obj_t BgL_tmpz00_6345;

					if (STRINGP(BgL_currentz00_5270))
						{	/* Llib/error.scm 538 */
							BgL_tmpz00_6345 = BgL_currentz00_5270;
						}
					else
						{
							obj_t BgL_auxz00_6348;

							BgL_auxz00_6348 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2811z00zz__errorz00,
								BINT(22087L), BGl_string2837z00zz__errorz00,
								BGl_string2838z00zz__errorz00, BgL_currentz00_5270);
							FAILURE(BgL_auxz00_6348, BFALSE, BFALSE);
						}
					BgL_auxz00_6344 = BSTRING_TO_STRING(BgL_tmpz00_6345);
				}
				return
					BGl_modulezd2initzd2errorz00zz__errorz00(BgL_auxz00_6344,
					BgL_auxz00_6353);
			}
		}

	}



/* error */
	BGL_EXPORTED_DEF obj_t BGl_errorz00zz__errorz00(obj_t BgL_procz00_20,
		obj_t BgL_msgz00_21, obj_t BgL_objz00_22)
	{
		{	/* Llib/error.scm 544 */
			{	/* Llib/error.scm 546 */
				BgL_z62errorz62_bglt BgL_arg1509z00_1870;

				{	/* Llib/error.scm 546 */
					BgL_z62errorz62_bglt BgL_new1079z00_1871;

					{	/* Llib/error.scm 546 */
						BgL_z62errorz62_bglt BgL_new1078z00_1874;

						BgL_new1078z00_1874 =
							((BgL_z62errorz62_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_z62errorz62_bgl))));
						{	/* Llib/error.scm 546 */
							long BgL_arg1513z00_1875;

							BgL_arg1513z00_1875 = BGL_CLASS_NUM(BGl_z62errorz62zz__objectz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1078z00_1874),
								BgL_arg1513z00_1875);
						}
						BgL_new1079z00_1871 = BgL_new1078z00_1874;
					}
					((((BgL_z62exceptionz62_bglt) COBJECT(
									((BgL_z62exceptionz62_bglt) BgL_new1079z00_1871)))->
							BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
					((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
										BgL_new1079z00_1871)))->BgL_locationz00) =
						((obj_t) BFALSE), BUNSPEC);
					{
						obj_t BgL_auxz00_6371;

						{	/* Llib/error.scm 546 */
							obj_t BgL_arg1510z00_1872;

							{	/* Llib/error.scm 546 */
								obj_t BgL_arg1511z00_1873;

								{	/* Llib/error.scm 546 */
									obj_t BgL_classz00_3862;

									BgL_classz00_3862 = BGl_z62errorz62zz__objectz00;
									BgL_arg1511z00_1873 = BGL_CLASS_ALL_FIELDS(BgL_classz00_3862);
								}
								BgL_arg1510z00_1872 = VECTOR_REF(BgL_arg1511z00_1873, 2L);
							}
							BgL_auxz00_6371 =
								BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
								(BgL_arg1510z00_1872);
						}
						((((BgL_z62exceptionz62_bglt) COBJECT(
										((BgL_z62exceptionz62_bglt) BgL_new1079z00_1871)))->
								BgL_stackz00) = ((obj_t) BgL_auxz00_6371), BUNSPEC);
					}
					((((BgL_z62errorz62_bglt) COBJECT(BgL_new1079z00_1871))->
							BgL_procz00) = ((obj_t) BgL_procz00_20), BUNSPEC);
					((((BgL_z62errorz62_bglt) COBJECT(BgL_new1079z00_1871))->BgL_msgz00) =
						((obj_t) BgL_msgz00_21), BUNSPEC);
					((((BgL_z62errorz62_bglt) COBJECT(BgL_new1079z00_1871))->BgL_objz00) =
						((obj_t) BgL_objz00_22), BUNSPEC);
					BgL_arg1509z00_1870 = BgL_new1079z00_1871;
				}
				return BGl_raisez00zz__errorz00(((obj_t) BgL_arg1509z00_1870));
			}
		}

	}



/* &error2758 */
	obj_t BGl_z62error2758z62zz__errorz00(obj_t BgL_envz00_5272,
		obj_t BgL_procz00_5273, obj_t BgL_msgz00_5274, obj_t BgL_objz00_5275)
	{
		{	/* Llib/error.scm 544 */
			return
				BGl_errorz00zz__errorz00(BgL_procz00_5273, BgL_msgz00_5274,
				BgL_objz00_5275);
		}

	}



/* error/location */
	BGL_EXPORTED_DEF obj_t BGl_errorzf2locationzf2zz__errorz00(obj_t
		BgL_procz00_23, obj_t BgL_msgz00_24, obj_t BgL_objz00_25,
		obj_t BgL_fnamez00_26, obj_t BgL_locz00_27)
	{
		{	/* Llib/error.scm 554 */
			{	/* Llib/error.scm 556 */
				BgL_z62errorz62_bglt BgL_arg1514z00_3864;

				{	/* Llib/error.scm 556 */
					BgL_z62errorz62_bglt BgL_new1081z00_3865;

					{	/* Llib/error.scm 557 */
						BgL_z62errorz62_bglt BgL_new1080z00_3866;

						BgL_new1080z00_3866 =
							((BgL_z62errorz62_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_z62errorz62_bgl))));
						{	/* Llib/error.scm 557 */
							long BgL_arg1521z00_3867;

							BgL_arg1521z00_3867 = BGL_CLASS_NUM(BGl_z62errorz62zz__objectz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1080z00_3866),
								BgL_arg1521z00_3867);
						}
						BgL_new1081z00_3865 = BgL_new1080z00_3866;
					}
					((((BgL_z62exceptionz62_bglt) COBJECT(
									((BgL_z62exceptionz62_bglt) BgL_new1081z00_3865)))->
							BgL_fnamez00) = ((obj_t) BgL_fnamez00_26), BUNSPEC);
					((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
										BgL_new1081z00_3865)))->BgL_locationz00) =
						((obj_t) BgL_locz00_27), BUNSPEC);
					{
						obj_t BgL_auxz00_6391;

						{	/* Llib/error.scm 558 */
							obj_t BgL_arg1516z00_3868;

							{	/* Llib/error.scm 558 */
								obj_t BgL_arg1517z00_3869;

								{	/* Llib/error.scm 558 */
									obj_t BgL_classz00_3873;

									BgL_classz00_3873 = BGl_z62errorz62zz__objectz00;
									BgL_arg1517z00_3869 = BGL_CLASS_ALL_FIELDS(BgL_classz00_3873);
								}
								BgL_arg1516z00_3868 = VECTOR_REF(BgL_arg1517z00_3869, 2L);
							}
							BgL_auxz00_6391 =
								BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
								(BgL_arg1516z00_3868);
						}
						((((BgL_z62exceptionz62_bglt) COBJECT(
										((BgL_z62exceptionz62_bglt) BgL_new1081z00_3865)))->
								BgL_stackz00) = ((obj_t) BgL_auxz00_6391), BUNSPEC);
					}
					((((BgL_z62errorz62_bglt) COBJECT(BgL_new1081z00_3865))->
							BgL_procz00) = ((obj_t) BgL_procz00_23), BUNSPEC);
					((((BgL_z62errorz62_bglt) COBJECT(BgL_new1081z00_3865))->BgL_msgz00) =
						((obj_t) BgL_msgz00_24), BUNSPEC);
					((((BgL_z62errorz62_bglt) COBJECT(BgL_new1081z00_3865))->BgL_objz00) =
						((obj_t) BgL_objz00_25), BUNSPEC);
					BgL_arg1514z00_3864 = BgL_new1081z00_3865;
				}
				return BGl_raisez00zz__errorz00(((obj_t) BgL_arg1514z00_3864));
			}
		}

	}



/* &error/location */
	obj_t BGl_z62errorzf2locationz90zz__errorz00(obj_t BgL_envz00_5276,
		obj_t BgL_procz00_5277, obj_t BgL_msgz00_5278, obj_t BgL_objz00_5279,
		obj_t BgL_fnamez00_5280, obj_t BgL_locz00_5281)
	{
		{	/* Llib/error.scm 554 */
			return
				BGl_errorzf2locationzf2zz__errorz00(BgL_procz00_5277, BgL_msgz00_5278,
				BgL_objz00_5279, BgL_fnamez00_5280, BgL_locz00_5281);
		}

	}



/* error/source-location */
	BGL_EXPORTED_DEF obj_t BGl_errorzf2sourcezd2locationz20zz__errorz00(obj_t
		BgL_procz00_28, obj_t BgL_msgz00_29, obj_t BgL_objz00_30,
		obj_t BgL_locz00_31)
	{
		{	/* Llib/error.scm 566 */
			if (PAIRP(BgL_locz00_31))
				{	/* Llib/error.scm 567 */
					obj_t BgL_cdrzd2186zd2_1888;

					BgL_cdrzd2186zd2_1888 = CDR(((obj_t) BgL_locz00_31));
					if ((CAR(((obj_t) BgL_locz00_31)) == BGl_symbol2815z00zz__errorz00))
						{	/* Llib/error.scm 567 */
							if (PAIRP(BgL_cdrzd2186zd2_1888))
								{	/* Llib/error.scm 567 */
									obj_t BgL_cdrzd2190zd2_1892;

									BgL_cdrzd2190zd2_1892 = CDR(BgL_cdrzd2186zd2_1888);
									if (PAIRP(BgL_cdrzd2190zd2_1892))
										{	/* Llib/error.scm 567 */
											if (NULLP(CDR(BgL_cdrzd2190zd2_1892)))
												{	/* Llib/error.scm 567 */
													return
														BGl_errorzf2locationzf2zz__errorz00(BgL_procz00_28,
														BgL_msgz00_29, BgL_objz00_30,
														CAR(BgL_cdrzd2186zd2_1888),
														CAR(BgL_cdrzd2190zd2_1892));
												}
											else
												{	/* Llib/error.scm 567 */
													return
														BGl_errorz00zz__errorz00(BgL_procz00_28,
														BgL_msgz00_29, BgL_objz00_30);
												}
										}
									else
										{	/* Llib/error.scm 567 */
											return
												BGl_errorz00zz__errorz00(BgL_procz00_28, BgL_msgz00_29,
												BgL_objz00_30);
										}
								}
							else
								{	/* Llib/error.scm 567 */
									return
										BGl_errorz00zz__errorz00(BgL_procz00_28, BgL_msgz00_29,
										BgL_objz00_30);
								}
						}
					else
						{	/* Llib/error.scm 567 */
							return
								BGl_errorz00zz__errorz00(BgL_procz00_28, BgL_msgz00_29,
								BgL_objz00_30);
						}
				}
			else
				{	/* Llib/error.scm 567 */
					return
						BGl_errorz00zz__errorz00(BgL_procz00_28, BgL_msgz00_29,
						BgL_objz00_30);
				}
		}

	}



/* &error/source-location */
	obj_t BGl_z62errorzf2sourcezd2locationz42zz__errorz00(obj_t BgL_envz00_5282,
		obj_t BgL_procz00_5283, obj_t BgL_msgz00_5284, obj_t BgL_objz00_5285,
		obj_t BgL_locz00_5286)
	{
		{	/* Llib/error.scm 566 */
			return
				BGl_errorzf2sourcezd2locationz20zz__errorz00(BgL_procz00_5283,
				BgL_msgz00_5284, BgL_objz00_5285, BgL_locz00_5286);
		}

	}



/* error/source */
	BGL_EXPORTED_DEF obj_t BGl_errorzf2sourcezf2zz__errorz00(obj_t BgL_procz00_32,
		obj_t BgL_msgz00_33, obj_t BgL_objz00_34, obj_t BgL_sourcez00_35)
	{
		{	/* Llib/error.scm 576 */
			if (EPAIRP(BgL_sourcez00_35))
				{	/* Llib/error.scm 579 */
					obj_t BgL_arg1537z00_3894;

					BgL_arg1537z00_3894 = CER(((obj_t) BgL_sourcez00_35));
					BGL_TAIL return
						BGl_errorzf2sourcezd2locationz20zz__errorz00(BgL_procz00_32,
						BgL_msgz00_33, BgL_objz00_34, BgL_arg1537z00_3894);
				}
			else
				{	/* Llib/error.scm 577 */
					return
						BGl_errorz00zz__errorz00(BgL_procz00_32, BgL_msgz00_33,
						BgL_objz00_34);
				}
		}

	}



/* &error/source */
	obj_t BGl_z62errorzf2sourcez90zz__errorz00(obj_t BgL_envz00_5287,
		obj_t BgL_procz00_5288, obj_t BgL_msgz00_5289, obj_t BgL_objz00_5290,
		obj_t BgL_sourcez00_5291)
	{
		{	/* Llib/error.scm 576 */
			return
				BGl_errorzf2sourcezf2zz__errorz00(BgL_procz00_5288, BgL_msgz00_5289,
				BgL_objz00_5290, BgL_sourcez00_5291);
		}

	}



/* error/c-location */
	BGL_EXPORTED_DEF obj_t BGl_errorzf2czd2locationz20zz__errorz00(obj_t
		BgL_procz00_36, obj_t BgL_messagez00_37, obj_t BgL_objectz00_38,
		char *BgL_fnamez00_39, long BgL_locz00_40)
	{
		{	/* Llib/error.scm 588 */
			{	/* Llib/error.scm 556 */
				BgL_z62errorz62_bglt BgL_arg1514z00_3896;

				{	/* Llib/error.scm 556 */
					BgL_z62errorz62_bglt BgL_new1081z00_3897;

					{	/* Llib/error.scm 557 */
						BgL_z62errorz62_bglt BgL_new1080z00_3898;

						BgL_new1080z00_3898 =
							((BgL_z62errorz62_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_z62errorz62_bgl))));
						{	/* Llib/error.scm 557 */
							long BgL_arg1521z00_3899;

							BgL_arg1521z00_3899 = BGL_CLASS_NUM(BGl_z62errorz62zz__objectz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1080z00_3898),
								BgL_arg1521z00_3899);
						}
						BgL_new1081z00_3897 = BgL_new1080z00_3898;
					}
					((((BgL_z62exceptionz62_bglt) COBJECT(
									((BgL_z62exceptionz62_bglt) BgL_new1081z00_3897)))->
							BgL_fnamez00) =
						((obj_t) string_to_bstring(BgL_fnamez00_39)), BUNSPEC);
					((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
										BgL_new1081z00_3897)))->BgL_locationz00) =
						((obj_t) BINT(BgL_locz00_40)), BUNSPEC);
					{
						obj_t BgL_auxz00_6445;

						{	/* Llib/error.scm 558 */
							obj_t BgL_arg1516z00_3900;

							{	/* Llib/error.scm 558 */
								obj_t BgL_arg1517z00_3901;

								{	/* Llib/error.scm 558 */
									obj_t BgL_classz00_3905;

									BgL_classz00_3905 = BGl_z62errorz62zz__objectz00;
									BgL_arg1517z00_3901 = BGL_CLASS_ALL_FIELDS(BgL_classz00_3905);
								}
								BgL_arg1516z00_3900 = VECTOR_REF(BgL_arg1517z00_3901, 2L);
							}
							BgL_auxz00_6445 =
								BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
								(BgL_arg1516z00_3900);
						}
						((((BgL_z62exceptionz62_bglt) COBJECT(
										((BgL_z62exceptionz62_bglt) BgL_new1081z00_3897)))->
								BgL_stackz00) = ((obj_t) BgL_auxz00_6445), BUNSPEC);
					}
					((((BgL_z62errorz62_bglt) COBJECT(BgL_new1081z00_3897))->
							BgL_procz00) = ((obj_t) BgL_procz00_36), BUNSPEC);
					((((BgL_z62errorz62_bglt) COBJECT(BgL_new1081z00_3897))->BgL_msgz00) =
						((obj_t) BgL_messagez00_37), BUNSPEC);
					((((BgL_z62errorz62_bglt) COBJECT(BgL_new1081z00_3897))->BgL_objz00) =
						((obj_t) BgL_objectz00_38), BUNSPEC);
					BgL_arg1514z00_3896 = BgL_new1081z00_3897;
				}
				return BGl_raisez00zz__errorz00(((obj_t) BgL_arg1514z00_3896));
			}
		}

	}



/* &error/c-location */
	obj_t BGl_z62errorzf2czd2locationz42zz__errorz00(obj_t BgL_envz00_5292,
		obj_t BgL_procz00_5293, obj_t BgL_messagez00_5294, obj_t BgL_objectz00_5295,
		obj_t BgL_fnamez00_5296, obj_t BgL_locz00_5297)
	{
		{	/* Llib/error.scm 588 */
			{	/* Llib/error.scm 556 */
				long BgL_auxz00_6465;
				char *BgL_auxz00_6456;

				{	/* Llib/error.scm 556 */
					obj_t BgL_tmpz00_6466;

					if (INTEGERP(BgL_locz00_5297))
						{	/* Llib/error.scm 556 */
							BgL_tmpz00_6466 = BgL_locz00_5297;
						}
					else
						{
							obj_t BgL_auxz00_6469;

							BgL_auxz00_6469 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2811z00zz__errorz00,
								BINT(22720L), BGl_string2839z00zz__errorz00,
								BGl_string2813z00zz__errorz00, BgL_locz00_5297);
							FAILURE(BgL_auxz00_6469, BFALSE, BFALSE);
						}
					BgL_auxz00_6465 = (long) CINT(BgL_tmpz00_6466);
				}
				{	/* Llib/error.scm 556 */
					obj_t BgL_tmpz00_6457;

					if (STRINGP(BgL_fnamez00_5296))
						{	/* Llib/error.scm 556 */
							BgL_tmpz00_6457 = BgL_fnamez00_5296;
						}
					else
						{
							obj_t BgL_auxz00_6460;

							BgL_auxz00_6460 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2811z00zz__errorz00,
								BINT(22720L), BGl_string2839z00zz__errorz00,
								BGl_string2838z00zz__errorz00, BgL_fnamez00_5296);
							FAILURE(BgL_auxz00_6460, BFALSE, BFALSE);
						}
					BgL_auxz00_6456 = BSTRING_TO_STRING(BgL_tmpz00_6457);
				}
				return
					BGl_errorzf2czd2locationz20zz__errorz00(BgL_procz00_5293,
					BgL_messagez00_5294, BgL_objectz00_5295, BgL_auxz00_6456,
					BgL_auxz00_6465);
			}
		}

	}



/* bigloo-type-error-msg */
	BGL_EXPORTED_DEF obj_t BGl_bigloozd2typezd2errorzd2msgzd2zz__errorz00(obj_t
		BgL_prefixz00_41, obj_t BgL_fromz00_42, obj_t BgL_toz00_43)
	{
		{	/* Llib/error.scm 594 */
			{	/* Llib/error.scm 595 */
				obj_t BgL_list1538z00_3907;

				{	/* Llib/error.scm 595 */
					obj_t BgL_arg1539z00_3908;

					{	/* Llib/error.scm 595 */
						obj_t BgL_arg1540z00_3909;

						{	/* Llib/error.scm 595 */
							obj_t BgL_arg1543z00_3910;

							{	/* Llib/error.scm 595 */
								obj_t BgL_arg1544z00_3911;

								{	/* Llib/error.scm 595 */
									obj_t BgL_arg1546z00_3912;

									BgL_arg1546z00_3912 =
										MAKE_YOUNG_PAIR(BGl_string2840z00zz__errorz00, BNIL);
									BgL_arg1544z00_3911 =
										MAKE_YOUNG_PAIR(BgL_toz00_43, BgL_arg1546z00_3912);
								}
								BgL_arg1543z00_3910 =
									MAKE_YOUNG_PAIR(BGl_string2841z00zz__errorz00,
									BgL_arg1544z00_3911);
							}
							BgL_arg1540z00_3909 =
								MAKE_YOUNG_PAIR(BgL_fromz00_42, BgL_arg1543z00_3910);
						}
						BgL_arg1539z00_3908 =
							MAKE_YOUNG_PAIR(BGl_string2842z00zz__errorz00,
							BgL_arg1540z00_3909);
					}
					BgL_list1538z00_3907 =
						MAKE_YOUNG_PAIR(BgL_prefixz00_41, BgL_arg1539z00_3908);
				}
				return
					BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_list1538z00_3907);
			}
		}

	}



/* &bigloo-type-error-msg */
	obj_t BGl_z62bigloozd2typezd2errorzd2msgzb0zz__errorz00(obj_t BgL_envz00_5298,
		obj_t BgL_prefixz00_5299, obj_t BgL_fromz00_5300, obj_t BgL_toz00_5301)
	{
		{	/* Llib/error.scm 594 */
			{	/* Llib/error.scm 595 */
				obj_t BgL_auxz00_6496;
				obj_t BgL_auxz00_6489;
				obj_t BgL_auxz00_6482;

				if (STRINGP(BgL_toz00_5301))
					{	/* Llib/error.scm 595 */
						BgL_auxz00_6496 = BgL_toz00_5301;
					}
				else
					{
						obj_t BgL_auxz00_6499;

						BgL_auxz00_6499 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2811z00zz__errorz00,
							BINT(24503L), BGl_string2843z00zz__errorz00,
							BGl_string2838z00zz__errorz00, BgL_toz00_5301);
						FAILURE(BgL_auxz00_6499, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_fromz00_5300))
					{	/* Llib/error.scm 595 */
						BgL_auxz00_6489 = BgL_fromz00_5300;
					}
				else
					{
						obj_t BgL_auxz00_6492;

						BgL_auxz00_6492 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2811z00zz__errorz00,
							BINT(24503L), BGl_string2843z00zz__errorz00,
							BGl_string2838z00zz__errorz00, BgL_fromz00_5300);
						FAILURE(BgL_auxz00_6492, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_prefixz00_5299))
					{	/* Llib/error.scm 595 */
						BgL_auxz00_6482 = BgL_prefixz00_5299;
					}
				else
					{
						obj_t BgL_auxz00_6485;

						BgL_auxz00_6485 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2811z00zz__errorz00,
							BINT(24503L), BGl_string2843z00zz__errorz00,
							BGl_string2838z00zz__errorz00, BgL_prefixz00_5299);
						FAILURE(BgL_auxz00_6485, BFALSE, BFALSE);
					}
				return
					BGl_bigloozd2typezd2errorzd2msgzd2zz__errorz00(BgL_auxz00_6482,
					BgL_auxz00_6489, BgL_auxz00_6496);
			}
		}

	}



/* bigloo-type-error */
	BGL_EXPORTED_DEF obj_t BGl_bigloozd2typezd2errorz00zz__errorz00(obj_t
		BgL_procz00_44, obj_t BgL_typez00_45, obj_t BgL_objz00_46)
	{
		{	/* Llib/error.scm 600 */
			{	/* Llib/error.scm 601 */
				obj_t BgL_tyz00_1908;

				if (STRINGP(BgL_typez00_45))
					{	/* Llib/error.scm 602 */
						BgL_tyz00_1908 = BgL_typez00_45;
					}
				else
					{	/* Llib/error.scm 602 */
						if (SYMBOLP(BgL_typez00_45))
							{	/* Llib/error.scm 605 */
								obj_t BgL_arg2328z00_3914;

								BgL_arg2328z00_3914 = SYMBOL_TO_STRING(BgL_typez00_45);
								BgL_tyz00_1908 =
									BGl_stringzd2copyzd2zz__r4_strings_6_7z00
									(BgL_arg2328z00_3914);
							}
						else
							{	/* Llib/error.scm 604 */
								BgL_tyz00_1908 = BGl_string2809z00zz__errorz00;
							}
					}
				{	/* Llib/error.scm 601 */
					obj_t BgL_msgz00_1909;

					{	/* Llib/error.scm 608 */
						obj_t BgL_arg1554z00_1916;

						BgL_arg1554z00_1916 = bgl_typeof(BgL_objz00_46);
						{	/* Llib/error.scm 595 */
							obj_t BgL_list1538z00_3918;

							{	/* Llib/error.scm 595 */
								obj_t BgL_arg1539z00_3919;

								{	/* Llib/error.scm 595 */
									obj_t BgL_arg1540z00_3920;

									{	/* Llib/error.scm 595 */
										obj_t BgL_arg1543z00_3921;

										{	/* Llib/error.scm 595 */
											obj_t BgL_arg1544z00_3922;

											{	/* Llib/error.scm 595 */
												obj_t BgL_arg1546z00_3923;

												BgL_arg1546z00_3923 =
													MAKE_YOUNG_PAIR(BGl_string2840z00zz__errorz00, BNIL);
												BgL_arg1544z00_3922 =
													MAKE_YOUNG_PAIR(BgL_arg1554z00_1916,
													BgL_arg1546z00_3923);
											}
											BgL_arg1543z00_3921 =
												MAKE_YOUNG_PAIR(BGl_string2841z00zz__errorz00,
												BgL_arg1544z00_3922);
										}
										BgL_arg1540z00_3920 =
											MAKE_YOUNG_PAIR(BgL_tyz00_1908, BgL_arg1543z00_3921);
									}
									BgL_arg1539z00_3919 =
										MAKE_YOUNG_PAIR(BGl_string2842z00zz__errorz00,
										BgL_arg1540z00_3920);
								}
								BgL_list1538z00_3918 =
									MAKE_YOUNG_PAIR(BGl_string2810z00zz__errorz00,
									BgL_arg1539z00_3919);
							}
							BgL_msgz00_1909 =
								BGl_stringzd2appendzd2zz__r4_strings_6_7z00
								(BgL_list1538z00_3918);
						}
					}
					{	/* Llib/error.scm 608 */

						{	/* Llib/error.scm 610 */
							BgL_z62typezd2errorzb0_bglt BgL_arg1547z00_1910;

							{	/* Llib/error.scm 610 */
								BgL_z62typezd2errorzb0_bglt BgL_new1083z00_1911;

								{	/* Llib/error.scm 610 */
									BgL_z62typezd2errorzb0_bglt BgL_new1082z00_1914;

									BgL_new1082z00_1914 =
										((BgL_z62typezd2errorzb0_bglt)
										BOBJECT(GC_MALLOC(sizeof(struct
													BgL_z62typezd2errorzb0_bgl))));
									{	/* Llib/error.scm 610 */
										long BgL_arg1553z00_1915;

										BgL_arg1553z00_1915 =
											BGL_CLASS_NUM(BGl_z62typezd2errorzb0zz__objectz00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1082z00_1914),
											BgL_arg1553z00_1915);
									}
									BgL_new1083z00_1911 = BgL_new1082z00_1914;
								}
								((((BgL_z62exceptionz62_bglt) COBJECT(
												((BgL_z62exceptionz62_bglt) BgL_new1083z00_1911)))->
										BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
								((((BgL_z62exceptionz62_bglt)
											COBJECT(((BgL_z62exceptionz62_bglt)
													BgL_new1083z00_1911)))->BgL_locationz00) =
									((obj_t) BFALSE), BUNSPEC);
								{
									obj_t BgL_auxz00_6526;

									{	/* Llib/error.scm 610 */
										obj_t BgL_arg1549z00_1912;

										{	/* Llib/error.scm 610 */
											obj_t BgL_arg1552z00_1913;

											{	/* Llib/error.scm 610 */
												obj_t BgL_classz00_3927;

												BgL_classz00_3927 = BGl_z62typezd2errorzb0zz__objectz00;
												BgL_arg1552z00_1913 =
													BGL_CLASS_ALL_FIELDS(BgL_classz00_3927);
											}
											BgL_arg1549z00_1912 = VECTOR_REF(BgL_arg1552z00_1913, 2L);
										}
										BgL_auxz00_6526 =
											BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
											(BgL_arg1549z00_1912);
									}
									((((BgL_z62exceptionz62_bglt) COBJECT(
													((BgL_z62exceptionz62_bglt) BgL_new1083z00_1911)))->
											BgL_stackz00) = ((obj_t) BgL_auxz00_6526), BUNSPEC);
								}
								((((BgL_z62errorz62_bglt) COBJECT(
												((BgL_z62errorz62_bglt) BgL_new1083z00_1911)))->
										BgL_procz00) = ((obj_t) BgL_procz00_44), BUNSPEC);
								((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
													BgL_new1083z00_1911)))->BgL_msgz00) =
									((obj_t) BgL_msgz00_1909), BUNSPEC);
								((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
													BgL_new1083z00_1911)))->BgL_objz00) =
									((obj_t) BgL_objz00_46), BUNSPEC);
								((((BgL_z62typezd2errorzb0_bglt) COBJECT(BgL_new1083z00_1911))->
										BgL_typez00) = ((obj_t) BgL_typez00_45), BUNSPEC);
								BgL_arg1547z00_1910 = BgL_new1083z00_1911;
							}
							return BGl_raisez00zz__errorz00(((obj_t) BgL_arg1547z00_1910));
						}
					}
				}
			}
		}

	}



/* &bigloo-type-error */
	obj_t BGl_z62bigloozd2typezd2errorz62zz__errorz00(obj_t BgL_envz00_5302,
		obj_t BgL_procz00_5303, obj_t BgL_typez00_5304, obj_t BgL_objz00_5305)
	{
		{	/* Llib/error.scm 600 */
			return
				BGl_bigloozd2typezd2errorz00zz__errorz00(BgL_procz00_5303,
				BgL_typez00_5304, BgL_objz00_5305);
		}

	}



/* bigloo-type-error/location */
	BGL_EXPORTED_DEF obj_t
		BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(obj_t BgL_procz00_47,
		obj_t BgL_typez00_48, obj_t BgL_objz00_49, obj_t BgL_fnamez00_50,
		obj_t BgL_locz00_51)
	{
		{	/* Llib/error.scm 619 */
			return
				BGl_raisez00zz__errorz00(BGl_typezd2errorzd2zz__errorz00
				(BgL_fnamez00_50, BgL_locz00_51, BgL_procz00_47, BgL_typez00_48,
					BgL_objz00_49));
		}

	}



/* &bigloo-type-error/location */
	obj_t BGl_z62bigloozd2typezd2errorzf2locationz90zz__errorz00(obj_t
		BgL_envz00_5306, obj_t BgL_procz00_5307, obj_t BgL_typez00_5308,
		obj_t BgL_objz00_5309, obj_t BgL_fnamez00_5310, obj_t BgL_locz00_5311)
	{
		{	/* Llib/error.scm 619 */
			return
				BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BgL_procz00_5307,
				BgL_typez00_5308, BgL_objz00_5309, BgL_fnamez00_5310, BgL_locz00_5311);
		}

	}



/* type-error */
	BGL_EXPORTED_DEF obj_t BGl_typezd2errorzd2zz__errorz00(obj_t BgL_fnamez00_52,
		obj_t BgL_locz00_53, obj_t BgL_procz00_54, obj_t BgL_typez00_55,
		obj_t BgL_objz00_56)
	{
		{	/* Llib/error.scm 625 */
			{	/* Llib/error.scm 626 */
				obj_t BgL_tyz00_1920;

				if (STRINGP(BgL_typez00_55))
					{	/* Llib/error.scm 627 */
						BgL_tyz00_1920 = BgL_typez00_55;
					}
				else
					{	/* Llib/error.scm 627 */
						if (SYMBOLP(BgL_typez00_55))
							{	/* Llib/error.scm 628 */
								obj_t BgL_arg2328z00_3931;

								BgL_arg2328z00_3931 = SYMBOL_TO_STRING(BgL_typez00_55);
								BgL_tyz00_1920 =
									BGl_stringzd2copyzd2zz__r4_strings_6_7z00
									(BgL_arg2328z00_3931);
							}
						else
							{	/* Llib/error.scm 628 */
								BgL_tyz00_1920 = BGl_string2809z00zz__errorz00;
							}
					}
				{	/* Llib/error.scm 626 */
					obj_t BgL_msgz00_1921;

					{	/* Llib/error.scm 630 */
						obj_t BgL_arg1562z00_1927;

						BgL_arg1562z00_1927 = bgl_typeof(BgL_objz00_56);
						{	/* Llib/error.scm 595 */
							obj_t BgL_list1538z00_3935;

							{	/* Llib/error.scm 595 */
								obj_t BgL_arg1539z00_3936;

								{	/* Llib/error.scm 595 */
									obj_t BgL_arg1540z00_3937;

									{	/* Llib/error.scm 595 */
										obj_t BgL_arg1543z00_3938;

										{	/* Llib/error.scm 595 */
											obj_t BgL_arg1544z00_3939;

											{	/* Llib/error.scm 595 */
												obj_t BgL_arg1546z00_3940;

												BgL_arg1546z00_3940 =
													MAKE_YOUNG_PAIR(BGl_string2840z00zz__errorz00, BNIL);
												BgL_arg1544z00_3939 =
													MAKE_YOUNG_PAIR(BgL_arg1562z00_1927,
													BgL_arg1546z00_3940);
											}
											BgL_arg1543z00_3938 =
												MAKE_YOUNG_PAIR(BGl_string2841z00zz__errorz00,
												BgL_arg1544z00_3939);
										}
										BgL_arg1540z00_3937 =
											MAKE_YOUNG_PAIR(BgL_tyz00_1920, BgL_arg1543z00_3938);
									}
									BgL_arg1539z00_3936 =
										MAKE_YOUNG_PAIR(BGl_string2842z00zz__errorz00,
										BgL_arg1540z00_3937);
								}
								BgL_list1538z00_3935 =
									MAKE_YOUNG_PAIR(BGl_string2810z00zz__errorz00,
									BgL_arg1539z00_3936);
							}
							BgL_msgz00_1921 =
								BGl_stringzd2appendzd2zz__r4_strings_6_7z00
								(BgL_list1538z00_3935);
						}
					}
					{	/* Llib/error.scm 630 */

						{	/* Llib/error.scm 631 */
							BgL_z62typezd2errorzb0_bglt BgL_new1085z00_1922;

							{	/* Llib/error.scm 632 */
								BgL_z62typezd2errorzb0_bglt BgL_new1084z00_1925;

								BgL_new1084z00_1925 =
									((BgL_z62typezd2errorzb0_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_z62typezd2errorzb0_bgl))));
								{	/* Llib/error.scm 632 */
									long BgL_arg1561z00_1926;

									BgL_arg1561z00_1926 =
										BGL_CLASS_NUM(BGl_z62typezd2errorzb0zz__objectz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1084z00_1925),
										BgL_arg1561z00_1926);
								}
								BgL_new1085z00_1922 = BgL_new1084z00_1925;
							}
							((((BgL_z62exceptionz62_bglt) COBJECT(
											((BgL_z62exceptionz62_bglt) BgL_new1085z00_1922)))->
									BgL_fnamez00) = ((obj_t) BgL_fnamez00_52), BUNSPEC);
							((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
												BgL_new1085z00_1922)))->BgL_locationz00) =
								((obj_t) BgL_locz00_53), BUNSPEC);
							{
								obj_t BgL_auxz00_6567;

								{	/* Llib/error.scm 633 */
									obj_t BgL_arg1558z00_1923;

									{	/* Llib/error.scm 633 */
										obj_t BgL_arg1559z00_1924;

										{	/* Llib/error.scm 633 */
											obj_t BgL_classz00_3944;

											BgL_classz00_3944 = BGl_z62typezd2errorzb0zz__objectz00;
											BgL_arg1559z00_1924 =
												BGL_CLASS_ALL_FIELDS(BgL_classz00_3944);
										}
										BgL_arg1558z00_1923 = VECTOR_REF(BgL_arg1559z00_1924, 2L);
									}
									BgL_auxz00_6567 =
										BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
										(BgL_arg1558z00_1923);
								}
								((((BgL_z62exceptionz62_bglt) COBJECT(
												((BgL_z62exceptionz62_bglt) BgL_new1085z00_1922)))->
										BgL_stackz00) = ((obj_t) BgL_auxz00_6567), BUNSPEC);
							}
							((((BgL_z62errorz62_bglt) COBJECT(
											((BgL_z62errorz62_bglt) BgL_new1085z00_1922)))->
									BgL_procz00) = ((obj_t) BgL_procz00_54), BUNSPEC);
							((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
												BgL_new1085z00_1922)))->BgL_msgz00) =
								((obj_t) BgL_msgz00_1921), BUNSPEC);
							((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
												BgL_new1085z00_1922)))->BgL_objz00) =
								((obj_t) BgL_objz00_56), BUNSPEC);
							((((BgL_z62typezd2errorzb0_bglt) COBJECT(BgL_new1085z00_1922))->
									BgL_typez00) = ((obj_t) BgL_typez00_55), BUNSPEC);
							return ((obj_t) BgL_new1085z00_1922);
						}
					}
				}
			}
		}

	}



/* &type-error2759 */
	obj_t BGl_z62typezd2error2759zb0zz__errorz00(obj_t BgL_envz00_5312,
		obj_t BgL_fnamez00_5313, obj_t BgL_locz00_5314, obj_t BgL_procz00_5315,
		obj_t BgL_typez00_5316, obj_t BgL_objz00_5317)
	{
		{	/* Llib/error.scm 625 */
			return
				BGl_typezd2errorzd2zz__errorz00(BgL_fnamez00_5313, BgL_locz00_5314,
				BgL_procz00_5315, BgL_typez00_5316, BgL_objz00_5317);
		}

	}



/* index-out-of-bounds-error */
	BGL_EXPORTED_DEF obj_t
		BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(obj_t BgL_fnamez00_62,
		obj_t BgL_locz00_63, obj_t BgL_procz00_64, obj_t BgL_objz00_65,
		int BgL_lenz00_66, int BgL_iz00_67)
	{
		{	/* Llib/error.scm 659 */
			{	/* Llib/error.scm 660 */
				obj_t BgL_msgz00_1940;

				{	/* Llib/error.scm 665 */
					obj_t BgL_arg1579z00_1946;
					obj_t BgL_arg1580z00_1947;

					{	/* Ieee/fixnum.scm 1001 */

						BgL_arg1579z00_1946 =
							BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
							(long) (BgL_iz00_67), 10L);
					}
					{	/* Ieee/fixnum.scm 1001 */

						BgL_arg1580z00_1947 =
							BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
							((long) (BgL_lenz00_66) - 1L), 10L);
					}
					{	/* Llib/error.scm 664 */
						obj_t BgL_list1581z00_1948;

						{	/* Llib/error.scm 664 */
							obj_t BgL_arg1582z00_1949;

							{	/* Llib/error.scm 664 */
								obj_t BgL_arg1583z00_1950;

								{	/* Llib/error.scm 664 */
									obj_t BgL_arg1584z00_1951;

									{	/* Llib/error.scm 664 */
										obj_t BgL_arg1585z00_1952;

										BgL_arg1585z00_1952 =
											MAKE_YOUNG_PAIR(BGl_string2844z00zz__errorz00, BNIL);
										BgL_arg1584z00_1951 =
											MAKE_YOUNG_PAIR(BgL_arg1580z00_1947, BgL_arg1585z00_1952);
									}
									BgL_arg1583z00_1950 =
										MAKE_YOUNG_PAIR(BGl_string2845z00zz__errorz00,
										BgL_arg1584z00_1951);
								}
								BgL_arg1582z00_1949 =
									MAKE_YOUNG_PAIR(BgL_arg1579z00_1946, BgL_arg1583z00_1950);
							}
							BgL_list1581z00_1948 =
								MAKE_YOUNG_PAIR(BGl_string2846z00zz__errorz00,
								BgL_arg1582z00_1949);
						}
						BgL_msgz00_1940 =
							BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_list1581z00_1948);
				}}
				{	/* Llib/error.scm 664 */

					{	/* Llib/error.scm 668 */
						BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt
							BgL_new1089z00_1941;
						{	/* Llib/error.scm 669 */
							BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt
								BgL_new1088z00_1944;
							BgL_new1088z00_1944 =
								((BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt)
								BOBJECT(GC_MALLOC(sizeof(struct
											BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bgl))));
							{	/* Llib/error.scm 669 */
								long BgL_arg1578z00_1945;

								BgL_arg1578z00_1945 =
									BGL_CLASS_NUM
									(BGl_z62indexzd2outzd2ofzd2boundszd2errorz62zz__objectz00);
								BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
										BgL_new1088z00_1944), BgL_arg1578z00_1945);
							}
							BgL_new1089z00_1941 = BgL_new1088z00_1944;
						}
						((((BgL_z62exceptionz62_bglt) COBJECT(
										((BgL_z62exceptionz62_bglt) BgL_new1089z00_1941)))->
								BgL_fnamez00) = ((obj_t) BgL_fnamez00_62), BUNSPEC);
						((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
											BgL_new1089z00_1941)))->BgL_locationz00) =
							((obj_t) BgL_locz00_63), BUNSPEC);
						{
							obj_t BgL_auxz00_6601;

							{	/* Llib/error.scm 670 */
								obj_t BgL_arg1575z00_1942;

								{	/* Llib/error.scm 670 */
									obj_t BgL_arg1576z00_1943;

									{	/* Llib/error.scm 670 */
										obj_t BgL_classz00_3966;

										BgL_classz00_3966 =
											BGl_z62indexzd2outzd2ofzd2boundszd2errorz62zz__objectz00;
										BgL_arg1576z00_1943 =
											BGL_CLASS_ALL_FIELDS(BgL_classz00_3966);
									}
									BgL_arg1575z00_1942 = VECTOR_REF(BgL_arg1576z00_1943, 2L);
								}
								BgL_auxz00_6601 =
									BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
									(BgL_arg1575z00_1942);
							}
							((((BgL_z62exceptionz62_bglt) COBJECT(
											((BgL_z62exceptionz62_bglt) BgL_new1089z00_1941)))->
									BgL_stackz00) = ((obj_t) BgL_auxz00_6601), BUNSPEC);
						}
						((((BgL_z62errorz62_bglt) COBJECT(
										((BgL_z62errorz62_bglt) BgL_new1089z00_1941)))->
								BgL_procz00) = ((obj_t) BgL_procz00_64), BUNSPEC);
						((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
											BgL_new1089z00_1941)))->BgL_msgz00) =
							((obj_t) BgL_msgz00_1940), BUNSPEC);
						((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
											BgL_new1089z00_1941)))->BgL_objz00) =
							((obj_t) BgL_objz00_65), BUNSPEC);
						((((BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt)
									COBJECT(BgL_new1089z00_1941))->BgL_indexz00) =
							((obj_t) BINT(BgL_lenz00_66)), BUNSPEC);
						return ((obj_t) BgL_new1089z00_1941);
					}
				}
			}
		}

	}



/* &index-out-of-bounds2760 */
	obj_t BGl_z62indexzd2outzd2ofzd2bounds2760zb0zz__errorz00(obj_t
		BgL_envz00_5318, obj_t BgL_fnamez00_5319, obj_t BgL_locz00_5320,
		obj_t BgL_procz00_5321, obj_t BgL_objz00_5322, obj_t BgL_lenz00_5323,
		obj_t BgL_iz00_5324)
	{
		{	/* Llib/error.scm 659 */
			{	/* Llib/error.scm 660 */
				int BgL_auxz00_6625;
				int BgL_auxz00_6616;

				{	/* Llib/error.scm 660 */
					obj_t BgL_tmpz00_6626;

					if (INTEGERP(BgL_iz00_5324))
						{	/* Llib/error.scm 660 */
							BgL_tmpz00_6626 = BgL_iz00_5324;
						}
					else
						{
							obj_t BgL_auxz00_6629;

							BgL_auxz00_6629 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2811z00zz__errorz00,
								BINT(26845L), BGl_string2847z00zz__errorz00,
								BGl_string2813z00zz__errorz00, BgL_iz00_5324);
							FAILURE(BgL_auxz00_6629, BFALSE, BFALSE);
						}
					BgL_auxz00_6625 = CINT(BgL_tmpz00_6626);
				}
				{	/* Llib/error.scm 660 */
					obj_t BgL_tmpz00_6617;

					if (INTEGERP(BgL_lenz00_5323))
						{	/* Llib/error.scm 660 */
							BgL_tmpz00_6617 = BgL_lenz00_5323;
						}
					else
						{
							obj_t BgL_auxz00_6620;

							BgL_auxz00_6620 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2811z00zz__errorz00,
								BINT(26845L), BGl_string2847z00zz__errorz00,
								BGl_string2813z00zz__errorz00, BgL_lenz00_5323);
							FAILURE(BgL_auxz00_6620, BFALSE, BFALSE);
						}
					BgL_auxz00_6616 = CINT(BgL_tmpz00_6617);
				}
				return
					BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00
					(BgL_fnamez00_5319, BgL_locz00_5320, BgL_procz00_5321,
					BgL_objz00_5322, BgL_auxz00_6616, BgL_auxz00_6625);
			}
		}

	}



/* warning */
	BGL_EXPORTED_DEF obj_t BGl_warningz00zz__errorz00(obj_t BgL_argsz00_68)
	{
		{	/* Llib/error.scm 679 */
			{	/* Llib/error.scm 681 */
				BgL_z62warningz62_bglt BgL_arg1591z00_1963;

				{	/* Llib/error.scm 681 */
					BgL_z62warningz62_bglt BgL_new1091z00_1964;

					{	/* Llib/error.scm 681 */
						BgL_z62warningz62_bglt BgL_new1090z00_1967;

						BgL_new1090z00_1967 =
							((BgL_z62warningz62_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_z62warningz62_bgl))));
						{	/* Llib/error.scm 681 */
							long BgL_arg1595z00_1968;

							BgL_arg1595z00_1968 =
								BGL_CLASS_NUM(BGl_z62warningz62zz__objectz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1090z00_1967),
								BgL_arg1595z00_1968);
						}
						BgL_new1091z00_1964 = BgL_new1090z00_1967;
					}
					((((BgL_z62exceptionz62_bglt) COBJECT(
									((BgL_z62exceptionz62_bglt) BgL_new1091z00_1964)))->
							BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
					((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
										BgL_new1091z00_1964)))->BgL_locationz00) =
						((obj_t) BFALSE), BUNSPEC);
					{
						obj_t BgL_auxz00_6643;

						{	/* Llib/error.scm 681 */
							obj_t BgL_arg1593z00_1965;

							{	/* Llib/error.scm 681 */
								obj_t BgL_arg1594z00_1966;

								{	/* Llib/error.scm 681 */
									obj_t BgL_classz00_3971;

									BgL_classz00_3971 = BGl_z62warningz62zz__objectz00;
									BgL_arg1594z00_1966 = BGL_CLASS_ALL_FIELDS(BgL_classz00_3971);
								}
								BgL_arg1593z00_1965 = VECTOR_REF(BgL_arg1594z00_1966, 2L);
							}
							BgL_auxz00_6643 =
								BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
								(BgL_arg1593z00_1965);
						}
						((((BgL_z62exceptionz62_bglt) COBJECT(
										((BgL_z62exceptionz62_bglt) BgL_new1091z00_1964)))->
								BgL_stackz00) = ((obj_t) BgL_auxz00_6643), BUNSPEC);
					}
					((((BgL_z62warningz62_bglt) COBJECT(BgL_new1091z00_1964))->
							BgL_argsz00) = ((obj_t) BgL_argsz00_68), BUNSPEC);
					BgL_arg1591z00_1963 = BgL_new1091z00_1964;
				}
				BGL_TAIL return
					BGl_warningzd2notifyzd2zz__errorz00(((obj_t) BgL_arg1591z00_1963));
			}
		}

	}



/* &warning2761 */
	obj_t BGl_z62warning2761z62zz__errorz00(obj_t BgL_envz00_5325,
		obj_t BgL_argsz00_5326)
	{
		{	/* Llib/error.scm 679 */
			return BGl_warningz00zz__errorz00(BgL_argsz00_5326);
		}

	}



/* warning/location */
	BGL_EXPORTED_DEF obj_t BGl_warningzf2locationzf2zz__errorz00(obj_t
		BgL_fnamez00_69, obj_t BgL_locz00_70, obj_t BgL_argsz00_71)
	{
		{	/* Llib/error.scm 686 */
			{	/* Llib/error.scm 688 */
				BgL_z62warningz62_bglt BgL_arg1598z00_3973;

				{	/* Llib/error.scm 688 */
					BgL_z62warningz62_bglt BgL_new1093z00_3974;

					{	/* Llib/error.scm 688 */
						BgL_z62warningz62_bglt BgL_new1092z00_3975;

						BgL_new1092z00_3975 =
							((BgL_z62warningz62_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_z62warningz62_bgl))));
						{	/* Llib/error.scm 688 */
							long BgL_arg1603z00_3976;

							BgL_arg1603z00_3976 =
								BGL_CLASS_NUM(BGl_z62warningz62zz__objectz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1092z00_3975),
								BgL_arg1603z00_3976);
						}
						BgL_new1093z00_3974 = BgL_new1092z00_3975;
					}
					((((BgL_z62exceptionz62_bglt) COBJECT(
									((BgL_z62exceptionz62_bglt) BgL_new1093z00_3974)))->
							BgL_fnamez00) = ((obj_t) BgL_fnamez00_69), BUNSPEC);
					((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
										BgL_new1093z00_3974)))->BgL_locationz00) =
						((obj_t) BgL_locz00_70), BUNSPEC);
					{
						obj_t BgL_auxz00_6661;

						{	/* Llib/error.scm 688 */
							obj_t BgL_arg1601z00_3977;

							{	/* Llib/error.scm 688 */
								obj_t BgL_arg1602z00_3978;

								{	/* Llib/error.scm 688 */
									obj_t BgL_classz00_3982;

									BgL_classz00_3982 = BGl_z62warningz62zz__objectz00;
									BgL_arg1602z00_3978 = BGL_CLASS_ALL_FIELDS(BgL_classz00_3982);
								}
								BgL_arg1601z00_3977 = VECTOR_REF(BgL_arg1602z00_3978, 2L);
							}
							BgL_auxz00_6661 =
								BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
								(BgL_arg1601z00_3977);
						}
						((((BgL_z62exceptionz62_bglt) COBJECT(
										((BgL_z62exceptionz62_bglt) BgL_new1093z00_3974)))->
								BgL_stackz00) = ((obj_t) BgL_auxz00_6661), BUNSPEC);
					}
					((((BgL_z62warningz62_bglt) COBJECT(BgL_new1093z00_3974))->
							BgL_argsz00) = ((obj_t) BgL_argsz00_71), BUNSPEC);
					BgL_arg1598z00_3973 = BgL_new1093z00_3974;
				}
				return
					BGl_warningzd2notifyzd2zz__errorz00(((obj_t) BgL_arg1598z00_3973));
			}
		}

	}



/* &warning/location */
	obj_t BGl_z62warningzf2locationz90zz__errorz00(obj_t BgL_envz00_5327,
		obj_t BgL_fnamez00_5328, obj_t BgL_locz00_5329, obj_t BgL_argsz00_5330)
	{
		{	/* Llib/error.scm 686 */
			return
				BGl_warningzf2locationzf2zz__errorz00(BgL_fnamez00_5328,
				BgL_locz00_5329, BgL_argsz00_5330);
		}

	}



/* warning/loc */
	BGL_EXPORTED_DEF obj_t BGl_warningzf2loczf2zz__errorz00(obj_t BgL_locz00_72,
		obj_t BgL_argsz00_73)
	{
		{	/* Llib/error.scm 693 */
			if (PAIRP(BgL_locz00_72))
				{	/* Llib/error.scm 694 */
					obj_t BgL_cdrzd2204zd2_1981;

					BgL_cdrzd2204zd2_1981 = CDR(((obj_t) BgL_locz00_72));
					if ((CAR(((obj_t) BgL_locz00_72)) == BGl_symbol2815z00zz__errorz00))
						{	/* Llib/error.scm 694 */
							if (PAIRP(BgL_cdrzd2204zd2_1981))
								{	/* Llib/error.scm 694 */
									obj_t BgL_cdrzd2208zd2_1985;

									BgL_cdrzd2208zd2_1985 = CDR(BgL_cdrzd2204zd2_1981);
									if (PAIRP(BgL_cdrzd2208zd2_1985))
										{	/* Llib/error.scm 694 */
											if (NULLP(CDR(BgL_cdrzd2208zd2_1985)))
												{	/* Llib/error.scm 694 */
													obj_t BgL_arg1611z00_1989;
													obj_t BgL_arg1612z00_1990;

													BgL_arg1611z00_1989 = CAR(BgL_cdrzd2204zd2_1981);
													BgL_arg1612z00_1990 = CAR(BgL_cdrzd2208zd2_1985);
													{	/* Llib/error.scm 696 */
														obj_t BgL_list1616z00_3994;

														{	/* Llib/error.scm 696 */
															obj_t BgL_arg1617z00_3995;

															BgL_arg1617z00_3995 =
																MAKE_YOUNG_PAIR(BgL_argsz00_73, BNIL);
															BgL_list1616z00_3994 =
																MAKE_YOUNG_PAIR(BgL_arg1612z00_1990,
																BgL_arg1617z00_3995);
														}
														return
															BGl_applyz00zz__r4_control_features_6_9z00
															(BGl_warningzf2locationzd2envz20zz__errorz00,
															BgL_arg1611z00_1989, BgL_list1616z00_3994);
													}
												}
											else
												{	/* Llib/error.scm 694 */
													return BGl_warningz00zz__errorz00(BgL_argsz00_73);
												}
										}
									else
										{	/* Llib/error.scm 694 */
											return BGl_warningz00zz__errorz00(BgL_argsz00_73);
										}
								}
							else
								{	/* Llib/error.scm 694 */
									return BGl_warningz00zz__errorz00(BgL_argsz00_73);
								}
						}
					else
						{	/* Llib/error.scm 694 */
							return BGl_warningz00zz__errorz00(BgL_argsz00_73);
						}
				}
			else
				{	/* Llib/error.scm 694 */
					return BGl_warningz00zz__errorz00(BgL_argsz00_73);
				}
		}

	}



/* &warning/loc */
	obj_t BGl_z62warningzf2locz90zz__errorz00(obj_t BgL_envz00_5331,
		obj_t BgL_locz00_5332, obj_t BgL_argsz00_5333)
	{
		{	/* Llib/error.scm 693 */
			return
				BGl_warningzf2loczf2zz__errorz00(BgL_locz00_5332, BgL_argsz00_5333);
		}

	}



/* warning/c-location */
	BGL_EXPORTED_DEF obj_t BGl_warningzf2czd2locationz20zz__errorz00(char
		*BgL_fnamez00_74, long BgL_locz00_75, obj_t BgL_argsz00_76)
	{
		{	/* Llib/error.scm 707 */
			{	/* Llib/error.scm 708 */
				obj_t BgL_list1619z00_4001;

				{	/* Llib/error.scm 708 */
					obj_t BgL_arg1620z00_4002;

					BgL_arg1620z00_4002 = MAKE_YOUNG_PAIR(BgL_argsz00_76, BNIL);
					BgL_list1619z00_4001 =
						MAKE_YOUNG_PAIR(BINT(BgL_locz00_75), BgL_arg1620z00_4002);
				}
				return
					BGl_applyz00zz__r4_control_features_6_9z00
					(BGl_warningzf2locationzd2envz20zz__errorz00,
					string_to_bstring(BgL_fnamez00_74), BgL_list1619z00_4001);
			}
		}

	}



/* &warning/c-location */
	obj_t BGl_z62warningzf2czd2locationz42zz__errorz00(obj_t BgL_envz00_5334,
		obj_t BgL_fnamez00_5335, obj_t BgL_locz00_5336, obj_t BgL_argsz00_5337)
	{
		{	/* Llib/error.scm 707 */
			{	/* Llib/error.scm 708 */
				long BgL_auxz00_6712;
				char *BgL_auxz00_6703;

				{	/* Llib/error.scm 708 */
					obj_t BgL_tmpz00_6713;

					if (INTEGERP(BgL_locz00_5336))
						{	/* Llib/error.scm 708 */
							BgL_tmpz00_6713 = BgL_locz00_5336;
						}
					else
						{
							obj_t BgL_auxz00_6716;

							BgL_auxz00_6716 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2811z00zz__errorz00,
								BINT(28832L), BGl_string2848z00zz__errorz00,
								BGl_string2813z00zz__errorz00, BgL_locz00_5336);
							FAILURE(BgL_auxz00_6716, BFALSE, BFALSE);
						}
					BgL_auxz00_6712 = (long) CINT(BgL_tmpz00_6713);
				}
				{	/* Llib/error.scm 708 */
					obj_t BgL_tmpz00_6704;

					if (STRINGP(BgL_fnamez00_5335))
						{	/* Llib/error.scm 708 */
							BgL_tmpz00_6704 = BgL_fnamez00_5335;
						}
					else
						{
							obj_t BgL_auxz00_6707;

							BgL_auxz00_6707 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2811z00zz__errorz00,
								BINT(28832L), BGl_string2848z00zz__errorz00,
								BGl_string2838z00zz__errorz00, BgL_fnamez00_5335);
							FAILURE(BgL_auxz00_6707, BFALSE, BFALSE);
						}
					BgL_auxz00_6703 = BSTRING_TO_STRING(BgL_tmpz00_6704);
				}
				return
					BGl_warningzf2czd2locationz20zz__errorz00(BgL_auxz00_6703,
					BgL_auxz00_6712, BgL_argsz00_5337);
			}
		}

	}



/* notify-&error */
	obj_t BGl_notifyzd2z62errorzb0zz__errorz00(obj_t BgL_errz00_77)
	{
		{	/* Llib/error.scm 713 */
			{	/* Llib/error.scm 714 */
				obj_t BgL_portz00_1998;

				{	/* Llib/error.scm 714 */
					obj_t BgL_tmpz00_6722;

					BgL_tmpz00_6722 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_portz00_1998 = BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_6722);
				}
				bgl_flush_output_port(BgL_portz00_1998);
				{	/* Llib/error.scm 717 */
					obj_t BgL_list1621z00_2000;

					BgL_list1621z00_2000 = MAKE_YOUNG_PAIR(BgL_portz00_1998, BNIL);
					BGl_displayz00zz__r4_output_6_10_3z00(BGl_string2836z00zz__errorz00,
						BgL_list1621z00_2000);
				}
				{	/* Llib/error.scm 718 */
					obj_t BgL_arg1622z00_2001;

					BgL_arg1622z00_2001 =
						(((BgL_z62errorz62_bglt) COBJECT(
								((BgL_z62errorz62_bglt) BgL_errz00_77)))->BgL_procz00);
					BGl_displayzd2circlezd2zz__pp_circlez00(BgL_arg1622z00_2001,
						BgL_portz00_1998);
				}
				{	/* Llib/error.scm 719 */
					obj_t BgL_list1623z00_2002;

					BgL_list1623z00_2002 = MAKE_YOUNG_PAIR(BgL_portz00_1998, BNIL);
					BGl_displayz00zz__r4_output_6_10_3z00(BGl_string2849z00zz__errorz00,
						BgL_list1623z00_2002);
				}
				{	/* Llib/error.scm 720 */
					obj_t BgL_arg1624z00_2003;

					BgL_arg1624z00_2003 =
						(((BgL_z62errorz62_bglt) COBJECT(
								((BgL_z62errorz62_bglt) BgL_errz00_77)))->BgL_msgz00);
					BGl_displayzd2circlezd2zz__pp_circlez00(BgL_arg1624z00_2003,
						BgL_portz00_1998);
				}
				if (
					((((BgL_z62errorz62_bglt) COBJECT(
									((BgL_z62errorz62_bglt) BgL_errz00_77)))->BgL_objz00) ==
						BGl_symbol2850z00zz__errorz00))
					{	/* Llib/error.scm 721 */
						BFALSE;
					}
				else
					{	/* Llib/error.scm 721 */
						{	/* Llib/error.scm 722 */
							obj_t BgL_list1627z00_2006;

							BgL_list1627z00_2006 = MAKE_YOUNG_PAIR(BgL_portz00_1998, BNIL);
							BGl_displayz00zz__r4_output_6_10_3z00
								(BGl_string2852z00zz__errorz00, BgL_list1627z00_2006);
						}
						{	/* Llib/error.scm 723 */
							obj_t BgL_arg1628z00_2007;

							BgL_arg1628z00_2007 =
								(((BgL_z62errorz62_bglt) COBJECT(
										((BgL_z62errorz62_bglt) BgL_errz00_77)))->BgL_objz00);
							BGl_displayzd2circlezd2zz__pp_circlez00(BgL_arg1628z00_2007,
								BgL_portz00_1998);
						}
					}
				{	/* Llib/error.scm 724 */
					obj_t BgL_list1630z00_2009;

					BgL_list1630z00_2009 = MAKE_YOUNG_PAIR(BgL_portz00_1998, BNIL);
					BGl_newlinez00zz__r4_output_6_10_3z00(BgL_list1630z00_2009);
				}
				{	/* Llib/error.scm 725 */
					obj_t BgL_arg1631z00_2010;

					{	/* Llib/error.scm 725 */
						obj_t BgL__ortest_1095z00_2014;

						BgL__ortest_1095z00_2014 =
							(((BgL_z62exceptionz62_bglt) COBJECT(
									((BgL_z62exceptionz62_bglt)
										((BgL_z62errorz62_bglt) BgL_errz00_77))))->BgL_stackz00);
						if (CBOOL(BgL__ortest_1095z00_2014))
							{	/* Llib/error.scm 725 */
								BgL_arg1631z00_2010 = BgL__ortest_1095z00_2014;
							}
						else
							{	/* Llib/error.scm 725 */

								{	/* Llib/error.scm 725 */

									BgL_arg1631z00_2010 =
										BGl_getzd2tracezd2stackz00zz__errorz00(BFALSE);
								}
							}
					}
					{	/* Llib/error.scm 273 */

						BGl_displayzd2tracezd2stackz00zz__errorz00(BgL_arg1631z00_2010,
							BgL_portz00_1998, BINT(1L));
					}
				}
				return bgl_flush_output_port(BgL_portz00_1998);
			}
		}

	}



/* notify-&error/location-no-loc */
	obj_t BGl_notifyzd2z62errorzf2locationzd2nozd2locz42zz__errorz00(obj_t
		BgL_errz00_78)
	{
		{	/* Llib/error.scm 731 */
			{	/* Llib/error.scm 732 */
				obj_t BgL_portz00_2016;

				{	/* Llib/error.scm 732 */
					obj_t BgL_tmpz00_6756;

					BgL_tmpz00_6756 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_portz00_2016 = BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_6756);
				}
				bgl_flush_output_port(BgL_portz00_2016);
				{	/* Llib/error.scm 735 */
					obj_t BgL_list1632z00_2018;

					BgL_list1632z00_2018 = MAKE_YOUNG_PAIR(BgL_portz00_2016, BNIL);
					BGl_newlinez00zz__r4_output_6_10_3z00(BgL_list1632z00_2018);
				}
				{	/* Llib/error.scm 736 */
					obj_t BgL_arg1634z00_2019;
					obj_t BgL_arg1636z00_2020;

					BgL_arg1634z00_2019 =
						(((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt)
									((BgL_z62errorz62_bglt) BgL_errz00_78))))->BgL_fnamez00);
					BgL_arg1636z00_2020 =
						(((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt)
									((BgL_z62errorz62_bglt) BgL_errz00_78))))->BgL_locationz00);
					{	/* Llib/error.scm 736 */
						obj_t BgL_list1637z00_2021;

						{	/* Llib/error.scm 736 */
							obj_t BgL_arg1638z00_2022;

							{	/* Llib/error.scm 736 */
								obj_t BgL_arg1639z00_2023;

								{	/* Llib/error.scm 736 */
									obj_t BgL_arg1640z00_2024;

									{	/* Llib/error.scm 736 */
										obj_t BgL_arg1641z00_2025;

										BgL_arg1641z00_2025 =
											MAKE_YOUNG_PAIR(BCHAR(((unsigned char) ':')), BNIL);
										BgL_arg1640z00_2024 =
											MAKE_YOUNG_PAIR(BgL_arg1636z00_2020, BgL_arg1641z00_2025);
									}
									BgL_arg1639z00_2023 =
										MAKE_YOUNG_PAIR(BGl_string2853z00zz__errorz00,
										BgL_arg1640z00_2024);
								}
								BgL_arg1638z00_2022 =
									MAKE_YOUNG_PAIR(BgL_arg1634z00_2019, BgL_arg1639z00_2023);
							}
							BgL_list1637z00_2021 =
								MAKE_YOUNG_PAIR(BGl_string2854z00zz__errorz00,
								BgL_arg1638z00_2022);
						}
						BGl_fprintz00zz__r4_output_6_10_3z00(BgL_portz00_2016,
							BgL_list1637z00_2021);
				}}
				BGL_TAIL return BGl_notifyzd2z62errorzb0zz__errorz00(BgL_errz00_78);
			}
		}

	}



/* notify-&error/location-loc */
	obj_t BGl_notifyzd2z62errorzf2locationzd2locz90zz__errorz00(obj_t
		BgL_errz00_79, obj_t BgL_fnamez00_80, obj_t BgL_linez00_81,
		obj_t BgL_locz00_82, obj_t BgL_stringz00_83, obj_t BgL_colz00_84)
	{
		{	/* Llib/error.scm 742 */
		BGl_notifyzd2z62errorzf2locationzd2locz90zz__errorz00:
			{	/* Llib/error.scm 743 */
				long BgL_lenz00_2026;

				BgL_lenz00_2026 = STRING_LENGTH(((obj_t) BgL_stringz00_83));
				if ((BgL_lenz00_2026 > 256L))
					{	/* Llib/error.scm 745 */
						obj_t BgL_nstringz00_2029;

						{	/* Llib/error.scm 746 */
							long BgL_arg1649z00_2035;
							long BgL_arg1650z00_2036;

							{	/* Llib/error.scm 746 */
								long BgL_arg1651z00_2037;

								BgL_arg1651z00_2037 = ((long) CINT(BgL_colz00_84) - 60L);
								{	/* Llib/error.scm 746 */
									obj_t BgL_list1652z00_2038;

									BgL_list1652z00_2038 =
										MAKE_YOUNG_PAIR(BINT(BgL_arg1651z00_2037), BNIL);
									BgL_arg1649z00_2035 =
										BGl_maxfxz00zz__r4_numbers_6_5_fixnumz00(0L,
										BgL_list1652z00_2038);
							}}
							BgL_arg1650z00_2036 = ((long) CINT(BgL_colz00_84) + 10L);
							BgL_nstringz00_2029 =
								BGl_substringz00zz__r4_strings_6_7z00(BgL_stringz00_83,
								BgL_arg1649z00_2035, BgL_arg1650z00_2036);
						}
						{	/* Llib/error.scm 746 */

							{	/* Llib/error.scm 748 */
								obj_t BgL_arg1643z00_2030;
								long BgL_arg1644z00_2031;

								{	/* Llib/error.scm 748 */
									obj_t BgL_list1645z00_2032;

									{	/* Llib/error.scm 748 */
										obj_t BgL_arg1646z00_2033;

										{	/* Llib/error.scm 748 */
											obj_t BgL_arg1648z00_2034;

											BgL_arg1648z00_2034 =
												MAKE_YOUNG_PAIR(BGl_string2855z00zz__errorz00, BNIL);
											BgL_arg1646z00_2033 =
												MAKE_YOUNG_PAIR(BgL_nstringz00_2029,
												BgL_arg1648z00_2034);
										}
										BgL_list1645z00_2032 =
											MAKE_YOUNG_PAIR(BGl_string2855z00zz__errorz00,
											BgL_arg1646z00_2033);
									}
									BgL_arg1643z00_2030 =
										BGl_stringzd2appendzd2zz__r4_strings_6_7z00
										(BgL_list1645z00_2032);
								}
								BgL_arg1644z00_2031 = (60L + 3L);
								{
									obj_t BgL_colz00_6794;
									obj_t BgL_stringz00_6793;

									BgL_stringz00_6793 = BgL_arg1643z00_2030;
									BgL_colz00_6794 = BINT(BgL_arg1644z00_2031);
									BgL_colz00_84 = BgL_colz00_6794;
									BgL_stringz00_83 = BgL_stringz00_6793;
									goto BGl_notifyzd2z62errorzf2locationzd2locz90zz__errorz00;
								}
							}
						}
					}
				else
					{	/* Llib/error.scm 751 */
						obj_t BgL_portz00_2040;

						{	/* Llib/error.scm 751 */
							obj_t BgL_tmpz00_6796;

							BgL_tmpz00_6796 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_portz00_2040 = BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_6796);
						}
						bgl_flush_output_port(BgL_portz00_2040);
						{	/* Llib/error.scm 754 */
							obj_t BgL_list1653z00_2041;

							BgL_list1653z00_2041 = MAKE_YOUNG_PAIR(BgL_portz00_2040, BNIL);
							BGl_newlinez00zz__r4_output_6_10_3z00(BgL_list1653z00_2041);
						}
						{	/* Llib/error.scm 755 */
							obj_t BgL_spacezd2stringzd2_2042;

							if (((long) CINT(BgL_colz00_84) > 0L))
								{	/* Llib/error.scm 755 */
									BgL_spacezd2stringzd2_2042 =
										make_string(
										(long) CINT(BgL_colz00_84), ((unsigned char) ' '));
								}
							else
								{	/* Llib/error.scm 755 */
									BgL_spacezd2stringzd2_2042 = BGl_string2856z00zz__errorz00;
								}
							{	/* Llib/error.scm 755 */
								long BgL_lz00_2043;

								BgL_lz00_2043 = STRING_LENGTH(((obj_t) BgL_stringz00_83));
								{	/* Llib/error.scm 756 */
									obj_t BgL_nzd2colzd2_2044;

									if (((long) CINT(BgL_colz00_84) >= BgL_lz00_2043))
										{	/* Llib/error.scm 757 */
											BgL_nzd2colzd2_2044 = BINT(BgL_lz00_2043);
										}
									else
										{	/* Llib/error.scm 757 */
											BgL_nzd2colzd2_2044 = BgL_colz00_84;
										}
									{	/* Llib/error.scm 757 */

										BGl_fixzd2tabulationz12zc0zz__errorz00(BgL_nzd2colzd2_2044,
											BgL_stringz00_83, BgL_spacezd2stringzd2_2042);
										BGl_printzd2cursorzd2zz__errorz00(BgL_fnamez00_80,
											BgL_linez00_81, BgL_locz00_82, BgL_stringz00_83,
											BgL_spacezd2stringzd2_2042);
										{	/* Llib/error.scm 763 */
											obj_t BgL_list1654z00_2045;

											BgL_list1654z00_2045 =
												MAKE_YOUNG_PAIR(BgL_portz00_2040, BNIL);
											BGl_displayz00zz__r4_output_6_10_3z00
												(BGl_string2836z00zz__errorz00, BgL_list1654z00_2045);
										}
										{	/* Llib/error.scm 764 */
											obj_t BgL_arg1656z00_2046;

											BgL_arg1656z00_2046 =
												(((BgL_z62errorz62_bglt) COBJECT(
														((BgL_z62errorz62_bglt) BgL_errz00_79)))->
												BgL_procz00);
											BGl_displayzd2circlezd2zz__pp_circlez00
												(BgL_arg1656z00_2046, BgL_portz00_2040);
										}
										{	/* Llib/error.scm 765 */
											obj_t BgL_list1657z00_2047;

											BgL_list1657z00_2047 =
												MAKE_YOUNG_PAIR(BgL_portz00_2040, BNIL);
											BGl_newlinez00zz__r4_output_6_10_3z00
												(BgL_list1657z00_2047);
										}
										{	/* Llib/error.scm 766 */
											obj_t BgL_arg1658z00_2048;

											BgL_arg1658z00_2048 =
												(((BgL_z62errorz62_bglt) COBJECT(
														((BgL_z62errorz62_bglt) BgL_errz00_79)))->
												BgL_msgz00);
											BGl_displayzd2circlezd2zz__pp_circlez00
												(BgL_arg1658z00_2048, BgL_portz00_2040);
										}
										if (
											((((BgL_z62errorz62_bglt) COBJECT(
															((BgL_z62errorz62_bglt) BgL_errz00_79)))->
													BgL_objz00) == BGl_symbol2850z00zz__errorz00))
											{	/* Llib/error.scm 767 */
												BFALSE;
											}
										else
											{	/* Llib/error.scm 767 */
												{	/* Llib/error.scm 768 */
													obj_t BgL_list1663z00_2051;

													BgL_list1663z00_2051 =
														MAKE_YOUNG_PAIR(BgL_portz00_2040, BNIL);
													BGl_displayz00zz__r4_output_6_10_3z00
														(BGl_string2852z00zz__errorz00,
														BgL_list1663z00_2051);
												}
												{	/* Llib/error.scm 769 */
													obj_t BgL_arg1664z00_2052;

													BgL_arg1664z00_2052 =
														(((BgL_z62errorz62_bglt) COBJECT(
																((BgL_z62errorz62_bglt) BgL_errz00_79)))->
														BgL_objz00);
													BGl_displayzd2circlezd2zz__pp_circlez00
														(BgL_arg1664z00_2052, BgL_portz00_2040);
												}
											}
										{	/* Llib/error.scm 770 */
											obj_t BgL_list1668z00_2054;

											BgL_list1668z00_2054 =
												MAKE_YOUNG_PAIR(BgL_portz00_2040, BNIL);
											BGl_newlinez00zz__r4_output_6_10_3z00
												(BgL_list1668z00_2054);
										}
										{	/* Llib/error.scm 771 */
											obj_t BgL_arg1669z00_2055;

											{	/* Llib/error.scm 771 */
												obj_t BgL__ortest_1098z00_2059;

												BgL__ortest_1098z00_2059 =
													(((BgL_z62exceptionz62_bglt) COBJECT(
															((BgL_z62exceptionz62_bglt)
																((BgL_z62errorz62_bglt) BgL_errz00_79))))->
													BgL_stackz00);
												if (CBOOL(BgL__ortest_1098z00_2059))
													{	/* Llib/error.scm 771 */
														BgL_arg1669z00_2055 = BgL__ortest_1098z00_2059;
													}
												else
													{	/* Llib/error.scm 771 */

														{	/* Llib/error.scm 771 */

															BgL_arg1669z00_2055 =
																BGl_getzd2tracezd2stackz00zz__errorz00(BFALSE);
														}
													}
											}
											{	/* Llib/error.scm 273 */

												BGl_displayzd2tracezd2stackz00zz__errorz00
													(BgL_arg1669z00_2055, BgL_portz00_2040, BINT(1L));
											}
										}
										return bgl_flush_output_port(BgL_portz00_2040);
									}
								}
							}
						}
					}
			}
		}

	}



/* notify-&error/loc */
	obj_t BGl_notifyzd2z62errorzf2locz42zz__errorz00(obj_t BgL_errz00_85,
		obj_t BgL_fnamez00_86, obj_t BgL_locz00_87)
	{
		{	/* Llib/error.scm 778 */
			{	/* Llib/error.scm 779 */
				bool_t BgL_test3241z00_6845;

				if (STRINGP(BgL_fnamez00_86))
					{	/* Llib/error.scm 779 */
						if (INTEGERP(BgL_locz00_87))
							{	/* Llib/error.scm 779 */
								BgL_test3241z00_6845 = ((bool_t) 0);
							}
						else
							{	/* Llib/error.scm 779 */
								BgL_test3241z00_6845 = ((bool_t) 1);
							}
					}
				else
					{	/* Llib/error.scm 779 */
						BgL_test3241z00_6845 = ((bool_t) 1);
					}
				if (BgL_test3241z00_6845)
					{	/* Llib/error.scm 779 */
						return BGl_notifyzd2z62errorzb0zz__errorz00(BgL_errz00_85);
					}
				else
					{	/* Llib/error.scm 781 */
						obj_t BgL_filez00_2066;

						{	/* Llib/error.scm 782 */
							obj_t BgL_arg1675z00_2070;

							{	/* Llib/error.scm 782 */
								obj_t BgL_list1676z00_2071;

								{	/* Llib/error.scm 782 */
									obj_t BgL_arg1678z00_2072;

									{	/* Llib/error.scm 782 */
										obj_t BgL_arg1681z00_2073;

										BgL_arg1681z00_2073 = MAKE_YOUNG_PAIR(BNIL, BNIL);
										BgL_arg1678z00_2072 =
											MAKE_YOUNG_PAIR(BgL_locz00_87, BgL_arg1681z00_2073);
									}
									BgL_list1676z00_2071 =
										MAKE_YOUNG_PAIR(BgL_fnamez00_86, BgL_arg1678z00_2072);
								}
								BgL_arg1675z00_2070 =
									BGl_consza2za2zz__r4_pairs_and_lists_6_3z00
									(BGl_symbol2815z00zz__errorz00, BgL_list1676z00_2071);
							}
							BgL_filez00_2066 =
								BGl_locationzd2linezd2numz00zz__errorz00(BgL_arg1675z00_2070);
						}
						{	/* Llib/error.scm 782 */
							obj_t BgL_lnumz00_2067;
							obj_t BgL_lpointz00_2068;
							obj_t BgL_lstringz00_2069;

							{	/* Llib/error.scm 783 */
								obj_t BgL_tmpz00_4024;

								{	/* Llib/error.scm 783 */
									int BgL_tmpz00_6856;

									BgL_tmpz00_6856 = (int) (1L);
									BgL_tmpz00_4024 = BGL_MVALUES_VAL(BgL_tmpz00_6856);
								}
								{	/* Llib/error.scm 783 */
									int BgL_tmpz00_6859;

									BgL_tmpz00_6859 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_6859, BUNSPEC);
								}
								BgL_lnumz00_2067 = BgL_tmpz00_4024;
							}
							{	/* Llib/error.scm 783 */
								obj_t BgL_tmpz00_4025;

								{	/* Llib/error.scm 783 */
									int BgL_tmpz00_6862;

									BgL_tmpz00_6862 = (int) (2L);
									BgL_tmpz00_4025 = BGL_MVALUES_VAL(BgL_tmpz00_6862);
								}
								{	/* Llib/error.scm 783 */
									int BgL_tmpz00_6865;

									BgL_tmpz00_6865 = (int) (2L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_6865, BUNSPEC);
								}
								BgL_lpointz00_2068 = BgL_tmpz00_4025;
							}
							{	/* Llib/error.scm 783 */
								obj_t BgL_tmpz00_4026;

								{	/* Llib/error.scm 783 */
									int BgL_tmpz00_6868;

									BgL_tmpz00_6868 = (int) (3L);
									BgL_tmpz00_4026 = BGL_MVALUES_VAL(BgL_tmpz00_6868);
								}
								{	/* Llib/error.scm 783 */
									int BgL_tmpz00_6871;

									BgL_tmpz00_6871 = (int) (3L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_6871, BUNSPEC);
								}
								BgL_lstringz00_2069 = BgL_tmpz00_4026;
							}
							if (CBOOL(BgL_lnumz00_2067))
								{	/* Llib/error.scm 783 */
									return
										BGl_notifyzd2z62errorzf2locationzd2locz90zz__errorz00
										(BgL_errz00_85, BgL_fnamez00_86, BgL_lnumz00_2067,
										BgL_locz00_87, BgL_lstringz00_2069, BgL_lpointz00_2068);
								}
							else
								{	/* Llib/error.scm 783 */
									return
										BGl_notifyzd2z62errorzf2locationzd2nozd2locz42zz__errorz00
										(BgL_errz00_85);
								}
						}
					}
			}
		}

	}



/* open-for-error */
	obj_t BGl_openzd2forzd2errorz00zz__errorz00(obj_t BgL_fnamez00_88)
	{
		{	/* Llib/error.scm 790 */
			if (fexists(BSTRING_TO_STRING(BgL_fnamez00_88)))
				{	/* Llib/error.scm 792 */
					if (bgl_directoryp(BSTRING_TO_STRING(BgL_fnamez00_88)))
						{	/* Llib/error.scm 793 */
							return BFALSE;
						}
					else
						{	/* Llib/error.scm 794 */
							obj_t BgL_env1107z00_2079;

							BgL_env1107z00_2079 = BGL_CURRENT_DYNAMIC_ENV();
							{	/* Llib/error.scm 794 */
								obj_t BgL_cell1102z00_2080;

								BgL_cell1102z00_2080 = MAKE_STACK_CELL(BUNSPEC);
								{	/* Llib/error.scm 794 */
									obj_t BgL_val1106z00_2081;

									BgL_val1106z00_2081 =
										BGl_zc3z04exitza31686ze3ze70z60zz__errorz00(BgL_fnamez00_88,
										BgL_cell1102z00_2080, BgL_env1107z00_2079);
									if ((BgL_val1106z00_2081 == BgL_cell1102z00_2080))
										{	/* Llib/error.scm 794 */
											{	/* Llib/error.scm 794 */
												int BgL_tmpz00_6889;

												BgL_tmpz00_6889 = (int) (0L);
												BGL_SIGSETMASK(BgL_tmpz00_6889);
											}
											{	/* Llib/error.scm 794 */
												obj_t BgL_arg1685z00_2083;

												BgL_arg1685z00_2083 =
													CELL_REF(((obj_t) BgL_val1106z00_2081));
												return BFALSE;
											}
										}
									else
										{	/* Llib/error.scm 794 */
											return BgL_val1106z00_2081;
										}
								}
							}
						}
				}
			else
				{	/* Llib/error.scm 797 */
					bool_t BgL_test3248z00_6894;

					{	/* Llib/error.scm 797 */
						long BgL_l1z00_4037;

						BgL_l1z00_4037 = STRING_LENGTH(((obj_t) BgL_fnamez00_88));
						if ((BgL_l1z00_4037 == 5L))
							{	/* Llib/error.scm 797 */
								int BgL_arg2321z00_4040;

								{	/* Llib/error.scm 797 */
									char *BgL_auxz00_6902;
									char *BgL_tmpz00_6899;

									BgL_auxz00_6902 =
										BSTRING_TO_STRING(BGl_string2857z00zz__errorz00);
									BgL_tmpz00_6899 =
										BSTRING_TO_STRING(((obj_t) BgL_fnamez00_88));
									BgL_arg2321z00_4040 =
										memcmp(BgL_tmpz00_6899, BgL_auxz00_6902, BgL_l1z00_4037);
								}
								BgL_test3248z00_6894 = ((long) (BgL_arg2321z00_4040) == 0L);
							}
						else
							{	/* Llib/error.scm 797 */
								BgL_test3248z00_6894 = ((bool_t) 0);
							}
					}
					if (BgL_test3248z00_6894)
						{	/* Llib/error.scm 798 */
							obj_t BgL_arg1691z00_2101;

							{	/* Llib/error.scm 798 */
								obj_t BgL_arg1692z00_2105;

								{	/* Llib/error.scm 798 */
									obj_t BgL_tmpz00_6907;

									BgL_tmpz00_6907 = BGL_CURRENT_DYNAMIC_ENV();
									BgL_arg1692z00_2105 =
										BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_6907);
								}
								BgL_arg1691z00_2101 =
									BGL_INPUT_PORT_BUFFER(BgL_arg1692z00_2105);
							}
							{	/* Ieee/port.scm 469 */
								long BgL_endz00_2104;

								BgL_endz00_2104 = STRING_LENGTH(BgL_arg1691z00_2101);
								{	/* Ieee/port.scm 469 */

									return
										BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00
										(BgL_arg1691z00_2101, BINT(0L), BINT(BgL_endz00_2104));
								}
							}
						}
					else
						{	/* Llib/error.scm 799 */
							bool_t BgL_test3250z00_6915;

							{	/* Llib/error.scm 799 */

								BgL_test3250z00_6915 =
									BGl_stringzd2prefixzf3z21zz__r4_strings_6_7z00
									(BGl_string2858z00zz__errorz00, BgL_fnamez00_88, BFALSE,
									BFALSE, BFALSE, BFALSE);
							}
							if (BgL_test3250z00_6915)
								{	/* Llib/error.scm 800 */
									obj_t BgL_arg1699z00_2113;

									{	/* Ieee/string.scm 194 */
										long BgL_endz00_2119;

										BgL_endz00_2119 = STRING_LENGTH(((obj_t) BgL_fnamez00_88));
										{	/* Ieee/string.scm 194 */

											BgL_arg1699z00_2113 =
												BGl_substringz00zz__r4_strings_6_7z00(BgL_fnamez00_88,
												9L, BgL_endz00_2119);
									}}
									{	/* Ieee/port.scm 469 */
										long BgL_endz00_2116;

										BgL_endz00_2116 = STRING_LENGTH(BgL_arg1699z00_2113);
										{	/* Ieee/port.scm 469 */

											return
												BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00
												(BgL_arg1699z00_2113, BINT(0L), BINT(BgL_endz00_2116));
										}
									}
								}
							else
								{	/* Llib/error.scm 799 */
									return BFALSE;
								}
						}
				}
		}

	}



/* <@exit:1686>~0 */
	obj_t BGl_zc3z04exitza31686ze3ze70z60zz__errorz00(obj_t BgL_fnamez00_5407,
		obj_t BgL_cell1102z00_5406, obj_t BgL_env1107z00_5405)
	{
		{	/* Llib/error.scm 794 */
			jmp_buf_t jmpbuf;
			void *BgL_an_exit1111z00_2085;

			if (SET_EXIT(BgL_an_exit1111z00_2085))
				{
					return BGL_EXIT_VALUE();
				}
			else
				{
#if( SIGSETJMP_SAVESIGS == 0 )
				 // MS: CARE 5 jan 2021: see runtime/Clib/csystem.c
				 // bgl_restore_signal_handlers();
#endif

					BgL_an_exit1111z00_2085 = (void *) jmpbuf;
					PUSH_ENV_EXIT(BgL_env1107z00_5405, BgL_an_exit1111z00_2085, 1L);
					{	/* Llib/error.scm 794 */
						obj_t BgL_escape1103z00_2086;

						BgL_escape1103z00_2086 =
							BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1107z00_5405);
						{	/* Llib/error.scm 794 */
							obj_t BgL_res1114z00_2087;

							{	/* Llib/error.scm 794 */
								obj_t BgL_ohs1099z00_2088;

								BgL_ohs1099z00_2088 =
									BGL_ENV_ERROR_HANDLER_GET(BgL_env1107z00_5405);
								{	/* Llib/error.scm 794 */
									obj_t BgL_hds1100z00_2089;

									BgL_hds1100z00_2089 =
										MAKE_STACK_PAIR(BgL_escape1103z00_2086,
										BgL_cell1102z00_5406);
									BGL_ENV_ERROR_HANDLER_SET(BgL_env1107z00_5405,
										BgL_hds1100z00_2089);
									BUNSPEC;
									{	/* Llib/error.scm 794 */
										obj_t BgL_exitd1108z00_2090;

										BgL_exitd1108z00_2090 =
											BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1107z00_5405);
										{	/* Llib/error.scm 794 */
											obj_t BgL_tmp1110z00_2091;

											{	/* Llib/error.scm 794 */
												obj_t BgL_arg1688z00_2096;

												BgL_arg1688z00_2096 =
													BGL_EXITD_PROTECT(BgL_exitd1108z00_2090);
												BgL_tmp1110z00_2091 =
													MAKE_YOUNG_PAIR(BgL_ohs1099z00_2088,
													BgL_arg1688z00_2096);
											}
											{	/* Llib/error.scm 794 */

												BGL_EXITD_PROTECT_SET(BgL_exitd1108z00_2090,
													BgL_tmp1110z00_2091);
												BUNSPEC;
												{	/* Llib/error.scm 796 */
													obj_t BgL_tmp1109z00_2092;

													{	/* Ieee/port.scm 466 */

														BgL_tmp1109z00_2092 =
															BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00
															(BgL_fnamez00_5407, BTRUE, BINT(5000000L));
													}
													{	/* Llib/error.scm 794 */
														bool_t BgL_test3251z00_6937;

														{	/* Llib/error.scm 794 */
															obj_t BgL_arg2609z00_4030;

															BgL_arg2609z00_4030 =
																BGL_EXITD_PROTECT(BgL_exitd1108z00_2090);
															BgL_test3251z00_6937 = PAIRP(BgL_arg2609z00_4030);
														}
														if (BgL_test3251z00_6937)
															{	/* Llib/error.scm 794 */
																obj_t BgL_arg2607z00_4031;

																{	/* Llib/error.scm 794 */
																	obj_t BgL_arg2608z00_4032;

																	BgL_arg2608z00_4032 =
																		BGL_EXITD_PROTECT(BgL_exitd1108z00_2090);
																	BgL_arg2607z00_4031 =
																		CDR(((obj_t) BgL_arg2608z00_4032));
																}
																BGL_EXITD_PROTECT_SET(BgL_exitd1108z00_2090,
																	BgL_arg2607z00_4031);
																BUNSPEC;
															}
														else
															{	/* Llib/error.scm 794 */
																BFALSE;
															}
													}
													BGL_ENV_ERROR_HANDLER_SET(BgL_env1107z00_5405,
														BgL_ohs1099z00_2088);
													BUNSPEC;
													BgL_res1114z00_2087 = BgL_tmp1109z00_2092;
												}
											}
										}
									}
								}
							}
							POP_ENV_EXIT(BgL_env1107z00_5405);
							return BgL_res1114z00_2087;
						}
					}
				}
		}

	}



/* filename-for-error */
	obj_t BGl_filenamezd2forzd2errorz00zz__errorz00(obj_t BgL_filez00_89,
		long BgL_sza7za7_90)
	{
		{	/* Llib/error.scm 807 */
			if ((BgL_sza7za7_90 < 0L))
				{	/* Llib/error.scm 809 */
					return BGl_string2855z00zz__errorz00;
				}
			else
				{	/* Llib/error.scm 809 */
					if (fexists(BSTRING_TO_STRING(BgL_filez00_89)))
						{	/* Llib/error.scm 811 */
							return BGl_relativezd2filezd2namez00zz__errorz00(BgL_filez00_89);
						}
					else
						{	/* Llib/error.scm 813 */
							bool_t BgL_test3254z00_6952;

							{	/* Llib/error.scm 813 */

								BgL_test3254z00_6952 =
									BGl_stringzd2prefixzf3z21zz__r4_strings_6_7z00
									(BGl_string2858z00zz__errorz00, BgL_filez00_89, BFALSE,
									BFALSE, BFALSE, BFALSE);
							}
							if (BgL_test3254z00_6952)
								{	/* Llib/error.scm 813 */
									if (
										(STRING_LENGTH(
												((obj_t) BgL_filez00_89)) <= (9L + BgL_sza7za7_90)))
										{	/* Ieee/string.scm 194 */
											long BgL_endz00_2145;

											BgL_endz00_2145 = STRING_LENGTH(((obj_t) BgL_filez00_89));
											{	/* Ieee/string.scm 194 */

												return
													BGl_substringz00zz__r4_strings_6_7z00(BgL_filez00_89,
													9L, BgL_endz00_2145);
											}
										}
									else
										{	/* Llib/error.scm 816 */
											obj_t BgL_arg1707z00_2146;

											BgL_arg1707z00_2146 =
												BGl_substringz00zz__r4_strings_6_7z00(BgL_filez00_89,
												9L, (BgL_sza7za7_90 + 6L));
											{	/* Llib/error.scm 816 */
												obj_t BgL_list1708z00_2147;

												{	/* Llib/error.scm 816 */
													obj_t BgL_arg1709z00_2148;

													BgL_arg1709z00_2148 =
														MAKE_YOUNG_PAIR(BGl_string2855z00zz__errorz00,
														BNIL);
													BgL_list1708z00_2147 =
														MAKE_YOUNG_PAIR(BgL_arg1707z00_2146,
														BgL_arg1709z00_2148);
												}
												return
													BGl_stringzd2appendzd2zz__r4_strings_6_7z00
													(BgL_list1708z00_2147);
											}
										}
								}
							else
								{	/* Llib/error.scm 813 */
									if (
										(STRING_LENGTH(((obj_t) BgL_filez00_89)) <= BgL_sza7za7_90))
										{	/* Llib/error.scm 817 */
											return BgL_filez00_89;
										}
									else
										{	/* Llib/error.scm 817 */
											if ((BgL_sza7za7_90 < 4L))
												{	/* Llib/error.scm 819 */
													return BGl_string2855z00zz__errorz00;
												}
											else
												{	/* Llib/error.scm 822 */
													obj_t BgL_arg1718z00_2155;

													BgL_arg1718z00_2155 =
														BGl_substringz00zz__r4_strings_6_7z00
														(BgL_filez00_89, 0L, (BgL_sza7za7_90 - 3L));
													{	/* Llib/error.scm 822 */
														obj_t BgL_list1719z00_2156;

														{	/* Llib/error.scm 822 */
															obj_t BgL_arg1720z00_2157;

															BgL_arg1720z00_2157 =
																MAKE_YOUNG_PAIR(BGl_string2855z00zz__errorz00,
																BNIL);
															BgL_list1719z00_2156 =
																MAKE_YOUNG_PAIR(BgL_arg1718z00_2155,
																BgL_arg1720z00_2157);
														}
														return
															BGl_stringzd2appendzd2zz__r4_strings_6_7z00
															(BgL_list1719z00_2156);
													}
												}
										}
								}
						}
				}
		}

	}



/* location-line-num */
	obj_t BGl_locationzd2linezd2numz00zz__errorz00(obj_t BgL_locz00_93)
	{
		{	/* Llib/error.scm 827 */
			{

				if (PAIRP(BgL_locz00_93))
					{	/* Llib/error.scm 881 */
						obj_t BgL_cdrzd2226zd2_2182;

						BgL_cdrzd2226zd2_2182 = CDR(((obj_t) BgL_locz00_93));
						if ((CAR(((obj_t) BgL_locz00_93)) == BGl_symbol2815z00zz__errorz00))
							{	/* Llib/error.scm 881 */
								if (PAIRP(BgL_cdrzd2226zd2_2182))
									{	/* Llib/error.scm 881 */
										obj_t BgL_cdrzd2230zd2_2186;

										BgL_cdrzd2230zd2_2186 = CDR(BgL_cdrzd2226zd2_2182);
										if (PAIRP(BgL_cdrzd2230zd2_2186))
											{	/* Llib/error.scm 881 */
												if (NULLP(CDR(BgL_cdrzd2230zd2_2186)))
													{	/* Llib/error.scm 881 */
														return
															BGl_locationzd2atze70z35zz__errorz00(CAR
															(BgL_cdrzd2226zd2_2182),
															CAR(BgL_cdrzd2230zd2_2186));
													}
												else
													{	/* Llib/error.scm 881 */
													BgL_tagzd2217zd2_2179:
														{	/* Llib/error.scm 892 */
															obj_t BgL_list1759z00_2221;

															{	/* Llib/error.scm 892 */
																obj_t BgL_arg1760z00_2222;

																{	/* Llib/error.scm 892 */
																	obj_t BgL_arg1761z00_2223;

																	{	/* Llib/error.scm 892 */
																		obj_t BgL_arg1762z00_2224;

																		BgL_arg1762z00_2224 =
																			MAKE_YOUNG_PAIR(BFALSE, BNIL);
																		BgL_arg1761z00_2223 =
																			MAKE_YOUNG_PAIR(BFALSE,
																			BgL_arg1762z00_2224);
																	}
																	BgL_arg1760z00_2222 =
																		MAKE_YOUNG_PAIR(BFALSE,
																		BgL_arg1761z00_2223);
																}
																BgL_list1759z00_2221 =
																	MAKE_YOUNG_PAIR(BFALSE, BgL_arg1760z00_2222);
															}
															BGL_TAIL return
																BGl_valuesz00zz__r5_control_features_6_4z00
																(BgL_list1759z00_2221);
														}
													}
											}
										else
											{	/* Llib/error.scm 881 */
												goto BgL_tagzd2217zd2_2179;
											}
									}
								else
									{	/* Llib/error.scm 881 */
										goto BgL_tagzd2217zd2_2179;
									}
							}
						else
							{	/* Llib/error.scm 881 */
								if (
									(CAR(
											((obj_t) BgL_locz00_93)) ==
										BGl_symbol2859z00zz__errorz00))
									{	/* Llib/error.scm 881 */
										if (PAIRP(BgL_cdrzd2226zd2_2182))
											{	/* Llib/error.scm 881 */
												obj_t BgL_cdrzd2282zd2_2197;

												BgL_cdrzd2282zd2_2197 = CDR(BgL_cdrzd2226zd2_2182);
												if (PAIRP(BgL_cdrzd2282zd2_2197))
													{	/* Llib/error.scm 881 */
														obj_t BgL_cdrzd2287zd2_2199;

														BgL_cdrzd2287zd2_2199 = CDR(BgL_cdrzd2282zd2_2197);
														if (PAIRP(BgL_cdrzd2287zd2_2199))
															{	/* Llib/error.scm 881 */
																if (NULLP(CDR(BgL_cdrzd2287zd2_2199)))
																	{	/* Llib/error.scm 881 */
																		return
																			BGl_locationzd2linezd2colze70ze7zz__errorz00
																			(CAR(BgL_cdrzd2226zd2_2182),
																			CAR(BgL_cdrzd2282zd2_2197),
																			CAR(BgL_cdrzd2287zd2_2199));
																	}
																else
																	{	/* Llib/error.scm 881 */
																		goto BgL_tagzd2217zd2_2179;
																	}
															}
														else
															{	/* Llib/error.scm 881 */
																goto BgL_tagzd2217zd2_2179;
															}
													}
												else
													{	/* Llib/error.scm 881 */
														goto BgL_tagzd2217zd2_2179;
													}
											}
										else
											{	/* Llib/error.scm 881 */
												goto BgL_tagzd2217zd2_2179;
											}
									}
								else
									{	/* Llib/error.scm 881 */
										obj_t BgL_cdrzd2314zd2_2207;

										BgL_cdrzd2314zd2_2207 = CDR(((obj_t) BgL_locz00_93));
										if (
											(CAR(
													((obj_t) BgL_locz00_93)) ==
												BGl_symbol2861z00zz__errorz00))
											{	/* Llib/error.scm 881 */
												if (PAIRP(BgL_cdrzd2314zd2_2207))
													{	/* Llib/error.scm 881 */
														obj_t BgL_cdrzd2318zd2_2211;

														BgL_cdrzd2318zd2_2211 = CDR(BgL_cdrzd2314zd2_2207);
														if (PAIRP(BgL_cdrzd2318zd2_2211))
															{	/* Llib/error.scm 881 */
																if (NULLP(CDR(BgL_cdrzd2318zd2_2211)))
																	{	/* Llib/error.scm 881 */
																		return
																			BGl_locationzd2linezd2colze70ze7zz__errorz00
																			(CAR(BgL_cdrzd2314zd2_2207),
																			CAR(BgL_cdrzd2318zd2_2211), BINT(0L));
																	}
																else
																	{	/* Llib/error.scm 881 */
																		goto BgL_tagzd2217zd2_2179;
																	}
															}
														else
															{	/* Llib/error.scm 881 */
																goto BgL_tagzd2217zd2_2179;
															}
													}
												else
													{	/* Llib/error.scm 881 */
														goto BgL_tagzd2217zd2_2179;
													}
											}
										else
											{	/* Llib/error.scm 881 */
												goto BgL_tagzd2217zd2_2179;
											}
									}
							}
					}
				else
					{	/* Llib/error.scm 881 */
						goto BgL_tagzd2217zd2_2179;
					}
			}
		}

	}



/* location-at~0 */
	obj_t BGl_locationzd2atze70z35zz__errorz00(obj_t BgL_filez00_2233,
		obj_t BgL_pointz00_2234)
	{
		{	/* Llib/error.scm 857 */
			{	/* Llib/error.scm 836 */
				bool_t BgL_test3272z00_7039;

				if (STRINGP(BgL_filez00_2233))
					{	/* Llib/error.scm 836 */
						BgL_test3272z00_7039 =
							BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BgL_pointz00_2234);
					}
				else
					{	/* Llib/error.scm 836 */
						BgL_test3272z00_7039 = ((bool_t) 0);
					}
				if (BgL_test3272z00_7039)
					{	/* Llib/error.scm 837 */
						obj_t BgL_fnamez00_2238;

						{	/* Llib/error.scm 837 */
							bool_t BgL_test3274z00_7043;

							{	/* Llib/error.scm 837 */
								obj_t BgL_string1z00_4072;

								BgL_string1z00_4072 = string_to_bstring(OS_CLASS);
								{	/* Llib/error.scm 837 */
									long BgL_l1z00_4074;

									BgL_l1z00_4074 = STRING_LENGTH(BgL_string1z00_4072);
									if ((BgL_l1z00_4074 == 5L))
										{	/* Llib/error.scm 837 */
											int BgL_arg2321z00_4077;

											{	/* Llib/error.scm 837 */
												char *BgL_auxz00_7050;
												char *BgL_tmpz00_7048;

												BgL_auxz00_7050 =
													BSTRING_TO_STRING(BGl_string2863z00zz__errorz00);
												BgL_tmpz00_7048 =
													BSTRING_TO_STRING(BgL_string1z00_4072);
												BgL_arg2321z00_4077 =
													memcmp(BgL_tmpz00_7048, BgL_auxz00_7050,
													BgL_l1z00_4074);
											}
											BgL_test3274z00_7043 =
												((long) (BgL_arg2321z00_4077) == 0L);
										}
									else
										{	/* Llib/error.scm 837 */
											BgL_test3274z00_7043 = ((bool_t) 0);
										}
								}
							}
							if (BgL_test3274z00_7043)
								{	/* Llib/error.scm 837 */
									BgL_fnamez00_2238 =
										BGl_stringzd2replacezd2zz__r4_strings_6_7z00
										(BGl_uncygdrivez00zz__errorz00(BgL_filez00_2233),
										(char) (((unsigned char) '/')),
										(char) (((unsigned char) '\\')));
								}
							else
								{	/* Llib/error.scm 837 */
									BgL_fnamez00_2238 = BgL_filez00_2233;
								}
						}
						{	/* Llib/error.scm 837 */
							obj_t BgL_portz00_2239;

							BgL_portz00_2239 =
								BGl_openzd2forzd2errorz00zz__errorz00(BgL_fnamez00_2238);
							{	/* Llib/error.scm 840 */

								if (INPUT_PORTP(BgL_portz00_2239))
									{	/* Llib/error.scm 842 */
										obj_t BgL_exitd1117z00_2241;

										BgL_exitd1117z00_2241 = BGL_EXITD_TOP_AS_OBJ();
										{	/* Llib/error.scm 856 */
											obj_t BgL_zc3z04anonymousza31799ze3z87_5339;

											BgL_zc3z04anonymousza31799ze3z87_5339 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31799ze3ze5zz__errorz00,
												(int) (0L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31799ze3z87_5339,
												(int) (0L), BgL_portz00_2239);
											{	/* Llib/error.scm 842 */
												obj_t BgL_arg2610z00_4084;

												{	/* Llib/error.scm 842 */
													obj_t BgL_arg2611z00_4085;

													BgL_arg2611z00_4085 =
														BGL_EXITD_PROTECT(BgL_exitd1117z00_2241);
													BgL_arg2610z00_4084 =
														MAKE_YOUNG_PAIR
														(BgL_zc3z04anonymousza31799ze3z87_5339,
														BgL_arg2611z00_4085);
												}
												BGL_EXITD_PROTECT_SET(BgL_exitd1117z00_2241,
													BgL_arg2610z00_4084);
												BUNSPEC;
											}
											{	/* Llib/error.scm 846 */
												obj_t BgL_tmp1119z00_2243;

												{
													obj_t BgL_ostringz00_2245;
													long BgL_lnumz00_2246;
													long BgL_oposz00_2247;

													BgL_ostringz00_2245 = BFALSE;
													BgL_lnumz00_2246 = 1L;
													BgL_oposz00_2247 = 0L;
												BgL_zc3z04anonymousza31773ze3z87_2248:
													{	/* Llib/error.scm 846 */
														obj_t BgL_lstringz00_2249;

														BgL_lstringz00_2249 =
															BGl_readzd2linezd2zz__r4_input_6_10_2z00
															(BgL_portz00_2239);
														if (EOF_OBJECTP(BgL_lstringz00_2249))
															{	/* Llib/error.scm 848 */
																obj_t BgL_arg1775z00_2251;
																long BgL_arg1777z00_2252;
																obj_t BgL_arg1779z00_2253;

																{	/* Llib/error.scm 830 */
																	obj_t BgL_relz00_4086;

																	BgL_relz00_4086 =
																		BGl_relativezd2filezd2namez00zz__errorz00
																		(BgL_fnamez00_2238);
																	if ((STRING_LENGTH(((obj_t) BgL_relz00_4086))
																			<
																			STRING_LENGTH(((obj_t)
																					BgL_fnamez00_2238))))
																		{	/* Llib/error.scm 831 */
																			BgL_arg1775z00_2251 = BgL_relz00_4086;
																		}
																	else
																		{	/* Llib/error.scm 831 */
																			BgL_arg1775z00_2251 = BgL_fnamez00_2238;
																		}
																}
																BgL_arg1777z00_2252 =
																	(1L +
																	((long) CINT(BgL_pointz00_2234) -
																		BgL_oposz00_2247));
																if (STRINGP(BgL_ostringz00_2245))
																	{	/* Llib/error.scm 850 */
																		obj_t BgL_list1787z00_2260;

																		{	/* Llib/error.scm 850 */
																			obj_t BgL_arg1788z00_2261;

																			BgL_arg1788z00_2261 =
																				MAKE_YOUNG_PAIR
																				(BGl_string2864z00zz__errorz00, BNIL);
																			BgL_list1787z00_2260 =
																				MAKE_YOUNG_PAIR(BgL_ostringz00_2245,
																				BgL_arg1788z00_2261);
																		}
																		BgL_arg1779z00_2253 =
																			BGl_stringzd2appendzd2zz__r4_strings_6_7z00
																			(BgL_list1787z00_2260);
																	}
																else
																	{	/* Llib/error.scm 849 */
																		BgL_arg1779z00_2253 =
																			BGl_string2864z00zz__errorz00;
																	}
																{	/* Llib/error.scm 848 */
																	obj_t BgL_list1780z00_2254;

																	{	/* Llib/error.scm 848 */
																		obj_t BgL_arg1781z00_2255;

																		{	/* Llib/error.scm 848 */
																			obj_t BgL_arg1782z00_2256;

																			{	/* Llib/error.scm 848 */
																				obj_t BgL_arg1783z00_2257;

																				BgL_arg1783z00_2257 =
																					MAKE_YOUNG_PAIR(BgL_arg1779z00_2253,
																					BNIL);
																				BgL_arg1782z00_2256 =
																					MAKE_YOUNG_PAIR(BINT
																					(BgL_arg1777z00_2252),
																					BgL_arg1783z00_2257);
																			}
																			BgL_arg1781z00_2255 =
																				MAKE_YOUNG_PAIR(BINT(BgL_lnumz00_2246),
																				BgL_arg1782z00_2256);
																		}
																		BgL_list1780z00_2254 =
																			MAKE_YOUNG_PAIR(BgL_arg1775z00_2251,
																			BgL_arg1781z00_2255);
																	}
																	BgL_tmp1119z00_2243 =
																		BGl_valuesz00zz__r5_control_features_6_4z00
																		(BgL_list1780z00_2254);
																}
															}
														else
															{	/* Llib/error.scm 852 */
																bool_t BgL_test3280z00_7096;

																{	/* Llib/error.scm 852 */
																	long BgL_arg1798z00_2272;

																	{	/* Llib/error.scm 852 */
																		obj_t BgL_tmpz00_7097;

																		BgL_tmpz00_7097 =
																			((obj_t) BgL_portz00_2239);
																		BgL_arg1798z00_2272 =
																			INPUT_PORT_FILEPOS(BgL_tmpz00_7097);
																	}
																	BgL_test3280z00_7096 =
																		(BgL_arg1798z00_2272 >
																		(long) CINT(BgL_pointz00_2234));
																}
																if (BgL_test3280z00_7096)
																	{	/* Llib/error.scm 853 */
																		obj_t BgL_arg1791z00_2264;
																		long BgL_arg1792z00_2265;

																		{	/* Llib/error.scm 830 */
																			obj_t BgL_relz00_4100;

																			BgL_relz00_4100 =
																				BGl_relativezd2filezd2namez00zz__errorz00
																				(BgL_filez00_2233);
																			if ((STRING_LENGTH(((obj_t)
																							BgL_relz00_4100)) <
																					STRING_LENGTH(((obj_t)
																							BgL_filez00_2233))))
																				{	/* Llib/error.scm 831 */
																					BgL_arg1791z00_2264 = BgL_relz00_4100;
																				}
																			else
																				{	/* Llib/error.scm 831 */
																					BgL_arg1791z00_2264 =
																						BgL_filez00_2233;
																				}
																		}
																		BgL_arg1792z00_2265 =
																			(
																			(long) CINT(BgL_pointz00_2234) -
																			BgL_oposz00_2247);
																		{	/* Llib/error.scm 853 */
																			obj_t BgL_list1793z00_2266;

																			{	/* Llib/error.scm 853 */
																				obj_t BgL_arg1794z00_2267;

																				{	/* Llib/error.scm 853 */
																					obj_t BgL_arg1795z00_2268;

																					{	/* Llib/error.scm 853 */
																						obj_t BgL_arg1796z00_2269;

																						BgL_arg1796z00_2269 =
																							MAKE_YOUNG_PAIR
																							(BgL_lstringz00_2249, BNIL);
																						BgL_arg1795z00_2268 =
																							MAKE_YOUNG_PAIR(BINT
																							(BgL_arg1792z00_2265),
																							BgL_arg1796z00_2269);
																					}
																					BgL_arg1794z00_2267 =
																						MAKE_YOUNG_PAIR(BINT
																						(BgL_lnumz00_2246),
																						BgL_arg1795z00_2268);
																				}
																				BgL_list1793z00_2266 =
																					MAKE_YOUNG_PAIR(BgL_arg1791z00_2264,
																					BgL_arg1794z00_2267);
																			}
																			BgL_tmp1119z00_2243 =
																				BGl_valuesz00zz__r5_control_features_6_4z00
																				(BgL_list1793z00_2266);
																	}}
																else
																	{	/* Llib/error.scm 854 */
																		long BgL_oposz00_2270;

																		{	/* Llib/error.scm 854 */
																			obj_t BgL_tmpz00_7118;

																			BgL_tmpz00_7118 =
																				((obj_t) BgL_portz00_2239);
																			BgL_oposz00_2270 =
																				INPUT_PORT_FILEPOS(BgL_tmpz00_7118);
																		}
																		{
																			long BgL_oposz00_7124;
																			long BgL_lnumz00_7122;
																			obj_t BgL_ostringz00_7121;

																			BgL_ostringz00_7121 = BgL_lstringz00_2249;
																			BgL_lnumz00_7122 =
																				(BgL_lnumz00_2246 + 1L);
																			BgL_oposz00_7124 = BgL_oposz00_2270;
																			BgL_oposz00_2247 = BgL_oposz00_7124;
																			BgL_lnumz00_2246 = BgL_lnumz00_7122;
																			BgL_ostringz00_2245 = BgL_ostringz00_7121;
																			goto
																				BgL_zc3z04anonymousza31773ze3z87_2248;
																		}
																	}
															}
													}
												}
												{	/* Llib/error.scm 842 */
													bool_t BgL_test3282z00_7125;

													{	/* Llib/error.scm 842 */
														obj_t BgL_arg2609z00_4113;

														BgL_arg2609z00_4113 =
															BGL_EXITD_PROTECT(BgL_exitd1117z00_2241);
														BgL_test3282z00_7125 = PAIRP(BgL_arg2609z00_4113);
													}
													if (BgL_test3282z00_7125)
														{	/* Llib/error.scm 842 */
															obj_t BgL_arg2607z00_4114;

															{	/* Llib/error.scm 842 */
																obj_t BgL_arg2608z00_4115;

																BgL_arg2608z00_4115 =
																	BGL_EXITD_PROTECT(BgL_exitd1117z00_2241);
																BgL_arg2607z00_4114 =
																	CDR(((obj_t) BgL_arg2608z00_4115));
															}
															BGL_EXITD_PROTECT_SET(BgL_exitd1117z00_2241,
																BgL_arg2607z00_4114);
															BUNSPEC;
														}
													else
														{	/* Llib/error.scm 842 */
															BFALSE;
														}
												}
												bgl_close_input_port(BgL_portz00_2239);
												return BgL_tmp1119z00_2243;
											}
										}
									}
								else
									{	/* Llib/error.scm 857 */
										obj_t BgL_arg1800z00_2276;

										{	/* Llib/error.scm 830 */
											obj_t BgL_relz00_4118;

											BgL_relz00_4118 =
												BGl_relativezd2filezd2namez00zz__errorz00
												(BgL_filez00_2233);
											if ((STRING_LENGTH(((obj_t) BgL_relz00_4118)) <
													STRING_LENGTH(BgL_filez00_2233)))
												{	/* Llib/error.scm 831 */
													BgL_arg1800z00_2276 = BgL_relz00_4118;
												}
											else
												{	/* Llib/error.scm 831 */
													BgL_arg1800z00_2276 = BgL_filez00_2233;
												}
										}
										{	/* Llib/error.scm 857 */
											obj_t BgL_list1801z00_2277;

											{	/* Llib/error.scm 857 */
												obj_t BgL_arg1802z00_2278;

												{	/* Llib/error.scm 857 */
													obj_t BgL_arg1803z00_2279;

													{	/* Llib/error.scm 857 */
														obj_t BgL_arg1804z00_2280;

														BgL_arg1804z00_2280 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
														BgL_arg1803z00_2279 =
															MAKE_YOUNG_PAIR(BgL_pointz00_2234,
															BgL_arg1804z00_2280);
													}
													BgL_arg1802z00_2278 =
														MAKE_YOUNG_PAIR(BFALSE, BgL_arg1803z00_2279);
												}
												BgL_list1801z00_2277 =
													MAKE_YOUNG_PAIR(BgL_arg1800z00_2276,
													BgL_arg1802z00_2278);
											}
											return
												BGl_valuesz00zz__r5_control_features_6_4z00
												(BgL_list1801z00_2277);
										}
									}
							}
						}
					}
				else
					{	/* Llib/error.scm 836 */
						return BFALSE;
					}
			}
		}

	}



/* location-line-col~0 */
	obj_t BGl_locationzd2linezd2colze70ze7zz__errorz00(obj_t BgL_filez00_2286,
		obj_t BgL_linez00_2287, obj_t BgL_colz00_2288)
	{
		{	/* Llib/error.scm 879 */
			{	/* Llib/error.scm 860 */
				bool_t BgL_test3284z00_7144;

				if (((long) CINT(BgL_linez00_2287) >= 0L))
					{	/* Llib/error.scm 860 */
						BgL_test3284z00_7144 = ((long) CINT(BgL_colz00_2288) >= 0L);
					}
				else
					{	/* Llib/error.scm 860 */
						BgL_test3284z00_7144 = ((bool_t) 0);
					}
				if (BgL_test3284z00_7144)
					{	/* Llib/error.scm 861 */
						obj_t BgL_fnamez00_2292;

						{	/* Llib/error.scm 861 */
							bool_t BgL_test3286z00_7150;

							{	/* Llib/error.scm 861 */
								obj_t BgL_string1z00_4128;

								BgL_string1z00_4128 = string_to_bstring(OS_CLASS);
								{	/* Llib/error.scm 861 */
									long BgL_l1z00_4130;

									BgL_l1z00_4130 = STRING_LENGTH(BgL_string1z00_4128);
									if ((BgL_l1z00_4130 == 5L))
										{	/* Llib/error.scm 861 */
											int BgL_arg2321z00_4133;

											{	/* Llib/error.scm 861 */
												char *BgL_auxz00_7157;
												char *BgL_tmpz00_7155;

												BgL_auxz00_7157 =
													BSTRING_TO_STRING(BGl_string2863z00zz__errorz00);
												BgL_tmpz00_7155 =
													BSTRING_TO_STRING(BgL_string1z00_4128);
												BgL_arg2321z00_4133 =
													memcmp(BgL_tmpz00_7155, BgL_auxz00_7157,
													BgL_l1z00_4130);
											}
											BgL_test3286z00_7150 =
												((long) (BgL_arg2321z00_4133) == 0L);
										}
									else
										{	/* Llib/error.scm 861 */
											BgL_test3286z00_7150 = ((bool_t) 0);
										}
								}
							}
							if (BgL_test3286z00_7150)
								{	/* Llib/error.scm 861 */
									BgL_fnamez00_2292 =
										BGl_stringzd2replacezd2zz__r4_strings_6_7z00
										(BGl_uncygdrivez00zz__errorz00(BgL_filez00_2286),
										(char) (((unsigned char) '/')),
										(char) (((unsigned char) '\\')));
								}
							else
								{	/* Llib/error.scm 861 */
									BgL_fnamez00_2292 = BgL_filez00_2286;
								}
						}
						{	/* Llib/error.scm 861 */
							obj_t BgL_portz00_2293;

							BgL_portz00_2293 =
								BGl_openzd2forzd2errorz00zz__errorz00(BgL_fnamez00_2292);
							{	/* Llib/error.scm 864 */

								if (INPUT_PORTP(BgL_portz00_2293))
									{	/* Llib/error.scm 866 */
										obj_t BgL_exitd1121z00_2295;

										BgL_exitd1121z00_2295 = BGL_EXITD_TOP_AS_OBJ();
										{	/* Llib/error.scm 877 */
											obj_t BgL_zc3z04anonymousza31835ze3z87_5338;

											BgL_zc3z04anonymousza31835ze3z87_5338 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31835ze3ze5zz__errorz00,
												(int) (0L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31835ze3z87_5338,
												(int) (0L), BgL_portz00_2293);
											{	/* Llib/error.scm 866 */
												obj_t BgL_arg2610z00_4140;

												{	/* Llib/error.scm 866 */
													obj_t BgL_arg2611z00_4141;

													BgL_arg2611z00_4141 =
														BGL_EXITD_PROTECT(BgL_exitd1121z00_2295);
													BgL_arg2610z00_4140 =
														MAKE_YOUNG_PAIR
														(BgL_zc3z04anonymousza31835ze3z87_5338,
														BgL_arg2611z00_4141);
												}
												BGL_EXITD_PROTECT_SET(BgL_exitd1121z00_2295,
													BgL_arg2610z00_4140);
												BUNSPEC;
											}
											{	/* Llib/error.scm 869 */
												obj_t BgL_tmp1123z00_2297;

												{
													obj_t BgL_ostringz00_2299;
													obj_t BgL_lnumz00_2300;

													BgL_ostringz00_2299 = BFALSE;
													BgL_lnumz00_2300 = BgL_linez00_2287;
												BgL_zc3z04anonymousza31813ze3z87_2301:
													{	/* Llib/error.scm 869 */
														obj_t BgL_lstringz00_2302;

														BgL_lstringz00_2302 =
															BGl_readzd2linezd2zz__r4_input_6_10_2z00
															(BgL_portz00_2293);
														if (EOF_OBJECTP(BgL_lstringz00_2302))
															{	/* Llib/error.scm 871 */
																obj_t BgL_arg1815z00_2304;
																long BgL_arg1816z00_2305;
																obj_t BgL_arg1817z00_2306;

																{	/* Llib/error.scm 830 */
																	obj_t BgL_relz00_4142;

																	BgL_relz00_4142 =
																		BGl_relativezd2filezd2namez00zz__errorz00
																		(BgL_filez00_2286);
																	if ((STRING_LENGTH(((obj_t) BgL_relz00_4142))
																			<
																			STRING_LENGTH(((obj_t)
																					BgL_filez00_2286))))
																		{	/* Llib/error.scm 831 */
																			BgL_arg1815z00_2304 = BgL_relz00_4142;
																		}
																	else
																		{	/* Llib/error.scm 831 */
																			BgL_arg1815z00_2304 = BgL_filez00_2286;
																		}
																}
																BgL_arg1816z00_2305 =
																	((long) CINT(BgL_colz00_2288) + 1L);
																{	/* Llib/error.scm 872 */
																	obj_t BgL_list1823z00_2311;

																	{	/* Llib/error.scm 872 */
																		obj_t BgL_arg1826z00_2312;

																		BgL_arg1826z00_2312 =
																			MAKE_YOUNG_PAIR
																			(BGl_string2864z00zz__errorz00, BNIL);
																		BgL_list1823z00_2311 =
																			MAKE_YOUNG_PAIR(BgL_ostringz00_2299,
																			BgL_arg1826z00_2312);
																	}
																	BgL_arg1817z00_2306 =
																		BGl_stringzd2appendzd2zz__r4_strings_6_7z00
																		(BgL_list1823z00_2311);
																}
																{	/* Llib/error.scm 871 */
																	obj_t BgL_list1818z00_2307;

																	{	/* Llib/error.scm 871 */
																		obj_t BgL_arg1819z00_2308;

																		{	/* Llib/error.scm 871 */
																			obj_t BgL_arg1820z00_2309;

																			{	/* Llib/error.scm 871 */
																				obj_t BgL_arg1822z00_2310;

																				BgL_arg1822z00_2310 =
																					MAKE_YOUNG_PAIR(BgL_arg1817z00_2306,
																					BNIL);
																				BgL_arg1820z00_2309 =
																					MAKE_YOUNG_PAIR(BINT
																					(BgL_arg1816z00_2305),
																					BgL_arg1822z00_2310);
																			}
																			BgL_arg1819z00_2308 =
																				MAKE_YOUNG_PAIR(BgL_linez00_2287,
																				BgL_arg1820z00_2309);
																		}
																		BgL_list1818z00_2307 =
																			MAKE_YOUNG_PAIR(BgL_arg1815z00_2304,
																			BgL_arg1819z00_2308);
																	}
																	BgL_tmp1123z00_2297 =
																		BGl_valuesz00zz__r5_control_features_6_4z00
																		(BgL_list1818z00_2307);
															}}
														else
															{	/* Llib/error.scm 870 */
																if (((long) CINT(BgL_lnumz00_2300) == 0L))
																	{	/* Llib/error.scm 874 */
																		obj_t BgL_arg1828z00_2314;

																		{	/* Llib/error.scm 830 */
																			obj_t BgL_relz00_4152;

																			BgL_relz00_4152 =
																				BGl_relativezd2filezd2namez00zz__errorz00
																				(BgL_filez00_2286);
																			if ((STRING_LENGTH(((obj_t)
																							BgL_relz00_4152)) <
																					STRING_LENGTH(((obj_t)
																							BgL_filez00_2286))))
																				{	/* Llib/error.scm 831 */
																					BgL_arg1828z00_2314 = BgL_relz00_4152;
																				}
																			else
																				{	/* Llib/error.scm 831 */
																					BgL_arg1828z00_2314 =
																						BgL_filez00_2286;
																				}
																		}
																		{	/* Llib/error.scm 874 */
																			obj_t BgL_list1829z00_2315;

																			{	/* Llib/error.scm 874 */
																				obj_t BgL_arg1831z00_2316;

																				{	/* Llib/error.scm 874 */
																					obj_t BgL_arg1832z00_2317;

																					{	/* Llib/error.scm 874 */
																						obj_t BgL_arg1833z00_2318;

																						BgL_arg1833z00_2318 =
																							MAKE_YOUNG_PAIR
																							(BgL_lstringz00_2302, BNIL);
																						BgL_arg1832z00_2317 =
																							MAKE_YOUNG_PAIR(BgL_colz00_2288,
																							BgL_arg1833z00_2318);
																					}
																					BgL_arg1831z00_2316 =
																						MAKE_YOUNG_PAIR(BgL_linez00_2287,
																						BgL_arg1832z00_2317);
																				}
																				BgL_list1829z00_2315 =
																					MAKE_YOUNG_PAIR(BgL_arg1828z00_2314,
																					BgL_arg1831z00_2316);
																			}
																			BgL_tmp1123z00_2297 =
																				BGl_valuesz00zz__r5_control_features_6_4z00
																				(BgL_list1829z00_2315);
																		}
																	}
																else
																	{	/* Llib/error.scm 875 */
																		long BgL_oposz00_2319;

																		{	/* Llib/error.scm 875 */
																			obj_t BgL_tmpz00_7214;

																			BgL_tmpz00_7214 =
																				((obj_t) BgL_portz00_2293);
																			BgL_oposz00_2319 =
																				INPUT_PORT_FILEPOS(BgL_tmpz00_7214);
																		}
																		{	/* Llib/error.scm 876 */
																			long BgL_arg1834z00_2320;

																			BgL_arg1834z00_2320 =
																				((long) CINT(BgL_lnumz00_2300) - 1L);
																			{
																				obj_t BgL_lnumz00_7220;
																				obj_t BgL_ostringz00_7219;

																				BgL_ostringz00_7219 =
																					BgL_lstringz00_2302;
																				BgL_lnumz00_7220 =
																					BINT(BgL_arg1834z00_2320);
																				BgL_lnumz00_2300 = BgL_lnumz00_7220;
																				BgL_ostringz00_2299 =
																					BgL_ostringz00_7219;
																				goto
																					BgL_zc3z04anonymousza31813ze3z87_2301;
																			}
																		}
																	}
															}
													}
												}
												{	/* Llib/error.scm 866 */
													bool_t BgL_test3293z00_7222;

													{	/* Llib/error.scm 866 */
														obj_t BgL_arg2609z00_4163;

														BgL_arg2609z00_4163 =
															BGL_EXITD_PROTECT(BgL_exitd1121z00_2295);
														BgL_test3293z00_7222 = PAIRP(BgL_arg2609z00_4163);
													}
													if (BgL_test3293z00_7222)
														{	/* Llib/error.scm 866 */
															obj_t BgL_arg2607z00_4164;

															{	/* Llib/error.scm 866 */
																obj_t BgL_arg2608z00_4165;

																BgL_arg2608z00_4165 =
																	BGL_EXITD_PROTECT(BgL_exitd1121z00_2295);
																BgL_arg2607z00_4164 =
																	CDR(((obj_t) BgL_arg2608z00_4165));
															}
															BGL_EXITD_PROTECT_SET(BgL_exitd1121z00_2295,
																BgL_arg2607z00_4164);
															BUNSPEC;
														}
													else
														{	/* Llib/error.scm 866 */
															BFALSE;
														}
												}
												bgl_close_input_port(BgL_portz00_2293);
												return BgL_tmp1123z00_2297;
											}
										}
									}
								else
									{	/* Llib/error.scm 878 */
										obj_t BgL_arg1836z00_2324;

										{	/* Llib/error.scm 830 */
											obj_t BgL_relz00_4168;

											BgL_relz00_4168 =
												BGl_relativezd2filezd2namez00zz__errorz00
												(BgL_filez00_2286);
											if ((STRING_LENGTH(((obj_t) BgL_relz00_4168)) <
													STRING_LENGTH(((obj_t) BgL_filez00_2286))))
												{	/* Llib/error.scm 831 */
													BgL_arg1836z00_2324 = BgL_relz00_4168;
												}
											else
												{	/* Llib/error.scm 831 */
													BgL_arg1836z00_2324 = BgL_filez00_2286;
												}
										}
										{	/* Llib/error.scm 878 */
											obj_t BgL_list1837z00_2325;

											{	/* Llib/error.scm 878 */
												obj_t BgL_arg1838z00_2326;

												{	/* Llib/error.scm 878 */
													obj_t BgL_arg1839z00_2327;

													{	/* Llib/error.scm 878 */
														obj_t BgL_arg1840z00_2328;

														BgL_arg1840z00_2328 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
														BgL_arg1839z00_2327 =
															MAKE_YOUNG_PAIR(BgL_colz00_2288,
															BgL_arg1840z00_2328);
													}
													BgL_arg1838z00_2326 =
														MAKE_YOUNG_PAIR(BgL_linez00_2287,
														BgL_arg1839z00_2327);
												}
												BgL_list1837z00_2325 =
													MAKE_YOUNG_PAIR(BgL_arg1836z00_2324,
													BgL_arg1838z00_2326);
											}
											return
												BGl_valuesz00zz__r5_control_features_6_4z00
												(BgL_list1837z00_2325);
										}
									}
							}
						}
					}
				else
					{	/* Llib/error.scm 879 */
						obj_t BgL_arg1846z00_2333;

						{	/* Llib/error.scm 830 */
							obj_t BgL_relz00_4176;

							BgL_relz00_4176 =
								BGl_relativezd2filezd2namez00zz__errorz00(BgL_filez00_2286);
							if (
								(STRING_LENGTH(
										((obj_t) BgL_relz00_4176)) <
									STRING_LENGTH(((obj_t) BgL_filez00_2286))))
								{	/* Llib/error.scm 831 */
									BgL_arg1846z00_2333 = BgL_relz00_4176;
								}
							else
								{	/* Llib/error.scm 831 */
									BgL_arg1846z00_2333 = BgL_filez00_2286;
								}
						}
						{	/* Llib/error.scm 879 */
							obj_t BgL_list1847z00_2334;

							{	/* Llib/error.scm 879 */
								obj_t BgL_arg1848z00_2335;

								{	/* Llib/error.scm 879 */
									obj_t BgL_arg1849z00_2336;

									{	/* Llib/error.scm 879 */
										obj_t BgL_arg1850z00_2337;

										BgL_arg1850z00_2337 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
										BgL_arg1849z00_2336 =
											MAKE_YOUNG_PAIR(BgL_colz00_2288, BgL_arg1850z00_2337);
									}
									BgL_arg1848z00_2335 =
										MAKE_YOUNG_PAIR(BgL_linez00_2287, BgL_arg1849z00_2336);
								}
								BgL_list1847z00_2334 =
									MAKE_YOUNG_PAIR(BgL_arg1846z00_2333, BgL_arg1848z00_2335);
							}
							return
								BGl_valuesz00zz__r5_control_features_6_4z00
								(BgL_list1847z00_2334);
						}
					}
			}
		}

	}



/* &<@anonymous:1835> */
	obj_t BGl_z62zc3z04anonymousza31835ze3ze5zz__errorz00(obj_t BgL_envz00_5340)
	{
		{	/* Llib/error.scm 866 */
			{	/* Llib/error.scm 877 */
				obj_t BgL_portz00_5341;

				BgL_portz00_5341 = PROCEDURE_REF(BgL_envz00_5340, (int) (0L));
				return bgl_close_input_port(((obj_t) BgL_portz00_5341));
			}
		}

	}



/* &<@anonymous:1799> */
	obj_t BGl_z62zc3z04anonymousza31799ze3ze5zz__errorz00(obj_t BgL_envz00_5342)
	{
		{	/* Llib/error.scm 842 */
			{	/* Llib/error.scm 856 */
				obj_t BgL_portz00_5343;

				BgL_portz00_5343 = PROCEDURE_REF(BgL_envz00_5342, (int) (0L));
				return bgl_close_input_port(((obj_t) BgL_portz00_5343));
			}
		}

	}



/* error-notify */
	BGL_EXPORTED_DEF obj_t BGl_errorzd2notifyzd2zz__errorz00(obj_t BgL_ez00_95)
	{
		{	/* Llib/error.scm 904 */
			{	/* Llib/error.scm 906 */
				bool_t BgL_test3296z00_7262;

				{	/* Llib/error.scm 906 */
					obj_t BgL_classz00_4211;

					BgL_classz00_4211 = BGl_z62errorz62zz__objectz00;
					if (BGL_OBJECTP(BgL_ez00_95))
						{	/* Llib/error.scm 906 */
							BgL_objectz00_bglt BgL_arg2721z00_4213;

							BgL_arg2721z00_4213 = (BgL_objectz00_bglt) (BgL_ez00_95);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Llib/error.scm 906 */
									long BgL_idxz00_4219;

									BgL_idxz00_4219 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg2721z00_4213);
									BgL_test3296z00_7262 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_4219 + 3L)) == BgL_classz00_4211);
								}
							else
								{	/* Llib/error.scm 906 */
									bool_t BgL_res2742z00_4244;

									{	/* Llib/error.scm 906 */
										obj_t BgL_oclassz00_4227;

										{	/* Llib/error.scm 906 */
											obj_t BgL_arg2728z00_4235;
											long BgL_arg2729z00_4236;

											BgL_arg2728z00_4235 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Llib/error.scm 906 */
												long BgL_arg2731z00_4237;

												BgL_arg2731z00_4237 =
													BGL_OBJECT_CLASS_NUM(BgL_arg2721z00_4213);
												BgL_arg2729z00_4236 =
													(BgL_arg2731z00_4237 - OBJECT_TYPE);
											}
											BgL_oclassz00_4227 =
												VECTOR_REF(BgL_arg2728z00_4235, BgL_arg2729z00_4236);
										}
										{	/* Llib/error.scm 906 */
											bool_t BgL__ortest_1198z00_4228;

											BgL__ortest_1198z00_4228 =
												(BgL_classz00_4211 == BgL_oclassz00_4227);
											if (BgL__ortest_1198z00_4228)
												{	/* Llib/error.scm 906 */
													BgL_res2742z00_4244 = BgL__ortest_1198z00_4228;
												}
											else
												{	/* Llib/error.scm 906 */
													long BgL_odepthz00_4229;

													{	/* Llib/error.scm 906 */
														obj_t BgL_arg2717z00_4230;

														BgL_arg2717z00_4230 = (BgL_oclassz00_4227);
														BgL_odepthz00_4229 =
															BGL_CLASS_DEPTH(BgL_arg2717z00_4230);
													}
													if ((3L < BgL_odepthz00_4229))
														{	/* Llib/error.scm 906 */
															obj_t BgL_arg2715z00_4232;

															{	/* Llib/error.scm 906 */
																obj_t BgL_arg2716z00_4233;

																BgL_arg2716z00_4233 = (BgL_oclassz00_4227);
																BgL_arg2715z00_4232 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg2716z00_4233,
																	3L);
															}
															BgL_res2742z00_4244 =
																(BgL_arg2715z00_4232 == BgL_classz00_4211);
														}
													else
														{	/* Llib/error.scm 906 */
															BgL_res2742z00_4244 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test3296z00_7262 = BgL_res2742z00_4244;
								}
						}
					else
						{	/* Llib/error.scm 906 */
							BgL_test3296z00_7262 = ((bool_t) 0);
						}
				}
				if (BgL_test3296z00_7262)
					{	/* Llib/error.scm 907 */
						bool_t BgL_test3301z00_7285;

						{	/* Llib/error.scm 899 */
							bool_t BgL_test3302z00_7286;

							{	/* Llib/error.scm 899 */
								obj_t BgL_tmpz00_7287;

								BgL_tmpz00_7287 =
									(((BgL_z62exceptionz62_bglt) COBJECT(
											((BgL_z62exceptionz62_bglt) BgL_ez00_95)))->BgL_fnamez00);
								BgL_test3302z00_7286 = STRINGP(BgL_tmpz00_7287);
							}
							if (BgL_test3302z00_7286)
								{	/* Llib/error.scm 899 */
									BgL_test3301z00_7285 =
										BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(
										(((BgL_z62exceptionz62_bglt) COBJECT(
													((BgL_z62exceptionz62_bglt) BgL_ez00_95)))->
											BgL_locationz00));
								}
							else
								{	/* Llib/error.scm 899 */
									BgL_test3301z00_7285 = ((bool_t) 0);
								}
						}
						if (BgL_test3301z00_7285)
							{	/* Llib/error.scm 909 */
								obj_t BgL_arg1856z00_2349;
								obj_t BgL_arg1857z00_2350;

								BgL_arg1856z00_2349 =
									(((BgL_z62exceptionz62_bglt) COBJECT(
											((BgL_z62exceptionz62_bglt) BgL_ez00_95)))->BgL_fnamez00);
								BgL_arg1857z00_2350 =
									(((BgL_z62exceptionz62_bglt) COBJECT(
											((BgL_z62exceptionz62_bglt) BgL_ez00_95)))->
									BgL_locationz00);
								return BGl_notifyzd2z62errorzf2locz42zz__errorz00(BgL_ez00_95,
									BgL_arg1856z00_2349, BgL_arg1857z00_2350);
							}
						else
							{	/* Llib/error.scm 907 */
								BGL_TAIL return
									BGl_notifyzd2z62errorzb0zz__errorz00(BgL_ez00_95);
							}
					}
				else
					{	/* Llib/error.scm 911 */
						bool_t BgL_test3303z00_7300;

						{	/* Llib/error.scm 911 */
							obj_t BgL_classz00_4249;

							BgL_classz00_4249 = BGl_z62conditionz62zz__objectz00;
							if (BGL_OBJECTP(BgL_ez00_95))
								{	/* Llib/error.scm 911 */
									BgL_objectz00_bglt BgL_arg2721z00_4251;

									BgL_arg2721z00_4251 = (BgL_objectz00_bglt) (BgL_ez00_95);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Llib/error.scm 911 */
											long BgL_idxz00_4257;

											BgL_idxz00_4257 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg2721z00_4251);
											BgL_test3303z00_7300 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_4257 + 1L)) == BgL_classz00_4249);
										}
									else
										{	/* Llib/error.scm 911 */
											bool_t BgL_res2743z00_4282;

											{	/* Llib/error.scm 911 */
												obj_t BgL_oclassz00_4265;

												{	/* Llib/error.scm 911 */
													obj_t BgL_arg2728z00_4273;
													long BgL_arg2729z00_4274;

													BgL_arg2728z00_4273 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Llib/error.scm 911 */
														long BgL_arg2731z00_4275;

														BgL_arg2731z00_4275 =
															BGL_OBJECT_CLASS_NUM(BgL_arg2721z00_4251);
														BgL_arg2729z00_4274 =
															(BgL_arg2731z00_4275 - OBJECT_TYPE);
													}
													BgL_oclassz00_4265 =
														VECTOR_REF(BgL_arg2728z00_4273,
														BgL_arg2729z00_4274);
												}
												{	/* Llib/error.scm 911 */
													bool_t BgL__ortest_1198z00_4266;

													BgL__ortest_1198z00_4266 =
														(BgL_classz00_4249 == BgL_oclassz00_4265);
													if (BgL__ortest_1198z00_4266)
														{	/* Llib/error.scm 911 */
															BgL_res2743z00_4282 = BgL__ortest_1198z00_4266;
														}
													else
														{	/* Llib/error.scm 911 */
															long BgL_odepthz00_4267;

															{	/* Llib/error.scm 911 */
																obj_t BgL_arg2717z00_4268;

																BgL_arg2717z00_4268 = (BgL_oclassz00_4265);
																BgL_odepthz00_4267 =
																	BGL_CLASS_DEPTH(BgL_arg2717z00_4268);
															}
															if ((1L < BgL_odepthz00_4267))
																{	/* Llib/error.scm 911 */
																	obj_t BgL_arg2715z00_4270;

																	{	/* Llib/error.scm 911 */
																		obj_t BgL_arg2716z00_4271;

																		BgL_arg2716z00_4271 = (BgL_oclassz00_4265);
																		BgL_arg2715z00_4270 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg2716z00_4271, 1L);
																	}
																	BgL_res2743z00_4282 =
																		(BgL_arg2715z00_4270 == BgL_classz00_4249);
																}
															else
																{	/* Llib/error.scm 911 */
																	BgL_res2743z00_4282 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test3303z00_7300 = BgL_res2743z00_4282;
										}
								}
							else
								{	/* Llib/error.scm 911 */
									BgL_test3303z00_7300 = ((bool_t) 0);
								}
						}
						if (BgL_test3303z00_7300)
							{	/* Llib/error.scm 912 */
								obj_t BgL_arg1859z00_2352;

								{	/* Llib/error.scm 912 */
									obj_t BgL_tmpz00_7323;

									BgL_tmpz00_7323 = BGL_CURRENT_DYNAMIC_ENV();
									BgL_arg1859z00_2352 =
										BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_7323);
								}
								{	/* Llib/error.scm 912 */
									obj_t BgL_list1860z00_2353;

									{	/* Llib/error.scm 912 */
										obj_t BgL_arg1862z00_2354;

										BgL_arg1862z00_2354 = MAKE_YOUNG_PAIR(BgL_ez00_95, BNIL);
										BgL_list1860z00_2353 =
											MAKE_YOUNG_PAIR(BGl_string2865z00zz__errorz00,
											BgL_arg1862z00_2354);
									}
									return
										BGl_fprintz00zz__r4_output_6_10_3z00(BgL_arg1859z00_2352,
										BgL_list1860z00_2353);
								}
							}
						else
							{	/* Llib/error.scm 911 */
								return BFALSE;
							}
					}
			}
		}

	}



/* &error-notify */
	obj_t BGl_z62errorzd2notifyzb0zz__errorz00(obj_t BgL_envz00_5344,
		obj_t BgL_ez00_5345)
	{
		{	/* Llib/error.scm 904 */
			return BGl_errorzd2notifyzd2zz__errorz00(BgL_ez00_5345);
		}

	}



/* error-notify/location */
	BGL_EXPORTED_DEF obj_t BGl_errorzd2notifyzf2locationz20zz__errorz00(obj_t
		BgL_ez00_96, obj_t BgL_fnamez00_97, int BgL_locationz00_98)
	{
		{	/* Llib/error.scm 917 */
			{	/* Llib/error.scm 918 */
				bool_t BgL_test3308z00_7330;

				{	/* Llib/error.scm 918 */
					obj_t BgL_classz00_4285;

					BgL_classz00_4285 = BGl_z62errorz62zz__objectz00;
					if (BGL_OBJECTP(BgL_ez00_96))
						{	/* Llib/error.scm 918 */
							BgL_objectz00_bglt BgL_arg2721z00_4287;

							BgL_arg2721z00_4287 = (BgL_objectz00_bglt) (BgL_ez00_96);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Llib/error.scm 918 */
									long BgL_idxz00_4293;

									BgL_idxz00_4293 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg2721z00_4287);
									BgL_test3308z00_7330 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_4293 + 3L)) == BgL_classz00_4285);
								}
							else
								{	/* Llib/error.scm 918 */
									bool_t BgL_res2744z00_4318;

									{	/* Llib/error.scm 918 */
										obj_t BgL_oclassz00_4301;

										{	/* Llib/error.scm 918 */
											obj_t BgL_arg2728z00_4309;
											long BgL_arg2729z00_4310;

											BgL_arg2728z00_4309 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Llib/error.scm 918 */
												long BgL_arg2731z00_4311;

												BgL_arg2731z00_4311 =
													BGL_OBJECT_CLASS_NUM(BgL_arg2721z00_4287);
												BgL_arg2729z00_4310 =
													(BgL_arg2731z00_4311 - OBJECT_TYPE);
											}
											BgL_oclassz00_4301 =
												VECTOR_REF(BgL_arg2728z00_4309, BgL_arg2729z00_4310);
										}
										{	/* Llib/error.scm 918 */
											bool_t BgL__ortest_1198z00_4302;

											BgL__ortest_1198z00_4302 =
												(BgL_classz00_4285 == BgL_oclassz00_4301);
											if (BgL__ortest_1198z00_4302)
												{	/* Llib/error.scm 918 */
													BgL_res2744z00_4318 = BgL__ortest_1198z00_4302;
												}
											else
												{	/* Llib/error.scm 918 */
													long BgL_odepthz00_4303;

													{	/* Llib/error.scm 918 */
														obj_t BgL_arg2717z00_4304;

														BgL_arg2717z00_4304 = (BgL_oclassz00_4301);
														BgL_odepthz00_4303 =
															BGL_CLASS_DEPTH(BgL_arg2717z00_4304);
													}
													if ((3L < BgL_odepthz00_4303))
														{	/* Llib/error.scm 918 */
															obj_t BgL_arg2715z00_4306;

															{	/* Llib/error.scm 918 */
																obj_t BgL_arg2716z00_4307;

																BgL_arg2716z00_4307 = (BgL_oclassz00_4301);
																BgL_arg2715z00_4306 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg2716z00_4307,
																	3L);
															}
															BgL_res2744z00_4318 =
																(BgL_arg2715z00_4306 == BgL_classz00_4285);
														}
													else
														{	/* Llib/error.scm 918 */
															BgL_res2744z00_4318 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test3308z00_7330 = BgL_res2744z00_4318;
								}
						}
					else
						{	/* Llib/error.scm 918 */
							BgL_test3308z00_7330 = ((bool_t) 0);
						}
				}
				if (BgL_test3308z00_7330)
					{	/* Llib/error.scm 918 */
						return
							BGl_notifyzd2z62errorzf2locz42zz__errorz00(BgL_ez00_96,
							BgL_fnamez00_97, BINT(BgL_locationz00_98));
					}
				else
					{	/* Llib/error.scm 918 */
						return BFALSE;
					}
			}
		}

	}



/* &error-notify/location */
	obj_t BGl_z62errorzd2notifyzf2locationz42zz__errorz00(obj_t BgL_envz00_5346,
		obj_t BgL_ez00_5347, obj_t BgL_fnamez00_5348, obj_t BgL_locationz00_5349)
	{
		{	/* Llib/error.scm 917 */
			{	/* Llib/error.scm 918 */
				int BgL_auxz00_7362;
				obj_t BgL_auxz00_7355;

				{	/* Llib/error.scm 918 */
					obj_t BgL_tmpz00_7363;

					if (INTEGERP(BgL_locationz00_5349))
						{	/* Llib/error.scm 918 */
							BgL_tmpz00_7363 = BgL_locationz00_5349;
						}
					else
						{
							obj_t BgL_auxz00_7366;

							BgL_auxz00_7366 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2811z00zz__errorz00,
								BINT(36967L), BGl_string2866z00zz__errorz00,
								BGl_string2813z00zz__errorz00, BgL_locationz00_5349);
							FAILURE(BgL_auxz00_7366, BFALSE, BFALSE);
						}
					BgL_auxz00_7362 = CINT(BgL_tmpz00_7363);
				}
				if (STRINGP(BgL_fnamez00_5348))
					{	/* Llib/error.scm 918 */
						BgL_auxz00_7355 = BgL_fnamez00_5348;
					}
				else
					{
						obj_t BgL_auxz00_7358;

						BgL_auxz00_7358 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2811z00zz__errorz00,
							BINT(36967L), BGl_string2866z00zz__errorz00,
							BGl_string2838z00zz__errorz00, BgL_fnamez00_5348);
						FAILURE(BgL_auxz00_7358, BFALSE, BFALSE);
					}
				return
					BGl_errorzd2notifyzf2locationz20zz__errorz00(BgL_ez00_5347,
					BgL_auxz00_7355, BgL_auxz00_7362);
			}
		}

	}



/* warning-notify */
	BGL_EXPORTED_DEF obj_t BGl_warningzd2notifyzd2zz__errorz00(obj_t BgL_ez00_99)
	{
		{	/* Llib/error.scm 924 */
			{
				obj_t BgL_ez00_2378;

				{	/* Llib/error.scm 938 */
					bool_t BgL_test3315z00_7372;

					{	/* Llib/error.scm 938 */
						int BgL_arg1880z00_2377;

						BgL_arg1880z00_2377 = BGl_bigloozd2warningzd2zz__paramz00();
						BgL_test3315z00_7372 = ((long) (BgL_arg1880z00_2377) > 0L);
					}
					if (BgL_test3315z00_7372)
						{	/* Llib/error.scm 938 */
							{	/* Llib/error.scm 939 */
								bool_t BgL_test3316z00_7376;

								{	/* Llib/error.scm 899 */
									bool_t BgL_test3317z00_7377;

									{	/* Llib/error.scm 899 */
										obj_t BgL_tmpz00_7378;

										BgL_tmpz00_7378 =
											(((BgL_z62exceptionz62_bglt) COBJECT(
													((BgL_z62exceptionz62_bglt) BgL_ez00_99)))->
											BgL_fnamez00);
										BgL_test3317z00_7377 = STRINGP(BgL_tmpz00_7378);
									}
									if (BgL_test3317z00_7377)
										{	/* Llib/error.scm 899 */
											BgL_test3316z00_7376 =
												BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(
												(((BgL_z62exceptionz62_bglt) COBJECT(
															((BgL_z62exceptionz62_bglt) BgL_ez00_99)))->
													BgL_locationz00));
										}
									else
										{	/* Llib/error.scm 899 */
											BgL_test3316z00_7376 = ((bool_t) 0);
										}
								}
								if (BgL_test3316z00_7376)
									{	/* Llib/error.scm 942 */
										bool_t BgL_test3318z00_7385;

										{	/* Llib/error.scm 942 */
											obj_t BgL_arg1876z00_2369;

											BgL_arg1876z00_2369 =
												(((BgL_z62exceptionz62_bglt) COBJECT(
														((BgL_z62exceptionz62_bglt)
															((BgL_z62warningz62_bglt) BgL_ez00_99))))->
												BgL_fnamez00);
											{	/* Llib/error.scm 942 */
												long BgL_l1z00_4338;

												BgL_l1z00_4338 =
													STRING_LENGTH(((obj_t) BgL_arg1876z00_2369));
												if ((BgL_l1z00_4338 == 8L))
													{	/* Llib/error.scm 942 */
														int BgL_arg2321z00_4341;

														{	/* Llib/error.scm 942 */
															char *BgL_auxz00_7396;
															char *BgL_tmpz00_7393;

															BgL_auxz00_7396 =
																BSTRING_TO_STRING
																(BGl_string2869z00zz__errorz00);
															BgL_tmpz00_7393 =
																BSTRING_TO_STRING(((obj_t)
																	BgL_arg1876z00_2369));
															BgL_arg2321z00_4341 =
																memcmp(BgL_tmpz00_7393, BgL_auxz00_7396,
																BgL_l1z00_4338);
														}
														BgL_test3318z00_7385 =
															((long) (BgL_arg2321z00_4341) == 0L);
													}
												else
													{	/* Llib/error.scm 942 */
														BgL_test3318z00_7385 = ((bool_t) 0);
													}
											}
										}
										if (BgL_test3318z00_7385)
											{	/* Llib/error.scm 942 */
												BgL_ez00_2378 = BgL_ez00_99;
											BgL_zc3z04anonymousza31881ze3z87_2379:
												{	/* Llib/error.scm 926 */
													obj_t BgL_arg1882z00_2380;

													{	/* Llib/error.scm 926 */
														obj_t BgL_tmpz00_7401;

														BgL_tmpz00_7401 = BGL_CURRENT_DYNAMIC_ENV();
														BgL_arg1882z00_2380 =
															BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_7401);
													}
													bgl_flush_output_port(BgL_arg1882z00_2380);
												}
												{	/* Llib/error.scm 927 */
													obj_t BgL_arg1883z00_2381;

													{	/* Llib/error.scm 927 */
														obj_t BgL_tmpz00_7405;

														BgL_tmpz00_7405 = BGL_CURRENT_DYNAMIC_ENV();
														BgL_arg1883z00_2381 =
															BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_7405);
													}
													{	/* Llib/error.scm 927 */
														obj_t BgL_list1884z00_2382;

														BgL_list1884z00_2382 =
															MAKE_YOUNG_PAIR(BgL_arg1883z00_2381, BNIL);
														BGl_displayz00zz__r4_output_6_10_3z00
															(BGl_string2867z00zz__errorz00,
															BgL_list1884z00_2382);
													}
												}
												if (NULLP(
														(((BgL_z62warningz62_bglt) COBJECT(
																	((BgL_z62warningz62_bglt) BgL_ez00_2378)))->
															BgL_argsz00)))
													{	/* Llib/error.scm 929 */
														BFALSE;
													}
												else
													{	/* Llib/error.scm 929 */
														{	/* Llib/error.scm 931 */
															obj_t BgL_arg1887z00_2386;
															obj_t BgL_arg1888z00_2387;

															{	/* Llib/error.scm 931 */
																obj_t BgL_pairz00_4322;

																BgL_pairz00_4322 =
																	(((BgL_z62warningz62_bglt) COBJECT(
																			((BgL_z62warningz62_bglt)
																				BgL_ez00_2378)))->BgL_argsz00);
																BgL_arg1887z00_2386 = CAR(BgL_pairz00_4322);
															}
															{	/* Llib/error.scm 931 */
																obj_t BgL_tmpz00_7417;

																BgL_tmpz00_7417 = BGL_CURRENT_DYNAMIC_ENV();
																BgL_arg1888z00_2387 =
																	BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_7417);
															}
															BGl_displayzd2circlezd2zz__pp_circlez00
																(BgL_arg1887z00_2386, BgL_arg1888z00_2387);
														}
														{	/* Llib/error.scm 932 */
															obj_t BgL_arg1890z00_2389;

															{	/* Llib/error.scm 932 */
																obj_t BgL_tmpz00_7421;

																BgL_tmpz00_7421 = BGL_CURRENT_DYNAMIC_ENV();
																BgL_arg1890z00_2389 =
																	BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_7421);
															}
															{	/* Llib/error.scm 932 */
																obj_t BgL_list1891z00_2390;

																BgL_list1891z00_2390 =
																	MAKE_YOUNG_PAIR(BgL_arg1890z00_2389, BNIL);
																BGl_newlinez00zz__r4_output_6_10_3z00
																	(BgL_list1891z00_2390);
															}
														}
														{	/* Llib/error.scm 933 */
															obj_t BgL_arg1893z00_2392;

															{	/* Llib/error.scm 935 */
																obj_t BgL_pairz00_4325;

																BgL_pairz00_4325 =
																	(((BgL_z62warningz62_bglt) COBJECT(
																			((BgL_z62warningz62_bglt)
																				BgL_ez00_2378)))->BgL_argsz00);
																BgL_arg1893z00_2392 = CDR(BgL_pairz00_4325);
															}
															{	/* Llib/error.scm 933 */
																obj_t BgL_list1894z00_2393;

																BgL_list1894z00_2393 =
																	MAKE_YOUNG_PAIR(BgL_arg1893z00_2392, BNIL);
																BGl_forzd2eachzd2zz__r4_control_features_6_9z00
																	(BGl_proc2868z00zz__errorz00,
																	BgL_list1894z00_2393);
															}
														}
													}
												{	/* Llib/error.scm 936 */
													obj_t BgL_arg1899z00_2400;

													{	/* Llib/error.scm 936 */
														obj_t BgL_tmpz00_7431;

														BgL_tmpz00_7431 = BGL_CURRENT_DYNAMIC_ENV();
														BgL_arg1899z00_2400 =
															BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_7431);
													}
													{	/* Llib/error.scm 936 */
														obj_t BgL_list1900z00_2401;

														BgL_list1900z00_2401 =
															MAKE_YOUNG_PAIR(BgL_arg1899z00_2400, BNIL);
														BGl_newlinez00zz__r4_output_6_10_3z00
															(BgL_list1900z00_2401);
													}
												}
												{	/* Llib/error.scm 937 */
													obj_t BgL_arg1901z00_2402;

													{	/* Llib/error.scm 937 */
														obj_t BgL_tmpz00_7436;

														BgL_tmpz00_7436 = BGL_CURRENT_DYNAMIC_ENV();
														BgL_arg1901z00_2402 =
															BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_7436);
													}
													bgl_flush_output_port(BgL_arg1901z00_2402);
												}
											}
										else
											{	/* Llib/error.scm 944 */
												bool_t BgL_test3321z00_7440;

												{	/* Llib/error.scm 944 */
													obj_t BgL_arg1875z00_2368;

													BgL_arg1875z00_2368 =
														(((BgL_z62exceptionz62_bglt) COBJECT(
																((BgL_z62exceptionz62_bglt)
																	((BgL_z62warningz62_bglt) BgL_ez00_99))))->
														BgL_fnamez00);
													{	/* Llib/error.scm 944 */
														long BgL_l1z00_4349;

														BgL_l1z00_4349 =
															STRING_LENGTH(((obj_t) BgL_arg1875z00_2368));
														if ((BgL_l1z00_4349 == 7L))
															{	/* Llib/error.scm 944 */
																int BgL_arg2321z00_4352;

																{	/* Llib/error.scm 944 */
																	char *BgL_auxz00_7451;
																	char *BgL_tmpz00_7448;

																	BgL_auxz00_7451 =
																		BSTRING_TO_STRING
																		(BGl_string2870z00zz__errorz00);
																	BgL_tmpz00_7448 =
																		BSTRING_TO_STRING(((obj_t)
																			BgL_arg1875z00_2368));
																	BgL_arg2321z00_4352 =
																		memcmp(BgL_tmpz00_7448, BgL_auxz00_7451,
																		BgL_l1z00_4349);
																}
																BgL_test3321z00_7440 =
																	((long) (BgL_arg2321z00_4352) == 0L);
															}
														else
															{	/* Llib/error.scm 944 */
																BgL_test3321z00_7440 = ((bool_t) 0);
															}
													}
												}
												if (BgL_test3321z00_7440)
													{
														obj_t BgL_ez00_7456;

														BgL_ez00_7456 = BgL_ez00_99;
														BgL_ez00_2378 = BgL_ez00_7456;
														goto BgL_zc3z04anonymousza31881ze3z87_2379;
													}
												else
													{	/* Llib/error.scm 947 */
														obj_t BgL_arg1872z00_2365;
														obj_t BgL_arg1873z00_2366;
														obj_t BgL_arg1874z00_2367;

														BgL_arg1872z00_2365 =
															(((BgL_z62exceptionz62_bglt) COBJECT(
																	((BgL_z62exceptionz62_bglt)
																		((BgL_z62warningz62_bglt) BgL_ez00_99))))->
															BgL_fnamez00);
														BgL_arg1873z00_2366 =
															(((BgL_z62exceptionz62_bglt)
																COBJECT(((BgL_z62exceptionz62_bglt) (
																			(BgL_z62warningz62_bglt) BgL_ez00_99))))->
															BgL_locationz00);
														BgL_arg1874z00_2367 =
															(((BgL_z62warningz62_bglt)
																COBJECT(((BgL_z62warningz62_bglt)
																		BgL_ez00_99)))->BgL_argsz00);
														BGl_warningzf2locationzd2filez20zz__errorz00
															(BgL_arg1872z00_2365, BgL_arg1873z00_2366,
															BgL_arg1874z00_2367);
													}
											}
									}
								else
									{
										obj_t BgL_ez00_7466;

										BgL_ez00_7466 = BgL_ez00_99;
										BgL_ez00_2378 = BgL_ez00_7466;
										goto BgL_zc3z04anonymousza31881ze3z87_2379;
									}
							}
							if (CBOOL(
									(((BgL_z62exceptionz62_bglt) COBJECT(
												((BgL_z62exceptionz62_bglt)
													((BgL_z62warningz62_bglt) BgL_ez00_99))))->
										BgL_stackz00)))
								{	/* Llib/error.scm 952 */
									obj_t BgL_arg1878z00_2372;
									obj_t BgL_arg1879z00_2373;

									BgL_arg1878z00_2372 =
										(((BgL_z62exceptionz62_bglt) COBJECT(
												((BgL_z62exceptionz62_bglt)
													((BgL_z62warningz62_bglt) BgL_ez00_99))))->
										BgL_stackz00);
									{	/* Llib/error.scm 952 */
										obj_t BgL_tmpz00_7475;

										BgL_tmpz00_7475 = BGL_CURRENT_DYNAMIC_ENV();
										BgL_arg1879z00_2373 =
											BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_7475);
									}
									{	/* Llib/error.scm 273 */

										BGl_displayzd2tracezd2stackz00zz__errorz00
											(BgL_arg1878z00_2372, BgL_arg1879z00_2373, BINT(1L));
									}
								}
							else
								{	/* Llib/error.scm 951 */
									BFALSE;
								}
						}
					else
						{	/* Llib/error.scm 938 */
							BFALSE;
						}
				}
				return BFALSE;
			}
		}

	}



/* &warning-notify */
	obj_t BGl_z62warningzd2notifyzb0zz__errorz00(obj_t BgL_envz00_5351,
		obj_t BgL_ez00_5352)
	{
		{	/* Llib/error.scm 924 */
			return BGl_warningzd2notifyzd2zz__errorz00(BgL_ez00_5352);
		}

	}



/* &<@anonymous:1895> */
	obj_t BGl_z62zc3z04anonymousza31895ze3ze5zz__errorz00(obj_t BgL_envz00_5353,
		obj_t BgL_az00_5354)
	{
		{	/* Llib/error.scm 933 */
			{	/* Llib/error.scm 934 */
				obj_t BgL_arg1896z00_5461;

				{	/* Llib/error.scm 934 */
					obj_t BgL_tmpz00_7481;

					BgL_tmpz00_7481 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg1896z00_5461 = BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_7481);
				}
				return
					BGl_displayzd2circlezd2zz__pp_circlez00(BgL_az00_5354,
					BgL_arg1896z00_5461);
			}
		}

	}



/* warning-notify/location */
	BGL_EXPORTED_DEF obj_t BGl_warningzd2notifyzf2locationz20zz__errorz00(obj_t
		BgL_ez00_100, obj_t BgL_fnamez00_101, int BgL_locationz00_102)
	{
		{	/* Llib/error.scm 958 */
			{	/* Llib/error.scm 959 */
				bool_t BgL_test3324z00_7485;

				{	/* Llib/error.scm 959 */
					int BgL_arg1906z00_4360;

					BgL_arg1906z00_4360 = BGl_bigloozd2warningzd2zz__paramz00();
					BgL_test3324z00_7485 = ((long) (BgL_arg1906z00_4360) > 0L);
				}
				if (BgL_test3324z00_7485)
					{	/* Llib/error.scm 961 */
						obj_t BgL_arg1904z00_4362;

						BgL_arg1904z00_4362 =
							(((BgL_z62warningz62_bglt) COBJECT(
									((BgL_z62warningz62_bglt) BgL_ez00_100)))->BgL_argsz00);
						return
							BGl_warningzf2locationzd2filez20zz__errorz00(BgL_fnamez00_101,
							BINT(BgL_locationz00_102), BgL_arg1904z00_4362);
					}
				else
					{	/* Llib/error.scm 959 */
						return BFALSE;
					}
			}
		}

	}



/* &warning-notify/location */
	obj_t BGl_z62warningzd2notifyzf2locationz42zz__errorz00(obj_t BgL_envz00_5355,
		obj_t BgL_ez00_5356, obj_t BgL_fnamez00_5357, obj_t BgL_locationz00_5358)
	{
		{	/* Llib/error.scm 958 */
			{	/* Llib/error.scm 959 */
				int BgL_auxz00_7500;
				obj_t BgL_auxz00_7493;

				{	/* Llib/error.scm 959 */
					obj_t BgL_tmpz00_7501;

					if (INTEGERP(BgL_locationz00_5358))
						{	/* Llib/error.scm 959 */
							BgL_tmpz00_7501 = BgL_locationz00_5358;
						}
					else
						{
							obj_t BgL_auxz00_7504;

							BgL_auxz00_7504 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2811z00zz__errorz00,
								BINT(38480L), BGl_string2871z00zz__errorz00,
								BGl_string2813z00zz__errorz00, BgL_locationz00_5358);
							FAILURE(BgL_auxz00_7504, BFALSE, BFALSE);
						}
					BgL_auxz00_7500 = CINT(BgL_tmpz00_7501);
				}
				if (STRINGP(BgL_fnamez00_5357))
					{	/* Llib/error.scm 959 */
						BgL_auxz00_7493 = BgL_fnamez00_5357;
					}
				else
					{
						obj_t BgL_auxz00_7496;

						BgL_auxz00_7496 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2811z00zz__errorz00,
							BINT(38480L), BGl_string2871z00zz__errorz00,
							BGl_string2838z00zz__errorz00, BgL_fnamez00_5357);
						FAILURE(BgL_auxz00_7496, BFALSE, BFALSE);
					}
				return
					BGl_warningzd2notifyzf2locationz20zz__errorz00(BgL_ez00_5356,
					BgL_auxz00_7493, BgL_auxz00_7500);
			}
		}

	}



/* warning/location-file */
	obj_t BGl_warningzf2locationzd2filez20zz__errorz00(obj_t BgL_fnamez00_103,
		obj_t BgL_locz00_104, obj_t BgL_argsz00_105)
	{
		{	/* Llib/error.scm 966 */
			{	/* Llib/error.scm 968 */
				obj_t BgL_portz00_2409;

				BgL_portz00_2409 =
					BGl_openzd2forzd2errorz00zz__errorz00(BgL_fnamez00_103);
				if (INPUT_PORTP(BgL_portz00_2409))
					{	/* Llib/error.scm 974 */
						obj_t BgL_filez00_2411;

						{	/* Llib/error.scm 975 */
							obj_t BgL_arg1910z00_2416;

							{	/* Llib/error.scm 975 */
								obj_t BgL_list1911z00_2417;

								{	/* Llib/error.scm 975 */
									obj_t BgL_arg1912z00_2418;

									{	/* Llib/error.scm 975 */
										obj_t BgL_arg1913z00_2419;

										BgL_arg1913z00_2419 = MAKE_YOUNG_PAIR(BNIL, BNIL);
										BgL_arg1912z00_2418 =
											MAKE_YOUNG_PAIR(BgL_locz00_104, BgL_arg1913z00_2419);
									}
									BgL_list1911z00_2417 =
										MAKE_YOUNG_PAIR(BgL_fnamez00_103, BgL_arg1912z00_2418);
								}
								BgL_arg1910z00_2416 =
									BGl_consza2za2zz__r4_pairs_and_lists_6_3z00
									(BGl_symbol2815z00zz__errorz00, BgL_list1911z00_2417);
							}
							BgL_filez00_2411 =
								BGl_locationzd2linezd2numz00zz__errorz00(BgL_arg1910z00_2416);
						}
						{	/* Llib/error.scm 975 */
							obj_t BgL_lnumz00_2412;
							obj_t BgL_lpointz00_2413;
							obj_t BgL_lstringz00_2414;

							{	/* Llib/error.scm 976 */
								obj_t BgL_tmpz00_4364;

								{	/* Llib/error.scm 976 */
									int BgL_tmpz00_7518;

									BgL_tmpz00_7518 = (int) (1L);
									BgL_tmpz00_4364 = BGL_MVALUES_VAL(BgL_tmpz00_7518);
								}
								{	/* Llib/error.scm 976 */
									int BgL_tmpz00_7521;

									BgL_tmpz00_7521 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_7521, BUNSPEC);
								}
								BgL_lnumz00_2412 = BgL_tmpz00_4364;
							}
							{	/* Llib/error.scm 976 */
								obj_t BgL_tmpz00_4365;

								{	/* Llib/error.scm 976 */
									int BgL_tmpz00_7524;

									BgL_tmpz00_7524 = (int) (2L);
									BgL_tmpz00_4365 = BGL_MVALUES_VAL(BgL_tmpz00_7524);
								}
								{	/* Llib/error.scm 976 */
									int BgL_tmpz00_7527;

									BgL_tmpz00_7527 = (int) (2L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_7527, BUNSPEC);
								}
								BgL_lpointz00_2413 = BgL_tmpz00_4365;
							}
							{	/* Llib/error.scm 976 */
								obj_t BgL_tmpz00_4366;

								{	/* Llib/error.scm 976 */
									int BgL_tmpz00_7530;

									BgL_tmpz00_7530 = (int) (3L);
									BgL_tmpz00_4366 = BGL_MVALUES_VAL(BgL_tmpz00_7530);
								}
								{	/* Llib/error.scm 976 */
									int BgL_tmpz00_7533;

									BgL_tmpz00_7533 = (int) (3L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_7533, BUNSPEC);
								}
								BgL_lstringz00_2414 = BgL_tmpz00_4366;
							}
							if (CBOOL(BgL_lnumz00_2412))
								{	/* Llib/error.scm 976 */
									return
										BGl_dozd2warnzf2locationz20zz__errorz00(BgL_fnamez00_103,
										BgL_lnumz00_2412, BgL_locz00_104, BgL_lstringz00_2414,
										BgL_lpointz00_2413, BgL_argsz00_105);
								}
							else
								{	/* Llib/error.scm 976 */
									return BGl_warningz00zz__errorz00(BgL_argsz00_105);
								}
						}
					}
				else
					{	/* Llib/error.scm 969 */
						return BGl_warningz00zz__errorz00(BgL_argsz00_105);
					}
			}
		}

	}



/* do-warn/location */
	obj_t BGl_dozd2warnzf2locationz20zz__errorz00(obj_t BgL_fnamez00_106,
		obj_t BgL_linez00_107, obj_t BgL_charz00_108, obj_t BgL_stringz00_109,
		obj_t BgL_markerz00_110, obj_t BgL_argsz00_111)
	{
		{	/* Llib/error.scm 983 */
			{	/* Llib/error.scm 984 */
				obj_t BgL_arg1916z00_2421;

				{	/* Llib/error.scm 984 */
					obj_t BgL_tmpz00_7541;

					BgL_tmpz00_7541 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg1916z00_2421 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_7541);
				}
				bgl_flush_output_port(BgL_arg1916z00_2421);
			}
			{	/* Llib/error.scm 985 */
				obj_t BgL_arg1917z00_2422;

				{	/* Llib/error.scm 985 */
					obj_t BgL_tmpz00_7545;

					BgL_tmpz00_7545 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg1917z00_2422 = BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_7545);
				}
				{	/* Llib/error.scm 985 */
					obj_t BgL_list1918z00_2423;

					BgL_list1918z00_2423 = MAKE_YOUNG_PAIR(BgL_arg1917z00_2422, BNIL);
					BGl_newlinez00zz__r4_output_6_10_3z00(BgL_list1918z00_2423);
				}
			}
			{	/* Llib/error.scm 986 */
				obj_t BgL_spacezd2stringzd2_2424;

				if (((long) CINT(BgL_markerz00_110) > 0L))
					{	/* Llib/error.scm 986 */
						BgL_spacezd2stringzd2_2424 =
							make_string(
							(long) CINT(BgL_markerz00_110), ((unsigned char) ' '));
					}
				else
					{	/* Llib/error.scm 986 */
						BgL_spacezd2stringzd2_2424 = BGl_string2856z00zz__errorz00;
					}
				{	/* Llib/error.scm 986 */
					long BgL_lz00_2425;

					BgL_lz00_2425 = STRING_LENGTH(((obj_t) BgL_stringz00_109));
					{	/* Llib/error.scm 989 */
						obj_t BgL_nzd2markerzd2_2426;

						if (((long) CINT(BgL_markerz00_110) >= BgL_lz00_2425))
							{	/* Llib/error.scm 990 */
								BgL_nzd2markerzd2_2426 = BINT(BgL_lz00_2425);
							}
						else
							{	/* Llib/error.scm 990 */
								BgL_nzd2markerzd2_2426 = BgL_markerz00_110;
							}
						{	/* Llib/error.scm 990 */

							BGl_fixzd2tabulationz12zc0zz__errorz00(BgL_nzd2markerzd2_2426,
								BgL_stringz00_109, BgL_spacezd2stringzd2_2424);
							BGl_printzd2cursorzd2zz__errorz00(BgL_fnamez00_106,
								BgL_linez00_107, BgL_charz00_108, BgL_stringz00_109,
								BgL_spacezd2stringzd2_2424);
							{	/* Llib/error.scm 996 */
								obj_t BgL_arg1919z00_2427;

								{	/* Llib/error.scm 996 */
									obj_t BgL_tmpz00_7563;

									BgL_tmpz00_7563 = BGL_CURRENT_DYNAMIC_ENV();
									BgL_arg1919z00_2427 =
										BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_7563);
								}
								{	/* Llib/error.scm 996 */
									obj_t BgL_list1920z00_2428;

									BgL_list1920z00_2428 =
										MAKE_YOUNG_PAIR(BgL_arg1919z00_2427, BNIL);
									BGl_displayz00zz__r4_output_6_10_3z00
										(BGl_string2867z00zz__errorz00, BgL_list1920z00_2428);
								}
							}
							if (NULLP(BgL_argsz00_111))
								{	/* Llib/error.scm 997 */
									BFALSE;
								}
							else
								{	/* Llib/error.scm 998 */
									obj_t BgL_portz00_2430;

									{	/* Llib/error.scm 998 */
										obj_t BgL_tmpz00_7570;

										BgL_tmpz00_7570 = BGL_CURRENT_DYNAMIC_ENV();
										BgL_portz00_2430 =
											BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_7570);
									}
									{	/* Llib/error.scm 999 */
										obj_t BgL_arg1923z00_2431;

										BgL_arg1923z00_2431 = CAR(((obj_t) BgL_argsz00_111));
										BGl_displayzd2circlezd2zz__pp_circlez00(BgL_arg1923z00_2431,
											BgL_portz00_2430);
									}
									{	/* Llib/error.scm 1000 */
										obj_t BgL_list1924z00_2432;

										BgL_list1924z00_2432 =
											MAKE_YOUNG_PAIR(BgL_portz00_2430, BNIL);
										BGl_newlinez00zz__r4_output_6_10_3z00(BgL_list1924z00_2432);
									}
									{	/* Llib/error.scm 1001 */
										obj_t BgL_arg1926z00_2434;

										BgL_arg1926z00_2434 = CDR(((obj_t) BgL_argsz00_111));
										{	/* Llib/error.scm 1001 */
											obj_t BgL_list1927z00_2435;

											BgL_list1927z00_2435 =
												MAKE_YOUNG_PAIR(BgL_arg1926z00_2434, BNIL);
											BGl_forzd2eachzd2zz__r4_control_features_6_9z00
												(BGl_proc2872z00zz__errorz00, BgL_list1927z00_2435);
										}
									}
								}
							{	/* Llib/error.scm 1004 */
								obj_t BgL_arg1930z00_2440;

								{	/* Llib/error.scm 1004 */
									obj_t BgL_tmpz00_7582;

									BgL_tmpz00_7582 = BGL_CURRENT_DYNAMIC_ENV();
									BgL_arg1930z00_2440 =
										BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_7582);
								}
								{	/* Llib/error.scm 1004 */
									obj_t BgL_list1931z00_2441;

									BgL_list1931z00_2441 =
										MAKE_YOUNG_PAIR(BgL_arg1930z00_2440, BNIL);
									BGl_newlinez00zz__r4_output_6_10_3z00(BgL_list1931z00_2441);
								}
							}
							{	/* Llib/error.scm 1005 */
								obj_t BgL_arg1932z00_2442;

								{	/* Llib/error.scm 1005 */
									obj_t BgL_tmpz00_7587;

									BgL_tmpz00_7587 = BGL_CURRENT_DYNAMIC_ENV();
									BgL_arg1932z00_2442 =
										BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_7587);
								}
								return bgl_flush_output_port(BgL_arg1932z00_2442);
							}
						}
					}
				}
			}
		}

	}



/* &<@anonymous:1928> */
	obj_t BGl_z62zc3z04anonymousza31928ze3ze5zz__errorz00(obj_t BgL_envz00_5360,
		obj_t BgL_az00_5361)
	{
		{	/* Llib/error.scm 1001 */
			{	/* Llib/error.scm 1002 */
				obj_t BgL_arg1929z00_5462;

				{	/* Llib/error.scm 1002 */
					obj_t BgL_tmpz00_7591;

					BgL_tmpz00_7591 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg1929z00_5462 = BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_7591);
				}
				return
					BGl_displayzd2circlezd2zz__pp_circlez00(BgL_az00_5361,
					BgL_arg1929z00_5462);
			}
		}

	}



/* _display-trace-stack */
	obj_t BGl__displayzd2tracezd2stackz00zz__errorz00(obj_t BgL_env1216z00_117,
		obj_t BgL_opt1215z00_116)
	{
		{	/* Llib/error.scm 1018 */
			{	/* Llib/error.scm 1018 */
				obj_t BgL_g1217z00_2447;
				obj_t BgL_g1218z00_2448;

				BgL_g1217z00_2447 = VECTOR_REF(BgL_opt1215z00_116, 0L);
				BgL_g1218z00_2448 = VECTOR_REF(BgL_opt1215z00_116, 1L);
				switch (VECTOR_LENGTH(BgL_opt1215z00_116))
					{
					case 2L:

						{	/* Llib/error.scm 1018 */

							{	/* Llib/error.scm 1018 */
								obj_t BgL_auxz00_7597;

								if (OUTPUT_PORTP(BgL_g1218z00_2448))
									{	/* Llib/error.scm 1018 */
										BgL_auxz00_7597 = BgL_g1218z00_2448;
									}
								else
									{
										obj_t BgL_auxz00_7600;

										BgL_auxz00_7600 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2811z00zz__errorz00, BINT(41116L),
											BGl_string2873z00zz__errorz00,
											BGl_string2874z00zz__errorz00, BgL_g1218z00_2448);
										FAILURE(BgL_auxz00_7600, BFALSE, BFALSE);
									}
								return
									BGl_displayzd2tracezd2stackz00zz__errorz00(BgL_g1217z00_2447,
									BgL_auxz00_7597, BINT(1L));
							}
						}
						break;
					case 3L:

						{	/* Llib/error.scm 1018 */
							obj_t BgL_offsetz00_2452;

							BgL_offsetz00_2452 = VECTOR_REF(BgL_opt1215z00_116, 2L);
							{	/* Llib/error.scm 1018 */

								{	/* Llib/error.scm 1018 */
									obj_t BgL_auxz00_7607;

									if (OUTPUT_PORTP(BgL_g1218z00_2448))
										{	/* Llib/error.scm 1018 */
											BgL_auxz00_7607 = BgL_g1218z00_2448;
										}
									else
										{
											obj_t BgL_auxz00_7610;

											BgL_auxz00_7610 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2811z00zz__errorz00, BINT(41116L),
												BGl_string2873z00zz__errorz00,
												BGl_string2874z00zz__errorz00, BgL_g1218z00_2448);
											FAILURE(BgL_auxz00_7610, BFALSE, BFALSE);
										}
									return
										BGl_displayzd2tracezd2stackz00zz__errorz00
										(BgL_g1217z00_2447, BgL_auxz00_7607, BgL_offsetz00_2452);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* display-trace-stack */
	BGL_EXPORTED_DEF obj_t BGl_displayzd2tracezd2stackz00zz__errorz00(obj_t
		BgL_stackz00_113, obj_t BgL_portz00_114, obj_t BgL_offsetz00_115)
	{
		{	/* Llib/error.scm 1018 */
			if (PAIRP(BgL_stackz00_113))
				{	/* Llib/error.scm 1102 */
					obj_t BgL_g1134z00_2456;
					obj_t BgL_g1135z00_2457;

					BgL_g1134z00_2456 = CDR(BgL_stackz00_113);
					BgL_g1135z00_2457 = CAR(BgL_stackz00_113);
					{
						obj_t BgL_iz00_2459;
						obj_t BgL_stkz00_2460;
						obj_t BgL_hdsz00_2461;
						long BgL_hdnz00_2462;

						BgL_iz00_2459 = BgL_offsetz00_115;
						BgL_stkz00_2460 = BgL_g1134z00_2456;
						BgL_hdsz00_2461 = BgL_g1135z00_2457;
						BgL_hdnz00_2462 = 1L;
					BgL_zc3z04anonymousza31937ze3z87_2463:
						if (NULLP(BgL_stkz00_2460))
							{	/* Llib/error.scm 1107 */
								BGl_displayzd2tracezd2stackzd2frameze70z35zz__errorz00
									(BgL_portz00_114, BgL_hdsz00_2461, BgL_iz00_2459,
									BgL_hdnz00_2462);
								return bgl_flush_output_port(BgL_portz00_114);
							}
						else
							{	/* Llib/error.scm 1107 */
								if (PAIRP(BgL_stkz00_2460))
									{	/* Llib/error.scm 1110 */
										if ((CAR(BgL_stkz00_2460) == BgL_hdsz00_2461))
											{	/* Llib/error.scm 1115 */
												long BgL_arg1942z00_2468;
												obj_t BgL_arg1943z00_2469;
												long BgL_arg1944z00_2470;

												BgL_arg1942z00_2468 = ((long) CINT(BgL_iz00_2459) + 1L);
												BgL_arg1943z00_2469 = CDR(BgL_stkz00_2460);
												BgL_arg1944z00_2470 = (BgL_hdnz00_2462 + 1L);
												{
													long BgL_hdnz00_7637;
													obj_t BgL_stkz00_7636;
													obj_t BgL_iz00_7634;

													BgL_iz00_7634 = BINT(BgL_arg1942z00_2468);
													BgL_stkz00_7636 = BgL_arg1943z00_2469;
													BgL_hdnz00_7637 = BgL_arg1944z00_2470;
													BgL_hdnz00_2462 = BgL_hdnz00_7637;
													BgL_stkz00_2460 = BgL_stkz00_7636;
													BgL_iz00_2459 = BgL_iz00_7634;
													goto BgL_zc3z04anonymousza31937ze3z87_2463;
												}
											}
										else
											{
												long BgL_hdnz00_7644;
												obj_t BgL_hdsz00_7642;
												obj_t BgL_stkz00_7640;
												obj_t BgL_iz00_7638;

												BgL_iz00_7638 =
													BGl_displayzd2tracezd2stackzd2frameze70z35zz__errorz00
													(BgL_portz00_114, BgL_hdsz00_2461, BgL_iz00_2459,
													BgL_hdnz00_2462);
												BgL_stkz00_7640 = CDR(BgL_stkz00_2460);
												BgL_hdsz00_7642 = CAR(BgL_stkz00_2460);
												BgL_hdnz00_7644 = 1L;
												BgL_hdnz00_2462 = BgL_hdnz00_7644;
												BgL_hdsz00_2461 = BgL_hdsz00_7642;
												BgL_stkz00_2460 = BgL_stkz00_7640;
												BgL_iz00_2459 = BgL_iz00_7638;
												goto BgL_zc3z04anonymousza31937ze3z87_2463;
											}
									}
								else
									{	/* Llib/error.scm 1110 */
										{	/* Llib/error.scm 1111 */
											obj_t BgL_arg1948z00_2475;

											{	/* Llib/error.scm 1111 */
												obj_t BgL_tmpz00_7645;

												BgL_tmpz00_7645 = BGL_CURRENT_DYNAMIC_ENV();
												BgL_arg1948z00_2475 =
													BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_7645);
											}
											{	/* Llib/error.scm 1111 */
												obj_t BgL_list1949z00_2476;

												BgL_list1949z00_2476 =
													MAKE_YOUNG_PAIR(BgL_stackz00_113, BNIL);
												BGl_fprintfz00zz__r4_output_6_10_3z00
													(BgL_arg1948z00_2475, BGl_string2875z00zz__errorz00,
													BgL_list1949z00_2476);
											}
										}
										return bgl_flush_output_port(BgL_portz00_114);
									}
							}
					}
				}
			else
				{	/* Llib/error.scm 1101 */
					return BFALSE;
				}
		}

	}



/* display-trace-stack-frame~0 */
	obj_t BGl_displayzd2tracezd2stackzd2frameze70z35zz__errorz00(obj_t
		BgL_portz00_5404, obj_t BgL_framez00_2484, obj_t BgL_levelz00_2485,
		long BgL_numz00_2486)
	{
		{	/* Llib/error.scm 1099 */
			{
				obj_t BgL_namez00_2488;
				obj_t BgL_locz00_2489;
				obj_t BgL_restz00_2490;
				obj_t BgL_namez00_2492;

				if (PAIRP(BgL_framez00_2484))
					{	/* Llib/error.scm 1099 */
						obj_t BgL_cdrzd2347zd2_2498;

						BgL_cdrzd2347zd2_2498 = CDR(((obj_t) BgL_framez00_2484));
						if (PAIRP(BgL_cdrzd2347zd2_2498))
							{	/* Llib/error.scm 1099 */
								obj_t BgL_cdrzd2352zd2_2500;

								BgL_cdrzd2352zd2_2500 = CDR(BgL_cdrzd2347zd2_2498);
								{	/* Llib/error.scm 1099 */
									bool_t BgL_test3340z00_7658;

									if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00
										(BgL_cdrzd2352zd2_2500))
										{	/* Llib/error.scm 1013 */
											obj_t BgL_list1935z00_4421;

											BgL_list1935z00_4421 =
												MAKE_YOUNG_PAIR(BgL_cdrzd2352zd2_2500, BNIL);
											BgL_test3340z00_7658 =
												CBOOL(BGl_everyz00zz__r4_pairs_and_lists_6_3z00
												(BGl_pairzf3zd2envz21zz__r4_pairs_and_lists_6_3z00,
													BgL_list1935z00_4421));
										}
									else
										{	/* Llib/error.scm 1013 */
											BgL_test3340z00_7658 = ((bool_t) 0);
										}
									if (BgL_test3340z00_7658)
										{	/* Llib/error.scm 1099 */
											obj_t BgL_arg1957z00_2502;
											obj_t BgL_arg1958z00_2503;

											BgL_arg1957z00_2502 = CAR(((obj_t) BgL_framez00_2484));
											BgL_arg1958z00_2503 = CAR(BgL_cdrzd2347zd2_2498);
											{	/* Llib/error.scm 1099 */
												long BgL_tmpz00_7667;

												BgL_namez00_2488 = BgL_arg1957z00_2502;
												BgL_locz00_2489 = BgL_arg1958z00_2503;
												BgL_restz00_2490 = BgL_cdrzd2352zd2_2500;
												{	/* Llib/error.scm 1032 */
													obj_t BgL_namez00_2511;

													if (SYMBOLP(BgL_namez00_2488))
														{	/* Llib/error.scm 1032 */
															obj_t BgL_arg2328z00_4387;

															BgL_arg2328z00_4387 =
																SYMBOL_TO_STRING(BgL_namez00_2488);
															BgL_namez00_2511 =
																BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																(BgL_arg2328z00_4387);
														}
													else
														{	/* Llib/error.scm 1032 */
															BgL_namez00_2511 = BgL_namez00_2488;
														}
													{	/* Llib/error.scm 1032 */
														obj_t BgL_marginz00_2512;

														BgL_marginz00_2512 =
															BGl_assqz00zz__r4_pairs_and_lists_6_3z00
															(BGl_symbol2881z00zz__errorz00, BgL_restz00_2490);
														{	/* Llib/error.scm 1033 */
															obj_t BgL_fmtz00_2513;

															BgL_fmtz00_2513 =
																BGl_assqz00zz__r4_pairs_and_lists_6_3z00
																(BGl_symbol2883z00zz__errorz00,
																BgL_restz00_2490);
															{	/* Llib/error.scm 1034 */
																obj_t BgL_nmz00_2514;

																{	/* Llib/error.scm 1035 */
																	bool_t BgL_test3343z00_7674;

																	if (PAIRP(BgL_fmtz00_2513))
																		{	/* Llib/error.scm 1035 */
																			obj_t BgL_tmpz00_7677;

																			BgL_tmpz00_7677 = CDR(BgL_fmtz00_2513);
																			BgL_test3343z00_7674 =
																				STRINGP(BgL_tmpz00_7677);
																		}
																	else
																		{	/* Llib/error.scm 1035 */
																			BgL_test3343z00_7674 = ((bool_t) 0);
																		}
																	if (BgL_test3343z00_7674)
																		{	/* Llib/error.scm 1036 */
																			obj_t BgL_arg2008z00_2567;

																			BgL_arg2008z00_2567 =
																				CDR(BgL_fmtz00_2513);
																			{	/* Llib/error.scm 1036 */
																				obj_t BgL_list2009z00_2568;

																				BgL_list2009z00_2568 =
																					MAKE_YOUNG_PAIR(BgL_namez00_2511,
																					BNIL);
																				BgL_nmz00_2514 =
																					BGl_formatz00zz__r4_output_6_10_3z00
																					(BgL_arg2008z00_2567,
																					BgL_list2009z00_2568);
																			}
																		}
																	else
																		{	/* Llib/error.scm 1035 */
																			BgL_nmz00_2514 = BgL_namez00_2511;
																		}
																}
																{	/* Llib/error.scm 1035 */

																	{	/* Llib/error.scm 1039 */
																		bool_t BgL_test3345z00_7683;

																		if (PAIRP(BgL_marginz00_2512))
																			{	/* Llib/error.scm 1039 */
																				obj_t BgL_tmpz00_7686;

																				BgL_tmpz00_7686 =
																					CDR(BgL_marginz00_2512);
																				BgL_test3345z00_7683 =
																					CHARP(BgL_tmpz00_7686);
																			}
																		else
																			{	/* Llib/error.scm 1039 */
																				BgL_test3345z00_7683 = ((bool_t) 0);
																			}
																		if (BgL_test3345z00_7683)
																			{	/* Llib/error.scm 1040 */
																				obj_t BgL_arg1969z00_2518;

																				BgL_arg1969z00_2518 =
																					CDR(BgL_marginz00_2512);
																				{	/* Llib/error.scm 1040 */
																					obj_t BgL_list1970z00_2519;

																					BgL_list1970z00_2519 =
																						MAKE_YOUNG_PAIR(BgL_portz00_5404,
																						BNIL);
																					BGl_displayz00zz__r4_output_6_10_3z00
																						(BgL_arg1969z00_2518,
																						BgL_list1970z00_2519);
																				}
																			}
																		else
																			{	/* Llib/error.scm 1041 */
																				obj_t BgL_list1971z00_2520;

																				BgL_list1971z00_2520 =
																					MAKE_YOUNG_PAIR(BgL_portz00_5404,
																					BNIL);
																				BGl_displayz00zz__r4_output_6_10_3z00
																					(BGl_string2885z00zz__errorz00,
																					BgL_list1971z00_2520);
																			}
																	}
																	if (((long) CINT(BgL_levelz00_2485) < 10L))
																		{	/* Llib/error.scm 1043 */
																			obj_t BgL_list1974z00_2524;

																			BgL_list1974z00_2524 =
																				MAKE_YOUNG_PAIR(BgL_portz00_5404, BNIL);
																			BGl_displayz00zz__r4_output_6_10_3z00
																				(BGl_string2878z00zz__errorz00,
																				BgL_list1974z00_2524);
																		}
																	else
																		{	/* Llib/error.scm 1043 */
																			if (
																				((long) CINT(BgL_levelz00_2485) < 100L))
																				{	/* Llib/error.scm 1044 */
																					obj_t BgL_list1976z00_2526;

																					BgL_list1976z00_2526 =
																						MAKE_YOUNG_PAIR(BgL_portz00_5404,
																						BNIL);
																					BGl_displayz00zz__r4_output_6_10_3z00
																						(BGl_string2879z00zz__errorz00,
																						BgL_list1976z00_2526);
																				}
																			else
																				{	/* Llib/error.scm 1044 */
																					if (
																						((long) CINT(BgL_levelz00_2485) <
																							1000L))
																						{	/* Llib/error.scm 1045 */
																							obj_t BgL_list1978z00_2528;

																							BgL_list1978z00_2528 =
																								MAKE_YOUNG_PAIR
																								(BgL_portz00_5404, BNIL);
																							BGl_displayz00zz__r4_output_6_10_3z00
																								(BGl_string2885z00zz__errorz00,
																								BgL_list1978z00_2528);
																						}
																					else
																						{	/* Llib/error.scm 1045 */
																							BFALSE;
																						}
																				}
																		}
																	{	/* Llib/error.scm 1047 */
																		obj_t BgL_list1979z00_2529;

																		BgL_list1979z00_2529 =
																			MAKE_YOUNG_PAIR(BgL_portz00_5404, BNIL);
																		BGl_displayz00zz__r4_output_6_10_3z00
																			(BgL_levelz00_2485, BgL_list1979z00_2529);
																	}
																	{	/* Llib/error.scm 1048 */
																		obj_t BgL_list1980z00_2530;

																		BgL_list1980z00_2530 =
																			MAKE_YOUNG_PAIR(BgL_portz00_5404, BNIL);
																		BGl_displayz00zz__r4_output_6_10_3z00
																			(BGl_string2880z00zz__errorz00,
																			BgL_list1980z00_2530);
																	}
																	{	/* Llib/error.scm 1050 */
																		obj_t BgL_iz00_2531;

																		{	/* Ieee/string.scm 223 */

																			BgL_iz00_2531 =
																				BGl_stringzd2indexzd2zz__r4_strings_6_7z00
																				(BgL_namez00_2511,
																				BCHAR(((unsigned char) '@')), BINT(0L));
																		}
																		{	/* Llib/error.scm 1051 */
																			bool_t BgL_test3350z00_7716;

																			if (CBOOL(BgL_iz00_2531))
																				{	/* Llib/error.scm 1051 */
																					if (((long) CINT(BgL_iz00_2531) > 0L))
																						{	/* Llib/error.scm 1051 */
																							BgL_test3350z00_7716 =
																								CBOOL(BgL_locz00_2489);
																						}
																					else
																						{	/* Llib/error.scm 1051 */
																							BgL_test3350z00_7716 =
																								((bool_t) 0);
																						}
																				}
																			else
																				{	/* Llib/error.scm 1051 */
																					BgL_test3350z00_7716 = ((bool_t) 0);
																				}
																			if (BgL_test3350z00_7716)
																				{	/* Llib/error.scm 1052 */
																					obj_t BgL_arg1983z00_2534;

																					BgL_arg1983z00_2534 =
																						BGl_substringz00zz__r4_strings_6_7z00
																						(BgL_namez00_2511, 0L,
																						(long) CINT(BgL_iz00_2531));
																					{	/* Llib/error.scm 1052 */
																						obj_t BgL_list1984z00_2535;

																						BgL_list1984z00_2535 =
																							MAKE_YOUNG_PAIR(BgL_portz00_5404,
																							BNIL);
																						BGl_displayz00zz__r4_output_6_10_3z00
																							(BgL_arg1983z00_2534,
																							BgL_list1984z00_2535);
																				}}
																			else
																				{	/* Llib/error.scm 1053 */
																					obj_t BgL_list1985z00_2536;

																					BgL_list1985z00_2536 =
																						MAKE_YOUNG_PAIR(BgL_portz00_5404,
																						BNIL);
																					BGl_displayz00zz__r4_output_6_10_3z00
																						(BgL_namez00_2511,
																						BgL_list1985z00_2536);
																				}
																		}
																	}
																	if ((BgL_numz00_2486 > 1L))
																		{	/* Llib/error.scm 1055 */
																			{	/* Llib/error.scm 1056 */
																				obj_t BgL_list1989z00_2542;

																				BgL_list1989z00_2542 =
																					MAKE_YOUNG_PAIR(BgL_portz00_5404,
																					BNIL);
																				BGl_displayz00zz__r4_output_6_10_3z00
																					(BGl_string2886z00zz__errorz00,
																					BgL_list1989z00_2542);
																			}
																			{	/* Llib/error.scm 1057 */
																				obj_t BgL_list1990z00_2543;

																				BgL_list1990z00_2543 =
																					MAKE_YOUNG_PAIR(BgL_portz00_5404,
																					BNIL);
																				BGl_displayz00zz__r4_output_6_10_3z00
																					(BINT(BgL_numz00_2486),
																					BgL_list1990z00_2543);
																			}
																			{	/* Llib/error.scm 1058 */
																				obj_t BgL_list1991z00_2544;

																				BgL_list1991z00_2544 =
																					MAKE_YOUNG_PAIR(BgL_portz00_5404,
																					BNIL);
																				BGl_displayz00zz__r4_output_6_10_3z00
																					(BGl_string2887z00zz__errorz00,
																					BgL_list1991z00_2544);
																			}
																		}
																	else
																		{	/* Llib/error.scm 1055 */
																			if (CBOOL(BgL_locz00_2489))
																				{	/* Llib/error.scm 1060 */
																					obj_t BgL_filez00_2545;

																					BgL_filez00_2545 =
																						BGl_locationzd2linezd2numz00zz__errorz00
																						(BgL_locz00_2489);
																					{	/* Llib/error.scm 1061 */
																						obj_t BgL_lnumz00_2546;
																						obj_t BgL_lpointz00_2547;
																						obj_t BgL_lstringz00_2548;

																						{	/* Llib/error.scm 1065 */
																							obj_t BgL_tmpz00_4397;

																							{	/* Llib/error.scm 1065 */
																								int BgL_tmpz00_7741;

																								BgL_tmpz00_7741 = (int) (1L);
																								BgL_tmpz00_4397 =
																									BGL_MVALUES_VAL
																									(BgL_tmpz00_7741);
																							}
																							{	/* Llib/error.scm 1065 */
																								int BgL_tmpz00_7744;

																								BgL_tmpz00_7744 = (int) (1L);
																								BGL_MVALUES_VAL_SET
																									(BgL_tmpz00_7744, BUNSPEC);
																							}
																							BgL_lnumz00_2546 =
																								BgL_tmpz00_4397;
																						}
																						{	/* Llib/error.scm 1065 */
																							obj_t BgL_tmpz00_4398;

																							{	/* Llib/error.scm 1065 */
																								int BgL_tmpz00_7747;

																								BgL_tmpz00_7747 = (int) (2L);
																								BgL_tmpz00_4398 =
																									BGL_MVALUES_VAL
																									(BgL_tmpz00_7747);
																							}
																							{	/* Llib/error.scm 1065 */
																								int BgL_tmpz00_7750;

																								BgL_tmpz00_7750 = (int) (2L);
																								BGL_MVALUES_VAL_SET
																									(BgL_tmpz00_7750, BUNSPEC);
																							}
																							BgL_lpointz00_2547 =
																								BgL_tmpz00_4398;
																						}
																						{	/* Llib/error.scm 1065 */
																							obj_t BgL_tmpz00_4399;

																							{	/* Llib/error.scm 1065 */
																								int BgL_tmpz00_7753;

																								BgL_tmpz00_7753 = (int) (3L);
																								BgL_tmpz00_4399 =
																									BGL_MVALUES_VAL
																									(BgL_tmpz00_7753);
																							}
																							{	/* Llib/error.scm 1065 */
																								int BgL_tmpz00_7756;

																								BgL_tmpz00_7756 = (int) (3L);
																								BGL_MVALUES_VAL_SET
																									(BgL_tmpz00_7756, BUNSPEC);
																							}
																							BgL_lstringz00_2548 =
																								BgL_tmpz00_4399;
																						}
																						{	/* Llib/error.scm 1063 */
																							bool_t BgL_test3355z00_7759;

																							if (STRINGP(BgL_filez00_2545))
																								{	/* Llib/error.scm 1063 */
																									bool_t BgL_test3357z00_7762;

																									{	/* Llib/error.scm 1063 */
																										long BgL_l1z00_4402;

																										BgL_l1z00_4402 =
																											STRING_LENGTH
																											(BgL_filez00_2545);
																										if ((BgL_l1z00_4402 == 1L))
																											{	/* Llib/error.scm 1063 */
																												int BgL_arg2321z00_4405;

																												{	/* Llib/error.scm 1063 */
																													char *BgL_auxz00_7768;
																													char *BgL_tmpz00_7766;

																													BgL_auxz00_7768 =
																														BSTRING_TO_STRING
																														(BGl_string2888z00zz__errorz00);
																													BgL_tmpz00_7766 =
																														BSTRING_TO_STRING
																														(BgL_filez00_2545);
																													BgL_arg2321z00_4405 =
																														memcmp
																														(BgL_tmpz00_7766,
																														BgL_auxz00_7768,
																														BgL_l1z00_4402);
																												}
																												BgL_test3357z00_7762 =
																													(
																													(long)
																													(BgL_arg2321z00_4405)
																													== 0L);
																											}
																										else
																											{	/* Llib/error.scm 1063 */
																												BgL_test3357z00_7762 =
																													((bool_t) 0);
																											}
																									}
																									if (BgL_test3357z00_7762)
																										{	/* Llib/error.scm 1063 */
																											BgL_test3355z00_7759 =
																												((bool_t) 0);
																										}
																									else
																										{	/* Llib/error.scm 1063 */
																											BgL_test3355z00_7759 =
																												((bool_t) 1);
																										}
																								}
																							else
																								{	/* Llib/error.scm 1063 */
																									BgL_test3355z00_7759 =
																										((bool_t) 0);
																								}
																							if (BgL_test3355z00_7759)
																								{	/* Llib/error.scm 1063 */
																									{	/* Llib/error.scm 1064 */
																										obj_t BgL_list1995z00_2552;

																										BgL_list1995z00_2552 =
																											MAKE_YOUNG_PAIR
																											(BgL_portz00_5404, BNIL);
																										BGl_displayz00zz__r4_output_6_10_3z00
																											(BGl_string2889z00zz__errorz00,
																											BgL_list1995z00_2552);
																									}
																									{	/* Llib/error.scm 1065 */
																										obj_t BgL_list1996z00_2553;

																										BgL_list1996z00_2553 =
																											MAKE_YOUNG_PAIR
																											(BgL_portz00_5404, BNIL);
																										BGl_displayz00zz__r4_output_6_10_3z00
																											(BgL_filez00_2545,
																											BgL_list1996z00_2553);
																									}
																								}
																							else
																								{	/* Llib/error.scm 1063 */
																									BFALSE;
																								}
																						}
																						{	/* Llib/error.scm 1068 */
																							bool_t BgL_test3359z00_7777;

																							if (INTEGERP(BgL_lpointz00_2547))
																								{	/* Llib/error.scm 1068 */
																									BgL_test3359z00_7777 =
																										(
																										(long)
																										CINT(BgL_lpointz00_2547) ==
																										0L);
																								}
																							else
																								{	/* Llib/error.scm 1068 */
																									BgL_test3359z00_7777 =
																										((bool_t) 0);
																								}
																							if (BgL_test3359z00_7777)
																								{	/* Llib/error.scm 1068 */
																									BUNSPEC;
																								}
																							else
																								{	/* Llib/error.scm 1068 */
																									if (CBOOL(BgL_lnumz00_2546))
																										{	/* Llib/error.scm 1070 */
																											{	/* Llib/error.scm 1071 */
																												obj_t
																													BgL_list1999z00_2558;
																												BgL_list1999z00_2558 =
																													MAKE_YOUNG_PAIR
																													(BgL_portz00_5404,
																													BNIL);
																												BGl_displayz00zz__r4_output_6_10_3z00
																													(BGl_string2824z00zz__errorz00,
																													BgL_list1999z00_2558);
																											}
																											{	/* Llib/error.scm 1072 */
																												obj_t
																													BgL_list2000z00_2559;
																												BgL_list2000z00_2559 =
																													MAKE_YOUNG_PAIR
																													(BgL_portz00_5404,
																													BNIL);
																												BGl_displayz00zz__r4_output_6_10_3z00
																													(BgL_lnumz00_2546,
																													BgL_list2000z00_2559);
																											}
																										}
																									else
																										{	/* Llib/error.scm 1070 */
																											if (CBOOL
																												(BgL_lpointz00_2547))
																												{	/* Llib/error.scm 1073 */
																													{	/* Llib/error.scm 1074 */
																														obj_t
																															BgL_list2001z00_2560;
																														BgL_list2001z00_2560
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_portz00_5404,
																															BNIL);
																														BGl_displayz00zz__r4_output_6_10_3z00
																															(BGl_string2890z00zz__errorz00,
																															BgL_list2001z00_2560);
																													}
																													{	/* Llib/error.scm 1075 */
																														obj_t
																															BgL_list2002z00_2561;
																														BgL_list2002z00_2561
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_portz00_5404,
																															BNIL);
																														BGl_displayz00zz__r4_output_6_10_3z00
																															(BgL_lpointz00_2547,
																															BgL_list2002z00_2561);
																													}
																												}
																											else
																												{	/* Llib/error.scm 1073 */
																													BFALSE;
																												}
																										}
																								}
																						}
																					}
																				}
																			else
																				{	/* Llib/error.scm 1059 */
																					BFALSE;
																				}
																		}
																	{	/* Llib/error.scm 1076 */
																		obj_t BgL_list2003z00_2563;

																		BgL_list2003z00_2563 =
																			MAKE_YOUNG_PAIR(BgL_portz00_5404, BNIL);
																		BGl_newlinez00zz__r4_output_6_10_3z00
																			(BgL_list2003z00_2563);
																	}
																}
															}
														}
													}
												}
												BgL_tmpz00_7667 = ((long) CINT(BgL_levelz00_2485) + 1L);
												return BINT(BgL_tmpz00_7667);
											}
										}
									else
										{	/* Llib/error.scm 1099 */
											long BgL_tmpz00_7799;

										BgL_tagzd2338zd2_2495:
											{	/* Llib/error.scm 1096 */
												obj_t BgL_list2027z00_2588;

												BgL_list2027z00_2588 =
													MAKE_YOUNG_PAIR(BgL_portz00_5404, BNIL);
												BGl_displayz00zz__r4_output_6_10_3z00
													(BGl_string2876z00zz__errorz00, BgL_list2027z00_2588);
											}
											{	/* Llib/error.scm 1097 */
												obj_t BgL_list2028z00_2589;

												BgL_list2028z00_2589 =
													MAKE_YOUNG_PAIR(BgL_portz00_5404, BNIL);
												BGl_displayz00zz__r4_output_6_10_3z00(BgL_framez00_2484,
													BgL_list2028z00_2589);
											}
											{	/* Llib/error.scm 1098 */
												obj_t BgL_list2029z00_2590;

												BgL_list2029z00_2590 =
													MAKE_YOUNG_PAIR(BgL_portz00_5404, BNIL);
												BGl_newlinez00zz__r4_output_6_10_3z00
													(BgL_list2029z00_2590);
											}
											BgL_tmpz00_7799 = ((long) CINT(BgL_levelz00_2485) + 1L);
											return BINT(BgL_tmpz00_7799);
										}
								}
							}
						else
							{	/* Llib/error.scm 1099 */
								if (NULLP(BgL_cdrzd2347zd2_2498))
									{	/* Llib/error.scm 1099 */
										obj_t BgL_arg1962z00_2507;

										BgL_arg1962z00_2507 = CAR(((obj_t) BgL_framez00_2484));
										{	/* Llib/error.scm 1099 */
											long BgL_tmpz00_7813;

											BgL_namez00_2492 = BgL_arg1962z00_2507;
											if (((long) CINT(BgL_levelz00_2485) < 10L))
												{	/* Llib/error.scm 1080 */
													obj_t BgL_list2013z00_2573;

													BgL_list2013z00_2573 =
														MAKE_YOUNG_PAIR(BgL_portz00_5404, BNIL);
													BGl_displayz00zz__r4_output_6_10_3z00
														(BGl_string2877z00zz__errorz00,
														BgL_list2013z00_2573);
												}
											else
												{	/* Llib/error.scm 1080 */
													if (((long) CINT(BgL_levelz00_2485) < 100L))
														{	/* Llib/error.scm 1081 */
															obj_t BgL_list2015z00_2575;

															BgL_list2015z00_2575 =
																MAKE_YOUNG_PAIR(BgL_portz00_5404, BNIL);
															BGl_displayz00zz__r4_output_6_10_3z00
																(BGl_string2878z00zz__errorz00,
																BgL_list2015z00_2575);
														}
													else
														{	/* Llib/error.scm 1081 */
															if (((long) CINT(BgL_levelz00_2485) < 1000L))
																{	/* Llib/error.scm 1082 */
																	obj_t BgL_list2017z00_2577;

																	BgL_list2017z00_2577 =
																		MAKE_YOUNG_PAIR(BgL_portz00_5404, BNIL);
																	BGl_displayz00zz__r4_output_6_10_3z00
																		(BGl_string2879z00zz__errorz00,
																		BgL_list2017z00_2577);
																}
															else
																{	/* Llib/error.scm 1082 */
																	BFALSE;
																}
														}
												}
											{	/* Llib/error.scm 1084 */
												obj_t BgL_list2018z00_2578;

												BgL_list2018z00_2578 =
													MAKE_YOUNG_PAIR(BgL_portz00_5404, BNIL);
												BGl_displayz00zz__r4_output_6_10_3z00(BgL_levelz00_2485,
													BgL_list2018z00_2578);
											}
											{	/* Llib/error.scm 1085 */
												obj_t BgL_arg2019z00_2579;

												{	/* Llib/error.scm 1085 */
													bool_t BgL_test3367z00_7831;

													if (SYMBOLP(BgL_namez00_2492))
														{	/* Llib/error.scm 1085 */
															BgL_test3367z00_7831 = ((bool_t) 1);
														}
													else
														{	/* Llib/error.scm 1085 */
															BgL_test3367z00_7831 = STRINGP(BgL_namez00_2492);
														}
													if (BgL_test3367z00_7831)
														{	/* Llib/error.scm 1085 */
															BgL_arg2019z00_2579 =
																BGl_string2880z00zz__errorz00;
														}
													else
														{	/* Llib/error.scm 1085 */
															BgL_arg2019z00_2579 =
																BGl_string2876z00zz__errorz00;
														}
												}
												{	/* Llib/error.scm 1085 */
													obj_t BgL_list2020z00_2580;

													BgL_list2020z00_2580 =
														MAKE_YOUNG_PAIR(BgL_portz00_5404, BNIL);
													BGl_displayz00zz__r4_output_6_10_3z00
														(BgL_arg2019z00_2579, BgL_list2020z00_2580);
												}
											}
											{	/* Llib/error.scm 1086 */
												obj_t BgL_list2023z00_2584;

												BgL_list2023z00_2584 =
													MAKE_YOUNG_PAIR(BgL_portz00_5404, BNIL);
												BGl_displayz00zz__r4_output_6_10_3z00(BgL_namez00_2492,
													BgL_list2023z00_2584);
											}
											{	/* Llib/error.scm 1087 */
												obj_t BgL_list2024z00_2585;

												BgL_list2024z00_2585 =
													MAKE_YOUNG_PAIR(BgL_portz00_5404, BNIL);
												BGl_newlinez00zz__r4_output_6_10_3z00
													(BgL_list2024z00_2585);
											}
											BgL_tmpz00_7813 = ((long) CINT(BgL_levelz00_2485) + 1L);
											return BINT(BgL_tmpz00_7813);
										}
									}
								else
									{	/* Llib/error.scm 1099 */
										long BgL_tmpz00_7844;

										goto BgL_tagzd2338zd2_2495;
										return BINT(BgL_tmpz00_7844);
									}
							}
					}
				else
					{	/* Llib/error.scm 1099 */
						if (STRINGP(BgL_framez00_2484))
							{	/* Llib/error.scm 1099 */
								{	/* Llib/error.scm 1091 */
									obj_t BgL_list2025z00_2586;

									BgL_list2025z00_2586 =
										MAKE_YOUNG_PAIR(BgL_portz00_5404, BNIL);
									BGl_displayz00zz__r4_output_6_10_3z00(BgL_framez00_2484,
										BgL_list2025z00_2586);
								}
								{	/* Llib/error.scm 1092 */
									obj_t BgL_list2026z00_2587;

									BgL_list2026z00_2587 =
										MAKE_YOUNG_PAIR(BgL_portz00_5404, BNIL);
									BGl_newlinez00zz__r4_output_6_10_3z00(BgL_list2026z00_2587);
								}
								return BgL_levelz00_2485;
							}
						else
							{	/* Llib/error.scm 1099 */
								long BgL_tmpz00_7852;

								goto BgL_tagzd2338zd2_2495;
								return BINT(BgL_tmpz00_7852);
							}
					}
			}
		}

	}



/* display-trace-stack-source */
	BGL_EXPORTED_DEF obj_t
		BGl_displayzd2tracezd2stackzd2sourcezd2zz__errorz00(obj_t BgL_stackz00_118,
		obj_t BgL_portz00_119)
	{
		{	/* Llib/error.scm 1126 */
			{
				obj_t BgL_stackz00_2595;

				BgL_stackz00_2595 = BgL_stackz00_118;
			BgL_zc3z04anonymousza32030ze3z87_2596:
				if (PAIRP(BgL_stackz00_2595))
					{
						obj_t BgL_namez00_2598;
						obj_t BgL_locz00_2599;
						obj_t BgL_restz00_2600;

						{	/* Llib/error.scm 1139 */
							obj_t BgL_ezd2371zd2_2603;

							BgL_ezd2371zd2_2603 = CAR(BgL_stackz00_2595);
							if (PAIRP(BgL_ezd2371zd2_2603))
								{	/* Llib/error.scm 1139 */
									obj_t BgL_cdrzd2379zd2_2605;

									BgL_cdrzd2379zd2_2605 = CDR(BgL_ezd2371zd2_2603);
									if (PAIRP(BgL_cdrzd2379zd2_2605))
										{	/* Llib/error.scm 1139 */
											obj_t BgL_cdrzd2384zd2_2607;

											BgL_cdrzd2384zd2_2607 = CDR(BgL_cdrzd2379zd2_2605);
											{	/* Llib/error.scm 1139 */
												bool_t BgL_test3373z00_7863;

												if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00
													(BgL_cdrzd2384zd2_2607))
													{	/* Llib/error.scm 1013 */
														obj_t BgL_list1935z00_4469;

														BgL_list1935z00_4469 =
															MAKE_YOUNG_PAIR(BgL_cdrzd2384zd2_2607, BNIL);
														BgL_test3373z00_7863 =
															CBOOL(BGl_everyz00zz__r4_pairs_and_lists_6_3z00
															(BGl_pairzf3zd2envz21zz__r4_pairs_and_lists_6_3z00,
																BgL_list1935z00_4469));
													}
												else
													{	/* Llib/error.scm 1013 */
														BgL_test3373z00_7863 = ((bool_t) 0);
													}
												if (BgL_test3373z00_7863)
													{	/* Llib/error.scm 1139 */
														BgL_namez00_2598 = CAR(BgL_ezd2371zd2_2603);
														BgL_locz00_2599 = CAR(BgL_cdrzd2379zd2_2605);
														BgL_restz00_2600 = BgL_cdrzd2384zd2_2607;
														{	/* Llib/error.scm 1142 */
															obj_t BgL_filez00_2611;

															BgL_filez00_2611 =
																BGl_locationzd2linezd2numz00zz__errorz00
																(BgL_locz00_2599);
															{	/* Llib/error.scm 1143 */
																obj_t BgL_lnumz00_2612;
																obj_t BgL_lpointz00_2613;
																obj_t BgL_lstringz00_2614;

																{	/* Llib/error.scm 1145 */
																	obj_t BgL_tmpz00_4447;

																	{	/* Llib/error.scm 1145 */
																		int BgL_tmpz00_7870;

																		BgL_tmpz00_7870 = (int) (1L);
																		BgL_tmpz00_4447 =
																			BGL_MVALUES_VAL(BgL_tmpz00_7870);
																	}
																	{	/* Llib/error.scm 1145 */
																		int BgL_tmpz00_7873;

																		BgL_tmpz00_7873 = (int) (1L);
																		BGL_MVALUES_VAL_SET(BgL_tmpz00_7873,
																			BUNSPEC);
																	}
																	BgL_lnumz00_2612 = BgL_tmpz00_4447;
																}
																{	/* Llib/error.scm 1145 */
																	obj_t BgL_tmpz00_4448;

																	{	/* Llib/error.scm 1145 */
																		int BgL_tmpz00_7876;

																		BgL_tmpz00_7876 = (int) (2L);
																		BgL_tmpz00_4448 =
																			BGL_MVALUES_VAL(BgL_tmpz00_7876);
																	}
																	{	/* Llib/error.scm 1145 */
																		int BgL_tmpz00_7879;

																		BgL_tmpz00_7879 = (int) (2L);
																		BGL_MVALUES_VAL_SET(BgL_tmpz00_7879,
																			BUNSPEC);
																	}
																	BgL_lpointz00_2613 = BgL_tmpz00_4448;
																}
																{	/* Llib/error.scm 1145 */
																	obj_t BgL_tmpz00_4449;

																	{	/* Llib/error.scm 1145 */
																		int BgL_tmpz00_7882;

																		BgL_tmpz00_7882 = (int) (3L);
																		BgL_tmpz00_4449 =
																			BGL_MVALUES_VAL(BgL_tmpz00_7882);
																	}
																	{	/* Llib/error.scm 1145 */
																		int BgL_tmpz00_7885;

																		BgL_tmpz00_7885 = (int) (3L);
																		BGL_MVALUES_VAL_SET(BgL_tmpz00_7885,
																			BUNSPEC);
																	}
																	BgL_lstringz00_2614 = BgL_tmpz00_4449;
																}
																{	/* Llib/error.scm 1145 */
																	bool_t BgL_test3375z00_7888;

																	if (STRINGP(BgL_filez00_2611))
																		{	/* Llib/error.scm 1145 */
																			BgL_test3375z00_7888 =
																				STRINGP(BgL_lstringz00_2614);
																		}
																	else
																		{	/* Llib/error.scm 1145 */
																			BgL_test3375z00_7888 = ((bool_t) 0);
																		}
																	if (BgL_test3375z00_7888)
																		{	/* Llib/error.scm 1129 */
																			obj_t BgL_tabsz00_4450;

																			if (
																				((long) CINT(BgL_lpointz00_2613) > 0L))
																				{	/* Llib/error.scm 1129 */
																					BgL_tabsz00_4450 =
																						make_string(
																						(long) CINT(BgL_lpointz00_2613),
																						((unsigned char) ' '));
																				}
																			else
																				{	/* Llib/error.scm 1129 */
																					BgL_tabsz00_4450 =
																						BGl_string2856z00zz__errorz00;
																				}
																			{	/* Llib/error.scm 1129 */
																				long BgL_lz00_4452;

																				BgL_lz00_4452 =
																					STRING_LENGTH(BgL_lstringz00_2614);
																				{	/* Llib/error.scm 1130 */
																					obj_t BgL_ncolz00_4453;

																					if (
																						((long) CINT(BgL_lpointz00_2613) >=
																							BgL_lz00_4452))
																						{	/* Llib/error.scm 1131 */
																							BgL_ncolz00_4453 =
																								BINT(BgL_lz00_4452);
																						}
																					else
																						{	/* Llib/error.scm 1131 */
																							BgL_ncolz00_4453 =
																								BgL_lpointz00_2613;
																						}
																					{	/* Llib/error.scm 1131 */

																						BGl_fixzd2tabulationz12zc0zz__errorz00
																							(BgL_ncolz00_4453,
																							BgL_lstringz00_2614,
																							BgL_tabsz00_4450);
																						return
																							BGl_printzd2cursorzd2zz__errorz00
																							(BgL_filez00_2611,
																							BgL_lnumz00_2612,
																							BgL_lpointz00_2613,
																							BgL_lstringz00_2614,
																							BgL_tabsz00_4450);
																					}
																				}
																			}
																		}
																	else
																		{	/* Llib/error.scm 1147 */
																			bool_t BgL_test3379z00_7904;

																			if (STRINGP(BgL_filez00_2611))
																				{	/* Llib/error.scm 1147 */
																					BgL_test3379z00_7904 =
																						BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
																						(BgL_lpointz00_2613);
																				}
																			else
																				{	/* Llib/error.scm 1147 */
																					BgL_test3379z00_7904 = ((bool_t) 0);
																				}
																			if (BgL_test3379z00_7904)
																				{	/* Llib/error.scm 1147 */
																					if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BgL_lnumz00_2612))
																						{	/* Llib/error.scm 1149 */
																							obj_t BgL_arg2044z00_2620;
																							obj_t BgL_arg2045z00_2621;

																							{	/* Llib/error.scm 1149 */
																								obj_t BgL_tmpz00_7910;

																								BgL_tmpz00_7910 =
																									BGL_CURRENT_DYNAMIC_ENV();
																								BgL_arg2044z00_2620 =
																									BGL_ENV_CURRENT_ERROR_PORT
																									(BgL_tmpz00_7910);
																							}
																							{	/* Llib/error.scm 1151 */

																								BgL_arg2045z00_2621 =
																									BGl_filenamezd2forzd2errorz00zz__errorz00
																									(BgL_filez00_2611, 255L);
																							}
																							{	/* Llib/error.scm 1149 */
																								obj_t BgL_list2046z00_2622;

																								{	/* Llib/error.scm 1149 */
																									obj_t BgL_arg2047z00_2623;

																									{	/* Llib/error.scm 1149 */
																										obj_t BgL_arg2048z00_2624;

																										BgL_arg2048z00_2624 =
																											MAKE_YOUNG_PAIR
																											(BgL_lpointz00_2613,
																											BNIL);
																										BgL_arg2047z00_2623 =
																											MAKE_YOUNG_PAIR
																											(BgL_lnumz00_2612,
																											BgL_arg2048z00_2624);
																									}
																									BgL_list2046z00_2622 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg2045z00_2621,
																										BgL_arg2047z00_2623);
																								}
																								return
																									BGl_fprintfz00zz__r4_output_6_10_3z00
																									(BgL_arg2044z00_2620,
																									BGl_string2891z00zz__errorz00,
																									BgL_list2046z00_2622);
																							}
																						}
																					else
																						{	/* Llib/error.scm 1154 */
																							obj_t BgL_arg2049z00_2627;
																							obj_t BgL_arg2050z00_2628;

																							{	/* Llib/error.scm 1154 */
																								obj_t BgL_tmpz00_7918;

																								BgL_tmpz00_7918 =
																									BGL_CURRENT_DYNAMIC_ENV();
																								BgL_arg2049z00_2627 =
																									BGL_ENV_CURRENT_ERROR_PORT
																									(BgL_tmpz00_7918);
																							}
																							{	/* Llib/error.scm 1156 */

																								BgL_arg2050z00_2628 =
																									BGl_filenamezd2forzd2errorz00zz__errorz00
																									(BgL_filez00_2611, 255L);
																							}
																							{	/* Llib/error.scm 1154 */
																								obj_t BgL_list2051z00_2629;

																								{	/* Llib/error.scm 1154 */
																									obj_t BgL_arg2052z00_2630;

																									BgL_arg2052z00_2630 =
																										MAKE_YOUNG_PAIR
																										(BgL_lpointz00_2613, BNIL);
																									BgL_list2051z00_2629 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg2050z00_2628,
																										BgL_arg2052z00_2630);
																								}
																								return
																									BGl_fprintfz00zz__r4_output_6_10_3z00
																									(BgL_arg2049z00_2627,
																									BGl_string2892z00zz__errorz00,
																									BgL_list2051z00_2629);
																							}
																						}
																				}
																			else
																				{	/* Llib/error.scm 1159 */
																					obj_t BgL_arg2055z00_2633;

																					BgL_arg2055z00_2633 =
																						CDR(((obj_t) BgL_stackz00_2595));
																					{
																						obj_t BgL_stackz00_7927;

																						BgL_stackz00_7927 =
																							BgL_arg2055z00_2633;
																						BgL_stackz00_2595 =
																							BgL_stackz00_7927;
																						goto
																							BgL_zc3z04anonymousza32030ze3z87_2596;
																					}
																				}
																		}
																}
															}
														}
													}
												else
													{
														obj_t BgL_stackz00_7930;

														BgL_stackz00_7930 = CDR(BgL_stackz00_2595);
														BgL_stackz00_2595 = BgL_stackz00_7930;
														goto BgL_zc3z04anonymousza32030ze3z87_2596;
													}
											}
										}
									else
										{
											obj_t BgL_stackz00_7932;

											BgL_stackz00_7932 = CDR(BgL_stackz00_2595);
											BgL_stackz00_2595 = BgL_stackz00_7932;
											goto BgL_zc3z04anonymousza32030ze3z87_2596;
										}
								}
							else
								{
									obj_t BgL_stackz00_7934;

									BgL_stackz00_7934 = CDR(BgL_stackz00_2595);
									BgL_stackz00_2595 = BgL_stackz00_7934;
									goto BgL_zc3z04anonymousza32030ze3z87_2596;
								}
						}
					}
				else
					{	/* Llib/error.scm 1138 */
						return BFALSE;
					}
			}
		}

	}



/* &display-trace-stack-source */
	obj_t BGl_z62displayzd2tracezd2stackzd2sourcezb0zz__errorz00(obj_t
		BgL_envz00_5364, obj_t BgL_stackz00_5365, obj_t BgL_portz00_5366)
	{
		{	/* Llib/error.scm 1126 */
			{	/* Llib/error.scm 1138 */
				obj_t BgL_auxz00_7936;

				if (OUTPUT_PORTP(BgL_portz00_5366))
					{	/* Llib/error.scm 1138 */
						BgL_auxz00_7936 = BgL_portz00_5366;
					}
				else
					{
						obj_t BgL_auxz00_7939;

						BgL_auxz00_7939 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2811z00zz__errorz00,
							BINT(44711L), BGl_string2893z00zz__errorz00,
							BGl_string2874z00zz__errorz00, BgL_portz00_5366);
						FAILURE(BgL_auxz00_7939, BFALSE, BFALSE);
					}
				return
					BGl_displayzd2tracezd2stackzd2sourcezd2zz__errorz00(BgL_stackz00_5365,
					BgL_auxz00_7936);
			}
		}

	}



/* dump-trace-stack */
	BGL_EXPORTED_DEF obj_t BGl_dumpzd2tracezd2stackz00zz__errorz00(obj_t
		BgL_portz00_120, obj_t BgL_depthz00_121)
	{
		{	/* Llib/error.scm 1166 */
			{	/* Llib/error.scm 1167 */
				obj_t BgL_arg2060z00_4478;

				BgL_arg2060z00_4478 =
					BGl_getzd2tracezd2stackz00zz__errorz00(BgL_depthz00_121);
				{	/* Llib/error.scm 273 */

					return
						BGl_displayzd2tracezd2stackz00zz__errorz00(BgL_arg2060z00_4478,
						BgL_portz00_120, BINT(1L));
				}
			}
		}

	}



/* &dump-trace-stack */
	obj_t BGl_z62dumpzd2tracezd2stackz62zz__errorz00(obj_t BgL_envz00_5367,
		obj_t BgL_portz00_5368, obj_t BgL_depthz00_5369)
	{
		{	/* Llib/error.scm 1166 */
			return
				BGl_dumpzd2tracezd2stackz00zz__errorz00(BgL_portz00_5368,
				BgL_depthz00_5369);
		}

	}



/* fix-tabulation! */
	obj_t BGl_fixzd2tabulationz12zc0zz__errorz00(obj_t BgL_markerz00_122,
		obj_t BgL_srcz00_123, obj_t BgL_dstz00_124)
	{
		{	/* Llib/error.scm 1172 */
			{	/* Llib/error.scm 1173 */
				long BgL_g1136z00_2653;

				BgL_g1136z00_2653 = ((long) CINT(BgL_markerz00_122) - 1L);
				{
					long BgL_readz00_2655;

					BgL_readz00_2655 = BgL_g1136z00_2653;
				BgL_zc3z04anonymousza32061ze3z87_2656:
					if ((BgL_readz00_2655 == -1L))
						{	/* Llib/error.scm 1175 */
							return BGl_symbol2894z00zz__errorz00;
						}
					else
						{	/* Llib/error.scm 1175 */
							if (
								(STRING_REF(
										((obj_t) BgL_srcz00_123),
										BgL_readz00_2655) == ((unsigned char) 9)))
								{	/* Llib/error.scm 1177 */
									STRING_SET(BgL_dstz00_124, BgL_readz00_2655,
										((unsigned char) 9));
									{
										long BgL_readz00_7957;

										BgL_readz00_7957 = (BgL_readz00_2655 - 1L);
										BgL_readz00_2655 = BgL_readz00_7957;
										goto BgL_zc3z04anonymousza32061ze3z87_2656;
									}
								}
							else
								{
									long BgL_readz00_7959;

									BgL_readz00_7959 = (BgL_readz00_2655 - 1L);
									BgL_readz00_2655 = BgL_readz00_7959;
									goto BgL_zc3z04anonymousza32061ze3z87_2656;
								}
						}
				}
			}
		}

	}



/* print-cursor */
	obj_t BGl_printzd2cursorzd2zz__errorz00(obj_t BgL_fnamez00_125,
		obj_t BgL_linez00_126, obj_t BgL_charz00_127, obj_t BgL_stringz00_128,
		obj_t BgL_spacezd2stringzd2_129)
	{
		{	/* Llib/error.scm 1186 */
			{	/* Llib/error.scm 1187 */
				obj_t BgL_arg2069z00_2664;
				obj_t BgL_arg2070z00_2665;

				{	/* Llib/error.scm 1187 */
					obj_t BgL_tmpz00_7961;

					BgL_tmpz00_7961 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg2069z00_2664 = BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_7961);
				}
				{	/* Llib/error.scm 1188 */

					BgL_arg2070z00_2665 =
						BGl_filenamezd2forzd2errorz00zz__errorz00(BgL_fnamez00_125, 255L);
				}
				{	/* Llib/error.scm 1187 */
					obj_t BgL_list2071z00_2666;

					{	/* Llib/error.scm 1187 */
						obj_t BgL_arg2072z00_2667;

						{	/* Llib/error.scm 1187 */
							obj_t BgL_arg2074z00_2668;

							{	/* Llib/error.scm 1187 */
								obj_t BgL_arg2075z00_2669;

								{	/* Llib/error.scm 1187 */
									obj_t BgL_arg2076z00_2670;

									{	/* Llib/error.scm 1187 */
										obj_t BgL_arg2077z00_2671;

										{	/* Llib/error.scm 1187 */
											obj_t BgL_arg2078z00_2672;

											{	/* Llib/error.scm 1187 */
												obj_t BgL_arg2079z00_2673;

												{	/* Llib/error.scm 1187 */
													obj_t BgL_arg2080z00_2674;

													{	/* Llib/error.scm 1187 */
														obj_t BgL_arg2081z00_2675;

														{	/* Llib/error.scm 1187 */
															obj_t BgL_arg2082z00_2676;

															{	/* Llib/error.scm 1187 */
																obj_t BgL_arg2083z00_2677;

																{	/* Llib/error.scm 1187 */
																	obj_t BgL_arg2084z00_2678;

																	{	/* Llib/error.scm 1187 */
																		obj_t BgL_arg2086z00_2679;

																		BgL_arg2086z00_2679 =
																			MAKE_YOUNG_PAIR
																			(BGl_string2896z00zz__errorz00, BNIL);
																		BgL_arg2084z00_2678 =
																			MAKE_YOUNG_PAIR(BgL_spacezd2stringzd2_129,
																			BgL_arg2086z00_2679);
																	}
																	BgL_arg2083z00_2677 =
																		MAKE_YOUNG_PAIR
																		(BGl_string2897z00zz__errorz00,
																		BgL_arg2084z00_2678);
																}
																BgL_arg2082z00_2676 =
																	MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)),
																	BgL_arg2083z00_2677);
															}
															BgL_arg2081z00_2675 =
																MAKE_YOUNG_PAIR(BgL_stringz00_128,
																BgL_arg2082z00_2676);
														}
														BgL_arg2080z00_2674 =
															MAKE_YOUNG_PAIR(BGl_string2897z00zz__errorz00,
															BgL_arg2081z00_2675);
													}
													BgL_arg2079z00_2673 =
														MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)),
														BgL_arg2080z00_2674);
												}
												BgL_arg2078z00_2672 =
													MAKE_YOUNG_PAIR(BGl_string2824z00zz__errorz00,
													BgL_arg2079z00_2673);
											}
											BgL_arg2077z00_2671 =
												MAKE_YOUNG_PAIR(BgL_charz00_127, BgL_arg2078z00_2672);
										}
										BgL_arg2076z00_2670 =
											MAKE_YOUNG_PAIR(BGl_string2898z00zz__errorz00,
											BgL_arg2077z00_2671);
									}
									BgL_arg2075z00_2669 =
										MAKE_YOUNG_PAIR(BgL_linez00_126, BgL_arg2076z00_2670);
								}
								BgL_arg2074z00_2668 =
									MAKE_YOUNG_PAIR(BGl_string2899z00zz__errorz00,
									BgL_arg2075z00_2669);
							}
							BgL_arg2072z00_2667 =
								MAKE_YOUNG_PAIR(BgL_arg2070z00_2665, BgL_arg2074z00_2668);
						}
						BgL_list2071z00_2666 =
							MAKE_YOUNG_PAIR(BGl_string2854z00zz__errorz00,
							BgL_arg2072z00_2667);
					}
					return
						BGl_fprintz00zz__r4_output_6_10_3z00(BgL_arg2069z00_2664,
						BgL_list2071z00_2666);
				}
			}
		}

	}



/* relative-file-name */
	obj_t BGl_relativezd2filezd2namez00zz__errorz00(obj_t BgL_fnamez00_130)
	{
		{	/* Llib/error.scm 1201 */
			{	/* Llib/error.scm 1202 */
				obj_t BgL_pwdz00_2682;
				obj_t BgL_dnamez00_2683;

				BgL_pwdz00_2682 = BGl_pwdz00zz__osz00();
				BgL_dnamez00_2683 = BGl_dirnamez00zz__osz00(BgL_fnamez00_130);
				{	/* Llib/error.scm 1204 */
					bool_t BgL_test3385z00_7984;

					if (STRINGP(BgL_pwdz00_2682))
						{	/* Llib/error.scm 1205 */
							bool_t BgL_test3387z00_7987;

							{	/* Llib/error.scm 1205 */
								long BgL_l1z00_4496;

								BgL_l1z00_4496 = STRING_LENGTH(BgL_dnamez00_2683);
								if ((BgL_l1z00_4496 == 1L))
									{	/* Llib/error.scm 1205 */
										int BgL_arg2321z00_4499;

										{	/* Llib/error.scm 1205 */
											char *BgL_auxz00_7993;
											char *BgL_tmpz00_7991;

											BgL_auxz00_7993 =
												BSTRING_TO_STRING(BGl_string2888z00zz__errorz00);
											BgL_tmpz00_7991 = BSTRING_TO_STRING(BgL_dnamez00_2683);
											BgL_arg2321z00_4499 =
												memcmp(BgL_tmpz00_7991, BgL_auxz00_7993,
												BgL_l1z00_4496);
										}
										BgL_test3387z00_7987 = ((long) (BgL_arg2321z00_4499) == 0L);
									}
								else
									{	/* Llib/error.scm 1205 */
										BgL_test3387z00_7987 = ((bool_t) 0);
									}
							}
							if (BgL_test3387z00_7987)
								{	/* Llib/error.scm 1205 */
									BgL_test3385z00_7984 = ((bool_t) 1);
								}
							else
								{	/* Llib/error.scm 1205 */
									if (
										(STRING_REF(
												((obj_t) BgL_fnamez00_130),
												0L) == ((unsigned char) '/')))
										{	/* Llib/error.scm 1206 */
											BgL_test3385z00_7984 = ((bool_t) 0);
										}
									else
										{	/* Llib/error.scm 1206 */
											BgL_test3385z00_7984 = ((bool_t) 1);
										}
								}
						}
					else
						{	/* Llib/error.scm 1204 */
							BgL_test3385z00_7984 = ((bool_t) 1);
						}
					if (BgL_test3385z00_7984)
						{	/* Llib/error.scm 1204 */
							return BgL_fnamez00_130;
						}
					else
						{	/* Llib/error.scm 1209 */
							obj_t BgL_originalzd2cmpzd2pathz00_2690;

							BgL_originalzd2cmpzd2pathz00_2690 =
								BGl_dirnamezd2ze3listz31zz__errorz00(BgL_dnamez00_2683);
							{	/* Llib/error.scm 1210 */
								obj_t BgL_g1137z00_2691;

								BgL_g1137z00_2691 =
									BGl_dirnamezd2ze3listz31zz__errorz00(BgL_pwdz00_2682);
								{
									obj_t BgL_cmpzd2pathzd2_2693;
									obj_t BgL_curzd2pathzd2_2694;

									BgL_cmpzd2pathzd2_2693 = BgL_originalzd2cmpzd2pathz00_2690;
									BgL_curzd2pathzd2_2694 = BgL_g1137z00_2691;
								BgL_zc3z04anonymousza32093ze3z87_2695:
									if (NULLP(BgL_cmpzd2pathzd2_2693))
										{	/* Llib/error.scm 1213 */
											if (NULLP(BgL_curzd2pathzd2_2694))
												{	/* Llib/error.scm 1214 */
													return BGl_basenamez00zz__osz00(BgL_fnamez00_130);
												}
											else
												{	/* Llib/error.scm 1217 */
													long BgL_g1138z00_2698;
													obj_t BgL_g1139z00_2699;

													BgL_g1138z00_2698 =
														bgl_list_length(BgL_curzd2pathzd2_2694);
													BgL_g1139z00_2699 =
														BGl_basenamez00zz__osz00(BgL_fnamez00_130);
													{
														long BgL_lenz00_4523;
														obj_t BgL_resz00_4524;

														BgL_lenz00_4523 = BgL_g1138z00_2698;
														BgL_resz00_4524 = BgL_g1139z00_2699;
													BgL_loopz00_4522:
														if ((BgL_lenz00_4523 == 0L))
															{	/* Llib/error.scm 1219 */
																return BgL_resz00_4524;
															}
														else
															{	/* Llib/error.scm 1221 */
																long BgL_arg2098z00_4531;
																obj_t BgL_arg2099z00_4532;

																BgL_arg2098z00_4531 = (BgL_lenz00_4523 - 1L);
																{	/* Llib/error.scm 1221 */
																	obj_t BgL_list2100z00_4533;

																	{	/* Llib/error.scm 1221 */
																		obj_t BgL_arg2101z00_4534;

																		BgL_arg2101z00_4534 =
																			MAKE_YOUNG_PAIR(BgL_resz00_4524, BNIL);
																		BgL_list2100z00_4533 =
																			MAKE_YOUNG_PAIR
																			(BGl_string2900z00zz__errorz00,
																			BgL_arg2101z00_4534);
																	}
																	BgL_arg2099z00_4532 =
																		BGl_stringzd2appendzd2zz__r4_strings_6_7z00
																		(BgL_list2100z00_4533);
																}
																{
																	obj_t BgL_resz00_8018;
																	long BgL_lenz00_8017;

																	BgL_lenz00_8017 = BgL_arg2098z00_4531;
																	BgL_resz00_8018 = BgL_arg2099z00_4532;
																	BgL_resz00_4524 = BgL_resz00_8018;
																	BgL_lenz00_4523 = BgL_lenz00_8017;
																	goto BgL_loopz00_4522;
																}
															}
													}
												}
										}
									else
										{	/* Llib/error.scm 1213 */
											if (NULLP(BgL_curzd2pathzd2_2694))
												{	/* Llib/error.scm 1223 */
													obj_t BgL_g1140z00_2711;
													obj_t BgL_g1141z00_2712;

													BgL_g1140z00_2711 =
														bgl_reverse_bang(BgL_cmpzd2pathzd2_2693);
													BgL_g1141z00_2712 =
														BGl_basenamez00zz__osz00(BgL_fnamez00_130);
													{
														obj_t BgL_pathz00_4556;
														obj_t BgL_resz00_4557;

														BgL_pathz00_4556 = BgL_g1140z00_2711;
														BgL_resz00_4557 = BgL_g1141z00_2712;
													BgL_loopz00_4555:
														if (NULLP(BgL_pathz00_4556))
															{	/* Llib/error.scm 1225 */
																return BgL_resz00_4557;
															}
														else
															{	/* Llib/error.scm 1227 */
																obj_t BgL_arg2105z00_4566;
																obj_t BgL_arg2106z00_4567;

																BgL_arg2105z00_4566 =
																	CDR(((obj_t) BgL_pathz00_4556));
																{	/* Llib/error.scm 1228 */
																	obj_t BgL_arg2107z00_4568;

																	BgL_arg2107z00_4568 =
																		CAR(((obj_t) BgL_pathz00_4556));
																	{	/* Llib/error.scm 1228 */
																		obj_t BgL_list2108z00_4569;

																		{	/* Llib/error.scm 1228 */
																			obj_t BgL_arg2109z00_4570;

																			{	/* Llib/error.scm 1228 */
																				obj_t BgL_arg2110z00_4571;

																				BgL_arg2110z00_4571 =
																					MAKE_YOUNG_PAIR(BgL_resz00_4557,
																					BNIL);
																				BgL_arg2109z00_4570 =
																					MAKE_YOUNG_PAIR
																					(BGl_string2901z00zz__errorz00,
																					BgL_arg2110z00_4571);
																			}
																			BgL_list2108z00_4569 =
																				MAKE_YOUNG_PAIR(BgL_arg2107z00_4568,
																				BgL_arg2109z00_4570);
																		}
																		BgL_arg2106z00_4567 =
																			BGl_stringzd2appendzd2zz__r4_strings_6_7z00
																			(BgL_list2108z00_4569);
																	}
																}
																{
																	obj_t BgL_resz00_8034;
																	obj_t BgL_pathz00_8033;

																	BgL_pathz00_8033 = BgL_arg2105z00_4566;
																	BgL_resz00_8034 = BgL_arg2106z00_4567;
																	BgL_resz00_4557 = BgL_resz00_8034;
																	BgL_pathz00_4556 = BgL_pathz00_8033;
																	goto BgL_loopz00_4555;
																}
															}
													}
												}
											else
												{	/* Llib/error.scm 1229 */
													bool_t BgL_test3395z00_8035;

													{	/* Llib/error.scm 1229 */
														obj_t BgL_arg2134z00_2758;
														obj_t BgL_arg2135z00_2759;

														BgL_arg2134z00_2758 =
															CAR(((obj_t) BgL_curzd2pathzd2_2694));
														BgL_arg2135z00_2759 =
															CAR(((obj_t) BgL_cmpzd2pathzd2_2693));
														{	/* Llib/error.scm 1229 */
															long BgL_l1z00_4578;

															BgL_l1z00_4578 =
																STRING_LENGTH(((obj_t) BgL_arg2134z00_2758));
															if (
																(BgL_l1z00_4578 ==
																	STRING_LENGTH(((obj_t) BgL_arg2135z00_2759))))
																{	/* Llib/error.scm 1229 */
																	int BgL_arg2321z00_4581;

																	{	/* Llib/error.scm 1229 */
																		char *BgL_auxz00_8049;
																		char *BgL_tmpz00_8046;

																		BgL_auxz00_8049 =
																			BSTRING_TO_STRING(
																			((obj_t) BgL_arg2135z00_2759));
																		BgL_tmpz00_8046 =
																			BSTRING_TO_STRING(
																			((obj_t) BgL_arg2134z00_2758));
																		BgL_arg2321z00_4581 =
																			memcmp(BgL_tmpz00_8046, BgL_auxz00_8049,
																			BgL_l1z00_4578);
																	}
																	BgL_test3395z00_8035 =
																		((long) (BgL_arg2321z00_4581) == 0L);
																}
															else
																{	/* Llib/error.scm 1229 */
																	BgL_test3395z00_8035 = ((bool_t) 0);
																}
														}
													}
													if (BgL_test3395z00_8035)
														{	/* Llib/error.scm 1230 */
															obj_t BgL_arg2114z00_2728;
															obj_t BgL_arg2115z00_2729;

															BgL_arg2114z00_2728 =
																CDR(((obj_t) BgL_cmpzd2pathzd2_2693));
															BgL_arg2115z00_2729 =
																CDR(((obj_t) BgL_curzd2pathzd2_2694));
															{
																obj_t BgL_curzd2pathzd2_8060;
																obj_t BgL_cmpzd2pathzd2_8059;

																BgL_cmpzd2pathzd2_8059 = BgL_arg2114z00_2728;
																BgL_curzd2pathzd2_8060 = BgL_arg2115z00_2729;
																BgL_curzd2pathzd2_2694 = BgL_curzd2pathzd2_8060;
																BgL_cmpzd2pathzd2_2693 = BgL_cmpzd2pathzd2_8059;
																goto BgL_zc3z04anonymousza32093ze3z87_2695;
															}
														}
													else
														{	/* Llib/error.scm 1232 */
															obj_t BgL_g1142z00_2730;
															obj_t BgL_g1143z00_2731;

															BgL_g1142z00_2730 =
																bgl_reverse(BgL_cmpzd2pathzd2_2693);
															BgL_g1143z00_2731 =
																BGl_basenamez00zz__osz00(BgL_fnamez00_130);
															{
																obj_t BgL_pathz00_2733;
																obj_t BgL_resz00_2734;

																BgL_pathz00_2733 = BgL_g1142z00_2730;
																BgL_resz00_2734 = BgL_g1143z00_2731;
															BgL_zc3z04anonymousza32116ze3z87_2735:
																if (NULLP(BgL_pathz00_2733))
																	{	/* Llib/error.scm 1234 */
																		if (
																			(BgL_cmpzd2pathzd2_2693 ==
																				BgL_originalzd2cmpzd2pathz00_2690))
																			{	/* Llib/error.scm 1236 */
																				obj_t BgL_list2119z00_2738;

																				{	/* Llib/error.scm 1236 */
																					obj_t BgL_arg2120z00_2739;

																					BgL_arg2120z00_2739 =
																						MAKE_YOUNG_PAIR(BgL_resz00_2734,
																						BNIL);
																					BgL_list2119z00_2738 =
																						MAKE_YOUNG_PAIR
																						(BGl_string2901z00zz__errorz00,
																						BgL_arg2120z00_2739);
																				}
																				BGL_TAIL return
																					BGl_stringzd2appendzd2zz__r4_strings_6_7z00
																					(BgL_list2119z00_2738);
																			}
																		else
																			{	/* Llib/error.scm 1237 */
																				long BgL_g1144z00_2740;

																				BgL_g1144z00_2740 =
																					bgl_list_length
																					(BgL_curzd2pathzd2_2694);
																				{
																					long BgL_lenz00_4604;
																					obj_t BgL_resz00_4605;

																					BgL_lenz00_4604 = BgL_g1144z00_2740;
																					BgL_resz00_4605 = BgL_resz00_2734;
																				BgL_loopz00_4603:
																					if ((BgL_lenz00_4604 == 0L))
																						{	/* Llib/error.scm 1239 */
																							return BgL_resz00_4605;
																						}
																					else
																						{	/* Llib/error.scm 1241 */
																							long BgL_arg2123z00_4612;
																							obj_t BgL_arg2124z00_4613;

																							BgL_arg2123z00_4612 =
																								(BgL_lenz00_4604 - 1L);
																							{	/* Llib/error.scm 1242 */
																								obj_t BgL_list2125z00_4614;

																								{	/* Llib/error.scm 1242 */
																									obj_t BgL_arg2126z00_4615;

																									BgL_arg2126z00_4615 =
																										MAKE_YOUNG_PAIR
																										(BgL_resz00_4605, BNIL);
																									BgL_list2125z00_4614 =
																										MAKE_YOUNG_PAIR
																										(BGl_string2900z00zz__errorz00,
																										BgL_arg2126z00_4615);
																								}
																								BgL_arg2124z00_4613 =
																									BGl_stringzd2appendzd2zz__r4_strings_6_7z00
																									(BgL_list2125z00_4614);
																							}
																							{
																								obj_t BgL_resz00_8078;
																								long BgL_lenz00_8077;

																								BgL_lenz00_8077 =
																									BgL_arg2123z00_4612;
																								BgL_resz00_8078 =
																									BgL_arg2124z00_4613;
																								BgL_resz00_4605 =
																									BgL_resz00_8078;
																								BgL_lenz00_4604 =
																									BgL_lenz00_8077;
																								goto BgL_loopz00_4603;
																							}
																						}
																				}
																			}
																	}
																else
																	{	/* Llib/error.scm 1243 */
																		obj_t BgL_arg2127z00_2751;
																		obj_t BgL_arg2129z00_2752;

																		BgL_arg2127z00_2751 =
																			CDR(((obj_t) BgL_pathz00_2733));
																		{	/* Llib/error.scm 1244 */
																			obj_t BgL_arg2130z00_2753;

																			BgL_arg2130z00_2753 =
																				CAR(((obj_t) BgL_pathz00_2733));
																			{	/* Llib/error.scm 1244 */
																				obj_t BgL_list2131z00_2754;

																				{	/* Llib/error.scm 1244 */
																					obj_t BgL_arg2132z00_2755;

																					{	/* Llib/error.scm 1244 */
																						obj_t BgL_arg2133z00_2756;

																						BgL_arg2133z00_2756 =
																							MAKE_YOUNG_PAIR(BgL_resz00_2734,
																							BNIL);
																						BgL_arg2132z00_2755 =
																							MAKE_YOUNG_PAIR
																							(BGl_string2901z00zz__errorz00,
																							BgL_arg2133z00_2756);
																					}
																					BgL_list2131z00_2754 =
																						MAKE_YOUNG_PAIR(BgL_arg2130z00_2753,
																						BgL_arg2132z00_2755);
																				}
																				BgL_arg2129z00_2752 =
																					BGl_stringzd2appendzd2zz__r4_strings_6_7z00
																					(BgL_list2131z00_2754);
																			}
																		}
																		{
																			obj_t BgL_resz00_8088;
																			obj_t BgL_pathz00_8087;

																			BgL_pathz00_8087 = BgL_arg2127z00_2751;
																			BgL_resz00_8088 = BgL_arg2129z00_2752;
																			BgL_resz00_2734 = BgL_resz00_8088;
																			BgL_pathz00_2733 = BgL_pathz00_8087;
																			goto
																				BgL_zc3z04anonymousza32116ze3z87_2735;
																		}
																	}
															}
														}
												}
										}
								}
							}
						}
				}
			}
		}

	}



/* uncygdrive */
	obj_t BGl_uncygdrivez00zz__errorz00(obj_t BgL_strz00_131)
	{
		{	/* Llib/error.scm 1252 */
			if (bigloo_strncmp(BGl_string2902z00zz__errorz00, BgL_strz00_131, 10L))
				{	/* Llib/error.scm 1254 */
					bool_t BgL_test3401z00_8091;

					if ((STRING_LENGTH(BgL_strz00_131) > 12L))
						{	/* Llib/error.scm 1255 */
							bool_t BgL_test3403z00_8095;

							{	/* Llib/error.scm 1255 */
								unsigned char BgL_tmpz00_8096;

								BgL_tmpz00_8096 = STRING_REF(BgL_strz00_131, 10L);
								BgL_test3403z00_8095 = isalpha(BgL_tmpz00_8096);
							}
							if (BgL_test3403z00_8095)
								{	/* Llib/error.scm 1255 */
									BgL_test3401z00_8091 =
										(STRING_REF(BgL_strz00_131, 11L) == ((unsigned char) '/'));
								}
							else
								{	/* Llib/error.scm 1255 */
									BgL_test3401z00_8091 = ((bool_t) 0);
								}
						}
					else
						{	/* Llib/error.scm 1254 */
							BgL_test3401z00_8091 = ((bool_t) 0);
						}
					if (BgL_test3401z00_8091)
						{	/* Llib/error.scm 1257 */
							obj_t BgL_arg2147z00_2774;
							obj_t BgL_arg2148z00_2775;

							{	/* Llib/error.scm 1257 */
								unsigned char BgL_arg2151z00_2778;

								BgL_arg2151z00_2778 = STRING_REF(BgL_strz00_131, 10L);
								{	/* Llib/error.scm 1257 */
									obj_t BgL_list2152z00_2779;

									{	/* Llib/error.scm 1257 */
										obj_t BgL_arg2154z00_2780;

										{	/* Llib/error.scm 1257 */
											obj_t BgL_arg2155z00_2781;

											BgL_arg2155z00_2781 =
												MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '/')), BNIL);
											BgL_arg2154z00_2780 =
												MAKE_YOUNG_PAIR(BCHAR(((unsigned char) ':')),
												BgL_arg2155z00_2781);
										}
										BgL_list2152z00_2779 =
											MAKE_YOUNG_PAIR(BCHAR(BgL_arg2151z00_2778),
											BgL_arg2154z00_2780);
									}
									BgL_arg2147z00_2774 =
										BGl_listzd2ze3stringz31zz__r4_strings_6_7z00
										(BgL_list2152z00_2779);
							}}
							BgL_arg2148z00_2775 =
								BGl_substringz00zz__r4_strings_6_7z00(BgL_strz00_131, 12L,
								STRING_LENGTH(BgL_strz00_131));
							{	/* Llib/error.scm 1257 */
								obj_t BgL_list2149z00_2776;

								{	/* Llib/error.scm 1257 */
									obj_t BgL_arg2150z00_2777;

									BgL_arg2150z00_2777 =
										MAKE_YOUNG_PAIR(BgL_arg2148z00_2775, BNIL);
									BgL_list2149z00_2776 =
										MAKE_YOUNG_PAIR(BgL_arg2147z00_2774, BgL_arg2150z00_2777);
								}
								return
									BGl_stringzd2appendzd2zz__r4_strings_6_7z00
									(BgL_list2149z00_2776);
							}
						}
					else
						{	/* Llib/error.scm 1254 */
							return BgL_strz00_131;
						}
				}
			else
				{	/* Llib/error.scm 1253 */
					return BgL_strz00_131;
				}
		}

	}



/* dirname->list */
	obj_t BGl_dirnamezd2ze3listz31zz__errorz00(obj_t BgL_pathz00_132)
	{
		{	/* Llib/error.scm 1265 */
			{	/* Llib/error.scm 1266 */
				long BgL_lenz00_2788;
				long BgL_initz00_2789;

				{	/* Llib/error.scm 1266 */
					long BgL_lenz00_2811;

					BgL_lenz00_2811 = STRING_LENGTH(((obj_t) BgL_pathz00_132));
					if (
						(STRING_REF(
								((obj_t) BgL_pathz00_132),
								(BgL_lenz00_2811 - 1L)) == (unsigned char) (FILE_SEPARATOR)))
						{	/* Llib/error.scm 1267 */
							BgL_lenz00_2788 = (BgL_lenz00_2811 - 1L);
						}
					else
						{	/* Llib/error.scm 1267 */
							BgL_lenz00_2788 = BgL_lenz00_2811;
						}
				}
				if (
					(STRING_REF(
							((obj_t) BgL_pathz00_132), 0L) ==
						(unsigned char) (FILE_SEPARATOR)))
					{	/* Llib/error.scm 1270 */
						BgL_initz00_2789 = 1L;
					}
				else
					{	/* Llib/error.scm 1270 */
						BgL_initz00_2789 = 0L;
					}
				{	/* Llib/error.scm 1271 */
					bool_t BgL_test3406z00_8128;

					{	/* Llib/error.scm 1271 */
						long BgL_l1z00_4643;

						BgL_l1z00_4643 = STRING_LENGTH(((obj_t) BgL_pathz00_132));
						if ((BgL_l1z00_4643 == 1L))
							{	/* Llib/error.scm 1271 */
								int BgL_arg2321z00_4646;

								{	/* Llib/error.scm 1271 */
									char *BgL_auxz00_8136;
									char *BgL_tmpz00_8133;

									BgL_auxz00_8136 =
										BSTRING_TO_STRING(BGl_string2901z00zz__errorz00);
									BgL_tmpz00_8133 =
										BSTRING_TO_STRING(((obj_t) BgL_pathz00_132));
									BgL_arg2321z00_4646 =
										memcmp(BgL_tmpz00_8133, BgL_auxz00_8136, BgL_l1z00_4643);
								}
								BgL_test3406z00_8128 = ((long) (BgL_arg2321z00_4646) == 0L);
							}
						else
							{	/* Llib/error.scm 1271 */
								BgL_test3406z00_8128 = ((bool_t) 0);
							}
					}
					if (BgL_test3406z00_8128)
						{	/* Llib/error.scm 1271 */
							return BNIL;
						}
					else
						{
							long BgL_readz00_2793;
							long BgL_prevz00_2794;
							obj_t BgL_listz00_2795;

							BgL_readz00_2793 = BgL_initz00_2789;
							BgL_prevz00_2794 = BgL_initz00_2789;
							BgL_listz00_2795 = BNIL;
						BgL_zc3z04anonymousza32161ze3z87_2796:
							if ((BgL_readz00_2793 == BgL_lenz00_2788))
								{	/* Llib/error.scm 1278 */
									obj_t BgL_arg2163z00_2798;

									BgL_arg2163z00_2798 =
										MAKE_YOUNG_PAIR(BGl_substringz00zz__r4_strings_6_7z00
										(BgL_pathz00_132, BgL_prevz00_2794, BgL_readz00_2793),
										BgL_listz00_2795);
									return bgl_reverse_bang(BgL_arg2163z00_2798);
								}
							else
								{	/* Llib/error.scm 1277 */
									if (
										(STRING_REF(
												((obj_t) BgL_pathz00_132), BgL_readz00_2793) ==
											(unsigned char) (FILE_SEPARATOR)))
										{	/* Llib/error.scm 1280 */
											long BgL_arg2168z00_2803;
											long BgL_arg2169z00_2804;
											obj_t BgL_arg2170z00_2805;

											BgL_arg2168z00_2803 = (BgL_readz00_2793 + 1L);
											BgL_arg2169z00_2804 = (BgL_readz00_2793 + 1L);
											BgL_arg2170z00_2805 =
												MAKE_YOUNG_PAIR(BGl_substringz00zz__r4_strings_6_7z00
												(BgL_pathz00_132, BgL_prevz00_2794, BgL_readz00_2793),
												BgL_listz00_2795);
											{
												obj_t BgL_listz00_8157;
												long BgL_prevz00_8156;
												long BgL_readz00_8155;

												BgL_readz00_8155 = BgL_arg2168z00_2803;
												BgL_prevz00_8156 = BgL_arg2169z00_2804;
												BgL_listz00_8157 = BgL_arg2170z00_2805;
												BgL_listz00_2795 = BgL_listz00_8157;
												BgL_prevz00_2794 = BgL_prevz00_8156;
												BgL_readz00_2793 = BgL_readz00_8155;
												goto BgL_zc3z04anonymousza32161ze3z87_2796;
											}
										}
									else
										{
											long BgL_readz00_8158;

											BgL_readz00_8158 = (BgL_readz00_2793 + 1L);
											BgL_readz00_2793 = BgL_readz00_8158;
											goto BgL_zc3z04anonymousza32161ze3z87_2796;
										}
								}
						}
				}
			}
		}

	}



/* typeof */
	BGL_EXPORTED_DEF obj_t bgl_typeof(obj_t BgL_objz00_133)
	{
		{	/* Llib/error.scm 1292 */
			if (INTEGERP(BgL_objz00_133))
				{	/* Llib/error.scm 1294 */
					return BGl_string2813z00zz__errorz00;
				}
			else
				{	/* Llib/error.scm 1294 */
					if (REALP(BgL_objz00_133))
						{	/* Llib/error.scm 1296 */
							return BGl_string2903z00zz__errorz00;
						}
					else
						{	/* Llib/error.scm 1296 */
							if (STRINGP(BgL_objz00_133))
								{	/* Llib/error.scm 1298 */
									return BGl_string2838z00zz__errorz00;
								}
							else
								{	/* Llib/error.scm 1298 */
									if (SYMBOLP(BgL_objz00_133))
										{	/* Llib/error.scm 1300 */
											return BGl_string2904z00zz__errorz00;
										}
									else
										{	/* Llib/error.scm 1300 */
											if (KEYWORDP(BgL_objz00_133))
												{	/* Llib/error.scm 1302 */
													return BGl_string2905z00zz__errorz00;
												}
											else
												{	/* Llib/error.scm 1302 */
													if (CHARP(BgL_objz00_133))
														{	/* Llib/error.scm 1304 */
															return BGl_string2906z00zz__errorz00;
														}
													else
														{	/* Llib/error.scm 1304 */
															if (BOOLEANP(BgL_objz00_133))
																{	/* Llib/error.scm 1306 */
																	return BGl_string2907z00zz__errorz00;
																}
															else
																{	/* Llib/error.scm 1306 */
																	if (NULLP(BgL_objz00_133))
																		{	/* Llib/error.scm 1308 */
																			return BGl_string2908z00zz__errorz00;
																		}
																	else
																		{	/* Llib/error.scm 1308 */
																			if ((BgL_objz00_133 == BUNSPEC))
																				{	/* Llib/error.scm 1310 */
																					return BGl_string2909z00zz__errorz00;
																				}
																			else
																				{	/* Llib/error.scm 1310 */
																					if (EPAIRP(BgL_objz00_133))
																						{	/* Llib/error.scm 1312 */
																							return
																								BGl_string2910z00zz__errorz00;
																						}
																					else
																						{	/* Llib/error.scm 1312 */
																							if (PAIRP(BgL_objz00_133))
																								{	/* Llib/error.scm 1314 */
																									return
																										BGl_string2911z00zz__errorz00;
																								}
																							else
																								{	/* Llib/error.scm 1314 */
																									if (BGl_classzf3zf3zz__objectz00(BgL_objz00_133))
																										{	/* Llib/error.scm 1316 */
																											return
																												BGl_string2912z00zz__errorz00;
																										}
																									else
																										{	/* Llib/error.scm 1316 */
																											if (VECTORP
																												(BgL_objz00_133))
																												{	/* Llib/error.scm 1318 */
																													return
																														BGl_string2913z00zz__errorz00;
																												}
																											else
																												{	/* Llib/error.scm 1318 */
																													if (TVECTORP
																														(BgL_objz00_133))
																														{	/* Llib/error.scm 1320 */
																															return
																																BGl_string2914z00zz__errorz00;
																														}
																													else
																														{	/* Llib/error.scm 1320 */
																															if (STRUCTP
																																(BgL_objz00_133))
																																{	/* Llib/error.scm 1323 */
																																	obj_t
																																		BgL_arg2202z00_2839;
																																	{	/* Llib/error.scm 1323 */
																																		obj_t
																																			BgL_arg2205z00_2842;
																																		BgL_arg2205z00_2842
																																			=
																																			STRUCT_KEY
																																			(BgL_objz00_133);
																																		{	/* Llib/error.scm 1323 */
																																			obj_t
																																				BgL_arg2328z00_4664;
																																			BgL_arg2328z00_4664
																																				=
																																				SYMBOL_TO_STRING
																																				(BgL_arg2205z00_2842);
																																			BgL_arg2202z00_2839
																																				=
																																				BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																				(BgL_arg2328z00_4664);
																																		}
																																	}
																																	{	/* Llib/error.scm 1323 */
																																		obj_t
																																			BgL_list2203z00_2840;
																																		{	/* Llib/error.scm 1323 */
																																			obj_t
																																				BgL_arg2204z00_2841;
																																			BgL_arg2204z00_2841
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg2202z00_2839,
																																				BNIL);
																																			BgL_list2203z00_2840
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_string2915z00zz__errorz00,
																																				BgL_arg2204z00_2841);
																																		}
																																		BGL_TAIL
																																			return
																																			BGl_stringzd2appendzd2zz__r4_strings_6_7z00
																																			(BgL_list2203z00_2840);
																																	}
																																}
																															else
																																{	/* Llib/error.scm 1322 */
																																	if (PROCEDUREP
																																		(BgL_objz00_133))
																																		{	/* Llib/error.scm 1324 */
																																			return
																																				BGl_string2821z00zz__errorz00;
																																		}
																																	else
																																		{	/* Llib/error.scm 1324 */
																																			if (INPUT_PORTP(BgL_objz00_133))
																																				{	/* Llib/error.scm 1326 */
																																					return
																																						BGl_string2916z00zz__errorz00;
																																				}
																																			else
																																				{	/* Llib/error.scm 1326 */
																																					if (OUTPUT_PORTP(BgL_objz00_133))
																																						{	/* Llib/error.scm 1328 */
																																							return
																																								BGl_string2874z00zz__errorz00;
																																						}
																																					else
																																						{	/* Llib/error.scm 1328 */
																																							if (BINARY_PORTP(BgL_objz00_133))
																																								{	/* Llib/error.scm 1330 */
																																									return
																																										BGl_string2917z00zz__errorz00;
																																								}
																																							else
																																								{	/* Llib/error.scm 1330 */
																																									if (CELLP(BgL_objz00_133))
																																										{	/* Llib/error.scm 1332 */
																																											return
																																												BGl_string2918z00zz__errorz00;
																																										}
																																									else
																																										{	/* Llib/error.scm 1332 */
																																											if (FOREIGNP(BgL_objz00_133))
																																												{	/* Llib/error.scm 1335 */
																																													obj_t
																																														BgL_arg2212z00_2849;
																																													{	/* Llib/error.scm 1335 */
																																														obj_t
																																															BgL_arg2217z00_2852;
																																														BgL_arg2217z00_2852
																																															=
																																															FOREIGN_ID
																																															(BgL_objz00_133);
																																														{	/* Llib/error.scm 1335 */
																																															obj_t
																																																BgL_arg2328z00_4666;
																																															BgL_arg2328z00_4666
																																																=
																																																SYMBOL_TO_STRING
																																																(BgL_arg2217z00_2852);
																																															BgL_arg2212z00_2849
																																																=
																																																BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																																(BgL_arg2328z00_4666);
																																														}
																																													}
																																													{	/* Llib/error.scm 1335 */
																																														obj_t
																																															BgL_list2215z00_2850;
																																														{	/* Llib/error.scm 1335 */
																																															obj_t
																																																BgL_arg2216z00_2851;
																																															BgL_arg2216z00_2851
																																																=
																																																MAKE_YOUNG_PAIR
																																																(BgL_arg2212z00_2849,
																																																BNIL);
																																															BgL_list2215z00_2850
																																																=
																																																MAKE_YOUNG_PAIR
																																																(BGl_string2919z00zz__errorz00,
																																																BgL_arg2216z00_2851);
																																														}
																																														BGL_TAIL
																																															return
																																															BGl_stringzd2appendzd2zz__r4_strings_6_7z00
																																															(BgL_list2215z00_2850);
																																													}
																																												}
																																											else
																																												{	/* Llib/error.scm 1334 */
																																													if (SOCKETP(BgL_objz00_133))
																																														{	/* Llib/error.scm 1336 */
																																															return
																																																BGl_string2920z00zz__errorz00;
																																														}
																																													else
																																														{	/* Llib/error.scm 1336 */
																																															if (BGL_DATAGRAM_SOCKETP(BgL_objz00_133))
																																																{	/* Llib/error.scm 1338 */
																																																	return
																																																		BGl_string2921z00zz__errorz00;
																																																}
																																															else
																																																{	/* Llib/error.scm 1338 */
																																																	if (PROCESSP(BgL_objz00_133))
																																																		{	/* Llib/error.scm 1340 */
																																																			return
																																																				BGl_string2922z00zz__errorz00;
																																																		}
																																																	else
																																																		{	/* Llib/error.scm 1340 */
																																																			if (CUSTOMP(BgL_objz00_133))
																																																				{	/* Llib/error.scm 1342 */
																																																					return
																																																						BGl_string2923z00zz__errorz00;
																																																				}
																																																			else
																																																				{	/* Llib/error.scm 1342 */
																																																					if (OPAQUEP(BgL_objz00_133))
																																																						{	/* Llib/error.scm 1344 */
																																																							return
																																																								BGl_string2924z00zz__errorz00;
																																																						}
																																																					else
																																																						{	/* Llib/error.scm 1344 */
																																																							if (BGL_OBJECTP(BgL_objz00_133))
																																																								{	/* Llib/error.scm 1347 */
																																																									obj_t
																																																										BgL_classz00_2859;
																																																									{	/* Llib/error.scm 1347 */
																																																										obj_t
																																																											BgL_arg2728z00_4668;
																																																										long
																																																											BgL_arg2729z00_4669;
																																																										BgL_arg2728z00_4668
																																																											=
																																																											(BGl_za2classesza2z00zz__objectz00);
																																																										{	/* Llib/error.scm 1347 */
																																																											long
																																																												BgL_arg2731z00_4670;
																																																											BgL_arg2731z00_4670
																																																												=
																																																												BGL_OBJECT_CLASS_NUM
																																																												(
																																																												((BgL_objectz00_bglt) BgL_objz00_133));
																																																											BgL_arg2729z00_4669
																																																												=
																																																												(BgL_arg2731z00_4670
																																																												-
																																																												OBJECT_TYPE);
																																																										}
																																																										BgL_classz00_2859
																																																											=
																																																											VECTOR_REF
																																																											(BgL_arg2728z00_4668,
																																																											BgL_arg2729z00_4669);
																																																									}
																																																									if (BGl_classzf3zf3zz__objectz00(BgL_classz00_2859))
																																																										{	/* Llib/error.scm 1352 */
																																																											obj_t
																																																												BgL_symz00_2861;
																																																											BgL_symz00_2861
																																																												=
																																																												BGl_classzd2namezd2zz__objectz00
																																																												(BgL_classz00_2859);
																																																											{	/* Llib/error.scm 1354 */
																																																												obj_t
																																																													BgL_arg2328z00_4677;
																																																												BgL_arg2328z00_4677
																																																													=
																																																													SYMBOL_TO_STRING
																																																													(BgL_symz00_2861);
																																																												return
																																																													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																																													(BgL_arg2328z00_4677);
																																																											}
																																																										}
																																																									else
																																																										{	/* Llib/error.scm 1351 */
																																																											return
																																																												BGl_string2925z00zz__errorz00;
																																																										}
																																																								}
																																																							else
																																																								{	/* Llib/error.scm 1346 */
																																																									if (UCS2_STRINGP(BgL_objz00_133))
																																																										{	/* Llib/error.scm 1357 */
																																																											return
																																																												BGl_string2926z00zz__errorz00;
																																																										}
																																																									else
																																																										{	/* Llib/error.scm 1357 */
																																																											if (UCS2P(BgL_objz00_133))
																																																												{	/* Llib/error.scm 1359 */
																																																													return
																																																														BGl_string2927z00zz__errorz00;
																																																												}
																																																											else
																																																												{	/* Llib/error.scm 1359 */
																																																													if (ELONGP(BgL_objz00_133))
																																																														{	/* Llib/error.scm 1361 */
																																																															return
																																																																BGl_string2928z00zz__errorz00;
																																																														}
																																																													else
																																																														{	/* Llib/error.scm 1361 */
																																																															if (LLONGP(BgL_objz00_133))
																																																																{	/* Llib/error.scm 1363 */
																																																																	return
																																																																		BGl_string2929z00zz__errorz00;
																																																																}
																																																															else
																																																																{	/* Llib/error.scm 1363 */
																																																																	if (BGL_MUTEXP(BgL_objz00_133))
																																																																		{	/* Llib/error.scm 1365 */
																																																																			return
																																																																				BGl_string2930z00zz__errorz00;
																																																																		}
																																																																	else
																																																																		{	/* Llib/error.scm 1365 */
																																																																			if (BGL_CONDVARP(BgL_objz00_133))
																																																																				{	/* Llib/error.scm 1367 */
																																																																					return
																																																																						BGl_string2931z00zz__errorz00;
																																																																				}
																																																																			else
																																																																				{	/* Llib/error.scm 1367 */
																																																																					if (BGL_DATEP(BgL_objz00_133))
																																																																						{	/* Llib/error.scm 1369 */
																																																																							return
																																																																								BGl_string2932z00zz__errorz00;
																																																																						}
																																																																					else
																																																																						{	/* Llib/error.scm 1369 */
																																																																							if (BGL_HVECTORP(BgL_objz00_133))
																																																																								{	/* Llib/error.scm 1372 */
																																																																									obj_t
																																																																										BgL_tagz00_2871;
																																																																									BgL_tagz00_2871
																																																																										=
																																																																										BGl_homogeneouszd2vectorzd2infoz00zz__srfi4z00
																																																																										(BgL_objz00_133);
																																																																									{	/* Llib/error.scm 1373 */
																																																																										obj_t
																																																																											BgL__z00_2872;
																																																																										obj_t
																																																																											BgL__z00_2873;
																																																																										obj_t
																																																																											BgL__z00_2874;
																																																																										{	/* Llib/error.scm 1374 */
																																																																											obj_t
																																																																												BgL_tmpz00_4678;
																																																																											{	/* Llib/error.scm 1374 */
																																																																												int
																																																																													BgL_tmpz00_8253;
																																																																												BgL_tmpz00_8253
																																																																													=
																																																																													(int)
																																																																													(1L);
																																																																												BgL_tmpz00_4678
																																																																													=
																																																																													BGL_MVALUES_VAL
																																																																													(BgL_tmpz00_8253);
																																																																											}
																																																																											{	/* Llib/error.scm 1374 */
																																																																												int
																																																																													BgL_tmpz00_8256;
																																																																												BgL_tmpz00_8256
																																																																													=
																																																																													(int)
																																																																													(1L);
																																																																												BGL_MVALUES_VAL_SET
																																																																													(BgL_tmpz00_8256,
																																																																													BUNSPEC);
																																																																											}
																																																																											BgL__z00_2872
																																																																												=
																																																																												BgL_tmpz00_4678;
																																																																										}
																																																																										{	/* Llib/error.scm 1374 */
																																																																											obj_t
																																																																												BgL_tmpz00_4679;
																																																																											{	/* Llib/error.scm 1374 */
																																																																												int
																																																																													BgL_tmpz00_8259;
																																																																												BgL_tmpz00_8259
																																																																													=
																																																																													(int)
																																																																													(2L);
																																																																												BgL_tmpz00_4679
																																																																													=
																																																																													BGL_MVALUES_VAL
																																																																													(BgL_tmpz00_8259);
																																																																											}
																																																																											{	/* Llib/error.scm 1374 */
																																																																												int
																																																																													BgL_tmpz00_8262;
																																																																												BgL_tmpz00_8262
																																																																													=
																																																																													(int)
																																																																													(2L);
																																																																												BGL_MVALUES_VAL_SET
																																																																													(BgL_tmpz00_8262,
																																																																													BUNSPEC);
																																																																											}
																																																																											BgL__z00_2873
																																																																												=
																																																																												BgL_tmpz00_4679;
																																																																										}
																																																																										{	/* Llib/error.scm 1374 */
																																																																											obj_t
																																																																												BgL_tmpz00_4680;
																																																																											{	/* Llib/error.scm 1374 */
																																																																												int
																																																																													BgL_tmpz00_8265;
																																																																												BgL_tmpz00_8265
																																																																													=
																																																																													(int)
																																																																													(3L);
																																																																												BgL_tmpz00_4680
																																																																													=
																																																																													BGL_MVALUES_VAL
																																																																													(BgL_tmpz00_8265);
																																																																											}
																																																																											{	/* Llib/error.scm 1374 */
																																																																												int
																																																																													BgL_tmpz00_8268;
																																																																												BgL_tmpz00_8268
																																																																													=
																																																																													(int)
																																																																													(3L);
																																																																												BGL_MVALUES_VAL_SET
																																																																													(BgL_tmpz00_8268,
																																																																													BUNSPEC);
																																																																											}
																																																																											BgL__z00_2874
																																																																												=
																																																																												BgL_tmpz00_4680;
																																																																										}
																																																																										{	/* Llib/error.scm 1374 */
																																																																											obj_t
																																																																												BgL_arg2234z00_2875;
																																																																											{	/* Llib/error.scm 1374 */
																																																																												obj_t
																																																																													BgL_arg2328z00_4682;
																																																																												BgL_arg2328z00_4682
																																																																													=
																																																																													SYMBOL_TO_STRING
																																																																													(
																																																																													((obj_t) BgL_tagz00_2871));
																																																																												BgL_arg2234z00_2875
																																																																													=
																																																																													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																																																													(BgL_arg2328z00_4682);
																																																																											}
																																																																											{	/* Llib/error.scm 1374 */
																																																																												obj_t
																																																																													BgL_list2235z00_2876;
																																																																												{	/* Llib/error.scm 1374 */
																																																																													obj_t
																																																																														BgL_arg2236z00_2877;
																																																																													BgL_arg2236z00_2877
																																																																														=
																																																																														MAKE_YOUNG_PAIR
																																																																														(BGl_string2913z00zz__errorz00,
																																																																														BNIL);
																																																																													BgL_list2235z00_2876
																																																																														=
																																																																														MAKE_YOUNG_PAIR
																																																																														(BgL_arg2234z00_2875,
																																																																														BgL_arg2236z00_2877);
																																																																												}
																																																																												BGL_TAIL
																																																																													return
																																																																													BGl_stringzd2appendzd2zz__r4_strings_6_7z00
																																																																													(BgL_list2235z00_2876);
																																																																											}
																																																																										}
																																																																									}
																																																																								}
																																																																							else
																																																																								{	/* Llib/error.scm 1371 */
																																																																									if (BIGNUMP(BgL_objz00_133))
																																																																										{	/* Llib/error.scm 1375 */
																																																																											return
																																																																												BGl_string2933z00zz__errorz00;
																																																																										}
																																																																									else
																																																																										{	/* Llib/error.scm 1375 */
																																																																											if (BGL_MMAPP(BgL_objz00_133))
																																																																												{	/* Llib/error.scm 1377 */
																																																																													return
																																																																														BGl_string2934z00zz__errorz00;
																																																																												}
																																																																											else
																																																																												{	/* Llib/error.scm 1377 */
																																																																													if (BGL_REGEXPP(BgL_objz00_133))
																																																																														{	/* Llib/error.scm 1379 */
																																																																															return
																																																																																BGl_string2935z00zz__errorz00;
																																																																														}
																																																																													else
																																																																														{	/* Llib/error.scm 1379 */
																																																																															if (BGL_INT8P(BgL_objz00_133))
																																																																																{	/* Llib/error.scm 1381 */
																																																																																	return
																																																																																		BGl_string2936z00zz__errorz00;
																																																																																}
																																																																															else
																																																																																{	/* Llib/error.scm 1381 */
																																																																																	if (BGL_UINT8P(BgL_objz00_133))
																																																																																		{	/* Llib/error.scm 1383 */
																																																																																			return
																																																																																				BGl_string2937z00zz__errorz00;
																																																																																		}
																																																																																	else
																																																																																		{	/* Llib/error.scm 1383 */
																																																																																			if (BGL_INT16P(BgL_objz00_133))
																																																																																				{	/* Llib/error.scm 1385 */
																																																																																					return
																																																																																						BGl_string2938z00zz__errorz00;
																																																																																				}
																																																																																			else
																																																																																				{	/* Llib/error.scm 1385 */
																																																																																					if (BGL_UINT16P(BgL_objz00_133))
																																																																																						{	/* Llib/error.scm 1387 */
																																																																																							return
																																																																																								BGl_string2939z00zz__errorz00;
																																																																																						}
																																																																																					else
																																																																																						{	/* Llib/error.scm 1387 */
																																																																																							if (BGL_INT32P(BgL_objz00_133))
																																																																																								{	/* Llib/error.scm 1389 */
																																																																																									return
																																																																																										BGl_string2940z00zz__errorz00;
																																																																																								}
																																																																																							else
																																																																																								{	/* Llib/error.scm 1389 */
																																																																																									if (BGL_UINT32P(BgL_objz00_133))
																																																																																										{	/* Llib/error.scm 1391 */
																																																																																											return
																																																																																												BGl_string2941z00zz__errorz00;
																																																																																										}
																																																																																									else
																																																																																										{	/* Llib/error.scm 1391 */
																																																																																											if (BGL_INT64P(BgL_objz00_133))
																																																																																												{	/* Llib/error.scm 1393 */
																																																																																													return
																																																																																														BGl_string2942z00zz__errorz00;
																																																																																												}
																																																																																											else
																																																																																												{	/* Llib/error.scm 1393 */
																																																																																													if (BGL_UINT64P(BgL_objz00_133))
																																																																																														{	/* Llib/error.scm 1395 */
																																																																																															return
																																																																																																BGl_string2943z00zz__errorz00;
																																																																																														}
																																																																																													else
																																																																																														{	/* Llib/error.scm 1395 */
																																																																																															if (CNSTP(BgL_objz00_133))
																																																																																																{	/* Llib/error.scm 1397 */
																																																																																																	return
																																																																																																		BGl_string2944z00zz__errorz00;
																																																																																																}
																																																																																															else
																																																																																																{	/* Llib/error.scm 1397 */
																																																																																																	return
																																																																																																		string_to_bstring
																																																																																																		(FOREIGN_TYPE_NAME
																																																																																																		(BgL_objz00_133));
																																																																																																}
																																																																																														}
																																																																																												}
																																																																																										}
																																																																																								}
																																																																																						}
																																																																																				}
																																																																																		}
																																																																																}
																																																																														}
																																																																												}
																																																																										}
																																																																								}
																																																																						}
																																																																				}
																																																																		}
																																																																}
																																																														}
																																																												}
																																																										}
																																																								}
																																																						}
																																																				}
																																																		}
																																																}
																																														}
																																												}
																																										}
																																								}
																																						}
																																				}
																																		}
																																}
																														}
																												}
																										}
																								}
																						}
																				}
																		}
																}
														}
												}
										}
								}
						}
				}
		}

	}



/* &typeof */
	obj_t BGl_z62typeofz62zz__errorz00(obj_t BgL_envz00_5370,
		obj_t BgL_objz00_5371)
	{
		{	/* Llib/error.scm 1292 */
			return bgl_typeof(BgL_objz00_5371);
		}

	}



/* find-runtime-type */
	BGL_EXPORTED_DEF obj_t bgl_find_runtime_type(obj_t BgL_oz00_134)
	{
		{	/* Llib/error.scm 1405 */
			BGL_TAIL return bgl_typeof(BgL_oz00_134);
		}

	}



/* &find-runtime-type */
	obj_t BGl_z62findzd2runtimezd2typez62zz__errorz00(obj_t BgL_envz00_5372,
		obj_t BgL_oz00_5373)
	{
		{	/* Llib/error.scm 1405 */
			return bgl_find_runtime_type(BgL_oz00_5373);
		}

	}



/* c-debugging-show-type */
	BGL_EXPORTED_DEF char *bgl_show_type(obj_t BgL_objz00_135)
	{
		{	/* Llib/error.scm 1413 */
			{	/* Llib/error.scm 1414 */
				obj_t BgL_tz00_4683;

				BgL_tz00_4683 = bgl_typeof(BgL_objz00_135);
				{	/* Llib/error.scm 1415 */
					obj_t BgL_arg2249z00_4684;

					{	/* Llib/error.scm 1415 */
						obj_t BgL_tmpz00_8307;

						BgL_tmpz00_8307 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_arg2249z00_4684 = BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_8307);
					}
					{	/* Llib/error.scm 1415 */
						obj_t BgL_list2250z00_4685;

						BgL_list2250z00_4685 = MAKE_YOUNG_PAIR(BgL_tz00_4683, BNIL);
						BGl_fprintz00zz__r4_output_6_10_3z00(BgL_arg2249z00_4684,
							BgL_list2250z00_4685);
				}}
				return BSTRING_TO_STRING(BgL_tz00_4683);
			}
		}

	}



/* &c-debugging-show-type */
	obj_t BGl_z62czd2debuggingzd2showzd2typezb0zz__errorz00(obj_t BgL_envz00_5374,
		obj_t BgL_objz00_5375)
	{
		{	/* Llib/error.scm 1413 */
			return string_to_bstring(bgl_show_type(BgL_objz00_5375));
		}

	}



/* &try */
	BGL_EXPORTED_DEF obj_t BGl_z62tryz62zz__errorz00(obj_t BgL_thunkz00_136,
		obj_t BgL_handlerz00_137)
	{
		{	/* Llib/error.scm 1421 */
			BGL_TAIL return
				BGl_zc3z04exitza32251ze3ze70z60zz__errorz00(BgL_thunkz00_136,
				BgL_handlerz00_137);
		}

	}



/* <@exit:2251>~0 */
	obj_t BGl_zc3z04exitza32251ze3ze70z60zz__errorz00(obj_t BgL_thunkz00_5403,
		obj_t BgL_handlerz00_5402)
	{
		{	/* Llib/error.scm 1422 */
			jmp_buf_t jmpbuf;
			void *BgL_an_exit1147z00_2894;

			if (SET_EXIT(BgL_an_exit1147z00_2894))
				{
					return BGL_EXIT_VALUE();
				}
			else
				{
#if( SIGSETJMP_SAVESIGS == 0 )
				 // MS: CARE 5 jan 2021: see runtime/Clib/csystem.c
				 // bgl_restore_signal_handlers();
#endif

					BgL_an_exit1147z00_2894 = (void *) jmpbuf;
					{	/* Llib/error.scm 1422 */
						obj_t BgL_env1151z00_2895;

						BgL_env1151z00_2895 = BGL_CURRENT_DYNAMIC_ENV();
						PUSH_ENV_EXIT(BgL_env1151z00_2895, BgL_an_exit1147z00_2894, 1L);
						{	/* Llib/error.scm 1422 */
							obj_t BgL_an_exitd1148z00_2896;

							BgL_an_exitd1148z00_2896 =
								BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1151z00_2895);
							{	/* Llib/error.scm 1422 */
								obj_t BgL_escz00_5376;

								BgL_escz00_5376 =
									MAKE_FX_PROCEDURE(BGl_z62escz62zz__errorz00,
									(int) (1L), (int) (1L));
								PROCEDURE_SET(BgL_escz00_5376,
									(int) (0L), BgL_an_exitd1148z00_2896);
								{	/* Llib/error.scm 1422 */
									obj_t BgL_res1150z00_2899;

									{	/* Llib/error.scm 1425 */
										obj_t BgL_zc3z04anonymousza32253ze3z87_5377;

										BgL_zc3z04anonymousza32253ze3z87_5377 =
											MAKE_FX_PROCEDURE
											(BGl_z62zc3z04anonymousza32253ze3ze5zz__errorz00,
											(int) (1L), (int) (3L));
										PROCEDURE_SET(BgL_zc3z04anonymousza32253ze3z87_5377,
											(int) (0L), BgL_an_exitd1148z00_2896);
										PROCEDURE_SET(BgL_zc3z04anonymousza32253ze3z87_5377,
											(int) (1L), BgL_handlerz00_5402);
										PROCEDURE_SET(BgL_zc3z04anonymousza32253ze3z87_5377,
											(int) (2L), BgL_escz00_5376);
										BgL_res1150z00_2899 =
											BGl_withzd2exceptionzd2handlerz00zz__errorz00
											(BgL_zc3z04anonymousza32253ze3z87_5377,
											BgL_thunkz00_5403);
									}
									POP_ENV_EXIT(BgL_env1151z00_2895);
									return BgL_res1150z00_2899;
								}
							}
						}
					}
				}
		}

	}



/* &&try */
	obj_t BGl_z62z62tryz00zz__errorz00(obj_t BgL_envz00_5378,
		obj_t BgL_thunkz00_5379, obj_t BgL_handlerz00_5380)
	{
		{	/* Llib/error.scm 1421 */
			{	/* Llib/error.scm 1422 */
				obj_t BgL_auxz00_8344;
				obj_t BgL_auxz00_8337;

				if (PROCEDUREP(BgL_handlerz00_5380))
					{	/* Llib/error.scm 1422 */
						BgL_auxz00_8344 = BgL_handlerz00_5380;
					}
				else
					{
						obj_t BgL_auxz00_8347;

						BgL_auxz00_8347 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2811z00zz__errorz00,
							BINT(54010L), BGl_string2945z00zz__errorz00,
							BGl_string2821z00zz__errorz00, BgL_handlerz00_5380);
						FAILURE(BgL_auxz00_8347, BFALSE, BFALSE);
					}
				if (PROCEDUREP(BgL_thunkz00_5379))
					{	/* Llib/error.scm 1422 */
						BgL_auxz00_8337 = BgL_thunkz00_5379;
					}
				else
					{
						obj_t BgL_auxz00_8340;

						BgL_auxz00_8340 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2811z00zz__errorz00,
							BINT(54010L), BGl_string2945z00zz__errorz00,
							BGl_string2821z00zz__errorz00, BgL_thunkz00_5379);
						FAILURE(BgL_auxz00_8340, BFALSE, BFALSE);
					}
				return BGl_z62tryz62zz__errorz00(BgL_auxz00_8337, BgL_auxz00_8344);
			}
		}

	}



/* &<@anonymous:2253> */
	obj_t BGl_z62zc3z04anonymousza32253ze3ze5zz__errorz00(obj_t BgL_envz00_5381,
		obj_t BgL_ez00_5385)
	{
		{	/* Llib/error.scm 1424 */
			{	/* Llib/error.scm 1425 */
				obj_t BgL_handlerz00_5383;
				obj_t BgL_escz00_5384;

				BgL_handlerz00_5383 =
					((obj_t) PROCEDURE_REF(BgL_envz00_5381, (int) (1L)));
				BgL_escz00_5384 = ((obj_t) PROCEDURE_REF(BgL_envz00_5381, (int) (2L)));
				{	/* Llib/error.scm 1425 */
					bool_t BgL_test3460z00_8358;

					{	/* Llib/error.scm 1425 */
						obj_t BgL_classz00_5463;

						BgL_classz00_5463 = BGl_z62errorz62zz__objectz00;
						if (BGL_OBJECTP(BgL_ez00_5385))
							{	/* Llib/error.scm 1425 */
								BgL_objectz00_bglt BgL_arg2721z00_5464;

								BgL_arg2721z00_5464 = (BgL_objectz00_bglt) (BgL_ez00_5385);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Llib/error.scm 1425 */
										long BgL_idxz00_5465;

										BgL_idxz00_5465 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg2721z00_5464);
										BgL_test3460z00_8358 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_5465 + 3L)) == BgL_classz00_5463);
									}
								else
									{	/* Llib/error.scm 1425 */
										bool_t BgL_res2746z00_5468;

										{	/* Llib/error.scm 1425 */
											obj_t BgL_oclassz00_5469;

											{	/* Llib/error.scm 1425 */
												obj_t BgL_arg2728z00_5470;
												long BgL_arg2729z00_5471;

												BgL_arg2728z00_5470 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Llib/error.scm 1425 */
													long BgL_arg2731z00_5472;

													BgL_arg2731z00_5472 =
														BGL_OBJECT_CLASS_NUM(BgL_arg2721z00_5464);
													BgL_arg2729z00_5471 =
														(BgL_arg2731z00_5472 - OBJECT_TYPE);
												}
												BgL_oclassz00_5469 =
													VECTOR_REF(BgL_arg2728z00_5470, BgL_arg2729z00_5471);
											}
											{	/* Llib/error.scm 1425 */
												bool_t BgL__ortest_1198z00_5473;

												BgL__ortest_1198z00_5473 =
													(BgL_classz00_5463 == BgL_oclassz00_5469);
												if (BgL__ortest_1198z00_5473)
													{	/* Llib/error.scm 1425 */
														BgL_res2746z00_5468 = BgL__ortest_1198z00_5473;
													}
												else
													{	/* Llib/error.scm 1425 */
														long BgL_odepthz00_5474;

														{	/* Llib/error.scm 1425 */
															obj_t BgL_arg2717z00_5475;

															BgL_arg2717z00_5475 = (BgL_oclassz00_5469);
															BgL_odepthz00_5474 =
																BGL_CLASS_DEPTH(BgL_arg2717z00_5475);
														}
														if ((3L < BgL_odepthz00_5474))
															{	/* Llib/error.scm 1425 */
																obj_t BgL_arg2715z00_5476;

																{	/* Llib/error.scm 1425 */
																	obj_t BgL_arg2716z00_5477;

																	BgL_arg2716z00_5477 = (BgL_oclassz00_5469);
																	BgL_arg2715z00_5476 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg2716z00_5477,
																		3L);
																}
																BgL_res2746z00_5468 =
																	(BgL_arg2715z00_5476 == BgL_classz00_5463);
															}
														else
															{	/* Llib/error.scm 1425 */
																BgL_res2746z00_5468 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test3460z00_8358 = BgL_res2746z00_5468;
									}
							}
						else
							{	/* Llib/error.scm 1425 */
								BgL_test3460z00_8358 = ((bool_t) 0);
							}
					}
					if (BgL_test3460z00_8358)
						{	/* Llib/error.scm 1425 */
							{	/* Llib/error.scm 1427 */
								obj_t BgL_arg2255z00_5478;
								obj_t BgL_arg2256z00_5479;
								obj_t BgL_arg2257z00_5480;

								BgL_arg2255z00_5478 =
									(((BgL_z62errorz62_bglt) COBJECT(
											((BgL_z62errorz62_bglt) BgL_ez00_5385)))->BgL_procz00);
								BgL_arg2256z00_5479 =
									(((BgL_z62errorz62_bglt) COBJECT(
											((BgL_z62errorz62_bglt) BgL_ez00_5385)))->BgL_msgz00);
								BgL_arg2257z00_5480 =
									(((BgL_z62errorz62_bglt) COBJECT(
											((BgL_z62errorz62_bglt) BgL_ez00_5385)))->BgL_objz00);
								BGL_PROCEDURE_CALL4(BgL_handlerz00_5383, BgL_escz00_5384,
									BgL_arg2255z00_5478, BgL_arg2256z00_5479,
									BgL_arg2257z00_5480);
							}
							{	/* Llib/error.scm 1428 */
								obj_t BgL_list2258z00_5481;

								BgL_list2258z00_5481 = MAKE_YOUNG_PAIR(BINT(4L), BNIL);
								return BGl_exitz00zz__errorz00(BgL_list2258z00_5481);
							}
						}
					else
						{	/* Llib/error.scm 1425 */
							return BGl_raisez00zz__errorz00(BgL_ez00_5385);
						}
				}
			}
		}

	}



/* &esc */
	obj_t BGl_z62escz62zz__errorz00(obj_t BgL_envz00_5386,
		obj_t BgL_val1149z00_5388)
	{
		{	/* Llib/error.scm 1422 */
			return
				unwind_stack_until(PROCEDURE_REF(BgL_envz00_5386,
					(int) (0L)), BFALSE, BgL_val1149z00_5388, BFALSE, BFALSE);
		}

	}



/* push-error-handler! */
	BGL_EXPORTED_DEF obj_t BGl_pushzd2errorzd2handlerz12z12zz__errorz00(obj_t
		BgL_hdlz00_138, obj_t BgL_exitz00_139)
	{
		{	/* Llib/error.scm 1435 */
			BGL_ERROR_HANDLER_PUSH(BgL_hdlz00_138, BgL_exitz00_139);
			return BUNSPEC;
		}

	}



/* &push-error-handler! */
	obj_t BGl_z62pushzd2errorzd2handlerz12z70zz__errorz00(obj_t BgL_envz00_5389,
		obj_t BgL_hdlz00_5390, obj_t BgL_exitz00_5391)
	{
		{	/* Llib/error.scm 1435 */
			{	/* Llib/error.scm 1435 */
				obj_t BgL_auxz00_8402;

				if (PROCEDUREP(BgL_hdlz00_5390))
					{	/* Llib/error.scm 1435 */
						BgL_auxz00_8402 = BgL_hdlz00_5390;
					}
				else
					{
						obj_t BgL_auxz00_8405;

						BgL_auxz00_8405 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2811z00zz__errorz00,
							BINT(54431L), BGl_string2946z00zz__errorz00,
							BGl_string2821z00zz__errorz00, BgL_hdlz00_5390);
						FAILURE(BgL_auxz00_8405, BFALSE, BFALSE);
					}
				return
					BGl_pushzd2errorzd2handlerz12z12zz__errorz00(BgL_auxz00_8402,
					BgL_exitz00_5391);
			}
		}

	}



/* set-error-handler! */
	BGL_EXPORTED_DEF obj_t BGl_setzd2errorzd2handlerz12z12zz__errorz00(obj_t
		BgL_ehdlz00_140)
	{
		{	/* Llib/error.scm 1441 */
			BGL_ERROR_HANDLER_SET(BgL_ehdlz00_140);
			return BUNSPEC;
		}

	}



/* &set-error-handler! */
	obj_t BGl_z62setzd2errorzd2handlerz12z70zz__errorz00(obj_t BgL_envz00_5392,
		obj_t BgL_ehdlz00_5393)
	{
		{	/* Llib/error.scm 1441 */
			return BGl_setzd2errorzd2handlerz12z12zz__errorz00(BgL_ehdlz00_5393);
		}

	}



/* env-set-error-handler! */
	BGL_EXPORTED_DEF obj_t BGl_envzd2setzd2errorzd2handlerz12zc0zz__errorz00(obj_t
		BgL_envz00_141, obj_t BgL_ehdlz00_142)
	{
		{	/* Llib/error.scm 1447 */
			BGL_ENV_ERROR_HANDLER_SET(BgL_envz00_141, BgL_ehdlz00_142);
			return BUNSPEC;
		}

	}



/* &env-set-error-handler! */
	obj_t BGl_z62envzd2setzd2errorzd2handlerz12za2zz__errorz00(obj_t
		BgL_envz00_5394, obj_t BgL_envz00_5395, obj_t BgL_ehdlz00_5396)
	{
		{	/* Llib/error.scm 1447 */
			{	/* Llib/error.scm 1447 */
				obj_t BgL_auxz00_8413;

				if (BGL_DYNAMIC_ENVP(BgL_envz00_5395))
					{	/* Llib/error.scm 1447 */
						BgL_auxz00_8413 = BgL_envz00_5395;
					}
				else
					{
						obj_t BgL_auxz00_8416;

						BgL_auxz00_8416 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2811z00zz__errorz00,
							BINT(55031L), BGl_string2947z00zz__errorz00,
							BGl_string2948z00zz__errorz00, BgL_envz00_5395);
						FAILURE(BgL_auxz00_8416, BFALSE, BFALSE);
					}
				return
					BGl_envzd2setzd2errorzd2handlerz12zc0zz__errorz00(BgL_auxz00_8413,
					BgL_ehdlz00_5396);
			}
		}

	}



/* get-error-handler */
	BGL_EXPORTED_DEF obj_t BGl_getzd2errorzd2handlerz00zz__errorz00(void)
	{
		{	/* Llib/error.scm 1453 */
			return BGL_ERROR_HANDLER_GET();
		}

	}



/* &get-error-handler */
	obj_t BGl_z62getzd2errorzd2handlerz62zz__errorz00(obj_t BgL_envz00_5397)
	{
		{	/* Llib/error.scm 1453 */
			return BGl_getzd2errorzd2handlerz00zz__errorz00();
		}

	}



/* env-get-error-handler */
	BGL_EXPORTED_DEF obj_t BGl_envzd2getzd2errorzd2handlerzd2zz__errorz00(obj_t
		BgL_envz00_143)
	{
		{	/* Llib/error.scm 1459 */
			return BGL_ENV_ERROR_HANDLER_GET(BgL_envz00_143);
		}

	}



/* &env-get-error-handler */
	obj_t BGl_z62envzd2getzd2errorzd2handlerzb0zz__errorz00(obj_t BgL_envz00_5398,
		obj_t BgL_envz00_5399)
	{
		{	/* Llib/error.scm 1459 */
			{	/* Llib/error.scm 1460 */
				obj_t BgL_auxz00_8424;

				if (BGL_DYNAMIC_ENVP(BgL_envz00_5399))
					{	/* Llib/error.scm 1460 */
						BgL_auxz00_8424 = BgL_envz00_5399;
					}
				else
					{
						obj_t BgL_auxz00_8427;

						BgL_auxz00_8427 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2811z00zz__errorz00,
							BINT(55671L), BGl_string2949z00zz__errorz00,
							BGl_string2948z00zz__errorz00, BgL_envz00_5399);
						FAILURE(BgL_auxz00_8427, BFALSE, BFALSE);
					}
				return BGl_envzd2getzd2errorzd2handlerzd2zz__errorz00(BgL_auxz00_8424);
			}
		}

	}



/* notify-interrupt */
	BGL_EXPORTED_DEF obj_t BGl_notifyzd2interruptzd2zz__errorz00(int
		BgL_sigz00_144)
	{
		{	/* Llib/error.scm 1465 */
			{	/* Llib/error.scm 1466 */
				obj_t BgL_notifyz00_4721;

				BgL_notifyz00_4721 = BGL_INTERRUPT_NOTIFIER_GET();
				if (PROCEDUREP(BgL_notifyz00_4721))
					{	/* Llib/error.scm 1467 */
						return
							BGL_PROCEDURE_CALL1(BgL_notifyz00_4721, BINT(BgL_sigz00_144));
					}
				else
					{	/* Llib/error.scm 1476 */
						obj_t BgL_portz00_4723;

						{	/* Llib/error.scm 1476 */
							obj_t BgL_tmpz00_8440;

							BgL_tmpz00_8440 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_portz00_4723 = BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_8440);
						}
						{	/* Llib/error.scm 1477 */
							obj_t BgL_list2260z00_4724;

							BgL_list2260z00_4724 = MAKE_YOUNG_PAIR(BgL_portz00_4723, BNIL);
							BGl_newlinez00zz__r4_output_6_10_3z00(BgL_list2260z00_4724);
						}
						{	/* Llib/error.scm 1478 */
							obj_t BgL_list2261z00_4725;

							BgL_list2261z00_4725 =
								MAKE_YOUNG_PAIR(BGl_string2950z00zz__errorz00, BNIL);
							BGl_fprintz00zz__r4_output_6_10_3z00(BgL_portz00_4723,
								BgL_list2261z00_4725);
						}
						return bgl_flush_output_port(BgL_portz00_4723);
					}
			}
		}

	}



/* &notify-interrupt */
	obj_t BGl_z62notifyzd2interruptzb0zz__errorz00(obj_t BgL_envz00_5400,
		obj_t BgL_sigz00_5401)
	{
		{	/* Llib/error.scm 1465 */
			{	/* Llib/error.scm 1466 */
				int BgL_auxz00_8448;

				{	/* Llib/error.scm 1466 */
					obj_t BgL_tmpz00_8449;

					if (INTEGERP(BgL_sigz00_5401))
						{	/* Llib/error.scm 1466 */
							BgL_tmpz00_8449 = BgL_sigz00_5401;
						}
					else
						{
							obj_t BgL_auxz00_8452;

							BgL_auxz00_8452 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2811z00zz__errorz00,
								BINT(55958L), BGl_string2951z00zz__errorz00,
								BGl_string2813z00zz__errorz00, BgL_sigz00_5401);
							FAILURE(BgL_auxz00_8452, BFALSE, BFALSE);
						}
					BgL_auxz00_8448 = CINT(BgL_tmpz00_8449);
				}
				return BGl_notifyzd2interruptzd2zz__errorz00(BgL_auxz00_8448);
			}
		}

	}



/* &sigfpe-error-handler */
	obj_t BGl_z62sigfpezd2errorzd2handlerz62zz__errorz00(obj_t BgL_envz00_5231,
		obj_t BgL_nz00_5232)
	{
		{	/* Llib/error.scm 1484 */
			{	/* Llib/error.scm 546 */
				BgL_z62errorz62_bglt BgL_arg1509z00_5482;

				{	/* Llib/error.scm 546 */
					BgL_z62errorz62_bglt BgL_new1079z00_5483;

					{	/* Llib/error.scm 546 */
						BgL_z62errorz62_bglt BgL_new1078z00_5484;

						BgL_new1078z00_5484 =
							((BgL_z62errorz62_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_z62errorz62_bgl))));
						{	/* Llib/error.scm 546 */
							long BgL_arg1513z00_5485;

							BgL_arg1513z00_5485 = BGL_CLASS_NUM(BGl_z62errorz62zz__objectz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1078z00_5484),
								BgL_arg1513z00_5485);
						}
						BgL_new1079z00_5483 = BgL_new1078z00_5484;
					}
					((((BgL_z62exceptionz62_bglt) COBJECT(
									((BgL_z62exceptionz62_bglt) BgL_new1079z00_5483)))->
							BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
					((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
										BgL_new1079z00_5483)))->BgL_locationz00) =
						((obj_t) BFALSE), BUNSPEC);
					{
						obj_t BgL_auxz00_8466;

						{	/* Llib/error.scm 546 */
							obj_t BgL_arg1510z00_5486;

							{	/* Llib/error.scm 546 */
								obj_t BgL_arg1511z00_5487;

								{	/* Llib/error.scm 546 */
									obj_t BgL_classz00_5488;

									BgL_classz00_5488 = BGl_z62errorz62zz__objectz00;
									BgL_arg1511z00_5487 = BGL_CLASS_ALL_FIELDS(BgL_classz00_5488);
								}
								BgL_arg1510z00_5486 = VECTOR_REF(BgL_arg1511z00_5487, 2L);
							}
							BgL_auxz00_8466 =
								BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
								(BgL_arg1510z00_5486);
						}
						((((BgL_z62exceptionz62_bglt) COBJECT(
										((BgL_z62exceptionz62_bglt) BgL_new1079z00_5483)))->
								BgL_stackz00) = ((obj_t) BgL_auxz00_8466), BUNSPEC);
					}
					((((BgL_z62errorz62_bglt) COBJECT(BgL_new1079z00_5483))->
							BgL_procz00) = ((obj_t) BGl_string2952z00zz__errorz00), BUNSPEC);
					((((BgL_z62errorz62_bglt) COBJECT(BgL_new1079z00_5483))->BgL_msgz00) =
						((obj_t) BGl_string2953z00zz__errorz00), BUNSPEC);
					((((BgL_z62errorz62_bglt) COBJECT(BgL_new1079z00_5483))->BgL_objz00) =
						((obj_t) BGl_string2954z00zz__errorz00), BUNSPEC);
					BgL_arg1509z00_5482 = BgL_new1079z00_5483;
				}
				return BGl_raisez00zz__errorz00(((obj_t) BgL_arg1509z00_5482));
			}
		}

	}



/* &sigill-error-handler */
	obj_t BGl_z62sigillzd2errorzd2handlerz62zz__errorz00(obj_t BgL_envz00_5233,
		obj_t BgL_nz00_5234)
	{
		{	/* Llib/error.scm 1490 */
			{	/* Llib/error.scm 546 */
				BgL_z62errorz62_bglt BgL_arg1509z00_5489;

				{	/* Llib/error.scm 546 */
					BgL_z62errorz62_bglt BgL_new1079z00_5490;

					{	/* Llib/error.scm 546 */
						BgL_z62errorz62_bglt BgL_new1078z00_5491;

						BgL_new1078z00_5491 =
							((BgL_z62errorz62_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_z62errorz62_bgl))));
						{	/* Llib/error.scm 546 */
							long BgL_arg1513z00_5492;

							BgL_arg1513z00_5492 = BGL_CLASS_NUM(BGl_z62errorz62zz__objectz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1078z00_5491),
								BgL_arg1513z00_5492);
						}
						BgL_new1079z00_5490 = BgL_new1078z00_5491;
					}
					((((BgL_z62exceptionz62_bglt) COBJECT(
									((BgL_z62exceptionz62_bglt) BgL_new1079z00_5490)))->
							BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
					((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
										BgL_new1079z00_5490)))->BgL_locationz00) =
						((obj_t) BFALSE), BUNSPEC);
					{
						obj_t BgL_auxz00_8485;

						{	/* Llib/error.scm 546 */
							obj_t BgL_arg1510z00_5493;

							{	/* Llib/error.scm 546 */
								obj_t BgL_arg1511z00_5494;

								{	/* Llib/error.scm 546 */
									obj_t BgL_classz00_5495;

									BgL_classz00_5495 = BGl_z62errorz62zz__objectz00;
									BgL_arg1511z00_5494 = BGL_CLASS_ALL_FIELDS(BgL_classz00_5495);
								}
								BgL_arg1510z00_5493 = VECTOR_REF(BgL_arg1511z00_5494, 2L);
							}
							BgL_auxz00_8485 =
								BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
								(BgL_arg1510z00_5493);
						}
						((((BgL_z62exceptionz62_bglt) COBJECT(
										((BgL_z62exceptionz62_bglt) BgL_new1079z00_5490)))->
								BgL_stackz00) = ((obj_t) BgL_auxz00_8485), BUNSPEC);
					}
					((((BgL_z62errorz62_bglt) COBJECT(BgL_new1079z00_5490))->
							BgL_procz00) = ((obj_t) BGl_string2955z00zz__errorz00), BUNSPEC);
					((((BgL_z62errorz62_bglt) COBJECT(BgL_new1079z00_5490))->BgL_msgz00) =
						((obj_t) BGl_string2956z00zz__errorz00), BUNSPEC);
					((((BgL_z62errorz62_bglt) COBJECT(BgL_new1079z00_5490))->BgL_objz00) =
						((obj_t) BGl_string2954z00zz__errorz00), BUNSPEC);
					BgL_arg1509z00_5489 = BgL_new1079z00_5490;
				}
				return BGl_raisez00zz__errorz00(((obj_t) BgL_arg1509z00_5489));
			}
		}

	}



/* &sigbus-error-handler */
	obj_t BGl_z62sigbuszd2errorzd2handlerz62zz__errorz00(obj_t BgL_envz00_5235,
		obj_t BgL_nz00_5236)
	{
		{	/* Llib/error.scm 1496 */
			{	/* Llib/error.scm 546 */
				BgL_z62errorz62_bglt BgL_arg1509z00_5496;

				{	/* Llib/error.scm 546 */
					BgL_z62errorz62_bglt BgL_new1079z00_5497;

					{	/* Llib/error.scm 546 */
						BgL_z62errorz62_bglt BgL_new1078z00_5498;

						BgL_new1078z00_5498 =
							((BgL_z62errorz62_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_z62errorz62_bgl))));
						{	/* Llib/error.scm 546 */
							long BgL_arg1513z00_5499;

							BgL_arg1513z00_5499 = BGL_CLASS_NUM(BGl_z62errorz62zz__objectz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1078z00_5498),
								BgL_arg1513z00_5499);
						}
						BgL_new1079z00_5497 = BgL_new1078z00_5498;
					}
					((((BgL_z62exceptionz62_bglt) COBJECT(
									((BgL_z62exceptionz62_bglt) BgL_new1079z00_5497)))->
							BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
					((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
										BgL_new1079z00_5497)))->BgL_locationz00) =
						((obj_t) BFALSE), BUNSPEC);
					{
						obj_t BgL_auxz00_8504;

						{	/* Llib/error.scm 546 */
							obj_t BgL_arg1510z00_5500;

							{	/* Llib/error.scm 546 */
								obj_t BgL_arg1511z00_5501;

								{	/* Llib/error.scm 546 */
									obj_t BgL_classz00_5502;

									BgL_classz00_5502 = BGl_z62errorz62zz__objectz00;
									BgL_arg1511z00_5501 = BGL_CLASS_ALL_FIELDS(BgL_classz00_5502);
								}
								BgL_arg1510z00_5500 = VECTOR_REF(BgL_arg1511z00_5501, 2L);
							}
							BgL_auxz00_8504 =
								BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
								(BgL_arg1510z00_5500);
						}
						((((BgL_z62exceptionz62_bglt) COBJECT(
										((BgL_z62exceptionz62_bglt) BgL_new1079z00_5497)))->
								BgL_stackz00) = ((obj_t) BgL_auxz00_8504), BUNSPEC);
					}
					((((BgL_z62errorz62_bglt) COBJECT(BgL_new1079z00_5497))->
							BgL_procz00) = ((obj_t) BGl_string2955z00zz__errorz00), BUNSPEC);
					((((BgL_z62errorz62_bglt) COBJECT(BgL_new1079z00_5497))->BgL_msgz00) =
						((obj_t) BGl_string2957z00zz__errorz00), BUNSPEC);
					((((BgL_z62errorz62_bglt) COBJECT(BgL_new1079z00_5497))->BgL_objz00) =
						((obj_t) BGl_string2954z00zz__errorz00), BUNSPEC);
					BgL_arg1509z00_5496 = BgL_new1079z00_5497;
				}
				return BGl_raisez00zz__errorz00(((obj_t) BgL_arg1509z00_5496));
			}
		}

	}



/* &sigsegv-error-handler */
	obj_t BGl_z62sigsegvzd2errorzd2handlerz62zz__errorz00(obj_t BgL_envz00_5237,
		obj_t BgL_nz00_5238)
	{
		{	/* Llib/error.scm 1502 */
			{	/* Llib/error.scm 546 */
				BgL_z62errorz62_bglt BgL_arg1509z00_5503;

				{	/* Llib/error.scm 546 */
					BgL_z62errorz62_bglt BgL_new1079z00_5504;

					{	/* Llib/error.scm 546 */
						BgL_z62errorz62_bglt BgL_new1078z00_5505;

						BgL_new1078z00_5505 =
							((BgL_z62errorz62_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_z62errorz62_bgl))));
						{	/* Llib/error.scm 546 */
							long BgL_arg1513z00_5506;

							BgL_arg1513z00_5506 = BGL_CLASS_NUM(BGl_z62errorz62zz__objectz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1078z00_5505),
								BgL_arg1513z00_5506);
						}
						BgL_new1079z00_5504 = BgL_new1078z00_5505;
					}
					((((BgL_z62exceptionz62_bglt) COBJECT(
									((BgL_z62exceptionz62_bglt) BgL_new1079z00_5504)))->
							BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
					((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
										BgL_new1079z00_5504)))->BgL_locationz00) =
						((obj_t) BFALSE), BUNSPEC);
					{
						obj_t BgL_auxz00_8523;

						{	/* Llib/error.scm 546 */
							obj_t BgL_arg1510z00_5507;

							{	/* Llib/error.scm 546 */
								obj_t BgL_arg1511z00_5508;

								{	/* Llib/error.scm 546 */
									obj_t BgL_classz00_5509;

									BgL_classz00_5509 = BGl_z62errorz62zz__objectz00;
									BgL_arg1511z00_5508 = BGL_CLASS_ALL_FIELDS(BgL_classz00_5509);
								}
								BgL_arg1510z00_5507 = VECTOR_REF(BgL_arg1511z00_5508, 2L);
							}
							BgL_auxz00_8523 =
								BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
								(BgL_arg1510z00_5507);
						}
						((((BgL_z62exceptionz62_bglt) COBJECT(
										((BgL_z62exceptionz62_bglt) BgL_new1079z00_5504)))->
								BgL_stackz00) = ((obj_t) BgL_auxz00_8523), BUNSPEC);
					}
					((((BgL_z62errorz62_bglt) COBJECT(BgL_new1079z00_5504))->
							BgL_procz00) = ((obj_t) BGl_string2955z00zz__errorz00), BUNSPEC);
					((((BgL_z62errorz62_bglt) COBJECT(BgL_new1079z00_5504))->BgL_msgz00) =
						((obj_t) BGl_string2958z00zz__errorz00), BUNSPEC);
					((((BgL_z62errorz62_bglt) COBJECT(BgL_new1079z00_5504))->BgL_objz00) =
						((obj_t) BGl_string2954z00zz__errorz00), BUNSPEC);
					BgL_arg1509z00_5503 = BgL_new1079z00_5504;
				}
				return BGl_raisez00zz__errorz00(((obj_t) BgL_arg1509z00_5503));
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__errorz00(void)
	{
		{	/* Llib/error.scm 18 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__errorz00(void)
	{
		{	/* Llib/error.scm 18 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__errorz00(void)
	{
		{	/* Llib/error.scm 18 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__errorz00(void)
	{
		{	/* Llib/error.scm 18 */
			BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(323064722L,
				BSTRING_TO_STRING(BGl_string2959z00zz__errorz00));
			return
				BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string2959z00zz__errorz00));
		}

	}

#ifdef __cplusplus
}
#endif
