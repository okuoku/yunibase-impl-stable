/*===========================================================================*/
/*   (Llib/bigloo.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Llib/bigloo.scm -indent -o objs/obj_u/Llib/bigloo.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___BIGLOO_TYPE_DEFINITIONS
#define BGL___BIGLOO_TYPE_DEFINITIONS
#endif													// BGL___BIGLOO_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62bigloozd2classzd2demanglez62zz__biglooz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_cnstzf3zf3zz__biglooz00(obj_t);
	static obj_t BGl_z62bigloozd2classzd2mangledzf3z91zz__biglooz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_opaquezf3zf3zz__biglooz00(obj_t);
	extern obj_t bstring_to_keyword(obj_t);
	static obj_t BGl_z62opaquezf3z91zz__biglooz00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__biglooz00 = BUNSPEC;
	extern obj_t bgl_remq_bang(obj_t, obj_t);
	static obj_t BGl_z62bigloozd2modulezd2demanglez62zz__biglooz00(obj_t, obj_t);
	static obj_t BGl_z62unregisterzd2exitzd2functionz12z70zz__biglooz00(obj_t,
		obj_t);
	static obj_t BGl_za2levelza2z00zz__biglooz00 = BUNSPEC;
	static obj_t BGl_z62timez62zz__biglooz00(obj_t, obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__configurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static long BGl_getzd28bitszd2integerze70ze7zz__biglooz00(obj_t, obj_t);
	static obj_t BGl_z62bigloozd2needzd2manglingzf3z91zz__biglooz00(obj_t, obj_t);
	static obj_t BGl_z62cellzf3z91zz__biglooz00(obj_t, obj_t);
	static obj_t BGl_symbol1861z00zz__biglooz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t bigloo_demangle(obj_t);
	static long BGl_charzd2ze3digitze70zd6zz__biglooz00(unsigned char);
	static obj_t BGl_symbol1867z00zz__biglooz00 = BUNSPEC;
	static obj_t BGl_z62bigloozd2demanglezb0zz__biglooz00(obj_t, obj_t);
	static obj_t BGl_z62bigloozd2modulezd2manglez62zz__biglooz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62bigloozd2exitzd2applyz62zz__biglooz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t bigloo_mangledp(obj_t);
	static obj_t BGl_z62bigloozd2mangledzf3z43zz__biglooz00(obj_t, obj_t);
	static obj_t BGl_z62bigloozd2exitzd2mutexz62zz__biglooz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_gczd2finaliza7ez75zz__biglooz00(void);
	static obj_t BGl_z62nullzd2orzd2unspecifiedzf3z91zz__biglooz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_procedurezd2attrzd2setz12z12zz__biglooz00(obj_t,
		obj_t);
	static obj_t BGl_toplevelzd2initzd2zz__biglooz00(void);
	static obj_t BGl_za2exitzd2mutexza2zd2zz__biglooz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_timez00zz__biglooz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_checkzd2versionz12zc0zz__biglooz00(obj_t, char *,
		obj_t);
	static obj_t BGl_bigloozd2demanglezd2atze70ze7zz__biglooz00(obj_t, long,
		obj_t);
	static obj_t BGl_z62cnstzf3z91zz__biglooz00(obj_t, obj_t);
	static obj_t BGl_z62cellzd2setz12za2zz__biglooz00(obj_t, obj_t, obj_t);
	static obj_t BGl__gcz00zz__biglooz00(obj_t, obj_t);
	extern bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_unspecifiedz00zz__biglooz00(void);
	BGL_EXPORTED_DECL obj_t bigloo_module_mangle(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__biglooz00(void);
	static obj_t BGl_genericzd2initzd2zz__biglooz00(void);
	extern obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_unregisterzd2exitzd2functionz12z12zz__biglooz00(obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_nullzd2orzd2unspecifiedzf3zf3zz__biglooz00(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__biglooz00(void);
	static obj_t BGl_list1864z00zz__biglooz00 = BUNSPEC;
	static obj_t BGl_gczd2rootszd2initz00zz__biglooz00(void);
	static obj_t BGl_z62bmemzd2resetz12za2zz__biglooz00(obj_t);
	static obj_t BGl_objectzd2initzd2zz__biglooz00(void);
	BGL_EXPORTED_DECL obj_t BGl_opaquezd2nilzd2zz__biglooz00(void);
	BGL_EXPORTED_DECL obj_t bigloo_mangle(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cellzd2refzd2zz__biglooz00(obj_t);
	extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t bigloo_exit_apply(obj_t);
	BGL_EXPORTED_DECL int BGl_procedurezd2lengthzd2zz__biglooz00(obj_t);
	BGL_EXPORTED_DECL obj_t bigloo_module_demangle(obj_t);
	static obj_t BGl_za2bigloozd2exitzd2functionsza2z00zz__biglooz00 = BUNSPEC;
	extern obj_t string_append_3(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t bgl_exit_mutex(void);
	extern void bgl_gc_verbose_set(bool_t);
	static obj_t BGl_za2releaseza2z00zz__biglooz00 = BUNSPEC;
	static obj_t BGl_z62procedurezd2attrzb0zz__biglooz00(obj_t, obj_t);
	extern obj_t bgl_bmem_reset(void);
	BGL_EXPORTED_DECL obj_t BGl_cellzd2setz12zc0zz__biglooz00(obj_t, obj_t);
	static obj_t BGl_z62opaquezd2nilzb0zz__biglooz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_gczd2verbosezd2setz12z12zz__biglooz00(bool_t);
	static obj_t BGl_z62gczd2verbosezd2setz12z70zz__biglooz00(obj_t, obj_t);
	static obj_t BGl_z62makezd2cellzb0zz__biglooz00(obj_t, obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	extern obj_t c_substring(obj_t, long, long);
	BGL_EXPORTED_DECL obj_t
		BGl_registerzd2exitzd2functionz12z12zz__biglooz00(obj_t);
	static obj_t BGl_methodzd2initzd2zz__biglooz00(void);
	static obj_t BGl_z62bigloozd2manglezb0zz__biglooz00(obj_t, obj_t);
	static obj_t BGl_z62registerzd2exitzd2functionz12z70zz__biglooz00(obj_t,
		obj_t);
	extern obj_t blit_string(obj_t, long, obj_t, long, long);
	BGL_EXPORTED_DECL obj_t bigloo_class_demangle(obj_t);
	static obj_t BGl_z62gczd2finaliza7ez17zz__biglooz00(obj_t);
	static obj_t BGl_z62procedurezd2attrzd2setz12z70zz__biglooz00(obj_t, obj_t,
		obj_t);
	static long BGl_manglezd2atz12zc0zz__biglooz00(obj_t, obj_t, long, long);
	BGL_EXPORTED_DECL bool_t bigloo_class_mangledp(obj_t);
	extern obj_t make_string(long, unsigned char);
	BGL_EXPORTED_DECL obj_t BGl_makezd2cellzd2zz__biglooz00(obj_t);
	static obj_t BGl_za2modulesza2z00zz__biglooz00 = BUNSPEC;
	static obj_t BGl_z62cellzd2refzb0zz__biglooz00(obj_t, obj_t);
	static obj_t BGl_z62unspecifiedz62zz__biglooz00(obj_t);
	static obj_t BGl_z62procedurezd2lengthzb0zz__biglooz00(obj_t, obj_t);
	BGL_EXPORTED_DECL int BGl_procedurezd2arityzd2zz__biglooz00(obj_t);
	extern obj_t string_append(obj_t, obj_t);
	static obj_t BGl_z62procedurezd2arityzb0zz__biglooz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_procedurezd2attrzd2zz__biglooz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_bmemzd2resetz12zc0zz__biglooz00(void);
	static obj_t BGl_z62checkzd2versionz12za2zz__biglooz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_keyword1865z00zz__biglooz00 = BUNSPEC;
	BGL_EXPORTED_DECL bool_t
		BGl_bigloozd2needzd2manglingzf3zf3zz__biglooz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cellzf3zf3zz__biglooz00(obj_t);
	extern bool_t bigloo_strncmp(obj_t, obj_t, long);
	static obj_t BGl_symbol1827z00zz__biglooz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_gcz00zz__biglooz00(obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_unspecifiedzd2envzd2zz__biglooz00,
		BgL_bgl_za762unspecifiedza761874z00, BGl_z62unspecifiedz62zz__biglooz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1870z00zz__biglooz00,
		BgL_bgl_string1870za700za7za7_1875za7,
		"wrong number of arguments: [0..1] expected, provided", 52);
	      DEFINE_STRING(BGl_string1871z00zz__biglooz00,
		BgL_bgl_string1871za700za7za7_1876za7, "_gc", 3);
	      DEFINE_STRING(BGl_string1872z00zz__biglooz00,
		BgL_bgl_string1872za700za7za7_1877za7, "bint", 4);
	      DEFINE_STRING(BGl_string1873z00zz__biglooz00,
		BgL_bgl_string1873za700za7za7_1878za7, "__bigloo", 8);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_unregisterzd2exitzd2functionz12zd2envzc0zz__biglooz00,
		BgL_bgl_za762unregisterza7d21879z00,
		BGl_z62unregisterzd2exitzd2functionz12z70zz__biglooz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_procedurezd2attrzd2envz00zz__biglooz00,
		BgL_bgl_za762procedureza7d2a1880z00,
		BGl_z62procedurezd2attrzb0zz__biglooz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_opaquezd2nilzd2envz00zz__biglooz00,
		BgL_bgl_za762opaqueza7d2nilza71881za7, BGl_z62opaquezd2nilzb0zz__biglooz00,
		0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_bigloozd2mangledzf3zd2envzf3zz__biglooz00,
		BgL_bgl_za762biglooza7d2mang1882z00,
		BGl_z62bigloozd2mangledzf3z43zz__biglooz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_bigloozd2exitzd2mutexzd2envzd2zz__biglooz00,
		BgL_bgl_za762biglooza7d2exit1883z00,
		BGl_z62bigloozd2exitzd2mutexz62zz__biglooz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_gczd2verbosezd2setz12zd2envzc0zz__biglooz00,
		BgL_bgl_za762gcza7d2verboseza71884za7,
		BGl_z62gczd2verbosezd2setz12z70zz__biglooz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2modulezd2manglezd2envzd2zz__biglooz00,
		BgL_bgl_za762biglooza7d2modu1885z00,
		BGl_z62bigloozd2modulezd2manglez62zz__biglooz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_nullzd2orzd2unspecifiedzf3zd2envz21zz__biglooz00,
		BgL_bgl_za762nullza7d2orza7d2u1886za7,
		BGl_z62nullzd2orzd2unspecifiedzf3z91zz__biglooz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bmemzd2resetz12zd2envz12zz__biglooz00,
		BgL_bgl_za762bmemza7d2resetza71887za7,
		BGl_z62bmemzd2resetz12za2zz__biglooz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2needzd2manglingzf3zd2envz21zz__biglooz00,
		BgL_bgl_za762biglooza7d2need1888z00,
		BGl_z62bigloozd2needzd2manglingzf3z91zz__biglooz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_gczd2envzd2zz__biglooz00,
		BgL_bgl__gcza700za7za7__bigloo1889za7, opt_generic_entry,
		BGl__gcz00zz__biglooz00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_procedurezd2lengthzd2envz00zz__biglooz00,
		BgL_bgl_za762procedureza7d2l1890z00,
		BGl_z62procedurezd2lengthzb0zz__biglooz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_gczd2finaliza7ezd2envza7zz__biglooz00,
		BgL_bgl_za762gcza7d2finaliza7a1891za7,
		BGl_z62gczd2finaliza7ez17zz__biglooz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2classzd2mangledzf3zd2envz21zz__biglooz00,
		BgL_bgl_za762biglooza7d2clas1892z00,
		BGl_z62bigloozd2classzd2mangledzf3z91zz__biglooz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_opaquezf3zd2envz21zz__biglooz00,
		BgL_bgl_za762opaqueza7f3za791za71893z00, BGl_z62opaquezf3z91zz__biglooz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2cellzd2envz00zz__biglooz00,
		BgL_bgl_za762makeza7d2cellza7b1894za7, BGl_z62makezd2cellzb0zz__biglooz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_procedurezd2arityzd2envz00zz__biglooz00,
		BgL_bgl_za762procedureza7d2a1895z00,
		BGl_z62procedurezd2arityzb0zz__biglooz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bigloozd2manglezd2envz00zz__biglooz00,
		BgL_bgl_za762biglooza7d2mang1896z00, BGl_z62bigloozd2manglezb0zz__biglooz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cellzf3zd2envz21zz__biglooz00,
		BgL_bgl_za762cellza7f3za791za7za7_1897za7, BGl_z62cellzf3z91zz__biglooz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_registerzd2exitzd2functionz12zd2envzc0zz__biglooz00,
		BgL_bgl_za762registerza7d2ex1898z00,
		BGl_z62registerzd2exitzd2functionz12z70zz__biglooz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bigloozd2demanglezd2envz00zz__biglooz00,
		BgL_bgl_za762biglooza7d2dema1899z00,
		BGl_z62bigloozd2demanglezb0zz__biglooz00, 0L, BUNSPEC, 1);
	extern obj_t BGl_stringzd2envzd2zz__r4_strings_6_7z00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2modulezd2demanglezd2envzd2zz__biglooz00,
		BgL_bgl_za762biglooza7d2modu1900z00,
		BGl_z62bigloozd2modulezd2demanglez62zz__biglooz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_checkzd2versionz12zd2envz12zz__biglooz00,
		BgL_bgl_za762checkza7d2versi1901z00,
		BGl_z62checkzd2versionz12za2zz__biglooz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string1828z00zz__biglooz00,
		BgL_bgl_string1828za700za7za7_1902za7, "bigloo-exit", 11);
	      DEFINE_STRING(BGl_string1829z00zz__biglooz00,
		BgL_bgl_string1829za700za7za7_1903za7, " (level 0)", 10);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cnstzf3zd2envz21zz__biglooz00,
		BgL_bgl_za762cnstza7f3za791za7za7_1904za7, BGl_z62cnstzf3z91zz__biglooz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1830z00zz__biglooz00,
		BgL_bgl_string1830za700za7za7_1905za7,
		"Some modules have been compiled by: ", 36);
	      DEFINE_STRING(BGl_string1831z00zz__biglooz00,
		BgL_bgl_string1831za700za7za7_1906za7, "and other by: ", 14);
	      DEFINE_STRING(BGl_string1832z00zz__biglooz00,
		BgL_bgl_string1832za700za7za7_1907za7,
		"/tmp/bigloo/runtime/Llib/bigloo.scm", 35);
	      DEFINE_STRING(BGl_string1833z00zz__biglooz00,
		BgL_bgl_string1833za700za7za7_1908za7, "&check-version!", 15);
	      DEFINE_STRING(BGl_string1834z00zz__biglooz00,
		BgL_bgl_string1834za700za7za7_1909za7, "bstring", 7);
	      DEFINE_STRING(BGl_string1835z00zz__biglooz00,
		BgL_bgl_string1835za700za7za7_1910za7, "&procedure-arity", 16);
	      DEFINE_STRING(BGl_string1836z00zz__biglooz00,
		BgL_bgl_string1836za700za7za7_1911za7, "procedure", 9);
	      DEFINE_STRING(BGl_string1837z00zz__biglooz00,
		BgL_bgl_string1837za700za7za7_1912za7, "&procedure-length", 17);
	      DEFINE_STRING(BGl_string1838z00zz__biglooz00,
		BgL_bgl_string1838za700za7za7_1913za7, "&procedure-attr", 15);
	      DEFINE_STRING(BGl_string1839z00zz__biglooz00,
		BgL_bgl_string1839za700za7za7_1914za7, "&procedure-attr-set!", 20);
	      DEFINE_STRING(BGl_string1840z00zz__biglooz00,
		BgL_bgl_string1840za700za7za7_1915za7, "0123456789abcdef", 16);
	      DEFINE_STRING(BGl_string1841z00zz__biglooz00,
		BgL_bgl_string1841za700za7za7_1916za7, "bigloo-mangle-string", 20);
	      DEFINE_STRING(BGl_string1842z00zz__biglooz00,
		BgL_bgl_string1842za700za7za7_1917za7, "Can't mangle empty string", 25);
	      DEFINE_STRING(BGl_string1843z00zz__biglooz00,
		BgL_bgl_string1843za700za7za7_1918za7, "BgL_", 4);
	      DEFINE_STRING(BGl_string1844z00zz__biglooz00,
		BgL_bgl_string1844za700za7za7_1919za7, "&bigloo-mangle", 14);
	      DEFINE_STRING(BGl_string1845z00zz__biglooz00,
		BgL_bgl_string1845za700za7za7_1920za7, "BGl_", 4);
	      DEFINE_STRING(BGl_string1846z00zz__biglooz00,
		BgL_bgl_string1846za700za7za7_1921za7, "&bigloo-module-mangle", 21);
	      DEFINE_STRING(BGl_string1847z00zz__biglooz00,
		BgL_bgl_string1847za700za7za7_1922za7, "&bigloo-mangled?", 16);
	      DEFINE_STRING(BGl_string1848z00zz__biglooz00,
		BgL_bgl_string1848za700za7za7_1923za7, "&bigloo-need-mangling?", 22);
	      DEFINE_STRING(BGl_string1849z00zz__biglooz00,
		BgL_bgl_string1849za700za7za7_1924za7, "bigloo-demangle", 15);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cellzd2refzd2envz00zz__biglooz00,
		BgL_bgl_za762cellza7d2refza7b01925za7, BGl_z62cellzd2refzb0zz__biglooz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1850z00zz__biglooz00,
		BgL_bgl_string1850za700za7za7_1926za7, "Illegal mangling on", 19);
	      DEFINE_STRING(BGl_string1851z00zz__biglooz00,
		BgL_bgl_string1851za700za7za7_1927za7, "&bigloo-demangle", 16);
	      DEFINE_STRING(BGl_string1852z00zz__biglooz00,
		BgL_bgl_string1852za700za7za7_1928za7, "@", 1);
	      DEFINE_STRING(BGl_string1853z00zz__biglooz00,
		BgL_bgl_string1853za700za7za7_1929za7, "&bigloo-module-demangle", 23);
	      DEFINE_STRING(BGl_string1854z00zz__biglooz00,
		BgL_bgl_string1854za700za7za7_1930za7, "&bigloo-class-mangled?", 22);
	      DEFINE_STRING(BGl_string1855z00zz__biglooz00,
		BgL_bgl_string1855za700za7za7_1931za7, "_bglt", 5);
	      DEFINE_STRING(BGl_string1856z00zz__biglooz00,
		BgL_bgl_string1856za700za7za7_1932za7, "&bigloo-class-demangle", 22);
	      DEFINE_STRING(BGl_string1857z00zz__biglooz00,
		BgL_bgl_string1857za700za7za7_1933za7, "bigloo-exit-register!", 21);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_bigloozd2exitzd2applyzd2envzd2zz__biglooz00,
		BgL_bgl_za762biglooza7d2exit1934z00,
		BGl_z62bigloozd2exitzd2applyz62zz__biglooz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1858z00zz__biglooz00,
		BgL_bgl_string1858za700za7za7_1935za7, "Wrong procedure arity", 21);
	      DEFINE_STRING(BGl_string1859z00zz__biglooz00,
		BgL_bgl_string1859za700za7za7_1936za7, "&register-exit-function!", 24);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2classzd2demanglezd2envzd2zz__biglooz00,
		BgL_bgl_za762biglooza7d2clas1937z00,
		BGl_z62bigloozd2classzd2demanglez62zz__biglooz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_procedurezd2attrzd2setz12zd2envzc0zz__biglooz00,
		BgL_bgl_za762procedureza7d2a1938z00,
		BGl_z62procedurezd2attrzd2setz12z70zz__biglooz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_timezd2envzd2zz__biglooz00,
		BgL_bgl_za762timeza762za7za7__bi1939z00, BGl_z62timez62zz__biglooz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1860z00zz__biglooz00,
		BgL_bgl_string1860za700za7za7_1940za7, "&unregister-exit-function!", 26);
	      DEFINE_STRING(BGl_string1862z00zz__biglooz00,
		BgL_bgl_string1862za700za7za7_1941za7, "time", 4);
	      DEFINE_STRING(BGl_string1863z00zz__biglooz00,
		BgL_bgl_string1863za700za7za7_1942za7, "&time", 5);
	      DEFINE_STRING(BGl_string1866z00zz__biglooz00,
		BgL_bgl_string1866za700za7za7_1943za7, "finalize", 8);
	      DEFINE_STRING(BGl_string1868z00zz__biglooz00,
		BgL_bgl_string1868za700za7za7_1944za7, "gc", 2);
	      DEFINE_STRING(BGl_string1869z00zz__biglooz00,
		BgL_bgl_string1869za700za7za7_1945za7, "Illegal keyword argument", 24);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cellzd2setz12zd2envz12zz__biglooz00,
		BgL_bgl_za762cellza7d2setza7121946za7, BGl_z62cellzd2setz12za2zz__biglooz00,
		0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__biglooz00));
		     ADD_ROOT((void *) (&BGl_za2levelza2z00zz__biglooz00));
		     ADD_ROOT((void *) (&BGl_symbol1861z00zz__biglooz00));
		     ADD_ROOT((void *) (&BGl_symbol1867z00zz__biglooz00));
		     ADD_ROOT((void *) (&BGl_za2exitzd2mutexza2zd2zz__biglooz00));
		     ADD_ROOT((void *) (&BGl_list1864z00zz__biglooz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2bigloozd2exitzd2functionsza2z00zz__biglooz00));
		     ADD_ROOT((void *) (&BGl_za2releaseza2z00zz__biglooz00));
		     ADD_ROOT((void *) (&BGl_za2modulesza2z00zz__biglooz00));
		     ADD_ROOT((void *) (&BGl_keyword1865z00zz__biglooz00));
		     ADD_ROOT((void *) (&BGl_symbol1827z00zz__biglooz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long
		BgL_checksumz00_2421, char *BgL_fromz00_2422)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__biglooz00))
				{
					BGl_requirezd2initializa7ationz75zz__biglooz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__biglooz00();
					BGl_cnstzd2initzd2zz__biglooz00();
					BGl_importedzd2moduleszd2initz00zz__biglooz00();
					return BGl_toplevelzd2initzd2zz__biglooz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__biglooz00(void)
	{
		{	/* Llib/bigloo.scm 14 */
			BGl_symbol1827z00zz__biglooz00 =
				bstring_to_symbol(BGl_string1828z00zz__biglooz00);
			BGl_symbol1861z00zz__biglooz00 =
				bstring_to_symbol(BGl_string1862z00zz__biglooz00);
			BGl_keyword1865z00zz__biglooz00 =
				bstring_to_keyword(BGl_string1866z00zz__biglooz00);
			BGl_list1864z00zz__biglooz00 =
				MAKE_YOUNG_PAIR(BGl_keyword1865z00zz__biglooz00, BNIL);
			return (BGl_symbol1867z00zz__biglooz00 =
				bstring_to_symbol(BGl_string1868z00zz__biglooz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__biglooz00(void)
	{
		{	/* Llib/bigloo.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__biglooz00(void)
	{
		{	/* Llib/bigloo.scm 14 */
			BGl_za2releaseza2z00zz__biglooz00 = BFALSE;
			BGl_za2levelza2z00zz__biglooz00 = BFALSE;
			BGl_za2modulesza2z00zz__biglooz00 = BNIL;
			BGl_za2exitzd2mutexza2zd2zz__biglooz00 =
				bgl_make_mutex(BGl_symbol1827z00zz__biglooz00);
			return (BGl_za2bigloozd2exitzd2functionsza2z00zz__biglooz00 =
				BNIL, BUNSPEC);
		}

	}



/* check-version! */
	BGL_EXPORTED_DEF obj_t BGl_checkzd2versionz12zc0zz__biglooz00(obj_t
		BgL_modulez00_3, char *BgL_releasez00_4, obj_t BgL_levelz00_5)
	{
		{	/* Llib/bigloo.scm 339 */
			if (STRINGP(BGl_za2releaseza2z00zz__biglooz00))
				{	/* Llib/bigloo.scm 345 */
					bool_t BgL_test1949z00_2439;

					{	/* Llib/bigloo.scm 345 */
						bool_t BgL_test1950z00_2440;

						{	/* Llib/bigloo.scm 345 */
							long BgL_minz00_1145;

							{	/* Llib/bigloo.scm 345 */
								long BgL_arg1212z00_1148;

								{	/* Llib/bigloo.scm 345 */
									long BgL_az00_1149;
									long BgL_bz00_1150;

									BgL_az00_1149 =
										STRING_LENGTH(string_to_bstring(BgL_releasez00_4));
									BgL_bz00_1150 =
										STRING_LENGTH(BGl_za2releaseza2z00zz__biglooz00);
									if ((BgL_az00_1149 < BgL_bz00_1150))
										{	/* Llib/bigloo.scm 346 */
											BgL_arg1212z00_1148 = BgL_az00_1149;
										}
									else
										{	/* Llib/bigloo.scm 346 */
											BgL_arg1212z00_1148 = BgL_bz00_1150;
										}
								}
								BgL_minz00_1145 = (BgL_arg1212z00_1148 - 1L);
							}
							{	/* Llib/bigloo.scm 348 */
								obj_t BgL_arg1209z00_1146;
								obj_t BgL_arg1210z00_1147;

								BgL_arg1209z00_1146 =
									c_substring(string_to_bstring(BgL_releasez00_4), 0L,
									BgL_minz00_1145);
								BgL_arg1210z00_1147 =
									c_substring(BGl_za2releaseza2z00zz__biglooz00, 0L,
									BgL_minz00_1145);
								{	/* Llib/bigloo.scm 348 */
									long BgL_l1z00_1823;

									BgL_l1z00_1823 = STRING_LENGTH(BgL_arg1209z00_1146);
									if ((BgL_l1z00_1823 == STRING_LENGTH(BgL_arg1210z00_1147)))
										{	/* Llib/bigloo.scm 348 */
											int BgL_arg1472z00_1826;

											{	/* Llib/bigloo.scm 348 */
												char *BgL_auxz00_2456;
												char *BgL_tmpz00_2454;

												BgL_auxz00_2456 =
													BSTRING_TO_STRING(BgL_arg1210z00_1147);
												BgL_tmpz00_2454 =
													BSTRING_TO_STRING(BgL_arg1209z00_1146);
												BgL_arg1472z00_1826 =
													memcmp(BgL_tmpz00_2454, BgL_auxz00_2456,
													BgL_l1z00_1823);
											}
											BgL_test1950z00_2440 =
												((long) (BgL_arg1472z00_1826) == 0L);
										}
									else
										{	/* Llib/bigloo.scm 348 */
											BgL_test1950z00_2440 = ((bool_t) 0);
										}
								}
							}
						}
						if (BgL_test1950z00_2440)
							{	/* Llib/bigloo.scm 345 */
								if (CHARP(BgL_levelz00_5))
									{	/* Llib/bigloo.scm 350 */
										if (CHARP(BGl_za2levelza2z00zz__biglooz00))
											{	/* Llib/bigloo.scm 350 */
												if (
													(CCHAR(BGl_za2levelza2z00zz__biglooz00) ==
														CCHAR(BgL_levelz00_5)))
													{	/* Llib/bigloo.scm 350 */
														BgL_test1949z00_2439 = ((bool_t) 0);
													}
												else
													{	/* Llib/bigloo.scm 350 */
														BgL_test1949z00_2439 = ((bool_t) 1);
													}
											}
										else
											{	/* Llib/bigloo.scm 350 */
												BgL_test1949z00_2439 = ((bool_t) 0);
											}
									}
								else
									{	/* Llib/bigloo.scm 350 */
										BgL_test1949z00_2439 = ((bool_t) 0);
									}
							}
						else
							{	/* Llib/bigloo.scm 345 */
								BgL_test1949z00_2439 = ((bool_t) 1);
							}
					}
					if (BgL_test1949z00_2439)
						{	/* Llib/bigloo.scm 358 */
							obj_t BgL_arg1200z00_1130;
							obj_t BgL_arg1201z00_1131;
							obj_t BgL_arg1202z00_1132;

							{	/* Llib/bigloo.scm 358 */
								obj_t BgL_arg1203z00_1133;

								{	/* Llib/bigloo.scm 358 */
									obj_t BgL_releasez00_1839;
									obj_t BgL_levelz00_1840;

									BgL_releasez00_1839 = BGl_za2releaseza2z00zz__biglooz00;
									BgL_levelz00_1840 = BGl_za2levelza2z00zz__biglooz00;
									if (CHARP(BgL_levelz00_1840))
										{	/* Llib/bigloo.scm 353 */
											obj_t BgL_sz00_1842;

											BgL_sz00_1842 =
												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
												(BGl_string1829z00zz__biglooz00);
											{	/* Llib/bigloo.scm 354 */
												unsigned char BgL_tmpz00_2472;

												BgL_tmpz00_2472 = CCHAR(BgL_levelz00_1840);
												STRING_SET(BgL_sz00_1842, 8L, BgL_tmpz00_2472);
											}
											BgL_arg1203z00_1133 =
												string_append(BgL_releasez00_1839, BgL_sz00_1842);
										}
									else
										{	/* Llib/bigloo.scm 352 */
											BgL_arg1203z00_1133 = BgL_releasez00_1839;
										}
								}
								BgL_arg1200z00_1130 =
									string_append(BGl_string1830z00zz__biglooz00,
									BgL_arg1203z00_1133);
							}
							{	/* Llib/bigloo.scm 360 */
								obj_t BgL_arg1206z00_1134;

								if (CHARP(BgL_levelz00_5))
									{	/* Llib/bigloo.scm 353 */
										obj_t BgL_sz00_1846;

										BgL_sz00_1846 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BGl_string1829z00zz__biglooz00);
										{	/* Llib/bigloo.scm 354 */
											unsigned char BgL_tmpz00_2480;

											BgL_tmpz00_2480 = CCHAR(BgL_levelz00_5);
											STRING_SET(BgL_sz00_1846, 8L, BgL_tmpz00_2480);
										}
										BgL_arg1206z00_1134 =
											string_append(string_to_bstring(BgL_releasez00_4),
											BgL_sz00_1846);
									}
								else
									{	/* Llib/bigloo.scm 352 */
										BgL_arg1206z00_1134 = string_to_bstring(BgL_releasez00_4);
									}
								BgL_arg1201z00_1131 =
									string_append(BGl_string1831z00zz__biglooz00,
									BgL_arg1206z00_1134);
							}
							BgL_arg1202z00_1132 =
								MAKE_YOUNG_PAIR(BgL_modulez00_3,
								BGl_za2modulesza2z00zz__biglooz00);
							return BGl_errorz00zz__errorz00(BgL_arg1200z00_1130,
								BgL_arg1201z00_1131, BgL_arg1202z00_1132);
						}
					else
						{	/* Llib/bigloo.scm 345 */
							return (BGl_za2modulesza2z00zz__biglooz00 =
								MAKE_YOUNG_PAIR(BgL_modulez00_3,
									BGl_za2modulesza2z00zz__biglooz00), BUNSPEC);
						}
				}
			else
				{	/* Llib/bigloo.scm 341 */
					{	/* Llib/bigloo.scm 342 */
						obj_t BgL_list1213z00_1152;

						BgL_list1213z00_1152 = MAKE_YOUNG_PAIR(BgL_modulez00_3, BNIL);
						BGl_za2modulesza2z00zz__biglooz00 = BgL_list1213z00_1152;
					}
					BGl_za2releaseza2z00zz__biglooz00 =
						string_to_bstring(BgL_releasez00_4);
					return (BGl_za2levelza2z00zz__biglooz00 = BgL_levelz00_5, BUNSPEC);
				}
		}

	}



/* &check-version! */
	obj_t BGl_z62checkzd2versionz12za2zz__biglooz00(obj_t BgL_envz00_2315,
		obj_t BgL_modulez00_2316, obj_t BgL_releasez00_2317,
		obj_t BgL_levelz00_2318)
	{
		{	/* Llib/bigloo.scm 339 */
			{	/* Llib/bigloo.scm 341 */
				char *BgL_auxz00_2492;

				{	/* Llib/bigloo.scm 341 */
					obj_t BgL_tmpz00_2493;

					if (STRINGP(BgL_releasez00_2317))
						{	/* Llib/bigloo.scm 341 */
							BgL_tmpz00_2493 = BgL_releasez00_2317;
						}
					else
						{
							obj_t BgL_auxz00_2496;

							BgL_auxz00_2496 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1832z00zz__biglooz00,
								BINT(12535L), BGl_string1833z00zz__biglooz00,
								BGl_string1834z00zz__biglooz00, BgL_releasez00_2317);
							FAILURE(BgL_auxz00_2496, BFALSE, BFALSE);
						}
					BgL_auxz00_2492 = BSTRING_TO_STRING(BgL_tmpz00_2493);
				}
				return
					BGl_checkzd2versionz12zc0zz__biglooz00(BgL_modulez00_2316,
					BgL_auxz00_2492, BgL_levelz00_2318);
			}
		}

	}



/* procedure-arity */
	BGL_EXPORTED_DEF int BGl_procedurezd2arityzd2zz__biglooz00(obj_t
		BgL_procz00_6)
	{
		{	/* Llib/bigloo.scm 375 */
			return PROCEDURE_ARITY(BgL_procz00_6);
		}

	}



/* &procedure-arity */
	obj_t BGl_z62procedurezd2arityzb0zz__biglooz00(obj_t BgL_envz00_2319,
		obj_t BgL_procz00_2320)
	{
		{	/* Llib/bigloo.scm 375 */
			{	/* Llib/bigloo.scm 376 */
				int BgL_tmpz00_2503;

				{	/* Llib/bigloo.scm 376 */
					obj_t BgL_auxz00_2504;

					if (PROCEDUREP(BgL_procz00_2320))
						{	/* Llib/bigloo.scm 376 */
							BgL_auxz00_2504 = BgL_procz00_2320;
						}
					else
						{
							obj_t BgL_auxz00_2507;

							BgL_auxz00_2507 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1832z00zz__biglooz00,
								BINT(13952L), BGl_string1835z00zz__biglooz00,
								BGl_string1836z00zz__biglooz00, BgL_procz00_2320);
							FAILURE(BgL_auxz00_2507, BFALSE, BFALSE);
						}
					BgL_tmpz00_2503 =
						BGl_procedurezd2arityzd2zz__biglooz00(BgL_auxz00_2504);
				}
				return BINT(BgL_tmpz00_2503);
			}
		}

	}



/* procedure-length */
	BGL_EXPORTED_DEF int BGl_procedurezd2lengthzd2zz__biglooz00(obj_t
		BgL_procz00_7)
	{
		{	/* Llib/bigloo.scm 381 */
			return PROCEDURE_LENGTH(BgL_procz00_7);
		}

	}



/* &procedure-length */
	obj_t BGl_z62procedurezd2lengthzb0zz__biglooz00(obj_t BgL_envz00_2321,
		obj_t BgL_procz00_2322)
	{
		{	/* Llib/bigloo.scm 381 */
			{	/* Llib/bigloo.scm 382 */
				int BgL_tmpz00_2514;

				{	/* Llib/bigloo.scm 382 */
					obj_t BgL_auxz00_2515;

					if (PROCEDUREP(BgL_procz00_2322))
						{	/* Llib/bigloo.scm 382 */
							BgL_auxz00_2515 = BgL_procz00_2322;
						}
					else
						{
							obj_t BgL_auxz00_2518;

							BgL_auxz00_2518 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1832z00zz__biglooz00,
								BINT(14242L), BGl_string1837z00zz__biglooz00,
								BGl_string1836z00zz__biglooz00, BgL_procz00_2322);
							FAILURE(BgL_auxz00_2518, BFALSE, BFALSE);
						}
					BgL_tmpz00_2514 =
						BGl_procedurezd2lengthzd2zz__biglooz00(BgL_auxz00_2515);
				}
				return BINT(BgL_tmpz00_2514);
			}
		}

	}



/* procedure-attr */
	BGL_EXPORTED_DEF obj_t BGl_procedurezd2attrzd2zz__biglooz00(obj_t
		BgL_procz00_8)
	{
		{	/* Llib/bigloo.scm 387 */
			return PROCEDURE_ATTR(BgL_procz00_8);
		}

	}



/* &procedure-attr */
	obj_t BGl_z62procedurezd2attrzb0zz__biglooz00(obj_t BgL_envz00_2323,
		obj_t BgL_procz00_2324)
	{
		{	/* Llib/bigloo.scm 387 */
			{	/* Llib/bigloo.scm 388 */
				obj_t BgL_auxz00_2525;

				if (PROCEDUREP(BgL_procz00_2324))
					{	/* Llib/bigloo.scm 388 */
						BgL_auxz00_2525 = BgL_procz00_2324;
					}
				else
					{
						obj_t BgL_auxz00_2528;

						BgL_auxz00_2528 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1832z00zz__biglooz00,
							BINT(14531L), BGl_string1838z00zz__biglooz00,
							BGl_string1836z00zz__biglooz00, BgL_procz00_2324);
						FAILURE(BgL_auxz00_2528, BFALSE, BFALSE);
					}
				return BGl_procedurezd2attrzd2zz__biglooz00(BgL_auxz00_2525);
			}
		}

	}



/* procedure-attr-set! */
	BGL_EXPORTED_DEF obj_t BGl_procedurezd2attrzd2setz12z12zz__biglooz00(obj_t
		BgL_procz00_9, obj_t BgL_objz00_10)
	{
		{	/* Llib/bigloo.scm 393 */
			PROCEDURE_ATTR_SET(BgL_procz00_9, BgL_objz00_10);
			return BgL_objz00_10;
		}

	}



/* &procedure-attr-set! */
	obj_t BGl_z62procedurezd2attrzd2setz12z70zz__biglooz00(obj_t BgL_envz00_2325,
		obj_t BgL_procz00_2326, obj_t BgL_objz00_2327)
	{
		{	/* Llib/bigloo.scm 393 */
			{	/* Llib/bigloo.scm 394 */
				obj_t BgL_auxz00_2534;

				if (PROCEDUREP(BgL_procz00_2326))
					{	/* Llib/bigloo.scm 394 */
						BgL_auxz00_2534 = BgL_procz00_2326;
					}
				else
					{
						obj_t BgL_auxz00_2537;

						BgL_auxz00_2537 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1832z00zz__biglooz00,
							BINT(14857L), BGl_string1839z00zz__biglooz00,
							BGl_string1836z00zz__biglooz00, BgL_procz00_2326);
						FAILURE(BgL_auxz00_2537, BFALSE, BFALSE);
					}
				return
					BGl_procedurezd2attrzd2setz12z12zz__biglooz00(BgL_auxz00_2534,
					BgL_objz00_2327);
			}
		}

	}



/* unspecified */
	BGL_EXPORTED_DEF obj_t BGl_unspecifiedz00zz__biglooz00(void)
	{
		{	/* Llib/bigloo.scm 400 */
			return BUNSPEC;
		}

	}



/* &unspecified */
	obj_t BGl_z62unspecifiedz62zz__biglooz00(obj_t BgL_envz00_2328)
	{
		{	/* Llib/bigloo.scm 400 */
			return BGl_unspecifiedz00zz__biglooz00();
		}

	}



/* null-or-unspecified? */
	BGL_EXPORTED_DEF bool_t BGl_nullzd2orzd2unspecifiedzf3zf3zz__biglooz00(obj_t
		BgL_objz00_11)
	{
		{	/* Llib/bigloo.scm 406 */
			return BGL_NULL_OR_UNSPECIFIEDP(BgL_objz00_11);
		}

	}



/* &null-or-unspecified? */
	obj_t BGl_z62nullzd2orzd2unspecifiedzf3z91zz__biglooz00(obj_t BgL_envz00_2329,
		obj_t BgL_objz00_2330)
	{
		{	/* Llib/bigloo.scm 406 */
			return
				BBOOL(BGl_nullzd2orzd2unspecifiedzf3zf3zz__biglooz00(BgL_objz00_2330));
		}

	}



/* cnst? */
	BGL_EXPORTED_DEF bool_t BGl_cnstzf3zf3zz__biglooz00(obj_t BgL_objz00_12)
	{
		{	/* Llib/bigloo.scm 418 */
			return CNSTP(BgL_objz00_12);
		}

	}



/* &cnst? */
	obj_t BGl_z62cnstzf3z91zz__biglooz00(obj_t BgL_envz00_2331,
		obj_t BgL_objz00_2332)
	{
		{	/* Llib/bigloo.scm 418 */
			return BBOOL(BGl_cnstzf3zf3zz__biglooz00(BgL_objz00_2332));
		}

	}



/* opaque? */
	BGL_EXPORTED_DEF bool_t BGl_opaquezf3zf3zz__biglooz00(obj_t BgL_objz00_13)
	{
		{	/* Llib/bigloo.scm 424 */
			return OPAQUEP(BgL_objz00_13);
		}

	}



/* &opaque? */
	obj_t BGl_z62opaquezf3z91zz__biglooz00(obj_t BgL_envz00_2333,
		obj_t BgL_objz00_2334)
	{
		{	/* Llib/bigloo.scm 424 */
			return BBOOL(BGl_opaquezf3zf3zz__biglooz00(BgL_objz00_2334));
		}

	}



/* opaque-nil */
	BGL_EXPORTED_DEF obj_t BGl_opaquezd2nilzd2zz__biglooz00(void)
	{
		{	/* Llib/bigloo.scm 430 */
			return BGL_OPAQUE_NIL();
		}

	}



/* &opaque-nil */
	obj_t BGl_z62opaquezd2nilzb0zz__biglooz00(obj_t BgL_envz00_2335)
	{
		{	/* Llib/bigloo.scm 430 */
			return BGl_opaquezd2nilzd2zz__biglooz00();
		}

	}



/* mangle-at! */
	long BGl_manglezd2atz12zc0zz__biglooz00(obj_t BgL_newz00_15,
		obj_t BgL_oldz00_16, long BgL_lenz00_17, long BgL_offsetz00_18)
	{
		{	/* Llib/bigloo.scm 443 */
			{
				long BgL_rz00_1155;
				long BgL_wz00_1156;
				long BgL_newzd2lenzd2_1157;
				long BgL_checksumz00_1158;

				BgL_rz00_1155 = 0L;
				BgL_wz00_1156 = BgL_offsetz00_18;
				BgL_newzd2lenzd2_1157 = BgL_offsetz00_18;
				BgL_checksumz00_1158 = 0L;
			BgL_zc3z04anonymousza31214ze3z87_1159:
				if ((BgL_rz00_1155 == BgL_lenz00_17))
					{	/* Llib/bigloo.scm 448 */
						STRING_SET(BgL_newz00_15, BgL_wz00_1156, ((unsigned char) 'z'));
						{	/* Llib/bigloo.scm 451 */
							unsigned char BgL_auxz00_2559;
							long BgL_tmpz00_2557;

							BgL_auxz00_2559 =
								STRING_REF(BGl_string1840z00zz__biglooz00,
								(BgL_checksumz00_1158 & 15L));
							BgL_tmpz00_2557 = (BgL_wz00_1156 + 1L);
							STRING_SET(BgL_newz00_15, BgL_tmpz00_2557, BgL_auxz00_2559);
						}
						{	/* Llib/bigloo.scm 454 */
							unsigned char BgL_auxz00_2565;
							long BgL_tmpz00_2563;

							BgL_auxz00_2565 =
								STRING_REF(BGl_string1840z00zz__biglooz00,
								((BgL_checksumz00_1158 >> (int) (4L)) & 15L));
							BgL_tmpz00_2563 = (BgL_wz00_1156 + 2L);
							STRING_SET(BgL_newz00_15, BgL_tmpz00_2563, BgL_auxz00_2565);
						}
						return (BgL_wz00_1156 + 3L);
					}
				else
					{	/* Llib/bigloo.scm 458 */
						unsigned char BgL_cz00_1168;

						BgL_cz00_1168 = STRING_REF(BgL_oldz00_16, BgL_rz00_1155);
						{	/* Llib/bigloo.scm 459 */
							bool_t BgL_test1964z00_2573;

							{	/* Llib/bigloo.scm 459 */
								bool_t BgL_test1965z00_2574;

								if (isalpha(BgL_cz00_1168))
									{	/* Llib/bigloo.scm 459 */
										if ((BgL_cz00_1168 == ((unsigned char) 'z')))
											{	/* Llib/bigloo.scm 459 */
												BgL_test1965z00_2574 = ((bool_t) 0);
											}
										else
											{	/* Llib/bigloo.scm 459 */
												BgL_test1965z00_2574 = ((bool_t) 1);
											}
									}
								else
									{	/* Llib/bigloo.scm 459 */
										BgL_test1965z00_2574 = ((bool_t) 0);
									}
								if (BgL_test1965z00_2574)
									{	/* Llib/bigloo.scm 459 */
										BgL_test1964z00_2573 = ((bool_t) 1);
									}
								else
									{	/* Llib/bigloo.scm 459 */
										if (isdigit(BgL_cz00_1168))
											{	/* Llib/bigloo.scm 460 */
												BgL_test1964z00_2573 = ((bool_t) 1);
											}
										else
											{	/* Llib/bigloo.scm 460 */
												BgL_test1964z00_2573 =
													(BgL_cz00_1168 == ((unsigned char) '_'));
							}}}
							if (BgL_test1964z00_2573)
								{	/* Llib/bigloo.scm 459 */
									STRING_SET(BgL_newz00_15, BgL_wz00_1156, BgL_cz00_1168);
									{
										long BgL_newzd2lenzd2_2587;
										long BgL_wz00_2585;
										long BgL_rz00_2583;

										BgL_rz00_2583 = (BgL_rz00_1155 + 1L);
										BgL_wz00_2585 = (BgL_wz00_1156 + 1L);
										BgL_newzd2lenzd2_2587 = (BgL_newzd2lenzd2_1157 + 1L);
										BgL_newzd2lenzd2_1157 = BgL_newzd2lenzd2_2587;
										BgL_wz00_1156 = BgL_wz00_2585;
										BgL_rz00_1155 = BgL_rz00_2583;
										goto BgL_zc3z04anonymousza31214ze3z87_1159;
									}
								}
							else
								{	/* Llib/bigloo.scm 468 */
									long BgL_icz00_1179;

									BgL_icz00_1179 = (BgL_cz00_1168);
									STRING_SET(BgL_newz00_15, BgL_wz00_1156,
										((unsigned char) 'z'));
									{	/* Llib/bigloo.scm 470 */
										unsigned char BgL_auxz00_2593;
										long BgL_tmpz00_2591;

										BgL_auxz00_2593 =
											STRING_REF(BGl_string1840z00zz__biglooz00,
											(BgL_icz00_1179 & 15L));
										BgL_tmpz00_2591 = (BgL_wz00_1156 + 1L);
										STRING_SET(BgL_newz00_15, BgL_tmpz00_2591, BgL_auxz00_2593);
									}
									{	/* Llib/bigloo.scm 473 */
										unsigned char BgL_auxz00_2599;
										long BgL_tmpz00_2597;

										BgL_auxz00_2599 =
											STRING_REF(BGl_string1840z00zz__biglooz00,
											((BgL_icz00_1179 >> (int) (4L)) & 15L));
										BgL_tmpz00_2597 = (BgL_wz00_1156 + 2L);
										STRING_SET(BgL_newz00_15, BgL_tmpz00_2597, BgL_auxz00_2599);
									}
									{
										long BgL_checksumz00_2611;
										long BgL_newzd2lenzd2_2609;
										long BgL_wz00_2607;
										long BgL_rz00_2605;

										BgL_rz00_2605 = (BgL_rz00_1155 + 1L);
										BgL_wz00_2607 = (BgL_wz00_1156 + 3L);
										BgL_newzd2lenzd2_2609 = (BgL_newzd2lenzd2_1157 + 3L);
										BgL_checksumz00_2611 =
											(BgL_checksumz00_1158 ^ (BgL_cz00_1168));
										BgL_checksumz00_1158 = BgL_checksumz00_2611;
										BgL_newzd2lenzd2_1157 = BgL_newzd2lenzd2_2609;
										BgL_wz00_1156 = BgL_wz00_2607;
										BgL_rz00_1155 = BgL_rz00_2605;
										goto BgL_zc3z04anonymousza31214ze3z87_1159;
									}
								}
						}
					}
			}
		}

	}



/* bigloo-mangle */
	BGL_EXPORTED_DEF obj_t bigloo_mangle(obj_t BgL_stringz00_19)
	{
		{	/* Llib/bigloo.scm 484 */
			{	/* Llib/bigloo.scm 485 */
				long BgL_lenz00_1197;

				BgL_lenz00_1197 = STRING_LENGTH(BgL_stringz00_19);
				{	/* Llib/bigloo.scm 485 */
					obj_t BgL_newz00_1198;

					{	/* Llib/bigloo.scm 486 */
						long BgL_arg1304z00_1201;

						BgL_arg1304z00_1201 = ((BgL_lenz00_1197 * 3L) + 7L);
						{	/* Ieee/string.scm 172 */

							BgL_newz00_1198 =
								make_string(BgL_arg1304z00_1201, ((unsigned char) ' '));
					}}
					{	/* Llib/bigloo.scm 486 */

						if ((BgL_lenz00_1197 == 0L))
							{	/* Llib/bigloo.scm 487 */
								return
									BGl_errorz00zz__errorz00(BGl_string1841z00zz__biglooz00,
									BGl_string1842z00zz__biglooz00, BgL_stringz00_19);
							}
						else
							{	/* Llib/bigloo.scm 489 */
								long BgL_stopz00_1200;

								BgL_stopz00_1200 =
									BGl_manglezd2atz12zc0zz__biglooz00(BgL_newz00_1198,
									BgL_stringz00_19, BgL_lenz00_1197, 4L);
								blit_string(BGl_string1843z00zz__biglooz00, 0L, BgL_newz00_1198,
									0L, 4L);
								return c_substring(BgL_newz00_1198, 0L, BgL_stopz00_1200);
							}
					}
				}
			}
		}

	}



/* &bigloo-mangle */
	obj_t BGl_z62bigloozd2manglezb0zz__biglooz00(obj_t BgL_envz00_2336,
		obj_t BgL_stringz00_2337)
	{
		{	/* Llib/bigloo.scm 484 */
			{	/* Llib/bigloo.scm 485 */
				obj_t BgL_auxz00_2624;

				if (STRINGP(BgL_stringz00_2337))
					{	/* Llib/bigloo.scm 485 */
						BgL_auxz00_2624 = BgL_stringz00_2337;
					}
				else
					{
						obj_t BgL_auxz00_2627;

						BgL_auxz00_2627 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1832z00zz__biglooz00,
							BINT(18215L), BGl_string1844z00zz__biglooz00,
							BGl_string1834z00zz__biglooz00, BgL_stringz00_2337);
						FAILURE(BgL_auxz00_2627, BFALSE, BFALSE);
					}
				return bigloo_mangle(BgL_auxz00_2624);
			}
		}

	}



/* bigloo-module-mangle */
	BGL_EXPORTED_DEF obj_t bigloo_module_mangle(obj_t BgL_idz00_20,
		obj_t BgL_modulez00_21)
	{
		{	/* Llib/bigloo.scm 496 */
			{	/* Llib/bigloo.scm 497 */
				long BgL_lenz00_1205;

				BgL_lenz00_1205 =
					(STRING_LENGTH(BgL_idz00_20) + STRING_LENGTH(BgL_modulez00_21));
				{	/* Llib/bigloo.scm 497 */
					obj_t BgL_newz00_1206;

					{	/* Llib/bigloo.scm 498 */
						long BgL_arg1311z00_1214;

						BgL_arg1311z00_1214 = ((BgL_lenz00_1205 * 3L) + 12L);
						{	/* Ieee/string.scm 172 */

							BgL_newz00_1206 =
								make_string(BgL_arg1311z00_1214, ((unsigned char) ' '));
					}}
					{	/* Llib/bigloo.scm 498 */

						if ((BgL_lenz00_1205 == 0L))
							{	/* Llib/bigloo.scm 499 */
								return
									BGl_errorz00zz__errorz00(BGl_string1841z00zz__biglooz00,
									BGl_string1842z00zz__biglooz00,
									BGl_stringzd2envzd2zz__r4_strings_6_7z00);
							}
						else
							{	/* Llib/bigloo.scm 501 */
								long BgL_modzd2startzd2_1208;

								BgL_modzd2startzd2_1208 =
									BGl_manglezd2atz12zc0zz__biglooz00(BgL_newz00_1206,
									BgL_idz00_20, STRING_LENGTH(BgL_idz00_20), 4L);
								STRING_SET(BgL_newz00_1206, BgL_modzd2startzd2_1208,
									((unsigned char) 'z'));
								{	/* Llib/bigloo.scm 503 */
									long BgL_tmpz00_2644;

									BgL_tmpz00_2644 = (1L + BgL_modzd2startzd2_1208);
									STRING_SET(BgL_newz00_1206, BgL_tmpz00_2644,
										((unsigned char) 'z'));
								}
								{	/* Llib/bigloo.scm 504 */
									long BgL_stopz00_1210;

									BgL_stopz00_1210 =
										BGl_manglezd2atz12zc0zz__biglooz00(BgL_newz00_1206,
										BgL_modulez00_21, STRING_LENGTH(BgL_modulez00_21),
										(BgL_modzd2startzd2_1208 + 2L));
									blit_string(BGl_string1845z00zz__biglooz00, 0L,
										BgL_newz00_1206, 0L, 4L);
									return c_substring(BgL_newz00_1206, 0L, BgL_stopz00_1210);
								}
							}
					}
				}
			}
		}

	}



/* &bigloo-module-mangle */
	obj_t BGl_z62bigloozd2modulezd2manglez62zz__biglooz00(obj_t BgL_envz00_2338,
		obj_t BgL_idz00_2339, obj_t BgL_modulez00_2340)
	{
		{	/* Llib/bigloo.scm 496 */
			{	/* Llib/bigloo.scm 497 */
				obj_t BgL_auxz00_2659;
				obj_t BgL_auxz00_2652;

				if (STRINGP(BgL_modulez00_2340))
					{	/* Llib/bigloo.scm 497 */
						BgL_auxz00_2659 = BgL_modulez00_2340;
					}
				else
					{
						obj_t BgL_auxz00_2662;

						BgL_auxz00_2662 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1832z00zz__biglooz00,
							BINT(18771L), BGl_string1846z00zz__biglooz00,
							BGl_string1834z00zz__biglooz00, BgL_modulez00_2340);
						FAILURE(BgL_auxz00_2662, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_idz00_2339))
					{	/* Llib/bigloo.scm 497 */
						BgL_auxz00_2652 = BgL_idz00_2339;
					}
				else
					{
						obj_t BgL_auxz00_2655;

						BgL_auxz00_2655 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1832z00zz__biglooz00,
							BINT(18771L), BGl_string1846z00zz__biglooz00,
							BGl_string1834z00zz__biglooz00, BgL_idz00_2339);
						FAILURE(BgL_auxz00_2655, BFALSE, BFALSE);
					}
				return bigloo_module_mangle(BgL_auxz00_2652, BgL_auxz00_2659);
			}
		}

	}



/* bigloo-mangled? */
	BGL_EXPORTED_DEF bool_t bigloo_mangledp(obj_t BgL_stringz00_22)
	{
		{	/* Llib/bigloo.scm 513 */
			{	/* Llib/bigloo.scm 514 */
				long BgL_lenz00_1220;

				BgL_lenz00_1220 = STRING_LENGTH(BgL_stringz00_22);
				if ((BgL_lenz00_1220 > 7L))
					{	/* Llib/bigloo.scm 516 */
						bool_t BgL_test1975z00_2670;

						{	/* Llib/bigloo.scm 516 */
							bool_t BgL__ortest_1045z00_1237;

							BgL__ortest_1045z00_1237 =
								bigloo_strncmp(BgL_stringz00_22, BGl_string1843z00zz__biglooz00,
								4L);
							if (BgL__ortest_1045z00_1237)
								{	/* Llib/bigloo.scm 516 */
									BgL_test1975z00_2670 = BgL__ortest_1045z00_1237;
								}
							else
								{	/* Llib/bigloo.scm 516 */
									BgL_test1975z00_2670 =
										bigloo_strncmp(BgL_stringz00_22,
										BGl_string1845z00zz__biglooz00, 4L);
								}
						}
						if (BgL_test1975z00_2670)
							{	/* Llib/bigloo.scm 516 */
								if (
									(STRING_REF(BgL_stringz00_22,
											(BgL_lenz00_1220 - 3L)) == ((unsigned char) 'z')))
									{	/* Llib/bigloo.scm 519 */
										bool_t BgL_test1978z00_2678;

										{	/* Llib/bigloo.scm 519 */
											bool_t BgL__ortest_1046z00_1230;

											{	/* Llib/bigloo.scm 519 */
												unsigned char BgL_tmpz00_2679;

												BgL_tmpz00_2679 =
													STRING_REF(BgL_stringz00_22, (BgL_lenz00_1220 - 2L));
												BgL__ortest_1046z00_1230 = isalpha(BgL_tmpz00_2679);
											}
											if (BgL__ortest_1046z00_1230)
												{	/* Llib/bigloo.scm 519 */
													BgL_test1978z00_2678 = BgL__ortest_1046z00_1230;
												}
											else
												{	/* Llib/bigloo.scm 520 */
													unsigned char BgL_tmpz00_2684;

													BgL_tmpz00_2684 =
														STRING_REF(BgL_stringz00_22,
														(BgL_lenz00_1220 - 2L));
													BgL_test1978z00_2678 = isdigit(BgL_tmpz00_2684);
										}}
										if (BgL_test1978z00_2678)
											{	/* Llib/bigloo.scm 521 */
												bool_t BgL__ortest_1047z00_1225;

												{	/* Llib/bigloo.scm 521 */
													unsigned char BgL_tmpz00_2688;

													BgL_tmpz00_2688 =
														STRING_REF(BgL_stringz00_22,
														(BgL_lenz00_1220 - 1L));
													BgL__ortest_1047z00_1225 = isalpha(BgL_tmpz00_2688);
												}
												if (BgL__ortest_1047z00_1225)
													{	/* Llib/bigloo.scm 521 */
														return BgL__ortest_1047z00_1225;
													}
												else
													{	/* Llib/bigloo.scm 522 */
														unsigned char BgL_tmpz00_2693;

														BgL_tmpz00_2693 =
															STRING_REF(BgL_stringz00_22,
															(BgL_lenz00_1220 - 1L));
														return isdigit(BgL_tmpz00_2693);
													}
											}
										else
											{	/* Llib/bigloo.scm 519 */
												return ((bool_t) 0);
											}
									}
								else
									{	/* Llib/bigloo.scm 518 */
										return ((bool_t) 0);
									}
							}
						else
							{	/* Llib/bigloo.scm 516 */
								return ((bool_t) 0);
							}
					}
				else
					{	/* Llib/bigloo.scm 515 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &bigloo-mangled? */
	obj_t BGl_z62bigloozd2mangledzf3z43zz__biglooz00(obj_t BgL_envz00_2343,
		obj_t BgL_stringz00_2344)
	{
		{	/* Llib/bigloo.scm 513 */
			{	/* Llib/bigloo.scm 514 */
				bool_t BgL_tmpz00_2697;

				{	/* Llib/bigloo.scm 514 */
					obj_t BgL_auxz00_2698;

					if (STRINGP(BgL_stringz00_2344))
						{	/* Llib/bigloo.scm 514 */
							BgL_auxz00_2698 = BgL_stringz00_2344;
						}
					else
						{
							obj_t BgL_auxz00_2701;

							BgL_auxz00_2701 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1832z00zz__biglooz00,
								BINT(19541L), BGl_string1847z00zz__biglooz00,
								BGl_string1834z00zz__biglooz00, BgL_stringz00_2344);
							FAILURE(BgL_auxz00_2701, BFALSE, BFALSE);
						}
					BgL_tmpz00_2697 = bigloo_mangledp(BgL_auxz00_2698);
				}
				return BBOOL(BgL_tmpz00_2697);
			}
		}

	}



/* bigloo-need-mangling? */
	BGL_EXPORTED_DEF bool_t BGl_bigloozd2needzd2manglingzf3zf3zz__biglooz00(obj_t
		BgL_stringz00_23)
	{
		{	/* Llib/bigloo.scm 527 */
			{	/* Llib/bigloo.scm 528 */
				long BgL_lenz00_1238;

				BgL_lenz00_1238 = STRING_LENGTH(BgL_stringz00_23);
				if ((BgL_lenz00_1238 > 0L))
					{	/* Llib/bigloo.scm 530 */
						bool_t BgL__ortest_1049z00_1240;

						{	/* Llib/bigloo.scm 530 */
							bool_t BgL_test1983z00_2710;

							{	/* Llib/bigloo.scm 530 */
								bool_t BgL_test1984z00_2711;

								{	/* Llib/bigloo.scm 530 */
									unsigned char BgL_tmpz00_2712;

									BgL_tmpz00_2712 = STRING_REF(BgL_stringz00_23, 0L);
									BgL_test1984z00_2711 = isalpha(BgL_tmpz00_2712);
								}
								if (BgL_test1984z00_2711)
									{	/* Llib/bigloo.scm 530 */
										BgL_test1983z00_2710 = ((bool_t) 1);
									}
								else
									{	/* Llib/bigloo.scm 530 */
										BgL_test1983z00_2710 =
											(STRING_REF(BgL_stringz00_23,
												0L) == ((unsigned char) '_'));
							}}
							if (BgL_test1983z00_2710)
								{	/* Llib/bigloo.scm 530 */
									BgL__ortest_1049z00_1240 = ((bool_t) 0);
								}
							else
								{	/* Llib/bigloo.scm 530 */
									BgL__ortest_1049z00_1240 = ((bool_t) 1);
								}
						}
						if (BgL__ortest_1049z00_1240)
							{	/* Llib/bigloo.scm 530 */
								return BgL__ortest_1049z00_1240;
							}
						else
							{
								long BgL_iz00_1242;

								BgL_iz00_1242 = 1L;
							BgL_zc3z04anonymousza31327ze3z87_1243:
								if ((BgL_iz00_1242 >= BgL_lenz00_1238))
									{	/* Llib/bigloo.scm 533 */
										return ((bool_t) 0);
									}
								else
									{	/* Llib/bigloo.scm 535 */
										unsigned char BgL_cz00_1245;

										BgL_cz00_1245 = STRING_REF(BgL_stringz00_23, BgL_iz00_1242);
										{	/* Llib/bigloo.scm 536 */
											bool_t BgL_test1987z00_2721;

											if (isalpha(BgL_cz00_1245))
												{	/* Llib/bigloo.scm 536 */
													BgL_test1987z00_2721 = ((bool_t) 1);
												}
											else
												{	/* Llib/bigloo.scm 536 */
													if (isdigit(BgL_cz00_1245))
														{	/* Llib/bigloo.scm 537 */
															BgL_test1987z00_2721 = ((bool_t) 1);
														}
													else
														{	/* Llib/bigloo.scm 537 */
															BgL_test1987z00_2721 =
																(BgL_cz00_1245 == ((unsigned char) '_'));
												}}
											if (BgL_test1987z00_2721)
												{
													long BgL_iz00_2727;

													BgL_iz00_2727 = (BgL_iz00_1242 + 1L);
													BgL_iz00_1242 = BgL_iz00_2727;
													goto BgL_zc3z04anonymousza31327ze3z87_1243;
												}
											else
												{	/* Llib/bigloo.scm 536 */
													return ((bool_t) 1);
												}
										}
									}
							}
					}
				else
					{	/* Llib/bigloo.scm 529 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &bigloo-need-mangling? */
	obj_t BGl_z62bigloozd2needzd2manglingzf3z91zz__biglooz00(obj_t
		BgL_envz00_2345, obj_t BgL_stringz00_2346)
	{
		{	/* Llib/bigloo.scm 527 */
			{	/* Llib/bigloo.scm 528 */
				bool_t BgL_tmpz00_2729;

				{	/* Llib/bigloo.scm 528 */
					obj_t BgL_auxz00_2730;

					if (STRINGP(BgL_stringz00_2346))
						{	/* Llib/bigloo.scm 528 */
							BgL_auxz00_2730 = BgL_stringz00_2346;
						}
					else
						{
							obj_t BgL_auxz00_2733;

							BgL_auxz00_2733 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1832z00zz__biglooz00,
								BINT(20226L), BGl_string1848z00zz__biglooz00,
								BGl_string1834z00zz__biglooz00, BgL_stringz00_2346);
							FAILURE(BgL_auxz00_2733, BFALSE, BFALSE);
						}
					BgL_tmpz00_2729 =
						BGl_bigloozd2needzd2manglingzf3zf3zz__biglooz00(BgL_auxz00_2730);
				}
				return BBOOL(BgL_tmpz00_2729);
			}
		}

	}



/* bigloo-demangle */
	BGL_EXPORTED_DEF obj_t bigloo_demangle(obj_t BgL_stringz00_24)
	{
		{	/* Llib/bigloo.scm 545 */
			{	/* Llib/bigloo.scm 546 */
				long BgL_lenz00_1261;

				BgL_lenz00_1261 = STRING_LENGTH(BgL_stringz00_24);
				{	/* Llib/bigloo.scm 546 */
					long BgL_clenz00_1262;

					BgL_clenz00_1262 = (BgL_lenz00_1261 - 3L);
					{	/* Llib/bigloo.scm 547 */

						{

							if ((BgL_lenz00_1261 < 8L))
								{	/* Llib/bigloo.scm 596 */
									return BgL_stringz00_24;
								}
							else
								{	/* Llib/bigloo.scm 596 */
									if (bigloo_strncmp(BgL_stringz00_24,
											BGl_string1843z00zz__biglooz00, 4L))
										{	/* Llib/bigloo.scm 598 */
											{	/* Llib/bigloo.scm 586 */
												obj_t BgL_strz00_1323;

												BgL_strz00_1323 =
													BGl_bigloozd2demanglezd2atze70ze7zz__biglooz00
													(BgL_stringz00_24, BgL_clenz00_1262, BINT(4L));
												{	/* Llib/bigloo.scm 587 */
													obj_t BgL_offsetz00_1324;

													{	/* Llib/bigloo.scm 588 */
														obj_t BgL_tmpz00_2042;

														{	/* Llib/bigloo.scm 588 */
															int BgL_tmpz00_2747;

															BgL_tmpz00_2747 = (int) (1L);
															BgL_tmpz00_2042 =
																BGL_MVALUES_VAL(BgL_tmpz00_2747);
														}
														{	/* Llib/bigloo.scm 588 */
															int BgL_tmpz00_2750;

															BgL_tmpz00_2750 = (int) (1L);
															BGL_MVALUES_VAL_SET(BgL_tmpz00_2750, BUNSPEC);
														}
														BgL_offsetz00_1324 = BgL_tmpz00_2042;
													}
													{	/* Llib/bigloo.scm 588 */
														int BgL_tmpz00_2753;

														BgL_tmpz00_2753 = (int) (2L);
														BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2753);
													}
													{	/* Llib/bigloo.scm 588 */
														int BgL_tmpz00_2756;

														BgL_tmpz00_2756 = (int) (1L);
														BGL_MVALUES_VAL_SET(BgL_tmpz00_2756, BUNSPEC);
													}
													return BgL_strz00_1323;
												}
											}
										}
									else
										{	/* Llib/bigloo.scm 598 */
											if (bigloo_strncmp(BgL_stringz00_24,
													BGl_string1845z00zz__biglooz00, 4L))
												{	/* Llib/bigloo.scm 600 */
													{	/* Llib/bigloo.scm 590 */
														obj_t BgL_idz00_1328;

														BgL_idz00_1328 =
															BGl_bigloozd2demanglezd2atze70ze7zz__biglooz00
															(BgL_stringz00_24, BgL_clenz00_1262, BINT(4L));
														{	/* Llib/bigloo.scm 591 */
															obj_t BgL_offsetz00_1329;

															{	/* Llib/bigloo.scm 592 */
																obj_t BgL_tmpz00_2043;

																{	/* Llib/bigloo.scm 592 */
																	int BgL_tmpz00_2763;

																	BgL_tmpz00_2763 = (int) (1L);
																	BgL_tmpz00_2043 =
																		BGL_MVALUES_VAL(BgL_tmpz00_2763);
																}
																{	/* Llib/bigloo.scm 592 */
																	int BgL_tmpz00_2766;

																	BgL_tmpz00_2766 = (int) (1L);
																	BGL_MVALUES_VAL_SET(BgL_tmpz00_2766, BUNSPEC);
																}
																BgL_offsetz00_1329 = BgL_tmpz00_2043;
															}
															{	/* Llib/bigloo.scm 592 */
																obj_t BgL_modulez00_1330;

																BgL_modulez00_1330 =
																	BGl_bigloozd2demanglezd2atze70ze7zz__biglooz00
																	(BgL_stringz00_24, BgL_clenz00_1262,
																	BgL_offsetz00_1329);
																{	/* Llib/bigloo.scm 593 */
																	obj_t BgL_offsetz00_1331;

																	{	/* Llib/bigloo.scm 594 */
																		obj_t BgL_tmpz00_2044;

																		{	/* Llib/bigloo.scm 594 */
																			int BgL_tmpz00_2770;

																			BgL_tmpz00_2770 = (int) (1L);
																			BgL_tmpz00_2044 =
																				BGL_MVALUES_VAL(BgL_tmpz00_2770);
																		}
																		{	/* Llib/bigloo.scm 594 */
																			int BgL_tmpz00_2773;

																			BgL_tmpz00_2773 = (int) (1L);
																			BGL_MVALUES_VAL_SET(BgL_tmpz00_2773,
																				BUNSPEC);
																		}
																		BgL_offsetz00_1331 = BgL_tmpz00_2044;
																	}
																	{	/* Llib/bigloo.scm 594 */
																		int BgL_tmpz00_2776;

																		BgL_tmpz00_2776 = (int) (2L);
																		BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2776);
																	}
																	{	/* Llib/bigloo.scm 594 */
																		int BgL_tmpz00_2779;

																		BgL_tmpz00_2779 = (int) (1L);
																		BGL_MVALUES_VAL_SET(BgL_tmpz00_2779,
																			BgL_modulez00_1330);
																	}
																	return BgL_idz00_1328;
																}
															}
														}
													}
												}
											else
												{	/* Llib/bigloo.scm 600 */
													return BgL_stringz00_24;
												}
										}
								}
						}
					}
				}
			}
		}

	}



/* char->digit~0 */
	long BGl_charzd2ze3digitze70zd6zz__biglooz00(unsigned char BgL_cz00_1273)
	{
		{	/* Llib/bigloo.scm 553 */
			if (isdigit(BgL_cz00_1273))
				{	/* Llib/bigloo.scm 551 */
					return ((BgL_cz00_1273) - 48L);
				}
			else
				{	/* Llib/bigloo.scm 551 */
					return (10L + ((BgL_cz00_1273) - 97L));
				}
		}

	}



/* get-8bits-integer~0 */
	long BGl_getzd28bitszd2integerze70ze7zz__biglooz00(obj_t BgL_stringz00_2377,
		obj_t BgL_rz00_1279)
	{
		{	/* Llib/bigloo.scm 559 */
			{	/* Llib/bigloo.scm 558 */

				return
					(BGl_charzd2ze3digitze70zd6zz__biglooz00(STRING_REF
						(BgL_stringz00_2377,
							((long) CINT(BgL_rz00_1279) + 1L))) +
					(BGl_charzd2ze3digitze70zd6zz__biglooz00(STRING_REF
							(BgL_stringz00_2377,
								((long) CINT(BgL_rz00_1279) + 2L))) << (int) (4L)));
		}}

	}



/* bigloo-demangle-at~0 */
	obj_t BGl_bigloozd2demanglezd2atze70ze7zz__biglooz00(obj_t BgL_stringz00_2379,
		long BgL_clenz00_2378, obj_t BgL_offsetz00_1288)
	{
		{	/* Llib/bigloo.scm 584 */
			{	/* Llib/bigloo.scm 561 */
				obj_t BgL_newz00_1290;

				{	/* Ieee/string.scm 172 */

					BgL_newz00_1290 =
						make_string(BgL_clenz00_2378, ((unsigned char) ' '));
				}
				{
					obj_t BgL_rz00_1292;
					long BgL_wz00_1293;
					long BgL_checksumz00_1294;

					BgL_rz00_1292 = BgL_offsetz00_1288;
					BgL_wz00_1293 = 0L;
					BgL_checksumz00_1294 = 0L;
				BgL_zc3z04anonymousza31354ze3z87_1295:
					if (((long) CINT(BgL_rz00_1292) == BgL_clenz00_2378))
						{	/* Llib/bigloo.scm 565 */
							if (
								(BgL_checksumz00_1294 ==
									BGl_getzd28bitszd2integerze70ze7zz__biglooz00
									(BgL_stringz00_2379, BgL_rz00_1292)))
								{	/* Llib/bigloo.scm 568 */
									obj_t BgL_val0_1087z00_1299;
									long BgL_val1_1088z00_1300;

									BgL_val0_1087z00_1299 =
										c_substring(BgL_newz00_1290, 0L, BgL_wz00_1293);
									BgL_val1_1088z00_1300 = ((long) CINT(BgL_rz00_1292) + 3L);
									{	/* Llib/bigloo.scm 568 */
										int BgL_tmpz00_2810;

										BgL_tmpz00_2810 = (int) (2L);
										BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2810);
									}
									{	/* Llib/bigloo.scm 568 */
										obj_t BgL_auxz00_2815;
										int BgL_tmpz00_2813;

										BgL_auxz00_2815 = BINT(BgL_val1_1088z00_1300);
										BgL_tmpz00_2813 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_2813, BgL_auxz00_2815);
									}
									return BgL_val0_1087z00_1299;
								}
							else
								{	/* Llib/bigloo.scm 567 */
									return
										BGl_errorz00zz__errorz00(BGl_string1849z00zz__biglooz00,
										BGl_string1850z00zz__biglooz00, BgL_stringz00_2379);
								}
						}
					else
						{	/* Llib/bigloo.scm 570 */
							unsigned char BgL_cz00_1302;

							BgL_cz00_1302 =
								STRING_REF(BgL_stringz00_2379, (long) CINT(BgL_rz00_1292));
							if ((BgL_cz00_1302 == ((unsigned char) 'z')))
								{	/* Llib/bigloo.scm 571 */
									if (
										(STRING_REF(BgL_stringz00_2379,
												((long) CINT(BgL_rz00_1292) + 1L)) ==
											((unsigned char) 'z')))
										{	/* Llib/bigloo.scm 573 */
											obj_t BgL_val0_1089z00_1307;
											long BgL_val1_1090z00_1308;

											BgL_val0_1089z00_1307 =
												c_substring(BgL_newz00_1290, 0L, (BgL_wz00_1293 - 1L));
											BgL_val1_1090z00_1308 = ((long) CINT(BgL_rz00_1292) + 2L);
											{	/* Llib/bigloo.scm 573 */
												int BgL_tmpz00_2832;

												BgL_tmpz00_2832 = (int) (2L);
												BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2832);
											}
											{	/* Llib/bigloo.scm 573 */
												obj_t BgL_auxz00_2837;
												int BgL_tmpz00_2835;

												BgL_auxz00_2837 = BINT(BgL_val1_1090z00_1308);
												BgL_tmpz00_2835 = (int) (1L);
												BGL_MVALUES_VAL_SET(BgL_tmpz00_2835, BgL_auxz00_2837);
											}
											return BgL_val0_1089z00_1307;
										}
									else
										{	/* Llib/bigloo.scm 574 */
											long BgL_iz00_1310;

											BgL_iz00_1310 =
												BGl_getzd28bitszd2integerze70ze7zz__biglooz00
												(BgL_stringz00_2379, BgL_rz00_1292);
											{	/* Llib/bigloo.scm 574 */
												unsigned char BgL_ncz00_1311;

												BgL_ncz00_1311 = (BgL_iz00_1310);
												{	/* Llib/bigloo.scm 575 */

													STRING_SET(BgL_newz00_1290, BgL_wz00_1293,
														BgL_ncz00_1311);
													{	/* Llib/bigloo.scm 577 */
														long BgL_arg1364z00_1312;
														long BgL_arg1365z00_1313;
														long BgL_arg1366z00_1314;

														BgL_arg1364z00_1312 =
															((long) CINT(BgL_rz00_1292) + 3L);
														BgL_arg1365z00_1313 = (BgL_wz00_1293 + 1L);
														BgL_arg1366z00_1314 =
															(BgL_checksumz00_1294 ^ BgL_iz00_1310);
														{
															long BgL_checksumz00_2850;
															long BgL_wz00_2849;
															obj_t BgL_rz00_2847;

															BgL_rz00_2847 = BINT(BgL_arg1364z00_1312);
															BgL_wz00_2849 = BgL_arg1365z00_1313;
															BgL_checksumz00_2850 = BgL_arg1366z00_1314;
															BgL_checksumz00_1294 = BgL_checksumz00_2850;
															BgL_wz00_1293 = BgL_wz00_2849;
															BgL_rz00_1292 = BgL_rz00_2847;
															goto BgL_zc3z04anonymousza31354ze3z87_1295;
														}
													}
												}
											}
										}
								}
							else
								{	/* Llib/bigloo.scm 571 */
									STRING_SET(BgL_newz00_1290, BgL_wz00_1293, BgL_cz00_1302);
									{	/* Llib/bigloo.scm 582 */
										long BgL_arg1369z00_1317;
										long BgL_arg1370z00_1318;

										BgL_arg1369z00_1317 = ((long) CINT(BgL_rz00_1292) + 1L);
										BgL_arg1370z00_1318 = (BgL_wz00_1293 + 1L);
										{
											long BgL_wz00_2857;
											obj_t BgL_rz00_2855;

											BgL_rz00_2855 = BINT(BgL_arg1369z00_1317);
											BgL_wz00_2857 = BgL_arg1370z00_1318;
											BgL_wz00_1293 = BgL_wz00_2857;
											BgL_rz00_1292 = BgL_rz00_2855;
											goto BgL_zc3z04anonymousza31354ze3z87_1295;
										}
									}
								}
						}
				}
			}
		}

	}



/* &bigloo-demangle */
	obj_t BGl_z62bigloozd2demanglezb0zz__biglooz00(obj_t BgL_envz00_2347,
		obj_t BgL_stringz00_2348)
	{
		{	/* Llib/bigloo.scm 545 */
			{	/* Llib/bigloo.scm 546 */
				obj_t BgL_auxz00_2858;

				if (STRINGP(BgL_stringz00_2348))
					{	/* Llib/bigloo.scm 546 */
						BgL_auxz00_2858 = BgL_stringz00_2348;
					}
				else
					{
						obj_t BgL_auxz00_2861;

						BgL_auxz00_2861 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1832z00zz__biglooz00,
							BINT(20869L), BGl_string1851z00zz__biglooz00,
							BGl_string1834z00zz__biglooz00, BgL_stringz00_2348);
						FAILURE(BgL_auxz00_2861, BFALSE, BFALSE);
					}
				return bigloo_demangle(BgL_auxz00_2858);
			}
		}

	}



/* bigloo-module-demangle */
	BGL_EXPORTED_DEF obj_t bigloo_module_demangle(obj_t BgL_stringz00_25)
	{
		{	/* Llib/bigloo.scm 608 */
			{	/* Llib/bigloo.scm 609 */
				obj_t BgL_idz00_2050;

				BgL_idz00_2050 = bigloo_demangle(BgL_stringz00_25);
				{	/* Llib/bigloo.scm 610 */
					obj_t BgL_modulez00_2051;

					{	/* Llib/bigloo.scm 611 */
						obj_t BgL_tmpz00_2053;

						{	/* Llib/bigloo.scm 611 */
							int BgL_tmpz00_2867;

							BgL_tmpz00_2867 = (int) (1L);
							BgL_tmpz00_2053 = BGL_MVALUES_VAL(BgL_tmpz00_2867);
						}
						{	/* Llib/bigloo.scm 611 */
							int BgL_tmpz00_2870;

							BgL_tmpz00_2870 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_2870, BUNSPEC);
						}
						BgL_modulez00_2051 = BgL_tmpz00_2053;
					}
					if (STRINGP(BgL_modulez00_2051))
						{	/* Llib/bigloo.scm 611 */
							return
								string_append_3(BgL_idz00_2050, BGl_string1852z00zz__biglooz00,
								BgL_modulez00_2051);
						}
					else
						{	/* Llib/bigloo.scm 611 */
							return BgL_idz00_2050;
						}
				}
			}
		}

	}



/* &bigloo-module-demangle */
	obj_t BGl_z62bigloozd2modulezd2demanglez62zz__biglooz00(obj_t BgL_envz00_2349,
		obj_t BgL_stringz00_2350)
	{
		{	/* Llib/bigloo.scm 608 */
			{	/* Llib/bigloo.scm 609 */
				obj_t BgL_auxz00_2876;

				if (STRINGP(BgL_stringz00_2350))
					{	/* Llib/bigloo.scm 609 */
						BgL_auxz00_2876 = BgL_stringz00_2350;
					}
				else
					{
						obj_t BgL_auxz00_2879;

						BgL_auxz00_2879 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1832z00zz__biglooz00,
							BINT(22945L), BGl_string1853z00zz__biglooz00,
							BGl_string1834z00zz__biglooz00, BgL_stringz00_2350);
						FAILURE(BgL_auxz00_2879, BFALSE, BFALSE);
					}
				return bigloo_module_demangle(BgL_auxz00_2876);
			}
		}

	}



/* bigloo-class-mangled? */
	BGL_EXPORTED_DEF bool_t bigloo_class_mangledp(obj_t BgL_stringz00_26)
	{
		{	/* Llib/bigloo.scm 618 */
			{	/* Llib/bigloo.scm 619 */
				long BgL_lenz00_1343;

				BgL_lenz00_1343 = STRING_LENGTH(BgL_stringz00_26);
				if ((BgL_lenz00_1343 > 8L))
					{	/* Llib/bigloo.scm 620 */
						if (
							(STRING_REF(BgL_stringz00_26,
									(BgL_lenz00_1343 - 1L)) == ((unsigned char) 't')))
							{	/* Llib/bigloo.scm 621 */
								if (
									(STRING_REF(BgL_stringz00_26,
											(BgL_lenz00_1343 - 2L)) == ((unsigned char) 'l')))
									{	/* Llib/bigloo.scm 622 */
										if (
											(STRING_REF(BgL_stringz00_26,
													(BgL_lenz00_1343 - 3L)) == ((unsigned char) 'g')))
											{	/* Llib/bigloo.scm 623 */
												if (
													(STRING_REF(BgL_stringz00_26,
															(BgL_lenz00_1343 - 4L)) == ((unsigned char) 'b')))
													{	/* Llib/bigloo.scm 624 */
														if (
															(STRING_REF(BgL_stringz00_26,
																	(BgL_lenz00_1343 - 5L)) ==
																((unsigned char) '_')))
															{	/* Llib/bigloo.scm 625 */
																BGL_TAIL return
																	bigloo_mangledp(c_substring(BgL_stringz00_26,
																		0L, (BgL_lenz00_1343 - 5L)));
															}
														else
															{	/* Llib/bigloo.scm 625 */
																return ((bool_t) 0);
															}
													}
												else
													{	/* Llib/bigloo.scm 624 */
														return ((bool_t) 0);
													}
											}
										else
											{	/* Llib/bigloo.scm 623 */
												return ((bool_t) 0);
											}
									}
								else
									{	/* Llib/bigloo.scm 622 */
										return ((bool_t) 0);
									}
							}
						else
							{	/* Llib/bigloo.scm 621 */
								return ((bool_t) 0);
							}
					}
				else
					{	/* Llib/bigloo.scm 620 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &bigloo-class-mangled? */
	obj_t BGl_z62bigloozd2classzd2mangledzf3z91zz__biglooz00(obj_t
		BgL_envz00_2351, obj_t BgL_stringz00_2352)
	{
		{	/* Llib/bigloo.scm 618 */
			{	/* Llib/bigloo.scm 619 */
				bool_t BgL_tmpz00_2910;

				{	/* Llib/bigloo.scm 619 */
					obj_t BgL_auxz00_2911;

					if (STRINGP(BgL_stringz00_2352))
						{	/* Llib/bigloo.scm 619 */
							BgL_auxz00_2911 = BgL_stringz00_2352;
						}
					else
						{
							obj_t BgL_auxz00_2914;

							BgL_auxz00_2914 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1832z00zz__biglooz00,
								BINT(23343L), BGl_string1854z00zz__biglooz00,
								BGl_string1834z00zz__biglooz00, BgL_stringz00_2352);
							FAILURE(BgL_auxz00_2914, BFALSE, BFALSE);
						}
					BgL_tmpz00_2910 = bigloo_class_mangledp(BgL_auxz00_2911);
				}
				return BBOOL(BgL_tmpz00_2910);
			}
		}

	}



/* bigloo-class-demangle */
	BGL_EXPORTED_DEF obj_t bigloo_class_demangle(obj_t BgL_stringz00_27)
	{
		{	/* Llib/bigloo.scm 631 */
			return
				string_append(bigloo_demangle(c_substring(BgL_stringz00_27, 0L,
						(STRING_LENGTH(BgL_stringz00_27) - 5L))),
				BGl_string1855z00zz__biglooz00);
		}

	}



/* &bigloo-class-demangle */
	obj_t BGl_z62bigloozd2classzd2demanglez62zz__biglooz00(obj_t BgL_envz00_2353,
		obj_t BgL_stringz00_2354)
	{
		{	/* Llib/bigloo.scm 631 */
			{	/* Llib/bigloo.scm 633 */
				obj_t BgL_auxz00_2925;

				if (STRINGP(BgL_stringz00_2354))
					{	/* Llib/bigloo.scm 633 */
						BgL_auxz00_2925 = BgL_stringz00_2354;
					}
				else
					{
						obj_t BgL_auxz00_2928;

						BgL_auxz00_2928 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1832z00zz__biglooz00,
							BINT(24035L), BGl_string1856z00zz__biglooz00,
							BGl_string1834z00zz__biglooz00, BgL_stringz00_2354);
						FAILURE(BgL_auxz00_2928, BFALSE, BFALSE);
					}
				return bigloo_class_demangle(BgL_auxz00_2925);
			}
		}

	}



/* bigloo-exit-mutex */
	BGL_EXPORTED_DEF obj_t bgl_exit_mutex(void)
	{
		{	/* Llib/bigloo.scm 644 */
			return BGl_za2exitzd2mutexza2zd2zz__biglooz00;
		}

	}



/* &bigloo-exit-mutex */
	obj_t BGl_z62bigloozd2exitzd2mutexz62zz__biglooz00(obj_t BgL_envz00_2355)
	{
		{	/* Llib/bigloo.scm 644 */
			return bgl_exit_mutex();
		}

	}



/* register-exit-function! */
	BGL_EXPORTED_DEF obj_t BGl_registerzd2exitzd2functionz12z12zz__biglooz00(obj_t
		BgL_funz00_28)
	{
		{	/* Llib/bigloo.scm 655 */
			{	/* Llib/bigloo.scm 656 */
				obj_t BgL_mutex1394z00_1366;

				BgL_mutex1394z00_1366 = BGl_za2exitzd2mutexza2zd2zz__biglooz00;
				{	/* Llib/bigloo.scm 656 */
					obj_t BgL_top2011z00_2935;

					BgL_top2011z00_2935 = BGL_EXITD_TOP_AS_OBJ();
					BGL_MUTEX_LOCK(BgL_mutex1394z00_1366);
					BGL_EXITD_PUSH_PROTECT(BgL_top2011z00_2935, BgL_mutex1394z00_1366);
					BUNSPEC;
					{	/* Llib/bigloo.scm 656 */
						obj_t BgL_tmp2010z00_2934;

						if (PROCEDURE_CORRECT_ARITYP(BgL_funz00_28, (int) (1L)))
							{	/* Llib/bigloo.scm 657 */
								BgL_tmp2010z00_2934 =
									(BGl_za2bigloozd2exitzd2functionsza2z00zz__biglooz00 =
									MAKE_YOUNG_PAIR(BgL_funz00_28,
										BGl_za2bigloozd2exitzd2functionsza2z00zz__biglooz00),
									BUNSPEC);
							}
						else
							{	/* Llib/bigloo.scm 657 */
								BgL_tmp2010z00_2934 =
									BGl_errorz00zz__errorz00(BGl_string1857z00zz__biglooz00,
									BGl_string1858z00zz__biglooz00, BgL_funz00_28);
							}
						BGL_EXITD_POP_PROTECT(BgL_top2011z00_2935);
						BUNSPEC;
						BGL_MUTEX_UNLOCK(BgL_mutex1394z00_1366);
						return BgL_tmp2010z00_2934;
					}
				}
			}
		}

	}



/* &register-exit-function! */
	obj_t BGl_z62registerzd2exitzd2functionz12z70zz__biglooz00(obj_t
		BgL_envz00_2356, obj_t BgL_funz00_2357)
	{
		{	/* Llib/bigloo.scm 655 */
			{	/* Llib/bigloo.scm 656 */
				obj_t BgL_auxz00_2946;

				if (PROCEDUREP(BgL_funz00_2357))
					{	/* Llib/bigloo.scm 656 */
						BgL_auxz00_2946 = BgL_funz00_2357;
					}
				else
					{
						obj_t BgL_auxz00_2949;

						BgL_auxz00_2949 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1832z00zz__biglooz00,
							BINT(25140L), BGl_string1859z00zz__biglooz00,
							BGl_string1836z00zz__biglooz00, BgL_funz00_2357);
						FAILURE(BgL_auxz00_2949, BFALSE, BFALSE);
					}
				return
					BGl_registerzd2exitzd2functionz12z12zz__biglooz00(BgL_auxz00_2946);
			}
		}

	}



/* unregister-exit-function! */
	BGL_EXPORTED_DEF obj_t
		BGl_unregisterzd2exitzd2functionz12z12zz__biglooz00(obj_t BgL_funz00_29)
	{
		{	/* Llib/bigloo.scm 666 */
			{	/* Llib/bigloo.scm 667 */
				obj_t BgL_mutex1395z00_2092;

				BgL_mutex1395z00_2092 = BGl_za2exitzd2mutexza2zd2zz__biglooz00;
				{	/* Llib/bigloo.scm 667 */
					obj_t BgL_top2015z00_2955;

					BgL_top2015z00_2955 = BGL_EXITD_TOP_AS_OBJ();
					BGL_MUTEX_LOCK(BgL_mutex1395z00_2092);
					BGL_EXITD_PUSH_PROTECT(BgL_top2015z00_2955, BgL_mutex1395z00_2092);
					BUNSPEC;
					{	/* Llib/bigloo.scm 667 */
						obj_t BgL_tmp2014z00_2954;

						BgL_tmp2014z00_2954 =
							(BGl_za2bigloozd2exitzd2functionsza2z00zz__biglooz00 =
							bgl_remq_bang(BgL_funz00_29,
								BGl_za2bigloozd2exitzd2functionsza2z00zz__biglooz00), BUNSPEC);
						BGL_EXITD_POP_PROTECT(BgL_top2015z00_2955);
						BUNSPEC;
						BGL_MUTEX_UNLOCK(BgL_mutex1395z00_2092);
						return BgL_tmp2014z00_2954;
					}
				}
			}
		}

	}



/* &unregister-exit-function! */
	obj_t BGl_z62unregisterzd2exitzd2functionz12z70zz__biglooz00(obj_t
		BgL_envz00_2358, obj_t BgL_funz00_2359)
	{
		{	/* Llib/bigloo.scm 666 */
			{	/* Llib/bigloo.scm 667 */
				obj_t BgL_auxz00_2962;

				if (PROCEDUREP(BgL_funz00_2359))
					{	/* Llib/bigloo.scm 667 */
						BgL_auxz00_2962 = BgL_funz00_2359;
					}
				else
					{
						obj_t BgL_auxz00_2965;

						BgL_auxz00_2965 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1832z00zz__biglooz00,
							BINT(25625L), BGl_string1860z00zz__biglooz00,
							BGl_string1836z00zz__biglooz00, BgL_funz00_2359);
						FAILURE(BgL_auxz00_2965, BFALSE, BFALSE);
					}
				return
					BGl_unregisterzd2exitzd2functionz12z12zz__biglooz00(BgL_auxz00_2962);
			}
		}

	}



/* bigloo-exit-apply */
	BGL_EXPORTED_DEF obj_t bigloo_exit_apply(obj_t BgL_valz00_30)
	{
		{	/* Llib/bigloo.scm 673 */
			{	/* Llib/bigloo.scm 674 */
				obj_t BgL_mutz00_1368;

				if (BGL_MUTEXP(BGl_za2exitzd2mutexza2zd2zz__biglooz00))
					{	/* Llib/bigloo.scm 674 */
						BgL_mutz00_1368 = BGl_za2exitzd2mutexza2zd2zz__biglooz00;
					}
				else
					{	/* Llib/bigloo.scm 674 */
						BgL_mutz00_1368 = bgl_make_mutex(BGl_symbol1827z00zz__biglooz00);
					}
				{	/* Llib/bigloo.scm 677 */
					obj_t BgL_top2019z00_2974;

					BgL_top2019z00_2974 = BGL_EXITD_TOP_AS_OBJ();
					BGL_MUTEX_LOCK(BgL_mutz00_1368);
					BGL_EXITD_PUSH_PROTECT(BgL_top2019z00_2974, BgL_mutz00_1368);
					BUNSPEC;
					{	/* Llib/bigloo.scm 677 */
						obj_t BgL_tmp2018z00_2973;

						{
							obj_t BgL_valz00_1370;

							BgL_valz00_1370 = BgL_valz00_30;
						BgL_zc3z04anonymousza31396ze3z87_1371:
							{	/* Llib/bigloo.scm 679 */
								obj_t BgL_valz00_1372;

								if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
									(BgL_valz00_1370))
									{	/* Llib/bigloo.scm 679 */
										BgL_valz00_1372 = BgL_valz00_1370;
									}
								else
									{	/* Llib/bigloo.scm 679 */
										BgL_valz00_1372 = BINT(0L);
									}
								if (PAIRP(BGl_za2bigloozd2exitzd2functionsza2z00zz__biglooz00))
									{	/* Llib/bigloo.scm 683 */
										obj_t BgL_funz00_1374;

										BgL_funz00_1374 =
											CAR(BGl_za2bigloozd2exitzd2functionsza2z00zz__biglooz00);
										BGl_za2bigloozd2exitzd2functionsza2z00zz__biglooz00 =
											CDR(BGl_za2bigloozd2exitzd2functionsza2z00zz__biglooz00);
										{	/* Llib/bigloo.scm 686 */
											obj_t BgL_nvalz00_1375;

											BgL_nvalz00_1375 =
												BGL_PROCEDURE_CALL1(BgL_funz00_1374, BgL_valz00_1372);
											{	/* Llib/bigloo.scm 687 */
												obj_t BgL_arg1399z00_1376;

												if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
													(BgL_nvalz00_1375))
													{	/* Llib/bigloo.scm 687 */
														BgL_arg1399z00_1376 = BgL_nvalz00_1375;
													}
												else
													{	/* Llib/bigloo.scm 687 */
														BgL_arg1399z00_1376 = BgL_valz00_1372;
													}
												{
													obj_t BgL_valz00_2991;

													BgL_valz00_2991 = BgL_arg1399z00_1376;
													BgL_valz00_1370 = BgL_valz00_2991;
													goto BgL_zc3z04anonymousza31396ze3z87_1371;
												}
											}
										}
									}
								else
									{	/* Llib/bigloo.scm 682 */
										BgL_tmp2018z00_2973 = BgL_valz00_1372;
									}
							}
						}
						BGL_EXITD_POP_PROTECT(BgL_top2019z00_2974);
						BUNSPEC;
						BGL_MUTEX_UNLOCK(BgL_mutz00_1368);
						return BgL_tmp2018z00_2973;
					}
				}
			}
		}

	}



/* &bigloo-exit-apply */
	obj_t BGl_z62bigloozd2exitzd2applyz62zz__biglooz00(obj_t BgL_envz00_2360,
		obj_t BgL_valz00_2361)
	{
		{	/* Llib/bigloo.scm 673 */
			return bigloo_exit_apply(BgL_valz00_2361);
		}

	}



/* time */
	BGL_EXPORTED_DEF obj_t BGl_timez00zz__biglooz00(obj_t BgL_procz00_31)
	{
		{	/* Llib/bigloo.scm 693 */
			if (PROCEDURE_CORRECT_ARITYP(BgL_procz00_31, (int) (0L)))
				{	/* Llib/bigloo.scm 694 */
					return bgl_time(BgL_procz00_31);
				}
			else
				{	/* Llib/bigloo.scm 694 */
					return
						BGl_errorz00zz__errorz00(BGl_symbol1861z00zz__biglooz00,
						BGl_string1858z00zz__biglooz00, BgL_procz00_31);
				}
		}

	}



/* &time */
	obj_t BGl_z62timez62zz__biglooz00(obj_t BgL_envz00_2362,
		obj_t BgL_procz00_2363)
	{
		{	/* Llib/bigloo.scm 693 */
			{	/* Llib/bigloo.scm 694 */
				obj_t BgL_auxz00_3000;

				if (PROCEDUREP(BgL_procz00_2363))
					{	/* Llib/bigloo.scm 694 */
						BgL_auxz00_3000 = BgL_procz00_2363;
					}
				else
					{
						obj_t BgL_auxz00_3003;

						BgL_auxz00_3003 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1832z00zz__biglooz00,
							BINT(26696L), BGl_string1863z00zz__biglooz00,
							BGl_string1836z00zz__biglooz00, BgL_procz00_2363);
						FAILURE(BgL_auxz00_3003, BFALSE, BFALSE);
					}
				return BGl_timez00zz__biglooz00(BgL_auxz00_3000);
			}
		}

	}



/* bmem-reset! */
	BGL_EXPORTED_DEF obj_t BGl_bmemzd2resetz12zc0zz__biglooz00(void)
	{
		{	/* Llib/bigloo.scm 709 */
			BGL_TAIL return bgl_bmem_reset();
		}

	}



/* &bmem-reset! */
	obj_t BGl_z62bmemzd2resetz12za2zz__biglooz00(obj_t BgL_envz00_2364)
	{
		{	/* Llib/bigloo.scm 709 */
			return BGl_bmemzd2resetz12zc0zz__biglooz00();
		}

	}



/* gc-verbose-set! */
	BGL_EXPORTED_DEF obj_t BGl_gczd2verbosezd2setz12z12zz__biglooz00(bool_t
		BgL_boolz00_32)
	{
		{	/* Llib/bigloo.scm 717 */
			bgl_gc_verbose_set(BgL_boolz00_32);
			return BUNSPEC;
		}

	}



/* &gc-verbose-set! */
	obj_t BGl_z62gczd2verbosezd2setz12z70zz__biglooz00(obj_t BgL_envz00_2365,
		obj_t BgL_boolz00_2366)
	{
		{	/* Llib/bigloo.scm 717 */
			return BGl_gczd2verbosezd2setz12z12zz__biglooz00(CBOOL(BgL_boolz00_2366));
		}

	}



/* _gc */
	obj_t BGl__gcz00zz__biglooz00(obj_t BgL_env1110z00_35,
		obj_t BgL_opt1109z00_34)
	{
		{	/* Llib/bigloo.scm 725 */
			{	/* Llib/bigloo.scm 725 */

				{	/* Llib/bigloo.scm 725 */
					obj_t BgL_finaliza7eza7_1387;

					BgL_finaliza7eza7_1387 = BTRUE;
					{	/* Llib/bigloo.scm 725 */

						{
							long BgL_iz00_1388;

							BgL_iz00_1388 = 0L;
						BgL_check1113z00_1389:
							if ((BgL_iz00_1388 == VECTOR_LENGTH(BgL_opt1109z00_34)))
								{	/* Llib/bigloo.scm 725 */
									BNIL;
								}
							else
								{	/* Llib/bigloo.scm 725 */
									bool_t BgL_test2026z00_3016;

									{	/* Llib/bigloo.scm 725 */
										obj_t BgL_arg1411z00_1395;

										BgL_arg1411z00_1395 =
											VECTOR_REF(BgL_opt1109z00_34, BgL_iz00_1388);
										BgL_test2026z00_3016 =
											CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
											(BgL_arg1411z00_1395, BGl_list1864z00zz__biglooz00));
									}
									if (BgL_test2026z00_3016)
										{
											long BgL_iz00_3020;

											BgL_iz00_3020 = (BgL_iz00_1388 + 2L);
											BgL_iz00_1388 = BgL_iz00_3020;
											goto BgL_check1113z00_1389;
										}
									else
										{	/* Llib/bigloo.scm 725 */
											obj_t BgL_arg1410z00_1394;

											BgL_arg1410z00_1394 =
												VECTOR_REF(BgL_opt1109z00_34, BgL_iz00_1388);
											BGl_errorz00zz__errorz00(BGl_symbol1867z00zz__biglooz00,
												BGl_string1869z00zz__biglooz00, BgL_arg1410z00_1394);
										}
								}
						}
						{	/* Llib/bigloo.scm 725 */
							obj_t BgL_index1115z00_1396;

							{
								long BgL_iz00_2113;

								BgL_iz00_2113 = 0L;
							BgL_search1112z00_2112:
								if ((BgL_iz00_2113 == VECTOR_LENGTH(BgL_opt1109z00_34)))
									{	/* Llib/bigloo.scm 725 */
										BgL_index1115z00_1396 = BINT(-1L);
									}
								else
									{	/* Llib/bigloo.scm 725 */
										if (
											(BgL_iz00_2113 ==
												(VECTOR_LENGTH(BgL_opt1109z00_34) - 1L)))
											{	/* Llib/bigloo.scm 725 */
												BgL_index1115z00_1396 =
													BGl_errorz00zz__errorz00
													(BGl_symbol1867z00zz__biglooz00,
													BGl_string1870z00zz__biglooz00,
													BINT(VECTOR_LENGTH(BgL_opt1109z00_34)));
											}
										else
											{	/* Llib/bigloo.scm 725 */
												obj_t BgL_vz00_2123;

												BgL_vz00_2123 =
													VECTOR_REF(BgL_opt1109z00_34, BgL_iz00_2113);
												if ((BgL_vz00_2123 == BGl_keyword1865z00zz__biglooz00))
													{	/* Llib/bigloo.scm 725 */
														BgL_index1115z00_1396 = BINT((BgL_iz00_2113 + 1L));
													}
												else
													{
														long BgL_iz00_3040;

														BgL_iz00_3040 = (BgL_iz00_2113 + 2L);
														BgL_iz00_2113 = BgL_iz00_3040;
														goto BgL_search1112z00_2112;
													}
											}
									}
							}
							{	/* Llib/bigloo.scm 725 */
								bool_t BgL_test2030z00_3042;

								{	/* Llib/bigloo.scm 725 */
									long BgL_n1z00_2127;

									{	/* Llib/bigloo.scm 725 */
										obj_t BgL_tmpz00_3043;

										if (INTEGERP(BgL_index1115z00_1396))
											{	/* Llib/bigloo.scm 725 */
												BgL_tmpz00_3043 = BgL_index1115z00_1396;
											}
										else
											{
												obj_t BgL_auxz00_3046;

												BgL_auxz00_3046 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string1832z00zz__biglooz00, BINT(28046L),
													BGl_string1871z00zz__biglooz00,
													BGl_string1872z00zz__biglooz00,
													BgL_index1115z00_1396);
												FAILURE(BgL_auxz00_3046, BFALSE, BFALSE);
											}
										BgL_n1z00_2127 = (long) CINT(BgL_tmpz00_3043);
									}
									BgL_test2030z00_3042 = (BgL_n1z00_2127 >= 0L);
								}
								if (BgL_test2030z00_3042)
									{
										long BgL_auxz00_3052;

										{	/* Llib/bigloo.scm 725 */
											obj_t BgL_tmpz00_3053;

											if (INTEGERP(BgL_index1115z00_1396))
												{	/* Llib/bigloo.scm 725 */
													BgL_tmpz00_3053 = BgL_index1115z00_1396;
												}
											else
												{
													obj_t BgL_auxz00_3056;

													BgL_auxz00_3056 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string1832z00zz__biglooz00, BINT(28046L),
														BGl_string1871z00zz__biglooz00,
														BGl_string1872z00zz__biglooz00,
														BgL_index1115z00_1396);
													FAILURE(BgL_auxz00_3056, BFALSE, BFALSE);
												}
											BgL_auxz00_3052 = (long) CINT(BgL_tmpz00_3053);
										}
										BgL_finaliza7eza7_1387 =
											VECTOR_REF(BgL_opt1109z00_34, BgL_auxz00_3052);
									}
								else
									{	/* Llib/bigloo.scm 725 */
										BFALSE;
									}
							}
						}
						{	/* Llib/bigloo.scm 725 */
							obj_t BgL_finaliza7eza7_1398;

							BgL_finaliza7eza7_1398 = BgL_finaliza7eza7_1387;
							GC_COLLECT();
							BUNSPEC;
							if (CBOOL(BgL_finaliza7eza7_1398))
								{	/* Llib/bigloo.scm 727 */
									GC_invoke_finalizers();
									return BUNSPEC;
								}
							else
								{	/* Llib/bigloo.scm 727 */
									return BFALSE;
								}
						}
					}
				}
			}
		}

	}



/* gc */
	BGL_EXPORTED_DEF obj_t BGl_gcz00zz__biglooz00(obj_t BgL_finaliza7eza7_33)
	{
		{	/* Llib/bigloo.scm 725 */
			GC_COLLECT();
			BUNSPEC;
			if (CBOOL(BgL_finaliza7eza7_33))
				{	/* Llib/bigloo.scm 727 */
					GC_invoke_finalizers();
					return BUNSPEC;
				}
			else
				{	/* Llib/bigloo.scm 727 */
					return BFALSE;
				}
		}

	}



/* gc-finalize */
	BGL_EXPORTED_DEF obj_t BGl_gczd2finaliza7ez75zz__biglooz00(void)
	{
		{	/* Llib/bigloo.scm 733 */
			GC_invoke_finalizers();
			return BUNSPEC;
		}

	}



/* &gc-finalize */
	obj_t BGl_z62gczd2finaliza7ez17zz__biglooz00(obj_t BgL_envz00_2367)
	{
		{	/* Llib/bigloo.scm 733 */
			return BGl_gczd2finaliza7ez75zz__biglooz00();
		}

	}



/* make-cell */
	BGL_EXPORTED_DEF obj_t BGl_makezd2cellzd2zz__biglooz00(obj_t BgL_valz00_36)
	{
		{	/* Llib/bigloo.scm 741 */
			return MAKE_YOUNG_CELL(BgL_valz00_36);
		}

	}



/* &make-cell */
	obj_t BGl_z62makezd2cellzb0zz__biglooz00(obj_t BgL_envz00_2368,
		obj_t BgL_valz00_2369)
	{
		{	/* Llib/bigloo.scm 741 */
			return BGl_makezd2cellzd2zz__biglooz00(BgL_valz00_2369);
		}

	}



/* cell? */
	BGL_EXPORTED_DEF obj_t BGl_cellzf3zf3zz__biglooz00(obj_t BgL_objz00_37)
	{
		{	/* Llib/bigloo.scm 747 */
			return BBOOL(CELLP(BgL_objz00_37));
		}

	}



/* &cell? */
	obj_t BGl_z62cellzf3z91zz__biglooz00(obj_t BgL_envz00_2370,
		obj_t BgL_objz00_2371)
	{
		{	/* Llib/bigloo.scm 747 */
			return BGl_cellzf3zf3zz__biglooz00(BgL_objz00_2371);
		}

	}



/* cell-ref */
	BGL_EXPORTED_DEF obj_t BGl_cellzd2refzd2zz__biglooz00(obj_t BgL_cellz00_38)
	{
		{	/* Llib/bigloo.scm 753 */
			return CELL_REF(BgL_cellz00_38);
		}

	}



/* &cell-ref */
	obj_t BGl_z62cellzd2refzb0zz__biglooz00(obj_t BgL_envz00_2372,
		obj_t BgL_cellz00_2373)
	{
		{	/* Llib/bigloo.scm 753 */
			return BGl_cellzd2refzd2zz__biglooz00(BgL_cellz00_2373);
		}

	}



/* cell-set! */
	BGL_EXPORTED_DEF obj_t BGl_cellzd2setz12zc0zz__biglooz00(obj_t BgL_cellz00_39,
		obj_t BgL_valz00_40)
	{
		{	/* Llib/bigloo.scm 759 */
			return CELL_SET(BgL_cellz00_39, BgL_valz00_40);
		}

	}



/* &cell-set! */
	obj_t BGl_z62cellzd2setz12za2zz__biglooz00(obj_t BgL_envz00_2374,
		obj_t BgL_cellz00_2375, obj_t BgL_valz00_2376)
	{
		{	/* Llib/bigloo.scm 759 */
			return
				BGl_cellzd2setz12zc0zz__biglooz00(BgL_cellz00_2375, BgL_valz00_2376);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__biglooz00(void)
	{
		{	/* Llib/bigloo.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__biglooz00(void)
	{
		{	/* Llib/bigloo.scm 14 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__biglooz00(void)
	{
		{	/* Llib/bigloo.scm 14 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__biglooz00(void)
	{
		{	/* Llib/bigloo.scm 14 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1873z00zz__biglooz00));
			BGl_modulezd2initializa7ationz75zz__configurez00(35034923L,
				BSTRING_TO_STRING(BGl_string1873z00zz__biglooz00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1873z00zz__biglooz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string1873z00zz__biglooz00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string1873z00zz__biglooz00));
			return
				BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string1873z00zz__biglooz00));
		}

	}

#ifdef __cplusplus
}
#endif
