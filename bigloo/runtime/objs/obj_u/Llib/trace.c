/*===========================================================================*/
/*   (Llib/trace.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Llib/trace.scm -indent -o objs/obj_u/Llib/trace.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___TRACE_TYPE_DEFINITIONS
#define BGL___TRACE_TYPE_DEFINITIONS
#endif													// BGL___TRACE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62tracezd2colorzb0zz__tracez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_tracezd2marginzd2setz12z12zz__tracez00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_z52withzd2tracez80zz__tracez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__tracez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_tracezd2stringzd2zz__tracez00(obj_t);
	static obj_t BGl_symbol1694z00zz__tracez00 = BUNSPEC;
	static obj_t BGl_symbol1696z00zz__tracez00 = BUNSPEC;
	static obj_t BGl_symbol1698z00zz__tracez00 = BUNSPEC;
	extern obj_t bgl_display_obj(obj_t, obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__tracez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_withzd2outputzd2tozd2portzd2zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_tracezd2boldzd2zz__tracez00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31203ze3ze5zz__tracez00(obj_t);
	static obj_t BGl_z62tracezd2activezf3z43zz__tracez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_tracezd2portzd2zz__tracez00(void);
	static obj_t BGl_toplevelzd2initzd2zz__tracez00(void);
	extern int bgl_debug(void);
	static obj_t BGl_tracezd2alistzd2zz__tracez00(void);
	static obj_t BGl_z62tracezd2boldzb0zz__tracez00(obj_t, obj_t);
	extern bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	extern obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__tracez00(void);
	static obj_t BGl_genericzd2initzd2zz__tracez00(void);
	static obj_t BGl_z62tracezd2portzb0zz__tracez00(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__tracez00(void);
	extern obj_t BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zz__tracez00(void);
	static obj_t BGl_objectzd2initzd2zz__tracez00(void);
	static obj_t BGl_z62zc3z04anonymousza31231ze3ze5zz__tracez00(obj_t);
	extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_displayzd2circlezd2zz__pp_circlez00(obj_t, obj_t);
	extern obj_t BGl_bigloozd2tracezd2zz__paramz00(void);
	BGL_EXPORTED_DECL obj_t BGl_tracezd2colorzd2zz__tracez00(int, obj_t);
	extern obj_t BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31240ze3ze5zz__tracez00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31313ze3ze5zz__tracez00(obj_t);
	extern obj_t BGl_displayza2za2zz__r4_output_6_10_3z00(obj_t);
	static obj_t BGl_z62z52withzd2traceze2zz__tracez00(obj_t, obj_t, obj_t,
		obj_t);
	extern bool_t BGl_bigloozd2tracezd2colorz00zz__paramz00(void);
	static obj_t BGl_z62zc3z04anonymousza31330ze3ze5zz__tracez00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31217ze3ze5zz__tracez00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_tracezd2portzd2setz12z12zz__tracez00(obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_z62tracezd2portzd2setz12z70zz__tracez00(obj_t, obj_t);
	extern obj_t BGl_writez00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_symbol1701z00zz__tracez00 = BUNSPEC;
	static obj_t BGl_symbol1703z00zz__tracez00 = BUNSPEC;
	static obj_t BGl_methodzd2initzd2zz__tracez00(void);
	static obj_t BGl_symbol1706z00zz__tracez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_tracezd2itemzd2zz__tracez00(obj_t);
	static obj_t BGl_z62tracezd2marginzb0zz__tracez00(obj_t);
	static obj_t BGl_za2tracezd2mutexza2zd2zz__tracez00 = BUNSPEC;
	static obj_t BGl_z62tracezd2marginzd2setz12z70zz__tracez00(obj_t, obj_t);
	static obj_t BGl_z62tracezd2itemzb0zz__tracez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_tracezd2marginzd2zz__tracez00(void);
	extern obj_t string_append(obj_t, obj_t);
	static obj_t BGl_ttyzd2tracezd2colorz00zz__tracez00(int, obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31328ze3ze5zz__tracez00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_tracezd2activezf3z21zz__tracez00(obj_t);
	static obj_t BGl_z62tracezd2stringzb0zz__tracez00(obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_tracezd2colorzd2envz00zz__tracez00,
		BgL_bgl_za762traceza7d2color1725z00, va_generic_entry,
		BGl_z62tracezd2colorzb0zz__tracez00, BUNSPEC, -2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_z52withzd2tracezd2envz52zz__tracez00,
		BgL_bgl_za762za752withza7d2tra1726za7,
		BGl_z62z52withzd2traceze2zz__tracez00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_tracezd2marginzd2envz00zz__tracez00,
		BgL_bgl_za762traceza7d2margi1727z00, BGl_z62tracezd2marginzb0zz__tracez00,
		0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_tracezd2portzd2setz12zd2envzc0zz__tracez00,
		BgL_bgl_za762traceza7d2portza71728za7,
		BGl_z62tracezd2portzd2setz12z70zz__tracez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_tracezd2boldzd2envz00zz__tracez00,
		BgL_bgl_za762traceza7d2boldza71729za7, va_generic_entry,
		BGl_z62tracezd2boldzb0zz__tracez00, BUNSPEC, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_tracezd2itemzd2envz00zz__tracez00,
		BgL_bgl_za762traceza7d2itemza71730za7, va_generic_entry,
		BGl_z62tracezd2itemzb0zz__tracez00, BUNSPEC, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_tracezd2portzd2envz00zz__tracez00,
		BgL_bgl_za762traceza7d2portza71731za7, BGl_z62tracezd2portzb0zz__tracez00,
		0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1700z00zz__tracez00,
		BgL_bgl_string1700za700za7za7_1732za7, "", 0);
	      DEFINE_STRING(BGl_string1702z00zz__tracez00,
		BgL_bgl_string1702za700za7za7_1733za7, "margin-level", 12);
	      DEFINE_STRING(BGl_string1704z00zz__tracez00,
		BgL_bgl_string1704za700za7za7_1734za7, "trace-alist-get", 15);
	      DEFINE_STRING(BGl_string1705z00zz__tracez00,
		BgL_bgl_string1705za700za7za7_1735za7, "Can't find trace-value", 22);
	      DEFINE_STRING(BGl_string1707z00zz__tracez00,
		BgL_bgl_string1707za700za7za7_1736za7, "trace-alist-set!", 16);
	      DEFINE_STRING(BGl_string1708z00zz__tracez00,
		BgL_bgl_string1708za700za7za7_1737za7, "/tmp/bigloo/runtime/Llib/trace.scm",
		34);
	      DEFINE_STRING(BGl_string1709z00zz__tracez00,
		BgL_bgl_string1709za700za7za7_1738za7, "&trace-port-set!", 16);
	      DEFINE_STRING(BGl_string1710z00zz__tracez00,
		BgL_bgl_string1710za700za7za7_1739za7, "output-port", 11);
	      DEFINE_STRING(BGl_string1711z00zz__tracez00,
		BgL_bgl_string1711za700za7za7_1740za7, "&trace-margin-set!", 18);
	      DEFINE_STRING(BGl_string1712z00zz__tracez00,
		BgL_bgl_string1712za700za7za7_1741za7, "bstring", 7);
	      DEFINE_STRING(BGl_string1713z00zz__tracez00,
		BgL_bgl_string1713za700za7za7_1742za7, "&trace-color", 12);
	      DEFINE_STRING(BGl_string1714z00zz__tracez00,
		BgL_bgl_string1714za700za7za7_1743za7, "bint", 4);
	      DEFINE_STRING(BGl_string1715z00zz__tracez00,
		BgL_bgl_string1715za700za7za7_1744za7, "m", 1);
	      DEFINE_STRING(BGl_string1716z00zz__tracez00,
		BgL_bgl_string1716za700za7za7_1745za7, "\033[0m\033[1;", 8);
	      DEFINE_STRING(BGl_string1717z00zz__tracez00,
		BgL_bgl_string1717za700za7za7_1746za7, "\033[0m", 4);
	      DEFINE_STRING(BGl_string1718z00zz__tracez00,
		BgL_bgl_string1718za700za7za7_1747za7, "- ", 2);
	      DEFINE_STRING(BGl_string1719z00zz__tracez00,
		BgL_bgl_string1719za700za7za7_1748za7, "  |", 3);
	      DEFINE_STRING(BGl_string1720z00zz__tracez00,
		BgL_bgl_string1720za700za7za7_1749za7, "&%with-trace", 12);
	      DEFINE_STRING(BGl_string1721z00zz__tracez00,
		BgL_bgl_string1721za700za7za7_1750za7, "procedure", 9);
	      DEFINE_STRING(BGl_string1722z00zz__tracez00,
		BgL_bgl_string1722za700za7za7_1751za7, "+ ", 2);
	      DEFINE_STRING(BGl_string1723z00zz__tracez00,
		BgL_bgl_string1723za700za7za7_1752za7, "--+ ", 4);
	      DEFINE_STRING(BGl_string1724z00zz__tracez00,
		BgL_bgl_string1724za700za7za7_1753za7, "__trace", 7);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_tracezd2activezf3zd2envzf3zz__tracez00,
		BgL_bgl_za762traceza7d2activ1754z00,
		BGl_z62tracezd2activezf3z43zz__tracez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_tracezd2stringzd2envz00zz__tracez00,
		BgL_bgl_za762traceza7d2strin1755z00, BGl_z62tracezd2stringzb0zz__tracez00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1693z00zz__tracez00,
		BgL_bgl_string1693za700za7za7_1756za7, "trace", 5);
	      DEFINE_STRING(BGl_string1695z00zz__tracez00,
		BgL_bgl_string1695za700za7za7_1757za7, "port", 4);
	      DEFINE_STRING(BGl_string1697z00zz__tracez00,
		BgL_bgl_string1697za700za7za7_1758za7, "depth", 5);
	      DEFINE_STRING(BGl_string1699z00zz__tracez00,
		BgL_bgl_string1699za700za7za7_1759za7, "margin", 6);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tracezd2marginzd2setz12zd2envzc0zz__tracez00,
		BgL_bgl_za762traceza7d2margi1760z00,
		BGl_z62tracezd2marginzd2setz12z70zz__tracez00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__tracez00));
		     ADD_ROOT((void *) (&BGl_symbol1694z00zz__tracez00));
		     ADD_ROOT((void *) (&BGl_symbol1696z00zz__tracez00));
		     ADD_ROOT((void *) (&BGl_symbol1698z00zz__tracez00));
		     ADD_ROOT((void *) (&BGl_symbol1701z00zz__tracez00));
		     ADD_ROOT((void *) (&BGl_symbol1703z00zz__tracez00));
		     ADD_ROOT((void *) (&BGl_symbol1706z00zz__tracez00));
		     ADD_ROOT((void *) (&BGl_za2tracezd2mutexza2zd2zz__tracez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__tracez00(long
		BgL_checksumz00_2113, char *BgL_fromz00_2114)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__tracez00))
				{
					BGl_requirezd2initializa7ationz75zz__tracez00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__tracez00();
					BGl_cnstzd2initzd2zz__tracez00();
					BGl_importedzd2moduleszd2initz00zz__tracez00();
					return BGl_toplevelzd2initzd2zz__tracez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__tracez00(void)
	{
		{	/* Llib/trace.scm 15 */
			BGl_symbol1694z00zz__tracez00 =
				bstring_to_symbol(BGl_string1695z00zz__tracez00);
			BGl_symbol1696z00zz__tracez00 =
				bstring_to_symbol(BGl_string1697z00zz__tracez00);
			BGl_symbol1698z00zz__tracez00 =
				bstring_to_symbol(BGl_string1699z00zz__tracez00);
			BGl_symbol1701z00zz__tracez00 =
				bstring_to_symbol(BGl_string1702z00zz__tracez00);
			BGl_symbol1703z00zz__tracez00 =
				bstring_to_symbol(BGl_string1704z00zz__tracez00);
			return (BGl_symbol1706z00zz__tracez00 =
				bstring_to_symbol(BGl_string1707z00zz__tracez00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__tracez00(void)
	{
		{	/* Llib/trace.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__tracez00(void)
	{
		{	/* Llib/trace.scm 15 */
			return (BGl_za2tracezd2mutexza2zd2zz__tracez00 =
				bgl_make_mutex(BGl_string1693z00zz__tracez00), BUNSPEC);
		}

	}



/* trace-alist */
	obj_t BGl_tracezd2alistzd2zz__tracez00(void)
	{
		{	/* Llib/trace.scm 82 */
			{	/* Llib/trace.scm 83 */
				obj_t BgL_alz00_1114;

				BgL_alz00_1114 = BGL_DEBUG_ALIST_GET();
				if (NULLP(BgL_alz00_1114))
					{	/* Llib/trace.scm 86 */
						obj_t BgL_newzd2alzd2_1116;

						{	/* Llib/trace.scm 86 */
							obj_t BgL_arg1182z00_1117;
							obj_t BgL_arg1183z00_1118;
							obj_t BgL_arg1187z00_1119;
							obj_t BgL_arg1188z00_1120;

							{	/* Llib/trace.scm 86 */
								obj_t BgL_arg1194z00_1125;

								{	/* Llib/trace.scm 86 */
									obj_t BgL_tmpz00_2133;

									BgL_tmpz00_2133 = BGL_CURRENT_DYNAMIC_ENV();
									BgL_arg1194z00_1125 =
										BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_2133);
								}
								BgL_arg1182z00_1117 =
									MAKE_YOUNG_PAIR(BGl_symbol1694z00zz__tracez00,
									BgL_arg1194z00_1125);
							}
							BgL_arg1183z00_1118 =
								MAKE_YOUNG_PAIR(BGl_symbol1696z00zz__tracez00, BINT(0L));
							BgL_arg1187z00_1119 =
								MAKE_YOUNG_PAIR(BGl_symbol1698z00zz__tracez00,
								BGl_string1700z00zz__tracez00);
							BgL_arg1188z00_1120 =
								MAKE_YOUNG_PAIR(BGl_symbol1701z00zz__tracez00, BINT(0L));
							{	/* Llib/trace.scm 86 */
								obj_t BgL_list1189z00_1121;

								{	/* Llib/trace.scm 86 */
									obj_t BgL_arg1190z00_1122;

									{	/* Llib/trace.scm 86 */
										obj_t BgL_arg1191z00_1123;

										{	/* Llib/trace.scm 86 */
											obj_t BgL_arg1193z00_1124;

											BgL_arg1193z00_1124 =
												MAKE_YOUNG_PAIR(BgL_arg1188z00_1120, BNIL);
											BgL_arg1191z00_1123 =
												MAKE_YOUNG_PAIR(BgL_arg1187z00_1119,
												BgL_arg1193z00_1124);
										}
										BgL_arg1190z00_1122 =
											MAKE_YOUNG_PAIR(BgL_arg1183z00_1118, BgL_arg1191z00_1123);
									}
									BgL_list1189z00_1121 =
										MAKE_YOUNG_PAIR(BgL_arg1182z00_1117, BgL_arg1190z00_1122);
								}
								BgL_newzd2alzd2_1116 = BgL_list1189z00_1121;
							}
						}
						BGL_DEBUG_ALIST_SET(BgL_newzd2alzd2_1116);
						BUNSPEC;
						return BgL_newzd2alzd2_1116;
					}
				else
					{	/* Llib/trace.scm 84 */
						return BgL_alz00_1114;
					}
			}
		}

	}



/* trace-port */
	BGL_EXPORTED_DEF obj_t BGl_tracezd2portzd2zz__tracez00(void)
	{
		{	/* Llib/trace.scm 114 */
			{	/* Llib/trace.scm 115 */
				obj_t BgL_arg1197z00_1659;

				BgL_arg1197z00_1659 = BGl_tracezd2alistzd2zz__tracez00();
				{	/* Llib/trace.scm 115 */
					obj_t BgL_keyz00_1660;

					BgL_keyz00_1660 = BGl_symbol1694z00zz__tracez00;
					{	/* Llib/trace.scm 97 */
						obj_t BgL_cz00_1661;

						BgL_cz00_1661 =
							BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_keyz00_1660,
							BgL_arg1197z00_1659);
						if (PAIRP(BgL_cz00_1661))
							{	/* Llib/trace.scm 98 */
								return CDR(BgL_cz00_1661);
							}
						else
							{	/* Llib/trace.scm 98 */
								return
									BGl_errorz00zz__errorz00(BGl_symbol1703z00zz__tracez00,
									BGl_string1705z00zz__tracez00, BgL_keyz00_1660);
							}
					}
				}
			}
		}

	}



/* &trace-port */
	obj_t BGl_z62tracezd2portzb0zz__tracez00(obj_t BgL_envz00_2014)
	{
		{	/* Llib/trace.scm 114 */
			return BGl_tracezd2portzd2zz__tracez00();
		}

	}



/* trace-port-set! */
	BGL_EXPORTED_DEF obj_t BGl_tracezd2portzd2setz12z12zz__tracez00(obj_t
		BgL_pz00_8)
	{
		{	/* Llib/trace.scm 120 */
			{	/* Llib/trace.scm 121 */
				obj_t BgL_arg1198z00_1664;

				BgL_arg1198z00_1664 = BGl_tracezd2alistzd2zz__tracez00();
				{	/* Llib/trace.scm 121 */
					obj_t BgL_keyz00_1665;

					BgL_keyz00_1665 = BGl_symbol1694z00zz__tracez00;
					{	/* Llib/trace.scm 106 */
						obj_t BgL_cz00_1666;

						BgL_cz00_1666 =
							BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_keyz00_1665,
							BgL_arg1198z00_1664);
						if (PAIRP(BgL_cz00_1666))
							{	/* Llib/trace.scm 107 */
								return SET_CDR(BgL_cz00_1666, BgL_pz00_8);
							}
						else
							{	/* Llib/trace.scm 107 */
								return
									BGl_errorz00zz__errorz00(BGl_symbol1706z00zz__tracez00,
									BGl_string1705z00zz__tracez00, BgL_keyz00_1665);
							}
					}
				}
			}
		}

	}



/* &trace-port-set! */
	obj_t BGl_z62tracezd2portzd2setz12z70zz__tracez00(obj_t BgL_envz00_2015,
		obj_t BgL_pz00_2016)
	{
		{	/* Llib/trace.scm 120 */
			{	/* Llib/trace.scm 121 */
				obj_t BgL_auxz00_2160;

				if (OUTPUT_PORTP(BgL_pz00_2016))
					{	/* Llib/trace.scm 121 */
						BgL_auxz00_2160 = BgL_pz00_2016;
					}
				else
					{
						obj_t BgL_auxz00_2163;

						BgL_auxz00_2163 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1708z00zz__tracez00,
							BINT(4543L), BGl_string1709z00zz__tracez00,
							BGl_string1710z00zz__tracez00, BgL_pz00_2016);
						FAILURE(BgL_auxz00_2163, BFALSE, BFALSE);
					}
				return BGl_tracezd2portzd2setz12z12zz__tracez00(BgL_auxz00_2160);
			}
		}

	}



/* trace-margin */
	BGL_EXPORTED_DEF obj_t BGl_tracezd2marginzd2zz__tracez00(void)
	{
		{	/* Llib/trace.scm 126 */
			{	/* Llib/trace.scm 127 */
				obj_t BgL_arg1199z00_1669;

				BgL_arg1199z00_1669 = BGl_tracezd2alistzd2zz__tracez00();
				{	/* Llib/trace.scm 127 */
					obj_t BgL_keyz00_1670;

					BgL_keyz00_1670 = BGl_symbol1698z00zz__tracez00;
					{	/* Llib/trace.scm 97 */
						obj_t BgL_cz00_1671;

						BgL_cz00_1671 =
							BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_keyz00_1670,
							BgL_arg1199z00_1669);
						if (PAIRP(BgL_cz00_1671))
							{	/* Llib/trace.scm 98 */
								return CDR(BgL_cz00_1671);
							}
						else
							{	/* Llib/trace.scm 98 */
								return
									BGl_errorz00zz__errorz00(BGl_symbol1703z00zz__tracez00,
									BGl_string1705z00zz__tracez00, BgL_keyz00_1670);
							}
					}
				}
			}
		}

	}



/* &trace-margin */
	obj_t BGl_z62tracezd2marginzb0zz__tracez00(obj_t BgL_envz00_2017)
	{
		{	/* Llib/trace.scm 126 */
			return BGl_tracezd2marginzd2zz__tracez00();
		}

	}



/* trace-margin-set! */
	BGL_EXPORTED_DEF obj_t BGl_tracezd2marginzd2setz12z12zz__tracez00(obj_t
		BgL_mz00_9)
	{
		{	/* Llib/trace.scm 132 */
			{	/* Llib/trace.scm 133 */
				obj_t BgL_arg1200z00_1674;

				BgL_arg1200z00_1674 = BGl_tracezd2alistzd2zz__tracez00();
				{	/* Llib/trace.scm 133 */
					obj_t BgL_keyz00_1675;

					BgL_keyz00_1675 = BGl_symbol1698z00zz__tracez00;
					{	/* Llib/trace.scm 106 */
						obj_t BgL_cz00_1676;

						BgL_cz00_1676 =
							BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_keyz00_1675,
							BgL_arg1200z00_1674);
						if (PAIRP(BgL_cz00_1676))
							{	/* Llib/trace.scm 107 */
								return SET_CDR(BgL_cz00_1676, BgL_mz00_9);
							}
						else
							{	/* Llib/trace.scm 107 */
								return
									BGl_errorz00zz__errorz00(BGl_symbol1706z00zz__tracez00,
									BGl_string1705z00zz__tracez00, BgL_keyz00_1675);
							}
					}
				}
			}
		}

	}



/* &trace-margin-set! */
	obj_t BGl_z62tracezd2marginzd2setz12z70zz__tracez00(obj_t BgL_envz00_2018,
		obj_t BgL_mz00_2019)
	{
		{	/* Llib/trace.scm 132 */
			{	/* Llib/trace.scm 133 */
				obj_t BgL_auxz00_2181;

				if (STRINGP(BgL_mz00_2019))
					{	/* Llib/trace.scm 133 */
						BgL_auxz00_2181 = BgL_mz00_2019;
					}
				else
					{
						obj_t BgL_auxz00_2184;

						BgL_auxz00_2184 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1708z00zz__tracez00,
							BINT(5131L), BGl_string1711z00zz__tracez00,
							BGl_string1712z00zz__tracez00, BgL_mz00_2019);
						FAILURE(BgL_auxz00_2184, BFALSE, BFALSE);
					}
				return BGl_tracezd2marginzd2setz12z12zz__tracez00(BgL_auxz00_2181);
			}
		}

	}



/* trace-color */
	BGL_EXPORTED_DEF obj_t BGl_tracezd2colorzd2zz__tracez00(int BgL_colz00_10,
		obj_t BgL_oz00_11)
	{
		{	/* Llib/trace.scm 138 */
			{	/* Llib/trace.scm 140 */
				obj_t BgL_arg1201z00_1134;

				if (BGl_bigloozd2tracezd2colorz00zz__paramz00())
					{	/* Llib/trace.scm 141 */
						obj_t BgL_zc3z04anonymousza31203ze3z87_2020;

						BgL_zc3z04anonymousza31203ze3z87_2020 =
							MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31203ze3ze5zz__tracez00,
							(int) (0L), (int) (2L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31203ze3z87_2020,
							(int) (0L), BINT(BgL_colz00_10));
						PROCEDURE_SET(BgL_zc3z04anonymousza31203ze3z87_2020,
							(int) (1L), BgL_oz00_11);
						BgL_arg1201z00_1134 = BgL_zc3z04anonymousza31203ze3z87_2020;
					}
				else
					{	/* Llib/trace.scm 145 */
						obj_t BgL_zc3z04anonymousza31217ze3z87_2021;

						BgL_zc3z04anonymousza31217ze3z87_2021 =
							MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31217ze3ze5zz__tracez00,
							(int) (0L), (int) (1L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31217ze3z87_2021,
							(int) (0L), BgL_oz00_11);
						BgL_arg1201z00_1134 = BgL_zc3z04anonymousza31217ze3z87_2021;
					}
				return
					BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00
					(BgL_arg1201z00_1134);
			}
		}

	}



/* &trace-color */
	obj_t BGl_z62tracezd2colorzb0zz__tracez00(obj_t BgL_envz00_2022,
		obj_t BgL_colz00_2023, obj_t BgL_oz00_2024)
	{
		{	/* Llib/trace.scm 138 */
			{	/* Llib/trace.scm 140 */
				int BgL_auxz00_2205;

				{	/* Llib/trace.scm 140 */
					obj_t BgL_tmpz00_2206;

					if (INTEGERP(BgL_colz00_2023))
						{	/* Llib/trace.scm 140 */
							BgL_tmpz00_2206 = BgL_colz00_2023;
						}
					else
						{
							obj_t BgL_auxz00_2209;

							BgL_auxz00_2209 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1708z00zz__tracez00,
								BINT(5447L), BGl_string1713z00zz__tracez00,
								BGl_string1714z00zz__tracez00, BgL_colz00_2023);
							FAILURE(BgL_auxz00_2209, BFALSE, BFALSE);
						}
					BgL_auxz00_2205 = CINT(BgL_tmpz00_2206);
				}
				return BGl_tracezd2colorzd2zz__tracez00(BgL_auxz00_2205, BgL_oz00_2024);
			}
		}

	}



/* &<@anonymous:1203> */
	obj_t BGl_z62zc3z04anonymousza31203ze3ze5zz__tracez00(obj_t BgL_envz00_2025)
	{
		{	/* Llib/trace.scm 141 */
			{	/* Llib/trace.scm 142 */
				int BgL_colz00_2026;
				obj_t BgL_oz00_2027;

				BgL_colz00_2026 = CINT(PROCEDURE_REF(BgL_envz00_2025, (int) (0L)));
				BgL_oz00_2027 = PROCEDURE_REF(BgL_envz00_2025, (int) (1L));
				{	/* Llib/trace.scm 142 */
					long BgL_arg1206z00_2076;

					BgL_arg1206z00_2076 = (31L + (long) (BgL_colz00_2026));
					{	/* Llib/trace.scm 142 */
						obj_t BgL_list1207z00_2077;

						{	/* Llib/trace.scm 142 */
							obj_t BgL_arg1208z00_2078;

							{	/* Llib/trace.scm 142 */
								obj_t BgL_arg1209z00_2079;

								BgL_arg1209z00_2079 =
									MAKE_YOUNG_PAIR(BGl_string1715z00zz__tracez00, BNIL);
								BgL_arg1208z00_2078 =
									MAKE_YOUNG_PAIR(BINT(BgL_arg1206z00_2076),
									BgL_arg1209z00_2079);
							}
							BgL_list1207z00_2077 =
								MAKE_YOUNG_PAIR(BGl_string1716z00zz__tracez00,
								BgL_arg1208z00_2078);
						}
						BGl_displayza2za2zz__r4_output_6_10_3z00(BgL_list1207z00_2077);
				}}
				{
					obj_t BgL_l1077z00_2081;

					BgL_l1077z00_2081 = BgL_oz00_2027;
				BgL_zc3z04anonymousza31210ze3z87_2080:
					if (PAIRP(BgL_l1077z00_2081))
						{	/* Llib/trace.scm 143 */
							{	/* Llib/trace.scm 143 */
								obj_t BgL_arg1212z00_2082;

								BgL_arg1212z00_2082 = CAR(BgL_l1077z00_2081);
								{	/* Llib/trace.scm 143 */
									obj_t BgL_portz00_2083;

									{	/* Llib/trace.scm 143 */
										obj_t BgL_tmpz00_2230;

										BgL_tmpz00_2230 = BGL_CURRENT_DYNAMIC_ENV();
										BgL_portz00_2083 =
											BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_2230);
									}
									{	/* Llib/trace.scm 143 */

										BGl_displayzd2circlezd2zz__pp_circlez00(BgL_arg1212z00_2082,
											BgL_portz00_2083);
									}
								}
							}
							{
								obj_t BgL_l1077z00_2234;

								BgL_l1077z00_2234 = CDR(BgL_l1077z00_2081);
								BgL_l1077z00_2081 = BgL_l1077z00_2234;
								goto BgL_zc3z04anonymousza31210ze3z87_2080;
							}
						}
					else
						{	/* Llib/trace.scm 143 */
							((bool_t) 1);
						}
				}
				{	/* Llib/trace.scm 144 */
					obj_t BgL_arg1216z00_2084;

					{	/* Llib/trace.scm 144 */
						obj_t BgL_tmpz00_2236;

						BgL_tmpz00_2236 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_arg1216z00_2084 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_2236);
					}
					return
						bgl_display_string(BGl_string1717z00zz__tracez00,
						BgL_arg1216z00_2084);
				}
			}
		}

	}



/* &<@anonymous:1217> */
	obj_t BGl_z62zc3z04anonymousza31217ze3ze5zz__tracez00(obj_t BgL_envz00_2028)
	{
		{	/* Llib/trace.scm 145 */
			{	/* Llib/trace.scm 146 */
				obj_t BgL_oz00_2029;

				BgL_oz00_2029 = PROCEDURE_REF(BgL_envz00_2028, (int) (0L));
				{	/* Llib/trace.scm 146 */
					bool_t BgL_tmpz00_2242;

					{
						obj_t BgL_l1079z00_2086;

						BgL_l1079z00_2086 = BgL_oz00_2029;
					BgL_zc3z04anonymousza31218ze3z87_2085:
						if (PAIRP(BgL_l1079z00_2086))
							{	/* Llib/trace.scm 146 */
								{	/* Llib/trace.scm 146 */
									obj_t BgL_arg1220z00_2087;

									BgL_arg1220z00_2087 = CAR(BgL_l1079z00_2086);
									{	/* Llib/trace.scm 146 */
										obj_t BgL_portz00_2088;

										{	/* Llib/trace.scm 146 */
											obj_t BgL_tmpz00_2246;

											BgL_tmpz00_2246 = BGL_CURRENT_DYNAMIC_ENV();
											BgL_portz00_2088 =
												BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_2246);
										}
										{	/* Llib/trace.scm 146 */

											BGl_displayzd2circlezd2zz__pp_circlez00
												(BgL_arg1220z00_2087, BgL_portz00_2088);
										}
									}
								}
								{
									obj_t BgL_l1079z00_2250;

									BgL_l1079z00_2250 = CDR(BgL_l1079z00_2086);
									BgL_l1079z00_2086 = BgL_l1079z00_2250;
									goto BgL_zc3z04anonymousza31218ze3z87_2085;
								}
							}
						else
							{	/* Llib/trace.scm 146 */
								BgL_tmpz00_2242 = ((bool_t) 1);
							}
					}
					return BBOOL(BgL_tmpz00_2242);
				}
			}
		}

	}



/* trace-bold */
	BGL_EXPORTED_DEF obj_t BGl_tracezd2boldzd2zz__tracez00(obj_t BgL_oz00_12)
	{
		{	/* Llib/trace.scm 151 */
			{	/* Llib/trace.scm 152 */
				obj_t BgL_runner1224z00_1689;

				{	/* Llib/trace.scm 152 */
					obj_t BgL_list1222z00_1690;

					BgL_list1222z00_1690 = MAKE_YOUNG_PAIR(BgL_oz00_12, BNIL);
					BgL_runner1224z00_1689 =
						BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(BINT(-30L),
						BgL_list1222z00_1690);
				}
				{	/* Llib/trace.scm 152 */
					int BgL_aux1223z00_1691;

					{	/* Llib/trace.scm 152 */
						obj_t BgL_pairz00_1692;

						BgL_pairz00_1692 = BgL_runner1224z00_1689;
						BgL_aux1223z00_1691 = CINT(CAR(BgL_pairz00_1692));
					}
					BgL_runner1224z00_1689 = CDR(BgL_runner1224z00_1689);
					return
						BGl_tracezd2colorzd2zz__tracez00(BgL_aux1223z00_1691,
						BgL_runner1224z00_1689);
				}
			}
		}

	}



/* &trace-bold */
	obj_t BGl_z62tracezd2boldzb0zz__tracez00(obj_t BgL_envz00_2030,
		obj_t BgL_oz00_2031)
	{
		{	/* Llib/trace.scm 151 */
			return BGl_tracezd2boldzd2zz__tracez00(BgL_oz00_2031);
		}

	}



/* tty-trace-color */
	obj_t BGl_ttyzd2tracezd2colorz00zz__tracez00(int BgL_colz00_13,
		obj_t BgL_oz00_14)
	{
		{	/* Llib/trace.scm 157 */
			{	/* Llib/trace.scm 158 */
				bool_t BgL_test1773z00_2261;

				{	/* Llib/trace.scm 158 */
					obj_t BgL_arg1238z00_1181;

					{	/* Llib/trace.scm 158 */
						obj_t BgL_res1683z00_1699;

						{	/* Llib/trace.scm 115 */
							obj_t BgL_arg1197z00_1694;

							BgL_arg1197z00_1694 = BGl_tracezd2alistzd2zz__tracez00();
							{	/* Llib/trace.scm 115 */
								obj_t BgL_keyz00_1695;

								BgL_keyz00_1695 = BGl_symbol1694z00zz__tracez00;
								{	/* Llib/trace.scm 97 */
									obj_t BgL_cz00_1696;

									BgL_cz00_1696 =
										BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_keyz00_1695,
										BgL_arg1197z00_1694);
									if (PAIRP(BgL_cz00_1696))
										{	/* Llib/trace.scm 98 */
											BgL_res1683z00_1699 = CDR(BgL_cz00_1696);
										}
									else
										{	/* Llib/trace.scm 98 */
											BgL_res1683z00_1699 =
												BGl_errorz00zz__errorz00(BGl_symbol1703z00zz__tracez00,
												BGl_string1705z00zz__tracez00, BgL_keyz00_1695);
										}
								}
							}
						}
						BgL_arg1238z00_1181 = BgL_res1683z00_1699;
					}
					BgL_test1773z00_2261 = bgl_port_isatty(BgL_arg1238z00_1181);
				}
				if (BgL_test1773z00_2261)
					{	/* Llib/trace.scm 159 */
						obj_t BgL_runner1229z00_1168;

						{	/* Llib/trace.scm 159 */
							obj_t BgL_list1227z00_1166;

							BgL_list1227z00_1166 = MAKE_YOUNG_PAIR(BgL_oz00_14, BNIL);
							BgL_runner1229z00_1168 =
								BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(BINT(BgL_colz00_13),
								BgL_list1227z00_1166);
						}
						{	/* Llib/trace.scm 159 */
							int BgL_aux1228z00_1167;

							{	/* Llib/trace.scm 159 */
								obj_t BgL_pairz00_1701;

								BgL_pairz00_1701 = BgL_runner1229z00_1168;
								BgL_aux1228z00_1167 = CINT(CAR(BgL_pairz00_1701));
							}
							BgL_runner1229z00_1168 = CDR(BgL_runner1229z00_1168);
							BGL_TAIL return
								BGl_tracezd2colorzd2zz__tracez00(BgL_aux1228z00_1167,
								BgL_runner1229z00_1168);
						}
					}
				else
					{	/* Llib/trace.scm 162 */
						obj_t BgL_zc3z04anonymousza31231ze3z87_2032;

						BgL_zc3z04anonymousza31231ze3z87_2032 =
							MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31231ze3ze5zz__tracez00,
							(int) (0L), (int) (1L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31231ze3z87_2032,
							(int) (0L), BgL_oz00_14);
						return
							BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00
							(BgL_zc3z04anonymousza31231ze3z87_2032);
					}
			}
		}

	}



/* &<@anonymous:1231> */
	obj_t BGl_z62zc3z04anonymousza31231ze3ze5zz__tracez00(obj_t BgL_envz00_2033)
	{
		{	/* Llib/trace.scm 161 */
			{	/* Llib/trace.scm 162 */
				obj_t BgL_oz00_2034;

				BgL_oz00_2034 = PROCEDURE_REF(BgL_envz00_2033, (int) (0L));
				{	/* Llib/trace.scm 162 */
					bool_t BgL_tmpz00_2284;

					{
						obj_t BgL_l1081z00_2090;

						BgL_l1081z00_2090 = BgL_oz00_2034;
					BgL_zc3z04anonymousza31232ze3z87_2089:
						if (PAIRP(BgL_l1081z00_2090))
							{	/* Llib/trace.scm 162 */
								{	/* Llib/trace.scm 162 */
									obj_t BgL_arg1234z00_2091;

									BgL_arg1234z00_2091 = CAR(BgL_l1081z00_2090);
									{	/* Llib/trace.scm 162 */
										obj_t BgL_portz00_2092;

										{	/* Llib/trace.scm 162 */
											obj_t BgL_tmpz00_2288;

											BgL_tmpz00_2288 = BGL_CURRENT_DYNAMIC_ENV();
											BgL_portz00_2092 =
												BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_2288);
										}
										{	/* Llib/trace.scm 162 */

											BGl_displayzd2circlezd2zz__pp_circlez00
												(BgL_arg1234z00_2091, BgL_portz00_2092);
										}
									}
								}
								{
									obj_t BgL_l1081z00_2292;

									BgL_l1081z00_2292 = CDR(BgL_l1081z00_2090);
									BgL_l1081z00_2090 = BgL_l1081z00_2292;
									goto BgL_zc3z04anonymousza31232ze3z87_2089;
								}
							}
						else
							{	/* Llib/trace.scm 162 */
								BgL_tmpz00_2284 = ((bool_t) 1);
							}
					}
					return BBOOL(BgL_tmpz00_2284);
				}
			}
		}

	}



/* trace-string */
	BGL_EXPORTED_DEF obj_t BGl_tracezd2stringzd2zz__tracez00(obj_t BgL_oz00_15)
	{
		{	/* Llib/trace.scm 167 */
			{	/* Llib/trace.scm 170 */
				obj_t BgL_zc3z04anonymousza31240ze3z87_2035;

				BgL_zc3z04anonymousza31240ze3z87_2035 =
					MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31240ze3ze5zz__tracez00,
					(int) (0L), (int) (1L));
				PROCEDURE_SET(BgL_zc3z04anonymousza31240ze3z87_2035,
					(int) (0L), BgL_oz00_15);
				return
					BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00
					(BgL_zc3z04anonymousza31240ze3z87_2035);
			}
		}

	}



/* &trace-string */
	obj_t BGl_z62tracezd2stringzb0zz__tracez00(obj_t BgL_envz00_2036,
		obj_t BgL_oz00_2037)
	{
		{	/* Llib/trace.scm 167 */
			return BGl_tracezd2stringzd2zz__tracez00(BgL_oz00_2037);
		}

	}



/* &<@anonymous:1240> */
	obj_t BGl_z62zc3z04anonymousza31240ze3ze5zz__tracez00(obj_t BgL_envz00_2038)
	{
		{	/* Llib/trace.scm 169 */
			return
				BGl_writez00zz__r4_output_6_10_3z00(PROCEDURE_REF(BgL_envz00_2038,
					(int) (0L)), BNIL);
		}

	}



/* trace-item */
	BGL_EXPORTED_DEF obj_t BGl_tracezd2itemzd2zz__tracez00(obj_t BgL_argsz00_16)
	{
		{	/* Llib/trace.scm 175 */
			{	/* Llib/trace.scm 176 */
				bool_t BgL_test1776z00_2305;

				{	/* Llib/trace.scm 176 */
					int BgL_arg1304z00_1206;

					BgL_arg1304z00_1206 = bgl_debug();
					BgL_test1776z00_2305 = ((long) (BgL_arg1304z00_1206) > 0L);
				}
				if (BgL_test1776z00_2305)
					{	/* Llib/trace.scm 177 */
						obj_t BgL_alz00_1188;

						BgL_alz00_1188 = BGl_tracezd2alistzd2zz__tracez00();
						{	/* Llib/trace.scm 178 */
							bool_t BgL_test1777z00_2310;

							{	/* Llib/trace.scm 178 */
								obj_t BgL_arg1284z00_1205;

								{	/* Llib/trace.scm 178 */
									obj_t BgL_keyz00_1710;

									BgL_keyz00_1710 = BGl_symbol1701z00zz__tracez00;
									{	/* Llib/trace.scm 97 */
										obj_t BgL_cz00_1711;

										BgL_cz00_1711 =
											BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_keyz00_1710,
											BgL_alz00_1188);
										if (PAIRP(BgL_cz00_1711))
											{	/* Llib/trace.scm 98 */
												BgL_arg1284z00_1205 = CDR(BgL_cz00_1711);
											}
										else
											{	/* Llib/trace.scm 98 */
												BgL_arg1284z00_1205 =
													BGl_errorz00zz__errorz00
													(BGl_symbol1703z00zz__tracez00,
													BGl_string1705z00zz__tracez00, BgL_keyz00_1710);
											}
									}
								}
								BgL_test1777z00_2310 =
									CBOOL(BGl_tracezd2activezf3z21zz__tracez00
									(BgL_arg1284z00_1205));
							}
							if (BgL_test1777z00_2310)
								{	/* Llib/trace.scm 179 */
									obj_t BgL_pz00_1191;

									{	/* Llib/trace.scm 179 */
										obj_t BgL_res1684z00_1719;

										{	/* Llib/trace.scm 115 */
											obj_t BgL_arg1197z00_1714;

											BgL_arg1197z00_1714 = BGl_tracezd2alistzd2zz__tracez00();
											{	/* Llib/trace.scm 115 */
												obj_t BgL_keyz00_1715;

												BgL_keyz00_1715 = BGl_symbol1694z00zz__tracez00;
												{	/* Llib/trace.scm 97 */
													obj_t BgL_cz00_1716;

													BgL_cz00_1716 =
														BGl_assqz00zz__r4_pairs_and_lists_6_3z00
														(BgL_keyz00_1715, BgL_arg1197z00_1714);
													if (PAIRP(BgL_cz00_1716))
														{	/* Llib/trace.scm 98 */
															BgL_res1684z00_1719 = CDR(BgL_cz00_1716);
														}
													else
														{	/* Llib/trace.scm 98 */
															BgL_res1684z00_1719 =
																BGl_errorz00zz__errorz00
																(BGl_symbol1703z00zz__tracez00,
																BGl_string1705z00zz__tracez00, BgL_keyz00_1715);
														}
												}
											}
										}
										BgL_pz00_1191 = BgL_res1684z00_1719;
									}
									{	/* Llib/trace.scm 180 */
										obj_t BgL_mutex1246z00_1192;

										BgL_mutex1246z00_1192 =
											BGl_za2tracezd2mutexza2zd2zz__tracez00;
										{	/* Llib/trace.scm 180 */
											obj_t BgL_top1781z00_2325;

											BgL_top1781z00_2325 = BGL_EXITD_TOP_AS_OBJ();
											BGL_MUTEX_LOCK(BgL_mutex1246z00_1192);
											BGL_EXITD_PUSH_PROTECT(BgL_top1781z00_2325,
												BgL_mutex1246z00_1192);
											BUNSPEC;
											{	/* Llib/trace.scm 180 */
												obj_t BgL_tmp1780z00_2324;

												{	/* Llib/trace.scm 181 */
													obj_t BgL_arg1248z00_1193;

													{	/* Llib/trace.scm 181 */
														obj_t BgL_keyz00_1720;

														BgL_keyz00_1720 = BGl_symbol1698z00zz__tracez00;
														{	/* Llib/trace.scm 97 */
															obj_t BgL_cz00_1721;

															BgL_cz00_1721 =
																BGl_assqz00zz__r4_pairs_and_lists_6_3z00
																(BgL_keyz00_1720, BgL_alz00_1188);
															if (PAIRP(BgL_cz00_1721))
																{	/* Llib/trace.scm 98 */
																	BgL_arg1248z00_1193 = CDR(BgL_cz00_1721);
																}
															else
																{	/* Llib/trace.scm 98 */
																	BgL_arg1248z00_1193 =
																		BGl_errorz00zz__errorz00
																		(BGl_symbol1703z00zz__tracez00,
																		BGl_string1705z00zz__tracez00,
																		BgL_keyz00_1720);
																}
														}
													}
													bgl_display_obj(BgL_arg1248z00_1193, BgL_pz00_1191);
												}
												{	/* Llib/trace.scm 182 */
													obj_t BgL_arg1249z00_1194;

													{	/* Llib/trace.scm 182 */
														long BgL_arg1252z00_1195;

														{	/* Llib/trace.scm 182 */
															obj_t BgL_arg1268z00_1197;

															{	/* Llib/trace.scm 182 */
																obj_t BgL_keyz00_1724;

																BgL_keyz00_1724 = BGl_symbol1696z00zz__tracez00;
																{	/* Llib/trace.scm 97 */
																	obj_t BgL_cz00_1725;

																	BgL_cz00_1725 =
																		BGl_assqz00zz__r4_pairs_and_lists_6_3z00
																		(BgL_keyz00_1724, BgL_alz00_1188);
																	if (PAIRP(BgL_cz00_1725))
																		{	/* Llib/trace.scm 98 */
																			BgL_arg1268z00_1197 = CDR(BgL_cz00_1725);
																		}
																	else
																		{	/* Llib/trace.scm 98 */
																			BgL_arg1268z00_1197 =
																				BGl_errorz00zz__errorz00
																				(BGl_symbol1703z00zz__tracez00,
																				BGl_string1705z00zz__tracez00,
																				BgL_keyz00_1724);
																		}
																}
															}
															BgL_arg1252z00_1195 =
																((long) CINT(BgL_arg1268z00_1197) - 1L);
														}
														{	/* Llib/trace.scm 182 */
															obj_t BgL_list1253z00_1196;

															BgL_list1253z00_1196 =
																MAKE_YOUNG_PAIR(BGl_string1718z00zz__tracez00,
																BNIL);
															BgL_arg1249z00_1194 =
																BGl_ttyzd2tracezd2colorz00zz__tracez00((int)
																(BgL_arg1252z00_1195), BgL_list1253z00_1196);
													}}
													bgl_display_obj(BgL_arg1249z00_1194, BgL_pz00_1191);
												}
												{
													obj_t BgL_l1084z00_1740;

													BgL_l1084z00_1740 = BgL_argsz00_16;
												BgL_zc3z04anonymousza31269ze3z87_1739:
													if (PAIRP(BgL_l1084z00_1740))
														{	/* Llib/trace.scm 183 */
															BGl_displayzd2circlezd2zz__pp_circlez00(CAR
																(BgL_l1084z00_1740), BgL_pz00_1191);
															{
																obj_t BgL_l1084z00_2350;

																BgL_l1084z00_2350 = CDR(BgL_l1084z00_1740);
																BgL_l1084z00_1740 = BgL_l1084z00_2350;
																goto BgL_zc3z04anonymousza31269ze3z87_1739;
															}
														}
													else
														{	/* Llib/trace.scm 183 */
															((bool_t) 1);
														}
												}
												bgl_display_char(((unsigned char) 10), BgL_pz00_1191);
												BgL_tmp1780z00_2324 =
													bgl_flush_output_port(BgL_pz00_1191);
												BGL_EXITD_POP_PROTECT(BgL_top1781z00_2325);
												BUNSPEC;
												BGL_MUTEX_UNLOCK(BgL_mutex1246z00_1192);
												return BgL_tmp1780z00_2324;
											}
										}
									}
								}
							else
								{	/* Llib/trace.scm 178 */
									return BFALSE;
								}
						}
					}
				else
					{	/* Llib/trace.scm 176 */
						return BFALSE;
					}
			}
		}

	}



/* &trace-item */
	obj_t BGl_z62tracezd2itemzb0zz__tracez00(obj_t BgL_envz00_2040,
		obj_t BgL_argsz00_2041)
	{
		{	/* Llib/trace.scm 175 */
			return BGl_tracezd2itemzd2zz__tracez00(BgL_argsz00_2041);
		}

	}



/* trace-active? */
	BGL_EXPORTED_DEF obj_t BGl_tracezd2activezf3z21zz__tracez00(obj_t
		BgL_lvlz00_17)
	{
		{	/* Llib/trace.scm 190 */
			if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BgL_lvlz00_17))
				{	/* Llib/trace.scm 192 */
					int BgL_arg1306z00_1208;

					BgL_arg1306z00_1208 = bgl_debug();
					return
						BBOOL(((long) (BgL_arg1306z00_1208) >= (long) CINT(BgL_lvlz00_17)));
				}
			else
				{	/* Llib/trace.scm 192 */
					if (SYMBOLP(BgL_lvlz00_17))
						{	/* Llib/trace.scm 193 */
							return
								BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_lvlz00_17,
								BGl_bigloozd2tracezd2zz__paramz00());
						}
					else
						{	/* Llib/trace.scm 193 */
							return BFALSE;
						}
				}
		}

	}



/* &trace-active? */
	obj_t BGl_z62tracezd2activezf3z43zz__tracez00(obj_t BgL_envz00_2042,
		obj_t BgL_lvlz00_2043)
	{
		{	/* Llib/trace.scm 190 */
			return BGl_tracezd2activezf3z21zz__tracez00(BgL_lvlz00_2043);
		}

	}



/* %with-trace */
	BGL_EXPORTED_DEF obj_t BGl_z52withzd2tracez80zz__tracez00(obj_t BgL_lvlz00_18,
		obj_t BgL_lblz00_19, obj_t BgL_thunkz00_20)
	{
		{	/* Llib/trace.scm 198 */
			{	/* Llib/trace.scm 199 */
				obj_t BgL_alz00_1211;

				BgL_alz00_1211 = BGl_tracezd2alistzd2zz__tracez00();
				{	/* Llib/trace.scm 199 */
					obj_t BgL_olz00_1212;

					{	/* Llib/trace.scm 200 */
						obj_t BgL_keyz00_1753;

						BgL_keyz00_1753 = BGl_symbol1701z00zz__tracez00;
						{	/* Llib/trace.scm 97 */
							obj_t BgL_cz00_1754;

							BgL_cz00_1754 =
								BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_keyz00_1753,
								BgL_alz00_1211);
							if (PAIRP(BgL_cz00_1754))
								{	/* Llib/trace.scm 98 */
									BgL_olz00_1212 = CDR(BgL_cz00_1754);
								}
							else
								{	/* Llib/trace.scm 98 */
									BgL_olz00_1212 =
										BGl_errorz00zz__errorz00(BGl_symbol1703z00zz__tracez00,
										BGl_string1705z00zz__tracez00, BgL_keyz00_1753);
								}
						}
					}
					{	/* Llib/trace.scm 200 */

						{	/* Llib/trace.scm 201 */
							obj_t BgL_keyz00_1757;

							BgL_keyz00_1757 = BGl_symbol1701z00zz__tracez00;
							{	/* Llib/trace.scm 106 */
								obj_t BgL_cz00_1758;

								BgL_cz00_1758 =
									BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_keyz00_1757,
									BgL_alz00_1211);
								if (PAIRP(BgL_cz00_1758))
									{	/* Llib/trace.scm 107 */
										SET_CDR(BgL_cz00_1758, BgL_lvlz00_18);
									}
								else
									{	/* Llib/trace.scm 107 */
										BGl_errorz00zz__errorz00(BGl_symbol1706z00zz__tracez00,
											BGl_string1705z00zz__tracez00, BgL_keyz00_1757);
									}
							}
						}
						if (CBOOL(BGl_tracezd2activezf3z21zz__tracez00(BgL_lvlz00_18)))
							{	/* Llib/trace.scm 203 */
								obj_t BgL_dz00_1214;

								{	/* Llib/trace.scm 203 */
									obj_t BgL_keyz00_1761;

									BgL_keyz00_1761 = BGl_symbol1696z00zz__tracez00;
									{	/* Llib/trace.scm 97 */
										obj_t BgL_cz00_1762;

										BgL_cz00_1762 =
											BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_keyz00_1761,
											BgL_alz00_1211);
										if (PAIRP(BgL_cz00_1762))
											{	/* Llib/trace.scm 98 */
												BgL_dz00_1214 = CDR(BgL_cz00_1762);
											}
										else
											{	/* Llib/trace.scm 98 */
												BgL_dz00_1214 =
													BGl_errorz00zz__errorz00
													(BGl_symbol1703z00zz__tracez00,
													BGl_string1705z00zz__tracez00, BgL_keyz00_1761);
											}
									}
								}
								{	/* Llib/trace.scm 203 */
									obj_t BgL_omz00_1215;

									{	/* Llib/trace.scm 204 */
										obj_t BgL_keyz00_1765;

										BgL_keyz00_1765 = BGl_symbol1698z00zz__tracez00;
										{	/* Llib/trace.scm 97 */
											obj_t BgL_cz00_1766;

											BgL_cz00_1766 =
												BGl_assqz00zz__r4_pairs_and_lists_6_3z00
												(BgL_keyz00_1765, BgL_alz00_1211);
											if (PAIRP(BgL_cz00_1766))
												{	/* Llib/trace.scm 98 */
													BgL_omz00_1215 = CDR(BgL_cz00_1766);
												}
											else
												{	/* Llib/trace.scm 98 */
													BgL_omz00_1215 =
														BGl_errorz00zz__errorz00
														(BGl_symbol1703z00zz__tracez00,
														BGl_string1705z00zz__tracez00, BgL_keyz00_1765);
												}
										}
									}
									{	/* Llib/trace.scm 204 */
										obj_t BgL_maz00_1216;

										{	/* Llib/trace.scm 205 */
											obj_t BgL_list1329z00_1240;

											BgL_list1329z00_1240 =
												MAKE_YOUNG_PAIR(BGl_string1719z00zz__tracez00, BNIL);
											BgL_maz00_1216 =
												BGl_ttyzd2tracezd2colorz00zz__tracez00(CINT
												(BgL_dz00_1214), BgL_list1329z00_1240);
										}
										{	/* Llib/trace.scm 205 */

											{	/* Llib/trace.scm 206 */
												obj_t BgL_mutex1310z00_1217;

												BgL_mutex1310z00_1217 =
													BGl_za2tracezd2mutexza2zd2zz__tracez00;
												{	/* Llib/trace.scm 206 */
													obj_t BgL_top1793z00_2397;

													BgL_top1793z00_2397 = BGL_EXITD_TOP_AS_OBJ();
													BGL_MUTEX_LOCK(BgL_mutex1310z00_1217);
													BGL_EXITD_PUSH_PROTECT(BgL_top1793z00_2397,
														BgL_mutex1310z00_1217);
													BUNSPEC;
													{	/* Llib/trace.scm 206 */
														obj_t BgL_tmp1792z00_2396;

														{	/* Llib/trace.scm 207 */
															obj_t BgL_arg1311z00_1218;

															{	/* Llib/trace.scm 207 */
																obj_t BgL_res1685z00_1774;

																{	/* Llib/trace.scm 115 */
																	obj_t BgL_arg1197z00_1769;

																	BgL_arg1197z00_1769 =
																		BGl_tracezd2alistzd2zz__tracez00();
																	{	/* Llib/trace.scm 115 */
																		obj_t BgL_keyz00_1770;

																		BgL_keyz00_1770 =
																			BGl_symbol1694z00zz__tracez00;
																		{	/* Llib/trace.scm 97 */
																			obj_t BgL_cz00_1771;

																			BgL_cz00_1771 =
																				BGl_assqz00zz__r4_pairs_and_lists_6_3z00
																				(BgL_keyz00_1770, BgL_arg1197z00_1769);
																			if (PAIRP(BgL_cz00_1771))
																				{	/* Llib/trace.scm 98 */
																					BgL_res1685z00_1774 =
																						CDR(BgL_cz00_1771);
																				}
																			else
																				{	/* Llib/trace.scm 98 */
																					BgL_res1685z00_1774 =
																						BGl_errorz00zz__errorz00
																						(BGl_symbol1703z00zz__tracez00,
																						BGl_string1705z00zz__tracez00,
																						BgL_keyz00_1770);
																				}
																		}
																	}
																}
																BgL_arg1311z00_1218 = BgL_res1685z00_1774;
															}
															{	/* Llib/trace.scm 209 */
																obj_t BgL_zc3z04anonymousza31313ze3z87_2045;

																BgL_zc3z04anonymousza31313ze3z87_2045 =
																	MAKE_FX_PROCEDURE
																	(BGl_z62zc3z04anonymousza31313ze3ze5zz__tracez00,
																	(int) (0L), (int) (3L));
																PROCEDURE_SET
																	(BgL_zc3z04anonymousza31313ze3z87_2045,
																	(int) (0L), BgL_alz00_1211);
																PROCEDURE_SET
																	(BgL_zc3z04anonymousza31313ze3z87_2045,
																	(int) (1L), BgL_lblz00_19);
																PROCEDURE_SET
																	(BgL_zc3z04anonymousza31313ze3z87_2045,
																	(int) (2L), BgL_dz00_1214);
																BgL_tmp1792z00_2396 =
																	BGl_withzd2outputzd2tozd2portzd2zz__r4_ports_6_10_1z00
																	(BgL_arg1311z00_1218,
																	BgL_zc3z04anonymousza31313ze3z87_2045);
														}}
														BGL_EXITD_POP_PROTECT(BgL_top1793z00_2397);
														BUNSPEC;
														BGL_MUTEX_UNLOCK(BgL_mutex1310z00_1217);
														BgL_tmp1792z00_2396;
											}}}
											{	/* Llib/trace.scm 215 */
												long BgL_arg1326z00_1233;

												BgL_arg1326z00_1233 = ((long) CINT(BgL_dz00_1214) + 1L);
												{	/* Llib/trace.scm 215 */
													obj_t BgL_keyz00_1787;

													BgL_keyz00_1787 = BGl_symbol1696z00zz__tracez00;
													{	/* Llib/trace.scm 106 */
														obj_t BgL_cz00_1788;

														BgL_cz00_1788 =
															BGl_assqz00zz__r4_pairs_and_lists_6_3z00
															(BgL_keyz00_1787, BgL_alz00_1211);
														if (PAIRP(BgL_cz00_1788))
															{	/* Llib/trace.scm 108 */
																obj_t BgL_tmpz00_2424;

																BgL_tmpz00_2424 = BINT(BgL_arg1326z00_1233);
																SET_CDR(BgL_cz00_1788, BgL_tmpz00_2424);
															}
														else
															{	/* Llib/trace.scm 107 */
																BGl_errorz00zz__errorz00
																	(BGl_symbol1706z00zz__tracez00,
																	BGl_string1705z00zz__tracez00,
																	BgL_keyz00_1787);
															}
													}
												}
											}
											{	/* Llib/trace.scm 216 */
												obj_t BgL_arg1327z00_1234;

												BgL_arg1327z00_1234 =
													string_append(BgL_omz00_1215, BgL_maz00_1216);
												{	/* Llib/trace.scm 216 */
													obj_t BgL_keyz00_1791;

													BgL_keyz00_1791 = BGl_symbol1698z00zz__tracez00;
													{	/* Llib/trace.scm 106 */
														obj_t BgL_cz00_1792;

														BgL_cz00_1792 =
															BGl_assqz00zz__r4_pairs_and_lists_6_3z00
															(BgL_keyz00_1791, BgL_alz00_1211);
														if (PAIRP(BgL_cz00_1792))
															{	/* Llib/trace.scm 107 */
																SET_CDR(BgL_cz00_1792, BgL_arg1327z00_1234);
															}
														else
															{	/* Llib/trace.scm 107 */
																BGl_errorz00zz__errorz00
																	(BGl_symbol1706z00zz__tracez00,
																	BGl_string1705z00zz__tracez00,
																	BgL_keyz00_1791);
															}
													}
												}
											}
											{	/* Llib/trace.scm 217 */
												obj_t BgL_exitd1039z00_1235;

												BgL_exitd1039z00_1235 = BGL_EXITD_TOP_AS_OBJ();
												{	/* Llib/trace.scm 220 */
													obj_t BgL_zc3z04anonymousza31328ze3z87_2044;

													BgL_zc3z04anonymousza31328ze3z87_2044 =
														MAKE_FX_PROCEDURE
														(BGl_z62zc3z04anonymousza31328ze3ze5zz__tracez00,
														(int) (0L), (int) (4L));
													PROCEDURE_SET(BgL_zc3z04anonymousza31328ze3z87_2044,
														(int) (0L), BgL_alz00_1211);
													PROCEDURE_SET(BgL_zc3z04anonymousza31328ze3z87_2044,
														(int) (1L), BgL_dz00_1214);
													PROCEDURE_SET(BgL_zc3z04anonymousza31328ze3z87_2044,
														(int) (2L), BgL_omz00_1215);
													PROCEDURE_SET(BgL_zc3z04anonymousza31328ze3z87_2044,
														(int) (3L), BgL_olz00_1212);
													{	/* Llib/trace.scm 217 */
														obj_t BgL_arg1678z00_1807;

														{	/* Llib/trace.scm 217 */
															obj_t BgL_arg1681z00_1808;

															BgL_arg1681z00_1808 =
																BGL_EXITD_PROTECT(BgL_exitd1039z00_1235);
															BgL_arg1678z00_1807 =
																MAKE_YOUNG_PAIR
																(BgL_zc3z04anonymousza31328ze3z87_2044,
																BgL_arg1681z00_1808);
														}
														BGL_EXITD_PROTECT_SET(BgL_exitd1039z00_1235,
															BgL_arg1678z00_1807);
														BUNSPEC;
													}
													{	/* Llib/trace.scm 218 */
														obj_t BgL_tmp1041z00_1237;

														BgL_tmp1041z00_1237 =
															BGL_PROCEDURE_CALL0(BgL_thunkz00_20);
														{	/* Llib/trace.scm 217 */
															bool_t BgL_test1797z00_2452;

															{	/* Llib/trace.scm 217 */
																obj_t BgL_arg1676z00_1810;

																BgL_arg1676z00_1810 =
																	BGL_EXITD_PROTECT(BgL_exitd1039z00_1235);
																BgL_test1797z00_2452 =
																	PAIRP(BgL_arg1676z00_1810);
															}
															if (BgL_test1797z00_2452)
																{	/* Llib/trace.scm 217 */
																	obj_t BgL_arg1670z00_1811;

																	{	/* Llib/trace.scm 217 */
																		obj_t BgL_arg1675z00_1812;

																		BgL_arg1675z00_1812 =
																			BGL_EXITD_PROTECT(BgL_exitd1039z00_1235);
																		BgL_arg1670z00_1811 =
																			CDR(((obj_t) BgL_arg1675z00_1812));
																	}
																	BGL_EXITD_PROTECT_SET(BgL_exitd1039z00_1235,
																		BgL_arg1670z00_1811);
																	BUNSPEC;
																}
															else
																{	/* Llib/trace.scm 217 */
																	BFALSE;
																}
														}
														BGl_z62zc3z04anonymousza31328ze3ze5zz__tracez00
															(BgL_zc3z04anonymousza31328ze3z87_2044);
														return BgL_tmp1041z00_1237;
													}
												}
											}
										}
									}
								}
							}
						else
							{	/* Llib/trace.scm 223 */
								obj_t BgL_exitd1043z00_1241;

								BgL_exitd1043z00_1241 = BGL_EXITD_TOP_AS_OBJ();
								{	/* Llib/trace.scm 225 */
									obj_t BgL_zc3z04anonymousza31330ze3z87_2046;

									BgL_zc3z04anonymousza31330ze3z87_2046 =
										MAKE_FX_PROCEDURE
										(BGl_z62zc3z04anonymousza31330ze3ze5zz__tracez00,
										(int) (0L), (int) (2L));
									PROCEDURE_SET(BgL_zc3z04anonymousza31330ze3z87_2046,
										(int) (0L), BgL_alz00_1211);
									PROCEDURE_SET(BgL_zc3z04anonymousza31330ze3z87_2046,
										(int) (1L), BgL_olz00_1212);
									{	/* Llib/trace.scm 223 */
										obj_t BgL_arg1678z00_1818;

										{	/* Llib/trace.scm 223 */
											obj_t BgL_arg1681z00_1819;

											BgL_arg1681z00_1819 =
												BGL_EXITD_PROTECT(BgL_exitd1043z00_1241);
											BgL_arg1678z00_1818 =
												MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31330ze3z87_2046,
												BgL_arg1681z00_1819);
										}
										BGL_EXITD_PROTECT_SET(BgL_exitd1043z00_1241,
											BgL_arg1678z00_1818);
										BUNSPEC;
									}
									{	/* Llib/trace.scm 224 */
										obj_t BgL_tmp1045z00_1243;

										BgL_tmp1045z00_1243 = BGL_PROCEDURE_CALL0(BgL_thunkz00_20);
										{	/* Llib/trace.scm 223 */
											bool_t BgL_test1798z00_2474;

											{	/* Llib/trace.scm 223 */
												obj_t BgL_arg1676z00_1821;

												BgL_arg1676z00_1821 =
													BGL_EXITD_PROTECT(BgL_exitd1043z00_1241);
												BgL_test1798z00_2474 = PAIRP(BgL_arg1676z00_1821);
											}
											if (BgL_test1798z00_2474)
												{	/* Llib/trace.scm 223 */
													obj_t BgL_arg1670z00_1822;

													{	/* Llib/trace.scm 223 */
														obj_t BgL_arg1675z00_1823;

														BgL_arg1675z00_1823 =
															BGL_EXITD_PROTECT(BgL_exitd1043z00_1241);
														BgL_arg1670z00_1822 =
															CDR(((obj_t) BgL_arg1675z00_1823));
													}
													BGL_EXITD_PROTECT_SET(BgL_exitd1043z00_1241,
														BgL_arg1670z00_1822);
													BUNSPEC;
												}
											else
												{	/* Llib/trace.scm 223 */
													BFALSE;
												}
										}
										{	/* Llib/trace.scm 225 */
											obj_t BgL_keyz00_1825;

											BgL_keyz00_1825 = BGl_symbol1701z00zz__tracez00;
											{	/* Llib/trace.scm 106 */
												obj_t BgL_cz00_1826;

												BgL_cz00_1826 =
													BGl_assqz00zz__r4_pairs_and_lists_6_3z00
													(BgL_keyz00_1825, BgL_alz00_1211);
												if (PAIRP(BgL_cz00_1826))
													{	/* Llib/trace.scm 107 */
														SET_CDR(BgL_cz00_1826, BgL_olz00_1212);
													}
												else
													{	/* Llib/trace.scm 107 */
														BGl_errorz00zz__errorz00
															(BGl_symbol1706z00zz__tracez00,
															BGl_string1705z00zz__tracez00, BgL_keyz00_1825);
													}
											}
										}
										return BgL_tmp1045z00_1243;
									}
								}
							}
					}
				}
			}
		}

	}



/* &%with-trace */
	obj_t BGl_z62z52withzd2traceze2zz__tracez00(obj_t BgL_envz00_2047,
		obj_t BgL_lvlz00_2048, obj_t BgL_lblz00_2049, obj_t BgL_thunkz00_2050)
	{
		{	/* Llib/trace.scm 198 */
			{	/* Llib/trace.scm 199 */
				obj_t BgL_auxz00_2486;

				if (PROCEDUREP(BgL_thunkz00_2050))
					{	/* Llib/trace.scm 199 */
						BgL_auxz00_2486 = BgL_thunkz00_2050;
					}
				else
					{
						obj_t BgL_auxz00_2489;

						BgL_auxz00_2489 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1708z00zz__tracez00,
							BINT(7925L), BGl_string1720z00zz__tracez00,
							BGl_string1721z00zz__tracez00, BgL_thunkz00_2050);
						FAILURE(BgL_auxz00_2489, BFALSE, BFALSE);
					}
				return
					BGl_z52withzd2tracez80zz__tracez00(BgL_lvlz00_2048, BgL_lblz00_2049,
					BgL_auxz00_2486);
			}
		}

	}



/* &<@anonymous:1328> */
	obj_t BGl_z62zc3z04anonymousza31328ze3ze5zz__tracez00(obj_t BgL_envz00_2051)
	{
		{	/* Llib/trace.scm 217 */
			{	/* Llib/trace.scm 220 */
				obj_t BgL_alz00_2052;
				obj_t BgL_dz00_2053;
				obj_t BgL_omz00_2054;
				obj_t BgL_olz00_2055;

				BgL_alz00_2052 = ((obj_t) PROCEDURE_REF(BgL_envz00_2051, (int) (0L)));
				BgL_dz00_2053 = PROCEDURE_REF(BgL_envz00_2051, (int) (1L));
				BgL_omz00_2054 = PROCEDURE_REF(BgL_envz00_2051, (int) (2L));
				BgL_olz00_2055 = PROCEDURE_REF(BgL_envz00_2051, (int) (3L));
				{	/* Llib/trace.scm 220 */
					obj_t BgL_keyz00_2093;

					BgL_keyz00_2093 = BGl_symbol1696z00zz__tracez00;
					{	/* Llib/trace.scm 106 */
						obj_t BgL_cz00_2094;

						BgL_cz00_2094 =
							BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_keyz00_2093,
							BgL_alz00_2052);
						if (PAIRP(BgL_cz00_2094))
							{	/* Llib/trace.scm 107 */
								SET_CDR(BgL_cz00_2094, BgL_dz00_2053);
							}
						else
							{	/* Llib/trace.scm 107 */
								BGl_errorz00zz__errorz00(BGl_symbol1706z00zz__tracez00,
									BGl_string1705z00zz__tracez00, BgL_keyz00_2093);
							}
					}
				}
				{	/* Llib/trace.scm 221 */
					obj_t BgL_keyz00_2095;

					BgL_keyz00_2095 = BGl_symbol1698z00zz__tracez00;
					{	/* Llib/trace.scm 106 */
						obj_t BgL_cz00_2096;

						BgL_cz00_2096 =
							BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_keyz00_2095,
							BgL_alz00_2052);
						if (PAIRP(BgL_cz00_2096))
							{	/* Llib/trace.scm 107 */
								SET_CDR(BgL_cz00_2096, BgL_omz00_2054);
							}
						else
							{	/* Llib/trace.scm 107 */
								BGl_errorz00zz__errorz00(BGl_symbol1706z00zz__tracez00,
									BGl_string1705z00zz__tracez00, BgL_keyz00_2095);
							}
					}
				}
				{	/* Llib/trace.scm 222 */
					obj_t BgL_keyz00_2097;

					BgL_keyz00_2097 = BGl_symbol1701z00zz__tracez00;
					{	/* Llib/trace.scm 106 */
						obj_t BgL_cz00_2098;

						BgL_cz00_2098 =
							BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_keyz00_2097,
							BgL_alz00_2052);
						if (PAIRP(BgL_cz00_2098))
							{	/* Llib/trace.scm 107 */
								return SET_CDR(BgL_cz00_2098, BgL_olz00_2055);
							}
						else
							{	/* Llib/trace.scm 107 */
								return
									BGl_errorz00zz__errorz00(BGl_symbol1706z00zz__tracez00,
									BGl_string1705z00zz__tracez00, BgL_keyz00_2097);
							}
					}
				}
			}
		}

	}



/* &<@anonymous:1313> */
	obj_t BGl_z62zc3z04anonymousza31313ze3ze5zz__tracez00(obj_t BgL_envz00_2056)
	{
		{	/* Llib/trace.scm 208 */
			{	/* Llib/trace.scm 209 */
				obj_t BgL_alz00_2057;
				obj_t BgL_lblz00_2058;
				obj_t BgL_dz00_2059;

				BgL_alz00_2057 = ((obj_t) PROCEDURE_REF(BgL_envz00_2056, (int) (0L)));
				BgL_lblz00_2058 = PROCEDURE_REF(BgL_envz00_2056, (int) (1L));
				BgL_dz00_2059 = PROCEDURE_REF(BgL_envz00_2056, (int) (2L));
				{	/* Llib/trace.scm 209 */
					obj_t BgL_arg1314z00_2099;
					obj_t BgL_arg1315z00_2100;

					{	/* Llib/trace.scm 209 */
						obj_t BgL_keyz00_2101;

						BgL_keyz00_2101 = BGl_symbol1698z00zz__tracez00;
						{	/* Llib/trace.scm 97 */
							obj_t BgL_cz00_2102;

							BgL_cz00_2102 =
								BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_keyz00_2101,
								BgL_alz00_2057);
							if (PAIRP(BgL_cz00_2102))
								{	/* Llib/trace.scm 98 */
									BgL_arg1314z00_2099 = CDR(BgL_cz00_2102);
								}
							else
								{	/* Llib/trace.scm 98 */
									BgL_arg1314z00_2099 =
										BGl_errorz00zz__errorz00(BGl_symbol1703z00zz__tracez00,
										BGl_string1705z00zz__tracez00, BgL_keyz00_2101);
								}
						}
					}
					{	/* Llib/trace.scm 209 */
						obj_t BgL_tmpz00_2530;

						BgL_tmpz00_2530 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_arg1315z00_2100 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_2530);
					}
					bgl_display_obj(BgL_arg1314z00_2099, BgL_arg1315z00_2100);
				}
				{	/* Llib/trace.scm 210 */
					obj_t BgL_arg1316z00_2103;
					obj_t BgL_arg1317z00_2104;

					if (((long) CINT(BgL_dz00_2059) == 0L))
						{	/* Llib/trace.scm 211 */
							obj_t BgL_list1319z00_2105;

							{	/* Llib/trace.scm 211 */
								obj_t BgL_arg1320z00_2106;

								BgL_arg1320z00_2106 = MAKE_YOUNG_PAIR(BgL_lblz00_2058, BNIL);
								BgL_list1319z00_2105 =
									MAKE_YOUNG_PAIR(BGl_string1722z00zz__tracez00,
									BgL_arg1320z00_2106);
							}
							BgL_arg1316z00_2103 =
								BGl_ttyzd2tracezd2colorz00zz__tracez00(CINT(BgL_dz00_2059),
								BgL_list1319z00_2105);
						}
					else
						{	/* Llib/trace.scm 212 */
							obj_t BgL_list1321z00_2107;

							{	/* Llib/trace.scm 212 */
								obj_t BgL_arg1322z00_2108;

								BgL_arg1322z00_2108 = MAKE_YOUNG_PAIR(BgL_lblz00_2058, BNIL);
								BgL_list1321z00_2107 =
									MAKE_YOUNG_PAIR(BGl_string1723z00zz__tracez00,
									BgL_arg1322z00_2108);
							}
							BgL_arg1316z00_2103 =
								BGl_ttyzd2tracezd2colorz00zz__tracez00(CINT(BgL_dz00_2059),
								BgL_list1321z00_2107);
						}
					{	/* Llib/trace.scm 210 */
						obj_t BgL_tmpz00_2545;

						BgL_tmpz00_2545 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_arg1317z00_2104 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_2545);
					}
					bgl_display_obj(BgL_arg1316z00_2103, BgL_arg1317z00_2104);
				}
				{	/* Llib/trace.scm 213 */
					obj_t BgL_arg1323z00_2109;

					{	/* Llib/trace.scm 213 */
						obj_t BgL_tmpz00_2549;

						BgL_tmpz00_2549 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_arg1323z00_2109 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_2549);
					}
					bgl_display_char(((unsigned char) 10), BgL_arg1323z00_2109);
				}
				{	/* Llib/trace.scm 214 */
					obj_t BgL_arg1325z00_2110;

					{	/* Llib/trace.scm 214 */
						obj_t BgL_tmpz00_2553;

						BgL_tmpz00_2553 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_arg1325z00_2110 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_2553);
					}
					return bgl_flush_output_port(BgL_arg1325z00_2110);
				}
			}
		}

	}



/* &<@anonymous:1330> */
	obj_t BGl_z62zc3z04anonymousza31330ze3ze5zz__tracez00(obj_t BgL_envz00_2060)
	{
		{	/* Llib/trace.scm 223 */
			{	/* Llib/trace.scm 225 */
				obj_t BgL_alz00_2061;
				obj_t BgL_olz00_2062;

				BgL_alz00_2061 = ((obj_t) PROCEDURE_REF(BgL_envz00_2060, (int) (0L)));
				BgL_olz00_2062 = PROCEDURE_REF(BgL_envz00_2060, (int) (1L));
				{	/* Llib/trace.scm 225 */
					obj_t BgL_keyz00_2111;

					BgL_keyz00_2111 = BGl_symbol1701z00zz__tracez00;
					{	/* Llib/trace.scm 106 */
						obj_t BgL_cz00_2112;

						BgL_cz00_2112 =
							BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_keyz00_2111,
							BgL_alz00_2061);
						if (PAIRP(BgL_cz00_2112))
							{	/* Llib/trace.scm 107 */
								return SET_CDR(BgL_cz00_2112, BgL_olz00_2062);
							}
						else
							{	/* Llib/trace.scm 107 */
								return
									BGl_errorz00zz__errorz00(BGl_symbol1706z00zz__tracez00,
									BGl_string1705z00zz__tracez00, BgL_keyz00_2111);
							}
					}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__tracez00(void)
	{
		{	/* Llib/trace.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__tracez00(void)
	{
		{	/* Llib/trace.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__tracez00(void)
	{
		{	/* Llib/trace.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__tracez00(void)
	{
		{	/* Llib/trace.scm 15 */
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1724z00zz__tracez00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string1724z00zz__tracez00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string1724z00zz__tracez00));
			return
				BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L,
				BSTRING_TO_STRING(BGl_string1724z00zz__tracez00));
		}

	}

#ifdef __cplusplus
}
#endif
