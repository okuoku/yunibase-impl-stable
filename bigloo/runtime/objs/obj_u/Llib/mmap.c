/*===========================================================================*/
/*   (Llib/mmap.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Llib/mmap.scm -indent -o objs/obj_u/Llib/mmap.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___MMAP_TYPE_DEFINITIONS
#define BGL___MMAP_TYPE_DEFINITIONS
#endif													// BGL___MMAP_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_keyword1794z00zz__mmapz00 = BUNSPEC;
	static obj_t BGl_keyword1796z00zz__mmapz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_mmapzd2putzd2charz12z12zz__mmapz00(obj_t,
		unsigned char);
	static obj_t BGl_symbol1831z00zz__mmapz00 = BUNSPEC;
	static obj_t BGl_symbol1838z00zz__mmapz00 = BUNSPEC;
	extern obj_t BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62closezd2mmapzb0zz__mmapz00(obj_t, obj_t);
	static obj_t BGl_z62mmapzd2readzd2positionz62zz__mmapz00(obj_t, obj_t);
	static obj_t BGl_symbol1841z00zz__mmapz00 = BUNSPEC;
	extern obj_t bstring_to_keyword(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_mmapzd2ze3bstringz31zz__mmapz00(obj_t);
	static obj_t BGl_z62mmapzf3z91zz__mmapz00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__mmapz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_mmapzd2putzd2stringz12z12zz__mmapz00(obj_t,
		obj_t);
	static obj_t BGl_z62mmapzd2getzd2charz62zz__mmapz00(obj_t, obj_t);
	static obj_t BGl__openzd2mmapzd2zz__mmapz00(obj_t, obj_t);
	static obj_t BGl_z62mmapzd2substringzd2setz12z70zz__mmapz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__mmapz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_z62mmapzd2namezb0zz__mmapz00(obj_t, obj_t);
	static obj_t BGl_z62mmapzd2lengthzb0zz__mmapz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_mmapzd2setz12zc0zz__mmapz00(obj_t, long,
		unsigned char);
	BGL_EXPORTED_DECL obj_t BGl_openzd2mmapzd2zz__mmapz00(obj_t, obj_t, obj_t);
	extern obj_t string_to_bstring_len(char *, int);
	BGL_EXPORTED_DECL obj_t BGl_stringzd2ze3mmapz31zz__mmapz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol1798z00zz__mmapz00 = BUNSPEC;
	static obj_t BGl_z62mmapzd2refzd2urz62zz__mmapz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_mmapzd2getzd2stringz00zz__mmapz00(obj_t, long);
	static obj_t BGl_z62mmapzd2getzd2stringz62zz__mmapz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62mmapzd2ze3stringz53zz__mmapz00(obj_t, obj_t);
	static obj_t BGl_z62mmapzd2ze3bstringz53zz__mmapz00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__mmapz00(void);
	static obj_t BGl_genericzd2initzd2zz__mmapz00(void);
	static obj_t BGl_z62mmapzd2writezd2positionz62zz__mmapz00(obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__mmapz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__mmapz00(void);
	BGL_EXPORTED_DECL unsigned char BGl_mmapzd2refzd2zz__mmapz00(obj_t, long);
	static obj_t BGl_objectzd2initzd2zz__mmapz00(void);
	BGL_EXPORTED_DECL obj_t BGl_mmapzd2namezd2zz__mmapz00(obj_t);
	extern obj_t BGl_2zb2zb2zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62mmapzd2substringzb0zz__mmapz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_2zd2zd2zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL char *BGl_mmapzd2ze3stringz31zz__mmapz00(obj_t);
	static obj_t BGl_list1793z00zz__mmapz00 = BUNSPEC;
	extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62mmapzd2putzd2stringz12z70zz__mmapz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62mmapzd2setzd2urz12z70zz__mmapz00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL long
		BGl_mmapzd2writezd2positionzd2setz12zc0zz__mmapz00(obj_t, long);
	extern obj_t string_append_3(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_mmapzd2setzd2urz12z12zz__mmapz00(obj_t, long,
		unsigned char);
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_z62mmapzd2putzd2charz12z70zz__mmapz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_mmapzd2substringzd2setz12z12zz__mmapz00(obj_t,
		long, obj_t);
	BGL_EXPORTED_DECL long BGl_mmapzd2lengthzd2zz__mmapz00(obj_t);
	static obj_t BGl_methodzd2initzd2zz__mmapz00(void);
	BGL_EXPORTED_DECL unsigned char BGl_mmapzd2refzd2urz00zz__mmapz00(obj_t,
		long);
	BGL_EXPORTED_DECL obj_t BGl_mmapzd2substringzd2zz__mmapz00(obj_t, long, long);
	static obj_t BGl_z62mmapzd2refzb0zz__mmapz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol1806z00zz__mmapz00 = BUNSPEC;
	BGL_EXPORTED_DECL bool_t BGl_mmapzf3zf3zz__mmapz00(obj_t);
	BGL_EXPORTED_DECL long
		BGl_mmapzd2readzd2positionzd2setz12zc0zz__mmapz00(obj_t, long);
	static obj_t BGl_z62mmapzd2readzd2positionzd2setz12za2zz__mmapz00(obj_t,
		obj_t, obj_t);
	extern obj_t string_append(obj_t, obj_t);
	BGL_EXPORTED_DECL unsigned char BGl_mmapzd2getzd2charz00zz__mmapz00(obj_t);
	static obj_t BGl_z62mmapzd2writezd2positionzd2setz12za2zz__mmapz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL long BGl_mmapzd2writezd2positionz00zz__mmapz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_closezd2mmapzd2zz__mmapz00(obj_t);
	extern obj_t make_string_sans_fill(long);
	BGL_EXPORTED_DECL long BGl_mmapzd2readzd2positionz00zz__mmapz00(obj_t);
	static obj_t BGl_symbol1825z00zz__mmapz00 = BUNSPEC;
	static obj_t BGl_symbol1828z00zz__mmapz00 = BUNSPEC;
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62mmapzd2setz12za2zz__mmapz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl__stringzd2ze3mmapz31zz__mmapz00(obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string1795z00zz__mmapz00,
		BgL_bgl_string1795za700za7za7_1848za7, "read", 4);
	      DEFINE_STRING(BGl_string1797z00zz__mmapz00,
		BgL_bgl_string1797za700za7za7_1849za7, "write", 5);
	      DEFINE_STRING(BGl_string1799z00zz__mmapz00,
		BgL_bgl_string1799za700za7za7_1850za7, "open-mmap", 9);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_mmapzd2writezd2positionzd2envzd2zz__mmapz00,
		BgL_bgl_za762mmapza7d2writeza71851za7,
		BGl_z62mmapzd2writezd2positionz62zz__mmapz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_mmapzd2lengthzd2envz00zz__mmapz00,
		BgL_bgl_za762mmapza7d2length1852z00, BGl_z62mmapzd2lengthzb0zz__mmapz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_mmapzd2putzd2charz12zd2envzc0zz__mmapz00,
		BgL_bgl_za762mmapza7d2putza7d21853za7,
		BGl_z62mmapzd2putzd2charz12z70zz__mmapz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_mmapzd2writezd2positionzd2setz12zd2envz12zz__mmapz00,
		BgL_bgl_za762mmapza7d2writeza71854za7,
		BGl_z62mmapzd2writezd2positionzd2setz12za2zz__mmapz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_mmapzd2getzd2stringzd2envzd2zz__mmapz00,
		BgL_bgl_za762mmapza7d2getza7d21855za7,
		BGl_z62mmapzd2getzd2stringz62zz__mmapz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_mmapzf3zd2envz21zz__mmapz00,
		BgL_bgl_za762mmapza7f3za791za7za7_1856za7, BGl_z62mmapzf3z91zz__mmapz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_closezd2mmapzd2envz00zz__mmapz00,
		BgL_bgl_za762closeza7d2mmapza71857za7, BGl_z62closezd2mmapzb0zz__mmapz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_mmapzd2substringzd2setz12zd2envzc0zz__mmapz00,
		BgL_bgl_za762mmapza7d2substr1858z00,
		BGl_z62mmapzd2substringzd2setz12z70zz__mmapz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_mmapzd2readzd2positionzd2envzd2zz__mmapz00,
		BgL_bgl_za762mmapza7d2readza7d1859za7,
		BGl_z62mmapzd2readzd2positionz62zz__mmapz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_mmapzd2setz12zd2envz12zz__mmapz00,
		BgL_bgl_za762mmapza7d2setza7121860za7, BGl_z62mmapzd2setz12za2zz__mmapz00,
		0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_stringzd2ze3mmapzd2envze3zz__mmapz00,
		BgL_bgl__stringza7d2za7e3mma1861z00, opt_generic_entry,
		BGl__stringzd2ze3mmapz31zz__mmapz00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_mmapzd2putzd2stringz12zd2envzc0zz__mmapz00,
		BgL_bgl_za762mmapza7d2putza7d21862za7,
		BGl_z62mmapzd2putzd2stringz12z70zz__mmapz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_mmapzd2getzd2charzd2envzd2zz__mmapz00,
		BgL_bgl_za762mmapza7d2getza7d21863za7,
		BGl_z62mmapzd2getzd2charz62zz__mmapz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1800z00zz__mmapz00,
		BgL_bgl_string1800za700za7za7_1864za7, "Illegal keyword argument", 24);
	      DEFINE_STRING(BGl_string1801z00zz__mmapz00,
		BgL_bgl_string1801za700za7za7_1865za7,
		"wrong number of arguments: [1..3] expected, provided", 52);
	      DEFINE_STRING(BGl_string1802z00zz__mmapz00,
		BgL_bgl_string1802za700za7za7_1866za7, "/tmp/bigloo/runtime/Llib/mmap.scm",
		33);
	      DEFINE_STRING(BGl_string1803z00zz__mmapz00,
		BgL_bgl_string1803za700za7za7_1867za7, "_open-mmap", 10);
	      DEFINE_STRING(BGl_string1804z00zz__mmapz00,
		BgL_bgl_string1804za700za7za7_1868za7, "bint", 4);
	      DEFINE_STRING(BGl_string1805z00zz__mmapz00,
		BgL_bgl_string1805za700za7za7_1869za7, "bstring", 7);
	      DEFINE_STRING(BGl_string1807z00zz__mmapz00,
		BgL_bgl_string1807za700za7za7_1870za7, "string->mmap", 12);
	      DEFINE_STRING(BGl_string1808z00zz__mmapz00,
		BgL_bgl_string1808za700za7za7_1871za7, "_string->mmap", 13);
	      DEFINE_STRING(BGl_string1809z00zz__mmapz00,
		BgL_bgl_string1809za700za7za7_1872za7, "&mmap-name", 10);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_mmapzd2refzd2envz00zz__mmapz00,
		BgL_bgl_za762mmapza7d2refza7b01873za7, BGl_z62mmapzd2refzb0zz__mmapz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_mmapzd2namezd2envz00zz__mmapz00,
		BgL_bgl_za762mmapza7d2nameza7b1874za7, BGl_z62mmapzd2namezb0zz__mmapz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_openzd2mmapzd2envz00zz__mmapz00,
		BgL_bgl__openza7d2mmapza7d2za71875za7, opt_generic_entry,
		BGl__openzd2mmapzd2zz__mmapz00, BFALSE, -1);
	      DEFINE_STRING(BGl_string1810z00zz__mmapz00,
		BgL_bgl_string1810za700za7za7_1876za7, "mmap", 4);
	      DEFINE_STRING(BGl_string1811z00zz__mmapz00,
		BgL_bgl_string1811za700za7za7_1877za7, "&mmap->string", 13);
	      DEFINE_STRING(BGl_string1812z00zz__mmapz00,
		BgL_bgl_string1812za700za7za7_1878za7, "&mmap->bstring", 14);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_mmapzd2ze3stringzd2envze3zz__mmapz00,
		BgL_bgl_za762mmapza7d2za7e3str1879za7,
		BGl_z62mmapzd2ze3stringz53zz__mmapz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1813z00zz__mmapz00,
		BgL_bgl_string1813za700za7za7_1880za7, "&close-mmap", 11);
	      DEFINE_STRING(BGl_string1814z00zz__mmapz00,
		BgL_bgl_string1814za700za7za7_1881za7, "&mmap-length", 12);
	      DEFINE_STRING(BGl_string1815z00zz__mmapz00,
		BgL_bgl_string1815za700za7za7_1882za7, "&mmap-read-position", 19);
	      DEFINE_STRING(BGl_string1816z00zz__mmapz00,
		BgL_bgl_string1816za700za7za7_1883za7, "&mmap-read-position-set!", 24);
	      DEFINE_STRING(BGl_string1817z00zz__mmapz00,
		BgL_bgl_string1817za700za7za7_1884za7, "belong", 6);
	      DEFINE_STRING(BGl_string1818z00zz__mmapz00,
		BgL_bgl_string1818za700za7za7_1885za7, "&mmap-write-position", 20);
	      DEFINE_STRING(BGl_string1819z00zz__mmapz00,
		BgL_bgl_string1819za700za7za7_1886za7, "&mmap-write-position-set!", 25);
	      DEFINE_STRING(BGl_string1820z00zz__mmapz00,
		BgL_bgl_string1820za700za7za7_1887za7, "&mmap-ref-ur", 12);
	      DEFINE_STRING(BGl_string1821z00zz__mmapz00,
		BgL_bgl_string1821za700za7za7_1888za7, "&mmap-set-ur!", 13);
	      DEFINE_STRING(BGl_string1822z00zz__mmapz00,
		BgL_bgl_string1822za700za7za7_1889za7, "bchar", 5);
	      DEFINE_STRING(BGl_string1823z00zz__mmapz00,
		BgL_bgl_string1823za700za7za7_1890za7, "index out of range [0..", 23);
	      DEFINE_STRING(BGl_string1824z00zz__mmapz00,
		BgL_bgl_string1824za700za7za7_1891za7, "]", 1);
	      DEFINE_STRING(BGl_string1826z00zz__mmapz00,
		BgL_bgl_string1826za700za7za7_1892za7, "mmap-ref", 8);
	      DEFINE_STRING(BGl_string1827z00zz__mmapz00,
		BgL_bgl_string1827za700za7za7_1893za7, "&mmap-ref", 9);
	      DEFINE_STRING(BGl_string1829z00zz__mmapz00,
		BgL_bgl_string1829za700za7za7_1894za7, "mmap-set!", 9);
	      DEFINE_STRING(BGl_string1830z00zz__mmapz00,
		BgL_bgl_string1830za700za7za7_1895za7, "&mmap-set!", 10);
	      DEFINE_STRING(BGl_string1832z00zz__mmapz00,
		BgL_bgl_string1832za700za7za7_1896za7, "mmap-substring", 14);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_mmapzd2ze3bstringzd2envze3zz__mmapz00,
		BgL_bgl_za762mmapza7d2za7e3bst1897za7,
		BGl_z62mmapzd2ze3bstringz53zz__mmapz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1833z00zz__mmapz00,
		BgL_bgl_string1833za700za7za7_1898za7, "length too small", 16);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_mmapzd2readzd2positionzd2setz12zd2envz12zz__mmapz00,
		BgL_bgl_za762mmapza7d2readza7d1899za7,
		BGl_z62mmapzd2readzd2positionzd2setz12za2zz__mmapz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1834z00zz__mmapz00,
		BgL_bgl_string1834za700za7za7_1900za7, "Illegal index", 13);
	      DEFINE_STRING(BGl_string1835z00zz__mmapz00,
		BgL_bgl_string1835za700za7za7_1901za7, "start+length bigger than ", 25);
	      DEFINE_STRING(BGl_string1836z00zz__mmapz00,
		BgL_bgl_string1836za700za7za7_1902za7, "&mmap-substring", 15);
	      DEFINE_STRING(BGl_string1837z00zz__mmapz00,
		BgL_bgl_string1837za700za7za7_1903za7, "mmap-substring-set!", 19);
	      DEFINE_STRING(BGl_string1839z00zz__mmapz00,
		BgL_bgl_string1839za700za7za7_1904za7, "mmap-sbustring-set!", 19);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_mmapzd2setzd2urz12zd2envzc0zz__mmapz00,
		BgL_bgl_za762mmapza7d2setza7d21905za7,
		BGl_z62mmapzd2setzd2urz12z70zz__mmapz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string1840z00zz__mmapz00,
		BgL_bgl_string1840za700za7za7_1906za7, "[", 1);
	      DEFINE_STRING(BGl_string1842z00zz__mmapz00,
		BgL_bgl_string1842za700za7za7_1907za7, "&mmap-substring-set!", 20);
	      DEFINE_STRING(BGl_string1843z00zz__mmapz00,
		BgL_bgl_string1843za700za7za7_1908za7, "&mmap-get-char", 14);
	      DEFINE_STRING(BGl_string1844z00zz__mmapz00,
		BgL_bgl_string1844za700za7za7_1909za7, "&mmap-put-char!", 15);
	      DEFINE_STRING(BGl_string1845z00zz__mmapz00,
		BgL_bgl_string1845za700za7za7_1910za7, "&mmap-get-string", 16);
	      DEFINE_STRING(BGl_string1846z00zz__mmapz00,
		BgL_bgl_string1846za700za7za7_1911za7, "&mmap-put-string!", 17);
	      DEFINE_STRING(BGl_string1847z00zz__mmapz00,
		BgL_bgl_string1847za700za7za7_1912za7, "__mmap", 6);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_mmapzd2refzd2urzd2envzd2zz__mmapz00,
		BgL_bgl_za762mmapza7d2refza7d21913za7, BGl_z62mmapzd2refzd2urz62zz__mmapz00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_mmapzd2substringzd2envz00zz__mmapz00,
		BgL_bgl_za762mmapza7d2substr1914z00, BGl_z62mmapzd2substringzb0zz__mmapz00,
		0L, BUNSPEC, 3);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_keyword1794z00zz__mmapz00));
		     ADD_ROOT((void *) (&BGl_keyword1796z00zz__mmapz00));
		     ADD_ROOT((void *) (&BGl_symbol1831z00zz__mmapz00));
		     ADD_ROOT((void *) (&BGl_symbol1838z00zz__mmapz00));
		     ADD_ROOT((void *) (&BGl_symbol1841z00zz__mmapz00));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__mmapz00));
		     ADD_ROOT((void *) (&BGl_symbol1798z00zz__mmapz00));
		     ADD_ROOT((void *) (&BGl_list1793z00zz__mmapz00));
		     ADD_ROOT((void *) (&BGl_symbol1806z00zz__mmapz00));
		     ADD_ROOT((void *) (&BGl_symbol1825z00zz__mmapz00));
		     ADD_ROOT((void *) (&BGl_symbol1828z00zz__mmapz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__mmapz00(long
		BgL_checksumz00_2341, char *BgL_fromz00_2342)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__mmapz00))
				{
					BGl_requirezd2initializa7ationz75zz__mmapz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__mmapz00();
					BGl_cnstzd2initzd2zz__mmapz00();
					BGl_importedzd2moduleszd2initz00zz__mmapz00();
					return BGl_methodzd2initzd2zz__mmapz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__mmapz00(void)
	{
		{	/* Llib/mmap.scm 19 */
			BGl_keyword1794z00zz__mmapz00 =
				bstring_to_keyword(BGl_string1795z00zz__mmapz00);
			BGl_keyword1796z00zz__mmapz00 =
				bstring_to_keyword(BGl_string1797z00zz__mmapz00);
			BGl_list1793z00zz__mmapz00 =
				MAKE_YOUNG_PAIR(BGl_keyword1794z00zz__mmapz00,
				MAKE_YOUNG_PAIR(BGl_keyword1796z00zz__mmapz00, BNIL));
			BGl_symbol1798z00zz__mmapz00 =
				bstring_to_symbol(BGl_string1799z00zz__mmapz00);
			BGl_symbol1806z00zz__mmapz00 =
				bstring_to_symbol(BGl_string1807z00zz__mmapz00);
			BGl_symbol1825z00zz__mmapz00 =
				bstring_to_symbol(BGl_string1826z00zz__mmapz00);
			BGl_symbol1828z00zz__mmapz00 =
				bstring_to_symbol(BGl_string1829z00zz__mmapz00);
			BGl_symbol1831z00zz__mmapz00 =
				bstring_to_symbol(BGl_string1832z00zz__mmapz00);
			BGl_symbol1838z00zz__mmapz00 =
				bstring_to_symbol(BGl_string1839z00zz__mmapz00);
			return (BGl_symbol1841z00zz__mmapz00 =
				bstring_to_symbol(BGl_string1837z00zz__mmapz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__mmapz00(void)
	{
		{	/* Llib/mmap.scm 19 */
			return bgl_gc_roots_register();
		}

	}



/* mmap? */
	BGL_EXPORTED_DEF bool_t BGl_mmapzf3zf3zz__mmapz00(obj_t BgL_objz00_3)
	{
		{	/* Llib/mmap.scm 117 */
			return BGL_MMAPP(BgL_objz00_3);
		}

	}



/* &mmap? */
	obj_t BGl_z62mmapzf3z91zz__mmapz00(obj_t BgL_envz00_2167,
		obj_t BgL_objz00_2168)
	{
		{	/* Llib/mmap.scm 117 */
			return BBOOL(BGl_mmapzf3zf3zz__mmapz00(BgL_objz00_2168));
		}

	}



/* _open-mmap */
	obj_t BGl__openzd2mmapzd2zz__mmapz00(obj_t BgL_env1102z00_8,
		obj_t BgL_opt1101z00_7)
	{
		{	/* Llib/mmap.scm 123 */
			{	/* Llib/mmap.scm 123 */
				obj_t BgL_g1109z00_1143;

				BgL_g1109z00_1143 = VECTOR_REF(BgL_opt1101z00_7, 0L);
				{	/* Llib/mmap.scm 123 */
					obj_t BgL_readz00_1144;

					BgL_readz00_1144 = BTRUE;
					{	/* Llib/mmap.scm 123 */
						obj_t BgL_writez00_1145;

						BgL_writez00_1145 = BTRUE;
						{	/* Llib/mmap.scm 123 */

							{
								long BgL_iz00_1146;

								BgL_iz00_1146 = 1L;
							BgL_check1105z00_1147:
								if ((BgL_iz00_1146 == VECTOR_LENGTH(BgL_opt1101z00_7)))
									{	/* Llib/mmap.scm 123 */
										BNIL;
									}
								else
									{	/* Llib/mmap.scm 123 */
										bool_t BgL_test1917z00_2369;

										{	/* Llib/mmap.scm 123 */
											obj_t BgL_arg1196z00_1153;

											BgL_arg1196z00_1153 =
												VECTOR_REF(BgL_opt1101z00_7, BgL_iz00_1146);
											BgL_test1917z00_2369 =
												CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
												(BgL_arg1196z00_1153, BGl_list1793z00zz__mmapz00));
										}
										if (BgL_test1917z00_2369)
											{
												long BgL_iz00_2373;

												BgL_iz00_2373 = (BgL_iz00_1146 + 2L);
												BgL_iz00_1146 = BgL_iz00_2373;
												goto BgL_check1105z00_1147;
											}
										else
											{	/* Llib/mmap.scm 123 */
												obj_t BgL_arg1194z00_1152;

												BgL_arg1194z00_1152 =
													VECTOR_REF(BgL_opt1101z00_7, BgL_iz00_1146);
												BGl_errorz00zz__errorz00(BGl_symbol1798z00zz__mmapz00,
													BGl_string1800z00zz__mmapz00, BgL_arg1194z00_1152);
											}
									}
							}
							{	/* Llib/mmap.scm 123 */
								obj_t BgL_index1107z00_1154;

								{
									long BgL_iz00_1710;

									BgL_iz00_1710 = 1L;
								BgL_search1104z00_1709:
									if ((BgL_iz00_1710 == VECTOR_LENGTH(BgL_opt1101z00_7)))
										{	/* Llib/mmap.scm 123 */
											BgL_index1107z00_1154 = BINT(-1L);
										}
									else
										{	/* Llib/mmap.scm 123 */
											if (
												(BgL_iz00_1710 ==
													(VECTOR_LENGTH(BgL_opt1101z00_7) - 1L)))
												{	/* Llib/mmap.scm 123 */
													BgL_index1107z00_1154 =
														BGl_errorz00zz__errorz00
														(BGl_symbol1798z00zz__mmapz00,
														BGl_string1801z00zz__mmapz00,
														BINT(VECTOR_LENGTH(BgL_opt1101z00_7)));
												}
											else
												{	/* Llib/mmap.scm 123 */
													obj_t BgL_vz00_1720;

													BgL_vz00_1720 =
														VECTOR_REF(BgL_opt1101z00_7, BgL_iz00_1710);
													if ((BgL_vz00_1720 == BGl_keyword1794z00zz__mmapz00))
														{	/* Llib/mmap.scm 123 */
															BgL_index1107z00_1154 =
																BINT((BgL_iz00_1710 + 1L));
														}
													else
														{
															long BgL_iz00_2393;

															BgL_iz00_2393 = (BgL_iz00_1710 + 2L);
															BgL_iz00_1710 = BgL_iz00_2393;
															goto BgL_search1104z00_1709;
														}
												}
										}
								}
								{	/* Llib/mmap.scm 123 */
									bool_t BgL_test1921z00_2395;

									{	/* Llib/mmap.scm 123 */
										long BgL_n1z00_1724;

										{	/* Llib/mmap.scm 123 */
											obj_t BgL_tmpz00_2396;

											if (INTEGERP(BgL_index1107z00_1154))
												{	/* Llib/mmap.scm 123 */
													BgL_tmpz00_2396 = BgL_index1107z00_1154;
												}
											else
												{
													obj_t BgL_auxz00_2399;

													BgL_auxz00_2399 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string1802z00zz__mmapz00, BINT(5352L),
														BGl_string1803z00zz__mmapz00,
														BGl_string1804z00zz__mmapz00,
														BgL_index1107z00_1154);
													FAILURE(BgL_auxz00_2399, BFALSE, BFALSE);
												}
											BgL_n1z00_1724 = (long) CINT(BgL_tmpz00_2396);
										}
										BgL_test1921z00_2395 = (BgL_n1z00_1724 >= 0L);
									}
									if (BgL_test1921z00_2395)
										{
											long BgL_auxz00_2405;

											{	/* Llib/mmap.scm 123 */
												obj_t BgL_tmpz00_2406;

												if (INTEGERP(BgL_index1107z00_1154))
													{	/* Llib/mmap.scm 123 */
														BgL_tmpz00_2406 = BgL_index1107z00_1154;
													}
												else
													{
														obj_t BgL_auxz00_2409;

														BgL_auxz00_2409 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string1802z00zz__mmapz00, BINT(5352L),
															BGl_string1803z00zz__mmapz00,
															BGl_string1804z00zz__mmapz00,
															BgL_index1107z00_1154);
														FAILURE(BgL_auxz00_2409, BFALSE, BFALSE);
													}
												BgL_auxz00_2405 = (long) CINT(BgL_tmpz00_2406);
											}
											BgL_readz00_1144 =
												VECTOR_REF(BgL_opt1101z00_7, BgL_auxz00_2405);
										}
									else
										{	/* Llib/mmap.scm 123 */
											BFALSE;
										}
								}
							}
							{	/* Llib/mmap.scm 123 */
								obj_t BgL_index1108z00_1156;

								{
									long BgL_iz00_1726;

									BgL_iz00_1726 = 1L;
								BgL_search1104z00_1725:
									if ((BgL_iz00_1726 == VECTOR_LENGTH(BgL_opt1101z00_7)))
										{	/* Llib/mmap.scm 123 */
											BgL_index1108z00_1156 = BINT(-1L);
										}
									else
										{	/* Llib/mmap.scm 123 */
											if (
												(BgL_iz00_1726 ==
													(VECTOR_LENGTH(BgL_opt1101z00_7) - 1L)))
												{	/* Llib/mmap.scm 123 */
													BgL_index1108z00_1156 =
														BGl_errorz00zz__errorz00
														(BGl_symbol1798z00zz__mmapz00,
														BGl_string1801z00zz__mmapz00,
														BINT(VECTOR_LENGTH(BgL_opt1101z00_7)));
												}
											else
												{	/* Llib/mmap.scm 123 */
													obj_t BgL_vz00_1736;

													BgL_vz00_1736 =
														VECTOR_REF(BgL_opt1101z00_7, BgL_iz00_1726);
													if ((BgL_vz00_1736 == BGl_keyword1796z00zz__mmapz00))
														{	/* Llib/mmap.scm 123 */
															BgL_index1108z00_1156 =
																BINT((BgL_iz00_1726 + 1L));
														}
													else
														{
															long BgL_iz00_2431;

															BgL_iz00_2431 = (BgL_iz00_1726 + 2L);
															BgL_iz00_1726 = BgL_iz00_2431;
															goto BgL_search1104z00_1725;
														}
												}
										}
								}
								{	/* Llib/mmap.scm 123 */
									bool_t BgL_test1927z00_2433;

									{	/* Llib/mmap.scm 123 */
										long BgL_n1z00_1740;

										{	/* Llib/mmap.scm 123 */
											obj_t BgL_tmpz00_2434;

											if (INTEGERP(BgL_index1108z00_1156))
												{	/* Llib/mmap.scm 123 */
													BgL_tmpz00_2434 = BgL_index1108z00_1156;
												}
											else
												{
													obj_t BgL_auxz00_2437;

													BgL_auxz00_2437 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string1802z00zz__mmapz00, BINT(5352L),
														BGl_string1803z00zz__mmapz00,
														BGl_string1804z00zz__mmapz00,
														BgL_index1108z00_1156);
													FAILURE(BgL_auxz00_2437, BFALSE, BFALSE);
												}
											BgL_n1z00_1740 = (long) CINT(BgL_tmpz00_2434);
										}
										BgL_test1927z00_2433 = (BgL_n1z00_1740 >= 0L);
									}
									if (BgL_test1927z00_2433)
										{
											long BgL_auxz00_2443;

											{	/* Llib/mmap.scm 123 */
												obj_t BgL_tmpz00_2444;

												if (INTEGERP(BgL_index1108z00_1156))
													{	/* Llib/mmap.scm 123 */
														BgL_tmpz00_2444 = BgL_index1108z00_1156;
													}
												else
													{
														obj_t BgL_auxz00_2447;

														BgL_auxz00_2447 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string1802z00zz__mmapz00, BINT(5352L),
															BGl_string1803z00zz__mmapz00,
															BGl_string1804z00zz__mmapz00,
															BgL_index1108z00_1156);
														FAILURE(BgL_auxz00_2447, BFALSE, BFALSE);
													}
												BgL_auxz00_2443 = (long) CINT(BgL_tmpz00_2444);
											}
											BgL_writez00_1145 =
												VECTOR_REF(BgL_opt1101z00_7, BgL_auxz00_2443);
										}
									else
										{	/* Llib/mmap.scm 123 */
											BFALSE;
										}
								}
							}
							{	/* Llib/mmap.scm 123 */
								obj_t BgL_arg1199z00_1158;

								BgL_arg1199z00_1158 = VECTOR_REF(BgL_opt1101z00_7, 0L);
								{	/* Llib/mmap.scm 123 */
									obj_t BgL_readz00_1159;

									BgL_readz00_1159 = BgL_readz00_1144;
									{	/* Llib/mmap.scm 123 */
										obj_t BgL_writez00_1160;

										BgL_writez00_1160 = BgL_writez00_1145;
										{	/* Llib/mmap.scm 123 */
											obj_t BgL_namez00_1741;

											if (STRINGP(BgL_arg1199z00_1158))
												{	/* Llib/mmap.scm 123 */
													BgL_namez00_1741 = BgL_arg1199z00_1158;
												}
											else
												{
													obj_t BgL_auxz00_2456;

													BgL_auxz00_2456 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string1802z00zz__mmapz00, BINT(5352L),
														BGl_string1803z00zz__mmapz00,
														BGl_string1805z00zz__mmapz00, BgL_arg1199z00_1158);
													FAILURE(BgL_auxz00_2456, BFALSE, BFALSE);
												}
											{	/* Llib/mmap.scm 124 */
												bool_t BgL_auxz00_2462;
												bool_t BgL_tmpz00_2460;

												BgL_auxz00_2462 = CBOOL(BgL_writez00_1160);
												BgL_tmpz00_2460 = CBOOL(BgL_readz00_1159);
												return
													bgl_open_mmap(BgL_namez00_1741, BgL_tmpz00_2460,
													BgL_auxz00_2462);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* open-mmap */
	BGL_EXPORTED_DEF obj_t BGl_openzd2mmapzd2zz__mmapz00(obj_t BgL_namez00_4,
		obj_t BgL_readz00_5, obj_t BgL_writez00_6)
	{
		{	/* Llib/mmap.scm 123 */
			{	/* Llib/mmap.scm 124 */
				bool_t BgL_auxz00_2467;
				bool_t BgL_tmpz00_2465;

				BgL_auxz00_2467 = CBOOL(BgL_writez00_6);
				BgL_tmpz00_2465 = CBOOL(BgL_readz00_5);
				return bgl_open_mmap(BgL_namez00_4, BgL_tmpz00_2465, BgL_auxz00_2467);
			}
		}

	}



/* _string->mmap */
	obj_t BGl__stringzd2ze3mmapz31zz__mmapz00(obj_t BgL_env1111z00_13,
		obj_t BgL_opt1110z00_12)
	{
		{	/* Llib/mmap.scm 129 */
			{	/* Llib/mmap.scm 129 */
				obj_t BgL_g1118z00_1172;

				BgL_g1118z00_1172 = VECTOR_REF(BgL_opt1110z00_12, 0L);
				{	/* Llib/mmap.scm 129 */
					obj_t BgL_readz00_1173;

					BgL_readz00_1173 = BTRUE;
					{	/* Llib/mmap.scm 129 */
						obj_t BgL_writez00_1174;

						BgL_writez00_1174 = BTRUE;
						{	/* Llib/mmap.scm 129 */

							{
								long BgL_iz00_1175;

								BgL_iz00_1175 = 1L;
							BgL_check1114z00_1176:
								if ((BgL_iz00_1175 == VECTOR_LENGTH(BgL_opt1110z00_12)))
									{	/* Llib/mmap.scm 129 */
										BNIL;
									}
								else
									{	/* Llib/mmap.scm 129 */
										bool_t BgL_test1932z00_2474;

										{	/* Llib/mmap.scm 129 */
											obj_t BgL_arg1216z00_1182;

											BgL_arg1216z00_1182 =
												VECTOR_REF(BgL_opt1110z00_12, BgL_iz00_1175);
											BgL_test1932z00_2474 =
												CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
												(BgL_arg1216z00_1182, BGl_list1793z00zz__mmapz00));
										}
										if (BgL_test1932z00_2474)
											{
												long BgL_iz00_2478;

												BgL_iz00_2478 = (BgL_iz00_1175 + 2L);
												BgL_iz00_1175 = BgL_iz00_2478;
												goto BgL_check1114z00_1176;
											}
										else
											{	/* Llib/mmap.scm 129 */
												obj_t BgL_arg1215z00_1181;

												BgL_arg1215z00_1181 =
													VECTOR_REF(BgL_opt1110z00_12, BgL_iz00_1175);
												BGl_errorz00zz__errorz00(BGl_symbol1806z00zz__mmapz00,
													BGl_string1800z00zz__mmapz00, BgL_arg1215z00_1181);
											}
									}
							}
							{	/* Llib/mmap.scm 129 */
								obj_t BgL_index1116z00_1183;

								{
									long BgL_iz00_1758;

									BgL_iz00_1758 = 1L;
								BgL_search1113z00_1757:
									if ((BgL_iz00_1758 == VECTOR_LENGTH(BgL_opt1110z00_12)))
										{	/* Llib/mmap.scm 129 */
											BgL_index1116z00_1183 = BINT(-1L);
										}
									else
										{	/* Llib/mmap.scm 129 */
											if (
												(BgL_iz00_1758 ==
													(VECTOR_LENGTH(BgL_opt1110z00_12) - 1L)))
												{	/* Llib/mmap.scm 129 */
													BgL_index1116z00_1183 =
														BGl_errorz00zz__errorz00
														(BGl_symbol1806z00zz__mmapz00,
														BGl_string1801z00zz__mmapz00,
														BINT(VECTOR_LENGTH(BgL_opt1110z00_12)));
												}
											else
												{	/* Llib/mmap.scm 129 */
													obj_t BgL_vz00_1768;

													BgL_vz00_1768 =
														VECTOR_REF(BgL_opt1110z00_12, BgL_iz00_1758);
													if ((BgL_vz00_1768 == BGl_keyword1794z00zz__mmapz00))
														{	/* Llib/mmap.scm 129 */
															BgL_index1116z00_1183 =
																BINT((BgL_iz00_1758 + 1L));
														}
													else
														{
															long BgL_iz00_2498;

															BgL_iz00_2498 = (BgL_iz00_1758 + 2L);
															BgL_iz00_1758 = BgL_iz00_2498;
															goto BgL_search1113z00_1757;
														}
												}
										}
								}
								{	/* Llib/mmap.scm 129 */
									bool_t BgL_test1936z00_2500;

									{	/* Llib/mmap.scm 129 */
										long BgL_n1z00_1772;

										{	/* Llib/mmap.scm 129 */
											obj_t BgL_tmpz00_2501;

											if (INTEGERP(BgL_index1116z00_1183))
												{	/* Llib/mmap.scm 129 */
													BgL_tmpz00_2501 = BgL_index1116z00_1183;
												}
											else
												{
													obj_t BgL_auxz00_2504;

													BgL_auxz00_2504 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string1802z00zz__mmapz00, BINT(5669L),
														BGl_string1808z00zz__mmapz00,
														BGl_string1804z00zz__mmapz00,
														BgL_index1116z00_1183);
													FAILURE(BgL_auxz00_2504, BFALSE, BFALSE);
												}
											BgL_n1z00_1772 = (long) CINT(BgL_tmpz00_2501);
										}
										BgL_test1936z00_2500 = (BgL_n1z00_1772 >= 0L);
									}
									if (BgL_test1936z00_2500)
										{
											long BgL_auxz00_2510;

											{	/* Llib/mmap.scm 129 */
												obj_t BgL_tmpz00_2511;

												if (INTEGERP(BgL_index1116z00_1183))
													{	/* Llib/mmap.scm 129 */
														BgL_tmpz00_2511 = BgL_index1116z00_1183;
													}
												else
													{
														obj_t BgL_auxz00_2514;

														BgL_auxz00_2514 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string1802z00zz__mmapz00, BINT(5669L),
															BGl_string1808z00zz__mmapz00,
															BGl_string1804z00zz__mmapz00,
															BgL_index1116z00_1183);
														FAILURE(BgL_auxz00_2514, BFALSE, BFALSE);
													}
												BgL_auxz00_2510 = (long) CINT(BgL_tmpz00_2511);
											}
											BgL_readz00_1173 =
												VECTOR_REF(BgL_opt1110z00_12, BgL_auxz00_2510);
										}
									else
										{	/* Llib/mmap.scm 129 */
											BFALSE;
										}
								}
							}
							{	/* Llib/mmap.scm 129 */
								obj_t BgL_index1117z00_1185;

								{
									long BgL_iz00_1774;

									BgL_iz00_1774 = 1L;
								BgL_search1113z00_1773:
									if ((BgL_iz00_1774 == VECTOR_LENGTH(BgL_opt1110z00_12)))
										{	/* Llib/mmap.scm 129 */
											BgL_index1117z00_1185 = BINT(-1L);
										}
									else
										{	/* Llib/mmap.scm 129 */
											if (
												(BgL_iz00_1774 ==
													(VECTOR_LENGTH(BgL_opt1110z00_12) - 1L)))
												{	/* Llib/mmap.scm 129 */
													BgL_index1117z00_1185 =
														BGl_errorz00zz__errorz00
														(BGl_symbol1806z00zz__mmapz00,
														BGl_string1801z00zz__mmapz00,
														BINT(VECTOR_LENGTH(BgL_opt1110z00_12)));
												}
											else
												{	/* Llib/mmap.scm 129 */
													obj_t BgL_vz00_1784;

													BgL_vz00_1784 =
														VECTOR_REF(BgL_opt1110z00_12, BgL_iz00_1774);
													if ((BgL_vz00_1784 == BGl_keyword1796z00zz__mmapz00))
														{	/* Llib/mmap.scm 129 */
															BgL_index1117z00_1185 =
																BINT((BgL_iz00_1774 + 1L));
														}
													else
														{
															long BgL_iz00_2536;

															BgL_iz00_2536 = (BgL_iz00_1774 + 2L);
															BgL_iz00_1774 = BgL_iz00_2536;
															goto BgL_search1113z00_1773;
														}
												}
										}
								}
								{	/* Llib/mmap.scm 129 */
									bool_t BgL_test1942z00_2538;

									{	/* Llib/mmap.scm 129 */
										long BgL_n1z00_1788;

										{	/* Llib/mmap.scm 129 */
											obj_t BgL_tmpz00_2539;

											if (INTEGERP(BgL_index1117z00_1185))
												{	/* Llib/mmap.scm 129 */
													BgL_tmpz00_2539 = BgL_index1117z00_1185;
												}
											else
												{
													obj_t BgL_auxz00_2542;

													BgL_auxz00_2542 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string1802z00zz__mmapz00, BINT(5669L),
														BGl_string1808z00zz__mmapz00,
														BGl_string1804z00zz__mmapz00,
														BgL_index1117z00_1185);
													FAILURE(BgL_auxz00_2542, BFALSE, BFALSE);
												}
											BgL_n1z00_1788 = (long) CINT(BgL_tmpz00_2539);
										}
										BgL_test1942z00_2538 = (BgL_n1z00_1788 >= 0L);
									}
									if (BgL_test1942z00_2538)
										{
											long BgL_auxz00_2548;

											{	/* Llib/mmap.scm 129 */
												obj_t BgL_tmpz00_2549;

												if (INTEGERP(BgL_index1117z00_1185))
													{	/* Llib/mmap.scm 129 */
														BgL_tmpz00_2549 = BgL_index1117z00_1185;
													}
												else
													{
														obj_t BgL_auxz00_2552;

														BgL_auxz00_2552 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string1802z00zz__mmapz00, BINT(5669L),
															BGl_string1808z00zz__mmapz00,
															BGl_string1804z00zz__mmapz00,
															BgL_index1117z00_1185);
														FAILURE(BgL_auxz00_2552, BFALSE, BFALSE);
													}
												BgL_auxz00_2548 = (long) CINT(BgL_tmpz00_2549);
											}
											BgL_writez00_1174 =
												VECTOR_REF(BgL_opt1110z00_12, BgL_auxz00_2548);
										}
									else
										{	/* Llib/mmap.scm 129 */
											BFALSE;
										}
								}
							}
							{	/* Llib/mmap.scm 129 */
								obj_t BgL_arg1219z00_1187;

								BgL_arg1219z00_1187 = VECTOR_REF(BgL_opt1110z00_12, 0L);
								{	/* Llib/mmap.scm 129 */
									obj_t BgL_readz00_1188;

									BgL_readz00_1188 = BgL_readz00_1173;
									{	/* Llib/mmap.scm 129 */
										obj_t BgL_writez00_1189;

										BgL_writez00_1189 = BgL_writez00_1174;
										{	/* Llib/mmap.scm 129 */
											obj_t BgL_sz00_1789;

											if (STRINGP(BgL_arg1219z00_1187))
												{	/* Llib/mmap.scm 129 */
													BgL_sz00_1789 = BgL_arg1219z00_1187;
												}
											else
												{
													obj_t BgL_auxz00_2561;

													BgL_auxz00_2561 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string1802z00zz__mmapz00, BINT(5669L),
														BGl_string1808z00zz__mmapz00,
														BGl_string1805z00zz__mmapz00, BgL_arg1219z00_1187);
													FAILURE(BgL_auxz00_2561, BFALSE, BFALSE);
												}
											{	/* Llib/mmap.scm 130 */
												bool_t BgL_auxz00_2567;
												bool_t BgL_tmpz00_2565;

												BgL_auxz00_2567 = CBOOL(BgL_writez00_1189);
												BgL_tmpz00_2565 = CBOOL(BgL_readz00_1188);
												return
													bgl_string_to_mmap(BgL_sz00_1789, BgL_tmpz00_2565,
													BgL_auxz00_2567);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* string->mmap */
	BGL_EXPORTED_DEF obj_t BGl_stringzd2ze3mmapz31zz__mmapz00(obj_t BgL_sz00_9,
		obj_t BgL_readz00_10, obj_t BgL_writez00_11)
	{
		{	/* Llib/mmap.scm 129 */
			{	/* Llib/mmap.scm 130 */
				bool_t BgL_auxz00_2572;
				bool_t BgL_tmpz00_2570;

				BgL_auxz00_2572 = CBOOL(BgL_writez00_11);
				BgL_tmpz00_2570 = CBOOL(BgL_readz00_10);
				return bgl_string_to_mmap(BgL_sz00_9, BgL_tmpz00_2570, BgL_auxz00_2572);
			}
		}

	}



/* mmap-name */
	BGL_EXPORTED_DEF obj_t BGl_mmapzd2namezd2zz__mmapz00(obj_t BgL_mmapz00_14)
	{
		{	/* Llib/mmap.scm 135 */
			return BGL_MMAP_NAME(BgL_mmapz00_14);
		}

	}



/* &mmap-name */
	obj_t BGl_z62mmapzd2namezb0zz__mmapz00(obj_t BgL_envz00_2169,
		obj_t BgL_mmapz00_2170)
	{
		{	/* Llib/mmap.scm 135 */
			{	/* Llib/mmap.scm 136 */
				obj_t BgL_auxz00_2576;

				if (BGL_MMAPP(BgL_mmapz00_2170))
					{	/* Llib/mmap.scm 136 */
						BgL_auxz00_2576 = BgL_mmapz00_2170;
					}
				else
					{
						obj_t BgL_auxz00_2579;

						BgL_auxz00_2579 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1802z00zz__mmapz00,
							BINT(6027L), BGl_string1809z00zz__mmapz00,
							BGl_string1810z00zz__mmapz00, BgL_mmapz00_2170);
						FAILURE(BgL_auxz00_2579, BFALSE, BFALSE);
					}
				return BGl_mmapzd2namezd2zz__mmapz00(BgL_auxz00_2576);
			}
		}

	}



/* mmap->string */
	BGL_EXPORTED_DEF char *BGl_mmapzd2ze3stringz31zz__mmapz00(obj_t
		BgL_mmapz00_15)
	{
		{	/* Llib/mmap.scm 141 */
			return BGL_MMAP_TO_STRING(BgL_mmapz00_15);
		}

	}



/* &mmap->string */
	obj_t BGl_z62mmapzd2ze3stringz53zz__mmapz00(obj_t BgL_envz00_2171,
		obj_t BgL_mmapz00_2172)
	{
		{	/* Llib/mmap.scm 141 */
			{	/* Llib/mmap.scm 142 */
				char *BgL_tmpz00_2585;

				{	/* Llib/mmap.scm 142 */
					obj_t BgL_auxz00_2586;

					if (BGL_MMAPP(BgL_mmapz00_2172))
						{	/* Llib/mmap.scm 142 */
							BgL_auxz00_2586 = BgL_mmapz00_2172;
						}
					else
						{
							obj_t BgL_auxz00_2589;

							BgL_auxz00_2589 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1802z00zz__mmapz00,
								BINT(6321L), BGl_string1811z00zz__mmapz00,
								BGl_string1810z00zz__mmapz00, BgL_mmapz00_2172);
							FAILURE(BgL_auxz00_2589, BFALSE, BFALSE);
						}
					BgL_tmpz00_2585 = BGl_mmapzd2ze3stringz31zz__mmapz00(BgL_auxz00_2586);
				}
				return string_to_bstring(BgL_tmpz00_2585);
			}
		}

	}



/* mmap->bstring */
	BGL_EXPORTED_DEF obj_t BGl_mmapzd2ze3bstringz31zz__mmapz00(obj_t
		BgL_mmapz00_16)
	{
		{	/* Llib/mmap.scm 147 */
			{	/* Llib/mmap.scm 148 */
				int BgL_lenz00_2311;

				{	/* Llib/mmap.scm 148 */
					long BgL_arg1228z00_2312;

					BgL_arg1228z00_2312 = BGL_MMAP_LENGTH(BgL_mmapz00_16);
					BgL_lenz00_2311 = (int) ((long) (BgL_arg1228z00_2312));
				}
				{	/* Llib/mmap.scm 149 */
					char *BgL_arg1227z00_2313;

					BgL_arg1227z00_2313 = BGL_MMAP_TO_STRING(BgL_mmapz00_16);
					return string_to_bstring_len(BgL_arg1227z00_2313, BgL_lenz00_2311);
				}
			}
		}

	}



/* &mmap->bstring */
	obj_t BGl_z62mmapzd2ze3bstringz53zz__mmapz00(obj_t BgL_envz00_2173,
		obj_t BgL_mmapz00_2174)
	{
		{	/* Llib/mmap.scm 147 */
			{	/* Llib/mmap.scm 148 */
				obj_t BgL_auxz00_2600;

				if (BGL_MMAPP(BgL_mmapz00_2174))
					{	/* Llib/mmap.scm 148 */
						BgL_auxz00_2600 = BgL_mmapz00_2174;
					}
				else
					{
						obj_t BgL_auxz00_2603;

						BgL_auxz00_2603 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1802z00zz__mmapz00,
							BINT(6620L), BGl_string1812z00zz__mmapz00,
							BGl_string1810z00zz__mmapz00, BgL_mmapz00_2174);
						FAILURE(BgL_auxz00_2603, BFALSE, BFALSE);
					}
				return BGl_mmapzd2ze3bstringz31zz__mmapz00(BgL_auxz00_2600);
			}
		}

	}



/* close-mmap */
	BGL_EXPORTED_DEF obj_t BGl_closezd2mmapzd2zz__mmapz00(obj_t BgL_mmapz00_17)
	{
		{	/* Llib/mmap.scm 154 */
			return bgl_close_mmap(BgL_mmapz00_17);
		}

	}



/* &close-mmap */
	obj_t BGl_z62closezd2mmapzb0zz__mmapz00(obj_t BgL_envz00_2175,
		obj_t BgL_mmapz00_2176)
	{
		{	/* Llib/mmap.scm 154 */
			{	/* Llib/mmap.scm 155 */
				obj_t BgL_auxz00_2609;

				if (BGL_MMAPP(BgL_mmapz00_2176))
					{	/* Llib/mmap.scm 155 */
						BgL_auxz00_2609 = BgL_mmapz00_2176;
					}
				else
					{
						obj_t BgL_auxz00_2612;

						BgL_auxz00_2612 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1802z00zz__mmapz00,
							BINT(6996L), BGl_string1813z00zz__mmapz00,
							BGl_string1810z00zz__mmapz00, BgL_mmapz00_2176);
						FAILURE(BgL_auxz00_2612, BFALSE, BFALSE);
					}
				return BGl_closezd2mmapzd2zz__mmapz00(BgL_auxz00_2609);
			}
		}

	}



/* mmap-length */
	BGL_EXPORTED_DEF long BGl_mmapzd2lengthzd2zz__mmapz00(obj_t BgL_objz00_18)
	{
		{	/* Llib/mmap.scm 160 */
			return BGL_MMAP_LENGTH(BgL_objz00_18);
		}

	}



/* &mmap-length */
	obj_t BGl_z62mmapzd2lengthzb0zz__mmapz00(obj_t BgL_envz00_2177,
		obj_t BgL_objz00_2178)
	{
		{	/* Llib/mmap.scm 160 */
			{	/* Llib/mmap.scm 161 */
				long BgL_tmpz00_2618;

				{	/* Llib/mmap.scm 161 */
					obj_t BgL_auxz00_2619;

					if (BGL_MMAPP(BgL_objz00_2178))
						{	/* Llib/mmap.scm 161 */
							BgL_auxz00_2619 = BgL_objz00_2178;
						}
					else
						{
							obj_t BgL_auxz00_2622;

							BgL_auxz00_2622 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1802z00zz__mmapz00,
								BINT(7281L), BGl_string1814z00zz__mmapz00,
								BGl_string1810z00zz__mmapz00, BgL_objz00_2178);
							FAILURE(BgL_auxz00_2622, BFALSE, BFALSE);
						}
					BgL_tmpz00_2618 = BGl_mmapzd2lengthzd2zz__mmapz00(BgL_auxz00_2619);
				}
				return make_belong(BgL_tmpz00_2618);
			}
		}

	}



/* mmap-read-position */
	BGL_EXPORTED_DEF long BGl_mmapzd2readzd2positionz00zz__mmapz00(obj_t
		BgL_mmz00_19)
	{
		{	/* Llib/mmap.scm 166 */
			return BGL_MMAP_RP_GET(BgL_mmz00_19);
		}

	}



/* &mmap-read-position */
	obj_t BGl_z62mmapzd2readzd2positionz62zz__mmapz00(obj_t BgL_envz00_2179,
		obj_t BgL_mmz00_2180)
	{
		{	/* Llib/mmap.scm 166 */
			{	/* Llib/mmap.scm 167 */
				long BgL_tmpz00_2629;

				{	/* Llib/mmap.scm 167 */
					obj_t BgL_auxz00_2630;

					if (BGL_MMAPP(BgL_mmz00_2180))
						{	/* Llib/mmap.scm 167 */
							BgL_auxz00_2630 = BgL_mmz00_2180;
						}
					else
						{
							obj_t BgL_auxz00_2633;

							BgL_auxz00_2633 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1802z00zz__mmapz00,
								BINT(7572L), BGl_string1815z00zz__mmapz00,
								BGl_string1810z00zz__mmapz00, BgL_mmz00_2180);
							FAILURE(BgL_auxz00_2633, BFALSE, BFALSE);
						}
					BgL_tmpz00_2629 =
						BGl_mmapzd2readzd2positionz00zz__mmapz00(BgL_auxz00_2630);
				}
				return make_belong(BgL_tmpz00_2629);
			}
		}

	}



/* mmap-read-position-set! */
	BGL_EXPORTED_DEF long BGl_mmapzd2readzd2positionzd2setz12zc0zz__mmapz00(obj_t
		BgL_mmz00_20, long BgL_pz00_21)
	{
		{	/* Llib/mmap.scm 172 */
			BGL_MMAP_RP_SET(BgL_mmz00_20, BgL_pz00_21);
			BUNSPEC;
			return BgL_pz00_21;
		}

	}



/* &mmap-read-position-set! */
	obj_t BGl_z62mmapzd2readzd2positionzd2setz12za2zz__mmapz00(obj_t
		BgL_envz00_2181, obj_t BgL_mmz00_2182, obj_t BgL_pz00_2183)
	{
		{	/* Llib/mmap.scm 172 */
			{	/* Llib/mmap.scm 173 */
				long BgL_tmpz00_2640;

				{	/* Llib/mmap.scm 173 */
					long BgL_auxz00_2648;
					obj_t BgL_auxz00_2641;

					{	/* Llib/mmap.scm 173 */
						obj_t BgL_tmpz00_2649;

						if (ELONGP(BgL_pz00_2183))
							{	/* Llib/mmap.scm 173 */
								BgL_tmpz00_2649 = BgL_pz00_2183;
							}
						else
							{
								obj_t BgL_auxz00_2652;

								BgL_auxz00_2652 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1802z00zz__mmapz00,
									BINT(7884L), BGl_string1816z00zz__mmapz00,
									BGl_string1817z00zz__mmapz00, BgL_pz00_2183);
								FAILURE(BgL_auxz00_2652, BFALSE, BFALSE);
							}
						BgL_auxz00_2648 = BELONG_TO_LONG(BgL_tmpz00_2649);
					}
					if (BGL_MMAPP(BgL_mmz00_2182))
						{	/* Llib/mmap.scm 173 */
							BgL_auxz00_2641 = BgL_mmz00_2182;
						}
					else
						{
							obj_t BgL_auxz00_2644;

							BgL_auxz00_2644 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1802z00zz__mmapz00,
								BINT(7884L), BGl_string1816z00zz__mmapz00,
								BGl_string1810z00zz__mmapz00, BgL_mmz00_2182);
							FAILURE(BgL_auxz00_2644, BFALSE, BFALSE);
						}
					BgL_tmpz00_2640 =
						BGl_mmapzd2readzd2positionzd2setz12zc0zz__mmapz00(BgL_auxz00_2641,
						BgL_auxz00_2648);
				}
				return make_belong(BgL_tmpz00_2640);
			}
		}

	}



/* mmap-write-position */
	BGL_EXPORTED_DEF long BGl_mmapzd2writezd2positionz00zz__mmapz00(obj_t
		BgL_mmz00_22)
	{
		{	/* Llib/mmap.scm 179 */
			return BGL_MMAP_WP_GET(BgL_mmz00_22);
		}

	}



/* &mmap-write-position */
	obj_t BGl_z62mmapzd2writezd2positionz62zz__mmapz00(obj_t BgL_envz00_2184,
		obj_t BgL_mmz00_2185)
	{
		{	/* Llib/mmap.scm 179 */
			{	/* Llib/mmap.scm 180 */
				long BgL_tmpz00_2660;

				{	/* Llib/mmap.scm 180 */
					obj_t BgL_auxz00_2661;

					if (BGL_MMAPP(BgL_mmz00_2185))
						{	/* Llib/mmap.scm 180 */
							BgL_auxz00_2661 = BgL_mmz00_2185;
						}
					else
						{
							obj_t BgL_auxz00_2664;

							BgL_auxz00_2664 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1802z00zz__mmapz00,
								BINT(8164L), BGl_string1818z00zz__mmapz00,
								BGl_string1810z00zz__mmapz00, BgL_mmz00_2185);
							FAILURE(BgL_auxz00_2664, BFALSE, BFALSE);
						}
					BgL_tmpz00_2660 =
						BGl_mmapzd2writezd2positionz00zz__mmapz00(BgL_auxz00_2661);
				}
				return make_belong(BgL_tmpz00_2660);
			}
		}

	}



/* mmap-write-position-set! */
	BGL_EXPORTED_DEF long BGl_mmapzd2writezd2positionzd2setz12zc0zz__mmapz00(obj_t
		BgL_mmz00_23, long BgL_pz00_24)
	{
		{	/* Llib/mmap.scm 185 */
			BGL_MMAP_WP_SET(BgL_mmz00_23, BgL_pz00_24);
			BUNSPEC;
			return BgL_pz00_24;
		}

	}



/* &mmap-write-position-set! */
	obj_t BGl_z62mmapzd2writezd2positionzd2setz12za2zz__mmapz00(obj_t
		BgL_envz00_2186, obj_t BgL_mmz00_2187, obj_t BgL_pz00_2188)
	{
		{	/* Llib/mmap.scm 185 */
			{	/* Llib/mmap.scm 186 */
				long BgL_tmpz00_2671;

				{	/* Llib/mmap.scm 186 */
					long BgL_auxz00_2679;
					obj_t BgL_auxz00_2672;

					{	/* Llib/mmap.scm 186 */
						obj_t BgL_tmpz00_2680;

						if (ELONGP(BgL_pz00_2188))
							{	/* Llib/mmap.scm 186 */
								BgL_tmpz00_2680 = BgL_pz00_2188;
							}
						else
							{
								obj_t BgL_auxz00_2683;

								BgL_auxz00_2683 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1802z00zz__mmapz00,
									BINT(8477L), BGl_string1819z00zz__mmapz00,
									BGl_string1817z00zz__mmapz00, BgL_pz00_2188);
								FAILURE(BgL_auxz00_2683, BFALSE, BFALSE);
							}
						BgL_auxz00_2679 = BELONG_TO_LONG(BgL_tmpz00_2680);
					}
					if (BGL_MMAPP(BgL_mmz00_2187))
						{	/* Llib/mmap.scm 186 */
							BgL_auxz00_2672 = BgL_mmz00_2187;
						}
					else
						{
							obj_t BgL_auxz00_2675;

							BgL_auxz00_2675 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1802z00zz__mmapz00,
								BINT(8477L), BGl_string1819z00zz__mmapz00,
								BGl_string1810z00zz__mmapz00, BgL_mmz00_2187);
							FAILURE(BgL_auxz00_2675, BFALSE, BFALSE);
						}
					BgL_tmpz00_2671 =
						BGl_mmapzd2writezd2positionzd2setz12zc0zz__mmapz00(BgL_auxz00_2672,
						BgL_auxz00_2679);
				}
				return make_belong(BgL_tmpz00_2671);
			}
		}

	}



/* mmap-ref-ur */
	BGL_EXPORTED_DEF unsigned char BGl_mmapzd2refzd2urz00zz__mmapz00(obj_t
		BgL_mmz00_25, long BgL_iz00_26)
	{
		{	/* Llib/mmap.scm 192 */
			{	/* Llib/mmap.scm 193 */
				unsigned char BgL_cz00_2314;

				BgL_cz00_2314 = BGL_MMAP_REF(BgL_mmz00_25, BgL_iz00_26);
				{	/* Llib/mmap.scm 194 */
					long BgL_arg1229z00_2315;

					BgL_arg1229z00_2315 = (BgL_iz00_26 + ((long) 1));
					BGL_MMAP_RP_SET(BgL_mmz00_25, BgL_arg1229z00_2315);
					BUNSPEC;
					BgL_arg1229z00_2315;
				}
				return BgL_cz00_2314;
			}
		}

	}



/* &mmap-ref-ur */
	obj_t BGl_z62mmapzd2refzd2urz62zz__mmapz00(obj_t BgL_envz00_2189,
		obj_t BgL_mmz00_2190, obj_t BgL_iz00_2191)
	{
		{	/* Llib/mmap.scm 192 */
			{	/* Llib/mmap.scm 193 */
				unsigned char BgL_tmpz00_2693;

				{	/* Llib/mmap.scm 193 */
					long BgL_auxz00_2701;
					obj_t BgL_auxz00_2694;

					{	/* Llib/mmap.scm 193 */
						obj_t BgL_tmpz00_2702;

						if (ELONGP(BgL_iz00_2191))
							{	/* Llib/mmap.scm 193 */
								BgL_tmpz00_2702 = BgL_iz00_2191;
							}
						else
							{
								obj_t BgL_auxz00_2705;

								BgL_auxz00_2705 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1802z00zz__mmapz00,
									BINT(8765L), BGl_string1820z00zz__mmapz00,
									BGl_string1817z00zz__mmapz00, BgL_iz00_2191);
								FAILURE(BgL_auxz00_2705, BFALSE, BFALSE);
							}
						BgL_auxz00_2701 = BELONG_TO_LONG(BgL_tmpz00_2702);
					}
					if (BGL_MMAPP(BgL_mmz00_2190))
						{	/* Llib/mmap.scm 193 */
							BgL_auxz00_2694 = BgL_mmz00_2190;
						}
					else
						{
							obj_t BgL_auxz00_2697;

							BgL_auxz00_2697 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1802z00zz__mmapz00,
								BINT(8765L), BGl_string1820z00zz__mmapz00,
								BGl_string1810z00zz__mmapz00, BgL_mmz00_2190);
							FAILURE(BgL_auxz00_2697, BFALSE, BFALSE);
						}
					BgL_tmpz00_2693 =
						BGl_mmapzd2refzd2urz00zz__mmapz00(BgL_auxz00_2694, BgL_auxz00_2701);
				}
				return BCHAR(BgL_tmpz00_2693);
			}
		}

	}



/* mmap-set-ur! */
	BGL_EXPORTED_DEF obj_t BGl_mmapzd2setzd2urz12z12zz__mmapz00(obj_t
		BgL_mmz00_27, long BgL_iz00_28, unsigned char BgL_cz00_29)
	{
		{	/* Llib/mmap.scm 200 */
			BGL_MMAP_SET(BgL_mmz00_27, BgL_iz00_28, BgL_cz00_29);
			{	/* Llib/mmap.scm 202 */
				long BgL_arg1230z00_2316;

				BgL_arg1230z00_2316 = (BgL_iz00_28 + ((long) 1));
				BGL_MMAP_WP_SET(BgL_mmz00_27, BgL_arg1230z00_2316);
				BUNSPEC;
				return make_belong(BgL_arg1230z00_2316);
			}
		}

	}



/* &mmap-set-ur! */
	obj_t BGl_z62mmapzd2setzd2urz12z70zz__mmapz00(obj_t BgL_envz00_2192,
		obj_t BgL_mmz00_2193, obj_t BgL_iz00_2194, obj_t BgL_cz00_2195)
	{
		{	/* Llib/mmap.scm 200 */
			{	/* Llib/mmap.scm 201 */
				unsigned char BgL_auxz00_2732;
				long BgL_auxz00_2723;
				obj_t BgL_auxz00_2716;

				{	/* Llib/mmap.scm 201 */
					obj_t BgL_tmpz00_2733;

					if (CHARP(BgL_cz00_2195))
						{	/* Llib/mmap.scm 201 */
							BgL_tmpz00_2733 = BgL_cz00_2195;
						}
					else
						{
							obj_t BgL_auxz00_2736;

							BgL_auxz00_2736 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1802z00zz__mmapz00,
								BINT(9159L), BGl_string1821z00zz__mmapz00,
								BGl_string1822z00zz__mmapz00, BgL_cz00_2195);
							FAILURE(BgL_auxz00_2736, BFALSE, BFALSE);
						}
					BgL_auxz00_2732 = CCHAR(BgL_tmpz00_2733);
				}
				{	/* Llib/mmap.scm 201 */
					obj_t BgL_tmpz00_2724;

					if (ELONGP(BgL_iz00_2194))
						{	/* Llib/mmap.scm 201 */
							BgL_tmpz00_2724 = BgL_iz00_2194;
						}
					else
						{
							obj_t BgL_auxz00_2727;

							BgL_auxz00_2727 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1802z00zz__mmapz00,
								BINT(9159L), BGl_string1821z00zz__mmapz00,
								BGl_string1817z00zz__mmapz00, BgL_iz00_2194);
							FAILURE(BgL_auxz00_2727, BFALSE, BFALSE);
						}
					BgL_auxz00_2723 = BELONG_TO_LONG(BgL_tmpz00_2724);
				}
				if (BGL_MMAPP(BgL_mmz00_2193))
					{	/* Llib/mmap.scm 201 */
						BgL_auxz00_2716 = BgL_mmz00_2193;
					}
				else
					{
						obj_t BgL_auxz00_2719;

						BgL_auxz00_2719 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1802z00zz__mmapz00,
							BINT(9159L), BGl_string1821z00zz__mmapz00,
							BGl_string1810z00zz__mmapz00, BgL_mmz00_2193);
						FAILURE(BgL_auxz00_2719, BFALSE, BFALSE);
					}
				return
					BGl_mmapzd2setzd2urz12z12zz__mmapz00(BgL_auxz00_2716, BgL_auxz00_2723,
					BgL_auxz00_2732);
			}
		}

	}



/* mmap-ref */
	BGL_EXPORTED_DEF unsigned char BGl_mmapzd2refzd2zz__mmapz00(obj_t
		BgL_mmz00_30, long BgL_iz00_31)
	{
		{	/* Llib/mmap.scm 207 */
			{	/* Llib/mmap.scm 208 */
				bool_t BgL_test1962z00_2742;

				{	/* Llib/mmap.scm 208 */
					long BgL_arg1238z00_2317;

					BgL_arg1238z00_2317 = BGL_MMAP_LENGTH(BgL_mmz00_30);
					BgL_test1962z00_2742 = BOUND_CHECK(BgL_iz00_31, BgL_arg1238z00_2317);
				}
				if (BgL_test1962z00_2742)
					{	/* Llib/mmap.scm 209 */
						unsigned char BgL_res1708z00_2318;

						{	/* Llib/mmap.scm 193 */
							unsigned char BgL_cz00_2319;

							BgL_cz00_2319 = BGL_MMAP_REF(BgL_mmz00_30, BgL_iz00_31);
							{	/* Llib/mmap.scm 194 */
								long BgL_arg1229z00_2320;

								BgL_arg1229z00_2320 = (BgL_iz00_31 + ((long) 1));
								BGL_MMAP_RP_SET(BgL_mmz00_30, BgL_arg1229z00_2320);
								BUNSPEC;
								BgL_arg1229z00_2320;
							}
							BgL_res1708z00_2318 = BgL_cz00_2319;
						}
						return BgL_res1708z00_2318;
					}
				else
					{	/* Llib/mmap.scm 212 */
						obj_t BgL_arg1233z00_2321;

						{	/* Llib/mmap.scm 212 */
							obj_t BgL_arg1234z00_2322;

							{	/* Llib/mmap.scm 212 */
								obj_t BgL_arg1236z00_2323;

								{	/* Llib/mmap.scm 212 */
									long BgL_a1084z00_2324;

									BgL_a1084z00_2324 = BGL_MMAP_LENGTH(BgL_mmz00_30);
									{	/* Llib/mmap.scm 212 */

										BgL_arg1236z00_2323 =
											BGl_2zd2zd2zz__r4_numbers_6_5z00(make_belong
											(BgL_a1084z00_2324), BINT(1L));
								}}
								{	/* Llib/mmap.scm 212 */

									BgL_arg1234z00_2322 =
										BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00
										(BgL_arg1236z00_2323, BINT(10L));
							}}
							BgL_arg1233z00_2321 =
								string_append_3(BGl_string1823z00zz__mmapz00,
								BgL_arg1234z00_2322, BGl_string1824z00zz__mmapz00);
						}
						return
							CCHAR(BGl_errorz00zz__errorz00(BGl_symbol1825z00zz__mmapz00,
								BgL_arg1233z00_2321, make_belong(BgL_iz00_31)));
					}
			}
		}

	}



/* &mmap-ref */
	obj_t BGl_z62mmapzd2refzb0zz__mmapz00(obj_t BgL_envz00_2196,
		obj_t BgL_mmz00_2197, obj_t BgL_iz00_2198)
	{
		{	/* Llib/mmap.scm 207 */
			{	/* Llib/mmap.scm 208 */
				unsigned char BgL_tmpz00_2758;

				{	/* Llib/mmap.scm 208 */
					long BgL_auxz00_2766;
					obj_t BgL_auxz00_2759;

					{	/* Llib/mmap.scm 208 */
						obj_t BgL_tmpz00_2767;

						if (ELONGP(BgL_iz00_2198))
							{	/* Llib/mmap.scm 208 */
								BgL_tmpz00_2767 = BgL_iz00_2198;
							}
						else
							{
								obj_t BgL_auxz00_2770;

								BgL_auxz00_2770 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1802z00zz__mmapz00,
									BINT(9487L), BGl_string1827z00zz__mmapz00,
									BGl_string1817z00zz__mmapz00, BgL_iz00_2198);
								FAILURE(BgL_auxz00_2770, BFALSE, BFALSE);
							}
						BgL_auxz00_2766 = BELONG_TO_LONG(BgL_tmpz00_2767);
					}
					if (BGL_MMAPP(BgL_mmz00_2197))
						{	/* Llib/mmap.scm 208 */
							BgL_auxz00_2759 = BgL_mmz00_2197;
						}
					else
						{
							obj_t BgL_auxz00_2762;

							BgL_auxz00_2762 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1802z00zz__mmapz00,
								BINT(9487L), BGl_string1827z00zz__mmapz00,
								BGl_string1810z00zz__mmapz00, BgL_mmz00_2197);
							FAILURE(BgL_auxz00_2762, BFALSE, BFALSE);
						}
					BgL_tmpz00_2758 =
						BGl_mmapzd2refzd2zz__mmapz00(BgL_auxz00_2759, BgL_auxz00_2766);
				}
				return BCHAR(BgL_tmpz00_2758);
			}
		}

	}



/* mmap-set! */
	BGL_EXPORTED_DEF obj_t BGl_mmapzd2setz12zc0zz__mmapz00(obj_t BgL_mmz00_32,
		long BgL_iz00_33, unsigned char BgL_cz00_34)
	{
		{	/* Llib/mmap.scm 219 */
			{	/* Llib/mmap.scm 220 */
				bool_t BgL_test1965z00_2777;

				{	/* Llib/mmap.scm 220 */
					long BgL_arg1252z00_2325;

					BgL_arg1252z00_2325 = BGL_MMAP_LENGTH(BgL_mmz00_32);
					BgL_test1965z00_2777 = BOUND_CHECK(BgL_iz00_33, BgL_arg1252z00_2325);
				}
				if (BgL_test1965z00_2777)
					{	/* Llib/mmap.scm 220 */
						BGL_MMAP_SET(BgL_mmz00_32, BgL_iz00_33, BgL_cz00_34);
						{	/* Llib/mmap.scm 202 */
							long BgL_arg1230z00_2326;

							BgL_arg1230z00_2326 = (BgL_iz00_33 + ((long) 1));
							BGL_MMAP_WP_SET(BgL_mmz00_32, BgL_arg1230z00_2326);
							BUNSPEC;
							return make_belong(BgL_arg1230z00_2326);
						}
					}
				else
					{	/* Llib/mmap.scm 224 */
						obj_t BgL_arg1242z00_2327;

						{	/* Llib/mmap.scm 224 */
							obj_t BgL_arg1244z00_2328;

							{	/* Llib/mmap.scm 224 */
								obj_t BgL_arg1248z00_2329;

								{	/* Llib/mmap.scm 224 */
									long BgL_a1086z00_2330;

									BgL_a1086z00_2330 = BGL_MMAP_LENGTH(BgL_mmz00_32);
									{	/* Llib/mmap.scm 224 */

										BgL_arg1248z00_2329 =
											BGl_2zd2zd2zz__r4_numbers_6_5z00(make_belong
											(BgL_a1086z00_2330), BINT(1L));
								}}
								{	/* Llib/mmap.scm 224 */

									BgL_arg1244z00_2328 =
										BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00
										(BgL_arg1248z00_2329, BINT(10L));
							}}
							BgL_arg1242z00_2327 =
								string_append_3(BGl_string1823z00zz__mmapz00,
								BgL_arg1244z00_2328, BGl_string1824z00zz__mmapz00);
						}
						return
							BGl_errorz00zz__errorz00(BGl_symbol1828z00zz__mmapz00,
							BgL_arg1242z00_2327, make_belong(BgL_iz00_33));
					}
			}
		}

	}



/* &mmap-set! */
	obj_t BGl_z62mmapzd2setz12za2zz__mmapz00(obj_t BgL_envz00_2199,
		obj_t BgL_mmz00_2200, obj_t BgL_iz00_2201, obj_t BgL_cz00_2202)
	{
		{	/* Llib/mmap.scm 219 */
			{	/* Llib/mmap.scm 220 */
				unsigned char BgL_auxz00_2809;
				long BgL_auxz00_2800;
				obj_t BgL_auxz00_2793;

				{	/* Llib/mmap.scm 220 */
					obj_t BgL_tmpz00_2810;

					if (CHARP(BgL_cz00_2202))
						{	/* Llib/mmap.scm 220 */
							BgL_tmpz00_2810 = BgL_cz00_2202;
						}
					else
						{
							obj_t BgL_auxz00_2813;

							BgL_auxz00_2813 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1802z00zz__mmapz00,
								BINT(9987L), BGl_string1830z00zz__mmapz00,
								BGl_string1822z00zz__mmapz00, BgL_cz00_2202);
							FAILURE(BgL_auxz00_2813, BFALSE, BFALSE);
						}
					BgL_auxz00_2809 = CCHAR(BgL_tmpz00_2810);
				}
				{	/* Llib/mmap.scm 220 */
					obj_t BgL_tmpz00_2801;

					if (ELONGP(BgL_iz00_2201))
						{	/* Llib/mmap.scm 220 */
							BgL_tmpz00_2801 = BgL_iz00_2201;
						}
					else
						{
							obj_t BgL_auxz00_2804;

							BgL_auxz00_2804 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1802z00zz__mmapz00,
								BINT(9987L), BGl_string1830z00zz__mmapz00,
								BGl_string1817z00zz__mmapz00, BgL_iz00_2201);
							FAILURE(BgL_auxz00_2804, BFALSE, BFALSE);
						}
					BgL_auxz00_2800 = BELONG_TO_LONG(BgL_tmpz00_2801);
				}
				if (BGL_MMAPP(BgL_mmz00_2200))
					{	/* Llib/mmap.scm 220 */
						BgL_auxz00_2793 = BgL_mmz00_2200;
					}
				else
					{
						obj_t BgL_auxz00_2796;

						BgL_auxz00_2796 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1802z00zz__mmapz00,
							BINT(9987L), BGl_string1830z00zz__mmapz00,
							BGl_string1810z00zz__mmapz00, BgL_mmz00_2200);
						FAILURE(BgL_auxz00_2796, BFALSE, BFALSE);
					}
				return
					BGl_mmapzd2setz12zc0zz__mmapz00(BgL_auxz00_2793, BgL_auxz00_2800,
					BgL_auxz00_2809);
			}
		}

	}



/* mmap-substring */
	BGL_EXPORTED_DEF obj_t BGl_mmapzd2substringzd2zz__mmapz00(obj_t BgL_mmz00_35,
		long BgL_startz00_36, long BgL_endz00_37)
	{
		{	/* Llib/mmap.scm 231 */
			if ((BgL_endz00_37 < BgL_startz00_36))
				{	/* Llib/mmap.scm 234 */
					long BgL_arg1268z00_1226;

					{	/* Llib/mmap.scm 234 */
						long BgL_res1709z00_1854;

						{	/* Llib/mmap.scm 234 */
							long BgL_tmpz00_2821;

							BgL_tmpz00_2821 = (BgL_endz00_37 - BgL_startz00_36);
							BgL_res1709z00_1854 = (long) (BgL_tmpz00_2821);
						}
						BgL_arg1268z00_1226 = BgL_res1709z00_1854;
					}
					return
						BGl_errorz00zz__errorz00(BGl_symbol1831z00zz__mmapz00,
						BGl_string1833z00zz__mmapz00, make_belong(BgL_arg1268z00_1226));
				}
			else
				{	/* Llib/mmap.scm 235 */
					bool_t BgL_test1970z00_2826;

					{	/* Llib/mmap.scm 235 */
						long BgL_arg1318z00_1250;

						{	/* Llib/mmap.scm 235 */
							long BgL_arg1319z00_1251;

							BgL_arg1319z00_1251 = BGL_MMAP_LENGTH(BgL_mmz00_35);
							BgL_arg1318z00_1250 = (((long) 1) + BgL_arg1319z00_1251);
						}
						BgL_test1970z00_2826 =
							BOUND_CHECK(BgL_endz00_37, BgL_arg1318z00_1250);
					}
					if (BgL_test1970z00_2826)
						{	/* Llib/mmap.scm 240 */
							bool_t BgL_test1971z00_2830;

							{	/* Llib/mmap.scm 240 */
								long BgL_arg1314z00_1244;

								BgL_arg1314z00_1244 = BGL_MMAP_LENGTH(BgL_mmz00_35);
								BgL_test1971z00_2830 =
									BOUND_CHECK(BgL_startz00_36, BgL_arg1314z00_1244);
							}
							if (BgL_test1971z00_2830)
								{	/* Llib/mmap.scm 243 */
									obj_t BgL_rz00_1232;

									{	/* Llib/mmap.scm 243 */
										long BgL_arg1311z00_1242;

										{	/* Llib/mmap.scm 243 */
											long BgL_arg1312z00_1243;

											{	/* Llib/mmap.scm 243 */
												long BgL_res1710z00_1861;

												{	/* Llib/mmap.scm 243 */
													long BgL_tmpz00_2833;

													BgL_tmpz00_2833 = (BgL_endz00_37 - BgL_startz00_36);
													BgL_res1710z00_1861 = (long) (BgL_tmpz00_2833);
												}
												BgL_arg1312z00_1243 = BgL_res1710z00_1861;
											}
											BgL_arg1311z00_1242 = (long) (BgL_arg1312z00_1243);
										}
										BgL_rz00_1232 = make_string_sans_fill(BgL_arg1311z00_1242);
									}
									{
										long BgL_iz00_1883;
										long BgL_jz00_1884;

										BgL_iz00_1883 = BgL_startz00_36;
										BgL_jz00_1884 = 0L;
									BgL_loopz00_1882:
										if ((BgL_iz00_1883 == BgL_endz00_37))
											{	/* Llib/mmap.scm 246 */
												BGL_MMAP_RP_SET(BgL_mmz00_35, BgL_iz00_1883);
												BUNSPEC;
												BgL_iz00_1883;
												return BgL_rz00_1232;
											}
										else
											{	/* Llib/mmap.scm 246 */
												{	/* Llib/mmap.scm 251 */
													unsigned char BgL_arg1308z00_1890;

													{	/* Llib/mmap.scm 251 */
														unsigned char BgL_res1711z00_1891;

														{	/* Llib/mmap.scm 193 */
															unsigned char BgL_cz00_1894;

															BgL_cz00_1894 =
																BGL_MMAP_REF(BgL_mmz00_35, BgL_iz00_1883);
															{	/* Llib/mmap.scm 194 */
																long BgL_arg1229z00_1895;

																BgL_arg1229z00_1895 =
																	(BgL_iz00_1883 + ((long) 1));
																BGL_MMAP_RP_SET(BgL_mmz00_35,
																	BgL_arg1229z00_1895);
																BUNSPEC;
																BgL_arg1229z00_1895;
															}
															BgL_res1711z00_1891 = BgL_cz00_1894;
														}
														BgL_arg1308z00_1890 = BgL_res1711z00_1891;
													}
													STRING_SET(BgL_rz00_1232, BgL_jz00_1884,
														BgL_arg1308z00_1890);
												}
												{	/* Llib/mmap.scm 252 */
													long BgL_arg1309z00_1903;
													long BgL_arg1310z00_1904;

													BgL_arg1309z00_1903 = (BgL_iz00_1883 + (long) (1L));
													BgL_arg1310z00_1904 = (BgL_jz00_1884 + 1L);
													{
														long BgL_jz00_2849;
														long BgL_iz00_2848;

														BgL_iz00_2848 = BgL_arg1309z00_1903;
														BgL_jz00_2849 = BgL_arg1310z00_1904;
														BgL_jz00_1884 = BgL_jz00_2849;
														BgL_iz00_1883 = BgL_iz00_2848;
														goto BgL_loopz00_1882;
													}
												}
											}
									}
								}
							else
								{	/* Llib/mmap.scm 240 */
									return
										BGl_errorz00zz__errorz00(BGl_symbol1831z00zz__mmapz00,
										BGl_string1834z00zz__mmapz00, make_belong(BgL_startz00_36));
								}
						}
					else
						{	/* Llib/mmap.scm 238 */
							obj_t BgL_arg1315z00_1245;

							{	/* Llib/mmap.scm 238 */
								obj_t BgL_arg1316z00_1246;

								{	/* Llib/mmap.scm 238 */
									long BgL_arg1317z00_1247;

									BgL_arg1317z00_1247 = BGL_MMAP_LENGTH(BgL_mmz00_35);
									{	/* Ieee/number.scm 174 */

										BgL_arg1316z00_1246 =
											BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(make_belong
											(BgL_arg1317z00_1247), BINT(10L));
								}}
								BgL_arg1315z00_1245 =
									string_append(BGl_string1835z00zz__mmapz00,
									BgL_arg1316z00_1246);
							}
							return
								BGl_errorz00zz__errorz00(BGl_symbol1831z00zz__mmapz00,
								BgL_arg1315z00_1245, make_belong(BgL_endz00_37));
						}
				}
		}

	}



/* &mmap-substring */
	obj_t BGl_z62mmapzd2substringzb0zz__mmapz00(obj_t BgL_envz00_2203,
		obj_t BgL_mmz00_2204, obj_t BgL_startz00_2205, obj_t BgL_endz00_2206)
	{
		{	/* Llib/mmap.scm 231 */
			{	/* Llib/mmap.scm 233 */
				long BgL_auxz00_2875;
				long BgL_auxz00_2866;
				obj_t BgL_auxz00_2859;

				{	/* Llib/mmap.scm 233 */
					obj_t BgL_tmpz00_2876;

					if (ELONGP(BgL_endz00_2206))
						{	/* Llib/mmap.scm 233 */
							BgL_tmpz00_2876 = BgL_endz00_2206;
						}
					else
						{
							obj_t BgL_auxz00_2879;

							BgL_auxz00_2879 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1802z00zz__mmapz00,
								BINT(10497L), BGl_string1836z00zz__mmapz00,
								BGl_string1817z00zz__mmapz00, BgL_endz00_2206);
							FAILURE(BgL_auxz00_2879, BFALSE, BFALSE);
						}
					BgL_auxz00_2875 = BELONG_TO_LONG(BgL_tmpz00_2876);
				}
				{	/* Llib/mmap.scm 233 */
					obj_t BgL_tmpz00_2867;

					if (ELONGP(BgL_startz00_2205))
						{	/* Llib/mmap.scm 233 */
							BgL_tmpz00_2867 = BgL_startz00_2205;
						}
					else
						{
							obj_t BgL_auxz00_2870;

							BgL_auxz00_2870 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1802z00zz__mmapz00,
								BINT(10497L), BGl_string1836z00zz__mmapz00,
								BGl_string1817z00zz__mmapz00, BgL_startz00_2205);
							FAILURE(BgL_auxz00_2870, BFALSE, BFALSE);
						}
					BgL_auxz00_2866 = BELONG_TO_LONG(BgL_tmpz00_2867);
				}
				if (BGL_MMAPP(BgL_mmz00_2204))
					{	/* Llib/mmap.scm 233 */
						BgL_auxz00_2859 = BgL_mmz00_2204;
					}
				else
					{
						obj_t BgL_auxz00_2862;

						BgL_auxz00_2862 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1802z00zz__mmapz00,
							BINT(10497L), BGl_string1836z00zz__mmapz00,
							BGl_string1810z00zz__mmapz00, BgL_mmz00_2204);
						FAILURE(BgL_auxz00_2862, BFALSE, BFALSE);
					}
				return
					BGl_mmapzd2substringzd2zz__mmapz00(BgL_auxz00_2859, BgL_auxz00_2866,
					BgL_auxz00_2875);
			}
		}

	}



/* mmap-substring-set! */
	BGL_EXPORTED_DEF obj_t BGl_mmapzd2substringzd2setz12z12zz__mmapz00(obj_t
		BgL_mmz00_38, long BgL_oz00_39, obj_t BgL_sz00_40)
	{
		{	/* Llib/mmap.scm 257 */
			{	/* Llib/mmap.scm 258 */
				long BgL_lenz00_1252;

				BgL_lenz00_1252 = STRING_LENGTH(BgL_sz00_40);
				if ((BgL_oz00_39 < ((long) 0)))
					{	/* Llib/mmap.scm 260 */
						return
							BGl_errorz00zz__errorz00(BGl_string1837z00zz__mmapz00,
							BGl_string1834z00zz__mmapz00, make_belong(BgL_oz00_39));
					}
				else
					{	/* Llib/mmap.scm 262 */
						bool_t BgL_test1977z00_2890;

						{	/* Llib/mmap.scm 262 */
							long BgL_arg1348z00_1285;

							{	/* Llib/mmap.scm 262 */
								long BgL_arg1349z00_1286;

								BgL_arg1349z00_1286 = BGL_MMAP_LENGTH(BgL_mmz00_38);
								BgL_arg1348z00_1285 = (BgL_arg1349z00_1286 + ((long) 1));
							}
							BgL_test1977z00_2890 =
								BOUND_CHECK(BgL_oz00_39, BgL_arg1348z00_1285);
						}
						if (BgL_test1977z00_2890)
							{	/* Llib/mmap.scm 268 */
								bool_t BgL_test1978z00_2894;

								{	/* Llib/mmap.scm 268 */
									long BgL_arg1340z00_1276;
									long BgL_arg1341z00_1277;

									{	/* Llib/mmap.scm 268 */
										long BgL_arg1342z00_1278;

										BgL_arg1342z00_1278 = (long) (BgL_lenz00_1252);
										BgL_arg1340z00_1276 = (BgL_oz00_39 + BgL_arg1342z00_1278);
									}
									{	/* Llib/mmap.scm 269 */
										long BgL_arg1343z00_1279;

										BgL_arg1343z00_1279 = BGL_MMAP_LENGTH(BgL_mmz00_38);
										BgL_arg1341z00_1277 = (BgL_arg1343z00_1279 + ((long) 1));
									}
									BgL_test1978z00_2894 =
										BOUND_CHECK(BgL_arg1340z00_1276, BgL_arg1341z00_1277);
								}
								if (BgL_test1978z00_2894)
									{
										long BgL_iz00_1939;
										long BgL_jz00_1940;

										BgL_iz00_1939 = 0L;
										BgL_jz00_1940 = BgL_oz00_39;
									BgL_loopz00_1938:
										if ((BgL_iz00_1939 == BgL_lenz00_1252))
											{	/* Llib/mmap.scm 278 */
												BGL_MMAP_WP_SET(BgL_mmz00_38, BgL_jz00_1940);
												BUNSPEC;
												BgL_jz00_1940;
												return BgL_mmz00_38;
											}
										else
											{	/* Llib/mmap.scm 278 */
												{	/* Llib/mmap.scm 283 */
													unsigned char BgL_arg1332z00_1946;

													BgL_arg1332z00_1946 =
														STRING_REF(BgL_sz00_40, BgL_iz00_1939);
													BGL_MMAP_SET(BgL_mmz00_38, BgL_jz00_1940,
														BgL_arg1332z00_1946);
													{	/* Llib/mmap.scm 202 */
														long BgL_arg1230z00_1952;

														BgL_arg1230z00_1952 = (BgL_jz00_1940 + ((long) 1));
														BGL_MMAP_WP_SET(BgL_mmz00_38, BgL_arg1230z00_1952);
														BUNSPEC;
														BgL_arg1230z00_1952;
												}}
												{	/* Llib/mmap.scm 284 */
													long BgL_arg1333z00_1957;
													long BgL_arg1334z00_1958;

													BgL_arg1333z00_1957 = (BgL_iz00_1939 + 1L);
													BgL_arg1334z00_1958 = (BgL_jz00_1940 + (long) (1L));
													{
														long BgL_jz00_2911;
														long BgL_iz00_2910;

														BgL_iz00_2910 = BgL_arg1333z00_1957;
														BgL_jz00_2911 = BgL_arg1334z00_1958;
														BgL_jz00_1940 = BgL_jz00_2911;
														BgL_iz00_1939 = BgL_iz00_2910;
														goto BgL_loopz00_1938;
													}
												}
											}
									}
								else
									{	/* Llib/mmap.scm 272 */
										obj_t BgL_arg1335z00_1271;
										obj_t BgL_arg1336z00_1272;

										{	/* Llib/mmap.scm 272 */
											obj_t BgL_arg1337z00_1273;

											{	/* Llib/mmap.scm 272 */
												long BgL_arg1338z00_1274;

												BgL_arg1338z00_1274 = BGL_MMAP_LENGTH(BgL_mmz00_38);
												BgL_arg1337z00_1273 =
													BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00
													(make_belong(BgL_arg1338z00_1274), BINT(1L));
											}
											BgL_arg1335z00_1271 =
												string_append_3(BGl_string1823z00zz__mmapz00,
												BgL_arg1337z00_1273, BGl_string1824z00zz__mmapz00);
										}
										BgL_arg1336z00_1272 =
											BGl_2zb2zb2zz__r4_numbers_6_5z00(make_belong(BgL_oz00_39),
											BINT(BgL_lenz00_1252));
										return
											BGl_errorz00zz__errorz00(BGl_symbol1838z00zz__mmapz00,
											BgL_arg1335z00_1271, BgL_arg1336z00_1272);
									}
							}
						else
							{	/* Llib/mmap.scm 265 */
								obj_t BgL_arg1344z00_1280;

								{	/* Llib/mmap.scm 265 */
									obj_t BgL_arg1346z00_1281;

									{	/* Llib/mmap.scm 265 */
										long BgL_arg1347z00_1282;

										BgL_arg1347z00_1282 = BGL_MMAP_LENGTH(BgL_mmz00_38);
										{	/* Ieee/number.scm 174 */

											BgL_arg1346z00_1281 =
												BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00
												(make_belong(BgL_arg1347z00_1282), BINT(10L));
									}}
									BgL_arg1344z00_1280 =
										string_append_3(BGl_string1823z00zz__mmapz00,
										BgL_arg1346z00_1281, BGl_string1840z00zz__mmapz00);
								}
								return
									BGl_errorz00zz__errorz00(BGl_symbol1841z00zz__mmapz00,
									BgL_arg1344z00_1280, make_belong(BgL_oz00_39));
							}
					}
			}
		}

	}



/* &mmap-substring-set! */
	obj_t BGl_z62mmapzd2substringzd2setz12z70zz__mmapz00(obj_t BgL_envz00_2207,
		obj_t BgL_mmz00_2208, obj_t BgL_oz00_2209, obj_t BgL_sz00_2210)
	{
		{	/* Llib/mmap.scm 257 */
			{	/* Llib/mmap.scm 258 */
				obj_t BgL_auxz00_2944;
				long BgL_auxz00_2935;
				obj_t BgL_auxz00_2928;

				if (STRINGP(BgL_sz00_2210))
					{	/* Llib/mmap.scm 258 */
						BgL_auxz00_2944 = BgL_sz00_2210;
					}
				else
					{
						obj_t BgL_auxz00_2947;

						BgL_auxz00_2947 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1802z00zz__mmapz00,
							BINT(11493L), BGl_string1842z00zz__mmapz00,
							BGl_string1805z00zz__mmapz00, BgL_sz00_2210);
						FAILURE(BgL_auxz00_2947, BFALSE, BFALSE);
					}
				{	/* Llib/mmap.scm 258 */
					obj_t BgL_tmpz00_2936;

					if (ELONGP(BgL_oz00_2209))
						{	/* Llib/mmap.scm 258 */
							BgL_tmpz00_2936 = BgL_oz00_2209;
						}
					else
						{
							obj_t BgL_auxz00_2939;

							BgL_auxz00_2939 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1802z00zz__mmapz00,
								BINT(11493L), BGl_string1842z00zz__mmapz00,
								BGl_string1817z00zz__mmapz00, BgL_oz00_2209);
							FAILURE(BgL_auxz00_2939, BFALSE, BFALSE);
						}
					BgL_auxz00_2935 = BELONG_TO_LONG(BgL_tmpz00_2936);
				}
				if (BGL_MMAPP(BgL_mmz00_2208))
					{	/* Llib/mmap.scm 258 */
						BgL_auxz00_2928 = BgL_mmz00_2208;
					}
				else
					{
						obj_t BgL_auxz00_2931;

						BgL_auxz00_2931 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1802z00zz__mmapz00,
							BINT(11493L), BGl_string1842z00zz__mmapz00,
							BGl_string1810z00zz__mmapz00, BgL_mmz00_2208);
						FAILURE(BgL_auxz00_2931, BFALSE, BFALSE);
					}
				return
					BGl_mmapzd2substringzd2setz12z12zz__mmapz00(BgL_auxz00_2928,
					BgL_auxz00_2935, BgL_auxz00_2944);
			}
		}

	}



/* mmap-get-char */
	BGL_EXPORTED_DEF unsigned char BGl_mmapzd2getzd2charz00zz__mmapz00(obj_t
		BgL_mmz00_41)
	{
		{	/* Llib/mmap.scm 289 */
			{	/* Llib/mmap.scm 290 */
				long BgL_arg1350z00_2331;

				BgL_arg1350z00_2331 = BGL_MMAP_RP_GET(BgL_mmz00_41);
				{	/* Llib/mmap.scm 290 */
					unsigned char BgL_res1712z00_2332;

					{	/* Llib/mmap.scm 193 */
						unsigned char BgL_cz00_2333;

						BgL_cz00_2333 = BGL_MMAP_REF(BgL_mmz00_41, BgL_arg1350z00_2331);
						{	/* Llib/mmap.scm 194 */
							long BgL_arg1229z00_2334;

							BgL_arg1229z00_2334 = (BgL_arg1350z00_2331 + ((long) 1));
							BGL_MMAP_RP_SET(BgL_mmz00_41, BgL_arg1229z00_2334);
							BUNSPEC;
							BgL_arg1229z00_2334;
						}
						BgL_res1712z00_2332 = BgL_cz00_2333;
					}
					return BgL_res1712z00_2332;
				}
			}
		}

	}



/* &mmap-get-char */
	obj_t BGl_z62mmapzd2getzd2charz62zz__mmapz00(obj_t BgL_envz00_2211,
		obj_t BgL_mmz00_2212)
	{
		{	/* Llib/mmap.scm 289 */
			{	/* Llib/mmap.scm 290 */
				unsigned char BgL_tmpz00_2956;

				{	/* Llib/mmap.scm 290 */
					obj_t BgL_auxz00_2957;

					if (BGL_MMAPP(BgL_mmz00_2212))
						{	/* Llib/mmap.scm 290 */
							BgL_auxz00_2957 = BgL_mmz00_2212;
						}
					else
						{
							obj_t BgL_auxz00_2960;

							BgL_auxz00_2960 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1802z00zz__mmapz00,
								BINT(12534L), BGl_string1843z00zz__mmapz00,
								BGl_string1810z00zz__mmapz00, BgL_mmz00_2212);
							FAILURE(BgL_auxz00_2960, BFALSE, BFALSE);
						}
					BgL_tmpz00_2956 =
						BGl_mmapzd2getzd2charz00zz__mmapz00(BgL_auxz00_2957);
				}
				return BCHAR(BgL_tmpz00_2956);
			}
		}

	}



/* mmap-put-char! */
	BGL_EXPORTED_DEF obj_t BGl_mmapzd2putzd2charz12z12zz__mmapz00(obj_t
		BgL_mmz00_42, unsigned char BgL_cz00_43)
	{
		{	/* Llib/mmap.scm 295 */
			{	/* Llib/mmap.scm 296 */
				long BgL_arg1351z00_2335;

				BgL_arg1351z00_2335 = BGL_MMAP_WP_GET(BgL_mmz00_42);
				BGL_MMAP_SET(BgL_mmz00_42, BgL_arg1351z00_2335, BgL_cz00_43);
				{	/* Llib/mmap.scm 202 */
					long BgL_arg1230z00_2336;

					BgL_arg1230z00_2336 = (BgL_arg1351z00_2335 + ((long) 1));
					BGL_MMAP_WP_SET(BgL_mmz00_42, BgL_arg1230z00_2336);
					BUNSPEC;
					return make_belong(BgL_arg1230z00_2336);
				}
			}
		}

	}



/* &mmap-put-char! */
	obj_t BGl_z62mmapzd2putzd2charz12z70zz__mmapz00(obj_t BgL_envz00_2213,
		obj_t BgL_mmz00_2214, obj_t BgL_cz00_2215)
	{
		{	/* Llib/mmap.scm 295 */
			{	/* Llib/mmap.scm 296 */
				unsigned char BgL_auxz00_2978;
				obj_t BgL_auxz00_2971;

				{	/* Llib/mmap.scm 296 */
					obj_t BgL_tmpz00_2979;

					if (CHARP(BgL_cz00_2215))
						{	/* Llib/mmap.scm 296 */
							BgL_tmpz00_2979 = BgL_cz00_2215;
						}
					else
						{
							obj_t BgL_auxz00_2982;

							BgL_auxz00_2982 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1802z00zz__mmapz00,
								BINT(12833L), BGl_string1844z00zz__mmapz00,
								BGl_string1822z00zz__mmapz00, BgL_cz00_2215);
							FAILURE(BgL_auxz00_2982, BFALSE, BFALSE);
						}
					BgL_auxz00_2978 = CCHAR(BgL_tmpz00_2979);
				}
				if (BGL_MMAPP(BgL_mmz00_2214))
					{	/* Llib/mmap.scm 296 */
						BgL_auxz00_2971 = BgL_mmz00_2214;
					}
				else
					{
						obj_t BgL_auxz00_2974;

						BgL_auxz00_2974 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1802z00zz__mmapz00,
							BINT(12833L), BGl_string1844z00zz__mmapz00,
							BGl_string1810z00zz__mmapz00, BgL_mmz00_2214);
						FAILURE(BgL_auxz00_2974, BFALSE, BFALSE);
					}
				return
					BGl_mmapzd2putzd2charz12z12zz__mmapz00(BgL_auxz00_2971,
					BgL_auxz00_2978);
			}
		}

	}



/* mmap-get-string */
	BGL_EXPORTED_DEF obj_t BGl_mmapzd2getzd2stringz00zz__mmapz00(obj_t
		BgL_mmz00_44, long BgL_lenz00_45)
	{
		{	/* Llib/mmap.scm 301 */
			{	/* Llib/mmap.scm 302 */
				long BgL_arg1352z00_2337;
				long BgL_arg1354z00_2338;

				BgL_arg1352z00_2337 = BGL_MMAP_RP_GET(BgL_mmz00_44);
				{	/* Llib/mmap.scm 302 */
					long BgL_arg1356z00_2339;

					BgL_arg1356z00_2339 = BGL_MMAP_RP_GET(BgL_mmz00_44);
					BgL_arg1354z00_2338 = (BgL_arg1356z00_2339 + BgL_lenz00_45);
				}
				return
					BGl_mmapzd2substringzd2zz__mmapz00(BgL_mmz00_44, BgL_arg1352z00_2337,
					BgL_arg1354z00_2338);
			}
		}

	}



/* &mmap-get-string */
	obj_t BGl_z62mmapzd2getzd2stringz62zz__mmapz00(obj_t BgL_envz00_2216,
		obj_t BgL_mmz00_2217, obj_t BgL_lenz00_2218)
	{
		{	/* Llib/mmap.scm 301 */
			{	/* Llib/mmap.scm 302 */
				long BgL_auxz00_2999;
				obj_t BgL_auxz00_2992;

				{	/* Llib/mmap.scm 302 */
					obj_t BgL_tmpz00_3000;

					if (ELONGP(BgL_lenz00_2218))
						{	/* Llib/mmap.scm 302 */
							BgL_tmpz00_3000 = BgL_lenz00_2218;
						}
					else
						{
							obj_t BgL_auxz00_3003;

							BgL_auxz00_3003 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1802z00zz__mmapz00,
								BINT(13142L), BGl_string1845z00zz__mmapz00,
								BGl_string1817z00zz__mmapz00, BgL_lenz00_2218);
							FAILURE(BgL_auxz00_3003, BFALSE, BFALSE);
						}
					BgL_auxz00_2999 = BELONG_TO_LONG(BgL_tmpz00_3000);
				}
				if (BGL_MMAPP(BgL_mmz00_2217))
					{	/* Llib/mmap.scm 302 */
						BgL_auxz00_2992 = BgL_mmz00_2217;
					}
				else
					{
						obj_t BgL_auxz00_2995;

						BgL_auxz00_2995 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1802z00zz__mmapz00,
							BINT(13142L), BGl_string1845z00zz__mmapz00,
							BGl_string1810z00zz__mmapz00, BgL_mmz00_2217);
						FAILURE(BgL_auxz00_2995, BFALSE, BFALSE);
					}
				return
					BGl_mmapzd2getzd2stringz00zz__mmapz00(BgL_auxz00_2992,
					BgL_auxz00_2999);
			}
		}

	}



/* mmap-put-string! */
	BGL_EXPORTED_DEF obj_t BGl_mmapzd2putzd2stringz12z12zz__mmapz00(obj_t
		BgL_mmz00_46, obj_t BgL_sz00_47)
	{
		{	/* Llib/mmap.scm 307 */
			{	/* Llib/mmap.scm 308 */
				long BgL_arg1357z00_2340;

				BgL_arg1357z00_2340 = BGL_MMAP_WP_GET(BgL_mmz00_46);
				return
					BGl_mmapzd2substringzd2setz12z12zz__mmapz00(BgL_mmz00_46,
					BgL_arg1357z00_2340, BgL_sz00_47);
			}
		}

	}



/* &mmap-put-string! */
	obj_t BGl_z62mmapzd2putzd2stringz12z70zz__mmapz00(obj_t BgL_envz00_2219,
		obj_t BgL_mmz00_2220, obj_t BgL_sz00_2221)
	{
		{	/* Llib/mmap.scm 307 */
			{	/* Llib/mmap.scm 308 */
				obj_t BgL_auxz00_3018;
				obj_t BgL_auxz00_3011;

				if (STRINGP(BgL_sz00_2221))
					{	/* Llib/mmap.scm 308 */
						BgL_auxz00_3018 = BgL_sz00_2221;
					}
				else
					{
						obj_t BgL_auxz00_3021;

						BgL_auxz00_3021 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1802z00zz__mmapz00,
							BINT(13480L), BGl_string1846z00zz__mmapz00,
							BGl_string1805z00zz__mmapz00, BgL_sz00_2221);
						FAILURE(BgL_auxz00_3021, BFALSE, BFALSE);
					}
				if (BGL_MMAPP(BgL_mmz00_2220))
					{	/* Llib/mmap.scm 308 */
						BgL_auxz00_3011 = BgL_mmz00_2220;
					}
				else
					{
						obj_t BgL_auxz00_3014;

						BgL_auxz00_3014 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1802z00zz__mmapz00,
							BINT(13480L), BGl_string1846z00zz__mmapz00,
							BGl_string1810z00zz__mmapz00, BgL_mmz00_2220);
						FAILURE(BgL_auxz00_3014, BFALSE, BFALSE);
					}
				return
					BGl_mmapzd2putzd2stringz12z12zz__mmapz00(BgL_auxz00_3011,
					BgL_auxz00_3018);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__mmapz00(void)
	{
		{	/* Llib/mmap.scm 19 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__mmapz00(void)
	{
		{	/* Llib/mmap.scm 19 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__mmapz00(void)
	{
		{	/* Llib/mmap.scm 19 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__mmapz00(void)
	{
		{	/* Llib/mmap.scm 19 */
			return
				BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1847z00zz__mmapz00));
		}

	}

#ifdef __cplusplus
}
#endif
