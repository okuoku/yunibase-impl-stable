/*===========================================================================*/
/*   (Llib/bexit.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Llib/bexit.scm -indent -o objs/obj_u/Llib/bexit.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___BEXIT_TYPE_DEFINITIONS
#define BGL___BEXIT_TYPE_DEFINITIONS
#endif													// BGL___BEXIT_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zz__bexitz00 = BUNSPEC;
	static obj_t BGl_z62envzd2getzd2exitdzd2valzb0zz__bexitz00(obj_t, obj_t);
	static obj_t BGl_z62valzd2fromzd2exitzf3z91zz__bexitz00(obj_t, obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_EXPORTED_DECL obj_t BGl_unwindzd2untilz12zc0zz__bexitz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t bgl_uncaught_exception_handler(obj_t);
	BGL_EXPORTED_DECL obj_t bgl_failsafe_mutex_profile(void);
	static obj_t BGl_z62z42failsafezd2mutexzd2profilez20zz__bexitz00(obj_t);
	extern obj_t BGl_evaluate2zd2restorezd2statez12z12zz__evaluatez00(obj_t);
	BGL_EXPORTED_DECL obj_t unwind_stack_until(obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_exitdzd2execzd2protectz00zz__bexitz00(obj_t);
	extern bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_envzd2getzd2exitdzd2valzd2zz__bexitz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_exitdzd2pushzd2protectz12z12zz__bexitz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62defaultzd2uncaughtzd2exceptionzd2handlerzb0zz__bexitz00(obj_t,
		obj_t);
	static obj_t BGl_genericzd2initzd2zz__bexitz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zz__bexitz00(void);
	static bool_t
		BGl_exitdzd2execzd2andzd2popzd2protectsz12z12zz__bexitz00(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zz__bexitz00(void);
	static obj_t BGl_objectzd2initzd2zz__bexitz00(void);
	static obj_t BGl_z62unwindzd2stackzd2valuezf3z91zz__bexitz00(obj_t, obj_t);
	static obj_t BGl_z62unwindzd2untilz12za2zz__bexitz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_valzd2fromzd2exitzf3zf3zz__bexitz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_envzd2setzd2exitdzd2valz12zc0zz__bexitz00(obj_t,
		obj_t);
	extern obj_t BGl_evaluate2zd2restorezd2bpz12z12zz__evaluatez00(int);
	static obj_t BGl_methodzd2initzd2zz__bexitz00(void);
	static obj_t BGl_z62envzd2setzd2exitdzd2valz12za2zz__bexitz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_exitdzd2popzd2protectz12z12zz__bexitz00(obj_t);
	static obj_t BGl_z62z42exitdzd2mutexzd2profilez20zz__bexitz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_exitdzd2protectzd2setz12z12zz__bexitz00(obj_t,
		obj_t);
	static obj_t BGl_loopze70ze7zz__bexitz00(obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62unwindzd2stackzd2untilz12z70zz__bexitz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62exitdzd2popzd2protectz12z70zz__bexitz00(obj_t, obj_t);
	static obj_t BGl_z62exitdzd2pushzd2protectz12z70zz__bexitz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t bgl_exitd_mutex_profile(void);
	static obj_t BGl_z62exitdzd2protectzd2setz12z70zz__bexitz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t unwind_stack_value_p(obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_unwindzd2stackzd2valuezf3zd2envz21zz__bexitz00,
		BgL_bgl_za762unwindza7d2stac1568z00,
		BGl_z62unwindzd2stackzd2valuezf3z91zz__bexitz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_defaultzd2uncaughtzd2exceptionzd2handlerzd2envz00zz__bexitz00,
		BgL_bgl_za762defaultza7d2unc1569z00,
		BGl_z62defaultzd2uncaughtzd2exceptionzd2handlerzb0zz__bexitz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_exitdzd2protectzd2setz12zd2envzc0zz__bexitz00,
		BgL_bgl_za762exitdza7d2prote1570z00,
		BGl_z62exitdzd2protectzd2setz12z70zz__bexitz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_z42failsafezd2mutexzd2profilezd2envz90zz__bexitz00,
		BgL_bgl_za762za742failsafeza7d1571za7,
		BGl_z62z42failsafezd2mutexzd2profilez20zz__bexitz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_envzd2getzd2exitdzd2valzd2envz00zz__bexitz00,
		BgL_bgl_za762envza7d2getza7d2e1572za7,
		BGl_z62envzd2getzd2exitdzd2valzb0zz__bexitz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1561z00zz__bexitz00,
		BgL_bgl_string1561za700za7za7_1573za7, "unwind-protect", 14);
	      DEFINE_STRING(BGl_string1562z00zz__bexitz00,
		BgL_bgl_string1562za700za7za7_1574za7, "exit out of dynamic scope", 25);
	      DEFINE_STRING(BGl_string1563z00zz__bexitz00,
		BgL_bgl_string1563za700za7za7_1575za7, "/tmp/bigloo/runtime/Llib/bexit.scm",
		34);
	      DEFINE_STRING(BGl_string1564z00zz__bexitz00,
		BgL_bgl_string1564za700za7za7_1576za7, "&env-get-exitd-val", 18);
	      DEFINE_STRING(BGl_string1565z00zz__bexitz00,
		BgL_bgl_string1565za700za7za7_1577za7, "dynamic-env", 11);
	      DEFINE_STRING(BGl_string1566z00zz__bexitz00,
		BgL_bgl_string1566za700za7za7_1578za7, "&env-set-exitd-val!", 19);
	      DEFINE_STRING(BGl_string1567z00zz__bexitz00,
		BgL_bgl_string1567za700za7za7_1579za7, "__bexit", 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_unwindzd2stackzd2untilz12zd2envzc0zz__bexitz00,
		BgL_bgl_za762unwindza7d2stac1580z00,
		BGl_z62unwindzd2stackzd2untilz12z70zz__bexitz00, 0L, BUNSPEC, 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_exitdzd2pushzd2protectz12zd2envzc0zz__bexitz00,
		BgL_bgl_za762exitdza7d2pushza71581za7,
		BGl_z62exitdzd2pushzd2protectz12z70zz__bexitz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_z42exitdzd2mutexzd2profilezd2envz90zz__bexitz00,
		BgL_bgl_za762za742exitdza7d2mu1582za7,
		BGl_z62z42exitdzd2mutexzd2profilez20zz__bexitz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_exitdzd2popzd2protectz12zd2envzc0zz__bexitz00,
		BgL_bgl_za762exitdza7d2popza7d1583za7,
		BGl_z62exitdzd2popzd2protectz12z70zz__bexitz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_unwindzd2untilz12zd2envz12zz__bexitz00,
		BgL_bgl_za762unwindza7d2unti1584z00,
		BGl_z62unwindzd2untilz12za2zz__bexitz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_valzd2fromzd2exitzf3zd2envz21zz__bexitz00,
		BgL_bgl_za762valza7d2fromza7d21585za7,
		BGl_z62valzd2fromzd2exitzf3z91zz__bexitz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_envzd2setzd2exitdzd2valz12zd2envz12zz__bexitz00,
		BgL_bgl_za762envza7d2setza7d2e1586za7,
		BGl_z62envzd2setzd2exitdzd2valz12za2zz__bexitz00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__bexitz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long
		BgL_checksumz00_1736, char *BgL_fromz00_1737)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__bexitz00))
				{
					BGl_requirezd2initializa7ationz75zz__bexitz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__bexitz00();
					BGl_importedzd2moduleszd2initz00zz__bexitz00();
					return BGl_methodzd2initzd2zz__bexitz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__bexitz00(void)
	{
		{	/* Llib/bexit.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* val-from-exit? */
	BGL_EXPORTED_DEF obj_t BGl_valzd2fromzd2exitzf3zf3zz__bexitz00(obj_t
		BgL_valz00_3)
	{
		{	/* Llib/bexit.scm 144 */
			{	/* Llib/bexit.scm 145 */
				obj_t BgL_arg1154z00_1497;

				BgL_arg1154z00_1497 = BGL_EXITD_VAL();
				return BBOOL((BgL_valz00_3 == BgL_arg1154z00_1497));
			}
		}

	}



/* &val-from-exit? */
	obj_t BGl_z62valzd2fromzd2exitzf3z91zz__bexitz00(obj_t BgL_envz00_1692,
		obj_t BgL_valz00_1693)
	{
		{	/* Llib/bexit.scm 144 */
			return BGl_valzd2fromzd2exitzf3zf3zz__bexitz00(BgL_valz00_1693);
		}

	}



/* unwind-stack-value? */
	BGL_EXPORTED_DEF bool_t unwind_stack_value_p(obj_t BgL_valz00_4)
	{
		{	/* Llib/bexit.scm 155 */
			{	/* Llib/bexit.scm 145 */
				obj_t BgL_arg1154z00_1498;

				BgL_arg1154z00_1498 = BGL_EXITD_VAL();
				return (BgL_valz00_4 == BgL_arg1154z00_1498);
			}
		}

	}



/* &unwind-stack-value? */
	obj_t BGl_z62unwindzd2stackzd2valuezf3z91zz__bexitz00(obj_t BgL_envz00_1694,
		obj_t BgL_valz00_1695)
	{
		{	/* Llib/bexit.scm 155 */
			return BBOOL(unwind_stack_value_p(BgL_valz00_1695));
		}

	}



/* unwind-until! */
	BGL_EXPORTED_DEF obj_t BGl_unwindzd2untilz12zc0zz__bexitz00(obj_t
		BgL_exitdz00_5, obj_t BgL_valz00_6)
	{
		{	/* Llib/bexit.scm 165 */
			return
				BGl_loopze70ze7zz__bexitz00(BFALSE, BgL_exitdz00_5, BFALSE,
				BgL_valz00_6, BFALSE);
		}

	}



/* &unwind-until! */
	obj_t BGl_z62unwindzd2untilz12za2zz__bexitz00(obj_t BgL_envz00_1696,
		obj_t BgL_exitdz00_1697, obj_t BgL_valz00_1698)
	{
		{	/* Llib/bexit.scm 165 */
			return
				BGl_unwindzd2untilz12zc0zz__bexitz00(BgL_exitdz00_1697,
				BgL_valz00_1698);
		}

	}



/* unwind-stack-until! */
	BGL_EXPORTED_DEF obj_t unwind_stack_until(obj_t BgL_exitdz00_7,
		obj_t BgL_estampz00_8, obj_t BgL_valz00_9, obj_t BgL_proczd2bottomzd2_10,
		obj_t BgL_tracestkz00_11)
	{
		{	/* Llib/bexit.scm 176 */
			BGL_TAIL return
				BGl_loopze70ze7zz__bexitz00(BgL_proczd2bottomzd2_10, BgL_exitdz00_7,
				BgL_estampz00_8, BgL_valz00_9, BgL_tracestkz00_11);
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zz__bexitz00(obj_t BgL_proczd2bottomzd2_1726,
		obj_t BgL_exitdz00_1725, obj_t BgL_estampz00_1724, obj_t BgL_valz00_1723,
		obj_t BgL_tracestkz00_1722)
	{
		{	/* Llib/bexit.scm 177 */
		BGl_loopze70ze7zz__bexitz00:
			{	/* Llib/bexit.scm 178 */
				obj_t BgL_exitdzd2topzd2_1074;

				BgL_exitdzd2topzd2_1074 = BGL_EXITD_TOP_AS_OBJ();
				if (BGL_EXITD_BOTTOMP(BgL_exitdzd2topzd2_1074))
					{	/* Llib/bexit.scm 179 */
						BGl_exitdzd2execzd2andzd2popzd2protectsz12z12zz__bexitz00
							(BgL_exitdzd2topzd2_1074);
						if (PROCEDUREP(BgL_proczd2bottomzd2_1726))
							{	/* Llib/bexit.scm 182 */
								return
									BGL_PROCEDURE_CALL1(BgL_proczd2bottomzd2_1726,
									BgL_valz00_1723);
							}
						else
							{	/* Llib/bexit.scm 184 */
								obj_t BgL_hdlz00_1077;

								BgL_hdlz00_1077 = BGL_UNCAUGHT_EXCEPTION_HANDLER_GET();
								if (PROCEDUREP(BgL_hdlz00_1077))
									{	/* Llib/bexit.scm 185 */
										return
											BGL_PROCEDURE_CALL1(BgL_hdlz00_1077, BgL_valz00_1723);
									}
								else
									{	/* Llib/bexit.scm 185 */
										return
											BGl_errorz00zz__errorz00(BGl_string1561z00zz__bexitz00,
											BGl_string1562z00zz__bexitz00, BgL_valz00_1723);
									}
							}
					}
				else
					{	/* Llib/bexit.scm 179 */
						BGl_exitdzd2execzd2andzd2popzd2protectsz12z12zz__bexitz00
							(BgL_exitdzd2topzd2_1074);
						POP_EXIT();
						if (CBOOL(BgL_tracestkz00_1722))
							{	/* Llib/bexit.scm 193 */
								BGL_SET_TRACE_STACKSP(BgL_tracestkz00_1722);
							}
						else
							{	/* Llib/bexit.scm 195 */
								obj_t BgL_tmpz00_1779;

								BgL_tmpz00_1779 = BGL_CURRENT_DYNAMIC_ENV();
								bgl_init_trace(BgL_tmpz00_1779);
							}
						{	/* Llib/bexit.scm 198 */
							bool_t BgL_test1593z00_1782;

							if ((BgL_exitdzd2topzd2_1074 == BgL_exitdz00_1725))
								{	/* Llib/bexit.scm 199 */
									bool_t BgL__ortest_1039z00_1086;

									if (INTEGERP(BgL_estampz00_1724))
										{	/* Llib/bexit.scm 199 */
											BgL__ortest_1039z00_1086 = ((bool_t) 0);
										}
									else
										{	/* Llib/bexit.scm 199 */
											BgL__ortest_1039z00_1086 = ((bool_t) 1);
										}
									if (BgL__ortest_1039z00_1086)
										{	/* Llib/bexit.scm 199 */
											BgL_test1593z00_1782 = BgL__ortest_1039z00_1086;
										}
									else
										{	/* Llib/bexit.scm 200 */
											obj_t BgL_arg1172z00_1087;

											BgL_arg1172z00_1087 =
												EXITD_STAMP(BgL_exitdzd2topzd2_1074);
											BgL_test1593z00_1782 =
												(
												(long) CINT(BgL_arg1172z00_1087) ==
												(long) CINT(BgL_estampz00_1724));
								}}
							else
								{	/* Llib/bexit.scm 198 */
									BgL_test1593z00_1782 = ((bool_t) 0);
								}
							if (BgL_test1593z00_1782)
								{	/* Llib/bexit.scm 198 */
									if (EXITD_CALLCCP(BgL_exitdzd2topzd2_1074))
										{	/* Llib/bexit.scm 203 */
											void *BgL_arg1171z00_1085;

											BgL_arg1171z00_1085 =
												EXITD_TO_EXIT(BgL_exitdzd2topzd2_1074);
											CALLCC_JUMP_EXIT(BgL_arg1171z00_1085, BgL_valz00_1723);
										}
									else
										{	/* Llib/bexit.scm 201 */
											JUMP_EXIT(EXITD_TO_EXIT(BgL_exitdzd2topzd2_1074),
												BgL_valz00_1723);
										}
									return BUNSPEC;
								}
							else
								{

									goto BGl_loopze70ze7zz__bexitz00;
								}
						}
					}
			}
		}

	}



/* &unwind-stack-until! */
	obj_t BGl_z62unwindzd2stackzd2untilz12z70zz__bexitz00(obj_t BgL_envz00_1699,
		obj_t BgL_exitdz00_1700, obj_t BgL_estampz00_1701, obj_t BgL_valz00_1702,
		obj_t BgL_proczd2bottomzd2_1703, obj_t BgL_tracestkz00_1704)
	{
		{	/* Llib/bexit.scm 176 */
			return
				unwind_stack_until(BgL_exitdz00_1700, BgL_estampz00_1701,
				BgL_valz00_1702, BgL_proczd2bottomzd2_1703, BgL_tracestkz00_1704);
		}

	}



/* exitd-exec-and-pop-protects! */
	bool_t BGl_exitdzd2execzd2andzd2popzd2protectsz12z12zz__bexitz00(obj_t
		BgL_exitdz00_12)
	{
		{	/* Llib/bexit.scm 216 */
			{	/* Llib/bexit.scm 217 */
				obj_t BgL_g1040z00_1090;

				BgL_g1040z00_1090 = BGL_EXITD_PROTECT(BgL_exitdz00_12);
				{
					obj_t BgL_lz00_1092;

					BgL_lz00_1092 = BgL_g1040z00_1090;
				BgL_zc3z04anonymousza31173ze3z87_1093:
					if (PAIRP(BgL_lz00_1092))
						{	/* Llib/bexit.scm 219 */
							obj_t BgL_pz00_1095;

							BgL_pz00_1095 = CAR(BgL_lz00_1092);
							{	/* Llib/bexit.scm 220 */
								obj_t BgL_arg1182z00_1096;

								BgL_arg1182z00_1096 = CDR(BgL_lz00_1092);
								BGL_EXITD_PROTECT_SET(BgL_exitdz00_12, BgL_arg1182z00_1096);
								BUNSPEC;
							}
							BGl_exitdzd2execzd2protectz00zz__bexitz00(BgL_pz00_1095);
							{
								obj_t BgL_lz00_1807;

								BgL_lz00_1807 = CDR(BgL_lz00_1092);
								BgL_lz00_1092 = BgL_lz00_1807;
								goto BgL_zc3z04anonymousza31173ze3z87_1093;
							}
						}
					else
						{	/* Llib/bexit.scm 218 */
							return ((bool_t) 0);
						}
				}
			}
		}

	}



/* exitd-exec-protect */
	obj_t BGl_exitdzd2execzd2protectz00zz__bexitz00(obj_t BgL_pz00_13)
	{
		{	/* Llib/bexit.scm 227 */
			if (BGL_MUTEXP(BgL_pz00_13))
				{	/* Llib/bexit.scm 229 */
					int BgL_arg1521z00_1505;

					{	/* Llib/bexit.scm 229 */
						obj_t BgL_tmpz00_1811;

						BgL_tmpz00_1811 = ((obj_t) BgL_pz00_13);
						BgL_arg1521z00_1505 = BGL_MUTEX_UNLOCK(BgL_tmpz00_1811);
					}
					return BBOOL(((long) (BgL_arg1521z00_1505) == 0L));
				}
			else
				{	/* Llib/bexit.scm 229 */
					if (PROCEDUREP(BgL_pz00_13))
						{	/* Llib/bexit.scm 230 */
							return BGL_PROCEDURE_CALL0(BgL_pz00_13);
						}
					else
						{	/* Llib/bexit.scm 230 */
							if (PAIRP(BgL_pz00_13))
								{	/* Llib/bexit.scm 231 */
									BGL_ERROR_HANDLER_SET(BgL_pz00_13);
									return BUNSPEC;
								}
							else
								{	/* Llib/bexit.scm 231 */
									if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
										(BgL_pz00_13))
										{	/* Llib/bexit.scm 232 */
											return
												BGl_evaluate2zd2restorezd2bpz12z12zz__evaluatez00(CINT
												(BgL_pz00_13));
										}
									else
										{	/* Llib/bexit.scm 232 */
											if (VECTORP(BgL_pz00_13))
												{	/* Llib/bexit.scm 233 */
													return
														BGl_evaluate2zd2restorezd2statez12z12zz__evaluatez00
														(BgL_pz00_13);
												}
											else
												{	/* Llib/bexit.scm 233 */
													return BFALSE;
												}
										}
								}
						}
				}
		}

	}



/* exitd-protect-set! */
	BGL_EXPORTED_DEF obj_t BGl_exitdzd2protectzd2setz12z12zz__bexitz00(obj_t
		BgL_exitdz00_14, obj_t BgL_pz00_15)
	{
		{	/* Llib/bexit.scm 238 */
			BGL_EXITD_PROTECT_SET(BgL_exitdz00_14, BgL_pz00_15);
			return BUNSPEC;
		}

	}



/* &exitd-protect-set! */
	obj_t BGl_z62exitdzd2protectzd2setz12z70zz__bexitz00(obj_t BgL_envz00_1705,
		obj_t BgL_exitdz00_1706, obj_t BgL_pz00_1707)
	{
		{	/* Llib/bexit.scm 238 */
			return
				BGl_exitdzd2protectzd2setz12z12zz__bexitz00(BgL_exitdz00_1706,
				BgL_pz00_1707);
		}

	}



/* exitd-push-protect! */
	BGL_EXPORTED_DEF obj_t BGl_exitdzd2pushzd2protectz12z12zz__bexitz00(obj_t
		BgL_exitdz00_16, obj_t BgL_mz00_17)
	{
		{	/* Llib/bexit.scm 244 */
			{	/* Llib/bexit.scm 245 */
				obj_t BgL_arg1189z00_1731;

				{	/* Llib/bexit.scm 245 */
					obj_t BgL_arg1190z00_1732;

					BgL_arg1190z00_1732 = BGL_EXITD_PROTECT(BgL_exitdz00_16);
					BgL_arg1189z00_1731 =
						MAKE_YOUNG_PAIR(BgL_mz00_17, BgL_arg1190z00_1732);
				}
				BGL_EXITD_PROTECT_SET(BgL_exitdz00_16, BgL_arg1189z00_1731);
				return BUNSPEC;
			}
		}

	}



/* &exitd-push-protect! */
	obj_t BGl_z62exitdzd2pushzd2protectz12z70zz__bexitz00(obj_t BgL_envz00_1708,
		obj_t BgL_exitdz00_1709, obj_t BgL_mz00_1710)
	{
		{	/* Llib/bexit.scm 244 */
			return
				BGl_exitdzd2pushzd2protectz12z12zz__bexitz00(BgL_exitdz00_1709,
				BgL_mz00_1710);
		}

	}



/* exitd-pop-protect! */
	BGL_EXPORTED_DEF obj_t BGl_exitdzd2popzd2protectz12z12zz__bexitz00(obj_t
		BgL_exitdz00_18)
	{
		{	/* Llib/bexit.scm 250 */
			{	/* Llib/bexit.scm 251 */
				bool_t BgL_test1604z00_1838;

				{	/* Llib/bexit.scm 251 */
					obj_t BgL_arg1196z00_1733;

					BgL_arg1196z00_1733 = BGL_EXITD_PROTECT(BgL_exitdz00_18);
					BgL_test1604z00_1838 = PAIRP(BgL_arg1196z00_1733);
				}
				if (BgL_test1604z00_1838)
					{	/* Llib/bexit.scm 252 */
						obj_t BgL_arg1193z00_1734;

						{	/* Llib/bexit.scm 252 */
							obj_t BgL_arg1194z00_1735;

							BgL_arg1194z00_1735 = BGL_EXITD_PROTECT(BgL_exitdz00_18);
							BgL_arg1193z00_1734 = CDR(((obj_t) BgL_arg1194z00_1735));
						}
						BGL_EXITD_PROTECT_SET(BgL_exitdz00_18, BgL_arg1193z00_1734);
						return BUNSPEC;
					}
				else
					{	/* Llib/bexit.scm 251 */
						return BFALSE;
					}
			}
		}

	}



/* &exitd-pop-protect! */
	obj_t BGl_z62exitdzd2popzd2protectz12z70zz__bexitz00(obj_t BgL_envz00_1711,
		obj_t BgL_exitdz00_1712)
	{
		{	/* Llib/bexit.scm 250 */
			return BGl_exitdzd2popzd2protectz12z12zz__bexitz00(BgL_exitdz00_1712);
		}

	}



/* default-uncaught-exception-handler */
	BGL_EXPORTED_DEF obj_t bgl_uncaught_exception_handler(obj_t BgL_valz00_19)
	{
		{	/* Llib/bexit.scm 257 */
			return
				BGl_errorz00zz__errorz00(BGl_string1561z00zz__bexitz00,
				BGl_string1562z00zz__bexitz00, BgL_valz00_19);
		}

	}



/* &default-uncaught-exception-handler */
	obj_t BGl_z62defaultzd2uncaughtzd2exceptionzd2handlerzb0zz__bexitz00(obj_t
		BgL_envz00_1713, obj_t BgL_valz00_1714)
	{
		{	/* Llib/bexit.scm 257 */
			return bgl_uncaught_exception_handler(BgL_valz00_1714);
		}

	}



/* $exitd-mutex-profile */
	BGL_EXPORTED_DEF obj_t bgl_exitd_mutex_profile(void)
	{
		{	/* Llib/bexit.scm 268 */
			return BUNSPEC;
		}

	}



/* &$exitd-mutex-profile */
	obj_t BGl_z62z42exitdzd2mutexzd2profilez20zz__bexitz00(obj_t BgL_envz00_1715)
	{
		{	/* Llib/bexit.scm 268 */
			return bgl_exitd_mutex_profile();
		}

	}



/* $failsafe-mutex-profile */
	BGL_EXPORTED_DEF obj_t bgl_failsafe_mutex_profile(void)
	{
		{	/* Llib/bexit.scm 269 */
			return BUNSPEC;
		}

	}



/* &$failsafe-mutex-profile */
	obj_t BGl_z62z42failsafezd2mutexzd2profilez20zz__bexitz00(obj_t
		BgL_envz00_1716)
	{
		{	/* Llib/bexit.scm 269 */
			return bgl_failsafe_mutex_profile();
		}

	}



/* env-get-exitd-val */
	BGL_EXPORTED_DEF obj_t BGl_envzd2getzd2exitdzd2valzd2zz__bexitz00(obj_t
		BgL_envz00_20)
	{
		{	/* Llib/bexit.scm 276 */
			return BGL_ENV_EXITD_VAL(BgL_envz00_20);
		}

	}



/* &env-get-exitd-val */
	obj_t BGl_z62envzd2getzd2exitdzd2valzb0zz__bexitz00(obj_t BgL_envz00_1717,
		obj_t BgL_envz00_1718)
	{
		{	/* Llib/bexit.scm 276 */
			{	/* Llib/bexit.scm 276 */
				obj_t BgL_auxz00_1851;

				if (BGL_DYNAMIC_ENVP(BgL_envz00_1718))
					{	/* Llib/bexit.scm 276 */
						BgL_auxz00_1851 = BgL_envz00_1718;
					}
				else
					{
						obj_t BgL_auxz00_1854;

						BgL_auxz00_1854 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1563z00zz__bexitz00,
							BINT(12088L), BGl_string1564z00zz__bexitz00,
							BGl_string1565z00zz__bexitz00, BgL_envz00_1718);
						FAILURE(BgL_auxz00_1854, BFALSE, BFALSE);
					}
				return BGl_envzd2getzd2exitdzd2valzd2zz__bexitz00(BgL_auxz00_1851);
			}
		}

	}



/* env-set-exitd-val! */
	BGL_EXPORTED_DEF obj_t BGl_envzd2setzd2exitdzd2valz12zc0zz__bexitz00(obj_t
		BgL_envz00_21, obj_t BgL_objz00_22)
	{
		{	/* Llib/bexit.scm 277 */
			return BGL_ENV_EXITD_VAL_SET(BgL_envz00_21, BgL_objz00_22);
		}

	}



/* &env-set-exitd-val! */
	obj_t BGl_z62envzd2setzd2exitdzd2valz12za2zz__bexitz00(obj_t BgL_envz00_1719,
		obj_t BgL_envz00_1720, obj_t BgL_objz00_1721)
	{
		{	/* Llib/bexit.scm 277 */
			{	/* Llib/bexit.scm 277 */
				obj_t BgL_auxz00_1860;

				if (BGL_DYNAMIC_ENVP(BgL_envz00_1720))
					{	/* Llib/bexit.scm 277 */
						BgL_auxz00_1860 = BgL_envz00_1720;
					}
				else
					{
						obj_t BgL_auxz00_1863;

						BgL_auxz00_1863 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1563z00zz__bexitz00,
							BINT(12162L), BGl_string1566z00zz__bexitz00,
							BGl_string1565z00zz__bexitz00, BgL_envz00_1720);
						FAILURE(BgL_auxz00_1863, BFALSE, BFALSE);
					}
				return
					BGl_envzd2setzd2exitdzd2valz12zc0zz__bexitz00(BgL_auxz00_1860,
					BgL_objz00_1721);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__bexitz00(void)
	{
		{	/* Llib/bexit.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__bexitz00(void)
	{
		{	/* Llib/bexit.scm 14 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__bexitz00(void)
	{
		{	/* Llib/bexit.scm 14 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__bexitz00(void)
	{
		{	/* Llib/bexit.scm 14 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1567z00zz__bexitz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string1567z00zz__bexitz00));
			return
				BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string1567z00zz__bexitz00));
		}

	}

#ifdef __cplusplus
}
#endif
