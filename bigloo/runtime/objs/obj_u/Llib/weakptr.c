/*===========================================================================*/
/*   (Llib/weakptr.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Llib/weakptr.scm -indent -o objs/obj_u/Llib/weakptr.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___WEAKPTR_TYPE_DEFINITIONS
#define BGL___WEAKPTR_TYPE_DEFINITIONS
#endif													// BGL___WEAKPTR_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62weakptrzd2datazb0zz__weakptrz00(obj_t, obj_t);
	static obj_t BGl__makezd2weakptrzd2zz__weakptrz00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__weakptrz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_weakptrzd2datazd2zz__weakptrz00(obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__weakptrz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__hashz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_EXPORTED_DECL bool_t BGl_weakptrzf3zf3zz__weakptrz00(obj_t);
	extern void bgl_weakptr_ref_set(obj_t, obj_t);
	static obj_t BGl_z62weakptrzd2refzd2setz12z70zz__weakptrz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_makezd2weakptrzd2zz__weakptrz00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zz__weakptrz00(void);
	extern void bgl_weakptr_data_set(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_weakptrzd2refzd2zz__weakptrz00(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__weakptrz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__weakptrz00(void);
	static obj_t BGl_objectzd2initzd2zz__weakptrz00(void);
	static obj_t BGl_z62weakptrzd2datazd2setz12z70zz__weakptrz00(obj_t, obj_t,
		obj_t);
	extern obj_t bgl_weakptr_data(obj_t);
	extern obj_t bgl_weakptr_ref(obj_t);
	static obj_t BGl_z62weakptrzf3z91zz__weakptrz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zz__weakptrz00(void);
	extern obj_t bgl_make_weakptr(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_weakptrzd2datazd2setz12z12zz__weakptrz00(obj_t,
		obj_t);
	static obj_t BGl_z62weakptrzd2refzb0zz__weakptrz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_weakptrzd2refzd2setz12z12zz__weakptrz00(obj_t,
		obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_weakptrzd2datazd2envz00zz__weakptrz00,
		BgL_bgl_za762weakptrza7d2dat1578z00, BGl_z62weakptrzd2datazb0zz__weakptrz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_weakptrzd2refzd2envz00zz__weakptrz00,
		BgL_bgl_za762weakptrza7d2ref1579z00, BGl_z62weakptrzd2refzb0zz__weakptrz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_weakptrzd2refzd2setz12zd2envzc0zz__weakptrz00,
		BgL_bgl_za762weakptrza7d2ref1580z00,
		BGl_z62weakptrzd2refzd2setz12z70zz__weakptrz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_weakptrzf3zd2envz21zz__weakptrz00,
		BgL_bgl_za762weakptrza7f3za7911581za7, BGl_z62weakptrzf3z91zz__weakptrz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_weakptrzd2datazd2setz12zd2envzc0zz__weakptrz00,
		BgL_bgl_za762weakptrza7d2dat1582z00,
		BGl_z62weakptrzd2datazd2setz12z70zz__weakptrz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2weakptrzd2envz00zz__weakptrz00,
		BgL_bgl__makeza7d2weakptrza71583z00, opt_generic_entry,
		BGl__makezd2weakptrzd2zz__weakptrz00, BFALSE, -1);
	      DEFINE_STRING(BGl_string1571z00zz__weakptrz00,
		BgL_bgl_string1571za700za7za7_1584za7,
		"/tmp/bigloo/runtime/Llib/weakptr.scm", 36);
	      DEFINE_STRING(BGl_string1572z00zz__weakptrz00,
		BgL_bgl_string1572za700za7za7_1585za7, "&weakptr-data", 13);
	      DEFINE_STRING(BGl_string1573z00zz__weakptrz00,
		BgL_bgl_string1573za700za7za7_1586za7, "weakptr", 7);
	      DEFINE_STRING(BGl_string1574z00zz__weakptrz00,
		BgL_bgl_string1574za700za7za7_1587za7, "&weakptr-data-set!", 18);
	      DEFINE_STRING(BGl_string1575z00zz__weakptrz00,
		BgL_bgl_string1575za700za7za7_1588za7, "&weakptr-ref", 12);
	      DEFINE_STRING(BGl_string1576z00zz__weakptrz00,
		BgL_bgl_string1576za700za7za7_1589za7, "&weakptr-ref-set!", 17);
	      DEFINE_STRING(BGl_string1577z00zz__weakptrz00,
		BgL_bgl_string1577za700za7za7_1590za7, "__weakptr", 9);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__weakptrz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__weakptrz00(long
		BgL_checksumz00_1718, char *BgL_fromz00_1719)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__weakptrz00))
				{
					BGl_requirezd2initializa7ationz75zz__weakptrz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__weakptrz00();
					BGl_importedzd2moduleszd2initz00zz__weakptrz00();
					return BGl_methodzd2initzd2zz__weakptrz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__weakptrz00(void)
	{
		{	/* Llib/weakptr.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* weakptr? */
	BGL_EXPORTED_DEF bool_t BGl_weakptrzf3zf3zz__weakptrz00(obj_t BgL_objz00_3)
	{
		{	/* Llib/weakptr.scm 84 */
			return BGL_WEAKPTRP(BgL_objz00_3);
		}

	}



/* &weakptr? */
	obj_t BGl_z62weakptrzf3z91zz__weakptrz00(obj_t BgL_envz00_1696,
		obj_t BgL_objz00_1697)
	{
		{	/* Llib/weakptr.scm 84 */
			return BBOOL(BGl_weakptrzf3zf3zz__weakptrz00(BgL_objz00_1697));
		}

	}



/* weakptr-data */
	BGL_EXPORTED_DEF obj_t BGl_weakptrzd2datazd2zz__weakptrz00(obj_t BgL_objz00_4)
	{
		{	/* Llib/weakptr.scm 90 */
			BGL_TAIL return bgl_weakptr_data(BgL_objz00_4);
		}

	}



/* &weakptr-data */
	obj_t BGl_z62weakptrzd2datazb0zz__weakptrz00(obj_t BgL_envz00_1698,
		obj_t BgL_objz00_1699)
	{
		{	/* Llib/weakptr.scm 90 */
			{	/* Llib/weakptr.scm 91 */
				obj_t BgL_auxz00_1731;

				if (BGL_WEAKPTRP(BgL_objz00_1699))
					{	/* Llib/weakptr.scm 91 */
						BgL_auxz00_1731 = BgL_objz00_1699;
					}
				else
					{
						obj_t BgL_auxz00_1734;

						BgL_auxz00_1734 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1571z00zz__weakptrz00,
							BINT(3417L), BGl_string1572z00zz__weakptrz00,
							BGl_string1573z00zz__weakptrz00, BgL_objz00_1699);
						FAILURE(BgL_auxz00_1734, BFALSE, BFALSE);
					}
				return BGl_weakptrzd2datazd2zz__weakptrz00(BgL_auxz00_1731);
			}
		}

	}



/* weakptr-data-set! */
	BGL_EXPORTED_DEF obj_t BGl_weakptrzd2datazd2setz12z12zz__weakptrz00(obj_t
		BgL_ptrz00_5, obj_t BgL_objz00_6)
	{
		{	/* Llib/weakptr.scm 96 */
			bgl_weakptr_data_set(BgL_ptrz00_5, BgL_objz00_6);
			BUNSPEC;
			return BUNSPEC;
		}

	}



/* &weakptr-data-set! */
	obj_t BGl_z62weakptrzd2datazd2setz12z70zz__weakptrz00(obj_t BgL_envz00_1700,
		obj_t BgL_ptrz00_1701, obj_t BgL_objz00_1702)
	{
		{	/* Llib/weakptr.scm 96 */
			{	/* Llib/weakptr.scm 97 */
				obj_t BgL_auxz00_1740;

				if (BGL_WEAKPTRP(BgL_ptrz00_1701))
					{	/* Llib/weakptr.scm 97 */
						BgL_auxz00_1740 = BgL_ptrz00_1701;
					}
				else
					{
						obj_t BgL_auxz00_1743;

						BgL_auxz00_1743 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1571z00zz__weakptrz00,
							BINT(3734L), BGl_string1574z00zz__weakptrz00,
							BGl_string1573z00zz__weakptrz00, BgL_ptrz00_1701);
						FAILURE(BgL_auxz00_1743, BFALSE, BFALSE);
					}
				return
					BGl_weakptrzd2datazd2setz12z12zz__weakptrz00(BgL_auxz00_1740,
					BgL_objz00_1702);
			}
		}

	}



/* weakptr-ref */
	BGL_EXPORTED_DEF obj_t BGl_weakptrzd2refzd2zz__weakptrz00(obj_t BgL_objz00_7)
	{
		{	/* Llib/weakptr.scm 103 */
			BGL_TAIL return bgl_weakptr_ref(BgL_objz00_7);
		}

	}



/* &weakptr-ref */
	obj_t BGl_z62weakptrzd2refzb0zz__weakptrz00(obj_t BgL_envz00_1703,
		obj_t BgL_objz00_1704)
	{
		{	/* Llib/weakptr.scm 103 */
			{	/* Llib/weakptr.scm 104 */
				obj_t BgL_auxz00_1749;

				if (BGL_WEAKPTRP(BgL_objz00_1704))
					{	/* Llib/weakptr.scm 104 */
						BgL_auxz00_1749 = BgL_objz00_1704;
					}
				else
					{
						obj_t BgL_auxz00_1752;

						BgL_auxz00_1752 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1571z00zz__weakptrz00,
							BINT(4012L), BGl_string1575z00zz__weakptrz00,
							BGl_string1573z00zz__weakptrz00, BgL_objz00_1704);
						FAILURE(BgL_auxz00_1752, BFALSE, BFALSE);
					}
				return BGl_weakptrzd2refzd2zz__weakptrz00(BgL_auxz00_1749);
			}
		}

	}



/* weakptr-ref-set! */
	BGL_EXPORTED_DEF obj_t BGl_weakptrzd2refzd2setz12z12zz__weakptrz00(obj_t
		BgL_ptrz00_8, obj_t BgL_objz00_9)
	{
		{	/* Llib/weakptr.scm 109 */
			bgl_weakptr_ref_set(BgL_ptrz00_8, BgL_objz00_9);
			BUNSPEC;
			return BUNSPEC;
		}

	}



/* &weakptr-ref-set! */
	obj_t BGl_z62weakptrzd2refzd2setz12z70zz__weakptrz00(obj_t BgL_envz00_1705,
		obj_t BgL_ptrz00_1706, obj_t BgL_objz00_1707)
	{
		{	/* Llib/weakptr.scm 109 */
			{	/* Llib/weakptr.scm 110 */
				obj_t BgL_auxz00_1758;

				if (BGL_WEAKPTRP(BgL_ptrz00_1706))
					{	/* Llib/weakptr.scm 110 */
						BgL_auxz00_1758 = BgL_ptrz00_1706;
					}
				else
					{
						obj_t BgL_auxz00_1761;

						BgL_auxz00_1761 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1571z00zz__weakptrz00,
							BINT(4326L), BGl_string1576z00zz__weakptrz00,
							BGl_string1573z00zz__weakptrz00, BgL_ptrz00_1706);
						FAILURE(BgL_auxz00_1761, BFALSE, BFALSE);
					}
				return
					BGl_weakptrzd2refzd2setz12z12zz__weakptrz00(BgL_auxz00_1758,
					BgL_objz00_1707);
			}
		}

	}



/* _make-weakptr */
	obj_t BGl__makezd2weakptrzd2zz__weakptrz00(obj_t BgL_env1086z00_13,
		obj_t BgL_opt1085z00_12)
	{
		{	/* Llib/weakptr.scm 116 */
			{	/* Llib/weakptr.scm 116 */
				obj_t BgL_g1087z00_1716;

				BgL_g1087z00_1716 = VECTOR_REF(BgL_opt1085z00_12, 0L);
				switch (VECTOR_LENGTH(BgL_opt1085z00_12))
					{
					case 1L:

						{	/* Llib/weakptr.scm 116 */

							return bgl_make_weakptr(BgL_g1087z00_1716, BFALSE);
						}
						break;
					case 2L:

						{	/* Llib/weakptr.scm 116 */
							obj_t BgL_refz00_1717;

							BgL_refz00_1717 = VECTOR_REF(BgL_opt1085z00_12, 1L);
							{	/* Llib/weakptr.scm 116 */

								return bgl_make_weakptr(BgL_g1087z00_1716, BgL_refz00_1717);
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* make-weakptr */
	BGL_EXPORTED_DEF obj_t BGl_makezd2weakptrzd2zz__weakptrz00(obj_t
		BgL_objz00_10, obj_t BgL_refz00_11)
	{
		{	/* Llib/weakptr.scm 116 */
			BGL_TAIL return bgl_make_weakptr(BgL_objz00_10, BgL_refz00_11);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__weakptrz00(void)
	{
		{	/* Llib/weakptr.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__weakptrz00(void)
	{
		{	/* Llib/weakptr.scm 14 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__weakptrz00(void)
	{
		{	/* Llib/weakptr.scm 14 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__weakptrz00(void)
	{
		{	/* Llib/weakptr.scm 14 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1577z00zz__weakptrz00));
			BGl_modulezd2initializa7ationz75zz__hashz00(482391669L,
				BSTRING_TO_STRING(BGl_string1577z00zz__weakptrz00));
			BGl_modulezd2initializa7ationz75zz__readerz00(220648206L,
				BSTRING_TO_STRING(BGl_string1577z00zz__weakptrz00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1577z00zz__weakptrz00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string1577z00zz__weakptrz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string1577z00zz__weakptrz00));
			return
				BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string1577z00zz__weakptrz00));
		}

	}

#ifdef __cplusplus
}
#endif
