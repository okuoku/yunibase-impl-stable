/*===========================================================================*/
/*   (Llib/param.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Llib/param.scm -indent -o objs/obj_u/Llib/param.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___PARAM_TYPE_DEFINITIONS
#define BGL___PARAM_TYPE_DEFINITIONS
#endif													// BGL___PARAM_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t
		BGl_z62bigloozd2dnszd2cachezd2validityzd2timeoutz62zz__paramz00(obj_t);
	static obj_t BGl_z62bigloozd2loadzd2modulezd2setz12za2zz__paramz00(obj_t,
		obj_t);
	static obj_t BGl_z62bigloozd2profilezb0zz__paramz00(obj_t);
	extern bool_t BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_z62bigloozd2debugzd2modulezd2setz12za2zz__paramz00(obj_t,
		obj_t);
	static obj_t BGl_symbol1672z00zz__paramz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_bigloozd2tracezd2setz12z12zz__paramz00(obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_bigloozd2strictzd2r5rszd2stringszd2zz__paramz00(void);
	static obj_t BGl_za2parameterzd2mutexza2zd2zz__paramz00 = BUNSPEC;
	static obj_t BGl_z62bigloozd2profilezd2setz12z70zz__paramz00(obj_t, obj_t);
	static bool_t BGl_za2bigloozd2evalzd2strictzd2moduleza2zd2zz__paramz00;
	BGL_EXPORTED_DECL int BGl_bigloozd2warningzd2zz__paramz00(void);
	BGL_EXPORTED_DECL obj_t BGl_bigloozd2loadzd2readerz00zz__paramz00(void);
	static obj_t BGl_z62bigloozd2debugzd2setz12z70zz__paramz00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__paramz00 = BUNSPEC;
	static obj_t BGl_za2bigloozd2debugzd2moduleza2z00zz__paramz00 = BUNSPEC;
	static obj_t
		BGl_z62bigloozd2tracezd2stackzd2depthzd2setz12z70zz__paramz00(obj_t, obj_t);
	static obj_t BGl_z62bigloozd2tracezd2stackzd2depthzb0zz__paramz00(obj_t);
	static obj_t BGl_z62bigloozd2debugzd2modulez62zz__paramz00(obj_t);
	static obj_t BGl_list1663z00zz__paramz00 = BUNSPEC;
	static obj_t BGl_za2bigloozd2profileza2zd2zz__paramz00 = BUNSPEC;
	static obj_t BGl_z62bigloozd2evalzd2strictzd2modulezb0zz__paramz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bigloozd2dnszd2cachezd2validityzd2timeoutzd2setz12zc0zz__paramz00(long);
	BGL_EXPORTED_DECL obj_t
		BGl_bigloozd2compilerzd2debugzd2setz12zc0zz__paramz00(int);
	static obj_t BGl_za2bigloozd2warningza2zd2zz__paramz00 = BUNSPEC;
	static obj_t BGl_z62bigloozd2debugzb0zz__paramz00(obj_t);
	static obj_t BGl_z62bigloozd2strictzd2r5rszd2stringszb0zz__paramz00(obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__configurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_EXPORTED_DECL obj_t BGl_bigloozd2loadzd2modulez00zz__paramz00(void);
	static obj_t
		BGl_z62bigloozd2dnszd2enablezd2cachezd2setz12z70zz__paramz00(obj_t, obj_t);
	static obj_t BGl_za2bigloozd2debugza2zd2zz__paramz00 = BUNSPEC;
	static obj_t
		BGl_z62bigloozd2evalzd2strictzd2modulezd2setz12z70zz__paramz00(obj_t,
		obj_t);
	static obj_t BGl_z62bigloozd2loadzd2readerz62zz__paramz00(obj_t);
	BGL_EXPORTED_DECL long bgl_dns_cache_validity_timeout(void);
	static obj_t BGl_toplevelzd2initzd2zz__paramz00(void);
	BGL_EXPORTED_DECL int bgl_debug(void);
	static bool_t BGl_za2bigloozd2dnszd2enablezd2cacheza2zd2zz__paramz00;
	static obj_t BGl_z62bigloozd2compilerzd2debugzd2setz12za2zz__paramz00(obj_t,
		obj_t);
	static obj_t BGl_za2bigloozd2loadzd2readerza2z00zz__paramz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_bigloozd2dnszd2enablezd2cachezd2setz12z12zz__paramz00(bool_t);
	static obj_t BGl_z62bigloozd2warningzd2setz12z70zz__paramz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_bigloozd2initializa7edz12z67zz__paramz00(void);
	static obj_t BGl_z62bigloozd2loadzd2modulez62zz__paramz00(obj_t);
	BGL_EXPORTED_DECL bool_t bgl_dns_enable_cache(void);
	BGL_EXPORTED_DECL obj_t
		BGl_bigloozd2casezd2sensitivezd2setz12zc0zz__paramz00(obj_t);
	static obj_t BGl_z62bigloozd2dnszd2enablezd2cachezb0zz__paramz00(obj_t);
	extern obj_t BGl_getenvz00zz__osz00(obj_t);
	BGL_EXPORTED_DECL obj_t bgl_debug_set(int);
	static obj_t BGl_cnstzd2initzd2zz__paramz00(void);
	BGL_EXPORTED_DECL int BGl_bigloozd2profilezd2zz__paramz00(void);
	extern bool_t BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_z62bigloozd2tracezb0zz__paramz00(obj_t);
	static obj_t BGl_genericzd2initzd2zz__paramz00(void);
	BGL_EXPORTED_DECL bool_t BGl_bigloozd2initializa7edzf3z86zz__paramz00(void);
	BGL_EXPORTED_DECL bool_t
		BGl_bigloozd2evalzd2strictzd2modulezd2zz__paramz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zz__paramz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__paramz00(void);
	static obj_t BGl_objectzd2initzd2zz__paramz00(void);
	static obj_t BGl_z62bigloozd2libraryzd2pathz62zz__paramz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bigloozd2modulezd2extensionzd2handlerzd2zz__paramz00(void);
	static obj_t
		BGl_z62bigloozd2modulezd2extensionzd2handlerzb0zz__paramz00(obj_t);
	static obj_t BGl_z62bigloozd2casezd2sensitivezd2setz12za2zz__paramz00(obj_t,
		obj_t);
	extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bigloozd2libraryzd2pathzd2setz12zc0zz__paramz00(obj_t);
	static obj_t BGl_za2bigloozd2modulezd2extensionzd2handlerza2zd2zz__paramz00 =
		BUNSPEC;
	static bool_t BGl_za2bigloozd2tracezd2colorza2z00zz__paramz00;
	BGL_EXPORTED_DECL obj_t BGl_bigloozd2tracezd2zz__paramz00(void);
	extern obj_t BGl_bigloozd2configzd2zz__configurez00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_bigloozd2casezd2sensitivez00zz__paramz00(void);
	extern obj_t BGl_stringzd2splitzd2zz__r4_strings_6_7z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_bigloozd2profilezd2setz12z12zz__paramz00(int);
	BGL_EXPORTED_DECL obj_t
		BGl_bigloozd2evalzd2strictzd2modulezd2setz12z12zz__paramz00(bool_t);
	BGL_EXPORTED_DECL bool_t BGl_bigloozd2tracezd2colorz00zz__paramz00(void);
	static obj_t BGl_za2bigloozd2loadzd2moduleza2z00zz__paramz00 = BUNSPEC;
	BGL_EXPORTED_DECL int BGl_bigloozd2compilerzd2debugz00zz__paramz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_bigloozd2debugzd2modulezd2setz12zc0zz__paramz00(int);
	extern obj_t bstring_to_symbol(obj_t);
	BGL_EXPORTED_DECL int BGl_bigloozd2debugzd2modulez00zz__paramz00(void);
	static obj_t
		BGl_z62bigloozd2modulezd2extensionzd2handlerzd2setz12z70zz__paramz00(obj_t,
		obj_t);
	static long BGl_za2bigloozd2dnszd2cachezd2validityzd2timeoutza2z00zz__paramz00
		= 0L;
	static obj_t BGl_za2bigloozd2compilerzd2debugza2z00zz__paramz00 = BUNSPEC;
	static obj_t BGl_methodzd2initzd2zz__paramz00(void);
	static bool_t BGl_za2bigloozd2initializa7edpza2z75zz__paramz00;
	static obj_t BGl_z62bigloozd2warningzb0zz__paramz00(obj_t);
	static obj_t BGl_z62bigloozd2tracezd2colorz62zz__paramz00(obj_t);
	static obj_t BGl_za2bigloozd2libraryzd2pathza2z00zz__paramz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_bigloozd2strictzd2r5rszd2stringszd2setz12z12zz__paramz00(bool_t);
	static obj_t
		BGl_z62bigloozd2strictzd2r5rszd2stringszd2setz12z70zz__paramz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL int BGl_bigloozd2tracezd2stackzd2depthzd2zz__paramz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_bigloozd2modulezd2extensionzd2handlerzd2setz12z12zz__paramz00(obj_t);
	static obj_t BGl_symbol1634z00zz__paramz00 = BUNSPEC;
	static obj_t BGl_symbol1636z00zz__paramz00 = BUNSPEC;
	static obj_t BGl_symbol1639z00zz__paramz00 = BUNSPEC;
	static obj_t BGl_z62bigloozd2casezd2sensitivez62zz__paramz00(obj_t);
	static obj_t
		BGl_z62bigloozd2dnszd2cachezd2validityzd2timeoutzd2setz12za2zz__paramz00
		(obj_t, obj_t);
	static obj_t BGl_z62bigloozd2tracezd2setz12z70zz__paramz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_bigloozd2warningzd2setz12z12zz__paramz00(int);
	static obj_t BGl_za2bigloozd2casezd2sensitiveza2z00zz__paramz00 = BUNSPEC;
	static obj_t BGl_symbol1645z00zz__paramz00 = BUNSPEC;
	static obj_t BGl_z62bigloozd2initializa7edz12z05zz__paramz00(obj_t);
	static bool_t BGl_za2bigloozd2strictzd2r5rszd2stringsza2zd2zz__paramz00;
	static obj_t BGl_symbol1648z00zz__paramz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_bigloozd2tracezd2stackzd2depthzd2setz12z12zz__paramz00(int);
	static obj_t BGl_z62bigloozd2libraryzd2pathzd2setz12za2zz__paramz00(obj_t,
		obj_t);
	static obj_t BGl_z62bigloozd2compilerzd2debugz62zz__paramz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bigloozd2tracezd2colorzd2setz12zc0zz__paramz00(bool_t);
	static obj_t BGl_symbol1652z00zz__paramz00 = BUNSPEC;
	static obj_t BGl_z62bigloozd2initializa7edzf3ze4zz__paramz00(obj_t);
	static obj_t BGl_z62bigloozd2tracezd2colorzd2setz12za2zz__paramz00(obj_t,
		obj_t);
	static obj_t BGl_symbol1656z00zz__paramz00 = BUNSPEC;
	static obj_t BGl_za2bigloozd2traceza2zd2zz__paramz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_bigloozd2libraryzd2pathz00zz__paramz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_bigloozd2loadzd2readerzd2setz12zc0zz__paramz00(obj_t);
	static obj_t BGl_symbol1664z00zz__paramz00 = BUNSPEC;
	static obj_t BGl_symbol1666z00zz__paramz00 = BUNSPEC;
	static obj_t BGl_z62bigloozd2loadzd2readerzd2setz12za2zz__paramz00(obj_t,
		obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static long BGl_za2bigloozd2tracezd2stackzd2depthza2zd2zz__paramz00 = 0L;
	BGL_EXPORTED_DECL obj_t
		BGl_bigloozd2loadzd2modulezd2setz12zc0zz__paramz00(obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2dnszd2cachezd2validityzd2timeoutzd2setz12zd2envz12zz__paramz00,
		BgL_bgl_za762biglooza7d2dnsza71679za7,
		BGl_z62bigloozd2dnszd2cachezd2validityzd2timeoutzd2setz12za2zz__paramz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_bigloozd2loadzd2modulezd2envzd2zz__paramz00,
		BgL_bgl_za762biglooza7d2load1680z00,
		BGl_z62bigloozd2loadzd2modulez62zz__paramz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2modulezd2extensionzd2handlerzd2setz12zd2envzc0zz__paramz00,
		BgL_bgl_za762biglooza7d2modu1681z00,
		BGl_z62bigloozd2modulezd2extensionzd2handlerzd2setz12z70zz__paramz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bigloozd2debugzd2envz00zz__paramz00,
		BgL_bgl_za762biglooza7d2debu1682z00, BGl_z62bigloozd2debugzb0zz__paramz00,
		0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2initializa7edz12zd2envzb5zz__paramz00,
		BgL_bgl_za762biglooza7d2init1683z00,
		BGl_z62bigloozd2initializa7edz12z05zz__paramz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_bigloozd2loadzd2readerzd2envzd2zz__paramz00,
		BgL_bgl_za762biglooza7d2load1684z00,
		BGl_z62bigloozd2loadzd2readerz62zz__paramz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2tracezd2colorzd2setz12zd2envz12zz__paramz00,
		BgL_bgl_za762biglooza7d2trac1685z00,
		BGl_z62bigloozd2tracezd2colorzd2setz12za2zz__paramz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2debugzd2modulezd2envzd2zz__paramz00,
		BgL_bgl_za762biglooza7d2debu1686z00,
		BGl_z62bigloozd2debugzd2modulez62zz__paramz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2modulezd2extensionzd2handlerzd2envz00zz__paramz00,
		BgL_bgl_za762biglooza7d2modu1687z00,
		BGl_z62bigloozd2modulezd2extensionzd2handlerzb0zz__paramz00, 0L, BUNSPEC,
		0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2loadzd2modulezd2setz12zd2envz12zz__paramz00,
		BgL_bgl_za762biglooza7d2load1688z00,
		BGl_z62bigloozd2loadzd2modulezd2setz12za2zz__paramz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bigloozd2tracezd2envz00zz__paramz00,
		BgL_bgl_za762biglooza7d2trac1689z00, BGl_z62bigloozd2tracezb0zz__paramz00,
		0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2casezd2sensitivezd2envzd2zz__paramz00,
		BgL_bgl_za762biglooza7d2case1690z00,
		BGl_z62bigloozd2casezd2sensitivez62zz__paramz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2tracezd2setz12zd2envzc0zz__paramz00,
		BgL_bgl_za762biglooza7d2trac1691z00,
		BGl_z62bigloozd2tracezd2setz12z70zz__paramz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2strictzd2r5rszd2stringszd2envz00zz__paramz00,
		BgL_bgl_za762biglooza7d2stri1692z00,
		BGl_z62bigloozd2strictzd2r5rszd2stringszb0zz__paramz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2profilezd2setz12zd2envzc0zz__paramz00,
		BgL_bgl_za762biglooza7d2prof1693z00,
		BGl_z62bigloozd2profilezd2setz12z70zz__paramz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2dnszd2enablezd2cachezd2setz12zd2envzc0zz__paramz00,
		BgL_bgl_za762biglooza7d2dnsza71694za7,
		BGl_z62bigloozd2dnszd2enablezd2cachezd2setz12z70zz__paramz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2evalzd2strictzd2modulezd2setz12zd2envzc0zz__paramz00,
		BgL_bgl_za762biglooza7d2eval1695z00,
		BGl_z62bigloozd2evalzd2strictzd2modulezd2setz12z70zz__paramz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_bigloozd2tracezd2colorzd2envzd2zz__paramz00,
		BgL_bgl_za762biglooza7d2trac1696z00,
		BGl_z62bigloozd2tracezd2colorz62zz__paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1631z00zz__paramz00,
		BgL_bgl_string1631za700za7za7_1697za7, "param", 5);
	      DEFINE_STRING(BGl_string1632z00zz__paramz00,
		BgL_bgl_string1632za700za7za7_1698za7, "BIGLOOTRACE", 11);
	      DEFINE_STRING(BGl_string1633z00zz__paramz00,
		BgL_bgl_string1633za700za7za7_1699za7, "BIGLOOSTACKDEPTH", 16);
	      DEFINE_STRING(BGl_string1635z00zz__paramz00,
		BgL_bgl_string1635za700za7za7_1700za7, "sensitive", 9);
	      DEFINE_STRING(BGl_string1637z00zz__paramz00,
		BgL_bgl_string1637za700za7za7_1701za7, "library-directory", 17);
	      DEFINE_STRING(BGl_string1638z00zz__paramz00,
		BgL_bgl_string1638za700za7za7_1702za7, ".", 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2tracezd2stackzd2depthzd2setz12zd2envzc0zz__paramz00,
		BgL_bgl_za762biglooza7d2trac1703z00,
		BGl_z62bigloozd2tracezd2stackzd2depthzd2setz12z70zz__paramz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2compilerzd2debugzd2envzd2zz__paramz00,
		BgL_bgl_za762biglooza7d2comp1704z00,
		BGl_z62bigloozd2compilerzd2debugz62zz__paramz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1640z00zz__paramz00,
		BgL_bgl_string1640za700za7za7_1705za7, "bigloo-compiler-debug-set!", 26);
	      DEFINE_STRING(BGl_string1641z00zz__paramz00,
		BgL_bgl_string1641za700za7za7_1706za7, "Illegal debug level", 19);
	      DEFINE_STRING(BGl_string1642z00zz__paramz00,
		BgL_bgl_string1642za700za7za7_1707za7, "/tmp/bigloo/runtime/Llib/param.scm",
		34);
	      DEFINE_STRING(BGl_string1643z00zz__paramz00,
		BgL_bgl_string1643za700za7za7_1708za7, "&bigloo-compiler-debug-set!", 27);
	      DEFINE_STRING(BGl_string1644z00zz__paramz00,
		BgL_bgl_string1644za700za7za7_1709za7, "bint", 4);
	      DEFINE_STRING(BGl_string1646z00zz__paramz00,
		BgL_bgl_string1646za700za7za7_1710za7, "bigloo-debug-set!", 17);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2compilerzd2debugzd2setz12zd2envz12zz__paramz00,
		BgL_bgl_za762biglooza7d2comp1711z00,
		BGl_z62bigloozd2compilerzd2debugzd2setz12za2zz__paramz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1647z00zz__paramz00,
		BgL_bgl_string1647za700za7za7_1712za7, "&bigloo-debug-set!", 18);
	      DEFINE_STRING(BGl_string1649z00zz__paramz00,
		BgL_bgl_string1649za700za7za7_1713za7, "bigloo-debug-module-set!", 24);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2loadzd2readerzd2setz12zd2envz12zz__paramz00,
		BgL_bgl_za762biglooza7d2load1714z00,
		BGl_z62bigloozd2loadzd2readerzd2setz12za2zz__paramz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2strictzd2r5rszd2stringszd2setz12zd2envzc0zz__paramz00,
		BgL_bgl_za762biglooza7d2stri1715z00,
		BGl_z62bigloozd2strictzd2r5rszd2stringszd2setz12z70zz__paramz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1650z00zz__paramz00,
		BgL_bgl_string1650za700za7za7_1716za7, "Illegal debug module level", 26);
	      DEFINE_STRING(BGl_string1651z00zz__paramz00,
		BgL_bgl_string1651za700za7za7_1717za7, "&bigloo-debug-module-set!", 25);
	      DEFINE_STRING(BGl_string1653z00zz__paramz00,
		BgL_bgl_string1653za700za7za7_1718za7, "bigloo-warning-set!", 19);
	      DEFINE_STRING(BGl_string1654z00zz__paramz00,
		BgL_bgl_string1654za700za7za7_1719za7, "Illegal warning level", 21);
	      DEFINE_STRING(BGl_string1655z00zz__paramz00,
		BgL_bgl_string1655za700za7za7_1720za7, "&bigloo-warning-set!", 20);
	      DEFINE_STRING(BGl_string1657z00zz__paramz00,
		BgL_bgl_string1657za700za7za7_1721za7, "bigloo-profile-set!", 19);
	      DEFINE_STRING(BGl_string1658z00zz__paramz00,
		BgL_bgl_string1658za700za7za7_1722za7, "Illegal profile level", 21);
	      DEFINE_STRING(BGl_string1659z00zz__paramz00,
		BgL_bgl_string1659za700za7za7_1723za7, "&bigloo-profile-set!", 20);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2dnszd2cachezd2validityzd2timeoutzd2envzd2zz__paramz00,
		BgL_bgl_za762biglooza7d2dnsza71724za7,
		BGl_z62bigloozd2dnszd2cachezd2validityzd2timeoutz62zz__paramz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1660z00zz__paramz00,
		BgL_bgl_string1660za700za7za7_1725za7, "&bigloo-trace-set!", 18);
	      DEFINE_STRING(BGl_string1661z00zz__paramz00,
		BgL_bgl_string1661za700za7za7_1726za7, "pair-nil", 8);
	      DEFINE_STRING(BGl_string1662z00zz__paramz00,
		BgL_bgl_string1662za700za7za7_1727za7, "&bigloo-trace-stack-depth-set!",
		30);
	      DEFINE_STRING(BGl_string1665z00zz__paramz00,
		BgL_bgl_string1665za700za7za7_1728za7, "downcase", 8);
	      DEFINE_STRING(BGl_string1667z00zz__paramz00,
		BgL_bgl_string1667za700za7za7_1729za7, "upcase", 6);
	      DEFINE_STRING(BGl_string1668z00zz__paramz00,
		BgL_bgl_string1668za700za7za7_1730za7, "bigloo-sensitivity-set!", 23);
	      DEFINE_STRING(BGl_string1669z00zz__paramz00,
		BgL_bgl_string1669za700za7za7_1731za7, "Illegal sensitive value", 23);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bigloozd2warningzd2envz00zz__paramz00,
		BgL_bgl_za762biglooza7d2warn1732z00, BGl_z62bigloozd2warningzb0zz__paramz00,
		0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2casezd2sensitivezd2setz12zd2envz12zz__paramz00,
		BgL_bgl_za762biglooza7d2case1733z00,
		BGl_z62bigloozd2casezd2sensitivezd2setz12za2zz__paramz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2debugzd2setz12zd2envzc0zz__paramz00,
		BgL_bgl_za762biglooza7d2debu1734z00,
		BGl_z62bigloozd2debugzd2setz12z70zz__paramz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1670z00zz__paramz00,
		BgL_bgl_string1670za700za7za7_1735za7, "&bigloo-case-sensitive-set!", 27);
	      DEFINE_STRING(BGl_string1671z00zz__paramz00,
		BgL_bgl_string1671za700za7za7_1736za7, "symbol", 6);
	      DEFINE_STRING(BGl_string1673z00zz__paramz00,
		BgL_bgl_string1673za700za7za7_1737za7, "bigloo-library-path-set!", 24);
	      DEFINE_STRING(BGl_string1674z00zz__paramz00,
		BgL_bgl_string1674za700za7za7_1738za7, "Illegal values", 14);
	      DEFINE_STRING(BGl_string1675z00zz__paramz00,
		BgL_bgl_string1675za700za7za7_1739za7, "Illegal list", 12);
	      DEFINE_STRING(BGl_string1676z00zz__paramz00,
		BgL_bgl_string1676za700za7za7_1740za7, "&bigloo-library-path-set!", 25);
	      DEFINE_STRING(BGl_string1677z00zz__paramz00,
		BgL_bgl_string1677za700za7za7_1741za7,
		"&bigloo-dns-cache-validity-timeout-set!", 39);
	      DEFINE_STRING(BGl_string1678z00zz__paramz00,
		BgL_bgl_string1678za700za7za7_1742za7, "__param", 7);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bigloozd2profilezd2envz00zz__paramz00,
		BgL_bgl_za762biglooza7d2prof1743z00, BGl_z62bigloozd2profilezb0zz__paramz00,
		0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2debugzd2modulezd2setz12zd2envz12zz__paramz00,
		BgL_bgl_za762biglooza7d2debu1744z00,
		BGl_z62bigloozd2debugzd2modulezd2setz12za2zz__paramz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2initializa7edzf3zd2envz54zz__paramz00,
		BgL_bgl_za762biglooza7d2init1745z00,
		BGl_z62bigloozd2initializa7edzf3ze4zz__paramz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2evalzd2strictzd2modulezd2envz00zz__paramz00,
		BgL_bgl_za762biglooza7d2eval1746z00,
		BGl_z62bigloozd2evalzd2strictzd2modulezb0zz__paramz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2libraryzd2pathzd2envzd2zz__paramz00,
		BgL_bgl_za762biglooza7d2libr1747z00,
		BGl_z62bigloozd2libraryzd2pathz62zz__paramz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2libraryzd2pathzd2setz12zd2envz12zz__paramz00,
		BgL_bgl_za762biglooza7d2libr1748z00,
		BGl_z62bigloozd2libraryzd2pathzd2setz12za2zz__paramz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2tracezd2stackzd2depthzd2envz00zz__paramz00,
		BgL_bgl_za762biglooza7d2trac1749z00,
		BGl_z62bigloozd2tracezd2stackzd2depthzb0zz__paramz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2warningzd2setz12zd2envzc0zz__paramz00,
		BgL_bgl_za762biglooza7d2warn1750z00,
		BGl_z62bigloozd2warningzd2setz12z70zz__paramz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bigloozd2dnszd2enablezd2cachezd2envz00zz__paramz00,
		BgL_bgl_za762biglooza7d2dnsza71751za7,
		BGl_z62bigloozd2dnszd2enablezd2cachezb0zz__paramz00, 0L, BUNSPEC, 0);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_symbol1672z00zz__paramz00));
		     ADD_ROOT((void *) (&BGl_za2parameterzd2mutexza2zd2zz__paramz00));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__paramz00));
		     ADD_ROOT((void *) (&BGl_za2bigloozd2debugzd2moduleza2z00zz__paramz00));
		     ADD_ROOT((void *) (&BGl_list1663z00zz__paramz00));
		     ADD_ROOT((void *) (&BGl_za2bigloozd2profileza2zd2zz__paramz00));
		     ADD_ROOT((void *) (&BGl_za2bigloozd2warningza2zd2zz__paramz00));
		     ADD_ROOT((void *) (&BGl_za2bigloozd2debugza2zd2zz__paramz00));
		     ADD_ROOT((void *) (&BGl_za2bigloozd2loadzd2readerza2z00zz__paramz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2bigloozd2modulezd2extensionzd2handlerza2zd2zz__paramz00));
		     ADD_ROOT((void *) (&BGl_za2bigloozd2loadzd2moduleza2z00zz__paramz00));
		   
			 ADD_ROOT((void *) (&BGl_za2bigloozd2compilerzd2debugza2z00zz__paramz00));
		     ADD_ROOT((void *) (&BGl_za2bigloozd2libraryzd2pathza2z00zz__paramz00));
		     ADD_ROOT((void *) (&BGl_symbol1634z00zz__paramz00));
		     ADD_ROOT((void *) (&BGl_symbol1636z00zz__paramz00));
		     ADD_ROOT((void *) (&BGl_symbol1639z00zz__paramz00));
		   
			 ADD_ROOT((void *) (&BGl_za2bigloozd2casezd2sensitiveza2z00zz__paramz00));
		     ADD_ROOT((void *) (&BGl_symbol1645z00zz__paramz00));
		     ADD_ROOT((void *) (&BGl_symbol1648z00zz__paramz00));
		     ADD_ROOT((void *) (&BGl_symbol1652z00zz__paramz00));
		     ADD_ROOT((void *) (&BGl_symbol1656z00zz__paramz00));
		     ADD_ROOT((void *) (&BGl_za2bigloozd2traceza2zd2zz__paramz00));
		     ADD_ROOT((void *) (&BGl_symbol1664z00zz__paramz00));
		     ADD_ROOT((void *) (&BGl_symbol1666z00zz__paramz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long
		BgL_checksumz00_1877, char *BgL_fromz00_1878)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__paramz00))
				{
					BGl_requirezd2initializa7ationz75zz__paramz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__paramz00();
					BGl_cnstzd2initzd2zz__paramz00();
					BGl_importedzd2moduleszd2initz00zz__paramz00();
					return BGl_toplevelzd2initzd2zz__paramz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__paramz00(void)
	{
		{	/* Llib/param.scm 15 */
			BGl_symbol1634z00zz__paramz00 =
				bstring_to_symbol(BGl_string1635z00zz__paramz00);
			BGl_symbol1636z00zz__paramz00 =
				bstring_to_symbol(BGl_string1637z00zz__paramz00);
			BGl_symbol1639z00zz__paramz00 =
				bstring_to_symbol(BGl_string1640z00zz__paramz00);
			BGl_symbol1645z00zz__paramz00 =
				bstring_to_symbol(BGl_string1646z00zz__paramz00);
			BGl_symbol1648z00zz__paramz00 =
				bstring_to_symbol(BGl_string1649z00zz__paramz00);
			BGl_symbol1652z00zz__paramz00 =
				bstring_to_symbol(BGl_string1653z00zz__paramz00);
			BGl_symbol1656z00zz__paramz00 =
				bstring_to_symbol(BGl_string1657z00zz__paramz00);
			BGl_symbol1664z00zz__paramz00 =
				bstring_to_symbol(BGl_string1665z00zz__paramz00);
			BGl_symbol1666z00zz__paramz00 =
				bstring_to_symbol(BGl_string1667z00zz__paramz00);
			BGl_list1663z00zz__paramz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1634z00zz__paramz00,
				MAKE_YOUNG_PAIR(BGl_symbol1664z00zz__paramz00,
					MAKE_YOUNG_PAIR(BGl_symbol1666z00zz__paramz00, BNIL)));
			return (BGl_symbol1672z00zz__paramz00 =
				bstring_to_symbol(BGl_string1673z00zz__paramz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__paramz00(void)
	{
		{	/* Llib/param.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__paramz00(void)
	{
		{	/* Llib/param.scm 15 */
			BGl_za2parameterzd2mutexza2zd2zz__paramz00 =
				bgl_make_mutex(BGl_string1631z00zz__paramz00);
			BGl_za2bigloozd2strictzd2r5rszd2stringsza2zd2zz__paramz00 = ((bool_t) 0);
			BGl_za2bigloozd2compilerzd2debugza2z00zz__paramz00 = BINT(0L);
			BGl_za2bigloozd2debugza2zd2zz__paramz00 = BINT(0L);
			BGl_za2bigloozd2debugzd2moduleza2z00zz__paramz00 = BINT(0L);
			BGl_za2bigloozd2warningza2zd2zz__paramz00 = BINT(1L);
			BGl_za2bigloozd2profileza2zd2zz__paramz00 = BINT(0L);
			BGl_za2bigloozd2tracezd2colorza2z00zz__paramz00 = ((bool_t) 1);
			{	/* Llib/param.scm 206 */
				obj_t BgL_envzd2valuezd2_1090;

				BgL_envzd2valuezd2_1090 =
					BGl_getenvz00zz__osz00(BGl_string1632z00zz__paramz00);
				if (STRINGP(BgL_envzd2valuezd2_1090))
					{	/* Llib/param.scm 208 */
						obj_t BgL_l1072z00_1092;

						BgL_l1072z00_1092 =
							BGl_stringzd2splitzd2zz__r4_strings_6_7z00
							(BgL_envzd2valuezd2_1090, BNIL);
						if (NULLP(BgL_l1072z00_1092))
							{	/* Llib/param.scm 208 */
								BGl_za2bigloozd2traceza2zd2zz__paramz00 = BNIL;
							}
						else
							{	/* Llib/param.scm 208 */
								obj_t BgL_head1074z00_1094;

								{	/* Llib/param.scm 208 */
									obj_t BgL_arg1183z00_1106;

									{	/* Llib/param.scm 208 */
										obj_t BgL_arg1187z00_1107;

										BgL_arg1187z00_1107 = CAR(BgL_l1072z00_1092);
										BgL_arg1183z00_1106 =
											bstring_to_symbol(((obj_t) BgL_arg1187z00_1107));
									}
									BgL_head1074z00_1094 =
										MAKE_YOUNG_PAIR(BgL_arg1183z00_1106, BNIL);
								}
								{	/* Llib/param.scm 208 */
									obj_t BgL_g1077z00_1095;

									BgL_g1077z00_1095 = CDR(BgL_l1072z00_1092);
									{
										obj_t BgL_l1072z00_1595;
										obj_t BgL_tail1075z00_1596;

										BgL_l1072z00_1595 = BgL_g1077z00_1095;
										BgL_tail1075z00_1596 = BgL_head1074z00_1094;
									BgL_zc3z04anonymousza31167ze3z87_1594:
										if (NULLP(BgL_l1072z00_1595))
											{	/* Llib/param.scm 208 */
												BGl_za2bigloozd2traceza2zd2zz__paramz00 =
													BgL_head1074z00_1094;
											}
										else
											{	/* Llib/param.scm 208 */
												obj_t BgL_newtail1076z00_1603;

												{	/* Llib/param.scm 208 */
													obj_t BgL_arg1172z00_1604;

													{	/* Llib/param.scm 208 */
														obj_t BgL_arg1182z00_1605;

														BgL_arg1182z00_1605 =
															CAR(((obj_t) BgL_l1072z00_1595));
														BgL_arg1172z00_1604 =
															bstring_to_symbol(((obj_t) BgL_arg1182z00_1605));
													}
													BgL_newtail1076z00_1603 =
														MAKE_YOUNG_PAIR(BgL_arg1172z00_1604, BNIL);
												}
												SET_CDR(BgL_tail1075z00_1596, BgL_newtail1076z00_1603);
												{	/* Llib/param.scm 208 */
													obj_t BgL_arg1171z00_1606;

													BgL_arg1171z00_1606 =
														CDR(((obj_t) BgL_l1072z00_1595));
													{
														obj_t BgL_tail1075z00_1928;
														obj_t BgL_l1072z00_1927;

														BgL_l1072z00_1927 = BgL_arg1171z00_1606;
														BgL_tail1075z00_1928 = BgL_newtail1076z00_1603;
														BgL_tail1075z00_1596 = BgL_tail1075z00_1928;
														BgL_l1072z00_1595 = BgL_l1072z00_1927;
														goto BgL_zc3z04anonymousza31167ze3z87_1594;
													}
												}
											}
									}
								}
							}
					}
				else
					{	/* Llib/param.scm 207 */
						BGl_za2bigloozd2traceza2zd2zz__paramz00 = BNIL;
					}
			}
			{	/* Llib/param.scm 217 */
				obj_t BgL_envzd2valuezd2_1109;

				BgL_envzd2valuezd2_1109 =
					BGl_getenvz00zz__osz00(BGl_string1633z00zz__paramz00);
				if (STRINGP(BgL_envzd2valuezd2_1109))
					{	/* Llib/param.scm 219 */
						char *BgL_tmpz00_1932;

						BgL_tmpz00_1932 = BSTRING_TO_STRING(BgL_envzd2valuezd2_1109);
						BGl_za2bigloozd2tracezd2stackzd2depthza2zd2zz__paramz00 =
							BGL_STRTOL(BgL_tmpz00_1932, 0L, 10L);
					}
				else
					{	/* Llib/param.scm 218 */
						BGl_za2bigloozd2tracezd2stackzd2depthza2zd2zz__paramz00 = 10L;
					}
			}
			BGl_za2bigloozd2casezd2sensitiveza2z00zz__paramz00 =
				BGl_symbol1634z00zz__paramz00;
			BGl_za2bigloozd2initializa7edpza2z75zz__paramz00 = ((bool_t) 0);
			BGl_za2bigloozd2loadzd2readerza2z00zz__paramz00 = BFALSE;
			BGl_za2bigloozd2loadzd2moduleza2z00zz__paramz00 = BFALSE;
			BGl_za2bigloozd2modulezd2extensionzd2handlerza2zd2zz__paramz00 = BFALSE;
			BGl_za2bigloozd2evalzd2strictzd2moduleza2zd2zz__paramz00 = ((bool_t) 1);
			{	/* Llib/param.scm 281 */
				obj_t BgL_arg1190z00_1111;

				BgL_arg1190z00_1111 =
					BGl_bigloozd2configzd2zz__configurez00(BGl_symbol1636z00zz__paramz00);
				{	/* Llib/param.scm 281 */
					obj_t BgL_list1191z00_1112;

					{	/* Llib/param.scm 281 */
						obj_t BgL_arg1193z00_1113;

						BgL_arg1193z00_1113 = MAKE_YOUNG_PAIR(BgL_arg1190z00_1111, BNIL);
						BgL_list1191z00_1112 =
							MAKE_YOUNG_PAIR(BGl_string1638z00zz__paramz00,
							BgL_arg1193z00_1113);
					}
					BGl_za2bigloozd2libraryzd2pathza2z00zz__paramz00 =
						BgL_list1191z00_1112;
				}
			}
			BGl_za2bigloozd2dnszd2enablezd2cacheza2zd2zz__paramz00 = ((bool_t) 1);
			return (BGl_za2bigloozd2dnszd2cachezd2validityzd2timeoutza2z00zz__paramz00
				= 20L, BUNSPEC);
		}

	}



/* bigloo-strict-r5rs-strings */
	BGL_EXPORTED_DEF bool_t
		BGl_bigloozd2strictzd2r5rszd2stringszd2zz__paramz00(void)
	{
		{	/* Llib/param.scm 135 */
			return BGl_za2bigloozd2strictzd2r5rszd2stringsza2zd2zz__paramz00;
		}

	}



/* &bigloo-strict-r5rs-strings */
	obj_t BGl_z62bigloozd2strictzd2r5rszd2stringszb0zz__paramz00(obj_t
		BgL_envz00_1801)
	{
		{	/* Llib/param.scm 135 */
			return BBOOL(BGl_bigloozd2strictzd2r5rszd2stringszd2zz__paramz00());
		}

	}



/* bigloo-strict-r5rs-strings-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bigloozd2strictzd2r5rszd2stringszd2setz12z12zz__paramz00(bool_t
		BgL_vz00_3)
	{
		{	/* Llib/param.scm 135 */
			BGL_MUTEX_LOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00);
			{	/* Llib/param.scm 135 */
				obj_t BgL_tmp1757z00_1940;

				BgL_tmp1757z00_1940 =
					(BGl_za2bigloozd2strictzd2r5rszd2stringsza2zd2zz__paramz00 =
					BgL_vz00_3, BUNSPEC);
				BGL_MUTEX_UNLOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00);
				BgL_tmp1757z00_1940;
			}
			return BBOOL(BgL_vz00_3);
		}

	}



/* &bigloo-strict-r5rs-strings-set! */
	obj_t BGl_z62bigloozd2strictzd2r5rszd2stringszd2setz12z70zz__paramz00(obj_t
		BgL_envz00_1802, obj_t BgL_vz00_1803)
	{
		{	/* Llib/param.scm 135 */
			return
				BGl_bigloozd2strictzd2r5rszd2stringszd2setz12z12zz__paramz00(CBOOL
				(BgL_vz00_1803));
		}

	}



/* bigloo-compiler-debug */
	BGL_EXPORTED_DEF int BGl_bigloozd2compilerzd2debugz00zz__paramz00(void)
	{
		{	/* Llib/param.scm 142 */
			return CINT(BGl_za2bigloozd2compilerzd2debugza2z00zz__paramz00);
		}

	}



/* &bigloo-compiler-debug */
	obj_t BGl_z62bigloozd2compilerzd2debugz62zz__paramz00(obj_t BgL_envz00_1804)
	{
		{	/* Llib/param.scm 142 */
			return BINT(BGl_bigloozd2compilerzd2debugz00zz__paramz00());
		}

	}



/* bigloo-compiler-debug-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bigloozd2compilerzd2debugzd2setz12zc0zz__paramz00(int BgL_vz00_4)
	{
		{	/* Llib/param.scm 142 */
			{	/* Llib/param.scm 142 */
				obj_t BgL_top1759z00_1950;

				BgL_top1759z00_1950 = BGL_EXITD_TOP_AS_OBJ();
				BGL_MUTEX_LOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00);
				BGL_EXITD_PUSH_PROTECT(BgL_top1759z00_1950,
					BGl_za2parameterzd2mutexza2zd2zz__paramz00);
				BUNSPEC;
				{	/* Llib/param.scm 142 */
					obj_t BgL_tmp1758z00_1949;

					if (((long) (BgL_vz00_4) < 0L))
						{	/* Llib/param.scm 145 */
							BgL_tmp1758z00_1949 =
								(BGl_za2bigloozd2compilerzd2debugza2z00zz__paramz00 =
								BGl_errorz00zz__errorz00(BGl_symbol1639z00zz__paramz00,
									BGl_string1641z00zz__paramz00, BINT(BgL_vz00_4)), BUNSPEC);
						}
					else
						{	/* Llib/param.scm 145 */
							BgL_tmp1758z00_1949 =
								(BGl_za2bigloozd2compilerzd2debugza2z00zz__paramz00 =
								BINT(BgL_vz00_4), BUNSPEC);
						}
					BGL_EXITD_POP_PROTECT(BgL_top1759z00_1950);
					BUNSPEC;
					BGL_MUTEX_UNLOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00);
					BgL_tmp1758z00_1949;
				}
			}
			return BINT(BgL_vz00_4);
		}

	}



/* &bigloo-compiler-debug-set! */
	obj_t BGl_z62bigloozd2compilerzd2debugzd2setz12za2zz__paramz00(obj_t
		BgL_envz00_1805, obj_t BgL_vz00_1806)
	{
		{	/* Llib/param.scm 142 */
			{	/* Llib/param.scm 142 */
				int BgL_auxz00_1963;

				{	/* Llib/param.scm 142 */
					obj_t BgL_tmpz00_1964;

					if (INTEGERP(BgL_vz00_1806))
						{	/* Llib/param.scm 142 */
							BgL_tmpz00_1964 = BgL_vz00_1806;
						}
					else
						{
							obj_t BgL_auxz00_1967;

							BgL_auxz00_1967 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1642z00zz__paramz00,
								BINT(4860L), BGl_string1643z00zz__paramz00,
								BGl_string1644z00zz__paramz00, BgL_vz00_1806);
							FAILURE(BgL_auxz00_1967, BFALSE, BFALSE);
						}
					BgL_auxz00_1963 = CINT(BgL_tmpz00_1964);
				}
				return
					BGl_bigloozd2compilerzd2debugzd2setz12zc0zz__paramz00
					(BgL_auxz00_1963);
			}
		}

	}



/* bigloo-debug */
	BGL_EXPORTED_DEF int bgl_debug(void)
	{
		{	/* Llib/param.scm 154 */
			return CINT(BGl_za2bigloozd2debugza2zd2zz__paramz00);
		}

	}



/* &bigloo-debug */
	obj_t BGl_z62bigloozd2debugzb0zz__paramz00(obj_t BgL_envz00_1807)
	{
		{	/* Llib/param.scm 154 */
			return BINT(bgl_debug());
		}

	}



/* bigloo-debug-set! */
	BGL_EXPORTED_DEF obj_t bgl_debug_set(int BgL_vz00_5)
	{
		{	/* Llib/param.scm 154 */
			{	/* Llib/param.scm 154 */
				obj_t BgL_top1763z00_1977;

				BgL_top1763z00_1977 = BGL_EXITD_TOP_AS_OBJ();
				BGL_MUTEX_LOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00);
				BGL_EXITD_PUSH_PROTECT(BgL_top1763z00_1977,
					BGl_za2parameterzd2mutexza2zd2zz__paramz00);
				BUNSPEC;
				{	/* Llib/param.scm 154 */
					obj_t BgL_tmp1762z00_1976;

					if (((long) (BgL_vz00_5) < 0L))
						{	/* Llib/param.scm 157 */
							BgL_tmp1762z00_1976 = (BGl_za2bigloozd2debugza2zd2zz__paramz00 =
								BGl_errorz00zz__errorz00(BGl_symbol1645z00zz__paramz00,
									BGl_string1641z00zz__paramz00, BINT(BgL_vz00_5)), BUNSPEC);
						}
					else
						{	/* Llib/param.scm 157 */
							BgL_tmp1762z00_1976 = (BGl_za2bigloozd2debugza2zd2zz__paramz00 =
								BINT(BgL_vz00_5), BUNSPEC);
						}
					BGL_EXITD_POP_PROTECT(BgL_top1763z00_1977);
					BUNSPEC;
					BGL_MUTEX_UNLOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00);
					BgL_tmp1762z00_1976;
				}
			}
			return BINT(BgL_vz00_5);
		}

	}



/* &bigloo-debug-set! */
	obj_t BGl_z62bigloozd2debugzd2setz12z70zz__paramz00(obj_t BgL_envz00_1808,
		obj_t BgL_vz00_1809)
	{
		{	/* Llib/param.scm 154 */
			{	/* Llib/param.scm 154 */
				int BgL_auxz00_1990;

				{	/* Llib/param.scm 154 */
					obj_t BgL_tmpz00_1991;

					if (INTEGERP(BgL_vz00_1809))
						{	/* Llib/param.scm 154 */
							BgL_tmpz00_1991 = BgL_vz00_1809;
						}
					else
						{
							obj_t BgL_auxz00_1994;

							BgL_auxz00_1994 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1642z00zz__paramz00,
								BINT(5413L), BGl_string1647z00zz__paramz00,
								BGl_string1644z00zz__paramz00, BgL_vz00_1809);
							FAILURE(BgL_auxz00_1994, BFALSE, BFALSE);
						}
					BgL_auxz00_1990 = CINT(BgL_tmpz00_1991);
				}
				return bgl_debug_set(BgL_auxz00_1990);
			}
		}

	}



/* bigloo-debug-module */
	BGL_EXPORTED_DEF int BGl_bigloozd2debugzd2modulez00zz__paramz00(void)
	{
		{	/* Llib/param.scm 166 */
			return CINT(BGl_za2bigloozd2debugzd2moduleza2z00zz__paramz00);
		}

	}



/* &bigloo-debug-module */
	obj_t BGl_z62bigloozd2debugzd2modulez62zz__paramz00(obj_t BgL_envz00_1810)
	{
		{	/* Llib/param.scm 166 */
			return BINT(BGl_bigloozd2debugzd2modulez00zz__paramz00());
		}

	}



/* bigloo-debug-module-set! */
	BGL_EXPORTED_DEF obj_t BGl_bigloozd2debugzd2modulezd2setz12zc0zz__paramz00(int
		BgL_vz00_6)
	{
		{	/* Llib/param.scm 166 */
			{	/* Llib/param.scm 166 */
				obj_t BgL_top1767z00_2004;

				BgL_top1767z00_2004 = BGL_EXITD_TOP_AS_OBJ();
				BGL_MUTEX_LOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00);
				BGL_EXITD_PUSH_PROTECT(BgL_top1767z00_2004,
					BGl_za2parameterzd2mutexza2zd2zz__paramz00);
				BUNSPEC;
				{	/* Llib/param.scm 166 */
					obj_t BgL_tmp1766z00_2003;

					if (((long) (BgL_vz00_6) < 0L))
						{	/* Llib/param.scm 169 */
							BgL_tmp1766z00_2003 =
								(BGl_za2bigloozd2debugzd2moduleza2z00zz__paramz00 =
								BGl_errorz00zz__errorz00(BGl_symbol1648z00zz__paramz00,
									BGl_string1650z00zz__paramz00, BINT(BgL_vz00_6)), BUNSPEC);
						}
					else
						{	/* Llib/param.scm 169 */
							BgL_tmp1766z00_2003 =
								(BGl_za2bigloozd2debugzd2moduleza2z00zz__paramz00 =
								BINT(BgL_vz00_6), BUNSPEC);
						}
					BGL_EXITD_POP_PROTECT(BgL_top1767z00_2004);
					BUNSPEC;
					BGL_MUTEX_UNLOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00);
					BgL_tmp1766z00_2003;
				}
			}
			return BINT(BgL_vz00_6);
		}

	}



/* &bigloo-debug-module-set! */
	obj_t BGl_z62bigloozd2debugzd2modulezd2setz12za2zz__paramz00(obj_t
		BgL_envz00_1811, obj_t BgL_vz00_1812)
	{
		{	/* Llib/param.scm 166 */
			{	/* Llib/param.scm 166 */
				int BgL_auxz00_2017;

				{	/* Llib/param.scm 166 */
					obj_t BgL_tmpz00_2018;

					if (INTEGERP(BgL_vz00_1812))
						{	/* Llib/param.scm 166 */
							BgL_tmpz00_2018 = BgL_vz00_1812;
						}
					else
						{
							obj_t BgL_auxz00_2021;

							BgL_auxz00_2021 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1642z00zz__paramz00,
								BINT(5945L), BGl_string1651z00zz__paramz00,
								BGl_string1644z00zz__paramz00, BgL_vz00_1812);
							FAILURE(BgL_auxz00_2021, BFALSE, BFALSE);
						}
					BgL_auxz00_2017 = CINT(BgL_tmpz00_2018);
				}
				return
					BGl_bigloozd2debugzd2modulezd2setz12zc0zz__paramz00(BgL_auxz00_2017);
			}
		}

	}



/* bigloo-warning */
	BGL_EXPORTED_DEF int BGl_bigloozd2warningzd2zz__paramz00(void)
	{
		{	/* Llib/param.scm 178 */
			return CINT(BGl_za2bigloozd2warningza2zd2zz__paramz00);
		}

	}



/* &bigloo-warning */
	obj_t BGl_z62bigloozd2warningzb0zz__paramz00(obj_t BgL_envz00_1813)
	{
		{	/* Llib/param.scm 178 */
			return BINT(BGl_bigloozd2warningzd2zz__paramz00());
		}

	}



/* bigloo-warning-set! */
	BGL_EXPORTED_DEF obj_t BGl_bigloozd2warningzd2setz12z12zz__paramz00(int
		BgL_vz00_7)
	{
		{	/* Llib/param.scm 178 */
			{	/* Llib/param.scm 178 */
				obj_t BgL_top1771z00_2031;

				BgL_top1771z00_2031 = BGL_EXITD_TOP_AS_OBJ();
				BGL_MUTEX_LOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00);
				BGL_EXITD_PUSH_PROTECT(BgL_top1771z00_2031,
					BGl_za2parameterzd2mutexza2zd2zz__paramz00);
				BUNSPEC;
				{	/* Llib/param.scm 178 */
					obj_t BgL_tmp1770z00_2030;

					if (((long) (BgL_vz00_7) < 0L))
						{	/* Llib/param.scm 181 */
							BgL_tmp1770z00_2030 = (BGl_za2bigloozd2warningza2zd2zz__paramz00 =
								BGl_errorz00zz__errorz00(BGl_symbol1652z00zz__paramz00,
									BGl_string1654z00zz__paramz00, BINT(BgL_vz00_7)), BUNSPEC);
						}
					else
						{	/* Llib/param.scm 181 */
							BgL_tmp1770z00_2030 = (BGl_za2bigloozd2warningza2zd2zz__paramz00 =
								BINT(BgL_vz00_7), BUNSPEC);
						}
					BGL_EXITD_POP_PROTECT(BgL_top1771z00_2031);
					BUNSPEC;
					BGL_MUTEX_UNLOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00);
					BgL_tmp1770z00_2030;
				}
			}
			return BINT(BgL_vz00_7);
		}

	}



/* &bigloo-warning-set! */
	obj_t BGl_z62bigloozd2warningzd2setz12z70zz__paramz00(obj_t BgL_envz00_1814,
		obj_t BgL_vz00_1815)
	{
		{	/* Llib/param.scm 178 */
			{	/* Llib/param.scm 178 */
				int BgL_auxz00_2044;

				{	/* Llib/param.scm 178 */
					obj_t BgL_tmpz00_2045;

					if (INTEGERP(BgL_vz00_1815))
						{	/* Llib/param.scm 178 */
							BgL_tmpz00_2045 = BgL_vz00_1815;
						}
					else
						{
							obj_t BgL_auxz00_2048;

							BgL_auxz00_2048 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1642z00zz__paramz00,
								BINT(6498L), BGl_string1655z00zz__paramz00,
								BGl_string1644z00zz__paramz00, BgL_vz00_1815);
							FAILURE(BgL_auxz00_2048, BFALSE, BFALSE);
						}
					BgL_auxz00_2044 = CINT(BgL_tmpz00_2045);
				}
				return BGl_bigloozd2warningzd2setz12z12zz__paramz00(BgL_auxz00_2044);
			}
		}

	}



/* bigloo-profile */
	BGL_EXPORTED_DEF int BGl_bigloozd2profilezd2zz__paramz00(void)
	{
		{	/* Llib/param.scm 190 */
			return CINT(BGl_za2bigloozd2profileza2zd2zz__paramz00);
		}

	}



/* &bigloo-profile */
	obj_t BGl_z62bigloozd2profilezb0zz__paramz00(obj_t BgL_envz00_1816)
	{
		{	/* Llib/param.scm 190 */
			return BINT(BGl_bigloozd2profilezd2zz__paramz00());
		}

	}



/* bigloo-profile-set! */
	BGL_EXPORTED_DEF obj_t BGl_bigloozd2profilezd2setz12z12zz__paramz00(int
		BgL_vz00_8)
	{
		{	/* Llib/param.scm 190 */
			{	/* Llib/param.scm 190 */
				obj_t BgL_top1775z00_2058;

				BgL_top1775z00_2058 = BGL_EXITD_TOP_AS_OBJ();
				BGL_MUTEX_LOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00);
				BGL_EXITD_PUSH_PROTECT(BgL_top1775z00_2058,
					BGl_za2parameterzd2mutexza2zd2zz__paramz00);
				BUNSPEC;
				{	/* Llib/param.scm 190 */
					obj_t BgL_tmp1774z00_2057;

					if (((long) (BgL_vz00_8) < 0L))
						{	/* Llib/param.scm 193 */
							BgL_tmp1774z00_2057 = (BGl_za2bigloozd2profileza2zd2zz__paramz00 =
								BGl_errorz00zz__errorz00(BGl_symbol1656z00zz__paramz00,
									BGl_string1658z00zz__paramz00, BINT(BgL_vz00_8)), BUNSPEC);
						}
					else
						{	/* Llib/param.scm 193 */
							BgL_tmp1774z00_2057 = (BGl_za2bigloozd2profileza2zd2zz__paramz00 =
								BINT(BgL_vz00_8), BUNSPEC);
						}
					BGL_EXITD_POP_PROTECT(BgL_top1775z00_2058);
					BUNSPEC;
					BGL_MUTEX_UNLOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00);
					BgL_tmp1774z00_2057;
				}
			}
			return BINT(BgL_vz00_8);
		}

	}



/* &bigloo-profile-set! */
	obj_t BGl_z62bigloozd2profilezd2setz12z70zz__paramz00(obj_t BgL_envz00_1817,
		obj_t BgL_vz00_1818)
	{
		{	/* Llib/param.scm 190 */
			{	/* Llib/param.scm 190 */
				int BgL_auxz00_2071;

				{	/* Llib/param.scm 190 */
					obj_t BgL_tmpz00_2072;

					if (INTEGERP(BgL_vz00_1818))
						{	/* Llib/param.scm 190 */
							BgL_tmpz00_2072 = BgL_vz00_1818;
						}
					else
						{
							obj_t BgL_auxz00_2075;

							BgL_auxz00_2075 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1642z00zz__paramz00,
								BINT(7036L), BGl_string1659z00zz__paramz00,
								BGl_string1644z00zz__paramz00, BgL_vz00_1818);
							FAILURE(BgL_auxz00_2075, BFALSE, BFALSE);
						}
					BgL_auxz00_2071 = CINT(BgL_tmpz00_2072);
				}
				return BGl_bigloozd2profilezd2setz12z12zz__paramz00(BgL_auxz00_2071);
			}
		}

	}



/* bigloo-trace-color */
	BGL_EXPORTED_DEF bool_t BGl_bigloozd2tracezd2colorz00zz__paramz00(void)
	{
		{	/* Llib/param.scm 200 */
			return BGl_za2bigloozd2tracezd2colorza2z00zz__paramz00;
		}

	}



/* &bigloo-trace-color */
	obj_t BGl_z62bigloozd2tracezd2colorz62zz__paramz00(obj_t BgL_envz00_1819)
	{
		{	/* Llib/param.scm 200 */
			return BBOOL(BGl_bigloozd2tracezd2colorz00zz__paramz00());
		}

	}



/* bigloo-trace-color-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bigloozd2tracezd2colorzd2setz12zc0zz__paramz00(bool_t BgL_vz00_9)
	{
		{	/* Llib/param.scm 200 */
			BGL_MUTEX_LOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00);
			{	/* Llib/param.scm 200 */
				obj_t BgL_tmp1778z00_2083;

				BgL_tmp1778z00_2083 = (BGl_za2bigloozd2tracezd2colorza2z00zz__paramz00 =
					BgL_vz00_9, BUNSPEC);
				BGL_MUTEX_UNLOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00);
				BgL_tmp1778z00_2083;
			}
			return BBOOL(BgL_vz00_9);
		}

	}



/* &bigloo-trace-color-set! */
	obj_t BGl_z62bigloozd2tracezd2colorzd2setz12za2zz__paramz00(obj_t
		BgL_envz00_1820, obj_t BgL_vz00_1821)
	{
		{	/* Llib/param.scm 200 */
			return
				BGl_bigloozd2tracezd2colorzd2setz12zc0zz__paramz00(CBOOL
				(BgL_vz00_1821));
		}

	}



/* bigloo-trace */
	BGL_EXPORTED_DEF obj_t BGl_bigloozd2tracezd2zz__paramz00(void)
	{
		{	/* Llib/param.scm 205 */
			return BGl_za2bigloozd2traceza2zd2zz__paramz00;
		}

	}



/* &bigloo-trace */
	obj_t BGl_z62bigloozd2tracezb0zz__paramz00(obj_t BgL_envz00_1822)
	{
		{	/* Llib/param.scm 205 */
			return BGl_bigloozd2tracezd2zz__paramz00();
		}

	}



/* bigloo-trace-set! */
	BGL_EXPORTED_DEF obj_t BGl_bigloozd2tracezd2setz12z12zz__paramz00(obj_t
		BgL_vz00_10)
	{
		{	/* Llib/param.scm 205 */
			BGL_MUTEX_LOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00);
			{	/* Llib/param.scm 205 */
				obj_t BgL_tmp1779z00_2090;

				BgL_tmp1779z00_2090 = (BGl_za2bigloozd2traceza2zd2zz__paramz00 =
					BgL_vz00_10, BUNSPEC);
				BGL_MUTEX_UNLOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00);
				BgL_tmp1779z00_2090;
			}
			return BgL_vz00_10;
		}

	}



/* &bigloo-trace-set! */
	obj_t BGl_z62bigloozd2tracezd2setz12z70zz__paramz00(obj_t BgL_envz00_1823,
		obj_t BgL_vz00_1824)
	{
		{	/* Llib/param.scm 205 */
			{	/* Llib/param.scm 205 */
				obj_t BgL_auxz00_2093;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_vz00_1824))
					{	/* Llib/param.scm 205 */
						BgL_auxz00_2093 = BgL_vz00_1824;
					}
				else
					{
						obj_t BgL_auxz00_2096;

						BgL_auxz00_2096 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1642z00zz__paramz00,
							BINT(7690L), BGl_string1660z00zz__paramz00,
							BGl_string1661z00zz__paramz00, BgL_vz00_1824);
						FAILURE(BgL_auxz00_2096, BFALSE, BFALSE);
					}
				return BGl_bigloozd2tracezd2setz12z12zz__paramz00(BgL_auxz00_2093);
			}
		}

	}



/* bigloo-trace-stack-depth */
	BGL_EXPORTED_DEF int BGl_bigloozd2tracezd2stackzd2depthzd2zz__paramz00(void)
	{
		{	/* Llib/param.scm 216 */
			return (int) (BGl_za2bigloozd2tracezd2stackzd2depthza2zd2zz__paramz00);
		}

	}



/* &bigloo-trace-stack-depth */
	obj_t BGl_z62bigloozd2tracezd2stackzd2depthzb0zz__paramz00(obj_t
		BgL_envz00_1825)
	{
		{	/* Llib/param.scm 216 */
			return BINT(BGl_bigloozd2tracezd2stackzd2depthzd2zz__paramz00());
		}

	}



/* bigloo-trace-stack-depth-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bigloozd2tracezd2stackzd2depthzd2setz12z12zz__paramz00(int BgL_vz00_11)
	{
		{	/* Llib/param.scm 216 */
			BGL_MUTEX_LOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00);
			{	/* Llib/param.scm 216 */
				obj_t BgL_tmp1781z00_2104;

				BgL_tmp1781z00_2104 =
					(BGl_za2bigloozd2tracezd2stackzd2depthza2zd2zz__paramz00 =
					(long) (BgL_vz00_11), BUNSPEC);
				BGL_MUTEX_UNLOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00);
				BgL_tmp1781z00_2104;
			}
			return BINT(BgL_vz00_11);
		}

	}



/* &bigloo-trace-stack-depth-set! */
	obj_t BGl_z62bigloozd2tracezd2stackzd2depthzd2setz12z70zz__paramz00(obj_t
		BgL_envz00_1826, obj_t BgL_vz00_1827)
	{
		{	/* Llib/param.scm 216 */
			{	/* Llib/param.scm 216 */
				int BgL_auxz00_2109;

				{	/* Llib/param.scm 216 */
					obj_t BgL_tmpz00_2110;

					if (INTEGERP(BgL_vz00_1827))
						{	/* Llib/param.scm 216 */
							BgL_tmpz00_2110 = BgL_vz00_1827;
						}
					else
						{
							obj_t BgL_auxz00_2113;

							BgL_auxz00_2113 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1642z00zz__paramz00,
								BINT(8226L), BGl_string1662z00zz__paramz00,
								BGl_string1644z00zz__paramz00, BgL_vz00_1827);
							FAILURE(BgL_auxz00_2113, BFALSE, BFALSE);
						}
					BgL_auxz00_2109 = CINT(BgL_tmpz00_2110);
				}
				return
					BGl_bigloozd2tracezd2stackzd2depthzd2setz12z12zz__paramz00
					(BgL_auxz00_2109);
			}
		}

	}



/* bigloo-case-sensitive */
	BGL_EXPORTED_DEF obj_t BGl_bigloozd2casezd2sensitivez00zz__paramz00(void)
	{
		{	/* Llib/param.scm 231 */
			return BGl_za2bigloozd2casezd2sensitiveza2z00zz__paramz00;
		}

	}



/* &bigloo-case-sensitive */
	obj_t BGl_z62bigloozd2casezd2sensitivez62zz__paramz00(obj_t BgL_envz00_1828)
	{
		{	/* Llib/param.scm 231 */
			return BGl_bigloozd2casezd2sensitivez00zz__paramz00();
		}

	}



/* bigloo-case-sensitive-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bigloozd2casezd2sensitivezd2setz12zc0zz__paramz00(obj_t BgL_vz00_12)
	{
		{	/* Llib/param.scm 231 */
			{	/* Llib/param.scm 231 */
				obj_t BgL_top1784z00_2121;

				BgL_top1784z00_2121 = BGL_EXITD_TOP_AS_OBJ();
				BGL_MUTEX_LOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00);
				BGL_EXITD_PUSH_PROTECT(BgL_top1784z00_2121,
					BGl_za2parameterzd2mutexza2zd2zz__paramz00);
				BUNSPEC;
				{	/* Llib/param.scm 231 */
					obj_t BgL_tmp1783z00_2120;

					if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_vz00_12,
								BGl_list1663z00zz__paramz00)))
						{	/* Llib/param.scm 235 */
							BgL_tmp1783z00_2120 =
								(BGl_za2bigloozd2casezd2sensitiveza2z00zz__paramz00 =
								BgL_vz00_12, BUNSPEC);
						}
					else
						{	/* Llib/param.scm 235 */
							BgL_tmp1783z00_2120 =
								(BGl_za2bigloozd2casezd2sensitiveza2z00zz__paramz00 =
								BGl_errorz00zz__errorz00(BGl_string1668z00zz__paramz00,
									BGl_string1669z00zz__paramz00, BgL_vz00_12), BUNSPEC);
						}
					BGL_EXITD_POP_PROTECT(BgL_top1784z00_2121);
					BUNSPEC;
					BGL_MUTEX_UNLOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00);
					BgL_tmp1783z00_2120;
				}
			}
			return BgL_vz00_12;
		}

	}



/* &bigloo-case-sensitive-set! */
	obj_t BGl_z62bigloozd2casezd2sensitivezd2setz12za2zz__paramz00(obj_t
		BgL_envz00_1829, obj_t BgL_vz00_1830)
	{
		{	/* Llib/param.scm 231 */
			{	/* Llib/param.scm 231 */
				obj_t BgL_auxz00_2131;

				if (SYMBOLP(BgL_vz00_1830))
					{	/* Llib/param.scm 231 */
						BgL_auxz00_2131 = BgL_vz00_1830;
					}
				else
					{
						obj_t BgL_auxz00_2134;

						BgL_auxz00_2134 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1642z00zz__paramz00,
							BINT(9056L), BGl_string1670z00zz__paramz00,
							BGl_string1671z00zz__paramz00, BgL_vz00_1830);
						FAILURE(BgL_auxz00_2134, BFALSE, BFALSE);
					}
				return
					BGl_bigloozd2casezd2sensitivezd2setz12zc0zz__paramz00
					(BgL_auxz00_2131);
			}
		}

	}



/* bigloo-initialized! */
	BGL_EXPORTED_DEF obj_t BGl_bigloozd2initializa7edz12z67zz__paramz00(void)
	{
		{	/* Llib/param.scm 248 */
			return (BGl_za2bigloozd2initializa7edpza2z75zz__paramz00 =
				((bool_t) 1), BUNSPEC);
		}

	}



/* &bigloo-initialized! */
	obj_t BGl_z62bigloozd2initializa7edz12z05zz__paramz00(obj_t BgL_envz00_1831)
	{
		{	/* Llib/param.scm 248 */
			return BGl_bigloozd2initializa7edz12z67zz__paramz00();
		}

	}



/* bigloo-initialized? */
	BGL_EXPORTED_DEF bool_t BGl_bigloozd2initializa7edzf3z86zz__paramz00(void)
	{
		{	/* Llib/param.scm 254 */
			return BGl_za2bigloozd2initializa7edpza2z75zz__paramz00;
		}

	}



/* &bigloo-initialized? */
	obj_t BGl_z62bigloozd2initializa7edzf3ze4zz__paramz00(obj_t BgL_envz00_1832)
	{
		{	/* Llib/param.scm 254 */
			return BBOOL(BGl_bigloozd2initializa7edzf3z86zz__paramz00());
		}

	}



/* bigloo-load-reader */
	BGL_EXPORTED_DEF obj_t BGl_bigloozd2loadzd2readerz00zz__paramz00(void)
	{
		{	/* Llib/param.scm 260 */
			return BGl_za2bigloozd2loadzd2readerza2z00zz__paramz00;
		}

	}



/* &bigloo-load-reader */
	obj_t BGl_z62bigloozd2loadzd2readerz62zz__paramz00(obj_t BgL_envz00_1833)
	{
		{	/* Llib/param.scm 260 */
			return BGl_bigloozd2loadzd2readerz00zz__paramz00();
		}

	}



/* bigloo-load-reader-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bigloozd2loadzd2readerzd2setz12zc0zz__paramz00(obj_t BgL_vz00_13)
	{
		{	/* Llib/param.scm 260 */
			BGL_MUTEX_LOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00);
			{	/* Llib/param.scm 260 */
				obj_t BgL_tmp1787z00_2143;

				BgL_tmp1787z00_2143 = (BGl_za2bigloozd2loadzd2readerza2z00zz__paramz00 =
					BgL_vz00_13, BUNSPEC);
				BGL_MUTEX_UNLOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00);
				BgL_tmp1787z00_2143;
			}
			return BgL_vz00_13;
		}

	}



/* &bigloo-load-reader-set! */
	obj_t BGl_z62bigloozd2loadzd2readerzd2setz12za2zz__paramz00(obj_t
		BgL_envz00_1834, obj_t BgL_vz00_1835)
	{
		{	/* Llib/param.scm 260 */
			return BGl_bigloozd2loadzd2readerzd2setz12zc0zz__paramz00(BgL_vz00_1835);
		}

	}



/* bigloo-load-module */
	BGL_EXPORTED_DEF obj_t BGl_bigloozd2loadzd2modulez00zz__paramz00(void)
	{
		{	/* Llib/param.scm 265 */
			return BGl_za2bigloozd2loadzd2moduleza2z00zz__paramz00;
		}

	}



/* &bigloo-load-module */
	obj_t BGl_z62bigloozd2loadzd2modulez62zz__paramz00(obj_t BgL_envz00_1836)
	{
		{	/* Llib/param.scm 265 */
			return BGl_bigloozd2loadzd2modulez00zz__paramz00();
		}

	}



/* bigloo-load-module-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bigloozd2loadzd2modulezd2setz12zc0zz__paramz00(obj_t BgL_vz00_14)
	{
		{	/* Llib/param.scm 265 */
			BGL_MUTEX_LOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00);
			{	/* Llib/param.scm 265 */
				obj_t BgL_tmp1788z00_2148;

				BgL_tmp1788z00_2148 = (BGl_za2bigloozd2loadzd2moduleza2z00zz__paramz00 =
					BgL_vz00_14, BUNSPEC);
				BGL_MUTEX_UNLOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00);
				BgL_tmp1788z00_2148;
			}
			return BgL_vz00_14;
		}

	}



/* &bigloo-load-module-set! */
	obj_t BGl_z62bigloozd2loadzd2modulezd2setz12za2zz__paramz00(obj_t
		BgL_envz00_1837, obj_t BgL_vz00_1838)
	{
		{	/* Llib/param.scm 265 */
			return BGl_bigloozd2loadzd2modulezd2setz12zc0zz__paramz00(BgL_vz00_1838);
		}

	}



/* bigloo-module-extension-handler */
	BGL_EXPORTED_DEF obj_t
		BGl_bigloozd2modulezd2extensionzd2handlerzd2zz__paramz00(void)
	{
		{	/* Llib/param.scm 270 */
			return BGl_za2bigloozd2modulezd2extensionzd2handlerza2zd2zz__paramz00;
		}

	}



/* &bigloo-module-extension-handler */
	obj_t BGl_z62bigloozd2modulezd2extensionzd2handlerzb0zz__paramz00(obj_t
		BgL_envz00_1839)
	{
		{	/* Llib/param.scm 270 */
			return BGl_bigloozd2modulezd2extensionzd2handlerzd2zz__paramz00();
		}

	}



/* bigloo-module-extension-handler-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bigloozd2modulezd2extensionzd2handlerzd2setz12z12zz__paramz00(obj_t
		BgL_vz00_15)
	{
		{	/* Llib/param.scm 270 */
			BGL_MUTEX_LOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00);
			{	/* Llib/param.scm 270 */
				obj_t BgL_tmp1789z00_2153;

				BgL_tmp1789z00_2153 =
					(BGl_za2bigloozd2modulezd2extensionzd2handlerza2zd2zz__paramz00 =
					BgL_vz00_15, BUNSPEC);
				BGL_MUTEX_UNLOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00);
				BgL_tmp1789z00_2153;
			}
			return BgL_vz00_15;
		}

	}



/* &bigloo-module-extension-handler-set! */
	obj_t
		BGl_z62bigloozd2modulezd2extensionzd2handlerzd2setz12z70zz__paramz00(obj_t
		BgL_envz00_1840, obj_t BgL_vz00_1841)
	{
		{	/* Llib/param.scm 270 */
			return
				BGl_bigloozd2modulezd2extensionzd2handlerzd2setz12z12zz__paramz00
				(BgL_vz00_1841);
		}

	}



/* bigloo-eval-strict-module */
	BGL_EXPORTED_DEF bool_t
		BGl_bigloozd2evalzd2strictzd2modulezd2zz__paramz00(void)
	{
		{	/* Llib/param.scm 275 */
			return BGl_za2bigloozd2evalzd2strictzd2moduleza2zd2zz__paramz00;
		}

	}



/* &bigloo-eval-strict-module */
	obj_t BGl_z62bigloozd2evalzd2strictzd2modulezb0zz__paramz00(obj_t
		BgL_envz00_1842)
	{
		{	/* Llib/param.scm 275 */
			return BBOOL(BGl_bigloozd2evalzd2strictzd2modulezd2zz__paramz00());
		}

	}



/* bigloo-eval-strict-module-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bigloozd2evalzd2strictzd2modulezd2setz12z12zz__paramz00(bool_t
		BgL_vz00_16)
	{
		{	/* Llib/param.scm 275 */
			BGL_MUTEX_LOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00);
			{	/* Llib/param.scm 275 */
				obj_t BgL_tmp1790z00_2159;

				BgL_tmp1790z00_2159 =
					(BGl_za2bigloozd2evalzd2strictzd2moduleza2zd2zz__paramz00 =
					BgL_vz00_16, BUNSPEC);
				BGL_MUTEX_UNLOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00);
				BgL_tmp1790z00_2159;
			}
			return BBOOL(BgL_vz00_16);
		}

	}



/* &bigloo-eval-strict-module-set! */
	obj_t BGl_z62bigloozd2evalzd2strictzd2modulezd2setz12z70zz__paramz00(obj_t
		BgL_envz00_1843, obj_t BgL_vz00_1844)
	{
		{	/* Llib/param.scm 275 */
			return
				BGl_bigloozd2evalzd2strictzd2modulezd2setz12z12zz__paramz00(CBOOL
				(BgL_vz00_1844));
		}

	}



/* bigloo-library-path */
	BGL_EXPORTED_DEF obj_t BGl_bigloozd2libraryzd2pathz00zz__paramz00(void)
	{
		{	/* Llib/param.scm 280 */
			return BGl_za2bigloozd2libraryzd2pathza2z00zz__paramz00;
		}

	}



/* &bigloo-library-path */
	obj_t BGl_z62bigloozd2libraryzd2pathz62zz__paramz00(obj_t BgL_envz00_1845)
	{
		{	/* Llib/param.scm 280 */
			return BGl_bigloozd2libraryzd2pathz00zz__paramz00();
		}

	}



/* bigloo-library-path-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bigloozd2libraryzd2pathzd2setz12zc0zz__paramz00(obj_t BgL_vz00_17)
	{
		{	/* Llib/param.scm 280 */
			{	/* Llib/param.scm 280 */
				obj_t BgL_top1792z00_2167;

				BgL_top1792z00_2167 = BGL_EXITD_TOP_AS_OBJ();
				BGL_MUTEX_LOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00);
				BGL_EXITD_PUSH_PROTECT(BgL_top1792z00_2167,
					BGl_za2parameterzd2mutexza2zd2zz__paramz00);
				BUNSPEC;
				{	/* Llib/param.scm 280 */
					obj_t BgL_tmp1791z00_2166;

					if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_vz00_17))
						{	/* Llib/param.scm 286 */
							bool_t BgL_test1794z00_2173;

							{
								obj_t BgL_l1078z00_1174;

								BgL_l1078z00_1174 = BgL_vz00_17;
							BgL_zc3z04anonymousza31227ze3z87_1175:
								if (NULLP(BgL_l1078z00_1174))
									{	/* Llib/param.scm 286 */
										BgL_test1794z00_2173 = ((bool_t) 1);
									}
								else
									{	/* Llib/param.scm 286 */
										bool_t BgL_test1796z00_2176;

										{	/* Llib/param.scm 286 */
											obj_t BgL_tmpz00_2177;

											BgL_tmpz00_2177 = CAR(((obj_t) BgL_l1078z00_1174));
											BgL_test1796z00_2176 = STRINGP(BgL_tmpz00_2177);
										}
										if (BgL_test1796z00_2176)
											{
												obj_t BgL_l1078z00_2181;

												BgL_l1078z00_2181 = CDR(((obj_t) BgL_l1078z00_1174));
												BgL_l1078z00_1174 = BgL_l1078z00_2181;
												goto BgL_zc3z04anonymousza31227ze3z87_1175;
											}
										else
											{	/* Llib/param.scm 286 */
												BgL_test1794z00_2173 = ((bool_t) 0);
											}
									}
							}
							if (BgL_test1794z00_2173)
								{	/* Llib/param.scm 286 */
									BgL_tmp1791z00_2166 =
										(BGl_za2bigloozd2libraryzd2pathza2z00zz__paramz00 =
										BgL_vz00_17, BUNSPEC);
								}
							else
								{	/* Llib/param.scm 289 */
									obj_t BgL_arg1218z00_1156;

									{	/* Llib/param.scm 289 */
										obj_t BgL_hook1085z00_1157;

										BgL_hook1085z00_1157 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
										{
											obj_t BgL_l1082z00_1159;
											obj_t BgL_h1083z00_1160;

											BgL_l1082z00_1159 = BgL_vz00_17;
											BgL_h1083z00_1160 = BgL_hook1085z00_1157;
										BgL_zc3z04anonymousza31219ze3z87_1161:
											if (NULLP(BgL_l1082z00_1159))
												{	/* Llib/param.scm 289 */
													BgL_arg1218z00_1156 = CDR(BgL_hook1085z00_1157);
												}
											else
												{	/* Llib/param.scm 289 */
													bool_t BgL_test1798z00_2188;

													{	/* Llib/param.scm 289 */
														bool_t BgL_test1799z00_2189;

														{	/* Llib/param.scm 289 */
															obj_t BgL_tmpz00_2190;

															BgL_tmpz00_2190 =
																CAR(((obj_t) BgL_l1082z00_1159));
															BgL_test1799z00_2189 = STRINGP(BgL_tmpz00_2190);
														}
														if (BgL_test1799z00_2189)
															{	/* Llib/param.scm 289 */
																BgL_test1798z00_2188 = ((bool_t) 0);
															}
														else
															{	/* Llib/param.scm 289 */
																BgL_test1798z00_2188 = ((bool_t) 1);
															}
													}
													if (BgL_test1798z00_2188)
														{	/* Llib/param.scm 289 */
															obj_t BgL_nh1084z00_1166;

															{	/* Llib/param.scm 289 */
																obj_t BgL_arg1225z00_1168;

																BgL_arg1225z00_1168 =
																	CAR(((obj_t) BgL_l1082z00_1159));
																BgL_nh1084z00_1166 =
																	MAKE_YOUNG_PAIR(BgL_arg1225z00_1168, BNIL);
															}
															SET_CDR(BgL_h1083z00_1160, BgL_nh1084z00_1166);
															{	/* Llib/param.scm 289 */
																obj_t BgL_arg1223z00_1167;

																BgL_arg1223z00_1167 =
																	CDR(((obj_t) BgL_l1082z00_1159));
																{
																	obj_t BgL_h1083z00_2201;
																	obj_t BgL_l1082z00_2200;

																	BgL_l1082z00_2200 = BgL_arg1223z00_1167;
																	BgL_h1083z00_2201 = BgL_nh1084z00_1166;
																	BgL_h1083z00_1160 = BgL_h1083z00_2201;
																	BgL_l1082z00_1159 = BgL_l1082z00_2200;
																	goto BgL_zc3z04anonymousza31219ze3z87_1161;
																}
															}
														}
													else
														{	/* Llib/param.scm 289 */
															obj_t BgL_arg1226z00_1169;

															BgL_arg1226z00_1169 =
																CDR(((obj_t) BgL_l1082z00_1159));
															{
																obj_t BgL_l1082z00_2204;

																BgL_l1082z00_2204 = BgL_arg1226z00_1169;
																BgL_l1082z00_1159 = BgL_l1082z00_2204;
																goto BgL_zc3z04anonymousza31219ze3z87_1161;
															}
														}
												}
										}
									}
									BgL_tmp1791z00_2166 =
										(BGl_za2bigloozd2libraryzd2pathza2z00zz__paramz00 =
										BGl_errorz00zz__errorz00(BGl_symbol1672z00zz__paramz00,
											BGl_string1674z00zz__paramz00, BgL_arg1218z00_1156),
										BUNSPEC);
								}
						}
					else
						{	/* Llib/param.scm 284 */
							BgL_tmp1791z00_2166 =
								(BGl_za2bigloozd2libraryzd2pathza2z00zz__paramz00 =
								BGl_errorz00zz__errorz00(BGl_symbol1672z00zz__paramz00,
									BGl_string1675z00zz__paramz00, BgL_vz00_17), BUNSPEC);
						}
					BGL_EXITD_POP_PROTECT(BgL_top1792z00_2167);
					BUNSPEC;
					BGL_MUTEX_UNLOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00);
					BgL_tmp1791z00_2166;
				}
			}
			return BgL_vz00_17;
		}

	}



/* &bigloo-library-path-set! */
	obj_t BGl_z62bigloozd2libraryzd2pathzd2setz12za2zz__paramz00(obj_t
		BgL_envz00_1846, obj_t BgL_vz00_1847)
	{
		{	/* Llib/param.scm 280 */
			{	/* Llib/param.scm 280 */
				obj_t BgL_auxz00_2209;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_vz00_1847))
					{	/* Llib/param.scm 280 */
						BgL_auxz00_2209 = BgL_vz00_1847;
					}
				else
					{
						obj_t BgL_auxz00_2212;

						BgL_auxz00_2212 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1642z00zz__paramz00,
							BINT(11394L), BGl_string1676z00zz__paramz00,
							BGl_string1661z00zz__paramz00, BgL_vz00_1847);
						FAILURE(BgL_auxz00_2212, BFALSE, BFALSE);
					}
				return
					BGl_bigloozd2libraryzd2pathzd2setz12zc0zz__paramz00(BgL_auxz00_2209);
			}
		}

	}



/* bigloo-dns-enable-cache */
	BGL_EXPORTED_DEF bool_t bgl_dns_enable_cache(void)
	{
		{	/* Llib/param.scm 296 */
			return BGl_za2bigloozd2dnszd2enablezd2cacheza2zd2zz__paramz00;
		}

	}



/* &bigloo-dns-enable-cache */
	obj_t BGl_z62bigloozd2dnszd2enablezd2cachezb0zz__paramz00(obj_t
		BgL_envz00_1848)
	{
		{	/* Llib/param.scm 296 */
			return BBOOL(bgl_dns_enable_cache());
		}

	}



/* bigloo-dns-enable-cache-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bigloozd2dnszd2enablezd2cachezd2setz12z12zz__paramz00(bool_t
		BgL_vz00_18)
	{
		{	/* Llib/param.scm 296 */
			BGL_MUTEX_LOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00);
			{	/* Llib/param.scm 296 */
				obj_t BgL_tmp1801z00_2219;

				BgL_tmp1801z00_2219 =
					(BGl_za2bigloozd2dnszd2enablezd2cacheza2zd2zz__paramz00 =
					BgL_vz00_18, BUNSPEC);
				BGL_MUTEX_UNLOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00);
				BgL_tmp1801z00_2219;
			}
			return BBOOL(BgL_vz00_18);
		}

	}



/* &bigloo-dns-enable-cache-set! */
	obj_t BGl_z62bigloozd2dnszd2enablezd2cachezd2setz12z70zz__paramz00(obj_t
		BgL_envz00_1849, obj_t BgL_vz00_1850)
	{
		{	/* Llib/param.scm 296 */
			return
				BGl_bigloozd2dnszd2enablezd2cachezd2setz12z12zz__paramz00(CBOOL
				(BgL_vz00_1850));
		}

	}



/* bigloo-dns-cache-validity-timeout */
	BGL_EXPORTED_DEF long bgl_dns_cache_validity_timeout(void)
	{
		{	/* Llib/param.scm 301 */
			return BGl_za2bigloozd2dnszd2cachezd2validityzd2timeoutza2z00zz__paramz00;
		}

	}



/* &bigloo-dns-cache-validity-timeout */
	obj_t BGl_z62bigloozd2dnszd2cachezd2validityzd2timeoutz62zz__paramz00(obj_t
		BgL_envz00_1851)
	{
		{	/* Llib/param.scm 301 */
			return BINT(bgl_dns_cache_validity_timeout());
		}

	}



/* bigloo-dns-cache-validity-timeout-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bigloozd2dnszd2cachezd2validityzd2timeoutzd2setz12zc0zz__paramz00(long
		BgL_vz00_19)
	{
		{	/* Llib/param.scm 301 */
			BGL_MUTEX_LOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00);
			{	/* Llib/param.scm 301 */
				obj_t BgL_tmp1802z00_2227;

				BgL_tmp1802z00_2227 =
					(BGl_za2bigloozd2dnszd2cachezd2validityzd2timeoutza2z00zz__paramz00 =
					BgL_vz00_19, BUNSPEC);
				BGL_MUTEX_UNLOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00);
				BgL_tmp1802z00_2227;
			}
			return BINT(BgL_vz00_19);
		}

	}



/* &bigloo-dns-cache-validity-timeout-set! */
	obj_t
		BGl_z62bigloozd2dnszd2cachezd2validityzd2timeoutzd2setz12za2zz__paramz00
		(obj_t BgL_envz00_1852, obj_t BgL_vz00_1853)
	{
		{	/* Llib/param.scm 301 */
			{	/* Llib/param.scm 301 */
				long BgL_auxz00_2231;

				{	/* Llib/param.scm 301 */
					obj_t BgL_tmpz00_2232;

					if (INTEGERP(BgL_vz00_1853))
						{	/* Llib/param.scm 301 */
							BgL_tmpz00_2232 = BgL_vz00_1853;
						}
					else
						{
							obj_t BgL_auxz00_2235;

							BgL_auxz00_2235 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1642z00zz__paramz00,
								BINT(12233L), BGl_string1677z00zz__paramz00,
								BGl_string1644z00zz__paramz00, BgL_vz00_1853);
							FAILURE(BgL_auxz00_2235, BFALSE, BFALSE);
						}
					BgL_auxz00_2231 = (long) CINT(BgL_tmpz00_2232);
				}
				return
					BGl_bigloozd2dnszd2cachezd2validityzd2timeoutzd2setz12zc0zz__paramz00
					(BgL_auxz00_2231);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__paramz00(void)
	{
		{	/* Llib/param.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__paramz00(void)
	{
		{	/* Llib/param.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__paramz00(void)
	{
		{	/* Llib/param.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__paramz00(void)
	{
		{	/* Llib/param.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string1678z00zz__paramz00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string1678z00zz__paramz00));
			return
				BGl_modulezd2initializa7ationz75zz__configurez00(35034923L,
				BSTRING_TO_STRING(BGl_string1678z00zz__paramz00));
		}

	}

#ifdef __cplusplus
}
#endif
