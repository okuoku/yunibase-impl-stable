/*===========================================================================*/
/*   (Pp/pp.scm)                                                             */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Pp/pp.scm -indent -o objs/obj_u/Pp/pp.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___PP_TYPE_DEFINITIONS
#define BGL___PP_TYPE_DEFINITIONS

/* object type definitions */

#endif													// BGL___PP_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_symbol2002z00zz__ppz00 = BUNSPEC;
	extern obj_t BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00(obj_t);
	static obj_t BGl_symbol2004z00zz__ppz00 = BUNSPEC;
	static obj_t BGl_symbol2006z00zz__ppz00 = BUNSPEC;
	static obj_t BGl_symbol2008z00zz__ppz00 = BUNSPEC;
	static obj_t BGl_z62ppzd2defunzb0zz__ppz00(obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern bool_t BGl_bigloozd2strictzd2r5rszd2stringszd2zz__paramz00(void);
	static obj_t BGl_genericzd2writezd2zz__ppz00(obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_2minz00zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_symbol2010z00zz__ppz00 = BUNSPEC;
	static obj_t BGl_symbol2012z00zz__ppz00 = BUNSPEC;
	static obj_t BGl_symbol2014z00zz__ppz00 = BUNSPEC;
	static obj_t BGl_symbol2016z00zz__ppz00 = BUNSPEC;
	static obj_t BGl_symbol2018z00zz__ppz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2ppzd2widthza2zd2zz__ppz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31435ze3ze5zz__ppz00(obj_t, obj_t);
	static obj_t BGl_z62ppzd2exprzd2defunz62zz__ppz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__ppz00 = BUNSPEC;
	static obj_t BGl_symbol2020z00zz__ppz00 = BUNSPEC;
	static obj_t BGl_symbol2022z00zz__ppz00 = BUNSPEC;
	static obj_t BGl_symbol2024z00zz__ppz00 = BUNSPEC;
	static obj_t BGl_symbol2026z00zz__ppz00 = BUNSPEC;
	static obj_t BGl_symbol2028z00zz__ppz00 = BUNSPEC;
	static obj_t BGl_outze70ze7zz__ppz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62ppzd2dozb0zz__ppz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62ppzd2casezb0zz__ppz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62wrz62zz__ppz00(obj_t, obj_t, obj_t, obj_t);
	extern obj_t bgl_display_obj(obj_t, obj_t);
	extern obj_t string_for_read(obj_t);
	static obj_t BGl_symbol2030z00zz__ppz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__ppz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_symbol2032z00zz__ppz00 = BUNSPEC;
	static obj_t BGl_symbol2034z00zz__ppz00 = BUNSPEC;
	static obj_t BGl_symbol2036z00zz__ppz00 = BUNSPEC;
	extern obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_z62ppzd2exprzd2listz62zz__ppz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_symbol1950z00zz__ppz00 = BUNSPEC;
	extern bool_t BGl_numberzf3zf3zz__r4_numbers_6_5z00(obj_t);
	static obj_t BGl_toplevelzd2initzd2zz__ppz00(void);
	static obj_t BGl_vectorzd2prefixzd2zz__ppz00(obj_t);
	static obj_t BGl_z62ppzd2exprzb0zz__ppz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62ppzd2definezb0zz__ppz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_revzd2stringzd2appendze70ze7zz__ppz00(obj_t, long);
	static obj_t BGl_symbol1963z00zz__ppz00 = BUNSPEC;
	static obj_t BGl_z62ppzd2generalzb0zz__ppz00(long, obj_t, obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, bool_t, obj_t, obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__ppz00(void);
	static obj_t BGl_genericzd2initzd2zz__ppz00(void);
	extern obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_z62ppzd2downzb0zz__ppz00(obj_t, obj_t, obj_t, obj_t, long,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, long, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__ppz00(void);
	static obj_t BGl_symbol1976z00zz__ppz00 = BUNSPEC;
	extern obj_t BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zz__ppz00(void);
	static obj_t BGl_z62ppzd2letzb0zz__ppz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zz__ppz00(void);
	extern obj_t BGl_2zd2zd2zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62ppzd2ifzb0zz__ppz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62ppzd2commentzb0zz__ppz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_symbol1985z00zz__ppz00 = BUNSPEC;
	static obj_t BGl_symbol1987z00zz__ppz00 = BUNSPEC;
	static obj_t BGl_symbol1989z00zz__ppz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2ppzd2caseza2zd2zz__ppz00 = BUNSPEC;
	extern obj_t string_append_3(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol1991z00zz__ppz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31369ze3ze5zz__ppz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_ppz00zz__ppz00(obj_t, obj_t);
	extern obj_t BGl_classzd2namezd2zz__objectz00(obj_t);
	static obj_t BGl_reversezd2stringzd2appendz00zz__ppz00(obj_t);
	static obj_t BGl_z62ppzd2andzb0zz__ppz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62ppzd2lambdazb0zz__ppz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62ppzd2condzb0zz__ppz00(obj_t, obj_t, obj_t, obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	extern obj_t c_substring(obj_t, long, long);
	extern obj_t BGl_writez00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zz__ppz00(void);
	static obj_t BGl_z62ppz62zz__ppz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62prz62zz__ppz00(obj_t, obj_t, obj_t, obj_t, long, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, long, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31219ze3ze5zz__ppz00(obj_t, obj_t);
	static bool_t BGl_z62readzd2macrozf3z43zz__ppz00(obj_t);
	extern obj_t make_string(long, unsigned char);
	static obj_t BGl_z62indentz62zz__ppz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_openzd2outputzd2stringz00zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_z62ppzd2beginzb0zz__ppz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62readzd2macrozd2prefixz62zz__ppz00(obj_t);
	extern obj_t string_append(obj_t, obj_t);
	extern obj_t BGl_stringzd2upcasezd2zz__r4_strings_6_7z00(obj_t);
	extern obj_t BGl_stringzd2downcasezd2zz__r4_strings_6_7z00(obj_t);
	extern obj_t bgl_close_output_port(obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2000z00zz__ppz00 = BUNSPEC;
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string1951z00zz__ppz00,
		BgL_bgl_string1951za700za7za7_2039za7, "respect", 7);
	      DEFINE_STRING(BGl_string1952z00zz__ppz00,
		BgL_bgl_string1952za700za7za7_2040za7, "pp", 2);
	      DEFINE_STRING(BGl_string1953z00zz__ppz00,
		BgL_bgl_string1953za700za7za7_2041za7, "not an output-port", 18);
	      DEFINE_STRING(BGl_string1954z00zz__ppz00,
		BgL_bgl_string1954za700za7za7_2042za7, "#", 1);
	      DEFINE_STRING(BGl_string1955z00zz__ppz00,
		BgL_bgl_string1955za700za7za7_2043za7, "#0", 2);
	      DEFINE_STRING(BGl_string1956z00zz__ppz00,
		BgL_bgl_string1956za700za7za7_2044za7, "#00", 3);
	      DEFINE_STRING(BGl_string1957z00zz__ppz00,
		BgL_bgl_string1957za700za7za7_2045za7, ")", 1);
	      DEFINE_STRING(BGl_string1958z00zz__ppz00,
		BgL_bgl_string1958za700za7za7_2046za7, ".", 1);
	      DEFINE_STRING(BGl_string1959z00zz__ppz00,
		BgL_bgl_string1959za700za7za7_2047za7, "()", 2);
	      DEFINE_STRING(BGl_string1960z00zz__ppz00,
		BgL_bgl_string1960za700za7za7_2048za7, "(", 1);
	      DEFINE_STRING(BGl_string1961z00zz__ppz00,
		BgL_bgl_string1961za700za7za7_2049za7, " ", 1);
	      DEFINE_STRING(BGl_string1962z00zz__ppz00,
		BgL_bgl_string1962za700za7za7_2050za7, " . ", 3);
	      DEFINE_STRING(BGl_string1964z00zz__ppz00,
		BgL_bgl_string1964za700za7za7_2051za7, "comment", 7);
	      DEFINE_STRING(BGl_string1965z00zz__ppz00,
		BgL_bgl_string1965za700za7za7_2052za7, "#t", 2);
	      DEFINE_STRING(BGl_string1966z00zz__ppz00,
		BgL_bgl_string1966za700za7za7_2053za7, "#f", 2);
	      DEFINE_STRING(BGl_string1967z00zz__ppz00,
		BgL_bgl_string1967za700za7za7_2054za7, "#e", 2);
	      DEFINE_STRING(BGl_string1968z00zz__ppz00,
		BgL_bgl_string1968za700za7za7_2055za7, "#l", 2);
	      DEFINE_STRING(BGl_string1969z00zz__ppz00,
		BgL_bgl_string1969za700za7za7_2056za7, "#u8:", 4);
	      DEFINE_STRING(BGl_string1970z00zz__ppz00,
		BgL_bgl_string1970za700za7za7_2057za7, "#u16:", 5);
	      DEFINE_STRING(BGl_string1971z00zz__ppz00,
		BgL_bgl_string1971za700za7za7_2058za7, "#u32:", 5);
	      DEFINE_STRING(BGl_string1972z00zz__ppz00,
		BgL_bgl_string1972za700za7za7_2059za7, "#s8:", 4);
	      DEFINE_STRING(BGl_string1973z00zz__ppz00,
		BgL_bgl_string1973za700za7za7_2060za7, "#s16:", 5);
	      DEFINE_STRING(BGl_string1974z00zz__ppz00,
		BgL_bgl_string1974za700za7za7_2061za7, "#s32:", 5);
	      DEFINE_STRING(BGl_string1975z00zz__ppz00,
		BgL_bgl_string1975za700za7za7_2062za7, "#z", 2);
	      DEFINE_STRING(BGl_string1977z00zz__ppz00,
		BgL_bgl_string1977za700za7za7_2063za7, "upper", 5);
	      DEFINE_STRING(BGl_string1978z00zz__ppz00,
		BgL_bgl_string1978za700za7za7_2064za7, "#\"", 2);
	      DEFINE_STRING(BGl_string1979z00zz__ppz00,
		BgL_bgl_string1979za700za7za7_2065za7, "\"", 1);
	      DEFINE_STRING(BGl_string1980z00zz__ppz00,
		BgL_bgl_string1980za700za7za7_2066za7, "#[input-port]", 13);
	      DEFINE_STRING(BGl_string1981z00zz__ppz00,
		BgL_bgl_string1981za700za7za7_2067za7, "#[output-port]", 14);
	      DEFINE_STRING(BGl_string1982z00zz__ppz00,
		BgL_bgl_string1982za700za7za7_2068za7, "#[eof-object]", 13);
	      DEFINE_STRING(BGl_string1983z00zz__ppz00,
		BgL_bgl_string1983za700za7za7_2069za7, "#|", 2);
	      DEFINE_STRING(BGl_string1984z00zz__ppz00,
		BgL_bgl_string1984za700za7za7_2070za7, "|", 1);
	      DEFINE_STRING(BGl_string1986z00zz__ppz00,
		BgL_bgl_string1986za700za7za7_2071za7, "quote", 5);
	      DEFINE_STRING(BGl_string1988z00zz__ppz00,
		BgL_bgl_string1988za700za7za7_2072za7, "quasiquote", 10);
	      DEFINE_STRING(BGl_string1990z00zz__ppz00,
		BgL_bgl_string1990za700za7za7_2073za7, "unquote", 7);
	      DEFINE_STRING(BGl_string1992z00zz__ppz00,
		BgL_bgl_string1992za700za7za7_2074za7, "unquote-splicing", 16);
	      DEFINE_STRING(BGl_string1993z00zz__ppz00,
		BgL_bgl_string1993za700za7za7_2075za7, "'", 1);
	      DEFINE_STRING(BGl_string1994z00zz__ppz00,
		BgL_bgl_string1994za700za7za7_2076za7, "`", 1);
	      DEFINE_STRING(BGl_string1995z00zz__ppz00,
		BgL_bgl_string1995za700za7za7_2077za7, ",", 1);
	      DEFINE_STRING(BGl_string1996z00zz__ppz00,
		BgL_bgl_string1996za700za7za7_2078za7, ",@", 2);
	      DEFINE_STRING(BGl_string1997z00zz__ppz00,
		BgL_bgl_string1997za700za7za7_2079za7, "        ", 8);
	      DEFINE_STRING(BGl_string1998z00zz__ppz00,
		BgL_bgl_string1998za700za7za7_2080za7, "\n", 1);
	      DEFINE_STRING(BGl_string1999z00zz__ppz00,
		BgL_bgl_string1999za700za7za7_2081za7, "({}", 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ppzd2envzd2zz__ppz00,
		BgL_bgl_za762ppza762za7za7__ppza702082za7, va_generic_entry,
		BGl_z62ppz62zz__ppz00, BUNSPEC, -2);
	      DEFINE_STRING(BGl_string2001z00zz__ppz00,
		BgL_bgl_string2001za700za7za7_2083za7, "lambda", 6);
	      DEFINE_STRING(BGl_string2003z00zz__ppz00,
		BgL_bgl_string2003za700za7za7_2084za7, "let*", 4);
	      DEFINE_STRING(BGl_string2005z00zz__ppz00,
		BgL_bgl_string2005za700za7za7_2085za7, "letrec", 6);
	      DEFINE_STRING(BGl_string2007z00zz__ppz00,
		BgL_bgl_string2007za700za7za7_2086za7, "define", 6);
	      DEFINE_STRING(BGl_string2009z00zz__ppz00,
		BgL_bgl_string2009za700za7za7_2087za7, "define-inline", 13);
	      DEFINE_STRING(BGl_string2011z00zz__ppz00,
		BgL_bgl_string2011za700za7za7_2088za7, "define-method", 13);
	      DEFINE_STRING(BGl_string2013z00zz__ppz00,
		BgL_bgl_string2013za700za7za7_2089za7, "define-generic", 14);
	      DEFINE_STRING(BGl_string2015z00zz__ppz00,
		BgL_bgl_string2015za700za7za7_2090za7, "module", 6);
	      DEFINE_STRING(BGl_string2017z00zz__ppz00,
		BgL_bgl_string2017za700za7za7_2091za7, "defun", 5);
	      DEFINE_STRING(BGl_string2019z00zz__ppz00,
		BgL_bgl_string2019za700za7za7_2092za7, "de", 2);
	      DEFINE_STRING(BGl_string2021z00zz__ppz00,
		BgL_bgl_string2021za700za7za7_2093za7, "if", 2);
	      DEFINE_STRING(BGl_string2023z00zz__ppz00,
		BgL_bgl_string2023za700za7za7_2094za7, "set!", 4);
	      DEFINE_STRING(BGl_string2025z00zz__ppz00,
		BgL_bgl_string2025za700za7za7_2095za7, "cond", 4);
	      DEFINE_STRING(BGl_string2027z00zz__ppz00,
		BgL_bgl_string2027za700za7za7_2096za7, "case", 4);
	      DEFINE_STRING(BGl_string2029z00zz__ppz00,
		BgL_bgl_string2029za700za7za7_2097za7, "and", 3);
	      DEFINE_STRING(BGl_string2031z00zz__ppz00,
		BgL_bgl_string2031za700za7za7_2098za7, "or", 2);
	      DEFINE_STRING(BGl_string2033z00zz__ppz00,
		BgL_bgl_string2033za700za7za7_2099za7, "let", 3);
	      DEFINE_STRING(BGl_string2035z00zz__ppz00,
		BgL_bgl_string2035za700za7za7_2100za7, "begin", 5);
	      DEFINE_STRING(BGl_string2037z00zz__ppz00,
		BgL_bgl_string2037za700za7za7_2101za7, "do", 2);
	      DEFINE_STRING(BGl_string2038z00zz__ppz00,
		BgL_bgl_string2038za700za7za7_2102za7, "__pp", 4);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_symbol2002z00zz__ppz00));
		     ADD_ROOT((void *) (&BGl_symbol2004z00zz__ppz00));
		     ADD_ROOT((void *) (&BGl_symbol2006z00zz__ppz00));
		     ADD_ROOT((void *) (&BGl_symbol2008z00zz__ppz00));
		     ADD_ROOT((void *) (&BGl_symbol2010z00zz__ppz00));
		     ADD_ROOT((void *) (&BGl_symbol2012z00zz__ppz00));
		     ADD_ROOT((void *) (&BGl_symbol2014z00zz__ppz00));
		     ADD_ROOT((void *) (&BGl_symbol2016z00zz__ppz00));
		     ADD_ROOT((void *) (&BGl_symbol2018z00zz__ppz00));
		     ADD_ROOT((void *) (&BGl_za2ppzd2widthza2zd2zz__ppz00));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__ppz00));
		     ADD_ROOT((void *) (&BGl_symbol2020z00zz__ppz00));
		     ADD_ROOT((void *) (&BGl_symbol2022z00zz__ppz00));
		     ADD_ROOT((void *) (&BGl_symbol2024z00zz__ppz00));
		     ADD_ROOT((void *) (&BGl_symbol2026z00zz__ppz00));
		     ADD_ROOT((void *) (&BGl_symbol2028z00zz__ppz00));
		     ADD_ROOT((void *) (&BGl_symbol2030z00zz__ppz00));
		     ADD_ROOT((void *) (&BGl_symbol2032z00zz__ppz00));
		     ADD_ROOT((void *) (&BGl_symbol2034z00zz__ppz00));
		     ADD_ROOT((void *) (&BGl_symbol2036z00zz__ppz00));
		     ADD_ROOT((void *) (&BGl_symbol1950z00zz__ppz00));
		     ADD_ROOT((void *) (&BGl_symbol1963z00zz__ppz00));
		     ADD_ROOT((void *) (&BGl_symbol1976z00zz__ppz00));
		     ADD_ROOT((void *) (&BGl_symbol1985z00zz__ppz00));
		     ADD_ROOT((void *) (&BGl_symbol1987z00zz__ppz00));
		     ADD_ROOT((void *) (&BGl_symbol1989z00zz__ppz00));
		     ADD_ROOT((void *) (&BGl_za2ppzd2caseza2zd2zz__ppz00));
		     ADD_ROOT((void *) (&BGl_symbol1991z00zz__ppz00));
		     ADD_ROOT((void *) (&BGl_symbol2000z00zz__ppz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__ppz00(long
		BgL_checksumz00_3920, char *BgL_fromz00_3921)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__ppz00))
				{
					BGl_requirezd2initializa7ationz75zz__ppz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__ppz00();
					BGl_cnstzd2initzd2zz__ppz00();
					BGl_importedzd2moduleszd2initz00zz__ppz00();
					return BGl_toplevelzd2initzd2zz__ppz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__ppz00(void)
	{
		{	/* Pp/pp.scm 14 */
			BGl_symbol1950z00zz__ppz00 =
				bstring_to_symbol(BGl_string1951z00zz__ppz00);
			BGl_symbol1963z00zz__ppz00 =
				bstring_to_symbol(BGl_string1964z00zz__ppz00);
			BGl_symbol1976z00zz__ppz00 =
				bstring_to_symbol(BGl_string1977z00zz__ppz00);
			BGl_symbol1985z00zz__ppz00 =
				bstring_to_symbol(BGl_string1986z00zz__ppz00);
			BGl_symbol1987z00zz__ppz00 =
				bstring_to_symbol(BGl_string1988z00zz__ppz00);
			BGl_symbol1989z00zz__ppz00 =
				bstring_to_symbol(BGl_string1990z00zz__ppz00);
			BGl_symbol1991z00zz__ppz00 =
				bstring_to_symbol(BGl_string1992z00zz__ppz00);
			BGl_symbol2000z00zz__ppz00 =
				bstring_to_symbol(BGl_string2001z00zz__ppz00);
			BGl_symbol2002z00zz__ppz00 =
				bstring_to_symbol(BGl_string2003z00zz__ppz00);
			BGl_symbol2004z00zz__ppz00 =
				bstring_to_symbol(BGl_string2005z00zz__ppz00);
			BGl_symbol2006z00zz__ppz00 =
				bstring_to_symbol(BGl_string2007z00zz__ppz00);
			BGl_symbol2008z00zz__ppz00 =
				bstring_to_symbol(BGl_string2009z00zz__ppz00);
			BGl_symbol2010z00zz__ppz00 =
				bstring_to_symbol(BGl_string2011z00zz__ppz00);
			BGl_symbol2012z00zz__ppz00 =
				bstring_to_symbol(BGl_string2013z00zz__ppz00);
			BGl_symbol2014z00zz__ppz00 =
				bstring_to_symbol(BGl_string2015z00zz__ppz00);
			BGl_symbol2016z00zz__ppz00 =
				bstring_to_symbol(BGl_string2017z00zz__ppz00);
			BGl_symbol2018z00zz__ppz00 =
				bstring_to_symbol(BGl_string2019z00zz__ppz00);
			BGl_symbol2020z00zz__ppz00 =
				bstring_to_symbol(BGl_string2021z00zz__ppz00);
			BGl_symbol2022z00zz__ppz00 =
				bstring_to_symbol(BGl_string2023z00zz__ppz00);
			BGl_symbol2024z00zz__ppz00 =
				bstring_to_symbol(BGl_string2025z00zz__ppz00);
			BGl_symbol2026z00zz__ppz00 =
				bstring_to_symbol(BGl_string2027z00zz__ppz00);
			BGl_symbol2028z00zz__ppz00 =
				bstring_to_symbol(BGl_string2029z00zz__ppz00);
			BGl_symbol2030z00zz__ppz00 =
				bstring_to_symbol(BGl_string2031z00zz__ppz00);
			BGl_symbol2032z00zz__ppz00 =
				bstring_to_symbol(BGl_string2033z00zz__ppz00);
			BGl_symbol2034z00zz__ppz00 =
				bstring_to_symbol(BGl_string2035z00zz__ppz00);
			return (BGl_symbol2036z00zz__ppz00 =
				bstring_to_symbol(BGl_string2037z00zz__ppz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__ppz00(void)
	{
		{	/* Pp/pp.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__ppz00(void)
	{
		{	/* Pp/pp.scm 14 */
			BGl_za2ppzd2caseza2zd2zz__ppz00 = BGl_symbol1950z00zz__ppz00;
			return (BGl_za2ppzd2widthza2zd2zz__ppz00 = BINT(80L), BUNSPEC);
		}

	}



/* pp */
	BGL_EXPORTED_DEF obj_t BGl_ppz00zz__ppz00(obj_t BgL_expz00_3,
		obj_t BgL_portz00_4)
	{
		{	/* Pp/pp.scm 65 */
			{	/* Pp/pp.scm 66 */
				obj_t BgL_portz00_1201;

				if (NULLP(BgL_portz00_4))
					{	/* Pp/pp.scm 67 */
						obj_t BgL_tmpz00_3959;

						BgL_tmpz00_3959 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_portz00_1201 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_3959);
					}
				else
					{	/* Pp/pp.scm 68 */
						obj_t BgL_portz00_1207;

						BgL_portz00_1207 = CAR(((obj_t) BgL_portz00_4));
						if (OUTPUT_PORTP(BgL_portz00_1207))
							{	/* Pp/pp.scm 69 */
								BgL_portz00_1201 = BgL_portz00_1207;
							}
						else
							{	/* Pp/pp.scm 69 */
								BgL_portz00_1201 =
									BGl_errorz00zz__errorz00(BGl_string1952z00zz__ppz00,
									BGl_string1953z00zz__ppz00, BgL_portz00_1207);
							}
					}
				{	/* Pp/pp.scm 72 */
					obj_t BgL_zc3z04anonymousza31219ze3z87_3394;

					{
						int BgL_tmpz00_3967;

						BgL_tmpz00_3967 = (int) (1L);
						BgL_zc3z04anonymousza31219ze3z87_3394 =
							MAKE_L_PROCEDURE(BGl_z62zc3z04anonymousza31219ze3ze5zz__ppz00,
							BgL_tmpz00_3967);
					}
					PROCEDURE_L_SET(BgL_zc3z04anonymousza31219ze3z87_3394,
						(int) (0L), BgL_portz00_1201);
					BGl_genericzd2writezd2zz__ppz00(BgL_expz00_3, BFALSE,
						BGl_za2ppzd2widthza2zd2zz__ppz00,
						BgL_zc3z04anonymousza31219ze3z87_3394);
				}
				return BUNSPEC;
			}
		}

	}



/* &pp */
	obj_t BGl_z62ppz62zz__ppz00(obj_t BgL_envz00_3395, obj_t BgL_expz00_3396,
		obj_t BgL_portz00_3397)
	{
		{	/* Pp/pp.scm 65 */
			return BGl_ppz00zz__ppz00(BgL_expz00_3396, BgL_portz00_3397);
		}

	}



/* &<@anonymous:1219> */
	obj_t BGl_z62zc3z04anonymousza31219ze3ze5zz__ppz00(obj_t BgL_envz00_3398,
		obj_t BgL_sz00_3400)
	{
		{	/* Pp/pp.scm 72 */
			{	/* Pp/pp.scm 72 */
				obj_t BgL_portz00_3399;

				BgL_portz00_3399 = PROCEDURE_L_REF(BgL_envz00_3398, (int) (0L));
				{	/* Pp/pp.scm 72 */
					bool_t BgL_tmpz00_3976;

					bgl_display_obj(BgL_sz00_3400, BgL_portz00_3399);
					BgL_tmpz00_3976 = ((bool_t) 1);
					return BBOOL(BgL_tmpz00_3976);
				}
			}
		}

	}



/* vector-prefix */
	obj_t BGl_vectorzd2prefixzd2zz__ppz00(obj_t BgL_objz00_5)
	{
		{	/* Pp/pp.scm 76 */
			{	/* Pp/pp.scm 77 */
				int BgL_tagz00_1209;

				{	/* Pp/pp.scm 77 */
					obj_t BgL_tmpz00_3979;

					BgL_tmpz00_3979 = ((obj_t) BgL_objz00_5);
					BgL_tagz00_1209 = VECTOR_TAG(BgL_tmpz00_3979);
				}
				if (((long) (BgL_tagz00_1209) == 0L))
					{	/* Pp/pp.scm 78 */
						return BGl_string1954z00zz__ppz00;
					}
				else
					{	/* Pp/pp.scm 78 */
						if (((long) (BgL_tagz00_1209) >= 100L))
							{	/* Pp/pp.scm 81 */
								obj_t BgL_arg1225z00_1212;

								{	/* Ieee/number.scm 174 */

									BgL_arg1225z00_1212 =
										BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(BINT
										(BgL_tagz00_1209), BINT(10L));
								}
								return
									string_append(BGl_string1954z00zz__ppz00,
									BgL_arg1225z00_1212);
							}
						else
							{	/* Pp/pp.scm 80 */
								if (((long) (BgL_tagz00_1209) >= 10L))
									{	/* Pp/pp.scm 83 */
										obj_t BgL_arg1227z00_1216;

										{	/* Ieee/number.scm 174 */

											BgL_arg1227z00_1216 =
												BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(BINT
												(BgL_tagz00_1209), BINT(10L));
										}
										return
											string_append(BGl_string1955z00zz__ppz00,
											BgL_arg1227z00_1216);
									}
								else
									{	/* Pp/pp.scm 84 */
										obj_t BgL_arg1228z00_1219;

										{	/* Ieee/number.scm 174 */

											BgL_arg1228z00_1219 =
												BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(BINT
												(BgL_tagz00_1209), BINT(10L));
										}
										return
											string_append(BGl_string1956z00zz__ppz00,
											BgL_arg1228z00_1219);
									}
							}
					}
			}
		}

	}



/* generic-write */
	obj_t BGl_genericzd2writezd2zz__ppz00(obj_t BgL_objz00_6,
		obj_t BgL_displayzf3zf3_7, obj_t BgL_widthz00_8, obj_t BgL_outputz00_9)
	{
		{	/* Pp/pp.scm 112 */
			{
				obj_t BgL_objz00_1461;
				long BgL_colz00_1462;

				if (CBOOL(BgL_widthz00_8))
					{	/* Pp/pp.scm 463 */
						obj_t BgL_arg1229z00_1228;
						obj_t BgL_arg1230z00_1229;

						BgL_arg1229z00_1228 = make_string(1L, ((unsigned char) 10));
						BgL_objz00_1461 = BgL_objz00_6;
						BgL_colz00_1462 = 0L;
						{	/* Pp/pp.scm 249 */
							obj_t BgL_ppzd2dozd2_3410;
							obj_t BgL_ppzd2beginzd2_3411;
							obj_t BgL_ppzd2andzd2_3413;
							obj_t BgL_ppzd2casezd2_3414;
							obj_t BgL_ppzd2condzd2_3415;
							obj_t BgL_ppzd2ifzd2_3416;
							obj_t BgL_ppzd2commentzd2_3407;
							obj_t BgL_ppzd2lambdazd2_3420;
							obj_t BgL_ppzd2letzd2_3412;
							obj_t BgL_ppzd2defunzd2_3418;
							obj_t BgL_ppzd2definezd2_3419;
							obj_t BgL_ppzd2exprzd2defunz00_3417;
							obj_t BgL_ppzd2exprzd2listz00_3409;
							obj_t BgL_ppzd2exprzd2_3408;

							BgL_ppzd2dozd2_3410 =
								MAKE_FX_PROCEDURE(BGl_z62ppzd2dozb0zz__ppz00,
								(int) (3L), (int) (18L));
							BgL_ppzd2beginzd2_3411 =
								MAKE_FX_PROCEDURE(BGl_z62ppzd2beginzb0zz__ppz00,
								(int) (3L), (int) (18L));
							BgL_ppzd2andzd2_3413 =
								MAKE_FX_PROCEDURE(BGl_z62ppzd2andzb0zz__ppz00,
								(int) (3L), (int) (18L));
							BgL_ppzd2casezd2_3414 =
								MAKE_FX_PROCEDURE(BGl_z62ppzd2casezb0zz__ppz00,
								(int) (3L), (int) (18L));
							BgL_ppzd2condzd2_3415 =
								MAKE_FX_PROCEDURE(BGl_z62ppzd2condzb0zz__ppz00,
								(int) (3L), (int) (18L));
							BgL_ppzd2ifzd2_3416 =
								MAKE_FX_PROCEDURE(BGl_z62ppzd2ifzb0zz__ppz00,
								(int) (3L), (int) (18L));
							BgL_ppzd2commentzd2_3407 =
								MAKE_FX_PROCEDURE(BGl_z62ppzd2commentzb0zz__ppz00,
								(int) (3L), (int) (18L));
							BgL_ppzd2lambdazd2_3420 =
								MAKE_FX_PROCEDURE(BGl_z62ppzd2lambdazb0zz__ppz00,
								(int) (3L), (int) (18L));
							BgL_ppzd2letzd2_3412 =
								MAKE_FX_PROCEDURE(BGl_z62ppzd2letzb0zz__ppz00,
								(int) (3L), (int) (18L));
							BgL_ppzd2defunzd2_3418 =
								MAKE_FX_PROCEDURE(BGl_z62ppzd2defunzb0zz__ppz00,
								(int) (3L), (int) (18L));
							BgL_ppzd2definezd2_3419 =
								MAKE_FX_PROCEDURE(BGl_z62ppzd2definezb0zz__ppz00,
								(int) (3L), (int) (18L));
							BgL_ppzd2exprzd2defunz00_3417 =
								MAKE_FX_PROCEDURE(BGl_z62ppzd2exprzd2defunz62zz__ppz00,
								(int) (3L), (int) (18L));
							BgL_ppzd2exprzd2listz00_3409 =
								MAKE_FX_PROCEDURE(BGl_z62ppzd2exprzd2listz62zz__ppz00,
								(int) (3L), (int) (18L));
							BgL_ppzd2exprzd2_3408 =
								MAKE_FX_PROCEDURE(BGl_z62ppzd2exprzb0zz__ppz00,
								(int) (3L), (int) (18L));
							PROCEDURE_SET(BgL_ppzd2dozd2_3410, (int) (0L), BINT(2L));
							PROCEDURE_SET(BgL_ppzd2dozd2_3410,
								(int) (1L), BgL_ppzd2exprzd2defunz00_3417);
							PROCEDURE_SET(BgL_ppzd2dozd2_3410,
								(int) (2L), BgL_ppzd2commentzd2_3407);
							PROCEDURE_SET(BgL_ppzd2dozd2_3410,
								(int) (3L), BgL_ppzd2beginzd2_3411);
							PROCEDURE_SET(BgL_ppzd2dozd2_3410,
								(int) (4L), BgL_ppzd2letzd2_3412);
							PROCEDURE_SET(BgL_ppzd2dozd2_3410,
								(int) (5L), BgL_ppzd2andzd2_3413);
							PROCEDURE_SET(BgL_ppzd2dozd2_3410,
								(int) (6L), BgL_ppzd2casezd2_3414);
							PROCEDURE_SET(BgL_ppzd2dozd2_3410,
								(int) (7L), BgL_ppzd2condzd2_3415);
							PROCEDURE_SET(BgL_ppzd2dozd2_3410,
								(int) (8L), BgL_ppzd2ifzd2_3416);
							PROCEDURE_SET(BgL_ppzd2dozd2_3410,
								(int) (9L), BgL_ppzd2defunzd2_3418);
							PROCEDURE_SET(BgL_ppzd2dozd2_3410,
								(int) (10L), BgL_ppzd2definezd2_3419);
							PROCEDURE_SET(BgL_ppzd2dozd2_3410,
								(int) (11L), BgL_ppzd2lambdazd2_3420);
							PROCEDURE_SET(BgL_ppzd2dozd2_3410, (int) (12L), BINT(5L));
							PROCEDURE_SET(BgL_ppzd2dozd2_3410, (int) (13L), BgL_widthz00_8);
							PROCEDURE_SET(BgL_ppzd2dozd2_3410,
								(int) (14L), BgL_displayzf3zf3_7);
							PROCEDURE_SET(BgL_ppzd2dozd2_3410, (int) (15L), BgL_outputz00_9);
							PROCEDURE_SET(BgL_ppzd2dozd2_3410,
								(int) (16L), BgL_ppzd2exprzd2listz00_3409);
							PROCEDURE_SET(BgL_ppzd2dozd2_3410,
								(int) (17L), BgL_ppzd2exprzd2_3408);
							PROCEDURE_SET(BgL_ppzd2beginzd2_3411, (int) (0L), BINT(2L));
							PROCEDURE_SET(BgL_ppzd2beginzd2_3411,
								(int) (1L), BgL_ppzd2exprzd2defunz00_3417);
							PROCEDURE_SET(BgL_ppzd2beginzd2_3411, (int) (2L), BgL_widthz00_8);
							PROCEDURE_SET(BgL_ppzd2beginzd2_3411,
								(int) (3L), BgL_displayzf3zf3_7);
							PROCEDURE_SET(BgL_ppzd2beginzd2_3411,
								(int) (4L), BgL_ppzd2exprzd2listz00_3409);
							PROCEDURE_SET(BgL_ppzd2beginzd2_3411,
								(int) (5L), BgL_ppzd2commentzd2_3407);
							PROCEDURE_SET(BgL_ppzd2beginzd2_3411,
								(int) (6L), BgL_ppzd2dozd2_3410);
							PROCEDURE_SET(BgL_ppzd2beginzd2_3411,
								(int) (7L), BgL_ppzd2letzd2_3412);
							PROCEDURE_SET(BgL_ppzd2beginzd2_3411,
								(int) (8L), BgL_ppzd2andzd2_3413);
							PROCEDURE_SET(BgL_ppzd2beginzd2_3411,
								(int) (9L), BgL_ppzd2casezd2_3414);
							PROCEDURE_SET(BgL_ppzd2beginzd2_3411,
								(int) (10L), BgL_ppzd2condzd2_3415);
							PROCEDURE_SET(BgL_ppzd2beginzd2_3411,
								(int) (11L), BgL_ppzd2ifzd2_3416);
							PROCEDURE_SET(BgL_ppzd2beginzd2_3411,
								(int) (12L), BgL_ppzd2defunzd2_3418);
							PROCEDURE_SET(BgL_ppzd2beginzd2_3411,
								(int) (13L), BgL_ppzd2definezd2_3419);
							PROCEDURE_SET(BgL_ppzd2beginzd2_3411,
								(int) (14L), BgL_ppzd2lambdazd2_3420);
							PROCEDURE_SET(BgL_ppzd2beginzd2_3411,
								(int) (15L), BgL_outputz00_9);
							PROCEDURE_SET(BgL_ppzd2beginzd2_3411, (int) (16L), BINT(5L));
							PROCEDURE_SET(BgL_ppzd2beginzd2_3411,
								(int) (17L), BgL_ppzd2exprzd2_3408);
							PROCEDURE_SET(BgL_ppzd2andzd2_3413, (int) (0L), BINT(2L));
							PROCEDURE_SET(BgL_ppzd2andzd2_3413,
								(int) (1L), BgL_ppzd2exprzd2defunz00_3417);
							PROCEDURE_SET(BgL_ppzd2andzd2_3413, (int) (2L), BgL_widthz00_8);
							PROCEDURE_SET(BgL_ppzd2andzd2_3413,
								(int) (3L), BgL_displayzf3zf3_7);
							PROCEDURE_SET(BgL_ppzd2andzd2_3413,
								(int) (4L), BgL_ppzd2exprzd2listz00_3409);
							PROCEDURE_SET(BgL_ppzd2andzd2_3413,
								(int) (5L), BgL_ppzd2commentzd2_3407);
							PROCEDURE_SET(BgL_ppzd2andzd2_3413,
								(int) (6L), BgL_ppzd2dozd2_3410);
							PROCEDURE_SET(BgL_ppzd2andzd2_3413,
								(int) (7L), BgL_ppzd2beginzd2_3411);
							PROCEDURE_SET(BgL_ppzd2andzd2_3413,
								(int) (8L), BgL_ppzd2letzd2_3412);
							PROCEDURE_SET(BgL_ppzd2andzd2_3413,
								(int) (9L), BgL_ppzd2casezd2_3414);
							PROCEDURE_SET(BgL_ppzd2andzd2_3413,
								(int) (10L), BgL_ppzd2condzd2_3415);
							PROCEDURE_SET(BgL_ppzd2andzd2_3413,
								(int) (11L), BgL_ppzd2ifzd2_3416);
							PROCEDURE_SET(BgL_ppzd2andzd2_3413,
								(int) (12L), BgL_ppzd2defunzd2_3418);
							PROCEDURE_SET(BgL_ppzd2andzd2_3413,
								(int) (13L), BgL_ppzd2definezd2_3419);
							PROCEDURE_SET(BgL_ppzd2andzd2_3413,
								(int) (14L), BgL_ppzd2lambdazd2_3420);
							PROCEDURE_SET(BgL_ppzd2andzd2_3413, (int) (15L), BINT(5L));
							PROCEDURE_SET(BgL_ppzd2andzd2_3413, (int) (16L), BgL_outputz00_9);
							PROCEDURE_SET(BgL_ppzd2andzd2_3413,
								(int) (17L), BgL_ppzd2exprzd2_3408);
							PROCEDURE_SET(BgL_ppzd2casezd2_3414, (int) (0L), BINT(2L));
							PROCEDURE_SET(BgL_ppzd2casezd2_3414,
								(int) (1L), BgL_ppzd2exprzd2defunz00_3417);
							PROCEDURE_SET(BgL_ppzd2casezd2_3414, (int) (2L), BgL_widthz00_8);
							PROCEDURE_SET(BgL_ppzd2casezd2_3414,
								(int) (3L), BgL_displayzf3zf3_7);
							PROCEDURE_SET(BgL_ppzd2casezd2_3414,
								(int) (4L), BgL_ppzd2commentzd2_3407);
							PROCEDURE_SET(BgL_ppzd2casezd2_3414,
								(int) (5L), BgL_ppzd2dozd2_3410);
							PROCEDURE_SET(BgL_ppzd2casezd2_3414,
								(int) (6L), BgL_ppzd2beginzd2_3411);
							PROCEDURE_SET(BgL_ppzd2casezd2_3414,
								(int) (7L), BgL_ppzd2letzd2_3412);
							PROCEDURE_SET(BgL_ppzd2casezd2_3414,
								(int) (8L), BgL_ppzd2andzd2_3413);
							PROCEDURE_SET(BgL_ppzd2casezd2_3414,
								(int) (9L), BgL_ppzd2condzd2_3415);
							PROCEDURE_SET(BgL_ppzd2casezd2_3414,
								(int) (10L), BgL_ppzd2ifzd2_3416);
							PROCEDURE_SET(BgL_ppzd2casezd2_3414,
								(int) (11L), BgL_ppzd2defunzd2_3418);
							PROCEDURE_SET(BgL_ppzd2casezd2_3414,
								(int) (12L), BgL_ppzd2definezd2_3419);
							PROCEDURE_SET(BgL_ppzd2casezd2_3414,
								(int) (13L), BgL_ppzd2lambdazd2_3420);
							PROCEDURE_SET(BgL_ppzd2casezd2_3414,
								(int) (14L), BgL_outputz00_9);
							PROCEDURE_SET(BgL_ppzd2casezd2_3414, (int) (15L), BINT(5L));
							PROCEDURE_SET(BgL_ppzd2casezd2_3414,
								(int) (16L), BgL_ppzd2exprzd2_3408);
							PROCEDURE_SET(BgL_ppzd2casezd2_3414,
								(int) (17L), BgL_ppzd2exprzd2listz00_3409);
							PROCEDURE_SET(BgL_ppzd2condzd2_3415, (int) (0L), BINT(2L));
							PROCEDURE_SET(BgL_ppzd2condzd2_3415,
								(int) (1L), BgL_ppzd2exprzd2defunz00_3417);
							PROCEDURE_SET(BgL_ppzd2condzd2_3415, (int) (2L), BgL_widthz00_8);
							PROCEDURE_SET(BgL_ppzd2condzd2_3415,
								(int) (3L), BgL_displayzf3zf3_7);
							PROCEDURE_SET(BgL_ppzd2condzd2_3415,
								(int) (4L), BgL_ppzd2commentzd2_3407);
							PROCEDURE_SET(BgL_ppzd2condzd2_3415,
								(int) (5L), BgL_ppzd2dozd2_3410);
							PROCEDURE_SET(BgL_ppzd2condzd2_3415,
								(int) (6L), BgL_ppzd2beginzd2_3411);
							PROCEDURE_SET(BgL_ppzd2condzd2_3415,
								(int) (7L), BgL_ppzd2letzd2_3412);
							PROCEDURE_SET(BgL_ppzd2condzd2_3415,
								(int) (8L), BgL_ppzd2andzd2_3413);
							PROCEDURE_SET(BgL_ppzd2condzd2_3415,
								(int) (9L), BgL_ppzd2casezd2_3414);
							PROCEDURE_SET(BgL_ppzd2condzd2_3415,
								(int) (10L), BgL_ppzd2ifzd2_3416);
							PROCEDURE_SET(BgL_ppzd2condzd2_3415,
								(int) (11L), BgL_ppzd2defunzd2_3418);
							PROCEDURE_SET(BgL_ppzd2condzd2_3415,
								(int) (12L), BgL_ppzd2definezd2_3419);
							PROCEDURE_SET(BgL_ppzd2condzd2_3415,
								(int) (13L), BgL_ppzd2lambdazd2_3420);
							PROCEDURE_SET(BgL_ppzd2condzd2_3415, (int) (14L), BINT(5L));
							PROCEDURE_SET(BgL_ppzd2condzd2_3415,
								(int) (15L), BgL_ppzd2exprzd2_3408);
							PROCEDURE_SET(BgL_ppzd2condzd2_3415,
								(int) (16L), BgL_outputz00_9);
							PROCEDURE_SET(BgL_ppzd2condzd2_3415,
								(int) (17L), BgL_ppzd2exprzd2listz00_3409);
							PROCEDURE_SET(BgL_ppzd2ifzd2_3416, (int) (0L), BINT(2L));
							PROCEDURE_SET(BgL_ppzd2ifzd2_3416,
								(int) (1L), BgL_ppzd2exprzd2defunz00_3417);
							PROCEDURE_SET(BgL_ppzd2ifzd2_3416, (int) (2L), BgL_widthz00_8);
							PROCEDURE_SET(BgL_ppzd2ifzd2_3416,
								(int) (3L), BgL_displayzf3zf3_7);
							PROCEDURE_SET(BgL_ppzd2ifzd2_3416,
								(int) (4L), BgL_ppzd2exprzd2listz00_3409);
							PROCEDURE_SET(BgL_ppzd2ifzd2_3416,
								(int) (5L), BgL_ppzd2commentzd2_3407);
							PROCEDURE_SET(BgL_ppzd2ifzd2_3416,
								(int) (6L), BgL_ppzd2dozd2_3410);
							PROCEDURE_SET(BgL_ppzd2ifzd2_3416,
								(int) (7L), BgL_ppzd2beginzd2_3411);
							PROCEDURE_SET(BgL_ppzd2ifzd2_3416,
								(int) (8L), BgL_ppzd2letzd2_3412);
							PROCEDURE_SET(BgL_ppzd2ifzd2_3416,
								(int) (9L), BgL_ppzd2andzd2_3413);
							PROCEDURE_SET(BgL_ppzd2ifzd2_3416,
								(int) (10L), BgL_ppzd2casezd2_3414);
							PROCEDURE_SET(BgL_ppzd2ifzd2_3416,
								(int) (11L), BgL_ppzd2condzd2_3415);
							PROCEDURE_SET(BgL_ppzd2ifzd2_3416,
								(int) (12L), BgL_ppzd2defunzd2_3418);
							PROCEDURE_SET(BgL_ppzd2ifzd2_3416,
								(int) (13L), BgL_ppzd2definezd2_3419);
							PROCEDURE_SET(BgL_ppzd2ifzd2_3416,
								(int) (14L), BgL_ppzd2lambdazd2_3420);
							PROCEDURE_SET(BgL_ppzd2ifzd2_3416, (int) (15L), BgL_outputz00_9);
							PROCEDURE_SET(BgL_ppzd2ifzd2_3416, (int) (16L), BINT(5L));
							PROCEDURE_SET(BgL_ppzd2ifzd2_3416,
								(int) (17L), BgL_ppzd2exprzd2_3408);
							PROCEDURE_SET(BgL_ppzd2commentzd2_3407,
								(int) (0L), BgL_ppzd2exprzd2defunz00_3417);
							PROCEDURE_SET(BgL_ppzd2commentzd2_3407,
								(int) (1L), BgL_ppzd2exprzd2listz00_3409);
							PROCEDURE_SET(BgL_ppzd2commentzd2_3407,
								(int) (2L), BgL_ppzd2dozd2_3410);
							PROCEDURE_SET(BgL_ppzd2commentzd2_3407,
								(int) (3L), BgL_ppzd2beginzd2_3411);
							PROCEDURE_SET(BgL_ppzd2commentzd2_3407,
								(int) (4L), BgL_ppzd2letzd2_3412);
							PROCEDURE_SET(BgL_ppzd2commentzd2_3407,
								(int) (5L), BgL_ppzd2andzd2_3413);
							PROCEDURE_SET(BgL_ppzd2commentzd2_3407,
								(int) (6L), BgL_ppzd2casezd2_3414);
							PROCEDURE_SET(BgL_ppzd2commentzd2_3407,
								(int) (7L), BgL_ppzd2condzd2_3415);
							PROCEDURE_SET(BgL_ppzd2commentzd2_3407,
								(int) (8L), BgL_ppzd2ifzd2_3416);
							PROCEDURE_SET(BgL_ppzd2commentzd2_3407,
								(int) (9L), BgL_ppzd2defunzd2_3418);
							PROCEDURE_SET(BgL_ppzd2commentzd2_3407,
								(int) (10L), BgL_ppzd2definezd2_3419);
							PROCEDURE_SET(BgL_ppzd2commentzd2_3407,
								(int) (11L), BgL_ppzd2lambdazd2_3420);
							PROCEDURE_SET(BgL_ppzd2commentzd2_3407, (int) (12L), BINT(5L));
							PROCEDURE_SET(BgL_ppzd2commentzd2_3407,
								(int) (13L), BgL_widthz00_8);
							PROCEDURE_SET(BgL_ppzd2commentzd2_3407,
								(int) (14L), BgL_displayzf3zf3_7);
							PROCEDURE_SET(BgL_ppzd2commentzd2_3407, (int) (15L), BINT(2L));
							PROCEDURE_SET(BgL_ppzd2commentzd2_3407,
								(int) (16L), BgL_outputz00_9);
							PROCEDURE_SET(BgL_ppzd2commentzd2_3407,
								(int) (17L), BgL_ppzd2exprzd2_3408);
							PROCEDURE_SET(BgL_ppzd2lambdazd2_3420, (int) (0L), BINT(2L));
							PROCEDURE_SET(BgL_ppzd2lambdazd2_3420,
								(int) (1L), BgL_ppzd2exprzd2defunz00_3417);
							PROCEDURE_SET(BgL_ppzd2lambdazd2_3420,
								(int) (2L), BgL_widthz00_8);
							PROCEDURE_SET(BgL_ppzd2lambdazd2_3420,
								(int) (3L), BgL_displayzf3zf3_7);
							PROCEDURE_SET(BgL_ppzd2lambdazd2_3420,
								(int) (4L), BgL_ppzd2exprzd2listz00_3409);
							PROCEDURE_SET(BgL_ppzd2lambdazd2_3420,
								(int) (5L), BgL_ppzd2commentzd2_3407);
							PROCEDURE_SET(BgL_ppzd2lambdazd2_3420,
								(int) (6L), BgL_ppzd2dozd2_3410);
							PROCEDURE_SET(BgL_ppzd2lambdazd2_3420,
								(int) (7L), BgL_ppzd2beginzd2_3411);
							PROCEDURE_SET(BgL_ppzd2lambdazd2_3420,
								(int) (8L), BgL_ppzd2letzd2_3412);
							PROCEDURE_SET(BgL_ppzd2lambdazd2_3420,
								(int) (9L), BgL_ppzd2andzd2_3413);
							PROCEDURE_SET(BgL_ppzd2lambdazd2_3420,
								(int) (10L), BgL_ppzd2casezd2_3414);
							PROCEDURE_SET(BgL_ppzd2lambdazd2_3420,
								(int) (11L), BgL_ppzd2condzd2_3415);
							PROCEDURE_SET(BgL_ppzd2lambdazd2_3420,
								(int) (12L), BgL_ppzd2ifzd2_3416);
							PROCEDURE_SET(BgL_ppzd2lambdazd2_3420,
								(int) (13L), BgL_ppzd2defunzd2_3418);
							PROCEDURE_SET(BgL_ppzd2lambdazd2_3420,
								(int) (14L), BgL_ppzd2definezd2_3419);
							PROCEDURE_SET(BgL_ppzd2lambdazd2_3420, (int) (15L), BINT(5L));
							PROCEDURE_SET(BgL_ppzd2lambdazd2_3420,
								(int) (16L), BgL_outputz00_9);
							PROCEDURE_SET(BgL_ppzd2lambdazd2_3420,
								(int) (17L), BgL_ppzd2exprzd2_3408);
							PROCEDURE_SET(BgL_ppzd2letzd2_3412, (int) (0L), BINT(2L));
							PROCEDURE_SET(BgL_ppzd2letzd2_3412,
								(int) (1L), BgL_ppzd2exprzd2defunz00_3417);
							PROCEDURE_SET(BgL_ppzd2letzd2_3412,
								(int) (2L), BgL_ppzd2commentzd2_3407);
							PROCEDURE_SET(BgL_ppzd2letzd2_3412,
								(int) (3L), BgL_ppzd2dozd2_3410);
							PROCEDURE_SET(BgL_ppzd2letzd2_3412,
								(int) (4L), BgL_ppzd2beginzd2_3411);
							PROCEDURE_SET(BgL_ppzd2letzd2_3412,
								(int) (5L), BgL_ppzd2andzd2_3413);
							PROCEDURE_SET(BgL_ppzd2letzd2_3412,
								(int) (6L), BgL_ppzd2casezd2_3414);
							PROCEDURE_SET(BgL_ppzd2letzd2_3412,
								(int) (7L), BgL_ppzd2condzd2_3415);
							PROCEDURE_SET(BgL_ppzd2letzd2_3412,
								(int) (8L), BgL_ppzd2ifzd2_3416);
							PROCEDURE_SET(BgL_ppzd2letzd2_3412,
								(int) (9L), BgL_ppzd2defunzd2_3418);
							PROCEDURE_SET(BgL_ppzd2letzd2_3412,
								(int) (10L), BgL_ppzd2definezd2_3419);
							PROCEDURE_SET(BgL_ppzd2letzd2_3412,
								(int) (11L), BgL_ppzd2lambdazd2_3420);
							PROCEDURE_SET(BgL_ppzd2letzd2_3412, (int) (12L), BINT(5L));
							PROCEDURE_SET(BgL_ppzd2letzd2_3412, (int) (13L), BgL_widthz00_8);
							PROCEDURE_SET(BgL_ppzd2letzd2_3412,
								(int) (14L), BgL_displayzf3zf3_7);
							PROCEDURE_SET(BgL_ppzd2letzd2_3412, (int) (15L), BgL_outputz00_9);
							PROCEDURE_SET(BgL_ppzd2letzd2_3412,
								(int) (16L), BgL_ppzd2exprzd2listz00_3409);
							PROCEDURE_SET(BgL_ppzd2letzd2_3412,
								(int) (17L), BgL_ppzd2exprzd2_3408);
							PROCEDURE_SET(BgL_ppzd2defunzd2_3418, (int) (0L), BINT(2L));
							PROCEDURE_SET(BgL_ppzd2defunzd2_3418,
								(int) (1L), BgL_ppzd2exprzd2listz00_3409);
							PROCEDURE_SET(BgL_ppzd2defunzd2_3418,
								(int) (2L), BgL_ppzd2commentzd2_3407);
							PROCEDURE_SET(BgL_ppzd2defunzd2_3418,
								(int) (3L), BgL_ppzd2dozd2_3410);
							PROCEDURE_SET(BgL_ppzd2defunzd2_3418,
								(int) (4L), BgL_ppzd2beginzd2_3411);
							PROCEDURE_SET(BgL_ppzd2defunzd2_3418,
								(int) (5L), BgL_ppzd2letzd2_3412);
							PROCEDURE_SET(BgL_ppzd2defunzd2_3418,
								(int) (6L), BgL_ppzd2andzd2_3413);
							PROCEDURE_SET(BgL_ppzd2defunzd2_3418,
								(int) (7L), BgL_ppzd2casezd2_3414);
							PROCEDURE_SET(BgL_ppzd2defunzd2_3418,
								(int) (8L), BgL_ppzd2condzd2_3415);
							PROCEDURE_SET(BgL_ppzd2defunzd2_3418,
								(int) (9L), BgL_ppzd2ifzd2_3416);
							PROCEDURE_SET(BgL_ppzd2defunzd2_3418,
								(int) (10L), BgL_ppzd2definezd2_3419);
							PROCEDURE_SET(BgL_ppzd2defunzd2_3418,
								(int) (11L), BgL_ppzd2lambdazd2_3420);
							PROCEDURE_SET(BgL_ppzd2defunzd2_3418, (int) (12L), BINT(5L));
							PROCEDURE_SET(BgL_ppzd2defunzd2_3418,
								(int) (13L), BgL_widthz00_8);
							PROCEDURE_SET(BgL_ppzd2defunzd2_3418,
								(int) (14L), BgL_displayzf3zf3_7);
							PROCEDURE_SET(BgL_ppzd2defunzd2_3418,
								(int) (15L), BgL_ppzd2exprzd2defunz00_3417);
							PROCEDURE_SET(BgL_ppzd2defunzd2_3418,
								(int) (16L), BgL_ppzd2exprzd2_3408);
							PROCEDURE_SET(BgL_ppzd2defunzd2_3418,
								(int) (17L), BgL_outputz00_9);
							PROCEDURE_SET(BgL_ppzd2definezd2_3419, (int) (0L), BINT(2L));
							PROCEDURE_SET(BgL_ppzd2definezd2_3419,
								(int) (1L), BgL_ppzd2exprzd2defunz00_3417);
							PROCEDURE_SET(BgL_ppzd2definezd2_3419,
								(int) (2L), BgL_ppzd2commentzd2_3407);
							PROCEDURE_SET(BgL_ppzd2definezd2_3419,
								(int) (3L), BgL_ppzd2dozd2_3410);
							PROCEDURE_SET(BgL_ppzd2definezd2_3419,
								(int) (4L), BgL_ppzd2beginzd2_3411);
							PROCEDURE_SET(BgL_ppzd2definezd2_3419,
								(int) (5L), BgL_ppzd2letzd2_3412);
							PROCEDURE_SET(BgL_ppzd2definezd2_3419,
								(int) (6L), BgL_ppzd2andzd2_3413);
							PROCEDURE_SET(BgL_ppzd2definezd2_3419,
								(int) (7L), BgL_ppzd2casezd2_3414);
							PROCEDURE_SET(BgL_ppzd2definezd2_3419,
								(int) (8L), BgL_ppzd2condzd2_3415);
							PROCEDURE_SET(BgL_ppzd2definezd2_3419,
								(int) (9L), BgL_ppzd2ifzd2_3416);
							PROCEDURE_SET(BgL_ppzd2definezd2_3419,
								(int) (10L), BgL_ppzd2defunzd2_3418);
							PROCEDURE_SET(BgL_ppzd2definezd2_3419,
								(int) (11L), BgL_ppzd2lambdazd2_3420);
							PROCEDURE_SET(BgL_ppzd2definezd2_3419, (int) (12L), BINT(5L));
							PROCEDURE_SET(BgL_ppzd2definezd2_3419,
								(int) (13L), BgL_widthz00_8);
							PROCEDURE_SET(BgL_ppzd2definezd2_3419,
								(int) (14L), BgL_displayzf3zf3_7);
							PROCEDURE_SET(BgL_ppzd2definezd2_3419,
								(int) (15L), BgL_ppzd2exprzd2listz00_3409);
							PROCEDURE_SET(BgL_ppzd2definezd2_3419,
								(int) (16L), BgL_ppzd2exprzd2_3408);
							PROCEDURE_SET(BgL_ppzd2definezd2_3419,
								(int) (17L), BgL_outputz00_9);
							PROCEDURE_SET(BgL_ppzd2exprzd2defunz00_3417,
								(int) (0L), BINT(2L));
							PROCEDURE_SET(BgL_ppzd2exprzd2defunz00_3417,
								(int) (1L), BgL_widthz00_8);
							PROCEDURE_SET(BgL_ppzd2exprzd2defunz00_3417,
								(int) (2L), BgL_displayzf3zf3_7);
							PROCEDURE_SET(BgL_ppzd2exprzd2defunz00_3417,
								(int) (3L), BgL_ppzd2exprzd2listz00_3409);
							PROCEDURE_SET(BgL_ppzd2exprzd2defunz00_3417,
								(int) (4L), BgL_ppzd2commentzd2_3407);
							PROCEDURE_SET(BgL_ppzd2exprzd2defunz00_3417,
								(int) (5L), BgL_ppzd2dozd2_3410);
							PROCEDURE_SET(BgL_ppzd2exprzd2defunz00_3417,
								(int) (6L), BgL_ppzd2beginzd2_3411);
							PROCEDURE_SET(BgL_ppzd2exprzd2defunz00_3417,
								(int) (7L), BgL_ppzd2letzd2_3412);
							PROCEDURE_SET(BgL_ppzd2exprzd2defunz00_3417,
								(int) (8L), BgL_ppzd2andzd2_3413);
							PROCEDURE_SET(BgL_ppzd2exprzd2defunz00_3417,
								(int) (9L), BgL_ppzd2casezd2_3414);
							PROCEDURE_SET(BgL_ppzd2exprzd2defunz00_3417,
								(int) (10L), BgL_ppzd2condzd2_3415);
							PROCEDURE_SET(BgL_ppzd2exprzd2defunz00_3417,
								(int) (11L), BgL_ppzd2ifzd2_3416);
							PROCEDURE_SET(BgL_ppzd2exprzd2defunz00_3417,
								(int) (12L), BgL_ppzd2defunzd2_3418);
							PROCEDURE_SET(BgL_ppzd2exprzd2defunz00_3417,
								(int) (13L), BgL_ppzd2definezd2_3419);
							PROCEDURE_SET(BgL_ppzd2exprzd2defunz00_3417,
								(int) (14L), BgL_ppzd2lambdazd2_3420);
							PROCEDURE_SET(BgL_ppzd2exprzd2defunz00_3417,
								(int) (15L), BINT(5L));
							PROCEDURE_SET(BgL_ppzd2exprzd2defunz00_3417,
								(int) (16L), BgL_outputz00_9);
							PROCEDURE_SET(BgL_ppzd2exprzd2defunz00_3417,
								(int) (17L), BgL_ppzd2exprzd2_3408);
							PROCEDURE_SET(BgL_ppzd2exprzd2listz00_3409, (int) (0L), BINT(2L));
							PROCEDURE_SET(BgL_ppzd2exprzd2listz00_3409,
								(int) (1L), BgL_ppzd2exprzd2defunz00_3417);
							PROCEDURE_SET(BgL_ppzd2exprzd2listz00_3409,
								(int) (2L), BgL_widthz00_8);
							PROCEDURE_SET(BgL_ppzd2exprzd2listz00_3409,
								(int) (3L), BgL_displayzf3zf3_7);
							PROCEDURE_SET(BgL_ppzd2exprzd2listz00_3409,
								(int) (4L), BgL_ppzd2commentzd2_3407);
							PROCEDURE_SET(BgL_ppzd2exprzd2listz00_3409,
								(int) (5L), BgL_ppzd2dozd2_3410);
							PROCEDURE_SET(BgL_ppzd2exprzd2listz00_3409,
								(int) (6L), BgL_ppzd2beginzd2_3411);
							PROCEDURE_SET(BgL_ppzd2exprzd2listz00_3409,
								(int) (7L), BgL_ppzd2letzd2_3412);
							PROCEDURE_SET(BgL_ppzd2exprzd2listz00_3409,
								(int) (8L), BgL_ppzd2andzd2_3413);
							PROCEDURE_SET(BgL_ppzd2exprzd2listz00_3409,
								(int) (9L), BgL_ppzd2casezd2_3414);
							PROCEDURE_SET(BgL_ppzd2exprzd2listz00_3409,
								(int) (10L), BgL_ppzd2condzd2_3415);
							PROCEDURE_SET(BgL_ppzd2exprzd2listz00_3409,
								(int) (11L), BgL_ppzd2ifzd2_3416);
							PROCEDURE_SET(BgL_ppzd2exprzd2listz00_3409,
								(int) (12L), BgL_ppzd2defunzd2_3418);
							PROCEDURE_SET(BgL_ppzd2exprzd2listz00_3409,
								(int) (13L), BgL_ppzd2definezd2_3419);
							PROCEDURE_SET(BgL_ppzd2exprzd2listz00_3409,
								(int) (14L), BgL_ppzd2lambdazd2_3420);
							PROCEDURE_SET(BgL_ppzd2exprzd2listz00_3409,
								(int) (15L), BINT(5L));
							PROCEDURE_SET(BgL_ppzd2exprzd2listz00_3409,
								(int) (16L), BgL_outputz00_9);
							PROCEDURE_SET(BgL_ppzd2exprzd2listz00_3409,
								(int) (17L), BgL_ppzd2exprzd2_3408);
							PROCEDURE_SET(BgL_ppzd2exprzd2_3408, (int) (0L), BINT(2L));
							PROCEDURE_SET(BgL_ppzd2exprzd2_3408,
								(int) (1L), BgL_ppzd2exprzd2defunz00_3417);
							PROCEDURE_SET(BgL_ppzd2exprzd2_3408, (int) (2L), BgL_widthz00_8);
							PROCEDURE_SET(BgL_ppzd2exprzd2_3408,
								(int) (3L), BgL_displayzf3zf3_7);
							PROCEDURE_SET(BgL_ppzd2exprzd2_3408,
								(int) (4L), BgL_ppzd2exprzd2listz00_3409);
							PROCEDURE_SET(BgL_ppzd2exprzd2_3408,
								(int) (5L), BgL_ppzd2commentzd2_3407);
							PROCEDURE_SET(BgL_ppzd2exprzd2_3408,
								(int) (6L), BgL_ppzd2dozd2_3410);
							PROCEDURE_SET(BgL_ppzd2exprzd2_3408,
								(int) (7L), BgL_ppzd2beginzd2_3411);
							PROCEDURE_SET(BgL_ppzd2exprzd2_3408,
								(int) (8L), BgL_ppzd2letzd2_3412);
							PROCEDURE_SET(BgL_ppzd2exprzd2_3408,
								(int) (9L), BgL_ppzd2andzd2_3413);
							PROCEDURE_SET(BgL_ppzd2exprzd2_3408,
								(int) (10L), BgL_ppzd2casezd2_3414);
							PROCEDURE_SET(BgL_ppzd2exprzd2_3408,
								(int) (11L), BgL_ppzd2condzd2_3415);
							PROCEDURE_SET(BgL_ppzd2exprzd2_3408,
								(int) (12L), BgL_ppzd2ifzd2_3416);
							PROCEDURE_SET(BgL_ppzd2exprzd2_3408,
								(int) (13L), BgL_ppzd2defunzd2_3418);
							PROCEDURE_SET(BgL_ppzd2exprzd2_3408,
								(int) (14L), BgL_ppzd2definezd2_3419);
							PROCEDURE_SET(BgL_ppzd2exprzd2_3408,
								(int) (15L), BgL_ppzd2lambdazd2_3420);
							PROCEDURE_SET(BgL_ppzd2exprzd2_3408,
								(int) (16L), BgL_outputz00_9);
							PROCEDURE_SET(BgL_ppzd2exprzd2_3408, (int) (17L), BINT(5L));
							BgL_arg1230z00_1229 =
								BGl_z62prz62zz__ppz00(BgL_ppzd2exprzd2_3408, BgL_outputz00_9,
								BgL_displayzf3zf3_7, BgL_widthz00_8, 5L,
								BgL_ppzd2lambdazd2_3420, BgL_ppzd2definezd2_3419,
								BgL_ppzd2defunzd2_3418, BgL_ppzd2ifzd2_3416,
								BgL_ppzd2condzd2_3415, BgL_ppzd2casezd2_3414,
								BgL_ppzd2andzd2_3413, BgL_ppzd2letzd2_3412,
								BgL_ppzd2beginzd2_3411, BgL_ppzd2dozd2_3410,
								BgL_ppzd2commentzd2_3407, BgL_ppzd2exprzd2listz00_3409,
								BgL_ppzd2exprzd2defunz00_3417, 2L, BgL_objz00_1461,
								BINT(BgL_colz00_1462), BINT(0L), BgL_ppzd2exprzd2_3408);
						}
						if (CBOOL(BgL_arg1230z00_1229))
							{	/* Pp/pp.scm 133 */
								if (CBOOL(
										((obj_t(*)(obj_t,
													obj_t))
											PROCEDURE_L_ENTRY(BgL_outputz00_9)) (BgL_outputz00_9,
											BgL_arg1229z00_1228)))
									{	/* Pp/pp.scm 133 */
										return
											ADDFX(BgL_arg1230z00_1229,
											BINT(STRING_LENGTH(BgL_arg1229z00_1228)));
									}
								else
									{	/* Pp/pp.scm 133 */
										return BFALSE;
									}
							}
						else
							{	/* Pp/pp.scm 133 */
								return BFALSE;
							}
					}
				else
					{	/* Pp/pp.scm 462 */
						return
							BGl_z62wrz62zz__ppz00(BgL_displayzf3zf3_7, BgL_outputz00_9,
							BgL_objz00_6, BINT(0L));
					}
			}
		}

	}



/* &pp-down */
	obj_t BGl_z62ppzd2downzb0zz__ppz00(obj_t BgL_outputz00_3442,
		obj_t BgL_ppzd2exprzd2_3441, obj_t BgL_displayzf3zf3_3440,
		obj_t BgL_widthz00_3439, long BgL_maxzd2callzd2headzd2widthzd2_3438,
		obj_t BgL_ppzd2lambdazd2_3437, obj_t BgL_ppzd2definezd2_3436,
		obj_t BgL_ppzd2defunzd2_3435, obj_t BgL_ppzd2ifzd2_3434,
		obj_t BgL_ppzd2condzd2_3433, obj_t BgL_ppzd2casezd2_3432,
		obj_t BgL_ppzd2andzd2_3431, obj_t BgL_ppzd2letzd2_3430,
		obj_t BgL_ppzd2beginzd2_3429, obj_t BgL_ppzd2dozd2_3428,
		obj_t BgL_ppzd2commentzd2_3427, obj_t BgL_ppzd2exprzd2listz00_3426,
		obj_t BgL_ppzd2exprzd2defunz00_3425, long BgL_indentzd2generalzd2_3424,
		obj_t BgL_lz00_1577, obj_t BgL_col1z00_1578, obj_t BgL_col2z00_1579,
		obj_t BgL_extraz00_1580, obj_t BgL_ppzd2itemzd2_1581)
	{
		{	/* Pp/pp.scm 334 */
			{
				obj_t BgL_lz00_1584;
				obj_t BgL_colz00_1585;

				BgL_lz00_1584 = BgL_lz00_1577;
				BgL_colz00_1585 = BgL_col1z00_1578;
			BgL_zc3z04anonymousza31468ze3z87_1586:
				if (CBOOL(BgL_colz00_1585))
					{	/* Pp/pp.scm 321 */
						if (PAIRP(BgL_lz00_1584))
							{	/* Pp/pp.scm 323 */
								obj_t BgL_restz00_1589;

								BgL_restz00_1589 = CDR(BgL_lz00_1584);
								{	/* Pp/pp.scm 324 */
									long BgL_extraz00_1590;

									if (NULLP(BgL_restz00_1589))
										{	/* Pp/pp.scm 324 */
											BgL_extraz00_1590 = ((long) CINT(BgL_extraz00_1580) + 1L);
										}
									else
										{	/* Pp/pp.scm 324 */
											BgL_extraz00_1590 = 0L;
										}
									{
										obj_t BgL_colz00_4606;
										obj_t BgL_lz00_4605;

										BgL_lz00_4605 = BgL_restz00_1589;
										BgL_colz00_4606 =
											BGl_z62prz62zz__ppz00(BgL_ppzd2exprzd2_3441,
											BgL_outputz00_3442, BgL_displayzf3zf3_3440,
											BgL_widthz00_3439, BgL_maxzd2callzd2headzd2widthzd2_3438,
											BgL_ppzd2lambdazd2_3437, BgL_ppzd2definezd2_3436,
											BgL_ppzd2defunzd2_3435, BgL_ppzd2ifzd2_3434,
											BgL_ppzd2condzd2_3433, BgL_ppzd2casezd2_3432,
											BgL_ppzd2andzd2_3431, BgL_ppzd2letzd2_3430,
											BgL_ppzd2beginzd2_3429, BgL_ppzd2dozd2_3428,
											BgL_ppzd2commentzd2_3427, BgL_ppzd2exprzd2listz00_3426,
											BgL_ppzd2exprzd2defunz00_3425,
											BgL_indentzd2generalzd2_3424, CAR(BgL_lz00_1584),
											BGl_z62indentz62zz__ppz00(BgL_outputz00_3442,
												BgL_col2z00_1579, BgL_colz00_1585),
											BINT(BgL_extraz00_1590), BgL_ppzd2itemzd2_1581);
										BgL_colz00_1585 = BgL_colz00_4606;
										BgL_lz00_1584 = BgL_lz00_4605;
										goto BgL_zc3z04anonymousza31468ze3z87_1586;
									}
								}
							}
						else
							{	/* Pp/pp.scm 322 */
								if (NULLP(BgL_lz00_1584))
									{	/* Pp/pp.scm 327 */
										if (CBOOL(BgL_colz00_1585))
											{	/* Pp/pp.scm 133 */
												obj_t BgL__andtest_1044z00_2848;

												BgL__andtest_1044z00_2848 =
													((obj_t(*)(obj_t,
															obj_t))
													PROCEDURE_L_ENTRY(BgL_outputz00_3442))
													(BgL_outputz00_3442, BGl_string1957z00zz__ppz00);
												if (CBOOL(BgL__andtest_1044z00_2848))
													{	/* Pp/pp.scm 133 */
														return ADDFX(BgL_colz00_1585, BINT(1L));
													}
												else
													{	/* Pp/pp.scm 133 */
														return BFALSE;
													}
											}
										else
											{	/* Pp/pp.scm 133 */
												return BFALSE;
											}
									}
								else
									{	/* Pp/pp.scm 332 */
										obj_t BgL_arg1477z00_1596;

										{	/* Pp/pp.scm 332 */
											obj_t BgL_arg1478z00_1597;
											long BgL_arg1479z00_1598;

											{	/* Pp/pp.scm 332 */
												obj_t BgL_arg1480z00_1599;

												{	/* Pp/pp.scm 332 */
													obj_t BgL_arg1481z00_1600;

													BgL_arg1481z00_1600 =
														BGl_z62indentz62zz__ppz00(BgL_outputz00_3442,
														BgL_col2z00_1579, BgL_colz00_1585);
													if (CBOOL(BgL_arg1481z00_1600))
														{	/* Pp/pp.scm 133 */
															obj_t BgL__andtest_1044z00_2854;

															BgL__andtest_1044z00_2854 =
																((obj_t(*)(obj_t,
																		obj_t))
																PROCEDURE_L_ENTRY(BgL_outputz00_3442))
																(BgL_outputz00_3442,
																BGl_string1958z00zz__ppz00);
															if (CBOOL(BgL__andtest_1044z00_2854))
																{	/* Pp/pp.scm 133 */
																	BgL_arg1480z00_1599 =
																		ADDFX(BgL_arg1481z00_1600, BINT(1L));
																}
															else
																{	/* Pp/pp.scm 133 */
																	BgL_arg1480z00_1599 = BFALSE;
																}
														}
													else
														{	/* Pp/pp.scm 133 */
															BgL_arg1480z00_1599 = BFALSE;
														}
												}
												BgL_arg1478z00_1597 =
													BGl_z62indentz62zz__ppz00(BgL_outputz00_3442,
													BgL_col2z00_1579, BgL_arg1480z00_1599);
											}
											BgL_arg1479z00_1598 =
												((long) CINT(BgL_extraz00_1580) + 1L);
											BgL_arg1477z00_1596 =
												BGl_z62prz62zz__ppz00(BgL_ppzd2exprzd2_3441,
												BgL_outputz00_3442, BgL_displayzf3zf3_3440,
												BgL_widthz00_3439,
												BgL_maxzd2callzd2headzd2widthzd2_3438,
												BgL_ppzd2lambdazd2_3437, BgL_ppzd2definezd2_3436,
												BgL_ppzd2defunzd2_3435, BgL_ppzd2ifzd2_3434,
												BgL_ppzd2condzd2_3433, BgL_ppzd2casezd2_3432,
												BgL_ppzd2andzd2_3431, BgL_ppzd2letzd2_3430,
												BgL_ppzd2beginzd2_3429, BgL_ppzd2dozd2_3428,
												BgL_ppzd2commentzd2_3427, BgL_ppzd2exprzd2listz00_3426,
												BgL_ppzd2exprzd2defunz00_3425,
												BgL_indentzd2generalzd2_3424, BgL_lz00_1584,
												BgL_arg1478z00_1597, BINT(BgL_arg1479z00_1598),
												BgL_ppzd2itemzd2_1581);
										}
										if (CBOOL(BgL_arg1477z00_1596))
											{	/* Pp/pp.scm 133 */
												obj_t BgL__andtest_1044z00_2861;

												BgL__andtest_1044z00_2861 =
													((obj_t(*)(obj_t,
															obj_t))
													PROCEDURE_L_ENTRY(BgL_outputz00_3442))
													(BgL_outputz00_3442, BGl_string1957z00zz__ppz00);
												if (CBOOL(BgL__andtest_1044z00_2861))
													{	/* Pp/pp.scm 133 */
														return ADDFX(BgL_arg1477z00_1596, BINT(1L));
													}
												else
													{	/* Pp/pp.scm 133 */
														return BFALSE;
													}
											}
										else
											{	/* Pp/pp.scm 133 */
												return BFALSE;
											}
									}
							}
					}
				else
					{	/* Pp/pp.scm 321 */
						return BFALSE;
					}
			}
		}

	}



/* &wr */
	obj_t BGl_z62wrz62zz__ppz00(obj_t BgL_displayzf3zf3_3444,
		obj_t BgL_outputz00_3443, obj_t BgL_objz00_1265, obj_t BgL_colz00_1266)
	{
		{	/* Pp/pp.scm 140 */
		BGl_z62wrz62zz__ppz00:
			{
				obj_t BgL_lz00_1437;
				obj_t BgL_colz00_1438;

				{	/* Pp/pp.scm 151 */
					bool_t BgL_test2122z00_4649;

					if (PAIRP(BgL_objz00_1265))
						{	/* Pp/pp.scm 151 */
							obj_t BgL_cdrzd2105zd2_1418;

							BgL_cdrzd2105zd2_1418 = CDR(((obj_t) BgL_objz00_1265));
							if (
								(CAR(((obj_t) BgL_objz00_1265)) == BGl_symbol1963z00zz__ppz00))
								{	/* Pp/pp.scm 151 */
									if (PAIRP(BgL_cdrzd2105zd2_1418))
										{	/* Pp/pp.scm 151 */
											obj_t BgL_cdrzd2107zd2_1421;

											BgL_cdrzd2107zd2_1421 = CDR(BgL_cdrzd2105zd2_1418);
											if (INTEGERP(CAR(BgL_cdrzd2105zd2_1418)))
												{	/* Pp/pp.scm 151 */
													if (PAIRP(BgL_cdrzd2107zd2_1421))
														{	/* Pp/pp.scm 151 */
															bool_t BgL_test2128z00_4666;

															{	/* Pp/pp.scm 151 */
																obj_t BgL_tmpz00_4667;

																BgL_tmpz00_4667 = CAR(BgL_cdrzd2107zd2_1421);
																BgL_test2128z00_4666 = STRINGP(BgL_tmpz00_4667);
															}
															if (BgL_test2128z00_4666)
																{	/* Pp/pp.scm 151 */
																	BgL_test2122z00_4649 =
																		NULLP(CDR(BgL_cdrzd2107zd2_1421));
																}
															else
																{	/* Pp/pp.scm 151 */
																	BgL_test2122z00_4649 = ((bool_t) 0);
																}
														}
													else
														{	/* Pp/pp.scm 151 */
															BgL_test2122z00_4649 = ((bool_t) 0);
														}
												}
											else
												{	/* Pp/pp.scm 151 */
													BgL_test2122z00_4649 = ((bool_t) 0);
												}
										}
									else
										{	/* Pp/pp.scm 151 */
											BgL_test2122z00_4649 = ((bool_t) 0);
										}
								}
							else
								{	/* Pp/pp.scm 151 */
									BgL_test2122z00_4649 = ((bool_t) 0);
								}
						}
					else
						{	/* Pp/pp.scm 151 */
							BgL_test2122z00_4649 = ((bool_t) 0);
						}
					if (BgL_test2122z00_4649)
						{	/* Pp/pp.scm 156 */
							obj_t BgL_addz00_1291;

							{	/* Pp/pp.scm 156 */
								long BgL_b1111z00_1297;

								{	/* Pp/pp.scm 156 */

									{	/* Pp/pp.scm 156 */
										long BgL_tmpz00_4672;

										{	/* Pp/pp.scm 156 */
											obj_t BgL_stringz00_2400;

											{	/* Pp/pp.scm 156 */
												obj_t BgL_pairz00_2399;

												{	/* Pp/pp.scm 156 */
													obj_t BgL_pairz00_2398;

													BgL_pairz00_2398 = CDR(((obj_t) BgL_objz00_1265));
													BgL_pairz00_2399 = CDR(BgL_pairz00_2398);
												}
												BgL_stringz00_2400 = CAR(BgL_pairz00_2399);
											}
											BgL_tmpz00_4672 = STRING_LENGTH(BgL_stringz00_2400);
										}
										BgL_b1111z00_1297 = (BgL_tmpz00_4672 + 3L);
								}}
								if (INTEGERP(BGl_za2ppzd2widthza2zd2zz__ppz00))
									{	/* Pp/pp.scm 156 */
										BgL_addz00_1291 =
											SUBFX(BGl_za2ppzd2widthza2zd2zz__ppz00,
											BINT(BgL_b1111z00_1297));
									}
								else
									{	/* Pp/pp.scm 156 */
										BgL_addz00_1291 =
											BGl_2zd2zd2zz__r4_numbers_6_5z00
											(BGl_za2ppzd2widthza2zd2zz__ppz00,
											BINT(BgL_b1111z00_1297));
									}
							}
							if (((long) CINT(BgL_addz00_1291) <= 0L))
								{	/* Pp/pp.scm 158 */
									obj_t BgL_arg1309z00_1293;

									{	/* Pp/pp.scm 158 */
										obj_t BgL_pairz00_2411;

										{	/* Pp/pp.scm 158 */
											obj_t BgL_pairz00_2410;

											BgL_pairz00_2410 = CDR(((obj_t) BgL_objz00_1265));
											BgL_pairz00_2411 = CDR(BgL_pairz00_2410);
										}
										BgL_arg1309z00_1293 = CAR(BgL_pairz00_2411);
									}
									if (CBOOL(BgL_colz00_1266))
										{	/* Pp/pp.scm 133 */
											obj_t BgL__andtest_1044z00_2413;

											BgL__andtest_1044z00_2413 =
												((obj_t(*)(obj_t,
														obj_t))
												PROCEDURE_L_ENTRY(BgL_outputz00_3443))
												(BgL_outputz00_3443, BgL_arg1309z00_1293);
											if (CBOOL(BgL__andtest_1044z00_2413))
												{	/* Pp/pp.scm 133 */
													return
														ADDFX(BgL_colz00_1266,
														BINT(STRING_LENGTH(((obj_t) BgL_arg1309z00_1293))));
												}
											else
												{	/* Pp/pp.scm 133 */
													return BFALSE;
												}
										}
									else
										{	/* Pp/pp.scm 133 */
											return BFALSE;
										}
								}
							else
								{	/* Pp/pp.scm 159 */
									obj_t BgL_arg1310z00_1294;

									{	/* Pp/pp.scm 159 */
										obj_t BgL_arg1311z00_1295;
										obj_t BgL_arg1312z00_1296;

										{	/* Pp/pp.scm 159 */
											obj_t BgL_pairz00_2423;

											{	/* Pp/pp.scm 159 */
												obj_t BgL_pairz00_2422;

												BgL_pairz00_2422 = CDR(((obj_t) BgL_objz00_1265));
												BgL_pairz00_2423 = CDR(BgL_pairz00_2422);
											}
											BgL_arg1311z00_1295 = CAR(BgL_pairz00_2423);
										}
										BgL_arg1312z00_1296 =
											make_string(
											(long) CINT(BgL_addz00_1291), ((unsigned char) ' '));
										BgL_arg1310z00_1294 =
											string_append(BgL_arg1311z00_1295, BgL_arg1312z00_1296);
									}
									if (CBOOL(BgL_colz00_1266))
										{	/* Pp/pp.scm 133 */
											obj_t BgL__andtest_1044z00_2426;

											BgL__andtest_1044z00_2426 =
												((obj_t(*)(obj_t,
														obj_t))
												PROCEDURE_L_ENTRY(BgL_outputz00_3443))
												(BgL_outputz00_3443, BgL_arg1310z00_1294);
											if (CBOOL(BgL__andtest_1044z00_2426))
												{	/* Pp/pp.scm 133 */
													return
														ADDFX(BgL_colz00_1266,
														BINT(STRING_LENGTH(BgL_arg1310z00_1294)));
												}
											else
												{	/* Pp/pp.scm 133 */
													return BFALSE;
												}
										}
									else
										{	/* Pp/pp.scm 133 */
											return BFALSE;
										}
								}
						}
					else
						{	/* Pp/pp.scm 151 */
							if (PAIRP(BgL_objz00_1265))
								{	/* Pp/pp.scm 161 */
									if (BGl_z62readzd2macrozf3z43zz__ppz00(BgL_objz00_1265))
										{
											obj_t BgL_colz00_4729;
											obj_t BgL_objz00_4726;

											BgL_objz00_4726 = CAR(CDR(BgL_objz00_1265));
											BgL_colz00_4729 =
												BGl_outze70ze7zz__ppz00(BgL_outputz00_3443,
												BGl_z62readzd2macrozd2prefixz62zz__ppz00
												(BgL_objz00_1265), BgL_colz00_1266);
											BgL_colz00_1266 = BgL_colz00_4729;
											BgL_objz00_1265 = BgL_objz00_4726;
											goto BGl_z62wrz62zz__ppz00;
										}
									else
										{	/* Pp/pp.scm 138 */
											BgL_lz00_1437 = BgL_objz00_1265;
											BgL_colz00_1438 = BgL_colz00_1266;
										BgL_zc3z04anonymousza31405ze3z87_1439:
											if (NULLP(BgL_lz00_1437))
												{	/* Pp/pp.scm 143 */
													if (CBOOL(BgL_colz00_1438))
														{	/* Pp/pp.scm 133 */
															obj_t BgL__andtest_1044z00_2383;

															BgL__andtest_1044z00_2383 =
																((obj_t(*)(obj_t,
																		obj_t))
																PROCEDURE_L_ENTRY(BgL_outputz00_3443))
																(BgL_outputz00_3443,
																BGl_string1959z00zz__ppz00);
															if (CBOOL(BgL__andtest_1044z00_2383))
																{	/* Pp/pp.scm 133 */
																	return ADDFX(BgL_colz00_1438, BINT(2L));
																}
															else
																{	/* Pp/pp.scm 133 */
																	return BFALSE;
																}
														}
													else
														{	/* Pp/pp.scm 133 */
															return BFALSE;
														}
												}
											else
												{	/* Pp/pp.scm 144 */
													obj_t BgL_g1045z00_1441;
													obj_t BgL_g1046z00_1442;

													BgL_g1045z00_1441 = CDR(BgL_lz00_1437);
													{	/* Pp/pp.scm 144 */
														obj_t BgL_arg1417z00_1457;
														obj_t BgL_arg1418z00_1458;

														BgL_arg1417z00_1457 = CAR(BgL_lz00_1437);
														if (CBOOL(BgL_colz00_1438))
															{	/* Pp/pp.scm 133 */
																obj_t BgL__andtest_1044z00_2351;

																BgL__andtest_1044z00_2351 =
																	((obj_t(*)(obj_t,
																			obj_t))
																	PROCEDURE_L_ENTRY(BgL_outputz00_3443))
																	(BgL_outputz00_3443,
																	BGl_string1960z00zz__ppz00);
																if (CBOOL(BgL__andtest_1044z00_2351))
																	{	/* Pp/pp.scm 133 */
																		BgL_arg1418z00_1458 =
																			ADDFX(BgL_colz00_1438, BINT(1L));
																	}
																else
																	{	/* Pp/pp.scm 133 */
																		BgL_arg1418z00_1458 = BFALSE;
																	}
															}
														else
															{	/* Pp/pp.scm 133 */
																BgL_arg1418z00_1458 = BFALSE;
															}
														BgL_g1046z00_1442 =
															BGl_z62wrz62zz__ppz00(BgL_displayzf3zf3_3444,
															BgL_outputz00_3443, BgL_arg1417z00_1457,
															BgL_arg1418z00_1458);
													}
													{
														obj_t BgL_lz00_1444;
														obj_t BgL_colz00_1445;

														BgL_lz00_1444 = BgL_g1045z00_1441;
														BgL_colz00_1445 = BgL_g1046z00_1442;
													BgL_zc3z04anonymousza31407ze3z87_1446:
														if (CBOOL(BgL_colz00_1445))
															{	/* Pp/pp.scm 145 */
																if (PAIRP(BgL_lz00_1444))
																	{	/* Pp/pp.scm 146 */
																		obj_t BgL_arg1410z00_1449;
																		obj_t BgL_arg1411z00_1450;

																		BgL_arg1410z00_1449 = CDR(BgL_lz00_1444);
																		{	/* Pp/pp.scm 146 */
																			obj_t BgL_arg1412z00_1451;
																			obj_t BgL_arg1413z00_1452;

																			BgL_arg1412z00_1451 = CAR(BgL_lz00_1444);
																			if (CBOOL(BgL_colz00_1445))
																				{	/* Pp/pp.scm 133 */
																					obj_t BgL__andtest_1044z00_2359;

																					BgL__andtest_1044z00_2359 =
																						((obj_t(*)(obj_t,
																								obj_t))
																						PROCEDURE_L_ENTRY
																						(BgL_outputz00_3443))
																						(BgL_outputz00_3443,
																						BGl_string1961z00zz__ppz00);
																					if (CBOOL(BgL__andtest_1044z00_2359))
																						{	/* Pp/pp.scm 133 */
																							BgL_arg1413z00_1452 =
																								ADDFX(BgL_colz00_1445,
																								BINT(1L));
																						}
																					else
																						{	/* Pp/pp.scm 133 */
																							BgL_arg1413z00_1452 = BFALSE;
																						}
																				}
																			else
																				{	/* Pp/pp.scm 133 */
																					BgL_arg1413z00_1452 = BFALSE;
																				}
																			BgL_arg1411z00_1450 =
																				BGl_z62wrz62zz__ppz00
																				(BgL_displayzf3zf3_3444,
																				BgL_outputz00_3443, BgL_arg1412z00_1451,
																				BgL_arg1413z00_1452);
																		}
																		{
																			obj_t BgL_colz00_4775;
																			obj_t BgL_lz00_4774;

																			BgL_lz00_4774 = BgL_arg1410z00_1449;
																			BgL_colz00_4775 = BgL_arg1411z00_1450;
																			BgL_colz00_1445 = BgL_colz00_4775;
																			BgL_lz00_1444 = BgL_lz00_4774;
																			goto
																				BgL_zc3z04anonymousza31407ze3z87_1446;
																		}
																	}
																else
																	{	/* Pp/pp.scm 146 */
																		if (NULLP(BgL_lz00_1444))
																			{	/* Pp/pp.scm 147 */
																				if (CBOOL(BgL_colz00_1445))
																					{	/* Pp/pp.scm 133 */
																						obj_t BgL__andtest_1044z00_2365;

																						BgL__andtest_1044z00_2365 =
																							((obj_t(*)(obj_t,
																									obj_t))
																							PROCEDURE_L_ENTRY
																							(BgL_outputz00_3443))
																							(BgL_outputz00_3443,
																							BGl_string1957z00zz__ppz00);
																						if (CBOOL
																							(BgL__andtest_1044z00_2365))
																							{	/* Pp/pp.scm 133 */
																								return
																									ADDFX(BgL_colz00_1445,
																									BINT(1L));
																							}
																						else
																							{	/* Pp/pp.scm 133 */
																								return BFALSE;
																							}
																					}
																				else
																					{	/* Pp/pp.scm 133 */
																						return BFALSE;
																					}
																			}
																		else
																			{	/* Pp/pp.scm 148 */
																				obj_t BgL_arg1415z00_1454;

																				{	/* Pp/pp.scm 148 */
																					obj_t BgL_arg1416z00_1455;

																					if (CBOOL(BgL_colz00_1445))
																						{	/* Pp/pp.scm 133 */
																							obj_t BgL__andtest_1044z00_2371;

																							BgL__andtest_1044z00_2371 =
																								((obj_t(*)(obj_t,
																										obj_t))
																								PROCEDURE_L_ENTRY
																								(BgL_outputz00_3443))
																								(BgL_outputz00_3443,
																								BGl_string1962z00zz__ppz00);
																							if (CBOOL
																								(BgL__andtest_1044z00_2371))
																								{	/* Pp/pp.scm 133 */
																									BgL_arg1416z00_1455 =
																										ADDFX(BgL_colz00_1445,
																										BINT(3L));
																								}
																							else
																								{	/* Pp/pp.scm 133 */
																									BgL_arg1416z00_1455 = BFALSE;
																								}
																						}
																					else
																						{	/* Pp/pp.scm 133 */
																							BgL_arg1416z00_1455 = BFALSE;
																						}
																					BgL_arg1415z00_1454 =
																						BGl_z62wrz62zz__ppz00
																						(BgL_displayzf3zf3_3444,
																						BgL_outputz00_3443, BgL_lz00_1444,
																						BgL_arg1416z00_1455);
																				}
																				if (CBOOL(BgL_arg1415z00_1454))
																					{	/* Pp/pp.scm 133 */
																						obj_t BgL__andtest_1044z00_2377;

																						BgL__andtest_1044z00_2377 =
																							((obj_t(*)(obj_t,
																									obj_t))
																							PROCEDURE_L_ENTRY
																							(BgL_outputz00_3443))
																							(BgL_outputz00_3443,
																							BGl_string1957z00zz__ppz00);
																						if (CBOOL
																							(BgL__andtest_1044z00_2377))
																							{	/* Pp/pp.scm 133 */
																								return
																									ADDFX(BgL_arg1415z00_1454,
																									BINT(1L));
																							}
																						else
																							{	/* Pp/pp.scm 133 */
																								return BFALSE;
																							}
																					}
																				else
																					{	/* Pp/pp.scm 133 */
																						return BFALSE;
																					}
																			}
																	}
															}
														else
															{	/* Pp/pp.scm 145 */
																return BFALSE;
															}
													}
												}
										}
								}
							else
								{	/* Pp/pp.scm 161 */
									if (NULLP(BgL_objz00_1265))
										{
											obj_t BgL_colz00_4812;
											obj_t BgL_lz00_4811;

											BgL_lz00_4811 = BgL_objz00_1265;
											BgL_colz00_4812 = BgL_colz00_1266;
											BgL_colz00_1438 = BgL_colz00_4812;
											BgL_lz00_1437 = BgL_lz00_4811;
											goto BgL_zc3z04anonymousza31405ze3z87_1439;
										}
									else
										{	/* Pp/pp.scm 163 */
											if (VECTORP(BgL_objz00_1265))
												{	/* Pp/pp.scm 166 */
													obj_t BgL_arg1319z00_1306;
													obj_t BgL_arg1320z00_1307;

													BgL_arg1319z00_1306 =
														BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00
														(BgL_objz00_1265);
													{	/* Pp/pp.scm 167 */
														obj_t BgL_arg1321z00_1308;

														BgL_arg1321z00_1308 =
															BGl_vectorzd2prefixzd2zz__ppz00(BgL_objz00_1265);
														if (CBOOL(BgL_colz00_1266))
															{	/* Pp/pp.scm 133 */
																obj_t BgL__andtest_1044z00_2440;

																BgL__andtest_1044z00_2440 =
																	((obj_t(*)(obj_t,
																			obj_t))
																	PROCEDURE_L_ENTRY(BgL_outputz00_3443))
																	(BgL_outputz00_3443, BgL_arg1321z00_1308);
																if (CBOOL(BgL__andtest_1044z00_2440))
																	{	/* Pp/pp.scm 133 */
																		BgL_arg1320z00_1307 =
																			ADDFX(BgL_colz00_1266,
																			BINT(STRING_LENGTH(BgL_arg1321z00_1308)));
																	}
																else
																	{	/* Pp/pp.scm 133 */
																		BgL_arg1320z00_1307 = BFALSE;
																	}
															}
														else
															{	/* Pp/pp.scm 133 */
																BgL_arg1320z00_1307 = BFALSE;
															}
													}
													{
														obj_t BgL_colz00_4829;
														obj_t BgL_lz00_4828;

														BgL_lz00_4828 = BgL_arg1319z00_1306;
														BgL_colz00_4829 = BgL_arg1320z00_1307;
														BgL_colz00_1438 = BgL_colz00_4829;
														BgL_lz00_1437 = BgL_lz00_4828;
														goto BgL_zc3z04anonymousza31405ze3z87_1439;
													}
												}
											else
												{	/* Pp/pp.scm 165 */
													if (BOOLEANP(BgL_objz00_1265))
														{	/* Pp/pp.scm 169 */
															obj_t BgL_arg1323z00_1310;

															if (CBOOL(BgL_objz00_1265))
																{	/* Pp/pp.scm 169 */
																	BgL_arg1323z00_1310 =
																		BGl_string1965z00zz__ppz00;
																}
															else
																{	/* Pp/pp.scm 169 */
																	BgL_arg1323z00_1310 =
																		BGl_string1966z00zz__ppz00;
																}
															if (CBOOL(BgL_colz00_1266))
																{	/* Pp/pp.scm 133 */
																	obj_t BgL__andtest_1044z00_2446;

																	BgL__andtest_1044z00_2446 =
																		((obj_t(*)(obj_t,
																				obj_t))
																		PROCEDURE_L_ENTRY(BgL_outputz00_3443))
																		(BgL_outputz00_3443, BgL_arg1323z00_1310);
																	if (CBOOL(BgL__andtest_1044z00_2446))
																		{	/* Pp/pp.scm 133 */
																			return
																				ADDFX(BgL_colz00_1266,
																				BINT(STRING_LENGTH
																					(BgL_arg1323z00_1310)));
																		}
																	else
																		{	/* Pp/pp.scm 133 */
																			return BFALSE;
																		}
																}
															else
																{	/* Pp/pp.scm 133 */
																	return BFALSE;
																}
														}
													else
														{	/* Pp/pp.scm 168 */
															if (BGl_numberzf3zf3zz__r4_numbers_6_5z00
																(BgL_objz00_1265))
																{	/* Pp/pp.scm 170 */
																	if (ELONGP(BgL_objz00_1265))
																		{	/* Pp/pp.scm 173 */
																			obj_t BgL_arg1326z00_1313;

																			{	/* Pp/pp.scm 173 */
																				obj_t BgL_arg1327z00_1314;

																				{	/* Ieee/number.scm 174 */

																					BgL_arg1327z00_1314 =
																						BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00
																						(BgL_objz00_1265, BINT(10L));
																				}
																				BgL_arg1326z00_1313 =
																					string_append
																					(BGl_string1967z00zz__ppz00,
																					BgL_arg1327z00_1314);
																			}
																			if (CBOOL(BgL_colz00_1266))
																				{	/* Pp/pp.scm 133 */
																					obj_t BgL__andtest_1044z00_2452;

																					BgL__andtest_1044z00_2452 =
																						((obj_t(*)(obj_t,
																								obj_t))
																						PROCEDURE_L_ENTRY
																						(BgL_outputz00_3443))
																						(BgL_outputz00_3443,
																						BgL_arg1326z00_1313);
																					if (CBOOL(BgL__andtest_1044z00_2452))
																						{	/* Pp/pp.scm 133 */
																							return
																								ADDFX(BgL_colz00_1266,
																								BINT(STRING_LENGTH
																									(BgL_arg1326z00_1313)));
																						}
																					else
																						{	/* Pp/pp.scm 133 */
																							return BFALSE;
																						}
																				}
																			else
																				{	/* Pp/pp.scm 133 */
																					return BFALSE;
																				}
																		}
																	else
																		{	/* Pp/pp.scm 172 */
																			if (LLONGP(BgL_objz00_1265))
																				{	/* Pp/pp.scm 175 */
																					obj_t BgL_arg1329z00_1318;

																					{	/* Pp/pp.scm 175 */
																						obj_t BgL_arg1331z00_1319;

																						{	/* Ieee/number.scm 174 */

																							BgL_arg1331z00_1319 =
																								BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00
																								(BgL_objz00_1265, BINT(10L));
																						}
																						BgL_arg1329z00_1318 =
																							string_append
																							(BGl_string1968z00zz__ppz00,
																							BgL_arg1331z00_1319);
																					}
																					if (CBOOL(BgL_colz00_1266))
																						{	/* Pp/pp.scm 133 */
																							obj_t BgL__andtest_1044z00_2458;

																							BgL__andtest_1044z00_2458 =
																								((obj_t(*)(obj_t,
																										obj_t))
																								PROCEDURE_L_ENTRY
																								(BgL_outputz00_3443))
																								(BgL_outputz00_3443,
																								BgL_arg1329z00_1318);
																							if (CBOOL
																								(BgL__andtest_1044z00_2458))
																								{	/* Pp/pp.scm 133 */
																									return
																										ADDFX(BgL_colz00_1266,
																										BINT(STRING_LENGTH
																											(BgL_arg1329z00_1318)));
																								}
																							else
																								{	/* Pp/pp.scm 133 */
																									return BFALSE;
																								}
																						}
																					else
																						{	/* Pp/pp.scm 133 */
																							return BFALSE;
																						}
																				}
																			else
																				{	/* Pp/pp.scm 174 */
																					if (BGL_UINT8P(BgL_objz00_1265))
																						{	/* Pp/pp.scm 177 */
																							obj_t BgL_arg1333z00_1323;

																							{	/* Pp/pp.scm 177 */
																								obj_t BgL_arg1334z00_1324;

																								{	/* Ieee/number.scm 174 */

																									BgL_arg1334z00_1324 =
																										BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00
																										(BgL_objz00_1265,
																										BINT(10L));
																								}
																								BgL_arg1333z00_1323 =
																									string_append
																									(BGl_string1969z00zz__ppz00,
																									BgL_arg1334z00_1324);
																							}
																							if (CBOOL(BgL_colz00_1266))
																								{	/* Pp/pp.scm 133 */
																									obj_t
																										BgL__andtest_1044z00_2464;
																									BgL__andtest_1044z00_2464 =
																										((obj_t(*)(obj_t,
																												obj_t))
																										PROCEDURE_L_ENTRY
																										(BgL_outputz00_3443))
																										(BgL_outputz00_3443,
																										BgL_arg1333z00_1323);
																									if (CBOOL
																										(BgL__andtest_1044z00_2464))
																										{	/* Pp/pp.scm 133 */
																											return
																												ADDFX(BgL_colz00_1266,
																												BINT(STRING_LENGTH
																													(BgL_arg1333z00_1323)));
																										}
																									else
																										{	/* Pp/pp.scm 133 */
																											return BFALSE;
																										}
																								}
																							else
																								{	/* Pp/pp.scm 133 */
																									return BFALSE;
																								}
																						}
																					else
																						{	/* Pp/pp.scm 176 */
																							if (BGL_UINT16P(BgL_objz00_1265))
																								{	/* Pp/pp.scm 179 */
																									obj_t BgL_arg1336z00_1328;

																									{	/* Pp/pp.scm 179 */
																										obj_t BgL_arg1337z00_1329;

																										{	/* Ieee/number.scm 174 */

																											BgL_arg1337z00_1329 =
																												BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00
																												(BgL_objz00_1265,
																												BINT(10L));
																										}
																										BgL_arg1336z00_1328 =
																											string_append
																											(BGl_string1970z00zz__ppz00,
																											BgL_arg1337z00_1329);
																									}
																									if (CBOOL(BgL_colz00_1266))
																										{	/* Pp/pp.scm 133 */
																											obj_t
																												BgL__andtest_1044z00_2470;
																											BgL__andtest_1044z00_2470
																												=
																												((obj_t(*)(obj_t,
																														obj_t))
																												PROCEDURE_L_ENTRY
																												(BgL_outputz00_3443))
																												(BgL_outputz00_3443,
																												BgL_arg1336z00_1328);
																											if (CBOOL
																												(BgL__andtest_1044z00_2470))
																												{	/* Pp/pp.scm 133 */
																													return
																														ADDFX
																														(BgL_colz00_1266,
																														BINT(STRING_LENGTH
																															(BgL_arg1336z00_1328)));
																												}
																											else
																												{	/* Pp/pp.scm 133 */
																													return BFALSE;
																												}
																										}
																									else
																										{	/* Pp/pp.scm 133 */
																											return BFALSE;
																										}
																								}
																							else
																								{	/* Pp/pp.scm 178 */
																									if (BGL_UINT32P
																										(BgL_objz00_1265))
																										{	/* Pp/pp.scm 181 */
																											obj_t BgL_arg1339z00_1333;

																											{	/* Pp/pp.scm 181 */
																												obj_t
																													BgL_arg1340z00_1334;
																												{	/* Ieee/number.scm 174 */

																													BgL_arg1340z00_1334 =
																														BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00
																														(BgL_objz00_1265,
																														BINT(10L));
																												}
																												BgL_arg1339z00_1333 =
																													string_append
																													(BGl_string1971z00zz__ppz00,
																													BgL_arg1340z00_1334);
																											}
																											if (CBOOL
																												(BgL_colz00_1266))
																												{	/* Pp/pp.scm 133 */
																													obj_t
																														BgL__andtest_1044z00_2476;
																													BgL__andtest_1044z00_2476
																														=
																														((obj_t(*)(obj_t,
																																obj_t))
																														PROCEDURE_L_ENTRY
																														(BgL_outputz00_3443))
																														(BgL_outputz00_3443,
																														BgL_arg1339z00_1333);
																													if (CBOOL
																														(BgL__andtest_1044z00_2476))
																														{	/* Pp/pp.scm 133 */
																															return
																																ADDFX
																																(BgL_colz00_1266,
																																BINT
																																(STRING_LENGTH
																																	(BgL_arg1339z00_1333)));
																														}
																													else
																														{	/* Pp/pp.scm 133 */
																															return BFALSE;
																														}
																												}
																											else
																												{	/* Pp/pp.scm 133 */
																													return BFALSE;
																												}
																										}
																									else
																										{	/* Pp/pp.scm 180 */
																											if (BGL_INT8P
																												(BgL_objz00_1265))
																												{	/* Pp/pp.scm 183 */
																													obj_t
																														BgL_arg1342z00_1338;
																													{	/* Pp/pp.scm 183 */
																														obj_t
																															BgL_arg1343z00_1339;
																														{	/* Ieee/number.scm 174 */

																															BgL_arg1343z00_1339
																																=
																																BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00
																																(BgL_objz00_1265,
																																BINT(10L));
																														}
																														BgL_arg1342z00_1338
																															=
																															string_append
																															(BGl_string1972z00zz__ppz00,
																															BgL_arg1343z00_1339);
																													}
																													if (CBOOL
																														(BgL_colz00_1266))
																														{	/* Pp/pp.scm 133 */
																															obj_t
																																BgL__andtest_1044z00_2482;
																															BgL__andtest_1044z00_2482
																																=
																																((obj_t(*)
																																	(obj_t,
																																		obj_t))
																																PROCEDURE_L_ENTRY
																																(BgL_outputz00_3443))
																																(BgL_outputz00_3443,
																																BgL_arg1342z00_1338);
																															if (CBOOL
																																(BgL__andtest_1044z00_2482))
																																{	/* Pp/pp.scm 133 */
																																	return
																																		ADDFX
																																		(BgL_colz00_1266,
																																		BINT
																																		(STRING_LENGTH
																																			(BgL_arg1342z00_1338)));
																																}
																															else
																																{	/* Pp/pp.scm 133 */
																																	return BFALSE;
																																}
																														}
																													else
																														{	/* Pp/pp.scm 133 */
																															return BFALSE;
																														}
																												}
																											else
																												{	/* Pp/pp.scm 182 */
																													if (BGL_INT16P
																														(BgL_objz00_1265))
																														{	/* Pp/pp.scm 185 */
																															obj_t
																																BgL_arg1346z00_1343;
																															{	/* Pp/pp.scm 185 */
																																obj_t
																																	BgL_arg1347z00_1344;
																																{	/* Ieee/number.scm 174 */

																																	BgL_arg1347z00_1344
																																		=
																																		BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00
																																		(BgL_objz00_1265,
																																		BINT(10L));
																																}
																																BgL_arg1346z00_1343
																																	=
																																	string_append
																																	(BGl_string1973z00zz__ppz00,
																																	BgL_arg1347z00_1344);
																															}
																															if (CBOOL
																																(BgL_colz00_1266))
																																{	/* Pp/pp.scm 133 */
																																	obj_t
																																		BgL__andtest_1044z00_2488;
																																	BgL__andtest_1044z00_2488
																																		=
																																		((obj_t(*)
																																			(obj_t,
																																				obj_t))
																																		PROCEDURE_L_ENTRY
																																		(BgL_outputz00_3443))
																																		(BgL_outputz00_3443,
																																		BgL_arg1346z00_1343);
																																	if (CBOOL
																																		(BgL__andtest_1044z00_2488))
																																		{	/* Pp/pp.scm 133 */
																																			return
																																				ADDFX
																																				(BgL_colz00_1266,
																																				BINT
																																				(STRING_LENGTH
																																					(BgL_arg1346z00_1343)));
																																		}
																																	else
																																		{	/* Pp/pp.scm 133 */
																																			return
																																				BFALSE;
																																		}
																																}
																															else
																																{	/* Pp/pp.scm 133 */
																																	return BFALSE;
																																}
																														}
																													else
																														{	/* Pp/pp.scm 184 */
																															if (BGL_INT32P
																																(BgL_objz00_1265))
																																{	/* Pp/pp.scm 187 */
																																	obj_t
																																		BgL_arg1349z00_1348;
																																	{	/* Pp/pp.scm 187 */
																																		obj_t
																																			BgL_arg1350z00_1349;
																																		{	/* Ieee/number.scm 174 */

																																			BgL_arg1350z00_1349
																																				=
																																				BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00
																																				(BgL_objz00_1265,
																																				BINT
																																				(10L));
																																		}
																																		BgL_arg1349z00_1348
																																			=
																																			string_append
																																			(BGl_string1974z00zz__ppz00,
																																			BgL_arg1350z00_1349);
																																	}
																																	if (CBOOL
																																		(BgL_colz00_1266))
																																		{	/* Pp/pp.scm 133 */
																																			obj_t
																																				BgL__andtest_1044z00_2494;
																																			BgL__andtest_1044z00_2494
																																				=
																																				((obj_t
																																					(*)
																																					(obj_t,
																																						obj_t))
																																				PROCEDURE_L_ENTRY
																																				(BgL_outputz00_3443))
																																				(BgL_outputz00_3443,
																																				BgL_arg1349z00_1348);
																																			if (CBOOL
																																				(BgL__andtest_1044z00_2494))
																																				{	/* Pp/pp.scm 133 */
																																					return
																																						ADDFX
																																						(BgL_colz00_1266,
																																						BINT
																																						(STRING_LENGTH
																																							(BgL_arg1349z00_1348)));
																																				}
																																			else
																																				{	/* Pp/pp.scm 133 */
																																					return
																																						BFALSE;
																																				}
																																		}
																																	else
																																		{	/* Pp/pp.scm 133 */
																																			return
																																				BFALSE;
																																		}
																																}
																															else
																																{	/* Pp/pp.scm 186 */
																																	if (BIGNUMP
																																		(BgL_objz00_1265))
																																		{	/* Pp/pp.scm 189 */
																																			obj_t
																																				BgL_arg1352z00_1353;
																																			{	/* Pp/pp.scm 189 */
																																				obj_t
																																					BgL_arg1354z00_1354;
																																				{	/* Ieee/number.scm 174 */

																																					BgL_arg1354z00_1354
																																						=
																																						BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00
																																						(BgL_objz00_1265,
																																						BINT
																																						(10L));
																																				}
																																				BgL_arg1352z00_1353
																																					=
																																					string_append
																																					(BGl_string1975z00zz__ppz00,
																																					BgL_arg1354z00_1354);
																																			}
																																			if (CBOOL
																																				(BgL_colz00_1266))
																																				{	/* Pp/pp.scm 133 */
																																					obj_t
																																						BgL__andtest_1044z00_2500;
																																					BgL__andtest_1044z00_2500
																																						=
																																						(
																																						(obj_t
																																							(*)
																																							(obj_t,
																																								obj_t))
																																						PROCEDURE_L_ENTRY
																																						(BgL_outputz00_3443))
																																						(BgL_outputz00_3443,
																																						BgL_arg1352z00_1353);
																																					if (CBOOL(BgL__andtest_1044z00_2500))
																																						{	/* Pp/pp.scm 133 */
																																							return
																																								ADDFX
																																								(BgL_colz00_1266,
																																								BINT
																																								(STRING_LENGTH
																																									(BgL_arg1352z00_1353)));
																																						}
																																					else
																																						{	/* Pp/pp.scm 133 */
																																							return
																																								BFALSE;
																																						}
																																				}
																																			else
																																				{	/* Pp/pp.scm 133 */
																																					return
																																						BFALSE;
																																				}
																																		}
																																	else
																																		{	/* Pp/pp.scm 191 */
																																			obj_t
																																				BgL_arg1356z00_1357;
																																			{	/* Ieee/number.scm 174 */

																																				BgL_arg1356z00_1357
																																					=
																																					BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00
																																					(BgL_objz00_1265,
																																					BINT
																																					(10L));
																																			}
																																			if (CBOOL
																																				(BgL_colz00_1266))
																																				{	/* Pp/pp.scm 133 */
																																					obj_t
																																						BgL__andtest_1044z00_2506;
																																					BgL__andtest_1044z00_2506
																																						=
																																						(
																																						(obj_t
																																							(*)
																																							(obj_t,
																																								obj_t))
																																						PROCEDURE_L_ENTRY
																																						(BgL_outputz00_3443))
																																						(BgL_outputz00_3443,
																																						BgL_arg1356z00_1357);
																																					if (CBOOL(BgL__andtest_1044z00_2506))
																																						{	/* Pp/pp.scm 133 */
																																							return
																																								ADDFX
																																								(BgL_colz00_1266,
																																								BINT
																																								(STRING_LENGTH
																																									(BgL_arg1356z00_1357)));
																																						}
																																					else
																																						{	/* Pp/pp.scm 133 */
																																							return
																																								BFALSE;
																																						}
																																				}
																																			else
																																				{	/* Pp/pp.scm 133 */
																																					return
																																						BFALSE;
																																				}
																																		}
																																}
																														}
																												}
																										}
																								}
																						}
																				}
																		}
																}
															else
																{	/* Pp/pp.scm 170 */
																	if (SYMBOLP(BgL_objz00_1265))
																		{	/* Pp/pp.scm 193 */
																			obj_t BgL_pz00_1361;

																			{	/* Pp/pp.scm 193 */

																				{	/* Ieee/port.scm 485 */

																					BgL_pz00_1361 =
																						BGl_openzd2outputzd2stringz00zz__r4_ports_6_10_1z00
																						(BTRUE);
																				}
																			}
																			if (CBOOL(BgL_displayzf3zf3_3444))
																				{	/* Pp/pp.scm 194 */
																					bgl_display_obj(BgL_objz00_1265,
																						BgL_pz00_1361);
																				}
																			else
																				{	/* Pp/pp.scm 196 */
																					obj_t BgL_list1358z00_1362;

																					BgL_list1358z00_1362 =
																						MAKE_YOUNG_PAIR(BgL_pz00_1361,
																						BNIL);
																					BGl_writez00zz__r4_output_6_10_3z00
																						(BgL_objz00_1265,
																						BgL_list1358z00_1362);
																				}
																			{	/* Pp/pp.scm 197 */
																				obj_t BgL_casezd2valuezd2_1363;

																				BgL_casezd2valuezd2_1363 =
																					BGl_za2ppzd2caseza2zd2zz__ppz00;
																				if ((BgL_casezd2valuezd2_1363 ==
																						BGl_symbol1950z00zz__ppz00))
																					{	/* Pp/pp.scm 199 */
																						obj_t BgL_arg1360z00_1365;

																						BgL_arg1360z00_1365 =
																							bgl_close_output_port
																							(BgL_pz00_1361);
																						if (CBOOL(BgL_colz00_1266))
																							{	/* Pp/pp.scm 133 */
																								obj_t BgL__andtest_1044z00_2514;

																								BgL__andtest_1044z00_2514 =
																									((obj_t(*)(obj_t,
																											obj_t))
																									PROCEDURE_L_ENTRY
																									(BgL_outputz00_3443))
																									(BgL_outputz00_3443,
																									BgL_arg1360z00_1365);
																								if (CBOOL
																									(BgL__andtest_1044z00_2514))
																									{	/* Pp/pp.scm 133 */
																										return
																											ADDFX(BgL_colz00_1266,
																											BINT(STRING_LENGTH(
																													((obj_t)
																														BgL_arg1360z00_1365))));
																									}
																								else
																									{	/* Pp/pp.scm 133 */
																										return BFALSE;
																									}
																							}
																						else
																							{	/* Pp/pp.scm 133 */
																								return BFALSE;
																							}
																					}
																				else
																					{	/* Pp/pp.scm 197 */
																						if (
																							(BgL_casezd2valuezd2_1363 ==
																								BGl_symbol1976z00zz__ppz00))
																							{	/* Pp/pp.scm 201 */
																								obj_t BgL_arg1362z00_1367;

																								BgL_arg1362z00_1367 =
																									BGl_stringzd2upcasezd2zz__r4_strings_6_7z00
																									(bgl_close_output_port
																									(BgL_pz00_1361));
																								if (CBOOL(BgL_colz00_1266))
																									{	/* Pp/pp.scm 133 */
																										obj_t
																											BgL__andtest_1044z00_2522;
																										BgL__andtest_1044z00_2522 =
																											((obj_t(*)(obj_t,
																													obj_t))
																											PROCEDURE_L_ENTRY
																											(BgL_outputz00_3443))
																											(BgL_outputz00_3443,
																											BgL_arg1362z00_1367);
																										if (CBOOL
																											(BgL__andtest_1044z00_2522))
																											{	/* Pp/pp.scm 133 */
																												return
																													ADDFX(BgL_colz00_1266,
																													BINT(STRING_LENGTH
																														(BgL_arg1362z00_1367)));
																											}
																										else
																											{	/* Pp/pp.scm 133 */
																												return BFALSE;
																											}
																									}
																								else
																									{	/* Pp/pp.scm 133 */
																										return BFALSE;
																									}
																							}
																						else
																							{	/* Pp/pp.scm 204 */
																								obj_t BgL_arg1364z00_1369;

																								BgL_arg1364z00_1369 =
																									BGl_stringzd2downcasezd2zz__r4_strings_6_7z00
																									(bgl_close_output_port
																									(BgL_pz00_1361));
																								if (CBOOL(BgL_colz00_1266))
																									{	/* Pp/pp.scm 133 */
																										obj_t
																											BgL__andtest_1044z00_2529;
																										BgL__andtest_1044z00_2529 =
																											((obj_t(*)(obj_t,
																													obj_t))
																											PROCEDURE_L_ENTRY
																											(BgL_outputz00_3443))
																											(BgL_outputz00_3443,
																											BgL_arg1364z00_1369);
																										if (CBOOL
																											(BgL__andtest_1044z00_2529))
																											{	/* Pp/pp.scm 133 */
																												return
																													ADDFX(BgL_colz00_1266,
																													BINT(STRING_LENGTH
																														(BgL_arg1364z00_1369)));
																											}
																										else
																											{	/* Pp/pp.scm 133 */
																												return BFALSE;
																											}
																									}
																								else
																									{	/* Pp/pp.scm 133 */
																										return BFALSE;
																									}
																							}
																					}
																			}
																		}
																	else
																		{	/* Pp/pp.scm 192 */
																			if (PROCEDUREP(BgL_objz00_1265))
																				{	/* Pp/pp.scm 207 */
																					obj_t BgL_arg1367z00_1373;

																					{	/* Pp/pp.scm 207 */
																						obj_t
																							BgL_zc3z04anonymousza31369ze3z87_3402;
																						BgL_zc3z04anonymousza31369ze3z87_3402
																							=
																							MAKE_FX_PROCEDURE
																							(BGl_z62zc3z04anonymousza31369ze3ze5zz__ppz00,
																							(int) (0L), (int) (1L));
																						PROCEDURE_SET
																							(BgL_zc3z04anonymousza31369ze3z87_3402,
																							(int) (0L), BgL_objz00_1265);
																						BgL_arg1367z00_1373 =
																							BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00
																							(BgL_zc3z04anonymousza31369ze3z87_3402);
																					}
																					if (CBOOL(BgL_colz00_1266))
																						{	/* Pp/pp.scm 133 */
																							obj_t BgL__andtest_1044z00_2537;

																							BgL__andtest_1044z00_2537 =
																								((obj_t(*)(obj_t,
																										obj_t))
																								PROCEDURE_L_ENTRY
																								(BgL_outputz00_3443))
																								(BgL_outputz00_3443,
																								BgL_arg1367z00_1373);
																							if (CBOOL
																								(BgL__andtest_1044z00_2537))
																								{	/* Pp/pp.scm 133 */
																									return
																										ADDFX(BgL_colz00_1266,
																										BINT(STRING_LENGTH
																											(BgL_arg1367z00_1373)));
																								}
																							else
																								{	/* Pp/pp.scm 133 */
																									return BFALSE;
																								}
																						}
																					else
																						{	/* Pp/pp.scm 133 */
																							return BFALSE;
																						}
																				}
																			else
																				{	/* Pp/pp.scm 206 */
																					if (STRINGP(BgL_objz00_1265))
																						{	/* Pp/pp.scm 209 */
																							obj_t BgL_objz00_1379;

																							BgL_objz00_1379 =
																								string_for_read
																								(BgL_objz00_1265);
																							if (CBOOL(BgL_displayzf3zf3_3444))
																								{	/* Pp/pp.scm 210 */
																									if (CBOOL(BgL_colz00_1266))
																										{	/* Pp/pp.scm 133 */
																											obj_t
																												BgL__andtest_1044z00_2544;
																											BgL__andtest_1044z00_2544
																												=
																												((obj_t(*)(obj_t,
																														obj_t))
																												PROCEDURE_L_ENTRY
																												(BgL_outputz00_3443))
																												(BgL_outputz00_3443,
																												BgL_objz00_1379);
																											if (CBOOL
																												(BgL__andtest_1044z00_2544))
																												{	/* Pp/pp.scm 133 */
																													return
																														ADDFX
																														(BgL_colz00_1266,
																														BINT(STRING_LENGTH
																															(BgL_objz00_1379)));
																												}
																											else
																												{	/* Pp/pp.scm 133 */
																													return BFALSE;
																												}
																										}
																									else
																										{	/* Pp/pp.scm 133 */
																											return BFALSE;
																										}
																								}
																							else
																								{	/* Pp/pp.scm 212 */
																									obj_t BgL_g1048z00_1380;

																									{	/* Pp/pp.scm 214 */
																										obj_t BgL_arg1379z00_1394;

																										if (BGl_bigloozd2strictzd2r5rszd2stringszd2zz__paramz00())
																											{	/* Pp/pp.scm 214 */
																												BgL_arg1379z00_1394 =
																													BGl_string1978z00zz__ppz00;
																											}
																										else
																											{	/* Pp/pp.scm 214 */
																												BgL_arg1379z00_1394 =
																													BGl_string1979z00zz__ppz00;
																											}
																										if (CBOOL(BgL_colz00_1266))
																											{	/* Pp/pp.scm 133 */
																												obj_t
																													BgL__andtest_1044z00_2550;
																												BgL__andtest_1044z00_2550
																													=
																													((obj_t(*)(obj_t,
																															obj_t))
																													PROCEDURE_L_ENTRY
																													(BgL_outputz00_3443))
																													(BgL_outputz00_3443,
																													BgL_arg1379z00_1394);
																												if (CBOOL
																													(BgL__andtest_1044z00_2550))
																													{	/* Pp/pp.scm 133 */
																														BgL_g1048z00_1380 =
																															ADDFX
																															(BgL_colz00_1266,
																															BINT(STRING_LENGTH
																																(BgL_arg1379z00_1394)));
																													}
																												else
																													{	/* Pp/pp.scm 133 */
																														BgL_g1048z00_1380 =
																															BFALSE;
																													}
																											}
																										else
																											{	/* Pp/pp.scm 133 */
																												BgL_g1048z00_1380 =
																													BFALSE;
																											}
																									}
																									{
																										long BgL_jz00_2604;

																										BgL_jz00_2604 = 0L;
																									BgL_loopz00_2603:
																										{	/* Pp/pp.scm 218 */
																											bool_t
																												BgL_test2211z00_5103;
																											if (CBOOL
																												(BgL_g1048z00_1380))
																												{	/* Pp/pp.scm 218 */
																													BgL_test2211z00_5103 =
																														(BgL_jz00_2604 <
																														STRING_LENGTH
																														(BgL_objz00_1379));
																												}
																											else
																												{	/* Pp/pp.scm 218 */
																													BgL_test2211z00_5103 =
																														((bool_t) 0);
																												}
																											if (BgL_test2211z00_5103)
																												{
																													long BgL_jz00_5108;

																													BgL_jz00_5108 =
																														(BgL_jz00_2604 +
																														1L);
																													BgL_jz00_2604 =
																														BgL_jz00_5108;
																													goto BgL_loopz00_2603;
																												}
																											else
																												{	/* Pp/pp.scm 222 */
																													obj_t
																														BgL_arg1376z00_2609;
																													BgL_arg1376z00_2609 =
																														BGl_outze70ze7zz__ppz00
																														(BgL_outputz00_3443,
																														c_substring
																														(BgL_objz00_1379,
																															0L,
																															BgL_jz00_2604),
																														BgL_g1048z00_1380);
																													if (CBOOL
																														(BgL_arg1376z00_2609))
																														{	/* Pp/pp.scm 133 */
																															obj_t
																																BgL__andtest_1044z00_2620;
																															BgL__andtest_1044z00_2620
																																=
																																((obj_t(*)
																																	(obj_t,
																																		obj_t))
																																PROCEDURE_L_ENTRY
																																(BgL_outputz00_3443))
																																(BgL_outputz00_3443,
																																BGl_string1979z00zz__ppz00);
																															if (CBOOL
																																(BgL__andtest_1044z00_2620))
																																{	/* Pp/pp.scm 133 */
																																	return
																																		ADDFX
																																		(BgL_arg1376z00_2609,
																																		BINT(1L));
																																}
																															else
																																{	/* Pp/pp.scm 133 */
																																	return BFALSE;
																																}
																														}
																													else
																														{	/* Pp/pp.scm 133 */
																															return BFALSE;
																														}
																												}
																										}
																									}
																								}
																						}
																					else
																						{	/* Pp/pp.scm 208 */
																							if (CHARP(BgL_objz00_1265))
																								{	/* Pp/pp.scm 224 */
																									if (CBOOL
																										(BgL_displayzf3zf3_3444))
																										{	/* Pp/pp.scm 226 */
																											obj_t BgL_arg1382z00_1397;

																											BgL_arg1382z00_1397 =
																												make_string(1L,
																												CCHAR(BgL_objz00_1265));
																											if (CBOOL
																												(BgL_colz00_1266))
																												{	/* Pp/pp.scm 133 */
																													obj_t
																														BgL__andtest_1044z00_2626;
																													BgL__andtest_1044z00_2626
																														=
																														((obj_t(*)(obj_t,
																																obj_t))
																														PROCEDURE_L_ENTRY
																														(BgL_outputz00_3443))
																														(BgL_outputz00_3443,
																														BgL_arg1382z00_1397);
																													if (CBOOL
																														(BgL__andtest_1044z00_2626))
																														{	/* Pp/pp.scm 133 */
																															return
																																ADDFX
																																(BgL_colz00_1266,
																																BINT
																																(STRING_LENGTH
																																	(BgL_arg1382z00_1397)));
																														}
																													else
																														{	/* Pp/pp.scm 133 */
																															return BFALSE;
																														}
																												}
																											else
																												{	/* Pp/pp.scm 133 */
																													return BFALSE;
																												}
																										}
																									else
																										{	/* Pp/pp.scm 227 */
																											obj_t BgL_pz00_1398;

																											{	/* Pp/pp.scm 227 */

																												{	/* Ieee/port.scm 485 */

																													BgL_pz00_1398 =
																														BGl_openzd2outputzd2stringz00zz__r4_ports_6_10_1z00
																														(BTRUE);
																												}
																											}
																											{	/* Pp/pp.scm 228 */
																												obj_t
																													BgL_list1383z00_1399;
																												BgL_list1383z00_1399 =
																													MAKE_YOUNG_PAIR
																													(BgL_pz00_1398, BNIL);
																												BGl_writez00zz__r4_output_6_10_3z00
																													(BgL_objz00_1265,
																													BgL_list1383z00_1399);
																											}
																											{	/* Pp/pp.scm 229 */
																												obj_t
																													BgL_arg1384z00_1400;
																												BgL_arg1384z00_1400 =
																													bgl_close_output_port
																													(BgL_pz00_1398);
																												if (CBOOL
																													(BgL_colz00_1266))
																													{	/* Pp/pp.scm 133 */
																														obj_t
																															BgL__andtest_1044z00_2633;
																														BgL__andtest_1044z00_2633
																															=
																															((obj_t(*)(obj_t,
																																	obj_t))
																															PROCEDURE_L_ENTRY
																															(BgL_outputz00_3443))
																															(BgL_outputz00_3443,
																															BgL_arg1384z00_1400);
																														if (CBOOL
																															(BgL__andtest_1044z00_2633))
																															{	/* Pp/pp.scm 133 */
																																return
																																	ADDFX
																																	(BgL_colz00_1266,
																																	BINT
																																	(STRING_LENGTH
																																		(((obj_t)
																																				BgL_arg1384z00_1400))));
																															}
																														else
																															{	/* Pp/pp.scm 133 */
																																return BFALSE;
																															}
																													}
																												else
																													{	/* Pp/pp.scm 133 */
																														return BFALSE;
																													}
																											}
																										}
																								}
																							else
																								{	/* Pp/pp.scm 224 */
																									if (INPUT_PORTP
																										(BgL_objz00_1265))
																										{	/* Pp/pp.scm 230 */
																											if (CBOOL
																												(BgL_colz00_1266))
																												{	/* Pp/pp.scm 133 */
																													obj_t
																														BgL__andtest_1044z00_2639;
																													BgL__andtest_1044z00_2639
																														=
																														((obj_t(*)(obj_t,
																																obj_t))
																														PROCEDURE_L_ENTRY
																														(BgL_outputz00_3443))
																														(BgL_outputz00_3443,
																														BGl_string1980z00zz__ppz00);
																													if (CBOOL
																														(BgL__andtest_1044z00_2639))
																														{	/* Pp/pp.scm 133 */
																															return
																																ADDFX
																																(BgL_colz00_1266,
																																BINT(13L));
																														}
																													else
																														{	/* Pp/pp.scm 133 */
																															return BFALSE;
																														}
																												}
																											else
																												{	/* Pp/pp.scm 133 */
																													return BFALSE;
																												}
																										}
																									else
																										{	/* Pp/pp.scm 230 */
																											if (OUTPUT_PORTP
																												(BgL_objz00_1265))
																												{	/* Pp/pp.scm 232 */
																													if (CBOOL
																														(BgL_colz00_1266))
																														{	/* Pp/pp.scm 133 */
																															obj_t
																																BgL__andtest_1044z00_2645;
																															BgL__andtest_1044z00_2645
																																=
																																((obj_t(*)
																																	(obj_t,
																																		obj_t))
																																PROCEDURE_L_ENTRY
																																(BgL_outputz00_3443))
																																(BgL_outputz00_3443,
																																BGl_string1981z00zz__ppz00);
																															if (CBOOL
																																(BgL__andtest_1044z00_2645))
																																{	/* Pp/pp.scm 133 */
																																	return
																																		ADDFX
																																		(BgL_colz00_1266,
																																		BINT(14L));
																																}
																															else
																																{	/* Pp/pp.scm 133 */
																																	return BFALSE;
																																}
																														}
																													else
																														{	/* Pp/pp.scm 133 */
																															return BFALSE;
																														}
																												}
																											else
																												{	/* Pp/pp.scm 232 */
																													if (EOF_OBJECTP
																														(BgL_objz00_1265))
																														{	/* Pp/pp.scm 234 */
																															if (CBOOL
																																(BgL_colz00_1266))
																																{	/* Pp/pp.scm 133 */
																																	obj_t
																																		BgL__andtest_1044z00_2651;
																																	BgL__andtest_1044z00_2651
																																		=
																																		((obj_t(*)
																																			(obj_t,
																																				obj_t))
																																		PROCEDURE_L_ENTRY
																																		(BgL_outputz00_3443))
																																		(BgL_outputz00_3443,
																																		BGl_string1982z00zz__ppz00);
																																	if (CBOOL
																																		(BgL__andtest_1044z00_2651))
																																		{	/* Pp/pp.scm 133 */
																																			return
																																				ADDFX
																																				(BgL_colz00_1266,
																																				BINT
																																				(13L));
																																		}
																																	else
																																		{	/* Pp/pp.scm 133 */
																																			return
																																				BFALSE;
																																		}
																																}
																															else
																																{	/* Pp/pp.scm 133 */
																																	return BFALSE;
																																}
																														}
																													else
																														{	/* Pp/pp.scm 234 */
																															if (BGL_OBJECTP
																																(BgL_objz00_1265))
																																{	/* Pp/pp.scm 238 */
																																	obj_t
																																		BgL_arg1389z00_1406;
																																	{	/* Pp/pp.scm 238 */
																																		obj_t
																																			BgL_arg1390z00_1407;
																																		{	/* Pp/pp.scm 238 */
																																			obj_t
																																				BgL_arg1391z00_1408;
																																			{	/* Pp/pp.scm 238 */
																																				obj_t
																																					BgL_arg1392z00_1409;
																																				{	/* Pp/pp.scm 238 */
																																					obj_t
																																						BgL_arg1933z00_2657;
																																					long
																																						BgL_arg1934z00_2658;
																																					BgL_arg1933z00_2657
																																						=
																																						(BGl_za2classesza2z00zz__objectz00);
																																					{	/* Pp/pp.scm 238 */
																																						long
																																							BgL_arg1935z00_2659;
																																						BgL_arg1935z00_2659
																																							=
																																							BGL_OBJECT_CLASS_NUM
																																							(((BgL_objectz00_bglt) BgL_objz00_1265));
																																						BgL_arg1934z00_2658
																																							=
																																							(BgL_arg1935z00_2659
																																							-
																																							OBJECT_TYPE);
																																					}
																																					BgL_arg1392z00_1409
																																						=
																																						VECTOR_REF
																																						(BgL_arg1933z00_2657,
																																						BgL_arg1934z00_2658);
																																				}
																																				BgL_arg1391z00_1408
																																					=
																																					BGl_classzd2namezd2zz__objectz00
																																					(BgL_arg1392z00_1409);
																																			}
																																			{	/* Pp/pp.scm 238 */
																																				obj_t
																																					BgL_arg1744z00_2666;
																																				BgL_arg1744z00_2666
																																					=
																																					SYMBOL_TO_STRING
																																					(BgL_arg1391z00_1408);
																																				BgL_arg1390z00_1407
																																					=
																																					BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																					(BgL_arg1744z00_2666);
																																		}}
																																		BgL_arg1389z00_1406
																																			=
																																			string_append_3
																																			(BGl_string1983z00zz__ppz00,
																																			BgL_arg1390z00_1407,
																																			BGl_string1984z00zz__ppz00);
																																	}
																																	if (CBOOL
																																		(BgL_colz00_1266))
																																		{	/* Pp/pp.scm 133 */
																																			obj_t
																																				BgL__andtest_1044z00_2668;
																																			BgL__andtest_1044z00_2668
																																				=
																																				((obj_t
																																					(*)
																																					(obj_t,
																																						obj_t))
																																				PROCEDURE_L_ENTRY
																																				(BgL_outputz00_3443))
																																				(BgL_outputz00_3443,
																																				BgL_arg1389z00_1406);
																																			if (CBOOL
																																				(BgL__andtest_1044z00_2668))
																																				{	/* Pp/pp.scm 133 */
																																					return
																																						ADDFX
																																						(BgL_colz00_1266,
																																						BINT
																																						(STRING_LENGTH
																																							(BgL_arg1389z00_1406)));
																																				}
																																			else
																																				{	/* Pp/pp.scm 133 */
																																					return
																																						BFALSE;
																																				}
																																		}
																																	else
																																		{	/* Pp/pp.scm 133 */
																																			return
																																				BFALSE;
																																		}
																																}
																															else
																																{	/* Pp/pp.scm 241 */
																																	obj_t
																																		BgL_arg1393z00_1410;
																																	{	/* Pp/pp.scm 241 */
																																		obj_t
																																			BgL_pz00_1411;
																																		{	/* Pp/pp.scm 241 */

																																			{	/* Ieee/port.scm 485 */

																																				BgL_pz00_1411
																																					=
																																					BGl_openzd2outputzd2stringz00zz__r4_ports_6_10_1z00
																																					(BTRUE);
																																			}
																																		}
																																		{	/* Pp/pp.scm 242 */
																																			obj_t
																																				BgL_list1394z00_1412;
																																			BgL_list1394z00_1412
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_pz00_1411,
																																				BNIL);
																																			BGl_writez00zz__r4_output_6_10_3z00
																																				(BgL_objz00_1265,
																																				BgL_list1394z00_1412);
																																		}
																																		BgL_arg1393z00_1410
																																			=
																																			bgl_close_output_port
																																			(BgL_pz00_1411);
																																	}
																																	if (CBOOL
																																		(BgL_colz00_1266))
																																		{	/* Pp/pp.scm 133 */
																																			obj_t
																																				BgL__andtest_1044z00_2675;
																																			BgL__andtest_1044z00_2675
																																				=
																																				((obj_t
																																					(*)
																																					(obj_t,
																																						obj_t))
																																				PROCEDURE_L_ENTRY
																																				(BgL_outputz00_3443))
																																				(BgL_outputz00_3443,
																																				BgL_arg1393z00_1410);
																																			if (CBOOL
																																				(BgL__andtest_1044z00_2675))
																																				{	/* Pp/pp.scm 133 */
																																					return
																																						ADDFX
																																						(BgL_colz00_1266,
																																						BINT
																																						(STRING_LENGTH
																																							(((obj_t) BgL_arg1393z00_1410))));
																																				}
																																			else
																																				{	/* Pp/pp.scm 133 */
																																					return
																																						BFALSE;
																																				}
																																		}
																																	else
																																		{	/* Pp/pp.scm 133 */
																																			return
																																				BFALSE;
																																		}
																																}
																														}
																												}
																										}
																								}
																						}
																				}
																		}
																}
														}
												}
										}
								}
						}
				}
			}
		}

	}



/* out~0 */
	obj_t BGl_outze70ze7zz__ppz00(obj_t BgL_outputz00_3804, obj_t BgL_strz00_1259,
		obj_t BgL_colz00_1260)
	{
		{	/* Pp/pp.scm 133 */
			if (CBOOL(BgL_colz00_1260))
				{	/* Pp/pp.scm 133 */
					obj_t BgL__andtest_1044z00_2329;

					BgL__andtest_1044z00_2329 =
						((obj_t(*)(obj_t,
								obj_t))
						PROCEDURE_L_ENTRY(BgL_outputz00_3804)) (BgL_outputz00_3804,
						BgL_strz00_1259);
					if (CBOOL(BgL__andtest_1044z00_2329))
						{	/* Pp/pp.scm 133 */
							return
								ADDFX(BgL_colz00_1260,
								BINT(STRING_LENGTH(((obj_t) BgL_strz00_1259))));
						}
					else
						{	/* Pp/pp.scm 133 */
							return BFALSE;
						}
				}
			else
				{	/* Pp/pp.scm 133 */
					return BFALSE;
				}
		}

	}



/* &read-macro? */
	bool_t BGl_z62readzd2macrozf3z43zz__ppz00(obj_t BgL_lz00_1230)
	{
		{	/* Pp/pp.scm 115 */
			{	/* Pp/pp.scm 116 */
				obj_t BgL_headz00_1233;
				obj_t BgL_tailz00_1234;

				BgL_headz00_1233 = CAR(((obj_t) BgL_lz00_1230));
				BgL_tailz00_1234 = CDR(((obj_t) BgL_lz00_1230));
				{	/* Pp/pp.scm 117 */
					bool_t BgL_test2237z00_5245;

					{	/* Pp/pp.scm 117 */
						bool_t BgL__ortest_1040z00_1240;

						BgL__ortest_1040z00_1240 =
							(BgL_headz00_1233 == BGl_symbol1985z00zz__ppz00);
						if (BgL__ortest_1040z00_1240)
							{	/* Pp/pp.scm 117 */
								BgL_test2237z00_5245 = BgL__ortest_1040z00_1240;
							}
						else
							{	/* Pp/pp.scm 117 */
								bool_t BgL__ortest_1041z00_1241;

								BgL__ortest_1041z00_1241 =
									(BgL_headz00_1233 == BGl_symbol1987z00zz__ppz00);
								if (BgL__ortest_1041z00_1241)
									{	/* Pp/pp.scm 117 */
										BgL_test2237z00_5245 = BgL__ortest_1041z00_1241;
									}
								else
									{	/* Pp/pp.scm 117 */
										bool_t BgL__ortest_1042z00_1242;

										BgL__ortest_1042z00_1242 =
											(BgL_headz00_1233 == BGl_symbol1989z00zz__ppz00);
										if (BgL__ortest_1042z00_1242)
											{	/* Pp/pp.scm 117 */
												BgL_test2237z00_5245 = BgL__ortest_1042z00_1242;
											}
										else
											{	/* Pp/pp.scm 117 */
												BgL_test2237z00_5245 =
													(BgL_headz00_1233 == BGl_symbol1991z00zz__ppz00);
											}
									}
							}
					}
					if (BgL_test2237z00_5245)
						{	/* Pp/pp.scm 117 */
							if (PAIRP(BgL_tailz00_1234))
								{	/* Pp/pp.scm 115 */
									return NULLP(CDR(BgL_tailz00_1234));
								}
							else
								{	/* Pp/pp.scm 115 */
									return ((bool_t) 0);
								}
						}
					else
						{	/* Pp/pp.scm 117 */
							return ((bool_t) 0);
						}
				}
			}
		}

	}



/* &read-macro-prefix */
	obj_t BGl_z62readzd2macrozd2prefixz62zz__ppz00(obj_t BgL_lz00_1250)
	{
		{	/* Pp/pp.scm 130 */
			{	/* Pp/pp.scm 125 */
				obj_t BgL_headz00_1252;

				BgL_headz00_1252 = CAR(((obj_t) BgL_lz00_1250));
				if ((BgL_headz00_1252 == BGl_symbol1985z00zz__ppz00))
					{	/* Pp/pp.scm 126 */
						return BGl_string1993z00zz__ppz00;
					}
				else
					{	/* Pp/pp.scm 126 */
						if ((BgL_headz00_1252 == BGl_symbol1987z00zz__ppz00))
							{	/* Pp/pp.scm 126 */
								return BGl_string1994z00zz__ppz00;
							}
						else
							{	/* Pp/pp.scm 126 */
								if ((BgL_headz00_1252 == BGl_symbol1989z00zz__ppz00))
									{	/* Pp/pp.scm 126 */
										return BGl_string1995z00zz__ppz00;
									}
								else
									{	/* Pp/pp.scm 126 */
										if ((BgL_headz00_1252 == BGl_symbol1991z00zz__ppz00))
											{	/* Pp/pp.scm 126 */
												return BGl_string1996z00zz__ppz00;
											}
										else
											{	/* Pp/pp.scm 126 */
												return BUNSPEC;
											}
									}
							}
					}
			}
		}

	}



/* &pr */
	obj_t BGl_z62prz62zz__ppz00(obj_t BgL_ppzd2exprzd2_3463,
		obj_t BgL_outputz00_3462, obj_t BgL_displayzf3zf3_3461,
		obj_t BgL_widthz00_3460, long BgL_maxzd2callzd2headzd2widthzd2_3459,
		obj_t BgL_ppzd2lambdazd2_3458, obj_t BgL_ppzd2definezd2_3457,
		obj_t BgL_ppzd2defunzd2_3456, obj_t BgL_ppzd2ifzd2_3455,
		obj_t BgL_ppzd2condzd2_3454, obj_t BgL_ppzd2casezd2_3453,
		obj_t BgL_ppzd2andzd2_3452, obj_t BgL_ppzd2letzd2_3451,
		obj_t BgL_ppzd2beginzd2_3450, obj_t BgL_ppzd2dozd2_3449,
		obj_t BgL_ppzd2commentzd2_3448, obj_t BgL_ppzd2exprzd2listz00_3447,
		obj_t BgL_ppzd2exprzd2defunz00_3446, long BgL_indentzd2generalzd2_3445,
		obj_t BgL_objz00_1507, obj_t BgL_colz00_1508, obj_t BgL_extraz00_1509,
		obj_t BgL_ppzd2pairzd2_1510)
	{
		{	/* Pp/pp.scm 277 */
			{	/* Pp/pp.scm 262 */
				bool_t BgL_test2246z00_5267;

				if (PAIRP(BgL_objz00_1507))
					{	/* Pp/pp.scm 262 */
						BgL_test2246z00_5267 = ((bool_t) 1);
					}
				else
					{	/* Pp/pp.scm 262 */
						BgL_test2246z00_5267 = VECTORP(BgL_objz00_1507);
					}
				if (BgL_test2246z00_5267)
					{	/* Pp/pp.scm 263 */
						obj_t BgL_resultz00_3464;
						obj_t BgL_leftz00_3465;

						BgL_resultz00_3464 = MAKE_CELL(BNIL);
						BgL_leftz00_3465 =
							MAKE_CELL(BGl_2minz00zz__r4_numbers_6_5z00(BINT(
									((((long) CINT(BgL_widthz00_3460) -
												(long) CINT(BgL_colz00_1508)) -
											(long) CINT(BgL_extraz00_1509)) + 1L)), BINT(50L)));
						{	/* Pp/pp.scm 267 */
							obj_t BgL_zc3z04anonymousza31435ze3z87_3421;

							{
								int BgL_tmpz00_5280;

								BgL_tmpz00_5280 = (int) (2L);
								BgL_zc3z04anonymousza31435ze3z87_3421 =
									MAKE_L_PROCEDURE(BGl_z62zc3z04anonymousza31435ze3ze5zz__ppz00,
									BgL_tmpz00_5280);
							}
							PROCEDURE_L_SET(BgL_zc3z04anonymousza31435ze3z87_3421,
								(int) (0L), ((obj_t) BgL_resultz00_3464));
							PROCEDURE_L_SET(BgL_zc3z04anonymousza31435ze3z87_3421,
								(int) (1L), ((obj_t) BgL_leftz00_3465));
							BGl_genericzd2writezd2zz__ppz00(BgL_objz00_1507,
								BgL_displayzf3zf3_3461, BFALSE,
								BgL_zc3z04anonymousza31435ze3z87_3421);
						}
						if (((long) CINT(CELL_REF(BgL_leftz00_3465)) > 0L))
							{	/* Pp/pp.scm 271 */
								obj_t BgL_arg1438z00_1522;

								BgL_arg1438z00_1522 =
									BGl_reversezd2stringzd2appendz00zz__ppz00(CELL_REF
									(BgL_resultz00_3464));
								if (CBOOL(BgL_colz00_1508))
									{	/* Pp/pp.scm 133 */
										obj_t BgL__andtest_1044z00_2744;

										BgL__andtest_1044z00_2744 =
											((obj_t(*)(obj_t,
													obj_t))
											PROCEDURE_L_ENTRY(BgL_outputz00_3462))
											(BgL_outputz00_3462, BgL_arg1438z00_1522);
										if (CBOOL(BgL__andtest_1044z00_2744))
											{	/* Pp/pp.scm 133 */
												return
													ADDFX(BgL_colz00_1508,
													BINT(STRING_LENGTH(BgL_arg1438z00_1522)));
											}
										else
											{	/* Pp/pp.scm 133 */
												return BFALSE;
											}
									}
								else
									{	/* Pp/pp.scm 133 */
										return BFALSE;
									}
							}
						else
							{	/* Pp/pp.scm 270 */
								if (PAIRP(BgL_objz00_1507))
									{	/* Pp/pp.scm 272 */
										return
											BGL_PROCEDURE_CALL3(BgL_ppzd2pairzd2_1510,
											BgL_objz00_1507, BgL_colz00_1508, BgL_extraz00_1509);
									}
								else
									{	/* Pp/pp.scm 274 */
										obj_t BgL_arg1440z00_1524;
										obj_t BgL_arg1441z00_1525;

										BgL_arg1440z00_1524 =
											BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00
											(BgL_objz00_1507);
										{	/* Pp/pp.scm 275 */
											obj_t BgL_arg1442z00_1526;

											BgL_arg1442z00_1526 =
												BGl_vectorzd2prefixzd2zz__ppz00(BgL_objz00_1507);
											if (CBOOL(BgL_colz00_1508))
												{	/* Pp/pp.scm 133 */
													obj_t BgL__andtest_1044z00_2750;

													BgL__andtest_1044z00_2750 =
														((obj_t(*)(obj_t,
																obj_t))
														PROCEDURE_L_ENTRY(BgL_outputz00_3462))
														(BgL_outputz00_3462, BgL_arg1442z00_1526);
													if (CBOOL(BgL__andtest_1044z00_2750))
														{	/* Pp/pp.scm 133 */
															BgL_arg1441z00_1525 =
																ADDFX(BgL_colz00_1508,
																BINT(STRING_LENGTH(BgL_arg1442z00_1526)));
														}
													else
														{	/* Pp/pp.scm 133 */
															BgL_arg1441z00_1525 = BFALSE;
														}
												}
											else
												{	/* Pp/pp.scm 133 */
													BgL_arg1441z00_1525 = BFALSE;
												}
										}
										{	/* Pp/pp.scm 308 */
											obj_t BgL_colz00_2755;

											if (CBOOL(BgL_arg1441z00_1525))
												{	/* Pp/pp.scm 133 */
													obj_t BgL__andtest_1044z00_2757;

													BgL__andtest_1044z00_2757 =
														((obj_t(*)(obj_t,
																obj_t))
														PROCEDURE_L_ENTRY(BgL_outputz00_3462))
														(BgL_outputz00_3462, BGl_string1960z00zz__ppz00);
													if (CBOOL(BgL__andtest_1044z00_2757))
														{	/* Pp/pp.scm 133 */
															BgL_colz00_2755 =
																ADDFX(BgL_arg1441z00_1525, BINT(1L));
														}
													else
														{	/* Pp/pp.scm 133 */
															BgL_colz00_2755 = BFALSE;
														}
												}
											else
												{	/* Pp/pp.scm 133 */
													BgL_colz00_2755 = BFALSE;
												}
											return
												BGl_z62ppzd2downzb0zz__ppz00(BgL_outputz00_3462,
												BgL_ppzd2exprzd2_3463, BgL_displayzf3zf3_3461,
												BgL_widthz00_3460,
												BgL_maxzd2callzd2headzd2widthzd2_3459,
												BgL_ppzd2lambdazd2_3458, BgL_ppzd2definezd2_3457,
												BgL_ppzd2defunzd2_3456, BgL_ppzd2ifzd2_3455,
												BgL_ppzd2condzd2_3454, BgL_ppzd2casezd2_3453,
												BgL_ppzd2andzd2_3452, BgL_ppzd2letzd2_3451,
												BgL_ppzd2beginzd2_3450, BgL_ppzd2dozd2_3449,
												BgL_ppzd2commentzd2_3448, BgL_ppzd2exprzd2listz00_3447,
												BgL_ppzd2exprzd2defunz00_3446,
												BgL_indentzd2generalzd2_3445, BgL_arg1440z00_1524,
												BgL_colz00_2755, BgL_colz00_2755, BgL_extraz00_1509,
												BgL_ppzd2exprzd2_3463);
										}
									}
							}
					}
				else
					{	/* Pp/pp.scm 262 */
						return
							BGl_z62wrz62zz__ppz00(BgL_displayzf3zf3_3461, BgL_outputz00_3462,
							BgL_objz00_1507, BgL_colz00_1508);
					}
			}
		}

	}



/* &indent */
	obj_t BGl_z62indentz62zz__ppz00(obj_t BgL_outputz00_3468,
		obj_t BgL_toz00_1499, obj_t BgL_colz00_1500)
	{
		{	/* Pp/pp.scm 259 */
			{
				long BgL_nz00_1491;
				obj_t BgL_colz00_1492;

				if (CBOOL(BgL_colz00_1500))
					{	/* Pp/pp.scm 256 */
						if (((long) CINT(BgL_toz00_1499) < (long) CINT(BgL_colz00_1500)))
							{	/* Pp/pp.scm 258 */
								obj_t BgL__andtest_1050z00_1504;

								{	/* Pp/pp.scm 258 */
									obj_t BgL_arg1428z00_1505;

									BgL_arg1428z00_1505 = make_string(1L, ((unsigned char) 10));
									if (CBOOL(BgL_colz00_1500))
										{	/* Pp/pp.scm 133 */
											obj_t BgL__andtest_1044z00_2700;

											BgL__andtest_1044z00_2700 =
												((obj_t(*)(obj_t,
														obj_t))
												PROCEDURE_L_ENTRY(BgL_outputz00_3468))
												(BgL_outputz00_3468, BgL_arg1428z00_1505);
											if (CBOOL(BgL__andtest_1044z00_2700))
												{	/* Pp/pp.scm 133 */
													BgL__andtest_1050z00_1504 =
														ADDFX(BgL_colz00_1500,
														BINT(STRING_LENGTH(BgL_arg1428z00_1505)));
												}
											else
												{	/* Pp/pp.scm 133 */
													BgL__andtest_1050z00_1504 = BFALSE;
												}
										}
									else
										{	/* Pp/pp.scm 133 */
											BgL__andtest_1050z00_1504 = BFALSE;
										}
								}
								if (CBOOL(BgL__andtest_1050z00_1504))
									{
										obj_t BgL_nz00_2706;
										obj_t BgL_colz00_2707;

										BgL_nz00_2706 = BgL_toz00_1499;
										BgL_colz00_2707 = BINT(0L);
									BgL_spacesz00_2705:
										if (((long) CINT(BgL_nz00_2706) > 0L))
											{	/* Pp/pp.scm 249 */
												if (((long) CINT(BgL_nz00_2706) > 7L))
													{	/* Pp/pp.scm 251 */
														long BgL_arg1423z00_2712;
														obj_t BgL_arg1424z00_2713;

														BgL_arg1423z00_2712 =
															((long) CINT(BgL_nz00_2706) - 8L);
														if (CBOOL(BgL_colz00_2707))
															{	/* Pp/pp.scm 133 */
																obj_t BgL__andtest_1044z00_2716;

																BgL__andtest_1044z00_2716 =
																	((obj_t(*)(obj_t,
																			obj_t))
																	PROCEDURE_L_ENTRY(BgL_outputz00_3468))
																	(BgL_outputz00_3468,
																	BGl_string1997z00zz__ppz00);
																if (CBOOL(BgL__andtest_1044z00_2716))
																	{	/* Pp/pp.scm 133 */
																		BgL_arg1424z00_2713 =
																			ADDFX(BgL_colz00_2707, BINT(8L));
																	}
																else
																	{	/* Pp/pp.scm 133 */
																		BgL_arg1424z00_2713 = BFALSE;
																	}
															}
														else
															{	/* Pp/pp.scm 133 */
																BgL_arg1424z00_2713 = BFALSE;
															}
														{
															obj_t BgL_colz00_5378;
															obj_t BgL_nz00_5376;

															BgL_nz00_5376 = BINT(BgL_arg1423z00_2712);
															BgL_colz00_5378 = BgL_arg1424z00_2713;
															BgL_colz00_2707 = BgL_colz00_5378;
															BgL_nz00_2706 = BgL_nz00_5376;
															goto BgL_spacesz00_2705;
														}
													}
												else
													{	/* Pp/pp.scm 252 */
														obj_t BgL_arg1425z00_2721;

														BgL_arg1425z00_2721 =
															c_substring(BGl_string1997z00zz__ppz00, 0L,
															(long) CINT(BgL_nz00_2706));
														if (CBOOL(BgL_colz00_2707))
															{	/* Pp/pp.scm 133 */
																obj_t BgL__andtest_1044z00_2725;

																BgL__andtest_1044z00_2725 =
																	((obj_t(*)(obj_t,
																			obj_t))
																	PROCEDURE_L_ENTRY(BgL_outputz00_3468))
																	(BgL_outputz00_3468, BgL_arg1425z00_2721);
																if (CBOOL(BgL__andtest_1044z00_2725))
																	{	/* Pp/pp.scm 133 */
																		return
																			ADDFX(BgL_colz00_2707,
																			BINT(STRING_LENGTH(BgL_arg1425z00_2721)));
																	}
																else
																	{	/* Pp/pp.scm 133 */
																		return BFALSE;
																	}
															}
														else
															{	/* Pp/pp.scm 133 */
																return BFALSE;
															}
													}
											}
										else
											{	/* Pp/pp.scm 249 */
												return BgL_colz00_2707;
											}
									}
								else
									{	/* Pp/pp.scm 258 */
										return BFALSE;
									}
							}
						else
							{	/* Pp/pp.scm 257 */
								BgL_nz00_1491 =
									((long) CINT(BgL_toz00_1499) - (long) CINT(BgL_colz00_1500));
								BgL_colz00_1492 = BgL_colz00_1500;
							BgL_zc3z04anonymousza31420ze3z87_1493:
								if ((BgL_nz00_1491 > 0L))
									{	/* Pp/pp.scm 249 */
										if ((BgL_nz00_1491 > 7L))
											{	/* Pp/pp.scm 251 */
												long BgL_arg1423z00_1496;
												obj_t BgL_arg1424z00_1497;

												BgL_arg1423z00_1496 = (BgL_nz00_1491 - 8L);
												if (CBOOL(BgL_colz00_1492))
													{	/* Pp/pp.scm 133 */
														obj_t BgL__andtest_1044z00_2684;

														BgL__andtest_1044z00_2684 =
															((obj_t(*)(obj_t,
																	obj_t))
															PROCEDURE_L_ENTRY(BgL_outputz00_3468))
															(BgL_outputz00_3468, BGl_string1997z00zz__ppz00);
														if (CBOOL(BgL__andtest_1044z00_2684))
															{	/* Pp/pp.scm 133 */
																BgL_arg1424z00_1497 =
																	ADDFX(BgL_colz00_1492, BINT(8L));
															}
														else
															{	/* Pp/pp.scm 133 */
																BgL_arg1424z00_1497 = BFALSE;
															}
													}
												else
													{	/* Pp/pp.scm 133 */
														BgL_arg1424z00_1497 = BFALSE;
													}
												{
													obj_t BgL_colz00_5409;
													long BgL_nz00_5408;

													BgL_nz00_5408 = BgL_arg1423z00_1496;
													BgL_colz00_5409 = BgL_arg1424z00_1497;
													BgL_colz00_1492 = BgL_colz00_5409;
													BgL_nz00_1491 = BgL_nz00_5408;
													goto BgL_zc3z04anonymousza31420ze3z87_1493;
												}
											}
										else
											{	/* Pp/pp.scm 252 */
												obj_t BgL_arg1425z00_1498;

												BgL_arg1425z00_1498 =
													c_substring(BGl_string1997z00zz__ppz00, 0L,
													BgL_nz00_1491);
												if (CBOOL(BgL_colz00_1492))
													{	/* Pp/pp.scm 133 */
														obj_t BgL__andtest_1044z00_2692;

														BgL__andtest_1044z00_2692 =
															((obj_t(*)(obj_t,
																	obj_t))
															PROCEDURE_L_ENTRY(BgL_outputz00_3468))
															(BgL_outputz00_3468, BgL_arg1425z00_1498);
														if (CBOOL(BgL__andtest_1044z00_2692))
															{	/* Pp/pp.scm 133 */
																return
																	ADDFX(BgL_colz00_1492,
																	BINT(STRING_LENGTH(BgL_arg1425z00_1498)));
															}
														else
															{	/* Pp/pp.scm 133 */
																return BFALSE;
															}
													}
												else
													{	/* Pp/pp.scm 133 */
														return BFALSE;
													}
											}
									}
								else
									{	/* Pp/pp.scm 249 */
										return BgL_colz00_1492;
									}
							}
					}
				else
					{	/* Pp/pp.scm 256 */
						return BFALSE;
					}
			}
		}

	}



/* &pp-general */
	obj_t BGl_z62ppzd2generalzb0zz__ppz00(long BgL_indentzd2generalzd2_3487,
		obj_t BgL_outputz00_3486, obj_t BgL_ppzd2exprzd2_3485,
		obj_t BgL_displayzf3zf3_3484, obj_t BgL_widthz00_3483,
		long BgL_maxzd2callzd2headzd2widthzd2_3482, obj_t BgL_ppzd2lambdazd2_3481,
		obj_t BgL_ppzd2definezd2_3480, obj_t BgL_ppzd2defunzd2_3479,
		obj_t BgL_ppzd2ifzd2_3478, obj_t BgL_ppzd2condzd2_3477,
		obj_t BgL_ppzd2casezd2_3476, obj_t BgL_ppzd2andzd2_3475,
		obj_t BgL_ppzd2letzd2_3474, obj_t BgL_ppzd2beginzd2_3473,
		obj_t BgL_ppzd2dozd2_3472, obj_t BgL_ppzd2commentzd2_3471,
		obj_t BgL_ppzd2exprzd2listz00_3470, obj_t BgL_ppzd2exprzd2defunz00_3469,
		obj_t BgL_exprz00_1602, obj_t BgL_colz00_1603, obj_t BgL_extraz00_1604,
		bool_t BgL_namedzf3zf3_1605, obj_t BgL_ppzd21zd2_1606,
		obj_t BgL_ppzd22zd2_1607, obj_t BgL_ppzd23zd2_1608)
	{
		{	/* Pp/pp.scm 344 */
			{
				obj_t BgL_restz00_1638;
				long BgL_col1z00_1639;
				obj_t BgL_col2z00_1640;
				long BgL_col3z00_1641;

				{	/* Pp/pp.scm 357 */
					obj_t BgL_headz00_1613;

					BgL_headz00_1613 = CAR(((obj_t) BgL_exprz00_1602));
					{	/* Pp/pp.scm 357 */
						obj_t BgL_restz00_1614;

						BgL_restz00_1614 = CDR(((obj_t) BgL_exprz00_1602));
						{	/* Pp/pp.scm 358 */
							obj_t BgL_colza2za2_1615;

							{	/* Pp/pp.scm 359 */
								obj_t BgL_arg1489z00_1625;

								if (CBOOL(BgL_colz00_1603))
									{	/* Pp/pp.scm 133 */
										obj_t BgL__andtest_1044z00_2909;

										BgL__andtest_1044z00_2909 =
											((obj_t(*)(obj_t,
													obj_t))
											PROCEDURE_L_ENTRY(BgL_outputz00_3486))
											(BgL_outputz00_3486, BGl_string1960z00zz__ppz00);
										if (CBOOL(BgL__andtest_1044z00_2909))
											{	/* Pp/pp.scm 133 */
												BgL_arg1489z00_1625 = ADDFX(BgL_colz00_1603, BINT(1L));
											}
										else
											{	/* Pp/pp.scm 133 */
												BgL_arg1489z00_1625 = BFALSE;
											}
									}
								else
									{	/* Pp/pp.scm 133 */
										BgL_arg1489z00_1625 = BFALSE;
									}
								BgL_colza2za2_1615 =
									BGl_z62wrz62zz__ppz00(BgL_displayzf3zf3_3484,
									BgL_outputz00_3486, BgL_headz00_1613, BgL_arg1489z00_1625);
							}
							{	/* Pp/pp.scm 359 */

								{	/* Pp/pp.scm 360 */
									bool_t BgL_test2275z00_5440;

									if (BgL_namedzf3zf3_1605)
										{	/* Pp/pp.scm 360 */
											BgL_test2275z00_5440 = PAIRP(BgL_restz00_1614);
										}
									else
										{	/* Pp/pp.scm 360 */
											BgL_test2275z00_5440 = ((bool_t) 0);
										}
									if (BgL_test2275z00_5440)
										{	/* Pp/pp.scm 361 */
											obj_t BgL_namez00_1617;

											BgL_namez00_1617 = CAR(BgL_restz00_1614);
											{	/* Pp/pp.scm 361 */
												obj_t BgL_restz00_1618;

												BgL_restz00_1618 = CDR(BgL_restz00_1614);
												{	/* Pp/pp.scm 362 */
													obj_t BgL_colza2za2z00_1619;

													{	/* Pp/pp.scm 363 */
														obj_t BgL_arg1486z00_1622;

														if (CBOOL(BgL_colza2za2_1615))
															{	/* Pp/pp.scm 133 */
																obj_t BgL__andtest_1044z00_2917;

																BgL__andtest_1044z00_2917 =
																	((obj_t(*)(obj_t,
																			obj_t))
																	PROCEDURE_L_ENTRY(BgL_outputz00_3486))
																	(BgL_outputz00_3486,
																	BGl_string1961z00zz__ppz00);
																if (CBOOL(BgL__andtest_1044z00_2917))
																	{	/* Pp/pp.scm 133 */
																		BgL_arg1486z00_1622 =
																			ADDFX(BgL_colza2za2_1615, BINT(1L));
																	}
																else
																	{	/* Pp/pp.scm 133 */
																		BgL_arg1486z00_1622 = BFALSE;
																	}
															}
														else
															{	/* Pp/pp.scm 133 */
																BgL_arg1486z00_1622 = BFALSE;
															}
														BgL_colza2za2z00_1619 =
															BGl_z62wrz62zz__ppz00(BgL_displayzf3zf3_3484,
															BgL_outputz00_3486, BgL_namez00_1617,
															BgL_arg1486z00_1622);
													}
													{	/* Pp/pp.scm 363 */

														{	/* Pp/pp.scm 364 */
															long BgL_arg1484z00_1620;
															long BgL_arg1485z00_1621;

															BgL_arg1484z00_1620 =
																(
																(long) CINT(BgL_colz00_1603) +
																BgL_indentzd2generalzd2_3487);
															BgL_arg1485z00_1621 =
																((long) CINT(BgL_colza2za2z00_1619) + 1L);
															{	/* Pp/pp.scm 339 */
																bool_t BgL_test2279z00_5460;

																if (CBOOL(BgL_ppzd21zd2_1606))
																	{	/* Pp/pp.scm 339 */
																		BgL_test2279z00_5460 =
																			PAIRP(BgL_restz00_1618);
																	}
																else
																	{	/* Pp/pp.scm 339 */
																		BgL_test2279z00_5460 = ((bool_t) 0);
																	}
																if (BgL_test2279z00_5460)
																	{	/* Pp/pp.scm 340 */
																		obj_t BgL_val1z00_2926;

																		BgL_val1z00_2926 = CAR(BgL_restz00_1618);
																		{	/* Pp/pp.scm 340 */
																			obj_t BgL_restz00_2927;

																			BgL_restz00_2927 = CDR(BgL_restz00_1618);
																			{	/* Pp/pp.scm 341 */
																				long BgL_extraz00_2928;

																				if (NULLP(BgL_restz00_2927))
																					{	/* Pp/pp.scm 342 */
																						BgL_extraz00_2928 =
																							(
																							(long) CINT(BgL_extraz00_1604) +
																							1L);
																					}
																				else
																					{	/* Pp/pp.scm 342 */
																						BgL_extraz00_2928 = 0L;
																					}
																				{	/* Pp/pp.scm 342 */

																					BgL_restz00_1638 = BgL_restz00_2927;
																					BgL_col1z00_1639 =
																						BgL_arg1484z00_1620;
																					BgL_col2z00_1640 =
																						BGl_z62prz62zz__ppz00
																						(BgL_ppzd2exprzd2_3485,
																						BgL_outputz00_3486,
																						BgL_displayzf3zf3_3484,
																						BgL_widthz00_3483,
																						BgL_maxzd2callzd2headzd2widthzd2_3482,
																						BgL_ppzd2lambdazd2_3481,
																						BgL_ppzd2definezd2_3480,
																						BgL_ppzd2defunzd2_3479,
																						BgL_ppzd2ifzd2_3478,
																						BgL_ppzd2condzd2_3477,
																						BgL_ppzd2casezd2_3476,
																						BgL_ppzd2andzd2_3475,
																						BgL_ppzd2letzd2_3474,
																						BgL_ppzd2beginzd2_3473,
																						BgL_ppzd2dozd2_3472,
																						BgL_ppzd2commentzd2_3471,
																						BgL_ppzd2exprzd2listz00_3470,
																						BgL_ppzd2exprzd2defunz00_3469,
																						BgL_indentzd2generalzd2_3487,
																						BgL_val1z00_2926,
																						BGl_z62indentz62zz__ppz00
																						(BgL_outputz00_3486,
																							BINT(BgL_arg1485z00_1621),
																							BgL_colza2za2z00_1619),
																						BINT(BgL_extraz00_2928),
																						BgL_ppzd21zd2_1606);
																					BgL_col3z00_1641 =
																						BgL_arg1485z00_1621;
																				BgL_zc3z04anonymousza31496ze3z87_1642:
																					{	/* Pp/pp.scm 347 */
																						bool_t BgL_test2282z00_5470;

																						if (CBOOL(BgL_ppzd22zd2_1607))
																							{	/* Pp/pp.scm 347 */
																								BgL_test2282z00_5470 =
																									PAIRP(BgL_restz00_1638);
																							}
																						else
																							{	/* Pp/pp.scm 347 */
																								BgL_test2282z00_5470 =
																									((bool_t) 0);
																							}
																						if (BgL_test2282z00_5470)
																							{	/* Pp/pp.scm 348 */
																								obj_t BgL_val1z00_2897;

																								BgL_val1z00_2897 =
																									CAR(BgL_restz00_1638);
																								{	/* Pp/pp.scm 348 */
																									obj_t BgL_restz00_2898;

																									BgL_restz00_2898 =
																										CDR(BgL_restz00_1638);
																									{	/* Pp/pp.scm 349 */
																										long BgL_extraz00_2899;

																										if (NULLP(BgL_restz00_2898))
																											{	/* Pp/pp.scm 350 */
																												BgL_extraz00_2899 =
																													(
																													(long)
																													CINT
																													(BgL_extraz00_1604) +
																													1L);
																											}
																										else
																											{	/* Pp/pp.scm 350 */
																												BgL_extraz00_2899 = 0L;
																											}
																										{	/* Pp/pp.scm 350 */

																											return
																												BGl_z62ppzd2downzb0zz__ppz00
																												(BgL_outputz00_3486,
																												BgL_ppzd2exprzd2_3485,
																												BgL_displayzf3zf3_3484,
																												BgL_widthz00_3483,
																												BgL_maxzd2callzd2headzd2widthzd2_3482,
																												BgL_ppzd2lambdazd2_3481,
																												BgL_ppzd2definezd2_3480,
																												BgL_ppzd2defunzd2_3479,
																												BgL_ppzd2ifzd2_3478,
																												BgL_ppzd2condzd2_3477,
																												BgL_ppzd2casezd2_3476,
																												BgL_ppzd2andzd2_3475,
																												BgL_ppzd2letzd2_3474,
																												BgL_ppzd2beginzd2_3473,
																												BgL_ppzd2dozd2_3472,
																												BgL_ppzd2commentzd2_3471,
																												BgL_ppzd2exprzd2listz00_3470,
																												BgL_ppzd2exprzd2defunz00_3469,
																												BgL_indentzd2generalzd2_3487,
																												BgL_restz00_2898,
																												BGl_z62prz62zz__ppz00
																												(BgL_ppzd2exprzd2_3485,
																													BgL_outputz00_3486,
																													BgL_displayzf3zf3_3484,
																													BgL_widthz00_3483,
																													BgL_maxzd2callzd2headzd2widthzd2_3482,
																													BgL_ppzd2lambdazd2_3481,
																													BgL_ppzd2definezd2_3480,
																													BgL_ppzd2defunzd2_3479,
																													BgL_ppzd2ifzd2_3478,
																													BgL_ppzd2condzd2_3477,
																													BgL_ppzd2casezd2_3476,
																													BgL_ppzd2andzd2_3475,
																													BgL_ppzd2letzd2_3474,
																													BgL_ppzd2beginzd2_3473,
																													BgL_ppzd2dozd2_3472,
																													BgL_ppzd2commentzd2_3471,
																													BgL_ppzd2exprzd2listz00_3470,
																													BgL_ppzd2exprzd2defunz00_3469,
																													BgL_indentzd2generalzd2_3487,
																													BgL_val1z00_2897,
																													BGl_z62indentz62zz__ppz00
																													(BgL_outputz00_3486,
																														BINT
																														(BgL_col3z00_1641),
																														BgL_col2z00_1640),
																													BINT
																													(BgL_extraz00_2899),
																													BgL_ppzd22zd2_1607),
																												BINT(BgL_col1z00_1639),
																												BgL_extraz00_1604,
																												BgL_ppzd23zd2_1608);
																										}
																									}
																								}
																							}
																						else
																							{	/* Pp/pp.scm 347 */
																								return
																									BGl_z62ppzd2downzb0zz__ppz00
																									(BgL_outputz00_3486,
																									BgL_ppzd2exprzd2_3485,
																									BgL_displayzf3zf3_3484,
																									BgL_widthz00_3483,
																									BgL_maxzd2callzd2headzd2widthzd2_3482,
																									BgL_ppzd2lambdazd2_3481,
																									BgL_ppzd2definezd2_3480,
																									BgL_ppzd2defunzd2_3479,
																									BgL_ppzd2ifzd2_3478,
																									BgL_ppzd2condzd2_3477,
																									BgL_ppzd2casezd2_3476,
																									BgL_ppzd2andzd2_3475,
																									BgL_ppzd2letzd2_3474,
																									BgL_ppzd2beginzd2_3473,
																									BgL_ppzd2dozd2_3472,
																									BgL_ppzd2commentzd2_3471,
																									BgL_ppzd2exprzd2listz00_3470,
																									BgL_ppzd2exprzd2defunz00_3469,
																									BgL_indentzd2generalzd2_3487,
																									BgL_restz00_1638,
																									BgL_col2z00_1640,
																									BINT(BgL_col1z00_1639),
																									BgL_extraz00_1604,
																									BgL_ppzd23zd2_1608);
																							}
																					}
																				}
																			}
																		}
																	}
																else
																	{
																		long BgL_col3z00_5495;
																		obj_t BgL_col2z00_5494;
																		long BgL_col1z00_5493;
																		obj_t BgL_restz00_5492;

																		BgL_restz00_5492 = BgL_restz00_1618;
																		BgL_col1z00_5493 = BgL_arg1484z00_1620;
																		BgL_col2z00_5494 = BgL_colza2za2z00_1619;
																		BgL_col3z00_5495 = BgL_arg1485z00_1621;
																		BgL_col3z00_1641 = BgL_col3z00_5495;
																		BgL_col2z00_1640 = BgL_col2z00_5494;
																		BgL_col1z00_1639 = BgL_col1z00_5493;
																		BgL_restz00_1638 = BgL_restz00_5492;
																		goto BgL_zc3z04anonymousza31496ze3z87_1642;
																	}
															}
														}
													}
												}
											}
										}
									else
										{	/* Pp/pp.scm 365 */
											long BgL_arg1487z00_1623;
											long BgL_arg1488z00_1624;

											BgL_arg1487z00_1623 =
												(
												(long) CINT(BgL_colz00_1603) +
												BgL_indentzd2generalzd2_3487);
											BgL_arg1488z00_1624 =
												((long) CINT(BgL_colza2za2_1615) + 1L);
											{	/* Pp/pp.scm 339 */
												bool_t BgL_test2285z00_5500;

												if (CBOOL(BgL_ppzd21zd2_1606))
													{	/* Pp/pp.scm 339 */
														BgL_test2285z00_5500 = PAIRP(BgL_restz00_1614);
													}
												else
													{	/* Pp/pp.scm 339 */
														BgL_test2285z00_5500 = ((bool_t) 0);
													}
												if (BgL_test2285z00_5500)
													{	/* Pp/pp.scm 340 */
														obj_t BgL_val1z00_2939;

														BgL_val1z00_2939 = CAR(BgL_restz00_1614);
														{	/* Pp/pp.scm 340 */
															obj_t BgL_restz00_2940;

															BgL_restz00_2940 = CDR(BgL_restz00_1614);
															{	/* Pp/pp.scm 341 */
																long BgL_extraz00_2941;

																if (NULLP(BgL_restz00_2940))
																	{	/* Pp/pp.scm 342 */
																		BgL_extraz00_2941 =
																			((long) CINT(BgL_extraz00_1604) + 1L);
																	}
																else
																	{	/* Pp/pp.scm 342 */
																		BgL_extraz00_2941 = 0L;
																	}
																{	/* Pp/pp.scm 342 */

																	{
																		long BgL_col3z00_5517;
																		obj_t BgL_col2z00_5512;
																		long BgL_col1z00_5511;
																		obj_t BgL_restz00_5510;

																		BgL_restz00_5510 = BgL_restz00_2940;
																		BgL_col1z00_5511 = BgL_arg1487z00_1623;
																		BgL_col2z00_5512 =
																			BGl_z62prz62zz__ppz00
																			(BgL_ppzd2exprzd2_3485,
																			BgL_outputz00_3486,
																			BgL_displayzf3zf3_3484, BgL_widthz00_3483,
																			BgL_maxzd2callzd2headzd2widthzd2_3482,
																			BgL_ppzd2lambdazd2_3481,
																			BgL_ppzd2definezd2_3480,
																			BgL_ppzd2defunzd2_3479,
																			BgL_ppzd2ifzd2_3478,
																			BgL_ppzd2condzd2_3477,
																			BgL_ppzd2casezd2_3476,
																			BgL_ppzd2andzd2_3475,
																			BgL_ppzd2letzd2_3474,
																			BgL_ppzd2beginzd2_3473,
																			BgL_ppzd2dozd2_3472,
																			BgL_ppzd2commentzd2_3471,
																			BgL_ppzd2exprzd2listz00_3470,
																			BgL_ppzd2exprzd2defunz00_3469,
																			BgL_indentzd2generalzd2_3487,
																			BgL_val1z00_2939,
																			BGl_z62indentz62zz__ppz00
																			(BgL_outputz00_3486,
																				BINT(BgL_arg1488z00_1624),
																				BgL_colza2za2_1615),
																			BINT(BgL_extraz00_2941),
																			BgL_ppzd21zd2_1606);
																		BgL_col3z00_5517 = BgL_arg1488z00_1624;
																		BgL_col3z00_1641 = BgL_col3z00_5517;
																		BgL_col2z00_1640 = BgL_col2z00_5512;
																		BgL_col1z00_1639 = BgL_col1z00_5511;
																		BgL_restz00_1638 = BgL_restz00_5510;
																		goto BgL_zc3z04anonymousza31496ze3z87_1642;
																	}
																}
															}
														}
													}
												else
													{
														long BgL_col3z00_5521;
														obj_t BgL_col2z00_5520;
														long BgL_col1z00_5519;
														obj_t BgL_restz00_5518;

														BgL_restz00_5518 = BgL_restz00_1614;
														BgL_col1z00_5519 = BgL_arg1487z00_1623;
														BgL_col2z00_5520 = BgL_colza2za2_1615;
														BgL_col3z00_5521 = BgL_arg1488z00_1624;
														BgL_col3z00_1641 = BgL_col3z00_5521;
														BgL_col2z00_1640 = BgL_col2z00_5520;
														BgL_col1z00_1639 = BgL_col1z00_5519;
														BgL_restz00_1638 = BgL_restz00_5518;
														goto BgL_zc3z04anonymousza31496ze3z87_1642;
													}
											}
										}
								}
							}
						}
					}
				}
			}
		}

	}



/* &pp-lambda */
	obj_t BGl_z62ppzd2lambdazb0zz__ppz00(obj_t BgL_envz00_3488,
		obj_t BgL_exprz00_3507, obj_t BgL_colz00_3508, obj_t BgL_extraz00_3509)
	{
		{	/* Pp/pp.scm 392 */
			{	/* Pp/pp.scm 388 */
				long BgL_indentzd2generalzd2_3489;
				obj_t BgL_ppzd2exprzd2defunz00_3490;
				obj_t BgL_widthz00_3491;
				obj_t BgL_displayzf3zf3_3492;
				obj_t BgL_ppzd2exprzd2listz00_3493;
				obj_t BgL_ppzd2commentzd2_3494;
				obj_t BgL_ppzd2dozd2_3495;
				obj_t BgL_ppzd2beginzd2_3496;
				obj_t BgL_ppzd2letzd2_3497;
				obj_t BgL_ppzd2andzd2_3498;
				obj_t BgL_ppzd2casezd2_3499;
				obj_t BgL_ppzd2condzd2_3500;
				obj_t BgL_ppzd2ifzd2_3501;
				obj_t BgL_ppzd2defunzd2_3502;
				obj_t BgL_ppzd2definezd2_3503;
				long BgL_maxzd2callzd2headzd2widthzd2_3504;
				obj_t BgL_outputz00_3505;
				obj_t BgL_ppzd2exprzd2_3506;

				BgL_indentzd2generalzd2_3489 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_3488, (int) (0L)));
				BgL_ppzd2exprzd2defunz00_3490 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3488, (int) (1L)));
				BgL_widthz00_3491 = PROCEDURE_REF(BgL_envz00_3488, (int) (2L));
				BgL_displayzf3zf3_3492 = PROCEDURE_REF(BgL_envz00_3488, (int) (3L));
				BgL_ppzd2exprzd2listz00_3493 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3488, (int) (4L)));
				BgL_ppzd2commentzd2_3494 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3488, (int) (5L)));
				BgL_ppzd2dozd2_3495 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3488, (int) (6L)));
				BgL_ppzd2beginzd2_3496 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3488, (int) (7L)));
				BgL_ppzd2letzd2_3497 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3488, (int) (8L)));
				BgL_ppzd2andzd2_3498 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3488, (int) (9L)));
				BgL_ppzd2casezd2_3499 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3488, (int) (10L)));
				BgL_ppzd2condzd2_3500 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3488, (int) (11L)));
				BgL_ppzd2ifzd2_3501 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3488, (int) (12L)));
				BgL_ppzd2defunzd2_3502 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3488, (int) (13L)));
				BgL_ppzd2definezd2_3503 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3488, (int) (14L)));
				BgL_maxzd2callzd2headzd2widthzd2_3504 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_3488, (int) (15L)));
				BgL_outputz00_3505 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3488, (int) (16L)));
				BgL_ppzd2exprzd2_3506 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3488, (int) (17L)));
				{	/* Pp/pp.scm 388 */
					obj_t BgL_colz00_3835;

					if (CBOOL(BgL_colz00_3508))
						{	/* Pp/pp.scm 133 */
							obj_t BgL__andtest_1044z00_3836;

							BgL__andtest_1044z00_3836 =
								((obj_t(*)(obj_t,
										obj_t))
								PROCEDURE_L_ENTRY(BgL_outputz00_3505)) (BgL_outputz00_3505,
								BGl_string1960z00zz__ppz00);
							if (CBOOL(BgL__andtest_1044z00_3836))
								{	/* Pp/pp.scm 133 */
									BgL_colz00_3835 = ADDFX(BgL_colz00_3508, BINT(1L));
								}
							else
								{	/* Pp/pp.scm 133 */
									BgL_colz00_3835 = BFALSE;
								}
						}
					else
						{	/* Pp/pp.scm 133 */
							BgL_colz00_3835 = BFALSE;
						}
					{	/* Pp/pp.scm 388 */
						obj_t BgL_col2z00_3837;

						{	/* Pp/pp.scm 389 */
							obj_t BgL_arg1511z00_3838;

							BgL_arg1511z00_3838 = CAR(((obj_t) BgL_exprz00_3507));
							BgL_col2z00_3837 =
								BGl_z62wrz62zz__ppz00(BgL_displayzf3zf3_3492,
								BgL_outputz00_3505, BgL_arg1511z00_3838, BgL_colz00_3835);
						}
						{	/* Pp/pp.scm 389 */
							obj_t BgL_col3z00_3839;

							if (CBOOL(BgL_col2z00_3837))
								{	/* Pp/pp.scm 133 */
									obj_t BgL__andtest_1044z00_3840;

									BgL__andtest_1044z00_3840 =
										((obj_t(*)(obj_t,
												obj_t))
										PROCEDURE_L_ENTRY(BgL_outputz00_3505)) (BgL_outputz00_3505,
										BGl_string1961z00zz__ppz00);
									if (CBOOL(BgL__andtest_1044z00_3840))
										{	/* Pp/pp.scm 133 */
											BgL_col3z00_3839 = ADDFX(BgL_col2z00_3837, BINT(1L));
										}
									else
										{	/* Pp/pp.scm 133 */
											BgL_col3z00_3839 = BFALSE;
										}
								}
							else
								{	/* Pp/pp.scm 133 */
									BgL_col3z00_3839 = BFALSE;
								}
							{	/* Pp/pp.scm 390 */
								obj_t BgL_col4z00_3841;

								{	/* Pp/pp.scm 391 */
									obj_t BgL_arg1510z00_3842;

									{	/* Pp/pp.scm 391 */
										obj_t BgL_pairz00_3843;

										BgL_pairz00_3843 = CDR(((obj_t) BgL_exprz00_3507));
										BgL_arg1510z00_3842 = CAR(BgL_pairz00_3843);
									}
									BgL_col4z00_3841 =
										BGl_z62wrz62zz__ppz00(BgL_displayzf3zf3_3492,
										BgL_outputz00_3505, BgL_arg1510z00_3842, BgL_col3z00_3839);
								}
								{	/* Pp/pp.scm 391 */

									{	/* Pp/pp.scm 392 */
										obj_t BgL_arg1508z00_3844;
										long BgL_arg1509z00_3845;

										{	/* Pp/pp.scm 392 */
											obj_t BgL_pairz00_3846;

											BgL_pairz00_3846 = CDR(((obj_t) BgL_exprz00_3507));
											BgL_arg1508z00_3844 = CDR(BgL_pairz00_3846);
										}
										BgL_arg1509z00_3845 = ((long) CINT(BgL_colz00_3835) + 2L);
										return
											BGl_z62ppzd2downzb0zz__ppz00(BgL_outputz00_3505,
											BgL_ppzd2exprzd2_3506, BgL_displayzf3zf3_3492,
											BgL_widthz00_3491, BgL_maxzd2callzd2headzd2widthzd2_3504,
											BgL_envz00_3488, BgL_ppzd2definezd2_3503,
											BgL_ppzd2defunzd2_3502, BgL_ppzd2ifzd2_3501,
											BgL_ppzd2condzd2_3500, BgL_ppzd2casezd2_3499,
											BgL_ppzd2andzd2_3498, BgL_ppzd2letzd2_3497,
											BgL_ppzd2beginzd2_3496, BgL_ppzd2dozd2_3495,
											BgL_ppzd2commentzd2_3494, BgL_ppzd2exprzd2listz00_3493,
											BgL_ppzd2exprzd2defunz00_3490,
											BgL_indentzd2generalzd2_3489, BgL_arg1508z00_3844,
											BgL_col3z00_3839, BINT(BgL_arg1509z00_3845),
											BgL_extraz00_3509, BgL_ppzd2exprzd2_3506);
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &pp-define */
	obj_t BGl_z62ppzd2definezb0zz__ppz00(obj_t BgL_envz00_3510,
		obj_t BgL_exprz00_3529, obj_t BgL_colz00_3530, obj_t BgL_extraz00_3531)
	{
		{	/* Pp/pp.scm 374 */
			{	/* Pp/pp.scm 374 */
				long BgL_indentzd2generalzd2_3511;
				obj_t BgL_ppzd2exprzd2defunz00_3512;
				obj_t BgL_ppzd2commentzd2_3513;
				obj_t BgL_ppzd2dozd2_3514;
				obj_t BgL_ppzd2beginzd2_3515;
				obj_t BgL_ppzd2letzd2_3516;
				obj_t BgL_ppzd2andzd2_3517;
				obj_t BgL_ppzd2casezd2_3518;
				obj_t BgL_ppzd2condzd2_3519;
				obj_t BgL_ppzd2ifzd2_3520;
				obj_t BgL_ppzd2defunzd2_3521;
				obj_t BgL_ppzd2lambdazd2_3522;
				long BgL_maxzd2callzd2headzd2widthzd2_3523;
				obj_t BgL_widthz00_3524;
				obj_t BgL_displayzf3zf3_3525;
				obj_t BgL_ppzd2exprzd2listz00_3526;
				obj_t BgL_ppzd2exprzd2_3527;
				obj_t BgL_outputz00_3528;

				BgL_indentzd2generalzd2_3511 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_3510, (int) (0L)));
				BgL_ppzd2exprzd2defunz00_3512 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3510, (int) (1L)));
				BgL_ppzd2commentzd2_3513 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3510, (int) (2L)));
				BgL_ppzd2dozd2_3514 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3510, (int) (3L)));
				BgL_ppzd2beginzd2_3515 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3510, (int) (4L)));
				BgL_ppzd2letzd2_3516 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3510, (int) (5L)));
				BgL_ppzd2andzd2_3517 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3510, (int) (6L)));
				BgL_ppzd2casezd2_3518 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3510, (int) (7L)));
				BgL_ppzd2condzd2_3519 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3510, (int) (8L)));
				BgL_ppzd2ifzd2_3520 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3510, (int) (9L)));
				BgL_ppzd2defunzd2_3521 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3510, (int) (10L)));
				BgL_ppzd2lambdazd2_3522 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3510, (int) (11L)));
				BgL_maxzd2callzd2headzd2widthzd2_3523 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_3510, (int) (12L)));
				BgL_widthz00_3524 = PROCEDURE_REF(BgL_envz00_3510, (int) (13L));
				BgL_displayzf3zf3_3525 = PROCEDURE_REF(BgL_envz00_3510, (int) (14L));
				BgL_ppzd2exprzd2listz00_3526 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3510, (int) (15L)));
				BgL_ppzd2exprzd2_3527 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3510, (int) (16L)));
				BgL_outputz00_3528 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3510, (int) (17L)));
				BGl_z62ppzd2generalzb0zz__ppz00(BgL_indentzd2generalzd2_3511,
					BgL_outputz00_3528, BgL_ppzd2exprzd2_3527, BgL_displayzf3zf3_3525,
					BgL_widthz00_3524, BgL_maxzd2callzd2headzd2widthzd2_3523,
					BgL_ppzd2lambdazd2_3522, BgL_envz00_3510, BgL_ppzd2defunzd2_3521,
					BgL_ppzd2ifzd2_3520, BgL_ppzd2condzd2_3519, BgL_ppzd2casezd2_3518,
					BgL_ppzd2andzd2_3517, BgL_ppzd2letzd2_3516, BgL_ppzd2beginzd2_3515,
					BgL_ppzd2dozd2_3514, BgL_ppzd2commentzd2_3513,
					BgL_ppzd2exprzd2listz00_3526, BgL_ppzd2exprzd2defunz00_3512,
					BgL_exprz00_3529, BgL_colz00_3530, BgL_extraz00_3531, ((bool_t) 0),
					BgL_ppzd2exprzd2listz00_3526, BFALSE, BgL_ppzd2exprzd2_3527);
				{	/* Pp/pp.scm 133 */
					obj_t BgL__andtest_1044z00_3847;

					BgL__andtest_1044z00_3847 =
						((obj_t(*)(obj_t,
								obj_t))
						PROCEDURE_L_ENTRY(BgL_outputz00_3528)) (BgL_outputz00_3528,
						BGl_string1998z00zz__ppz00);
					if (CBOOL(BgL__andtest_1044z00_3847))
						{	/* Pp/pp.scm 133 */
							return BINT((0L + 1L));
						}
					else
						{	/* Pp/pp.scm 133 */
							return BFALSE;
						}
				}
			}
		}

	}



/* &pp-defun */
	obj_t BGl_z62ppzd2defunzb0zz__ppz00(obj_t BgL_envz00_3532,
		obj_t BgL_exprz00_3551, obj_t BgL_colz00_3552, obj_t BgL_extraz00_3553)
	{
		{	/* Pp/pp.scm 378 */
			{	/* Pp/pp.scm 378 */
				long BgL_indentzd2generalzd2_3533;
				obj_t BgL_ppzd2exprzd2listz00_3534;
				obj_t BgL_ppzd2commentzd2_3535;
				obj_t BgL_ppzd2dozd2_3536;
				obj_t BgL_ppzd2beginzd2_3537;
				obj_t BgL_ppzd2letzd2_3538;
				obj_t BgL_ppzd2andzd2_3539;
				obj_t BgL_ppzd2casezd2_3540;
				obj_t BgL_ppzd2condzd2_3541;
				obj_t BgL_ppzd2ifzd2_3542;
				obj_t BgL_ppzd2definezd2_3543;
				obj_t BgL_ppzd2lambdazd2_3544;
				long BgL_maxzd2callzd2headzd2widthzd2_3545;
				obj_t BgL_widthz00_3546;
				obj_t BgL_displayzf3zf3_3547;
				obj_t BgL_ppzd2exprzd2defunz00_3548;
				obj_t BgL_ppzd2exprzd2_3549;
				obj_t BgL_outputz00_3550;

				BgL_indentzd2generalzd2_3533 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_3532, (int) (0L)));
				BgL_ppzd2exprzd2listz00_3534 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3532, (int) (1L)));
				BgL_ppzd2commentzd2_3535 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3532, (int) (2L)));
				BgL_ppzd2dozd2_3536 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3532, (int) (3L)));
				BgL_ppzd2beginzd2_3537 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3532, (int) (4L)));
				BgL_ppzd2letzd2_3538 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3532, (int) (5L)));
				BgL_ppzd2andzd2_3539 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3532, (int) (6L)));
				BgL_ppzd2casezd2_3540 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3532, (int) (7L)));
				BgL_ppzd2condzd2_3541 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3532, (int) (8L)));
				BgL_ppzd2ifzd2_3542 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3532, (int) (9L)));
				BgL_ppzd2definezd2_3543 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3532, (int) (10L)));
				BgL_ppzd2lambdazd2_3544 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3532, (int) (11L)));
				BgL_maxzd2callzd2headzd2widthzd2_3545 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_3532, (int) (12L)));
				BgL_widthz00_3546 = PROCEDURE_REF(BgL_envz00_3532, (int) (13L));
				BgL_displayzf3zf3_3547 = PROCEDURE_REF(BgL_envz00_3532, (int) (14L));
				BgL_ppzd2exprzd2defunz00_3548 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3532, (int) (15L)));
				BgL_ppzd2exprzd2_3549 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3532, (int) (16L)));
				BgL_outputz00_3550 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3532, (int) (17L)));
				BGl_z62ppzd2generalzb0zz__ppz00(BgL_indentzd2generalzd2_3533,
					BgL_outputz00_3550, BgL_ppzd2exprzd2_3549, BgL_displayzf3zf3_3547,
					BgL_widthz00_3546, BgL_maxzd2callzd2headzd2widthzd2_3545,
					BgL_ppzd2lambdazd2_3544, BgL_ppzd2definezd2_3543, BgL_envz00_3532,
					BgL_ppzd2ifzd2_3542, BgL_ppzd2condzd2_3541, BgL_ppzd2casezd2_3540,
					BgL_ppzd2andzd2_3539, BgL_ppzd2letzd2_3538, BgL_ppzd2beginzd2_3537,
					BgL_ppzd2dozd2_3536, BgL_ppzd2commentzd2_3535,
					BgL_ppzd2exprzd2listz00_3534, BgL_ppzd2exprzd2defunz00_3548,
					BgL_exprz00_3551, BgL_colz00_3552, BgL_extraz00_3553, ((bool_t) 1),
					BgL_ppzd2exprzd2defunz00_3548, BFALSE, BgL_ppzd2exprzd2_3549);
				{	/* Pp/pp.scm 133 */
					obj_t BgL__andtest_1044z00_3848;

					BgL__andtest_1044z00_3848 =
						((obj_t(*)(obj_t,
								obj_t))
						PROCEDURE_L_ENTRY(BgL_outputz00_3550)) (BgL_outputz00_3550,
						BGl_string1998z00zz__ppz00);
					if (CBOOL(BgL__andtest_1044z00_3848))
						{	/* Pp/pp.scm 133 */
							return BINT((0L + 1L));
						}
					else
						{	/* Pp/pp.scm 133 */
							return BFALSE;
						}
				}
			}
		}

	}



/* &pp-if */
	obj_t BGl_z62ppzd2ifzb0zz__ppz00(obj_t BgL_envz00_3554,
		obj_t BgL_exprz00_3573, obj_t BgL_colz00_3574, obj_t BgL_extraz00_3575)
	{
		{	/* Pp/pp.scm 411 */
			{	/* Pp/pp.scm 411 */
				long BgL_indentzd2generalzd2_3555;
				obj_t BgL_ppzd2exprzd2defunz00_3556;
				obj_t BgL_widthz00_3557;
				obj_t BgL_displayzf3zf3_3558;
				obj_t BgL_ppzd2exprzd2listz00_3559;
				obj_t BgL_ppzd2commentzd2_3560;
				obj_t BgL_ppzd2dozd2_3561;
				obj_t BgL_ppzd2beginzd2_3562;
				obj_t BgL_ppzd2letzd2_3563;
				obj_t BgL_ppzd2andzd2_3564;
				obj_t BgL_ppzd2casezd2_3565;
				obj_t BgL_ppzd2condzd2_3566;
				obj_t BgL_ppzd2defunzd2_3567;
				obj_t BgL_ppzd2definezd2_3568;
				obj_t BgL_ppzd2lambdazd2_3569;
				obj_t BgL_outputz00_3570;
				long BgL_maxzd2callzd2headzd2widthzd2_3571;
				obj_t BgL_ppzd2exprzd2_3572;

				BgL_indentzd2generalzd2_3555 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_3554, (int) (0L)));
				BgL_ppzd2exprzd2defunz00_3556 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3554, (int) (1L)));
				BgL_widthz00_3557 = PROCEDURE_REF(BgL_envz00_3554, (int) (2L));
				BgL_displayzf3zf3_3558 = PROCEDURE_REF(BgL_envz00_3554, (int) (3L));
				BgL_ppzd2exprzd2listz00_3559 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3554, (int) (4L)));
				BgL_ppzd2commentzd2_3560 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3554, (int) (5L)));
				BgL_ppzd2dozd2_3561 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3554, (int) (6L)));
				BgL_ppzd2beginzd2_3562 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3554, (int) (7L)));
				BgL_ppzd2letzd2_3563 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3554, (int) (8L)));
				BgL_ppzd2andzd2_3564 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3554, (int) (9L)));
				BgL_ppzd2casezd2_3565 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3554, (int) (10L)));
				BgL_ppzd2condzd2_3566 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3554, (int) (11L)));
				BgL_ppzd2defunzd2_3567 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3554, (int) (12L)));
				BgL_ppzd2definezd2_3568 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3554, (int) (13L)));
				BgL_ppzd2lambdazd2_3569 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3554, (int) (14L)));
				BgL_outputz00_3570 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3554, (int) (15L)));
				BgL_maxzd2callzd2headzd2widthzd2_3571 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_3554, (int) (16L)));
				BgL_ppzd2exprzd2_3572 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3554, (int) (17L)));
				return
					BGl_z62ppzd2generalzb0zz__ppz00(BgL_indentzd2generalzd2_3555,
					BgL_outputz00_3570, BgL_ppzd2exprzd2_3572, BgL_displayzf3zf3_3558,
					BgL_widthz00_3557, BgL_maxzd2callzd2headzd2widthzd2_3571,
					BgL_ppzd2lambdazd2_3569, BgL_ppzd2definezd2_3568,
					BgL_ppzd2defunzd2_3567, BgL_envz00_3554, BgL_ppzd2condzd2_3566,
					BgL_ppzd2casezd2_3565, BgL_ppzd2andzd2_3564, BgL_ppzd2letzd2_3563,
					BgL_ppzd2beginzd2_3562, BgL_ppzd2dozd2_3561, BgL_ppzd2commentzd2_3560,
					BgL_ppzd2exprzd2listz00_3559, BgL_ppzd2exprzd2defunz00_3556,
					BgL_exprz00_3573, BgL_colz00_3574, BgL_extraz00_3575, ((bool_t) 0),
					BgL_ppzd2exprzd2_3572, BFALSE, BgL_ppzd2exprzd2_3572);
			}
		}

	}



/* &pp-cond */
	obj_t BGl_z62ppzd2condzb0zz__ppz00(obj_t BgL_envz00_3576,
		obj_t BgL_exprz00_3595, obj_t BgL_colz00_3596, obj_t BgL_extraz00_3597)
	{
		{	/* Pp/pp.scm 414 */
			{	/* Pp/pp.scm 300 */
				long BgL_indentzd2generalzd2_3577;
				obj_t BgL_ppzd2exprzd2defunz00_3578;
				obj_t BgL_widthz00_3579;
				obj_t BgL_displayzf3zf3_3580;
				obj_t BgL_ppzd2commentzd2_3581;
				obj_t BgL_ppzd2dozd2_3582;
				obj_t BgL_ppzd2beginzd2_3583;
				obj_t BgL_ppzd2letzd2_3584;
				obj_t BgL_ppzd2andzd2_3585;
				obj_t BgL_ppzd2casezd2_3586;
				obj_t BgL_ppzd2ifzd2_3587;
				obj_t BgL_ppzd2defunzd2_3588;
				obj_t BgL_ppzd2definezd2_3589;
				obj_t BgL_ppzd2lambdazd2_3590;
				long BgL_maxzd2callzd2headzd2widthzd2_3591;
				obj_t BgL_ppzd2exprzd2_3592;
				obj_t BgL_outputz00_3593;
				obj_t BgL_ppzd2exprzd2listz00_3594;

				BgL_indentzd2generalzd2_3577 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_3576, (int) (0L)));
				BgL_ppzd2exprzd2defunz00_3578 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3576, (int) (1L)));
				BgL_widthz00_3579 = PROCEDURE_REF(BgL_envz00_3576, (int) (2L));
				BgL_displayzf3zf3_3580 = PROCEDURE_REF(BgL_envz00_3576, (int) (3L));
				BgL_ppzd2commentzd2_3581 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3576, (int) (4L)));
				BgL_ppzd2dozd2_3582 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3576, (int) (5L)));
				BgL_ppzd2beginzd2_3583 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3576, (int) (6L)));
				BgL_ppzd2letzd2_3584 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3576, (int) (7L)));
				BgL_ppzd2andzd2_3585 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3576, (int) (8L)));
				BgL_ppzd2casezd2_3586 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3576, (int) (9L)));
				BgL_ppzd2ifzd2_3587 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3576, (int) (10L)));
				BgL_ppzd2defunzd2_3588 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3576, (int) (11L)));
				BgL_ppzd2definezd2_3589 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3576, (int) (12L)));
				BgL_ppzd2lambdazd2_3590 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3576, (int) (13L)));
				BgL_maxzd2callzd2headzd2widthzd2_3591 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_3576, (int) (14L)));
				BgL_ppzd2exprzd2_3592 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3576, (int) (15L)));
				BgL_outputz00_3593 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3576, (int) (16L)));
				BgL_ppzd2exprzd2listz00_3594 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3576, (int) (17L)));
				{	/* Pp/pp.scm 300 */
					obj_t BgL_colza2za2_3849;

					{	/* Pp/pp.scm 300 */
						obj_t BgL_arg1459z00_3850;
						obj_t BgL_arg1460z00_3851;

						BgL_arg1459z00_3850 = CAR(((obj_t) BgL_exprz00_3595));
						if (CBOOL(BgL_colz00_3596))
							{	/* Pp/pp.scm 133 */
								obj_t BgL__andtest_1044z00_3852;

								BgL__andtest_1044z00_3852 =
									((obj_t(*)(obj_t,
											obj_t))
									PROCEDURE_L_ENTRY(BgL_outputz00_3593)) (BgL_outputz00_3593,
									BGl_string1960z00zz__ppz00);
								if (CBOOL(BgL__andtest_1044z00_3852))
									{	/* Pp/pp.scm 133 */
										BgL_arg1460z00_3851 = ADDFX(BgL_colz00_3596, BINT(1L));
									}
								else
									{	/* Pp/pp.scm 133 */
										BgL_arg1460z00_3851 = BFALSE;
									}
							}
						else
							{	/* Pp/pp.scm 133 */
								BgL_arg1460z00_3851 = BFALSE;
							}
						BgL_colza2za2_3849 =
							BGl_z62wrz62zz__ppz00(BgL_displayzf3zf3_3580, BgL_outputz00_3593,
							BgL_arg1459z00_3850, BgL_arg1460z00_3851);
					}
					if (CBOOL(BgL_colz00_3596))
						{	/* Pp/pp.scm 302 */
							obj_t BgL_arg1457z00_3853;
							long BgL_arg1458z00_3854;

							BgL_arg1457z00_3853 = CDR(((obj_t) BgL_exprz00_3595));
							BgL_arg1458z00_3854 = ((long) CINT(BgL_colza2za2_3849) + 1L);
							return
								BGl_z62ppzd2downzb0zz__ppz00(BgL_outputz00_3593,
								BgL_ppzd2exprzd2_3592, BgL_displayzf3zf3_3580,
								BgL_widthz00_3579, BgL_maxzd2callzd2headzd2widthzd2_3591,
								BgL_ppzd2lambdazd2_3590, BgL_ppzd2definezd2_3589,
								BgL_ppzd2defunzd2_3588, BgL_ppzd2ifzd2_3587, BgL_envz00_3576,
								BgL_ppzd2casezd2_3586, BgL_ppzd2andzd2_3585,
								BgL_ppzd2letzd2_3584, BgL_ppzd2beginzd2_3583,
								BgL_ppzd2dozd2_3582, BgL_ppzd2commentzd2_3581,
								BgL_ppzd2exprzd2listz00_3594, BgL_ppzd2exprzd2defunz00_3578,
								BgL_indentzd2generalzd2_3577, BgL_arg1457z00_3853,
								BgL_colza2za2_3849, BINT(BgL_arg1458z00_3854),
								BgL_extraz00_3597, BgL_ppzd2exprzd2listz00_3594);
						}
					else
						{	/* Pp/pp.scm 301 */
							return BFALSE;
						}
				}
			}
		}

	}



/* &pp-case */
	obj_t BGl_z62ppzd2casezb0zz__ppz00(obj_t BgL_envz00_3598,
		obj_t BgL_exprz00_3617, obj_t BgL_colz00_3618, obj_t BgL_extraz00_3619)
	{
		{	/* Pp/pp.scm 417 */
			{	/* Pp/pp.scm 417 */
				long BgL_indentzd2generalzd2_3599;
				obj_t BgL_ppzd2exprzd2defunz00_3600;
				obj_t BgL_widthz00_3601;
				obj_t BgL_displayzf3zf3_3602;
				obj_t BgL_ppzd2commentzd2_3603;
				obj_t BgL_ppzd2dozd2_3604;
				obj_t BgL_ppzd2beginzd2_3605;
				obj_t BgL_ppzd2letzd2_3606;
				obj_t BgL_ppzd2andzd2_3607;
				obj_t BgL_ppzd2condzd2_3608;
				obj_t BgL_ppzd2ifzd2_3609;
				obj_t BgL_ppzd2defunzd2_3610;
				obj_t BgL_ppzd2definezd2_3611;
				obj_t BgL_ppzd2lambdazd2_3612;
				obj_t BgL_outputz00_3613;
				long BgL_maxzd2callzd2headzd2widthzd2_3614;
				obj_t BgL_ppzd2exprzd2_3615;
				obj_t BgL_ppzd2exprzd2listz00_3616;

				BgL_indentzd2generalzd2_3599 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_3598, (int) (0L)));
				BgL_ppzd2exprzd2defunz00_3600 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3598, (int) (1L)));
				BgL_widthz00_3601 = PROCEDURE_REF(BgL_envz00_3598, (int) (2L));
				BgL_displayzf3zf3_3602 = PROCEDURE_REF(BgL_envz00_3598, (int) (3L));
				BgL_ppzd2commentzd2_3603 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3598, (int) (4L)));
				BgL_ppzd2dozd2_3604 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3598, (int) (5L)));
				BgL_ppzd2beginzd2_3605 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3598, (int) (6L)));
				BgL_ppzd2letzd2_3606 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3598, (int) (7L)));
				BgL_ppzd2andzd2_3607 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3598, (int) (8L)));
				BgL_ppzd2condzd2_3608 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3598, (int) (9L)));
				BgL_ppzd2ifzd2_3609 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3598, (int) (10L)));
				BgL_ppzd2defunzd2_3610 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3598, (int) (11L)));
				BgL_ppzd2definezd2_3611 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3598, (int) (12L)));
				BgL_ppzd2lambdazd2_3612 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3598, (int) (13L)));
				BgL_outputz00_3613 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3598, (int) (14L)));
				BgL_maxzd2callzd2headzd2widthzd2_3614 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_3598, (int) (15L)));
				BgL_ppzd2exprzd2_3615 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3598, (int) (16L)));
				BgL_ppzd2exprzd2listz00_3616 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3598, (int) (17L)));
				return
					BGl_z62ppzd2generalzb0zz__ppz00(BgL_indentzd2generalzd2_3599,
					BgL_outputz00_3613, BgL_ppzd2exprzd2_3615, BgL_displayzf3zf3_3602,
					BgL_widthz00_3601, BgL_maxzd2callzd2headzd2widthzd2_3614,
					BgL_ppzd2lambdazd2_3612, BgL_ppzd2definezd2_3611,
					BgL_ppzd2defunzd2_3610, BgL_ppzd2ifzd2_3609, BgL_ppzd2condzd2_3608,
					BgL_envz00_3598, BgL_ppzd2andzd2_3607, BgL_ppzd2letzd2_3606,
					BgL_ppzd2beginzd2_3605, BgL_ppzd2dozd2_3604, BgL_ppzd2commentzd2_3603,
					BgL_ppzd2exprzd2listz00_3616, BgL_ppzd2exprzd2defunz00_3600,
					BgL_exprz00_3617, BgL_colz00_3618, BgL_extraz00_3619, ((bool_t) 0),
					BgL_ppzd2exprzd2_3615, BFALSE, BgL_ppzd2exprzd2listz00_3616);
			}
		}

	}



/* &pp-and */
	obj_t BGl_z62ppzd2andzb0zz__ppz00(obj_t BgL_envz00_3620,
		obj_t BgL_exprz00_3639, obj_t BgL_colz00_3640, obj_t BgL_extraz00_3641)
	{
		{	/* Pp/pp.scm 420 */
			{	/* Pp/pp.scm 300 */
				long BgL_indentzd2generalzd2_3621;
				obj_t BgL_ppzd2exprzd2defunz00_3622;
				obj_t BgL_widthz00_3623;
				obj_t BgL_displayzf3zf3_3624;
				obj_t BgL_ppzd2exprzd2listz00_3625;
				obj_t BgL_ppzd2commentzd2_3626;
				obj_t BgL_ppzd2dozd2_3627;
				obj_t BgL_ppzd2beginzd2_3628;
				obj_t BgL_ppzd2letzd2_3629;
				obj_t BgL_ppzd2casezd2_3630;
				obj_t BgL_ppzd2condzd2_3631;
				obj_t BgL_ppzd2ifzd2_3632;
				obj_t BgL_ppzd2defunzd2_3633;
				obj_t BgL_ppzd2definezd2_3634;
				obj_t BgL_ppzd2lambdazd2_3635;
				long BgL_maxzd2callzd2headzd2widthzd2_3636;
				obj_t BgL_outputz00_3637;
				obj_t BgL_ppzd2exprzd2_3638;

				BgL_indentzd2generalzd2_3621 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_3620, (int) (0L)));
				BgL_ppzd2exprzd2defunz00_3622 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3620, (int) (1L)));
				BgL_widthz00_3623 = PROCEDURE_REF(BgL_envz00_3620, (int) (2L));
				BgL_displayzf3zf3_3624 = PROCEDURE_REF(BgL_envz00_3620, (int) (3L));
				BgL_ppzd2exprzd2listz00_3625 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3620, (int) (4L)));
				BgL_ppzd2commentzd2_3626 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3620, (int) (5L)));
				BgL_ppzd2dozd2_3627 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3620, (int) (6L)));
				BgL_ppzd2beginzd2_3628 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3620, (int) (7L)));
				BgL_ppzd2letzd2_3629 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3620, (int) (8L)));
				BgL_ppzd2casezd2_3630 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3620, (int) (9L)));
				BgL_ppzd2condzd2_3631 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3620, (int) (10L)));
				BgL_ppzd2ifzd2_3632 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3620, (int) (11L)));
				BgL_ppzd2defunzd2_3633 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3620, (int) (12L)));
				BgL_ppzd2definezd2_3634 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3620, (int) (13L)));
				BgL_ppzd2lambdazd2_3635 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3620, (int) (14L)));
				BgL_maxzd2callzd2headzd2widthzd2_3636 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_3620, (int) (15L)));
				BgL_outputz00_3637 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3620, (int) (16L)));
				BgL_ppzd2exprzd2_3638 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3620, (int) (17L)));
				{	/* Pp/pp.scm 300 */
					obj_t BgL_colza2za2_3855;

					{	/* Pp/pp.scm 300 */
						obj_t BgL_arg1459z00_3856;
						obj_t BgL_arg1460z00_3857;

						BgL_arg1459z00_3856 = CAR(((obj_t) BgL_exprz00_3639));
						if (CBOOL(BgL_colz00_3640))
							{	/* Pp/pp.scm 133 */
								obj_t BgL__andtest_1044z00_3858;

								BgL__andtest_1044z00_3858 =
									((obj_t(*)(obj_t,
											obj_t))
									PROCEDURE_L_ENTRY(BgL_outputz00_3637)) (BgL_outputz00_3637,
									BGl_string1960z00zz__ppz00);
								if (CBOOL(BgL__andtest_1044z00_3858))
									{	/* Pp/pp.scm 133 */
										BgL_arg1460z00_3857 = ADDFX(BgL_colz00_3640, BINT(1L));
									}
								else
									{	/* Pp/pp.scm 133 */
										BgL_arg1460z00_3857 = BFALSE;
									}
							}
						else
							{	/* Pp/pp.scm 133 */
								BgL_arg1460z00_3857 = BFALSE;
							}
						BgL_colza2za2_3855 =
							BGl_z62wrz62zz__ppz00(BgL_displayzf3zf3_3624, BgL_outputz00_3637,
							BgL_arg1459z00_3856, BgL_arg1460z00_3857);
					}
					if (CBOOL(BgL_colz00_3640))
						{	/* Pp/pp.scm 302 */
							obj_t BgL_arg1457z00_3859;
							long BgL_arg1458z00_3860;

							BgL_arg1457z00_3859 = CDR(((obj_t) BgL_exprz00_3639));
							BgL_arg1458z00_3860 = ((long) CINT(BgL_colza2za2_3855) + 1L);
							return
								BGl_z62ppzd2downzb0zz__ppz00(BgL_outputz00_3637,
								BgL_ppzd2exprzd2_3638, BgL_displayzf3zf3_3624,
								BgL_widthz00_3623, BgL_maxzd2callzd2headzd2widthzd2_3636,
								BgL_ppzd2lambdazd2_3635, BgL_ppzd2definezd2_3634,
								BgL_ppzd2defunzd2_3633, BgL_ppzd2ifzd2_3632,
								BgL_ppzd2condzd2_3631, BgL_ppzd2casezd2_3630, BgL_envz00_3620,
								BgL_ppzd2letzd2_3629, BgL_ppzd2beginzd2_3628,
								BgL_ppzd2dozd2_3627, BgL_ppzd2commentzd2_3626,
								BgL_ppzd2exprzd2listz00_3625, BgL_ppzd2exprzd2defunz00_3622,
								BgL_indentzd2generalzd2_3621, BgL_arg1457z00_3859,
								BgL_colza2za2_3855, BINT(BgL_arg1458z00_3860),
								BgL_extraz00_3641, BgL_ppzd2exprzd2_3638);
						}
					else
						{	/* Pp/pp.scm 301 */
							return BFALSE;
						}
				}
			}
		}

	}



/* &pp-let */
	obj_t BGl_z62ppzd2letzb0zz__ppz00(obj_t BgL_envz00_3642,
		obj_t BgL_exprz00_3661, obj_t BgL_colz00_3662, obj_t BgL_extraz00_3663)
	{
		{	/* Pp/pp.scm 382 */
			{	/* Pp/pp.scm 382 */
				long BgL_indentzd2generalzd2_3643;
				obj_t BgL_ppzd2exprzd2defunz00_3644;
				obj_t BgL_ppzd2commentzd2_3645;
				obj_t BgL_ppzd2dozd2_3646;
				obj_t BgL_ppzd2beginzd2_3647;
				obj_t BgL_ppzd2andzd2_3648;
				obj_t BgL_ppzd2casezd2_3649;
				obj_t BgL_ppzd2condzd2_3650;
				obj_t BgL_ppzd2ifzd2_3651;
				obj_t BgL_ppzd2defunzd2_3652;
				obj_t BgL_ppzd2definezd2_3653;
				obj_t BgL_ppzd2lambdazd2_3654;
				long BgL_maxzd2callzd2headzd2widthzd2_3655;
				obj_t BgL_widthz00_3656;
				obj_t BgL_displayzf3zf3_3657;
				obj_t BgL_outputz00_3658;
				obj_t BgL_ppzd2exprzd2listz00_3659;
				obj_t BgL_ppzd2exprzd2_3660;

				BgL_indentzd2generalzd2_3643 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_3642, (int) (0L)));
				BgL_ppzd2exprzd2defunz00_3644 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3642, (int) (1L)));
				BgL_ppzd2commentzd2_3645 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3642, (int) (2L)));
				BgL_ppzd2dozd2_3646 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3642, (int) (3L)));
				BgL_ppzd2beginzd2_3647 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3642, (int) (4L)));
				BgL_ppzd2andzd2_3648 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3642, (int) (5L)));
				BgL_ppzd2casezd2_3649 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3642, (int) (6L)));
				BgL_ppzd2condzd2_3650 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3642, (int) (7L)));
				BgL_ppzd2ifzd2_3651 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3642, (int) (8L)));
				BgL_ppzd2defunzd2_3652 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3642, (int) (9L)));
				BgL_ppzd2definezd2_3653 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3642, (int) (10L)));
				BgL_ppzd2lambdazd2_3654 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3642, (int) (11L)));
				BgL_maxzd2callzd2headzd2widthzd2_3655 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_3642, (int) (12L)));
				BgL_widthz00_3656 = PROCEDURE_REF(BgL_envz00_3642, (int) (13L));
				BgL_displayzf3zf3_3657 = PROCEDURE_REF(BgL_envz00_3642, (int) (14L));
				BgL_outputz00_3658 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3642, (int) (15L)));
				BgL_ppzd2exprzd2listz00_3659 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3642, (int) (16L)));
				BgL_ppzd2exprzd2_3660 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3642, (int) (17L)));
				return
					BGl_z62ppzd2generalzb0zz__ppz00(BgL_indentzd2generalzd2_3643,
					BgL_outputz00_3658, BgL_ppzd2exprzd2_3660, BgL_displayzf3zf3_3657,
					BgL_widthz00_3656, BgL_maxzd2callzd2headzd2widthzd2_3655,
					BgL_ppzd2lambdazd2_3654, BgL_ppzd2definezd2_3653,
					BgL_ppzd2defunzd2_3652, BgL_ppzd2ifzd2_3651, BgL_ppzd2condzd2_3650,
					BgL_ppzd2casezd2_3649, BgL_ppzd2andzd2_3648, BgL_envz00_3642,
					BgL_ppzd2beginzd2_3647, BgL_ppzd2dozd2_3646, BgL_ppzd2commentzd2_3645,
					BgL_ppzd2exprzd2listz00_3659, BgL_ppzd2exprzd2defunz00_3644,
					BgL_exprz00_3661, BgL_colz00_3662, BgL_extraz00_3663, ((bool_t) 0),
					BgL_ppzd2exprzd2listz00_3659, BFALSE, BgL_ppzd2exprzd2_3660);
			}
		}

	}



/* &pp-begin */
	obj_t BGl_z62ppzd2beginzb0zz__ppz00(obj_t BgL_envz00_3664,
		obj_t BgL_exprz00_3683, obj_t BgL_colz00_3684, obj_t BgL_extraz00_3685)
	{
		{	/* Pp/pp.scm 428 */
			{	/* Pp/pp.scm 428 */
				long BgL_indentzd2generalzd2_3665;
				obj_t BgL_ppzd2exprzd2defunz00_3666;
				obj_t BgL_widthz00_3667;
				obj_t BgL_displayzf3zf3_3668;
				obj_t BgL_ppzd2exprzd2listz00_3669;
				obj_t BgL_ppzd2commentzd2_3670;
				obj_t BgL_ppzd2dozd2_3671;
				obj_t BgL_ppzd2letzd2_3672;
				obj_t BgL_ppzd2andzd2_3673;
				obj_t BgL_ppzd2casezd2_3674;
				obj_t BgL_ppzd2condzd2_3675;
				obj_t BgL_ppzd2ifzd2_3676;
				obj_t BgL_ppzd2defunzd2_3677;
				obj_t BgL_ppzd2definezd2_3678;
				obj_t BgL_ppzd2lambdazd2_3679;
				obj_t BgL_outputz00_3680;
				long BgL_maxzd2callzd2headzd2widthzd2_3681;
				obj_t BgL_ppzd2exprzd2_3682;

				BgL_indentzd2generalzd2_3665 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_3664, (int) (0L)));
				BgL_ppzd2exprzd2defunz00_3666 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3664, (int) (1L)));
				BgL_widthz00_3667 = PROCEDURE_REF(BgL_envz00_3664, (int) (2L));
				BgL_displayzf3zf3_3668 = PROCEDURE_REF(BgL_envz00_3664, (int) (3L));
				BgL_ppzd2exprzd2listz00_3669 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3664, (int) (4L)));
				BgL_ppzd2commentzd2_3670 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3664, (int) (5L)));
				BgL_ppzd2dozd2_3671 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3664, (int) (6L)));
				BgL_ppzd2letzd2_3672 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3664, (int) (7L)));
				BgL_ppzd2andzd2_3673 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3664, (int) (8L)));
				BgL_ppzd2casezd2_3674 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3664, (int) (9L)));
				BgL_ppzd2condzd2_3675 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3664, (int) (10L)));
				BgL_ppzd2ifzd2_3676 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3664, (int) (11L)));
				BgL_ppzd2defunzd2_3677 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3664, (int) (12L)));
				BgL_ppzd2definezd2_3678 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3664, (int) (13L)));
				BgL_ppzd2lambdazd2_3679 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3664, (int) (14L)));
				BgL_outputz00_3680 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3664, (int) (15L)));
				BgL_maxzd2callzd2headzd2widthzd2_3681 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_3664, (int) (16L)));
				BgL_ppzd2exprzd2_3682 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3664, (int) (17L)));
				return
					BGl_z62ppzd2generalzb0zz__ppz00(BgL_indentzd2generalzd2_3665,
					BgL_outputz00_3680, BgL_ppzd2exprzd2_3682, BgL_displayzf3zf3_3668,
					BgL_widthz00_3667, BgL_maxzd2callzd2headzd2widthzd2_3681,
					BgL_ppzd2lambdazd2_3679, BgL_ppzd2definezd2_3678,
					BgL_ppzd2defunzd2_3677, BgL_ppzd2ifzd2_3676, BgL_ppzd2condzd2_3675,
					BgL_ppzd2casezd2_3674, BgL_ppzd2andzd2_3673, BgL_ppzd2letzd2_3672,
					BgL_envz00_3664, BgL_ppzd2dozd2_3671, BgL_ppzd2commentzd2_3670,
					BgL_ppzd2exprzd2listz00_3669, BgL_ppzd2exprzd2defunz00_3666,
					BgL_exprz00_3683, BgL_colz00_3684, BgL_extraz00_3685, ((bool_t) 0),
					BFALSE, BFALSE, BgL_ppzd2exprzd2_3682);
			}
		}

	}



/* &pp-do */
	obj_t BGl_z62ppzd2dozb0zz__ppz00(obj_t BgL_envz00_3686,
		obj_t BgL_exprz00_3705, obj_t BgL_colz00_3706, obj_t BgL_extraz00_3707)
	{
		{	/* Pp/pp.scm 431 */
			{	/* Pp/pp.scm 431 */
				long BgL_indentzd2generalzd2_3687;
				obj_t BgL_ppzd2exprzd2defunz00_3688;
				obj_t BgL_ppzd2commentzd2_3689;
				obj_t BgL_ppzd2beginzd2_3690;
				obj_t BgL_ppzd2letzd2_3691;
				obj_t BgL_ppzd2andzd2_3692;
				obj_t BgL_ppzd2casezd2_3693;
				obj_t BgL_ppzd2condzd2_3694;
				obj_t BgL_ppzd2ifzd2_3695;
				obj_t BgL_ppzd2defunzd2_3696;
				obj_t BgL_ppzd2definezd2_3697;
				obj_t BgL_ppzd2lambdazd2_3698;
				long BgL_maxzd2callzd2headzd2widthzd2_3699;
				obj_t BgL_widthz00_3700;
				obj_t BgL_displayzf3zf3_3701;
				obj_t BgL_outputz00_3702;
				obj_t BgL_ppzd2exprzd2listz00_3703;
				obj_t BgL_ppzd2exprzd2_3704;

				BgL_indentzd2generalzd2_3687 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_3686, (int) (0L)));
				BgL_ppzd2exprzd2defunz00_3688 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3686, (int) (1L)));
				BgL_ppzd2commentzd2_3689 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3686, (int) (2L)));
				BgL_ppzd2beginzd2_3690 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3686, (int) (3L)));
				BgL_ppzd2letzd2_3691 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3686, (int) (4L)));
				BgL_ppzd2andzd2_3692 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3686, (int) (5L)));
				BgL_ppzd2casezd2_3693 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3686, (int) (6L)));
				BgL_ppzd2condzd2_3694 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3686, (int) (7L)));
				BgL_ppzd2ifzd2_3695 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3686, (int) (8L)));
				BgL_ppzd2defunzd2_3696 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3686, (int) (9L)));
				BgL_ppzd2definezd2_3697 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3686, (int) (10L)));
				BgL_ppzd2lambdazd2_3698 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3686, (int) (11L)));
				BgL_maxzd2callzd2headzd2widthzd2_3699 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_3686, (int) (12L)));
				BgL_widthz00_3700 = PROCEDURE_REF(BgL_envz00_3686, (int) (13L));
				BgL_displayzf3zf3_3701 = PROCEDURE_REF(BgL_envz00_3686, (int) (14L));
				BgL_outputz00_3702 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3686, (int) (15L)));
				BgL_ppzd2exprzd2listz00_3703 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3686, (int) (16L)));
				BgL_ppzd2exprzd2_3704 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3686, (int) (17L)));
				return
					BGl_z62ppzd2generalzb0zz__ppz00(BgL_indentzd2generalzd2_3687,
					BgL_outputz00_3702, BgL_ppzd2exprzd2_3704, BgL_displayzf3zf3_3701,
					BgL_widthz00_3700, BgL_maxzd2callzd2headzd2widthzd2_3699,
					BgL_ppzd2lambdazd2_3698, BgL_ppzd2definezd2_3697,
					BgL_ppzd2defunzd2_3696, BgL_ppzd2ifzd2_3695, BgL_ppzd2condzd2_3694,
					BgL_ppzd2casezd2_3693, BgL_ppzd2andzd2_3692, BgL_ppzd2letzd2_3691,
					BgL_ppzd2beginzd2_3690, BgL_envz00_3686, BgL_ppzd2commentzd2_3689,
					BgL_ppzd2exprzd2listz00_3703, BgL_ppzd2exprzd2defunz00_3688,
					BgL_exprz00_3705, BgL_colz00_3706, BgL_extraz00_3707, ((bool_t) 0),
					BgL_ppzd2exprzd2listz00_3703, BgL_ppzd2exprzd2listz00_3703,
					BgL_ppzd2exprzd2_3704);
			}
		}

	}



/* &pp-comment */
	obj_t BGl_z62ppzd2commentzb0zz__ppz00(obj_t BgL_envz00_3708,
		obj_t BgL_exprz00_3727, obj_t BgL_colz00_3728, obj_t BgL_extraz00_3729)
	{
		{	/* Pp/pp.scm 408 */
			{	/* Pp/pp.scm 408 */
				obj_t BgL_ppzd2exprzd2defunz00_3709;
				obj_t BgL_ppzd2exprzd2listz00_3710;
				obj_t BgL_ppzd2dozd2_3711;
				obj_t BgL_ppzd2beginzd2_3712;
				obj_t BgL_ppzd2letzd2_3713;
				obj_t BgL_ppzd2andzd2_3714;
				obj_t BgL_ppzd2casezd2_3715;
				obj_t BgL_ppzd2condzd2_3716;
				obj_t BgL_ppzd2ifzd2_3717;
				obj_t BgL_ppzd2defunzd2_3718;
				obj_t BgL_ppzd2definezd2_3719;
				obj_t BgL_ppzd2lambdazd2_3720;
				long BgL_maxzd2callzd2headzd2widthzd2_3721;
				obj_t BgL_widthz00_3722;
				obj_t BgL_displayzf3zf3_3723;
				long BgL_indentzd2generalzd2_3724;
				obj_t BgL_outputz00_3725;
				obj_t BgL_ppzd2exprzd2_3726;

				BgL_ppzd2exprzd2defunz00_3709 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3708, (int) (0L)));
				BgL_ppzd2exprzd2listz00_3710 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3708, (int) (1L)));
				BgL_ppzd2dozd2_3711 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3708, (int) (2L)));
				BgL_ppzd2beginzd2_3712 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3708, (int) (3L)));
				BgL_ppzd2letzd2_3713 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3708, (int) (4L)));
				BgL_ppzd2andzd2_3714 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3708, (int) (5L)));
				BgL_ppzd2casezd2_3715 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3708, (int) (6L)));
				BgL_ppzd2condzd2_3716 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3708, (int) (7L)));
				BgL_ppzd2ifzd2_3717 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3708, (int) (8L)));
				BgL_ppzd2defunzd2_3718 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3708, (int) (9L)));
				BgL_ppzd2definezd2_3719 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3708, (int) (10L)));
				BgL_ppzd2lambdazd2_3720 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3708, (int) (11L)));
				BgL_maxzd2callzd2headzd2widthzd2_3721 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_3708, (int) (12L)));
				BgL_widthz00_3722 = PROCEDURE_REF(BgL_envz00_3708, (int) (13L));
				BgL_displayzf3zf3_3723 = PROCEDURE_REF(BgL_envz00_3708, (int) (14L));
				BgL_indentzd2generalzd2_3724 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_3708, (int) (15L)));
				BgL_outputz00_3725 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3708, (int) (16L)));
				BgL_ppzd2exprzd2_3726 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3708, (int) (17L)));
				{
					obj_t BgL_columnz00_3863;
					obj_t BgL_stringz00_3864;

					if (PAIRP(BgL_exprz00_3727))
						{	/* Pp/pp.scm 408 */
							obj_t BgL_cdrzd2118zd2_3873;

							BgL_cdrzd2118zd2_3873 = CDR(((obj_t) BgL_exprz00_3727));
							if (
								(CAR(((obj_t) BgL_exprz00_3727)) == BGl_symbol1963z00zz__ppz00))
								{	/* Pp/pp.scm 408 */
									if (PAIRP(BgL_cdrzd2118zd2_3873))
										{	/* Pp/pp.scm 408 */
											obj_t BgL_carzd2121zd2_3874;
											obj_t BgL_cdrzd2122zd2_3875;

											BgL_carzd2121zd2_3874 = CAR(BgL_cdrzd2118zd2_3873);
											BgL_cdrzd2122zd2_3875 = CDR(BgL_cdrzd2118zd2_3873);
											if (INTEGERP(BgL_carzd2121zd2_3874))
												{	/* Pp/pp.scm 408 */
													if (PAIRP(BgL_cdrzd2122zd2_3875))
														{	/* Pp/pp.scm 408 */
															obj_t BgL_carzd2127zd2_3876;

															BgL_carzd2127zd2_3876 =
																CAR(BgL_cdrzd2122zd2_3875);
															if (STRINGP(BgL_carzd2127zd2_3876))
																{	/* Pp/pp.scm 408 */
																	if (NULLP(CDR(BgL_cdrzd2122zd2_3875)))
																		{	/* Pp/pp.scm 408 */
																			BgL_columnz00_3863 =
																				BgL_carzd2121zd2_3874;
																			BgL_stringz00_3864 =
																				BgL_carzd2127zd2_3876;
																			{	/* Pp/pp.scm 397 */
																				obj_t BgL_addz00_3865;

																				{	/* Pp/pp.scm 397 */
																					long BgL_b1114z00_3866;

																					{	/* Pp/pp.scm 397 */

																						BgL_b1114z00_3866 =
																							(STRING_LENGTH(BgL_stringz00_3864)
																							+ 3L);
																					}
																					if (INTEGERP
																						(BGl_za2ppzd2widthza2zd2zz__ppz00))
																						{	/* Pp/pp.scm 397 */
																							BgL_addz00_3865 =
																								SUBFX
																								(BGl_za2ppzd2widthza2zd2zz__ppz00,
																								BINT(BgL_b1114z00_3866));
																						}
																					else
																						{	/* Pp/pp.scm 397 */
																							BgL_addz00_3865 =
																								BGl_2zd2zd2zz__r4_numbers_6_5z00
																								(BGl_za2ppzd2widthza2zd2zz__ppz00,
																								BINT(BgL_b1114z00_3866));
																						}
																				}
																				if (
																					((long) CINT(BgL_columnz00_3863) ==
																						0L))
																					{	/* Pp/pp.scm 398 */
																						if (
																							((long) CINT(BgL_addz00_3865) >
																								0L))
																							{	/* Pp/pp.scm 400 */
																								obj_t BgL_arg1527z00_3867;

																								BgL_arg1527z00_3867 =
																									string_append
																									(BgL_stringz00_3864,
																									make_string((long)
																										CINT(BgL_addz00_3865),
																										((unsigned char) ' ')));
																								{	/* Pp/pp.scm 133 */
																									obj_t
																										BgL__andtest_1044z00_3868;
																									BgL__andtest_1044z00_3868 =
																										((obj_t(*)(obj_t,
																												obj_t))
																										PROCEDURE_L_ENTRY
																										(BgL_outputz00_3725))
																										(BgL_outputz00_3725,
																										BgL_arg1527z00_3867);
																									if (CBOOL
																										(BgL__andtest_1044z00_3868))
																										{	/* Pp/pp.scm 133 */
																											return
																												BINT(
																												(0L +
																													STRING_LENGTH
																													(BgL_arg1527z00_3867)));
																										}
																									else
																										{	/* Pp/pp.scm 133 */
																											return BFALSE;
																										}
																								}
																							}
																						else
																							{	/* Pp/pp.scm 133 */
																								obj_t BgL__andtest_1044z00_3869;

																								BgL__andtest_1044z00_3869 =
																									((obj_t(*)(obj_t,
																											obj_t))
																									PROCEDURE_L_ENTRY
																									(BgL_outputz00_3725))
																									(BgL_outputz00_3725,
																									BgL_stringz00_3864);
																								if (CBOOL
																									(BgL__andtest_1044z00_3869))
																									{	/* Pp/pp.scm 133 */
																										return
																											BINT(
																											(0L +
																												STRING_LENGTH
																												(BgL_stringz00_3864)));
																									}
																								else
																									{	/* Pp/pp.scm 133 */
																										return BFALSE;
																									}
																							}
																					}
																				else
																					{	/* Pp/pp.scm 398 */
																						if (
																							((long) CINT(BgL_addz00_3865) >
																								0L))
																							{	/* Pp/pp.scm 404 */
																								obj_t BgL_arg1530z00_3870;

																								BgL_arg1530z00_3870 =
																									string_append
																									(BgL_stringz00_3864,
																									make_string((long)
																										CINT(BgL_addz00_3865),
																										((unsigned char) ' ')));
																								if (CBOOL(BgL_colz00_3728))
																									{	/* Pp/pp.scm 133 */
																										obj_t
																											BgL__andtest_1044z00_3871;
																										BgL__andtest_1044z00_3871 =
																											((obj_t(*)(obj_t,
																													obj_t))
																											PROCEDURE_L_ENTRY
																											(BgL_outputz00_3725))
																											(BgL_outputz00_3725,
																											BgL_arg1530z00_3870);
																										if (CBOOL
																											(BgL__andtest_1044z00_3871))
																											{	/* Pp/pp.scm 133 */
																												return
																													ADDFX(BgL_colz00_3728,
																													BINT(STRING_LENGTH
																														(BgL_arg1530z00_3870)));
																											}
																										else
																											{	/* Pp/pp.scm 133 */
																												return BFALSE;
																											}
																									}
																								else
																									{	/* Pp/pp.scm 133 */
																										return BFALSE;
																									}
																							}
																						else
																							{	/* Pp/pp.scm 403 */
																								if (CBOOL(BgL_colz00_3728))
																									{	/* Pp/pp.scm 133 */
																										obj_t
																											BgL__andtest_1044z00_3872;
																										BgL__andtest_1044z00_3872 =
																											((obj_t(*)(obj_t,
																													obj_t))
																											PROCEDURE_L_ENTRY
																											(BgL_outputz00_3725))
																											(BgL_outputz00_3725,
																											BgL_stringz00_3864);
																										if (CBOOL
																											(BgL__andtest_1044z00_3872))
																											{	/* Pp/pp.scm 133 */
																												return
																													ADDFX(BgL_colz00_3728,
																													BINT(STRING_LENGTH
																														(BgL_stringz00_3864)));
																											}
																										else
																											{	/* Pp/pp.scm 133 */
																												return BFALSE;
																											}
																									}
																								else
																									{	/* Pp/pp.scm 133 */
																										return BFALSE;
																									}
																							}
																					}
																			}
																		}
																	else
																		{	/* Pp/pp.scm 408 */
																		BgL_tagzd2111zd2_3861:
																			return
																				BGl_z62ppzd2generalzb0zz__ppz00
																				(BgL_indentzd2generalzd2_3724,
																				BgL_outputz00_3725,
																				BgL_ppzd2exprzd2_3726,
																				BgL_displayzf3zf3_3723,
																				BgL_widthz00_3722,
																				BgL_maxzd2callzd2headzd2widthzd2_3721,
																				BgL_ppzd2lambdazd2_3720,
																				BgL_ppzd2definezd2_3719,
																				BgL_ppzd2defunzd2_3718,
																				BgL_ppzd2ifzd2_3717,
																				BgL_ppzd2condzd2_3716,
																				BgL_ppzd2casezd2_3715,
																				BgL_ppzd2andzd2_3714,
																				BgL_ppzd2letzd2_3713,
																				BgL_ppzd2beginzd2_3712,
																				BgL_ppzd2dozd2_3711, BgL_envz00_3708,
																				BgL_ppzd2exprzd2listz00_3710,
																				BgL_ppzd2exprzd2defunz00_3709,
																				BgL_exprz00_3727, BgL_colz00_3728,
																				BgL_extraz00_3729, ((bool_t) 0),
																				BgL_ppzd2exprzd2_3726, BFALSE,
																				BgL_ppzd2exprzd2_3726);
																		}
																}
															else
																{	/* Pp/pp.scm 408 */
																	goto BgL_tagzd2111zd2_3861;
																}
														}
													else
														{	/* Pp/pp.scm 408 */
															goto BgL_tagzd2111zd2_3861;
														}
												}
											else
												{	/* Pp/pp.scm 408 */
													goto BgL_tagzd2111zd2_3861;
												}
										}
									else
										{	/* Pp/pp.scm 408 */
											goto BgL_tagzd2111zd2_3861;
										}
								}
							else
								{	/* Pp/pp.scm 408 */
									goto BgL_tagzd2111zd2_3861;
								}
						}
					else
						{	/* Pp/pp.scm 408 */
							goto BgL_tagzd2111zd2_3861;
						}
				}
			}
		}

	}



/* &pp-expr-defun */
	obj_t BGl_z62ppzd2exprzd2defunz62zz__ppz00(obj_t BgL_envz00_3730,
		obj_t BgL_lz00_3749, obj_t BgL_colz00_3750, obj_t BgL_extraz00_3751)
	{
		{	/* Pp/pp.scm 371 */
			{	/* Pp/pp.scm 314 */
				long BgL_indentzd2generalzd2_3731;
				obj_t BgL_widthz00_3732;
				obj_t BgL_displayzf3zf3_3733;
				obj_t BgL_ppzd2exprzd2listz00_3734;
				obj_t BgL_ppzd2commentzd2_3735;
				obj_t BgL_ppzd2dozd2_3736;
				obj_t BgL_ppzd2beginzd2_3737;
				obj_t BgL_ppzd2letzd2_3738;
				obj_t BgL_ppzd2andzd2_3739;
				obj_t BgL_ppzd2casezd2_3740;
				obj_t BgL_ppzd2condzd2_3741;
				obj_t BgL_ppzd2ifzd2_3742;
				obj_t BgL_ppzd2defunzd2_3743;
				obj_t BgL_ppzd2definezd2_3744;
				obj_t BgL_ppzd2lambdazd2_3745;
				long BgL_maxzd2callzd2headzd2widthzd2_3746;
				obj_t BgL_outputz00_3747;
				obj_t BgL_ppzd2exprzd2_3748;

				BgL_indentzd2generalzd2_3731 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_3730, (int) (0L)));
				BgL_widthz00_3732 = PROCEDURE_REF(BgL_envz00_3730, (int) (1L));
				BgL_displayzf3zf3_3733 = PROCEDURE_REF(BgL_envz00_3730, (int) (2L));
				BgL_ppzd2exprzd2listz00_3734 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3730, (int) (3L)));
				BgL_ppzd2commentzd2_3735 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3730, (int) (4L)));
				BgL_ppzd2dozd2_3736 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3730, (int) (5L)));
				BgL_ppzd2beginzd2_3737 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3730, (int) (6L)));
				BgL_ppzd2letzd2_3738 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3730, (int) (7L)));
				BgL_ppzd2andzd2_3739 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3730, (int) (8L)));
				BgL_ppzd2casezd2_3740 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3730, (int) (9L)));
				BgL_ppzd2condzd2_3741 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3730, (int) (10L)));
				BgL_ppzd2ifzd2_3742 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3730, (int) (11L)));
				BgL_ppzd2defunzd2_3743 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3730, (int) (12L)));
				BgL_ppzd2definezd2_3744 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3730, (int) (13L)));
				BgL_ppzd2lambdazd2_3745 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3730, (int) (14L)));
				BgL_maxzd2callzd2headzd2widthzd2_3746 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_3730, (int) (15L)));
				BgL_outputz00_3747 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3730, (int) (16L)));
				BgL_ppzd2exprzd2_3748 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3730, (int) (17L)));
				{	/* Pp/pp.scm 314 */
					obj_t BgL_colz00_3877;

					if (CBOOL(BgL_colz00_3750))
						{	/* Pp/pp.scm 133 */
							obj_t BgL__andtest_1044z00_3878;

							BgL__andtest_1044z00_3878 =
								((obj_t(*)(obj_t,
										obj_t))
								PROCEDURE_L_ENTRY(BgL_outputz00_3747)) (BgL_outputz00_3747,
								BGl_string1999z00zz__ppz00);
							if (CBOOL(BgL__andtest_1044z00_3878))
								{	/* Pp/pp.scm 133 */
									BgL_colz00_3877 = ADDFX(BgL_colz00_3750, BINT(3L));
								}
							else
								{	/* Pp/pp.scm 133 */
									BgL_colz00_3877 = BFALSE;
								}
						}
					else
						{	/* Pp/pp.scm 133 */
							BgL_colz00_3877 = BFALSE;
						}
					{	/* Pp/pp.scm 314 */
						obj_t BgL_col2z00_3879;

						{	/* Pp/pp.scm 315 */
							obj_t BgL_arg1466z00_3880;

							BgL_arg1466z00_3880 = CAR(((obj_t) BgL_lz00_3749));
							BgL_col2z00_3879 =
								BGl_z62wrz62zz__ppz00(BgL_displayzf3zf3_3733,
								BgL_outputz00_3747, BgL_arg1466z00_3880, BgL_colz00_3877);
						}
						{	/* Pp/pp.scm 315 */
							obj_t BgL_col3z00_3881;

							{	/* Pp/pp.scm 316 */
								obj_t BgL_arg1465z00_3882;

								{	/* Pp/pp.scm 316 */
									obj_t BgL_pairz00_3883;

									BgL_pairz00_3883 = CDR(((obj_t) BgL_lz00_3749));
									BgL_arg1465z00_3882 = CAR(BgL_pairz00_3883);
								}
								BgL_col3z00_3881 =
									BGl_z62wrz62zz__ppz00(BgL_displayzf3zf3_3733,
									BgL_outputz00_3747, BgL_arg1465z00_3882, BgL_col2z00_3879);
							}
							{	/* Pp/pp.scm 316 */

								{	/* Pp/pp.scm 317 */
									obj_t BgL_arg1463z00_3884;
									long BgL_arg1464z00_3885;

									{	/* Pp/pp.scm 317 */
										obj_t BgL_pairz00_3886;

										BgL_pairz00_3886 = CDR(((obj_t) BgL_lz00_3749));
										BgL_arg1463z00_3884 = CDR(BgL_pairz00_3886);
									}
									BgL_arg1464z00_3885 = ((long) CINT(BgL_col3z00_3881) + 1L);
									return
										BGl_z62ppzd2downzb0zz__ppz00(BgL_outputz00_3747,
										BgL_ppzd2exprzd2_3748, BgL_displayzf3zf3_3733,
										BgL_widthz00_3732, BgL_maxzd2callzd2headzd2widthzd2_3746,
										BgL_ppzd2lambdazd2_3745, BgL_ppzd2definezd2_3744,
										BgL_ppzd2defunzd2_3743, BgL_ppzd2ifzd2_3742,
										BgL_ppzd2condzd2_3741, BgL_ppzd2casezd2_3740,
										BgL_ppzd2andzd2_3739, BgL_ppzd2letzd2_3738,
										BgL_ppzd2beginzd2_3737, BgL_ppzd2dozd2_3736,
										BgL_ppzd2commentzd2_3735, BgL_ppzd2exprzd2listz00_3734,
										BgL_envz00_3730, BgL_indentzd2generalzd2_3731,
										BgL_arg1463z00_3884, BgL_col3z00_3881,
										BINT(BgL_arg1464z00_3885), BgL_extraz00_3751,
										BgL_ppzd2exprzd2_3748);
								}
							}
						}
					}
				}
			}
		}

	}



/* &pp-expr-list */
	obj_t BGl_z62ppzd2exprzd2listz62zz__ppz00(obj_t BgL_envz00_3752,
		obj_t BgL_lz00_3771, obj_t BgL_colz00_3772, obj_t BgL_extraz00_3773)
	{
		{	/* Pp/pp.scm 368 */
			{	/* Pp/pp.scm 308 */
				long BgL_indentzd2generalzd2_3753;
				obj_t BgL_ppzd2exprzd2defunz00_3754;
				obj_t BgL_widthz00_3755;
				obj_t BgL_displayzf3zf3_3756;
				obj_t BgL_ppzd2commentzd2_3757;
				obj_t BgL_ppzd2dozd2_3758;
				obj_t BgL_ppzd2beginzd2_3759;
				obj_t BgL_ppzd2letzd2_3760;
				obj_t BgL_ppzd2andzd2_3761;
				obj_t BgL_ppzd2casezd2_3762;
				obj_t BgL_ppzd2condzd2_3763;
				obj_t BgL_ppzd2ifzd2_3764;
				obj_t BgL_ppzd2defunzd2_3765;
				obj_t BgL_ppzd2definezd2_3766;
				obj_t BgL_ppzd2lambdazd2_3767;
				long BgL_maxzd2callzd2headzd2widthzd2_3768;
				obj_t BgL_outputz00_3769;
				obj_t BgL_ppzd2exprzd2_3770;

				BgL_indentzd2generalzd2_3753 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_3752, (int) (0L)));
				BgL_ppzd2exprzd2defunz00_3754 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3752, (int) (1L)));
				BgL_widthz00_3755 = PROCEDURE_REF(BgL_envz00_3752, (int) (2L));
				BgL_displayzf3zf3_3756 = PROCEDURE_REF(BgL_envz00_3752, (int) (3L));
				BgL_ppzd2commentzd2_3757 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3752, (int) (4L)));
				BgL_ppzd2dozd2_3758 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3752, (int) (5L)));
				BgL_ppzd2beginzd2_3759 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3752, (int) (6L)));
				BgL_ppzd2letzd2_3760 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3752, (int) (7L)));
				BgL_ppzd2andzd2_3761 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3752, (int) (8L)));
				BgL_ppzd2casezd2_3762 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3752, (int) (9L)));
				BgL_ppzd2condzd2_3763 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3752, (int) (10L)));
				BgL_ppzd2ifzd2_3764 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3752, (int) (11L)));
				BgL_ppzd2defunzd2_3765 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3752, (int) (12L)));
				BgL_ppzd2definezd2_3766 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3752, (int) (13L)));
				BgL_ppzd2lambdazd2_3767 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3752, (int) (14L)));
				BgL_maxzd2callzd2headzd2widthzd2_3768 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_3752, (int) (15L)));
				BgL_outputz00_3769 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3752, (int) (16L)));
				BgL_ppzd2exprzd2_3770 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3752, (int) (17L)));
				{	/* Pp/pp.scm 308 */
					obj_t BgL_colz00_3887;

					if (CBOOL(BgL_colz00_3772))
						{	/* Pp/pp.scm 133 */
							obj_t BgL__andtest_1044z00_3888;

							BgL__andtest_1044z00_3888 =
								((obj_t(*)(obj_t,
										obj_t))
								PROCEDURE_L_ENTRY(BgL_outputz00_3769)) (BgL_outputz00_3769,
								BGl_string1960z00zz__ppz00);
							if (CBOOL(BgL__andtest_1044z00_3888))
								{	/* Pp/pp.scm 133 */
									BgL_colz00_3887 = ADDFX(BgL_colz00_3772, BINT(1L));
								}
							else
								{	/* Pp/pp.scm 133 */
									BgL_colz00_3887 = BFALSE;
								}
						}
					else
						{	/* Pp/pp.scm 133 */
							BgL_colz00_3887 = BFALSE;
						}
					return
						BGl_z62ppzd2downzb0zz__ppz00(BgL_outputz00_3769,
						BgL_ppzd2exprzd2_3770, BgL_displayzf3zf3_3756, BgL_widthz00_3755,
						BgL_maxzd2callzd2headzd2widthzd2_3768, BgL_ppzd2lambdazd2_3767,
						BgL_ppzd2definezd2_3766, BgL_ppzd2defunzd2_3765,
						BgL_ppzd2ifzd2_3764, BgL_ppzd2condzd2_3763, BgL_ppzd2casezd2_3762,
						BgL_ppzd2andzd2_3761, BgL_ppzd2letzd2_3760, BgL_ppzd2beginzd2_3759,
						BgL_ppzd2dozd2_3758, BgL_ppzd2commentzd2_3757, BgL_envz00_3752,
						BgL_ppzd2exprzd2defunz00_3754, BgL_indentzd2generalzd2_3753,
						BgL_lz00_3771, BgL_colz00_3887, BgL_colz00_3887, BgL_extraz00_3773,
						BgL_ppzd2exprzd2_3770);
				}
			}
		}

	}



/* &pp-expr */
	obj_t BGl_z62ppzd2exprzb0zz__ppz00(obj_t BgL_envz00_3774,
		obj_t BgL_exprz00_3793, obj_t BgL_colz00_3794, obj_t BgL_extraz00_3795)
	{
		{	/* Pp/pp.scm 294 */
			{	/* Pp/pp.scm 280 */
				long BgL_indentzd2generalzd2_3775;
				obj_t BgL_ppzd2exprzd2defunz00_3776;
				obj_t BgL_widthz00_3777;
				obj_t BgL_displayzf3zf3_3778;
				obj_t BgL_ppzd2exprzd2listz00_3779;
				obj_t BgL_ppzd2commentzd2_3780;
				obj_t BgL_ppzd2dozd2_3781;
				obj_t BgL_ppzd2beginzd2_3782;
				obj_t BgL_ppzd2letzd2_3783;
				obj_t BgL_ppzd2andzd2_3784;
				obj_t BgL_ppzd2casezd2_3785;
				obj_t BgL_ppzd2condzd2_3786;
				obj_t BgL_ppzd2ifzd2_3787;
				obj_t BgL_ppzd2defunzd2_3788;
				obj_t BgL_ppzd2definezd2_3789;
				obj_t BgL_ppzd2lambdazd2_3790;
				obj_t BgL_outputz00_3791;
				long BgL_maxzd2callzd2headzd2widthzd2_3792;

				BgL_indentzd2generalzd2_3775 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_3774, (int) (0L)));
				BgL_ppzd2exprzd2defunz00_3776 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3774, (int) (1L)));
				BgL_widthz00_3777 = PROCEDURE_REF(BgL_envz00_3774, (int) (2L));
				BgL_displayzf3zf3_3778 = PROCEDURE_REF(BgL_envz00_3774, (int) (3L));
				BgL_ppzd2exprzd2listz00_3779 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3774, (int) (4L)));
				BgL_ppzd2commentzd2_3780 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3774, (int) (5L)));
				BgL_ppzd2dozd2_3781 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3774, (int) (6L)));
				BgL_ppzd2beginzd2_3782 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3774, (int) (7L)));
				BgL_ppzd2letzd2_3783 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3774, (int) (8L)));
				BgL_ppzd2andzd2_3784 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3774, (int) (9L)));
				BgL_ppzd2casezd2_3785 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3774, (int) (10L)));
				BgL_ppzd2condzd2_3786 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3774, (int) (11L)));
				BgL_ppzd2ifzd2_3787 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3774, (int) (12L)));
				BgL_ppzd2defunzd2_3788 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3774, (int) (13L)));
				BgL_ppzd2definezd2_3789 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3774, (int) (14L)));
				BgL_ppzd2lambdazd2_3790 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3774, (int) (15L)));
				BgL_outputz00_3791 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3774, (int) (16L)));
				BgL_maxzd2callzd2headzd2widthzd2_3792 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_3774, (int) (17L)));
				{
					obj_t BgL_headz00_3890;

					if (BGl_z62readzd2macrozf3z43zz__ppz00(BgL_exprz00_3793))
						{	/* Pp/pp.scm 281 */
							obj_t BgL_arg1447z00_3900;
							obj_t BgL_arg1448z00_3901;

							{	/* Pp/pp.scm 122 */
								obj_t BgL_pairz00_3902;

								BgL_pairz00_3902 = CDR(((obj_t) BgL_exprz00_3793));
								BgL_arg1447z00_3900 = CAR(BgL_pairz00_3902);
							}
							{	/* Pp/pp.scm 282 */
								obj_t BgL_arg1449z00_3903;

								BgL_arg1449z00_3903 =
									BGl_z62readzd2macrozd2prefixz62zz__ppz00(BgL_exprz00_3793);
								if (CBOOL(BgL_colz00_3794))
									{	/* Pp/pp.scm 133 */
										obj_t BgL__andtest_1044z00_3904;

										BgL__andtest_1044z00_3904 =
											((obj_t(*)(obj_t,
													obj_t))
											PROCEDURE_L_ENTRY(BgL_outputz00_3791))
											(BgL_outputz00_3791, BgL_arg1449z00_3903);
										if (CBOOL(BgL__andtest_1044z00_3904))
											{	/* Pp/pp.scm 133 */
												BgL_arg1448z00_3901 =
													ADDFX(BgL_colz00_3794,
													BINT(STRING_LENGTH(((obj_t) BgL_arg1449z00_3903))));
											}
										else
											{	/* Pp/pp.scm 133 */
												BgL_arg1448z00_3901 = BFALSE;
											}
									}
								else
									{	/* Pp/pp.scm 133 */
										BgL_arg1448z00_3901 = BFALSE;
									}
							}
							return
								BGl_z62prz62zz__ppz00(BgL_envz00_3774, BgL_outputz00_3791,
								BgL_displayzf3zf3_3778, BgL_widthz00_3777,
								BgL_maxzd2callzd2headzd2widthzd2_3792, BgL_ppzd2lambdazd2_3790,
								BgL_ppzd2definezd2_3789, BgL_ppzd2defunzd2_3788,
								BgL_ppzd2ifzd2_3787, BgL_ppzd2condzd2_3786,
								BgL_ppzd2casezd2_3785, BgL_ppzd2andzd2_3784,
								BgL_ppzd2letzd2_3783, BgL_ppzd2beginzd2_3782,
								BgL_ppzd2dozd2_3781, BgL_ppzd2commentzd2_3780,
								BgL_ppzd2exprzd2listz00_3779, BgL_ppzd2exprzd2defunz00_3776,
								BgL_indentzd2generalzd2_3775, BgL_arg1447z00_3900,
								BgL_arg1448z00_3901, BgL_extraz00_3795, BgL_envz00_3774);
						}
					else
						{	/* Pp/pp.scm 285 */
							obj_t BgL_headz00_3905;

							BgL_headz00_3905 = CAR(((obj_t) BgL_exprz00_3793));
							if (SYMBOLP(BgL_headz00_3905))
								{	/* Pp/pp.scm 287 */
									obj_t BgL_procz00_3906;

									BgL_headz00_3890 = BgL_headz00_3905;
									{	/* Pp/pp.scm 442 */
										obj_t BgL_casezd2valuezd2_3891;

										if (
											(BGl_za2ppzd2caseza2zd2zz__ppz00 ==
												BGl_symbol1950z00zz__ppz00))
											{	/* Pp/pp.scm 443 */
												obj_t BgL_arg1556z00_3892;

												BgL_arg1556z00_3892 =
													SYMBOL_TO_STRING(BgL_headz00_3890);
												BgL_casezd2valuezd2_3891 =
													bstring_to_symbol(BgL_arg1556z00_3892);
											}
										else
											{	/* Pp/pp.scm 442 */
												BgL_casezd2valuezd2_3891 = BgL_headz00_3890;
											}
										if (
											(BgL_casezd2valuezd2_3891 == BGl_symbol2000z00zz__ppz00))
											{	/* Pp/pp.scm 442 */
												BgL_procz00_3906 = BgL_ppzd2lambdazd2_3790;
											}
										else
											{	/* Pp/pp.scm 442 */
												bool_t BgL_test2327z00_6499;

												{	/* Pp/pp.scm 442 */
													bool_t BgL__ortest_1054z00_3893;

													BgL__ortest_1054z00_3893 =
														(BgL_casezd2valuezd2_3891 ==
														BGl_symbol2002z00zz__ppz00);
													if (BgL__ortest_1054z00_3893)
														{	/* Pp/pp.scm 442 */
															BgL_test2327z00_6499 = BgL__ortest_1054z00_3893;
														}
													else
														{	/* Pp/pp.scm 442 */
															BgL_test2327z00_6499 =
																(BgL_casezd2valuezd2_3891 ==
																BGl_symbol2004z00zz__ppz00);
														}
												}
												if (BgL_test2327z00_6499)
													{	/* Pp/pp.scm 442 */
														BgL_procz00_3906 = BgL_ppzd2letzd2_3783;
													}
												else
													{	/* Pp/pp.scm 442 */
														bool_t BgL_test2329z00_6503;

														{	/* Pp/pp.scm 442 */
															bool_t BgL__ortest_1055z00_3894;

															BgL__ortest_1055z00_3894 =
																(BgL_casezd2valuezd2_3891 ==
																BGl_symbol2006z00zz__ppz00);
															if (BgL__ortest_1055z00_3894)
																{	/* Pp/pp.scm 442 */
																	BgL_test2329z00_6503 =
																		BgL__ortest_1055z00_3894;
																}
															else
																{	/* Pp/pp.scm 442 */
																	bool_t BgL__ortest_1056z00_3895;

																	BgL__ortest_1056z00_3895 =
																		(BgL_casezd2valuezd2_3891 ==
																		BGl_symbol2008z00zz__ppz00);
																	if (BgL__ortest_1056z00_3895)
																		{	/* Pp/pp.scm 442 */
																			BgL_test2329z00_6503 =
																				BgL__ortest_1056z00_3895;
																		}
																	else
																		{	/* Pp/pp.scm 442 */
																			bool_t BgL__ortest_1057z00_3896;

																			BgL__ortest_1057z00_3896 =
																				(BgL_casezd2valuezd2_3891 ==
																				BGl_symbol2010z00zz__ppz00);
																			if (BgL__ortest_1057z00_3896)
																				{	/* Pp/pp.scm 442 */
																					BgL_test2329z00_6503 =
																						BgL__ortest_1057z00_3896;
																				}
																			else
																				{	/* Pp/pp.scm 442 */
																					BgL_test2329z00_6503 =
																						(BgL_casezd2valuezd2_3891 ==
																						BGl_symbol2012z00zz__ppz00);
																				}
																		}
																}
														}
														if (BgL_test2329z00_6503)
															{	/* Pp/pp.scm 442 */
																BgL_procz00_3906 = BgL_ppzd2definezd2_3789;
															}
														else
															{	/* Pp/pp.scm 442 */
																if (
																	(BgL_casezd2valuezd2_3891 ==
																		BGl_symbol2014z00zz__ppz00))
																	{	/* Pp/pp.scm 442 */
																		BgL_procz00_3906 = BgL_ppzd2definezd2_3789;
																	}
																else
																	{	/* Pp/pp.scm 442 */
																		bool_t BgL_test2334z00_6513;

																		{	/* Pp/pp.scm 442 */
																			bool_t BgL__ortest_1058z00_3897;

																			BgL__ortest_1058z00_3897 =
																				(BgL_casezd2valuezd2_3891 ==
																				BGl_symbol2016z00zz__ppz00);
																			if (BgL__ortest_1058z00_3897)
																				{	/* Pp/pp.scm 442 */
																					BgL_test2334z00_6513 =
																						BgL__ortest_1058z00_3897;
																				}
																			else
																				{	/* Pp/pp.scm 442 */
																					BgL_test2334z00_6513 =
																						(BgL_casezd2valuezd2_3891 ==
																						BGl_symbol2018z00zz__ppz00);
																				}
																		}
																		if (BgL_test2334z00_6513)
																			{	/* Pp/pp.scm 442 */
																				BgL_procz00_3906 =
																					BgL_ppzd2defunzd2_3788;
																			}
																		else
																			{	/* Pp/pp.scm 442 */
																				bool_t BgL_test2336z00_6517;

																				{	/* Pp/pp.scm 442 */
																					bool_t BgL__ortest_1059z00_3898;

																					BgL__ortest_1059z00_3898 =
																						(BgL_casezd2valuezd2_3891 ==
																						BGl_symbol2020z00zz__ppz00);
																					if (BgL__ortest_1059z00_3898)
																						{	/* Pp/pp.scm 442 */
																							BgL_test2336z00_6517 =
																								BgL__ortest_1059z00_3898;
																						}
																					else
																						{	/* Pp/pp.scm 442 */
																							BgL_test2336z00_6517 =
																								(BgL_casezd2valuezd2_3891 ==
																								BGl_symbol2022z00zz__ppz00);
																						}
																				}
																				if (BgL_test2336z00_6517)
																					{	/* Pp/pp.scm 442 */
																						BgL_procz00_3906 =
																							BgL_ppzd2ifzd2_3787;
																					}
																				else
																					{	/* Pp/pp.scm 442 */
																						if (
																							(BgL_casezd2valuezd2_3891 ==
																								BGl_symbol2024z00zz__ppz00))
																							{	/* Pp/pp.scm 442 */
																								BgL_procz00_3906 =
																									BgL_ppzd2condzd2_3786;
																							}
																						else
																							{	/* Pp/pp.scm 442 */
																								if (
																									(BgL_casezd2valuezd2_3891 ==
																										BGl_symbol2026z00zz__ppz00))
																									{	/* Pp/pp.scm 442 */
																										BgL_procz00_3906 =
																											BgL_ppzd2casezd2_3785;
																									}
																								else
																									{	/* Pp/pp.scm 442 */
																										bool_t BgL_test2340z00_6525;

																										{	/* Pp/pp.scm 442 */
																											bool_t
																												BgL__ortest_1060z00_3899;
																											BgL__ortest_1060z00_3899 =
																												(BgL_casezd2valuezd2_3891
																												==
																												BGl_symbol2028z00zz__ppz00);
																											if (BgL__ortest_1060z00_3899)
																												{	/* Pp/pp.scm 442 */
																													BgL_test2340z00_6525 =
																														BgL__ortest_1060z00_3899;
																												}
																											else
																												{	/* Pp/pp.scm 442 */
																													BgL_test2340z00_6525 =
																														(BgL_casezd2valuezd2_3891
																														==
																														BGl_symbol2030z00zz__ppz00);
																												}
																										}
																										if (BgL_test2340z00_6525)
																											{	/* Pp/pp.scm 442 */
																												BgL_procz00_3906 =
																													BgL_ppzd2andzd2_3784;
																											}
																										else
																											{	/* Pp/pp.scm 442 */
																												if (
																													(BgL_casezd2valuezd2_3891
																														==
																														BGl_symbol2032z00zz__ppz00))
																													{	/* Pp/pp.scm 442 */
																														BgL_procz00_3906 =
																															BgL_ppzd2letzd2_3783;
																													}
																												else
																													{	/* Pp/pp.scm 442 */
																														if (
																															(BgL_casezd2valuezd2_3891
																																==
																																BGl_symbol2034z00zz__ppz00))
																															{	/* Pp/pp.scm 442 */
																																BgL_procz00_3906
																																	=
																																	BgL_ppzd2beginzd2_3782;
																															}
																														else
																															{	/* Pp/pp.scm 442 */
																																if (
																																	(BgL_casezd2valuezd2_3891
																																		==
																																		BGl_symbol2036z00zz__ppz00))
																																	{	/* Pp/pp.scm 442 */
																																		BgL_procz00_3906
																																			=
																																			BgL_ppzd2dozd2_3781;
																																	}
																																else
																																	{	/* Pp/pp.scm 442 */
																																		if (
																																			(BgL_casezd2valuezd2_3891
																																				==
																																				BGl_symbol1963z00zz__ppz00))
																																			{	/* Pp/pp.scm 442 */
																																				BgL_procz00_3906
																																					=
																																					BgL_ppzd2commentzd2_3780;
																																			}
																																		else
																																			{	/* Pp/pp.scm 442 */
																																				BgL_procz00_3906
																																					=
																																					BFALSE;
																																			}
																																	}
																															}
																													}
																											}
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
									}
									if (CBOOL(BgL_procz00_3906))
										{	/* Pp/pp.scm 288 */
											return
												BGL_PROCEDURE_CALL3(BgL_procz00_3906, BgL_exprz00_3793,
												BgL_colz00_3794, BgL_extraz00_3795);
										}
									else
										{	/* Pp/pp.scm 290 */
											bool_t BgL_test2347z00_6545;

											{	/* Pp/pp.scm 290 */
												long BgL_arg1454z00_3907;

												{	/* Pp/pp.scm 290 */
													obj_t BgL_arg1455z00_3908;

													BgL_arg1455z00_3908 =
														SYMBOL_TO_STRING(BgL_headz00_3905);
													BgL_arg1454z00_3907 =
														STRING_LENGTH(BgL_arg1455z00_3908);
												}
												BgL_test2347z00_6545 =
													(BgL_arg1454z00_3907 >
													BgL_maxzd2callzd2headzd2widthzd2_3792);
											}
											if (BgL_test2347z00_6545)
												{	/* Pp/pp.scm 290 */
													return
														BGl_z62ppzd2generalzb0zz__ppz00
														(BgL_indentzd2generalzd2_3775, BgL_outputz00_3791,
														BgL_envz00_3774, BgL_displayzf3zf3_3778,
														BgL_widthz00_3777,
														BgL_maxzd2callzd2headzd2widthzd2_3792,
														BgL_ppzd2lambdazd2_3790, BgL_ppzd2definezd2_3789,
														BgL_ppzd2defunzd2_3788, BgL_ppzd2ifzd2_3787,
														BgL_ppzd2condzd2_3786, BgL_ppzd2casezd2_3785,
														BgL_ppzd2andzd2_3784, BgL_ppzd2letzd2_3783,
														BgL_ppzd2beginzd2_3782, BgL_ppzd2dozd2_3781,
														BgL_ppzd2commentzd2_3780,
														BgL_ppzd2exprzd2listz00_3779,
														BgL_ppzd2exprzd2defunz00_3776, BgL_exprz00_3793,
														BgL_colz00_3794, BgL_extraz00_3795, ((bool_t) 0),
														BFALSE, BFALSE, BgL_envz00_3774);
												}
											else
												{	/* Pp/pp.scm 300 */
													obj_t BgL_colza2za2_3909;

													{	/* Pp/pp.scm 300 */
														obj_t BgL_arg1459z00_3910;
														obj_t BgL_arg1460z00_3911;

														BgL_arg1459z00_3910 =
															CAR(((obj_t) BgL_exprz00_3793));
														if (CBOOL(BgL_colz00_3794))
															{	/* Pp/pp.scm 133 */
																obj_t BgL__andtest_1044z00_3912;

																BgL__andtest_1044z00_3912 =
																	((obj_t(*)(obj_t,
																			obj_t))
																	PROCEDURE_L_ENTRY(BgL_outputz00_3791))
																	(BgL_outputz00_3791,
																	BGl_string1960z00zz__ppz00);
																if (CBOOL(BgL__andtest_1044z00_3912))
																	{	/* Pp/pp.scm 133 */
																		BgL_arg1460z00_3911 =
																			ADDFX(BgL_colz00_3794, BINT(1L));
																	}
																else
																	{	/* Pp/pp.scm 133 */
																		BgL_arg1460z00_3911 = BFALSE;
																	}
															}
														else
															{	/* Pp/pp.scm 133 */
																BgL_arg1460z00_3911 = BFALSE;
															}
														BgL_colza2za2_3909 =
															BGl_z62wrz62zz__ppz00(BgL_displayzf3zf3_3778,
															BgL_outputz00_3791, BgL_arg1459z00_3910,
															BgL_arg1460z00_3911);
													}
													if (CBOOL(BgL_colz00_3794))
														{	/* Pp/pp.scm 302 */
															obj_t BgL_arg1457z00_3913;
															long BgL_arg1458z00_3914;

															BgL_arg1457z00_3913 =
																CDR(((obj_t) BgL_exprz00_3793));
															BgL_arg1458z00_3914 =
																((long) CINT(BgL_colza2za2_3909) + 1L);
															return
																BGl_z62ppzd2downzb0zz__ppz00(BgL_outputz00_3791,
																BgL_envz00_3774, BgL_displayzf3zf3_3778,
																BgL_widthz00_3777,
																BgL_maxzd2callzd2headzd2widthzd2_3792,
																BgL_ppzd2lambdazd2_3790,
																BgL_ppzd2definezd2_3789, BgL_ppzd2defunzd2_3788,
																BgL_ppzd2ifzd2_3787, BgL_ppzd2condzd2_3786,
																BgL_ppzd2casezd2_3785, BgL_ppzd2andzd2_3784,
																BgL_ppzd2letzd2_3783, BgL_ppzd2beginzd2_3782,
																BgL_ppzd2dozd2_3781, BgL_ppzd2commentzd2_3780,
																BgL_ppzd2exprzd2listz00_3779,
																BgL_ppzd2exprzd2defunz00_3776,
																BgL_indentzd2generalzd2_3775,
																BgL_arg1457z00_3913, BgL_colza2za2_3909,
																BINT(BgL_arg1458z00_3914), BgL_extraz00_3795,
																BgL_envz00_3774);
														}
													else
														{	/* Pp/pp.scm 301 */
															return BFALSE;
														}
												}
										}
								}
							else
								{	/* Pp/pp.scm 308 */
									obj_t BgL_colz00_3915;

									if (CBOOL(BgL_colz00_3794))
										{	/* Pp/pp.scm 133 */
											obj_t BgL__andtest_1044z00_3916;

											BgL__andtest_1044z00_3916 =
												((obj_t(*)(obj_t,
														obj_t))
												PROCEDURE_L_ENTRY(BgL_outputz00_3791))
												(BgL_outputz00_3791, BGl_string1960z00zz__ppz00);
											if (CBOOL(BgL__andtest_1044z00_3916))
												{	/* Pp/pp.scm 133 */
													BgL_colz00_3915 = ADDFX(BgL_colz00_3794, BINT(1L));
												}
											else
												{	/* Pp/pp.scm 133 */
													BgL_colz00_3915 = BFALSE;
												}
										}
									else
										{	/* Pp/pp.scm 133 */
											BgL_colz00_3915 = BFALSE;
										}
									return
										BGl_z62ppzd2downzb0zz__ppz00(BgL_outputz00_3791,
										BgL_envz00_3774, BgL_displayzf3zf3_3778, BgL_widthz00_3777,
										BgL_maxzd2callzd2headzd2widthzd2_3792,
										BgL_ppzd2lambdazd2_3790, BgL_ppzd2definezd2_3789,
										BgL_ppzd2defunzd2_3788, BgL_ppzd2ifzd2_3787,
										BgL_ppzd2condzd2_3786, BgL_ppzd2casezd2_3785,
										BgL_ppzd2andzd2_3784, BgL_ppzd2letzd2_3783,
										BgL_ppzd2beginzd2_3782, BgL_ppzd2dozd2_3781,
										BgL_ppzd2commentzd2_3780, BgL_ppzd2exprzd2listz00_3779,
										BgL_ppzd2exprzd2defunz00_3776, BgL_indentzd2generalzd2_3775,
										BgL_exprz00_3793, BgL_colz00_3915, BgL_colz00_3915,
										BgL_extraz00_3795, BgL_envz00_3774);
								}
						}
				}
			}
		}

	}



/* &<@anonymous:1435> */
	obj_t BGl_z62zc3z04anonymousza31435ze3ze5zz__ppz00(obj_t BgL_envz00_3796,
		obj_t BgL_strz00_3799)
	{
		{	/* Pp/pp.scm 266 */
			{	/* Pp/pp.scm 267 */
				obj_t BgL_resultz00_3797;
				obj_t BgL_leftz00_3798;

				BgL_resultz00_3797 = PROCEDURE_L_REF(BgL_envz00_3796, (int) (0L));
				BgL_leftz00_3798 = PROCEDURE_L_REF(BgL_envz00_3796, (int) (1L));
				{	/* Pp/pp.scm 267 */
					bool_t BgL_tmpz00_6586;

					{	/* Pp/pp.scm 267 */
						obj_t BgL_auxz00_3917;

						BgL_auxz00_3917 =
							MAKE_YOUNG_PAIR(BgL_strz00_3799, CELL_REF(BgL_resultz00_3797));
						CELL_SET(BgL_resultz00_3797, BgL_auxz00_3917);
					}
					{	/* Pp/pp.scm 268 */
						obj_t BgL_auxz00_3918;

						BgL_auxz00_3918 =
							SUBFX(CELL_REF(BgL_leftz00_3798),
							BINT(STRING_LENGTH(((obj_t) BgL_strz00_3799))));
						CELL_SET(BgL_leftz00_3798, BgL_auxz00_3918);
					}
					BgL_tmpz00_6586 = ((long) CINT(CELL_REF(BgL_leftz00_3798)) > 0L);
					return BBOOL(BgL_tmpz00_6586);
				}
			}
		}

	}



/* &<@anonymous:1369> */
	obj_t BGl_z62zc3z04anonymousza31369ze3ze5zz__ppz00(obj_t BgL_envz00_3802)
	{
		{	/* Pp/pp.scm 207 */
			{	/* Pp/pp.scm 207 */
				obj_t BgL_objz00_3803;

				BgL_objz00_3803 = PROCEDURE_REF(BgL_envz00_3802, (int) (0L));
				{	/* Pp/pp.scm 207 */
					obj_t BgL_arg1370z00_3919;

					{	/* Pp/pp.scm 207 */
						obj_t BgL_tmpz00_6597;

						BgL_tmpz00_6597 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_arg1370z00_3919 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6597);
					}
					return bgl_display_obj(BgL_objz00_3803, BgL_arg1370z00_3919);
				}
			}
		}

	}



/* reverse-string-append */
	obj_t BGl_reversezd2stringzd2appendz00zz__ppz00(obj_t BgL_lz00_10)
	{
		{	/* Pp/pp.scm 468 */
			return BGl_revzd2stringzd2appendze70ze7zz__ppz00(BgL_lz00_10, 0L);
		}

	}



/* rev-string-append~0 */
	obj_t BGl_revzd2stringzd2appendze70ze7zz__ppz00(obj_t BgL_lz00_1820,
		long BgL_iz00_1821)
	{
		{	/* Pp/pp.scm 481 */
			if (PAIRP(BgL_lz00_1820))
				{	/* Pp/pp.scm 472 */
					obj_t BgL_strz00_1824;

					BgL_strz00_1824 = CAR(BgL_lz00_1820);
					{	/* Pp/pp.scm 472 */
						long BgL_lenz00_1825;

						BgL_lenz00_1825 = STRING_LENGTH(((obj_t) BgL_strz00_1824));
						{	/* Pp/pp.scm 473 */
							obj_t BgL_resultz00_1826;

							BgL_resultz00_1826 =
								BGl_revzd2stringzd2appendze70ze7zz__ppz00(CDR(BgL_lz00_1820),
								(BgL_iz00_1821 + BgL_lenz00_1825));
							{	/* Pp/pp.scm 474 */

								{	/* Pp/pp.scm 475 */
									long BgL_g1061z00_1827;

									BgL_g1061z00_1827 =
										(
										(STRING_LENGTH(BgL_resultz00_1826) - BgL_iz00_1821) -
										BgL_lenz00_1825);
									{
										long BgL_jz00_3139;
										long BgL_kz00_3140;

										BgL_jz00_3139 = 0L;
										BgL_kz00_3140 = BgL_g1061z00_1827;
									BgL_loopz00_3138:
										if ((BgL_jz00_3139 < BgL_lenz00_1825))
											{	/* Pp/pp.scm 476 */
												{	/* Pp/pp.scm 478 */
													unsigned char BgL_arg1561z00_3146;

													BgL_arg1561z00_3146 =
														STRING_REF(
														((obj_t) BgL_strz00_1824), BgL_jz00_3139);
													STRING_SET(BgL_resultz00_1826, BgL_kz00_3140,
														BgL_arg1561z00_3146);
												}
												{
													long BgL_kz00_6620;
													long BgL_jz00_6618;

													BgL_jz00_6618 = (BgL_jz00_3139 + 1L);
													BgL_kz00_6620 = (BgL_kz00_3140 + 1L);
													BgL_kz00_3140 = BgL_kz00_6620;
													BgL_jz00_3139 = BgL_jz00_6618;
													goto BgL_loopz00_3138;
												}
											}
										else
											{	/* Pp/pp.scm 476 */
												return BgL_resultz00_1826;
											}
									}
								}
							}
						}
					}
				}
			else
				{	/* Ieee/string.scm 172 */

					return make_string(BgL_iz00_1821, ((unsigned char) ' '));
		}}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__ppz00(void)
	{
		{	/* Pp/pp.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__ppz00(void)
	{
		{	/* Pp/pp.scm 14 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__ppz00(void)
	{
		{	/* Pp/pp.scm 14 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__ppz00(void)
	{
		{	/* Pp/pp.scm 14 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string2038z00zz__ppz00));
			return
				BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L,
				BSTRING_TO_STRING(BGl_string2038z00zz__ppz00));
		}

	}

#ifdef __cplusplus
}
#endif
