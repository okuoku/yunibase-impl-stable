/*===========================================================================*/
/*   (Pp/circle.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Pp/circle.scm -indent -o objs/obj_u/Pp/circle.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___PP_CIRCLE_TYPE_DEFINITIONS
#define BGL___PP_CIRCLE_TYPE_DEFINITIONS

/* object type definitions */

#endif													// BGL___PP_CIRCLE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_symbol1834z00zz__pp_circlez00 = BUNSPEC;
	static obj_t BGl_z62jvmzd2debuggingzd2printz62zz__pp_circlez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__pp_circlez00 = BUNSPEC;
	extern obj_t bgl_write_ucs2(obj_t, obj_t);
	extern obj_t bgl_display_obj(obj_t, obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_writezd2stringzd2zz__r4_output_6_10_3z00(obj_t, obj_t);
	extern obj_t bgl_write_char(obj_t, obj_t);
	extern obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zz__pp_circlez00(void);
	static obj_t BGl_objectzd2initzd2zz__pp_circlez00(void);
	extern obj_t BGl_createzd2hashtablezd2zz__hashz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL char *BGl_jvmzd2debuggingzd2printz00zz__pp_circlez00(obj_t,
		int);
	extern bool_t BGl_hashtablezf3zf3zz__hashz00(obj_t);
	extern obj_t BGl_writezd2ucs2stringzd2zz__r4_output_6_10_3z00(obj_t, obj_t);
	extern obj_t BGl_objectzd2printzd2zz__objectz00(BgL_objectz00_bglt, obj_t,
		obj_t);
	extern obj_t BGl_displayzd2flonumzd2zz__r4_output_6_10_3z00(obj_t, obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	extern obj_t BGl_writez00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zz__pp_circlez00(void);
	static obj_t BGl_registerze70ze7zz__pp_circlez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31309ze3ze5zz__pp_circlez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t dprint(obj_t);
	extern obj_t BGl_classzd2fieldzd2accessorz00zz__objectz00(obj_t);
	static obj_t BGl_loopzd2matchedze70z35zz__pp_circlez00(obj_t, obj_t, bool_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31362ze3ze5zz__pp_circlez00(obj_t);
	static obj_t BGl__displayzd2circlezd2zz__pp_circlez00(obj_t, obj_t);
	extern obj_t string_for_read(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__pp_circlez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__hashz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t bgl_display_ucs2(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31364ze3ze5zz__pp_circlez00(obj_t);
	extern bool_t BGl_classzf3zf3zz__objectz00(obj_t);
	extern bool_t BGl_numberzf3zf3zz__r4_numbers_6_5z00(obj_t);
	static obj_t BGl__writezd2circlezd2zz__pp_circlez00(obj_t, obj_t);
	extern obj_t BGl_displayzd2ucs2stringzd2zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__pp_circlez00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zz__pp_circlez00(void);
	extern obj_t BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zz__pp_circlez00(void);
	extern obj_t bgl_display_fixnum(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_displayzd2circlezd2zz__pp_circlez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_writezd2circlezd2zz__pp_circlez00(obj_t, obj_t);
	static obj_t BGl_z62czd2debuggingzd2printz62zz__pp_circlez00(obj_t, obj_t);
	extern obj_t BGl_hashtablezd2putz12zc0zz__hashz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_displayzd2symbolzd2zz__r4_output_6_10_3z00(obj_t, obj_t);
	extern obj_t BGl_writezd2symbolzd2zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_circlezd2writezf2displayz20zz__pp_circlez00(obj_t, obj_t,
		bool_t);
	static obj_t BGl_z62outputzd2componentzb0zz__pp_circlez00(obj_t, bool_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_hashtablezd2getzd2zz__hashz00(obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_czd2debuggingzd2printzd2envzd2zz__pp_circlez00,
		BgL_bgl_za762cza7d2debugging1846z00,
		BGl_z62czd2debuggingzd2printz62zz__pp_circlez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jvmzd2debuggingzd2printzd2envzd2zz__pp_circlez00,
		BgL_bgl_za762jvmza7d2debuggi1847z00,
		BGl_z62jvmzd2debuggingzd2printz62zz__pp_circlez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_displayzd2circlezd2envz00zz__pp_circlez00,
		BgL_bgl__displayza7d2circl1848za7, opt_generic_entry,
		BGl__displayzd2circlezd2zz__pp_circlez00, BFALSE, -1);
	      DEFINE_REAL(BGl_real1836z00zz__pp_circlez00,
		BgL_bgl_real1836za700za7za7__p1849za7, 2.0);
	      DEFINE_STRING(BGl_string1829z00zz__pp_circlez00,
		BgL_bgl_string1829za700za7za7_1850za7, "/tmp/bigloo/runtime/Pp/circle.scm",
		33);
	      DEFINE_STRING(BGl_string1830z00zz__pp_circlez00,
		BgL_bgl_string1830za700za7za7_1851za7, "_display-circle", 15);
	      DEFINE_STRING(BGl_string1831z00zz__pp_circlez00,
		BgL_bgl_string1831za700za7za7_1852za7, "vector", 6);
	      DEFINE_STRING(BGl_string1832z00zz__pp_circlez00,
		BgL_bgl_string1832za700za7za7_1853za7, "output-port", 11);
	      DEFINE_STRING(BGl_string1833z00zz__pp_circlez00,
		BgL_bgl_string1833za700za7za7_1854za7, "_write-circle", 13);
	      DEFINE_STRING(BGl_string1835z00zz__pp_circlez00,
		BgL_bgl_string1835za700za7za7_1855za7, "none", 4);
	      DEFINE_STRING(BGl_string1837z00zz__pp_circlez00,
		BgL_bgl_string1837za700za7za7_1856za7, " . ", 3);
	      DEFINE_STRING(BGl_string1838z00zz__pp_circlez00,
		BgL_bgl_string1838za700za7za7_1857za7, "#{", 2);
	      DEFINE_STRING(BGl_string1839z00zz__pp_circlez00,
		BgL_bgl_string1839za700za7za7_1858za7, "#<cell:", 7);
	      DEFINE_STRING(BGl_string1840z00zz__pp_circlez00,
		BgL_bgl_string1840za700za7za7_1859za7, ">", 1);
	      DEFINE_STRING(BGl_string1841z00zz__pp_circlez00,
		BgL_bgl_string1841za700za7za7_1860za7, "#<mutex:", 8);
	      DEFINE_STRING(BGl_string1842z00zz__pp_circlez00,
		BgL_bgl_string1842za700za7za7_1861za7, "#<condition-variable:", 21);
	      DEFINE_STRING(BGl_string1843z00zz__pp_circlez00,
		BgL_bgl_string1843za700za7za7_1862za7, "&jvm-debugging-print", 20);
	      DEFINE_STRING(BGl_string1844z00zz__pp_circlez00,
		BgL_bgl_string1844za700za7za7_1863za7, "bint", 4);
	      DEFINE_STRING(BGl_string1845z00zz__pp_circlez00,
		BgL_bgl_string1845za700za7za7_1864za7, "__pp_circle", 11);
	extern obj_t BGl_eqzf3zd2envz21zz__r4_equivalence_6_2z00;
	extern obj_t BGl_displayzd2envzd2zz__r4_output_6_10_3z00;
	extern obj_t BGl_writezd2envzd2zz__r4_output_6_10_3z00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_writezd2circlezd2envz00zz__pp_circlez00,
		BgL_bgl__writeza7d2circleza71865z00, opt_generic_entry,
		BGl__writezd2circlezd2zz__pp_circlez00, BFALSE, -1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_symbol1834z00zz__pp_circlez00));
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__pp_circlez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__pp_circlez00(long
		BgL_checksumz00_2487, char *BgL_fromz00_2488)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__pp_circlez00))
				{
					BGl_requirezd2initializa7ationz75zz__pp_circlez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__pp_circlez00();
					BGl_cnstzd2initzd2zz__pp_circlez00();
					BGl_importedzd2moduleszd2initz00zz__pp_circlez00();
					return BGl_methodzd2initzd2zz__pp_circlez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__pp_circlez00(void)
	{
		{	/* Pp/circle.scm 26 */
			return (BGl_symbol1834z00zz__pp_circlez00 =
				bstring_to_symbol(BGl_string1835z00zz__pp_circlez00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__pp_circlez00(void)
	{
		{	/* Pp/circle.scm 26 */
			return bgl_gc_roots_register();
		}

	}



/* _display-circle */
	obj_t BGl__displayzd2circlezd2zz__pp_circlez00(obj_t BgL_env1128z00_6,
		obj_t BgL_opt1127z00_5)
	{
		{	/* Pp/circle.scm 82 */
			{	/* Pp/circle.scm 82 */
				obj_t BgL_g1129z00_1323;

				{
					obj_t BgL_auxz00_2498;

					if (VECTORP(BgL_opt1127z00_5))
						{	/* Pp/circle.scm 82 */
							BgL_auxz00_2498 = BgL_opt1127z00_5;
						}
					else
						{
							obj_t BgL_auxz00_2501;

							BgL_auxz00_2501 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string1829z00zz__pp_circlez00, BINT(3230L),
								BGl_string1830z00zz__pp_circlez00,
								BGl_string1831z00zz__pp_circlez00, BgL_opt1127z00_5);
							FAILURE(BgL_auxz00_2501, BFALSE, BFALSE);
						}
					BgL_g1129z00_1323 = VECTOR_REF(BgL_auxz00_2498, 0L);
				}
				{	/* Pp/circle.scm 82 */
					long BgL_aux1131z00_1325;

					{
						obj_t BgL_auxz00_2506;

						if (VECTORP(BgL_opt1127z00_5))
							{	/* Pp/circle.scm 82 */
								BgL_auxz00_2506 = BgL_opt1127z00_5;
							}
						else
							{
								obj_t BgL_auxz00_2509;

								BgL_auxz00_2509 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1829z00zz__pp_circlez00, BINT(3230L),
									BGl_string1830z00zz__pp_circlez00,
									BGl_string1831z00zz__pp_circlez00, BgL_opt1127z00_5);
								FAILURE(BgL_auxz00_2509, BFALSE, BFALSE);
							}
						BgL_aux1131z00_1325 = VECTOR_LENGTH(BgL_auxz00_2506);
					}
					switch (BgL_aux1131z00_1325)
						{
						case 1L:

							{	/* Pp/circle.scm 82 */
								obj_t BgL_portz00_1326;

								{	/* Pp/circle.scm 82 */
									obj_t BgL_tmpz00_2514;

									BgL_tmpz00_2514 = BGL_CURRENT_DYNAMIC_ENV();
									BgL_portz00_1326 =
										BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_2514);
								}
								{	/* Pp/circle.scm 82 */

									return
										BGl_circlezd2writezf2displayz20zz__pp_circlez00
										(BgL_g1129z00_1323, BgL_portz00_1326, ((bool_t) 1));
								}
							}
							break;
						case 2L:

							{	/* Pp/circle.scm 82 */
								obj_t BgL_portz00_1327;

								{
									obj_t BgL_auxz00_2518;

									if (VECTORP(BgL_opt1127z00_5))
										{	/* Pp/circle.scm 82 */
											BgL_auxz00_2518 = BgL_opt1127z00_5;
										}
									else
										{
											obj_t BgL_auxz00_2521;

											BgL_auxz00_2521 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string1829z00zz__pp_circlez00, BINT(3230L),
												BGl_string1830z00zz__pp_circlez00,
												BGl_string1831z00zz__pp_circlez00, BgL_opt1127z00_5);
											FAILURE(BgL_auxz00_2521, BFALSE, BFALSE);
										}
									BgL_portz00_1327 = VECTOR_REF(BgL_auxz00_2518, 1L);
								}
								{	/* Pp/circle.scm 82 */

									{	/* Pp/circle.scm 82 */
										obj_t BgL_portz00_2041;

										if (OUTPUT_PORTP(BgL_portz00_1327))
											{	/* Pp/circle.scm 82 */
												BgL_portz00_2041 = BgL_portz00_1327;
											}
										else
											{
												obj_t BgL_auxz00_2528;

												BgL_auxz00_2528 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string1829z00zz__pp_circlez00, BINT(3230L),
													BGl_string1830z00zz__pp_circlez00,
													BGl_string1832z00zz__pp_circlez00, BgL_portz00_1327);
												FAILURE(BgL_auxz00_2528, BFALSE, BFALSE);
											}
										return
											BGl_circlezd2writezf2displayz20zz__pp_circlez00
											(BgL_g1129z00_1323, BgL_portz00_2041, ((bool_t) 1));
									}
								}
							}
							break;
						default:
							return BUNSPEC;
						}
				}
			}
		}

	}



/* display-circle */
	BGL_EXPORTED_DEF obj_t BGl_displayzd2circlezd2zz__pp_circlez00(obj_t
		BgL_oz00_3, obj_t BgL_portz00_4)
	{
		{	/* Pp/circle.scm 82 */
			return
				BGl_circlezd2writezf2displayz20zz__pp_circlez00(BgL_oz00_3,
				BgL_portz00_4, ((bool_t) 1));
		}

	}



/* _write-circle */
	obj_t BGl__writezd2circlezd2zz__pp_circlez00(obj_t BgL_env1133z00_10,
		obj_t BgL_opt1132z00_9)
	{
		{	/* Pp/circle.scm 88 */
			{	/* Pp/circle.scm 88 */
				obj_t BgL_g1134z00_1328;

				{
					obj_t BgL_auxz00_2535;

					if (VECTORP(BgL_opt1132z00_9))
						{	/* Pp/circle.scm 88 */
							BgL_auxz00_2535 = BgL_opt1132z00_9;
						}
					else
						{
							obj_t BgL_auxz00_2538;

							BgL_auxz00_2538 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string1829z00zz__pp_circlez00, BINT(3570L),
								BGl_string1833z00zz__pp_circlez00,
								BGl_string1831z00zz__pp_circlez00, BgL_opt1132z00_9);
							FAILURE(BgL_auxz00_2538, BFALSE, BFALSE);
						}
					BgL_g1134z00_1328 = VECTOR_REF(BgL_auxz00_2535, 0L);
				}
				{	/* Pp/circle.scm 88 */
					long BgL_aux1136z00_1330;

					{
						obj_t BgL_auxz00_2543;

						if (VECTORP(BgL_opt1132z00_9))
							{	/* Pp/circle.scm 88 */
								BgL_auxz00_2543 = BgL_opt1132z00_9;
							}
						else
							{
								obj_t BgL_auxz00_2546;

								BgL_auxz00_2546 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1829z00zz__pp_circlez00, BINT(3570L),
									BGl_string1833z00zz__pp_circlez00,
									BGl_string1831z00zz__pp_circlez00, BgL_opt1132z00_9);
								FAILURE(BgL_auxz00_2546, BFALSE, BFALSE);
							}
						BgL_aux1136z00_1330 = VECTOR_LENGTH(BgL_auxz00_2543);
					}
					switch (BgL_aux1136z00_1330)
						{
						case 1L:

							{	/* Pp/circle.scm 88 */
								obj_t BgL_portz00_1331;

								{	/* Pp/circle.scm 88 */
									obj_t BgL_tmpz00_2551;

									BgL_tmpz00_2551 = BGL_CURRENT_DYNAMIC_ENV();
									BgL_portz00_1331 =
										BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_2551);
								}
								{	/* Pp/circle.scm 88 */

									return
										BGl_circlezd2writezf2displayz20zz__pp_circlez00
										(BgL_g1134z00_1328, BgL_portz00_1331, ((bool_t) 0));
								}
							}
							break;
						case 2L:

							{	/* Pp/circle.scm 88 */
								obj_t BgL_portz00_1332;

								{
									obj_t BgL_auxz00_2555;

									if (VECTORP(BgL_opt1132z00_9))
										{	/* Pp/circle.scm 88 */
											BgL_auxz00_2555 = BgL_opt1132z00_9;
										}
									else
										{
											obj_t BgL_auxz00_2558;

											BgL_auxz00_2558 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string1829z00zz__pp_circlez00, BINT(3570L),
												BGl_string1833z00zz__pp_circlez00,
												BGl_string1831z00zz__pp_circlez00, BgL_opt1132z00_9);
											FAILURE(BgL_auxz00_2558, BFALSE, BFALSE);
										}
									BgL_portz00_1332 = VECTOR_REF(BgL_auxz00_2555, 1L);
								}
								{	/* Pp/circle.scm 88 */

									{	/* Pp/circle.scm 88 */
										obj_t BgL_portz00_2044;

										if (OUTPUT_PORTP(BgL_portz00_1332))
											{	/* Pp/circle.scm 88 */
												BgL_portz00_2044 = BgL_portz00_1332;
											}
										else
											{
												obj_t BgL_auxz00_2565;

												BgL_auxz00_2565 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string1829z00zz__pp_circlez00, BINT(3570L),
													BGl_string1833z00zz__pp_circlez00,
													BGl_string1832z00zz__pp_circlez00, BgL_portz00_1332);
												FAILURE(BgL_auxz00_2565, BFALSE, BFALSE);
											}
										return
											BGl_circlezd2writezf2displayz20zz__pp_circlez00
											(BgL_g1134z00_1328, BgL_portz00_2044, ((bool_t) 0));
									}
								}
							}
							break;
						default:
							return BUNSPEC;
						}
				}
			}
		}

	}



/* write-circle */
	BGL_EXPORTED_DEF obj_t BGl_writezd2circlezd2zz__pp_circlez00(obj_t BgL_oz00_7,
		obj_t BgL_portz00_8)
	{
		{	/* Pp/circle.scm 88 */
			return
				BGl_circlezd2writezf2displayz20zz__pp_circlez00(BgL_oz00_7,
				BgL_portz00_8, ((bool_t) 0));
		}

	}



/* circle-write/display */
	obj_t BGl_circlezd2writezf2displayz20zz__pp_circlez00(obj_t BgL_objz00_11,
		obj_t BgL_portz00_12, bool_t BgL_flagz00_13)
	{
		{	/* Pp/circle.scm 113 */
			{	/* Pp/circle.scm 114 */
				obj_t BgL_cachez00_2431;

				BgL_cachez00_2431 = MAKE_CELL(BNIL);
				{	/* Pp/circle.scm 114 */
					struct bgl_cell BgL_box1875_2463z00;
					obj_t BgL_cachezd2countzd2_2463;

					BgL_cachezd2countzd2_2463 =
						MAKE_CELL_STACK(BINT(0L), BgL_box1875_2463z00);
					{	/* Pp/circle.scm 115 */
						obj_t BgL_nextzd2cardinalzd2_1335;

						{	/* Pp/circle.scm 116 */
							obj_t BgL_serialz00_2432;

							BgL_serialz00_2432 = MAKE_CELL(BINT(-1L));
							{	/* Pp/circle.scm 117 */
								obj_t BgL_zc3z04anonymousza31362ze3z87_2416;

								{
									int BgL_tmpz00_2574;

									BgL_tmpz00_2574 = (int) (1L);
									BgL_zc3z04anonymousza31362ze3z87_2416 =
										MAKE_EL_PROCEDURE(BgL_tmpz00_2574);
								}
								PROCEDURE_EL_SET(BgL_zc3z04anonymousza31362ze3z87_2416,
									(int) (0L), ((obj_t) BgL_serialz00_2432));
								BgL_nextzd2cardinalzd2_1335 =
									BgL_zc3z04anonymousza31362ze3z87_2416;
						}}
						{	/* Pp/circle.scm 116 */

							BGl_registerze70ze7zz__pp_circlez00(BgL_cachezd2countzd2_2463,
								BgL_cachez00_2431, BgL_objz00_11);
							BGl_z62outputzd2componentzb0zz__pp_circlez00
								(BgL_nextzd2cardinalzd2_1335, BgL_flagz00_13, BgL_portz00_12,
								BgL_cachez00_2431, BgL_objz00_11);
			}}}}
			return BUNSPEC;
		}

	}



/* register~0 */
	obj_t BGl_registerze70ze7zz__pp_circlez00(obj_t BgL_cachezd2countzd2_2460,
		obj_t BgL_cachez00_2459, obj_t BgL_objz00_1424)
	{
		{	/* Pp/circle.scm 176 */
		BGl_registerze70ze7zz__pp_circlez00:
			{	/* Pp/circle.scm 131 */
				bool_t BgL_test1876z00_2582;

				if (BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_objz00_1424))
					{	/* Pp/circle.scm 131 */
						BgL_test1876z00_2582 = ((bool_t) 1);
					}
				else
					{	/* Pp/circle.scm 131 */
						if (SYMBOLP(BgL_objz00_1424))
							{	/* Pp/circle.scm 132 */
								BgL_test1876z00_2582 = ((bool_t) 1);
							}
						else
							{	/* Pp/circle.scm 132 */
								if (STRINGP(BgL_objz00_1424))
									{	/* Pp/circle.scm 133 */
										BgL_test1876z00_2582 = ((bool_t) 1);
									}
								else
									{	/* Pp/circle.scm 133 */
										if (UCS2_STRINGP(BgL_objz00_1424))
											{	/* Pp/circle.scm 134 */
												BgL_test1876z00_2582 = ((bool_t) 1);
											}
										else
											{	/* Pp/circle.scm 134 */
												if (BGL_DATEP(BgL_objz00_1424))
													{	/* Pp/circle.scm 135 */
														BgL_test1876z00_2582 = ((bool_t) 1);
													}
												else
													{	/* Pp/circle.scm 135 */
														if (CNSTP(BgL_objz00_1424))
															{	/* Pp/circle.scm 136 */
																BgL_test1876z00_2582 = ((bool_t) 1);
															}
														else
															{	/* Pp/circle.scm 136 */
																if (NULLP(BgL_objz00_1424))
																	{	/* Pp/circle.scm 137 */
																		BgL_test1876z00_2582 = ((bool_t) 1);
																	}
																else
																	{	/* Pp/circle.scm 137 */
																		BgL_test1876z00_2582 =
																			BGl_classzf3zf3zz__objectz00
																			(BgL_objz00_1424);
																	}
															}
													}
											}
									}
							}
					}
				if (BgL_test1876z00_2582)
					{	/* Pp/circle.scm 131 */
						return BFALSE;
					}
				else
					{	/* Pp/circle.scm 139 */
						obj_t BgL_matchz00_1434;

						if (BGl_hashtablezf3zf3zz__hashz00(CELL_REF(BgL_cachez00_2459)))
							{	/* Pp/circle.scm 123 */
								BgL_matchz00_1434 =
									BGl_hashtablezd2getzd2zz__hashz00(CELL_REF(BgL_cachez00_2459),
									BgL_objz00_1424);
							}
						else
							{	/* Pp/circle.scm 123 */
								BgL_matchz00_1434 =
									BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_objz00_1424,
									CELL_REF(BgL_cachez00_2459));
							}
						if (CBOOL(BgL_matchz00_1434))
							{	/* Pp/circle.scm 141 */
								obj_t BgL_tmpz00_2604;

								BgL_tmpz00_2604 = ((obj_t) BgL_matchz00_1434);
								return SET_CDR(BgL_tmpz00_2604, BTRUE);
							}
						else
							{	/* Pp/circle.scm 142 */
								obj_t BgL_entryz00_1435;

								BgL_entryz00_1435 = MAKE_YOUNG_PAIR(BgL_objz00_1424, BFALSE);
								if (BGl_hashtablezf3zf3zz__hashz00(CELL_REF(BgL_cachez00_2459)))
									{	/* Pp/circle.scm 143 */
										BGl_hashtablezd2putz12zc0zz__hashz00(CELL_REF
											(BgL_cachez00_2459), BgL_objz00_1424, BgL_entryz00_1435);
									}
								else
									{	/* Pp/circle.scm 145 */
										bool_t BgL_test1887z00_2611;

										{	/* Pp/circle.scm 145 */
											long BgL_n1z00_2049;

											BgL_n1z00_2049 =
												(long) CINT(
												((obj_t)
													((obj_t) CELL_REF(BgL_cachezd2countzd2_2460))));
											BgL_test1887z00_2611 = (BgL_n1z00_2049 > 64L);
										}
										if (BgL_test1887z00_2611)
											{	/* Pp/circle.scm 146 */
												obj_t BgL_hz00_1438;

												BgL_hz00_1438 =
													BGl_createzd2hashtablezd2zz__hashz00(BGL_REAL_CNST
													(BGl_real1836z00zz__pp_circlez00),
													BGl_eqzf3zd2envz21zz__r4_equivalence_6_2z00, BFALSE,
													BINT(10L), BINT(16384L), BFALSE, BINT(128L),
													BGl_symbol1834z00zz__pp_circlez00);
												{
													obj_t BgL_l1108z00_1440;

													BgL_l1108z00_1440 = CELL_REF(BgL_cachez00_2459);
												BgL_zc3z04anonymousza31333ze3z87_1441:
													if (PAIRP(BgL_l1108z00_1440))
														{	/* Pp/circle.scm 149 */
															{	/* Pp/circle.scm 150 */
																obj_t BgL_cellz00_1443;

																BgL_cellz00_1443 = CAR(BgL_l1108z00_1440);
																{	/* Pp/circle.scm 150 */
																	obj_t BgL_arg1335z00_1444;

																	BgL_arg1335z00_1444 =
																		CAR(((obj_t) BgL_cellz00_1443));
																	BGl_hashtablezd2putz12zc0zz__hashz00
																		(BgL_hz00_1438, BgL_arg1335z00_1444,
																		BgL_cellz00_1443);
																}
															}
															{
																obj_t BgL_l1108z00_2625;

																BgL_l1108z00_2625 = CDR(BgL_l1108z00_1440);
																BgL_l1108z00_1440 = BgL_l1108z00_2625;
																goto BgL_zc3z04anonymousza31333ze3z87_1441;
															}
														}
													else
														{	/* Pp/circle.scm 149 */
															((bool_t) 1);
														}
												}
												CELL_SET(BgL_cachez00_2459, BgL_hz00_1438);
												{	/* Pp/circle.scm 153 */
													obj_t BgL_auxz00_2461;

													BgL_auxz00_2461 = BINT(-1L);
													CELL_SET(BgL_cachezd2countzd2_2460, BgL_auxz00_2461);
												}
												BGl_hashtablezd2putz12zc0zz__hashz00(CELL_REF
													(BgL_cachez00_2459), BgL_objz00_1424,
													BgL_entryz00_1435);
											}
										else
											{	/* Pp/circle.scm 145 */
												{	/* Pp/circle.scm 156 */
													obj_t BgL_auxz00_2438;

													BgL_auxz00_2438 =
														MAKE_YOUNG_PAIR(BgL_entryz00_1435,
														CELL_REF(BgL_cachez00_2459));
													CELL_SET(BgL_cachez00_2459, BgL_auxz00_2438);
												}
												{	/* Pp/circle.scm 157 */
													obj_t BgL_auxz00_2462;

													BgL_auxz00_2462 =
														ADDFX(
														((obj_t)
															((obj_t) CELL_REF(BgL_cachezd2countzd2_2460))),
														BINT(1L));
													CELL_SET(BgL_cachezd2countzd2_2460, BgL_auxz00_2462);
												}
											}
									}
								if (PAIRP(BgL_objz00_1424))
									{	/* Pp/circle.scm 159 */
										{	/* Pp/circle.scm 160 */
											obj_t BgL_arg1338z00_1456;

											BgL_arg1338z00_1456 = CAR(BgL_objz00_1424);
											BGl_registerze70ze7zz__pp_circlez00
												(BgL_cachezd2countzd2_2460, BgL_cachez00_2459,
												BgL_arg1338z00_1456);
										}
										{	/* Pp/circle.scm 161 */
											obj_t BgL_arg1339z00_1457;

											BgL_arg1339z00_1457 = CDR(BgL_objz00_1424);
											{
												obj_t BgL_objz00_2638;

												BgL_objz00_2638 = BgL_arg1339z00_1457;
												BgL_objz00_1424 = BgL_objz00_2638;
												goto BGl_registerze70ze7zz__pp_circlez00;
											}
										}
									}
								else
									{	/* Pp/circle.scm 159 */
										if (VECTORP(BgL_objz00_1424))
											{
												long BgL_iz00_2063;

												{	/* Pp/circle.scm 163 */
													bool_t BgL_tmpz00_2641;

													BgL_iz00_2063 = 0L;
												BgL_for1043z00_2062:
													if ((BgL_iz00_2063 < VECTOR_LENGTH(BgL_objz00_1424)))
														{	/* Pp/circle.scm 163 */
															{	/* Pp/circle.scm 164 */
																obj_t BgL_arg1343z00_2067;

																BgL_arg1343z00_2067 =
																	VECTOR_REF(
																	((obj_t) BgL_objz00_1424), BgL_iz00_2063);
																BGl_registerze70ze7zz__pp_circlez00
																	(BgL_cachezd2countzd2_2460, BgL_cachez00_2459,
																	BgL_arg1343z00_2067);
															}
															{
																long BgL_iz00_2648;

																BgL_iz00_2648 = (BgL_iz00_2063 + 1L);
																BgL_iz00_2063 = BgL_iz00_2648;
																goto BgL_for1043z00_2062;
															}
														}
													else
														{	/* Pp/circle.scm 163 */
															BgL_tmpz00_2641 = ((bool_t) 0);
														}
													return BBOOL(BgL_tmpz00_2641);
												}
											}
										else
											{	/* Pp/circle.scm 162 */
												if (STRUCTP(BgL_objz00_1424))
													{	/* Pp/circle.scm 166 */
														int BgL_stop1046z00_1468;

														BgL_stop1046z00_1468 =
															STRUCT_LENGTH(BgL_objz00_1424);
														{
															long BgL_iz00_2079;

															{	/* Pp/circle.scm 166 */
																bool_t BgL_tmpz00_2654;

																BgL_iz00_2079 = 0L;
															BgL_for1045z00_2078:
																if (
																	(BgL_iz00_2079 <
																		(long) (BgL_stop1046z00_1468)))
																	{	/* Pp/circle.scm 166 */
																		{	/* Pp/circle.scm 167 */
																			obj_t BgL_arg1348z00_2083;

																			BgL_arg1348z00_2083 =
																				STRUCT_REF(
																				((obj_t) BgL_objz00_1424),
																				(int) (BgL_iz00_2079));
																			BGl_registerze70ze7zz__pp_circlez00
																				(BgL_cachezd2countzd2_2460,
																				BgL_cachez00_2459, BgL_arg1348z00_2083);
																		}
																		{
																			long BgL_iz00_2662;

																			BgL_iz00_2662 = (BgL_iz00_2079 + 1L);
																			BgL_iz00_2079 = BgL_iz00_2662;
																			goto BgL_for1045z00_2078;
																		}
																	}
																else
																	{	/* Pp/circle.scm 166 */
																		BgL_tmpz00_2654 = ((bool_t) 0);
																	}
																return BBOOL(BgL_tmpz00_2654);
															}
														}
													}
												else
													{	/* Pp/circle.scm 165 */
														if (BGL_OBJECTP(BgL_objz00_1424))
															{	/* Pp/circle.scm 169 */
																obj_t BgL_klassz00_1477;

																{	/* Pp/circle.scm 169 */
																	obj_t BgL_arg1789z00_2089;
																	long BgL_arg1790z00_2090;

																	BgL_arg1789z00_2089 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Pp/circle.scm 169 */
																		long BgL_arg1791z00_2091;

																		BgL_arg1791z00_2091 =
																			BGL_OBJECT_CLASS_NUM(
																			((BgL_objectz00_bglt) BgL_objz00_1424));
																		BgL_arg1790z00_2090 =
																			(BgL_arg1791z00_2091 - OBJECT_TYPE);
																	}
																	BgL_klassz00_1477 =
																		VECTOR_REF(BgL_arg1789z00_2089,
																		BgL_arg1790z00_2090);
																}
																{	/* Pp/circle.scm 169 */
																	obj_t BgL_fieldsz00_1478;

																	{	/* Pp/circle.scm 170 */
																		obj_t BgL_tmpz00_2672;

																		BgL_tmpz00_2672 =
																			((obj_t) BgL_klassz00_1477);
																		BgL_fieldsz00_1478 =
																			BGL_CLASS_ALL_FIELDS(BgL_tmpz00_2672);
																	}
																	{	/* Pp/circle.scm 170 */

																		{
																			long BgL_iz00_2105;

																			{	/* Pp/circle.scm 171 */
																				bool_t BgL_tmpz00_2675;

																				BgL_iz00_2105 = 0L;
																			BgL_for1047z00_2104:
																				if (
																					(BgL_iz00_2105 <
																						VECTOR_LENGTH(BgL_fieldsz00_1478)))
																					{	/* Pp/circle.scm 171 */
																						{	/* Pp/circle.scm 172 */
																							obj_t BgL_fz00_2109;

																							BgL_fz00_2109 =
																								VECTOR_REF(BgL_fieldsz00_1478,
																								BgL_iz00_2105);
																							{	/* Pp/circle.scm 172 */
																								obj_t BgL_gvz00_2112;

																								BgL_gvz00_2112 =
																									BGl_classzd2fieldzd2accessorz00zz__objectz00
																									(BgL_fz00_2109);
																								{	/* Pp/circle.scm 173 */

																									{	/* Pp/circle.scm 174 */
																										obj_t BgL_arg1354z00_2113;

																										BgL_arg1354z00_2113 =
																											BGL_PROCEDURE_CALL1
																											(BgL_gvz00_2112,
																											BgL_objz00_1424);
																										BGl_registerze70ze7zz__pp_circlez00
																											(BgL_cachezd2countzd2_2460,
																											BgL_cachez00_2459,
																											BgL_arg1354z00_2113);
																									}
																								}
																							}
																						}
																						{
																							long BgL_iz00_2686;

																							BgL_iz00_2686 =
																								(BgL_iz00_2105 + 1L);
																							BgL_iz00_2105 = BgL_iz00_2686;
																							goto BgL_for1047z00_2104;
																						}
																					}
																				else
																					{	/* Pp/circle.scm 171 */
																						BgL_tmpz00_2675 = ((bool_t) 0);
																					}
																				return BBOOL(BgL_tmpz00_2675);
																			}
																		}
																	}
																}
															}
														else
															{	/* Pp/circle.scm 168 */
																if (CELLP(BgL_objz00_1424))
																	{	/* Pp/circle.scm 176 */
																		obj_t BgL_arg1358z00_1490;

																		BgL_arg1358z00_1490 =
																			CELL_REF(BgL_objz00_1424);
																		{
																			obj_t BgL_objz00_2692;

																			BgL_objz00_2692 = BgL_arg1358z00_1490;
																			BgL_objz00_1424 = BgL_objz00_2692;
																			goto BGl_registerze70ze7zz__pp_circlez00;
																		}
																	}
																else
																	{	/* Pp/circle.scm 175 */
																		return BFALSE;
																	}
															}
													}
											}
									}
							}
					}
			}
		}

	}



/* &output-component */
	obj_t BGl_z62outputzd2componentzb0zz__pp_circlez00(obj_t
		BgL_nextzd2cardinalzd2_2420, bool_t BgL_flagz00_2419,
		obj_t BgL_portz00_2418, obj_t BgL_cachez00_2417, obj_t BgL_objz00_1342)
	{
		{	/* Pp/circle.scm 191 */
			{	/* Pp/circle.scm 192 */
				obj_t BgL_g1049z00_1344;

				if (BGl_hashtablezf3zf3zz__hashz00(CELL_REF(BgL_cachez00_2417)))
					{	/* Pp/circle.scm 123 */
						BgL_g1049z00_1344 =
							BGl_hashtablezd2getzd2zz__hashz00(CELL_REF(BgL_cachez00_2417),
							BgL_objz00_1342);
					}
				else
					{	/* Pp/circle.scm 123 */
						BgL_g1049z00_1344 =
							BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_objz00_1342,
							CELL_REF(BgL_cachez00_2417));
					}
				return
					BGl_loopzd2matchedze70z35zz__pp_circlez00(BgL_nextzd2cardinalzd2_2420,
					BgL_cachez00_2417, BgL_flagz00_2419, BgL_portz00_2418,
					BgL_objz00_1342, BgL_g1049z00_1344);
			}
		}

	}



/* loop-matched~0 */
	obj_t BGl_loopzd2matchedze70z35zz__pp_circlez00(obj_t
		BgL_nextzd2cardinalzd2_2458, obj_t BgL_cachez00_2457,
		bool_t BgL_flagz00_2456, obj_t BgL_portz00_2455, obj_t BgL_objz00_1346,
		obj_t BgL_matchz00_1347)
	{
		{	/* Pp/circle.scm 192 */
		BGl_loopzd2matchedze70z35zz__pp_circlez00:
			if (CBOOL(BgL_matchz00_1347))
				{	/* Pp/circle.scm 195 */
					obj_t BgL_valuez00_1349;

					BgL_valuez00_1349 = CDR(((obj_t) BgL_matchz00_1347));
					if (INTEGERP(BgL_valuez00_1349))
						{	/* Pp/circle.scm 197 */
							bgl_display_char(((unsigned char) '#'), BgL_portz00_2455);
							BGl_z62outputzd2componentzb0zz__pp_circlez00
								(BgL_nextzd2cardinalzd2_2458, BgL_flagz00_2456,
								BgL_portz00_2455, BgL_cachez00_2457, BgL_valuez00_1349);
							return bgl_display_char(((unsigned char) '#'), BgL_portz00_2455);
						}
					else
						{	/* Pp/circle.scm 197 */
							if (CBOOL(BgL_valuez00_1349))
								{	/* Pp/circle.scm 204 */
									obj_t BgL_serialz00_1351;

									BgL_serialz00_1351 =
										BGl_z62zc3z04anonymousza31362ze3ze5zz__pp_circlez00
										(BgL_nextzd2cardinalzd2_2458);
									{	/* Pp/circle.scm 205 */
										obj_t BgL_tmpz00_2712;

										BgL_tmpz00_2712 = ((obj_t) BgL_matchz00_1347);
										SET_CDR(BgL_tmpz00_2712, BgL_serialz00_1351);
									}
									bgl_display_char(((unsigned char) '#'), BgL_portz00_2455);
									BGl_z62outputzd2componentzb0zz__pp_circlez00
										(BgL_nextzd2cardinalzd2_2458, BgL_flagz00_2456,
										BgL_portz00_2455, BgL_cachez00_2457, BgL_serialz00_1351);
									bgl_display_char(((unsigned char) '='), BgL_portz00_2455);
									{
										obj_t BgL_matchz00_2718;

										BgL_matchz00_2718 = BFALSE;
										BgL_matchz00_1347 = BgL_matchz00_2718;
										goto BGl_loopzd2matchedze70z35zz__pp_circlez00;
									}
								}
							else
								{
									obj_t BgL_matchz00_2719;

									BgL_matchz00_2719 = BFALSE;
									BgL_matchz00_1347 = BgL_matchz00_2719;
									goto BGl_loopzd2matchedze70z35zz__pp_circlez00;
								}
						}
				}
			else
				{	/* Pp/circle.scm 194 */
					if (INTEGERP(BgL_objz00_1346))
						{	/* Pp/circle.scm 216 */
							return bgl_display_fixnum(BgL_objz00_1346, BgL_portz00_2455);
						}
					else
						{	/* Pp/circle.scm 216 */
							if (CHARP(BgL_objz00_1346))
								{	/* Pp/circle.scm 219 */
									if (BgL_flagz00_2456)
										{	/* Pp/circle.scm 221 */
											unsigned char BgL_tmpz00_2726;

											BgL_tmpz00_2726 = CCHAR(BgL_objz00_1346);
											return
												bgl_display_char(BgL_tmpz00_2726, BgL_portz00_2455);
										}
									else
										{	/* Pp/circle.scm 220 */
											return bgl_write_char(BgL_objz00_1346, BgL_portz00_2455);
										}
								}
							else
								{	/* Pp/circle.scm 219 */
									if (SYMBOLP(BgL_objz00_1346))
										{	/* Pp/circle.scm 224 */
											if (BgL_flagz00_2456)
												{	/* Pp/circle.scm 225 */
													return
														BGl_displayzd2symbolzd2zz__r4_output_6_10_3z00
														(BgL_objz00_1346, BgL_portz00_2455);
												}
											else
												{	/* Pp/circle.scm 225 */
													return
														BGl_writezd2symbolzd2zz__r4_output_6_10_3z00
														(BgL_objz00_1346, BgL_portz00_2455);
												}
										}
									else
										{	/* Pp/circle.scm 224 */
											if (STRINGP(BgL_objz00_1346))
												{	/* Pp/circle.scm 229 */
													if (BgL_flagz00_2456)
														{	/* Pp/circle.scm 230 */
															return
																bgl_display_string(BgL_objz00_1346,
																BgL_portz00_2455);
														}
													else
														{	/* Pp/circle.scm 230 */
															return
																BGl_writezd2stringzd2zz__r4_output_6_10_3z00
																(string_for_read(BgL_objz00_1346),
																BgL_portz00_2455);
														}
												}
											else
												{	/* Pp/circle.scm 229 */
													if (PAIRP(BgL_objz00_1346))
														{	/* Pp/circle.scm 234 */
															bgl_display_char(((unsigned char) '('),
																BgL_portz00_2455);
															{
																obj_t BgL_lz00_1359;

																BgL_lz00_1359 = BgL_objz00_1346;
															BgL_zc3z04anonymousza31236ze3z87_1360:
																{	/* Pp/circle.scm 237 */
																	obj_t BgL_arg1238z00_1361;

																	BgL_arg1238z00_1361 =
																		CAR(((obj_t) BgL_lz00_1359));
																	BGl_z62outputzd2componentzb0zz__pp_circlez00
																		(BgL_nextzd2cardinalzd2_2458,
																		BgL_flagz00_2456, BgL_portz00_2455,
																		BgL_cachez00_2457, BgL_arg1238z00_1361);
																}
																{	/* Pp/circle.scm 238 */
																	obj_t BgL_restz00_1362;

																	BgL_restz00_1362 =
																		CDR(((obj_t) BgL_lz00_1359));
																	if (NULLP(BgL_restz00_1362))
																		{	/* Pp/circle.scm 239 */
																			return
																				bgl_display_char(((unsigned char) ')'),
																				BgL_portz00_2455);
																		}
																	else
																		{	/* Pp/circle.scm 241 */
																			obj_t BgL_matchz00_1364;

																			if (BGl_hashtablezf3zf3zz__hashz00
																				(CELL_REF(BgL_cachez00_2457)))
																				{	/* Pp/circle.scm 123 */
																					BgL_matchz00_1364 =
																						BGl_hashtablezd2getzd2zz__hashz00
																						(CELL_REF(BgL_cachez00_2457),
																						BgL_restz00_1362);
																				}
																			else
																				{	/* Pp/circle.scm 123 */
																					BgL_matchz00_1364 =
																						BGl_assqz00zz__r4_pairs_and_lists_6_3z00
																						(BgL_restz00_1362,
																						CELL_REF(BgL_cachez00_2457));
																				}
																			{	/* Pp/circle.scm 242 */
																				bool_t BgL_test1911z00_2756;

																				if (PAIRP(BgL_restz00_1362))
																					{	/* Pp/circle.scm 242 */
																						if (CBOOL(BgL_matchz00_1364))
																							{	/* Pp/circle.scm 243 */
																								BgL_test1911z00_2756 =
																									CBOOL(CDR(
																										((obj_t)
																											BgL_matchz00_1364)));
																							}
																						else
																							{	/* Pp/circle.scm 243 */
																								BgL_test1911z00_2756 =
																									((bool_t) 0);
																							}
																					}
																				else
																					{	/* Pp/circle.scm 242 */
																						BgL_test1911z00_2756 = ((bool_t) 1);
																					}
																				if (BgL_test1911z00_2756)
																					{	/* Pp/circle.scm 242 */
																						bgl_display_string
																							(BGl_string1837z00zz__pp_circlez00,
																							BgL_portz00_2455);
																						BGl_loopzd2matchedze70z35zz__pp_circlez00
																							(BgL_nextzd2cardinalzd2_2458,
																							BgL_cachez00_2457,
																							BgL_flagz00_2456,
																							BgL_portz00_2455,
																							BgL_restz00_1362,
																							BgL_matchz00_1364);
																						return bgl_display_char(((unsigned
																									char) ')'), BgL_portz00_2455);
																					}
																				else
																					{	/* Pp/circle.scm 242 */
																						bgl_display_char(((unsigned char)
																								' '), BgL_portz00_2455);
																						{
																							obj_t BgL_lz00_2768;

																							BgL_lz00_2768 = BgL_restz00_1362;
																							BgL_lz00_1359 = BgL_lz00_2768;
																							goto
																								BgL_zc3z04anonymousza31236ze3z87_1360;
																						}
																					}
																			}
																		}
																}
															}
														}
													else
														{	/* Pp/circle.scm 234 */
															if (BGl_classzf3zf3zz__objectz00(BgL_objz00_1346))
																{	/* Pp/circle.scm 252 */
																	return
																		bgl_display_obj(BgL_objz00_1346,
																		BgL_portz00_2455);
																}
															else
																{	/* Pp/circle.scm 252 */
																	if (VECTORP(BgL_objz00_1346))
																		{	/* Pp/circle.scm 255 */
																			bgl_display_char(((unsigned char) '#'),
																				BgL_portz00_2455);
																			{	/* Pp/circle.scm 257 */
																				int BgL_tagz00_1373;

																				BgL_tagz00_1373 =
																					VECTOR_TAG(BgL_objz00_1346);
																				if (((long) (BgL_tagz00_1373) > 0L))
																					{	/* Pp/circle.scm 258 */
																						if (
																							((long) (BgL_tagz00_1373) < 100L))
																							{	/* Pp/circle.scm 259 */
																								if (
																									((long) (BgL_tagz00_1373) >
																										10L))
																									{	/* Pp/circle.scm 261 */
																										bgl_display_char(((unsigned
																													char) '0'),
																											BgL_portz00_2455);
																									}
																								else
																									{	/* Pp/circle.scm 261 */
																										BFALSE;
																									}
																								bgl_display_char(((unsigned
																											char) '0'),
																									BgL_portz00_2455);
																							}
																						else
																							{	/* Pp/circle.scm 263 */
																								obj_t BgL_list1247z00_1377;

																								BgL_list1247z00_1377 =
																									MAKE_YOUNG_PAIR
																									(BgL_portz00_2455, BNIL);
																								BGl_writez00zz__r4_output_6_10_3z00
																									(BINT(BgL_tagz00_1373),
																									BgL_list1247z00_1377);
																							}
																					}
																				else
																					{	/* Pp/circle.scm 258 */
																						BFALSE;
																					}
																			}
																			bgl_display_char(((unsigned char) '('),
																				BgL_portz00_2455);
																			{
																				long BgL_iz00_1380;

																				BgL_iz00_1380 = 0L;
																			BgL_zc3z04anonymousza31248ze3z87_1381:
																				if (
																					(BgL_iz00_1380 ==
																						VECTOR_LENGTH(BgL_objz00_1346)))
																					{	/* Pp/circle.scm 267 */
																						((bool_t) 0);
																					}
																				else
																					{	/* Pp/circle.scm 267 */
																						{	/* Pp/circle.scm 269 */
																							obj_t BgL_arg1252z00_1383;

																							BgL_arg1252z00_1383 =
																								VECTOR_REF(
																								((obj_t) BgL_objz00_1346),
																								BgL_iz00_1380);
																							BGl_z62outputzd2componentzb0zz__pp_circlez00
																								(BgL_nextzd2cardinalzd2_2458,
																								BgL_flagz00_2456,
																								BgL_portz00_2455,
																								BgL_cachez00_2457,
																								BgL_arg1252z00_1383);
																						}
																						{	/* Pp/circle.scm 270 */
																							long BgL_nextz00_1384;

																							BgL_nextz00_1384 =
																								(1L + BgL_iz00_1380);
																							if (
																								(BgL_nextz00_1384 ==
																									VECTOR_LENGTH
																									(BgL_objz00_1346)))
																								{	/* Pp/circle.scm 271 */
																									BFALSE;
																								}
																							else
																								{	/* Pp/circle.scm 271 */
																									bgl_display_char(((unsigned
																												char) ' '),
																										BgL_portz00_2455);
																								}
																							{
																								long BgL_iz00_2802;

																								BgL_iz00_2802 =
																									(1L + BgL_iz00_1380);
																								BgL_iz00_1380 = BgL_iz00_2802;
																								goto
																									BgL_zc3z04anonymousza31248ze3z87_1381;
																							}
																						}
																					}
																			}
																			return
																				bgl_display_char(((unsigned char) ')'),
																				BgL_portz00_2455);
																		}
																	else
																		{	/* Pp/circle.scm 255 */
																			if (STRUCTP(BgL_objz00_1346))
																				{	/* Pp/circle.scm 276 */
																					bgl_display_string
																						(BGl_string1838z00zz__pp_circlez00,
																						BgL_portz00_2455);
																					{	/* Pp/circle.scm 278 */
																						obj_t BgL_arg1272z00_1389;

																						BgL_arg1272z00_1389 =
																							STRUCT_KEY(BgL_objz00_1346);
																						{	/* Pp/circle.scm 278 */
																							obj_t BgL_list1273z00_1390;

																							BgL_list1273z00_1390 =
																								MAKE_YOUNG_PAIR
																								(BgL_portz00_2455, BNIL);
																							BGl_writez00zz__r4_output_6_10_3z00
																								(BgL_arg1272z00_1389,
																								BgL_list1273z00_1390);
																						}
																					}
																					bgl_display_char(((unsigned char)
																							' '), BgL_portz00_2455);
																					{	/* Pp/circle.scm 280 */
																						int BgL_lenz00_1391;

																						BgL_lenz00_1391 =
																							STRUCT_LENGTH(BgL_objz00_1346);
																						{
																							long BgL_iz00_1393;

																							BgL_iz00_1393 = 0L;
																						BgL_zc3z04anonymousza31274ze3z87_1394:
																							if (
																								(BgL_iz00_1393 ==
																									(long) (BgL_lenz00_1391)))
																								{	/* Pp/circle.scm 282 */
																									((bool_t) 0);
																								}
																							else
																								{	/* Pp/circle.scm 282 */
																									{	/* Pp/circle.scm 284 */
																										obj_t BgL_arg1284z00_1396;

																										BgL_arg1284z00_1396 =
																											STRUCT_REF(
																											((obj_t) BgL_objz00_1346),
																											(int) (BgL_iz00_1393));
																										BGl_z62outputzd2componentzb0zz__pp_circlez00
																											(BgL_nextzd2cardinalzd2_2458,
																											BgL_flagz00_2456,
																											BgL_portz00_2455,
																											BgL_cachez00_2457,
																											BgL_arg1284z00_1396);
																									}
																									{	/* Pp/circle.scm 285 */
																										long BgL_nextz00_1397;

																										BgL_nextz00_1397 =
																											(1L + BgL_iz00_1393);
																										if (
																											(BgL_nextz00_1397 ==
																												(long)
																												(BgL_lenz00_1391)))
																											{	/* Pp/circle.scm 286 */
																												BFALSE;
																											}
																										else
																											{	/* Pp/circle.scm 286 */
																												bgl_display_char((
																														(unsigned char)
																														' '),
																													BgL_portz00_2455);
																											}
																										{
																											long BgL_iz00_2825;

																											BgL_iz00_2825 =
																												(1L + BgL_iz00_1393);
																											BgL_iz00_1393 =
																												BgL_iz00_2825;
																											goto
																												BgL_zc3z04anonymousza31274ze3z87_1394;
																										}
																									}
																								}
																						}
																					}
																					return
																						bgl_display_char(((unsigned char)
																							'}'), BgL_portz00_2455);
																				}
																			else
																				{	/* Pp/circle.scm 276 */
																					if (CELLP(BgL_objz00_1346))
																						{	/* Pp/circle.scm 291 */
																							bgl_display_string
																								(BGl_string1839z00zz__pp_circlez00,
																								BgL_portz00_2455);
																							{	/* Pp/circle.scm 293 */
																								obj_t BgL_arg1306z00_1402;

																								BgL_arg1306z00_1402 =
																									CELL_REF(BgL_objz00_1346);
																								BGl_z62outputzd2componentzb0zz__pp_circlez00
																									(BgL_nextzd2cardinalzd2_2458,
																									BgL_flagz00_2456,
																									BgL_portz00_2455,
																									BgL_cachez00_2457,
																									BgL_arg1306z00_1402);
																							}
																							return
																								bgl_display_string
																								(BGl_string1840z00zz__pp_circlez00,
																								BgL_portz00_2455);
																						}
																					else
																						{	/* Pp/circle.scm 291 */
																							if (BGL_OBJECTP(BgL_objz00_1346))
																								{	/* Pp/circle.scm 298 */
																									obj_t
																										BgL_zc3z04anonymousza31309ze3z87_2414;
																									BgL_zc3z04anonymousza31309ze3z87_2414
																										=
																										MAKE_VA_PROCEDURE
																										(BGl_z62zc3z04anonymousza31309ze3ze5zz__pp_circlez00,
																										(int) (-2L), (int) (4L));
																									PROCEDURE_SET
																										(BgL_zc3z04anonymousza31309ze3z87_2414,
																										(int) (0L),
																										((obj_t)
																											BgL_cachez00_2457));
																									PROCEDURE_SET
																										(BgL_zc3z04anonymousza31309ze3z87_2414,
																										(int) (1L),
																										BgL_portz00_2455);
																									PROCEDURE_SET
																										(BgL_zc3z04anonymousza31309ze3z87_2414,
																										(int) (2L),
																										BBOOL(BgL_flagz00_2456));
																									PROCEDURE_SET
																										(BgL_zc3z04anonymousza31309ze3z87_2414,
																										(int) (3L),
																										BgL_nextzd2cardinalzd2_2458);
																									return
																										BGl_objectzd2printzd2zz__objectz00
																										(((BgL_objectz00_bglt)
																											BgL_objz00_1346),
																										BgL_portz00_2455,
																										BgL_zc3z04anonymousza31309ze3z87_2414);
																								}
																							else
																								{	/* Pp/circle.scm 296 */
																									if (UCS2_STRINGP
																										(BgL_objz00_1346))
																										{	/* Pp/circle.scm 300 */
																											if (BgL_flagz00_2456)
																												{	/* Pp/circle.scm 301 */
																													return
																														BGl_displayzd2ucs2stringzd2zz__r4_output_6_10_3z00
																														(BgL_objz00_1346,
																														BgL_portz00_2455);
																												}
																											else
																												{	/* Pp/circle.scm 301 */
																													return
																														BGl_writezd2ucs2stringzd2zz__r4_output_6_10_3z00
																														(BgL_objz00_1346,
																														BgL_portz00_2455);
																												}
																										}
																									else
																										{	/* Pp/circle.scm 300 */
																											if (UCS2P
																												(BgL_objz00_1346))
																												{	/* Pp/circle.scm 305 */
																													if (BgL_flagz00_2456)
																														{	/* Pp/circle.scm 306 */
																															return
																																bgl_display_ucs2
																																(BgL_objz00_1346,
																																BgL_portz00_2455);
																														}
																													else
																														{	/* Pp/circle.scm 306 */
																															return
																																bgl_write_ucs2
																																(BgL_objz00_1346,
																																BgL_portz00_2455);
																														}
																												}
																											else
																												{	/* Pp/circle.scm 305 */
																													if (REALP
																														(BgL_objz00_1346))
																														{	/* Pp/circle.scm 310 */
																															return
																																BGl_displayzd2flonumzd2zz__r4_output_6_10_3z00
																																(DOUBLE_TO_REAL
																																(REAL_TO_DOUBLE
																																	(BgL_objz00_1346)),
																																BgL_portz00_2455);
																														}
																													else
																														{	/* Pp/circle.scm 310 */
																															if (BGL_DATEP
																																(BgL_objz00_1346))
																																{	/* Pp/circle.scm 313 */
																																	if (BgL_flagz00_2456)
																																		{	/* Pp/circle.scm 314 */
																																			return
																																				bgl_display_obj
																																				(BgL_objz00_1346,
																																				BgL_portz00_2455);
																																		}
																																	else
																																		{	/* Pp/circle.scm 316 */
																																			obj_t
																																				BgL_list1314z00_1413;
																																			BgL_list1314z00_1413
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_portz00_2455,
																																				BNIL);
																																			return
																																				BGl_writez00zz__r4_output_6_10_3z00
																																				(BgL_objz00_1346,
																																				BgL_list1314z00_1413);
																																		}
																																}
																															else
																																{	/* Pp/circle.scm 313 */
																																	if (BGL_MUTEXP
																																		(BgL_objz00_1346))
																																		{	/* Pp/circle.scm 318 */
																																			bgl_display_string
																																				(BGl_string1841z00zz__pp_circlez00,
																																				BgL_portz00_2455);
																																			{	/* Pp/circle.scm 320 */
																																				obj_t
																																					BgL_arg1316z00_1415;
																																				{	/* Pp/circle.scm 320 */
																																					obj_t
																																						BgL_tmpz00_2875;
																																					BgL_tmpz00_2875
																																						=
																																						(
																																						(obj_t)
																																						BgL_objz00_1346);
																																					BgL_arg1316z00_1415
																																						=
																																						BGL_MUTEX_NAME
																																						(BgL_tmpz00_2875);
																																				}
																																				bgl_display_obj
																																					(BgL_arg1316z00_1415,
																																					BgL_portz00_2455);
																																			}
																																			return
																																				bgl_display_string
																																				(BGl_string1840z00zz__pp_circlez00,
																																				BgL_portz00_2455);
																																		}
																																	else
																																		{	/* Pp/circle.scm 318 */
																																			if (BGL_CONDVARP(BgL_objz00_1346))
																																				{	/* Pp/circle.scm 323 */
																																					bgl_display_string
																																						(BGl_string1842z00zz__pp_circlez00,
																																						BgL_portz00_2455);
																																					{	/* Pp/circle.scm 325 */
																																						obj_t
																																							BgL_arg1318z00_1417;
																																						{	/* Pp/circle.scm 325 */
																																							obj_t
																																								BgL_tmpz00_2883;
																																							BgL_tmpz00_2883
																																								=
																																								(
																																								(obj_t)
																																								BgL_objz00_1346);
																																							BgL_arg1318z00_1417
																																								=
																																								BGL_CONDVAR_NAME
																																								(BgL_tmpz00_2883);
																																						}
																																						bgl_display_obj
																																							(BgL_arg1318z00_1417,
																																							BgL_portz00_2455);
																																					}
																																					return
																																						bgl_display_string
																																						(BGl_string1840z00zz__pp_circlez00,
																																						BgL_portz00_2455);
																																				}
																																			else
																																				{	/* Pp/circle.scm 329 */
																																					obj_t
																																						BgL_list1319z00_1418;
																																					BgL_list1319z00_1418
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_portz00_2455,
																																						BNIL);
																																					return
																																						BGl_writez00zz__r4_output_6_10_3z00
																																						(BgL_objz00_1346,
																																						BgL_list1319z00_1418);
																																				}
																																		}
																																}
																														}
																												}
																										}
																								}
																						}
																				}
																		}
																}
														}
												}
										}
								}
						}
				}
		}

	}



/* &<@anonymous:1309> */
	obj_t BGl_z62zc3z04anonymousza31309ze3ze5zz__pp_circlez00(obj_t
		BgL_envz00_2421, obj_t BgL_xz00_2426, obj_t BgL_pz00_2427)
	{
		{	/* Pp/circle.scm 297 */
			{	/* Pp/circle.scm 298 */
				obj_t BgL_cachez00_2422;
				obj_t BgL_portz00_2423;
				bool_t BgL_flagz00_2424;
				obj_t BgL_nextzd2cardinalzd2_2425;

				BgL_cachez00_2422 = PROCEDURE_REF(BgL_envz00_2421, (int) (0L));
				BgL_portz00_2423 = ((obj_t) PROCEDURE_REF(BgL_envz00_2421, (int) (1L)));
				BgL_flagz00_2424 = CBOOL(PROCEDURE_REF(BgL_envz00_2421, (int) (2L)));
				BgL_nextzd2cardinalzd2_2425 =
					((obj_t) PROCEDURE_REF(BgL_envz00_2421, (int) (3L)));
				return
					BGl_z62outputzd2componentzb0zz__pp_circlez00
					(BgL_nextzd2cardinalzd2_2425, BgL_flagz00_2424, BgL_portz00_2423,
					BgL_cachez00_2422, BgL_xz00_2426);
			}
		}

	}



/* &<@anonymous:1362> */
	obj_t BGl_z62zc3z04anonymousza31362ze3ze5zz__pp_circlez00(obj_t
		BgL_envz00_2428)
	{
		{	/* Pp/circle.scm 117 */
			{	/* Pp/circle.scm 118 */
				obj_t BgL_serialz00_2429;

				BgL_serialz00_2429 = PROCEDURE_EL_REF(BgL_envz00_2428, (int) (0L));
				{	/* Pp/circle.scm 118 */
					obj_t BgL_auxz00_2483;

					BgL_auxz00_2483 = ADDFX(BINT(1L), CELL_REF(BgL_serialz00_2429));
					CELL_SET(BgL_serialz00_2429, BgL_auxz00_2483);
				}
				return CELL_REF(BgL_serialz00_2429);
			}
		}

	}



/* c-debugging-print */
	BGL_EXPORTED_DEF obj_t dprint(obj_t BgL_objz00_14)
	{
		{	/* Pp/circle.scm 335 */
			{	/* Pp/circle.scm 336 */
				obj_t BgL_portz00_2170;

				{	/* Pp/circle.scm 336 */
					obj_t BgL_tmpz00_2906;

					BgL_tmpz00_2906 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_portz00_2170 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_2906);
				}
				BGl_circlezd2writezf2displayz20zz__pp_circlez00(BgL_objz00_14,
					BgL_portz00_2170, ((bool_t) 0));
				bgl_display_char(((unsigned char) 10), BgL_portz00_2170);
				return BgL_objz00_14;
			}
		}

	}



/* &c-debugging-print */
	obj_t BGl_z62czd2debuggingzd2printz62zz__pp_circlez00(obj_t BgL_envz00_2440,
		obj_t BgL_objz00_2441)
	{
		{	/* Pp/circle.scm 335 */
			return dprint(BgL_objz00_2441);
		}

	}



/* jvm-debugging-print */
	BGL_EXPORTED_DEF char *BGl_jvmzd2debuggingzd2printz00zz__pp_circlez00(obj_t
		BgL_objz00_15, int BgL_printerzd2numzd2_16)
	{
		{	/* Pp/circle.scm 344 */
			{	/* Pp/circle.scm 346 */
				obj_t BgL_zc3z04anonymousza31364ze3z87_2442;

				BgL_zc3z04anonymousza31364ze3z87_2442 =
					MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31364ze3ze5zz__pp_circlez00,
					(int) (0L), (int) (2L));
				PROCEDURE_SET(BgL_zc3z04anonymousza31364ze3z87_2442,
					(int) (0L), BINT(BgL_printerzd2numzd2_16));
				PROCEDURE_SET(BgL_zc3z04anonymousza31364ze3z87_2442,
					(int) (1L), BgL_objz00_15);
				return
					BSTRING_TO_STRING
					(BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00
					(BgL_zc3z04anonymousza31364ze3z87_2442));
			}
		}

	}



/* &jvm-debugging-print */
	obj_t BGl_z62jvmzd2debuggingzd2printz62zz__pp_circlez00(obj_t BgL_envz00_2449,
		obj_t BgL_objz00_2450, obj_t BgL_printerzd2numzd2_2451)
	{
		{	/* Pp/circle.scm 344 */
			{	/* Pp/circle.scm 346 */
				char *BgL_tmpz00_2922;

				{	/* Pp/circle.scm 346 */
					int BgL_auxz00_2923;

					{	/* Pp/circle.scm 346 */
						obj_t BgL_tmpz00_2924;

						if (INTEGERP(BgL_printerzd2numzd2_2451))
							{	/* Pp/circle.scm 346 */
								BgL_tmpz00_2924 = BgL_printerzd2numzd2_2451;
							}
						else
							{
								obj_t BgL_auxz00_2927;

								BgL_auxz00_2927 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1829z00zz__pp_circlez00, BINT(11316L),
									BGl_string1843z00zz__pp_circlez00,
									BGl_string1844z00zz__pp_circlez00, BgL_printerzd2numzd2_2451);
								FAILURE(BgL_auxz00_2927, BFALSE, BFALSE);
							}
						BgL_auxz00_2923 = CINT(BgL_tmpz00_2924);
					}
					BgL_tmpz00_2922 =
						BGl_jvmzd2debuggingzd2printz00zz__pp_circlez00(BgL_objz00_2450,
						BgL_auxz00_2923);
				}
				return string_to_bstring(BgL_tmpz00_2922);
			}
		}

	}



/* &<@anonymous:1364> */
	obj_t BGl_z62zc3z04anonymousza31364ze3ze5zz__pp_circlez00(obj_t
		BgL_envz00_2452)
	{
		{	/* Pp/circle.scm 346 */
			{	/* Pp/circle.scm 346 */
				int BgL_printerzd2numzd2_2453;
				obj_t BgL_objz00_2454;

				BgL_printerzd2numzd2_2453 =
					CINT(PROCEDURE_REF(BgL_envz00_2452, (int) (0L)));
				BgL_objz00_2454 = PROCEDURE_REF(BgL_envz00_2452, (int) (1L));
				{	/* Pp/circle.scm 346 */
					obj_t BgL_aux1052z00_2484;

					BgL_aux1052z00_2484 = BINT(BgL_printerzd2numzd2_2453);
					{	/* Pp/circle.scm 346 */
						obj_t BgL_fun1366z00_2485;

						switch ((long) CINT(BgL_aux1052z00_2484))
							{
							case 1L:

								BgL_fun1366z00_2485 = BGl_writezd2envzd2zz__r4_output_6_10_3z00;
								break;
							case 2L:

								BgL_fun1366z00_2485 =
									BGl_writezd2circlezd2envz00zz__pp_circlez00;
								break;
							case 3L:

								BgL_fun1366z00_2485 =
									BGl_displayzd2circlezd2envz00zz__pp_circlez00;
								break;
							default:
								BgL_fun1366z00_2485 =
									BGl_displayzd2envzd2zz__r4_output_6_10_3z00;
							}
						{	/* Pp/circle.scm 352 */
							obj_t BgL_arg1365z00_2486;

							{	/* Pp/circle.scm 352 */
								obj_t BgL_tmpz00_2942;

								BgL_tmpz00_2942 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_arg1365z00_2486 =
									BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_2942);
							}
							return
								BGL_PROCEDURE_CALL2(BgL_fun1366z00_2485, BgL_objz00_2454,
								BgL_arg1365z00_2486);
						}
					}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__pp_circlez00(void)
	{
		{	/* Pp/circle.scm 26 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__pp_circlez00(void)
	{
		{	/* Pp/circle.scm 26 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__pp_circlez00(void)
	{
		{	/* Pp/circle.scm 26 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__pp_circlez00(void)
	{
		{	/* Pp/circle.scm 26 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1845z00zz__pp_circlez00));
			BGl_modulezd2initializa7ationz75zz__hashz00(482391669L,
				BSTRING_TO_STRING(BGl_string1845z00zz__pp_circlez00));
			return
				BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L,
				BSTRING_TO_STRING(BGl_string1845z00zz__pp_circlez00));
		}

	}

#ifdef __cplusplus
}
#endif
