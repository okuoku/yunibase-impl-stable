/*===========================================================================*/
/*   (Rgc/rgcexpand.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Rgc/rgcexpand.scm -indent -o objs/obj_u/Rgc/rgcexpand.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___RGC_EXPAND_TYPE_DEFINITIONS
#define BGL___RGC_EXPAND_TYPE_DEFINITIONS
#endif													// BGL___RGC_EXPAND_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_symbol2561z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2480z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2563z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2482z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2485z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2534z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2372z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2568z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2535z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2487z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2373z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2536z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2489z00zz__rgc_expandz00 = BUNSPEC;
	extern obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_symbol2571z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2491z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2574z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2541z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2493z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2542z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2543z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2495z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2577z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2544z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2497z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2547z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2499z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2548z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_requirezd2initializa7ationz75zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2580z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2583z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2551z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2552z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2553z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2587z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2554z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2555z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2589z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2556z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2557z00zz__rgc_expandz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__rgc_expandz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__rgc_setz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__rgcz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__rgc_configz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__rgc_compilez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__rgc_dfaz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__rgc_treez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__rgc_rulesz00(long, char *);
	static obj_t BGl_list2565z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2566z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2567z00zz__rgc_expandz00 = BUNSPEC;
	extern obj_t BGl_regularzd2treezd2ze3nodeze3zz__rgc_treez00(obj_t);
	static obj_t BGl_list2570z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2573z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2576z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2579z00zz__rgc_expandz00 = BUNSPEC;
	extern obj_t BGl_nodezd2ze3dfaz31zz__rgc_dfaz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_resetzd2dfaz12zc0zz__rgc_dfaz00(void);
	static obj_t BGl_list2582z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2586z00zz__rgc_expandz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2regularzd2grammarz00zz__rgc_expandz00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__rgc_expandz00(void);
	static obj_t BGl_symbol2301z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2303z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_genericzd2initzd2zz__rgc_expandz00(void);
	static obj_t BGl_symbol2305z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2307z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2309z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_importedzd2moduleszd2initz00zz__rgc_expandz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__rgc_expandz00(void);
	static obj_t BGl_objectzd2initzd2zz__rgc_expandz00(void);
	static obj_t BGl_symbol2313z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2315z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2318z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2400z00zz__rgc_expandz00 = BUNSPEC;
	extern obj_t bgl_reverse_bang(obj_t);
	static obj_t BGl_symbol2402z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2321z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2404z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2323z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2406z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2325z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2408z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2327z00zz__rgc_expandz00 = BUNSPEC;
	extern obj_t BGl_getzd2initialzd2statez00zz__rgc_dfaz00(void);
	static obj_t BGl_makezd2regularzd2parserz00zz__rgc_expandz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2410z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2330z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2412z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2332z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2414z00zz__rgc_expandz00 = BUNSPEC;
	extern obj_t BGl_compilezd2dfazd2zz__rgc_compilez00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2416z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2335z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2418z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2338z00zz__rgc_expandz00 = BUNSPEC;
	extern obj_t BGl_statezd2namezd2zz__rgc_dfaz00(obj_t);
	static obj_t BGl_symbol2501z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2420z00zz__rgc_expandz00 = BUNSPEC;
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_symbol2340z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2503z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2422z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2342z00zz__rgc_expandz00 = BUNSPEC;
	extern obj_t BGl_resetzd2treez12zc0zz__rgc_treez00(void);
	static obj_t BGl_symbol2505z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2424z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2507z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2426z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2345z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2509z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2348z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2317z00zz__rgc_expandz00 = BUNSPEC;
	extern obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	extern obj_t BGl_resetzd2specialzd2matchzd2charz12zc0zz__rgc_rulesz00(void);
	static obj_t BGl_symbol2511z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2430z00zz__rgc_expandz00 = BUNSPEC;
	extern obj_t BGl_za2unsafezd2rgcza2zd2zz__rgcz00;
	static obj_t BGl_symbol2350z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2513z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2432z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_methodzd2initzd2zz__rgc_expandz00(void);
	static obj_t BGl_symbol2515z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2434z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2320z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2354z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2517z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2436z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2519z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2438z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2329z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2521z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2440z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2360z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2523z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2443z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2526z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2445z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2364z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2528z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2447z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2334z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2449z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2337z00zz__rgc_expandz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_expandzd2stringzd2casez00zz__rgc_expandz00(obj_t,
		obj_t);
	static obj_t BGl_symbol2530z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2532z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2451z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2370z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2453z00zz__rgc_expandz00 = BUNSPEC;
	extern obj_t BGl_ruleszd2ze3regularzd2treeze3zz__rgc_rulesz00(obj_t, obj_t);
	static obj_t BGl_symbol2455z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2374z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2537z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2457z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2376z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2539z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2344z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2459z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2378z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2297z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2347z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2299z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2461z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2380z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2463z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2382z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2545z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2384z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2352z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2467z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2386z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2353z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2549z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2469z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2388z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2356z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2357z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2358z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2359z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_z62expandzd2stringzd2casez62zz__rgc_expandz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol2471z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2390z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2473z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2392z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2394z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2476z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2362z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2558z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2525z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2396z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2363z00zz__rgc_expandz00 = BUNSPEC;
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2478z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2398z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2366z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2367z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2368z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_list2369z00zz__rgc_expandz00 = BUNSPEC;
	static obj_t BGl_z62expandzd2regularzd2grammarz62zz__rgc_expandz00(obj_t,
		obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2regularzd2grammarzd2envzd2zz__rgc_expandz00,
		BgL_bgl_za762expandza7d2regu2592z00,
		BGl_z62expandzd2regularzd2grammarz62zz__rgc_expandz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2300z00zz__rgc_expandz00,
		BgL_bgl_string2300za700za7za7_2593za7, "open-input-string", 17);
	      DEFINE_STRING(BGl_string2302z00zz__rgc_expandz00,
		BgL_bgl_string2302za700za7za7_2594za7, "regular-grammar", 15);
	      DEFINE_STRING(BGl_string2304z00zz__rgc_expandz00,
		BgL_bgl_string2304za700za7za7_2595za7, "read/rp", 7);
	      DEFINE_STRING(BGl_string2306z00zz__rgc_expandz00,
		BgL_bgl_string2306za700za7za7_2596za7, "close-input-port", 16);
	      DEFINE_STRING(BGl_string2308z00zz__rgc_expandz00,
		BgL_bgl_string2308za700za7za7_2597za7, "unwind-protect", 14);
	      DEFINE_STRING(BGl_string2310z00zz__rgc_expandz00,
		BgL_bgl_string2310za700za7za7_2598za7, "let", 3);
	      DEFINE_STRING(BGl_string2311z00zz__rgc_expandz00,
		BgL_bgl_string2311za700za7za7_2599za7, "string-case", 11);
	      DEFINE_STRING(BGl_string2312z00zz__rgc_expandz00,
		BgL_bgl_string2312za700za7za7_2600za7, "Illegal form", 12);
	      DEFINE_STRING(BGl_string2314z00zz__rgc_expandz00,
		BgL_bgl_string2314za700za7za7_2601za7, "the-rgc-context", 15);
	      DEFINE_STRING(BGl_string2316z00zz__rgc_expandz00,
		BgL_bgl_string2316za700za7za7_2602za7, "iport", 5);
	      DEFINE_STRING(BGl_string2319z00zz__rgc_expandz00,
		BgL_bgl_string2319za700za7za7_2603za7, "define", 6);
	      DEFINE_STRING(BGl_string2401z00zz__rgc_expandz00,
		BgL_bgl_string2401za700za7za7_2604za7, "min::int", 8);
	      DEFINE_STRING(BGl_string2403z00zz__rgc_expandz00,
		BgL_bgl_string2403za700za7za7_2605za7, "the-substring::bstring", 22);
	      DEFINE_STRING(BGl_string2322z00zz__rgc_expandz00,
		BgL_bgl_string2322za700za7za7_2606za7, "rgc-submatch-stop2!", 19);
	      DEFINE_STRING(BGl_string2405z00zz__rgc_expandz00,
		BgL_bgl_string2405za700za7za7_2607za7, "min", 3);
	      DEFINE_STRING(BGl_string2324z00zz__rgc_expandz00,
		BgL_bgl_string2324za700za7za7_2608za7, "match::int", 10);
	      DEFINE_STRING(BGl_string2407z00zz__rgc_expandz00,
		BgL_bgl_string2407za700za7za7_2609za7, "max", 3);
	      DEFINE_STRING(BGl_string2326z00zz__rgc_expandz00,
		BgL_bgl_string2326za700za7za7_2610za7, "submatch::int", 13);
	      DEFINE_STRING(BGl_string2409z00zz__rgc_expandz00,
		BgL_bgl_string2409za700za7za7_2611za7, "<fx", 3);
	      DEFINE_STRING(BGl_string2328z00zz__rgc_expandz00,
		BgL_bgl_string2328za700za7za7_2612za7, "forward", 7);
	      DEFINE_STRING(BGl_string2411z00zz__rgc_expandz00,
		BgL_bgl_string2411za700za7za7_2613za7, "+fx", 3);
	      DEFINE_STRING(BGl_string2331z00zz__rgc_expandz00,
		BgL_bgl_string2331za700za7za7_2614za7, "set!", 4);
	      DEFINE_STRING(BGl_string2413z00zz__rgc_expandz00,
		BgL_bgl_string2413za700za7za7_2615za7, "when", 4);
	      DEFINE_STRING(BGl_string2333z00zz__rgc_expandz00,
		BgL_bgl_string2333za700za7za7_2616za7, "rgc-submatches", 14);
	      DEFINE_STRING(BGl_string2415z00zz__rgc_expandz00,
		BgL_bgl_string2415za700za7za7_2617za7, ">=fx", 4);
	      DEFINE_STRING(BGl_string2417z00zz__rgc_expandz00,
		BgL_bgl_string2417za700za7za7_2618za7, "<=fx", 4);
	      DEFINE_STRING(BGl_string2336z00zz__rgc_expandz00,
		BgL_bgl_string2336za700za7za7_2619za7, "cons", 4);
	      DEFINE_STRING(BGl_string2419z00zz__rgc_expandz00,
		BgL_bgl_string2419za700za7za7_2620za7, "and", 3);
	      DEFINE_STRING(BGl_string2339z00zz__rgc_expandz00,
		BgL_bgl_string2339za700za7za7_2621za7, "vector", 6);
	      DEFINE_STRING(BGl_string2500z00zz__rgc_expandz00,
		BgL_bgl_string2500za700za7za7_2622za7, "the-failure", 11);
	      DEFINE_STRING(BGl_string2502z00zz__rgc_expandz00,
		BgL_bgl_string2502za700za7za7_2623za7, "=fx", 3);
	      DEFINE_STRING(BGl_string2421z00zz__rgc_expandz00,
		BgL_bgl_string2421za700za7za7_2624za7, "__r4_output_6_10_3", 18);
	      DEFINE_STRING(BGl_string2341z00zz__rgc_expandz00,
		BgL_bgl_string2341za700za7za7_2625za7, "match", 5);
	      DEFINE_STRING(BGl_string2504z00zz__rgc_expandz00,
		BgL_bgl_string2504za700za7za7_2626za7, "eof-object", 10);
	      DEFINE_STRING(BGl_string2423z00zz__rgc_expandz00,
		BgL_bgl_string2423za700za7za7_2627za7, "format", 6);
	      DEFINE_STRING(BGl_string2343z00zz__rgc_expandz00,
		BgL_bgl_string2343za700za7za7_2628za7, "submatch", 8);
	      DEFINE_STRING(BGl_string2506z00zz__rgc_expandz00,
		BgL_bgl_string2506za700za7za7_2629za7, "the-context", 11);
	      DEFINE_STRING(BGl_string2425z00zz__rgc_expandz00,
		BgL_bgl_string2425za700za7za7_2630za7, "@", 1);
	      DEFINE_STRING(BGl_string2508z00zz__rgc_expandz00,
		BgL_bgl_string2508za700za7za7_2631za7, "context", 7);
	      DEFINE_STRING(BGl_string2427z00zz__rgc_expandz00,
		BgL_bgl_string2427za700za7za7_2632za7, "the-string", 10);
	      DEFINE_STRING(BGl_string2346z00zz__rgc_expandz00,
		BgL_bgl_string2346za700za7za7_2633za7, "rgc-buffer-position", 19);
	      DEFINE_STRING(BGl_string2428z00zz__rgc_expandz00,
		BgL_bgl_string2428za700za7za7_2634za7, "Illegal range `~a'", 18);
	      DEFINE_STRING(BGl_string2429z00zz__rgc_expandz00,
		BgL_bgl_string2429za700za7za7_2635za7, "the-substring", 13);
	      DEFINE_STRING(BGl_string2349z00zz__rgc_expandz00,
		BgL_bgl_string2349za700za7za7_2636za7, "quote", 5);
	      DEFINE_STRING(BGl_string2510z00zz__rgc_expandz00,
		BgL_bgl_string2510za700za7za7_2637za7, "rgc-context?::bool", 18);
	      DEFINE_STRING(BGl_string2512z00zz__rgc_expandz00,
		BgL_bgl_string2512za700za7za7_2638za7, "eq?", 3);
	      DEFINE_STRING(BGl_string2431z00zz__rgc_expandz00,
		BgL_bgl_string2431za700za7za7_2639za7, "error", 5);
	      DEFINE_STRING(BGl_string2351z00zz__rgc_expandz00,
		BgL_bgl_string2351za700za7za7_2640za7, "stop", 4);
	      DEFINE_STRING(BGl_string2514z00zz__rgc_expandz00,
		BgL_bgl_string2514za700za7za7_2641za7, "rgc-set-context!", 16);
	      DEFINE_STRING(BGl_string2433z00zz__rgc_expandz00,
		BgL_bgl_string2433za700za7za7_2642za7, "if", 2);
	      DEFINE_STRING(BGl_string2516z00zz__rgc_expandz00,
		BgL_bgl_string2516za700za7za7_2643za7, "rgc-context", 11);
	      DEFINE_STRING(BGl_string2435z00zz__rgc_expandz00,
		BgL_bgl_string2435za700za7za7_2644za7, "strict::bool", 12);
	      DEFINE_STRING(BGl_string2355z00zz__rgc_expandz00,
		BgL_bgl_string2355za700za7za7_2645za7, "rgc-submatch-start*2!", 21);
	      DEFINE_STRING(BGl_string2518z00zz__rgc_expandz00,
		BgL_bgl_string2518za700za7za7_2646za7, "pair?", 5);
	      DEFINE_STRING(BGl_string2437z00zz__rgc_expandz00,
		BgL_bgl_string2437za700za7za7_2647za7, "the-escape-substring::bstring", 29);
	      DEFINE_STRING(BGl_string2439z00zz__rgc_expandz00,
		BgL_bgl_string2439za700za7za7_2648za7, "strict", 6);
	      DEFINE_STRING(BGl_string2520z00zz__rgc_expandz00,
		BgL_bgl_string2520za700za7za7_2649za7, "car", 3);
	      DEFINE_STRING(BGl_string2522z00zz__rgc_expandz00,
		BgL_bgl_string2522za700za7za7_2650za7, "ignore", 6);
	      DEFINE_STRING(BGl_string2441z00zz__rgc_expandz00,
		BgL_bgl_string2441za700za7za7_2651za7, "rgc-buffer-escape-substring", 27);
	      DEFINE_STRING(BGl_string2442z00zz__rgc_expandz00,
		BgL_bgl_string2442za700za7za7_2652za7, "the-escape-substring", 20);
	      DEFINE_STRING(BGl_string2361z00zz__rgc_expandz00,
		BgL_bgl_string2361za700za7za7_2653za7, "start*", 6);
	      DEFINE_STRING(BGl_string2524z00zz__rgc_expandz00,
		BgL_bgl_string2524za700za7za7_2654za7, "rgc-start-match!", 16);
	      DEFINE_STRING(BGl_string2444z00zz__rgc_expandz00,
		BgL_bgl_string2444za700za7za7_2655za7, "the-length::int", 15);
	      DEFINE_STRING(BGl_string2527z00zz__rgc_expandz00,
		BgL_bgl_string2527za700za7za7_2656za7, "rgc-buffer-forward", 18);
	      DEFINE_STRING(BGl_string2446z00zz__rgc_expandz00,
		BgL_bgl_string2446za700za7za7_2657za7, "rgc-buffer-length", 17);
	      DEFINE_STRING(BGl_string2365z00zz__rgc_expandz00,
		BgL_bgl_string2365za700za7za7_2658za7, "rgc-submatch-start2!", 20);
	      DEFINE_STRING(BGl_string2529z00zz__rgc_expandz00,
		BgL_bgl_string2529za700za7za7_2659za7, "rgc-buffer-bufpos", 17);
	      DEFINE_STRING(BGl_string2448z00zz__rgc_expandz00,
		BgL_bgl_string2448za700za7za7_2660za7, "the-fixnum::long", 16);
	      DEFINE_STRING(BGl_string2531z00zz__rgc_expandz00,
		BgL_bgl_string2531za700za7za7_2661za7, "match::long", 11);
	      DEFINE_STRING(BGl_string2450z00zz__rgc_expandz00,
		BgL_bgl_string2450za700za7za7_2662za7, "rgc-buffer-fixnum", 17);
	      DEFINE_STRING(BGl_string2533z00zz__rgc_expandz00,
		BgL_bgl_string2533za700za7za7_2663za7, "rgc-set-filepos!", 16);
	      DEFINE_STRING(BGl_string2452z00zz__rgc_expandz00,
		BgL_bgl_string2452za700za7za7_2664za7, "the-integer::obj", 16);
	      DEFINE_STRING(BGl_string2371z00zz__rgc_expandz00,
		BgL_bgl_string2371za700za7za7_2665za7, "start", 5);
	      DEFINE_STRING(BGl_string2454z00zz__rgc_expandz00,
		BgL_bgl_string2454za700za7za7_2666za7, "rgc-buffer-integer", 18);
	      DEFINE_STRING(BGl_string2456z00zz__rgc_expandz00,
		BgL_bgl_string2456za700za7za7_2667za7, "the-flonum::double", 18);
	      DEFINE_STRING(BGl_string2375z00zz__rgc_expandz00,
		BgL_bgl_string2375za700za7za7_2668za7, "the-port::input-port", 20);
	      DEFINE_STRING(BGl_string2538z00zz__rgc_expandz00,
		BgL_bgl_string2538za700za7za7_2669za7, "the-submatch", 12);
	      DEFINE_STRING(BGl_string2458z00zz__rgc_expandz00,
		BgL_bgl_string2458za700za7za7_2670za7, "rgc-buffer-flonum", 17);
	      DEFINE_STRING(BGl_string2377z00zz__rgc_expandz00,
		BgL_bgl_string2377za700za7za7_2671za7, "the-character::char", 19);
	      DEFINE_STRING(BGl_string2379z00zz__rgc_expandz00,
		BgL_bgl_string2379za700za7za7_2672za7, "rgc-buffer-character", 20);
	      DEFINE_STRING(BGl_string2298z00zz__rgc_expandz00,
		BgL_bgl_string2298za700za7za7_2673za7, "port", 4);
	      DEFINE_STRING(BGl_string2540z00zz__rgc_expandz00,
		BgL_bgl_string2540za700za7za7_2674za7, "num", 3);
	      DEFINE_STRING(BGl_string2460z00zz__rgc_expandz00,
		BgL_bgl_string2460za700za7za7_2675za7, "the-symbol::symbol", 18);
	      DEFINE_STRING(BGl_string2462z00zz__rgc_expandz00,
		BgL_bgl_string2462za700za7za7_2676za7, "rgc-buffer-symbol", 17);
	      DEFINE_STRING(BGl_string2381z00zz__rgc_expandz00,
		BgL_bgl_string2381za700za7za7_2677za7, "the-byte::int", 13);
	      DEFINE_STRING(BGl_string2464z00zz__rgc_expandz00,
		BgL_bgl_string2464za700za7za7_2678za7, "the-subsymbol::symbol", 21);
	      DEFINE_STRING(BGl_string2383z00zz__rgc_expandz00,
		BgL_bgl_string2383za700za7za7_2679za7, "rgc-buffer-byte", 15);
	      DEFINE_STRING(BGl_string2546z00zz__rgc_expandz00,
		BgL_bgl_string2546za700za7za7_2680za7, "multiple-value-bind", 19);
	      DEFINE_STRING(BGl_string2465z00zz__rgc_expandz00,
		BgL_bgl_string2465za700za7za7_2681za7, "Illegal range", 13);
	      DEFINE_STRING(BGl_string2466z00zz__rgc_expandz00,
		BgL_bgl_string2466za700za7za7_2682za7, "the-subsymbol", 13);
	      DEFINE_STRING(BGl_string2385z00zz__rgc_expandz00,
		BgL_bgl_string2385za700za7za7_2683za7, "offset::int", 11);
	      DEFINE_STRING(BGl_string2468z00zz__rgc_expandz00,
		BgL_bgl_string2468za700za7za7_2684za7, "rgc-buffer-subsymbol", 20);
	      DEFINE_STRING(BGl_string2387z00zz__rgc_expandz00,
		BgL_bgl_string2387za700za7za7_2685za7, "the-byte-ref::int", 17);
	      DEFINE_STRING(BGl_string2389z00zz__rgc_expandz00,
		BgL_bgl_string2389za700za7za7_2686za7, "offset", 6);
	      DEFINE_STRING(BGl_string2550z00zz__rgc_expandz00,
		BgL_bgl_string2550za700za7za7_2687za7, "rgc-the-submatch", 16);
	      DEFINE_STRING(BGl_string2470z00zz__rgc_expandz00,
		BgL_bgl_string2470za700za7za7_2688za7, "the-downcase-symbol::symbol", 27);
	      DEFINE_STRING(BGl_string2472z00zz__rgc_expandz00,
		BgL_bgl_string2472za700za7za7_2689za7, "rgc-buffer-downcase-symbol", 26);
	      DEFINE_STRING(BGl_string2391z00zz__rgc_expandz00,
		BgL_bgl_string2391za700za7za7_2690za7, "rgc-buffer-byte-ref", 19);
	      DEFINE_STRING(BGl_string2474z00zz__rgc_expandz00,
		BgL_bgl_string2474za700za7za7_2691za7, "the-downcase-subsymbol::symbol",
		30);
	      DEFINE_STRING(BGl_string2393z00zz__rgc_expandz00,
		BgL_bgl_string2393za700za7za7_2692za7, "the-string::bstring", 19);
	      DEFINE_STRING(BGl_string2475z00zz__rgc_expandz00,
		BgL_bgl_string2475za700za7za7_2693za7, "the-downcase-subsymbol", 22);
	      DEFINE_STRING(BGl_string2395z00zz__rgc_expandz00,
		BgL_bgl_string2395za700za7za7_2694za7, "the-length", 10);
	      DEFINE_STRING(BGl_string2477z00zz__rgc_expandz00,
		BgL_bgl_string2477za700za7za7_2695za7, "rgc-buffer-downcase-subsymbol", 29);
	      DEFINE_STRING(BGl_string2559z00zz__rgc_expandz00,
		BgL_bgl_string2559za700za7za7_2696za7, "", 0);
	      DEFINE_STRING(BGl_string2397z00zz__rgc_expandz00,
		BgL_bgl_string2397za700za7za7_2697za7, "rgc-buffer-substring", 20);
	      DEFINE_STRING(BGl_string2479z00zz__rgc_expandz00,
		BgL_bgl_string2479za700za7za7_2698za7, "the-upcase-symbol::symbol", 25);
	      DEFINE_STRING(BGl_string2399z00zz__rgc_expandz00,
		BgL_bgl_string2399za700za7za7_2699za7, "max::int", 8);
	      DEFINE_STRING(BGl_string2560z00zz__rgc_expandz00,
		BgL_bgl_string2560za700za7za7_2700za7, "Illegal match", 13);
	      DEFINE_STRING(BGl_string2562z00zz__rgc_expandz00,
		BgL_bgl_string2562za700za7za7_2701za7, "else", 4);
	      DEFINE_STRING(BGl_string2481z00zz__rgc_expandz00,
		BgL_bgl_string2481za700za7za7_2702za7, "rgc-buffer-upcase-symbol", 24);
	      DEFINE_STRING(BGl_string2564z00zz__rgc_expandz00,
		BgL_bgl_string2564za700za7za7_2703za7, "case", 4);
	      DEFINE_STRING(BGl_string2483z00zz__rgc_expandz00,
		BgL_bgl_string2483za700za7za7_2704za7, "the-upcase-subsymbol::symbol", 28);
	      DEFINE_STRING(BGl_string2484z00zz__rgc_expandz00,
		BgL_bgl_string2484za700za7za7_2705za7, "the-upcase-subsymbol", 20);
	      DEFINE_STRING(BGl_string2486z00zz__rgc_expandz00,
		BgL_bgl_string2486za700za7za7_2706za7, "rgc-buffer-upcase-subsymbol", 27);
	      DEFINE_STRING(BGl_string2569z00zz__rgc_expandz00,
		BgL_bgl_string2569za700za7za7_2707za7, "closed-input-port?", 18);
	      DEFINE_STRING(BGl_string2488z00zz__rgc_expandz00,
		BgL_bgl_string2488za700za7za7_2708za7, "the-keyword::keyword", 20);
	      DEFINE_STRING(BGl_string2490z00zz__rgc_expandz00,
		BgL_bgl_string2490za700za7za7_2709za7, "rgc-buffer-keyword", 18);
	      DEFINE_STRING(BGl_string2572z00zz__rgc_expandz00,
		BgL_bgl_string2572za700za7za7_2710za7, "the-port", 8);
	      DEFINE_STRING(BGl_string2492z00zz__rgc_expandz00,
		BgL_bgl_string2492za700za7za7_2711za7, "the-downcase-keyword::keyword", 29);
	      DEFINE_STRING(BGl_string2575z00zz__rgc_expandz00,
		BgL_bgl_string2575za700za7za7_2712za7, "raise", 5);
	      DEFINE_STRING(BGl_string2494z00zz__rgc_expandz00,
		BgL_bgl_string2494za700za7za7_2713za7, "rgc-buffer-downcase-keyword", 27);
	      DEFINE_STRING(BGl_string2496z00zz__rgc_expandz00,
		BgL_bgl_string2496za700za7za7_2714za7, "the-upcase-keyword::keyword", 27);
	      DEFINE_STRING(BGl_string2578z00zz__rgc_expandz00,
		BgL_bgl_string2578za700za7za7_2715za7, "instantiate::&io-closed-error", 29);
	      DEFINE_STRING(BGl_string2498z00zz__rgc_expandz00,
		BgL_bgl_string2498za700za7za7_2716za7, "rgc-buffer-upcase-keyword", 25);
	      DEFINE_STRING(BGl_string2581z00zz__rgc_expandz00,
		BgL_bgl_string2581za700za7za7_2717za7, "proc", 4);
	      DEFINE_STRING(BGl_string2584z00zz__rgc_expandz00,
		BgL_bgl_string2584za700za7za7_2718za7, "msg", 3);
	      DEFINE_STRING(BGl_string2585z00zz__rgc_expandz00,
		BgL_bgl_string2585za700za7za7_2719za7, "Can't read on a closed input port",
		33);
	      DEFINE_STRING(BGl_string2588z00zz__rgc_expandz00,
		BgL_bgl_string2588za700za7za7_2720za7, "obj", 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2stringzd2casezd2envzd2zz__rgc_expandz00,
		BgL_bgl_za762expandza7d2stri2721z00,
		BGl_z62expandzd2stringzd2casez62zz__rgc_expandz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2590z00zz__rgc_expandz00,
		BgL_bgl_string2590za700za7za7_2722za7, "lambda", 6);
	      DEFINE_STRING(BGl_string2591z00zz__rgc_expandz00,
		BgL_bgl_string2591za700za7za7_2723za7, "__rgc_expand", 12);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_symbol2561z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2480z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2563z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2482z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2485z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2534z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2372z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2568z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2535z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2487z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2373z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2536z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2489z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2571z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2491z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2574z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2541z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2493z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2542z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2543z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2495z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2577z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2544z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2497z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2547z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2499z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2548z00zz__rgc_expandz00));
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2580z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2583z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2551z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2552z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2553z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2587z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2554z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2555z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2589z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2556z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2557z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2565z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2566z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2567z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2570z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2573z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2576z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2579z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2582z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2586z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2301z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2303z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2305z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2307z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2309z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2313z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2315z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2318z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2400z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2402z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2321z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2404z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2323z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2406z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2325z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2408z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2327z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2410z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2330z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2412z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2332z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2414z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2416z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2335z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2418z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2338z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2501z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2420z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2340z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2503z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2422z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2342z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2505z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2424z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2507z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2426z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2345z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2509z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2348z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2317z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2511z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2430z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2350z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2513z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2432z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2515z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2434z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2320z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2354z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2517z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2436z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2519z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2438z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2329z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2521z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2440z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2360z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2523z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2443z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2526z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2445z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2364z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2528z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2447z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2334z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2449z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2337z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2530z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2532z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2451z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2370z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2453z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2455z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2374z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2537z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2457z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2376z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2539z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2344z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2459z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2378z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2297z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2347z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2299z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2461z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2380z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2463z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2382z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2545z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2384z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2352z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2467z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2386z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2353z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2549z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2469z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2388z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2356z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2357z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2358z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2359z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2471z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2390z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2473z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2392z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2394z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2476z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2362z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2558z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2525z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2396z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2363z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2478z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2398z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2366z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2367z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2368z00zz__rgc_expandz00));
		     ADD_ROOT((void *) (&BGl_list2369z00zz__rgc_expandz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__rgc_expandz00(long
		BgL_checksumz00_2683, char *BgL_fromz00_2684)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__rgc_expandz00))
				{
					BGl_requirezd2initializa7ationz75zz__rgc_expandz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__rgc_expandz00();
					BGl_cnstzd2initzd2zz__rgc_expandz00();
					BGl_importedzd2moduleszd2initz00zz__rgc_expandz00();
					return BGl_methodzd2initzd2zz__rgc_expandz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__rgc_expandz00(void)
	{
		{	/* Rgc/rgcexpand.scm 22 */
			BGl_symbol2297z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2298z00zz__rgc_expandz00);
			BGl_symbol2299z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2300z00zz__rgc_expandz00);
			BGl_symbol2301z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2302z00zz__rgc_expandz00);
			BGl_symbol2303z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2304z00zz__rgc_expandz00);
			BGl_symbol2305z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2306z00zz__rgc_expandz00);
			BGl_symbol2307z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2308z00zz__rgc_expandz00);
			BGl_symbol2309z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2310z00zz__rgc_expandz00);
			BGl_symbol2313z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2314z00zz__rgc_expandz00);
			BGl_symbol2315z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2316z00zz__rgc_expandz00);
			BGl_symbol2318z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2319z00zz__rgc_expandz00);
			BGl_symbol2321z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2322z00zz__rgc_expandz00);
			BGl_symbol2323z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2324z00zz__rgc_expandz00);
			BGl_symbol2325z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2326z00zz__rgc_expandz00);
			BGl_symbol2327z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2328z00zz__rgc_expandz00);
			BGl_list2320z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2321z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_symbol2323z00zz__rgc_expandz00,
					MAKE_YOUNG_PAIR(BGl_symbol2325z00zz__rgc_expandz00,
						MAKE_YOUNG_PAIR(BGl_symbol2327z00zz__rgc_expandz00, BNIL))));
			BGl_symbol2330z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2331z00zz__rgc_expandz00);
			BGl_symbol2332z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2333z00zz__rgc_expandz00);
			BGl_symbol2335z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2336z00zz__rgc_expandz00);
			BGl_symbol2338z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2339z00zz__rgc_expandz00);
			BGl_symbol2340z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2341z00zz__rgc_expandz00);
			BGl_symbol2342z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2343z00zz__rgc_expandz00);
			BGl_symbol2345z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2346z00zz__rgc_expandz00);
			BGl_list2344z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2345z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_symbol2315z00zz__rgc_expandz00,
					MAKE_YOUNG_PAIR(BGl_symbol2327z00zz__rgc_expandz00, BNIL)));
			BGl_symbol2348z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2349z00zz__rgc_expandz00);
			BGl_symbol2350z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2351z00zz__rgc_expandz00);
			BGl_list2347z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2348z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_symbol2350z00zz__rgc_expandz00, BNIL));
			BGl_list2337z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2338z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_symbol2340z00zz__rgc_expandz00,
					MAKE_YOUNG_PAIR(BGl_symbol2342z00zz__rgc_expandz00,
						MAKE_YOUNG_PAIR(BGl_list2344z00zz__rgc_expandz00,
							MAKE_YOUNG_PAIR(BGl_list2347z00zz__rgc_expandz00, BNIL)))));
			BGl_list2334z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2335z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_list2337z00zz__rgc_expandz00,
					MAKE_YOUNG_PAIR(BGl_symbol2332z00zz__rgc_expandz00, BNIL)));
			BGl_list2329z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2330z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_symbol2332z00zz__rgc_expandz00,
					MAKE_YOUNG_PAIR(BGl_list2334z00zz__rgc_expandz00, BNIL)));
			BGl_list2317z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2318z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_list2320z00zz__rgc_expandz00,
					MAKE_YOUNG_PAIR(BGl_list2329z00zz__rgc_expandz00, BNIL)));
			BGl_symbol2354z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2355z00zz__rgc_expandz00);
			BGl_list2353z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2354z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_symbol2323z00zz__rgc_expandz00,
					MAKE_YOUNG_PAIR(BGl_symbol2325z00zz__rgc_expandz00,
						MAKE_YOUNG_PAIR(BGl_symbol2327z00zz__rgc_expandz00, BNIL))));
			BGl_symbol2360z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2361z00zz__rgc_expandz00);
			BGl_list2359z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2348z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_symbol2360z00zz__rgc_expandz00, BNIL));
			BGl_list2358z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2338z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_symbol2340z00zz__rgc_expandz00,
					MAKE_YOUNG_PAIR(BGl_symbol2342z00zz__rgc_expandz00,
						MAKE_YOUNG_PAIR(BGl_list2344z00zz__rgc_expandz00,
							MAKE_YOUNG_PAIR(BGl_list2359z00zz__rgc_expandz00, BNIL)))));
			BGl_list2357z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2335z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_list2358z00zz__rgc_expandz00,
					MAKE_YOUNG_PAIR(BGl_symbol2332z00zz__rgc_expandz00, BNIL)));
			BGl_list2356z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2330z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_symbol2332z00zz__rgc_expandz00,
					MAKE_YOUNG_PAIR(BGl_list2357z00zz__rgc_expandz00, BNIL)));
			BGl_list2352z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2318z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_list2353z00zz__rgc_expandz00,
					MAKE_YOUNG_PAIR(BGl_list2356z00zz__rgc_expandz00, BNIL)));
			BGl_symbol2364z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2365z00zz__rgc_expandz00);
			BGl_list2363z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2364z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_symbol2323z00zz__rgc_expandz00,
					MAKE_YOUNG_PAIR(BGl_symbol2325z00zz__rgc_expandz00,
						MAKE_YOUNG_PAIR(BGl_symbol2327z00zz__rgc_expandz00, BNIL))));
			BGl_symbol2370z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2371z00zz__rgc_expandz00);
			BGl_list2369z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2348z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_symbol2370z00zz__rgc_expandz00, BNIL));
			BGl_list2368z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2338z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_symbol2340z00zz__rgc_expandz00,
					MAKE_YOUNG_PAIR(BGl_symbol2342z00zz__rgc_expandz00,
						MAKE_YOUNG_PAIR(BGl_list2344z00zz__rgc_expandz00,
							MAKE_YOUNG_PAIR(BGl_list2369z00zz__rgc_expandz00, BNIL)))));
			BGl_list2367z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2335z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_list2368z00zz__rgc_expandz00,
					MAKE_YOUNG_PAIR(BGl_symbol2332z00zz__rgc_expandz00, BNIL)));
			BGl_list2366z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2330z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_symbol2332z00zz__rgc_expandz00,
					MAKE_YOUNG_PAIR(BGl_list2367z00zz__rgc_expandz00, BNIL)));
			BGl_list2362z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2318z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_list2363z00zz__rgc_expandz00,
					MAKE_YOUNG_PAIR(BGl_list2366z00zz__rgc_expandz00, BNIL)));
			BGl_list2373z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2348z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BNIL, BNIL));
			BGl_list2372z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2318z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_symbol2332z00zz__rgc_expandz00,
					MAKE_YOUNG_PAIR(BGl_list2373z00zz__rgc_expandz00, BNIL)));
			BGl_symbol2374z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2375z00zz__rgc_expandz00);
			BGl_symbol2376z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2377z00zz__rgc_expandz00);
			BGl_symbol2378z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2379z00zz__rgc_expandz00);
			BGl_symbol2380z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2381z00zz__rgc_expandz00);
			BGl_symbol2382z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2383z00zz__rgc_expandz00);
			BGl_symbol2384z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2385z00zz__rgc_expandz00);
			BGl_symbol2386z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2387z00zz__rgc_expandz00);
			BGl_symbol2388z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2389z00zz__rgc_expandz00);
			BGl_symbol2390z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2391z00zz__rgc_expandz00);
			BGl_symbol2392z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2393z00zz__rgc_expandz00);
			BGl_symbol2394z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2395z00zz__rgc_expandz00);
			BGl_symbol2396z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2397z00zz__rgc_expandz00);
			BGl_symbol2398z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2399z00zz__rgc_expandz00);
			BGl_symbol2400z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2401z00zz__rgc_expandz00);
			BGl_symbol2402z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2403z00zz__rgc_expandz00);
			BGl_symbol2404z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2405z00zz__rgc_expandz00);
			BGl_symbol2406z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2407z00zz__rgc_expandz00);
			BGl_symbol2408z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2409z00zz__rgc_expandz00);
			BGl_symbol2410z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2411z00zz__rgc_expandz00);
			BGl_symbol2412z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2413z00zz__rgc_expandz00);
			BGl_symbol2414z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2415z00zz__rgc_expandz00);
			BGl_symbol2416z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2417z00zz__rgc_expandz00);
			BGl_symbol2418z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2419z00zz__rgc_expandz00);
			BGl_symbol2420z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2421z00zz__rgc_expandz00);
			BGl_symbol2422z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2423z00zz__rgc_expandz00);
			BGl_symbol2424z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2425z00zz__rgc_expandz00);
			BGl_symbol2426z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2427z00zz__rgc_expandz00);
			BGl_symbol2430z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2431z00zz__rgc_expandz00);
			BGl_symbol2432z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2433z00zz__rgc_expandz00);
			BGl_symbol2434z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2435z00zz__rgc_expandz00);
			BGl_symbol2436z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2437z00zz__rgc_expandz00);
			BGl_symbol2438z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2439z00zz__rgc_expandz00);
			BGl_symbol2440z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2441z00zz__rgc_expandz00);
			BGl_symbol2443z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2444z00zz__rgc_expandz00);
			BGl_symbol2445z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2446z00zz__rgc_expandz00);
			BGl_symbol2447z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2448z00zz__rgc_expandz00);
			BGl_symbol2449z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2450z00zz__rgc_expandz00);
			BGl_symbol2451z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2452z00zz__rgc_expandz00);
			BGl_symbol2453z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2454z00zz__rgc_expandz00);
			BGl_symbol2455z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2456z00zz__rgc_expandz00);
			BGl_symbol2457z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2458z00zz__rgc_expandz00);
			BGl_symbol2459z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2460z00zz__rgc_expandz00);
			BGl_symbol2461z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2462z00zz__rgc_expandz00);
			BGl_symbol2463z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2464z00zz__rgc_expandz00);
			BGl_symbol2467z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2468z00zz__rgc_expandz00);
			BGl_symbol2469z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2470z00zz__rgc_expandz00);
			BGl_symbol2471z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2472z00zz__rgc_expandz00);
			BGl_symbol2473z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2474z00zz__rgc_expandz00);
			BGl_symbol2476z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2477z00zz__rgc_expandz00);
			BGl_symbol2478z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2479z00zz__rgc_expandz00);
			BGl_symbol2480z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2481z00zz__rgc_expandz00);
			BGl_symbol2482z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2483z00zz__rgc_expandz00);
			BGl_symbol2485z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2486z00zz__rgc_expandz00);
			BGl_symbol2487z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2488z00zz__rgc_expandz00);
			BGl_symbol2489z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2490z00zz__rgc_expandz00);
			BGl_symbol2491z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2492z00zz__rgc_expandz00);
			BGl_symbol2493z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2494z00zz__rgc_expandz00);
			BGl_symbol2495z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2496z00zz__rgc_expandz00);
			BGl_symbol2497z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2498z00zz__rgc_expandz00);
			BGl_symbol2499z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2500z00zz__rgc_expandz00);
			BGl_symbol2501z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2502z00zz__rgc_expandz00);
			BGl_symbol2503z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2504z00zz__rgc_expandz00);
			BGl_symbol2505z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2506z00zz__rgc_expandz00);
			BGl_symbol2507z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2508z00zz__rgc_expandz00);
			BGl_symbol2509z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2510z00zz__rgc_expandz00);
			BGl_symbol2511z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2512z00zz__rgc_expandz00);
			BGl_symbol2513z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2514z00zz__rgc_expandz00);
			BGl_symbol2515z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2516z00zz__rgc_expandz00);
			BGl_symbol2517z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2518z00zz__rgc_expandz00);
			BGl_symbol2519z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2520z00zz__rgc_expandz00);
			BGl_symbol2521z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2522z00zz__rgc_expandz00);
			BGl_symbol2523z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2524z00zz__rgc_expandz00);
			BGl_list2525z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2330z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_symbol2332z00zz__rgc_expandz00,
					MAKE_YOUNG_PAIR(BGl_list2373z00zz__rgc_expandz00, BNIL)));
			BGl_symbol2526z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2527z00zz__rgc_expandz00);
			BGl_symbol2528z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2529z00zz__rgc_expandz00);
			BGl_symbol2530z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2531z00zz__rgc_expandz00);
			BGl_symbol2532z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2533z00zz__rgc_expandz00);
			BGl_symbol2537z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2538z00zz__rgc_expandz00);
			BGl_symbol2539z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2540z00zz__rgc_expandz00);
			BGl_list2536z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2537z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_symbol2539z00zz__rgc_expandz00, BNIL));
			BGl_list2542z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2501z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_symbol2539z00zz__rgc_expandz00,
					MAKE_YOUNG_PAIR(BINT(0L), BNIL)));
			BGl_list2543z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2426z00zz__rgc_expandz00, BNIL);
			BGl_symbol2545z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2546z00zz__rgc_expandz00);
			BGl_list2547z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2370z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_symbol2350z00zz__rgc_expandz00, BNIL));
			BGl_symbol2549z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2550z00zz__rgc_expandz00);
			BGl_list2552z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2526z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_symbol2315z00zz__rgc_expandz00, BNIL));
			BGl_list2551z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2345z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_symbol2315z00zz__rgc_expandz00,
					MAKE_YOUNG_PAIR(BGl_list2552z00zz__rgc_expandz00, BNIL)));
			BGl_list2548z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2549z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_symbol2332z00zz__rgc_expandz00,
					MAKE_YOUNG_PAIR(BGl_list2551z00zz__rgc_expandz00,
						MAKE_YOUNG_PAIR(BGl_symbol2340z00zz__rgc_expandz00,
							MAKE_YOUNG_PAIR(BGl_symbol2539z00zz__rgc_expandz00, BNIL)))));
			BGl_list2555z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2414z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_symbol2370z00zz__rgc_expandz00,
					MAKE_YOUNG_PAIR(BINT(0L), BNIL)));
			BGl_list2556z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2414z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_symbol2350z00zz__rgc_expandz00,
					MAKE_YOUNG_PAIR(BGl_symbol2370z00zz__rgc_expandz00, BNIL)));
			BGl_list2554z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2418z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_list2555z00zz__rgc_expandz00,
					MAKE_YOUNG_PAIR(BGl_list2556z00zz__rgc_expandz00, BNIL)));
			BGl_symbol2558z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2429z00zz__rgc_expandz00);
			BGl_list2557z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2558z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_symbol2370z00zz__rgc_expandz00,
					MAKE_YOUNG_PAIR(BGl_symbol2350z00zz__rgc_expandz00, BNIL)));
			BGl_list2553z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2432z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_list2554z00zz__rgc_expandz00,
					MAKE_YOUNG_PAIR(BGl_list2557z00zz__rgc_expandz00,
						MAKE_YOUNG_PAIR(BGl_string2559z00zz__rgc_expandz00, BNIL))));
			BGl_list2544z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2545z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_list2547z00zz__rgc_expandz00,
					MAKE_YOUNG_PAIR(BGl_list2548z00zz__rgc_expandz00,
						MAKE_YOUNG_PAIR(BGl_list2553z00zz__rgc_expandz00, BNIL))));
			BGl_list2541z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2432z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_list2542z00zz__rgc_expandz00,
					MAKE_YOUNG_PAIR(BGl_list2543z00zz__rgc_expandz00,
						MAKE_YOUNG_PAIR(BGl_list2544z00zz__rgc_expandz00, BNIL))));
			BGl_list2535z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2318z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_list2536z00zz__rgc_expandz00,
					MAKE_YOUNG_PAIR(BGl_list2541z00zz__rgc_expandz00, BNIL)));
			BGl_list2534z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_list2535z00zz__rgc_expandz00, BNIL);
			BGl_symbol2561z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2562z00zz__rgc_expandz00);
			BGl_symbol2563z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2564z00zz__rgc_expandz00);
			BGl_list2565z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2521z00zz__rgc_expandz00, BNIL);
			BGl_symbol2568z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2569z00zz__rgc_expandz00);
			BGl_symbol2571z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2572z00zz__rgc_expandz00);
			BGl_list2570z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2571z00zz__rgc_expandz00, BNIL);
			BGl_list2567z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2568z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_list2570z00zz__rgc_expandz00, BNIL));
			BGl_symbol2574z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2575z00zz__rgc_expandz00);
			BGl_symbol2577z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2578z00zz__rgc_expandz00);
			BGl_symbol2580z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2581z00zz__rgc_expandz00);
			BGl_list2579z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2580z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_string2302z00zz__rgc_expandz00, BNIL));
			BGl_symbol2583z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2584z00zz__rgc_expandz00);
			BGl_list2582z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2583z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_string2585z00zz__rgc_expandz00, BNIL));
			BGl_symbol2587z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2588z00zz__rgc_expandz00);
			BGl_list2586z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2587z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_list2570z00zz__rgc_expandz00, BNIL));
			BGl_list2576z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2577z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_list2579z00zz__rgc_expandz00,
					MAKE_YOUNG_PAIR(BGl_list2582z00zz__rgc_expandz00,
						MAKE_YOUNG_PAIR(BGl_list2586z00zz__rgc_expandz00, BNIL))));
			BGl_list2573z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2574z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_list2576z00zz__rgc_expandz00, BNIL));
			BGl_list2566z00zz__rgc_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2432z00zz__rgc_expandz00,
				MAKE_YOUNG_PAIR(BGl_list2567z00zz__rgc_expandz00,
					MAKE_YOUNG_PAIR(BGl_list2573z00zz__rgc_expandz00,
						MAKE_YOUNG_PAIR(BGl_list2565z00zz__rgc_expandz00, BNIL))));
			return (BGl_symbol2589z00zz__rgc_expandz00 =
				bstring_to_symbol(BGl_string2590z00zz__rgc_expandz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__rgc_expandz00(void)
	{
		{	/* Rgc/rgcexpand.scm 22 */
			return bgl_gc_roots_register();
		}

	}



/* expand-string-case */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2stringzd2casez00zz__rgc_expandz00(obj_t
		BgL_xz00_3, obj_t BgL_ez00_4)
	{
		{	/* Rgc/rgcexpand.scm 75 */
			{
				obj_t BgL_strz00_1186;
				obj_t BgL_clausesz00_1187;

				if (PAIRP(BgL_xz00_3))
					{	/* Rgc/rgcexpand.scm 76 */
						obj_t BgL_cdrzd2109zd2_1192;

						BgL_cdrzd2109zd2_1192 = CDR(((obj_t) BgL_xz00_3));
						if (PAIRP(BgL_cdrzd2109zd2_1192))
							{	/* Rgc/rgcexpand.scm 76 */
								BgL_strz00_1186 = CAR(BgL_cdrzd2109zd2_1192);
								BgL_clausesz00_1187 = CDR(BgL_cdrzd2109zd2_1192);
								{	/* Rgc/rgcexpand.scm 78 */
									obj_t BgL_portzd2idzd2_1196;

									BgL_portzd2idzd2_1196 =
										BGl_gensymz00zz__r4_symbols_6_4z00
										(BGl_symbol2297z00zz__rgc_expandz00);
									{	/* Rgc/rgcexpand.scm 79 */
										obj_t BgL_newz00_1197;

										{	/* Rgc/rgcexpand.scm 79 */
											obj_t BgL_arg1198z00_1200;

											{	/* Rgc/rgcexpand.scm 79 */
												obj_t BgL_arg1199z00_1201;
												obj_t BgL_arg1200z00_1202;

												{	/* Rgc/rgcexpand.scm 79 */
													obj_t BgL_arg1201z00_1203;

													{	/* Rgc/rgcexpand.scm 79 */
														obj_t BgL_arg1202z00_1204;

														{	/* Rgc/rgcexpand.scm 79 */
															obj_t BgL_arg1203z00_1205;

															{	/* Rgc/rgcexpand.scm 79 */
																obj_t BgL_arg1206z00_1206;

																BgL_arg1206z00_1206 =
																	MAKE_YOUNG_PAIR(BgL_strz00_1186, BNIL);
																BgL_arg1203z00_1205 =
																	MAKE_YOUNG_PAIR
																	(BGl_symbol2299z00zz__rgc_expandz00,
																	BgL_arg1206z00_1206);
															}
															BgL_arg1202z00_1204 =
																MAKE_YOUNG_PAIR(BgL_arg1203z00_1205, BNIL);
														}
														BgL_arg1201z00_1203 =
															MAKE_YOUNG_PAIR(BgL_portzd2idzd2_1196,
															BgL_arg1202z00_1204);
													}
													BgL_arg1199z00_1201 =
														MAKE_YOUNG_PAIR(BgL_arg1201z00_1203, BNIL);
												}
												{	/* Rgc/rgcexpand.scm 81 */
													obj_t BgL_arg1208z00_1207;

													{	/* Rgc/rgcexpand.scm 81 */
														obj_t BgL_arg1209z00_1208;

														{	/* Rgc/rgcexpand.scm 81 */
															obj_t BgL_arg1210z00_1209;
															obj_t BgL_arg1212z00_1210;

															{	/* Rgc/rgcexpand.scm 81 */
																obj_t BgL_arg1215z00_1211;

																{	/* Rgc/rgcexpand.scm 81 */
																	obj_t BgL_arg1216z00_1212;
																	obj_t BgL_arg1218z00_1213;

																	{	/* Rgc/rgcexpand.scm 81 */
																		obj_t BgL_arg1219z00_1214;

																		BgL_arg1219z00_1214 =
																			MAKE_YOUNG_PAIR(BNIL,
																			BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																			(BgL_clausesz00_1187, BNIL));
																		BgL_arg1216z00_1212 =
																			MAKE_YOUNG_PAIR
																			(BGl_symbol2301z00zz__rgc_expandz00,
																			BgL_arg1219z00_1214);
																	}
																	BgL_arg1218z00_1213 =
																		MAKE_YOUNG_PAIR(BgL_portzd2idzd2_1196,
																		BNIL);
																	BgL_arg1215z00_1211 =
																		MAKE_YOUNG_PAIR(BgL_arg1216z00_1212,
																		BgL_arg1218z00_1213);
																}
																BgL_arg1210z00_1209 =
																	MAKE_YOUNG_PAIR
																	(BGl_symbol2303z00zz__rgc_expandz00,
																	BgL_arg1215z00_1211);
															}
															{	/* Rgc/rgcexpand.scm 84 */
																obj_t BgL_arg1221z00_1216;

																{	/* Rgc/rgcexpand.scm 84 */
																	obj_t BgL_arg1223z00_1217;

																	BgL_arg1223z00_1217 =
																		MAKE_YOUNG_PAIR(BgL_portzd2idzd2_1196,
																		BNIL);
																	BgL_arg1221z00_1216 =
																		MAKE_YOUNG_PAIR
																		(BGl_symbol2305z00zz__rgc_expandz00,
																		BgL_arg1223z00_1217);
																}
																BgL_arg1212z00_1210 =
																	MAKE_YOUNG_PAIR(BgL_arg1221z00_1216, BNIL);
															}
															BgL_arg1209z00_1208 =
																MAKE_YOUNG_PAIR(BgL_arg1210z00_1209,
																BgL_arg1212z00_1210);
														}
														BgL_arg1208z00_1207 =
															MAKE_YOUNG_PAIR
															(BGl_symbol2307z00zz__rgc_expandz00,
															BgL_arg1209z00_1208);
													}
													BgL_arg1200z00_1202 =
														MAKE_YOUNG_PAIR(BgL_arg1208z00_1207, BNIL);
												}
												BgL_arg1198z00_1200 =
													MAKE_YOUNG_PAIR(BgL_arg1199z00_1201,
													BgL_arg1200z00_1202);
											}
											BgL_newz00_1197 =
												MAKE_YOUNG_PAIR(BGl_symbol2309z00zz__rgc_expandz00,
												BgL_arg1198z00_1200);
										}
										{	/* Rgc/rgcexpand.scm 85 */
											obj_t BgL_arg1196z00_1198;

											BgL_arg1196z00_1198 = CAR(BgL_newz00_1197);
											{	/* Rgc/rgcexpand.scm 85 */
												obj_t BgL_tmpz00_2977;

												BgL_tmpz00_2977 = ((obj_t) BgL_xz00_3);
												SET_CAR(BgL_tmpz00_2977, BgL_arg1196z00_1198);
											}
										}
										{	/* Rgc/rgcexpand.scm 86 */
											obj_t BgL_arg1197z00_1199;

											BgL_arg1197z00_1199 = CDR(BgL_newz00_1197);
											{	/* Rgc/rgcexpand.scm 86 */
												obj_t BgL_tmpz00_2981;

												BgL_tmpz00_2981 = ((obj_t) BgL_xz00_3);
												SET_CDR(BgL_tmpz00_2981, BgL_arg1197z00_1199);
											}
										}
										return
											BGL_PROCEDURE_CALL2(BgL_ez00_4, BgL_xz00_3, BgL_ez00_4);
									}
								}
							}
						else
							{	/* Rgc/rgcexpand.scm 76 */
								return
									BGl_errorz00zz__errorz00(BGl_string2311z00zz__rgc_expandz00,
									BGl_string2312z00zz__rgc_expandz00, BgL_xz00_3);
							}
					}
				else
					{	/* Rgc/rgcexpand.scm 76 */
						return
							BGl_errorz00zz__errorz00(BGl_string2311z00zz__rgc_expandz00,
							BGl_string2312z00zz__rgc_expandz00, BgL_xz00_3);
					}
			}
		}

	}



/* &expand-string-case */
	obj_t BGl_z62expandzd2stringzd2casez62zz__rgc_expandz00(obj_t BgL_envz00_2559,
		obj_t BgL_xz00_2560, obj_t BgL_ez00_2561)
	{
		{	/* Rgc/rgcexpand.scm 75 */
			return
				BGl_expandzd2stringzd2casez00zz__rgc_expandz00(BgL_xz00_2560,
				BgL_ez00_2561);
		}

	}



/* expand-regular-grammar */
	BGL_EXPORTED_DEF obj_t
		BGl_expandzd2regularzd2grammarz00zz__rgc_expandz00(obj_t BgL_xz00_5,
		obj_t BgL_ez00_6)
	{
		{	/* Rgc/rgcexpand.scm 96 */
			{
				obj_t BgL_optsz00_1243;

				{
					obj_t BgL_optsz00_1219;
					obj_t BgL_clausesz00_1220;

					if (PAIRP(BgL_xz00_5))
						{	/* Rgc/rgcexpand.scm 108 */
							obj_t BgL_cdrzd2124zd2_1225;

							BgL_cdrzd2124zd2_1225 = CDR(((obj_t) BgL_xz00_5));
							if (PAIRP(BgL_cdrzd2124zd2_1225))
								{	/* Rgc/rgcexpand.scm 108 */
									BgL_optsz00_1219 = CAR(BgL_cdrzd2124zd2_1225);
									BgL_clausesz00_1220 = CDR(BgL_cdrzd2124zd2_1225);
									{	/* Rgc/rgcexpand.scm 110 */
										obj_t BgL_uenvz00_1229;

										BgL_optsz00_1243 = BgL_optsz00_1219;
										{
											obj_t BgL_osz00_1248;
											obj_t BgL_varsz00_1249;
											obj_t BgL_argsz00_1250;

											BgL_osz00_1248 = BgL_optsz00_1243;
											BgL_varsz00_1249 = BNIL;
											BgL_argsz00_1250 = BNIL;
										BgL_zc3z04anonymousza31230ze3z87_1251:
											if (NULLP(BgL_osz00_1248))
												{	/* Rgc/rgcexpand.scm 103 */
													obj_t BgL_val0_1089z00_1253;
													obj_t BgL_val1_1090z00_1254;

													BgL_val0_1089z00_1253 =
														bgl_reverse_bang(BgL_varsz00_1249);
													BgL_val1_1090z00_1254 =
														bgl_reverse_bang(BgL_argsz00_1250);
													{	/* Rgc/rgcexpand.scm 103 */
														int BgL_tmpz00_3004;

														BgL_tmpz00_3004 = (int) (2L);
														BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3004);
													}
													{	/* Rgc/rgcexpand.scm 103 */
														int BgL_tmpz00_3007;

														BgL_tmpz00_3007 = (int) (1L);
														BGL_MVALUES_VAL_SET(BgL_tmpz00_3007,
															BgL_val1_1090z00_1254);
													}
													BgL_uenvz00_1229 = BgL_val0_1089z00_1253;
												}
											else
												{	/* Rgc/rgcexpand.scm 104 */
													bool_t BgL_test2730z00_3010;

													{	/* Rgc/rgcexpand.scm 104 */
														obj_t BgL_tmpz00_3011;

														BgL_tmpz00_3011 = CAR(((obj_t) BgL_osz00_1248));
														BgL_test2730z00_3010 = SYMBOLP(BgL_tmpz00_3011);
													}
													if (BgL_test2730z00_3010)
														{	/* Rgc/rgcexpand.scm 105 */
															obj_t BgL_arg1234z00_1257;
															obj_t BgL_arg1236z00_1258;

															BgL_arg1234z00_1257 =
																CDR(((obj_t) BgL_osz00_1248));
															{	/* Rgc/rgcexpand.scm 105 */
																obj_t BgL_arg1238z00_1259;

																BgL_arg1238z00_1259 =
																	CAR(((obj_t) BgL_osz00_1248));
																BgL_arg1236z00_1258 =
																	MAKE_YOUNG_PAIR(BgL_arg1238z00_1259,
																	BgL_argsz00_1250);
															}
															{
																obj_t BgL_argsz00_3021;
																obj_t BgL_osz00_3020;

																BgL_osz00_3020 = BgL_arg1234z00_1257;
																BgL_argsz00_3021 = BgL_arg1236z00_1258;
																BgL_argsz00_1250 = BgL_argsz00_3021;
																BgL_osz00_1248 = BgL_osz00_3020;
																goto BgL_zc3z04anonymousza31230ze3z87_1251;
															}
														}
													else
														{	/* Rgc/rgcexpand.scm 107 */
															obj_t BgL_arg1239z00_1260;
															obj_t BgL_arg1242z00_1261;

															BgL_arg1239z00_1260 =
																CDR(((obj_t) BgL_osz00_1248));
															{	/* Rgc/rgcexpand.scm 107 */
																obj_t BgL_arg1244z00_1262;

																BgL_arg1244z00_1262 =
																	CAR(((obj_t) BgL_osz00_1248));
																BgL_arg1242z00_1261 =
																	MAKE_YOUNG_PAIR(BgL_arg1244z00_1262,
																	BgL_varsz00_1249);
															}
															{
																obj_t BgL_varsz00_3028;
																obj_t BgL_osz00_3027;

																BgL_osz00_3027 = BgL_arg1239z00_1260;
																BgL_varsz00_3028 = BgL_arg1242z00_1261;
																BgL_varsz00_1249 = BgL_varsz00_3028;
																BgL_osz00_1248 = BgL_osz00_3027;
																goto BgL_zc3z04anonymousza31230ze3z87_1251;
															}
														}
												}
										}
										{	/* Rgc/rgcexpand.scm 111 */
											obj_t BgL_argsz00_1230;

											{	/* Rgc/rgcexpand.scm 112 */
												obj_t BgL_tmpz00_2285;

												{	/* Rgc/rgcexpand.scm 112 */
													int BgL_tmpz00_3029;

													BgL_tmpz00_3029 = (int) (1L);
													BgL_tmpz00_2285 = BGL_MVALUES_VAL(BgL_tmpz00_3029);
												}
												{	/* Rgc/rgcexpand.scm 112 */
													int BgL_tmpz00_3032;

													BgL_tmpz00_3032 = (int) (1L);
													BGL_MVALUES_VAL_SET(BgL_tmpz00_3032, BUNSPEC);
												}
												BgL_argsz00_1230 = BgL_tmpz00_2285;
											}
											{	/* Rgc/rgcexpand.scm 112 */
												obj_t BgL_treez00_1231;

												BgL_treez00_1231 =
													BGl_ruleszd2ze3regularzd2treeze3zz__rgc_rulesz00
													(BgL_uenvz00_1229, BgL_clausesz00_1220);
												{	/* Rgc/rgcexpand.scm 115 */
													obj_t BgL_actionsz00_1232;
													obj_t BgL_elsezd2numzd2_1233;
													obj_t BgL_submatchzf3zf3_1234;
													obj_t BgL_defsz00_1235;

													{	/* Rgc/rgcexpand.scm 116 */
														obj_t BgL_tmpz00_2286;

														{	/* Rgc/rgcexpand.scm 116 */
															int BgL_tmpz00_3036;

															BgL_tmpz00_3036 = (int) (1L);
															BgL_tmpz00_2286 =
																BGL_MVALUES_VAL(BgL_tmpz00_3036);
														}
														{	/* Rgc/rgcexpand.scm 116 */
															int BgL_tmpz00_3039;

															BgL_tmpz00_3039 = (int) (1L);
															BGL_MVALUES_VAL_SET(BgL_tmpz00_3039, BUNSPEC);
														}
														BgL_actionsz00_1232 = BgL_tmpz00_2286;
													}
													{	/* Rgc/rgcexpand.scm 116 */
														obj_t BgL_tmpz00_2287;

														{	/* Rgc/rgcexpand.scm 116 */
															int BgL_tmpz00_3042;

															BgL_tmpz00_3042 = (int) (2L);
															BgL_tmpz00_2287 =
																BGL_MVALUES_VAL(BgL_tmpz00_3042);
														}
														{	/* Rgc/rgcexpand.scm 116 */
															int BgL_tmpz00_3045;

															BgL_tmpz00_3045 = (int) (2L);
															BGL_MVALUES_VAL_SET(BgL_tmpz00_3045, BUNSPEC);
														}
														BgL_elsezd2numzd2_1233 = BgL_tmpz00_2287;
													}
													{	/* Rgc/rgcexpand.scm 116 */
														obj_t BgL_tmpz00_2288;

														{	/* Rgc/rgcexpand.scm 116 */
															int BgL_tmpz00_3048;

															BgL_tmpz00_3048 = (int) (3L);
															BgL_tmpz00_2288 =
																BGL_MVALUES_VAL(BgL_tmpz00_3048);
														}
														{	/* Rgc/rgcexpand.scm 116 */
															int BgL_tmpz00_3051;

															BgL_tmpz00_3051 = (int) (3L);
															BGL_MVALUES_VAL_SET(BgL_tmpz00_3051, BUNSPEC);
														}
														BgL_submatchzf3zf3_1234 = BgL_tmpz00_2288;
													}
													{	/* Rgc/rgcexpand.scm 116 */
														obj_t BgL_tmpz00_2289;

														{	/* Rgc/rgcexpand.scm 116 */
															int BgL_tmpz00_3054;

															BgL_tmpz00_3054 = (int) (4L);
															BgL_tmpz00_2289 =
																BGL_MVALUES_VAL(BgL_tmpz00_3054);
														}
														{	/* Rgc/rgcexpand.scm 116 */
															int BgL_tmpz00_3057;

															BgL_tmpz00_3057 = (int) (4L);
															BGL_MVALUES_VAL_SET(BgL_tmpz00_3057, BUNSPEC);
														}
														BgL_defsz00_1235 = BgL_tmpz00_2289;
													}
													{	/* Rgc/rgcexpand.scm 116 */
														obj_t BgL_nodez00_1236;

														BgL_nodez00_1236 =
															BGl_regularzd2treezd2ze3nodeze3zz__rgc_treez00
															(BgL_treez00_1231);
														{	/* Rgc/rgcexpand.scm 120 */
															obj_t BgL_followposz00_1237;
															obj_t BgL_positionsz00_1238;
															obj_t BgL_submatchesz00_1239;

															{	/* Rgc/rgcexpand.scm 123 */
																obj_t BgL_tmpz00_2290;

																{	/* Rgc/rgcexpand.scm 123 */
																	int BgL_tmpz00_3061;

																	BgL_tmpz00_3061 = (int) (1L);
																	BgL_tmpz00_2290 =
																		BGL_MVALUES_VAL(BgL_tmpz00_3061);
																}
																{	/* Rgc/rgcexpand.scm 123 */
																	int BgL_tmpz00_3064;

																	BgL_tmpz00_3064 = (int) (1L);
																	BGL_MVALUES_VAL_SET(BgL_tmpz00_3064, BUNSPEC);
																}
																BgL_followposz00_1237 = BgL_tmpz00_2290;
															}
															{	/* Rgc/rgcexpand.scm 123 */
																obj_t BgL_tmpz00_2291;

																{	/* Rgc/rgcexpand.scm 123 */
																	int BgL_tmpz00_3067;

																	BgL_tmpz00_3067 = (int) (2L);
																	BgL_tmpz00_2291 =
																		BGL_MVALUES_VAL(BgL_tmpz00_3067);
																}
																{	/* Rgc/rgcexpand.scm 123 */
																	int BgL_tmpz00_3070;

																	BgL_tmpz00_3070 = (int) (2L);
																	BGL_MVALUES_VAL_SET(BgL_tmpz00_3070, BUNSPEC);
																}
																BgL_positionsz00_1238 = BgL_tmpz00_2291;
															}
															{	/* Rgc/rgcexpand.scm 123 */
																obj_t BgL_tmpz00_2292;

																{	/* Rgc/rgcexpand.scm 123 */
																	int BgL_tmpz00_3073;

																	BgL_tmpz00_3073 = (int) (3L);
																	BgL_tmpz00_2292 =
																		BGL_MVALUES_VAL(BgL_tmpz00_3073);
																}
																{	/* Rgc/rgcexpand.scm 123 */
																	int BgL_tmpz00_3076;

																	BgL_tmpz00_3076 = (int) (3L);
																	BGL_MVALUES_VAL_SET(BgL_tmpz00_3076, BUNSPEC);
																}
																BgL_submatchesz00_1239 = BgL_tmpz00_2292;
															}
															{	/* Rgc/rgcexpand.scm 123 */
																obj_t BgL_dfaz00_1240;

																BgL_dfaz00_1240 =
																	BGl_nodezd2ze3dfaz31zz__rgc_dfaz00
																	(BgL_nodez00_1236, BgL_followposz00_1237,
																	BgL_positionsz00_1238);
																{	/* Rgc/rgcexpand.scm 123 */
																	obj_t BgL_sexpz00_1241;

																	BgL_sexpz00_1241 =
																		BGl_makezd2regularzd2parserz00zz__rgc_expandz00
																		(BgL_argsz00_1230,
																		BGl_compilezd2dfazd2zz__rgc_compilez00
																		(BgL_submatchesz00_1239, BgL_dfaz00_1240,
																			BgL_positionsz00_1238),
																		BgL_actionsz00_1232, BgL_elsezd2numzd2_1233,
																		BgL_submatchzf3zf3_1234, BgL_defsz00_1235);
																	{	/* Rgc/rgcexpand.scm 124 */

																		BGl_resetzd2specialzd2matchzd2charz12zc0zz__rgc_rulesz00
																			();
																		BGl_resetzd2treez12zc0zz__rgc_treez00();
																		BGl_resetzd2dfaz12zc0zz__rgc_dfaz00();
																		return
																			BGL_PROCEDURE_CALL2(BgL_ez00_6,
																			BgL_sexpz00_1241, BgL_ez00_6);
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							else
								{	/* Rgc/rgcexpand.scm 108 */
									return
										BGl_errorz00zz__errorz00(BGl_string2302z00zz__rgc_expandz00,
										BGl_string2312z00zz__rgc_expandz00, BgL_xz00_5);
								}
						}
					else
						{	/* Rgc/rgcexpand.scm 108 */
							return
								BGl_errorz00zz__errorz00(BGl_string2302z00zz__rgc_expandz00,
								BGl_string2312z00zz__rgc_expandz00, BgL_xz00_5);
						}
				}
			}
		}

	}



/* &expand-regular-grammar */
	obj_t BGl_z62expandzd2regularzd2grammarz62zz__rgc_expandz00(obj_t
		BgL_envz00_2562, obj_t BgL_xz00_2563, obj_t BgL_ez00_2564)
	{
		{	/* Rgc/rgcexpand.scm 96 */
			return
				BGl_expandzd2regularzd2grammarz00zz__rgc_expandz00(BgL_xz00_2563,
				BgL_ez00_2564);
		}

	}



/* make-regular-parser */
	obj_t BGl_makezd2regularzd2parserz00zz__rgc_expandz00(obj_t BgL_argsz00_7,
		obj_t BgL_statesz00_8, obj_t BgL_actionsz00_9, obj_t BgL_elsezd2numzd2_10,
		obj_t BgL_submatchzf3zf3_11, obj_t BgL_defsz00_12)
	{
		{	/* Rgc/rgcexpand.scm 141 */
			{	/* Rgc/rgcexpand.scm 142 */
				obj_t BgL_arg1249z00_1266;

				{	/* Rgc/rgcexpand.scm 142 */
					obj_t BgL_arg1252z00_1267;
					obj_t BgL_arg1268z00_1268;

					{	/* Rgc/rgcexpand.scm 142 */
						obj_t BgL_arg1272z00_1269;

						{	/* Rgc/rgcexpand.scm 142 */
							obj_t BgL_arg1284z00_1270;

							BgL_arg1284z00_1270 = MAKE_YOUNG_PAIR(BUNSPEC, BNIL);
							BgL_arg1272z00_1269 =
								MAKE_YOUNG_PAIR(BGl_symbol2313z00zz__rgc_expandz00,
								BgL_arg1284z00_1270);
						}
						BgL_arg1252z00_1267 = MAKE_YOUNG_PAIR(BgL_arg1272z00_1269, BNIL);
					}
					{	/* Rgc/rgcexpand.scm 143 */
						obj_t BgL_arg1304z00_1271;

						{	/* Rgc/rgcexpand.scm 143 */
							obj_t BgL_arg1305z00_1272;

							{	/* Rgc/rgcexpand.scm 143 */
								obj_t BgL_arg1306z00_1273;
								obj_t BgL_arg1307z00_1274;

								{	/* Rgc/rgcexpand.scm 143 */
									obj_t BgL_arg1308z00_1275;

									BgL_arg1308z00_1275 =
										BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
										(BgL_argsz00_7, BNIL);
									BgL_arg1306z00_1273 =
										MAKE_YOUNG_PAIR(BGl_symbol2315z00zz__rgc_expandz00,
										BgL_arg1308z00_1275);
								}
								{	/* Rgc/rgcexpand.scm 145 */
									obj_t BgL_arg1309z00_1276;
									obj_t BgL_arg1310z00_1277;

									if (CBOOL(BgL_submatchzf3zf3_11))
										{	/* Rgc/rgcexpand.scm 146 */
											obj_t BgL_list1311z00_1278;

											{	/* Rgc/rgcexpand.scm 146 */
												obj_t BgL_arg1312z00_1279;

												{	/* Rgc/rgcexpand.scm 146 */
													obj_t BgL_arg1314z00_1280;

													{	/* Rgc/rgcexpand.scm 146 */
														obj_t BgL_arg1315z00_1281;

														BgL_arg1315z00_1281 =
															MAKE_YOUNG_PAIR(BGl_list2317z00zz__rgc_expandz00,
															BNIL);
														BgL_arg1314z00_1280 =
															MAKE_YOUNG_PAIR(BGl_list2352z00zz__rgc_expandz00,
															BgL_arg1315z00_1281);
													}
													BgL_arg1312z00_1279 =
														MAKE_YOUNG_PAIR(BGl_list2362z00zz__rgc_expandz00,
														BgL_arg1314z00_1280);
												}
												BgL_list1311z00_1278 =
													MAKE_YOUNG_PAIR(BGl_list2372z00zz__rgc_expandz00,
													BgL_arg1312z00_1279);
											}
											BgL_arg1309z00_1276 = BgL_list1311z00_1278;
										}
									else
										{	/* Rgc/rgcexpand.scm 145 */
											BgL_arg1309z00_1276 = BNIL;
										}
									{	/* Rgc/rgcexpand.scm 173 */
										obj_t BgL_arg1316z00_1282;

										{	/* Rgc/rgcexpand.scm 173 */
											obj_t BgL_arg1317z00_1283;
											obj_t BgL_arg1318z00_1284;

											{	/* Rgc/rgcexpand.scm 173 */
												obj_t BgL_arg1319z00_1285;

												{	/* Rgc/rgcexpand.scm 173 */
													obj_t BgL_arg1320z00_1286;
													obj_t BgL_arg1321z00_1287;

													BgL_arg1320z00_1286 =
														MAKE_YOUNG_PAIR(BGl_symbol2374z00zz__rgc_expandz00,
														BNIL);
													BgL_arg1321z00_1287 =
														MAKE_YOUNG_PAIR(BGl_symbol2315z00zz__rgc_expandz00,
														BNIL);
													BgL_arg1319z00_1285 =
														MAKE_YOUNG_PAIR(BgL_arg1320z00_1286,
														BgL_arg1321z00_1287);
												}
												BgL_arg1317z00_1283 =
													MAKE_YOUNG_PAIR(BGl_symbol2318z00zz__rgc_expandz00,
													BgL_arg1319z00_1285);
											}
											{	/* Rgc/rgcexpand.scm 176 */
												obj_t BgL_arg1322z00_1288;
												obj_t BgL_arg1323z00_1289;

												{	/* Rgc/rgcexpand.scm 176 */
													obj_t BgL_arg1325z00_1290;

													{	/* Rgc/rgcexpand.scm 176 */
														obj_t BgL_arg1326z00_1291;
														obj_t BgL_arg1327z00_1292;

														BgL_arg1326z00_1291 =
															MAKE_YOUNG_PAIR
															(BGl_symbol2376z00zz__rgc_expandz00, BNIL);
														{	/* Rgc/rgcexpand.scm 177 */
															obj_t BgL_arg1328z00_1293;

															{	/* Rgc/rgcexpand.scm 177 */
																obj_t BgL_arg1329z00_1294;

																BgL_arg1329z00_1294 =
																	MAKE_YOUNG_PAIR
																	(BGl_symbol2315z00zz__rgc_expandz00, BNIL);
																BgL_arg1328z00_1293 =
																	MAKE_YOUNG_PAIR
																	(BGl_symbol2378z00zz__rgc_expandz00,
																	BgL_arg1329z00_1294);
															}
															BgL_arg1327z00_1292 =
																MAKE_YOUNG_PAIR(BgL_arg1328z00_1293, BNIL);
														}
														BgL_arg1325z00_1290 =
															MAKE_YOUNG_PAIR(BgL_arg1326z00_1291,
															BgL_arg1327z00_1292);
													}
													BgL_arg1322z00_1288 =
														MAKE_YOUNG_PAIR(BGl_symbol2318z00zz__rgc_expandz00,
														BgL_arg1325z00_1290);
												}
												{	/* Rgc/rgcexpand.scm 179 */
													obj_t BgL_arg1331z00_1295;
													obj_t BgL_arg1332z00_1296;

													{	/* Rgc/rgcexpand.scm 179 */
														obj_t BgL_arg1333z00_1297;

														{	/* Rgc/rgcexpand.scm 179 */
															obj_t BgL_arg1334z00_1298;
															obj_t BgL_arg1335z00_1299;

															BgL_arg1334z00_1298 =
																MAKE_YOUNG_PAIR
																(BGl_symbol2380z00zz__rgc_expandz00, BNIL);
															{	/* Rgc/rgcexpand.scm 180 */
																obj_t BgL_arg1336z00_1300;

																{	/* Rgc/rgcexpand.scm 180 */
																	obj_t BgL_arg1337z00_1301;

																	BgL_arg1337z00_1301 =
																		MAKE_YOUNG_PAIR
																		(BGl_symbol2315z00zz__rgc_expandz00, BNIL);
																	BgL_arg1336z00_1300 =
																		MAKE_YOUNG_PAIR
																		(BGl_symbol2382z00zz__rgc_expandz00,
																		BgL_arg1337z00_1301);
																}
																BgL_arg1335z00_1299 =
																	MAKE_YOUNG_PAIR(BgL_arg1336z00_1300, BNIL);
															}
															BgL_arg1333z00_1297 =
																MAKE_YOUNG_PAIR(BgL_arg1334z00_1298,
																BgL_arg1335z00_1299);
														}
														BgL_arg1331z00_1295 =
															MAKE_YOUNG_PAIR
															(BGl_symbol2318z00zz__rgc_expandz00,
															BgL_arg1333z00_1297);
													}
													{	/* Rgc/rgcexpand.scm 182 */
														obj_t BgL_arg1338z00_1302;
														obj_t BgL_arg1339z00_1303;

														{	/* Rgc/rgcexpand.scm 182 */
															obj_t BgL_arg1340z00_1304;

															{	/* Rgc/rgcexpand.scm 182 */
																obj_t BgL_arg1341z00_1305;
																obj_t BgL_arg1342z00_1306;

																{	/* Rgc/rgcexpand.scm 182 */
																	obj_t BgL_arg1343z00_1307;

																	BgL_arg1343z00_1307 =
																		MAKE_YOUNG_PAIR
																		(BGl_symbol2384z00zz__rgc_expandz00, BNIL);
																	BgL_arg1341z00_1305 =
																		MAKE_YOUNG_PAIR
																		(BGl_symbol2386z00zz__rgc_expandz00,
																		BgL_arg1343z00_1307);
																}
																{	/* Rgc/rgcexpand.scm 183 */
																	obj_t BgL_arg1344z00_1308;

																	{	/* Rgc/rgcexpand.scm 183 */
																		obj_t BgL_arg1346z00_1309;

																		{	/* Rgc/rgcexpand.scm 183 */
																			obj_t BgL_arg1347z00_1310;

																			BgL_arg1347z00_1310 =
																				MAKE_YOUNG_PAIR
																				(BGl_symbol2388z00zz__rgc_expandz00,
																				BNIL);
																			BgL_arg1346z00_1309 =
																				MAKE_YOUNG_PAIR
																				(BGl_symbol2315z00zz__rgc_expandz00,
																				BgL_arg1347z00_1310);
																		}
																		BgL_arg1344z00_1308 =
																			MAKE_YOUNG_PAIR
																			(BGl_symbol2390z00zz__rgc_expandz00,
																			BgL_arg1346z00_1309);
																	}
																	BgL_arg1342z00_1306 =
																		MAKE_YOUNG_PAIR(BgL_arg1344z00_1308, BNIL);
																}
																BgL_arg1340z00_1304 =
																	MAKE_YOUNG_PAIR(BgL_arg1341z00_1305,
																	BgL_arg1342z00_1306);
															}
															BgL_arg1338z00_1302 =
																MAKE_YOUNG_PAIR
																(BGl_symbol2318z00zz__rgc_expandz00,
																BgL_arg1340z00_1304);
														}
														{	/* Rgc/rgcexpand.scm 185 */
															obj_t BgL_arg1348z00_1311;
															obj_t BgL_arg1349z00_1312;

															{	/* Rgc/rgcexpand.scm 185 */
																obj_t BgL_arg1350z00_1313;

																{	/* Rgc/rgcexpand.scm 185 */
																	obj_t BgL_arg1351z00_1314;
																	obj_t BgL_arg1352z00_1315;

																	BgL_arg1351z00_1314 =
																		MAKE_YOUNG_PAIR
																		(BGl_symbol2392z00zz__rgc_expandz00, BNIL);
																	{	/* Rgc/rgcexpand.scm 186 */
																		obj_t BgL_arg1354z00_1316;

																		{	/* Rgc/rgcexpand.scm 186 */
																			obj_t BgL_arg1356z00_1317;

																			{	/* Rgc/rgcexpand.scm 186 */
																				obj_t BgL_arg1357z00_1318;

																				{	/* Rgc/rgcexpand.scm 186 */
																					obj_t BgL_arg1358z00_1319;

																					{	/* Rgc/rgcexpand.scm 186 */
																						obj_t BgL_arg1359z00_1320;

																						BgL_arg1359z00_1320 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol2394z00zz__rgc_expandz00,
																							BNIL);
																						BgL_arg1358z00_1319 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1359z00_1320, BNIL);
																					}
																					BgL_arg1357z00_1318 =
																						MAKE_YOUNG_PAIR(BINT(0L),
																						BgL_arg1358z00_1319);
																				}
																				BgL_arg1356z00_1317 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol2315z00zz__rgc_expandz00,
																					BgL_arg1357z00_1318);
																			}
																			BgL_arg1354z00_1316 =
																				MAKE_YOUNG_PAIR
																				(BGl_symbol2396z00zz__rgc_expandz00,
																				BgL_arg1356z00_1317);
																		}
																		BgL_arg1352z00_1315 =
																			MAKE_YOUNG_PAIR(BgL_arg1354z00_1316,
																			BNIL);
																	}
																	BgL_arg1350z00_1313 =
																		MAKE_YOUNG_PAIR(BgL_arg1351z00_1314,
																		BgL_arg1352z00_1315);
																}
																BgL_arg1348z00_1311 =
																	MAKE_YOUNG_PAIR
																	(BGl_symbol2318z00zz__rgc_expandz00,
																	BgL_arg1350z00_1313);
															}
															{	/* Rgc/rgcexpand.scm 188 */
																obj_t BgL_arg1360z00_1321;
																obj_t BgL_arg1361z00_1322;

																{	/* Rgc/rgcexpand.scm 188 */
																	obj_t BgL_arg1362z00_1323;

																	{	/* Rgc/rgcexpand.scm 188 */
																		obj_t BgL_arg1363z00_1324;
																		obj_t BgL_arg1364z00_1325;

																		{	/* Rgc/rgcexpand.scm 188 */
																			obj_t BgL_arg1365z00_1326;

																			{	/* Rgc/rgcexpand.scm 188 */
																				obj_t BgL_arg1366z00_1327;

																				BgL_arg1366z00_1327 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol2398z00zz__rgc_expandz00,
																					BNIL);
																				BgL_arg1365z00_1326 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol2400z00zz__rgc_expandz00,
																					BgL_arg1366z00_1327);
																			}
																			BgL_arg1363z00_1324 =
																				MAKE_YOUNG_PAIR
																				(BGl_symbol2402z00zz__rgc_expandz00,
																				BgL_arg1365z00_1326);
																		}
																		{	/* Rgc/rgcexpand.scm 189 */
																			obj_t BgL_arg1367z00_1328;
																			obj_t BgL_arg1368z00_1329;

																			{	/* Rgc/rgcexpand.scm 189 */
																				obj_t BgL_arg1369z00_1330;

																				{	/* Rgc/rgcexpand.scm 189 */
																					obj_t BgL_arg1370z00_1331;
																					obj_t BgL_arg1371z00_1332;

																					{	/* Rgc/rgcexpand.scm 189 */
																						obj_t BgL_arg1372z00_1333;

																						{	/* Rgc/rgcexpand.scm 189 */
																							obj_t BgL_arg1373z00_1334;

																							BgL_arg1373z00_1334 =
																								MAKE_YOUNG_PAIR
																								(BGl_symbol2404z00zz__rgc_expandz00,
																								BNIL);
																							BgL_arg1372z00_1333 =
																								MAKE_YOUNG_PAIR
																								(BGl_symbol2406z00zz__rgc_expandz00,
																								BgL_arg1373z00_1334);
																						}
																						BgL_arg1370z00_1331 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol2408z00zz__rgc_expandz00,
																							BgL_arg1372z00_1333);
																					}
																					{	/* Rgc/rgcexpand.scm 189 */
																						obj_t BgL_arg1375z00_1335;

																						{	/* Rgc/rgcexpand.scm 189 */
																							obj_t BgL_arg1376z00_1336;

																							{	/* Rgc/rgcexpand.scm 189 */
																								obj_t BgL_arg1377z00_1337;

																								{	/* Rgc/rgcexpand.scm 189 */
																									obj_t BgL_arg1378z00_1338;

																									{	/* Rgc/rgcexpand.scm 189 */
																										obj_t BgL_arg1379z00_1339;

																										{	/* Rgc/rgcexpand.scm 189 */
																											obj_t BgL_arg1380z00_1340;
																											obj_t BgL_arg1382z00_1341;

																											BgL_arg1380z00_1340 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol2394z00zz__rgc_expandz00,
																												BNIL);
																											BgL_arg1382z00_1341 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol2406z00zz__rgc_expandz00,
																												BNIL);
																											BgL_arg1379z00_1339 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1380z00_1340,
																												BgL_arg1382z00_1341);
																										}
																										BgL_arg1378z00_1338 =
																											MAKE_YOUNG_PAIR
																											(BGl_symbol2410z00zz__rgc_expandz00,
																											BgL_arg1379z00_1339);
																									}
																									BgL_arg1377z00_1337 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1378z00_1338, BNIL);
																								}
																								BgL_arg1376z00_1336 =
																									MAKE_YOUNG_PAIR
																									(BGl_symbol2406z00zz__rgc_expandz00,
																									BgL_arg1377z00_1337);
																							}
																							BgL_arg1375z00_1335 =
																								MAKE_YOUNG_PAIR
																								(BGl_symbol2330z00zz__rgc_expandz00,
																								BgL_arg1376z00_1336);
																						}
																						BgL_arg1371z00_1332 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1375z00_1335, BNIL);
																					}
																					BgL_arg1369z00_1330 =
																						MAKE_YOUNG_PAIR(BgL_arg1370z00_1331,
																						BgL_arg1371z00_1332);
																				}
																				BgL_arg1367z00_1328 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol2412z00zz__rgc_expandz00,
																					BgL_arg1369z00_1330);
																			}
																			{	/* Rgc/rgcexpand.scm 190 */
																				obj_t BgL_arg1383z00_1342;

																				{	/* Rgc/rgcexpand.scm 190 */
																					obj_t BgL_arg1384z00_1343;

																					{	/* Rgc/rgcexpand.scm 190 */
																						obj_t BgL_arg1387z00_1344;
																						obj_t BgL_arg1388z00_1345;

																						{	/* Rgc/rgcexpand.scm 190 */
																							obj_t BgL_arg1389z00_1346;

																							{	/* Rgc/rgcexpand.scm 190 */
																								obj_t BgL_arg1390z00_1347;
																								obj_t BgL_arg1391z00_1348;

																								{	/* Rgc/rgcexpand.scm 190 */
																									obj_t BgL_arg1392z00_1349;

																									{	/* Rgc/rgcexpand.scm 190 */
																										obj_t BgL_arg1393z00_1350;

																										BgL_arg1393z00_1350 =
																											MAKE_YOUNG_PAIR(BINT(0L),
																											BNIL);
																										BgL_arg1392z00_1349 =
																											MAKE_YOUNG_PAIR
																											(BGl_symbol2404z00zz__rgc_expandz00,
																											BgL_arg1393z00_1350);
																									}
																									BgL_arg1390z00_1347 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2414z00zz__rgc_expandz00,
																										BgL_arg1392z00_1349);
																								}
																								{	/* Rgc/rgcexpand.scm 190 */
																									obj_t BgL_arg1394z00_1351;
																									obj_t BgL_arg1395z00_1352;

																									{	/* Rgc/rgcexpand.scm 190 */
																										obj_t BgL_arg1396z00_1353;

																										{	/* Rgc/rgcexpand.scm 190 */
																											obj_t BgL_arg1397z00_1354;

																											BgL_arg1397z00_1354 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol2404z00zz__rgc_expandz00,
																												BNIL);
																											BgL_arg1396z00_1353 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol2406z00zz__rgc_expandz00,
																												BgL_arg1397z00_1354);
																										}
																										BgL_arg1394z00_1351 =
																											MAKE_YOUNG_PAIR
																											(BGl_symbol2414z00zz__rgc_expandz00,
																											BgL_arg1396z00_1353);
																									}
																									{	/* Rgc/rgcexpand.scm 190 */
																										obj_t BgL_arg1399z00_1355;

																										{	/* Rgc/rgcexpand.scm 190 */
																											obj_t BgL_arg1400z00_1356;

																											{	/* Rgc/rgcexpand.scm 190 */
																												obj_t
																													BgL_arg1401z00_1357;
																												{	/* Rgc/rgcexpand.scm 190 */
																													obj_t
																														BgL_arg1402z00_1358;
																													BgL_arg1402z00_1358 =
																														MAKE_YOUNG_PAIR
																														(BGl_symbol2394z00zz__rgc_expandz00,
																														BNIL);
																													BgL_arg1401z00_1357 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1402z00_1358,
																														BNIL);
																												}
																												BgL_arg1400z00_1356 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol2406z00zz__rgc_expandz00,
																													BgL_arg1401z00_1357);
																											}
																											BgL_arg1399z00_1355 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol2416z00zz__rgc_expandz00,
																												BgL_arg1400z00_1356);
																										}
																										BgL_arg1395z00_1352 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1399z00_1355,
																											BNIL);
																									}
																									BgL_arg1391z00_1348 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1394z00_1351,
																										BgL_arg1395z00_1352);
																								}
																								BgL_arg1389z00_1346 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1390z00_1347,
																									BgL_arg1391z00_1348);
																							}
																							BgL_arg1387z00_1344 =
																								MAKE_YOUNG_PAIR
																								(BGl_symbol2418z00zz__rgc_expandz00,
																								BgL_arg1389z00_1346);
																						}
																						{	/* Rgc/rgcexpand.scm 191 */
																							obj_t BgL_arg1403z00_1359;
																							obj_t BgL_arg1404z00_1360;

																							{	/* Rgc/rgcexpand.scm 191 */
																								obj_t BgL_arg1405z00_1361;

																								{	/* Rgc/rgcexpand.scm 191 */
																									obj_t BgL_arg1406z00_1362;

																									{	/* Rgc/rgcexpand.scm 191 */
																										obj_t BgL_arg1407z00_1363;

																										BgL_arg1407z00_1363 =
																											MAKE_YOUNG_PAIR
																											(BGl_symbol2406z00zz__rgc_expandz00,
																											BNIL);
																										BgL_arg1406z00_1362 =
																											MAKE_YOUNG_PAIR
																											(BGl_symbol2404z00zz__rgc_expandz00,
																											BgL_arg1407z00_1363);
																									}
																									BgL_arg1405z00_1361 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2315z00zz__rgc_expandz00,
																										BgL_arg1406z00_1362);
																								}
																								BgL_arg1403z00_1359 =
																									MAKE_YOUNG_PAIR
																									(BGl_symbol2396z00zz__rgc_expandz00,
																									BgL_arg1405z00_1361);
																							}
																							{	/* Rgc/rgcexpand.scm 193 */
																								obj_t BgL_arg1408z00_1364;

																								{	/* Rgc/rgcexpand.scm 193 */
																									obj_t BgL_arg1410z00_1365;

																									{	/* Rgc/rgcexpand.scm 193 */
																										obj_t BgL_arg1411z00_1366;

																										{	/* Rgc/rgcexpand.scm 193 */
																											obj_t BgL_arg1412z00_1367;
																											obj_t BgL_arg1413z00_1368;

																											{	/* Rgc/rgcexpand.scm 193 */
																												obj_t
																													BgL_arg1414z00_1369;
																												obj_t
																													BgL_arg1415z00_1370;
																												{	/* Rgc/rgcexpand.scm 193 */
																													obj_t
																														BgL_arg1416z00_1371;
																													{	/* Rgc/rgcexpand.scm 193 */
																														obj_t
																															BgL_arg1417z00_1372;
																														BgL_arg1417z00_1372
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_symbol2420z00zz__rgc_expandz00,
																															BNIL);
																														BgL_arg1416z00_1371
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_symbol2422z00zz__rgc_expandz00,
																															BgL_arg1417z00_1372);
																													}
																													BgL_arg1414z00_1369 =
																														MAKE_YOUNG_PAIR
																														(BGl_symbol2424z00zz__rgc_expandz00,
																														BgL_arg1416z00_1371);
																												}
																												{	/* Rgc/rgcexpand.scm 194 */
																													obj_t
																														BgL_arg1418z00_1373;
																													{	/* Rgc/rgcexpand.scm 194 */
																														obj_t
																															BgL_arg1419z00_1374;
																														BgL_arg1419z00_1374
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_symbol2426z00zz__rgc_expandz00,
																															BNIL);
																														BgL_arg1418z00_1373
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1419z00_1374,
																															BNIL);
																													}
																													BgL_arg1415z00_1370 =
																														MAKE_YOUNG_PAIR
																														(BGl_string2428z00zz__rgc_expandz00,
																														BgL_arg1418z00_1373);
																												}
																												BgL_arg1412z00_1367 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1414z00_1369,
																													BgL_arg1415z00_1370);
																											}
																											{	/* Rgc/rgcexpand.scm 195 */
																												obj_t
																													BgL_arg1420z00_1375;
																												{	/* Rgc/rgcexpand.scm 195 */
																													obj_t
																														BgL_arg1421z00_1376;
																													{	/* Rgc/rgcexpand.scm 195 */
																														obj_t
																															BgL_arg1422z00_1377;
																														BgL_arg1422z00_1377
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_symbol2406z00zz__rgc_expandz00,
																															BNIL);
																														BgL_arg1421z00_1376
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_symbol2404z00zz__rgc_expandz00,
																															BgL_arg1422z00_1377);
																													}
																													BgL_arg1420z00_1375 =
																														MAKE_YOUNG_PAIR
																														(BGl_symbol2335z00zz__rgc_expandz00,
																														BgL_arg1421z00_1376);
																												}
																												BgL_arg1413z00_1368 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1420z00_1375,
																													BNIL);
																											}
																											BgL_arg1411z00_1366 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1412z00_1367,
																												BgL_arg1413z00_1368);
																										}
																										BgL_arg1410z00_1365 =
																											MAKE_YOUNG_PAIR
																											(BGl_string2429z00zz__rgc_expandz00,
																											BgL_arg1411z00_1366);
																									}
																									BgL_arg1408z00_1364 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2430z00zz__rgc_expandz00,
																										BgL_arg1410z00_1365);
																								}
																								BgL_arg1404z00_1360 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1408z00_1364, BNIL);
																							}
																							BgL_arg1388z00_1345 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1403z00_1359,
																								BgL_arg1404z00_1360);
																						}
																						BgL_arg1384z00_1343 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1387z00_1344,
																							BgL_arg1388z00_1345);
																					}
																					BgL_arg1383z00_1342 =
																						MAKE_YOUNG_PAIR
																						(BGl_symbol2432z00zz__rgc_expandz00,
																						BgL_arg1384z00_1343);
																				}
																				BgL_arg1368z00_1329 =
																					MAKE_YOUNG_PAIR(BgL_arg1383z00_1342,
																					BNIL);
																			}
																			BgL_arg1364z00_1325 =
																				MAKE_YOUNG_PAIR(BgL_arg1367z00_1328,
																				BgL_arg1368z00_1329);
																		}
																		BgL_arg1362z00_1323 =
																			MAKE_YOUNG_PAIR(BgL_arg1363z00_1324,
																			BgL_arg1364z00_1325);
																	}
																	BgL_arg1360z00_1321 =
																		MAKE_YOUNG_PAIR
																		(BGl_symbol2318z00zz__rgc_expandz00,
																		BgL_arg1362z00_1323);
																}
																{	/* Rgc/rgcexpand.scm 197 */
																	obj_t BgL_arg1423z00_1378;
																	obj_t BgL_arg1424z00_1379;

																	{	/* Rgc/rgcexpand.scm 197 */
																		obj_t BgL_arg1425z00_1380;

																		{	/* Rgc/rgcexpand.scm 197 */
																			obj_t BgL_arg1426z00_1381;
																			obj_t BgL_arg1427z00_1382;

																			{	/* Rgc/rgcexpand.scm 197 */
																				obj_t BgL_arg1428z00_1383;

																				{	/* Rgc/rgcexpand.scm 197 */
																					obj_t BgL_arg1429z00_1384;

																					{	/* Rgc/rgcexpand.scm 197 */
																						obj_t BgL_arg1430z00_1385;

																						BgL_arg1430z00_1385 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol2434z00zz__rgc_expandz00,
																							BNIL);
																						BgL_arg1429z00_1384 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol2398z00zz__rgc_expandz00,
																							BgL_arg1430z00_1385);
																					}
																					BgL_arg1428z00_1383 =
																						MAKE_YOUNG_PAIR
																						(BGl_symbol2400z00zz__rgc_expandz00,
																						BgL_arg1429z00_1384);
																				}
																				BgL_arg1426z00_1381 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol2436z00zz__rgc_expandz00,
																					BgL_arg1428z00_1383);
																			}
																			{	/* Rgc/rgcexpand.scm 198 */
																				obj_t BgL_arg1431z00_1386;
																				obj_t BgL_arg1434z00_1387;

																				{	/* Rgc/rgcexpand.scm 198 */
																					obj_t BgL_arg1435z00_1388;

																					{	/* Rgc/rgcexpand.scm 198 */
																						obj_t BgL_arg1436z00_1389;
																						obj_t BgL_arg1437z00_1390;

																						{	/* Rgc/rgcexpand.scm 198 */
																							obj_t BgL_arg1438z00_1391;

																							{	/* Rgc/rgcexpand.scm 198 */
																								obj_t BgL_arg1439z00_1392;

																								BgL_arg1439z00_1392 =
																									MAKE_YOUNG_PAIR(BINT(0L),
																									BNIL);
																								BgL_arg1438z00_1391 =
																									MAKE_YOUNG_PAIR
																									(BGl_symbol2406z00zz__rgc_expandz00,
																									BgL_arg1439z00_1392);
																							}
																							BgL_arg1436z00_1389 =
																								MAKE_YOUNG_PAIR
																								(BGl_symbol2408z00zz__rgc_expandz00,
																								BgL_arg1438z00_1391);
																						}
																						{	/* Rgc/rgcexpand.scm 198 */
																							obj_t BgL_arg1440z00_1393;

																							{	/* Rgc/rgcexpand.scm 198 */
																								obj_t BgL_arg1441z00_1394;

																								{	/* Rgc/rgcexpand.scm 198 */
																									obj_t BgL_arg1442z00_1395;

																									{	/* Rgc/rgcexpand.scm 198 */
																										obj_t BgL_arg1443z00_1396;

																										{	/* Rgc/rgcexpand.scm 198 */
																											obj_t BgL_arg1444z00_1397;

																											{	/* Rgc/rgcexpand.scm 198 */
																												obj_t
																													BgL_arg1445z00_1398;
																												obj_t
																													BgL_arg1446z00_1399;
																												BgL_arg1445z00_1398 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol2394z00zz__rgc_expandz00,
																													BNIL);
																												BgL_arg1446z00_1399 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol2406z00zz__rgc_expandz00,
																													BNIL);
																												BgL_arg1444z00_1397 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1445z00_1398,
																													BgL_arg1446z00_1399);
																											}
																											BgL_arg1443z00_1396 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol2410z00zz__rgc_expandz00,
																												BgL_arg1444z00_1397);
																										}
																										BgL_arg1442z00_1395 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1443z00_1396,
																											BNIL);
																									}
																									BgL_arg1441z00_1394 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2406z00zz__rgc_expandz00,
																										BgL_arg1442z00_1395);
																								}
																								BgL_arg1440z00_1393 =
																									MAKE_YOUNG_PAIR
																									(BGl_symbol2330z00zz__rgc_expandz00,
																									BgL_arg1441z00_1394);
																							}
																							BgL_arg1437z00_1390 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1440z00_1393, BNIL);
																						}
																						BgL_arg1435z00_1388 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1436z00_1389,
																							BgL_arg1437z00_1390);
																					}
																					BgL_arg1431z00_1386 =
																						MAKE_YOUNG_PAIR
																						(BGl_symbol2412z00zz__rgc_expandz00,
																						BgL_arg1435z00_1388);
																				}
																				{	/* Rgc/rgcexpand.scm 199 */
																					obj_t BgL_arg1447z00_1400;

																					{	/* Rgc/rgcexpand.scm 199 */
																						obj_t BgL_arg1448z00_1401;

																						{	/* Rgc/rgcexpand.scm 199 */
																							obj_t BgL_arg1449z00_1402;
																							obj_t BgL_arg1450z00_1403;

																							{	/* Rgc/rgcexpand.scm 199 */
																								obj_t BgL_arg1451z00_1404;

																								{	/* Rgc/rgcexpand.scm 199 */
																									obj_t BgL_arg1452z00_1405;
																									obj_t BgL_arg1453z00_1406;

																									{	/* Rgc/rgcexpand.scm 199 */
																										obj_t BgL_arg1454z00_1407;

																										{	/* Rgc/rgcexpand.scm 199 */
																											obj_t BgL_arg1455z00_1408;

																											BgL_arg1455z00_1408 =
																												MAKE_YOUNG_PAIR(BINT
																												(0L), BNIL);
																											BgL_arg1454z00_1407 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol2404z00zz__rgc_expandz00,
																												BgL_arg1455z00_1408);
																										}
																										BgL_arg1452z00_1405 =
																											MAKE_YOUNG_PAIR
																											(BGl_symbol2414z00zz__rgc_expandz00,
																											BgL_arg1454z00_1407);
																									}
																									{	/* Rgc/rgcexpand.scm 199 */
																										obj_t BgL_arg1456z00_1409;
																										obj_t BgL_arg1457z00_1410;

																										{	/* Rgc/rgcexpand.scm 199 */
																											obj_t BgL_arg1458z00_1411;

																											{	/* Rgc/rgcexpand.scm 199 */
																												obj_t
																													BgL_arg1459z00_1412;
																												BgL_arg1459z00_1412 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol2404z00zz__rgc_expandz00,
																													BNIL);
																												BgL_arg1458z00_1411 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol2406z00zz__rgc_expandz00,
																													BgL_arg1459z00_1412);
																											}
																											BgL_arg1456z00_1409 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol2414z00zz__rgc_expandz00,
																												BgL_arg1458z00_1411);
																										}
																										{	/* Rgc/rgcexpand.scm 199 */
																											obj_t BgL_arg1460z00_1413;

																											{	/* Rgc/rgcexpand.scm 199 */
																												obj_t
																													BgL_arg1461z00_1414;
																												{	/* Rgc/rgcexpand.scm 199 */
																													obj_t
																														BgL_arg1462z00_1415;
																													{	/* Rgc/rgcexpand.scm 199 */
																														obj_t
																															BgL_arg1463z00_1416;
																														BgL_arg1463z00_1416
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_symbol2394z00zz__rgc_expandz00,
																															BNIL);
																														BgL_arg1462z00_1415
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1463z00_1416,
																															BNIL);
																													}
																													BgL_arg1461z00_1414 =
																														MAKE_YOUNG_PAIR
																														(BGl_symbol2406z00zz__rgc_expandz00,
																														BgL_arg1462z00_1415);
																												}
																												BgL_arg1460z00_1413 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol2416z00zz__rgc_expandz00,
																													BgL_arg1461z00_1414);
																											}
																											BgL_arg1457z00_1410 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1460z00_1413,
																												BNIL);
																										}
																										BgL_arg1453z00_1406 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1456z00_1409,
																											BgL_arg1457z00_1410);
																									}
																									BgL_arg1451z00_1404 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1452z00_1405,
																										BgL_arg1453z00_1406);
																								}
																								BgL_arg1449z00_1402 =
																									MAKE_YOUNG_PAIR
																									(BGl_symbol2418z00zz__rgc_expandz00,
																									BgL_arg1451z00_1404);
																							}
																							{	/* Rgc/rgcexpand.scm 200 */
																								obj_t BgL_arg1464z00_1417;
																								obj_t BgL_arg1465z00_1418;

																								{	/* Rgc/rgcexpand.scm 200 */
																									obj_t BgL_arg1466z00_1419;

																									{	/* Rgc/rgcexpand.scm 200 */
																										obj_t BgL_arg1467z00_1420;

																										{	/* Rgc/rgcexpand.scm 200 */
																											obj_t BgL_arg1468z00_1421;

																											{	/* Rgc/rgcexpand.scm 200 */
																												obj_t
																													BgL_arg1469z00_1422;
																												BgL_arg1469z00_1422 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol2438z00zz__rgc_expandz00,
																													BNIL);
																												BgL_arg1468z00_1421 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol2406z00zz__rgc_expandz00,
																													BgL_arg1469z00_1422);
																											}
																											BgL_arg1467z00_1420 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol2404z00zz__rgc_expandz00,
																												BgL_arg1468z00_1421);
																										}
																										BgL_arg1466z00_1419 =
																											MAKE_YOUNG_PAIR
																											(BGl_symbol2315z00zz__rgc_expandz00,
																											BgL_arg1467z00_1420);
																									}
																									BgL_arg1464z00_1417 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2440z00zz__rgc_expandz00,
																										BgL_arg1466z00_1419);
																								}
																								{	/* Rgc/rgcexpand.scm 202 */
																									obj_t BgL_arg1472z00_1423;

																									{	/* Rgc/rgcexpand.scm 202 */
																										obj_t BgL_arg1473z00_1424;

																										{	/* Rgc/rgcexpand.scm 202 */
																											obj_t BgL_arg1474z00_1425;

																											{	/* Rgc/rgcexpand.scm 202 */
																												obj_t
																													BgL_arg1476z00_1426;
																												obj_t
																													BgL_arg1477z00_1427;
																												{	/* Rgc/rgcexpand.scm 202 */
																													obj_t
																														BgL_arg1478z00_1428;
																													obj_t
																														BgL_arg1479z00_1429;
																													{	/* Rgc/rgcexpand.scm 202 */
																														obj_t
																															BgL_arg1480z00_1430;
																														{	/* Rgc/rgcexpand.scm 202 */
																															obj_t
																																BgL_arg1481z00_1431;
																															BgL_arg1481z00_1431
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol2420z00zz__rgc_expandz00,
																																BNIL);
																															BgL_arg1480z00_1430
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol2422z00zz__rgc_expandz00,
																																BgL_arg1481z00_1431);
																														}
																														BgL_arg1478z00_1428
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_symbol2424z00zz__rgc_expandz00,
																															BgL_arg1480z00_1430);
																													}
																													{	/* Rgc/rgcexpand.scm 203 */
																														obj_t
																															BgL_arg1482z00_1432;
																														{	/* Rgc/rgcexpand.scm 203 */
																															obj_t
																																BgL_arg1483z00_1433;
																															BgL_arg1483z00_1433
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol2426z00zz__rgc_expandz00,
																																BNIL);
																															BgL_arg1482z00_1432
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1483z00_1433,
																																BNIL);
																														}
																														BgL_arg1479z00_1429
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_string2428z00zz__rgc_expandz00,
																															BgL_arg1482z00_1432);
																													}
																													BgL_arg1476z00_1426 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1478z00_1428,
																														BgL_arg1479z00_1429);
																												}
																												{	/* Rgc/rgcexpand.scm 204 */
																													obj_t
																														BgL_arg1484z00_1434;
																													{	/* Rgc/rgcexpand.scm 204 */
																														obj_t
																															BgL_arg1485z00_1435;
																														{	/* Rgc/rgcexpand.scm 204 */
																															obj_t
																																BgL_arg1486z00_1436;
																															BgL_arg1486z00_1436
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol2406z00zz__rgc_expandz00,
																																BNIL);
																															BgL_arg1485z00_1435
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol2404z00zz__rgc_expandz00,
																																BgL_arg1486z00_1436);
																														}
																														BgL_arg1484z00_1434
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_symbol2335z00zz__rgc_expandz00,
																															BgL_arg1485z00_1435);
																													}
																													BgL_arg1477z00_1427 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1484z00_1434,
																														BNIL);
																												}
																												BgL_arg1474z00_1425 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1476z00_1426,
																													BgL_arg1477z00_1427);
																											}
																											BgL_arg1473z00_1424 =
																												MAKE_YOUNG_PAIR
																												(BGl_string2442z00zz__rgc_expandz00,
																												BgL_arg1474z00_1425);
																										}
																										BgL_arg1472z00_1423 =
																											MAKE_YOUNG_PAIR
																											(BGl_symbol2430z00zz__rgc_expandz00,
																											BgL_arg1473z00_1424);
																									}
																									BgL_arg1465z00_1418 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1472z00_1423, BNIL);
																								}
																								BgL_arg1450z00_1403 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1464z00_1417,
																									BgL_arg1465z00_1418);
																							}
																							BgL_arg1448z00_1401 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1449z00_1402,
																								BgL_arg1450z00_1403);
																						}
																						BgL_arg1447z00_1400 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol2432z00zz__rgc_expandz00,
																							BgL_arg1448z00_1401);
																					}
																					BgL_arg1434z00_1387 =
																						MAKE_YOUNG_PAIR(BgL_arg1447z00_1400,
																						BNIL);
																				}
																				BgL_arg1427z00_1382 =
																					MAKE_YOUNG_PAIR(BgL_arg1431z00_1386,
																					BgL_arg1434z00_1387);
																			}
																			BgL_arg1425z00_1380 =
																				MAKE_YOUNG_PAIR(BgL_arg1426z00_1381,
																				BgL_arg1427z00_1382);
																		}
																		BgL_arg1423z00_1378 =
																			MAKE_YOUNG_PAIR
																			(BGl_symbol2318z00zz__rgc_expandz00,
																			BgL_arg1425z00_1380);
																	}
																	{	/* Rgc/rgcexpand.scm 206 */
																		obj_t BgL_arg1487z00_1437;
																		obj_t BgL_arg1488z00_1438;

																		{	/* Rgc/rgcexpand.scm 206 */
																			obj_t BgL_arg1489z00_1439;

																			{	/* Rgc/rgcexpand.scm 206 */
																				obj_t BgL_arg1490z00_1440;
																				obj_t BgL_arg1492z00_1441;

																				BgL_arg1490z00_1440 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol2443z00zz__rgc_expandz00,
																					BNIL);
																				{	/* Rgc/rgcexpand.scm 207 */
																					obj_t BgL_arg1494z00_1442;

																					{	/* Rgc/rgcexpand.scm 207 */
																						obj_t BgL_arg1495z00_1443;

																						BgL_arg1495z00_1443 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol2315z00zz__rgc_expandz00,
																							BNIL);
																						BgL_arg1494z00_1442 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol2445z00zz__rgc_expandz00,
																							BgL_arg1495z00_1443);
																					}
																					BgL_arg1492z00_1441 =
																						MAKE_YOUNG_PAIR(BgL_arg1494z00_1442,
																						BNIL);
																				}
																				BgL_arg1489z00_1439 =
																					MAKE_YOUNG_PAIR(BgL_arg1490z00_1440,
																					BgL_arg1492z00_1441);
																			}
																			BgL_arg1487z00_1437 =
																				MAKE_YOUNG_PAIR
																				(BGl_symbol2318z00zz__rgc_expandz00,
																				BgL_arg1489z00_1439);
																		}
																		{	/* Rgc/rgcexpand.scm 209 */
																			obj_t BgL_arg1497z00_1444;
																			obj_t BgL_arg1498z00_1445;

																			{	/* Rgc/rgcexpand.scm 209 */
																				obj_t BgL_arg1499z00_1446;

																				{	/* Rgc/rgcexpand.scm 209 */
																					obj_t BgL_arg1500z00_1447;
																					obj_t BgL_arg1501z00_1448;

																					BgL_arg1500z00_1447 =
																						MAKE_YOUNG_PAIR
																						(BGl_symbol2447z00zz__rgc_expandz00,
																						BNIL);
																					{	/* Rgc/rgcexpand.scm 210 */
																						obj_t BgL_arg1502z00_1449;

																						{	/* Rgc/rgcexpand.scm 210 */
																							obj_t BgL_arg1503z00_1450;

																							BgL_arg1503z00_1450 =
																								MAKE_YOUNG_PAIR
																								(BGl_symbol2315z00zz__rgc_expandz00,
																								BNIL);
																							BgL_arg1502z00_1449 =
																								MAKE_YOUNG_PAIR
																								(BGl_symbol2449z00zz__rgc_expandz00,
																								BgL_arg1503z00_1450);
																						}
																						BgL_arg1501z00_1448 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1502z00_1449, BNIL);
																					}
																					BgL_arg1499z00_1446 =
																						MAKE_YOUNG_PAIR(BgL_arg1500z00_1447,
																						BgL_arg1501z00_1448);
																				}
																				BgL_arg1497z00_1444 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol2318z00zz__rgc_expandz00,
																					BgL_arg1499z00_1446);
																			}
																			{	/* Rgc/rgcexpand.scm 212 */
																				obj_t BgL_arg1504z00_1451;
																				obj_t BgL_arg1505z00_1452;

																				{	/* Rgc/rgcexpand.scm 212 */
																					obj_t BgL_arg1506z00_1453;

																					{	/* Rgc/rgcexpand.scm 212 */
																						obj_t BgL_arg1507z00_1454;
																						obj_t BgL_arg1508z00_1455;

																						BgL_arg1507z00_1454 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol2451z00zz__rgc_expandz00,
																							BNIL);
																						{	/* Rgc/rgcexpand.scm 213 */
																							obj_t BgL_arg1509z00_1456;

																							{	/* Rgc/rgcexpand.scm 213 */
																								obj_t BgL_arg1510z00_1457;

																								BgL_arg1510z00_1457 =
																									MAKE_YOUNG_PAIR
																									(BGl_symbol2315z00zz__rgc_expandz00,
																									BNIL);
																								BgL_arg1509z00_1456 =
																									MAKE_YOUNG_PAIR
																									(BGl_symbol2453z00zz__rgc_expandz00,
																									BgL_arg1510z00_1457);
																							}
																							BgL_arg1508z00_1455 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1509z00_1456, BNIL);
																						}
																						BgL_arg1506z00_1453 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1507z00_1454,
																							BgL_arg1508z00_1455);
																					}
																					BgL_arg1504z00_1451 =
																						MAKE_YOUNG_PAIR
																						(BGl_symbol2318z00zz__rgc_expandz00,
																						BgL_arg1506z00_1453);
																				}
																				{	/* Rgc/rgcexpand.scm 215 */
																					obj_t BgL_arg1511z00_1458;
																					obj_t BgL_arg1513z00_1459;

																					{	/* Rgc/rgcexpand.scm 215 */
																						obj_t BgL_arg1514z00_1460;

																						{	/* Rgc/rgcexpand.scm 215 */
																							obj_t BgL_arg1516z00_1461;
																							obj_t BgL_arg1517z00_1462;

																							BgL_arg1516z00_1461 =
																								MAKE_YOUNG_PAIR
																								(BGl_symbol2455z00zz__rgc_expandz00,
																								BNIL);
																							{	/* Rgc/rgcexpand.scm 216 */
																								obj_t BgL_arg1521z00_1463;

																								{	/* Rgc/rgcexpand.scm 216 */
																									obj_t BgL_arg1522z00_1464;

																									BgL_arg1522z00_1464 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2315z00zz__rgc_expandz00,
																										BNIL);
																									BgL_arg1521z00_1463 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2457z00zz__rgc_expandz00,
																										BgL_arg1522z00_1464);
																								}
																								BgL_arg1517z00_1462 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1521z00_1463, BNIL);
																							}
																							BgL_arg1514z00_1460 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1516z00_1461,
																								BgL_arg1517z00_1462);
																						}
																						BgL_arg1511z00_1458 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol2318z00zz__rgc_expandz00,
																							BgL_arg1514z00_1460);
																					}
																					{	/* Rgc/rgcexpand.scm 218 */
																						obj_t BgL_arg1523z00_1465;
																						obj_t BgL_arg1524z00_1466;

																						{	/* Rgc/rgcexpand.scm 218 */
																							obj_t BgL_arg1525z00_1467;

																							{	/* Rgc/rgcexpand.scm 218 */
																								obj_t BgL_arg1526z00_1468;
																								obj_t BgL_arg1527z00_1469;

																								BgL_arg1526z00_1468 =
																									MAKE_YOUNG_PAIR
																									(BGl_symbol2459z00zz__rgc_expandz00,
																									BNIL);
																								{	/* Rgc/rgcexpand.scm 219 */
																									obj_t BgL_arg1528z00_1470;

																									{	/* Rgc/rgcexpand.scm 219 */
																										obj_t BgL_arg1529z00_1471;

																										BgL_arg1529z00_1471 =
																											MAKE_YOUNG_PAIR
																											(BGl_symbol2315z00zz__rgc_expandz00,
																											BNIL);
																										BgL_arg1528z00_1470 =
																											MAKE_YOUNG_PAIR
																											(BGl_symbol2461z00zz__rgc_expandz00,
																											BgL_arg1529z00_1471);
																									}
																									BgL_arg1527z00_1469 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1528z00_1470, BNIL);
																								}
																								BgL_arg1525z00_1467 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1526z00_1468,
																									BgL_arg1527z00_1469);
																							}
																							BgL_arg1523z00_1465 =
																								MAKE_YOUNG_PAIR
																								(BGl_symbol2318z00zz__rgc_expandz00,
																								BgL_arg1525z00_1467);
																						}
																						{	/* Rgc/rgcexpand.scm 221 */
																							obj_t BgL_arg1530z00_1472;
																							obj_t BgL_arg1531z00_1473;

																							{	/* Rgc/rgcexpand.scm 221 */
																								obj_t BgL_arg1535z00_1474;

																								{	/* Rgc/rgcexpand.scm 221 */
																									obj_t BgL_arg1536z00_1475;
																									obj_t BgL_arg1537z00_1476;

																									{	/* Rgc/rgcexpand.scm 221 */
																										obj_t BgL_arg1539z00_1477;

																										{	/* Rgc/rgcexpand.scm 221 */
																											obj_t BgL_arg1540z00_1478;

																											BgL_arg1540z00_1478 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol2406z00zz__rgc_expandz00,
																												BNIL);
																											BgL_arg1539z00_1477 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol2404z00zz__rgc_expandz00,
																												BgL_arg1540z00_1478);
																										}
																										BgL_arg1536z00_1475 =
																											MAKE_YOUNG_PAIR
																											(BGl_symbol2463z00zz__rgc_expandz00,
																											BgL_arg1539z00_1477);
																									}
																									{	/* Rgc/rgcexpand.scm 222 */
																										obj_t BgL_arg1543z00_1479;

																										{	/* Rgc/rgcexpand.scm 222 */
																											obj_t BgL_arg1544z00_1480;

																											{	/* Rgc/rgcexpand.scm 222 */
																												obj_t
																													BgL_arg1546z00_1481;
																												obj_t
																													BgL_arg1547z00_1482;
																												{	/* Rgc/rgcexpand.scm 222 */
																													obj_t
																														BgL_arg1549z00_1483;
																													{	/* Rgc/rgcexpand.scm 222 */
																														obj_t
																															BgL_arg1552z00_1484;
																														BgL_arg1552z00_1484
																															=
																															MAKE_YOUNG_PAIR
																															(BINT(0L), BNIL);
																														BgL_arg1549z00_1483
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_symbol2406z00zz__rgc_expandz00,
																															BgL_arg1552z00_1484);
																													}
																													BgL_arg1546z00_1481 =
																														MAKE_YOUNG_PAIR
																														(BGl_symbol2408z00zz__rgc_expandz00,
																														BgL_arg1549z00_1483);
																												}
																												{	/* Rgc/rgcexpand.scm 223 */
																													obj_t
																														BgL_arg1553z00_1485;
																													obj_t
																														BgL_arg1554z00_1486;
																													{	/* Rgc/rgcexpand.scm 223 */
																														obj_t
																															BgL_arg1555z00_1487;
																														{	/* Rgc/rgcexpand.scm 223 */
																															obj_t
																																BgL_arg1556z00_1488;
																															obj_t
																																BgL_arg1557z00_1489;
																															{	/* Rgc/rgcexpand.scm 223 */
																																obj_t
																																	BgL_arg1558z00_1490;
																																{	/* Rgc/rgcexpand.scm 223 */
																																	obj_t
																																		BgL_arg1559z00_1491;
																																	{	/* Rgc/rgcexpand.scm 223 */
																																		obj_t
																																			BgL_arg1561z00_1492;
																																		{	/* Rgc/rgcexpand.scm 223 */
																																			obj_t
																																				BgL_arg1562z00_1493;
																																			{	/* Rgc/rgcexpand.scm 223 */
																																				obj_t
																																					BgL_arg1564z00_1494;
																																				obj_t
																																					BgL_arg1565z00_1495;
																																				BgL_arg1564z00_1494
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BGl_symbol2394z00zz__rgc_expandz00,
																																					BNIL);
																																				BgL_arg1565z00_1495
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BGl_symbol2406z00zz__rgc_expandz00,
																																					BNIL);
																																				BgL_arg1562z00_1493
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1564z00_1494,
																																					BgL_arg1565z00_1495);
																																			}
																																			BgL_arg1561z00_1492
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_symbol2410z00zz__rgc_expandz00,
																																				BgL_arg1562z00_1493);
																																		}
																																		BgL_arg1559z00_1491
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1561z00_1492,
																																			BNIL);
																																	}
																																	BgL_arg1558z00_1490
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_symbol2350z00zz__rgc_expandz00,
																																		BgL_arg1559z00_1491);
																																}
																																BgL_arg1556z00_1488
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1558z00_1490,
																																	BNIL);
																															}
																															{	/* Rgc/rgcexpand.scm 224 */
																																obj_t
																																	BgL_arg1567z00_1496;
																																{	/* Rgc/rgcexpand.scm 224 */
																																	obj_t
																																		BgL_arg1571z00_1497;
																																	{	/* Rgc/rgcexpand.scm 224 */
																																		obj_t
																																			BgL_arg1573z00_1498;
																																		obj_t
																																			BgL_arg1575z00_1499;
																																		{	/* Rgc/rgcexpand.scm 224 */
																																			obj_t
																																				BgL_arg1576z00_1500;
																																			{	/* Rgc/rgcexpand.scm 224 */
																																				obj_t
																																					BgL_arg1578z00_1501;
																																				BgL_arg1578z00_1501
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BGl_symbol2404z00zz__rgc_expandz00,
																																					BNIL);
																																				BgL_arg1576z00_1500
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BGl_symbol2350z00zz__rgc_expandz00,
																																					BgL_arg1578z00_1501);
																																			}
																																			BgL_arg1573z00_1498
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_symbol2416z00zz__rgc_expandz00,
																																				BgL_arg1576z00_1500);
																																		}
																																		{	/* Rgc/rgcexpand.scm 225 */
																																			obj_t
																																				BgL_arg1579z00_1502;
																																			obj_t
																																				BgL_arg1580z00_1503;
																																			{	/* Rgc/rgcexpand.scm 225 */
																																				obj_t
																																					BgL_arg1582z00_1504;
																																				{	/* Rgc/rgcexpand.scm 225 */
																																					obj_t
																																						BgL_arg1583z00_1505;
																																					{	/* Rgc/rgcexpand.scm 225 */
																																						obj_t
																																							BgL_arg1584z00_1506;
																																						{	/* Rgc/rgcexpand.scm 225 */
																																							obj_t
																																								BgL_arg1585z00_1507;
																																							{	/* Rgc/rgcexpand.scm 225 */
																																								obj_t
																																									BgL_arg1586z00_1508;
																																								{	/* Rgc/rgcexpand.scm 225 */
																																									obj_t
																																										BgL_arg1587z00_1509;
																																									BgL_arg1587z00_1509
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BGl_symbol2406z00zz__rgc_expandz00,
																																										BNIL);
																																									BgL_arg1586z00_1508
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BGl_symbol2404z00zz__rgc_expandz00,
																																										BgL_arg1587z00_1509);
																																								}
																																								BgL_arg1585z00_1507
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BGl_symbol2335z00zz__rgc_expandz00,
																																									BgL_arg1586z00_1508);
																																							}
																																							BgL_arg1584z00_1506
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_arg1585z00_1507,
																																								BNIL);
																																						}
																																						BgL_arg1583z00_1505
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BGl_string2465z00zz__rgc_expandz00,
																																							BgL_arg1584z00_1506);
																																					}
																																					BgL_arg1582z00_1504
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_string2466z00zz__rgc_expandz00,
																																						BgL_arg1583z00_1505);
																																				}
																																				BgL_arg1579z00_1502
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BGl_symbol2430z00zz__rgc_expandz00,
																																					BgL_arg1582z00_1504);
																																			}
																																			{	/* Rgc/rgcexpand.scm 226 */
																																				obj_t
																																					BgL_arg1589z00_1510;
																																				{	/* Rgc/rgcexpand.scm 226 */
																																					obj_t
																																						BgL_arg1591z00_1511;
																																					{	/* Rgc/rgcexpand.scm 226 */
																																						obj_t
																																							BgL_arg1593z00_1512;
																																						{	/* Rgc/rgcexpand.scm 226 */
																																							obj_t
																																								BgL_arg1594z00_1513;
																																							BgL_arg1594z00_1513
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BGl_symbol2350z00zz__rgc_expandz00,
																																								BNIL);
																																							BgL_arg1593z00_1512
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BGl_symbol2404z00zz__rgc_expandz00,
																																								BgL_arg1594z00_1513);
																																						}
																																						BgL_arg1591z00_1511
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BGl_symbol2315z00zz__rgc_expandz00,
																																							BgL_arg1593z00_1512);
																																					}
																																					BgL_arg1589z00_1510
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_symbol2467z00zz__rgc_expandz00,
																																						BgL_arg1591z00_1511);
																																				}
																																				BgL_arg1580z00_1503
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1589z00_1510,
																																					BNIL);
																																			}
																																			BgL_arg1575z00_1499
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1579z00_1502,
																																				BgL_arg1580z00_1503);
																																		}
																																		BgL_arg1571z00_1497
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1573z00_1498,
																																			BgL_arg1575z00_1499);
																																	}
																																	BgL_arg1567z00_1496
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_symbol2432z00zz__rgc_expandz00,
																																		BgL_arg1571z00_1497);
																																}
																																BgL_arg1557z00_1489
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1567z00_1496,
																																	BNIL);
																															}
																															BgL_arg1555z00_1487
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1556z00_1488,
																																BgL_arg1557z00_1489);
																														}
																														BgL_arg1553z00_1485
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_symbol2309z00zz__rgc_expandz00,
																															BgL_arg1555z00_1487);
																													}
																													{	/* Rgc/rgcexpand.scm 227 */
																														obj_t
																															BgL_arg1595z00_1514;
																														{	/* Rgc/rgcexpand.scm 227 */
																															obj_t
																																BgL_arg1598z00_1515;
																															{	/* Rgc/rgcexpand.scm 227 */
																																obj_t
																																	BgL_arg1601z00_1516;
																																obj_t
																																	BgL_arg1602z00_1517;
																																{	/* Rgc/rgcexpand.scm 227 */
																																	obj_t
																																		BgL_arg1603z00_1518;
																																	{	/* Rgc/rgcexpand.scm 227 */
																																		obj_t
																																			BgL_arg1605z00_1519;
																																		obj_t
																																			BgL_arg1606z00_1520;
																																		{	/* Rgc/rgcexpand.scm 227 */
																																			obj_t
																																				BgL_arg1607z00_1521;
																																			{	/* Rgc/rgcexpand.scm 227 */
																																				obj_t
																																					BgL_arg1608z00_1522;
																																				BgL_arg1608z00_1522
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BINT
																																					(0L),
																																					BNIL);
																																				BgL_arg1607z00_1521
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BGl_symbol2404z00zz__rgc_expandz00,
																																					BgL_arg1608z00_1522);
																																			}
																																			BgL_arg1605z00_1519
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_symbol2414z00zz__rgc_expandz00,
																																				BgL_arg1607z00_1521);
																																		}
																																		{	/* Rgc/rgcexpand.scm 227 */
																																			obj_t
																																				BgL_arg1609z00_1523;
																																			obj_t
																																				BgL_arg1610z00_1524;
																																			{	/* Rgc/rgcexpand.scm 227 */
																																				obj_t
																																					BgL_arg1611z00_1525;
																																				{	/* Rgc/rgcexpand.scm 227 */
																																					obj_t
																																						BgL_arg1612z00_1526;
																																					{	/* Rgc/rgcexpand.scm 227 */
																																						obj_t
																																							BgL_arg1613z00_1527;
																																						BgL_arg1613z00_1527
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BGl_symbol2394z00zz__rgc_expandz00,
																																							BNIL);
																																						BgL_arg1612z00_1526
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg1613z00_1527,
																																							BNIL);
																																					}
																																					BgL_arg1611z00_1525
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_symbol2406z00zz__rgc_expandz00,
																																						BgL_arg1612z00_1526);
																																				}
																																				BgL_arg1609z00_1523
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BGl_symbol2416z00zz__rgc_expandz00,
																																					BgL_arg1611z00_1525);
																																			}
																																			{	/* Rgc/rgcexpand.scm 227 */
																																				obj_t
																																					BgL_arg1615z00_1528;
																																				{	/* Rgc/rgcexpand.scm 227 */
																																					obj_t
																																						BgL_arg1616z00_1529;
																																					{	/* Rgc/rgcexpand.scm 227 */
																																						obj_t
																																							BgL_arg1617z00_1530;
																																						BgL_arg1617z00_1530
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BGl_symbol2404z00zz__rgc_expandz00,
																																							BNIL);
																																						BgL_arg1616z00_1529
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BGl_symbol2406z00zz__rgc_expandz00,
																																							BgL_arg1617z00_1530);
																																					}
																																					BgL_arg1615z00_1528
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_symbol2414z00zz__rgc_expandz00,
																																						BgL_arg1616z00_1529);
																																				}
																																				BgL_arg1610z00_1524
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1615z00_1528,
																																					BNIL);
																																			}
																																			BgL_arg1606z00_1520
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1609z00_1523,
																																				BgL_arg1610z00_1524);
																																		}
																																		BgL_arg1603z00_1518
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1605z00_1519,
																																			BgL_arg1606z00_1520);
																																	}
																																	BgL_arg1601z00_1516
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_symbol2418z00zz__rgc_expandz00,
																																		BgL_arg1603z00_1518);
																																}
																																{	/* Rgc/rgcexpand.scm 228 */
																																	obj_t
																																		BgL_arg1618z00_1531;
																																	obj_t
																																		BgL_arg1619z00_1532;
																																	{	/* Rgc/rgcexpand.scm 228 */
																																		obj_t
																																			BgL_arg1620z00_1533;
																																		{	/* Rgc/rgcexpand.scm 228 */
																																			obj_t
																																				BgL_arg1621z00_1534;
																																			{	/* Rgc/rgcexpand.scm 228 */
																																				obj_t
																																					BgL_arg1622z00_1535;
																																				BgL_arg1622z00_1535
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BGl_symbol2406z00zz__rgc_expandz00,
																																					BNIL);
																																				BgL_arg1621z00_1534
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BGl_symbol2404z00zz__rgc_expandz00,
																																					BgL_arg1622z00_1535);
																																			}
																																			BgL_arg1620z00_1533
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_symbol2315z00zz__rgc_expandz00,
																																				BgL_arg1621z00_1534);
																																		}
																																		BgL_arg1618z00_1531
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BGl_symbol2467z00zz__rgc_expandz00,
																																			BgL_arg1620z00_1533);
																																	}
																																	{	/* Rgc/rgcexpand.scm 229 */
																																		obj_t
																																			BgL_arg1623z00_1536;
																																		{	/* Rgc/rgcexpand.scm 229 */
																																			obj_t
																																				BgL_arg1624z00_1537;
																																			{	/* Rgc/rgcexpand.scm 229 */
																																				obj_t
																																					BgL_arg1625z00_1538;
																																				{	/* Rgc/rgcexpand.scm 229 */
																																					obj_t
																																						BgL_arg1626z00_1539;
																																					{	/* Rgc/rgcexpand.scm 229 */
																																						obj_t
																																							BgL_arg1627z00_1540;
																																						{	/* Rgc/rgcexpand.scm 229 */
																																							obj_t
																																								BgL_arg1628z00_1541;
																																							{	/* Rgc/rgcexpand.scm 229 */
																																								obj_t
																																									BgL_arg1629z00_1542;
																																								BgL_arg1629z00_1542
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BGl_symbol2406z00zz__rgc_expandz00,
																																									BNIL);
																																								BgL_arg1628z00_1541
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BGl_symbol2404z00zz__rgc_expandz00,
																																									BgL_arg1629z00_1542);
																																							}
																																							BgL_arg1627z00_1540
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BGl_symbol2335z00zz__rgc_expandz00,
																																								BgL_arg1628z00_1541);
																																						}
																																						BgL_arg1626z00_1539
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg1627z00_1540,
																																							BNIL);
																																					}
																																					BgL_arg1625z00_1538
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_string2465z00zz__rgc_expandz00,
																																						BgL_arg1626z00_1539);
																																				}
																																				BgL_arg1624z00_1537
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BGl_string2466z00zz__rgc_expandz00,
																																					BgL_arg1625z00_1538);
																																			}
																																			BgL_arg1623z00_1536
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_symbol2430z00zz__rgc_expandz00,
																																				BgL_arg1624z00_1537);
																																		}
																																		BgL_arg1619z00_1532
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1623z00_1536,
																																			BNIL);
																																	}
																																	BgL_arg1602z00_1517
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1618z00_1531,
																																		BgL_arg1619z00_1532);
																																}
																																BgL_arg1598z00_1515
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1601z00_1516,
																																	BgL_arg1602z00_1517);
																															}
																															BgL_arg1595z00_1514
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol2432z00zz__rgc_expandz00,
																																BgL_arg1598z00_1515);
																														}
																														BgL_arg1554z00_1486
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1595z00_1514,
																															BNIL);
																													}
																													BgL_arg1547z00_1482 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1553z00_1485,
																														BgL_arg1554z00_1486);
																												}
																												BgL_arg1544z00_1480 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1546z00_1481,
																													BgL_arg1547z00_1482);
																											}
																											BgL_arg1543z00_1479 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol2432z00zz__rgc_expandz00,
																												BgL_arg1544z00_1480);
																										}
																										BgL_arg1537z00_1476 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1543z00_1479,
																											BNIL);
																									}
																									BgL_arg1535z00_1474 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1536z00_1475,
																										BgL_arg1537z00_1476);
																								}
																								BgL_arg1530z00_1472 =
																									MAKE_YOUNG_PAIR
																									(BGl_symbol2318z00zz__rgc_expandz00,
																									BgL_arg1535z00_1474);
																							}
																							{	/* Rgc/rgcexpand.scm 231 */
																								obj_t BgL_arg1630z00_1543;
																								obj_t BgL_arg1631z00_1544;

																								{	/* Rgc/rgcexpand.scm 231 */
																									obj_t BgL_arg1634z00_1545;

																									{	/* Rgc/rgcexpand.scm 231 */
																										obj_t BgL_arg1636z00_1546;
																										obj_t BgL_arg1637z00_1547;

																										BgL_arg1636z00_1546 =
																											MAKE_YOUNG_PAIR
																											(BGl_symbol2469z00zz__rgc_expandz00,
																											BNIL);
																										{	/* Rgc/rgcexpand.scm 232 */
																											obj_t BgL_arg1638z00_1548;

																											{	/* Rgc/rgcexpand.scm 232 */
																												obj_t
																													BgL_arg1639z00_1549;
																												BgL_arg1639z00_1549 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol2315z00zz__rgc_expandz00,
																													BNIL);
																												BgL_arg1638z00_1548 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol2471z00zz__rgc_expandz00,
																													BgL_arg1639z00_1549);
																											}
																											BgL_arg1637z00_1547 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1638z00_1548,
																												BNIL);
																										}
																										BgL_arg1634z00_1545 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1636z00_1546,
																											BgL_arg1637z00_1547);
																									}
																									BgL_arg1630z00_1543 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2318z00zz__rgc_expandz00,
																										BgL_arg1634z00_1545);
																								}
																								{	/* Rgc/rgcexpand.scm 234 */
																									obj_t BgL_arg1640z00_1550;
																									obj_t BgL_arg1641z00_1551;

																									{	/* Rgc/rgcexpand.scm 234 */
																										obj_t BgL_arg1642z00_1552;

																										{	/* Rgc/rgcexpand.scm 234 */
																											obj_t BgL_arg1643z00_1553;
																											obj_t BgL_arg1644z00_1554;

																											{	/* Rgc/rgcexpand.scm 234 */
																												obj_t
																													BgL_arg1645z00_1555;
																												{	/* Rgc/rgcexpand.scm 234 */
																													obj_t
																														BgL_arg1646z00_1556;
																													BgL_arg1646z00_1556 =
																														MAKE_YOUNG_PAIR
																														(BGl_symbol2406z00zz__rgc_expandz00,
																														BNIL);
																													BgL_arg1645z00_1555 =
																														MAKE_YOUNG_PAIR
																														(BGl_symbol2404z00zz__rgc_expandz00,
																														BgL_arg1646z00_1556);
																												}
																												BgL_arg1643z00_1553 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol2473z00zz__rgc_expandz00,
																													BgL_arg1645z00_1555);
																											}
																											{	/* Rgc/rgcexpand.scm 235 */
																												obj_t
																													BgL_arg1648z00_1557;
																												{	/* Rgc/rgcexpand.scm 235 */
																													obj_t
																														BgL_arg1649z00_1558;
																													{	/* Rgc/rgcexpand.scm 235 */
																														obj_t
																															BgL_arg1650z00_1559;
																														obj_t
																															BgL_arg1651z00_1560;
																														{	/* Rgc/rgcexpand.scm 235 */
																															obj_t
																																BgL_arg1652z00_1561;
																															{	/* Rgc/rgcexpand.scm 235 */
																																obj_t
																																	BgL_arg1653z00_1562;
																																BgL_arg1653z00_1562
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BINT(0L),
																																	BNIL);
																																BgL_arg1652z00_1561
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BGl_symbol2406z00zz__rgc_expandz00,
																																	BgL_arg1653z00_1562);
																															}
																															BgL_arg1650z00_1559
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol2408z00zz__rgc_expandz00,
																																BgL_arg1652z00_1561);
																														}
																														{	/* Rgc/rgcexpand.scm 236 */
																															obj_t
																																BgL_arg1654z00_1563;
																															obj_t
																																BgL_arg1656z00_1564;
																															{	/* Rgc/rgcexpand.scm 236 */
																																obj_t
																																	BgL_arg1657z00_1565;
																																{	/* Rgc/rgcexpand.scm 236 */
																																	obj_t
																																		BgL_arg1658z00_1566;
																																	obj_t
																																		BgL_arg1661z00_1567;
																																	{	/* Rgc/rgcexpand.scm 236 */
																																		obj_t
																																			BgL_arg1663z00_1568;
																																		{	/* Rgc/rgcexpand.scm 236 */
																																			obj_t
																																				BgL_arg1664z00_1569;
																																			{	/* Rgc/rgcexpand.scm 236 */
																																				obj_t
																																					BgL_arg1667z00_1570;
																																				{	/* Rgc/rgcexpand.scm 236 */
																																					obj_t
																																						BgL_arg1668z00_1571;
																																					{	/* Rgc/rgcexpand.scm 236 */
																																						obj_t
																																							BgL_arg1669z00_1572;
																																						obj_t
																																							BgL_arg1670z00_1573;
																																						BgL_arg1669z00_1572
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BGl_symbol2394z00zz__rgc_expandz00,
																																							BNIL);
																																						BgL_arg1670z00_1573
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BGl_symbol2406z00zz__rgc_expandz00,
																																							BNIL);
																																						BgL_arg1668z00_1571
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg1669z00_1572,
																																							BgL_arg1670z00_1573);
																																					}
																																					BgL_arg1667z00_1570
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_symbol2410z00zz__rgc_expandz00,
																																						BgL_arg1668z00_1571);
																																				}
																																				BgL_arg1664z00_1569
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1667z00_1570,
																																					BNIL);
																																			}
																																			BgL_arg1663z00_1568
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_symbol2350z00zz__rgc_expandz00,
																																				BgL_arg1664z00_1569);
																																		}
																																		BgL_arg1658z00_1566
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1663z00_1568,
																																			BNIL);
																																	}
																																	{	/* Rgc/rgcexpand.scm 237 */
																																		obj_t
																																			BgL_arg1675z00_1574;
																																		{	/* Rgc/rgcexpand.scm 237 */
																																			obj_t
																																				BgL_arg1676z00_1575;
																																			{	/* Rgc/rgcexpand.scm 237 */
																																				obj_t
																																					BgL_arg1678z00_1576;
																																				obj_t
																																					BgL_arg1681z00_1577;
																																				{	/* Rgc/rgcexpand.scm 237 */
																																					obj_t
																																						BgL_arg1684z00_1578;
																																					{	/* Rgc/rgcexpand.scm 237 */
																																						obj_t
																																							BgL_arg1685z00_1579;
																																						BgL_arg1685z00_1579
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BGl_symbol2404z00zz__rgc_expandz00,
																																							BNIL);
																																						BgL_arg1684z00_1578
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BGl_symbol2350z00zz__rgc_expandz00,
																																							BgL_arg1685z00_1579);
																																					}
																																					BgL_arg1678z00_1576
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_symbol2416z00zz__rgc_expandz00,
																																						BgL_arg1684z00_1578);
																																				}
																																				{	/* Rgc/rgcexpand.scm 239 */
																																					obj_t
																																						BgL_arg1688z00_1580;
																																					obj_t
																																						BgL_arg1689z00_1581;
																																					{	/* Rgc/rgcexpand.scm 239 */
																																						obj_t
																																							BgL_arg1691z00_1582;
																																						{	/* Rgc/rgcexpand.scm 239 */
																																							obj_t
																																								BgL_arg1692z00_1583;
																																							{	/* Rgc/rgcexpand.scm 239 */
																																								obj_t
																																									BgL_arg1699z00_1584;
																																								{	/* Rgc/rgcexpand.scm 239 */
																																									obj_t
																																										BgL_arg1700z00_1585;
																																									{	/* Rgc/rgcexpand.scm 239 */
																																										obj_t
																																											BgL_arg1701z00_1586;
																																										{	/* Rgc/rgcexpand.scm 239 */
																																											obj_t
																																												BgL_arg1702z00_1587;
																																											BgL_arg1702z00_1587
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BGl_symbol2406z00zz__rgc_expandz00,
																																												BNIL);
																																											BgL_arg1701z00_1586
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BGl_symbol2404z00zz__rgc_expandz00,
																																												BgL_arg1702z00_1587);
																																										}
																																										BgL_arg1700z00_1585
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BGl_symbol2335z00zz__rgc_expandz00,
																																											BgL_arg1701z00_1586);
																																									}
																																									BgL_arg1699z00_1584
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_arg1700z00_1585,
																																										BNIL);
																																								}
																																								BgL_arg1692z00_1583
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BGl_string2465z00zz__rgc_expandz00,
																																									BgL_arg1699z00_1584);
																																							}
																																							BgL_arg1691z00_1582
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BGl_string2475z00zz__rgc_expandz00,
																																								BgL_arg1692z00_1583);
																																						}
																																						BgL_arg1688z00_1580
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BGl_symbol2430z00zz__rgc_expandz00,
																																							BgL_arg1691z00_1582);
																																					}
																																					{	/* Rgc/rgcexpand.scm 240 */
																																						obj_t
																																							BgL_arg1703z00_1588;
																																						{	/* Rgc/rgcexpand.scm 240 */
																																							obj_t
																																								BgL_arg1704z00_1589;
																																							{	/* Rgc/rgcexpand.scm 240 */
																																								obj_t
																																									BgL_arg1705z00_1590;
																																								{	/* Rgc/rgcexpand.scm 240 */
																																									obj_t
																																										BgL_arg1706z00_1591;
																																									BgL_arg1706z00_1591
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BGl_symbol2350z00zz__rgc_expandz00,
																																										BNIL);
																																									BgL_arg1705z00_1590
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BGl_symbol2404z00zz__rgc_expandz00,
																																										BgL_arg1706z00_1591);
																																								}
																																								BgL_arg1704z00_1589
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BGl_symbol2315z00zz__rgc_expandz00,
																																									BgL_arg1705z00_1590);
																																							}
																																							BgL_arg1703z00_1588
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BGl_symbol2476z00zz__rgc_expandz00,
																																								BgL_arg1704z00_1589);
																																						}
																																						BgL_arg1689z00_1581
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg1703z00_1588,
																																							BNIL);
																																					}
																																					BgL_arg1681z00_1577
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg1688z00_1580,
																																						BgL_arg1689z00_1581);
																																				}
																																				BgL_arg1676z00_1575
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1678z00_1576,
																																					BgL_arg1681z00_1577);
																																			}
																																			BgL_arg1675z00_1574
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_symbol2432z00zz__rgc_expandz00,
																																				BgL_arg1676z00_1575);
																																		}
																																		BgL_arg1661z00_1567
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1675z00_1574,
																																			BNIL);
																																	}
																																	BgL_arg1657z00_1565
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1658z00_1566,
																																		BgL_arg1661z00_1567);
																																}
																																BgL_arg1654z00_1563
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BGl_symbol2309z00zz__rgc_expandz00,
																																	BgL_arg1657z00_1565);
																															}
																															{	/* Rgc/rgcexpand.scm 241 */
																																obj_t
																																	BgL_arg1707z00_1592;
																																{	/* Rgc/rgcexpand.scm 241 */
																																	obj_t
																																		BgL_arg1708z00_1593;
																																	{	/* Rgc/rgcexpand.scm 241 */
																																		obj_t
																																			BgL_arg1709z00_1594;
																																		obj_t
																																			BgL_arg1710z00_1595;
																																		{	/* Rgc/rgcexpand.scm 241 */
																																			obj_t
																																				BgL_arg1711z00_1596;
																																			{	/* Rgc/rgcexpand.scm 241 */
																																				obj_t
																																					BgL_arg1714z00_1597;
																																				obj_t
																																					BgL_arg1715z00_1598;
																																				{	/* Rgc/rgcexpand.scm 241 */
																																					obj_t
																																						BgL_arg1717z00_1599;
																																					{	/* Rgc/rgcexpand.scm 241 */
																																						obj_t
																																							BgL_arg1718z00_1600;
																																						BgL_arg1718z00_1600
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BINT
																																							(0L),
																																							BNIL);
																																						BgL_arg1717z00_1599
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BGl_symbol2404z00zz__rgc_expandz00,
																																							BgL_arg1718z00_1600);
																																					}
																																					BgL_arg1714z00_1597
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_symbol2414z00zz__rgc_expandz00,
																																						BgL_arg1717z00_1599);
																																				}
																																				{	/* Rgc/rgcexpand.scm 241 */
																																					obj_t
																																						BgL_arg1720z00_1601;
																																					obj_t
																																						BgL_arg1722z00_1602;
																																					{	/* Rgc/rgcexpand.scm 241 */
																																						obj_t
																																							BgL_arg1723z00_1603;
																																						{	/* Rgc/rgcexpand.scm 241 */
																																							obj_t
																																								BgL_arg1724z00_1604;
																																							{	/* Rgc/rgcexpand.scm 241 */
																																								obj_t
																																									BgL_arg1725z00_1605;
																																								BgL_arg1725z00_1605
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BGl_symbol2394z00zz__rgc_expandz00,
																																									BNIL);
																																								BgL_arg1724z00_1604
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_arg1725z00_1605,
																																									BNIL);
																																							}
																																							BgL_arg1723z00_1603
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BGl_symbol2406z00zz__rgc_expandz00,
																																								BgL_arg1724z00_1604);
																																						}
																																						BgL_arg1720z00_1601
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BGl_symbol2416z00zz__rgc_expandz00,
																																							BgL_arg1723z00_1603);
																																					}
																																					{	/* Rgc/rgcexpand.scm 241 */
																																						obj_t
																																							BgL_arg1726z00_1606;
																																						{	/* Rgc/rgcexpand.scm 241 */
																																							obj_t
																																								BgL_arg1727z00_1607;
																																							{	/* Rgc/rgcexpand.scm 241 */
																																								obj_t
																																									BgL_arg1728z00_1608;
																																								BgL_arg1728z00_1608
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BGl_symbol2404z00zz__rgc_expandz00,
																																									BNIL);
																																								BgL_arg1727z00_1607
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BGl_symbol2406z00zz__rgc_expandz00,
																																									BgL_arg1728z00_1608);
																																							}
																																							BgL_arg1726z00_1606
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BGl_symbol2414z00zz__rgc_expandz00,
																																								BgL_arg1727z00_1607);
																																						}
																																						BgL_arg1722z00_1602
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg1726z00_1606,
																																							BNIL);
																																					}
																																					BgL_arg1715z00_1598
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg1720z00_1601,
																																						BgL_arg1722z00_1602);
																																				}
																																				BgL_arg1711z00_1596
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1714z00_1597,
																																					BgL_arg1715z00_1598);
																																			}
																																			BgL_arg1709z00_1594
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_symbol2418z00zz__rgc_expandz00,
																																				BgL_arg1711z00_1596);
																																		}
																																		{	/* Rgc/rgcexpand.scm 242 */
																																			obj_t
																																				BgL_arg1729z00_1609;
																																			obj_t
																																				BgL_arg1730z00_1610;
																																			{	/* Rgc/rgcexpand.scm 242 */
																																				obj_t
																																					BgL_arg1731z00_1611;
																																				{	/* Rgc/rgcexpand.scm 242 */
																																					obj_t
																																						BgL_arg1733z00_1612;
																																					{	/* Rgc/rgcexpand.scm 242 */
																																						obj_t
																																							BgL_arg1734z00_1613;
																																						BgL_arg1734z00_1613
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BGl_symbol2406z00zz__rgc_expandz00,
																																							BNIL);
																																						BgL_arg1733z00_1612
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BGl_symbol2404z00zz__rgc_expandz00,
																																							BgL_arg1734z00_1613);
																																					}
																																					BgL_arg1731z00_1611
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_symbol2315z00zz__rgc_expandz00,
																																						BgL_arg1733z00_1612);
																																				}
																																				BgL_arg1729z00_1609
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BGl_symbol2476z00zz__rgc_expandz00,
																																					BgL_arg1731z00_1611);
																																			}
																																			{	/* Rgc/rgcexpand.scm 244 */
																																				obj_t
																																					BgL_arg1735z00_1614;
																																				{	/* Rgc/rgcexpand.scm 244 */
																																					obj_t
																																						BgL_arg1736z00_1615;
																																					{	/* Rgc/rgcexpand.scm 244 */
																																						obj_t
																																							BgL_arg1737z00_1616;
																																						{	/* Rgc/rgcexpand.scm 244 */
																																							obj_t
																																								BgL_arg1738z00_1617;
																																							{	/* Rgc/rgcexpand.scm 244 */
																																								obj_t
																																									BgL_arg1739z00_1618;
																																								{	/* Rgc/rgcexpand.scm 244 */
																																									obj_t
																																										BgL_arg1740z00_1619;
																																									{	/* Rgc/rgcexpand.scm 244 */
																																										obj_t
																																											BgL_arg1741z00_1620;
																																										BgL_arg1741z00_1620
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BGl_symbol2406z00zz__rgc_expandz00,
																																											BNIL);
																																										BgL_arg1740z00_1619
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BGl_symbol2404z00zz__rgc_expandz00,
																																											BgL_arg1741z00_1620);
																																									}
																																									BgL_arg1739z00_1618
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BGl_symbol2335z00zz__rgc_expandz00,
																																										BgL_arg1740z00_1619);
																																								}
																																								BgL_arg1738z00_1617
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_arg1739z00_1618,
																																									BNIL);
																																							}
																																							BgL_arg1737z00_1616
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BGl_string2465z00zz__rgc_expandz00,
																																								BgL_arg1738z00_1617);
																																						}
																																						BgL_arg1736z00_1615
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BGl_string2475z00zz__rgc_expandz00,
																																							BgL_arg1737z00_1616);
																																					}
																																					BgL_arg1735z00_1614
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_symbol2430z00zz__rgc_expandz00,
																																						BgL_arg1736z00_1615);
																																				}
																																				BgL_arg1730z00_1610
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1735z00_1614,
																																					BNIL);
																																			}
																																			BgL_arg1710z00_1595
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1729z00_1609,
																																				BgL_arg1730z00_1610);
																																		}
																																		BgL_arg1708z00_1593
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1709z00_1594,
																																			BgL_arg1710z00_1595);
																																	}
																																	BgL_arg1707z00_1592
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_symbol2432z00zz__rgc_expandz00,
																																		BgL_arg1708z00_1593);
																																}
																																BgL_arg1656z00_1564
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1707z00_1592,
																																	BNIL);
																															}
																															BgL_arg1651z00_1560
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1654z00_1563,
																																BgL_arg1656z00_1564);
																														}
																														BgL_arg1649z00_1558
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1650z00_1559,
																															BgL_arg1651z00_1560);
																													}
																													BgL_arg1648z00_1557 =
																														MAKE_YOUNG_PAIR
																														(BGl_symbol2432z00zz__rgc_expandz00,
																														BgL_arg1649z00_1558);
																												}
																												BgL_arg1644z00_1554 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1648z00_1557,
																													BNIL);
																											}
																											BgL_arg1642z00_1552 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1643z00_1553,
																												BgL_arg1644z00_1554);
																										}
																										BgL_arg1640z00_1550 =
																											MAKE_YOUNG_PAIR
																											(BGl_symbol2318z00zz__rgc_expandz00,
																											BgL_arg1642z00_1552);
																									}
																									{	/* Rgc/rgcexpand.scm 246 */
																										obj_t BgL_arg1743z00_1621;
																										obj_t BgL_arg1744z00_1622;

																										{	/* Rgc/rgcexpand.scm 246 */
																											obj_t BgL_arg1745z00_1623;

																											{	/* Rgc/rgcexpand.scm 246 */
																												obj_t
																													BgL_arg1746z00_1624;
																												obj_t
																													BgL_arg1747z00_1625;
																												BgL_arg1746z00_1624 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol2478z00zz__rgc_expandz00,
																													BNIL);
																												{	/* Rgc/rgcexpand.scm 247 */
																													obj_t
																														BgL_arg1748z00_1626;
																													{	/* Rgc/rgcexpand.scm 247 */
																														obj_t
																															BgL_arg1749z00_1627;
																														BgL_arg1749z00_1627
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_symbol2315z00zz__rgc_expandz00,
																															BNIL);
																														BgL_arg1748z00_1626
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_symbol2480z00zz__rgc_expandz00,
																															BgL_arg1749z00_1627);
																													}
																													BgL_arg1747z00_1625 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1748z00_1626,
																														BNIL);
																												}
																												BgL_arg1745z00_1623 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1746z00_1624,
																													BgL_arg1747z00_1625);
																											}
																											BgL_arg1743z00_1621 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol2318z00zz__rgc_expandz00,
																												BgL_arg1745z00_1623);
																										}
																										{	/* Rgc/rgcexpand.scm 249 */
																											obj_t BgL_arg1750z00_1628;
																											obj_t BgL_arg1751z00_1629;

																											{	/* Rgc/rgcexpand.scm 249 */
																												obj_t
																													BgL_arg1752z00_1630;
																												{	/* Rgc/rgcexpand.scm 249 */
																													obj_t
																														BgL_arg1753z00_1631;
																													obj_t
																														BgL_arg1754z00_1632;
																													{	/* Rgc/rgcexpand.scm 249 */
																														obj_t
																															BgL_arg1755z00_1633;
																														{	/* Rgc/rgcexpand.scm 249 */
																															obj_t
																																BgL_arg1756z00_1634;
																															BgL_arg1756z00_1634
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol2406z00zz__rgc_expandz00,
																																BNIL);
																															BgL_arg1755z00_1633
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol2404z00zz__rgc_expandz00,
																																BgL_arg1756z00_1634);
																														}
																														BgL_arg1753z00_1631
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_symbol2482z00zz__rgc_expandz00,
																															BgL_arg1755z00_1633);
																													}
																													{	/* Rgc/rgcexpand.scm 250 */
																														obj_t
																															BgL_arg1757z00_1635;
																														{	/* Rgc/rgcexpand.scm 250 */
																															obj_t
																																BgL_arg1758z00_1636;
																															{	/* Rgc/rgcexpand.scm 250 */
																																obj_t
																																	BgL_arg1759z00_1637;
																																obj_t
																																	BgL_arg1760z00_1638;
																																{	/* Rgc/rgcexpand.scm 250 */
																																	obj_t
																																		BgL_arg1761z00_1639;
																																	{	/* Rgc/rgcexpand.scm 250 */
																																		obj_t
																																			BgL_arg1762z00_1640;
																																		BgL_arg1762z00_1640
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BINT(0L),
																																			BNIL);
																																		BgL_arg1761z00_1639
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BGl_symbol2406z00zz__rgc_expandz00,
																																			BgL_arg1762z00_1640);
																																	}
																																	BgL_arg1759z00_1637
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_symbol2408z00zz__rgc_expandz00,
																																		BgL_arg1761z00_1639);
																																}
																																{	/* Rgc/rgcexpand.scm 251 */
																																	obj_t
																																		BgL_arg1763z00_1641;
																																	obj_t
																																		BgL_arg1764z00_1642;
																																	{	/* Rgc/rgcexpand.scm 251 */
																																		obj_t
																																			BgL_arg1765z00_1643;
																																		{	/* Rgc/rgcexpand.scm 251 */
																																			obj_t
																																				BgL_arg1766z00_1644;
																																			obj_t
																																				BgL_arg1767z00_1645;
																																			{	/* Rgc/rgcexpand.scm 251 */
																																				obj_t
																																					BgL_arg1768z00_1646;
																																				{	/* Rgc/rgcexpand.scm 251 */
																																					obj_t
																																						BgL_arg1769z00_1647;
																																					{	/* Rgc/rgcexpand.scm 251 */
																																						obj_t
																																							BgL_arg1770z00_1648;
																																						{	/* Rgc/rgcexpand.scm 251 */
																																							obj_t
																																								BgL_arg1771z00_1649;
																																							{	/* Rgc/rgcexpand.scm 251 */
																																								obj_t
																																									BgL_arg1772z00_1650;
																																								obj_t
																																									BgL_arg1773z00_1651;
																																								BgL_arg1772z00_1650
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BGl_symbol2394z00zz__rgc_expandz00,
																																									BNIL);
																																								BgL_arg1773z00_1651
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BGl_symbol2406z00zz__rgc_expandz00,
																																									BNIL);
																																								BgL_arg1771z00_1649
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_arg1772z00_1650,
																																									BgL_arg1773z00_1651);
																																							}
																																							BgL_arg1770z00_1648
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BGl_symbol2410z00zz__rgc_expandz00,
																																								BgL_arg1771z00_1649);
																																						}
																																						BgL_arg1769z00_1647
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg1770z00_1648,
																																							BNIL);
																																					}
																																					BgL_arg1768z00_1646
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_symbol2350z00zz__rgc_expandz00,
																																						BgL_arg1769z00_1647);
																																				}
																																				BgL_arg1766z00_1644
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1768z00_1646,
																																					BNIL);
																																			}
																																			{	/* Rgc/rgcexpand.scm 252 */
																																				obj_t
																																					BgL_arg1774z00_1652;
																																				{	/* Rgc/rgcexpand.scm 252 */
																																					obj_t
																																						BgL_arg1775z00_1653;
																																					{	/* Rgc/rgcexpand.scm 252 */
																																						obj_t
																																							BgL_arg1777z00_1654;
																																						obj_t
																																							BgL_arg1779z00_1655;
																																						{	/* Rgc/rgcexpand.scm 252 */
																																							obj_t
																																								BgL_arg1781z00_1656;
																																							{	/* Rgc/rgcexpand.scm 252 */
																																								obj_t
																																									BgL_arg1782z00_1657;
																																								BgL_arg1782z00_1657
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BGl_symbol2404z00zz__rgc_expandz00,
																																									BNIL);
																																								BgL_arg1781z00_1656
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BGl_symbol2350z00zz__rgc_expandz00,
																																									BgL_arg1782z00_1657);
																																							}
																																							BgL_arg1777z00_1654
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BGl_symbol2416z00zz__rgc_expandz00,
																																								BgL_arg1781z00_1656);
																																						}
																																						{	/* Rgc/rgcexpand.scm 254 */
																																							obj_t
																																								BgL_arg1783z00_1658;
																																							obj_t
																																								BgL_arg1785z00_1659;
																																							{	/* Rgc/rgcexpand.scm 254 */
																																								obj_t
																																									BgL_arg1786z00_1660;
																																								{	/* Rgc/rgcexpand.scm 254 */
																																									obj_t
																																										BgL_arg1787z00_1661;
																																									{	/* Rgc/rgcexpand.scm 254 */
																																										obj_t
																																											BgL_arg1788z00_1662;
																																										{	/* Rgc/rgcexpand.scm 254 */
																																											obj_t
																																												BgL_arg1789z00_1663;
																																											{	/* Rgc/rgcexpand.scm 254 */
																																												obj_t
																																													BgL_arg1790z00_1664;
																																												{	/* Rgc/rgcexpand.scm 254 */
																																													obj_t
																																														BgL_arg1791z00_1665;
																																													BgL_arg1791z00_1665
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BGl_symbol2406z00zz__rgc_expandz00,
																																														BNIL);
																																													BgL_arg1790z00_1664
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BGl_symbol2404z00zz__rgc_expandz00,
																																														BgL_arg1791z00_1665);
																																												}
																																												BgL_arg1789z00_1663
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BGl_symbol2335z00zz__rgc_expandz00,
																																													BgL_arg1790z00_1664);
																																											}
																																											BgL_arg1788z00_1662
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_arg1789z00_1663,
																																												BNIL);
																																										}
																																										BgL_arg1787z00_1661
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BGl_string2465z00zz__rgc_expandz00,
																																											BgL_arg1788z00_1662);
																																									}
																																									BgL_arg1786z00_1660
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BGl_string2484z00zz__rgc_expandz00,
																																										BgL_arg1787z00_1661);
																																								}
																																								BgL_arg1783z00_1658
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BGl_symbol2430z00zz__rgc_expandz00,
																																									BgL_arg1786z00_1660);
																																							}
																																							{	/* Rgc/rgcexpand.scm 255 */
																																								obj_t
																																									BgL_arg1792z00_1666;
																																								{	/* Rgc/rgcexpand.scm 255 */
																																									obj_t
																																										BgL_arg1793z00_1667;
																																									{	/* Rgc/rgcexpand.scm 255 */
																																										obj_t
																																											BgL_arg1794z00_1668;
																																										{	/* Rgc/rgcexpand.scm 255 */
																																											obj_t
																																												BgL_arg1795z00_1669;
																																											BgL_arg1795z00_1669
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BGl_symbol2350z00zz__rgc_expandz00,
																																												BNIL);
																																											BgL_arg1794z00_1668
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BGl_symbol2404z00zz__rgc_expandz00,
																																												BgL_arg1795z00_1669);
																																										}
																																										BgL_arg1793z00_1667
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BGl_symbol2315z00zz__rgc_expandz00,
																																											BgL_arg1794z00_1668);
																																									}
																																									BgL_arg1792z00_1666
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BGl_symbol2485z00zz__rgc_expandz00,
																																										BgL_arg1793z00_1667);
																																								}
																																								BgL_arg1785z00_1659
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_arg1792z00_1666,
																																									BNIL);
																																							}
																																							BgL_arg1779z00_1655
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_arg1783z00_1658,
																																								BgL_arg1785z00_1659);
																																						}
																																						BgL_arg1775z00_1653
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg1777z00_1654,
																																							BgL_arg1779z00_1655);
																																					}
																																					BgL_arg1774z00_1652
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_symbol2432z00zz__rgc_expandz00,
																																						BgL_arg1775z00_1653);
																																				}
																																				BgL_arg1767z00_1645
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1774z00_1652,
																																					BNIL);
																																			}
																																			BgL_arg1765z00_1643
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1766z00_1644,
																																				BgL_arg1767z00_1645);
																																		}
																																		BgL_arg1763z00_1641
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BGl_symbol2309z00zz__rgc_expandz00,
																																			BgL_arg1765z00_1643);
																																	}
																																	{	/* Rgc/rgcexpand.scm 256 */
																																		obj_t
																																			BgL_arg1796z00_1670;
																																		{	/* Rgc/rgcexpand.scm 256 */
																																			obj_t
																																				BgL_arg1797z00_1671;
																																			{	/* Rgc/rgcexpand.scm 256 */
																																				obj_t
																																					BgL_arg1798z00_1672;
																																				obj_t
																																					BgL_arg1799z00_1673;
																																				{	/* Rgc/rgcexpand.scm 256 */
																																					obj_t
																																						BgL_arg1800z00_1674;
																																					{	/* Rgc/rgcexpand.scm 256 */
																																						obj_t
																																							BgL_arg1801z00_1675;
																																						obj_t
																																							BgL_arg1802z00_1676;
																																						{	/* Rgc/rgcexpand.scm 256 */
																																							obj_t
																																								BgL_arg1803z00_1677;
																																							{	/* Rgc/rgcexpand.scm 256 */
																																								obj_t
																																									BgL_arg1804z00_1678;
																																								BgL_arg1804z00_1678
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BINT
																																									(0L),
																																									BNIL);
																																								BgL_arg1803z00_1677
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BGl_symbol2404z00zz__rgc_expandz00,
																																									BgL_arg1804z00_1678);
																																							}
																																							BgL_arg1801z00_1675
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BGl_symbol2414z00zz__rgc_expandz00,
																																								BgL_arg1803z00_1677);
																																						}
																																						{	/* Rgc/rgcexpand.scm 256 */
																																							obj_t
																																								BgL_arg1805z00_1679;
																																							obj_t
																																								BgL_arg1806z00_1680;
																																							{	/* Rgc/rgcexpand.scm 256 */
																																								obj_t
																																									BgL_arg1807z00_1681;
																																								{	/* Rgc/rgcexpand.scm 256 */
																																									obj_t
																																										BgL_arg1808z00_1682;
																																									{	/* Rgc/rgcexpand.scm 256 */
																																										obj_t
																																											BgL_arg1809z00_1683;
																																										BgL_arg1809z00_1683
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BGl_symbol2394z00zz__rgc_expandz00,
																																											BNIL);
																																										BgL_arg1808z00_1682
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_arg1809z00_1683,
																																											BNIL);
																																									}
																																									BgL_arg1807z00_1681
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BGl_symbol2406z00zz__rgc_expandz00,
																																										BgL_arg1808z00_1682);
																																								}
																																								BgL_arg1805z00_1679
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BGl_symbol2416z00zz__rgc_expandz00,
																																									BgL_arg1807z00_1681);
																																							}
																																							{	/* Rgc/rgcexpand.scm 256 */
																																								obj_t
																																									BgL_arg1810z00_1684;
																																								{	/* Rgc/rgcexpand.scm 256 */
																																									obj_t
																																										BgL_arg1811z00_1685;
																																									{	/* Rgc/rgcexpand.scm 256 */
																																										obj_t
																																											BgL_arg1812z00_1686;
																																										BgL_arg1812z00_1686
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BGl_symbol2404z00zz__rgc_expandz00,
																																											BNIL);
																																										BgL_arg1811z00_1685
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BGl_symbol2406z00zz__rgc_expandz00,
																																											BgL_arg1812z00_1686);
																																									}
																																									BgL_arg1810z00_1684
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BGl_symbol2414z00zz__rgc_expandz00,
																																										BgL_arg1811z00_1685);
																																								}
																																								BgL_arg1806z00_1680
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_arg1810z00_1684,
																																									BNIL);
																																							}
																																							BgL_arg1802z00_1676
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_arg1805z00_1679,
																																								BgL_arg1806z00_1680);
																																						}
																																						BgL_arg1800z00_1674
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg1801z00_1675,
																																							BgL_arg1802z00_1676);
																																					}
																																					BgL_arg1798z00_1672
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_symbol2418z00zz__rgc_expandz00,
																																						BgL_arg1800z00_1674);
																																				}
																																				{	/* Rgc/rgcexpand.scm 257 */
																																					obj_t
																																						BgL_arg1813z00_1687;
																																					obj_t
																																						BgL_arg1814z00_1688;
																																					{	/* Rgc/rgcexpand.scm 257 */
																																						obj_t
																																							BgL_arg1815z00_1689;
																																						{	/* Rgc/rgcexpand.scm 257 */
																																							obj_t
																																								BgL_arg1816z00_1690;
																																							{	/* Rgc/rgcexpand.scm 257 */
																																								obj_t
																																									BgL_arg1817z00_1691;
																																								BgL_arg1817z00_1691
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BGl_symbol2406z00zz__rgc_expandz00,
																																									BNIL);
																																								BgL_arg1816z00_1690
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BGl_symbol2404z00zz__rgc_expandz00,
																																									BgL_arg1817z00_1691);
																																							}
																																							BgL_arg1815z00_1689
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BGl_symbol2315z00zz__rgc_expandz00,
																																								BgL_arg1816z00_1690);
																																						}
																																						BgL_arg1813z00_1687
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BGl_symbol2485z00zz__rgc_expandz00,
																																							BgL_arg1815z00_1689);
																																					}
																																					{	/* Rgc/rgcexpand.scm 259 */
																																						obj_t
																																							BgL_arg1818z00_1692;
																																						{	/* Rgc/rgcexpand.scm 259 */
																																							obj_t
																																								BgL_arg1819z00_1693;
																																							{	/* Rgc/rgcexpand.scm 259 */
																																								obj_t
																																									BgL_arg1820z00_1694;
																																								{	/* Rgc/rgcexpand.scm 259 */
																																									obj_t
																																										BgL_arg1822z00_1695;
																																									{	/* Rgc/rgcexpand.scm 259 */
																																										obj_t
																																											BgL_arg1823z00_1696;
																																										{	/* Rgc/rgcexpand.scm 259 */
																																											obj_t
																																												BgL_arg1826z00_1697;
																																											{	/* Rgc/rgcexpand.scm 259 */
																																												obj_t
																																													BgL_arg1827z00_1698;
																																												BgL_arg1827z00_1698
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BGl_symbol2406z00zz__rgc_expandz00,
																																													BNIL);
																																												BgL_arg1826z00_1697
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BGl_symbol2404z00zz__rgc_expandz00,
																																													BgL_arg1827z00_1698);
																																											}
																																											BgL_arg1823z00_1696
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BGl_symbol2335z00zz__rgc_expandz00,
																																												BgL_arg1826z00_1697);
																																										}
																																										BgL_arg1822z00_1695
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_arg1823z00_1696,
																																											BNIL);
																																									}
																																									BgL_arg1820z00_1694
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BGl_string2465z00zz__rgc_expandz00,
																																										BgL_arg1822z00_1695);
																																								}
																																								BgL_arg1819z00_1693
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BGl_string2484z00zz__rgc_expandz00,
																																									BgL_arg1820z00_1694);
																																							}
																																							BgL_arg1818z00_1692
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BGl_symbol2430z00zz__rgc_expandz00,
																																								BgL_arg1819z00_1693);
																																						}
																																						BgL_arg1814z00_1688
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg1818z00_1692,
																																							BNIL);
																																					}
																																					BgL_arg1799z00_1673
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg1813z00_1687,
																																						BgL_arg1814z00_1688);
																																				}
																																				BgL_arg1797z00_1671
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1798z00_1672,
																																					BgL_arg1799z00_1673);
																																			}
																																			BgL_arg1796z00_1670
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_symbol2432z00zz__rgc_expandz00,
																																				BgL_arg1797z00_1671);
																																		}
																																		BgL_arg1764z00_1642
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1796z00_1670,
																																			BNIL);
																																	}
																																	BgL_arg1760z00_1638
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1763z00_1641,
																																		BgL_arg1764z00_1642);
																																}
																																BgL_arg1758z00_1636
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1759z00_1637,
																																	BgL_arg1760z00_1638);
																															}
																															BgL_arg1757z00_1635
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol2432z00zz__rgc_expandz00,
																																BgL_arg1758z00_1636);
																														}
																														BgL_arg1754z00_1632
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1757z00_1635,
																															BNIL);
																													}
																													BgL_arg1752z00_1630 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1753z00_1631,
																														BgL_arg1754z00_1632);
																												}
																												BgL_arg1750z00_1628 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol2318z00zz__rgc_expandz00,
																													BgL_arg1752z00_1630);
																											}
																											{	/* Rgc/rgcexpand.scm 261 */
																												obj_t
																													BgL_arg1828z00_1699;
																												obj_t
																													BgL_arg1829z00_1700;
																												{	/* Rgc/rgcexpand.scm 261 */
																													obj_t
																														BgL_arg1831z00_1701;
																													{	/* Rgc/rgcexpand.scm 261 */
																														obj_t
																															BgL_arg1832z00_1702;
																														obj_t
																															BgL_arg1833z00_1703;
																														BgL_arg1832z00_1702
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_symbol2487z00zz__rgc_expandz00,
																															BNIL);
																														{	/* Rgc/rgcexpand.scm 262 */
																															obj_t
																																BgL_arg1834z00_1704;
																															{	/* Rgc/rgcexpand.scm 262 */
																																obj_t
																																	BgL_arg1835z00_1705;
																																BgL_arg1835z00_1705
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BGl_symbol2315z00zz__rgc_expandz00,
																																	BNIL);
																																BgL_arg1834z00_1704
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BGl_symbol2489z00zz__rgc_expandz00,
																																	BgL_arg1835z00_1705);
																															}
																															BgL_arg1833z00_1703
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1834z00_1704,
																																BNIL);
																														}
																														BgL_arg1831z00_1701
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1832z00_1702,
																															BgL_arg1833z00_1703);
																													}
																													BgL_arg1828z00_1699 =
																														MAKE_YOUNG_PAIR
																														(BGl_symbol2318z00zz__rgc_expandz00,
																														BgL_arg1831z00_1701);
																												}
																												{	/* Rgc/rgcexpand.scm 264 */
																													obj_t
																														BgL_arg1836z00_1706;
																													obj_t
																														BgL_arg1837z00_1707;
																													{	/* Rgc/rgcexpand.scm 264 */
																														obj_t
																															BgL_arg1838z00_1708;
																														{	/* Rgc/rgcexpand.scm 264 */
																															obj_t
																																BgL_arg1839z00_1709;
																															obj_t
																																BgL_arg1840z00_1710;
																															BgL_arg1839z00_1709
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol2491z00zz__rgc_expandz00,
																																BNIL);
																															{	/* Rgc/rgcexpand.scm 265 */
																																obj_t
																																	BgL_arg1842z00_1711;
																																{	/* Rgc/rgcexpand.scm 265 */
																																	obj_t
																																		BgL_arg1843z00_1712;
																																	BgL_arg1843z00_1712
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_symbol2315z00zz__rgc_expandz00,
																																		BNIL);
																																	BgL_arg1842z00_1711
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_symbol2493z00zz__rgc_expandz00,
																																		BgL_arg1843z00_1712);
																																}
																																BgL_arg1840z00_1710
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1842z00_1711,
																																	BNIL);
																															}
																															BgL_arg1838z00_1708
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1839z00_1709,
																																BgL_arg1840z00_1710);
																														}
																														BgL_arg1836z00_1706
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_symbol2318z00zz__rgc_expandz00,
																															BgL_arg1838z00_1708);
																													}
																													{	/* Rgc/rgcexpand.scm 267 */
																														obj_t
																															BgL_arg1844z00_1713;
																														obj_t
																															BgL_arg1845z00_1714;
																														{	/* Rgc/rgcexpand.scm 267 */
																															obj_t
																																BgL_arg1846z00_1715;
																															{	/* Rgc/rgcexpand.scm 267 */
																																obj_t
																																	BgL_arg1847z00_1716;
																																obj_t
																																	BgL_arg1848z00_1717;
																																BgL_arg1847z00_1716
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BGl_symbol2495z00zz__rgc_expandz00,
																																	BNIL);
																																{	/* Rgc/rgcexpand.scm 268 */
																																	obj_t
																																		BgL_arg1849z00_1718;
																																	{	/* Rgc/rgcexpand.scm 268 */
																																		obj_t
																																			BgL_arg1850z00_1719;
																																		BgL_arg1850z00_1719
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BGl_symbol2315z00zz__rgc_expandz00,
																																			BNIL);
																																		BgL_arg1849z00_1718
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BGl_symbol2497z00zz__rgc_expandz00,
																																			BgL_arg1850z00_1719);
																																	}
																																	BgL_arg1848z00_1717
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1849z00_1718,
																																		BNIL);
																																}
																																BgL_arg1846z00_1715
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1847z00_1716,
																																	BgL_arg1848z00_1717);
																															}
																															BgL_arg1844z00_1713
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol2318z00zz__rgc_expandz00,
																																BgL_arg1846z00_1715);
																														}
																														{	/* Rgc/rgcexpand.scm 270 */
																															obj_t
																																BgL_arg1851z00_1720;
																															obj_t
																																BgL_arg1852z00_1721;
																															{	/* Rgc/rgcexpand.scm 270 */
																																obj_t
																																	BgL_arg1853z00_1722;
																																{	/* Rgc/rgcexpand.scm 270 */
																																	obj_t
																																		BgL_arg1854z00_1723;
																																	obj_t
																																		BgL_arg1856z00_1724;
																																	BgL_arg1854z00_1723
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_symbol2499z00zz__rgc_expandz00,
																																		BNIL);
																																	{	/* Rgc/rgcexpand.scm 271 */
																																		obj_t
																																			BgL_arg1857z00_1725;
																																		{	/* Rgc/rgcexpand.scm 271 */
																																			obj_t
																																				BgL_arg1858z00_1726;
																																			{	/* Rgc/rgcexpand.scm 271 */
																																				obj_t
																																					BgL_arg1859z00_1727;
																																				obj_t
																																					BgL_arg1860z00_1728;
																																				{	/* Rgc/rgcexpand.scm 271 */
																																					obj_t
																																						BgL_arg1862z00_1729;
																																					{	/* Rgc/rgcexpand.scm 271 */
																																						obj_t
																																							BgL_arg1863z00_1730;
																																						obj_t
																																							BgL_arg1864z00_1731;
																																						{	/* Rgc/rgcexpand.scm 271 */
																																							obj_t
																																								BgL_arg1866z00_1732;
																																							BgL_arg1866z00_1732
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BGl_symbol2315z00zz__rgc_expandz00,
																																								BNIL);
																																							BgL_arg1863z00_1730
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BGl_symbol2445z00zz__rgc_expandz00,
																																								BgL_arg1866z00_1732);
																																						}
																																						BgL_arg1864z00_1731
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BINT
																																							(0L),
																																							BNIL);
																																						BgL_arg1862z00_1729
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg1863z00_1730,
																																							BgL_arg1864z00_1731);
																																					}
																																					BgL_arg1859z00_1727
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_symbol2501z00zz__rgc_expandz00,
																																						BgL_arg1862z00_1729);
																																				}
																																				{	/* Rgc/rgcexpand.scm 273 */
																																					obj_t
																																						BgL_arg1868z00_1733;
																																					obj_t
																																						BgL_arg1869z00_1734;
																																					BgL_arg1868z00_1733
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_symbol2503z00zz__rgc_expandz00,
																																						BNIL);
																																					{	/* Rgc/rgcexpand.scm 274 */
																																						obj_t
																																							BgL_arg1870z00_1735;
																																						{	/* Rgc/rgcexpand.scm 274 */
																																							obj_t
																																								BgL_arg1872z00_1736;
																																							BgL_arg1872z00_1736
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BGl_symbol2315z00zz__rgc_expandz00,
																																								BNIL);
																																							BgL_arg1870z00_1735
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BGl_symbol2378z00zz__rgc_expandz00,
																																								BgL_arg1872z00_1736);
																																						}
																																						BgL_arg1869z00_1734
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg1870z00_1735,
																																							BNIL);
																																					}
																																					BgL_arg1860z00_1728
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg1868z00_1733,
																																						BgL_arg1869z00_1734);
																																				}
																																				BgL_arg1858z00_1726
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1859z00_1727,
																																					BgL_arg1860z00_1728);
																																			}
																																			BgL_arg1857z00_1725
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_symbol2432z00zz__rgc_expandz00,
																																				BgL_arg1858z00_1726);
																																		}
																																		BgL_arg1856z00_1724
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1857z00_1725,
																																			BNIL);
																																	}
																																	BgL_arg1853z00_1722
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1854z00_1723,
																																		BgL_arg1856z00_1724);
																																}
																																BgL_arg1851z00_1720
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BGl_symbol2318z00zz__rgc_expandz00,
																																	BgL_arg1853z00_1722);
																															}
																															{	/* Rgc/rgcexpand.scm 276 */
																																obj_t
																																	BgL_arg1873z00_1737;
																																obj_t
																																	BgL_arg1874z00_1738;
																																{	/* Rgc/rgcexpand.scm 276 */
																																	obj_t
																																		BgL_arg1875z00_1739;
																																	{	/* Rgc/rgcexpand.scm 276 */
																																		obj_t
																																			BgL_arg1876z00_1740;
																																		obj_t
																																			BgL_arg1877z00_1741;
																																		BgL_arg1876z00_1740
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BGl_symbol2505z00zz__rgc_expandz00,
																																			BNIL);
																																		BgL_arg1877z00_1741
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BGl_symbol2313z00zz__rgc_expandz00,
																																			BNIL);
																																		BgL_arg1875z00_1739
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1876z00_1740,
																																			BgL_arg1877z00_1741);
																																	}
																																	BgL_arg1873z00_1737
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_symbol2318z00zz__rgc_expandz00,
																																		BgL_arg1875z00_1739);
																																}
																																{	/* Rgc/rgcexpand.scm 279 */
																																	obj_t
																																		BgL_arg1878z00_1742;
																																	obj_t
																																		BgL_arg1879z00_1743;
																																	{	/* Rgc/rgcexpand.scm 279 */
																																		obj_t
																																			BgL_arg1880z00_1744;
																																		{	/* Rgc/rgcexpand.scm 279 */
																																			obj_t
																																				BgL_arg1882z00_1745;
																																			obj_t
																																				BgL_arg1883z00_1746;
																																			{	/* Rgc/rgcexpand.scm 279 */
																																				obj_t
																																					BgL_arg1884z00_1747;
																																				BgL_arg1884z00_1747
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BGl_symbol2507z00zz__rgc_expandz00,
																																					BNIL);
																																				BgL_arg1882z00_1745
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BGl_symbol2509z00zz__rgc_expandz00,
																																					BgL_arg1884z00_1747);
																																			}
																																			{	/* Rgc/rgcexpand.scm 280 */
																																				obj_t
																																					BgL_arg1885z00_1748;
																																				{	/* Rgc/rgcexpand.scm 280 */
																																					obj_t
																																						BgL_arg1887z00_1749;
																																					{	/* Rgc/rgcexpand.scm 280 */
																																						obj_t
																																							BgL_arg1888z00_1750;
																																						BgL_arg1888z00_1750
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BGl_symbol2507z00zz__rgc_expandz00,
																																							BNIL);
																																						BgL_arg1887z00_1749
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BGl_symbol2313z00zz__rgc_expandz00,
																																							BgL_arg1888z00_1750);
																																					}
																																					BgL_arg1885z00_1748
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_symbol2511z00zz__rgc_expandz00,
																																						BgL_arg1887z00_1749);
																																				}
																																				BgL_arg1883z00_1746
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1885z00_1748,
																																					BNIL);
																																			}
																																			BgL_arg1880z00_1744
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1882z00_1745,
																																				BgL_arg1883z00_1746);
																																		}
																																		BgL_arg1878z00_1742
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BGl_symbol2318z00zz__rgc_expandz00,
																																			BgL_arg1880z00_1744);
																																	}
																																	{	/* Rgc/rgcexpand.scm 282 */
																																		obj_t
																																			BgL_arg1889z00_1751;
																																		obj_t
																																			BgL_arg1890z00_1752;
																																		{	/* Rgc/rgcexpand.scm 282 */
																																			obj_t
																																				BgL_arg1891z00_1753;
																																			{	/* Rgc/rgcexpand.scm 282 */
																																				obj_t
																																					BgL_arg1892z00_1754;
																																				obj_t
																																					BgL_arg1893z00_1755;
																																				{	/* Rgc/rgcexpand.scm 282 */
																																					obj_t
																																						BgL_arg1894z00_1756;
																																					BgL_arg1894z00_1756
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_symbol2507z00zz__rgc_expandz00,
																																						BNIL);
																																					BgL_arg1892z00_1754
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_symbol2513z00zz__rgc_expandz00,
																																						BgL_arg1894z00_1756);
																																				}
																																				{	/* Rgc/rgcexpand.scm 283 */
																																					obj_t
																																						BgL_arg1896z00_1757;
																																					{	/* Rgc/rgcexpand.scm 283 */
																																						obj_t
																																							BgL_arg1897z00_1758;
																																						{	/* Rgc/rgcexpand.scm 283 */
																																							obj_t
																																								BgL_arg1898z00_1759;
																																							BgL_arg1898z00_1759
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BGl_symbol2507z00zz__rgc_expandz00,
																																								BNIL);
																																							BgL_arg1897z00_1758
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BGl_symbol2313z00zz__rgc_expandz00,
																																								BgL_arg1898z00_1759);
																																						}
																																						BgL_arg1896z00_1757
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BGl_symbol2330z00zz__rgc_expandz00,
																																							BgL_arg1897z00_1758);
																																					}
																																					BgL_arg1893z00_1755
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg1896z00_1757,
																																						BNIL);
																																				}
																																				BgL_arg1891z00_1753
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1892z00_1754,
																																					BgL_arg1893z00_1755);
																																			}
																																			BgL_arg1889z00_1751
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_symbol2318z00zz__rgc_expandz00,
																																				BgL_arg1891z00_1753);
																																		}
																																		{	/* Rgc/rgcexpand.scm 284 */
																																			obj_t
																																				BgL_arg1899z00_1760;
																																			obj_t
																																				BgL_arg1901z00_1761;
																																			{	/* Rgc/rgcexpand.scm 284 */
																																				obj_t
																																					BgL_arg1902z00_1762;
																																				{	/* Rgc/rgcexpand.scm 284 */
																																					obj_t
																																						BgL_arg1903z00_1763;
																																					obj_t
																																						BgL_arg1904z00_1764;
																																					BgL_arg1903z00_1763
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_symbol2515z00zz__rgc_expandz00,
																																						BGl_symbol2507z00zz__rgc_expandz00);
																																					{	/* Rgc/rgcexpand.scm 285 */
																																						obj_t
																																							BgL_arg1906z00_1765;
																																						{	/* Rgc/rgcexpand.scm 285 */
																																							obj_t
																																								BgL_arg1910z00_1766;
																																							{	/* Rgc/rgcexpand.scm 285 */
																																								obj_t
																																									BgL_arg1911z00_1767;
																																								obj_t
																																									BgL_arg1912z00_1768;
																																								{	/* Rgc/rgcexpand.scm 285 */
																																									obj_t
																																										BgL_arg1913z00_1769;
																																									BgL_arg1913z00_1769
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BGl_symbol2507z00zz__rgc_expandz00,
																																										BNIL);
																																									BgL_arg1911z00_1767
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BGl_symbol2517z00zz__rgc_expandz00,
																																										BgL_arg1913z00_1769);
																																								}
																																								{	/* Rgc/rgcexpand.scm 286 */
																																									obj_t
																																										BgL_arg1914z00_1770;
																																									obj_t
																																										BgL_arg1916z00_1771;
																																									{	/* Rgc/rgcexpand.scm 286 */
																																										obj_t
																																											BgL_arg1917z00_1772;
																																										{	/* Rgc/rgcexpand.scm 286 */
																																											obj_t
																																												BgL_arg1918z00_1773;
																																											{	/* Rgc/rgcexpand.scm 286 */
																																												obj_t
																																													BgL_arg1919z00_1774;
																																												{	/* Rgc/rgcexpand.scm 286 */
																																													obj_t
																																														BgL_arg1920z00_1775;
																																													BgL_arg1920z00_1775
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BGl_symbol2507z00zz__rgc_expandz00,
																																														BNIL);
																																													BgL_arg1919z00_1774
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BGl_symbol2519z00zz__rgc_expandz00,
																																														BgL_arg1920z00_1775);
																																												}
																																												BgL_arg1918z00_1773
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_arg1919z00_1774,
																																													BNIL);
																																											}
																																											BgL_arg1917z00_1772
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BGl_symbol2313z00zz__rgc_expandz00,
																																												BgL_arg1918z00_1773);
																																										}
																																										BgL_arg1914z00_1770
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BGl_symbol2330z00zz__rgc_expandz00,
																																											BgL_arg1917z00_1772);
																																									}
																																									{	/* Rgc/rgcexpand.scm 287 */
																																										obj_t
																																											BgL_arg1923z00_1776;
																																										{	/* Rgc/rgcexpand.scm 287 */
																																											obj_t
																																												BgL_arg1924z00_1777;
																																											{	/* Rgc/rgcexpand.scm 287 */
																																												obj_t
																																													BgL_arg1925z00_1778;
																																												BgL_arg1925z00_1778
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BUNSPEC,
																																													BNIL);
																																												BgL_arg1924z00_1777
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BGl_symbol2313z00zz__rgc_expandz00,
																																													BgL_arg1925z00_1778);
																																											}
																																											BgL_arg1923z00_1776
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BGl_symbol2330z00zz__rgc_expandz00,
																																												BgL_arg1924z00_1777);
																																										}
																																										BgL_arg1916z00_1771
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_arg1923z00_1776,
																																											BNIL);
																																									}
																																									BgL_arg1912z00_1768
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_arg1914z00_1770,
																																										BgL_arg1916z00_1771);
																																								}
																																								BgL_arg1910z00_1766
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_arg1911z00_1767,
																																									BgL_arg1912z00_1768);
																																							}
																																							BgL_arg1906z00_1765
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BGl_symbol2432z00zz__rgc_expandz00,
																																								BgL_arg1910z00_1766);
																																						}
																																						BgL_arg1904z00_1764
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg1906z00_1765,
																																							BNIL);
																																					}
																																					BgL_arg1902z00_1762
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg1903z00_1763,
																																						BgL_arg1904z00_1764);
																																				}
																																				BgL_arg1899z00_1760
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BGl_symbol2318z00zz__rgc_expandz00,
																																					BgL_arg1902z00_1762);
																																			}
																																			{	/* Rgc/rgcexpand.scm 291 */
																																				obj_t
																																					BgL_arg1926z00_1779;
																																				{	/* Rgc/rgcexpand.scm 291 */
																																					obj_t
																																						BgL_arg1927z00_1780;
																																					obj_t
																																						BgL_arg1928z00_1781;
																																					{	/* Rgc/rgcexpand.scm 291 */
																																						obj_t
																																							BgL_arg1929z00_1782;
																																						{	/* Rgc/rgcexpand.scm 291 */
																																							obj_t
																																								BgL_arg1930z00_1783;
																																							obj_t
																																								BgL_arg1931z00_1784;
																																							BgL_arg1930z00_1783
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BGl_symbol2521z00zz__rgc_expandz00,
																																								BNIL);
																																							{	/* Rgc/rgcexpand.scm 292 */
																																								obj_t
																																									BgL_arg1932z00_1785;
																																								obj_t
																																									BgL_arg1933z00_1786;
																																								{	/* Rgc/rgcexpand.scm 292 */
																																									obj_t
																																										BgL_arg1934z00_1787;
																																									BgL_arg1934z00_1787
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BGl_symbol2315z00zz__rgc_expandz00,
																																										BNIL);
																																									BgL_arg1932z00_1785
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BGl_symbol2523z00zz__rgc_expandz00,
																																										BgL_arg1934z00_1787);
																																								}
																																								{	/* Rgc/rgcexpand.scm 293 */
																																									obj_t
																																										BgL_arg1935z00_1788;
																																									obj_t
																																										BgL_arg1936z00_1789;
																																									if (CBOOL(BgL_submatchzf3zf3_11))
																																										{	/* Rgc/rgcexpand.scm 294 */
																																											obj_t
																																												BgL_list1937z00_1790;
																																											BgL_list1937z00_1790
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BGl_list2525z00zz__rgc_expandz00,
																																												BNIL);
																																											BgL_arg1935z00_1788
																																												=
																																												BgL_list1937z00_1790;
																																										}
																																									else
																																										{	/* Rgc/rgcexpand.scm 293 */
																																											BgL_arg1935z00_1788
																																												=
																																												BNIL;
																																										}
																																									{	/* Rgc/rgcexpand.scm 296 */
																																										obj_t
																																											BgL_arg1938z00_1791;
																																										{	/* Rgc/rgcexpand.scm 296 */
																																											obj_t
																																												BgL_arg1939z00_1792;
																																											{	/* Rgc/rgcexpand.scm 296 */
																																												obj_t
																																													BgL_arg1940z00_1793;
																																												obj_t
																																													BgL_arg1941z00_1794;
																																												{	/* Rgc/rgcexpand.scm 296 */
																																													obj_t
																																														BgL_arg1942z00_1795;
																																													{	/* Rgc/rgcexpand.scm 296 */
																																														obj_t
																																															BgL_arg1943z00_1796;
																																														{	/* Rgc/rgcexpand.scm 296 */
																																															obj_t
																																																BgL_arg1944z00_1797;
																																															{	/* Rgc/rgcexpand.scm 296 */
																																																obj_t
																																																	BgL_arg1945z00_1798;
																																																obj_t
																																																	BgL_arg1946z00_1799;
																																																BgL_arg1945z00_1798
																																																	=
																																																	BGl_statezd2namezd2zz__rgc_dfaz00
																																																	(BGl_getzd2initialzd2statez00zz__rgc_dfaz00
																																																	());
																																																{	/* Rgc/rgcexpand.scm 299 */
																																																	obj_t
																																																		BgL_arg1948z00_1801;
																																																	{	/* Rgc/rgcexpand.scm 299 */
																																																		obj_t
																																																			BgL_arg1949z00_1802;
																																																		{	/* Rgc/rgcexpand.scm 299 */
																																																			obj_t
																																																				BgL_arg1950z00_1803;
																																																			obj_t
																																																				BgL_arg1951z00_1804;
																																																			{	/* Rgc/rgcexpand.scm 299 */
																																																				obj_t
																																																					BgL_arg1952z00_1805;
																																																				BgL_arg1952z00_1805
																																																					=
																																																					MAKE_YOUNG_PAIR
																																																					(BGl_symbol2315z00zz__rgc_expandz00,
																																																					BNIL);
																																																				BgL_arg1950z00_1803
																																																					=
																																																					MAKE_YOUNG_PAIR
																																																					(BGl_symbol2526z00zz__rgc_expandz00,
																																																					BgL_arg1952z00_1805);
																																																			}
																																																			{	/* Rgc/rgcexpand.scm 300 */
																																																				obj_t
																																																					BgL_arg1953z00_1806;
																																																				{	/* Rgc/rgcexpand.scm 300 */
																																																					obj_t
																																																						BgL_arg1954z00_1807;
																																																					BgL_arg1954z00_1807
																																																						=
																																																						MAKE_YOUNG_PAIR
																																																						(BGl_symbol2315z00zz__rgc_expandz00,
																																																						BNIL);
																																																					BgL_arg1953z00_1806
																																																						=
																																																						MAKE_YOUNG_PAIR
																																																						(BGl_symbol2528z00zz__rgc_expandz00,
																																																						BgL_arg1954z00_1807);
																																																				}
																																																				BgL_arg1951z00_1804
																																																					=
																																																					MAKE_YOUNG_PAIR
																																																					(BgL_arg1953z00_1806,
																																																					BNIL);
																																																			}
																																																			BgL_arg1949z00_1802
																																																				=
																																																				MAKE_YOUNG_PAIR
																																																				(BgL_arg1950z00_1803,
																																																				BgL_arg1951z00_1804);
																																																		}
																																																		BgL_arg1948z00_1801
																																																			=
																																																			MAKE_YOUNG_PAIR
																																																			(BgL_elsezd2numzd2_10,
																																																			BgL_arg1949z00_1802);
																																																	}
																																																	BgL_arg1946z00_1799
																																																		=
																																																		MAKE_YOUNG_PAIR
																																																		(BGl_symbol2315z00zz__rgc_expandz00,
																																																		BgL_arg1948z00_1801);
																																																}
																																																BgL_arg1944z00_1797
																																																	=
																																																	MAKE_YOUNG_PAIR
																																																	(BgL_arg1945z00_1798,
																																																	BgL_arg1946z00_1799);
																																															}
																																															BgL_arg1943z00_1796
																																																=
																																																MAKE_YOUNG_PAIR
																																																(BgL_arg1944z00_1797,
																																																BNIL);
																																														}
																																														BgL_arg1942z00_1795
																																															=
																																															MAKE_YOUNG_PAIR
																																															(BGl_symbol2530z00zz__rgc_expandz00,
																																															BgL_arg1943z00_1796);
																																													}
																																													BgL_arg1940z00_1793
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BgL_arg1942z00_1795,
																																														BNIL);
																																												}
																																												{	/* Rgc/rgcexpand.scm 301 */
																																													obj_t
																																														BgL_arg1955z00_1808;
																																													obj_t
																																														BgL_arg1956z00_1809;
																																													{	/* Rgc/rgcexpand.scm 301 */
																																														obj_t
																																															BgL_arg1957z00_1810;
																																														BgL_arg1957z00_1810
																																															=
																																															MAKE_YOUNG_PAIR
																																															(BGl_symbol2315z00zz__rgc_expandz00,
																																															BNIL);
																																														BgL_arg1955z00_1808
																																															=
																																															MAKE_YOUNG_PAIR
																																															(BGl_symbol2532z00zz__rgc_expandz00,
																																															BgL_arg1957z00_1810);
																																													}
																																													{	/* Rgc/rgcexpand.scm 302 */
																																														obj_t
																																															BgL_arg1958z00_1811;
																																														obj_t
																																															BgL_arg1959z00_1812;
																																														if (CBOOL(BgL_submatchzf3zf3_11))
																																															{	/* Rgc/rgcexpand.scm 302 */
																																																BgL_arg1958z00_1811
																																																	=
																																																	BGl_list2534z00zz__rgc_expandz00;
																																															}
																																														else
																																															{	/* Rgc/rgcexpand.scm 302 */
																																																BgL_arg1958z00_1811
																																																	=
																																																	BNIL;
																																															}
																																														{	/* Rgc/rgcexpand.scm 317 */
																																															obj_t
																																																BgL_arg1960z00_1813;
																																															{	/* Rgc/rgcexpand.scm 317 */
																																																obj_t
																																																	BgL_arg1961z00_1814;
																																																{	/* Rgc/rgcexpand.scm 317 */
																																																	obj_t
																																																		BgL_arg1962z00_1815;
																																																	{	/* Rgc/rgcexpand.scm 317 */
																																																		obj_t
																																																			BgL_arg1963z00_1816;
																																																		obj_t
																																																			BgL_arg1964z00_1817;
																																																		{
																																																			obj_t
																																																				BgL_actionsz00_2321;
																																																			long
																																																				BgL_numz00_2322;
																																																			obj_t
																																																				BgL_resz00_2323;
																																																			BgL_actionsz00_2321
																																																				=
																																																				BgL_actionsz00_9;
																																																			BgL_numz00_2322
																																																				=
																																																				0L;
																																																			BgL_resz00_2323
																																																				=
																																																				BNIL;
																																																		BgL_loopz00_2320:
																																																			if (NULLP(BgL_actionsz00_2321))
																																																				{	/* Rgc/rgcexpand.scm 320 */
																																																					BgL_arg1963z00_1816
																																																						=
																																																						BgL_resz00_2323;
																																																				}
																																																			else
																																																				{	/* Rgc/rgcexpand.scm 322 */
																																																					obj_t
																																																						BgL_arg1967z00_2333;
																																																					long
																																																						BgL_arg1968z00_2334;
																																																					obj_t
																																																						BgL_arg1969z00_2335;
																																																					BgL_arg1967z00_2333
																																																						=
																																																						CDR
																																																						(
																																																						((obj_t) BgL_actionsz00_2321));
																																																					BgL_arg1968z00_2334
																																																						=
																																																						(BgL_numz00_2322
																																																						+
																																																						1L);
																																																					{	/* Rgc/rgcexpand.scm 324 */
																																																						obj_t
																																																							BgL_arg1970z00_2336;
																																																						{	/* Rgc/rgcexpand.scm 324 */
																																																							obj_t
																																																								BgL_arg1971z00_2337;
																																																							obj_t
																																																								BgL_arg1972z00_2338;
																																																							BgL_arg1971z00_2337
																																																								=
																																																								MAKE_YOUNG_PAIR
																																																								(BINT
																																																								(BgL_numz00_2322),
																																																								BNIL);
																																																							{	/* Rgc/rgcexpand.scm 324 */
																																																								obj_t
																																																									BgL_arg1973z00_2339;
																																																								BgL_arg1973z00_2339
																																																									=
																																																									CAR
																																																									(
																																																									((obj_t) BgL_actionsz00_2321));
																																																								BgL_arg1972z00_2338
																																																									=
																																																									MAKE_YOUNG_PAIR
																																																									(BgL_arg1973z00_2339,
																																																									BNIL);
																																																							}
																																																							BgL_arg1970z00_2336
																																																								=
																																																								MAKE_YOUNG_PAIR
																																																								(BgL_arg1971z00_2337,
																																																								BgL_arg1972z00_2338);
																																																						}
																																																						BgL_arg1969z00_2335
																																																							=
																																																							MAKE_YOUNG_PAIR
																																																							(BgL_arg1970z00_2336,
																																																							BgL_resz00_2323);
																																																					}
																																																					{
																																																						obj_t
																																																							BgL_resz00_3626;
																																																						long
																																																							BgL_numz00_3625;
																																																						obj_t
																																																							BgL_actionsz00_3624;
																																																						BgL_actionsz00_3624
																																																							=
																																																							BgL_arg1967z00_2333;
																																																						BgL_numz00_3625
																																																							=
																																																							BgL_arg1968z00_2334;
																																																						BgL_resz00_3626
																																																							=
																																																							BgL_arg1969z00_2335;
																																																						BgL_resz00_2323
																																																							=
																																																							BgL_resz00_3626;
																																																						BgL_numz00_2322
																																																							=
																																																							BgL_numz00_3625;
																																																						BgL_actionsz00_2321
																																																							=
																																																							BgL_actionsz00_3624;
																																																						goto
																																																							BgL_loopz00_2320;
																																																					}
																																																				}
																																																		}
																																																		{	/* Rgc/rgcexpand.scm 326 */
																																																			obj_t
																																																				BgL_arg1974z00_1833;
																																																			{	/* Rgc/rgcexpand.scm 326 */
																																																				obj_t
																																																					BgL_arg1975z00_1834;
																																																				{	/* Rgc/rgcexpand.scm 326 */
																																																					obj_t
																																																						BgL_arg1976z00_1835;
																																																					{	/* Rgc/rgcexpand.scm 326 */
																																																						obj_t
																																																							BgL_arg1977z00_1836;
																																																						{	/* Rgc/rgcexpand.scm 326 */
																																																							obj_t
																																																								BgL_arg1978z00_1837;
																																																							{	/* Rgc/rgcexpand.scm 326 */
																																																								obj_t
																																																									BgL_arg1979z00_1838;
																																																								BgL_arg1979z00_1838
																																																									=
																																																									MAKE_YOUNG_PAIR
																																																									(BGl_symbol2340z00zz__rgc_expandz00,
																																																									BNIL);
																																																								BgL_arg1978z00_1837
																																																									=
																																																									MAKE_YOUNG_PAIR
																																																									(BGl_string2560z00zz__rgc_expandz00,
																																																									BgL_arg1979z00_1838);
																																																							}
																																																							BgL_arg1977z00_1836
																																																								=
																																																								MAKE_YOUNG_PAIR
																																																								(BGl_string2302z00zz__rgc_expandz00,
																																																								BgL_arg1978z00_1837);
																																																						}
																																																						BgL_arg1976z00_1835
																																																							=
																																																							MAKE_YOUNG_PAIR
																																																							(BGl_symbol2430z00zz__rgc_expandz00,
																																																							BgL_arg1977z00_1836);
																																																					}
																																																					BgL_arg1975z00_1834
																																																						=
																																																						MAKE_YOUNG_PAIR
																																																						(BgL_arg1976z00_1835,
																																																						BNIL);
																																																				}
																																																				BgL_arg1974z00_1833
																																																					=
																																																					MAKE_YOUNG_PAIR
																																																					(BGl_symbol2561z00zz__rgc_expandz00,
																																																					BgL_arg1975z00_1834);
																																																			}
																																																			BgL_arg1964z00_1817
																																																				=
																																																				MAKE_YOUNG_PAIR
																																																				(BgL_arg1974z00_1833,
																																																				BNIL);
																																																		}
																																																		BgL_arg1962z00_1815
																																																			=
																																																			BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																																			(BgL_arg1963z00_1816,
																																																			BgL_arg1964z00_1817);
																																																	}
																																																	BgL_arg1961z00_1814
																																																		=
																																																		MAKE_YOUNG_PAIR
																																																		(BGl_symbol2340z00zz__rgc_expandz00,
																																																		BgL_arg1962z00_1815);
																																																}
																																																BgL_arg1960z00_1813
																																																	=
																																																	MAKE_YOUNG_PAIR
																																																	(BGl_symbol2563z00zz__rgc_expandz00,
																																																	BgL_arg1961z00_1814);
																																															}
																																															BgL_arg1959z00_1812
																																																=
																																																MAKE_YOUNG_PAIR
																																																(BgL_arg1960z00_1813,
																																																BNIL);
																																														}
																																														BgL_arg1956z00_1809
																																															=
																																															BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																															(BgL_arg1958z00_1811,
																																															BgL_arg1959z00_1812);
																																													}
																																													BgL_arg1941z00_1794
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BgL_arg1955z00_1808,
																																														BgL_arg1956z00_1809);
																																												}
																																												BgL_arg1939z00_1792
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_arg1940z00_1793,
																																													BgL_arg1941z00_1794);
																																											}
																																											BgL_arg1938z00_1791
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BGl_symbol2309z00zz__rgc_expandz00,
																																												BgL_arg1939z00_1792);
																																										}
																																										BgL_arg1936z00_1789
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_arg1938z00_1791,
																																											BNIL);
																																									}
																																									BgL_arg1933z00_1786
																																										=
																																										BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																										(BgL_arg1935z00_1788,
																																										BgL_arg1936z00_1789);
																																								}
																																								BgL_arg1931z00_1784
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_arg1932z00_1785,
																																									BgL_arg1933z00_1786);
																																							}
																																							BgL_arg1929z00_1782
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_arg1930z00_1783,
																																								BgL_arg1931z00_1784);
																																						}
																																						BgL_arg1927z00_1780
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BGl_symbol2318z00zz__rgc_expandz00,
																																							BgL_arg1929z00_1782);
																																					}
																																					{	/* Rgc/rgcexpand.scm 330 */
																																						obj_t
																																							BgL_arg1980z00_1839;
																																						if (CBOOL(BGl_za2unsafezd2rgcza2zd2zz__rgcz00))
																																							{	/* Rgc/rgcexpand.scm 330 */
																																								BgL_arg1980z00_1839
																																									=
																																									BGl_list2565z00zz__rgc_expandz00;
																																							}
																																						else
																																							{	/* Rgc/rgcexpand.scm 330 */
																																								BgL_arg1980z00_1839
																																									=
																																									BGl_list2566z00zz__rgc_expandz00;
																																							}
																																						BgL_arg1928z00_1781
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg1980z00_1839,
																																							BNIL);
																																					}
																																					BgL_arg1926z00_1779
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg1927z00_1780,
																																						BgL_arg1928z00_1781);
																																				}
																																				BgL_arg1901z00_1761
																																					=
																																					BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																					(BgL_defsz00_12,
																																					BgL_arg1926z00_1779);
																																			}
																																			BgL_arg1890z00_1752
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1899z00_1760,
																																				BgL_arg1901z00_1761);
																																		}
																																		BgL_arg1879z00_1743
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1889z00_1751,
																																			BgL_arg1890z00_1752);
																																	}
																																	BgL_arg1874z00_1738
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1878z00_1742,
																																		BgL_arg1879z00_1743);
																																}
																																BgL_arg1852z00_1721
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1873z00_1737,
																																	BgL_arg1874z00_1738);
																															}
																															BgL_arg1845z00_1714
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1851z00_1720,
																																BgL_arg1852z00_1721);
																														}
																														BgL_arg1837z00_1707
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1844z00_1713,
																															BgL_arg1845z00_1714);
																													}
																													BgL_arg1829z00_1700 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1836z00_1706,
																														BgL_arg1837z00_1707);
																												}
																												BgL_arg1751z00_1629 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1828z00_1699,
																													BgL_arg1829z00_1700);
																											}
																											BgL_arg1744z00_1622 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1750z00_1628,
																												BgL_arg1751z00_1629);
																										}
																										BgL_arg1641z00_1551 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1743z00_1621,
																											BgL_arg1744z00_1622);
																									}
																									BgL_arg1631z00_1544 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1640z00_1550,
																										BgL_arg1641z00_1551);
																								}
																								BgL_arg1531z00_1473 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1630z00_1543,
																									BgL_arg1631z00_1544);
																							}
																							BgL_arg1524z00_1466 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1530z00_1472,
																								BgL_arg1531z00_1473);
																						}
																						BgL_arg1513z00_1459 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1523z00_1465,
																							BgL_arg1524z00_1466);
																					}
																					BgL_arg1505z00_1452 =
																						MAKE_YOUNG_PAIR(BgL_arg1511z00_1458,
																						BgL_arg1513z00_1459);
																				}
																				BgL_arg1498z00_1445 =
																					MAKE_YOUNG_PAIR(BgL_arg1504z00_1451,
																					BgL_arg1505z00_1452);
																			}
																			BgL_arg1488z00_1438 =
																				MAKE_YOUNG_PAIR(BgL_arg1497z00_1444,
																				BgL_arg1498z00_1445);
																		}
																		BgL_arg1424z00_1379 =
																			MAKE_YOUNG_PAIR(BgL_arg1487z00_1437,
																			BgL_arg1488z00_1438);
																	}
																	BgL_arg1361z00_1322 =
																		MAKE_YOUNG_PAIR(BgL_arg1423z00_1378,
																		BgL_arg1424z00_1379);
																}
																BgL_arg1349z00_1312 =
																	MAKE_YOUNG_PAIR(BgL_arg1360z00_1321,
																	BgL_arg1361z00_1322);
															}
															BgL_arg1339z00_1303 =
																MAKE_YOUNG_PAIR(BgL_arg1348z00_1311,
																BgL_arg1349z00_1312);
														}
														BgL_arg1332z00_1296 =
															MAKE_YOUNG_PAIR(BgL_arg1338z00_1302,
															BgL_arg1339z00_1303);
													}
													BgL_arg1323z00_1289 =
														MAKE_YOUNG_PAIR(BgL_arg1331z00_1295,
														BgL_arg1332z00_1296);
												}
												BgL_arg1318z00_1284 =
													MAKE_YOUNG_PAIR(BgL_arg1322z00_1288,
													BgL_arg1323z00_1289);
											}
											BgL_arg1316z00_1282 =
												MAKE_YOUNG_PAIR(BgL_arg1317z00_1283,
												BgL_arg1318z00_1284);
										}
										BgL_arg1310z00_1277 =
											BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
											(BgL_statesz00_8, BgL_arg1316z00_1282);
									}
									BgL_arg1307z00_1274 =
										BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
										(BgL_arg1309z00_1276, BgL_arg1310z00_1277);
								}
								BgL_arg1305z00_1272 =
									MAKE_YOUNG_PAIR(BgL_arg1306z00_1273, BgL_arg1307z00_1274);
							}
							BgL_arg1304z00_1271 =
								MAKE_YOUNG_PAIR(BGl_symbol2589z00zz__rgc_expandz00,
								BgL_arg1305z00_1272);
						}
						BgL_arg1268z00_1268 = MAKE_YOUNG_PAIR(BgL_arg1304z00_1271, BNIL);
					}
					BgL_arg1249z00_1266 =
						MAKE_YOUNG_PAIR(BgL_arg1252z00_1267, BgL_arg1268z00_1268);
				}
				return
					MAKE_YOUNG_PAIR(BGl_symbol2309z00zz__rgc_expandz00,
					BgL_arg1249z00_1266);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__rgc_expandz00(void)
	{
		{	/* Rgc/rgcexpand.scm 22 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__rgc_expandz00(void)
	{
		{	/* Rgc/rgcexpand.scm 22 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__rgc_expandz00(void)
	{
		{	/* Rgc/rgcexpand.scm 22 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__rgc_expandz00(void)
	{
		{	/* Rgc/rgcexpand.scm 22 */
			BGl_modulezd2initializa7ationz75zz__rgc_rulesz00(181068393L,
				BSTRING_TO_STRING(BGl_string2591z00zz__rgc_expandz00));
			BGl_modulezd2initializa7ationz75zz__rgc_treez00(424729538L,
				BSTRING_TO_STRING(BGl_string2591z00zz__rgc_expandz00));
			BGl_modulezd2initializa7ationz75zz__rgc_dfaz00(477555221L,
				BSTRING_TO_STRING(BGl_string2591z00zz__rgc_expandz00));
			BGl_modulezd2initializa7ationz75zz__rgc_compilez00(433137012L,
				BSTRING_TO_STRING(BGl_string2591z00zz__rgc_expandz00));
			BGl_modulezd2initializa7ationz75zz__rgc_configz00(428274736L,
				BSTRING_TO_STRING(BGl_string2591z00zz__rgc_expandz00));
			BGl_modulezd2initializa7ationz75zz__rgcz00(352600006L,
				BSTRING_TO_STRING(BGl_string2591z00zz__rgc_expandz00));
			BGl_modulezd2initializa7ationz75zz__rgc_setz00(225075643L,
				BSTRING_TO_STRING(BGl_string2591z00zz__rgc_expandz00));
			return
				BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string2591z00zz__rgc_expandz00));
		}

	}

#ifdef __cplusplus
}
#endif
