/*===========================================================================*/
/*   (Rgc/rgccompile.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Rgc/rgccompile.scm -indent -o objs/obj_u/Rgc/rgccompile.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___RGC_COMPILE_TYPE_DEFINITIONS
#define BGL___RGC_COMPILE_TYPE_DEFINITIONS
#endif													// BGL___RGC_COMPILE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_statezd2positionszd2zz__rgc_dfaz00(obj_t);
	extern obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern int
		BGl_specialzd2matchzd2charzd2ze3rulezd2numberze3zz__rgc_rulesz00(int);
	static obj_t BGl_z62compilezd2dfazb0zz__rgc_compilez00(obj_t, obj_t, obj_t,
		obj_t);
	extern bool_t BGl_specialzd2charzf3z21zz__rgc_rulesz00(int);
	static obj_t BGl_requirezd2initializa7ationz75zz__rgc_compilez00 = BUNSPEC;
	static obj_t BGl_compilezd2submatcheszd2zz__rgc_compilez00(obj_t, obj_t,
		obj_t, obj_t);
	extern bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__rgc_compilez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__rgc_configz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__rgc_setz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__rgc_dfaz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__rgc_rulesz00(long, char *);
	static obj_t BGl_compilezd2condzd2regularz00zz__rgc_compilez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol2201z00zz__rgc_compilez00 = BUNSPEC;
	static obj_t BGl_symbol2203z00zz__rgc_compilez00 = BUNSPEC;
	static obj_t BGl_symbol2205z00zz__rgc_compilez00 = BUNSPEC;
	extern obj_t BGl_rgcsetzd2addz12zc0zz__rgc_setz00(obj_t, long);
	static obj_t BGl_symbol2207z00zz__rgc_compilez00 = BUNSPEC;
	static obj_t BGl_symbol2209z00zz__rgc_compilez00 = BUNSPEC;
	extern obj_t BGl_vectorzd2fillz12zc0zz__r4_vectors_6_8z00(obj_t, obj_t, long,
		long);
	static obj_t BGl_toplevelzd2initzd2zz__rgc_compilez00(void);
	static obj_t BGl_symbol2211z00zz__rgc_compilez00 = BUNSPEC;
	extern obj_t BGl_za2rgczd2optimza2zd2zz__rgc_configz00;
	extern obj_t BGl_rgcsetzd2ze3listz31zz__rgc_setz00(obj_t);
	extern obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__rgc_compilez00(void);
	static obj_t BGl_genericzd2initzd2zz__rgc_compilez00(void);
	static obj_t BGl_symbol2144z00zz__rgc_compilez00 = BUNSPEC;
	extern long bgl_list_length(obj_t);
	static obj_t BGl_symbol2146z00zz__rgc_compilez00 = BUNSPEC;
	static obj_t BGl_symbol2148z00zz__rgc_compilez00 = BUNSPEC;
	extern bool_t BGl_specialzd2charzd2matchzf3zf3zz__rgc_rulesz00(int);
	extern obj_t BGl_rgcsetzd2notzd2zz__rgc_setz00(obj_t);
	static obj_t BGl_compilezd2condzd2testz00zz__rgc_compilez00(obj_t, obj_t,
		long);
	static obj_t BGl_importedzd2moduleszd2initz00zz__rgc_compilez00(void);
	static obj_t BGl_compilezd2memberzd2vectorz00zz__rgc_compilez00 = BUNSPEC;
	static obj_t BGl_gczd2rootszd2initz00zz__rgc_compilez00(void);
	static obj_t BGl_compilezd2statezd2zz__rgc_compilez00(obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zz__rgc_compilez00(void);
	static obj_t BGl_compilezd2memberzd2testz00zz__rgc_compilez00(obj_t, obj_t);
	extern obj_t BGl_2zb2zb2zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_symbol2150z00zz__rgc_compilez00 = BUNSPEC;
	static obj_t BGl_symbol2152z00zz__rgc_compilez00 = BUNSPEC;
	static obj_t BGl_symbol2154z00zz__rgc_compilez00 = BUNSPEC;
	static obj_t BGl_symbol2156z00zz__rgc_compilez00 = BUNSPEC;
	static obj_t BGl_symbol2158z00zz__rgc_compilez00 = BUNSPEC;
	extern obj_t bgl_reverse_bang(obj_t);
	static obj_t BGl_compilezd2rangezd2testze70ze7zz__rgc_compilez00(obj_t,
		obj_t);
	static obj_t BGl_symbol2160z00zz__rgc_compilez00 = BUNSPEC;
	static obj_t BGl_symbol2163z00zz__rgc_compilez00 = BUNSPEC;
	static obj_t BGl_symbol2166z00zz__rgc_compilez00 = BUNSPEC;
	static obj_t BGl_symbol2168z00zz__rgc_compilez00 = BUNSPEC;
	static obj_t BGl_charszd2ze3charzd2rangesze3zz__rgc_compilez00(obj_t);
	extern obj_t BGl_statezd2transitionszd2zz__rgc_dfaz00(obj_t);
	static obj_t BGl_insortz00zz__rgc_compilez00(int, obj_t);
	static obj_t BGl_symbol2170z00zz__rgc_compilez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_compilezd2dfazd2zz__rgc_compilez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol2172z00zz__rgc_compilez00 = BUNSPEC;
	static obj_t BGl_statezd2transitionzd2listz00zz__rgc_compilez00(obj_t);
	static obj_t BGl_symbol2174z00zz__rgc_compilez00 = BUNSPEC;
	static obj_t BGl_symbol2176z00zz__rgc_compilez00 = BUNSPEC;
	static obj_t BGl_symbol2178z00zz__rgc_compilez00 = BUNSPEC;
	static long BGl_za2casezd2thresholdza2zd2zz__rgc_compilez00 = 0L;
	extern obj_t BGl_statezd2namezd2zz__rgc_dfaz00(obj_t);
	static obj_t BGl_charzd2rangeszd2ze3testze3zz__rgc_compilez00(obj_t, obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_symbol2180z00zz__rgc_compilez00 = BUNSPEC;
	extern obj_t make_vector(long, obj_t);
	static obj_t BGl_compilezd2matchzd2zz__rgc_compilez00(obj_t);
	static obj_t BGl_symbol2183z00zz__rgc_compilez00 = BUNSPEC;
	static obj_t BGl_symbol2185z00zz__rgc_compilez00 = BUNSPEC;
	static obj_t BGl_symbol2187z00zz__rgc_compilez00 = BUNSPEC;
	static obj_t BGl_symbol2189z00zz__rgc_compilez00 = BUNSPEC;
	static obj_t BGl_charzd2rangezd2ze3testze70z04zz__rgc_compilez00(obj_t,
		obj_t);
	static obj_t BGl_compilezd2casezd2transitionze70ze7zz__rgc_compilez00(obj_t,
		obj_t);
	static obj_t BGl_methodzd2initzd2zz__rgc_compilez00(void);
	static obj_t BGl_symbol2191z00zz__rgc_compilez00 = BUNSPEC;
	static obj_t BGl_symbol2193z00zz__rgc_compilez00 = BUNSPEC;
	static obj_t BGl_symbol2195z00zz__rgc_compilez00 = BUNSPEC;
	static obj_t BGl_list2162z00zz__rgc_compilez00 = BUNSPEC;
	static obj_t BGl_symbol2197z00zz__rgc_compilez00 = BUNSPEC;
	static obj_t BGl_list2165z00zz__rgc_compilez00 = BUNSPEC;
	static obj_t BGl_symbol2199z00zz__rgc_compilez00 = BUNSPEC;
	extern bool_t BGl_rgcsetzd2memberzf3z21zz__rgc_setz00(obj_t, long);
	extern obj_t BGl_listzd2ze3rgcsetz31zz__rgc_setz00(obj_t, long);
	static obj_t BGl_splitzd2transitionszd2zz__rgc_compilez00(obj_t);
	static obj_t BGl_loopze70ze7zz__rgc_compilez00(obj_t);
	static obj_t
		BGl_initzd2compilezd2memberzd2vectorz12zc0zz__rgc_compilez00(void);
	static obj_t BGl_compilezd2casezd2regularz00zz__rgc_compilez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_list2182z00zz__rgc_compilez00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31649ze3ze5zz__rgc_compilez00(obj_t,
		obj_t);
	extern obj_t BGl_rgczd2maxzd2charz00zz__rgc_configz00(void);
	static obj_t BGl_compilezd2regularzd2zz__rgc_compilez00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_predicatezd2matchzd2zz__rgc_rulesz00(int);
	extern long BGl_rgcsetzd2lengthzd2zz__rgc_setz00(obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string2200z00zz__rgc_compilez00,
		BgL_bgl_string2200za700za7za7_2216za7, "begin", 5);
	      DEFINE_STRING(BGl_string2202z00zz__rgc_compilez00,
		BgL_bgl_string2202za700za7za7_2217za7, "<=fx", 4);
	      DEFINE_STRING(BGl_string2204z00zz__rgc_compilez00,
		BgL_bgl_string2204za700za7za7_2218za7, "quote", 5);
	      DEFINE_STRING(BGl_string2206z00zz__rgc_compilez00,
		BgL_bgl_string2206za700za7za7_2219za7, "memq", 4);
	      DEFINE_STRING(BGl_string2208z00zz__rgc_compilez00,
		BgL_bgl_string2208za700za7za7_2220za7, "rgc-submatch-start*2!", 21);
	      DEFINE_STRING(BGl_string2210z00zz__rgc_compilez00,
		BgL_bgl_string2210za700za7za7_2221za7, "rgc-submatch-start2!", 20);
	      DEFINE_STRING(BGl_string2212z00zz__rgc_compilez00,
		BgL_bgl_string2212za700za7za7_2222za7, "rgc-submatch-stop2!", 19);
	      DEFINE_STRING(BGl_string2213z00zz__rgc_compilez00,
		BgL_bgl_string2213za700za7za7_2223za7, "compile-submatches", 18);
	      DEFINE_STRING(BGl_string2214z00zz__rgc_compilez00,
		BgL_bgl_string2214za700za7za7_2224za7, "Illegal char description", 24);
	      DEFINE_STRING(BGl_string2215z00zz__rgc_compilez00,
		BgL_bgl_string2215za700za7za7_2225za7, "__rgc_compile", 13);
	      DEFINE_STRING(BGl_string2145z00zz__rgc_compilez00,
		BgL_bgl_string2145za700za7za7_2226za7, "bufpos", 6);
	      DEFINE_STRING(BGl_string2147z00zz__rgc_compilez00,
		BgL_bgl_string2147za700za7za7_2227za7, "forward", 7);
	      DEFINE_STRING(BGl_string2149z00zz__rgc_compilez00,
		BgL_bgl_string2149za700za7za7_2228za7, "last-match", 10);
	      DEFINE_STRING(BGl_string2151z00zz__rgc_compilez00,
		BgL_bgl_string2151za700za7za7_2229za7, "iport", 5);
	      DEFINE_STRING(BGl_string2153z00zz__rgc_compilez00,
		BgL_bgl_string2153za700za7za7_2230za7, "new-match", 9);
	      DEFINE_STRING(BGl_string2155z00zz__rgc_compilez00,
		BgL_bgl_string2155za700za7za7_2231za7, "let", 3);
	      DEFINE_STRING(BGl_string2157z00zz__rgc_compilez00,
		BgL_bgl_string2157za700za7za7_2232za7, "define", 6);
	      DEFINE_STRING(BGl_string2159z00zz__rgc_compilez00,
		BgL_bgl_string2159za700za7za7_2233za7, "=fx", 3);
	      DEFINE_STRING(BGl_string2161z00zz__rgc_compilez00,
		BgL_bgl_string2161za700za7za7_2234za7, "rgc-fill-buffer", 15);
	      DEFINE_STRING(BGl_string2164z00zz__rgc_compilez00,
		BgL_bgl_string2164za700za7za7_2235za7, "rgc-buffer-forward", 18);
	      DEFINE_STRING(BGl_string2167z00zz__rgc_compilez00,
		BgL_bgl_string2167za700za7za7_2236za7, "rgc-buffer-bufpos", 17);
	      DEFINE_STRING(BGl_string2169z00zz__rgc_compilez00,
		BgL_bgl_string2169za700za7za7_2237za7, "if", 2);
	      DEFINE_STRING(BGl_string2171z00zz__rgc_compilez00,
		BgL_bgl_string2171za700za7za7_2238za7, "rgc-buffer-get-char", 19);
	      DEFINE_STRING(BGl_string2173z00zz__rgc_compilez00,
		BgL_bgl_string2173za700za7za7_2239za7, "cur::int", 8);
	      DEFINE_STRING(BGl_string2175z00zz__rgc_compilez00,
		BgL_bgl_string2175za700za7za7_2240za7, "cur", 3);
	      DEFINE_STRING(BGl_string2177z00zz__rgc_compilez00,
		BgL_bgl_string2177za700za7za7_2241za7, "let*", 4);
	      DEFINE_STRING(BGl_string2179z00zz__rgc_compilez00,
		BgL_bgl_string2179za700za7za7_2242za7, "else", 4);
	      DEFINE_STRING(BGl_string2181z00zz__rgc_compilez00,
		BgL_bgl_string2181za700za7za7_2243za7, "case", 4);
	      DEFINE_STRING(BGl_string2184z00zz__rgc_compilez00,
		BgL_bgl_string2184za700za7za7_2244za7, "+fx", 3);
	      DEFINE_STRING(BGl_string2186z00zz__rgc_compilez00,
		BgL_bgl_string2186za700za7za7_2245za7, "cond", 4);
	      DEFINE_STRING(BGl_string2188z00zz__rgc_compilez00,
		BgL_bgl_string2188za700za7za7_2246za7, "not", 3);
	      DEFINE_STRING(BGl_string2190z00zz__rgc_compilez00,
		BgL_bgl_string2190za700za7za7_2247za7, "or", 2);
	      DEFINE_STRING(BGl_string2192z00zz__rgc_compilez00,
		BgL_bgl_string2192za700za7za7_2248za7, ">=fx", 4);
	      DEFINE_STRING(BGl_string2194z00zz__rgc_compilez00,
		BgL_bgl_string2194za700za7za7_2249za7, "<fx", 3);
	      DEFINE_STRING(BGl_string2196z00zz__rgc_compilez00,
		BgL_bgl_string2196za700za7za7_2250za7, "and", 3);
	      DEFINE_STRING(BGl_string2198z00zz__rgc_compilez00,
		BgL_bgl_string2198za700za7za7_2251za7, "rgc-stop-match!", 15);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_compilezd2dfazd2envz00zz__rgc_compilez00,
		BgL_bgl_za762compileza7d2dfa2252z00,
		BGl_z62compilezd2dfazb0zz__rgc_compilez00, 0L, BUNSPEC, 3);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__rgc_compilez00));
		     ADD_ROOT((void *) (&BGl_symbol2201z00zz__rgc_compilez00));
		     ADD_ROOT((void *) (&BGl_symbol2203z00zz__rgc_compilez00));
		     ADD_ROOT((void *) (&BGl_symbol2205z00zz__rgc_compilez00));
		     ADD_ROOT((void *) (&BGl_symbol2207z00zz__rgc_compilez00));
		     ADD_ROOT((void *) (&BGl_symbol2209z00zz__rgc_compilez00));
		     ADD_ROOT((void *) (&BGl_symbol2211z00zz__rgc_compilez00));
		     ADD_ROOT((void *) (&BGl_symbol2144z00zz__rgc_compilez00));
		     ADD_ROOT((void *) (&BGl_symbol2146z00zz__rgc_compilez00));
		     ADD_ROOT((void *) (&BGl_symbol2148z00zz__rgc_compilez00));
		   
			 ADD_ROOT((void *) (&BGl_compilezd2memberzd2vectorz00zz__rgc_compilez00));
		     ADD_ROOT((void *) (&BGl_symbol2150z00zz__rgc_compilez00));
		     ADD_ROOT((void *) (&BGl_symbol2152z00zz__rgc_compilez00));
		     ADD_ROOT((void *) (&BGl_symbol2154z00zz__rgc_compilez00));
		     ADD_ROOT((void *) (&BGl_symbol2156z00zz__rgc_compilez00));
		     ADD_ROOT((void *) (&BGl_symbol2158z00zz__rgc_compilez00));
		     ADD_ROOT((void *) (&BGl_symbol2160z00zz__rgc_compilez00));
		     ADD_ROOT((void *) (&BGl_symbol2163z00zz__rgc_compilez00));
		     ADD_ROOT((void *) (&BGl_symbol2166z00zz__rgc_compilez00));
		     ADD_ROOT((void *) (&BGl_symbol2168z00zz__rgc_compilez00));
		     ADD_ROOT((void *) (&BGl_symbol2170z00zz__rgc_compilez00));
		     ADD_ROOT((void *) (&BGl_symbol2172z00zz__rgc_compilez00));
		     ADD_ROOT((void *) (&BGl_symbol2174z00zz__rgc_compilez00));
		     ADD_ROOT((void *) (&BGl_symbol2176z00zz__rgc_compilez00));
		     ADD_ROOT((void *) (&BGl_symbol2178z00zz__rgc_compilez00));
		     ADD_ROOT((void *) (&BGl_symbol2180z00zz__rgc_compilez00));
		     ADD_ROOT((void *) (&BGl_symbol2183z00zz__rgc_compilez00));
		     ADD_ROOT((void *) (&BGl_symbol2185z00zz__rgc_compilez00));
		     ADD_ROOT((void *) (&BGl_symbol2187z00zz__rgc_compilez00));
		     ADD_ROOT((void *) (&BGl_symbol2189z00zz__rgc_compilez00));
		     ADD_ROOT((void *) (&BGl_symbol2191z00zz__rgc_compilez00));
		     ADD_ROOT((void *) (&BGl_symbol2193z00zz__rgc_compilez00));
		     ADD_ROOT((void *) (&BGl_symbol2195z00zz__rgc_compilez00));
		     ADD_ROOT((void *) (&BGl_list2162z00zz__rgc_compilez00));
		     ADD_ROOT((void *) (&BGl_symbol2197z00zz__rgc_compilez00));
		     ADD_ROOT((void *) (&BGl_list2165z00zz__rgc_compilez00));
		     ADD_ROOT((void *) (&BGl_symbol2199z00zz__rgc_compilez00));
		     ADD_ROOT((void *) (&BGl_list2182z00zz__rgc_compilez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__rgc_compilez00(long
		BgL_checksumz00_3154, char *BgL_fromz00_3155)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__rgc_compilez00))
				{
					BGl_requirezd2initializa7ationz75zz__rgc_compilez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__rgc_compilez00();
					BGl_cnstzd2initzd2zz__rgc_compilez00();
					BGl_importedzd2moduleszd2initz00zz__rgc_compilez00();
					return BGl_toplevelzd2initzd2zz__rgc_compilez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__rgc_compilez00(void)
	{
		{	/* Rgc/rgccompile.scm 15 */
			BGl_symbol2144z00zz__rgc_compilez00 =
				bstring_to_symbol(BGl_string2145z00zz__rgc_compilez00);
			BGl_symbol2146z00zz__rgc_compilez00 =
				bstring_to_symbol(BGl_string2147z00zz__rgc_compilez00);
			BGl_symbol2148z00zz__rgc_compilez00 =
				bstring_to_symbol(BGl_string2149z00zz__rgc_compilez00);
			BGl_symbol2150z00zz__rgc_compilez00 =
				bstring_to_symbol(BGl_string2151z00zz__rgc_compilez00);
			BGl_symbol2152z00zz__rgc_compilez00 =
				bstring_to_symbol(BGl_string2153z00zz__rgc_compilez00);
			BGl_symbol2154z00zz__rgc_compilez00 =
				bstring_to_symbol(BGl_string2155z00zz__rgc_compilez00);
			BGl_symbol2156z00zz__rgc_compilez00 =
				bstring_to_symbol(BGl_string2157z00zz__rgc_compilez00);
			BGl_symbol2158z00zz__rgc_compilez00 =
				bstring_to_symbol(BGl_string2159z00zz__rgc_compilez00);
			BGl_symbol2160z00zz__rgc_compilez00 =
				bstring_to_symbol(BGl_string2161z00zz__rgc_compilez00);
			BGl_symbol2163z00zz__rgc_compilez00 =
				bstring_to_symbol(BGl_string2164z00zz__rgc_compilez00);
			BGl_list2162z00zz__rgc_compilez00 =
				MAKE_YOUNG_PAIR(BGl_symbol2163z00zz__rgc_compilez00,
				MAKE_YOUNG_PAIR(BGl_symbol2150z00zz__rgc_compilez00, BNIL));
			BGl_symbol2166z00zz__rgc_compilez00 =
				bstring_to_symbol(BGl_string2167z00zz__rgc_compilez00);
			BGl_list2165z00zz__rgc_compilez00 =
				MAKE_YOUNG_PAIR(BGl_symbol2166z00zz__rgc_compilez00,
				MAKE_YOUNG_PAIR(BGl_symbol2150z00zz__rgc_compilez00, BNIL));
			BGl_symbol2168z00zz__rgc_compilez00 =
				bstring_to_symbol(BGl_string2169z00zz__rgc_compilez00);
			BGl_symbol2170z00zz__rgc_compilez00 =
				bstring_to_symbol(BGl_string2171z00zz__rgc_compilez00);
			BGl_symbol2172z00zz__rgc_compilez00 =
				bstring_to_symbol(BGl_string2173z00zz__rgc_compilez00);
			BGl_symbol2174z00zz__rgc_compilez00 =
				bstring_to_symbol(BGl_string2175z00zz__rgc_compilez00);
			BGl_symbol2176z00zz__rgc_compilez00 =
				bstring_to_symbol(BGl_string2177z00zz__rgc_compilez00);
			BGl_symbol2178z00zz__rgc_compilez00 =
				bstring_to_symbol(BGl_string2179z00zz__rgc_compilez00);
			BGl_symbol2180z00zz__rgc_compilez00 =
				bstring_to_symbol(BGl_string2181z00zz__rgc_compilez00);
			BGl_symbol2183z00zz__rgc_compilez00 =
				bstring_to_symbol(BGl_string2184z00zz__rgc_compilez00);
			BGl_list2182z00zz__rgc_compilez00 =
				MAKE_YOUNG_PAIR(BGl_symbol2183z00zz__rgc_compilez00,
				MAKE_YOUNG_PAIR(BINT(1L),
					MAKE_YOUNG_PAIR(BGl_symbol2146z00zz__rgc_compilez00, BNIL)));
			BGl_symbol2185z00zz__rgc_compilez00 =
				bstring_to_symbol(BGl_string2186z00zz__rgc_compilez00);
			BGl_symbol2187z00zz__rgc_compilez00 =
				bstring_to_symbol(BGl_string2188z00zz__rgc_compilez00);
			BGl_symbol2189z00zz__rgc_compilez00 =
				bstring_to_symbol(BGl_string2190z00zz__rgc_compilez00);
			BGl_symbol2191z00zz__rgc_compilez00 =
				bstring_to_symbol(BGl_string2192z00zz__rgc_compilez00);
			BGl_symbol2193z00zz__rgc_compilez00 =
				bstring_to_symbol(BGl_string2194z00zz__rgc_compilez00);
			BGl_symbol2195z00zz__rgc_compilez00 =
				bstring_to_symbol(BGl_string2196z00zz__rgc_compilez00);
			BGl_symbol2197z00zz__rgc_compilez00 =
				bstring_to_symbol(BGl_string2198z00zz__rgc_compilez00);
			BGl_symbol2199z00zz__rgc_compilez00 =
				bstring_to_symbol(BGl_string2200z00zz__rgc_compilez00);
			BGl_symbol2201z00zz__rgc_compilez00 =
				bstring_to_symbol(BGl_string2202z00zz__rgc_compilez00);
			BGl_symbol2203z00zz__rgc_compilez00 =
				bstring_to_symbol(BGl_string2204z00zz__rgc_compilez00);
			BGl_symbol2205z00zz__rgc_compilez00 =
				bstring_to_symbol(BGl_string2206z00zz__rgc_compilez00);
			BGl_symbol2207z00zz__rgc_compilez00 =
				bstring_to_symbol(BGl_string2208z00zz__rgc_compilez00);
			BGl_symbol2209z00zz__rgc_compilez00 =
				bstring_to_symbol(BGl_string2210z00zz__rgc_compilez00);
			return (BGl_symbol2211z00zz__rgc_compilez00 =
				bstring_to_symbol(BGl_string2212z00zz__rgc_compilez00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__rgc_compilez00(void)
	{
		{	/* Rgc/rgccompile.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__rgc_compilez00(void)
	{
		{	/* Rgc/rgccompile.scm 15 */
			if (CBOOL(BGl_za2rgczd2optimza2zd2zz__rgc_configz00))
				{	/* Rgc/rgccompile.scm 77 */
					BGl_za2casezd2thresholdza2zd2zz__rgc_compilez00 = (80L / 2L);
				}
			else
				{	/* Rgc/rgccompile.scm 77 */
					BGl_za2casezd2thresholdza2zd2zz__rgc_compilez00 = 80L;
				}
			return (BGl_compilezd2memberzd2vectorz00zz__rgc_compilez00 =
				BUNSPEC, BUNSPEC);
		}

	}



/* compile-dfa */
	BGL_EXPORTED_DEF obj_t BGl_compilezd2dfazd2zz__rgc_compilez00(obj_t
		BgL_submatchesz00_3, obj_t BgL_statesz00_4, obj_t BgL_positionsz00_5)
	{
		{	/* Rgc/rgccompile.scm 59 */
			BGl_initzd2compilezd2memberzd2vectorz12zc0zz__rgc_compilez00();
			{	/* Rgc/rgccompile.scm 61 */
				obj_t BgL_resz00_1224;

				if (NULLP(BgL_statesz00_4))
					{	/* Rgc/rgccompile.scm 61 */
						BgL_resz00_1224 = BNIL;
					}
				else
					{	/* Rgc/rgccompile.scm 61 */
						obj_t BgL_head1100z00_1227;

						BgL_head1100z00_1227 = MAKE_YOUNG_PAIR(BNIL, BNIL);
						{
							obj_t BgL_l1098z00_2381;
							obj_t BgL_tail1101z00_2382;

							BgL_l1098z00_2381 = BgL_statesz00_4;
							BgL_tail1101z00_2382 = BgL_head1100z00_1227;
						BgL_zc3z04anonymousza31250ze3z87_2380:
							if (NULLP(BgL_l1098z00_2381))
								{	/* Rgc/rgccompile.scm 61 */
									BgL_resz00_1224 = CDR(BgL_head1100z00_1227);
								}
							else
								{	/* Rgc/rgccompile.scm 61 */
									obj_t BgL_newtail1102z00_2389;

									{	/* Rgc/rgccompile.scm 61 */
										obj_t BgL_arg1268z00_2390;

										{	/* Rgc/rgccompile.scm 61 */
											obj_t BgL_statez00_2391;

											BgL_statez00_2391 = CAR(((obj_t) BgL_l1098z00_2381));
											BgL_arg1268z00_2390 =
												BGl_compilezd2statezd2zz__rgc_compilez00
												(BgL_submatchesz00_3, BgL_statez00_2391,
												BgL_positionsz00_5);
										}
										BgL_newtail1102z00_2389 =
											MAKE_YOUNG_PAIR(BgL_arg1268z00_2390, BNIL);
									}
									SET_CDR(BgL_tail1101z00_2382, BgL_newtail1102z00_2389);
									{	/* Rgc/rgccompile.scm 61 */
										obj_t BgL_arg1252z00_2392;

										BgL_arg1252z00_2392 = CDR(((obj_t) BgL_l1098z00_2381));
										{
											obj_t BgL_tail1101z00_3223;
											obj_t BgL_l1098z00_3222;

											BgL_l1098z00_3222 = BgL_arg1252z00_2392;
											BgL_tail1101z00_3223 = BgL_newtail1102z00_2389;
											BgL_tail1101z00_2382 = BgL_tail1101z00_3223;
											BgL_l1098z00_2381 = BgL_l1098z00_3222;
											goto BgL_zc3z04anonymousza31250ze3z87_2380;
										}
									}
								}
						}
					}
				BGl_compilezd2memberzd2vectorz00zz__rgc_compilez00 = BUNSPEC;
				return BgL_resz00_1224;
			}
		}

	}



/* &compile-dfa */
	obj_t BGl_z62compilezd2dfazb0zz__rgc_compilez00(obj_t BgL_envz00_3017,
		obj_t BgL_submatchesz00_3018, obj_t BgL_statesz00_3019,
		obj_t BgL_positionsz00_3020)
	{
		{	/* Rgc/rgccompile.scm 59 */
			return
				BGl_compilezd2dfazd2zz__rgc_compilez00(BgL_submatchesz00_3018,
				BgL_statesz00_3019, BgL_positionsz00_3020);
		}

	}



/* compile-state */
	obj_t BGl_compilezd2statezd2zz__rgc_compilez00(obj_t BgL_submatchesz00_6,
		obj_t BgL_statez00_7, obj_t BgL_positionszd2tozd2charz00_8)
	{
		{	/* Rgc/rgccompile.scm 93 */
			{	/* Rgc/rgccompile.scm 97 */
				obj_t BgL_arg1272z00_1238;

				{	/* Rgc/rgccompile.scm 97 */
					obj_t BgL_arg1284z00_1239;
					obj_t BgL_arg1304z00_1240;

					{	/* Rgc/rgccompile.scm 97 */
						obj_t BgL_arg1305z00_1241;
						obj_t BgL_arg1306z00_1242;

						BgL_arg1305z00_1241 =
							BGl_statezd2namezd2zz__rgc_dfaz00(BgL_statez00_7);
						{	/* Rgc/rgccompile.scm 97 */
							obj_t BgL_arg1307z00_1243;

							{	/* Rgc/rgccompile.scm 97 */
								obj_t BgL_arg1308z00_1244;

								{	/* Rgc/rgccompile.scm 97 */
									obj_t BgL_arg1309z00_1245;

									BgL_arg1309z00_1245 =
										MAKE_YOUNG_PAIR(BGl_symbol2144z00zz__rgc_compilez00, BNIL);
									BgL_arg1308z00_1244 =
										MAKE_YOUNG_PAIR(BGl_symbol2146z00zz__rgc_compilez00,
										BgL_arg1309z00_1245);
								}
								BgL_arg1307z00_1243 =
									MAKE_YOUNG_PAIR(BGl_symbol2148z00zz__rgc_compilez00,
									BgL_arg1308z00_1244);
							}
							BgL_arg1306z00_1242 =
								MAKE_YOUNG_PAIR(BGl_symbol2150z00zz__rgc_compilez00,
								BgL_arg1307z00_1243);
						}
						BgL_arg1284z00_1239 =
							MAKE_YOUNG_PAIR(BgL_arg1305z00_1241, BgL_arg1306z00_1242);
					}
					{	/* Rgc/rgccompile.scm 98 */
						obj_t BgL_arg1310z00_1246;

						{	/* Rgc/rgccompile.scm 98 */
							obj_t BgL_transitionsz00_1247;
							obj_t BgL_positionsz00_1248;

							BgL_transitionsz00_1247 =
								BGl_statezd2transitionszd2zz__rgc_dfaz00(BgL_statez00_7);
							BgL_positionsz00_1248 =
								BGl_statezd2positionszd2zz__rgc_dfaz00(BgL_statez00_7);
							if (NULLP(BgL_transitionsz00_1247))
								{	/* Rgc/rgccompile.scm 100 */
									BgL_arg1310z00_1246 = BGl_symbol2148z00zz__rgc_compilez00;
								}
							else
								{	/* Rgc/rgccompile.scm 102 */
									obj_t BgL_specialzd2transzd2_1250;

									BgL_specialzd2transzd2_1250 =
										BGl_splitzd2transitionszd2zz__rgc_compilez00
										(BgL_transitionsz00_1247);
									{	/* Rgc/rgccompile.scm 103 */
										obj_t BgL_regularzd2transzd2_1251;

										{	/* Rgc/rgccompile.scm 104 */
											obj_t BgL_tmpz00_2397;

											{	/* Rgc/rgccompile.scm 104 */
												int BgL_tmpz00_3236;

												BgL_tmpz00_3236 = (int) (1L);
												BgL_tmpz00_2397 = BGL_MVALUES_VAL(BgL_tmpz00_3236);
											}
											{	/* Rgc/rgccompile.scm 104 */
												int BgL_tmpz00_3239;

												BgL_tmpz00_3239 = (int) (1L);
												BGL_MVALUES_VAL_SET(BgL_tmpz00_3239, BUNSPEC);
											}
											BgL_regularzd2transzd2_1251 = BgL_tmpz00_2397;
										}
										{	/* Rgc/rgccompile.scm 104 */
											obj_t BgL_matchzd2bodyzd2_1252;

											BgL_matchzd2bodyzd2_1252 =
												BGl_compilezd2matchzd2zz__rgc_compilez00
												(BgL_specialzd2transzd2_1250);
											if (CBOOL(BgL_matchzd2bodyzd2_1252))
												{	/* Rgc/rgccompile.scm 106 */
													obj_t BgL_arg1312z00_1253;

													{	/* Rgc/rgccompile.scm 106 */
														obj_t BgL_arg1314z00_1254;
														obj_t BgL_arg1315z00_1255;

														{	/* Rgc/rgccompile.scm 106 */
															obj_t BgL_arg1316z00_1256;

															{	/* Rgc/rgccompile.scm 106 */
																obj_t BgL_arg1317z00_1257;

																BgL_arg1317z00_1257 =
																	MAKE_YOUNG_PAIR(BgL_matchzd2bodyzd2_1252,
																	BNIL);
																BgL_arg1316z00_1256 =
																	MAKE_YOUNG_PAIR
																	(BGl_symbol2152z00zz__rgc_compilez00,
																	BgL_arg1317z00_1257);
															}
															BgL_arg1314z00_1254 =
																MAKE_YOUNG_PAIR(BgL_arg1316z00_1256, BNIL);
														}
														BgL_arg1315z00_1255 =
															MAKE_YOUNG_PAIR
															(BGl_compilezd2regularzd2zz__rgc_compilez00
															(BgL_submatchesz00_6, BgL_statez00_7,
																BgL_regularzd2transzd2_1251,
																BGl_symbol2152z00zz__rgc_compilez00,
																BgL_positionszd2tozd2charz00_8), BNIL);
														BgL_arg1312z00_1253 =
															MAKE_YOUNG_PAIR(BgL_arg1314z00_1254,
															BgL_arg1315z00_1255);
													}
													BgL_arg1310z00_1246 =
														MAKE_YOUNG_PAIR(BGl_symbol2154z00zz__rgc_compilez00,
														BgL_arg1312z00_1253);
												}
											else
												{	/* Rgc/rgccompile.scm 105 */
													BgL_arg1310z00_1246 =
														BGl_compilezd2regularzd2zz__rgc_compilez00
														(BgL_submatchesz00_6, BgL_statez00_7,
														BgL_regularzd2transzd2_1251,
														BGl_symbol2148z00zz__rgc_compilez00,
														BgL_positionszd2tozd2charz00_8);
												}
										}
									}
								}
						}
						BgL_arg1304z00_1240 = MAKE_YOUNG_PAIR(BgL_arg1310z00_1246, BNIL);
					}
					BgL_arg1272z00_1238 =
						MAKE_YOUNG_PAIR(BgL_arg1284z00_1239, BgL_arg1304z00_1240);
				}
				return
					MAKE_YOUNG_PAIR(BGl_symbol2156z00zz__rgc_compilez00,
					BgL_arg1272z00_1238);
			}
		}

	}



/* split-transitions */
	obj_t BGl_splitzd2transitionszd2zz__rgc_compilez00(obj_t BgL_transitionsz00_9)
	{
		{	/* Rgc/rgccompile.scm 120 */
			{
				obj_t BgL_transitionsz00_1262;
				obj_t BgL_specialsz00_1263;
				obj_t BgL_regularsz00_1264;

				BgL_transitionsz00_1262 = BgL_transitionsz00_9;
				BgL_specialsz00_1263 = BNIL;
				BgL_regularsz00_1264 = BNIL;
			BgL_zc3z04anonymousza31319ze3z87_1265:
				if (NULLP(BgL_transitionsz00_1262))
					{	/* Rgc/rgccompile.scm 125 */
						{	/* Rgc/rgccompile.scm 126 */
							int BgL_tmpz00_3258;

							BgL_tmpz00_3258 = (int) (2L);
							BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3258);
						}
						{	/* Rgc/rgccompile.scm 126 */
							int BgL_tmpz00_3261;

							BgL_tmpz00_3261 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3261, BgL_regularsz00_1264);
						}
						return BgL_specialsz00_1263;
					}
				else
					{	/* Rgc/rgccompile.scm 127 */
						bool_t BgL_test2260z00_3264;

						{	/* Rgc/rgccompile.scm 127 */
							obj_t BgL_arg1332z00_1278;

							{	/* Rgc/rgccompile.scm 127 */
								obj_t BgL_pairz00_2399;

								BgL_pairz00_2399 = CAR(((obj_t) BgL_transitionsz00_1262));
								BgL_arg1332z00_1278 = CAR(BgL_pairz00_2399);
							}
							BgL_test2260z00_3264 =
								BGl_specialzd2charzf3z21zz__rgc_rulesz00(CINT
								(BgL_arg1332z00_1278));
						}
						if (BgL_test2260z00_3264)
							{	/* Rgc/rgccompile.scm 128 */
								obj_t BgL_arg1325z00_1272;
								obj_t BgL_arg1326z00_1273;

								BgL_arg1325z00_1272 = CDR(((obj_t) BgL_transitionsz00_1262));
								{	/* Rgc/rgccompile.scm 129 */
									obj_t BgL_arg1327z00_1274;

									BgL_arg1327z00_1274 = CAR(((obj_t) BgL_transitionsz00_1262));
									BgL_arg1326z00_1273 =
										MAKE_YOUNG_PAIR(BgL_arg1327z00_1274, BgL_specialsz00_1263);
								}
								{
									obj_t BgL_specialsz00_3276;
									obj_t BgL_transitionsz00_3275;

									BgL_transitionsz00_3275 = BgL_arg1325z00_1272;
									BgL_specialsz00_3276 = BgL_arg1326z00_1273;
									BgL_specialsz00_1263 = BgL_specialsz00_3276;
									BgL_transitionsz00_1262 = BgL_transitionsz00_3275;
									goto BgL_zc3z04anonymousza31319ze3z87_1265;
								}
							}
						else
							{	/* Rgc/rgccompile.scm 132 */
								obj_t BgL_arg1328z00_1275;
								obj_t BgL_arg1329z00_1276;

								BgL_arg1328z00_1275 = CDR(((obj_t) BgL_transitionsz00_1262));
								{	/* Rgc/rgccompile.scm 134 */
									obj_t BgL_arg1331z00_1277;

									BgL_arg1331z00_1277 = CAR(((obj_t) BgL_transitionsz00_1262));
									BgL_arg1329z00_1276 =
										MAKE_YOUNG_PAIR(BgL_arg1331z00_1277, BgL_regularsz00_1264);
								}
								{
									obj_t BgL_regularsz00_3283;
									obj_t BgL_transitionsz00_3282;

									BgL_transitionsz00_3282 = BgL_arg1328z00_1275;
									BgL_regularsz00_3283 = BgL_arg1329z00_1276;
									BgL_regularsz00_1264 = BgL_regularsz00_3283;
									BgL_transitionsz00_1262 = BgL_transitionsz00_3282;
									goto BgL_zc3z04anonymousza31319ze3z87_1265;
								}
							}
					}
			}
		}

	}



/* compile-regular */
	obj_t BGl_compilezd2regularzd2zz__rgc_compilez00(obj_t BgL_submatchesz00_10,
		obj_t BgL_currentzd2statezd2_11, obj_t BgL_transitionsz00_12,
		obj_t BgL_lastzd2matchzd2_13, obj_t BgL_pzd2ze3cz31_14)
	{
		{	/* Rgc/rgccompile.scm 139 */
			{	/* Rgc/rgccompile.scm 142 */
				obj_t BgL_statezd2transzd2_1281;
				obj_t BgL_positionsz00_1282;

				BgL_statezd2transzd2_1281 =
					BGl_statezd2transitionzd2listz00zz__rgc_compilez00
					(BgL_transitionsz00_12);
				BgL_positionsz00_1282 =
					BGl_statezd2positionszd2zz__rgc_dfaz00(BgL_currentzd2statezd2_11);
				if (NULLP(BgL_statezd2transzd2_1281))
					{	/* Rgc/rgccompile.scm 144 */
						return BgL_lastzd2matchzd2_13;
					}
				else
					{	/* Rgc/rgccompile.scm 146 */
						obj_t BgL_arg1335z00_1284;

						{	/* Rgc/rgccompile.scm 146 */
							obj_t BgL_arg1336z00_1285;
							obj_t BgL_arg1337z00_1286;

							{	/* Rgc/rgccompile.scm 146 */
								obj_t BgL_arg1338z00_1287;

								{	/* Rgc/rgccompile.scm 146 */
									obj_t BgL_arg1339z00_1288;

									BgL_arg1339z00_1288 =
										MAKE_YOUNG_PAIR(BGl_symbol2144z00zz__rgc_compilez00, BNIL);
									BgL_arg1338z00_1287 =
										MAKE_YOUNG_PAIR(BGl_symbol2146z00zz__rgc_compilez00,
										BgL_arg1339z00_1288);
								}
								BgL_arg1336z00_1285 =
									MAKE_YOUNG_PAIR(BGl_symbol2158z00zz__rgc_compilez00,
									BgL_arg1338z00_1287);
							}
							{	/* Rgc/rgccompile.scm 148 */
								obj_t BgL_arg1340z00_1289;
								obj_t BgL_arg1341z00_1290;

								{	/* Rgc/rgccompile.scm 148 */
									obj_t BgL_arg1342z00_1291;

									{	/* Rgc/rgccompile.scm 148 */
										obj_t BgL_arg1343z00_1292;
										obj_t BgL_arg1344z00_1293;

										{	/* Rgc/rgccompile.scm 148 */
											obj_t BgL_arg1346z00_1294;

											BgL_arg1346z00_1294 =
												MAKE_YOUNG_PAIR(BGl_symbol2150z00zz__rgc_compilez00,
												BNIL);
											BgL_arg1343z00_1292 =
												MAKE_YOUNG_PAIR(BGl_symbol2160z00zz__rgc_compilez00,
												BgL_arg1346z00_1294);
										}
										{	/* Rgc/rgccompile.scm 149 */
											obj_t BgL_arg1347z00_1295;
											obj_t BgL_arg1348z00_1296;

											{	/* Rgc/rgccompile.scm 149 */
												obj_t BgL_matchz00_2404;
												obj_t BgL_forwardz00_2405;
												obj_t BgL_bufposz00_2406;

												BgL_matchz00_2404 = BGl_symbol2148z00zz__rgc_compilez00;
												BgL_forwardz00_2405 = BGl_list2162z00zz__rgc_compilez00;
												BgL_bufposz00_2406 = BGl_list2165z00zz__rgc_compilez00;
												{	/* Rgc/rgccompile.scm 167 */
													obj_t BgL_arg1366z00_2407;
													obj_t BgL_arg1367z00_2408;

													BgL_arg1366z00_2407 =
														BGl_statezd2namezd2zz__rgc_dfaz00
														(BgL_currentzd2statezd2_11);
													{	/* Rgc/rgccompile.scm 167 */
														obj_t BgL_arg1368z00_2409;

														{	/* Rgc/rgccompile.scm 167 */
															obj_t BgL_arg1369z00_2410;

															{	/* Rgc/rgccompile.scm 167 */
																obj_t BgL_arg1370z00_2411;

																BgL_arg1370z00_2411 =
																	MAKE_YOUNG_PAIR(BgL_bufposz00_2406, BNIL);
																BgL_arg1369z00_2410 =
																	MAKE_YOUNG_PAIR(BgL_forwardz00_2405,
																	BgL_arg1370z00_2411);
															}
															BgL_arg1368z00_2409 =
																MAKE_YOUNG_PAIR(BgL_matchz00_2404,
																BgL_arg1369z00_2410);
														}
														BgL_arg1367z00_2408 =
															MAKE_YOUNG_PAIR
															(BGl_symbol2150z00zz__rgc_compilez00,
															BgL_arg1368z00_2409);
													}
													BgL_arg1347z00_1295 =
														MAKE_YOUNG_PAIR(BgL_arg1366z00_2407,
														BgL_arg1367z00_2408);
												}
											}
											BgL_arg1348z00_1296 =
												MAKE_YOUNG_PAIR(BgL_lastzd2matchzd2_13, BNIL);
											BgL_arg1344z00_1293 =
												MAKE_YOUNG_PAIR(BgL_arg1347z00_1295,
												BgL_arg1348z00_1296);
										}
										BgL_arg1342z00_1291 =
											MAKE_YOUNG_PAIR(BgL_arg1343z00_1292, BgL_arg1344z00_1293);
									}
									BgL_arg1340z00_1289 =
										MAKE_YOUNG_PAIR(BGl_symbol2168z00zz__rgc_compilez00,
										BgL_arg1342z00_1291);
								}
								{	/* Rgc/rgccompile.scm 155 */
									obj_t BgL_arg1349z00_1297;

									{	/* Rgc/rgccompile.scm 155 */
										obj_t BgL_arg1350z00_1298;

										{	/* Rgc/rgccompile.scm 155 */
											obj_t BgL_arg1351z00_1299;
											obj_t BgL_arg1352z00_1300;

											{	/* Rgc/rgccompile.scm 155 */
												obj_t BgL_arg1354z00_1301;

												{	/* Rgc/rgccompile.scm 155 */
													obj_t BgL_arg1356z00_1302;

													{	/* Rgc/rgccompile.scm 155 */
														obj_t BgL_arg1357z00_1303;

														{	/* Rgc/rgccompile.scm 155 */
															obj_t BgL_arg1358z00_1304;

															{	/* Rgc/rgccompile.scm 155 */
																obj_t BgL_arg1359z00_1305;

																BgL_arg1359z00_1305 =
																	MAKE_YOUNG_PAIR
																	(BGl_symbol2146z00zz__rgc_compilez00, BNIL);
																BgL_arg1358z00_1304 =
																	MAKE_YOUNG_PAIR
																	(BGl_symbol2150z00zz__rgc_compilez00,
																	BgL_arg1359z00_1305);
															}
															BgL_arg1357z00_1303 =
																MAKE_YOUNG_PAIR
																(BGl_symbol2170z00zz__rgc_compilez00,
																BgL_arg1358z00_1304);
														}
														BgL_arg1356z00_1302 =
															MAKE_YOUNG_PAIR(BgL_arg1357z00_1303, BNIL);
													}
													BgL_arg1354z00_1301 =
														MAKE_YOUNG_PAIR(BGl_symbol2172z00zz__rgc_compilez00,
														BgL_arg1356z00_1302);
												}
												BgL_arg1351z00_1299 =
													MAKE_YOUNG_PAIR(BgL_arg1354z00_1301, BNIL);
											}
											{	/* Rgc/rgccompile.scm 156 */
												obj_t BgL_arg1360z00_1306;
												obj_t BgL_arg1361z00_1307;

												BgL_arg1360z00_1306 =
													BGl_compilezd2submatcheszd2zz__rgc_compilez00
													(BGl_symbol2174z00zz__rgc_compilez00,
													BgL_submatchesz00_10, BgL_positionsz00_1282,
													BgL_pzd2ze3cz31_14);
												{	/* Rgc/rgccompile.scm 157 */
													obj_t BgL_arg1362z00_1308;

													if (
														(bgl_list_length(BgL_statezd2transzd2_1281) <= 12L))
														{	/* Rgc/rgccompile.scm 157 */
															BgL_arg1362z00_1308 =
																BGl_compilezd2condzd2regularz00zz__rgc_compilez00
																(BgL_currentzd2statezd2_11,
																BgL_statezd2transzd2_1281,
																BgL_lastzd2matchzd2_13);
														}
													else
														{	/* Rgc/rgccompile.scm 157 */
															BgL_arg1362z00_1308 =
																BGl_compilezd2casezd2regularz00zz__rgc_compilez00
																(BgL_currentzd2statezd2_11,
																BgL_statezd2transzd2_1281,
																BgL_lastzd2matchzd2_13);
														}
													BgL_arg1361z00_1307 =
														MAKE_YOUNG_PAIR(BgL_arg1362z00_1308, BNIL);
												}
												BgL_arg1352z00_1300 =
													BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
													(BgL_arg1360z00_1306, BgL_arg1361z00_1307);
											}
											BgL_arg1350z00_1298 =
												MAKE_YOUNG_PAIR(BgL_arg1351z00_1299,
												BgL_arg1352z00_1300);
										}
										BgL_arg1349z00_1297 =
											MAKE_YOUNG_PAIR(BGl_symbol2176z00zz__rgc_compilez00,
											BgL_arg1350z00_1298);
									}
									BgL_arg1341z00_1290 =
										MAKE_YOUNG_PAIR(BgL_arg1349z00_1297, BNIL);
								}
								BgL_arg1337z00_1286 =
									MAKE_YOUNG_PAIR(BgL_arg1340z00_1289, BgL_arg1341z00_1290);
							}
							BgL_arg1335z00_1284 =
								MAKE_YOUNG_PAIR(BgL_arg1336z00_1285, BgL_arg1337z00_1286);
						}
						return
							MAKE_YOUNG_PAIR(BGl_symbol2168z00zz__rgc_compilez00,
							BgL_arg1335z00_1284);
					}
			}
		}

	}



/* compile-case-regular */
	obj_t BGl_compilezd2casezd2regularz00zz__rgc_compilez00(obj_t
		BgL_currentzd2statezd2_19, obj_t BgL_statezd2transzd2_20,
		obj_t BgL_matchz00_21)
	{
		{	/* Rgc/rgccompile.scm 172 */
			{	/* Rgc/rgccompile.scm 182 */
				obj_t BgL_arg1371z00_1318;

				{	/* Rgc/rgccompile.scm 182 */
					obj_t BgL_arg1372z00_1319;

					{	/* Rgc/rgccompile.scm 182 */
						obj_t BgL_arg1373z00_1320;
						obj_t BgL_arg1375z00_1321;

						if (NULLP(BgL_statezd2transzd2_20))
							{	/* Rgc/rgccompile.scm 182 */
								BgL_arg1373z00_1320 = BNIL;
							}
						else
							{	/* Rgc/rgccompile.scm 182 */
								obj_t BgL_head1107z00_1324;

								{	/* Rgc/rgccompile.scm 182 */
									obj_t BgL_arg1383z00_1336;

									{	/* Rgc/rgccompile.scm 182 */
										obj_t BgL_arg1384z00_1337;

										BgL_arg1384z00_1337 =
											CAR(((obj_t) BgL_statezd2transzd2_20));
										BgL_arg1383z00_1336 =
											BGl_compilezd2casezd2transitionze70ze7zz__rgc_compilez00
											(BgL_matchz00_21, BgL_arg1384z00_1337);
									}
									BgL_head1107z00_1324 =
										MAKE_YOUNG_PAIR(BgL_arg1383z00_1336, BNIL);
								}
								{	/* Rgc/rgccompile.scm 182 */
									obj_t BgL_g1110z00_1325;

									BgL_g1110z00_1325 = CDR(((obj_t) BgL_statezd2transzd2_20));
									{
										obj_t BgL_l1105z00_2446;
										obj_t BgL_tail1108z00_2447;

										BgL_l1105z00_2446 = BgL_g1110z00_1325;
										BgL_tail1108z00_2447 = BgL_head1107z00_1324;
									BgL_zc3z04anonymousza31377ze3z87_2445:
										if (NULLP(BgL_l1105z00_2446))
											{	/* Rgc/rgccompile.scm 182 */
												BgL_arg1373z00_1320 = BgL_head1107z00_1324;
											}
										else
											{	/* Rgc/rgccompile.scm 182 */
												obj_t BgL_newtail1109z00_2454;

												{	/* Rgc/rgccompile.scm 182 */
													obj_t BgL_arg1380z00_2455;

													{	/* Rgc/rgccompile.scm 182 */
														obj_t BgL_arg1382z00_2456;

														BgL_arg1382z00_2456 =
															CAR(((obj_t) BgL_l1105z00_2446));
														BgL_arg1380z00_2455 =
															BGl_compilezd2casezd2transitionze70ze7zz__rgc_compilez00
															(BgL_matchz00_21, BgL_arg1382z00_2456);
													}
													BgL_newtail1109z00_2454 =
														MAKE_YOUNG_PAIR(BgL_arg1380z00_2455, BNIL);
												}
												SET_CDR(BgL_tail1108z00_2447, BgL_newtail1109z00_2454);
												{	/* Rgc/rgccompile.scm 182 */
													obj_t BgL_arg1379z00_2457;

													BgL_arg1379z00_2457 =
														CDR(((obj_t) BgL_l1105z00_2446));
													{
														obj_t BgL_tail1108z00_3341;
														obj_t BgL_l1105z00_3340;

														BgL_l1105z00_3340 = BgL_arg1379z00_2457;
														BgL_tail1108z00_3341 = BgL_newtail1109z00_2454;
														BgL_tail1108z00_2447 = BgL_tail1108z00_3341;
														BgL_l1105z00_2446 = BgL_l1105z00_3340;
														goto BgL_zc3z04anonymousza31377ze3z87_2445;
													}
												}
											}
									}
								}
							}
						{	/* Rgc/rgccompile.scm 183 */
							obj_t BgL_arg1387z00_1338;

							{	/* Rgc/rgccompile.scm 183 */
								obj_t BgL_arg1388z00_1339;

								BgL_arg1388z00_1339 = MAKE_YOUNG_PAIR(BgL_matchz00_21, BNIL);
								BgL_arg1387z00_1338 =
									MAKE_YOUNG_PAIR(BGl_symbol2178z00zz__rgc_compilez00,
									BgL_arg1388z00_1339);
							}
							BgL_arg1375z00_1321 = MAKE_YOUNG_PAIR(BgL_arg1387z00_1338, BNIL);
						}
						BgL_arg1372z00_1319 =
							BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
							(BgL_arg1373z00_1320, BgL_arg1375z00_1321);
					}
					BgL_arg1371z00_1318 =
						MAKE_YOUNG_PAIR(BGl_symbol2174z00zz__rgc_compilez00,
						BgL_arg1372z00_1319);
				}
				return
					MAKE_YOUNG_PAIR(BGl_symbol2180z00zz__rgc_compilez00,
					BgL_arg1371z00_1318);
			}
		}

	}



/* compile-case-transition~0 */
	obj_t BGl_compilezd2casezd2transitionze70ze7zz__rgc_compilez00(obj_t
		BgL_matchz00_3027, obj_t BgL_statezd2transzd2_1340)
	{
		{	/* Rgc/rgccompile.scm 179 */
			{	/* Rgc/rgccompile.scm 175 */
				obj_t BgL_setz00_1342;
				obj_t BgL_statez00_1343;

				BgL_setz00_1342 = CDR(((obj_t) BgL_statezd2transzd2_1340));
				BgL_statez00_1343 = CAR(((obj_t) BgL_statezd2transzd2_1340));
				{	/* Rgc/rgccompile.scm 177 */
					obj_t BgL_casezd2testzd2_1344;

					BgL_casezd2testzd2_1344 =
						BGl_rgcsetzd2ze3listz31zz__rgc_setz00(BgL_setz00_1342);
					{	/* Rgc/rgccompile.scm 178 */
						obj_t BgL_arg1390z00_1345;

						{	/* Rgc/rgccompile.scm 178 */
							obj_t BgL_arg1391z00_1346;

							{	/* Rgc/rgccompile.scm 178 */
								obj_t BgL_forwardz00_2420;
								obj_t BgL_bufposz00_2421;

								BgL_forwardz00_2420 = BGl_list2182z00zz__rgc_compilez00;
								BgL_bufposz00_2421 = BGl_symbol2144z00zz__rgc_compilez00;
								{	/* Rgc/rgccompile.scm 167 */
									obj_t BgL_arg1366z00_2422;
									obj_t BgL_arg1367z00_2423;

									BgL_arg1366z00_2422 =
										BGl_statezd2namezd2zz__rgc_dfaz00(BgL_statez00_1343);
									{	/* Rgc/rgccompile.scm 167 */
										obj_t BgL_arg1368z00_2424;

										{	/* Rgc/rgccompile.scm 167 */
											obj_t BgL_arg1369z00_2425;

											{	/* Rgc/rgccompile.scm 167 */
												obj_t BgL_arg1370z00_2426;

												BgL_arg1370z00_2426 =
													MAKE_YOUNG_PAIR(BgL_bufposz00_2421, BNIL);
												BgL_arg1369z00_2425 =
													MAKE_YOUNG_PAIR(BgL_forwardz00_2420,
													BgL_arg1370z00_2426);
											}
											BgL_arg1368z00_2424 =
												MAKE_YOUNG_PAIR(BgL_matchz00_3027, BgL_arg1369z00_2425);
										}
										BgL_arg1367z00_2423 =
											MAKE_YOUNG_PAIR(BGl_symbol2150z00zz__rgc_compilez00,
											BgL_arg1368z00_2424);
									}
									BgL_arg1391z00_1346 =
										MAKE_YOUNG_PAIR(BgL_arg1366z00_2422, BgL_arg1367z00_2423);
								}
							}
							BgL_arg1390z00_1345 = MAKE_YOUNG_PAIR(BgL_arg1391z00_1346, BNIL);
						}
						return
							MAKE_YOUNG_PAIR(BgL_casezd2testzd2_1344, BgL_arg1390z00_1345);
					}
				}
			}
		}

	}



/* compile-cond-regular */
	obj_t BGl_compilezd2condzd2regularz00zz__rgc_compilez00(obj_t
		BgL_currentzd2statezd2_22, obj_t BgL_statezd2transzd2_23,
		obj_t BgL_matchz00_24)
	{
		{	/* Rgc/rgccompile.scm 189 */
			{
				obj_t BgL_statezd2transzd2_1381;
				long BgL_prevzd2testzd2lenz00_1382;

				{
					obj_t BgL_transz00_1351;
					obj_t BgL_testsz00_1352;
					long BgL_costz00_1353;
					long BgL_prevzd2lenzd2_1354;
					bool_t BgL_elsepz00_1355;

					BgL_transz00_1351 = BgL_statezd2transzd2_23;
					BgL_testsz00_1352 = BNIL;
					BgL_costz00_1353 = 0L;
					BgL_prevzd2lenzd2_1354 = 0L;
					BgL_elsepz00_1355 = ((bool_t) 0);
				BgL_zc3z04anonymousza31392ze3z87_1356:
					{	/* Rgc/rgccompile.scm 206 */
						bool_t BgL_test2265z00_3361;

						if (NULLP(BgL_transz00_1351))
							{	/* Rgc/rgccompile.scm 206 */
								BgL_test2265z00_3361 = ((bool_t) 1);
							}
						else
							{	/* Rgc/rgccompile.scm 206 */
								BgL_test2265z00_3361 = BgL_elsepz00_1355;
							}
						if (BgL_test2265z00_3361)
							{	/* Rgc/rgccompile.scm 206 */
								if (
									(BgL_costz00_1353 >
										BGl_za2casezd2thresholdza2zd2zz__rgc_compilez00))
									{	/* Rgc/rgccompile.scm 208 */
										BGL_TAIL return
											BGl_compilezd2casezd2regularz00zz__rgc_compilez00
											(BgL_currentzd2statezd2_22, BgL_statezd2transzd2_23,
											BgL_matchz00_24);
									}
								else
									{	/* Rgc/rgccompile.scm 211 */
										obj_t BgL_arg1396z00_1360;

										{	/* Rgc/rgccompile.scm 211 */
											obj_t BgL_arg1397z00_1361;
											obj_t BgL_arg1399z00_1362;

											BgL_arg1397z00_1361 = bgl_reverse_bang(BgL_testsz00_1352);
											{	/* Rgc/rgccompile.scm 212 */
												obj_t BgL_arg1400z00_1363;

												if (BgL_elsepz00_1355)
													{	/* Rgc/rgccompile.scm 212 */
														BgL_arg1400z00_1363 = BNIL;
													}
												else
													{	/* Rgc/rgccompile.scm 214 */
														obj_t BgL_arg1401z00_1364;

														{	/* Rgc/rgccompile.scm 214 */
															obj_t BgL_arg1402z00_1365;

															BgL_arg1402z00_1365 =
																MAKE_YOUNG_PAIR(BgL_matchz00_24, BNIL);
															BgL_arg1401z00_1364 =
																MAKE_YOUNG_PAIR
																(BGl_symbol2178z00zz__rgc_compilez00,
																BgL_arg1402z00_1365);
														}
														BgL_arg1400z00_1363 =
															MAKE_YOUNG_PAIR(BgL_arg1401z00_1364, BNIL);
													}
												BgL_arg1399z00_1362 =
													BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
													(BgL_arg1400z00_1363, BNIL);
											}
											BgL_arg1396z00_1360 =
												BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
												(BgL_arg1397z00_1361, BgL_arg1399z00_1362);
										}
										return
											MAKE_YOUNG_PAIR(BGl_symbol2185z00zz__rgc_compilez00,
											BgL_arg1396z00_1360);
									}
							}
						else
							{	/* Rgc/rgccompile.scm 215 */
								obj_t BgL_testz00_1366;

								{	/* Rgc/rgccompile.scm 216 */
									obj_t BgL_arg1413z00_1378;

									BgL_arg1413z00_1378 = CAR(((obj_t) BgL_transz00_1351));
									BgL_statezd2transzd2_1381 = BgL_arg1413z00_1378;
									BgL_prevzd2testzd2lenz00_1382 = BgL_prevzd2lenzd2_1354;
									{	/* Rgc/rgccompile.scm 192 */
										obj_t BgL_setz00_1384;
										obj_t BgL_statez00_1385;

										BgL_setz00_1384 = CDR(((obj_t) BgL_statezd2transzd2_1381));
										BgL_statez00_1385 =
											CAR(((obj_t) BgL_statezd2transzd2_1381));
										{	/* Rgc/rgccompile.scm 194 */
											obj_t BgL_condzd2testzd2_1386;

											BgL_condzd2testzd2_1386 =
												BGl_compilezd2condzd2testz00zz__rgc_compilez00
												(BgL_setz00_1384, BGl_symbol2174z00zz__rgc_compilez00,
												BgL_prevzd2testzd2lenz00_1382);
											{	/* Rgc/rgccompile.scm 195 */
												obj_t BgL_condzd2costzd2_1387;

												{	/* Rgc/rgccompile.scm 196 */
													obj_t BgL_tmpz00_2463;

													{	/* Rgc/rgccompile.scm 196 */
														int BgL_tmpz00_3382;

														BgL_tmpz00_3382 = (int) (1L);
														BgL_tmpz00_2463 = BGL_MVALUES_VAL(BgL_tmpz00_3382);
													}
													{	/* Rgc/rgccompile.scm 196 */
														int BgL_tmpz00_3385;

														BgL_tmpz00_3385 = (int) (1L);
														BGL_MVALUES_VAL_SET(BgL_tmpz00_3385, BUNSPEC);
													}
													BgL_condzd2costzd2_1387 = BgL_tmpz00_2463;
												}
												{	/* Rgc/rgccompile.scm 196 */
													obj_t BgL_val0_1111z00_1388;

													{	/* Rgc/rgccompile.scm 197 */
														obj_t BgL_arg1415z00_1390;

														{	/* Rgc/rgccompile.scm 197 */
															obj_t BgL_arg1416z00_1391;

															{	/* Rgc/rgccompile.scm 197 */
																obj_t BgL_forwardz00_2464;
																obj_t BgL_bufposz00_2465;

																BgL_forwardz00_2464 =
																	BGl_list2182z00zz__rgc_compilez00;
																BgL_bufposz00_2465 =
																	BGl_symbol2144z00zz__rgc_compilez00;
																{	/* Rgc/rgccompile.scm 167 */
																	obj_t BgL_arg1366z00_2466;
																	obj_t BgL_arg1367z00_2467;

																	BgL_arg1366z00_2466 =
																		BGl_statezd2namezd2zz__rgc_dfaz00
																		(BgL_statez00_1385);
																	{	/* Rgc/rgccompile.scm 167 */
																		obj_t BgL_arg1368z00_2468;

																		{	/* Rgc/rgccompile.scm 167 */
																			obj_t BgL_arg1369z00_2469;

																			{	/* Rgc/rgccompile.scm 167 */
																				obj_t BgL_arg1370z00_2470;

																				BgL_arg1370z00_2470 =
																					MAKE_YOUNG_PAIR(BgL_bufposz00_2465,
																					BNIL);
																				BgL_arg1369z00_2469 =
																					MAKE_YOUNG_PAIR(BgL_forwardz00_2464,
																					BgL_arg1370z00_2470);
																			}
																			BgL_arg1368z00_2468 =
																				MAKE_YOUNG_PAIR(BgL_matchz00_24,
																				BgL_arg1369z00_2469);
																		}
																		BgL_arg1367z00_2467 =
																			MAKE_YOUNG_PAIR
																			(BGl_symbol2150z00zz__rgc_compilez00,
																			BgL_arg1368z00_2468);
																	}
																	BgL_arg1416z00_1391 =
																		MAKE_YOUNG_PAIR(BgL_arg1366z00_2466,
																		BgL_arg1367z00_2467);
															}}
															BgL_arg1415z00_1390 =
																MAKE_YOUNG_PAIR(BgL_arg1416z00_1391, BNIL);
														}
														BgL_val0_1111z00_1388 =
															MAKE_YOUNG_PAIR(BgL_condzd2testzd2_1386,
															BgL_arg1415z00_1390);
													}
													{	/* Rgc/rgccompile.scm 196 */
														int BgL_tmpz00_3396;

														BgL_tmpz00_3396 = (int) (2L);
														BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3396);
													}
													{	/* Rgc/rgccompile.scm 196 */
														int BgL_tmpz00_3399;

														BgL_tmpz00_3399 = (int) (1L);
														BGL_MVALUES_VAL_SET(BgL_tmpz00_3399,
															BgL_condzd2costzd2_1387);
													}
													BgL_testz00_1366 = BgL_val0_1111z00_1388;
								}}}}}
								{	/* Rgc/rgccompile.scm 216 */
									obj_t BgL_cz00_1367;

									{	/* Rgc/rgccompile.scm 217 */
										obj_t BgL_tmpz00_2474;

										{	/* Rgc/rgccompile.scm 217 */
											int BgL_tmpz00_3402;

											BgL_tmpz00_3402 = (int) (1L);
											BgL_tmpz00_2474 = BGL_MVALUES_VAL(BgL_tmpz00_3402);
										}
										{	/* Rgc/rgccompile.scm 217 */
											int BgL_tmpz00_3405;

											BgL_tmpz00_3405 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_3405, BUNSPEC);
										}
										BgL_cz00_1367 = BgL_tmpz00_2474;
									}
									{	/* Rgc/rgccompile.scm 217 */
										obj_t BgL_arg1403z00_1368;
										obj_t BgL_arg1404z00_1369;
										long BgL_arg1405z00_1370;
										long BgL_arg1406z00_1371;
										bool_t BgL_arg1407z00_1372;

										BgL_arg1403z00_1368 = CDR(((obj_t) BgL_transz00_1351));
										BgL_arg1404z00_1369 =
											MAKE_YOUNG_PAIR(BgL_testz00_1366, BgL_testsz00_1352);
										BgL_arg1405z00_1370 =
											((long) CINT(BgL_cz00_1367) + BgL_costz00_1353);
										{	/* Rgc/rgccompile.scm 220 */
											long BgL_arg1408z00_1373;

											{	/* Rgc/rgccompile.scm 220 */
												obj_t BgL_arg1410z00_1374;

												{	/* Rgc/rgccompile.scm 220 */
													obj_t BgL_pairz00_2479;

													BgL_pairz00_2479 = CAR(((obj_t) BgL_transz00_1351));
													BgL_arg1410z00_1374 = CDR(BgL_pairz00_2479);
												}
												BgL_arg1408z00_1373 =
													BGl_rgcsetzd2lengthzd2zz__rgc_setz00
													(BgL_arg1410z00_1374);
											}
											BgL_arg1406z00_1371 =
												(BgL_prevzd2lenzd2_1354 + BgL_arg1408z00_1373);
										}
										BgL_arg1407z00_1372 =
											(CAR(BgL_testz00_1366) ==
											BGl_symbol2178z00zz__rgc_compilez00);
										{
											bool_t BgL_elsepz00_3424;
											long BgL_prevzd2lenzd2_3423;
											long BgL_costz00_3422;
											obj_t BgL_testsz00_3421;
											obj_t BgL_transz00_3420;

											BgL_transz00_3420 = BgL_arg1403z00_1368;
											BgL_testsz00_3421 = BgL_arg1404z00_1369;
											BgL_costz00_3422 = BgL_arg1405z00_1370;
											BgL_prevzd2lenzd2_3423 = BgL_arg1406z00_1371;
											BgL_elsepz00_3424 = BgL_arg1407z00_1372;
											BgL_elsepz00_1355 = BgL_elsepz00_3424;
											BgL_prevzd2lenzd2_1354 = BgL_prevzd2lenzd2_3423;
											BgL_costz00_1353 = BgL_costz00_3422;
											BgL_testsz00_1352 = BgL_testsz00_3421;
											BgL_transz00_1351 = BgL_transz00_3420;
											goto BgL_zc3z04anonymousza31392ze3z87_1356;
										}
									}
								}
							}
					}
				}
			}
		}

	}



/* compile-cond-test */
	obj_t BGl_compilezd2condzd2testz00zz__rgc_compilez00(obj_t BgL_setz00_25,
		obj_t BgL_varz00_26, long BgL_prevzd2testzd2lenz00_27)
	{
		{	/* Rgc/rgccompile.scm 226 */
			{	/* Rgc/rgccompile.scm 278 */
				obj_t BgL_maxz00_1397;
				long BgL_lenz00_1398;

				BgL_maxz00_1397 = BGl_rgczd2maxzd2charz00zz__rgc_configz00();
				BgL_lenz00_1398 = BGl_rgcsetzd2lengthzd2zz__rgc_setz00(BgL_setz00_25);
				{	/* Rgc/rgccompile.scm 281 */
					bool_t BgL_test2269z00_3427;

					{	/* Rgc/rgccompile.scm 281 */
						obj_t BgL_arg1427z00_1415;
						long BgL_arg1428z00_1416;

						BgL_arg1427z00_1415 = BGl_rgczd2maxzd2charz00zz__rgc_configz00();
						BgL_arg1428z00_1416 =
							(BgL_lenz00_1398 + BgL_prevzd2testzd2lenz00_27);
						BgL_test2269z00_3427 =
							((long) CINT(BgL_arg1427z00_1415) == BgL_arg1428z00_1416);
					}
					if (BgL_test2269z00_3427)
						{	/* Rgc/rgccompile.scm 285 */
							obj_t BgL_val0_1127z00_1402;

							BgL_val0_1127z00_1402 = BGl_symbol2178z00zz__rgc_compilez00;
							{	/* Rgc/rgccompile.scm 285 */
								int BgL_tmpz00_3432;

								BgL_tmpz00_3432 = (int) (2L);
								BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3432);
							}
							{	/* Rgc/rgccompile.scm 285 */
								obj_t BgL_auxz00_3437;
								int BgL_tmpz00_3435;

								BgL_auxz00_3437 = BINT(0L);
								BgL_tmpz00_3435 = (int) (1L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_3435, BgL_auxz00_3437);
							}
							return BgL_val0_1127z00_1402;
						}
					else
						{	/* Rgc/rgccompile.scm 281 */
							if (
								(BgL_lenz00_1398 > (2L + ((long) CINT(BgL_maxz00_1397) / 2L))))
								{	/* Rgc/rgccompile.scm 288 */
									obj_t BgL_testz00_1407;

									BgL_testz00_1407 =
										BGl_compilezd2rangezd2testze70ze7zz__rgc_compilez00
										(BgL_varz00_26,
										BGl_rgcsetzd2notzd2zz__rgc_setz00(BgL_setz00_25));
									{	/* Rgc/rgccompile.scm 289 */
										obj_t BgL_costz00_1408;

										{	/* Rgc/rgccompile.scm 290 */
											obj_t BgL_tmpz00_2557;

											{	/* Rgc/rgccompile.scm 290 */
												int BgL_tmpz00_3447;

												BgL_tmpz00_3447 = (int) (1L);
												BgL_tmpz00_2557 = BGL_MVALUES_VAL(BgL_tmpz00_3447);
											}
											{	/* Rgc/rgccompile.scm 290 */
												int BgL_tmpz00_3450;

												BgL_tmpz00_3450 = (int) (1L);
												BGL_MVALUES_VAL_SET(BgL_tmpz00_3450, BUNSPEC);
											}
											BgL_costz00_1408 = BgL_tmpz00_2557;
										}
										{	/* Rgc/rgccompile.scm 290 */
											obj_t BgL_val0_1129z00_1409;
											long BgL_val1_1130z00_1410;

											{	/* Rgc/rgccompile.scm 290 */
												obj_t BgL_arg1423z00_1411;

												BgL_arg1423z00_1411 =
													MAKE_YOUNG_PAIR(BgL_testz00_1407, BNIL);
												BgL_val0_1129z00_1409 =
													MAKE_YOUNG_PAIR(BGl_symbol2187z00zz__rgc_compilez00,
													BgL_arg1423z00_1411);
											}
											BgL_val1_1130z00_1410 =
												(1L + (long) CINT(BgL_costz00_1408));
											{	/* Rgc/rgccompile.scm 290 */
												int BgL_tmpz00_3457;

												BgL_tmpz00_3457 = (int) (2L);
												BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3457);
											}
											{	/* Rgc/rgccompile.scm 290 */
												obj_t BgL_auxz00_3462;
												int BgL_tmpz00_3460;

												BgL_auxz00_3462 = BINT(BgL_val1_1130z00_1410);
												BgL_tmpz00_3460 = (int) (1L);
												BGL_MVALUES_VAL_SET(BgL_tmpz00_3460, BgL_auxz00_3462);
											}
											return BgL_val0_1129z00_1409;
										}
									}
								}
							else
								{	/* Rgc/rgccompile.scm 286 */
									return
										BGl_compilezd2rangezd2testze70ze7zz__rgc_compilez00
										(BgL_varz00_26, BgL_setz00_25);
								}
						}
				}
			}
		}

	}



/* compile-range-test~0 */
	obj_t BGl_compilezd2rangezd2testze70ze7zz__rgc_compilez00(obj_t
		BgL_varz00_3026, obj_t BgL_setz00_1499)
	{
		{	/* Rgc/rgccompile.scm 276 */
			{
				obj_t BgL_startz00_1439;
				obj_t BgL_stopz00_1440;
				obj_t BgL_setz00_1441;

				{	/* Rgc/rgccompile.scm 266 */
					long BgL_g1044z00_1501;

					{	/* Rgc/rgccompile.scm 229 */
						obj_t BgL_maxz00_2518;

						BgL_maxz00_2518 = BGl_rgczd2maxzd2charz00zz__rgc_configz00();
						{
							long BgL_iz00_2520;

							BgL_iz00_2520 = 1L;
						BgL_loopz00_2519:
							if ((BgL_iz00_2520 == (long) CINT(BgL_maxz00_2518)))
								{	/* Rgc/rgccompile.scm 232 */
									BgL_g1044z00_1501 = -1L;
								}
							else
								{	/* Rgc/rgccompile.scm 232 */
									if (BGl_rgcsetzd2memberzf3z21zz__rgc_setz00(BgL_setz00_1499,
											BgL_iz00_2520))
										{	/* Rgc/rgccompile.scm 233 */
											BgL_g1044z00_1501 = BgL_iz00_2520;
										}
									else
										{
											long BgL_iz00_3472;

											BgL_iz00_3472 = (BgL_iz00_2520 + 1L);
											BgL_iz00_2520 = BgL_iz00_3472;
											goto BgL_loopz00_2519;
										}
								}
						}
					}
					{
						obj_t BgL_startz00_1504;
						obj_t BgL_testsz00_1505;
						long BgL_costz00_1506;

						BgL_startz00_1504 = BINT(BgL_g1044z00_1501);
						BgL_testsz00_1505 = BNIL;
						BgL_costz00_1506 = 0L;
					BgL_zc3z04anonymousza31483ze3z87_1507:
						if (((long) CINT(BgL_startz00_1504) == -1L))
							{	/* Rgc/rgccompile.scm 270 */
								obj_t BgL_val0_1125z00_1509;

								{	/* Rgc/rgccompile.scm 270 */
									obj_t BgL_arg1485z00_1511;

									BgL_arg1485z00_1511 =
										BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
										(bgl_reverse_bang(BgL_testsz00_1505), BNIL);
									BgL_val0_1125z00_1509 =
										MAKE_YOUNG_PAIR(BGl_symbol2189z00zz__rgc_compilez00,
										BgL_arg1485z00_1511);
								}
								{	/* Rgc/rgccompile.scm 270 */
									int BgL_tmpz00_3480;

									BgL_tmpz00_3480 = (int) (2L);
									BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3480);
								}
								{	/* Rgc/rgccompile.scm 270 */
									obj_t BgL_auxz00_3485;
									int BgL_tmpz00_3483;

									BgL_auxz00_3485 = BINT(BgL_costz00_1506);
									BgL_tmpz00_3483 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3483, BgL_auxz00_3485);
								}
								return BgL_val0_1125z00_1509;
							}
						else
							{	/* Rgc/rgccompile.scm 271 */
								obj_t BgL_stopz00_1513;

								{	/* Rgc/rgccompile.scm 237 */
									obj_t BgL_maxz00_2528;

									BgL_maxz00_2528 = BGl_rgczd2maxzd2charz00zz__rgc_configz00();
									{
										obj_t BgL_iz00_2530;

										BgL_iz00_2530 = BgL_startz00_1504;
									BgL_loopz00_2529:
										if (
											((long) CINT(BgL_iz00_2530) ==
												(long) CINT(BgL_maxz00_2528)))
											{	/* Rgc/rgccompile.scm 240 */
												BgL_stopz00_1513 = BgL_maxz00_2528;
											}
										else
											{	/* Rgc/rgccompile.scm 240 */
												if (BGl_rgcsetzd2memberzf3z21zz__rgc_setz00
													(BgL_setz00_1499, (long) CINT(BgL_iz00_2530)))
													{	/* Rgc/rgccompile.scm 241 */
														long BgL_arg1439z00_2533;

														BgL_arg1439z00_2533 =
															((long) CINT(BgL_iz00_2530) + 1L);
														{
															obj_t BgL_iz00_3498;

															BgL_iz00_3498 = BINT(BgL_arg1439z00_2533);
															BgL_iz00_2530 = BgL_iz00_3498;
															goto BgL_loopz00_2529;
														}
													}
												else
													{	/* Rgc/rgccompile.scm 241 */
														BgL_stopz00_1513 = BgL_iz00_2530;
													}
											}
									}
								}
								{	/* Rgc/rgccompile.scm 272 */
									obj_t BgL_testz00_1514;

									BgL_startz00_1439 = BgL_startz00_1504;
									BgL_stopz00_1440 = BgL_stopz00_1513;
									BgL_setz00_1441 = BgL_setz00_1499;
									if (
										(((long) CINT(BgL_stopz00_1440) - 1L) ==
											(long) CINT(BgL_startz00_1439)))
										{	/* Rgc/rgccompile.scm 247 */
											obj_t BgL_val0_1113z00_1445;

											{	/* Rgc/rgccompile.scm 247 */
												obj_t BgL_arg1443z00_1447;

												{	/* Rgc/rgccompile.scm 247 */
													obj_t BgL_arg1444z00_1448;

													BgL_arg1444z00_1448 =
														MAKE_YOUNG_PAIR(BgL_startz00_1439, BNIL);
													BgL_arg1443z00_1447 =
														MAKE_YOUNG_PAIR(BgL_varz00_3026,
														BgL_arg1444z00_1448);
												}
												BgL_val0_1113z00_1445 =
													MAKE_YOUNG_PAIR(BGl_symbol2158z00zz__rgc_compilez00,
													BgL_arg1443z00_1447);
											}
											{	/* Rgc/rgccompile.scm 247 */
												int BgL_tmpz00_3508;

												BgL_tmpz00_3508 = (int) (2L);
												BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3508);
											}
											{	/* Rgc/rgccompile.scm 247 */
												obj_t BgL_auxz00_3513;
												int BgL_tmpz00_3511;

												BgL_auxz00_3513 = BINT(1L);
												BgL_tmpz00_3511 = (int) (1L);
												BGL_MVALUES_VAL_SET(BgL_tmpz00_3511, BgL_auxz00_3513);
											}
											BgL_testz00_1514 = BgL_val0_1113z00_1445;
										}
									else
										{	/* Rgc/rgccompile.scm 246 */
											if (
												(((long) CINT(BgL_stopz00_1440) -
														(long) CINT(BgL_startz00_1439)) < 4L))
												{	/* Rgc/rgccompile.scm 249 */
													obj_t BgL_val0_1115z00_1451;
													long BgL_val1_1116z00_1452;

													{	/* Rgc/rgccompile.scm 249 */
														obj_t BgL_arg1447z00_1453;

														{	/* Rgc/rgccompile.scm 249 */
															obj_t BgL_arg1448z00_1454;

															{
																obj_t BgL_startz00_1457;
																obj_t BgL_resz00_1458;

																BgL_startz00_1457 = BgL_startz00_1439;
																BgL_resz00_1458 = BNIL;
															BgL_zc3z04anonymousza31449ze3z87_1459:
																if (
																	((long) CINT(BgL_startz00_1457) ==
																		(long) CINT(BgL_stopz00_1440)))
																	{	/* Rgc/rgccompile.scm 251 */
																		BgL_arg1448z00_1454 = BgL_resz00_1458;
																	}
																else
																	{	/* Rgc/rgccompile.scm 253 */
																		long BgL_arg1451z00_1461;
																		obj_t BgL_arg1452z00_1462;

																		BgL_arg1451z00_1461 =
																			((long) CINT(BgL_startz00_1457) + 1L);
																		{	/* Rgc/rgccompile.scm 254 */
																			obj_t BgL_arg1453z00_1463;

																			{	/* Rgc/rgccompile.scm 254 */
																				obj_t BgL_arg1454z00_1464;

																				{	/* Rgc/rgccompile.scm 254 */
																					obj_t BgL_arg1455z00_1465;

																					BgL_arg1455z00_1465 =
																						MAKE_YOUNG_PAIR(BgL_startz00_1457,
																						BNIL);
																					BgL_arg1454z00_1464 =
																						MAKE_YOUNG_PAIR(BgL_varz00_3026,
																						BgL_arg1455z00_1465);
																				}
																				BgL_arg1453z00_1463 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol2158z00zz__rgc_compilez00,
																					BgL_arg1454z00_1464);
																			}
																			BgL_arg1452z00_1462 =
																				MAKE_YOUNG_PAIR(BgL_arg1453z00_1463,
																				BgL_resz00_1458);
																		}
																		{
																			obj_t BgL_resz00_3533;
																			obj_t BgL_startz00_3531;

																			BgL_startz00_3531 =
																				BINT(BgL_arg1451z00_1461);
																			BgL_resz00_3533 = BgL_arg1452z00_1462;
																			BgL_resz00_1458 = BgL_resz00_3533;
																			BgL_startz00_1457 = BgL_startz00_3531;
																			goto
																				BgL_zc3z04anonymousza31449ze3z87_1459;
																		}
																	}
															}
															BgL_arg1447z00_1453 =
																BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																(BgL_arg1448z00_1454, BNIL);
														}
														BgL_val0_1115z00_1451 =
															MAKE_YOUNG_PAIR
															(BGl_symbol2189z00zz__rgc_compilez00,
															BgL_arg1447z00_1453);
													}
													BgL_val1_1116z00_1452 =
														(
														(long) CINT(BgL_stopz00_1440) -
														(long) CINT(BgL_startz00_1439));
													{	/* Rgc/rgccompile.scm 249 */
														int BgL_tmpz00_3539;

														BgL_tmpz00_3539 = (int) (2L);
														BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3539);
													}
													{	/* Rgc/rgccompile.scm 249 */
														obj_t BgL_auxz00_3544;
														int BgL_tmpz00_3542;

														BgL_auxz00_3544 = BINT(BgL_val1_1116z00_1452);
														BgL_tmpz00_3542 = (int) (1L);
														BGL_MVALUES_VAL_SET(BgL_tmpz00_3542,
															BgL_auxz00_3544);
													}
													BgL_testz00_1514 = BgL_val0_1115z00_1451;
												}
											else
												{	/* Rgc/rgccompile.scm 256 */
													bool_t BgL_test2279z00_3547;

													{	/* Rgc/rgccompile.scm 256 */
														bool_t BgL_test2280z00_3548;

														{	/* Rgc/rgccompile.scm 256 */
															obj_t BgL_arg1479z00_1496;

															BgL_arg1479z00_1496 =
																BGl_rgczd2maxzd2charz00zz__rgc_configz00();
															BgL_test2280z00_3548 =
																(
																(long) CINT(BgL_stopz00_1440) ==
																(long) CINT(BgL_arg1479z00_1496));
														}
														if (BgL_test2280z00_3548)
															{	/* Rgc/rgccompile.scm 256 */
																BgL_test2279z00_3547 =
																	((long) CINT(BgL_startz00_1439) == 1L);
															}
														else
															{	/* Rgc/rgccompile.scm 256 */
																BgL_test2279z00_3547 = ((bool_t) 0);
															}
													}
													if (BgL_test2279z00_3547)
														{	/* Rgc/rgccompile.scm 256 */
															{	/* Rgc/rgccompile.scm 257 */
																int BgL_tmpz00_3555;

																BgL_tmpz00_3555 = (int) (2L);
																BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3555);
															}
															{	/* Rgc/rgccompile.scm 257 */
																obj_t BgL_auxz00_3560;
																int BgL_tmpz00_3558;

																BgL_auxz00_3560 = BINT(0L);
																BgL_tmpz00_3558 = (int) (1L);
																BGL_MVALUES_VAL_SET(BgL_tmpz00_3558,
																	BgL_auxz00_3560);
															}
															BgL_testz00_1514 = BTRUE;
														}
													else
														{	/* Rgc/rgccompile.scm 258 */
															bool_t BgL_test2281z00_3563;

															{	/* Rgc/rgccompile.scm 258 */
																obj_t BgL_arg1478z00_1494;

																BgL_arg1478z00_1494 =
																	BGl_rgczd2maxzd2charz00zz__rgc_configz00();
																BgL_test2281z00_3563 =
																	(
																	(long) CINT(BgL_stopz00_1440) ==
																	(long) CINT(BgL_arg1478z00_1494));
															}
															if (BgL_test2281z00_3563)
																{	/* Rgc/rgccompile.scm 259 */
																	obj_t BgL_val0_1119z00_1475;

																	{	/* Rgc/rgccompile.scm 259 */
																		obj_t BgL_arg1462z00_1477;

																		{	/* Rgc/rgccompile.scm 259 */
																			obj_t BgL_arg1463z00_1478;

																			BgL_arg1463z00_1478 =
																				MAKE_YOUNG_PAIR(BgL_startz00_1439,
																				BNIL);
																			BgL_arg1462z00_1477 =
																				MAKE_YOUNG_PAIR(BgL_varz00_3026,
																				BgL_arg1463z00_1478);
																		}
																		BgL_val0_1119z00_1475 =
																			MAKE_YOUNG_PAIR
																			(BGl_symbol2191z00zz__rgc_compilez00,
																			BgL_arg1462z00_1477);
																	}
																	{	/* Rgc/rgccompile.scm 259 */
																		int BgL_tmpz00_3571;

																		BgL_tmpz00_3571 = (int) (2L);
																		BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3571);
																	}
																	{	/* Rgc/rgccompile.scm 259 */
																		obj_t BgL_auxz00_3576;
																		int BgL_tmpz00_3574;

																		BgL_auxz00_3576 = BINT(1L);
																		BgL_tmpz00_3574 = (int) (1L);
																		BGL_MVALUES_VAL_SET(BgL_tmpz00_3574,
																			BgL_auxz00_3576);
																	}
																	BgL_testz00_1514 = BgL_val0_1119z00_1475;
																}
															else
																{	/* Rgc/rgccompile.scm 258 */
																	if (((long) CINT(BgL_startz00_1439) == 1L))
																		{	/* Rgc/rgccompile.scm 261 */
																			obj_t BgL_val0_1121z00_1480;

																			{	/* Rgc/rgccompile.scm 261 */
																				obj_t BgL_arg1465z00_1482;

																				{	/* Rgc/rgccompile.scm 261 */
																					obj_t BgL_arg1466z00_1483;

																					BgL_arg1466z00_1483 =
																						MAKE_YOUNG_PAIR(BgL_stopz00_1440,
																						BNIL);
																					BgL_arg1465z00_1482 =
																						MAKE_YOUNG_PAIR(BgL_varz00_3026,
																						BgL_arg1466z00_1483);
																				}
																				BgL_val0_1121z00_1480 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol2193z00zz__rgc_compilez00,
																					BgL_arg1465z00_1482);
																			}
																			{	/* Rgc/rgccompile.scm 261 */
																				int BgL_tmpz00_3585;

																				BgL_tmpz00_3585 = (int) (2L);
																				BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3585);
																			}
																			{	/* Rgc/rgccompile.scm 261 */
																				obj_t BgL_auxz00_3590;
																				int BgL_tmpz00_3588;

																				BgL_auxz00_3590 = BINT(1L);
																				BgL_tmpz00_3588 = (int) (1L);
																				BGL_MVALUES_VAL_SET(BgL_tmpz00_3588,
																					BgL_auxz00_3590);
																			}
																			BgL_testz00_1514 = BgL_val0_1121z00_1480;
																		}
																	else
																		{	/* Rgc/rgccompile.scm 263 */
																			obj_t BgL_val0_1123z00_1484;

																			{	/* Rgc/rgccompile.scm 263 */
																				obj_t BgL_arg1467z00_1486;

																				{	/* Rgc/rgccompile.scm 263 */
																					obj_t BgL_arg1468z00_1487;
																					obj_t BgL_arg1469z00_1488;

																					{	/* Rgc/rgccompile.scm 263 */
																						obj_t BgL_arg1472z00_1489;

																						{	/* Rgc/rgccompile.scm 263 */
																							obj_t BgL_arg1473z00_1490;

																							BgL_arg1473z00_1490 =
																								MAKE_YOUNG_PAIR
																								(BgL_startz00_1439, BNIL);
																							BgL_arg1472z00_1489 =
																								MAKE_YOUNG_PAIR(BgL_varz00_3026,
																								BgL_arg1473z00_1490);
																						}
																						BgL_arg1468z00_1487 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol2191z00zz__rgc_compilez00,
																							BgL_arg1472z00_1489);
																					}
																					{	/* Rgc/rgccompile.scm 263 */
																						obj_t BgL_arg1474z00_1491;

																						{	/* Rgc/rgccompile.scm 263 */
																							obj_t BgL_arg1476z00_1492;

																							{	/* Rgc/rgccompile.scm 263 */
																								obj_t BgL_arg1477z00_1493;

																								BgL_arg1477z00_1493 =
																									MAKE_YOUNG_PAIR
																									(BgL_stopz00_1440, BNIL);
																								BgL_arg1476z00_1492 =
																									MAKE_YOUNG_PAIR
																									(BgL_varz00_3026,
																									BgL_arg1477z00_1493);
																							}
																							BgL_arg1474z00_1491 =
																								MAKE_YOUNG_PAIR
																								(BGl_symbol2193z00zz__rgc_compilez00,
																								BgL_arg1476z00_1492);
																						}
																						BgL_arg1469z00_1488 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1474z00_1491, BNIL);
																					}
																					BgL_arg1467z00_1486 =
																						MAKE_YOUNG_PAIR(BgL_arg1468z00_1487,
																						BgL_arg1469z00_1488);
																				}
																				BgL_val0_1123z00_1484 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol2195z00zz__rgc_compilez00,
																					BgL_arg1467z00_1486);
																			}
																			{	/* Rgc/rgccompile.scm 263 */
																				int BgL_tmpz00_3602;

																				BgL_tmpz00_3602 = (int) (2L);
																				BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3602);
																			}
																			{	/* Rgc/rgccompile.scm 263 */
																				obj_t BgL_auxz00_3607;
																				int BgL_tmpz00_3605;

																				BgL_auxz00_3607 = BINT(3L);
																				BgL_tmpz00_3605 = (int) (1L);
																				BGL_MVALUES_VAL_SET(BgL_tmpz00_3605,
																					BgL_auxz00_3607);
																			}
																			BgL_testz00_1514 = BgL_val0_1123z00_1484;
										}}}}}
									{	/* Rgc/rgccompile.scm 273 */
										obj_t BgL_cz00_1515;

										{	/* Rgc/rgccompile.scm 274 */
											obj_t BgL_tmpz00_2537;

											{	/* Rgc/rgccompile.scm 274 */
												int BgL_tmpz00_3610;

												BgL_tmpz00_3610 = (int) (1L);
												BgL_tmpz00_2537 = BGL_MVALUES_VAL(BgL_tmpz00_3610);
											}
											{	/* Rgc/rgccompile.scm 274 */
												int BgL_tmpz00_3613;

												BgL_tmpz00_3613 = (int) (1L);
												BGL_MVALUES_VAL_SET(BgL_tmpz00_3613, BUNSPEC);
											}
											BgL_cz00_1515 = BgL_tmpz00_2537;
										}
										{	/* Rgc/rgccompile.scm 274 */
											obj_t BgL_arg1487z00_1516;
											obj_t BgL_arg1488z00_1517;
											long BgL_arg1489z00_1518;

											{	/* Rgc/rgccompile.scm 229 */
												obj_t BgL_maxz00_2538;

												BgL_maxz00_2538 =
													BGl_rgczd2maxzd2charz00zz__rgc_configz00();
												{
													obj_t BgL_iz00_2540;

													BgL_iz00_2540 = BgL_stopz00_1513;
												BgL_loopz00_2539:
													if (
														((long) CINT(BgL_iz00_2540) ==
															(long) CINT(BgL_maxz00_2538)))
														{	/* Rgc/rgccompile.scm 232 */
															BgL_arg1487z00_1516 = BINT(-1L);
														}
													else
														{	/* Rgc/rgccompile.scm 232 */
															if (BGl_rgcsetzd2memberzf3z21zz__rgc_setz00
																(BgL_setz00_1499, (long) CINT(BgL_iz00_2540)))
																{	/* Rgc/rgccompile.scm 233 */
																	BgL_arg1487z00_1516 = BgL_iz00_2540;
																}
															else
																{	/* Rgc/rgccompile.scm 234 */
																	long BgL_arg1434z00_2543;

																	BgL_arg1434z00_2543 =
																		((long) CINT(BgL_iz00_2540) + 1L);
																	{
																		obj_t BgL_iz00_3627;

																		BgL_iz00_3627 = BINT(BgL_arg1434z00_2543);
																		BgL_iz00_2540 = BgL_iz00_3627;
																		goto BgL_loopz00_2539;
																	}
																}
														}
												}
											}
											BgL_arg1488z00_1517 =
												MAKE_YOUNG_PAIR(BgL_testz00_1514, BgL_testsz00_1505);
											BgL_arg1489z00_1518 =
												((long) CINT(BgL_cz00_1515) + BgL_costz00_1506);
											{
												long BgL_costz00_3634;
												obj_t BgL_testsz00_3633;
												obj_t BgL_startz00_3632;

												BgL_startz00_3632 = BgL_arg1487z00_1516;
												BgL_testsz00_3633 = BgL_arg1488z00_1517;
												BgL_costz00_3634 = BgL_arg1489z00_1518;
												BgL_costz00_1506 = BgL_costz00_3634;
												BgL_testsz00_1505 = BgL_testsz00_3633;
												BgL_startz00_1504 = BgL_startz00_3632;
												goto BgL_zc3z04anonymousza31483ze3z87_1507;
											}
										}
									}
								}
							}
					}
				}
			}
		}

	}



/* state-transition-list */
	obj_t BGl_statezd2transitionzd2listz00zz__rgc_compilez00(obj_t
		BgL_transitionsz00_28)
	{
		{	/* Rgc/rgccompile.scm 297 */
			{
				obj_t BgL_transitionsz00_1526;
				obj_t BgL_resz00_1527;

				BgL_transitionsz00_1526 = BgL_transitionsz00_28;
				BgL_resz00_1527 = BNIL;
			BgL_zc3z04anonymousza31490ze3z87_1528:
				if (NULLP(BgL_transitionsz00_1526))
					{	/* Rgc/rgccompile.scm 300 */
						return BgL_resz00_1527;
					}
				else
					{	/* Rgc/rgccompile.scm 302 */
						obj_t BgL_transitionz00_1530;

						BgL_transitionz00_1530 = CAR(((obj_t) BgL_transitionsz00_1526));
						{	/* Rgc/rgccompile.scm 302 */
							obj_t BgL_charz00_1531;

							BgL_charz00_1531 = CAR(((obj_t) BgL_transitionz00_1530));
							{	/* Rgc/rgccompile.scm 303 */
								obj_t BgL_statez00_1532;

								BgL_statez00_1532 = CDR(((obj_t) BgL_transitionz00_1530));
								{	/* Rgc/rgccompile.scm 304 */

									{	/* Rgc/rgccompile.scm 305 */
										obj_t BgL_cellz00_1533;

										BgL_cellz00_1533 =
											BGl_assqz00zz__r4_pairs_and_lists_6_3z00
											(BgL_statez00_1532, BgL_resz00_1527);
										if (PAIRP(BgL_cellz00_1533))
											{	/* Rgc/rgccompile.scm 306 */
												BGl_rgcsetzd2addz12zc0zz__rgc_setz00(CDR
													(BgL_cellz00_1533), (long) CINT(BgL_charz00_1531));
												{	/* Rgc/rgccompile.scm 312 */
													obj_t BgL_arg1495z00_1536;

													BgL_arg1495z00_1536 =
														CDR(((obj_t) BgL_transitionsz00_1526));
													{
														obj_t BgL_transitionsz00_3652;

														BgL_transitionsz00_3652 = BgL_arg1495z00_1536;
														BgL_transitionsz00_1526 = BgL_transitionsz00_3652;
														goto BgL_zc3z04anonymousza31490ze3z87_1528;
													}
												}
											}
										else
											{	/* Rgc/rgccompile.scm 307 */
												obj_t BgL_setz00_1537;

												{	/* Rgc/rgccompile.scm 307 */
													obj_t BgL_arg1500z00_1541;
													obj_t BgL_arg1501z00_1542;

													{	/* Rgc/rgccompile.scm 307 */
														obj_t BgL_list1502z00_1543;

														BgL_list1502z00_1543 =
															MAKE_YOUNG_PAIR(BgL_charz00_1531, BNIL);
														BgL_arg1500z00_1541 = BgL_list1502z00_1543;
													}
													BgL_arg1501z00_1542 =
														BGl_rgczd2maxzd2charz00zz__rgc_configz00();
													BgL_setz00_1537 =
														BGl_listzd2ze3rgcsetz31zz__rgc_setz00
														(BgL_arg1500z00_1541,
														(long) CINT(BgL_arg1501z00_1542));
												}
												{	/* Rgc/rgccompile.scm 308 */
													obj_t BgL_arg1497z00_1538;
													obj_t BgL_arg1498z00_1539;

													BgL_arg1497z00_1538 =
														CDR(((obj_t) BgL_transitionsz00_1526));
													{	/* Rgc/rgccompile.scm 309 */
														obj_t BgL_arg1499z00_1540;

														BgL_arg1499z00_1540 =
															MAKE_YOUNG_PAIR(BgL_statez00_1532,
															BgL_setz00_1537);
														BgL_arg1498z00_1539 =
															MAKE_YOUNG_PAIR(BgL_arg1499z00_1540,
															BgL_resz00_1527);
													}
													{
														obj_t BgL_resz00_3662;
														obj_t BgL_transitionsz00_3661;

														BgL_transitionsz00_3661 = BgL_arg1497z00_1538;
														BgL_resz00_3662 = BgL_arg1498z00_1539;
														BgL_resz00_1527 = BgL_resz00_3662;
														BgL_transitionsz00_1526 = BgL_transitionsz00_3661;
														goto BgL_zc3z04anonymousza31490ze3z87_1528;
													}
												}
											}
									}
								}
							}
						}
					}
			}
		}

	}



/* insort */
	obj_t BGl_insortz00zz__rgc_compilez00(int BgL_elz00_29, obj_t BgL_lstz00_30)
	{
		{	/* Rgc/rgccompile.scm 319 */
			if (NULLP(BgL_lstz00_30))
				{	/* Rgc/rgccompile.scm 321 */
					obj_t BgL_list1504z00_1546;

					BgL_list1504z00_1546 = MAKE_YOUNG_PAIR(BINT(BgL_elz00_29), BNIL);
					return BgL_list1504z00_1546;
				}
			else
				{	/* Rgc/rgccompile.scm 321 */
					if (
						((long) (BgL_elz00_29) < (long) CINT(CAR(((obj_t) BgL_lstz00_30)))))
						{	/* Rgc/rgccompile.scm 322 */
							return MAKE_YOUNG_PAIR(BINT(BgL_elz00_29), BgL_lstz00_30);
						}
					else
						{	/* Rgc/rgccompile.scm 322 */
							if (
								((long) (BgL_elz00_29) ==
									(long) CINT(CAR(((obj_t) BgL_lstz00_30)))))
								{	/* Rgc/rgccompile.scm 323 */
									return BgL_lstz00_30;
								}
							else
								{	/* Rgc/rgccompile.scm 324 */
									obj_t BgL_arg1509z00_1551;
									obj_t BgL_arg1510z00_1552;

									BgL_arg1509z00_1551 = CAR(((obj_t) BgL_lstz00_30));
									{	/* Rgc/rgccompile.scm 324 */
										obj_t BgL_arg1511z00_1553;

										BgL_arg1511z00_1553 = CDR(((obj_t) BgL_lstz00_30));
										BgL_arg1510z00_1552 =
											BGl_insortz00zz__rgc_compilez00(BgL_elz00_29,
											BgL_arg1511z00_1553);
									}
									return
										MAKE_YOUNG_PAIR(BgL_arg1509z00_1551, BgL_arg1510z00_1552);
								}
						}
				}
		}

	}



/* compile-match */
	obj_t BGl_compilezd2matchzd2zz__rgc_compilez00(obj_t BgL_transitionsz00_31)
	{
		{	/* Rgc/rgccompile.scm 334 */
			{
				obj_t BgL_matchesz00_1572;

				{
					obj_t BgL_transitionsz00_1559;
					obj_t BgL_matchesz00_1560;

					BgL_transitionsz00_1559 = BgL_transitionsz00_31;
					BgL_matchesz00_1560 = BNIL;
				BgL_zc3z04anonymousza31515ze3z87_1561:
					if (NULLP(BgL_transitionsz00_1559))
						{	/* Rgc/rgccompile.scm 360 */
							BgL_matchesz00_1572 = BgL_matchesz00_1560;
							if (NULLP(BgL_matchesz00_1572))
								{	/* Rgc/rgccompile.scm 339 */
									return BFALSE;
								}
							else
								{	/* Rgc/rgccompile.scm 339 */
									return BGl_loopze70ze7zz__rgc_compilez00(BgL_matchesz00_1572);
								}
						}
					else
						{	/* Rgc/rgccompile.scm 362 */
							obj_t BgL_transitionz00_1563;

							BgL_transitionz00_1563 = CAR(((obj_t) BgL_transitionsz00_1559));
							{	/* Rgc/rgccompile.scm 362 */
								obj_t BgL_charz00_1564;

								BgL_charz00_1564 = CAR(((obj_t) BgL_transitionz00_1563));
								{	/* Rgc/rgccompile.scm 364 */

									if (BGl_specialzd2charzd2matchzf3zf3zz__rgc_rulesz00(CINT
											(BgL_charz00_1564)))
										{	/* Rgc/rgccompile.scm 366 */
											obj_t BgL_arg1521z00_1567;
											obj_t BgL_arg1522z00_1568;

											BgL_arg1521z00_1567 =
												CDR(((obj_t) BgL_transitionsz00_1559));
											BgL_arg1522z00_1568 =
												BGl_insortz00zz__rgc_compilez00
												(BGl_specialzd2matchzd2charzd2ze3rulezd2numberze3zz__rgc_rulesz00
												(CINT(BgL_charz00_1564)), BgL_matchesz00_1560);
											{
												obj_t BgL_matchesz00_3705;
												obj_t BgL_transitionsz00_3704;

												BgL_transitionsz00_3704 = BgL_arg1521z00_1567;
												BgL_matchesz00_3705 = BgL_arg1522z00_1568;
												BgL_matchesz00_1560 = BgL_matchesz00_3705;
												BgL_transitionsz00_1559 = BgL_transitionsz00_3704;
												goto BgL_zc3z04anonymousza31515ze3z87_1561;
											}
										}
									else
										{	/* Rgc/rgccompile.scm 368 */
											obj_t BgL_arg1524z00_1570;

											BgL_arg1524z00_1570 =
												CDR(((obj_t) BgL_transitionsz00_1559));
											{
												obj_t BgL_transitionsz00_3708;

												BgL_transitionsz00_3708 = BgL_arg1524z00_1570;
												BgL_transitionsz00_1559 = BgL_transitionsz00_3708;
												goto BgL_zc3z04anonymousza31515ze3z87_1561;
											}
										}
								}
							}
						}
				}
			}
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zz__rgc_compilez00(obj_t BgL_matchesz00_1576)
	{
		{	/* Rgc/rgccompile.scm 342 */
			if (NULLP(BgL_matchesz00_1576))
				{	/* Rgc/rgccompile.scm 343 */
					return BGl_symbol2148z00zz__rgc_compilez00;
				}
			else
				{	/* Rgc/rgccompile.scm 345 */
					obj_t BgL_matchz00_1579;

					BgL_matchz00_1579 = CAR(((obj_t) BgL_matchesz00_1576));
					{	/* Rgc/rgccompile.scm 345 */
						obj_t BgL_predsz00_1580;

						BgL_predsz00_1580 =
							BGl_predicatezd2matchzd2zz__rgc_rulesz00(CINT(BgL_matchz00_1579));
						{	/* Rgc/rgccompile.scm 346 */

							if (PAIRP(BgL_predsz00_1580))
								{	/* Rgc/rgccompile.scm 349 */
									obj_t BgL_arg1530z00_1582;

									{	/* Rgc/rgccompile.scm 349 */
										obj_t BgL_arg1531z00_1583;
										obj_t BgL_arg1535z00_1584;

										{	/* Rgc/rgccompile.scm 349 */
											obj_t BgL_arg1536z00_1585;

											BgL_arg1536z00_1585 =
												BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
												(BgL_predsz00_1580, BNIL);
											BgL_arg1531z00_1583 =
												MAKE_YOUNG_PAIR(BGl_symbol2195z00zz__rgc_compilez00,
												BgL_arg1536z00_1585);
										}
										{	/* Rgc/rgccompile.scm 351 */
											obj_t BgL_arg1537z00_1586;
											obj_t BgL_arg1539z00_1587;

											{	/* Rgc/rgccompile.scm 351 */
												obj_t BgL_arg1540z00_1588;

												{	/* Rgc/rgccompile.scm 351 */
													obj_t BgL_arg1543z00_1589;
													obj_t BgL_arg1544z00_1590;

													{	/* Rgc/rgccompile.scm 351 */
														obj_t BgL_arg1546z00_1591;

														{	/* Rgc/rgccompile.scm 351 */
															obj_t BgL_arg1547z00_1592;

															BgL_arg1547z00_1592 =
																MAKE_YOUNG_PAIR
																(BGl_symbol2146z00zz__rgc_compilez00, BNIL);
															BgL_arg1546z00_1591 =
																MAKE_YOUNG_PAIR
																(BGl_symbol2150z00zz__rgc_compilez00,
																BgL_arg1547z00_1592);
														}
														BgL_arg1543z00_1589 =
															MAKE_YOUNG_PAIR
															(BGl_symbol2197z00zz__rgc_compilez00,
															BgL_arg1546z00_1591);
													}
													BgL_arg1544z00_1590 =
														MAKE_YOUNG_PAIR(BgL_matchz00_1579, BNIL);
													BgL_arg1540z00_1588 =
														MAKE_YOUNG_PAIR(BgL_arg1543z00_1589,
														BgL_arg1544z00_1590);
												}
												BgL_arg1537z00_1586 =
													MAKE_YOUNG_PAIR(BGl_symbol2199z00zz__rgc_compilez00,
													BgL_arg1540z00_1588);
											}
											{	/* Rgc/rgccompile.scm 353 */
												obj_t BgL_arg1549z00_1593;

												{	/* Rgc/rgccompile.scm 353 */
													obj_t BgL_arg1552z00_1594;

													BgL_arg1552z00_1594 =
														CDR(((obj_t) BgL_matchesz00_1576));
													BgL_arg1549z00_1593 =
														BGl_loopze70ze7zz__rgc_compilez00
														(BgL_arg1552z00_1594);
												}
												BgL_arg1539z00_1587 =
													MAKE_YOUNG_PAIR(BgL_arg1549z00_1593, BNIL);
											}
											BgL_arg1535z00_1584 =
												MAKE_YOUNG_PAIR(BgL_arg1537z00_1586,
												BgL_arg1539z00_1587);
										}
										BgL_arg1530z00_1582 =
											MAKE_YOUNG_PAIR(BgL_arg1531z00_1583, BgL_arg1535z00_1584);
									}
									return
										MAKE_YOUNG_PAIR(BGl_symbol2168z00zz__rgc_compilez00,
										BgL_arg1530z00_1582);
								}
							else
								{	/* Rgc/rgccompile.scm 355 */
									obj_t BgL_arg1553z00_1595;

									{	/* Rgc/rgccompile.scm 355 */
										obj_t BgL_arg1554z00_1596;
										obj_t BgL_arg1555z00_1597;

										{	/* Rgc/rgccompile.scm 355 */
											obj_t BgL_arg1556z00_1598;

											{	/* Rgc/rgccompile.scm 355 */
												obj_t BgL_arg1557z00_1599;

												BgL_arg1557z00_1599 =
													MAKE_YOUNG_PAIR(BGl_symbol2146z00zz__rgc_compilez00,
													BNIL);
												BgL_arg1556z00_1598 =
													MAKE_YOUNG_PAIR(BGl_symbol2150z00zz__rgc_compilez00,
													BgL_arg1557z00_1599);
											}
											BgL_arg1554z00_1596 =
												MAKE_YOUNG_PAIR(BGl_symbol2197z00zz__rgc_compilez00,
												BgL_arg1556z00_1598);
										}
										BgL_arg1555z00_1597 =
											MAKE_YOUNG_PAIR(BgL_matchz00_1579, BNIL);
										BgL_arg1553z00_1595 =
											MAKE_YOUNG_PAIR(BgL_arg1554z00_1596, BgL_arg1555z00_1597);
									}
									return
										MAKE_YOUNG_PAIR(BGl_symbol2199z00zz__rgc_compilez00,
										BgL_arg1553z00_1595);
								}
						}
					}
				}
		}

	}



/* init-compile-member-vector! */
	obj_t BGl_initzd2compilezd2memberzd2vectorz12zc0zz__rgc_compilez00(void)
	{
		{	/* Rgc/rgccompile.scm 379 */
			if (VECTORP(BGl_compilezd2memberzd2vectorz00zz__rgc_compilez00))
				{	/* Rgc/rgccompile.scm 380 */
					return BFALSE;
				}
			else
				{	/* Rgc/rgccompile.scm 381 */
					obj_t BgL_arg1559z00_1603;

					{	/* Rgc/rgccompile.scm 381 */
						obj_t BgL_b1132z00_1606;

						BgL_b1132z00_1606 = BGl_rgczd2maxzd2charz00zz__rgc_configz00();
						{	/* Rgc/rgccompile.scm 381 */

							if (INTEGERP(BgL_b1132z00_1606))
								{	/* Rgc/rgccompile.scm 381 */
									BgL_arg1559z00_1603 = ADDFX(BINT(1L), BgL_b1132z00_1606);
								}
							else
								{	/* Rgc/rgccompile.scm 381 */
									BgL_arg1559z00_1603 =
										BGl_2zb2zb2zz__r4_numbers_6_5z00(BINT(1L),
										BgL_b1132z00_1606);
								}
						}
					}
					return (BGl_compilezd2memberzd2vectorz00zz__rgc_compilez00 =
						make_vector((long) CINT(BgL_arg1559z00_1603), BUNSPEC), BUNSPEC);
		}}

	}



/* chars->char-ranges */
	obj_t BGl_charszd2ze3charzd2rangesze3zz__rgc_compilez00(obj_t BgL_charsz00_32)
	{
		{	/* Rgc/rgccompile.scm 396 */
			{	/* Rgc/rgccompile.scm 398 */
				obj_t BgL_maxz00_1608;

				BgL_maxz00_1608 = BGl_rgczd2maxzd2charz00zz__rgc_configz00();
				{
					obj_t BgL_iz00_1644;
					obj_t BgL_iz00_1635;

					{	/* Rgc/rgccompile.scm 420 */
						obj_t BgL_vecz00_1612;

						BgL_vecz00_1612 =
							BGl_compilezd2memberzd2vectorz00zz__rgc_compilez00;
						{	/* Ieee/vector.scm 105 */

							BGl_vectorzd2fillz12zc0zz__r4_vectors_6_8z00(BgL_vecz00_1612,
								BFALSE, 0L, VECTOR_LENGTH(((obj_t) BgL_vecz00_1612)));
					}}
					{
						obj_t BgL_l1133z00_2616;

						BgL_l1133z00_2616 = BgL_charsz00_32;
					BgL_zc3z04anonymousza31563ze3z87_2615:
						if (PAIRP(BgL_l1133z00_2616))
							{	/* Rgc/rgccompile.scm 421 */
								{	/* Rgc/rgccompile.scm 421 */
									obj_t BgL_xz00_2621;

									BgL_xz00_2621 = CAR(BgL_l1133z00_2616);
									VECTOR_SET(BGl_compilezd2memberzd2vectorz00zz__rgc_compilez00,
										(long) CINT(BgL_xz00_2621), BTRUE);
								}
								{
									obj_t BgL_l1133z00_3758;

									BgL_l1133z00_3758 = CDR(BgL_l1133z00_2616);
									BgL_l1133z00_2616 = BgL_l1133z00_3758;
									goto BgL_zc3z04anonymousza31563ze3z87_2615;
								}
							}
						else
							{	/* Rgc/rgccompile.scm 421 */
								((bool_t) 1);
							}
					}
					{
						obj_t BgL_iz00_2636;
						obj_t BgL_rangesz00_2637;

						BgL_iz00_2636 = BINT(0L);
						BgL_rangesz00_2637 = BNIL;
					BgL_loopz00_2635:
						if (((long) CINT(BgL_iz00_2636) >= (long) CINT(BgL_maxz00_1608)))
							{	/* Rgc/rgccompile.scm 424 */
								return bgl_reverse_bang(BgL_rangesz00_2637);
							}
						else
							{	/* Rgc/rgccompile.scm 426 */
								obj_t BgL_rangez00_2641;

								{	/* Rgc/rgccompile.scm 415 */
									obj_t BgL_startz00_2642;

									BgL_iz00_1635 = BgL_iz00_2636;
									{
										obj_t BgL_iz00_1638;

										BgL_iz00_1638 = BgL_iz00_1635;
									BgL_zc3z04anonymousza31577ze3z87_1639:
										if (
											((long) CINT(BgL_iz00_1638) ==
												(long) CINT(BgL_maxz00_1608)))
											{	/* Rgc/rgccompile.scm 403 */
												BgL_startz00_2642 = BFALSE;
											}
										else
											{	/* Rgc/rgccompile.scm 404 */
												bool_t BgL_test2300z00_3769;

												{	/* Rgc/rgccompile.scm 404 */
													obj_t BgL_vectorz00_2588;
													long BgL_kz00_2589;

													BgL_vectorz00_2588 =
														BGl_compilezd2memberzd2vectorz00zz__rgc_compilez00;
													BgL_kz00_2589 = (long) CINT(BgL_iz00_1638);
													BgL_test2300z00_3769 =
														CBOOL(VECTOR_REF(BgL_vectorz00_2588,
															BgL_kz00_2589));
												}
												if (BgL_test2300z00_3769)
													{	/* Rgc/rgccompile.scm 404 */
														BgL_startz00_2642 = BgL_iz00_1638;
													}
												else
													{
														obj_t BgL_iz00_3773;

														BgL_iz00_3773 = ADDFX(BgL_iz00_1638, BINT(1L));
														BgL_iz00_1638 = BgL_iz00_3773;
														goto BgL_zc3z04anonymousza31577ze3z87_1639;
													}
											}
									}
									if (CBOOL(BgL_startz00_2642))
										{	/* Rgc/rgccompile.scm 418 */
											long BgL_arg1587z00_2643;

											{	/* Rgc/rgccompile.scm 418 */
												long BgL_tmpz00_3778;

												{	/* Rgc/rgccompile.scm 418 */
													obj_t BgL_tmpz00_3779;

													BgL_iz00_1644 = BgL_startz00_2642;
													{
														obj_t BgL_iz00_1647;

														BgL_iz00_1647 = BgL_iz00_1644;
													BgL_zc3z04anonymousza31582ze3z87_1648:
														if (
															((long) CINT(BgL_iz00_1647) ==
																(long) CINT(BgL_maxz00_1608)))
															{	/* Rgc/rgccompile.scm 410 */
																BgL_tmpz00_3779 = BgL_maxz00_1608;
															}
														else
															{	/* Rgc/rgccompile.scm 411 */
																bool_t BgL_test2303z00_3784;

																{	/* Rgc/rgccompile.scm 411 */
																	obj_t BgL_vectorz00_2593;
																	long BgL_kz00_2594;

																	BgL_vectorz00_2593 =
																		BGl_compilezd2memberzd2vectorz00zz__rgc_compilez00;
																	BgL_kz00_2594 = (long) CINT(BgL_iz00_1647);
																	BgL_test2303z00_3784 =
																		CBOOL(VECTOR_REF(BgL_vectorz00_2593,
																			BgL_kz00_2594));
																}
																if (BgL_test2303z00_3784)
																	{
																		obj_t BgL_iz00_3788;

																		BgL_iz00_3788 =
																			ADDFX(BgL_iz00_1647, BINT(1L));
																		BgL_iz00_1647 = BgL_iz00_3788;
																		goto BgL_zc3z04anonymousza31582ze3z87_1648;
																	}
																else
																	{	/* Rgc/rgccompile.scm 411 */
																		BgL_tmpz00_3779 = BgL_iz00_1647;
																	}
															}
													}
													BgL_tmpz00_3778 = (long) CINT(BgL_tmpz00_3779);
												}
												BgL_arg1587z00_2643 = (BgL_tmpz00_3778 - 1L);
											}
											BgL_rangez00_2641 =
												MAKE_YOUNG_PAIR(BgL_startz00_2642,
												BINT(BgL_arg1587z00_2643));
										}
									else
										{	/* Rgc/rgccompile.scm 416 */
											BgL_rangez00_2641 = BgL_maxz00_1608;
										}
								}
								if (PAIRP(BgL_rangez00_2641))
									{	/* Rgc/rgccompile.scm 428 */
										long BgL_arg1571z00_2647;
										obj_t BgL_arg1573z00_2648;

										BgL_arg1571z00_2647 =
											((long) CINT(CDR(BgL_rangez00_2641)) + 1L);
										BgL_arg1573z00_2648 =
											MAKE_YOUNG_PAIR(BgL_rangez00_2641, BgL_rangesz00_2637);
										{
											obj_t BgL_rangesz00_3803;
											obj_t BgL_iz00_3801;

											BgL_iz00_3801 = BINT(BgL_arg1571z00_2647);
											BgL_rangesz00_3803 = BgL_arg1573z00_2648;
											BgL_rangesz00_2637 = BgL_rangesz00_3803;
											BgL_iz00_2636 = BgL_iz00_3801;
											goto BgL_loopz00_2635;
										}
									}
								else
									{
										obj_t BgL_iz00_3804;

										BgL_iz00_3804 = BgL_rangez00_2641;
										BgL_iz00_2636 = BgL_iz00_3804;
										goto BgL_loopz00_2635;
									}
							}
					}
				}
			}
		}

	}



/* char-ranges->test */
	obj_t BGl_charzd2rangeszd2ze3testze3zz__rgc_compilez00(obj_t
		BgL_currentz00_33, obj_t BgL_rangesz00_34)
	{
		{	/* Rgc/rgccompile.scm 434 */
			{	/* Rgc/rgccompile.scm 443 */
				obj_t BgL_arg1591z00_1662;

				{	/* Rgc/rgccompile.scm 443 */
					obj_t BgL_arg1593z00_1663;

					if (NULLP(BgL_rangesz00_34))
						{	/* Rgc/rgccompile.scm 443 */
							BgL_arg1593z00_1663 = BNIL;
						}
					else
						{	/* Rgc/rgccompile.scm 443 */
							obj_t BgL_head1137z00_1666;

							{	/* Rgc/rgccompile.scm 443 */
								obj_t BgL_arg1603z00_1678;

								{	/* Rgc/rgccompile.scm 443 */
									obj_t BgL_arg1605z00_1679;

									BgL_arg1605z00_1679 = CAR(((obj_t) BgL_rangesz00_34));
									BgL_arg1603z00_1678 =
										BGl_charzd2rangezd2ze3testze70z04zz__rgc_compilez00
										(BgL_currentz00_33, BgL_arg1605z00_1679);
								}
								BgL_head1137z00_1666 =
									MAKE_YOUNG_PAIR(BgL_arg1603z00_1678, BNIL);
							}
							{	/* Rgc/rgccompile.scm 443 */
								obj_t BgL_g1140z00_1667;

								BgL_g1140z00_1667 = CDR(((obj_t) BgL_rangesz00_34));
								{
									obj_t BgL_l1135z00_2675;
									obj_t BgL_tail1138z00_2676;

									BgL_l1135z00_2675 = BgL_g1140z00_1667;
									BgL_tail1138z00_2676 = BgL_head1137z00_1666;
								BgL_zc3z04anonymousza31595ze3z87_2674:
									if (NULLP(BgL_l1135z00_2675))
										{	/* Rgc/rgccompile.scm 443 */
											BgL_arg1593z00_1663 = BgL_head1137z00_1666;
										}
									else
										{	/* Rgc/rgccompile.scm 443 */
											obj_t BgL_newtail1139z00_2683;

											{	/* Rgc/rgccompile.scm 443 */
												obj_t BgL_arg1601z00_2684;

												{	/* Rgc/rgccompile.scm 443 */
													obj_t BgL_arg1602z00_2685;

													BgL_arg1602z00_2685 =
														CAR(((obj_t) BgL_l1135z00_2675));
													BgL_arg1601z00_2684 =
														BGl_charzd2rangezd2ze3testze70z04zz__rgc_compilez00
														(BgL_currentz00_33, BgL_arg1602z00_2685);
												}
												BgL_newtail1139z00_2683 =
													MAKE_YOUNG_PAIR(BgL_arg1601z00_2684, BNIL);
											}
											SET_CDR(BgL_tail1138z00_2676, BgL_newtail1139z00_2683);
											{	/* Rgc/rgccompile.scm 443 */
												obj_t BgL_arg1598z00_2686;

												BgL_arg1598z00_2686 = CDR(((obj_t) BgL_l1135z00_2675));
												{
													obj_t BgL_tail1138z00_3824;
													obj_t BgL_l1135z00_3823;

													BgL_l1135z00_3823 = BgL_arg1598z00_2686;
													BgL_tail1138z00_3824 = BgL_newtail1139z00_2683;
													BgL_tail1138z00_2676 = BgL_tail1138z00_3824;
													BgL_l1135z00_2675 = BgL_l1135z00_3823;
													goto BgL_zc3z04anonymousza31595ze3z87_2674;
												}
											}
										}
								}
							}
						}
					BgL_arg1591z00_1662 =
						BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(BgL_arg1593z00_1663,
						BNIL);
				}
				return
					MAKE_YOUNG_PAIR(BGl_symbol2189z00zz__rgc_compilez00,
					BgL_arg1591z00_1662);
			}
		}

	}



/* char-range->test~0 */
	obj_t BGl_charzd2rangezd2ze3testze70z04zz__rgc_compilez00(obj_t
		BgL_currentz00_3025, obj_t BgL_rangez00_1680)
	{
		{	/* Rgc/rgccompile.scm 441 */
			{	/* Rgc/rgccompile.scm 437 */
				obj_t BgL_startz00_1682;
				obj_t BgL_stopz00_1683;

				BgL_startz00_1682 = CAR(((obj_t) BgL_rangez00_1680));
				BgL_stopz00_1683 = CDR(((obj_t) BgL_rangez00_1680));
				if (((long) CINT(BgL_startz00_1682) == (long) CINT(BgL_stopz00_1683)))
					{	/* Rgc/rgccompile.scm 440 */
						obj_t BgL_arg1608z00_1685;

						{	/* Rgc/rgccompile.scm 440 */
							obj_t BgL_arg1609z00_1686;

							BgL_arg1609z00_1686 = MAKE_YOUNG_PAIR(BgL_startz00_1682, BNIL);
							BgL_arg1608z00_1685 =
								MAKE_YOUNG_PAIR(BgL_currentz00_3025, BgL_arg1609z00_1686);
						}
						return
							MAKE_YOUNG_PAIR(BGl_symbol2158z00zz__rgc_compilez00,
							BgL_arg1608z00_1685);
					}
				else
					{	/* Rgc/rgccompile.scm 441 */
						obj_t BgL_arg1610z00_1687;

						{	/* Rgc/rgccompile.scm 441 */
							obj_t BgL_arg1611z00_1688;
							obj_t BgL_arg1612z00_1689;

							{	/* Rgc/rgccompile.scm 441 */
								obj_t BgL_arg1613z00_1690;

								{	/* Rgc/rgccompile.scm 441 */
									obj_t BgL_arg1615z00_1691;

									BgL_arg1615z00_1691 =
										MAKE_YOUNG_PAIR(BgL_startz00_1682, BNIL);
									BgL_arg1613z00_1690 =
										MAKE_YOUNG_PAIR(BgL_currentz00_3025, BgL_arg1615z00_1691);
								}
								BgL_arg1611z00_1688 =
									MAKE_YOUNG_PAIR(BGl_symbol2191z00zz__rgc_compilez00,
									BgL_arg1613z00_1690);
							}
							{	/* Rgc/rgccompile.scm 441 */
								obj_t BgL_arg1616z00_1692;

								{	/* Rgc/rgccompile.scm 441 */
									obj_t BgL_arg1617z00_1693;

									{	/* Rgc/rgccompile.scm 441 */
										obj_t BgL_arg1618z00_1694;

										BgL_arg1618z00_1694 =
											MAKE_YOUNG_PAIR(BgL_stopz00_1683, BNIL);
										BgL_arg1617z00_1693 =
											MAKE_YOUNG_PAIR(BgL_currentz00_3025, BgL_arg1618z00_1694);
									}
									BgL_arg1616z00_1692 =
										MAKE_YOUNG_PAIR(BGl_symbol2201z00zz__rgc_compilez00,
										BgL_arg1617z00_1693);
								}
								BgL_arg1612z00_1689 =
									MAKE_YOUNG_PAIR(BgL_arg1616z00_1692, BNIL);
							}
							BgL_arg1610z00_1687 =
								MAKE_YOUNG_PAIR(BgL_arg1611z00_1688, BgL_arg1612z00_1689);
						}
						return
							MAKE_YOUNG_PAIR(BGl_symbol2195z00zz__rgc_compilez00,
							BgL_arg1610z00_1687);
					}
			}
		}

	}



/* compile-member-test */
	obj_t BGl_compilezd2memberzd2testz00zz__rgc_compilez00(obj_t
		BgL_currentz00_35, obj_t BgL_charsz00_36)
	{
		{	/* Rgc/rgccompile.scm 450 */
			{

				if (PAIRP(BgL_charsz00_36))
					{	/* Rgc/rgccompile.scm 451 */
						if (NULLP(CDR(((obj_t) BgL_charsz00_36))))
							{	/* Rgc/rgccompile.scm 451 */
								obj_t BgL_arg1622z00_1703;

								BgL_arg1622z00_1703 = CAR(((obj_t) BgL_charsz00_36));
								{	/* Rgc/rgccompile.scm 453 */
									obj_t BgL_arg1624z00_2697;

									{	/* Rgc/rgccompile.scm 453 */
										obj_t BgL_arg1625z00_2698;

										BgL_arg1625z00_2698 =
											MAKE_YOUNG_PAIR(BgL_arg1622z00_1703, BNIL);
										BgL_arg1624z00_2697 =
											MAKE_YOUNG_PAIR(BgL_currentz00_35, BgL_arg1625z00_2698);
									}
									return
										MAKE_YOUNG_PAIR(BGl_symbol2158z00zz__rgc_compilez00,
										BgL_arg1624z00_2697);
								}
							}
						else
							{	/* Rgc/rgccompile.scm 451 */
							BgL_tagzd2102zd2_1698:
								{	/* Rgc/rgccompile.scm 455 */
									obj_t BgL_rangesz00_1707;

									BgL_rangesz00_1707 =
										BGl_charszd2ze3charzd2rangesze3zz__rgc_compilez00
										(BgL_charsz00_36);
									if ((bgl_list_length(BgL_rangesz00_1707) >
											(bgl_list_length(BgL_charsz00_36) / 3L)))
										{	/* Rgc/rgccompile.scm 457 */
											obj_t BgL_arg1630z00_1712;

											{	/* Rgc/rgccompile.scm 457 */
												obj_t BgL_arg1631z00_1713;

												{	/* Rgc/rgccompile.scm 457 */
													obj_t BgL_arg1634z00_1714;

													{	/* Rgc/rgccompile.scm 457 */
														obj_t BgL_arg1636z00_1715;

														BgL_arg1636z00_1715 =
															MAKE_YOUNG_PAIR(BgL_charsz00_36, BNIL);
														BgL_arg1634z00_1714 =
															MAKE_YOUNG_PAIR
															(BGl_symbol2203z00zz__rgc_compilez00,
															BgL_arg1636z00_1715);
													}
													BgL_arg1631z00_1713 =
														MAKE_YOUNG_PAIR(BgL_arg1634z00_1714, BNIL);
												}
												BgL_arg1630z00_1712 =
													MAKE_YOUNG_PAIR(BgL_currentz00_35,
													BgL_arg1631z00_1713);
											}
											return
												MAKE_YOUNG_PAIR(BGl_symbol2205z00zz__rgc_compilez00,
												BgL_arg1630z00_1712);
										}
									else
										{	/* Rgc/rgccompile.scm 456 */
											return
												BGl_charzd2rangeszd2ze3testze3zz__rgc_compilez00
												(BgL_currentz00_35, BgL_rangesz00_1707);
										}
								}
							}
					}
				else
					{	/* Rgc/rgccompile.scm 451 */
						goto BgL_tagzd2102zd2_1698;
					}
			}
		}

	}



/* compile-submatches */
	obj_t BGl_compilezd2submatcheszd2zz__rgc_compilez00(obj_t BgL_currentz00_37,
		obj_t BgL_submatchesz00_38, obj_t BgL_positionsz00_39,
		obj_t BgL_positionszd2tozd2charz00_40)
	{
		{	/* Rgc/rgccompile.scm 463 */
			{
				obj_t BgL_cellz00_1906;
				obj_t BgL_smz00_1907;

				{	/* Rgc/rgccompile.scm 479 */
					obj_t BgL_g1049z00_1721;

					BgL_g1049z00_1721 =
						BGl_rgcsetzd2ze3listz31zz__rgc_setz00(BgL_positionsz00_39);
					{
						obj_t BgL_positionsz00_1724;
						obj_t BgL_charzd2submatcheszd2_1725;

						BgL_positionsz00_1724 = BgL_g1049z00_1721;
						BgL_charzd2submatcheszd2_1725 = BNIL;
					BgL_zc3z04anonymousza31640ze3z87_1726:
						if (NULLP(BgL_positionsz00_1724))
							{	/* Rgc/rgccompile.scm 482 */
								obj_t BgL_fun1146z00_1728;

								{	/* Rgc/rgccompile.scm 482 */
									obj_t BgL_zc3z04anonymousza31649ze3z87_3021;

									{
										int BgL_tmpz00_3873;

										BgL_tmpz00_3873 = (int) (1L);
										BgL_zc3z04anonymousza31649ze3z87_3021 =
											MAKE_EL_PROCEDURE(BgL_tmpz00_3873);
									}
									PROCEDURE_EL_SET(BgL_zc3z04anonymousza31649ze3z87_3021,
										(int) (0L), BgL_currentz00_37);
									BgL_fun1146z00_1728 = BgL_zc3z04anonymousza31649ze3z87_3021;
								}
								if (NULLP(BgL_charzd2submatcheszd2_1725))
									{	/* Rgc/rgccompile.scm 482 */
										return BNIL;
									}
								else
									{	/* Rgc/rgccompile.scm 482 */
										obj_t BgL_head1143z00_1731;

										BgL_head1143z00_1731 = MAKE_YOUNG_PAIR(BNIL, BNIL);
										{
											obj_t BgL_l1141z00_2772;
											obj_t BgL_tail1144z00_2773;

											BgL_l1141z00_2772 = BgL_charzd2submatcheszd2_1725;
											BgL_tail1144z00_2773 = BgL_head1143z00_1731;
										BgL_zc3z04anonymousza31643ze3z87_2771:
											if (NULLP(BgL_l1141z00_2772))
												{	/* Rgc/rgccompile.scm 482 */
													return CDR(BgL_head1143z00_1731);
												}
											else
												{	/* Rgc/rgccompile.scm 482 */
													obj_t BgL_newtail1145z00_2780;

													{	/* Rgc/rgccompile.scm 482 */
														obj_t BgL_arg1646z00_2781;

														{	/* Rgc/rgccompile.scm 482 */
															obj_t BgL_arg1648z00_2782;

															BgL_arg1648z00_2782 =
																CAR(((obj_t) BgL_l1141z00_2772));
															BgL_arg1646z00_2781 =
																BGl_z62zc3z04anonymousza31649ze3ze5zz__rgc_compilez00
																(BgL_fun1146z00_1728, BgL_arg1648z00_2782);
														}
														BgL_newtail1145z00_2780 =
															MAKE_YOUNG_PAIR(BgL_arg1646z00_2781, BNIL);
													}
													SET_CDR(BgL_tail1144z00_2773,
														BgL_newtail1145z00_2780);
													{	/* Rgc/rgccompile.scm 482 */
														obj_t BgL_arg1645z00_2783;

														BgL_arg1645z00_2783 =
															CDR(((obj_t) BgL_l1141z00_2772));
														{
															obj_t BgL_tail1144z00_3895;
															obj_t BgL_l1141z00_3894;

															BgL_l1141z00_3894 = BgL_arg1645z00_2783;
															BgL_tail1144z00_3895 = BgL_newtail1145z00_2780;
															BgL_tail1144z00_2773 = BgL_tail1144z00_3895;
															BgL_l1141z00_2772 = BgL_l1141z00_3894;
															goto BgL_zc3z04anonymousza31643ze3z87_2771;
														}
													}
												}
										}
									}
							}
						else
							{	/* Rgc/rgccompile.scm 507 */
								obj_t BgL_posz00_1892;

								BgL_posz00_1892 = CAR(((obj_t) BgL_positionsz00_1724));
								{	/* Rgc/rgccompile.scm 507 */
									obj_t BgL_charz00_1893;

									{	/* Rgc/rgccompile.scm 508 */
										long BgL_kz00_2790;

										BgL_kz00_2790 = (long) CINT(BgL_posz00_1892);
										BgL_charz00_1893 =
											VECTOR_REF(
											((obj_t) BgL_positionszd2tozd2charz00_40), BgL_kz00_2790);
									}
									{	/* Rgc/rgccompile.scm 508 */
										obj_t BgL_cellz00_1894;

										{	/* Rgc/rgccompile.scm 509 */
											long BgL_kz00_2792;

											BgL_kz00_2792 = (long) CINT(BgL_posz00_1892);
											BgL_cellz00_1894 =
												VECTOR_REF(
												((obj_t) BgL_submatchesz00_38), BgL_kz00_2792);
										}
										{	/* Rgc/rgccompile.scm 509 */

											if (NULLP(BgL_cellz00_1894))
												{	/* Rgc/rgccompile.scm 511 */
													obj_t BgL_arg1789z00_1896;

													BgL_arg1789z00_1896 =
														CDR(((obj_t) BgL_positionsz00_1724));
													{
														obj_t BgL_positionsz00_3908;

														BgL_positionsz00_3908 = BgL_arg1789z00_1896;
														BgL_positionsz00_1724 = BgL_positionsz00_3908;
														goto BgL_zc3z04anonymousza31640ze3z87_1726;
													}
												}
											else
												{	/* Rgc/rgccompile.scm 513 */
													obj_t BgL_oldz00_1897;

													BgL_cellz00_1906 = BgL_cellz00_1894;
													BgL_smz00_1907 = BgL_charzd2submatcheszd2_1725;
													{
														obj_t BgL_smz00_1910;

														BgL_smz00_1910 = BgL_smz00_1907;
													BgL_zc3z04anonymousza31798ze3z87_1911:
														if (NULLP(BgL_smz00_1910))
															{	/* Rgc/rgccompile.scm 468 */
																BgL_oldz00_1897 = BNIL;
															}
														else
															{	/* Rgc/rgccompile.scm 470 */
																bool_t BgL_test2316z00_3911;

																{	/* Rgc/rgccompile.scm 470 */
																	obj_t BgL_auxz00_3912;

																	{	/* Rgc/rgccompile.scm 470 */
																		obj_t BgL_pairz00_2700;

																		BgL_pairz00_2700 =
																			CAR(((obj_t) BgL_smz00_1910));
																		BgL_auxz00_3912 = CDR(BgL_pairz00_2700);
																	}
																	BgL_test2316z00_3911 =
																		BGl_equalzf3zf3zz__r4_equivalence_6_2z00
																		(BgL_cellz00_1906, BgL_auxz00_3912);
																}
																if (BgL_test2316z00_3911)
																	{	/* Rgc/rgccompile.scm 470 */
																		BgL_oldz00_1897 =
																			CAR(((obj_t) BgL_smz00_1910));
																	}
																else
																	{
																		obj_t BgL_smz00_3919;

																		BgL_smz00_3919 =
																			CDR(((obj_t) BgL_smz00_1910));
																		BgL_smz00_1910 = BgL_smz00_3919;
																		goto BgL_zc3z04anonymousza31798ze3z87_1911;
																	}
															}
													}
													if (PAIRP(BgL_oldz00_1897))
														{	/* Rgc/rgccompile.scm 514 */
															{	/* Rgc/rgccompile.scm 476 */
																obj_t BgL_arg1807z00_2794;

																BgL_arg1807z00_2794 =
																	MAKE_YOUNG_PAIR(BgL_charz00_1893,
																	CAR(BgL_oldz00_1897));
																SET_CAR(BgL_oldz00_1897, BgL_arg1807z00_2794);
															}
															BgL_oldz00_1897;
															{	/* Rgc/rgccompile.scm 517 */
																obj_t BgL_arg1791z00_1899;

																BgL_arg1791z00_1899 =
																	CDR(((obj_t) BgL_positionsz00_1724));
																{
																	obj_t BgL_positionsz00_3929;

																	BgL_positionsz00_3929 = BgL_arg1791z00_1899;
																	BgL_positionsz00_1724 = BgL_positionsz00_3929;
																	goto BgL_zc3z04anonymousza31640ze3z87_1726;
																}
															}
														}
													else
														{	/* Rgc/rgccompile.scm 519 */
															obj_t BgL_arg1792z00_1900;
															obj_t BgL_arg1793z00_1901;

															BgL_arg1792z00_1900 =
																CDR(((obj_t) BgL_positionsz00_1724));
															{	/* Rgc/rgccompile.scm 520 */
																obj_t BgL_arg1794z00_1902;

																{	/* Rgc/rgccompile.scm 520 */
																	obj_t BgL_arg1795z00_1903;

																	{	/* Rgc/rgccompile.scm 520 */
																		obj_t BgL_list1796z00_1904;

																		BgL_list1796z00_1904 =
																			MAKE_YOUNG_PAIR(BgL_charz00_1893, BNIL);
																		BgL_arg1795z00_1903 = BgL_list1796z00_1904;
																	}
																	BgL_arg1794z00_1902 =
																		MAKE_YOUNG_PAIR(BgL_arg1795z00_1903,
																		BgL_cellz00_1894);
																}
																BgL_arg1793z00_1901 =
																	MAKE_YOUNG_PAIR(BgL_arg1794z00_1902,
																	BgL_charzd2submatcheszd2_1725);
															}
															{
																obj_t BgL_charzd2submatcheszd2_3936;
																obj_t BgL_positionsz00_3935;

																BgL_positionsz00_3935 = BgL_arg1792z00_1900;
																BgL_charzd2submatcheszd2_3936 =
																	BgL_arg1793z00_1901;
																BgL_charzd2submatcheszd2_1725 =
																	BgL_charzd2submatcheszd2_3936;
																BgL_positionsz00_1724 = BgL_positionsz00_3935;
																goto BgL_zc3z04anonymousza31640ze3z87_1726;
															}
														}
												}
										}
									}
								}
							}
					}
				}
			}
		}

	}



/* &<@anonymous:1649> */
	obj_t BGl_z62zc3z04anonymousza31649ze3ze5zz__rgc_compilez00(obj_t
		BgL_envz00_3022, obj_t BgL_ezd2113zd2_3024)
	{
		{	/* Rgc/rgccompile.scm 482 */
			{	/* Rgc/rgccompile.scm 482 */
				obj_t BgL_currentz00_3023;

				BgL_currentz00_3023 =
					((obj_t) PROCEDURE_EL_REF(BgL_envz00_3022, (int) (0L)));
				{
					obj_t BgL_charz00_3117;
					obj_t BgL_mz00_3118;
					obj_t BgL_smz00_3119;
					obj_t BgL_charz00_3097;
					obj_t BgL_sz00_3098;
					obj_t BgL_mz00_3099;
					obj_t BgL_smz00_3100;
					obj_t BgL_charz00_3065;
					obj_t BgL_sz00_3066;
					obj_t BgL_mz00_3067;
					obj_t BgL_smz00_3068;
					obj_t BgL_tsz00_3069;
					obj_t BgL_tsmz00_3070;

					if (PAIRP(BgL_ezd2113zd2_3024))
						{	/* Rgc/rgccompile.scm 482 */
							obj_t BgL_cdrzd2125zd2_3130;

							BgL_cdrzd2125zd2_3130 = CDR(BgL_ezd2113zd2_3024);
							if (PAIRP(BgL_cdrzd2125zd2_3130))
								{	/* Rgc/rgccompile.scm 482 */
									obj_t BgL_cdrzd2130zd2_3131;

									BgL_cdrzd2130zd2_3131 = CDR(BgL_cdrzd2125zd2_3130);
									if (NULLP(CAR(BgL_cdrzd2125zd2_3130)))
										{	/* Rgc/rgccompile.scm 482 */
											if (PAIRP(BgL_cdrzd2130zd2_3131))
												{	/* Rgc/rgccompile.scm 482 */
													obj_t BgL_carzd2133zd2_3132;

													BgL_carzd2133zd2_3132 = CAR(BgL_cdrzd2130zd2_3131);
													if (PAIRP(BgL_carzd2133zd2_3132))
														{	/* Rgc/rgccompile.scm 482 */
															if (NULLP(CDR(BgL_cdrzd2130zd2_3131)))
																{	/* Rgc/rgccompile.scm 482 */
																	BgL_charz00_3117 = CAR(BgL_ezd2113zd2_3024);
																	BgL_mz00_3118 = CAR(BgL_carzd2133zd2_3132);
																	BgL_smz00_3119 = CDR(BgL_carzd2133zd2_3132);
																	{	/* Rgc/rgccompile.scm 484 */
																		obj_t BgL_arg1730z00_3120;

																		{	/* Rgc/rgccompile.scm 484 */
																			obj_t BgL_arg1731z00_3121;
																			obj_t BgL_arg1733z00_3122;

																			BgL_arg1731z00_3121 =
																				BGl_compilezd2memberzd2testz00zz__rgc_compilez00
																				(BgL_currentz00_3023, BgL_charz00_3117);
																			{	/* Rgc/rgccompile.scm 485 */
																				obj_t BgL_arg1734z00_3123;

																				{	/* Rgc/rgccompile.scm 485 */
																					obj_t BgL_arg1735z00_3124;

																					{	/* Rgc/rgccompile.scm 485 */
																						obj_t BgL_arg1736z00_3125;

																						{	/* Rgc/rgccompile.scm 485 */
																							obj_t BgL_arg1737z00_3126;

																							{	/* Rgc/rgccompile.scm 485 */
																								obj_t BgL_arg1738z00_3127;

																								{	/* Rgc/rgccompile.scm 485 */
																									obj_t BgL_arg1739z00_3128;

																									{	/* Rgc/rgccompile.scm 485 */
																										obj_t BgL_arg1740z00_3129;

																										BgL_arg1740z00_3129 =
																											MAKE_YOUNG_PAIR
																											(BGl_symbol2146z00zz__rgc_compilez00,
																											BNIL);
																										BgL_arg1739z00_3128 =
																											MAKE_YOUNG_PAIR(BINT(1L),
																											BgL_arg1740z00_3129);
																									}
																									BgL_arg1738z00_3127 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2183z00zz__rgc_compilez00,
																										BgL_arg1739z00_3128);
																								}
																								BgL_arg1737z00_3126 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1738z00_3127, BNIL);
																							}
																							BgL_arg1736z00_3125 =
																								MAKE_YOUNG_PAIR(BgL_smz00_3119,
																								BgL_arg1737z00_3126);
																						}
																						BgL_arg1735z00_3124 =
																							MAKE_YOUNG_PAIR(BgL_mz00_3118,
																							BgL_arg1736z00_3125);
																					}
																					BgL_arg1734z00_3123 =
																						MAKE_YOUNG_PAIR
																						(BGl_symbol2211z00zz__rgc_compilez00,
																						BgL_arg1735z00_3124);
																				}
																				BgL_arg1733z00_3122 =
																					MAKE_YOUNG_PAIR(BgL_arg1734z00_3123,
																					BNIL);
																			}
																			BgL_arg1730z00_3120 =
																				MAKE_YOUNG_PAIR(BgL_arg1731z00_3121,
																				BgL_arg1733z00_3122);
																		}
																		return
																			MAKE_YOUNG_PAIR
																			(BGl_symbol2168z00zz__rgc_compilez00,
																			BgL_arg1730z00_3120);
																	}
																}
															else
																{	/* Rgc/rgccompile.scm 482 */
																	return
																		BGl_errorz00zz__errorz00
																		(BGl_string2213z00zz__rgc_compilez00,
																		BGl_string2214z00zz__rgc_compilez00,
																		BgL_ezd2113zd2_3024);
																}
														}
													else
														{	/* Rgc/rgccompile.scm 482 */
															return
																BGl_errorz00zz__errorz00
																(BGl_string2213z00zz__rgc_compilez00,
																BGl_string2214z00zz__rgc_compilez00,
																BgL_ezd2113zd2_3024);
														}
												}
											else
												{	/* Rgc/rgccompile.scm 482 */
													obj_t BgL_carzd2218zd2_3133;

													BgL_carzd2218zd2_3133 =
														CAR(((obj_t) BgL_cdrzd2125zd2_3130));
													if (PAIRP(BgL_carzd2218zd2_3133))
														{	/* Rgc/rgccompile.scm 482 */
															obj_t BgL_carzd2223zd2_3134;

															BgL_carzd2223zd2_3134 =
																CAR(BgL_carzd2218zd2_3133);
															if (PAIRP(BgL_carzd2223zd2_3134))
																{	/* Rgc/rgccompile.scm 482 */
																	obj_t BgL_cdrzd2229zd2_3135;

																	BgL_cdrzd2229zd2_3135 =
																		CDR(BgL_carzd2223zd2_3134);
																	if (PAIRP(BgL_cdrzd2229zd2_3135))
																		{	/* Rgc/rgccompile.scm 482 */
																			obj_t BgL_cdrzd2234zd2_3136;

																			BgL_cdrzd2234zd2_3136 =
																				CDR(BgL_cdrzd2229zd2_3135);
																			if (PAIRP(BgL_cdrzd2234zd2_3136))
																				{	/* Rgc/rgccompile.scm 482 */
																					if (NULLP(CDR(BgL_cdrzd2234zd2_3136)))
																						{	/* Rgc/rgccompile.scm 482 */
																							if (NULLP(CDR
																									(BgL_carzd2218zd2_3133)))
																								{	/* Rgc/rgccompile.scm 482 */
																									if (NULLP(CDR(
																												((obj_t)
																													BgL_cdrzd2125zd2_3130))))
																										{	/* Rgc/rgccompile.scm 482 */
																											BgL_charz00_3097 =
																												CAR
																												(BgL_ezd2113zd2_3024);
																											BgL_sz00_3098 =
																												CAR
																												(BgL_carzd2223zd2_3134);
																											BgL_mz00_3099 =
																												CAR
																												(BgL_cdrzd2229zd2_3135);
																											BgL_smz00_3100 =
																												CAR
																												(BgL_cdrzd2234zd2_3136);
																										BgL_tagzd2110zd2_3063:
																											{	/* Rgc/rgccompile.scm 487 */
																												obj_t
																													BgL_arg1741z00_3101;
																												{	/* Rgc/rgccompile.scm 487 */
																													obj_t
																														BgL_arg1743z00_3102;
																													obj_t
																														BgL_arg1744z00_3103;
																													BgL_arg1743z00_3102 =
																														BGl_compilezd2memberzd2testz00zz__rgc_compilez00
																														(BgL_currentz00_3023,
																														BgL_charz00_3097);
																													{	/* Rgc/rgccompile.scm 488 */
																														obj_t
																															BgL_arg1745z00_3104;
																														if (CBOOL
																															(BgL_sz00_3098))
																															{	/* Rgc/rgccompile.scm 491 */
																																obj_t
																																	BgL_arg1746z00_3105;
																																{	/* Rgc/rgccompile.scm 491 */
																																	obj_t
																																		BgL_arg1747z00_3106;
																																	{	/* Rgc/rgccompile.scm 491 */
																																		obj_t
																																			BgL_arg1748z00_3107;
																																		{	/* Rgc/rgccompile.scm 491 */
																																			obj_t
																																				BgL_arg1749z00_3108;
																																			{	/* Rgc/rgccompile.scm 491 */
																																				obj_t
																																					BgL_arg1750z00_3109;
																																				{	/* Rgc/rgccompile.scm 491 */
																																					obj_t
																																						BgL_arg1751z00_3110;
																																					BgL_arg1751z00_3110
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_symbol2146z00zz__rgc_compilez00,
																																						BNIL);
																																					BgL_arg1750z00_3109
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BINT
																																						(1L),
																																						BgL_arg1751z00_3110);
																																				}
																																				BgL_arg1749z00_3108
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BGl_symbol2183z00zz__rgc_compilez00,
																																					BgL_arg1750z00_3109);
																																			}
																																			BgL_arg1748z00_3107
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1749z00_3108,
																																				BNIL);
																																		}
																																		BgL_arg1747z00_3106
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_smz00_3100,
																																			BgL_arg1748z00_3107);
																																	}
																																	BgL_arg1746z00_3105
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_mz00_3099,
																																		BgL_arg1747z00_3106);
																																}
																																BgL_arg1745z00_3104
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BGl_symbol2207z00zz__rgc_compilez00,
																																	BgL_arg1746z00_3105);
																															}
																														else
																															{	/* Rgc/rgccompile.scm 492 */
																																obj_t
																																	BgL_arg1752z00_3111;
																																{	/* Rgc/rgccompile.scm 492 */
																																	obj_t
																																		BgL_arg1753z00_3112;
																																	{	/* Rgc/rgccompile.scm 492 */
																																		obj_t
																																			BgL_arg1754z00_3113;
																																		{	/* Rgc/rgccompile.scm 492 */
																																			obj_t
																																				BgL_arg1755z00_3114;
																																			{	/* Rgc/rgccompile.scm 492 */
																																				obj_t
																																					BgL_arg1756z00_3115;
																																				{	/* Rgc/rgccompile.scm 492 */
																																					obj_t
																																						BgL_arg1757z00_3116;
																																					BgL_arg1757z00_3116
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_symbol2146z00zz__rgc_compilez00,
																																						BNIL);
																																					BgL_arg1756z00_3115
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BINT
																																						(1L),
																																						BgL_arg1757z00_3116);
																																				}
																																				BgL_arg1755z00_3114
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BGl_symbol2183z00zz__rgc_compilez00,
																																					BgL_arg1756z00_3115);
																																			}
																																			BgL_arg1754z00_3113
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1755z00_3114,
																																				BNIL);
																																		}
																																		BgL_arg1753z00_3112
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_smz00_3100,
																																			BgL_arg1754z00_3113);
																																	}
																																	BgL_arg1752z00_3111
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_mz00_3099,
																																		BgL_arg1753z00_3112);
																																}
																																BgL_arg1745z00_3104
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BGl_symbol2209z00zz__rgc_compilez00,
																																	BgL_arg1752z00_3111);
																															}
																														BgL_arg1744z00_3103
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1745z00_3104,
																															BNIL);
																													}
																													BgL_arg1741z00_3101 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1743z00_3102,
																														BgL_arg1744z00_3103);
																												}
																												return
																													MAKE_YOUNG_PAIR
																													(BGl_symbol2168z00zz__rgc_compilez00,
																													BgL_arg1741z00_3101);
																											}
																										}
																									else
																										{	/* Rgc/rgccompile.scm 482 */
																											return
																												BGl_errorz00zz__errorz00
																												(BGl_string2213z00zz__rgc_compilez00,
																												BGl_string2214z00zz__rgc_compilez00,
																												BgL_ezd2113zd2_3024);
																										}
																								}
																							else
																								{	/* Rgc/rgccompile.scm 482 */
																									return
																										BGl_errorz00zz__errorz00
																										(BGl_string2213z00zz__rgc_compilez00,
																										BGl_string2214z00zz__rgc_compilez00,
																										BgL_ezd2113zd2_3024);
																								}
																						}
																					else
																						{	/* Rgc/rgccompile.scm 482 */
																							return
																								BGl_errorz00zz__errorz00
																								(BGl_string2213z00zz__rgc_compilez00,
																								BGl_string2214z00zz__rgc_compilez00,
																								BgL_ezd2113zd2_3024);
																						}
																				}
																			else
																				{	/* Rgc/rgccompile.scm 482 */
																					return
																						BGl_errorz00zz__errorz00
																						(BGl_string2213z00zz__rgc_compilez00,
																						BGl_string2214z00zz__rgc_compilez00,
																						BgL_ezd2113zd2_3024);
																				}
																		}
																	else
																		{	/* Rgc/rgccompile.scm 482 */
																			return
																				BGl_errorz00zz__errorz00
																				(BGl_string2213z00zz__rgc_compilez00,
																				BGl_string2214z00zz__rgc_compilez00,
																				BgL_ezd2113zd2_3024);
																		}
																}
															else
																{	/* Rgc/rgccompile.scm 482 */
																	return
																		BGl_errorz00zz__errorz00
																		(BGl_string2213z00zz__rgc_compilez00,
																		BGl_string2214z00zz__rgc_compilez00,
																		BgL_ezd2113zd2_3024);
																}
														}
													else
														{	/* Rgc/rgccompile.scm 482 */
															return
																BGl_errorz00zz__errorz00
																(BGl_string2213z00zz__rgc_compilez00,
																BGl_string2214z00zz__rgc_compilez00,
																BgL_ezd2113zd2_3024);
														}
												}
										}
									else
										{	/* Rgc/rgccompile.scm 482 */
											obj_t BgL_carzd2383zd2_3137;

											BgL_carzd2383zd2_3137 =
												CAR(((obj_t) BgL_cdrzd2125zd2_3130));
											if (PAIRP(BgL_carzd2383zd2_3137))
												{	/* Rgc/rgccompile.scm 482 */
													obj_t BgL_carzd2388zd2_3138;

													BgL_carzd2388zd2_3138 = CAR(BgL_carzd2383zd2_3137);
													if (PAIRP(BgL_carzd2388zd2_3138))
														{	/* Rgc/rgccompile.scm 482 */
															obj_t BgL_cdrzd2394zd2_3139;

															BgL_cdrzd2394zd2_3139 =
																CDR(BgL_carzd2388zd2_3138);
															if (PAIRP(BgL_cdrzd2394zd2_3139))
																{	/* Rgc/rgccompile.scm 482 */
																	obj_t BgL_cdrzd2399zd2_3140;

																	BgL_cdrzd2399zd2_3140 =
																		CDR(BgL_cdrzd2394zd2_3139);
																	if (PAIRP(BgL_cdrzd2399zd2_3140))
																		{	/* Rgc/rgccompile.scm 482 */
																			if (NULLP(CDR(BgL_cdrzd2399zd2_3140)))
																				{	/* Rgc/rgccompile.scm 482 */
																					if (NULLP(CDR(BgL_carzd2383zd2_3137)))
																						{	/* Rgc/rgccompile.scm 482 */
																							if (NULLP(CDR(
																										((obj_t)
																											BgL_cdrzd2125zd2_3130))))
																								{
																									obj_t BgL_smz00_4059;
																									obj_t BgL_mz00_4057;
																									obj_t BgL_sz00_4055;
																									obj_t BgL_charz00_4053;

																									BgL_charz00_4053 =
																										CAR(BgL_ezd2113zd2_3024);
																									BgL_sz00_4055 =
																										CAR(BgL_carzd2388zd2_3138);
																									BgL_mz00_4057 =
																										CAR(BgL_cdrzd2394zd2_3139);
																									BgL_smz00_4059 =
																										CAR(BgL_cdrzd2399zd2_3140);
																									BgL_smz00_3100 =
																										BgL_smz00_4059;
																									BgL_mz00_3099 = BgL_mz00_4057;
																									BgL_sz00_3098 = BgL_sz00_4055;
																									BgL_charz00_3097 =
																										BgL_charz00_4053;
																									goto BgL_tagzd2110zd2_3063;
																								}
																							else
																								{	/* Rgc/rgccompile.scm 482 */
																									obj_t BgL_cdrzd2435zd2_3142;

																									BgL_cdrzd2435zd2_3142 =
																										CDR(
																										((obj_t)
																											BgL_cdrzd2125zd2_3130));
																									{	/* Rgc/rgccompile.scm 482 */
																										obj_t BgL_carzd2442zd2_3143;

																										{	/* Rgc/rgccompile.scm 482 */
																											obj_t BgL_pairz00_3144;

																											BgL_pairz00_3144 =
																												CAR(
																												((obj_t)
																													BgL_cdrzd2125zd2_3130));
																											BgL_carzd2442zd2_3143 =
																												CAR(BgL_pairz00_3144);
																										}
																										{	/* Rgc/rgccompile.scm 482 */
																											obj_t
																												BgL_cdrzd2451zd2_3145;
																											BgL_cdrzd2451zd2_3145 =
																												CDR(((obj_t)
																													BgL_carzd2442zd2_3143));
																											if (PAIRP
																												(BgL_cdrzd2435zd2_3142))
																												{	/* Rgc/rgccompile.scm 482 */
																													obj_t
																														BgL_carzd2470zd2_3146;
																													BgL_carzd2470zd2_3146
																														=
																														CAR
																														(BgL_cdrzd2435zd2_3142);
																													if (PAIRP
																														(BgL_carzd2470zd2_3146))
																														{	/* Rgc/rgccompile.scm 482 */
																															if (NULLP(CDR
																																	(BgL_cdrzd2435zd2_3142)))
																																{	/* Rgc/rgccompile.scm 482 */
																																	obj_t
																																		BgL_arg1714z00_3147;
																																	obj_t
																																		BgL_arg1715z00_3148;
																																	obj_t
																																		BgL_arg1717z00_3149;
																																	obj_t
																																		BgL_arg1718z00_3150;
																																	obj_t
																																		BgL_arg1720z00_3151;
																																	obj_t
																																		BgL_arg1722z00_3152;
																																	BgL_arg1714z00_3147
																																		=
																																		CAR
																																		(BgL_ezd2113zd2_3024);
																																	BgL_arg1715z00_3148
																																		=
																																		CAR(((obj_t)
																																			BgL_carzd2442zd2_3143));
																																	BgL_arg1717z00_3149
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd2451zd2_3145));
																																	{	/* Rgc/rgccompile.scm 482 */
																																		obj_t
																																			BgL_pairz00_3153;
																																		BgL_pairz00_3153
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_cdrzd2451zd2_3145));
																																		BgL_arg1718z00_3150
																																			=
																																			CAR
																																			(BgL_pairz00_3153);
																																	}
																																	BgL_arg1720z00_3151
																																		=
																																		CAR
																																		(BgL_carzd2470zd2_3146);
																																	BgL_arg1722z00_3152
																																		=
																																		CDR
																																		(BgL_carzd2470zd2_3146);
																																	BgL_charz00_3065
																																		=
																																		BgL_arg1714z00_3147;
																																	BgL_sz00_3066
																																		=
																																		BgL_arg1715z00_3148;
																																	BgL_mz00_3067
																																		=
																																		BgL_arg1717z00_3149;
																																	BgL_smz00_3068
																																		=
																																		BgL_arg1718z00_3150;
																																	BgL_tsz00_3069
																																		=
																																		BgL_arg1720z00_3151;
																																	BgL_tsmz00_3070
																																		=
																																		BgL_arg1722z00_3152;
																																	{	/* Rgc/rgccompile.scm 494 */
																																		obj_t
																																			BgL_arg1758z00_3071;
																																		{	/* Rgc/rgccompile.scm 494 */
																																			obj_t
																																				BgL_arg1759z00_3072;
																																			obj_t
																																				BgL_arg1760z00_3073;
																																			BgL_arg1759z00_3072
																																				=
																																				BGl_compilezd2memberzd2testz00zz__rgc_compilez00
																																				(BgL_currentz00_3023,
																																				BgL_charz00_3065);
																																			{	/* Rgc/rgccompile.scm 496 */
																																				obj_t
																																					BgL_arg1761z00_3074;
																																				{	/* Rgc/rgccompile.scm 496 */
																																					obj_t
																																						BgL_arg1762z00_3075;
																																					{	/* Rgc/rgccompile.scm 496 */
																																						obj_t
																																							BgL_arg1763z00_3076;
																																						obj_t
																																							BgL_arg1764z00_3077;
																																						if (CBOOL(BgL_sz00_3066))
																																							{	/* Rgc/rgccompile.scm 499 */
																																								obj_t
																																									BgL_arg1765z00_3078;
																																								{	/* Rgc/rgccompile.scm 499 */
																																									obj_t
																																										BgL_arg1766z00_3079;
																																									{	/* Rgc/rgccompile.scm 499 */
																																										obj_t
																																											BgL_arg1767z00_3080;
																																										{	/* Rgc/rgccompile.scm 499 */
																																											obj_t
																																												BgL_arg1768z00_3081;
																																											{	/* Rgc/rgccompile.scm 499 */
																																												obj_t
																																													BgL_arg1769z00_3082;
																																												{	/* Rgc/rgccompile.scm 499 */
																																													obj_t
																																														BgL_arg1770z00_3083;
																																													BgL_arg1770z00_3083
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BGl_symbol2146z00zz__rgc_compilez00,
																																														BNIL);
																																													BgL_arg1769z00_3082
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BINT
																																														(1L),
																																														BgL_arg1770z00_3083);
																																												}
																																												BgL_arg1768z00_3081
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BGl_symbol2183z00zz__rgc_compilez00,
																																													BgL_arg1769z00_3082);
																																											}
																																											BgL_arg1767z00_3080
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_arg1768z00_3081,
																																												BNIL);
																																										}
																																										BgL_arg1766z00_3079
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_smz00_3068,
																																											BgL_arg1767z00_3080);
																																									}
																																									BgL_arg1765z00_3078
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_mz00_3067,
																																										BgL_arg1766z00_3079);
																																								}
																																								BgL_arg1763z00_3076
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BGl_symbol2207z00zz__rgc_compilez00,
																																									BgL_arg1765z00_3078);
																																							}
																																						else
																																							{	/* Rgc/rgccompile.scm 500 */
																																								obj_t
																																									BgL_arg1771z00_3084;
																																								{	/* Rgc/rgccompile.scm 500 */
																																									obj_t
																																										BgL_arg1772z00_3085;
																																									{	/* Rgc/rgccompile.scm 500 */
																																										obj_t
																																											BgL_arg1773z00_3086;
																																										{	/* Rgc/rgccompile.scm 500 */
																																											obj_t
																																												BgL_arg1774z00_3087;
																																											{	/* Rgc/rgccompile.scm 500 */
																																												obj_t
																																													BgL_arg1775z00_3088;
																																												{	/* Rgc/rgccompile.scm 500 */
																																													obj_t
																																														BgL_arg1777z00_3089;
																																													BgL_arg1777z00_3089
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BGl_symbol2146z00zz__rgc_compilez00,
																																														BNIL);
																																													BgL_arg1775z00_3088
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BINT
																																														(1L),
																																														BgL_arg1777z00_3089);
																																												}
																																												BgL_arg1774z00_3087
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BGl_symbol2183z00zz__rgc_compilez00,
																																													BgL_arg1775z00_3088);
																																											}
																																											BgL_arg1773z00_3086
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_arg1774z00_3087,
																																												BNIL);
																																										}
																																										BgL_arg1772z00_3085
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_smz00_3068,
																																											BgL_arg1773z00_3086);
																																									}
																																									BgL_arg1771z00_3084
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_mz00_3067,
																																										BgL_arg1772z00_3085);
																																								}
																																								BgL_arg1763z00_3076
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BGl_symbol2209z00zz__rgc_compilez00,
																																									BgL_arg1771z00_3084);
																																							}
																																						{	/* Rgc/rgccompile.scm 501 */
																																							obj_t
																																								BgL_arg1779z00_3090;
																																							{	/* Rgc/rgccompile.scm 501 */
																																								obj_t
																																									BgL_arg1781z00_3091;
																																								{	/* Rgc/rgccompile.scm 501 */
																																									obj_t
																																										BgL_arg1782z00_3092;
																																									{	/* Rgc/rgccompile.scm 501 */
																																										obj_t
																																											BgL_arg1783z00_3093;
																																										{	/* Rgc/rgccompile.scm 501 */
																																											obj_t
																																												BgL_arg1785z00_3094;
																																											{	/* Rgc/rgccompile.scm 501 */
																																												obj_t
																																													BgL_arg1786z00_3095;
																																												{	/* Rgc/rgccompile.scm 501 */
																																													obj_t
																																														BgL_arg1787z00_3096;
																																													BgL_arg1787z00_3096
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BGl_symbol2146z00zz__rgc_compilez00,
																																														BNIL);
																																													BgL_arg1786z00_3095
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BINT
																																														(1L),
																																														BgL_arg1787z00_3096);
																																												}
																																												BgL_arg1785z00_3094
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BGl_symbol2183z00zz__rgc_compilez00,
																																													BgL_arg1786z00_3095);
																																											}
																																											BgL_arg1783z00_3093
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_arg1785z00_3094,
																																												BNIL);
																																										}
																																										BgL_arg1782z00_3092
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_tsmz00_3070,
																																											BgL_arg1783z00_3093);
																																									}
																																									BgL_arg1781z00_3091
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_tsz00_3069,
																																										BgL_arg1782z00_3092);
																																								}
																																								BgL_arg1779z00_3090
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BGl_symbol2211z00zz__rgc_compilez00,
																																									BgL_arg1781z00_3091);
																																							}
																																							BgL_arg1764z00_3077
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_arg1779z00_3090,
																																								BNIL);
																																						}
																																						BgL_arg1762z00_3075
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg1763z00_3076,
																																							BgL_arg1764z00_3077);
																																					}
																																					BgL_arg1761z00_3074
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_symbol2199z00zz__rgc_compilez00,
																																						BgL_arg1762z00_3075);
																																				}
																																				BgL_arg1760z00_3073
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1761z00_3074,
																																					BNIL);
																																			}
																																			BgL_arg1758z00_3071
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1759z00_3072,
																																				BgL_arg1760z00_3073);
																																		}
																																		return
																																			MAKE_YOUNG_PAIR
																																			(BGl_symbol2168z00zz__rgc_compilez00,
																																			BgL_arg1758z00_3071);
																																	}
																																}
																															else
																																{	/* Rgc/rgccompile.scm 482 */
																																	return
																																		BGl_errorz00zz__errorz00
																																		(BGl_string2213z00zz__rgc_compilez00,
																																		BGl_string2214z00zz__rgc_compilez00,
																																		BgL_ezd2113zd2_3024);
																																}
																														}
																													else
																														{	/* Rgc/rgccompile.scm 482 */
																															return
																																BGl_errorz00zz__errorz00
																																(BGl_string2213z00zz__rgc_compilez00,
																																BGl_string2214z00zz__rgc_compilez00,
																																BgL_ezd2113zd2_3024);
																														}
																												}
																											else
																												{	/* Rgc/rgccompile.scm 482 */
																													return
																														BGl_errorz00zz__errorz00
																														(BGl_string2213z00zz__rgc_compilez00,
																														BGl_string2214z00zz__rgc_compilez00,
																														BgL_ezd2113zd2_3024);
																												}
																										}
																									}
																								}
																						}
																					else
																						{	/* Rgc/rgccompile.scm 482 */
																							return
																								BGl_errorz00zz__errorz00
																								(BGl_string2213z00zz__rgc_compilez00,
																								BGl_string2214z00zz__rgc_compilez00,
																								BgL_ezd2113zd2_3024);
																						}
																				}
																			else
																				{	/* Rgc/rgccompile.scm 482 */
																					return
																						BGl_errorz00zz__errorz00
																						(BGl_string2213z00zz__rgc_compilez00,
																						BGl_string2214z00zz__rgc_compilez00,
																						BgL_ezd2113zd2_3024);
																				}
																		}
																	else
																		{	/* Rgc/rgccompile.scm 482 */
																			return
																				BGl_errorz00zz__errorz00
																				(BGl_string2213z00zz__rgc_compilez00,
																				BGl_string2214z00zz__rgc_compilez00,
																				BgL_ezd2113zd2_3024);
																		}
																}
															else
																{	/* Rgc/rgccompile.scm 482 */
																	return
																		BGl_errorz00zz__errorz00
																		(BGl_string2213z00zz__rgc_compilez00,
																		BGl_string2214z00zz__rgc_compilez00,
																		BgL_ezd2113zd2_3024);
																}
														}
													else
														{	/* Rgc/rgccompile.scm 482 */
															return
																BGl_errorz00zz__errorz00
																(BGl_string2213z00zz__rgc_compilez00,
																BGl_string2214z00zz__rgc_compilez00,
																BgL_ezd2113zd2_3024);
														}
												}
											else
												{	/* Rgc/rgccompile.scm 482 */
													return
														BGl_errorz00zz__errorz00
														(BGl_string2213z00zz__rgc_compilez00,
														BGl_string2214z00zz__rgc_compilez00,
														BgL_ezd2113zd2_3024);
												}
										}
								}
							else
								{	/* Rgc/rgccompile.scm 482 */
									return
										BGl_errorz00zz__errorz00
										(BGl_string2213z00zz__rgc_compilez00,
										BGl_string2214z00zz__rgc_compilez00, BgL_ezd2113zd2_3024);
								}
						}
					else
						{	/* Rgc/rgccompile.scm 482 */
							return
								BGl_errorz00zz__errorz00(BGl_string2213z00zz__rgc_compilez00,
								BGl_string2214z00zz__rgc_compilez00, BgL_ezd2113zd2_3024);
						}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__rgc_compilez00(void)
	{
		{	/* Rgc/rgccompile.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__rgc_compilez00(void)
	{
		{	/* Rgc/rgccompile.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__rgc_compilez00(void)
	{
		{	/* Rgc/rgccompile.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__rgc_compilez00(void)
	{
		{	/* Rgc/rgccompile.scm 15 */
			BGl_modulezd2initializa7ationz75zz__rgc_rulesz00(181068393L,
				BSTRING_TO_STRING(BGl_string2215z00zz__rgc_compilez00));
			BGl_modulezd2initializa7ationz75zz__rgc_dfaz00(477555221L,
				BSTRING_TO_STRING(BGl_string2215z00zz__rgc_compilez00));
			BGl_modulezd2initializa7ationz75zz__rgc_setz00(225075643L,
				BSTRING_TO_STRING(BGl_string2215z00zz__rgc_compilez00));
			BGl_modulezd2initializa7ationz75zz__rgc_configz00(428274736L,
				BSTRING_TO_STRING(BGl_string2215z00zz__rgc_compilez00));
			return
				BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string2215z00zz__rgc_compilez00));
		}

	}

#ifdef __cplusplus
}
#endif
