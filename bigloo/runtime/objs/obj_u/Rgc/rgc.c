/*===========================================================================*/
/*   (Rgc/rgc.scm)                                                           */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Rgc/rgc.scm -indent -o objs/obj_u/Rgc/rgc.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___RGC_TYPE_DEFINITIONS
#define BGL___RGC_TYPE_DEFINITIONS
#endif													// BGL___RGC_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL int BGl_rgczd2bufferzd2ungetzd2charzd2zz__rgcz00(obj_t,
		int);
	BGL_EXPORTED_DECL obj_t BGl_rgczd2bufferzd2symbolz00zz__rgcz00(obj_t);
	BGL_EXPORTED_DECL long BGl_rgczd2stopzd2matchz12z12zz__rgcz00(obj_t, long);
	BGL_EXPORTED_DECL bool_t BGl_rgczd2bufferzd2bolzf3zf3zz__rgcz00(obj_t);
	static obj_t BGl_z62rgczd2stopzd2matchz12z70zz__rgcz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_rgczd2bufferzd2subsymbolz00zz__rgcz00(obj_t, long,
		long);
	static obj_t BGl_z62rgczd2bufferzd2bolzf3z91zz__rgcz00(obj_t, obj_t);
	static obj_t BGl_z62rgczd2setzd2fileposz12z70zz__rgcz00(obj_t, obj_t);
	extern obj_t rgc_buffer_upcase_subsymbol(obj_t, long, long);
	BGL_EXPORTED_DECL bool_t
		BGl_rgczd2bufferzd2insertzd2charz12zc0zz__rgcz00(obj_t, long);
	static obj_t BGl_requirezd2initializa7ationz75zz__rgcz00 = BUNSPEC;
	extern obj_t rgc_buffer_escape_substring(obj_t, long, long, bool_t);
	static obj_t BGl_z62rgczd2bufferzd2insertzd2charz12za2zz__rgcz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rgczd2bufferzd2keywordz62zz__rgcz00(obj_t, obj_t);
	BGL_EXPORTED_DECL int BGl_rgczd2bufferzd2getzd2charzd2zz__rgcz00(obj_t, long);
	BGL_EXPORTED_DECL long BGl_rgczd2bufferzd2bufposz00zz__rgcz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rgczd2bufferzd2upcasezd2keywordzd2zz__rgcz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rgczd2bufferzd2downcasezd2symbolzd2zz__rgcz00(obj_t);
	extern bool_t rgc_buffer_bol_p(obj_t);
	static obj_t BGl_z62rgczd2thezd2submatchz62zz__rgcz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rgczd2bufferzd2downcasezd2keywordzd2zz__rgcz00(obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__rgcz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_z62rgczd2bufferzd2characterz62zz__rgcz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rgczd2bufferzd2eofzf3zf3zz__rgcz00(obj_t);
	static obj_t BGl_z62rgczd2bufferzd2eofzf3z91zz__rgcz00(obj_t, obj_t);
	static obj_t BGl_z62rgczd2bufferzd2ungetzd2charzb0zz__rgcz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_toplevelzd2initzd2zz__rgcz00(void);
	static obj_t BGl_z62rgczd2bufferzd2downcasezd2symbolzb0zz__rgcz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rgczd2bufferzd2upcasezd2symbolzd2zz__rgcz00(obj_t);
	extern bool_t rgc_buffer_insert_substring(obj_t, obj_t, long, long);
	extern obj_t rgc_buffer_downcase_keyword(obj_t);
	BGL_EXPORTED_DECL unsigned char
		BGl_rgczd2bufferzd2characterz00zz__rgcz00(obj_t);
	extern bool_t rgc_buffer_eof_p(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rgczd2bufferzd2downcasezd2subsymbolzd2zz__rgcz00(obj_t, long, long);
	BGL_EXPORTED_DECL long BGl_rgczd2startzd2matchz12z12zz__rgcz00(obj_t);
	static obj_t BGl_z62rgczd2bufferzd2bytezd2refzb0zz__rgcz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zz__rgcz00(void);
	BGL_EXPORTED_DECL bool_t BGl_rgczd2bufferzd2eof2zf3zf3zz__rgcz00(obj_t, long,
		long);
	static obj_t BGl_genericzd2initzd2zz__rgcz00(void);
	static obj_t BGl_z62rgczd2bufferzd2positionz62zz__rgcz00(obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__rgcz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__rgcz00(void);
	extern obj_t rgc_buffer_subsymbol(obj_t, long, long);
	static obj_t BGl_z62rgczd2bufferzd2substringz62zz__rgcz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_objectzd2initzd2zz__rgcz00(void);
	extern obj_t rgc_buffer_integer(obj_t);
	static obj_t BGl_z62rgczd2bufferzd2upcasezd2subsymbolzb0zz__rgcz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62rgczd2bufferzd2escapezd2substringzb0zz__rgcz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	extern long rgc_buffer_fixnum(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_rgczd2bufferzd2substringz00zz__rgcz00(obj_t, long,
		long);
	BGL_EXPORTED_DECL obj_t BGl_rgczd2bufferzd2integerz00zz__rgcz00(obj_t);
	extern double rgc_buffer_flonum(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rgczd2bufferzd2eolzf3zf3zz__rgcz00(obj_t, long,
		long);
	extern bool_t rgc_buffer_insert_char(obj_t, int);
	static obj_t BGl_z62rgczd2bufferzd2eolzf3z91zz__rgcz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rgczd2bufferzd2downcasezd2subsymbolzb0zz__rgcz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62rgczd2bufferzd2insertzd2substringz12za2zz__rgcz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_z62rgczd2bufferzd2getzd2charzb0zz__rgcz00(obj_t, obj_t,
		obj_t);
	extern int rgc_buffer_unget_char(obj_t, int);
	BGL_EXPORTED_DECL bool_t BGl_rgczd2bufferzd2bofzf3zf3zz__rgcz00(obj_t);
	extern obj_t rgc_buffer_symbol(obj_t);
	static obj_t BGl_z62rgczd2bufferzd2bofzf3z91zz__rgcz00(obj_t, obj_t);
	static obj_t BGl_z62rgczd2bufferzd2fixnumz62zz__rgcz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_rgczd2setzd2fileposz12z12zz__rgcz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rgczd2fillzd2bufferz00zz__rgcz00(obj_t);
	static obj_t BGl_z62rgczd2bufferzd2lengthz62zz__rgcz00(obj_t, obj_t);
	static obj_t BGl_z62rgczd2bufferzd2upcasezd2symbolzb0zz__rgcz00(obj_t, obj_t);
	static obj_t BGl_z62rgczd2bufferzd2downcasezd2keywordzb0zz__rgcz00(obj_t,
		obj_t);
	static obj_t BGl_z62rgczd2fillzd2bufferz62zz__rgcz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2unsafezd2rgcza2zd2zz__rgcz00 = BUNSPEC;
	static obj_t BGl_methodzd2initzd2zz__rgcz00(void);
	static obj_t BGl_z62rgczd2bufferzd2integerz62zz__rgcz00(obj_t, obj_t);
	BGL_EXPORTED_DECL long BGl_rgczd2bufferzd2forwardz00zz__rgcz00(obj_t);
	static obj_t BGl_z62rgczd2bufferzd2flonumz62zz__rgcz00(obj_t, obj_t);
	extern bool_t rgc_buffer_eol_p(obj_t, long, long);
	BGL_EXPORTED_DECL bool_t
		BGl_rgczd2bufferzd2insertzd2substringz12zc0zz__rgcz00(obj_t, obj_t, long,
		long);
	static obj_t BGl_z62rgczd2startzd2matchz12z70zz__rgcz00(obj_t, obj_t);
	extern bool_t rgc_buffer_eof2_p(obj_t, long, long);
	static obj_t BGl_z62rgczd2bufferzd2eof2zf3z91zz__rgcz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t rgc_buffer_upcase_keyword(obj_t);
	extern bool_t rgc_buffer_bof_p(obj_t);
	static obj_t BGl_z62rgczd2bufferzd2symbolz62zz__rgcz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_rgczd2thezd2submatchz00zz__rgcz00(obj_t, long,
		long, long);
	extern bool_t rgc_fill_buffer(obj_t);
	static obj_t BGl_z62rgczd2bufferzd2forwardz62zz__rgcz00(obj_t, obj_t);
	BGL_EXPORTED_DECL int BGl_rgczd2bufferzd2bytezd2refzd2zz__rgcz00(obj_t, int);
	extern obj_t rgc_buffer_keyword(obj_t);
	BGL_EXPORTED_DECL int BGl_rgczd2bufferzd2bytez00zz__rgcz00(obj_t);
	static obj_t BGl_z62rgczd2bufferzd2bufposz62zz__rgcz00(obj_t, obj_t);
	BGL_EXPORTED_DECL long BGl_rgczd2bufferzd2positionz00zz__rgcz00(obj_t, long);
	BGL_EXPORTED_DECL long BGl_rgczd2bufferzd2fixnumz00zz__rgcz00(obj_t);
	static obj_t BGl_z62rgczd2bufferzd2upcasezd2keywordzb0zz__rgcz00(obj_t,
		obj_t);
	static obj_t BGl_symbol1739z00zz__rgcz00 = BUNSPEC;
	static obj_t BGl_z62rgczd2bufferzd2bytez62zz__rgcz00(obj_t, obj_t);
	BGL_EXPORTED_DECL long BGl_rgczd2bufferzd2lengthz00zz__rgcz00(obj_t);
	extern obj_t rgc_buffer_substring(obj_t, long, long);
	static obj_t BGl_z62rgczd2bufferzd2subsymbolz62zz__rgcz00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rgczd2bufferzd2upcasezd2subsymbolzd2zz__rgcz00(obj_t, long, long);
	BGL_EXPORTED_DECL double BGl_rgczd2bufferzd2flonumz00zz__rgcz00(obj_t);
	static obj_t BGl_symbol1741z00zz__rgcz00 = BUNSPEC;
	static obj_t BGl_symbol1743z00zz__rgcz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_rgczd2bufferzd2escapezd2substringzd2zz__rgcz00(obj_t, long, long,
		bool_t);
	BGL_EXPORTED_DECL obj_t BGl_rgczd2bufferzd2keywordz00zz__rgcz00(obj_t);
	extern obj_t rgc_buffer_downcase_subsymbol(obj_t, long, long);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgczd2bufferzd2eof2zf3zd2envz21zz__rgcz00,
		BgL_bgl_za762rgcza7d2bufferza71747za7,
		BGl_z62rgczd2bufferzd2eof2zf3z91zz__rgcz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgczd2bufferzd2positionzd2envzd2zz__rgcz00,
		BgL_bgl_za762rgcza7d2bufferza71748za7,
		BGl_z62rgczd2bufferzd2positionz62zz__rgcz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgczd2bufferzd2bofzf3zd2envz21zz__rgcz00,
		BgL_bgl_za762rgcza7d2bufferza71749za7,
		BGl_z62rgczd2bufferzd2bofzf3z91zz__rgcz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgczd2bufferzd2flonumzd2envzd2zz__rgcz00,
		BgL_bgl_za762rgcza7d2bufferza71750za7,
		BGl_z62rgczd2bufferzd2flonumz62zz__rgcz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgczd2bufferzd2keywordzd2envzd2zz__rgcz00,
		BgL_bgl_za762rgcza7d2bufferza71751za7,
		BGl_z62rgczd2bufferzd2keywordz62zz__rgcz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgczd2thezd2submatchzd2envzd2zz__rgcz00,
		BgL_bgl_za762rgcza7d2theza7d2s1752za7,
		BGl_z62rgczd2thezd2submatchz62zz__rgcz00, 0L, BUNSPEC, 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rgczd2bufferzd2upcasezd2symbolzd2envz00zz__rgcz00,
		BgL_bgl_za762rgcza7d2bufferza71753za7,
		BGl_z62rgczd2bufferzd2upcasezd2symbolzb0zz__rgcz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rgczd2bufferzd2bytezd2refzd2envz00zz__rgcz00,
		BgL_bgl_za762rgcza7d2bufferza71754za7,
		BGl_z62rgczd2bufferzd2bytezd2refzb0zz__rgcz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgczd2bufferzd2eolzf3zd2envz21zz__rgcz00,
		BgL_bgl_za762rgcza7d2bufferza71755za7,
		BGl_z62rgczd2bufferzd2eolzf3z91zz__rgcz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rgczd2bufferzd2insertzd2charz12zd2envz12zz__rgcz00,
		BgL_bgl_za762rgcza7d2bufferza71756za7,
		BGl_z62rgczd2bufferzd2insertzd2charz12za2zz__rgcz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgczd2bufferzd2lengthzd2envzd2zz__rgcz00,
		BgL_bgl_za762rgcza7d2bufferza71757za7,
		BGl_z62rgczd2bufferzd2lengthz62zz__rgcz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgczd2fillzd2bufferzd2envzd2zz__rgcz00,
		BgL_bgl_za762rgcza7d2fillza7d21758za7,
		BGl_z62rgczd2fillzd2bufferz62zz__rgcz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rgczd2bufferzd2upcasezd2subsymbolzd2envz00zz__rgcz00,
		BgL_bgl_za762rgcza7d2bufferza71759za7,
		BGl_z62rgczd2bufferzd2upcasezd2subsymbolzb0zz__rgcz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgczd2bufferzd2eofzf3zd2envz21zz__rgcz00,
		BgL_bgl_za762rgcza7d2bufferza71760za7,
		BGl_z62rgczd2bufferzd2eofzf3z91zz__rgcz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rgczd2bufferzd2ungetzd2charzd2envz00zz__rgcz00,
		BgL_bgl_za762rgcza7d2bufferza71761za7,
		BGl_z62rgczd2bufferzd2ungetzd2charzb0zz__rgcz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgczd2bufferzd2symbolzd2envzd2zz__rgcz00,
		BgL_bgl_za762rgcza7d2bufferza71762za7,
		BGl_z62rgczd2bufferzd2symbolz62zz__rgcz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1701z00zz__rgcz00,
		BgL_bgl_string1701za700za7za7_1763za7, "/tmp/bigloo/runtime/Rgc/rgc.scm",
		31);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgczd2stopzd2matchz12zd2envzc0zz__rgcz00,
		BgL_bgl_za762rgcza7d2stopza7d21764za7,
		BGl_z62rgczd2stopzd2matchz12z70zz__rgcz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1702z00zz__rgcz00,
		BgL_bgl_string1702za700za7za7_1765za7, "&rgc-buffer-get-char", 20);
	      DEFINE_STRING(BGl_string1703z00zz__rgcz00,
		BgL_bgl_string1703za700za7za7_1766za7, "input-port", 10);
	      DEFINE_STRING(BGl_string1704z00zz__rgcz00,
		BgL_bgl_string1704za700za7za7_1767za7, "bint", 4);
	      DEFINE_STRING(BGl_string1705z00zz__rgcz00,
		BgL_bgl_string1705za700za7za7_1768za7, "&rgc-buffer-unget-char", 22);
	      DEFINE_STRING(BGl_string1706z00zz__rgcz00,
		BgL_bgl_string1706za700za7za7_1769za7, "&rgc-buffer-insert-substring!", 29);
	      DEFINE_STRING(BGl_string1707z00zz__rgcz00,
		BgL_bgl_string1707za700za7za7_1770za7, "bstring", 7);
	      DEFINE_STRING(BGl_string1708z00zz__rgcz00,
		BgL_bgl_string1708za700za7za7_1771za7, "&rgc-buffer-insert-char!", 24);
	      DEFINE_STRING(BGl_string1709z00zz__rgcz00,
		BgL_bgl_string1709za700za7za7_1772za7, "&rgc-buffer-character", 21);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgczd2bufferzd2characterzd2envzd2zz__rgcz00,
		BgL_bgl_za762rgcza7d2bufferza71773za7,
		BGl_z62rgczd2bufferzd2characterz62zz__rgcz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1710z00zz__rgcz00,
		BgL_bgl_string1710za700za7za7_1774za7, "&rgc-buffer-byte", 16);
	      DEFINE_STRING(BGl_string1711z00zz__rgcz00,
		BgL_bgl_string1711za700za7za7_1775za7, "&rgc-buffer-byte-ref", 20);
	      DEFINE_STRING(BGl_string1712z00zz__rgcz00,
		BgL_bgl_string1712za700za7za7_1776za7, "&rgc-buffer-substring", 21);
	      DEFINE_STRING(BGl_string1713z00zz__rgcz00,
		BgL_bgl_string1713za700za7za7_1777za7, "&rgc-buffer-escape-substring", 28);
	      DEFINE_STRING(BGl_string1714z00zz__rgcz00,
		BgL_bgl_string1714za700za7za7_1778za7, "&rgc-buffer-length", 18);
	      DEFINE_STRING(BGl_string1715z00zz__rgcz00,
		BgL_bgl_string1715za700za7za7_1779za7, "&rgc-buffer-fixnum", 18);
	      DEFINE_STRING(BGl_string1716z00zz__rgcz00,
		BgL_bgl_string1716za700za7za7_1780za7, "&rgc-buffer-integer", 19);
	      DEFINE_STRING(BGl_string1717z00zz__rgcz00,
		BgL_bgl_string1717za700za7za7_1781za7, "&rgc-buffer-flonum", 18);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rgczd2bufferzd2downcasezd2subsymbolzd2envz00zz__rgcz00,
		BgL_bgl_za762rgcza7d2bufferza71782za7,
		BGl_z62rgczd2bufferzd2downcasezd2subsymbolzb0zz__rgcz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string1718z00zz__rgcz00,
		BgL_bgl_string1718za700za7za7_1783za7, "&rgc-buffer-symbol", 18);
	      DEFINE_STRING(BGl_string1719z00zz__rgcz00,
		BgL_bgl_string1719za700za7za7_1784za7, "&rgc-buffer-subsymbol", 21);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgczd2bufferzd2fixnumzd2envzd2zz__rgcz00,
		BgL_bgl_za762rgcza7d2bufferza71785za7,
		BGl_z62rgczd2bufferzd2fixnumz62zz__rgcz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1720z00zz__rgcz00,
		BgL_bgl_string1720za700za7za7_1786za7, "&rgc-buffer-downcase-symbol", 27);
	      DEFINE_STRING(BGl_string1721z00zz__rgcz00,
		BgL_bgl_string1721za700za7za7_1787za7, "&rgc-buffer-downcase-subsymbol",
		30);
	      DEFINE_STRING(BGl_string1722z00zz__rgcz00,
		BgL_bgl_string1722za700za7za7_1788za7, "&rgc-buffer-upcase-symbol", 25);
	      DEFINE_STRING(BGl_string1723z00zz__rgcz00,
		BgL_bgl_string1723za700za7za7_1789za7, "&rgc-buffer-upcase-subsymbol", 28);
	      DEFINE_STRING(BGl_string1724z00zz__rgcz00,
		BgL_bgl_string1724za700za7za7_1790za7, "&rgc-buffer-keyword", 19);
	      DEFINE_STRING(BGl_string1725z00zz__rgcz00,
		BgL_bgl_string1725za700za7za7_1791za7, "&rgc-buffer-downcase-keyword", 28);
	      DEFINE_STRING(BGl_string1726z00zz__rgcz00,
		BgL_bgl_string1726za700za7za7_1792za7, "&rgc-buffer-upcase-keyword", 26);
	      DEFINE_STRING(BGl_string1727z00zz__rgcz00,
		BgL_bgl_string1727za700za7za7_1793za7, "&rgc-buffer-position", 20);
	      DEFINE_STRING(BGl_string1728z00zz__rgcz00,
		BgL_bgl_string1728za700za7za7_1794za7, "&rgc-buffer-forward", 19);
	      DEFINE_STRING(BGl_string1729z00zz__rgcz00,
		BgL_bgl_string1729za700za7za7_1795za7, "&rgc-buffer-bufpos", 18);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rgczd2bufferzd2downcasezd2keywordzd2envz00zz__rgcz00,
		BgL_bgl_za762rgcza7d2bufferza71796za7,
		BGl_z62rgczd2bufferzd2downcasezd2keywordzb0zz__rgcz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1730z00zz__rgcz00,
		BgL_bgl_string1730za700za7za7_1797za7, "&rgc-set-filepos!", 17);
	      DEFINE_STRING(BGl_string1731z00zz__rgcz00,
		BgL_bgl_string1731za700za7za7_1798za7, "&rgc-start-match!", 17);
	      DEFINE_STRING(BGl_string1732z00zz__rgcz00,
		BgL_bgl_string1732za700za7za7_1799za7, "&rgc-stop-match!", 16);
	      DEFINE_STRING(BGl_string1733z00zz__rgcz00,
		BgL_bgl_string1733za700za7za7_1800za7, "&rgc-fill-buffer", 16);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rgczd2bufferzd2escapezd2substringzd2envz00zz__rgcz00,
		BgL_bgl_za762rgcza7d2bufferza71801za7,
		BGl_z62rgczd2bufferzd2escapezd2substringzb0zz__rgcz00, 0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string1734z00zz__rgcz00,
		BgL_bgl_string1734za700za7za7_1802za7, "&rgc-buffer-bol?", 16);
	      DEFINE_STRING(BGl_string1735z00zz__rgcz00,
		BgL_bgl_string1735za700za7za7_1803za7, "&rgc-buffer-eol?", 16);
	      DEFINE_STRING(BGl_string1736z00zz__rgcz00,
		BgL_bgl_string1736za700za7za7_1804za7, "&rgc-buffer-bof?", 16);
	      DEFINE_STRING(BGl_string1737z00zz__rgcz00,
		BgL_bgl_string1737za700za7za7_1805za7, "&rgc-buffer-eof?", 16);
	      DEFINE_STRING(BGl_string1738z00zz__rgcz00,
		BgL_bgl_string1738za700za7za7_1806za7, "&rgc-buffer-eof2?", 17);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rgczd2bufferzd2insertzd2substringz12zd2envz12zz__rgcz00,
		BgL_bgl_za762rgcza7d2bufferza71807za7,
		BGl_z62rgczd2bufferzd2insertzd2substringz12za2zz__rgcz00, 0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string1740z00zz__rgcz00,
		BgL_bgl_string1740za700za7za7_1808za7, "stop", 4);
	      DEFINE_STRING(BGl_string1742z00zz__rgcz00,
		BgL_bgl_string1742za700za7za7_1809za7, "start", 5);
	      DEFINE_STRING(BGl_string1744z00zz__rgcz00,
		BgL_bgl_string1744za700za7za7_1810za7, "start*", 6);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgczd2bufferzd2substringzd2envzd2zz__rgcz00,
		BgL_bgl_za762rgcza7d2bufferza71811za7,
		BGl_z62rgczd2bufferzd2substringz62zz__rgcz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string1745z00zz__rgcz00,
		BgL_bgl_string1745za700za7za7_1812za7, "&rgc-the-submatch", 17);
	      DEFINE_STRING(BGl_string1746z00zz__rgcz00,
		BgL_bgl_string1746za700za7za7_1813za7, "__rgc", 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgczd2startzd2matchz12zd2envzc0zz__rgcz00,
		BgL_bgl_za762rgcza7d2startza7d1814za7,
		BGl_z62rgczd2startzd2matchz12z70zz__rgcz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rgczd2bufferzd2getzd2charzd2envz00zz__rgcz00,
		BgL_bgl_za762rgcza7d2bufferza71815za7,
		BGl_z62rgczd2bufferzd2getzd2charzb0zz__rgcz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgczd2bufferzd2bufposzd2envzd2zz__rgcz00,
		BgL_bgl_za762rgcza7d2bufferza71816za7,
		BGl_z62rgczd2bufferzd2bufposz62zz__rgcz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rgczd2bufferzd2upcasezd2keywordzd2envz00zz__rgcz00,
		BgL_bgl_za762rgcza7d2bufferza71817za7,
		BGl_z62rgczd2bufferzd2upcasezd2keywordzb0zz__rgcz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgczd2bufferzd2bytezd2envzd2zz__rgcz00,
		BgL_bgl_za762rgcza7d2bufferza71818za7,
		BGl_z62rgczd2bufferzd2bytez62zz__rgcz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rgczd2bufferzd2downcasezd2symbolzd2envz00zz__rgcz00,
		BgL_bgl_za762rgcza7d2bufferza71819za7,
		BGl_z62rgczd2bufferzd2downcasezd2symbolzb0zz__rgcz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgczd2setzd2fileposz12zd2envzc0zz__rgcz00,
		BgL_bgl_za762rgcza7d2setza7d2f1820za7,
		BGl_z62rgczd2setzd2fileposz12z70zz__rgcz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgczd2bufferzd2forwardzd2envzd2zz__rgcz00,
		BgL_bgl_za762rgcza7d2bufferza71821za7,
		BGl_z62rgczd2bufferzd2forwardz62zz__rgcz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgczd2bufferzd2bolzf3zd2envz21zz__rgcz00,
		BgL_bgl_za762rgcza7d2bufferza71822za7,
		BGl_z62rgczd2bufferzd2bolzf3z91zz__rgcz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgczd2bufferzd2subsymbolzd2envzd2zz__rgcz00,
		BgL_bgl_za762rgcza7d2bufferza71823za7,
		BGl_z62rgczd2bufferzd2subsymbolz62zz__rgcz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgczd2bufferzd2integerzd2envzd2zz__rgcz00,
		BgL_bgl_za762rgcza7d2bufferza71824za7,
		BGl_z62rgczd2bufferzd2integerz62zz__rgcz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__rgcz00));
		     ADD_ROOT((void *) (&BGl_za2unsafezd2rgcza2zd2zz__rgcz00));
		     ADD_ROOT((void *) (&BGl_symbol1739z00zz__rgcz00));
		     ADD_ROOT((void *) (&BGl_symbol1741z00zz__rgcz00));
		     ADD_ROOT((void *) (&BGl_symbol1743z00zz__rgcz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__rgcz00(long
		BgL_checksumz00_2082, char *BgL_fromz00_2083)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__rgcz00))
				{
					BGl_requirezd2initializa7ationz75zz__rgcz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__rgcz00();
					BGl_cnstzd2initzd2zz__rgcz00();
					BGl_importedzd2moduleszd2initz00zz__rgcz00();
					return BGl_toplevelzd2initzd2zz__rgcz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__rgcz00(void)
	{
		{	/* Rgc/rgc.scm 17 */
			BGl_symbol1739z00zz__rgcz00 =
				bstring_to_symbol(BGl_string1740z00zz__rgcz00);
			BGl_symbol1741z00zz__rgcz00 =
				bstring_to_symbol(BGl_string1742z00zz__rgcz00);
			return (BGl_symbol1743z00zz__rgcz00 =
				bstring_to_symbol(BGl_string1744z00zz__rgcz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__rgcz00(void)
	{
		{	/* Rgc/rgc.scm 17 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__rgcz00(void)
	{
		{	/* Rgc/rgc.scm 17 */
			return (BGl_za2unsafezd2rgcza2zd2zz__rgcz00 = BFALSE, BUNSPEC);
		}

	}



/* rgc-buffer-get-char */
	BGL_EXPORTED_DEF int BGl_rgczd2bufferzd2getzd2charzd2zz__rgcz00(obj_t
		BgL_inputzd2portzd2_3, long BgL_indexz00_4)
	{
		{	/* Rgc/rgc.scm 241 */
			return RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_3, BgL_indexz00_4);
		}

	}



/* &rgc-buffer-get-char */
	obj_t BGl_z62rgczd2bufferzd2getzd2charzb0zz__rgcz00(obj_t BgL_envz00_1885,
		obj_t BgL_inputzd2portzd2_1886, obj_t BgL_indexz00_1887)
	{
		{	/* Rgc/rgc.scm 241 */
			{	/* Rgc/rgc.scm 242 */
				int BgL_tmpz00_2096;

				{	/* Rgc/rgc.scm 242 */
					long BgL_auxz00_2104;
					obj_t BgL_auxz00_2097;

					{	/* Rgc/rgc.scm 242 */
						obj_t BgL_tmpz00_2105;

						if (INTEGERP(BgL_indexz00_1887))
							{	/* Rgc/rgc.scm 242 */
								BgL_tmpz00_2105 = BgL_indexz00_1887;
							}
						else
							{
								obj_t BgL_auxz00_2108;

								BgL_auxz00_2108 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
									BINT(11343L), BGl_string1702z00zz__rgcz00,
									BGl_string1704z00zz__rgcz00, BgL_indexz00_1887);
								FAILURE(BgL_auxz00_2108, BFALSE, BFALSE);
							}
						BgL_auxz00_2104 = (long) CINT(BgL_tmpz00_2105);
					}
					if (INPUT_PORTP(BgL_inputzd2portzd2_1886))
						{	/* Rgc/rgc.scm 242 */
							BgL_auxz00_2097 = BgL_inputzd2portzd2_1886;
						}
					else
						{
							obj_t BgL_auxz00_2100;

							BgL_auxz00_2100 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
								BINT(11343L), BGl_string1702z00zz__rgcz00,
								BGl_string1703z00zz__rgcz00, BgL_inputzd2portzd2_1886);
							FAILURE(BgL_auxz00_2100, BFALSE, BFALSE);
						}
					BgL_tmpz00_2096 =
						BGl_rgczd2bufferzd2getzd2charzd2zz__rgcz00(BgL_auxz00_2097,
						BgL_auxz00_2104);
				}
				return BINT(BgL_tmpz00_2096);
			}
		}

	}



/* rgc-buffer-unget-char */
	BGL_EXPORTED_DEF int BGl_rgczd2bufferzd2ungetzd2charzd2zz__rgcz00(obj_t
		BgL_inputzd2portzd2_5, int BgL_charz00_6)
	{
		{	/* Rgc/rgc.scm 247 */
			BGL_TAIL return
				rgc_buffer_unget_char(BgL_inputzd2portzd2_5, BgL_charz00_6);
		}

	}



/* &rgc-buffer-unget-char */
	obj_t BGl_z62rgczd2bufferzd2ungetzd2charzb0zz__rgcz00(obj_t BgL_envz00_1888,
		obj_t BgL_inputzd2portzd2_1889, obj_t BgL_charz00_1890)
	{
		{	/* Rgc/rgc.scm 247 */
			{	/* Rgc/rgc.scm 248 */
				int BgL_tmpz00_2116;

				{	/* Rgc/rgc.scm 248 */
					int BgL_auxz00_2124;
					obj_t BgL_auxz00_2117;

					{	/* Rgc/rgc.scm 248 */
						obj_t BgL_tmpz00_2125;

						if (INTEGERP(BgL_charz00_1890))
							{	/* Rgc/rgc.scm 248 */
								BgL_tmpz00_2125 = BgL_charz00_1890;
							}
						else
							{
								obj_t BgL_auxz00_2128;

								BgL_auxz00_2128 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
									BINT(11665L), BGl_string1705z00zz__rgcz00,
									BGl_string1704z00zz__rgcz00, BgL_charz00_1890);
								FAILURE(BgL_auxz00_2128, BFALSE, BFALSE);
							}
						BgL_auxz00_2124 = CINT(BgL_tmpz00_2125);
					}
					if (INPUT_PORTP(BgL_inputzd2portzd2_1889))
						{	/* Rgc/rgc.scm 248 */
							BgL_auxz00_2117 = BgL_inputzd2portzd2_1889;
						}
					else
						{
							obj_t BgL_auxz00_2120;

							BgL_auxz00_2120 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
								BINT(11665L), BGl_string1705z00zz__rgcz00,
								BGl_string1703z00zz__rgcz00, BgL_inputzd2portzd2_1889);
							FAILURE(BgL_auxz00_2120, BFALSE, BFALSE);
						}
					BgL_tmpz00_2116 =
						BGl_rgczd2bufferzd2ungetzd2charzd2zz__rgcz00(BgL_auxz00_2117,
						BgL_auxz00_2124);
				}
				return BINT(BgL_tmpz00_2116);
			}
		}

	}



/* rgc-buffer-insert-substring! */
	BGL_EXPORTED_DEF bool_t
		BGl_rgczd2bufferzd2insertzd2substringz12zc0zz__rgcz00(obj_t
		BgL_inputzd2portzd2_7, obj_t BgL_strz00_8, long BgL_fromz00_9,
		long BgL_toz00_10)
	{
		{	/* Rgc/rgc.scm 253 */
			BGL_TAIL return
				rgc_buffer_insert_substring(BgL_inputzd2portzd2_7, BgL_strz00_8,
				BgL_fromz00_9, BgL_toz00_10);
		}

	}



/* &rgc-buffer-insert-substring! */
	obj_t BGl_z62rgczd2bufferzd2insertzd2substringz12za2zz__rgcz00(obj_t
		BgL_envz00_1891, obj_t BgL_inputzd2portzd2_1892, obj_t BgL_strz00_1893,
		obj_t BgL_fromz00_1894, obj_t BgL_toz00_1895)
	{
		{	/* Rgc/rgc.scm 253 */
			{	/* Rgc/rgc.scm 254 */
				bool_t BgL_tmpz00_2136;

				{	/* Rgc/rgc.scm 254 */
					long BgL_auxz00_2160;
					long BgL_auxz00_2151;
					obj_t BgL_auxz00_2144;
					obj_t BgL_auxz00_2137;

					{	/* Rgc/rgc.scm 254 */
						obj_t BgL_tmpz00_2161;

						if (INTEGERP(BgL_toz00_1895))
							{	/* Rgc/rgc.scm 254 */
								BgL_tmpz00_2161 = BgL_toz00_1895;
							}
						else
							{
								obj_t BgL_auxz00_2164;

								BgL_auxz00_2164 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
									BINT(12002L), BGl_string1706z00zz__rgcz00,
									BGl_string1704z00zz__rgcz00, BgL_toz00_1895);
								FAILURE(BgL_auxz00_2164, BFALSE, BFALSE);
							}
						BgL_auxz00_2160 = (long) CINT(BgL_tmpz00_2161);
					}
					{	/* Rgc/rgc.scm 254 */
						obj_t BgL_tmpz00_2152;

						if (INTEGERP(BgL_fromz00_1894))
							{	/* Rgc/rgc.scm 254 */
								BgL_tmpz00_2152 = BgL_fromz00_1894;
							}
						else
							{
								obj_t BgL_auxz00_2155;

								BgL_auxz00_2155 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
									BINT(12002L), BGl_string1706z00zz__rgcz00,
									BGl_string1704z00zz__rgcz00, BgL_fromz00_1894);
								FAILURE(BgL_auxz00_2155, BFALSE, BFALSE);
							}
						BgL_auxz00_2151 = (long) CINT(BgL_tmpz00_2152);
					}
					if (STRINGP(BgL_strz00_1893))
						{	/* Rgc/rgc.scm 254 */
							BgL_auxz00_2144 = BgL_strz00_1893;
						}
					else
						{
							obj_t BgL_auxz00_2147;

							BgL_auxz00_2147 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
								BINT(12002L), BGl_string1706z00zz__rgcz00,
								BGl_string1707z00zz__rgcz00, BgL_strz00_1893);
							FAILURE(BgL_auxz00_2147, BFALSE, BFALSE);
						}
					if (INPUT_PORTP(BgL_inputzd2portzd2_1892))
						{	/* Rgc/rgc.scm 254 */
							BgL_auxz00_2137 = BgL_inputzd2portzd2_1892;
						}
					else
						{
							obj_t BgL_auxz00_2140;

							BgL_auxz00_2140 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
								BINT(12002L), BGl_string1706z00zz__rgcz00,
								BGl_string1703z00zz__rgcz00, BgL_inputzd2portzd2_1892);
							FAILURE(BgL_auxz00_2140, BFALSE, BFALSE);
						}
					BgL_tmpz00_2136 =
						BGl_rgczd2bufferzd2insertzd2substringz12zc0zz__rgcz00
						(BgL_auxz00_2137, BgL_auxz00_2144, BgL_auxz00_2151,
						BgL_auxz00_2160);
				}
				return BBOOL(BgL_tmpz00_2136);
			}
		}

	}



/* rgc-buffer-insert-char! */
	BGL_EXPORTED_DEF bool_t BGl_rgczd2bufferzd2insertzd2charz12zc0zz__rgcz00(obj_t
		BgL_inputzd2portzd2_11, long BgL_charz00_12)
	{
		{	/* Rgc/rgc.scm 259 */
			return
				rgc_buffer_insert_char(BgL_inputzd2portzd2_11, (int) (BgL_charz00_12));
		}

	}



/* &rgc-buffer-insert-char! */
	obj_t BGl_z62rgczd2bufferzd2insertzd2charz12za2zz__rgcz00(obj_t
		BgL_envz00_1896, obj_t BgL_inputzd2portzd2_1897, obj_t BgL_charz00_1898)
	{
		{	/* Rgc/rgc.scm 259 */
			{	/* Rgc/rgc.scm 260 */
				bool_t BgL_tmpz00_2173;

				{	/* Rgc/rgc.scm 260 */
					long BgL_auxz00_2181;
					obj_t BgL_auxz00_2174;

					{	/* Rgc/rgc.scm 260 */
						obj_t BgL_tmpz00_2182;

						if (INTEGERP(BgL_charz00_1898))
							{	/* Rgc/rgc.scm 260 */
								BgL_tmpz00_2182 = BgL_charz00_1898;
							}
						else
							{
								obj_t BgL_auxz00_2185;

								BgL_auxz00_2185 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
									BINT(12340L), BGl_string1708z00zz__rgcz00,
									BGl_string1704z00zz__rgcz00, BgL_charz00_1898);
								FAILURE(BgL_auxz00_2185, BFALSE, BFALSE);
							}
						BgL_auxz00_2181 = (long) CINT(BgL_tmpz00_2182);
					}
					if (INPUT_PORTP(BgL_inputzd2portzd2_1897))
						{	/* Rgc/rgc.scm 260 */
							BgL_auxz00_2174 = BgL_inputzd2portzd2_1897;
						}
					else
						{
							obj_t BgL_auxz00_2177;

							BgL_auxz00_2177 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
								BINT(12340L), BGl_string1708z00zz__rgcz00,
								BGl_string1703z00zz__rgcz00, BgL_inputzd2portzd2_1897);
							FAILURE(BgL_auxz00_2177, BFALSE, BFALSE);
						}
					BgL_tmpz00_2173 =
						BGl_rgczd2bufferzd2insertzd2charz12zc0zz__rgcz00(BgL_auxz00_2174,
						BgL_auxz00_2181);
				}
				return BBOOL(BgL_tmpz00_2173);
			}
		}

	}



/* rgc-buffer-character */
	BGL_EXPORTED_DEF unsigned char BGl_rgczd2bufferzd2characterz00zz__rgcz00(obj_t
		BgL_inputzd2portzd2_13)
	{
		{	/* Rgc/rgc.scm 265 */
			return RGC_BUFFER_CHARACTER(BgL_inputzd2portzd2_13);
		}

	}



/* &rgc-buffer-character */
	obj_t BGl_z62rgczd2bufferzd2characterz62zz__rgcz00(obj_t BgL_envz00_1899,
		obj_t BgL_inputzd2portzd2_1900)
	{
		{	/* Rgc/rgc.scm 265 */
			{	/* Rgc/rgc.scm 266 */
				unsigned char BgL_tmpz00_2193;

				{	/* Rgc/rgc.scm 266 */
					obj_t BgL_auxz00_2194;

					if (INPUT_PORTP(BgL_inputzd2portzd2_1900))
						{	/* Rgc/rgc.scm 266 */
							BgL_auxz00_2194 = BgL_inputzd2portzd2_1900;
						}
					else
						{
							obj_t BgL_auxz00_2197;

							BgL_auxz00_2197 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
								BINT(12658L), BGl_string1709z00zz__rgcz00,
								BGl_string1703z00zz__rgcz00, BgL_inputzd2portzd2_1900);
							FAILURE(BgL_auxz00_2197, BFALSE, BFALSE);
						}
					BgL_tmpz00_2193 =
						BGl_rgczd2bufferzd2characterz00zz__rgcz00(BgL_auxz00_2194);
				}
				return BCHAR(BgL_tmpz00_2193);
			}
		}

	}



/* rgc-buffer-byte */
	BGL_EXPORTED_DEF int BGl_rgczd2bufferzd2bytez00zz__rgcz00(obj_t
		BgL_inputzd2portzd2_14)
	{
		{	/* Rgc/rgc.scm 271 */
			return RGC_BUFFER_BYTE(BgL_inputzd2portzd2_14);
		}

	}



/* &rgc-buffer-byte */
	obj_t BGl_z62rgczd2bufferzd2bytez62zz__rgcz00(obj_t BgL_envz00_1901,
		obj_t BgL_inputzd2portzd2_1902)
	{
		{	/* Rgc/rgc.scm 271 */
			{	/* Rgc/rgc.scm 272 */
				int BgL_tmpz00_2204;

				{	/* Rgc/rgc.scm 272 */
					obj_t BgL_auxz00_2205;

					if (INPUT_PORTP(BgL_inputzd2portzd2_1902))
						{	/* Rgc/rgc.scm 272 */
							BgL_auxz00_2205 = BgL_inputzd2portzd2_1902;
						}
					else
						{
							obj_t BgL_auxz00_2208;

							BgL_auxz00_2208 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
								BINT(12964L), BGl_string1710z00zz__rgcz00,
								BGl_string1703z00zz__rgcz00, BgL_inputzd2portzd2_1902);
							FAILURE(BgL_auxz00_2208, BFALSE, BFALSE);
						}
					BgL_tmpz00_2204 =
						BGl_rgczd2bufferzd2bytez00zz__rgcz00(BgL_auxz00_2205);
				}
				return BINT(BgL_tmpz00_2204);
			}
		}

	}



/* rgc-buffer-byte-ref */
	BGL_EXPORTED_DEF int BGl_rgczd2bufferzd2bytezd2refzd2zz__rgcz00(obj_t
		BgL_inputzd2portzd2_15, int BgL_offsetz00_16)
	{
		{	/* Rgc/rgc.scm 277 */
			return RGC_BUFFER_BYTE_REF(BgL_inputzd2portzd2_15, BgL_offsetz00_16);
		}

	}



/* &rgc-buffer-byte-ref */
	obj_t BGl_z62rgczd2bufferzd2bytezd2refzb0zz__rgcz00(obj_t BgL_envz00_1903,
		obj_t BgL_inputzd2portzd2_1904, obj_t BgL_offsetz00_1905)
	{
		{	/* Rgc/rgc.scm 277 */
			{	/* Rgc/rgc.scm 278 */
				int BgL_tmpz00_2215;

				{	/* Rgc/rgc.scm 278 */
					int BgL_auxz00_2223;
					obj_t BgL_auxz00_2216;

					{	/* Rgc/rgc.scm 278 */
						obj_t BgL_tmpz00_2224;

						if (INTEGERP(BgL_offsetz00_1905))
							{	/* Rgc/rgc.scm 278 */
								BgL_tmpz00_2224 = BgL_offsetz00_1905;
							}
						else
							{
								obj_t BgL_auxz00_2227;

								BgL_auxz00_2227 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
									BINT(13276L), BGl_string1711z00zz__rgcz00,
									BGl_string1704z00zz__rgcz00, BgL_offsetz00_1905);
								FAILURE(BgL_auxz00_2227, BFALSE, BFALSE);
							}
						BgL_auxz00_2223 = CINT(BgL_tmpz00_2224);
					}
					if (INPUT_PORTP(BgL_inputzd2portzd2_1904))
						{	/* Rgc/rgc.scm 278 */
							BgL_auxz00_2216 = BgL_inputzd2portzd2_1904;
						}
					else
						{
							obj_t BgL_auxz00_2219;

							BgL_auxz00_2219 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
								BINT(13276L), BGl_string1711z00zz__rgcz00,
								BGl_string1703z00zz__rgcz00, BgL_inputzd2portzd2_1904);
							FAILURE(BgL_auxz00_2219, BFALSE, BFALSE);
						}
					BgL_tmpz00_2215 =
						BGl_rgczd2bufferzd2bytezd2refzd2zz__rgcz00(BgL_auxz00_2216,
						BgL_auxz00_2223);
				}
				return BINT(BgL_tmpz00_2215);
			}
		}

	}



/* rgc-buffer-substring */
	BGL_EXPORTED_DEF obj_t BGl_rgczd2bufferzd2substringz00zz__rgcz00(obj_t
		BgL_inputzd2portzd2_17, long BgL_startz00_18, long BgL_stopz00_19)
	{
		{	/* Rgc/rgc.scm 283 */
			BGL_TAIL return
				rgc_buffer_substring(BgL_inputzd2portzd2_17, BgL_startz00_18,
				BgL_stopz00_19);
		}

	}



/* &rgc-buffer-substring */
	obj_t BGl_z62rgczd2bufferzd2substringz62zz__rgcz00(obj_t BgL_envz00_1906,
		obj_t BgL_inputzd2portzd2_1907, obj_t BgL_startz00_1908,
		obj_t BgL_stopz00_1909)
	{
		{	/* Rgc/rgc.scm 283 */
			{	/* Rgc/rgc.scm 284 */
				long BgL_auxz00_2251;
				long BgL_auxz00_2242;
				obj_t BgL_auxz00_2235;

				{	/* Rgc/rgc.scm 284 */
					obj_t BgL_tmpz00_2252;

					if (INTEGERP(BgL_stopz00_1909))
						{	/* Rgc/rgc.scm 284 */
							BgL_tmpz00_2252 = BgL_stopz00_1909;
						}
					else
						{
							obj_t BgL_auxz00_2255;

							BgL_auxz00_2255 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
								BINT(13604L), BGl_string1712z00zz__rgcz00,
								BGl_string1704z00zz__rgcz00, BgL_stopz00_1909);
							FAILURE(BgL_auxz00_2255, BFALSE, BFALSE);
						}
					BgL_auxz00_2251 = (long) CINT(BgL_tmpz00_2252);
				}
				{	/* Rgc/rgc.scm 284 */
					obj_t BgL_tmpz00_2243;

					if (INTEGERP(BgL_startz00_1908))
						{	/* Rgc/rgc.scm 284 */
							BgL_tmpz00_2243 = BgL_startz00_1908;
						}
					else
						{
							obj_t BgL_auxz00_2246;

							BgL_auxz00_2246 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
								BINT(13604L), BGl_string1712z00zz__rgcz00,
								BGl_string1704z00zz__rgcz00, BgL_startz00_1908);
							FAILURE(BgL_auxz00_2246, BFALSE, BFALSE);
						}
					BgL_auxz00_2242 = (long) CINT(BgL_tmpz00_2243);
				}
				if (INPUT_PORTP(BgL_inputzd2portzd2_1907))
					{	/* Rgc/rgc.scm 284 */
						BgL_auxz00_2235 = BgL_inputzd2portzd2_1907;
					}
				else
					{
						obj_t BgL_auxz00_2238;

						BgL_auxz00_2238 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
							BINT(13604L), BGl_string1712z00zz__rgcz00,
							BGl_string1703z00zz__rgcz00, BgL_inputzd2portzd2_1907);
						FAILURE(BgL_auxz00_2238, BFALSE, BFALSE);
					}
				return
					BGl_rgczd2bufferzd2substringz00zz__rgcz00(BgL_auxz00_2235,
					BgL_auxz00_2242, BgL_auxz00_2251);
			}
		}

	}



/* rgc-buffer-escape-substring */
	BGL_EXPORTED_DEF obj_t
		BGl_rgczd2bufferzd2escapezd2substringzd2zz__rgcz00(obj_t
		BgL_inputzd2portzd2_20, long BgL_startz00_21, long BgL_stopz00_22,
		bool_t BgL_strictz00_23)
	{
		{	/* Rgc/rgc.scm 289 */
			BGL_TAIL return
				rgc_buffer_escape_substring(BgL_inputzd2portzd2_20, BgL_startz00_21,
				BgL_stopz00_22, BgL_strictz00_23);
		}

	}



/* &rgc-buffer-escape-substring */
	obj_t BGl_z62rgczd2bufferzd2escapezd2substringzb0zz__rgcz00(obj_t
		BgL_envz00_1910, obj_t BgL_inputzd2portzd2_1911, obj_t BgL_startz00_1912,
		obj_t BgL_stopz00_1913, obj_t BgL_strictz00_1914)
	{
		{	/* Rgc/rgc.scm 289 */
			{	/* Rgc/rgc.scm 290 */
				long BgL_auxz00_2278;
				long BgL_auxz00_2269;
				obj_t BgL_auxz00_2262;

				{	/* Rgc/rgc.scm 290 */
					obj_t BgL_tmpz00_2279;

					if (INTEGERP(BgL_stopz00_1913))
						{	/* Rgc/rgc.scm 290 */
							BgL_tmpz00_2279 = BgL_stopz00_1913;
						}
					else
						{
							obj_t BgL_auxz00_2282;

							BgL_auxz00_2282 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
								BINT(13951L), BGl_string1713z00zz__rgcz00,
								BGl_string1704z00zz__rgcz00, BgL_stopz00_1913);
							FAILURE(BgL_auxz00_2282, BFALSE, BFALSE);
						}
					BgL_auxz00_2278 = (long) CINT(BgL_tmpz00_2279);
				}
				{	/* Rgc/rgc.scm 290 */
					obj_t BgL_tmpz00_2270;

					if (INTEGERP(BgL_startz00_1912))
						{	/* Rgc/rgc.scm 290 */
							BgL_tmpz00_2270 = BgL_startz00_1912;
						}
					else
						{
							obj_t BgL_auxz00_2273;

							BgL_auxz00_2273 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
								BINT(13951L), BGl_string1713z00zz__rgcz00,
								BGl_string1704z00zz__rgcz00, BgL_startz00_1912);
							FAILURE(BgL_auxz00_2273, BFALSE, BFALSE);
						}
					BgL_auxz00_2269 = (long) CINT(BgL_tmpz00_2270);
				}
				if (INPUT_PORTP(BgL_inputzd2portzd2_1911))
					{	/* Rgc/rgc.scm 290 */
						BgL_auxz00_2262 = BgL_inputzd2portzd2_1911;
					}
				else
					{
						obj_t BgL_auxz00_2265;

						BgL_auxz00_2265 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
							BINT(13951L), BGl_string1713z00zz__rgcz00,
							BGl_string1703z00zz__rgcz00, BgL_inputzd2portzd2_1911);
						FAILURE(BgL_auxz00_2265, BFALSE, BFALSE);
					}
				return
					BGl_rgczd2bufferzd2escapezd2substringzd2zz__rgcz00(BgL_auxz00_2262,
					BgL_auxz00_2269, BgL_auxz00_2278, CBOOL(BgL_strictz00_1914));
			}
		}

	}



/* rgc-buffer-length */
	BGL_EXPORTED_DEF long BGl_rgczd2bufferzd2lengthz00zz__rgcz00(obj_t
		BgL_inputzd2portzd2_24)
	{
		{	/* Rgc/rgc.scm 295 */
			return RGC_BUFFER_MATCH_LENGTH(BgL_inputzd2portzd2_24);
		}

	}



/* &rgc-buffer-length */
	obj_t BGl_z62rgczd2bufferzd2lengthz62zz__rgcz00(obj_t BgL_envz00_1915,
		obj_t BgL_inputzd2portzd2_1916)
	{
		{	/* Rgc/rgc.scm 295 */
			{	/* Rgc/rgc.scm 296 */
				long BgL_tmpz00_2290;

				{	/* Rgc/rgc.scm 296 */
					obj_t BgL_auxz00_2291;

					if (INPUT_PORTP(BgL_inputzd2portzd2_1916))
						{	/* Rgc/rgc.scm 296 */
							BgL_auxz00_2291 = BgL_inputzd2portzd2_1916;
						}
					else
						{
							obj_t BgL_auxz00_2294;

							BgL_auxz00_2294 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
								BINT(14302L), BGl_string1714z00zz__rgcz00,
								BGl_string1703z00zz__rgcz00, BgL_inputzd2portzd2_1916);
							FAILURE(BgL_auxz00_2294, BFALSE, BFALSE);
						}
					BgL_tmpz00_2290 =
						BGl_rgczd2bufferzd2lengthz00zz__rgcz00(BgL_auxz00_2291);
				}
				return BINT(BgL_tmpz00_2290);
			}
		}

	}



/* rgc-buffer-fixnum */
	BGL_EXPORTED_DEF long BGl_rgczd2bufferzd2fixnumz00zz__rgcz00(obj_t
		BgL_inputzd2portzd2_25)
	{
		{	/* Rgc/rgc.scm 301 */
			BGL_TAIL return rgc_buffer_fixnum(BgL_inputzd2portzd2_25);
		}

	}



/* &rgc-buffer-fixnum */
	obj_t BGl_z62rgczd2bufferzd2fixnumz62zz__rgcz00(obj_t BgL_envz00_1917,
		obj_t BgL_inputzd2portzd2_1918)
	{
		{	/* Rgc/rgc.scm 301 */
			{	/* Rgc/rgc.scm 302 */
				long BgL_tmpz00_2301;

				{	/* Rgc/rgc.scm 302 */
					obj_t BgL_auxz00_2302;

					if (INPUT_PORTP(BgL_inputzd2portzd2_1918))
						{	/* Rgc/rgc.scm 302 */
							BgL_auxz00_2302 = BgL_inputzd2portzd2_1918;
						}
					else
						{
							obj_t BgL_auxz00_2305;

							BgL_auxz00_2305 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
								BINT(14625L), BGl_string1715z00zz__rgcz00,
								BGl_string1703z00zz__rgcz00, BgL_inputzd2portzd2_1918);
							FAILURE(BgL_auxz00_2305, BFALSE, BFALSE);
						}
					BgL_tmpz00_2301 =
						BGl_rgczd2bufferzd2fixnumz00zz__rgcz00(BgL_auxz00_2302);
				}
				return BINT(BgL_tmpz00_2301);
			}
		}

	}



/* rgc-buffer-integer */
	BGL_EXPORTED_DEF obj_t BGl_rgczd2bufferzd2integerz00zz__rgcz00(obj_t
		BgL_inputzd2portzd2_26)
	{
		{	/* Rgc/rgc.scm 307 */
			BGL_TAIL return rgc_buffer_integer(BgL_inputzd2portzd2_26);
		}

	}



/* &rgc-buffer-integer */
	obj_t BGl_z62rgczd2bufferzd2integerz62zz__rgcz00(obj_t BgL_envz00_1919,
		obj_t BgL_inputzd2portzd2_1920)
	{
		{	/* Rgc/rgc.scm 307 */
			{	/* Rgc/rgc.scm 308 */
				obj_t BgL_auxz00_2312;

				if (INPUT_PORTP(BgL_inputzd2portzd2_1920))
					{	/* Rgc/rgc.scm 308 */
						BgL_auxz00_2312 = BgL_inputzd2portzd2_1920;
					}
				else
					{
						obj_t BgL_auxz00_2315;

						BgL_auxz00_2315 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
							BINT(14948L), BGl_string1716z00zz__rgcz00,
							BGl_string1703z00zz__rgcz00, BgL_inputzd2portzd2_1920);
						FAILURE(BgL_auxz00_2315, BFALSE, BFALSE);
					}
				return BGl_rgczd2bufferzd2integerz00zz__rgcz00(BgL_auxz00_2312);
			}
		}

	}



/* rgc-buffer-flonum */
	BGL_EXPORTED_DEF double BGl_rgczd2bufferzd2flonumz00zz__rgcz00(obj_t
		BgL_inputzd2portzd2_27)
	{
		{	/* Rgc/rgc.scm 313 */
			BGL_TAIL return rgc_buffer_flonum(BgL_inputzd2portzd2_27);
		}

	}



/* &rgc-buffer-flonum */
	obj_t BGl_z62rgczd2bufferzd2flonumz62zz__rgcz00(obj_t BgL_envz00_1921,
		obj_t BgL_inputzd2portzd2_1922)
	{
		{	/* Rgc/rgc.scm 313 */
			{	/* Rgc/rgc.scm 314 */
				double BgL_tmpz00_2321;

				{	/* Rgc/rgc.scm 314 */
					obj_t BgL_auxz00_2322;

					if (INPUT_PORTP(BgL_inputzd2portzd2_1922))
						{	/* Rgc/rgc.scm 314 */
							BgL_auxz00_2322 = BgL_inputzd2portzd2_1922;
						}
					else
						{
							obj_t BgL_auxz00_2325;

							BgL_auxz00_2325 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
								BINT(15274L), BGl_string1717z00zz__rgcz00,
								BGl_string1703z00zz__rgcz00, BgL_inputzd2portzd2_1922);
							FAILURE(BgL_auxz00_2325, BFALSE, BFALSE);
						}
					BgL_tmpz00_2321 =
						BGl_rgczd2bufferzd2flonumz00zz__rgcz00(BgL_auxz00_2322);
				}
				return DOUBLE_TO_REAL(BgL_tmpz00_2321);
			}
		}

	}



/* rgc-buffer-symbol */
	BGL_EXPORTED_DEF obj_t BGl_rgczd2bufferzd2symbolz00zz__rgcz00(obj_t
		BgL_inputzd2portzd2_28)
	{
		{	/* Rgc/rgc.scm 319 */
			BGL_TAIL return rgc_buffer_symbol(BgL_inputzd2portzd2_28);
		}

	}



/* &rgc-buffer-symbol */
	obj_t BGl_z62rgczd2bufferzd2symbolz62zz__rgcz00(obj_t BgL_envz00_1923,
		obj_t BgL_inputzd2portzd2_1924)
	{
		{	/* Rgc/rgc.scm 319 */
			{	/* Rgc/rgc.scm 320 */
				obj_t BgL_auxz00_2332;

				if (INPUT_PORTP(BgL_inputzd2portzd2_1924))
					{	/* Rgc/rgc.scm 320 */
						BgL_auxz00_2332 = BgL_inputzd2portzd2_1924;
					}
				else
					{
						obj_t BgL_auxz00_2335;

						BgL_auxz00_2335 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
							BINT(15599L), BGl_string1718z00zz__rgcz00,
							BGl_string1703z00zz__rgcz00, BgL_inputzd2portzd2_1924);
						FAILURE(BgL_auxz00_2335, BFALSE, BFALSE);
					}
				return BGl_rgczd2bufferzd2symbolz00zz__rgcz00(BgL_auxz00_2332);
			}
		}

	}



/* rgc-buffer-subsymbol */
	BGL_EXPORTED_DEF obj_t BGl_rgczd2bufferzd2subsymbolz00zz__rgcz00(obj_t
		BgL_inputzd2portzd2_29, long BgL_startz00_30, long BgL_stopz00_31)
	{
		{	/* Rgc/rgc.scm 325 */
			BGL_TAIL return
				rgc_buffer_subsymbol(BgL_inputzd2portzd2_29, BgL_startz00_30,
				BgL_stopz00_31);
		}

	}



/* &rgc-buffer-subsymbol */
	obj_t BGl_z62rgczd2bufferzd2subsymbolz62zz__rgcz00(obj_t BgL_envz00_1925,
		obj_t BgL_inputzd2portzd2_1926, obj_t BgL_startz00_1927,
		obj_t BgL_stopz00_1928)
	{
		{	/* Rgc/rgc.scm 325 */
			{	/* Rgc/rgc.scm 326 */
				long BgL_auxz00_2357;
				long BgL_auxz00_2348;
				obj_t BgL_auxz00_2341;

				{	/* Rgc/rgc.scm 326 */
					obj_t BgL_tmpz00_2358;

					if (INTEGERP(BgL_stopz00_1928))
						{	/* Rgc/rgc.scm 326 */
							BgL_tmpz00_2358 = BgL_stopz00_1928;
						}
					else
						{
							obj_t BgL_auxz00_2361;

							BgL_auxz00_2361 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
								BINT(15918L), BGl_string1719z00zz__rgcz00,
								BGl_string1704z00zz__rgcz00, BgL_stopz00_1928);
							FAILURE(BgL_auxz00_2361, BFALSE, BFALSE);
						}
					BgL_auxz00_2357 = (long) CINT(BgL_tmpz00_2358);
				}
				{	/* Rgc/rgc.scm 326 */
					obj_t BgL_tmpz00_2349;

					if (INTEGERP(BgL_startz00_1927))
						{	/* Rgc/rgc.scm 326 */
							BgL_tmpz00_2349 = BgL_startz00_1927;
						}
					else
						{
							obj_t BgL_auxz00_2352;

							BgL_auxz00_2352 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
								BINT(15918L), BGl_string1719z00zz__rgcz00,
								BGl_string1704z00zz__rgcz00, BgL_startz00_1927);
							FAILURE(BgL_auxz00_2352, BFALSE, BFALSE);
						}
					BgL_auxz00_2348 = (long) CINT(BgL_tmpz00_2349);
				}
				if (INPUT_PORTP(BgL_inputzd2portzd2_1926))
					{	/* Rgc/rgc.scm 326 */
						BgL_auxz00_2341 = BgL_inputzd2portzd2_1926;
					}
				else
					{
						obj_t BgL_auxz00_2344;

						BgL_auxz00_2344 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
							BINT(15918L), BGl_string1719z00zz__rgcz00,
							BGl_string1703z00zz__rgcz00, BgL_inputzd2portzd2_1926);
						FAILURE(BgL_auxz00_2344, BFALSE, BFALSE);
					}
				return
					BGl_rgczd2bufferzd2subsymbolz00zz__rgcz00(BgL_auxz00_2341,
					BgL_auxz00_2348, BgL_auxz00_2357);
			}
		}

	}



/* rgc-buffer-downcase-symbol */
	BGL_EXPORTED_DEF obj_t BGl_rgczd2bufferzd2downcasezd2symbolzd2zz__rgcz00(obj_t
		BgL_inputzd2portzd2_32)
	{
		{	/* Rgc/rgc.scm 331 */
			{	/* Rgc/rgc.scm 332 */
				long BgL_arg1191z00_2080;

				BgL_arg1191z00_2080 = RGC_BUFFER_MATCH_LENGTH(BgL_inputzd2portzd2_32);
				return
					rgc_buffer_downcase_subsymbol(BgL_inputzd2portzd2_32, 0L,
					BgL_arg1191z00_2080);
			}
		}

	}



/* &rgc-buffer-downcase-symbol */
	obj_t BGl_z62rgczd2bufferzd2downcasezd2symbolzb0zz__rgcz00(obj_t
		BgL_envz00_1929, obj_t BgL_inputzd2portzd2_1930)
	{
		{	/* Rgc/rgc.scm 331 */
			{	/* Rgc/rgc.scm 332 */
				obj_t BgL_auxz00_2369;

				if (INPUT_PORTP(BgL_inputzd2portzd2_1930))
					{	/* Rgc/rgc.scm 332 */
						BgL_auxz00_2369 = BgL_inputzd2portzd2_1930;
					}
				else
					{
						obj_t BgL_auxz00_2372;

						BgL_auxz00_2372 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
							BINT(16311L), BGl_string1720z00zz__rgcz00,
							BGl_string1703z00zz__rgcz00, BgL_inputzd2portzd2_1930);
						FAILURE(BgL_auxz00_2372, BFALSE, BFALSE);
					}
				return
					BGl_rgczd2bufferzd2downcasezd2symbolzd2zz__rgcz00(BgL_auxz00_2369);
			}
		}

	}



/* rgc-buffer-downcase-subsymbol */
	BGL_EXPORTED_DEF obj_t
		BGl_rgczd2bufferzd2downcasezd2subsymbolzd2zz__rgcz00(obj_t
		BgL_inputzd2portzd2_33, long BgL_startz00_34, long BgL_stopz00_35)
	{
		{	/* Rgc/rgc.scm 337 */
			BGL_TAIL return
				rgc_buffer_downcase_subsymbol(BgL_inputzd2portzd2_33, BgL_startz00_34,
				BgL_stopz00_35);
		}

	}



/* &rgc-buffer-downcase-subsymbol */
	obj_t BGl_z62rgczd2bufferzd2downcasezd2subsymbolzb0zz__rgcz00(obj_t
		BgL_envz00_1931, obj_t BgL_inputzd2portzd2_1932, obj_t BgL_startz00_1933,
		obj_t BgL_stopz00_1934)
	{
		{	/* Rgc/rgc.scm 337 */
			{	/* Rgc/rgc.scm 338 */
				long BgL_auxz00_2394;
				long BgL_auxz00_2385;
				obj_t BgL_auxz00_2378;

				{	/* Rgc/rgc.scm 338 */
					obj_t BgL_tmpz00_2395;

					if (INTEGERP(BgL_stopz00_1934))
						{	/* Rgc/rgc.scm 338 */
							BgL_tmpz00_2395 = BgL_stopz00_1934;
						}
					else
						{
							obj_t BgL_auxz00_2398;

							BgL_auxz00_2398 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
								BINT(16651L), BGl_string1721z00zz__rgcz00,
								BGl_string1704z00zz__rgcz00, BgL_stopz00_1934);
							FAILURE(BgL_auxz00_2398, BFALSE, BFALSE);
						}
					BgL_auxz00_2394 = (long) CINT(BgL_tmpz00_2395);
				}
				{	/* Rgc/rgc.scm 338 */
					obj_t BgL_tmpz00_2386;

					if (INTEGERP(BgL_startz00_1933))
						{	/* Rgc/rgc.scm 338 */
							BgL_tmpz00_2386 = BgL_startz00_1933;
						}
					else
						{
							obj_t BgL_auxz00_2389;

							BgL_auxz00_2389 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
								BINT(16651L), BGl_string1721z00zz__rgcz00,
								BGl_string1704z00zz__rgcz00, BgL_startz00_1933);
							FAILURE(BgL_auxz00_2389, BFALSE, BFALSE);
						}
					BgL_auxz00_2385 = (long) CINT(BgL_tmpz00_2386);
				}
				if (INPUT_PORTP(BgL_inputzd2portzd2_1932))
					{	/* Rgc/rgc.scm 338 */
						BgL_auxz00_2378 = BgL_inputzd2portzd2_1932;
					}
				else
					{
						obj_t BgL_auxz00_2381;

						BgL_auxz00_2381 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
							BINT(16651L), BGl_string1721z00zz__rgcz00,
							BGl_string1703z00zz__rgcz00, BgL_inputzd2portzd2_1932);
						FAILURE(BgL_auxz00_2381, BFALSE, BFALSE);
					}
				return
					BGl_rgczd2bufferzd2downcasezd2subsymbolzd2zz__rgcz00(BgL_auxz00_2378,
					BgL_auxz00_2385, BgL_auxz00_2394);
			}
		}

	}



/* rgc-buffer-upcase-symbol */
	BGL_EXPORTED_DEF obj_t BGl_rgczd2bufferzd2upcasezd2symbolzd2zz__rgcz00(obj_t
		BgL_inputzd2portzd2_36)
	{
		{	/* Rgc/rgc.scm 343 */
			{	/* Rgc/rgc.scm 344 */
				long BgL_arg1193z00_2081;

				BgL_arg1193z00_2081 = RGC_BUFFER_MATCH_LENGTH(BgL_inputzd2portzd2_36);
				return
					rgc_buffer_upcase_subsymbol(BgL_inputzd2portzd2_36, 0L,
					BgL_arg1193z00_2081);
			}
		}

	}



/* &rgc-buffer-upcase-symbol */
	obj_t BGl_z62rgczd2bufferzd2upcasezd2symbolzb0zz__rgcz00(obj_t
		BgL_envz00_1935, obj_t BgL_inputzd2portzd2_1936)
	{
		{	/* Rgc/rgc.scm 343 */
			{	/* Rgc/rgc.scm 344 */
				obj_t BgL_auxz00_2406;

				if (INPUT_PORTP(BgL_inputzd2portzd2_1936))
					{	/* Rgc/rgc.scm 344 */
						BgL_auxz00_2406 = BgL_inputzd2portzd2_1936;
					}
				else
					{
						obj_t BgL_auxz00_2409;

						BgL_auxz00_2409 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
							BINT(17049L), BGl_string1722z00zz__rgcz00,
							BGl_string1703z00zz__rgcz00, BgL_inputzd2portzd2_1936);
						FAILURE(BgL_auxz00_2409, BFALSE, BFALSE);
					}
				return BGl_rgczd2bufferzd2upcasezd2symbolzd2zz__rgcz00(BgL_auxz00_2406);
			}
		}

	}



/* rgc-buffer-upcase-subsymbol */
	BGL_EXPORTED_DEF obj_t
		BGl_rgczd2bufferzd2upcasezd2subsymbolzd2zz__rgcz00(obj_t
		BgL_inputzd2portzd2_37, long BgL_startz00_38, long BgL_stopz00_39)
	{
		{	/* Rgc/rgc.scm 349 */
			BGL_TAIL return
				rgc_buffer_upcase_subsymbol(BgL_inputzd2portzd2_37, BgL_startz00_38,
				BgL_stopz00_39);
		}

	}



/* &rgc-buffer-upcase-subsymbol */
	obj_t BGl_z62rgczd2bufferzd2upcasezd2subsymbolzb0zz__rgcz00(obj_t
		BgL_envz00_1937, obj_t BgL_inputzd2portzd2_1938, obj_t BgL_startz00_1939,
		obj_t BgL_stopz00_1940)
	{
		{	/* Rgc/rgc.scm 349 */
			{	/* Rgc/rgc.scm 350 */
				long BgL_auxz00_2431;
				long BgL_auxz00_2422;
				obj_t BgL_auxz00_2415;

				{	/* Rgc/rgc.scm 350 */
					obj_t BgL_tmpz00_2432;

					if (INTEGERP(BgL_stopz00_1940))
						{	/* Rgc/rgc.scm 350 */
							BgL_tmpz00_2432 = BgL_stopz00_1940;
						}
					else
						{
							obj_t BgL_auxz00_2435;

							BgL_auxz00_2435 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
								BINT(17387L), BGl_string1723z00zz__rgcz00,
								BGl_string1704z00zz__rgcz00, BgL_stopz00_1940);
							FAILURE(BgL_auxz00_2435, BFALSE, BFALSE);
						}
					BgL_auxz00_2431 = (long) CINT(BgL_tmpz00_2432);
				}
				{	/* Rgc/rgc.scm 350 */
					obj_t BgL_tmpz00_2423;

					if (INTEGERP(BgL_startz00_1939))
						{	/* Rgc/rgc.scm 350 */
							BgL_tmpz00_2423 = BgL_startz00_1939;
						}
					else
						{
							obj_t BgL_auxz00_2426;

							BgL_auxz00_2426 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
								BINT(17387L), BGl_string1723z00zz__rgcz00,
								BGl_string1704z00zz__rgcz00, BgL_startz00_1939);
							FAILURE(BgL_auxz00_2426, BFALSE, BFALSE);
						}
					BgL_auxz00_2422 = (long) CINT(BgL_tmpz00_2423);
				}
				if (INPUT_PORTP(BgL_inputzd2portzd2_1938))
					{	/* Rgc/rgc.scm 350 */
						BgL_auxz00_2415 = BgL_inputzd2portzd2_1938;
					}
				else
					{
						obj_t BgL_auxz00_2418;

						BgL_auxz00_2418 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
							BINT(17387L), BGl_string1723z00zz__rgcz00,
							BGl_string1703z00zz__rgcz00, BgL_inputzd2portzd2_1938);
						FAILURE(BgL_auxz00_2418, BFALSE, BFALSE);
					}
				return
					BGl_rgczd2bufferzd2upcasezd2subsymbolzd2zz__rgcz00(BgL_auxz00_2415,
					BgL_auxz00_2422, BgL_auxz00_2431);
			}
		}

	}



/* rgc-buffer-keyword */
	BGL_EXPORTED_DEF obj_t BGl_rgczd2bufferzd2keywordz00zz__rgcz00(obj_t
		BgL_inputzd2portzd2_40)
	{
		{	/* Rgc/rgc.scm 355 */
			BGL_TAIL return rgc_buffer_keyword(BgL_inputzd2portzd2_40);
		}

	}



/* &rgc-buffer-keyword */
	obj_t BGl_z62rgczd2bufferzd2keywordz62zz__rgcz00(obj_t BgL_envz00_1941,
		obj_t BgL_inputzd2portzd2_1942)
	{
		{	/* Rgc/rgc.scm 355 */
			{	/* Rgc/rgc.scm 356 */
				obj_t BgL_auxz00_2442;

				if (INPUT_PORTP(BgL_inputzd2portzd2_1942))
					{	/* Rgc/rgc.scm 356 */
						BgL_auxz00_2442 = BgL_inputzd2portzd2_1942;
					}
				else
					{
						obj_t BgL_auxz00_2445;

						BgL_auxz00_2445 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
							BINT(17735L), BGl_string1724z00zz__rgcz00,
							BGl_string1703z00zz__rgcz00, BgL_inputzd2portzd2_1942);
						FAILURE(BgL_auxz00_2445, BFALSE, BFALSE);
					}
				return BGl_rgczd2bufferzd2keywordz00zz__rgcz00(BgL_auxz00_2442);
			}
		}

	}



/* rgc-buffer-downcase-keyword */
	BGL_EXPORTED_DEF obj_t
		BGl_rgczd2bufferzd2downcasezd2keywordzd2zz__rgcz00(obj_t
		BgL_inputzd2portzd2_41)
	{
		{	/* Rgc/rgc.scm 361 */
			BGL_TAIL return rgc_buffer_downcase_keyword(BgL_inputzd2portzd2_41);
		}

	}



/* &rgc-buffer-downcase-keyword */
	obj_t BGl_z62rgczd2bufferzd2downcasezd2keywordzb0zz__rgcz00(obj_t
		BgL_envz00_1943, obj_t BgL_inputzd2portzd2_1944)
	{
		{	/* Rgc/rgc.scm 361 */
			{	/* Rgc/rgc.scm 362 */
				obj_t BgL_auxz00_2451;

				if (INPUT_PORTP(BgL_inputzd2portzd2_1944))
					{	/* Rgc/rgc.scm 362 */
						BgL_auxz00_2451 = BgL_inputzd2portzd2_1944;
					}
				else
					{
						obj_t BgL_auxz00_2454;

						BgL_auxz00_2454 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
							BINT(18072L), BGl_string1725z00zz__rgcz00,
							BGl_string1703z00zz__rgcz00, BgL_inputzd2portzd2_1944);
						FAILURE(BgL_auxz00_2454, BFALSE, BFALSE);
					}
				return
					BGl_rgczd2bufferzd2downcasezd2keywordzd2zz__rgcz00(BgL_auxz00_2451);
			}
		}

	}



/* rgc-buffer-upcase-keyword */
	BGL_EXPORTED_DEF obj_t BGl_rgczd2bufferzd2upcasezd2keywordzd2zz__rgcz00(obj_t
		BgL_inputzd2portzd2_42)
	{
		{	/* Rgc/rgc.scm 367 */
			BGL_TAIL return rgc_buffer_upcase_keyword(BgL_inputzd2portzd2_42);
		}

	}



/* &rgc-buffer-upcase-keyword */
	obj_t BGl_z62rgczd2bufferzd2upcasezd2keywordzb0zz__rgcz00(obj_t
		BgL_envz00_1945, obj_t BgL_inputzd2portzd2_1946)
	{
		{	/* Rgc/rgc.scm 367 */
			{	/* Rgc/rgc.scm 368 */
				obj_t BgL_auxz00_2460;

				if (INPUT_PORTP(BgL_inputzd2portzd2_1946))
					{	/* Rgc/rgc.scm 368 */
						BgL_auxz00_2460 = BgL_inputzd2portzd2_1946;
					}
				else
					{
						obj_t BgL_auxz00_2463;

						BgL_auxz00_2463 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
							BINT(18416L), BGl_string1726z00zz__rgcz00,
							BGl_string1703z00zz__rgcz00, BgL_inputzd2portzd2_1946);
						FAILURE(BgL_auxz00_2463, BFALSE, BFALSE);
					}
				return
					BGl_rgczd2bufferzd2upcasezd2keywordzd2zz__rgcz00(BgL_auxz00_2460);
			}
		}

	}



/* rgc-buffer-position */
	BGL_EXPORTED_DEF long BGl_rgczd2bufferzd2positionz00zz__rgcz00(obj_t
		BgL_inputzd2portzd2_43, long BgL_forwardz00_44)
	{
		{	/* Rgc/rgc.scm 373 */
			return RGC_BUFFER_POSITION(BgL_inputzd2portzd2_43, BgL_forwardz00_44);
		}

	}



/* &rgc-buffer-position */
	obj_t BGl_z62rgczd2bufferzd2positionz62zz__rgcz00(obj_t BgL_envz00_1947,
		obj_t BgL_inputzd2portzd2_1948, obj_t BgL_forwardz00_1949)
	{
		{	/* Rgc/rgc.scm 373 */
			{	/* Rgc/rgc.scm 374 */
				long BgL_tmpz00_2469;

				{	/* Rgc/rgc.scm 374 */
					long BgL_auxz00_2477;
					obj_t BgL_auxz00_2470;

					{	/* Rgc/rgc.scm 374 */
						obj_t BgL_tmpz00_2478;

						if (INTEGERP(BgL_forwardz00_1949))
							{	/* Rgc/rgc.scm 374 */
								BgL_tmpz00_2478 = BgL_forwardz00_1949;
							}
						else
							{
								obj_t BgL_auxz00_2481;

								BgL_auxz00_2481 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
									BINT(18757L), BGl_string1727z00zz__rgcz00,
									BGl_string1704z00zz__rgcz00, BgL_forwardz00_1949);
								FAILURE(BgL_auxz00_2481, BFALSE, BFALSE);
							}
						BgL_auxz00_2477 = (long) CINT(BgL_tmpz00_2478);
					}
					if (INPUT_PORTP(BgL_inputzd2portzd2_1948))
						{	/* Rgc/rgc.scm 374 */
							BgL_auxz00_2470 = BgL_inputzd2portzd2_1948;
						}
					else
						{
							obj_t BgL_auxz00_2473;

							BgL_auxz00_2473 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
								BINT(18757L), BGl_string1727z00zz__rgcz00,
								BGl_string1703z00zz__rgcz00, BgL_inputzd2portzd2_1948);
							FAILURE(BgL_auxz00_2473, BFALSE, BFALSE);
						}
					BgL_tmpz00_2469 =
						BGl_rgczd2bufferzd2positionz00zz__rgcz00(BgL_auxz00_2470,
						BgL_auxz00_2477);
				}
				return BINT(BgL_tmpz00_2469);
			}
		}

	}



/* rgc-buffer-forward */
	BGL_EXPORTED_DEF long BGl_rgczd2bufferzd2forwardz00zz__rgcz00(obj_t
		BgL_inputzd2portzd2_45)
	{
		{	/* Rgc/rgc.scm 379 */
			return RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_45);
		}

	}



/* &rgc-buffer-forward */
	obj_t BGl_z62rgczd2bufferzd2forwardz62zz__rgcz00(obj_t BgL_envz00_1950,
		obj_t BgL_inputzd2portzd2_1951)
	{
		{	/* Rgc/rgc.scm 379 */
			{	/* Rgc/rgc.scm 380 */
				long BgL_tmpz00_2489;

				{	/* Rgc/rgc.scm 380 */
					obj_t BgL_auxz00_2490;

					if (INPUT_PORTP(BgL_inputzd2portzd2_1951))
						{	/* Rgc/rgc.scm 380 */
							BgL_auxz00_2490 = BgL_inputzd2portzd2_1951;
						}
					else
						{
							obj_t BgL_auxz00_2493;

							BgL_auxz00_2493 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
								BINT(19091L), BGl_string1728z00zz__rgcz00,
								BGl_string1703z00zz__rgcz00, BgL_inputzd2portzd2_1951);
							FAILURE(BgL_auxz00_2493, BFALSE, BFALSE);
						}
					BgL_tmpz00_2489 =
						BGl_rgczd2bufferzd2forwardz00zz__rgcz00(BgL_auxz00_2490);
				}
				return BINT(BgL_tmpz00_2489);
			}
		}

	}



/* rgc-buffer-bufpos */
	BGL_EXPORTED_DEF long BGl_rgczd2bufferzd2bufposz00zz__rgcz00(obj_t
		BgL_inputzd2portzd2_46)
	{
		{	/* Rgc/rgc.scm 385 */
			return RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_46);
		}

	}



/* &rgc-buffer-bufpos */
	obj_t BGl_z62rgczd2bufferzd2bufposz62zz__rgcz00(obj_t BgL_envz00_1952,
		obj_t BgL_inputzd2portzd2_1953)
	{
		{	/* Rgc/rgc.scm 385 */
			{	/* Rgc/rgc.scm 386 */
				long BgL_tmpz00_2500;

				{	/* Rgc/rgc.scm 386 */
					obj_t BgL_auxz00_2501;

					if (INPUT_PORTP(BgL_inputzd2portzd2_1953))
						{	/* Rgc/rgc.scm 386 */
							BgL_auxz00_2501 = BgL_inputzd2portzd2_1953;
						}
					else
						{
							obj_t BgL_auxz00_2504;

							BgL_auxz00_2504 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
								BINT(19415L), BGl_string1729z00zz__rgcz00,
								BGl_string1703z00zz__rgcz00, BgL_inputzd2portzd2_1953);
							FAILURE(BgL_auxz00_2504, BFALSE, BFALSE);
						}
					BgL_tmpz00_2500 =
						BGl_rgczd2bufferzd2bufposz00zz__rgcz00(BgL_auxz00_2501);
				}
				return BINT(BgL_tmpz00_2500);
			}
		}

	}



/* rgc-set-filepos! */
	BGL_EXPORTED_DEF obj_t BGl_rgczd2setzd2fileposz12z12zz__rgcz00(obj_t
		BgL_inputzd2portzd2_47)
	{
		{	/* Rgc/rgc.scm 391 */
			return BINT(RGC_SET_FILEPOS(BgL_inputzd2portzd2_47));
		}

	}



/* &rgc-set-filepos! */
	obj_t BGl_z62rgczd2setzd2fileposz12z70zz__rgcz00(obj_t BgL_envz00_1954,
		obj_t BgL_inputzd2portzd2_1955)
	{
		{	/* Rgc/rgc.scm 391 */
			{	/* Rgc/rgc.scm 392 */
				obj_t BgL_auxz00_2512;

				if (INPUT_PORTP(BgL_inputzd2portzd2_1955))
					{	/* Rgc/rgc.scm 392 */
						BgL_auxz00_2512 = BgL_inputzd2portzd2_1955;
					}
				else
					{
						obj_t BgL_auxz00_2515;

						BgL_auxz00_2515 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
							BINT(19731L), BGl_string1730z00zz__rgcz00,
							BGl_string1703z00zz__rgcz00, BgL_inputzd2portzd2_1955);
						FAILURE(BgL_auxz00_2515, BFALSE, BFALSE);
					}
				return BGl_rgczd2setzd2fileposz12z12zz__rgcz00(BgL_auxz00_2512);
			}
		}

	}



/* rgc-start-match! */
	BGL_EXPORTED_DEF long BGl_rgczd2startzd2matchz12z12zz__rgcz00(obj_t
		BgL_inputzd2portzd2_48)
	{
		{	/* Rgc/rgc.scm 397 */
			return RGC_START_MATCH(BgL_inputzd2portzd2_48);
		}

	}



/* &rgc-start-match! */
	obj_t BGl_z62rgczd2startzd2matchz12z70zz__rgcz00(obj_t BgL_envz00_1956,
		obj_t BgL_inputzd2portzd2_1957)
	{
		{	/* Rgc/rgc.scm 397 */
			{	/* Rgc/rgc.scm 398 */
				long BgL_tmpz00_2521;

				{	/* Rgc/rgc.scm 398 */
					obj_t BgL_auxz00_2522;

					if (INPUT_PORTP(BgL_inputzd2portzd2_1957))
						{	/* Rgc/rgc.scm 398 */
							BgL_auxz00_2522 = BgL_inputzd2portzd2_1957;
						}
					else
						{
							obj_t BgL_auxz00_2525;

							BgL_auxz00_2525 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
								BINT(20034L), BGl_string1731z00zz__rgcz00,
								BGl_string1703z00zz__rgcz00, BgL_inputzd2portzd2_1957);
							FAILURE(BgL_auxz00_2525, BFALSE, BFALSE);
						}
					BgL_tmpz00_2521 =
						BGl_rgczd2startzd2matchz12z12zz__rgcz00(BgL_auxz00_2522);
				}
				return BINT(BgL_tmpz00_2521);
			}
		}

	}



/* rgc-stop-match! */
	BGL_EXPORTED_DEF long BGl_rgczd2stopzd2matchz12z12zz__rgcz00(obj_t
		BgL_inputzd2portzd2_49, long BgL_forwardz00_50)
	{
		{	/* Rgc/rgc.scm 403 */
			return RGC_STOP_MATCH(BgL_inputzd2portzd2_49, BgL_forwardz00_50);
		}

	}



/* &rgc-stop-match! */
	obj_t BGl_z62rgczd2stopzd2matchz12z70zz__rgcz00(obj_t BgL_envz00_1958,
		obj_t BgL_inputzd2portzd2_1959, obj_t BgL_forwardz00_1960)
	{
		{	/* Rgc/rgc.scm 403 */
			{	/* Rgc/rgc.scm 404 */
				long BgL_tmpz00_2532;

				{	/* Rgc/rgc.scm 404 */
					long BgL_auxz00_2540;
					obj_t BgL_auxz00_2533;

					{	/* Rgc/rgc.scm 404 */
						obj_t BgL_tmpz00_2541;

						if (INTEGERP(BgL_forwardz00_1960))
							{	/* Rgc/rgc.scm 404 */
								BgL_tmpz00_2541 = BgL_forwardz00_1960;
							}
						else
							{
								obj_t BgL_auxz00_2544;

								BgL_auxz00_2544 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
									BINT(20344L), BGl_string1732z00zz__rgcz00,
									BGl_string1704z00zz__rgcz00, BgL_forwardz00_1960);
								FAILURE(BgL_auxz00_2544, BFALSE, BFALSE);
							}
						BgL_auxz00_2540 = (long) CINT(BgL_tmpz00_2541);
					}
					if (INPUT_PORTP(BgL_inputzd2portzd2_1959))
						{	/* Rgc/rgc.scm 404 */
							BgL_auxz00_2533 = BgL_inputzd2portzd2_1959;
						}
					else
						{
							obj_t BgL_auxz00_2536;

							BgL_auxz00_2536 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
								BINT(20344L), BGl_string1732z00zz__rgcz00,
								BGl_string1703z00zz__rgcz00, BgL_inputzd2portzd2_1959);
							FAILURE(BgL_auxz00_2536, BFALSE, BFALSE);
						}
					BgL_tmpz00_2532 =
						BGl_rgczd2stopzd2matchz12z12zz__rgcz00(BgL_auxz00_2533,
						BgL_auxz00_2540);
				}
				return BINT(BgL_tmpz00_2532);
			}
		}

	}



/* rgc-fill-buffer */
	BGL_EXPORTED_DEF bool_t BGl_rgczd2fillzd2bufferz00zz__rgcz00(obj_t
		BgL_inputzd2portzd2_51)
	{
		{	/* Rgc/rgc.scm 409 */
			BGL_TAIL return rgc_fill_buffer(BgL_inputzd2portzd2_51);
		}

	}



/* &rgc-fill-buffer */
	obj_t BGl_z62rgczd2fillzd2bufferz62zz__rgcz00(obj_t BgL_envz00_1961,
		obj_t BgL_inputzd2portzd2_1962)
	{
		{	/* Rgc/rgc.scm 409 */
			{	/* Rgc/rgc.scm 410 */
				bool_t BgL_tmpz00_2552;

				{	/* Rgc/rgc.scm 410 */
					obj_t BgL_auxz00_2553;

					if (INPUT_PORTP(BgL_inputzd2portzd2_1962))
						{	/* Rgc/rgc.scm 410 */
							BgL_auxz00_2553 = BgL_inputzd2portzd2_1962;
						}
					else
						{
							obj_t BgL_auxz00_2556;

							BgL_auxz00_2556 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
								BINT(20671L), BGl_string1733z00zz__rgcz00,
								BGl_string1703z00zz__rgcz00, BgL_inputzd2portzd2_1962);
							FAILURE(BgL_auxz00_2556, BFALSE, BFALSE);
						}
					BgL_tmpz00_2552 =
						BGl_rgczd2fillzd2bufferz00zz__rgcz00(BgL_auxz00_2553);
				}
				return BBOOL(BgL_tmpz00_2552);
			}
		}

	}



/* rgc-buffer-bol? */
	BGL_EXPORTED_DEF bool_t BGl_rgczd2bufferzd2bolzf3zf3zz__rgcz00(obj_t
		BgL_inputzd2portzd2_52)
	{
		{	/* Rgc/rgc.scm 415 */
			BGL_TAIL return rgc_buffer_bol_p(BgL_inputzd2portzd2_52);
		}

	}



/* &rgc-buffer-bol? */
	obj_t BGl_z62rgczd2bufferzd2bolzf3z91zz__rgcz00(obj_t BgL_envz00_1963,
		obj_t BgL_inputzd2portzd2_1964)
	{
		{	/* Rgc/rgc.scm 415 */
			{	/* Rgc/rgc.scm 416 */
				bool_t BgL_tmpz00_2563;

				{	/* Rgc/rgc.scm 416 */
					obj_t BgL_auxz00_2564;

					if (INPUT_PORTP(BgL_inputzd2portzd2_1964))
						{	/* Rgc/rgc.scm 416 */
							BgL_auxz00_2564 = BgL_inputzd2portzd2_1964;
						}
					else
						{
							obj_t BgL_auxz00_2567;

							BgL_auxz00_2567 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
								BINT(20990L), BGl_string1734z00zz__rgcz00,
								BGl_string1703z00zz__rgcz00, BgL_inputzd2portzd2_1964);
							FAILURE(BgL_auxz00_2567, BFALSE, BFALSE);
						}
					BgL_tmpz00_2563 =
						BGl_rgczd2bufferzd2bolzf3zf3zz__rgcz00(BgL_auxz00_2564);
				}
				return BBOOL(BgL_tmpz00_2563);
			}
		}

	}



/* rgc-buffer-eol? */
	BGL_EXPORTED_DEF bool_t BGl_rgczd2bufferzd2eolzf3zf3zz__rgcz00(obj_t
		BgL_inputzd2portzd2_53, long BgL_forwardz00_54, long BgL_bufposz00_55)
	{
		{	/* Rgc/rgc.scm 421 */
			BGL_TAIL return
				rgc_buffer_eol_p(BgL_inputzd2portzd2_53, BgL_forwardz00_54,
				BgL_bufposz00_55);
		}

	}



/* &rgc-buffer-eol? */
	obj_t BGl_z62rgczd2bufferzd2eolzf3z91zz__rgcz00(obj_t BgL_envz00_1965,
		obj_t BgL_inputzd2portzd2_1966, obj_t BgL_forwardz00_1967,
		obj_t BgL_bufposz00_1968)
	{
		{	/* Rgc/rgc.scm 421 */
			{	/* Rgc/rgc.scm 422 */
				bool_t BgL_tmpz00_2574;

				{	/* Rgc/rgc.scm 422 */
					long BgL_auxz00_2591;
					long BgL_auxz00_2582;
					obj_t BgL_auxz00_2575;

					{	/* Rgc/rgc.scm 422 */
						obj_t BgL_tmpz00_2592;

						if (INTEGERP(BgL_bufposz00_1968))
							{	/* Rgc/rgc.scm 422 */
								BgL_tmpz00_2592 = BgL_bufposz00_1968;
							}
						else
							{
								obj_t BgL_auxz00_2595;

								BgL_auxz00_2595 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
									BINT(21324L), BGl_string1735z00zz__rgcz00,
									BGl_string1704z00zz__rgcz00, BgL_bufposz00_1968);
								FAILURE(BgL_auxz00_2595, BFALSE, BFALSE);
							}
						BgL_auxz00_2591 = (long) CINT(BgL_tmpz00_2592);
					}
					{	/* Rgc/rgc.scm 422 */
						obj_t BgL_tmpz00_2583;

						if (INTEGERP(BgL_forwardz00_1967))
							{	/* Rgc/rgc.scm 422 */
								BgL_tmpz00_2583 = BgL_forwardz00_1967;
							}
						else
							{
								obj_t BgL_auxz00_2586;

								BgL_auxz00_2586 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
									BINT(21324L), BGl_string1735z00zz__rgcz00,
									BGl_string1704z00zz__rgcz00, BgL_forwardz00_1967);
								FAILURE(BgL_auxz00_2586, BFALSE, BFALSE);
							}
						BgL_auxz00_2582 = (long) CINT(BgL_tmpz00_2583);
					}
					if (INPUT_PORTP(BgL_inputzd2portzd2_1966))
						{	/* Rgc/rgc.scm 422 */
							BgL_auxz00_2575 = BgL_inputzd2portzd2_1966;
						}
					else
						{
							obj_t BgL_auxz00_2578;

							BgL_auxz00_2578 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
								BINT(21324L), BGl_string1735z00zz__rgcz00,
								BGl_string1703z00zz__rgcz00, BgL_inputzd2portzd2_1966);
							FAILURE(BgL_auxz00_2578, BFALSE, BFALSE);
						}
					BgL_tmpz00_2574 =
						BGl_rgczd2bufferzd2eolzf3zf3zz__rgcz00(BgL_auxz00_2575,
						BgL_auxz00_2582, BgL_auxz00_2591);
				}
				return BBOOL(BgL_tmpz00_2574);
			}
		}

	}



/* rgc-buffer-bof? */
	BGL_EXPORTED_DEF bool_t BGl_rgczd2bufferzd2bofzf3zf3zz__rgcz00(obj_t
		BgL_inputzd2portzd2_56)
	{
		{	/* Rgc/rgc.scm 427 */
			BGL_TAIL return rgc_buffer_bof_p(BgL_inputzd2portzd2_56);
		}

	}



/* &rgc-buffer-bof? */
	obj_t BGl_z62rgczd2bufferzd2bofzf3z91zz__rgcz00(obj_t BgL_envz00_1969,
		obj_t BgL_inputzd2portzd2_1970)
	{
		{	/* Rgc/rgc.scm 427 */
			{	/* Rgc/rgc.scm 428 */
				bool_t BgL_tmpz00_2603;

				{	/* Rgc/rgc.scm 428 */
					obj_t BgL_auxz00_2604;

					if (INPUT_PORTP(BgL_inputzd2portzd2_1970))
						{	/* Rgc/rgc.scm 428 */
							BgL_auxz00_2604 = BgL_inputzd2portzd2_1970;
						}
					else
						{
							obj_t BgL_auxz00_2607;

							BgL_auxz00_2607 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
								BINT(21658L), BGl_string1736z00zz__rgcz00,
								BGl_string1703z00zz__rgcz00, BgL_inputzd2portzd2_1970);
							FAILURE(BgL_auxz00_2607, BFALSE, BFALSE);
						}
					BgL_tmpz00_2603 =
						BGl_rgczd2bufferzd2bofzf3zf3zz__rgcz00(BgL_auxz00_2604);
				}
				return BBOOL(BgL_tmpz00_2603);
			}
		}

	}



/* rgc-buffer-eof? */
	BGL_EXPORTED_DEF bool_t BGl_rgczd2bufferzd2eofzf3zf3zz__rgcz00(obj_t
		BgL_inputzd2portzd2_57)
	{
		{	/* Rgc/rgc.scm 433 */
			BGL_TAIL return rgc_buffer_eof_p(BgL_inputzd2portzd2_57);
		}

	}



/* &rgc-buffer-eof? */
	obj_t BGl_z62rgczd2bufferzd2eofzf3z91zz__rgcz00(obj_t BgL_envz00_1971,
		obj_t BgL_inputzd2portzd2_1972)
	{
		{	/* Rgc/rgc.scm 433 */
			{	/* Rgc/rgc.scm 434 */
				bool_t BgL_tmpz00_2614;

				{	/* Rgc/rgc.scm 434 */
					obj_t BgL_auxz00_2615;

					if (INPUT_PORTP(BgL_inputzd2portzd2_1972))
						{	/* Rgc/rgc.scm 434 */
							BgL_auxz00_2615 = BgL_inputzd2portzd2_1972;
						}
					else
						{
							obj_t BgL_auxz00_2618;

							BgL_auxz00_2618 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
								BINT(21977L), BGl_string1737z00zz__rgcz00,
								BGl_string1703z00zz__rgcz00, BgL_inputzd2portzd2_1972);
							FAILURE(BgL_auxz00_2618, BFALSE, BFALSE);
						}
					BgL_tmpz00_2614 =
						BGl_rgczd2bufferzd2eofzf3zf3zz__rgcz00(BgL_auxz00_2615);
				}
				return BBOOL(BgL_tmpz00_2614);
			}
		}

	}



/* rgc-buffer-eof2? */
	BGL_EXPORTED_DEF bool_t BGl_rgczd2bufferzd2eof2zf3zf3zz__rgcz00(obj_t
		BgL_inputzd2portzd2_58, long BgL_forwardz00_59, long BgL_bufposz00_60)
	{
		{	/* Rgc/rgc.scm 439 */
			BGL_TAIL return
				rgc_buffer_eof2_p(BgL_inputzd2portzd2_58, BgL_forwardz00_59,
				BgL_bufposz00_60);
		}

	}



/* &rgc-buffer-eof2? */
	obj_t BGl_z62rgczd2bufferzd2eof2zf3z91zz__rgcz00(obj_t BgL_envz00_1973,
		obj_t BgL_inputzd2portzd2_1974, obj_t BgL_forwardz00_1975,
		obj_t BgL_bufposz00_1976)
	{
		{	/* Rgc/rgc.scm 439 */
			{	/* Rgc/rgc.scm 440 */
				bool_t BgL_tmpz00_2625;

				{	/* Rgc/rgc.scm 440 */
					long BgL_auxz00_2642;
					long BgL_auxz00_2633;
					obj_t BgL_auxz00_2626;

					{	/* Rgc/rgc.scm 440 */
						obj_t BgL_tmpz00_2643;

						if (INTEGERP(BgL_bufposz00_1976))
							{	/* Rgc/rgc.scm 440 */
								BgL_tmpz00_2643 = BgL_bufposz00_1976;
							}
						else
							{
								obj_t BgL_auxz00_2646;

								BgL_auxz00_2646 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
									BINT(22312L), BGl_string1738z00zz__rgcz00,
									BGl_string1704z00zz__rgcz00, BgL_bufposz00_1976);
								FAILURE(BgL_auxz00_2646, BFALSE, BFALSE);
							}
						BgL_auxz00_2642 = (long) CINT(BgL_tmpz00_2643);
					}
					{	/* Rgc/rgc.scm 440 */
						obj_t BgL_tmpz00_2634;

						if (INTEGERP(BgL_forwardz00_1975))
							{	/* Rgc/rgc.scm 440 */
								BgL_tmpz00_2634 = BgL_forwardz00_1975;
							}
						else
							{
								obj_t BgL_auxz00_2637;

								BgL_auxz00_2637 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
									BINT(22312L), BGl_string1738z00zz__rgcz00,
									BGl_string1704z00zz__rgcz00, BgL_forwardz00_1975);
								FAILURE(BgL_auxz00_2637, BFALSE, BFALSE);
							}
						BgL_auxz00_2633 = (long) CINT(BgL_tmpz00_2634);
					}
					if (INPUT_PORTP(BgL_inputzd2portzd2_1974))
						{	/* Rgc/rgc.scm 440 */
							BgL_auxz00_2626 = BgL_inputzd2portzd2_1974;
						}
					else
						{
							obj_t BgL_auxz00_2629;

							BgL_auxz00_2629 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
								BINT(22312L), BGl_string1738z00zz__rgcz00,
								BGl_string1703z00zz__rgcz00, BgL_inputzd2portzd2_1974);
							FAILURE(BgL_auxz00_2629, BFALSE, BFALSE);
						}
					BgL_tmpz00_2625 =
						BGl_rgczd2bufferzd2eof2zf3zf3zz__rgcz00(BgL_auxz00_2626,
						BgL_auxz00_2633, BgL_auxz00_2642);
				}
				return BBOOL(BgL_tmpz00_2625);
			}
		}

	}



/* rgc-the-submatch */
	BGL_EXPORTED_DEF obj_t BGl_rgczd2thezd2submatchz00zz__rgcz00(obj_t
		BgL_rgczd2submatcheszd2_61, long BgL_posz00_62, long BgL_matchz00_63,
		long BgL_submatchz00_64)
	{
		{	/* Rgc/rgc.scm 449 */
			{
				obj_t BgL_submatchesz00_1183;
				long BgL_startz00_1184;
				obj_t BgL_stopz00_1185;

				BgL_submatchesz00_1183 = BgL_rgczd2submatcheszd2_61;
				BgL_startz00_1184 = -1L;
				BgL_stopz00_1185 = BINT(-1L);
			BgL_zc3z04anonymousza31194ze3z87_1186:
				if (NULLP(BgL_submatchesz00_1183))
					{	/* Rgc/rgc.scm 458 */
						{	/* Rgc/rgc.scm 459 */
							int BgL_tmpz00_2655;

							BgL_tmpz00_2655 = (int) (2L);
							BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2655);
						}
						{	/* Rgc/rgc.scm 459 */
							int BgL_tmpz00_2658;

							BgL_tmpz00_2658 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_2658, BgL_stopz00_1185);
						}
						return BINT(BgL_startz00_1184);
					}
				else
					{	/* Rgc/rgc.scm 460 */
						obj_t BgL_mvz00_1190;

						BgL_mvz00_1190 = CAR(((obj_t) BgL_submatchesz00_1183));
						{	/* Rgc/rgc.scm 460 */
							obj_t BgL_ruz00_1191;

							BgL_ruz00_1191 = VECTOR_REF(((obj_t) BgL_mvz00_1190), 0L);
							{	/* Rgc/rgc.scm 461 */
								obj_t BgL_smz00_1192;

								BgL_smz00_1192 = VECTOR_REF(((obj_t) BgL_mvz00_1190), 1L);
								{	/* Rgc/rgc.scm 462 */
									obj_t BgL_spz00_1193;

									BgL_spz00_1193 = VECTOR_REF(((obj_t) BgL_mvz00_1190), 2L);
									{	/* Rgc/rgc.scm 463 */
										obj_t BgL_whatz00_1194;

										BgL_whatz00_1194 = VECTOR_REF(((obj_t) BgL_mvz00_1190), 3L);
										{	/* Rgc/rgc.scm 464 */

											{	/* Rgc/rgc.scm 466 */
												bool_t BgL_test1884z00_2672;

												if (((long) CINT(BgL_ruz00_1191) == BgL_matchz00_63))
													{	/* Rgc/rgc.scm 466 */
														if (
															((long) CINT(BgL_smz00_1192) ==
																BgL_submatchz00_64))
															{	/* Rgc/rgc.scm 466 */
																BgL_test1884z00_2672 =
																	(
																	(long) CINT(BgL_spz00_1193) <= BgL_posz00_62);
															}
														else
															{	/* Rgc/rgc.scm 466 */
																BgL_test1884z00_2672 = ((bool_t) 0);
															}
													}
												else
													{	/* Rgc/rgc.scm 466 */
														BgL_test1884z00_2672 = ((bool_t) 0);
													}
												if (BgL_test1884z00_2672)
													{	/* Rgc/rgc.scm 466 */
														if (
															(BgL_whatz00_1194 == BGl_symbol1739z00zz__rgcz00))
															{	/* Rgc/rgc.scm 467 */
																if (((long) CINT(BgL_stopz00_1185) < 0L))
																	{	/* Rgc/rgc.scm 470 */
																		obj_t BgL_arg1201z00_1201;

																		BgL_arg1201z00_1201 =
																			CDR(((obj_t) BgL_submatchesz00_1183));
																		{
																			obj_t BgL_stopz00_2689;
																			obj_t BgL_submatchesz00_2688;

																			BgL_submatchesz00_2688 =
																				BgL_arg1201z00_1201;
																			BgL_stopz00_2689 = BgL_spz00_1193;
																			BgL_stopz00_1185 = BgL_stopz00_2689;
																			BgL_submatchesz00_1183 =
																				BgL_submatchesz00_2688;
																			goto
																				BgL_zc3z04anonymousza31194ze3z87_1186;
																		}
																	}
																else
																	{	/* Rgc/rgc.scm 471 */
																		obj_t BgL_arg1202z00_1202;

																		BgL_arg1202z00_1202 =
																			CDR(((obj_t) BgL_submatchesz00_1183));
																		{
																			obj_t BgL_submatchesz00_2692;

																			BgL_submatchesz00_2692 =
																				BgL_arg1202z00_1202;
																			BgL_submatchesz00_1183 =
																				BgL_submatchesz00_2692;
																			goto
																				BgL_zc3z04anonymousza31194ze3z87_1186;
																		}
																	}
															}
														else
															{	/* Rgc/rgc.scm 467 */
																if (
																	(BgL_whatz00_1194 ==
																		BGl_symbol1741z00zz__rgcz00))
																	{	/* Rgc/rgc.scm 473 */
																		long BgL_val0_1088z00_1204;

																		BgL_val0_1088z00_1204 =
																			((long) CINT(BgL_spz00_1193) - 1L);
																		{	/* Rgc/rgc.scm 473 */
																			int BgL_tmpz00_2697;

																			BgL_tmpz00_2697 = (int) (2L);
																			BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2697);
																		}
																		{	/* Rgc/rgc.scm 473 */
																			int BgL_tmpz00_2700;

																			BgL_tmpz00_2700 = (int) (1L);
																			BGL_MVALUES_VAL_SET(BgL_tmpz00_2700,
																				BgL_stopz00_1185);
																		}
																		return BINT(BgL_val0_1088z00_1204);
																	}
																else
																	{	/* Rgc/rgc.scm 467 */
																		if (
																			(BgL_whatz00_1194 ==
																				BGl_symbol1743z00zz__rgcz00))
																			{	/* Rgc/rgc.scm 475 */
																				obj_t BgL_arg1206z00_1207;
																				long BgL_arg1208z00_1208;

																				BgL_arg1206z00_1207 =
																					CDR(((obj_t) BgL_submatchesz00_1183));
																				BgL_arg1208z00_1208 =
																					((long) CINT(BgL_spz00_1193) - 1L);
																				{
																					long BgL_startz00_2711;
																					obj_t BgL_submatchesz00_2710;

																					BgL_submatchesz00_2710 =
																						BgL_arg1206z00_1207;
																					BgL_startz00_2711 =
																						BgL_arg1208z00_1208;
																					BgL_startz00_1184 = BgL_startz00_2711;
																					BgL_submatchesz00_1183 =
																						BgL_submatchesz00_2710;
																					goto
																						BgL_zc3z04anonymousza31194ze3z87_1186;
																				}
																			}
																		else
																			{	/* Rgc/rgc.scm 467 */
																				return BUNSPEC;
																			}
																	}
															}
													}
												else
													{	/* Rgc/rgc.scm 466 */
														if ((BgL_whatz00_1194 == BgL_stopz00_1185))
															{	/* Rgc/rgc.scm 476 */
																{	/* Rgc/rgc.scm 477 */
																	int BgL_tmpz00_2714;

																	BgL_tmpz00_2714 = (int) (2L);
																	BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2714);
																}
																{	/* Rgc/rgc.scm 477 */
																	int BgL_tmpz00_2717;

																	BgL_tmpz00_2717 = (int) (1L);
																	BGL_MVALUES_VAL_SET(BgL_tmpz00_2717,
																		BgL_stopz00_1185);
																}
																return BINT(BgL_startz00_1184);
															}
														else
															{	/* Rgc/rgc.scm 479 */
																obj_t BgL_arg1209z00_1211;

																BgL_arg1209z00_1211 =
																	CDR(((obj_t) BgL_submatchesz00_1183));
																{
																	obj_t BgL_submatchesz00_2723;

																	BgL_submatchesz00_2723 = BgL_arg1209z00_1211;
																	BgL_submatchesz00_1183 =
																		BgL_submatchesz00_2723;
																	goto BgL_zc3z04anonymousza31194ze3z87_1186;
																}
															}
													}
											}
										}
									}
								}
							}
						}
					}
			}
		}

	}



/* &rgc-the-submatch */
	obj_t BGl_z62rgczd2thezd2submatchz62zz__rgcz00(obj_t BgL_envz00_1977,
		obj_t BgL_rgczd2submatcheszd2_1978, obj_t BgL_posz00_1979,
		obj_t BgL_matchz00_1980, obj_t BgL_submatchz00_1981)
	{
		{	/* Rgc/rgc.scm 449 */
			{	/* Rgc/rgc.scm 458 */
				long BgL_auxz00_2743;
				long BgL_auxz00_2734;
				long BgL_auxz00_2725;

				{	/* Rgc/rgc.scm 458 */
					obj_t BgL_tmpz00_2744;

					if (INTEGERP(BgL_submatchz00_1981))
						{	/* Rgc/rgc.scm 458 */
							BgL_tmpz00_2744 = BgL_submatchz00_1981;
						}
					else
						{
							obj_t BgL_auxz00_2747;

							BgL_auxz00_2747 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
								BINT(23377L), BGl_string1745z00zz__rgcz00,
								BGl_string1704z00zz__rgcz00, BgL_submatchz00_1981);
							FAILURE(BgL_auxz00_2747, BFALSE, BFALSE);
						}
					BgL_auxz00_2743 = (long) CINT(BgL_tmpz00_2744);
				}
				{	/* Rgc/rgc.scm 458 */
					obj_t BgL_tmpz00_2735;

					if (INTEGERP(BgL_matchz00_1980))
						{	/* Rgc/rgc.scm 458 */
							BgL_tmpz00_2735 = BgL_matchz00_1980;
						}
					else
						{
							obj_t BgL_auxz00_2738;

							BgL_auxz00_2738 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
								BINT(23377L), BGl_string1745z00zz__rgcz00,
								BGl_string1704z00zz__rgcz00, BgL_matchz00_1980);
							FAILURE(BgL_auxz00_2738, BFALSE, BFALSE);
						}
					BgL_auxz00_2734 = (long) CINT(BgL_tmpz00_2735);
				}
				{	/* Rgc/rgc.scm 458 */
					obj_t BgL_tmpz00_2726;

					if (INTEGERP(BgL_posz00_1979))
						{	/* Rgc/rgc.scm 458 */
							BgL_tmpz00_2726 = BgL_posz00_1979;
						}
					else
						{
							obj_t BgL_auxz00_2729;

							BgL_auxz00_2729 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1701z00zz__rgcz00,
								BINT(23377L), BGl_string1745z00zz__rgcz00,
								BGl_string1704z00zz__rgcz00, BgL_posz00_1979);
							FAILURE(BgL_auxz00_2729, BFALSE, BFALSE);
						}
					BgL_auxz00_2725 = (long) CINT(BgL_tmpz00_2726);
				}
				return
					BGl_rgczd2thezd2submatchz00zz__rgcz00(BgL_rgczd2submatcheszd2_1978,
					BgL_auxz00_2725, BgL_auxz00_2734, BgL_auxz00_2743);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__rgcz00(void)
	{
		{	/* Rgc/rgc.scm 17 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__rgcz00(void)
	{
		{	/* Rgc/rgc.scm 17 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__rgcz00(void)
	{
		{	/* Rgc/rgc.scm 17 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__rgcz00(void)
	{
		{	/* Rgc/rgc.scm 17 */
			return
				BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1746z00zz__rgcz00));
		}

	}

#ifdef __cplusplus
}
#endif
