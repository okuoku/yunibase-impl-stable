/*===========================================================================*/
/*   (Rgc/rgcdfa.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Rgc/rgcdfa.scm -indent -o objs/obj_u/Rgc/rgcdfa.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___RGC_DFA_TYPE_DEFINITIONS
#define BGL___RGC_DFA_TYPE_DEFINITIONS
#endif													// BGL___RGC_DFA_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_symbol1674z00zz__rgc_dfaz00 = BUNSPEC;
	static obj_t BGl_za2statesza2z00zz__rgc_dfaz00 = BUNSPEC;
	extern obj_t BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_makezd2statezd2zz__rgc_dfaz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_statezd2positionszd2zz__rgc_dfaz00(obj_t);
	static obj_t BGl_z62statezd2positionszb0zz__rgc_dfaz00(obj_t, obj_t);
	static obj_t BGl_symbol1681z00zz__rgc_dfaz00 = BUNSPEC;
	static obj_t BGl_symbol1684z00zz__rgc_dfaz00 = BUNSPEC;
	static obj_t BGl_symbol1687z00zz__rgc_dfaz00 = BUNSPEC;
	static obj_t BGl_requirezd2initializa7ationz75zz__rgc_dfaz00 = BUNSPEC;
	extern obj_t BGl_forzd2eachzd2rgcsetz00zz__rgc_setz00(obj_t, obj_t);
	static obj_t BGl_symbol1690z00zz__rgc_dfaz00 = BUNSPEC;
	static obj_t BGl_z62statezf3z91zz__rgc_dfaz00(obj_t, obj_t);
	static obj_t BGl_symbol1694z00zz__rgc_dfaz00 = BUNSPEC;
	static obj_t BGl_symbol1698z00zz__rgc_dfaz00 = BUNSPEC;
	extern obj_t bgl_display_obj(obj_t, obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31211ze3ze5zz__rgc_dfaz00(obj_t, obj_t);
	static obj_t BGl_z62getzd2initialzd2statez62zz__rgc_dfaz00(obj_t);
	static obj_t BGl_toplevelzd2initzd2zz__rgc_dfaz00(void);
	static obj_t BGl_z62statezd2transitionszb0zz__rgc_dfaz00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zz__rgc_dfaz00(void);
	static obj_t BGl_objectzd2initzd2zz__rgc_dfaz00(void);
	extern obj_t string_append_3(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31209ze3ze5zz__rgc_dfaz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_statezd2namezd2zz__rgc_dfaz00(obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_methodzd2initzd2zz__rgc_dfaz00(void);
	static obj_t BGl_z62statezd2namezb0zz__rgc_dfaz00(obj_t, obj_t);
	static obj_t BGl_z62printzd2dfazb0zz__rgc_dfaz00(obj_t, obj_t);
	static obj_t BGl_za2initialzd2stateza2zd2zz__rgc_dfaz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31253ze3ze5zz__rgc_dfaz00(obj_t, obj_t);
	static obj_t BGl_list1700z00zz__rgc_dfaz00 = BUNSPEC;
	static obj_t BGl_list1703z00zz__rgc_dfaz00 = BUNSPEC;
	static obj_t BGl_list1706z00zz__rgc_dfaz00 = BUNSPEC;
	static obj_t BGl_list1709z00zz__rgc_dfaz00 = BUNSPEC;
	static obj_t BGl_list1710z00zz__rgc_dfaz00 = BUNSPEC;
	static obj_t BGl_list1713z00zz__rgc_dfaz00 = BUNSPEC;
	static obj_t BGl_list1716z00zz__rgc_dfaz00 = BUNSPEC;
	static obj_t BGl_list1717z00zz__rgc_dfaz00 = BUNSPEC;
	extern obj_t BGl_hashtablezd2ze3listz31zz__hashz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_printzd2dfazd2zz__rgc_dfaz00(obj_t);
	static obj_t BGl_list1720z00zz__rgc_dfaz00 = BUNSPEC;
	static obj_t BGl_list1723z00zz__rgc_dfaz00 = BUNSPEC;
	static obj_t BGl_z62nodezd2ze3dfaz53zz__rgc_dfaz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_list1730z00zz__rgc_dfaz00 = BUNSPEC;
	static obj_t BGl_list1733z00zz__rgc_dfaz00 = BUNSPEC;
	static obj_t BGl_list1736z00zz__rgc_dfaz00 = BUNSPEC;
	extern obj_t BGl_makezd2hashtablezd2zz__hashz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__rgc_dfaz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__rgc_rulesz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__rgc_setz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__rgc_configz00(long, char *);
	extern obj_t BGl_rgcsetzd2addz12zc0zz__rgc_setz00(obj_t, long);
	static obj_t BGl_list1680z00zz__rgc_dfaz00 = BUNSPEC;
	static obj_t BGl_list1683z00zz__rgc_dfaz00 = BUNSPEC;
	extern obj_t BGl_treezd2maxzd2charz00zz__rgc_rulesz00(void);
	static obj_t BGl_list1686z00zz__rgc_dfaz00 = BUNSPEC;
	static obj_t BGl_list1689z00zz__rgc_dfaz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_nodezd2ze3dfaz31zz__rgc_dfaz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_resetzd2dfaz12zc0zz__rgc_dfaz00(void);
	extern obj_t create_struct(obj_t, int);
	static obj_t BGl_list1693z00zz__rgc_dfaz00 = BUNSPEC;
	static obj_t BGl_list1697z00zz__rgc_dfaz00 = BUNSPEC;
	static obj_t BGl_cnstzd2initzd2zz__rgc_dfaz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zz__rgc_dfaz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__rgc_dfaz00(void);
	static obj_t BGl_initzd2statesz12zc0zz__rgc_dfaz00(void);
	static obj_t BGl_z62resetzd2dfaz12za2zz__rgc_dfaz00(obj_t);
	static long BGl_bucketzd2lenzd2zz__rgc_dfaz00 = 0L;
	BGL_EXPORTED_DECL bool_t BGl_statezf3zf3zz__rgc_dfaz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_getzd2initialzd2statez00zz__rgc_dfaz00(void);
	BGL_EXPORTED_DECL obj_t BGl_statezd2transitionszd2zz__rgc_dfaz00(obj_t);
	extern obj_t BGl_rgcsetzd2orz12zc0zz__rgc_setz00(obj_t, obj_t);
	static obj_t BGl_za2statezd2numza2zd2zz__rgc_dfaz00 = BUNSPEC;
	extern obj_t BGl_hashtablezd2putz12zc0zz__hashz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_symbol1701z00zz__rgc_dfaz00 = BUNSPEC;
	static obj_t BGl_symbol1704z00zz__rgc_dfaz00 = BUNSPEC;
	static obj_t BGl_symbol1707z00zz__rgc_dfaz00 = BUNSPEC;
	static obj_t BGl_symbol1711z00zz__rgc_dfaz00 = BUNSPEC;
	static obj_t BGl_symbol1714z00zz__rgc_dfaz00 = BUNSPEC;
	static obj_t BGl_symbol1718z00zz__rgc_dfaz00 = BUNSPEC;
	static obj_t BGl_symbol1721z00zz__rgc_dfaz00 = BUNSPEC;
	static obj_t BGl_symbol1724z00zz__rgc_dfaz00 = BUNSPEC;
	static obj_t BGl_symbol1731z00zz__rgc_dfaz00 = BUNSPEC;
	static obj_t BGl_symbol1734z00zz__rgc_dfaz00 = BUNSPEC;
	static obj_t BGl_symbol1737z00zz__rgc_dfaz00 = BUNSPEC;
	static obj_t BGl_symbol1739z00zz__rgc_dfaz00 = BUNSPEC;
	extern obj_t BGl_makezd2rgcsetzd2zz__rgc_setz00(long);
	extern obj_t BGl_hashtablezd2getzd2zz__hashz00(obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_printzd2dfazd2envz00zz__rgc_dfaz00,
		BgL_bgl_za762printza7d2dfaza7b1743za7, BGl_z62printzd2dfazb0zz__rgc_dfaz00,
		0L, BUNSPEC, 1);
	extern obj_t BGl_rgcsetzd2ze3hashzd2envze3zz__rgc_setz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_resetzd2dfaz12zd2envz12zz__rgc_dfaz00,
		BgL_bgl_za762resetza7d2dfaza711744za7,
		BGl_z62resetzd2dfaz12za2zz__rgc_dfaz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_nodezd2ze3dfazd2envze3zz__rgc_dfaz00,
		BgL_bgl_za762nodeza7d2za7e3dfa1745za7,
		BGl_z62nodezd2ze3dfaz53zz__rgc_dfaz00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_statezd2namezd2envz00zz__rgc_dfaz00,
		BgL_bgl_za762stateza7d2nameza71746za7, BGl_z62statezd2namezb0zz__rgc_dfaz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1702z00zz__rgc_dfaz00,
		BgL_bgl_string1702za700za7za7_1747za7, "and", 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_statezd2transitionszd2envz00zz__rgc_dfaz00,
		BgL_bgl_za762stateza7d2trans1748z00,
		BGl_z62statezd2transitionszb0zz__rgc_dfaz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1705z00zz__rgc_dfaz00,
		BgL_bgl_string1705za700za7za7_1749za7, "<", 1);
	      DEFINE_STRING(BGl_string1708z00zz__rgc_dfaz00,
		BgL_bgl_string1708za700za7za7_1750za7, "let", 3);
	      DEFINE_STRING(BGl_string1712z00zz__rgc_dfaz00,
		BgL_bgl_string1712za700za7za7_1751za7, "c", 1);
	      DEFINE_STRING(BGl_string1715z00zz__rgc_dfaz00,
		BgL_bgl_string1715za700za7za7_1752za7, "integer->char", 13);
	      DEFINE_STRING(BGl_string1719z00zz__rgc_dfaz00,
		BgL_bgl_string1719za700za7za7_1753za7, "or", 2);
	      DEFINE_STRING(BGl_string1722z00zz__rgc_dfaz00,
		BgL_bgl_string1722za700za7za7_1754za7, "char-alphabetic?", 16);
	      DEFINE_STRING(BGl_string1725z00zz__rgc_dfaz00,
		BgL_bgl_string1725za700za7za7_1755za7, "char-numeric?", 13);
	      DEFINE_STRING(BGl_string1726z00zz__rgc_dfaz00,
		BgL_bgl_string1726za700za7za7_1756za7, "ascii", 5);
	      DEFINE_STRING(BGl_string1727z00zz__rgc_dfaz00,
		BgL_bgl_string1727za700za7za7_1757za7, "special", 7);
	      DEFINE_STRING(BGl_string1728z00zz__rgc_dfaz00,
		BgL_bgl_string1728za700za7za7_1758za7, "]", 1);
	      DEFINE_STRING(BGl_string1729z00zz__rgc_dfaz00,
		BgL_bgl_string1729za700za7za7_1759za7, "  -->  ", 7);
	      DEFINE_STRING(BGl_string1732z00zz__rgc_dfaz00,
		BgL_bgl_string1732za700za7za7_1760za7, "__state-number", 14);
	      DEFINE_STRING(BGl_string1735z00zz__rgc_dfaz00,
		BgL_bgl_string1735za700za7za7_1761za7, "cdr", 3);
	      DEFINE_STRING(BGl_string1738z00zz__rgc_dfaz00,
		BgL_bgl_string1738za700za7za7_1762za7, "__state-transitions", 19);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2initialzd2statezd2envzd2zz__rgc_dfaz00,
		BgL_bgl_za762getza7d2initial1763z00,
		BGl_z62getzd2initialzd2statez62zz__rgc_dfaz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1740z00zz__rgc_dfaz00,
		BgL_bgl_string1740za700za7za7_1764za7, "state", 5);
	      DEFINE_STRING(BGl_string1741z00zz__rgc_dfaz00,
		BgL_bgl_string1741za700za7za7_1765za7,
		"==================================================", 50);
	      DEFINE_STRING(BGl_string1742z00zz__rgc_dfaz00,
		BgL_bgl_string1742za700za7za7_1766za7, "__rgc_dfa", 9);
	      DEFINE_STRING(BGl_string1671z00zz__rgc_dfaz00,
		BgL_bgl_string1671za700za7za7_1767za7, "/tmp/bigloo/runtime/Rgc/rgcdfa.scm",
		34);
	      DEFINE_STRING(BGl_string1672z00zz__rgc_dfaz00,
		BgL_bgl_string1672za700za7za7_1768za7, "&node->dfa", 10);
	      DEFINE_STRING(BGl_string1673z00zz__rgc_dfaz00,
		BgL_bgl_string1673za700za7za7_1769za7, "vector", 6);
	      DEFINE_STRING(BGl_string1675z00zz__rgc_dfaz00,
		BgL_bgl_string1675za700za7za7_1770za7, "__state", 7);
	      DEFINE_STRING(BGl_string1676z00zz__rgc_dfaz00,
		BgL_bgl_string1676za700za7za7_1771za7, "STATE-", 6);
	      DEFINE_STRING(BGl_string1677z00zz__rgc_dfaz00,
		BgL_bgl_string1677za700za7za7_1772za7, "-", 1);
	      DEFINE_STRING(BGl_string1678z00zz__rgc_dfaz00,
		BgL_bgl_string1678za700za7za7_1773za7,
		"========= DFA ====================================", 50);
	      DEFINE_STRING(BGl_string1679z00zz__rgc_dfaz00,
		BgL_bgl_string1679za700za7za7_1774za7, "state: ", 7);
	      DEFINE_STRING(BGl_string1682z00zz__rgc_dfaz00,
		BgL_bgl_string1682za700za7za7_1775za7, "for-each", 8);
	      DEFINE_STRING(BGl_string1685z00zz__rgc_dfaz00,
		BgL_bgl_string1685za700za7za7_1776za7, "lambda", 6);
	      DEFINE_STRING(BGl_string1688z00zz__rgc_dfaz00,
		BgL_bgl_string1688za700za7za7_1777za7, "trans", 5);
	      DEFINE_STRING(BGl_string1691z00zz__rgc_dfaz00,
		BgL_bgl_string1691za700za7za7_1778za7, "print", 5);
	      DEFINE_STRING(BGl_string1692z00zz__rgc_dfaz00,
		BgL_bgl_string1692za700za7za7_1779za7, "   ", 3);
	      DEFINE_STRING(BGl_string1695z00zz__rgc_dfaz00,
		BgL_bgl_string1695za700za7za7_1780za7, "car", 3);
	      DEFINE_STRING(BGl_string1696z00zz__rgc_dfaz00,
		BgL_bgl_string1696za700za7za7_1781za7, " [", 2);
	      DEFINE_STRING(BGl_string1699z00zz__rgc_dfaz00,
		BgL_bgl_string1699za700za7za7_1782za7, "if", 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_statezf3zd2envz21zz__rgc_dfaz00,
		BgL_bgl_za762stateza7f3za791za7za71783za7, BGl_z62statezf3z91zz__rgc_dfaz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_statezd2positionszd2envz00zz__rgc_dfaz00,
		BgL_bgl_za762stateza7d2posit1784z00,
		BGl_z62statezd2positionszb0zz__rgc_dfaz00, 0L, BUNSPEC, 1);
	extern obj_t BGl_rgcsetzd2equalzf3zd2envzf3zz__rgc_setz00;

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_symbol1674z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_za2statesza2z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_symbol1681z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_symbol1684z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_symbol1687z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_symbol1690z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_symbol1694z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_symbol1698z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_za2initialzd2stateza2zd2zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_list1700z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_list1703z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_list1706z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_list1709z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_list1710z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_list1713z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_list1716z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_list1717z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_list1720z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_list1723z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_list1730z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_list1733z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_list1736z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_list1680z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_list1683z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_list1686z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_list1689z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_list1693z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_list1697z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_za2statezd2numza2zd2zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_symbol1701z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_symbol1704z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_symbol1707z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_symbol1711z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_symbol1714z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_symbol1718z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_symbol1721z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_symbol1724z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_symbol1731z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_symbol1734z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_symbol1737z00zz__rgc_dfaz00));
		     ADD_ROOT((void *) (&BGl_symbol1739z00zz__rgc_dfaz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__rgc_dfaz00(long
		BgL_checksumz00_2149, char *BgL_fromz00_2150)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__rgc_dfaz00))
				{
					BGl_requirezd2initializa7ationz75zz__rgc_dfaz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__rgc_dfaz00();
					BGl_cnstzd2initzd2zz__rgc_dfaz00();
					BGl_importedzd2moduleszd2initz00zz__rgc_dfaz00();
					return BGl_toplevelzd2initzd2zz__rgc_dfaz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__rgc_dfaz00(void)
	{
		{	/* Rgc/rgcdfa.scm 15 */
			BGl_symbol1674z00zz__rgc_dfaz00 =
				bstring_to_symbol(BGl_string1675z00zz__rgc_dfaz00);
			BGl_symbol1681z00zz__rgc_dfaz00 =
				bstring_to_symbol(BGl_string1682z00zz__rgc_dfaz00);
			BGl_symbol1684z00zz__rgc_dfaz00 =
				bstring_to_symbol(BGl_string1685z00zz__rgc_dfaz00);
			BGl_symbol1687z00zz__rgc_dfaz00 =
				bstring_to_symbol(BGl_string1688z00zz__rgc_dfaz00);
			BGl_list1686z00zz__rgc_dfaz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1687z00zz__rgc_dfaz00, BNIL);
			BGl_symbol1690z00zz__rgc_dfaz00 =
				bstring_to_symbol(BGl_string1691z00zz__rgc_dfaz00);
			BGl_symbol1694z00zz__rgc_dfaz00 =
				bstring_to_symbol(BGl_string1695z00zz__rgc_dfaz00);
			BGl_list1693z00zz__rgc_dfaz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1694z00zz__rgc_dfaz00,
				MAKE_YOUNG_PAIR(BGl_symbol1687z00zz__rgc_dfaz00, BNIL));
			BGl_symbol1698z00zz__rgc_dfaz00 =
				bstring_to_symbol(BGl_string1699z00zz__rgc_dfaz00);
			BGl_symbol1701z00zz__rgc_dfaz00 =
				bstring_to_symbol(BGl_string1702z00zz__rgc_dfaz00);
			BGl_symbol1704z00zz__rgc_dfaz00 =
				bstring_to_symbol(BGl_string1705z00zz__rgc_dfaz00);
			BGl_list1703z00zz__rgc_dfaz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1704z00zz__rgc_dfaz00,
				MAKE_YOUNG_PAIR(BGl_list1693z00zz__rgc_dfaz00,
					MAKE_YOUNG_PAIR(BINT(256L), BNIL)));
			BGl_symbol1707z00zz__rgc_dfaz00 =
				bstring_to_symbol(BGl_string1708z00zz__rgc_dfaz00);
			BGl_symbol1711z00zz__rgc_dfaz00 =
				bstring_to_symbol(BGl_string1712z00zz__rgc_dfaz00);
			BGl_symbol1714z00zz__rgc_dfaz00 =
				bstring_to_symbol(BGl_string1715z00zz__rgc_dfaz00);
			BGl_list1713z00zz__rgc_dfaz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1714z00zz__rgc_dfaz00,
				MAKE_YOUNG_PAIR(BGl_list1693z00zz__rgc_dfaz00, BNIL));
			BGl_list1710z00zz__rgc_dfaz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1711z00zz__rgc_dfaz00,
				MAKE_YOUNG_PAIR(BGl_list1713z00zz__rgc_dfaz00, BNIL));
			BGl_list1709z00zz__rgc_dfaz00 =
				MAKE_YOUNG_PAIR(BGl_list1710z00zz__rgc_dfaz00, BNIL);
			BGl_symbol1718z00zz__rgc_dfaz00 =
				bstring_to_symbol(BGl_string1719z00zz__rgc_dfaz00);
			BGl_symbol1721z00zz__rgc_dfaz00 =
				bstring_to_symbol(BGl_string1722z00zz__rgc_dfaz00);
			BGl_list1720z00zz__rgc_dfaz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1721z00zz__rgc_dfaz00,
				MAKE_YOUNG_PAIR(BGl_symbol1711z00zz__rgc_dfaz00, BNIL));
			BGl_symbol1724z00zz__rgc_dfaz00 =
				bstring_to_symbol(BGl_string1725z00zz__rgc_dfaz00);
			BGl_list1723z00zz__rgc_dfaz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1724z00zz__rgc_dfaz00,
				MAKE_YOUNG_PAIR(BGl_symbol1711z00zz__rgc_dfaz00, BNIL));
			BGl_list1717z00zz__rgc_dfaz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1718z00zz__rgc_dfaz00,
				MAKE_YOUNG_PAIR(BGl_list1720z00zz__rgc_dfaz00,
					MAKE_YOUNG_PAIR(BGl_list1723z00zz__rgc_dfaz00, BNIL)));
			BGl_list1716z00zz__rgc_dfaz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1698z00zz__rgc_dfaz00,
				MAKE_YOUNG_PAIR(BGl_list1717z00zz__rgc_dfaz00,
					MAKE_YOUNG_PAIR(BGl_symbol1711z00zz__rgc_dfaz00,
						MAKE_YOUNG_PAIR(BGl_string1726z00zz__rgc_dfaz00, BNIL))));
			BGl_list1706z00zz__rgc_dfaz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1707z00zz__rgc_dfaz00,
				MAKE_YOUNG_PAIR(BGl_list1709z00zz__rgc_dfaz00,
					MAKE_YOUNG_PAIR(BGl_list1716z00zz__rgc_dfaz00, BNIL)));
			BGl_list1700z00zz__rgc_dfaz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1701z00zz__rgc_dfaz00,
				MAKE_YOUNG_PAIR(BGl_list1703z00zz__rgc_dfaz00,
					MAKE_YOUNG_PAIR(BGl_list1706z00zz__rgc_dfaz00, BNIL)));
			BGl_list1697z00zz__rgc_dfaz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1698z00zz__rgc_dfaz00,
				MAKE_YOUNG_PAIR(BGl_list1700z00zz__rgc_dfaz00,
					MAKE_YOUNG_PAIR(BGl_string1727z00zz__rgc_dfaz00, BNIL)));
			BGl_symbol1731z00zz__rgc_dfaz00 =
				bstring_to_symbol(BGl_string1732z00zz__rgc_dfaz00);
			BGl_symbol1734z00zz__rgc_dfaz00 =
				bstring_to_symbol(BGl_string1735z00zz__rgc_dfaz00);
			BGl_list1733z00zz__rgc_dfaz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1734z00zz__rgc_dfaz00,
				MAKE_YOUNG_PAIR(BGl_symbol1687z00zz__rgc_dfaz00, BNIL));
			BGl_list1730z00zz__rgc_dfaz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1731z00zz__rgc_dfaz00,
				MAKE_YOUNG_PAIR(BGl_list1733z00zz__rgc_dfaz00, BNIL));
			BGl_list1689z00zz__rgc_dfaz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1690z00zz__rgc_dfaz00,
				MAKE_YOUNG_PAIR(BGl_string1692z00zz__rgc_dfaz00,
					MAKE_YOUNG_PAIR(BGl_list1693z00zz__rgc_dfaz00,
						MAKE_YOUNG_PAIR(BGl_string1696z00zz__rgc_dfaz00,
							MAKE_YOUNG_PAIR(BGl_list1697z00zz__rgc_dfaz00,
								MAKE_YOUNG_PAIR(BGl_string1728z00zz__rgc_dfaz00,
									MAKE_YOUNG_PAIR(BGl_string1729z00zz__rgc_dfaz00,
										MAKE_YOUNG_PAIR(BGl_list1730z00zz__rgc_dfaz00, BNIL))))))));
			BGl_list1683z00zz__rgc_dfaz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1684z00zz__rgc_dfaz00,
				MAKE_YOUNG_PAIR(BGl_list1686z00zz__rgc_dfaz00,
					MAKE_YOUNG_PAIR(BGl_list1689z00zz__rgc_dfaz00, BNIL)));
			BGl_symbol1737z00zz__rgc_dfaz00 =
				bstring_to_symbol(BGl_string1738z00zz__rgc_dfaz00);
			BGl_symbol1739z00zz__rgc_dfaz00 =
				bstring_to_symbol(BGl_string1740z00zz__rgc_dfaz00);
			BGl_list1736z00zz__rgc_dfaz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1737z00zz__rgc_dfaz00,
				MAKE_YOUNG_PAIR(BGl_symbol1739z00zz__rgc_dfaz00, BNIL));
			return (BGl_list1680z00zz__rgc_dfaz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1681z00zz__rgc_dfaz00,
					MAKE_YOUNG_PAIR(BGl_list1683z00zz__rgc_dfaz00,
						MAKE_YOUNG_PAIR(BGl_list1736z00zz__rgc_dfaz00, BNIL))), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__rgc_dfaz00(void)
	{
		{	/* Rgc/rgcdfa.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__rgc_dfaz00(void)
	{
		{	/* Rgc/rgcdfa.scm 15 */
			BGl_bucketzd2lenzd2zz__rgc_dfaz00 = 64L;
			BGl_za2initialzd2stateza2zd2zz__rgc_dfaz00 = BUNSPEC;
			BGl_za2statesza2z00zz__rgc_dfaz00 = BUNSPEC;
			return (BGl_za2statezd2numza2zd2zz__rgc_dfaz00 = BUNSPEC, BUNSPEC);
		}

	}



/* node->dfa */
	BGL_EXPORTED_DEF obj_t BGl_nodezd2ze3dfaz31zz__rgc_dfaz00(obj_t
		BgL_rootz00_17, obj_t BgL_followposz00_18, obj_t BgL_positionsz00_19)
	{
		{	/* Rgc/rgcdfa.scm 71 */
			BGl_initzd2statesz12zc0zz__rgc_dfaz00();
			{	/* Rgc/rgcdfa.scm 78 */
				obj_t BgL_initialzd2statezd2_1239;

				{	/* Rgc/rgcdfa.scm 78 */
					obj_t BgL_arg1219z00_1270;

					BgL_arg1219z00_1270 =
						STRUCT_REF(((obj_t) BgL_rootz00_17), (int) (0L));
					BgL_initialzd2statezd2_1239 =
						BGl_makezd2statezd2zz__rgc_dfaz00(BgL_arg1219z00_1270);
				}
				BGl_za2initialzd2stateza2zd2zz__rgc_dfaz00 =
					BgL_initialzd2statezd2_1239;
				{	/* Rgc/rgcdfa.scm 81 */
					obj_t BgL_g1039z00_1241;

					{	/* Rgc/rgcdfa.scm 81 */
						obj_t BgL_list1218z00_1269;

						BgL_list1218z00_1269 =
							MAKE_YOUNG_PAIR(BgL_initialzd2statezd2_1239, BNIL);
						BgL_g1039z00_1241 = BgL_list1218z00_1269;
					}
					{
						obj_t BgL_dzd2stateszd2_1243;

						BgL_dzd2stateszd2_1243 = BgL_g1039z00_1241;
					BgL_zc3z04anonymousza31204ze3z87_1244:
						if (NULLP(BgL_dzd2stateszd2_1243))
							{	/* Rgc/rgcdfa.scm 83 */
								return
									BGl_hashtablezd2ze3listz31zz__hashz00
									(BGl_za2statesza2z00zz__rgc_dfaz00);
							}
						else
							{	/* Rgc/rgcdfa.scm 87 */
								obj_t BgL_newzd2dzd2statesz00_2092;

								{	/* Rgc/rgcdfa.scm 87 */
									obj_t BgL_cellvalz00_2239;

									BgL_cellvalz00_2239 = CDR(((obj_t) BgL_dzd2stateszd2_1243));
									BgL_newzd2dzd2statesz00_2092 = MAKE_CELL(BgL_cellvalz00_2239);
								}
								{	/* Rgc/rgcdfa.scm 87 */
									obj_t BgL_tz00_1247;

									BgL_tz00_1247 = CAR(((obj_t) BgL_dzd2stateszd2_1243));
									{	/* Rgc/rgcdfa.scm 88 */
										obj_t BgL_tzd2positionszd2_1248;

										BgL_tzd2positionszd2_1248 =
											STRUCT_REF(((obj_t) BgL_tz00_1247), (int) (3L));
										{	/* Rgc/rgcdfa.scm 89 */

											{	/* Rgc/rgcdfa.scm 95 */
												obj_t BgL_arg1208z00_1250;

												{	/* Rgc/rgcdfa.scm 245 */
													obj_t BgL_symbolzd2setzd2_1761;

													{	/* Rgc/rgcdfa.scm 245 */
														obj_t BgL_arg1272z00_1762;

														BgL_arg1272z00_1762 =
															BGl_treezd2maxzd2charz00zz__rgc_rulesz00();
														BgL_symbolzd2setzd2_1761 =
															BGl_makezd2rgcsetzd2zz__rgc_setz00(
															(long) CINT(BgL_arg1272z00_1762));
													}
													{	/* Rgc/rgcdfa.scm 247 */
														obj_t BgL_zc3z04anonymousza31253ze3z87_2064;

														BgL_zc3z04anonymousza31253ze3z87_2064 =
															MAKE_FX_PROCEDURE
															(BGl_z62zc3z04anonymousza31253ze3ze5zz__rgc_dfaz00,
															(int) (1L), (int) (2L));
														PROCEDURE_SET(BgL_zc3z04anonymousza31253ze3z87_2064,
															(int) (0L), BgL_positionsz00_19);
														PROCEDURE_SET(BgL_zc3z04anonymousza31253ze3z87_2064,
															(int) (1L), BgL_symbolzd2setzd2_1761);
														BGl_forzd2eachzd2rgcsetz00zz__rgc_setz00
															(BgL_zc3z04anonymousza31253ze3z87_2064,
															BgL_tzd2positionszd2_1248);
													}
													BgL_arg1208z00_1250 = BgL_symbolzd2setzd2_1761;
												}
												{	/* Rgc/rgcdfa.scm 96 */
													obj_t BgL_zc3z04anonymousza31209ze3z87_2063;

													BgL_zc3z04anonymousza31209ze3z87_2063 =
														MAKE_FX_PROCEDURE
														(BGl_z62zc3z04anonymousza31209ze3ze5zz__rgc_dfaz00,
														(int) (1L), (int) (6L));
													PROCEDURE_SET(BgL_zc3z04anonymousza31209ze3z87_2063,
														(int) (0L),
														BINT(VECTOR_LENGTH(BgL_positionsz00_19)));
													PROCEDURE_SET(BgL_zc3z04anonymousza31209ze3z87_2063,
														(int) (1L), BgL_followposz00_18);
													PROCEDURE_SET(BgL_zc3z04anonymousza31209ze3z87_2063,
														(int) (2L), BgL_positionsz00_19);
													PROCEDURE_SET(BgL_zc3z04anonymousza31209ze3z87_2063,
														(int) (3L), BgL_tzd2positionszd2_1248);
													PROCEDURE_SET(BgL_zc3z04anonymousza31209ze3z87_2063,
														(int) (4L), ((obj_t) BgL_newzd2dzd2statesz00_2092));
													PROCEDURE_SET(BgL_zc3z04anonymousza31209ze3z87_2063,
														(int) (5L), BgL_tz00_1247);
													BGl_forzd2eachzd2rgcsetz00zz__rgc_setz00
														(BgL_zc3z04anonymousza31209ze3z87_2063,
														BgL_arg1208z00_1250);
											}}
											{
												obj_t BgL_dzd2stateszd2_2277;

												BgL_dzd2stateszd2_2277 =
													CELL_REF(BgL_newzd2dzd2statesz00_2092);
												BgL_dzd2stateszd2_1243 = BgL_dzd2stateszd2_2277;
												goto BgL_zc3z04anonymousza31204ze3z87_1244;
											}
										}
									}
								}
							}
					}
				}
			}
		}

	}



/* &node->dfa */
	obj_t BGl_z62nodezd2ze3dfaz53zz__rgc_dfaz00(obj_t BgL_envz00_2065,
		obj_t BgL_rootz00_2066, obj_t BgL_followposz00_2067,
		obj_t BgL_positionsz00_2068)
	{
		{	/* Rgc/rgcdfa.scm 71 */
			{	/* Rgc/rgcdfa.scm 73 */
				obj_t BgL_auxz00_2285;
				obj_t BgL_auxz00_2278;

				if (VECTORP(BgL_positionsz00_2068))
					{	/* Rgc/rgcdfa.scm 73 */
						BgL_auxz00_2285 = BgL_positionsz00_2068;
					}
				else
					{
						obj_t BgL_auxz00_2288;

						BgL_auxz00_2288 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1671z00zz__rgc_dfaz00,
							BINT(2485L), BGl_string1672z00zz__rgc_dfaz00,
							BGl_string1673z00zz__rgc_dfaz00, BgL_positionsz00_2068);
						FAILURE(BgL_auxz00_2288, BFALSE, BFALSE);
					}
				if (VECTORP(BgL_followposz00_2067))
					{	/* Rgc/rgcdfa.scm 73 */
						BgL_auxz00_2278 = BgL_followposz00_2067;
					}
				else
					{
						obj_t BgL_auxz00_2281;

						BgL_auxz00_2281 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1671z00zz__rgc_dfaz00,
							BINT(2485L), BGl_string1672z00zz__rgc_dfaz00,
							BGl_string1673z00zz__rgc_dfaz00, BgL_followposz00_2067);
						FAILURE(BgL_auxz00_2281, BFALSE, BFALSE);
					}
				return
					BGl_nodezd2ze3dfaz31zz__rgc_dfaz00(BgL_rootz00_2066, BgL_auxz00_2278,
					BgL_auxz00_2285);
			}
		}

	}



/* &<@anonymous:1209> */
	obj_t BGl_z62zc3z04anonymousza31209ze3ze5zz__rgc_dfaz00(obj_t BgL_envz00_2069,
		obj_t BgL_az00_2076)
	{
		{	/* Rgc/rgcdfa.scm 95 */
			{	/* Rgc/rgcdfa.scm 96 */
				long BgL_positionszd2numzd2_2070;
				obj_t BgL_followposz00_2071;
				obj_t BgL_positionsz00_2072;
				obj_t BgL_tzd2positionszd2_2073;
				obj_t BgL_newzd2dzd2statesz00_2074;
				obj_t BgL_tz00_2075;

				BgL_positionszd2numzd2_2070 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_2069, (int) (0L)));
				BgL_followposz00_2071 =
					((obj_t) PROCEDURE_REF(BgL_envz00_2069, (int) (1L)));
				BgL_positionsz00_2072 =
					((obj_t) PROCEDURE_REF(BgL_envz00_2069, (int) (2L)));
				BgL_tzd2positionszd2_2073 = PROCEDURE_REF(BgL_envz00_2069, (int) (3L));
				BgL_newzd2dzd2statesz00_2074 =
					PROCEDURE_REF(BgL_envz00_2069, (int) (4L));
				BgL_tz00_2075 = PROCEDURE_REF(BgL_envz00_2069, (int) (5L));
				{	/* Rgc/rgcdfa.scm 96 */
					obj_t BgL_uz00_2136;
					obj_t BgL_uzd2emptyzf3z21_2137;

					BgL_uz00_2136 =
						BGl_makezd2rgcsetzd2zz__rgc_setz00(BgL_positionszd2numzd2_2070);
					BgL_uzd2emptyzf3z21_2137 = MAKE_CELL(BTRUE);
					{	/* Rgc/rgcdfa.scm 100 */
						obj_t BgL_zc3z04anonymousza31211ze3z87_2138;

						BgL_zc3z04anonymousza31211ze3z87_2138 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31211ze3ze5zz__rgc_dfaz00, (int) (1L),
							(int) (5L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31211ze3z87_2138, (int) (0L),
							((obj_t) BgL_uzd2emptyzf3z21_2137));
						PROCEDURE_SET(BgL_zc3z04anonymousza31211ze3z87_2138, (int) (1L),
							BgL_followposz00_2071);
						PROCEDURE_SET(BgL_zc3z04anonymousza31211ze3z87_2138, (int) (2L),
							BgL_uz00_2136);
						PROCEDURE_SET(BgL_zc3z04anonymousza31211ze3z87_2138, (int) (3L),
							BgL_positionsz00_2072);
						PROCEDURE_SET(BgL_zc3z04anonymousza31211ze3z87_2138, (int) (4L),
							BgL_az00_2076);
						BGl_forzd2eachzd2rgcsetz00zz__rgc_setz00
							(BgL_zc3z04anonymousza31211ze3z87_2138,
							BgL_tzd2positionszd2_2073);
					}
					if (CBOOL(CELL_REF(BgL_uzd2emptyzf3z21_2137)))
						{	/* Rgc/rgcdfa.scm 107 */
							return BFALSE;
						}
					else
						{	/* Rgc/rgcdfa.scm 108 */
							obj_t BgL_usz00_2139;

							{	/* Rgc/rgcdfa.scm 108 */
								obj_t BgL_ostatez00_2140;

								BgL_ostatez00_2140 =
									BGl_hashtablezd2getzd2zz__hashz00
									(BGl_za2statesza2z00zz__rgc_dfaz00, BgL_uz00_2136);
								{	/* Rgc/rgcdfa.scm 110 */
									bool_t BgL_test1790z00_2327;

									if (STRUCTP(BgL_ostatez00_2140))
										{	/* Rgc/rgcdfa.scm 149 */
											BgL_test1790z00_2327 =
												(STRUCT_KEY(BgL_ostatez00_2140) ==
												BGl_symbol1674z00zz__rgc_dfaz00);
										}
									else
										{	/* Rgc/rgcdfa.scm 149 */
											BgL_test1790z00_2327 = ((bool_t) 0);
										}
									if (BgL_test1790z00_2327)
										{	/* Rgc/rgcdfa.scm 110 */
											BgL_usz00_2139 = BgL_ostatez00_2140;
										}
									else
										{	/* Rgc/rgcdfa.scm 111 */
											obj_t BgL_newzd2statezd2_2141;

											BgL_newzd2statezd2_2141 =
												BGl_makezd2statezd2zz__rgc_dfaz00(BgL_uz00_2136);
											{	/* Rgc/rgcdfa.scm 114 */
												obj_t BgL_auxz00_2142;

												BgL_auxz00_2142 =
													MAKE_YOUNG_PAIR(BgL_newzd2statezd2_2141,
													CELL_REF(BgL_newzd2dzd2statesz00_2074));
												CELL_SET(BgL_newzd2dzd2statesz00_2074, BgL_auxz00_2142);
											}
											BgL_usz00_2139 = BgL_newzd2statezd2_2141;
										}
								}
							}
							{	/* Rgc/rgcdfa.scm 239 */
								obj_t BgL_arg1244z00_2143;

								{	/* Rgc/rgcdfa.scm 239 */
									obj_t BgL_arg1248z00_2144;
									obj_t BgL_arg1249z00_2145;

									BgL_arg1248z00_2144 =
										MAKE_YOUNG_PAIR(BgL_az00_2076, BgL_usz00_2139);
									BgL_arg1249z00_2145 =
										STRUCT_REF(((obj_t) BgL_tz00_2075), (int) (2L));
									BgL_arg1244z00_2143 =
										MAKE_YOUNG_PAIR(BgL_arg1248z00_2144, BgL_arg1249z00_2145);
								}
								{	/* Rgc/rgcdfa.scm 149 */
									int BgL_auxz00_2341;
									obj_t BgL_tmpz00_2339;

									BgL_auxz00_2341 = (int) (2L);
									BgL_tmpz00_2339 = ((obj_t) BgL_tz00_2075);
									return
										STRUCT_SET(BgL_tmpz00_2339, BgL_auxz00_2341,
										BgL_arg1244z00_2143);
								}
							}
						}
				}
			}
		}

	}



/* &<@anonymous:1211> */
	obj_t BGl_z62zc3z04anonymousza31211ze3ze5zz__rgc_dfaz00(obj_t BgL_envz00_2080,
		obj_t BgL_pz00_2086)
	{
		{	/* Rgc/rgcdfa.scm 99 */
			{	/* Rgc/rgcdfa.scm 100 */
				obj_t BgL_uzd2emptyzf3z21_2081;
				obj_t BgL_followposz00_2082;
				obj_t BgL_uz00_2083;
				obj_t BgL_positionsz00_2084;
				obj_t BgL_az00_2085;

				BgL_uzd2emptyzf3z21_2081 = PROCEDURE_REF(BgL_envz00_2080, (int) (0L));
				BgL_followposz00_2082 =
					((obj_t) PROCEDURE_REF(BgL_envz00_2080, (int) (1L)));
				BgL_uz00_2083 = PROCEDURE_REF(BgL_envz00_2080, (int) (2L));
				BgL_positionsz00_2084 =
					((obj_t) PROCEDURE_REF(BgL_envz00_2080, (int) (3L)));
				BgL_az00_2085 = PROCEDURE_REF(BgL_envz00_2080, (int) (4L));
				{	/* Rgc/rgcdfa.scm 100 */
					bool_t BgL_test1792z00_2356;

					{	/* Rgc/rgcdfa.scm 100 */
						obj_t BgL_arg1216z00_2146;

						BgL_arg1216z00_2146 =
							VECTOR_REF(BgL_positionsz00_2084, (long) CINT(BgL_pz00_2086));
						BgL_test1792z00_2356 =
							((long) CINT(BgL_az00_2085) == (long) CINT(BgL_arg1216z00_2146));
					}
					if (BgL_test1792z00_2356)
						{	/* Rgc/rgcdfa.scm 100 */
							{	/* Rgc/rgcdfa.scm 102 */
								obj_t BgL_auxz00_2147;

								BgL_auxz00_2147 = BFALSE;
								CELL_SET(BgL_uzd2emptyzf3z21_2081, BgL_auxz00_2147);
							}
							return
								BGl_rgcsetzd2orz12zc0zz__rgc_setz00(BgL_uz00_2083,
								VECTOR_REF(BgL_followposz00_2082, (long) CINT(BgL_pz00_2086)));
						}
					else
						{	/* Rgc/rgcdfa.scm 100 */
							return BFALSE;
						}
				}
			}
		}

	}



/* &<@anonymous:1253> */
	obj_t BGl_z62zc3z04anonymousza31253ze3ze5zz__rgc_dfaz00(obj_t BgL_envz00_2088,
		obj_t BgL_iz00_2091)
	{
		{	/* Rgc/rgcdfa.scm 246 */
			{	/* Rgc/rgcdfa.scm 247 */
				obj_t BgL_positionsz00_2089;
				obj_t BgL_symbolzd2setzd2_2090;

				BgL_positionsz00_2089 =
					((obj_t) PROCEDURE_REF(BgL_envz00_2088, (int) (0L)));
				BgL_symbolzd2setzd2_2090 = PROCEDURE_REF(BgL_envz00_2088, (int) (1L));
				{	/* Rgc/rgcdfa.scm 247 */
					obj_t BgL_arg1268z00_2148;

					BgL_arg1268z00_2148 =
						VECTOR_REF(BgL_positionsz00_2089, (long) CINT(BgL_iz00_2091));
					return
						BGl_rgcsetzd2addz12zc0zz__rgc_setz00(BgL_symbolzd2setzd2_2090,
						(long) CINT(BgL_arg1268z00_2148));
		}}}

	}



/* reset-dfa! */
	BGL_EXPORTED_DEF obj_t BGl_resetzd2dfaz12zc0zz__rgc_dfaz00(void)
	{
		{	/* Rgc/rgcdfa.scm 128 */
			BGl_za2initialzd2stateza2zd2zz__rgc_dfaz00 = BUNSPEC;
			BGl_za2statesza2z00zz__rgc_dfaz00 = BUNSPEC;
			return (BGl_za2statezd2numza2zd2zz__rgc_dfaz00 = BUNSPEC, BUNSPEC);
		}

	}



/* &reset-dfa! */
	obj_t BGl_z62resetzd2dfaz12za2zz__rgc_dfaz00(obj_t BgL_envz00_2094)
	{
		{	/* Rgc/rgcdfa.scm 128 */
			return BGl_resetzd2dfaz12zc0zz__rgc_dfaz00();
		}

	}



/* get-initial-state */
	BGL_EXPORTED_DEF obj_t BGl_getzd2initialzd2statez00zz__rgc_dfaz00(void)
	{
		{	/* Rgc/rgcdfa.scm 141 */
			return BGl_za2initialzd2stateza2zd2zz__rgc_dfaz00;
		}

	}



/* &get-initial-state */
	obj_t BGl_z62getzd2initialzd2statez62zz__rgc_dfaz00(obj_t BgL_envz00_2095)
	{
		{	/* Rgc/rgcdfa.scm 141 */
			return BGl_getzd2initialzd2statez00zz__rgc_dfaz00();
		}

	}



/* init-states! */
	obj_t BGl_initzd2statesz12zc0zz__rgc_dfaz00(void)
	{
		{	/* Rgc/rgcdfa.scm 165 */
			BGl_za2statezd2numza2zd2zz__rgc_dfaz00 = BINT(-1L);
			{	/* Rgc/rgcdfa.scm 167 */
				obj_t BgL_list1232z00_1284;

				{	/* Rgc/rgcdfa.scm 167 */
					obj_t BgL_arg1233z00_1285;

					{	/* Rgc/rgcdfa.scm 167 */
						obj_t BgL_arg1234z00_1286;

						{	/* Rgc/rgcdfa.scm 167 */
							obj_t BgL_arg1236z00_1287;

							BgL_arg1236z00_1287 =
								MAKE_YOUNG_PAIR(BGl_rgcsetzd2ze3hashzd2envze3zz__rgc_setz00,
								BNIL);
							BgL_arg1234z00_1286 =
								MAKE_YOUNG_PAIR(BGl_rgcsetzd2equalzf3zd2envzf3zz__rgc_setz00,
								BgL_arg1236z00_1287);
						}
						BgL_arg1233z00_1285 =
							MAKE_YOUNG_PAIR(BINT(64L), BgL_arg1234z00_1286);
					}
					BgL_list1232z00_1284 =
						MAKE_YOUNG_PAIR(BINT(1024L), BgL_arg1233z00_1285);
				}
				return (BGl_za2statesza2z00zz__rgc_dfaz00 =
					BGl_makezd2hashtablezd2zz__hashz00(BgL_list1232z00_1284), BUNSPEC);
			}
		}

	}



/* make-state */
	obj_t BGl_makezd2statezd2zz__rgc_dfaz00(obj_t BgL_positionsz00_38)
	{
		{	/* Rgc/rgcdfa.scm 187 */
			{	/* Rgc/rgcdfa.scm 188 */
				obj_t BgL_numz00_1288;

				BGl_za2statezd2numza2zd2zz__rgc_dfaz00 =
					ADDFX(BGl_za2statezd2numza2zd2zz__rgc_dfaz00, BINT(1L));
				BgL_numz00_1288 = BGl_za2statezd2numza2zd2zz__rgc_dfaz00;
				{	/* Rgc/rgcdfa.scm 188 */
					obj_t BgL_namez00_1289;

					{	/* Rgc/rgcdfa.scm 189 */
						obj_t BgL_arg1239z00_1292;

						{	/* Rgc/rgcdfa.scm 189 */
							obj_t BgL_arg1242z00_1293;

							{	/* Ieee/number.scm 174 */

								BgL_arg1242z00_1293 =
									BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00
									(BgL_numz00_1288, BINT(10L));
							}
							BgL_arg1239z00_1292 =
								string_append_3(BGl_string1676z00zz__rgc_dfaz00,
								BgL_arg1242z00_1293, BGl_string1677z00zz__rgc_dfaz00);
						}
						BgL_namez00_1289 =
							BGl_gensymz00zz__r4_symbols_6_4z00(BgL_arg1239z00_1292);
					}
					{	/* Rgc/rgcdfa.scm 189 */
						obj_t BgL_statez00_1290;

						{	/* Rgc/rgcdfa.scm 149 */
							obj_t BgL_newz00_1804;

							BgL_newz00_1804 =
								create_struct(BGl_symbol1674z00zz__rgc_dfaz00, (int) (4L));
							{	/* Rgc/rgcdfa.scm 149 */
								int BgL_tmpz00_2392;

								BgL_tmpz00_2392 = (int) (3L);
								STRUCT_SET(BgL_newz00_1804, BgL_tmpz00_2392,
									BgL_positionsz00_38);
							}
							{	/* Rgc/rgcdfa.scm 149 */
								int BgL_tmpz00_2395;

								BgL_tmpz00_2395 = (int) (2L);
								STRUCT_SET(BgL_newz00_1804, BgL_tmpz00_2395, BNIL);
							}
							{	/* Rgc/rgcdfa.scm 149 */
								int BgL_tmpz00_2398;

								BgL_tmpz00_2398 = (int) (1L);
								STRUCT_SET(BgL_newz00_1804, BgL_tmpz00_2398, BgL_numz00_1288);
							}
							{	/* Rgc/rgcdfa.scm 149 */
								int BgL_tmpz00_2401;

								BgL_tmpz00_2401 = (int) (0L);
								STRUCT_SET(BgL_newz00_1804, BgL_tmpz00_2401, BgL_namez00_1289);
							}
							BgL_statez00_1290 = BgL_newz00_1804;
						}
						{	/* Rgc/rgcdfa.scm 190 */

							{	/* Rgc/rgcdfa.scm 192 */
								obj_t BgL_arg1238z00_1291;

								BgL_arg1238z00_1291 = STRUCT_REF(BgL_statez00_1290, (int) (3L));
								BGl_hashtablezd2putz12zc0zz__hashz00
									(BGl_za2statesza2z00zz__rgc_dfaz00, BgL_arg1238z00_1291,
									BgL_statez00_1290);
							}
							return BgL_statez00_1290;
						}
					}
				}
			}
		}

	}



/* state-positions */
	BGL_EXPORTED_DEF obj_t BGl_statezd2positionszd2zz__rgc_dfaz00(obj_t
		BgL_statez00_40)
	{
		{	/* Rgc/rgcdfa.scm 206 */
			return STRUCT_REF(((obj_t) BgL_statez00_40), (int) (3L));
		}

	}



/* &state-positions */
	obj_t BGl_z62statezd2positionszb0zz__rgc_dfaz00(obj_t BgL_envz00_2101,
		obj_t BgL_statez00_2102)
	{
		{	/* Rgc/rgcdfa.scm 206 */
			return BGl_statezd2positionszd2zz__rgc_dfaz00(BgL_statez00_2102);
		}

	}



/* state-name */
	BGL_EXPORTED_DEF obj_t BGl_statezd2namezd2zz__rgc_dfaz00(obj_t
		BgL_statez00_41)
	{
		{	/* Rgc/rgcdfa.scm 212 */
			return STRUCT_REF(((obj_t) BgL_statez00_41), (int) (0L));
		}

	}



/* &state-name */
	obj_t BGl_z62statezd2namezb0zz__rgc_dfaz00(obj_t BgL_envz00_2103,
		obj_t BgL_statez00_2104)
	{
		{	/* Rgc/rgcdfa.scm 212 */
			return BGl_statezd2namezd2zz__rgc_dfaz00(BgL_statez00_2104);
		}

	}



/* state-transitions */
	BGL_EXPORTED_DEF obj_t BGl_statezd2transitionszd2zz__rgc_dfaz00(obj_t
		BgL_statez00_43)
	{
		{	/* Rgc/rgcdfa.scm 224 */
			return STRUCT_REF(((obj_t) BgL_statez00_43), (int) (2L));
		}

	}



/* &state-transitions */
	obj_t BGl_z62statezd2transitionszb0zz__rgc_dfaz00(obj_t BgL_envz00_2105,
		obj_t BgL_statez00_2106)
	{
		{	/* Rgc/rgcdfa.scm 224 */
			return BGl_statezd2transitionszd2zz__rgc_dfaz00(BgL_statez00_2106);
		}

	}



/* state? */
	BGL_EXPORTED_DEF bool_t BGl_statezf3zf3zz__rgc_dfaz00(obj_t BgL_objz00_44)
	{
		{	/* Rgc/rgcdfa.scm 230 */
			if (STRUCTP(BgL_objz00_44))
				{	/* Rgc/rgcdfa.scm 149 */
					return (STRUCT_KEY(BgL_objz00_44) == BGl_symbol1674z00zz__rgc_dfaz00);
				}
			else
				{	/* Rgc/rgcdfa.scm 149 */
					return ((bool_t) 0);
				}
		}

	}



/* &state? */
	obj_t BGl_z62statezf3z91zz__rgc_dfaz00(obj_t BgL_envz00_2107,
		obj_t BgL_objz00_2108)
	{
		{	/* Rgc/rgcdfa.scm 230 */
			return BBOOL(BGl_statezf3zf3zz__rgc_dfaz00(BgL_objz00_2108));
		}

	}



/* print-dfa */
	BGL_EXPORTED_DEF obj_t BGl_printzd2dfazd2zz__rgc_dfaz00(obj_t BgL_dfaz00_50)
	{
		{	/* Rgc/rgcdfa.scm 254 */
			{	/* Rgc/rgcdfa.scm 255 */
				obj_t BgL_port1087z00_1306;

				{	/* Rgc/rgcdfa.scm 255 */
					obj_t BgL_tmpz00_2425;

					BgL_tmpz00_2425 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_port1087z00_1306 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_2425);
				}
				bgl_display_string(BGl_string1678z00zz__rgc_dfaz00,
					BgL_port1087z00_1306);
				bgl_display_char(((unsigned char) 10), BgL_port1087z00_1306);
			}
			{
				obj_t BgL_l1089z00_1308;

				BgL_l1089z00_1308 = BgL_dfaz00_50;
			BgL_zc3z04anonymousza31273ze3z87_1309:
				if (PAIRP(BgL_l1089z00_1308))
					{	/* Rgc/rgcdfa.scm 256 */
						{	/* Rgc/rgcdfa.scm 257 */
							obj_t BgL_statez00_1311;

							BgL_statez00_1311 = CAR(BgL_l1089z00_1308);
							{	/* Rgc/rgcdfa.scm 257 */
								obj_t BgL_port1088z00_1312;

								{	/* Rgc/rgcdfa.scm 257 */
									obj_t BgL_tmpz00_2433;

									BgL_tmpz00_2433 = BGL_CURRENT_DYNAMIC_ENV();
									BgL_port1088z00_1312 =
										BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_2433);
								}
								bgl_display_string(BGl_string1679z00zz__rgc_dfaz00,
									BgL_port1088z00_1312);
								{	/* Rgc/rgcdfa.scm 257 */
									obj_t BgL_arg1284z00_1313;

									BgL_arg1284z00_1313 =
										STRUCT_REF(((obj_t) BgL_statez00_1311), (int) (1L));
									bgl_display_obj(BgL_arg1284z00_1313, BgL_port1088z00_1312);
								}
								bgl_display_char(((unsigned char) 10), BgL_port1088z00_1312);
							} BGl_list1680z00zz__rgc_dfaz00;
						}
						{
							obj_t BgL_l1089z00_2442;

							BgL_l1089z00_2442 = CDR(BgL_l1089z00_1308);
							BgL_l1089z00_1308 = BgL_l1089z00_2442;
							goto BgL_zc3z04anonymousza31273ze3z87_1309;
						}
					}
				else
					{	/* Rgc/rgcdfa.scm 256 */
						((bool_t) 1);
					}
			}
			{	/* Rgc/rgcdfa.scm 273 */
				obj_t BgL_port1091z00_1316;

				{	/* Rgc/rgcdfa.scm 273 */
					obj_t BgL_tmpz00_2444;

					BgL_tmpz00_2444 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_port1091z00_1316 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_2444);
				}
				bgl_display_string(BGl_string1741z00zz__rgc_dfaz00,
					BgL_port1091z00_1316);
				bgl_display_char(((unsigned char) 10), BgL_port1091z00_1316);
			}
			{	/* Rgc/rgcdfa.scm 274 */
				obj_t BgL_arg1305z00_1317;

				{	/* Rgc/rgcdfa.scm 274 */
					obj_t BgL_tmpz00_2449;

					BgL_tmpz00_2449 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg1305z00_1317 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_2449);
				}
				return bgl_display_char(((unsigned char) 10), BgL_arg1305z00_1317);
		}}

	}



/* &print-dfa */
	obj_t BGl_z62printzd2dfazb0zz__rgc_dfaz00(obj_t BgL_envz00_2109,
		obj_t BgL_dfaz00_2110)
	{
		{	/* Rgc/rgcdfa.scm 254 */
			return BGl_printzd2dfazd2zz__rgc_dfaz00(BgL_dfaz00_2110);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__rgc_dfaz00(void)
	{
		{	/* Rgc/rgcdfa.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__rgc_dfaz00(void)
	{
		{	/* Rgc/rgcdfa.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__rgc_dfaz00(void)
	{
		{	/* Rgc/rgcdfa.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__rgc_dfaz00(void)
	{
		{	/* Rgc/rgcdfa.scm 15 */
			BGl_modulezd2initializa7ationz75zz__rgc_configz00(428274736L,
				BSTRING_TO_STRING(BGl_string1742z00zz__rgc_dfaz00));
			BGl_modulezd2initializa7ationz75zz__rgc_setz00(225075643L,
				BSTRING_TO_STRING(BGl_string1742z00zz__rgc_dfaz00));
			BGl_modulezd2initializa7ationz75zz__rgc_rulesz00(181068393L,
				BSTRING_TO_STRING(BGl_string1742z00zz__rgc_dfaz00));
			return
				BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1742z00zz__rgc_dfaz00));
		}

	}

#ifdef __cplusplus
}
#endif
