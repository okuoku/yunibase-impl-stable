/*===========================================================================*/
/*   (Rgc/rgcrules.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Rgc/rgcrules.scm -indent -o objs/obj_u/Rgc/rgcrules.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___RGC_RULES_TYPE_DEFINITIONS
#define BGL___RGC_RULES_TYPE_DEFINITIONS
#endif													// BGL___RGC_RULES_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00(obj_t);
	static obj_t BGl_expandzd2matchzd2rulez00zz__rgc_rulesz00(long, obj_t, obj_t);
	static obj_t BGl_list2290z00zz__rgc_rulesz00 = BUNSPEC;
	extern obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_specialzd2matchzd2charzd2ze3rulezd2numberze3zz__rgc_rulesz00(int);
	static obj_t
		BGl_z62specialzd2matchzd2charzd2ze3rulezd2numberz81zz__rgc_rulesz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_specialzd2charzf3z21zz__rgc_rulesz00(int);
	static obj_t BGl_za2specialzd2stopzd2matchzd2charza2zd2zz__rgc_rulesz00 =
		BUNSPEC;
	static obj_t BGl_requirezd2initializa7ationz75zz__rgc_rulesz00 = BUNSPEC;
	extern obj_t BGl_rgcsetzd2notz12zc0zz__rgc_setz00(obj_t);
	static obj_t BGl_expandzd2butzd2zz__rgc_rulesz00(long, obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__rgc_rulesz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__rgc_posixz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__rgc_setz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__rgc_configz00(long, char *);
	static obj_t BGl_expandzd2submatchzd2zz__rgc_rulesz00(long, obj_t, obj_t,
		obj_t);
	static obj_t BGl_charzd2rangeze70z35zz__rgc_rulesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_expandzd2orzd2zz__rgc_rulesz00(long, obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zz__rgc_rulesz00(void);
	BGL_EXPORTED_DECL obj_t BGl_treezd2maxzd2charz00zz__rgc_rulesz00(void);
	static bool_t BGl_rgczd2charzf3ze70zc6zz__rgc_rulesz00(obj_t);
	static bool_t BGl_za2lockzd2submatchza2zd2zz__rgc_rulesz00;
	static obj_t BGl_z62ruleszd2ze3regularzd2treez81zz__rgc_rulesz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_rgcsetzd2ze3listz31zz__rgc_setz00(obj_t);
	static obj_t BGl_z62specialzd2charzf3z43zz__rgc_rulesz00(obj_t, obj_t);
	extern obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__rgc_rulesz00(void);
	extern obj_t bgl_reverse(obj_t);
	static obj_t BGl_genericzd2initzd2zz__rgc_rulesz00(void);
	static obj_t BGl_symbol2304z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_symbol2306z00zz__rgc_rulesz00 = BUNSPEC;
	static long BGl_za2submatchzd2countza2zd2zz__rgc_rulesz00 = 0L;
	static obj_t BGl_symbol2308z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_makezd2sequencezd2zz__rgc_rulesz00(obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_specialzd2charzd2matchzf3zf3zz__rgc_rulesz00(int);
	static long BGl_specialzd2charzd2numz00zz__rgc_rulesz00 = 0L;
	static obj_t BGl_importedzd2moduleszd2initz00zz__rgc_rulesz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__rgc_rulesz00(void);
	static obj_t BGl_objectzd2initzd2zz__rgc_rulesz00(void);
	static obj_t BGl_symbol2310z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t
		BGl_z62resetzd2specialzd2matchzd2charz12za2zz__rgc_rulesz00(obj_t);
	extern obj_t BGl_2zb2zb2zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_symbol2312z00zz__rgc_rulesz00 = BUNSPEC;
	extern obj_t BGl_2zd2zd2zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_symbol2314z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_symbol2317z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_symbol2319z00zz__rgc_rulesz00 = BUNSPEC;
	extern bool_t BGl_2zc3zc3zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t bgl_reverse_bang(obj_t);
	extern obj_t BGl_rgczd2downcasezd2zz__rgc_configz00(obj_t);
	static obj_t BGl_symbol2321z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_stringzd2rangeze70z35zz__rgc_rulesz00(obj_t, obj_t);
	static obj_t BGl_symbol2324z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_symbol2328z00zz__rgc_rulesz00 = BUNSPEC;
	extern obj_t BGl_rgcsetzd2andz12zc0zz__rgc_setz00(obj_t, obj_t);
	static long BGl_za2maxzd2rgczd2zd3zd2numza2z01zz__rgc_rulesz00 = 0L;
	static obj_t BGl_expandzd2uncasezd2zz__rgc_rulesz00(long, obj_t, obj_t);
	static obj_t BGl_z62treezd2maxzd2charz62zz__rgc_rulesz00(obj_t);
	static obj_t BGl_expandzd2atomzd2zz__rgc_rulesz00(long, obj_t, obj_t);
	static obj_t BGl_symbol2331z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_expandzd2andzd2zz__rgc_rulesz00(long, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol2333z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_symbol2335z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_symbol2338z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_za2specialzd2matchzd2charza2z00zz__rgc_rulesz00 = BUNSPEC;
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_symbol2341z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_expandzd2za2za2zd2zz__rgc_rulesz00(long, obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_rgcsetzd2butz12zc0zz__rgc_setz00(obj_t, obj_t);
	extern obj_t make_vector(long, obj_t);
	static obj_t BGl_symbol2345z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_symbol2347z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_list2316z00zz__rgc_rulesz00 = BUNSPEC;
	extern obj_t BGl_rgczd2envzd2zz__rgc_configz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_resetzd2specialzd2matchzd2charz12zc0zz__rgc_rulesz00(void);
	static obj_t BGl_appendzd221011zd2zz__rgc_rulesz00(obj_t, obj_t);
	static long BGl_getzd2newzd2submatchz00zz__rgc_rulesz00(void);
	extern obj_t BGl_stringzd2ze3listz31zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_symbol2350z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_symbol2352z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_methodzd2initzd2zz__rgc_rulesz00(void);
	static obj_t BGl_expandzd2rulezd2zz__rgc_rulesz00(long, obj_t, obj_t);
	static obj_t BGl_list2323z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_symbol2358z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_list2326z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_list2327z00zz__rgc_rulesz00 = BUNSPEC;
	extern obj_t BGl_listzd2ze3rgcsetz31zz__rgc_setz00(obj_t, long);
	static obj_t BGl_symbol2360z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_symbol2363z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_list2330z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_symbol2282z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_symbol2365z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_symbol2284z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_symbol2367z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_symbol2286z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_loopze70ze7zz__rgc_rulesz00(obj_t, obj_t);
	static obj_t BGl_loopze71ze7zz__rgc_rulesz00(obj_t, obj_t, long);
	static obj_t BGl_symbol2369z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_symbol2288z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_list2337z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_expandzd2zb2z60zz__rgc_rulesz00(long, obj_t, obj_t);
	extern obj_t BGl_posixzd2ze3rgcz31zz__rgc_posixz00(obj_t);
	static obj_t BGl_addzd2predicatezd2matchz12z12zz__rgc_rulesz00(long, obj_t);
	static obj_t BGl_symbol2371z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_symbol2291z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_expandzd2inzd2zz__rgc_rulesz00(long, obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2373z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_list2340z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_expandzd2outzd2zz__rgc_rulesz00(long, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_ruleszd2ze3regularzd2treeze3zz__rgc_rulesz00(obj_t, obj_t);
	static obj_t BGl_symbol2375z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_symbol2294z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_expandzd2dotszd2zz__rgc_rulesz00(long, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_list2343z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_symbol2377z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_list2344z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_expandzd2zd3z01zz__rgc_rulesz00(long, obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_rgczd2upcasezd2zz__rgc_configz00(obj_t);
	static obj_t BGl_symbol2379z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_list2349z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_za2specialzd2startzd2matchzd2charza2zd2zz__rgc_rulesz00 =
		BUNSPEC;
	extern bool_t BGl_rgczd2alphabeticzf3z21zz__rgc_configz00(obj_t);
	static obj_t BGl_za2predicatesza2z00zz__rgc_rulesz00 = BUNSPEC;
	extern obj_t BGl_rgczd2maxzd2charz00zz__rgc_configz00(void);
	static obj_t BGl_symbol2381z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_explodezd2sequenceze70z35zz__rgc_rulesz00(obj_t, obj_t);
	static obj_t BGl_symbol2383z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_expandzd2sequencezd2zz__rgc_rulesz00(long, obj_t, obj_t);
	static obj_t BGl_symbol2385z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_symbol2387z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_list2354z00zz__rgc_rulesz00 = BUNSPEC;
	static bool_t BGl_za2submatchzf3za2zf3zz__rgc_rulesz00;
	static obj_t BGl_list2355z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_z62specialzd2charzd2matchzf3z91zz__rgc_rulesz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_predicatezd2matchzd2zz__rgc_rulesz00(int);
	static obj_t BGl_symbol2389z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_list2356z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_list2357z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_z62predicatezd2matchzb0zz__rgc_rulesz00(obj_t, obj_t);
	static obj_t BGl_expandzd2stringzd2zz__rgc_rulesz00(obj_t, obj_t);
	static obj_t BGl_symbol2391z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t BGl_makezd2variablezd2envz00zz__rgc_rulesz00(obj_t);
	static obj_t BGl_expandzd2ze3zd3ze2zz__rgc_rulesz00(long, obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2397z00zz__rgc_rulesz00 = BUNSPEC;
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_ruleszd2ze3regularzd2treezd2envz31zz__rgc_rulesz00,
		BgL_bgl_za762rulesza7d2za7e3re2404za7,
		BGl_z62ruleszd2ze3regularzd2treez81zz__rgc_rulesz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2300z00zz__rgc_rulesz00,
		BgL_bgl_string2300za700za7za7_2405za7, "bint", 4);
	      DEFINE_STRING(BGl_string2301z00zz__rgc_rulesz00,
		BgL_bgl_string2301za700za7za7_2406za7, "&special-char-match?", 20);
	      DEFINE_STRING(BGl_string2302z00zz__rgc_rulesz00,
		BgL_bgl_string2302za700za7za7_2407za7, "&special-match-char->rule-number",
		32);
	      DEFINE_STRING(BGl_string2303z00zz__rgc_rulesz00,
		BgL_bgl_string2303za700za7za7_2408za7, "&predicate-match", 16);
	      DEFINE_STRING(BGl_string2305z00zz__rgc_rulesz00,
		BgL_bgl_string2305za700za7za7_2409za7, "when", 4);
	      DEFINE_STRING(BGl_string2307z00zz__rgc_rulesz00,
		BgL_bgl_string2307za700za7za7_2410za7, "context", 7);
	      DEFINE_STRING(BGl_string2309z00zz__rgc_rulesz00,
		BgL_bgl_string2309za700za7za7_2411za7, "quote", 5);
	      DEFINE_STRING(BGl_string2311z00zz__rgc_rulesz00,
		BgL_bgl_string2311za700za7za7_2412za7, "the-rgc-context", 15);
	      DEFINE_STRING(BGl_string2313z00zz__rgc_rulesz00,
		BgL_bgl_string2313za700za7za7_2413za7, "eq?", 3);
	      DEFINE_STRING(BGl_string2315z00zz__rgc_rulesz00,
		BgL_bgl_string2315za700za7za7_2414za7, "bol", 3);
	      DEFINE_STRING(BGl_string2318z00zz__rgc_rulesz00,
		BgL_bgl_string2318za700za7za7_2415za7, "rgc-buffer-bol?", 15);
	      DEFINE_STRING(BGl_string2400z00zz__rgc_rulesz00,
		BgL_bgl_string2400za700za7za7_2416za7, "RGC:Illegal range string", 24);
	      DEFINE_STRING(BGl_string2401z00zz__rgc_rulesz00,
		BgL_bgl_string2401za700za7za7_2417za7, "RGC:Illegal range", 17);
	      DEFINE_STRING(BGl_string2320z00zz__rgc_rulesz00,
		BgL_bgl_string2320za700za7za7_2418za7, "iport", 5);
	      DEFINE_STRING(BGl_string2402z00zz__rgc_rulesz00,
		BgL_bgl_string2402za700za7za7_2419za7, "RGC:Illegal nested submatches", 29);
	      DEFINE_STRING(BGl_string2403z00zz__rgc_rulesz00,
		BgL_bgl_string2403za700za7za7_2420za7, "__rgc_rules", 11);
	      DEFINE_STRING(BGl_string2322z00zz__rgc_rulesz00,
		BgL_bgl_string2322za700za7za7_2421za7, "eol", 3);
	      DEFINE_STRING(BGl_string2325z00zz__rgc_rulesz00,
		BgL_bgl_string2325za700za7za7_2422za7, "let", 3);
	      DEFINE_STRING(BGl_string2329z00zz__rgc_rulesz00,
		BgL_bgl_string2329za700za7za7_2423za7, "r", 1);
	      DEFINE_STRING(BGl_string2332z00zz__rgc_rulesz00,
		BgL_bgl_string2332za700za7za7_2424za7, "rgc-buffer-eol?", 15);
	      DEFINE_STRING(BGl_string2334z00zz__rgc_rulesz00,
		BgL_bgl_string2334za700za7za7_2425za7, "forward", 7);
	      DEFINE_STRING(BGl_string2336z00zz__rgc_rulesz00,
		BgL_bgl_string2336za700za7za7_2426za7, "bufpos", 6);
	      DEFINE_STRING(BGl_string2339z00zz__rgc_rulesz00,
		BgL_bgl_string2339za700za7za7_2427za7, "set!", 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_resetzd2specialzd2matchzd2charz12zd2envz12zz__rgc_rulesz00,
		BgL_bgl_za762resetza7d2speci2428z00,
		BGl_z62resetzd2specialzd2matchzd2charz12za2zz__rgc_rulesz00, 0L, BUNSPEC,
		0);
	      DEFINE_STRING(BGl_string2342z00zz__rgc_rulesz00,
		BgL_bgl_string2342za700za7za7_2429za7, "rgc-buffer-forward", 18);
	      DEFINE_STRING(BGl_string2346z00zz__rgc_rulesz00,
		BgL_bgl_string2346za700za7za7_2430za7, "rgc-buffer-bufpos", 17);
	      DEFINE_STRING(BGl_string2348z00zz__rgc_rulesz00,
		BgL_bgl_string2348za700za7za7_2431za7, "bof", 3);
	      DEFINE_STRING(BGl_string2351z00zz__rgc_rulesz00,
		BgL_bgl_string2351za700za7za7_2432za7, "rgc-buffer-bof?", 15);
	      DEFINE_STRING(BGl_string2353z00zz__rgc_rulesz00,
		BgL_bgl_string2353za700za7za7_2433za7, "eof", 3);
	      DEFINE_STRING(BGl_string2359z00zz__rgc_rulesz00,
		BgL_bgl_string2359za700za7za7_2434za7, "rgc-buffer-eof2?", 16);
	      DEFINE_STRING(BGl_string2361z00zz__rgc_rulesz00,
		BgL_bgl_string2361za700za7za7_2435za7, "...", 3);
	      DEFINE_STRING(BGl_string2362z00zz__rgc_rulesz00,
		BgL_bgl_string2362za700za7za7_2436za7, "RGC:Illegal construction", 24);
	      DEFINE_STRING(BGl_string2281z00zz__rgc_rulesz00,
		BgL_bgl_string2281za700za7za7_2437za7, "RGC:Illegal clauses", 19);
	      DEFINE_STRING(BGl_string2364z00zz__rgc_rulesz00,
		BgL_bgl_string2364za700za7za7_2438za7, "uncase", 6);
	      DEFINE_STRING(BGl_string2283z00zz__rgc_rulesz00,
		BgL_bgl_string2283za700za7za7_2439za7, "in", 2);
	      DEFINE_STRING(BGl_string2366z00zz__rgc_rulesz00,
		BgL_bgl_string2366za700za7za7_2440za7, "*", 1);
	      DEFINE_STRING(BGl_string2285z00zz__rgc_rulesz00,
		BgL_bgl_string2285za700za7za7_2441za7, "begin", 5);
	      DEFINE_STRING(BGl_string2368z00zz__rgc_rulesz00,
		BgL_bgl_string2368za700za7za7_2442za7, "+", 1);
	      DEFINE_STRING(BGl_string2287z00zz__rgc_rulesz00,
		BgL_bgl_string2287za700za7za7_2443za7, "else", 4);
	      DEFINE_STRING(BGl_string2289z00zz__rgc_rulesz00,
		BgL_bgl_string2289za700za7za7_2444za7, "or", 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_specialzd2charzd2matchzf3zd2envz21zz__rgc_rulesz00,
		BgL_bgl_za762specialza7d2cha2445z00,
		BGl_z62specialzd2charzd2matchzf3z91zz__rgc_rulesz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_predicatezd2matchzd2envz00zz__rgc_rulesz00,
		BgL_bgl_za762predicateza7d2m2446z00,
		BGl_z62predicatezd2matchzb0zz__rgc_rulesz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2370z00zz__rgc_rulesz00,
		BgL_bgl_string2370za700za7za7_2447za7, "?", 1);
	      DEFINE_STRING(BGl_string2372z00zz__rgc_rulesz00,
		BgL_bgl_string2372za700za7za7_2448za7, "epsilon", 7);
	      DEFINE_STRING(BGl_string2292z00zz__rgc_rulesz00,
		BgL_bgl_string2292za700za7za7_2449za7, "the-failure", 11);
	      DEFINE_STRING(BGl_string2374z00zz__rgc_rulesz00,
		BgL_bgl_string2374za700za7za7_2450za7, "=", 1);
	      DEFINE_STRING(BGl_string2293z00zz__rgc_rulesz00,
		BgL_bgl_string2293za700za7za7_2451za7, "RGC:Illegal else clause", 23);
	      DEFINE_STRING(BGl_string2376z00zz__rgc_rulesz00,
		BgL_bgl_string2376za700za7za7_2452za7, ">=", 2);
	      DEFINE_STRING(BGl_string2295z00zz__rgc_rulesz00,
		BgL_bgl_string2295za700za7za7_2453za7, "define", 6);
	      DEFINE_STRING(BGl_string2296z00zz__rgc_rulesz00,
		BgL_bgl_string2296za700za7za7_2454za7, "RGC:Illegal clause", 18);
	      DEFINE_STRING(BGl_string2378z00zz__rgc_rulesz00,
		BgL_bgl_string2378za700za7za7_2455za7, "**", 2);
	      DEFINE_STRING(BGl_string2297z00zz__rgc_rulesz00,
		BgL_bgl_string2297za700za7za7_2456za7,
		"RGC:Illegal regular variable definition", 39);
	      DEFINE_STRING(BGl_string2298z00zz__rgc_rulesz00,
		BgL_bgl_string2298za700za7za7_2457za7,
		"/tmp/bigloo/runtime/Rgc/rgcrules.scm", 36);
	      DEFINE_STRING(BGl_string2299z00zz__rgc_rulesz00,
		BgL_bgl_string2299za700za7za7_2458za7, "&special-char?", 14);
	      DEFINE_STRING(BGl_string2380z00zz__rgc_rulesz00,
		BgL_bgl_string2380za700za7za7_2459za7, "out", 3);
	      DEFINE_STRING(BGl_string2382z00zz__rgc_rulesz00,
		BgL_bgl_string2382za700za7za7_2460za7, "and", 3);
	      DEFINE_STRING(BGl_string2384z00zz__rgc_rulesz00,
		BgL_bgl_string2384za700za7za7_2461za7, "but", 3);
	      DEFINE_STRING(BGl_string2386z00zz__rgc_rulesz00,
		BgL_bgl_string2386za700za7za7_2462za7, "submatch", 8);
	      DEFINE_STRING(BGl_string2388z00zz__rgc_rulesz00,
		BgL_bgl_string2388za700za7za7_2463za7, ":", 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_treezd2maxzd2charzd2envzd2zz__rgc_rulesz00,
		BgL_bgl_za762treeza7d2maxza7d22464za7,
		BGl_z62treezd2maxzd2charz62zz__rgc_rulesz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2390z00zz__rgc_rulesz00,
		BgL_bgl_string2390za700za7za7_2465za7, "seq", 3);
	      DEFINE_STRING(BGl_string2392z00zz__rgc_rulesz00,
		BgL_bgl_string2392za700za7za7_2466za7, "posix", 5);
	      DEFINE_STRING(BGl_string2393z00zz__rgc_rulesz00,
		BgL_bgl_string2393za700za7za7_2467za7, "RGC:illegal atom", 16);
	      DEFINE_STRING(BGl_string2394z00zz__rgc_rulesz00,
		BgL_bgl_string2394za700za7za7_2468za7, "RGC:regular variable unbound", 28);
	      DEFINE_STRING(BGl_string2395z00zz__rgc_rulesz00,
		BgL_bgl_string2395za700za7za7_2469za7, "RGC:Illegal regular expression",
		30);
	      DEFINE_STRING(BGl_string2396z00zz__rgc_rulesz00,
		BgL_bgl_string2396za700za7za7_2470za7, "RGC:Illegal empty string", 24);
	      DEFINE_STRING(BGl_string2398z00zz__rgc_rulesz00,
		BgL_bgl_string2398za700za7za7_2471za7, "sequence", 8);
	      DEFINE_STRING(BGl_string2399z00zz__rgc_rulesz00,
		BgL_bgl_string2399za700za7za7_2472za7, "RGC:Illegal regular range", 25);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_specialzd2matchzd2charzd2ze3rulezd2numberzd2envz31zz__rgc_rulesz00,
		BgL_bgl_za762specialza7d2mat2473z00,
		BGl_z62specialzd2matchzd2charzd2ze3rulezd2numberz81zz__rgc_rulesz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_specialzd2charzf3zd2envzf3zz__rgc_rulesz00,
		BgL_bgl_za762specialza7d2cha2474z00,
		BGl_z62specialzd2charzf3z43zz__rgc_rulesz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_list2290z00zz__rgc_rulesz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2specialzd2stopzd2matchzd2charza2zd2zz__rgc_rulesz00));
		     ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2304z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2306z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2308z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2310z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2312z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2314z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2317z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2319z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2321z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2324z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2328z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2331z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2333z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2335z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2338z00zz__rgc_rulesz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2specialzd2matchzd2charza2z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2341z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2345z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2347z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_list2316z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2350z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2352z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_list2323z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2358z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_list2326z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_list2327z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2360z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2363z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_list2330z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2282z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2365z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2284z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2367z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2286z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2369z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2288z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_list2337z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2371z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2291z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2373z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_list2340z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2375z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2294z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_list2343z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2377z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_list2344z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2379z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_list2349z00zz__rgc_rulesz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2specialzd2startzd2matchzd2charza2zd2zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_za2predicatesza2z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2381z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2383z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2385z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2387z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_list2354z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_list2355z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2389z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_list2356z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_list2357z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2391z00zz__rgc_rulesz00));
		     ADD_ROOT((void *) (&BGl_symbol2397z00zz__rgc_rulesz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__rgc_rulesz00(long
		BgL_checksumz00_3619, char *BgL_fromz00_3620)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__rgc_rulesz00))
				{
					BGl_requirezd2initializa7ationz75zz__rgc_rulesz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__rgc_rulesz00();
					BGl_cnstzd2initzd2zz__rgc_rulesz00();
					BGl_importedzd2moduleszd2initz00zz__rgc_rulesz00();
					return BGl_toplevelzd2initzd2zz__rgc_rulesz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__rgc_rulesz00(void)
	{
		{	/* Rgc/rgcrules.scm 24 */
			BGl_symbol2282z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2283z00zz__rgc_rulesz00);
			BGl_symbol2284z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2285z00zz__rgc_rulesz00);
			BGl_symbol2286z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2287z00zz__rgc_rulesz00);
			BGl_symbol2288z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2289z00zz__rgc_rulesz00);
			BGl_symbol2291z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2292z00zz__rgc_rulesz00);
			BGl_list2290z00zz__rgc_rulesz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2291z00zz__rgc_rulesz00, BNIL);
			BGl_symbol2294z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2295z00zz__rgc_rulesz00);
			BGl_symbol2304z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2305z00zz__rgc_rulesz00);
			BGl_symbol2306z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2307z00zz__rgc_rulesz00);
			BGl_symbol2308z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2309z00zz__rgc_rulesz00);
			BGl_symbol2310z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2311z00zz__rgc_rulesz00);
			BGl_symbol2312z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2313z00zz__rgc_rulesz00);
			BGl_symbol2314z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2315z00zz__rgc_rulesz00);
			BGl_symbol2317z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2318z00zz__rgc_rulesz00);
			BGl_symbol2319z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2320z00zz__rgc_rulesz00);
			BGl_list2316z00zz__rgc_rulesz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2317z00zz__rgc_rulesz00,
				MAKE_YOUNG_PAIR(BGl_symbol2319z00zz__rgc_rulesz00, BNIL));
			BGl_symbol2321z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2322z00zz__rgc_rulesz00);
			BGl_symbol2324z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2325z00zz__rgc_rulesz00);
			BGl_symbol2328z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2329z00zz__rgc_rulesz00);
			BGl_symbol2331z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2332z00zz__rgc_rulesz00);
			BGl_symbol2333z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2334z00zz__rgc_rulesz00);
			BGl_symbol2335z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2336z00zz__rgc_rulesz00);
			BGl_list2330z00zz__rgc_rulesz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2331z00zz__rgc_rulesz00,
				MAKE_YOUNG_PAIR(BGl_symbol2319z00zz__rgc_rulesz00,
					MAKE_YOUNG_PAIR(BGl_symbol2333z00zz__rgc_rulesz00,
						MAKE_YOUNG_PAIR(BGl_symbol2335z00zz__rgc_rulesz00, BNIL))));
			BGl_list2327z00zz__rgc_rulesz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2328z00zz__rgc_rulesz00,
				MAKE_YOUNG_PAIR(BGl_list2330z00zz__rgc_rulesz00, BNIL));
			BGl_list2326z00zz__rgc_rulesz00 =
				MAKE_YOUNG_PAIR(BGl_list2327z00zz__rgc_rulesz00, BNIL);
			BGl_symbol2338z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2339z00zz__rgc_rulesz00);
			BGl_symbol2341z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2342z00zz__rgc_rulesz00);
			BGl_list2340z00zz__rgc_rulesz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2341z00zz__rgc_rulesz00,
				MAKE_YOUNG_PAIR(BGl_symbol2319z00zz__rgc_rulesz00, BNIL));
			BGl_list2337z00zz__rgc_rulesz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2338z00zz__rgc_rulesz00,
				MAKE_YOUNG_PAIR(BGl_symbol2333z00zz__rgc_rulesz00,
					MAKE_YOUNG_PAIR(BGl_list2340z00zz__rgc_rulesz00, BNIL)));
			BGl_symbol2345z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2346z00zz__rgc_rulesz00);
			BGl_list2344z00zz__rgc_rulesz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2345z00zz__rgc_rulesz00,
				MAKE_YOUNG_PAIR(BGl_symbol2319z00zz__rgc_rulesz00, BNIL));
			BGl_list2343z00zz__rgc_rulesz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2338z00zz__rgc_rulesz00,
				MAKE_YOUNG_PAIR(BGl_symbol2335z00zz__rgc_rulesz00,
					MAKE_YOUNG_PAIR(BGl_list2344z00zz__rgc_rulesz00, BNIL)));
			BGl_list2323z00zz__rgc_rulesz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2324z00zz__rgc_rulesz00,
				MAKE_YOUNG_PAIR(BGl_list2326z00zz__rgc_rulesz00,
					MAKE_YOUNG_PAIR(BGl_list2337z00zz__rgc_rulesz00,
						MAKE_YOUNG_PAIR(BGl_list2343z00zz__rgc_rulesz00,
							MAKE_YOUNG_PAIR(BGl_symbol2328z00zz__rgc_rulesz00, BNIL)))));
			BGl_symbol2347z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2348z00zz__rgc_rulesz00);
			BGl_symbol2350z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2351z00zz__rgc_rulesz00);
			BGl_list2349z00zz__rgc_rulesz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2350z00zz__rgc_rulesz00,
				MAKE_YOUNG_PAIR(BGl_symbol2319z00zz__rgc_rulesz00, BNIL));
			BGl_symbol2352z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2353z00zz__rgc_rulesz00);
			BGl_symbol2358z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2359z00zz__rgc_rulesz00);
			BGl_list2357z00zz__rgc_rulesz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2358z00zz__rgc_rulesz00,
				MAKE_YOUNG_PAIR(BGl_symbol2319z00zz__rgc_rulesz00,
					MAKE_YOUNG_PAIR(BGl_symbol2333z00zz__rgc_rulesz00,
						MAKE_YOUNG_PAIR(BGl_symbol2335z00zz__rgc_rulesz00, BNIL))));
			BGl_list2356z00zz__rgc_rulesz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2328z00zz__rgc_rulesz00,
				MAKE_YOUNG_PAIR(BGl_list2357z00zz__rgc_rulesz00, BNIL));
			BGl_list2355z00zz__rgc_rulesz00 =
				MAKE_YOUNG_PAIR(BGl_list2356z00zz__rgc_rulesz00, BNIL);
			BGl_list2354z00zz__rgc_rulesz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2324z00zz__rgc_rulesz00,
				MAKE_YOUNG_PAIR(BGl_list2355z00zz__rgc_rulesz00,
					MAKE_YOUNG_PAIR(BGl_list2337z00zz__rgc_rulesz00,
						MAKE_YOUNG_PAIR(BGl_list2343z00zz__rgc_rulesz00,
							MAKE_YOUNG_PAIR(BGl_symbol2328z00zz__rgc_rulesz00, BNIL)))));
			BGl_symbol2360z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2361z00zz__rgc_rulesz00);
			BGl_symbol2363z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2364z00zz__rgc_rulesz00);
			BGl_symbol2365z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2366z00zz__rgc_rulesz00);
			BGl_symbol2367z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2368z00zz__rgc_rulesz00);
			BGl_symbol2369z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2370z00zz__rgc_rulesz00);
			BGl_symbol2371z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2372z00zz__rgc_rulesz00);
			BGl_symbol2373z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2374z00zz__rgc_rulesz00);
			BGl_symbol2375z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2376z00zz__rgc_rulesz00);
			BGl_symbol2377z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2378z00zz__rgc_rulesz00);
			BGl_symbol2379z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2380z00zz__rgc_rulesz00);
			BGl_symbol2381z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2382z00zz__rgc_rulesz00);
			BGl_symbol2383z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2384z00zz__rgc_rulesz00);
			BGl_symbol2385z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2386z00zz__rgc_rulesz00);
			BGl_symbol2387z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2388z00zz__rgc_rulesz00);
			BGl_symbol2389z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2390z00zz__rgc_rulesz00);
			BGl_symbol2391z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2392z00zz__rgc_rulesz00);
			return (BGl_symbol2397z00zz__rgc_rulesz00 =
				bstring_to_symbol(BGl_string2398z00zz__rgc_rulesz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__rgc_rulesz00(void)
	{
		{	/* Rgc/rgcrules.scm 24 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__rgc_rulesz00(void)
	{
		{	/* Rgc/rgcrules.scm 24 */
			BGl_za2maxzd2rgczd2zd3zd2numza2z01zz__rgc_rulesz00 = 81L;
			{	/* Rgc/rgcrules.scm 163 */
				obj_t BgL_arg1242z00_1260;

				BgL_arg1242z00_1260 = BGl_rgczd2maxzd2charz00zz__rgc_configz00();
				BGl_specialzd2charzd2numz00zz__rgc_rulesz00 =
					((long) CINT(BgL_arg1242z00_1260) - 1L);
			}
			BGl_za2specialzd2matchzd2charza2z00zz__rgc_rulesz00 = BNIL;
			BGl_za2specialzd2startzd2matchzd2charza2zd2zz__rgc_rulesz00 = BNIL;
			BGl_za2specialzd2stopzd2matchzd2charza2zd2zz__rgc_rulesz00 = BNIL;
			BGl_za2predicatesza2z00zz__rgc_rulesz00 = BNIL;
			BGl_za2submatchzf3za2zf3zz__rgc_rulesz00 = ((bool_t) 0);
			BGl_za2submatchzd2countza2zd2zz__rgc_rulesz00 = 0L;
			return (BGl_za2lockzd2submatchza2zd2zz__rgc_rulesz00 =
				((bool_t) 0), BUNSPEC);
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zz__rgc_rulesz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1261;

				BgL_headz00_1261 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_2601;
					obj_t BgL_tailz00_2602;

					BgL_prevz00_2601 = BgL_headz00_1261;
					BgL_tailz00_2602 = BgL_l1z00_1;
				BgL_loopz00_2600:
					if (PAIRP(BgL_tailz00_2602))
						{
							obj_t BgL_newzd2prevzd2_2608;

							BgL_newzd2prevzd2_2608 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_2602), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_2601, BgL_newzd2prevzd2_2608);
							{
								obj_t BgL_tailz00_3722;
								obj_t BgL_prevz00_3721;

								BgL_prevz00_3721 = BgL_newzd2prevzd2_2608;
								BgL_tailz00_3722 = CDR(BgL_tailz00_2602);
								BgL_tailz00_2602 = BgL_tailz00_3722;
								BgL_prevz00_2601 = BgL_prevz00_3721;
								goto BgL_loopz00_2600;
							}
						}
					else
						{
							BNIL;
						}
				}
				return CDR(BgL_headz00_1261);
			}
		}

	}



/* rules->regular-tree */
	BGL_EXPORTED_DEF obj_t BGl_ruleszd2ze3regularzd2treeze3zz__rgc_rulesz00(obj_t
		BgL_userzd2envzd2_3, obj_t BgL_clausesz00_4)
	{
		{	/* Rgc/rgcrules.scm 80 */
			BGl_resetzd2specialzd2matchzd2charz12zc0zz__rgc_rulesz00();
			if (NULLP(BgL_clausesz00_4))
				{	/* Rgc/rgcrules.scm 82 */
					return
						BGl_errorz00zz__errorz00(BFALSE, BGl_string2281z00zz__rgc_rulesz00,
						BgL_clausesz00_4);
				}
			else
				{	/* Rgc/rgcrules.scm 84 */
					obj_t BgL_envz00_1270;
					obj_t BgL_dfltz00_1271;

					BgL_envz00_1270 =
						BGl_makezd2variablezd2envz00zz__rgc_rulesz00
						(BGl_appendzd221011zd2zz__rgc_rulesz00(BgL_userzd2envzd2_3,
							BGl_rgczd2envzd2zz__rgc_configz00()));
					{	/* Rgc/rgcrules.scm 85 */
						obj_t BgL_arg1331z00_1333;

						{	/* Rgc/rgcrules.scm 85 */
							obj_t BgL_arg1332z00_1334;

							{	/* Rgc/rgcrules.scm 85 */
								obj_t BgL_arg1333z00_1335;

								{	/* Rgc/rgcrules.scm 85 */
									obj_t BgL_arg1334z00_1336;

									{	/* Rgc/rgcrules.scm 85 */
										obj_t BgL_a1101z00_1337;

										BgL_a1101z00_1337 =
											BGl_rgczd2maxzd2charz00zz__rgc_configz00();
										{	/* Rgc/rgcrules.scm 85 */

											if (INTEGERP(BgL_a1101z00_1337))
												{	/* Rgc/rgcrules.scm 85 */
													BgL_arg1334z00_1336 =
														SUBFX(BgL_a1101z00_1337, BINT(1L));
												}
											else
												{	/* Rgc/rgcrules.scm 85 */
													BgL_arg1334z00_1336 =
														BGl_2zd2zd2zz__r4_numbers_6_5z00(BgL_a1101z00_1337,
														BINT(1L));
												}
										}
									}
									BgL_arg1333z00_1335 =
										MAKE_YOUNG_PAIR(BgL_arg1334z00_1336, BNIL);
								}
								BgL_arg1332z00_1334 =
									MAKE_YOUNG_PAIR(BINT(0L), BgL_arg1333z00_1335);
							}
							BgL_arg1331z00_1333 = MAKE_YOUNG_PAIR(BgL_arg1332z00_1334, BNIL);
						}
						BgL_dfltz00_1271 =
							MAKE_YOUNG_PAIR(BGl_symbol2282z00zz__rgc_rulesz00,
							BgL_arg1331z00_1333);
					}
					{
						obj_t BgL_clausesz00_1276;
						long BgL_matchz00_1277;
						obj_t BgL_branchesz00_1278;
						obj_t BgL_actionsz00_1279;
						obj_t BgL_defsz00_1280;

						BgL_clausesz00_1276 = BgL_clausesz00_4;
						BgL_matchz00_1277 = 0L;
						BgL_branchesz00_1278 = BNIL;
						BgL_actionsz00_1279 = BNIL;
						BgL_defsz00_1280 = BNIL;
					BgL_zc3z04anonymousza31250ze3z87_1281:
						{
							obj_t BgL_rulez00_1283;
							obj_t BgL_actsz00_1284;

							{	/* Rgc/rgcrules.scm 91 */
								obj_t BgL_ezd2104zd2_1287;

								BgL_ezd2104zd2_1287 = CAR(((obj_t) BgL_clausesz00_1276));
								if (PAIRP(BgL_ezd2104zd2_1287))
									{	/* Rgc/rgcrules.scm 86 */
										if (
											(CAR(BgL_ezd2104zd2_1287) ==
												BGl_symbol2294z00zz__rgc_rulesz00))
											{	/* Rgc/rgcrules.scm 86 */
												{	/* Rgc/rgcrules.scm 93 */
													obj_t BgL_arg1284z00_1295;
													obj_t BgL_arg1304z00_1296;

													BgL_arg1284z00_1295 =
														CDR(((obj_t) BgL_clausesz00_1276));
													{	/* Rgc/rgcrules.scm 97 */
														obj_t BgL_arg1305z00_1297;

														BgL_arg1305z00_1297 =
															CAR(((obj_t) BgL_clausesz00_1276));
														BgL_arg1304z00_1296 =
															MAKE_YOUNG_PAIR(BgL_arg1305z00_1297,
															BgL_defsz00_1280);
													}
													{
														obj_t BgL_defsz00_3757;
														obj_t BgL_clausesz00_3756;

														BgL_clausesz00_3756 = BgL_arg1284z00_1295;
														BgL_defsz00_3757 = BgL_arg1304z00_1296;
														BgL_defsz00_1280 = BgL_defsz00_3757;
														BgL_clausesz00_1276 = BgL_clausesz00_3756;
														goto BgL_zc3z04anonymousza31250ze3z87_1281;
													}
												}
											}
										else
											{	/* Rgc/rgcrules.scm 86 */
												obj_t BgL_cdrzd2114zd2_1291;

												BgL_cdrzd2114zd2_1291 = CDR(BgL_ezd2104zd2_1287);
												if (PAIRP(BgL_cdrzd2114zd2_1291))
													{	/* Rgc/rgcrules.scm 86 */
														BgL_rulez00_1283 = CAR(BgL_ezd2104zd2_1287);
														BgL_actsz00_1284 = BgL_cdrzd2114zd2_1291;
														{	/* Rgc/rgcrules.scm 99 */
															obj_t BgL_actz00_1298;

															{	/* Rgc/rgcrules.scm 99 */
																obj_t BgL_arg1327z00_1329;

																BgL_arg1327z00_1329 =
																	BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																	(BgL_actsz00_1284, BNIL);
																BgL_actz00_1298 =
																	MAKE_YOUNG_PAIR
																	(BGl_symbol2284z00zz__rgc_rulesz00,
																	BgL_arg1327z00_1329);
															}
															if (NULLP(CDR(((obj_t) BgL_clausesz00_1276))))
																{	/* Rgc/rgcrules.scm 100 */
																	if (
																		(BgL_rulez00_1283 ==
																			BGl_symbol2286z00zz__rgc_rulesz00))
																		{	/* Rgc/rgcrules.scm 103 */
																			obj_t BgL_val0_1103z00_1301;
																			obj_t BgL_val1_1104z00_1302;
																			bool_t BgL_val3_1106z00_1304;

																			{	/* Rgc/rgcrules.scm 103 */
																				obj_t BgL_arg1308z00_1306;

																				BgL_arg1308z00_1306 =
																					MAKE_YOUNG_PAIR
																					(BGl_expandzd2matchzd2rulez00zz__rgc_rulesz00
																					(BgL_matchz00_1277, BgL_envz00_1270,
																						BgL_dfltz00_1271),
																					BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																					(BgL_branchesz00_1278, BNIL));
																				BgL_val0_1103z00_1301 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol2288z00zz__rgc_rulesz00,
																					BgL_arg1308z00_1306);
																			}
																			{	/* Rgc/rgcrules.scm 105 */
																				obj_t BgL_arg1311z00_1309;

																				BgL_arg1311z00_1309 =
																					MAKE_YOUNG_PAIR(BgL_actz00_1298,
																					BgL_actionsz00_1279);
																				BgL_val1_1104z00_1302 =
																					bgl_reverse_bang(BgL_arg1311z00_1309);
																			}
																			BgL_val3_1106z00_1304 =
																				BGl_za2submatchzf3za2zf3zz__rgc_rulesz00;
																			{	/* Rgc/rgcrules.scm 103 */
																				int BgL_tmpz00_3775;

																				BgL_tmpz00_3775 = (int) (5L);
																				BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3775);
																			}
																			{	/* Rgc/rgcrules.scm 103 */
																				int BgL_tmpz00_3778;

																				BgL_tmpz00_3778 = (int) (1L);
																				BGL_MVALUES_VAL_SET(BgL_tmpz00_3778,
																					BgL_val1_1104z00_1302);
																			}
																			{	/* Rgc/rgcrules.scm 103 */
																				obj_t BgL_auxz00_3783;
																				int BgL_tmpz00_3781;

																				BgL_auxz00_3783 =
																					BINT(BgL_matchz00_1277);
																				BgL_tmpz00_3781 = (int) (2L);
																				BGL_MVALUES_VAL_SET(BgL_tmpz00_3781,
																					BgL_auxz00_3783);
																			}
																			{	/* Rgc/rgcrules.scm 103 */
																				obj_t BgL_auxz00_3788;
																				int BgL_tmpz00_3786;

																				BgL_auxz00_3788 =
																					BBOOL(BgL_val3_1106z00_1304);
																				BgL_tmpz00_3786 = (int) (3L);
																				BGL_MVALUES_VAL_SET(BgL_tmpz00_3786,
																					BgL_auxz00_3788);
																			}
																			{	/* Rgc/rgcrules.scm 103 */
																				int BgL_tmpz00_3791;

																				BgL_tmpz00_3791 = (int) (4L);
																				BGL_MVALUES_VAL_SET(BgL_tmpz00_3791,
																					BgL_defsz00_1280);
																			}
																			return BgL_val0_1103z00_1301;
																		}
																	else
																		{	/* Rgc/rgcrules.scm 109 */
																			obj_t BgL_val0_1108z00_1310;
																			obj_t BgL_val1_1109z00_1311;
																			long BgL_val2_1110z00_1312;
																			bool_t BgL_val3_1111z00_1313;

																			{	/* Rgc/rgcrules.scm 109 */
																				obj_t BgL_arg1312z00_1315;

																				{	/* Rgc/rgcrules.scm 109 */
																					obj_t BgL_arg1314z00_1316;
																					obj_t BgL_arg1315z00_1317;

																					BgL_arg1314z00_1316 =
																						BGl_expandzd2matchzd2rulez00zz__rgc_rulesz00
																						((BgL_matchz00_1277 + 1L),
																						BgL_envz00_1270, BgL_dfltz00_1271);
																					BgL_arg1315z00_1317 =
																						MAKE_YOUNG_PAIR
																						(BGl_expandzd2matchzd2rulez00zz__rgc_rulesz00
																						(BgL_matchz00_1277, BgL_envz00_1270,
																							BgL_rulez00_1283),
																						BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																						(BgL_branchesz00_1278, BNIL));
																					BgL_arg1312z00_1315 =
																						MAKE_YOUNG_PAIR(BgL_arg1314z00_1316,
																						BgL_arg1315z00_1317);
																				}
																				BgL_val0_1108z00_1310 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol2288z00zz__rgc_rulesz00,
																					BgL_arg1312z00_1315);
																			}
																			{	/* Rgc/rgcrules.scm 114 */
																				obj_t BgL_arg1319z00_1321;

																				{	/* Rgc/rgcrules.scm 114 */
																					obj_t BgL_arg1320z00_1322;

																					BgL_arg1320z00_1322 =
																						MAKE_YOUNG_PAIR(BgL_actz00_1298,
																						BgL_actionsz00_1279);
																					BgL_arg1319z00_1321 =
																						MAKE_YOUNG_PAIR
																						(BGl_list2290z00zz__rgc_rulesz00,
																						BgL_arg1320z00_1322);
																				}
																				BgL_val1_1109z00_1311 =
																					bgl_reverse_bang(BgL_arg1319z00_1321);
																			}
																			BgL_val2_1110z00_1312 =
																				(1L + BgL_matchz00_1277);
																			BgL_val3_1111z00_1313 =
																				BGl_za2submatchzf3za2zf3zz__rgc_rulesz00;
																			{	/* Rgc/rgcrules.scm 109 */
																				int BgL_tmpz00_3805;

																				BgL_tmpz00_3805 = (int) (5L);
																				BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3805);
																			}
																			{	/* Rgc/rgcrules.scm 109 */
																				int BgL_tmpz00_3808;

																				BgL_tmpz00_3808 = (int) (1L);
																				BGL_MVALUES_VAL_SET(BgL_tmpz00_3808,
																					BgL_val1_1109z00_1311);
																			}
																			{	/* Rgc/rgcrules.scm 109 */
																				obj_t BgL_auxz00_3813;
																				int BgL_tmpz00_3811;

																				BgL_auxz00_3813 =
																					BINT(BgL_val2_1110z00_1312);
																				BgL_tmpz00_3811 = (int) (2L);
																				BGL_MVALUES_VAL_SET(BgL_tmpz00_3811,
																					BgL_auxz00_3813);
																			}
																			{	/* Rgc/rgcrules.scm 109 */
																				obj_t BgL_auxz00_3818;
																				int BgL_tmpz00_3816;

																				BgL_auxz00_3818 =
																					BBOOL(BgL_val3_1111z00_1313);
																				BgL_tmpz00_3816 = (int) (3L);
																				BGL_MVALUES_VAL_SET(BgL_tmpz00_3816,
																					BgL_auxz00_3818);
																			}
																			{	/* Rgc/rgcrules.scm 109 */
																				int BgL_tmpz00_3821;

																				BgL_tmpz00_3821 = (int) (4L);
																				BGL_MVALUES_VAL_SET(BgL_tmpz00_3821,
																					BgL_defsz00_1280);
																			}
																			return BgL_val0_1108z00_1310;
																		}
																}
															else
																{	/* Rgc/rgcrules.scm 100 */
																	if (
																		(BgL_rulez00_1283 ==
																			BGl_symbol2286z00zz__rgc_rulesz00))
																		{	/* Rgc/rgcrules.scm 121 */
																			return
																				BGl_errorz00zz__errorz00(BFALSE,
																				BGl_string2293z00zz__rgc_rulesz00,
																				BgL_clausesz00_1276);
																		}
																	else
																		{	/* Rgc/rgcrules.scm 123 */
																			obj_t BgL_erulez00_1323;

																			BgL_erulez00_1323 =
																				BGl_expandzd2matchzd2rulez00zz__rgc_rulesz00
																				(BgL_matchz00_1277, BgL_envz00_1270,
																				BgL_rulez00_1283);
																			{	/* Rgc/rgcrules.scm 124 */
																				obj_t BgL_arg1321z00_1324;
																				long BgL_arg1322z00_1325;
																				obj_t BgL_arg1323z00_1326;
																				obj_t BgL_arg1325z00_1327;

																				BgL_arg1321z00_1324 =
																					CDR(((obj_t) BgL_clausesz00_1276));
																				BgL_arg1322z00_1325 =
																					(BgL_matchz00_1277 + 1L);
																				BgL_arg1323z00_1326 =
																					MAKE_YOUNG_PAIR(BgL_erulez00_1323,
																					BgL_branchesz00_1278);
																				BgL_arg1325z00_1327 =
																					MAKE_YOUNG_PAIR(BgL_actz00_1298,
																					BgL_actionsz00_1279);
																				{
																					obj_t BgL_actionsz00_3836;
																					obj_t BgL_branchesz00_3835;
																					long BgL_matchz00_3834;
																					obj_t BgL_clausesz00_3833;

																					BgL_clausesz00_3833 =
																						BgL_arg1321z00_1324;
																					BgL_matchz00_3834 =
																						BgL_arg1322z00_1325;
																					BgL_branchesz00_3835 =
																						BgL_arg1323z00_1326;
																					BgL_actionsz00_3836 =
																						BgL_arg1325z00_1327;
																					BgL_actionsz00_1279 =
																						BgL_actionsz00_3836;
																					BgL_branchesz00_1278 =
																						BgL_branchesz00_3835;
																					BgL_matchz00_1277 = BgL_matchz00_3834;
																					BgL_clausesz00_1276 =
																						BgL_clausesz00_3833;
																					goto
																						BgL_zc3z04anonymousza31250ze3z87_1281;
																				}
																			}
																		}
																}
														}
													}
												else
													{	/* Rgc/rgcrules.scm 86 */
														return
															BGl_errorz00zz__errorz00(BFALSE,
															BGl_string2296z00zz__rgc_rulesz00,
															BgL_clausesz00_1276);
													}
											}
									}
								else
									{	/* Rgc/rgcrules.scm 86 */
										return
											BGl_errorz00zz__errorz00(BFALSE,
											BGl_string2296z00zz__rgc_rulesz00, BgL_clausesz00_1276);
									}
							}
						}
					}
				}
		}

	}



/* &rules->regular-tree */
	obj_t BGl_z62ruleszd2ze3regularzd2treez81zz__rgc_rulesz00(obj_t
		BgL_envz00_3554, obj_t BgL_userzd2envzd2_3555, obj_t BgL_clausesz00_3556)
	{
		{	/* Rgc/rgcrules.scm 80 */
			return
				BGl_ruleszd2ze3regularzd2treeze3zz__rgc_rulesz00(BgL_userzd2envzd2_3555,
				BgL_clausesz00_3556);
		}

	}



/* make-variable-env */
	obj_t BGl_makezd2variablezd2envz00zz__rgc_rulesz00(obj_t BgL_bindingsz00_5)
	{
		{	/* Rgc/rgcrules.scm 138 */
			if (NULLP(BgL_bindingsz00_5))
				{	/* Rgc/rgcrules.scm 139 */
					return BNIL;
				}
			else
				{	/* Rgc/rgcrules.scm 141 */
					obj_t BgL_ezd2125zd2_1345;

					BgL_ezd2125zd2_1345 = CAR(((obj_t) BgL_bindingsz00_5));
					if (PAIRP(BgL_ezd2125zd2_1345))
						{	/* Rgc/rgcrules.scm 141 */
							obj_t BgL_carzd2130zd2_1347;
							obj_t BgL_cdrzd2131zd2_1348;

							BgL_carzd2130zd2_1347 = CAR(BgL_ezd2125zd2_1345);
							BgL_cdrzd2131zd2_1348 = CDR(BgL_ezd2125zd2_1345);
							if (SYMBOLP(BgL_carzd2130zd2_1347))
								{	/* Rgc/rgcrules.scm 141 */
									if (PAIRP(BgL_cdrzd2131zd2_1348))
										{	/* Rgc/rgcrules.scm 141 */
											if (NULLP(CDR(BgL_cdrzd2131zd2_1348)))
												{	/* Rgc/rgcrules.scm 141 */
													obj_t BgL_arg1342z00_1353;

													BgL_arg1342z00_1353 = CAR(BgL_cdrzd2131zd2_1348);
													{	/* Rgc/rgcrules.scm 143 */
														obj_t BgL_envz00_2639;

														{	/* Rgc/rgcrules.scm 143 */
															obj_t BgL_arg1346z00_2640;

															BgL_arg1346z00_2640 =
																CDR(((obj_t) BgL_bindingsz00_5));
															BgL_envz00_2639 =
																BGl_makezd2variablezd2envz00zz__rgc_rulesz00
																(BgL_arg1346z00_2640);
														}
														{	/* Rgc/rgcrules.scm 144 */
															obj_t BgL_arg1344z00_2641;

															BgL_arg1344z00_2641 =
																MAKE_YOUNG_PAIR(BgL_carzd2130zd2_1347,
																BgL_arg1342z00_1353);
															return MAKE_YOUNG_PAIR(BgL_arg1344z00_2641,
																BgL_envz00_2639);
														}
													}
												}
											else
												{	/* Rgc/rgcrules.scm 141 */
													return
														BGl_errorz00zz__errorz00(BFALSE,
														BGl_string2297z00zz__rgc_rulesz00,
														BgL_ezd2125zd2_1345);
												}
										}
									else
										{	/* Rgc/rgcrules.scm 141 */
											return
												BGl_errorz00zz__errorz00(BFALSE,
												BGl_string2297z00zz__rgc_rulesz00, BgL_ezd2125zd2_1345);
										}
								}
							else
								{	/* Rgc/rgcrules.scm 141 */
									return
										BGl_errorz00zz__errorz00(BFALSE,
										BGl_string2297z00zz__rgc_rulesz00, BgL_ezd2125zd2_1345);
								}
						}
					else
						{	/* Rgc/rgcrules.scm 141 */
							return
								BGl_errorz00zz__errorz00(BFALSE,
								BGl_string2297z00zz__rgc_rulesz00, BgL_ezd2125zd2_1345);
						}
				}
		}

	}



/* special-char? */
	BGL_EXPORTED_DEF bool_t BGl_specialzd2charzf3z21zz__rgc_rulesz00(int
		BgL_charz00_6)
	{
		{	/* Rgc/rgcrules.scm 171 */
			{	/* Rgc/rgcrules.scm 172 */
				obj_t BgL_arg1348z00_2652;

				BgL_arg1348z00_2652 = BGl_rgczd2maxzd2charz00zz__rgc_configz00();
				return ((long) (BgL_charz00_6) >= (long) CINT(BgL_arg1348z00_2652));
		}}

	}



/* &special-char? */
	obj_t BGl_z62specialzd2charzf3z43zz__rgc_rulesz00(obj_t BgL_envz00_3557,
		obj_t BgL_charz00_3558)
	{
		{	/* Rgc/rgcrules.scm 171 */
			{	/* Rgc/rgcrules.scm 172 */
				bool_t BgL_tmpz00_3870;

				{	/* Rgc/rgcrules.scm 172 */
					int BgL_auxz00_3871;

					{	/* Rgc/rgcrules.scm 172 */
						obj_t BgL_tmpz00_3872;

						if (INTEGERP(BgL_charz00_3558))
							{	/* Rgc/rgcrules.scm 172 */
								BgL_tmpz00_3872 = BgL_charz00_3558;
							}
						else
							{
								obj_t BgL_auxz00_3875;

								BgL_auxz00_3875 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string2298z00zz__rgc_rulesz00, BINT(6901L),
									BGl_string2299z00zz__rgc_rulesz00,
									BGl_string2300z00zz__rgc_rulesz00, BgL_charz00_3558);
								FAILURE(BgL_auxz00_3875, BFALSE, BFALSE);
							}
						BgL_auxz00_3871 = CINT(BgL_tmpz00_3872);
					}
					BgL_tmpz00_3870 =
						BGl_specialzd2charzf3z21zz__rgc_rulesz00(BgL_auxz00_3871);
				}
				return BBOOL(BgL_tmpz00_3870);
			}
		}

	}



/* tree-max-char */
	BGL_EXPORTED_DEF obj_t BGl_treezd2maxzd2charz00zz__rgc_rulesz00(void)
	{
		{	/* Rgc/rgcrules.scm 181 */
			{	/* Rgc/rgcrules.scm 182 */
				long BgL_za72za7_2655;

				BgL_za72za7_2655 = BGl_specialzd2charzd2numz00zz__rgc_rulesz00;
				return BINT((1L + BgL_za72za7_2655));
			}
		}

	}



/* &tree-max-char */
	obj_t BGl_z62treezd2maxzd2charz62zz__rgc_rulesz00(obj_t BgL_envz00_3559)
	{
		{	/* Rgc/rgcrules.scm 181 */
			return BGl_treezd2maxzd2charz00zz__rgc_rulesz00();
		}

	}



/* reset-special-match-char! */
	BGL_EXPORTED_DEF obj_t
		BGl_resetzd2specialzd2matchzd2charz12zc0zz__rgc_rulesz00(void)
	{
		{	/* Rgc/rgcrules.scm 196 */
			BGl_za2submatchzf3za2zf3zz__rgc_rulesz00 = ((bool_t) 0);
			BGl_za2predicatesza2z00zz__rgc_rulesz00 = BNIL;
			return (BGl_za2specialzd2matchzd2charza2z00zz__rgc_rulesz00 =
				BNIL, BUNSPEC);
		}

	}



/* &reset-special-match-char! */
	obj_t BGl_z62resetzd2specialzd2matchzd2charz12za2zz__rgc_rulesz00(obj_t
		BgL_envz00_3560)
	{
		{	/* Rgc/rgcrules.scm 196 */
			return BGl_resetzd2specialzd2matchzd2charz12zc0zz__rgc_rulesz00();
		}

	}



/* special-char-match? */
	BGL_EXPORTED_DEF bool_t BGl_specialzd2charzd2matchzf3zf3zz__rgc_rulesz00(int
		BgL_charz00_9)
	{
		{	/* Rgc/rgcrules.scm 210 */
			{	/* Rgc/rgcrules.scm 211 */
				obj_t BgL_tmpz00_3886;

				BgL_tmpz00_3886 =
					BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BINT(BgL_charz00_9),
					BGl_za2specialzd2matchzd2charza2z00zz__rgc_rulesz00);
				return PAIRP(BgL_tmpz00_3886);
			}
		}

	}



/* &special-char-match? */
	obj_t BGl_z62specialzd2charzd2matchzf3z91zz__rgc_rulesz00(obj_t
		BgL_envz00_3561, obj_t BgL_charz00_3562)
	{
		{	/* Rgc/rgcrules.scm 210 */
			{	/* Rgc/rgcrules.scm 211 */
				bool_t BgL_tmpz00_3890;

				{	/* Rgc/rgcrules.scm 211 */
					int BgL_auxz00_3891;

					{	/* Rgc/rgcrules.scm 211 */
						obj_t BgL_tmpz00_3892;

						if (INTEGERP(BgL_charz00_3562))
							{	/* Rgc/rgcrules.scm 211 */
								BgL_tmpz00_3892 = BgL_charz00_3562;
							}
						else
							{
								obj_t BgL_auxz00_3895;

								BgL_auxz00_3895 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string2298z00zz__rgc_rulesz00, BINT(8835L),
									BGl_string2301z00zz__rgc_rulesz00,
									BGl_string2300z00zz__rgc_rulesz00, BgL_charz00_3562);
								FAILURE(BgL_auxz00_3895, BFALSE, BFALSE);
							}
						BgL_auxz00_3891 = CINT(BgL_tmpz00_3892);
					}
					BgL_tmpz00_3890 =
						BGl_specialzd2charzd2matchzf3zf3zz__rgc_rulesz00(BgL_auxz00_3891);
				}
				return BBOOL(BgL_tmpz00_3890);
			}
		}

	}



/* special-match-char->rule-number */
	BGL_EXPORTED_DEF int
		BGl_specialzd2matchzd2charzd2ze3rulezd2numberze3zz__rgc_rulesz00(int
		BgL_charz00_10)
	{
		{	/* Rgc/rgcrules.scm 216 */
			{	/* Rgc/rgcrules.scm 217 */
				obj_t BgL_arg1351z00_2658;

				BgL_arg1351z00_2658 =
					BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BINT(BgL_charz00_10),
					BGl_za2specialzd2matchzd2charza2z00zz__rgc_rulesz00);
				return CINT(CDR(((obj_t) BgL_arg1351z00_2658)));
			}
		}

	}



/* &special-match-char->rule-number */
	obj_t
		BGl_z62specialzd2matchzd2charzd2ze3rulezd2numberz81zz__rgc_rulesz00(obj_t
		BgL_envz00_3563, obj_t BgL_charz00_3564)
	{
		{	/* Rgc/rgcrules.scm 216 */
			{	/* Rgc/rgcrules.scm 217 */
				int BgL_tmpz00_3907;

				{	/* Rgc/rgcrules.scm 217 */
					int BgL_auxz00_3908;

					{	/* Rgc/rgcrules.scm 217 */
						obj_t BgL_tmpz00_3909;

						if (INTEGERP(BgL_charz00_3564))
							{	/* Rgc/rgcrules.scm 217 */
								BgL_tmpz00_3909 = BgL_charz00_3564;
							}
						else
							{
								obj_t BgL_auxz00_3912;

								BgL_auxz00_3912 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string2298z00zz__rgc_rulesz00, BINT(9148L),
									BGl_string2302z00zz__rgc_rulesz00,
									BGl_string2300z00zz__rgc_rulesz00, BgL_charz00_3564);
								FAILURE(BgL_auxz00_3912, BFALSE, BFALSE);
							}
						BgL_auxz00_3908 = CINT(BgL_tmpz00_3909);
					}
					BgL_tmpz00_3907 =
						BGl_specialzd2matchzd2charzd2ze3rulezd2numberze3zz__rgc_rulesz00
						(BgL_auxz00_3908);
				}
				return BINT(BgL_tmpz00_3907);
			}
		}

	}



/* get-new-submatch */
	long BGl_getzd2newzd2submatchz00zz__rgc_rulesz00(void)
	{
		{	/* Rgc/rgcrules.scm 233 */
			BGl_za2submatchzf3za2zf3zz__rgc_rulesz00 = ((bool_t) 1);
			BGl_za2submatchzd2countza2zd2zz__rgc_rulesz00 =
				(1L + BGl_za2submatchzd2countza2zd2zz__rgc_rulesz00);
			return BGl_za2submatchzd2countza2zd2zz__rgc_rulesz00;
		}

	}



/* add-predicate-match! */
	obj_t BGl_addzd2predicatezd2matchz12z12zz__rgc_rulesz00(long BgL_matchz00_11,
		obj_t BgL_predicatez00_12)
	{
		{	/* Rgc/rgcrules.scm 241 */
			{	/* Rgc/rgcrules.scm 242 */
				obj_t BgL_cellz00_1363;

				BgL_cellz00_1363 =
					BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BINT(BgL_matchz00_11),
					BGl_za2predicatesza2z00zz__rgc_rulesz00);
				if (PAIRP(BgL_cellz00_1363))
					{	/* Rgc/rgcrules.scm 244 */
						obj_t BgL_arg1354z00_1365;

						BgL_arg1354z00_1365 =
							MAKE_YOUNG_PAIR(BgL_predicatez00_12, CDR(BgL_cellz00_1363));
						return SET_CDR(BgL_cellz00_1363, BgL_arg1354z00_1365);
					}
				else
					{	/* Rgc/rgcrules.scm 245 */
						obj_t BgL_arg1357z00_1367;

						{	/* Rgc/rgcrules.scm 245 */
							obj_t BgL_arg1358z00_1368;

							{	/* Rgc/rgcrules.scm 245 */
								obj_t BgL_list1359z00_1369;

								BgL_list1359z00_1369 =
									MAKE_YOUNG_PAIR(BgL_predicatez00_12, BNIL);
								BgL_arg1358z00_1368 = BgL_list1359z00_1369;
							}
							BgL_arg1357z00_1367 =
								MAKE_YOUNG_PAIR(BINT(BgL_matchz00_11), BgL_arg1358z00_1368);
						}
						return (BGl_za2predicatesza2z00zz__rgc_rulesz00 =
							MAKE_YOUNG_PAIR(BgL_arg1357z00_1367,
								BGl_za2predicatesza2z00zz__rgc_rulesz00), BUNSPEC);
					}
			}
		}

	}



/* predicate-match */
	BGL_EXPORTED_DEF obj_t BGl_predicatezd2matchzd2zz__rgc_rulesz00(int
		BgL_matchz00_13)
	{
		{	/* Rgc/rgcrules.scm 251 */
			{	/* Rgc/rgcrules.scm 252 */
				obj_t BgL_cellz00_2664;

				BgL_cellz00_2664 =
					BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BINT(BgL_matchz00_13),
					BGl_za2predicatesza2z00zz__rgc_rulesz00);
				if (PAIRP(BgL_cellz00_2664))
					{	/* Rgc/rgcrules.scm 253 */
						return CDR(BgL_cellz00_2664);
					}
				else
					{	/* Rgc/rgcrules.scm 253 */
						return BFALSE;
					}
			}
		}

	}



/* &predicate-match */
	obj_t BGl_z62predicatezd2matchzb0zz__rgc_rulesz00(obj_t BgL_envz00_3565,
		obj_t BgL_matchz00_3566)
	{
		{	/* Rgc/rgcrules.scm 251 */
			{	/* Rgc/rgcrules.scm 252 */
				int BgL_auxz00_3936;

				{	/* Rgc/rgcrules.scm 252 */
					obj_t BgL_tmpz00_3937;

					if (INTEGERP(BgL_matchz00_3566))
						{	/* Rgc/rgcrules.scm 252 */
							BgL_tmpz00_3937 = BgL_matchz00_3566;
						}
					else
						{
							obj_t BgL_auxz00_3940;

							BgL_auxz00_3940 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2298z00zz__rgc_rulesz00, BINT(10790L),
								BGl_string2303z00zz__rgc_rulesz00,
								BGl_string2300z00zz__rgc_rulesz00, BgL_matchz00_3566);
							FAILURE(BgL_auxz00_3940, BFALSE, BFALSE);
						}
					BgL_auxz00_3936 = CINT(BgL_tmpz00_3937);
				}
				return BGl_predicatezd2matchzd2zz__rgc_rulesz00(BgL_auxz00_3936);
			}
		}

	}



/* expand-match-rule */
	obj_t BGl_expandzd2matchzd2rulez00zz__rgc_rulesz00(long BgL_matchz00_14,
		obj_t BgL_envz00_15, obj_t BgL_rulez00_16)
	{
		{	/* Rgc/rgcrules.scm 264 */
			BGl_za2submatchzd2countza2zd2zz__rgc_rulesz00 = 0L;
			{	/* Rgc/rgcrules.scm 266 */
				long BgL_specialzd2charzd2_1372;

				BGl_specialzd2charzd2numz00zz__rgc_rulesz00 =
					(1L + BGl_specialzd2charzd2numz00zz__rgc_rulesz00);
				BgL_specialzd2charzd2_1372 =
					BGl_specialzd2charzd2numz00zz__rgc_rulesz00;
				{	/* Rgc/rgcrules.scm 205 */
					obj_t BgL_arg1349z00_2668;

					BgL_arg1349z00_2668 =
						MAKE_YOUNG_PAIR(BINT(BgL_specialzd2charzd2_1372),
						BINT(BgL_matchz00_14));
					BGl_za2specialzd2matchzd2charza2z00zz__rgc_rulesz00 =
						MAKE_YOUNG_PAIR(BgL_arg1349z00_2668,
						BGl_za2specialzd2matchzd2charza2z00zz__rgc_rulesz00);
				}
				{
					obj_t BgL_rulez00_1374;

					BgL_rulez00_1374 = BgL_rulez00_16;
					{	/* Rgc/rgcrules.scm 272 */
						obj_t BgL_arg1362z00_1376;

						{	/* Rgc/rgcrules.scm 272 */
							obj_t BgL_arg1363z00_1377;

							{
								obj_t BgL_rulez00_1381;

								BgL_rulez00_1381 = BgL_rulez00_1374;
							BgL_zc3z04anonymousza31366ze3z87_1382:
								if (PAIRP(BgL_rulez00_1381))
									{	/* Rgc/rgcrules.scm 272 */
										obj_t BgL_cdrzd2153zd2_1400;

										BgL_cdrzd2153zd2_1400 = CDR(((obj_t) BgL_rulez00_1381));
										if (
											(CAR(
													((obj_t) BgL_rulez00_1381)) ==
												BGl_symbol2304z00zz__rgc_rulesz00))
											{	/* Rgc/rgcrules.scm 272 */
												if (PAIRP(BgL_cdrzd2153zd2_1400))
													{	/* Rgc/rgcrules.scm 272 */
														obj_t BgL_cdrzd2157zd2_1404;

														BgL_cdrzd2157zd2_1404 = CDR(BgL_cdrzd2153zd2_1400);
														if (PAIRP(BgL_cdrzd2157zd2_1404))
															{	/* Rgc/rgcrules.scm 272 */
																if (NULLP(CDR(BgL_cdrzd2157zd2_1404)))
																	{	/* Rgc/rgcrules.scm 272 */
																		obj_t BgL_arg1375z00_1408;
																		obj_t BgL_arg1376z00_1409;

																		BgL_arg1375z00_1408 =
																			CAR(BgL_cdrzd2153zd2_1400);
																		BgL_arg1376z00_1409 =
																			CAR(BgL_cdrzd2157zd2_1404);
																		BGl_addzd2predicatezd2matchz12z12zz__rgc_rulesz00
																			(BgL_matchz00_14, BgL_arg1375z00_1408);
																		{
																			obj_t BgL_rulez00_3970;

																			BgL_rulez00_3970 = BgL_arg1376z00_1409;
																			BgL_rulez00_1381 = BgL_rulez00_3970;
																			goto
																				BgL_zc3z04anonymousza31366ze3z87_1382;
																		}
																	}
																else
																	{	/* Rgc/rgcrules.scm 272 */
																		BgL_arg1363z00_1377 =
																			BGl_expandzd2rulezd2zz__rgc_rulesz00
																			(BgL_matchz00_14, BgL_envz00_15,
																			BgL_rulez00_1381);
																	}
															}
														else
															{	/* Rgc/rgcrules.scm 272 */
																BgL_arg1363z00_1377 =
																	BGl_expandzd2rulezd2zz__rgc_rulesz00
																	(BgL_matchz00_14, BgL_envz00_15,
																	BgL_rulez00_1381);
															}
													}
												else
													{	/* Rgc/rgcrules.scm 272 */
														BgL_arg1363z00_1377 =
															BGl_expandzd2rulezd2zz__rgc_rulesz00
															(BgL_matchz00_14, BgL_envz00_15,
															BgL_rulez00_1381);
													}
											}
										else
											{	/* Rgc/rgcrules.scm 272 */
												if (
													(CAR(
															((obj_t) BgL_rulez00_1381)) ==
														BGl_symbol2306z00zz__rgc_rulesz00))
													{	/* Rgc/rgcrules.scm 272 */
														if (PAIRP(BgL_cdrzd2153zd2_1400))
															{	/* Rgc/rgcrules.scm 272 */
																obj_t BgL_cdrzd2208zd2_1415;

																BgL_cdrzd2208zd2_1415 =
																	CDR(BgL_cdrzd2153zd2_1400);
																if (PAIRP(BgL_cdrzd2208zd2_1415))
																	{	/* Rgc/rgcrules.scm 272 */
																		if (NULLP(CDR(BgL_cdrzd2208zd2_1415)))
																			{	/* Rgc/rgcrules.scm 272 */
																				obj_t BgL_arg1384z00_1419;
																				obj_t BgL_arg1387z00_1420;

																				BgL_arg1384z00_1419 =
																					CAR(BgL_cdrzd2153zd2_1400);
																				BgL_arg1387z00_1420 =
																					CAR(BgL_cdrzd2208zd2_1415);
																				{	/* Rgc/rgcrules.scm 279 */
																					obj_t BgL_arg1424z00_2730;

																					{	/* Rgc/rgcrules.scm 279 */
																						obj_t BgL_arg1425z00_2731;

																						{	/* Rgc/rgcrules.scm 279 */
																							obj_t BgL_arg1426z00_2732;

																							{	/* Rgc/rgcrules.scm 279 */
																								obj_t BgL_arg1427z00_2733;

																								{	/* Rgc/rgcrules.scm 279 */
																									obj_t BgL_arg1428z00_2734;

																									BgL_arg1428z00_2734 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1384z00_1419, BNIL);
																									BgL_arg1427z00_2733 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2308z00zz__rgc_rulesz00,
																										BgL_arg1428z00_2734);
																								}
																								BgL_arg1426z00_2732 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1427z00_2733, BNIL);
																							}
																							BgL_arg1425z00_2731 =
																								MAKE_YOUNG_PAIR
																								(BGl_symbol2310z00zz__rgc_rulesz00,
																								BgL_arg1426z00_2732);
																						}
																						BgL_arg1424z00_2730 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol2312z00zz__rgc_rulesz00,
																							BgL_arg1425z00_2731);
																					}
																					BGl_addzd2predicatezd2matchz12z12zz__rgc_rulesz00
																						(BgL_matchz00_14,
																						BgL_arg1424z00_2730);
																				}
																				{
																					obj_t BgL_rulez00_3994;

																					BgL_rulez00_3994 =
																						BgL_arg1387z00_1420;
																					BgL_rulez00_1381 = BgL_rulez00_3994;
																					goto
																						BgL_zc3z04anonymousza31366ze3z87_1382;
																				}
																			}
																		else
																			{	/* Rgc/rgcrules.scm 272 */
																				BgL_arg1363z00_1377 =
																					BGl_expandzd2rulezd2zz__rgc_rulesz00
																					(BgL_matchz00_14, BgL_envz00_15,
																					BgL_rulez00_1381);
																			}
																	}
																else
																	{	/* Rgc/rgcrules.scm 272 */
																		BgL_arg1363z00_1377 =
																			BGl_expandzd2rulezd2zz__rgc_rulesz00
																			(BgL_matchz00_14, BgL_envz00_15,
																			BgL_rulez00_1381);
																	}
															}
														else
															{	/* Rgc/rgcrules.scm 272 */
																BgL_arg1363z00_1377 =
																	BGl_expandzd2rulezd2zz__rgc_rulesz00
																	(BgL_matchz00_14, BgL_envz00_15,
																	BgL_rulez00_1381);
															}
													}
												else
													{	/* Rgc/rgcrules.scm 272 */
														obj_t BgL_cdrzd2241zd2_1422;

														BgL_cdrzd2241zd2_1422 =
															CDR(((obj_t) BgL_rulez00_1381));
														if (
															(CAR(
																	((obj_t) BgL_rulez00_1381)) ==
																BGl_symbol2314z00zz__rgc_rulesz00))
															{	/* Rgc/rgcrules.scm 272 */
																if (PAIRP(BgL_cdrzd2241zd2_1422))
																	{	/* Rgc/rgcrules.scm 272 */
																		if (NULLP(CDR(BgL_cdrzd2241zd2_1422)))
																			{	/* Rgc/rgcrules.scm 272 */
																				obj_t BgL_arg1394z00_1428;

																				BgL_arg1394z00_1428 =
																					CAR(BgL_cdrzd2241zd2_1422);
																				BGl_addzd2predicatezd2matchz12z12zz__rgc_rulesz00
																					(BgL_matchz00_14,
																					BGl_list2316z00zz__rgc_rulesz00);
																				{
																					obj_t BgL_rulez00_4011;

																					BgL_rulez00_4011 =
																						BgL_arg1394z00_1428;
																					BgL_rulez00_1381 = BgL_rulez00_4011;
																					goto
																						BgL_zc3z04anonymousza31366ze3z87_1382;
																				}
																			}
																		else
																			{	/* Rgc/rgcrules.scm 272 */
																				BgL_arg1363z00_1377 =
																					BGl_expandzd2rulezd2zz__rgc_rulesz00
																					(BgL_matchz00_14, BgL_envz00_15,
																					BgL_rulez00_1381);
																			}
																	}
																else
																	{	/* Rgc/rgcrules.scm 272 */
																		BgL_arg1363z00_1377 =
																			BGl_expandzd2rulezd2zz__rgc_rulesz00
																			(BgL_matchz00_14, BgL_envz00_15,
																			BgL_rulez00_1381);
																	}
															}
														else
															{	/* Rgc/rgcrules.scm 272 */
																if (
																	(CAR(
																			((obj_t) BgL_rulez00_1381)) ==
																		BGl_symbol2321z00zz__rgc_rulesz00))
																	{	/* Rgc/rgcrules.scm 272 */
																		if (PAIRP(BgL_cdrzd2241zd2_1422))
																			{	/* Rgc/rgcrules.scm 272 */
																				if (NULLP(CDR(BgL_cdrzd2241zd2_1422)))
																					{	/* Rgc/rgcrules.scm 272 */
																						obj_t BgL_arg1401z00_1436;

																						BgL_arg1401z00_1436 =
																							CAR(BgL_cdrzd2241zd2_1422);
																						BGl_addzd2predicatezd2matchz12z12zz__rgc_rulesz00
																							(BgL_matchz00_14,
																							BGl_list2323z00zz__rgc_rulesz00);
																						{
																							obj_t BgL_rulez00_4025;

																							BgL_rulez00_4025 =
																								BgL_arg1401z00_1436;
																							BgL_rulez00_1381 =
																								BgL_rulez00_4025;
																							goto
																								BgL_zc3z04anonymousza31366ze3z87_1382;
																						}
																					}
																				else
																					{	/* Rgc/rgcrules.scm 272 */
																						BgL_arg1363z00_1377 =
																							BGl_expandzd2rulezd2zz__rgc_rulesz00
																							(BgL_matchz00_14, BgL_envz00_15,
																							BgL_rulez00_1381);
																					}
																			}
																		else
																			{	/* Rgc/rgcrules.scm 272 */
																				BgL_arg1363z00_1377 =
																					BGl_expandzd2rulezd2zz__rgc_rulesz00
																					(BgL_matchz00_14, BgL_envz00_15,
																					BgL_rulez00_1381);
																			}
																	}
																else
																	{	/* Rgc/rgcrules.scm 272 */
																		obj_t BgL_cdrzd2277zd2_1438;

																		BgL_cdrzd2277zd2_1438 =
																			CDR(((obj_t) BgL_rulez00_1381));
																		if (
																			(CAR(
																					((obj_t) BgL_rulez00_1381)) ==
																				BGl_symbol2347z00zz__rgc_rulesz00))
																			{	/* Rgc/rgcrules.scm 272 */
																				if (PAIRP(BgL_cdrzd2277zd2_1438))
																					{	/* Rgc/rgcrules.scm 272 */
																						if (NULLP(CDR
																								(BgL_cdrzd2277zd2_1438)))
																							{	/* Rgc/rgcrules.scm 272 */
																								obj_t BgL_arg1408z00_1444;

																								BgL_arg1408z00_1444 =
																									CAR(BgL_cdrzd2277zd2_1438);
																								BGl_addzd2predicatezd2matchz12z12zz__rgc_rulesz00
																									(BgL_matchz00_14,
																									BGl_list2349z00zz__rgc_rulesz00);
																								{
																									obj_t BgL_rulez00_4041;

																									BgL_rulez00_4041 =
																										BgL_arg1408z00_1444;
																									BgL_rulez00_1381 =
																										BgL_rulez00_4041;
																									goto
																										BgL_zc3z04anonymousza31366ze3z87_1382;
																								}
																							}
																						else
																							{	/* Rgc/rgcrules.scm 272 */
																								BgL_arg1363z00_1377 =
																									BGl_expandzd2rulezd2zz__rgc_rulesz00
																									(BgL_matchz00_14,
																									BgL_envz00_15,
																									BgL_rulez00_1381);
																							}
																					}
																				else
																					{	/* Rgc/rgcrules.scm 272 */
																						BgL_arg1363z00_1377 =
																							BGl_expandzd2rulezd2zz__rgc_rulesz00
																							(BgL_matchz00_14, BgL_envz00_15,
																							BgL_rulez00_1381);
																					}
																			}
																		else
																			{	/* Rgc/rgcrules.scm 272 */
																				if (
																					(CAR(
																							((obj_t) BgL_rulez00_1381)) ==
																						BGl_symbol2352z00zz__rgc_rulesz00))
																					{	/* Rgc/rgcrules.scm 272 */
																						if (PAIRP(BgL_cdrzd2277zd2_1438))
																							{	/* Rgc/rgcrules.scm 272 */
																								if (NULLP(CDR
																										(BgL_cdrzd2277zd2_1438)))
																									{	/* Rgc/rgcrules.scm 272 */
																										obj_t BgL_arg1416z00_1452;

																										BgL_arg1416z00_1452 =
																											CAR
																											(BgL_cdrzd2277zd2_1438);
																										BGl_addzd2predicatezd2matchz12z12zz__rgc_rulesz00
																											(BgL_matchz00_14,
																											BGl_list2354z00zz__rgc_rulesz00);
																										{
																											obj_t BgL_rulez00_4055;

																											BgL_rulez00_4055 =
																												BgL_arg1416z00_1452;
																											BgL_rulez00_1381 =
																												BgL_rulez00_4055;
																											goto
																												BgL_zc3z04anonymousza31366ze3z87_1382;
																										}
																									}
																								else
																									{	/* Rgc/rgcrules.scm 272 */
																										BgL_arg1363z00_1377 =
																											BGl_expandzd2rulezd2zz__rgc_rulesz00
																											(BgL_matchz00_14,
																											BgL_envz00_15,
																											BgL_rulez00_1381);
																									}
																							}
																						else
																							{	/* Rgc/rgcrules.scm 272 */
																								BgL_arg1363z00_1377 =
																									BGl_expandzd2rulezd2zz__rgc_rulesz00
																									(BgL_matchz00_14,
																									BgL_envz00_15,
																									BgL_rulez00_1381);
																							}
																					}
																				else
																					{	/* Rgc/rgcrules.scm 272 */
																						BgL_arg1363z00_1377 =
																							BGl_expandzd2rulezd2zz__rgc_rulesz00
																							(BgL_matchz00_14, BgL_envz00_15,
																							BgL_rulez00_1381);
																					}
																			}
																	}
															}
													}
											}
									}
								else
									{	/* Rgc/rgcrules.scm 272 */
										BgL_arg1363z00_1377 =
											BGl_expandzd2rulezd2zz__rgc_rulesz00(BgL_matchz00_14,
											BgL_envz00_15, BgL_rulez00_1381);
									}
							}
							{	/* Rgc/rgcrules.scm 272 */
								obj_t BgL_list1364z00_1378;

								{	/* Rgc/rgcrules.scm 272 */
									obj_t BgL_arg1365z00_1379;

									BgL_arg1365z00_1379 =
										MAKE_YOUNG_PAIR(BINT(BgL_specialzd2charzd2_1372), BNIL);
									BgL_list1364z00_1378 =
										MAKE_YOUNG_PAIR(BgL_arg1363z00_1377, BgL_arg1365z00_1379);
								}
								BgL_arg1362z00_1376 = BgL_list1364z00_1378;
							}
						}
						return BGl_makezd2sequencezd2zz__rgc_rulesz00(BgL_arg1362z00_1376);
					}
				}
			}
		}

	}



/* expand-rule */
	obj_t BGl_expandzd2rulezd2zz__rgc_rulesz00(long BgL_matchz00_17,
		obj_t BgL_envz00_18, obj_t BgL_rulez00_19)
	{
		{	/* Rgc/rgcrules.scm 312 */
		BGl_expandzd2rulezd2zz__rgc_rulesz00:
			if (PAIRP(BgL_rulez00_19))
				{	/* Rgc/rgcrules.scm 315 */
					obj_t BgL_cdrzd2337zd2_1510;

					BgL_cdrzd2337zd2_1510 = CDR(BgL_rulez00_19);
					if ((CAR(BgL_rulez00_19) == BGl_symbol2360z00zz__rgc_rulesz00))
						{	/* Rgc/rgcrules.scm 315 */
							if (PAIRP(BgL_cdrzd2337zd2_1510))
								{	/* Rgc/rgcrules.scm 315 */
									obj_t BgL_cdrzd2341zd2_1514;

									BgL_cdrzd2341zd2_1514 = CDR(BgL_cdrzd2337zd2_1510);
									if (PAIRP(BgL_cdrzd2341zd2_1514))
										{	/* Rgc/rgcrules.scm 315 */
											if (NULLP(CDR(BgL_cdrzd2341zd2_1514)))
												{	/* Rgc/rgcrules.scm 315 */
													return
														BGl_expandzd2dotszd2zz__rgc_rulesz00
														(BgL_matchz00_17, BgL_envz00_18,
														CAR(BgL_cdrzd2337zd2_1510),
														CAR(BgL_cdrzd2341zd2_1514), BgL_rulez00_19);
												}
											else
												{	/* Rgc/rgcrules.scm 315 */
													return
														BGl_errorz00zz__errorz00(BFALSE,
														BGl_string2362z00zz__rgc_rulesz00, BgL_rulez00_19);
												}
										}
									else
										{	/* Rgc/rgcrules.scm 315 */
											return
												BGl_errorz00zz__errorz00(BFALSE,
												BGl_string2362z00zz__rgc_rulesz00, BgL_rulez00_19);
										}
								}
							else
								{	/* Rgc/rgcrules.scm 315 */
									return
										BGl_errorz00zz__errorz00(BFALSE,
										BGl_string2362z00zz__rgc_rulesz00, BgL_rulez00_19);
								}
						}
					else
						{	/* Rgc/rgcrules.scm 315 */
							if ((CAR(BgL_rulez00_19) == BGl_symbol2363z00zz__rgc_rulesz00))
								{	/* Rgc/rgcrules.scm 315 */
									if (PAIRP(BgL_cdrzd2337zd2_1510))
										{	/* Rgc/rgcrules.scm 315 */
											if (NULLP(CDR(BgL_cdrzd2337zd2_1510)))
												{	/* Rgc/rgcrules.scm 315 */
													BGL_TAIL return
														BGl_expandzd2uncasezd2zz__rgc_rulesz00
														(BgL_matchz00_17, BgL_envz00_18,
														CAR(BgL_cdrzd2337zd2_1510));
												}
											else
												{	/* Rgc/rgcrules.scm 315 */
													return
														BGl_errorz00zz__errorz00(BFALSE,
														BGl_string2362z00zz__rgc_rulesz00, BgL_rulez00_19);
												}
										}
									else
										{	/* Rgc/rgcrules.scm 315 */
											return
												BGl_errorz00zz__errorz00(BFALSE,
												BGl_string2362z00zz__rgc_rulesz00, BgL_rulez00_19);
										}
								}
							else
								{	/* Rgc/rgcrules.scm 315 */
									obj_t BgL_cdrzd2994zd2_1529;

									BgL_cdrzd2994zd2_1529 = CDR(BgL_rulez00_19);
									if (
										(CAR(BgL_rulez00_19) == BGl_symbol2365z00zz__rgc_rulesz00))
										{	/* Rgc/rgcrules.scm 315 */
											if (PAIRP(BgL_cdrzd2994zd2_1529))
												{	/* Rgc/rgcrules.scm 315 */
													if (NULLP(CDR(BgL_cdrzd2994zd2_1529)))
														{	/* Rgc/rgcrules.scm 315 */
															obj_t BgL_arg1452z00_1535;

															BgL_arg1452z00_1535 = CAR(BgL_cdrzd2994zd2_1529);
															{	/* Rgc/rgcrules.scm 459 */
																obj_t BgL_arg1691z00_2827;

																BgL_arg1691z00_2827 =
																	MAKE_YOUNG_PAIR
																	(BGl_expandzd2rulezd2zz__rgc_rulesz00
																	(BgL_matchz00_17, BgL_envz00_18,
																		BgL_arg1452z00_1535), BNIL);
																return
																	MAKE_YOUNG_PAIR
																	(BGl_symbol2365z00zz__rgc_rulesz00,
																	BgL_arg1691z00_2827);
															}
														}
													else
														{	/* Rgc/rgcrules.scm 315 */
															return
																BGl_errorz00zz__errorz00(BFALSE,
																BGl_string2362z00zz__rgc_rulesz00,
																BgL_rulez00_19);
														}
												}
											else
												{	/* Rgc/rgcrules.scm 315 */
													return
														BGl_errorz00zz__errorz00(BFALSE,
														BGl_string2362z00zz__rgc_rulesz00, BgL_rulez00_19);
												}
										}
									else
										{	/* Rgc/rgcrules.scm 315 */
											if (
												(CAR(BgL_rulez00_19) ==
													BGl_symbol2367z00zz__rgc_rulesz00))
												{	/* Rgc/rgcrules.scm 315 */
													if (PAIRP(BgL_cdrzd2994zd2_1529))
														{	/* Rgc/rgcrules.scm 315 */
															if (NULLP(CDR(BgL_cdrzd2994zd2_1529)))
																{	/* Rgc/rgcrules.scm 315 */
																	BGL_TAIL return
																		BGl_expandzd2zb2z60zz__rgc_rulesz00
																		(BgL_matchz00_17, BgL_envz00_18,
																		CAR(BgL_cdrzd2994zd2_1529));
																}
															else
																{	/* Rgc/rgcrules.scm 315 */
																	return
																		BGl_errorz00zz__errorz00(BFALSE,
																		BGl_string2362z00zz__rgc_rulesz00,
																		BgL_rulez00_19);
																}
														}
													else
														{	/* Rgc/rgcrules.scm 315 */
															return
																BGl_errorz00zz__errorz00(BFALSE,
																BGl_string2362z00zz__rgc_rulesz00,
																BgL_rulez00_19);
														}
												}
											else
												{	/* Rgc/rgcrules.scm 315 */
													obj_t BgL_cdrzd21430zd2_1545;

													BgL_cdrzd21430zd2_1545 = CDR(BgL_rulez00_19);
													if (
														(CAR(BgL_rulez00_19) ==
															BGl_symbol2369z00zz__rgc_rulesz00))
														{	/* Rgc/rgcrules.scm 315 */
															if (PAIRP(BgL_cdrzd21430zd2_1545))
																{	/* Rgc/rgcrules.scm 315 */
																	if (NULLP(CDR(BgL_cdrzd21430zd2_1545)))
																		{	/* Rgc/rgcrules.scm 315 */
																			obj_t BgL_arg1466z00_1551;

																			BgL_arg1466z00_1551 =
																				CAR(BgL_cdrzd21430zd2_1545);
																			{	/* Rgc/rgcrules.scm 472 */
																				obj_t BgL_arg1703z00_2837;

																				{	/* Rgc/rgcrules.scm 472 */
																					obj_t BgL_arg1704z00_2838;

																					BgL_arg1704z00_2838 =
																						MAKE_YOUNG_PAIR
																						(BGl_expandzd2rulezd2zz__rgc_rulesz00
																						(BgL_matchz00_17, BgL_envz00_18,
																							BgL_arg1466z00_1551), BNIL);
																					BgL_arg1703z00_2837 =
																						MAKE_YOUNG_PAIR
																						(BGl_symbol2371z00zz__rgc_rulesz00,
																						BgL_arg1704z00_2838);
																				}
																				return
																					MAKE_YOUNG_PAIR
																					(BGl_symbol2288z00zz__rgc_rulesz00,
																					BgL_arg1703z00_2837);
																			}
																		}
																	else
																		{	/* Rgc/rgcrules.scm 315 */
																			return
																				BGl_errorz00zz__errorz00(BFALSE,
																				BGl_string2362z00zz__rgc_rulesz00,
																				BgL_rulez00_19);
																		}
																}
															else
																{	/* Rgc/rgcrules.scm 315 */
																	return
																		BGl_errorz00zz__errorz00(BFALSE,
																		BGl_string2362z00zz__rgc_rulesz00,
																		BgL_rulez00_19);
																}
														}
													else
														{	/* Rgc/rgcrules.scm 315 */
															if (
																(CAR(BgL_rulez00_19) ==
																	BGl_symbol2288z00zz__rgc_rulesz00))
																{	/* Rgc/rgcrules.scm 315 */
																	BGL_TAIL return
																		BGl_expandzd2orzd2zz__rgc_rulesz00
																		(BgL_matchz00_17, BgL_envz00_18,
																		BgL_cdrzd21430zd2_1545);
																}
															else
																{	/* Rgc/rgcrules.scm 315 */
																	if (
																		(CAR(BgL_rulez00_19) ==
																			BGl_symbol2373z00zz__rgc_rulesz00))
																		{	/* Rgc/rgcrules.scm 315 */
																			if (PAIRP(BgL_cdrzd21430zd2_1545))
																				{	/* Rgc/rgcrules.scm 315 */
																					obj_t BgL_cdrzd21632zd2_1560;

																					BgL_cdrzd21632zd2_1560 =
																						CDR(BgL_cdrzd21430zd2_1545);
																					if (PAIRP(BgL_cdrzd21632zd2_1560))
																						{	/* Rgc/rgcrules.scm 315 */
																							if (NULLP(CDR
																									(BgL_cdrzd21632zd2_1560)))
																								{	/* Rgc/rgcrules.scm 315 */
																									return
																										BGl_expandzd2zd3z01zz__rgc_rulesz00
																										(BgL_matchz00_17,
																										BgL_envz00_18,
																										CAR(BgL_cdrzd21430zd2_1545),
																										CAR(BgL_cdrzd21632zd2_1560),
																										BgL_rulez00_19);
																								}
																							else
																								{	/* Rgc/rgcrules.scm 315 */
																									return
																										BGl_errorz00zz__errorz00
																										(BFALSE,
																										BGl_string2362z00zz__rgc_rulesz00,
																										BgL_rulez00_19);
																								}
																						}
																					else
																						{	/* Rgc/rgcrules.scm 315 */
																							return
																								BGl_errorz00zz__errorz00(BFALSE,
																								BGl_string2362z00zz__rgc_rulesz00,
																								BgL_rulez00_19);
																						}
																				}
																			else
																				{	/* Rgc/rgcrules.scm 315 */
																					return
																						BGl_errorz00zz__errorz00(BFALSE,
																						BGl_string2362z00zz__rgc_rulesz00,
																						BgL_rulez00_19);
																				}
																		}
																	else
																		{	/* Rgc/rgcrules.scm 315 */
																			obj_t BgL_cdrzd21839zd2_1567;

																			BgL_cdrzd21839zd2_1567 =
																				CDR(BgL_rulez00_19);
																			if (
																				(CAR(BgL_rulez00_19) ==
																					BGl_symbol2375z00zz__rgc_rulesz00))
																				{	/* Rgc/rgcrules.scm 315 */
																					if (PAIRP(BgL_cdrzd21839zd2_1567))
																						{	/* Rgc/rgcrules.scm 315 */
																							obj_t BgL_cdrzd21843zd2_1571;

																							BgL_cdrzd21843zd2_1571 =
																								CDR(BgL_cdrzd21839zd2_1567);
																							if (PAIRP(BgL_cdrzd21843zd2_1571))
																								{	/* Rgc/rgcrules.scm 315 */
																									if (NULLP(CDR
																											(BgL_cdrzd21843zd2_1571)))
																										{	/* Rgc/rgcrules.scm 315 */
																											return
																												BGl_expandzd2ze3zd3ze2zz__rgc_rulesz00
																												(BgL_matchz00_17,
																												BgL_envz00_18,
																												CAR
																												(BgL_cdrzd21839zd2_1567),
																												CAR
																												(BgL_cdrzd21843zd2_1571),
																												BgL_rulez00_19);
																										}
																									else
																										{	/* Rgc/rgcrules.scm 315 */
																											return
																												BGl_errorz00zz__errorz00
																												(BFALSE,
																												BGl_string2362z00zz__rgc_rulesz00,
																												BgL_rulez00_19);
																										}
																								}
																							else
																								{	/* Rgc/rgcrules.scm 315 */
																									return
																										BGl_errorz00zz__errorz00
																										(BFALSE,
																										BGl_string2362z00zz__rgc_rulesz00,
																										BgL_rulez00_19);
																								}
																						}
																					else
																						{	/* Rgc/rgcrules.scm 315 */
																							return
																								BGl_errorz00zz__errorz00(BFALSE,
																								BGl_string2362z00zz__rgc_rulesz00,
																								BgL_rulez00_19);
																						}
																				}
																			else
																				{	/* Rgc/rgcrules.scm 315 */
																					if (
																						(CAR(BgL_rulez00_19) ==
																							BGl_symbol2377z00zz__rgc_rulesz00))
																						{	/* Rgc/rgcrules.scm 315 */
																							if (PAIRP(BgL_cdrzd21839zd2_1567))
																								{	/* Rgc/rgcrules.scm 315 */
																									obj_t BgL_cdrzd22022zd2_1582;

																									BgL_cdrzd22022zd2_1582 =
																										CDR(BgL_cdrzd21839zd2_1567);
																									if (PAIRP
																										(BgL_cdrzd22022zd2_1582))
																										{	/* Rgc/rgcrules.scm 315 */
																											obj_t
																												BgL_cdrzd22027zd2_1584;
																											BgL_cdrzd22027zd2_1584 =
																												CDR
																												(BgL_cdrzd22022zd2_1582);
																											if (PAIRP
																												(BgL_cdrzd22027zd2_1584))
																												{	/* Rgc/rgcrules.scm 315 */
																													if (NULLP(CDR
																															(BgL_cdrzd22027zd2_1584)))
																														{	/* Rgc/rgcrules.scm 315 */
																															return
																																BGl_expandzd2za2za2zd2zz__rgc_rulesz00
																																(BgL_matchz00_17,
																																BgL_envz00_18,
																																CAR
																																(BgL_cdrzd21839zd2_1567),
																																CAR
																																(BgL_cdrzd22022zd2_1582),
																																CAR
																																(BgL_cdrzd22027zd2_1584),
																																BgL_rulez00_19);
																														}
																													else
																														{	/* Rgc/rgcrules.scm 315 */
																															return
																																BGl_errorz00zz__errorz00
																																(BFALSE,
																																BGl_string2362z00zz__rgc_rulesz00,
																																BgL_rulez00_19);
																														}
																												}
																											else
																												{	/* Rgc/rgcrules.scm 315 */
																													return
																														BGl_errorz00zz__errorz00
																														(BFALSE,
																														BGl_string2362z00zz__rgc_rulesz00,
																														BgL_rulez00_19);
																												}
																										}
																									else
																										{	/* Rgc/rgcrules.scm 315 */
																											return
																												BGl_errorz00zz__errorz00
																												(BFALSE,
																												BGl_string2362z00zz__rgc_rulesz00,
																												BgL_rulez00_19);
																										}
																								}
																							else
																								{	/* Rgc/rgcrules.scm 315 */
																									return
																										BGl_errorz00zz__errorz00
																										(BFALSE,
																										BGl_string2362z00zz__rgc_rulesz00,
																										BgL_rulez00_19);
																								}
																						}
																					else
																						{	/* Rgc/rgcrules.scm 315 */
																							if (
																								(CAR(BgL_rulez00_19) ==
																									BGl_symbol2282z00zz__rgc_rulesz00))
																								{	/* Rgc/rgcrules.scm 315 */
																									return
																										BGl_expandzd2inzd2zz__rgc_rulesz00
																										(BgL_matchz00_17,
																										BgL_envz00_18,
																										CDR(BgL_rulez00_19),
																										BgL_rulez00_19);
																								}
																							else
																								{	/* Rgc/rgcrules.scm 315 */
																									if (
																										(CAR(BgL_rulez00_19) ==
																											BGl_symbol2379z00zz__rgc_rulesz00))
																										{	/* Rgc/rgcrules.scm 315 */
																											return
																												BGl_expandzd2outzd2zz__rgc_rulesz00
																												(BgL_matchz00_17,
																												BgL_envz00_18,
																												CDR(BgL_rulez00_19),
																												BgL_rulez00_19);
																										}
																									else
																										{	/* Rgc/rgcrules.scm 315 */
																											obj_t
																												BgL_cdrzd22225zd2_1598;
																											BgL_cdrzd22225zd2_1598 =
																												CDR(BgL_rulez00_19);
																											if ((CAR(BgL_rulez00_19)
																													==
																													BGl_symbol2381z00zz__rgc_rulesz00))
																												{	/* Rgc/rgcrules.scm 315 */
																													if (PAIRP
																														(BgL_cdrzd22225zd2_1598))
																														{	/* Rgc/rgcrules.scm 315 */
																															obj_t
																																BgL_cdrzd22229zd2_1602;
																															BgL_cdrzd22229zd2_1602
																																=
																																CDR
																																(BgL_cdrzd22225zd2_1598);
																															if (PAIRP
																																(BgL_cdrzd22229zd2_1602))
																																{	/* Rgc/rgcrules.scm 315 */
																																	if (NULLP(CDR
																																			(BgL_cdrzd22229zd2_1602)))
																																		{	/* Rgc/rgcrules.scm 315 */
																																			return
																																				BGl_expandzd2andzd2zz__rgc_rulesz00
																																				(BgL_matchz00_17,
																																				BgL_envz00_18,
																																				CAR
																																				(BgL_cdrzd22225zd2_1598),
																																				CAR
																																				(BgL_cdrzd22229zd2_1602),
																																				BgL_rulez00_19);
																																		}
																																	else
																																		{	/* Rgc/rgcrules.scm 315 */
																																			return
																																				BGl_errorz00zz__errorz00
																																				(BFALSE,
																																				BGl_string2362z00zz__rgc_rulesz00,
																																				BgL_rulez00_19);
																																		}
																																}
																															else
																																{	/* Rgc/rgcrules.scm 315 */
																																	return
																																		BGl_errorz00zz__errorz00
																																		(BFALSE,
																																		BGl_string2362z00zz__rgc_rulesz00,
																																		BgL_rulez00_19);
																																}
																														}
																													else
																														{	/* Rgc/rgcrules.scm 315 */
																															return
																																BGl_errorz00zz__errorz00
																																(BFALSE,
																																BGl_string2362z00zz__rgc_rulesz00,
																																BgL_rulez00_19);
																														}
																												}
																											else
																												{	/* Rgc/rgcrules.scm 315 */
																													if (
																														(CAR(BgL_rulez00_19)
																															==
																															BGl_symbol2383z00zz__rgc_rulesz00))
																														{	/* Rgc/rgcrules.scm 315 */
																															if (PAIRP
																																(BgL_cdrzd22225zd2_1598))
																																{	/* Rgc/rgcrules.scm 315 */
																																	obj_t
																																		BgL_cdrzd22307zd2_1613;
																																	BgL_cdrzd22307zd2_1613
																																		=
																																		CDR
																																		(BgL_cdrzd22225zd2_1598);
																																	if (PAIRP
																																		(BgL_cdrzd22307zd2_1613))
																																		{	/* Rgc/rgcrules.scm 315 */
																																			if (NULLP
																																				(CDR
																																					(BgL_cdrzd22307zd2_1613)))
																																				{	/* Rgc/rgcrules.scm 315 */
																																					return
																																						BGl_expandzd2butzd2zz__rgc_rulesz00
																																						(BgL_matchz00_17,
																																						BgL_envz00_18,
																																						CAR
																																						(BgL_cdrzd22225zd2_1598),
																																						CAR
																																						(BgL_cdrzd22307zd2_1613),
																																						BgL_rulez00_19);
																																				}
																																			else
																																				{	/* Rgc/rgcrules.scm 315 */
																																					return
																																						BGl_errorz00zz__errorz00
																																						(BFALSE,
																																						BGl_string2362z00zz__rgc_rulesz00,
																																						BgL_rulez00_19);
																																				}
																																		}
																																	else
																																		{	/* Rgc/rgcrules.scm 315 */
																																			return
																																				BGl_errorz00zz__errorz00
																																				(BFALSE,
																																				BGl_string2362z00zz__rgc_rulesz00,
																																				BgL_rulez00_19);
																																		}
																																}
																															else
																																{	/* Rgc/rgcrules.scm 315 */
																																	return
																																		BGl_errorz00zz__errorz00
																																		(BFALSE,
																																		BGl_string2362z00zz__rgc_rulesz00,
																																		BgL_rulez00_19);
																																}
																														}
																													else
																														{	/* Rgc/rgcrules.scm 315 */
																															obj_t
																																BgL_cdrzd22357zd2_1620;
																															BgL_cdrzd22357zd2_1620
																																=
																																CDR
																																(BgL_rulez00_19);
																															if ((CAR
																																	(BgL_rulez00_19)
																																	==
																																	BGl_symbol2385z00zz__rgc_rulesz00))
																																{	/* Rgc/rgcrules.scm 315 */
																																	if (PAIRP
																																		(BgL_cdrzd22357zd2_1620))
																																		{	/* Rgc/rgcrules.scm 315 */
																																			if (NULLP
																																				(CDR
																																					(BgL_cdrzd22357zd2_1620)))
																																				{	/* Rgc/rgcrules.scm 315 */
																																					return
																																						BGl_expandzd2submatchzd2zz__rgc_rulesz00
																																						(BgL_matchz00_17,
																																						BgL_envz00_18,
																																						CAR
																																						(BgL_cdrzd22357zd2_1620),
																																						BgL_rulez00_19);
																																				}
																																			else
																																				{	/* Rgc/rgcrules.scm 315 */
																																					return
																																						BGl_errorz00zz__errorz00
																																						(BFALSE,
																																						BGl_string2362z00zz__rgc_rulesz00,
																																						BgL_rulez00_19);
																																				}
																																		}
																																	else
																																		{	/* Rgc/rgcrules.scm 315 */
																																			return
																																				BGl_errorz00zz__errorz00
																																				(BFALSE,
																																				BGl_string2362z00zz__rgc_rulesz00,
																																				BgL_rulez00_19);
																																		}
																																}
																															else
																																{	/* Rgc/rgcrules.scm 315 */
																																	obj_t
																																		BgL_carzd22383zd2_1628;
																																	BgL_carzd22383zd2_1628
																																		=
																																		CAR
																																		(BgL_rulez00_19);
																																	if (
																																		(BgL_carzd22383zd2_1628
																																			==
																																			BGl_symbol2387z00zz__rgc_rulesz00))
																																		{	/* Rgc/rgcrules.scm 315 */
																																			return
																																				BGl_expandzd2sequencezd2zz__rgc_rulesz00
																																				(BgL_matchz00_17,
																																				BgL_envz00_18,
																																				BgL_cdrzd22357zd2_1620);
																																		}
																																	else
																																		{	/* Rgc/rgcrules.scm 315 */
																																			if (
																																				(BgL_carzd22383zd2_1628
																																					==
																																					BGl_symbol2389z00zz__rgc_rulesz00))
																																				{	/* Rgc/rgcrules.scm 315 */
																																					return
																																						BGl_expandzd2sequencezd2zz__rgc_rulesz00
																																						(BgL_matchz00_17,
																																						BgL_envz00_18,
																																						BgL_cdrzd22357zd2_1620);
																																				}
																																			else
																																				{	/* Rgc/rgcrules.scm 315 */
																																					obj_t
																																						BgL_cdrzd22390zd2_1630;
																																					BgL_cdrzd22390zd2_1630
																																						=
																																						CDR
																																						(BgL_rulez00_19);
																																					if (
																																						(CAR
																																							(BgL_rulez00_19)
																																							==
																																							BGl_symbol2391z00zz__rgc_rulesz00))
																																						{	/* Rgc/rgcrules.scm 315 */
																																							if (PAIRP(BgL_cdrzd22390zd2_1630))
																																								{	/* Rgc/rgcrules.scm 315 */
																																									if (NULLP(CDR(BgL_cdrzd22390zd2_1630)))
																																										{	/* Rgc/rgcrules.scm 315 */
																																											obj_t
																																												BgL_arg1546z00_1636;
																																											BgL_arg1546z00_1636
																																												=
																																												CAR
																																												(BgL_cdrzd22390zd2_1630);
																																											if (STRINGP(BgL_arg1546z00_1636))
																																												{
																																													obj_t
																																														BgL_rulez00_4277;
																																													BgL_rulez00_4277
																																														=
																																														BGl_posixzd2ze3rgcz31zz__rgc_posixz00
																																														(BgL_arg1546z00_1636);
																																													BgL_rulez00_19
																																														=
																																														BgL_rulez00_4277;
																																													goto
																																														BGl_expandzd2rulezd2zz__rgc_rulesz00;
																																												}
																																											else
																																												{	/* Rgc/rgcrules.scm 728 */
																																													return
																																														BGl_errorz00zz__errorz00
																																														(BFALSE,
																																														BGl_string2362z00zz__rgc_rulesz00,
																																														BgL_rulez00_19);
																																												}
																																										}
																																									else
																																										{	/* Rgc/rgcrules.scm 315 */
																																											return
																																												BGl_errorz00zz__errorz00
																																												(BFALSE,
																																												BGl_string2362z00zz__rgc_rulesz00,
																																												BgL_rulez00_19);
																																										}
																																								}
																																							else
																																								{	/* Rgc/rgcrules.scm 315 */
																																									return
																																										BGl_errorz00zz__errorz00
																																										(BFALSE,
																																										BGl_string2362z00zz__rgc_rulesz00,
																																										BgL_rulez00_19);
																																								}
																																						}
																																					else
																																						{	/* Rgc/rgcrules.scm 315 */
																																							return
																																								BGl_errorz00zz__errorz00
																																								(BFALSE,
																																								BGl_string2362z00zz__rgc_rulesz00,
																																								BgL_rulez00_19);
																																						}
																																				}
																																		}
																																}
																														}
																												}
																										}
																								}
																						}
																				}
																		}
																}
														}
												}
										}
								}
						}
				}
			else
				{	/* Rgc/rgcrules.scm 313 */
					BGL_TAIL return
						BGl_expandzd2atomzd2zz__rgc_rulesz00(BgL_matchz00_17, BgL_envz00_18,
						BgL_rulez00_19);
				}
		}

	}



/* expand-atom */
	obj_t BGl_expandzd2atomzd2zz__rgc_rulesz00(long BgL_matchz00_20,
		obj_t BgL_envz00_21, obj_t BgL_rulez00_22)
	{
		{	/* Rgc/rgcrules.scm 340 */
			if (CHARP(BgL_rulez00_22))
				{	/* Rgc/rgcrules.scm 342 */
					return BINT((CCHAR(BgL_rulez00_22)));
				}
			else
				{	/* Rgc/rgcrules.scm 344 */
					bool_t BgL_test2568z00_4289;

					if (INTEGERP(BgL_rulez00_22))
						{	/* Rgc/rgcrules.scm 344 */
							if (((long) CINT(BgL_rulez00_22) >= 0L))
								{	/* Rgc/rgcrules.scm 346 */
									obj_t BgL_b1113z00_1666;

									BgL_b1113z00_1666 =
										BGl_rgczd2maxzd2charz00zz__rgc_configz00();
									if (INTEGERP(BgL_b1113z00_1666))
										{	/* Rgc/rgcrules.scm 346 */
											BgL_test2568z00_4289 =
												(
												(long) CINT(BgL_rulez00_22) <
												(long) CINT(BgL_b1113z00_1666));
										}
									else
										{	/* Rgc/rgcrules.scm 346 */
											BgL_test2568z00_4289 =
												BGl_2zc3zc3zz__r4_numbers_6_5z00(BgL_rulez00_22,
												BgL_b1113z00_1666);
										}
								}
							else
								{	/* Rgc/rgcrules.scm 345 */
									BgL_test2568z00_4289 = ((bool_t) 0);
								}
						}
					else
						{	/* Rgc/rgcrules.scm 344 */
							BgL_test2568z00_4289 = ((bool_t) 0);
						}
					if (BgL_test2568z00_4289)
						{	/* Rgc/rgcrules.scm 344 */
							return
								BGl_errorz00zz__errorz00(BFALSE,
								BGl_string2393z00zz__rgc_rulesz00, BgL_rulez00_22);
						}
					else
						{	/* Rgc/rgcrules.scm 344 */
							if (STRINGP(BgL_rulez00_22))
								{	/* Rgc/rgcrules.scm 348 */
									return
										BGl_expandzd2stringzd2zz__rgc_rulesz00(BgL_envz00_21,
										BgL_rulez00_22);
								}
							else
								{	/* Rgc/rgcrules.scm 348 */
									if (SYMBOLP(BgL_rulez00_22))
										{	/* Rgc/rgcrules.scm 351 */
											obj_t BgL_cellz00_1661;

											BgL_cellz00_1661 =
												BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_rulez00_22,
												BgL_envz00_21);
											if (PAIRP(BgL_cellz00_1661))
												{	/* Rgc/rgcrules.scm 352 */
													BGL_TAIL return
														BGl_expandzd2rulezd2zz__rgc_rulesz00
														(BgL_matchz00_20, BgL_envz00_21,
														CDR(BgL_cellz00_1661));
												}
											else
												{	/* Rgc/rgcrules.scm 352 */
													return
														BGl_errorz00zz__errorz00(BFALSE,
														BGl_string2394z00zz__rgc_rulesz00, BgL_rulez00_22);
												}
										}
									else
										{	/* Rgc/rgcrules.scm 350 */
											return
												BGl_errorz00zz__errorz00(BFALSE,
												BGl_string2395z00zz__rgc_rulesz00, BgL_rulez00_22);
										}
								}
						}
				}
		}

	}



/* expand-string */
	obj_t BGl_expandzd2stringzd2zz__rgc_rulesz00(obj_t BgL_envz00_23,
		obj_t BgL_rulez00_24)
	{
		{	/* Rgc/rgcrules.scm 367 */
			if ((STRING_LENGTH(BgL_rulez00_24) == 0L))
				{	/* Rgc/rgcrules.scm 368 */
					return
						BGl_errorz00zz__errorz00(BFALSE, BGl_string2396z00zz__rgc_rulesz00,
						BgL_rulez00_24);
				}
			else
				{	/* Rgc/rgcrules.scm 370 */
					obj_t BgL_arg1584z00_1670;

					{	/* Rgc/rgcrules.scm 370 */
						obj_t BgL_l1114z00_1671;

						BgL_l1114z00_1671 =
							BGl_stringzd2ze3listz31zz__r4_strings_6_7z00(BgL_rulez00_24);
						if (NULLP(BgL_l1114z00_1671))
							{	/* Rgc/rgcrules.scm 370 */
								BgL_arg1584z00_1670 = BNIL;
							}
						else
							{	/* Rgc/rgcrules.scm 370 */
								obj_t BgL_head1116z00_1673;

								{	/* Rgc/rgcrules.scm 370 */
									long BgL_arg1594z00_1685;

									BgL_arg1594z00_1685 = (CCHAR(CAR(BgL_l1114z00_1671)));
									BgL_head1116z00_1673 =
										MAKE_YOUNG_PAIR(BINT(BgL_arg1594z00_1685), BNIL);
								}
								{	/* Rgc/rgcrules.scm 370 */
									obj_t BgL_g1119z00_1674;

									BgL_g1119z00_1674 = CDR(BgL_l1114z00_1671);
									{
										obj_t BgL_l1114z00_2919;
										obj_t BgL_tail1117z00_2920;

										BgL_l1114z00_2919 = BgL_g1119z00_1674;
										BgL_tail1117z00_2920 = BgL_head1116z00_1673;
									BgL_zc3z04anonymousza31586ze3z87_2918:
										if (NULLP(BgL_l1114z00_2919))
											{	/* Rgc/rgcrules.scm 370 */
												BgL_arg1584z00_1670 = BgL_head1116z00_1673;
											}
										else
											{	/* Rgc/rgcrules.scm 370 */
												obj_t BgL_newtail1118z00_2927;

												{	/* Rgc/rgcrules.scm 370 */
													long BgL_arg1591z00_2928;

													BgL_arg1591z00_2928 =
														(CCHAR(CAR(((obj_t) BgL_l1114z00_2919))));
													BgL_newtail1118z00_2927 =
														MAKE_YOUNG_PAIR(BINT(BgL_arg1591z00_2928), BNIL);
												}
												SET_CDR(BgL_tail1117z00_2920, BgL_newtail1118z00_2927);
												{	/* Rgc/rgcrules.scm 370 */
													obj_t BgL_arg1589z00_2930;

													BgL_arg1589z00_2930 =
														CDR(((obj_t) BgL_l1114z00_2919));
													{
														obj_t BgL_tail1117z00_4340;
														obj_t BgL_l1114z00_4339;

														BgL_l1114z00_4339 = BgL_arg1589z00_2930;
														BgL_tail1117z00_4340 = BgL_newtail1118z00_2927;
														BgL_tail1117z00_2920 = BgL_tail1117z00_4340;
														BgL_l1114z00_2919 = BgL_l1114z00_4339;
														goto BgL_zc3z04anonymousza31586ze3z87_2918;
													}
												}
											}
									}
								}
							}
					}
					return BGl_makezd2sequencezd2zz__rgc_rulesz00(BgL_arg1584z00_1670);
				}
		}

	}



/* expand-dots */
	obj_t BGl_expandzd2dotszd2zz__rgc_rulesz00(long BgL_matchz00_25,
		obj_t BgL_envz00_26, obj_t BgL_numz00_27, obj_t BgL_rulez00_28,
		obj_t BgL_errz00_29)
	{
		{	/* Rgc/rgcrules.scm 385 */
			{	/* Rgc/rgcrules.scm 395 */
				bool_t BgL_test2578z00_4342;

				if (INTEGERP(BgL_numz00_27))
					{	/* Rgc/rgcrules.scm 395 */
						if (((long) CINT(BgL_numz00_27) > 0L))
							{	/* Rgc/rgcrules.scm 395 */
								BgL_test2578z00_4342 = ((long) CINT(BgL_numz00_27) < 81L);
							}
						else
							{	/* Rgc/rgcrules.scm 395 */
								BgL_test2578z00_4342 = ((bool_t) 0);
							}
					}
				else
					{	/* Rgc/rgcrules.scm 395 */
						BgL_test2578z00_4342 = ((bool_t) 0);
					}
				if (BgL_test2578z00_4342)
					{
						obj_t BgL_rulesz00_1693;

						{	/* Rgc/rgcrules.scm 397 */
							obj_t BgL_ezd22521zd2_1696;

							BgL_ezd22521zd2_1696 =
								BGl_expandzd2rulezd2zz__rgc_rulesz00(BgL_matchz00_25,
								BgL_envz00_26, BgL_rulez00_28);
							if (PAIRP(BgL_ezd22521zd2_1696))
								{	/* Rgc/rgcrules.scm 397 */
									if (
										(CAR(BgL_ezd22521zd2_1696) ==
											BGl_symbol2397z00zz__rgc_rulesz00))
										{	/* Rgc/rgcrules.scm 397 */
											BgL_rulesz00_1693 = CDR(BgL_ezd22521zd2_1696);
											{	/* Rgc/rgcrules.scm 399 */
												obj_t BgL_arg1609z00_1702;

												{	/* Rgc/rgcrules.scm 399 */
													obj_t BgL_arg1610z00_1703;

													{	/* Rgc/rgcrules.scm 399 */
														obj_t BgL_l1125z00_1704;

														BgL_l1125z00_1704 =
															BGl_explodezd2sequenceze70z35zz__rgc_rulesz00
															(BgL_numz00_27, BgL_rulesz00_1693);
														if (NULLP(BgL_l1125z00_1704))
															{	/* Rgc/rgcrules.scm 399 */
																BgL_arg1610z00_1703 = BNIL;
															}
														else
															{	/* Rgc/rgcrules.scm 399 */
																obj_t BgL_head1127z00_1706;

																BgL_head1127z00_1706 =
																	MAKE_YOUNG_PAIR
																	(BGl_makezd2sequencezd2zz__rgc_rulesz00(CAR
																		(BgL_l1125z00_1704)), BNIL);
																{	/* Rgc/rgcrules.scm 399 */
																	obj_t BgL_g1130z00_1707;

																	BgL_g1130z00_1707 = CDR(BgL_l1125z00_1704);
																	{
																		obj_t BgL_l1125z00_2997;
																		obj_t BgL_tail1128z00_2998;

																		BgL_l1125z00_2997 = BgL_g1130z00_1707;
																		BgL_tail1128z00_2998 = BgL_head1127z00_1706;
																	BgL_zc3z04anonymousza31612ze3z87_2996:
																		if (NULLP(BgL_l1125z00_2997))
																			{	/* Rgc/rgcrules.scm 399 */
																				BgL_arg1610z00_1703 =
																					BgL_head1127z00_1706;
																			}
																		else
																			{	/* Rgc/rgcrules.scm 399 */
																				obj_t BgL_newtail1129z00_3005;

																				{	/* Rgc/rgcrules.scm 399 */
																					obj_t BgL_arg1616z00_3006;

																					{	/* Rgc/rgcrules.scm 399 */
																						obj_t BgL_arg1617z00_3007;

																						BgL_arg1617z00_3007 =
																							CAR(((obj_t) BgL_l1125z00_2997));
																						BgL_arg1616z00_3006 =
																							BGl_makezd2sequencezd2zz__rgc_rulesz00
																							(BgL_arg1617z00_3007);
																					}
																					BgL_newtail1129z00_3005 =
																						MAKE_YOUNG_PAIR(BgL_arg1616z00_3006,
																						BNIL);
																				}
																				SET_CDR(BgL_tail1128z00_2998,
																					BgL_newtail1129z00_3005);
																				{	/* Rgc/rgcrules.scm 399 */
																					obj_t BgL_arg1615z00_3008;

																					BgL_arg1615z00_3008 =
																						CDR(((obj_t) BgL_l1125z00_2997));
																					{
																						obj_t BgL_tail1128z00_4373;
																						obj_t BgL_l1125z00_4372;

																						BgL_l1125z00_4372 =
																							BgL_arg1615z00_3008;
																						BgL_tail1128z00_4373 =
																							BgL_newtail1129z00_3005;
																						BgL_tail1128z00_2998 =
																							BgL_tail1128z00_4373;
																						BgL_l1125z00_2997 =
																							BgL_l1125z00_4372;
																						goto
																							BgL_zc3z04anonymousza31612ze3z87_2996;
																					}
																				}
																			}
																	}
																}
															}
													}
													BgL_arg1609z00_1702 =
														BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
														(BgL_arg1610z00_1703, BNIL);
												}
												return
													MAKE_YOUNG_PAIR(BGl_symbol2288z00zz__rgc_rulesz00,
													BgL_arg1609z00_1702);
											}
										}
									else
										{	/* Rgc/rgcrules.scm 397 */
											return
												BGl_errorz00zz__errorz00(BFALSE,
												BGl_string2395z00zz__rgc_rulesz00, BgL_errz00_29);
										}
								}
							else
								{	/* Rgc/rgcrules.scm 397 */
									return
										BGl_errorz00zz__errorz00(BFALSE,
										BGl_string2395z00zz__rgc_rulesz00, BgL_errz00_29);
								}
						}
					}
				else
					{	/* Rgc/rgcrules.scm 395 */
						return
							BGl_errorz00zz__errorz00(BFALSE,
							BGl_string2399z00zz__rgc_rulesz00, BgL_errz00_29);
					}
			}
		}

	}



/* loop~1 */
	obj_t BGl_loopze71ze7zz__rgc_rulesz00(obj_t BgL_numz00_3569,
		obj_t BgL_rulesz00_1726, long BgL_iz00_1727)
	{
		{	/* Rgc/rgcrules.scm 387 */
			{	/* Rgc/rgcrules.scm 389 */
				bool_t BgL_test2585z00_4380;

				if (NULLP(BgL_rulesz00_1726))
					{	/* Rgc/rgcrules.scm 389 */
						BgL_test2585z00_4380 = ((bool_t) 1);
					}
				else
					{	/* Rgc/rgcrules.scm 389 */
						BgL_test2585z00_4380 =
							(BgL_iz00_1727 == (long) CINT(BgL_numz00_3569));
					}
				if (BgL_test2585z00_4380)
					{	/* Rgc/rgcrules.scm 389 */
						return BNIL;
					}
				else
					{	/* Rgc/rgcrules.scm 391 */
						obj_t BgL_firstz00_1731;

						BgL_firstz00_1731 = CAR(((obj_t) BgL_rulesz00_1726));
						{	/* Rgc/rgcrules.scm 392 */
							obj_t BgL_arg1624z00_1732;
							obj_t BgL_arg1625z00_1733;

							{	/* Rgc/rgcrules.scm 392 */
								obj_t BgL_list1626z00_1734;

								BgL_list1626z00_1734 = MAKE_YOUNG_PAIR(BgL_firstz00_1731, BNIL);
								BgL_arg1624z00_1732 = BgL_list1626z00_1734;
							}
							{	/* Rgc/rgcrules.scm 393 */
								obj_t BgL_l1120z00_1735;

								{	/* Rgc/rgcrules.scm 394 */
									obj_t BgL_arg1634z00_1748;
									long BgL_arg1636z00_1749;

									BgL_arg1634z00_1748 = CDR(((obj_t) BgL_rulesz00_1726));
									BgL_arg1636z00_1749 = (BgL_iz00_1727 + 1L);
									BgL_l1120z00_1735 =
										BGl_loopze71ze7zz__rgc_rulesz00(BgL_numz00_3569,
										BgL_arg1634z00_1748, BgL_arg1636z00_1749);
								}
								if (NULLP(BgL_l1120z00_1735))
									{	/* Rgc/rgcrules.scm 393 */
										BgL_arg1625z00_1733 = BNIL;
									}
								else
									{	/* Rgc/rgcrules.scm 393 */
										obj_t BgL_head1122z00_1737;

										BgL_head1122z00_1737 = MAKE_YOUNG_PAIR(BNIL, BNIL);
										{
											obj_t BgL_l1120z00_2960;
											obj_t BgL_tail1123z00_2961;

											BgL_l1120z00_2960 = BgL_l1120z00_1735;
											BgL_tail1123z00_2961 = BgL_head1122z00_1737;
										BgL_zc3z04anonymousza31628ze3z87_2959:
											if (NULLP(BgL_l1120z00_2960))
												{	/* Rgc/rgcrules.scm 393 */
													BgL_arg1625z00_1733 = CDR(BgL_head1122z00_1737);
												}
											else
												{	/* Rgc/rgcrules.scm 393 */
													obj_t BgL_newtail1124z00_2968;

													{	/* Rgc/rgcrules.scm 393 */
														obj_t BgL_arg1631z00_2969;

														{	/* Rgc/rgcrules.scm 393 */
															obj_t BgL_rz00_2970;

															BgL_rz00_2970 = CAR(((obj_t) BgL_l1120z00_2960));
															BgL_arg1631z00_2969 =
																MAKE_YOUNG_PAIR(BgL_firstz00_1731,
																BgL_rz00_2970);
														}
														BgL_newtail1124z00_2968 =
															MAKE_YOUNG_PAIR(BgL_arg1631z00_2969, BNIL);
													}
													SET_CDR(BgL_tail1123z00_2961,
														BgL_newtail1124z00_2968);
													{	/* Rgc/rgcrules.scm 393 */
														obj_t BgL_arg1630z00_2971;

														BgL_arg1630z00_2971 =
															CDR(((obj_t) BgL_l1120z00_2960));
														{
															obj_t BgL_tail1123z00_4406;
															obj_t BgL_l1120z00_4405;

															BgL_l1120z00_4405 = BgL_arg1630z00_2971;
															BgL_tail1123z00_4406 = BgL_newtail1124z00_2968;
															BgL_tail1123z00_2961 = BgL_tail1123z00_4406;
															BgL_l1120z00_2960 = BgL_l1120z00_4405;
															goto BgL_zc3z04anonymousza31628ze3z87_2959;
														}
													}
												}
										}
									}
							}
							return MAKE_YOUNG_PAIR(BgL_arg1624z00_1732, BgL_arg1625z00_1733);
						}
					}
			}
		}

	}



/* explode-sequence~0 */
	obj_t BGl_explodezd2sequenceze70z35zz__rgc_rulesz00(obj_t BgL_numz00_3570,
		obj_t BgL_rulesz00_1723)
	{
		{	/* Rgc/rgcrules.scm 394 */
			return
				BGl_loopze71ze7zz__rgc_rulesz00(BgL_numz00_3570, BgL_rulesz00_1723, 0L);
		}

	}



/* expand-uncase */
	obj_t BGl_expandzd2uncasezd2zz__rgc_rulesz00(long BgL_matchz00_30,
		obj_t BgL_envz00_31, obj_t BgL_rulez00_32)
	{
		{	/* Rgc/rgcrules.scm 422 */
			return
				BGl_loopze70ze7zz__rgc_rulesz00(BGl_expandzd2rulezd2zz__rgc_rulesz00
				(BgL_matchz00_30, BgL_envz00_31, BgL_rulez00_32), BNIL);
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zz__rgc_rulesz00(obj_t BgL_rulez00_1756,
		obj_t BgL_resz00_1757)
	{
		{	/* Rgc/rgcrules.scm 423 */
		BGl_loopze70ze7zz__rgc_rulesz00:
			if (NULLP(BgL_rulez00_1756))
				{	/* Rgc/rgcrules.scm 426 */
					return bgl_reverse_bang(BgL_resz00_1757);
				}
			else
				{	/* Rgc/rgcrules.scm 426 */
					if (PAIRP(BgL_rulez00_1756))
						{	/* Rgc/rgcrules.scm 437 */
							bool_t BgL_test2591z00_4416;

							{	/* Rgc/rgcrules.scm 437 */
								obj_t BgL_tmpz00_4417;

								BgL_tmpz00_4417 = CAR(BgL_rulez00_1756);
								BgL_test2591z00_4416 = PAIRP(BgL_tmpz00_4417);
							}
							if (BgL_test2591z00_4416)
								{	/* Rgc/rgcrules.scm 438 */
									obj_t BgL_arg1642z00_1763;
									obj_t BgL_arg1643z00_1764;

									BgL_arg1642z00_1763 = CDR(BgL_rulez00_1756);
									BgL_arg1643z00_1764 =
										MAKE_YOUNG_PAIR(BGl_loopze70ze7zz__rgc_rulesz00(CAR
											(BgL_rulez00_1756), BNIL), BgL_resz00_1757);
									{
										obj_t BgL_resz00_4425;
										obj_t BgL_rulez00_4424;

										BgL_rulez00_4424 = BgL_arg1642z00_1763;
										BgL_resz00_4425 = BgL_arg1643z00_1764;
										BgL_resz00_1757 = BgL_resz00_4425;
										BgL_rulez00_1756 = BgL_rulez00_4424;
										goto BGl_loopze70ze7zz__rgc_rulesz00;
									}
								}
							else
								{	/* Rgc/rgcrules.scm 437 */
									if (INTEGERP(CAR(BgL_rulez00_1756)))
										{	/* Rgc/rgcrules.scm 440 */
											if (BGl_rgczd2alphabeticzf3z21zz__rgc_configz00(CAR
													(BgL_rulez00_1756)))
												{	/* Rgc/rgcrules.scm 445 */
													obj_t BgL_arg1650z00_1771;
													obj_t BgL_arg1651z00_1772;

													BgL_arg1650z00_1771 = CDR(BgL_rulez00_1756);
													{	/* Rgc/rgcrules.scm 446 */
														obj_t BgL_arg1652z00_1773;

														{	/* Rgc/rgcrules.scm 446 */
															obj_t BgL_arg1653z00_1774;

															{	/* Rgc/rgcrules.scm 446 */
																obj_t BgL_arg1654z00_1775;
																obj_t BgL_arg1656z00_1776;

																BgL_arg1654z00_1775 =
																	BGl_rgczd2upcasezd2zz__rgc_configz00(CAR
																	(BgL_rulez00_1756));
																BgL_arg1656z00_1776 =
																	MAKE_YOUNG_PAIR
																	(BGl_rgczd2downcasezd2zz__rgc_configz00(CAR
																		(BgL_rulez00_1756)), BNIL);
																BgL_arg1653z00_1774 =
																	MAKE_YOUNG_PAIR(BgL_arg1654z00_1775,
																	BgL_arg1656z00_1776);
															}
															BgL_arg1652z00_1773 =
																MAKE_YOUNG_PAIR
																(BGl_symbol2288z00zz__rgc_rulesz00,
																BgL_arg1653z00_1774);
														}
														BgL_arg1651z00_1772 =
															MAKE_YOUNG_PAIR(BgL_arg1652z00_1773,
															BgL_resz00_1757);
													}
													{
														obj_t BgL_resz00_4442;
														obj_t BgL_rulez00_4441;

														BgL_rulez00_4441 = BgL_arg1650z00_1771;
														BgL_resz00_4442 = BgL_arg1651z00_1772;
														BgL_resz00_1757 = BgL_resz00_4442;
														BgL_rulez00_1756 = BgL_rulez00_4441;
														goto BGl_loopze70ze7zz__rgc_rulesz00;
													}
												}
											else
												{	/* Rgc/rgcrules.scm 449 */
													obj_t BgL_arg1663z00_1780;
													obj_t BgL_arg1664z00_1781;

													BgL_arg1663z00_1780 = CDR(BgL_rulez00_1756);
													BgL_arg1664z00_1781 =
														MAKE_YOUNG_PAIR(CAR(BgL_rulez00_1756),
														BgL_resz00_1757);
													{
														obj_t BgL_resz00_4447;
														obj_t BgL_rulez00_4446;

														BgL_rulez00_4446 = BgL_arg1663z00_1780;
														BgL_resz00_4447 = BgL_arg1664z00_1781;
														BgL_resz00_1757 = BgL_resz00_4447;
														BgL_rulez00_1756 = BgL_rulez00_4446;
														goto BGl_loopze70ze7zz__rgc_rulesz00;
													}
												}
										}
									else
										{	/* Rgc/rgcrules.scm 452 */
											obj_t BgL_arg1669z00_1784;
											obj_t BgL_arg1670z00_1785;

											BgL_arg1669z00_1784 = CDR(BgL_rulez00_1756);
											BgL_arg1670z00_1785 =
												MAKE_YOUNG_PAIR(CAR(BgL_rulez00_1756), BgL_resz00_1757);
											{
												obj_t BgL_resz00_4452;
												obj_t BgL_rulez00_4451;

												BgL_rulez00_4451 = BgL_arg1669z00_1784;
												BgL_resz00_4452 = BgL_arg1670z00_1785;
												BgL_resz00_1757 = BgL_resz00_4452;
												BgL_rulez00_1756 = BgL_rulez00_4451;
												goto BGl_loopze70ze7zz__rgc_rulesz00;
											}
										}
								}
						}
					else
						{	/* Rgc/rgcrules.scm 428 */
							if (INTEGERP(BgL_rulez00_1756))
								{	/* Rgc/rgcrules.scm 429 */
									if (BGl_rgczd2alphabeticzf3z21zz__rgc_configz00
										(BgL_rulez00_1756))
										{	/* Rgc/rgcrules.scm 434 */
											obj_t BgL_arg1681z00_1791;

											{	/* Rgc/rgcrules.scm 434 */
												obj_t BgL_arg1684z00_1792;
												obj_t BgL_arg1685z00_1793;

												BgL_arg1684z00_1792 =
													BGl_rgczd2upcasezd2zz__rgc_configz00
													(BgL_rulez00_1756);
												BgL_arg1685z00_1793 =
													MAKE_YOUNG_PAIR(BGl_rgczd2downcasezd2zz__rgc_configz00
													(BgL_rulez00_1756),
													BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
													(BgL_resz00_1757, BNIL));
												BgL_arg1681z00_1791 =
													MAKE_YOUNG_PAIR(BgL_arg1684z00_1792,
													BgL_arg1685z00_1793);
											}
											return
												MAKE_YOUNG_PAIR(BGl_symbol2288z00zz__rgc_rulesz00,
												BgL_arg1681z00_1791);
										}
									else
										{	/* Rgc/rgcrules.scm 430 */
											return BgL_rulez00_1756;
										}
								}
							else
								{	/* Rgc/rgcrules.scm 429 */
									return BgL_rulez00_1756;
								}
						}
				}
		}

	}



/* expand-+ */
	obj_t BGl_expandzd2zb2z60zz__rgc_rulesz00(long BgL_matchz00_36,
		obj_t BgL_envz00_37, obj_t BgL_rulez00_38)
	{
		{	/* Rgc/rgcrules.scm 464 */
			{	/* Rgc/rgcrules.scm 465 */
				obj_t BgL_erulez00_3028;

				BgL_erulez00_3028 =
					BGl_expandzd2rulezd2zz__rgc_rulesz00(BgL_matchz00_36, BgL_envz00_37,
					BgL_rulez00_38);
				{	/* Rgc/rgcrules.scm 466 */
					obj_t BgL_arg1699z00_3029;

					{	/* Rgc/rgcrules.scm 466 */
						obj_t BgL_arg1700z00_3030;

						{	/* Rgc/rgcrules.scm 466 */
							obj_t BgL_arg1701z00_3031;

							{	/* Rgc/rgcrules.scm 466 */
								obj_t BgL_arg1702z00_3032;

								BgL_arg1702z00_3032 = MAKE_YOUNG_PAIR(BgL_erulez00_3028, BNIL);
								BgL_arg1701z00_3031 =
									MAKE_YOUNG_PAIR(BGl_symbol2365z00zz__rgc_rulesz00,
									BgL_arg1702z00_3032);
							}
							BgL_arg1700z00_3030 = MAKE_YOUNG_PAIR(BgL_arg1701z00_3031, BNIL);
						}
						BgL_arg1699z00_3029 =
							MAKE_YOUNG_PAIR(BgL_erulez00_3028, BgL_arg1700z00_3030);
					}
					return
						MAKE_YOUNG_PAIR(BGl_symbol2397z00zz__rgc_rulesz00,
						BgL_arg1699z00_3029);
				}
			}
		}

	}



/* expand-or */
	obj_t BGl_expandzd2orzd2zz__rgc_rulesz00(long BgL_matchz00_42,
		obj_t BgL_envz00_43, obj_t BgL_rulesz00_44)
	{
		{	/* Rgc/rgcrules.scm 477 */
			if (NULLP(BgL_rulesz00_44))
				{	/* Rgc/rgcrules.scm 478 */
					return BGl_symbol2371z00zz__rgc_rulesz00;
				}
			else
				{
					obj_t BgL_rulesz00_1810;
					obj_t BgL_resz00_1811;

					BgL_rulesz00_1810 = BgL_rulesz00_44;
					BgL_resz00_1811 = BNIL;
				BgL_zc3z04anonymousza31707ze3z87_1812:
					if (NULLP(BgL_rulesz00_1810))
						{	/* Rgc/rgcrules.scm 483 */
							obj_t BgL_arg1709z00_1814;

							BgL_arg1709z00_1814 =
								BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(bgl_reverse_bang
								(BgL_resz00_1811), BNIL);
							return MAKE_YOUNG_PAIR(BGl_symbol2288z00zz__rgc_rulesz00,
								BgL_arg1709z00_1814);
						}
					else
						{	/* Rgc/rgcrules.scm 484 */
							obj_t BgL_rulez00_1816;

							{	/* Rgc/rgcrules.scm 484 */
								obj_t BgL_arg1726z00_1828;

								BgL_arg1726z00_1828 = CAR(((obj_t) BgL_rulesz00_1810));
								BgL_rulez00_1816 =
									BGl_expandzd2rulezd2zz__rgc_rulesz00(BgL_matchz00_42,
									BgL_envz00_43, BgL_arg1726z00_1828);
							}
							{	/* Rgc/rgcrules.scm 485 */
								bool_t BgL_test2598z00_4479;

								if (PAIRP(BgL_rulez00_1816))
									{	/* Rgc/rgcrules.scm 485 */
										BgL_test2598z00_4479 =
											(CAR(BgL_rulez00_1816) ==
											BGl_symbol2288z00zz__rgc_rulesz00);
									}
								else
									{	/* Rgc/rgcrules.scm 485 */
										BgL_test2598z00_4479 = ((bool_t) 0);
									}
								if (BgL_test2598z00_4479)
									{	/* Rgc/rgcrules.scm 486 */
										obj_t BgL_arg1717z00_1820;
										obj_t BgL_arg1718z00_1821;

										BgL_arg1717z00_1820 = CDR(((obj_t) BgL_rulesz00_1810));
										BgL_arg1718z00_1821 =
											BGl_appendzd221011zd2zz__rgc_rulesz00(bgl_reverse_bang(CDR
												(BgL_rulez00_1816)), BgL_resz00_1811);
										{
											obj_t BgL_resz00_4490;
											obj_t BgL_rulesz00_4489;

											BgL_rulesz00_4489 = BgL_arg1717z00_1820;
											BgL_resz00_4490 = BgL_arg1718z00_1821;
											BgL_resz00_1811 = BgL_resz00_4490;
											BgL_rulesz00_1810 = BgL_rulesz00_4489;
											goto BgL_zc3z04anonymousza31707ze3z87_1812;
										}
									}
								else
									{	/* Rgc/rgcrules.scm 487 */
										obj_t BgL_arg1723z00_1824;
										obj_t BgL_arg1724z00_1825;

										BgL_arg1723z00_1824 = CDR(((obj_t) BgL_rulesz00_1810));
										BgL_arg1724z00_1825 =
											MAKE_YOUNG_PAIR(BgL_rulez00_1816, BgL_resz00_1811);
										{
											obj_t BgL_resz00_4495;
											obj_t BgL_rulesz00_4494;

											BgL_rulesz00_4494 = BgL_arg1723z00_1824;
											BgL_resz00_4495 = BgL_arg1724z00_1825;
											BgL_resz00_1811 = BgL_resz00_4495;
											BgL_rulesz00_1810 = BgL_rulesz00_4494;
											goto BgL_zc3z04anonymousza31707ze3z87_1812;
										}
									}
							}
						}
				}
		}

	}



/* expand-= */
	obj_t BGl_expandzd2zd3z01zz__rgc_rulesz00(long BgL_matchz00_45,
		obj_t BgL_envz00_46, obj_t BgL_numz00_47, obj_t BgL_rulez00_48,
		obj_t BgL_errz00_49)
	{
		{	/* Rgc/rgcrules.scm 497 */
			{	/* Rgc/rgcrules.scm 498 */
				bool_t BgL_test2600z00_4496;

				if (INTEGERP(BgL_numz00_47))
					{	/* Rgc/rgcrules.scm 498 */
						if (((long) CINT(BgL_numz00_47) > 0L))
							{	/* Rgc/rgcrules.scm 498 */
								BgL_test2600z00_4496 = ((long) CINT(BgL_numz00_47) < 81L);
							}
						else
							{	/* Rgc/rgcrules.scm 498 */
								BgL_test2600z00_4496 = ((bool_t) 0);
							}
					}
				else
					{	/* Rgc/rgcrules.scm 498 */
						BgL_test2600z00_4496 = ((bool_t) 0);
					}
				if (BgL_test2600z00_4496)
					{	/* Rgc/rgcrules.scm 500 */
						obj_t BgL_erulez00_1834;

						BgL_erulez00_1834 =
							BGl_expandzd2rulezd2zz__rgc_rulesz00(BgL_matchz00_45,
							BgL_envz00_46, BgL_rulez00_48);
						return
							BGl_makezd2sequencezd2zz__rgc_rulesz00
							(BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00(make_vector((long)
									CINT(BgL_numz00_47), BgL_erulez00_1834)));
					}
				else
					{	/* Rgc/rgcrules.scm 498 */
						return
							BGl_errorz00zz__errorz00(BFALSE,
							BGl_string2399z00zz__rgc_rulesz00, BgL_errz00_49);
					}
			}
		}

	}



/* expand->= */
	obj_t BGl_expandzd2ze3zd3ze2zz__rgc_rulesz00(long BgL_matchz00_50,
		obj_t BgL_envz00_51, obj_t BgL_numz00_52, obj_t BgL_rulez00_53,
		obj_t BgL_errz00_54)
	{
		{	/* Rgc/rgcrules.scm 511 */
			{	/* Rgc/rgcrules.scm 512 */
				bool_t BgL_test2603z00_4510;

				if (INTEGERP(BgL_numz00_52))
					{	/* Rgc/rgcrules.scm 512 */
						if (((long) CINT(BgL_numz00_52) > 0L))
							{	/* Rgc/rgcrules.scm 512 */
								BgL_test2603z00_4510 = ((long) CINT(BgL_numz00_52) < 81L);
							}
						else
							{	/* Rgc/rgcrules.scm 512 */
								BgL_test2603z00_4510 = ((bool_t) 0);
							}
					}
				else
					{	/* Rgc/rgcrules.scm 512 */
						BgL_test2603z00_4510 = ((bool_t) 0);
					}
				if (BgL_test2603z00_4510)
					{	/* Rgc/rgcrules.scm 514 */
						obj_t BgL_erulez00_1844;

						BgL_erulez00_1844 =
							BGl_expandzd2rulezd2zz__rgc_rulesz00(BgL_matchz00_50,
							BgL_envz00_51, BgL_rulez00_53);
						{	/* Rgc/rgcrules.scm 515 */
							obj_t BgL_arg1738z00_1845;

							{	/* Rgc/rgcrules.scm 515 */
								obj_t BgL_arg1739z00_1846;
								obj_t BgL_arg1740z00_1847;

								BgL_arg1739z00_1846 =
									BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00(make_vector(
										(long) CINT(BgL_numz00_52), BgL_erulez00_1844));
								{	/* Rgc/rgcrules.scm 516 */
									obj_t BgL_arg1743z00_1849;

									{	/* Rgc/rgcrules.scm 516 */
										obj_t BgL_arg1744z00_1850;

										BgL_arg1744z00_1850 =
											MAKE_YOUNG_PAIR(BgL_erulez00_1844, BNIL);
										BgL_arg1743z00_1849 =
											MAKE_YOUNG_PAIR(BGl_symbol2365z00zz__rgc_rulesz00,
											BgL_arg1744z00_1850);
									}
									BgL_arg1740z00_1847 =
										MAKE_YOUNG_PAIR(BgL_arg1743z00_1849, BNIL);
								}
								BgL_arg1738z00_1845 =
									BGl_appendzd221011zd2zz__rgc_rulesz00(BgL_arg1739z00_1846,
									BgL_arg1740z00_1847);
							}
							return
								BGl_makezd2sequencezd2zz__rgc_rulesz00(BgL_arg1738z00_1845);
						}
					}
				else
					{	/* Rgc/rgcrules.scm 512 */
						return
							BGl_errorz00zz__errorz00(BFALSE,
							BGl_string2399z00zz__rgc_rulesz00, BgL_errz00_54);
					}
			}
		}

	}



/* expand-** */
	obj_t BGl_expandzd2za2za2zd2zz__rgc_rulesz00(long BgL_matchz00_55,
		obj_t BgL_envz00_56, obj_t BgL_minz00_57, obj_t BgL_maxz00_58,
		obj_t BgL_rulez00_59, obj_t BgL_errz00_60)
	{
		{	/* Rgc/rgcrules.scm 526 */
			{	/* Rgc/rgcrules.scm 527 */
				bool_t BgL_test2606z00_4528;

				if (INTEGERP(BgL_minz00_57))
					{	/* Rgc/rgcrules.scm 527 */
						if (((long) CINT(BgL_minz00_57) > 0L))
							{	/* Rgc/rgcrules.scm 528 */
								if (INTEGERP(BgL_maxz00_58))
									{	/* Rgc/rgcrules.scm 529 */
										if (
											((long) CINT(BgL_maxz00_58) > (long) CINT(BgL_minz00_57)))
											{	/* Rgc/rgcrules.scm 530 */
												BgL_test2606z00_4528 =
													((long) CINT(BgL_maxz00_58) < 81L);
											}
										else
											{	/* Rgc/rgcrules.scm 530 */
												BgL_test2606z00_4528 = ((bool_t) 0);
											}
									}
								else
									{	/* Rgc/rgcrules.scm 529 */
										BgL_test2606z00_4528 = ((bool_t) 0);
									}
							}
						else
							{	/* Rgc/rgcrules.scm 528 */
								BgL_test2606z00_4528 = ((bool_t) 0);
							}
					}
				else
					{	/* Rgc/rgcrules.scm 527 */
						BgL_test2606z00_4528 = ((bool_t) 0);
					}
				if (BgL_test2606z00_4528)
					{	/* Rgc/rgcrules.scm 533 */
						obj_t BgL_erulez00_1862;

						BgL_erulez00_1862 =
							BGl_expandzd2rulezd2zz__rgc_rulesz00(BgL_matchz00_55,
							BgL_envz00_56, BgL_rulez00_59);
						{
							obj_t BgL_minz00_1865;
							obj_t BgL_resz00_1866;

							BgL_minz00_1865 = BgL_minz00_57;
							BgL_resz00_1866 = BNIL;
						BgL_zc3z04anonymousza31752ze3z87_1867:
							{	/* Rgc/rgcrules.scm 536 */
								bool_t BgL_test2611z00_4543;

								{	/* Rgc/rgcrules.scm 536 */
									bool_t BgL_test2612z00_4544;

									if (INTEGERP(BgL_minz00_1865))
										{	/* Rgc/rgcrules.scm 536 */
											BgL_test2612z00_4544 = INTEGERP(BgL_maxz00_58);
										}
									else
										{	/* Rgc/rgcrules.scm 536 */
											BgL_test2612z00_4544 = ((bool_t) 0);
										}
									if (BgL_test2612z00_4544)
										{	/* Rgc/rgcrules.scm 536 */
											BgL_test2611z00_4543 =
												(
												(long) CINT(BgL_minz00_1865) >
												(long) CINT(BgL_maxz00_58));
										}
									else
										{	/* Rgc/rgcrules.scm 536 */
											BgL_test2611z00_4543 =
												BGl_2ze3ze3zz__r4_numbers_6_5z00(BgL_minz00_1865,
												BgL_maxz00_58);
										}
								}
								if (BgL_test2611z00_4543)
									{	/* Rgc/rgcrules.scm 537 */
										obj_t BgL_arg1755z00_1870;

										BgL_arg1755z00_1870 =
											BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
											(bgl_reverse_bang(BgL_resz00_1866), BNIL);
										return MAKE_YOUNG_PAIR(BGl_symbol2288z00zz__rgc_rulesz00,
											BgL_arg1755z00_1870);
									}
								else
									{	/* Rgc/rgcrules.scm 538 */
										obj_t BgL_arg1757z00_1872;
										obj_t BgL_arg1758z00_1873;

										if (INTEGERP(BgL_minz00_1865))
											{	/* Rgc/rgcrules.scm 538 */
												BgL_arg1757z00_1872 = ADDFX(BgL_minz00_1865, BINT(1L));
											}
										else
											{	/* Rgc/rgcrules.scm 538 */
												BgL_arg1757z00_1872 =
													BGl_2zb2zb2zz__r4_numbers_6_5z00(BgL_minz00_1865,
													BINT(1L));
											}
										BgL_arg1758z00_1873 =
											MAKE_YOUNG_PAIR(BGl_makezd2sequencezd2zz__rgc_rulesz00
											(BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00(make_vector(
														(long) CINT(BgL_minz00_1865), BgL_erulez00_1862))),
											BgL_resz00_1866);
										{
											obj_t BgL_resz00_4567;
											obj_t BgL_minz00_4566;

											BgL_minz00_4566 = BgL_arg1757z00_1872;
											BgL_resz00_4567 = BgL_arg1758z00_1873;
											BgL_resz00_1866 = BgL_resz00_4567;
											BgL_minz00_1865 = BgL_minz00_4566;
											goto BgL_zc3z04anonymousza31752ze3z87_1867;
										}
									}
							}
						}
					}
				else
					{	/* Rgc/rgcrules.scm 527 */
						return
							BGl_errorz00zz__errorz00(BFALSE,
							BGl_string2399z00zz__rgc_rulesz00, BgL_errz00_60);
					}
			}
		}

	}



/* expand-in */
	obj_t BGl_expandzd2inzd2zz__rgc_rulesz00(long BgL_matchz00_61,
		obj_t BgL_envz00_62, obj_t BgL_valuesz00_63, obj_t BgL_errz00_64)
	{
		{	/* Rgc/rgcrules.scm 554 */
			if (NULLP(BgL_valuesz00_63))
				{	/* Rgc/rgcrules.scm 586 */
					return BGl_symbol2371z00zz__rgc_rulesz00;
				}
			else
				{
					obj_t BgL_valuesz00_1891;
					obj_t BgL_charsz00_1892;

					BgL_valuesz00_1891 = BgL_valuesz00_63;
					BgL_charsz00_1892 = BNIL;
				BgL_zc3z04anonymousza31763ze3z87_1893:
					if (NULLP(BgL_valuesz00_1891))
						{	/* Rgc/rgcrules.scm 591 */
							obj_t BgL_arg1765z00_1895;

							BgL_arg1765z00_1895 =
								BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
								(BgL_charsz00_1892, BNIL);
							return MAKE_YOUNG_PAIR(BGl_symbol2288z00zz__rgc_rulesz00,
								BgL_arg1765z00_1895);
						}
					else
						{	/* Rgc/rgcrules.scm 592 */
							obj_t BgL_valuez00_1896;

							BgL_valuez00_1896 = CAR(((obj_t) BgL_valuesz00_1891));
							{
								obj_t BgL_orzd2charszd2_1904;

								if (BGl_rgczd2charzf3ze70zc6zz__rgc_rulesz00(BgL_valuez00_1896))
									{	/* Rgc/rgcrules.scm 593 */
										{	/* Rgc/rgcrules.scm 595 */
											obj_t BgL_arg1811z00_1954;
											obj_t BgL_arg1812z00_1955;

											BgL_arg1811z00_1954 = CDR(((obj_t) BgL_valuesz00_1891));
											{	/* Rgc/rgcrules.scm 595 */
												obj_t BgL_arg1813z00_1956;

												if (CHARP(BgL_valuez00_1896))
													{	/* Rgc/rgcrules.scm 595 */
														BgL_arg1813z00_1956 =
															BINT((CCHAR(BgL_valuez00_1896)));
													}
												else
													{	/* Rgc/rgcrules.scm 595 */
														BgL_arg1813z00_1956 = BgL_valuez00_1896;
													}
												BgL_arg1812z00_1955 =
													MAKE_YOUNG_PAIR(BgL_arg1813z00_1956,
													BgL_charsz00_1892);
											}
											{
												obj_t BgL_charsz00_4588;
												obj_t BgL_valuesz00_4587;

												BgL_valuesz00_4587 = BgL_arg1811z00_1954;
												BgL_charsz00_4588 = BgL_arg1812z00_1955;
												BgL_charsz00_1892 = BgL_charsz00_4588;
												BgL_valuesz00_1891 = BgL_valuesz00_4587;
												goto BgL_zc3z04anonymousza31763ze3z87_1893;
											}
										}
									}
								else
									{	/* Rgc/rgcrules.scm 593 */
										if (STRINGP(BgL_valuez00_1896))
											{	/* Rgc/rgcrules.scm 593 */
												if ((STRING_LENGTH(((obj_t) BgL_valuez00_1896)) == 0L))
													{	/* Rgc/rgcrules.scm 600 */
														return BGl_symbol2371z00zz__rgc_rulesz00;
													}
												else
													{	/* Rgc/rgcrules.scm 602 */
														obj_t BgL_arg1817z00_1960;
														obj_t BgL_arg1818z00_1961;

														BgL_arg1817z00_1960 =
															CDR(((obj_t) BgL_valuesz00_1891));
														{	/* Rgc/rgcrules.scm 603 */
															obj_t BgL_arg1819z00_1962;

															{	/* Rgc/rgcrules.scm 603 */
																obj_t BgL_l1133z00_1963;

																BgL_l1133z00_1963 =
																	BGl_stringzd2ze3listz31zz__r4_strings_6_7z00
																	(BgL_valuez00_1896);
																if (NULLP(BgL_l1133z00_1963))
																	{	/* Rgc/rgcrules.scm 603 */
																		BgL_arg1819z00_1962 = BNIL;
																	}
																else
																	{	/* Rgc/rgcrules.scm 603 */
																		obj_t BgL_head1135z00_1965;

																		{	/* Rgc/rgcrules.scm 603 */
																			long BgL_arg1828z00_1977;

																			BgL_arg1828z00_1977 =
																				(CCHAR(CAR(BgL_l1133z00_1963)));
																			BgL_head1135z00_1965 =
																				MAKE_YOUNG_PAIR(BINT
																				(BgL_arg1828z00_1977), BNIL);
																		}
																		{	/* Rgc/rgcrules.scm 603 */
																			obj_t BgL_g1138z00_1966;

																			BgL_g1138z00_1966 =
																				CDR(BgL_l1133z00_1963);
																			{
																				obj_t BgL_l1133z00_3153;
																				obj_t BgL_tail1136z00_3154;

																				BgL_l1133z00_3153 = BgL_g1138z00_1966;
																				BgL_tail1136z00_3154 =
																					BgL_head1135z00_1965;
																			BgL_zc3z04anonymousza31821ze3z87_3152:
																				if (NULLP(BgL_l1133z00_3153))
																					{	/* Rgc/rgcrules.scm 603 */
																						BgL_arg1819z00_1962 =
																							BgL_head1135z00_1965;
																					}
																				else
																					{	/* Rgc/rgcrules.scm 603 */
																						obj_t BgL_newtail1137z00_3161;

																						{	/* Rgc/rgcrules.scm 603 */
																							long BgL_arg1826z00_3162;

																							BgL_arg1826z00_3162 =
																								(CCHAR(CAR(
																										((obj_t)
																											BgL_l1133z00_3153))));
																							BgL_newtail1137z00_3161 =
																								MAKE_YOUNG_PAIR(BINT
																								(BgL_arg1826z00_3162), BNIL);
																						}
																						SET_CDR(BgL_tail1136z00_3154,
																							BgL_newtail1137z00_3161);
																						{	/* Rgc/rgcrules.scm 603 */
																							obj_t BgL_arg1823z00_3164;

																							BgL_arg1823z00_3164 =
																								CDR(
																								((obj_t) BgL_l1133z00_3153));
																							{
																								obj_t BgL_tail1136z00_4618;
																								obj_t BgL_l1133z00_4617;

																								BgL_l1133z00_4617 =
																									BgL_arg1823z00_3164;
																								BgL_tail1136z00_4618 =
																									BgL_newtail1137z00_3161;
																								BgL_tail1136z00_3154 =
																									BgL_tail1136z00_4618;
																								BgL_l1133z00_3153 =
																									BgL_l1133z00_4617;
																								goto
																									BgL_zc3z04anonymousza31821ze3z87_3152;
																							}
																						}
																					}
																			}
																		}
																	}
															}
															BgL_arg1818z00_1961 =
																BGl_appendzd221011zd2zz__rgc_rulesz00
																(BgL_arg1819z00_1962, BgL_charsz00_1892);
														}
														{
															obj_t BgL_charsz00_4621;
															obj_t BgL_valuesz00_4620;

															BgL_valuesz00_4620 = BgL_arg1817z00_1960;
															BgL_charsz00_4621 = BgL_arg1818z00_1961;
															BgL_charsz00_1892 = BgL_charsz00_4621;
															BgL_valuesz00_1891 = BgL_valuesz00_4620;
															goto BgL_zc3z04anonymousza31763ze3z87_1893;
														}
													}
											}
										else
											{	/* Rgc/rgcrules.scm 593 */
												if (PAIRP(BgL_valuez00_1896))
													{	/* Rgc/rgcrules.scm 593 */
														obj_t BgL_carzd22548zd2_1911;
														obj_t BgL_cdrzd22549zd2_1912;

														BgL_carzd22548zd2_1911 =
															CAR(((obj_t) BgL_valuez00_1896));
														BgL_cdrzd22549zd2_1912 =
															CDR(((obj_t) BgL_valuez00_1896));
														if (BGl_rgczd2charzf3ze70zc6zz__rgc_rulesz00
															(BgL_carzd22548zd2_1911))
															{	/* Rgc/rgcrules.scm 593 */
																if (PAIRP(BgL_cdrzd22549zd2_1912))
																	{	/* Rgc/rgcrules.scm 593 */
																		obj_t BgL_carzd22554zd2_1915;

																		BgL_carzd22554zd2_1915 =
																			CAR(BgL_cdrzd22549zd2_1912);
																		if (BGl_rgczd2charzf3ze70zc6zz__rgc_rulesz00
																			(BgL_carzd22554zd2_1915))
																			{	/* Rgc/rgcrules.scm 593 */
																				if (NULLP(CDR(BgL_cdrzd22549zd2_1912)))
																					{	/* Rgc/rgcrules.scm 606 */
																						obj_t BgL_arg1832z00_3197;
																						obj_t BgL_arg1833z00_3198;

																						BgL_arg1832z00_3197 =
																							CDR(((obj_t) BgL_valuesz00_1891));
																						BgL_arg1833z00_3198 =
																							BGl_appendzd221011zd2zz__rgc_rulesz00
																							(BGl_charzd2rangeze70z35zz__rgc_rulesz00
																							(BgL_errz00_64,
																								BgL_carzd22548zd2_1911,
																								BgL_carzd22554zd2_1915),
																							BgL_charsz00_1892);
																						{
																							obj_t BgL_charsz00_4643;
																							obj_t BgL_valuesz00_4642;

																							BgL_valuesz00_4642 =
																								BgL_arg1832z00_3197;
																							BgL_charsz00_4643 =
																								BgL_arg1833z00_3198;
																							BgL_charsz00_1892 =
																								BgL_charsz00_4643;
																							BgL_valuesz00_1891 =
																								BgL_valuesz00_4642;
																							goto
																								BgL_zc3z04anonymousza31763ze3z87_1893;
																						}
																					}
																				else
																					{	/* Rgc/rgcrules.scm 593 */
																						if (
																							(CAR(
																									((obj_t) BgL_valuez00_1896))
																								==
																								BGl_symbol2288z00zz__rgc_rulesz00))
																							{	/* Rgc/rgcrules.scm 593 */
																								obj_t BgL_arg1777z00_1921;

																								BgL_arg1777z00_1921 =
																									CDR(
																									((obj_t) BgL_valuez00_1896));
																								BgL_orzd2charszd2_1904 =
																									BgL_arg1777z00_1921;
																							BgL_tagzd22531zd2_1905:
																								{
																									obj_t BgL_csz00_1988;
																									obj_t BgL_resz00_1989;

																									BgL_csz00_1988 =
																										BgL_orzd2charszd2_1904;
																									BgL_resz00_1989 = BNIL;
																								BgL_zc3z04anonymousza31838ze3z87_1990:
																									if (NULLP
																										(BgL_csz00_1988))
																										{	/* Rgc/rgcrules.scm 614 */
																											obj_t BgL_arg1840z00_1992;
																											obj_t BgL_arg1842z00_1993;

																											BgL_arg1840z00_1992 =
																												CDR(
																												((obj_t)
																													BgL_valuesz00_1891));
																											BgL_arg1842z00_1993 =
																												BGl_appendzd221011zd2zz__rgc_rulesz00
																												(BgL_resz00_1989,
																												BgL_charsz00_1892);
																											{
																												obj_t BgL_charsz00_4656;
																												obj_t
																													BgL_valuesz00_4655;
																												BgL_valuesz00_4655 =
																													BgL_arg1840z00_1992;
																												BgL_charsz00_4656 =
																													BgL_arg1842z00_1993;
																												BgL_charsz00_1892 =
																													BgL_charsz00_4656;
																												BgL_valuesz00_1891 =
																													BgL_valuesz00_4655;
																												goto
																													BgL_zc3z04anonymousza31763ze3z87_1893;
																											}
																										}
																									else
																										{	/* Rgc/rgcrules.scm 616 */
																											bool_t
																												BgL_test2630z00_4657;
																											{	/* Rgc/rgcrules.scm 616 */
																												obj_t
																													BgL_arg1859z00_2010;
																												BgL_arg1859z00_2010 =
																													CAR(((obj_t)
																														BgL_csz00_1988));
																												BgL_test2630z00_4657 =
																													BGl_rgczd2charzf3ze70zc6zz__rgc_rulesz00
																													(BgL_arg1859z00_2010);
																											}
																											if (BgL_test2630z00_4657)
																												{	/* Rgc/rgcrules.scm 617 */
																													obj_t
																														BgL_arg1845z00_1996;
																													obj_t
																														BgL_arg1846z00_1997;
																													BgL_arg1845z00_1996 =
																														CDR(((obj_t)
																															BgL_csz00_1988));
																													{	/* Rgc/rgcrules.scm 617 */
																														obj_t
																															BgL_arg1847z00_1998;
																														BgL_arg1847z00_1998
																															=
																															CAR(((obj_t)
																																BgL_csz00_1988));
																														BgL_arg1846z00_1997
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1847z00_1998,
																															BgL_resz00_1989);
																													}
																													{
																														obj_t
																															BgL_resz00_4667;
																														obj_t
																															BgL_csz00_4666;
																														BgL_csz00_4666 =
																															BgL_arg1845z00_1996;
																														BgL_resz00_4667 =
																															BgL_arg1846z00_1997;
																														BgL_resz00_1989 =
																															BgL_resz00_4667;
																														BgL_csz00_1988 =
																															BgL_csz00_4666;
																														goto
																															BgL_zc3z04anonymousza31838ze3z87_1990;
																													}
																												}
																											else
																												{	/* Rgc/rgcrules.scm 618 */
																													bool_t
																														BgL_test2631z00_4668;
																													{	/* Rgc/rgcrules.scm 618 */
																														bool_t
																															BgL_test2632z00_4669;
																														{	/* Rgc/rgcrules.scm 618 */
																															obj_t
																																BgL_tmpz00_4670;
																															BgL_tmpz00_4670 =
																																CAR(((obj_t)
																																	BgL_csz00_1988));
																															BgL_test2632z00_4669
																																=
																																PAIRP
																																(BgL_tmpz00_4670);
																														}
																														if (BgL_test2632z00_4669)
																															{	/* Rgc/rgcrules.scm 618 */
																																obj_t
																																	BgL_tmpz00_4674;
																																{	/* Rgc/rgcrules.scm 618 */
																																	obj_t
																																		BgL_pairz00_3185;
																																	BgL_pairz00_3185
																																		=
																																		CAR(((obj_t)
																																			BgL_csz00_1988));
																																	BgL_tmpz00_4674
																																		=
																																		CAR
																																		(BgL_pairz00_3185);
																																}
																																BgL_test2631z00_4668
																																	=
																																	(BgL_tmpz00_4674
																																	==
																																	BGl_symbol2288z00zz__rgc_rulesz00);
																															}
																														else
																															{	/* Rgc/rgcrules.scm 618 */
																																BgL_test2631z00_4668
																																	=
																																	((bool_t) 0);
																															}
																													}
																													if (BgL_test2631z00_4668)
																														{	/* Rgc/rgcrules.scm 619 */
																															obj_t
																																BgL_arg1853z00_2004;
																															{	/* Rgc/rgcrules.scm 619 */
																																obj_t
																																	BgL_arg1854z00_2005;
																																obj_t
																																	BgL_arg1856z00_2006;
																																{	/* Rgc/rgcrules.scm 619 */
																																	obj_t
																																		BgL_pairz00_3189;
																																	BgL_pairz00_3189
																																		=
																																		CAR(((obj_t)
																																			BgL_csz00_1988));
																																	BgL_arg1854z00_2005
																																		=
																																		CDR
																																		(BgL_pairz00_3189);
																																}
																																BgL_arg1856z00_2006
																																	=
																																	CDR(((obj_t)
																																		BgL_csz00_1988));
																																BgL_arg1853z00_2004
																																	=
																																	BGl_appendzd221011zd2zz__rgc_rulesz00
																																	(BgL_arg1854z00_2005,
																																	BgL_arg1856z00_2006);
																															}
																															{
																																obj_t
																																	BgL_csz00_4685;
																																BgL_csz00_4685 =
																																	BgL_arg1853z00_2004;
																																BgL_csz00_1988 =
																																	BgL_csz00_4685;
																																goto
																																	BgL_zc3z04anonymousza31838ze3z87_1990;
																															}
																														}
																													else
																														{	/* Rgc/rgcrules.scm 618 */
																															return
																																BGl_errorz00zz__errorz00
																																(BFALSE,
																																BGl_string2362z00zz__rgc_rulesz00,
																																BgL_errz00_64);
																														}
																												}
																										}
																								}
																							}
																						else
																							{	/* Rgc/rgcrules.scm 593 */
																							BgL_tagzd22532zd2_1906:
																								{	/* Rgc/rgcrules.scm 624 */
																									obj_t BgL_arg1860z00_2012;

																									{	/* Rgc/rgcrules.scm 624 */
																										obj_t BgL_arg1862z00_2013;
																										obj_t BgL_arg1863z00_2014;

																										{	/* Rgc/rgcrules.scm 624 */
																											obj_t BgL_arg1864z00_2015;

																											BgL_arg1864z00_2015 =
																												CAR(
																												((obj_t)
																													BgL_valuesz00_1891));
																											BgL_arg1862z00_2013 =
																												BGl_expandzd2rulezd2zz__rgc_rulesz00
																												(BgL_matchz00_61,
																												BgL_envz00_62,
																												BgL_arg1864z00_2015);
																										}
																										BgL_arg1863z00_2014 =
																											CDR(
																											((obj_t)
																												BgL_valuesz00_1891));
																										BgL_arg1860z00_2012 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1862z00_2013,
																											BgL_arg1863z00_2014);
																									}
																									{
																										obj_t BgL_valuesz00_4693;

																										BgL_valuesz00_4693 =
																											BgL_arg1860z00_2012;
																										BgL_valuesz00_1891 =
																											BgL_valuesz00_4693;
																										goto
																											BgL_zc3z04anonymousza31763ze3z87_1893;
																									}
																								}
																							}
																					}
																			}
																		else
																			{	/* Rgc/rgcrules.scm 593 */
																				if (
																					(CAR(
																							((obj_t) BgL_valuez00_1896)) ==
																						BGl_symbol2288z00zz__rgc_rulesz00))
																					{	/* Rgc/rgcrules.scm 593 */
																						obj_t BgL_arg1785z00_1926;

																						BgL_arg1785z00_1926 =
																							CDR(((obj_t) BgL_valuez00_1896));
																						{
																							obj_t BgL_orzd2charszd2_4700;

																							BgL_orzd2charszd2_4700 =
																								BgL_arg1785z00_1926;
																							BgL_orzd2charszd2_1904 =
																								BgL_orzd2charszd2_4700;
																							goto BgL_tagzd22531zd2_1905;
																						}
																					}
																				else
																					{	/* Rgc/rgcrules.scm 593 */
																						goto BgL_tagzd22532zd2_1906;
																					}
																			}
																	}
																else
																	{	/* Rgc/rgcrules.scm 593 */
																		obj_t BgL_carzd22581zd2_1928;

																		BgL_carzd22581zd2_1928 =
																			CAR(((obj_t) BgL_valuez00_1896));
																		if (STRINGP(BgL_carzd22581zd2_1928))
																			{	/* Rgc/rgcrules.scm 593 */
																				if (NULLP(CDR(
																							((obj_t) BgL_valuez00_1896))))
																					{	/* Rgc/rgcrules.scm 608 */
																						obj_t BgL_arg1835z00_3207;
																						obj_t BgL_arg1836z00_3208;

																						BgL_arg1835z00_3207 =
																							CDR(((obj_t) BgL_valuesz00_1891));
																						BgL_arg1836z00_3208 =
																							BGl_appendzd221011zd2zz__rgc_rulesz00
																							(BGl_stringzd2rangeze70z35zz__rgc_rulesz00
																							(BgL_errz00_64,
																								BgL_carzd22581zd2_1928),
																							BgL_charsz00_1892);
																						{
																							obj_t BgL_charsz00_4714;
																							obj_t BgL_valuesz00_4713;

																							BgL_valuesz00_4713 =
																								BgL_arg1835z00_3207;
																							BgL_charsz00_4714 =
																								BgL_arg1836z00_3208;
																							BgL_charsz00_1892 =
																								BgL_charsz00_4714;
																							BgL_valuesz00_1891 =
																								BgL_valuesz00_4713;
																							goto
																								BgL_zc3z04anonymousza31763ze3z87_1893;
																						}
																					}
																				else
																					{	/* Rgc/rgcrules.scm 593 */
																						if (
																							(BgL_carzd22581zd2_1928 ==
																								BGl_symbol2288z00zz__rgc_rulesz00))
																							{	/* Rgc/rgcrules.scm 593 */
																								obj_t BgL_arg1792z00_1934;

																								BgL_arg1792z00_1934 =
																									CDR(
																									((obj_t) BgL_valuez00_1896));
																								{
																									obj_t BgL_orzd2charszd2_4719;

																									BgL_orzd2charszd2_4719 =
																										BgL_arg1792z00_1934;
																									BgL_orzd2charszd2_1904 =
																										BgL_orzd2charszd2_4719;
																									goto BgL_tagzd22531zd2_1905;
																								}
																							}
																						else
																							{	/* Rgc/rgcrules.scm 593 */
																								goto BgL_tagzd22532zd2_1906;
																							}
																					}
																			}
																		else
																			{	/* Rgc/rgcrules.scm 593 */
																				if (
																					(BgL_carzd22581zd2_1928 ==
																						BGl_symbol2288z00zz__rgc_rulesz00))
																					{	/* Rgc/rgcrules.scm 593 */
																						obj_t BgL_arg1797z00_1939;

																						BgL_arg1797z00_1939 =
																							CDR(((obj_t) BgL_valuez00_1896));
																						{
																							obj_t BgL_orzd2charszd2_4724;

																							BgL_orzd2charszd2_4724 =
																								BgL_arg1797z00_1939;
																							BgL_orzd2charszd2_1904 =
																								BgL_orzd2charszd2_4724;
																							goto BgL_tagzd22531zd2_1905;
																						}
																					}
																				else
																					{	/* Rgc/rgcrules.scm 593 */
																						goto BgL_tagzd22532zd2_1906;
																					}
																			}
																	}
															}
														else
															{	/* Rgc/rgcrules.scm 593 */
																obj_t BgL_carzd22600zd2_1941;

																BgL_carzd22600zd2_1941 =
																	CAR(((obj_t) BgL_valuez00_1896));
																if (STRINGP(BgL_carzd22600zd2_1941))
																	{	/* Rgc/rgcrules.scm 593 */
																		if (NULLP(CDR(((obj_t) BgL_valuez00_1896))))
																			{	/* Rgc/rgcrules.scm 608 */
																				obj_t BgL_arg1835z00_3217;
																				obj_t BgL_arg1836z00_3218;

																				BgL_arg1835z00_3217 =
																					CDR(((obj_t) BgL_valuesz00_1891));
																				BgL_arg1836z00_3218 =
																					BGl_appendzd221011zd2zz__rgc_rulesz00
																					(BGl_stringzd2rangeze70z35zz__rgc_rulesz00
																					(BgL_errz00_64,
																						BgL_carzd22600zd2_1941),
																					BgL_charsz00_1892);
																				{
																					obj_t BgL_charsz00_4738;
																					obj_t BgL_valuesz00_4737;

																					BgL_valuesz00_4737 =
																						BgL_arg1835z00_3217;
																					BgL_charsz00_4738 =
																						BgL_arg1836z00_3218;
																					BgL_charsz00_1892 = BgL_charsz00_4738;
																					BgL_valuesz00_1891 =
																						BgL_valuesz00_4737;
																					goto
																						BgL_zc3z04anonymousza31763ze3z87_1893;
																				}
																			}
																		else
																			{	/* Rgc/rgcrules.scm 593 */
																				if (
																					(BgL_carzd22600zd2_1941 ==
																						BGl_symbol2288z00zz__rgc_rulesz00))
																					{	/* Rgc/rgcrules.scm 593 */
																						obj_t BgL_arg1804z00_1947;

																						BgL_arg1804z00_1947 =
																							CDR(((obj_t) BgL_valuez00_1896));
																						{
																							obj_t BgL_orzd2charszd2_4743;

																							BgL_orzd2charszd2_4743 =
																								BgL_arg1804z00_1947;
																							BgL_orzd2charszd2_1904 =
																								BgL_orzd2charszd2_4743;
																							goto BgL_tagzd22531zd2_1905;
																						}
																					}
																				else
																					{	/* Rgc/rgcrules.scm 593 */
																						goto BgL_tagzd22532zd2_1906;
																					}
																			}
																	}
																else
																	{	/* Rgc/rgcrules.scm 593 */
																		if (
																			(BgL_carzd22600zd2_1941 ==
																				BGl_symbol2288z00zz__rgc_rulesz00))
																			{	/* Rgc/rgcrules.scm 593 */
																				obj_t BgL_arg1809z00_1952;

																				BgL_arg1809z00_1952 =
																					CDR(((obj_t) BgL_valuez00_1896));
																				{
																					obj_t BgL_orzd2charszd2_4748;

																					BgL_orzd2charszd2_4748 =
																						BgL_arg1809z00_1952;
																					BgL_orzd2charszd2_1904 =
																						BgL_orzd2charszd2_4748;
																					goto BgL_tagzd22531zd2_1905;
																				}
																			}
																		else
																			{	/* Rgc/rgcrules.scm 593 */
																				goto BgL_tagzd22532zd2_1906;
																			}
																	}
															}
													}
												else
													{	/* Rgc/rgcrules.scm 593 */
														goto BgL_tagzd22532zd2_1906;
													}
											}
									}
							}
						}
				}
		}

	}



/* string-range~0 */
	obj_t BGl_stringzd2rangeze70z35zz__rgc_rulesz00(obj_t BgL_errz00_3567,
		obj_t BgL_stringz00_2040)
	{
		{	/* Rgc/rgcrules.scm 585 */
			{	/* Rgc/rgcrules.scm 575 */
				long BgL_lenz00_2042;

				BgL_lenz00_2042 = STRING_LENGTH(BgL_stringz00_2040);
				{	/* Rgc/rgcrules.scm 576 */
					bool_t BgL_test2642z00_4750;

					{	/* Rgc/rgcrules.scm 576 */
						long BgL_a1131z00_2060;

						{	/* Rgc/rgcrules.scm 576 */
							long BgL_n1z00_3084;
							long BgL_n2z00_3085;

							BgL_n1z00_3084 = BgL_lenz00_2042;
							BgL_n2z00_3085 = 2L;
							{	/* Rgc/rgcrules.scm 576 */
								bool_t BgL_test2643z00_4751;

								{	/* Rgc/rgcrules.scm 576 */
									long BgL_arg2120z00_3087;

									BgL_arg2120z00_3087 =
										(((BgL_n1z00_3084) | (BgL_n2z00_3085)) & -2147483648);
									BgL_test2643z00_4751 = (BgL_arg2120z00_3087 == 0L);
								}
								if (BgL_test2643z00_4751)
									{	/* Rgc/rgcrules.scm 576 */
										int32_t BgL_arg2117z00_3088;

										{	/* Rgc/rgcrules.scm 576 */
											int32_t BgL_arg2118z00_3089;
											int32_t BgL_arg2119z00_3090;

											BgL_arg2118z00_3089 = (int32_t) (BgL_n1z00_3084);
											BgL_arg2119z00_3090 = (int32_t) (BgL_n2z00_3085);
											BgL_arg2117z00_3088 =
												(BgL_arg2118z00_3089 % BgL_arg2119z00_3090);
										}
										{	/* Rgc/rgcrules.scm 576 */
											long BgL_arg2223z00_3095;

											BgL_arg2223z00_3095 = (long) (BgL_arg2117z00_3088);
											BgL_a1131z00_2060 = (long) (BgL_arg2223z00_3095);
									}}
								else
									{	/* Rgc/rgcrules.scm 576 */
										BgL_a1131z00_2060 = (BgL_n1z00_3084 % BgL_n2z00_3085);
									}
							}
						}
						{	/* Rgc/rgcrules.scm 576 */

							BgL_test2642z00_4750 = (BgL_a1131z00_2060 > 0L);
						}
					}
					if (BgL_test2642z00_4750)
						{	/* Rgc/rgcrules.scm 576 */
							return
								BGl_errorz00zz__errorz00(BFALSE,
								BGl_string2400z00zz__rgc_rulesz00, BgL_errz00_3567);
						}
					else
						{
							long BgL_iz00_3108;
							obj_t BgL_resz00_3109;

							BgL_iz00_3108 = 0L;
							BgL_resz00_3109 = BNIL;
						BgL_loopz00_3107:
							if ((BgL_iz00_3108 == BgL_lenz00_2042))
								{	/* Rgc/rgcrules.scm 580 */
									return BgL_resz00_3109;
								}
							else
								{	/* Rgc/rgcrules.scm 582 */
									long BgL_arg1882z00_3113;
									obj_t BgL_arg1883z00_3114;

									BgL_arg1882z00_3113 = (BgL_iz00_3108 + 2L);
									{	/* Rgc/rgcrules.scm 583 */
										obj_t BgL_arg1884z00_3116;

										{	/* Rgc/rgcrules.scm 583 */
											unsigned char BgL_arg1885z00_3117;
											unsigned char BgL_arg1887z00_3118;

											BgL_arg1885z00_3117 =
												STRING_REF(BgL_stringz00_2040, BgL_iz00_3108);
											BgL_arg1887z00_3118 =
												STRING_REF(BgL_stringz00_2040, (BgL_iz00_3108 + 1L));
											BgL_arg1884z00_3116 =
												BGl_charzd2rangeze70z35zz__rgc_rulesz00(BgL_errz00_3567,
												BCHAR(BgL_arg1885z00_3117), BCHAR(BgL_arg1887z00_3118));
										}
										BgL_arg1883z00_3114 =
											BGl_appendzd221011zd2zz__rgc_rulesz00(BgL_arg1884z00_3116,
											BgL_resz00_3109);
									}
									{
										obj_t BgL_resz00_4773;
										long BgL_iz00_4772;

										BgL_iz00_4772 = BgL_arg1882z00_3113;
										BgL_resz00_4773 = BgL_arg1883z00_3114;
										BgL_resz00_3109 = BgL_resz00_4773;
										BgL_iz00_3108 = BgL_iz00_4772;
										goto BgL_loopz00_3107;
									}
								}
						}
				}
			}
		}

	}



/* rgc-char?~0 */
	bool_t BGl_rgczd2charzf3ze70zc6zz__rgc_rulesz00(obj_t BgL_xz00_2017)
	{
		{	/* Rgc/rgcrules.scm 561 */
			{	/* Rgc/rgcrules.scm 558 */
				bool_t BgL__ortest_1046z00_2019;

				BgL__ortest_1046z00_2019 = CHARP(BgL_xz00_2017);
				if (BgL__ortest_1046z00_2019)
					{	/* Rgc/rgcrules.scm 558 */
						return BgL__ortest_1046z00_2019;
					}
				else
					{	/* Rgc/rgcrules.scm 558 */
						if (INTEGERP(BgL_xz00_2017))
							{	/* Rgc/rgcrules.scm 559 */
								if (((long) CINT(BgL_xz00_2017) >= 0L))
									{	/* Rgc/rgcrules.scm 561 */
										obj_t BgL_arg1866z00_2022;

										BgL_arg1866z00_2022 =
											BGl_rgczd2maxzd2charz00zz__rgc_configz00();
										return
											(
											(long) CINT(BgL_xz00_2017) <
											(long) CINT(BgL_arg1866z00_2022));
									}
								else
									{	/* Rgc/rgcrules.scm 560 */
										return ((bool_t) 0);
									}
							}
						else
							{	/* Rgc/rgcrules.scm 559 */
								return ((bool_t) 0);
							}
					}
			}
		}

	}



/* char-range~0 */
	obj_t BGl_charzd2rangeze70z35zz__rgc_rulesz00(obj_t BgL_errz00_3568,
		obj_t BgL_minz00_2023, obj_t BgL_maxz00_2024)
	{
		{	/* Rgc/rgcrules.scm 573 */
			{	/* Rgc/rgcrules.scm 563 */
				obj_t BgL_minz00_2026;
				obj_t BgL_maxz00_2027;

				if (CHARP(BgL_minz00_2023))
					{	/* Rgc/rgcrules.scm 563 */
						BgL_minz00_2026 = BINT((CCHAR(BgL_minz00_2023)));
					}
				else
					{	/* Rgc/rgcrules.scm 563 */
						BgL_minz00_2026 = BgL_minz00_2023;
					}
				if (CHARP(BgL_maxz00_2024))
					{	/* Rgc/rgcrules.scm 564 */
						BgL_maxz00_2027 = BINT((CCHAR(BgL_maxz00_2024)));
					}
				else
					{	/* Rgc/rgcrules.scm 564 */
						BgL_maxz00_2027 = BgL_maxz00_2024;
					}
				if (((long) CINT(BgL_maxz00_2027) >= (long) CINT(BgL_minz00_2026)))
					{
						obj_t BgL_maxz00_3072;
						obj_t BgL_resz00_3073;

						BgL_maxz00_3072 = BgL_maxz00_2027;
						BgL_resz00_3073 = BNIL;
					BgL_loopz00_3071:
						if (((long) CINT(BgL_maxz00_3072) == (long) CINT(BgL_minz00_2026)))
							{	/* Rgc/rgcrules.scm 569 */
								return MAKE_YOUNG_PAIR(BgL_minz00_2026, BgL_resz00_3073);
							}
						else
							{	/* Rgc/rgcrules.scm 571 */
								long BgL_arg1872z00_3078;
								obj_t BgL_arg1873z00_3079;

								BgL_arg1872z00_3078 = ((long) CINT(BgL_maxz00_3072) - 1L);
								BgL_arg1873z00_3079 =
									MAKE_YOUNG_PAIR(BgL_maxz00_3072, BgL_resz00_3073);
								{
									obj_t BgL_resz00_4809;
									obj_t BgL_maxz00_4807;

									BgL_maxz00_4807 = BINT(BgL_arg1872z00_3078);
									BgL_resz00_4809 = BgL_arg1873z00_3079;
									BgL_resz00_3073 = BgL_resz00_4809;
									BgL_maxz00_3072 = BgL_maxz00_4807;
									goto BgL_loopz00_3071;
								}
							}
					}
				else
					{	/* Rgc/rgcrules.scm 566 */
						return
							BGl_errorz00zz__errorz00(BFALSE,
							BGl_string2401z00zz__rgc_rulesz00, BgL_errz00_3568);
					}
			}
		}

	}



/* expand-out */
	obj_t BGl_expandzd2outzd2zz__rgc_rulesz00(long BgL_matchz00_65,
		obj_t BgL_envz00_66, obj_t BgL_valuesz00_67, obj_t BgL_errz00_68)
	{
		{	/* Rgc/rgcrules.scm 635 */
			{	/* Rgc/rgcrules.scm 636 */
				obj_t BgL_inz00_3225;

				BgL_inz00_3225 =
					BGl_expandzd2inzd2zz__rgc_rulesz00(BgL_matchz00_65, BgL_envz00_66,
					BgL_valuesz00_67, BgL_errz00_68);
				{	/* Rgc/rgcrules.scm 636 */
					obj_t BgL_bcz00_3226;

					{	/* Rgc/rgcrules.scm 637 */
						obj_t BgL_arg1891z00_3227;
						obj_t BgL_arg1892z00_3228;

						BgL_arg1891z00_3227 = CDR(((obj_t) BgL_inz00_3225));
						BgL_arg1892z00_3228 = BGl_rgczd2maxzd2charz00zz__rgc_configz00();
						BgL_bcz00_3226 =
							BGl_listzd2ze3rgcsetz31zz__rgc_setz00(BgL_arg1891z00_3227,
							(long) CINT(BgL_arg1892z00_3228));
					}
					{	/* Rgc/rgcrules.scm 637 */

						BGl_rgcsetzd2notz12zc0zz__rgc_setz00(BgL_bcz00_3226);
						{	/* Rgc/rgcrules.scm 639 */
							obj_t BgL_arg1889z00_3229;

							BgL_arg1889z00_3229 =
								BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
								(BGl_rgcsetzd2ze3listz31zz__rgc_setz00(BgL_bcz00_3226), BNIL);
							return MAKE_YOUNG_PAIR(BGl_symbol2288z00zz__rgc_rulesz00,
								BgL_arg1889z00_3229);
						}
					}
				}
			}
		}

	}



/* expand-and */
	obj_t BGl_expandzd2andzd2zz__rgc_rulesz00(long BgL_matchz00_69,
		obj_t BgL_envz00_70, obj_t BgL_val1z00_71, obj_t BgL_val2z00_72,
		obj_t BgL_errz00_73)
	{
		{	/* Rgc/rgcrules.scm 646 */
			{	/* Rgc/rgcrules.scm 647 */
				obj_t BgL_in1z00_3232;

				{	/* Rgc/rgcrules.scm 647 */
					obj_t BgL_arg1903z00_3233;

					{	/* Rgc/rgcrules.scm 647 */
						obj_t BgL_list1904z00_3234;

						BgL_list1904z00_3234 = MAKE_YOUNG_PAIR(BgL_val1z00_71, BNIL);
						BgL_arg1903z00_3233 = BgL_list1904z00_3234;
					}
					BgL_in1z00_3232 =
						BGl_expandzd2inzd2zz__rgc_rulesz00(BgL_matchz00_69, BgL_envz00_70,
						BgL_arg1903z00_3233, BgL_errz00_73);
				}
				{	/* Rgc/rgcrules.scm 647 */
					obj_t BgL_in2z00_3235;

					{	/* Rgc/rgcrules.scm 648 */
						obj_t BgL_arg1901z00_3236;

						{	/* Rgc/rgcrules.scm 648 */
							obj_t BgL_list1902z00_3237;

							BgL_list1902z00_3237 = MAKE_YOUNG_PAIR(BgL_val2z00_72, BNIL);
							BgL_arg1901z00_3236 = BgL_list1902z00_3237;
						}
						BgL_in2z00_3235 =
							BGl_expandzd2inzd2zz__rgc_rulesz00(BgL_matchz00_69, BgL_envz00_70,
							BgL_arg1901z00_3236, BgL_errz00_73);
					}
					{	/* Rgc/rgcrules.scm 648 */
						obj_t BgL_bc1z00_3238;

						{	/* Rgc/rgcrules.scm 649 */
							obj_t BgL_arg1898z00_3239;
							obj_t BgL_arg1899z00_3240;

							BgL_arg1898z00_3239 = CDR(((obj_t) BgL_in1z00_3232));
							BgL_arg1899z00_3240 = BGl_rgczd2maxzd2charz00zz__rgc_configz00();
							BgL_bc1z00_3238 =
								BGl_listzd2ze3rgcsetz31zz__rgc_setz00(BgL_arg1898z00_3239,
								(long) CINT(BgL_arg1899z00_3240));
						}
						{	/* Rgc/rgcrules.scm 649 */
							obj_t BgL_bc2z00_3241;

							{	/* Rgc/rgcrules.scm 650 */
								obj_t BgL_arg1896z00_3242;
								obj_t BgL_arg1897z00_3243;

								BgL_arg1896z00_3242 = CDR(((obj_t) BgL_in2z00_3235));
								BgL_arg1897z00_3243 =
									BGl_rgczd2maxzd2charz00zz__rgc_configz00();
								BgL_bc2z00_3241 =
									BGl_listzd2ze3rgcsetz31zz__rgc_setz00(BgL_arg1896z00_3242,
									(long) CINT(BgL_arg1897z00_3243));
							}
							{	/* Rgc/rgcrules.scm 650 */

								BGl_rgcsetzd2andz12zc0zz__rgc_setz00(BgL_bc1z00_3238,
									BgL_bc2z00_3241);
								{	/* Rgc/rgcrules.scm 652 */
									obj_t BgL_arg1893z00_3244;

									BgL_arg1893z00_3244 =
										BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
										(BGl_rgcsetzd2ze3listz31zz__rgc_setz00(BgL_bc1z00_3238),
										BNIL);
									return MAKE_YOUNG_PAIR(BGl_symbol2288z00zz__rgc_rulesz00,
										BgL_arg1893z00_3244);
								}
							}
						}
					}
				}
			}
		}

	}



/* expand-but */
	obj_t BGl_expandzd2butzd2zz__rgc_rulesz00(long BgL_matchz00_74,
		obj_t BgL_envz00_75, obj_t BgL_val1z00_76, obj_t BgL_val2z00_77,
		obj_t BgL_errz00_78)
	{
		{	/* Rgc/rgcrules.scm 659 */
			{	/* Rgc/rgcrules.scm 660 */
				obj_t BgL_in1z00_3250;

				{	/* Rgc/rgcrules.scm 660 */
					obj_t BgL_arg1918z00_3251;

					{	/* Rgc/rgcrules.scm 660 */
						obj_t BgL_list1919z00_3252;

						BgL_list1919z00_3252 = MAKE_YOUNG_PAIR(BgL_val1z00_76, BNIL);
						BgL_arg1918z00_3251 = BgL_list1919z00_3252;
					}
					BgL_in1z00_3250 =
						BGl_expandzd2inzd2zz__rgc_rulesz00(BgL_matchz00_74, BgL_envz00_75,
						BgL_arg1918z00_3251, BgL_errz00_78);
				}
				{	/* Rgc/rgcrules.scm 660 */
					obj_t BgL_in2z00_3253;

					{	/* Rgc/rgcrules.scm 661 */
						obj_t BgL_arg1916z00_3254;

						{	/* Rgc/rgcrules.scm 661 */
							obj_t BgL_list1917z00_3255;

							BgL_list1917z00_3255 = MAKE_YOUNG_PAIR(BgL_val2z00_77, BNIL);
							BgL_arg1916z00_3254 = BgL_list1917z00_3255;
						}
						BgL_in2z00_3253 =
							BGl_expandzd2inzd2zz__rgc_rulesz00(BgL_matchz00_74, BgL_envz00_75,
							BgL_arg1916z00_3254, BgL_errz00_78);
					}
					{	/* Rgc/rgcrules.scm 661 */
						obj_t BgL_bc1z00_3256;

						{	/* Rgc/rgcrules.scm 662 */
							obj_t BgL_arg1913z00_3257;
							obj_t BgL_arg1914z00_3258;

							BgL_arg1913z00_3257 = CDR(((obj_t) BgL_in1z00_3250));
							BgL_arg1914z00_3258 = BGl_rgczd2maxzd2charz00zz__rgc_configz00();
							BgL_bc1z00_3256 =
								BGl_listzd2ze3rgcsetz31zz__rgc_setz00(BgL_arg1913z00_3257,
								(long) CINT(BgL_arg1914z00_3258));
						}
						{	/* Rgc/rgcrules.scm 662 */
							obj_t BgL_bc2z00_3259;

							{	/* Rgc/rgcrules.scm 663 */
								obj_t BgL_arg1911z00_3260;
								obj_t BgL_arg1912z00_3261;

								BgL_arg1911z00_3260 = CDR(((obj_t) BgL_in2z00_3253));
								BgL_arg1912z00_3261 =
									BGl_rgczd2maxzd2charz00zz__rgc_configz00();
								BgL_bc2z00_3259 =
									BGl_listzd2ze3rgcsetz31zz__rgc_setz00(BgL_arg1911z00_3260,
									(long) CINT(BgL_arg1912z00_3261));
							}
							{	/* Rgc/rgcrules.scm 663 */

								BGl_rgcsetzd2butz12zc0zz__rgc_setz00(BgL_bc1z00_3256,
									BgL_bc2z00_3259);
								{	/* Rgc/rgcrules.scm 665 */
									obj_t BgL_arg1906z00_3262;

									BgL_arg1906z00_3262 =
										BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
										(BGl_rgcsetzd2ze3listz31zz__rgc_setz00(BgL_bc1z00_3256),
										BNIL);
									return MAKE_YOUNG_PAIR(BGl_symbol2288z00zz__rgc_rulesz00,
										BgL_arg1906z00_3262);
								}
							}
						}
					}
				}
			}
		}

	}



/* expand-submatch */
	obj_t BGl_expandzd2submatchzd2zz__rgc_rulesz00(long BgL_matchz00_79,
		obj_t BgL_envz00_80, obj_t BgL_rulez00_81, obj_t BgL_errz00_82)
	{
		{	/* Rgc/rgcrules.scm 675 */
			if (BGl_za2lockzd2submatchza2zd2zz__rgc_rulesz00)
				{	/* Rgc/rgcrules.scm 676 */
					return
						BGl_errorz00zz__errorz00(BFALSE, BGl_string2402z00zz__rgc_rulesz00,
						BgL_errz00_82);
				}
			else
				{	/* Rgc/rgcrules.scm 678 */
					long BgL_submatchz00_3268;

					BgL_submatchz00_3268 = BGl_getzd2newzd2submatchz00zz__rgc_rulesz00();
					BGl_za2lockzd2submatchza2zd2zz__rgc_rulesz00 = ((bool_t) 1);
					{	/* Rgc/rgcrules.scm 680 */
						obj_t BgL_resz00_3269;

						{	/* Rgc/rgcrules.scm 682 */
							obj_t BgL_arg1920z00_3270;

							{	/* Rgc/rgcrules.scm 682 */
								obj_t BgL_arg1923z00_3271;

								{	/* Rgc/rgcrules.scm 682 */
									obj_t BgL_arg1924z00_3272;

									BgL_arg1924z00_3272 =
										MAKE_YOUNG_PAIR(BGl_expandzd2rulezd2zz__rgc_rulesz00
										(BgL_matchz00_79, BgL_envz00_80, BgL_rulez00_81), BNIL);
									BgL_arg1923z00_3271 =
										MAKE_YOUNG_PAIR(BINT(BgL_submatchz00_3268),
										BgL_arg1924z00_3272);
								}
								BgL_arg1920z00_3270 =
									MAKE_YOUNG_PAIR(BINT(BgL_matchz00_79), BgL_arg1923z00_3271);
							}
							BgL_resz00_3269 =
								MAKE_YOUNG_PAIR(BGl_symbol2385z00zz__rgc_rulesz00,
								BgL_arg1920z00_3270);
						}
						BGl_za2lockzd2submatchza2zd2zz__rgc_rulesz00 = ((bool_t) 0);
						return BgL_resz00_3269;
					}
				}
		}

	}



/* expand-sequence */
	obj_t BGl_expandzd2sequencezd2zz__rgc_rulesz00(long BgL_matchz00_87,
		obj_t BgL_envz00_88, obj_t BgL_rulesz00_89)
	{
		{	/* Rgc/rgcrules.scm 698 */
			{	/* Rgc/rgcrules.scm 699 */
				obj_t BgL_arg1928z00_2108;

				{	/* Rgc/rgcrules.scm 699 */
					obj_t BgL_head1141z00_2111;

					BgL_head1141z00_2111 = MAKE_YOUNG_PAIR(BNIL, BNIL);
					{
						obj_t BgL_l1139z00_3293;
						obj_t BgL_tail1142z00_3294;

						BgL_l1139z00_3293 = BgL_rulesz00_89;
						BgL_tail1142z00_3294 = BgL_head1141z00_2111;
					BgL_zc3z04anonymousza31930ze3z87_3292:
						if (NULLP(BgL_l1139z00_3293))
							{	/* Rgc/rgcrules.scm 699 */
								BgL_arg1928z00_2108 = CDR(BgL_head1141z00_2111);
							}
						else
							{	/* Rgc/rgcrules.scm 699 */
								obj_t BgL_newtail1143z00_3301;

								{	/* Rgc/rgcrules.scm 699 */
									obj_t BgL_arg1933z00_3302;

									{	/* Rgc/rgcrules.scm 699 */
										obj_t BgL_rz00_3303;

										BgL_rz00_3303 = CAR(((obj_t) BgL_l1139z00_3293));
										BgL_arg1933z00_3302 =
											BGl_expandzd2rulezd2zz__rgc_rulesz00(BgL_matchz00_87,
											BgL_envz00_88, BgL_rz00_3303);
									}
									BgL_newtail1143z00_3301 =
										MAKE_YOUNG_PAIR(BgL_arg1933z00_3302, BNIL);
								}
								SET_CDR(BgL_tail1142z00_3294, BgL_newtail1143z00_3301);
								{	/* Rgc/rgcrules.scm 699 */
									obj_t BgL_arg1932z00_3304;

									BgL_arg1932z00_3304 = CDR(((obj_t) BgL_l1139z00_3293));
									{
										obj_t BgL_tail1142z00_4879;
										obj_t BgL_l1139z00_4878;

										BgL_l1139z00_4878 = BgL_arg1932z00_3304;
										BgL_tail1142z00_4879 = BgL_newtail1143z00_3301;
										BgL_tail1142z00_3294 = BgL_tail1142z00_4879;
										BgL_l1139z00_3293 = BgL_l1139z00_4878;
										goto BgL_zc3z04anonymousza31930ze3z87_3292;
									}
								}
							}
					}
				}
				return BGl_makezd2sequencezd2zz__rgc_rulesz00(BgL_arg1928z00_2108);
			}
		}

	}



/* make-sequence */
	obj_t BGl_makezd2sequencezd2zz__rgc_rulesz00(obj_t BgL_rulesz00_90)
	{
		{	/* Rgc/rgcrules.scm 713 */
			{
				obj_t BgL_rulesz00_2124;
				obj_t BgL_resz00_2125;

				BgL_rulesz00_2124 = BgL_rulesz00_90;
				BgL_resz00_2125 = BNIL;
			BgL_zc3z04anonymousza31934ze3z87_2126:
				{

					if (NULLP(BgL_rulesz00_2124))
						{	/* Rgc/rgcrules.scm 714 */
							{	/* Rgc/rgcrules.scm 718 */
								obj_t BgL_arg1943z00_2142;

								BgL_arg1943z00_2142 =
									BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
									(bgl_reverse_bang(BgL_resz00_2125), BNIL);
								return MAKE_YOUNG_PAIR(BGl_symbol2397z00zz__rgc_rulesz00,
									BgL_arg1943z00_2142);
							}
						}
					else
						{	/* Rgc/rgcrules.scm 714 */
							if (PAIRP(BgL_rulesz00_2124))
								{	/* Rgc/rgcrules.scm 714 */
									obj_t BgL_carzd22629zd2_2135;

									BgL_carzd22629zd2_2135 = CAR(((obj_t) BgL_rulesz00_2124));
									if (PAIRP(BgL_carzd22629zd2_2135))
										{	/* Rgc/rgcrules.scm 714 */
											if (
												(CAR(BgL_carzd22629zd2_2135) ==
													BGl_symbol2397z00zz__rgc_rulesz00))
												{	/* Rgc/rgcrules.scm 714 */
													obj_t BgL_arg1940z00_2139;
													obj_t BgL_arg1941z00_2140;

													BgL_arg1940z00_2139 = CDR(BgL_carzd22629zd2_2135);
													BgL_arg1941z00_2140 =
														CDR(((obj_t) BgL_rulesz00_2124));
													{
														obj_t BgL_resz00_4899;
														obj_t BgL_rulesz00_4898;

														BgL_rulesz00_4898 = BgL_arg1941z00_2140;
														BgL_resz00_4899 =
															BGl_appendzd221011zd2zz__rgc_rulesz00(bgl_reverse
															(BgL_arg1940z00_2139), BgL_resz00_2125);
														BgL_resz00_2125 = BgL_resz00_4899;
														BgL_rulesz00_2124 = BgL_rulesz00_4898;
														goto BgL_zc3z04anonymousza31934ze3z87_2126;
													}
												}
											else
												{	/* Rgc/rgcrules.scm 714 */
												BgL_tagzd22621zd2_2131:
													{	/* Rgc/rgcrules.scm 722 */
														obj_t BgL_arg1947z00_2146;
														obj_t BgL_arg1948z00_2147;

														BgL_arg1947z00_2146 =
															CDR(((obj_t) BgL_rulesz00_2124));
														{	/* Rgc/rgcrules.scm 722 */
															obj_t BgL_arg1949z00_2148;

															BgL_arg1949z00_2148 =
																CAR(((obj_t) BgL_rulesz00_2124));
															BgL_arg1948z00_2147 =
																MAKE_YOUNG_PAIR(BgL_arg1949z00_2148,
																BgL_resz00_2125);
														}
														{
															obj_t BgL_resz00_4908;
															obj_t BgL_rulesz00_4907;

															BgL_rulesz00_4907 = BgL_arg1947z00_2146;
															BgL_resz00_4908 = BgL_arg1948z00_2147;
															BgL_resz00_2125 = BgL_resz00_4908;
															BgL_rulesz00_2124 = BgL_rulesz00_4907;
															goto BgL_zc3z04anonymousza31934ze3z87_2126;
														}
													}
												}
										}
									else
										{	/* Rgc/rgcrules.scm 714 */
											goto BgL_tagzd22621zd2_2131;
										}
								}
							else
								{	/* Rgc/rgcrules.scm 714 */
									goto BgL_tagzd22621zd2_2131;
								}
						}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__rgc_rulesz00(void)
	{
		{	/* Rgc/rgcrules.scm 24 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__rgc_rulesz00(void)
	{
		{	/* Rgc/rgcrules.scm 24 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__rgc_rulesz00(void)
	{
		{	/* Rgc/rgcrules.scm 24 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__rgc_rulesz00(void)
	{
		{	/* Rgc/rgcrules.scm 24 */
			BGl_modulezd2initializa7ationz75zz__rgc_configz00(428274736L,
				BSTRING_TO_STRING(BGl_string2403z00zz__rgc_rulesz00));
			BGl_modulezd2initializa7ationz75zz__rgc_setz00(225075643L,
				BSTRING_TO_STRING(BGl_string2403z00zz__rgc_rulesz00));
			BGl_modulezd2initializa7ationz75zz__rgc_posixz00(447103600L,
				BSTRING_TO_STRING(BGl_string2403z00zz__rgc_rulesz00));
			return
				BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string2403z00zz__rgc_rulesz00));
		}

	}

#ifdef __cplusplus
}
#endif
