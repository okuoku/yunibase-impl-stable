/*===========================================================================*/
/*   (Rgc/rgcposix.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Rgc/rgcposix.scm -indent -o objs/obj_u/Rgc/rgcposix.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___RGC_POSIX_TYPE_DEFINITIONS
#define BGL___RGC_POSIX_TYPE_DEFINITIONS
#endif													// BGL___RGC_POSIX_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_za2errzd2stringza2zd2zz__rgc_posixz00 = BUNSPEC;
	extern obj_t BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_makezd2rgczd2sequencez00zz__rgc_posixz00(obj_t);
	static obj_t BGl_parsezd2posixzd2expz00zz__rgc_posixz00(obj_t, long);
	extern obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_list1818z00zz__rgc_posixz00 = BUNSPEC;
	static obj_t BGl_requirezd2initializa7ationz75zz__rgc_posixz00 = BUNSPEC;
	static obj_t BGl_parsezd2posixzd2branchz00zz__rgc_posixz00(obj_t, long);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__rgc_posixz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_symbol1795z00zz__rgc_posixz00 = BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zz__rgc_posixz00(void);
	static obj_t BGl_symbol1797z00zz__rgc_posixz00 = BUNSPEC;
	static obj_t BGl_symbol1799z00zz__rgc_posixz00 = BUNSPEC;
	static obj_t BGl_cnstzd2initzd2zz__rgc_posixz00(void);
	extern obj_t bgl_reverse(obj_t);
	static obj_t BGl_genericzd2initzd2zz__rgc_posixz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zz__rgc_posixz00(void);
	static obj_t BGl_parsezd2posixzd2piecez00zz__rgc_posixz00(obj_t, obj_t);
	static obj_t BGl_gczd2rootszd2initz00zz__rgc_posixz00(void);
	static obj_t BGl_makezd2rgczd2repeatz00zz__rgc_posixz00(obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zz__rgc_posixz00(void);
	extern obj_t BGl_2zb2zb2zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_listzd2ze3stringz31zz__r4_strings_6_7z00(obj_t);
	extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_parsezd2posixzd2atomz00zz__rgc_posixz00(obj_t, obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	extern obj_t c_substring(obj_t, long, long);
	static obj_t BGl_methodzd2initzd2zz__rgc_posixz00(void);
	static obj_t BGl_makezd2rgczd2orz00zz__rgc_posixz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_posixzd2ze3rgcz31zz__rgc_posixz00(obj_t);
	static obj_t BGl_symbol1801z00zz__rgc_posixz00 = BUNSPEC;
	static obj_t BGl_symbol1803z00zz__rgc_posixz00 = BUNSPEC;
	static obj_t BGl_parsezd2posixzd2bracketz00zz__rgc_posixz00(obj_t, long);
	static obj_t BGl_symbol1806z00zz__rgc_posixz00 = BUNSPEC;
	static obj_t BGl_symbol1808z00zz__rgc_posixz00 = BUNSPEC;
	static obj_t BGl_symbol1812z00zz__rgc_posixz00 = BUNSPEC;
	static obj_t BGl_symbol1814z00zz__rgc_posixz00 = BUNSPEC;
	static obj_t BGl_parsezd2posixzd2bracesz00zz__rgc_posixz00(obj_t, obj_t);
	static obj_t BGl_z62posixzd2ze3rgcz53zz__rgc_posixz00(obj_t, obj_t);
	static obj_t BGl_symbol1820z00zz__rgc_posixz00 = BUNSPEC;
	static obj_t BGl_symbol1822z00zz__rgc_posixz00 = BUNSPEC;
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string1791z00zz__rgc_posixz00,
		BgL_bgl_string1791za700za7za7_1828za7,
		"RGC:Illegal Posix regexp -- terminated early", 44);
	      DEFINE_STRING(BGl_string1792z00zz__rgc_posixz00,
		BgL_bgl_string1792za700za7za7_1829za7,
		"/tmp/bigloo/runtime/Rgc/rgcposix.scm", 36);
	      DEFINE_STRING(BGl_string1793z00zz__rgc_posixz00,
		BgL_bgl_string1793za700za7za7_1830za7, "&posix->rgc", 11);
	      DEFINE_STRING(BGl_string1794z00zz__rgc_posixz00,
		BgL_bgl_string1794za700za7za7_1831za7, "bstring", 7);
	      DEFINE_STRING(BGl_string1796z00zz__rgc_posixz00,
		BgL_bgl_string1796za700za7za7_1832za7, "*", 1);
	      DEFINE_STRING(BGl_string1798z00zz__rgc_posixz00,
		BgL_bgl_string1798za700za7za7_1833za7, "+", 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_posixzd2ze3rgczd2envze3zz__rgc_posixz00,
		BgL_bgl_za762posixza7d2za7e3rg1834za7,
		BGl_z62posixzd2ze3rgcz53zz__rgc_posixz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1800z00zz__rgc_posixz00,
		BgL_bgl_string1800za700za7za7_1835za7, "?", 1);
	      DEFINE_STRING(BGl_string1802z00zz__rgc_posixz00,
		BgL_bgl_string1802za700za7za7_1836za7, "**", 2);
	      DEFINE_STRING(BGl_string1804z00zz__rgc_posixz00,
		BgL_bgl_string1804za700za7za7_1837za7, ">=", 2);
	      DEFINE_STRING(BGl_string1805z00zz__rgc_posixz00,
		BgL_bgl_string1805za700za7za7_1838za7, "RGC:Illegal posix string", 24);
	      DEFINE_STRING(BGl_string1807z00zz__rgc_posixz00,
		BgL_bgl_string1807za700za7za7_1839za7, "or", 2);
	      DEFINE_STRING(BGl_string1809z00zz__rgc_posixz00,
		BgL_bgl_string1809za700za7za7_1840za7, ":", 1);
	      DEFINE_STRING(BGl_string1810z00zz__rgc_posixz00,
		BgL_bgl_string1810za700za7za7_1841za7, "RGC: `^' regexps not supported.",
		31);
	      DEFINE_STRING(BGl_string1811z00zz__rgc_posixz00,
		BgL_bgl_string1811za700za7za7_1842za7, "RGC: `$' regexps not supported.",
		31);
	      DEFINE_STRING(BGl_string1813z00zz__rgc_posixz00,
		BgL_bgl_string1813za700za7za7_1843za7, "all", 3);
	      DEFINE_STRING(BGl_string1815z00zz__rgc_posixz00,
		BgL_bgl_string1815za700za7za7_1844za7, "submatch", 8);
	      DEFINE_STRING(BGl_string1816z00zz__rgc_posixz00,
		BgL_bgl_string1816za700za7za7_1845za7,
		"RGC:subexpression has no terminating close parenthesis", 54);
	      DEFINE_STRING(BGl_string1817z00zz__rgc_posixz00,
		BgL_bgl_string1817za700za7za7_1846za7,
		"RGC:expression may not terminate with a backslash", 49);
	      DEFINE_STRING(BGl_string1819z00zz__rgc_posixz00,
		BgL_bgl_string1819za700za7za7_1847za7,
		"double-bracket regexps not supported.", 37);
	      DEFINE_STRING(BGl_string1821z00zz__rgc_posixz00,
		BgL_bgl_string1821za700za7za7_1848za7, "out", 3);
	      DEFINE_STRING(BGl_string1823z00zz__rgc_posixz00,
		BgL_bgl_string1823za700za7za7_1849za7, "in", 2);
	      DEFINE_STRING(BGl_string1824z00zz__rgc_posixz00,
		BgL_bgl_string1824za700za7za7_1850za7, "Illegal - in [...] regexp", 25);
	      DEFINE_STRING(BGl_string1825z00zz__rgc_posixz00,
		BgL_bgl_string1825za700za7za7_1851za7,
		"Missing close right bracket in regexp", 37);
	      DEFINE_STRING(BGl_string1826z00zz__rgc_posixz00,
		BgL_bgl_string1826za700za7za7_1852za7, "Missing close brace in regexp", 29);
	      DEFINE_STRING(BGl_string1827z00zz__rgc_posixz00,
		BgL_bgl_string1827za700za7za7_1853za7, "__rgc_posix", 11);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_za2errzd2stringza2zd2zz__rgc_posixz00));
		     ADD_ROOT((void *) (&BGl_list1818z00zz__rgc_posixz00));
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__rgc_posixz00));
		     ADD_ROOT((void *) (&BGl_symbol1795z00zz__rgc_posixz00));
		     ADD_ROOT((void *) (&BGl_symbol1797z00zz__rgc_posixz00));
		     ADD_ROOT((void *) (&BGl_symbol1799z00zz__rgc_posixz00));
		     ADD_ROOT((void *) (&BGl_symbol1801z00zz__rgc_posixz00));
		     ADD_ROOT((void *) (&BGl_symbol1803z00zz__rgc_posixz00));
		     ADD_ROOT((void *) (&BGl_symbol1806z00zz__rgc_posixz00));
		     ADD_ROOT((void *) (&BGl_symbol1808z00zz__rgc_posixz00));
		     ADD_ROOT((void *) (&BGl_symbol1812z00zz__rgc_posixz00));
		     ADD_ROOT((void *) (&BGl_symbol1814z00zz__rgc_posixz00));
		     ADD_ROOT((void *) (&BGl_symbol1820z00zz__rgc_posixz00));
		     ADD_ROOT((void *) (&BGl_symbol1822z00zz__rgc_posixz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__rgc_posixz00(long
		BgL_checksumz00_2319, char *BgL_fromz00_2320)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__rgc_posixz00))
				{
					BGl_requirezd2initializa7ationz75zz__rgc_posixz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__rgc_posixz00();
					BGl_cnstzd2initzd2zz__rgc_posixz00();
					BGl_importedzd2moduleszd2initz00zz__rgc_posixz00();
					return BGl_toplevelzd2initzd2zz__rgc_posixz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__rgc_posixz00(void)
	{
		{	/* Rgc/rgcposix.scm 14 */
			BGl_symbol1795z00zz__rgc_posixz00 =
				bstring_to_symbol(BGl_string1796z00zz__rgc_posixz00);
			BGl_symbol1797z00zz__rgc_posixz00 =
				bstring_to_symbol(BGl_string1798z00zz__rgc_posixz00);
			BGl_symbol1799z00zz__rgc_posixz00 =
				bstring_to_symbol(BGl_string1800z00zz__rgc_posixz00);
			BGl_symbol1801z00zz__rgc_posixz00 =
				bstring_to_symbol(BGl_string1802z00zz__rgc_posixz00);
			BGl_symbol1803z00zz__rgc_posixz00 =
				bstring_to_symbol(BGl_string1804z00zz__rgc_posixz00);
			BGl_symbol1806z00zz__rgc_posixz00 =
				bstring_to_symbol(BGl_string1807z00zz__rgc_posixz00);
			BGl_symbol1808z00zz__rgc_posixz00 =
				bstring_to_symbol(BGl_string1809z00zz__rgc_posixz00);
			BGl_symbol1812z00zz__rgc_posixz00 =
				bstring_to_symbol(BGl_string1813z00zz__rgc_posixz00);
			BGl_symbol1814z00zz__rgc_posixz00 =
				bstring_to_symbol(BGl_string1815z00zz__rgc_posixz00);
			BGl_list1818z00zz__rgc_posixz00 =
				MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '.')),
				MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '=')),
					MAKE_YOUNG_PAIR(BCHAR(((unsigned char) ':')), BNIL)));
			BGl_symbol1820z00zz__rgc_posixz00 =
				bstring_to_symbol(BGl_string1821z00zz__rgc_posixz00);
			return (BGl_symbol1822z00zz__rgc_posixz00 =
				bstring_to_symbol(BGl_string1823z00zz__rgc_posixz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__rgc_posixz00(void)
	{
		{	/* Rgc/rgcposix.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__rgc_posixz00(void)
	{
		{	/* Rgc/rgcposix.scm 14 */
			return (BGl_za2errzd2stringza2zd2zz__rgc_posixz00 = BUNSPEC, BUNSPEC);
		}

	}



/* posix->rgc */
	BGL_EXPORTED_DEF obj_t BGl_posixzd2ze3rgcz31zz__rgc_posixz00(obj_t BgL_sz00_3)
	{
		{	/* Rgc/rgcposix.scm 52 */
			BGl_za2errzd2stringza2zd2zz__rgc_posixz00 = BgL_sz00_3;
			{	/* Rgc/rgcposix.scm 54 */
				obj_t BgL_rgcz00_1195;

				BgL_rgcz00_1195 =
					BGl_parsezd2posixzd2expz00zz__rgc_posixz00(BgL_sz00_3, 0L);
				{	/* Rgc/rgcposix.scm 55 */
					obj_t BgL_iz00_1196;

					{	/* Rgc/rgcposix.scm 56 */
						obj_t BgL_tmpz00_1877;

						{	/* Rgc/rgcposix.scm 56 */
							int BgL_tmpz00_2347;

							BgL_tmpz00_2347 = (int) (1L);
							BgL_tmpz00_1877 = BGL_MVALUES_VAL(BgL_tmpz00_2347);
						}
						{	/* Rgc/rgcposix.scm 56 */
							int BgL_tmpz00_2350;

							BgL_tmpz00_2350 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_2350, BUNSPEC);
						}
						BgL_iz00_1196 = BgL_tmpz00_1877;
					}
					if (((long) CINT(BgL_iz00_1196) == STRING_LENGTH(BgL_sz00_3)))
						{	/* Rgc/rgcposix.scm 56 */
							return BgL_rgcz00_1195;
						}
					else
						{	/* Rgc/rgcposix.scm 56 */
							return
								BGl_errorz00zz__errorz00(BFALSE,
								BGl_string1791z00zz__rgc_posixz00,
								BGl_za2errzd2stringza2zd2zz__rgc_posixz00);
						}
				}
			}
		}

	}



/* &posix->rgc */
	obj_t BGl_z62posixzd2ze3rgcz53zz__rgc_posixz00(obj_t BgL_envz00_2304,
		obj_t BgL_sz00_2305)
	{
		{	/* Rgc/rgcposix.scm 52 */
			{	/* Rgc/rgcposix.scm 53 */
				obj_t BgL_auxz00_2358;

				if (STRINGP(BgL_sz00_2305))
					{	/* Rgc/rgcposix.scm 53 */
						BgL_auxz00_2358 = BgL_sz00_2305;
					}
				else
					{
						obj_t BgL_auxz00_2361;

						BgL_auxz00_2361 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1792z00zz__rgc_posixz00,
							BINT(1819L), BGl_string1793z00zz__rgc_posixz00,
							BGl_string1794z00zz__rgc_posixz00, BgL_sz00_2305);
						FAILURE(BgL_auxz00_2361, BFALSE, BFALSE);
					}
				return BGl_posixzd2ze3rgcz31zz__rgc_posixz00(BgL_auxz00_2358);
			}
		}

	}



/* make-rgc-repeat */
	obj_t BGl_makezd2rgczd2repeatz00zz__rgc_posixz00(obj_t BgL_minz00_5,
		obj_t BgL_maxz00_6, obj_t BgL_rgcz00_7)
	{
		{	/* Rgc/rgcposix.scm 77 */
			{	/* Rgc/rgcposix.scm 79 */
				bool_t BgL_test1857z00_2366;

				if ((BgL_minz00_5 == BINT(0L)))
					{	/* Rgc/rgcposix.scm 79 */
						if (CBOOL(BgL_maxz00_6))
							{	/* Rgc/rgcposix.scm 79 */
								BgL_test1857z00_2366 = ((bool_t) 0);
							}
						else
							{	/* Rgc/rgcposix.scm 79 */
								BgL_test1857z00_2366 = ((bool_t) 1);
							}
					}
				else
					{	/* Rgc/rgcposix.scm 79 */
						BgL_test1857z00_2366 = ((bool_t) 0);
					}
				if (BgL_test1857z00_2366)
					{	/* Rgc/rgcposix.scm 80 */
						obj_t BgL_arg1284z00_1201;

						BgL_arg1284z00_1201 = MAKE_YOUNG_PAIR(BgL_rgcz00_7, BNIL);
						return
							MAKE_YOUNG_PAIR(BGl_symbol1795z00zz__rgc_posixz00,
							BgL_arg1284z00_1201);
					}
				else
					{	/* Rgc/rgcposix.scm 81 */
						bool_t BgL_test1860z00_2374;

						if ((BgL_minz00_5 == BINT(1L)))
							{	/* Rgc/rgcposix.scm 81 */
								if (CBOOL(BgL_maxz00_6))
									{	/* Rgc/rgcposix.scm 81 */
										BgL_test1860z00_2374 = ((bool_t) 0);
									}
								else
									{	/* Rgc/rgcposix.scm 81 */
										BgL_test1860z00_2374 = ((bool_t) 1);
									}
							}
						else
							{	/* Rgc/rgcposix.scm 81 */
								BgL_test1860z00_2374 = ((bool_t) 0);
							}
						if (BgL_test1860z00_2374)
							{	/* Rgc/rgcposix.scm 82 */
								obj_t BgL_arg1304z00_1203;

								BgL_arg1304z00_1203 = MAKE_YOUNG_PAIR(BgL_rgcz00_7, BNIL);
								return
									MAKE_YOUNG_PAIR(BGl_symbol1797z00zz__rgc_posixz00,
									BgL_arg1304z00_1203);
							}
						else
							{	/* Rgc/rgcposix.scm 83 */
								bool_t BgL_test1863z00_2382;

								if ((BgL_minz00_5 == BINT(0L)))
									{	/* Rgc/rgcposix.scm 83 */
										BgL_test1863z00_2382 = (BgL_maxz00_6 == BINT(1L));
									}
								else
									{	/* Rgc/rgcposix.scm 83 */
										BgL_test1863z00_2382 = ((bool_t) 0);
									}
								if (BgL_test1863z00_2382)
									{	/* Rgc/rgcposix.scm 84 */
										obj_t BgL_arg1306z00_1205;

										BgL_arg1306z00_1205 = MAKE_YOUNG_PAIR(BgL_rgcz00_7, BNIL);
										return
											MAKE_YOUNG_PAIR(BGl_symbol1799z00zz__rgc_posixz00,
											BgL_arg1306z00_1205);
									}
								else
									{	/* Rgc/rgcposix.scm 83 */
										if (CBOOL(BgL_maxz00_6))
											{	/* Rgc/rgcposix.scm 88 */
												obj_t BgL_arg1307z00_1206;

												{	/* Rgc/rgcposix.scm 88 */
													obj_t BgL_arg1308z00_1207;

													{	/* Rgc/rgcposix.scm 88 */
														obj_t BgL_arg1309z00_1208;

														BgL_arg1309z00_1208 =
															MAKE_YOUNG_PAIR(BgL_rgcz00_7, BNIL);
														BgL_arg1308z00_1207 =
															MAKE_YOUNG_PAIR(BgL_maxz00_6,
															BgL_arg1309z00_1208);
													}
													BgL_arg1307z00_1206 =
														MAKE_YOUNG_PAIR(BgL_minz00_5, BgL_arg1308z00_1207);
												}
												return
													MAKE_YOUNG_PAIR(BGl_symbol1801z00zz__rgc_posixz00,
													BgL_arg1307z00_1206);
											}
										else
											{	/* Rgc/rgcposix.scm 86 */
												obj_t BgL_arg1310z00_1209;

												{	/* Rgc/rgcposix.scm 86 */
													obj_t BgL_arg1311z00_1210;

													BgL_arg1311z00_1210 =
														MAKE_YOUNG_PAIR(BgL_rgcz00_7, BNIL);
													BgL_arg1310z00_1209 =
														MAKE_YOUNG_PAIR(BgL_minz00_5, BgL_arg1311z00_1210);
												}
												return
													MAKE_YOUNG_PAIR(BGl_symbol1803z00zz__rgc_posixz00,
													BgL_arg1310z00_1209);
											}
									}
							}
					}
			}
		}

	}



/* make-rgc-or */
	obj_t BGl_makezd2rgczd2orz00zz__rgc_posixz00(obj_t BgL_rgcsz00_8)
	{
		{	/* Rgc/rgcposix.scm 93 */
			if (NULLP(BgL_rgcsz00_8))
				{	/* Rgc/rgcposix.scm 95 */
					return
						BGl_errorz00zz__errorz00(BFALSE, BGl_string1805z00zz__rgc_posixz00,
						BGl_za2errzd2stringza2zd2zz__rgc_posixz00);
				}
			else
				{	/* Rgc/rgcposix.scm 95 */
					if (NULLP(CDR(BgL_rgcsz00_8)))
						{	/* Rgc/rgcposix.scm 97 */
							return CAR(BgL_rgcsz00_8);
						}
					else
						{	/* Rgc/rgcposix.scm 100 */
							obj_t BgL_arg1316z00_1214;

							BgL_arg1316z00_1214 =
								BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(BgL_rgcsz00_8,
								BNIL);
							return MAKE_YOUNG_PAIR(BGl_symbol1806z00zz__rgc_posixz00,
								BgL_arg1316z00_1214);
						}
				}
		}

	}



/* make-rgc-sequence */
	obj_t BGl_makezd2rgczd2sequencez00zz__rgc_posixz00(obj_t BgL_rgcsz00_9)
	{
		{	/* Rgc/rgcposix.scm 105 */
			if (NULLP(BgL_rgcsz00_9))
				{	/* Rgc/rgcposix.scm 107 */
					return
						BGl_errorz00zz__errorz00(BFALSE, BGl_string1805z00zz__rgc_posixz00,
						BGl_za2errzd2stringza2zd2zz__rgc_posixz00);
				}
			else
				{	/* Rgc/rgcposix.scm 107 */
					if (NULLP(CDR(BgL_rgcsz00_9)))
						{	/* Rgc/rgcposix.scm 109 */
							return CAR(BgL_rgcsz00_9);
						}
					else
						{	/* Rgc/rgcposix.scm 112 */
							obj_t BgL_arg1321z00_1219;

							BgL_arg1321z00_1219 =
								BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(BgL_rgcsz00_9,
								BNIL);
							return MAKE_YOUNG_PAIR(BGl_symbol1808z00zz__rgc_posixz00,
								BgL_arg1321z00_1219);
						}
				}
		}

	}



/* parse-posix-exp */
	obj_t BGl_parsezd2posixzd2expz00zz__rgc_posixz00(obj_t BgL_sz00_10,
		long BgL_iz00_11)
	{
		{	/* Rgc/rgcposix.scm 119 */
			{	/* Rgc/rgcposix.scm 120 */
				long BgL_lenz00_1221;

				BgL_lenz00_1221 = STRING_LENGTH(BgL_sz00_10);
				if ((BgL_iz00_11 < BgL_lenz00_1221))
					{
						long BgL_iz00_1225;
						obj_t BgL_branchesz00_1226;

						BgL_iz00_1225 = BgL_iz00_11;
						BgL_branchesz00_1226 = BNIL;
					BgL_zc3z04anonymousza31324ze3z87_1227:
						{	/* Rgc/rgcposix.scm 124 */
							obj_t BgL_branchz00_1228;

							BgL_branchz00_1228 =
								BGl_parsezd2posixzd2branchz00zz__rgc_posixz00(BgL_sz00_10,
								BgL_iz00_1225);
							{	/* Rgc/rgcposix.scm 125 */
								obj_t BgL_iz00_1229;

								{	/* Rgc/rgcposix.scm 126 */
									obj_t BgL_tmpz00_1888;

									{	/* Rgc/rgcposix.scm 126 */
										int BgL_tmpz00_2421;

										BgL_tmpz00_2421 = (int) (1L);
										BgL_tmpz00_1888 = BGL_MVALUES_VAL(BgL_tmpz00_2421);
									}
									{	/* Rgc/rgcposix.scm 126 */
										int BgL_tmpz00_2424;

										BgL_tmpz00_2424 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_2424, BUNSPEC);
									}
									BgL_iz00_1229 = BgL_tmpz00_1888;
								}
								{	/* Rgc/rgcposix.scm 126 */
									obj_t BgL_branchesz00_1230;

									BgL_branchesz00_1230 =
										MAKE_YOUNG_PAIR(BgL_branchz00_1228, BgL_branchesz00_1226);
									{	/* Rgc/rgcposix.scm 127 */
										bool_t BgL_test1871z00_2428;

										if (((long) CINT(BgL_iz00_1229) < BgL_lenz00_1221))
											{	/* Rgc/rgcposix.scm 127 */
												BgL_test1871z00_2428 =
													(((unsigned char) '|') ==
													STRING_REF(BgL_sz00_10, (long) CINT(BgL_iz00_1229)));
											}
										else
											{	/* Rgc/rgcposix.scm 127 */
												BgL_test1871z00_2428 = ((bool_t) 0);
											}
										if (BgL_test1871z00_2428)
											{
												obj_t BgL_branchesz00_2438;
												long BgL_iz00_2435;

												BgL_iz00_2435 = ((long) CINT(BgL_iz00_1229) + 1L);
												BgL_branchesz00_2438 = BgL_branchesz00_1230;
												BgL_branchesz00_1226 = BgL_branchesz00_2438;
												BgL_iz00_1225 = BgL_iz00_2435;
												goto BgL_zc3z04anonymousza31324ze3z87_1227;
											}
										else
											{	/* Rgc/rgcposix.scm 129 */
												obj_t BgL_val0_1102z00_1235;

												BgL_val0_1102z00_1235 =
													BGl_makezd2rgczd2orz00zz__rgc_posixz00(bgl_reverse
													(BgL_branchesz00_1230));
												{	/* Rgc/rgcposix.scm 129 */
													int BgL_tmpz00_2441;

													BgL_tmpz00_2441 = (int) (2L);
													BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2441);
												}
												{	/* Rgc/rgcposix.scm 129 */
													int BgL_tmpz00_2444;

													BgL_tmpz00_2444 = (int) (1L);
													BGL_MVALUES_VAL_SET(BgL_tmpz00_2444, BgL_iz00_1229);
												}
												return BgL_val0_1102z00_1235;
											}
									}
								}
							}
						}
					}
				else
					{	/* Rgc/rgcposix.scm 121 */
						{	/* Rgc/rgcposix.scm 130 */
							int BgL_tmpz00_2447;

							BgL_tmpz00_2447 = (int) (2L);
							BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2447);
						}
						{	/* Rgc/rgcposix.scm 130 */
							obj_t BgL_auxz00_2452;
							int BgL_tmpz00_2450;

							BgL_auxz00_2452 = BINT(BgL_iz00_11);
							BgL_tmpz00_2450 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_2450, BgL_auxz00_2452);
						}
						return BNIL;
					}
			}
		}

	}



/* parse-posix-branch */
	obj_t BGl_parsezd2posixzd2branchz00zz__rgc_posixz00(obj_t BgL_sz00_12,
		long BgL_iz00_13)
	{
		{	/* Rgc/rgcposix.scm 138 */
			{	/* Rgc/rgcposix.scm 139 */
				long BgL_lenz00_1243;

				BgL_lenz00_1243 = STRING_LENGTH(BgL_sz00_12);
				{
					obj_t BgL_iz00_1246;
					obj_t BgL_piecesz00_1247;

					BgL_iz00_1246 = BINT(BgL_iz00_13);
					BgL_piecesz00_1247 = BNIL;
				BgL_zc3z04anonymousza31332ze3z87_1248:
					if (((long) CINT(BgL_iz00_1246) < BgL_lenz00_1243))
						{	/* Rgc/rgcposix.scm 143 */
							obj_t BgL_piecez00_1250;

							BgL_piecez00_1250 =
								BGl_parsezd2posixzd2piecez00zz__rgc_posixz00(BgL_sz00_12,
								BgL_iz00_1246);
							{	/* Rgc/rgcposix.scm 144 */
								obj_t BgL_iz00_1251;

								{	/* Rgc/rgcposix.scm 145 */
									obj_t BgL_tmpz00_1899;

									{	/* Rgc/rgcposix.scm 145 */
										int BgL_tmpz00_2460;

										BgL_tmpz00_2460 = (int) (1L);
										BgL_tmpz00_1899 = BGL_MVALUES_VAL(BgL_tmpz00_2460);
									}
									{	/* Rgc/rgcposix.scm 145 */
										int BgL_tmpz00_2463;

										BgL_tmpz00_2463 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_2463, BUNSPEC);
									}
									BgL_iz00_1251 = BgL_tmpz00_1899;
								}
								{	/* Rgc/rgcposix.scm 145 */
									obj_t BgL_piecesz00_1252;

									BgL_piecesz00_1252 =
										MAKE_YOUNG_PAIR(BgL_piecez00_1250, BgL_piecesz00_1247);
									if (((long) CINT(BgL_iz00_1251) < BgL_lenz00_1243))
										{	/* Rgc/rgcposix.scm 147 */
											unsigned char BgL_aux1042z00_1255;

											BgL_aux1042z00_1255 =
												STRING_REF(BgL_sz00_12, (long) CINT(BgL_iz00_1251));
											switch (BgL_aux1042z00_1255)
												{
												case ((unsigned char) ')'):
												case ((unsigned char) '|'):

													{	/* Rgc/rgcposix.scm 149 */
														obj_t BgL_val0_1106z00_1257;

														BgL_val0_1106z00_1257 =
															BGl_makezd2rgczd2sequencez00zz__rgc_posixz00
															(bgl_reverse(BgL_piecesz00_1252));
														{	/* Rgc/rgcposix.scm 149 */
															int BgL_tmpz00_2474;

															BgL_tmpz00_2474 = (int) (2L);
															BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2474);
														}
														{	/* Rgc/rgcposix.scm 149 */
															int BgL_tmpz00_2477;

															BgL_tmpz00_2477 = (int) (1L);
															BGL_MVALUES_VAL_SET(BgL_tmpz00_2477,
																BgL_iz00_1251);
														}
														return BgL_val0_1106z00_1257;
													}
													break;
												default:
													{
														obj_t BgL_piecesz00_2481;
														obj_t BgL_iz00_2480;

														BgL_iz00_2480 = BgL_iz00_1251;
														BgL_piecesz00_2481 = BgL_piecesz00_1252;
														BgL_piecesz00_1247 = BgL_piecesz00_2481;
														BgL_iz00_1246 = BgL_iz00_2480;
														goto BgL_zc3z04anonymousza31332ze3z87_1248;
													}
												}
										}
									else
										{	/* Rgc/rgcposix.scm 152 */
											obj_t BgL_val0_1108z00_1260;

											BgL_val0_1108z00_1260 =
												BGl_makezd2rgczd2sequencez00zz__rgc_posixz00(bgl_reverse
												(BgL_piecesz00_1252));
											{	/* Rgc/rgcposix.scm 152 */
												int BgL_tmpz00_2485;

												BgL_tmpz00_2485 = (int) (2L);
												BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2485);
											}
											{	/* Rgc/rgcposix.scm 152 */
												int BgL_tmpz00_2488;

												BgL_tmpz00_2488 = (int) (1L);
												BGL_MVALUES_VAL_SET(BgL_tmpz00_2488, BgL_iz00_1251);
											}
											return BgL_val0_1108z00_1260;
										}
								}
							}
						}
					else
						{	/* Rgc/rgcposix.scm 153 */
							obj_t BgL_val0_1110z00_1263;

							BgL_val0_1110z00_1263 =
								BGl_makezd2rgczd2sequencez00zz__rgc_posixz00(bgl_reverse
								(BgL_piecesz00_1247));
							{	/* Rgc/rgcposix.scm 153 */
								int BgL_tmpz00_2493;

								BgL_tmpz00_2493 = (int) (2L);
								BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2493);
							}
							{	/* Rgc/rgcposix.scm 153 */
								int BgL_tmpz00_2496;

								BgL_tmpz00_2496 = (int) (1L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_2496, BgL_iz00_1246);
							}
							return BgL_val0_1110z00_1263;
						}
				}
			}
		}

	}



/* parse-posix-piece */
	obj_t BGl_parsezd2posixzd2piecez00zz__rgc_posixz00(obj_t BgL_sz00_14,
		obj_t BgL_iz00_15)
	{
		{	/* Rgc/rgcposix.scm 161 */
			{	/* Rgc/rgcposix.scm 162 */
				long BgL_lenz00_1267;

				BgL_lenz00_1267 = STRING_LENGTH(BgL_sz00_14);
				{	/* Rgc/rgcposix.scm 163 */
					obj_t BgL_atomz00_1268;

					BgL_atomz00_1268 =
						BGl_parsezd2posixzd2atomz00zz__rgc_posixz00(BgL_sz00_14,
						BgL_iz00_15);
					{	/* Rgc/rgcposix.scm 164 */
						obj_t BgL_iz00_1269;

						{	/* Rgc/rgcposix.scm 165 */
							obj_t BgL_tmpz00_1905;

							{	/* Rgc/rgcposix.scm 165 */
								int BgL_tmpz00_2502;

								BgL_tmpz00_2502 = (int) (1L);
								BgL_tmpz00_1905 = BGL_MVALUES_VAL(BgL_tmpz00_2502);
							}
							{	/* Rgc/rgcposix.scm 165 */
								int BgL_tmpz00_2505;

								BgL_tmpz00_2505 = (int) (1L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_2505, BUNSPEC);
							}
							BgL_iz00_1269 = BgL_tmpz00_1905;
						}
						if (((long) CINT(BgL_iz00_1269) < BgL_lenz00_1267))
							{

								{	/* Rgc/rgcposix.scm 166 */
									unsigned char BgL_aux1044z00_1272;

									BgL_aux1044z00_1272 =
										STRING_REF(BgL_sz00_14, (long) CINT(BgL_iz00_1269));
									switch (BgL_aux1044z00_1272)
										{
										case ((unsigned char) '*'):
										case ((unsigned char) '+'):
										case ((unsigned char) '?'):

											{	/* Rgc/rgcposix.scm 168 */
												obj_t BgL_fromz00_1274;

												switch (BgL_aux1044z00_1272)
													{
													case ((unsigned char) '*'):

														{	/* Rgc/rgcposix.scm 170 */
															int BgL_tmpz00_2513;

															BgL_tmpz00_2513 = (int) (2L);
															BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2513);
														}
														{	/* Rgc/rgcposix.scm 170 */
															int BgL_tmpz00_2516;

															BgL_tmpz00_2516 = (int) (1L);
															BGL_MVALUES_VAL_SET(BgL_tmpz00_2516, BFALSE);
														}
														BgL_fromz00_1274 = BINT(0L);
														break;
													case ((unsigned char) '+'):

														{	/* Rgc/rgcposix.scm 171 */
															int BgL_tmpz00_2520;

															BgL_tmpz00_2520 = (int) (2L);
															BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2520);
														}
														{	/* Rgc/rgcposix.scm 171 */
															int BgL_tmpz00_2523;

															BgL_tmpz00_2523 = (int) (1L);
															BGL_MVALUES_VAL_SET(BgL_tmpz00_2523, BFALSE);
														}
														BgL_fromz00_1274 = BINT(1L);
														break;
													case ((unsigned char) '?'):

														{	/* Rgc/rgcposix.scm 172 */
															int BgL_tmpz00_2527;

															BgL_tmpz00_2527 = (int) (2L);
															BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2527);
														}
														{	/* Rgc/rgcposix.scm 172 */
															obj_t BgL_auxz00_2532;
															int BgL_tmpz00_2530;

															BgL_auxz00_2532 = BINT(1L);
															BgL_tmpz00_2530 = (int) (1L);
															BGL_MVALUES_VAL_SET(BgL_tmpz00_2530,
																BgL_auxz00_2532);
														}
														BgL_fromz00_1274 = BINT(0L);
														break;
													default:
														BgL_fromz00_1274 = BUNSPEC;
													}
												{	/* Rgc/rgcposix.scm 169 */
													obj_t BgL_toz00_1275;

													{	/* Rgc/rgcposix.scm 173 */
														obj_t BgL_tmpz00_1912;

														{	/* Rgc/rgcposix.scm 173 */
															int BgL_tmpz00_2537;

															BgL_tmpz00_2537 = (int) (1L);
															BgL_tmpz00_1912 =
																BGL_MVALUES_VAL(BgL_tmpz00_2537);
														}
														{	/* Rgc/rgcposix.scm 173 */
															int BgL_tmpz00_2540;

															BgL_tmpz00_2540 = (int) (1L);
															BGL_MVALUES_VAL_SET(BgL_tmpz00_2540, BUNSPEC);
														}
														BgL_toz00_1275 = BgL_tmpz00_1912;
													}
													{	/* Rgc/rgcposix.scm 173 */
														obj_t BgL_val0_1120z00_1276;
														long BgL_val1_1121z00_1277;

														BgL_val0_1120z00_1276 =
															BGl_makezd2rgczd2repeatz00zz__rgc_posixz00
															(BgL_fromz00_1274, BgL_toz00_1275,
															BgL_atomz00_1268);
														BgL_val1_1121z00_1277 =
															((long) CINT(BgL_iz00_1269) + 1L);
														{	/* Rgc/rgcposix.scm 173 */
															int BgL_tmpz00_2546;

															BgL_tmpz00_2546 = (int) (2L);
															BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2546);
														}
														{	/* Rgc/rgcposix.scm 173 */
															obj_t BgL_auxz00_2551;
															int BgL_tmpz00_2549;

															BgL_auxz00_2551 = BINT(BgL_val1_1121z00_1277);
															BgL_tmpz00_2549 = (int) (1L);
															BGL_MVALUES_VAL_SET(BgL_tmpz00_2549,
																BgL_auxz00_2551);
														}
														return BgL_val0_1120z00_1276;
													}
												}
											}
											break;
										case ((unsigned char) '{'):

											{	/* Rgc/rgcposix.scm 175 */
												obj_t BgL_fromz00_1287;

												{	/* Rgc/rgcposix.scm 176 */
													obj_t BgL_arg1342z00_1292;

													if (INTEGERP(BgL_iz00_1269))
														{	/* Rgc/rgcposix.scm 176 */
															BgL_arg1342z00_1292 =
																ADDFX(BgL_iz00_1269, BINT(1L));
														}
													else
														{	/* Rgc/rgcposix.scm 176 */
															BgL_arg1342z00_1292 =
																BGl_2zb2zb2zz__r4_numbers_6_5z00(BgL_iz00_1269,
																BINT(1L));
														}
													BgL_fromz00_1287 =
														BGl_parsezd2posixzd2bracesz00zz__rgc_posixz00
														(BgL_sz00_14, BgL_arg1342z00_1292);
												}
												{	/* Rgc/rgcposix.scm 176 */
													obj_t BgL_toz00_1288;
													obj_t BgL_iz00_1289;

													{	/* Rgc/rgcposix.scm 177 */
														obj_t BgL_tmpz00_1915;

														{	/* Rgc/rgcposix.scm 177 */
															int BgL_tmpz00_2561;

															BgL_tmpz00_2561 = (int) (1L);
															BgL_tmpz00_1915 =
																BGL_MVALUES_VAL(BgL_tmpz00_2561);
														}
														{	/* Rgc/rgcposix.scm 177 */
															int BgL_tmpz00_2564;

															BgL_tmpz00_2564 = (int) (1L);
															BGL_MVALUES_VAL_SET(BgL_tmpz00_2564, BUNSPEC);
														}
														BgL_toz00_1288 = BgL_tmpz00_1915;
													}
													{	/* Rgc/rgcposix.scm 177 */
														obj_t BgL_tmpz00_1916;

														{	/* Rgc/rgcposix.scm 177 */
															int BgL_tmpz00_2567;

															BgL_tmpz00_2567 = (int) (2L);
															BgL_tmpz00_1916 =
																BGL_MVALUES_VAL(BgL_tmpz00_2567);
														}
														{	/* Rgc/rgcposix.scm 177 */
															int BgL_tmpz00_2570;

															BgL_tmpz00_2570 = (int) (2L);
															BGL_MVALUES_VAL_SET(BgL_tmpz00_2570, BUNSPEC);
														}
														BgL_iz00_1289 = BgL_tmpz00_1916;
													}
													{	/* Rgc/rgcposix.scm 177 */
														obj_t BgL_val0_1122z00_1290;

														BgL_val0_1122z00_1290 =
															BGl_makezd2rgczd2repeatz00zz__rgc_posixz00
															(BgL_fromz00_1287, BgL_toz00_1288,
															BgL_atomz00_1268);
														{	/* Rgc/rgcposix.scm 177 */
															int BgL_tmpz00_2574;

															BgL_tmpz00_2574 = (int) (2L);
															BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2574);
														}
														{	/* Rgc/rgcposix.scm 177 */
															int BgL_tmpz00_2577;

															BgL_tmpz00_2577 = (int) (1L);
															BGL_MVALUES_VAL_SET(BgL_tmpz00_2577,
																BgL_iz00_1289);
														}
														return BgL_val0_1122z00_1290;
													}
												}
											}
											break;
										default:
											{	/* Rgc/rgcposix.scm 178 */
												int BgL_tmpz00_2580;

												BgL_tmpz00_2580 = (int) (2L);
												BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2580);
											}
											{	/* Rgc/rgcposix.scm 178 */
												int BgL_tmpz00_2583;

												BgL_tmpz00_2583 = (int) (1L);
												BGL_MVALUES_VAL_SET(BgL_tmpz00_2583, BgL_iz00_1269);
											}
											return BgL_atomz00_1268;
										}
								}
							}
						else
							{	/* Rgc/rgcposix.scm 165 */
								{	/* Rgc/rgcposix.scm 179 */
									int BgL_tmpz00_2587;

									BgL_tmpz00_2587 = (int) (2L);
									BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2587);
								}
								{	/* Rgc/rgcposix.scm 179 */
									int BgL_tmpz00_2590;

									BgL_tmpz00_2590 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_2590, BgL_iz00_1269);
								}
								return BgL_atomz00_1268;
							}
					}
				}
			}
		}

	}



/* parse-posix-atom */
	obj_t BGl_parsezd2posixzd2atomz00zz__rgc_posixz00(obj_t BgL_sz00_16,
		obj_t BgL_iz00_17)
	{
		{	/* Rgc/rgcposix.scm 187 */
			{	/* Rgc/rgcposix.scm 188 */
				long BgL_lenz00_1297;

				BgL_lenz00_1297 = STRING_LENGTH(BgL_sz00_16);
				if (((long) CINT(BgL_iz00_17) < BgL_lenz00_1297))
					{	/* Rgc/rgcposix.scm 190 */
						unsigned char BgL_cz00_1300;

						BgL_cz00_1300 = STRING_REF(BgL_sz00_16, (long) CINT(BgL_iz00_17));
						{

							switch (BgL_cz00_1300)
								{
								case ((unsigned char) '^'):

									return
										BGl_errorz00zz__errorz00(BFALSE,
										BGl_string1810z00zz__rgc_posixz00,
										BGl_za2errzd2stringza2zd2zz__rgc_posixz00);
									break;
								case ((unsigned char) '$'):

									return
										BGl_errorz00zz__errorz00(BFALSE,
										BGl_string1811z00zz__rgc_posixz00,
										BGl_za2errzd2stringza2zd2zz__rgc_posixz00);
									break;
								case ((unsigned char) '.'):

									{	/* Rgc/rgcposix.scm 194 */
										obj_t BgL_val0_1128z00_1304;
										long BgL_val1_1129z00_1305;

										BgL_val0_1128z00_1304 = BGl_symbol1812z00zz__rgc_posixz00;
										BgL_val1_1129z00_1305 = ((long) CINT(BgL_iz00_17) + 1L);
										{	/* Rgc/rgcposix.scm 194 */
											int BgL_tmpz00_2603;

											BgL_tmpz00_2603 = (int) (2L);
											BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2603);
										}
										{	/* Rgc/rgcposix.scm 194 */
											obj_t BgL_auxz00_2608;
											int BgL_tmpz00_2606;

											BgL_auxz00_2608 = BINT(BgL_val1_1129z00_1305);
											BgL_tmpz00_2606 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2606, BgL_auxz00_2608);
										}
										return BgL_val0_1128z00_1304;
									}
									break;
								case ((unsigned char) '['):

									return
										BGl_parsezd2posixzd2bracketz00zz__rgc_posixz00(BgL_sz00_16,
										((long) CINT(BgL_iz00_17) + 1L));
									break;
								case ((unsigned char) '('):

									{	/* Rgc/rgcposix.scm 196 */
										obj_t BgL_rez00_1307;

										BgL_rez00_1307 =
											BGl_parsezd2posixzd2expz00zz__rgc_posixz00(BgL_sz00_16,
											((long) CINT(BgL_iz00_17) + 1L));
										{	/* Rgc/rgcposix.scm 197 */
											obj_t BgL_iz00_1308;

											{	/* Rgc/rgcposix.scm 198 */
												obj_t BgL_tmpz00_1927;

												{	/* Rgc/rgcposix.scm 198 */
													int BgL_tmpz00_2617;

													BgL_tmpz00_2617 = (int) (1L);
													BgL_tmpz00_1927 = BGL_MVALUES_VAL(BgL_tmpz00_2617);
												}
												{	/* Rgc/rgcposix.scm 198 */
													int BgL_tmpz00_2620;

													BgL_tmpz00_2620 = (int) (1L);
													BGL_MVALUES_VAL_SET(BgL_tmpz00_2620, BUNSPEC);
												}
												BgL_iz00_1308 = BgL_tmpz00_1927;
											}
											{	/* Rgc/rgcposix.scm 198 */
												bool_t BgL_test1878z00_2623;

												if (((long) CINT(BgL_iz00_1308) < BgL_lenz00_1297))
													{	/* Rgc/rgcposix.scm 198 */
														BgL_test1878z00_2623 =
															(((unsigned char) ')') ==
															STRING_REF(BgL_sz00_16,
																(long) CINT(BgL_iz00_1308)));
													}
												else
													{	/* Rgc/rgcposix.scm 198 */
														BgL_test1878z00_2623 = ((bool_t) 0);
													}
												if (BgL_test1878z00_2623)
													{	/* Rgc/rgcposix.scm 199 */
														obj_t BgL_val0_1130z00_1312;
														long BgL_val1_1131z00_1313;

														{	/* Rgc/rgcposix.scm 199 */
															obj_t BgL_arg1350z00_1314;

															BgL_arg1350z00_1314 =
																MAKE_YOUNG_PAIR(BgL_rez00_1307, BNIL);
															BgL_val0_1130z00_1312 =
																MAKE_YOUNG_PAIR
																(BGl_symbol1814z00zz__rgc_posixz00,
																BgL_arg1350z00_1314);
														}
														BgL_val1_1131z00_1313 =
															((long) CINT(BgL_iz00_1308) + 1L);
														{	/* Rgc/rgcposix.scm 199 */
															int BgL_tmpz00_2634;

															BgL_tmpz00_2634 = (int) (2L);
															BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2634);
														}
														{	/* Rgc/rgcposix.scm 199 */
															obj_t BgL_auxz00_2639;
															int BgL_tmpz00_2637;

															BgL_auxz00_2639 = BINT(BgL_val1_1131z00_1313);
															BgL_tmpz00_2637 = (int) (1L);
															BGL_MVALUES_VAL_SET(BgL_tmpz00_2637,
																BgL_auxz00_2639);
														}
														return BgL_val0_1130z00_1312;
													}
												else
													{	/* Rgc/rgcposix.scm 198 */
														return
															BGl_errorz00zz__errorz00(BFALSE,
															BGl_string1816z00zz__rgc_posixz00,
															BGl_za2errzd2stringza2zd2zz__rgc_posixz00);
													}
											}
										}
									}
									break;
								case ((unsigned char) '\\'):

									{	/* Rgc/rgcposix.scm 201 */
										long BgL_iz00_1318;

										BgL_iz00_1318 = ((long) CINT(BgL_iz00_17) + 1L);
										if ((BgL_iz00_1318 < BgL_lenz00_1297))
											{	/* Rgc/rgcposix.scm 203 */
												obj_t BgL_val0_1132z00_1320;
												long BgL_val1_1133z00_1321;

												{	/* Rgc/rgcposix.scm 203 */
													unsigned char BgL_arg1354z00_1322;

													BgL_arg1354z00_1322 =
														STRING_REF(BgL_sz00_16, BgL_iz00_1318);
													{	/* Rgc/rgcposix.scm 203 */
														obj_t BgL_list1355z00_1323;

														BgL_list1355z00_1323 =
															MAKE_YOUNG_PAIR(BCHAR(BgL_arg1354z00_1322), BNIL);
														BgL_val0_1132z00_1320 =
															BGl_listzd2ze3stringz31zz__r4_strings_6_7z00
															(BgL_list1355z00_1323);
												}}
												BgL_val1_1133z00_1321 = (BgL_iz00_1318 + 1L);
												{	/* Rgc/rgcposix.scm 203 */
													int BgL_tmpz00_2652;

													BgL_tmpz00_2652 = (int) (2L);
													BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2652);
												}
												{	/* Rgc/rgcposix.scm 203 */
													obj_t BgL_auxz00_2657;
													int BgL_tmpz00_2655;

													BgL_auxz00_2657 = BINT(BgL_val1_1133z00_1321);
													BgL_tmpz00_2655 = (int) (1L);
													BGL_MVALUES_VAL_SET(BgL_tmpz00_2655, BgL_auxz00_2657);
												}
												return BgL_val0_1132z00_1320;
											}
										else
											{	/* Rgc/rgcposix.scm 202 */
												return
													BGl_errorz00zz__errorz00(BFALSE,
													BGl_string1817z00zz__rgc_posixz00,
													BGl_za2errzd2stringza2zd2zz__rgc_posixz00);
											}
									}
									break;
								case ((unsigned char) ')'):
								case ((unsigned char) '|'):
								case ((unsigned char) '*'):
								case ((unsigned char) '+'):
								case ((unsigned char) '?'):
								case ((unsigned char) '{'):

									{	/* Rgc/rgcposix.scm 206 */
										int BgL_tmpz00_2661;

										BgL_tmpz00_2661 = (int) (2L);
										BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2661);
									}
									{	/* Rgc/rgcposix.scm 206 */
										int BgL_tmpz00_2664;

										BgL_tmpz00_2664 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_2664, BgL_iz00_17);
									}
									return BNIL;
									break;
								default:
									{	/* Rgc/rgcposix.scm 207 */
										obj_t BgL_val0_1126z00_1326;
										long BgL_val1_1127z00_1327;

										{	/* Rgc/rgcposix.scm 207 */
											obj_t BgL_list1356z00_1328;

											BgL_list1356z00_1328 =
												MAKE_YOUNG_PAIR(BCHAR(BgL_cz00_1300), BNIL);
											BgL_val0_1126z00_1326 =
												BGl_listzd2ze3stringz31zz__r4_strings_6_7z00
												(BgL_list1356z00_1328);
										}
										BgL_val1_1127z00_1327 = ((long) CINT(BgL_iz00_17) + 1L);
										{	/* Rgc/rgcposix.scm 207 */
											int BgL_tmpz00_2672;

											BgL_tmpz00_2672 = (int) (2L);
											BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2672);
										}
										{	/* Rgc/rgcposix.scm 207 */
											obj_t BgL_auxz00_2677;
											int BgL_tmpz00_2675;

											BgL_auxz00_2677 = BINT(BgL_val1_1127z00_1327);
											BgL_tmpz00_2675 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_2675, BgL_auxz00_2677);
										}
										return BgL_val0_1126z00_1326;
									}
								}
						}
					}
				else
					{	/* Rgc/rgcposix.scm 189 */
						{	/* Rgc/rgcposix.scm 208 */
							int BgL_tmpz00_2681;

							BgL_tmpz00_2681 = (int) (2L);
							BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2681);
						}
						{	/* Rgc/rgcposix.scm 208 */
							int BgL_tmpz00_2684;

							BgL_tmpz00_2684 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_2684, BgL_iz00_17);
						}
						return BNIL;
					}
			}
		}

	}



/* parse-posix-bracket */
	obj_t BGl_parsezd2posixzd2bracketz00zz__rgc_posixz00(obj_t BgL_sz00_18,
		long BgL_iz00_19)
	{
		{	/* Rgc/rgcposix.scm 216 */
			{	/* Rgc/rgcposix.scm 217 */
				long BgL_lenz00_1332;

				BgL_lenz00_1332 = STRING_LENGTH(BgL_sz00_18);
				if ((BgL_iz00_19 < BgL_lenz00_1332))
					{	/* Rgc/rgcposix.scm 219 */
						bool_t BgL_negatezf3zf3_1334;

						{	/* Rgc/rgcposix.scm 220 */
							unsigned char BgL_cz00_1400;

							BgL_cz00_1400 = STRING_REF(BgL_sz00_18, BgL_iz00_19);
							if ((BgL_cz00_1400 == ((unsigned char) '^')))
								{	/* Rgc/rgcposix.scm 222 */
									long BgL_val1_1139z00_1403;

									BgL_val1_1139z00_1403 = (BgL_iz00_19 + 1L);
									{	/* Rgc/rgcposix.scm 222 */
										int BgL_tmpz00_2694;

										BgL_tmpz00_2694 = (int) (2L);
										BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2694);
									}
									{	/* Rgc/rgcposix.scm 222 */
										obj_t BgL_auxz00_2699;
										int BgL_tmpz00_2697;

										BgL_auxz00_2699 = BINT(BgL_val1_1139z00_1403);
										BgL_tmpz00_2697 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_2697, BgL_auxz00_2699);
									}
									BgL_negatezf3zf3_1334 = ((bool_t) 1);
								}
							else
								{	/* Rgc/rgcposix.scm 221 */
									{	/* Rgc/rgcposix.scm 223 */
										int BgL_tmpz00_2702;

										BgL_tmpz00_2702 = (int) (2L);
										BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2702);
									}
									{	/* Rgc/rgcposix.scm 223 */
										obj_t BgL_auxz00_2707;
										int BgL_tmpz00_2705;

										BgL_auxz00_2707 = BINT(BgL_iz00_19);
										BgL_tmpz00_2705 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_2705, BgL_auxz00_2707);
									}
									BgL_negatezf3zf3_1334 = ((bool_t) 0);
						}}
						{	/* Rgc/rgcposix.scm 220 */
							obj_t BgL_i0z00_1335;

							{	/* Rgc/rgcposix.scm 224 */
								obj_t BgL_tmpz00_1949;

								{	/* Rgc/rgcposix.scm 224 */
									int BgL_tmpz00_2710;

									BgL_tmpz00_2710 = (int) (1L);
									BgL_tmpz00_1949 = BGL_MVALUES_VAL(BgL_tmpz00_2710);
								}
								{	/* Rgc/rgcposix.scm 224 */
									int BgL_tmpz00_2713;

									BgL_tmpz00_2713 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_2713, BUNSPEC);
								}
								BgL_i0z00_1335 = BgL_tmpz00_1949;
							}
							{
								obj_t BgL_iz00_1338;
								obj_t BgL_csetz00_1339;

								BgL_iz00_1338 = BgL_i0z00_1335;
								BgL_csetz00_1339 = BNIL;
							BgL_zc3z04anonymousza31359ze3z87_1340:
								if (((long) CINT(BgL_iz00_1338) < BgL_lenz00_1332))
									{	/* Rgc/rgcposix.scm 227 */
										unsigned char BgL_cz00_1342;

										BgL_cz00_1342 =
											STRING_REF(BgL_sz00_18, (long) CINT(BgL_iz00_1338));
										{

											switch (BgL_cz00_1342)
												{
												case ((unsigned char) '['):

													{	/* Rgc/rgcposix.scm 231 */
														long BgL_i1z00_1346;

														BgL_i1z00_1346 = ((long) CINT(BgL_iz00_1338) + 1L);
														{	/* Rgc/rgcposix.scm 233 */
															bool_t BgL_test1884z00_2723;

															if ((BgL_i1z00_1346 < BgL_lenz00_1332))
																{	/* Rgc/rgcposix.scm 233 */
																	BgL_test1884z00_2723 =
																		CBOOL
																		(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																		(BCHAR(STRING_REF(BgL_sz00_18,
																					BgL_i1z00_1346)),
																			BGl_list1818z00zz__rgc_posixz00));
																}
															else
																{	/* Rgc/rgcposix.scm 233 */
																	BgL_test1884z00_2723 = ((bool_t) 0);
																}
															if (BgL_test1884z00_2723)
																{	/* Rgc/rgcposix.scm 233 */
																	return
																		BGl_errorz00zz__errorz00(BFALSE,
																		BGl_string1819z00zz__rgc_posixz00,
																		BGl_za2errzd2stringza2zd2zz__rgc_posixz00);
																}
															else
																{	/* Rgc/rgcposix.scm 238 */
																	obj_t BgL_arg1365z00_1350;

																	BgL_arg1365z00_1350 =
																		MAKE_YOUNG_PAIR(BCHAR(((unsigned char)
																				'[')), BgL_csetz00_1339);
																	{
																		obj_t BgL_csetz00_2735;
																		obj_t BgL_iz00_2733;

																		BgL_iz00_2733 = BINT(BgL_i1z00_1346);
																		BgL_csetz00_2735 = BgL_arg1365z00_1350;
																		BgL_csetz00_1339 = BgL_csetz00_2735;
																		BgL_iz00_1338 = BgL_iz00_2733;
																		goto BgL_zc3z04anonymousza31359ze3z87_1340;
																	}
																}
														}
													}
													break;
												case ((unsigned char) ']'):

													if (
														((long) CINT(BgL_iz00_1338) ==
															(long) CINT(BgL_i0z00_1335)))
														{	/* Rgc/rgcposix.scm 242 */
															long BgL_arg1368z00_1354;
															obj_t BgL_arg1369z00_1355;

															BgL_arg1368z00_1354 =
																((long) CINT(BgL_iz00_1338) + 1L);
															BgL_arg1369z00_1355 =
																MAKE_YOUNG_PAIR(BCHAR(((unsigned char) ']')),
																BgL_csetz00_1339);
															{
																obj_t BgL_csetz00_2746;
																obj_t BgL_iz00_2744;

																BgL_iz00_2744 = BINT(BgL_arg1368z00_1354);
																BgL_csetz00_2746 = BgL_arg1369z00_1355;
																BgL_csetz00_1339 = BgL_csetz00_2746;
																BgL_iz00_1338 = BgL_iz00_2744;
																goto BgL_zc3z04anonymousza31359ze3z87_1340;
															}
														}
													else
														{	/* Rgc/rgcposix.scm 241 */
															if (BgL_negatezf3zf3_1334)
																{	/* Rgc/rgcposix.scm 246 */
																	obj_t BgL_val0_1142z00_1356;
																	long BgL_val1_1143z00_1357;

																	{	/* Rgc/rgcposix.scm 246 */
																		obj_t BgL_arg1370z00_1358;

																		BgL_arg1370z00_1358 =
																			BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																			(BgL_csetz00_1339, BNIL);
																		BgL_val0_1142z00_1356 =
																			MAKE_YOUNG_PAIR
																			(BGl_symbol1820z00zz__rgc_posixz00,
																			BgL_arg1370z00_1358);
																	}
																	BgL_val1_1143z00_1357 =
																		((long) CINT(BgL_iz00_1338) + 1L);
																	{	/* Rgc/rgcposix.scm 246 */
																		int BgL_tmpz00_2752;

																		BgL_tmpz00_2752 = (int) (2L);
																		BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2752);
																	}
																	{	/* Rgc/rgcposix.scm 246 */
																		obj_t BgL_auxz00_2757;
																		int BgL_tmpz00_2755;

																		BgL_auxz00_2757 =
																			BINT(BgL_val1_1143z00_1357);
																		BgL_tmpz00_2755 = (int) (1L);
																		BGL_MVALUES_VAL_SET(BgL_tmpz00_2755,
																			BgL_auxz00_2757);
																	}
																	return BgL_val0_1142z00_1356;
																}
															else
																{	/* Rgc/rgcposix.scm 247 */
																	obj_t BgL_val0_1144z00_1359;
																	long BgL_val1_1145z00_1360;

																	{	/* Rgc/rgcposix.scm 247 */
																		obj_t BgL_arg1371z00_1361;

																		BgL_arg1371z00_1361 =
																			BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																			(BgL_csetz00_1339, BNIL);
																		BgL_val0_1144z00_1359 =
																			MAKE_YOUNG_PAIR
																			(BGl_symbol1822z00zz__rgc_posixz00,
																			BgL_arg1371z00_1361);
																	}
																	BgL_val1_1145z00_1360 =
																		((long) CINT(BgL_iz00_1338) + 1L);
																	{	/* Rgc/rgcposix.scm 247 */
																		int BgL_tmpz00_2764;

																		BgL_tmpz00_2764 = (int) (2L);
																		BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2764);
																	}
																	{	/* Rgc/rgcposix.scm 247 */
																		obj_t BgL_auxz00_2769;
																		int BgL_tmpz00_2767;

																		BgL_auxz00_2769 =
																			BINT(BgL_val1_1145z00_1360);
																		BgL_tmpz00_2767 = (int) (1L);
																		BGL_MVALUES_VAL_SET(BgL_tmpz00_2767,
																			BgL_auxz00_2769);
																	}
																	return BgL_val0_1144z00_1359;
																}
														}
													break;
												case ((unsigned char) '-'):

													{	/* Rgc/rgcposix.scm 250 */
														bool_t BgL_test1888z00_2772;

														if (
															((long) CINT(BgL_iz00_1338) ==
																(long) CINT(BgL_i0z00_1335)))
															{	/* Rgc/rgcposix.scm 250 */
																BgL_test1888z00_2772 = ((bool_t) 1);
															}
														else
															{	/* Rgc/rgcposix.scm 250 */
																if (
																	(((long) CINT(BgL_iz00_1338) + 1L) <
																		BgL_lenz00_1332))
																	{	/* Rgc/rgcposix.scm 251 */
																		BgL_test1888z00_2772 =
																			(((unsigned char) ']') ==
																			STRING_REF(BgL_sz00_18,
																				((long) CINT(BgL_iz00_1338) + 1L)));
																	}
																else
																	{	/* Rgc/rgcposix.scm 251 */
																		BgL_test1888z00_2772 = ((bool_t) 0);
																	}
															}
														if (BgL_test1888z00_2772)
															{	/* Rgc/rgcposix.scm 253 */
																long BgL_arg1378z00_1368;
																obj_t BgL_arg1379z00_1369;

																BgL_arg1378z00_1368 =
																	((long) CINT(BgL_iz00_1338) + 1L);
																BgL_arg1379z00_1369 =
																	MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '-')),
																	BgL_csetz00_1339);
																{
																	obj_t BgL_csetz00_2791;
																	obj_t BgL_iz00_2789;

																	BgL_iz00_2789 = BINT(BgL_arg1378z00_1368);
																	BgL_csetz00_2791 = BgL_arg1379z00_1369;
																	BgL_csetz00_1339 = BgL_csetz00_2791;
																	BgL_iz00_1338 = BgL_iz00_2789;
																	goto BgL_zc3z04anonymousza31359ze3z87_1340;
																}
															}
														else
															{	/* Rgc/rgcposix.scm 250 */
																return
																	BGl_errorz00zz__errorz00(BFALSE,
																	BGl_string1824z00zz__rgc_posixz00,
																	BGl_za2errzd2stringza2zd2zz__rgc_posixz00);
															}
													}
													break;
												default:
													{	/* Rgc/rgcposix.scm 260 */
														long BgL_iz00_1375;

														BgL_iz00_1375 = ((long) CINT(BgL_iz00_1338) + 1L);
														{	/* Rgc/rgcposix.scm 261 */
															bool_t BgL_test1891z00_2795;

															if (((BgL_iz00_1375 + 1L) < BgL_lenz00_1332))
																{	/* Rgc/rgcposix.scm 261 */
																	BgL_test1891z00_2795 =
																		(((unsigned char) '-') ==
																		STRING_REF(BgL_sz00_18, BgL_iz00_1375));
																}
															else
																{	/* Rgc/rgcposix.scm 261 */
																	BgL_test1891z00_2795 = ((bool_t) 0);
																}
															if (BgL_test1891z00_2795)
																{	/* Rgc/rgcposix.scm 264 */
																	long BgL_iz00_1381;

																	BgL_iz00_1381 = (BgL_iz00_1375 + 1L);
																	{	/* Rgc/rgcposix.scm 264 */
																		long BgL_toz00_1382;

																		BgL_toz00_1382 =
																			(STRING_REF(BgL_sz00_18, BgL_iz00_1381));
																		{	/* Rgc/rgcposix.scm 265 */

																			{	/* Rgc/rgcposix.scm 266 */
																				long BgL_g1050z00_1383;

																				BgL_g1050z00_1383 = (BgL_cz00_1342);
																				{
																					long BgL_jz00_1988;
																					obj_t BgL_csetz00_1989;

																					BgL_jz00_1988 = BgL_g1050z00_1383;
																					BgL_csetz00_1989 = BgL_csetz00_1339;
																				BgL_laapz00_1987:
																					if ((BgL_jz00_1988 > BgL_toz00_1382))
																						{	/* Rgc/rgcposix.scm 269 */
																							long BgL_arg1392z00_1996;

																							BgL_arg1392z00_1996 =
																								(BgL_iz00_1381 + 1L);
																							{
																								obj_t BgL_csetz00_2810;
																								obj_t BgL_iz00_2808;

																								BgL_iz00_2808 =
																									BINT(BgL_arg1392z00_1996);
																								BgL_csetz00_2810 =
																									BgL_csetz00_1989;
																								BgL_csetz00_1339 =
																									BgL_csetz00_2810;
																								BgL_iz00_1338 = BgL_iz00_2808;
																								goto
																									BgL_zc3z04anonymousza31359ze3z87_1340;
																							}
																						}
																					else
																						{	/* Rgc/rgcposix.scm 271 */
																							long BgL_arg1393z00_1997;
																							obj_t BgL_arg1394z00_1998;

																							BgL_arg1393z00_1997 =
																								(BgL_jz00_1988 + 1L);
																							{	/* Rgc/rgcposix.scm 272 */
																								unsigned char
																									BgL_arg1395z00_1999;
																								BgL_arg1395z00_1999 =
																									(BgL_jz00_1988);
																								BgL_arg1394z00_1998 =
																									MAKE_YOUNG_PAIR(BCHAR
																									(BgL_arg1395z00_1999),
																									BgL_csetz00_1989);
																							}
																							{
																								obj_t BgL_csetz00_2816;
																								long BgL_jz00_2815;

																								BgL_jz00_2815 =
																									BgL_arg1393z00_1997;
																								BgL_csetz00_2816 =
																									BgL_arg1394z00_1998;
																								BgL_csetz00_1989 =
																									BgL_csetz00_2816;
																								BgL_jz00_1988 = BgL_jz00_2815;
																								goto BgL_laapz00_1987;
																							}
																						}
																				}
																			}
																		}
																	}
																}
															else
																{	/* Rgc/rgcposix.scm 275 */
																	obj_t BgL_arg1397z00_1395;

																	BgL_arg1397z00_1395 =
																		MAKE_YOUNG_PAIR(BCHAR(BgL_cz00_1342),
																		BgL_csetz00_1339);
																	{
																		obj_t BgL_csetz00_2821;
																		obj_t BgL_iz00_2819;

																		BgL_iz00_2819 = BINT(BgL_iz00_1375);
																		BgL_csetz00_2821 = BgL_arg1397z00_1395;
																		BgL_csetz00_1339 = BgL_csetz00_2821;
																		BgL_iz00_1338 = BgL_iz00_2819;
																		goto BgL_zc3z04anonymousza31359ze3z87_1340;
																	}
																}
														}
													}
												}
										}
									}
								else
									{	/* Rgc/rgcposix.scm 226 */
										return
											BGl_errorz00zz__errorz00(BFALSE,
											BGl_string1825z00zz__rgc_posixz00,
											BGl_za2errzd2stringza2zd2zz__rgc_posixz00);
									}
							}
						}
					}
				else
					{	/* Rgc/rgcposix.scm 218 */
						return
							BGl_errorz00zz__errorz00(BFALSE,
							BGl_string1825z00zz__rgc_posixz00,
							BGl_za2errzd2stringza2zd2zz__rgc_posixz00);
					}
			}
		}

	}



/* parse-posix-braces */
	obj_t BGl_parsezd2posixzd2bracesz00zz__rgc_posixz00(obj_t BgL_sz00_20,
		obj_t BgL_iz00_21)
	{
		{	/* Rgc/rgcposix.scm 285 */
			{	/* Rgc/rgcposix.scm 296 */
				obj_t BgL_commaz00_1407;
				obj_t BgL_rbz00_1408;

				{	/* Rgc/rgcposix.scm 287 */
					long BgL_lenz00_2041;

					BgL_lenz00_2041 = STRING_LENGTH(BgL_sz00_20);
					{
						obj_t BgL_offsetz00_2043;

						BgL_offsetz00_2043 = BgL_iz00_21;
					BgL_loopz00_2042:
						if (((long) CINT(BgL_offsetz00_2043) >= BgL_lenz00_2041))
							{	/* Rgc/rgcposix.scm 290 */
								BgL_commaz00_1407 = BFALSE;
							}
						else
							{	/* Rgc/rgcposix.scm 290 */
								if (
									(STRING_REF(BgL_sz00_20,
											(long) CINT(BgL_offsetz00_2043)) ==
										((unsigned char) ',')))
									{	/* Rgc/rgcposix.scm 292 */
										BgL_commaz00_1407 = BgL_offsetz00_2043;
									}
								else
									{
										obj_t BgL_offsetz00_2833;

										BgL_offsetz00_2833 = ADDFX(BgL_offsetz00_2043, BINT(1L));
										BgL_offsetz00_2043 = BgL_offsetz00_2833;
										goto BgL_loopz00_2042;
									}
							}
					}
				}
				{	/* Rgc/rgcposix.scm 287 */
					long BgL_lenz00_2056;

					BgL_lenz00_2056 = STRING_LENGTH(BgL_sz00_20);
					{
						obj_t BgL_offsetz00_2058;

						BgL_offsetz00_2058 = BgL_iz00_21;
					BgL_loopz00_2057:
						if (((long) CINT(BgL_offsetz00_2058) >= BgL_lenz00_2056))
							{	/* Rgc/rgcposix.scm 290 */
								BgL_rbz00_1408 = BFALSE;
							}
						else
							{	/* Rgc/rgcposix.scm 290 */
								if (
									(STRING_REF(BgL_sz00_20,
											(long) CINT(BgL_offsetz00_2058)) ==
										((unsigned char) '}')))
									{	/* Rgc/rgcposix.scm 292 */
										BgL_rbz00_1408 = BgL_offsetz00_2058;
									}
								else
									{
										obj_t BgL_offsetz00_2844;

										BgL_offsetz00_2844 = ADDFX(BgL_offsetz00_2058, BINT(1L));
										BgL_offsetz00_2058 = BgL_offsetz00_2844;
										goto BgL_loopz00_2057;
									}
							}
					}
				}
				if (CBOOL(BgL_rbz00_1408))
					{	/* Rgc/rgcposix.scm 299 */
						bool_t BgL_test1899z00_2849;

						if (CBOOL(BgL_commaz00_1407))
							{	/* Rgc/rgcposix.scm 299 */
								BgL_test1899z00_2849 =
									(
									(long) CINT(BgL_commaz00_1407) < (long) CINT(BgL_rbz00_1408));
							}
						else
							{	/* Rgc/rgcposix.scm 299 */
								BgL_test1899z00_2849 = ((bool_t) 0);
							}
						if (BgL_test1899z00_2849)
							{	/* Rgc/rgcposix.scm 300 */
								obj_t BgL_val0_1146z00_1410;
								obj_t BgL_val1_1147z00_1411;
								long BgL_val2_1148z00_1412;

								{	/* Rgc/rgcposix.scm 300 */
									obj_t BgL_arg1403z00_1413;

									BgL_arg1403z00_1413 =
										c_substring(BgL_sz00_20,
										(long) CINT(BgL_iz00_21), (long) CINT(BgL_commaz00_1407));
									{	/* Ieee/number.scm 175 */

										BgL_val0_1146z00_1410 =
											BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00
											(BgL_arg1403z00_1413, BINT(10L));
								}}
								if (
									(((long) CINT(BgL_commaz00_1407) + 1L) ==
										(long) CINT(BgL_rbz00_1408)))
									{	/* Rgc/rgcposix.scm 301 */
										BgL_val1_1147z00_1411 = BFALSE;
									}
								else
									{	/* Rgc/rgcposix.scm 302 */
										obj_t BgL_arg1405z00_1417;

										BgL_arg1405z00_1417 =
											c_substring(BgL_sz00_20,
											((long) CINT(BgL_commaz00_1407) + 1L),
											(long) CINT(BgL_rbz00_1408));
										{	/* Ieee/number.scm 175 */

											BgL_val1_1147z00_1411 =
												BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00
												(BgL_arg1405z00_1417, BINT(10L));
									}}
								BgL_val2_1148z00_1412 = ((long) CINT(BgL_rbz00_1408) + 1L);
								{	/* Rgc/rgcposix.scm 300 */
									int BgL_tmpz00_2873;

									BgL_tmpz00_2873 = (int) (3L);
									BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2873);
								}
								{	/* Rgc/rgcposix.scm 300 */
									int BgL_tmpz00_2876;

									BgL_tmpz00_2876 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_2876, BgL_val1_1147z00_1411);
								}
								{	/* Rgc/rgcposix.scm 300 */
									obj_t BgL_auxz00_2881;
									int BgL_tmpz00_2879;

									BgL_auxz00_2881 = BINT(BgL_val2_1148z00_1412);
									BgL_tmpz00_2879 = (int) (2L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_2879, BgL_auxz00_2881);
								}
								return BgL_val0_1146z00_1410;
							}
						else
							{	/* Rgc/rgcposix.scm 304 */
								obj_t BgL_mz00_1422;

								{	/* Rgc/rgcposix.scm 304 */
									obj_t BgL_arg1408z00_1426;

									BgL_arg1408z00_1426 =
										c_substring(BgL_sz00_20,
										(long) CINT(BgL_iz00_21), (long) CINT(BgL_rbz00_1408));
									{	/* Ieee/number.scm 175 */

										BgL_mz00_1422 =
											BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00
											(BgL_arg1408z00_1426, BINT(10L));
								}}
								{	/* Rgc/rgcposix.scm 305 */
									long BgL_val2_1151z00_1425;

									BgL_val2_1151z00_1425 = ((long) CINT(BgL_rbz00_1408) + 1L);
									{	/* Rgc/rgcposix.scm 305 */
										int BgL_tmpz00_2891;

										BgL_tmpz00_2891 = (int) (3L);
										BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2891);
									}
									{	/* Rgc/rgcposix.scm 305 */
										int BgL_tmpz00_2894;

										BgL_tmpz00_2894 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_2894, BgL_mz00_1422);
									}
									{	/* Rgc/rgcposix.scm 305 */
										obj_t BgL_auxz00_2899;
										int BgL_tmpz00_2897;

										BgL_auxz00_2899 = BINT(BgL_val2_1151z00_1425);
										BgL_tmpz00_2897 = (int) (2L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_2897, BgL_auxz00_2899);
									}
									return BgL_mz00_1422;
								}
							}
					}
				else
					{	/* Rgc/rgcposix.scm 298 */
						return
							BGl_errorz00zz__errorz00(BFALSE,
							BGl_string1826z00zz__rgc_posixz00,
							BGl_za2errzd2stringza2zd2zz__rgc_posixz00);
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__rgc_posixz00(void)
	{
		{	/* Rgc/rgcposix.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__rgc_posixz00(void)
	{
		{	/* Rgc/rgcposix.scm 14 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__rgc_posixz00(void)
	{
		{	/* Rgc/rgcposix.scm 14 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__rgc_posixz00(void)
	{
		{	/* Rgc/rgcposix.scm 14 */
			return
				BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1827z00zz__rgc_posixz00));
		}

	}

#ifdef __cplusplus
}
#endif
