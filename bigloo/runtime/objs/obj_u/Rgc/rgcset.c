/*===========================================================================*/
/*   (Rgc/rgcset.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Rgc/rgcset.scm -indent -o objs/obj_u/Rgc/rgcset.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___RGC_SET_TYPE_DEFINITIONS
#define BGL___RGC_SET_TYPE_DEFINITIONS
#endif													// BGL___RGC_SET_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_symbol1752z00zz__rgc_setz00 = BUNSPEC;
	static obj_t BGl_z62rgcsetzd2andz12za2zz__rgc_setz00(obj_t, obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__rgc_setz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_forzd2eachzd2rgcsetz00zz__rgc_setz00(obj_t,
		obj_t);
	static obj_t BGl_z62forzd2eachzd2rgcsetz62zz__rgc_setz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62rgcsetzd2butz12za2zz__rgc_setz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_rgcsetzd2notz12zc0zz__rgc_setz00(obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__rgc_setz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_z62rgcsetzd2lengthzb0zz__rgc_setz00(obj_t, obj_t);
	static obj_t BGl_z62rgcsetzd2ze3listz53zz__rgc_setz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_rgcsetzd2addz12zc0zz__rgc_setz00(obj_t, long);
	static obj_t BGl_toplevelzd2initzd2zz__rgc_setz00(void);
	BGL_EXPORTED_DECL obj_t BGl_rgcsetzd2removez12zc0zz__rgc_setz00(obj_t, long);
	extern obj_t create_struct(obj_t, int);
	BGL_EXPORTED_DECL obj_t BGl_rgcsetzd2ze3listz31zz__rgc_setz00(obj_t);
	static obj_t BGl_z62rgcsetzd2memberzf3z43zz__rgc_setz00(obj_t, obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__rgc_setz00(void);
	static long BGl_bitzd2perzd2wordz00zz__rgc_setz00 = 0L;
	static obj_t BGl_genericzd2initzd2zz__rgc_setz00(void);
	BGL_EXPORTED_DECL obj_t BGl_rgcsetzd2notzd2zz__rgc_setz00(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__rgc_setz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__rgc_setz00(void);
	static obj_t BGl_objectzd2initzd2zz__rgc_setz00(void);
	static obj_t BGl_z62rgcsetzd2ze3hashz53zz__rgc_setz00(obj_t, obj_t);
	extern obj_t BGl_2zd2zd2zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62makezd2rgcsetzb0zz__rgc_setz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_rgcsetzd2andz12zc0zz__rgc_setz00(obj_t, obj_t);
	static obj_t BGl_z62rgcsetzd2notzb0zz__rgc_setz00(obj_t, obj_t);
	BGL_EXPORTED_DECL long BGl_rgcsetzd2ze3hashz31zz__rgc_setz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_rgcsetzd2orz12zc0zz__rgc_setz00(obj_t, obj_t);
	static obj_t BGl_z62rgcsetzd2orzb0zz__rgc_setz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62listzd2ze3rgcsetz53zz__rgc_setz00(obj_t, obj_t, obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_rgcsetzd2butz12zc0zz__rgc_setz00(obj_t, obj_t);
	extern obj_t make_vector(long, obj_t);
	static obj_t BGl_z62rgcsetzd2notz12za2zz__rgc_setz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31307ze3ze5zz__rgc_setz00(obj_t, obj_t);
	static obj_t BGl_z62rgcsetzd2orz12za2zz__rgc_setz00(obj_t, obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zz__rgc_setz00(void);
	BGL_EXPORTED_DECL bool_t BGl_rgcsetzd2memberzf3z21zz__rgc_setz00(obj_t, long);
	BGL_EXPORTED_DECL bool_t BGl_rgcsetzd2equalzf3z21zz__rgc_setz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_listzd2ze3rgcsetz31zz__rgc_setz00(obj_t, long);
	static obj_t BGl_z62rgcsetzd2addz12za2zz__rgc_setz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_rgcsetzd2orzd2zz__rgc_setz00(obj_t, obj_t);
	BGL_EXPORTED_DECL long BGl_rgcsetzd2lengthzd2zz__rgc_setz00(obj_t);
	static obj_t BGl_z62rgcsetzd2removez12za2zz__rgc_setz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62rgcsetzd2equalzf3z43zz__rgc_setz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_makezd2rgcsetzd2zz__rgc_setz00(long);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgcsetzd2ze3listzd2envze3zz__rgc_setz00,
		BgL_bgl_za762rgcsetza7d2za7e3l1764za7,
		BGl_z62rgcsetzd2ze3listz53zz__rgc_setz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgcsetzd2orzd2envz00zz__rgc_setz00,
		BgL_bgl_za762rgcsetza7d2orza7b1765za7, BGl_z62rgcsetzd2orzb0zz__rgc_setz00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgcsetzd2ze3hashzd2envze3zz__rgc_setz00,
		BgL_bgl_za762rgcsetza7d2za7e3h1766za7,
		BGl_z62rgcsetzd2ze3hashz53zz__rgc_setz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgcsetzd2orz12zd2envz12zz__rgc_setz00,
		BgL_bgl_za762rgcsetza7d2orza711767za7,
		BGl_z62rgcsetzd2orz12za2zz__rgc_setz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgcsetzd2removez12zd2envz12zz__rgc_setz00,
		BgL_bgl_za762rgcsetza7d2remo1768z00,
		BGl_z62rgcsetzd2removez12za2zz__rgc_setz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_listzd2ze3rgcsetzd2envze3zz__rgc_setz00,
		BgL_bgl_za762listza7d2za7e3rgc1769za7,
		BGl_z62listzd2ze3rgcsetz53zz__rgc_setz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_forzd2eachzd2rgcsetzd2envzd2zz__rgc_setz00,
		BgL_bgl_za762forza7d2eachza7d21770za7,
		BGl_z62forzd2eachzd2rgcsetz62zz__rgc_setz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgcsetzd2lengthzd2envz00zz__rgc_setz00,
		BgL_bgl_za762rgcsetza7d2leng1771z00,
		BGl_z62rgcsetzd2lengthzb0zz__rgc_setz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgcsetzd2equalzf3zd2envzf3zz__rgc_setz00,
		BgL_bgl_za762rgcsetza7d2equa1772z00,
		BGl_z62rgcsetzd2equalzf3z43zz__rgc_setz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgcsetzd2memberzf3zd2envzf3zz__rgc_setz00,
		BgL_bgl_za762rgcsetza7d2memb1773z00,
		BGl_z62rgcsetzd2memberzf3z43zz__rgc_setz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rgcsetzd2envz00zz__rgc_setz00,
		BgL_bgl_za762makeza7d2rgcset1774z00, BGl_z62makezd2rgcsetzb0zz__rgc_setz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgcsetzd2andz12zd2envz12zz__rgc_setz00,
		BgL_bgl_za762rgcsetza7d2andza71775za7,
		BGl_z62rgcsetzd2andz12za2zz__rgc_setz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1753z00zz__rgc_setz00,
		BgL_bgl_string1753za700za7za7_1776za7, "__rgcset", 8);
	      DEFINE_STRING(BGl_string1754z00zz__rgc_setz00,
		BgL_bgl_string1754za700za7za7_1777za7, "/tmp/bigloo/runtime/Rgc/rgcset.scm",
		34);
	      DEFINE_STRING(BGl_string1755z00zz__rgc_setz00,
		BgL_bgl_string1755za700za7za7_1778za7, "&make-rgcset", 12);
	      DEFINE_STRING(BGl_string1756z00zz__rgc_setz00,
		BgL_bgl_string1756za700za7za7_1779za7, "bint", 4);
	      DEFINE_STRING(BGl_string1757z00zz__rgc_setz00,
		BgL_bgl_string1757za700za7za7_1780za7, "&rgcset-add!", 12);
	      DEFINE_STRING(BGl_string1758z00zz__rgc_setz00,
		BgL_bgl_string1758za700za7za7_1781za7, "&rgcset-member?", 15);
	      DEFINE_STRING(BGl_string1759z00zz__rgc_setz00,
		BgL_bgl_string1759za700za7za7_1782za7, "&list->rgcset", 13);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgcsetzd2butz12zd2envz12zz__rgc_setz00,
		BgL_bgl_za762rgcsetza7d2butza71783za7,
		BGl_z62rgcsetzd2butz12za2zz__rgc_setz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgcsetzd2notzd2envz00zz__rgc_setz00,
		BgL_bgl_za762rgcsetza7d2notza71784za7, BGl_z62rgcsetzd2notzb0zz__rgc_setz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgcsetzd2notz12zd2envz12zz__rgc_setz00,
		BgL_bgl_za762rgcsetza7d2notza71785za7,
		BGl_z62rgcsetzd2notz12za2zz__rgc_setz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1760z00zz__rgc_setz00,
		BgL_bgl_string1760za700za7za7_1786za7, "&for-each-rgcset", 16);
	      DEFINE_STRING(BGl_string1761z00zz__rgc_setz00,
		BgL_bgl_string1761za700za7za7_1787za7, "procedure", 9);
	      DEFINE_STRING(BGl_string1762z00zz__rgc_setz00,
		BgL_bgl_string1762za700za7za7_1788za7, "&rgcset-remove!", 15);
	      DEFINE_STRING(BGl_string1763z00zz__rgc_setz00,
		BgL_bgl_string1763za700za7za7_1789za7, "__rgc_set", 9);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgcsetzd2addz12zd2envz12zz__rgc_setz00,
		BgL_bgl_za762rgcsetza7d2addza71790za7,
		BGl_z62rgcsetzd2addz12za2zz__rgc_setz00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_symbol1752z00zz__rgc_setz00));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__rgc_setz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__rgc_setz00(long
		BgL_checksumz00_2634, char *BgL_fromz00_2635)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__rgc_setz00))
				{
					BGl_requirezd2initializa7ationz75zz__rgc_setz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__rgc_setz00();
					BGl_cnstzd2initzd2zz__rgc_setz00();
					BGl_importedzd2moduleszd2initz00zz__rgc_setz00();
					return BGl_toplevelzd2initzd2zz__rgc_setz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__rgc_setz00(void)
	{
		{	/* Rgc/rgcset.scm 15 */
			return (BGl_symbol1752z00zz__rgc_setz00 =
				bstring_to_symbol(BGl_string1753z00zz__rgc_setz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__rgc_setz00(void)
	{
		{	/* Rgc/rgcset.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__rgc_setz00(void)
	{
		{	/* Rgc/rgcset.scm 15 */
			BGl_bitzd2perzd2wordz00zz__rgc_setz00 = (BGL_INT_BIT_SIZE - 1L);
			return BUNSPEC;
		}

	}



/* make-rgcset */
	BGL_EXPORTED_DEF obj_t BGl_makezd2rgcsetzd2zz__rgc_setz00(long
		BgL_siza7eza7_13)
	{
		{	/* Rgc/rgcset.scm 84 */
			{	/* Rgc/rgcset.scm 85 */
				obj_t BgL_arg1209z00_1229;

				{	/* Rgc/rgcset.scm 85 */
					long BgL_arg1210z00_1230;

					{	/* Rgc/rgcset.scm 85 */
						long BgL_b1092z00_1232;

						BgL_b1092z00_1232 =
							(BgL_siza7eza7_13 / BGl_bitzd2perzd2wordz00zz__rgc_setz00);
						{	/* Rgc/rgcset.scm 85 */

							BgL_arg1210z00_1230 = (1L + BgL_b1092z00_1232);
					}}
					BgL_arg1209z00_1229 = make_vector(BgL_arg1210z00_1230, BINT(0L));
				}
				{	/* Rgc/rgcset.scm 79 */
					obj_t BgL_newz00_1876;

					BgL_newz00_1876 =
						create_struct(BGl_symbol1752z00zz__rgc_setz00, (int) (2L));
					{	/* Rgc/rgcset.scm 79 */
						int BgL_tmpz00_2652;

						BgL_tmpz00_2652 = (int) (1L);
						STRUCT_SET(BgL_newz00_1876, BgL_tmpz00_2652, BgL_arg1209z00_1229);
					}
					{	/* Rgc/rgcset.scm 79 */
						obj_t BgL_auxz00_2657;
						int BgL_tmpz00_2655;

						BgL_auxz00_2657 = BINT(BgL_siza7eza7_13);
						BgL_tmpz00_2655 = (int) (0L);
						STRUCT_SET(BgL_newz00_1876, BgL_tmpz00_2655, BgL_auxz00_2657);
					}
					return BgL_newz00_1876;
				}
			}
		}

	}



/* &make-rgcset */
	obj_t BGl_z62makezd2rgcsetzb0zz__rgc_setz00(obj_t BgL_envz00_2574,
		obj_t BgL_siza7eza7_2575)
	{
		{	/* Rgc/rgcset.scm 84 */
			{	/* Rgc/rgcset.scm 85 */
				long BgL_auxz00_2660;

				{	/* Rgc/rgcset.scm 85 */
					obj_t BgL_tmpz00_2661;

					if (INTEGERP(BgL_siza7eza7_2575))
						{	/* Rgc/rgcset.scm 85 */
							BgL_tmpz00_2661 = BgL_siza7eza7_2575;
						}
					else
						{
							obj_t BgL_auxz00_2664;

							BgL_auxz00_2664 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1754z00zz__rgc_setz00,
								BINT(3274L), BGl_string1755z00zz__rgc_setz00,
								BGl_string1756z00zz__rgc_setz00, BgL_siza7eza7_2575);
							FAILURE(BgL_auxz00_2664, BFALSE, BFALSE);
						}
					BgL_auxz00_2660 = (long) CINT(BgL_tmpz00_2661);
				}
				return BGl_makezd2rgcsetzd2zz__rgc_setz00(BgL_auxz00_2660);
			}
		}

	}



/* rgcset-add! */
	BGL_EXPORTED_DEF obj_t BGl_rgcsetzd2addz12zc0zz__rgc_setz00(obj_t
		BgL_rgcsetz00_20, long BgL_numz00_21)
	{
		{	/* Rgc/rgcset.scm 108 */
			{	/* Rgc/rgcset.scm 109 */
				long BgL_wordzd2numzd2_1890;

				BgL_wordzd2numzd2_1890 =
					(BgL_numz00_21 / BGl_bitzd2perzd2wordz00zz__rgc_setz00);
				{	/* Rgc/rgcset.scm 109 */
					long BgL_wordzd2bitzd2_1891;

					{	/* Rgc/rgcset.scm 110 */
						long BgL_n1z00_1897;
						long BgL_n2z00_1898;

						BgL_n1z00_1897 = BgL_numz00_21;
						BgL_n2z00_1898 = BGl_bitzd2perzd2wordz00zz__rgc_setz00;
						{	/* Rgc/rgcset.scm 110 */
							bool_t BgL_test1793z00_2671;

							{	/* Rgc/rgcset.scm 110 */
								long BgL_arg1557z00_1900;

								BgL_arg1557z00_1900 =
									(((BgL_n1z00_1897) | (BgL_n2z00_1898)) & -2147483648);
								BgL_test1793z00_2671 = (BgL_arg1557z00_1900 == 0L);
							}
							if (BgL_test1793z00_2671)
								{	/* Rgc/rgcset.scm 110 */
									int32_t BgL_arg1554z00_1901;

									{	/* Rgc/rgcset.scm 110 */
										int32_t BgL_arg1555z00_1902;
										int32_t BgL_arg1556z00_1903;

										BgL_arg1555z00_1902 = (int32_t) (BgL_n1z00_1897);
										BgL_arg1556z00_1903 = (int32_t) (BgL_n2z00_1898);
										BgL_arg1554z00_1901 =
											(BgL_arg1555z00_1902 % BgL_arg1556z00_1903);
									}
									{	/* Rgc/rgcset.scm 110 */
										long BgL_arg1692z00_1908;

										BgL_arg1692z00_1908 = (long) (BgL_arg1554z00_1901);
										BgL_wordzd2bitzd2_1891 = (long) (BgL_arg1692z00_1908);
								}}
							else
								{	/* Rgc/rgcset.scm 110 */
									BgL_wordzd2bitzd2_1891 = (BgL_n1z00_1897 % BgL_n2z00_1898);
								}
						}
					}
					{	/* Rgc/rgcset.scm 110 */
						obj_t BgL_oldz00_1892;

						{	/* Rgc/rgcset.scm 97 */
							obj_t BgL_arg1215z00_1910;

							BgL_arg1215z00_1910 =
								STRUCT_REF(((obj_t) BgL_rgcsetz00_20), (int) (1L));
							BgL_oldz00_1892 =
								VECTOR_REF(
								((obj_t) BgL_arg1215z00_1910), BgL_wordzd2numzd2_1890);
						}
						{	/* Rgc/rgcset.scm 111 */

							{	/* Rgc/rgcset.scm 114 */
								long BgL_arg1218z00_1893;

								BgL_arg1218z00_1893 =
									(
									(long) CINT(BgL_oldz00_1892) |
									(1L << (int) (BgL_wordzd2bitzd2_1891)));
								{	/* Rgc/rgcset.scm 91 */
									obj_t BgL_arg1212z00_1917;

									BgL_arg1212z00_1917 =
										STRUCT_REF(((obj_t) BgL_rgcsetz00_20), (int) (1L));
									return
										VECTOR_SET(
										((obj_t) BgL_arg1212z00_1917), BgL_wordzd2numzd2_1890,
										BINT(BgL_arg1218z00_1893));
								}
							}
						}
					}
				}
			}
		}

	}



/* &rgcset-add! */
	obj_t BGl_z62rgcsetzd2addz12za2zz__rgc_setz00(obj_t BgL_envz00_2576,
		obj_t BgL_rgcsetz00_2577, obj_t BgL_numz00_2578)
	{
		{	/* Rgc/rgcset.scm 108 */
			{	/* Rgc/rgcset.scm 109 */
				long BgL_auxz00_2695;

				{	/* Rgc/rgcset.scm 109 */
					obj_t BgL_tmpz00_2696;

					if (INTEGERP(BgL_numz00_2578))
						{	/* Rgc/rgcset.scm 109 */
							BgL_tmpz00_2696 = BgL_numz00_2578;
						}
					else
						{
							obj_t BgL_auxz00_2699;

							BgL_auxz00_2699 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1754z00zz__rgc_setz00,
								BINT(4509L), BGl_string1757z00zz__rgc_setz00,
								BGl_string1756z00zz__rgc_setz00, BgL_numz00_2578);
							FAILURE(BgL_auxz00_2699, BFALSE, BFALSE);
						}
					BgL_auxz00_2695 = (long) CINT(BgL_tmpz00_2696);
				}
				return
					BGl_rgcsetzd2addz12zc0zz__rgc_setz00(BgL_rgcsetz00_2577,
					BgL_auxz00_2695);
			}
		}

	}



/* rgcset-member? */
	BGL_EXPORTED_DEF bool_t BGl_rgcsetzd2memberzf3z21zz__rgc_setz00(obj_t
		BgL_setz00_22, long BgL_numz00_23)
	{
		{	/* Rgc/rgcset.scm 119 */
			{	/* Rgc/rgcset.scm 120 */
				long BgL_wordzd2numzd2_1921;

				BgL_wordzd2numzd2_1921 =
					(BgL_numz00_23 / BGl_bitzd2perzd2wordz00zz__rgc_setz00);
				{	/* Rgc/rgcset.scm 120 */
					long BgL_wordzd2bitzd2_1922;

					{	/* Rgc/rgcset.scm 121 */
						long BgL_n1z00_1928;
						long BgL_n2z00_1929;

						BgL_n1z00_1928 = BgL_numz00_23;
						BgL_n2z00_1929 = BGl_bitzd2perzd2wordz00zz__rgc_setz00;
						{	/* Rgc/rgcset.scm 121 */
							bool_t BgL_test1795z00_2706;

							{	/* Rgc/rgcset.scm 121 */
								long BgL_arg1557z00_1931;

								BgL_arg1557z00_1931 =
									(((BgL_n1z00_1928) | (BgL_n2z00_1929)) & -2147483648);
								BgL_test1795z00_2706 = (BgL_arg1557z00_1931 == 0L);
							}
							if (BgL_test1795z00_2706)
								{	/* Rgc/rgcset.scm 121 */
									int32_t BgL_arg1554z00_1932;

									{	/* Rgc/rgcset.scm 121 */
										int32_t BgL_arg1555z00_1933;
										int32_t BgL_arg1556z00_1934;

										BgL_arg1555z00_1933 = (int32_t) (BgL_n1z00_1928);
										BgL_arg1556z00_1934 = (int32_t) (BgL_n2z00_1929);
										BgL_arg1554z00_1932 =
											(BgL_arg1555z00_1933 % BgL_arg1556z00_1934);
									}
									{	/* Rgc/rgcset.scm 121 */
										long BgL_arg1692z00_1939;

										BgL_arg1692z00_1939 = (long) (BgL_arg1554z00_1932);
										BgL_wordzd2bitzd2_1922 = (long) (BgL_arg1692z00_1939);
								}}
							else
								{	/* Rgc/rgcset.scm 121 */
									BgL_wordzd2bitzd2_1922 = (BgL_n1z00_1928 % BgL_n2z00_1929);
								}
						}
					}
					{	/* Rgc/rgcset.scm 121 */
						obj_t BgL_oldz00_1923;

						{	/* Rgc/rgcset.scm 97 */
							obj_t BgL_arg1215z00_1941;

							BgL_arg1215z00_1941 =
								STRUCT_REF(((obj_t) BgL_setz00_22), (int) (1L));
							BgL_oldz00_1923 =
								VECTOR_REF(
								((obj_t) BgL_arg1215z00_1941), BgL_wordzd2numzd2_1921);
						}
						{	/* Rgc/rgcset.scm 122 */
							long BgL_maskz00_1924;

							BgL_maskz00_1924 = (1L << (int) (BgL_wordzd2bitzd2_1922));
							{	/* Rgc/rgcset.scm 123 */

								return
									(BgL_maskz00_1924 ==
									((long) CINT(BgL_oldz00_1923) & BgL_maskz00_1924));
		}}}}}}

	}



/* &rgcset-member? */
	obj_t BGl_z62rgcsetzd2memberzf3z43zz__rgc_setz00(obj_t BgL_envz00_2579,
		obj_t BgL_setz00_2580, obj_t BgL_numz00_2581)
	{
		{	/* Rgc/rgcset.scm 119 */
			{	/* Rgc/rgcset.scm 120 */
				bool_t BgL_tmpz00_2725;

				{	/* Rgc/rgcset.scm 120 */
					long BgL_auxz00_2726;

					{	/* Rgc/rgcset.scm 120 */
						obj_t BgL_tmpz00_2727;

						if (INTEGERP(BgL_numz00_2581))
							{	/* Rgc/rgcset.scm 120 */
								BgL_tmpz00_2727 = BgL_numz00_2581;
							}
						else
							{
								obj_t BgL_auxz00_2730;

								BgL_auxz00_2730 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1754z00zz__rgc_setz00, BINT(4992L),
									BGl_string1758z00zz__rgc_setz00,
									BGl_string1756z00zz__rgc_setz00, BgL_numz00_2581);
								FAILURE(BgL_auxz00_2730, BFALSE, BFALSE);
							}
						BgL_auxz00_2726 = (long) CINT(BgL_tmpz00_2727);
					}
					BgL_tmpz00_2725 =
						BGl_rgcsetzd2memberzf3z21zz__rgc_setz00(BgL_setz00_2580,
						BgL_auxz00_2726);
				}
				return BBOOL(BgL_tmpz00_2725);
			}
		}

	}



/* list->rgcset */
	BGL_EXPORTED_DEF obj_t BGl_listzd2ze3rgcsetz31zz__rgc_setz00(obj_t
		BgL_lstz00_24, long BgL_maxz00_25)
	{
		{	/* Rgc/rgcset.scm 129 */
			{	/* Rgc/rgcset.scm 130 */
				obj_t BgL_setz00_1950;

				BgL_setz00_1950 = BGl_makezd2rgcsetzd2zz__rgc_setz00(BgL_maxz00_25);
				{
					obj_t BgL_l1093z00_1999;

					BgL_l1093z00_1999 = BgL_lstz00_24;
				BgL_zc3z04anonymousza31221ze3z87_1998:
					if (PAIRP(BgL_l1093z00_1999))
						{	/* Rgc/rgcset.scm 131 */
							{	/* Rgc/rgcset.scm 131 */
								obj_t BgL_itemz00_2004;

								BgL_itemz00_2004 = CAR(BgL_l1093z00_1999);
								BGl_rgcsetzd2addz12zc0zz__rgc_setz00(BgL_setz00_1950,
									(long) CINT(BgL_itemz00_2004));
							}
							{
								obj_t BgL_l1093z00_2743;

								BgL_l1093z00_2743 = CDR(BgL_l1093z00_1999);
								BgL_l1093z00_1999 = BgL_l1093z00_2743;
								goto BgL_zc3z04anonymousza31221ze3z87_1998;
							}
						}
					else
						{	/* Rgc/rgcset.scm 131 */
							((bool_t) 1);
						}
				}
				return BgL_setz00_1950;
			}
		}

	}



/* &list->rgcset */
	obj_t BGl_z62listzd2ze3rgcsetz53zz__rgc_setz00(obj_t BgL_envz00_2582,
		obj_t BgL_lstz00_2583, obj_t BgL_maxz00_2584)
	{
		{	/* Rgc/rgcset.scm 129 */
			{	/* Rgc/rgcset.scm 130 */
				long BgL_auxz00_2745;

				{	/* Rgc/rgcset.scm 130 */
					obj_t BgL_tmpz00_2746;

					if (INTEGERP(BgL_maxz00_2584))
						{	/* Rgc/rgcset.scm 130 */
							BgL_tmpz00_2746 = BgL_maxz00_2584;
						}
					else
						{
							obj_t BgL_auxz00_2749;

							BgL_auxz00_2749 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1754z00zz__rgc_setz00,
								BINT(5454L), BGl_string1759z00zz__rgc_setz00,
								BGl_string1756z00zz__rgc_setz00, BgL_maxz00_2584);
							FAILURE(BgL_auxz00_2749, BFALSE, BFALSE);
						}
					BgL_auxz00_2745 = (long) CINT(BgL_tmpz00_2746);
				}
				return
					BGl_listzd2ze3rgcsetz31zz__rgc_setz00(BgL_lstz00_2583,
					BgL_auxz00_2745);
			}
		}

	}



/* rgcset->list */
	BGL_EXPORTED_DEF obj_t BGl_rgcsetzd2ze3listz31zz__rgc_setz00(obj_t
		BgL_setz00_26)
	{
		{	/* Rgc/rgcset.scm 137 */
			{	/* Rgc/rgcset.scm 138 */
				obj_t BgL_maxz00_1255;
				long BgL_maxmaskz00_1256;

				BgL_maxz00_1255 = STRUCT_REF(((obj_t) BgL_setz00_26), (int) (0L));
				{	/* Rgc/rgcset.scm 139 */
					long BgL_yz00_2009;

					BgL_yz00_2009 = BGl_bitzd2perzd2wordz00zz__rgc_setz00;
					BgL_maxmaskz00_1256 = (1L << (int) (BgL_yz00_2009));
				}
				{	/* Rgc/rgcset.scm 140 */
					obj_t BgL_g1039z00_1257;

					{	/* Rgc/rgcset.scm 97 */
						obj_t BgL_arg1215z00_2010;

						BgL_arg1215z00_2010 =
							STRUCT_REF(((obj_t) BgL_setz00_26), (int) (1L));
						BgL_g1039z00_1257 = VECTOR_REF(((obj_t) BgL_arg1215z00_2010), 0L);
					}
					{
						long BgL_iz00_2032;
						long BgL_wordzd2numzd2_2033;
						obj_t BgL_wordz00_2034;
						long BgL_maskz00_2035;
						obj_t BgL_resz00_2036;

						BgL_iz00_2032 = 0L;
						BgL_wordzd2numzd2_2033 = 0L;
						BgL_wordz00_2034 = BgL_g1039z00_1257;
						BgL_maskz00_2035 = 1L;
						BgL_resz00_2036 = BNIL;
					BgL_loopz00_2031:
						if ((BgL_iz00_2032 == (long) CINT(BgL_maxz00_1255)))
							{	/* Rgc/rgcset.scm 146 */
								return BgL_resz00_2036;
							}
						else
							{	/* Rgc/rgcset.scm 146 */
								if ((BgL_maskz00_2035 == BgL_maxmaskz00_1256))
									{	/* Rgc/rgcset.scm 150 */
										long BgL_arg1227z00_2043;
										obj_t BgL_arg1228z00_2044;

										BgL_arg1227z00_2043 = (BgL_wordzd2numzd2_2033 + 1L);
										{	/* Rgc/rgcset.scm 151 */
											long BgL_arg1229z00_2046;

											BgL_arg1229z00_2046 = (BgL_wordzd2numzd2_2033 + 1L);
											{	/* Rgc/rgcset.scm 97 */
												obj_t BgL_arg1215z00_2048;

												BgL_arg1215z00_2048 =
													STRUCT_REF(((obj_t) BgL_setz00_26), (int) (1L));
												BgL_arg1228z00_2044 =
													VECTOR_REF(
													((obj_t) BgL_arg1215z00_2048), BgL_arg1229z00_2046);
										}}
										{
											long BgL_maskz00_2779;
											obj_t BgL_wordz00_2778;
											long BgL_wordzd2numzd2_2777;

											BgL_wordzd2numzd2_2777 = BgL_arg1227z00_2043;
											BgL_wordz00_2778 = BgL_arg1228z00_2044;
											BgL_maskz00_2779 = 1L;
											BgL_maskz00_2035 = BgL_maskz00_2779;
											BgL_wordz00_2034 = BgL_wordz00_2778;
											BgL_wordzd2numzd2_2033 = BgL_wordzd2numzd2_2777;
											goto BgL_loopz00_2031;
										}
									}
								else
									{	/* Rgc/rgcset.scm 148 */
										if (
											(((long) CINT(BgL_wordz00_2034) & BgL_maskz00_2035) ==
												BgL_maskz00_2035))
											{	/* Rgc/rgcset.scm 155 */
												long BgL_arg1232z00_2058;
												long BgL_arg1233z00_2059;
												obj_t BgL_arg1234z00_2060;

												BgL_arg1232z00_2058 = (BgL_iz00_2032 + 1L);
												BgL_arg1233z00_2059 = (BgL_maskz00_2035 << (int) (1L));
												BgL_arg1234z00_2060 =
													MAKE_YOUNG_PAIR(BINT(BgL_iz00_2032), BgL_resz00_2036);
												{
													obj_t BgL_resz00_2791;
													long BgL_maskz00_2790;
													long BgL_iz00_2789;

													BgL_iz00_2789 = BgL_arg1232z00_2058;
													BgL_maskz00_2790 = BgL_arg1233z00_2059;
													BgL_resz00_2791 = BgL_arg1234z00_2060;
													BgL_resz00_2036 = BgL_resz00_2791;
													BgL_maskz00_2035 = BgL_maskz00_2790;
													BgL_iz00_2032 = BgL_iz00_2789;
													goto BgL_loopz00_2031;
												}
											}
										else
											{
												long BgL_maskz00_2794;
												long BgL_iz00_2792;

												BgL_iz00_2792 = (BgL_iz00_2032 + 1L);
												BgL_maskz00_2794 = (BgL_maskz00_2035 << (int) (1L));
												BgL_maskz00_2035 = BgL_maskz00_2794;
												BgL_iz00_2032 = BgL_iz00_2792;
												goto BgL_loopz00_2031;
											}
									}
							}
					}
				}
			}
		}

	}



/* &rgcset->list */
	obj_t BGl_z62rgcsetzd2ze3listz53zz__rgc_setz00(obj_t BgL_envz00_2585,
		obj_t BgL_setz00_2586)
	{
		{	/* Rgc/rgcset.scm 137 */
			return BGl_rgcsetzd2ze3listz31zz__rgc_setz00(BgL_setz00_2586);
		}

	}



/* for-each-rgcset */
	BGL_EXPORTED_DEF obj_t BGl_forzd2eachzd2rgcsetz00zz__rgc_setz00(obj_t
		BgL_procz00_27, obj_t BgL_setz00_28)
	{
		{	/* Rgc/rgcset.scm 162 */
			{	/* Rgc/rgcset.scm 163 */
				obj_t BgL_maxz00_1280;
				long BgL_maxmaskz00_1281;

				BgL_maxz00_1280 = STRUCT_REF(((obj_t) BgL_setz00_28), (int) (0L));
				{	/* Rgc/rgcset.scm 164 */
					long BgL_yz00_2068;

					BgL_yz00_2068 = BGl_bitzd2perzd2wordz00zz__rgc_setz00;
					BgL_maxmaskz00_1281 = (1L << (int) (BgL_yz00_2068));
				}
				{	/* Rgc/rgcset.scm 165 */
					obj_t BgL_g1041z00_1282;

					{	/* Rgc/rgcset.scm 97 */
						obj_t BgL_arg1215z00_2069;

						BgL_arg1215z00_2069 =
							STRUCT_REF(((obj_t) BgL_setz00_28), (int) (1L));
						BgL_g1041z00_1282 = VECTOR_REF(((obj_t) BgL_arg1215z00_2069), 0L);
					}
					{
						long BgL_iz00_2091;
						long BgL_wordzd2numzd2_2092;
						obj_t BgL_wordz00_2093;
						long BgL_maskz00_2094;

						BgL_iz00_2091 = 0L;
						BgL_wordzd2numzd2_2092 = 0L;
						BgL_wordz00_2093 = BgL_g1041z00_1282;
						BgL_maskz00_2094 = 1L;
					BgL_loopz00_2090:
						if ((BgL_iz00_2091 == (long) CINT(BgL_maxz00_1280)))
							{	/* Rgc/rgcset.scm 170 */
								return BUNSPEC;
							}
						else
							{	/* Rgc/rgcset.scm 170 */
								if ((BgL_maskz00_2094 == BgL_maxmaskz00_1281))
									{	/* Rgc/rgcset.scm 174 */
										long BgL_arg1244z00_2101;
										obj_t BgL_arg1248z00_2102;

										BgL_arg1244z00_2101 = (BgL_wordzd2numzd2_2092 + 1L);
										{	/* Rgc/rgcset.scm 175 */
											long BgL_arg1249z00_2104;

											BgL_arg1249z00_2104 = (BgL_wordzd2numzd2_2092 + 1L);
											{	/* Rgc/rgcset.scm 97 */
												obj_t BgL_arg1215z00_2106;

												BgL_arg1215z00_2106 =
													STRUCT_REF(((obj_t) BgL_setz00_28), (int) (1L));
												BgL_arg1248z00_2102 =
													VECTOR_REF(
													((obj_t) BgL_arg1215z00_2106), BgL_arg1249z00_2104);
										}}
										{
											long BgL_maskz00_2822;
											obj_t BgL_wordz00_2821;
											long BgL_wordzd2numzd2_2820;

											BgL_wordzd2numzd2_2820 = BgL_arg1244z00_2101;
											BgL_wordz00_2821 = BgL_arg1248z00_2102;
											BgL_maskz00_2822 = 1L;
											BgL_maskz00_2094 = BgL_maskz00_2822;
											BgL_wordz00_2093 = BgL_wordz00_2821;
											BgL_wordzd2numzd2_2092 = BgL_wordzd2numzd2_2820;
											goto BgL_loopz00_2090;
										}
									}
								else
									{	/* Rgc/rgcset.scm 172 */
										if (
											(((long) CINT(BgL_wordz00_2093) & BgL_maskz00_2094) ==
												BgL_maskz00_2094))
											{	/* Rgc/rgcset.scm 177 */
												BGL_PROCEDURE_CALL1(BgL_procz00_27,
													BINT(BgL_iz00_2091));
												{
													long BgL_maskz00_2834;
													long BgL_iz00_2832;

													BgL_iz00_2832 = (BgL_iz00_2091 + 1L);
													BgL_maskz00_2834 = (BgL_maskz00_2094 << (int) (1L));
													BgL_maskz00_2094 = BgL_maskz00_2834;
													BgL_iz00_2091 = BgL_iz00_2832;
													goto BgL_loopz00_2090;
												}
											}
										else
											{
												long BgL_maskz00_2839;
												long BgL_iz00_2837;

												BgL_iz00_2837 = (BgL_iz00_2091 + 1L);
												BgL_maskz00_2839 = (BgL_maskz00_2094 << (int) (1L));
												BgL_maskz00_2094 = BgL_maskz00_2839;
												BgL_iz00_2091 = BgL_iz00_2837;
												goto BgL_loopz00_2090;
											}
									}
							}
					}
				}
			}
		}

	}



/* &for-each-rgcset */
	obj_t BGl_z62forzd2eachzd2rgcsetz62zz__rgc_setz00(obj_t BgL_envz00_2587,
		obj_t BgL_procz00_2588, obj_t BgL_setz00_2589)
	{
		{	/* Rgc/rgcset.scm 162 */
			{	/* Rgc/rgcset.scm 163 */
				obj_t BgL_auxz00_2842;

				if (PROCEDUREP(BgL_procz00_2588))
					{	/* Rgc/rgcset.scm 163 */
						BgL_auxz00_2842 = BgL_procz00_2588;
					}
				else
					{
						obj_t BgL_auxz00_2845;

						BgL_auxz00_2845 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1754z00zz__rgc_setz00,
							BINT(6606L), BGl_string1760z00zz__rgc_setz00,
							BGl_string1761z00zz__rgc_setz00, BgL_procz00_2588);
						FAILURE(BgL_auxz00_2845, BFALSE, BFALSE);
					}
				return
					BGl_forzd2eachzd2rgcsetz00zz__rgc_setz00(BgL_auxz00_2842,
					BgL_setz00_2589);
			}
		}

	}



/* rgcset-length */
	BGL_EXPORTED_DEF long BGl_rgcsetzd2lengthzd2zz__rgc_setz00(obj_t
		BgL_setz00_29)
	{
		{	/* Rgc/rgcset.scm 188 */
			{	/* Rgc/rgcset.scm 189 */
				obj_t BgL_numz00_2597;

				BgL_numz00_2597 = MAKE_CELL(BINT(0L));
				{	/* Rgc/rgcset.scm 190 */
					obj_t BgL_zc3z04anonymousza31307ze3z87_2590;

					BgL_zc3z04anonymousza31307ze3z87_2590 =
						MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31307ze3ze5zz__rgc_setz00,
						(int) (1L), (int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31307ze3z87_2590,
						(int) (0L), ((obj_t) BgL_numz00_2597));
					BGl_forzd2eachzd2rgcsetz00zz__rgc_setz00
						(BgL_zc3z04anonymousza31307ze3z87_2590, BgL_setz00_29);
				}
				return (long) CINT(CELL_REF(BgL_numz00_2597));
		}}

	}



/* &rgcset-length */
	obj_t BGl_z62rgcsetzd2lengthzb0zz__rgc_setz00(obj_t BgL_envz00_2591,
		obj_t BgL_setz00_2592)
	{
		{	/* Rgc/rgcset.scm 188 */
			return BINT(BGl_rgcsetzd2lengthzd2zz__rgc_setz00(BgL_setz00_2592));
		}

	}



/* &<@anonymous:1307> */
	obj_t BGl_z62zc3z04anonymousza31307ze3ze5zz__rgc_setz00(obj_t BgL_envz00_2593,
		obj_t BgL_xz00_2595)
	{
		{	/* Rgc/rgcset.scm 190 */
			{	/* Rgc/rgcset.scm 190 */
				obj_t BgL_numz00_2594;

				BgL_numz00_2594 = PROCEDURE_REF(BgL_envz00_2593, (int) (0L));
				{	/* Rgc/rgcset.scm 190 */
					obj_t BgL_auxz00_2633;

					BgL_auxz00_2633 = ADDFX(BINT(1L), CELL_REF(BgL_numz00_2594));
					return CELL_SET(BgL_numz00_2594, BgL_auxz00_2633);
				}
			}
		}

	}



/* rgcset-not! */
	BGL_EXPORTED_DEF obj_t BGl_rgcsetzd2notz12zc0zz__rgc_setz00(obj_t
		BgL_setz00_30)
	{
		{	/* Rgc/rgcset.scm 196 */
			{	/* Rgc/rgcset.scm 198 */
				long BgL_lenz00_1307;

				{	/* Rgc/rgcset.scm 103 */
					obj_t BgL_arg1216z00_2128;

					BgL_arg1216z00_2128 = STRUCT_REF(((obj_t) BgL_setz00_30), (int) (1L));
					BgL_lenz00_1307 = VECTOR_LENGTH(((obj_t) BgL_arg1216z00_2128));
				}
				{
					long BgL_iz00_1309;

					{	/* Rgc/rgcset.scm 199 */
						bool_t BgL_tmpz00_2870;

						BgL_iz00_1309 = 0L;
					BgL_zc3z04anonymousza31308ze3z87_1310:
						if ((BgL_iz00_1309 < BgL_lenz00_1307))
							{	/* Rgc/rgcset.scm 200 */
								{	/* Rgc/rgcset.scm 202 */
									long BgL_arg1311z00_1313;

									{	/* Rgc/rgcset.scm 202 */
										obj_t BgL_arg1312z00_1314;

										{	/* Rgc/rgcset.scm 97 */
											obj_t BgL_arg1215z00_2133;

											BgL_arg1215z00_2133 =
												STRUCT_REF(((obj_t) BgL_setz00_30), (int) (1L));
											BgL_arg1312z00_1314 =
												VECTOR_REF(
												((obj_t) BgL_arg1215z00_2133), BgL_iz00_1309);
										}
										BgL_arg1311z00_1313 = ~((long) CINT(BgL_arg1312z00_1314));
									}
									{	/* Rgc/rgcset.scm 91 */
										obj_t BgL_arg1212z00_2138;

										BgL_arg1212z00_2138 =
											STRUCT_REF(((obj_t) BgL_setz00_30), (int) (1L));
										VECTOR_SET(
											((obj_t) BgL_arg1212z00_2138), BgL_iz00_1309,
											BINT(BgL_arg1311z00_1313));
								}}
								{
									long BgL_iz00_2886;

									BgL_iz00_2886 = (BgL_iz00_1309 + 1L);
									BgL_iz00_1309 = BgL_iz00_2886;
									goto BgL_zc3z04anonymousza31308ze3z87_1310;
								}
							}
						else
							{	/* Rgc/rgcset.scm 200 */
								BgL_tmpz00_2870 = ((bool_t) 0);
							}
						return BBOOL(BgL_tmpz00_2870);
					}
				}
			}
		}

	}



/* &rgcset-not! */
	obj_t BGl_z62rgcsetzd2notz12za2zz__rgc_setz00(obj_t BgL_envz00_2599,
		obj_t BgL_setz00_2600)
	{
		{	/* Rgc/rgcset.scm 196 */
			return BGl_rgcsetzd2notz12zc0zz__rgc_setz00(BgL_setz00_2600);
		}

	}



/* rgcset-not */
	BGL_EXPORTED_DEF obj_t BGl_rgcsetzd2notzd2zz__rgc_setz00(obj_t BgL_setz00_31)
	{
		{	/* Rgc/rgcset.scm 208 */
			{	/* Rgc/rgcset.scm 210 */
				long BgL_lenz00_1318;
				obj_t BgL_newz00_1319;

				{	/* Rgc/rgcset.scm 103 */
					obj_t BgL_arg1216z00_2143;

					BgL_arg1216z00_2143 = STRUCT_REF(((obj_t) BgL_setz00_31), (int) (1L));
					BgL_lenz00_1318 = VECTOR_LENGTH(((obj_t) BgL_arg1216z00_2143));
				}
				{	/* Rgc/rgcset.scm 211 */
					obj_t BgL_arg1321z00_1330;

					BgL_arg1321z00_1330 = STRUCT_REF(((obj_t) BgL_setz00_31), (int) (0L));
					BgL_newz00_1319 =
						BGl_makezd2rgcsetzd2zz__rgc_setz00(
						(long) CINT(BgL_arg1321z00_1330));
				}
				{
					long BgL_iz00_1321;

					BgL_iz00_1321 = 0L;
				BgL_zc3z04anonymousza31315ze3z87_1322:
					if ((BgL_iz00_1321 < BgL_lenz00_1318))
						{	/* Rgc/rgcset.scm 213 */
							{	/* Rgc/rgcset.scm 215 */
								long BgL_arg1318z00_1325;

								{	/* Rgc/rgcset.scm 215 */
									obj_t BgL_arg1319z00_1326;

									{	/* Rgc/rgcset.scm 97 */
										obj_t BgL_arg1215z00_2149;

										BgL_arg1215z00_2149 =
											STRUCT_REF(((obj_t) BgL_setz00_31), (int) (1L));
										BgL_arg1319z00_1326 =
											VECTOR_REF(((obj_t) BgL_arg1215z00_2149), BgL_iz00_1321);
									}
									BgL_arg1318z00_1325 = ~((long) CINT(BgL_arg1319z00_1326));
								}
								{	/* Rgc/rgcset.scm 91 */
									obj_t BgL_arg1212z00_2154;

									BgL_arg1212z00_2154 =
										STRUCT_REF(((obj_t) BgL_newz00_1319), (int) (1L));
									VECTOR_SET(
										((obj_t) BgL_arg1212z00_2154), BgL_iz00_1321,
										BINT(BgL_arg1318z00_1325));
							}}
							{
								long BgL_iz00_2915;

								BgL_iz00_2915 = (BgL_iz00_1321 + 1L);
								BgL_iz00_1321 = BgL_iz00_2915;
								goto BgL_zc3z04anonymousza31315ze3z87_1322;
							}
						}
					else
						{	/* Rgc/rgcset.scm 213 */
							return BgL_newz00_1319;
						}
				}
			}
		}

	}



/* &rgcset-not */
	obj_t BGl_z62rgcsetzd2notzb0zz__rgc_setz00(obj_t BgL_envz00_2601,
		obj_t BgL_setz00_2602)
	{
		{	/* Rgc/rgcset.scm 208 */
			return BGl_rgcsetzd2notzd2zz__rgc_setz00(BgL_setz00_2602);
		}

	}



/* rgcset-and! */
	BGL_EXPORTED_DEF obj_t BGl_rgcsetzd2andz12zc0zz__rgc_setz00(obj_t
		BgL_set1z00_32, obj_t BgL_set2z00_33)
	{
		{	/* Rgc/rgcset.scm 222 */
			{	/* Rgc/rgcset.scm 224 */
				long BgL_len1z00_1331;
				long BgL_len2z00_1332;

				{	/* Rgc/rgcset.scm 103 */
					obj_t BgL_arg1216z00_2159;

					BgL_arg1216z00_2159 =
						STRUCT_REF(((obj_t) BgL_set1z00_32), (int) (1L));
					BgL_len1z00_1331 = VECTOR_LENGTH(((obj_t) BgL_arg1216z00_2159));
				}
				{	/* Rgc/rgcset.scm 103 */
					obj_t BgL_arg1216z00_2162;

					BgL_arg1216z00_2162 =
						STRUCT_REF(((obj_t) BgL_set2z00_33), (int) (1L));
					BgL_len2z00_1332 = VECTOR_LENGTH(((obj_t) BgL_arg1216z00_2162));
				}
				{
					long BgL_iz00_1334;

					{	/* Rgc/rgcset.scm 226 */
						bool_t BgL_tmpz00_2928;

						BgL_iz00_1334 = 0L;
					BgL_zc3z04anonymousza31322ze3z87_1335:
						{	/* Rgc/rgcset.scm 227 */
							bool_t BgL_test1808z00_2929;

							if ((BgL_iz00_1334 < BgL_len1z00_1331))
								{	/* Rgc/rgcset.scm 227 */
									BgL_test1808z00_2929 = (BgL_iz00_1334 < BgL_len2z00_1332);
								}
							else
								{	/* Rgc/rgcset.scm 227 */
									BgL_test1808z00_2929 = ((bool_t) 0);
								}
							if (BgL_test1808z00_2929)
								{	/* Rgc/rgcset.scm 227 */
									{	/* Rgc/rgcset.scm 231 */
										long BgL_arg1327z00_1341;

										{	/* Rgc/rgcset.scm 231 */
											obj_t BgL_arg1328z00_1342;
											obj_t BgL_arg1329z00_1343;

											{	/* Rgc/rgcset.scm 97 */
												obj_t BgL_arg1215z00_2169;

												BgL_arg1215z00_2169 =
													STRUCT_REF(((obj_t) BgL_set1z00_32), (int) (1L));
												BgL_arg1328z00_1342 =
													VECTOR_REF(
													((obj_t) BgL_arg1215z00_2169), BgL_iz00_1334);
											}
											{	/* Rgc/rgcset.scm 97 */
												obj_t BgL_arg1215z00_2173;

												BgL_arg1215z00_2173 =
													STRUCT_REF(((obj_t) BgL_set2z00_33), (int) (1L));
												BgL_arg1329z00_1343 =
													VECTOR_REF(
													((obj_t) BgL_arg1215z00_2173), BgL_iz00_1334);
											}
											BgL_arg1327z00_1341 =
												(
												(long) CINT(BgL_arg1328z00_1342) &
												(long) CINT(BgL_arg1329z00_1343));
										}
										{	/* Rgc/rgcset.scm 91 */
											obj_t BgL_arg1212z00_2179;

											BgL_arg1212z00_2179 =
												STRUCT_REF(((obj_t) BgL_set1z00_32), (int) (1L));
											VECTOR_SET(
												((obj_t) BgL_arg1212z00_2179), BgL_iz00_1334,
												BINT(BgL_arg1327z00_1341));
									}}
									{
										long BgL_iz00_2952;

										BgL_iz00_2952 = (BgL_iz00_1334 + 1L);
										BgL_iz00_1334 = BgL_iz00_2952;
										goto BgL_zc3z04anonymousza31322ze3z87_1335;
									}
								}
							else
								{	/* Rgc/rgcset.scm 227 */
									BgL_tmpz00_2928 = ((bool_t) 0);
								}
						}
						return BBOOL(BgL_tmpz00_2928);
					}
				}
			}
		}

	}



/* &rgcset-and! */
	obj_t BGl_z62rgcsetzd2andz12za2zz__rgc_setz00(obj_t BgL_envz00_2603,
		obj_t BgL_set1z00_2604, obj_t BgL_set2z00_2605)
	{
		{	/* Rgc/rgcset.scm 222 */
			return
				BGl_rgcsetzd2andz12zc0zz__rgc_setz00(BgL_set1z00_2604,
				BgL_set2z00_2605);
		}

	}



/* rgcset-or! */
	BGL_EXPORTED_DEF obj_t BGl_rgcsetzd2orz12zc0zz__rgc_setz00(obj_t
		BgL_set1z00_34, obj_t BgL_set2z00_35)
	{
		{	/* Rgc/rgcset.scm 238 */
			{	/* Rgc/rgcset.scm 240 */
				long BgL_len1z00_1349;
				long BgL_len2z00_1350;

				{	/* Rgc/rgcset.scm 103 */
					obj_t BgL_arg1216z00_2184;

					BgL_arg1216z00_2184 =
						STRUCT_REF(((obj_t) BgL_set1z00_34), (int) (1L));
					BgL_len1z00_1349 = VECTOR_LENGTH(((obj_t) BgL_arg1216z00_2184));
				}
				{	/* Rgc/rgcset.scm 103 */
					obj_t BgL_arg1216z00_2187;

					BgL_arg1216z00_2187 =
						STRUCT_REF(((obj_t) BgL_set2z00_35), (int) (1L));
					BgL_len2z00_1350 = VECTOR_LENGTH(((obj_t) BgL_arg1216z00_2187));
				}
				{
					long BgL_iz00_1352;

					{	/* Rgc/rgcset.scm 243 */
						bool_t BgL_tmpz00_2966;

						BgL_iz00_1352 = 0L;
					BgL_zc3z04anonymousza31332ze3z87_1353:
						if ((BgL_iz00_1352 < BgL_len1z00_1349))
							{	/* Rgc/rgcset.scm 244 */
								{	/* Rgc/rgcset.scm 248 */
									long BgL_arg1335z00_1356;

									{	/* Rgc/rgcset.scm 248 */
										obj_t BgL_arg1336z00_1357;
										obj_t BgL_arg1337z00_1358;

										{	/* Rgc/rgcset.scm 97 */
											obj_t BgL_arg1215z00_2192;

											BgL_arg1215z00_2192 =
												STRUCT_REF(((obj_t) BgL_set1z00_34), (int) (1L));
											BgL_arg1336z00_1357 =
												VECTOR_REF(
												((obj_t) BgL_arg1215z00_2192), BgL_iz00_1352);
										}
										{	/* Rgc/rgcset.scm 97 */
											obj_t BgL_arg1215z00_2196;

											BgL_arg1215z00_2196 =
												STRUCT_REF(((obj_t) BgL_set2z00_35), (int) (1L));
											BgL_arg1337z00_1358 =
												VECTOR_REF(
												((obj_t) BgL_arg1215z00_2196), BgL_iz00_1352);
										}
										BgL_arg1335z00_1356 =
											(
											(long) CINT(BgL_arg1336z00_1357) |
											(long) CINT(BgL_arg1337z00_1358));
									}
									{	/* Rgc/rgcset.scm 91 */
										obj_t BgL_arg1212z00_2202;

										BgL_arg1212z00_2202 =
											STRUCT_REF(((obj_t) BgL_set1z00_34), (int) (1L));
										VECTOR_SET(
											((obj_t) BgL_arg1212z00_2202), BgL_iz00_1352,
											BINT(BgL_arg1335z00_1356));
								}}
								{
									long BgL_iz00_2988;

									BgL_iz00_2988 = (BgL_iz00_1352 + 1L);
									BgL_iz00_1352 = BgL_iz00_2988;
									goto BgL_zc3z04anonymousza31332ze3z87_1353;
								}
							}
						else
							{	/* Rgc/rgcset.scm 244 */
								BgL_tmpz00_2966 = ((bool_t) 0);
							}
						return BBOOL(BgL_tmpz00_2966);
					}
				}
			}
		}

	}



/* &rgcset-or! */
	obj_t BGl_z62rgcsetzd2orz12za2zz__rgc_setz00(obj_t BgL_envz00_2606,
		obj_t BgL_set1z00_2607, obj_t BgL_set2z00_2608)
	{
		{	/* Rgc/rgcset.scm 238 */
			return
				BGl_rgcsetzd2orz12zc0zz__rgc_setz00(BgL_set1z00_2607, BgL_set2z00_2608);
		}

	}



/* rgcset-or */
	BGL_EXPORTED_DEF obj_t BGl_rgcsetzd2orzd2zz__rgc_setz00(obj_t BgL_set1z00_36,
		obj_t BgL_set2z00_37)
	{
		{	/* Rgc/rgcset.scm 255 */
			{	/* Rgc/rgcset.scm 257 */
				long BgL_len1z00_1362;

				{	/* Rgc/rgcset.scm 103 */
					obj_t BgL_arg1216z00_2207;

					BgL_arg1216z00_2207 =
						STRUCT_REF(((obj_t) BgL_set1z00_36), (int) (1L));
					BgL_len1z00_1362 = VECTOR_LENGTH(((obj_t) BgL_arg1216z00_2207));
				}
				{	/* Rgc/rgcset.scm 257 */
					long BgL_len2z00_1363;

					{	/* Rgc/rgcset.scm 103 */
						obj_t BgL_arg1216z00_2210;

						BgL_arg1216z00_2210 =
							STRUCT_REF(((obj_t) BgL_set2z00_37), (int) (1L));
						BgL_len2z00_1363 = VECTOR_LENGTH(((obj_t) BgL_arg1216z00_2210));
					}
					{	/* Rgc/rgcset.scm 258 */
						obj_t BgL_newz00_1364;

						{	/* Rgc/rgcset.scm 259 */
							obj_t BgL_arg1347z00_1376;

							BgL_arg1347z00_1376 =
								STRUCT_REF(((obj_t) BgL_set1z00_36), (int) (0L));
							BgL_newz00_1364 =
								BGl_makezd2rgcsetzd2zz__rgc_setz00(
								(long) CINT(BgL_arg1347z00_1376));
						}
						{	/* Rgc/rgcset.scm 259 */

							{
								long BgL_iz00_1366;

								BgL_iz00_1366 = 0L;
							BgL_zc3z04anonymousza31339ze3z87_1367:
								if ((BgL_iz00_1366 < BgL_len1z00_1362))
									{	/* Rgc/rgcset.scm 262 */
										{	/* Rgc/rgcset.scm 266 */
											long BgL_arg1342z00_1370;

											{	/* Rgc/rgcset.scm 266 */
												obj_t BgL_arg1343z00_1371;
												obj_t BgL_arg1344z00_1372;

												{	/* Rgc/rgcset.scm 97 */
													obj_t BgL_arg1215z00_2216;

													BgL_arg1215z00_2216 =
														STRUCT_REF(((obj_t) BgL_set1z00_36), (int) (1L));
													BgL_arg1343z00_1371 =
														VECTOR_REF(
														((obj_t) BgL_arg1215z00_2216), BgL_iz00_1366);
												}
												{	/* Rgc/rgcset.scm 97 */
													obj_t BgL_arg1215z00_2220;

													BgL_arg1215z00_2220 =
														STRUCT_REF(((obj_t) BgL_set2z00_37), (int) (1L));
													BgL_arg1344z00_1372 =
														VECTOR_REF(
														((obj_t) BgL_arg1215z00_2220), BgL_iz00_1366);
												}
												BgL_arg1342z00_1370 =
													(
													(long) CINT(BgL_arg1343z00_1371) |
													(long) CINT(BgL_arg1344z00_1372));
											}
											{	/* Rgc/rgcset.scm 91 */
												obj_t BgL_arg1212z00_2226;

												BgL_arg1212z00_2226 =
													STRUCT_REF(((obj_t) BgL_newz00_1364), (int) (1L));
												VECTOR_SET(
													((obj_t) BgL_arg1212z00_2226), BgL_iz00_1366,
													BINT(BgL_arg1342z00_1370));
										}}
										{
											long BgL_iz00_3028;

											BgL_iz00_3028 = (BgL_iz00_1366 + 1L);
											BgL_iz00_1366 = BgL_iz00_3028;
											goto BgL_zc3z04anonymousza31339ze3z87_1367;
										}
									}
								else
									{	/* Rgc/rgcset.scm 262 */
										return BgL_newz00_1364;
									}
							}
						}
					}
				}
			}
		}

	}



/* &rgcset-or */
	obj_t BGl_z62rgcsetzd2orzb0zz__rgc_setz00(obj_t BgL_envz00_2609,
		obj_t BgL_set1z00_2610, obj_t BgL_set2z00_2611)
	{
		{	/* Rgc/rgcset.scm 255 */
			return
				BGl_rgcsetzd2orzd2zz__rgc_setz00(BgL_set1z00_2610, BgL_set2z00_2611);
		}

	}



/* rgcset-but! */
	BGL_EXPORTED_DEF obj_t BGl_rgcsetzd2butz12zc0zz__rgc_setz00(obj_t
		BgL_set1z00_38, obj_t BgL_set2z00_39)
	{
		{	/* Rgc/rgcset.scm 274 */
			{	/* Rgc/rgcset.scm 275 */
				long BgL_len1z00_1377;
				long BgL_len2z00_1378;

				{	/* Rgc/rgcset.scm 103 */
					obj_t BgL_arg1216z00_2231;

					BgL_arg1216z00_2231 =
						STRUCT_REF(((obj_t) BgL_set1z00_38), (int) (1L));
					BgL_len1z00_1377 = VECTOR_LENGTH(((obj_t) BgL_arg1216z00_2231));
				}
				{	/* Rgc/rgcset.scm 103 */
					obj_t BgL_arg1216z00_2234;

					BgL_arg1216z00_2234 =
						STRUCT_REF(((obj_t) BgL_set2z00_39), (int) (1L));
					BgL_len2z00_1378 = VECTOR_LENGTH(((obj_t) BgL_arg1216z00_2234));
				}
				{
					long BgL_iz00_1380;

					{	/* Rgc/rgcset.scm 278 */
						bool_t BgL_tmpz00_3041;

						BgL_iz00_1380 = 0L;
					BgL_zc3z04anonymousza31348ze3z87_1381:
						if ((BgL_iz00_1380 < BgL_len1z00_1377))
							{	/* Rgc/rgcset.scm 279 */
								{	/* Rgc/rgcset.scm 283 */
									obj_t BgL_arg1351z00_1384;

									{	/* Rgc/rgcset.scm 283 */
										obj_t BgL_a1095z00_1385;

										{	/* Rgc/rgcset.scm 97 */
											obj_t BgL_arg1215z00_2239;

											BgL_arg1215z00_2239 =
												STRUCT_REF(((obj_t) BgL_set1z00_38), (int) (1L));
											BgL_a1095z00_1385 =
												VECTOR_REF(
												((obj_t) BgL_arg1215z00_2239), BgL_iz00_1380);
										}
										{	/* Rgc/rgcset.scm 284 */
											obj_t BgL_b1096z00_1386;

											{	/* Rgc/rgcset.scm 97 */
												obj_t BgL_arg1215z00_2243;

												BgL_arg1215z00_2243 =
													STRUCT_REF(((obj_t) BgL_set2z00_39), (int) (1L));
												BgL_b1096z00_1386 =
													VECTOR_REF(
													((obj_t) BgL_arg1215z00_2243), BgL_iz00_1380);
											}
											{	/* Rgc/rgcset.scm 283 */

												{	/* Rgc/rgcset.scm 283 */
													bool_t BgL_test1813z00_3054;

													if (INTEGERP(BgL_a1095z00_1385))
														{	/* Rgc/rgcset.scm 283 */
															BgL_test1813z00_3054 =
																INTEGERP(BgL_b1096z00_1386);
														}
													else
														{	/* Rgc/rgcset.scm 283 */
															BgL_test1813z00_3054 = ((bool_t) 0);
														}
													if (BgL_test1813z00_3054)
														{	/* Rgc/rgcset.scm 283 */
															BgL_arg1351z00_1384 =
																SUBFX(BgL_a1095z00_1385, BgL_b1096z00_1386);
														}
													else
														{	/* Rgc/rgcset.scm 283 */
															BgL_arg1351z00_1384 =
																BGl_2zd2zd2zz__r4_numbers_6_5z00
																(BgL_a1095z00_1385, BgL_b1096z00_1386);
														}
												}
											}
										}
									}
									{	/* Rgc/rgcset.scm 91 */
										obj_t BgL_arg1212z00_2249;

										BgL_arg1212z00_2249 =
											STRUCT_REF(((obj_t) BgL_set1z00_38), (int) (1L));
										VECTOR_SET(
											((obj_t) BgL_arg1212z00_2249), BgL_iz00_1380,
											BgL_arg1351z00_1384);
								}}
								{
									long BgL_iz00_3065;

									BgL_iz00_3065 = (BgL_iz00_1380 + 1L);
									BgL_iz00_1380 = BgL_iz00_3065;
									goto BgL_zc3z04anonymousza31348ze3z87_1381;
								}
							}
						else
							{	/* Rgc/rgcset.scm 279 */
								BgL_tmpz00_3041 = ((bool_t) 0);
							}
						return BBOOL(BgL_tmpz00_3041);
					}
				}
			}
		}

	}



/* &rgcset-but! */
	obj_t BGl_z62rgcsetzd2butz12za2zz__rgc_setz00(obj_t BgL_envz00_2612,
		obj_t BgL_set1z00_2613, obj_t BgL_set2z00_2614)
	{
		{	/* Rgc/rgcset.scm 274 */
			return
				BGl_rgcsetzd2butz12zc0zz__rgc_setz00(BgL_set1z00_2613,
				BgL_set2z00_2614);
		}

	}



/* rgcset-equal? */
	BGL_EXPORTED_DEF bool_t BGl_rgcsetzd2equalzf3z21zz__rgc_setz00(obj_t
		BgL_set1z00_40, obj_t BgL_set2z00_41)
	{
		{	/* Rgc/rgcset.scm 290 */
			{	/* Rgc/rgcset.scm 291 */
				long BgL_len1z00_1391;
				long BgL_len2z00_1392;

				{	/* Rgc/rgcset.scm 103 */
					obj_t BgL_arg1216z00_2254;

					BgL_arg1216z00_2254 =
						STRUCT_REF(((obj_t) BgL_set1z00_40), (int) (1L));
					BgL_len1z00_1391 = VECTOR_LENGTH(((obj_t) BgL_arg1216z00_2254));
				}
				{	/* Rgc/rgcset.scm 103 */
					obj_t BgL_arg1216z00_2257;

					BgL_arg1216z00_2257 =
						STRUCT_REF(((obj_t) BgL_set2z00_41), (int) (1L));
					BgL_len2z00_1392 = VECTOR_LENGTH(((obj_t) BgL_arg1216z00_2257));
				}
				if ((BgL_len1z00_1391 == BgL_len2z00_1392))
					{	/* Rgc/rgcset.scm 294 */
						obj_t BgL_w1z00_1394;
						obj_t BgL_w2z00_1395;

						BgL_w1z00_1394 = STRUCT_REF(((obj_t) BgL_set1z00_40), (int) (1L));
						BgL_w2z00_1395 = STRUCT_REF(((obj_t) BgL_set2z00_41), (int) (1L));
						{
							long BgL_iz00_2274;

							BgL_iz00_2274 = 0L;
						BgL_loopz00_2273:
							if ((BgL_iz00_2274 == BgL_len1z00_1391))
								{	/* Rgc/rgcset.scm 298 */
									return ((bool_t) 1);
								}
							else
								{	/* Rgc/rgcset.scm 300 */
									bool_t BgL_test1817z00_3089;

									{	/* Rgc/rgcset.scm 300 */
										obj_t BgL_arg1361z00_2279;
										obj_t BgL_arg1362z00_2280;

										BgL_arg1361z00_2279 =
											VECTOR_REF(((obj_t) BgL_w1z00_1394), BgL_iz00_2274);
										BgL_arg1362z00_2280 =
											VECTOR_REF(((obj_t) BgL_w2z00_1395), BgL_iz00_2274);
										BgL_test1817z00_3089 =
											(
											(long) CINT(BgL_arg1361z00_2279) ==
											(long) CINT(BgL_arg1362z00_2280));
									}
									if (BgL_test1817z00_3089)
										{
											long BgL_iz00_3097;

											BgL_iz00_3097 = (BgL_iz00_2274 + 1L);
											BgL_iz00_2274 = BgL_iz00_3097;
											goto BgL_loopz00_2273;
										}
									else
										{	/* Rgc/rgcset.scm 300 */
											return ((bool_t) 0);
										}
								}
						}
					}
				else
					{	/* Rgc/rgcset.scm 293 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rgcset-equal? */
	obj_t BGl_z62rgcsetzd2equalzf3z43zz__rgc_setz00(obj_t BgL_envz00_2615,
		obj_t BgL_set1z00_2616, obj_t BgL_set2z00_2617)
	{
		{	/* Rgc/rgcset.scm 290 */
			return
				BBOOL(BGl_rgcsetzd2equalzf3z21zz__rgc_setz00(BgL_set1z00_2616,
					BgL_set2z00_2617));
		}

	}



/* rgcset->hash */
	BGL_EXPORTED_DEF long BGl_rgcsetzd2ze3hashz31zz__rgc_setz00(obj_t
		BgL_setz00_42)
	{
		{	/* Rgc/rgcset.scm 308 */
			{	/* Rgc/rgcset.scm 309 */
				long BgL_lenz00_1407;

				{	/* Rgc/rgcset.scm 103 */
					obj_t BgL_arg1216z00_2289;

					BgL_arg1216z00_2289 = STRUCT_REF(((obj_t) BgL_setz00_42), (int) (1L));
					BgL_lenz00_1407 = VECTOR_LENGTH(((obj_t) BgL_arg1216z00_2289));
				}
				{	/* Rgc/rgcset.scm 310 */
					obj_t BgL_g1043z00_1408;

					{	/* Rgc/rgcset.scm 97 */
						obj_t BgL_arg1215z00_2292;

						BgL_arg1215z00_2292 =
							STRUCT_REF(((obj_t) BgL_setz00_42), (int) (1L));
						BgL_g1043z00_1408 = VECTOR_REF(((obj_t) BgL_arg1215z00_2292), 0L);
					}
					{
						long BgL_iz00_1410;
						obj_t BgL_resz00_1411;

						{	/* Rgc/rgcset.scm 310 */
							obj_t BgL_tmpz00_3111;

							BgL_iz00_1410 = 1L;
							BgL_resz00_1411 = BgL_g1043z00_1408;
						BgL_zc3z04anonymousza31363ze3z87_1412:
							if ((BgL_iz00_1410 == BgL_lenz00_1407))
								{	/* Rgc/rgcset.scm 312 */
									if (((long) CINT(BgL_resz00_1411) < 0L))
										{	/* Rgc/rgcset.scm 313 */
											BgL_tmpz00_3111 = BINT(NEG((long) CINT(BgL_resz00_1411)));
										}
									else
										{	/* Rgc/rgcset.scm 313 */
											BgL_tmpz00_3111 = BgL_resz00_1411;
										}
								}
							else
								{	/* Rgc/rgcset.scm 316 */
									long BgL_vz00_1415;

									{	/* Rgc/rgcset.scm 316 */
										long BgL_arg1371z00_1421;

										{	/* Rgc/rgcset.scm 316 */
											long BgL_arg1372z00_1422;
											obj_t BgL_arg1373z00_1423;

											BgL_arg1372z00_1422 =
												((long) CINT(BgL_resz00_1411) << (int) (3L));
											{	/* Rgc/rgcset.scm 97 */
												obj_t BgL_arg1215z00_2300;

												BgL_arg1215z00_2300 =
													STRUCT_REF(((obj_t) BgL_setz00_42), (int) (1L));
												BgL_arg1373z00_1423 =
													VECTOR_REF(
													((obj_t) BgL_arg1215z00_2300), BgL_iz00_1410);
											}
											BgL_arg1371z00_1421 =
												(BgL_arg1372z00_1422 +
												(long) CINT(BgL_arg1373z00_1423));
										}
										BgL_vz00_1415 =
											((long) CINT(BgL_resz00_1411) + BgL_arg1371z00_1421);
									}
									{	/* Rgc/rgcset.scm 317 */
										long BgL_arg1366z00_1416;
										long BgL_arg1367z00_1417;

										BgL_arg1366z00_1416 = (BgL_iz00_1410 + 1L);
										{	/* Rgc/rgcset.scm 318 */
											bool_t BgL_test1820z00_3133;

											{	/* Rgc/rgcset.scm 318 */
												obj_t BgL_arg1370z00_1420;

												{	/* Rgc/rgcset.scm 97 */
													obj_t BgL_arg1215z00_2309;

													BgL_arg1215z00_2309 =
														STRUCT_REF(((obj_t) BgL_setz00_42), (int) (1L));
													BgL_arg1370z00_1420 =
														VECTOR_REF(
														((obj_t) BgL_arg1215z00_2309), BgL_iz00_1410);
												}
												BgL_test1820z00_3133 =
													((long) CINT(BgL_arg1370z00_1420) == 0L);
											}
											if (BgL_test1820z00_3133)
												{	/* Rgc/rgcset.scm 318 */
													BgL_arg1367z00_1417 = BgL_vz00_1415;
												}
											else
												{	/* Rgc/rgcset.scm 318 */
													BgL_arg1367z00_1417 = (BgL_iz00_1410 + BgL_vz00_1415);
												}
										}
										{
											obj_t BgL_resz00_3143;
											long BgL_iz00_3142;

											BgL_iz00_3142 = BgL_arg1366z00_1416;
											BgL_resz00_3143 = BINT(BgL_arg1367z00_1417);
											BgL_resz00_1411 = BgL_resz00_3143;
											BgL_iz00_1410 = BgL_iz00_3142;
											goto BgL_zc3z04anonymousza31363ze3z87_1412;
										}
									}
								}
							return (long) CINT(BgL_tmpz00_3111);
		}}}}}

	}



/* &rgcset->hash */
	obj_t BGl_z62rgcsetzd2ze3hashz53zz__rgc_setz00(obj_t BgL_envz00_2618,
		obj_t BgL_setz00_2619)
	{
		{	/* Rgc/rgcset.scm 308 */
			return BINT(BGl_rgcsetzd2ze3hashz31zz__rgc_setz00(BgL_setz00_2619));
		}

	}



/* rgcset-remove! */
	BGL_EXPORTED_DEF obj_t BGl_rgcsetzd2removez12zc0zz__rgc_setz00(obj_t
		BgL_setz00_43, long BgL_numz00_44)
	{
		{	/* Rgc/rgcset.scm 325 */
			{	/* Rgc/rgcset.scm 326 */
				long BgL_wordzd2numzd2_2316;

				BgL_wordzd2numzd2_2316 =
					(BgL_numz00_44 / BGl_bitzd2perzd2wordz00zz__rgc_setz00);
				{	/* Rgc/rgcset.scm 326 */
					long BgL_wordzd2bitzd2_2317;

					{	/* Rgc/rgcset.scm 327 */
						long BgL_n1z00_2323;
						long BgL_n2z00_2324;

						BgL_n1z00_2323 = BgL_numz00_44;
						BgL_n2z00_2324 = BGl_bitzd2perzd2wordz00zz__rgc_setz00;
						{	/* Rgc/rgcset.scm 327 */
							bool_t BgL_test1821z00_3149;

							{	/* Rgc/rgcset.scm 327 */
								long BgL_arg1557z00_2326;

								BgL_arg1557z00_2326 =
									(((BgL_n1z00_2323) | (BgL_n2z00_2324)) & -2147483648);
								BgL_test1821z00_3149 = (BgL_arg1557z00_2326 == 0L);
							}
							if (BgL_test1821z00_3149)
								{	/* Rgc/rgcset.scm 327 */
									int32_t BgL_arg1554z00_2327;

									{	/* Rgc/rgcset.scm 327 */
										int32_t BgL_arg1555z00_2328;
										int32_t BgL_arg1556z00_2329;

										BgL_arg1555z00_2328 = (int32_t) (BgL_n1z00_2323);
										BgL_arg1556z00_2329 = (int32_t) (BgL_n2z00_2324);
										BgL_arg1554z00_2327 =
											(BgL_arg1555z00_2328 % BgL_arg1556z00_2329);
									}
									{	/* Rgc/rgcset.scm 327 */
										long BgL_arg1692z00_2334;

										BgL_arg1692z00_2334 = (long) (BgL_arg1554z00_2327);
										BgL_wordzd2bitzd2_2317 = (long) (BgL_arg1692z00_2334);
								}}
							else
								{	/* Rgc/rgcset.scm 327 */
									BgL_wordzd2bitzd2_2317 = (BgL_n1z00_2323 % BgL_n2z00_2324);
								}
						}
					}
					{	/* Rgc/rgcset.scm 327 */
						obj_t BgL_oldz00_2318;

						{	/* Rgc/rgcset.scm 97 */
							obj_t BgL_arg1215z00_2336;

							BgL_arg1215z00_2336 =
								STRUCT_REF(((obj_t) BgL_setz00_43), (int) (1L));
							BgL_oldz00_2318 =
								VECTOR_REF(
								((obj_t) BgL_arg1215z00_2336), BgL_wordzd2numzd2_2316);
						}
						{	/* Rgc/rgcset.scm 328 */

							{	/* Rgc/rgcset.scm 329 */
								long BgL_arg1375z00_2319;

								BgL_arg1375z00_2319 =
									(
									(long) CINT(BgL_oldz00_2318) ^
									(1L << (int) (BgL_wordzd2bitzd2_2317)));
								{	/* Rgc/rgcset.scm 91 */
									obj_t BgL_arg1212z00_2343;

									BgL_arg1212z00_2343 =
										STRUCT_REF(((obj_t) BgL_setz00_43), (int) (1L));
									return
										VECTOR_SET(
										((obj_t) BgL_arg1212z00_2343), BgL_wordzd2numzd2_2316,
										BINT(BgL_arg1375z00_2319));
								}
							}
						}
					}
				}
			}
		}

	}



/* &rgcset-remove! */
	obj_t BGl_z62rgcsetzd2removez12za2zz__rgc_setz00(obj_t BgL_envz00_2620,
		obj_t BgL_setz00_2621, obj_t BgL_numz00_2622)
	{
		{	/* Rgc/rgcset.scm 325 */
			{	/* Rgc/rgcset.scm 326 */
				long BgL_auxz00_3173;

				{	/* Rgc/rgcset.scm 326 */
					obj_t BgL_tmpz00_3174;

					if (INTEGERP(BgL_numz00_2622))
						{	/* Rgc/rgcset.scm 326 */
							BgL_tmpz00_3174 = BgL_numz00_2622;
						}
					else
						{
							obj_t BgL_auxz00_3177;

							BgL_auxz00_3177 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1754z00zz__rgc_setz00,
								BINT(12634L), BGl_string1762z00zz__rgc_setz00,
								BGl_string1756z00zz__rgc_setz00, BgL_numz00_2622);
							FAILURE(BgL_auxz00_3177, BFALSE, BFALSE);
						}
					BgL_auxz00_3173 = (long) CINT(BgL_tmpz00_3174);
				}
				return
					BGl_rgcsetzd2removez12zc0zz__rgc_setz00(BgL_setz00_2621,
					BgL_auxz00_3173);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__rgc_setz00(void)
	{
		{	/* Rgc/rgcset.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__rgc_setz00(void)
	{
		{	/* Rgc/rgcset.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__rgc_setz00(void)
	{
		{	/* Rgc/rgcset.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__rgc_setz00(void)
	{
		{	/* Rgc/rgcset.scm 15 */
			return
				BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1763z00zz__rgc_setz00));
		}

	}

#ifdef __cplusplus
}
#endif
