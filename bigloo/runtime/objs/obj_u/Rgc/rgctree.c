/*===========================================================================*/
/*   (Rgc/rgctree.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Rgc/rgctree.scm -indent -o objs/obj_u/Rgc/rgctree.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___RGC_TREE_TYPE_DEFINITIONS
#define BGL___RGC_TREE_TYPE_DEFINITIONS
#endif													// BGL___RGC_TREE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_symbol1750z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_list1800z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_symbol1759z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_za2positionzd2numberza2zd2zz__rgc_treez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_printzd2nodezd2zz__rgc_treez00(obj_t);
	static obj_t BGl_symbol1763z00zz__rgc_treez00 = BUNSPEC;
	static long BGl_regularzd2treezd2positionzd2numberzd2zz__rgc_treez00(obj_t);
	static obj_t BGl_symbol1766z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_z62or2zd2ze3nodez53zz__rgc_treez00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol1768z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_requirezd2initializa7ationz75zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_orzd2ze3nodez31zz__rgc_treez00(obj_t);
	extern obj_t BGl_forzd2eachzd2rgcsetz00zz__rgc_setz00(obj_t, obj_t);
	static obj_t BGl_treezd2ze3nodez31zz__rgc_treez00(obj_t);
	static obj_t BGl_symbol1771z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_z62resetzd2treez12za2zz__rgc_treez00(obj_t);
	static obj_t BGl_initzd2followposz12zc0zz__rgc_treez00(void);
	static obj_t BGl_symbol1775z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_symbol1778z00zz__rgc_treez00 = BUNSPEC;
	extern obj_t bgl_display_obj(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__rgc_treez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__rgc_configz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__rgc_setz00(long, char *);
	static obj_t BGl_submatchzd2stopzd2addz12z12zz__rgc_treez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62printzd2nodezb0zz__rgc_treez00(obj_t, obj_t);
	static obj_t BGl_symbol1781z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_submatchzd2startzd2addz12z12zz__rgc_treez00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_symbol1784z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_symbol1787z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_list1758z00zz__rgc_treez00 = BUNSPEC;
	extern obj_t BGl_rgcsetzd2addz12zc0zz__rgc_setz00(obj_t, long);
	BGL_EXPORTED_DECL obj_t BGl_regularzd2treezd2ze3nodeze3zz__rgc_treez00(obj_t);
	static obj_t BGl_symbol1791z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_symbol1794z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_list1761z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_list1762z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zz__rgc_treez00(void);
	static obj_t BGl_symbol1797z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_list1765z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31310ze3ze5zz__rgc_treez00(obj_t, obj_t);
	extern obj_t create_struct(obj_t, int);
	static obj_t BGl_list1770z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_list1773z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_list1774z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_list1777z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_cnstzd2initzd2zz__rgc_treez00(void);
	static obj_t BGl_genericzd2initzd2zz__rgc_treez00(void);
	static obj_t BGl_z62regularzd2treezd2ze3nodez81zz__rgc_treez00(obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__rgc_treez00(void);
	static obj_t BGl_list1780z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_list1783z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_gczd2rootszd2initz00zz__rgc_treez00(void);
	static obj_t BGl_list1786z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_objectzd2initzd2zz__rgc_treez00(void);
	static obj_t BGl_za2followposza2z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_za2currentzd2positionza2zd2zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_za2submatchesza2z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_submatchzd2ze3nodez31zz__rgc_treez00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31320ze3ze5zz__rgc_treez00(obj_t, obj_t);
	static obj_t BGl_initzd2positionsz12zc0zz__rgc_treez00(void);
	static obj_t BGl_list1790z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_list1793z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_list1796z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_list1799z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_binaryzd2ze3nodez31zz__rgc_treez00(obj_t, obj_t);
	static obj_t BGl_integerzd2ze3nodez31zz__rgc_treez00(obj_t);
	extern obj_t BGl_rgcsetzd2orz12zc0zz__rgc_setz00(obj_t, obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_resetzd2treez12zc0zz__rgc_treez00(void);
	static obj_t BGl_z62sequence2zd2ze3nodez53zz__rgc_treez00(obj_t, obj_t,
		obj_t);
	extern obj_t make_vector(long, obj_t);
	static obj_t BGl_methodzd2initzd2zz__rgc_treez00(void);
	static obj_t BGl_za2positionsza2z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_loopze70ze7zz__rgc_treez00(obj_t, obj_t);
	static long BGl_loopze71ze7zz__rgc_treez00(obj_t, long);
	static obj_t BGl_z62zc3z04anonymousza31333ze3ze5zz__rgc_treez00(obj_t, obj_t);
	extern obj_t BGl_rgcsetzd2orzd2zz__rgc_setz00(obj_t, obj_t);
	static obj_t BGl_symbol1801z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_symbol1804z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_za2zd2ze3nodez93zz__rgc_treez00(obj_t);
	static obj_t BGl_symbol1738z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_sequencezd2ze3nodez31zz__rgc_treez00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31335ze3ze5zz__rgc_treez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_printzd2followposzd2zz__rgc_treez00(obj_t);
	static obj_t BGl_symbol1740z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_symbol1742z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_z62printzd2followposzb0zz__rgc_treez00(obj_t, obj_t);
	static obj_t BGl_symbol1744z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_symbol1746z00zz__rgc_treez00 = BUNSPEC;
	static obj_t BGl_symbol1748z00zz__rgc_treez00 = BUNSPEC;
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_makezd2rgcsetzd2zz__rgc_setz00(long);
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string1792z00zz__rgc_treez00,
		BgL_bgl_string1792za700za7za7_1807za7, "reverse", 7);
	      DEFINE_STRING(BGl_string1795z00zz__rgc_treez00,
		BgL_bgl_string1795za700za7za7_1808za7, "rgcset->list", 12);
	      DEFINE_STRING(BGl_string1798z00zz__rgc_treez00,
		BgL_bgl_string1798za700za7za7_1809za7, "vector-ref", 10);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regularzd2treezd2ze3nodezd2envz31zz__rgc_treez00,
		BgL_bgl_za762regularza7d2tre1810z00,
		BGl_z62regularzd2treezd2ze3nodez81zz__rgc_treez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1802z00zz__rgc_treez00,
		BgL_bgl_string1802za700za7za7_1811za7, "+fx", 3);
	      DEFINE_STRING(BGl_string1803z00zz__rgc_treez00,
		BgL_bgl_string1803za700za7za7_1812za7,
		"==================================================", 50);
	      DEFINE_STRING(BGl_string1805z00zz__rgc_treez00,
		BgL_bgl_string1805za700za7za7_1813za7, "blop", 4);
	      DEFINE_STRING(BGl_string1806z00zz__rgc_treez00,
		BgL_bgl_string1806za700za7za7_1814za7, "__rgc_tree", 10);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_printzd2followposzd2envz00zz__rgc_treez00,
		BgL_bgl_za762printza7d2follo1815z00,
		BGl_z62printzd2followposzb0zz__rgc_treez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1739z00zz__rgc_treez00,
		BgL_bgl_string1739za700za7za7_1816za7, "epsilon", 7);
	      DEFINE_STRING(BGl_string1741z00zz__rgc_treez00,
		BgL_bgl_string1741za700za7za7_1817za7, "node", 4);
	      DEFINE_STRING(BGl_string1743z00zz__rgc_treez00,
		BgL_bgl_string1743za700za7za7_1818za7, "or", 2);
	      DEFINE_STRING(BGl_string1745z00zz__rgc_treez00,
		BgL_bgl_string1745za700za7za7_1819za7, "sequence", 8);
	      DEFINE_STRING(BGl_string1747z00zz__rgc_treez00,
		BgL_bgl_string1747za700za7za7_1820za7, "*", 1);
	      DEFINE_STRING(BGl_string1749z00zz__rgc_treez00,
		BgL_bgl_string1749za700za7za7_1821za7, "submatch", 8);
	      DEFINE_STRING(BGl_string1751z00zz__rgc_treez00,
		BgL_bgl_string1751za700za7za7_1822za7, "bol", 3);
	      DEFINE_STRING(BGl_string1752z00zz__rgc_treez00,
		BgL_bgl_string1752za700za7za7_1823za7, "RGC:Unknown function", 20);
	      DEFINE_STRING(BGl_string1753z00zz__rgc_treez00,
		BgL_bgl_string1753za700za7za7_1824za7, "RGC:Illegal tree", 16);
	      DEFINE_STRING(BGl_string1756z00zz__rgc_treez00,
		BgL_bgl_string1756za700za7za7_1825za7,
		"========= FOLLOWPOS ==============================", 50);
	      DEFINE_STRING(BGl_string1757z00zz__rgc_treez00,
		BgL_bgl_string1757za700za7za7_1826za7, "number of pos: ", 15);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_resetzd2treez12zd2envz12zz__rgc_treez00,
		BgL_bgl_za762resetza7d2treeza71827za7,
		BGl_z62resetzd2treez12za2zz__rgc_treez00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1760z00zz__rgc_treez00,
		BgL_bgl_string1760za700za7za7_1828za7, "let", 3);
	      DEFINE_BGL_L_PROCEDURE(BGl_proc1754z00zz__rgc_treez00,
		BgL_bgl_za762or2za7d2za7e3node1829za7,
		BGl_z62or2zd2ze3nodez53zz__rgc_treez00);
	      DEFINE_BGL_L_PROCEDURE(BGl_proc1755z00zz__rgc_treez00,
		BgL_bgl_za762sequence2za7d2za71830za7,
		BGl_z62sequence2zd2ze3nodez53zz__rgc_treez00);
	      DEFINE_STRING(BGl_string1764z00zz__rgc_treez00,
		BgL_bgl_string1764za700za7za7_1831za7, "sz", 2);
	      DEFINE_STRING(BGl_string1767z00zz__rgc_treez00,
		BgL_bgl_string1767za700za7za7_1832za7, "vector-length", 13);
	      DEFINE_STRING(BGl_string1769z00zz__rgc_treez00,
		BgL_bgl_string1769za700za7za7_1833za7, "fp", 2);
	      DEFINE_STRING(BGl_string1772z00zz__rgc_treez00,
		BgL_bgl_string1772za700za7za7_1834za7, "loop", 4);
	      DEFINE_STRING(BGl_string1776z00zz__rgc_treez00,
		BgL_bgl_string1776za700za7za7_1835za7, "i", 1);
	      DEFINE_STRING(BGl_string1779z00zz__rgc_treez00,
		BgL_bgl_string1779za700za7za7_1836za7, "if", 2);
	      DEFINE_STRING(BGl_string1782z00zz__rgc_treez00,
		BgL_bgl_string1782za700za7za7_1837za7, "<fx", 3);
	      DEFINE_STRING(BGl_string1785z00zz__rgc_treez00,
		BgL_bgl_string1785za700za7za7_1838za7, "begin", 5);
	      DEFINE_STRING(BGl_string1788z00zz__rgc_treez00,
		BgL_bgl_string1788za700za7za7_1839za7, "print", 5);
	      DEFINE_STRING(BGl_string1789z00zz__rgc_treez00,
		BgL_bgl_string1789za700za7za7_1840za7, ": ", 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_printzd2nodezd2envz00zz__rgc_treez00,
		BgL_bgl_za762printza7d2nodeza71841za7,
		BGl_z62printzd2nodezb0zz__rgc_treez00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_symbol1750z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_list1800z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_symbol1759z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_za2positionzd2numberza2zd2zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_symbol1763z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_symbol1766z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_symbol1768z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_symbol1771z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_symbol1775z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_symbol1778z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_symbol1781z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_symbol1784z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_symbol1787z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_list1758z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_symbol1791z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_symbol1794z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_list1761z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_list1762z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_symbol1797z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_list1765z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_list1770z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_list1773z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_list1774z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_list1777z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_list1780z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_list1783z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_list1786z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_za2followposza2z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_za2currentzd2positionza2zd2zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_za2submatchesza2z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_list1790z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_list1793z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_list1796z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_list1799z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_za2positionsza2z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_symbol1801z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_symbol1804z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_symbol1738z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_symbol1740z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_symbol1742z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_symbol1744z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_symbol1746z00zz__rgc_treez00));
		     ADD_ROOT((void *) (&BGl_symbol1748z00zz__rgc_treez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__rgc_treez00(long
		BgL_checksumz00_2304, char *BgL_fromz00_2305)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__rgc_treez00))
				{
					BGl_requirezd2initializa7ationz75zz__rgc_treez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__rgc_treez00();
					BGl_cnstzd2initzd2zz__rgc_treez00();
					BGl_importedzd2moduleszd2initz00zz__rgc_treez00();
					return BGl_toplevelzd2initzd2zz__rgc_treez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__rgc_treez00(void)
	{
		{	/* Rgc/rgctree.scm 14 */
			BGl_symbol1738z00zz__rgc_treez00 =
				bstring_to_symbol(BGl_string1739z00zz__rgc_treez00);
			BGl_symbol1740z00zz__rgc_treez00 =
				bstring_to_symbol(BGl_string1741z00zz__rgc_treez00);
			BGl_symbol1742z00zz__rgc_treez00 =
				bstring_to_symbol(BGl_string1743z00zz__rgc_treez00);
			BGl_symbol1744z00zz__rgc_treez00 =
				bstring_to_symbol(BGl_string1745z00zz__rgc_treez00);
			BGl_symbol1746z00zz__rgc_treez00 =
				bstring_to_symbol(BGl_string1747z00zz__rgc_treez00);
			BGl_symbol1748z00zz__rgc_treez00 =
				bstring_to_symbol(BGl_string1749z00zz__rgc_treez00);
			BGl_symbol1750z00zz__rgc_treez00 =
				bstring_to_symbol(BGl_string1751z00zz__rgc_treez00);
			BGl_symbol1759z00zz__rgc_treez00 =
				bstring_to_symbol(BGl_string1760z00zz__rgc_treez00);
			BGl_symbol1763z00zz__rgc_treez00 =
				bstring_to_symbol(BGl_string1764z00zz__rgc_treez00);
			BGl_symbol1766z00zz__rgc_treez00 =
				bstring_to_symbol(BGl_string1767z00zz__rgc_treez00);
			BGl_symbol1768z00zz__rgc_treez00 =
				bstring_to_symbol(BGl_string1769z00zz__rgc_treez00);
			BGl_list1765z00zz__rgc_treez00 =
				MAKE_YOUNG_PAIR(BGl_symbol1766z00zz__rgc_treez00,
				MAKE_YOUNG_PAIR(BGl_symbol1768z00zz__rgc_treez00, BNIL));
			BGl_list1762z00zz__rgc_treez00 =
				MAKE_YOUNG_PAIR(BGl_symbol1763z00zz__rgc_treez00,
				MAKE_YOUNG_PAIR(BGl_list1765z00zz__rgc_treez00, BNIL));
			BGl_list1761z00zz__rgc_treez00 =
				MAKE_YOUNG_PAIR(BGl_list1762z00zz__rgc_treez00, BNIL);
			BGl_symbol1771z00zz__rgc_treez00 =
				bstring_to_symbol(BGl_string1772z00zz__rgc_treez00);
			BGl_symbol1775z00zz__rgc_treez00 =
				bstring_to_symbol(BGl_string1776z00zz__rgc_treez00);
			BGl_list1774z00zz__rgc_treez00 =
				MAKE_YOUNG_PAIR(BGl_symbol1775z00zz__rgc_treez00,
				MAKE_YOUNG_PAIR(BINT(0L), BNIL));
			BGl_list1773z00zz__rgc_treez00 =
				MAKE_YOUNG_PAIR(BGl_list1774z00zz__rgc_treez00, BNIL);
			BGl_symbol1778z00zz__rgc_treez00 =
				bstring_to_symbol(BGl_string1779z00zz__rgc_treez00);
			BGl_symbol1781z00zz__rgc_treez00 =
				bstring_to_symbol(BGl_string1782z00zz__rgc_treez00);
			BGl_list1780z00zz__rgc_treez00 =
				MAKE_YOUNG_PAIR(BGl_symbol1781z00zz__rgc_treez00,
				MAKE_YOUNG_PAIR(BGl_symbol1775z00zz__rgc_treez00,
					MAKE_YOUNG_PAIR(BGl_symbol1763z00zz__rgc_treez00, BNIL)));
			BGl_symbol1784z00zz__rgc_treez00 =
				bstring_to_symbol(BGl_string1785z00zz__rgc_treez00);
			BGl_symbol1787z00zz__rgc_treez00 =
				bstring_to_symbol(BGl_string1788z00zz__rgc_treez00);
			BGl_symbol1791z00zz__rgc_treez00 =
				bstring_to_symbol(BGl_string1792z00zz__rgc_treez00);
			BGl_symbol1794z00zz__rgc_treez00 =
				bstring_to_symbol(BGl_string1795z00zz__rgc_treez00);
			BGl_symbol1797z00zz__rgc_treez00 =
				bstring_to_symbol(BGl_string1798z00zz__rgc_treez00);
			BGl_list1796z00zz__rgc_treez00 =
				MAKE_YOUNG_PAIR(BGl_symbol1797z00zz__rgc_treez00,
				MAKE_YOUNG_PAIR(BGl_symbol1768z00zz__rgc_treez00,
					MAKE_YOUNG_PAIR(BGl_symbol1775z00zz__rgc_treez00, BNIL)));
			BGl_list1793z00zz__rgc_treez00 =
				MAKE_YOUNG_PAIR(BGl_symbol1794z00zz__rgc_treez00,
				MAKE_YOUNG_PAIR(BGl_list1796z00zz__rgc_treez00, BNIL));
			BGl_list1790z00zz__rgc_treez00 =
				MAKE_YOUNG_PAIR(BGl_symbol1791z00zz__rgc_treez00,
				MAKE_YOUNG_PAIR(BGl_list1793z00zz__rgc_treez00, BNIL));
			BGl_list1786z00zz__rgc_treez00 =
				MAKE_YOUNG_PAIR(BGl_symbol1787z00zz__rgc_treez00,
				MAKE_YOUNG_PAIR(BGl_symbol1775z00zz__rgc_treez00,
					MAKE_YOUNG_PAIR(BGl_string1789z00zz__rgc_treez00,
						MAKE_YOUNG_PAIR(BGl_list1790z00zz__rgc_treez00, BNIL))));
			BGl_symbol1801z00zz__rgc_treez00 =
				bstring_to_symbol(BGl_string1802z00zz__rgc_treez00);
			BGl_list1800z00zz__rgc_treez00 =
				MAKE_YOUNG_PAIR(BGl_symbol1801z00zz__rgc_treez00,
				MAKE_YOUNG_PAIR(BGl_symbol1775z00zz__rgc_treez00,
					MAKE_YOUNG_PAIR(BINT(1L), BNIL)));
			BGl_list1799z00zz__rgc_treez00 =
				MAKE_YOUNG_PAIR(BGl_symbol1771z00zz__rgc_treez00,
				MAKE_YOUNG_PAIR(BGl_list1800z00zz__rgc_treez00, BNIL));
			BGl_list1783z00zz__rgc_treez00 =
				MAKE_YOUNG_PAIR(BGl_symbol1784z00zz__rgc_treez00,
				MAKE_YOUNG_PAIR(BGl_list1786z00zz__rgc_treez00,
					MAKE_YOUNG_PAIR(BGl_list1799z00zz__rgc_treez00, BNIL)));
			BGl_list1777z00zz__rgc_treez00 =
				MAKE_YOUNG_PAIR(BGl_symbol1778z00zz__rgc_treez00,
				MAKE_YOUNG_PAIR(BGl_list1780z00zz__rgc_treez00,
					MAKE_YOUNG_PAIR(BGl_list1783z00zz__rgc_treez00, BNIL)));
			BGl_list1770z00zz__rgc_treez00 =
				MAKE_YOUNG_PAIR(BGl_symbol1759z00zz__rgc_treez00,
				MAKE_YOUNG_PAIR(BGl_symbol1771z00zz__rgc_treez00,
					MAKE_YOUNG_PAIR(BGl_list1773z00zz__rgc_treez00,
						MAKE_YOUNG_PAIR(BGl_list1777z00zz__rgc_treez00, BNIL))));
			BGl_list1758z00zz__rgc_treez00 =
				MAKE_YOUNG_PAIR(BGl_symbol1759z00zz__rgc_treez00,
				MAKE_YOUNG_PAIR(BGl_list1761z00zz__rgc_treez00,
					MAKE_YOUNG_PAIR(BGl_list1770z00zz__rgc_treez00, BNIL)));
			return (BGl_symbol1804z00zz__rgc_treez00 =
				bstring_to_symbol(BGl_string1805z00zz__rgc_treez00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__rgc_treez00(void)
	{
		{	/* Rgc/rgctree.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__rgc_treez00(void)
	{
		{	/* Rgc/rgctree.scm 14 */
			BGl_za2positionzd2numberza2zd2zz__rgc_treez00 = BUNSPEC;
			BGl_za2positionsza2z00zz__rgc_treez00 = BUNSPEC;
			BGl_za2submatchesza2z00zz__rgc_treez00 = BUNSPEC;
			BGl_za2currentzd2positionza2zd2zz__rgc_treez00 = BUNSPEC;
			return (BGl_za2followposza2z00zz__rgc_treez00 = BUNSPEC, BUNSPEC);
		}

	}



/* regular-tree->node */
	BGL_EXPORTED_DEF obj_t BGl_regularzd2treezd2ze3nodeze3zz__rgc_treez00(obj_t
		BgL_treez00_17)
	{
		{	/* Rgc/rgctree.scm 59 */
			BGl_za2positionzd2numberza2zd2zz__rgc_treez00 =
				BINT(BGl_regularzd2treezd2positionzd2numberzd2zz__rgc_treez00
				(BgL_treez00_17));
			BGl_initzd2positionsz12zc0zz__rgc_treez00();
			BGl_initzd2followposz12zc0zz__rgc_treez00();
			{	/* Rgc/rgctree.scm 67 */
				obj_t BgL_treez00_1226;

				BgL_treez00_1226 = BGl_treezd2ze3nodez31zz__rgc_treez00(BgL_treez00_17);
				{	/* Rgc/rgctree.scm 69 */
					obj_t BgL_val1_1089z00_1228;
					obj_t BgL_val2_1090z00_1229;
					obj_t BgL_val3_1091z00_1230;

					BgL_val1_1089z00_1228 = BGl_za2followposza2z00zz__rgc_treez00;
					BgL_val2_1090z00_1229 = BGl_za2positionsza2z00zz__rgc_treez00;
					BgL_val3_1091z00_1230 = BGl_za2submatchesza2z00zz__rgc_treez00;
					{	/* Rgc/rgctree.scm 69 */
						int BgL_tmpz00_2383;

						BgL_tmpz00_2383 = (int) (4L);
						BGL_MVALUES_NUMBER_SET(BgL_tmpz00_2383);
					}
					{	/* Rgc/rgctree.scm 69 */
						int BgL_tmpz00_2386;

						BgL_tmpz00_2386 = (int) (1L);
						BGL_MVALUES_VAL_SET(BgL_tmpz00_2386, BgL_val1_1089z00_1228);
					}
					{	/* Rgc/rgctree.scm 69 */
						int BgL_tmpz00_2389;

						BgL_tmpz00_2389 = (int) (2L);
						BGL_MVALUES_VAL_SET(BgL_tmpz00_2389, BgL_val2_1090z00_1229);
					}
					{	/* Rgc/rgctree.scm 69 */
						int BgL_tmpz00_2392;

						BgL_tmpz00_2392 = (int) (3L);
						BGL_MVALUES_VAL_SET(BgL_tmpz00_2392, BgL_val3_1091z00_1230);
					}
					return BgL_treez00_1226;
				}
			}
		}

	}



/* &regular-tree->node */
	obj_t BGl_z62regularzd2treezd2ze3nodez81zz__rgc_treez00(obj_t BgL_envz00_2225,
		obj_t BgL_treez00_2226)
	{
		{	/* Rgc/rgctree.scm 59 */
			return BGl_regularzd2treezd2ze3nodeze3zz__rgc_treez00(BgL_treez00_2226);
		}

	}



/* regular-tree-position-number */
	long BGl_regularzd2treezd2positionzd2numberzd2zz__rgc_treez00(obj_t
		BgL_treez00_18)
	{
		{	/* Rgc/rgctree.scm 85 */
			return BGl_loopze71ze7zz__rgc_treez00(BgL_treez00_18, 0L);
		}

	}



/* loop~1 */
	long BGl_loopze71ze7zz__rgc_treez00(obj_t BgL_treez00_1232,
		long BgL_numz00_1233)
	{
		{	/* Rgc/rgctree.scm 86 */
		BGl_loopze71ze7zz__rgc_treez00:
			if (NULLP(BgL_treez00_1232))
				{	/* Rgc/rgctree.scm 89 */
					return BgL_numz00_1233;
				}
			else
				{	/* Rgc/rgctree.scm 91 */
					bool_t BgL_test1844z00_2399;

					{	/* Rgc/rgctree.scm 91 */
						obj_t BgL_tmpz00_2400;

						BgL_tmpz00_2400 = CAR(((obj_t) BgL_treez00_1232));
						BgL_test1844z00_2399 = PAIRP(BgL_tmpz00_2400);
					}
					if (BgL_test1844z00_2399)
						{
							long BgL_numz00_2407;
							obj_t BgL_treez00_2404;

							BgL_treez00_2404 = CDR(((obj_t) BgL_treez00_1232));
							BgL_numz00_2407 =
								BGl_loopze71ze7zz__rgc_treez00(CAR(
									((obj_t) BgL_treez00_1232)), BgL_numz00_1233);
							BgL_numz00_1233 = BgL_numz00_2407;
							BgL_treez00_1232 = BgL_treez00_2404;
							goto BGl_loopze71ze7zz__rgc_treez00;
						}
					else
						{	/* Rgc/rgctree.scm 91 */
							if (INTEGERP(CAR(((obj_t) BgL_treez00_1232))))
								{
									long BgL_numz00_2418;
									obj_t BgL_treez00_2415;

									BgL_treez00_2415 = CDR(((obj_t) BgL_treez00_1232));
									BgL_numz00_2418 = (BgL_numz00_1233 + 1L);
									BgL_numz00_1233 = BgL_numz00_2418;
									BgL_treez00_1232 = BgL_treez00_2415;
									goto BGl_loopze71ze7zz__rgc_treez00;
								}
							else
								{
									obj_t BgL_treez00_2420;

									BgL_treez00_2420 = CDR(((obj_t) BgL_treez00_1232));
									BgL_treez00_1232 = BgL_treez00_2420;
									goto BGl_loopze71ze7zz__rgc_treez00;
								}
						}
				}
		}

	}



/* init-positions! */
	obj_t BGl_initzd2positionsz12zc0zz__rgc_treez00(void)
	{
		{	/* Rgc/rgctree.scm 101 */
			BGl_za2currentzd2positionza2zd2zz__rgc_treez00 = BINT(-1L);
			BGl_za2positionsza2z00zz__rgc_treez00 =
				make_vector(
				(long) CINT(BGl_za2positionzd2numberza2zd2zz__rgc_treez00), BINT(-1L));
			return (BGl_za2submatchesza2z00zz__rgc_treez00 =
				make_vector(
					(long) CINT(BGl_za2positionzd2numberza2zd2zz__rgc_treez00), BNIL),
				BUNSPEC);
		}

	}



/* tree->node */
	obj_t BGl_treezd2ze3nodez31zz__rgc_treez00(obj_t BgL_treez00_20)
	{
		{	/* Rgc/rgctree.scm 122 */
			if (INTEGERP(BgL_treez00_20))
				{	/* Rgc/rgctree.scm 124 */
					return BGl_integerzd2ze3nodez31zz__rgc_treez00(BgL_treez00_20);
				}
			else
				{	/* Rgc/rgctree.scm 124 */
					if ((BgL_treez00_20 == BGl_symbol1738z00zz__rgc_treez00))
						{	/* Rgc/rgctree.scm 154 */
							obj_t BgL_firstposz00_1845;
							obj_t BgL_lastposz00_1846;

							BgL_firstposz00_1845 =
								BGl_makezd2rgcsetzd2zz__rgc_setz00(
								(long) CINT(BGl_za2positionzd2numberza2zd2zz__rgc_treez00));
							BgL_lastposz00_1846 =
								BGl_makezd2rgcsetzd2zz__rgc_setz00(
								(long) CINT(BGl_za2positionzd2numberza2zd2zz__rgc_treez00));
							{	/* Rgc/rgctree.scm 156 */
								obj_t BgL_newz00_1847;

								BgL_newz00_1847 =
									create_struct(BGl_symbol1740z00zz__rgc_treez00, (int) (3L));
								{	/* Rgc/rgctree.scm 156 */
									int BgL_tmpz00_2440;

									BgL_tmpz00_2440 = (int) (2L);
									STRUCT_SET(BgL_newz00_1847, BgL_tmpz00_2440, BTRUE);
								}
								{	/* Rgc/rgctree.scm 156 */
									int BgL_tmpz00_2443;

									BgL_tmpz00_2443 = (int) (1L);
									STRUCT_SET(BgL_newz00_1847, BgL_tmpz00_2443,
										BgL_lastposz00_1846);
								}
								{	/* Rgc/rgctree.scm 156 */
									int BgL_tmpz00_2446;

									BgL_tmpz00_2446 = (int) (0L);
									STRUCT_SET(BgL_newz00_1847, BgL_tmpz00_2446,
										BgL_firstposz00_1845);
								}
								return BgL_newz00_1847;
							}
						}
					else
						{	/* Rgc/rgctree.scm 126 */
							if (PAIRP(BgL_treez00_20))
								{	/* Rgc/rgctree.scm 131 */
									obj_t BgL_casezd2valuezd2_1251;

									BgL_casezd2valuezd2_1251 = CAR(BgL_treez00_20);
									if (
										(BgL_casezd2valuezd2_1251 ==
											BGl_symbol1742z00zz__rgc_treez00))
										{	/* Rgc/rgctree.scm 131 */
											BGL_TAIL return
												BGl_orzd2ze3nodez31zz__rgc_treez00(CDR(BgL_treez00_20));
										}
									else
										{	/* Rgc/rgctree.scm 131 */
											if (
												(BgL_casezd2valuezd2_1251 ==
													BGl_symbol1744z00zz__rgc_treez00))
												{	/* Rgc/rgctree.scm 131 */
													BGL_TAIL return
														BGl_sequencezd2ze3nodez31zz__rgc_treez00(CDR
														(BgL_treez00_20));
												}
											else
												{	/* Rgc/rgctree.scm 131 */
													if (
														(BgL_casezd2valuezd2_1251 ==
															BGl_symbol1746z00zz__rgc_treez00))
														{	/* Rgc/rgctree.scm 131 */
															BGL_TAIL return
																BGl_za2zd2ze3nodez93zz__rgc_treez00(CAR(CDR
																	(BgL_treez00_20)));
														}
													else
														{	/* Rgc/rgctree.scm 131 */
															if (
																(BgL_casezd2valuezd2_1251 ==
																	BGl_symbol1748z00zz__rgc_treez00))
																{	/* Rgc/rgctree.scm 131 */
																	BGL_TAIL return
																		BGl_submatchzd2ze3nodez31zz__rgc_treez00(CDR
																		(BgL_treez00_20));
																}
															else
																{	/* Rgc/rgctree.scm 131 */
																	if (
																		(BgL_casezd2valuezd2_1251 ==
																			BGl_symbol1750z00zz__rgc_treez00))
																		{	/* Rgc/rgctree.scm 136 */
																			obj_t BgL_arg1238z00_1261;

																			BgL_arg1238z00_1261 = CDR(BgL_treez00_20);
																			{	/* Rgc/rgctree.scm 242 */
																				obj_t BgL_nodez00_1865;

																				BgL_nodez00_1865 =
																					BGl_treezd2ze3nodez31zz__rgc_treez00
																					(BgL_arg1238z00_1261);
																				{	/* Rgc/rgctree.scm 242 */
																					obj_t BgL_firstposz00_1866;

																					BgL_firstposz00_1866 =
																						STRUCT_REF(
																						((obj_t) BgL_nodez00_1865),
																						(int) (0L));
																					{	/* Rgc/rgctree.scm 243 */
																						obj_t BgL_lastposz00_1867;

																						BgL_lastposz00_1867 =
																							STRUCT_REF(
																							((obj_t) BgL_nodez00_1865),
																							(int) (1L));
																						{	/* Rgc/rgctree.scm 244 */

																							return
																								BGL_PROCEDURE_CALL3
																								(BgL_nodez00_1865,
																								BgL_firstposz00_1866,
																								BgL_lastposz00_1867, BFALSE);
																						}
																					}
																				}
																			}
																		}
																	else
																		{	/* Rgc/rgctree.scm 131 */
																			return
																				BGl_errorz00zz__errorz00(BFALSE,
																				BGl_string1752z00zz__rgc_treez00,
																				BgL_treez00_20);
																		}
																}
														}
												}
										}
								}
							else
								{	/* Rgc/rgctree.scm 128 */
									return
										BGl_errorz00zz__errorz00(BFALSE,
										BGl_string1753z00zz__rgc_treez00, BgL_treez00_20);
								}
						}
				}
		}

	}



/* integer->node */
	obj_t BGl_integerzd2ze3nodez31zz__rgc_treez00(obj_t BgL_treez00_21)
	{
		{	/* Rgc/rgctree.scm 142 */
			{	/* Rgc/rgctree.scm 143 */
				obj_t BgL_positionz00_1262;
				obj_t BgL_firstposz00_1263;
				obj_t BgL_lastposz00_1264;

				BGl_za2currentzd2positionza2zd2zz__rgc_treez00 =
					ADDFX(BGl_za2currentzd2positionza2zd2zz__rgc_treez00, BINT(1L));
				VECTOR_SET(BGl_za2positionsza2z00zz__rgc_treez00,
					(long) CINT(BGl_za2currentzd2positionza2zd2zz__rgc_treez00),
					BgL_treez00_21);
				BgL_positionz00_1262 = BGl_za2currentzd2positionza2zd2zz__rgc_treez00;
				BgL_firstposz00_1263 =
					BGl_makezd2rgcsetzd2zz__rgc_setz00(
					(long) CINT(BGl_za2positionzd2numberza2zd2zz__rgc_treez00));
				BgL_lastposz00_1264 =
					BGl_makezd2rgcsetzd2zz__rgc_setz00(
					(long) CINT(BGl_za2positionzd2numberza2zd2zz__rgc_treez00));
				BGl_rgcsetzd2addz12zc0zz__rgc_setz00(BgL_firstposz00_1263,
					(long) CINT(BgL_positionz00_1262));
				BGl_rgcsetzd2addz12zc0zz__rgc_setz00(BgL_lastposz00_1264,
					(long) CINT(BgL_positionz00_1262));
				{	/* Rgc/rgctree.scm 148 */
					obj_t BgL_newz00_1873;

					BgL_newz00_1873 =
						create_struct(BGl_symbol1740z00zz__rgc_treez00, (int) (3L));
					{	/* Rgc/rgctree.scm 148 */
						int BgL_tmpz00_2501;

						BgL_tmpz00_2501 = (int) (2L);
						STRUCT_SET(BgL_newz00_1873, BgL_tmpz00_2501, BFALSE);
					}
					{	/* Rgc/rgctree.scm 148 */
						int BgL_tmpz00_2504;

						BgL_tmpz00_2504 = (int) (1L);
						STRUCT_SET(BgL_newz00_1873, BgL_tmpz00_2504, BgL_lastposz00_1264);
					}
					{	/* Rgc/rgctree.scm 148 */
						int BgL_tmpz00_2507;

						BgL_tmpz00_2507 = (int) (0L);
						STRUCT_SET(BgL_newz00_1873, BgL_tmpz00_2507, BgL_firstposz00_1263);
					}
					return BgL_newz00_1873;
				}
			}
		}

	}



/* binary->node */
	obj_t BGl_binaryzd2ze3nodez31zz__rgc_treez00(obj_t BgL_binzd2opzd2_23,
		obj_t BgL_tsz00_24)
	{
		{	/* Rgc/rgctree.scm 161 */
			if (NULLP(BgL_tsz00_24))
				{	/* Rgc/rgctree.scm 162 */
					return
						BGl_treezd2ze3nodez31zz__rgc_treez00
						(BGl_symbol1738z00zz__rgc_treez00);
				}
			else
				{	/* Rgc/rgctree.scm 162 */
					BGL_TAIL return
						BGl_loopze70ze7zz__rgc_treez00(BgL_binzd2opzd2_23, BgL_tsz00_24);
				}
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zz__rgc_treez00(obj_t BgL_binzd2opzd2_2259,
		obj_t BgL_tsz00_1269)
	{
		{	/* Rgc/rgctree.scm 164 */
			if (NULLP(CDR(((obj_t) BgL_tsz00_1269))))
				{	/* Rgc/rgctree.scm 166 */
					obj_t BgL_arg1244z00_1273;

					BgL_arg1244z00_1273 = CAR(((obj_t) BgL_tsz00_1269));
					return BGl_treezd2ze3nodez31zz__rgc_treez00(BgL_arg1244z00_1273);
				}
			else
				{	/* Rgc/rgctree.scm 167 */
					obj_t BgL_arg1248z00_1274;
					obj_t BgL_arg1249z00_1275;

					{	/* Rgc/rgctree.scm 167 */
						obj_t BgL_arg1252z00_1276;

						BgL_arg1252z00_1276 = CAR(((obj_t) BgL_tsz00_1269));
						BgL_arg1248z00_1274 =
							BGl_treezd2ze3nodez31zz__rgc_treez00(BgL_arg1252z00_1276);
					}
					{	/* Rgc/rgctree.scm 167 */
						obj_t BgL_arg1268z00_1277;

						BgL_arg1268z00_1277 = CDR(((obj_t) BgL_tsz00_1269));
						BgL_arg1249z00_1275 =
							BGl_loopze70ze7zz__rgc_treez00(BgL_binzd2opzd2_2259,
							BgL_arg1268z00_1277);
					}
					return
						((obj_t(*)(obj_t, obj_t,
								obj_t))
						PROCEDURE_L_ENTRY(BgL_binzd2opzd2_2259)) (BgL_binzd2opzd2_2259,
						BgL_arg1248z00_1274, BgL_arg1249z00_1275);
				}
		}

	}



/* or->node */
	obj_t BGl_orzd2ze3nodez31zz__rgc_treez00(obj_t BgL_tsz00_25)
	{
		{	/* Rgc/rgctree.scm 172 */
			return
				BGl_binaryzd2ze3nodez31zz__rgc_treez00(BGl_proc1754z00zz__rgc_treez00,
				BgL_tsz00_25);
		}

	}



/* &or2->node */
	obj_t BGl_z62or2zd2ze3nodez53zz__rgc_treez00(obj_t BgL_envz00_2228,
		obj_t BgL_n1z00_2229, obj_t BgL_n2z00_2230)
	{
		{	/* Rgc/rgctree.scm 177 */
			{	/* Rgc/rgctree.scm 174 */
				obj_t BgL_firstposz00_2284;

				{	/* Rgc/rgctree.scm 174 */
					obj_t BgL_arg1305z00_2285;
					obj_t BgL_arg1306z00_2286;

					BgL_arg1305z00_2285 =
						STRUCT_REF(((obj_t) BgL_n1z00_2229), (int) (0L));
					BgL_arg1306z00_2286 =
						STRUCT_REF(((obj_t) BgL_n2z00_2230), (int) (0L));
					BgL_firstposz00_2284 =
						BGl_rgcsetzd2orzd2zz__rgc_setz00(BgL_arg1305z00_2285,
						BgL_arg1306z00_2286);
				}
				{	/* Rgc/rgctree.scm 174 */
					obj_t BgL_lastposz00_2287;

					{	/* Rgc/rgctree.scm 175 */
						obj_t BgL_arg1284z00_2288;
						obj_t BgL_arg1304z00_2289;

						BgL_arg1284z00_2288 =
							STRUCT_REF(((obj_t) BgL_n1z00_2229), (int) (1L));
						BgL_arg1304z00_2289 =
							STRUCT_REF(((obj_t) BgL_n2z00_2230), (int) (1L));
						BgL_lastposz00_2287 =
							BGl_rgcsetzd2orzd2zz__rgc_setz00(BgL_arg1284z00_2288,
							BgL_arg1304z00_2289);
					}
					{	/* Rgc/rgctree.scm 175 */
						obj_t BgL_nullablezf3zf3_2290;

						{	/* Rgc/rgctree.scm 176 */
							obj_t BgL__ortest_1039z00_2291;

							BgL__ortest_1039z00_2291 =
								STRUCT_REF(((obj_t) BgL_n1z00_2229), (int) (2L));
							if (CBOOL(BgL__ortest_1039z00_2291))
								{	/* Rgc/rgctree.scm 176 */
									BgL_nullablezf3zf3_2290 = BgL__ortest_1039z00_2291;
								}
							else
								{	/* Rgc/rgctree.scm 176 */
									BgL_nullablezf3zf3_2290 =
										STRUCT_REF(((obj_t) BgL_n2z00_2230), (int) (2L));
						}}
						{	/* Rgc/rgctree.scm 176 */

							{	/* Rgc/rgctree.scm 177 */
								obj_t BgL_newz00_2292;

								BgL_newz00_2292 =
									create_struct(BGl_symbol1740z00zz__rgc_treez00, (int) (3L));
								{	/* Rgc/rgctree.scm 177 */
									int BgL_tmpz00_2557;

									BgL_tmpz00_2557 = (int) (2L);
									STRUCT_SET(BgL_newz00_2292, BgL_tmpz00_2557,
										BgL_nullablezf3zf3_2290);
								}
								{	/* Rgc/rgctree.scm 177 */
									int BgL_tmpz00_2560;

									BgL_tmpz00_2560 = (int) (1L);
									STRUCT_SET(BgL_newz00_2292, BgL_tmpz00_2560,
										BgL_lastposz00_2287);
								}
								{	/* Rgc/rgctree.scm 177 */
									int BgL_tmpz00_2563;

									BgL_tmpz00_2563 = (int) (0L);
									STRUCT_SET(BgL_newz00_2292, BgL_tmpz00_2563,
										BgL_firstposz00_2284);
								}
								return BgL_newz00_2292;
							}
						}
					}
				}
			}
		}

	}



/* sequence->node */
	obj_t BGl_sequencezd2ze3nodez31zz__rgc_treez00(obj_t BgL_tsz00_26)
	{
		{	/* Rgc/rgctree.scm 183 */
			return
				BGl_binaryzd2ze3nodez31zz__rgc_treez00(BGl_proc1755z00zz__rgc_treez00,
				BgL_tsz00_26);
		}

	}



/* &sequence2->node */
	obj_t BGl_z62sequence2zd2ze3nodez53zz__rgc_treez00(obj_t BgL_envz00_2233,
		obj_t BgL_n1z00_2234, obj_t BgL_n2z00_2235)
	{
		{	/* Rgc/rgctree.scm 197 */
			{	/* Rgc/rgctree.scm 185 */
				obj_t BgL_firstposz00_2293;
				obj_t BgL_lastposz00_2294;
				obj_t BgL_nullablezf3zf3_2295;

				if (CBOOL(STRUCT_REF(((obj_t) BgL_n1z00_2234), (int) (2L))))
					{	/* Rgc/rgctree.scm 186 */
						obj_t BgL_arg1314z00_2296;
						obj_t BgL_arg1315z00_2297;

						BgL_arg1314z00_2296 =
							STRUCT_REF(((obj_t) BgL_n1z00_2234), (int) (0L));
						BgL_arg1315z00_2297 =
							STRUCT_REF(((obj_t) BgL_n2z00_2235), (int) (0L));
						BgL_firstposz00_2293 =
							BGl_rgcsetzd2orzd2zz__rgc_setz00(BgL_arg1314z00_2296,
							BgL_arg1315z00_2297);
					}
				else
					{	/* Rgc/rgctree.scm 185 */
						BgL_firstposz00_2293 =
							STRUCT_REF(((obj_t) BgL_n1z00_2234), (int) (0L));
					}
				if (CBOOL(STRUCT_REF(((obj_t) BgL_n2z00_2235), (int) (2L))))
					{	/* Rgc/rgctree.scm 189 */
						obj_t BgL_arg1317z00_2298;
						obj_t BgL_arg1318z00_2299;

						BgL_arg1317z00_2298 =
							STRUCT_REF(((obj_t) BgL_n1z00_2234), (int) (1L));
						BgL_arg1318z00_2299 =
							STRUCT_REF(((obj_t) BgL_n2z00_2235), (int) (1L));
						BgL_lastposz00_2294 =
							BGl_rgcsetzd2orzd2zz__rgc_setz00(BgL_arg1317z00_2298,
							BgL_arg1318z00_2299);
					}
				else
					{	/* Rgc/rgctree.scm 188 */
						BgL_lastposz00_2294 =
							STRUCT_REF(((obj_t) BgL_n2z00_2235), (int) (1L));
					}
				if (CBOOL(STRUCT_REF(((obj_t) BgL_n2z00_2235), (int) (2L))))
					{	/* Rgc/rgctree.scm 191 */
						BgL_nullablezf3zf3_2295 =
							STRUCT_REF(((obj_t) BgL_n1z00_2234), (int) (2L));
					}
				else
					{	/* Rgc/rgctree.scm 191 */
						BgL_nullablezf3zf3_2295 = BFALSE;
					}
				{	/* Rgc/rgctree.scm 193 */
					obj_t BgL_arg1309z00_2300;

					BgL_arg1309z00_2300 =
						STRUCT_REF(((obj_t) BgL_n1z00_2234), (int) (1L));
					{	/* Rgc/rgctree.scm 194 */
						obj_t BgL_zc3z04anonymousza31310ze3z87_2301;

						BgL_zc3z04anonymousza31310ze3z87_2301 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31310ze3ze5zz__rgc_treez00, (int) (1L),
							(int) (1L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31310ze3z87_2301, (int) (0L),
							BgL_n2z00_2235);
						BGl_forzd2eachzd2rgcsetz00zz__rgc_setz00
							(BgL_zc3z04anonymousza31310ze3z87_2301, BgL_arg1309z00_2300);
				}}
				{	/* Rgc/rgctree.scm 197 */
					obj_t BgL_newz00_2302;

					BgL_newz00_2302 =
						create_struct(BGl_symbol1740z00zz__rgc_treez00, (int) (3L));
					{	/* Rgc/rgctree.scm 197 */
						int BgL_tmpz00_2616;

						BgL_tmpz00_2616 = (int) (2L);
						STRUCT_SET(BgL_newz00_2302, BgL_tmpz00_2616,
							BgL_nullablezf3zf3_2295);
					}
					{	/* Rgc/rgctree.scm 197 */
						int BgL_tmpz00_2619;

						BgL_tmpz00_2619 = (int) (1L);
						STRUCT_SET(BgL_newz00_2302, BgL_tmpz00_2619, BgL_lastposz00_2294);
					}
					{	/* Rgc/rgctree.scm 197 */
						int BgL_tmpz00_2622;

						BgL_tmpz00_2622 = (int) (0L);
						STRUCT_SET(BgL_newz00_2302, BgL_tmpz00_2622, BgL_firstposz00_2293);
					}
					return BgL_newz00_2302;
				}
			}
		}

	}



/* &<@anonymous:1310> */
	obj_t BGl_z62zc3z04anonymousza31310ze3ze5zz__rgc_treez00(obj_t
		BgL_envz00_2236, obj_t BgL_iz00_2238)
	{
		{	/* Rgc/rgctree.scm 193 */
			{	/* Rgc/rgctree.scm 194 */
				obj_t BgL_n2z00_2237;

				BgL_n2z00_2237 = PROCEDURE_REF(BgL_envz00_2236, (int) (0L));
				{	/* Rgc/rgctree.scm 194 */
					obj_t BgL_arg1311z00_2303;

					BgL_arg1311z00_2303 =
						STRUCT_REF(((obj_t) BgL_n2z00_2237), (int) (0L));
					return
						BGl_rgcsetzd2orz12zc0zz__rgc_setz00(VECTOR_REF
						(BGl_za2followposza2z00zz__rgc_treez00, (long) CINT(BgL_iz00_2238)),
						BgL_arg1311z00_2303);
		}}}

	}



/* *->node */
	obj_t BGl_za2zd2ze3nodez93zz__rgc_treez00(obj_t BgL_exprz00_27)
	{
		{	/* Rgc/rgctree.scm 203 */
			{	/* Rgc/rgctree.scm 204 */
				obj_t BgL_subzd2nodezd2_1314;

				BgL_subzd2nodezd2_1314 =
					BGl_treezd2ze3nodez31zz__rgc_treez00(BgL_exprz00_27);
				{	/* Rgc/rgctree.scm 204 */
					obj_t BgL_firstposz00_1315;

					BgL_firstposz00_1315 =
						STRUCT_REF(((obj_t) BgL_subzd2nodezd2_1314), (int) (0L));
					{	/* Rgc/rgctree.scm 205 */
						obj_t BgL_lastposz00_1316;

						BgL_lastposz00_1316 =
							STRUCT_REF(((obj_t) BgL_subzd2nodezd2_1314), (int) (1L));
						{	/* Rgc/rgctree.scm 206 */

							{	/* Rgc/rgctree.scm 209 */
								obj_t BgL_zc3z04anonymousza31320ze3z87_2239;

								BgL_zc3z04anonymousza31320ze3z87_2239 =
									MAKE_FX_PROCEDURE
									(BGl_z62zc3z04anonymousza31320ze3ze5zz__rgc_treez00,
									(int) (1L), (int) (1L));
								PROCEDURE_SET(BgL_zc3z04anonymousza31320ze3z87_2239, (int) (0L),
									BgL_firstposz00_1315);
								BGl_forzd2eachzd2rgcsetz00zz__rgc_setz00
									(BgL_zc3z04anonymousza31320ze3z87_2239, BgL_lastposz00_1316);
							}
							{	/* Rgc/rgctree.scm 212 */
								obj_t BgL_newz00_1922;

								BgL_newz00_1922 =
									create_struct(BGl_symbol1740z00zz__rgc_treez00, (int) (3L));
								{	/* Rgc/rgctree.scm 212 */
									int BgL_tmpz00_2648;

									BgL_tmpz00_2648 = (int) (2L);
									STRUCT_SET(BgL_newz00_1922, BgL_tmpz00_2648, BTRUE);
								}
								{	/* Rgc/rgctree.scm 212 */
									int BgL_tmpz00_2651;

									BgL_tmpz00_2651 = (int) (1L);
									STRUCT_SET(BgL_newz00_1922, BgL_tmpz00_2651,
										BgL_lastposz00_1316);
								}
								{	/* Rgc/rgctree.scm 212 */
									int BgL_tmpz00_2654;

									BgL_tmpz00_2654 = (int) (0L);
									STRUCT_SET(BgL_newz00_1922, BgL_tmpz00_2654,
										BgL_firstposz00_1315);
								}
								return BgL_newz00_1922;
							}
						}
					}
				}
			}
		}

	}



/* &<@anonymous:1320> */
	obj_t BGl_z62zc3z04anonymousza31320ze3ze5zz__rgc_treez00(obj_t
		BgL_envz00_2240, obj_t BgL_iz00_2242)
	{
		{	/* Rgc/rgctree.scm 208 */
			{	/* Rgc/rgctree.scm 268 */
				obj_t BgL_firstposz00_2241;

				BgL_firstposz00_2241 = PROCEDURE_REF(BgL_envz00_2240, (int) (0L));
				return
					BGl_rgcsetzd2orz12zc0zz__rgc_setz00(VECTOR_REF
					(BGl_za2followposza2z00zz__rgc_treez00, (long) CINT(BgL_iz00_2242)),
					BgL_firstposz00_2241);
		}}

	}



/* submatch->node */
	obj_t BGl_submatchzd2ze3nodez31zz__rgc_treez00(obj_t BgL_exprz00_28)
	{
		{	/* Rgc/rgctree.scm 217 */
			if (PAIRP(BgL_exprz00_28))
				{	/* Rgc/rgctree.scm 218 */
					obj_t BgL_cdrzd2111zd2_1328;

					BgL_cdrzd2111zd2_1328 = CDR(((obj_t) BgL_exprz00_28));
					if (PAIRP(BgL_cdrzd2111zd2_1328))
						{	/* Rgc/rgctree.scm 218 */
							obj_t BgL_cdrzd2116zd2_1330;

							BgL_cdrzd2116zd2_1330 = CDR(BgL_cdrzd2111zd2_1328);
							if (PAIRP(BgL_cdrzd2116zd2_1330))
								{	/* Rgc/rgctree.scm 218 */
									if (NULLP(CDR(BgL_cdrzd2116zd2_1330)))
										{	/* Rgc/rgctree.scm 218 */
											obj_t BgL_arg1327z00_1334;
											obj_t BgL_arg1328z00_1335;
											obj_t BgL_arg1329z00_1336;

											BgL_arg1327z00_1334 = CAR(((obj_t) BgL_exprz00_28));
											BgL_arg1328z00_1335 = CAR(BgL_cdrzd2111zd2_1328);
											BgL_arg1329z00_1336 = CAR(BgL_cdrzd2116zd2_1330);
											{	/* Rgc/rgctree.scm 220 */
												obj_t BgL_nodez00_1943;

												BgL_nodez00_1943 =
													BGl_treezd2ze3nodez31zz__rgc_treez00
													(BgL_arg1329z00_1336);
												{	/* Rgc/rgctree.scm 220 */
													obj_t BgL_firstposz00_1944;

													BgL_firstposz00_1944 =
														STRUCT_REF(((obj_t) BgL_nodez00_1943), (int) (0L));
													{	/* Rgc/rgctree.scm 221 */
														obj_t BgL_lastposz00_1945;

														BgL_lastposz00_1945 =
															STRUCT_REF(
															((obj_t) BgL_nodez00_1943), (int) (1L));
														{	/* Rgc/rgctree.scm 222 */
															obj_t BgL_nullablezf3zf3_1946;

															BgL_nullablezf3zf3_1946 =
																STRUCT_REF(
																((obj_t) BgL_nodez00_1943), (int) (2L));
															{	/* Rgc/rgctree.scm 223 */

																{	/* Rgc/rgctree.scm 229 */
																	obj_t BgL_zc3z04anonymousza31333ze3z87_2244;

																	BgL_zc3z04anonymousza31333ze3z87_2244 =
																		MAKE_FX_PROCEDURE
																		(BGl_z62zc3z04anonymousza31333ze3ze5zz__rgc_treez00,
																		(int) (1L), (int) (3L));
																	PROCEDURE_SET
																		(BgL_zc3z04anonymousza31333ze3z87_2244,
																		(int) (0L), BgL_nullablezf3zf3_1946);
																	PROCEDURE_SET
																		(BgL_zc3z04anonymousza31333ze3z87_2244,
																		(int) (1L), BgL_arg1327z00_1334);
																	PROCEDURE_SET
																		(BgL_zc3z04anonymousza31333ze3z87_2244,
																		(int) (2L), BgL_arg1328z00_1335);
																	BGl_forzd2eachzd2rgcsetz00zz__rgc_setz00
																		(BgL_zc3z04anonymousza31333ze3z87_2244,
																		BgL_firstposz00_1944);
																}
																{	/* Rgc/rgctree.scm 232 */
																	obj_t BgL_zc3z04anonymousza31335ze3z87_2243;

																	BgL_zc3z04anonymousza31335ze3z87_2243 =
																		MAKE_FX_PROCEDURE
																		(BGl_z62zc3z04anonymousza31335ze3ze5zz__rgc_treez00,
																		(int) (1L), (int) (2L));
																	PROCEDURE_SET
																		(BgL_zc3z04anonymousza31335ze3z87_2243,
																		(int) (0L), BgL_arg1327z00_1334);
																	PROCEDURE_SET
																		(BgL_zc3z04anonymousza31335ze3z87_2243,
																		(int) (1L), BgL_arg1328z00_1335);
																	BGl_forzd2eachzd2rgcsetz00zz__rgc_setz00
																		(BgL_zc3z04anonymousza31335ze3z87_2243,
																		BgL_lastposz00_1945);
																}
																return BgL_nodez00_1943;
															}
														}
													}
												}
											}
										}
									else
										{	/* Rgc/rgctree.scm 218 */
											return
												BGl_errorz00zz__errorz00(BFALSE,
												BGl_string1752z00zz__rgc_treez00, BgL_exprz00_28);
										}
								}
							else
								{	/* Rgc/rgctree.scm 218 */
									return
										BGl_errorz00zz__errorz00(BFALSE,
										BGl_string1752z00zz__rgc_treez00, BgL_exprz00_28);
								}
						}
					else
						{	/* Rgc/rgctree.scm 218 */
							return
								BGl_errorz00zz__errorz00(BFALSE,
								BGl_string1752z00zz__rgc_treez00, BgL_exprz00_28);
						}
				}
			else
				{	/* Rgc/rgctree.scm 218 */
					return
						BGl_errorz00zz__errorz00(BFALSE, BGl_string1752z00zz__rgc_treez00,
						BgL_exprz00_28);
				}
		}

	}



/* &<@anonymous:1335> */
	obj_t BGl_z62zc3z04anonymousza31335ze3ze5zz__rgc_treez00(obj_t
		BgL_envz00_2245, obj_t BgL_iz00_2248)
	{
		{	/* Rgc/rgctree.scm 231 */
			return
				BGl_submatchzd2stopzd2addz12z12zz__rgc_treez00(BgL_iz00_2248,
				PROCEDURE_REF(BgL_envz00_2245,
					(int) (0L)), PROCEDURE_REF(BgL_envz00_2245, (int) (1L)));
		}

	}



/* &<@anonymous:1333> */
	obj_t BGl_z62zc3z04anonymousza31333ze3ze5zz__rgc_treez00(obj_t
		BgL_envz00_2249, obj_t BgL_iz00_2253)
	{
		{	/* Rgc/rgctree.scm 228 */
			return
				BGl_submatchzd2startzd2addz12z12zz__rgc_treez00(BgL_iz00_2253,
				PROCEDURE_REF(BgL_envz00_2249,
					(int) (0L)),
				PROCEDURE_REF(BgL_envz00_2249,
					(int) (1L)), PROCEDURE_REF(BgL_envz00_2249, (int) (2L)));
		}

	}



/* init-followpos! */
	obj_t BGl_initzd2followposz12zc0zz__rgc_treez00(void)
	{
		{	/* Rgc/rgctree.scm 250 */
			{	/* Rgc/rgctree.scm 251 */
				obj_t BgL_followposz00_1353;

				BgL_followposz00_1353 =
					make_vector(
					(long) CINT(BGl_za2positionzd2numberza2zd2zz__rgc_treez00), BUNSPEC);
				{
					long BgL_iz00_1966;

					BgL_iz00_1966 = 0L;
				BgL_loopz00_1965:
					if (
						((long) CINT(BGl_za2positionzd2numberza2zd2zz__rgc_treez00) ==
							BgL_iz00_1966))
						{	/* Rgc/rgctree.scm 253 */
							return (BGl_za2followposza2z00zz__rgc_treez00 =
								BgL_followposz00_1353, BUNSPEC);
						}
					else
						{	/* Rgc/rgctree.scm 253 */
							VECTOR_SET(BgL_followposz00_1353, BgL_iz00_1966,
								BGl_makezd2rgcsetzd2zz__rgc_setz00(
									(long) CINT(BGl_za2positionzd2numberza2zd2zz__rgc_treez00)));
							{
								long BgL_iz00_2730;

								BgL_iz00_2730 = (BgL_iz00_1966 + 1L);
								BgL_iz00_1966 = BgL_iz00_2730;
								goto BgL_loopz00_1965;
							}
						}
				}
			}
		}

	}



/* submatch-start-add! */
	obj_t BGl_submatchzd2startzd2addz12z12zz__rgc_treez00(obj_t
		BgL_positionz00_32, obj_t BgL_nullablezf3zf3_33, obj_t BgL_matchz00_34,
		obj_t BgL_submatchz00_35)
	{
		{	/* Rgc/rgctree.scm 273 */
			{	/* Rgc/rgctree.scm 274 */
				obj_t BgL_cellz00_1363;

				BgL_cellz00_1363 =
					VECTOR_REF(BGl_za2submatchesza2z00zz__rgc_treez00,
					(long) CINT(BgL_positionz00_32));
				if (PAIRP(BgL_cellz00_1363))
					{	/* Rgc/rgctree.scm 279 */
						obj_t BgL_arg1343z00_1365;

						{	/* Rgc/rgctree.scm 279 */
							obj_t BgL_arg1344z00_1366;
							obj_t BgL_arg1346z00_1367;

							{	/* Rgc/rgctree.scm 279 */
								obj_t BgL_list1347z00_1368;

								{	/* Rgc/rgctree.scm 279 */
									obj_t BgL_arg1348z00_1369;

									{	/* Rgc/rgctree.scm 279 */
										obj_t BgL_arg1349z00_1370;

										BgL_arg1349z00_1370 =
											MAKE_YOUNG_PAIR(BgL_submatchz00_35, BNIL);
										BgL_arg1348z00_1369 =
											MAKE_YOUNG_PAIR(BgL_matchz00_34, BgL_arg1349z00_1370);
									}
									BgL_list1347z00_1368 =
										MAKE_YOUNG_PAIR(BgL_nullablezf3zf3_33, BgL_arg1348z00_1369);
								}
								BgL_arg1344z00_1366 = BgL_list1347z00_1368;
							}
							BgL_arg1346z00_1367 = CAR(BgL_cellz00_1363);
							BgL_arg1343z00_1365 =
								MAKE_YOUNG_PAIR(BgL_arg1344z00_1366, BgL_arg1346z00_1367);
						}
						return SET_CAR(BgL_cellz00_1363, BgL_arg1343z00_1365);
					}
				else
					{	/* Rgc/rgctree.scm 278 */
						obj_t BgL_arg1350z00_1371;

						{	/* Rgc/rgctree.scm 278 */
							obj_t BgL_arg1351z00_1372;

							{	/* Rgc/rgctree.scm 278 */
								obj_t BgL_arg1352z00_1373;

								{	/* Rgc/rgctree.scm 278 */
									obj_t BgL_list1354z00_1375;

									{	/* Rgc/rgctree.scm 278 */
										obj_t BgL_arg1356z00_1376;

										{	/* Rgc/rgctree.scm 278 */
											obj_t BgL_arg1357z00_1377;

											BgL_arg1357z00_1377 =
												MAKE_YOUNG_PAIR(BgL_submatchz00_35, BNIL);
											BgL_arg1356z00_1376 =
												MAKE_YOUNG_PAIR(BgL_matchz00_34, BgL_arg1357z00_1377);
										}
										BgL_list1354z00_1375 =
											MAKE_YOUNG_PAIR(BgL_nullablezf3zf3_33,
											BgL_arg1356z00_1376);
									}
									BgL_arg1352z00_1373 = BgL_list1354z00_1375;
								}
								{	/* Rgc/rgctree.scm 278 */
									obj_t BgL_list1353z00_1374;

									BgL_list1353z00_1374 =
										MAKE_YOUNG_PAIR(BgL_arg1352z00_1373, BNIL);
									BgL_arg1351z00_1372 = BgL_list1353z00_1374;
								}
							}
							BgL_arg1350z00_1371 = MAKE_YOUNG_PAIR(BgL_arg1351z00_1372, BNIL);
						}
						return
							VECTOR_SET(BGl_za2submatchesza2z00zz__rgc_treez00,
							(long) CINT(BgL_positionz00_32), BgL_arg1350z00_1371);
		}}}

	}



/* submatch-stop-add! */
	obj_t BGl_submatchzd2stopzd2addz12z12zz__rgc_treez00(obj_t BgL_positionz00_36,
		obj_t BgL_matchz00_37, obj_t BgL_submatchz00_38)
	{
		{	/* Rgc/rgctree.scm 284 */
			{	/* Rgc/rgctree.scm 285 */
				obj_t BgL_cellz00_1378;

				BgL_cellz00_1378 =
					VECTOR_REF(BGl_za2submatchesza2z00zz__rgc_treez00,
					(long) CINT(BgL_positionz00_36));
				if (PAIRP(BgL_cellz00_1378))
					{	/* Rgc/rgctree.scm 290 */
						obj_t BgL_arg1359z00_1380;

						{	/* Rgc/rgctree.scm 290 */
							obj_t BgL_arg1360z00_1381;
							obj_t BgL_arg1361z00_1382;

							BgL_arg1360z00_1381 =
								MAKE_YOUNG_PAIR(BgL_matchz00_37, BgL_submatchz00_38);
							BgL_arg1361z00_1382 = CDR(BgL_cellz00_1378);
							BgL_arg1359z00_1380 =
								MAKE_YOUNG_PAIR(BgL_arg1360z00_1381, BgL_arg1361z00_1382);
						}
						return SET_CDR(BgL_cellz00_1378, BgL_arg1359z00_1380);
					}
				else
					{	/* Rgc/rgctree.scm 289 */
						obj_t BgL_arg1362z00_1383;

						{	/* Rgc/rgctree.scm 289 */
							obj_t BgL_arg1363z00_1384;

							{	/* Rgc/rgctree.scm 289 */
								obj_t BgL_arg1364z00_1385;

								BgL_arg1364z00_1385 =
									MAKE_YOUNG_PAIR(BgL_matchz00_37, BgL_submatchz00_38);
								{	/* Rgc/rgctree.scm 289 */
									obj_t BgL_list1365z00_1386;

									BgL_list1365z00_1386 =
										MAKE_YOUNG_PAIR(BgL_arg1364z00_1385, BNIL);
									BgL_arg1363z00_1384 = BgL_list1365z00_1386;
								}
							}
							BgL_arg1362z00_1383 = MAKE_YOUNG_PAIR(BNIL, BgL_arg1363z00_1384);
						}
						return
							VECTOR_SET(BGl_za2submatchesza2z00zz__rgc_treez00,
							(long) CINT(BgL_positionz00_36), BgL_arg1362z00_1383);
		}}}

	}



/* reset-tree! */
	BGL_EXPORTED_DEF obj_t BGl_resetzd2treez12zc0zz__rgc_treez00(void)
	{
		{	/* Rgc/rgctree.scm 295 */
			BGl_za2followposza2z00zz__rgc_treez00 = BUNSPEC;
			BGl_za2positionsza2z00zz__rgc_treez00 = BUNSPEC;
			BGl_za2submatchesza2z00zz__rgc_treez00 = BUNSPEC;
			return (BGl_za2positionzd2numberza2zd2zz__rgc_treez00 = BUNSPEC, BUNSPEC);
		}

	}



/* &reset-tree! */
	obj_t BGl_z62resetzd2treez12za2zz__rgc_treez00(obj_t BgL_envz00_2254)
	{
		{	/* Rgc/rgctree.scm 295 */
			return BGl_resetzd2treez12zc0zz__rgc_treez00();
		}

	}



/* print-followpos */
	BGL_EXPORTED_DEF obj_t BGl_printzd2followposzd2zz__rgc_treez00(obj_t
		BgL_fpz00_39)
	{
		{	/* Rgc/rgctree.scm 304 */
			{	/* Rgc/rgctree.scm 305 */
				obj_t BgL_port1092z00_1387;

				{	/* Rgc/rgctree.scm 305 */
					obj_t BgL_tmpz00_2763;

					BgL_tmpz00_2763 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_port1092z00_1387 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_2763);
				}
				bgl_display_string(BGl_string1756z00zz__rgc_treez00,
					BgL_port1092z00_1387);
				bgl_display_char(((unsigned char) 10), BgL_port1092z00_1387);
			}
			{	/* Rgc/rgctree.scm 306 */
				obj_t BgL_port1093z00_1388;

				{	/* Rgc/rgctree.scm 306 */
					obj_t BgL_tmpz00_2768;

					BgL_tmpz00_2768 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_port1093z00_1388 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_2768);
				}
				bgl_display_string(BGl_string1757z00zz__rgc_treez00,
					BgL_port1093z00_1388);
				bgl_display_obj(BINT(VECTOR_LENGTH(((obj_t) BgL_fpz00_39))),
					BgL_port1093z00_1388);
				bgl_display_char(((unsigned char) 10), BgL_port1093z00_1388);
			} BGl_list1758z00zz__rgc_treez00;
			{	/* Rgc/rgctree.scm 313 */
				obj_t BgL_port1094z00_1390;

				{	/* Rgc/rgctree.scm 313 */
					obj_t BgL_tmpz00_2777;

					BgL_tmpz00_2777 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_port1094z00_1390 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_2777);
				}
				bgl_display_string(BGl_string1803z00zz__rgc_treez00,
					BgL_port1094z00_1390);
				return bgl_display_char(((unsigned char) 10), BgL_port1094z00_1390);
		}}

	}



/* &print-followpos */
	obj_t BGl_z62printzd2followposzb0zz__rgc_treez00(obj_t BgL_envz00_2255,
		obj_t BgL_fpz00_2256)
	{
		{	/* Rgc/rgctree.scm 304 */
			return BGl_printzd2followposzd2zz__rgc_treez00(BgL_fpz00_2256);
		}

	}



/* print-node */
	BGL_EXPORTED_DEF obj_t BGl_printzd2nodezd2zz__rgc_treez00(obj_t
		BgL_nodez00_40)
	{
		{	/* Rgc/rgctree.scm 318 */
			return BGl_symbol1804z00zz__rgc_treez00;
		}

	}



/* &print-node */
	obj_t BGl_z62printzd2nodezb0zz__rgc_treez00(obj_t BgL_envz00_2257,
		obj_t BgL_nodez00_2258)
	{
		{	/* Rgc/rgctree.scm 318 */
			return BGl_printzd2nodezd2zz__rgc_treez00(BgL_nodez00_2258);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__rgc_treez00(void)
	{
		{	/* Rgc/rgctree.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__rgc_treez00(void)
	{
		{	/* Rgc/rgctree.scm 14 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__rgc_treez00(void)
	{
		{	/* Rgc/rgctree.scm 14 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__rgc_treez00(void)
	{
		{	/* Rgc/rgctree.scm 14 */
			BGl_modulezd2initializa7ationz75zz__rgc_setz00(225075643L,
				BSTRING_TO_STRING(BGl_string1806z00zz__rgc_treez00));
			BGl_modulezd2initializa7ationz75zz__rgc_configz00(428274736L,
				BSTRING_TO_STRING(BGl_string1806z00zz__rgc_treez00));
			return
				BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1806z00zz__rgc_treez00));
		}

	}

#ifdef __cplusplus
}
#endif
