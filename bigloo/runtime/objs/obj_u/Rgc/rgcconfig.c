/*===========================================================================*/
/*   (Rgc/rgcconfig.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Rgc/rgcconfig.scm -indent -o objs/obj_u/Rgc/rgcconfig.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___RGC_CONFIG_TYPE_DEFINITIONS
#define BGL___RGC_CONFIG_TYPE_DEFINITIONS
#endif													// BGL___RGC_CONFIG_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_symbol1673z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_list1640z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_list1643z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_list1645z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_list1648z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31191ze3ze5zz__rgc_configz00(obj_t,
		obj_t);
	static obj_t BGl_list1649z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_symbol1680z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_list1651z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_symbol1685z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_list1654z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_list1657z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31192ze3ze5zz__rgc_configz00(obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_symbol1690z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_symbol1692z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_list1660z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_list1661z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_list1663z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_list1666z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_list1669z00zz__rgc_configz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__rgc_configz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_list1670z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_list1672z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_list1675z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_list1676z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_list1677z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31194ze3ze5zz__rgc_configz00(obj_t,
		obj_t);
	static obj_t BGl_list1679z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zz__rgc_configz00(void);
	static obj_t BGl_list1682z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_list1684z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_list1687z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_list1689z00zz__rgc_configz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2rgczd2optimza2zd2zz__rgc_configz00 = BUNSPEC;
	extern obj_t create_struct(obj_t, int);
	static obj_t BGl_cnstzd2initzd2zz__rgc_configz00(void);
	static obj_t BGl_genericzd2initzd2zz__rgc_configz00(void);
	static obj_t BGl_z62rgczd2downcasezb0zz__rgc_configz00(obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__rgc_configz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__rgc_configz00(void);
	static obj_t BGl_objectzd2initzd2zz__rgc_configz00(void);
	static obj_t BGl_za2asciizd2configza2zd2zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_z62rgczd2charzf3z43zz__rgc_configz00(obj_t, obj_t);
	extern bool_t BGl_2zc3zc3zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31198ze3ze5zz__rgc_configz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_rgczd2downcasezd2zz__rgc_configz00(obj_t);
	static obj_t BGl_za2rgczd2configza2zd2zz__rgc_configz00 = BUNSPEC;
	extern obj_t bstring_to_symbol(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_rgczd2envzd2zz__rgc_configz00(void);
	static obj_t BGl_z62rgczd2envzb0zz__rgc_configz00(obj_t);
	static obj_t BGl_methodzd2initzd2zz__rgc_configz00(void);
	BGL_EXPORTED_DECL bool_t BGl_rgczd2charzf3z21zz__rgc_configz00(obj_t);
	static obj_t BGl_z62rgczd2maxzd2charz62zz__rgc_configz00(obj_t);
	static obj_t BGl_symbol1632z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_symbol1635z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_symbol1638z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_symbol1641z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_symbol1646z00zz__rgc_configz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_rgczd2upcasezd2zz__rgc_configz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rgczd2alphabeticzf3z21zz__rgc_configz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_rgczd2maxzd2charz00zz__rgc_configz00(void);
	static obj_t BGl_z62rgczd2alphabeticzf3z43zz__rgc_configz00(obj_t, obj_t);
	static obj_t BGl_symbol1652z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_symbol1655z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_symbol1658z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_list1630z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_symbol1664z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_list1631z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_symbol1667z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_list1634z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_list1637z00zz__rgc_configz00 = BUNSPEC;
	static obj_t BGl_z62rgczd2upcasezb0zz__rgc_configz00(obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rgczd2alphabeticzf3zd2envzf3zz__rgc_configz00,
		BgL_bgl_za762rgcza7d2alphabe1696z00,
		BGl_z62rgczd2alphabeticzf3z43zz__rgc_configz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgczd2envzd2envz00zz__rgc_configz00,
		BgL_bgl_za762rgcza7d2envza7b0za71697z00,
		BGl_z62rgczd2envzb0zz__rgc_configz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgczd2charzf3zd2envzf3zz__rgc_configz00,
		BgL_bgl_za762rgcza7d2charza7f31698za7,
		BGl_z62rgczd2charzf3z43zz__rgc_configz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgczd2upcasezd2envz00zz__rgc_configz00,
		BgL_bgl_za762rgcza7d2upcaseza71699za7,
		BGl_z62rgczd2upcasezb0zz__rgc_configz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgczd2maxzd2charzd2envzd2zz__rgc_configz00,
		BgL_bgl_za762rgcza7d2maxza7d2c1700za7,
		BGl_z62rgczd2maxzd2charz62zz__rgc_configz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1626z00zz__rgc_configz00,
		BgL_bgl_za762za7c3za704anonymo1701za7,
		BGl_z62zc3z04anonymousza31198ze3ze5zz__rgc_configz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1633z00zz__rgc_configz00,
		BgL_bgl_string1633za700za7za7_1702za7, "all", 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1627z00zz__rgc_configz00,
		BgL_bgl_za762za7c3za704anonymo1703za7,
		BGl_z62zc3z04anonymousza31194ze3ze5zz__rgc_configz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1628z00zz__rgc_configz00,
		BgL_bgl_za762za7c3za704anonymo1704za7,
		BGl_z62zc3z04anonymousza31192ze3ze5zz__rgc_configz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1629z00zz__rgc_configz00,
		BgL_bgl_za762za7c3za704anonymo1705za7,
		BGl_z62zc3z04anonymousza31191ze3ze5zz__rgc_configz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1636z00zz__rgc_configz00,
		BgL_bgl_string1636za700za7za7_1706za7, "out", 3);
	      DEFINE_STRING(BGl_string1639z00zz__rgc_configz00,
		BgL_bgl_string1639za700za7za7_1707za7, "lower", 5);
	      DEFINE_STRING(BGl_string1642z00zz__rgc_configz00,
		BgL_bgl_string1642za700za7za7_1708za7, "in", 2);
	      DEFINE_STRING(BGl_string1644z00zz__rgc_configz00,
		BgL_bgl_string1644za700za7za7_1709za7, "az", 2);
	      DEFINE_STRING(BGl_string1647z00zz__rgc_configz00,
		BgL_bgl_string1647za700za7za7_1710za7, "upper", 5);
	      DEFINE_STRING(BGl_string1650z00zz__rgc_configz00,
		BgL_bgl_string1650za700za7za7_1711za7, "AZ", 2);
	      DEFINE_STRING(BGl_string1653z00zz__rgc_configz00,
		BgL_bgl_string1653za700za7za7_1712za7, "alpha", 5);
	      DEFINE_STRING(BGl_string1656z00zz__rgc_configz00,
		BgL_bgl_string1656za700za7za7_1713za7, "or", 2);
	      DEFINE_STRING(BGl_string1659z00zz__rgc_configz00,
		BgL_bgl_string1659za700za7za7_1714za7, "digit", 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rgczd2downcasezd2envz00zz__rgc_configz00,
		BgL_bgl_za762rgcza7d2downcas1715z00,
		BGl_z62rgczd2downcasezb0zz__rgc_configz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1662z00zz__rgc_configz00,
		BgL_bgl_string1662za700za7za7_1716za7, "09", 2);
	      DEFINE_STRING(BGl_string1665z00zz__rgc_configz00,
		BgL_bgl_string1665za700za7za7_1717za7, "xdigit", 6);
	      DEFINE_STRING(BGl_string1668z00zz__rgc_configz00,
		BgL_bgl_string1668za700za7za7_1718za7, "uncase", 6);
	      DEFINE_STRING(BGl_string1671z00zz__rgc_configz00,
		BgL_bgl_string1671za700za7za7_1719za7, "af09", 4);
	      DEFINE_STRING(BGl_string1674z00zz__rgc_configz00,
		BgL_bgl_string1674za700za7za7_1720za7, "alnum", 5);
	      DEFINE_STRING(BGl_string1678z00zz__rgc_configz00,
		BgL_bgl_string1678za700za7za7_1721za7, "az09", 4);
	      DEFINE_STRING(BGl_string1681z00zz__rgc_configz00,
		BgL_bgl_string1681za700za7za7_1722za7, "punct", 5);
	      DEFINE_STRING(BGl_string1683z00zz__rgc_configz00,
		BgL_bgl_string1683za700za7za7_1723za7, ".,;!?", 5);
	      DEFINE_STRING(BGl_string1686z00zz__rgc_configz00,
		BgL_bgl_string1686za700za7za7_1724za7, "blank", 5);
	      DEFINE_STRING(BGl_string1688z00zz__rgc_configz00,
		BgL_bgl_string1688za700za7za7_1725za7, " \t\n", 3);
	      DEFINE_STRING(BGl_string1691z00zz__rgc_configz00,
		BgL_bgl_string1691za700za7za7_1726za7, "space", 5);
	      DEFINE_STRING(BGl_string1693z00zz__rgc_configz00,
		BgL_bgl_string1693za700za7za7_1727za7, "rgc-config", 10);
	      DEFINE_STRING(BGl_string1694z00zz__rgc_configz00,
		BgL_bgl_string1694za700za7za7_1728za7, "ascii", 5);
	      DEFINE_STRING(BGl_string1695z00zz__rgc_configz00,
		BgL_bgl_string1695za700za7za7_1729za7, "__rgc_config", 12);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_symbol1673z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_list1640z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_list1643z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_list1645z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_list1648z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_list1649z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_symbol1680z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_list1651z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_symbol1685z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_list1654z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_list1657z00zz__rgc_configz00));
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_symbol1690z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_symbol1692z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_list1660z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_list1661z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_list1663z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_list1666z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_list1669z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_list1670z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_list1672z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_list1675z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_list1676z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_list1677z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_list1679z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_list1682z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_list1684z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_list1687z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_list1689z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_za2rgczd2optimza2zd2zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_za2asciizd2configza2zd2zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_za2rgczd2configza2zd2zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_symbol1632z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_symbol1635z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_symbol1638z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_symbol1641z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_symbol1646z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_symbol1652z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_symbol1655z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_symbol1658z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_list1630z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_symbol1664z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_list1631z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_symbol1667z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_list1634z00zz__rgc_configz00));
		     ADD_ROOT((void *) (&BGl_list1637z00zz__rgc_configz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__rgc_configz00(long
		BgL_checksumz00_2003, char *BgL_fromz00_2004)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__rgc_configz00))
				{
					BGl_requirezd2initializa7ationz75zz__rgc_configz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__rgc_configz00();
					BGl_cnstzd2initzd2zz__rgc_configz00();
					BGl_importedzd2moduleszd2initz00zz__rgc_configz00();
					return BGl_toplevelzd2initzd2zz__rgc_configz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__rgc_configz00(void)
	{
		{	/* Rgc/rgcconfig.scm 14 */
			BGl_symbol1632z00zz__rgc_configz00 =
				bstring_to_symbol(BGl_string1633z00zz__rgc_configz00);
			BGl_symbol1635z00zz__rgc_configz00 =
				bstring_to_symbol(BGl_string1636z00zz__rgc_configz00);
			BGl_list1634z00zz__rgc_configz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1635z00zz__rgc_configz00,
				MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL));
			BGl_list1631z00zz__rgc_configz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1632z00zz__rgc_configz00,
				MAKE_YOUNG_PAIR(BGl_list1634z00zz__rgc_configz00, BNIL));
			BGl_symbol1638z00zz__rgc_configz00 =
				bstring_to_symbol(BGl_string1639z00zz__rgc_configz00);
			BGl_symbol1641z00zz__rgc_configz00 =
				bstring_to_symbol(BGl_string1642z00zz__rgc_configz00);
			BGl_list1643z00zz__rgc_configz00 =
				MAKE_YOUNG_PAIR(BGl_string1644z00zz__rgc_configz00, BNIL);
			BGl_list1640z00zz__rgc_configz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1641z00zz__rgc_configz00,
				MAKE_YOUNG_PAIR(BGl_list1643z00zz__rgc_configz00, BNIL));
			BGl_list1637z00zz__rgc_configz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1638z00zz__rgc_configz00,
				MAKE_YOUNG_PAIR(BGl_list1640z00zz__rgc_configz00, BNIL));
			BGl_symbol1646z00zz__rgc_configz00 =
				bstring_to_symbol(BGl_string1647z00zz__rgc_configz00);
			BGl_list1649z00zz__rgc_configz00 =
				MAKE_YOUNG_PAIR(BGl_string1650z00zz__rgc_configz00, BNIL);
			BGl_list1648z00zz__rgc_configz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1641z00zz__rgc_configz00,
				MAKE_YOUNG_PAIR(BGl_list1649z00zz__rgc_configz00, BNIL));
			BGl_list1645z00zz__rgc_configz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1646z00zz__rgc_configz00,
				MAKE_YOUNG_PAIR(BGl_list1648z00zz__rgc_configz00, BNIL));
			BGl_symbol1652z00zz__rgc_configz00 =
				bstring_to_symbol(BGl_string1653z00zz__rgc_configz00);
			BGl_symbol1655z00zz__rgc_configz00 =
				bstring_to_symbol(BGl_string1656z00zz__rgc_configz00);
			BGl_list1654z00zz__rgc_configz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1655z00zz__rgc_configz00,
				MAKE_YOUNG_PAIR(BGl_symbol1638z00zz__rgc_configz00,
					MAKE_YOUNG_PAIR(BGl_symbol1646z00zz__rgc_configz00, BNIL)));
			BGl_list1651z00zz__rgc_configz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1652z00zz__rgc_configz00,
				MAKE_YOUNG_PAIR(BGl_list1654z00zz__rgc_configz00, BNIL));
			BGl_symbol1658z00zz__rgc_configz00 =
				bstring_to_symbol(BGl_string1659z00zz__rgc_configz00);
			BGl_list1661z00zz__rgc_configz00 =
				MAKE_YOUNG_PAIR(BGl_string1662z00zz__rgc_configz00, BNIL);
			BGl_list1660z00zz__rgc_configz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1641z00zz__rgc_configz00,
				MAKE_YOUNG_PAIR(BGl_list1661z00zz__rgc_configz00, BNIL));
			BGl_list1657z00zz__rgc_configz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1658z00zz__rgc_configz00,
				MAKE_YOUNG_PAIR(BGl_list1660z00zz__rgc_configz00, BNIL));
			BGl_symbol1664z00zz__rgc_configz00 =
				bstring_to_symbol(BGl_string1665z00zz__rgc_configz00);
			BGl_symbol1667z00zz__rgc_configz00 =
				bstring_to_symbol(BGl_string1668z00zz__rgc_configz00);
			BGl_list1670z00zz__rgc_configz00 =
				MAKE_YOUNG_PAIR(BGl_string1671z00zz__rgc_configz00, BNIL);
			BGl_list1669z00zz__rgc_configz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1641z00zz__rgc_configz00,
				MAKE_YOUNG_PAIR(BGl_list1670z00zz__rgc_configz00, BNIL));
			BGl_list1666z00zz__rgc_configz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1667z00zz__rgc_configz00,
				MAKE_YOUNG_PAIR(BGl_list1669z00zz__rgc_configz00, BNIL));
			BGl_list1663z00zz__rgc_configz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1664z00zz__rgc_configz00,
				MAKE_YOUNG_PAIR(BGl_list1666z00zz__rgc_configz00, BNIL));
			BGl_symbol1673z00zz__rgc_configz00 =
				bstring_to_symbol(BGl_string1674z00zz__rgc_configz00);
			BGl_list1677z00zz__rgc_configz00 =
				MAKE_YOUNG_PAIR(BGl_string1678z00zz__rgc_configz00, BNIL);
			BGl_list1676z00zz__rgc_configz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1641z00zz__rgc_configz00,
				MAKE_YOUNG_PAIR(BGl_list1677z00zz__rgc_configz00, BNIL));
			BGl_list1675z00zz__rgc_configz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1667z00zz__rgc_configz00,
				MAKE_YOUNG_PAIR(BGl_list1676z00zz__rgc_configz00, BNIL));
			BGl_list1672z00zz__rgc_configz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1673z00zz__rgc_configz00,
				MAKE_YOUNG_PAIR(BGl_list1675z00zz__rgc_configz00, BNIL));
			BGl_symbol1680z00zz__rgc_configz00 =
				bstring_to_symbol(BGl_string1681z00zz__rgc_configz00);
			BGl_list1682z00zz__rgc_configz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1641z00zz__rgc_configz00,
				MAKE_YOUNG_PAIR(BGl_string1683z00zz__rgc_configz00, BNIL));
			BGl_list1679z00zz__rgc_configz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1680z00zz__rgc_configz00,
				MAKE_YOUNG_PAIR(BGl_list1682z00zz__rgc_configz00, BNIL));
			BGl_symbol1685z00zz__rgc_configz00 =
				bstring_to_symbol(BGl_string1686z00zz__rgc_configz00);
			BGl_list1687z00zz__rgc_configz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1641z00zz__rgc_configz00,
				MAKE_YOUNG_PAIR(BGl_string1688z00zz__rgc_configz00, BNIL));
			BGl_list1684z00zz__rgc_configz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1685z00zz__rgc_configz00,
				MAKE_YOUNG_PAIR(BGl_list1687z00zz__rgc_configz00, BNIL));
			BGl_symbol1690z00zz__rgc_configz00 =
				bstring_to_symbol(BGl_string1691z00zz__rgc_configz00);
			BGl_list1689z00zz__rgc_configz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1690z00zz__rgc_configz00,
				MAKE_YOUNG_PAIR(BCHAR(((unsigned char) ' ')), BNIL));
			BGl_list1630z00zz__rgc_configz00 =
				MAKE_YOUNG_PAIR(BGl_list1631z00zz__rgc_configz00,
				MAKE_YOUNG_PAIR(BGl_list1637z00zz__rgc_configz00,
					MAKE_YOUNG_PAIR(BGl_list1645z00zz__rgc_configz00,
						MAKE_YOUNG_PAIR(BGl_list1651z00zz__rgc_configz00,
							MAKE_YOUNG_PAIR(BGl_list1657z00zz__rgc_configz00,
								MAKE_YOUNG_PAIR(BGl_list1663z00zz__rgc_configz00,
									MAKE_YOUNG_PAIR(BGl_list1672z00zz__rgc_configz00,
										MAKE_YOUNG_PAIR(BGl_list1679z00zz__rgc_configz00,
											MAKE_YOUNG_PAIR(BGl_list1684z00zz__rgc_configz00,
												MAKE_YOUNG_PAIR(BGl_list1689z00zz__rgc_configz00,
													BNIL))))))))));
			return (BGl_symbol1692z00zz__rgc_configz00 =
				bstring_to_symbol(BGl_string1693z00zz__rgc_configz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__rgc_configz00(void)
	{
		{	/* Rgc/rgcconfig.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__rgc_configz00(void)
	{
		{	/* Rgc/rgcconfig.scm 14 */
			BGl_za2rgczd2optimza2zd2zz__rgc_configz00 = BTRUE;
			{	/* Rgc/rgcconfig.scm 116 */
				obj_t BgL_envz00_1705;

				BgL_envz00_1705 = BGl_list1630z00zz__rgc_configz00;
				{	/* Rgc/rgcconfig.scm 63 */
					obj_t BgL_newz00_1706;

					BgL_newz00_1706 =
						create_struct(BGl_symbol1692z00zz__rgc_configz00, (int) (7L));
					{	/* Rgc/rgcconfig.scm 63 */
						int BgL_tmpz00_2090;

						BgL_tmpz00_2090 = (int) (6L);
						STRUCT_SET(BgL_newz00_1706, BgL_tmpz00_2090, BgL_envz00_1705);
					}
					{	/* Rgc/rgcconfig.scm 63 */
						int BgL_tmpz00_2093;

						BgL_tmpz00_2093 = (int) (5L);
						STRUCT_SET(BgL_newz00_1706, BgL_tmpz00_2093,
							BGl_proc1626z00zz__rgc_configz00);
					}
					{	/* Rgc/rgcconfig.scm 63 */
						int BgL_tmpz00_2096;

						BgL_tmpz00_2096 = (int) (4L);
						STRUCT_SET(BgL_newz00_1706, BgL_tmpz00_2096,
							BGl_proc1627z00zz__rgc_configz00);
					}
					{	/* Rgc/rgcconfig.scm 63 */
						int BgL_tmpz00_2099;

						BgL_tmpz00_2099 = (int) (3L);
						STRUCT_SET(BgL_newz00_1706, BgL_tmpz00_2099,
							BGl_proc1628z00zz__rgc_configz00);
					}
					{	/* Rgc/rgcconfig.scm 63 */
						int BgL_tmpz00_2102;

						BgL_tmpz00_2102 = (int) (2L);
						STRUCT_SET(BgL_newz00_1706, BgL_tmpz00_2102,
							BGl_proc1629z00zz__rgc_configz00);
					}
					{	/* Rgc/rgcconfig.scm 63 */
						obj_t BgL_auxz00_2107;
						int BgL_tmpz00_2105;

						BgL_auxz00_2107 = BINT(256L);
						BgL_tmpz00_2105 = (int) (1L);
						STRUCT_SET(BgL_newz00_1706, BgL_tmpz00_2105, BgL_auxz00_2107);
					}
					{	/* Rgc/rgcconfig.scm 63 */
						int BgL_tmpz00_2110;

						BgL_tmpz00_2110 = (int) (0L);
						STRUCT_SET(BgL_newz00_1706, BgL_tmpz00_2110,
							BGl_string1694z00zz__rgc_configz00);
					}
					BGl_za2asciizd2configza2zd2zz__rgc_configz00 = BgL_newz00_1706;
			}}
			return (BGl_za2rgczd2configza2zd2zz__rgc_configz00 =
				BGl_za2asciizd2configza2zd2zz__rgc_configz00, BUNSPEC);
		}

	}



/* &<@anonymous:1191> */
	obj_t BGl_z62zc3z04anonymousza31191ze3ze5zz__rgc_configz00(obj_t
		BgL_envz00_1969, obj_t BgL_xz00_1970)
	{
		{	/* Rgc/rgcconfig.scm 118 */
			{	/* Rgc/rgcconfig.scm 119 */
				bool_t BgL_tmpz00_2113;

				{	/* Rgc/rgcconfig.scm 119 */
					bool_t BgL_test1731z00_2114;

					if (INTEGERP(BgL_xz00_1970))
						{	/* Rgc/rgcconfig.scm 119 */
							BgL_test1731z00_2114 = ((long) CINT(BgL_xz00_1970) > 0L);
						}
					else
						{	/* Rgc/rgcconfig.scm 119 */
							BgL_test1731z00_2114 =
								BGl_2ze3ze3zz__r4_numbers_6_5z00(BgL_xz00_1970, BINT(0L));
						}
					if (BgL_test1731z00_2114)
						{	/* Rgc/rgcconfig.scm 119 */
							bool_t BgL_test1733z00_2121;

							if (INTEGERP(BgL_xz00_1970))
								{	/* Rgc/rgcconfig.scm 119 */
									BgL_test1733z00_2121 = ((long) CINT(BgL_xz00_1970) < 256L);
								}
							else
								{	/* Rgc/rgcconfig.scm 119 */
									BgL_test1733z00_2121 =
										BGl_2zc3zc3zz__r4_numbers_6_5z00(BgL_xz00_1970, BINT(256L));
								}
							if (BgL_test1733z00_2121)
								{	/* Rgc/rgcconfig.scm 119 */
									BgL_tmpz00_2113 = CHARP(BgL_xz00_1970);
								}
							else
								{	/* Rgc/rgcconfig.scm 119 */
									BgL_tmpz00_2113 = ((bool_t) 0);
								}
						}
					else
						{	/* Rgc/rgcconfig.scm 119 */
							BgL_tmpz00_2113 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_2113);
			}
		}

	}



/* &<@anonymous:1192> */
	obj_t BGl_z62zc3z04anonymousza31192ze3ze5zz__rgc_configz00(obj_t
		BgL_envz00_1971, obj_t BgL_xz00_1972)
	{
		{	/* Rgc/rgcconfig.scm 120 */
			{	/* Rgc/rgcconfig.scm 121 */
				bool_t BgL_tmpz00_2130;

				{	/* Rgc/rgcconfig.scm 121 */
					bool_t BgL_test1735z00_2131;

					if (INTEGERP(BgL_xz00_1972))
						{	/* Rgc/rgcconfig.scm 121 */
							BgL_test1735z00_2131 = ((long) CINT(BgL_xz00_1972) > 0L);
						}
					else
						{	/* Rgc/rgcconfig.scm 121 */
							BgL_test1735z00_2131 =
								BGl_2ze3ze3zz__r4_numbers_6_5z00(BgL_xz00_1972, BINT(0L));
						}
					if (BgL_test1735z00_2131)
						{	/* Rgc/rgcconfig.scm 121 */
							bool_t BgL_test1737z00_2138;

							if (INTEGERP(BgL_xz00_1972))
								{	/* Rgc/rgcconfig.scm 121 */
									BgL_test1737z00_2138 = ((long) CINT(BgL_xz00_1972) < 256L);
								}
							else
								{	/* Rgc/rgcconfig.scm 121 */
									BgL_test1737z00_2138 =
										BGl_2zc3zc3zz__r4_numbers_6_5z00(BgL_xz00_1972, BINT(256L));
								}
							if (BgL_test1737z00_2138)
								{	/* Rgc/rgcconfig.scm 121 */
									unsigned char BgL_tmpz00_2145;

									BgL_tmpz00_2145 = ((long) CINT(BgL_xz00_1972));
									BgL_tmpz00_2130 = isalpha(BgL_tmpz00_2145);
								}
							else
								{	/* Rgc/rgcconfig.scm 121 */
									BgL_tmpz00_2130 = ((bool_t) 0);
								}
						}
					else
						{	/* Rgc/rgcconfig.scm 121 */
							BgL_tmpz00_2130 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_2130);
			}
		}

	}



/* &<@anonymous:1194> */
	obj_t BGl_z62zc3z04anonymousza31194ze3ze5zz__rgc_configz00(obj_t
		BgL_envz00_1973, obj_t BgL_xz00_1974)
	{
		{	/* Rgc/rgcconfig.scm 122 */
			return BINT((toupper(((long) CINT(BgL_xz00_1974)))));
		}

	}



/* &<@anonymous:1198> */
	obj_t BGl_z62zc3z04anonymousza31198ze3ze5zz__rgc_configz00(obj_t
		BgL_envz00_1975, obj_t BgL_xz00_1976)
	{
		{	/* Rgc/rgcconfig.scm 124 */
			return BINT((tolower(((long) CINT(BgL_xz00_1976)))));
		}

	}



/* rgc-max-char */
	BGL_EXPORTED_DEF obj_t BGl_rgczd2maxzd2charz00zz__rgc_configz00(void)
	{
		{	/* Rgc/rgcconfig.scm 77 */
			return STRUCT_REF(BGl_za2rgczd2configza2zd2zz__rgc_configz00, (int) (1L));
		}

	}



/* &rgc-max-char */
	obj_t BGl_z62rgczd2maxzd2charz62zz__rgc_configz00(obj_t BgL_envz00_1977)
	{
		{	/* Rgc/rgcconfig.scm 77 */
			return BGl_rgczd2maxzd2charz00zz__rgc_configz00();
		}

	}



/* rgc-char? */
	BGL_EXPORTED_DEF bool_t BGl_rgczd2charzf3z21zz__rgc_configz00(obj_t
		BgL_charz00_33)
	{
		{	/* Rgc/rgcconfig.scm 83 */
			{	/* Rgc/rgcconfig.scm 84 */
				obj_t BgL_fun1221z00_1737;

				BgL_fun1221z00_1737 =
					STRUCT_REF(BGl_za2rgczd2configza2zd2zz__rgc_configz00, (int) (2L));
				return CBOOL(BGL_PROCEDURE_CALL1(BgL_fun1221z00_1737, BgL_charz00_33));
			}
		}

	}



/* &rgc-char? */
	obj_t BGl_z62rgczd2charzf3z43zz__rgc_configz00(obj_t BgL_envz00_1978,
		obj_t BgL_charz00_1979)
	{
		{	/* Rgc/rgcconfig.scm 83 */
			return BBOOL(BGl_rgczd2charzf3z21zz__rgc_configz00(BgL_charz00_1979));
		}

	}



/* rgc-alphabetic? */
	BGL_EXPORTED_DEF bool_t BGl_rgczd2alphabeticzf3z21zz__rgc_configz00(obj_t
		BgL_charz00_34)
	{
		{	/* Rgc/rgcconfig.scm 89 */
			{	/* Rgc/rgcconfig.scm 90 */
				obj_t BgL_fun1222z00_1740;

				BgL_fun1222z00_1740 =
					STRUCT_REF(BGl_za2rgczd2configza2zd2zz__rgc_configz00, (int) (3L));
				return CBOOL(BGL_PROCEDURE_CALL1(BgL_fun1222z00_1740, BgL_charz00_34));
			}
		}

	}



/* &rgc-alphabetic? */
	obj_t BGl_z62rgczd2alphabeticzf3z43zz__rgc_configz00(obj_t BgL_envz00_1980,
		obj_t BgL_charz00_1981)
	{
		{	/* Rgc/rgcconfig.scm 89 */
			return
				BBOOL(BGl_rgczd2alphabeticzf3z21zz__rgc_configz00(BgL_charz00_1981));
		}

	}



/* rgc-upcase */
	BGL_EXPORTED_DEF obj_t BGl_rgczd2upcasezd2zz__rgc_configz00(obj_t
		BgL_charz00_35)
	{
		{	/* Rgc/rgcconfig.scm 95 */
			{	/* Rgc/rgcconfig.scm 96 */
				obj_t BgL_fun1223z00_1743;

				BgL_fun1223z00_1743 =
					STRUCT_REF(BGl_za2rgczd2configza2zd2zz__rgc_configz00, (int) (4L));
				return BGL_PROCEDURE_CALL1(BgL_fun1223z00_1743, BgL_charz00_35);
			}
		}

	}



/* &rgc-upcase */
	obj_t BGl_z62rgczd2upcasezb0zz__rgc_configz00(obj_t BgL_envz00_1982,
		obj_t BgL_charz00_1983)
	{
		{	/* Rgc/rgcconfig.scm 95 */
			return BGl_rgczd2upcasezd2zz__rgc_configz00(BgL_charz00_1983);
		}

	}



/* rgc-downcase */
	BGL_EXPORTED_DEF obj_t BGl_rgczd2downcasezd2zz__rgc_configz00(obj_t
		BgL_charz00_36)
	{
		{	/* Rgc/rgcconfig.scm 101 */
			{	/* Rgc/rgcconfig.scm 102 */
				obj_t BgL_fun1224z00_1746;

				BgL_fun1224z00_1746 =
					STRUCT_REF(BGl_za2rgczd2configza2zd2zz__rgc_configz00, (int) (5L));
				return BGL_PROCEDURE_CALL1(BgL_fun1224z00_1746, BgL_charz00_36);
			}
		}

	}



/* &rgc-downcase */
	obj_t BGl_z62rgczd2downcasezb0zz__rgc_configz00(obj_t BgL_envz00_1984,
		obj_t BgL_charz00_1985)
	{
		{	/* Rgc/rgcconfig.scm 101 */
			return BGl_rgczd2downcasezd2zz__rgc_configz00(BgL_charz00_1985);
		}

	}



/* rgc-env */
	BGL_EXPORTED_DEF obj_t BGl_rgczd2envzd2zz__rgc_configz00(void)
	{
		{	/* Rgc/rgcconfig.scm 107 */
			return STRUCT_REF(BGl_za2rgczd2configza2zd2zz__rgc_configz00, (int) (6L));
		}

	}



/* &rgc-env */
	obj_t BGl_z62rgczd2envzb0zz__rgc_configz00(obj_t BgL_envz00_1986)
	{
		{	/* Rgc/rgcconfig.scm 107 */
			return BGl_rgczd2envzd2zz__rgc_configz00();
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__rgc_configz00(void)
	{
		{	/* Rgc/rgcconfig.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__rgc_configz00(void)
	{
		{	/* Rgc/rgcconfig.scm 14 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__rgc_configz00(void)
	{
		{	/* Rgc/rgcconfig.scm 14 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__rgc_configz00(void)
	{
		{	/* Rgc/rgcconfig.scm 14 */
			return
				BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1695z00zz__rgc_configz00));
		}

	}

#ifdef __cplusplus
}
#endif
