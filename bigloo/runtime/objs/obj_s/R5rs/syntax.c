/*===========================================================================*/
/*   (R5rs/syntax.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c R5rs/syntax.scm -indent -o objs/obj_s/R5rs/syntax.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___R5_MACRO_4_3_SYNTAX_TYPE_DEFINITIONS
#define BGL___R5_MACRO_4_3_SYNTAX_TYPE_DEFINITIONS
#endif // BGL___R5_MACRO_4_3_SYNTAX_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

static obj_t BGl_symbol2800z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_expandzd2letreczd2syntaxz00zz__r5_macro_4_3_syntaxz00(obj_t, obj_t);
extern bool_t BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
extern obj_t BGl_substringz00zz__r4_strings_6_7z00(obj_t, long, long);
static obj_t BGl_hygienezd2prefixzd2zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2644z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2726z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2646z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2728z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
extern obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_syntaxzd2getzd2framesz00zz__r5_macro_4_3_syntaxz00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol2811z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2813z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static bool_t BGl_ellipsiszf3zf3zz__r5_macro_4_3_syntaxz00(obj_t);
static obj_t BGl_symbol2733z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2815z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2701z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2702z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2703z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2655z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2704z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2705z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2657z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2706z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2707z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2708z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
extern obj_t bstring_to_keyword(obj_t);
static obj_t BGl_list2709z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza31273ze3ze5zz__r5_macro_4_3_syntaxz00(obj_t, obj_t, obj_t);
static obj_t BGl_requirezd2initializa7ationz75zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_hygienezd2markzd2zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2901z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_syntaxzd2matcheszd2patternzf3zf3zz__r5_macro_4_3_syntaxz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_symbol2660z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2823z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2742z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2710z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2662z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_expandzd2letzd2syntaxz00zz__r5_macro_4_3_syntaxz00(obj_t, obj_t);
static obj_t BGl_list2711z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2712z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2746z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2713z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2714z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2715z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static bool_t BGl_hygienezd2eqzf3z21zz__r5_macro_4_3_syntaxz00(obj_t, obj_t);
static obj_t BGl_list2716z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2668z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2717z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
extern bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__r5_macro_4_3_syntaxz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
extern obj_t BGl_installzd2expanderzd2zz__macroz00(obj_t, obj_t);
static obj_t BGl_keyword2958z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2910z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2832z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2751z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2670z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2802z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2721z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2803z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2722z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2837z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2804z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2723z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2675z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2805z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2724z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2839z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2806z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2725z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_hygieniza7eza7zz__r5_macro_4_3_syntaxz00(obj_t, obj_t);
static obj_t BGl_list2807z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2808z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2809z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static long BGl_hygienezd2prefixzd2lenz00zz__r5_macro_4_3_syntaxz00 = 0L;
static obj_t BGl_genvarsz00zz__r5_macro_4_3_syntaxz00(obj_t);
static obj_t BGl_initzd2syntaxzd2expandersz12z12zz__r5_macro_4_3_syntaxz00(void);
static obj_t BGl_symbol2760z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2680z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2810z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2762z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_z62syntaxzd2ruleszd2ze3expanderz81zz__r5_macro_4_3_syntaxz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_symbol2925z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2730z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2682z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2845z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2764z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2731z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2927z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2732z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2847z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2766z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2685z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_toplevelzd2initzd2zz__r5_macro_4_3_syntaxz00(void);
static obj_t BGl_list2735z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza31810ze3ze5zz__r5_macro_4_3_syntaxz00(obj_t, obj_t, obj_t);
static obj_t BGl_list2817z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2736z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2737z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2689z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_unhygieniza7eza7zz__r5_macro_4_3_syntaxz00(obj_t);
static obj_t BGl_list2819z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2738z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2739z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2659z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_getzd2syntaxzd2expanderz00zz__r5_macro_4_3_syntaxz00(obj_t);
static obj_t BGl_syntaxesz00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2930z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2770z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2900z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2820z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2935z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2854z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2821z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2740z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2692z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2822z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2741z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2856z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2775z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2858z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2825z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2744z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2826z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2745z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2697z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2664z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2827z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2779z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2665z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_installzd2syntaxzd2expanderz00zz__r5_macro_4_3_syntaxz00(obj_t, obj_t);
static obj_t BGl_list2909z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2828z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2699z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2666z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2829z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2748z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2667z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2749z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_z62installzd2syntaxzd2expanderz62zz__r5_macro_4_3_syntaxz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31891ze3ze5zz__r5_macro_4_3_syntaxz00(obj_t, obj_t, obj_t);
extern obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_cnstzd2initzd2zz__r5_macro_4_3_syntaxz00(void);
extern bool_t BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
extern obj_t bgl_reverse(obj_t);
static obj_t BGl_genericzd2initzd2zz__r5_macro_4_3_syntaxz00(void);
extern obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
extern long bgl_list_length(obj_t);
static obj_t BGl_symbol2861z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2863z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2830z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2831z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2783z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2750z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_importedzd2moduleszd2initz00zz__r5_macro_4_3_syntaxz00(void);
static obj_t BGl_list2834z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2753z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2868z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2835z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2754z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2836z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2755z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2674z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_gczd2rootszd2initz00zz__r5_macro_4_3_syntaxz00(void);
static obj_t BGl_list2756z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2757z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2758z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2677z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_z62expandzd2definezd2syntaxz62zz__r5_macro_4_3_syntaxz00(obj_t, obj_t, obj_t);
static obj_t BGl_list2759z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2678z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2679z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_objectzd2initzd2zz__r5_macro_4_3_syntaxz00(void);
static obj_t BGl_hygienezd2valueza2z70zz__r5_macro_4_3_syntaxz00(obj_t);
static obj_t BGl_symbol2950z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza31231ze3ze5zz__r5_macro_4_3_syntaxz00(obj_t, obj_t, obj_t);
static obj_t BGl_list2841z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2793z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2842z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2924z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2843z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2795z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2844z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza31902ze3ze5zz__r5_macro_4_3_syntaxz00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol2798z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2684z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_list2929z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2849z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2768z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2687z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza31384ze3ze5zz__r5_macro_4_3_syntaxz00(obj_t, obj_t, obj_t);
extern bool_t bigloo_strcmp_at(obj_t, obj_t, long);
static obj_t BGl_list2769z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2688z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza31376ze3ze5zz__r5_macro_4_3_syntaxz00(obj_t, obj_t, obj_t);
static obj_t BGl_list2850z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2851z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_expandzd2definezd2syntaxz00zz__r5_macro_4_3_syntaxz00(obj_t, obj_t);
static obj_t BGl_list2852z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2934z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2853z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2772z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2691z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2773z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2888z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2774z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2694z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2695z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2777z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2696z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2778z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_syntaxzd2expanderszd2mutexz00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2890z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_gennameze70ze7zz__r5_macro_4_3_syntaxz00(obj_t);
static obj_t BGl_symbol2892z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2860z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2943z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2781z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2782z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_symbol2898z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2865z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza31823ze3ze5zz__r5_macro_4_3_syntaxz00(obj_t, obj_t, obj_t);
static obj_t BGl_list2866z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2785z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2867z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2786z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2949z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2787z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2788z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2789z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
extern obj_t bstring_to_symbol(obj_t);
BGL_EXPORTED_DECL obj_t BGl_syntaxzd2ruleszd2ze3expanderze3zz__r5_macro_4_3_syntaxz00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol2981z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2870z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2871z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2790z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2872z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2791z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2873z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2792z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2874z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2875z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2876z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
extern obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
static obj_t BGl_list2877z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2878z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2797z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2879z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_appendzd221011zd2zz__r5_macro_4_3_syntaxz00(obj_t, obj_t);
static obj_t BGl_syntaxzd2expandzd2patternz00zz__r5_macro_4_3_syntaxz00(obj_t, obj_t, obj_t);
static obj_t BGl_methodzd2initzd2zz__r5_macro_4_3_syntaxz00(void);
static obj_t BGl_z62zc3z04anonymousza31340ze3ze5zz__r5_macro_4_3_syntaxz00(obj_t, obj_t, obj_t);
static obj_t BGl_list2880z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2881z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2882z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_list2887z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_getzd2ellipsiszd2framesz00zz__r5_macro_4_3_syntaxz00(obj_t, obj_t);
extern obj_t BGl_mapz00zz__r4_control_features_6_9z00(obj_t, obj_t);
static obj_t BGl_loopze70ze7zz__r5_macro_4_3_syntaxz00(obj_t, obj_t);
static obj_t BGl_loopze71ze7zz__r5_macro_4_3_syntaxz00(obj_t, obj_t);
static obj_t BGl_loopze72ze7zz__r5_macro_4_3_syntaxz00(obj_t, obj_t);
extern obj_t BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_loopze73ze7zz__r5_macro_4_3_syntaxz00(obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31244ze3ze5zz__r5_macro_4_3_syntaxz00(obj_t, obj_t, obj_t);
static obj_t BGl_list2897z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_syntaxzd2mutexzd2zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza31843ze3ze5zz__r5_macro_4_3_syntaxz00(obj_t, obj_t, obj_t);
static obj_t BGl_hygienezd2valuezd2zz__r5_macro_4_3_syntaxz00(obj_t);
static obj_t BGl_subze70ze7zz__r5_macro_4_3_syntaxz00(obj_t, obj_t);
extern obj_t string_append(obj_t, obj_t);
static obj_t BGl_z62expandzd2letreczd2syntaxz62zz__r5_macro_4_3_syntaxz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62expandzd2letzd2syntaxz62zz__r5_macro_4_3_syntaxz00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol2719z00zz__r5_macro_4_3_syntaxz00 = BUNSPEC;
extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
static obj_t BGl_syntaxzd2expanderzd2zz__r5_macro_4_3_syntaxz00(obj_t, obj_t);
static obj_t BGl_flattenz00zz__r5_macro_4_3_syntaxz00(obj_t);
static obj_t BGl_z62syntaxzd2expanderzb0zz__r5_macro_4_3_syntaxz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31853ze3ze5zz__r5_macro_4_3_syntaxz00(obj_t, obj_t, obj_t);
static obj_t *__cnst;


DEFINE_STRING( BGl_string2920z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2920za700za7za7_2995za7, "Illegal bindings", 16 );
DEFINE_STRING( BGl_string2921z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2921za700za7za7_2996za7, "&expand-letrec-syntax", 21 );
DEFINE_STRING( BGl_string2840z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2840za700za7za7_2997za7, "y", 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_installzd2syntaxzd2expanderzd2envzd2zz__r5_macro_4_3_syntaxz00, BgL_bgl_za762installza7d2syn2998z00, BGl_z62installzd2syntaxzd2expanderz62zz__r5_macro_4_3_syntaxz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2922z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2922za700za7za7_2999za7, "<@anonymous:1340>", 17 );
DEFINE_STRING( BGl_string2923z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2923za700za7za7_3000za7, "<@anonymous:1340>:Wrong number of arguments", 43 );
DEFINE_STRING( BGl_string2761z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2761za700za7za7_3001za7, "name", 4 );
DEFINE_STRING( BGl_string2681z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2681za700za7za7_3002za7, "test", 4 );
DEFINE_STRING( BGl_string2763z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2763za700za7za7_3003za7, "val", 3 );
DEFINE_STRING( BGl_string2926z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2926za700za7za7_3004za7, "e3", 2 );
DEFINE_STRING( BGl_string2683z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2683za700za7za7_3005za7, "result", 6 );
DEFINE_STRING( BGl_string2846z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2846za700za7za7_3006za7, "newtemp", 7 );
DEFINE_STRING( BGl_string2765z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2765za700za7za7_3007za7, "body1", 5 );
DEFINE_STRING( BGl_string2928z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2928za700za7za7_3008za7, "e2", 2 );
DEFINE_STRING( BGl_string2848z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2848za700za7za7_3009za7, "do", 2 );
DEFINE_STRING( BGl_string2767z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2767za700za7za7_3010za7, "body2", 5 );
DEFINE_STRING( BGl_string2686z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2686za700za7za7_3011za7, "let", 3 );
DEFINE_STRING( BGl_string2931z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2931za700za7za7_3012za7, "e4", 2 );
DEFINE_STRING( BGl_string2932z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2932za700za7za7_3013za7, "<@anonymous:1354>", 17 );
DEFINE_STRING( BGl_string2933z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2933za700za7za7_3014za7, "<@anonymous:1354>:Wrong number of arguments", 43 );
DEFINE_STRING( BGl_string2771z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2771za700za7za7_3015za7, "lambda", 6 );
DEFINE_STRING( BGl_string2690z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2690za700za7za7_3016za7, "temp", 4 );
DEFINE_STRING( BGl_string2936z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2936za700za7za7_3017za7, "arg1358", 7 );
DEFINE_STRING( BGl_string2855z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2855za700za7za7_3018za7, "var", 3 );
DEFINE_STRING( BGl_string2693z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2693za700za7za7_3019za7, "if", 2 );
DEFINE_STRING( BGl_string2937z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2937za700za7za7_3020za7, "TAG-177", 7 );
DEFINE_STRING( BGl_string2938z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2938za700za7za7_3021za7, "TAG-192", 7 );
DEFINE_STRING( BGl_string2857z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2857za700za7za7_3022za7, "init", 4 );
DEFINE_STRING( BGl_string2776z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2776za700za7za7_3023za7, "tag", 3 );
DEFINE_STRING( BGl_string2939z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2939za700za7za7_3024za7, "loop~1", 6 );
DEFINE_STRING( BGl_string2859z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2859za700za7za7_3025za7, "step", 4 );
DEFINE_STRING( BGl_string2698z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2698za700za7za7_3026za7, "clause1", 7 );
DEFINE_STRING( BGl_string2940z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2940za700za7za7_3027za7, "&expand-let-syntax", 18 );
DEFINE_STRING( BGl_string2941z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2941za700za7za7_3028za7, "<@anonymous:1376>", 17 );
DEFINE_STRING( BGl_string2942z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2942za700za7za7_3029za7, "<@anonymous:1376>:Wrong number of arguments", 43 );
DEFINE_STRING( BGl_string2780z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2780za700za7za7_3030za7, "letrec", 6 );
DEFINE_STRING( BGl_string2862z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2862za700za7za7_3031za7, "expr", 4 );
DEFINE_STRING( BGl_string2944z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2944za700za7za7_3032za7, "Illegal declaration", 19 );
DEFINE_STRING( BGl_string2945z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2945za700za7za7_3033za7, "&syntax-rules->expander", 23 );
DEFINE_STRING( BGl_string2864z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2864za700za7za7_3034za7, "command", 7 );
DEFINE_STRING( BGl_string2946z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2946za700za7za7_3035za7, "No matching clause", 18 );
DEFINE_STRING( BGl_string2784z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2784za700za7za7_3036za7, "let*", 4 );
DEFINE_STRING( BGl_string2947z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2947za700za7za7_3037za7, "TAG-220", 7 );
DEFINE_STRING( BGl_string2948z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2948za700za7za7_3038za7, "TAG-220:Wrong number of arguments", 33 );
DEFINE_STRING( BGl_string2869z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2869za700za7za7_3039za7, "loop", 4 );
DEFINE_STRING( BGl_string2951z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2951za700za7za7_3040za7, "arg1395", 7 );
DEFINE_STRING( BGl_string2952z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2952za700za7za7_3041za7, "TAG-221", 7 );
DEFINE_STRING( BGl_string2953z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2953za700za7za7_3042za7, "Illegal clause", 14 );
DEFINE_STRING( BGl_string2954z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2954za700za7za7_3043za7, "syntax-matches-pattern?", 23 );
DEFINE_STRING( BGl_string2955z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2955za700za7za7_3044za7, "every", 5 );
DEFINE_STRING( BGl_string2956z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2956za700za7za7_3045za7, "Illegal ellipsis", 16 );
DEFINE_STRING( BGl_string2794z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2794za700za7za7_3046za7, "name1", 5 );
DEFINE_STRING( BGl_string2957z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2957za700za7za7_3047za7, "syntax-get-frames", 17 );
DEFINE_STRING( BGl_string2796z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2796za700za7za7_3048za7, "val1", 4 );
DEFINE_STRING( BGl_string2959z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2959za700za7za7_3049za7, "ellipsis", 8 );
DEFINE_STRING( BGl_string2799z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2799za700za7za7_3050za7, "name2", 5 );
DEFINE_STRING( BGl_string2960z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2960za700za7za7_3051za7, "<@anonymous:1453>", 17 );
DEFINE_STRING( BGl_string2961z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2961za700za7za7_3052za7, "syntax-expand-pattern", 21 );
DEFINE_STRING( BGl_string2962z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2962za700za7za7_3053za7, "<@anonymous:1485>", 17 );
DEFINE_STRING( BGl_string2963z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2963za700za7za7_3054za7, "<@anonymous:1492>", 17 );
DEFINE_STRING( BGl_string2964z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2964za700za7za7_3055za7, "any", 3 );
DEFINE_STRING( BGl_string2965z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2965za700za7za7_3056za7, "<@anonymous:1480>", 17 );
DEFINE_STRING( BGl_string2966z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2966za700za7za7_3057za7, "liip", 4 );
DEFINE_STRING( BGl_string2885z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2885za700za7za7_3058za7, "syntax-expander", 15 );
DEFINE_STRING( BGl_string2967z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2967za700za7za7_3059za7, "sub~0", 5 );
DEFINE_STRING( BGl_string2886z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2886za700za7za7_3060za7, "syntax-expander:Wrong number of arguments", 41 );
DEFINE_STRING( BGl_string2968z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2968za700za7za7_3061za7, "ellipsis?", 9 );
DEFINE_STRING( BGl_string2969z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2969za700za7za7_3062za7, "TAG-234", 7 );
DEFINE_STRING( BGl_string2889z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2889za700za7za7_3063za7, "funcall", 7 );
DEFINE_STRING( BGl_string2970z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2970za700za7za7_3064za7, "TAG-236", 7 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2883z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_za762za7c3za704anonymo3065za7, BGl_z62zc3z04anonymousza31244ze3ze5zz__r5_macro_4_3_syntaxz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2971z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2971za700za7za7_3066za7, "TAG-237", 7 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2884z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_za762za7c3za704anonymo3067za7, BGl_z62zc3z04anonymousza31273ze3ze5zz__r5_macro_4_3_syntaxz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2972z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2972za700za7za7_3068za7, "<@anonymous:1829>", 17 );
DEFINE_STRING( BGl_string2891z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2891za700za7za7_3069za7, "e1", 2 );
DEFINE_STRING( BGl_string2974z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2974za700za7za7_3070za7, "TAG-238", 7 );
DEFINE_STRING( BGl_string2893z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2893za700za7za7_3071za7, "e", 1 );
DEFINE_STRING( BGl_string2975z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2975za700za7za7_3072za7, "<@anonymous:1858>", 17 );
DEFINE_STRING( BGl_string2894z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2894za700za7za7_3073za7, "epair", 5 );
extern obj_t BGl_appendzd2envzd2zz__r4_pairs_and_lists_6_3z00;
DEFINE_STRING( BGl_string2895z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2895za700za7za7_3074za7, "loop~3", 6 );
DEFINE_STRING( BGl_string2977z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2977za700za7za7_3075za7, "TAG-240", 7 );
DEFINE_STRING( BGl_string2896z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2896za700za7za7_3076za7, "loop~3:Wrong number of arguments", 32 );
DEFINE_STRING( BGl_string2978z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2978za700za7za7_3077za7, "<@anonymous:1906>", 17 );
DEFINE_STRING( BGl_string2899z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2899za700za7za7_3078za7, "arg1252", 7 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2973z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_za762za7c3za704anonymo3079za7, BGl_z62zc3z04anonymousza31823ze3ze5zz__r5_macro_4_3_syntaxz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2980z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2980za700za7za7_3080za7, "TAG-241", 7 );
DEFINE_STRING( BGl_string2982z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2982za700za7za7_3081za7, "bind-exit", 9 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2976z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_za762za7c3za704anonymo3082za7, BGl_z62zc3z04anonymousza31853ze3ze5zz__r5_macro_4_3_syntaxz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2983z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2983za700za7za7_3083za7, "hygienize", 9 );
DEFINE_STRING( BGl_string2984z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2984za700za7za7_3084za7, "<@anonymous:1891>", 17 );
DEFINE_STRING( BGl_string2985z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2985za700za7za7_3085za7, "<@anonymous:1902>", 17 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2979z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_za762za7c3za704anonymo3086za7, BGl_z62zc3z04anonymousza31902ze3ze5zz__r5_macro_4_3_syntaxz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2986z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2986za700za7za7_3087za7, "<@anonymous:1843>", 17 );
DEFINE_STRING( BGl_string2987z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2987za700za7za7_3088za7, "<@anonymous:1853>", 17 );
DEFINE_STRING( BGl_string2988z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2988za700za7za7_3089za7, "<@anonymous:1810>", 17 );
DEFINE_STRING( BGl_string2989z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2989za700za7za7_3090za7, "<@anonymous:1823>", 17 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_expandzd2definezd2syntaxzd2envzd2zz__r5_macro_4_3_syntaxz00, BgL_bgl_za762expandza7d2defi3091z00, BGl_z62expandzd2definezd2syntaxz62zz__r5_macro_4_3_syntaxz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2990z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2990za700za7za7_3092za7, "TAG-939", 7 );
DEFINE_STRING( BGl_string2991z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2991za700za7za7_3093za7, "__r5_macro_4_3_syntax", 21 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_expandzd2letzd2syntaxzd2envzd2zz__r5_macro_4_3_syntaxz00, BgL_bgl_za762expandza7d2letza73094za7, BGl_z62expandzd2letzd2syntaxz62zz__r5_macro_4_3_syntaxz00, 0L, BUNSPEC, 2 );
extern obj_t BGl_conszd2envzd2zz__r4_pairs_and_lists_6_3z00;
DEFINE_STRING( BGl_string2700z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2700za700za7za7_3095za7, "clause2", 7 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_syntaxzd2expanderzd2envz00zz__r5_macro_4_3_syntaxz00, BgL_bgl_za762syntaxza7d2expa3096z00, BGl_z62syntaxzd2expanderzb0zz__r5_macro_4_3_syntaxz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2718z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2718za700za7za7_3097za7, "init-syntax-expanders!", 22 );
DEFINE_STRING( BGl_string2801z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2801za700za7za7_3098za7, "val2", 4 );
DEFINE_STRING( BGl_string2720z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2720za700za7za7_3099za7, "case", 4 );
DEFINE_STRING( BGl_string2645z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2645za700za7za7_3100za7, "hygiene.r5rs.mark", 17 );
DEFINE_STRING( BGl_string2727z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2727za700za7za7_3101za7, "key", 3 );
DEFINE_STRING( BGl_string2647z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2647za700za7za7_3102za7, "mutex", 5 );
DEFINE_STRING( BGl_string2729z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2729za700za7za7_3103za7, "clauses", 7 );
DEFINE_STRING( BGl_string2648z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2648za700za7za7_3104za7, "/tmp/bigloo/runtime/R5rs/syntax.scm", 35 );
DEFINE_STRING( BGl_string2649z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2649za700za7za7_3105za7, "get-syntax-expander", 19 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_syntaxzd2ruleszd2ze3expanderzd2envz31zz__r5_macro_4_3_syntaxz00, BgL_bgl_za762syntaxza7d2rule3106z00, BGl_z62syntaxzd2ruleszd2ze3expanderz81zz__r5_macro_4_3_syntaxz00, 0L, BUNSPEC, 3 );
DEFINE_STRING( BGl_string2812z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2812za700za7za7_3107za7, "var1", 4 );
DEFINE_STRING( BGl_string2650z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2650za700za7za7_3108za7, "pair-nil", 8 );
DEFINE_STRING( BGl_string2651z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2651za700za7za7_3109za7, "&install-syntax-expander", 24 );
DEFINE_STRING( BGl_string2814z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2814za700za7za7_3110za7, "init1", 5 );
DEFINE_STRING( BGl_string2652z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2652za700za7za7_3111za7, "symbol", 6 );
DEFINE_STRING( BGl_string2734z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2734za700za7za7_3112za7, "atom-key", 8 );
DEFINE_STRING( BGl_string2653z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2653za700za7za7_3113za7, "procedure", 9 );
DEFINE_STRING( BGl_string2816z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2816za700za7za7_3114za7, "body", 4 );
DEFINE_STRING( BGl_string2818z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2818za700za7za7_3115za7, "generate temp names", 19 );
DEFINE_STRING( BGl_string2656z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2656za700za7za7_3116za7, "quote", 5 );
DEFINE_STRING( BGl_string2658z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2658za700za7za7_3117za7, "cond", 4 );
DEFINE_STRING( BGl_string2902z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2902za700za7za7_3118za7, "syntax-rules", 12 );
DEFINE_STRING( BGl_string2903z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2903za700za7za7_3119za7, "expand-define-syntax", 20 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2654z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_za762za7c3za704anonymo3120za7, BGl_z62zc3z04anonymousza31231ze3ze5zz__r5_macro_4_3_syntaxz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2904z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2904za700za7za7_3121za7, "define-syntax", 13 );
DEFINE_STRING( BGl_string2661z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2661za700za7za7_3122za7, "else", 4 );
DEFINE_STRING( BGl_string2905z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2905za700za7za7_3123za7, "Illegal form", 12 );
DEFINE_STRING( BGl_string2824z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2824za700za7za7_3124za7, "temp1", 5 );
DEFINE_STRING( BGl_string2743z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2743za700za7za7_3125za7, "atoms", 5 );
DEFINE_STRING( BGl_string2906z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2906za700za7za7_3126za7, "&expand-define-syntax", 21 );
DEFINE_STRING( BGl_string2663z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2663za700za7za7_3127za7, "=>", 2 );
DEFINE_STRING( BGl_string2907z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2907za700za7za7_3128za7, "<@anonymous:1318>", 17 );
DEFINE_STRING( BGl_string2908z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2908za700za7za7_3129za7, "<@anonymous:1318>:Wrong number of arguments", 43 );
DEFINE_STRING( BGl_string2747z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2747za700za7za7_3130za7, "memv", 4 );
DEFINE_STRING( BGl_string2669z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2669za700za7za7_3131za7, "result1", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_expandzd2letreczd2syntaxzd2envzd2zz__r5_macro_4_3_syntaxz00, BgL_bgl_za762expandza7d2letr3132z00, BGl_z62expandzd2letreczd2syntaxz62zz__r5_macro_4_3_syntaxz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2911z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2911za700za7za7_3133za7, "arg1322", 7 );
DEFINE_STRING( BGl_string2912z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2912za700za7za7_3134za7, "map", 3 );
DEFINE_STRING( BGl_string2913z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2913za700za7za7_3135za7, "list", 4 );
DEFINE_STRING( BGl_string2914z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2914za700za7za7_3136za7, "TAG-134", 7 );
DEFINE_STRING( BGl_string2833z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2833za700za7za7_3137za7, "set!", 4 );
DEFINE_STRING( BGl_string2752z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2752za700za7za7_3138za7, "clause", 6 );
DEFINE_STRING( BGl_string2671z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2671za700za7za7_3139za7, "result2", 7 );
DEFINE_STRING( BGl_string2915z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2915za700za7za7_3140za7, "letrec-syntax", 13 );
DEFINE_STRING( BGl_string2916z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2916za700za7za7_3141za7, "TAG-149", 7 );
DEFINE_STRING( BGl_string2673z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2673za700za7za7_3142za7, "...", 3 );
DEFINE_STRING( BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2917za700za7za7_3143za7, "pair", 4 );
DEFINE_STRING( BGl_string2918z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2918za700za7za7_3144za7, "loop~2", 6 );
DEFINE_STRING( BGl_string2919z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2919za700za7za7_3145za7, "let-syntax", 10 );
DEFINE_STRING( BGl_string2838z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2838za700za7za7_3146za7, "x", 1 );
DEFINE_STRING( BGl_string2676z00zz__r5_macro_4_3_syntaxz00, BgL_bgl_string2676za700za7za7_3147za7, "begin", 5 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_symbol2800z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_hygienezd2prefixzd2zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2644z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2726z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2646z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2728z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2811z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2813z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2733z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2815z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2701z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2702z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2703z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2655z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2704z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2705z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2657z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2706z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2707z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2708z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2709z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_hygienezd2markzd2zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2901z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2660z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2823z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2742z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2710z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2662z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2711z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2712z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2746z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2713z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2714z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2715z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2716z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2668z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2717z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_keyword2958z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2910z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2832z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2751z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2670z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2802z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2721z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2803z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2722z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2837z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2804z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2723z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2675z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2805z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2724z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2839z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2806z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2725z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2807z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2808z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2809z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2760z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2680z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2810z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2762z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2925z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2730z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2682z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2845z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2764z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2731z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2927z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2732z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2847z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2766z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2685z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2735z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2817z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2736z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2737z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2689z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2819z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2738z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2739z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2659z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_syntaxesz00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2930z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2770z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2900z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2820z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2935z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2854z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2821z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2740z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2692z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2822z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2741z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2856z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2775z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2858z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2825z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2744z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2826z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2745z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2697z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2664z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2827z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2779z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2665z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2909z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2828z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2699z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2666z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2829z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2748z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2667z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2749z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2861z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2863z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2830z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2831z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2783z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2750z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2834z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2753z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2868z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2835z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2754z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2836z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2755z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2674z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2756z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2757z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2758z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2677z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2759z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2678z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2679z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2950z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2841z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2793z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2842z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2924z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2843z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2795z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2844z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2798z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2684z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2929z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2849z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2768z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2687z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2769z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2688z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2850z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2851z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2852z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2934z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2853z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2772z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2691z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2773z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2888z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2774z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2694z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2695z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2777z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2696z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2778z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_syntaxzd2expanderszd2mutexz00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2890z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2892z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2860z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2943z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2781z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2782z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2898z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2865z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2866z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2785z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2867z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2786z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2949z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2787z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2788z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2789z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2981z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2870z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2871z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2790z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2872z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2791z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2873z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2792z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2874z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2875z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2876z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2877z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2878z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2797z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2879z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2880z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2881z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2882z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2887z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_list2897z00zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_syntaxzd2mutexzd2zz__r5_macro_4_3_syntaxz00) );
ADD_ROOT( (void *)(&BGl_symbol2719z00zz__r5_macro_4_3_syntaxz00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__r5_macro_4_3_syntaxz00(long BgL_checksumz00_3714, char * BgL_fromz00_3715)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__r5_macro_4_3_syntaxz00))
{ 
BGl_requirezd2initializa7ationz75zz__r5_macro_4_3_syntaxz00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__r5_macro_4_3_syntaxz00(); 
BGl_cnstzd2initzd2zz__r5_macro_4_3_syntaxz00(); 
BGl_importedzd2moduleszd2initz00zz__r5_macro_4_3_syntaxz00(); 
return 
BGl_toplevelzd2initzd2zz__r5_macro_4_3_syntaxz00();}  else 
{ 
return BUNSPEC;} } 

}



/* cnst-init */
obj_t BGl_cnstzd2initzd2zz__r5_macro_4_3_syntaxz00(void)
{
{ /* R5rs/syntax.scm 17 */
BGl_symbol2644z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2645z00zz__r5_macro_4_3_syntaxz00); 
BGl_symbol2646z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2647z00zz__r5_macro_4_3_syntaxz00); 
BGl_symbol2655z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2656z00zz__r5_macro_4_3_syntaxz00); 
BGl_symbol2657z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2658z00zz__r5_macro_4_3_syntaxz00); 
BGl_symbol2660z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2661z00zz__r5_macro_4_3_syntaxz00); 
BGl_symbol2662z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2663z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2659z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2660z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2662z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_symbol2668z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2669z00zz__r5_macro_4_3_syntaxz00); 
BGl_symbol2670z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2671z00zz__r5_macro_4_3_syntaxz00); 
BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2673z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2667z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2660z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2668z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2670z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL)))); 
BGl_list2666z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2657z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2667z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_symbol2675z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2676z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2674z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2675z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2668z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2670z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL)))); 
BGl_list2665z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2666z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2674z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_symbol2680z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2681z00zz__r5_macro_4_3_syntaxz00); 
BGl_symbol2682z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2683z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2679z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2680z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2662z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2682z00zz__r5_macro_4_3_syntaxz00, BNIL))); 
BGl_list2678z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2657z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2679z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_symbol2685z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2686z00zz__r5_macro_4_3_syntaxz00); 
BGl_symbol2689z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2690z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2688z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2689z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2680z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_list2687z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2688z00zz__r5_macro_4_3_syntaxz00, BNIL); 
BGl_symbol2692z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2693z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2694z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2682z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2689z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_list2691z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2692z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2689z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2694z00zz__r5_macro_4_3_syntaxz00, BNIL))); 
BGl_list2684z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2685z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2687z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2691z00zz__r5_macro_4_3_syntaxz00, BNIL))); 
BGl_list2677z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2678z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2684z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_symbol2697z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2698z00zz__r5_macro_4_3_syntaxz00); 
BGl_symbol2699z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2700z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2696z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2657z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2679z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2697z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2699z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL))))); 
BGl_list2703z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2657z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2697z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2699z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL)))); 
BGl_list2702z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2692z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2689z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2694z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2703z00zz__r5_macro_4_3_syntaxz00, BNIL)))); 
BGl_list2701z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2685z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2687z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2702z00zz__r5_macro_4_3_syntaxz00, BNIL))); 
BGl_list2695z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2696z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2701z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_list2706z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2680z00zz__r5_macro_4_3_syntaxz00, BNIL); 
BGl_list2705z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2657z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2706z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_list2704z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2705z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2680z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_list2708z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2657z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2706z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2697z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2699z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL))))); 
BGl_list2710z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2692z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2689z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2689z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2703z00zz__r5_macro_4_3_syntaxz00, BNIL)))); 
BGl_list2709z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2685z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2687z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2710z00zz__r5_macro_4_3_syntaxz00, BNIL))); 
BGl_list2707z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2708z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2709z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_list2713z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2680z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2668z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2670z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL)))); 
BGl_list2712z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2657z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2713z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_list2714z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2692z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2680z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2674z00zz__r5_macro_4_3_syntaxz00, BNIL))); 
BGl_list2711z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2712z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2714z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_list2716z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2657z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2713z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2697z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2699z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL))))); 
BGl_list2717z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2692z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2680z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2674z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2703z00zz__r5_macro_4_3_syntaxz00, BNIL)))); 
BGl_list2715z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2716z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2717z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_list2664z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2665z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2677z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2695z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2704z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2707z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2711z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2715z00zz__r5_macro_4_3_syntaxz00, BNIL))))))); 
BGl_symbol2719z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2720z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2721z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2660z00zz__r5_macro_4_3_syntaxz00, BNIL); 
BGl_symbol2726z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2727z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2725z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2726z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_symbol2728z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2729z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2724z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2719z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2725z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2728z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL)))); 
BGl_symbol2733z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2734z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2732z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2733z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2725z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_list2731z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2732z00zz__r5_macro_4_3_syntaxz00, BNIL); 
BGl_list2735z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2719z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2733z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2728z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL)))); 
BGl_list2730z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2685z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2731z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2735z00zz__r5_macro_4_3_syntaxz00, BNIL))); 
BGl_list2723z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2724z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2730z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_list2737z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2719z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2726z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2667z00zz__r5_macro_4_3_syntaxz00, BNIL))); 
BGl_list2736z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2737z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2674z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_symbol2742z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2743z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2741z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2742z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_list2740z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2741z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2668z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2670z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL)))); 
BGl_list2739z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2719z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2726z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2740z00zz__r5_macro_4_3_syntaxz00, BNIL))); 
BGl_symbol2746z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2747z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2748z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2655z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2741z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_list2745z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2746z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2726z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2748z00zz__r5_macro_4_3_syntaxz00, BNIL))); 
BGl_list2744z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2692z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2745z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2674z00zz__r5_macro_4_3_syntaxz00, BNIL))); 
BGl_list2738z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2739z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2744z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_symbol2751z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2752z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2750z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2719z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2726z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2740z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2751z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2728z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL)))))); 
BGl_list2754z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2719z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2726z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2751z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2728z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL))))); 
BGl_list2753z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2692z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2745z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2674z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2754z00zz__r5_macro_4_3_syntaxz00, BNIL)))); 
BGl_list2749z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2750z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2753z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_list2722z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2723z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2736z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2738z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2749z00zz__r5_macro_4_3_syntaxz00, BNIL)))); 
BGl_symbol2760z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2761z00zz__r5_macro_4_3_syntaxz00); 
BGl_symbol2762z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2763z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2759z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2760z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2762z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_list2758z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2759z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_symbol2764z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2765z00zz__r5_macro_4_3_syntaxz00); 
BGl_symbol2766z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2767z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2757z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2685z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2758z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2764z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2766z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL))))); 
BGl_symbol2770z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2771z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2772z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2760z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_list2769z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2770z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2772z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2764z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2766z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL))))); 
BGl_list2768z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2769z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2762z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL))); 
BGl_list2756z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2757z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2768z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_symbol2775z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2776z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2774z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2685z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2775z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2758z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2764z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2766z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL)))))); 
BGl_symbol2779z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2780z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2782z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2775z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2769z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_list2781z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2782z00zz__r5_macro_4_3_syntaxz00, BNIL); 
BGl_list2778z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2779z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2781z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2775z00zz__r5_macro_4_3_syntaxz00, BNIL))); 
BGl_list2777z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2778z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2762z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL))); 
BGl_list2773z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2774z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2777z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_list2755z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2756z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2773z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_symbol2783z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2784z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2787z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2783z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BNIL, 
MAKE_YOUNG_PAIR(BGl_symbol2764z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2766z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL))))); 
BGl_list2788z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2685z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BNIL, 
MAKE_YOUNG_PAIR(BGl_symbol2764z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2766z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL))))); 
BGl_list2786z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2787z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2788z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_symbol2793z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2794z00zz__r5_macro_4_3_syntaxz00); 
BGl_symbol2795z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2796z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2792z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2793z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2795z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_symbol2798z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2799z00zz__r5_macro_4_3_syntaxz00); 
BGl_symbol2800z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2801z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2797z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2798z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2800z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_list2791z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2792z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2797z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL))); 
BGl_list2790z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2783z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2791z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2764z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2766z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL))))); 
BGl_list2803z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2792z00zz__r5_macro_4_3_syntaxz00, BNIL); 
BGl_list2805z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2797z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_list2804z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2783z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2805z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2764z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2766z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL))))); 
BGl_list2802z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2685z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2803z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2804z00zz__r5_macro_4_3_syntaxz00, BNIL))); 
BGl_list2789z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2790z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2802z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_list2785z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2786z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2789z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_symbol2811z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2812z00zz__r5_macro_4_3_syntaxz00); 
BGl_symbol2813z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2814z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2810z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2811z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2813z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_list2809z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2810z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_symbol2815z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2816z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2808z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2779z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2809z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2815z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL)))); 
BGl_list2819z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2811z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_list2817z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2779z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_string2818z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2819z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BNIL, 
MAKE_YOUNG_PAIR(BGl_list2809z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2815z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL))))))); 
BGl_list2807z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2808z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2817z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_symbol2823z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2824z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2822z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2823z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_list2821z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2779z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_string2818z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BNIL, 
MAKE_YOUNG_PAIR(BGl_list2822z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2809z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2815z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL))))))); 
BGl_list2827z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2811z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BUNSPEC, BNIL)); 
BGl_list2826z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2827z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_list2830z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2823z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2813z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_list2829z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2830z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_symbol2832z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2833z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2831z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2832z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2811z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2823z00zz__r5_macro_4_3_syntaxz00, BNIL))); 
BGl_list2828z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2685z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2829z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2831z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2815z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL)))))); 
BGl_list2825z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2685z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2826z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2828z00zz__r5_macro_4_3_syntaxz00, BNIL))); 
BGl_list2820z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2821z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2825z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_symbol2837z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2838z00zz__r5_macro_4_3_syntaxz00); 
BGl_symbol2839z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2840z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2836z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2837z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2839z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL))); 
BGl_list2841z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2689z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_list2835z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2779z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_string2818z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2836z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2841z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2809z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2815z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL))))))); 
BGl_list2843z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2839z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_symbol2845z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2846z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2844z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2845z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2689z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL))); 
BGl_list2842z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2779z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_string2818z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2843z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2844z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2809z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2815z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL))))))); 
BGl_list2834z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2835z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2842z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_list2806z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2807z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2820z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2834z00zz__r5_macro_4_3_syntaxz00, BNIL))); 
BGl_symbol2847z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2848z00zz__r5_macro_4_3_syntaxz00); 
BGl_symbol2854z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2855z00zz__r5_macro_4_3_syntaxz00); 
BGl_symbol2856z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2857z00zz__r5_macro_4_3_syntaxz00); 
BGl_symbol2858z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2859z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2853z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2854z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2856z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2858z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL)))); 
BGl_list2852z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2853z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_symbol2861z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2862z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2860z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2680z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2861z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL))); 
BGl_symbol2863z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2864z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2851z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2847z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2852z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2860z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2863z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL))))); 
BGl_symbol2868z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2869z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2871z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2854z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_list2874z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2692z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(
BBOOL(((bool_t)0)), 
MAKE_YOUNG_PAIR(
BBOOL(((bool_t)0)), BNIL))); 
BGl_list2873z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2675z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2874z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2861z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL)))); 
BGl_list2877z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2847z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_string2859z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2854z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2858z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL))))); 
BGl_list2876z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2868z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2877z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL))); 
BGl_list2875z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2675z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2863z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2876z00zz__r5_macro_4_3_syntaxz00, BNIL)))); 
BGl_list2872z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2692z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2680z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2873z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2875z00zz__r5_macro_4_3_syntaxz00, BNIL)))); 
BGl_list2870z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2770z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2871z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2872z00zz__r5_macro_4_3_syntaxz00, BNIL))); 
BGl_list2867z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2868z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2870z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_list2866z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2867z00zz__r5_macro_4_3_syntaxz00, BNIL); 
BGl_list2878z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2868z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2856z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00, BNIL))); 
BGl_list2865z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2779z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2866z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2878z00zz__r5_macro_4_3_syntaxz00, BNIL))); 
BGl_list2850z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2851z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2865z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_list2880z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2847z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_string2859z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2837z00zz__r5_macro_4_3_syntaxz00, BNIL))); 
BGl_list2879z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2880z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2837z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_list2882z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2847z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_string2859z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2837z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2839z00zz__r5_macro_4_3_syntaxz00, BNIL)))); 
BGl_list2881z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2882z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2839z00zz__r5_macro_4_3_syntaxz00, BNIL)); 
BGl_list2849z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_list2850z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2879z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_list2881z00zz__r5_macro_4_3_syntaxz00, BNIL))); 
BGl_symbol2888z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2889z00zz__r5_macro_4_3_syntaxz00); 
BGl_symbol2890z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2891z00zz__r5_macro_4_3_syntaxz00); 
BGl_symbol2892z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2893z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2887z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2888z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2890z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2890z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2837z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2892z00zz__r5_macro_4_3_syntaxz00, BNIL))))); 
BGl_symbol2898z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2899z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2897z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2888z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2892z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2892z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2898z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2892z00zz__r5_macro_4_3_syntaxz00, BNIL))))); 
BGl_list2900z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2888z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2892z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2892z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2837z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2892z00zz__r5_macro_4_3_syntaxz00, BNIL))))); 
BGl_symbol2901z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2902z00zz__r5_macro_4_3_syntaxz00); 
BGl_symbol2910z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2911z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2909z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2888z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2890z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2890z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2910z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2890z00zz__r5_macro_4_3_syntaxz00, BNIL))))); 
BGl_symbol2925z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2926z00zz__r5_macro_4_3_syntaxz00); 
BGl_symbol2927z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2928z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2924z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2888z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2925z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2925z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2837z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2927z00zz__r5_macro_4_3_syntaxz00, BNIL))))); 
BGl_symbol2930z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2931z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2929z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2888z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2930z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2930z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2837z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2927z00zz__r5_macro_4_3_syntaxz00, BNIL))))); 
BGl_symbol2935z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2936z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2934z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2888z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2890z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2890z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2935z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2890z00zz__r5_macro_4_3_syntaxz00, BNIL))))); 
BGl_list2943z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2888z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2925z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2925z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2837z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2892z00zz__r5_macro_4_3_syntaxz00, BNIL))))); 
BGl_symbol2950z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2951z00zz__r5_macro_4_3_syntaxz00); 
BGl_list2949z00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2888z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2892z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2892z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2950z00zz__r5_macro_4_3_syntaxz00, 
MAKE_YOUNG_PAIR(BGl_symbol2892z00zz__r5_macro_4_3_syntaxz00, BNIL))))); 
BGl_keyword2958z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_keyword(BGl_string2959z00zz__r5_macro_4_3_syntaxz00); 
return ( 
BGl_symbol2981z00zz__r5_macro_4_3_syntaxz00 = 
bstring_to_symbol(BGl_string2982z00zz__r5_macro_4_3_syntaxz00), BUNSPEC) ;} 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__r5_macro_4_3_syntaxz00(void)
{
{ /* R5rs/syntax.scm 17 */
return 
bgl_gc_roots_register();} 

}



/* toplevel-init */
obj_t BGl_toplevelzd2initzd2zz__r5_macro_4_3_syntaxz00(void)
{
{ /* R5rs/syntax.scm 17 */
BGl_hygienezd2markzd2zz__r5_macro_4_3_syntaxz00 = 
BGl_gensymz00zz__r4_symbols_6_4z00(BGl_symbol2644z00zz__r5_macro_4_3_syntaxz00); 
{ /* R5rs/syntax.scm 60 */
 obj_t BgL_symbolz00_2480;
BgL_symbolz00_2480 = BGl_hygienezd2markzd2zz__r5_macro_4_3_syntaxz00; 
{ /* R5rs/syntax.scm 60 */
 obj_t BgL_arg2070z00_2481;
BgL_arg2070z00_2481 = 
SYMBOL_TO_STRING(BgL_symbolz00_2480); 
BGl_hygienezd2prefixzd2zz__r5_macro_4_3_syntaxz00 = 
BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg2070z00_2481); } } 
BGl_hygienezd2prefixzd2lenz00zz__r5_macro_4_3_syntaxz00 = 
STRING_LENGTH(BGl_hygienezd2prefixzd2zz__r5_macro_4_3_syntaxz00); 
BGl_syntaxesz00zz__r5_macro_4_3_syntaxz00 = BFALSE; 
{ /* R5rs/syntax.scm 64 */

{ /* Llib/thread.scm 218 */
 obj_t BgL_namez00_1109;
BgL_namez00_1109 = 
BGl_gensymz00zz__r4_symbols_6_4z00(BGl_symbol2646z00zz__r5_macro_4_3_syntaxz00); 
{ /* Llib/thread.scm 218 */

BGl_syntaxzd2mutexzd2zz__r5_macro_4_3_syntaxz00 = 
bgl_make_mutex(BgL_namez00_1109); } } } 
{ /* R5rs/syntax.scm 65 */

{ /* Llib/thread.scm 218 */
 obj_t BgL_namez00_1110;
BgL_namez00_1110 = 
BGl_gensymz00zz__r4_symbols_6_4z00(BGl_symbol2646z00zz__r5_macro_4_3_syntaxz00); 
{ /* Llib/thread.scm 218 */

return ( 
BGl_syntaxzd2expanderszd2mutexz00zz__r5_macro_4_3_syntaxz00 = 
bgl_make_mutex(BgL_namez00_1110), BUNSPEC) ;} } } } 

}



/* append-21011 */
obj_t BGl_appendzd221011zd2zz__r5_macro_4_3_syntaxz00(obj_t BgL_l1z00_1, obj_t BgL_l2z00_2)
{
{ 
{ 
 obj_t BgL_headz00_1111;
BgL_headz00_1111 = 
MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2); 
{ 
 obj_t BgL_prevz00_1112; obj_t BgL_tailz00_1113;
BgL_prevz00_1112 = BgL_headz00_1111; 
BgL_tailz00_1113 = BgL_l1z00_1; 
BgL_loopz00_1114:
if(
PAIRP(BgL_tailz00_1113))
{ 
 obj_t BgL_newzd2prevzd2_1116;
BgL_newzd2prevzd2_1116 = 
MAKE_YOUNG_PAIR(
CAR(BgL_tailz00_1113), BgL_l2z00_2); 
SET_CDR(BgL_prevz00_1112, BgL_newzd2prevzd2_1116); 
{ 
 obj_t BgL_tailz00_4237; obj_t BgL_prevz00_4236;
BgL_prevz00_4236 = BgL_newzd2prevzd2_1116; 
BgL_tailz00_4237 = 
CDR(BgL_tailz00_1113); 
BgL_tailz00_1113 = BgL_tailz00_4237; 
BgL_prevz00_1112 = BgL_prevz00_4236; 
goto BgL_loopz00_1114;} }  else 
{ BNIL; } 
return 
CDR(BgL_headz00_1111);} } } 

}



/* get-syntax-expander */
obj_t BGl_getzd2syntaxzd2expanderz00zz__r5_macro_4_3_syntaxz00(obj_t BgL_fz00_3)
{
{ /* R5rs/syntax.scm 70 */
{ /* R5rs/syntax.scm 71 */
 obj_t BgL_idz00_1119;
BgL_idz00_1119 = 
BGl_hygienezd2valueza2z70zz__r5_macro_4_3_syntaxz00(BgL_fz00_3); 
{ /* R5rs/syntax.scm 71 */
 obj_t BgL_cz00_1120;
{ /* R5rs/syntax.scm 72 */
 obj_t BgL_top3151z00_4242;
BgL_top3151z00_4242 = 
BGL_EXITD_TOP_AS_OBJ(); 
BGL_MUTEX_LOCK(BGl_syntaxzd2mutexzd2zz__r5_macro_4_3_syntaxz00); 
BGL_EXITD_PUSH_PROTECT(BgL_top3151z00_4242, BGl_syntaxzd2mutexzd2zz__r5_macro_4_3_syntaxz00); BUNSPEC; 
{ /* R5rs/syntax.scm 72 */
 obj_t BgL_tmp3150z00_4241;
{ /* R5rs/syntax.scm 72 */
 obj_t BgL_auxz00_4246;
{ /* R5rs/syntax.scm 72 */
 obj_t BgL_aux2270z00_3186;
BgL_aux2270z00_3186 = BGl_syntaxesz00zz__r5_macro_4_3_syntaxz00; 
{ /* R5rs/syntax.scm 72 */
 bool_t BgL_test3152z00_4247;
if(
PAIRP(BgL_aux2270z00_3186))
{ /* R5rs/syntax.scm 72 */
BgL_test3152z00_4247 = ((bool_t)1)
; }  else 
{ /* R5rs/syntax.scm 72 */
BgL_test3152z00_4247 = 
NULLP(BgL_aux2270z00_3186)
; } 
if(BgL_test3152z00_4247)
{ /* R5rs/syntax.scm 72 */
BgL_auxz00_4246 = BgL_aux2270z00_3186
; }  else 
{ 
 obj_t BgL_auxz00_4251;
BgL_auxz00_4251 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(2785L), BGl_string2649z00zz__r5_macro_4_3_syntaxz00, BGl_string2650z00zz__r5_macro_4_3_syntaxz00, BgL_aux2270z00_3186); 
FAILURE(BgL_auxz00_4251,BFALSE,BFALSE);} } } 
BgL_tmp3150z00_4241 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_idz00_1119, BgL_auxz00_4246); } 
BGL_EXITD_POP_PROTECT(BgL_top3151z00_4242); BUNSPEC; 
BGL_MUTEX_UNLOCK(BGl_syntaxzd2mutexzd2zz__r5_macro_4_3_syntaxz00); 
BgL_cz00_1120 = BgL_tmp3150z00_4241; } } 
{ /* R5rs/syntax.scm 72 */

if(
PAIRP(BgL_cz00_1120))
{ /* R5rs/syntax.scm 73 */
return 
CDR(BgL_cz00_1120);}  else 
{ /* R5rs/syntax.scm 73 */
return BFALSE;} } } } } 

}



/* install-syntax-expander */
BGL_EXPORTED_DEF obj_t BGl_installzd2syntaxzd2expanderz00zz__r5_macro_4_3_syntaxz00(obj_t BgL_keywordz00_4, obj_t BgL_expanderz00_5)
{
{ /* R5rs/syntax.scm 79 */
BGL_MUTEX_LOCK(BGl_syntaxzd2mutexzd2zz__r5_macro_4_3_syntaxz00); 
{ /* R5rs/syntax.scm 80 */
 obj_t BgL_tmp3155z00_4261;
{ /* R5rs/syntax.scm 81 */
 obj_t BgL_arg1229z00_2488;
BgL_arg1229z00_2488 = 
MAKE_YOUNG_PAIR(BgL_keywordz00_4, BgL_expanderz00_5); 
BgL_tmp3155z00_4261 = ( 
BGl_syntaxesz00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BgL_arg1229z00_2488, BGl_syntaxesz00zz__r5_macro_4_3_syntaxz00), BUNSPEC) ; } 
BGL_MUTEX_UNLOCK(BGl_syntaxzd2mutexzd2zz__r5_macro_4_3_syntaxz00); 
return BgL_tmp3155z00_4261;} } 

}



/* &install-syntax-expander */
obj_t BGl_z62installzd2syntaxzd2expanderz62zz__r5_macro_4_3_syntaxz00(obj_t BgL_envz00_3096, obj_t BgL_keywordz00_3097, obj_t BgL_expanderz00_3098)
{
{ /* R5rs/syntax.scm 79 */
{ /* R5rs/syntax.scm 80 */
 obj_t BgL_auxz00_4273; obj_t BgL_auxz00_4266;
if(
PROCEDUREP(BgL_expanderz00_3098))
{ /* R5rs/syntax.scm 80 */
BgL_auxz00_4273 = BgL_expanderz00_3098
; }  else 
{ 
 obj_t BgL_auxz00_4276;
BgL_auxz00_4276 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(3110L), BGl_string2651z00zz__r5_macro_4_3_syntaxz00, BGl_string2653z00zz__r5_macro_4_3_syntaxz00, BgL_expanderz00_3098); 
FAILURE(BgL_auxz00_4276,BFALSE,BFALSE);} 
if(
SYMBOLP(BgL_keywordz00_3097))
{ /* R5rs/syntax.scm 80 */
BgL_auxz00_4266 = BgL_keywordz00_3097
; }  else 
{ 
 obj_t BgL_auxz00_4269;
BgL_auxz00_4269 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(3110L), BGl_string2651z00zz__r5_macro_4_3_syntaxz00, BGl_string2652z00zz__r5_macro_4_3_syntaxz00, BgL_keywordz00_3097); 
FAILURE(BgL_auxz00_4269,BFALSE,BFALSE);} 
return 
BGl_installzd2syntaxzd2expanderz00zz__r5_macro_4_3_syntaxz00(BgL_auxz00_4266, BgL_auxz00_4273);} } 

}



/* init-syntax-expanders! */
obj_t BGl_initzd2syntaxzd2expandersz12z12zz__r5_macro_4_3_syntaxz00(void)
{
{ /* R5rs/syntax.scm 86 */
{ /* R5rs/syntax.scm 91 */
 obj_t BgL_top3159z00_4282;
BgL_top3159z00_4282 = 
BGL_EXITD_TOP_AS_OBJ(); 
BGL_MUTEX_LOCK(BGl_syntaxzd2expanderszd2mutexz00zz__r5_macro_4_3_syntaxz00); 
BGL_EXITD_PUSH_PROTECT(BgL_top3159z00_4282, BGl_syntaxzd2expanderszd2mutexz00zz__r5_macro_4_3_syntaxz00); BUNSPEC; 
{ /* R5rs/syntax.scm 91 */
 obj_t BgL_tmp3158z00_4281;
if(
CBOOL(BGl_syntaxesz00zz__r5_macro_4_3_syntaxz00))
{ /* R5rs/syntax.scm 92 */
BgL_tmp3158z00_4281 = BFALSE; }  else 
{ /* R5rs/syntax.scm 92 */
BGl_syntaxesz00zz__r5_macro_4_3_syntaxz00 = BNIL; 
{ /* R5rs/syntax.scm 95 */
 obj_t BgL_keywordz00_2493;
BgL_keywordz00_2493 = BGl_symbol2655z00zz__r5_macro_4_3_syntaxz00; 
BGL_MUTEX_LOCK(BGl_syntaxzd2mutexzd2zz__r5_macro_4_3_syntaxz00); 
{ /* R5rs/syntax.scm 80 */
 obj_t BgL_tmp3161z00_4288;
{ /* R5rs/syntax.scm 81 */
 obj_t BgL_arg1229z00_2494;
BgL_arg1229z00_2494 = 
MAKE_YOUNG_PAIR(BgL_keywordz00_2493, BGl_proc2654z00zz__r5_macro_4_3_syntaxz00); 
BgL_tmp3161z00_4288 = ( 
BGl_syntaxesz00zz__r5_macro_4_3_syntaxz00 = 
MAKE_YOUNG_PAIR(BgL_arg1229z00_2494, BGl_syntaxesz00zz__r5_macro_4_3_syntaxz00), BUNSPEC) ; } 
BGL_MUTEX_UNLOCK(BGl_syntaxzd2mutexzd2zz__r5_macro_4_3_syntaxz00); BgL_tmp3161z00_4288; } } 
{ /* R5rs/syntax.scm 97 */
 obj_t BgL_idz00_2495; obj_t BgL_literalsz00_2496; obj_t BgL_rulesz00_2497;
BgL_idz00_2495 = BGl_symbol2657z00zz__r5_macro_4_3_syntaxz00; 
BgL_literalsz00_2496 = BGl_list2659z00zz__r5_macro_4_3_syntaxz00; 
BgL_rulesz00_2497 = BGl_list2664z00zz__r5_macro_4_3_syntaxz00; 
{ /* R5rs/syntax.scm 89 */
 obj_t BgL_arg1233z00_2498;
BgL_arg1233z00_2498 = 
BGl_syntaxzd2ruleszd2ze3expanderze3zz__r5_macro_4_3_syntaxz00(BgL_idz00_2495, BgL_literalsz00_2496, BgL_rulesz00_2497); 
{ /* R5rs/syntax.scm 89 */
 obj_t BgL_auxz00_4294;
if(
PROCEDUREP(BgL_arg1233z00_2498))
{ /* R5rs/syntax.scm 89 */
BgL_auxz00_4294 = BgL_arg1233z00_2498
; }  else 
{ 
 obj_t BgL_auxz00_4297;
BgL_auxz00_4297 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(3589L), BGl_string2718z00zz__r5_macro_4_3_syntaxz00, BGl_string2653z00zz__r5_macro_4_3_syntaxz00, BgL_arg1233z00_2498); 
FAILURE(BgL_auxz00_4297,BFALSE,BFALSE);} 
BGl_installzd2syntaxzd2expanderz00zz__r5_macro_4_3_syntaxz00(BgL_idz00_2495, BgL_auxz00_4294); } } } 
{ /* R5rs/syntax.scm 123 */
 obj_t BgL_idz00_2499; obj_t BgL_literalsz00_2500; obj_t BgL_rulesz00_2501;
BgL_idz00_2499 = BGl_symbol2719z00zz__r5_macro_4_3_syntaxz00; 
BgL_literalsz00_2500 = BGl_list2721z00zz__r5_macro_4_3_syntaxz00; 
BgL_rulesz00_2501 = BGl_list2722z00zz__r5_macro_4_3_syntaxz00; 
{ /* R5rs/syntax.scm 89 */
 obj_t BgL_arg1233z00_2502;
BgL_arg1233z00_2502 = 
BGl_syntaxzd2ruleszd2ze3expanderze3zz__r5_macro_4_3_syntaxz00(BgL_idz00_2499, BgL_literalsz00_2500, BgL_rulesz00_2501); 
{ /* R5rs/syntax.scm 89 */
 obj_t BgL_auxz00_4303;
if(
PROCEDUREP(BgL_arg1233z00_2502))
{ /* R5rs/syntax.scm 89 */
BgL_auxz00_4303 = BgL_arg1233z00_2502
; }  else 
{ 
 obj_t BgL_auxz00_4306;
BgL_auxz00_4306 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(3589L), BGl_string2718z00zz__r5_macro_4_3_syntaxz00, BGl_string2653z00zz__r5_macro_4_3_syntaxz00, BgL_arg1233z00_2502); 
FAILURE(BgL_auxz00_4306,BFALSE,BFALSE);} 
BGl_installzd2syntaxzd2expanderz00zz__r5_macro_4_3_syntaxz00(BgL_idz00_2499, BgL_auxz00_4303); } } } 
{ /* R5rs/syntax.scm 143 */
 obj_t BgL_idz00_2503; obj_t BgL_rulesz00_2504;
BgL_idz00_2503 = BGl_symbol2685z00zz__r5_macro_4_3_syntaxz00; 
BgL_rulesz00_2504 = BGl_list2755z00zz__r5_macro_4_3_syntaxz00; 
{ /* R5rs/syntax.scm 89 */
 obj_t BgL_arg1233z00_2505;
BgL_arg1233z00_2505 = 
BGl_syntaxzd2ruleszd2ze3expanderze3zz__r5_macro_4_3_syntaxz00(BgL_idz00_2503, BNIL, BgL_rulesz00_2504); 
{ /* R5rs/syntax.scm 89 */
 obj_t BgL_auxz00_4312;
if(
PROCEDUREP(BgL_arg1233z00_2505))
{ /* R5rs/syntax.scm 89 */
BgL_auxz00_4312 = BgL_arg1233z00_2505
; }  else 
{ 
 obj_t BgL_auxz00_4315;
BgL_auxz00_4315 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(3589L), BGl_string2718z00zz__r5_macro_4_3_syntaxz00, BGl_string2653z00zz__r5_macro_4_3_syntaxz00, BgL_arg1233z00_2505); 
FAILURE(BgL_auxz00_4315,BFALSE,BFALSE);} 
BGl_installzd2syntaxzd2expanderz00zz__r5_macro_4_3_syntaxz00(BgL_idz00_2503, BgL_auxz00_4312); } } } 
{ /* R5rs/syntax.scm 154 */
 obj_t BgL_idz00_2506; obj_t BgL_rulesz00_2507;
BgL_idz00_2506 = BGl_symbol2783z00zz__r5_macro_4_3_syntaxz00; 
BgL_rulesz00_2507 = BGl_list2785z00zz__r5_macro_4_3_syntaxz00; 
{ /* R5rs/syntax.scm 89 */
 obj_t BgL_arg1233z00_2508;
BgL_arg1233z00_2508 = 
BGl_syntaxzd2ruleszd2ze3expanderze3zz__r5_macro_4_3_syntaxz00(BgL_idz00_2506, BNIL, BgL_rulesz00_2507); 
{ /* R5rs/syntax.scm 89 */
 obj_t BgL_auxz00_4321;
if(
PROCEDUREP(BgL_arg1233z00_2508))
{ /* R5rs/syntax.scm 89 */
BgL_auxz00_4321 = BgL_arg1233z00_2508
; }  else 
{ 
 obj_t BgL_auxz00_4324;
BgL_auxz00_4324 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(3589L), BGl_string2718z00zz__r5_macro_4_3_syntaxz00, BGl_string2653z00zz__r5_macro_4_3_syntaxz00, BgL_arg1233z00_2508); 
FAILURE(BgL_auxz00_4324,BFALSE,BFALSE);} 
BGl_installzd2syntaxzd2expanderz00zz__r5_macro_4_3_syntaxz00(BgL_idz00_2506, BgL_auxz00_4321); } } } 
{ /* R5rs/syntax.scm 164 */
 obj_t BgL_idz00_2509; obj_t BgL_rulesz00_2510;
BgL_idz00_2509 = BGl_symbol2779z00zz__r5_macro_4_3_syntaxz00; 
BgL_rulesz00_2510 = BGl_list2806z00zz__r5_macro_4_3_syntaxz00; 
{ /* R5rs/syntax.scm 89 */
 obj_t BgL_arg1233z00_2511;
BgL_arg1233z00_2511 = 
BGl_syntaxzd2ruleszd2ze3expanderze3zz__r5_macro_4_3_syntaxz00(BgL_idz00_2509, BNIL, BgL_rulesz00_2510); 
{ /* R5rs/syntax.scm 89 */
 obj_t BgL_auxz00_4330;
if(
PROCEDUREP(BgL_arg1233z00_2511))
{ /* R5rs/syntax.scm 89 */
BgL_auxz00_4330 = BgL_arg1233z00_2511
; }  else 
{ 
 obj_t BgL_auxz00_4333;
BgL_auxz00_4333 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(3589L), BGl_string2718z00zz__r5_macro_4_3_syntaxz00, BGl_string2653z00zz__r5_macro_4_3_syntaxz00, BgL_arg1233z00_2511); 
FAILURE(BgL_auxz00_4333,BFALSE,BFALSE);} 
BGl_installzd2syntaxzd2expanderz00zz__r5_macro_4_3_syntaxz00(BgL_idz00_2509, BgL_auxz00_4330); } } } 
{ /* R5rs/syntax.scm 193 */
 obj_t BgL_idz00_2512; obj_t BgL_rulesz00_2513;
BgL_idz00_2512 = BGl_symbol2847z00zz__r5_macro_4_3_syntaxz00; 
BgL_rulesz00_2513 = BGl_list2849z00zz__r5_macro_4_3_syntaxz00; 
{ /* R5rs/syntax.scm 89 */
 obj_t BgL_arg1233z00_2514;
BgL_arg1233z00_2514 = 
BGl_syntaxzd2ruleszd2ze3expanderze3zz__r5_macro_4_3_syntaxz00(BgL_idz00_2512, BNIL, BgL_rulesz00_2513); 
{ /* R5rs/syntax.scm 89 */
 obj_t BgL_auxz00_4339;
if(
PROCEDUREP(BgL_arg1233z00_2514))
{ /* R5rs/syntax.scm 89 */
BgL_auxz00_4339 = BgL_arg1233z00_2514
; }  else 
{ 
 obj_t BgL_auxz00_4342;
BgL_auxz00_4342 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(3589L), BGl_string2718z00zz__r5_macro_4_3_syntaxz00, BGl_string2653z00zz__r5_macro_4_3_syntaxz00, BgL_arg1233z00_2514); 
FAILURE(BgL_auxz00_4342,BFALSE,BFALSE);} 
BgL_tmp3158z00_4281 = 
BGl_installzd2syntaxzd2expanderz00zz__r5_macro_4_3_syntaxz00(BgL_idz00_2512, BgL_auxz00_4339); } } } } 
BGL_EXITD_POP_PROTECT(BgL_top3159z00_4282); BUNSPEC; 
BGL_MUTEX_UNLOCK(BGl_syntaxzd2expanderszd2mutexz00zz__r5_macro_4_3_syntaxz00); 
return BgL_tmp3158z00_4281;} } } 

}



/* &<@anonymous:1231> */
obj_t BGl_z62zc3z04anonymousza31231ze3ze5zz__r5_macro_4_3_syntaxz00(obj_t BgL_envz00_3100, obj_t BgL_xz00_3101, obj_t BgL_ez00_3102)
{
{ /* R5rs/syntax.scm 95 */
return BgL_xz00_3101;} 

}



/* syntax-expander */
obj_t BGl_syntaxzd2expanderzd2zz__r5_macro_4_3_syntaxz00(obj_t BgL_xz00_7, obj_t BgL_ez00_8)
{
{ /* R5rs/syntax.scm 225 */
{ /* R5rs/syntax.scm 226 */
 obj_t BgL_e1z00_1135;
if(
PAIRP(BgL_xz00_7))
{ /* R5rs/syntax.scm 229 */
 obj_t BgL_g1039z00_1146;
BgL_g1039z00_1146 = 
BGl_getzd2syntaxzd2expanderz00zz__r5_macro_4_3_syntaxz00(
CAR(BgL_xz00_7)); 
if(
CBOOL(BgL_g1039z00_1146))
{ /* R5rs/syntax.scm 229 */
BgL_e1z00_1135 = BgL_g1039z00_1146; }  else 
{ /* R5rs/syntax.scm 229 */
BgL_e1z00_1135 = BGl_proc2883z00zz__r5_macro_4_3_syntaxz00; } }  else 
{ /* R5rs/syntax.scm 227 */
BgL_e1z00_1135 = BGl_proc2884z00zz__r5_macro_4_3_syntaxz00; } 
{ /* R5rs/syntax.scm 242 */
 obj_t BgL_newz00_1136;
{ /* R5rs/syntax.scm 242 */
 obj_t BgL_funz00_3206;
if(
PROCEDUREP(BgL_e1z00_1135))
{ /* R5rs/syntax.scm 242 */
BgL_funz00_3206 = BgL_e1z00_1135; }  else 
{ 
 obj_t BgL_auxz00_4357;
BgL_auxz00_4357 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(7584L), BGl_string2885z00zz__r5_macro_4_3_syntaxz00, BGl_string2653z00zz__r5_macro_4_3_syntaxz00, BgL_e1z00_1135); 
FAILURE(BgL_auxz00_4357,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_3206, 2))
{ /* R5rs/syntax.scm 242 */
BgL_newz00_1136 = 
(VA_PROCEDUREP( BgL_funz00_3206 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_3206))(BgL_e1z00_1135, BgL_xz00_7, BgL_ez00_8, BEOA) : ((obj_t (*)(obj_t, obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_3206))(BgL_e1z00_1135, BgL_xz00_7, BgL_ez00_8) ); }  else 
{ /* R5rs/syntax.scm 242 */
FAILURE(BGl_string2886z00zz__r5_macro_4_3_syntaxz00,BGl_list2887z00zz__r5_macro_4_3_syntaxz00,BgL_funz00_3206);} } 
{ /* R5rs/syntax.scm 243 */
 bool_t BgL_test3172z00_4369;
if(
PAIRP(BgL_newz00_1136))
{ /* R5rs/syntax.scm 243 */
if(
EPAIRP(BgL_newz00_1136))
{ /* R5rs/syntax.scm 243 */
BgL_test3172z00_4369 = ((bool_t)0)
; }  else 
{ /* R5rs/syntax.scm 243 */
BgL_test3172z00_4369 = 
EPAIRP(BgL_xz00_7)
; } }  else 
{ /* R5rs/syntax.scm 243 */
BgL_test3172z00_4369 = ((bool_t)0)
; } 
if(BgL_test3172z00_4369)
{ /* R5rs/syntax.scm 244 */
 obj_t BgL_arg1238z00_1140; obj_t BgL_arg1239z00_1141; obj_t BgL_arg1242z00_1142;
BgL_arg1238z00_1140 = 
CAR(BgL_newz00_1136); 
BgL_arg1239z00_1141 = 
CDR(BgL_newz00_1136); 
{ /* R5rs/syntax.scm 244 */
 obj_t BgL_objz00_2520;
if(
EPAIRP(BgL_xz00_7))
{ /* R5rs/syntax.scm 244 */
BgL_objz00_2520 = BgL_xz00_7; }  else 
{ 
 obj_t BgL_auxz00_4379;
BgL_auxz00_4379 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(7687L), BGl_string2885z00zz__r5_macro_4_3_syntaxz00, BGl_string2894z00zz__r5_macro_4_3_syntaxz00, BgL_xz00_7); 
FAILURE(BgL_auxz00_4379,BFALSE,BFALSE);} 
BgL_arg1242z00_1142 = 
CER(BgL_objz00_2520); } 
{ /* R5rs/syntax.scm 244 */
 obj_t BgL_res2260z00_2521;
BgL_res2260z00_2521 = 
MAKE_YOUNG_EPAIR(BgL_arg1238z00_1140, BgL_arg1239z00_1141, BgL_arg1242z00_1142); 
return BgL_res2260z00_2521;} }  else 
{ /* R5rs/syntax.scm 243 */
return BgL_newz00_1136;} } } } } 

}



/* &syntax-expander */
obj_t BGl_z62syntaxzd2expanderzb0zz__r5_macro_4_3_syntaxz00(obj_t BgL_envz00_3105, obj_t BgL_xz00_3106, obj_t BgL_ez00_3107)
{
{ /* R5rs/syntax.scm 225 */
return 
BGl_syntaxzd2expanderzd2zz__r5_macro_4_3_syntaxz00(BgL_xz00_3106, BgL_ez00_3107);} 

}



/* &<@anonymous:1244> */
obj_t BGl_z62zc3z04anonymousza31244ze3ze5zz__r5_macro_4_3_syntaxz00(obj_t BgL_envz00_3108, obj_t BgL_xz00_3109, obj_t BgL_ez00_3110)
{
{ /* R5rs/syntax.scm 233 */
return 
BGl_loopze73ze7zz__r5_macro_4_3_syntaxz00(BgL_ez00_3110, BgL_xz00_3109);} 

}



/* loop~3 */
obj_t BGl_loopze73ze7zz__r5_macro_4_3_syntaxz00(obj_t BgL_ez00_3185, obj_t BgL_xz00_1153)
{
{ /* R5rs/syntax.scm 234 */
if(
PAIRP(BgL_xz00_1153))
{ /* R5rs/syntax.scm 237 */
 obj_t BgL_arg1248z00_1156; obj_t BgL_arg1249z00_1157;
{ /* R5rs/syntax.scm 237 */
 obj_t BgL_arg1252z00_1158;
BgL_arg1252z00_1158 = 
CAR(BgL_xz00_1153); 
{ /* R5rs/syntax.scm 237 */
 obj_t BgL_funz00_3212;
if(
PROCEDUREP(BgL_ez00_3185))
{ /* R5rs/syntax.scm 237 */
BgL_funz00_3212 = BgL_ez00_3185; }  else 
{ 
 obj_t BgL_auxz00_4392;
BgL_auxz00_4392 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(7476L), BGl_string2895z00zz__r5_macro_4_3_syntaxz00, BGl_string2653z00zz__r5_macro_4_3_syntaxz00, BgL_ez00_3185); 
FAILURE(BgL_auxz00_4392,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_3212, 2))
{ /* R5rs/syntax.scm 237 */
BgL_arg1248z00_1156 = 
(VA_PROCEDUREP( BgL_funz00_3212 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_3212))(BgL_ez00_3185, BgL_arg1252z00_1158, BgL_ez00_3185, BEOA) : ((obj_t (*)(obj_t, obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_3212))(BgL_ez00_3185, BgL_arg1252z00_1158, BgL_ez00_3185) ); }  else 
{ /* R5rs/syntax.scm 237 */
FAILURE(BGl_string2896z00zz__r5_macro_4_3_syntaxz00,BGl_list2897z00zz__r5_macro_4_3_syntaxz00,BgL_funz00_3212);} } } 
BgL_arg1249z00_1157 = 
BGl_loopze73ze7zz__r5_macro_4_3_syntaxz00(BgL_ez00_3185, 
CDR(BgL_xz00_1153)); 
return 
MAKE_YOUNG_PAIR(BgL_arg1248z00_1156, BgL_arg1249z00_1157);}  else 
{ /* R5rs/syntax.scm 236 */
if(
NULLP(BgL_xz00_1153))
{ /* R5rs/syntax.scm 238 */
return BNIL;}  else 
{ /* R5rs/syntax.scm 241 */
 obj_t BgL_funz00_3216;
if(
PROCEDUREP(BgL_ez00_3185))
{ /* R5rs/syntax.scm 241 */
BgL_funz00_3216 = BgL_ez00_3185; }  else 
{ 
 obj_t BgL_auxz00_4411;
BgL_auxz00_4411 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(7551L), BGl_string2895z00zz__r5_macro_4_3_syntaxz00, BGl_string2653z00zz__r5_macro_4_3_syntaxz00, BgL_ez00_3185); 
FAILURE(BgL_auxz00_4411,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_3216, 2))
{ /* R5rs/syntax.scm 241 */
return 
(VA_PROCEDUREP( BgL_funz00_3216 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_3216))(BgL_ez00_3185, BgL_xz00_1153, BgL_ez00_3185, BEOA) : ((obj_t (*)(obj_t, obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_3216))(BgL_ez00_3185, BgL_xz00_1153, BgL_ez00_3185) );}  else 
{ /* R5rs/syntax.scm 241 */
FAILURE(BGl_string2896z00zz__r5_macro_4_3_syntaxz00,BGl_list2900z00zz__r5_macro_4_3_syntaxz00,BgL_funz00_3216);} } } } 

}



/* &<@anonymous:1273> */
obj_t BGl_z62zc3z04anonymousza31273ze3ze5zz__r5_macro_4_3_syntaxz00(obj_t BgL_envz00_3111, obj_t BgL_xz00_3112, obj_t BgL_ez00_3113)
{
{ /* R5rs/syntax.scm 228 */
return BgL_xz00_3112;} 

}



/* expand-define-syntax */
BGL_EXPORTED_DEF obj_t BGl_expandzd2definezd2syntaxz00zz__r5_macro_4_3_syntaxz00(obj_t BgL_xz00_9, obj_t BgL_ez00_10)
{
{ /* R5rs/syntax.scm 250 */
if(
PAIRP(BgL_xz00_9))
{ /* R5rs/syntax.scm 251 */
 obj_t BgL_cdrzd2111zd2_1173;
BgL_cdrzd2111zd2_1173 = 
CDR(
((obj_t)BgL_xz00_9)); 
if(
PAIRP(BgL_cdrzd2111zd2_1173))
{ /* R5rs/syntax.scm 251 */
 obj_t BgL_carzd2115zd2_1175; obj_t BgL_cdrzd2116zd2_1176;
BgL_carzd2115zd2_1175 = 
CAR(BgL_cdrzd2111zd2_1173); 
BgL_cdrzd2116zd2_1176 = 
CDR(BgL_cdrzd2111zd2_1173); 
if(
SYMBOLP(BgL_carzd2115zd2_1175))
{ /* R5rs/syntax.scm 251 */
if(
PAIRP(BgL_cdrzd2116zd2_1176))
{ /* R5rs/syntax.scm 251 */
 obj_t BgL_carzd2122zd2_1179;
BgL_carzd2122zd2_1179 = 
CAR(BgL_cdrzd2116zd2_1176); 
if(
PAIRP(BgL_carzd2122zd2_1179))
{ /* R5rs/syntax.scm 251 */
 obj_t BgL_cdrzd2127zd2_1181;
BgL_cdrzd2127zd2_1181 = 
CDR(BgL_carzd2122zd2_1179); 
if(
(
CAR(BgL_carzd2122zd2_1179)==BGl_symbol2901z00zz__r5_macro_4_3_syntaxz00))
{ /* R5rs/syntax.scm 251 */
if(
PAIRP(BgL_cdrzd2127zd2_1181))
{ /* R5rs/syntax.scm 251 */
if(
NULLP(
CDR(BgL_cdrzd2116zd2_1176)))
{ /* R5rs/syntax.scm 251 */
 obj_t BgL_arg1306z00_1187; obj_t BgL_arg1307z00_1188;
BgL_arg1306z00_1187 = 
CAR(BgL_cdrzd2127zd2_1181); 
BgL_arg1307z00_1188 = 
CDR(BgL_cdrzd2127zd2_1181); 
{ /* R5rs/syntax.scm 253 */
 obj_t BgL_exz00_2535;
BGl_initzd2syntaxzd2expandersz12z12zz__r5_macro_4_3_syntaxz00(); 
{ /* R5rs/syntax.scm 313 */
 obj_t BgL_auxz00_4459; obj_t BgL_auxz00_4450;
{ /* R5rs/syntax.scm 313 */
 bool_t BgL_test3192z00_4460;
if(
PAIRP(BgL_arg1307z00_1188))
{ /* R5rs/syntax.scm 313 */
BgL_test3192z00_4460 = ((bool_t)1)
; }  else 
{ /* R5rs/syntax.scm 313 */
BgL_test3192z00_4460 = 
NULLP(BgL_arg1307z00_1188)
; } 
if(BgL_test3192z00_4460)
{ /* R5rs/syntax.scm 313 */
BgL_auxz00_4459 = BgL_arg1307z00_1188
; }  else 
{ 
 obj_t BgL_auxz00_4464;
BgL_auxz00_4464 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(10344L), BGl_string2903z00zz__r5_macro_4_3_syntaxz00, BGl_string2650z00zz__r5_macro_4_3_syntaxz00, BgL_arg1307z00_1188); 
FAILURE(BgL_auxz00_4464,BFALSE,BFALSE);} } 
{ /* R5rs/syntax.scm 313 */
 bool_t BgL_test3190z00_4451;
if(
PAIRP(BgL_arg1306z00_1187))
{ /* R5rs/syntax.scm 313 */
BgL_test3190z00_4451 = ((bool_t)1)
; }  else 
{ /* R5rs/syntax.scm 313 */
BgL_test3190z00_4451 = 
NULLP(BgL_arg1306z00_1187)
; } 
if(BgL_test3190z00_4451)
{ /* R5rs/syntax.scm 313 */
BgL_auxz00_4450 = BgL_arg1306z00_1187
; }  else 
{ 
 obj_t BgL_auxz00_4455;
BgL_auxz00_4455 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(10335L), BGl_string2903z00zz__r5_macro_4_3_syntaxz00, BGl_string2650z00zz__r5_macro_4_3_syntaxz00, BgL_arg1306z00_1187); 
FAILURE(BgL_auxz00_4455,BFALSE,BFALSE);} } 
BgL_exz00_2535 = 
BGl_syntaxzd2ruleszd2ze3expanderze3zz__r5_macro_4_3_syntaxz00(BgL_carzd2115zd2_1175, BgL_auxz00_4450, BgL_auxz00_4459); } 
{ /* R5rs/syntax.scm 254 */
 obj_t BgL_auxz00_4469;
if(
PROCEDUREP(BgL_exz00_2535))
{ /* R5rs/syntax.scm 254 */
BgL_auxz00_4469 = BgL_exz00_2535
; }  else 
{ 
 obj_t BgL_auxz00_4472;
BgL_auxz00_4472 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(8167L), BGl_string2903z00zz__r5_macro_4_3_syntaxz00, BGl_string2653z00zz__r5_macro_4_3_syntaxz00, BgL_exz00_2535); 
FAILURE(BgL_auxz00_4472,BFALSE,BFALSE);} 
BGl_installzd2syntaxzd2expanderz00zz__r5_macro_4_3_syntaxz00(BgL_carzd2115zd2_1175, BgL_auxz00_4469); } 
BGl_installzd2expanderzd2zz__macroz00(BgL_carzd2115zd2_1175, BgL_exz00_2535); 
return BUNSPEC;} }  else 
{ /* R5rs/syntax.scm 251 */
return 
BGl_errorz00zz__errorz00(BGl_string2904z00zz__r5_macro_4_3_syntaxz00, BGl_string2905z00zz__r5_macro_4_3_syntaxz00, BgL_xz00_9);} }  else 
{ /* R5rs/syntax.scm 251 */
return 
BGl_errorz00zz__errorz00(BGl_string2904z00zz__r5_macro_4_3_syntaxz00, BGl_string2905z00zz__r5_macro_4_3_syntaxz00, BgL_xz00_9);} }  else 
{ /* R5rs/syntax.scm 251 */
return 
BGl_errorz00zz__errorz00(BGl_string2904z00zz__r5_macro_4_3_syntaxz00, BGl_string2905z00zz__r5_macro_4_3_syntaxz00, BgL_xz00_9);} }  else 
{ /* R5rs/syntax.scm 251 */
return 
BGl_errorz00zz__errorz00(BGl_string2904z00zz__r5_macro_4_3_syntaxz00, BGl_string2905z00zz__r5_macro_4_3_syntaxz00, BgL_xz00_9);} }  else 
{ /* R5rs/syntax.scm 251 */
return 
BGl_errorz00zz__errorz00(BGl_string2904z00zz__r5_macro_4_3_syntaxz00, BGl_string2905z00zz__r5_macro_4_3_syntaxz00, BgL_xz00_9);} }  else 
{ /* R5rs/syntax.scm 251 */
return 
BGl_errorz00zz__errorz00(BGl_string2904z00zz__r5_macro_4_3_syntaxz00, BGl_string2905z00zz__r5_macro_4_3_syntaxz00, BgL_xz00_9);} }  else 
{ /* R5rs/syntax.scm 251 */
return 
BGl_errorz00zz__errorz00(BGl_string2904z00zz__r5_macro_4_3_syntaxz00, BGl_string2905z00zz__r5_macro_4_3_syntaxz00, BgL_xz00_9);} }  else 
{ /* R5rs/syntax.scm 251 */
return 
BGl_errorz00zz__errorz00(BGl_string2904z00zz__r5_macro_4_3_syntaxz00, BGl_string2905z00zz__r5_macro_4_3_syntaxz00, BgL_xz00_9);} } 

}



/* &expand-define-syntax */
obj_t BGl_z62expandzd2definezd2syntaxz62zz__r5_macro_4_3_syntaxz00(obj_t BgL_envz00_3114, obj_t BgL_xz00_3115, obj_t BgL_ez00_3116)
{
{ /* R5rs/syntax.scm 250 */
{ /* R5rs/syntax.scm 251 */
 obj_t BgL_auxz00_4486;
if(
PROCEDUREP(BgL_ez00_3116))
{ /* R5rs/syntax.scm 251 */
BgL_auxz00_4486 = BgL_ez00_3116
; }  else 
{ 
 obj_t BgL_auxz00_4489;
BgL_auxz00_4489 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(7966L), BGl_string2906z00zz__r5_macro_4_3_syntaxz00, BGl_string2653z00zz__r5_macro_4_3_syntaxz00, BgL_ez00_3116); 
FAILURE(BgL_auxz00_4489,BFALSE,BFALSE);} 
return 
BGl_expandzd2definezd2syntaxz00zz__r5_macro_4_3_syntaxz00(BgL_xz00_3115, BgL_auxz00_4486);} } 

}



/* expand-letrec-syntax */
BGL_EXPORTED_DEF obj_t BGl_expandzd2letreczd2syntaxz00zz__r5_macro_4_3_syntaxz00(obj_t BgL_xz00_11, obj_t BgL_ez00_12)
{
{ /* R5rs/syntax.scm 263 */
{ 
 obj_t BgL_bsz00_1192; obj_t BgL_bodyz00_1193;
if(
PAIRP(BgL_xz00_11))
{ /* R5rs/syntax.scm 264 */
 obj_t BgL_cdrzd2142zd2_1198;
BgL_cdrzd2142zd2_1198 = 
CDR(
((obj_t)BgL_xz00_11)); 
if(
PAIRP(BgL_cdrzd2142zd2_1198))
{ /* R5rs/syntax.scm 264 */
BgL_bsz00_1192 = 
CAR(BgL_cdrzd2142zd2_1198); 
BgL_bodyz00_1193 = 
CDR(BgL_cdrzd2142zd2_1198); 
{ /* R5rs/syntax.scm 266 */
 obj_t BgL_e1z00_1202;
BgL_e1z00_1202 = 
BGl_loopze72ze7zz__r5_macro_4_3_syntaxz00(BgL_ez00_12, BgL_bsz00_1192); 
{ /* R5rs/syntax.scm 280 */
 obj_t BgL_arg1315z00_1203;
{ /* R5rs/syntax.scm 280 */
 obj_t BgL_arg1316z00_1204;
if(
NULLP(BgL_bodyz00_1193))
{ /* R5rs/syntax.scm 280 */
BgL_arg1316z00_1204 = BNIL; }  else 
{ /* R5rs/syntax.scm 280 */
 obj_t BgL_head1095z00_1207;
BgL_head1095z00_1207 = 
MAKE_YOUNG_PAIR(BNIL, BNIL); 
{ 
 obj_t BgL_l1093z00_1209; obj_t BgL_tail1096z00_1210;
BgL_l1093z00_1209 = BgL_bodyz00_1193; 
BgL_tail1096z00_1210 = BgL_head1095z00_1207; 
BgL_zc3z04anonymousza31318ze3z87_1211:
if(
PAIRP(BgL_l1093z00_1209))
{ /* R5rs/syntax.scm 280 */
 obj_t BgL_newtail1097z00_1213;
{ /* R5rs/syntax.scm 280 */
 obj_t BgL_arg1321z00_1215;
{ /* R5rs/syntax.scm 280 */
 obj_t BgL_xz00_1216;
BgL_xz00_1216 = 
CAR(BgL_l1093z00_1209); 
{ /* R5rs/syntax.scm 280 */
 obj_t BgL_arg1322z00_1217;
BgL_arg1322z00_1217 = 
BGl_hygieniza7eza7zz__r5_macro_4_3_syntaxz00(BgL_xz00_1216, BNIL); 
{ /* R5rs/syntax.scm 280 */
 obj_t BgL_funz00_3230;
if(
PROCEDUREP(BgL_e1z00_1202))
{ /* R5rs/syntax.scm 280 */
BgL_funz00_3230 = BgL_e1z00_1202; }  else 
{ 
 obj_t BgL_auxz00_4510;
BgL_auxz00_4510 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(9042L), BGl_string2907z00zz__r5_macro_4_3_syntaxz00, BGl_string2653z00zz__r5_macro_4_3_syntaxz00, BgL_e1z00_1202); 
FAILURE(BgL_auxz00_4510,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_3230, 2))
{ /* R5rs/syntax.scm 280 */
BgL_arg1321z00_1215 = 
(VA_PROCEDUREP( BgL_funz00_3230 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_3230))(BgL_e1z00_1202, BgL_arg1322z00_1217, BgL_e1z00_1202, BEOA) : ((obj_t (*)(obj_t, obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_3230))(BgL_e1z00_1202, BgL_arg1322z00_1217, BgL_e1z00_1202) ); }  else 
{ /* R5rs/syntax.scm 280 */
FAILURE(BGl_string2908z00zz__r5_macro_4_3_syntaxz00,BGl_list2909z00zz__r5_macro_4_3_syntaxz00,BgL_funz00_3230);} } } } 
BgL_newtail1097z00_1213 = 
MAKE_YOUNG_PAIR(BgL_arg1321z00_1215, BNIL); } 
SET_CDR(BgL_tail1096z00_1210, BgL_newtail1097z00_1213); 
{ 
 obj_t BgL_tail1096z00_4526; obj_t BgL_l1093z00_4524;
BgL_l1093z00_4524 = 
CDR(BgL_l1093z00_1209); 
BgL_tail1096z00_4526 = BgL_newtail1097z00_1213; 
BgL_tail1096z00_1210 = BgL_tail1096z00_4526; 
BgL_l1093z00_1209 = BgL_l1093z00_4524; 
goto BgL_zc3z04anonymousza31318ze3z87_1211;} }  else 
{ /* R5rs/syntax.scm 280 */
if(
NULLP(BgL_l1093z00_1209))
{ /* R5rs/syntax.scm 280 */
BgL_arg1316z00_1204 = 
CDR(BgL_head1095z00_1207); }  else 
{ /* R5rs/syntax.scm 280 */
BgL_arg1316z00_1204 = 
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BGl_string2912z00zz__r5_macro_4_3_syntaxz00, BGl_string2913z00zz__r5_macro_4_3_syntaxz00, BgL_l1093z00_1209, BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(9025L)); } } } } 
{ /* R5rs/syntax.scm 279 */
 obj_t BgL_auxz00_4532;
{ /* R5rs/syntax.scm 279 */
 bool_t BgL_test3203z00_4533;
if(
PAIRP(BgL_arg1316z00_1204))
{ /* R5rs/syntax.scm 279 */
BgL_test3203z00_4533 = ((bool_t)1)
; }  else 
{ /* R5rs/syntax.scm 279 */
BgL_test3203z00_4533 = 
NULLP(BgL_arg1316z00_1204)
; } 
if(BgL_test3203z00_4533)
{ /* R5rs/syntax.scm 279 */
BgL_auxz00_4532 = BgL_arg1316z00_1204
; }  else 
{ 
 obj_t BgL_auxz00_4537;
BgL_auxz00_4537 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(9009L), BGl_string2914z00zz__r5_macro_4_3_syntaxz00, BGl_string2650z00zz__r5_macro_4_3_syntaxz00, BgL_arg1316z00_1204); 
FAILURE(BgL_auxz00_4537,BFALSE,BFALSE);} } 
BgL_arg1315z00_1203 = 
BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4532, BNIL); } } 
return 
MAKE_YOUNG_PAIR(BGl_symbol2675z00zz__r5_macro_4_3_syntaxz00, BgL_arg1315z00_1203);} } }  else 
{ /* R5rs/syntax.scm 264 */
return 
BGl_errorz00zz__errorz00(BGl_string2915z00zz__r5_macro_4_3_syntaxz00, BGl_string2905z00zz__r5_macro_4_3_syntaxz00, BgL_xz00_11);} }  else 
{ /* R5rs/syntax.scm 264 */
return 
BGl_errorz00zz__errorz00(BGl_string2915z00zz__r5_macro_4_3_syntaxz00, BGl_string2905z00zz__r5_macro_4_3_syntaxz00, BgL_xz00_11);} } } 

}



/* loop~2 */
obj_t BGl_loopze72ze7zz__r5_macro_4_3_syntaxz00(obj_t BgL_ez00_3184, obj_t BgL_bsz00_1221)
{
{ /* R5rs/syntax.scm 266 */
if(
NULLP(BgL_bsz00_1221))
{ /* R5rs/syntax.scm 267 */
return BgL_ez00_3184;}  else 
{ 
 obj_t BgL_mz00_1224; obj_t BgL_lsz00_1225; obj_t BgL_rulesz00_1226;
{ /* R5rs/syntax.scm 269 */
 obj_t BgL_ezd2151zd2_1229;
{ /* R5rs/syntax.scm 269 */
 obj_t BgL_pairz00_2538;
if(
PAIRP(BgL_bsz00_1221))
{ /* R5rs/syntax.scm 269 */
BgL_pairz00_2538 = BgL_bsz00_1221; }  else 
{ 
 obj_t BgL_auxz00_4551;
BgL_auxz00_4551 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(8669L), BGl_string2918z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_bsz00_1221); 
FAILURE(BgL_auxz00_4551,BFALSE,BFALSE);} 
BgL_ezd2151zd2_1229 = 
CAR(BgL_pairz00_2538); } 
if(
PAIRP(BgL_ezd2151zd2_1229))
{ /* R5rs/syntax.scm 269 */
 obj_t BgL_carzd2158zd2_1231; obj_t BgL_cdrzd2159zd2_1232;
BgL_carzd2158zd2_1231 = 
CAR(BgL_ezd2151zd2_1229); 
BgL_cdrzd2159zd2_1232 = 
CDR(BgL_ezd2151zd2_1229); 
if(
SYMBOLP(BgL_carzd2158zd2_1231))
{ /* R5rs/syntax.scm 269 */
if(
PAIRP(BgL_cdrzd2159zd2_1232))
{ /* R5rs/syntax.scm 269 */
 obj_t BgL_carzd2165zd2_1235;
BgL_carzd2165zd2_1235 = 
CAR(BgL_cdrzd2159zd2_1232); 
if(
PAIRP(BgL_carzd2165zd2_1235))
{ /* R5rs/syntax.scm 269 */
 obj_t BgL_cdrzd2170zd2_1237;
BgL_cdrzd2170zd2_1237 = 
CDR(BgL_carzd2165zd2_1235); 
if(
(
CAR(BgL_carzd2165zd2_1235)==BGl_symbol2901z00zz__r5_macro_4_3_syntaxz00))
{ /* R5rs/syntax.scm 269 */
if(
PAIRP(BgL_cdrzd2170zd2_1237))
{ /* R5rs/syntax.scm 269 */
if(
NULLP(
CDR(BgL_cdrzd2159zd2_1232)))
{ /* R5rs/syntax.scm 269 */
BgL_mz00_1224 = BgL_carzd2158zd2_1231; 
BgL_lsz00_1225 = 
CAR(BgL_cdrzd2170zd2_1237); 
BgL_rulesz00_1226 = 
CDR(BgL_cdrzd2170zd2_1237); 
{ /* R5rs/syntax.scm 271 */
 obj_t BgL_e3z00_1247; obj_t BgL_e4z00_1248;
BGl_initzd2syntaxzd2expandersz12z12zz__r5_macro_4_3_syntaxz00(); 
{ /* R5rs/syntax.scm 313 */
 obj_t BgL_auxz00_4586; obj_t BgL_auxz00_4577;
{ /* R5rs/syntax.scm 313 */
 bool_t BgL_test3216z00_4587;
if(
PAIRP(BgL_rulesz00_1226))
{ /* R5rs/syntax.scm 313 */
BgL_test3216z00_4587 = ((bool_t)1)
; }  else 
{ /* R5rs/syntax.scm 313 */
BgL_test3216z00_4587 = 
NULLP(BgL_rulesz00_1226)
; } 
if(BgL_test3216z00_4587)
{ /* R5rs/syntax.scm 313 */
BgL_auxz00_4586 = BgL_rulesz00_1226
; }  else 
{ 
 obj_t BgL_auxz00_4591;
BgL_auxz00_4591 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(10344L), BGl_string2916z00zz__r5_macro_4_3_syntaxz00, BGl_string2650z00zz__r5_macro_4_3_syntaxz00, BgL_rulesz00_1226); 
FAILURE(BgL_auxz00_4591,BFALSE,BFALSE);} } 
{ /* R5rs/syntax.scm 313 */
 bool_t BgL_test3214z00_4578;
if(
PAIRP(BgL_lsz00_1225))
{ /* R5rs/syntax.scm 313 */
BgL_test3214z00_4578 = ((bool_t)1)
; }  else 
{ /* R5rs/syntax.scm 313 */
BgL_test3214z00_4578 = 
NULLP(BgL_lsz00_1225)
; } 
if(BgL_test3214z00_4578)
{ /* R5rs/syntax.scm 313 */
BgL_auxz00_4577 = BgL_lsz00_1225
; }  else 
{ 
 obj_t BgL_auxz00_4582;
BgL_auxz00_4582 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(10335L), BGl_string2916z00zz__r5_macro_4_3_syntaxz00, BGl_string2650z00zz__r5_macro_4_3_syntaxz00, BgL_lsz00_1225); 
FAILURE(BgL_auxz00_4582,BFALSE,BFALSE);} } 
BgL_e3z00_1247 = 
BGl_syntaxzd2ruleszd2ze3expanderze3zz__r5_macro_4_3_syntaxz00(BgL_mz00_1224, BgL_auxz00_4577, BgL_auxz00_4586); } 
{ /* R5rs/syntax.scm 272 */
 obj_t BgL_arg1346z00_1257;
{ /* R5rs/syntax.scm 272 */
 obj_t BgL_pairz00_2536;
if(
PAIRP(BgL_bsz00_1221))
{ /* R5rs/syntax.scm 272 */
BgL_pairz00_2536 = BgL_bsz00_1221; }  else 
{ 
 obj_t BgL_auxz00_4598;
BgL_auxz00_4598 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(8811L), BGl_string2916z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_bsz00_1221); 
FAILURE(BgL_auxz00_4598,BFALSE,BFALSE);} 
BgL_arg1346z00_1257 = 
CDR(BgL_pairz00_2536); } 
BgL_e4z00_1248 = 
BGl_loopze72ze7zz__r5_macro_4_3_syntaxz00(BgL_ez00_3184, BgL_arg1346z00_1257); } 
{ /* R5rs/syntax.scm 273 */
 obj_t BgL_zc3z04anonymousza31340ze3z87_3117;
BgL_zc3z04anonymousza31340ze3z87_3117 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31340ze3ze5zz__r5_macro_4_3_syntaxz00, 
(int)(2L), 
(int)(3L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31340ze3z87_3117, 
(int)(0L), BgL_e4z00_1248); 
PROCEDURE_SET(BgL_zc3z04anonymousza31340ze3z87_3117, 
(int)(1L), BgL_e3z00_1247); 
PROCEDURE_SET(BgL_zc3z04anonymousza31340ze3z87_3117, 
(int)(2L), BgL_mz00_1224); 
return BgL_zc3z04anonymousza31340ze3z87_3117;} } }  else 
{ /* R5rs/syntax.scm 269 */
return 
BGl_errorz00zz__errorz00(BGl_string2919z00zz__r5_macro_4_3_syntaxz00, BGl_string2920z00zz__r5_macro_4_3_syntaxz00, BgL_bsz00_1221);} }  else 
{ /* R5rs/syntax.scm 269 */
return 
BGl_errorz00zz__errorz00(BGl_string2919z00zz__r5_macro_4_3_syntaxz00, BGl_string2920z00zz__r5_macro_4_3_syntaxz00, BgL_bsz00_1221);} }  else 
{ /* R5rs/syntax.scm 269 */
return 
BGl_errorz00zz__errorz00(BGl_string2919z00zz__r5_macro_4_3_syntaxz00, BGl_string2920z00zz__r5_macro_4_3_syntaxz00, BgL_bsz00_1221);} }  else 
{ /* R5rs/syntax.scm 269 */
return 
BGl_errorz00zz__errorz00(BGl_string2919z00zz__r5_macro_4_3_syntaxz00, BGl_string2920z00zz__r5_macro_4_3_syntaxz00, BgL_bsz00_1221);} }  else 
{ /* R5rs/syntax.scm 269 */
return 
BGl_errorz00zz__errorz00(BGl_string2919z00zz__r5_macro_4_3_syntaxz00, BGl_string2920z00zz__r5_macro_4_3_syntaxz00, BgL_bsz00_1221);} }  else 
{ /* R5rs/syntax.scm 269 */
return 
BGl_errorz00zz__errorz00(BGl_string2919z00zz__r5_macro_4_3_syntaxz00, BGl_string2920z00zz__r5_macro_4_3_syntaxz00, BgL_bsz00_1221);} }  else 
{ /* R5rs/syntax.scm 269 */
return 
BGl_errorz00zz__errorz00(BGl_string2919z00zz__r5_macro_4_3_syntaxz00, BGl_string2920z00zz__r5_macro_4_3_syntaxz00, BgL_bsz00_1221);} } } } 

}



/* &expand-letrec-syntax */
obj_t BGl_z62expandzd2letreczd2syntaxz62zz__r5_macro_4_3_syntaxz00(obj_t BgL_envz00_3118, obj_t BgL_xz00_3119, obj_t BgL_ez00_3120)
{
{ /* R5rs/syntax.scm 263 */
{ /* R5rs/syntax.scm 264 */
 obj_t BgL_auxz00_4622;
if(
PROCEDUREP(BgL_ez00_3120))
{ /* R5rs/syntax.scm 264 */
BgL_auxz00_4622 = BgL_ez00_3120
; }  else 
{ 
 obj_t BgL_auxz00_4625;
BgL_auxz00_4625 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(8548L), BGl_string2921z00zz__r5_macro_4_3_syntaxz00, BGl_string2653z00zz__r5_macro_4_3_syntaxz00, BgL_ez00_3120); 
FAILURE(BgL_auxz00_4625,BFALSE,BFALSE);} 
return 
BGl_expandzd2letreczd2syntaxz00zz__r5_macro_4_3_syntaxz00(BgL_xz00_3119, BgL_auxz00_4622);} } 

}



/* &<@anonymous:1340> */
obj_t BGl_z62zc3z04anonymousza31340ze3ze5zz__r5_macro_4_3_syntaxz00(obj_t BgL_envz00_3121, obj_t BgL_xz00_3125, obj_t BgL_e2z00_3126)
{
{ /* R5rs/syntax.scm 273 */
{ /* R5rs/syntax.scm 274 */
 obj_t BgL_e4z00_3122; obj_t BgL_e3z00_3123; obj_t BgL_mz00_3124;
BgL_e4z00_3122 = 
PROCEDURE_REF(BgL_envz00_3121, 
(int)(0L)); 
BgL_e3z00_3123 = 
PROCEDURE_REF(BgL_envz00_3121, 
(int)(1L)); 
BgL_mz00_3124 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3121, 
(int)(2L))); 
{ /* R5rs/syntax.scm 274 */
 bool_t BgL_test3220z00_4637;
if(
PAIRP(BgL_xz00_3125))
{ /* R5rs/syntax.scm 274 */
BgL_test3220z00_4637 = 
BGl_hygienezd2eqzf3z21zz__r5_macro_4_3_syntaxz00(BgL_mz00_3124, 
CAR(BgL_xz00_3125))
; }  else 
{ /* R5rs/syntax.scm 274 */
BgL_test3220z00_4637 = ((bool_t)0)
; } 
if(BgL_test3220z00_4637)
{ /* R5rs/syntax.scm 275 */
 obj_t BgL_funz00_3640;
if(
PROCEDUREP(BgL_e3z00_3123))
{ /* R5rs/syntax.scm 275 */
BgL_funz00_3640 = BgL_e3z00_3123; }  else 
{ 
 obj_t BgL_auxz00_4644;
BgL_auxz00_4644 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(8903L), BGl_string2922z00zz__r5_macro_4_3_syntaxz00, BGl_string2653z00zz__r5_macro_4_3_syntaxz00, BgL_e3z00_3123); 
FAILURE(BgL_auxz00_4644,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_3640, 2))
{ /* R5rs/syntax.scm 275 */
return 
(VA_PROCEDUREP( BgL_funz00_3640 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_3640))(BgL_e3z00_3123, BgL_xz00_3125, BgL_e2z00_3126, BEOA) : ((obj_t (*)(obj_t, obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_3640))(BgL_e3z00_3123, BgL_xz00_3125, BgL_e2z00_3126) );}  else 
{ /* R5rs/syntax.scm 275 */
FAILURE(BGl_string2923z00zz__r5_macro_4_3_syntaxz00,BGl_list2924z00zz__r5_macro_4_3_syntaxz00,BgL_funz00_3640);} }  else 
{ /* R5rs/syntax.scm 276 */
 obj_t BgL_funz00_3641;
if(
PROCEDUREP(BgL_e4z00_3122))
{ /* R5rs/syntax.scm 276 */
BgL_funz00_3641 = BgL_e4z00_3122; }  else 
{ 
 obj_t BgL_auxz00_4658;
BgL_auxz00_4658 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(8923L), BGl_string2922z00zz__r5_macro_4_3_syntaxz00, BGl_string2653z00zz__r5_macro_4_3_syntaxz00, BgL_e4z00_3122); 
FAILURE(BgL_auxz00_4658,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_3641, 2))
{ /* R5rs/syntax.scm 276 */
return 
(VA_PROCEDUREP( BgL_funz00_3641 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_3641))(BgL_e4z00_3122, BgL_xz00_3125, BgL_e2z00_3126, BEOA) : ((obj_t (*)(obj_t, obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_3641))(BgL_e4z00_3122, BgL_xz00_3125, BgL_e2z00_3126) );}  else 
{ /* R5rs/syntax.scm 276 */
FAILURE(BGl_string2923z00zz__r5_macro_4_3_syntaxz00,BGl_list2929z00zz__r5_macro_4_3_syntaxz00,BgL_funz00_3641);} } } } } 

}



/* expand-let-syntax */
BGL_EXPORTED_DEF obj_t BGl_expandzd2letzd2syntaxz00zz__r5_macro_4_3_syntaxz00(obj_t BgL_xz00_13, obj_t BgL_ez00_14)
{
{ /* R5rs/syntax.scm 287 */
{ 
 obj_t BgL_bsz00_1259; obj_t BgL_bodyz00_1260;
if(
PAIRP(BgL_xz00_13))
{ /* R5rs/syntax.scm 288 */
 obj_t BgL_cdrzd2185zd2_1265;
BgL_cdrzd2185zd2_1265 = 
CDR(
((obj_t)BgL_xz00_13)); 
if(
PAIRP(BgL_cdrzd2185zd2_1265))
{ /* R5rs/syntax.scm 288 */
BgL_bsz00_1259 = 
CAR(BgL_cdrzd2185zd2_1265); 
BgL_bodyz00_1260 = 
CDR(BgL_cdrzd2185zd2_1265); 
{ /* R5rs/syntax.scm 290 */
 obj_t BgL_e1z00_1269;
BgL_e1z00_1269 = 
BGl_loopze71ze7zz__r5_macro_4_3_syntaxz00(BgL_ez00_14, BgL_bsz00_1259); 
{ /* R5rs/syntax.scm 304 */
 obj_t BgL_arg1351z00_1270;
{ /* R5rs/syntax.scm 304 */
 obj_t BgL_arg1352z00_1271;
if(
NULLP(BgL_bodyz00_1260))
{ /* R5rs/syntax.scm 304 */
BgL_arg1352z00_1271 = BNIL; }  else 
{ /* R5rs/syntax.scm 304 */
 obj_t BgL_head1100z00_1274;
BgL_head1100z00_1274 = 
MAKE_YOUNG_PAIR(BNIL, BNIL); 
{ 
 obj_t BgL_l1098z00_1276; obj_t BgL_tail1101z00_1277;
BgL_l1098z00_1276 = BgL_bodyz00_1260; 
BgL_tail1101z00_1277 = BgL_head1100z00_1274; 
BgL_zc3z04anonymousza31354ze3z87_1278:
if(
PAIRP(BgL_l1098z00_1276))
{ /* R5rs/syntax.scm 304 */
 obj_t BgL_newtail1102z00_1280;
{ /* R5rs/syntax.scm 304 */
 obj_t BgL_arg1357z00_1282;
{ /* R5rs/syntax.scm 304 */
 obj_t BgL_xz00_1283;
BgL_xz00_1283 = 
CAR(BgL_l1098z00_1276); 
{ /* R5rs/syntax.scm 304 */
 obj_t BgL_arg1358z00_1284;
BgL_arg1358z00_1284 = 
BGl_hygieniza7eza7zz__r5_macro_4_3_syntaxz00(BgL_xz00_1283, BNIL); 
{ /* R5rs/syntax.scm 304 */
 obj_t BgL_funz00_3256;
if(
PROCEDUREP(BgL_e1z00_1269))
{ /* R5rs/syntax.scm 304 */
BgL_funz00_3256 = BgL_e1z00_1269; }  else 
{ 
 obj_t BgL_auxz00_4686;
BgL_auxz00_4686 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(9892L), BGl_string2932z00zz__r5_macro_4_3_syntaxz00, BGl_string2653z00zz__r5_macro_4_3_syntaxz00, BgL_e1z00_1269); 
FAILURE(BgL_auxz00_4686,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_3256, 2))
{ /* R5rs/syntax.scm 304 */
BgL_arg1357z00_1282 = 
(VA_PROCEDUREP( BgL_funz00_3256 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_3256))(BgL_e1z00_1269, BgL_arg1358z00_1284, BgL_e1z00_1269, BEOA) : ((obj_t (*)(obj_t, obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_3256))(BgL_e1z00_1269, BgL_arg1358z00_1284, BgL_e1z00_1269) ); }  else 
{ /* R5rs/syntax.scm 304 */
FAILURE(BGl_string2933z00zz__r5_macro_4_3_syntaxz00,BGl_list2934z00zz__r5_macro_4_3_syntaxz00,BgL_funz00_3256);} } } } 
BgL_newtail1102z00_1280 = 
MAKE_YOUNG_PAIR(BgL_arg1357z00_1282, BNIL); } 
SET_CDR(BgL_tail1101z00_1277, BgL_newtail1102z00_1280); 
{ 
 obj_t BgL_tail1101z00_4702; obj_t BgL_l1098z00_4700;
BgL_l1098z00_4700 = 
CDR(BgL_l1098z00_1276); 
BgL_tail1101z00_4702 = BgL_newtail1102z00_1280; 
BgL_tail1101z00_1277 = BgL_tail1101z00_4702; 
BgL_l1098z00_1276 = BgL_l1098z00_4700; 
goto BgL_zc3z04anonymousza31354ze3z87_1278;} }  else 
{ /* R5rs/syntax.scm 304 */
if(
NULLP(BgL_l1098z00_1276))
{ /* R5rs/syntax.scm 304 */
BgL_arg1352z00_1271 = 
CDR(BgL_head1100z00_1274); }  else 
{ /* R5rs/syntax.scm 304 */
BgL_arg1352z00_1271 = 
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BGl_string2912z00zz__r5_macro_4_3_syntaxz00, BGl_string2913z00zz__r5_macro_4_3_syntaxz00, BgL_l1098z00_1276, BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(9875L)); } } } } 
{ /* R5rs/syntax.scm 303 */
 obj_t BgL_auxz00_4708;
{ /* R5rs/syntax.scm 303 */
 bool_t BgL_test3233z00_4709;
if(
PAIRP(BgL_arg1352z00_1271))
{ /* R5rs/syntax.scm 303 */
BgL_test3233z00_4709 = ((bool_t)1)
; }  else 
{ /* R5rs/syntax.scm 303 */
BgL_test3233z00_4709 = 
NULLP(BgL_arg1352z00_1271)
; } 
if(BgL_test3233z00_4709)
{ /* R5rs/syntax.scm 303 */
BgL_auxz00_4708 = BgL_arg1352z00_1271
; }  else 
{ 
 obj_t BgL_auxz00_4713;
BgL_auxz00_4713 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(9859L), BGl_string2937z00zz__r5_macro_4_3_syntaxz00, BGl_string2650z00zz__r5_macro_4_3_syntaxz00, BgL_arg1352z00_1271); 
FAILURE(BgL_auxz00_4713,BFALSE,BFALSE);} } 
BgL_arg1351z00_1270 = 
BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4708, BNIL); } } 
return 
MAKE_YOUNG_PAIR(BGl_symbol2675z00zz__r5_macro_4_3_syntaxz00, BgL_arg1351z00_1270);} } }  else 
{ /* R5rs/syntax.scm 288 */
return 
BGl_errorz00zz__errorz00(BGl_string2919z00zz__r5_macro_4_3_syntaxz00, BGl_string2905z00zz__r5_macro_4_3_syntaxz00, BgL_xz00_13);} }  else 
{ /* R5rs/syntax.scm 288 */
return 
BGl_errorz00zz__errorz00(BGl_string2919z00zz__r5_macro_4_3_syntaxz00, BGl_string2905z00zz__r5_macro_4_3_syntaxz00, BgL_xz00_13);} } } 

}



/* loop~1 */
obj_t BGl_loopze71ze7zz__r5_macro_4_3_syntaxz00(obj_t BgL_ez00_3183, obj_t BgL_bsz00_1288)
{
{ /* R5rs/syntax.scm 290 */
if(
NULLP(BgL_bsz00_1288))
{ /* R5rs/syntax.scm 291 */
return BgL_ez00_3183;}  else 
{ 
 obj_t BgL_mz00_1291; obj_t BgL_lsz00_1292; obj_t BgL_rulesz00_1293;
{ /* R5rs/syntax.scm 293 */
 obj_t BgL_ezd2194zd2_1296;
{ /* R5rs/syntax.scm 293 */
 obj_t BgL_pairz00_2556;
if(
PAIRP(BgL_bsz00_1288))
{ /* R5rs/syntax.scm 293 */
BgL_pairz00_2556 = BgL_bsz00_1288; }  else 
{ 
 obj_t BgL_auxz00_4727;
BgL_auxz00_4727 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(9520L), BGl_string2939z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_bsz00_1288); 
FAILURE(BgL_auxz00_4727,BFALSE,BFALSE);} 
BgL_ezd2194zd2_1296 = 
CAR(BgL_pairz00_2556); } 
if(
PAIRP(BgL_ezd2194zd2_1296))
{ /* R5rs/syntax.scm 293 */
 obj_t BgL_carzd2201zd2_1298; obj_t BgL_cdrzd2202zd2_1299;
BgL_carzd2201zd2_1298 = 
CAR(BgL_ezd2194zd2_1296); 
BgL_cdrzd2202zd2_1299 = 
CDR(BgL_ezd2194zd2_1296); 
if(
SYMBOLP(BgL_carzd2201zd2_1298))
{ /* R5rs/syntax.scm 293 */
if(
PAIRP(BgL_cdrzd2202zd2_1299))
{ /* R5rs/syntax.scm 293 */
 obj_t BgL_carzd2208zd2_1302;
BgL_carzd2208zd2_1302 = 
CAR(BgL_cdrzd2202zd2_1299); 
if(
PAIRP(BgL_carzd2208zd2_1302))
{ /* R5rs/syntax.scm 293 */
 obj_t BgL_cdrzd2213zd2_1304;
BgL_cdrzd2213zd2_1304 = 
CDR(BgL_carzd2208zd2_1302); 
if(
(
CAR(BgL_carzd2208zd2_1302)==BGl_symbol2901z00zz__r5_macro_4_3_syntaxz00))
{ /* R5rs/syntax.scm 293 */
if(
PAIRP(BgL_cdrzd2213zd2_1304))
{ /* R5rs/syntax.scm 293 */
if(
NULLP(
CDR(BgL_cdrzd2202zd2_1299)))
{ /* R5rs/syntax.scm 293 */
BgL_mz00_1291 = BgL_carzd2201zd2_1298; 
BgL_lsz00_1292 = 
CAR(BgL_cdrzd2213zd2_1304); 
BgL_rulesz00_1293 = 
CDR(BgL_cdrzd2213zd2_1304); 
{ /* R5rs/syntax.scm 295 */
 obj_t BgL_e3z00_1314; obj_t BgL_e4z00_1315;
BGl_initzd2syntaxzd2expandersz12z12zz__r5_macro_4_3_syntaxz00(); 
{ /* R5rs/syntax.scm 313 */
 obj_t BgL_auxz00_4762; obj_t BgL_auxz00_4753;
{ /* R5rs/syntax.scm 313 */
 bool_t BgL_test3246z00_4763;
if(
PAIRP(BgL_rulesz00_1293))
{ /* R5rs/syntax.scm 313 */
BgL_test3246z00_4763 = ((bool_t)1)
; }  else 
{ /* R5rs/syntax.scm 313 */
BgL_test3246z00_4763 = 
NULLP(BgL_rulesz00_1293)
; } 
if(BgL_test3246z00_4763)
{ /* R5rs/syntax.scm 313 */
BgL_auxz00_4762 = BgL_rulesz00_1293
; }  else 
{ 
 obj_t BgL_auxz00_4767;
BgL_auxz00_4767 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(10344L), BGl_string2938z00zz__r5_macro_4_3_syntaxz00, BGl_string2650z00zz__r5_macro_4_3_syntaxz00, BgL_rulesz00_1293); 
FAILURE(BgL_auxz00_4767,BFALSE,BFALSE);} } 
{ /* R5rs/syntax.scm 313 */
 bool_t BgL_test3244z00_4754;
if(
PAIRP(BgL_lsz00_1292))
{ /* R5rs/syntax.scm 313 */
BgL_test3244z00_4754 = ((bool_t)1)
; }  else 
{ /* R5rs/syntax.scm 313 */
BgL_test3244z00_4754 = 
NULLP(BgL_lsz00_1292)
; } 
if(BgL_test3244z00_4754)
{ /* R5rs/syntax.scm 313 */
BgL_auxz00_4753 = BgL_lsz00_1292
; }  else 
{ 
 obj_t BgL_auxz00_4758;
BgL_auxz00_4758 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(10335L), BGl_string2938z00zz__r5_macro_4_3_syntaxz00, BGl_string2650z00zz__r5_macro_4_3_syntaxz00, BgL_lsz00_1292); 
FAILURE(BgL_auxz00_4758,BFALSE,BFALSE);} } 
BgL_e3z00_1314 = 
BGl_syntaxzd2ruleszd2ze3expanderze3zz__r5_macro_4_3_syntaxz00(BgL_mz00_1291, BgL_auxz00_4753, BgL_auxz00_4762); } 
{ /* R5rs/syntax.scm 296 */
 obj_t BgL_arg1382z00_1324;
{ /* R5rs/syntax.scm 296 */
 obj_t BgL_pairz00_2554;
if(
PAIRP(BgL_bsz00_1288))
{ /* R5rs/syntax.scm 296 */
BgL_pairz00_2554 = BgL_bsz00_1288; }  else 
{ 
 obj_t BgL_auxz00_4774;
BgL_auxz00_4774 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(9662L), BGl_string2938z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_bsz00_1288); 
FAILURE(BgL_auxz00_4774,BFALSE,BFALSE);} 
BgL_arg1382z00_1324 = 
CDR(BgL_pairz00_2554); } 
BgL_e4z00_1315 = 
BGl_loopze71ze7zz__r5_macro_4_3_syntaxz00(BgL_ez00_3183, BgL_arg1382z00_1324); } 
{ /* R5rs/syntax.scm 297 */
 obj_t BgL_zc3z04anonymousza31376ze3z87_3127;
BgL_zc3z04anonymousza31376ze3z87_3127 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31376ze3ze5zz__r5_macro_4_3_syntaxz00, 
(int)(2L), 
(int)(4L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31376ze3z87_3127, 
(int)(0L), BgL_e4z00_1315); 
PROCEDURE_SET(BgL_zc3z04anonymousza31376ze3z87_3127, 
(int)(1L), BgL_e3z00_1314); 
PROCEDURE_SET(BgL_zc3z04anonymousza31376ze3z87_3127, 
(int)(2L), BgL_ez00_3183); 
PROCEDURE_SET(BgL_zc3z04anonymousza31376ze3z87_3127, 
(int)(3L), BgL_mz00_1291); 
return BgL_zc3z04anonymousza31376ze3z87_3127;} } }  else 
{ /* R5rs/syntax.scm 293 */
return 
BGl_errorz00zz__errorz00(BGl_string2919z00zz__r5_macro_4_3_syntaxz00, BGl_string2920z00zz__r5_macro_4_3_syntaxz00, BgL_bsz00_1288);} }  else 
{ /* R5rs/syntax.scm 293 */
return 
BGl_errorz00zz__errorz00(BGl_string2919z00zz__r5_macro_4_3_syntaxz00, BGl_string2920z00zz__r5_macro_4_3_syntaxz00, BgL_bsz00_1288);} }  else 
{ /* R5rs/syntax.scm 293 */
return 
BGl_errorz00zz__errorz00(BGl_string2919z00zz__r5_macro_4_3_syntaxz00, BGl_string2920z00zz__r5_macro_4_3_syntaxz00, BgL_bsz00_1288);} }  else 
{ /* R5rs/syntax.scm 293 */
return 
BGl_errorz00zz__errorz00(BGl_string2919z00zz__r5_macro_4_3_syntaxz00, BGl_string2920z00zz__r5_macro_4_3_syntaxz00, BgL_bsz00_1288);} }  else 
{ /* R5rs/syntax.scm 293 */
return 
BGl_errorz00zz__errorz00(BGl_string2919z00zz__r5_macro_4_3_syntaxz00, BGl_string2920z00zz__r5_macro_4_3_syntaxz00, BgL_bsz00_1288);} }  else 
{ /* R5rs/syntax.scm 293 */
return 
BGl_errorz00zz__errorz00(BGl_string2919z00zz__r5_macro_4_3_syntaxz00, BGl_string2920z00zz__r5_macro_4_3_syntaxz00, BgL_bsz00_1288);} }  else 
{ /* R5rs/syntax.scm 293 */
return 
BGl_errorz00zz__errorz00(BGl_string2919z00zz__r5_macro_4_3_syntaxz00, BGl_string2920z00zz__r5_macro_4_3_syntaxz00, BgL_bsz00_1288);} } } } 

}



/* &expand-let-syntax */
obj_t BGl_z62expandzd2letzd2syntaxz62zz__r5_macro_4_3_syntaxz00(obj_t BgL_envz00_3128, obj_t BgL_xz00_3129, obj_t BgL_ez00_3130)
{
{ /* R5rs/syntax.scm 287 */
{ /* R5rs/syntax.scm 288 */
 obj_t BgL_auxz00_4800;
if(
PROCEDUREP(BgL_ez00_3130))
{ /* R5rs/syntax.scm 288 */
BgL_auxz00_4800 = BgL_ez00_3130
; }  else 
{ 
 obj_t BgL_auxz00_4803;
BgL_auxz00_4803 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(9399L), BGl_string2940z00zz__r5_macro_4_3_syntaxz00, BGl_string2653z00zz__r5_macro_4_3_syntaxz00, BgL_ez00_3130); 
FAILURE(BgL_auxz00_4803,BFALSE,BFALSE);} 
return 
BGl_expandzd2letzd2syntaxz00zz__r5_macro_4_3_syntaxz00(BgL_xz00_3129, BgL_auxz00_4800);} } 

}



/* &<@anonymous:1376> */
obj_t BGl_z62zc3z04anonymousza31376ze3ze5zz__r5_macro_4_3_syntaxz00(obj_t BgL_envz00_3131, obj_t BgL_xz00_3136, obj_t BgL_e2z00_3137)
{
{ /* R5rs/syntax.scm 297 */
{ /* R5rs/syntax.scm 298 */
 obj_t BgL_e4z00_3132; obj_t BgL_e3z00_3133; obj_t BgL_ez00_3134; obj_t BgL_mz00_3135;
BgL_e4z00_3132 = 
PROCEDURE_REF(BgL_envz00_3131, 
(int)(0L)); 
BgL_e3z00_3133 = 
PROCEDURE_REF(BgL_envz00_3131, 
(int)(1L)); 
BgL_ez00_3134 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3131, 
(int)(2L))); 
BgL_mz00_3135 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3131, 
(int)(3L))); 
{ /* R5rs/syntax.scm 298 */
 bool_t BgL_test3250z00_4818;
if(
PAIRP(BgL_xz00_3136))
{ /* R5rs/syntax.scm 298 */
BgL_test3250z00_4818 = 
BGl_hygienezd2eqzf3z21zz__r5_macro_4_3_syntaxz00(BgL_mz00_3135, 
CAR(BgL_xz00_3136))
; }  else 
{ /* R5rs/syntax.scm 298 */
BgL_test3250z00_4818 = ((bool_t)0)
; } 
if(BgL_test3250z00_4818)
{ /* R5rs/syntax.scm 299 */
 obj_t BgL_funz00_3645;
if(
PROCEDUREP(BgL_e3z00_3133))
{ /* R5rs/syntax.scm 299 */
BgL_funz00_3645 = BgL_e3z00_3133; }  else 
{ 
 obj_t BgL_auxz00_4825;
BgL_auxz00_4825 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(9754L), BGl_string2941z00zz__r5_macro_4_3_syntaxz00, BGl_string2653z00zz__r5_macro_4_3_syntaxz00, BgL_e3z00_3133); 
FAILURE(BgL_auxz00_4825,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_3645, 2))
{ /* R5rs/syntax.scm 299 */
return 
(VA_PROCEDUREP( BgL_funz00_3645 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_3645))(BgL_e3z00_3133, BgL_xz00_3136, BgL_ez00_3134, BEOA) : ((obj_t (*)(obj_t, obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_3645))(BgL_e3z00_3133, BgL_xz00_3136, BgL_ez00_3134) );}  else 
{ /* R5rs/syntax.scm 299 */
FAILURE(BGl_string2942z00zz__r5_macro_4_3_syntaxz00,BGl_list2943z00zz__r5_macro_4_3_syntaxz00,BgL_funz00_3645);} }  else 
{ /* R5rs/syntax.scm 300 */
 obj_t BgL_funz00_3646;
if(
PROCEDUREP(BgL_e4z00_3132))
{ /* R5rs/syntax.scm 300 */
BgL_funz00_3646 = BgL_e4z00_3132; }  else 
{ 
 obj_t BgL_auxz00_4839;
BgL_auxz00_4839 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(9773L), BGl_string2941z00zz__r5_macro_4_3_syntaxz00, BGl_string2653z00zz__r5_macro_4_3_syntaxz00, BgL_e4z00_3132); 
FAILURE(BgL_auxz00_4839,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_3646, 2))
{ /* R5rs/syntax.scm 300 */
return 
(VA_PROCEDUREP( BgL_funz00_3646 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_3646))(BgL_e4z00_3132, BgL_xz00_3136, BgL_e2z00_3137, BEOA) : ((obj_t (*)(obj_t, obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_3646))(BgL_e4z00_3132, BgL_xz00_3136, BgL_e2z00_3137) );}  else 
{ /* R5rs/syntax.scm 300 */
FAILURE(BGl_string2942z00zz__r5_macro_4_3_syntaxz00,BGl_list2929z00zz__r5_macro_4_3_syntaxz00,BgL_funz00_3646);} } } } } 

}



/* syntax-rules->expander */
BGL_EXPORTED_DEF obj_t BGl_syntaxzd2ruleszd2ze3expanderze3zz__r5_macro_4_3_syntaxz00(obj_t BgL_keywordz00_18, obj_t BgL_literalsz00_19, obj_t BgL_rulesz00_20)
{
{ /* R5rs/syntax.scm 318 */
{ /* R5rs/syntax.scm 319 */
 obj_t BgL_kz00_1326;
BgL_kz00_1326 = 
MAKE_YOUNG_PAIR(BgL_keywordz00_18, BgL_literalsz00_19); 
if(
BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_rulesz00_20))
{ /* R5rs/syntax.scm 321 */
 obj_t BgL_zc3z04anonymousza31384ze3z87_3138;
BgL_zc3z04anonymousza31384ze3z87_3138 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31384ze3ze5zz__r5_macro_4_3_syntaxz00, 
(int)(2L), 
(int)(3L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31384ze3z87_3138, 
(int)(0L), BgL_keywordz00_18); 
PROCEDURE_SET(BgL_zc3z04anonymousza31384ze3z87_3138, 
(int)(1L), BgL_kz00_1326); 
PROCEDURE_SET(BgL_zc3z04anonymousza31384ze3z87_3138, 
(int)(2L), BgL_rulesz00_20); 
return BgL_zc3z04anonymousza31384ze3z87_3138;}  else 
{ /* R5rs/syntax.scm 320 */
return 
BGl_errorz00zz__errorz00(BgL_keywordz00_18, BGl_string2944z00zz__r5_macro_4_3_syntaxz00, BgL_rulesz00_20);} } } 

}



/* &syntax-rules->expander */
obj_t BGl_z62syntaxzd2ruleszd2ze3expanderz81zz__r5_macro_4_3_syntaxz00(obj_t BgL_envz00_3139, obj_t BgL_keywordz00_3140, obj_t BgL_literalsz00_3141, obj_t BgL_rulesz00_3142)
{
{ /* R5rs/syntax.scm 318 */
{ /* R5rs/syntax.scm 319 */
 obj_t BgL_auxz00_4878; obj_t BgL_auxz00_4871; obj_t BgL_auxz00_4864;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_rulesz00_3142))
{ /* R5rs/syntax.scm 319 */
BgL_auxz00_4878 = BgL_rulesz00_3142
; }  else 
{ 
 obj_t BgL_auxz00_4881;
BgL_auxz00_4881 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(10634L), BGl_string2945z00zz__r5_macro_4_3_syntaxz00, BGl_string2650z00zz__r5_macro_4_3_syntaxz00, BgL_rulesz00_3142); 
FAILURE(BgL_auxz00_4881,BFALSE,BFALSE);} 
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_literalsz00_3141))
{ /* R5rs/syntax.scm 319 */
BgL_auxz00_4871 = BgL_literalsz00_3141
; }  else 
{ 
 obj_t BgL_auxz00_4874;
BgL_auxz00_4874 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(10634L), BGl_string2945z00zz__r5_macro_4_3_syntaxz00, BGl_string2650z00zz__r5_macro_4_3_syntaxz00, BgL_literalsz00_3141); 
FAILURE(BgL_auxz00_4874,BFALSE,BFALSE);} 
if(
SYMBOLP(BgL_keywordz00_3140))
{ /* R5rs/syntax.scm 319 */
BgL_auxz00_4864 = BgL_keywordz00_3140
; }  else 
{ 
 obj_t BgL_auxz00_4867;
BgL_auxz00_4867 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(10634L), BGl_string2945z00zz__r5_macro_4_3_syntaxz00, BGl_string2652z00zz__r5_macro_4_3_syntaxz00, BgL_keywordz00_3140); 
FAILURE(BgL_auxz00_4867,BFALSE,BFALSE);} 
return 
BGl_syntaxzd2ruleszd2ze3expanderze3zz__r5_macro_4_3_syntaxz00(BgL_auxz00_4864, BgL_auxz00_4871, BgL_auxz00_4878);} } 

}



/* &<@anonymous:1384> */
obj_t BGl_z62zc3z04anonymousza31384ze3ze5zz__r5_macro_4_3_syntaxz00(obj_t BgL_envz00_3143, obj_t BgL_xz00_3147, obj_t BgL_ez00_3148)
{
{ /* R5rs/syntax.scm 321 */
{ /* R5rs/syntax.scm 323 */
 obj_t BgL_keywordz00_3144; obj_t BgL_kz00_3145; obj_t BgL_rulesz00_3146;
BgL_keywordz00_3144 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3143, 
(int)(0L))); 
BgL_kz00_3145 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3143, 
(int)(1L))); 
BgL_rulesz00_3146 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3143, 
(int)(2L))); 
{ 
 obj_t BgL_rulesz00_3648;
BgL_rulesz00_3648 = BgL_rulesz00_3146; 
BgL_loopz00_3647:
if(
NULLP(BgL_rulesz00_3648))
{ /* R5rs/syntax.scm 323 */
return 
BGl_errorz00zz__errorz00(BgL_keywordz00_3144, BGl_string2946z00zz__r5_macro_4_3_syntaxz00, BgL_xz00_3147);}  else 
{ 
 obj_t BgL_patternz00_3651; obj_t BgL_templatez00_3652;
{ /* R5rs/syntax.scm 325 */
 obj_t BgL_ezd2222zd2_3662;
{ /* R5rs/syntax.scm 325 */
 obj_t BgL_pairz00_3663;
if(
PAIRP(BgL_rulesz00_3648))
{ /* R5rs/syntax.scm 325 */
BgL_pairz00_3663 = BgL_rulesz00_3648; }  else 
{ 
 obj_t BgL_auxz00_4900;
BgL_auxz00_4900 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(10830L), BGl_string2869z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_rulesz00_3648); 
FAILURE(BgL_auxz00_4900,BFALSE,BFALSE);} 
BgL_ezd2222zd2_3662 = 
CAR(BgL_pairz00_3663); } 
if(
PAIRP(BgL_ezd2222zd2_3662))
{ /* R5rs/syntax.scm 325 */
 obj_t BgL_cdrzd2228zd2_3664;
BgL_cdrzd2228zd2_3664 = 
CDR(BgL_ezd2222zd2_3662); 
if(
PAIRP(BgL_cdrzd2228zd2_3664))
{ /* R5rs/syntax.scm 325 */
if(
NULLP(
CDR(BgL_cdrzd2228zd2_3664)))
{ /* R5rs/syntax.scm 325 */
BgL_patternz00_3651 = 
CAR(BgL_ezd2222zd2_3662); 
BgL_templatez00_3652 = 
CAR(BgL_cdrzd2228zd2_3664); 
if(
CBOOL(
BGl_syntaxzd2matcheszd2patternzf3zf3zz__r5_macro_4_3_syntaxz00(BgL_keywordz00_3144, BgL_patternz00_3651, BgL_xz00_3147, BgL_kz00_3145)))
{ /* R5rs/syntax.scm 329 */
 obj_t BgL_fsz00_3653;
BgL_fsz00_3653 = 
BGl_syntaxzd2getzd2framesz00zz__r5_macro_4_3_syntaxz00(BgL_patternz00_3651, BgL_xz00_3147, BgL_kz00_3145); 
{ /* R5rs/syntax.scm 329 */
 obj_t BgL_tz00_3654;
BgL_tz00_3654 = 
BGl_syntaxzd2expandzd2patternz00zz__r5_macro_4_3_syntaxz00(BgL_templatez00_3652, BgL_fsz00_3653, BgL_kz00_3145); 
{ /* R5rs/syntax.scm 330 */
 obj_t BgL_tez00_3655;
BgL_tez00_3655 = 
BGl_syntaxzd2expanderzd2zz__r5_macro_4_3_syntaxz00(BgL_tz00_3654, BGl_syntaxzd2expanderzd2envz00zz__r5_macro_4_3_syntaxz00); 
{ /* R5rs/syntax.scm 331 */

{ /* R5rs/syntax.scm 332 */
 obj_t BgL_arg1395z00_3656;
BgL_arg1395z00_3656 = 
BGl_hygieniza7eza7zz__r5_macro_4_3_syntaxz00(BgL_tez00_3655, BNIL); 
{ /* R5rs/syntax.scm 332 */
 obj_t BgL_funz00_3657;
if(
PROCEDUREP(BgL_ez00_3148))
{ /* R5rs/syntax.scm 332 */
BgL_funz00_3657 = BgL_ez00_3148; }  else 
{ 
 obj_t BgL_auxz00_4922;
BgL_auxz00_4922 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(11081L), BGl_string2947z00zz__r5_macro_4_3_syntaxz00, BGl_string2653z00zz__r5_macro_4_3_syntaxz00, BgL_ez00_3148); 
FAILURE(BgL_auxz00_4922,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_3657, 2))
{ /* R5rs/syntax.scm 332 */
return 
(VA_PROCEDUREP( BgL_funz00_3657 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_3657))(BgL_ez00_3148, BgL_arg1395z00_3656, BgL_ez00_3148, BEOA) : ((obj_t (*)(obj_t, obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_3657))(BgL_ez00_3148, BgL_arg1395z00_3656, BgL_ez00_3148) );}  else 
{ /* R5rs/syntax.scm 332 */
FAILURE(BGl_string2948z00zz__r5_macro_4_3_syntaxz00,BGl_list2949z00zz__r5_macro_4_3_syntaxz00,BgL_funz00_3657);} } } } } } }  else 
{ /* R5rs/syntax.scm 333 */
 obj_t BgL_arg1396z00_3658;
{ /* R5rs/syntax.scm 333 */
 obj_t BgL_pairz00_3659;
if(
PAIRP(BgL_rulesz00_3648))
{ /* R5rs/syntax.scm 333 */
BgL_pairz00_3659 = BgL_rulesz00_3648; }  else 
{ 
 obj_t BgL_auxz00_4936;
BgL_auxz00_4936 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(11126L), BGl_string2947z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_rulesz00_3648); 
FAILURE(BgL_auxz00_4936,BFALSE,BFALSE);} 
BgL_arg1396z00_3658 = 
CDR(BgL_pairz00_3659); } 
{ 
 obj_t BgL_rulesz00_4941;
BgL_rulesz00_4941 = BgL_arg1396z00_3658; 
BgL_rulesz00_3648 = BgL_rulesz00_4941; 
goto BgL_loopz00_3647;} } }  else 
{ /* R5rs/syntax.scm 325 */
BgL_tagzd2221zd2_3650:
{ /* R5rs/syntax.scm 335 */
 obj_t BgL_arg1397z00_3660;
{ /* R5rs/syntax.scm 335 */
 obj_t BgL_pairz00_3661;
if(
PAIRP(BgL_rulesz00_3648))
{ /* R5rs/syntax.scm 335 */
BgL_pairz00_3661 = BgL_rulesz00_3648; }  else 
{ 
 obj_t BgL_auxz00_4946;
BgL_auxz00_4946 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(11191L), BGl_string2952z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_rulesz00_3648); 
FAILURE(BgL_auxz00_4946,BFALSE,BFALSE);} 
BgL_arg1397z00_3660 = 
CAR(BgL_pairz00_3661); } 
return 
BGl_errorz00zz__errorz00(BgL_keywordz00_3144, BGl_string2953z00zz__r5_macro_4_3_syntaxz00, BgL_arg1397z00_3660);} } }  else 
{ /* R5rs/syntax.scm 325 */
goto BgL_tagzd2221zd2_3650;} }  else 
{ /* R5rs/syntax.scm 325 */
goto BgL_tagzd2221zd2_3650;} } } } } } 

}



/* syntax-matches-pattern? */
obj_t BGl_syntaxzd2matcheszd2patternzf3zf3zz__r5_macro_4_3_syntaxz00(obj_t BgL_keywordz00_21, obj_t BgL_pz00_22, obj_t BgL_ez00_23, obj_t BgL_kz00_24)
{
{ /* R5rs/syntax.scm 341 */
BGl_syntaxzd2matcheszd2patternzf3zf3zz__r5_macro_4_3_syntaxz00:
if(
BGl_ellipsiszf3zf3zz__r5_macro_4_3_syntaxz00(BgL_pz00_22))
{ /* R5rs/syntax.scm 344 */
 bool_t BgL_test3271z00_4954;
{ /* R5rs/syntax.scm 344 */
 long BgL_tmpz00_4955;
{ /* R5rs/syntax.scm 344 */
 obj_t BgL_auxz00_4956;
{ /* R5rs/syntax.scm 344 */
 bool_t BgL_test3272z00_4957;
if(
PAIRP(BgL_pz00_22))
{ /* R5rs/syntax.scm 344 */
BgL_test3272z00_4957 = ((bool_t)1)
; }  else 
{ /* R5rs/syntax.scm 344 */
BgL_test3272z00_4957 = 
NULLP(BgL_pz00_22)
; } 
if(BgL_test3272z00_4957)
{ /* R5rs/syntax.scm 344 */
BgL_auxz00_4956 = BgL_pz00_22
; }  else 
{ 
 obj_t BgL_auxz00_4961;
BgL_auxz00_4961 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(11584L), BGl_string2954z00zz__r5_macro_4_3_syntaxz00, BGl_string2650z00zz__r5_macro_4_3_syntaxz00, BgL_pz00_22); 
FAILURE(BgL_auxz00_4961,BFALSE,BFALSE);} } 
BgL_tmpz00_4955 = 
bgl_list_length(BgL_auxz00_4956); } 
BgL_test3271z00_4954 = 
(BgL_tmpz00_4955==2L); } 
if(BgL_test3271z00_4954)
{ /* R5rs/syntax.scm 344 */
if(
BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_ez00_23))
{ /* R5rs/syntax.scm 347 */
 obj_t BgL_p0z00_1360;
{ /* R5rs/syntax.scm 347 */
 obj_t BgL_pairz00_2580;
if(
PAIRP(BgL_pz00_22))
{ /* R5rs/syntax.scm 347 */
BgL_pairz00_2580 = BgL_pz00_22; }  else 
{ 
 obj_t BgL_auxz00_4971;
BgL_auxz00_4971 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(11668L), BGl_string2954z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_pz00_22); 
FAILURE(BgL_auxz00_4971,BFALSE,BFALSE);} 
BgL_p0z00_1360 = 
CAR(BgL_pairz00_2580); } 
{ 
 obj_t BgL_l1103z00_1362;
BgL_l1103z00_1362 = BgL_ez00_23; 
BgL_zc3z04anonymousza31401ze3z87_1363:
if(
NULLP(BgL_l1103z00_1362))
{ /* R5rs/syntax.scm 348 */
return BTRUE;}  else 
{ /* R5rs/syntax.scm 348 */
if(
PAIRP(BgL_l1103z00_1362))
{ /* R5rs/syntax.scm 349 */
 obj_t BgL_nvz00_1366;
BgL_nvz00_1366 = 
BGl_syntaxzd2matcheszd2patternzf3zf3zz__r5_macro_4_3_syntaxz00(BgL_keywordz00_21, BgL_p0z00_1360, 
CAR(BgL_l1103z00_1362), BgL_kz00_24); 
if(
CBOOL(BgL_nvz00_1366))
{ 
 obj_t BgL_l1103z00_4984;
BgL_l1103z00_4984 = 
CDR(BgL_l1103z00_1362); 
BgL_l1103z00_1362 = BgL_l1103z00_4984; 
goto BgL_zc3z04anonymousza31401ze3z87_1363;}  else 
{ /* R5rs/syntax.scm 348 */
return BFALSE;} }  else 
{ /* R5rs/syntax.scm 348 */
return 
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BGl_string2955z00zz__r5_macro_4_3_syntaxz00, BGl_string2913z00zz__r5_macro_4_3_syntaxz00, BgL_l1103z00_1362, BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(11678L));} } } }  else 
{ /* R5rs/syntax.scm 346 */
return BFALSE;} }  else 
{ /* R5rs/syntax.scm 344 */
return 
BGl_errorz00zz__errorz00(BgL_keywordz00_21, BGl_string2956z00zz__r5_macro_4_3_syntaxz00, BgL_pz00_22);} }  else 
{ /* R5rs/syntax.scm 343 */
if(
PAIRP(BgL_pz00_22))
{ /* R5rs/syntax.scm 351 */
if(
PAIRP(BgL_ez00_23))
{ /* R5rs/syntax.scm 353 */
 obj_t BgL__andtest_1043z00_1374;
BgL__andtest_1043z00_1374 = 
BGl_syntaxzd2matcheszd2patternzf3zf3zz__r5_macro_4_3_syntaxz00(BgL_keywordz00_21, 
CAR(BgL_pz00_22), 
CAR(BgL_ez00_23), BgL_kz00_24); 
if(
CBOOL(BgL__andtest_1043z00_1374))
{ 
 obj_t BgL_ez00_5000; obj_t BgL_pz00_4998;
BgL_pz00_4998 = 
CDR(BgL_pz00_22); 
BgL_ez00_5000 = 
CDR(BgL_ez00_23); 
BgL_ez00_23 = BgL_ez00_5000; 
BgL_pz00_22 = BgL_pz00_4998; 
goto BGl_syntaxzd2matcheszd2patternzf3zf3zz__r5_macro_4_3_syntaxz00;}  else 
{ /* R5rs/syntax.scm 353 */
return BFALSE;} }  else 
{ /* R5rs/syntax.scm 352 */
return BFALSE;} }  else 
{ /* R5rs/syntax.scm 351 */
if(
SYMBOLP(BgL_pz00_22))
{ /* R5rs/syntax.scm 356 */
 bool_t BgL_test3283z00_5004;
{ /* R5rs/syntax.scm 356 */
 obj_t BgL_tmpz00_5005;
{ /* R5rs/syntax.scm 356 */
 obj_t BgL_auxz00_5006;
{ /* R5rs/syntax.scm 356 */
 bool_t BgL_test3284z00_5007;
if(
PAIRP(BgL_kz00_24))
{ /* R5rs/syntax.scm 356 */
BgL_test3284z00_5007 = ((bool_t)1)
; }  else 
{ /* R5rs/syntax.scm 356 */
BgL_test3284z00_5007 = 
NULLP(BgL_kz00_24)
; } 
if(BgL_test3284z00_5007)
{ /* R5rs/syntax.scm 356 */
BgL_auxz00_5006 = BgL_kz00_24
; }  else 
{ 
 obj_t BgL_auxz00_5011;
BgL_auxz00_5011 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(11955L), BGl_string2954z00zz__r5_macro_4_3_syntaxz00, BGl_string2650z00zz__r5_macro_4_3_syntaxz00, BgL_kz00_24); 
FAILURE(BgL_auxz00_5011,BFALSE,BFALSE);} } 
BgL_tmpz00_5005 = 
BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_pz00_22, BgL_auxz00_5006); } 
BgL_test3283z00_5004 = 
CBOOL(BgL_tmpz00_5005); } 
if(BgL_test3283z00_5004)
{ /* R5rs/syntax.scm 356 */
return 
BBOOL(
BGl_hygienezd2eqzf3z21zz__r5_macro_4_3_syntaxz00(BgL_ez00_23, BgL_pz00_22));}  else 
{ /* R5rs/syntax.scm 356 */
return BTRUE;} }  else 
{ /* R5rs/syntax.scm 355 */
return 
BBOOL(
BGl_equalzf3zf3zz__r4_equivalence_6_2z00(BgL_pz00_22, BgL_ez00_23));} } } } 

}



/* syntax-get-frames */
obj_t BGl_syntaxzd2getzd2framesz00zz__r5_macro_4_3_syntaxz00(obj_t BgL_pz00_25, obj_t BgL_ez00_26, obj_t BgL_kz00_27)
{
{ /* R5rs/syntax.scm 363 */
if(
BGl_ellipsiszf3zf3zz__r5_macro_4_3_syntaxz00(BgL_pz00_25))
{ /* R5rs/syntax.scm 366 */
 obj_t BgL_p0z00_1382;
{ /* R5rs/syntax.scm 366 */
 obj_t BgL_pairz00_2587;
if(
PAIRP(BgL_pz00_25))
{ /* R5rs/syntax.scm 366 */
BgL_pairz00_2587 = BgL_pz00_25; }  else 
{ 
 obj_t BgL_auxz00_5025;
BgL_auxz00_5025 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(12325L), BGl_string2957z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_pz00_25); 
FAILURE(BgL_auxz00_5025,BFALSE,BFALSE);} 
BgL_p0z00_1382 = 
CAR(BgL_pairz00_2587); } 
{ /* R5rs/syntax.scm 368 */
 obj_t BgL_arg1415z00_1383;
{ /* R5rs/syntax.scm 368 */
 obj_t BgL_arg1417z00_1385;
if(
NULLP(BgL_ez00_26))
{ /* R5rs/syntax.scm 368 */
BgL_arg1417z00_1385 = BNIL; }  else 
{ /* R5rs/syntax.scm 368 */
 obj_t BgL_head1108z00_1388;
BgL_head1108z00_1388 = 
MAKE_YOUNG_PAIR(BNIL, BNIL); 
{ 
 obj_t BgL_l1106z00_1390; obj_t BgL_tail1109z00_1391;
BgL_l1106z00_1390 = BgL_ez00_26; 
BgL_tail1109z00_1391 = BgL_head1108z00_1388; 
BgL_zc3z04anonymousza31419ze3z87_1392:
if(
PAIRP(BgL_l1106z00_1390))
{ /* R5rs/syntax.scm 368 */
 obj_t BgL_newtail1110z00_1394;
BgL_newtail1110z00_1394 = 
MAKE_YOUNG_PAIR(
BGl_syntaxzd2getzd2framesz00zz__r5_macro_4_3_syntaxz00(BgL_p0z00_1382, 
CAR(BgL_l1106z00_1390), BgL_kz00_27), BNIL); 
SET_CDR(BgL_tail1109z00_1391, BgL_newtail1110z00_1394); 
{ 
 obj_t BgL_tail1109z00_5041; obj_t BgL_l1106z00_5039;
BgL_l1106z00_5039 = 
CDR(BgL_l1106z00_1390); 
BgL_tail1109z00_5041 = BgL_newtail1110z00_1394; 
BgL_tail1109z00_1391 = BgL_tail1109z00_5041; 
BgL_l1106z00_1390 = BgL_l1106z00_5039; 
goto BgL_zc3z04anonymousza31419ze3z87_1392;} }  else 
{ /* R5rs/syntax.scm 368 */
if(
NULLP(BgL_l1106z00_1390))
{ /* R5rs/syntax.scm 368 */
BgL_arg1417z00_1385 = 
CDR(BgL_head1108z00_1388); }  else 
{ /* R5rs/syntax.scm 368 */
BgL_arg1417z00_1385 = 
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BGl_string2912z00zz__r5_macro_4_3_syntaxz00, BGl_string2913z00zz__r5_macro_4_3_syntaxz00, BgL_l1106z00_1390, BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(12360L)); } } } } 
BgL_arg1415z00_1383 = 
MAKE_YOUNG_PAIR(BGl_keyword2958z00zz__r5_macro_4_3_syntaxz00, BgL_arg1417z00_1385); } 
{ /* R5rs/syntax.scm 367 */
 obj_t BgL_list1416z00_1384;
BgL_list1416z00_1384 = 
MAKE_YOUNG_PAIR(BgL_arg1415z00_1383, BNIL); 
return BgL_list1416z00_1384;} } }  else 
{ /* R5rs/syntax.scm 365 */
if(
PAIRP(BgL_pz00_25))
{ /* R5rs/syntax.scm 372 */
 obj_t BgL_arg1425z00_1401; obj_t BgL_arg1426z00_1402;
{ /* R5rs/syntax.scm 372 */
 obj_t BgL_arg1427z00_1403; obj_t BgL_arg1428z00_1404;
BgL_arg1427z00_1403 = 
CAR(BgL_pz00_25); 
{ /* R5rs/syntax.scm 372 */
 obj_t BgL_pairz00_2594;
if(
PAIRP(BgL_ez00_26))
{ /* R5rs/syntax.scm 372 */
BgL_pairz00_2594 = BgL_ez00_26; }  else 
{ 
 obj_t BgL_auxz00_5054;
BgL_auxz00_5054 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(12487L), BGl_string2957z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_ez00_26); 
FAILURE(BgL_auxz00_5054,BFALSE,BFALSE);} 
BgL_arg1428z00_1404 = 
CAR(BgL_pairz00_2594); } 
BgL_arg1425z00_1401 = 
BGl_syntaxzd2getzd2framesz00zz__r5_macro_4_3_syntaxz00(BgL_arg1427z00_1403, BgL_arg1428z00_1404, BgL_kz00_27); } 
{ /* R5rs/syntax.scm 373 */
 obj_t BgL_arg1429z00_1405; obj_t BgL_arg1430z00_1406;
BgL_arg1429z00_1405 = 
CDR(BgL_pz00_25); 
{ /* R5rs/syntax.scm 373 */
 obj_t BgL_pairz00_2596;
if(
PAIRP(BgL_ez00_26))
{ /* R5rs/syntax.scm 373 */
BgL_pairz00_2596 = BgL_ez00_26; }  else 
{ 
 obj_t BgL_auxz00_5063;
BgL_auxz00_5063 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(12533L), BGl_string2957z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_ez00_26); 
FAILURE(BgL_auxz00_5063,BFALSE,BFALSE);} 
BgL_arg1430z00_1406 = 
CDR(BgL_pairz00_2596); } 
BgL_arg1426z00_1402 = 
BGl_syntaxzd2getzd2framesz00zz__r5_macro_4_3_syntaxz00(BgL_arg1429z00_1405, BgL_arg1430z00_1406, BgL_kz00_27); } 
{ /* R5rs/syntax.scm 372 */
 obj_t BgL_auxz00_5069;
{ /* R5rs/syntax.scm 372 */
 bool_t BgL_test3294z00_5070;
if(
PAIRP(BgL_arg1425z00_1401))
{ /* R5rs/syntax.scm 372 */
BgL_test3294z00_5070 = ((bool_t)1)
; }  else 
{ /* R5rs/syntax.scm 372 */
BgL_test3294z00_5070 = 
NULLP(BgL_arg1425z00_1401)
; } 
if(BgL_test3294z00_5070)
{ /* R5rs/syntax.scm 372 */
BgL_auxz00_5069 = BgL_arg1425z00_1401
; }  else 
{ 
 obj_t BgL_auxz00_5074;
BgL_auxz00_5074 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(12447L), BGl_string2957z00zz__r5_macro_4_3_syntaxz00, BGl_string2650z00zz__r5_macro_4_3_syntaxz00, BgL_arg1425z00_1401); 
FAILURE(BgL_auxz00_5074,BFALSE,BFALSE);} } 
return 
BGl_appendzd221011zd2zz__r5_macro_4_3_syntaxz00(BgL_auxz00_5069, BgL_arg1426z00_1402);} }  else 
{ /* R5rs/syntax.scm 371 */
if(
SYMBOLP(BgL_pz00_25))
{ /* R5rs/syntax.scm 375 */
 bool_t BgL_test3297z00_5081;
{ /* R5rs/syntax.scm 375 */
 obj_t BgL_tmpz00_5082;
{ /* R5rs/syntax.scm 375 */
 obj_t BgL_auxz00_5083;
{ /* R5rs/syntax.scm 375 */
 bool_t BgL_test3298z00_5084;
if(
PAIRP(BgL_kz00_27))
{ /* R5rs/syntax.scm 375 */
BgL_test3298z00_5084 = ((bool_t)1)
; }  else 
{ /* R5rs/syntax.scm 375 */
BgL_test3298z00_5084 = 
NULLP(BgL_kz00_27)
; } 
if(BgL_test3298z00_5084)
{ /* R5rs/syntax.scm 375 */
BgL_auxz00_5083 = BgL_kz00_27
; }  else 
{ 
 obj_t BgL_auxz00_5088;
BgL_auxz00_5088 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(12579L), BGl_string2957z00zz__r5_macro_4_3_syntaxz00, BGl_string2650z00zz__r5_macro_4_3_syntaxz00, BgL_kz00_27); 
FAILURE(BgL_auxz00_5088,BFALSE,BFALSE);} } 
BgL_tmpz00_5082 = 
BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_pz00_25, BgL_auxz00_5083); } 
BgL_test3297z00_5081 = 
CBOOL(BgL_tmpz00_5082); } 
if(BgL_test3297z00_5081)
{ /* R5rs/syntax.scm 375 */
return BNIL;}  else 
{ /* R5rs/syntax.scm 375 */
 obj_t BgL_arg1434z00_1409;
BgL_arg1434z00_1409 = 
MAKE_YOUNG_PAIR(BgL_pz00_25, 
BGl_unhygieniza7eza7zz__r5_macro_4_3_syntaxz00(BgL_ez00_26)); 
{ /* R5rs/syntax.scm 375 */
 obj_t BgL_list1435z00_1410;
BgL_list1435z00_1410 = 
MAKE_YOUNG_PAIR(BgL_arg1434z00_1409, BNIL); 
return BgL_list1435z00_1410;} } }  else 
{ /* R5rs/syntax.scm 374 */
return BNIL;} } } } 

}



/* syntax-expand-pattern */
obj_t BGl_syntaxzd2expandzd2patternz00zz__r5_macro_4_3_syntaxz00(obj_t BgL_pz00_28, obj_t BgL_envz00_29, obj_t BgL_kz00_30)
{
{ /* R5rs/syntax.scm 382 */
{ 
 obj_t BgL_p0z00_1427; obj_t BgL_envz00_1428; obj_t BgL_kz00_1429;
if(
BGl_ellipsiszf3zf3zz__r5_macro_4_3_syntaxz00(BgL_pz00_28))
{ /* R5rs/syntax.scm 395 */
 obj_t BgL_arg1438z00_1414; obj_t BgL_arg1439z00_1415;
{ /* R5rs/syntax.scm 395 */
 obj_t BgL_arg1440z00_1416;
{ /* R5rs/syntax.scm 395 */
 obj_t BgL_pairz00_2602;
if(
PAIRP(BgL_pz00_28))
{ /* R5rs/syntax.scm 395 */
BgL_pairz00_2602 = BgL_pz00_28; }  else 
{ 
 obj_t BgL_auxz00_5101;
BgL_auxz00_5101 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(13260L), BGl_string2961z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_pz00_28); 
FAILURE(BgL_auxz00_5101,BFALSE,BFALSE);} 
BgL_arg1440z00_1416 = 
CAR(BgL_pairz00_2602); } 
BgL_p0z00_1427 = BgL_arg1440z00_1416; 
BgL_envz00_1428 = BgL_envz00_29; 
BgL_kz00_1429 = BgL_kz00_30; 
{ /* R5rs/syntax.scm 385 */
 obj_t BgL_varsz00_1431;
BgL_varsz00_1431 = 
BGl_subze70ze7zz__r5_macro_4_3_syntaxz00(BgL_kz00_1429, BgL_p0z00_1427); 
{ /* R5rs/syntax.scm 385 */
 obj_t BgL_framesz00_1432;
BgL_framesz00_1432 = 
BGl_getzd2ellipsiszd2framesz00zz__r5_macro_4_3_syntaxz00(BgL_varsz00_1431, BgL_envz00_1428); 
{ /* R5rs/syntax.scm 386 */

if(
BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_framesz00_1432))
{ /* R5rs/syntax.scm 387 */
if(
NULLP(BgL_framesz00_1432))
{ /* R5rs/syntax.scm 389 */
BgL_arg1438z00_1414 = BNIL; }  else 
{ /* R5rs/syntax.scm 389 */
 obj_t BgL_head1113z00_1436;
BgL_head1113z00_1436 = 
MAKE_YOUNG_PAIR(BNIL, BNIL); 
{ 
 obj_t BgL_l1111z00_1438; obj_t BgL_tail1114z00_1439;
BgL_l1111z00_1438 = BgL_framesz00_1432; 
BgL_tail1114z00_1439 = BgL_head1113z00_1436; 
BgL_zc3z04anonymousza31453ze3z87_1440:
if(
PAIRP(BgL_l1111z00_1438))
{ /* R5rs/syntax.scm 389 */
 obj_t BgL_newtail1115z00_1442;
{ /* R5rs/syntax.scm 389 */
 obj_t BgL_arg1456z00_1444;
{ /* R5rs/syntax.scm 389 */
 obj_t BgL_fz00_1445;
BgL_fz00_1445 = 
CAR(BgL_l1111z00_1438); 
{ /* R5rs/syntax.scm 390 */
 obj_t BgL_arg1457z00_1446;
{ /* R5rs/syntax.scm 390 */
 obj_t BgL_auxz00_5116;
{ /* R5rs/syntax.scm 390 */
 bool_t BgL_test3305z00_5117;
if(
PAIRP(BgL_fz00_1445))
{ /* R5rs/syntax.scm 390 */
BgL_test3305z00_5117 = ((bool_t)1)
; }  else 
{ /* R5rs/syntax.scm 390 */
BgL_test3305z00_5117 = 
NULLP(BgL_fz00_1445)
; } 
if(BgL_test3305z00_5117)
{ /* R5rs/syntax.scm 390 */
BgL_auxz00_5116 = BgL_fz00_1445
; }  else 
{ 
 obj_t BgL_auxz00_5121;
BgL_auxz00_5121 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(13148L), BGl_string2960z00zz__r5_macro_4_3_syntaxz00, BGl_string2650z00zz__r5_macro_4_3_syntaxz00, BgL_fz00_1445); 
FAILURE(BgL_auxz00_5121,BFALSE,BFALSE);} } 
BgL_arg1457z00_1446 = 
BGl_appendzd221011zd2zz__r5_macro_4_3_syntaxz00(BgL_auxz00_5116, BgL_envz00_1428); } 
BgL_arg1456z00_1444 = 
BGl_syntaxzd2expandzd2patternz00zz__r5_macro_4_3_syntaxz00(BgL_p0z00_1427, BgL_arg1457z00_1446, BgL_kz00_1429); } } 
BgL_newtail1115z00_1442 = 
MAKE_YOUNG_PAIR(BgL_arg1456z00_1444, BNIL); } 
SET_CDR(BgL_tail1114z00_1439, BgL_newtail1115z00_1442); 
{ 
 obj_t BgL_tail1114z00_5131; obj_t BgL_l1111z00_5129;
BgL_l1111z00_5129 = 
CDR(BgL_l1111z00_1438); 
BgL_tail1114z00_5131 = BgL_newtail1115z00_1442; 
BgL_tail1114z00_1439 = BgL_tail1114z00_5131; 
BgL_l1111z00_1438 = BgL_l1111z00_5129; 
goto BgL_zc3z04anonymousza31453ze3z87_1440;} }  else 
{ /* R5rs/syntax.scm 389 */
if(
NULLP(BgL_l1111z00_1438))
{ /* R5rs/syntax.scm 389 */
BgL_arg1438z00_1414 = 
CDR(BgL_head1113z00_1436); }  else 
{ /* R5rs/syntax.scm 389 */
BgL_arg1438z00_1414 = 
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BGl_string2912z00zz__r5_macro_4_3_syntaxz00, BGl_string2913z00zz__r5_macro_4_3_syntaxz00, BgL_l1111z00_1438, BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(13098L)); } } } } }  else 
{ /* R5rs/syntax.scm 387 */
BgL_arg1438z00_1414 = BNIL; } } } } } 
{ /* R5rs/syntax.scm 396 */
 obj_t BgL_arg1441z00_1417;
{ /* R5rs/syntax.scm 396 */
 obj_t BgL_pairz00_2603;
if(
PAIRP(BgL_pz00_28))
{ /* R5rs/syntax.scm 396 */
BgL_pairz00_2603 = BgL_pz00_28; }  else 
{ 
 obj_t BgL_auxz00_5139;
BgL_auxz00_5139 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(13307L), BGl_string2961z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_pz00_28); 
FAILURE(BgL_auxz00_5139,BFALSE,BFALSE);} 
{ /* R5rs/syntax.scm 396 */
 obj_t BgL_pairz00_2606;
{ /* R5rs/syntax.scm 396 */
 obj_t BgL_aux2392z00_3318;
BgL_aux2392z00_3318 = 
CDR(BgL_pairz00_2603); 
if(
PAIRP(BgL_aux2392z00_3318))
{ /* R5rs/syntax.scm 396 */
BgL_pairz00_2606 = BgL_aux2392z00_3318; }  else 
{ 
 obj_t BgL_auxz00_5146;
BgL_auxz00_5146 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(13301L), BGl_string2961z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_aux2392z00_3318); 
FAILURE(BgL_auxz00_5146,BFALSE,BFALSE);} } 
BgL_arg1441z00_1417 = 
CDR(BgL_pairz00_2606); } } 
BgL_arg1439z00_1415 = 
BGl_syntaxzd2expandzd2patternz00zz__r5_macro_4_3_syntaxz00(BgL_arg1441z00_1417, BgL_envz00_29, BgL_kz00_30); } 
{ /* R5rs/syntax.scm 395 */
 obj_t BgL_auxz00_5152;
{ /* R5rs/syntax.scm 395 */
 bool_t BgL_test3310z00_5153;
if(
PAIRP(BgL_arg1438z00_1414))
{ /* R5rs/syntax.scm 395 */
BgL_test3310z00_5153 = ((bool_t)1)
; }  else 
{ /* R5rs/syntax.scm 395 */
BgL_test3310z00_5153 = 
NULLP(BgL_arg1438z00_1414)
; } 
if(BgL_test3310z00_5153)
{ /* R5rs/syntax.scm 395 */
BgL_auxz00_5152 = BgL_arg1438z00_1414
; }  else 
{ 
 obj_t BgL_auxz00_5157;
BgL_auxz00_5157 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(13223L), BGl_string2961z00zz__r5_macro_4_3_syntaxz00, BGl_string2650z00zz__r5_macro_4_3_syntaxz00, BgL_arg1438z00_1414); 
FAILURE(BgL_auxz00_5157,BFALSE,BFALSE);} } 
return 
BGl_appendzd221011zd2zz__r5_macro_4_3_syntaxz00(BgL_auxz00_5152, BgL_arg1439z00_1415);} }  else 
{ /* R5rs/syntax.scm 394 */
if(
PAIRP(BgL_pz00_28))
{ /* R5rs/syntax.scm 397 */
return 
MAKE_YOUNG_PAIR(
BGl_syntaxzd2expandzd2patternz00zz__r5_macro_4_3_syntaxz00(
CAR(BgL_pz00_28), BgL_envz00_29, BgL_kz00_30), 
BGl_syntaxzd2expandzd2patternz00zz__r5_macro_4_3_syntaxz00(
CDR(BgL_pz00_28), BgL_envz00_29, BgL_kz00_30));}  else 
{ /* R5rs/syntax.scm 397 */
if(
SYMBOLP(BgL_pz00_28))
{ /* R5rs/syntax.scm 401 */
 bool_t BgL_test3314z00_5171;
{ /* R5rs/syntax.scm 401 */
 obj_t BgL_tmpz00_5172;
{ /* R5rs/syntax.scm 401 */
 obj_t BgL_auxz00_5173;
{ /* R5rs/syntax.scm 401 */
 bool_t BgL_test3315z00_5174;
if(
PAIRP(BgL_kz00_30))
{ /* R5rs/syntax.scm 401 */
BgL_test3315z00_5174 = ((bool_t)1)
; }  else 
{ /* R5rs/syntax.scm 401 */
BgL_test3315z00_5174 = 
NULLP(BgL_kz00_30)
; } 
if(BgL_test3315z00_5174)
{ /* R5rs/syntax.scm 401 */
BgL_auxz00_5173 = BgL_kz00_30
; }  else 
{ 
 obj_t BgL_auxz00_5178;
BgL_auxz00_5178 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(13471L), BGl_string2961z00zz__r5_macro_4_3_syntaxz00, BGl_string2650z00zz__r5_macro_4_3_syntaxz00, BgL_kz00_30); 
FAILURE(BgL_auxz00_5178,BFALSE,BFALSE);} } 
BgL_tmpz00_5172 = 
BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_pz00_28, BgL_auxz00_5173); } 
BgL_test3314z00_5171 = 
CBOOL(BgL_tmpz00_5172); } 
if(BgL_test3314z00_5171)
{ /* R5rs/syntax.scm 401 */
return BgL_pz00_28;}  else 
{ /* R5rs/syntax.scm 403 */
 obj_t BgL_xz00_1425;
{ /* R5rs/syntax.scm 403 */
 obj_t BgL_auxz00_5184;
{ /* R5rs/syntax.scm 403 */
 bool_t BgL_test3317z00_5185;
if(
PAIRP(BgL_envz00_29))
{ /* R5rs/syntax.scm 403 */
BgL_test3317z00_5185 = ((bool_t)1)
; }  else 
{ /* R5rs/syntax.scm 403 */
BgL_test3317z00_5185 = 
NULLP(BgL_envz00_29)
; } 
if(BgL_test3317z00_5185)
{ /* R5rs/syntax.scm 403 */
BgL_auxz00_5184 = BgL_envz00_29
; }  else 
{ 
 obj_t BgL_auxz00_5189;
BgL_auxz00_5189 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(13501L), BGl_string2961z00zz__r5_macro_4_3_syntaxz00, BGl_string2650z00zz__r5_macro_4_3_syntaxz00, BgL_envz00_29); 
FAILURE(BgL_auxz00_5189,BFALSE,BFALSE);} } 
BgL_xz00_1425 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_pz00_28, BgL_auxz00_5184); } 
if(
PAIRP(BgL_xz00_1425))
{ /* R5rs/syntax.scm 404 */
return 
CDR(BgL_xz00_1425);}  else 
{ /* R5rs/syntax.scm 404 */
return BgL_pz00_28;} } }  else 
{ /* R5rs/syntax.scm 400 */
return BgL_pz00_28;} } } } } 

}



/* get-ellipsis-frames */
obj_t BGl_getzd2ellipsiszd2framesz00zz__r5_macro_4_3_syntaxz00(obj_t BgL_varsz00_31, obj_t BgL_framesz00_32)
{
{ /* R5rs/syntax.scm 413 */
{ 
 obj_t BgL_varsz00_1452; obj_t BgL_resz00_1453;
BgL_varsz00_1452 = BgL_varsz00_31; 
BgL_resz00_1453 = BNIL; 
BgL_zc3z04anonymousza31459ze3z87_1454:
if(
NULLP(BgL_varsz00_1452))
{ /* R5rs/syntax.scm 416 */
return BgL_resz00_1453;}  else 
{ /* R5rs/syntax.scm 418 */
 obj_t BgL_vz00_1456;
{ /* R5rs/syntax.scm 418 */
 obj_t BgL_pairz00_2610;
if(
PAIRP(BgL_varsz00_1452))
{ /* R5rs/syntax.scm 418 */
BgL_pairz00_2610 = BgL_varsz00_1452; }  else 
{ 
 obj_t BgL_auxz00_5201;
BgL_auxz00_5201 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(13931L), BGl_string2869z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_varsz00_1452); 
FAILURE(BgL_auxz00_5201,BFALSE,BFALSE);} 
BgL_vz00_1456 = 
CAR(BgL_pairz00_2610); } 
{ /* R5rs/syntax.scm 419 */
 obj_t BgL_fz00_1457;
{ 
 obj_t BgL_l1122z00_1499;
BgL_l1122z00_1499 = BgL_framesz00_32; 
BgL_zc3z04anonymousza31485ze3z87_1500:
if(
NULLP(BgL_l1122z00_1499))
{ /* R5rs/syntax.scm 419 */
BgL_fz00_1457 = BFALSE; }  else 
{ /* R5rs/syntax.scm 419 */
if(
PAIRP(BgL_l1122z00_1499))
{ /* R5rs/syntax.scm 420 */
 obj_t BgL__ortest_1124z00_1503;
{ /* R5rs/syntax.scm 420 */
 obj_t BgL_fz00_1505;
BgL_fz00_1505 = 
CAR(BgL_l1122z00_1499); 
{ /* R5rs/syntax.scm 420 */
 bool_t BgL_test3324z00_5211;
{ /* R5rs/syntax.scm 420 */
 obj_t BgL_tmpz00_5212;
{ /* R5rs/syntax.scm 420 */
 obj_t BgL_pairz00_2612;
if(
PAIRP(BgL_fz00_1505))
{ /* R5rs/syntax.scm 420 */
BgL_pairz00_2612 = BgL_fz00_1505; }  else 
{ 
 obj_t BgL_auxz00_5215;
BgL_auxz00_5215 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(13996L), BGl_string2962z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_fz00_1505); 
FAILURE(BgL_auxz00_5215,BFALSE,BFALSE);} 
BgL_tmpz00_5212 = 
CAR(BgL_pairz00_2612); } 
BgL_test3324z00_5211 = 
(BgL_tmpz00_5212==BGl_keyword2958z00zz__r5_macro_4_3_syntaxz00); } 
if(BgL_test3324z00_5211)
{ /* R5rs/syntax.scm 421 */
 obj_t BgL_ez00_1508;
{ /* R5rs/syntax.scm 421 */
 obj_t BgL_hook1120z00_1510;
BgL_hook1120z00_1510 = 
MAKE_YOUNG_PAIR(BFALSE, BNIL); 
{ /* R5rs/syntax.scm 421 */
 obj_t BgL_g1121z00_1511;
{ /* R5rs/syntax.scm 421 */
 obj_t BgL_pairz00_2613;
if(
PAIRP(BgL_fz00_1505))
{ /* R5rs/syntax.scm 421 */
BgL_pairz00_2613 = BgL_fz00_1505; }  else 
{ 
 obj_t BgL_auxz00_5224;
BgL_auxz00_5224 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(14061L), BGl_string2962z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_fz00_1505); 
FAILURE(BgL_auxz00_5224,BFALSE,BFALSE);} 
BgL_g1121z00_1511 = 
CDR(BgL_pairz00_2613); } 
{ 
 obj_t BgL_l1117z00_1513; obj_t BgL_h1118z00_1514;
BgL_l1117z00_1513 = BgL_g1121z00_1511; 
BgL_h1118z00_1514 = BgL_hook1120z00_1510; 
BgL_zc3z04anonymousza31492ze3z87_1515:
if(
NULLP(BgL_l1117z00_1513))
{ /* R5rs/syntax.scm 421 */
BgL_ez00_1508 = 
CDR(BgL_hook1120z00_1510); }  else 
{ /* R5rs/syntax.scm 421 */
 bool_t BgL_test3328z00_5232;
{ /* R5rs/syntax.scm 421 */
 obj_t BgL_tmpz00_5233;
{ /* R5rs/syntax.scm 421 */
 obj_t BgL_auxz00_5234;
{ /* R5rs/syntax.scm 421 */
 obj_t BgL_pairz00_2615;
if(
PAIRP(BgL_l1117z00_1513))
{ /* R5rs/syntax.scm 421 */
BgL_pairz00_2615 = BgL_l1117z00_1513; }  else 
{ 
 obj_t BgL_auxz00_5237;
BgL_auxz00_5237 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(14044L), BGl_string2963z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_l1117z00_1513); 
FAILURE(BgL_auxz00_5237,BFALSE,BFALSE);} 
{ /* R5rs/syntax.scm 421 */
 obj_t BgL_aux2408z00_3334;
BgL_aux2408z00_3334 = 
CAR(BgL_pairz00_2615); 
{ /* R5rs/syntax.scm 421 */
 bool_t BgL_test3330z00_5242;
if(
PAIRP(BgL_aux2408z00_3334))
{ /* R5rs/syntax.scm 421 */
BgL_test3330z00_5242 = ((bool_t)1)
; }  else 
{ /* R5rs/syntax.scm 421 */
BgL_test3330z00_5242 = 
NULLP(BgL_aux2408z00_3334)
; } 
if(BgL_test3330z00_5242)
{ /* R5rs/syntax.scm 421 */
BgL_auxz00_5234 = BgL_aux2408z00_3334
; }  else 
{ 
 obj_t BgL_auxz00_5246;
BgL_auxz00_5246 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(14044L), BGl_string2963z00zz__r5_macro_4_3_syntaxz00, BGl_string2650z00zz__r5_macro_4_3_syntaxz00, BgL_aux2408z00_3334); 
FAILURE(BgL_auxz00_5246,BFALSE,BFALSE);} } } } 
BgL_tmpz00_5233 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_vz00_1456, BgL_auxz00_5234); } 
BgL_test3328z00_5232 = 
CBOOL(BgL_tmpz00_5233); } 
if(BgL_test3328z00_5232)
{ /* R5rs/syntax.scm 421 */
 obj_t BgL_nh1119z00_1519;
{ /* R5rs/syntax.scm 421 */
 obj_t BgL_arg1497z00_1521;
{ /* R5rs/syntax.scm 421 */
 obj_t BgL_pairz00_2616;
if(
PAIRP(BgL_l1117z00_1513))
{ /* R5rs/syntax.scm 421 */
BgL_pairz00_2616 = BgL_l1117z00_1513; }  else 
{ 
 obj_t BgL_auxz00_5254;
BgL_auxz00_5254 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(14056L), BGl_string2963z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_l1117z00_1513); 
FAILURE(BgL_auxz00_5254,BFALSE,BFALSE);} 
BgL_arg1497z00_1521 = 
CAR(BgL_pairz00_2616); } 
BgL_nh1119z00_1519 = 
MAKE_YOUNG_PAIR(BgL_arg1497z00_1521, BNIL); } 
SET_CDR(BgL_h1118z00_1514, BgL_nh1119z00_1519); 
{ /* R5rs/syntax.scm 421 */
 obj_t BgL_arg1495z00_1520;
{ /* R5rs/syntax.scm 421 */
 obj_t BgL_pairz00_2618;
if(
PAIRP(BgL_l1117z00_1513))
{ /* R5rs/syntax.scm 421 */
BgL_pairz00_2618 = BgL_l1117z00_1513; }  else 
{ 
 obj_t BgL_auxz00_5263;
BgL_auxz00_5263 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(14056L), BGl_string2963z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_l1117z00_1513); 
FAILURE(BgL_auxz00_5263,BFALSE,BFALSE);} 
BgL_arg1495z00_1520 = 
CDR(BgL_pairz00_2618); } 
{ 
 obj_t BgL_h1118z00_5269; obj_t BgL_l1117z00_5268;
BgL_l1117z00_5268 = BgL_arg1495z00_1520; 
BgL_h1118z00_5269 = BgL_nh1119z00_1519; 
BgL_h1118z00_1514 = BgL_h1118z00_5269; 
BgL_l1117z00_1513 = BgL_l1117z00_5268; 
goto BgL_zc3z04anonymousza31492ze3z87_1515;} } }  else 
{ /* R5rs/syntax.scm 421 */
 obj_t BgL_arg1498z00_1522;
{ /* R5rs/syntax.scm 421 */
 obj_t BgL_pairz00_2619;
if(
PAIRP(BgL_l1117z00_1513))
{ /* R5rs/syntax.scm 421 */
BgL_pairz00_2619 = BgL_l1117z00_1513; }  else 
{ 
 obj_t BgL_auxz00_5272;
BgL_auxz00_5272 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(14056L), BGl_string2963z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_l1117z00_1513); 
FAILURE(BgL_auxz00_5272,BFALSE,BFALSE);} 
BgL_arg1498z00_1522 = 
CDR(BgL_pairz00_2619); } 
{ 
 obj_t BgL_l1117z00_5277;
BgL_l1117z00_5277 = BgL_arg1498z00_1522; 
BgL_l1117z00_1513 = BgL_l1117z00_5277; 
goto BgL_zc3z04anonymousza31492ze3z87_1515;} } } } } } 
if(
PAIRP(BgL_ez00_1508))
{ /* R5rs/syntax.scm 422 */
BgL__ortest_1124z00_1503 = BgL_ez00_1508; }  else 
{ /* R5rs/syntax.scm 422 */
BgL__ortest_1124z00_1503 = BFALSE; } }  else 
{ /* R5rs/syntax.scm 420 */
BgL__ortest_1124z00_1503 = BFALSE; } } } 
if(
CBOOL(BgL__ortest_1124z00_1503))
{ /* R5rs/syntax.scm 419 */
BgL_fz00_1457 = BgL__ortest_1124z00_1503; }  else 
{ 
 obj_t BgL_l1122z00_5282;
BgL_l1122z00_5282 = 
CDR(BgL_l1122z00_1499); 
BgL_l1122z00_1499 = BgL_l1122z00_5282; 
goto BgL_zc3z04anonymousza31485ze3z87_1500;} }  else 
{ /* R5rs/syntax.scm 419 */
BgL_fz00_1457 = 
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BGl_string2964z00zz__r5_macro_4_3_syntaxz00, BGl_string2913z00zz__r5_macro_4_3_syntaxz00, BgL_l1122z00_1499, BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(13954L)); } } } 
if(
CBOOL(BgL_fz00_1457))
{ /* R5rs/syntax.scm 425 */
 obj_t BgL_g1045z00_1458;
{ /* R5rs/syntax.scm 425 */
 obj_t BgL_pairz00_2621;
if(
PAIRP(BgL_varsz00_1452))
{ /* R5rs/syntax.scm 425 */
BgL_pairz00_2621 = BgL_varsz00_1452; }  else 
{ 
 obj_t BgL_auxz00_5290;
BgL_auxz00_5290 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(14150L), BGl_string2869z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_varsz00_1452); 
FAILURE(BgL_auxz00_5290,BFALSE,BFALSE);} 
BgL_g1045z00_1458 = 
CDR(BgL_pairz00_2621); } 
{ 
 obj_t BgL_ovarsz00_1461; obj_t BgL_nvarsz00_1462;
BgL_ovarsz00_1461 = BgL_g1045z00_1458; 
BgL_nvarsz00_1462 = BNIL; 
BgL_zc3z04anonymousza31461ze3z87_1463:
if(
NULLP(BgL_ovarsz00_1461))
{ /* R5rs/syntax.scm 428 */
if(
NULLP(BgL_resz00_1453))
{ 
 obj_t BgL_resz00_5300; obj_t BgL_varsz00_5299;
BgL_varsz00_5299 = BgL_nvarsz00_1462; 
BgL_resz00_5300 = BgL_fz00_1457; 
BgL_resz00_1453 = BgL_resz00_5300; 
BgL_varsz00_1452 = BgL_varsz00_5299; 
goto BgL_zc3z04anonymousza31459ze3z87_1454;}  else 
{ /* R5rs/syntax.scm 431 */
 obj_t BgL_arg1464z00_1466;
{ /* R5rs/syntax.scm 431 */
 obj_t BgL_list1465z00_1467;
{ /* R5rs/syntax.scm 431 */
 obj_t BgL_arg1466z00_1468;
BgL_arg1466z00_1468 = 
MAKE_YOUNG_PAIR(BgL_resz00_1453, BNIL); 
BgL_list1465z00_1467 = 
MAKE_YOUNG_PAIR(BgL_fz00_1457, BgL_arg1466z00_1468); } 
BgL_arg1464z00_1466 = 
BGl_mapz00zz__r4_control_features_6_9z00(BGl_appendzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_list1465z00_1467); } 
{ 
 obj_t BgL_resz00_5305; obj_t BgL_varsz00_5304;
BgL_varsz00_5304 = BgL_nvarsz00_1462; 
BgL_resz00_5305 = BgL_arg1464z00_1466; 
BgL_resz00_1453 = BgL_resz00_5305; 
BgL_varsz00_1452 = BgL_varsz00_5304; 
goto BgL_zc3z04anonymousza31459ze3z87_1454;} } }  else 
{ /* R5rs/syntax.scm 432 */
 bool_t BgL_test3341z00_5306;
{ 
 obj_t BgL_l1125z00_1486;
{ /* R5rs/syntax.scm 432 */
 obj_t BgL_tmpz00_5307;
BgL_l1125z00_1486 = BgL_fz00_1457; 
BgL_zc3z04anonymousza31480ze3z87_1487:
if(
NULLP(BgL_l1125z00_1486))
{ /* R5rs/syntax.scm 432 */
BgL_tmpz00_5307 = BFALSE
; }  else 
{ /* R5rs/syntax.scm 432 */
if(
PAIRP(BgL_l1125z00_1486))
{ /* R5rs/syntax.scm 432 */
 bool_t BgL__ortest_1127z00_1490;
{ /* R5rs/syntax.scm 432 */
 obj_t BgL_tmpz00_5312;
{ /* R5rs/syntax.scm 432 */
 obj_t BgL_auxz00_5321; obj_t BgL_auxz00_5313;
{ /* R5rs/syntax.scm 432 */
 obj_t BgL_aux2420z00_3346;
BgL_aux2420z00_3346 = 
CAR(BgL_l1125z00_1486); 
{ /* R5rs/syntax.scm 432 */
 bool_t BgL_test3345z00_5323;
if(
PAIRP(BgL_aux2420z00_3346))
{ /* R5rs/syntax.scm 432 */
BgL_test3345z00_5323 = ((bool_t)1)
; }  else 
{ /* R5rs/syntax.scm 432 */
BgL_test3345z00_5323 = 
NULLP(BgL_aux2420z00_3346)
; } 
if(BgL_test3345z00_5323)
{ /* R5rs/syntax.scm 432 */
BgL_auxz00_5321 = BgL_aux2420z00_3346
; }  else 
{ 
 obj_t BgL_auxz00_5327;
BgL_auxz00_5327 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(14329L), BGl_string2965z00zz__r5_macro_4_3_syntaxz00, BGl_string2650z00zz__r5_macro_4_3_syntaxz00, BgL_aux2420z00_3346); 
FAILURE(BgL_auxz00_5327,BFALSE,BFALSE);} } } 
{ /* R5rs/syntax.scm 432 */
 obj_t BgL_pairz00_2623;
if(
PAIRP(BgL_ovarsz00_1461))
{ /* R5rs/syntax.scm 432 */
BgL_pairz00_2623 = BgL_ovarsz00_1461; }  else 
{ 
 obj_t BgL_auxz00_5316;
BgL_auxz00_5316 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(14347L), BGl_string2965z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_ovarsz00_1461); 
FAILURE(BgL_auxz00_5316,BFALSE,BFALSE);} 
BgL_auxz00_5313 = 
CAR(BgL_pairz00_2623); } 
BgL_tmpz00_5312 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_5313, BgL_auxz00_5321); } 
BgL__ortest_1127z00_1490 = 
PAIRP(BgL_tmpz00_5312); } 
if(BgL__ortest_1127z00_1490)
{ /* R5rs/syntax.scm 432 */
BgL_tmpz00_5307 = 
BBOOL(BgL__ortest_1127z00_1490)
; }  else 
{ 
 obj_t BgL_l1125z00_5335;
BgL_l1125z00_5335 = 
CDR(BgL_l1125z00_1486); 
BgL_l1125z00_1486 = BgL_l1125z00_5335; 
goto BgL_zc3z04anonymousza31480ze3z87_1487;} }  else 
{ /* R5rs/syntax.scm 432 */
BgL_tmpz00_5307 = 
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BGl_string2964z00zz__r5_macro_4_3_syntaxz00, BGl_string2913z00zz__r5_macro_4_3_syntaxz00, BgL_l1125z00_1486, BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(14312L))
; } } 
BgL_test3341z00_5306 = 
CBOOL(BgL_tmpz00_5307); } } 
if(BgL_test3341z00_5306)
{ /* R5rs/syntax.scm 433 */
 obj_t BgL_arg1476z00_1481;
{ /* R5rs/syntax.scm 433 */
 obj_t BgL_pairz00_2625;
if(
PAIRP(BgL_ovarsz00_1461))
{ /* R5rs/syntax.scm 433 */
BgL_pairz00_2625 = BgL_ovarsz00_1461; }  else 
{ 
 obj_t BgL_auxz00_5342;
BgL_auxz00_5342 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(14379L), BGl_string2966z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_ovarsz00_1461); 
FAILURE(BgL_auxz00_5342,BFALSE,BFALSE);} 
BgL_arg1476z00_1481 = 
CDR(BgL_pairz00_2625); } 
{ 
 obj_t BgL_ovarsz00_5347;
BgL_ovarsz00_5347 = BgL_arg1476z00_1481; 
BgL_ovarsz00_1461 = BgL_ovarsz00_5347; 
goto BgL_zc3z04anonymousza31461ze3z87_1463;} }  else 
{ /* R5rs/syntax.scm 435 */
 obj_t BgL_arg1477z00_1482; obj_t BgL_arg1478z00_1483;
{ /* R5rs/syntax.scm 435 */
 obj_t BgL_pairz00_2626;
if(
PAIRP(BgL_ovarsz00_1461))
{ /* R5rs/syntax.scm 435 */
BgL_pairz00_2626 = BgL_ovarsz00_1461; }  else 
{ 
 obj_t BgL_auxz00_5350;
BgL_auxz00_5350 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(14422L), BGl_string2966z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_ovarsz00_1461); 
FAILURE(BgL_auxz00_5350,BFALSE,BFALSE);} 
BgL_arg1477z00_1482 = 
CDR(BgL_pairz00_2626); } 
{ /* R5rs/syntax.scm 435 */
 obj_t BgL_arg1479z00_1484;
{ /* R5rs/syntax.scm 435 */
 obj_t BgL_pairz00_2627;
if(
PAIRP(BgL_ovarsz00_1461))
{ /* R5rs/syntax.scm 435 */
BgL_pairz00_2627 = BgL_ovarsz00_1461; }  else 
{ 
 obj_t BgL_auxz00_5357;
BgL_auxz00_5357 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(14440L), BGl_string2966z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_ovarsz00_1461); 
FAILURE(BgL_auxz00_5357,BFALSE,BFALSE);} 
BgL_arg1479z00_1484 = 
CAR(BgL_pairz00_2627); } 
BgL_arg1478z00_1483 = 
MAKE_YOUNG_PAIR(BgL_arg1479z00_1484, BgL_nvarsz00_1462); } 
{ 
 obj_t BgL_nvarsz00_5364; obj_t BgL_ovarsz00_5363;
BgL_ovarsz00_5363 = BgL_arg1477z00_1482; 
BgL_nvarsz00_5364 = BgL_arg1478z00_1483; 
BgL_nvarsz00_1462 = BgL_nvarsz00_5364; 
BgL_ovarsz00_1461 = BgL_ovarsz00_5363; 
goto BgL_zc3z04anonymousza31461ze3z87_1463;} } } } }  else 
{ /* R5rs/syntax.scm 436 */
 obj_t BgL_arg1484z00_1497;
{ /* R5rs/syntax.scm 436 */
 obj_t BgL_pairz00_2628;
if(
PAIRP(BgL_varsz00_1452))
{ /* R5rs/syntax.scm 436 */
BgL_pairz00_2628 = BgL_varsz00_1452; }  else 
{ 
 obj_t BgL_auxz00_5367;
BgL_auxz00_5367 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(14475L), BGl_string2869z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_varsz00_1452); 
FAILURE(BgL_auxz00_5367,BFALSE,BFALSE);} 
BgL_arg1484z00_1497 = 
CDR(BgL_pairz00_2628); } 
{ 
 obj_t BgL_varsz00_5372;
BgL_varsz00_5372 = BgL_arg1484z00_1497; 
BgL_varsz00_1452 = BgL_varsz00_5372; 
goto BgL_zc3z04anonymousza31459ze3z87_1454;} } } } } } 

}



/* sub~0 */
obj_t BGl_subze70ze7zz__r5_macro_4_3_syntaxz00(obj_t BgL_kz00_3182, obj_t BgL_pz00_1529)
{
{ /* R5rs/syntax.scm 442 */
if(
BGl_ellipsiszf3zf3zz__r5_macro_4_3_syntaxz00(BgL_pz00_1529))
{ /* R5rs/syntax.scm 445 */
 obj_t BgL_arg1502z00_1532; obj_t BgL_arg1503z00_1533;
{ /* R5rs/syntax.scm 445 */
 obj_t BgL_arg1504z00_1534;
{ /* R5rs/syntax.scm 445 */
 obj_t BgL_pairz00_2629;
if(
PAIRP(BgL_pz00_1529))
{ /* R5rs/syntax.scm 445 */
BgL_pairz00_2629 = BgL_pz00_1529; }  else 
{ 
 obj_t BgL_auxz00_5377;
BgL_auxz00_5377 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(14823L), BGl_string2967z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_pz00_1529); 
FAILURE(BgL_auxz00_5377,BFALSE,BFALSE);} 
BgL_arg1504z00_1534 = 
CAR(BgL_pairz00_2629); } 
BgL_arg1502z00_1532 = 
BGl_subze70ze7zz__r5_macro_4_3_syntaxz00(BgL_kz00_3182, BgL_arg1504z00_1534); } 
{ /* R5rs/syntax.scm 445 */
 obj_t BgL_arg1505z00_1535;
{ /* R5rs/syntax.scm 445 */
 obj_t BgL_pairz00_2630;
if(
PAIRP(BgL_pz00_1529))
{ /* R5rs/syntax.scm 445 */
BgL_pairz00_2630 = BgL_pz00_1529; }  else 
{ 
 obj_t BgL_auxz00_5385;
BgL_auxz00_5385 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(14838L), BGl_string2967z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_pz00_1529); 
FAILURE(BgL_auxz00_5385,BFALSE,BFALSE);} 
{ /* R5rs/syntax.scm 445 */
 obj_t BgL_pairz00_2633;
{ /* R5rs/syntax.scm 445 */
 obj_t BgL_aux2434z00_3360;
BgL_aux2434z00_3360 = 
CDR(BgL_pairz00_2630); 
if(
PAIRP(BgL_aux2434z00_3360))
{ /* R5rs/syntax.scm 445 */
BgL_pairz00_2633 = BgL_aux2434z00_3360; }  else 
{ 
 obj_t BgL_auxz00_5392;
BgL_auxz00_5392 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(14832L), BGl_string2967z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_aux2434z00_3360); 
FAILURE(BgL_auxz00_5392,BFALSE,BFALSE);} } 
BgL_arg1505z00_1535 = 
CDR(BgL_pairz00_2633); } } 
BgL_arg1503z00_1533 = 
BGl_subze70ze7zz__r5_macro_4_3_syntaxz00(BgL_kz00_3182, BgL_arg1505z00_1535); } 
return 
MAKE_YOUNG_PAIR(BgL_arg1502z00_1532, BgL_arg1503z00_1533);}  else 
{ /* R5rs/syntax.scm 444 */
if(
PAIRP(BgL_pz00_1529))
{ /* R5rs/syntax.scm 447 */
 obj_t BgL_arg1507z00_1537; obj_t BgL_arg1508z00_1538;
BgL_arg1507z00_1537 = 
BGl_subze70ze7zz__r5_macro_4_3_syntaxz00(BgL_kz00_3182, 
CAR(BgL_pz00_1529)); 
BgL_arg1508z00_1538 = 
BGl_subze70ze7zz__r5_macro_4_3_syntaxz00(BgL_kz00_3182, 
CDR(BgL_pz00_1529)); 
{ /* R5rs/syntax.scm 447 */
 obj_t BgL_auxz00_5405;
{ /* R5rs/syntax.scm 447 */
 bool_t BgL_test3357z00_5406;
if(
PAIRP(BgL_arg1507z00_1537))
{ /* R5rs/syntax.scm 447 */
BgL_test3357z00_5406 = ((bool_t)1)
; }  else 
{ /* R5rs/syntax.scm 447 */
BgL_test3357z00_5406 = 
NULLP(BgL_arg1507z00_1537)
; } 
if(BgL_test3357z00_5406)
{ /* R5rs/syntax.scm 447 */
BgL_auxz00_5405 = BgL_arg1507z00_1537
; }  else 
{ 
 obj_t BgL_auxz00_5410;
BgL_auxz00_5410 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(14860L), BGl_string2967z00zz__r5_macro_4_3_syntaxz00, BGl_string2650z00zz__r5_macro_4_3_syntaxz00, BgL_arg1507z00_1537); 
FAILURE(BgL_auxz00_5410,BFALSE,BFALSE);} } 
return 
BGl_appendzd221011zd2zz__r5_macro_4_3_syntaxz00(BgL_auxz00_5405, BgL_arg1508z00_1538);} }  else 
{ /* R5rs/syntax.scm 446 */
if(
SYMBOLP(BgL_pz00_1529))
{ /* R5rs/syntax.scm 449 */
 bool_t BgL_test3360z00_5417;
{ /* R5rs/syntax.scm 449 */
 obj_t BgL_tmpz00_5418;
{ /* R5rs/syntax.scm 449 */
 obj_t BgL_auxz00_5419;
{ /* R5rs/syntax.scm 449 */
 bool_t BgL_test3361z00_5420;
if(
PAIRP(BgL_kz00_3182))
{ /* R5rs/syntax.scm 449 */
BgL_test3361z00_5420 = ((bool_t)1)
; }  else 
{ /* R5rs/syntax.scm 449 */
BgL_test3361z00_5420 = 
NULLP(BgL_kz00_3182)
; } 
if(BgL_test3361z00_5420)
{ /* R5rs/syntax.scm 449 */
BgL_auxz00_5419 = BgL_kz00_3182
; }  else 
{ 
 obj_t BgL_auxz00_5424;
BgL_auxz00_5424 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(14928L), BGl_string2967z00zz__r5_macro_4_3_syntaxz00, BGl_string2650z00zz__r5_macro_4_3_syntaxz00, BgL_kz00_3182); 
FAILURE(BgL_auxz00_5424,BFALSE,BFALSE);} } 
BgL_tmpz00_5418 = 
BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_pz00_1529, BgL_auxz00_5419); } 
BgL_test3360z00_5417 = 
CBOOL(BgL_tmpz00_5418); } 
if(BgL_test3360z00_5417)
{ /* R5rs/syntax.scm 449 */
return BNIL;}  else 
{ /* R5rs/syntax.scm 449 */
 obj_t BgL_list1513z00_1543;
BgL_list1513z00_1543 = 
MAKE_YOUNG_PAIR(BgL_pz00_1529, BNIL); 
return BgL_list1513z00_1543;} }  else 
{ /* R5rs/syntax.scm 448 */
return BNIL;} } } } 

}



/* ellipsis? */
bool_t BGl_ellipsiszf3zf3zz__r5_macro_4_3_syntaxz00(obj_t BgL_xz00_35)
{
{ /* R5rs/syntax.scm 456 */
if(
PAIRP(BgL_xz00_35))
{ /* R5rs/syntax.scm 458 */
 bool_t BgL_test3364z00_5433;
{ /* R5rs/syntax.scm 458 */
 obj_t BgL_tmpz00_5434;
BgL_tmpz00_5434 = 
CDR(BgL_xz00_35); 
BgL_test3364z00_5433 = 
PAIRP(BgL_tmpz00_5434); } 
if(BgL_test3364z00_5433)
{ /* R5rs/syntax.scm 459 */
 obj_t BgL_tmpz00_5437;
{ /* R5rs/syntax.scm 459 */
 obj_t BgL_pairz00_2641;
{ /* R5rs/syntax.scm 459 */
 obj_t BgL_aux2440z00_3366;
BgL_aux2440z00_3366 = 
CDR(BgL_xz00_35); 
if(
PAIRP(BgL_aux2440z00_3366))
{ /* R5rs/syntax.scm 459 */
BgL_pairz00_2641 = BgL_aux2440z00_3366; }  else 
{ 
 obj_t BgL_auxz00_5441;
BgL_auxz00_5441 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(15251L), BGl_string2968z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_aux2440z00_3366); 
FAILURE(BgL_auxz00_5441,BFALSE,BFALSE);} } 
BgL_tmpz00_5437 = 
CAR(BgL_pairz00_2641); } 
return 
(BgL_tmpz00_5437==BGl_symbol2672z00zz__r5_macro_4_3_syntaxz00);}  else 
{ /* R5rs/syntax.scm 458 */
return ((bool_t)0);} }  else 
{ /* R5rs/syntax.scm 457 */
return ((bool_t)0);} } 

}



/* hygiene-value* */
obj_t BGl_hygienezd2valueza2z70zz__r5_macro_4_3_syntaxz00(obj_t BgL_xz00_38)
{
{ /* R5rs/syntax.scm 477 */
if(
SYMBOLP(BgL_xz00_38))
{ /* R5rs/syntax.scm 482 */
 obj_t BgL_sz00_1554;
{ /* R5rs/syntax.scm 482 */
 obj_t BgL_arg2070z00_2654;
BgL_arg2070z00_2654 = 
SYMBOL_TO_STRING(BgL_xz00_38); 
BgL_sz00_1554 = 
BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg2070z00_2654); } 
{ 
 obj_t BgL_sz00_1556;
BgL_sz00_1556 = BgL_sz00_1554; 
BgL_zc3z04anonymousza31524ze3z87_1557:
if(
bigloo_strcmp_at(BgL_sz00_1556, BGl_hygienezd2prefixzd2zz__r5_macro_4_3_syntaxz00, 0L))
{ /* R5rs/syntax.scm 485 */
 obj_t BgL_arg1526z00_1559;
{ /* R5rs/syntax.scm 485 */
 long BgL_arg1527z00_1560;
BgL_arg1527z00_1560 = 
STRING_LENGTH(BgL_sz00_1556); 
BgL_arg1526z00_1559 = 
BGl_substringz00zz__r4_strings_6_7z00(BgL_sz00_1556, BGl_hygienezd2prefixzd2lenz00zz__r5_macro_4_3_syntaxz00, BgL_arg1527z00_1560); } 
{ 
 obj_t BgL_sz00_5455;
BgL_sz00_5455 = BgL_arg1526z00_1559; 
BgL_sz00_1556 = BgL_sz00_5455; 
goto BgL_zc3z04anonymousza31524ze3z87_1557;} }  else 
{ /* R5rs/syntax.scm 484 */
return 
bstring_to_symbol(BgL_sz00_1556);} } }  else 
{ /* R5rs/syntax.scm 480 */
return BgL_xz00_38;} } 

}



/* hygiene-value */
obj_t BGl_hygienezd2valuezd2zz__r5_macro_4_3_syntaxz00(obj_t BgL_xz00_39)
{
{ /* R5rs/syntax.scm 491 */
if(
SYMBOLP(BgL_xz00_39))
{ /* R5rs/syntax.scm 494 */
 obj_t BgL_sz00_1563;
{ /* R5rs/syntax.scm 494 */
 obj_t BgL_arg2070z00_2658;
BgL_arg2070z00_2658 = 
SYMBOL_TO_STRING(BgL_xz00_39); 
BgL_sz00_1563 = 
BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg2070z00_2658); } 
if(
bigloo_strcmp_at(BgL_sz00_1563, BGl_hygienezd2prefixzd2zz__r5_macro_4_3_syntaxz00, 0L))
{ /* R5rs/syntax.scm 497 */
 obj_t BgL_arg1530z00_1565;
{ /* R5rs/syntax.scm 497 */
 long BgL_arg1531z00_1566;
BgL_arg1531z00_1566 = 
STRING_LENGTH(BgL_sz00_1563); 
BgL_arg1530z00_1565 = 
BGl_substringz00zz__r4_strings_6_7z00(BgL_sz00_1563, BGl_hygienezd2prefixzd2lenz00zz__r5_macro_4_3_syntaxz00, BgL_arg1531z00_1566); } 
return 
bstring_to_symbol(BgL_arg1530z00_1565);}  else 
{ /* R5rs/syntax.scm 495 */
return BgL_xz00_39;} }  else 
{ /* R5rs/syntax.scm 492 */
return BgL_xz00_39;} } 

}



/* hygiene-eq? */
bool_t BGl_hygienezd2eqzf3z21zz__r5_macro_4_3_syntaxz00(obj_t BgL_xz00_40, obj_t BgL_idz00_41)
{
{ /* R5rs/syntax.scm 503 */
BGl_hygienezd2eqzf3z21zz__r5_macro_4_3_syntaxz00:
{ /* R5rs/syntax.scm 504 */
 bool_t BgL_test3370z00_5466;
if(
SYMBOLP(BgL_idz00_41))
{ /* R5rs/syntax.scm 504 */
BgL_test3370z00_5466 = 
SYMBOLP(BgL_xz00_40)
; }  else 
{ /* R5rs/syntax.scm 504 */
BgL_test3370z00_5466 = ((bool_t)0)
; } 
if(BgL_test3370z00_5466)
{ /* R5rs/syntax.scm 505 */
 bool_t BgL__ortest_1049z00_1569;
BgL__ortest_1049z00_1569 = 
(BgL_xz00_40==BgL_idz00_41); 
if(BgL__ortest_1049z00_1569)
{ /* R5rs/syntax.scm 505 */
return BgL__ortest_1049z00_1569;}  else 
{ /* R5rs/syntax.scm 506 */
 bool_t BgL_test3373z00_5472;
{ /* R5rs/syntax.scm 471 */
 obj_t BgL_sz00_2661;
{ /* R5rs/syntax.scm 471 */
 obj_t BgL_arg2070z00_2663;
BgL_arg2070z00_2663 = 
SYMBOL_TO_STRING(BgL_xz00_40); 
BgL_sz00_2661 = 
BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg2070z00_2663); } 
BgL_test3373z00_5472 = 
bigloo_strcmp_at(BgL_sz00_2661, BGl_hygienezd2prefixzd2zz__r5_macro_4_3_syntaxz00, 0L); } 
if(BgL_test3373z00_5472)
{ 
 obj_t BgL_xz00_5476;
BgL_xz00_5476 = 
BGl_hygienezd2valuezd2zz__r5_macro_4_3_syntaxz00(BgL_xz00_40); 
BgL_xz00_40 = BgL_xz00_5476; 
goto BGl_hygienezd2eqzf3z21zz__r5_macro_4_3_syntaxz00;}  else 
{ /* R5rs/syntax.scm 506 */
return ((bool_t)0);} } }  else 
{ /* R5rs/syntax.scm 504 */
return ((bool_t)0);} } } 

}



/* unhygienize */
obj_t BGl_unhygieniza7eza7zz__r5_macro_4_3_syntaxz00(obj_t BgL_xz00_42)
{
{ /* R5rs/syntax.scm 511 */
if(
SYMBOLP(BgL_xz00_42))
{ /* R5rs/syntax.scm 465 */
 obj_t BgL_arg1517z00_2664;
{ /* R5rs/syntax.scm 465 */
 obj_t BgL_arg1521z00_2665; obj_t BgL_arg1522z00_2666;
{ /* R5rs/syntax.scm 465 */
 obj_t BgL_symbolz00_2667;
BgL_symbolz00_2667 = BGl_hygienezd2markzd2zz__r5_macro_4_3_syntaxz00; 
{ /* R5rs/syntax.scm 465 */
 obj_t BgL_arg2070z00_2668;
BgL_arg2070z00_2668 = 
SYMBOL_TO_STRING(BgL_symbolz00_2667); 
BgL_arg1521z00_2665 = 
BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg2070z00_2668); } } 
{ /* R5rs/syntax.scm 465 */
 obj_t BgL_arg2070z00_2670;
BgL_arg2070z00_2670 = 
SYMBOL_TO_STRING(BgL_xz00_42); 
BgL_arg1522z00_2666 = 
BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg2070z00_2670); } 
BgL_arg1517z00_2664 = 
string_append(BgL_arg1521z00_2665, BgL_arg1522z00_2666); } 
return 
bstring_to_symbol(BgL_arg1517z00_2664);}  else 
{ /* R5rs/syntax.scm 513 */
if(
PAIRP(BgL_xz00_42))
{ /* R5rs/syntax.scm 515 */
return 
MAKE_YOUNG_PAIR(
BGl_unhygieniza7eza7zz__r5_macro_4_3_syntaxz00(
CAR(BgL_xz00_42)), 
BGl_unhygieniza7eza7zz__r5_macro_4_3_syntaxz00(
CDR(BgL_xz00_42)));}  else 
{ /* R5rs/syntax.scm 515 */
return BgL_xz00_42;} } } 

}



/* hygienize */
obj_t BGl_hygieniza7eza7zz__r5_macro_4_3_syntaxz00(obj_t BgL_xz00_43, obj_t BgL_envz00_44)
{
{ /* R5rs/syntax.scm 523 */
{ 
 obj_t BgL_varz00_1599; obj_t BgL_bodyz00_1600; obj_t BgL_bindingsz00_1596; obj_t BgL_bodyz00_1597; obj_t BgL_bindingsz00_1593; obj_t BgL_bodyz00_1594; obj_t BgL_bindingsz00_1590; obj_t BgL_bodyz00_1591; obj_t BgL_loopz00_1586; obj_t BgL_bindingsz00_1587; obj_t BgL_bodyz00_1588; obj_t BgL_varsz00_1583; obj_t BgL_bodyz00_1584; obj_t BgL_varz00_1579;
if(
SYMBOLP(BgL_xz00_43))
{ /* R5rs/syntax.scm 524 */
BgL_varz00_1579 = BgL_xz00_43; 
{ /* R5rs/syntax.scm 526 */
 bool_t BgL_test3377z00_5495;
{ /* R5rs/syntax.scm 471 */
 obj_t BgL_sz00_2674;
{ /* R5rs/syntax.scm 471 */
 obj_t BgL_symbolz00_2675;
if(
SYMBOLP(BgL_xz00_43))
{ /* R5rs/syntax.scm 471 */
BgL_symbolz00_2675 = BgL_xz00_43; }  else 
{ 
 obj_t BgL_auxz00_5498;
BgL_auxz00_5498 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(15832L), BGl_string2969z00zz__r5_macro_4_3_syntaxz00, BGl_string2652z00zz__r5_macro_4_3_syntaxz00, BgL_xz00_43); 
FAILURE(BgL_auxz00_5498,BFALSE,BFALSE);} 
{ /* R5rs/syntax.scm 471 */
 obj_t BgL_arg2070z00_2676;
BgL_arg2070z00_2676 = 
SYMBOL_TO_STRING(BgL_symbolz00_2675); 
BgL_sz00_2674 = 
BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg2070z00_2676); } } 
BgL_test3377z00_5495 = 
bigloo_strcmp_at(BgL_sz00_2674, BGl_hygienezd2prefixzd2zz__r5_macro_4_3_syntaxz00, 0L); } 
if(BgL_test3377z00_5495)
{ /* R5rs/syntax.scm 526 */
return 
BGl_hygienezd2valuezd2zz__r5_macro_4_3_syntaxz00(BgL_xz00_43);}  else 
{ /* R5rs/syntax.scm 528 */
 obj_t BgL_oz00_1852;
{ /* R5rs/syntax.scm 528 */
 obj_t BgL_auxz00_5506;
{ /* R5rs/syntax.scm 528 */
 bool_t BgL_test3379z00_5507;
if(
PAIRP(BgL_envz00_44))
{ /* R5rs/syntax.scm 528 */
BgL_test3379z00_5507 = ((bool_t)1)
; }  else 
{ /* R5rs/syntax.scm 528 */
BgL_test3379z00_5507 = 
NULLP(BgL_envz00_44)
; } 
if(BgL_test3379z00_5507)
{ /* R5rs/syntax.scm 528 */
BgL_auxz00_5506 = BgL_envz00_44
; }  else 
{ 
 obj_t BgL_auxz00_5511;
BgL_auxz00_5511 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(18135L), BGl_string2969z00zz__r5_macro_4_3_syntaxz00, BGl_string2650z00zz__r5_macro_4_3_syntaxz00, BgL_envz00_44); 
FAILURE(BgL_auxz00_5511,BFALSE,BFALSE);} } 
BgL_oz00_1852 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_varz00_1579, BgL_auxz00_5506); } 
if(
PAIRP(BgL_oz00_1852))
{ /* R5rs/syntax.scm 529 */
return 
CDR(BgL_oz00_1852);}  else 
{ /* R5rs/syntax.scm 529 */
return BgL_varz00_1579;} } } }  else 
{ /* R5rs/syntax.scm 524 */
if(
PAIRP(BgL_xz00_43))
{ /* R5rs/syntax.scm 524 */
if(
(
CAR(
((obj_t)BgL_xz00_43))==BGl_symbol2655z00zz__r5_macro_4_3_syntaxz00))
{ /* R5rs/syntax.scm 524 */
 obj_t BgL_arg1549z00_1609;
BgL_arg1549z00_1609 = 
CDR(
((obj_t)BgL_xz00_43)); 
{ /* R5rs/syntax.scm 533 */
 obj_t BgL_arg1794z00_2747;
BgL_arg1794z00_2747 = 
BGl_loopze70ze7zz__r5_macro_4_3_syntaxz00(BgL_envz00_44, BgL_arg1549z00_1609); 
return 
MAKE_YOUNG_PAIR(BGl_symbol2655z00zz__r5_macro_4_3_syntaxz00, BgL_arg1794z00_2747);} }  else 
{ /* R5rs/syntax.scm 524 */
 obj_t BgL_cdrzd2271zd2_1610;
BgL_cdrzd2271zd2_1610 = 
CDR(
((obj_t)BgL_xz00_43)); 
if(
(
CAR(
((obj_t)BgL_xz00_43))==BGl_symbol2770z00zz__r5_macro_4_3_syntaxz00))
{ /* R5rs/syntax.scm 524 */
if(
PAIRP(BgL_cdrzd2271zd2_1610))
{ /* R5rs/syntax.scm 524 */
BgL_varsz00_1583 = 
CAR(BgL_cdrzd2271zd2_1610); 
BgL_bodyz00_1584 = 
CDR(BgL_cdrzd2271zd2_1610); 
{ /* R5rs/syntax.scm 535 */
 obj_t BgL_nvarsz00_1855;
BgL_nvarsz00_1855 = 
BGl_genvarsz00zz__r5_macro_4_3_syntaxz00(BgL_varsz00_1583); 
{ /* R5rs/syntax.scm 535 */
 obj_t BgL_nenvz00_1856;
{ /* R5rs/syntax.scm 536 */
 obj_t BgL_arg1798z00_1860;
{ /* R5rs/syntax.scm 536 */
 obj_t BgL_arg1799z00_1861; obj_t BgL_arg1800z00_1862;
BgL_arg1799z00_1861 = 
BGl_flattenz00zz__r5_macro_4_3_syntaxz00(BgL_varsz00_1583); 
BgL_arg1800z00_1862 = 
BGl_flattenz00zz__r5_macro_4_3_syntaxz00(BgL_nvarsz00_1855); 
{ /* R5rs/syntax.scm 536 */
 obj_t BgL_list1801z00_1863;
{ /* R5rs/syntax.scm 536 */
 obj_t BgL_arg1802z00_1864;
BgL_arg1802z00_1864 = 
MAKE_YOUNG_PAIR(BgL_arg1800z00_1862, BNIL); 
BgL_list1801z00_1863 = 
MAKE_YOUNG_PAIR(BgL_arg1799z00_1861, BgL_arg1802z00_1864); } 
BgL_arg1798z00_1860 = 
BGl_mapz00zz__r4_control_features_6_9z00(BGl_conszd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_list1801z00_1863); } } 
BgL_nenvz00_1856 = 
BGl_appendzd221011zd2zz__r5_macro_4_3_syntaxz00(BgL_arg1798z00_1860, BgL_envz00_44); } 
{ /* R5rs/syntax.scm 536 */

{ /* R5rs/syntax.scm 538 */
 obj_t BgL_arg1795z00_1857;
{ /* R5rs/syntax.scm 538 */
 obj_t BgL_arg1796z00_1858;
{ /* R5rs/syntax.scm 538 */
 obj_t BgL_arg1797z00_1859;
BgL_arg1797z00_1859 = 
BGl_loopze70ze7zz__r5_macro_4_3_syntaxz00(BgL_nenvz00_1856, BgL_bodyz00_1584); 
{ /* R5rs/syntax.scm 537 */
 obj_t BgL_auxz00_5545;
{ /* R5rs/syntax.scm 537 */
 bool_t BgL_test3386z00_5546;
if(
PAIRP(BgL_arg1797z00_1859))
{ /* R5rs/syntax.scm 537 */
BgL_test3386z00_5546 = ((bool_t)1)
; }  else 
{ /* R5rs/syntax.scm 537 */
BgL_test3386z00_5546 = 
NULLP(BgL_arg1797z00_1859)
; } 
if(BgL_test3386z00_5546)
{ /* R5rs/syntax.scm 537 */
BgL_auxz00_5545 = BgL_arg1797z00_1859
; }  else 
{ 
 obj_t BgL_auxz00_5550;
BgL_auxz00_5550 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(18403L), BGl_string2970z00zz__r5_macro_4_3_syntaxz00, BGl_string2650z00zz__r5_macro_4_3_syntaxz00, BgL_arg1797z00_1859); 
FAILURE(BgL_auxz00_5550,BFALSE,BFALSE);} } 
BgL_arg1796z00_1858 = 
BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_5545, BNIL); } } 
BgL_arg1795z00_1857 = 
MAKE_YOUNG_PAIR(BgL_nvarsz00_1855, BgL_arg1796z00_1858); } 
return 
MAKE_YOUNG_PAIR(BGl_symbol2770z00zz__r5_macro_4_3_syntaxz00, BgL_arg1795z00_1857);} } } } }  else 
{ /* R5rs/syntax.scm 524 */
BGL_TAIL return 
BGl_loopze70ze7zz__r5_macro_4_3_syntaxz00(BgL_envz00_44, BgL_xz00_43);} }  else 
{ /* R5rs/syntax.scm 524 */
 obj_t BgL_cdrzd2312zd2_1616;
BgL_cdrzd2312zd2_1616 = 
CDR(
((obj_t)BgL_xz00_43)); 
if(
(
CAR(
((obj_t)BgL_xz00_43))==BGl_symbol2685z00zz__r5_macro_4_3_syntaxz00))
{ /* R5rs/syntax.scm 524 */
if(
PAIRP(BgL_cdrzd2312zd2_1616))
{ /* R5rs/syntax.scm 524 */
 obj_t BgL_carzd2316zd2_1620; obj_t BgL_cdrzd2317zd2_1621;
BgL_carzd2316zd2_1620 = 
CAR(BgL_cdrzd2312zd2_1616); 
BgL_cdrzd2317zd2_1621 = 
CDR(BgL_cdrzd2312zd2_1616); 
if(
SYMBOLP(BgL_carzd2316zd2_1620))
{ /* R5rs/syntax.scm 524 */
if(
PAIRP(BgL_cdrzd2317zd2_1621))
{ /* R5rs/syntax.scm 524 */
BgL_loopz00_1586 = BgL_carzd2316zd2_1620; 
BgL_bindingsz00_1587 = 
CAR(BgL_cdrzd2317zd2_1621); 
BgL_bodyz00_1588 = 
CDR(BgL_cdrzd2317zd2_1621); 
{ /* R5rs/syntax.scm 540 */
 obj_t BgL_nloopz00_1865;
BgL_nloopz00_1865 = 
BGl_genvarsz00zz__r5_macro_4_3_syntaxz00(BgL_loopz00_1586); 
{ /* R5rs/syntax.scm 540 */
 obj_t BgL_nvarsz00_1866;
{ /* R5rs/syntax.scm 541 */
 obj_t BgL_arg1827z00_1895;
if(
NULLP(BgL_bindingsz00_1587))
{ /* R5rs/syntax.scm 541 */
BgL_arg1827z00_1895 = BNIL; }  else 
{ /* R5rs/syntax.scm 541 */
 obj_t BgL_head1130z00_1898;
{ /* R5rs/syntax.scm 541 */
 obj_t BgL_arg1835z00_1911;
{ /* R5rs/syntax.scm 541 */
 obj_t BgL_pairz00_2680;
{ /* R5rs/syntax.scm 541 */
 obj_t BgL_pairz00_2679;
if(
PAIRP(BgL_bindingsz00_1587))
{ /* R5rs/syntax.scm 541 */
BgL_pairz00_2679 = BgL_bindingsz00_1587; }  else 
{ 
 obj_t BgL_auxz00_5579;
BgL_auxz00_5579 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(18568L), BGl_string2971z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_bindingsz00_1587); 
FAILURE(BgL_auxz00_5579,BFALSE,BFALSE);} 
{ /* R5rs/syntax.scm 541 */
 obj_t BgL_aux2450z00_3376;
BgL_aux2450z00_3376 = 
CAR(BgL_pairz00_2679); 
if(
PAIRP(BgL_aux2450z00_3376))
{ /* R5rs/syntax.scm 541 */
BgL_pairz00_2680 = BgL_aux2450z00_3376; }  else 
{ 
 obj_t BgL_auxz00_5586;
BgL_auxz00_5586 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(18568L), BGl_string2971z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_aux2450z00_3376); 
FAILURE(BgL_auxz00_5586,BFALSE,BFALSE);} } } 
BgL_arg1835z00_1911 = 
CAR(BgL_pairz00_2680); } 
BgL_head1130z00_1898 = 
MAKE_YOUNG_PAIR(BgL_arg1835z00_1911, BNIL); } 
{ /* R5rs/syntax.scm 541 */
 obj_t BgL_g1133z00_1899;
{ /* R5rs/syntax.scm 541 */
 obj_t BgL_pairz00_2681;
if(
PAIRP(BgL_bindingsz00_1587))
{ /* R5rs/syntax.scm 541 */
BgL_pairz00_2681 = BgL_bindingsz00_1587; }  else 
{ 
 obj_t BgL_auxz00_5594;
BgL_auxz00_5594 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(18568L), BGl_string2971z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_bindingsz00_1587); 
FAILURE(BgL_auxz00_5594,BFALSE,BFALSE);} 
BgL_g1133z00_1899 = 
CDR(BgL_pairz00_2681); } 
{ 
 obj_t BgL_l1128z00_1901; obj_t BgL_tail1131z00_1902;
BgL_l1128z00_1901 = BgL_g1133z00_1899; 
BgL_tail1131z00_1902 = BgL_head1130z00_1898; 
BgL_zc3z04anonymousza31829ze3z87_1903:
if(
PAIRP(BgL_l1128z00_1901))
{ /* R5rs/syntax.scm 541 */
 obj_t BgL_newtail1132z00_1905;
{ /* R5rs/syntax.scm 541 */
 obj_t BgL_arg1832z00_1907;
{ /* R5rs/syntax.scm 541 */
 obj_t BgL_pairz00_2683;
{ /* R5rs/syntax.scm 541 */
 obj_t BgL_aux2454z00_3380;
BgL_aux2454z00_3380 = 
CAR(BgL_l1128z00_1901); 
if(
PAIRP(BgL_aux2454z00_3380))
{ /* R5rs/syntax.scm 541 */
BgL_pairz00_2683 = BgL_aux2454z00_3380; }  else 
{ 
 obj_t BgL_auxz00_5604;
BgL_auxz00_5604 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(18568L), BGl_string2972z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_aux2454z00_3380); 
FAILURE(BgL_auxz00_5604,BFALSE,BFALSE);} } 
BgL_arg1832z00_1907 = 
CAR(BgL_pairz00_2683); } 
BgL_newtail1132z00_1905 = 
MAKE_YOUNG_PAIR(BgL_arg1832z00_1907, BNIL); } 
SET_CDR(BgL_tail1131z00_1902, BgL_newtail1132z00_1905); 
{ 
 obj_t BgL_tail1131z00_5613; obj_t BgL_l1128z00_5611;
BgL_l1128z00_5611 = 
CDR(BgL_l1128z00_1901); 
BgL_tail1131z00_5613 = BgL_newtail1132z00_1905; 
BgL_tail1131z00_1902 = BgL_tail1131z00_5613; 
BgL_l1128z00_1901 = BgL_l1128z00_5611; 
goto BgL_zc3z04anonymousza31829ze3z87_1903;} }  else 
{ /* R5rs/syntax.scm 541 */
if(
NULLP(BgL_l1128z00_1901))
{ /* R5rs/syntax.scm 541 */
BgL_arg1827z00_1895 = BgL_head1130z00_1898; }  else 
{ /* R5rs/syntax.scm 541 */
BgL_arg1827z00_1895 = 
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BGl_string2912z00zz__r5_macro_4_3_syntaxz00, BGl_string2913z00zz__r5_macro_4_3_syntaxz00, BgL_l1128z00_1901, BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(18568L)); } } } } } 
BgL_nvarsz00_1866 = 
BGl_genvarsz00zz__r5_macro_4_3_syntaxz00(BgL_arg1827z00_1895); } 
{ /* R5rs/syntax.scm 541 */
 obj_t BgL_nenvz00_1867;
{ /* R5rs/syntax.scm 542 */
 obj_t BgL_arg1816z00_1884; obj_t BgL_arg1817z00_1885;
BgL_arg1816z00_1884 = 
MAKE_YOUNG_PAIR(BgL_loopz00_1586, BgL_nloopz00_1865); 
{ /* R5rs/syntax.scm 544 */
 obj_t BgL_arg1818z00_1886;
{ /* R5rs/syntax.scm 543 */
 obj_t BgL_list1820z00_1888;
{ /* R5rs/syntax.scm 543 */
 obj_t BgL_arg1822z00_1889;
BgL_arg1822z00_1889 = 
MAKE_YOUNG_PAIR(BgL_nvarsz00_1866, BNIL); 
BgL_list1820z00_1888 = 
MAKE_YOUNG_PAIR(BgL_bindingsz00_1587, BgL_arg1822z00_1889); } 
BgL_arg1818z00_1886 = 
BGl_mapz00zz__r4_control_features_6_9z00(BGl_proc2973z00zz__r5_macro_4_3_syntaxz00, BgL_list1820z00_1888); } 
BgL_arg1817z00_1885 = 
BGl_appendzd221011zd2zz__r5_macro_4_3_syntaxz00(BgL_arg1818z00_1886, BgL_envz00_44); } 
BgL_nenvz00_1867 = 
MAKE_YOUNG_PAIR(BgL_arg1816z00_1884, BgL_arg1817z00_1885); } 
{ /* R5rs/syntax.scm 542 */

{ /* R5rs/syntax.scm 548 */
 obj_t BgL_arg1803z00_1868;
{ /* R5rs/syntax.scm 548 */
 obj_t BgL_arg1804z00_1869;
{ /* R5rs/syntax.scm 548 */
 obj_t BgL_arg1805z00_1870; obj_t BgL_arg1806z00_1871;
{ /* R5rs/syntax.scm 548 */
 obj_t BgL_zc3z04anonymousza31810ze3z87_3155;
BgL_zc3z04anonymousza31810ze3z87_3155 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31810ze3ze5zz__r5_macro_4_3_syntaxz00, 
(int)(2L), 
(int)(1L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31810ze3z87_3155, 
(int)(0L), BgL_envz00_44); 
{ /* R5rs/syntax.scm 547 */
 obj_t BgL_list1808z00_1873;
{ /* R5rs/syntax.scm 547 */
 obj_t BgL_arg1809z00_1874;
BgL_arg1809z00_1874 = 
MAKE_YOUNG_PAIR(BgL_nvarsz00_1866, BNIL); 
BgL_list1808z00_1873 = 
MAKE_YOUNG_PAIR(BgL_bindingsz00_1587, BgL_arg1809z00_1874); } 
BgL_arg1805z00_1870 = 
BGl_mapz00zz__r4_control_features_6_9z00(BgL_zc3z04anonymousza31810ze3z87_3155, BgL_list1808z00_1873); } } 
{ /* R5rs/syntax.scm 550 */
 obj_t BgL_arg1815z00_1883;
BgL_arg1815z00_1883 = 
BGl_loopze70ze7zz__r5_macro_4_3_syntaxz00(BgL_nenvz00_1867, BgL_bodyz00_1588); 
{ /* R5rs/syntax.scm 547 */
 obj_t BgL_auxz00_5634;
{ /* R5rs/syntax.scm 547 */
 bool_t BgL_test3399z00_5635;
if(
PAIRP(BgL_arg1815z00_1883))
{ /* R5rs/syntax.scm 547 */
BgL_test3399z00_5635 = ((bool_t)1)
; }  else 
{ /* R5rs/syntax.scm 547 */
BgL_test3399z00_5635 = 
NULLP(BgL_arg1815z00_1883)
; } 
if(BgL_test3399z00_5635)
{ /* R5rs/syntax.scm 547 */
BgL_auxz00_5634 = BgL_arg1815z00_1883
; }  else 
{ 
 obj_t BgL_auxz00_5639;
BgL_auxz00_5639 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(18728L), BGl_string2971z00zz__r5_macro_4_3_syntaxz00, BGl_string2650z00zz__r5_macro_4_3_syntaxz00, BgL_arg1815z00_1883); 
FAILURE(BgL_auxz00_5639,BFALSE,BFALSE);} } 
BgL_arg1806z00_1871 = 
BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_5634, BNIL); } } 
BgL_arg1804z00_1869 = 
MAKE_YOUNG_PAIR(BgL_arg1805z00_1870, BgL_arg1806z00_1871); } 
BgL_arg1803z00_1868 = 
MAKE_YOUNG_PAIR(BgL_nloopz00_1865, BgL_arg1804z00_1869); } 
return 
MAKE_YOUNG_PAIR(BGl_symbol2685z00zz__r5_macro_4_3_syntaxz00, BgL_arg1803z00_1868);} } } } } }  else 
{ /* R5rs/syntax.scm 524 */
 obj_t BgL_cdrzd2333zd2_1626;
BgL_cdrzd2333zd2_1626 = 
CDR(
((obj_t)BgL_xz00_43)); 
{ /* R5rs/syntax.scm 524 */
 obj_t BgL_arg1565z00_1627; obj_t BgL_arg1567z00_1628;
{ /* R5rs/syntax.scm 524 */
 obj_t BgL_pairz00_2759;
if(
PAIRP(BgL_cdrzd2333zd2_1626))
{ /* R5rs/syntax.scm 524 */
BgL_pairz00_2759 = BgL_cdrzd2333zd2_1626; }  else 
{ 
 obj_t BgL_auxz00_5653;
BgL_auxz00_5653 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(18015L), BGl_string2983z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_cdrzd2333zd2_1626); 
FAILURE(BgL_auxz00_5653,BFALSE,BFALSE);} 
BgL_arg1565z00_1627 = 
CAR(BgL_pairz00_2759); } 
{ /* R5rs/syntax.scm 524 */
 obj_t BgL_pairz00_2760;
if(
PAIRP(BgL_cdrzd2333zd2_1626))
{ /* R5rs/syntax.scm 524 */
BgL_pairz00_2760 = BgL_cdrzd2333zd2_1626; }  else 
{ 
 obj_t BgL_auxz00_5660;
BgL_auxz00_5660 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(18015L), BGl_string2983z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_cdrzd2333zd2_1626); 
FAILURE(BgL_auxz00_5660,BFALSE,BFALSE);} 
BgL_arg1567z00_1628 = 
CDR(BgL_pairz00_2760); } 
BgL_bindingsz00_1590 = BgL_arg1565z00_1627; 
BgL_bodyz00_1591 = BgL_arg1567z00_1628; 
BgL_tagzd2238zd2_1592:
{ /* R5rs/syntax.scm 552 */
 obj_t BgL_nvarsz00_1913;
{ /* R5rs/syntax.scm 552 */
 obj_t BgL_arg1856z00_1939;
if(
NULLP(BgL_bindingsz00_1590))
{ /* R5rs/syntax.scm 552 */
BgL_arg1856z00_1939 = BNIL; }  else 
{ /* R5rs/syntax.scm 552 */
 obj_t BgL_head1136z00_1942;
{ /* R5rs/syntax.scm 552 */
 obj_t BgL_arg1866z00_1955;
{ /* R5rs/syntax.scm 552 */
 obj_t BgL_pairz00_2698;
{ /* R5rs/syntax.scm 552 */
 obj_t BgL_pairz00_2697;
if(
PAIRP(BgL_bindingsz00_1590))
{ /* R5rs/syntax.scm 552 */
BgL_pairz00_2697 = BgL_bindingsz00_1590; }  else 
{ 
 obj_t BgL_auxz00_5669;
BgL_auxz00_5669 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(18914L), BGl_string2974z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_bindingsz00_1590); 
FAILURE(BgL_auxz00_5669,BFALSE,BFALSE);} 
{ /* R5rs/syntax.scm 552 */
 obj_t BgL_aux2460z00_3386;
BgL_aux2460z00_3386 = 
CAR(BgL_pairz00_2697); 
if(
PAIRP(BgL_aux2460z00_3386))
{ /* R5rs/syntax.scm 552 */
BgL_pairz00_2698 = BgL_aux2460z00_3386; }  else 
{ 
 obj_t BgL_auxz00_5676;
BgL_auxz00_5676 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(18914L), BGl_string2974z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_aux2460z00_3386); 
FAILURE(BgL_auxz00_5676,BFALSE,BFALSE);} } } 
BgL_arg1866z00_1955 = 
CAR(BgL_pairz00_2698); } 
BgL_head1136z00_1942 = 
MAKE_YOUNG_PAIR(BgL_arg1866z00_1955, BNIL); } 
{ /* R5rs/syntax.scm 552 */
 obj_t BgL_g1139z00_1943;
{ /* R5rs/syntax.scm 552 */
 obj_t BgL_pairz00_2699;
if(
PAIRP(BgL_bindingsz00_1590))
{ /* R5rs/syntax.scm 552 */
BgL_pairz00_2699 = BgL_bindingsz00_1590; }  else 
{ 
 obj_t BgL_auxz00_5684;
BgL_auxz00_5684 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(18914L), BGl_string2974z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_bindingsz00_1590); 
FAILURE(BgL_auxz00_5684,BFALSE,BFALSE);} 
BgL_g1139z00_1943 = 
CDR(BgL_pairz00_2699); } 
{ 
 obj_t BgL_l1134z00_1945; obj_t BgL_tail1137z00_1946;
BgL_l1134z00_1945 = BgL_g1139z00_1943; 
BgL_tail1137z00_1946 = BgL_head1136z00_1942; 
BgL_zc3z04anonymousza31858ze3z87_1947:
if(
PAIRP(BgL_l1134z00_1945))
{ /* R5rs/syntax.scm 552 */
 obj_t BgL_newtail1138z00_1949;
{ /* R5rs/syntax.scm 552 */
 obj_t BgL_arg1862z00_1951;
{ /* R5rs/syntax.scm 552 */
 obj_t BgL_pairz00_2701;
{ /* R5rs/syntax.scm 552 */
 obj_t BgL_aux2464z00_3390;
BgL_aux2464z00_3390 = 
CAR(BgL_l1134z00_1945); 
if(
PAIRP(BgL_aux2464z00_3390))
{ /* R5rs/syntax.scm 552 */
BgL_pairz00_2701 = BgL_aux2464z00_3390; }  else 
{ 
 obj_t BgL_auxz00_5694;
BgL_auxz00_5694 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(18914L), BGl_string2975z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_aux2464z00_3390); 
FAILURE(BgL_auxz00_5694,BFALSE,BFALSE);} } 
BgL_arg1862z00_1951 = 
CAR(BgL_pairz00_2701); } 
BgL_newtail1138z00_1949 = 
MAKE_YOUNG_PAIR(BgL_arg1862z00_1951, BNIL); } 
SET_CDR(BgL_tail1137z00_1946, BgL_newtail1138z00_1949); 
{ 
 obj_t BgL_tail1137z00_5703; obj_t BgL_l1134z00_5701;
BgL_l1134z00_5701 = 
CDR(BgL_l1134z00_1945); 
BgL_tail1137z00_5703 = BgL_newtail1138z00_1949; 
BgL_tail1137z00_1946 = BgL_tail1137z00_5703; 
BgL_l1134z00_1945 = BgL_l1134z00_5701; 
goto BgL_zc3z04anonymousza31858ze3z87_1947;} }  else 
{ /* R5rs/syntax.scm 552 */
if(
NULLP(BgL_l1134z00_1945))
{ /* R5rs/syntax.scm 552 */
BgL_arg1856z00_1939 = BgL_head1136z00_1942; }  else 
{ /* R5rs/syntax.scm 552 */
BgL_arg1856z00_1939 = 
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BGl_string2912z00zz__r5_macro_4_3_syntaxz00, BGl_string2913z00zz__r5_macro_4_3_syntaxz00, BgL_l1134z00_1945, BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(18914L)); } } } } } 
BgL_nvarsz00_1913 = 
BGl_genvarsz00zz__r5_macro_4_3_syntaxz00(BgL_arg1856z00_1939); } 
{ /* R5rs/syntax.scm 552 */
 obj_t BgL_nenvz00_1914;
{ /* R5rs/syntax.scm 554 */
 obj_t BgL_arg1849z00_1930;
{ /* R5rs/syntax.scm 553 */
 obj_t BgL_list1851z00_1932;
{ /* R5rs/syntax.scm 553 */
 obj_t BgL_arg1852z00_1933;
BgL_arg1852z00_1933 = 
MAKE_YOUNG_PAIR(BgL_nvarsz00_1913, BNIL); 
BgL_list1851z00_1932 = 
MAKE_YOUNG_PAIR(BgL_bindingsz00_1590, BgL_arg1852z00_1933); } 
BgL_arg1849z00_1930 = 
BGl_mapz00zz__r4_control_features_6_9z00(BGl_proc2976z00zz__r5_macro_4_3_syntaxz00, BgL_list1851z00_1932); } 
BgL_nenvz00_1914 = 
BGl_appendzd221011zd2zz__r5_macro_4_3_syntaxz00(BgL_arg1849z00_1930, BgL_envz00_44); } 
{ /* R5rs/syntax.scm 553 */

{ /* R5rs/syntax.scm 558 */
 obj_t BgL_arg1837z00_1915;
{ /* R5rs/syntax.scm 558 */
 obj_t BgL_arg1838z00_1916; obj_t BgL_arg1839z00_1917;
{ /* R5rs/syntax.scm 558 */
 obj_t BgL_zc3z04anonymousza31843ze3z87_3153;
BgL_zc3z04anonymousza31843ze3z87_3153 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31843ze3ze5zz__r5_macro_4_3_syntaxz00, 
(int)(2L), 
(int)(1L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31843ze3z87_3153, 
(int)(0L), BgL_envz00_44); 
{ /* R5rs/syntax.scm 557 */
 obj_t BgL_list1841z00_1919;
{ /* R5rs/syntax.scm 557 */
 obj_t BgL_arg1842z00_1920;
BgL_arg1842z00_1920 = 
MAKE_YOUNG_PAIR(BgL_nvarsz00_1913, BNIL); 
BgL_list1841z00_1919 = 
MAKE_YOUNG_PAIR(BgL_bindingsz00_1590, BgL_arg1842z00_1920); } 
BgL_arg1838z00_1916 = 
BGl_mapz00zz__r4_control_features_6_9z00(BgL_zc3z04anonymousza31843ze3z87_3153, BgL_list1841z00_1919); } } 
{ /* R5rs/syntax.scm 560 */
 obj_t BgL_arg1848z00_1929;
BgL_arg1848z00_1929 = 
BGl_loopze70ze7zz__r5_macro_4_3_syntaxz00(BgL_nenvz00_1914, BgL_bodyz00_1591); 
{ /* R5rs/syntax.scm 557 */
 obj_t BgL_auxz00_5722;
{ /* R5rs/syntax.scm 557 */
 bool_t BgL_test3410z00_5723;
if(
PAIRP(BgL_arg1848z00_1929))
{ /* R5rs/syntax.scm 557 */
BgL_test3410z00_5723 = ((bool_t)1)
; }  else 
{ /* R5rs/syntax.scm 557 */
BgL_test3410z00_5723 = 
NULLP(BgL_arg1848z00_1929)
; } 
if(BgL_test3410z00_5723)
{ /* R5rs/syntax.scm 557 */
BgL_auxz00_5722 = BgL_arg1848z00_1929
; }  else 
{ 
 obj_t BgL_auxz00_5727;
BgL_auxz00_5727 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(19040L), BGl_string2974z00zz__r5_macro_4_3_syntaxz00, BGl_string2650z00zz__r5_macro_4_3_syntaxz00, BgL_arg1848z00_1929); 
FAILURE(BgL_auxz00_5727,BFALSE,BFALSE);} } 
BgL_arg1839z00_1917 = 
BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_5722, BNIL); } } 
BgL_arg1837z00_1915 = 
MAKE_YOUNG_PAIR(BgL_arg1838z00_1916, BgL_arg1839z00_1917); } 
return 
MAKE_YOUNG_PAIR(BGl_symbol2685z00zz__r5_macro_4_3_syntaxz00, BgL_arg1837z00_1915);} } } } } } }  else 
{ /* R5rs/syntax.scm 524 */
 obj_t BgL_cdrzd2349zd2_1629;
BgL_cdrzd2349zd2_1629 = 
CDR(
((obj_t)BgL_xz00_43)); 
{ /* R5rs/syntax.scm 524 */
 obj_t BgL_arg1571z00_1630; obj_t BgL_arg1573z00_1631;
{ /* R5rs/syntax.scm 524 */
 obj_t BgL_pairz00_2762;
if(
PAIRP(BgL_cdrzd2349zd2_1629))
{ /* R5rs/syntax.scm 524 */
BgL_pairz00_2762 = BgL_cdrzd2349zd2_1629; }  else 
{ 
 obj_t BgL_auxz00_5738;
BgL_auxz00_5738 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(18015L), BGl_string2983z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_cdrzd2349zd2_1629); 
FAILURE(BgL_auxz00_5738,BFALSE,BFALSE);} 
BgL_arg1571z00_1630 = 
CAR(BgL_pairz00_2762); } 
{ /* R5rs/syntax.scm 524 */
 obj_t BgL_pairz00_2763;
if(
PAIRP(BgL_cdrzd2349zd2_1629))
{ /* R5rs/syntax.scm 524 */
BgL_pairz00_2763 = BgL_cdrzd2349zd2_1629; }  else 
{ 
 obj_t BgL_auxz00_5745;
BgL_auxz00_5745 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(18015L), BGl_string2983z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_cdrzd2349zd2_1629); 
FAILURE(BgL_auxz00_5745,BFALSE,BFALSE);} 
BgL_arg1573z00_1631 = 
CDR(BgL_pairz00_2763); } 
{ 
 obj_t BgL_bodyz00_5751; obj_t BgL_bindingsz00_5750;
BgL_bindingsz00_5750 = BgL_arg1571z00_1630; 
BgL_bodyz00_5751 = BgL_arg1573z00_1631; 
BgL_bodyz00_1591 = BgL_bodyz00_5751; 
BgL_bindingsz00_1590 = BgL_bindingsz00_5750; 
goto BgL_tagzd2238zd2_1592;} } } }  else 
{ /* R5rs/syntax.scm 524 */
BGL_TAIL return 
BGl_loopze70ze7zz__r5_macro_4_3_syntaxz00(BgL_envz00_44, BgL_xz00_43);} }  else 
{ /* R5rs/syntax.scm 524 */
 obj_t BgL_cdrzd2388zd2_1632;
BgL_cdrzd2388zd2_1632 = 
CDR(
((obj_t)BgL_xz00_43)); 
if(
(
CAR(
((obj_t)BgL_xz00_43))==BGl_symbol2783z00zz__r5_macro_4_3_syntaxz00))
{ /* R5rs/syntax.scm 524 */
if(
PAIRP(BgL_cdrzd2388zd2_1632))
{ /* R5rs/syntax.scm 524 */
BgL_bindingsz00_1593 = 
CAR(BgL_cdrzd2388zd2_1632); 
BgL_bodyz00_1594 = 
CDR(BgL_cdrzd2388zd2_1632); 
{ 
 obj_t BgL_bindingsz00_1959; obj_t BgL_nbindingsz00_1960; obj_t BgL_nenvz00_1961;
BgL_bindingsz00_1959 = BgL_bindingsz00_1593; 
BgL_nbindingsz00_1960 = BNIL; 
BgL_nenvz00_1961 = BgL_envz00_44; 
BgL_zc3z04anonymousza31869ze3z87_1962:
if(
NULLP(BgL_bindingsz00_1959))
{ /* R5rs/syntax.scm 566 */
 obj_t BgL_arg1872z00_1964;
{ /* R5rs/syntax.scm 566 */
 obj_t BgL_arg1873z00_1965; obj_t BgL_arg1874z00_1966;
BgL_arg1873z00_1965 = 
bgl_reverse(BgL_nbindingsz00_1960); 
{ /* R5rs/syntax.scm 566 */
 obj_t BgL_arg1875z00_1967;
BgL_arg1875z00_1967 = 
BGl_loopze70ze7zz__r5_macro_4_3_syntaxz00(BgL_nenvz00_1961, BgL_bodyz00_1594); 
{ /* R5rs/syntax.scm 566 */
 obj_t BgL_auxz00_5765;
{ /* R5rs/syntax.scm 566 */
 bool_t BgL_test3417z00_5766;
if(
PAIRP(BgL_arg1875z00_1967))
{ /* R5rs/syntax.scm 566 */
BgL_test3417z00_5766 = ((bool_t)1)
; }  else 
{ /* R5rs/syntax.scm 566 */
BgL_test3417z00_5766 = 
NULLP(BgL_arg1875z00_1967)
; } 
if(BgL_test3417z00_5766)
{ /* R5rs/syntax.scm 566 */
BgL_auxz00_5765 = BgL_arg1875z00_1967
; }  else 
{ 
 obj_t BgL_auxz00_5770;
BgL_auxz00_5770 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(19301L), BGl_string2869z00zz__r5_macro_4_3_syntaxz00, BGl_string2650z00zz__r5_macro_4_3_syntaxz00, BgL_arg1875z00_1967); 
FAILURE(BgL_auxz00_5770,BFALSE,BFALSE);} } 
BgL_arg1874z00_1966 = 
BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_5765, BNIL); } } 
BgL_arg1872z00_1964 = 
MAKE_YOUNG_PAIR(BgL_arg1873z00_1965, BgL_arg1874z00_1966); } 
return 
MAKE_YOUNG_PAIR(BGl_symbol2783z00zz__r5_macro_4_3_syntaxz00, BgL_arg1872z00_1964);}  else 
{ /* R5rs/syntax.scm 567 */
 obj_t BgL_varz00_1968;
{ /* R5rs/syntax.scm 567 */
 obj_t BgL_pairz00_2715;
if(
PAIRP(BgL_bindingsz00_1959))
{ /* R5rs/syntax.scm 567 */
BgL_pairz00_2715 = BgL_bindingsz00_1959; }  else 
{ 
 obj_t BgL_auxz00_5779;
BgL_auxz00_5779 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(19379L), BGl_string2869z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_bindingsz00_1959); 
FAILURE(BgL_auxz00_5779,BFALSE,BFALSE);} 
{ /* R5rs/syntax.scm 567 */
 obj_t BgL_pairz00_2718;
{ /* R5rs/syntax.scm 567 */
 obj_t BgL_aux2472z00_3398;
BgL_aux2472z00_3398 = 
CAR(BgL_pairz00_2715); 
if(
PAIRP(BgL_aux2472z00_3398))
{ /* R5rs/syntax.scm 567 */
BgL_pairz00_2718 = BgL_aux2472z00_3398; }  else 
{ 
 obj_t BgL_auxz00_5786;
BgL_auxz00_5786 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(19373L), BGl_string2869z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_aux2472z00_3398); 
FAILURE(BgL_auxz00_5786,BFALSE,BFALSE);} } 
BgL_varz00_1968 = 
CAR(BgL_pairz00_2718); } } 
{ /* R5rs/syntax.scm 568 */
 obj_t BgL_nvarz00_1970;
BgL_nvarz00_1970 = 
BGl_genvarsz00zz__r5_macro_4_3_syntaxz00(BgL_varz00_1968); 
{ /* R5rs/syntax.scm 569 */
 obj_t BgL_nenvz00_1971;
{ /* R5rs/syntax.scm 570 */
 obj_t BgL_arg1883z00_1978;
BgL_arg1883z00_1978 = 
MAKE_YOUNG_PAIR(BgL_varz00_1968, BgL_nvarz00_1970); 
BgL_nenvz00_1971 = 
MAKE_YOUNG_PAIR(BgL_arg1883z00_1978, BgL_envz00_44); } 
{ /* R5rs/syntax.scm 570 */

{ /* R5rs/syntax.scm 571 */
 obj_t BgL_arg1876z00_1972; obj_t BgL_arg1877z00_1973;
{ /* R5rs/syntax.scm 571 */
 obj_t BgL_pairz00_2725;
if(
PAIRP(BgL_bindingsz00_1959))
{ /* R5rs/syntax.scm 571 */
BgL_pairz00_2725 = BgL_bindingsz00_1959; }  else 
{ 
 obj_t BgL_auxz00_5796;
BgL_auxz00_5796 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(19504L), BGl_string2869z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_bindingsz00_1959); 
FAILURE(BgL_auxz00_5796,BFALSE,BFALSE);} 
BgL_arg1876z00_1972 = 
CDR(BgL_pairz00_2725); } 
{ /* R5rs/syntax.scm 572 */
 obj_t BgL_arg1878z00_1974;
{ /* R5rs/syntax.scm 572 */
 obj_t BgL_arg1879z00_1975;
BgL_arg1879z00_1975 = 
BGl_hygieniza7eza7zz__r5_macro_4_3_syntaxz00(BgL_varz00_1968, BgL_envz00_44); 
{ /* R5rs/syntax.scm 572 */
 obj_t BgL_list1880z00_1976;
{ /* R5rs/syntax.scm 572 */
 obj_t BgL_arg1882z00_1977;
BgL_arg1882z00_1977 = 
MAKE_YOUNG_PAIR(BgL_arg1879z00_1975, BNIL); 
BgL_list1880z00_1976 = 
MAKE_YOUNG_PAIR(BgL_varz00_1968, BgL_arg1882z00_1977); } 
BgL_arg1878z00_1974 = BgL_list1880z00_1976; } } 
BgL_arg1877z00_1973 = 
MAKE_YOUNG_PAIR(BgL_arg1878z00_1974, BgL_nbindingsz00_1960); } 
{ 
 obj_t BgL_nenvz00_5807; obj_t BgL_nbindingsz00_5806; obj_t BgL_bindingsz00_5805;
BgL_bindingsz00_5805 = BgL_arg1876z00_1972; 
BgL_nbindingsz00_5806 = BgL_arg1877z00_1973; 
BgL_nenvz00_5807 = BgL_nenvz00_1971; 
BgL_nenvz00_1961 = BgL_nenvz00_5807; 
BgL_nbindingsz00_1960 = BgL_nbindingsz00_5806; 
BgL_bindingsz00_1959 = BgL_bindingsz00_5805; 
goto BgL_zc3z04anonymousza31869ze3z87_1962;} } } } } } } }  else 
{ /* R5rs/syntax.scm 524 */
BGL_TAIL return 
BGl_loopze70ze7zz__r5_macro_4_3_syntaxz00(BgL_envz00_44, BgL_xz00_43);} }  else 
{ /* R5rs/syntax.scm 524 */
 obj_t BgL_cdrzd2410zd2_1638;
BgL_cdrzd2410zd2_1638 = 
CDR(
((obj_t)BgL_xz00_43)); 
if(
(
CAR(
((obj_t)BgL_xz00_43))==BGl_symbol2779z00zz__r5_macro_4_3_syntaxz00))
{ /* R5rs/syntax.scm 524 */
if(
PAIRP(BgL_cdrzd2410zd2_1638))
{ /* R5rs/syntax.scm 524 */
BgL_bindingsz00_1596 = 
CAR(BgL_cdrzd2410zd2_1638); 
BgL_bodyz00_1597 = 
CDR(BgL_cdrzd2410zd2_1638); 
{ /* R5rs/syntax.scm 575 */
 obj_t BgL_nvarsz00_1980;
{ /* R5rs/syntax.scm 575 */
 obj_t BgL_arg1904z00_2006;
if(
NULLP(BgL_bindingsz00_1596))
{ /* R5rs/syntax.scm 575 */
BgL_arg1904z00_2006 = BNIL; }  else 
{ /* R5rs/syntax.scm 575 */
 obj_t BgL_head1142z00_2009;
{ /* R5rs/syntax.scm 575 */
 obj_t BgL_arg1914z00_2022;
{ /* R5rs/syntax.scm 575 */
 obj_t BgL_pairz00_2728;
{ /* R5rs/syntax.scm 575 */
 obj_t BgL_pairz00_2727;
if(
PAIRP(BgL_bindingsz00_1596))
{ /* R5rs/syntax.scm 575 */
BgL_pairz00_2727 = BgL_bindingsz00_1596; }  else 
{ 
 obj_t BgL_auxz00_5823;
BgL_auxz00_5823 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(19654L), BGl_string2977z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_bindingsz00_1596); 
FAILURE(BgL_auxz00_5823,BFALSE,BFALSE);} 
{ /* R5rs/syntax.scm 575 */
 obj_t BgL_aux2478z00_3404;
BgL_aux2478z00_3404 = 
CAR(BgL_pairz00_2727); 
if(
PAIRP(BgL_aux2478z00_3404))
{ /* R5rs/syntax.scm 575 */
BgL_pairz00_2728 = BgL_aux2478z00_3404; }  else 
{ 
 obj_t BgL_auxz00_5830;
BgL_auxz00_5830 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(19654L), BGl_string2977z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_aux2478z00_3404); 
FAILURE(BgL_auxz00_5830,BFALSE,BFALSE);} } } 
BgL_arg1914z00_2022 = 
CAR(BgL_pairz00_2728); } 
BgL_head1142z00_2009 = 
MAKE_YOUNG_PAIR(BgL_arg1914z00_2022, BNIL); } 
{ /* R5rs/syntax.scm 575 */
 obj_t BgL_g1145z00_2010;
{ /* R5rs/syntax.scm 575 */
 obj_t BgL_pairz00_2729;
if(
PAIRP(BgL_bindingsz00_1596))
{ /* R5rs/syntax.scm 575 */
BgL_pairz00_2729 = BgL_bindingsz00_1596; }  else 
{ 
 obj_t BgL_auxz00_5838;
BgL_auxz00_5838 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(19654L), BGl_string2977z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_bindingsz00_1596); 
FAILURE(BgL_auxz00_5838,BFALSE,BFALSE);} 
BgL_g1145z00_2010 = 
CDR(BgL_pairz00_2729); } 
{ 
 obj_t BgL_l1140z00_2012; obj_t BgL_tail1143z00_2013;
BgL_l1140z00_2012 = BgL_g1145z00_2010; 
BgL_tail1143z00_2013 = BgL_head1142z00_2009; 
BgL_zc3z04anonymousza31906ze3z87_2014:
if(
PAIRP(BgL_l1140z00_2012))
{ /* R5rs/syntax.scm 575 */
 obj_t BgL_newtail1144z00_2016;
{ /* R5rs/syntax.scm 575 */
 obj_t BgL_arg1911z00_2018;
{ /* R5rs/syntax.scm 575 */
 obj_t BgL_pairz00_2731;
{ /* R5rs/syntax.scm 575 */
 obj_t BgL_aux2482z00_3408;
BgL_aux2482z00_3408 = 
CAR(BgL_l1140z00_2012); 
if(
PAIRP(BgL_aux2482z00_3408))
{ /* R5rs/syntax.scm 575 */
BgL_pairz00_2731 = BgL_aux2482z00_3408; }  else 
{ 
 obj_t BgL_auxz00_5848;
BgL_auxz00_5848 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(19654L), BGl_string2978z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_aux2482z00_3408); 
FAILURE(BgL_auxz00_5848,BFALSE,BFALSE);} } 
BgL_arg1911z00_2018 = 
CAR(BgL_pairz00_2731); } 
BgL_newtail1144z00_2016 = 
MAKE_YOUNG_PAIR(BgL_arg1911z00_2018, BNIL); } 
SET_CDR(BgL_tail1143z00_2013, BgL_newtail1144z00_2016); 
{ 
 obj_t BgL_tail1143z00_5857; obj_t BgL_l1140z00_5855;
BgL_l1140z00_5855 = 
CDR(BgL_l1140z00_2012); 
BgL_tail1143z00_5857 = BgL_newtail1144z00_2016; 
BgL_tail1143z00_2013 = BgL_tail1143z00_5857; 
BgL_l1140z00_2012 = BgL_l1140z00_5855; 
goto BgL_zc3z04anonymousza31906ze3z87_2014;} }  else 
{ /* R5rs/syntax.scm 575 */
if(
NULLP(BgL_l1140z00_2012))
{ /* R5rs/syntax.scm 575 */
BgL_arg1904z00_2006 = BgL_head1142z00_2009; }  else 
{ /* R5rs/syntax.scm 575 */
BgL_arg1904z00_2006 = 
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BGl_string2912z00zz__r5_macro_4_3_syntaxz00, BGl_string2913z00zz__r5_macro_4_3_syntaxz00, BgL_l1140z00_2012, BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(19654L)); } } } } } 
BgL_nvarsz00_1980 = 
BGl_genvarsz00zz__r5_macro_4_3_syntaxz00(BgL_arg1904z00_2006); } 
{ /* R5rs/syntax.scm 575 */
 obj_t BgL_nenvz00_1981;
{ /* R5rs/syntax.scm 577 */
 obj_t BgL_arg1898z00_1997;
{ /* R5rs/syntax.scm 576 */
 obj_t BgL_list1900z00_1999;
{ /* R5rs/syntax.scm 576 */
 obj_t BgL_arg1901z00_2000;
BgL_arg1901z00_2000 = 
MAKE_YOUNG_PAIR(BgL_nvarsz00_1980, BNIL); 
BgL_list1900z00_1999 = 
MAKE_YOUNG_PAIR(BgL_bindingsz00_1596, BgL_arg1901z00_2000); } 
BgL_arg1898z00_1997 = 
BGl_mapz00zz__r4_control_features_6_9z00(BGl_proc2979z00zz__r5_macro_4_3_syntaxz00, BgL_list1900z00_1999); } 
BgL_nenvz00_1981 = 
BGl_appendzd221011zd2zz__r5_macro_4_3_syntaxz00(BgL_arg1898z00_1997, BgL_envz00_44); } 
{ /* R5rs/syntax.scm 576 */

{ /* R5rs/syntax.scm 581 */
 obj_t BgL_arg1884z00_1982;
{ /* R5rs/syntax.scm 581 */
 obj_t BgL_arg1885z00_1983; obj_t BgL_arg1887z00_1984;
{ /* R5rs/syntax.scm 581 */
 obj_t BgL_zc3z04anonymousza31891ze3z87_3151;
BgL_zc3z04anonymousza31891ze3z87_3151 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31891ze3ze5zz__r5_macro_4_3_syntaxz00, 
(int)(2L), 
(int)(1L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31891ze3z87_3151, 
(int)(0L), BgL_nenvz00_1981); 
{ /* R5rs/syntax.scm 580 */
 obj_t BgL_list1889z00_1986;
{ /* R5rs/syntax.scm 580 */
 obj_t BgL_arg1890z00_1987;
BgL_arg1890z00_1987 = 
MAKE_YOUNG_PAIR(BgL_nvarsz00_1980, BNIL); 
BgL_list1889z00_1986 = 
MAKE_YOUNG_PAIR(BgL_bindingsz00_1596, BgL_arg1890z00_1987); } 
BgL_arg1885z00_1983 = 
BGl_mapz00zz__r4_control_features_6_9z00(BgL_zc3z04anonymousza31891ze3z87_3151, BgL_list1889z00_1986); } } 
{ /* R5rs/syntax.scm 583 */
 obj_t BgL_arg1897z00_1996;
BgL_arg1897z00_1996 = 
BGl_loopze70ze7zz__r5_macro_4_3_syntaxz00(BgL_nenvz00_1981, BgL_bodyz00_1597); 
{ /* R5rs/syntax.scm 580 */
 obj_t BgL_auxz00_5876;
{ /* R5rs/syntax.scm 580 */
 bool_t BgL_test3431z00_5877;
if(
PAIRP(BgL_arg1897z00_1996))
{ /* R5rs/syntax.scm 580 */
BgL_test3431z00_5877 = ((bool_t)1)
; }  else 
{ /* R5rs/syntax.scm 580 */
BgL_test3431z00_5877 = 
NULLP(BgL_arg1897z00_1996)
; } 
if(BgL_test3431z00_5877)
{ /* R5rs/syntax.scm 580 */
BgL_auxz00_5876 = BgL_arg1897z00_1996
; }  else 
{ 
 obj_t BgL_auxz00_5881;
BgL_auxz00_5881 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(19780L), BGl_string2977z00zz__r5_macro_4_3_syntaxz00, BGl_string2650z00zz__r5_macro_4_3_syntaxz00, BgL_arg1897z00_1996); 
FAILURE(BgL_auxz00_5881,BFALSE,BFALSE);} } 
BgL_arg1887z00_1984 = 
BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_5876, BNIL); } } 
BgL_arg1884z00_1982 = 
MAKE_YOUNG_PAIR(BgL_arg1885z00_1983, BgL_arg1887z00_1984); } 
return 
MAKE_YOUNG_PAIR(BGl_symbol2779z00zz__r5_macro_4_3_syntaxz00, BgL_arg1884z00_1982);} } } } }  else 
{ /* R5rs/syntax.scm 524 */
BGL_TAIL return 
BGl_loopze70ze7zz__r5_macro_4_3_syntaxz00(BgL_envz00_44, BgL_xz00_43);} }  else 
{ /* R5rs/syntax.scm 524 */
 obj_t BgL_cdrzd2426zd2_1644;
BgL_cdrzd2426zd2_1644 = 
CDR(
((obj_t)BgL_xz00_43)); 
if(
(
CAR(
((obj_t)BgL_xz00_43))==BGl_symbol2981z00zz__r5_macro_4_3_syntaxz00))
{ /* R5rs/syntax.scm 524 */
if(
PAIRP(BgL_cdrzd2426zd2_1644))
{ /* R5rs/syntax.scm 524 */
 obj_t BgL_carzd2429zd2_1648;
BgL_carzd2429zd2_1648 = 
CAR(BgL_cdrzd2426zd2_1644); 
if(
PAIRP(BgL_carzd2429zd2_1648))
{ /* R5rs/syntax.scm 524 */
if(
NULLP(
CDR(BgL_carzd2429zd2_1648)))
{ /* R5rs/syntax.scm 524 */
BgL_varz00_1599 = 
CAR(BgL_carzd2429zd2_1648); 
BgL_bodyz00_1600 = 
CDR(BgL_cdrzd2426zd2_1644); 
{ /* R5rs/syntax.scm 585 */
 obj_t BgL_nvarz00_2024;
BgL_nvarz00_2024 = 
BGl_genvarsz00zz__r5_macro_4_3_syntaxz00(BgL_varz00_1599); 
{ /* R5rs/syntax.scm 585 */
 obj_t BgL_nenvz00_2025;
{ /* R5rs/syntax.scm 586 */
 obj_t BgL_arg1923z00_2030;
BgL_arg1923z00_2030 = 
MAKE_YOUNG_PAIR(BgL_varz00_1599, BgL_nvarz00_2024); 
BgL_nenvz00_2025 = 
MAKE_YOUNG_PAIR(BgL_arg1923z00_2030, BgL_envz00_44); } 
{ /* R5rs/syntax.scm 586 */

{ /* R5rs/syntax.scm 587 */
 obj_t BgL_arg1917z00_2026;
{ /* R5rs/syntax.scm 587 */
 obj_t BgL_arg1918z00_2027; obj_t BgL_arg1919z00_2028;
BgL_arg1918z00_2027 = 
MAKE_YOUNG_PAIR(BgL_nvarz00_2024, BNIL); 
{ /* R5rs/syntax.scm 587 */
 obj_t BgL_arg1920z00_2029;
BgL_arg1920z00_2029 = 
BGl_loopze70ze7zz__r5_macro_4_3_syntaxz00(BgL_nenvz00_2025, BgL_bodyz00_1600); 
{ /* R5rs/syntax.scm 587 */
 obj_t BgL_auxz00_5910;
{ /* R5rs/syntax.scm 587 */
 bool_t BgL_test3437z00_5911;
if(
PAIRP(BgL_arg1920z00_2029))
{ /* R5rs/syntax.scm 587 */
BgL_test3437z00_5911 = ((bool_t)1)
; }  else 
{ /* R5rs/syntax.scm 587 */
BgL_test3437z00_5911 = 
NULLP(BgL_arg1920z00_2029)
; } 
if(BgL_test3437z00_5911)
{ /* R5rs/syntax.scm 587 */
BgL_auxz00_5910 = BgL_arg1920z00_2029
; }  else 
{ 
 obj_t BgL_auxz00_5915;
BgL_auxz00_5915 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(20021L), BGl_string2980z00zz__r5_macro_4_3_syntaxz00, BGl_string2650z00zz__r5_macro_4_3_syntaxz00, BgL_arg1920z00_2029); 
FAILURE(BgL_auxz00_5915,BFALSE,BFALSE);} } 
BgL_arg1919z00_2028 = 
BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_5910, BNIL); } } 
BgL_arg1917z00_2026 = 
MAKE_YOUNG_PAIR(BgL_arg1918z00_2027, BgL_arg1919z00_2028); } 
return 
MAKE_YOUNG_PAIR(BGl_symbol2981z00zz__r5_macro_4_3_syntaxz00, BgL_arg1917z00_2026);} } } } }  else 
{ /* R5rs/syntax.scm 524 */
BGL_TAIL return 
BGl_loopze70ze7zz__r5_macro_4_3_syntaxz00(BgL_envz00_44, BgL_xz00_43);} }  else 
{ /* R5rs/syntax.scm 524 */
BGL_TAIL return 
BGl_loopze70ze7zz__r5_macro_4_3_syntaxz00(BgL_envz00_44, BgL_xz00_43);} }  else 
{ /* R5rs/syntax.scm 524 */
BGL_TAIL return 
BGl_loopze70ze7zz__r5_macro_4_3_syntaxz00(BgL_envz00_44, BgL_xz00_43);} }  else 
{ /* R5rs/syntax.scm 524 */
BGL_TAIL return 
BGl_loopze70ze7zz__r5_macro_4_3_syntaxz00(BgL_envz00_44, BgL_xz00_43);} } } } } } }  else 
{ /* R5rs/syntax.scm 524 */
return BgL_xz00_43;} } } } 

}



/* &<@anonymous:1891> */
obj_t BGl_z62zc3z04anonymousza31891ze3ze5zz__r5_macro_4_3_syntaxz00(obj_t BgL_envz00_3157, obj_t BgL_bz00_3159, obj_t BgL_vz00_3160)
{
{ /* R5rs/syntax.scm 580 */
{ /* R5rs/syntax.scm 581 */
 obj_t BgL_nenvz00_3158;
BgL_nenvz00_3158 = 
PROCEDURE_REF(BgL_envz00_3157, 
(int)(0L)); 
{ /* R5rs/syntax.scm 581 */
 obj_t BgL_res2992z00_3691;
{ /* R5rs/syntax.scm 581 */
 obj_t BgL_arg1892z00_3684;
{ /* R5rs/syntax.scm 581 */
 obj_t BgL_arg1896z00_3685;
{ /* R5rs/syntax.scm 581 */
 obj_t BgL_pairz00_3686;
if(
PAIRP(BgL_bz00_3159))
{ /* R5rs/syntax.scm 581 */
BgL_pairz00_3686 = BgL_bz00_3159; }  else 
{ 
 obj_t BgL_auxz00_5932;
BgL_auxz00_5932 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(19840L), BGl_string2984z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_bz00_3159); 
FAILURE(BgL_auxz00_5932,BFALSE,BFALSE);} 
{ /* R5rs/syntax.scm 581 */
 obj_t BgL_pairz00_3687;
{ /* R5rs/syntax.scm 581 */
 obj_t BgL_aux2626z00_3688;
BgL_aux2626z00_3688 = 
CDR(BgL_pairz00_3686); 
if(
PAIRP(BgL_aux2626z00_3688))
{ /* R5rs/syntax.scm 581 */
BgL_pairz00_3687 = BgL_aux2626z00_3688; }  else 
{ 
 obj_t BgL_auxz00_5939;
BgL_auxz00_5939 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(19834L), BGl_string2984z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_aux2626z00_3688); 
FAILURE(BgL_auxz00_5939,BFALSE,BFALSE);} } 
BgL_arg1896z00_3685 = 
CAR(BgL_pairz00_3687); } } 
BgL_arg1892z00_3684 = 
BGl_hygieniza7eza7zz__r5_macro_4_3_syntaxz00(BgL_arg1896z00_3685, BgL_nenvz00_3158); } 
{ /* R5rs/syntax.scm 581 */
 obj_t BgL_list1893z00_3689;
{ /* R5rs/syntax.scm 581 */
 obj_t BgL_arg1894z00_3690;
BgL_arg1894z00_3690 = 
MAKE_YOUNG_PAIR(BgL_arg1892z00_3684, BNIL); 
BgL_list1893z00_3689 = 
MAKE_YOUNG_PAIR(BgL_vz00_3160, BgL_arg1894z00_3690); } 
BgL_res2992z00_3691 = BgL_list1893z00_3689; } } 
return BgL_res2992z00_3691;} } } 

}



/* &<@anonymous:1902> */
obj_t BGl_z62zc3z04anonymousza31902ze3ze5zz__r5_macro_4_3_syntaxz00(obj_t BgL_envz00_3161, obj_t BgL_bz00_3162, obj_t BgL_vz00_3163)
{
{ /* R5rs/syntax.scm 576 */
{ /* R5rs/syntax.scm 577 */
 obj_t BgL_arg1903z00_3692;
{ /* R5rs/syntax.scm 577 */
 obj_t BgL_pairz00_3693;
if(
PAIRP(BgL_bz00_3162))
{ /* R5rs/syntax.scm 577 */
BgL_pairz00_3693 = BgL_bz00_3162; }  else 
{ 
 obj_t BgL_auxz00_5949;
BgL_auxz00_5949 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(19734L), BGl_string2985z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_bz00_3162); 
FAILURE(BgL_auxz00_5949,BFALSE,BFALSE);} 
BgL_arg1903z00_3692 = 
CAR(BgL_pairz00_3693); } 
return 
MAKE_YOUNG_PAIR(BgL_arg1903z00_3692, BgL_vz00_3163);} } 

}



/* &<@anonymous:1843> */
obj_t BGl_z62zc3z04anonymousza31843ze3ze5zz__r5_macro_4_3_syntaxz00(obj_t BgL_envz00_3164, obj_t BgL_bz00_3166, obj_t BgL_vz00_3167)
{
{ /* R5rs/syntax.scm 557 */
{ /* R5rs/syntax.scm 558 */
 obj_t BgL_envz00_3165;
BgL_envz00_3165 = 
PROCEDURE_REF(BgL_envz00_3164, 
(int)(0L)); 
{ /* R5rs/syntax.scm 558 */
 obj_t BgL_res2993z00_3701;
{ /* R5rs/syntax.scm 558 */
 obj_t BgL_arg1844z00_3694;
{ /* R5rs/syntax.scm 558 */
 obj_t BgL_arg1847z00_3695;
{ /* R5rs/syntax.scm 558 */
 obj_t BgL_pairz00_3696;
if(
PAIRP(BgL_bz00_3166))
{ /* R5rs/syntax.scm 558 */
BgL_pairz00_3696 = BgL_bz00_3166; }  else 
{ 
 obj_t BgL_auxz00_5959;
BgL_auxz00_5959 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(19094L), BGl_string2986z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_bz00_3166); 
FAILURE(BgL_auxz00_5959,BFALSE,BFALSE);} 
{ /* R5rs/syntax.scm 558 */
 obj_t BgL_pairz00_3697;
{ /* R5rs/syntax.scm 558 */
 obj_t BgL_aux2632z00_3698;
BgL_aux2632z00_3698 = 
CDR(BgL_pairz00_3696); 
if(
PAIRP(BgL_aux2632z00_3698))
{ /* R5rs/syntax.scm 558 */
BgL_pairz00_3697 = BgL_aux2632z00_3698; }  else 
{ 
 obj_t BgL_auxz00_5966;
BgL_auxz00_5966 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(19088L), BGl_string2986z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_aux2632z00_3698); 
FAILURE(BgL_auxz00_5966,BFALSE,BFALSE);} } 
BgL_arg1847z00_3695 = 
CAR(BgL_pairz00_3697); } } 
BgL_arg1844z00_3694 = 
BGl_hygieniza7eza7zz__r5_macro_4_3_syntaxz00(BgL_arg1847z00_3695, BgL_envz00_3165); } 
{ /* R5rs/syntax.scm 558 */
 obj_t BgL_list1845z00_3699;
{ /* R5rs/syntax.scm 558 */
 obj_t BgL_arg1846z00_3700;
BgL_arg1846z00_3700 = 
MAKE_YOUNG_PAIR(BgL_arg1844z00_3694, BNIL); 
BgL_list1845z00_3699 = 
MAKE_YOUNG_PAIR(BgL_vz00_3167, BgL_arg1846z00_3700); } 
BgL_res2993z00_3701 = BgL_list1845z00_3699; } } 
return BgL_res2993z00_3701;} } } 

}



/* &<@anonymous:1853> */
obj_t BGl_z62zc3z04anonymousza31853ze3ze5zz__r5_macro_4_3_syntaxz00(obj_t BgL_envz00_3168, obj_t BgL_bz00_3169, obj_t BgL_vz00_3170)
{
{ /* R5rs/syntax.scm 553 */
{ /* R5rs/syntax.scm 554 */
 obj_t BgL_arg1854z00_3702;
{ /* R5rs/syntax.scm 554 */
 obj_t BgL_pairz00_3703;
if(
PAIRP(BgL_bz00_3169))
{ /* R5rs/syntax.scm 554 */
BgL_pairz00_3703 = BgL_bz00_3169; }  else 
{ 
 obj_t BgL_auxz00_5976;
BgL_auxz00_5976 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(18994L), BGl_string2987z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_bz00_3169); 
FAILURE(BgL_auxz00_5976,BFALSE,BFALSE);} 
BgL_arg1854z00_3702 = 
CAR(BgL_pairz00_3703); } 
return 
MAKE_YOUNG_PAIR(BgL_arg1854z00_3702, BgL_vz00_3170);} } 

}



/* &<@anonymous:1810> */
obj_t BGl_z62zc3z04anonymousza31810ze3ze5zz__r5_macro_4_3_syntaxz00(obj_t BgL_envz00_3171, obj_t BgL_bz00_3173, obj_t BgL_vz00_3174)
{
{ /* R5rs/syntax.scm 547 */
{ /* R5rs/syntax.scm 548 */
 obj_t BgL_envz00_3172;
BgL_envz00_3172 = 
PROCEDURE_REF(BgL_envz00_3171, 
(int)(0L)); 
{ /* R5rs/syntax.scm 548 */
 obj_t BgL_res2994z00_3711;
{ /* R5rs/syntax.scm 548 */
 obj_t BgL_arg1811z00_3704;
{ /* R5rs/syntax.scm 548 */
 obj_t BgL_arg1814z00_3705;
{ /* R5rs/syntax.scm 548 */
 obj_t BgL_pairz00_3706;
if(
PAIRP(BgL_bz00_3173))
{ /* R5rs/syntax.scm 548 */
BgL_pairz00_3706 = BgL_bz00_3173; }  else 
{ 
 obj_t BgL_auxz00_5986;
BgL_auxz00_5986 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(18789L), BGl_string2988z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_bz00_3173); 
FAILURE(BgL_auxz00_5986,BFALSE,BFALSE);} 
{ /* R5rs/syntax.scm 548 */
 obj_t BgL_pairz00_3707;
{ /* R5rs/syntax.scm 548 */
 obj_t BgL_aux2638z00_3708;
BgL_aux2638z00_3708 = 
CDR(BgL_pairz00_3706); 
if(
PAIRP(BgL_aux2638z00_3708))
{ /* R5rs/syntax.scm 548 */
BgL_pairz00_3707 = BgL_aux2638z00_3708; }  else 
{ 
 obj_t BgL_auxz00_5993;
BgL_auxz00_5993 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(18783L), BGl_string2988z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_aux2638z00_3708); 
FAILURE(BgL_auxz00_5993,BFALSE,BFALSE);} } 
BgL_arg1814z00_3705 = 
CAR(BgL_pairz00_3707); } } 
BgL_arg1811z00_3704 = 
BGl_hygieniza7eza7zz__r5_macro_4_3_syntaxz00(BgL_arg1814z00_3705, BgL_envz00_3172); } 
{ /* R5rs/syntax.scm 548 */
 obj_t BgL_list1812z00_3709;
{ /* R5rs/syntax.scm 548 */
 obj_t BgL_arg1813z00_3710;
BgL_arg1813z00_3710 = 
MAKE_YOUNG_PAIR(BgL_arg1811z00_3704, BNIL); 
BgL_list1812z00_3709 = 
MAKE_YOUNG_PAIR(BgL_vz00_3174, BgL_arg1813z00_3710); } 
BgL_res2994z00_3711 = BgL_list1812z00_3709; } } 
return BgL_res2994z00_3711;} } } 

}



/* &<@anonymous:1823> */
obj_t BGl_z62zc3z04anonymousza31823ze3ze5zz__r5_macro_4_3_syntaxz00(obj_t BgL_envz00_3175, obj_t BgL_bz00_3176, obj_t BgL_vz00_3177)
{
{ /* R5rs/syntax.scm 543 */
{ /* R5rs/syntax.scm 544 */
 obj_t BgL_arg1826z00_3712;
{ /* R5rs/syntax.scm 544 */
 obj_t BgL_pairz00_3713;
if(
PAIRP(BgL_bz00_3176))
{ /* R5rs/syntax.scm 544 */
BgL_pairz00_3713 = BgL_bz00_3176; }  else 
{ 
 obj_t BgL_auxz00_6003;
BgL_auxz00_6003 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(18676L), BGl_string2989z00zz__r5_macro_4_3_syntaxz00, BGl_string2917z00zz__r5_macro_4_3_syntaxz00, BgL_bz00_3176); 
FAILURE(BgL_auxz00_6003,BFALSE,BFALSE);} 
BgL_arg1826z00_3712 = 
CAR(BgL_pairz00_3713); } 
return 
MAKE_YOUNG_PAIR(BgL_arg1826z00_3712, BgL_vz00_3177);} } 

}



/* loop~0 */
obj_t BGl_loopze70ze7zz__r5_macro_4_3_syntaxz00(obj_t BgL_envz00_3181, obj_t BgL_xz00_2032)
{
{ /* R5rs/syntax.scm 597 */
if(
PAIRP(BgL_xz00_2032))
{ /* R5rs/syntax.scm 599 */
return 
MAKE_YOUNG_PAIR(
BGl_hygieniza7eza7zz__r5_macro_4_3_syntaxz00(
CAR(BgL_xz00_2032), BgL_envz00_3181), 
BGl_loopze70ze7zz__r5_macro_4_3_syntaxz00(BgL_envz00_3181, 
CDR(BgL_xz00_2032)));}  else 
{ /* R5rs/syntax.scm 599 */
if(
NULLP(BgL_xz00_2032))
{ /* R5rs/syntax.scm 601 */
return BNIL;}  else 
{ /* R5rs/syntax.scm 601 */
BGL_TAIL return 
BGl_hygieniza7eza7zz__r5_macro_4_3_syntaxz00(BgL_xz00_2032, BgL_envz00_3181);} } } 

}



/* flatten */
obj_t BGl_flattenz00zz__r5_macro_4_3_syntaxz00(obj_t BgL_lz00_47)
{
{ /* R5rs/syntax.scm 609 */
if(
PAIRP(BgL_lz00_47))
{ /* R5rs/syntax.scm 611 */
return 
MAKE_YOUNG_PAIR(
CAR(BgL_lz00_47), 
BGl_flattenz00zz__r5_macro_4_3_syntaxz00(
CDR(BgL_lz00_47)));}  else 
{ /* R5rs/syntax.scm 611 */
if(
NULLP(BgL_lz00_47))
{ /* R5rs/syntax.scm 612 */
return BgL_lz00_47;}  else 
{ /* R5rs/syntax.scm 613 */
 obj_t BgL_list1936z00_2046;
BgL_list1936z00_2046 = 
MAKE_YOUNG_PAIR(BgL_lz00_47, BNIL); 
return BgL_list1936z00_2046;} } } 

}



/* genvars */
obj_t BGl_genvarsz00zz__r5_macro_4_3_syntaxz00(obj_t BgL_lz00_48)
{
{ /* R5rs/syntax.scm 618 */
if(
PAIRP(BgL_lz00_48))
{ /* R5rs/syntax.scm 628 */
return 
MAKE_YOUNG_PAIR(
BGl_gennameze70ze7zz__r5_macro_4_3_syntaxz00(
CAR(BgL_lz00_48)), 
BGl_genvarsz00zz__r5_macro_4_3_syntaxz00(
CDR(BgL_lz00_48)));}  else 
{ /* R5rs/syntax.scm 628 */
if(
NULLP(BgL_lz00_48))
{ /* R5rs/syntax.scm 629 */
return BgL_lz00_48;}  else 
{ /* R5rs/syntax.scm 629 */
BGL_TAIL return 
BGl_gennameze70ze7zz__r5_macro_4_3_syntaxz00(BgL_lz00_48);} } } 

}



/* genname~0 */
obj_t BGl_gennameze70ze7zz__r5_macro_4_3_syntaxz00(obj_t BgL_lz00_2054)
{
{ /* R5rs/syntax.scm 626 */
{ 

if(
SYMBOLP(BgL_lz00_2054))
{ /* R5rs/syntax.scm 626 */
{ /* R5rs/syntax.scm 622 */
 bool_t BgL_test3455z00_6040;
{ /* R5rs/syntax.scm 471 */
 obj_t BgL_sz00_2887;
{ /* R5rs/syntax.scm 471 */
 obj_t BgL_symbolz00_2888;
if(
SYMBOLP(BgL_lz00_2054))
{ /* R5rs/syntax.scm 471 */
BgL_symbolz00_2888 = BgL_lz00_2054; }  else 
{ 
 obj_t BgL_auxz00_6043;
BgL_auxz00_6043 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2648z00zz__r5_macro_4_3_syntaxz00, 
BINT(15832L), BGl_string2990z00zz__r5_macro_4_3_syntaxz00, BGl_string2652z00zz__r5_macro_4_3_syntaxz00, BgL_lz00_2054); 
FAILURE(BgL_auxz00_6043,BFALSE,BFALSE);} 
{ /* R5rs/syntax.scm 471 */
 obj_t BgL_arg2070z00_2889;
BgL_arg2070z00_2889 = 
SYMBOL_TO_STRING(BgL_symbolz00_2888); 
BgL_sz00_2887 = 
BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg2070z00_2889); } } 
BgL_test3455z00_6040 = 
bigloo_strcmp_at(BgL_sz00_2887, BGl_hygienezd2prefixzd2zz__r5_macro_4_3_syntaxz00, 0L); } 
if(BgL_test3455z00_6040)
{ /* R5rs/syntax.scm 622 */
BGL_TAIL return 
BGl_hygienezd2valuezd2zz__r5_macro_4_3_syntaxz00(BgL_lz00_2054);}  else 
{ /* R5rs/syntax.scm 622 */
BGL_TAIL return 
BGl_gensymz00zz__r4_symbols_6_4z00(BgL_lz00_2054);} } }  else 
{ /* R5rs/syntax.scm 626 */

{ /* R5rs/syntax.scm 626 */

BGL_TAIL return 
BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);} } } } 

}



/* object-init */
obj_t BGl_objectzd2initzd2zz__r5_macro_4_3_syntaxz00(void)
{
{ /* R5rs/syntax.scm 17 */
return BUNSPEC;} 

}



/* generic-init */
obj_t BGl_genericzd2initzd2zz__r5_macro_4_3_syntaxz00(void)
{
{ /* R5rs/syntax.scm 17 */
return BUNSPEC;} 

}



/* method-init */
obj_t BGl_methodzd2initzd2zz__r5_macro_4_3_syntaxz00(void)
{
{ /* R5rs/syntax.scm 17 */
return BUNSPEC;} 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__r5_macro_4_3_syntaxz00(void)
{
{ /* R5rs/syntax.scm 17 */
BGl_modulezd2initializa7ationz75zz__errorz00(88804785L, 
BSTRING_TO_STRING(BGl_string2991z00zz__r5_macro_4_3_syntaxz00)); 
BGl_modulezd2initializa7ationz75zz__objectz00(475449627L, 
BSTRING_TO_STRING(BGl_string2991z00zz__r5_macro_4_3_syntaxz00)); 
BGl_modulezd2initializa7ationz75zz__threadz00(149516032L, 
BSTRING_TO_STRING(BGl_string2991z00zz__r5_macro_4_3_syntaxz00)); 
BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L, 
BSTRING_TO_STRING(BGl_string2991z00zz__r5_macro_4_3_syntaxz00)); 
BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L, 
BSTRING_TO_STRING(BGl_string2991z00zz__r5_macro_4_3_syntaxz00)); 
BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(481261960L, 
BSTRING_TO_STRING(BGl_string2991z00zz__r5_macro_4_3_syntaxz00)); 
BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L, 
BSTRING_TO_STRING(BGl_string2991z00zz__r5_macro_4_3_syntaxz00)); 
return 
BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(268155843L, 
BSTRING_TO_STRING(BGl_string2991z00zz__r5_macro_4_3_syntaxz00));} 

}

#ifdef __cplusplus
}
#endif
