/*===========================================================================*/
/*   (Ieee/control.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Ieee/control.scm -indent -o objs/obj_s/Ieee/control.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___R4_CONTROL_FEATURES_6_9_TYPE_DEFINITIONS
#define BGL___R4_CONTROL_FEATURES_6_9_TYPE_DEFINITIONS
#endif // BGL___R4_CONTROL_FEATURES_6_9_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

BGL_EXPORTED_DECL obj_t BGl_mapz12z12zz__r4_control_features_6_9z00(obj_t, obj_t);
extern bool_t BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_symbol1911z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_symbol1914z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_symbol1919z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_symbol1921z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_symbol1840z00zz__r4_control_features_6_9z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_appendzd2mapz12zc0zz__r4_control_features_6_9z00(obj_t, obj_t);
static obj_t BGl_symbol1844z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_symbol1846z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_symbol1928z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_requirezd2initializa7ationz75zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza31338ze3ze5zz__r4_control_features_6_9z00(obj_t);
static obj_t BGl_z62forcez62zz__r4_control_features_6_9z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_mapzd22zd2zz__r4_control_features_6_9z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_forzd2eachzd22z00zz__r4_control_features_6_9z00(obj_t, obj_t);
static obj_t BGl_symbol1932z00zz__r4_control_features_6_9z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_applyz00zz__r4_control_features_6_9z00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol1935z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_list1903z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_symbol1856z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_list1906z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_symbol1858z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_filterzd2mapzd22z00zz__r4_control_features_6_9z00(obj_t, obj_t);
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__evaluatez00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
BGL_EXPORTED_DECL obj_t BGl_callzd2withzd2currentzd2continuationzd2zz__r4_control_features_6_9z00(obj_t);
extern obj_t BGl_setzd2evaluationzd2contextz12z12zz__evaluatez00(obj_t);
static obj_t BGl_symbol1860z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_symbol1942z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_list1913z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_symbol1866z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_symbol1869z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_list1918z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_z62mapz62zz__r4_control_features_6_9z00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol1950z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_symbol1871z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_symbol1954z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_symbol1873z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_z62forzd2eachzd22z62zz__r4_control_features_6_9z00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol1956z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_z62forzd2eachzb0zz__r4_control_features_6_9z00(obj_t, obj_t, obj_t);
static obj_t BGl_list1843z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_toplevelzd2initzd2zz__r4_control_features_6_9z00(void);
static obj_t BGl_symbol1877z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_symbol1959z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_list1927z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_mapzd22z12zc0zz__r4_control_features_6_9z00(obj_t, obj_t);
static obj_t BGl_symbol1880z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_symbol1964z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_list1931z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_symbol1885z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_symbol1967z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_list1934z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_list1855z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_symbol1889z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_cnstzd2initzd2zz__r4_control_features_6_9z00(void);
extern long bgl_list_length(obj_t);
static obj_t BGl_z62mapzd22zb0zz__r4_control_features_6_9z00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol1971z00zz__r4_control_features_6_9z00 = BUNSPEC;
BGL_EXPORTED_DECL bool_t BGl_procedurezf3zf3zz__r4_control_features_6_9z00(obj_t);
static obj_t BGl_list1941z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_symbol1893z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_importedzd2moduleszd2initz00zz__r4_control_features_6_9z00(void);
static obj_t BGl_z62applyz62zz__r4_control_features_6_9z00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_gczd2rootszd2initz00zz__r4_control_features_6_9z00(void);
static obj_t BGl_list1946z00zz__r4_control_features_6_9z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_filterzd2mapzd2zz__r4_control_features_6_9z00(obj_t, obj_t);
static obj_t BGl_list1867z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_list1949z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_list1868z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_z62filterz62zz__r4_control_features_6_9z00(obj_t, obj_t, obj_t);
static obj_t BGl_list1953z00zz__r4_control_features_6_9z00 = BUNSPEC;
extern obj_t call_cc(obj_t);
static obj_t BGl_list1876z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_list1958z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza31198ze3ze5zz__r4_control_features_6_9z00(obj_t, obj_t);
extern obj_t BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
extern obj_t bgl_reverse_bang(obj_t);
BGL_EXPORTED_DECL obj_t BGl_appendzd2mapzd2zz__r4_control_features_6_9z00(obj_t, obj_t);
static obj_t BGl_z62procedurezf3z91zz__r4_control_features_6_9z00(obj_t, obj_t);
static obj_t BGl_list1963z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_list1884z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_list1966z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_z62filterzd2mapzb0zz__r4_control_features_6_9z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_filterz12z12zz__r4_control_features_6_9z00(obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31330ze3ze5zz__r4_control_features_6_9z00(obj_t, obj_t);
static obj_t BGl_z62appendzd2mapz12za2zz__r4_control_features_6_9z00(obj_t, obj_t, obj_t);
static obj_t BGl_list1970z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_z62filterz12z70zz__r4_control_features_6_9z00(obj_t, obj_t, obj_t);
static obj_t BGl_list1892z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_z62makezd2promisezb0zz__r4_control_features_6_9z00(obj_t, obj_t);
extern obj_t bstring_to_symbol(obj_t);
static obj_t BGl_z62appendzd2mapzb0zz__r4_control_features_6_9z00(obj_t, obj_t, obj_t);
static obj_t BGl_appendzd221011zd2zz__r4_control_features_6_9z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_makezd2promisezd2zz__r4_control_features_6_9z00(obj_t);
static obj_t BGl_z62zc3z04anonymousza31332ze3ze5zz__r4_control_features_6_9z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_callzf2cczf2zz__r4_control_features_6_9z00(obj_t);
static obj_t BGl_z62callzf2ccz90zz__r4_control_features_6_9z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_mapz00zz__r4_control_features_6_9z00(obj_t, obj_t);
static obj_t BGl_loopze70ze7zz__r4_control_features_6_9z00(obj_t, obj_t);
static obj_t BGl_loopze71ze7zz__r4_control_features_6_9z00(obj_t, obj_t);
static obj_t BGl_loopze72ze7zz__r4_control_features_6_9z00(obj_t, obj_t);
extern obj_t BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_loopze73ze7zz__r4_control_features_6_9z00(obj_t, obj_t);
static obj_t BGl_loopze74ze7zz__r4_control_features_6_9z00(obj_t);
static obj_t BGl_appendzd2map2z12zc0zz__r4_control_features_6_9z00(obj_t, obj_t);
static obj_t BGl_z62callzd2withzd2currentzd2continuationzb0zz__r4_control_features_6_9z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_filterz00zz__r4_control_features_6_9z00(obj_t, obj_t);
static obj_t BGl_z62dynamiczd2windzb0zz__r4_control_features_6_9z00(obj_t, obj_t, obj_t, obj_t);
extern obj_t BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_forcez00zz__r4_control_features_6_9z00(obj_t);
extern obj_t BGl_getzd2evaluationzd2contextz00zz__evaluatez00(void);
static obj_t BGl_z62mapz12z70zz__r4_control_features_6_9z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_forzd2eachzd2zz__r4_control_features_6_9z00(obj_t, obj_t);
static obj_t BGl_symbol1901z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_symbol1904z00zz__r4_control_features_6_9z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_dynamiczd2windzd2zz__r4_control_features_6_9z00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol1907z00zz__r4_control_features_6_9z00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza31328ze3ze5zz__r4_control_features_6_9z00(obj_t);
static obj_t *__cnst;


DEFINE_EXPORT_BGL_PROCEDURE( BGl_appendzd2mapz12zd2envz12zz__r4_control_features_6_9z00, BgL_bgl_za762appendza7d2mapza71976za7, va_generic_entry, BGl_z62appendzd2mapz12za2zz__r4_control_features_6_9z00, BUNSPEC, -2 );
DEFINE_STRING( BGl_string1951z00zz__r4_control_features_6_9z00, BgL_bgl_string1951za700za7za7_1977za7, "<@anonymous:1332>", 17 );
DEFINE_STRING( BGl_string1870z00zz__r4_control_features_6_9z00, BgL_bgl_string1870za700za7za7_1978za7, "map-2", 5 );
DEFINE_STRING( BGl_string1952z00zz__r4_control_features_6_9z00, BgL_bgl_string1952za700za7za7_1979za7, "<@anonymous:1332>:Wrong number of arguments", 43 );
DEFINE_STRING( BGl_string1872z00zz__r4_control_features_6_9z00, BgL_bgl_string1872za700za7za7_1980za7, "car-env", 7 );
DEFINE_STRING( BGl_string1955z00zz__r4_control_features_6_9z00, BgL_bgl_string1955za700za7za7_1981za7, "cont", 4 );
DEFINE_STRING( BGl_string1874z00zz__r4_control_features_6_9z00, BgL_bgl_string1874za700za7za7_1982za7, "l", 1 );
DEFINE_STRING( BGl_string1875z00zz__r4_control_features_6_9z00, BgL_bgl_string1875za700za7za7_1983za7, "&map", 4 );
DEFINE_STRING( BGl_string1957z00zz__r4_control_features_6_9z00, BgL_bgl_string1957za700za7za7_1984za7, "arg1336", 7 );
DEFINE_STRING( BGl_string1878z00zz__r4_control_features_6_9z00, BgL_bgl_string1878za700za7za7_1985za7, "arg1123", 7 );
DEFINE_STRING( BGl_string1879z00zz__r4_control_features_6_9z00, BgL_bgl_string1879za700za7za7_1986za7, "map!", 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_forzd2eachzd22zd2envzd2zz__r4_control_features_6_9z00, BgL_bgl_za762forza7d2eachza7d21987za7, BGl_z62forzd2eachzd22z62zz__r4_control_features_6_9z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_forzd2eachzd2envz00zz__r4_control_features_6_9z00, BgL_bgl_za762forza7d2eachza7b01988za7, va_generic_entry, BGl_z62forzd2eachzb0zz__r4_control_features_6_9z00, BUNSPEC, -2 );
DEFINE_STRING( BGl_string1960z00zz__r4_control_features_6_9z00, BgL_bgl_string1960za700za7za7_1989za7, "vals", 4 );
DEFINE_STRING( BGl_string1961z00zz__r4_control_features_6_9z00, BgL_bgl_string1961za700za7za7_1990za7, "&call-with-current-continuation", 31 );
DEFINE_STRING( BGl_string1962z00zz__r4_control_features_6_9z00, BgL_bgl_string1962za700za7za7_1991za7, "dynamic-wind:Wrong number of arguments", 38 );
DEFINE_STRING( BGl_string1881z00zz__r4_control_features_6_9z00, BgL_bgl_string1881za700za7za7_1992za7, "&map!", 5 );
DEFINE_STRING( BGl_string1882z00zz__r4_control_features_6_9z00, BgL_bgl_string1882za700za7za7_1993za7, "loop~2", 6 );
DEFINE_STRING( BGl_string1883z00zz__r4_control_features_6_9z00, BgL_bgl_string1883za700za7za7_1994za7, "loop~2:Wrong number of arguments", 32 );
DEFINE_STRING( BGl_string1965z00zz__r4_control_features_6_9z00, BgL_bgl_string1965za700za7za7_1995za7, "before", 6 );
DEFINE_STRING( BGl_string1886z00zz__r4_control_features_6_9z00, BgL_bgl_string1886za700za7za7_1996za7, "arg1149", 7 );
DEFINE_STRING( BGl_string1968z00zz__r4_control_features_6_9z00, BgL_bgl_string1968za700za7za7_1997za7, "thunk", 5 );
DEFINE_STRING( BGl_string1887z00zz__r4_control_features_6_9z00, BgL_bgl_string1887za700za7za7_1998za7, "append-map", 10 );
DEFINE_STRING( BGl_string1969z00zz__r4_control_features_6_9z00, BgL_bgl_string1969za700za7za7_1999za7, "dynamic-wind", 12 );
DEFINE_STRING( BGl_string1888z00zz__r4_control_features_6_9z00, BgL_bgl_string1888za700za7za7_2000za7, "loop~1", 6 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2promisezd2envz00zz__r4_control_features_6_9z00, BgL_bgl_za762makeza7d2promis2001z00, BGl_z62makezd2promisezb0zz__r4_control_features_6_9z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1890z00zz__r4_control_features_6_9z00, BgL_bgl_string1890za700za7za7_2002za7, "&append-map", 11 );
DEFINE_STRING( BGl_string1972z00zz__r4_control_features_6_9z00, BgL_bgl_string1972za700za7za7_2003za7, "after", 5 );
DEFINE_STRING( BGl_string1891z00zz__r4_control_features_6_9z00, BgL_bgl_string1891za700za7za7_2004za7, "<@anonymous:1184>:Wrong number of arguments", 43 );
DEFINE_STRING( BGl_string1973z00zz__r4_control_features_6_9z00, BgL_bgl_string1973za700za7za7_2005za7, "&dynamic-wind", 13 );
DEFINE_STRING( BGl_string1974z00zz__r4_control_features_6_9z00, BgL_bgl_string1974za700za7za7_2006za7, "<@anonymous:1338>:Wrong number of arguments", 43 );
DEFINE_STRING( BGl_string1975z00zz__r4_control_features_6_9z00, BgL_bgl_string1975za700za7za7_2007za7, "__r4_control_features_6_9", 25 );
DEFINE_STRING( BGl_string1894z00zz__r4_control_features_6_9z00, BgL_bgl_string1894za700za7za7_2008za7, "x", 1 );
DEFINE_STRING( BGl_string1895z00zz__r4_control_features_6_9z00, BgL_bgl_string1895za700za7za7_2009za7, "<@anonymous:1184>", 17 );
DEFINE_STRING( BGl_string1896z00zz__r4_control_features_6_9z00, BgL_bgl_string1896za700za7za7_2010za7, "for-each", 8 );
DEFINE_STRING( BGl_string1897z00zz__r4_control_features_6_9z00, BgL_bgl_string1897za700za7za7_2011za7, "list", 4 );
DEFINE_STRING( BGl_string1898z00zz__r4_control_features_6_9z00, BgL_bgl_string1898za700za7za7_2012za7, "append-map2!", 12 );
DEFINE_STRING( BGl_string1899z00zz__r4_control_features_6_9z00, BgL_bgl_string1899za700za7za7_2013za7, "append-map!", 11 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_mapzd2envzd2zz__r4_control_features_6_9z00, BgL_bgl_za762mapza762za7za7__r4_2014z00, va_generic_entry, BGl_z62mapz62zz__r4_control_features_6_9z00, BUNSPEC, -2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_appendzd2mapzd2envz00zz__r4_control_features_6_9z00, BgL_bgl_za762appendza7d2mapza72015za7, va_generic_entry, BGl_z62appendzd2mapzb0zz__r4_control_features_6_9z00, BUNSPEC, -2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_filterzd2envzd2zz__r4_control_features_6_9z00, BgL_bgl_za762filterza762za7za7__2016z00, BGl_z62filterz62zz__r4_control_features_6_9z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_filterz12zd2envzc0zz__r4_control_features_6_9z00, BgL_bgl_za762filterza712za770za72017z00, BGl_z62filterz12z70zz__r4_control_features_6_9z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_mapzd22zd2envz00zz__r4_control_features_6_9z00, BgL_bgl_za762mapza7d22za7b0za7za7_2018za7, BGl_z62mapzd22zb0zz__r4_control_features_6_9z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_procedurezf3zd2envz21zz__r4_control_features_6_9z00, BgL_bgl_za762procedureza7f3za72019za7, BGl_z62procedurezf3z91zz__r4_control_features_6_9z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_callzd2withzd2currentzd2continuationzd2envz00zz__r4_control_features_6_9z00, BgL_bgl_za762callza7d2withza7d2020za7, BGl_z62callzd2withzd2currentzd2continuationzb0zz__r4_control_features_6_9z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_forcezd2envzd2zz__r4_control_features_6_9z00, BgL_bgl_za762forceza762za7za7__r2021z00, BGl_z62forcez62zz__r4_control_features_6_9z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_applyzd2envzd2zz__r4_control_features_6_9z00, BgL_bgl_za762applyza762za7za7__r2022z00, va_generic_entry, BGl_z62applyz62zz__r4_control_features_6_9z00, BUNSPEC, -3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_filterzd2mapzd2envz00zz__r4_control_features_6_9z00, BgL_bgl_za762filterza7d2mapza72023za7, va_generic_entry, BGl_z62filterzd2mapzb0zz__r4_control_features_6_9z00, BUNSPEC, -2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_mapz12zd2envzc0zz__r4_control_features_6_9z00, BgL_bgl_za762mapza712za770za7za7__2024za7, va_generic_entry, BGl_z62mapz12z70zz__r4_control_features_6_9z00, BUNSPEC, -2 );
DEFINE_STRING( BGl_string1900z00zz__r4_control_features_6_9z00, BgL_bgl_string1900za700za7za7_2025za7, "&append-map!", 12 );
DEFINE_STRING( BGl_string1902z00zz__r4_control_features_6_9z00, BgL_bgl_string1902za700za7za7_2026za7, "<@anonymous:1198>", 17 );
DEFINE_STRING( BGl_string1905z00zz__r4_control_features_6_9z00, BgL_bgl_string1905za700za7za7_2027za7, "xs", 2 );
DEFINE_STRING( BGl_string1908z00zz__r4_control_features_6_9z00, BgL_bgl_string1908za700za7za7_2028za7, "arg1215", 7 );
DEFINE_STRING( BGl_string1909z00zz__r4_control_features_6_9z00, BgL_bgl_string1909za700za7za7_2029za7, "filter-map", 10 );
DEFINE_STRING( BGl_string1910z00zz__r4_control_features_6_9z00, BgL_bgl_string1910za700za7za7_2030za7, "loop~0", 6 );
DEFINE_STRING( BGl_string1912z00zz__r4_control_features_6_9z00, BgL_bgl_string1912za700za7za7_2031za7, "&filter-map", 11 );
DEFINE_STRING( BGl_string1915z00zz__r4_control_features_6_9z00, BgL_bgl_string1915za700za7za7_2032za7, "arg1232", 7 );
DEFINE_STRING( BGl_string1916z00zz__r4_control_features_6_9z00, BgL_bgl_string1916za700za7za7_2033za7, "&for-each-2", 11 );
DEFINE_STRING( BGl_string1917z00zz__r4_control_features_6_9z00, BgL_bgl_string1917za700za7za7_2034za7, "&for-each", 9 );
DEFINE_STRING( BGl_string1920z00zz__r4_control_features_6_9z00, BgL_bgl_string1920za700za7za7_2035za7, "pred", 4 );
DEFINE_STRING( BGl_string1922z00zz__r4_control_features_6_9z00, BgL_bgl_string1922za700za7za7_2036za7, "arg1304", 7 );
DEFINE_STRING( BGl_string1841z00zz__r4_control_features_6_9z00, BgL_bgl_string1841za700za7za7_2037za7, "apply", 5 );
DEFINE_STRING( BGl_string1923z00zz__r4_control_features_6_9z00, BgL_bgl_string1923za700za7za7_2038za7, "filter", 6 );
DEFINE_STRING( BGl_string1842z00zz__r4_control_features_6_9z00, BgL_bgl_string1842za700za7za7_2039za7, "Wrong number of arguments", 25 );
DEFINE_STRING( BGl_string1924z00zz__r4_control_features_6_9z00, BgL_bgl_string1924za700za7za7_2040za7, "&filter", 7 );
DEFINE_STRING( BGl_string1925z00zz__r4_control_features_6_9z00, BgL_bgl_string1925za700za7za7_2041za7, "lp", 2 );
DEFINE_STRING( BGl_string1926z00zz__r4_control_features_6_9z00, BgL_bgl_string1926za700za7za7_2042za7, "lp:Wrong number of arguments", 28 );
DEFINE_STRING( BGl_string1845z00zz__r4_control_features_6_9z00, BgL_bgl_string1845za700za7za7_2043za7, "proc", 4 );
DEFINE_STRING( BGl_string1847z00zz__r4_control_features_6_9z00, BgL_bgl_string1847za700za7za7_2044za7, "args", 4 );
DEFINE_STRING( BGl_string1929z00zz__r4_control_features_6_9z00, BgL_bgl_string1929za700za7za7_2045za7, "arg1327", 7 );
DEFINE_STRING( BGl_string1848z00zz__r4_control_features_6_9z00, BgL_bgl_string1848za700za7za7_2046za7, "/tmp/bigloo/runtime/Ieee/control.scm", 36 );
DEFINE_STRING( BGl_string1849z00zz__r4_control_features_6_9z00, BgL_bgl_string1849za700za7za7_2047za7, "loop~4", 6 );
extern obj_t BGl_cdrzd2envzd2zz__r4_pairs_and_lists_6_3z00;
DEFINE_EXPORT_BGL_PROCEDURE( BGl_dynamiczd2windzd2envz00zz__r4_control_features_6_9z00, BgL_bgl_za762dynamicza7d2win2048z00, BGl_z62dynamiczd2windzb0zz__r4_control_features_6_9z00, 0L, BUNSPEC, 3 );
DEFINE_STRING( BGl_string1930z00zz__r4_control_features_6_9z00, BgL_bgl_string1930za700za7za7_2049za7, "scan-in:Wrong number of arguments", 33 );
DEFINE_STRING( BGl_string1850z00zz__r4_control_features_6_9z00, BgL_bgl_string1850za700za7za7_2050za7, "pair", 4 );
DEFINE_STRING( BGl_string1851z00zz__r4_control_features_6_9z00, BgL_bgl_string1851za700za7za7_2051za7, "&apply", 6 );
DEFINE_STRING( BGl_string1933z00zz__r4_control_features_6_9z00, BgL_bgl_string1933za700za7za7_2052za7, "arg1316", 7 );
DEFINE_STRING( BGl_string1852z00zz__r4_control_features_6_9z00, BgL_bgl_string1852za700za7za7_2053za7, "procedure", 9 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_callzf2cczd2envz20zz__r4_control_features_6_9z00, BgL_bgl_za762callza7f2ccza790za72054z00, BGl_z62callzf2ccz90zz__r4_control_features_6_9z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1853z00zz__r4_control_features_6_9z00, BgL_bgl_string1853za700za7za7_2055za7, "loop", 4 );
DEFINE_STRING( BGl_string1854z00zz__r4_control_features_6_9z00, BgL_bgl_string1854za700za7za7_2056za7, "loop:Wrong number of arguments", 30 );
DEFINE_STRING( BGl_string1936z00zz__r4_control_features_6_9z00, BgL_bgl_string1936za700za7za7_2057za7, "arg1325", 7 );
DEFINE_STRING( BGl_string1937z00zz__r4_control_features_6_9z00, BgL_bgl_string1937za700za7za7_2058za7, "filter!", 7 );
DEFINE_STRING( BGl_string1938z00zz__r4_control_features_6_9z00, BgL_bgl_string1938za700za7za7_2059za7, "&filter!", 8 );
DEFINE_STRING( BGl_string1857z00zz__r4_control_features_6_9z00, BgL_bgl_string1857za700za7za7_2060za7, "funcall", 7 );
extern obj_t BGl_carzd2envzd2zz__r4_pairs_and_lists_6_3z00;
DEFINE_STRING( BGl_string1939z00zz__r4_control_features_6_9z00, BgL_bgl_string1939za700za7za7_2061za7, "force", 5 );
DEFINE_STRING( BGl_string1859z00zz__r4_control_features_6_9z00, BgL_bgl_string1859za700za7za7_2062za7, "f", 1 );
DEFINE_STRING( BGl_string1940z00zz__r4_control_features_6_9z00, BgL_bgl_string1940za700za7za7_2063za7, "force:Wrong number of arguments", 31 );
DEFINE_STRING( BGl_string1861z00zz__r4_control_features_6_9z00, BgL_bgl_string1861za700za7za7_2064za7, "arg1085", 7 );
DEFINE_STRING( BGl_string1943z00zz__r4_control_features_6_9z00, BgL_bgl_string1943za700za7za7_2065za7, "promise", 7 );
DEFINE_STRING( BGl_string1862z00zz__r4_control_features_6_9z00, BgL_bgl_string1862za700za7za7_2066za7, "&map-2", 6 );
DEFINE_STRING( BGl_string1944z00zz__r4_control_features_6_9z00, BgL_bgl_string1944za700za7za7_2067za7, "&make-promise", 13 );
DEFINE_STRING( BGl_string1863z00zz__r4_control_features_6_9z00, BgL_bgl_string1863za700za7za7_2068za7, "pair-nil", 8 );
DEFINE_STRING( BGl_string1945z00zz__r4_control_features_6_9z00, BgL_bgl_string1945za700za7za7_2069za7, "<@anonymous:1328>:Wrong number of arguments", 43 );
DEFINE_STRING( BGl_string1864z00zz__r4_control_features_6_9z00, BgL_bgl_string1864za700za7za7_2070za7, "map", 3 );
DEFINE_STRING( BGl_string1865z00zz__r4_control_features_6_9z00, BgL_bgl_string1865za700za7za7_2071za7, "loop~3", 6 );
DEFINE_STRING( BGl_string1947z00zz__r4_control_features_6_9z00, BgL_bgl_string1947za700za7za7_2072za7, "&call/cc", 8 );
DEFINE_STRING( BGl_string1948z00zz__r4_control_features_6_9z00, BgL_bgl_string1948za700za7za7_2073za7, "<@anonymous:1330>:Wrong number of arguments", 43 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_symbol1911z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_symbol1914z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_symbol1919z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_symbol1921z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_symbol1840z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_symbol1844z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_symbol1846z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_symbol1928z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_symbol1932z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_symbol1935z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_list1903z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_symbol1856z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_list1906z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_symbol1858z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_symbol1860z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_symbol1942z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_list1913z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_symbol1866z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_symbol1869z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_list1918z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_symbol1950z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_symbol1871z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_symbol1954z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_symbol1873z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_symbol1956z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_list1843z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_symbol1877z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_symbol1959z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_list1927z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_symbol1880z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_symbol1964z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_list1931z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_symbol1885z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_symbol1967z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_list1934z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_list1855z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_symbol1889z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_symbol1971z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_list1941z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_symbol1893z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_list1946z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_list1867z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_list1949z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_list1868z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_list1953z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_list1876z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_list1958z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_list1963z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_list1884z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_list1966z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_list1970z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_list1892z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_symbol1901z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_symbol1904z00zz__r4_control_features_6_9z00) );
ADD_ROOT( (void *)(&BGl_symbol1907z00zz__r4_control_features_6_9z00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long BgL_checksumz00_1817, char * BgL_fromz00_1818)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__r4_control_features_6_9z00))
{ 
BGl_requirezd2initializa7ationz75zz__r4_control_features_6_9z00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__r4_control_features_6_9z00(); 
BGl_cnstzd2initzd2zz__r4_control_features_6_9z00(); 
BGl_importedzd2moduleszd2initz00zz__r4_control_features_6_9z00(); 
return 
BGl_toplevelzd2initzd2zz__r4_control_features_6_9z00();}  else 
{ 
return BUNSPEC;} } 

}



/* cnst-init */
obj_t BGl_cnstzd2initzd2zz__r4_control_features_6_9z00(void)
{
{ /* Ieee/control.scm 14 */
BGl_symbol1840z00zz__r4_control_features_6_9z00 = 
bstring_to_symbol(BGl_string1841z00zz__r4_control_features_6_9z00); 
BGl_symbol1844z00zz__r4_control_features_6_9z00 = 
bstring_to_symbol(BGl_string1845z00zz__r4_control_features_6_9z00); 
BGl_symbol1846z00zz__r4_control_features_6_9z00 = 
bstring_to_symbol(BGl_string1847z00zz__r4_control_features_6_9z00); 
BGl_list1843z00zz__r4_control_features_6_9z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1840z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1844z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1846z00zz__r4_control_features_6_9z00, BNIL))); 
BGl_symbol1856z00zz__r4_control_features_6_9z00 = 
bstring_to_symbol(BGl_string1857z00zz__r4_control_features_6_9z00); 
BGl_symbol1858z00zz__r4_control_features_6_9z00 = 
bstring_to_symbol(BGl_string1859z00zz__r4_control_features_6_9z00); 
BGl_symbol1860z00zz__r4_control_features_6_9z00 = 
bstring_to_symbol(BGl_string1861z00zz__r4_control_features_6_9z00); 
BGl_list1855z00zz__r4_control_features_6_9z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1856z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1858z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1858z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1860z00zz__r4_control_features_6_9z00, BNIL)))); 
BGl_symbol1866z00zz__r4_control_features_6_9z00 = 
bstring_to_symbol(BGl_string1865z00zz__r4_control_features_6_9z00); 
BGl_symbol1869z00zz__r4_control_features_6_9z00 = 
bstring_to_symbol(BGl_string1870z00zz__r4_control_features_6_9z00); 
BGl_symbol1871z00zz__r4_control_features_6_9z00 = 
bstring_to_symbol(BGl_string1872z00zz__r4_control_features_6_9z00); 
BGl_symbol1873z00zz__r4_control_features_6_9z00 = 
bstring_to_symbol(BGl_string1874z00zz__r4_control_features_6_9z00); 
BGl_list1868z00zz__r4_control_features_6_9z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1869z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1871z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1873z00zz__r4_control_features_6_9z00, BNIL))); 
BGl_list1867z00zz__r4_control_features_6_9z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1840z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1858z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_list1868z00zz__r4_control_features_6_9z00, BNIL))); 
BGl_symbol1877z00zz__r4_control_features_6_9z00 = 
bstring_to_symbol(BGl_string1878z00zz__r4_control_features_6_9z00); 
BGl_list1876z00zz__r4_control_features_6_9z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1856z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1858z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1858z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1877z00zz__r4_control_features_6_9z00, BNIL)))); 
BGl_symbol1880z00zz__r4_control_features_6_9z00 = 
bstring_to_symbol(BGl_string1853z00zz__r4_control_features_6_9z00); 
BGl_symbol1885z00zz__r4_control_features_6_9z00 = 
bstring_to_symbol(BGl_string1886z00zz__r4_control_features_6_9z00); 
BGl_list1884z00zz__r4_control_features_6_9z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1856z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1858z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1858z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1885z00zz__r4_control_features_6_9z00, BNIL)))); 
BGl_symbol1889z00zz__r4_control_features_6_9z00 = 
bstring_to_symbol(BGl_string1888z00zz__r4_control_features_6_9z00); 
BGl_symbol1893z00zz__r4_control_features_6_9z00 = 
bstring_to_symbol(BGl_string1894z00zz__r4_control_features_6_9z00); 
BGl_list1892z00zz__r4_control_features_6_9z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1856z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1858z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1858z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1893z00zz__r4_control_features_6_9z00, BNIL)))); 
BGl_symbol1901z00zz__r4_control_features_6_9z00 = 
bstring_to_symbol(BGl_string1902z00zz__r4_control_features_6_9z00); 
BGl_symbol1904z00zz__r4_control_features_6_9z00 = 
bstring_to_symbol(BGl_string1905z00zz__r4_control_features_6_9z00); 
BGl_list1903z00zz__r4_control_features_6_9z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1840z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1858z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1904z00zz__r4_control_features_6_9z00, BNIL))); 
BGl_symbol1907z00zz__r4_control_features_6_9z00 = 
bstring_to_symbol(BGl_string1908z00zz__r4_control_features_6_9z00); 
BGl_list1906z00zz__r4_control_features_6_9z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1856z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1858z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1858z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1907z00zz__r4_control_features_6_9z00, BNIL)))); 
BGl_symbol1911z00zz__r4_control_features_6_9z00 = 
bstring_to_symbol(BGl_string1910z00zz__r4_control_features_6_9z00); 
BGl_symbol1914z00zz__r4_control_features_6_9z00 = 
bstring_to_symbol(BGl_string1915z00zz__r4_control_features_6_9z00); 
BGl_list1913z00zz__r4_control_features_6_9z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1856z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1858z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1858z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1914z00zz__r4_control_features_6_9z00, BNIL)))); 
BGl_symbol1919z00zz__r4_control_features_6_9z00 = 
bstring_to_symbol(BGl_string1920z00zz__r4_control_features_6_9z00); 
BGl_symbol1921z00zz__r4_control_features_6_9z00 = 
bstring_to_symbol(BGl_string1922z00zz__r4_control_features_6_9z00); 
BGl_list1918z00zz__r4_control_features_6_9z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1856z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1919z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1919z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1921z00zz__r4_control_features_6_9z00, BNIL)))); 
BGl_symbol1928z00zz__r4_control_features_6_9z00 = 
bstring_to_symbol(BGl_string1929z00zz__r4_control_features_6_9z00); 
BGl_list1927z00zz__r4_control_features_6_9z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1856z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1919z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1919z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1928z00zz__r4_control_features_6_9z00, BNIL)))); 
BGl_symbol1932z00zz__r4_control_features_6_9z00 = 
bstring_to_symbol(BGl_string1933z00zz__r4_control_features_6_9z00); 
BGl_list1931z00zz__r4_control_features_6_9z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1856z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1919z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1919z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1932z00zz__r4_control_features_6_9z00, BNIL)))); 
BGl_symbol1935z00zz__r4_control_features_6_9z00 = 
bstring_to_symbol(BGl_string1936z00zz__r4_control_features_6_9z00); 
BGl_list1934z00zz__r4_control_features_6_9z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1856z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1919z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1919z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1935z00zz__r4_control_features_6_9z00, BNIL)))); 
BGl_symbol1942z00zz__r4_control_features_6_9z00 = 
bstring_to_symbol(BGl_string1943z00zz__r4_control_features_6_9z00); 
BGl_list1941z00zz__r4_control_features_6_9z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1856z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1942z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1942z00zz__r4_control_features_6_9z00, BNIL))); 
BGl_list1946z00zz__r4_control_features_6_9z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1856z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1844z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1844z00zz__r4_control_features_6_9z00, BNIL))); 
BGl_symbol1950z00zz__r4_control_features_6_9z00 = 
bstring_to_symbol(BGl_string1951z00zz__r4_control_features_6_9z00); 
BGl_list1949z00zz__r4_control_features_6_9z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1856z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1844z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1844z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1950z00zz__r4_control_features_6_9z00, BNIL)))); 
BGl_symbol1954z00zz__r4_control_features_6_9z00 = 
bstring_to_symbol(BGl_string1955z00zz__r4_control_features_6_9z00); 
BGl_symbol1956z00zz__r4_control_features_6_9z00 = 
bstring_to_symbol(BGl_string1957z00zz__r4_control_features_6_9z00); 
BGl_list1953z00zz__r4_control_features_6_9z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1856z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1954z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1954z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1956z00zz__r4_control_features_6_9z00, BNIL)))); 
BGl_symbol1959z00zz__r4_control_features_6_9z00 = 
bstring_to_symbol(BGl_string1960z00zz__r4_control_features_6_9z00); 
BGl_list1958z00zz__r4_control_features_6_9z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1856z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1954z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1954z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1959z00zz__r4_control_features_6_9z00, BNIL)))); 
BGl_symbol1964z00zz__r4_control_features_6_9z00 = 
bstring_to_symbol(BGl_string1965z00zz__r4_control_features_6_9z00); 
BGl_list1963z00zz__r4_control_features_6_9z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1856z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1964z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1964z00zz__r4_control_features_6_9z00, BNIL))); 
BGl_symbol1967z00zz__r4_control_features_6_9z00 = 
bstring_to_symbol(BGl_string1968z00zz__r4_control_features_6_9z00); 
BGl_list1966z00zz__r4_control_features_6_9z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1856z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1967z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1967z00zz__r4_control_features_6_9z00, BNIL))); 
BGl_symbol1971z00zz__r4_control_features_6_9z00 = 
bstring_to_symbol(BGl_string1972z00zz__r4_control_features_6_9z00); 
return ( 
BGl_list1970z00zz__r4_control_features_6_9z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1856z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1971z00zz__r4_control_features_6_9z00, 
MAKE_YOUNG_PAIR(BGl_symbol1971z00zz__r4_control_features_6_9z00, BNIL))), BUNSPEC) ;} 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__r4_control_features_6_9z00(void)
{
{ /* Ieee/control.scm 14 */
return 
bgl_gc_roots_register();} 

}



/* toplevel-init */
obj_t BGl_toplevelzd2initzd2zz__r4_control_features_6_9z00(void)
{
{ /* Ieee/control.scm 14 */
return BUNSPEC;} 

}



/* append-21011 */
obj_t BGl_appendzd221011zd2zz__r4_control_features_6_9z00(obj_t BgL_l1z00_1, obj_t BgL_l2z00_2)
{
{ 
{ 
 obj_t BgL_headz00_741;
BgL_headz00_741 = 
MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2); 
{ 
 obj_t BgL_prevz00_742; obj_t BgL_tailz00_743;
BgL_prevz00_742 = BgL_headz00_741; 
BgL_tailz00_743 = BgL_l1z00_1; 
BgL_loopz00_744:
if(
PAIRP(BgL_tailz00_743))
{ 
 obj_t BgL_newzd2prevzd2_746;
BgL_newzd2prevzd2_746 = 
MAKE_YOUNG_PAIR(
CAR(BgL_tailz00_743), BgL_l2z00_2); 
SET_CDR(BgL_prevz00_742, BgL_newzd2prevzd2_746); 
{ 
 obj_t BgL_tailz00_1946; obj_t BgL_prevz00_1945;
BgL_prevz00_1945 = BgL_newzd2prevzd2_746; 
BgL_tailz00_1946 = 
CDR(BgL_tailz00_743); 
BgL_tailz00_743 = BgL_tailz00_1946; 
BgL_prevz00_742 = BgL_prevz00_1945; 
goto BgL_loopz00_744;} }  else 
{ BNIL; } 
return 
CDR(BgL_headz00_741);} } } 

}



/* procedure? */
BGL_EXPORTED_DEF bool_t BGl_procedurezf3zf3zz__r4_control_features_6_9z00(obj_t BgL_objz00_3)
{
{ /* Ieee/control.scm 79 */
return 
PROCEDUREP(BgL_objz00_3);} 

}



/* &procedure? */
obj_t BGl_z62procedurezf3z91zz__r4_control_features_6_9z00(obj_t BgL_envz00_1399, obj_t BgL_objz00_1400)
{
{ /* Ieee/control.scm 79 */
return 
BBOOL(
BGl_procedurezf3zf3zz__r4_control_features_6_9z00(BgL_objz00_1400));} 

}



/* apply */
BGL_EXPORTED_DEF obj_t BGl_applyz00zz__r4_control_features_6_9z00(obj_t BgL_procz00_4, obj_t BgL_argsz00_5, obj_t BgL_optz00_6)
{
{ /* Ieee/control.scm 85 */
{ /* Ieee/control.scm 86 */
 obj_t BgL_argsz00_749;
if(
PAIRP(BgL_optz00_6))
{ /* Ieee/control.scm 86 */
BgL_argsz00_749 = 
MAKE_YOUNG_PAIR(BgL_argsz00_5, 
BGl_loopze74ze7zz__r4_control_features_6_9z00(BgL_optz00_6)); }  else 
{ /* Ieee/control.scm 86 */
BgL_argsz00_749 = BgL_argsz00_5; } 
{ /* Ieee/control.scm 92 */
 int BgL_len1612z00_1488;
BgL_len1612z00_1488 = 
(int)(
bgl_list_length(
((obj_t)BgL_argsz00_749))); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_procz00_4, BgL_len1612z00_1488))
{ /* Ieee/control.scm 92 */
return 
apply(BgL_procz00_4, 
((obj_t)BgL_argsz00_749));}  else 
{ /* Ieee/control.scm 92 */
FAILURE(BGl_symbol1840z00zz__r4_control_features_6_9z00,BGl_string1842z00zz__r4_control_features_6_9z00,BGl_list1843z00zz__r4_control_features_6_9z00);} } } } 

}



/* loop~4 */
obj_t BGl_loopze74ze7zz__r4_control_features_6_9z00(obj_t BgL_optz00_753)
{
{ /* Ieee/control.scm 87 */
{ /* Ieee/control.scm 88 */
 bool_t BgL_test2078z00_1965;
{ /* Ieee/control.scm 88 */
 obj_t BgL_tmpz00_1966;
{ /* Ieee/control.scm 88 */
 obj_t BgL_pairz00_1259;
if(
PAIRP(BgL_optz00_753))
{ /* Ieee/control.scm 88 */
BgL_pairz00_1259 = BgL_optz00_753; }  else 
{ 
 obj_t BgL_auxz00_1969;
BgL_auxz00_1969 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(3388L), BGl_string1849z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_optz00_753); 
FAILURE(BgL_auxz00_1969,BFALSE,BFALSE);} 
BgL_tmpz00_1966 = 
CDR(BgL_pairz00_1259); } 
BgL_test2078z00_1965 = 
PAIRP(BgL_tmpz00_1966); } 
if(BgL_test2078z00_1965)
{ /* Ieee/control.scm 89 */
 obj_t BgL_arg1074z00_757; obj_t BgL_arg1075z00_758;
{ /* Ieee/control.scm 89 */
 obj_t BgL_pairz00_1260;
if(
PAIRP(BgL_optz00_753))
{ /* Ieee/control.scm 89 */
BgL_pairz00_1260 = BgL_optz00_753; }  else 
{ 
 obj_t BgL_auxz00_1977;
BgL_auxz00_1977 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(3414L), BGl_string1849z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_optz00_753); 
FAILURE(BgL_auxz00_1977,BFALSE,BFALSE);} 
BgL_arg1074z00_757 = 
CAR(BgL_pairz00_1260); } 
{ /* Ieee/control.scm 89 */
 obj_t BgL_arg1076z00_759;
{ /* Ieee/control.scm 89 */
 obj_t BgL_pairz00_1261;
if(
PAIRP(BgL_optz00_753))
{ /* Ieee/control.scm 89 */
BgL_pairz00_1261 = BgL_optz00_753; }  else 
{ 
 obj_t BgL_auxz00_1984;
BgL_auxz00_1984 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(3430L), BGl_string1849z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_optz00_753); 
FAILURE(BgL_auxz00_1984,BFALSE,BFALSE);} 
BgL_arg1076z00_759 = 
CDR(BgL_pairz00_1261); } 
BgL_arg1075z00_758 = 
BGl_loopze74ze7zz__r4_control_features_6_9z00(BgL_arg1076z00_759); } 
return 
MAKE_YOUNG_PAIR(BgL_arg1074z00_757, BgL_arg1075z00_758);}  else 
{ /* Ieee/control.scm 90 */
 obj_t BgL_pairz00_1262;
if(
PAIRP(BgL_optz00_753))
{ /* Ieee/control.scm 90 */
BgL_pairz00_1262 = BgL_optz00_753; }  else 
{ 
 obj_t BgL_auxz00_1993;
BgL_auxz00_1993 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(3451L), BGl_string1849z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_optz00_753); 
FAILURE(BgL_auxz00_1993,BFALSE,BFALSE);} 
return 
CAR(BgL_pairz00_1262);} } } 

}



/* &apply */
obj_t BGl_z62applyz62zz__r4_control_features_6_9z00(obj_t BgL_envz00_1401, obj_t BgL_procz00_1402, obj_t BgL_argsz00_1403, obj_t BgL_optz00_1404)
{
{ /* Ieee/control.scm 85 */
{ /* Ieee/control.scm 86 */
 obj_t BgL_auxz00_1998;
if(
PROCEDUREP(BgL_procz00_1402))
{ /* Ieee/control.scm 86 */
BgL_auxz00_1998 = BgL_procz00_1402
; }  else 
{ 
 obj_t BgL_auxz00_2001;
BgL_auxz00_2001 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(3301L), BGl_string1851z00zz__r4_control_features_6_9z00, BGl_string1852z00zz__r4_control_features_6_9z00, BgL_procz00_1402); 
FAILURE(BgL_auxz00_2001,BFALSE,BFALSE);} 
return 
BGl_applyz00zz__r4_control_features_6_9z00(BgL_auxz00_1998, BgL_argsz00_1403, BgL_optz00_1404);} } 

}



/* map-2 */
BGL_EXPORTED_DEF obj_t BGl_mapzd22zd2zz__r4_control_features_6_9z00(obj_t BgL_fz00_7, obj_t BgL_lz00_8)
{
{ /* Ieee/control.scm 97 */
{ 
 obj_t BgL_lz00_764; obj_t BgL_resz00_765;
BgL_lz00_764 = BgL_lz00_8; 
BgL_resz00_765 = BNIL; 
BgL_zc3z04anonymousza31079ze3z87_766:
if(
NULLP(BgL_lz00_764))
{ /* Ieee/control.scm 100 */
return 
bgl_reverse_bang(BgL_resz00_765);}  else 
{ /* Ieee/control.scm 102 */
 obj_t BgL_arg1082z00_768; obj_t BgL_arg1083z00_769;
{ /* Ieee/control.scm 102 */
 obj_t BgL_pairz00_1263;
if(
PAIRP(BgL_lz00_764))
{ /* Ieee/control.scm 102 */
BgL_pairz00_1263 = BgL_lz00_764; }  else 
{ 
 obj_t BgL_auxz00_2011;
BgL_auxz00_2011 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(3833L), BGl_string1853z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_lz00_764); 
FAILURE(BgL_auxz00_2011,BFALSE,BFALSE);} 
BgL_arg1082z00_768 = 
CDR(BgL_pairz00_1263); } 
{ /* Ieee/control.scm 102 */
 obj_t BgL_arg1084z00_770;
{ /* Ieee/control.scm 102 */
 obj_t BgL_arg1085z00_771;
{ /* Ieee/control.scm 102 */
 obj_t BgL_pairz00_1264;
if(
PAIRP(BgL_lz00_764))
{ /* Ieee/control.scm 102 */
BgL_pairz00_1264 = BgL_lz00_764; }  else 
{ 
 obj_t BgL_auxz00_2018;
BgL_auxz00_2018 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(3850L), BGl_string1853z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_lz00_764); 
FAILURE(BgL_auxz00_2018,BFALSE,BFALSE);} 
BgL_arg1085z00_771 = 
CAR(BgL_pairz00_1264); } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_fz00_7, 1))
{ /* Ieee/control.scm 102 */
BgL_arg1084z00_770 = 
BGL_PROCEDURE_CALL1(BgL_fz00_7, BgL_arg1085z00_771); }  else 
{ /* Ieee/control.scm 102 */
FAILURE(BGl_string1854z00zz__r4_control_features_6_9z00,BGl_list1855z00zz__r4_control_features_6_9z00,BgL_fz00_7);} } 
BgL_arg1083z00_769 = 
MAKE_YOUNG_PAIR(BgL_arg1084z00_770, BgL_resz00_765); } 
{ 
 obj_t BgL_resz00_2032; obj_t BgL_lz00_2031;
BgL_lz00_2031 = BgL_arg1082z00_768; 
BgL_resz00_2032 = BgL_arg1083z00_769; 
BgL_resz00_765 = BgL_resz00_2032; 
BgL_lz00_764 = BgL_lz00_2031; 
goto BgL_zc3z04anonymousza31079ze3z87_766;} } } } 

}



/* &map-2 */
obj_t BGl_z62mapzd22zb0zz__r4_control_features_6_9z00(obj_t BgL_envz00_1405, obj_t BgL_fz00_1406, obj_t BgL_lz00_1407)
{
{ /* Ieee/control.scm 97 */
{ /* Ieee/control.scm 100 */
 obj_t BgL_auxz00_2040; obj_t BgL_auxz00_2033;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_lz00_1407))
{ /* Ieee/control.scm 100 */
BgL_auxz00_2040 = BgL_lz00_1407
; }  else 
{ 
 obj_t BgL_auxz00_2043;
BgL_auxz00_2043 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(3787L), BGl_string1862z00zz__r4_control_features_6_9z00, BGl_string1863z00zz__r4_control_features_6_9z00, BgL_lz00_1407); 
FAILURE(BgL_auxz00_2043,BFALSE,BFALSE);} 
if(
PROCEDUREP(BgL_fz00_1406))
{ /* Ieee/control.scm 100 */
BgL_auxz00_2033 = BgL_fz00_1406
; }  else 
{ 
 obj_t BgL_auxz00_2036;
BgL_auxz00_2036 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(3787L), BGl_string1862z00zz__r4_control_features_6_9z00, BGl_string1852z00zz__r4_control_features_6_9z00, BgL_fz00_1406); 
FAILURE(BgL_auxz00_2036,BFALSE,BFALSE);} 
return 
BGl_mapzd22zd2zz__r4_control_features_6_9z00(BgL_auxz00_2033, BgL_auxz00_2040);} } 

}



/* map */
BGL_EXPORTED_DEF obj_t BGl_mapz00zz__r4_control_features_6_9z00(obj_t BgL_fz00_9, obj_t BgL_lz00_10)
{
{ /* Ieee/control.scm 107 */
if(
NULLP(BgL_lz00_10))
{ /* Ieee/control.scm 109 */
return BNIL;}  else 
{ /* Ieee/control.scm 111 */
 bool_t BgL_test2091z00_2050;
{ /* Ieee/control.scm 111 */
 obj_t BgL_tmpz00_2051;
{ /* Ieee/control.scm 111 */
 obj_t BgL_pairz00_1265;
if(
PAIRP(BgL_lz00_10))
{ /* Ieee/control.scm 111 */
BgL_pairz00_1265 = BgL_lz00_10; }  else 
{ 
 obj_t BgL_auxz00_2054;
BgL_auxz00_2054 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(4163L), BGl_string1864z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_lz00_10); 
FAILURE(BgL_auxz00_2054,BFALSE,BFALSE);} 
BgL_tmpz00_2051 = 
CDR(BgL_pairz00_1265); } 
BgL_test2091z00_2050 = 
NULLP(BgL_tmpz00_2051); } 
if(BgL_test2091z00_2050)
{ /* Ieee/control.scm 112 */
 obj_t BgL_arg1090z00_776;
{ /* Ieee/control.scm 112 */
 obj_t BgL_pairz00_1266;
if(
PAIRP(BgL_lz00_10))
{ /* Ieee/control.scm 112 */
BgL_pairz00_1266 = BgL_lz00_10; }  else 
{ 
 obj_t BgL_auxz00_2062;
BgL_auxz00_2062 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(4188L), BGl_string1864z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_lz00_10); 
FAILURE(BgL_auxz00_2062,BFALSE,BFALSE);} 
BgL_arg1090z00_776 = 
CAR(BgL_pairz00_1266); } 
{ /* Ieee/control.scm 112 */
 obj_t BgL_auxz00_2067;
{ /* Ieee/control.scm 112 */
 bool_t BgL_test2094z00_2068;
if(
PAIRP(BgL_arg1090z00_776))
{ /* Ieee/control.scm 112 */
BgL_test2094z00_2068 = ((bool_t)1)
; }  else 
{ /* Ieee/control.scm 112 */
BgL_test2094z00_2068 = 
NULLP(BgL_arg1090z00_776)
; } 
if(BgL_test2094z00_2068)
{ /* Ieee/control.scm 112 */
BgL_auxz00_2067 = BgL_arg1090z00_776
; }  else 
{ 
 obj_t BgL_auxz00_2072;
BgL_auxz00_2072 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(4189L), BGl_string1864z00zz__r4_control_features_6_9z00, BGl_string1863z00zz__r4_control_features_6_9z00, BgL_arg1090z00_776); 
FAILURE(BgL_auxz00_2072,BFALSE,BFALSE);} } 
return 
BGl_mapzd22zd2zz__r4_control_features_6_9z00(BgL_fz00_9, BgL_auxz00_2067);} }  else 
{ /* Ieee/control.scm 111 */
BGL_TAIL return 
BGl_loopze73ze7zz__r4_control_features_6_9z00(BgL_fz00_9, BgL_lz00_10);} } } 

}



/* loop~3 */
obj_t BGl_loopze73ze7zz__r4_control_features_6_9z00(obj_t BgL_fz00_1485, obj_t BgL_lz00_778)
{
{ /* Ieee/control.scm 114 */
{ /* Ieee/control.scm 115 */
 bool_t BgL_test2096z00_2078;
{ /* Ieee/control.scm 115 */
 obj_t BgL_tmpz00_2079;
{ /* Ieee/control.scm 115 */
 obj_t BgL_pairz00_1267;
if(
PAIRP(BgL_lz00_778))
{ /* Ieee/control.scm 115 */
BgL_pairz00_1267 = BgL_lz00_778; }  else 
{ 
 obj_t BgL_auxz00_2082;
BgL_auxz00_2082 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(4249L), BGl_string1865z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_lz00_778); 
FAILURE(BgL_auxz00_2082,BFALSE,BFALSE);} 
BgL_tmpz00_2079 = 
CAR(BgL_pairz00_1267); } 
BgL_test2096z00_2078 = 
NULLP(BgL_tmpz00_2079); } 
if(BgL_test2096z00_2078)
{ /* Ieee/control.scm 115 */
return BNIL;}  else 
{ /* Ieee/control.scm 117 */
 obj_t BgL_arg1097z00_782; obj_t BgL_arg1102z00_783;
{ /* Ieee/control.scm 117 */
 obj_t BgL_valz00_1520;
{ /* Ieee/control.scm 117 */
 obj_t BgL_auxz00_2088;
{ /* Ieee/control.scm 117 */
 bool_t BgL_test2098z00_2089;
if(
PAIRP(BgL_lz00_778))
{ /* Ieee/control.scm 117 */
BgL_test2098z00_2089 = ((bool_t)1)
; }  else 
{ /* Ieee/control.scm 117 */
BgL_test2098z00_2089 = 
NULLP(BgL_lz00_778)
; } 
if(BgL_test2098z00_2089)
{ /* Ieee/control.scm 117 */
BgL_auxz00_2088 = BgL_lz00_778
; }  else 
{ 
 obj_t BgL_auxz00_2093;
BgL_auxz00_2093 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(4297L), BGl_string1865z00zz__r4_control_features_6_9z00, BGl_string1863z00zz__r4_control_features_6_9z00, BgL_lz00_778); 
FAILURE(BgL_auxz00_2093,BFALSE,BFALSE);} } 
BgL_valz00_1520 = 
BGl_mapzd22zd2zz__r4_control_features_6_9z00(BGl_carzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_auxz00_2088); } 
{ /* Ieee/control.scm 117 */
 int BgL_len1642z00_1521;
BgL_len1642z00_1521 = 
(int)(
bgl_list_length(BgL_valz00_1520)); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_fz00_1485, BgL_len1642z00_1521))
{ /* Ieee/control.scm 117 */
BgL_arg1097z00_782 = 
apply(BgL_fz00_1485, BgL_valz00_1520); }  else 
{ /* Ieee/control.scm 117 */
FAILURE(BGl_symbol1866z00zz__r4_control_features_6_9z00,BGl_string1842z00zz__r4_control_features_6_9z00,BGl_list1867z00zz__r4_control_features_6_9z00);} } } 
{ /* Ieee/control.scm 118 */
 obj_t BgL_arg1103z00_784;
{ /* Ieee/control.scm 118 */
 obj_t BgL_auxz00_2105;
{ /* Ieee/control.scm 118 */
 bool_t BgL_test2101z00_2106;
if(
PAIRP(BgL_lz00_778))
{ /* Ieee/control.scm 118 */
BgL_test2101z00_2106 = ((bool_t)1)
; }  else 
{ /* Ieee/control.scm 118 */
BgL_test2101z00_2106 = 
NULLP(BgL_lz00_778)
; } 
if(BgL_test2101z00_2106)
{ /* Ieee/control.scm 118 */
BgL_auxz00_2105 = BgL_lz00_778
; }  else 
{ 
 obj_t BgL_auxz00_2110;
BgL_auxz00_2110 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(4324L), BGl_string1865z00zz__r4_control_features_6_9z00, BGl_string1863z00zz__r4_control_features_6_9z00, BgL_lz00_778); 
FAILURE(BgL_auxz00_2110,BFALSE,BFALSE);} } 
BgL_arg1103z00_784 = 
BGl_mapzd22zd2zz__r4_control_features_6_9z00(BGl_cdrzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_auxz00_2105); } 
BgL_arg1102z00_783 = 
BGl_loopze73ze7zz__r4_control_features_6_9z00(BgL_fz00_1485, BgL_arg1103z00_784); } 
return 
MAKE_YOUNG_PAIR(BgL_arg1097z00_782, BgL_arg1102z00_783);} } } 

}



/* &map */
obj_t BGl_z62mapz62zz__r4_control_features_6_9z00(obj_t BgL_envz00_1408, obj_t BgL_fz00_1409, obj_t BgL_lz00_1410)
{
{ /* Ieee/control.scm 107 */
{ /* Ieee/control.scm 109 */
 obj_t BgL_auxz00_2117;
if(
PROCEDUREP(BgL_fz00_1409))
{ /* Ieee/control.scm 109 */
BgL_auxz00_2117 = BgL_fz00_1409
; }  else 
{ 
 obj_t BgL_auxz00_2120;
BgL_auxz00_2120 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(4121L), BGl_string1875z00zz__r4_control_features_6_9z00, BGl_string1852z00zz__r4_control_features_6_9z00, BgL_fz00_1409); 
FAILURE(BgL_auxz00_2120,BFALSE,BFALSE);} 
return 
BGl_mapz00zz__r4_control_features_6_9z00(BgL_auxz00_2117, BgL_lz00_1410);} } 

}



/* map-2! */
obj_t BGl_mapzd22z12zc0zz__r4_control_features_6_9z00(obj_t BgL_fz00_11, obj_t BgL_l0z00_12)
{
{ /* Ieee/control.scm 123 */
{ 
 obj_t BgL_lz00_789;
BgL_lz00_789 = BgL_l0z00_12; 
BgL_zc3z04anonymousza31115ze3z87_790:
if(
NULLP(BgL_lz00_789))
{ /* Ieee/control.scm 125 */
return BgL_l0z00_12;}  else 
{ /* Ieee/control.scm 125 */
{ /* Ieee/control.scm 128 */
 obj_t BgL_arg1122z00_792;
{ /* Ieee/control.scm 128 */
 obj_t BgL_arg1123z00_793;
{ /* Ieee/control.scm 128 */
 obj_t BgL_pairz00_1268;
if(
PAIRP(BgL_lz00_789))
{ /* Ieee/control.scm 128 */
BgL_pairz00_1268 = BgL_lz00_789; }  else 
{ 
 obj_t BgL_auxz00_2129;
BgL_auxz00_2129 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(4663L), BGl_string1853z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_lz00_789); 
FAILURE(BgL_auxz00_2129,BFALSE,BFALSE);} 
BgL_arg1123z00_793 = 
CAR(BgL_pairz00_1268); } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_fz00_11, 1))
{ /* Ieee/control.scm 128 */
BgL_arg1122z00_792 = 
BGL_PROCEDURE_CALL1(BgL_fz00_11, BgL_arg1123z00_793); }  else 
{ /* Ieee/control.scm 128 */
FAILURE(BGl_string1854z00zz__r4_control_features_6_9z00,BGl_list1876z00zz__r4_control_features_6_9z00,BgL_fz00_11);} } 
{ /* Ieee/control.scm 128 */
 obj_t BgL_pairz00_1269;
if(
PAIRP(BgL_lz00_789))
{ /* Ieee/control.scm 128 */
BgL_pairz00_1269 = BgL_lz00_789; }  else 
{ 
 obj_t BgL_auxz00_2143;
BgL_auxz00_2143 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(4653L), BGl_string1853z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_lz00_789); 
FAILURE(BgL_auxz00_2143,BFALSE,BFALSE);} 
SET_CAR(BgL_pairz00_1269, BgL_arg1122z00_792); } } 
{ /* Ieee/control.scm 129 */
 obj_t BgL_arg1125z00_794;
{ /* Ieee/control.scm 129 */
 obj_t BgL_pairz00_1270;
if(
PAIRP(BgL_lz00_789))
{ /* Ieee/control.scm 129 */
BgL_pairz00_1270 = BgL_lz00_789; }  else 
{ 
 obj_t BgL_auxz00_2150;
BgL_auxz00_2150 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(4685L), BGl_string1853z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_lz00_789); 
FAILURE(BgL_auxz00_2150,BFALSE,BFALSE);} 
BgL_arg1125z00_794 = 
CDR(BgL_pairz00_1270); } 
{ 
 obj_t BgL_lz00_2155;
BgL_lz00_2155 = BgL_arg1125z00_794; 
BgL_lz00_789 = BgL_lz00_2155; 
goto BgL_zc3z04anonymousza31115ze3z87_790;} } } } } 

}



/* map! */
BGL_EXPORTED_DEF obj_t BGl_mapz12z12zz__r4_control_features_6_9z00(obj_t BgL_fz00_13, obj_t BgL_lz00_14)
{
{ /* Ieee/control.scm 134 */
if(
NULLP(BgL_lz00_14))
{ /* Ieee/control.scm 136 */
return BNIL;}  else 
{ /* Ieee/control.scm 138 */
 bool_t BgL_test2110z00_2158;
{ /* Ieee/control.scm 138 */
 obj_t BgL_tmpz00_2159;
{ /* Ieee/control.scm 138 */
 obj_t BgL_pairz00_1271;
if(
PAIRP(BgL_lz00_14))
{ /* Ieee/control.scm 138 */
BgL_pairz00_1271 = BgL_lz00_14; }  else 
{ 
 obj_t BgL_auxz00_2162;
BgL_auxz00_2162 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(4994L), BGl_string1879z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_lz00_14); 
FAILURE(BgL_auxz00_2162,BFALSE,BFALSE);} 
BgL_tmpz00_2159 = 
CDR(BgL_pairz00_1271); } 
BgL_test2110z00_2158 = 
NULLP(BgL_tmpz00_2159); } 
if(BgL_test2110z00_2158)
{ /* Ieee/control.scm 139 */
 obj_t BgL_arg1129z00_799;
{ /* Ieee/control.scm 139 */
 obj_t BgL_pairz00_1272;
if(
PAIRP(BgL_lz00_14))
{ /* Ieee/control.scm 139 */
BgL_pairz00_1272 = BgL_lz00_14; }  else 
{ 
 obj_t BgL_auxz00_2170;
BgL_auxz00_2170 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(5020L), BGl_string1879z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_lz00_14); 
FAILURE(BgL_auxz00_2170,BFALSE,BFALSE);} 
BgL_arg1129z00_799 = 
CAR(BgL_pairz00_1272); } 
{ /* Ieee/control.scm 139 */
 obj_t BgL_aux1658z00_1538;
BgL_aux1658z00_1538 = 
BGl_mapzd22z12zc0zz__r4_control_features_6_9z00(BgL_fz00_13, BgL_arg1129z00_799); 
{ /* Ieee/control.scm 139 */
 bool_t BgL_test2113z00_2176;
if(
PAIRP(BgL_aux1658z00_1538))
{ /* Ieee/control.scm 139 */
BgL_test2113z00_2176 = ((bool_t)1)
; }  else 
{ /* Ieee/control.scm 139 */
BgL_test2113z00_2176 = 
NULLP(BgL_aux1658z00_1538)
; } 
if(BgL_test2113z00_2176)
{ /* Ieee/control.scm 139 */
return BgL_aux1658z00_1538;}  else 
{ 
 obj_t BgL_auxz00_2180;
BgL_auxz00_2180 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(5005L), BGl_string1879z00zz__r4_control_features_6_9z00, BGl_string1863z00zz__r4_control_features_6_9z00, BgL_aux1658z00_1538); 
FAILURE(BgL_auxz00_2180,BFALSE,BFALSE);} } } }  else 
{ /* Ieee/control.scm 141 */
 obj_t BgL_l0z00_800;
{ /* Ieee/control.scm 141 */
 obj_t BgL_pairz00_1273;
if(
PAIRP(BgL_lz00_14))
{ /* Ieee/control.scm 141 */
BgL_pairz00_1273 = BgL_lz00_14; }  else 
{ 
 obj_t BgL_auxz00_2186;
BgL_auxz00_2186 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(5059L), BGl_string1879z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_lz00_14); 
FAILURE(BgL_auxz00_2186,BFALSE,BFALSE);} 
BgL_l0z00_800 = 
CAR(BgL_pairz00_1273); } 
{ 
 obj_t BgL_lz00_802;
{ /* Ieee/control.scm 142 */
 obj_t BgL_aux1673z00_1555;
BgL_lz00_802 = BgL_lz00_14; 
BgL_zc3z04anonymousza31130ze3z87_803:
{ /* Ieee/control.scm 143 */
 bool_t BgL_test2116z00_2191;
{ /* Ieee/control.scm 143 */
 obj_t BgL_tmpz00_2192;
{ /* Ieee/control.scm 143 */
 obj_t BgL_pairz00_1274;
if(
PAIRP(BgL_lz00_802))
{ /* Ieee/control.scm 143 */
BgL_pairz00_1274 = BgL_lz00_802; }  else 
{ 
 obj_t BgL_auxz00_2195;
BgL_auxz00_2195 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(5107L), BGl_string1853z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_lz00_802); 
FAILURE(BgL_auxz00_2195,BFALSE,BFALSE);} 
BgL_tmpz00_2192 = 
CAR(BgL_pairz00_1274); } 
BgL_test2116z00_2191 = 
NULLP(BgL_tmpz00_2192); } 
if(BgL_test2116z00_2191)
{ /* Ieee/control.scm 143 */
BgL_aux1673z00_1555 = BgL_l0z00_800; }  else 
{ /* Ieee/control.scm 143 */
{ /* Ieee/control.scm 146 */
 obj_t BgL_arg1137z00_806; obj_t BgL_arg1138z00_807;
{ /* Ieee/control.scm 146 */
 obj_t BgL_pairz00_1275;
if(
PAIRP(BgL_lz00_802))
{ /* Ieee/control.scm 146 */
BgL_pairz00_1275 = BgL_lz00_802; }  else 
{ 
 obj_t BgL_auxz00_2203;
BgL_auxz00_2203 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(5148L), BGl_string1853z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_lz00_802); 
FAILURE(BgL_auxz00_2203,BFALSE,BFALSE);} 
BgL_arg1137z00_806 = 
CAR(BgL_pairz00_1275); } 
{ /* Ieee/control.scm 146 */
 obj_t BgL_valz00_1549;
{ /* Ieee/control.scm 146 */
 obj_t BgL_auxz00_2208;
{ /* Ieee/control.scm 146 */
 bool_t BgL_test2119z00_2209;
if(
PAIRP(BgL_lz00_802))
{ /* Ieee/control.scm 146 */
BgL_test2119z00_2209 = ((bool_t)1)
; }  else 
{ /* Ieee/control.scm 146 */
BgL_test2119z00_2209 = 
NULLP(BgL_lz00_802)
; } 
if(BgL_test2119z00_2209)
{ /* Ieee/control.scm 146 */
BgL_auxz00_2208 = BgL_lz00_802
; }  else 
{ 
 obj_t BgL_auxz00_2213;
BgL_auxz00_2213 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(5171L), BGl_string1853z00zz__r4_control_features_6_9z00, BGl_string1863z00zz__r4_control_features_6_9z00, BgL_lz00_802); 
FAILURE(BgL_auxz00_2213,BFALSE,BFALSE);} } 
BgL_valz00_1549 = 
BGl_mapzd22zd2zz__r4_control_features_6_9z00(BGl_carzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_auxz00_2208); } 
{ /* Ieee/control.scm 146 */
 int BgL_len1668z00_1550;
BgL_len1668z00_1550 = 
(int)(
bgl_list_length(BgL_valz00_1549)); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_fz00_13, BgL_len1668z00_1550))
{ /* Ieee/control.scm 146 */
BgL_arg1138z00_807 = 
apply(BgL_fz00_13, BgL_valz00_1549); }  else 
{ /* Ieee/control.scm 146 */
FAILURE(BGl_symbol1880z00zz__r4_control_features_6_9z00,BGl_string1842z00zz__r4_control_features_6_9z00,BGl_list1867z00zz__r4_control_features_6_9z00);} } } 
{ /* Ieee/control.scm 146 */
 obj_t BgL_pairz00_1276;
if(
PAIRP(BgL_arg1137z00_806))
{ /* Ieee/control.scm 146 */
BgL_pairz00_1276 = BgL_arg1137z00_806; }  else 
{ 
 obj_t BgL_auxz00_2227;
BgL_auxz00_2227 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(5149L), BGl_string1853z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_arg1137z00_806); 
FAILURE(BgL_auxz00_2227,BFALSE,BFALSE);} 
SET_CAR(BgL_pairz00_1276, BgL_arg1138z00_807); } } 
{ /* Ieee/control.scm 147 */
 obj_t BgL_arg1140z00_808;
{ /* Ieee/control.scm 147 */
 obj_t BgL_auxz00_2232;
{ /* Ieee/control.scm 147 */
 bool_t BgL_test2123z00_2233;
if(
PAIRP(BgL_lz00_802))
{ /* Ieee/control.scm 147 */
BgL_test2123z00_2233 = ((bool_t)1)
; }  else 
{ /* Ieee/control.scm 147 */
BgL_test2123z00_2233 = 
NULLP(BgL_lz00_802)
; } 
if(BgL_test2123z00_2233)
{ /* Ieee/control.scm 147 */
BgL_auxz00_2232 = BgL_lz00_802
; }  else 
{ 
 obj_t BgL_auxz00_2237;
BgL_auxz00_2237 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(5199L), BGl_string1853z00zz__r4_control_features_6_9z00, BGl_string1863z00zz__r4_control_features_6_9z00, BgL_lz00_802); 
FAILURE(BgL_auxz00_2237,BFALSE,BFALSE);} } 
BgL_arg1140z00_808 = 
BGl_mapzd22zd2zz__r4_control_features_6_9z00(BGl_cdrzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_auxz00_2232); } 
{ 
 obj_t BgL_lz00_2242;
BgL_lz00_2242 = BgL_arg1140z00_808; 
BgL_lz00_802 = BgL_lz00_2242; 
goto BgL_zc3z04anonymousza31130ze3z87_803;} } } } 
{ /* Ieee/control.scm 142 */
 bool_t BgL_test2125z00_2243;
if(
PAIRP(BgL_aux1673z00_1555))
{ /* Ieee/control.scm 142 */
BgL_test2125z00_2243 = ((bool_t)1)
; }  else 
{ /* Ieee/control.scm 142 */
BgL_test2125z00_2243 = 
NULLP(BgL_aux1673z00_1555)
; } 
if(BgL_test2125z00_2243)
{ /* Ieee/control.scm 142 */
return BgL_aux1673z00_1555;}  else 
{ 
 obj_t BgL_auxz00_2247;
BgL_auxz00_2247 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(5067L), BGl_string1879z00zz__r4_control_features_6_9z00, BGl_string1863z00zz__r4_control_features_6_9z00, BgL_aux1673z00_1555); 
FAILURE(BgL_auxz00_2247,BFALSE,BFALSE);} } } } } } } 

}



/* &map! */
obj_t BGl_z62mapz12z70zz__r4_control_features_6_9z00(obj_t BgL_envz00_1415, obj_t BgL_fz00_1416, obj_t BgL_lz00_1417)
{
{ /* Ieee/control.scm 134 */
{ /* Ieee/control.scm 136 */
 obj_t BgL_auxz00_2251;
if(
PROCEDUREP(BgL_fz00_1416))
{ /* Ieee/control.scm 136 */
BgL_auxz00_2251 = BgL_fz00_1416
; }  else 
{ 
 obj_t BgL_auxz00_2254;
BgL_auxz00_2254 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(4952L), BGl_string1881z00zz__r4_control_features_6_9z00, BGl_string1852z00zz__r4_control_features_6_9z00, BgL_fz00_1416); 
FAILURE(BgL_auxz00_2254,BFALSE,BFALSE);} 
return 
BGl_mapz12z12zz__r4_control_features_6_9z00(BgL_auxz00_2251, BgL_lz00_1417);} } 

}



/* loop~2 */
obj_t BGl_loopze72ze7zz__r4_control_features_6_9z00(obj_t BgL_fz00_1484, obj_t BgL_lz00_813)
{
{ /* Ieee/control.scm 153 */
if(
NULLP(BgL_lz00_813))
{ /* Ieee/control.scm 154 */
return BNIL;}  else 
{ /* Ieee/control.scm 156 */
 obj_t BgL_arg1145z00_816; obj_t BgL_arg1148z00_817;
{ /* Ieee/control.scm 156 */
 obj_t BgL_arg1149z00_818;
{ /* Ieee/control.scm 156 */
 obj_t BgL_pairz00_1277;
if(
PAIRP(BgL_lz00_813))
{ /* Ieee/control.scm 156 */
BgL_pairz00_1277 = BgL_lz00_813; }  else 
{ 
 obj_t BgL_auxz00_2263;
BgL_auxz00_2263 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(5526L), BGl_string1882z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_lz00_813); 
FAILURE(BgL_auxz00_2263,BFALSE,BFALSE);} 
BgL_arg1149z00_818 = 
CAR(BgL_pairz00_1277); } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_fz00_1484, 1))
{ /* Ieee/control.scm 156 */
BgL_arg1145z00_816 = 
BGL_PROCEDURE_CALL1(BgL_fz00_1484, BgL_arg1149z00_818); }  else 
{ /* Ieee/control.scm 156 */
FAILURE(BGl_string1883z00zz__r4_control_features_6_9z00,BGl_list1884z00zz__r4_control_features_6_9z00,BgL_fz00_1484);} } 
{ /* Ieee/control.scm 156 */
 obj_t BgL_arg1152z00_819;
{ /* Ieee/control.scm 156 */
 obj_t BgL_pairz00_1278;
if(
PAIRP(BgL_lz00_813))
{ /* Ieee/control.scm 156 */
BgL_pairz00_1278 = BgL_lz00_813; }  else 
{ 
 obj_t BgL_auxz00_2277;
BgL_auxz00_2277 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(5541L), BGl_string1882z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_lz00_813); 
FAILURE(BgL_auxz00_2277,BFALSE,BFALSE);} 
BgL_arg1152z00_819 = 
CDR(BgL_pairz00_1278); } 
BgL_arg1148z00_817 = 
BGl_loopze72ze7zz__r4_control_features_6_9z00(BgL_fz00_1484, BgL_arg1152z00_819); } 
{ /* Ieee/control.scm 156 */
 obj_t BgL_auxz00_2283;
{ /* Ieee/control.scm 156 */
 bool_t BgL_test2132z00_2284;
if(
PAIRP(BgL_arg1145z00_816))
{ /* Ieee/control.scm 156 */
BgL_test2132z00_2284 = ((bool_t)1)
; }  else 
{ /* Ieee/control.scm 156 */
BgL_test2132z00_2284 = 
NULLP(BgL_arg1145z00_816)
; } 
if(BgL_test2132z00_2284)
{ /* Ieee/control.scm 156 */
BgL_auxz00_2283 = BgL_arg1145z00_816
; }  else 
{ 
 obj_t BgL_auxz00_2288;
BgL_auxz00_2288 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(5510L), BGl_string1882z00zz__r4_control_features_6_9z00, BGl_string1863z00zz__r4_control_features_6_9z00, BgL_arg1145z00_816); 
FAILURE(BgL_auxz00_2288,BFALSE,BFALSE);} } 
return 
BGl_appendzd221011zd2zz__r4_control_features_6_9z00(BgL_auxz00_2283, BgL_arg1148z00_817);} } } 

}



/* append-map */
BGL_EXPORTED_DEF obj_t BGl_appendzd2mapzd2zz__r4_control_features_6_9z00(obj_t BgL_fz00_17, obj_t BgL_lz00_18)
{
{ /* Ieee/control.scm 161 */
if(
NULLP(BgL_lz00_18))
{ /* Ieee/control.scm 163 */
return BNIL;}  else 
{ /* Ieee/control.scm 165 */
 bool_t BgL_test2135z00_2295;
{ /* Ieee/control.scm 165 */
 obj_t BgL_tmpz00_2296;
{ /* Ieee/control.scm 165 */
 obj_t BgL_pairz00_1279;
if(
PAIRP(BgL_lz00_18))
{ /* Ieee/control.scm 165 */
BgL_pairz00_1279 = BgL_lz00_18; }  else 
{ 
 obj_t BgL_auxz00_2299;
BgL_auxz00_2299 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(5856L), BGl_string1887z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_lz00_18); 
FAILURE(BgL_auxz00_2299,BFALSE,BFALSE);} 
BgL_tmpz00_2296 = 
CDR(BgL_pairz00_1279); } 
BgL_test2135z00_2295 = 
NULLP(BgL_tmpz00_2296); } 
if(BgL_test2135z00_2295)
{ /* Ieee/control.scm 166 */
 obj_t BgL_arg1157z00_824;
{ /* Ieee/control.scm 166 */
 obj_t BgL_pairz00_1280;
if(
PAIRP(BgL_lz00_18))
{ /* Ieee/control.scm 166 */
BgL_pairz00_1280 = BgL_lz00_18; }  else 
{ 
 obj_t BgL_auxz00_2307;
BgL_auxz00_2307 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(5887L), BGl_string1887z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_lz00_18); 
FAILURE(BgL_auxz00_2307,BFALSE,BFALSE);} 
BgL_arg1157z00_824 = 
CAR(BgL_pairz00_1280); } 
{ /* Ieee/control.scm 166 */
 obj_t BgL_aux1688z00_1571;
BgL_aux1688z00_1571 = 
BGl_loopze72ze7zz__r4_control_features_6_9z00(BgL_fz00_17, BgL_arg1157z00_824); 
{ /* Ieee/control.scm 166 */
 bool_t BgL_test2138z00_2313;
if(
PAIRP(BgL_aux1688z00_1571))
{ /* Ieee/control.scm 166 */
BgL_test2138z00_2313 = ((bool_t)1)
; }  else 
{ /* Ieee/control.scm 166 */
BgL_test2138z00_2313 = 
NULLP(BgL_aux1688z00_1571)
; } 
if(BgL_test2138z00_2313)
{ /* Ieee/control.scm 166 */
return BgL_aux1688z00_1571;}  else 
{ 
 obj_t BgL_auxz00_2317;
BgL_auxz00_2317 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(5867L), BGl_string1887z00zz__r4_control_features_6_9z00, BGl_string1863z00zz__r4_control_features_6_9z00, BgL_aux1688z00_1571); 
FAILURE(BgL_auxz00_2317,BFALSE,BFALSE);} } } }  else 
{ /* Ieee/control.scm 168 */
 obj_t BgL_aux1690z00_1573;
BgL_aux1690z00_1573 = 
BGl_loopze71ze7zz__r4_control_features_6_9z00(BgL_fz00_17, BgL_lz00_18); 
{ /* Ieee/control.scm 168 */
 bool_t BgL_test2140z00_2322;
if(
PAIRP(BgL_aux1690z00_1573))
{ /* Ieee/control.scm 168 */
BgL_test2140z00_2322 = ((bool_t)1)
; }  else 
{ /* Ieee/control.scm 168 */
BgL_test2140z00_2322 = 
NULLP(BgL_aux1690z00_1573)
; } 
if(BgL_test2140z00_2322)
{ /* Ieee/control.scm 168 */
return BgL_aux1690z00_1573;}  else 
{ 
 obj_t BgL_auxz00_2326;
BgL_auxz00_2326 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(5911L), BGl_string1887z00zz__r4_control_features_6_9z00, BGl_string1863z00zz__r4_control_features_6_9z00, BgL_aux1690z00_1573); 
FAILURE(BgL_auxz00_2326,BFALSE,BFALSE);} } } } } 

}



/* loop~1 */
obj_t BGl_loopze71ze7zz__r4_control_features_6_9z00(obj_t BgL_fz00_1483, obj_t BgL_lz00_826)
{
{ /* Ieee/control.scm 168 */
{ /* Ieee/control.scm 169 */
 bool_t BgL_test2142z00_2330;
{ /* Ieee/control.scm 169 */
 obj_t BgL_tmpz00_2331;
{ /* Ieee/control.scm 169 */
 obj_t BgL_pairz00_1281;
if(
PAIRP(BgL_lz00_826))
{ /* Ieee/control.scm 169 */
BgL_pairz00_1281 = BgL_lz00_826; }  else 
{ 
 obj_t BgL_auxz00_2334;
BgL_auxz00_2334 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(5948L), BGl_string1888z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_lz00_826); 
FAILURE(BgL_auxz00_2334,BFALSE,BFALSE);} 
BgL_tmpz00_2331 = 
CAR(BgL_pairz00_1281); } 
BgL_test2142z00_2330 = 
NULLP(BgL_tmpz00_2331); } 
if(BgL_test2142z00_2330)
{ /* Ieee/control.scm 169 */
return BNIL;}  else 
{ /* Ieee/control.scm 171 */
 obj_t BgL_arg1164z00_830; obj_t BgL_arg1166z00_831;
{ /* Ieee/control.scm 171 */
 obj_t BgL_valz00_1580;
{ /* Ieee/control.scm 171 */
 obj_t BgL_auxz00_2340;
{ /* Ieee/control.scm 171 */
 bool_t BgL_test2144z00_2341;
if(
PAIRP(BgL_lz00_826))
{ /* Ieee/control.scm 171 */
BgL_test2144z00_2341 = ((bool_t)1)
; }  else 
{ /* Ieee/control.scm 171 */
BgL_test2144z00_2341 = 
NULLP(BgL_lz00_826)
; } 
if(BgL_test2144z00_2341)
{ /* Ieee/control.scm 171 */
BgL_auxz00_2340 = BgL_lz00_826
; }  else 
{ 
 obj_t BgL_auxz00_2345;
BgL_auxz00_2345 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(5998L), BGl_string1888z00zz__r4_control_features_6_9z00, BGl_string1863z00zz__r4_control_features_6_9z00, BgL_lz00_826); 
FAILURE(BgL_auxz00_2345,BFALSE,BFALSE);} } 
BgL_valz00_1580 = 
BGl_mapzd22zd2zz__r4_control_features_6_9z00(BGl_carzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_auxz00_2340); } 
{ /* Ieee/control.scm 171 */
 int BgL_len1696z00_1581;
BgL_len1696z00_1581 = 
(int)(
bgl_list_length(BgL_valz00_1580)); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_fz00_1483, BgL_len1696z00_1581))
{ /* Ieee/control.scm 171 */
BgL_arg1164z00_830 = 
apply(BgL_fz00_1483, BgL_valz00_1580); }  else 
{ /* Ieee/control.scm 171 */
FAILURE(BGl_symbol1889z00zz__r4_control_features_6_9z00,BGl_string1842z00zz__r4_control_features_6_9z00,BGl_list1867z00zz__r4_control_features_6_9z00);} } } 
{ /* Ieee/control.scm 171 */
 obj_t BgL_arg1171z00_832;
{ /* Ieee/control.scm 171 */
 obj_t BgL_auxz00_2357;
{ /* Ieee/control.scm 171 */
 bool_t BgL_test2147z00_2358;
if(
PAIRP(BgL_lz00_826))
{ /* Ieee/control.scm 171 */
BgL_test2147z00_2358 = ((bool_t)1)
; }  else 
{ /* Ieee/control.scm 171 */
BgL_test2147z00_2358 = 
NULLP(BgL_lz00_826)
; } 
if(BgL_test2147z00_2358)
{ /* Ieee/control.scm 171 */
BgL_auxz00_2357 = BgL_lz00_826
; }  else 
{ 
 obj_t BgL_auxz00_2362;
BgL_auxz00_2362 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(6019L), BGl_string1888z00zz__r4_control_features_6_9z00, BGl_string1863z00zz__r4_control_features_6_9z00, BgL_lz00_826); 
FAILURE(BgL_auxz00_2362,BFALSE,BFALSE);} } 
BgL_arg1171z00_832 = 
BGl_mapzd22zd2zz__r4_control_features_6_9z00(BGl_cdrzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_auxz00_2357); } 
BgL_arg1166z00_831 = 
BGl_loopze71ze7zz__r4_control_features_6_9z00(BgL_fz00_1483, BgL_arg1171z00_832); } 
{ /* Ieee/control.scm 171 */
 obj_t BgL_auxz00_2368;
{ /* Ieee/control.scm 171 */
 bool_t BgL_test2149z00_2369;
if(
PAIRP(BgL_arg1164z00_830))
{ /* Ieee/control.scm 171 */
BgL_test2149z00_2369 = ((bool_t)1)
; }  else 
{ /* Ieee/control.scm 171 */
BgL_test2149z00_2369 = 
NULLP(BgL_arg1164z00_830)
; } 
if(BgL_test2149z00_2369)
{ /* Ieee/control.scm 171 */
BgL_auxz00_2368 = BgL_arg1164z00_830
; }  else 
{ 
 obj_t BgL_auxz00_2373;
BgL_auxz00_2373 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(5970L), BGl_string1888z00zz__r4_control_features_6_9z00, BGl_string1863z00zz__r4_control_features_6_9z00, BgL_arg1164z00_830); 
FAILURE(BgL_auxz00_2373,BFALSE,BFALSE);} } 
return 
BGl_appendzd221011zd2zz__r4_control_features_6_9z00(BgL_auxz00_2368, BgL_arg1166z00_831);} } } } 

}



/* &append-map */
obj_t BGl_z62appendzd2mapzb0zz__r4_control_features_6_9z00(obj_t BgL_envz00_1418, obj_t BgL_fz00_1419, obj_t BgL_lz00_1420)
{
{ /* Ieee/control.scm 161 */
{ /* Ieee/control.scm 163 */
 obj_t BgL_auxz00_2378;
if(
PROCEDUREP(BgL_fz00_1419))
{ /* Ieee/control.scm 163 */
BgL_auxz00_2378 = BgL_fz00_1419
; }  else 
{ 
 obj_t BgL_auxz00_2381;
BgL_auxz00_2381 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(5814L), BGl_string1890z00zz__r4_control_features_6_9z00, BGl_string1852z00zz__r4_control_features_6_9z00, BgL_fz00_1419); 
FAILURE(BgL_auxz00_2381,BFALSE,BFALSE);} 
return 
BGl_appendzd2mapzd2zz__r4_control_features_6_9z00(BgL_auxz00_2378, BgL_lz00_1420);} } 

}



/* append-map2! */
obj_t BGl_appendzd2map2z12zc0zz__r4_control_features_6_9z00(obj_t BgL_fz00_19, obj_t BgL_lz00_20)
{
{ /* Ieee/control.scm 176 */
if(
NULLP(BgL_lz00_20))
{ /* Ieee/control.scm 177 */
return BNIL;}  else 
{ /* Ieee/control.scm 179 */
 obj_t BgL_resultz00_837;
{ /* Ieee/control.scm 179 */
 obj_t BgL_list1189z00_849;
BgL_list1189z00_849 = 
MAKE_YOUNG_PAIR(BFALSE, BNIL); 
BgL_resultz00_837 = BgL_list1189z00_849; } 
{ /* Ieee/control.scm 179 */
 obj_t BgL_lpairz00_838;
BgL_lpairz00_838 = BgL_resultz00_837; 
{ /* Ieee/control.scm 180 */

{ 
 obj_t BgL_l1028z00_840;
BgL_l1028z00_840 = BgL_lz00_20; 
BgL_zc3z04anonymousza31184ze3z87_841:
if(
PAIRP(BgL_l1028z00_840))
{ /* Ieee/control.scm 181 */
{ /* Ieee/control.scm 182 */
 obj_t BgL_xz00_843;
BgL_xz00_843 = 
CAR(BgL_l1028z00_840); 
{ /* Ieee/control.scm 182 */
 obj_t BgL_result2z00_844;
if(
PROCEDURE_CORRECT_ARITYP(BgL_fz00_19, 1))
{ /* Ieee/control.scm 182 */
BgL_result2z00_844 = 
BGL_PROCEDURE_CALL1(BgL_fz00_19, BgL_xz00_843); }  else 
{ /* Ieee/control.scm 182 */
FAILURE(BGl_string1891z00zz__r4_control_features_6_9z00,BGl_list1892z00zz__r4_control_features_6_9z00,BgL_fz00_19);} 
if(
PAIRP(BgL_result2z00_844))
{ /* Ieee/control.scm 183 */
{ /* Ieee/control.scm 184 */
 obj_t BgL_pairz00_1284;
{ /* Ieee/control.scm 184 */
 obj_t BgL_aux1704z00_1590;
BgL_aux1704z00_1590 = BgL_lpairz00_838; 
if(
NULLP(BgL_aux1704z00_1590))
{ 
 obj_t BgL_auxz00_2403;
BgL_auxz00_2403 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(6465L), BGl_string1895z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_aux1704z00_1590); 
FAILURE(BgL_auxz00_2403,BFALSE,BFALSE);}  else 
{ /* Ieee/control.scm 184 */
BgL_pairz00_1284 = BgL_aux1704z00_1590; } } 
SET_CDR(BgL_pairz00_1284, BgL_result2z00_844); } 
BgL_lpairz00_838 = 
BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(BgL_result2z00_844); }  else 
{ /* Ieee/control.scm 183 */BFALSE; } } } 
{ 
 obj_t BgL_l1028z00_2409;
BgL_l1028z00_2409 = 
CDR(BgL_l1028z00_840); 
BgL_l1028z00_840 = BgL_l1028z00_2409; 
goto BgL_zc3z04anonymousza31184ze3z87_841;} }  else 
{ /* Ieee/control.scm 181 */
if(
NULLP(BgL_l1028z00_840))
{ /* Ieee/control.scm 181 */BTRUE; }  else 
{ /* Ieee/control.scm 181 */
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BGl_string1896z00zz__r4_control_features_6_9z00, BGl_string1897z00zz__r4_control_features_6_9z00, BgL_l1028z00_840, BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(6366L)); } } } 
{ /* Ieee/control.scm 187 */
 obj_t BgL_pairz00_1286;
if(
NULLP(BgL_resultz00_837))
{ 
 obj_t BgL_auxz00_2417;
BgL_auxz00_2417 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(6541L), BGl_string1898z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_resultz00_837); 
FAILURE(BgL_auxz00_2417,BFALSE,BFALSE);}  else 
{ /* Ieee/control.scm 187 */
BgL_pairz00_1286 = BgL_resultz00_837; } 
return 
CDR(BgL_pairz00_1286);} } } } } 

}



/* append-map! */
BGL_EXPORTED_DEF obj_t BGl_appendzd2mapz12zc0zz__r4_control_features_6_9z00(obj_t BgL_fz00_21, obj_t BgL_lz00_22)
{
{ /* Ieee/control.scm 192 */
if(
NULLP(BgL_lz00_22))
{ /* Ieee/control.scm 194 */
return BNIL;}  else 
{ /* Ieee/control.scm 196 */
 bool_t BgL_test2160z00_2424;
{ /* Ieee/control.scm 196 */
 obj_t BgL_tmpz00_2425;
{ /* Ieee/control.scm 196 */
 obj_t BgL_pairz00_1287;
if(
PAIRP(BgL_lz00_22))
{ /* Ieee/control.scm 196 */
BgL_pairz00_1287 = BgL_lz00_22; }  else 
{ 
 obj_t BgL_auxz00_2428;
BgL_auxz00_2428 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(6860L), BGl_string1899z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_lz00_22); 
FAILURE(BgL_auxz00_2428,BFALSE,BFALSE);} 
BgL_tmpz00_2425 = 
CDR(BgL_pairz00_1287); } 
BgL_test2160z00_2424 = 
NULLP(BgL_tmpz00_2425); } 
if(BgL_test2160z00_2424)
{ /* Ieee/control.scm 197 */
 obj_t BgL_arg1193z00_853;
{ /* Ieee/control.scm 197 */
 obj_t BgL_pairz00_1288;
if(
PAIRP(BgL_lz00_22))
{ /* Ieee/control.scm 197 */
BgL_pairz00_1288 = BgL_lz00_22; }  else 
{ 
 obj_t BgL_auxz00_2436;
BgL_auxz00_2436 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(6892L), BGl_string1899z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_lz00_22); 
FAILURE(BgL_auxz00_2436,BFALSE,BFALSE);} 
BgL_arg1193z00_853 = 
CAR(BgL_pairz00_1288); } 
{ /* Ieee/control.scm 197 */
 obj_t BgL_aux1712z00_1598;
BgL_aux1712z00_1598 = 
BGl_appendzd2map2z12zc0zz__r4_control_features_6_9z00(BgL_fz00_21, BgL_arg1193z00_853); 
{ /* Ieee/control.scm 197 */
 bool_t BgL_test2163z00_2442;
if(
PAIRP(BgL_aux1712z00_1598))
{ /* Ieee/control.scm 197 */
BgL_test2163z00_2442 = ((bool_t)1)
; }  else 
{ /* Ieee/control.scm 197 */
BgL_test2163z00_2442 = 
NULLP(BgL_aux1712z00_1598)
; } 
if(BgL_test2163z00_2442)
{ /* Ieee/control.scm 197 */
return BgL_aux1712z00_1598;}  else 
{ 
 obj_t BgL_auxz00_2446;
BgL_auxz00_2446 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(6871L), BGl_string1899z00zz__r4_control_features_6_9z00, BGl_string1863z00zz__r4_control_features_6_9z00, BgL_aux1712z00_1598); 
FAILURE(BgL_auxz00_2446,BFALSE,BFALSE);} } } }  else 
{ /* Ieee/control.scm 199 */
 bool_t BgL_test2165z00_2450;
{ /* Ieee/control.scm 199 */
 obj_t BgL_tmpz00_2451;
{ /* Ieee/control.scm 199 */
 obj_t BgL_pairz00_1289;
if(
PAIRP(BgL_lz00_22))
{ /* Ieee/control.scm 199 */
BgL_pairz00_1289 = BgL_lz00_22; }  else 
{ 
 obj_t BgL_auxz00_2454;
BgL_auxz00_2454 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(6932L), BGl_string1899z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_lz00_22); 
FAILURE(BgL_auxz00_2454,BFALSE,BFALSE);} 
BgL_tmpz00_2451 = 
CAR(BgL_pairz00_1289); } 
BgL_test2165z00_2450 = 
NULLP(BgL_tmpz00_2451); } 
if(BgL_test2165z00_2450)
{ /* Ieee/control.scm 199 */
return BNIL;}  else 
{ /* Ieee/control.scm 201 */
 obj_t BgL_resultz00_856;
{ /* Ieee/control.scm 201 */
 obj_t BgL_list1202z00_867;
BgL_list1202z00_867 = 
MAKE_YOUNG_PAIR(BFALSE, BNIL); 
BgL_resultz00_856 = BgL_list1202z00_867; } 
{ /* Ieee/control.scm 201 */
 obj_t BgL_lpairz00_1430;
BgL_lpairz00_1430 = 
MAKE_CELL(BgL_resultz00_856); 
{ /* Ieee/control.scm 202 */

{ /* Ieee/control.scm 203 */
 obj_t BgL_runner1201z00_866;
{ /* Ieee/control.scm 204 */
 obj_t BgL_zc3z04anonymousza31198ze3z87_1421;
BgL_zc3z04anonymousza31198ze3z87_1421 = 
MAKE_VA_PROCEDURE(BGl_z62zc3z04anonymousza31198ze3ze5zz__r4_control_features_6_9z00, 
(int)(-1L), 
(int)(2L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31198ze3z87_1421, 
(int)(0L), BgL_fz00_21); 
PROCEDURE_SET(BgL_zc3z04anonymousza31198ze3z87_1421, 
(int)(1L), 
((obj_t)BgL_lpairz00_1430)); 
{ /* Ieee/control.scm 203 */
 obj_t BgL_list1197z00_859;
BgL_list1197z00_859 = 
MAKE_YOUNG_PAIR(BgL_lz00_22, BNIL); 
BgL_runner1201z00_866 = 
BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(BgL_zc3z04anonymousza31198ze3z87_1421, BgL_list1197z00_859); } } 
{ /* Ieee/control.scm 203 */
 obj_t BgL_aux1200z00_865;
{ /* Ieee/control.scm 203 */
 obj_t BgL_pairz00_1292;
{ /* Ieee/control.scm 203 */
 obj_t BgL_aux1716z00_1602;
BgL_aux1716z00_1602 = BgL_runner1201z00_866; 
if(
PAIRP(BgL_aux1716z00_1602))
{ /* Ieee/control.scm 203 */
BgL_pairz00_1292 = BgL_aux1716z00_1602; }  else 
{ 
 obj_t BgL_auxz00_2473;
BgL_auxz00_2473 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(7008L), BGl_string1899z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_aux1716z00_1602); 
FAILURE(BgL_auxz00_2473,BFALSE,BFALSE);} } 
{ /* Ieee/control.scm 203 */
 obj_t BgL_aux1718z00_1604;
BgL_aux1718z00_1604 = 
CAR(BgL_pairz00_1292); 
if(
PROCEDUREP(BgL_aux1718z00_1604))
{ /* Ieee/control.scm 203 */
BgL_aux1200z00_865 = BgL_aux1718z00_1604; }  else 
{ 
 obj_t BgL_auxz00_2480;
BgL_auxz00_2480 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(7008L), BGl_string1899z00zz__r4_control_features_6_9z00, BGl_string1852z00zz__r4_control_features_6_9z00, BgL_aux1718z00_1604); 
FAILURE(BgL_auxz00_2480,BFALSE,BFALSE);} } } 
{ /* Ieee/control.scm 203 */
 obj_t BgL_pairz00_1293;
{ /* Ieee/control.scm 203 */
 obj_t BgL_aux1720z00_1606;
BgL_aux1720z00_1606 = BgL_runner1201z00_866; 
if(
PAIRP(BgL_aux1720z00_1606))
{ /* Ieee/control.scm 203 */
BgL_pairz00_1293 = BgL_aux1720z00_1606; }  else 
{ 
 obj_t BgL_auxz00_2486;
BgL_auxz00_2486 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(7008L), BGl_string1899z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_aux1720z00_1606); 
FAILURE(BgL_auxz00_2486,BFALSE,BFALSE);} } 
BgL_runner1201z00_866 = 
CDR(BgL_pairz00_1293); } 
BGl_forzd2eachzd2zz__r4_control_features_6_9z00(BgL_aux1200z00_865, BgL_runner1201z00_866); } } 
{ /* Ieee/control.scm 209 */
 obj_t BgL_pairz00_1294;
if(
NULLP(BgL_resultz00_856))
{ 
 obj_t BgL_auxz00_2494;
BgL_auxz00_2494 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(7208L), BGl_string1899z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_resultz00_856); 
FAILURE(BgL_auxz00_2494,BFALSE,BFALSE);}  else 
{ /* Ieee/control.scm 209 */
BgL_pairz00_1294 = BgL_resultz00_856; } 
{ /* Ieee/control.scm 209 */
 obj_t BgL_aux1724z00_1610;
BgL_aux1724z00_1610 = 
CDR(BgL_pairz00_1294); 
{ /* Ieee/control.scm 209 */
 bool_t BgL_test2171z00_2499;
if(
PAIRP(BgL_aux1724z00_1610))
{ /* Ieee/control.scm 209 */
BgL_test2171z00_2499 = ((bool_t)1)
; }  else 
{ /* Ieee/control.scm 209 */
BgL_test2171z00_2499 = 
NULLP(BgL_aux1724z00_1610)
; } 
if(BgL_test2171z00_2499)
{ /* Ieee/control.scm 209 */
return BgL_aux1724z00_1610;}  else 
{ 
 obj_t BgL_auxz00_2503;
BgL_auxz00_2503 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(7203L), BGl_string1899z00zz__r4_control_features_6_9z00, BGl_string1863z00zz__r4_control_features_6_9z00, BgL_aux1724z00_1610); 
FAILURE(BgL_auxz00_2503,BFALSE,BFALSE);} } } } } } } } } } 

}



/* &append-map! */
obj_t BGl_z62appendzd2mapz12za2zz__r4_control_features_6_9z00(obj_t BgL_envz00_1422, obj_t BgL_fz00_1423, obj_t BgL_lz00_1424)
{
{ /* Ieee/control.scm 192 */
{ /* Ieee/control.scm 194 */
 obj_t BgL_auxz00_2507;
if(
PROCEDUREP(BgL_fz00_1423))
{ /* Ieee/control.scm 194 */
BgL_auxz00_2507 = BgL_fz00_1423
; }  else 
{ 
 obj_t BgL_auxz00_2510;
BgL_auxz00_2510 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(6818L), BGl_string1900z00zz__r4_control_features_6_9z00, BGl_string1852z00zz__r4_control_features_6_9z00, BgL_fz00_1423); 
FAILURE(BgL_auxz00_2510,BFALSE,BFALSE);} 
return 
BGl_appendzd2mapz12zc0zz__r4_control_features_6_9z00(BgL_auxz00_2507, BgL_lz00_1424);} } 

}



/* &<@anonymous:1198> */
obj_t BGl_z62zc3z04anonymousza31198ze3ze5zz__r4_control_features_6_9z00(obj_t BgL_envz00_1425, obj_t BgL_xsz00_1428)
{
{ /* Ieee/control.scm 203 */
{ /* Ieee/control.scm 204 */
 obj_t BgL_fz00_1426; obj_t BgL_lpairz00_1427;
BgL_fz00_1426 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_1425, 
(int)(0L))); 
BgL_lpairz00_1427 = 
PROCEDURE_REF(BgL_envz00_1425, 
(int)(1L)); 
{ /* Ieee/control.scm 204 */
 obj_t BgL_result2z00_1796;
{ /* Ieee/control.scm 204 */
 int BgL_len1728z00_1797;
BgL_len1728z00_1797 = 
(int)(
bgl_list_length(
((obj_t)BgL_xsz00_1428))); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_fz00_1426, BgL_len1728z00_1797))
{ /* Ieee/control.scm 204 */
BgL_result2z00_1796 = 
apply(BgL_fz00_1426, 
((obj_t)BgL_xsz00_1428)); }  else 
{ /* Ieee/control.scm 204 */
FAILURE(BGl_symbol1901z00zz__r4_control_features_6_9z00,BGl_string1842z00zz__r4_control_features_6_9z00,BGl_list1903z00zz__r4_control_features_6_9z00);} } 
if(
PAIRP(BgL_result2z00_1796))
{ /* Ieee/control.scm 205 */
{ /* Ieee/control.scm 206 */
 obj_t BgL_pairz00_1798;
{ /* Ieee/control.scm 206 */
 obj_t BgL_aux1729z00_1799;
BgL_aux1729z00_1799 = 
CELL_REF(BgL_lpairz00_1427); 
if(
PAIRP(BgL_aux1729z00_1799))
{ /* Ieee/control.scm 206 */
BgL_pairz00_1798 = BgL_aux1729z00_1799; }  else 
{ 
 obj_t BgL_auxz00_2533;
BgL_auxz00_2533 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(7121L), BGl_string1902z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_aux1729z00_1799); 
FAILURE(BgL_auxz00_2533,BFALSE,BFALSE);} } 
SET_CDR(BgL_pairz00_1798, BgL_result2z00_1796); } 
{ /* Ieee/control.scm 207 */
 obj_t BgL_auxz00_1800;
BgL_auxz00_1800 = 
BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(BgL_result2z00_1796); 
return 
CELL_SET(BgL_lpairz00_1427, BgL_auxz00_1800);} }  else 
{ /* Ieee/control.scm 205 */
return BFALSE;} } } } 

}



/* filter-map-2 */
obj_t BGl_filterzd2mapzd22z00zz__r4_control_features_6_9z00(obj_t BgL_fz00_23, obj_t BgL_lz00_24)
{
{ /* Ieee/control.scm 214 */
{ 
 obj_t BgL_lz00_872; obj_t BgL_resz00_873;
BgL_lz00_872 = BgL_lz00_24; 
BgL_resz00_873 = BNIL; 
BgL_zc3z04anonymousza31207ze3z87_874:
if(
NULLP(BgL_lz00_872))
{ /* Ieee/control.scm 217 */
return 
bgl_reverse_bang(BgL_resz00_873);}  else 
{ /* Ieee/control.scm 219 */
 obj_t BgL_hdz00_876;
{ /* Ieee/control.scm 219 */
 obj_t BgL_arg1215z00_880;
{ /* Ieee/control.scm 219 */
 obj_t BgL_pairz00_1295;
if(
PAIRP(BgL_lz00_872))
{ /* Ieee/control.scm 219 */
BgL_pairz00_1295 = BgL_lz00_872; }  else 
{ 
 obj_t BgL_auxz00_2544;
BgL_auxz00_2544 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(7570L), BGl_string1853z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_lz00_872); 
FAILURE(BgL_auxz00_2544,BFALSE,BFALSE);} 
BgL_arg1215z00_880 = 
CAR(BgL_pairz00_1295); } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_fz00_23, 1))
{ /* Ieee/control.scm 219 */
BgL_hdz00_876 = 
BGL_PROCEDURE_CALL1(BgL_fz00_23, BgL_arg1215z00_880); }  else 
{ /* Ieee/control.scm 219 */
FAILURE(BGl_string1854z00zz__r4_control_features_6_9z00,BGl_list1906z00zz__r4_control_features_6_9z00,BgL_fz00_23);} } 
if(
CBOOL(BgL_hdz00_876))
{ /* Ieee/control.scm 221 */
 obj_t BgL_arg1209z00_877; obj_t BgL_arg1210z00_878;
{ /* Ieee/control.scm 221 */
 obj_t BgL_pairz00_1296;
if(
PAIRP(BgL_lz00_872))
{ /* Ieee/control.scm 221 */
BgL_pairz00_1296 = BgL_lz00_872; }  else 
{ 
 obj_t BgL_auxz00_2560;
BgL_auxz00_2560 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(7603L), BGl_string1853z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_lz00_872); 
FAILURE(BgL_auxz00_2560,BFALSE,BFALSE);} 
BgL_arg1209z00_877 = 
CDR(BgL_pairz00_1296); } 
BgL_arg1210z00_878 = 
MAKE_YOUNG_PAIR(BgL_hdz00_876, BgL_resz00_873); 
{ 
 obj_t BgL_resz00_2567; obj_t BgL_lz00_2566;
BgL_lz00_2566 = BgL_arg1209z00_877; 
BgL_resz00_2567 = BgL_arg1210z00_878; 
BgL_resz00_873 = BgL_resz00_2567; 
BgL_lz00_872 = BgL_lz00_2566; 
goto BgL_zc3z04anonymousza31207ze3z87_874;} }  else 
{ /* Ieee/control.scm 222 */
 obj_t BgL_arg1212z00_879;
{ /* Ieee/control.scm 222 */
 obj_t BgL_pairz00_1297;
if(
PAIRP(BgL_lz00_872))
{ /* Ieee/control.scm 222 */
BgL_pairz00_1297 = BgL_lz00_872; }  else 
{ 
 obj_t BgL_auxz00_2570;
BgL_auxz00_2570 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(7635L), BGl_string1853z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_lz00_872); 
FAILURE(BgL_auxz00_2570,BFALSE,BFALSE);} 
BgL_arg1212z00_879 = 
CDR(BgL_pairz00_1297); } 
{ 
 obj_t BgL_lz00_2575;
BgL_lz00_2575 = BgL_arg1212z00_879; 
BgL_lz00_872 = BgL_lz00_2575; 
goto BgL_zc3z04anonymousza31207ze3z87_874;} } } } } 

}



/* filter-map */
BGL_EXPORTED_DEF obj_t BGl_filterzd2mapzd2zz__r4_control_features_6_9z00(obj_t BgL_fz00_25, obj_t BgL_lz00_26)
{
{ /* Ieee/control.scm 227 */
if(
NULLP(BgL_lz00_26))
{ /* Ieee/control.scm 229 */
return BNIL;}  else 
{ /* Ieee/control.scm 231 */
 bool_t BgL_test2184z00_2578;
{ /* Ieee/control.scm 231 */
 obj_t BgL_tmpz00_2579;
{ /* Ieee/control.scm 231 */
 obj_t BgL_pairz00_1298;
if(
PAIRP(BgL_lz00_26))
{ /* Ieee/control.scm 231 */
BgL_pairz00_1298 = BgL_lz00_26; }  else 
{ 
 obj_t BgL_auxz00_2582;
BgL_auxz00_2582 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(7955L), BGl_string1909z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_lz00_26); 
FAILURE(BgL_auxz00_2582,BFALSE,BFALSE);} 
BgL_tmpz00_2579 = 
CDR(BgL_pairz00_1298); } 
BgL_test2184z00_2578 = 
NULLP(BgL_tmpz00_2579); } 
if(BgL_test2184z00_2578)
{ /* Ieee/control.scm 232 */
 obj_t BgL_arg1220z00_885;
{ /* Ieee/control.scm 232 */
 obj_t BgL_pairz00_1299;
if(
PAIRP(BgL_lz00_26))
{ /* Ieee/control.scm 232 */
BgL_pairz00_1299 = BgL_lz00_26; }  else 
{ 
 obj_t BgL_auxz00_2590;
BgL_auxz00_2590 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(7987L), BGl_string1909z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_lz00_26); 
FAILURE(BgL_auxz00_2590,BFALSE,BFALSE);} 
BgL_arg1220z00_885 = 
CAR(BgL_pairz00_1299); } 
BGL_TAIL return 
BGl_filterzd2mapzd22z00zz__r4_control_features_6_9z00(BgL_fz00_25, BgL_arg1220z00_885);}  else 
{ /* Ieee/control.scm 231 */
BGL_TAIL return 
BGl_loopze70ze7zz__r4_control_features_6_9z00(BgL_fz00_25, BgL_lz00_26);} } } 

}



/* loop~0 */
obj_t BGl_loopze70ze7zz__r4_control_features_6_9z00(obj_t BgL_fz00_1482, obj_t BgL_lz00_887)
{
{ /* Ieee/control.scm 234 */
BGl_loopze70ze7zz__r4_control_features_6_9z00:
{ /* Ieee/control.scm 235 */
 bool_t BgL_test2187z00_2597;
{ /* Ieee/control.scm 235 */
 obj_t BgL_tmpz00_2598;
{ /* Ieee/control.scm 235 */
 obj_t BgL_pairz00_1300;
if(
PAIRP(BgL_lz00_887))
{ /* Ieee/control.scm 235 */
BgL_pairz00_1300 = BgL_lz00_887; }  else 
{ 
 obj_t BgL_auxz00_2601;
BgL_auxz00_2601 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(8048L), BGl_string1910z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_lz00_887); 
FAILURE(BgL_auxz00_2601,BFALSE,BFALSE);} 
BgL_tmpz00_2598 = 
CAR(BgL_pairz00_1300); } 
BgL_test2187z00_2597 = 
NULLP(BgL_tmpz00_2598); } 
if(BgL_test2187z00_2597)
{ /* Ieee/control.scm 235 */
return BNIL;}  else 
{ /* Ieee/control.scm 237 */
 obj_t BgL_hdz00_891;
{ /* Ieee/control.scm 237 */
 obj_t BgL_valz00_1636;
{ /* Ieee/control.scm 237 */
 obj_t BgL_auxz00_2607;
{ /* Ieee/control.scm 237 */
 bool_t BgL_test2189z00_2608;
if(
PAIRP(BgL_lz00_887))
{ /* Ieee/control.scm 237 */
BgL_test2189z00_2608 = ((bool_t)1)
; }  else 
{ /* Ieee/control.scm 237 */
BgL_test2189z00_2608 = 
NULLP(BgL_lz00_887)
; } 
if(BgL_test2189z00_2608)
{ /* Ieee/control.scm 237 */
BgL_auxz00_2607 = BgL_lz00_887
; }  else 
{ 
 obj_t BgL_auxz00_2612;
BgL_auxz00_2612 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(8100L), BGl_string1910z00zz__r4_control_features_6_9z00, BGl_string1863z00zz__r4_control_features_6_9z00, BgL_lz00_887); 
FAILURE(BgL_auxz00_2612,BFALSE,BFALSE);} } 
BgL_valz00_1636 = 
BGl_mapzd22zd2zz__r4_control_features_6_9z00(BGl_carzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_auxz00_2607); } 
{ /* Ieee/control.scm 237 */
 int BgL_len1746z00_1637;
BgL_len1746z00_1637 = 
(int)(
bgl_list_length(BgL_valz00_1636)); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_fz00_1482, BgL_len1746z00_1637))
{ /* Ieee/control.scm 237 */
BgL_hdz00_891 = 
apply(BgL_fz00_1482, BgL_valz00_1636); }  else 
{ /* Ieee/control.scm 237 */
FAILURE(BGl_symbol1911z00zz__r4_control_features_6_9z00,BGl_string1842z00zz__r4_control_features_6_9z00,BGl_list1867z00zz__r4_control_features_6_9z00);} } } 
if(
CBOOL(BgL_hdz00_891))
{ /* Ieee/control.scm 239 */
 obj_t BgL_arg1225z00_892;
{ /* Ieee/control.scm 239 */
 obj_t BgL_arg1226z00_893;
{ /* Ieee/control.scm 239 */
 obj_t BgL_auxz00_2626;
{ /* Ieee/control.scm 239 */
 bool_t BgL_test2193z00_2627;
if(
PAIRP(BgL_lz00_887))
{ /* Ieee/control.scm 239 */
BgL_test2193z00_2627 = ((bool_t)1)
; }  else 
{ /* Ieee/control.scm 239 */
BgL_test2193z00_2627 = 
NULLP(BgL_lz00_887)
; } 
if(BgL_test2193z00_2627)
{ /* Ieee/control.scm 239 */
BgL_auxz00_2626 = BgL_lz00_887
; }  else 
{ 
 obj_t BgL_auxz00_2631;
BgL_auxz00_2631 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(8149L), BGl_string1910z00zz__r4_control_features_6_9z00, BGl_string1863z00zz__r4_control_features_6_9z00, BgL_lz00_887); 
FAILURE(BgL_auxz00_2631,BFALSE,BFALSE);} } 
BgL_arg1226z00_893 = 
BGl_mapzd22zd2zz__r4_control_features_6_9z00(BGl_cdrzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_auxz00_2626); } 
BgL_arg1225z00_892 = 
BGl_loopze70ze7zz__r4_control_features_6_9z00(BgL_fz00_1482, BgL_arg1226z00_893); } 
return 
MAKE_YOUNG_PAIR(BgL_hdz00_891, BgL_arg1225z00_892);}  else 
{ /* Ieee/control.scm 240 */
 obj_t BgL_arg1227z00_894;
{ /* Ieee/control.scm 240 */
 obj_t BgL_auxz00_2638;
{ /* Ieee/control.scm 240 */
 bool_t BgL_test2195z00_2639;
if(
PAIRP(BgL_lz00_887))
{ /* Ieee/control.scm 240 */
BgL_test2195z00_2639 = ((bool_t)1)
; }  else 
{ /* Ieee/control.scm 240 */
BgL_test2195z00_2639 = 
NULLP(BgL_lz00_887)
; } 
if(BgL_test2195z00_2639)
{ /* Ieee/control.scm 240 */
BgL_auxz00_2638 = BgL_lz00_887
; }  else 
{ 
 obj_t BgL_auxz00_2643;
BgL_auxz00_2643 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(8178L), BGl_string1910z00zz__r4_control_features_6_9z00, BGl_string1863z00zz__r4_control_features_6_9z00, BgL_lz00_887); 
FAILURE(BgL_auxz00_2643,BFALSE,BFALSE);} } 
BgL_arg1227z00_894 = 
BGl_mapzd22zd2zz__r4_control_features_6_9z00(BGl_cdrzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_auxz00_2638); } 
{ 
 obj_t BgL_lz00_2648;
BgL_lz00_2648 = BgL_arg1227z00_894; 
BgL_lz00_887 = BgL_lz00_2648; 
goto BGl_loopze70ze7zz__r4_control_features_6_9z00;} } } } } 

}



/* &filter-map */
obj_t BGl_z62filterzd2mapzb0zz__r4_control_features_6_9z00(obj_t BgL_envz00_1432, obj_t BgL_fz00_1433, obj_t BgL_lz00_1434)
{
{ /* Ieee/control.scm 227 */
{ /* Ieee/control.scm 229 */
 obj_t BgL_auxz00_2649;
if(
PROCEDUREP(BgL_fz00_1433))
{ /* Ieee/control.scm 229 */
BgL_auxz00_2649 = BgL_fz00_1433
; }  else 
{ 
 obj_t BgL_auxz00_2652;
BgL_auxz00_2652 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(7913L), BGl_string1912z00zz__r4_control_features_6_9z00, BGl_string1852z00zz__r4_control_features_6_9z00, BgL_fz00_1433); 
FAILURE(BgL_auxz00_2652,BFALSE,BFALSE);} 
return 
BGl_filterzd2mapzd2zz__r4_control_features_6_9z00(BgL_auxz00_2649, BgL_lz00_1434);} } 

}



/* for-each-2 */
BGL_EXPORTED_DEF obj_t BGl_forzd2eachzd22z00zz__r4_control_features_6_9z00(obj_t BgL_fz00_27, obj_t BgL_lz00_28)
{
{ /* Ieee/control.scm 245 */
{ 
 obj_t BgL_lz00_899;
BgL_lz00_899 = BgL_lz00_28; 
BgL_zc3z04anonymousza31230ze3z87_900:
if(
NULLP(BgL_lz00_899))
{ /* Ieee/control.scm 247 */
return BUNSPEC;}  else 
{ /* Ieee/control.scm 247 */
{ /* Ieee/control.scm 250 */
 obj_t BgL_arg1232z00_902;
{ /* Ieee/control.scm 250 */
 obj_t BgL_pairz00_1301;
if(
PAIRP(BgL_lz00_899))
{ /* Ieee/control.scm 250 */
BgL_pairz00_1301 = BgL_lz00_899; }  else 
{ 
 obj_t BgL_auxz00_2661;
BgL_auxz00_2661 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(8518L), BGl_string1853z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_lz00_899); 
FAILURE(BgL_auxz00_2661,BFALSE,BFALSE);} 
BgL_arg1232z00_902 = 
CAR(BgL_pairz00_1301); } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_fz00_27, 1))
{ /* Ieee/control.scm 250 */
BGL_PROCEDURE_CALL1(BgL_fz00_27, BgL_arg1232z00_902); }  else 
{ /* Ieee/control.scm 250 */
FAILURE(BGl_string1854z00zz__r4_control_features_6_9z00,BGl_list1913z00zz__r4_control_features_6_9z00,BgL_fz00_27);} } 
{ /* Ieee/control.scm 251 */
 obj_t BgL_arg1233z00_903;
{ /* Ieee/control.scm 251 */
 obj_t BgL_pairz00_1302;
if(
PAIRP(BgL_lz00_899))
{ /* Ieee/control.scm 251 */
BgL_pairz00_1302 = BgL_lz00_899; }  else 
{ 
 obj_t BgL_auxz00_2675;
BgL_auxz00_2675 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(8539L), BGl_string1853z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_lz00_899); 
FAILURE(BgL_auxz00_2675,BFALSE,BFALSE);} 
BgL_arg1233z00_903 = 
CDR(BgL_pairz00_1302); } 
{ 
 obj_t BgL_lz00_2680;
BgL_lz00_2680 = BgL_arg1233z00_903; 
BgL_lz00_899 = BgL_lz00_2680; 
goto BgL_zc3z04anonymousza31230ze3z87_900;} } } } } 

}



/* &for-each-2 */
obj_t BGl_z62forzd2eachzd22z62zz__r4_control_features_6_9z00(obj_t BgL_envz00_1435, obj_t BgL_fz00_1436, obj_t BgL_lz00_1437)
{
{ /* Ieee/control.scm 245 */
{ /* Ieee/control.scm 247 */
 obj_t BgL_auxz00_2688; obj_t BgL_auxz00_2681;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_lz00_1437))
{ /* Ieee/control.scm 247 */
BgL_auxz00_2688 = BgL_lz00_1437
; }  else 
{ 
 obj_t BgL_auxz00_2691;
BgL_auxz00_2691 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(8464L), BGl_string1916z00zz__r4_control_features_6_9z00, BGl_string1863z00zz__r4_control_features_6_9z00, BgL_lz00_1437); 
FAILURE(BgL_auxz00_2691,BFALSE,BFALSE);} 
if(
PROCEDUREP(BgL_fz00_1436))
{ /* Ieee/control.scm 247 */
BgL_auxz00_2681 = BgL_fz00_1436
; }  else 
{ 
 obj_t BgL_auxz00_2684;
BgL_auxz00_2684 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(8464L), BGl_string1916z00zz__r4_control_features_6_9z00, BGl_string1852z00zz__r4_control_features_6_9z00, BgL_fz00_1436); 
FAILURE(BgL_auxz00_2684,BFALSE,BFALSE);} 
return 
BGl_forzd2eachzd22z00zz__r4_control_features_6_9z00(BgL_auxz00_2681, BgL_auxz00_2688);} } 

}



/* for-each */
BGL_EXPORTED_DEF obj_t BGl_forzd2eachzd2zz__r4_control_features_6_9z00(obj_t BgL_fz00_29, obj_t BgL_lz00_30)
{
{ /* Ieee/control.scm 256 */
if(
NULLP(BgL_lz00_30))
{ /* Ieee/control.scm 258 */
return BUNSPEC;}  else 
{ /* Ieee/control.scm 260 */
 bool_t BgL_test2205z00_2698;
{ /* Ieee/control.scm 260 */
 obj_t BgL_tmpz00_2699;
{ /* Ieee/control.scm 260 */
 obj_t BgL_pairz00_1303;
if(
PAIRP(BgL_lz00_30))
{ /* Ieee/control.scm 260 */
BgL_pairz00_1303 = BgL_lz00_30; }  else 
{ 
 obj_t BgL_auxz00_2702;
BgL_auxz00_2702 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(8861L), BGl_string1896z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_lz00_30); 
FAILURE(BgL_auxz00_2702,BFALSE,BFALSE);} 
BgL_tmpz00_2699 = 
CDR(BgL_pairz00_1303); } 
BgL_test2205z00_2698 = 
NULLP(BgL_tmpz00_2699); } 
if(BgL_test2205z00_2698)
{ /* Ieee/control.scm 261 */
 obj_t BgL_arg1238z00_908;
{ /* Ieee/control.scm 261 */
 obj_t BgL_pairz00_1304;
if(
PAIRP(BgL_lz00_30))
{ /* Ieee/control.scm 261 */
BgL_pairz00_1304 = BgL_lz00_30; }  else 
{ 
 obj_t BgL_auxz00_2710;
BgL_auxz00_2710 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(8891L), BGl_string1896z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_lz00_30); 
FAILURE(BgL_auxz00_2710,BFALSE,BFALSE);} 
BgL_arg1238z00_908 = 
CAR(BgL_pairz00_1304); } 
{ /* Ieee/control.scm 261 */
 obj_t BgL_auxz00_2715;
{ /* Ieee/control.scm 261 */
 bool_t BgL_test2208z00_2716;
if(
PAIRP(BgL_arg1238z00_908))
{ /* Ieee/control.scm 261 */
BgL_test2208z00_2716 = ((bool_t)1)
; }  else 
{ /* Ieee/control.scm 261 */
BgL_test2208z00_2716 = 
NULLP(BgL_arg1238z00_908)
; } 
if(BgL_test2208z00_2716)
{ /* Ieee/control.scm 261 */
BgL_auxz00_2715 = BgL_arg1238z00_908
; }  else 
{ 
 obj_t BgL_auxz00_2720;
BgL_auxz00_2720 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(8892L), BGl_string1896z00zz__r4_control_features_6_9z00, BGl_string1863z00zz__r4_control_features_6_9z00, BgL_arg1238z00_908); 
FAILURE(BgL_auxz00_2720,BFALSE,BFALSE);} } 
return 
BGl_forzd2eachzd22z00zz__r4_control_features_6_9z00(BgL_fz00_29, BgL_auxz00_2715);} }  else 
{ 
 obj_t BgL_lz00_910;
BgL_lz00_910 = BgL_lz00_30; 
BgL_zc3z04anonymousza31239ze3z87_911:
{ /* Ieee/control.scm 264 */
 bool_t BgL_test2210z00_2725;
{ /* Ieee/control.scm 264 */
 obj_t BgL_tmpz00_2726;
{ /* Ieee/control.scm 264 */
 obj_t BgL_pairz00_1305;
if(
PAIRP(BgL_lz00_910))
{ /* Ieee/control.scm 264 */
BgL_pairz00_1305 = BgL_lz00_910; }  else 
{ 
 obj_t BgL_auxz00_2729;
BgL_auxz00_2729 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(8952L), BGl_string1853z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_lz00_910); 
FAILURE(BgL_auxz00_2729,BFALSE,BFALSE);} 
BgL_tmpz00_2726 = 
CAR(BgL_pairz00_1305); } 
BgL_test2210z00_2725 = 
NULLP(BgL_tmpz00_2726); } 
if(BgL_test2210z00_2725)
{ /* Ieee/control.scm 264 */
return BUNSPEC;}  else 
{ /* Ieee/control.scm 264 */
{ /* Ieee/control.scm 267 */
 obj_t BgL_valz00_1665;
{ /* Ieee/control.scm 267 */
 obj_t BgL_auxz00_2735;
{ /* Ieee/control.scm 267 */
 bool_t BgL_test2212z00_2736;
if(
PAIRP(BgL_lz00_910))
{ /* Ieee/control.scm 267 */
BgL_test2212z00_2736 = ((bool_t)1)
; }  else 
{ /* Ieee/control.scm 267 */
BgL_test2212z00_2736 = 
NULLP(BgL_lz00_910)
; } 
if(BgL_test2212z00_2736)
{ /* Ieee/control.scm 267 */
BgL_auxz00_2735 = BgL_lz00_910
; }  else 
{ 
 obj_t BgL_auxz00_2740;
BgL_auxz00_2740 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(9013L), BGl_string1853z00zz__r4_control_features_6_9z00, BGl_string1863z00zz__r4_control_features_6_9z00, BgL_lz00_910); 
FAILURE(BgL_auxz00_2740,BFALSE,BFALSE);} } 
BgL_valz00_1665 = 
BGl_mapzd22zd2zz__r4_control_features_6_9z00(BGl_carzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_auxz00_2735); } 
{ /* Ieee/control.scm 267 */
 int BgL_len1772z00_1666;
BgL_len1772z00_1666 = 
(int)(
bgl_list_length(BgL_valz00_1665)); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_fz00_29, BgL_len1772z00_1666))
{ /* Ieee/control.scm 267 */
apply(BgL_fz00_29, BgL_valz00_1665); }  else 
{ /* Ieee/control.scm 267 */
FAILURE(BGl_symbol1880z00zz__r4_control_features_6_9z00,BGl_string1842z00zz__r4_control_features_6_9z00,BGl_list1867z00zz__r4_control_features_6_9z00);} } } 
{ /* Ieee/control.scm 268 */
 obj_t BgL_arg1244z00_914;
{ /* Ieee/control.scm 268 */
 obj_t BgL_auxz00_2752;
{ /* Ieee/control.scm 268 */
 bool_t BgL_test2215z00_2753;
if(
PAIRP(BgL_lz00_910))
{ /* Ieee/control.scm 268 */
BgL_test2215z00_2753 = ((bool_t)1)
; }  else 
{ /* Ieee/control.scm 268 */
BgL_test2215z00_2753 = 
NULLP(BgL_lz00_910)
; } 
if(BgL_test2215z00_2753)
{ /* Ieee/control.scm 268 */
BgL_auxz00_2752 = BgL_lz00_910
; }  else 
{ 
 obj_t BgL_auxz00_2757;
BgL_auxz00_2757 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(9037L), BGl_string1853z00zz__r4_control_features_6_9z00, BGl_string1863z00zz__r4_control_features_6_9z00, BgL_lz00_910); 
FAILURE(BgL_auxz00_2757,BFALSE,BFALSE);} } 
BgL_arg1244z00_914 = 
BGl_mapzd22zd2zz__r4_control_features_6_9z00(BGl_cdrzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_auxz00_2752); } 
{ 
 obj_t BgL_lz00_2762;
BgL_lz00_2762 = BgL_arg1244z00_914; 
BgL_lz00_910 = BgL_lz00_2762; 
goto BgL_zc3z04anonymousza31239ze3z87_911;} } } } } } } 

}



/* &for-each */
obj_t BGl_z62forzd2eachzb0zz__r4_control_features_6_9z00(obj_t BgL_envz00_1438, obj_t BgL_fz00_1439, obj_t BgL_lz00_1440)
{
{ /* Ieee/control.scm 256 */
{ /* Ieee/control.scm 258 */
 obj_t BgL_auxz00_2763;
if(
PROCEDUREP(BgL_fz00_1439))
{ /* Ieee/control.scm 258 */
BgL_auxz00_2763 = BgL_fz00_1439
; }  else 
{ 
 obj_t BgL_auxz00_2766;
BgL_auxz00_2766 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(8810L), BGl_string1917z00zz__r4_control_features_6_9z00, BGl_string1852z00zz__r4_control_features_6_9z00, BgL_fz00_1439); 
FAILURE(BgL_auxz00_2766,BFALSE,BFALSE);} 
return 
BGl_forzd2eachzd2zz__r4_control_features_6_9z00(BgL_auxz00_2763, BgL_lz00_1440);} } 

}



/* filter */
BGL_EXPORTED_DEF obj_t BGl_filterz00zz__r4_control_features_6_9z00(obj_t BgL_predz00_31, obj_t BgL_lz00_32)
{
{ /* Ieee/control.scm 273 */
{ /* Ieee/control.scm 274 */
 obj_t BgL_hookz00_918;
BgL_hookz00_918 = 
MAKE_YOUNG_PAIR(BFALSE, BNIL); 
{ 
 obj_t BgL_lz00_920; obj_t BgL_hz00_921;
{ /* Ieee/control.scm 275 */
 obj_t BgL_aux1786z00_1681;
BgL_lz00_920 = BgL_lz00_32; 
BgL_hz00_921 = BgL_hookz00_918; 
BgL_zc3z04anonymousza31250ze3z87_922:
if(
NULLP(BgL_lz00_920))
{ /* Ieee/control.scm 278 */
BgL_aux1786z00_1681 = 
CDR(BgL_hookz00_918); }  else 
{ /* Ieee/control.scm 280 */
 bool_t BgL_test2219z00_2775;
{ /* Ieee/control.scm 280 */
 obj_t BgL_arg1304z00_930;
{ /* Ieee/control.scm 280 */
 obj_t BgL_pairz00_1307;
if(
PAIRP(BgL_lz00_920))
{ /* Ieee/control.scm 280 */
BgL_pairz00_1307 = BgL_lz00_920; }  else 
{ 
 obj_t BgL_auxz00_2778;
BgL_auxz00_2778 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(9420L), BGl_string1853z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_lz00_920); 
FAILURE(BgL_auxz00_2778,BFALSE,BFALSE);} 
BgL_arg1304z00_930 = 
CAR(BgL_pairz00_1307); } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_predz00_31, 1))
{ /* Ieee/control.scm 280 */
BgL_test2219z00_2775 = 
CBOOL(
BGL_PROCEDURE_CALL1(BgL_predz00_31, BgL_arg1304z00_930))
; }  else 
{ /* Ieee/control.scm 280 */
FAILURE(BGl_string1854z00zz__r4_control_features_6_9z00,BGl_list1918z00zz__r4_control_features_6_9z00,BgL_predz00_31);} } 
if(BgL_test2219z00_2775)
{ /* Ieee/control.scm 281 */
 obj_t BgL_nhz00_926;
{ /* Ieee/control.scm 281 */
 obj_t BgL_arg1272z00_928;
{ /* Ieee/control.scm 281 */
 obj_t BgL_pairz00_1308;
if(
PAIRP(BgL_lz00_920))
{ /* Ieee/control.scm 281 */
BgL_pairz00_1308 = BgL_lz00_920; }  else 
{ 
 obj_t BgL_auxz00_2793;
BgL_auxz00_2793 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(9451L), BGl_string1853z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_lz00_920); 
FAILURE(BgL_auxz00_2793,BFALSE,BFALSE);} 
BgL_arg1272z00_928 = 
CAR(BgL_pairz00_1308); } 
BgL_nhz00_926 = 
MAKE_YOUNG_PAIR(BgL_arg1272z00_928, BNIL); } 
SET_CDR(BgL_hz00_921, BgL_nhz00_926); 
{ /* Ieee/control.scm 283 */
 obj_t BgL_arg1268z00_927;
{ /* Ieee/control.scm 283 */
 obj_t BgL_pairz00_1310;
if(
PAIRP(BgL_lz00_920))
{ /* Ieee/control.scm 283 */
BgL_pairz00_1310 = BgL_lz00_920; }  else 
{ 
 obj_t BgL_auxz00_2802;
BgL_auxz00_2802 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(9492L), BGl_string1853z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_lz00_920); 
FAILURE(BgL_auxz00_2802,BFALSE,BFALSE);} 
BgL_arg1268z00_927 = 
CDR(BgL_pairz00_1310); } 
{ 
 obj_t BgL_hz00_2808; obj_t BgL_lz00_2807;
BgL_lz00_2807 = BgL_arg1268z00_927; 
BgL_hz00_2808 = BgL_nhz00_926; 
BgL_hz00_921 = BgL_hz00_2808; 
BgL_lz00_920 = BgL_lz00_2807; 
goto BgL_zc3z04anonymousza31250ze3z87_922;} } }  else 
{ /* Ieee/control.scm 285 */
 obj_t BgL_arg1284z00_929;
{ /* Ieee/control.scm 285 */
 obj_t BgL_pairz00_1311;
if(
PAIRP(BgL_lz00_920))
{ /* Ieee/control.scm 285 */
BgL_pairz00_1311 = BgL_lz00_920; }  else 
{ 
 obj_t BgL_auxz00_2811;
BgL_auxz00_2811 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(9529L), BGl_string1853z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_lz00_920); 
FAILURE(BgL_auxz00_2811,BFALSE,BFALSE);} 
BgL_arg1284z00_929 = 
CDR(BgL_pairz00_1311); } 
{ 
 obj_t BgL_lz00_2816;
BgL_lz00_2816 = BgL_arg1284z00_929; 
BgL_lz00_920 = BgL_lz00_2816; 
goto BgL_zc3z04anonymousza31250ze3z87_922;} } } 
{ /* Ieee/control.scm 275 */
 bool_t BgL_test2225z00_2817;
if(
PAIRP(BgL_aux1786z00_1681))
{ /* Ieee/control.scm 275 */
BgL_test2225z00_2817 = ((bool_t)1)
; }  else 
{ /* Ieee/control.scm 275 */
BgL_test2225z00_2817 = 
NULLP(BgL_aux1786z00_1681)
; } 
if(BgL_test2225z00_2817)
{ /* Ieee/control.scm 275 */
return BgL_aux1786z00_1681;}  else 
{ 
 obj_t BgL_auxz00_2821;
BgL_auxz00_2821 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(9331L), BGl_string1923z00zz__r4_control_features_6_9z00, BGl_string1863z00zz__r4_control_features_6_9z00, BgL_aux1786z00_1681); 
FAILURE(BgL_auxz00_2821,BFALSE,BFALSE);} } } } } } 

}



/* &filter */
obj_t BGl_z62filterz62zz__r4_control_features_6_9z00(obj_t BgL_envz00_1441, obj_t BgL_predz00_1442, obj_t BgL_lz00_1443)
{
{ /* Ieee/control.scm 273 */
{ /* Ieee/control.scm 274 */
 obj_t BgL_auxz00_2832; obj_t BgL_auxz00_2825;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_lz00_1443))
{ /* Ieee/control.scm 274 */
BgL_auxz00_2832 = BgL_lz00_1443
; }  else 
{ 
 obj_t BgL_auxz00_2835;
BgL_auxz00_2835 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(9297L), BGl_string1924z00zz__r4_control_features_6_9z00, BGl_string1863z00zz__r4_control_features_6_9z00, BgL_lz00_1443); 
FAILURE(BgL_auxz00_2835,BFALSE,BFALSE);} 
if(
PROCEDUREP(BgL_predz00_1442))
{ /* Ieee/control.scm 274 */
BgL_auxz00_2825 = BgL_predz00_1442
; }  else 
{ 
 obj_t BgL_auxz00_2828;
BgL_auxz00_2828 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(9297L), BGl_string1924z00zz__r4_control_features_6_9z00, BGl_string1852z00zz__r4_control_features_6_9z00, BgL_predz00_1442); 
FAILURE(BgL_auxz00_2828,BFALSE,BFALSE);} 
return 
BGl_filterz00zz__r4_control_features_6_9z00(BgL_auxz00_2825, BgL_auxz00_2832);} } 

}



/* filter! */
BGL_EXPORTED_DEF obj_t BGl_filterz12z12zz__r4_control_features_6_9z00(obj_t BgL_predz00_33, obj_t BgL_lisz00_34)
{
{ /* Ieee/control.scm 290 */
{ 
 obj_t BgL_ansz00_933;
{ /* Ieee/control.scm 291 */
 obj_t BgL_aux1805z00_1703;
BgL_ansz00_933 = BgL_lisz00_34; 
BgL_zc3z04anonymousza31305ze3z87_934:
if(
NULLP(BgL_ansz00_933))
{ /* Ieee/control.scm 293 */
BgL_aux1805z00_1703 = BgL_ansz00_933; }  else 
{ /* Ieee/control.scm 295 */
 bool_t BgL_test2230z00_2842;
{ /* Ieee/control.scm 295 */
 obj_t BgL_arg1327z00_966;
{ /* Ieee/control.scm 295 */
 obj_t BgL_pairz00_1312;
if(
PAIRP(BgL_ansz00_933))
{ /* Ieee/control.scm 295 */
BgL_pairz00_1312 = BgL_ansz00_933; }  else 
{ 
 obj_t BgL_auxz00_2845;
BgL_auxz00_2845 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(9867L), BGl_string1925z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_ansz00_933); 
FAILURE(BgL_auxz00_2845,BFALSE,BFALSE);} 
BgL_arg1327z00_966 = 
CAR(BgL_pairz00_1312); } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_predz00_33, 1))
{ /* Ieee/control.scm 295 */
BgL_test2230z00_2842 = 
CBOOL(
BGL_PROCEDURE_CALL1(BgL_predz00_33, BgL_arg1327z00_966))
; }  else 
{ /* Ieee/control.scm 295 */
FAILURE(BGl_string1926z00zz__r4_control_features_6_9z00,BGl_list1927z00zz__r4_control_features_6_9z00,BgL_predz00_33);} } 
if(BgL_test2230z00_2842)
{ 
 obj_t BgL_prevz00_950; obj_t BgL_lisz00_951; obj_t BgL_prevz00_941; obj_t BgL_lisz00_942;
{ /* Ieee/control.scm 312 */
 obj_t BgL_arg1309z00_940;
{ /* Ieee/control.scm 312 */
 obj_t BgL_pairz00_1321;
if(
PAIRP(BgL_ansz00_933))
{ /* Ieee/control.scm 312 */
BgL_pairz00_1321 = BgL_ansz00_933; }  else 
{ 
 obj_t BgL_auxz00_2860;
BgL_auxz00_2860 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(10350L), BGl_string1925z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_ansz00_933); 
FAILURE(BgL_auxz00_2860,BFALSE,BFALSE);} 
BgL_arg1309z00_940 = 
CDR(BgL_pairz00_1321); } 
BgL_prevz00_941 = BgL_ansz00_933; 
BgL_lisz00_942 = BgL_arg1309z00_940; 
BgL_zc3z04anonymousza31310ze3z87_943:
if(
PAIRP(BgL_lisz00_942))
{ /* Ieee/control.scm 300 */
 bool_t BgL_test2235z00_2867;
{ /* Ieee/control.scm 300 */
 obj_t BgL_arg1316z00_949;
BgL_arg1316z00_949 = 
CAR(BgL_lisz00_942); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_predz00_33, 1))
{ /* Ieee/control.scm 300 */
BgL_test2235z00_2867 = 
CBOOL(
BGL_PROCEDURE_CALL1(BgL_predz00_33, BgL_arg1316z00_949))
; }  else 
{ /* Ieee/control.scm 300 */
FAILURE(BGl_string1930z00zz__r4_control_features_6_9z00,BGl_list1931z00zz__r4_control_features_6_9z00,BgL_predz00_33);} } 
if(BgL_test2235z00_2867)
{ 
 obj_t BgL_lisz00_2878; obj_t BgL_prevz00_2877;
BgL_prevz00_2877 = BgL_lisz00_942; 
BgL_lisz00_2878 = 
CDR(BgL_lisz00_942); 
BgL_lisz00_942 = BgL_lisz00_2878; 
BgL_prevz00_941 = BgL_prevz00_2877; 
goto BgL_zc3z04anonymousza31310ze3z87_943;}  else 
{ /* Ieee/control.scm 300 */
BgL_prevz00_950 = BgL_prevz00_941; 
BgL_lisz00_951 = 
CDR(BgL_lisz00_942); 
{ 
 obj_t BgL_lisz00_954;
BgL_lisz00_954 = BgL_lisz00_951; 
BgL_zc3z04anonymousza31318ze3z87_955:
if(
PAIRP(BgL_lisz00_954))
{ /* Ieee/control.scm 306 */
 bool_t BgL_test2238z00_2882;
{ /* Ieee/control.scm 306 */
 obj_t BgL_arg1325z00_961;
BgL_arg1325z00_961 = 
CAR(BgL_lisz00_954); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_predz00_33, 1))
{ /* Ieee/control.scm 306 */
BgL_test2238z00_2882 = 
CBOOL(
BGL_PROCEDURE_CALL1(BgL_predz00_33, BgL_arg1325z00_961))
; }  else 
{ /* Ieee/control.scm 306 */
FAILURE(BGl_string1926z00zz__r4_control_features_6_9z00,BGl_list1934z00zz__r4_control_features_6_9z00,BgL_predz00_33);} } 
if(BgL_test2238z00_2882)
{ /* Ieee/control.scm 306 */
{ /* Ieee/control.scm 308 */
 obj_t BgL_pairz00_1317;
if(
PAIRP(BgL_prevz00_950))
{ /* Ieee/control.scm 308 */
BgL_pairz00_1317 = BgL_prevz00_950; }  else 
{ 
 obj_t BgL_auxz00_2894;
BgL_auxz00_2894 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(10220L), BGl_string1925z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_prevz00_950); 
FAILURE(BgL_auxz00_2894,BFALSE,BFALSE);} 
SET_CDR(BgL_pairz00_1317, BgL_lisz00_954); } 
{ 
 obj_t BgL_lisz00_2900; obj_t BgL_prevz00_2899;
BgL_prevz00_2899 = BgL_lisz00_954; 
BgL_lisz00_2900 = 
CDR(BgL_lisz00_954); 
BgL_lisz00_942 = BgL_lisz00_2900; 
BgL_prevz00_941 = BgL_prevz00_2899; 
goto BgL_zc3z04anonymousza31310ze3z87_943;} }  else 
{ 
 obj_t BgL_lisz00_2902;
BgL_lisz00_2902 = 
CDR(BgL_lisz00_954); 
BgL_lisz00_954 = BgL_lisz00_2902; 
goto BgL_zc3z04anonymousza31318ze3z87_955;} }  else 
{ /* Ieee/control.scm 311 */
 obj_t BgL_pairz00_1320;
if(
PAIRP(BgL_prevz00_950))
{ /* Ieee/control.scm 311 */
BgL_pairz00_1320 = BgL_prevz00_950; }  else 
{ 
 obj_t BgL_auxz00_2906;
BgL_auxz00_2906 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(10311L), BGl_string1925z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_prevz00_950); 
FAILURE(BgL_auxz00_2906,BFALSE,BFALSE);} 
SET_CDR(BgL_pairz00_1320, BgL_lisz00_954); } } } }  else 
{ /* Ieee/control.scm 299 */BFALSE; } } 
BgL_aux1805z00_1703 = BgL_ansz00_933; }  else 
{ /* Ieee/control.scm 296 */
 obj_t BgL_arg1326z00_965;
{ /* Ieee/control.scm 296 */
 obj_t BgL_pairz00_1322;
if(
PAIRP(BgL_ansz00_933))
{ /* Ieee/control.scm 296 */
BgL_pairz00_1322 = BgL_ansz00_933; }  else 
{ 
 obj_t BgL_auxz00_2914;
BgL_auxz00_2914 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(9886L), BGl_string1925z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_ansz00_933); 
FAILURE(BgL_auxz00_2914,BFALSE,BFALSE);} 
BgL_arg1326z00_965 = 
CDR(BgL_pairz00_1322); } 
{ 
 obj_t BgL_ansz00_2919;
BgL_ansz00_2919 = BgL_arg1326z00_965; 
BgL_ansz00_933 = BgL_ansz00_2919; 
goto BgL_zc3z04anonymousza31305ze3z87_934;} } } 
{ /* Ieee/control.scm 291 */
 bool_t BgL_test2243z00_2920;
if(
PAIRP(BgL_aux1805z00_1703))
{ /* Ieee/control.scm 291 */
BgL_test2243z00_2920 = ((bool_t)1)
; }  else 
{ /* Ieee/control.scm 291 */
BgL_test2243z00_2920 = 
NULLP(BgL_aux1805z00_1703)
; } 
if(BgL_test2243z00_2920)
{ /* Ieee/control.scm 291 */
return BgL_aux1805z00_1703;}  else 
{ 
 obj_t BgL_auxz00_2924;
BgL_auxz00_2924 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(9793L), BGl_string1937z00zz__r4_control_features_6_9z00, BGl_string1863z00zz__r4_control_features_6_9z00, BgL_aux1805z00_1703); 
FAILURE(BgL_auxz00_2924,BFALSE,BFALSE);} } } } } 

}



/* &filter! */
obj_t BGl_z62filterz12z70zz__r4_control_features_6_9z00(obj_t BgL_envz00_1444, obj_t BgL_predz00_1445, obj_t BgL_lisz00_1446)
{
{ /* Ieee/control.scm 290 */
{ /* Ieee/control.scm 293 */
 obj_t BgL_auxz00_2935; obj_t BgL_auxz00_2928;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_lisz00_1446))
{ /* Ieee/control.scm 293 */
BgL_auxz00_2935 = BgL_lisz00_1446
; }  else 
{ 
 obj_t BgL_auxz00_2938;
BgL_auxz00_2938 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(9827L), BGl_string1938z00zz__r4_control_features_6_9z00, BGl_string1863z00zz__r4_control_features_6_9z00, BgL_lisz00_1446); 
FAILURE(BgL_auxz00_2938,BFALSE,BFALSE);} 
if(
PROCEDUREP(BgL_predz00_1445))
{ /* Ieee/control.scm 293 */
BgL_auxz00_2928 = BgL_predz00_1445
; }  else 
{ 
 obj_t BgL_auxz00_2931;
BgL_auxz00_2931 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(9827L), BGl_string1938z00zz__r4_control_features_6_9z00, BGl_string1852z00zz__r4_control_features_6_9z00, BgL_predz00_1445); 
FAILURE(BgL_auxz00_2931,BFALSE,BFALSE);} 
return 
BGl_filterz12z12zz__r4_control_features_6_9z00(BgL_auxz00_2928, BgL_auxz00_2935);} } 

}



/* force */
BGL_EXPORTED_DEF obj_t BGl_forcez00zz__r4_control_features_6_9z00(obj_t BgL_promisez00_35)
{
{ /* Ieee/control.scm 318 */
{ /* Ieee/control.scm 319 */
 obj_t BgL_funz00_1809;
if(
PROCEDUREP(BgL_promisez00_35))
{ /* Ieee/control.scm 319 */
BgL_funz00_1809 = BgL_promisez00_35; }  else 
{ 
 obj_t BgL_auxz00_2945;
BgL_auxz00_2945 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(10628L), BGl_string1939z00zz__r4_control_features_6_9z00, BGl_string1852z00zz__r4_control_features_6_9z00, BgL_promisez00_35); 
FAILURE(BgL_auxz00_2945,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_1809, 0))
{ /* Ieee/control.scm 319 */
return 
(VA_PROCEDUREP( BgL_funz00_1809 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_1809))(BgL_promisez00_35, BEOA) : ((obj_t (*)(obj_t))PROCEDURE_ENTRY(BgL_funz00_1809))(BgL_promisez00_35) );}  else 
{ /* Ieee/control.scm 319 */
FAILURE(BGl_string1940z00zz__r4_control_features_6_9z00,BGl_list1941z00zz__r4_control_features_6_9z00,BgL_funz00_1809);} } } 

}



/* &force */
obj_t BGl_z62forcez62zz__r4_control_features_6_9z00(obj_t BgL_envz00_1447, obj_t BgL_promisez00_1448)
{
{ /* Ieee/control.scm 318 */
return 
BGl_forcez00zz__r4_control_features_6_9z00(BgL_promisez00_1448);} 

}



/* make-promise */
BGL_EXPORTED_DEF obj_t BGl_makezd2promisezd2zz__r4_control_features_6_9z00(obj_t BgL_procz00_36)
{
{ /* Ieee/control.scm 324 */
{ /* Ieee/control.scm 325 */
 obj_t BgL_resultzd2readyzf3z21_1458; obj_t BgL_resultz00_1459;
BgL_resultzd2readyzf3z21_1458 = 
MAKE_CELL(BFALSE); 
BgL_resultz00_1459 = 
MAKE_CELL(BFALSE); 
{ /* Ieee/control.scm 327 */
 obj_t BgL_zc3z04anonymousza31328ze3z87_1449;
BgL_zc3z04anonymousza31328ze3z87_1449 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31328ze3ze5zz__r4_control_features_6_9z00, 
(int)(0L), 
(int)(3L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31328ze3z87_1449, 
(int)(0L), BgL_procz00_36); 
PROCEDURE_SET(BgL_zc3z04anonymousza31328ze3z87_1449, 
(int)(1L), 
((obj_t)BgL_resultzd2readyzf3z21_1458)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31328ze3z87_1449, 
(int)(2L), 
((obj_t)BgL_resultz00_1459)); 
return BgL_zc3z04anonymousza31328ze3z87_1449;} } } 

}



/* &make-promise */
obj_t BGl_z62makezd2promisezb0zz__r4_control_features_6_9z00(obj_t BgL_envz00_1450, obj_t BgL_procz00_1451)
{
{ /* Ieee/control.scm 324 */
{ /* Ieee/control.scm 325 */
 obj_t BgL_auxz00_2967;
if(
PROCEDUREP(BgL_procz00_1451))
{ /* Ieee/control.scm 325 */
BgL_auxz00_2967 = BgL_procz00_1451
; }  else 
{ 
 obj_t BgL_auxz00_2970;
BgL_auxz00_2970 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(10893L), BGl_string1944z00zz__r4_control_features_6_9z00, BGl_string1852z00zz__r4_control_features_6_9z00, BgL_procz00_1451); 
FAILURE(BgL_auxz00_2970,BFALSE,BFALSE);} 
return 
BGl_makezd2promisezd2zz__r4_control_features_6_9z00(BgL_auxz00_2967);} } 

}



/* &<@anonymous:1328> */
obj_t BGl_z62zc3z04anonymousza31328ze3ze5zz__r4_control_features_6_9z00(obj_t BgL_envz00_1452)
{
{ /* Ieee/control.scm 327 */
{ /* Ieee/control.scm 328 */
 obj_t BgL_procz00_1453; obj_t BgL_resultzd2readyzf3z21_1454; obj_t BgL_resultz00_1455;
BgL_procz00_1453 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_1452, 
(int)(0L))); 
BgL_resultzd2readyzf3z21_1454 = 
PROCEDURE_REF(BgL_envz00_1452, 
(int)(1L)); 
BgL_resultz00_1455 = 
PROCEDURE_REF(BgL_envz00_1452, 
(int)(2L)); 
if(
CBOOL(
CELL_REF(BgL_resultzd2readyzf3z21_1454)))
{ /* Ieee/control.scm 328 */
return 
CELL_REF(BgL_resultz00_1455);}  else 
{ /* Ieee/control.scm 330 */
 obj_t BgL_xz00_1810;
if(
PROCEDURE_CORRECT_ARITYP(BgL_procz00_1453, 0))
{ /* Ieee/control.scm 330 */
BgL_xz00_1810 = 
BGL_PROCEDURE_CALL0(BgL_procz00_1453); }  else 
{ /* Ieee/control.scm 330 */
FAILURE(BGl_string1945z00zz__r4_control_features_6_9z00,BGl_list1946z00zz__r4_control_features_6_9z00,BgL_procz00_1453);} 
if(
CBOOL(
CELL_REF(BgL_resultzd2readyzf3z21_1454)))
{ /* Ieee/control.scm 331 */
return 
CELL_REF(BgL_resultz00_1455);}  else 
{ /* Ieee/control.scm 331 */
{ /* Ieee/control.scm 334 */
 obj_t BgL_auxz00_1811;
BgL_auxz00_1811 = BTRUE; 
CELL_SET(BgL_resultzd2readyzf3z21_1454, BgL_auxz00_1811); } 
CELL_SET(BgL_resultz00_1455, BgL_xz00_1810); 
return 
CELL_REF(BgL_resultz00_1455);} } } } 

}



/* call/cc */
BGL_EXPORTED_DEF obj_t BGl_callzf2cczf2zz__r4_control_features_6_9z00(obj_t BgL_procz00_37)
{
{ /* Ieee/control.scm 341 */
{ /* Ieee/control.scm 343 */
 obj_t BgL_zc3z04anonymousza31330ze3z87_1463;
BgL_zc3z04anonymousza31330ze3z87_1463 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31330ze3ze5zz__r4_control_features_6_9z00, 
(int)(1L), 
(int)(1L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31330ze3z87_1463, 
(int)(0L), BgL_procz00_37); 
BGL_TAIL return 
call_cc(BgL_zc3z04anonymousza31330ze3z87_1463);} } 

}



/* &call/cc */
obj_t BGl_z62callzf2ccz90zz__r4_control_features_6_9z00(obj_t BgL_envz00_1464, obj_t BgL_procz00_1465)
{
{ /* Ieee/control.scm 341 */
{ /* Ieee/control.scm 343 */
 obj_t BgL_auxz00_2998;
if(
PROCEDUREP(BgL_procz00_1465))
{ /* Ieee/control.scm 343 */
BgL_auxz00_2998 = BgL_procz00_1465
; }  else 
{ 
 obj_t BgL_auxz00_3001;
BgL_auxz00_3001 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(11423L), BGl_string1947z00zz__r4_control_features_6_9z00, BGl_string1852z00zz__r4_control_features_6_9z00, BgL_procz00_1465); 
FAILURE(BgL_auxz00_3001,BFALSE,BFALSE);} 
return 
BGl_callzf2cczf2zz__r4_control_features_6_9z00(BgL_auxz00_2998);} } 

}



/* &<@anonymous:1330> */
obj_t BGl_z62zc3z04anonymousza31330ze3ze5zz__r4_control_features_6_9z00(obj_t BgL_envz00_1466, obj_t BgL_contz00_1468)
{
{ /* Ieee/control.scm 342 */
{ /* Ieee/control.scm 343 */
 obj_t BgL_procz00_1467;
BgL_procz00_1467 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_1466, 
(int)(0L))); 
{ /* Ieee/control.scm 343 */
 obj_t BgL_evcz00_1812;
BgL_evcz00_1812 = 
BGl_getzd2evaluationzd2contextz00zz__evaluatez00(); 
{ /* Ieee/control.scm 345 */
 obj_t BgL_zc3z04anonymousza31332ze3z87_1813;
BgL_zc3z04anonymousza31332ze3z87_1813 = 
MAKE_VA_PROCEDURE(BGl_z62zc3z04anonymousza31332ze3ze5zz__r4_control_features_6_9z00, 
(int)(-1L), 
(int)(2L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31332ze3z87_1813, 
(int)(0L), BgL_evcz00_1812); 
PROCEDURE_SET(BgL_zc3z04anonymousza31332ze3z87_1813, 
(int)(1L), BgL_contz00_1468); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_procz00_1467, 1))
{ /* Ieee/control.scm 344 */
return 
BGL_PROCEDURE_CALL1(BgL_procz00_1467, BgL_zc3z04anonymousza31332ze3z87_1813);}  else 
{ /* Ieee/control.scm 344 */
FAILURE(BGl_string1948z00zz__r4_control_features_6_9z00,BGl_list1949z00zz__r4_control_features_6_9z00,BgL_procz00_1467);} } } } } 

}



/* &<@anonymous:1332> */
obj_t BGl_z62zc3z04anonymousza31332ze3ze5zz__r4_control_features_6_9z00(obj_t BgL_envz00_1469, obj_t BgL_valsz00_1472)
{
{ /* Ieee/control.scm 344 */
{ /* Ieee/control.scm 345 */
 obj_t BgL_evcz00_1470; obj_t BgL_contz00_1471;
BgL_evcz00_1470 = 
PROCEDURE_REF(BgL_envz00_1469, 
(int)(0L)); 
BgL_contz00_1471 = 
PROCEDURE_REF(BgL_envz00_1469, 
(int)(1L)); 
BGl_setzd2evaluationzd2contextz12z12zz__evaluatez00(BgL_evcz00_1470); 
{ /* Ieee/control.scm 346 */
 bool_t BgL_test2255z00_3029;
if(
PAIRP(BgL_valsz00_1472))
{ /* Ieee/control.scm 346 */
BgL_test2255z00_3029 = 
NULLP(
CDR(BgL_valsz00_1472))
; }  else 
{ /* Ieee/control.scm 346 */
BgL_test2255z00_3029 = ((bool_t)0)
; } 
if(BgL_test2255z00_3029)
{ /* Ieee/control.scm 347 */
 obj_t BgL_arg1336z00_1814;
BgL_arg1336z00_1814 = 
CAR(BgL_valsz00_1472); 
{ /* Ieee/control.scm 347 */
 obj_t BgL_funz00_1815;
if(
PROCEDUREP(BgL_contz00_1471))
{ /* Ieee/control.scm 347 */
BgL_funz00_1815 = BgL_contz00_1471; }  else 
{ 
 obj_t BgL_auxz00_3037;
BgL_auxz00_3037 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(11578L), BGl_string1951z00zz__r4_control_features_6_9z00, BGl_string1852z00zz__r4_control_features_6_9z00, BgL_contz00_1471); 
FAILURE(BgL_auxz00_3037,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_1815, 1))
{ /* Ieee/control.scm 347 */
return 
(VA_PROCEDUREP( BgL_funz00_1815 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_1815))(BgL_contz00_1471, BgL_arg1336z00_1814, BEOA) : ((obj_t (*)(obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_1815))(BgL_contz00_1471, BgL_arg1336z00_1814) );}  else 
{ /* Ieee/control.scm 347 */
FAILURE(BGl_string1952z00zz__r4_control_features_6_9z00,BGl_list1953z00zz__r4_control_features_6_9z00,BgL_funz00_1815);} } }  else 
{ /* Ieee/control.scm 346 */
{ /* Ieee/control.scm 349 */
 int BgL_tmpz00_3048;
BgL_tmpz00_3048 = 
(int)(-1L); 
BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3048); } 
{ /* Ieee/control.scm 350 */
 obj_t BgL_funz00_1816;
if(
PROCEDUREP(BgL_contz00_1471))
{ /* Ieee/control.scm 350 */
BgL_funz00_1816 = BgL_contz00_1471; }  else 
{ 
 obj_t BgL_auxz00_3053;
BgL_auxz00_3053 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(11651L), BGl_string1951z00zz__r4_control_features_6_9z00, BGl_string1852z00zz__r4_control_features_6_9z00, BgL_contz00_1471); 
FAILURE(BgL_auxz00_3053,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_1816, 1))
{ /* Ieee/control.scm 350 */
return 
(VA_PROCEDUREP( BgL_funz00_1816 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_1816))(BgL_contz00_1471, BgL_valsz00_1472, BEOA) : ((obj_t (*)(obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_1816))(BgL_contz00_1471, BgL_valsz00_1472) );}  else 
{ /* Ieee/control.scm 350 */
FAILURE(BGl_string1952z00zz__r4_control_features_6_9z00,BGl_list1958z00zz__r4_control_features_6_9z00,BgL_funz00_1816);} } } } } } 

}



/* call-with-current-continuation */
BGL_EXPORTED_DEF obj_t BGl_callzd2withzd2currentzd2continuationzd2zz__r4_control_features_6_9z00(obj_t BgL_procz00_38)
{
{ /* Ieee/control.scm 359 */
BGL_TAIL return 
BGl_callzf2cczf2zz__r4_control_features_6_9z00(BgL_procz00_38);} 

}



/* &call-with-current-continuation */
obj_t BGl_z62callzd2withzd2currentzd2continuationzb0zz__r4_control_features_6_9z00(obj_t BgL_envz00_1473, obj_t BgL_procz00_1474)
{
{ /* Ieee/control.scm 359 */
{ /* Ieee/control.scm 360 */
 obj_t BgL_auxz00_3065;
if(
PROCEDUREP(BgL_procz00_1474))
{ /* Ieee/control.scm 360 */
BgL_auxz00_3065 = BgL_procz00_1474
; }  else 
{ 
 obj_t BgL_auxz00_3068;
BgL_auxz00_3068 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(12040L), BGl_string1961z00zz__r4_control_features_6_9z00, BGl_string1852z00zz__r4_control_features_6_9z00, BgL_procz00_1474); 
FAILURE(BgL_auxz00_3068,BFALSE,BFALSE);} 
return 
BGl_callzd2withzd2currentzd2continuationzd2zz__r4_control_features_6_9z00(BgL_auxz00_3065);} } 

}



/* dynamic-wind */
BGL_EXPORTED_DEF obj_t BGl_dynamiczd2windzd2zz__r4_control_features_6_9z00(obj_t BgL_beforez00_39, obj_t BgL_thunkz00_40, obj_t BgL_afterz00_41)
{
{ /* Ieee/control.scm 365 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_beforez00_39, 0))
{ /* Ieee/control.scm 366 */
BGL_PROCEDURE_CALL0(BgL_beforez00_39); }  else 
{ /* Ieee/control.scm 366 */
FAILURE(BGl_string1962z00zz__r4_control_features_6_9z00,BGl_list1963z00zz__r4_control_features_6_9z00,BgL_beforez00_39);} 
{ /* Ieee/control.scm 367 */

PUSH_BEFORE(BgL_beforez00_39); 
{ /* Ieee/control.scm 369 */
 obj_t BgL_exitd1014z00_1325;
BgL_exitd1014z00_1325 = 
BGL_EXITD_TOP_AS_OBJ(); 
{ /* Ieee/control.scm 372 */
 obj_t BgL_zc3z04anonymousza31338ze3z87_1475;
BgL_zc3z04anonymousza31338ze3z87_1475 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31338ze3ze5zz__r4_control_features_6_9z00, 
(int)(0L), 
(int)(1L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31338ze3z87_1475, 
(int)(0L), BgL_afterz00_41); 
{ /* Ieee/control.scm 369 */
 obj_t BgL_arg1608z00_1328;
{ /* Ieee/control.scm 369 */
 obj_t BgL_arg1609z00_1329;
BgL_arg1609z00_1329 = 
BGL_EXITD_PROTECT(BgL_exitd1014z00_1325); 
BgL_arg1608z00_1328 = 
MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31338ze3z87_1475, BgL_arg1609z00_1329); } 
BGL_EXITD_PROTECT_SET(BgL_exitd1014z00_1325, BgL_arg1608z00_1328); BUNSPEC; } 
{ /* Ieee/control.scm 370 */
 obj_t BgL_tmp1016z00_1327;
if(
PROCEDURE_CORRECT_ARITYP(BgL_thunkz00_40, 0))
{ /* Ieee/control.scm 370 */
BgL_tmp1016z00_1327 = 
BGL_PROCEDURE_CALL0(BgL_thunkz00_40); }  else 
{ /* Ieee/control.scm 370 */
FAILURE(BGl_string1962z00zz__r4_control_features_6_9z00,BGl_list1966z00zz__r4_control_features_6_9z00,BgL_thunkz00_40);} 
{ /* Ieee/control.scm 369 */
 bool_t BgL_test2264z00_3095;
{ /* Ieee/control.scm 369 */
 obj_t BgL_arg1607z00_1331;
BgL_arg1607z00_1331 = 
BGL_EXITD_PROTECT(BgL_exitd1014z00_1325); 
BgL_test2264z00_3095 = 
PAIRP(BgL_arg1607z00_1331); } 
if(BgL_test2264z00_3095)
{ /* Ieee/control.scm 369 */
 obj_t BgL_arg1605z00_1332;
{ /* Ieee/control.scm 369 */
 obj_t BgL_arg1606z00_1333;
BgL_arg1606z00_1333 = 
BGL_EXITD_PROTECT(BgL_exitd1014z00_1325); 
{ /* Ieee/control.scm 369 */
 obj_t BgL_pairz00_1334;
if(
PAIRP(BgL_arg1606z00_1333))
{ /* Ieee/control.scm 369 */
BgL_pairz00_1334 = BgL_arg1606z00_1333; }  else 
{ 
 obj_t BgL_auxz00_3101;
BgL_auxz00_3101 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(12411L), BGl_string1969z00zz__r4_control_features_6_9z00, BGl_string1850z00zz__r4_control_features_6_9z00, BgL_arg1606z00_1333); 
FAILURE(BgL_auxz00_3101,BFALSE,BFALSE);} 
BgL_arg1605z00_1332 = 
CDR(BgL_pairz00_1334); } } 
BGL_EXITD_PROTECT_SET(BgL_exitd1014z00_1325, BgL_arg1605z00_1332); BUNSPEC; }  else 
{ /* Ieee/control.scm 369 */BFALSE; } } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_afterz00_41, 0))
{ /* Ieee/control.scm 372 */
BGL_PROCEDURE_CALL0(BgL_afterz00_41); }  else 
{ /* Ieee/control.scm 372 */
FAILURE(BGl_string1962z00zz__r4_control_features_6_9z00,BGl_list1970z00zz__r4_control_features_6_9z00,BgL_afterz00_41);} 
POP_BEFORE(); 
return BgL_tmp1016z00_1327;} } } } } 

}



/* &dynamic-wind */
obj_t BGl_z62dynamiczd2windzb0zz__r4_control_features_6_9z00(obj_t BgL_envz00_1476, obj_t BgL_beforez00_1477, obj_t BgL_thunkz00_1478, obj_t BgL_afterz00_1479)
{
{ /* Ieee/control.scm 365 */
{ /* Ieee/control.scm 366 */
 obj_t BgL_auxz00_3128; obj_t BgL_auxz00_3121; obj_t BgL_auxz00_3114;
if(
PROCEDUREP(BgL_afterz00_1479))
{ /* Ieee/control.scm 366 */
BgL_auxz00_3128 = BgL_afterz00_1479
; }  else 
{ 
 obj_t BgL_auxz00_3131;
BgL_auxz00_3131 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(12364L), BGl_string1973z00zz__r4_control_features_6_9z00, BGl_string1852z00zz__r4_control_features_6_9z00, BgL_afterz00_1479); 
FAILURE(BgL_auxz00_3131,BFALSE,BFALSE);} 
if(
PROCEDUREP(BgL_thunkz00_1478))
{ /* Ieee/control.scm 366 */
BgL_auxz00_3121 = BgL_thunkz00_1478
; }  else 
{ 
 obj_t BgL_auxz00_3124;
BgL_auxz00_3124 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(12364L), BGl_string1973z00zz__r4_control_features_6_9z00, BGl_string1852z00zz__r4_control_features_6_9z00, BgL_thunkz00_1478); 
FAILURE(BgL_auxz00_3124,BFALSE,BFALSE);} 
if(
PROCEDUREP(BgL_beforez00_1477))
{ /* Ieee/control.scm 366 */
BgL_auxz00_3114 = BgL_beforez00_1477
; }  else 
{ 
 obj_t BgL_auxz00_3117;
BgL_auxz00_3117 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1848z00zz__r4_control_features_6_9z00, 
BINT(12364L), BGl_string1973z00zz__r4_control_features_6_9z00, BGl_string1852z00zz__r4_control_features_6_9z00, BgL_beforez00_1477); 
FAILURE(BgL_auxz00_3117,BFALSE,BFALSE);} 
return 
BGl_dynamiczd2windzd2zz__r4_control_features_6_9z00(BgL_auxz00_3114, BgL_auxz00_3121, BgL_auxz00_3128);} } 

}



/* &<@anonymous:1338> */
obj_t BGl_z62zc3z04anonymousza31338ze3ze5zz__r4_control_features_6_9z00(obj_t BgL_envz00_1480)
{
{ /* Ieee/control.scm 369 */
{ /* Ieee/control.scm 372 */
 obj_t BgL_afterz00_1481;
BgL_afterz00_1481 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_1480, 
(int)(0L))); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_afterz00_1481, 0))
{ /* Ieee/control.scm 372 */
BGL_PROCEDURE_CALL0(BgL_afterz00_1481); }  else 
{ /* Ieee/control.scm 372 */
FAILURE(BGl_string1974z00zz__r4_control_features_6_9z00,BGl_list1970z00zz__r4_control_features_6_9z00,BgL_afterz00_1481);} 
return 
POP_BEFORE();} } 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__r4_control_features_6_9z00(void)
{
{ /* Ieee/control.scm 14 */
BGl_modulezd2initializa7ationz75zz__errorz00(88804785L, 
BSTRING_TO_STRING(BGl_string1975z00zz__r4_control_features_6_9z00)); 
BGl_modulezd2initializa7ationz75zz__paramz00(453939141L, 
BSTRING_TO_STRING(BGl_string1975z00zz__r4_control_features_6_9z00)); 
return 
BGl_modulezd2initializa7ationz75zz__evaluatez00(398574045L, 
BSTRING_TO_STRING(BGl_string1975z00zz__r4_control_features_6_9z00));} 

}

#ifdef __cplusplus
}
#endif
