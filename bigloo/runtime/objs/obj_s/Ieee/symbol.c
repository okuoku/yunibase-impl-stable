/*===========================================================================*/
/*   (Ieee/symbol.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Ieee/symbol.scm -indent -o objs/obj_s/Ieee/symbol.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___R4_SYMBOLS_6_4_TYPE_DEFINITIONS
#define BGL___R4_SYMBOLS_6_4_TYPE_DEFINITIONS
#endif // BGL___R4_SYMBOLS_6_4_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

BGL_EXPORTED_DECL obj_t BGl_putpropz12z12zz__r4_symbols_6_4z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_keywordzd2ze3stringz12z23zz__r4_symbols_6_4z00(obj_t);
static obj_t BGl_z62rempropz12z70zz__r4_symbols_6_4z00(obj_t, obj_t, obj_t);
extern obj_t bstring_to_keyword(obj_t);
static obj_t BGl_requirezd2initializa7ationz75zz__r4_symbols_6_4z00 = BUNSPEC;
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
BGL_EXPORTED_DECL bool_t BGl_keywordzf3zf3zz__r4_symbols_6_4z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_keywordzd2ze3stringz31zz__r4_symbols_6_4z00(obj_t);
static obj_t BGl_z62keywordzd2ze3stringz53zz__r4_symbols_6_4z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_stringzd2ze3symbolz31zz__r4_symbols_6_4z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_symbolzd2ze3keywordz31zz__r4_symbols_6_4z00(obj_t);
static obj_t BGl_z62symbolzd2ze3keywordz53zz__r4_symbols_6_4z00(obj_t, obj_t);
static obj_t BGl_toplevelzd2initzd2zz__r4_symbols_6_4z00(void);
static obj_t BGl_z62symbolzd2plistzb0zz__r4_symbols_6_4z00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_symbolzf3zf3zz__r4_symbols_6_4z00(obj_t);
static obj_t BGl_z62symbolzf3z91zz__r4_symbols_6_4z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_stringzd2ze3keywordz31zz__r4_symbols_6_4z00(obj_t);
static obj_t BGl_z62stringzd2ze3keywordz53zz__r4_symbols_6_4z00(obj_t, obj_t);
static obj_t BGl_cnstzd2initzd2zz__r4_symbols_6_4z00(void);
static obj_t BGl_z62symbolzd2ze3stringz53zz__r4_symbols_6_4z00(obj_t, obj_t);
static obj_t BGl_genericzd2initzd2zz__r4_symbols_6_4z00(void);
extern obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
static obj_t BGl_z62putpropz12z70zz__r4_symbols_6_4z00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_importedzd2moduleszd2initz00zz__r4_symbols_6_4z00(void);
static obj_t BGl_z62keywordzd2ze3stringz12z41zz__r4_symbols_6_4z00(obj_t, obj_t);
static obj_t BGl_gczd2rootszd2initz00zz__r4_symbols_6_4z00(void);
BGL_EXPORTED_DECL obj_t BGl_symbolzd2plistzd2zz__r4_symbols_6_4z00(obj_t);
static obj_t BGl_objectzd2initzd2zz__r4_symbols_6_4z00(void);
static obj_t BGl_symbolzd2appendze70z35zz__r4_symbols_6_4z00(obj_t);
extern obj_t bgl_gensym(obj_t);
static obj_t BGl_z62symbolzd2appendzb0zz__r4_symbols_6_4z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_getpropz00zz__r4_symbols_6_4z00(obj_t, obj_t);
static obj_t BGl_z62getpropz62zz__r4_symbols_6_4z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62keywordzf3z91zz__r4_symbols_6_4z00(obj_t, obj_t);
static long BGl_za2gensymzd2counterza2zd2zz__r4_symbols_6_4z00 = 0L;
BGL_EXPORTED_DECL obj_t BGl_symbolzd2ze3stringz12z23zz__r4_symbols_6_4z00(obj_t);
extern obj_t bstring_to_symbol(obj_t);
static obj_t BGl_z62symbolzd2ze3stringz12z41zz__r4_symbols_6_4z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_rempropz12z12zz__r4_symbols_6_4z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
static obj_t BGl_methodzd2initzd2zz__r4_symbols_6_4z00(void);
BGL_EXPORTED_DECL obj_t BGl_keywordzd2ze3symbolz31zz__r4_symbols_6_4z00(obj_t);
static obj_t BGl_z62keywordzd2ze3symbolz53zz__r4_symbols_6_4z00(obj_t, obj_t);
static obj_t BGl_z62stringzd2ze3symbolzd2ciz81zz__r4_symbols_6_4z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_symbolzd2ze3stringz31zz__r4_symbols_6_4z00(obj_t);
static obj_t BGl__gensymz00zz__r4_symbols_6_4z00(obj_t, obj_t);
static obj_t BGl_symbol1727z00zz__r4_symbols_6_4z00 = BUNSPEC;
static obj_t BGl_z62stringzd2ze3symbolz53zz__r4_symbols_6_4z00(obj_t, obj_t);
extern obj_t string_append(obj_t, obj_t);
extern obj_t BGl_stringzd2upcasezd2zz__r4_strings_6_7z00(obj_t);
extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_stringzd2ze3symbolzd2cize3zz__r4_symbols_6_4z00(obj_t);
static obj_t *__cnst;


DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2ze3keywordzd2envze3zz__r4_symbols_6_4z00, BgL_bgl_za762stringza7d2za7e3k1742za7, BGl_z62stringzd2ze3keywordz53zz__r4_symbols_6_4z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_rempropz12zd2envzc0zz__r4_symbols_6_4z00, BgL_bgl_za762rempropza712za7701743za7, BGl_z62rempropz12z70zz__r4_symbols_6_4z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_symbolzd2plistzd2envz00zz__r4_symbols_6_4z00, BgL_bgl_za762symbolza7d2plis1744z00, BGl_z62symbolzd2plistzb0zz__r4_symbols_6_4z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_keywordzf3zd2envz21zz__r4_symbols_6_4z00, BgL_bgl_za762keywordza7f3za7911745za7, BGl_z62keywordzf3z91zz__r4_symbols_6_4z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_keywordzd2ze3symbolzd2envze3zz__r4_symbols_6_4z00, BgL_bgl_za762keywordza7d2za7e31746za7, BGl_z62keywordzd2ze3symbolz53zz__r4_symbols_6_4z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2ze3symbolzd2cizd2envz31zz__r4_symbols_6_4z00, BgL_bgl_za762stringza7d2za7e3s1747za7, BGl_z62stringzd2ze3symbolzd2ciz81zz__r4_symbols_6_4z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2ze3symbolzd2envze3zz__r4_symbols_6_4z00, BgL_bgl_za762stringza7d2za7e3s1748za7, BGl_z62stringzd2ze3symbolz53zz__r4_symbols_6_4z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_gensymzd2envzd2zz__r4_symbols_6_4z00, BgL_bgl__gensymza700za7za7__r41749za7, opt_generic_entry, BGl__gensymz00zz__r4_symbols_6_4z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_symbolzf3zd2envz21zz__r4_symbols_6_4z00, BgL_bgl_za762symbolza7f3za791za71750z00, BGl_z62symbolzf3z91zz__r4_symbols_6_4z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1717z00zz__r4_symbols_6_4z00, BgL_bgl_string1717za700za7za7_1751za7, "/tmp/bigloo/runtime/Ieee/symbol.scm", 35 );
DEFINE_STRING( BGl_string1718z00zz__r4_symbols_6_4z00, BgL_bgl_string1718za700za7za7_1752za7, "&symbol->string", 15 );
DEFINE_STRING( BGl_string1719z00zz__r4_symbols_6_4z00, BgL_bgl_string1719za700za7za7_1753za7, "symbol", 6 );
DEFINE_STRING( BGl_string1720z00zz__r4_symbols_6_4z00, BgL_bgl_string1720za700za7za7_1754za7, "&symbol->string!", 16 );
DEFINE_STRING( BGl_string1721z00zz__r4_symbols_6_4z00, BgL_bgl_string1721za700za7za7_1755za7, "&string->symbol", 15 );
DEFINE_STRING( BGl_string1722z00zz__r4_symbols_6_4z00, BgL_bgl_string1722za700za7za7_1756za7, "bstring", 7 );
DEFINE_STRING( BGl_string1723z00zz__r4_symbols_6_4z00, BgL_bgl_string1723za700za7za7_1757za7, "&string->symbol-ci", 18 );
DEFINE_STRING( BGl_string1724z00zz__r4_symbols_6_4z00, BgL_bgl_string1724za700za7za7_1758za7, "", 0 );
DEFINE_STRING( BGl_string1725z00zz__r4_symbols_6_4z00, BgL_bgl_string1725za700za7za7_1759za7, "symbol-append~0", 15 );
DEFINE_STRING( BGl_string1726z00zz__r4_symbols_6_4z00, BgL_bgl_string1726za700za7za7_1760za7, "pair", 4 );
DEFINE_STRING( BGl_string1728z00zz__r4_symbols_6_4z00, BgL_bgl_string1728za700za7za7_1761za7, "gensym", 6 );
DEFINE_STRING( BGl_string1729z00zz__r4_symbols_6_4z00, BgL_bgl_string1729za700za7za7_1762za7, "wrong number of arguments: [0..1] expected, provided", 52 );
DEFINE_STRING( BGl_string1730z00zz__r4_symbols_6_4z00, BgL_bgl_string1730za700za7za7_1763za7, "Illegal argument", 16 );
DEFINE_STRING( BGl_string1731z00zz__r4_symbols_6_4z00, BgL_bgl_string1731za700za7za7_1764za7, "symbol-plist", 12 );
DEFINE_STRING( BGl_string1732z00zz__r4_symbols_6_4z00, BgL_bgl_string1732za700za7za7_1765za7, "argument is neither a symbol nor a keyword", 42 );
DEFINE_STRING( BGl_string1733z00zz__r4_symbols_6_4z00, BgL_bgl_string1733za700za7za7_1766za7, "loop", 4 );
DEFINE_STRING( BGl_string1734z00zz__r4_symbols_6_4z00, BgL_bgl_string1734za700za7za7_1767za7, "getprop", 7 );
DEFINE_STRING( BGl_string1735z00zz__r4_symbols_6_4z00, BgL_bgl_string1735za700za7za7_1768za7, "&keyword->string", 16 );
DEFINE_STRING( BGl_string1736z00zz__r4_symbols_6_4z00, BgL_bgl_string1736za700za7za7_1769za7, "keyword", 7 );
DEFINE_STRING( BGl_string1737z00zz__r4_symbols_6_4z00, BgL_bgl_string1737za700za7za7_1770za7, "&keyword->string!", 17 );
DEFINE_STRING( BGl_string1738z00zz__r4_symbols_6_4z00, BgL_bgl_string1738za700za7za7_1771za7, "&string->keyword", 16 );
DEFINE_STRING( BGl_string1739z00zz__r4_symbols_6_4z00, BgL_bgl_string1739za700za7za7_1772za7, "&symbol->keyword", 16 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_symbolzd2ze3stringzd2envze3zz__r4_symbols_6_4z00, BgL_bgl_za762symbolza7d2za7e3s1773za7, BGl_z62symbolzd2ze3stringz53zz__r4_symbols_6_4z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_putpropz12zd2envzc0zz__r4_symbols_6_4z00, BgL_bgl_za762putpropza712za7701774za7, BGl_z62putpropz12z70zz__r4_symbols_6_4z00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_symbolzd2ze3keywordzd2envze3zz__r4_symbols_6_4z00, BgL_bgl_za762symbolza7d2za7e3k1775za7, BGl_z62symbolzd2ze3keywordz53zz__r4_symbols_6_4z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1740z00zz__r4_symbols_6_4z00, BgL_bgl_string1740za700za7za7_1776za7, "&keyword->symbol", 16 );
DEFINE_STRING( BGl_string1741z00zz__r4_symbols_6_4z00, BgL_bgl_string1741za700za7za7_1777za7, "__r4_symbols_6_4", 16 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_getpropzd2envzd2zz__r4_symbols_6_4z00, BgL_bgl_za762getpropza762za7za7_1778z00, BGl_z62getpropz62zz__r4_symbols_6_4z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_symbolzd2ze3stringz12zd2envzf1zz__r4_symbols_6_4z00, BgL_bgl_za762symbolza7d2za7e3s1779za7, BGl_z62symbolzd2ze3stringz12z41zz__r4_symbols_6_4z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_keywordzd2ze3stringzd2envze3zz__r4_symbols_6_4z00, BgL_bgl_za762keywordza7d2za7e31780za7, BGl_z62keywordzd2ze3stringz53zz__r4_symbols_6_4z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_keywordzd2ze3stringz12zd2envzf1zz__r4_symbols_6_4z00, BgL_bgl_za762keywordza7d2za7e31781za7, BGl_z62keywordzd2ze3stringz12z41zz__r4_symbols_6_4z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_symbolzd2appendzd2envz00zz__r4_symbols_6_4z00, BgL_bgl_za762symbolza7d2appe1782z00, va_generic_entry, BGl_z62symbolzd2appendzb0zz__r4_symbols_6_4z00, BUNSPEC, -1 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__r4_symbols_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1727z00zz__r4_symbols_6_4z00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long BgL_checksumz00_1939, char * BgL_fromz00_1940)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__r4_symbols_6_4z00))
{ 
BGl_requirezd2initializa7ationz75zz__r4_symbols_6_4z00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__r4_symbols_6_4z00(); 
BGl_cnstzd2initzd2zz__r4_symbols_6_4z00(); 
BGl_importedzd2moduleszd2initz00zz__r4_symbols_6_4z00(); 
return 
BGl_toplevelzd2initzd2zz__r4_symbols_6_4z00();}  else 
{ 
return BUNSPEC;} } 

}



/* cnst-init */
obj_t BGl_cnstzd2initzd2zz__r4_symbols_6_4z00(void)
{
{ /* Ieee/symbol.scm 14 */
return ( 
BGl_symbol1727z00zz__r4_symbols_6_4z00 = 
bstring_to_symbol(BGl_string1728z00zz__r4_symbols_6_4z00), BUNSPEC) ;} 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__r4_symbols_6_4z00(void)
{
{ /* Ieee/symbol.scm 14 */
return 
bgl_gc_roots_register();} 

}



/* toplevel-init */
obj_t BGl_toplevelzd2initzd2zz__r4_symbols_6_4z00(void)
{
{ /* Ieee/symbol.scm 14 */
return ( 
BGl_za2gensymzd2counterza2zd2zz__r4_symbols_6_4z00 = 999L, BUNSPEC) ;} 

}



/* symbol? */
BGL_EXPORTED_DEF bool_t BGl_symbolzf3zf3zz__r4_symbols_6_4z00(obj_t BgL_objz00_3)
{
{ /* Ieee/symbol.scm 166 */
return 
SYMBOLP(BgL_objz00_3);} 

}



/* &symbol? */
obj_t BGl_z62symbolzf3z91zz__r4_symbols_6_4z00(obj_t BgL_envz00_1830, obj_t BgL_objz00_1831)
{
{ /* Ieee/symbol.scm 166 */
return 
BBOOL(
BGl_symbolzf3zf3zz__r4_symbols_6_4z00(BgL_objz00_1831));} 

}



/* symbol->string */
BGL_EXPORTED_DEF obj_t BGl_symbolzd2ze3stringz31zz__r4_symbols_6_4z00(obj_t BgL_symbolz00_4)
{
{ /* Ieee/symbol.scm 172 */
{ /* Ieee/symbol.scm 173 */
 obj_t BgL_arg1166z00_1937;
BgL_arg1166z00_1937 = 
SYMBOL_TO_STRING(BgL_symbolz00_4); 
return 
BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1166z00_1937);} } 

}



/* &symbol->string */
obj_t BGl_z62symbolzd2ze3stringz53zz__r4_symbols_6_4z00(obj_t BgL_envz00_1832, obj_t BgL_symbolz00_1833)
{
{ /* Ieee/symbol.scm 172 */
{ /* Ieee/symbol.scm 173 */
 obj_t BgL_auxz00_1955;
if(
SYMBOLP(BgL_symbolz00_1833))
{ /* Ieee/symbol.scm 173 */
BgL_auxz00_1955 = BgL_symbolz00_1833
; }  else 
{ 
 obj_t BgL_auxz00_1958;
BgL_auxz00_1958 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1717z00zz__r4_symbols_6_4z00, 
BINT(7250L), BGl_string1718z00zz__r4_symbols_6_4z00, BGl_string1719z00zz__r4_symbols_6_4z00, BgL_symbolz00_1833); 
FAILURE(BgL_auxz00_1958,BFALSE,BFALSE);} 
return 
BGl_symbolzd2ze3stringz31zz__r4_symbols_6_4z00(BgL_auxz00_1955);} } 

}



/* symbol->string! */
BGL_EXPORTED_DEF obj_t BGl_symbolzd2ze3stringz12z23zz__r4_symbols_6_4z00(obj_t BgL_symbolz00_5)
{
{ /* Ieee/symbol.scm 178 */
return 
SYMBOL_TO_STRING(BgL_symbolz00_5);} 

}



/* &symbol->string! */
obj_t BGl_z62symbolzd2ze3stringz12z41zz__r4_symbols_6_4z00(obj_t BgL_envz00_1834, obj_t BgL_symbolz00_1835)
{
{ /* Ieee/symbol.scm 178 */
{ /* Ieee/symbol.scm 179 */
 obj_t BgL_auxz00_1964;
if(
SYMBOLP(BgL_symbolz00_1835))
{ /* Ieee/symbol.scm 179 */
BgL_auxz00_1964 = BgL_symbolz00_1835
; }  else 
{ 
 obj_t BgL_auxz00_1967;
BgL_auxz00_1967 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1717z00zz__r4_symbols_6_4z00, 
BINT(7544L), BGl_string1720z00zz__r4_symbols_6_4z00, BGl_string1719z00zz__r4_symbols_6_4z00, BgL_symbolz00_1835); 
FAILURE(BgL_auxz00_1967,BFALSE,BFALSE);} 
return 
BGl_symbolzd2ze3stringz12z23zz__r4_symbols_6_4z00(BgL_auxz00_1964);} } 

}



/* string->symbol */
BGL_EXPORTED_DEF obj_t BGl_stringzd2ze3symbolz31zz__r4_symbols_6_4z00(obj_t BgL_stringz00_6)
{
{ /* Ieee/symbol.scm 184 */
BGL_TAIL return 
bstring_to_symbol(BgL_stringz00_6);} 

}



/* &string->symbol */
obj_t BGl_z62stringzd2ze3symbolz53zz__r4_symbols_6_4z00(obj_t BgL_envz00_1836, obj_t BgL_stringz00_1837)
{
{ /* Ieee/symbol.scm 184 */
{ /* Ieee/symbol.scm 187 */
 obj_t BgL_auxz00_1973;
if(
STRINGP(BgL_stringz00_1837))
{ /* Ieee/symbol.scm 187 */
BgL_auxz00_1973 = BgL_stringz00_1837
; }  else 
{ 
 obj_t BgL_auxz00_1976;
BgL_auxz00_1976 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1717z00zz__r4_symbols_6_4z00, 
BINT(7888L), BGl_string1721z00zz__r4_symbols_6_4z00, BGl_string1722z00zz__r4_symbols_6_4z00, BgL_stringz00_1837); 
FAILURE(BgL_auxz00_1976,BFALSE,BFALSE);} 
return 
BGl_stringzd2ze3symbolz31zz__r4_symbols_6_4z00(BgL_auxz00_1973);} } 

}



/* string->symbol-ci */
BGL_EXPORTED_DEF obj_t BGl_stringzd2ze3symbolzd2cize3zz__r4_symbols_6_4z00(obj_t BgL_stringz00_7)
{
{ /* Ieee/symbol.scm 194 */
BGL_TAIL return 
bstring_to_symbol(
BGl_stringzd2upcasezd2zz__r4_strings_6_7z00(BgL_stringz00_7));} 

}



/* &string->symbol-ci */
obj_t BGl_z62stringzd2ze3symbolzd2ciz81zz__r4_symbols_6_4z00(obj_t BgL_envz00_1838, obj_t BgL_stringz00_1839)
{
{ /* Ieee/symbol.scm 194 */
{ /* Ieee/symbol.scm 195 */
 obj_t BgL_auxz00_1983;
if(
STRINGP(BgL_stringz00_1839))
{ /* Ieee/symbol.scm 195 */
BgL_auxz00_1983 = BgL_stringz00_1839
; }  else 
{ 
 obj_t BgL_auxz00_1986;
BgL_auxz00_1986 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1717z00zz__r4_symbols_6_4z00, 
BINT(8241L), BGl_string1723z00zz__r4_symbols_6_4z00, BGl_string1722z00zz__r4_symbols_6_4z00, BgL_stringz00_1839); 
FAILURE(BgL_auxz00_1986,BFALSE,BFALSE);} 
return 
BGl_stringzd2ze3symbolzd2cize3zz__r4_symbols_6_4z00(BgL_auxz00_1983);} } 

}



/* symbol-append */
BGL_EXPORTED_DEF obj_t BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(obj_t BgL_listz00_8)
{
{ /* Ieee/symbol.scm 200 */
{ /* Ieee/symbol.scm 202 */
 obj_t BgL_arg1172z00_1086;
if(
NULLP(BgL_listz00_8))
{ /* Ieee/symbol.scm 202 */
BgL_arg1172z00_1086 = BGl_string1724z00zz__r4_symbols_6_4z00; }  else 
{ /* Ieee/symbol.scm 202 */
BgL_arg1172z00_1086 = 
BGl_symbolzd2appendze70z35zz__r4_symbols_6_4z00(BgL_listz00_8); } 
return 
bstring_to_symbol(BgL_arg1172z00_1086);} } 

}



/* symbol-append~0 */
obj_t BGl_symbolzd2appendze70z35zz__r4_symbols_6_4z00(obj_t BgL_listz00_1089)
{
{ /* Ieee/symbol.scm 204 */
{ /* Ieee/symbol.scm 205 */
 bool_t BgL_test1789z00_1995;
{ /* Ieee/symbol.scm 205 */
 obj_t BgL_tmpz00_1996;
{ /* Ieee/symbol.scm 205 */
 obj_t BgL_pairz00_1572;
if(
PAIRP(BgL_listz00_1089))
{ /* Ieee/symbol.scm 205 */
BgL_pairz00_1572 = BgL_listz00_1089; }  else 
{ 
 obj_t BgL_auxz00_1999;
BgL_auxz00_1999 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1717z00zz__r4_symbols_6_4z00, 
BINT(8618L), BGl_string1725z00zz__r4_symbols_6_4z00, BGl_string1726z00zz__r4_symbols_6_4z00, BgL_listz00_1089); 
FAILURE(BgL_auxz00_1999,BFALSE,BFALSE);} 
BgL_tmpz00_1996 = 
CDR(BgL_pairz00_1572); } 
BgL_test1789z00_1995 = 
NULLP(BgL_tmpz00_1996); } 
if(BgL_test1789z00_1995)
{ /* Ieee/symbol.scm 206 */
 obj_t BgL_arg1187z00_1093;
{ /* Ieee/symbol.scm 206 */
 obj_t BgL_pairz00_1573;
if(
PAIRP(BgL_listz00_1089))
{ /* Ieee/symbol.scm 206 */
BgL_pairz00_1573 = BgL_listz00_1089; }  else 
{ 
 obj_t BgL_auxz00_2007;
BgL_auxz00_2007 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1717z00zz__r4_symbols_6_4z00, 
BINT(8654L), BGl_string1725z00zz__r4_symbols_6_4z00, BGl_string1726z00zz__r4_symbols_6_4z00, BgL_listz00_1089); 
FAILURE(BgL_auxz00_2007,BFALSE,BFALSE);} 
BgL_arg1187z00_1093 = 
CAR(BgL_pairz00_1573); } 
{ /* Ieee/symbol.scm 206 */
 obj_t BgL_symbolz00_1574;
if(
SYMBOLP(BgL_arg1187z00_1093))
{ /* Ieee/symbol.scm 206 */
BgL_symbolz00_1574 = BgL_arg1187z00_1093; }  else 
{ 
 obj_t BgL_auxz00_2014;
BgL_auxz00_2014 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1717z00zz__r4_symbols_6_4z00, 
BINT(8658L), BGl_string1725z00zz__r4_symbols_6_4z00, BGl_string1719z00zz__r4_symbols_6_4z00, BgL_arg1187z00_1093); 
FAILURE(BgL_auxz00_2014,BFALSE,BFALSE);} 
{ /* Ieee/symbol.scm 173 */
 obj_t BgL_arg1166z00_1575;
BgL_arg1166z00_1575 = 
SYMBOL_TO_STRING(BgL_symbolz00_1574); 
return 
BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1166z00_1575);} } }  else 
{ /* Ieee/symbol.scm 207 */
 obj_t BgL_arg1188z00_1094; obj_t BgL_arg1189z00_1095;
{ /* Ieee/symbol.scm 207 */
 obj_t BgL_arg1190z00_1096;
{ /* Ieee/symbol.scm 207 */
 obj_t BgL_pairz00_1576;
if(
PAIRP(BgL_listz00_1089))
{ /* Ieee/symbol.scm 207 */
BgL_pairz00_1576 = BgL_listz00_1089; }  else 
{ 
 obj_t BgL_auxz00_2022;
BgL_auxz00_2022 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1717z00zz__r4_symbols_6_4z00, 
BINT(8706L), BGl_string1725z00zz__r4_symbols_6_4z00, BGl_string1726z00zz__r4_symbols_6_4z00, BgL_listz00_1089); 
FAILURE(BgL_auxz00_2022,BFALSE,BFALSE);} 
BgL_arg1190z00_1096 = 
CAR(BgL_pairz00_1576); } 
{ /* Ieee/symbol.scm 207 */
 obj_t BgL_symbolz00_1577;
if(
SYMBOLP(BgL_arg1190z00_1096))
{ /* Ieee/symbol.scm 207 */
BgL_symbolz00_1577 = BgL_arg1190z00_1096; }  else 
{ 
 obj_t BgL_auxz00_2029;
BgL_auxz00_2029 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1717z00zz__r4_symbols_6_4z00, 
BINT(8710L), BGl_string1725z00zz__r4_symbols_6_4z00, BGl_string1719z00zz__r4_symbols_6_4z00, BgL_arg1190z00_1096); 
FAILURE(BgL_auxz00_2029,BFALSE,BFALSE);} 
{ /* Ieee/symbol.scm 173 */
 obj_t BgL_arg1166z00_1578;
BgL_arg1166z00_1578 = 
SYMBOL_TO_STRING(BgL_symbolz00_1577); 
BgL_arg1188z00_1094 = 
BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1166z00_1578); } } } 
{ /* Ieee/symbol.scm 208 */
 obj_t BgL_arg1191z00_1097;
{ /* Ieee/symbol.scm 208 */
 obj_t BgL_pairz00_1579;
if(
PAIRP(BgL_listz00_1089))
{ /* Ieee/symbol.scm 208 */
BgL_pairz00_1579 = BgL_listz00_1089; }  else 
{ 
 obj_t BgL_auxz00_2037;
BgL_auxz00_2037 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1717z00zz__r4_symbols_6_4z00, 
BINT(8743L), BGl_string1725z00zz__r4_symbols_6_4z00, BGl_string1726z00zz__r4_symbols_6_4z00, BgL_listz00_1089); 
FAILURE(BgL_auxz00_2037,BFALSE,BFALSE);} 
BgL_arg1191z00_1097 = 
CDR(BgL_pairz00_1579); } 
BgL_arg1189z00_1095 = 
BGl_symbolzd2appendze70z35zz__r4_symbols_6_4z00(BgL_arg1191z00_1097); } 
return 
string_append(BgL_arg1188z00_1094, BgL_arg1189z00_1095);} } } 

}



/* &symbol-append */
obj_t BGl_z62symbolzd2appendzb0zz__r4_symbols_6_4z00(obj_t BgL_envz00_1840, obj_t BgL_listz00_1841)
{
{ /* Ieee/symbol.scm 200 */
return 
BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(BgL_listz00_1841);} 

}



/* _gensym */
obj_t BGl__gensymz00zz__r4_symbols_6_4z00(obj_t BgL_env1089z00_11, obj_t BgL_opt1088z00_10)
{
{ /* Ieee/symbol.scm 218 */
{ /* Ieee/symbol.scm 218 */

{ 

switch( 
VECTOR_LENGTH(BgL_opt1088z00_10)) { case 0L : 

{ /* Ieee/symbol.scm 218 */

return 
BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);} break;case 1L : 

{ /* Ieee/symbol.scm 218 */
 obj_t BgL_argz00_1103;
BgL_argz00_1103 = 
VECTOR_REF(BgL_opt1088z00_10,0L); 
{ /* Ieee/symbol.scm 218 */

return 
BGl_gensymz00zz__r4_symbols_6_4z00(BgL_argz00_1103);} } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol1727z00zz__r4_symbols_6_4z00, BGl_string1729z00zz__r4_symbols_6_4z00, 
BINT(
VECTOR_LENGTH(BgL_opt1088z00_10)));} } } } 

}



/* gensym */
BGL_EXPORTED_DEF obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t BgL_argz00_9)
{
{ /* Ieee/symbol.scm 218 */
{ /* Ieee/symbol.scm 221 */
 obj_t BgL_argz00_1105;
if(
CBOOL(BgL_argz00_9))
{ /* Ieee/symbol.scm 222 */
if(
SYMBOLP(BgL_argz00_9))
{ /* Ieee/symbol.scm 173 */
 obj_t BgL_arg1166z00_1582;
BgL_arg1166z00_1582 = 
SYMBOL_TO_STRING(BgL_argz00_9); 
BgL_argz00_1105 = 
BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1166z00_1582); }  else 
{ /* Ieee/symbol.scm 223 */
if(
STRINGP(BgL_argz00_9))
{ /* Ieee/symbol.scm 224 */
BgL_argz00_1105 = BgL_argz00_9; }  else 
{ /* Ieee/symbol.scm 224 */
BgL_argz00_1105 = 
BGl_errorz00zz__errorz00(BGl_string1728z00zz__r4_symbols_6_4z00, BGl_string1730z00zz__r4_symbols_6_4z00, BgL_argz00_9); } } }  else 
{ /* Ieee/symbol.scm 222 */
BgL_argz00_1105 = BgL_argz00_9; } 
BGL_TAIL return 
bgl_gensym(BgL_argz00_1105);} } 

}



/* symbol-plist */
BGL_EXPORTED_DEF obj_t BGl_symbolzd2plistzd2zz__r4_symbols_6_4z00(obj_t BgL_symbolz00_12)
{
{ /* Ieee/symbol.scm 244 */
if(
SYMBOLP(BgL_symbolz00_12))
{ /* Ieee/symbol.scm 246 */
return 
GET_SYMBOL_PLIST(BgL_symbolz00_12);}  else 
{ /* Ieee/symbol.scm 246 */
if(
KEYWORDP(BgL_symbolz00_12))
{ /* Ieee/symbol.scm 248 */
return 
GET_KEYWORD_PLIST(BgL_symbolz00_12);}  else 
{ /* Ieee/symbol.scm 248 */
return 
BGl_errorz00zz__errorz00(BGl_string1731z00zz__r4_symbols_6_4z00, BGl_string1732z00zz__r4_symbols_6_4z00, BgL_symbolz00_12);} } } 

}



/* &symbol-plist */
obj_t BGl_z62symbolzd2plistzb0zz__r4_symbols_6_4z00(obj_t BgL_envz00_1842, obj_t BgL_symbolz00_1843)
{
{ /* Ieee/symbol.scm 244 */
return 
BGl_symbolzd2plistzd2zz__r4_symbols_6_4z00(BgL_symbolz00_1843);} 

}



/* getprop */
BGL_EXPORTED_DEF obj_t BGl_getpropz00zz__r4_symbols_6_4z00(obj_t BgL_symbolz00_13, obj_t BgL_keyz00_14)
{
{ /* Ieee/symbol.scm 258 */
{ /* Ieee/symbol.scm 259 */
 bool_t BgL_test1801z00_2071;
if(
SYMBOLP(BgL_symbolz00_13))
{ /* Ieee/symbol.scm 259 */
BgL_test1801z00_2071 = ((bool_t)1)
; }  else 
{ /* Ieee/symbol.scm 259 */
BgL_test1801z00_2071 = 
KEYWORDP(BgL_symbolz00_13)
; } 
if(BgL_test1801z00_2071)
{ /* Ieee/symbol.scm 260 */
 obj_t BgL_g1040z00_1112;
if(
SYMBOLP(BgL_symbolz00_13))
{ /* Ieee/symbol.scm 246 */
BgL_g1040z00_1112 = 
GET_SYMBOL_PLIST(BgL_symbolz00_13); }  else 
{ /* Ieee/symbol.scm 246 */
if(
KEYWORDP(BgL_symbolz00_13))
{ /* Ieee/symbol.scm 248 */
BgL_g1040z00_1112 = 
GET_KEYWORD_PLIST(BgL_symbolz00_13); }  else 
{ /* Ieee/symbol.scm 248 */
BgL_g1040z00_1112 = 
BGl_errorz00zz__errorz00(BGl_string1731z00zz__r4_symbols_6_4z00, BGl_string1732z00zz__r4_symbols_6_4z00, BgL_symbolz00_13); } } 
{ 
 obj_t BgL_plz00_1114;
BgL_plz00_1114 = BgL_g1040z00_1112; 
BgL_zc3z04anonymousza31201ze3z87_1115:
if(
NULLP(BgL_plz00_1114))
{ /* Ieee/symbol.scm 262 */
return BFALSE;}  else 
{ /* Ieee/symbol.scm 264 */
 bool_t BgL_test1806z00_2084;
{ /* Ieee/symbol.scm 264 */
 obj_t BgL_tmpz00_2085;
{ /* Ieee/symbol.scm 264 */
 obj_t BgL_pairz00_1587;
if(
PAIRP(BgL_plz00_1114))
{ /* Ieee/symbol.scm 264 */
BgL_pairz00_1587 = BgL_plz00_1114; }  else 
{ 
 obj_t BgL_auxz00_2088;
BgL_auxz00_2088 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1717z00zz__r4_symbols_6_4z00, 
BINT(10795L), BGl_string1733z00zz__r4_symbols_6_4z00, BGl_string1726z00zz__r4_symbols_6_4z00, BgL_plz00_1114); 
FAILURE(BgL_auxz00_2088,BFALSE,BFALSE);} 
BgL_tmpz00_2085 = 
CAR(BgL_pairz00_1587); } 
BgL_test1806z00_2084 = 
(BgL_tmpz00_2085==BgL_keyz00_14); } 
if(BgL_test1806z00_2084)
{ /* Ieee/symbol.scm 265 */
 obj_t BgL_pairz00_1588;
if(
PAIRP(BgL_plz00_1114))
{ /* Ieee/symbol.scm 265 */
BgL_pairz00_1588 = BgL_plz00_1114; }  else 
{ 
 obj_t BgL_auxz00_2096;
BgL_auxz00_2096 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1717z00zz__r4_symbols_6_4z00, 
BINT(10817L), BGl_string1733z00zz__r4_symbols_6_4z00, BGl_string1726z00zz__r4_symbols_6_4z00, BgL_plz00_1114); 
FAILURE(BgL_auxz00_2096,BFALSE,BFALSE);} 
{ /* Ieee/symbol.scm 265 */
 obj_t BgL_pairz00_1591;
{ /* Ieee/symbol.scm 265 */
 obj_t BgL_aux1671z00_1890;
BgL_aux1671z00_1890 = 
CDR(BgL_pairz00_1588); 
if(
PAIRP(BgL_aux1671z00_1890))
{ /* Ieee/symbol.scm 265 */
BgL_pairz00_1591 = BgL_aux1671z00_1890; }  else 
{ 
 obj_t BgL_auxz00_2103;
BgL_auxz00_2103 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1717z00zz__r4_symbols_6_4z00, 
BINT(10811L), BGl_string1733z00zz__r4_symbols_6_4z00, BGl_string1726z00zz__r4_symbols_6_4z00, BgL_aux1671z00_1890); 
FAILURE(BgL_auxz00_2103,BFALSE,BFALSE);} } 
return 
CAR(BgL_pairz00_1591);} }  else 
{ /* Ieee/symbol.scm 267 */
 obj_t BgL_arg1206z00_1119;
{ /* Ieee/symbol.scm 267 */
 obj_t BgL_pairz00_1592;
if(
PAIRP(BgL_plz00_1114))
{ /* Ieee/symbol.scm 267 */
BgL_pairz00_1592 = BgL_plz00_1114; }  else 
{ 
 obj_t BgL_auxz00_2110;
BgL_auxz00_2110 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1717z00zz__r4_symbols_6_4z00, 
BINT(10853L), BGl_string1733z00zz__r4_symbols_6_4z00, BGl_string1726z00zz__r4_symbols_6_4z00, BgL_plz00_1114); 
FAILURE(BgL_auxz00_2110,BFALSE,BFALSE);} 
{ /* Ieee/symbol.scm 267 */
 obj_t BgL_pairz00_1595;
{ /* Ieee/symbol.scm 267 */
 obj_t BgL_aux1675z00_1894;
BgL_aux1675z00_1894 = 
CDR(BgL_pairz00_1592); 
if(
PAIRP(BgL_aux1675z00_1894))
{ /* Ieee/symbol.scm 267 */
BgL_pairz00_1595 = BgL_aux1675z00_1894; }  else 
{ 
 obj_t BgL_auxz00_2117;
BgL_auxz00_2117 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1717z00zz__r4_symbols_6_4z00, 
BINT(10847L), BGl_string1733z00zz__r4_symbols_6_4z00, BGl_string1726z00zz__r4_symbols_6_4z00, BgL_aux1675z00_1894); 
FAILURE(BgL_auxz00_2117,BFALSE,BFALSE);} } 
BgL_arg1206z00_1119 = 
CDR(BgL_pairz00_1595); } } 
{ 
 obj_t BgL_plz00_2122;
BgL_plz00_2122 = BgL_arg1206z00_1119; 
BgL_plz00_1114 = BgL_plz00_2122; 
goto BgL_zc3z04anonymousza31201ze3z87_1115;} } } } }  else 
{ /* Ieee/symbol.scm 259 */
return 
BGl_errorz00zz__errorz00(BGl_string1734z00zz__r4_symbols_6_4z00, BGl_string1732z00zz__r4_symbols_6_4z00, BgL_symbolz00_13);} } } 

}



/* &getprop */
obj_t BGl_z62getpropz62zz__r4_symbols_6_4z00(obj_t BgL_envz00_1844, obj_t BgL_symbolz00_1845, obj_t BgL_keyz00_1846)
{
{ /* Ieee/symbol.scm 258 */
return 
BGl_getpropz00zz__r4_symbols_6_4z00(BgL_symbolz00_1845, BgL_keyz00_1846);} 

}



/* putprop! */
BGL_EXPORTED_DEF obj_t BGl_putpropz12z12zz__r4_symbols_6_4z00(obj_t BgL_symbolz00_15, obj_t BgL_keyz00_16, obj_t BgL_valz00_17)
{
{ /* Ieee/symbol.scm 273 */
{ /* Ieee/symbol.scm 274 */
 bool_t BgL_test1812z00_2125;
if(
SYMBOLP(BgL_symbolz00_15))
{ /* Ieee/symbol.scm 274 */
BgL_test1812z00_2125 = ((bool_t)1)
; }  else 
{ /* Ieee/symbol.scm 274 */
BgL_test1812z00_2125 = 
KEYWORDP(BgL_symbolz00_15)
; } 
if(BgL_test1812z00_2125)
{ /* Ieee/symbol.scm 275 */
 obj_t BgL_g1041z00_1125;
if(
SYMBOLP(BgL_symbolz00_15))
{ /* Ieee/symbol.scm 246 */
BgL_g1041z00_1125 = 
GET_SYMBOL_PLIST(BgL_symbolz00_15); }  else 
{ /* Ieee/symbol.scm 246 */
if(
KEYWORDP(BgL_symbolz00_15))
{ /* Ieee/symbol.scm 248 */
BgL_g1041z00_1125 = 
GET_KEYWORD_PLIST(BgL_symbolz00_15); }  else 
{ /* Ieee/symbol.scm 248 */
BgL_g1041z00_1125 = 
BGl_errorz00zz__errorz00(BGl_string1731z00zz__r4_symbols_6_4z00, BGl_string1732z00zz__r4_symbols_6_4z00, BgL_symbolz00_15); } } 
{ 
 obj_t BgL_plz00_1127;
BgL_plz00_1127 = BgL_g1041z00_1125; 
BgL_zc3z04anonymousza31211ze3z87_1128:
if(
NULLP(BgL_plz00_1127))
{ /* Ieee/symbol.scm 278 */
 obj_t BgL_newz00_1130;
{ /* Ieee/symbol.scm 278 */
 obj_t BgL_arg1215z00_1132;
{ /* Ieee/symbol.scm 278 */
 obj_t BgL_arg1216z00_1133;
if(
SYMBOLP(BgL_symbolz00_15))
{ /* Ieee/symbol.scm 246 */
BgL_arg1216z00_1133 = 
GET_SYMBOL_PLIST(BgL_symbolz00_15); }  else 
{ /* Ieee/symbol.scm 246 */
if(
KEYWORDP(BgL_symbolz00_15))
{ /* Ieee/symbol.scm 248 */
BgL_arg1216z00_1133 = 
GET_KEYWORD_PLIST(BgL_symbolz00_15); }  else 
{ /* Ieee/symbol.scm 248 */
BgL_arg1216z00_1133 = 
BGl_errorz00zz__errorz00(BGl_string1731z00zz__r4_symbols_6_4z00, BGl_string1732z00zz__r4_symbols_6_4z00, BgL_symbolz00_15); } } 
BgL_arg1215z00_1132 = 
MAKE_YOUNG_PAIR(BgL_valz00_17, BgL_arg1216z00_1133); } 
BgL_newz00_1130 = 
MAKE_YOUNG_PAIR(BgL_keyz00_16, BgL_arg1215z00_1132); } 
if(
SYMBOLP(BgL_symbolz00_15))
{ /* Ieee/symbol.scm 279 */
SET_SYMBOL_PLIST(BgL_symbolz00_15, BgL_newz00_1130); }  else 
{ /* Ieee/symbol.scm 279 */
SET_KEYWORD_PLIST(BgL_symbolz00_15, BgL_newz00_1130); } 
return BgL_newz00_1130;}  else 
{ /* Ieee/symbol.scm 283 */
 bool_t BgL_test1820z00_2151;
{ /* Ieee/symbol.scm 283 */
 obj_t BgL_tmpz00_2152;
{ /* Ieee/symbol.scm 283 */
 obj_t BgL_pairz00_1600;
if(
PAIRP(BgL_plz00_1127))
{ /* Ieee/symbol.scm 283 */
BgL_pairz00_1600 = BgL_plz00_1127; }  else 
{ 
 obj_t BgL_auxz00_2155;
BgL_auxz00_2155 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1717z00zz__r4_symbols_6_4z00, 
BINT(11501L), BGl_string1733z00zz__r4_symbols_6_4z00, BGl_string1726z00zz__r4_symbols_6_4z00, BgL_plz00_1127); 
FAILURE(BgL_auxz00_2155,BFALSE,BFALSE);} 
BgL_tmpz00_2152 = 
CAR(BgL_pairz00_1600); } 
BgL_test1820z00_2151 = 
(BgL_tmpz00_2152==BgL_keyz00_16); } 
if(BgL_test1820z00_2151)
{ /* Ieee/symbol.scm 284 */
 obj_t BgL_arg1220z00_1136;
{ /* Ieee/symbol.scm 284 */
 obj_t BgL_pairz00_1601;
if(
PAIRP(BgL_plz00_1127))
{ /* Ieee/symbol.scm 284 */
BgL_pairz00_1601 = BgL_plz00_1127; }  else 
{ 
 obj_t BgL_auxz00_2163;
BgL_auxz00_2163 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1717z00zz__r4_symbols_6_4z00, 
BINT(11532L), BGl_string1733z00zz__r4_symbols_6_4z00, BGl_string1726z00zz__r4_symbols_6_4z00, BgL_plz00_1127); 
FAILURE(BgL_auxz00_2163,BFALSE,BFALSE);} 
BgL_arg1220z00_1136 = 
CDR(BgL_pairz00_1601); } 
{ /* Ieee/symbol.scm 284 */
 obj_t BgL_pairz00_1602;
if(
PAIRP(BgL_arg1220z00_1136))
{ /* Ieee/symbol.scm 284 */
BgL_pairz00_1602 = BgL_arg1220z00_1136; }  else 
{ 
 obj_t BgL_auxz00_2170;
BgL_auxz00_2170 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1717z00zz__r4_symbols_6_4z00, 
BINT(11534L), BGl_string1733z00zz__r4_symbols_6_4z00, BGl_string1726z00zz__r4_symbols_6_4z00, BgL_arg1220z00_1136); 
FAILURE(BgL_auxz00_2170,BFALSE,BFALSE);} 
return 
SET_CAR(BgL_pairz00_1602, BgL_valz00_17);} }  else 
{ /* Ieee/symbol.scm 286 */
 obj_t BgL_arg1221z00_1137;
{ /* Ieee/symbol.scm 286 */
 obj_t BgL_pairz00_1603;
if(
PAIRP(BgL_plz00_1127))
{ /* Ieee/symbol.scm 286 */
BgL_pairz00_1603 = BgL_plz00_1127; }  else 
{ 
 obj_t BgL_auxz00_2177;
BgL_auxz00_2177 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1717z00zz__r4_symbols_6_4z00, 
BINT(11573L), BGl_string1733z00zz__r4_symbols_6_4z00, BGl_string1726z00zz__r4_symbols_6_4z00, BgL_plz00_1127); 
FAILURE(BgL_auxz00_2177,BFALSE,BFALSE);} 
{ /* Ieee/symbol.scm 286 */
 obj_t BgL_pairz00_1606;
{ /* Ieee/symbol.scm 286 */
 obj_t BgL_aux1685z00_1904;
BgL_aux1685z00_1904 = 
CDR(BgL_pairz00_1603); 
if(
PAIRP(BgL_aux1685z00_1904))
{ /* Ieee/symbol.scm 286 */
BgL_pairz00_1606 = BgL_aux1685z00_1904; }  else 
{ 
 obj_t BgL_auxz00_2184;
BgL_auxz00_2184 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1717z00zz__r4_symbols_6_4z00, 
BINT(11567L), BGl_string1733z00zz__r4_symbols_6_4z00, BGl_string1726z00zz__r4_symbols_6_4z00, BgL_aux1685z00_1904); 
FAILURE(BgL_auxz00_2184,BFALSE,BFALSE);} } 
BgL_arg1221z00_1137 = 
CDR(BgL_pairz00_1606); } } 
{ 
 obj_t BgL_plz00_2189;
BgL_plz00_2189 = BgL_arg1221z00_1137; 
BgL_plz00_1127 = BgL_plz00_2189; 
goto BgL_zc3z04anonymousza31211ze3z87_1128;} } } } }  else 
{ /* Ieee/symbol.scm 274 */
BGL_TAIL return 
BGl_errorz00zz__errorz00(BGl_string1734z00zz__r4_symbols_6_4z00, BGl_string1732z00zz__r4_symbols_6_4z00, BgL_symbolz00_15);} } } 

}



/* &putprop! */
obj_t BGl_z62putpropz12z70zz__r4_symbols_6_4z00(obj_t BgL_envz00_1847, obj_t BgL_symbolz00_1848, obj_t BgL_keyz00_1849, obj_t BgL_valz00_1850)
{
{ /* Ieee/symbol.scm 273 */
return 
BGl_putpropz12z12zz__r4_symbols_6_4z00(BgL_symbolz00_1848, BgL_keyz00_1849, BgL_valz00_1850);} 

}



/* remprop! */
BGL_EXPORTED_DEF obj_t BGl_rempropz12z12zz__r4_symbols_6_4z00(obj_t BgL_symbolz00_18, obj_t BgL_keyz00_19)
{
{ /* Ieee/symbol.scm 292 */
{ /* Ieee/symbol.scm 293 */
 bool_t BgL_test1826z00_2192;
if(
SYMBOLP(BgL_symbolz00_18))
{ /* Ieee/symbol.scm 293 */
BgL_test1826z00_2192 = ((bool_t)1)
; }  else 
{ /* Ieee/symbol.scm 293 */
BgL_test1826z00_2192 = 
KEYWORDP(BgL_symbolz00_18)
; } 
if(BgL_test1826z00_2192)
{ /* Ieee/symbol.scm 294 */
 obj_t BgL_g1043z00_1144;
if(
SYMBOLP(BgL_symbolz00_18))
{ /* Ieee/symbol.scm 246 */
BgL_g1043z00_1144 = 
GET_SYMBOL_PLIST(BgL_symbolz00_18); }  else 
{ /* Ieee/symbol.scm 246 */
if(
KEYWORDP(BgL_symbolz00_18))
{ /* Ieee/symbol.scm 248 */
BgL_g1043z00_1144 = 
GET_KEYWORD_PLIST(BgL_symbolz00_18); }  else 
{ /* Ieee/symbol.scm 248 */
BgL_g1043z00_1144 = 
BGl_errorz00zz__errorz00(BGl_string1731z00zz__r4_symbols_6_4z00, BGl_string1732z00zz__r4_symbols_6_4z00, BgL_symbolz00_18); } } 
{ 
 obj_t BgL_oldz00_1146; obj_t BgL_lz00_1147;
BgL_oldz00_1146 = BNIL; 
BgL_lz00_1147 = BgL_g1043z00_1144; 
BgL_zc3z04anonymousza31226ze3z87_1148:
if(
NULLP(BgL_lz00_1147))
{ /* Ieee/symbol.scm 297 */
return BFALSE;}  else 
{ /* Ieee/symbol.scm 299 */
 bool_t BgL_test1831z00_2205;
{ /* Ieee/symbol.scm 299 */
 obj_t BgL_tmpz00_2206;
{ /* Ieee/symbol.scm 299 */
 obj_t BgL_pairz00_1609;
if(
PAIRP(BgL_lz00_1147))
{ /* Ieee/symbol.scm 299 */
BgL_pairz00_1609 = BgL_lz00_1147; }  else 
{ 
 obj_t BgL_auxz00_2209;
BgL_auxz00_2209 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1717z00zz__r4_symbols_6_4z00, 
BINT(12075L), BGl_string1733z00zz__r4_symbols_6_4z00, BGl_string1726z00zz__r4_symbols_6_4z00, BgL_lz00_1147); 
FAILURE(BgL_auxz00_2209,BFALSE,BFALSE);} 
BgL_tmpz00_2206 = 
CAR(BgL_pairz00_1609); } 
BgL_test1831z00_2205 = 
(BgL_tmpz00_2206==BgL_keyz00_19); } 
if(BgL_test1831z00_2205)
{ /* Ieee/symbol.scm 299 */
if(
PAIRP(BgL_oldz00_1146))
{ /* Ieee/symbol.scm 302 */
 obj_t BgL_arg1231z00_1153; obj_t BgL_arg1232z00_1154;
BgL_arg1231z00_1153 = 
CDR(BgL_oldz00_1146); 
{ /* Ieee/symbol.scm 302 */
 obj_t BgL_pairz00_1611;
if(
PAIRP(BgL_lz00_1147))
{ /* Ieee/symbol.scm 302 */
BgL_pairz00_1611 = BgL_lz00_1147; }  else 
{ 
 obj_t BgL_auxz00_2220;
BgL_auxz00_2220 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1717z00zz__r4_symbols_6_4z00, 
BINT(12142L), BGl_string1733z00zz__r4_symbols_6_4z00, BGl_string1726z00zz__r4_symbols_6_4z00, BgL_lz00_1147); 
FAILURE(BgL_auxz00_2220,BFALSE,BFALSE);} 
{ /* Ieee/symbol.scm 302 */
 obj_t BgL_pairz00_1614;
{ /* Ieee/symbol.scm 302 */
 obj_t BgL_aux1691z00_1910;
BgL_aux1691z00_1910 = 
CDR(BgL_pairz00_1611); 
if(
PAIRP(BgL_aux1691z00_1910))
{ /* Ieee/symbol.scm 302 */
BgL_pairz00_1614 = BgL_aux1691z00_1910; }  else 
{ 
 obj_t BgL_auxz00_2227;
BgL_auxz00_2227 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1717z00zz__r4_symbols_6_4z00, 
BINT(12136L), BGl_string1733z00zz__r4_symbols_6_4z00, BGl_string1726z00zz__r4_symbols_6_4z00, BgL_aux1691z00_1910); 
FAILURE(BgL_auxz00_2227,BFALSE,BFALSE);} } 
BgL_arg1232z00_1154 = 
CDR(BgL_pairz00_1614); } } 
{ /* Ieee/symbol.scm 302 */
 obj_t BgL_pairz00_1615;
if(
PAIRP(BgL_arg1231z00_1153))
{ /* Ieee/symbol.scm 302 */
BgL_pairz00_1615 = BgL_arg1231z00_1153; }  else 
{ 
 obj_t BgL_auxz00_2234;
BgL_auxz00_2234 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1717z00zz__r4_symbols_6_4z00, 
BINT(12134L), BGl_string1733z00zz__r4_symbols_6_4z00, BGl_string1726z00zz__r4_symbols_6_4z00, BgL_arg1231z00_1153); 
FAILURE(BgL_auxz00_2234,BFALSE,BFALSE);} 
return 
SET_CDR(BgL_pairz00_1615, BgL_arg1232z00_1154);} }  else 
{ /* Ieee/symbol.scm 301 */
if(
SYMBOLP(BgL_symbolz00_18))
{ /* Ieee/symbol.scm 305 */
 obj_t BgL_arg1234z00_1156;
{ /* Ieee/symbol.scm 305 */
 obj_t BgL_pairz00_1616;
if(
PAIRP(BgL_lz00_1147))
{ /* Ieee/symbol.scm 305 */
BgL_pairz00_1616 = BgL_lz00_1147; }  else 
{ 
 obj_t BgL_auxz00_2243;
BgL_auxz00_2243 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1717z00zz__r4_symbols_6_4z00, 
BINT(12220L), BGl_string1733z00zz__r4_symbols_6_4z00, BGl_string1726z00zz__r4_symbols_6_4z00, BgL_lz00_1147); 
FAILURE(BgL_auxz00_2243,BFALSE,BFALSE);} 
{ /* Ieee/symbol.scm 305 */
 obj_t BgL_pairz00_1619;
{ /* Ieee/symbol.scm 305 */
 obj_t BgL_aux1697z00_1916;
BgL_aux1697z00_1916 = 
CDR(BgL_pairz00_1616); 
if(
PAIRP(BgL_aux1697z00_1916))
{ /* Ieee/symbol.scm 305 */
BgL_pairz00_1619 = BgL_aux1697z00_1916; }  else 
{ 
 obj_t BgL_auxz00_2250;
BgL_auxz00_2250 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1717z00zz__r4_symbols_6_4z00, 
BINT(12214L), BGl_string1733z00zz__r4_symbols_6_4z00, BGl_string1726z00zz__r4_symbols_6_4z00, BgL_aux1697z00_1916); 
FAILURE(BgL_auxz00_2250,BFALSE,BFALSE);} } 
BgL_arg1234z00_1156 = 
CDR(BgL_pairz00_1619); } } 
return 
SET_SYMBOL_PLIST(BgL_symbolz00_18, BgL_arg1234z00_1156);}  else 
{ /* Ieee/symbol.scm 306 */
 obj_t BgL_arg1236z00_1157;
{ /* Ieee/symbol.scm 306 */
 obj_t BgL_pairz00_1620;
if(
PAIRP(BgL_lz00_1147))
{ /* Ieee/symbol.scm 306 */
BgL_pairz00_1620 = BgL_lz00_1147; }  else 
{ 
 obj_t BgL_auxz00_2258;
BgL_auxz00_2258 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1717z00zz__r4_symbols_6_4z00, 
BINT(12264L), BGl_string1733z00zz__r4_symbols_6_4z00, BGl_string1726z00zz__r4_symbols_6_4z00, BgL_lz00_1147); 
FAILURE(BgL_auxz00_2258,BFALSE,BFALSE);} 
{ /* Ieee/symbol.scm 306 */
 obj_t BgL_pairz00_1623;
{ /* Ieee/symbol.scm 306 */
 obj_t BgL_aux1701z00_1920;
BgL_aux1701z00_1920 = 
CDR(BgL_pairz00_1620); 
if(
PAIRP(BgL_aux1701z00_1920))
{ /* Ieee/symbol.scm 306 */
BgL_pairz00_1623 = BgL_aux1701z00_1920; }  else 
{ 
 obj_t BgL_auxz00_2265;
BgL_auxz00_2265 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1717z00zz__r4_symbols_6_4z00, 
BINT(12258L), BGl_string1733z00zz__r4_symbols_6_4z00, BGl_string1726z00zz__r4_symbols_6_4z00, BgL_aux1701z00_1920); 
FAILURE(BgL_auxz00_2265,BFALSE,BFALSE);} } 
BgL_arg1236z00_1157 = 
CDR(BgL_pairz00_1623); } } 
return 
SET_KEYWORD_PLIST(BgL_symbolz00_18, BgL_arg1236z00_1157);} } }  else 
{ /* Ieee/symbol.scm 308 */
 obj_t BgL_arg1238z00_1158;
{ /* Ieee/symbol.scm 308 */
 obj_t BgL_pairz00_1624;
if(
PAIRP(BgL_lz00_1147))
{ /* Ieee/symbol.scm 308 */
BgL_pairz00_1624 = BgL_lz00_1147; }  else 
{ 
 obj_t BgL_auxz00_2273;
BgL_auxz00_2273 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1717z00zz__r4_symbols_6_4z00, 
BINT(12305L), BGl_string1733z00zz__r4_symbols_6_4z00, BGl_string1726z00zz__r4_symbols_6_4z00, BgL_lz00_1147); 
FAILURE(BgL_auxz00_2273,BFALSE,BFALSE);} 
{ /* Ieee/symbol.scm 308 */
 obj_t BgL_pairz00_1627;
{ /* Ieee/symbol.scm 308 */
 obj_t BgL_aux1705z00_1924;
BgL_aux1705z00_1924 = 
CDR(BgL_pairz00_1624); 
if(
PAIRP(BgL_aux1705z00_1924))
{ /* Ieee/symbol.scm 308 */
BgL_pairz00_1627 = BgL_aux1705z00_1924; }  else 
{ 
 obj_t BgL_auxz00_2280;
BgL_auxz00_2280 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1717z00zz__r4_symbols_6_4z00, 
BINT(12299L), BGl_string1733z00zz__r4_symbols_6_4z00, BGl_string1726z00zz__r4_symbols_6_4z00, BgL_aux1705z00_1924); 
FAILURE(BgL_auxz00_2280,BFALSE,BFALSE);} } 
BgL_arg1238z00_1158 = 
CDR(BgL_pairz00_1627); } } 
{ 
 obj_t BgL_lz00_2286; obj_t BgL_oldz00_2285;
BgL_oldz00_2285 = BgL_lz00_1147; 
BgL_lz00_2286 = BgL_arg1238z00_1158; 
BgL_lz00_1147 = BgL_lz00_2286; 
BgL_oldz00_1146 = BgL_oldz00_2285; 
goto BgL_zc3z04anonymousza31226ze3z87_1148;} } } } }  else 
{ /* Ieee/symbol.scm 293 */
return 
BGl_errorz00zz__errorz00(BGl_string1734z00zz__r4_symbols_6_4z00, BGl_string1732z00zz__r4_symbols_6_4z00, BgL_symbolz00_18);} } } 

}



/* &remprop! */
obj_t BGl_z62rempropz12z70zz__r4_symbols_6_4z00(obj_t BgL_envz00_1851, obj_t BgL_symbolz00_1852, obj_t BgL_keyz00_1853)
{
{ /* Ieee/symbol.scm 292 */
return 
BGl_rempropz12z12zz__r4_symbols_6_4z00(BgL_symbolz00_1852, BgL_keyz00_1853);} 

}



/* keyword? */
BGL_EXPORTED_DEF bool_t BGl_keywordzf3zf3zz__r4_symbols_6_4z00(obj_t BgL_objz00_20)
{
{ /* Ieee/symbol.scm 314 */
return 
KEYWORDP(BgL_objz00_20);} 

}



/* &keyword? */
obj_t BGl_z62keywordzf3z91zz__r4_symbols_6_4z00(obj_t BgL_envz00_1854, obj_t BgL_objz00_1855)
{
{ /* Ieee/symbol.scm 314 */
return 
BBOOL(
BGl_keywordzf3zf3zz__r4_symbols_6_4z00(BgL_objz00_1855));} 

}



/* keyword->string */
BGL_EXPORTED_DEF obj_t BGl_keywordzd2ze3stringz31zz__r4_symbols_6_4z00(obj_t BgL_keywordz00_21)
{
{ /* Ieee/symbol.scm 320 */
{ /* Ieee/symbol.scm 321 */
 obj_t BgL_arg1242z00_1938;
BgL_arg1242z00_1938 = 
KEYWORD_TO_STRING(BgL_keywordz00_21); 
return 
BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1242z00_1938);} } 

}



/* &keyword->string */
obj_t BGl_z62keywordzd2ze3stringz53zz__r4_symbols_6_4z00(obj_t BgL_envz00_1856, obj_t BgL_keywordz00_1857)
{
{ /* Ieee/symbol.scm 320 */
{ /* Ieee/symbol.scm 321 */
 obj_t BgL_auxz00_2294;
if(
KEYWORDP(BgL_keywordz00_1857))
{ /* Ieee/symbol.scm 321 */
BgL_auxz00_2294 = BgL_keywordz00_1857
; }  else 
{ 
 obj_t BgL_auxz00_2297;
BgL_auxz00_2297 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1717z00zz__r4_symbols_6_4z00, 
BINT(12945L), BGl_string1735z00zz__r4_symbols_6_4z00, BGl_string1736z00zz__r4_symbols_6_4z00, BgL_keywordz00_1857); 
FAILURE(BgL_auxz00_2297,BFALSE,BFALSE);} 
return 
BGl_keywordzd2ze3stringz31zz__r4_symbols_6_4z00(BgL_auxz00_2294);} } 

}



/* keyword->string! */
BGL_EXPORTED_DEF obj_t BGl_keywordzd2ze3stringz12z23zz__r4_symbols_6_4z00(obj_t BgL_keywordz00_22)
{
{ /* Ieee/symbol.scm 326 */
return 
KEYWORD_TO_STRING(BgL_keywordz00_22);} 

}



/* &keyword->string! */
obj_t BGl_z62keywordzd2ze3stringz12z41zz__r4_symbols_6_4z00(obj_t BgL_envz00_1858, obj_t BgL_keywordz00_1859)
{
{ /* Ieee/symbol.scm 326 */
{ /* Ieee/symbol.scm 327 */
 obj_t BgL_auxz00_2303;
if(
KEYWORDP(BgL_keywordz00_1859))
{ /* Ieee/symbol.scm 327 */
BgL_auxz00_2303 = BgL_keywordz00_1859
; }  else 
{ 
 obj_t BgL_auxz00_2306;
BgL_auxz00_2306 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1717z00zz__r4_symbols_6_4z00, 
BINT(13243L), BGl_string1737z00zz__r4_symbols_6_4z00, BGl_string1736z00zz__r4_symbols_6_4z00, BgL_keywordz00_1859); 
FAILURE(BgL_auxz00_2306,BFALSE,BFALSE);} 
return 
BGl_keywordzd2ze3stringz12z23zz__r4_symbols_6_4z00(BgL_auxz00_2303);} } 

}



/* string->keyword */
BGL_EXPORTED_DEF obj_t BGl_stringzd2ze3keywordz31zz__r4_symbols_6_4z00(obj_t BgL_stringz00_23)
{
{ /* Ieee/symbol.scm 332 */
BGL_TAIL return 
bstring_to_keyword(BgL_stringz00_23);} 

}



/* &string->keyword */
obj_t BGl_z62stringzd2ze3keywordz53zz__r4_symbols_6_4z00(obj_t BgL_envz00_1860, obj_t BgL_stringz00_1861)
{
{ /* Ieee/symbol.scm 332 */
{ /* Ieee/symbol.scm 335 */
 obj_t BgL_auxz00_2312;
if(
STRINGP(BgL_stringz00_1861))
{ /* Ieee/symbol.scm 335 */
BgL_auxz00_2312 = BgL_stringz00_1861
; }  else 
{ 
 obj_t BgL_auxz00_2315;
BgL_auxz00_2315 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1717z00zz__r4_symbols_6_4z00, 
BINT(13590L), BGl_string1738z00zz__r4_symbols_6_4z00, BGl_string1722z00zz__r4_symbols_6_4z00, BgL_stringz00_1861); 
FAILURE(BgL_auxz00_2315,BFALSE,BFALSE);} 
return 
BGl_stringzd2ze3keywordz31zz__r4_symbols_6_4z00(BgL_auxz00_2312);} } 

}



/* symbol->keyword */
BGL_EXPORTED_DEF obj_t BGl_symbolzd2ze3keywordz31zz__r4_symbols_6_4z00(obj_t BgL_symbolz00_24)
{
{ /* Ieee/symbol.scm 342 */
{ /* Ieee/symbol.scm 343 */
 obj_t BgL_arg1244z00_1629;
{ /* Ieee/symbol.scm 173 */
 obj_t BgL_arg1166z00_1631;
BgL_arg1166z00_1631 = 
SYMBOL_TO_STRING(BgL_symbolz00_24); 
BgL_arg1244z00_1629 = 
BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1166z00_1631); } 
return 
bstring_to_keyword(BgL_arg1244z00_1629);} } 

}



/* &symbol->keyword */
obj_t BGl_z62symbolzd2ze3keywordz53zz__r4_symbols_6_4z00(obj_t BgL_envz00_1862, obj_t BgL_symbolz00_1863)
{
{ /* Ieee/symbol.scm 342 */
{ /* Ieee/symbol.scm 343 */
 obj_t BgL_auxz00_2323;
if(
SYMBOLP(BgL_symbolz00_1863))
{ /* Ieee/symbol.scm 343 */
BgL_auxz00_2323 = BgL_symbolz00_1863
; }  else 
{ 
 obj_t BgL_auxz00_2326;
BgL_auxz00_2326 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1717z00zz__r4_symbols_6_4z00, 
BINT(13944L), BGl_string1739z00zz__r4_symbols_6_4z00, BGl_string1719z00zz__r4_symbols_6_4z00, BgL_symbolz00_1863); 
FAILURE(BgL_auxz00_2326,BFALSE,BFALSE);} 
return 
BGl_symbolzd2ze3keywordz31zz__r4_symbols_6_4z00(BgL_auxz00_2323);} } 

}



/* keyword->symbol */
BGL_EXPORTED_DEF obj_t BGl_keywordzd2ze3symbolz31zz__r4_symbols_6_4z00(obj_t BgL_keywordz00_25)
{
{ /* Ieee/symbol.scm 348 */
{ /* Ieee/symbol.scm 349 */
 obj_t BgL_arg1248z00_1633;
{ /* Ieee/symbol.scm 321 */
 obj_t BgL_arg1242z00_1635;
BgL_arg1242z00_1635 = 
KEYWORD_TO_STRING(BgL_keywordz00_25); 
BgL_arg1248z00_1633 = 
BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1242z00_1635); } 
return 
bstring_to_symbol(BgL_arg1248z00_1633);} } 

}



/* &keyword->symbol */
obj_t BGl_z62keywordzd2ze3symbolz53zz__r4_symbols_6_4z00(obj_t BgL_envz00_1864, obj_t BgL_keywordz00_1865)
{
{ /* Ieee/symbol.scm 348 */
{ /* Ieee/symbol.scm 349 */
 obj_t BgL_auxz00_2334;
if(
KEYWORDP(BgL_keywordz00_1865))
{ /* Ieee/symbol.scm 349 */
BgL_auxz00_2334 = BgL_keywordz00_1865
; }  else 
{ 
 obj_t BgL_auxz00_2337;
BgL_auxz00_2337 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1717z00zz__r4_symbols_6_4z00, 
BINT(14246L), BGl_string1740z00zz__r4_symbols_6_4z00, BGl_string1736z00zz__r4_symbols_6_4z00, BgL_keywordz00_1865); 
FAILURE(BgL_auxz00_2337,BFALSE,BFALSE);} 
return 
BGl_keywordzd2ze3symbolz31zz__r4_symbols_6_4z00(BgL_auxz00_2334);} } 

}



/* object-init */
obj_t BGl_objectzd2initzd2zz__r4_symbols_6_4z00(void)
{
{ /* Ieee/symbol.scm 14 */
return BUNSPEC;} 

}



/* generic-init */
obj_t BGl_genericzd2initzd2zz__r4_symbols_6_4z00(void)
{
{ /* Ieee/symbol.scm 14 */
return BUNSPEC;} 

}



/* method-init */
obj_t BGl_methodzd2initzd2zz__r4_symbols_6_4z00(void)
{
{ /* Ieee/symbol.scm 14 */
return BUNSPEC;} 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__r4_symbols_6_4z00(void)
{
{ /* Ieee/symbol.scm 14 */
BGl_modulezd2initializa7ationz75zz__errorz00(88804785L, 
BSTRING_TO_STRING(BGl_string1741z00zz__r4_symbols_6_4z00)); 
return 
BGl_modulezd2initializa7ationz75zz__paramz00(453939141L, 
BSTRING_TO_STRING(BGl_string1741z00zz__r4_symbols_6_4z00));} 

}

#ifdef __cplusplus
}
#endif
